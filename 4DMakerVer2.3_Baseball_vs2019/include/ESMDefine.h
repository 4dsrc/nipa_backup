////////////////////////////////////////////////////////////////////////////////
//
//	ESMDefine.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-26
//
////////////////////////////////////////////////////////////////////////////////
#include "opencv2/core/core.hpp"
#include "opencv2/core/core_c.h"
#include "opencv2/cudawarping.hpp"
#include <vector>
using namespace std;

#pragma once

/***********************************************/ 
/* ESMMovieEvent                               */
/***********************************************/
#ifndef ESMMovieMsg
typedef struct _ESMMovieMsg
{
	_ESMMovieMsg()
	{
		nMsg = 0;
		nOpt = 0;
		nParam1 = 0;
		nParam2 = 0;
		pValue1 = NULL;
		pValue2 = NULL;
	}
	short	nMsg;	//-- 0x01 IMAGE, 0x02 CODEC, 0x03 FILTER
	int		nOpt;
	int		nParam1;
	int		nParam2;
	int		nParam3;
	int		nParam4;
	PVOID	pValue1;
	PVOID	pValue2;
	PVOID	pValue3;
}ESMMovieMsg;

struct MakeMovieInfo
{
	MakeMovieInfo()
	{
		memset(strDscID, 0, sizeof(TCHAR)* 15);
		memset(strMovieName, 0, sizeof(TCHAR)* 15);
		nStartFrame = 0;
		nEndFrame = 0;
		nFrameSpeed = 100;
		nDscIndex = 0;
		b3DLogo = FALSE;
		bLogoZoom = FALSE;
		nPriIndex = 0;
		//bInsertWB = FALSE;
		bPrnColorInfo = FALSE;
	}
	TCHAR strDscID[15];
	TCHAR strMovieName[15];
	TCHAR strMoviePath[MAX_PATH];
	int nDscIndex;
	BOOL b3DLogo;
	BOOL bLogoZoom;
	int nPriIndex;
	BOOL bColorRevision;
	int nPriValue;
	int nPriValue2;
	int nPrinumIndex;
	//BOOL bInsertWB;
	int	nStartFrame;
	int nEndFrame;
	int nFrameSpeed;
	BOOL bPrnColorInfo;
};

struct EffectInfo
{
	EffectInfo()
	{
		nPosX=  0;
		nPosY = 0;
		bZoom = FALSE;
		nZoomRatio = 0;
		bBlur = FALSE;
		nBlurSize = 0;
		nBlurWeight = 0;
		bStepFrame = FALSE;
		nStartZoom = 100;
		nEndZoom = 100;
//		nStepFrameCount = 1;
	}
	int		nPosX;
	int		nPosY;

	BOOL	bStepFrame;
//int		nStepFrameCount;
	int		nStartZoom;
	int		nEndZoom;
	BOOL	bZoom;
	int		nZoomRatio;	
	BOOL	bBlur;
	int		nBlurSize;
	int		nBlurWeight;	
};

struct FrameRGBInfo
{
	FrameRGBInfo()
	{
		dBlue = 0;
		dGreen = 0;
		dRed = 0;
	}
	double dBlue;
	double dGreen;
	double dRed;
};

struct MakeFrameInfo 
{
	/*해당 구조 추가 시 4DReplaySDIPlayer 새 버전으로 커밋해주셔야 됩니다!!!!*/
	MakeFrameInfo()
	{
		memset(strFramePath, 0, MAX_PATH);
		bComplete	= FALSE;
		nFrameSpeed = 0;
		nImageSize	= 0;
		nDscIndex	= 0;
		nPriIndex	= 0;
		nFrameIndex = 0;
		nObjectIndex = 0;
		bColorRevision	= 0;
		nSameFrameIdx = -1;	// cygil 2015-11-18 동일Frame Not Decoding
		Image		= NULL;
		memset(strCameraID, 0, MAX_PATH);
		nSecIdx		=	0;
		pYUVImg		=	NULL;
		bCompleteGPU = 0;
		nWidth		= 0;
		nHeight		= 0;
		bGPUPlay	= FALSE;
		pParent		= NULL;
		bReverse	= FALSE;
		bInsertWB   = FALSE;
		bPrnColorInfo = FALSE;
		memset(strTime,0,MAX_PATH);
		memset(strLocal, 0, MAX_PATH);
		nSameFrameArrIdx = -1;

		b3DLogo = FALSE;
		bLogoZoom = FALSE;
		bMovieReverse = FALSE;
		pY	= NULL;
		pU	= NULL;
		pV	= NULL;

		//wgkim 180513
		nVertical = 0;

		bAJAMaking = FALSE;
		nStabilizerFrame = 0;
	}
	TCHAR	strFramePath[MAX_PATH];
	int		nFrameIndex;
	int		nObjectIndex;
	int		nDscIndex;
	BOOL	b3DLogo;
	BOOL	bLogoZoom;
	int		nPriIndex;
	int		nPriValue;
	int		nPriValue2;
	int		nPrinumIndex;
	BOOL bColorRevision;
	BOOL	bComplete;
	BOOL	bCompleteGPU;
	int		nFrameSpeed;
	int		nImageSize;
	int		nSameFrameIdx;	// cygil 2015-11-18 동일Frame Not Decoding
	TCHAR	strCameraID[MAX_PATH];
	int		nSecIdx;		//2016-11-29 SecIdx 추가
	BYTE*	Image;
	BYTE*	pYUVImg;
	EffectInfo stEffectData;
	BOOL	bValid;
	BOOL	bPlay;
	BOOL	bSensorSync;
	BOOL	bTickSync;
	TCHAR	strDSC_ID[MAX_PATH];
	TCHAR	strDSC_IP[MAX_PATH];
	int		nWidth;
	int		nHeight;
	BOOL	bGPUPlay;
	void*	pParent;
	BOOL	bReverse;
	BOOL	bInsertWB;
	//170210 hjcho
	TCHAR strTime[MAX_PATH];
	TCHAR strLocal[MAX_PATH];
	FrameRGBInfo stColorInfo;
	FrameRGBInfo stAvgColorInfo;
	BOOL	bPrnColorInfo;
	int		nSameFrameArrIdx;
	BOOL	bMovieReverse;
	BYTE*	pY;
	BYTE*	pU;
	BYTE*	pV;

	//wgkim 180513
	int		nVertical;
	CPoint	ptVerticalCenterPoint;

	BOOL bSameDSC;
	BOOL bAJAMaking;
	int  nStabilizerFrame;
	/*해당 구조 추가 시 4DReplaySDIPlayer 새 버전으로 커밋해주셔야 됩니다!!!!*/
};

struct TimeLineInfo
{
	MakeMovieInfo stMovieData;
	vector<EffectInfo> arrEffectData;
};

struct FrameInfo
{
	FrameInfo()
	{
		nFramePos = 0;
		nIsEndFrame = FALSE;
		memset(strMovieID, 0, 15);
	}
	int nFramePos;
	BOOL nIsEndFrame;
	TCHAR strMovieID[15];
};

struct ReceiveFrameData {
	int nTimeStamp;
	int nSize;
	char* nImageData;
};

struct stFPoint
{
	stFPoint()
	{
		x = 0.0;
		y = 0.0;
	}
	double x;
	double y;
};

struct MakeYUVInfo{
	MakeYUVInfo()
	{
		pArrYImage = NULL;
		pArrUImage = NULL;
		pArrVImage = NULL;
		pArrFrameInfo = NULL;
		pszOutputSize = NULL;
		nDSCID = -1;
	}
	CString strPath;
	cv::Size *pszOutputSize;
	int nDSCID;
	//int nSecIdx;
	vector<cv::Mat>*pArrYImage;
	vector<cv::Mat>*pArrUImage;
	vector<cv::Mat>*pArrVImage;
	vector<MakeFrameInfo>* pArrFrameInfo;
};
//#pragma  pack(push, 1)
typedef struct _KSSHeader
{
	_KSSHeader() {	Init(); }
	void Init()
	{
		memset(cameraid, 0, MAX_PATH);
		index	= 0;
		lenght	= 0;
		secIdx	= 0;
		ImageData = NULL;
	}
	char command[4];
	TCHAR cameraid[MAX_PATH];
	int index;
	int lenght;
	int secIdx;
	char* ImageData;
} KSSHeader, *pKSSHeader;
//#pragma  pack(pop)

typedef struct s_copyInfo { 
	char targetText[MAX_PATH];
	char copyText[MAX_PATH];
	char startFolder[MAX_PATH];
	char EndFolder[MAX_PATH];
	int start_index;
	int end_index;	

} COPYINFO; 
struct stMarginInfo
{
	stMarginInfo()
	{
		nMarginX = 0;
		nMarginY = 0;
	}
	int nMarginX;
	int nMarginY;
};
struct stAdjustInfo
{
	stAdjustInfo()
	{
		AdjAngle = 0.0;
		AdjSize = 0.0;
		nWidth = 0;
		nHeight = 0;
	}
	CString strDSC;
	stFPoint AdjMove, AdjptRotate, AdjptRotate2;
	double AdjAngle, AdjSize;
	int nWidth, nHeight;

	//int nMarginX, nMarginY;
	stMarginInfo stMargin;
	CRect rtMargin;
};

// 
// struct CudaFrameData
// {
// 	cuda::GpuMat* m_pCvImageBuffer;
// };

//-- 2014-09-17 hongsu@esmlab.com
//-- For Valid Frame
struct ValidFrame
{
	ValidFrame()
	{
		strDSC = _T("");		
		nCurrentIndex = 0;
		nCorrectIndex = 0;
	}
	CString strDSC;
	int nCurrentIndex;
	int nCorrectIndex;
};
//170111 hjcho
struct DisplayMovieData
{
	DisplayMovieData()
	{
		nFinish = 0;
	}
	vector<cv::Mat> pPlayData;
	CString strPath;
	CString strTime;
	int nFinish;
};

//wgkim 181002
struct KZoneDataInfo
{
	KZoneDataInfo()
	{
		bIsEnabled = FALSE;
		//nDscId = 0;
	}
	BOOL bIsEnabled;
	//int nDscId;
	char szDscID[5];
	CPoint ptKZonePoint[10];
};

struct MuxDataInfo
{
	MuxDataInfo()
	{
		nOutWidth   = 0;
		nOutHeight  = 0;
		bInserID    = 0;
		nStartFrame = 0;
		nEndFrame   = 0;
		bColorTrans = 0;
		memset(strCamID, 0, MAX_PATH);
		memset(strPath, 0, MAX_PATH);
		memset(strSavePath, 0, MAX_PATH);
		memset(dbArrColorInfo,0,sizeof(double)*3);
		memset(strFileName,0,MAX_PATH);
		memset(strHomePath,0,MAX_PATH);
		//bDirect		= FALSE;
		bSendTo4DP	= FALSE;
		bMovieReverse = FALSE;
		bMuxBoth	= FALSE;
		dbZoomRatio	= 0.0;
		bPSCP		= FALSE;
		nEncodingMethod	= -1;
		bRefereeMode = FALSE;
		pAdjInfo = NULL;
		bLowBitrate  = FALSE;
		bLocal	= FALSE;
		ptCenterPoint = CPoint(0, 0);

		nSelectedContainer = 0;
		nBitrate = 0;
	}
	//Mux Method
	BOOL	bMuxBoth;
	int		nEncodingMethod;
	//Movie Size
	int		nOutWidth;
	int		nOutHeight;
	//Movie Frame Index
	int		nStartFrame;
	int		nEndFrame;
	//Path
	TCHAR	strFileName[3][MAX_PATH];
	TCHAR	strSavePath[3][MAX_PATH];
	TCHAR	strPath[MAX_PATH];//Movie file Path
	TCHAR	strCamID[MAX_PATH];
	TCHAR	strHomePath[MAX_PATH];
	BOOL	bInserID;
	//Image Processing
	BOOL     bColorTrans;
	double   dbArrColorInfo[3];
	BOOL	bMovieReverse;
	double	dbZoomRatio;
	CPoint	ptCenterPoint;	//wgkim 190113
	//Communication
	BOOL	bSendTo4DP; // TCP 
	BOOL	bPSCP;
	BOOL	bRefereeMode;
	stAdjustInfo* pAdjInfo;
	BOOL	bLowBitrate;
	BOOL	bLocal;
	
	BOOL nSelectedContainer; //wgkim 190214
	int nBitrate; //wgkim 190214
};
#endif

//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가
typedef struct _stimagelist
{
	_stimagelist()
	{
		ImagePtr = NULL;
		nFrameIndex = 0;
	}
	_stimagelist(int nFIdx, IplImage* iplImage)
	{
		nFrameIndex = nFIdx;
		ImagePtr = iplImage;
	}
	IplImage* ImagePtr;
	int nFrameIndex ;
}ST_IMAGELIST, *PST_IMAGELIST;


struct DetectInfo
{
	DetectInfo()
	{
		memset(_strId, 0, MAX_PATH);
		_nTime = 0;
	}
	TCHAR	_strId[MAX_PATH];
	int		_nTime;
};

// 카메라별로 property 값이 다를경우 로그로 표시하기 위해 쓰임
struct PropertyDiffCheckedID		
{
	PropertyDiffCheckedID()
	{
		bShutterSpeed = false;	
		bPictureWizard = false;
		bIso = false;
		bMovieSize = false;
		bWhiteBalance = false;
		bColorTemp = false;
		bPhotoStyle = false;	
	}
	bool bShutterSpeed;
	bool bPictureWizard;
	bool bIso;
	bool bMovieSize;
	bool bWhiteBalance;
	bool bColorTemp;
	bool bPhotoStyle;
};

struct PropertyDiffCount		
{
	PropertyDiffCount()
	{
		nShutterSpeed = 0;	
		nPictureWizard = 0;
		nIso = 0;
		nMovieSize = 0;
		nWhiteBalance = 0;
		nColorTemp = 0;
		nPhotoStyle = 0;	
		mapCheckedID = NULL;
	}		
	// 여러번 호출될때 중복체크가 되지않게 하기위해 추가
	CMap<CString, LPCTSTR, PropertyDiffCheckedID, PropertyDiffCheckedID>* mapCheckedID;
	USHORT nShutterSpeed;
	USHORT nPictureWizard;
	USHORT nIso;
	USHORT nMovieSize;
	USHORT nWhiteBalance;
	USHORT nColorTemp;
	USHORT nPhotoStyle;
};

struct PropertyMinMaxInfo
{
	PropertyMinMaxInfo()
	{
		nFMin = 0;
		nFMax = 0;
		nUnknown = 0;
		nAuto = 0;
		strIDList = NULL;
	}
	CStringList* strIDList;
	USHORT nFMin;
	USHORT nFMax;
	USHORT nUnknown;
	USHORT nAuto;
};

struct MakeMultiView
{
	MakeMultiView()
	{
		memset(strCamID,0x00,MAX_PATH);
		memset(strPath,0x00,MAX_PATH);
		nStartFrame = 0;
		nEndFrame = 0;
		nMovIdx = 0;
		nPointX = 1920;
		nPointY = 1080;
		nZoom	= 100;
		bReverse = FALSE;
	};
	//Path
	TCHAR	strCamID[MAX_PATH];
	TCHAR	strPath[MAX_PATH];
	int nStartFrame;
	int nEndFrame;
	int nMovIdx;
	int nZoom;
	int nPointX;
	int nPointY;
	int nFrameRate;
	BOOL bReverse;
};
enum
{
	ESM_CREATE_MOVIE_ORDER	,	
	ESM_CREATE_MOIVE_DECODE	,
	ESM_CREATE_MOIVE_RESIZE	,
	ESM_CREATE_MOIVE_ROTATE	,
	ESM_CREATE_MOIVE_MOVE	,
	ESM_CREATE_MOIVE_CUT	,
	ESM_CREATE_MOIVE_EFFECT	,
	//ESM_CREATE_MOIVE_COLORCALIB ,
	ESM_CREATE_MOIVE_lOGO ,

								// Add Level...
								
	ESM_CREATE_MOIVE_DONE,		// Last Level
};

enum
{
	ESM_OBJECT_FRAME_STATUS_NONE	,
	ESM_OBJECT_FRAME_STATUS_SELECT	,
	ESM_OBJECT_FRAME_STATUS_SPOT	,
};


#define ESM_MOVIE_PARCER			0x0100
//------------------------------------------------
//-- THREAD MANAGE
#define ESM_MOVIE_THREAD			0x00
#define ESM_MOVIE_THREAD_TERMINATE	0x0001

//------------------------------------------------
//-- INFO
#define ESM_MOVIE_MANAGER						0x01
#define ESM_MOVIE_MANAGER_ADJ_INFO				0x0101
#define ESM_MOVIE_MANAGER_CREATE				0x0102
#define ESM_MOVIE_MANAGER_MOVIELOAD				0x0103
#define ESM_MOVIE_MANAGER_SETMOIVECOUNT			0x0104
#define ESM_MOVIE_MANAGER_MOVIEMAKE				0x0105
#define ESM_MOVIE_MANAGER_SET_MARGIN			0x0106
#define ESM_MOVIE_MANAGER_INFORESET				0x0107
#define ESM_MOVIE_MANAGER_VALIDFRAME			0x0108
#define ESM_MOVIE_MANAGER_VALIDRESET			0x0109
#define ESM_MOVIE_MANAGER_SET_FPS				0x010a
#define ESM_MOVIE_MANAGER_MAKINGSTOP			0x010b
#define ESM_MOVIE_MANAGER_MAKEFRAME				0x010c
#define ESM_MOVIE_MANAGER_MAKEFRAME_REVERSE		0x010d
#define ESM_MOVIE_MANAGER_MAKEFRAME_LOCAL				0x010e
#define ESM_MOVIE_MANAGER_MAKEFRAME_REVERSE_LOCAL		0x010f
#define ESM_MOVIE_MANAGER_SET_IMAGESIZE			0x0110
#define ESM_MOVIE_MANAGER_MAKEFRAME_ADJUST		0x0111
#define ESM_MOVIE_MANAGER_MUX					0x0112

//wgkim 170727
#define ESM_MOVIE_MANAGER_SET_LIGHT_WEIGHT		0x0113
#define ESM_MOVIE_MANAGER_REQUEST_DATA			0x0114

//wgkim 180629
#define ESM_MOVIE_MANAGER_SET_FRMAERATE			0x0115
#define ESM_MOVIE_MANAGER_SET_KZONE_POINTS		0x0116
#define ESM_MOVIE_MANAGER_OPTION_AUTOADJUST_KZONE	0x0117
#define ESM_MOVIE_MANAGER_MULTIVIEW_MAKING		0x0118
//------------------------------------------------
//-- IMAGE
#define ESM_MOVIE_IMAGE				0x02
#define ESM_MOVIE_IMAGE_RESIZE		0x0201
#define ESM_MOVIE_IMAGE_ROTATE		0x0202
#define ESM_MOVIE_IMAGE_MOVE		0x0203
#define ESM_MOVIE_IMAGE_CUT			0x0204
#define ESM_MOVIE_IMAGE_EFFECT		0x0205
#define ESM_MOVIE_IMAGE_COLORCALIB	0x0206
#define ESM_MOVIE_IMAGE_LOGO		0x0207
#define ESM_MOVIE_IMAGE_NONFRAME	0x0210

//------------------------------------------------
//-- CODEC
#define ESM_MOVIE_CODEC					0x03
#define ESM_MOVIE_CODEC_H264_DECODE		0x0301
#define ESM_MOVIE_CODEC_H264_ENCODE		0x0302
#define ESM_MOVIE_CODEC_H265_DECODE		0x0303
#define ESM_MOVIE_CODEC_H265_ENCODE		0x0304
#define ESM_MOVIE_CODEC_GETMOVIEINFO	0x0305

//------------------------------------------------
//-- FILTER
#define ESM_MOVIE_FILTER				0x04
#define ESM_MOVIE_FILTER_DENOISE		0x0401
#define ESM_MOVIE_FILTER_COLORADJUST	0X0402

//------------------------------------------------
//-- KT Defined
#define KT_MULTIGPU
#define KT_PLAN_A
//-- IBC TEST
#define _IBC
enum {
	DECODING_TYPE_MOVIE = 0,
	DECODING_TYPE_FRAME,
};

#define GRAPIC_BOARD_NAME _T("NVIDIA GeForce GTX 1060 3GB")
#define GRAPIC_BOARD_NAME2 _T("NVIDIA GeForce GTX 760")
#define GRAPIC_BOARD_NAME3 _T("NVIDIA GeForce GTX 1050 Ti")
#define GRAPIC_BOARD_NAME4 _T("NVIDIA GeForce GTX 1060")
#define GRAPIC_BOARD_NAME5  _T("NVIDIA GeForce GTX 1060 6GB")

/**/
#define ESM_4DMAKER_SDIPLAYER			_T("SDIPlayer.ini")
#define STR_SECTION_SDIPLAYER			_T("SDIPlayer")
#define STR_STATE				_T("State")
#define STR_FILENAME			_T("FileName")
#define STR_FILEPATH			_T("FilePath")


struct ESMRefereeInfo{

	ESMRefereeInfo()
	{
		memset(strDSCId,	0, MAX_PATH);
		memset(strIp,		0, MAX_PATH);
		memset(strGroup,	0, MAX_PATH);
	}
	TCHAR strDSCId[MAX_PATH];
	TCHAR strIp[MAX_PATH];
	TCHAR strGroup[MAX_PATH];
};