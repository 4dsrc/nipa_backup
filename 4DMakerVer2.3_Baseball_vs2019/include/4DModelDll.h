#ifndef __MODELER_DLL_H__
#define __MODELER_DLL_H__


#define MODELER_EXPORT_DLL extern "C" __declspec(dllexport)
#define MODELER_IMPORT_DLL extern "C" __declspec(dllimport)
MODELER_EXPORT_DLL int GetDllMacAddrCount();
MODELER_EXPORT_DLL void GetDllMacAddrFormList(int nIndex, TCHAR* strDllMacAddr);



#endif//__NETWORK_TRAFFIC_LIB_H__