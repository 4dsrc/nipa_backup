////////////////////////////////////////////////////////////////////////////////
//
//	ESMDefine.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-26
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

/***********************************************/ 
/* ESMMovieEvent                               */
/***********************************************/
#ifndef ESMMotorMsg

typedef struct _ESMMotorMsg
{
	int		nMsg;
	int		nOpt;	
}ESMMotorMsg;

#endif