#ifndef OPTION_LIST_H
#define OPTION_LIST_H
#include "list.h"

typedef struct{
    char *key;
    char *val;
    int used;
} kvp;


int read_option(char *s, dark_list *options);
void option_insert(dark_list *l, char *key, char *val);
char *option_find(dark_list *l, char *key);
float option_find_float(dark_list *l, char *key, float def);
float option_find_float_quiet(dark_list *l, char *key, float def);
void option_unused(dark_list *l);

#endif
