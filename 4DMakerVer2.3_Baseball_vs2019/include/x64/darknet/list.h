#ifndef LIST_H
#define LIST_H
#include "darknet.h"

dark_list *make_list();
int list_find(dark_list *l, void *val);

void list_insert(dark_list *, void *);


void free_list_contents(dark_list *l);

#endif
