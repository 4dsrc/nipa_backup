#ifndef IMAGE_H
#define IMAGE_H

#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include "box.h"
#include "darknet.h"

#ifdef OPENCV
int fill_image_from_stream(CvCapture *cap, image im);
void ipl_into_image(IplImage* src, image im);
void flush_stream_buffer(CvCapture *cap, int n);
void show_image_cv(image p, const char *name, IplImage *disp);
#endif

int getMatType_F(int channels);
image ipl_to_image(IplImage* src);
cv::Mat image_to_mat(image im);
image mat_to_image(cv::Mat m);
void* open_video_stream(const char* f, int c, int w, int h, int fps);
image get_image_from_stream(void* p);
int show_image_cv(image im, const char* name, int ms);
void make_window(char* name, int w, int h, int fullscreen);
image mask_to_rgb(image mask);
static float get_pixel(image m, int x, int y, int c);
static float get_pixel_extend(image m, int x, int y, int c);
static void set_pixel(image m, int x, int y, int c, float val);
static void add_pixel(image m, int x, int y, int c, float val);
static float bilinear_interpolate(image im, float x, float y, int c);
void composite_image(image source, image dest, int dx, int dy);
image border_image(image a, int border);
image tile_images(image a, image b, int dx);
image get_label(image** characters, char* string, int size);
void draw_label(image a, int r, int c, image label, const float* rgb);
void draw_box_width(image a, int x1, int y1, int x2, int y2, int w, float r, float g, float b);
void draw_bbox(image a, box bbox, int w, float r, float g, float b);
image** load_alphabet();
void draw_detections(image im, detection* dets, int num, float thresh, char** names, image** alphabet, int classes);
void transpose_image(image im);
void rotate_image_cw(image im, int times);
void flip_image(image a);
void ghost_image(image source, image dest, int dx, int dy);
void blocky_image(image im, int s);
void censor_image(image im, int dx, int dy, int w, int h);
image collapse_image_layers(image source, int border);
void constrain_image(image im);
void normalize_image(image p);
void normalize_image2(image p);
image copy_image(image p);
void rgbgr_image(image im);
void show_image_cv(image p, const char* name, cv::Mat* disp);
int show_image(image p, const char* name, int ms);

void ipl_into_image(IplImage* src, image im);
image ipl_to_image(IplImage* src);
image load_image_cv(char* filename, int channels);
void flush_stream_buffer(CvCapture* cap, int n);
image get_image_from_stream(cv::VideoCapture* cap);
int fill_image_from_stream(cv::VideoCapture* cap, image im);
void save_image_jpg(image p, const char* name);
void save_image_png(image im, const char* name);
void save_image(image im, const char* name);
image make_random_image(int w, int h, int c);
image float_to_image(int w, int h, int c, float* data);
image center_crop_image(image im, int w, int h);
image rotate_image(image im, float rad);




float get_color(int c, int x, int max);
void draw_box(image a, int x1, int y1, int x2, int y2, float r, float g, float b);
void draw_bbox(image a, box bbox, int w, float r, float g, float b);
void write_label(image a, int r, int c, image *characters, char *string, float *rgb);
image image_distance(image a, image b);
void scale_image(image m, float s);
image rotate_crop_image(image im, float rad, float s, int w, int h, float dx, float dy, float aspect);
image random_crop_image(image im, int w, int h);
image random_augment_image(image im, float angle, float aspect, int low, int high, int w, int h);
augment_args random_augment_args(image im, float angle, float aspect, int low, int high, int w, int h);
void letterbox_image_into(image im, int w, int h, image boxed);
image resize_max(image im, int max);
void translate_image(image m, float s);
void embed_image(image source, image dest, int dx, int dy);
void place_image(image im, int w, int h, int dx, int dy, image canvas);
void saturate_image(image im, float sat);
void exposure_image(image im, float sat);
void distort_image(image im, float hue, float sat, float val);
void saturate_exposure_image(image im, float sat, float exposure);
void rgb_to_hsv(image im);
void hsv_to_rgb(image im);
void yuv_to_rgb(image im);
void rgb_to_yuv(image im);


image collapse_image_layers(image source, int border);
image collapse_images_horz(image *ims, int n);
image collapse_images_vert(image *ims, int n);

void show_image_normalized(image im, const char *name);
void show_images(image *ims, int n, char *window);
void show_image_layers(image p, char *name);
void show_image_collapsed(image p, char *name);

void print_image(image m);

image make_empty_image(int w, int h, int c);
void copy_image_into(image src, image dest);

image get_image_layer(image m, int l);

#endif

