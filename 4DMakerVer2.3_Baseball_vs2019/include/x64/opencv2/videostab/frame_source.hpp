/*M///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//
//                           License Agreement
//                For Open Source Computer Vision Library
//
// Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
// Copyright (C) 2009-2011, Willow Garage Inc., all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//   * Redistribution's of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//   * Redistribution's in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//
//   * The name of the copyright holders may not be used to endorse or promote products
//     derived from this software without specific prior written permission.
//
// This software is provided by the copyright holders and contributors "as is" and
// any express or implied warranties, including, but not limited to, the implied
// warranties of merchantability and fitness for a particular purpose are disclaimed.
// In no event shall the Intel Corporation or contributors be liable for any direct,
// indirect, incidental, special, exemplary, or consequential damages
// (including, but not limited to, procurement of substitute goods or services;
// loss of use, data, or profits; or business interruption) however caused
// and on any theory of liability, whether in contract, strict liability,
// or tort (including negligence or otherwise) arising in any way out of
// the use of this software, even if advised of the possibility of such damage.
//
//M*/

#ifndef OPENCV_VIDEOSTAB_FRAME_SOURCE_HPP
#define OPENCV_VIDEOSTAB_FRAME_SOURCE_HPP

#include <vector>
#include "opencv2/core.hpp"

namespace cv
{
namespace videostab
{

//! @addtogroup videostab
//! @{

class CV_EXPORTS IFrameSource
{
public:
    virtual ~IFrameSource() {}
    virtual void reset() = 0;
    virtual Mat nextFrame() = 0;

	//wgkim 191016
	virtual Mat nextFrameForGeneratingTransformationMatrix() = 0;
	virtual Mat nextFrameForApplyingTransformationMatrix() = 0;
	virtual Size size() = 0;
};

class CV_EXPORTS NullFrameSource : public IFrameSource
{
public:
    virtual void reset() CV_OVERRIDE {}
    virtual Mat nextFrame() CV_OVERRIDE { return Mat(); }

	//wgkim 191016
	virtual Mat nextFrameForGeneratingTransformationMatrix() { return Mat(); }
	virtual Mat nextFrameForApplyingTransformationMatrix() { return Mat(); }
	virtual Size size() { return Size(); }
};

class CV_EXPORTS VideoFileSource : public IFrameSource
{
public:
    VideoFileSource(const String &path, bool volatileFrame = false);

    virtual void reset() CV_OVERRIDE;
    virtual Mat nextFrame() CV_OVERRIDE;

	//wgkim 191016
	virtual	Mat nextFrameForApplyingTransformationMatrix();
	virtual Mat nextFrameForGeneratingTransformationMatrix();

    int width();
    int height();
    int count();
    double fps();

private:
    Ptr<IFrameSource> impl;
};

//wgkim 191016
//vector<Mat> 형태의 Image들을 바로 stabilization하기 위한 클래스
class CV_EXPORTS ImageSetForStabilization : public IFrameSource
{
public:
	std::vector<Mat> imagesForGeneratingTransformationMatrix;
	std::vector<Mat> imagesForApplyingTransformationMatrix;
	int readCount;
	int readCount_test;

public:
	ImageSetForStabilization() { readCount = 0; readCount_test = 0; }
	virtual void reset() { readCount = 0; readCount_test = 0; }

	virtual Mat nextFrame() { return Mat(); }

	void inputImageForGeneratingTransformationMatrix(Mat& image)
	{
		imagesForGeneratingTransformationMatrix.push_back(image.clone());
	}
	void inputImageForApplyingTransformationMatrix(Mat& image)
	{
		imagesForApplyingTransformationMatrix.push_back(image.clone());
	}

	virtual Mat nextFrameForGeneratingTransformationMatrix()
	{
		Mat frame;

		imagesForGeneratingTransformationMatrix[readCount].copyTo(frame);
		if (!frame.empty())
			readCount++;

		return frame;
	}

	virtual Mat nextFrameForApplyingTransformationMatrix()
	{
		Mat frame;

		imagesForApplyingTransformationMatrix[readCount_test].copyTo(frame);
		if (!frame.empty())
			readCount_test++;

		return frame;
	}

	virtual Size size()
	{
		return imagesForApplyingTransformationMatrix[readCount_test].size();
	}

	int width() { return static_cast<int>(imagesForApplyingTransformationMatrix[readCount].cols); }
	int height() { return static_cast<int>(imagesForApplyingTransformationMatrix[readCount].rows); }
	int count() { return static_cast<int>(imagesForApplyingTransformationMatrix.size()); }
	double fps() { return static_cast<int>(30.); }
};

class MaskFrameSource : public IFrameSource
{
public:
    MaskFrameSource(const Ptr<IFrameSource>& source): impl(source) {};

    virtual void reset() CV_OVERRIDE { impl->reset(); }
    virtual Mat nextFrame() CV_OVERRIDE {
        Mat nextFrame = impl->nextFrame();
        maskCallback_(nextFrame);
        return nextFrame;
    }

    void setMaskCallback(std::function<void(Mat&)> MaskCallback)
    {
        maskCallback_ = std::bind(MaskCallback, std::placeholders::_1);
    };

private:
    Ptr<IFrameSource> impl;
    std::function<void(Mat&)> maskCallback_;
};

//! @}

} // namespace videostab
} // namespace cv

#endif
