/**
	@file	AJATypes.h

	@copyright	Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.

	@brief	Declares the most fundamental data types used by NTV2.
			Since Windows NT was the Major Development Platform, many typedefs are Windows-centric.
**/
#ifndef AJATYPES_H
#define AJATYPES_H


typedef int            			LWord;
typedef unsigned int   			ULWord;
typedef unsigned int * 			PULWord;
typedef short          			Word;
typedef unsigned short 			UWord;
typedef unsigned char  			UByte;
typedef char           			SByte;


// Platform dependent

/////////////// WINDOWS/////////////////////////
#ifdef  MSWindows

#include <Basetsd.h>
typedef unsigned char Boolean;
typedef __int64 LWord64;
typedef unsigned __int64 ULWord64;
typedef unsigned __int64 Pointer64;
typedef LWord 				Fixed_;
typedef bool				BOOL_;
typedef UWord				UWord_;

typedef signed __int8    int8_t;
typedef signed __int16   int16_t;
typedef signed __int32   int32_t;
typedef signed __int64   int64_t;
typedef unsigned __int8  uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;

typedef UINT_PTR	AJASocket;

#define AJATargetBigEndian  0

/*
#ifndef POINTER_32
#if !defined(_MAC) && (defined(_M_MRX000) || defined(_M_ALPHA) || defined(_M_IA64)) && (_MSC_VER >= 1100) && !(defined(MIDL_PASS) || defined(RC_INVOKED))
#define POINTER_64 __ptr64
typedef unsigned __int64 POINTER_64_INT;
#if defined(_AXP64_)
#define POINTER_32 __ptr32
#else
#define POINTER_32
#endif
#else
#if defined(_MAC) && defined(_MAC_INT_64)
#define POINTER_64 __ptr64
typedef unsigned __int64 POINTER_64_INT;
#else
#define POINTER_64
typedef unsigned long POINTER_64_INT;
#endif
#define POINTER_32
#endif
#endif
*/
///////////////MAC OS X//////////////////////////
#elif defined (AJAMac)
typedef short					HANDLE;
typedef long long int			LWord64;
typedef unsigned long long int 	ULWord64;
typedef unsigned long long int  Pointer64;
typedef void*					PVOID;
typedef unsigned int			BOOL_;
typedef ULWord					UWord_;
typedef int						Fixed_;


typedef int		AJASocket;

#define AJATargetBigEndian  TARGET_RT_BIG_ENDIAN

#define MAX_PATH 4096

#define INVALID_HANDLE_VALUE (0)

typedef struct {
  int cx;
  int cy;
}SIZE;

#define POINTER_32

///////////////LINUX//////////////////////////////
#elif defined AJALinux

/* As of kernel 2.6.19, the C type _Bool is typedefed to bool to allow
 * generic booleans in the kernel.  Unfortunately, we #define bool
 * here and true and false there, so this fixes it ... until next time
 * -JAC 3/6/2007 */
#ifdef __KERNEL__
#include "linux/version.h"
#if defined (RHEL5) || (LINUX_VERSION_CODE > KERNEL_VERSION(2,6,19))
#include "linux/types.h"
#else/* LINUX_VERSION_CODE */
typedef unsigned char       bool;
#endif /* LINUX_VERSION_CODE */
#endif /* __KERNEL__ */

typedef long				HANDLE;
// this is what is is in Windows:
// typedef void *				HANDLE;
typedef unsigned long long	ULWord64;
typedef unsigned long long	Pointer64;
typedef unsigned long long	__int64;
typedef signed long long	LWord64;
typedef void * 				PVOID;
typedef void * 				LPVOID;
typedef LWord				Fixed_;


typedef bool				BOOL_;
#ifndef FS1
typedef bool				BOOL;
#endif
typedef UWord				UWord_;
typedef unsigned int        DWORD; /* 32 bits on 32 or 64 bit CPUS */

typedef int		AJASocket;

#define AJATargetBigEndian  0

typedef struct {
  int cx;
  int cy;
}SIZE;

typedef struct {
  int left;
  int right;
  int top;
  int bottom;
}RECT;

#ifndef INVALID_HANDLE_VALUE
# define INVALID_HANDLE_VALUE (-1L)
#endif
#define WINAPI
#define POINTER_32
#define MAX_PATH 4096

#endif

typedef struct
{
	ULWord registerNumber;
	ULWord registerValue;
	ULWord registerMask;
	ULWord registerShift;
} NTV2ReadWriteRegisterSingle;

// As of kernel 2.6.24, BIT is defined in the kernel source
// (linux/bitops.h).  By making that definition match the one in the
// kernel source *exactly* we supress compiler warnings (thanks Shaun)

#ifndef BIT
#ifndef AJALinux
#define BIT(x)		(1u << (x))
#else
#define BIT(nr)			(1UL << (nr))
#endif	// AJALinux
#endif
// or the stupid way.
#define BIT_0 (1u<<0)
#define BIT_1 (1u<<1)
#define BIT_2 (1u<<2)
#define BIT_3 (1u<<3)
#define BIT_4 (1u<<4)
#define BIT_5 (1u<<5)
#define BIT_6 (1u<<6)
#define BIT_7 (1u<<7)
#define BIT_8 (1u<<8)
#define BIT_9 (1u<<9)
#define BIT_10 (1u<<10)
#define BIT_11 (1u<<11)
#define BIT_12 (1u<<12)
#define BIT_13 (1u<<13)
#define BIT_14 (1u<<14)
#define BIT_15 (1u<<15)
#define BIT_16 (1u<<16)
#define BIT_17 (1u<<17)
#define BIT_18 (1u<<18)
#define BIT_19 (1u<<19)
#define BIT_20 (1u<<20)
#define BIT_21 (1u<<21)
#define BIT_22 (1u<<22)
#define BIT_23 (1u<<23)
#define BIT_24 (1u<<24)
#define BIT_25 (1u<<25)
#define BIT_26 (1u<<26)
#define BIT_27 (1u<<27)
#define BIT_28 (1u<<28)
#define BIT_29 (1u<<29)
#define BIT_30 (1u<<30)
#define BIT_31 (1u<<31)


#endif
