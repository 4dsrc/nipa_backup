/**
	@file		ntv2enums.h
	@copyright	Copyright (C) 2013-2014 AJA Video Systems, Inc.  All rights reserved.
	@brief		Enumerations for controlling NTV2 devices.
**/

#ifndef NTV2ENUMS_H
#define NTV2ENUMS_H



/**
	SDK VERSION
	The next several macro values are patched when the SDK is built by AJA.
**/
#define AJA_NTV2_SDK_VERSION_MAJOR		12								//	(unsigned decimal integer)
#define AJA_NTV2_SDK_VERSION_MINOR		2								//	(unsigned decimal integer)
#define AJA_NTV2_SDK_VERSION_POINT		1								//	(unsigned decimal integer)
#define AJA_NTV2_SDK_BUILD_NUMBER		92								//	(unsigned decimal integer)
#define AJA_NTV2_SDK_BUILD_DATETIME		"05/06/15 13:55:17"			//	"MM/DD/YYYY +8:hh:mm:ss"
#define AJA_NTV2_SDK_BUILD_TYPE			""								//	"a"==alpha, "b"==beta, "d"==development, ""==release

#define	AJA_NTV2_SDK_VERSION	((AJA_NTV2_SDK_VERSION_MAJOR << 24) | (AJA_NTV2_SDK_VERSION_MINOR << 16) | (AJA_NTV2_SDK_VERSION_POINT << 8) | (AJA_NTV2_SDK_BUILD_NUMBER))
#define	AJA_NTV2_SDK_VERSION_AT_LEAST(__a__,__b__)		(AJA_NTV2_SDK_VERSION >= (((__a__) << 24) | ((__b__) << 16)))
#define	AJA_NTV2_SDK_VERSION_BEFORE(__a__,__b__)		(AJA_NTV2_SDK_VERSION < (((__a__) << 24) | ((__b__) << 16)))


/**
	@mainpage	The NTV2 SDK

	@details	The NTV2 Software Development Kit (SDK) is a suite of classes and data types which allow end-users to access and control
				nearly any NTV2-compatible AJA device using the C++ programming language.
				AJA hardware products play back or ingest video/audio, plus ancillary data (e.g., timecode, captions, etc.) to
				or from a host computer. The purpose of the SDK is to enable third-party developers to easily access and/or control
				the video, audio or ancillary data entering or leaving the device. The SDK has two major parts -- a low-level device driver,
				and a user-space library.

				The device driver runs at the kernel level and handles low-level communication with the device. It is a required component
				of the SDK and provides the user-space library with the means to communicate and control the device.

				'classes.lib' (or 'classes.a') is the principal user-space library that an application must link with in order to access
				and control AJA devices. It implements a suite of C++ classes which an application can instantiate and use to perform
				various operations on an AJA device. This library contains many functions that are useful for interrogating and controlling
				AJA devices.
**/

/**
	@deprecated	This typedef will be deprecated in the very near future.
	@brief		This is a holdover from NTV2's legacy involvement in firmware.
				Only one boardtype works with NTV2 SDK 12.0 or later.
**/
typedef enum
{
	DEVICETYPE_UNKNOWN=0,
	DEVICETYPE_NTV2=256,
	DEVICETYPE_MAX=256
	#if !defined (NTV2_DEPRECATE)
		,BOARDTYPE_UNKNOWN	= DEVICETYPE_UNKNOWN,
		BOARDTYPE_HDNTV		= 1,
		BOARDTYPE_KSD		= 2,	// SD only
		BOARDTYPE_KHD		= 4,	// HD only
		BOARDTYPE_AJAXENA2	= 8,	// SD and HD
		BOARDTYPE_AJAKONA	= 8,	// SD and HD
		BOARDTYPE_KONA2		= 16,	// deprecated.
		BOARDTYPE_KONAX		= 32,	// deprecated.
		BOARDTYPE_FS1		= 64,	// SD and HD
		BOARDTYPE_BORG		= 128,	// SD and HD
		BOARDTYPE_NTV2		= DEVICETYPE_NTV2,
		BOARDTYPE_MAX		= DEVICETYPE_MAX
	#endif	//	!defined (NTV2_DEPRECATE)
} NTV2DeviceType;


typedef NTV2DeviceType	NTV2BoardType;


#if !defined (NTV2_DEPRECATE)
	#define BOARDTYPE_SCANNABLE					(BOARDTYPE_KSD | BOARDTYPE_KHD | BOARDTYPE_AJAKONA | BOARDTYPE_FS1 | BOARDTYPE_BORG | BOARDTYPE_NTV2)
#else
	#define BOARDTYPE_SCANNABLE					(BOARDTYPE_NTV2)
#endif

#if defined XENA2
	#define BOARDTYPE_AS_COMPILED               (BOARDTYPE_AJAXENA2)
#else
	#define BOARDTYPE_AS_COMPILED				(DEVICETYPE_NTV2)
#endif

/**
	@brief	The NTV2DeviceID is actually the PROM part number for a given device.
			They are used to determine the feature set of the device, and which firmware to flash.
**/
typedef enum
{
#if !defined (NTV2_DEPRECATE)
	BOARD_ID_XENA_SD		= 0x10133700,		///< @deprecated	Obsolete.
	BOARD_ID_XENA_SD22		= 0x10152100,		///< @deprecated	Obsolete.
	BOARD_ID_XENA_HD		= 0x10133900,		///< @deprecated	Obsolete.
	BOARD_ID_XENA_HD22		= 0x10148300,		///< @deprecated	Obsolete.
	BOARD_ID_HDNTV2			= 0x10156100,		///< @deprecated	Obsolete.
	BOARD_ID_KSD11			= 0x10114400,		///< @deprecated	Obsolete.
	BOARD_ID_XENA_SD_MM		= BOARD_ID_KSD11,	///< @deprecated	Obsolete.
	BOARD_ID_KSD22			= 0x10128600,		///< @deprecated	Obsolete.
	BOARD_ID_XENA_SD22_MM	= BOARD_ID_KSD22, 	///< @deprecated	Obsolete.
	BOARD_ID_KHD11			= 0x10113800,		///< @deprecated	Obsolete.
	BOARD_ID_XENA_HD_MM		= BOARD_ID_KHD11,	///< @deprecated	Obsolete.
	BOARD_ID_XENA_HD22_MM	= 0x10160100,		///< @deprecated	Obsolete.
	BOARD_ID_HDNTV2_MM		= 0x10160200,		///< @deprecated	Obsolete.
	BOARD_ID_KONA_SD		= 0x10111900,		///< @deprecated	Obsolete.
	BOARD_ID_KONA_HD		= 0x10120900,		///< @deprecated	Obsolete.
	BOARD_ID_KONA_HD2		= 0x10120901,		///< @deprecated	Obsolete.		//	Mac version of the KonaR
	BOARD_ID_KONAR			= 0x10133901,		///< @deprecated	Obsolete.		//	PC version of KonaR (revisited)
	BOARD_ID_KONAR_MM		= 0x10113801,		///< @deprecated	Obsolete.
	BOARD_ID_KONA2			= 0x10174700,		///< @deprecated	Obsolete.
	BOARD_ID_HDNTV			= 0,				///< @deprecated	Obsolete.		//	Not strictly true, this is an alias of HDNTV's Register 19, which
																					//	is an unused register that's initialized to 0
	BOARD_ID_KONALS			= 0x10183400,		///< @deprecated	Obsolete.
	BOARD_ID_XENALS			= BOARD_ID_KONALS,	///< @deprecated	Obsolete.
	BOARD_ID_KONAHDS		= 0x10184500,		///< @deprecated	Obsolete.
	BOARD_ID_KONALH			= BOARD_ID_KONAHDS,	///< @deprecated	Obsolete.
	BOARD_ID_XENALH			= BOARD_ID_KONAHDS,	///< @deprecated	Obsolete.
	BOARD_ID_XENADXT		= 0x10182800,		///< @deprecated	Obsolete.
	BOARD_ID_XENAHS			= BOARD_ID_XENADXT,	///< @deprecated	Obsolete.
	BOARD_ID_KONAX			= 0x10203000,		///< @deprecated	Obsolete.
	BOARD_ID_XENAX			= 0x10212300,		///< @deprecated	Obsolete.
	BOARD_ID_XENAHS2		= 0x10182801,		///< @deprecated	Obsolete.		//	XenaHS replacement
	BOARD_ID_FS1			= 0x10207701,		///< @deprecated	Obsolete.
	BOARD_ID_FS2			= 0x10301200,		///< @deprecated	Obsolete.
	BOARD_ID_MOAB			= 0x10224000,		///< @deprecated	Obsolete.
	BOARD_ID_XENAX2			= 0x10212304,		///< @deprecated	Obsolete.
	BOARD_ID_BORG			= 0x10244100,		///< @deprecated	Obsolete.
	BOARD_ID_BONES			= 0x10327200,		///< @deprecated	Obsolete.
	BOARD_ID_BARCLAY		= 0x10355900, 		///< @deprecated	Obsolete.
	BOARD_ID_KIPRO_QUAD		= 0x10393100, 		///< @deprecated	Obsolete.
	BOARD_ID_KIPRO_SPARE1	= 0x10424900, 		///< @deprecated	Obsolete.
	BOARD_ID_KIPRO_SPARE2	= 0x10425000, 		///< @deprecated	Obsolete.
	BOARD_ID_FORGE			= 0x10363700, 		///< @deprecated	Obsolete.
	BOARD_ID_XENA2			= 0x10196500,		///< @deprecated	Obsolete.
	BOARD_ID_KONA3			= BOARD_ID_XENA2,	///< @deprecated	Obsolete.
	BOARD_ID_LHI_DVI		= 0x10266401,		///< @deprecated	Obsolete.
	BOARD_ID_LHI_T			= 0x10266402,		///< @deprecated	Obsolete.
#endif	//	NTV2_DEPRECATE

	DEVICE_ID_CORVID1		= 0x10244800,
	DEVICE_ID_CORVID22		= 0x10293000,
	DEVICE_ID_CORVID24		= 0x10402100,
	DEVICE_ID_CORVID3G		= 0x10294900,
	DEVICE_ID_CORVID44		= 0x10565400,
	DEVICE_ID_CORVID88		= 0x10538200,
	DEVICE_ID_IO4K			= 0x10478300,
	DEVICE_ID_IO4KUFC		= 0x10478350,
	DEVICE_ID_IOEXPRESS		= 0x10280300,
	DEVICE_ID_IOXT			= 0x10378800,
	DEVICE_ID_KONA3G		= 0x10294700,
	DEVICE_ID_KONA3GQUAD	= 0x10322950,
	DEVICE_ID_KONA4			= 0X10518400,
	DEVICE_ID_KONA4UFC		= 0X10518450,
	DEVICE_ID_LHE_PLUS		= 0x10352300,
	DEVICE_ID_LHI			= 0x10266400,
	DEVICE_ID_TTAP			= 0x10416000,
	DEVICE_ID_NOTFOUND		= -1

#if !defined (NTV2_DEPRECATE)
	//	These will probably go away in the SDK 13 epoch...
	,BOARD_ID_CORVID1		= DEVICE_ID_CORVID1,	///< @deprecated	Use DEVICE_ID_CORVID1 instead.
	BOARD_ID_CORVID22		= DEVICE_ID_CORVID22,	///< @deprecated	Use DEVICE_ID_CORVID22 instead.
	BOARD_ID_CORVID24		= DEVICE_ID_CORVID24,	///< @deprecated	Use DEVICE_ID_CORVID24 instead.
	BOARD_ID_CORVID3G		= DEVICE_ID_CORVID3G,	///< @deprecated	Use DEVICE_ID_CORVID3G instead.
	BOARD_ID_CORVID44		= DEVICE_ID_CORVID44,	///< @deprecated	Use DEVICE_ID_CORVID44 instead.
	BOARD_ID_CORVID88		= DEVICE_ID_CORVID88,	///< @deprecated	Use DEVICE_ID_CORVID88 instead.
	BOARD_ID_IO4K			= DEVICE_ID_IO4K,		///< @deprecated	Use DEVICE_ID_IO4K instead.
	BOARD_ID_IO4KUFC		= DEVICE_ID_IO4KUFC,	///< @deprecated	Use DEVICE_ID_IO4KUFC instead.
	BOARD_ID_IOEXPRESS		= DEVICE_ID_IOEXPRESS,	///< @deprecated	Use DEVICE_ID_IOEXPRESS instead.
	BOARD_ID_IOXT			= DEVICE_ID_IOXT,		///< @deprecated	Use DEVICE_ID_IOXT instead.
	BOARD_ID_KONA3G			= DEVICE_ID_KONA3G,		///< @deprecated	Use DEVICE_ID_KONA3G instead.
	BOARD_ID_KONA3GQUAD		= DEVICE_ID_KONA3GQUAD,	///< @deprecated	Use DEVICE_ID_KONA3GQUAD instead.
	BOARD_ID_KONA4			= DEVICE_ID_KONA4,		///< @deprecated	Use DEVICE_ID_KONA4 instead.
	BOARD_ID_KONA4UFC		= DEVICE_ID_KONA4UFC,	///< @deprecated	Use DEVICE_ID_KONA4UFC instead.
	BOARD_ID_LHE_PLUS		= DEVICE_ID_LHE_PLUS,	///< @deprecated	Use DEVICE_ID_LHE_PLUS instead.
	BOARD_ID_LHI			= DEVICE_ID_LHI,		///< @deprecated	Use DEVICE_ID_LHI instead.
	BOARD_ID_TTAP			= DEVICE_ID_TTAP,		///< @deprecated	Use DEVICE_ID_TTAP instead.
	BOARD_ID_NOTFOUND		= DEVICE_ID_NOTFOUND	///< @deprecated	Use DEVICE_ID_NOTFOUND instead.

#endif	//	!defined (NTV2_DEPRECATE)

} NTV2DeviceID;

#if !defined (NTV2_DEPRECATE)
	typedef NTV2DeviceID	NTV2BoardID;	///< @deprecated	Use NTV2DeviceID instead. Identifiers with "board" in them are being phased out.

	/**
		@deprecated		NTV2BoardSubType is obsolete.
	**/
	typedef enum
	{
		BOARDSUBTYPE_AES,		///< @deprecated	Obsolete.
		BOARDSUBTYPE_22,		///< @deprecated	Obsolete.
		BOARDSUBTYPE_NONE		///< @deprecated	Obsolete.
	} NTV2BoardSubType;
#endif	//	!defined (NTV2_DEPRECATE)

typedef enum
{
	NTV2_STANDARD_1080,				// i/psf
	NTV2_STANDARD_720, 
	NTV2_STANDARD_525,				// interlaced
	NTV2_STANDARD_625,				// interlaced
	NTV2_STANDARD_1080p,
	NTV2_STANDARD_2K,				// 2048x1556psf
	NTV2_NUM_STANDARDS,
	NTV2_STANDARD_UNDEFINED = NTV2_NUM_STANDARDS
} NTV2Standard;

#define	IS_VALID_NTV2Standard(__s__)			((__s__) >= NTV2_STANDARD_1080 && (__s__) < NTV2_STANDARD_UNDEFINED)
#define IS_PROGRESSIVE_NTV2Standard(__s__)		((__s__) == NTV2_STANDARD_1080p || (__s__) == NTV2_STANDARD_720)


typedef enum
{
	NTV2_V2_STANDARD_1080,			// interlaced
	NTV2_V2_STANDARD_720, 
	NTV2_V2_STANDARD_525,
	NTV2_V2_STANDARD_625,
	NTV2_V2_STANDARD_1080p,
	NTV2_V2_STANDARD_2K,			// 2048x1556psf
	NTV2_V2_STANDARD_2Kx1080p,
	NTV2_V2_STANDARD_2Kx1080i,		// psf only
	NTV2_V2_STANDARD_3840x2160p,
	NTV2_V2_STANDARD_4096x2160p,
	NTV2_V2_STANDARD_3840HFR,
	NTV2_V2_STANDARD_4096HFR,
	NTV2_V2_NUM_STANDARDS,
	NTV2_V2_STANDARD_UNDEFINED = NTV2_V2_NUM_STANDARDS
} NTV2V2Standard;

#define	IS_VALID_NTV2V2Standard(__s__)			((__s__) >= NTV2_V2_STANDARD_1080 && (__s__) < NTV2_V2_STANDARD_UNDEFINED)


typedef enum
{
	NTV2_FBF_10BIT_YCBCR			= 0, 
	NTV2_FBF_8BIT_YCBCR				= 1, 
	NTV2_FBF_ARGB					= 2, 
	NTV2_FBF_RGBA					= 3,
	NTV2_FBF_10BIT_RGB				= 4,
	NTV2_FBF_8BIT_YCBCR_YUY2		= 5,
	NTV2_FBF_ABGR					= 6,
	NTV2_FBF_LAST_SD_FBF = NTV2_FBF_ABGR,
	NTV2_FBF_10BIT_DPX				= 7,
	NTV2_FBF_10BIT_YCBCR_DPX		= 8,
	NTV2_FBF_8BIT_DVCPRO			= 9,
	NTV2_FBF_8BIT_QREZ				= 10,
	NTV2_FBF_8BIT_HDV				= 11,
	NTV2_FBF_24BIT_RGB				= 12,
	NTV2_FBF_24BIT_BGR				= 13,
	NTV2_FBF_10BIT_YCBCRA			= 14,
	NTV2_FBF_10BIT_DPX_LITTLEENDIAN	= 15,
	NTV2_FBF_48BIT_RGB				= 16,
	NTV2_FBF_PRORES					= 17,
	NTV2_FBF_PRORES_DVCPRO			= 18,
	NTV2_FBF_PRORES_HDV				= 19,
	NTV2_FBF_10BIT_RGB_PACKED		= 20,
	NTV2_FBF_10BIT_ARGB				= 21,
	NTV2_FBF_16BIT_ARGB				= 22,
	NTV2_FBF_UNUSED_23				= 23,
	NTV2_FBF_10BIT_RAW_RGB			= 24,
	NTV2_FBF_10BIT_RAW_YCBCR		= 25,

	///note, if you add more you need to add another  bit in the channel control register.
	///and an entry in ntv2utils.cpp in frameBufferFormats[].
	NTV2_FBF_NUMFRAMEBUFFERFORMATS
} NTV2FrameBufferFormat;

#define	IS_VALID_NTV2FrameBufferFormat(__s__)		((__s__) >= NTV2_FBF_10BIT_YCBCR && (__s__) < NTV2_FBF_NUMFRAMEBUFFERFORMATS)

#define NTV2_IS_FBF_PRORES(__fbf__) 	(		(__fbf__) == NTV2_FBF_PRORES					\
											||	(__fbf__) == NTV2_FBF_PRORES_DVCPRO				\
											||	(__fbf__) == NTV2_FBF_PRORES_HDV				\
										)

#define NTV2_IS_FBF_RGB(__fbf__)		(		(__fbf__) == NTV2_FBF_ARGB						\
											||	(__fbf__) == NTV2_FBF_RGBA						\
											||	(__fbf__) == NTV2_FBF_10BIT_RGB					\
											||	(__fbf__) == NTV2_FBF_ABGR						\
	 										||	(__fbf__) == NTV2_FBF_10BIT_DPX					\
											||	(__fbf__) == NTV2_FBF_24BIT_RGB					\
											||	(__fbf__) == NTV2_FBF_24BIT_BGR					\
											||	(__fbf__) == NTV2_FBF_10BIT_DPX_LITTLEENDIAN	\
											||	(__fbf__) == NTV2_FBF_48BIT_RGB					\
											||	(__fbf__) == NTV2_FBF_10BIT_RGB_PACKED			\
											||	(__fbf__) == NTV2_FBF_10BIT_ARGB				\
											||	(__fbf__) == NTV2_FBF_16BIT_ARGB				\
										)

#define NTV2_IS_FBF_8BIT(__fbf__)		(		(__fbf__) == NTV2_FBF_8BIT_YCBCR				\
											||	(__fbf__) == NTV2_FBF_ARGB						\
											||	(__fbf__) == NTV2_FBF_RGBA						\
											||	(__fbf__) == NTV2_FBF_8BIT_YCBCR_YUY2			\
											||	(__fbf__) == NTV2_FBF_ABGR						\
											||	(__fbf__) == NTV2_FBF_8BIT_DVCPRO				\
											||	(__fbf__) == NTV2_FBF_8BIT_QREZ					\
										)

#define	NTV2_FBF_HAS_ALPHA(__fbf__)		(		(__fbf__) == NTV2_FBF_ARGB						\
											||	(__fbf__) == NTV2_FBF_RGBA						\
											||	(__fbf__) == NTV2_FBF_ABGR						\
											||	(__fbf__) == NTV2_FBF_10BIT_ARGB				\
											||	(__fbf__) == NTV2_FBF_16BIT_ARGB				\
											||	(__fbf__) == NTV2_FBF_10BIT_YCBCRA				\
										)

#define	NTV2_FBF_IS_RAW(__fbf__)		(		(__fbf__) == NTV2_FBF_10BIT_RAW_RGB				\
											||	(__fbf__) == NTV2_FBF_10BIT_RAW_YCBCR			\
										)


typedef enum
{
	NTV2_FG_1920x1080	= 0,
	NTV2_FG_1280x720	= 1, 
	NTV2_FG_720x486		= 2,
	NTV2_FG_720x576		= 3,
	NTV2_FG_1920x1114	= 4,
	NTV2_FG_2048x1114	= 5,
	NTV2_FG_720x508		= 6,
	NTV2_FG_720x598		= 7, 
	NTV2_FG_1920x1112	= 8, 
	NTV2_FG_1280x740	= 9,
	NTV2_FG_2048x1080	= 10,
	NTV2_FG_2048x1556	= 11,
	NTV2_FG_2048x1588 	= 12,
	NTV2_FG_2048x1112 	= 13,
	NTV2_FG_720x514 	= 14, // extra-wide ntsc
	NTV2_FG_720x612 	= 15, // extra-wide pal
	NTV2_FG_4x1920x1080 = 16,
	NTV2_FG_4x2048x1080 = 17,
	NTV2_FG_NUMFRAMEGEOMETRIES
} NTV2FrameGeometry;

#define	IS_VALID_NTV2FrameGeometry(__s__)		((__s__) >= NTV2_FG_1920x1080 && (__s__) < NTV2_FG_NUMFRAMEGEOMETRIES)

#define NTV2_IS_QUAD_FRAME_GEOMETRY(geom) \
	( geom >= NTV2_FG_4x1920x1080 && geom <= NTV2_FG_4x2048x1080 )

#define NTV2_IS_2K_1080_FRAME_GEOMETRY(geom) \
	(	geom == NTV2_FG_2048x1114 || \
		geom == NTV2_FG_2048x1080 || \
		geom == NTV2_FG_2048x1112 )


typedef enum
{
	//	These are tied to the hardware register values
	NTV2_FRAMERATE_UNKNOWN	= 0,
	NTV2_FRAMERATE_6000		= 1,
	NTV2_FRAMERATE_5994		= 2,
	NTV2_FRAMERATE_3000		= 3,
	NTV2_FRAMERATE_2997		= 4,
	NTV2_FRAMERATE_2500		= 5,
	NTV2_FRAMERATE_2400		= 6,
	NTV2_FRAMERATE_2398		= 7,
	NTV2_FRAMERATE_5000		= 8,
	NTV2_FRAMERATE_4800		= 9,
	NTV2_FRAMERATE_4795		= 10,
	NTV2_FRAMERATE_12000	= 11,
	NTV2_FRAMERATE_11988	= 12,
	NTV2_FRAMERATE_1500		= 13,
	NTV2_FRAMERATE_1498		= 14,

	// These were never implemented, and are here so old code will still compile
	NTV2_FRAMERATE_1900		= 15,	// Used to be 09 in previous SDKs
	NTV2_FRAMERATE_1898		= 16, 	// Used to be 10 in previous SDKs
	NTV2_FRAMERATE_1800		= 17,	// Used to be 11 in previous SDKs
	NTV2_FRAMERATE_1798		= 18,	// Used to be 12 in previous SDKs	

	NTV2_NUM_FRAMERATES
} NTV2FrameRate;

typedef enum
{
	NTV2_SG_UNKNOWN	= 0,
	NTV2_SG_525		= 1,
	NTV2_SG_625		= 2,
	NTV2_SG_750		= 3,
	NTV2_SG_1125	= 4,
	NTV2_SG_1250	= 5,
	NTV2_SG_2Kx1080	= 8,
	NTV2_SG_2Kx1556	= 9
} NTV2ScanGeometry;

// IMPORTANT When adding to this enum, don't forget to:
//		Add a corresponding case to GetNTV2FrameGeometryFromVideoFormat in ntv2deviceservices.cpp
//		Add a corresponding case to GetNTV2QuarterSizedVideoFormat in ntv2utils.cpp
//		Add a corresponding case to GetNTV2StandardFromVideoFormat in ntv2utils.cpp
//		Add a corresponding case to GetActiveVideoSize in ntv2utils.cpp
//		Add a corresponding case to GetNTV2FrameRateFromVideoFormat in ntv2utils.cpp
//		Add a corresponding case to GetDisplayWidth in ntv2utils.cpp
//		Add a corresponding case to GetDisplayHeight in ntv2utils.cpp
//		Add a corresponding string to NTV2VideoFormatStrings in ntv2utils.cpp
//		Add a corresponding standard to NTV2VideoFormatStandards in ntv2register.cpp
//		Add a corresponding framegeometry to NTV2VideoFormatFrameGeometrys in ntv2register.cpp
//		Add a corresponding framerate to NTV2VideoFormatFrameRates in ntv2register.cpp
//		Add a corresponding dual link bool to NTV2Smpte372 in ntv2register.cpp
//		Add a corresponding progressive bool to NTV2ProgressivePicture in ntv2register.cpp
//		Add a corresponding timing to NTV2KonaHDTiming in ntv2register.cpp
//		Add a corresponding timing to NTV2KonaLHTiming in ntv2register.cpp
//		Add a corresponding case to SetVPIDData in ntv2vpid.cpp
//		Add a corresponding case to NTV2VideoFromatString in ntv2debug.cpp
//		Add a corresponding case to NTV2BoardGetVideoFormatFromState_Ex in ntv2boardfeatures.cpp
//		Consider adding a new test case to commonapps/hi5_4k_diag/main.cpp
//		Add a corresponding case to AJAVideoFormatNTV2Table in commonclasses/ntv2videoformataja.cpp
//			(If the format is really new, videotypes.h in ajastuff/common may need updating)
//		Add a corresponding entry in the VideoFormatPairs struct in democlasses/ntv2framegrabber.cpp
//		Add a corresponding entry in the VideoFormatPairs struct in democlasses/ntv2burn4kquadrant.cpp
//		Add a corresponding entry in the VideoFormatPairs struct in democlasses/ntv2cudacapture.cpp
//		Update the #defines following this enum

typedef enum
{
	NTV2_FORMAT_UNKNOWN,											// 0
	NTV2_FORMAT_FIRST_HIGH_DEF_FORMAT,								// 1
	NTV2_FORMAT_1080i_5000 = NTV2_FORMAT_FIRST_HIGH_DEF_FORMAT,		// 1
	NTV2_FORMAT_1080psf_2500 = NTV2_FORMAT_1080i_5000,				// 1
	NTV2_FORMAT_1080i_5994,											// 2
	NTV2_FORMAT_1080psf_2997 = NTV2_FORMAT_1080i_5994,				// 2
	NTV2_FORMAT_1080i_6000,											// 3
	NTV2_FORMAT_1080psf_3000 = NTV2_FORMAT_1080i_6000,				// 3
	NTV2_FORMAT_720p_5994,			// 4
	NTV2_FORMAT_720p_6000,			// 5
	NTV2_FORMAT_1080psf_2398,		// 6
	NTV2_FORMAT_1080psf_2400,		// 7
	NTV2_FORMAT_1080p_2997,			// 8
	NTV2_FORMAT_1080p_3000,			// 9
	NTV2_FORMAT_1080p_2500,			// 10
	NTV2_FORMAT_1080p_2398,			// 11
	NTV2_FORMAT_1080p_2400,			// 12
	NTV2_FORMAT_1080p_2K_2398,		// 13
	NTV2_FORMAT_DEPRECATED_525_5994 = NTV2_FORMAT_1080p_2K_2398, // 13 - Backward compatibility for Linux .ntv2 files, do not use
	NTV2_FORMAT_1080p_2K_2400,		// 14
	NTV2_FORMAT_DEPRECATED_625_5000 = NTV2_FORMAT_1080p_2K_2400, // 14 - Backward compatibility for Linux .ntv2 files, do not use
	NTV2_FORMAT_1080psf_2K_2398,	// 15
	NTV2_FORMAT_1080psf_2K_2400,	// 16
	NTV2_FORMAT_720p_5000,			// 17
	NTV2_FORMAT_1080p_5000,			// 18
	NTV2_FORMAT_1080p_5000_B = NTV2_FORMAT_1080p_5000, // 18
	NTV2_FORMAT_1080p_5994,			// 19
	NTV2_FORMAT_1080p_5994_B = NTV2_FORMAT_1080p_5994, // 19
	NTV2_FORMAT_1080p_6000,			// 20
	NTV2_FORMAT_1080p_6000_B = NTV2_FORMAT_1080p_6000, // 20
	NTV2_FORMAT_720p_2398,			// 21
	NTV2_FORMAT_720p_2500,			// 22
	NTV2_FORMAT_1080p_5000_A,		// 23
	NTV2_FORMAT_1080p_5994_A,		// 24
	NTV2_FORMAT_1080p_6000_A,		// 25
	NTV2_FORMAT_1080p_2K_2500,		// 26
	NTV2_FORMAT_1080psf_2K_2500,		// 27
	NTV2_FORMAT_1080psf_2500_2,		// 28 - psf only (non-interlaced), deprecates NTV2_FORMAT_1080psf_2500
	NTV2_FORMAT_1080psf_2997_2,		// 29 - psf only (non-interlaced), deprecates NTV2_FORMAT_1080psf_2997
	NTV2_FORMAT_1080psf_3000_2,		// 30 - psf only (non-interlaced), deprecates NTV2_FORMAT_1080psf_3000
	// Add new HD formats here
	NTV2_FORMAT_END_HIGH_DEF_FORMATS,// 31

	NTV2_FORMAT_FIRST_STANDARD_DEF_FORMAT = 32,
	NTV2_FORMAT_525_5994 = NTV2_FORMAT_FIRST_STANDARD_DEF_FORMAT, // 32
	NTV2_FORMAT_625_5000,			// 33
	NTV2_FORMAT_525_2398,			// 34
	NTV2_FORMAT_525_2400,			// 35
	NTV2_FORMAT_525psf_2997,		// 36
	NTV2_FORMAT_625psf_2500,		// 37
	// Add new SD formats here
	NTV2_FORMAT_END_STANDARD_DEF_FORMATS, // 38

	// 2K Starts Here.
	NTV2_FORMAT_FIRST_2K_DEF_FORMAT	=  64,
	NTV2_FORMAT_2K_1498=NTV2_FORMAT_FIRST_2K_DEF_FORMAT,	// 64
	NTV2_FORMAT_2K_1500,				// 65
	NTV2_FORMAT_2K_2398,				// 66
	NTV2_FORMAT_2K_2400,				// 67
	NTV2_FORMAT_2K_2500,				// 68
	// Add new 2K formats here
	NTV2_FORMAT_END_2K_DEF_FORMATS,	// 69

	// 4K Starts Here.
	NTV2_FORMAT_FIRST_4K_DEF_FORMAT  =  80,
	NTV2_FORMAT_4x1920x1080psf_2398=NTV2_FORMAT_FIRST_4K_DEF_FORMAT, // 80
	NTV2_FORMAT_4x1920x1080psf_2400,	// 81
	NTV2_FORMAT_4x1920x1080psf_2500,	// 82
	NTV2_FORMAT_4x1920x1080p_2398,		// 83
	NTV2_FORMAT_4x1920x1080p_2400,		// 84
	NTV2_FORMAT_4x1920x1080p_2500,		// 85
	NTV2_FORMAT_4x2048x1080psf_2398,	// 86
	NTV2_FORMAT_4x2048x1080psf_2400,	// 87
	NTV2_FORMAT_4x2048x1080psf_2500,	// 88
	NTV2_FORMAT_4x2048x1080p_2398,		// 89
	NTV2_FORMAT_4x2048x1080p_2400,		// 90
	NTV2_FORMAT_4x2048x1080p_2500,		// 91
	NTV2_FORMAT_4x1920x1080p_2997,		// 92
	NTV2_FORMAT_4x1920x1080p_3000,		// 93
	NTV2_FORMAT_4x1920x1080psf_2997,	// 94 NOT SUPPORTED
	NTV2_FORMAT_4x1920x1080psf_3000,	// 95 NOT SUPPORTED
	NTV2_FORMAT_4x2048x1080p_2997,		// 96
	NTV2_FORMAT_4x2048x1080p_3000,		// 97
	NTV2_FORMAT_4x2048x1080psf_2997,	// 98 NOT SUPPORTED
	NTV2_FORMAT_4x2048x1080psf_3000,	// 99 NOT SUPPORTED
	NTV2_FORMAT_4x1920x1080p_5000,		// 100
	NTV2_FORMAT_4x1920x1080p_5994,		// 101
	NTV2_FORMAT_4x1920x1080p_6000,		// 102
	NTV2_FORMAT_4x2048x1080p_5000,		// 103
	NTV2_FORMAT_4x2048x1080p_5994,		// 104
	NTV2_FORMAT_4x2048x1080p_6000,		// 105
	NTV2_FORMAT_4x2048x1080p_4795,		// 106
	NTV2_FORMAT_4x2048x1080p_4800,		// 107
	NTV2_FORMAT_4x2048x1080p_11988,		// 108
	NTV2_FORMAT_4x2048x1080p_12000,		// 109
	// Add new 4K formats here
	NTV2_FORMAT_END_4K_DEF_FORMATS,		// 110

	NTV2_FORMAT_FIRST_HIGH_DEF_FORMAT2 = 110,
	NTV2_FORMAT_1080p_2K_6000=NTV2_FORMAT_FIRST_HIGH_DEF_FORMAT2,
	NTV2_FORMAT_1080p_2K_6000_A = NTV2_FORMAT_1080p_2K_6000,
	NTV2_FORMAT_1080p_2K_5994,			// 111
	NTV2_FORMAT_1080p_2K_5994_A = NTV2_FORMAT_1080p_2K_5994,
	NTV2_FORMAT_1080p_2K_2997,			// 112
	NTV2_FORMAT_1080p_2K_3000,			// 113
	NTV2_FORMAT_1080p_2K_5000,			// 114
	NTV2_FORMAT_1080p_2K_5000_A = NTV2_FORMAT_1080p_2K_5000,
	NTV2_FORMAT_1080p_2K_4795,			// 115
	NTV2_FORMAT_1080p_2K_4795_A = NTV2_FORMAT_1080p_2K_4795,
	NTV2_FORMAT_1080p_2K_4800,			// 116
	NTV2_FORMAT_1080p_2K_4800_A = NTV2_FORMAT_1080p_2K_4800,
	NTV2_FORMAT_END_HIGH_DEF_FORMATS2,	// 117
	NTV2_MAX_NUM_VIDEO_FORMATS = NTV2_FORMAT_END_HIGH_DEF_FORMATS2
} NTV2VideoFormat;


#define NTV2_IS_HD_VIDEO_FORMAT(__format__)							\
	(	(__format__) != NTV2_FORMAT_UNKNOWN	&&						\
		(((__format__) >= NTV2_FORMAT_FIRST_HIGH_DEF_FORMAT	&&		\
		(__format__) < NTV2_FORMAT_END_HIGH_DEF_FORMATS)	||		\
		((__format__) >= NTV2_FORMAT_FIRST_HIGH_DEF_FORMAT2	&&		\
		(__format__) < NTV2_FORMAT_END_HIGH_DEF_FORMATS2 )) )

#define NTV2_IS_SD_VIDEO_FORMAT(__format__)							\
	(	(__format__) != NTV2_FORMAT_UNKNOWN	&&						\
		(__format__) >= NTV2_FORMAT_FIRST_STANDARD_DEF_FORMAT	&&	\
		(__format__) < NTV2_FORMAT_END_STANDARD_DEF_FORMATS )

#define NTV2_IS_2K_VIDEO_FORMAT(__format__)							\
	(	(__format__) == NTV2_FORMAT_2K_1498	||						\
		(__format__) == NTV2_FORMAT_2K_1500	||						\
		(__format__) == NTV2_FORMAT_2K_2398	||						\
		(__format__) == NTV2_FORMAT_2K_2400	||						\
		(__format__) == NTV2_FORMAT_2K_2500	)

#define NTV2_IS_2K_1080_VIDEO_FORMAT(__format__)					\
	(	(__format__) == NTV2_FORMAT_1080p_2K_2398	||				\
		(__format__) == NTV2_FORMAT_1080psf_2K_2398	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_2400	||				\
		(__format__) == NTV2_FORMAT_1080psf_2K_2400	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_2500	||				\
		(__format__) == NTV2_FORMAT_1080psf_2K_2500	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_2997	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_3000	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_4795	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_4800	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_5000	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_5994	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_6000	)

#define NTV2_IS_4K_VIDEO_FORMAT(__format__)							\
	(	(__format__) >= NTV2_FORMAT_FIRST_4K_DEF_FORMAT &&			\
		(__format__) < NTV2_FORMAT_END_4K_DEF_FORMATS	)

#define NTV2_IS_4K_HFR_VIDEO_FORMAT(__format__)						\
    (	(__format__) >= NTV2_FORMAT_4x1920x1080p_5000	&&			\
		(__format__) <= NTV2_FORMAT_4x2048x1080p_6000	)

#define NTV2_IS_QUAD_FRAME_FORMAT(__format__)						\
	(	(__format__) >= NTV2_FORMAT_FIRST_4K_DEF_FORMAT &&			\
		(__format__) < NTV2_FORMAT_END_4K_DEF_FORMATS	)
	
#define NTV2_IS_4K_4096_VIDEO_FORMAT(__format__)					\
	(	(__format__) == NTV2_FORMAT_4x2048x1080p_2398	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080psf_2398	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_2400	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080psf_2400	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_2500	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080psf_2500	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_2997	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080psf_2997	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_3000	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080psf_3000	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_4795	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_4800	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_5000	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_5994	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_6000	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_11988	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_12000	)

#define NTV2_IS_4K_QUADHD_VIDEO_FORMAT(__format__)					\
	(	(__format__) == NTV2_FORMAT_4x1920x1080p_2398	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080psf_2398	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080p_2400	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080psf_2400	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080p_2500	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080psf_2500	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080p_2997	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080psf_2997	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080p_3000	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080psf_3000	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080p_5000	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080p_5994	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080p_6000	)

#define NTV2_IS_372_DUALLINK_FORMAT(__format__)						\
	(	(__format__) == NTV2_FORMAT_1080p_5000_B	||				\
		(__format__) == NTV2_FORMAT_1080p_5994_B	||				\
		(__format__) == NTV2_FORMAT_1080p_6000_B	)
	
#define NTV2_IS_525_FORMAT(__format__)								\
	(	(__format__) == NTV2_FORMAT_525_5994	||					\
		(__format__) == NTV2_FORMAT_525_2398	||					\
		(__format__) == NTV2_FORMAT_525_2400	||					\
		(__format__) == NTV2_FORMAT_525psf_2997	)
	
#define NTV2_IS_625_FORMAT(__format__)								\
	(	(__format__) == NTV2_FORMAT_625_5000	||					\
		(__format__) == NTV2_FORMAT_625psf_2500	)

#define NTV2_IS_INTERMEDIATE_FORMAT(__format__)						\
	(	(__format__) == NTV2_FORMAT_2K_2398 ||						\
		(__format__) == NTV2_FORMAT_2K_2400 ||						\
		(__format__) == NTV2_FORMAT_720p_2398 ||					\
		(__format__) == NTV2_FORMAT_525_2398	)

#define NTV2_IS_3G_FORMAT(__format__)								\
	(	(__format__) == NTV2_FORMAT_1080p_5000_A	||				\
		(__format__) == NTV2_FORMAT_1080p_5000_B	||				\
		(__format__) == NTV2_FORMAT_1080p_5994_A	||				\
		(__format__) == NTV2_FORMAT_1080p_5994_B	||				\
		(__format__) == NTV2_FORMAT_1080p_6000_A	||				\
		(__format__) == NTV2_FORMAT_1080p_6000_B	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_4795	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_4800	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_5000	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_5994	||				\
		(__format__) == NTV2_FORMAT_1080p_2K_6000	||				\
		(__format__) == NTV2_FORMAT_4x1920x1080p_5000	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080p_5994	||			\
		(__format__) == NTV2_FORMAT_4x1920x1080p_6000	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_4795	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_4800	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_5000	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_5994	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_6000	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_11988	||			\
		(__format__) == NTV2_FORMAT_4x2048x1080p_12000	)

#define NTV2_IS_3Gb_FORMAT(__format__)								\
	(	(__format__) == NTV2_FORMAT_1080p_5000_B ||					\
		(__format__) == NTV2_FORMAT_1080p_5994_B ||					\
		(__format__) == NTV2_FORMAT_1080p_6000_B	)

#define NTV2_IS_WIRE_FORMAT(____format____)							\
	(	(____format____) != NTV2_FORMAT_525_2398 &&					\
		(____format____) != NTV2_FORMAT_525_2400 &&					\
		(____format____) != NTV2_FORMAT_720p_2398 &&				\
		(____format____) != NTV2_FORMAT_720p_2500	)

#define NTV2_IS_PSF_VIDEO_FORMAT(__f__)								\
	(	(__f__) == NTV2_FORMAT_1080psf_2398 ||						\
		(__f__) == NTV2_FORMAT_1080psf_2400 ||						\
		(__f__) == NTV2_FORMAT_1080psf_2K_2398 ||					\
		(__f__) == NTV2_FORMAT_1080psf_2K_2400 ||					\
		(__f__) == NTV2_FORMAT_1080psf_2K_2500 ||					\
		(__f__) == NTV2_FORMAT_1080psf_2500_2 ||					\
		(__f__) == NTV2_FORMAT_1080psf_2997_2 ||					\
		(__f__) == NTV2_FORMAT_1080psf_3000_2 ||					\
		(__f__) == NTV2_FORMAT_525psf_2997 ||						\
		(__f__) == NTV2_FORMAT_625psf_2500 ||						\
		(__f__) == NTV2_FORMAT_4x1920x1080psf_2398 ||				\
		(__f__) == NTV2_FORMAT_4x1920x1080psf_2400 ||				\
		(__f__) == NTV2_FORMAT_4x1920x1080psf_2500 ||				\
		(__f__) == NTV2_FORMAT_4x1920x1080psf_2997 ||				\
		(__f__) == NTV2_FORMAT_4x1920x1080psf_3000 ||				\
		(__f__) == NTV2_FORMAT_4x2048x1080psf_2398 ||				\
		(__f__) == NTV2_FORMAT_4x2048x1080psf_2400 ||				\
		(__f__) == NTV2_FORMAT_4x2048x1080psf_2500 ||				\
		(__f__) == NTV2_FORMAT_4x2048x1080psf_2997 ||				\
		(__f__) == NTV2_FORMAT_4x2048x1080psf_3000	)

#define NTV2_VIDEO_FORMAT_HAS_PROGRESSIVE_PICTURE(__f__)			\
	(	(__f__) != NTV2_FORMAT_1080i_5000 &&						\
		(__f__) != NTV2_FORMAT_1080i_5994 &&						\
		(__f__) != NTV2_FORMAT_1080i_6000 &&						\
		(__f__) != NTV2_FORMAT_525_5994 &&							\
		(__f__) != NTV2_FORMAT_625_5000		)

#define NTV2_VIDEO_FORMAT_IS_A(__f__)								\
	(	(__f__) == NTV2_FORMAT_1080p_5000_A ||						\
		(__f__) == NTV2_FORMAT_1080p_5994_A ||						\
		(__f__) == NTV2_FORMAT_1080p_6000_A ||						\
		(__f__) == NTV2_FORMAT_1080p_2K_4795 ||						\
		(__f__) == NTV2_FORMAT_1080p_2K_4800 ||						\
		(__f__) == NTV2_FORMAT_1080p_2K_5000 ||						\
		(__f__) == NTV2_FORMAT_1080p_2K_5994 ||						\
		(__f__) == NTV2_FORMAT_1080p_2K_6000 ||						\
		(__f__) == NTV2_FORMAT_4x1920x1080p_5000 ||					\
		(__f__) == NTV2_FORMAT_4x1920x1080p_5994 ||					\
		(__f__) == NTV2_FORMAT_4x1920x1080p_6000 ||					\
		(__f__) == NTV2_FORMAT_4x2048x1080p_4795 ||					\
		(__f__) == NTV2_FORMAT_4x2048x1080p_4800 ||					\
		(__f__) == NTV2_FORMAT_4x2048x1080p_5000 ||					\
		(__f__) == NTV2_FORMAT_4x2048x1080p_5994 ||					\
		(__f__) == NTV2_FORMAT_4x2048x1080p_6000	)


typedef enum
{
	NTV2_MODE_DISPLAY,
	NTV2_MODE_CAPTURE 
} NTV2Mode;

typedef enum
{
	NTV2_FRAMBUFFERMODE_FRAME,
	NTV2_FRAMBUFFERMODE_FIELD
} NTV2FrameBufferMode;

typedef enum
{
	#if defined (NTV2_DEPRECATE)
		NTV2_INPUTSOURCE_ANALOG,
		NTV2_INPUTSOURCE_HDMI,
		NTV2_INPUTSOURCE_SDI1,
		NTV2_INPUTSOURCE_SDI2,
		NTV2_INPUTSOURCE_SDI3,
		NTV2_INPUTSOURCE_SDI4,
		NTV2_INPUTSOURCE_SDI5,
		NTV2_INPUTSOURCE_SDI6,
		NTV2_INPUTSOURCE_SDI7,
		NTV2_INPUTSOURCE_SDI8,
	#else
		NTV2_INPUTSOURCE_SDI,
		NTV2_INPUTSOURCE_SDI1		= NTV2_INPUTSOURCE_SDI,
		NTV2_INPUTSOURCE_ANALOG,
		NTV2_INPUTSOURCE_SDI2,
		NTV2_INPUTSOURCE_HDMI,
		NTV2_INPUTSOURCE_DUALLINK,
		NTV2_INPUTSOURCE_DUALLINK1	= NTV2_INPUTSOURCE_DUALLINK,
		NTV2_INPUTSOURCE_DUALLINK2,
		NTV2_INPUTSOURCE_SDI3,
		NTV2_INPUTSOURCE_SDI4,
		NTV2_INPUTSOURCE_DUALLINK3,
		NTV2_INPUTSOURCE_DUALLINK4,
		NTV2_INPUTSOURCE_SDI1_DS2,
		NTV2_INPUTSOURCE_SDI2_DS2,
		NTV2_INPUTSOURCE_SDI3_DS2,
		NTV2_INPUTSOURCE_SDI4_DS2,
		NTV2_INPUTSOURCE_SDI5,
		NTV2_INPUTSOURCE_SDI6,
		NTV2_INPUTSOURCE_SDI7,
		NTV2_INPUTSOURCE_SDI8,
		NTV2_INPUTSOURCE_SDI5_DS2,
		NTV2_INPUTSOURCE_SDI6_DS2,
		NTV2_INPUTSOURCE_SDI7_DS2,
		NTV2_INPUTSOURCE_SDI8_DS2,
		NTV2_INPUTSOURCE_DUALLINK5,
		NTV2_INPUTSOURCE_DUALLINK6,
		NTV2_INPUTSOURCE_DUALLINK7,
		NTV2_INPUTSOURCE_DUALLINK8,
	#endif	//	!defined (NTV2_DEPRECATE)
	NTV2_NUM_INPUTSOURCES			//	Always last!
} NTV2InputSource;


#define	NTV2_INPUT_SOURCE_IS_HDMI(_inpSrc_)				((_inpSrc_) == NTV2_INPUTSOURCE_HDMI)
#define	NTV2_INPUT_SOURCE_IS_ANALOG(_inpSrc_)			((_inpSrc_) == NTV2_INPUTSOURCE_ANALOG)
#define	NTV2_INPUT_SOURCE_IS_SDI(_inpSrc_)				(!NTV2_INPUT_SOURCE_IS_HDMI(_inpSrc_) && !NTV2_INPUT_SOURCE_IS_ANALOG(_inpSrc_) && ((_inpSrc_) < NTV2_NUM_INPUTSOURCES))
#define	NTV2_INPUT_SOURCE_IS_VALID(_inpSrc_)			(((_inpSrc_) >= 0) && ((_inpSrc_) < NTV2_NUM_INPUTSOURCES))
#if !defined (NTV2_DEPRECATE)
	#define	NTV2_INPUT_SOURCE_IS_DUALLINK_SDI(_inpSrc_)	(	(_inpSrc_) == NTV2_INPUTSOURCE_DUALLINK1 ||		\
															(_inpSrc_) == NTV2_INPUTSOURCE_DUALLINK2 ||		\
															(_inpSrc_) == NTV2_INPUTSOURCE_DUALLINK3 ||		\
															(_inpSrc_) == NTV2_INPUTSOURCE_DUALLINK4 ||		\
															(_inpSrc_) == NTV2_INPUTSOURCE_DUALLINK5 ||		\
															(_inpSrc_) == NTV2_INPUTSOURCE_DUALLINK6 ||		\
															(_inpSrc_) == NTV2_INPUTSOURCE_DUALLINK7 ||		\
															(_inpSrc_) == NTV2_INPUTSOURCE_DUALLINK8	)
#endif	//	!defined (NTV2_DEPRECATE)

typedef enum
{
	#if defined (NTV2_DEPRECATE)
		NTV2_OUTPUTDESTINATION_ANALOG,
		NTV2_OUTPUTDESTINATION_HDMI,
		NTV2_OUTPUTDESTINATION_SDI1,
		NTV2_OUTPUTDESTINATION_SDI2,
		NTV2_OUTPUTDESTINATION_SDI3,
		NTV2_OUTPUTDESTINATION_SDI4,
		NTV2_OUTPUTDESTINATION_SDI5,
		NTV2_OUTPUTDESTINATION_SDI6,
		NTV2_OUTPUTDESTINATION_SDI7,
		NTV2_OUTPUTDESTINATION_SDI8,
	#else
		NTV2_OUTPUTDESTINATION_SDI1,
		NTV2_OUTPUTDESTINATION_ANALOG,
		NTV2_OUTPUTDESTINATION_SDI2,
		NTV2_OUTPUTDESTINATION_HDMI,
		NTV2_OUTPUTDESTINATION_DUALLINK,
		NTV2_OUTPUTDESTINATION_DUALLINK1	= NTV2_OUTPUTDESTINATION_DUALLINK,
		NTV2_OUTPUTDESTINATION_HDMI_14,
		NTV2_OUTPUTDESTINATION_DUALLINK2,
		NTV2_OUTPUTDESTINATION_SDI3,
		NTV2_OUTPUTDESTINATION_SDI4,
		NTV2_OUTPUTDESTINATION_SDI5,
		NTV2_OUTPUTDESTINATION_SDI6,
		NTV2_OUTPUTDESTINATION_SDI7,
		NTV2_OUTPUTDESTINATION_SDI8,
		NTV2_OUTPUTDESTINATION_DUALLINK3,
		NTV2_OUTPUTDESTINATION_DUALLINK4,
		NTV2_OUTPUTDESTINATION_DUALLINK5,
		NTV2_OUTPUTDESTINATION_DUALLINK6,
		NTV2_OUTPUTDESTINATION_DUALLINK7,
		NTV2_OUTPUTDESTINATION_DUALLINK8,
	#endif	//	!defined (NTV2_DEPRECATE)
	NTV2_NUM_OUTPUTDESTINATIONS			//	Always last!
} NTV2OutputDestination;

#if !defined (NTV2_DEPRECATE)
	#define	NTV2_OUTPUT_DEST_IS_HDMI(_dest_)			((_dest_) == NTV2_OUTPUTDESTINATION_HDMI || (_dest_) == NTV2_OUTPUTDESTINATION_HDMI_14)
	#define	NTV2_OUTPUT_DEST_IS_ANALOG(_dest_)			((_dest_) == NTV2_OUTPUTDESTINATION_ANALOG)
	#define	NTV2_OUTPUT_DEST_IS_SDI(_dest_)				(!NTV2_OUTPUT_DEST_IS_HDMI(_dest_) && !NTV2_OUTPUT_DEST_IS_ANALOG(_dest_) && (_dest_) < NTV2_NUM_OUTPUTDESTINATIONS)
	#define	NTV2_OUTPUT_DEST_IS_DUAL_LINK(_dest_)		(	(_dest_) == NTV2_OUTPUTDESTINATION_DUALLINK1	||	\
															(_dest_) == NTV2_OUTPUTDESTINATION_DUALLINK2	||	\
															(_dest_) == NTV2_OUTPUTDESTINATION_DUALLINK3	||	\
															(_dest_) == NTV2_OUTPUTDESTINATION_DUALLINK4	||	\
															(_dest_) == NTV2_OUTPUTDESTINATION_DUALLINK5	||	\
															(_dest_) == NTV2_OUTPUTDESTINATION_DUALLINK6	||	\
															(_dest_) == NTV2_OUTPUTDESTINATION_DUALLINK7	||	\
															(_dest_) == NTV2_OUTPUTDESTINATION_DUALLINK8	)
#else
	#define	NTV2_OUTPUT_DEST_IS_HDMI(_dest_)			((_dest_) == NTV2_OUTPUTDESTINATION_HDMI)
	#define	NTV2_OUTPUT_DEST_IS_ANALOG(_dest_)			((_dest_) == NTV2_OUTPUTDESTINATION_ANALOG)
	#define	NTV2_OUTPUT_DEST_IS_SDI(_dest_)				((_dest_) >= NTV2_OUTPUTDESTINATION_SDI1 && (_dest_) <= NTV2_OUTPUTDESTINATION_SDI8)
#endif	//	!defined (NTV2_DEPRECATE)
#define	NTV2_OUTPUT_DEST_IS_VALID(_dest_)				(((_dest_) >= 0) && ((_dest_) < NTV2_NUM_OUTPUTDESTINATIONS))


typedef enum
{
	NTV2_CHANNEL1,
	NTV2_CHANNEL2,
	NTV2_CHANNEL3,
	NTV2_CHANNEL4,
	NTV2_CHANNEL5,
	NTV2_CHANNEL6,
	NTV2_CHANNEL7,
	NTV2_CHANNEL8,
	NTV2_MAX_NUM_CHANNELS			//	Always last!
} NTV2Channel;

#define IS_VALID_NTV2Channel(__x__)						((__x__) >= NTV2_CHANNEL1 && (__x__) < NTV2_MAX_NUM_CHANNELS)


typedef enum
{
	NTV2_REFERENCE_EXTERNAL,
	NTV2_REFERENCE_INPUT1,
	NTV2_REFERENCE_INPUT2,
	NTV2_REFERENCE_FREERUN,
	NTV2_REFERENCE_ANALOG_INPUT, 
	NTV2_REFERENCE_HDMI_INPUT,
	NTV2_REFERENCE_INPUT3,
	NTV2_REFERENCE_INPUT4,
	NTV2_REFERENCE_INPUT5,
	NTV2_REFERENCE_INPUT6,
	NTV2_REFERENCE_INPUT7,
	NTV2_REFERENCE_INPUT8,
	NTV2_NUM_REFERENCE_INPUTS			//	Always last!
} NTV2ReferenceSource;

#define IS_VALID_NTV2ReferenceSource(__x__)				((__x__) >= NTV2_REFERENCE_EXTERNAL && (__x__) < NTV2_NUM_REFERENCE_INPUTS)

typedef enum
{
	NTV2_REFVOLTAGE_1 , 
	NTV2_REFVOLTAGE_4 
} NTV2RefVoltage;


typedef enum				// used in FS1
{
	NTV2FS1_RefSelect_None = 0,
	NTV2FS1_RefSelect_RefBNC = 1,
	NTV2FS1_RefSelect_ComponentY = 2,
	NTV2FS1_RefSelect_SVideo = 4,
	NTV2FS1_RefSelect_Composite = 8
} NTV2FS1ReferenceSelect;

typedef enum				// used in FS1
{
	NTV2FS1_FreezeOutput_Disable = 0,
	NTV2FS1_FreezeOutput_Enable  = 1
} NTV2FS1FreezeOutput;

typedef enum				// used in FS1 
{
	NTV2FS1_OUTPUTTONE_DISABLE = 0,
	NTV2FS1_OUTPUTTONE_ENABLE = 1
} NTV2FS1OutputTone;

typedef enum				// used in FS1 
{
	NTV2FS1_AUDIOTONE_400Hz = 0,
	NTV2FS1_AUDIOTONE_1000Hz = 1
} NTV2FS1AudioTone;

typedef enum				// used in FS1 
{
	NTV2FS1_AUDIOLEVEL_24dBu = 0,
	NTV2FS1_AUDIOLEVEL_18dBu = 1,
	NTV2FS1_AUDIOLEVEL_12dBu = 2,
	NTV2FS1_AUDIOLEVEL_15dBu = 3
} NTV2FS1AudioLevel;

typedef enum
{
	NTV2_DISABLEINTERRUPTS			= 0x0,
	NTV2_OUTPUTVERTICAL				= 0x1, 
	NTV2_INPUT1VERTICAL				= 0x2, 
	NTV2_INPUT2VERTICAL				= 0x4,
	NTV2_AUDIOINTERRUPT				= 0x8,
	NTV2_AUDIOOUTWRAPINTERRUPT		= 0x10,
	NTV2_AUDIOINWRAPINTERRUPT		= 0x20,
	NTV2_AUDIOWRAPRATEINTERRUPT		= 0x40,
	NTV2_UART_TX_INTERRUPT			= 0x80,
	NTV2_UART_RX_INTERRUPT			= 0x100,
	NTV2_FS1_I2C_INTERRUPT1			= 0x200,
	NTV2_FS1_I2C_INTERRUPT2			= 0x400,
	NTV2_AUX_VERTICAL_INTERRUPT		= 0x800,
	NTV2_AUX_VERTICAL_INTERRUPT_CLEAR= 0x1000,
	NTV2_FS1_I2C_INTERRUPT2_CLEAR	= 0x2000,
	NTV2_FS1_I2C_INTERRUPT1_CLEAR	= 0x4000,
	NTV2_UART_RX_INTERRUPT_CLEAR	= 0x8000,
	NTV2_AUDIOCHUNKRATE_CLEAR		= 0x10000,
	NTV2_UART_TX_INTERRUPT2			= 0x20000,
	NTV2_OUTPUT2VERTICAL			= 0x40000,
	NTV2_OUTPUT3VERTICAL			= 0x80000,
	NTV2_OUTPUT4VERTICAL			= 0x100000,
	NTV2_OUTPUT4VERTICAL_CLEAR		= 0x200000,
	NTV2_OUTPUT3VERTICAL_CLEAR		= 0x400000,
	NTV2_OUTPUT2VERTICAL_CLEAR		= 0x800000,
	NTV2_UART_TX_INTERRUPT_CLEAR	= 0x1000000,
	NTV2_WRAPRATEINTERRUPT_CLEAR	= 0x2000000,
	NTV2_UART_TX_INTERRUPT2_CLEAR	= 0x4000000,
	NTV2_AUDIOOUTWRAPINTERRUPT_CLEAR= 0x8000000,
	NTV2_AUDIOINTERRUPT_CLEAR		= 0x10000000,
	NTV2_INPUT2VERTICAL_CLEAR		= 0x20000000,
	NTV2_INPUT1VERTICAL_CLEAR		= 0x40000000,
	NTV2_OUTPUTVERTICAL_CLEAR		= 0x80000000
} NTV2InterruptMask;

typedef enum
{
	NTV2_OUTPUTVERTICAL_SHIFT			= 0x0,
	NTV2_INPUT1VERTICAL_SHIFT			= 0x1,
	NTV2_INPUT2VERTICAL_SHIFT			= 0x2,
	NTV2_AUDIOINTERRUPT_SHIFT			= 0x3,
	NTV2_AUDIOOUTWRAPINTERRUPT_SHIFT	= 0x4,
	NTV2_AUDIOINWRAPINTERRUPT_SHIFT		= 0x5,
	NTV2_AUDIOWRAPRATEINTERRUPT_SHIFT	= 0x6,
	NTV2_UART_TX_INTERRUPT_SHIFT		= 0x7,
	NTV2_UART_RX_INTERRUPT_SHIFT		= 0x8,
	NTV2_FS1_I2C_INTERRUPT1_SHIFT		= 0x9,
	NTV2_FS1_I2C_INTERRUPT2_SHIFT		= 0xA,
	NTV2_AUX_VERTICAL_INTERRUPT_SHIFT	= 0xB,
	NTV2_AUX_VERTICAL_INTERRUPT_CLEAR_SHIFT	= 0xC,
	NTV2_FS1_I2C_INTERRUPT2_CLEAR_SHIFT		= 0xD,
	NTV2_FS1_I2C_INTERRUPT1_CLEAR_SHIFT		= 0xE,
	NTV2_UART_RX_INTERRUPT_CLEAR_SHIFT		= 0xF,
	NTV2_AUDIOCHUNKRATE_CLEAR_SHIFT			= 0x10,
	NTV2_UART_TX_INTERRUPT2_SHIFT			= 0x11,
	NTV2_OUTPUT2VERTICAL_SHIFT				= 0x12,
	NTV2_OUTPUT3VERTICAL_SHIFT				= 0x13,
	NTV2_OUTPUT4VERTICAL_SHIFT				= 0x14,
	NTV2_OUTPUT4VERTICAL_CLEAR_SHIFT		= 0x15,
	NTV2_OUTPUT3VERTICAL_CLEAR_SHIFT		= 0x16,
	NTV2_OUTPUT2VERTICAL_CLEAR_SHIFT		= 0x17,
	NTV2_UART_TX_INTERRUPT_CLEAR_SHIFT		= 0x18,
	NTV2_WRAPRATEINTERRUPT_CLEAR_SHIFT		= 0x19,
	NTV2_UART_TX_INTERRUPT2_CLEAR_SHIFT		= 0x1A,
	NTV2_AUDIOOUTWRAPINTERRUPT_CLEAR_SHIFT	= 0x1B,
	NTV2_AUDIOINTERRUPT_CLEAR_SHIFT			= 0x1C,
	NTV2_INPUT2VERTICAL_CLEAR_SHIFT			= 0x1D,
	NTV2_INPUT1VERTICAL_CLEAR_SHIFT			= 0x1E,
	NTV2_OUTPUTVERTICAL_CLEAR_SHIFT			= 0x1F
} NTV2InterruptShift;

typedef enum
{
	NTV2_INPUT3VERTICAL				= 0x2, 
	NTV2_INPUT4VERTICAL				= 0x4,
	NTV2_HDMIRXV2HOTPLUGDETECT		= 0x10,
	NTV2_HDMIRXV2HOTPLUGDETECT_CLEAR= 0x20,
	NTV2_HDMIRXV2AVICHANGE			= 0x40,
	NTV2_HDMIRXV2AVICHANGE_CLEAR	= 0x80,
	NTV2_INPUT5VERTICAL				= 0x100,
	NTV2_INPUT6VERTICAL				= 0x200,
	NTV2_INPUT7VERTICAL				= 0x400,
	NTV2_INPUT8VERTICAL				= 0x800,
	NTV2_OUTPUT5VERTICAL			= 0x1000,
	NTV2_OUTPUT6VERTICAL			= 0x2000,
	NTV2_OUTPUT7VERTICAL			= 0x4000,
	NTV2_OUTPUT8VERTICAL			= 0x8000,
	NTV2_OUTPUT8VERTICAL_CLEAR		= 0x10000,
	NTV2_OUTPUT7VERTICAL_CLEAR		= 0x20000,
	NTV2_OUTPUT6VERTICAL_CLEAR		= 0x40000,
	NTV2_OUTPUT5VERTICAL_CLEAR		= 0x80000,
	NTV2_INPUT8VERTICAL_CLEAR		= 0x2000000,
	NTV2_INPUT7VERTICAL_CLEAR		= 0x4000000,
	NTV2_INPUT6VERTICAL_CLEAR		= 0x8000000,
	NTV2_INPUT5VERTICAL_CLEAR		= 0x10000000,
	NTV2_INPUT4VERTICAL_CLEAR		= 0x20000000,
	NTV2_INPUT3VERTICAL_CLEAR		= 0x40000000
} NTV2Interrupt2Mask;

typedef enum
{
	NTV2_INPUT3VERTICAL_SHIFT			= 0x1, 
	NTV2_INPUT4VERTICAL_SHIFT			= 0x2,
	NTV2_HDMIRXV2HOTPLUGDETECT_SHIFT	= 0x4,
	NTV2_HDMIRXV2HOTPLUGDETECT_CLEAR_SHIFT= 0x5,
	NTV2_HDMIRXV2AVICHANGE_SHIFT		= 0x6,
	NTV2_HDMIRXV2AVICHANGE_CLEAR_SHIFT	= 0x7,
	NTV2_INPUT5VERTICAL_SHIFT			= 0x8,
	NTV2_INPUT6VERTICAL_SHIFT			= 0x9,
	NTV2_INPUT7VERTICAL_SHIFT			= 0xA,
	NTV2_INPUT8VERTICAL_SHIFT			= 0xB,
	NTV2_OUTPUT5VERTICAL_SHIFT			= 0xC,
	NTV2_OUTPUT6VERTICAL_SHIFT			= 0xD,
	NTV2_OUTPUT7VERTICAL_SHIFT			= 0xE,
	NTV2_OUTPUT8VERTICAL_SHIFT			= 0xF,
	NTV2_OUTPUT8VERTICAL_CLEAR_SHIFT	= 0x10,
	NTV2_OUTPUT7VERTICAL_CLEAR_SHIFT	= 0x11,
	NTV2_OUTPUT6VERTICAL_CLEAR_SHIFT	= 0x12,
	NTV2_OUTPUT5VERTICAL_CLEAR_SHIFT	= 0x13,
	NTV2_INPUT8VERTICAL_CLEAR_SHIFT		= 0x19,
	NTV2_INPUT7VERTICAL_CLEAR_SHIFT		= 0x1A,
	NTV2_INPUT6VERTICAL_CLEAR_SHIFT		= 0x1B,
	NTV2_INPUT5VERTICAL_CLEAR_SHIFT		= 0x1C,
	NTV2_INPUT4VERTICAL_CLEAR_SHIFT		= 0x1D,
	NTV2_INPUT3VERTICAL_CLEAR_SHIFT		= 0x1E
} NTV2Interrupt2Shift;

typedef enum
{
	NTV2_DISABLE_DMAINTERRUPTS	= 0,
	NTV2_DMA1_ENABLE	= 1, 
	NTV2_DMA2_ENABLE	= 2, 
	NTV2_DMA3_ENABLE	= 4,
	NTV2_DMA4_ENABLE	= 8,
	NTV2_DMA_BUS_ERROR	= 16
} NTV2DMAInterruptMask;

typedef enum
{
	NTV2_DMA1_CLEAR  = 0x08000000,
	NTV2_DMA2_CLEAR  = 0x10000000,
	NTV2_DMA3_CLEAR  = 0x20000000, 
	NTV2_DMA4_CLEAR  = 0x40000000,
	NTV2_BUSERROR_CLEAR= 0x80000000 
} NTV2DMAStatusBits;

typedef enum
{
	NTV2_REGWRITE_SYNCTOFIELD,
	NTV2_REGWRITE_SYNCTOFRAME,
	NTV2_REGWRITE_IMMEDIATE,
	NTV2_REGWRITE_SYNCTOFIELD_AFTER10LINES
} NTV2RegisterWriteMode;

typedef enum
{
	NTV2_SIGNALMASK_NONE	= 0,		///< Output Black.
	NTV2_SIGNALMASK_Y		= 1,		///< Output Y if set, else Output Y=0x40
	NTV2_SIGNALMASK_Cb		= 2,		///< Output Cb if set, elso Output Cb to 0x200
	NTV2_SIGNALMASK_Cr		= 4,		///< Output Cr if set, elso Output Cr to 0x200
	NTV2_SIGNALMASK_ALL		= 1+2+4		///< Output Cr if set, elso Output Cr to 0x200
} NTV2SignalMask;

/**
	@brief	These are AutoCirculate "channelSpecs" which specify an input (capture/ingest) channel or an output (playout) channel.
**/
typedef enum
{
	/**
		Proposed for 12.1:
			NTV2CROSSPOINT_CHANNEL1		= 256,
			NTV2CROSSPOINT_INPUT1,
			NTV2CROSSPOINT_CHANNEL2,
			NTV2CROSSPOINT_INPUT2,
			NTV2CROSSPOINT_CHANNEL3,
			NTV2CROSSPOINT_INPUT3,
			NTV2CROSSPOINT_CHANNEL4,
			NTV2CROSSPOINT_INPUT4,
			NTV2CROSSPOINT_CHANNEL5,
			NTV2CROSSPOINT_INPUT5,
			NTV2CROSSPOINT_CHANNEL6,
			NTV2CROSSPOINT_INPUT6,
			NTV2CROSSPOINT_CHANNEL7,
			NTV2CROSSPOINT_INPUT7,
			NTV2CROSSPOINT_CHANNEL8,
			NTV2CROSSPOINT_INPUT8,
		. . . add more channels if needed.
		The driver should accept both new and old enum values.
		if (crosspoint < NTV2CROSSPOINT_CHANNEL1)
			use old values, do things the old way
		else
		{
			//	Assume new values, do things the new way:
			Detect if IS_INPUT_CROSSPOINT:		NTV2_IS_INPUT_CROSSPOINT(_x_)	((_x_) & 1)
			Detect if IS_OUTPUT_CROSSPOINT:		NTV2_IS_OUTPUT_CROSSPOINT(_x_)	(((_x_) & 1) == 0)
			Convert to a channel number:  		NTV2CrosspointToChannel(_x_)	static_cast <NTV2Channel> ((NTV2Crosspoint - NTV2CROSSPOINT_CHANNEL1) / 2)
	**/
		NTV2CROSSPOINT_CHANNEL1,
		NTV2CROSSPOINT_CHANNEL2,
		NTV2CROSSPOINT_INPUT1,
		NTV2CROSSPOINT_INPUT2,
		NTV2CROSSPOINT_MATTE,		///< @deprecated	This is obsolete
		NTV2CROSSPOINT_FGKEY,		///< @deprecated	This is obsolete
		NTV2CROSSPOINT_CHANNEL3,
		NTV2CROSSPOINT_CHANNEL4,
		NTV2CROSSPOINT_INPUT3,
		NTV2CROSSPOINT_INPUT4,
		NTV2CROSSPOINT_CHANNEL5,
		NTV2CROSSPOINT_CHANNEL6,
		NTV2CROSSPOINT_CHANNEL7,
		NTV2CROSSPOINT_CHANNEL8,
		NTV2CROSSPOINT_INPUT5,
		NTV2CROSSPOINT_INPUT6,
		NTV2CROSSPOINT_INPUT7,
		NTV2CROSSPOINT_INPUT8,
	NTV2_NUM_CROSSPOINTS
} NTV2Crosspoint;

#define	NTV2_IS_VALID_NTV2Crosspoint(__x__)		((__x__) >= NTV2CROSSPOINT_CHANNEL1 && (__x__) < NTV2_NUM_CROSSPOINTS)

#define	NTV2_IS_INPUT_CROSSPOINT(__x__)			(	(__x__) == NTV2CROSSPOINT_INPUT1 ||							\
													(__x__) == NTV2CROSSPOINT_INPUT2 ||							\
													(__x__) == NTV2CROSSPOINT_INPUT3 ||							\
													(__x__) == NTV2CROSSPOINT_INPUT4 ||							\
													(__x__) == NTV2CROSSPOINT_INPUT5 ||							\
													(__x__) == NTV2CROSSPOINT_INPUT6 ||							\
													(__x__) == NTV2CROSSPOINT_INPUT7 ||							\
													(__x__) == NTV2CROSSPOINT_INPUT8	)

#define	NTV2_IS_OUTPUT_CROSSPOINT(__x__)		(!NTV2_IS_INPUT_CROSSPOINT(__x__))


typedef enum
{
	NTV2VIDPROCMODE_MIX,
	NTV2VIDPROCMODE_SPLIT,
	NTV2VIDPROCMODE_KEY
}NTV2Ch1VidProcMode;

typedef enum
{
	NTV2Ch2OUTPUTMODE_BGV,
	NTV2Ch2OUTPUTMODE_FGV,
	NTV2Ch2OUTPUTMODE_MIXEDKEY
}NTV2Ch2OutputMode;


typedef enum
{
	NTV2SPLITMODE_HORZSPLIT,
	NTV2SPLITMODE_VERTSPLIT,
	NTV2SPLITMODE_HORZSLIT,
	NTV2SPLITMODE_VERTSLIT
}NTV2SplitMode;


// Xena2 Video Processing Control

// one for foreground input, one for background input.
typedef enum
{
	XENA2VIDPROCINPUTCONTROL_FULLRASTER,
	XENA2VIDPROCINPUTCONTROL_SHAPED,
	XENA2VIDPROCINPUTCONTROL_UNSHAPED
}Xena2VidProcInputControl;

typedef enum
{
	XENAVIDPROCMODE_FOREGROUND_ON,	
	XENAVIDPROCMODE_MIX,	
	XENAVIDPROCMODE_SPLIT,	
	XENAVIDPROCMODE_FOREGROUND_OFF
}Xena2VidProcMode;
// End Xena2 additions.

typedef enum
{
	NTV2OUTPUTFILTER_NONE,
	NTV2OUTPUTFILTER_VERTICAL,
	NTV2OUTPUTFILTER_FIELD1,
	NTV2OUTPUTFILTER_FIELD2
} NTV2OutputFilter;

typedef enum
{
	NTV2PROCAMPSTANDARDDEFBRIGHTNESS,			/* SD Luma Offset					*/
	NTV2PROCAMPSTANDARDDEFCONTRAST,				/* SD Luma Gain 					*/
	NTV2PROCAMPSTANDARDDEFSATURATION,			/* SD Cb and Cr Gain				*/
	NTV2PROCAMPSTANDARDDEFHUE,					/* SD Composite and S-Video only 	*/
	NTV2PROCAMPHIGHDEFBRIGHTNESS,				/* HD Luma Offset					*/
	NTV2PROCAMPHIGHDEFCONTRAST,					/* HD Luma Gain 					*/
	NTV2PROCAMPHIGHDEFSATURATION,				/* HD Cb and Cr Gain 				*/
	NTV2PROCAMPHIGHDEFHUE						/* HD Hue, not implemented			*/
} NTV2ProcAmpControl;


#if defined(FS1) || defined(BORG)
// An FS1 or a Borg is the whole system, not a plugin board, so there is only one of it.
#define NTV2_MAXBOARDS 1	
#else
#define NTV2_MAXBOARDS 8
#endif


typedef enum
{
	PROP_SETTINGS,
	PROP_TESTPATTERN,
	PROP_EXPORT,
	PROP_CAPTURE,
	PROP_VIDEOPROC,
	PROP_NONE
} NTV2Prop ;

typedef enum
{
	NTV2_FIELD0, 
	NTV2_FIELD1
} NTV2FieldID;

typedef enum
{
	DMA_READ, 
	DMA_WRITE
} DMADirection;

typedef enum
{
	NTV2_PIO,   // don't change these equates 0-4
	NTV2_DMA1, 
	NTV2_DMA2,
	NTV2_DMA3,
	NTV2_DMA4,
	NTV2_AUTODMA2,
	NTV2_AUTODMA3,
	NTV2_DMA_FIRST_AVAILABLE
} NTV2DMAEngine;

#define NTV2_NUM_DMA_ENGINES (NTV2_DMA4 - NTV2_DMA1 + 1)

typedef enum 
{ 
	QUICKEXPORT_DESKTOP, 
	QUICKEXPORT_WINDOW,
	QUICKEXPORT_CLIPBOARD,
	QUICKEXPORT_FILE
} QuickExportMode;

typedef enum
{
	NTV2CAPTURESOURCE_INPUT1,
	NTV2CAPTURESOURCE_INPUT2,
	NTV2CAPTURESOURCE_INPUT1_PLUS_INPUT2,
	NTV2CAPTURESOURCE_FRAMEBUFFER
} NTV2CaptureSource;

typedef enum
{
	NTV2CAPTUREDESTINATION_CLIPBOARD,
	NTV2CAPTUREDESTINATION_BMPFILE,
	NTV2CAPTUREDESTINATION_JPEGFILE,
	NTV2CAPTUREDESTINATION_YUVFILE,
	NTV2CAPTUREDESTINATION_TIFFFILE,
	NTV2CAPTUREDESTINATION_WINDOW,
	NTV2CAPTUREDESTINATION_PNGFILE,
	NTV2CAPTUREDESTINATION_FRAMEBUFFERONLY,	//	Just leave in FrameBuffer
	NTV2_MAX_NUM_CaptureDestinations
} NTV2CaptureDestination;

typedef enum
{
	NTV2CAPTUREMODE_FIELD,
	NTV2CAPTUREMODE_FRAME,
	NTV2_MAX_NUM_CaptureModes
} NTV2CaptureMode;

typedef enum
{
	NTV2_AUDIO_BUFFER_BIG		= 1,	/* 4 MB 01*/
	#if !defined (NTV2_DEPRECATE)
	NTV2_AUDIO_BUFFER_STANDARD	= 0,	/* 1 MB 00*/
	NTV2_AUDIO_BUFFER_MEDIUM	= 2,	/* 2 MB 10*/
	NTV2_AUDIO_BUFFER_BIGGER	= 3,	/* 8 MB 11*/			// 8 MB capture buffer and 8 MB playback buffer
	#endif	//	!defined (NTV2_DEPRECATE)
	NTV2_MAX_NUM_AudioBufferSizes

} NTV2AudioBufferSize;

typedef enum
{
	NTV2_AUDIO_48K,
    NTV2_AUDIO_96K,
	NTV2_MAX_NUM_AudioRates
} NTV2AudioRate;

typedef enum
{
	NTV2_ENCODED_AUDIO_NORMAL,	// Normal, Sample Rate Converter enabled
	NTV2_ENCODED_AUDIO_SRC_DISABLED // AES ch. 1/2 encoded audio mode (SRC disabled)
} NTV2EncodedAudioMode;


/**
	@brief	This enum value determines/states which video input will be used to supply audio samples to an audio system.
			It assumes that the audio systems' audio source is set to NTV2_AUDIO_EMBEDDED.
			See the SetEmbeddedAudioInput/GetEmbeddedAudioInput methods of CNTV2Card.
**/
typedef enum
{
	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_1,
	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_2,
	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_3,
	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_4,
	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_5,
	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_6,
	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_7,
	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_8,
	NTV2_MAX_NUM_EmbeddedAudioInputs
} NTV2EmbeddedAudioInput;

typedef enum
{
	NTV2_EMBEDDED_AUDIO_CLOCK_REFERENCE,	///< Audio clock derived from the device reference
	NTV2_EMBEDDED_AUDIO_CLOCK_VIDEO_INPUT,	///< Audio clock derived from the video input
	NTV2_MAX_NUM_EmbeddedAudioClocks
} NTV2EmbeddedAudioClock;


/**
	@brief	This enum value determines/states where an audio system will obtain its audio samples.
			See the SetAudioSystemInputSource/GetAudioSystemInputSource methods of CNTV2Card.
**/
typedef enum
{
	NTV2_AUDIO_EMBEDDED,		///< Obtain audio samples from the audio that's embedded in the video HANC
    NTV2_AUDIO_AES,				///< Obtain audio samples from the device AES inputs, if available.
	NTV2_AUDIO_ANALOG,			///< Obtain audio samples from the device analog input(s), if available.
	NTV2_AUDIO_HDMI,			///< Obtain audio samples from the device HDMI input, if available
	NTV2_MAX_NUM_AudioSources
} NTV2AudioSource;

#define	NTV2_AUDIO_SOURCE_IS_EMBEDDED(_x_)		((_x_) == NTV2_AUDIO_EMBEDDED)
#define	NTV2_AUDIO_SOURCE_IS_AES(_x_)			((_x_) == NTV2_AUDIO_AES)
#define	NTV2_AUDIO_SOURCE_IS_ANALOG(_x_)		((_x_) == NTV2_AUDIO_ANALOG)
#define	NTV2_AUDIO_SOURCE_IS_HDMI(_x_)			((_x_) == NTV2_AUDIO_HDMI)


typedef enum
{
	NTV2_AUDIO_LOOPBACK_OFF,
    NTV2_AUDIO_LOOPBACK_ON
} NTV2AudioLoopBack;

typedef enum
{
	NTV2_AUDIOLEVEL_24dBu,
    NTV2_AUDIOLEVEL_18dBu,
    NTV2_AUDIOLEVEL_12dBu,
    NTV2_AUDIOLEVEL_15dBu,
	NTV2_MAX_NUM_AudioLevels
} NTV2AudioLevel;

typedef enum
{
	NTV2_FRAMEBUFFER_ORIENTATION_TOPDOWN,
	NTV2_FRAMEBUFFER_ORIENTATION_BOTTOMUP,
	NTV2_MAX_NUM_VideoFrameBufferOrientations
} NTV2VideoFrameBufferOrientation;

typedef enum
{
	NTV2_CCHOSTACCESS_CH1BANK0,
	NTV2_CCHOSTACCESS_CH1BANK1,
	NTV2_CCHOSTACCESS_CH2BANK0,
	NTV2_CCHOSTACCESS_CH2BANK1,
	NTV2_CCHOSTACCESS_CH3BANK0,
	NTV2_CCHOSTACCESS_CH3BANK1,
	NTV2_CCHOSTACCESS_CH4BANK0,
	NTV2_CCHOSTACCESS_CH4BANK1,
	NTV2_CCHOSTACCESS_CH5BANK0,
	NTV2_CCHOSTACCESS_CH5BANK1,
	NTV2_CCHOSTACCESS_CH6BANK0,
	NTV2_CCHOSTACCESS_CH6BANK1,
	NTV2_CCHOSTACCESS_CH7BANK0,
	NTV2_CCHOSTACCESS_CH7BANK1,
	NTV2_CCHOSTACCESS_CH8BANK0,
	NTV2_CCHOSTACCESS_CH8BANK1,
	NTV2_MAX_NUM_ColorCorrectionHostAccessBanks
}NTV2ColorCorrectionHostAccessBank;

typedef enum
{
	NTV2_CCMODE_OFF,
	NTV2_CCMODE_RGB,
	NTV2_CCMODE_YCbCr,
	NTV2_CCMODE_3WAY,
	NTV2_MAX_NUM_ColorCorrectionModes
} NTV2ColorCorrectionMode;

/////////////////////////////////////////////////////////////////////////////////////
// RP188 (timecode) enum and structs - added for oem driver
typedef enum
{ 
	NTV2_RP188_INPUT,
	NTV2_RP188_OUTPUT,
	NTV2_MAX_NUM_RP188Modes
} NTV2_RP188Mode;   // matches sense of hardware register bit


typedef enum
{
	NTV2_AUDIOPLAYBACK_NOW,
	NTV2_AUDIOPLAYBACK_NEXTFRAME,
	NTV2_AUDIOPLAYBACK_NORMALAUTOCIRCULATE,   // default
	NTV2_AUDIOPLAYBACK_1STAUTOCIRCULATEFRAME  // only works for channelspec = NTV2CROSSPOINT_CHANNEL1
} NTV2_GlobalAudioPlaybackMode;

//////////////////////////////////////////////////////////////////////////////////////
/// Kona2/Xena2 specific enums
typedef enum
{
	NTV2_FRAMESIZE_2MB,
	NTV2_FRAMESIZE_4MB,
	NTV2_FRAMESIZE_8MB,
	NTV2_FRAMESIZE_16MB,
	NTV2_FRAMESIZE_6MB,
	NTV2_FRAMESIZE_10MB,
	NTV2_FRAMESIZE_12MB,
	NTV2_FRAMESIZE_14MB,
	NTV2_FRAMESIZE_18MB,
	NTV2_FRAMESIZE_20MB,
	NTV2_FRAMESIZE_22MB,
	NTV2_FRAMESIZE_24MB,
	NTV2_FRAMESIZE_26MB,
	NTV2_FRAMESIZE_28MB,
	NTV2_FRAMESIZE_30MB,
	NTV2_FRAMESIZE_32MB,
	NTV2_MAX_NUM_Framesizes
} NTV2Framesize;

typedef enum
{
	NTV2_480iRGB,
	NTV2_480iYPbPrSMPTE,
	NTV2_480iYPbPrBetacam525,
	NTV2_480iYPbPrBetacamJapan,
	NTV2_480iNTSC_US_Composite,
	NTV2_480iNTSC_Japan_Composite,
	NTV2_576iRGB,
	NTV2_576iYPbPrSMPTE,
	NTV2_576iPAL_Composite,
	NTV2_1080iRGB,
	NTV2_1080psfRGB,
	NTV2_720pRGB,
	NTV2_1080iSMPTE,
	NTV2_1080psfSMPTE,
	NTV2_720pSMPTE,
	NTV2_1080iXVGA,
	NTV2_1080psfXVGA,
	NTV2_720pXVGA,
	NTV2_2Kx1080RGB,
	NTV2_2Kx1080SMPTE,
	NTV2_2Kx1080XVGA,
	NTV2_END_DACMODES,
	NTV2_MAX_NUM_VideoDACModes
} NTV2VideoDACMode;


typedef enum
{
	NTV2LHI_480iRGB						= 0xC,
	NTV2LHI_480iYPbPrSMPTE				= 0x8,
	NTV2LHI_480iYPbPrBetacam525			= 0x9,	
	NTV2LHI_480iYPbPrBetacamJapan		= 0xA,
	NTV2LHI_480iNTSC_US_Composite		= 0x1,	
	NTV2LHI_480iNTSC_Japan_Composite	= 0x2,
	NTV2LHI_576iRGB						= 0xC,	
	NTV2LHI_576iYPbPrSMPTE				= 0x8,
	NTV2LHI_576iPAL_Composite			= 0x0,	
	NTV2LHI_1080iRGB					= 0xC,
	NTV2LHI_1080psfRGB					= 0xC,
	NTV2LHI_1080iSMPTE					= 0x8,
	NTV2LHI_1080psfSMPTE				= 0x8,
	NTV2LHI_720pRGB						= 0xC,
	NTV2LHI_720pSMPTE					= 0x8,
	NTV2_MAX_NUM_LHIVideoDACModes
} NTV2LHIVideoDACMode;


typedef enum
{
	NTV2_480iADCComponentBeta,
	NTV2_480iADCComponentSMPTE,
	NTV2_480iADCSVideoUS,
	NTV2_480iADCCompositeUS,
	NTV2_480iADCComponentBetaJapan,
	NTV2_480iADCComponentSMPTEJapan,
	NTV2_480iADCSVideoJapan,
	NTV2_480iADCCompositeJapan,
	NTV2_576iADCComponentBeta,
	NTV2_576iADCComponentSMPTE,
	NTV2_576iADCSVideo,
	NTV2_576iADCComposite,
	NTV2_720p_60,	//	60 + 59.94
	NTV2_1080i_30,	//	30 + 29.97
	NTV2_720p_50,
	NTV2_1080i_25,
	NTV2_1080pSF24,	// 24 + 23.98
	NTV2_MAX_NUM_LSVideoADCModes
} NTV2LSVideoADCMode;


typedef enum
{
	NTV2_AnlgComposite,			// Composite or Composite
	NTV2_AnlgComponentSMPTE,	// Component (SMPTE/N10 levels)
	NTV2_AnlgComponentBetacam,	// Component (Betacam levels)
	NTV2_AnlgComponentRGB,		// Component (RGB)
	NTV2_AnlgXVGA,				// xVGA
	NTV2_AnlgSVideo,			// S-Video
	NTV2_MAX_NUM_AnalogTypes
} NTV2AnalogType;


typedef enum 
{
	NTV2_Black75IRE,			//	7.5 IRE (NTSC-US)
	NTV2_Black0IRE,				//	0   IRE (NTSC-J)
	NTV2_MAX_NUM_AnalogBlackLevels
} NTV2AnalogBlackLevel;


typedef enum				// used in Virtual Register: kK2RegInputSelect
{
	NTV2_Input1Select,
	NTV2_Input2Select,
	#if !defined (NTV2_DEPRECATE)
		NTV2_AnalogInputSelect		= NTV2_Input2Select,
	#endif	//	!defined (NTV2_DEPRECATE)
	NTV2_Input3Select,
	NTV2_Input4Select,
	NTV2_Input5Select,
	NTV2_DualLinkInputSelect,
	NTV2_DualLink2xSdi4k,
	NTV2_DualLink4xSdi4k,
	NTV2_MAX_NUM_InputVideoSelectEnums
} NTV2InputVideoSelect;


typedef enum
{
	NTV2_DeviceUnavailable		= -1,
	NTV2_DeviceNotInitialized	= 0,
	NTV2_DeviceInitialized		= 1
} NTV2DeviceInitialized;


typedef enum
{
	NTV2_YUVSelect,
	NTV2_RGBSelect,
	NTV2_Stereo3DSelect,
	NTV2_NUM_SDIInputFormats
} NTV2SDIInputFormatSelect;


typedef enum
{
	NTV2_AES_EBU_XLRSelect,
	NTV2_AES_EBU_BNCSelect,
	NTV2_Input1Embedded1_8Select,
	NTV2_Input1Embedded9_16Select,
	NTV2_Input2Embedded1_8Select,
	NTV2_Input2Embedded9_16Select,
	NTV2_AnalogSelect,
	NTV2_HDMISelect,
	NTV2_AudioInputOther,
	NTV2_MAX_NUM_InputAudioSelectEnums
} NTV2InputAudioSelect;


typedef enum
{
	NTV2_AudioMap12_12,
	NTV2_AudioMap34_12,
	NTV2_AudioMap56_12,
	NTV2_AudioMap78_12,
	NTV2_AudioMap910_12,
	NTV2_AudioMap1112_12,
	NTV2_AudioMap1314_12,
	NTV2_AudioMap1516_12,
	NTV2_MAX_NUM_AudioMapSelectEnums
} NTV2AudioMapSelect;


typedef enum
{
	NTV2_PrimaryOutputSelect,
	NTV2_SecondaryOutputSelect,
	NTV2_DualLinkOutputSelect,
	NTV2_VideoPlusKeySelect,
	NTV2_StereoOutputSelect,
	NTV2_Quadrant1Select,
	NTV2_Quadrant2Select,
	NTV2_Quadrant3Select,
	NTV2_Quadrant4Select,
	NTV2_Quarter4k,
	NTV2_4kHalfFrameRate,
	NTV2_2xSdi4k,
	NTV2_4xSdi4k,
	NTV2_MAX_NUM_OutputVideoSelectEnums
} NTV2OutputVideoSelect;


typedef enum
{
	NTV2_SDITransport_Auto=-1,				// auto mode
	NTV2_SDITransport_Off,					// transport disabled, disconnected
	NTV2_SDITransport_1_5,					// Single Link, 1 wire 1.5G
	NTV2_SDITransport_3Ga,					// Single Link, 1 wire 3Ga
	NTV2_SDITransport_DualLink_1_5,			// Dual Link, 2 wire 1.5G
	NTV2_SDITransport_DualLink_3Gb,			// Dual Link, 1 wire 3Gb
	NTV2_SDITransport_QuadLink_1_5,			// Quad Link, 4 wire 1.5G (4K YUV)
	NTV2_SDITransport_QuadLink_3Gb,			// Quad Link, 2 wire 3Gb (4K YUV or Stereo RGB)
	NTV2_SDITransport_QuadLink_3Ga,			// Quad Link, 4 wire 3Ga (4K HFR)
	NTV2_SDITransport_OctLink_3Gb,			// Oct Link, 4 wire 3Gb (4K RGB, HFR)
	NTV2_MAX_NUM_SDITransportTypes
} NTV2SDITransportType;


#if !defined (NTV2_DEPRECATE)
	// Audio Channel Mapping and Channel Gain/Phase controls used in FS1
	typedef enum                // used in FS1 
	{
		NTV2_AUDIOCHANNELMAPPING_EMB1CH1 = 0,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH2,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH3,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH4,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH5,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH6,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH7,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH8,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH1 = 8,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH2,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH3,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH4,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH5,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH6,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH7,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH8,
		NTV2_AUDIOCHANNELMAPPING_ANALOGCH1 = 16,
		NTV2_AUDIOCHANNELMAPPING_ANALOGCH2,
		NTV2_AUDIOCHANNELMAPPING_ANALOGCH3,
		NTV2_AUDIOCHANNELMAPPING_ANALOGCH4,
		NTV2_AUDIOCHANNELMAPPING_ANALOGCH5,
		NTV2_AUDIOCHANNELMAPPING_ANALOGCH6,
		NTV2_AUDIOCHANNELMAPPING_ANALOGCH7,
		NTV2_AUDIOCHANNELMAPPING_ANALOGCH8,
		NTV2_AUDIOCHANNELMAPPING_AESCH1 = 24,
		NTV2_AUDIOCHANNELMAPPING_AESCH2,
		NTV2_AUDIOCHANNELMAPPING_AESCH3,
		NTV2_AUDIOCHANNELMAPPING_AESCH4,
		NTV2_AUDIOCHANNELMAPPING_AESCH5,
		NTV2_AUDIOCHANNELMAPPING_AESCH6,
		NTV2_AUDIOCHANNELMAPPING_AESCH7,
		NTV2_AUDIOCHANNELMAPPING_AESCH8,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH9 = 32,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH10,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH11,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH12,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH13,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH14,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH15,
		NTV2_AUDIOCHANNELMAPPING_EMB1CH16,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH9 = 40,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH10,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH11,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH12,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH13,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH14,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH15,
		NTV2_AUDIOCHANNELMAPPING_EMB2CH16
	} NTV2AudioChannelMapping;
#endif	//	!defined (NTV2_DEPRECATE)


// Up/Down/Cross Converter modes
typedef enum
{
	NTV2_UpConvertAnamorphic,
	NTV2_UpConvertPillarbox4x3,
	NTV2_UpConvertZoom14x9,
	NTV2_UpConvertZoomLetterbox,
	NTV2_UpConvertZoomWide,
	NTV2_MAX_NUM_UpConvertModes
} NTV2UpConvertMode;


typedef enum
{
	NTV2_DownConvertLetterbox,
	NTV2_DownConvertCrop,
	NTV2_DownConvertAnamorphic,
	NTV2_DownConvert14x9,
	NTV2_MAX_NUM_DownConvertModes
} NTV2DownConvertMode;


typedef enum
{
	NTV2_IsoLetterBox,
	NTV2_IsoHCrop,
	NTV2_IsoPillarBox,
	NTV2_IsoVCrop,
	NTV2_Iso14x9,
	NTV2_IsoPassThrough,
	NTV2_MAX_NUM_IsoConvertModes
} NTV2IsoConvertMode;


#if !defined (NTV2_DEPRECATE)
	typedef enum
	{
		NTV2FS1AFDInsertMode_Off,
		NTV2FS1AFDInsertMode_Delete,
		NTV2FS1AFDInsertMode_Pass,
		NTV2FS1AFDInsertMode_Replace
	} NTV2AFDInsertMode;

	typedef enum
	{
		NTV2FS1AFD_InsertAR_4x3,
		NTV2FS1AFD_InsertAR_16x9
	} NTV2AFDInsertAspectRatio;

	typedef enum
	{
		NTV2AFDInsert_LetterboxTo16x9	= 0x04,
		NTV2AFDInsert_FullFrame			= 0x08,
		NTV2AFDInsert_Pillarbox4x3		= 0x09,
		NTV2AFDInsert_FullFrameProtected = 0x0A,
		NTV2AFDInsert_14x9				= 0x0B,
		NTV2AFDInsert_4x3Alt14x9		= 0x0D,
		NTV2AFDInsert_16x9Alt14x9		= 0x0E,
		NTV2AFDInsert_16x9Alt4x3		= 0x0F
	} NTV2AFDInsertCode;
#endif	//	!defined (NTV2_DEPRECATE)


typedef enum
{
	NTV2_QuarterSizeExpandOff,		//	Normal 1:1 playback
	NTV2_QuarterSizeExpandOn,		//	Hardware will pixel-double and line-double to expand quarter-size frame
	NTV2_MAX_NUM_QuarterSizeExpandModes
} NTV2QuarterSizeExpandMode;


typedef enum
{
	NTV2_StandardQuality		= 0x0,
	NTV2_HighQuality			= 0x1,
	NTV2_ProRes					= NTV2_StandardQuality,
	NTV2_ProResHQ				= NTV2_HighQuality,
	NTV2_ProResLT				= 0x2,
	NTV2_ProResProxy			= 0x4,
	NTV2_MAX_NUM_FrameBufferQuality
} NTV2FrameBufferQuality;


typedef enum
{
	NTV2_NoPSF,		//	Currently only used for ProRes encoder
	NTV2_IsPSF,
	NTV2_INVALID_EncodeAsPSF
} NTV2EncodeAsPSF;


typedef enum
{
	NTV2_PanModeOff,
	NTV2_PanModeReserved,
	NTV2_PanMode2Kx1080,
	NTV2_PanMode1920x1080,
	NTV2_MAX_NUM_PanModes
} NTV2PanMode;


typedef enum
{
	NTV2_XptBlack						= 0x0,
	NTV2_XptSDIIn1						= 0x1,
	NTV2_XptSDIIn1DS2					= 0x1E,
	NTV2_XptSDIIn2						= 0x2,
	NTV2_XptSDIIn2DS2					= 0x1F,
	NTV2_XptLUT1YUV						= 0x4,
	NTV2_XptCSCYUV						= 0x5,
	NTV2_XptCSC1VidYUV					= NTV2_XptCSCYUV,
	NTV2_XptConversionModule			= 0x6,
	NTV2_XptCompressionModule			= 0x7,
	NTV2_XptFrameBuffer1YUV				= 0x8,
	NTV2_XptFrameSync1YUV				= 0x9,
	NTV2_XptFrameSync2YUV				= 0xA,
	NTV2_XptDuallinkOut					= 0xB,
	NTV2_XptDuallinkOutDS2				= 0x26,
	NTV2_XptDuallinkOut2				= 0x1C,
	NTV2_XptDuallinkOut2DS2				= 0x27,
	NTV2_XptDuallinkOut3				= 0x36,
	NTV2_XptDuallinkOut3DS2				= 0x37,
	NTV2_XptDuallinkOut4				= 0x38,
	NTV2_XptDuallinkOut4DS2				= 0x39,
	NTV2_XptAlphaOut					= 0xC,
	NTV2_XptAnalogIn					= 0x16,
	NTV2_XptHDMIIn						= 0x17,
	NTV2_XptHDMIInQ2					= 0x41,
	NTV2_XptHDMIInQ3					= 0x42,
	NTV2_XptHDMIInQ4					= 0x43,
	NTV2_XptHDMIInRGB					= 0x97,
	NTV2_XptHDMIInQ2RGB					= 0xC1,
	NTV2_XptHDMIInQ3RGB					= 0xC2,
	NTV2_XptHDMIInQ4RGB					= 0xC3,
	#if !defined (NTV2_DEPRECATE)
		NTV2_XptFS1SecondConverter		= 0x18,
		NTV2_XptFS1ProcAmp				= 0x19,
		NTV2_XptFS1TestSignalGenerator	= 0x1D,
	#endif	//	!defined (NTV2_DEPRECATE)
	NTV2_XptDuallinkIn					= 0x83,
	NTV2_XptDuallinkIn2					= 0xA8,
	NTV2_XptDuallinkIn3					= 0xB4,
	NTV2_XptDuallinkIn4					= 0xB5,
	NTV2_XptLUT							= 0x84,
	NTV2_XptLUT1RGB						= NTV2_XptLUT,
	NTV2_XptCSCRGB						= 0x85,
	NTV2_XptCSC1VidRGB					= NTV2_XptCSCRGB,
	NTV2_XptFrameBuffer1RGB				= 0x88,
	NTV2_XptFrameSync1RGB				= 0x89,
	NTV2_XptFrameSync2RGB				= 0x8A,
	NTV2_XptLUT2RGB						= 0x8D,
	NTV2_XptCSC1KeyYUV					= 0xE,
	NTV2_XptFrameBuffer2YUV				= 0xF,
	NTV2_XptFrameBuffer2RGB				= 0x8F,
	NTV2_XptCSC2VidYUV					= 0x10,
	NTV2_XptCSC2VidRGB					= 0x90,
	NTV2_XptCSC2KeyYUV					= 0x11,
	NTV2_XptMixerVidYUV					= 0x12,
	NTV2_XptMixerKeyYUV					= 0x13,
	NTV2_XptWaterMarkerRGB				= 0x94,
	NTV2_XptWaterMarkerYUV				= 0x14,
	NTV2_XptWaterMarker2RGB				= 0x9A,
	NTV2_XptWaterMarker2YUV				= 0x1A,
	NTV2_XptIICTRGB						= 0x95,
	NTV2_XptIICT2RGB					= 0x9B,
	NTV2_XptTestPatternYUV				= 0x1D,
	NTV2_XptDCIMixerVidYUV				= 0x22,
	NTV2_XptDCIMixerVidRGB				= 0xA2,
	NTV2_XptMixer2VidYUV				= 0x20,
	NTV2_XptMixer2KeyYUV				= 0x21,
	NTV2_XptStereoCompressorOut			= 0x23,
	NTV2_XptLUT3Out						= 0xA9,
	NTV2_XptLUT4Out						= 0xAA,
	NTV2_XptFrameBuffer3YUV				= 0x24,
	NTV2_XptFrameBuffer3RGB				= 0xA4,
	NTV2_XptFrameBuffer4YUV				= 0x25,
	NTV2_XptFrameBuffer4RGB				= 0xA5,
	NTV2_XptSDIIn3						= 0x30,
	NTV2_XptSDIIn3DS2					= 0x32,
	NTV2_XptSDIIn4						= 0x31,
	NTV2_XptSDIIn4DS2					= 0x33,
 	NTV2_XptCSC3VidYUV					= 0x3A,
 	NTV2_XptCSC3VidRGB					= 0xBA,
 	NTV2_XptCSC3KeyYUV					= 0x3B,
 	NTV2_XptCSC4VidYUV					= 0x3C,
 	NTV2_XptCSC4VidRGB					= 0xBC,
 	NTV2_XptCSC4KeyYUV					= 0x3D,
	NTV2_XptCSC5VidYUV					= 0x2C,
	NTV2_XptCSC5VidRGB					= 0xAC,
	NTV2_XptCSC5KeyYUV					= 0x2D,
	NTV2_XptLUT5Out						= 0xAB,
	NTV2_XptDuallinkOut5				= 0x3E,
	NTV2_XptDuallinkOut5DS2				= 0x3F,
	NTV2_Xpt4KDownConverterOut			= 0x44,
	NTV2_Xpt4KDownConverterOutRGB		= 0xC4,
	NTV2_XptFrameBuffer5YUV				= 0x51,
	NTV2_XptFrameBuffer5RGB				= 0xD1,
	NTV2_XptFrameBuffer6YUV				= 0x52,
	NTV2_XptFrameBuffer6RGB				= 0xD2,
	NTV2_XptFrameBuffer7YUV				= 0x53,
	NTV2_XptFrameBuffer7RGB				= 0xD3,
	NTV2_XptFrameBuffer8YUV				= 0x54,
	NTV2_XptFrameBuffer8RGB				= 0xD4,
	NTV2_XptSDIIn5						= 0x45,
	NTV2_XptSDIIn5DS2					= 0x47,
	NTV2_XptSDIIn6						= 0x46,
	NTV2_XptSDIIn6DS2					= 0x48,
	NTV2_XptSDIIn7						= 0x49,
	NTV2_XptSDIIn7DS2					= 0x4B,
	NTV2_XptSDIIn8						= 0x4A,
	NTV2_XptSDIIn8DS2					= 0x4C,
	NTV2_XptCSC6VidYUV					= 0x59,
	NTV2_XptCSC6VidRGB					= 0xD9,
	NTV2_XptCSC6KeyYUV					= 0x5A,
	NTV2_XptCSC7VidYUV					= 0x5B,
	NTV2_XptCSC7VidRGB					= 0xDB,
	NTV2_XptCSC7KeyYUV					= 0x5C,
	NTV2_XptCSC8VidYUV					= 0x5D,
	NTV2_XptCSC8VidRGB					= 0xDD,
	NTV2_XptCSC8KeyYUV					= 0x5E,
	NTV2_XptLUT6Out						= 0xDF,
	NTV2_XptLUT7Out						= 0xE0,
	NTV2_XptLUT8Out						= 0xE1,
	NTV2_XptDuallinkOut6				= 0x62,
	NTV2_XptDuallinkOut6DS2				= 0x63,
	NTV2_XptDuallinkOut7				= 0x64,
	NTV2_XptDuallinkOut7DS2				= 0x65,
	NTV2_XptDuallinkOut8				= 0x66,
	NTV2_XptDuallinkOut8DS2				= 0x67,
	NTV2_XptMixer3VidYUV				= 0x55,
	NTV2_XptMixer3KeyYUV				= 0x56,
	NTV2_XptMixer4VidYUV				= 0x57,
	NTV2_XptMixer4KeyYUV				= 0x58,
	NTV2_XptDuallinkIn5					= 0xCD,
	NTV2_XptDuallinkIn6					= 0xCE,
	NTV2_XptDuallinkIn7					= 0xCF,
	NTV2_XptDuallinkIn8					= 0xD0,
	NTV2_Xpt425Mux1AYUV					= 0x68,
	NTV2_Xpt425Mux1ARGB					= 0xE8,
	NTV2_Xpt425Mux1BYUV					= 0x69,
	NTV2_Xpt425Mux1BRGB					= 0xE9,
	NTV2_Xpt425Mux2AYUV					= 0x6A,
	NTV2_Xpt425Mux2ARGB					= 0xEA,
	NTV2_Xpt425Mux2BYUV					= 0x6B,
	NTV2_Xpt425Mux2BRGB					= 0xEB,
	NTV2_Xpt425Mux3AYUV					= 0x6C,
	NTV2_Xpt425Mux3ARGB					= 0xEC,
	NTV2_Xpt425Mux3BYUV					= 0x6D,
	NTV2_Xpt425Mux3BRGB					= 0xED,
	NTV2_Xpt425Mux4AYUV					= 0x6E,
	NTV2_Xpt425Mux4ARGB					= 0xEE,
	NTV2_Xpt425Mux4BYUV					= 0x6F,
	NTV2_Xpt425Mux4BRGB					= 0xEF,
	NTV2_XptFrameBuffer1_425YUV			= 0x70,
	NTV2_XptFrameBuffer1_425RGB			= 0xF0,
	NTV2_XptFrameBuffer2_425YUV			= 0x71,
	NTV2_XptFrameBuffer2_425RGB			= 0xF1,
	NTV2_XptFrameBuffer3_425YUV			= 0x72,
	NTV2_XptFrameBuffer3_425RGB			= 0xF2,
	NTV2_XptFrameBuffer4_425YUV			= 0x73,
	NTV2_XptFrameBuffer4_425RGB			= 0xF3,
	NTV2_XptFrameBuffer5_425YUV			= 0x74,
	NTV2_XptFrameBuffer5_425RGB			= 0xF4,
	NTV2_XptFrameBuffer6_425YUV			= 0x75,
	NTV2_XptFrameBuffer6_425RGB			= 0xF5,
	NTV2_XptFrameBuffer7_425YUV			= 0x76,
	NTV2_XptFrameBuffer7_425RGB			= 0xF6,
	NTV2_XptFrameBuffer8_425YUV			= 0x77,
	NTV2_XptFrameBuffer8_425RGB			= 0xF7,
 	NTV2_XptRuntimeCalc					= 0xFF
} NTV2CrosspointID;


typedef enum 
{
	NTV2_WgtFrameBuffer1,
	NTV2_WgtFrameBuffer2,
	NTV2_WgtFrameBuffer3,
	NTV2_WgtFrameBuffer4,
	NTV2_WgtCSC1,
	NTV2_WgtCSC2,
	NTV2_WgtLUT1,
	NTV2_WgtLUT2,
	NTV2_WgtFrameSync1,
	NTV2_WgtFrameSync2,
	NTV2_WgtSDIIn1,
	NTV2_WgtSDIIn2,
	NTV2_Wgt3GSDIIn1,
	NTV2_Wgt3GSDIIn2,
	NTV2_Wgt3GSDIIn3,
	NTV2_Wgt3GSDIIn4,
	NTV2_WgtSDIOut1,
	NTV2_WgtSDIOut2,
	NTV2_WgtSDIOut3,
	NTV2_WgtSDIOut4,
	NTV2_Wgt3GSDIOut1,
	NTV2_Wgt3GSDIOut2,
	NTV2_Wgt3GSDIOut3,
	NTV2_Wgt3GSDIOut4,
	NTV2_WgtDualLinkIn1,
	NTV2_WgtDualLinkV2In1,
	NTV2_WgtDualLinkV2In2,
	NTV2_WgtDualLinkOut1,
	NTV2_WgtDualLinkOut2,
	NTV2_WgtDualLinkV2Out1,
	NTV2_WgtDualLinkV2Out2,
	NTV2_WgtAnalogIn1,
	NTV2_WgtAnalogOut1,
	NTV2_WgtAnalogCompositeOut1,
	NTV2_WgtHDMIIn1,
	NTV2_WgtHDMIOut1,
	NTV2_WgtUpDownConverter1,
	NTV2_WgtUpDownConverter2,
	NTV2_WgtMixer1,
	NTV2_WgtCompression1,
	NTV2_WgtProcAmp1,
	NTV2_WgtWaterMarker1,
	NTV2_WgtWaterMarker2,
	NTV2_WgtIICT1,
	NTV2_WgtIICT2,
	NTV2_WgtTestPattern1,
	NTV2_WgtGenLock,
	NTV2_WgtDCIMixer1,
	NTV2_WgtMixer2,
	NTV2_WgtStereoCompressor,
	NTV2_WgtLUT3,
	NTV2_WgtLUT4,
	NTV2_WgtDualLinkV2In3,
	NTV2_WgtDualLinkV2In4,
	NTV2_WgtDualLinkV2Out3,
	NTV2_WgtDualLinkV2Out4,
	NTV2_WgtCSC3,
	NTV2_WgtCSC4,
	NTV2_WgtHDMIIn1v2,
	NTV2_WgtHDMIOut1v2,
	NTV2_WgtSDIMonOut1,
	NTV2_WgtCSC5,
	NTV2_WgtLUT5,
	NTV2_WgtDualLinkV2Out5,
	NTV2_Wgt4KDownConverter,
	NTV2_Wgt3GSDIIn5,
	NTV2_Wgt3GSDIIn6,
	NTV2_Wgt3GSDIIn7,
	NTV2_Wgt3GSDIIn8,
	NTV2_Wgt3GSDIOut5,
	NTV2_Wgt3GSDIOut6,
	NTV2_Wgt3GSDIOut7,
	NTV2_Wgt3GSDIOut8,
	NTV2_WgtDualLinkV2In5,
	NTV2_WgtDualLinkV2In6,
	NTV2_WgtDualLinkV2In7,
	NTV2_WgtDualLinkV2In8,
	NTV2_WgtDualLinkV2Out6,
	NTV2_WgtDualLinkV2Out7,
	NTV2_WgtDualLinkV2Out8,
	NTV2_WgtCSC6,
	NTV2_WgtCSC7,
	NTV2_WgtCSC8,
	NTV2_WgtLUT6,
	NTV2_WgtLUT7,
	NTV2_WgtLUT8,
	NTV2_WgtMixer3,
	NTV2_WgtMixer4,
	NTV2_WgtFrameBuffer5,
	NTV2_WgtFrameBuffer6,
	NTV2_WgtFrameBuffer7,
	NTV2_WgtFrameBuffer8,
	NTV2_WgtHDMIIn1v3,
	NTV2_WgtHDMIOut1v3,
	NTV2_Wgt425Mux1,
	NTV2_Wgt425Mux2,
	NTV2_Wgt425Mux3,
	NTV2_Wgt425Mux4,
	NTV2_WgtModuleTypeCount,// always last
	NTV2_WgtUndefined = NTV2_WgtModuleTypeCount
} NTV2WidgetID;


#if !defined (NTV2_DEPRECATE)
	typedef enum
	{
		NTV2_FrameSync1Select,
		NTV2_FrameSync2Select,
		NTV2K2_FrameSync1Select		= NTV2_FrameSync1Select,
		NTV2K2_FrameSync2Select		= NTV2_FrameSync2Select,
		NTV2_MAX_NUM_FrameSyncSelect
	} NTV2FrameSyncSelect;		//	FS1 ONLY
#endif	//	!defined (NTV2_DEPRECATE)


typedef enum
{
	NTV2_BreakoutNone,
	NTV2_BreakoutCableXLR,		// AES/EBU audio via XLRs
	NTV2_BreakoutCableBNC,		// AES/EBU audio via BNCs
	NTV2_KBox,					// Kona2
	NTV2_KLBox,					// KonaLS
	NTV2_K3Box,					// Kona3
	NTV2_KLHiBox,				// Kona LHI
	NTV2_KLHePlusBox,			// Kona LHe+
	NTV2_K3GBox,				// Kona3G
	NTV2_MAX_NUM_BreakoutTypes
} NTV2BreakoutType;


typedef enum
{
	NTV2_AudioMonitor1_2,			// Analog Audio Monitor Channels 1-2
	NTV2_AudioMonitor3_4,			// Analog Audio Monitor Channels 3-4
	NTV2_AudioMonitor5_6,			// Analog Audio Monitor Channels 5-6
	NTV2_AudioMonitor7_8,			// Analog Audio Monitor Channels 7-8
	NTV2_AudioMonitor9_10,			// Analog Audio Monitor Channels 9-10
	NTV2_AudioMonitor11_12,			// Analog Audio Monitor Channels 11-12
	NTV2_AudioMonitor13_14,			// Analog Audio Monitor Channels 13-14
	NTV2_AudioMonitor15_16,			// Analog Audio Monitor Channels 15-16
	NTV2_MAX_NUM_AudioMonitorSelect
} NTV2AudioMonitorSelect;


typedef enum
{
	NTV2_AudioChannel1_2,			// Audio Channel Pair Selection 1-2
	NTV2_AudioChannel3_4,			// Audio Channel Pair Selection 3-4
	NTV2_AudioChannel5_6,			// Audio Channel Pair Selection 5-6
	NTV2_AudioChannel7_8,			// Audio Channel Pair Selection 7-8
	NTV2_AudioChannel9_10,			// Audio Channel Pair Selection 9-10
	NTV2_AudioChannel11_12,			// Audio Channel Pair Selection 11-12
	NTV2_AudioChannel13_14,			// Audio Channel Pair Selection 13-14
	NTV2_AudioChannel15_16,			// Audio Channel Pair Selection 15-16
	NTV2_MAX_NUM_AudioChannelPair
} NTV2AudioChannelPair;

#define	NTV2_AUDIO_CHANNEL_PAIR_IS_VALID(__p__)		((__p__) >= NTV2_AudioChannel1_2 && (__p__) < NTV2_MAX_NUM_AudioChannelPair)


typedef enum
{
	NTV2_AudioChannel1_4,			// Audio 4 Channel Selection 1-4
	NTV2_AudioChannel5_8,			// Audio 4 Channel Selection 5-8
	NTV2_AudioChannel9_12,			// Audio 4 Channel Selection 9-12
	NTV2_AudioChannel13_16,			// Audio 4 Channel Selection 13-16
	NTV2_MAX_NUM_Audio4ChannelSelect
} NTV2Audio4ChannelSelect;


typedef enum
{
	NTV2_AudioChannel1_8,			// Audio 8 Channel Quad Select 1-8
	NTV2_AudioChannel9_16,			// Audio 8 Channel Quad Select 9-16
	NTV2_MAX_NUM_Audio8ChannelSelect
} NTV2Audio8ChannelSelect;


typedef enum
{
	NTV2_VideoProcBitFile,			//	Video processor bit file (loaded in cpu/xilinx)
	NTV2_PCIBitFile,				//	PCI flash bit file (loaded in cpu/xilinx)
	NTV2_FWBitFile,					//	FireWire bit file (loaded in cpu/xilinx)
	NTV2_Firmware,					//	Device firmware (loaded in cpu/xilinx)

	NTV2_BitFile1	= 100,			//	bit file 1 - resident in device memory, loaded or not
	NTV2_BitFile2,					//	bit file 2 - resident in device memory, loaded or not
	NTV2_BitFile3,					//	bit file 3 - resident in device memory, loaded or not
	NTV2_BitFile4,					//	bit file 4 - resident in device memory, loaded or not
	NTV2_BitFile5,					//	bit file 5 - resident in device memory, loaded or not
	NTV2_BitFile6,					//	bit file 6 - resident in device memory, loaded or not
	NTV2_BitFile7,					//	bit file 7 - resident in device memory, loaded or not
	NTV2_BitFile8,					//	bit file 8 - resident in device memory, loaded or not
	NTV2_MAX_NUM_BitFileTypes
} NTV2BitFileType;


typedef enum
{
	NTV2_BITFILE_NO_CHANGE		= 0,		// no bitfile change needed
#if !defined (NTV2_DEPRECATE)
	NTV2_BITFILE_XENAHS_SD		= 1,		// XENA_HS: load SD bitfile								//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENAHS_HD		= 2,		// XENA-HS: load HD bitfile								//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_KONA2_DNCVT	= 3,		// KONA2: load downconverter bitfile					//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_KONA2_UPCVT	= 4,		// KONA2: load upconverter bitfile						//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENALH_SD		= 5,		// XENA_LH: load SD bitfile								//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENALH_HD		= 6,		// XENA-LH: load HD bitfile								//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENALS_UART	= 7,		// XENA-LS: with UART Support							//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENALS_CH2		= 8,		// XENA-LS: with Video Processing and Channel 2 Support	//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENA2_DNCVT	= 9,		// XENA2: load downconverter bitfile					//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENA2_UPCVT	= 10,		// XENA2: load upconverter bitfile						//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENA2_XDCVT	= 11,		// XENA2: load cross downconverter (1080->720) bitfile	//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENA2_XUCVT	= 12,		// XENA2: load cross upconverter (720->1080) bitfile	//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENALH_HDQRZ	= 13,		// XENA-LH - HD Qrez bitfile							//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENALH_SDQRZ	= 14,		// XENA-LH - SD Qrez bitfile							//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENAHS2_SD		= 15,		// XENA_HS2: load SD bitfile							//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENAHS2_HD		= 16,		// XENA-HS2: load HD bitfile							//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_MOAB_DNCVT		= 17,		// MOAB: load downconverter bitfile						//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_MOAB_UPCVT		= 18,		// MOAB: load upconverter bitfile						//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_MOAB_XDCVT		= 19,		// MOAB: load cross downconverter (1080->720) bitfile	//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_MOAB_XUCVT		= 20,		// MOAB: load cross upconverter (720->1080) bitfile		//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_XENA2_CSCVT	= 21,		// XENA2: load color space converter bitfile			//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_BORG_CODEC		= 25,		// Borg CoDec bitfile									//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_BORG_UFC		= 26,		// Borg VidProc bitfile									//	DEPRECATION_CANDIDATE
	NTV2_BITFILE_LHI_DVI_MAIN	= 34,		// LHi DVI main bitfile
#endif	//	!defined (NTV2_DEPRECATE)
	NTV2_BITFILE_CORVID1_MAIN	= 22,		// CORVID1 main bitfile
	NTV2_BITFILE_CORVID22_MAIN	= 23,		// Corvid22 main bitfile
	NTV2_BITFILE_KONA3G_MAIN	= 24,		// Kona3G main bitfile
	NTV2_BITFILE_LHI_MAIN		= 27,		// LHi main bitfile
	NTV2_BITFILE_IOEXPRESS_MAIN	= 28,		// IoExpress main bitfile
	NTV2_BITFILE_CORVID3G_MAIN	= 29,		// Corvid3G main bitfile
	NTV2_BITFILE_KONA3G_QUAD	= 30,		// Kona3G Quad bitfile
	NTV2_BITFILE_KONALHE_PLUS	= 31,		// LHePlus bitfile
	NTV2_BITFILE_IOXT_MAIN		= 32,			// IoXT bitfile
	NTV2_BITFILE_CORVID24_MAIN	= 33,		// Corvid24 main bitfile
	NTV2_BITFILE_TTAP_MAIN		= 35,			// T-Tap main bitfile
	NTV2_BITFILE_LHI_T_MAIN		= 36,		// LHi T main bitfile
	NTV2_BITFILE_IO4K_MAIN		= 37,
	NTV2_BITFILE_IO4KUFC_MAIN	= 38,
	NTV2_BITFILE_KONA4_MAIN		= 39,
	NTV2_BITFILE_KONA4UFC_MAIN	= 40,
	NTV2_BITFILE_CORVID88		= 41,
	NTV2_BITFILE_CORVID44		= 42,
	NTV2_BITFILE_NUMBITFILETYPES
} NTV2BitfileType;


#if !defined (NTV2_DEPRECATE)
	typedef enum
	{
		NTV2_BITFILE_K2					// KONA2: PCI firmware
	} NTV2PCIBitFileType;

	typedef enum
	{
		NTV2_BITFILE_MOABFW				// MOAB: firewire xilinx
	} NTV2FWBitFileType;


	typedef enum
	{
		NTV2_BITFILE_MOABPPC			// MOAB: PPC firmware
	} NTV2FirmwareBitFileType;
#endif	//	!defined (NTV2_DEPRECATE)


typedef enum
{
	NTV2_Rec709Matrix,			// Rec 709 is generally used in HD
	NTV2_Rec601Matrix,			// Rec 601 is generally used in SD

	#if !defined (NTV2_DEPRECATE)
		NTV2K2_Rec709Matrix		= NTV2_Rec709Matrix,
		NTV2K2_Rec601Matrix		= NTV2_Rec601Matrix,
	#endif	//	!defined (NTV2_DEPRECATE)
	NTV2_MAX_NUM_ColorSpaceMatrixTypes
} NTV2ColorSpaceMatrixType;


typedef enum
{
	NTV2_DSKModeOff,				//	DSK Off (output is full-screen FrameBuffer Video)
	NTV2_DSKModeFBOverMatte,		//	DSK On: Frame Buffer output over internally-generated color matte
	NTV2_DSKModeFBOverVideoIn,		//	DSK On: Frame Buffer output over Input Video
	NTV2_DSKModeGraphicOverMatte,	//	DSK On: Graphic Buffer over internally-generated color matte
	NTV2_DSKModeGraphicOverVideoIn,	//	DSK On: Graphic Buffer over Input Video
	NTV2_DSKModeGraphicOverFB,		//	DSK On: Graphic Buffer over Frame Buffer output
	NTV2_DSKModeMax
} NTV2DSKMode;


typedef enum
{
	NTV2_DSKForegroundUnshaped,		//	DSK Foreground is not pre-multiplied by alpha
	NTV2_DSKForegroundShaped		//	DSK Background has already been pre-multiplied by alpha
} NTV2DSKForegroundMode;


typedef enum
{
	NTV2_DSKAudioForeground,		//	Take audio from frame buffer ("Playback")
	NTV2_DSKAudioBackground			//	Take audio from input ("E-E")
} NTV2DSKAudioMode;


// note: Pause Mode is a "software" feature - not performed in hardware
typedef enum
{
	NTV2_PauseOnFrame,
	NTV2_PauseOnField
} NTV2PauseModeType;


// note: 24 fps <-> 30 fps Pulldown is a "software" feature - not performed in hardware
typedef enum
{
	NTV2_Pulldown2323,
	NTV2_Pulldown2332,
	NTV2_Pulldown2224
} NTV2PulldownPatternType;


// This is a user-pref control (currently only used on the Mac) that allows the user
// to specify which color-space matrix to use when converting between RGB and YUV
typedef enum
{
	NTV2_ColorSpaceTypeAuto,		// switch between Rec 601 for SD, Rec 709 for HD
	NTV2_ColorSpaceTypeRec601,		// always use Rec 601 matrix
	NTV2_ColorSpaceTypeRec709,		// always use Rec 709 matrix
	NTV2_MAX_NUM_ColorSpaceTypes
} NTV2ColorSpaceType;


/**
	@brief	This is a user-pref control (currently only used on the Mac) that allows the user
			to specify which gamma-correction function to use when converting between RGB and YUV
**/
typedef enum
{
	NTV2_GammaNone,				// don't change LUTs for gamma (aka "Custom")
	NTV2_GammaAuto,				// switch between Rec 601 for SD and Rec 709 for HD
	NTV2_GammaMac,				// 1.8 "Macintosh" Power-function gamma
	NTV2_GammaRec601,			// 2.2 Rec 601 Power-function gamma
	NTV2_GammaRec709,			// 2.22.. Rec 709 gamma
	NTV2_GammaPC,				// 2.5 "PC" Power-function gamma
	NTV2_MAX_NUM_GammaTypes
} NTV2GammaType;


/**
	@brief	This is a user-pref control (currently only used on the Mac) that allows the user
	to specify which RTB range of CSC function to use when converting between RGB and YUV
**/
typedef enum
{
	NTV2_RGBRangeAuto,				// don't change LUTs for gamma (aka "Custom")
	NTV2_RGBRangeFull,				// Levels are 0 - 1023 (Full)
	NTV2_RGBRangeSMPTE,				// Levels are 64 - 940 (SMPTE)
	NTV2_MAX_NUM_RGBRangeModes
} NTV2RGBRangeMode;


/**
	@brief	This is a user-pref control (currently only used on the Mac) that allows the user
			to specify which flavor of Stereo 3D is to be used
**/
typedef enum
{
	NTV2_Stereo3DOff,				// Stereo Mode disabled
	NTV2_Stereo3DSideBySide,		// Muxed Single Stream - Side-By-Side
	NTV2_Stereo3DTopBottom,			// Muxed Single Stream - Top over Bottom
	NTV2_Stereo3DDualStream,		// Two independant streams
	NTV2_MAX_NUM_Stereo3DModes
} NTV2Stereo3DMode;


/**
	@brief	This is a user-pref control (currently only used on the Mac) that allows the user
			to specify which Color Space
**/
typedef enum
{
	NTV2_ColorSpaceModeAuto,			// Auto Select
	NTV2_ColorSpaceModeYCbCr,			// YCbCr
	NTV2_ColorSpaceModeRGB,				// RGB
	NTV2_MAX_NUM_ColorSpaceModes
} NTV2ColorSpaceMode;


// The Mac implementation of color-space conversion uses the two LUT banks for holding
// two separate gamma-conversion LUTs: one for RGB=>YUV and the other for YUV=>RGB. This
// defines which bank is used for which conversion LUT.
// NOTE: using the LUT Banks this way conflicts with using them for AutoCirculate "Color Correction".
#define kLUTBank_RGB2YUV	0		// uses Bank 0 when converting from RGB=>YUV
#define	kLUTBank_YUV2RGB	1		// uses Bank 1 when converting from YUV=>RGB
#define kLUTBank_SMPTE2FULL	0		// uses Bank 0 when converting from SMPTE=>Full range RGB
#define kLUTBank_FULL2SMPTE	1		// uses Bank 0 when converting from Full=>SMPTE range RGB


/**
	@brief	This specifies what function(s) are currently loaded into the LUTs
**/
typedef enum
{
	NTV2_LUTUnknown,				//	Don't know...
	NTV2_LUTCustom,					//	None of the below (on purpose)
	NTV2_LUTLinear,					//	Linear (both banks)
	NTV2_LUTGamma18_Rec601,			//	Translates between Mac 1.8 and Rec 601 (Power Function 2.2), Full Range
	NTV2_LUTGamma18_Rec709,			//	Translates between Mac 1.8 and Rec 709, Full Range
	NTV2_LUTGamma18_Rec601_SMPTE,	//	Same as NTV2_LUTGamma18_Rec601, SMPTE range
	NTV2_LUTGamma18_Rec709_SMPTE,	//	Same as NTV2_LUTGamma18_Rec709, SMPTE range
	NTV2_LUTRGBRangeFull_SMPTE,		//	Translates Full <-> SMPTE range
	NTV2_MAX_NUM_LutTypes
} NTV2LutType;


/**
	@brief	This specifies the HDMI Out Stereo 3D Mode
**/
typedef enum
{
	NTV2_HDMI3DFramePacking	= 0x0,	//	Frame Packing mode
	NTV2_HDMI3DSideBySide	= 0x8,	//	Side By Side Stereo 3D mode
	NTV2_HDMI3DTopBottom	= 0x6,	//	Top Over Bottom Stereo 3D mode
	NTV2_MAX_NUM_HDMIOut3DModes
} NTV2HDMIOut3DMode;


/**
	@brief	This specifies the range of HDMI
**/
typedef enum
{
	NTV2_HDMIRangeSMPTE,		//	Levels are 16 - 235 (SMPTE)
	NTV2_HDMIRangeFull,			//	Levels are  0 - 255 (Full)
	NTV2_MAX_NUM_HDMIRanges
} NTV2HDMIRange;


/**
	@brief	This specifies the number of audio channels on output for HDMI
**/
typedef enum
{
	NTV2_HDMIAudio2Channels,	//	2 Channel output
	NTV2_HDMIAudio8Channels,	//	8 Channel output
	NTV2_MAX_NUM_HDMIAudioChannelEnums
} NTV2HDMIAudioChannels;


/**
	@brief	This specifies HDMI Color Space I/O
**/
typedef enum
{
	NTV2_HDMIColorSpaceAuto,	//	Auto Select
	NTV2_HDMIColorSpaceRGB,		//	RGB
	NTV2_HDMIColorSpaceYCbCr,	//	YCbCr
	NTV2_MAX_NUM_HDMIColorSpaces
} NTV2HDMIColorSpace;


// LHI version HDMI Color Space I/O
typedef enum
{
	NTV2_LHIHDMIColorSpaceYCbCr,	//	YCbCr
	NTV2_LHIHDMIColorSpaceRGB,		//	RGB
	NTV2_MAX_NUM_LHIHDMIColorSpaces
} NTV2LHIHDMIColorSpace;


// This specifies HDMI Protocol
typedef enum
{
	NTV2_HDMIProtocolHDMI,		//	HDMI Protocol
	NTV2_HDMIProtocolDVI,		//	DVI Protocol
	NTV2_MAX_NUM_HDMIProtocols
} NTV2HDMIProtocol;


// Bit depth on HDMI interface
typedef enum
{
	NTV2_HDMI8Bit,				//	8 bit
	NTV2_HDMI10Bit,				//	10 bit
	NTV2_MAX_NUM_HDMIBitDepths
} NTV2HDMIBitDepth;


// This specifies the range of levels for 10-bit RGB (aka DualLink)
typedef enum
{
	NTV2_RGB10RangeFull,		//	Levels are 0 - 1023 (Full)
	NTV2_RGB10RangeSMPTE,		//	Levels are 64 - 940 (SMPTE)
	NTV2_MAX_NUM_RGB10Ranges
} NTV2RGB10Range;


//	This specifies the endian 10-bit RGB (aka DualLink)
typedef enum
{
	NTV2_RGB10LittleEndian,		//	Little Endian
	NTV2_RGB10BigEndian,		//	Big Endian
	NTV2_MAX_NUM_RGB10EndianEnums
} NTV2RGB10Endian;


// This specifies the output selection for the LH board
typedef enum
{
	NTV2LHOutputSelect_VidProc1,	
	NTV2LHOutputSelect_DownConvert,
	NTV2LHOutputSelect_VidProc2,
	NTV2_MAX_NUM_LHOutputSelectEnums
} NTV2LHOutputSelect;	


//	NOTE:	Timecode Burn-In Mode is a "software" feature - not performed in hardware
typedef enum
{
	NTV2_TimecodeBurnInOff,				//	no burn-in
	NTV2_TimecodeBurnInTC,				//	display current timecode
	NTV2_TimecodeBurnInUB,				//	display current user bits
	NTV2_TimecodeBurnInFrameCount,		//	display current frame count
	NTV2_TimecodeBurnInQuickTime,		//	(like frame count, but shows Mac QuickTime frame time)
	NTV2_MAX_NUM_TimecodeBurnInModeTypes
} NTV2TimecodeBurnInModeType;


#if !defined (NTV2_DEPRECATE)
	//	Deprecated .. used only by Xena DXT application, which never shipped
	typedef enum 
	{	DT_XENA_DXT_FIRMWARE_DESIRED_UNINITIALIZED,
		DT_XENA_DXT_FIRMWARE_DESIRED_HD,
		DT_XENA_DXT_FIRMWARE_DESIRED_SD
	} DT_XENA_DXT_FIRMWARE_DESIRED;
#endif	//	!defined (NTV2_DEPRECATE)


typedef enum
{
	NTV2_1080i_5994to525_5994,
	NTV2_1080i_2500to625_2500,
	NTV2_720p_5994to525_5994,
	NTV2_720p_5000to625_2500,
	NTV2_525_5994to1080i_5994,
	NTV2_525_5994to720p_5994,
	NTV2_625_2500to1080i_2500,
	NTV2_625_2500to720p_5000,
	NTV2_720p_5000to1080i_2500,
	NTV2_720p_5994to1080i_5994,
	NTV2_720p_6000to1080i_3000,
	NTV2_1080i2398to525_2398,
	NTV2_1080i2398to525_2997,
	NTV2_1080i2400to525_2400,			//	Not Valid
	NTV2_1080p2398to525_2398,			//	Not Valid
	NTV2_1080p2398to525_2997,			//	Not Valid
	NTV2_1080p2400to525_2400,			//	Not Valid
	NTV2_1080i_2500to720p_5000,
	NTV2_1080i_5994to720p_5994,
	NTV2_1080i_3000to720p_6000,
	NTV2_1080i_2398to720p_2398,
	NTV2_720p_2398to1080i_2398,
	NTV2_525_2398to1080i_2398,
	NTV2_525_5994to525_5994,
	NTV2_625_2500to625_2500,
	NTV2_525_5994to525psf_2997,
	NTV2_625_5000to625psf_2500,
	NTV2_1080i_5000to1080psf_2500,
	NTV2_1080i_5994to1080psf_2997,
	NTV2_1080i_6000to1080psf_3000,
	NTV2_1080p_3000to720p_6000,
	NTV2_1080psf_2398to1080i_5994,		//	rate conversion only (3:2 pulldown)
	NTV2_1080psf_2400to1080i_3000,		//	rate conversion only (3:2 pulldown)
	NTV2_1080psf_2500to1080i_2500,		//	rate conversion only
	NTV2_1080p_2398to1080i_5994,		//	rate conversion only (3:2 pulldown)
	NTV2_1080p_2400to1080i_3000,		//	rate conversion only (3:2 pulldown)
	NTV2_1080p_2500to1080i_2500,		//	rate conversion only
	NTV2_NUM_CONVERSIONMODES,
	NTV2_CONVERSIONMODE_UNKNOWN = NTV2_NUM_CONVERSIONMODES
} NTV2ConversionMode;


typedef enum
{
	NTV2_RGBBLACKRANGE_0_0x3FF,
	NTV2_RGBBLACKRANGE_0x40_0x3C0,
	NTV2_MAX_NUM_RGBBlackRangeEnums
} NTV2RGBBlackRange;


typedef enum
{
	NTV2_VIDEOLIMITING_LEGALSDI,
	NTV2_VIDEOLIMITING_OFF,
	NTV2_VIDEOLIMITING_LEGALBROADCAST,
	NTV2_MAX_NUM_VideoLimitingEnums
} NTV2VideoLimiting;


typedef enum
{
	NTV2_VANCDATA_NORMAL,
	NTV2_VANCDATA_8BITSHIFT_ENABLE,
	NTV2_MAX_NUM_VANCDataShiftModes
} NTV2VANCDataShiftMode;


/////////////////////////////////////////////////////////////////////////////////////
// Driver debug message categories

typedef enum
{
   NTV2_DRIVER_ALL_DEBUG_MESSAGES	= -1,			//	Enable/disable all others
   NTV2_DRIVER_VIDEO_DEBUG_MESSAGES	= 0,			//	Video-specific messages
   NTV2_DRIVER_AUDIO_DEBUG_MESSAGES,				//	Audio-specific messages
   NTV2_DRIVER_AUTOCIRCULATE_DEBUG_MESSAGES,		//	ISR Autocirculate msgs
   NTV2_DRIVER_AUTOCIRCULATE_CONTROL_DEBUG_MESSAGES,//	Autocirculate control msgs
   NTV2_DRIVER_DMA_AUDIO_DEBUG_MESSAGES,			//	Audio Direct Memory Access msgs
   NTV2_DRIVER_DMA_VIDEO_DEBUG_MESSAGES,			//	Video Direct Memory Access msgs
   NTV2_DRIVER_RP188_DEBUG_MESSAGES,				//	For debugging RP188 messages
   NTV2_DRIVER_CUSTOM_ANC_DATA_DEBUG_MESSAGES,		//	For debugging ancillary data
   NTV2_DRIVER_DEBUG_DEBUG_MESSAGES,				//	For debugging debug messages
   NTV2_DRIVER_I2C_DEBUG_MESSAGES,					//	For debugging procamp/I2C writes
   NTV2_DRIVER_NUM_DEBUG_MESSAGE_SETS				//	Must be last!
} NTV2_DriverDebugMessageSet;


typedef enum 
{ 
	eFPGAVideoProc,
	eFPGACodec,
	eFPGA_NUM_FPGAs
} NTV2XilinxFPGA;


#if !defined (NTV2_DEPRECATE)
	typedef enum
	{
		eButtonReleased,
		eButtonPressed,
		eButtonNotSupported
	} NTV2ButtonState;
#endif	//	!defined (NTV2_DEPRECATE)


/////////////////////////////////////////////////////////////////////////////////////
// Stereo Compressor Stuff
typedef enum
{
	NTV2_STEREOCOMPRESSOR_PASS_LEFT,
	NTV2_STEREOCOMPRESSOR_SIDExSIDE,
	NTV2_STEREOCOMPRESSOR_TOP_BOTTOM,
	NTV2_STEREOCOMPRESSOR_PASS_RIGHT,
	NTV2_STEREOCOMPRESSOR_DISABLE = 0xF
} NTV2StereoCompressorOutputMode;


typedef enum
{
	NTV2_STEREOCOMPRESSOR_NO_FLIP		= 0x0,
	NTV2_STEREOCOMPRESSOR_LEFT_HORZ		= 0x1,
	NTV2_STEREOCOMPRESSOR_LEFT_VERT		= 0x2,
	NTV2_STEREOCOMPRESSOR_RIGHT_HORZ	= 0x4,
	NTV2_STEREOCOMPRESSOR_RIGHT_VERT	= 0x8
} NTV2StereoCompressorFlipMode;


typedef enum
{
	NTV2_LUTCONTROL_1_2,
	NTV2_LUTCONTROL_3_4,
	NTV2_LUTCONTROL_5
} NTV2LUTControlSelect;


typedef enum
{
	NTV2_AUDIOSYSTEM_1,
	NTV2_AUDIOSYSTEM_2,
	NTV2_AUDIOSYSTEM_3,
	NTV2_AUDIOSYSTEM_4,
	NTV2_AUDIOSYSTEM_5,
	NTV2_AUDIOSYSTEM_6,
	NTV2_AUDIOSYSTEM_7,
	NTV2_AUDIOSYSTEM_8,
	NTV2_MAX_NUM_AudioSystemEnums,
	NTV2_NUM_AUDIOSYSTEMS	= NTV2_MAX_NUM_AudioSystemEnums
} NTV2AudioSystem;

#define	NTV2_IS_VALID_NTV2AudioSystem(__x__)		((__x__) >= NTV2_AUDIOSYSTEM_1 && (__x__) < NTV2_MAX_NUM_AudioSystemEnums)


typedef enum
{
	NTV2_SDI1AUDIO,
	NTV2_SDI2AUDIO,
	NTV2_SDI3AUDIO,
	NTV2_SDI4AUDIO,
	NTV2_SDI5AUDIO,
	NTV2_SDI6AUDIO,
	NTV2_SDI7AUDIO,
	NTV2_SDI8AUDIO,
	NTV2_MAX_NUM_SDIAudioSelectEnums
	#if !defined (NTV2_DEPRECATE)
		,NTV2_NUM_SDIAUDIO	= NTV2_MAX_NUM_SDIAudioSelectEnums
	#endif	//	!defined (NTV2_DEPRECATE)
} NTV2SDIAudioSelect;


/**
	@brief	This enumerated data type identifies the two possible states of the bypass relays.
**/
typedef enum
{
	NTV2_DEVICE_BYPASSED,		///	Input & output directly connected
	NTV2_THROUGH_DEVICE,		///	Input & output routed through device
	#if !defined (NTV2_DEPRECATE)
		NTV2_BYPASS				= NTV2_DEVICE_BYPASSED,
		NTV2_STRAIGHT_THROUGH	= NTV2_THROUGH_DEVICE,
	#endif	//	NTV2_DEPRECATE
	NTV2_MAX_NUM_RelayStates
} NTV2RelayState;


typedef enum // Time code source
{
	NTV2_TCSOURCE_DEFAULT,
	NTV2_TCSOURCE_SDI1,			/// SDI 1 embedded
	NTV2_TCSOURCE_SDI2,			/// SDI 2 embedded
	NTV2_TCSOURCE_SDI3,			/// SDI 3 embedded
	NTV2_TCSOURCE_SDI4,			/// SDI 4 embedded
	NTV2_TCSOURCE_SDI1_LTC,		/// SDI 1 LTC embedded
	NTV2_TCSOURCE_SDI2_LTC,		/// SDI 2 LTC embedded
	NTV2_TCSOURCE_LTC1,			/// Analog 1 LTC
	NTV2_TCSOURCE_LTC2,			/// Analog 2 LTC
	NTV2_TCSOURCE_SDI5,			/// SDI 5 embedded
	NTV2_TCSOURCE_SDI6,			/// SDI 6 embedded
	NTV2_TCSOURCE_SDI7,			/// SDI 7 embedded
	NTV2_TCSOURCE_SDI8,			/// SDI 8 embedded
	NTV2_TCSOURCE_SDI3_LTC,		/// SDI 3 LTC embedded
	NTV2_TCSOURCE_SDI4_LTC,		/// SDI 4 LTC embedded
	NTV2_TCSOURCE_SDI5_LTC,		/// SDI 5 LTC embedded
	NTV2_TCSOURCE_SDI6_LTC,		/// SDI 6 LTC embedded
	NTV2_TCSOURCE_SDI7_LTC,		/// SDI 7 LTC embedded
	NTV2_TCSOURCE_SDI8_LTC,		/// SDI 8 LTC embedded
	NTV2_NUM_TCSOURCES
} NTV2TCSource;

typedef enum
{
	NTV2_HDMI_V2_HDSD_BIDIRECTIONAL,
	NTV2_HDMI_V2_4K_CAPTURE,
	NTV2_HDMI_V2_4K_PLAYBACK,
	NTV2_HDMI_V2_4K_DISABLE
} NTV2HDMIV2Mode;

typedef enum
{
	VPIDVersion_0					= 0x0,	// deprecated
	VPIDVersion_1					= 0x1	// use this one
} VPIDVersion;

typedef enum
{
	VPIDStandard_Unknown			= 0x00,	// default
	VPIDStandard_483_576			= 0x01,	// ntsc / pal
	VPIDStandard_483_576_DualLink	= 0x02, // no matching ntv2 format
	VPIDStandard_483_576_360Mbs		= 0x02,	// no matching ntv2 format
	VPIDStandard_483_576_540Mbs		= 0x03,	// no matching ntv2 format
	VPIDStandard_720				= 0x04,	// 720 single link
	VPIDStandard_1080				= 0x05,	// 1080 single link
	VPIDStandard_483_576_1485Mbs	= 0x06,	// no matching ntv2 format
	VPIDStandard_1080_DualLink		= 0x07,	// 1080 dual link
	VPIDStandard_720_3Ga			= 0x08,	// no matching ntv2 format
	VPIDStandard_1080_3Ga			= 0x09,	// 1080p 50/5994/60
	VPIDStandard_1080_DualLink_3Gb	= 0x0a,	// 1080 dual link 3Gb
	VPIDStandard_720_3Gb			= 0x0b,	// 2 - 720 links
	VPIDStandard_1080_3Gb			= 0x0c,	// 2 - 1080 links
	VPIDStandard_483_576_3Gb		= 0x0d,	// no matching ntv2 format
	VPIDStandard_1080_QuadLink		= 0x10,	// 1080 quad link
	VPIDStandard_2160_QuadLink_3Gb	= 0x11, // 2160 quad link 3Gb
	VPIDStandard_1080_Dual_3Ga		= 0x14, // 1080 dual link 3Ga
	VPIDStandard_1080_Dual_3Gb		= 0x15,	// 1080 dual link 3Gb
	VPIDStandard_2160_DualLink		= 0x16, // 2160 dual link
	VPIDStandard_2160_QuadLink_3Ga	= 0x17, // 2160 quad link 3Ga
	VPIDStandard_2160_QuadDualLink_3Gb	= 0x18, // 2160 quad link 3Gb
	VPIDStandard_1080_OctLink		= 0x20	// 1080 oct link
} VPIDStandard;

typedef enum
{
	VPIDPictureRate_None			= 0x0,
	VPIDPictureRate_Reserved1		= 0x1,
	VPIDPictureRate_2398			= 0x2,
	VPIDPictureRate_2400			= 0x3,
	VPIDPictureRate_4795			= 0x4,
	VPIDPictureRate_2500			= 0x5,
	VPIDPictureRate_2997			= 0x6,
	VPIDPictureRate_3000			= 0x7,
	VPIDPictureRate_4800			= 0x8,
	VPIDPictureRate_5000			= 0x9,
	VPIDPictureRate_5994			= 0xa,
	VPIDPictureRate_6000			= 0xb,
	VPIDPictureRate_ReservedC		= 0xc,
	VPIDPictureRate_ReservedD		= 0xd,
	VPIDPictureRate_ReservedE		= 0xe,
	VPIDPictureRate_ReservedF		= 0xf
} VPIDPictureRate;

typedef enum
{
	VPIDSampling_YUV_422			= 0x0,
	VPIDSampling_YUV_444			= 0x1,
	VPIDSampling_GBR_444			= 0x2,
	VPIDSampling_YUV_420			= 0x3,
	VPIDSampling_YUVA_4224			= 0x4,
	VPIDSampling_YUVA_4444			= 0x5,
	VPIDSampling_GBRA_4444			= 0x6,
	VPIDSampling_Reserved7			= 0x7,
	VPIDSampling_YUVD_4224			= 0x8,
	VPIDSampling_YUVD_4444			= 0x9,
	VPIDSampling_GBRD_4444			= 0xa,
	VPIDSampling_ReservedB			= 0xb,
	VPIDSampling_ReservedC			= 0xc,
	VPIDSampling_ReservedD			= 0xd,
	VPIDSampling_ReservedE			= 0xe,
	VPIDSampling_XYZ_444			= 0xf
} VPIDSampling;

typedef enum
{
	VPIDChannel_1				= 0x0,
	VPIDChannel_2				= 0x1,
	VPIDChannel_3				= 0x2,
	VPIDChannel_4				= 0x3,
	VPIDChannel_5				= 0x4,
	VPIDChannel_6				= 0x5,
	VPIDChannel_7				= 0x6,
	VPIDChannel_8				= 0x7
} VPIDChannel;

typedef enum
{
	VPIDDynamicRange_100		= 0x0,
	VPIDDynamicRange_200		= 0x1,
	VPIDDynamicRange_400		= 0x2,
	VPIDDynamicRange_Reserved3	= 0x3
} VPIDDynamicRange;

typedef enum
{
	VPIDBitDepth_8				= 0x0,
	VPIDBitDepth_10				= 0x1,
	VPIDBitDepth_12				= 0x2,
	VPIDBitDepth_Reserved3		= 0x3
} VPIDBitDepth;

typedef enum
{
	VPIDLink_1					= 0x0,
	VPIDLink_2					= 0x1,
	VPIDLink_3					= 0x2,
	VPIDLink_4					= 0x3,
	VPIDLink_5					= 0x4,
	VPIDLink_6					= 0x5,
	VPIDLink_7					= 0x6,
	VPIDLink_8					= 0x7
} VPIDLink;

typedef enum
{
	NTV2_RS422_NO_PARITY		= 0x0,
	NTV2_RS422_ODD_PARITY		= 0x1,
	NTV2_RS422_EVEN_PARITY		= 0x2
} NTV2_RS422_PARITY;

typedef enum
{
	NTV2_RS422_BAUD_RATE_38400	= 38400,
	NTV2_RS422_BAUD_RATE_19200	= 19200,
	NTV2_RS422_BAUD_RATE_9600	=  9600
} NTV2_RS422_BAUD_RATE;

typedef enum
{
	NTV2_HDMI_422 = 0,
	NTV2_HDMI_420 = 2
} NTV2HDMISampleStructure;

#if !defined (NTV2_DEPRECATE)
	typedef		NTV2AnalogBlackLevel				NTV2K2AnalogBlackLevel;				///< @deprecated	Use NTV2AnalogBlackLevel instead.
	typedef		NTV2AnalogType						NTV2K2AnalogType;					///< @deprecated	Use NTV2AnalogType instead.
	typedef		NTV2AudioChannelPair				NTV2Audio2ChannelSelect;			///< @deprecated	Use NTV2Audio2ChannelPair instead.
	typedef		NTV2Audio2ChannelSelect				NTV2K2Audio2ChannelSelect;			///< @deprecated	Use NTV2Audio2ChannelSelect instead.
	typedef		NTV2Audio4ChannelSelect				NTV2K2Audio4ChannelSelect;			///< @deprecated	Use NTV2Audio4ChannelSelect instead.
	typedef		NTV2Audio8ChannelSelect				NTV2K2Audio8ChannelSelect;			///< @deprecated	Use NTV2Audio8ChannelSelect instead.
	typedef		NTV2AudioMapSelect					NTV2K2AudioMapSelect;				///< @deprecated	Use NTV2AudioMapSelect instead.
	typedef		NTV2AudioMonitorSelect				NTV2K2AudioMonitorSelect;			///< @deprecated	Use NTV2AudioMonitorSelect instead.
	typedef		NTV2BitFileType						NTV2K2BitFileType;					///< @deprecated	Use NTV2BitFileType instead.
	typedef		NTV2BreakoutType					NTV2K2BreakoutType;					///< @deprecated	Use NTV2BreakoutType instead.
	typedef		NTV2ColorSpaceMatrixType			NTV2K2ColorSpaceMatrixType;			///< @deprecated	Use NTV2ColorSpaceMatrixType instead.
	typedef		NTV2ColorSpaceMode					NTV2K2ColorSpaceMode;				///< @deprecated	Use NTV2ColorSpaceMode instead.
	typedef		NTV2ConversionMode					NTV2K2ConversionMode;				///< @deprecated	Use NTV2ConversionMode instead.
	typedef		NTV2CrosspointID					NTV2CrosspointSelections;			///< @deprecated	Use NTV2CrosspointID instead.
	typedef		NTV2CrosspointSelections			NTV2K2CrosspointSelections;			///< @deprecated	Use NTV2CrosspointID instead.
	typedef		NTV2DownConvertMode					NTV2K2DownConvertMode;				///< @deprecated	Use NTV2DownConvertMode instead.
	typedef		NTV2EncodeAsPSF						NTV2K2EncodeAsPSF;					///< @deprecated	Use NTV2EncodeAsPSF instead.
	typedef		NTV2FrameBufferQuality				NTV2K2FrameBufferQuality;			///< @deprecated	Use NTV2FrameBufferQuality instead.
	typedef		NTV2Framesize						NTV2K2Framesize;					///< @deprecated	Use NTV2Framesize instead.
	typedef		NTV2FrameSyncSelect					NTV2K2FrameSyncSelect;				///< @deprecated	Use NTV2FrameSyncSelect instead.
	typedef		NTV2InputAudioSelect				NTV2K2InputAudioSelect;				///< @deprecated	Use NTV2InputAudioSelect instead.
	typedef		NTV2InputVideoSelect				NTV2K2InputVideoSelect;				///< @deprecated	Use NTV2InputVideoSelect instead.
	typedef		NTV2IsoConvertMode					NTV2K2IsoConvertMode;				///< @deprecated	Use NTV2IsoConvertMode instead.
	typedef		NTV2OutputVideoSelect				NTV2K2OutputVideoSelect;			///< @deprecated	Use NTV2OutputVideoSelect instead.
	typedef		NTV2QuarterSizeExpandMode			NTV2K2QuarterSizeExpandMode;		///< @deprecated	Use NTV2QuarterSizeExpandMode instead.
	typedef		NTV2RegisterWriteMode				NTV2RegWriteMode;					///< @deprecated	Use NTV2RegisterWriteMode instead.
	typedef		NTV2RGBBlackRange					NTV2K2RGBBlackRange;				///< @deprecated	Use NTV2RGBBlackRange instead.
	typedef		NTV2SDIInputFormatSelect			NTV2K2SDIInputFormatSelect;			///< @deprecated	Use NTV2SDIInputFormatSelect instead.
	typedef		NTV2Stereo3DMode					NTV2K2Stereo3DMode;					///< @deprecated	Use NTV2Stereo3DMode instead.
	typedef		NTV2UpConvertMode					NTV2K2UpConvertMode;				///< @deprecated	Use NTV2UpConvertMode instead.
	typedef		NTV2VideoDACMode					NTV2K2VideoDACMode;					///< @deprecated	Use NTV2VideoDACMode instead.

	//	NTV2CrosspointSelections
	#define		NTV2K2_XptBlack						NTV2_XptBlack						///< @deprecated	Use NTV2_XptBlack instead.
	#define		NTV2K2_XptSDIIn1					NTV2_XptSDIIn1						///< @deprecated	Use NTV2_XptSDIIn1 instead.
	#define		NTV2K2_XptSDIIn1DS2					NTV2_XptSDIIn1DS2					///< @deprecated	Use NTV2_XptSDIIn1DS2 instead.
	#define		NTV2K2_XptSDIIn2					NTV2_XptSDIIn2						///< @deprecated	Use NTV2_XptSDIIn2 instead.
	#define		NTV2K2_XptSDIIn2DS2					NTV2_XptSDIIn2DS2					///< @deprecated	Use NTV2_XptSDIIn2DS2 instead.
	#define		NTV2K2_XptLUT1YUV					NTV2_XptLUT1YUV						///< @deprecated	Use NTV2_XptLUT1YUV instead.
	#define		NTV2K2_XptCSCYUV					NTV2_XptCSCYUV						///< @deprecated	Use NTV2_XptCSCYUV instead.
	#define		NTV2K2_XptCSC1VidYUV				NTV2_XptCSC1VidYUV					///< @deprecated	Use NTV2_XptCSC1VidYUV instead.
	#define		NTV2K2_XptConversionModule			NTV2_XptConversionModule			///< @deprecated	Use NTV2_XptConversionModule instead.
	#define		NTV2K2_XptCompressionModule			NTV2_XptCompressionModule			///< @deprecated	Use NTV2_XptCompressionModule instead.
	#define		NTV2K2_XptFrameBuffer1YUV			NTV2_XptFrameBuffer1YUV				///< @deprecated	Use NTV2_XptFrameBuffer1YUV instead.
	#define		NTV2K2_XptFrameSync1YUV				NTV2_XptFrameSync1YUV				///< @deprecated	Use NTV2_XptFrameSync1YUV instead.
	#define		NTV2K2_XptFrameSync2YUV				NTV2_XptFrameSync2YUV				///< @deprecated	Use NTV2_XptFrameSync2YUV instead.
	#define		NTV2K2_XptDuallinkOut				NTV2_XptDuallinkOut					///< @deprecated	Use NTV2_XptDuallinkOut instead.
	#define		NTV2K2_XptDuallinkOutDS2			NTV2_XptDuallinkOutDS2				///< @deprecated	Use NTV2_XptDuallinkOutDS2 instead.
	#define		NTV2K2_XptDuallinkOut2				NTV2_XptDuallinkOut2				///< @deprecated	Use NTV2_XptDuallinkOut2 instead.
	#define		NTV2K2_XptDuallinkOut2DS2			NTV2_XptDuallinkOut2DS2				///< @deprecated	Use NTV2_XptDuallinkOut2DS2 instead.
	#define		NTV2K2_XptDuallinkOut3				NTV2_XptDuallinkOut3				///< @deprecated	Use NTV2_XptDuallinkOut3 instead.
	#define		NTV2K2_XptDuallinkOut3DS2			NTV2_XptDuallinkOut3DS2				///< @deprecated	Use NTV2_XptDuallinkOut3DS2 instead.
	#define		NTV2K2_XptDuallinkOut4				NTV2_XptDuallinkOut4				///< @deprecated	Use NTV2_XptDuallinkOut4 instead.
	#define		NTV2K2_XptDuallinkOut4DS2			NTV2_XptDuallinkOut4DS2				///< @deprecated	Use NTV2_XptDuallinkOut4DS2 instead.
	#define		NTV2K2_XptAlphaOut					NTV2_XptAlphaOut					///< @deprecated	Use NTV2_XptAlphaOut instead.
	#define		NTV2K2_XptAnalogIn					NTV2_XptAnalogIn					///< @deprecated	Use NTV2_XptAnalogIn instead.
	#define		NTV2K2_XptHDMIIn					NTV2_XptHDMIIn						///< @deprecated	Use NTV2_XptHDMIIn instead.
	#define		NTV2K2_XptHDMIInQ2					NTV2_XptHDMIInQ2					///< @deprecated	Use NTV2_XptHDMIInQ2 instead.
	#define		NTV2K2_XptHDMIInQ3					NTV2_XptHDMIInQ3					///< @deprecated	Use NTV2_XptHDMIInQ3 instead.
	#define		NTV2K2_XptHDMIInQ4					NTV2_XptHDMIInQ4					///< @deprecated	Use NTV2_XptHDMIInQ4 instead.
	#define		NTV2K2_XptHDMIInRGB					NTV2_XptHDMIInRGB					///< @deprecated	Use NTV2_XptHDMIInRGB instead.
	#define		NTV2K2_XptHDMIInQ2RGB				NTV2_XptHDMIInQ2RGB					///< @deprecated	Use NTV2_XptHDMIInQ2RGB instead.
	#define		NTV2K2_XptHDMIInQ3RGB				NTV2_XptHDMIInQ3RGB					///< @deprecated	Use NTV2_XptHDMIInQ3RGB instead.
	#define		NTV2K2_XptHDMIInQ4RGB				NTV2_XptHDMIInQ4RGB					///< @deprecated	Use NTV2_XptHDMIInQ4RGB instead.
	#define		NTV2K2_XptFS1SecondConverter		NTV2_XptFS1SecondConverter			///< @deprecated	This is obsolete.
	#define		NTV2KS_XptFS1ProcAmp				NTV2_XptFS1ProcAmp					///< @deprecated	This is obsolete.
	#define		NTV2KS_XptFS1TestSignalGenerator	NTV2_XptFS1TestSignalGenerator		///< @deprecated	This is obsolete.
	#define		NTV2K2_XptDuallinkIn				NTV2_XptDuallinkIn					///< @deprecated	Use NTV2_XptDuallinkIn instead.
	#define		NTV2K2_XptDuallinkIn2				NTV2_XptDuallinkIn2					///< @deprecated	Use NTV2_XptDuallinkIn2 instead.
	#define		NTV2K2_XptDuallinkIn3				NTV2_XptDuallinkIn3					///< @deprecated	Use NTV2_XptDuallinkIn3 instead.
	#define		NTV2K2_XptDuallinkIn4				NTV2_XptDuallinkIn4					///< @deprecated	Use NTV2_XptDuallinkIn4 instead.
	#define		NTV2K2_XptLUT						NTV2_XptLUT							///< @deprecated	Use NTV2_XptLUT instead.
	#define		NTV2K2_XptLUT1RGB					NTV2_XptLUT1RGB						///< @deprecated	Use NTV2_XptLUT1RGB instead.
	#define		NTV2K2_XptCSCRGB					NTV2_XptCSCRGB						///< @deprecated	Use NTV2_XptCSCRGB instead.
	#define		NTV2K2_XptCSC1VidRGB				NTV2_XptCSC1VidRGB					///< @deprecated	Use NTV2_XptCSC1VidRGB instead.
	#define		NTV2K2_XptFrameBuffer1RGB			NTV2_XptFrameBuffer1RGB				///< @deprecated	Use NTV2_XptFrameBuffer1RGB instead.
	#define		NTV2K2_XptFrameSync1RGB				NTV2_XptFrameSync1RGB				///< @deprecated	Use NTV2_XptFrameSync1RGB instead.
	#define		NTV2K2_XptFrameSync2RGB				NTV2_XptFrameSync2RGB				///< @deprecated	Use NTV2_XptFrameSync2RGB instead.
	#define		NTV2K2_XptLUT2RGB					NTV2_XptLUT2RGB						///< @deprecated	Use NTV2_XptLUT2RGB instead.
	#define		NTV2K2_XptCSC1KeyYUV				NTV2_XptCSC1KeyYUV					///< @deprecated	Use NTV2_XptCSC1KeyYUV instead.
	#define		NTV2K2_XptFrameBuffer2YUV			NTV2_XptFrameBuffer2YUV				///< @deprecated	Use NTV2_XptFrameBuffer2YUV instead.
	#define		NTV2K2_XptFrameBuffer2RGB			NTV2_XptFrameBuffer2RGB				///< @deprecated	Use NTV2_XptFrameBuffer2RGB instead.
	#define		NTV2K2_XptCSC2VidYUV				NTV2_XptCSC2VidYUV					///< @deprecated	Use NTV2_XptCSC2VidYUV instead.
	#define		NTV2K2_XptCSC2VidRGB				NTV2_XptCSC2VidRGB					///< @deprecated	Use NTV2_XptCSC2VidRGB instead.
	#define		NTV2K2_XptCSC2KeyYUV				NTV2_XptCSC2KeyYUV					///< @deprecated	Use NTV2_XptCSC2KeyYUV instead.
	#define		NTV2K2_XptMixerVidYUV				NTV2_XptMixerVidYUV					///< @deprecated	Use NTV2_XptMixerVidYUV instead.
	#define		NTV2K2_XptMixerKeyYUV				NTV2_XptMixerKeyYUV					///< @deprecated	Use NTV2_XptMixerKeyYUV instead.
	#define		NTV2K2_XptWaterMarkerRGB			NTV2_XptWaterMarkerRGB				///< @deprecated	Use NTV2_XptWaterMarkerRGB instead.
	#define		NTV2K2_XptWaterMarkerYUV			NTV2_XptWaterMarkerYUV				///< @deprecated	Use NTV2_XptWaterMarkerYUV instead.
	#define		NTV2K2_XptWaterMarker2RGB			NTV2_XptWaterMarker2RGB				///< @deprecated	Use NTV2_XptWaterMarker2RGB instead.
	#define		NTV2K2_XptWaterMarker2YUV			NTV2_XptWaterMarker2YUV				///< @deprecated	Use NTV2_XptWaterMarker2YUV instead.
	#define		NTV2K2_XptIICTRGB					NTV2_XptIICTRGB						///< @deprecated	Use NTV2_XptIICTRGB instead.
	#define		NTV2K2_XptIICT2RGB					NTV2_XptIICT2RGB					///< @deprecated	Use NTV2_XptIICT2RGB instead.
	#define		NTV2K2_XptTestPatternYUV			NTV2_XptTestPatternYUV				///< @deprecated	Use NTV2_XptTestPatternYUV instead.
	#define		NTV2K2_XptDCIMixerVidYUV			NTV2_XptDCIMixerVidYUV				///< @deprecated	Use NTV2_XptDCIMixerVidYUV instead.
	#define		NTV2K2_XptDCIMixerVidRGB			NTV2_XptDCIMixerVidRGB				///< @deprecated	Use NTV2_XptDCIMixerVidRGB instead.
	#define		NTV2K2_XptMixer2VidYUV				NTV2_XptMixer2VidYUV				///< @deprecated	Use NTV2_XptMixer2VidYUV instead.
	#define		NTV2K2_XptMixer2KeyYUV				NTV2_XptMixer2KeyYUV				///< @deprecated	Use NTV2_XptMixer2KeyYUV instead.
	#define		NTV2K2_XptStereoCompressorOut		NTV2_XptStereoCompressorOut			///< @deprecated	Use NTV2_XptStereoCompressorOut instead.
	#define		NTV2K2_XptLUT3Out					NTV2_XptLUT3Out						///< @deprecated	Use NTV2_XptLUT3Out instead.
	#define		NTV2K2_XptLUT4Out					NTV2_XptLUT4Out						///< @deprecated	Use NTV2_XptLUT4Out instead.
	#define		NTV2K2_XptFrameBuffer3YUV			NTV2_XptFrameBuffer3YUV				///< @deprecated	Use NTV2_XptFrameBuffer3YUV instead.
	#define		NTV2K2_XptFrameBuffer3RGB			NTV2_XptFrameBuffer3RGB				///< @deprecated	Use NTV2_XptFrameBuffer3RGB instead.
	#define		NTV2K2_XptFrameBuffer4YUV			NTV2_XptFrameBuffer4YUV				///< @deprecated	Use NTV2_XptFrameBuffer4YUV instead.
	#define		NTV2K2_XptFrameBuffer4RGB			NTV2_XptFrameBuffer4RGB				///< @deprecated	Use NTV2_XptFrameBuffer4RGB instead.
	#define		NTV2K2_XptSDIIn3					NTV2_XptSDIIn3						///< @deprecated	Use NTV2_XptSDIIn3 instead.
	#define		NTV2K2_XptSDIIn3DS2					NTV2_XptSDIIn3DS2					///< @deprecated	Use NTV2_XptSDIIn3DS2 instead.
	#define		NTV2K2_XptSDIIn4					NTV2_XptSDIIn4						///< @deprecated	Use NTV2_XptSDIIn4 instead.
	#define		NTV2K2_XptSDIIn4DS2					NTV2_XptSDIIn4DS2					///< @deprecated	Use NTV2_XptSDIIn4DS2 instead.
	#define		NTV2K2_XptCSC3VidYUV				NTV2_XptCSC3VidYUV					///< @deprecated	Use NTV2_XptCSC3VidYUV instead.
	#define		NTV2K2_XptCSC3VidRGB				NTV2_XptCSC3VidRGB					///< @deprecated	Use NTV2_XptCSC3VidRGB instead.
	#define		NTV2K2_XptCSC3KeyYUV				NTV2_XptCSC3KeyYUV					///< @deprecated	Use NTV2_XptCSC3KeyYUV instead.
	#define		NTV2K2_XptCSC4VidYUV				NTV2_XptCSC4VidYUV					///< @deprecated	Use NTV2_XptCSC4VidYUV instead.
	#define		NTV2K2_XptCSC4VidRGB				NTV2_XptCSC4VidRGB					///< @deprecated	Use NTV2_XptCSC4VidRGB instead.
	#define		NTV2K2_XptCSC4KeyYUV				NTV2_XptCSC4KeyYUV					///< @deprecated	Use NTV2_XptCSC4KeyYUV instead.
	#define		NTV2K2_XptCSC5VidYUV				NTV2_XptCSC5VidYUV					///< @deprecated	Use NTV2_XptCSC5VidYUV instead.
	#define		NTV2K2_XptCSC5VidRGB				NTV2_XptCSC5VidRGB					///< @deprecated	Use NTV2_XptCSC5VidRGB instead.
	#define		NTV2K2_XptCSC5KeyYUV				NTV2_XptCSC5KeyYUV					///< @deprecated	Use NTV2_XptCSC5KeyYUV instead.
	#define		NTV2K2_XptLUT5Out					NTV2_XptLUT5Out						///< @deprecated	Use NTV2_XptLUT5Out instead.
	#define		NTV2K2_XptDuallinkOut5				NTV2_XptDuallinkOut5				///< @deprecated	Use NTV2_XptDuallinkOut5 instead.
	#define		NTV2K2_XptDuallinkOut5DS2			NTV2_XptDuallinkOut5DS2				///< @deprecated	Use NTV2_XptDuallinkOut5DS2 instead.
	#define		NTV2K2_Xpt4KDownConverterOut		NTV2_Xpt4KDownConverterOut			///< @deprecated	Use NTV2_Xpt4KDownConverterOut instead.
	#define		NTV2K2_Xpt4KDownConverterOutRGB		NTV2_Xpt4KDownConverterOutRGB		///< @deprecated	Use NTV2_Xpt4KDownConverterOutRGB instead.
	#define		NTV2K2_XptFrameBuffer5YUV			NTV2_XptFrameBuffer5YUV				///< @deprecated	Use NTV2_Xpt4KDownConverterOutRGB instead.
	#define		NTV2K2_XptFrameBuffer5RGB			NTV2_XptFrameBuffer5RGB				///< @deprecated	Use NTV2_XptFrameBuffer5YUV instead.
	#define		NTV2K2_XptFrameBuffer6YUV			NTV2_XptFrameBuffer6YUV				///< @deprecated	Use NTV2_XptFrameBuffer5RGB instead.
	#define		NTV2K2_XptFrameBuffer6RGB			NTV2_XptFrameBuffer6RGB				///< @deprecated	Use NTV2_XptFrameBuffer6YUV instead.
	#define		NTV2K2_XptFrameBuffer7YUV			NTV2_XptFrameBuffer7YUV				///< @deprecated	Use NTV2_XptFrameBuffer6RGB instead.
	#define		NTV2K2_XptFrameBuffer7RGB			NTV2_XptFrameBuffer7RGB				///< @deprecated	Use NTV2_XptFrameBuffer7YUV instead.
	#define		NTV2K2_XptFrameBuffer8YUV			NTV2_XptFrameBuffer8YUV				///< @deprecated	Use NTV2_XptFrameBuffer7RGB instead.
	#define		NTV2K2_XptFrameBuffer8RGB			NTV2_XptFrameBuffer8RGB				///< @deprecated	Use NTV2_XptFrameBuffer8YUV instead.
	#define		NTV2K2_XptSDIIn5					NTV2_XptSDIIn5						///< @deprecated	Use NTV2_XptFrameBuffer8RGB instead.
	#define		NTV2K2_XptSDIIn5DS2					NTV2_XptSDIIn5DS2					///< @deprecated	Use NTV2_XptSDIIn5 instead.
	#define		NTV2K2_XptSDIIn6					NTV2_XptSDIIn6						///< @deprecated	Use NTV2_XptSDIIn5DS2 instead.
	#define		NTV2K2_XptSDIIn6DS2					NTV2_XptSDIIn6DS2					///< @deprecated	Use NTV2_XptSDIIn6 instead.
	#define		NTV2K2_XptSDIIn7					NTV2_XptSDIIn7						///< @deprecated	Use NTV2_XptSDIIn6DS2 instead.
	#define		NTV2K2_XptSDIIn7DS2					NTV2_XptSDIIn7DS2					///< @deprecated	Use NTV2_XptSDIIn7DS2 instead.
	#define		NTV2K2_XptSDIIn8					NTV2_XptSDIIn8						///< @deprecated	Use NTV2_XptSDIIn8 instead.
	#define		NTV2K2_XptSDIIn8DS2					NTV2_XptSDIIn8DS2					///< @deprecated	Use NTV2_XptSDIIn8DS2 instead.
	#define		NTV2K2_XptCSC6VidYUV				NTV2_XptCSC6VidYUV					///< @deprecated	Use NTV2_XptCSC6VidYUV instead.
	#define		NTV2K2_XptCSC6VidRGB				NTV2_XptCSC6VidRGB					///< @deprecated	Use NTV2_XptCSC6VidRGB instead.
	#define		NTV2K2_XptCSC6KeyYUV				NTV2_XptCSC6KeyYUV					///< @deprecated	Use NTV2_XptCSC6KeyYUV instead.
	#define		NTV2K2_XptCSC7VidYUV				NTV2_XptCSC7VidYUV					///< @deprecated	Use NTV2_XptCSC7VidYUV instead.
	#define		NTV2K2_XptCSC7VidRGB				NTV2_XptCSC7VidRGB					///< @deprecated	Use NTV2_XptCSC7VidRGB instead.
	#define		NTV2K2_XptCSC7KeyYUV				NTV2_XptCSC7KeyYUV					///< @deprecated	Use NTV2_XptCSC7KeyYUV instead.
	#define		NTV2K2_XptCSC8VidYUV				NTV2_XptCSC8VidYUV					///< @deprecated	Use NTV2_XptCSC8VidYUV instead.
	#define		NTV2K2_XptCSC8VidRGB				NTV2_XptCSC8VidRGB					///< @deprecated	Use NTV2_XptCSC8VidRGB instead.
	#define		NTV2K2_XptCSC8KeyYUV				NTV2_XptCSC8KeyYUV					///< @deprecated	Use NTV2_XptCSC8KeyYUV instead.
	#define		NTV2K2_XptLUT6Out					NTV2_XptLUT6Out						///< @deprecated	Use NTV2_XptLUT6Out instead.
	#define		NTV2K2_XptLUT7Out					NTV2_XptLUT7Out						///< @deprecated	Use NTV2_XptLUT7Out instead.
	#define		NTV2K2_XptLUT8Out					NTV2_XptLUT8Out						///< @deprecated	Use NTV2_XptLUT8Out instead.
	#define		NTV2K2_XptDuallinkOut6				NTV2_XptDuallinkOut6				///< @deprecated	Use NTV2_XptDuallinkOut6 instead.
	#define		NTV2K2_XptDuallinkOut6DS2			NTV2_XptDuallinkOut6DS2				///< @deprecated	Use NTV2_XptDuallinkOut6DS2 instead.
	#define		NTV2K2_XptDuallinkOut7				NTV2_XptDuallinkOut7				///< @deprecated	Use NTV2_XptDuallinkOut7 instead.
	#define		NTV2K2_XptDuallinkOut7DS2			NTV2_XptDuallinkOut7DS2				///< @deprecated	Use NTV2_XptDuallinkOut7DS2 instead.
	#define		NTV2K2_XptDuallinkOut8				NTV2_XptDuallinkOut8				///< @deprecated	Use NTV2_XptDuallinkOut8 instead.
	#define		NTV2K2_XptDuallinkOut8DS2			NTV2_XptDuallinkOut8DS2				///< @deprecated	Use NTV2_XptDuallinkOut8DS2 instead.
	#define		NTV2K2_XptMixer3VidYUV				NTV2_XptMixer3VidYUV				///< @deprecated	Use NTV2_XptMixer3VidYUV instead.
	#define		NTV2K2_XptMixer3KeyYUV				NTV2_XptMixer3KeyYUV				///< @deprecated	Use NTV2_XptMixer3KeyYUV instead.
	#define		NTV2K2_XptMixer4VidYUV				NTV2_XptMixer4VidYUV				///< @deprecated	Use NTV2_XptMixer4VidYUV instead.
	#define		NTV2K2_XptMixer4KeyYUV				NTV2_XptMixer4KeyYUV				///< @deprecated	Use NTV2_XptMixer4KeyYUV instead.
	#define		NTV2K2_XptDuallinkIn5				NTV2_XptDuallinkIn5					///< @deprecated	Use NTV2_XptDuallinkIn5 instead.
	#define		NTV2K2_XptDuallinkIn6				NTV2_XptDuallinkIn6					///< @deprecated	Use NTV2_XptDuallinkIn6 instead.
	#define		NTV2K2_XptDuallinkIn7				NTV2_XptDuallinkIn7					///< @deprecated	Use NTV2_XptDuallinkIn7 instead.
	#define		NTV2K2_XptDuallinkIn8				NTV2_XptDuallinkIn8					///< @deprecated	Use NTV2_XptDuallinkIn8 instead.
	#define		NTV2K2_XptRuntimeCalc				NTV2_XptRuntimeCalc					///< @deprecated	Use NTV2_XptRuntimeCalc instead.

	//	NTV2BitFileType
	#define		NTV2K2_VideoProcBitFile				NTV2_VideoProcBitFile				///< @deprecated	Use NTV2_VideoProcBitFile instead.
	#define		NTV2K2_PCIBitFile					NTV2_PCIBitFile						///< @deprecated	Use NTV2_PCIBitFile instead.
	#define		NTV2K2_FWBitFile					NTV2_FWBitFile						///< @deprecated	Use NTV2_FWBitFile instead.
	#define		NTV2K2_Firmware						NTV2_Firmware						///< @deprecated	Use NTV2_Firmware instead.
	#define		NTV2K2_BitFile1						NTV2_BitFile1						///< @deprecated	Use NTV2_BitFile1 instead.
	#define		NTV2K2_BitFile2						NTV2_BitFile2						///< @deprecated	Use NTV2_BitFile2 instead.
	#define		NTV2K2_BitFile3						NTV2_BitFile3						///< @deprecated	Use NTV2_BitFile3 instead.
	#define		NTV2K2_BitFile4						NTV2_BitFile4						///< @deprecated	Use NTV2_BitFile4 instead.
	#define		NTV2K2_BitFile5						NTV2_BitFile5						///< @deprecated	Use NTV2_BitFile5 instead.
	#define		NTV2K2_BitFile6						NTV2_BitFile6						///< @deprecated	Use NTV2_BitFile6 instead.
	#define		NTV2K2_BitFile7						NTV2_BitFile7						///< @deprecated	Use NTV2_BitFile7 instead.
	#define		NTV2K2_BitFile8						NTV2_BitFile8						///< @deprecated	Use NTV2_BitFile8 instead.

	//	NTV2ConversionMode
	#define		NTV2K2_1080i_5994to525_5994			NTV2_1080i_5994to525_5994			///< @deprecated	Use NTV2_1080i_5994to525_5994 instead.
	#define		NTV2K2_1080i_2500to625_2500			NTV2_1080i_2500to625_2500			///< @deprecated	Use NTV2_1080i_2500to625_2500 instead.
	#define		NTV2K2_720p_5994to525_5994			NTV2_720p_5994to525_5994			///< @deprecated	Use NTV2_720p_5994to525_5994 instead.
	#define		NTV2K2_720p_5000to625_2500			NTV2_720p_5000to625_2500			///< @deprecated	Use NTV2_720p_5000to625_2500 instead.
	#define		NTV2K2_525_5994to1080i_5994			NTV2_525_5994to1080i_5994			///< @deprecated	Use NTV2_525_5994to1080i_5994 instead.
	#define		NTV2K2_525_5994to720p_5994			NTV2_525_5994to720p_5994			///< @deprecated	Use NTV2_525_5994to720p_5994 instead.
	#define		NTV2K2_625_2500to1080i_2500			NTV2_625_2500to1080i_2500			///< @deprecated	Use NTV2_625_2500to1080i_2500 instead.
	#define		NTV2K2_625_2500to720p_5000			NTV2_625_2500to720p_5000			///< @deprecated	Use NTV2_625_2500to720p_5000 instead.
	#define		NTV2K2_720p_5000to1080i_2500		NTV2_720p_5000to1080i_2500			///< @deprecated	Use NTV2_720p_5000to1080i_2500 instead.
	#define		NTV2K2_720p_5994to1080i_5994		NTV2_720p_5994to1080i_5994			///< @deprecated	Use NTV2_720p_5994to1080i_5994 instead.
	#define		NTV2K2_720p_6000to1080i_3000		NTV2_720p_6000to1080i_3000			///< @deprecated	Use NTV2_720p_6000to1080i_3000 instead.
	#define		NTV2K2_1080i2398to525_2398			NTV2_1080i2398to525_2398			///< @deprecated	Use NTV2_1080i2398to525_2398 instead.
	#define		NTV2K2_1080i2398to525_2997			NTV2_1080i2398to525_2997			///< @deprecated	Use NTV2_1080i2398to525_2997 instead.
	#define		NTV2K2_1080i2400to525_2400			NTV2_1080i2400to525_2400			///< @deprecated	Use NTV2_1080i2400to525_2400 instead.
	#define		NTV2K2_1080p2398to525_2398			NTV2_1080p2398to525_2398			///< @deprecated	Use NTV2_1080p2398to525_2398 instead.
	#define		NTV2K2_1080p2398to525_2997			NTV2_1080p2398to525_2997			///< @deprecated	Use NTV2_1080p2398to525_2997 instead.
	#define		NTV2K2_1080p2400to525_2400			NTV2_1080p2400to525_2400			///< @deprecated	Use NTV2_1080p2400to525_2400 instead.
	#define		NTV2K2_1080i_2500to720p_5000		NTV2_1080i_2500to720p_5000			///< @deprecated	Use NTV2_1080i_2500to720p_5000 instead.
	#define		NTV2K2_1080i_5994to720p_5994		NTV2_1080i_5994to720p_5994			///< @deprecated	Use NTV2_1080i_5994to720p_5994 instead.
	#define		NTV2K2_1080i_3000to720p_6000		NTV2_1080i_3000to720p_6000			///< @deprecated	Use NTV2_1080i_3000to720p_6000 instead.
	#define		NTV2K2_1080i_2398to720p_2398		NTV2_1080i_2398to720p_2398			///< @deprecated	Use NTV2_1080i_2398to720p_2398 instead.
	#define		NTV2K2_720p_2398to1080i_2398		NTV2_720p_2398to1080i_2398			///< @deprecated	Use NTV2_720p_2398to1080i_2398 instead.
	#define		NTV2K2_525_2398to1080i_2398			NTV2_525_2398to1080i_2398			///< @deprecated	Use NTV2_525_2398to1080i_2398 instead.
	#define		NTV2K2_525_5994to525_5994			NTV2_525_5994to525_5994				///< @deprecated	Use NTV2_525_5994to525_5994 instead.
	#define		NTV2K2_625_2500to625_2500			NTV2_625_2500to625_2500				///< @deprecated	Use NTV2_625_2500to625_2500 instead.
	#define		NTV2K2_525_5994to525psf_2997		NTV2_525_5994to525psf_2997			///< @deprecated	Use NTV2_525_5994to525psf_2997 instead.
	#define		NTV2K2_625_5000to625psf_2500		NTV2_625_5000to625psf_2500			///< @deprecated	Use NTV2_625_5000to625psf_2500 instead.
	#define		NTV2K2_1080i_5000to1080psf_2500		NTV2_1080i_5000to1080psf_2500		///< @deprecated	Use NTV2_1080i_5000to1080psf_2500 instead.
	#define		NTV2K2_1080i_5994to1080psf_2997		NTV2_1080i_5994to1080psf_2997		///< @deprecated	Use NTV2_1080i_5994to1080psf_2997 instead.
	#define		NTV2K2_1080i_6000to1080psf_3000		NTV2_1080i_6000to1080psf_3000		///< @deprecated	Use NTV2_1080i_6000to1080psf_3000 instead.
	#define		NTV2K2_1080p_3000to720p_6000		NTV2_1080p_3000to720p_6000			///< @deprecated	Use NTV2_1080p_3000to720p_6000 instead.
	#define		NTV2K2_1080psf_2398to1080i_5994		NTV2_1080psf_2398to1080i_5994		///< @deprecated	Use NTV2_1080psf_2398to1080i_5994 instead.
	#define		NTV2K2_1080psf_2400to1080i_3000		NTV2_1080psf_2400to1080i_3000		///< @deprecated	Use NTV2_1080psf_2400to1080i_3000 instead.
	#define		NTV2K2_1080psf_2500to1080i_2500		NTV2_1080psf_2500to1080i_2500		///< @deprecated	Use NTV2_1080psf_2500to1080i_2500 instead.
	#define		NTV2K2_1080p_2398to1080i_5994		NTV2_1080p_2398to1080i_5994			///< @deprecated	Use NTV2_1080p_2398to1080i_5994 instead.
	#define		NTV2K2_1080p_2400to1080i_3000		NTV2_1080p_2400to1080i_3000			///< @deprecated	Use NTV2_1080p_2400to1080i_3000 instead.
	#define		NTV2K2_1080p_2500to1080i_2500		NTV2_1080p_2500to1080i_2500			///< @deprecated	Use NTV2_1080p_2500to1080i_2500 instead.
	#define		NTV2K2_NUM_CONVERSIONMODES			NTV2_NUM_CONVERSIONMODES			///< @deprecated	Use NTV2_NUM_CONVERSIONMODES instead.
	#define		NTV2K2_CONVERSIONMODE_UNKNOWN		NTV2_CONVERSIONMODE_UNKNOWN			///< @deprecated	Use NTV2_CONVERSIONMODE_UNKNOWN instead.

	//	NTV2RGBBlackRange
	#define		NTV2K2_RGBBLACKRANGE_0_0x3FF		NTV2_RGBBLACKRANGE_0_0x3FF			///< @deprecated	Use NTV2_RGBBLACKRANGE_0_0x3FF instead.
	#define		NTV2K2_RGBBLACKRANGE_0x40_0x3C0		NTV2_RGBBLACKRANGE_0x40_0x3C0		///< @deprecated	Use NTV2_RGBBLACKRANGE_0x40_0x3C0 instead.

	//	NTV2Framesize
	#define		NTV2K2_FRAMESIZE_2MB				NTV2_FRAMESIZE_2MB					///< @deprecated	Use NTV2_FRAMESIZE_2MB instead.
	#define		NTV2K2_FRAMESIZE_4MB				NTV2_FRAMESIZE_4MB					///< @deprecated	Use NTV2_FRAMESIZE_4MB instead.
	#define		NTV2K2_FRAMESIZE_8MB				NTV2_FRAMESIZE_8MB					///< @deprecated	Use NTV2_FRAMESIZE_8MB instead.
	#define		NTV2K2_FRAMESIZE_16MB				NTV2_FRAMESIZE_16MB					///< @deprecated	Use NTV2_FRAMESIZE_16MB instead.

	//	NTV2VideoDACMode
	#define		NTV2K2_480iRGB						NTV2_480iRGB						///< @deprecated	Use NTV2_480iRGB instead.
	#define		NTV2K2_480iYPbPrSMPTE				NTV2_480iYPbPrSMPTE					///< @deprecated	Use NTV2_480iYPbPrSMPTE instead.
	#define		NTV2K2_480iYPbPrBetacam525			NTV2_480iYPbPrBetacam525			///< @deprecated	Use NTV2_480iYPbPrBetacam525 instead.
	#define		NTV2K2_480iYPbPrBetacamJapan		NTV2_480iYPbPrBetacamJapan			///< @deprecated	Use NTV2_480iYPbPrBetacamJapan instead.
	#define		NTV2K2_480iNTSC_US_Composite		NTV2_480iNTSC_US_Composite			///< @deprecated	Use NTV2_480iNTSC_US_Composite instead.
	#define		NTV2K2_480iNTSC_Japan_Composite		NTV2_480iNTSC_Japan_Composite		///< @deprecated	Use NTV2_480iNTSC_Japan_Composite instead.
	#define		NTV2K2_576iRGB						NTV2_576iRGB						///< @deprecated	Use NTV2_576iRGB instead.
	#define		NTV2K2_576iYPbPrSMPTE				NTV2_576iYPbPrSMPTE					///< @deprecated	Use NTV2_576iYPbPrSMPTE instead.
	#define		NTV2K2_576iPAL_Composite			NTV2_576iPAL_Composite				///< @deprecated	Use NTV2_576iPAL_Composite instead.
	#define		NTV2K2_1080iRGB						NTV2_1080iRGB						///< @deprecated	Use NTV2_1080iRGB instead.
	#define		NTV2K2_1080psfRGB					NTV2_1080psfRGB						///< @deprecated	Use NTV2_1080psfRGB instead.
	#define		NTV2K2_720pRGB						NTV2_720pRGB						///< @deprecated	Use NTV2_720pRGB instead.
	#define		NTV2K2_1080iSMPTE					NTV2_1080iSMPTE						///< @deprecated	Use NTV2_1080iSMPTE instead.
	#define		NTV2K2_1080psfSMPTE					NTV2_1080psfSMPTE					///< @deprecated	Use NTV2_1080psfSMPTE instead.
	#define		NTV2K2_720pSMPTE					NTV2_720pSMPTE						///< @deprecated	Use NTV2_720pSMPTE instead.
	#define		NTV2K2_1080iXVGA					NTV2_1080iXVGA						///< @deprecated	Use NTV2_1080iXVGA instead.
	#define		NTV2K2_1080psfXVGA					NTV2_1080psfXVGA					///< @deprecated	Use NTV2_1080psfXVGA instead.
	#define		NTV2K2_720pXVGA						NTV2_720pXVGA						///< @deprecated	Use NTV2_720pXVGA instead.
	#define		NTV2K2_2Kx1080RGB					NTV2_2Kx1080RGB						///< @deprecated	Use NTV2_2Kx1080RGB instead.
	#define		NTV2K2_2Kx1080SMPTE					NTV2_2Kx1080SMPTE					///< @deprecated	Use NTV2_2Kx1080SMPTE instead.
	#define		NTV2K2_2Kx1080XVGA					NTV2_2Kx1080XVGA					///< @deprecated	Use NTV2_2Kx1080XVGA instead.

	//	NTV2LSVideoADCMode
	#define		NTV2K2_480iADCComponentBeta			NTV2_480iADCComponentBeta			///< @deprecated	Use NTV2_480iADCComponentBeta instead.
	#define		NTV2K2_480iADCComponentSMPTE		NTV2_480iADCComponentSMPTE			///< @deprecated	Use NTV2_480iADCComponentSMPTE instead.
	#define		NTV2K2_480iADCSVideoUS				NTV2_480iADCSVideoUS				///< @deprecated	Use NTV2_480iADCSVideoUS instead.
	#define		NTV2K2_480iADCCompositeUS			NTV2_480iADCCompositeUS				///< @deprecated	Use NTV2_480iADCCompositeUS instead.
	#define		NTV2K2_480iADCComponentBetaJapan	NTV2_480iADCComponentBetaJapan		///< @deprecated	Use NTV2_480iADCComponentBetaJapan instead.
	#define		NTV2K2_480iADCComponentSMPTEJapan	NTV2_480iADCComponentSMPTEJapan		///< @deprecated	Use NTV2_480iADCComponentSMPTEJapan instead.
	#define		NTV2K2_480iADCSVideoJapan			NTV2_480iADCSVideoJapan				///< @deprecated	Use NTV2_480iADCSVideoJapan instead.
	#define		NTV2K2_480iADCCompositeJapan		NTV2_480iADCCompositeJapan			///< @deprecated	Use NTV2_480iADCCompositeJapan instead.
	#define		NTV2K2_576iADCComponentBeta			NTV2_576iADCComponentBeta			///< @deprecated	Use NTV2_576iADCComponentBeta instead.
	#define		NTV2K2_576iADCComponentSMPTE		NTV2_576iADCComponentSMPTE			///< @deprecated	Use NTV2_576iADCComponentSMPTE instead.
	#define		NTV2K2_576iADCSVideo				NTV2_576iADCSVideo					///< @deprecated	Use NTV2_576iADCSVideo instead.
	#define		NTV2K2_576iADCComposite				NTV2_576iADCComposite				///< @deprecated	Use NTV2_576iADCComposite instead.
	#define		NTV2K2_720p_60						NTV2_720p_60						///< @deprecated	Use NTV2_720p_60 instead.
	#define		NTV2K2_1080i_30						NTV2_1080i_30						///< @deprecated	Use NTV2_1080i_30 instead.
	#define		NTV2K2_720p_50						NTV2_720p_50						///< @deprecated	Use NTV2_720p_50 instead.
	#define		NTV2K2_1080i_25						NTV2_1080i_25						///< @deprecated	Use NTV2_1080i_25 instead.
	#define		NTV2K2_1080pSF24					NTV2_1080pSF24						///< @deprecated	Use NTV2_1080pSF24 instead.

	//	NTV2AnalogType
	#define		NTV2K2_AnlgComposite				NTV2_AnlgComposite					///< @deprecated	Use NTV2_AnlgComposite instead.
	#define		NTV2K2_AnlgComponentSMPTE			NTV2_AnlgComponentSMPTE				///< @deprecated	Use NTV2_AnlgComponentSMPTE instead.
	#define		NTV2K2_AnlgComponentBetacam			NTV2_AnlgComponentBetacam			///< @deprecated	Use NTV2_AnlgComponentBetacam instead.
	#define		NTV2K2_AnlgComponentRGB				NTV2_AnlgComponentRGB				///< @deprecated	Use NTV2_AnlgComponentRGB instead.
	#define		NTV2K2_AnlgXVGA						NTV2_AnlgXVGA						///< @deprecated	Use NTV2_AnlgXVGA instead.
	#define		NTV2K2_AnlgSVideo					NTV2_AnlgSVideo						///< @deprecated	Use NTV2_AnlgSVideo instead.

	//	NTV2AnalogBlackLevel
	#define		NTV2K2_Black75IRE					NTV2_Black75IRE						///< @deprecated	Use NTV2_Black75IRE instead.
	#define		NTV2K2_Black0IRE					NTV2_Black0IRE						///< @deprecated	Use NTV2_Black0IRE instead.

	//	NTV2InputVideoSelect
	#define		NTV2K2_Input1Select					NTV2_Input1Select					///< @deprecated	Use NTV2_Input1Select instead.
	#define		NTV2K2_Input2Select					NTV2_Input2Select					///< @deprecated	Use NTV2_Input2Select instead.
	#define		NTV2K2_AnalogInputSelect			NTV2_AnalogInputSelect				///< @deprecated	Use NTV2_AnalogInputSelect instead.
	#define		NTV2K2_Input3Select					NTV2_Input3Select					///< @deprecated	Use NTV2_Input3Select instead.
	#define		NTV2K2_Input4Select					NTV2_Input4Select					///< @deprecated	Use NTV2_Input4Select instead.
	#define		NTV2K2_Input5Select					NTV2_Input5Select					///< @deprecated	Use NTV2_Input5Select instead.
	#define		NTV2K2_DualLinkInputSelect			NTV2_DualLinkInputSelect			///< @deprecated	Use NTV2_DualLinkInputSelect instead.
	#define		NTV2K2_DualLink2xSdi4k				NTV2_DualLink2xSdi4k				///< @deprecated	Use NTV2_DualLink2xSdi4k instead.
	#define		NTV2K2_DualLink4xSdi4k				NTV2_DualLink4xSdi4k				///< @deprecated	Use NTV2_DualLink4xSdi4k instead.
	#define		NTV2K2_InputSelectMax				NTV2_MAX_NUM_InputVideoSelectEnums	///< @deprecated	Use NTV2_MAX_NUM_InputVideoSelectEnums instead.

	//	NTV2SDIInputFormatSelect
	#define		NTV2K2_YUVSelect					NTV2_RGBSelect						///< @deprecated	Use NTV2_RGBSelect instead.
	#define		NTV2K2_RGBSelect					NTV2_RGBSelect						///< @deprecated	Use NTV2_RGBSelect instead.
	#define		NTV2K2_Stereo3DSelect				NTV2_Stereo3DSelect					///< @deprecated	Use NTV2_Stereo3DSelect instead.

	//	NTV2InputAudioSelect
	#define		NTV2K2_AES_EBU_XLRSelect			NTV2_AES_EBU_XLRSelect				///< @deprecated	Use NTV2_AES_EBU_XLRSelect instead.
	#define		NTV2K2_AES_EBU_BNCSelect			NTV2_AES_EBU_BNCSelect				///< @deprecated	Use NTV2_AES_EBU_BNCSelect instead.
	#define		NTV2K2_Input1Embedded1_8Select		NTV2_Input1Embedded1_8Select		///< @deprecated	Use NTV2_Input1Embedded1_8Select instead.
	#define		NTV2K2_Input1Embedded9_16Select		NTV2_Input1Embedded9_16Select		///< @deprecated	Use NTV2_Input1Embedded9_16Select instead.
	#define		NTV2K2_Input2Embedded1_8Select		NTV2_Input2Embedded1_8Select		///< @deprecated	Use NTV2_Input2Embedded1_8Select instead.
	#define		NTV2K2_Input2Embedded9_16Select		NTV2_Input2Embedded9_16Select		///< @deprecated	Use NTV2_Input2Embedded9_16Select instead.
	#define		NTV2K2_AnalogSelect					NTV2_AnalogSelect					///< @deprecated	Use NTV2_AnalogSelect instead.
	#define		NTV2K2_HDMISelect					NTV2_HDMISelect						///< @deprecated	Use NTV2_HDMISelect instead.
	#define		NTV2K2_AudioInputOther				NTV2_AudioInputOther				///< @deprecated	Use NTV2_AudioInputOther instead.

	//	NTV2AudioMapSelect
	#define		NTV2K2_AudioMap12_12				NTV2_AudioMap12_12					///< @deprecated	Use NTV2_AudioMap12_12 instead.
	#define		NTV2K2_AudioMap34_12				NTV2_AudioMap34_12					///< @deprecated	Use NTV2_AudioMap34_12 instead.
	#define		NTV2K2_AudioMap56_12				NTV2_AudioMap56_12					///< @deprecated	Use NTV2_AudioMap56_12 instead.
	#define		NTV2K2_AudioMap78_12				NTV2_AudioMap78_12					///< @deprecated	Use NTV2_AudioMap78_12 instead.
	#define		NTV2K2_AudioMap910_12				NTV2_AudioMap910_12					///< @deprecated	Use NTV2_AudioMap910_12 instead.
	#define		NTV2K2_AudioMap1112_12				NTV2_AudioMap1112_12				///< @deprecated	Use NTV2_AudioMap1112_12 instead.
	#define		NTV2K2_AudioMap1314_12				NTV2_AudioMap1314_12				///< @deprecated	Use NTV2_AudioMap1314_12 instead.
	#define		NTV2K2_AudioMap1516_12				NTV2_AudioMap1516_12				///< @deprecated	Use NTV2_AudioMap1516_12 instead.

	//	NTV2OutputVideoSelect
	#define		NTV2K2_PrimaryOutputSelect			NTV2_PrimaryOutputSelect			///< @deprecated	Use NTV2_PrimaryOutputSelect instead.
	#define		NTV2K2_SecondaryOutputSelect		NTV2_SecondaryOutputSelect			///< @deprecated	Use NTV2_SecondaryOutputSelect instead.
	#define		NTV2K2_DualLinkOutputSelect			NTV2_DualLinkOutputSelect			///< @deprecated	Use NTV2_DualLinkOutputSelect instead.
	#define		NTV2K2_VideoPlusKeySelect			NTV2_VideoPlusKeySelect				///< @deprecated	Use NTV2_VideoPlusKeySelect instead.
	#define		NTV2K2_StereoOutputSelect			NTV2_StereoOutputSelect				///< @deprecated	Use NTV2_StereoOutputSelect instead.
	#define		NTV2K2_Quadrant1Select				NTV2_Quadrant1Select				///< @deprecated	Use NTV2_Quadrant1Select instead.
	#define		NTV2K2_Quadrant2Select				NTV2_Quadrant2Select				///< @deprecated	Use NTV2_Quadrant2Select instead.
	#define		NTV2K2_Quadrant3Select				NTV2_Quadrant3Select				///< @deprecated	Use NTV2_Quadrant3Select instead.
	#define		NTV2K2_Quadrant4Select				NTV2_Quadrant4Select				///< @deprecated	Use NTV2_Quadrant4Select instead.
	#define		NTV2k2_Quarter4k					NTV2_Quarter4k						///< @deprecated	Use NTV2_Quarter4k instead.
	#define		NTV2K2_2xSdi4k						NTV2_2xSdi4k						///< @deprecated	Use NTV2_2xSdi4k instead.
	#define		NTV2K2_4xSdi4k						NTV2_4xSdi4k						///< @deprecated	Use NTV2_4xSdi4k instead.

	//	NTV2UpConvertMode
	#define		NTV2K2_UpConvertAnamorphic			NTV2_UpConvertAnamorphic			///< @deprecated	Use NTV2_UpConvertAnamorphic instead.
	#define		NTV2K2_UpConvertPillarbox4x3		NTV2_UpConvertPillarbox4x3			///< @deprecated	Use NTV2_UpConvertPillarbox4x3 instead.
	#define		NTV2K2_UpConvertZoom14x9			NTV2_UpConvertZoom14x9				///< @deprecated	Use NTV2_UpConvertZoom14x9 instead.
	#define		NTV2K2_UpConvertZoomLetterbox		NTV2_UpConvertZoomLetterbox			///< @deprecated	Use NTV2_UpConvertZoomLetterbox instead.
	#define		NTV2K2_UpConvertZoomWide			NTV2_UpConvertZoomWide				///< @deprecated	Use NTV2_UpConvertZoomWide instead.

	//	NTV2DownConvertMode
	#define		NTV2K2_DownConvertLetterbox			NTV2_DownConvertLetterbox			///< @deprecated	Use NTV2_DownConvertLetterbox instead.
	#define		NTV2K2_DownConvertCrop				NTV2_DownConvertCrop				///< @deprecated	Use NTV2_DownConvertCrop instead.
	#define		NTV2K2_DownConvertAnamorphic		NTV2_DownConvertAnamorphic			///< @deprecated	Use NTV2_DownConvertAnamorphic instead.
	#define		NTV2K2_DownConvert14x9				NTV2_DownConvert14x9				///< @deprecated	Use NTV2_DownConvert14x9 instead.

	//	NTV2IsoConvertMode
	#define		NTV2K2_IsoLetterBox					NTV2_IsoLetterBox					///< @deprecated	Use NTV2_IsoLetterBox instead.
	#define		NTV2K2_IsoHCrop						NTV2_IsoHCrop						///< @deprecated	Use NTV2_IsoHCrop instead.
	#define		NTV2K2_IsoPillarBox					NTV2_IsoPillarBox					///< @deprecated	Use NTV2_IsoPillarBox instead.
	#define		NTV2K2_IsoVCrop						NTV2_IsoVCrop						///< @deprecated	Use NTV2_IsoVCrop instead.
	#define		NTV2K2_Iso14x9						NTV2_Iso14x9						///< @deprecated	Use NTV2_Iso14x9 instead.
	#define		NTV2K2_IsoPassThrough				NTV2_IsoPassThrough					///< @deprecated	Use NTV2_IsoPassThrough instead.

	//	NTV2QuarterSizeExpandMode
	#define		NTV2K2_QuarterSizeExpandOff			NTV2_QuarterSizeExpandOff			///< @deprecated	Use NTV2_QuarterSizeExpandOff instead.
	#define		NTV2K2_QuarterSizeExpandOn			NTV2_QuarterSizeExpandOn			///< @deprecated	Use NTV2_QuarterSizeExpandOn instead.

	//	NTV2FrameBufferQuality
	#define		NTV2K2_StandardQuality				NTV2_StandardQuality				///< @deprecated	Use NTV2_StandardQuality instead.
	#define		NTV2K2_HighQuality					NTV2_HighQuality					///< @deprecated	Use NTV2_HighQuality instead.
	#define		NTV2K2_ProRes						NTV2_ProRes							///< @deprecated	Use NTV2_ProRes instead.
	#define		NTV2K2_ProResHQ						NTV2_ProResHQ						///< @deprecated	Use NTV2_ProResHQ instead.
	#define		NTV2K2_ProResLT						NTV2_ProResLT						///< @deprecated	Use NTV2_ProResLT instead.
	#define		NTV2K2_ProResProxy					NTV2_ProResProxy					///< @deprecated	Use NTV2_ProResProxy instead.

	//	NTV2EncodeAsPSF
	#define		NTV2K2_NoPSF						NTV2_NoPSF							///< @deprecated	Use NTV2_NoPSF instead.
	#define		NTV2K2_IsPSF						NTV2_IsPSF							///< @deprecated	Use NTV2_IsPSF instead.

	//	NTV2BreakoutType
	#define		NTV2K2_BreakoutNone					NTV2_BreakoutNone					///< @deprecated	Use NTV2_BreakoutNone instead.
	#define		NTV2K2_BreakoutCableXLR				NTV2_BreakoutCableXLR				///< @deprecated	Use NTV2_BreakoutCableXLR instead.
	#define		NTV2K2_BreakoutCableBNC				NTV2_BreakoutCableBNC				///< @deprecated	Use NTV2_BreakoutCableBNC instead.
	#define		NTV2K2_KBox							NTV2_KBox							///< @deprecated	Use NTV2_KBox instead.
	#define		NTV2K2_KLBox						NTV2_KLBox							///< @deprecated	Use NTV2_KLBox instead.
	#define		NTV2K2_K3Box						NTV2_K3Box							///< @deprecated	Use NTV2_K3Box instead.
	#define		NTV2K2_KLHiBox						NTV2_KLHiBox						///< @deprecated	Use NTV2_KLHiBox instead.
	#define		NTV2K2_KLHePlusBox					NTV2_KLHePlusBox					///< @deprecated	Use NTV2_KLHePlusBox instead.
	#define		NTV2K2_K3GBox						NTV2_K3GBox							///< @deprecated	Use NTV2_K3GBox instead.

	//	NTV2AudioMonitorSelect
	#define		NTV2K2_AudioMonitor1_2				NTV2_AudioMonitor1_2				///< @deprecated	Use NTV2_AudioMonitor1_2 instead.
	#define		NTV2K2_AudioMonitor3_4				NTV2_AudioMonitor3_4				///< @deprecated	Use NTV2_AudioMonitor3_4 instead.
	#define		NTV2K2_AudioMonitor5_6				NTV2_AudioMonitor5_6				///< @deprecated	Use NTV2_AudioMonitor5_6 instead.
	#define		NTV2K2_AudioMonitor7_8				NTV2_AudioMonitor7_8				///< @deprecated	Use NTV2_AudioMonitor7_8 instead.
	#define		NTV2K2_AudioMonitor9_10				NTV2_AudioMonitor9_10				///< @deprecated	Use NTV2_AudioMonitor9_10 instead.
	#define		NTV2K2_AudioMonitor11_12			NTV2_AudioMonitor11_12				///< @deprecated	Use NTV2_AudioMonitor11_12 instead.
	#define		NTV2K2_AudioMonitor13_14			NTV2_AudioMonitor13_14				///< @deprecated	Use NTV2_AudioMonitor13_14 instead.
	#define		NTV2K2_AudioMonitor15_16			NTV2_AudioMonitor15_16				///< @deprecated	Use NTV2_AudioMonitor15_16 instead.

	//	NTV2Audio2ChannelSelect
	#define		NTV2K2_AudioChannel1_2				NTV2_AudioChannel1_2				///< @deprecated	Use NTV2_AudioChannel1_2 instead.
	#define		NTV2K2_AudioChannel3_4				NTV2_AudioChannel3_4				///< @deprecated	Use NTV2_AudioChannel3_4 instead.
	#define		NTV2K2_AudioChannel5_6				NTV2_AudioChannel5_6				///< @deprecated	Use NTV2_AudioChannel5_6 instead.
	#define		NTV2K2_AudioChannel7_8				NTV2_AudioChannel7_8				///< @deprecated	Use NTV2_AudioChannel7_8 instead.
	#define		NTV2K2_AudioChannel9_10				NTV2_AudioChannel9_10				///< @deprecated	Use NTV2_AudioChannel9_10 instead.
	#define		NTV2K2_AudioChannel11_12			NTV2_AudioChannel11_12				///< @deprecated	Use NTV2_AudioChannel11_12 instead.
	#define		NTV2K2_AudioChannel13_14			NTV2_AudioChannel13_14				///< @deprecated	Use NTV2_AudioChannel13_14 instead.
	#define		NTV2K2_AudioChannel15_16			NTV2_AudioChannel15_16				///< @deprecated	Use NTV2_AudioChannel15_16 instead.
	#define		NTV2_MAX_NUM_Audio2ChannelSelect	NTV2_MAX_NUM_AudioChannelPair		///< @deprecated	Use NTV2_MAX_NUM_AudioChannelPair instead.

	//	NTV2Audio4ChannelSelect
	#define		NTV2K2_AudioChannel1_4				NTV2_AudioChannel1_4				///< @deprecated	Use NTV2_AudioChannel1_4 instead.
	#define		NTV2K2_AudioChannel5_8				NTV2_AudioChannel5_8				///< @deprecated	Use NTV2_AudioChannel5_8 instead.
	#define		NTV2K2_AudioChannel9_12				NTV2_AudioChannel9_12				///< @deprecated	Use NTV2_AudioChannel9_12 instead.
	#define		NTV2K2_AudioChannel13_16			NTV2_AudioChannel13_16				///< @deprecated	Use NTV2_AudioChannel13_16 instead.

	//	NTV2Audio8ChannelSelect
	#define		NTV2K2_AudioChannel1_8				NTV2_AudioChannel1_8				///< @deprecated	Use NTV2_AudioChannel1_8 instead.
	#define		NTV2K2_AudioChannel9_16				NTV2_AudioChannel9_16				///< @deprecated	Use NTV2_AudioChannel9_16 instead.

	//	NTV2ColorSpaceType
	#define		NTV2_ColorSpaceAuto					NTV2_ColorSpaceTypeAuto				///< @deprecated	Use NTV2_ColorSpaceTypeAuto instead.
	#define		NTV2_ColorSpaceRec601				NTV2_ColorSpaceTypeRec601			///< @deprecated	Use NTV2_ColorSpaceTypeRec601 instead.
	#define		NTV2_ColorSpaceRec709				NTV2_ColorSpaceTypeRec709			///< @deprecated	Use NTV2_ColorSpaceTypeRec709 instead.

	//	NTV2Stereo3DMode
	#define		NTV2K2_Stereo3DOff					NTV2_Stereo3DOff					///< @deprecated	Use NTV2_Stereo3DOff instead.
	#define		NTV2K2_Stereo3DSideBySide			NTV2_Stereo3DSideBySide				///< @deprecated	Use NTV2_Stereo3DSideBySide instead.
	#define		NTV2K2_Stereo3DTopBottom			NTV2_Stereo3DTopBottom				///< @deprecated	Use NTV2_Stereo3DTopBottom instead.
	#define		NTV2K2_Stereo3DDualStream			NTV2_Stereo3DDualStream				///< @deprecated	Use NTV2_Stereo3DDualStream instead.

	//	NTV2ColorSpaceMode
	#define		NTV2K2_ColorSpaceAuto				NTV2_ColorSpaceModeAuto				///< @deprecated	Use NTV2_ColorSpaceModeAuto instead.
	#define		NTV2K2_ColorSpaceYCbCr				NTV2_ColorSpaceModeYCbCr			///< @deprecated	Use NTV2_ColorSpaceModeYCbCr instead.
	#define		NTV2K2_ColorSpaceRGB				NTV2_ColorSpaceModeRGB				///< @deprecated	Use NTV2_ColorSpaceModeRGB instead.

#endif	//	!defined (NTV2_DEPRECATE)

#endif //NTV2ENUMS_H
