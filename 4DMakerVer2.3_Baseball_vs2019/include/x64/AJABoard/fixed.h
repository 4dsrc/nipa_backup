/*
  Fixed.h

  Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.

  Useful Fixed Pt routines. Assuming 16 bits of
  fraction
  */
#ifndef AJAFIXED_H
#define AJAFIXED_H

#include "ajatypes.h"

#define FIXED_ONE (1<<16)

#ifdef  MSWindows
//Visual Studio 2008 or lower
#if (_MSC_VER <= 1500) 
#define inline __inline
#endif
#endif

#ifdef AJAMac
// G5 + Panther just gets all pissed off if I try and use the functions below.
#ifdef FixedRound
#undef FixedRound
#endif
#ifdef FloatToFixed
#undef FloatToFixed
#endif
#ifdef FixedToFloat
#undef FixedToFloat
#endif
#define FixedTrunc(inFix) ((inFix)>>16)
#define FixedRound(inFix) (((inFix) < 0) ? (-((-(inFix)+0x8000)>>16)) :  (((inFix) + 0x8000)>>16))
#define FixedMix(min, max, mixer) (FixedRound((max-min)*(mixer)+(min)))
#define FloatToFixed(inFlt) ((Fixed_)((inFlt) * (float)FIXED_ONE))
#define FixedToFloat(inFix) (((float)(inFix)/(float)65536.0))
#define FixedFrac(inFix) (((inFix) < 0) ? (-(inFix)&0xFFFF)) :  ((inFix)&0xFFFF)

#else

// Prevent unsupported-floating link errors in Linux device driver.
// The following are not used in the Linux driver but cause the 2.6 kernel 
// linker to fail anyway.
#ifndef __KERNEL__
inline	Fixed_	FloatToFixed(float inFlt)
{ 
  return (Fixed_)(inFlt * (float)FIXED_ONE); 
}

inline	float	FixedToFloat(Fixed_ inFix)
{ 
  return((float)inFix/(float)65536.0); 
}
#endif

inline	Word	FixedRound(Fixed_ inFix)
{ 
  Word retValue;
  
  if ( inFix < 0 ) 
  {
	retValue = (Word)(-((-inFix+0x8000)>>16));
  }
  else
  {
	retValue = (Word)((inFix + 0x8000)>>16);
  }
  return retValue;
}

inline	Fixed_	FixedFrac(Fixed_ inFix)
{ 
  Fixed_ retValue;
  
  if ( inFix < 0 ) 
  {
	retValue = -inFix&0xFFFF;
  }
  else
  {
	retValue = inFix&0xFFFF;
  }
  
  return retValue;
}

inline Word FixedMix(Word min, Word max, Fixed_ mixer)
{
	Fixed_ result = (max-min)*mixer+min;

	return FixedRound(result);
}
#endif

#endif
