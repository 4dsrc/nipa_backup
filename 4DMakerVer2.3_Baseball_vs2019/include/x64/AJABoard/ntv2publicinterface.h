/**************************************************************************

	ntv2publicinterface.h

	Copyright (C) 2012 AJA Video Systems, Inc.  
	Proprietary and Confidential information.

	Purpose:

		Defines that are shared between usermode software and driver
		software

	Platform Independent

**************************************************************************/

#ifndef NTV2PUBLICINTERFACE_H
#define NTV2PUBLICINTERFACE_H

#include "ajatypes.h"
#include "ntv2enums.h"

#ifdef MSWindows
#include <basetsd.h>
#endif

typedef enum
{
	kRegGlobalControl,				// 0
	kRegCh1Control,					// 1
	kRegCh1PCIAccessFrame,			// 2
	kRegCh1OutputFrame,				// 3
	kRegCh1InputFrame,				// 4
	kRegCh2Control,					// 5
	kRegCh2PCIAccessFrame,			// 6
	kRegCh2OutputFrame,				// 7
	kRegCh2InputFrame,				// 8
	kRegVidProcControl,				// 9
	kRegVidProcXptControl,			// 10
	kRegMixerCoefficient,			// 11
	kRegSplitControl,				// 12
	kRegFlatMatteValue,				// 13
	kRegOutputTimingControl,		// 14
	kRegPanControl,					// 15
	kRegReserved2,					// 16
	kRegFlashProgramReg,			// 17
	kRegLineCount,					// 18
	kRegOutputTimingFinePhase,		// 19  (was kRegReserved3)
	kRegAud1Delay = kRegOutputTimingFinePhase, //19
	kRegVidIntControl,				// 20
	kRegStatus,						// 21
	kRegInputStatus,				// 22
	kRegAud1Detect,					// 23
	kRegAud1Control,				// 24
	kRegAud1SourceSelect,			// 25
	kRegAud1OutputLastAddr,			// 26
	kRegAud1InputLastAddr,			// 27
	kRegAud1Counter,				// 28
	kRegRP188InOut1DBB,				// 29
	kRegRP188InOut1Bits0_31,		// 30
	kRegRP188InOut1Bits32_63,		// 31
	KRegDMA1HostAddr,				// 32
	kRegDMA1LocalAddr,				// 33
	kRegDMA1XferCount,				// 34
	kRegDMA1NextDesc,				// 35
	KRegDMA2HostAddr,				// 36
	kRegDMA2LocalAddr,				// 37
	kRegDMA2XferCount,				// 38
	kRegDMA2NextDesc,				// 39
	KRegDMA3HostAddr,				// 40
	kRegDMA3LocalAddr,				// 41
	kRegDMA3XferCount,				// 42
	kRegDMA3NextDesc,				// 43
	KRegDMA4HostAddr,				// 44
	kRegDMA4LocalAddr,				// 45
	kRegDMA4XferCount,				// 46
	kRegDMA4NextDesc,				// 47
	kRegDMAControl,					// 48
	kRegDMAIntControl,				// 49
	kRegBoardID,					// 50
	kRegReserved51,					// 51	
	kRegReserved52,					// 52	
	kRegReserved53,					// 53	
	kRegReserved54,					// 54	
	kRegReserved55,					// 55	
	kRegReserved56,					// 56	
	kRegReserved57,					// 57	
	kRegXenaxFlashControlStatus,    // 58	
	kRegXenaxFlashAddress,			// 59	
	kRegXenaxFlashDIN,				// 60	
	kRegXenaxFlashDOUT,				// 61	
	kRegReserved62,					// 62	
	kRegReserved63,					// 63	
	kRegRP188InOut2DBB,				// 64
	kRegRP188InOut2Bits0_31,		// 65
	kRegRP188InOut2Bits32_63,		// 66
	kRegReserved67,					// 67
	kRegCh1ColorCorrectioncontrol,	// 68
	kRegCh2ColorCorrectioncontrol,	// 69
	kRegRS422Transmit,				// 70
	kRegRS422Receive,				// 71
	kRegRS422Control,				// 72
	kRegReserved73,					// 73
	kRegReserved74,					// 74
	kRegReserved75,					// 75
	kRegReserved76,					// 76
	kRegReserved77,					// 77
	kRegReserved78,					// 78
	kRegReserved79,					// 79

	kRegReserved80,					// 80
	kRegAnalogInputStatus,			// 81
	kRegAnalogInputControl,			// 82
	kRegReserved83,					// 83
	kRegFS1ProcAmpC1Y_C1CB,			// 84
	kRegFS1ProcAmpC1CR_C2CB,		// 85
	kRegFS1ProcAmpC2CROffsetY,		// 86
	kRegFS1AudioDelay,				// 87
	kRegAud2Delay = kRegFS1AudioDelay, //87
	kRegAuxInterruptDelay,			// 88
	kRegReserved89,					// 89
	
	kRegFS1I2CControl,				// 90
	kRegFS1I2C1Address,				// 91
	kRegFS1I2C1Data,				// 92
	kRegFS1I2C2Address,				// 93
	kRegFS1I2C2Data,				// 94
	kRegFS1ReferenceSelect,			// 95
	kRegAverageAudioLevelChan1_2,	// 96
	kRegAverageAudioLevelChan3_4,	// 97
	kRegAverageAudioLevelChan5_6,	// 98
	kRegAverageAudioLevelChan7_8,	// 99

	kRegDMA1HostAddrHigh,			// 100
	kRegDMA1NextDescHigh,			// 101
	kRegDMA2HostAddrHigh,			// 102
	kRegDMA2NextDescHigh,			// 103
	kRegDMA3HostAddrHigh,			// 104
	kRegDMA3NextDescHigh,			// 105

	kRegDMA4HostAddrHigh,			// 106
	kRegDMA4NextDescHigh,			// 107
	kRegReserved108,				// 108
	kRegTestPatternGenerator = kRegReserved108, // Borg
	kRegReserved109,				// 109
	kRegLANCAndLensTap = kRegReserved109, // Borg	//Note:  Lens Tap is no longer supported by software. ref bug 3342. 4/5/2012
	kRegLTCEmbeddedBits0_31,        // 110
	kRegLTCEmbeddedBits32_63,       // 111
	kRegLTCAnalogBits0_31,			// 112
	kRegLTCAnalogBits32_63,			// 113
	
	// temporary compatibility mode for Borg
	kRegLTCOutBits0_31 = kRegLTCEmbeddedBits0_31,		//110
	kRegLTCOutBits32_63 = kRegLTCEmbeddedBits32_63,		//111
	kRegLTCInBits0_31 = kRegLTCAnalogBits0_31,			//112
	kRegLTCInBits32_63 = kRegLTCAnalogBits32_63,		//113

	kRegReserved114,				// 114
	kRegReserved115,				// 115
	kRegSysmonControl,				// 116
	kRegSysmonConfig1_0,			// 117
	kRegSysmonConfig2,				// 118
	kRegSysmonVccIntDieTemp,		// 119

	kRegInternalExternalVoltage,	// 120
	kRegFlashProgramReg2,			// 121
	kRegHDMIOut3DStatus1,			// 122
	kRegHDMIOut3DStatus2,			// 123
	kRegHDMIOut3DControl,			// 124
	kRegHDMIOutControl,				// 125
	kRegHDMIInputStatus,			// 126
	kRegHDMIInputControl,			// 127
	kK2RegAnalogOutControl,			// 128
	kK2RegSDIOut1Control,			// 129

	kK2RegSDIOut2Control,			// 130
	kK2RegConversionControl,		// 131
	kK2RegFrameSync1Control,		// 132
	kRegI2CWriteData,				// 133
	kK2RegFrameSync2Control,		// 134
	kRegI2CWriteControl,			// 135
	kK2RegXptSelectGroup1,			// 136
	kK2RegXptSelectGroup2,			// 137
	kK2RegXptSelectGroup3,			// 138
	kK2RegXptSelectGroup4,			// 139
	kK2RegXptSelectGroup5,			// 140
	kK2RegXptSelectGroup6,			// 141

	kK2RegCSCoefficients1_2,		// 142
	kK2RegCSCoefficients3_4,		// 143
	kK2RegCSCoefficients5_6,		// 144
	kK2RegCSCoefficients7_8,		// 145
	kK2RegCSCoefficients9_10,		// 146

	kK2RegCS2Coefficients1_2,		// 147
	kK2RegCS2Coefficients3_4,		// 148
	kK2RegCS2Coefficients5_6,		// 149
	kK2RegCS2Coefficients7_8,		// 150
	kK2RegCS2Coefficients9_10,		// 151

	kRegField1Line21CaptionDecode,	// 152		//	OBSOLETE
	kRegField2Line21CaptionDecode,	// 153		//	OBSOLETE
	kRegField1Line21CaptionEncode,	// 154		//	OBSOLETE
	kRegField2Line21CaptionEncode,	// 155		//	OBSOLETE
	kRegVANCGrabberSetup,			// 156		//	OBSOLETE
	kRegVANCGrabberStatus1,		    // 157		//	OBSOLETE
	kRegVANCGrabberStatus2,		    // 158		//	OBSOLETE
	kRegVANCGrabberDataBuffer,		// 159		//	OBSOLETE
	kRegVANCInserterSetup1,			// 160		//	OBSOLETE
	kRegVANCInserterSetup2,			// 161		//	OBSOLETE
	kRegVANCInserterDataBuffer,		// 162		//	OBSOLETE
	
	kK2RegXptSelectGroup7,			// 163
	kK2RegXptSelectGroup8,			// 164
	kRegCh1ControlExtended,			// 165
	kRegCh2ControlExtended,			// 166
	kRegAFDVANCGrabber,			    // 167
	kRegFS1DownConverter2Control,	// 168
	kK2RegSDIOut3Control,			// 169
	kK2RegSDIOut4Control,			// 170
	kRegAFDVANCInserterSDI1,		// 171
	kRegAFDVANCInserterSDI2,		// 172
    kRegAudioChannelMappingCh1,     // 173
    kRegAudioChannelMappingCh2,     // 174
    kRegAudioChannelMappingCh3,     // 175
    kRegAudioChannelMappingCh4,     // 176
    kRegAudioChannelMappingCh5,     // 177
    kRegAudioChannelMappingCh6,     // 178
    kRegAudioChannelMappingCh7,     // 179
    kRegAudioChannelMappingCh8,     // 180
	
	kRegReserved181,				// 181
	kRegReserved182,				// 182
	kRegReserved183,				// 183
	kRegReserved184,				// 184
	kRegReserved185,				// 185
	kRegReserved186,				// 186
	kRegReserved187,				// 187
	kRegVideoPayloadIDLinkA,		// 188
	kRegVideoPayloadIDLinkB,		// 189
	kRegSDIIn1VPIDA = kRegVideoPayloadIDLinkA,
	kRegSDIIn1VPIDB = kRegVideoPayloadIDLinkB,
	kRegAudioOutputSourceMap,		// 190
	kK2RegXptSelectGroup11,			// 191
	kRegStereoCompressor,			// 192
	kK2RegXptSelectGroup12,			// 193
	kRegFrameApertureOffset,		// 194
	kRegReserved195,				// 195
	kRegReserved196,				// 196
	kRegReserved197,				// 197
	kRegReserved198,				// 198
	kRegReserved199,				// 199
	kRegReserved200,				// 200
	kRegReserved201,				// 201
	kRegReserved202,				// 202
	kRegReserved203,				// 203
	kRegReserved204,				// 204
	kRegReserved205,				// 205
	kRegReserved206,				// 206
	kRegReserved207,				// 207
	kRegReserved208,				// 208
	kRegReserved209,				// 209
	kRegReserved210,				// 210
	kRegReserved211,				// 211
	kRegReserved212,				// 212
	kRegReserved213,				// 213
	kRegReserved214,				// 214
	kRegReserved215,				// 215
	kRegReserved216,				// 216
	kRegReserved217,				// 217
	kRegReserved218,				// 218
	kRegReserved219,				// 219
	kRegReserved220,				// 220
	kRegReserved221,				// 221
	kRegReserved222,				// 222
	kRegReserved223,				// 223
	kRegReserved224,				// 224
	kRegReserved225,				// 225
	kRegReserved226,				// 226
	kRegReserved227,				// 227
	kRegReserved228,				// 228
	kRegReserved229,				// 229
	kRegReserved230,				// 230
	kRegReserved231,				// 231
	kRegSDIInput3GStatus,			// 232
	kRegLTCStatusControl,			// 233
	kRegSDIOut1VPIDA,				// 234
	kRegSDIOut1VPIDB,				// 235
	kRegSDIOut2VPIDA,				// 236
	kRegSDIOut2VPIDB,				// 237
	kRegSDIIn2VPIDA,				// 238
	kRegSDIIn2VPIDB,				// 239
	kRegAud2Control,				// 240
	kRegAud2SourceSelect,			// 241
	kRegAud2OutputLastAddr,			// 242
	kRegAud2InputLastAddr,			// 243
	kRegRS4222Transmit,				// 244
	kRegRS4222Receive,				// 245
	kRegRS4222Control,				// 246
	kRegVidProc2Control,			// 247
	kRegMixer2Coefficient,			// 248
	kRegFlatMatte2Value,			// 249
	kK2RegXptSelectGroup9,			// 250
	kK2RegXptSelectGroup10,			// 251
	kRegLTC2EmbeddedBits0_31,       // 252
	kRegLTC2EmbeddedBits32_63,      // 253
	kRegLTC2AnalogBits0_31,			// 254
	kRegLTC2AnalogBits32_63,		// 255
	kRegSDITransmitControl,			// 256
	kRegCh3Control,					// 257
	kRegCh3OutputFrame,				// 258
	kRegCh3InputFrame,				// 259
	kRegCh4Control,					// 260
	kRegCh4OutputFrame,				// 261
	kRegCh4InputFrame,				// 262
	kK2RegXptSelectGroup13,			// 263
	kK2RegXptSelectGroup14,			// 264
	kRegStatus2,					// 265
	kRegVidIntControl2,				// 266
	kRegGlobalControl2,				// 267
	kRegRP188InOut3DBB,				// 268
	kRegRP188InOut3Bits0_31,		// 269
	kRegRP188InOut3Bits32_63,		// 270
	kRegSDIOut3VPIDA,				// 271
	kRegSDIOut3VPIDB,				// 272
	kRegRP188InOut4DBB,				// 273
	kRegRP188InOut4Bits0_31,		// 274
	kRegRP188InOut4Bits32_63,		// 275
	kRegSDIOut4VPIDA,				// 276
	kRegSDIOut4VPIDB,				// 277
	kRegAud3Control,				// 278
	kRegAud4Control,				// 279
	kRegAud3SourceSelect,			// 280
	kRegAud4SourceSelect,			// 281
	kRegAudDetect2,					// 282
	kRegAud3OutputLastAddr,			// 283
	kRegAud3InputLastAddr,			// 284
	kRegAud4OutputLastAddr,			// 285
	kRegAud4InputLastAddr,			// 286
	kRegSDIInput3GStatus2,			// 287
	kRegInputStatus2,				// 288
	kRegCh3PCIAccessFrame,			// 289
	kRegCh4PCIAccessFrame,			// 290

	kK2RegCS3Coefficients1_2,		// 291
	kK2RegCS3Coefficients3_4,		// 292
	kK2RegCS3Coefficients5_6,		// 293
	kK2RegCS3Coefficients7_8,		// 294
	kK2RegCS3Coefficients9_10,		// 295

	kK2RegCS4Coefficients1_2,		// 296
	kK2RegCS4Coefficients3_4,		// 297
	kK2RegCS4Coefficients5_6,		// 298
	kK2RegCS4Coefficients7_8,		// 299
	kK2RegCS4Coefficients9_10,		// 300

	kK2RegXptSelectGroup17,			// 301
	kK2RegXptSelectGroup15,			// 302
	kK2RegXptSelectGroup16,			// 303

	kRegAud3Delay,					// 304
	kRegAud4Delay,					// 305

	kRegSDIIn3VPIDA,				// 306
	kRegSDIIn3VPIDB,				// 307
	kRegSDIIn4VPIDA,				// 308
	kRegSDIIn4VPIDB,				// 309

	kRegSDIWatchdogControlStatus,	// 310
	kRegSDIWatchdogTimeout,			// 311
	kRegSDIWatchdogKick1,			// 312
	kRegSDIWatchdogKick2,			// 313
	kRegReserved314,				// 314
	kRegReserved315,				// 315

	kRegLTC3EmbeddedBits0_31,       // 316
	kRegLTC3EmbeddedBits32_63,      // 317

	kRegLTC4EmbeddedBits0_31,       // 318
	kRegLTC4EmbeddedBits32_63,      // 319

	kRegReserved320,				// 320
	kRegReserved321,				// 321
	kRegReserved322,				// 322
	kRegReserved323,				// 323
	kRegReserved324,				// 324
	kRegReserved325,				// 325
	kRegReserved326,				// 326
	kRegReserved327,				// 327
	kRegReserved328,				// 328

	kRegHDMITimeCode,				// 329
	kRegHDMIReserved330,			// 330
	kRegHDMIReserved331,			// 331
	kRegHDMIReserved332,			// 332
	kRegHDMIReserved333,			// 333
	kRegHDMIReserved334,			// 334
	kRegHDMIReserved335,			// 335
	kRegHDMIReserved336,			// 336
	kK2RegSDIOut5Control,			// 337

	kRegSDIOut5VPIDA,				// 338
	kRegSDIOut5VPIDB,				// 339

	kRegRP188InOut5Bits0_31,		// 340
	kRegRP188InOut5Bits32_63,		// 341
	kRegRP188InOut5DBB,				// 342

	kRegReserved343,				// 343

	kRegLTC5EmbeddedBits0_31,       // 344
	kRegLTC5EmbeddedBits32_63,      // 345

	kRegDL5Control,					// 346

	kK2RegCS5Coefficients1_2,		// 347
	kK2RegCS5Coefficients3_4,		// 348
	kK2RegCS5Coefficients5_6,		// 349
	kK2RegCS5Coefficients7_8,		// 350
	kK2RegCS5Coefficients9_10,		// 351

	kK2RegXptSelectGroup18,			// 352

	kRegReserved353,				// 353
	
	kRegDC1,						// 354
	kRegDC2,						// 355
	kK2RegXptSelectGroup19,			// 356

	kK2RegXptSelectGroup20,			// 357
	kRegRasterizerControl,			// 358

	//HDMI V2 In Registers
	kRegHDMIV2I2C1Control,			// 359
	kRegHDMIV2I2C1Data,				// 360
	kRegHDMIV2VideoSetup,			// 361
	kRegHDMIV2HSyncDurationAndBackPorch,	// 362
	kRegHDMIV2HActive,				// 363
	kRegHDMIV2VSyncDurationAndBackPorchField1,	// 364
	kRegHDMIV2VSyncDurationAndBackPorchField2,	// 365
	kRegHDMIV2VActiveField1,		// 366
	kRegHDMIV2VActiveField2,		// 367
	kRegHDMIV2VideoStatus,			// 368
	kRegHDMIV2HorizontalMeasurements,		// 369
	kRegHDMIV2HBlankingMeasurements,		// 370
	kRegHDMIV2HBlankingMeasurements1,		// 371
	kRegHDMIV2VerticalMeasurementsField0,	// 372
	kRegHDMIV2VerticalMeasurementsField1,	// 373
	kRegHDMIV2i2c2Control,			// 374
	kRegHDMIV2i2c2Data,				// 375

	kRegLUTV2Control,				// 376

	//Scott: New Dax/Multi-channel Registers
	kRegGlobalControlCh2,			// 377
	kRegGlobalControlCh3,			// 378
	kRegGlobalControlCh4,			// 379
	kRegGlobalControlCh5,			// 380
	kRegGlobalControlCh6,			// 381
	kRegGlobalControlCh7,			// 382
	kRegGlobalControlCh8,			// 383

	kRegCh5Control,					// 384
	kRegCh5OutputFrame,				// 385
	kRegCh5InputFrame,				// 386
	kRegCh5PCIAccessFrame,			// 387

	kRegCh6Control,					// 388
	kRegCh6OutputFrame,				// 389
	kRegCh6InputFrame,				// 390
	kRegCh6PCIAccessFrame,			// 391

	kRegCh7Control,					// 392
	kRegCh7OutputFrame,				// 393
	kRegCh7InputFrame,				// 394
	kRegCh7PCIAccessFrame,			// 395

	kRegCh8Control,					// 396
	kRegCh8OutputFrame,				// 397
	kRegCh8InputFrame,				// 398
	kRegCh8PCIAccessFrame,			// 399

	kRegXptSelectGroup21,			// 400
	kRegXptSelectGroup22,			// 401
	kRegXptSelectGroup30,			// 402
	kRegXptSelectGroup23,			// 403
	kRegXptSelectGroup24,			// 404
	kRegXptSelectGroup25,			// 405
	kRegXptSelectGroup26,			// 406
	kRegXptSelectGroup27,			// 407
	kRegXptSelectGroup28,			// 408
	kRegXptSelectGroup29,			// 409

	kRegSDIIn5VPIDA,				// 410
	kRegSDIIn5VPIDB,				// 411

	kRegSDIIn6VPIDA,				// 412
	kRegSDIIn6VPIDB,				// 413
	kRegSDIOut6VPIDA,				// 414
	kRegSDIOut6VPIDB,				// 415
	kRegRP188InOut6Bits0_31,		// 416
	kRegRP188InOut6Bits32_63,		// 417
	kRegRP188InOut6DBB,				// 418
	kRegLTC6EmbeddedBits0_31,		// 419
	kRegLTC6EmbeddedBits32_63,		// 420

	kRegSDIIn7VPIDA,				// 421
	kRegSDIIn7VPIDB,				// 422
	kRegSDIOut7VPIDA,				// 423
	kRegSDIOut7VPIDB,				// 424
	kRegRP188InOut7Bits0_31,		// 425
	kRegRP188InOut7Bits32_63,		// 426
	kRegRP188InOut7DBB,				// 427
	kRegLTC7EmbeddedBits0_31,		// 428
	kRegLTC7EmbeddedBits32_63,		// 429

	kRegSDIIn8VPIDA,				// 430
	kRegSDIIn8VPIDB,				// 431
	kRegSDIOut8VPIDA,				// 432
	kRegSDIOut8VPIDB,				// 433
	kRegRP188InOut8Bits0_31,		// 434
	kRegRP188InOut8Bits32_63,		// 435
	kRegRP188InOut8DBB,				// 436
	kRegLTC8EmbeddedBits0_31,		// 437
	kRegLTC8EmbeddedBits32_63,		// 438

	kRegXptSelectGroup31,			// 439

	kRegAud5Control,				// 440
	kRegAud5SourceSelect,			// 441
	kRegAud5OutputLastAddr,			// 442
	kRegAud5InputLastAddr,			// 443

	kRegAud6Control,				// 444
	kRegAud6SourceSelect,			// 445
	kRegAud6OutputLastAddr,			// 446
	kRegAud6InputLastAddr,			// 447

	kRegAud7Control,				// 448
	kRegAud7SourceSelect,			// 449
	kRegAud7OutputLastAddr,			// 450
	kRegAud7InputLastAddr,			// 451

	kRegAud8Control,				// 452
	kRegAud8SourceSelect,			// 453
	kRegAud8OutputLastAddr,			// 454
	kRegAud8InputLastAddr,			// 455

	kRegAudioDetect5678,			// 456

	kRegSDI5678Input3GStatus,		// 457
	
	kRegInput56Status,				// 458
	kRegInput78Status,				// 459

	kK2RegCS6Coefficients1_2,			// 460
	kK2RegCS6Coefficients3_4,			// 461
	kK2RegCS6Coefficients5_6,			// 462
	kK2RegCS6Coefficients7_8,			// 463
	kK2RegCS6Coefficients9_10,		// 464

	kK2RegCS7Coefficients1_2,			// 465
	kK2RegCS7Coefficients3_4,			// 466
	kK2RegCS7Coefficients5_6,			// 467
	kK2RegCS7Coefficients7_8,			// 468
	kK2RegCS7Coefficients9_10,		// 469

	kK2RegCS8Coefficients1_2,			// 470
	kK2RegCS8Coefficients3_4,			// 471
	kK2RegCS8Coefficients5_6,			// 472
	kK2RegCS8Coefficients7_8,			// 473
	kK2RegCS8Coefficients9_10,		// 474

	kK2RegSDIOut6Control,			// 475
	kK2RegSDIOut7Control,			// 476
	kK2RegSDIOut8Control,			// 477

	kRegOutputTimingControlch2,		// 478
	kRegOutputTimingControlch3,		// 479
	kRegOutputTimingControlch4,		// 480
	kRegOutputTimingControlch5,		// 481
	kRegOutputTimingControlch6,		// 482
	kRegOutputTimingControlch7,		// 483
	kRegOutputTimingControlch8,		// 484

	kRegVidProc3Control,			// 485
	kRegMixer3Coefficient,			// 486
	kRegFlatMatte3Value,			// 487

	kRegVidProc4Control,			// 488
	kRegMixer4Coefficient,			// 489
	kRegFlatMatte4Value,			// 490

	kRegTRSErrorStatus,				// 491

	kRegAud5Delay,					// 492
	kRegAud6Delay,					// 493
	kRegAud7Delay,					// 494
	kRegAud8Delay,					// 495

	kRegPCMControl4321,				// 496
	kRegPCMControl8765,				// 497

	kRegCh1Control2MFrame,			// 498
	kRegCh2Control2MFrame,			// 499
	kRegCh3Control2MFrame,			// 500
	kRegCh4Control2MFrame,			// 501
	kRegCh5Control2MFrame,			// 502
	kRegCh6Control2MFrame,			// 503
	kRegCh7Control2MFrame,			// 504
	kRegCh8Control2MFrame,			// 505
	kRegXptSelectGroup32,			// 506
	kRegXptSelectGroup33,			// 507
	kRegXptSelectGroup34,			// 508
	kRegXptSelectGroup35,			// 509
	kRegReserved510,				// 510
	kRegReserved511,				// 511

	kRegNumRegisters
#if !defined (NTV2_DEPRECATE)
	, kRegAudDetect					=	kRegAud1Detect,				//	@deprecated		Use kRegAud1Detect instead.
	kRegAudControl					=	kRegAud1Control,			//	@deprecated		Use kRegAud1Control instead.
	kRegAudSourceSelect				=	kRegAud1SourceSelect,		//	@deprecated		Use kRegAud1SourceSelect instead.
	kRegAudOutputLastAddr			=	kRegAud1OutputLastAddr,		//	@deprecated		Use kRegAud1OutputLastAddr instead.
	kRegAudInputLastAddr			=	kRegAud1InputLastAddr,		//	@deprecated		Use kRegAud1InputLastAddr instead.
	kRegAudCounter					=	kRegAud1Counter				//	@deprecated		Use kRegAud1Counter instead.
#endif	//	NTV2_DEPRECATE
} RegisterNum;


#if !defined (NTV2_DEPRECATE)
	// Special Registers for the XENAX
	#define XENAX_REG_START 256
	#define XENAX_NUM_REGS  (119+36+41)
	#define NUM_HW_REGS     (XENAX_REG_START + XENAX_NUM_REGS)

	// Registers unique to a particular device with their own address space
	// These are 16-bit registers
	#define BORG_FUSION_REG_START 8000

	typedef enum
	{
		kRegBorgFusionBootFPGAVerBoardID = BORG_FUSION_REG_START, // 0
		kRegBorgFusionFPGAConfigCtrl,			// 1
		kRegBorgFusionPanelPushButtonDebounced,	// 2
		kRegBorgFusionPanelPushButtonChanges,	// 3
		kRegBorgFusionLEDPWMThreshholds,		// 4
		kRegBorgFusionPowerCtrl,				// 5
		kRegBorgFusionIRQ3nIntCtrl,				// 6
		kRegBorgFusionIRQ3nIntSrc,				// 7
		kRegBorgFusionDisplayCtrlStatus,		// 8
		kRegBorgFusionDisplayWriteData,			// 9
		kRegBorgFusionAnalogFlags,				// 10
		kRegBorgFusionReserved11,				// 11 
		kRegBonesActelCFSlots = kRegBorgFusionReserved11,			// Compact Flash S1 & S2 status
		kRegBorgFusionReserved12,				// 12
		kRegBonesActelCFSlotsChanges = kRegBorgFusionReserved12,	// Compact Flash Slot Changes Present
		kRegBorgFusionReserved13,				// 13
		kRegBorgFusionReserved14,				// 14
		kRegBorgFusionReserved15,				// 15
		kRegBorgFusionTempSensor1,				// 16
		kRegBorgFusionTempSensor2,				// 17
		kRegBorgFusion5_0vPowerRail,			// 18: +5.0 v power rail, 8mV res
		kRegBorgFusion2_5vPowerRail,			// 19: +2.5v power rail, 4mV res
		kRegBorgFusion1_95vPowerRail,			// 20: +1.95v power rail, 4mV res
		kRegBorgFusion1_8vPowerRail,			// 21: +1.8v power rail, 2mV res
		kRegBorgFusion1_5vPowerRail,			// 22: +1.5v power rail, 2mV res
		kRegBorgFusion1_0vPowerRail,			// 23: +1.0v power rail, 2mV res
		kRegBorgFusion12vPowerRail,				// 24: +12v input power, 8mV res
		kRegBorgFusion3_3vPowerRail,			// 25: +3.3v power rail, 4mV res

		kRegBorgFusionProdIdLo = BORG_FUSION_REG_START + 50,// 50, lo 16b of kRegBoardID data
		kRegBorgFusionProdIdHi,								// 51, hi 16b of kRegBoardID data
		kRegBorgFusionNumRegistersDummy,
		kRegBorgFusionNumRegisters = kRegBorgFusionNumRegistersDummy - BORG_FUSION_REG_START + 1
	} BorgFusionRegisterNum;

	#define DNX_REG_START 8100

	typedef enum
	{
		// The paragraph numbers following the Block and Section register
		// descriptions refer to the "KiProMini: DNX Codec" document
		// originally written by Avid and adapted by Paul Greaves.

		// Block 1.1: Nestor Common
		kRegDNX_DeviceID = DNX_REG_START,       // ro (read only)
		kRegDNX_Revision,                       // ro
		kRegDNX_Diagnostics,
		kRegDNX_Reset,
		kRegDNX_NestorControl,
		kRegDNX_NestorAutoloadControl,
		kRegDNX_Status,
		kRegDNX_NestorInterrupt,
		kRegDNX_NestorInterruptEnable,
		kRegDNX_EncoderM1AStatus,               // ro (Was Flash Addr)
		kRegDNX_EncoderM2AStatus,               // ro (Was Flash Data)
		kRegDNX_DecoderM1BStatus,               // ro (Was Temp Sensor Addr Ctl)
		kRegDNX_DecoderM2BStatus,               // ro (Was Temp Sensor Wr Ctl)
		kRegDNX_TempSensorReadData,             // ro (Unused)
		kRegDNX_Timeout,

		// Block 1.2: SMPTE Format
		kRegDNX_Field1StartEnd,
		kRegDNX_Field1ActiveStartEnd,
		kRegDNX_Field2StartEnd,
		kRegDNX_Field2ActiveStartEnd,
		kRegDNX_HorizontalStartEnd,
		kRegDNX_FormatChromaClipping,
		kRegDNX_FormatLumaClipping,
		kRegDNX_Reserved1,

		// Section 1.2.9: Formatter
		kRegDNX_A0Parameter,
		kRegDNX_A1Parameter,
		kRegDNX_A2Parameter,
		kRegDNX_A3Parameter,
		kRegDNX_A4Parameter,
		kRegDNX_A5Parameter,
		kRegDNX_A6Parameter,
		kRegDNX_A7Parameter,
		kRegDNX_A8Parameter,
		kRegDNX_A9Parameter,
		kRegDNX_TOFHorizontalResetValue,
		kRegDNX_TOFVerticalResetValue,
		kRegDNX_HorizontalStartOfHANCCode,
		kRegDNX_HorizontalStartOfSAVCode,
		kRegDNX_HorizontalStartOfActiveVideo,
		kRegDNX_HorizontalEndOfLine,

		// Block 1.5: Encoder
		kRegDNX_EncoderInterrupt,
		kRegDNX_EncoderControl,
		kRegDNX_EncoderInterruptEnable,
		kRegDNX_RateControlAddress,
		kRegDNX_RateControlReadData,            // ro
		kRegDNX_MacroblockLineNumber,           // ro
		kRegDNX_RateControlIndex,               // ro
		kRegDNX_DCTPackerIndex,                 // ro
		kRegDNX_EncodeTableWrite,
		kRegDNX_EncoderDebug,                   // ro

		// Section 1.5.11: The VCID used for encoding
		kRegDNX_EncoderVCIDRegister,

		// Section 1.5.12.1/2: Encoder Parameter RAM
		// We are only supporting seven VCID
		// types at this time.
		kRegDNX_EncoderParameterRAMLocation0_0,
		kRegDNX_EncoderParameterRAMLocation1_0,

		kRegDNX_EncoderParameterRAMLocation0_1,
		kRegDNX_EncoderParameterRAMLocation1_1,

		kRegDNX_EncoderParameterRAMLocation0_2,
		kRegDNX_EncoderParameterRAMLocation1_2,

		kRegDNX_EncoderParameterRAMLocation0_3,
		kRegDNX_EncoderParameterRAMLocation1_3,

		kRegDNX_EncoderParameterRAMLocation0_4,
		kRegDNX_EncoderParameterRAMLocation1_4,

		kRegDNX_EncoderParameterRAMLocation0_5,
		kRegDNX_EncoderParameterRAMLocation1_5,

		kRegDNX_EncoderParameterRAMLocation0_6,
		kRegDNX_EncoderParameterRAMLocation1_6,

		// Block 1.6: Decoder
		kRegDNX_DecoderInterrupt,
		kRegDNX_DecoderControl,
		kRegDNX_DecoderInterruptEnable,
		kRegDNX_DecodeTime,                     // ro
		kRegDNX_FrameCount,                     // ro
		kRegDNX_DecodeTableWrite,
		kRegDNX_DecoderDebug,                   // ro
		kRegDNX_Pipe1StallStatus1,              // ro
		kRegDNX_Pipe1StallStatus2,              // ro
		kRegDNX_Pipe2StallStatus1,              // ro
		kRegDNX_Pipe2StallStatus2,              // ro

		// Section 1.6.12: The VCID to use regardless of the frame header
		kRegDNX_DecoderVCIDRegister,

		// Section 1.6.13: Decoder Parameter RAM
		kRegDNX_DecoderParameterRAMLocation0_0,
		kRegDNX_DecoderParameterRAMLocation1_0,

		kRegDNX_DecoderParameterRAMLocation0_1,
		kRegDNX_DecoderParameterRAMLocation1_1,

		kRegDNX_DecoderParameterRAMLocation0_2,
		kRegDNX_DecoderParameterRAMLocation1_2,

		kRegDNX_DecoderParameterRAMLocation0_3,
		kRegDNX_DecoderParameterRAMLocation1_3,

		kRegDNX_DecoderParameterRAMLocation0_4,
		kRegDNX_DecoderParameterRAMLocation1_4,

		kRegDNX_DecoderParameterRAMLocation0_5,
		kRegDNX_DecoderParameterRAMLocation1_5,

		kRegDNX_DecoderParameterRAMLocation0_6,
		kRegDNX_DecoderParameterRAMLocation1_6,

		kRegDNX_MaximumRegister   = kRegDNX_DecoderParameterRAMLocation1_6,
		kRegDNX_NumberOfRegisters = ((kRegDNX_MaximumRegister - DNX_REG_START) + 1)
	} DNXRegisterNum;
#endif	//	!defined (NTV2_DEPRECATE)


// Virtual registers
#include "ntv2virtualregisters.h"

typedef struct
{
	ULWord initialized;
	ULWord contrast;
	ULWord brightness;
	ULWord hue;
	ULWord CbOffset;		// Not user controllable
	ULWord CrOffset;		// Not user controllable
	ULWord saturationCb;
	ULWord saturationCr;
} VirtualProcAmpRegisters_base;

typedef struct
{
	VirtualProcAmpRegisters_base SD;
	VirtualProcAmpRegisters_base HD;
} VirtualProcAmpRegisters;

// These have a nice mapping to the virtual registers
typedef struct
{
	UByte contrast;
	UByte brightness;
	UByte hue;
	UByte CbOffset;		// Not user controllable
	UByte CrOffset;		// Not user controllable
	UByte saturationCb;
	UByte saturationCr;
} ADV7189BProcAmpRegisters;	// Works for SD portion of ADV7402A also

// These do not have a nice mapping to the virtual registers
// All are 10-bit registers spread out over two I2C addresses.
typedef struct
{
	UByte hex73;	// [7:6] set, [5:0] upper bits contrast
	UByte hex74;	// [7:4] lower bits contrast, [3:0] upper bits saturation Cb
	UByte hex75;	// [7:2] lower bits saturation Cb, [1:0] upper bits saturation Cr
	UByte hex76;	// [7:0] lower bits saturation Cr
	UByte hex77;	// [7:6] clear, [5:0] upper bits brightness.
	UByte hex78;	// [7:4] lower bits brightness, [3:0] high bits Cb offset
	UByte hex79;	// [7:2] lower bits Cb offset, [1:0] high bits Cr offset
	UByte hex7A;	// [7:0] lower bits Cr offset
} ADV7402AHDProcAmpRegisters;

// Kind of a hack
// This will have to be a union or something if/when we add another proc amp processor
typedef struct
{
	ADV7189BProcAmpRegisters	SD;	// Works for SD portion of ADV7402A also
	ADV7402AHDProcAmpRegisters	HD;	
} HardwareProcAmpRegisterImage;

// SD procamp regs
typedef enum
{
	kRegADV7189BContrast = 0x08,
	kRegADV7189BBrightness = 0x0A,
	kRegADV7189BHue = 0x0B,
	kRegADV7189BCbOffset = 0xE1,
	kRegADV7189BCrOffset = 0xE2,
	kRegADV7189BSaturationCb = 0xE3,
	kRegADV7189BSaturationCr = 0xE4
} ADV7189BRegisterNum;	// Works for SD portion of ADV7402A also

typedef enum
{
	// Global Control
	kRegMaskFrameRate			= BIT(0) + BIT(1) + BIT(2),
	kRegMaskFrameRateHiBit		= BIT(22),
	kRegMaskGeometry			= BIT(3) + BIT(4) + BIT(5) + BIT(6),
	kRegMaskStandard			= BIT(7) + BIT(8) + BIT(9),
	kRegMaskRefSource			= BIT(10) + BIT(11) + BIT(12),
	kRegMaskRefInputVoltage		= BIT(12),	// DEPRECATED! - Now part of kRegMaskRefSource - do not use on new boards
	kRegMaskSmpte372Enable		= BIT(15),
	kRegMaskLED					= BIT(16) + BIT(17) + BIT(18) + BIT(19),
	kRegMaskRegClocking			= BIT(21) + BIT(20),
	kRegMaskDualLinkInEnable	= BIT(23),
	kRegMaskBankSelect			= BIT(25) + BIT(26),
	kRegMaskDualLinkOutEnable	= BIT(27),
	kRegMaskRP188ModeCh1		= BIT(28),
	kRegMaskRP188ModeCh2		= BIT(29),
	kRegMaskCCHostBankSelect	= BIT(30) + BIT(31),

	// Global Control 2
	kRegMaskRefSource2			= BIT(0),
	kRegMaskQuadMode			= BIT(3),
	kRegMaskAud1PlayCapMode		= BIT(4),
	kRegMaskAud2PlayCapMode		= BIT(5),
	kRegMaskAud3PlayCapMode		= BIT(6),
	kRegMaskAud4PlayCapMode		= BIT(7),
	kRegMaskAud5PlayCapMode		= BIT(8),
	kRegMaskAud6PlayCapMode		= BIT(9),
	kRegMaskAud7PlayCapMode		= BIT(10),
	kRegMaskAud8PlayCapMode		= BIT(11),
	kRegMaskQuadMode2			= BIT(12),
	kRegMaskIndependentMode		= BIT(16),
	kRegMask2MFrameSupport		= BIT(17),
	kRegMask425FB12				= BIT(20),
	kRegMask425FB34				= BIT(21),
	kRegMask425FB56				= BIT(22),
	kRegMask425FB78				= BIT(23),
	kRegMaskRP188ModeCh3		= BIT(28),
	kRegMaskRP188ModeCh4		= BIT(29),
	kRegMaskRP188ModeCh5		= BIT(30),
	kRegMaskRP188ModeCh6		= BIT(31),
	kRegMaskRP188ModeCh7		= BIT(26),
	kRegMaskRP188ModeCh8		= BIT(27),


	// Channel Control - kRegCh1Control, kRegCh2Control, kRegCh3Control, kRegCh4Control
	kRegMaskMode				= BIT(0),
	kRegMaskFrameFormat			= BIT(1) + BIT(2) + BIT(3) + BIT(4),
	kRegMaskAlphaFromInput2		= BIT(5),
	kRegMaskFrameFormatHiBit	= BIT(6), // KAM
	kRegMaskChannelDisable		= BIT(7),
	kRegMaskWriteBack			= BIT(8),
	kRegMaskFrameOrientation	= BIT(10),
	kRegMaskQuarterSizeMode		= BIT(11),
	kRegMaskFrameBufferMode		= BIT(12),
	kKHRegMaskDownconvertInput	= BIT(14),
	kLSRegMaskVideoInputSelect	= BIT(15),
	kRegMaskDitherOn8BitInput	= BIT(16),
	kRegMaskQuality				= BIT(17),
	kRegMaskEncodeAsPSF			= BIT(18),
	kK2RegMaskFrameSize			= BIT(21) + BIT(20),
	kRegMaskChannelCompressed	= BIT(22),
	kRegMaskRGB8b10bCvtMode		= BIT(23),
	kRegMaskVBlankRGBRange		= BIT(24),
	kRegMaskQuality2			= BIT(25) + BIT(26),
	kRegCh1BlackOutputMask		= BIT(27),		// KiPro black output bit
	kRegMaskSonySRExpressBit	= BIT(28),
	kRegMaskFrameSizeSetBySW	= BIT(29),
	kRegMaskVidProcVANCShift	= BIT(31),

	// Video Crosspoint Control
	kRegMaskVidXptFGVideo			= BIT(0) + BIT(1) + BIT(2),
	kRegMaskVidXptBGVideo			= BIT(4) + BIT(5) + BIT(6),
	kRegMaskVidXptFGKey				= BIT(8) + BIT(9) + BIT(10),
	kRegMaskVidXptBGKey				= BIT(12) + BIT(13) + BIT(14),
	kRegMaskVidXptSecVideo			= BIT(16) + BIT(17) + BIT(18),
	
	// Video Processing Control
	kRegMaskVidProcMux1				= BIT(0) + BIT(1),
	kRegMaskVidProcMux2				= BIT(2) + BIT(3),
	kRegMaskVidProcMux3				= BIT(4) + BIT(5),
	kRegMaskVidProcMux4				= BIT(6) + BIT(7),
	kRegMaskVidProcMux5				= BIT(8) + BIT(9) + BIT(10),
	kRegMaskVidProcLimiting			= BIT(11) + BIT(12),
	
	// Xena 2 only
	kRegMaskVidProcFGMatteEnable	= BIT(18),
	kRegMaskVidProcBGMatteEnable	= BIT(19),
	kRegMaskVidProcFGControl		= BIT(20) + BIT(21),
	kRegMaskVidProcBGControl		= BIT(22) + BIT(23),
	kRegMaskVidProcMode				= BIT(24) + BIT(25),
	kRegMaskVidProcSyncFail			= BIT(27),
	kRegMaskVidProcSplitStd			= BIT(28) + BIT(29) + BIT(30),
	kRegMaskVidProcSubtitleEnable	= BIT(31),
	
	// kRegStatus
	kRegMaskHardwareVersion			= BIT(0) + BIT(1) + BIT(2) + BIT(3),
	kRegMaskFPGAVersion				= BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9)+BIT(10)+BIT(11),
	kRegMaskLTCInPresent			= BIT(17),

	// Video Interrupt Control
	kRegMaskIntEnableMask	= BIT(5) + BIT(4) + BIT(3) + BIT(2) + BIT(1) + BIT(0),

	// Audio Control
	kRegMaskCaptureEnable			= BIT(0),
	kRegMaskNumBits					= BIT(1),	// shouldn't this be BIT(2)?
	kRegMaskOutputTone				= BIT(1),
	kRegMask20BitMode				= BIT(2),
	kRegMaskLoopBack				= BIT(3),
	kRegMaskAudioTone				= BIT(7),
	kRegMaskResetAudioInput			= BIT(8),
	kRegMaskResetAudioOutput		= BIT(9),
	kRegMaskPauseAudio				= BIT(11),
    kRegMaskEmbeddedOutputMuteCh1   = BIT(12), // added for FS1
    kRegMaskEmbeddedOutputSupressCh1 = BIT(13), // added for FS1 but available on other boards
	kRegMaskEmbeddedOutputSupressCh2 = BIT(15), // added for FS1 but available on other boards
	kRegMaskNumChannels				= BIT(16),
    kRegMaskEmbeddedOutputMuteCh2   = BIT(17), // added for FS1
	kRegMaskAudioRate				= BIT(18),
	kRegMaskEncodedAudioMode		= BIT(19), // addded for FS1 but available on other boards
	kRegMaskAudio16Channel			= BIT(20),
	kRegMaskAudio8Channel			= BIT(23),
	kK2RegMaskKBoxAnalogMonitor		= BIT(24) + BIT(25),
	kK2RegMaskKBoxAudioInputSelect	= BIT(26),
	kK2RegMaskKBoxDetect			= BIT(27),
	kK2RegMaskBOCableDetect			= BIT(28),
	kK2RegMaskAudioLevel			= BIT(29),
	kFS1RegMaskAudioLevel			= BIT(29)+BIT(30),
	kK2RegResetAudioDAC				= BIT(30),
	kK2RegMaskAudioBufferSize       = BIT(31),
	kK2RegMaskAverageAudioLevel		= 0xFFFFffff,	// read the entire register

	kFS1RegMaskAudioBufferSize		= BIT(31),
	kLHRegMaskResetAudioDAC			= BIT(31),
	// FS1 output control "Freeze (last good frame) On Input Loss"
	kRegMaskLossOfInput				= BIT(11),
	
	// Audio Source Select
	kRegMaskAudioSource				= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kRegMaskEmbeddedAudioInput		= BIT(16),
	kRegMaskAudioAutoErase			= BIT(19),
	kRegMaskAnalogHDMIvsAES     	= BIT(20),
	kRegMask3GbSelect				= BIT(21),
	kRegMaskEmbeddedAudioClock		= BIT(22),
	kRegMaskEmbeddedAudioInput2		= BIT(23),
	kRegMaskAnalogAudioInGain		= BIT(24),
	kRegMaskAnalogAudioInJack		= BIT(25),

	// Input Status
	kRegMaskInput1FrameRate			= BIT(0)+BIT(1)+BIT(2),
	kRegMaskInput1Geometry			= BIT(4)+BIT(5)+BIT(6),
	kRegMaskInput1Progressive		= BIT(7),
	kRegMaskInput2FrameRate			= BIT(8)+BIT(9)+BIT(10),
	kRegMaskInput2Geometry			= BIT(12)+BIT(13)+BIT(14),
	kRegMaskInput2Progressive		= BIT(15),
	kRegMaskReferenceFrameRate		= BIT(16)+BIT(17)+BIT(18)+BIT(19),
	kRegMaskReferenceFrameLines		= BIT(20)+BIT(21)+BIT(22),
	kRegMaskReferenceProgessive		= BIT(23),
	kRegMaskAESCh12Present			= BIT(24),
	kRegMaskAESCh34Present			= BIT(25),
	kRegMaskAESCh56Present			= BIT(26),
	kRegMaskAESCh78Present			= BIT(27),
	kRegMaskInput1FrameRateHigh		= BIT(28),
	kRegMaskInput2FrameRateHigh		= BIT(29),
	kRegMaskInput1GeometryHigh		= BIT(30),
	kRegMaskInput2GeometryHigh		= BIT(31),

	// Pan (2K crop) - Xena 2
	kRegMaskPanMode			= BIT(30) + BIT(31),
	kRegMaskPanOffsetH		= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9)+BIT(10)+BIT(11),
	kRegMaskPanOffsetV		= BIT(12)+BIT(13)+BIT(14)+BIT(15)+BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),

	// RP-188 Source
	kRegMaskRP188SourceSelect = BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),
	
    // DMA Control
    kRegMaskForce64         = BIT(4),
    kRegMaskAutodetect64    = BIT(5),
	kRegMaskFirmWareRev		= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kRegMaskDMAPauseDisable = BIT(16),

	// Color Correction Control
	kRegMaskSaturationValue = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9),
	kRegMaskCCOutputBankSelect    = BIT(16),
	kRegMaskCCMode          = BIT(17)+BIT(18),
	kRegMaskCC5HostAccessBankSelect = BIT(20),
	kRegMaskCC5OutputBankSelect = BIT(21),
	kRegMaskLUT5Select		= BIT(28),
	kRegMaskLUTSelect		= BIT(29),
	kRegMaskCC3OutputBankSelect	= BIT(30),
	kRegMaskCC4OutputBankSelect = BIT(31),

	// kRegLUTV2Control
	kRegMaskLUT1Enable					= BIT(0),
	kRegMaskLUT2Enable					= BIT(1),
	kRegMaskLUT3Enable					= BIT(2),
	kRegMaskLUT4Enable					= BIT(3),
	kRegMaskLUT5Enable					= BIT(4),
	kRegMaskLUT6Enable					= BIT(5),
	kRegMaskLUT7Enable					= BIT(6),
	kRegMaskLUT8Enable					= BIT(7),
	kRegMaskLUT1HostAccessBankSelect	= BIT(8),
	kRegMaskLUT2HostAccessBankSelect	= BIT(9),
	kRegMaskLUT3HostAccessBankSelect	= BIT(10),
	kRegMaskLUT4HostAccessBankSelect	= BIT(11),
	kRegMaskLUT5HostAccessBankSelect	= BIT(12),
	kRegMaskLUT6HostAccessBankSelect	= BIT(13),
	kRegMaskLUT7HostAccessBankSelect	= BIT(14),
	kRegMaskLUT8HostAccessBankSelect	= BIT(15),
	kRegMaskLUT1OutputBankSelect		= BIT(16),
	kRegMaskLUT2OutputBankSelect		= BIT(17),
	kRegMaskLUT3OutputBankSelect		= BIT(18),
	kRegMaskLUT4OutputBankSelect		= BIT(19),
	kRegMaskLUT5OutputBankSelect		= BIT(20),
	kRegMaskLUT6OutputBankSelect		= BIT(21),
	kRegMaskLUT7OutputBankSelect		= BIT(22),
	kRegMaskLUT8OutputBankSelect		= BIT(23),

	
	// RS422 Control 
	kRegMaskRS422TXEnable   	= BIT(0),
	kRegMaskRS422TXFIFOEmpty   	= BIT(1),
	kRegMaskRS422TXFIFOFull		= BIT(2),
	kRegMaskRS422RXEnable   	= BIT(3),
	kRegMaskRS422RXFIFONotEmpty	= BIT(4),
	kRegMaskRS422RXFIFOFull		= BIT(5),
	kRegMaskRS422RXParityError	= BIT(6),
	kRegMaskRS422RXFIFOOverrun	= BIT(7),
	kRegMaskRS422Flush			= BIT(6)+BIT(7),
	kRegMaskRS422Present		= BIT(8),
	kRegMaskRS422TXInhibit		= BIT(9),
	kRegMaskRS422ParitySense	= BIT(12),		// 0 = Odd, 1 = Even
	kRegMaskRS422ParityDisable	= BIT(13),		// 0 = Use bit 12 setting, 1 = No parity
	kRegMaskRS422BaudRate		= BIT(16)+BIT(17)+BIT(18),


	// FS1 ProcAmp Control
	kFS1RegMaskProcAmpC1Y     = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9)+BIT(10)+BIT(11),
	kFS1RegMaskProcAmpC1CB    = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23)+BIT(24)+BIT(25)+BIT(26)+BIT(27),
	kFS1RegMaskProcAmpC1CR    = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9)+BIT(10)+BIT(11),
	kFS1RegMaskProcAmpC2CB    = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23)+BIT(24)+BIT(25)+BIT(26)+BIT(27),
	kFS1RegMaskProcAmpC2CR    = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9)+BIT(10)+BIT(11),
	kFS1RegMaskProcAmpOffsetY = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23)+BIT(24)+BIT(25)+BIT(26)+BIT(27),


	kRegMaskAudioInDelay	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12),
	kRegMaskAudioOutDelay	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23)+BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28),

	// FS1 Audio Delay
	kFS1RegMaskAudioDelay		= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12),

	// Borg Audio Delay			
	kBorgRegMaskPlaybackEEAudioDelay = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kBorgRegMaskCaputreAudioDelay = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23)+BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),
	

	// kRegOutputTimingControl 
	kBorgRegMaskOutTimingCtrlHorzOfs = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12),
	kBorgRegMaskOutTimingCtrlVertOfs = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23)+BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28),


	// FS1 I2C
	kFS1RegMaskI2C1ControlWrite	= BIT(0),
	kFS1RegMaskI2C1ControlRead	= BIT(1),
	kFS1RegMaskI2C1ControlBusy	= BIT(2),
	kFS1RegMaskI2C1ControlError	= BIT(3),
	kFS1RegMaskI2C2ControlWrite	= BIT(4),
	kFS1RegMaskI2C2ControlRead	= BIT(5),
	kFS1RegMaskI2C2ControlBusy	= BIT(6),
	kFS1RegMaskI2C2ControlError	= BIT(7),
	
	kFS1RegMaskI2CAddress		= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4) + BIT(5)+BIT(6)+BIT(7),
	kFS1RegMaskI2CSubAddress	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kFS1RegMaskI2CWriteData	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4) + BIT(5)+BIT(6)+BIT(7),
	kFS1RegMaskI2CReadData	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	
	// kRegFS1ReferenceSelect (in Reg 95)
	kRegMaskLTCLoopback = BIT(10),
	kFS1RefMaskLTCOnRefInSelect = BIT(4),
	kRegMaskLTCOnRefInSelect = BIT(5),
	kFS1RefMaskLTCEmbeddedOutEnable = BIT(8),
	kFS1RegMaskReferenceInputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3),
	kFS1RegMaskColorFIDSubcarrierReset = BIT(14),
	kFS1RegMaskFreezeOutput = BIT(15),
	kFS1RegMaskProcAmpInputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kFS1RegMaskSecondAnalogOutInputSelect = BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	// FS1 AFD Mode
	kFS1RegMaskAFDReceived_Code = BIT(3)+BIT(2)+BIT(1)+BIT(0),
	kFS1RegMaskAFDReceived_AR = BIT(4),
	kFS1RegMaskAFDReceived_VANCPresent = BIT(7),
	kFS1RegMaskUpconvertAutoAFDEnable = BIT(20),
	kFS1RegMaskUpconvert2AFDDefaultHoldLast = BIT(21),
	kFS1RegMaskDownconvertAutoAFDEnable = BIT(24),
	kFS1RegMaskDownconvertAFDDefaultHoldLast = BIT(25),
	kFS1RegMaskDownconvert2AutoAFDEnable = BIT(28),
	kFS1RegMaskDownconvert2AFDDefaultHoldLast = BIT(29),

	// FS1 AFD Inserter
	kFS1RegMaskAFDVANCInserter_Code = BIT(3)+BIT(2)+BIT(1)+BIT(0),
	kFS1RegMaskAFDVANCInserter_AR = BIT(4),
	kFS1RegMaskAFDVANCInserter_Mode = BIT(13)+BIT(12),
	kFS1RegMaskAFDVANCInserter_Line = BIT(26)+BIT(25)+BIT(24)+BIT(23)+BIT(22)+BIT(21)+BIT(20)+BIT(19)+BIT(18)+BIT(17)+BIT(16),

    // FS1 Audio Channel Mapping
    kFS1RegMaskAudioChannelMapping_Gain = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4) + BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9),
    kFS1RegMaskAudioChannelMapping_Phase = BIT(15),
    kFS1RegMaskAudioChannelMapping_Source = BIT(21)+BIT(20)+BIT(19)+BIT(18)+BIT(17)+BIT(16),
    kFS1RegMaskAudioChannelMapping_Mute = BIT(31),

    // FS1 Output Timing Fine Phase Adjust
    kRegMaskOutputTimingFinePhase = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4) + BIT(5)+BIT(6)+BIT(7)+BIT(8),
	
	//kRegAnalogInputStatus
	kRegMaskAnalogCompositeLocked  = BIT(16),
	kRegMaskAnalogCompositeFormat625  = BIT(18),
	
	//kRegHDMIInputStatus / kRegAnalogInputStatus
	kRegMaskInputStatusLock = BIT(0),								// rename to kRegMaskAnalogInputStatusLock
	kLHIRegMaskHDMIInputColorSpace = BIT(2),
	kLHIRegMaskHDMIInputBitDepth = BIT(3),
	kRegMaskInputStatusV2Std = BIT(7)+BIT(6)+BIT(5)+BIT(4),
	kLHIRegMaskHDMIOutputEDIDRGB = BIT(10),
	kLHIRegMaskHDMIOutputEDID10Bit = BIT(11),
	kLHIRegMaskHDMIInput2ChAudio = BIT(12),
	kRegMaskHDMIInputProgressive = BIT(13),
	kRegMaskAnalogInputSD = BIT(14),
	kRegMaskAnalogInputIntegerRate = BIT(15),
	kLHIRegMaskHDMIOutputEDIDDVI = BIT(15),
	kRegMaskInputStatusStd = BIT(26)+BIT(25)+BIT(24),				// rename to kRegMaskAnalogInputStatusStd
	kLHIRegMaskHDMIInputProtocol = BIT(27),
	kRegMaskInputStatusFPS = BIT(31)+BIT(30)+BIT(29)+BIT(28),		// rename to kRegMaskAnalogInputStatusFPS
	
	//kRegAnalogInputControl  (Note - on some boards, ADC mode is set in Reg 128, kK2RegAnalogOutControl!)
	kRegMaskAnalogInputADCMode  = BIT(4)+BIT(3)+BIT(2)+BIT(1)+BIT(0),
	
	//kRegHDMIOut3DControl
	kRegMaskHDMIOut3DPresent	= BIT(3),
	kRegMaskHDMIOut3DMode		= BIT(4)+BIT(5)+BIT(6)+BIT(7),
	
	//kRegHDMIOutControl
	kRegMaskHDMIOutVideoStd		= BIT(2)+BIT(1)+BIT(0),
	kRegMaskHDMIOutV2VideoStd	= BIT(3)+BIT(2)+BIT(1)+BIT(0),
	kRegMaskHDMIV2TxBypass		= BIT(7),
	kLHIRegMaskHDMIOutColorSpace = BIT(8),
	kLHIRegMaskHDMIOutFPS		= BIT(12)+BIT(11)+BIT(10)+BIT(9),
	kRegMaskHDMIOutProgressive	= BIT(13),
	kLHIRegMaskHDMIOutBitDepth	= BIT(14),
	kRegMaskHDMIV2YCColor		= BIT(15),
	kRegMaskHDMISampling		= BIT(19)+BIT(18),
	kRegMaskHDMIOutPowerDown	= BIT(25),
	kRegMaskHDMIOutRange		= BIT(28),
	kRegMaskHDMIOutAudioCh		= BIT(29),
	kLHIRegMaskHDMIOutDVI		= BIT(30),

	//kRegHDMIInputStatus
	kRegMaskVideoCode			= BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9),
	kRegMaskHDMIInV2VideoStd	= BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9),
	kLHIRegMaskHDMIDownStreamDeviceYCbCrMode = BIT(10),
	kLHIRegMaskHDMIDownStreamDevice10BitMode = BIT(11),
	kRegMaskHDMIInStandard		= BIT(24)+BIT(25)+BIT(26),
	kRegMaskHDMIInFPS			= BIT(28)+BIT(29)+BIT(30)+BIT(31),
	
	//kRegHDMIInputControl
	kRefMaskHDMIAudioPairSelect = BIT(2)+BIT(3),
	kRegMaskHDMISampleRateConverterEnable = BIT(4),
	kRegMaskHDMIInputRange		= BIT(28),
	
	//kRegHDMIInputControl / kRegHDMIOutControl
	kRegMaskHDMIColorSpace		= BIT(4)+BIT(5),
	kRegMaskHDMIProtocol		= BIT(30),
	kRegMaskHDMIPolarity		= BIT(16)+BIT(17)+BIT(18)+BIT(19),

	//kK2RegAnalogOutControl - (controls Analog Inputs also, for some boards)
	kK2RegMaskVideoDACMode		= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4),
	kFS1RegMaskVideoDAC2Mode    = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12),
	kLHIRegMaskVideoDACStandard = BIT(13)+BIT(14)+BIT(15),
	kLSRegMaskVideoADCMode		= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20),
	kLHIRegMaskVideoDACMode		= BIT(21)+BIT(22)+BIT(23)+BIT(24),
	kLHIRegMaskVideoDACSetup	= BIT(21),							// bitwise interpretation of kLHIRegMaskVideoDACMode
	kLHIRegMaskVideoDACJapan	= BIT(22),							// bitwise interpretation of kLHIRegMaskVideoDACMode
	kLHIRegMaskVideoDACRGB		= BIT(23),							// bitwise interpretation of kLHIRegMaskVideoDACMode
	kLHIRegMaskVideoDACComponent = BIT(24),							// bitwise interpretation of kLHIRegMaskVideoDACMode
	kK2RegMaskOutHTiming		= BIT(31)+BIT(30)+BIT(29)+BIT(28)+BIT(27)+BIT(26)+BIT(25)+BIT(24),

	//kK2RegSDIOut1Control + kK2RegSDIOut2Control + kK2RegSDIOut3Control + kK2RegSDIOut4Control + kK2RegAnalogOutControl
	kK2RegMaskSDIOutStandard  = BIT(0)+BIT(1)+BIT(2),
	kK2RegMaskSDI1Out_2Kx1080Mode  = BIT(3),
	kLHRegMaskVideoOutputDigitalSelect = BIT(4) + BIT(5),
	kK2RegMaskSDIOutHBlankRGBRange  = BIT(7),
	kLHRegMaskVideoOutputAnalogSelect = BIT(8) + BIT(9),
	kRegMaskSDIOutLevelAtoLevelB = BIT(23),
	kLHIRegMaskSDIOut3GbpsMode = BIT(24),
	kLHIRegMaskSDIOutSMPTELevelBMode = BIT(25),
	kK2RegMaskVPIDInsertionEnable = BIT(26),
	kK2RegMaskVPIDInsertionOverwrite = BIT(27),	
	kK2RegMaskSDIOutDS1AudioSelect = BIT(28)+BIT(30),
	kK2RegMaskSDIOutDS2AudioSelect = BIT(29)+BIT(31),

	//kK2RegConversionControl and kK2Reg2ndConversionControl,
	kK2RegMaskConverterOutStandard = BIT(12)+BIT(13)+BIT(14),
	kK2RegMaskConverterOutRate = BIT(27)+BIT(28)+BIT(29)+BIT(30),
	kK2RegMaskUpConvertMode   = BIT(8)+BIT(9)+BIT(10),
	kK2RegMaskDownConvertMode   = BIT(4)+BIT(5),
	kK2RegMaskConverterInStandard = BIT(0)+BIT(1)+BIT(2),
	kK2RegMaskConverterInRate = BIT(23)+BIT(24)+BIT(25)+BIT(26),
	kK2RegMaskConverterPulldown = BIT(6),
	kK2RegMaskUCPassLine21 = BIT(16)+BIT(17),
	kK2RegMaskIsoConvertMode = BIT(20)+BIT(21)+BIT(22),
	kK2RegMaskDeinterlaceMode = BIT(15),
	kK2RegMaskEnableConverter = BIT(31),

	//kK2RegFrameSync1Control and kK2RegFrameSync2Control
	kK2RegMaskFrameSyncControlFrameDelay = BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29),
	kK2RegMaskFrameSyncControlStandard = BIT(8)+BIT(9)+BIT(10),
	kK2RegMaskFrameSyncControlGeometry = BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskFrameSyncControlFrameFormat = BIT(0)+BIT(1)+BIT(2)+BIT(3),
	
	//kK2RegXptSelectGroup1
	kK2RegMaskCompressionModInputSelect = BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),
	kK2RegMaskConversionModInputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskColorSpaceConverterInputSelect = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15), 
	kK2RegMaskXptLUTInputSelect = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7), 

	//kK2RegXptSelectGroup2
	kK2RegMaskDuallinkOutInputSelect = BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),
	kK2RegMaskFrameSync2InputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskFrameSync1InputSelect = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskFrameBuffer1InputSelect = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7), 

	//kK2RegXptSelectGroup3
	kK2RegMaskCSC1KeyInputSelect = BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),
	kK2RegMaskSDIOut2InputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskSDIOut1InputSelect = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskAnalogOutInputSelect = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),

	//kK2RegXptSelectGroup4
	kK2RegMaskMixerBGKeyInputSelect = BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),
	kK2RegMaskMixerBGVidInputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskMixerFGKeyInputSelect = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskMixerFGVidInputSelect = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	
	//kK2RegXptSelectGroup5
	kK2RegMaskCSC2KeyInputSelect = BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),
	kK2RegMaskCSC2VidInputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskXptLUT2InputSelect = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskFrameBuffer2InputSelect = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	
	//kK2RegXptSelectGroup6
	kK2RegMaskWaterMarkerInputSelect = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskIICTInputSelect = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskHDMIOutInputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskSecondConverterInputSelect = BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),
	
	//kK2RegXptSelectGroup7
	kK2RegMaskWaterMarker2InputSelect = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskIICT2InputSelect = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskDuallinkOut2InputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),

	//kK2RegXptSelectGroup8
	kK2RegMaskSDIOut3InputSelect = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskSDIOut4InputSelect = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskSDIOut5InputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	
	//kRegCh1ControlExtended
	//kRegCh2ControlExtended
	kK2RegMaskPulldownMode = BIT(2),

	//kK2RegXptSelectGroup9
	kK2RegMaskMixer2FGVidInputSelect = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskMixer2FGKeyInputSelect = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskMixer2BGVidInputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskMixer2BGKeyInputSelect = BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kK2RegXptSelectGroup10
	kK2RegMaskSDIOut1DS2InputSelect = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskSDIOut2DS2InputSelect = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	
	//kK2RegXptSelectGroup11
	kK2RegMaskDuallinkIn1InputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskDuallinkIn1DSInputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskDuallinkIn2InputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskDuallinkIn2DSInputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kK2RegXptSelectGroup12
	kK2RegMaskXptLUT3InputSelect = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskXptLUT4InputSelect = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskXptLUT5InputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),

	//kK2RegXptSelectGroup13
	kK2RegMaskFrameBuffer3InputSelect = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskFrameBuffer4InputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),

	//kK2RegXptSelectGroup14
	kK2RegMaskSDIOut3DS2InputSelect = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskSDIOut5DS2InputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskSDIOut4DS2InputSelect = BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kK2RegXptSelectGroup15
	kK2RegMaskDuallinkIn3InputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskDuallinkIn3DSInputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskDuallinkIn4InputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskDuallinkIn4DSInputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kK2RegXptSelectGroup16
	kK2RegMaskDuallinkOut3InputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskDuallinkOut4InputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskDuallinkOut5InputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),

	//kK2RegXptSelectGroup17
	kK2RegMaskCSC3VidInputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskCSC3KeyInputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskCSC4VidInputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskCSC4KeyInputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kK2RegXptSelectGroup18
	kK2RegMaskCSC5VidInputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskCSC5KeyInputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),

	//kK2RegXptSelectGroup19
	kK2RegMask4KDCQ1InputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMask4KDCQ2InputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMask4KDCQ3InputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMask4KDCQ4InputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kK2RegXptSelectGroup20
	kK2RegMaskHDMIOutV2Q1InputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskHDMIOutV2Q2InputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskHDMIOutV2Q3InputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskHDMIOutV2Q4InputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kRegXptSelectGroup21
	kK2RegMaskFrameBuffer5InputSelect = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskFrameBuffer6InputSelect = BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskFrameBuffer7InputSelect = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskFrameBuffer8InputSelect = BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kRegXptSelectGroup22
	kK2RegMaskSDIOut6InputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskSDIOut6DS2InputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskSDIOut7InputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskSDIOut7DS2InputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kRegXptSelectGroup23
	kK2RegMaskCSC7VidInputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskCSC7KeyInputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskCSC8VidInputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskCSC8KeyInputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kRegXptSelectGroup24
	kK2RegMaskXptLUT6InputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskXptLUT7InputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskXptLUT8InputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),

	//kRegXptSelectGroup25
	kK2RegMaskDuallinkIn5InputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskDuallinkIn5DSInputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskDuallinkIn6InputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskDuallinkIn6DSInputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kRegXptSelectGroup26
	kK2RegMaskDuallinkIn7InputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskDuallinkIn7DSInputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskDuallinkIn8InputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskDuallinkIn8DSInputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kRegXptSelectGroup27
	kK2RegMaskDuallinkOut6InputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskDuallinkOut7InputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskDuallinkOut8InputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),

	//kRegXptSelectGroup28
	kK2RegMaskMixer3FGVidInputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskMixer3FGKeyInputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskMixer3BGVidInputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskMixer3BGKeyInputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kRegXptSelectGroup29
	kK2RegMaskMixer4FGVidInputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskMixer4FGKeyInputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskMixer4BGVidInputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskMixer4BGKeyInputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kRegXptSelectGroup30
	kK2RegMaskSDIOut8InputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskSDIOut8DS2InputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskCSC6VidInputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskCSC6KeyInputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kRegXptSelectGroup31
	kK2RegMask425Mux1AInputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMask425Mux1BInputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMask425Mux2AInputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMask425Mux2BInputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kRegXptSelectGroup32
	kK2RegMask425Mux3AInputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMask425Mux3BInputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMask425Mux4AInputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMask425Mux4BInputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kRegXptSelectGroup33
	kK2RegMaskFrameBuffer1BInputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskFrameBuffer2BInputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskFrameBuffer3BInputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskFrameBuffer4BInputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kRegXptSelectGroup34
	kK2RegMaskFrameBuffer5BInputSelect	= BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kK2RegMaskFrameBuffer6BInputSelect	= BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15),
	kK2RegMaskFrameBuffer7BInputSelect	= BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23),
	kK2RegMaskFrameBuffer8BInputSelect	= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30)+BIT(31),

	//kK2RegCSCoefficients1_2
	//kK2RegCSC2oefficients1_2,
	//kK2RegCSC3oefficients1_2,
	//kK2RegCSC4oefficients1_2,
	//kK2RegCSC5Coefficients1_2,
	//kK2RegCSC6Coefficients1_2,
	//kK2RegCSC7Coefficients1_2,
	//kK2RegCSC8Coefficients1_2,
	kK2RegMaskVidKeySyncStatus       = BIT(28),
	kK2RegMaskMakeAlphaFromKeySelect = BIT(29),
	kK2RegMaskColorSpaceMatrixSelect = BIT(30),
	kK2RegMaskUseCustomCoefSelect    = BIT(31),

	//kK2RegCSCoefficients3_4,
	//kK2RegCS2Coefficients3_4,
	//kK2RegCS3Coefficients3_4,
	//kK2RegCS4Coefficients3_4,
	//kK2RegCS5Coefficients3_4,
	//kK2RegCS6Coefficients3_4,
	//kK2RegCS7Coefficients3_4,
	//kK2RegCS8Coefficients3_4,
	kK2RegMaskXena2RGBRange			= BIT(31),	

	//custom coefficients
	kK2RegMaskCustomCoefficientLow = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9)+BIT(10),
	kK2RegMaskCustomCoefficientHigh = BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23)+BIT(24)+BIT(25)+BIT(26),

	// Xena2K and Konax video processing
	kK2RegMaskXena2FgVidProcInputControl = BIT(20)+BIT(21),
	kK2RegMaskXena2BgVidProcInputControl = BIT(22)+BIT(23),
	kK2RegMaskXena2VidProcMode       	 = BIT(24)+BIT(25),
	kK2RegMaskXena2VidProcSplitStd		 = BIT(28) + BIT(29) + BIT(30),

	// 12(13 with sign) bit custom coefficients - backwards compatible with the 10(11 with sign) bit soft and hardware - jac
	kK2RegMaskCustomCoefficient12BitLow  = BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7)+BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12),
	kK2RegMaskCustomCoefficient12BitHigh = BIT(14)+BIT(15)+BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23)+BIT(24)+BIT(25)+BIT(26),
	
	//kRegLTCStatusControl
	kRegMaskLTC1InPresent	= BIT(0),
	kRegMaskLTC2InPresent	= BIT(8),
 
	//
	// Borg Fusion Registers
	//

	// Boot FPGA and BoardID
	kRegMaskBorgFusionBootFPGAVer = BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kRegMaskBorgFusionBoardID = BIT(0)+BIT(1)+BIT(2),

	// Codec and convert FPGA configuration control
	kRegMaskBorgFusionCodecFPGAProgram = BIT(0),
	kRegMaskBorgFusionCodecFPGAInit = BIT(1),
	kRegMaskBorgFusionCodecFPGADone = BIT(2),

	kRegMaskBorgFusionConvertFPGAProgram = BIT(4),
	kRegMaskBorgFusionConvertFPGAInit = BIT(5),
	kRegMaskBorgFusionConvertFPGADone = BIT(6),

	// Panel Push buttons debounced and SATA drive present state
	kRegMaskBorgFusionPushButtonSlotDebounced = BIT(0),
	kRegMaskBorgFusionPushButtonAdjustDownDebounced = BIT(1),
	kRegMaskBorgFusionPushButtonAdjustUpDebounced = BIT(2),
	kRegMaskBorgFusionPushButtonDeleteClipDebounced = BIT(3),
	kRegMaskBorgFusionPushButtonSelectDownDebounced = BIT(4),
	kRegMaskBorgFusionPushButtonSelectUpDebounced = BIT(5),
	kRegMaskBorgFusionPushButtonFastFwdDebounced = BIT(6),
	kRegMaskBorgFusionPushButtonRecordDebounced = BIT(7),
	kRegMaskBorgFusionPushButtonPlayDebounced = BIT(8),
	kRegMaskBorgFusionPushButtonStopDebounced = BIT(9),
	kRegMaskBorgFusionPushButtonRewindDebounced = BIT(10),
	kRegMaskBorgFusionPushButtonMediaDebounced = BIT(11),
	kRegMaskBorgFusionPushButtonConfigDebounced = BIT(12),
	kRegMaskBorgFusionPushButtonStatusDebounced = BIT(13),
	kRegMaskBorgFusionPushButtonSATADrivePresentDebounced = BIT(14),
	kRegMaskBorgFusionPushButtonPowerDebounced = BIT(15),

	// Panel Push buttons and SATA drive present changes
	kRegMaskBorgFusionPushButtonSlotChange = BIT(0),
	kRegMaskBorgFusionPushButtonAdjustDownChange = BIT(1),
	kRegMaskBorgFusionPushButtonAdjustUpChange = BIT(2),
	kRegMaskBorgFusionPushButtonDeleteClipChange = BIT(3),
	kRegMaskBorgFusionPushButtonSelectDownChange = BIT(4),
	kRegMaskBorgFusionPushButtonSelectUpChange = BIT(5),
	kRegMaskBorgFusionPushButtonFastFwdChange = BIT(6),
	kRegMaskBorgFusionPushButtonRecordChange = BIT(7),
	kRegMaskBorgFusionPushButtonPlayChange = BIT(8),
	kRegMaskBorgFusionPushButtonStopChange = BIT(9),
	kRegMaskBorgFusionPushButtonRewindChange = BIT(10),
	kRegMaskBorgFusionPushButtonMediaChange = BIT(11),
	kRegMaskBorgFusionPushButtonConfigChange = BIT(12),
	kRegMaskBorgFusionPushButtonStatusChange = BIT(13),
	kRegMaskBorgFusionPushButtonSATADrivePresentChange = BIT(14),
	kRegMaskBorgFusionPushButtonPowerButtonChange = BIT(15),

	// LED Pulse Width Modulation Threshholds
	kRegMaskBorgFusionPWMThreshExpressCard2 = BIT(0)+BIT(1)+BIT(2)+BIT(3),
	kRegMaskBorgFusionPWMThreshExpressCard1 = BIT(4)+BIT(5)+BIT(6)+BIT(7),
	kRegMaskBorgFusionPWMThreshPower = BIT(8)+BIT(9)+BIT(10)+BIT(11),
	kRegMaskBonesFusionPWMThreshLCDBacklightLED = BIT(12)+BIT(13)+BIT(14)+BIT(15),

	// Power control - System
	kRegMaskBorgFusionPowerCtrlWiFiReset = BIT(0),
	kRegMaskBorgFusionPowerCtrlFirewirePower = BIT(1),
	kRegMaskBorgFusionPowerCtrlGigEthReset = BIT(2),
	kRegMaskBorgFusionPowerCtrlPCIExpClockStop = BIT(3),

	// Power control - Storage devices - Borg Fusion
	kRegMaskBorgFusionPowerCtrlPCIExpCard1_3_3vPower = BIT(8),	// Express Card 1 3.3v power
	kRegMaskBorgFusionPowerCtrlPCIExpCard1_1_5vPower = BIT(9),	// Express Card 1 1.5v power
	kRegMaskBorgFusionPowerCtrlPCIExpCard2_3_3vPower = BIT(10),	// Express Card 2 3.3v power
	kRegMaskBorgFusionPowerCtrlPCIExpCard2_1_5vPower = BIT(11),	// Express Card 2 1.5v power
	kRegMaskBorgFusionPowerCtrlSata_12vPower = BIT(12),			// SATA Drive 12v power

	// Power control - Storage devices - Bones Actel
	kRegMaskBonesActelPowerCtrlCFSlot2_BridgeReset   = BIT(8),	// Bones Actel CF Slot 2 (CPU) Bridge Reset
	kRegMaskBonesActelPowerCtrlCFSlot2_Power         = BIT(9),	// Bones Actel CF Slot 2 (CPU) Power
	kRegMaskBonesActelPowerCtrlCFSlot1_Power         = BIT(10),	// Bones Actel CF Slot 1 (VIDeo) Power
	kRegMaskBonesActelPowerCtrlCFSlot1_BridgeReset   = BIT(11),	// Bones Actel CF Slot 1 (VIDeo) Bridge Reset

	// Power control - Storage devices - Barclay Actel Fusion
	kRegMaskBarclayFusionPowerCtrlPS1Active = BIT(6), 			// Barclay Fusion Power Supply 1 active bit
	kRegMaskBarclayFusionPowerCtrlPS2Active = BIT(5), 			// Barclay Fusion Power Supply 2 active bit

	kRegMaskBarclayFusionIdentifyLEDCtrl = BIT(1), 	//Barclay Identify LED On/Off bit, Rear LED //RS

	// Power control - Pushbutton LEDs
	kRegMaskBorgFusionPowerCtrlPCIExpCard2LED = BIT(13),
	kRegMaskBorgFusionPowerCtrlPCIExpCard1LED = BIT(14),
	kRegMaskBorgFusionPowerCtrlPowerButtonLED = BIT(15),

	// IRQ3n Interrupt control
	kRegMaskBorgFusionIRQ3nIntCtrlPushButtonChangeEnable = BIT(0),
	kRegMaskBorgFusionIRQ3nIntCtrlInputVoltageLow9vEnable = BIT(1),
	kRegMaskBorgFusionIRQ3nIntCtrlDisplayFIFOFullEnable = BIT(2),
	kRegMaskBorgFusionIRQ3nIntCtrlSATAPresentChangeEnable = BIT(3),
	kRegMaskBorgFusionIRQ3nIntCtrlTemp1HighEnable = BIT(4),
	kRegMaskBorgFusionIRQ3nIntCtrlTemp2HighEnable = BIT(5),
	kRegMaskBorgFusionIRQ3nIntCtrlPowerButtonChangeEnable = BIT(6),

	// IRQ3n Interrupt source
	kRegMaskBorgFusionIRQ3nIntSrcPushButtonChange= BIT(0),
	kRegMaskBorgFusionIRQ3nIntSrcInputVoltageLow9v= BIT(1),
	kRegMaskBorgFusionIRQ3nIntSrcDisplayFIFOFull= BIT(2),
	kRegMaskBorgFusionIRQ3nIntSrcSATAPresentChange= BIT(3),
	kRegMaskBorgFusionIRQ3nIntSrcTemp1High= BIT(4),
	kRegMaskBorgFusionIRQ3nIntSrcTemp2High= BIT(5),
	kRegMaskBorgFusionIRQ3nIntSrcPowerButtonChange= BIT(6),

	// Noritake Display Control/Status
	kRegMaskBorgFusionDisplayCtrlReset = BIT (0),
	kRegMaskBorgFusionDisplayStatusBusyRaw = BIT (1),		// Not needed by CPU, used internally by FPGA
	kRegMaskBorgFusionDisplayStatusInterfaceBusy = BIT (7),	// FIFO full

	// Analog ADC flags - battery
	kRegMaskBorgFusionAnalogFlagsPowerLTE9v = BIT(0),	// +12 v supply <= 9.0 v battery critical
	kRegMaskBorgFusionAnalogFlagsPowerLTE10v = BIT(1),	// +12 v supply <= 10.0 v battery depleting
	kRegMaskBorgFusionAnalogFlagsPowerLTE11v = BIT(2),	// +12 v supply <= 11.0 v battery depleting
	kRegMaskBorgFusionAnalogFlagsPowerGTE13v = BIT(3),	// +12 v supply >= 13.0 v battery charging

	// Analog ADC flags - temperature sensor
	kRegMaskBorgFusionAnalogFlagsPowerTemp1High = BIT(4),	// Temp sensor 1 > 65 C
	kRegMaskBorgFusionAnalogFlagsPowerTemp2High = BIT(5),	// Temp sensor 2 > 65 C
	
	// Bones Actel Compact Flash Slot Debounced Card Present
	kRegMaskBonesActelCFSlot1_Present   = BIT(1)+BIT(0),
	kRegMaskBonesActelCFSlot2_Present   = BIT(3)+BIT(2),

	// Bones Actel Compact Flash Slot Changes Present
	kRegMaskBonesActelCFSlot1_Changes   = BIT(1)+BIT(0),
	kRegMaskBonesActelCFSlot2_Changes   = BIT(3)+BIT(2),
	
	// kRegAudioOutputSourceMap
	kRegMaskMonitorSource				= BIT(21)+BIT(20)+BIT(19)+BIT(18)+BIT(17)+BIT(16),
	kRegMaskHDMIOutAudioSource			= BIT(31)+BIT(30)+BIT(29)+BIT(28)+BIT(27)+BIT(26)+BIT(25)+BIT(24),
	
#if !defined (NTV2_DEPRECATE)
	// kRegSDIInput3GStatus
	kLHIRegMaskSDIIn3GbpsMode = BIT(0),
	kLHIRegMaskSDIIn3GbpsSMPTELevelBMode = BIT(1),
	kLHIRegMaskSDIInVPIDLinkAValid = BIT(4),
	kLHIRegMaskSDIInVPIDLinkBValid = BIT(5),
	kLHIRegMaskSDIIn23GbpsMode = BIT(8),
	kLHIRegMaskSDIIn23GbpsSMPTELevelBMode = BIT(9),
	kLHIRegMaskSDIIn2VPIDLinkAValid = BIT(12),
	kLHIRegMaskSDIIn2VPIDLinkBValid = BIT(13),

	// kRegSDIInput3GStatus2
	kLHIRegMaskSDIIn33GbpsMode = BIT(0),
	kLHIRegMaskSDIIn33GbpsSMPTELevelBMode = BIT(1),
	kLHIRegMaskSDIIn3VPIDLinkAValid = BIT(4),
	kLHIRegMaskSDIIn3VPIDLinkBValid = BIT(5),
	kLHIRegMaskSDIIn43GbpsMode = BIT(8),
	kLHIRegMaskSDIIn43GbpsSMPTELevelBMode = BIT(9),
	kLHIRegMaskSDIIn4VPIDLinkAValid = BIT(12),
	kLHIRegMaskSDIIn4VPIDLinkBValid = BIT(13),
#endif

	// kRegSDIInput3GStatus
	kRegMaskSDIIn3GbpsMode = BIT(0),
	kRegMaskSDIIn3GbpsSMPTELevelBMode = BIT(1),
	kRegMaskSDIIn1LevelBtoLevelA = BIT(2),
	kRegMaskSDIInVPIDLinkAValid = BIT(4),
	kRegMaskSDIInVPIDLinkBValid = BIT(5),
	kRegMaskSDIIn23GbpsMode = BIT(8),
	kRegMaskSDIIn23GbpsSMPTELevelBMode = BIT(9),
	kRegMaskSDIIn2LevelBtoLevelA = BIT(10),
	kRegMaskSDIIn2VPIDLinkAValid = BIT(12),
	kRegMaskSDIIn2VPIDLinkBValid = BIT(13),

	// kRegSDIInput3GStatus2
	kRegMaskSDIIn33GbpsMode = BIT(0),
	kRegMaskSDIIn33GbpsSMPTELevelBMode = BIT(1),
	kRegMaskSDIIn3LevelBtoLevelA = BIT(2),
	kRegMaskSDIIn3VPIDLinkAValid = BIT(4),
	kRegMaskSDIIn3VPIDLinkBValid = BIT(5),
	kRegMaskSDIIn43GbpsMode = BIT(8),
	kRegMaskSDIIn43GbpsSMPTELevelBMode = BIT(9),
	kRegMaskSDIIn4LevelBtoLevelA = BIT(10),
	kRegMaskSDIIn4VPIDLinkAValid = BIT(12),
	kRegMaskSDIIn4VPIDLinkBValid = BIT(13),

	// kRegSDI5678Input3GStatus
	kRegMaskSDIIn53GbpsMode = BIT(0),
	kRegMaskSDIIn53GbpsSMPTELevelBMode = BIT(1),
	kRegMaskSDIIn5LevelBtoLevelA = BIT(2),
	kRegMaskSDIIn5VPIDLinkAValid = BIT(4),
	kRegMaskSDIIn5VPIDLinkBValid = BIT(5),
	kRegMaskSDIIn63GbpsMode = BIT(8),
	kRegMaskSDIIn63GbpsSMPTELevelBMode = BIT(9),
	kRegMaskSDIIn6LevelBtoLevelA = BIT(10),
	kRegMaskSDIIn6VPIDLinkAValid = BIT(12),
	kRegMaskSDIIn6VPIDLinkBValid = BIT(13),
	kRegMaskSDIIn73GbpsMode = BIT(16),
	kRegMaskSDIIn73GbpsSMPTELevelBMode = BIT(17),
	kRegMaskSDIIn7LevelBtoLevelA = BIT(18),
	kRegMaskSDIIn7VPIDLinkAValid = BIT(20),
	kRegMaskSDIIn7VPIDLinkBValid = BIT(21),
	kRegMaskSDIIn83GbpsMode = BIT(24),
	kRegMaskSDIIn83GbpsSMPTELevelBMode = BIT(25),
	kRegMaskSDIIn8LevelBtoLevelA = BIT(26),
	kRegMaskSDIIn8VPIDLinkAValid = BIT(28),
	kRegMaskSDIIn8VPIDLinkBValid = BIT(29),

	// kRegVPID
	kRegMaskVPIDBitDepth				= BIT(1)+BIT(0),
	kRegMaskVPIDDynamicRange			= BIT(4)+BIT(3),
	kRegMaskVPIDChannel					= BIT(7)+BIT(6),
	kRegMaskVPIDSampling				= BIT(11)+BIT(10)+BIT(9)+BIT(8),
	kRegMaskVPIDHorizontalSampling		= BIT(14),
	kRegMaskVPIDImageAspect16x9			= BIT(15),
	kRegMaskVPIDPictureRate				= BIT(19)+BIT(18)+BIT(17)+BIT(16),
	kRegMaskVPIDProgressivePicture		= BIT(22),
	kRegMaskVPIDProgressiveTransport	= BIT(23),
	kRegMaskVPIDStandard				= BIT(24)+BIT(25)+BIT(26)+BIT(27)+BIT(28)+BIT(29)+BIT(30),
	kRegMaskVPIDVersionID				= BIT(31),
	
	//Borg Test Pattern Generator
	kRegMaskTPGChromaSample             = BIT(9)+BIT(8)+BIT(7)+BIT(6)+BIT(5)+BIT(4)+BIT(3)+BIT(2)+BIT(1)+BIT(0),
	kRegMaskTPGLineBuffer               = BIT(11)+BIT(10),
	kRegMaskTPGFrameRate                = BIT(15)+BIT(14)+BIT(13)+BIT(12),
	kRegMaskTPGLuma                     = BIT(25)+BIT(24)+BIT(23)+BIT(22)+BIT(21)+BIT(20)+BIT(19)+BIT(18)+BIT(17)+BIT(16),
	kRegMaskTPGMulti                    = BIT(26),
    kRegMaskTPGReset                    = BIT(27),
	kRegMaskTPGStandard                 = BIT(30) + BIT(29) + BIT(28),
	kRegMaskTPGWriteEnable              = BIT(31),

	// Bones Actel Registers
	kRegMaskCFS1                        = BIT(0) + BIT(1), 
	kRegMaskCFS2                        = BIT(2) + BIT(3) ,

	// Audio Channel Control 2 or 8 channel playback mask
	kRegAudControlMask					= BIT(5),

	// Stereo Compressor Control
	kRegMaskStereoCompressorOutputMode		= BIT(3)+BIT(2)+BIT(1)+BIT(0),
	kRegMaskStereoCompressorFlipMode		= BIT(7)+BIT(6)+BIT(5)+BIT(4),
	kRegMaskStereoCompressorFlipLeftHorz	= BIT(4),
	kRegMaskStereoCompressorFlipLeftVert	= BIT(5),
	kRegMaskStereoCompressorFlipRightHorz	= BIT(6),
	kRegMaskStereoCompressorFlipRightVert	= BIT(7),
	kRegMaskStereoCompressorFormat			= BIT(10)+BIT(9)+BIT(8),
	kRegMaskStereoCompressorLeftSource		= BIT(23)+BIT(22)+BIT(21)+BIT(20)+BIT(19)+BIT(18)+BIT(17)+BIT(16),
	kRegMaskStereoCompressorRightSource		= BIT(31)+BIT(30)+BIT(29)+BIT(28)+BIT(27)+BIT(26)+BIT(25)+BIT(24),

	// SDI Direction Control
	kRegMaskSDI5Transmit	= BIT(24),
	kRegMaskSDI6Transmit	= BIT(25),
	kRegMaskSDI7Transmit	= BIT(26),
	kRegMaskSDI8Transmit	= BIT(27),
	kRegMaskSDI1Transmit	= BIT(28),
	kRegMaskSDI2Transmit	= BIT(29),
	kRegMaskSDI3Transmit	= BIT(30),
	kRegMaskSDI4Transmit	= BIT(31),
	
	// SDI watchdog control
	kRegMaskSDIRelayControl12	= BIT(0),
	kRegMaskSDIRelayControl34	= BIT(1),
	kRegMaskSDIWatchdogEnable12	= BIT(4),
	kRegMaskSDIWatchdogEnable34	= BIT(5),
	kRegMaskSDIRelayPosition12	= BIT(8),
	kRegMaskSDIRelayPosition34	= BIT(9),
	kRegMaskSDIWatchdogStatus	= BIT(12),

	// 4K Down Convert
	kRegMask4KDCRGBMode		= BIT(0),
	kRegMask4KDCYCC444Mode	= BIT(1),
	kRegMask4KDCPSFInMode	= BIT(2),
	kRegMask4KDCPSFOutMode	= BIT(3),

	// Quadrant Rasterizer Control
	kRegMaskRasterMode		= BIT(0)+BIT(1),
	kRegMaskRasterLevelB	= BIT(4),
	kRegMaskRasterDecimate	= BIT(8),

	kRegMaskTRSErrorSDI1		= BIT(0),
	kRegMaskTRSErrorSDI2		= BIT(1),
	kRegMaskTRSErrorSDI3		= BIT(2),
	kRegMaskTRSErrorSDI4		= BIT(3),
	kRegMaskTRSErrorSDI5		= BIT(4),
	kRegMaskTRSErrorSDI6		= BIT(5),
	kRegMaskTRSErrorSDI7		= BIT(6),
	kRegMaskTRSErrorSDI8		= BIT(7),
	kRegMaskLockSDI1			= BIT(16),
	kRegMaskLockSDI2			= BIT(17),
	kRegMaskLockSDI3			= BIT(18),
	kRegMaskLockSDI4			= BIT(19),
	kRegMaskLockSDI5			= BIT(20),
	kRegMaskLockSDI6			= BIT(21),
	kRegMaskLockSDI7			= BIT(22),
	kRegMaskLockSDI8			= BIT(23),

	kRegMask2MFrameSize			= BIT(4)+BIT(3)+BIT(2)+BIT(1)+BIT(0),
	kRegMaskChannelBar			= BIT(24)+BIT(23)+BIT(22)+BIT(21)+BIT(20)+BIT(19)+BIT(18)+BIT(17)+BIT(16),

	kRegMaskPCMControlA1P1_2		= BIT(0),
	kRegMaskPCMControlA1P3_4		= BIT(1),
	kRegMaskPCMControlA1P5_6		= BIT(2),
	kRegMaskPCMControlA1P7_8		= BIT(3),
	kRegMaskPCMControlA1P9_10		= BIT(4),
	kRegMaskPCMControlA1P11_12		= BIT(5),
	kRegMaskPCMControlA1P13_14		= BIT(6),
	kRegMaskPCMControlA1P15_16		= BIT(7),

	kRegMaskPCMControlA2P1_2		= BIT(8),
	kRegMaskPCMControlA2P3_4		= BIT(9),
	kRegMaskPCMControlA2P5_6		= BIT(10),
	kRegMaskPCMControlA2P7_8		= BIT(11),
	kRegMaskPCMControlA2P9_10		= BIT(12),
	kRegMaskPCMControlA2P11_12		= BIT(13),
	kRegMaskPCMControlA2P13_14		= BIT(14),
	kRegMaskPCMControlA2P15_16		= BIT(15),

	kRegMaskPCMControlA3P1_2		= BIT(16),
	kRegMaskPCMControlA3P3_4		= BIT(17),
	kRegMaskPCMControlA3P5_6		= BIT(18),
	kRegMaskPCMControlA3P7_8		= BIT(19),
	kRegMaskPCMControlA3P9_10		= BIT(20),
	kRegMaskPCMControlA3P11_12		= BIT(21),
	kRegMaskPCMControlA3P13_14		= BIT(22),
	kRegMaskPCMControlA3P15_16		= BIT(23),

	kRegMaskPCMControlA4P1_2		= BIT(24),
	kRegMaskPCMControlA4P3_4		= BIT(25),
	kRegMaskPCMControlA4P5_6		= BIT(26),
	kRegMaskPCMControlA4P7_8		= BIT(27),
	kRegMaskPCMControlA4P9_10		= BIT(28),
	kRegMaskPCMControlA4P11_12		= BIT(29),
	kRegMaskPCMControlA4P13_14		= BIT(30),
	kRegMaskPCMControlA4P15_16		= BIT(31),

	kRegMaskPCMControlA5P1_2		= BIT(0),
	kRegMaskPCMControlA5P3_4		= BIT(1),
	kRegMaskPCMControlA5P5_6		= BIT(2),
	kRegMaskPCMControlA5P7_8		= BIT(3),
	kRegMaskPCMControlA5P9_10		= BIT(4),
	kRegMaskPCMControlA5P11_12		= BIT(5),
	kRegMaskPCMControlA5P13_14		= BIT(6),
	kRegMaskPCMControlA5P15_16		= BIT(7),

	kRegMaskPCMControlA6P1_2		= BIT(8),
	kRegMaskPCMControlA6P3_4		= BIT(9),
	kRegMaskPCMControlA6P5_6		= BIT(10),
	kRegMaskPCMControlA6P7_8		= BIT(11),
	kRegMaskPCMControlA6P9_10		= BIT(12),
	kRegMaskPCMControlA6P11_12		= BIT(13),
	kRegMaskPCMControlA6P13_14		= BIT(14),
	kRegMaskPCMControlA6P15_16		= BIT(15),

	kRegMaskPCMControlA7P1_2		= BIT(16),
	kRegMaskPCMControlA7P3_4		= BIT(17),
	kRegMaskPCMControlA7P5_6		= BIT(18),
	kRegMaskPCMControlA7P7_8		= BIT(19),
	kRegMaskPCMControlA7P9_10		= BIT(20),
	kRegMaskPCMControlA7P11_12		= BIT(21),
	kRegMaskPCMControlA7P13_14		= BIT(22),
	kRegMaskPCMControlA7P15_16		= BIT(23),

	kRegMaskPCMControlA8P1_2		= BIT(24),
	kRegMaskPCMControlA8P3_4		= BIT(25),
	kRegMaskPCMControlA8P5_6		= BIT(26),
	kRegMaskPCMControlA8P7_8		= BIT(27),
	kRegMaskPCMControlA8P9_10		= BIT(28),
	kRegMaskPCMControlA8P11_12		= BIT(29),
	kRegMaskPCMControlA8P13_14		= BIT(30),
	kRegMaskPCMControlA8P15_16		= BIT(31)


} RegisterMask;

typedef enum
{
	// Global Control
	kRegShiftFrameRate					= 0,
	kRegShiftFrameRateHiBit				= 22,
	kRegShiftGeometry					= 3,
	kRegShiftStandard					= 7,
	kRegShiftRefSource					= 10,
	kRegShiftRefInputVoltage			= 12,
	kRegShiftSmpte372					= 15,
	kRegShiftLED						= 16,
	kRegShiftRegClocking				= 20,
	kRegShiftDualLinkInput				= 23,
	kRegShiftBankSelect					= 25,
	kRegShiftDualLinKOutput				= 27,
	kRegShiftRP188ModeCh1				= 28,
	kRegShiftRP188ModeCh2				= 29,
	kRegShiftCCHostAccessBankSelect		= 30,

	// Global Control 2
	kRegShiftRefSource2					= 0,
	kRegShiftQuadMode					= 3,
	kRegShiftAud1PlayCapMode			= 4,
	kRegShiftAud2PlayCapMode			= 5,
	kRegShiftAud3PlayCapMode			= 6,
	kRegShiftAud4PlayCapMode			= 7,
	kRegShiftAud5PlayCapMode			= 8,
	kRegShiftAud6PlayCapMode			= 9,
	kRegShiftAud7PlayCapMode			= 10,
	kRegShiftAud8PlayCapMode			= 11,
	kRegShiftQuadMode2					= 12,
	kRegShiftIndependentMode			= 16,
	kRegShift2MFrameSupport				= 17,
	kRegShift425FB12					= 20,
	kRegShift425FB34					= 21,
	kRegShift425FB56					= 22,
	kRegShift425FB78					= 23,
	kRegShiftRP188ModeCh3				= 28,
	kRegShiftRP188ModeCh4				= 29,
	kRegShiftRP188ModeCh5				= 30,
	kRegShiftRP188ModeCh6				= 31,
	kRegShiftRP188ModeCh7				= 26,
	kRegShiftRP188ModeCh8				= 27,

	// Channel Control - kRegCh1Control, kRegCh2Control, kRegCh3Control, kRegCh4Control
	kRegShiftMode						= 0,
	kRegShiftFrameFormat				= 1,
	kRegShiftFrameFormatHiBit			= 6, // KAM
	kRegShiftAlphaFromInput2			= 5,
	kRegShiftChannelDisable				= 7,
	kRegShiftWriteBack					= 8,
	kRegShiftFrameOrientation			= 10,
	kRegShiftQuarterSizeMode			= 11,
	kRegShiftFrameBufferMode			= 12,
	kKHRegShiftDownconvertInput			= 14,
	kLSRegShiftVideoInputSelect			= 15,
	kRegShiftDitherOn8BitInput			= 16,
	kRegShiftQuality					= 17,
	kRegShiftEncodeAsPSF				= 18,
	kK2RegShiftFrameSize				= 20,
	kRegShiftChannelCompressed			= 22,
	kRegShiftRGB8b10bCvtMode			= 23,
	kRegShiftVBlankRGBRangeMode			= 24,		// Deprecated
	kRegShiftVBlankRGBRange				= 24,
	kRegShiftQuality2					= 25,
	kRegCh1BlackOutputShift				= 27,		// KiPro bit set results in black output video and muted audio in capture mode, black output video in playback
	kRegShiftSonySRExpressBit			= 28,
	kRegShiftFrameSizeSetBySW			= 29,
	kRegShiftVidProcVANCShift			= 31,

	// Video Crosspoint Control
	kRegShiftVidXptFGVideo				= 0,
	kRegShiftVidXptBGVideo				= 4,
	kRegShiftVidXptFGKey				= 8,
	kRegShiftVidXptBGKey				= 12,
	kRegShiftVidXptSecVideo				= 16,

	// Video Processing Control
	kRegShiftVidProcMux1				= 0,
	kRegShiftVidProcMux2				= 2,
	kRegShiftVidProcMux3				= 4,
	kRegShiftVidProcMux4				= 6,
	kRegShiftVidProcMux5				= 8,

	kRegShiftVidProcLimiting			= 11,
	kRegShiftVidProcFGMatteEnable		= 18,
	kRegShiftVidProcBGMatteEnable		= 19,
	kRegShiftVidProcFGControl			= 20,
	kRegShiftVidProcBGControl			= 22,
	kRegShiftVidProcMode				= 24,
	kRegShiftVidProcSyncFail			= 26,
	kRegShiftVidProcSplitStd			= 28,
	kRegShiftVidProcSubtitleEnable	= BIT(31),


	// Note:  See more bitfields for this register below, in the 'Xena2K and Konax Video Processing.' section
	
	// kRegStatus
	kRegShiftHardwareVersion			= 0,
	kRegShiftFPGAVersion				= 4,
	kRegShiftLTCInPresent				= 17,

	// Video Interrupt Control
	kRegShiftIntEnableMask				= 0,

	// Audio Control
	kRegShiftCaptureEnable				= 0,
	kRegShiftNumBits					= 1,	// shouldn't this be 2?
	kRegShiftOutputTone					= 1,
	kRegShift20BitMode					= 2,
	kRegShiftLoopBack					= 3,
	kRegShiftAudioTone					= 7,
	kRegShiftResetAudioInput			= 8,
	kRegShiftResetAudioOutput			= 9,
	kRegShiftPauseAudio					= 11,
    kRegShiftEmbeddedOutputMuteCh1      = 12, // added for FS1
    kRegShiftEmbeddedOutputSupressCh1   = 13, // added for FS1 but available on other boards
	kRegShiftEmbeddedOutputSupressCh2	= 15, // added for FS1 but available on other boards
	kRegShiftNumChannels				= 16,
    kRegShiftEmbeddedOutputMuteCh2      = 17, // added for FS1
	kRegShiftAudioRate					= 18,
	kRegShiftEncodedAudioMode			= 19,
	kRegShiftAudio16Channel				= 20,
	kRegShiftAudio8Channel				= 23,
	kK2RegShiftKBoxAnalogMonitor		= 24,
	kK2RegShiftKBoxAudioInputSelect		= 26,
	kK2RegShiftKBoxDetect				= 27,
	kK2RegShiftBOCableDetect			= 28,
	kK2RegShiftAudioLevel				= 29,
	kK2RegShiftAverageAudioLevel		= 0,
	kFS1RegShiftAudioLevel				= 29,
	kK2RegShiftAudioBufferSize			= 31,
	kLHRegShiftResetAudioDAC			= 31,

	// Audio Source Select
	kRegShiftAudioSource				= 0,
	kRegShiftEmbeddedAudioInput			= 16,
	kRegShiftAnalogHDMIvsAES     		= 20,
	kRegShift3GbSelect					= 21,
	kRegShiftEmbeddedAudioClock			= 22,
	kRegShiftEmbeddedAudioInput2		= 23,
	kRegShiftAnalogAudioInGain			= 24,
	kRegShiftAnalogAudioInJack			= 25,

	kRegShiftLossOfInput				= 11,

	// Input Status
	kRegShiftInput1FrameRate			= 0,
	kRegShiftInput1Geometry				= 4,
	kRegShiftInput1Progressive			= 7,
	kRegShiftInput2FrameRate			= 8,
	kRegShiftInput2Geometry				= 12,
	kRegShiftInput2Progressive			= 15,
	kRegShiftReferenceFrameRate			= 16,
	kRegShiftReferenceFrameLines		= 20,
	kRegShiftReferenceProgessive		= 23,
	kRegShiftAESCh12Present				= 24,
	kRegShiftAESCh34Present				= 25,
	kRegShiftAESCh56Present				= 26,
	kRegShiftAESCh78Present				= 27,
	kRegShiftInput1FrameRateHigh		= 28,
	kRegShiftInput2FrameRateHigh		= 29,
	kRegShiftInput1GeometryHigh			= 30,
	kRegShiftInput2GeometryHigh			= 31,

	// Pan (2K crop) - Xena 2
	kRegShiftPanMode					= 30,
	kRegShiftPanOffsetV					= 0,
	kRegShiftPanOffsetH					= 12,
	
	// RP-188 Source
	kRegShiftRP188Source				= 24,

    // DMA Control
	kRegShiftForce64					= 4,
    kRegShiftAutodetect64				= 5,
	kRegShiftFirmWareRev				= 8,
	kRegShiftDMAPauseDisable			= 16,
	
	// Color Correction Control
	kRegShiftSaturationValue			= 0,
	kRegShiftCCOutputBankSelect			= 16,
	kRegShiftCCMode						= 17,
	kRegShiftCC5HostAccessBankSelect	= 20,
	kRegShiftCC5OutputBankSelect		= 21,
	kRegShiftLUT5Select					= 28,
	kRegShiftLUTSelect					= 29,
	kRegShiftCC3OutputBankSelect		= 30,
	kRegShiftCC4OutputBankSelect		= 31,

	// kRegLUTV2Control
	kRegShiftLUT1Enable					= 0,
	kRegShiftLUT2Enable					= 1,
	kRegShiftLUT3Enable					= 2,
	kRegShiftLUT4Enable					= 3,
	kRegShiftLUT5Enable					= 4,
	kRegShiftLUT6Enable					= 5,
	kRegShiftLUT7Enable					= 6,
	kRegShiftLUT8Enable					= 7,
	kRegShiftLUT1HostAccessBankSelect	= 8,
	kRegShiftLUT2HostAccessBankSelect	= 9,
	kRegShiftLUT3HostAccessBankSelect	= 10,
	kRegShiftLUT4HostAccessBankSelect	= 11,
	kRegShiftLUT5HostAccessBankSelect	= 12,
	kRegShiftLUT6HostAccessBankSelect	= 13,
	kRegShiftLUT7HostAccessBankSelect	= 14,
	kRegShiftLUT8HostAccessBankSelect	= 15,
	kRegShiftLUT1OutputBankSelect		= 16,
	kRegShiftLUT2OutputBankSelect		= 17,
	kRegShiftLUT3OutputBankSelect		= 18,
	kRegShiftLUT4OutputBankSelect		= 19,
	kRegShiftLUT5OutputBankSelect		= 20,
	kRegShiftLUT6OutputBankSelect		= 21,
	kRegShiftLUT7OutputBankSelect		= 22,
	kRegShiftLUT8OutputBankSelect		= 23,

	// RS422 Control 
	kRegShiftRS422TXEnable				= 0,
	kRegShiftRS422TXFIFOEmpty 	 	 	= 1,
	kRegShiftRS422TXFIFOFull			= 2,
	kRegShiftRS422RXEnable				= 3,
	kRegShiftRS422RXFIFONotEmpty		= 4,
	kRegShiftRS422RXFIFOFull			= 5,
	kRegShiftRS422RXParityError			= 6,
	kRegShiftRS422Flush					= 6,
	kRegShiftRS422RXFIFOOverrun			= 7,
	kRegShiftRS422Present				= 8,
	kRegShiftRS422TXInhibit				= 9,
	kRegShiftRS422ParitySense			= 12,
	kRegShiftRS422ParityDisable			= 13,
	kRegShiftRS422BaudRate				= 16,


	// FS1 ProcAmp Control
	kFS1RegShiftProcAmpC1Y				= 0,
	kFS1RegShiftProcAmpC1CB				= 16,
	kFS1RegShiftProcAmpC1CR				= 0,
	kFS1RegShiftProcAmpC2CB				= 16,
	kFS1RegShiftProcAmpC2CR				= 0,
	kFS1RegShiftProcAmpOffsetY			= 16,

	kRegShiftAudioInDelay				= 0,
	kRegShiftAudioOutDelay				= 16,
	
	// FS1 Audio Delay
	kFS1RegShiftAudioDelay				= 0,

	// Borg Audio Delay			
	kBorgRegShiftPlaybackEEAudioDelay = 0,
	kBorgRegShiftCaputreAudioDelay = 16,

	// kRegOutputTimingControl 
	kBorgRegShiftOutTimingCtrlHorzOfs = 0,
	kBorgRegShiftOutTimingCtrlVertOfs = 16,

	// FS1 I2C
	kFS1RegShiftI2C1ControlWrite		= 0,
	kFS1RegShiftI2C1ControlRead			= 1,
	kFS1RegShiftI2C1ControlBusy			= 2,
	kFS1RegShiftI2C1ControlError		= 3,
	kFS1RegShiftI2C2ControlWrite		= 4,
	kFS1RegShiftI2C2ControlRead			= 5,
	kFS1RegShiftI2C2ControlBusy			= 6,
	kFS1RegShiftI2C2ControlError		= 7,
	
	kFS1RegShiftI2CAddress				= 0,
	kFS1RegShiftI2CSubAddress			= 8,
	kFS1RegShiftI2CWriteData			= 0,
	kFS1RegShiftI2CReadData				= 8,
	
	//kRegFS1ReferenceSelect
	kRegShiftLTCLoopback				= 10,
	kFS1RegShiftReferenceInputSelect	= 0,
	kFS1RefShiftLTCOnRefInSelect		= 4,
	kRegShiftLTCOnRefInSelect			= 5,
	kFS1RefShiftLTCEmbeddedOutEnable	= 8,
	kFS1RegShiftColorFIDSubcarrierReset = 14,
	kFS1RegShiftFreezeOutput			= 15,
	kFS1RegShiftProcAmpInputSelect		= 16,
	kFS1RegShiftSecondAnalogOutInputSelect = 24,
	
	// FS1 AFD Mode
	kFS1RegShiftAFDReceived_Code = 0,
	kFS1RegShiftAFDReceived_AR = 4,
	kFS1RegShiftAFDReceived_VANCPresent = 7,
	kFS1RegShiftUpconvertAutoAFDEnable = 20,
	kFS1RegShiftUpconvert2AFDDefaultHoldLast = 21,
	kFS1RegShiftDownconvertAutoAFDEnable = 24,
	kFS1RegShiftDownconvertAFDDefaultHoldLast = 25,
	kFS1RegShiftDownconvert2AutoAFDEnable = 28,
	kFS1RegShiftDownconvert2AFDDefaultHoldLast = 29,

	// FS1 AFD Inserter
	kFS1RegShiftAFDVANCInserter_Code = 0,
	kFS1RegShiftAFDVANCInserter_AR = 4,
	kFS1RegShiftAFDVANCInserter_Mode = 12,
	kFS1RegShiftAFDVANCInserter_Line = 16,

    // FS1 Audio Channel Mapping
    kFS1RegShiftAudioChannelMapping_Gain = 0,
    kFS1RegShiftAudioChannelMapping_Phase = 15,
    kFS1RegShiftAudioChannelMapping_Source = 16,
    kFS1RegShiftAudioChannelMapping_Mute = 31,

    // FS1 Output Timing Fine Phase Adjust
    kRegShiftOutputTimingFinePhase = 0,
	
    //kRegAnalogInputStatus
	kRegShiftAnalogCompositeLocked		= 16,
	kRegShiftAnalogCompositeFormat625	= 18,

	//kRegHDMIInputStatus / kRegAnalogInputStatus
	kRegShiftInputStatusLock			= 0,
	kLHIRegShiftHDMIInputColorSpace		= 2,
	kLHIRegShiftHDMIInputBitDepth		= 3,
	kRegShiftHDMIInputStatusV2Std		= 4,
	kLHIRegShiftHDMIOutputEDIDRGB		= 10,
	kLHIRegShiftHDMIOutputEDID10Bit		= 11,
	kLHIRegShiftHDMIInput2ChAudio		= 12,
	kRegShiftHDMIInputProgressive		= 13,
	kRegShiftAnalogInputSD				= 14,
	kRegShiftAnalogInputIntegerRate		= 15,
	kLHIRegShiftHDMIOutputEDIDDVI		= 15,
	kRegShiftInputStatusStd				= 24,
	kLHIRegShiftHDMIInputProtocol		= 27,
	kRegShiftInputStatusFPS				= 28,

	//kRegAnalogInputControl
	kRegShiftAnalogInputADCMode			= 0,
	
	//kRegHDMIOut3DControl
	kRegShiftHDMIOut3DPresent			= 3,
	kRegShiftHDMIOut3DMode				= 4,
	
	//kRegHDMIOutControl
	kRegShiftHDMIOutVideoStd			= 0,
	kLHIRegShiftHDMIDownStreamDeviceYCbCrMode = 6,
	kLHIRegShiftHDMIDownStreamDevice10BitMode = 7,
	kRegShiftHDMIV2TxBypass				= 7,
	kLHIRegShiftHDMIOutColorSpace		= 8,
	kLHIRegShiftHDMIOutFPS				= 9,
	kRegShiftHDMIOutProgressive			= 13,
	kLHIRegShiftHDMIOutBitDepth			= 14,
	kRegShiftHDMISampling				= 18,
	kRegShiftHDMIOutPowerDown			= 25,
	kRegShiftHDMIOutRange				= 28,
	kRegShiftHDMIOutAudioCh				= 29,
	kLHIRegShiftHDMIOutDVI 				= 30,
	
	//kRegHDMIInputControl
	kRefShiftHDMIAudioPairSelect		= 2,
	kRegShiftHDMISampleRateConverterEnable = 4,
	kRegShiftHDMIInputRange				= 28,
	kRegShiftHDMIInputPolarity			= 16,
	
	//kRegHDMIInputControl / kRegHDMIOutControl
	kRegShiftHDMIColorSpace				= 4,
	kRegShiftHDMIProtocol				= 30,

	//kK2RegAnalogOutControl,
	kK2RegShiftVideoDACMode				= 0,
	kFS1RegShiftVideoDAC2Mode			= 8,
	kLHIRegShiftVideoDACStandard		= 13,
	kLSRegShiftVideoADCMode				= 16,
	kLHIRegShiftVideoDACMode			= 21,		// 4 bit enum equivalent of bit 21-24
	kLHIRegShiftVideoDACSetup			= 21,
	kLHIRegShiftVideoDACJapan			= 22,
	kLHIRegShiftVideoDACRGB				= 23,
	kLHIRegShiftVideoDACComponent		= 24,
	kK2RegShiftOutHTiming				= 24,

	//kK2RegSDIOut1Control + kRegK2SDIOut2Control + kK2RegAnalogOutControl
	kK2RegShiftSDIOutStandard			= 0,
	kK2RegShiftSDI1Out_2Kx1080Mode		= 3,
	kLHRegShiftVideoOutputDigitalSelect	= 4,
	kK2RegShiftSDIOutHBlankRGBRange		= 7,
	kLHRegShiftVideoOutputAnalogSelect  = 8,
	kRegShiftSDIOutLevelAtoLevelB		= 23,
	kLHIRegShiftSDIOut3GbpsMode			= 24,
	kLHIRegShiftSDIOutSMPTELevelBMode	= 25,
	kK2RegShiftVPIDInsertionEnable		= 26,
	kK2RegShiftVPIDInsertionOverwrite	= 27,
	kK2RegShiftSDIOutDS1AudioSelect		= 28,//30,
	kK2RegShiftSDIOutDS2AudioSelect		= 29,//31,

	//kK2RegConversionControl,
	kK2RegShiftConverterOutStandard		= 12,
	kK2RegShiftConverterOutRate			= 27,
	kK2RegShiftUpConvertMode			= 8,
	kK2RegShiftDownConvertMode			= 4,
	kK2RegShiftConverterInStandard		= 0,
	kK2RegShiftConverterInRate			= 23,
	kK2RegShiftConverterPulldown		= 6,
	kK2RegShiftUCPassLine21				= 16,
	kK2RegShiftUCAutoLine21				= 17,
	kK2RegShiftIsoConvertMode			= 20,
	kK2RegShiftDeinterlaceMode			= 15,
	kK2RegShiftEnableConverter			= 31,

	//kK2RegFrameSync1Control and kK2RegFrameSync2Control
	kK2RegShiftFrameSyncControlFrameDelay = 24,
	kK2RegShiftFrameSyncControlStandard = 8,
	kK2RegShiftFrameSyncControlGeometry = 4,
	kK2RegShiftFrameSyncControlFrameFormat = 0,

	//kK2RegXptSelectGroup1
	kK2RegShiftCompressionModInputSelect = 24,
	kK2RegShiftConversionModInputSelect = 16,
	kK2RegShiftColorSpaceConverterInputSelect = 8, 
	kK2RegShiftXptLUTInputSelect		= 0, 

	//kK2RegXptSelectGroup2
	kK2RegShiftDuallinkOutInputSelect	= 24,
	kK2RegShiftFrameSync2InputSelect	= 16,
	kK2RegShiftFrameSync1InputSelect	= 8,
	kK2RegShiftFrameBuffer1InputSelect	= 0, 

	//kK2RegXptSelectGroup3
	kK2RegShiftCSC1KeyInputSelect		= 24,
	kK2RegShiftSDIOut2InputSelect		= 16,
	kK2RegShiftSDIOut1InputSelect		= 8,
	kK2RegShiftAnalogOutInputSelect		= 0,

	//kK2RegXptSelectGroup4
	kK2RegShiftMixerBGKeyInputSelect	= 24,
	kK2RegShiftMixerBGVidInputSelect	= 16,
	kK2RegShiftMixerFGKeyInputSelect	= 8,
	kK2RegShiftMixerFGVidInputSelect	= 0,

	//kK2RegXptSelectGroup5
	kK2RegShiftCSC2KeyInputSelect		= 24,
	kK2RegShiftCSC2VidInputSelect		= 16,
	kK2RegShiftXptLUT2InputSelect		= 8,
	kK2RegShiftFrameBuffer2InputSelect	= 0,

	//kK2RegXptSelectGroup6
	kK2RegShiftSecondConverterInputSelect = 24,
	kK2RegShiftHDMIOutInputSelect		= 16,
	kK2RegShiftIICTInputSelect			= 8,
	kK2RegShiftWaterMarkerInputSelect	= 0,

	//kK2RegXptSelectGroup7
	kK2RegShiftDuallinkOut2InputSelect	= 16,
	kK2RegShiftIICT2InputSelect			= 8,
	kK2RegShiftWaterMarker2InputSelect	= 0,

	//kK2RegXptSelectGroup8
	kK2RegShiftSDIOut5InputSelect		= 16,
	kK2RegShiftSDIOut4InputSelect		= 8,
	kK2RegShiftSDIOut3InputSelect		= 0,
	
	//kRegCh1ControlExtended
	//kRegCh2ControlExtended
	kK2RegShiftPulldownMode	= 2,
	
	//kK2RegXptSelectGroup9
	kK2RegShiftMixer2BGKeyInputSelect	= 24,
	kK2RegShiftMixer2BGVidInputSelect	= 16,
	kK2RegShiftMixer2FGKeyInputSelect	= 8,
	kK2RegShiftMixer2FGVidInputSelect	= 0,

	//kK2RegXptSelectGroup10
	kK2RegShiftSDIOut2DS2InputSelect	= 8,
	kK2RegShiftSDIOut1DS2InputSelect	= 0,
	
	//kK2RegXptSelectGroup11
	kK2RegShiftDuallinkIn1InputSelect	= 0,
	kK2RegShiftDuallinkIn1DSInputSelect = 8,
	kK2RegShiftDuallinkIn2InputSelect	= 16,
	kK2RegShiftDuallinkIn2DSInputSelect = 24,

	//kK2RegXptSelectGroup12
	kK2RegShiftXptLUT3InputSelect		= 0,
	kK2RegShiftXptLUT4InputSelect		= 8,
	kK2RegShiftXptLUT5InputSelect		= 16,

	//kK2RegXptSelectGroup13
	kK2RegShiftFrameBuffer3InputSelect	= 0,
	kK2RegShiftFrameBuffer4InputSelect	= 16,

	//kK2RegXptSelectGroup14
	kK2RegShiftSDIOut3DS2InputSelect	= 8,
	kK2RegShiftSDIOut5DS2InputSelect	= 16,
	kK2RegShiftSDIOut4DS2InputSelect	= 24,

	//kK2RegXptSelectGroup15
	kK2RegShiftDuallinkIn3InputSelect	= 0,
	kK2RegShiftDuallinkIn3DSInputSelect = 8,
	kK2RegShiftDuallinkIn4InputSelect	= 16,
	kK2RegShiftDuallinkIn4DSInputSelect = 24,

	//kK2RegXptSelectGroup16
	kK2RegShiftDuallinkOut3InputSelect	= 0,
	kK2RegShiftDuallinkOut4InputSelect	= 8,
	kK2RegShiftDuallinkOut5InputSelect	= 16,

	//kK2RegXptSelectGroup17
	kK2RegShiftCSC3VidInputSelect = 0,
	kK2RegShiftCSC3KeyInputSelect = 8,
	kK2RegShiftCSC4VidInputSelect = 16,
	kK2RegShiftCSC4KeyInputSelect = 24,

	//kK2RegXptSelectGroup18
	kK2RegShiftCSC5VidInputSelect = 0,
	kK2RegShiftCSC5KeyInputSelect = 8,

	//kK2RegXptSelectGroup19
	kK2RegShift4KDCQ1InputSelect = 0,
	kK2RegShift4KDCQ2InputSelect = 8,
	kK2RegShift4KDCQ3InputSelect = 16,
	kK2RegShift4KDCQ4InputSelect = 24,

	//kK2RegXptSelectGroup20
	kK2RegShiftHDMIOutV2Q1InputSelect = 0,
	kK2RegShiftHDMIOutV2Q2InputSelect = 8,
	kK2RegShiftHDMIOutV2Q3InputSelect = 16,
	kK2RegShiftHDMIOutV2Q4InputSelect = 24,

	//kK2RegXptSelectGroup21
	kK2RegShiftFrameBuffer5InputSelect	= 0,
	kK2RegShiftFrameBuffer6InputSelect	= 8,
	kK2RegShiftFrameBuffer7InputSelect	= 16,
	kK2RegShiftFrameBuffer8InputSelect	= 24,

	//kK2RegXptSelectGroup22
	kK2RegShiftSDIOut6InputSelect		= 0,
	kK2RegShiftSDIOut6DS2InputSelect	= 8,
	kK2RegShiftSDIOut7InputSelect		= 16,
	kK2RegShiftSDIOut7DS2InputSelect	= 24,

	//kK2RegXptSelectGroup30
	kK2RegShiftSDIOut8InputSelect		= 0,
	kK2RegShiftSDIOut8DS2InputSelect	= 8,
	kK2RegShiftCSC6VidInputSelect		= 16,
	kK2RegShiftCSC6KeyInputSelect		= 24,

	//kK2RegXptSelectGroup23
	kK2RegShiftCSC7VidInputSelect	= 0,
	kK2RegShiftCSC7KeyInputSelect	= 8,
	kK2RegShiftCSC8VidInputSelect	= 16,
	kK2RegShiftCSC8KeyInputSelect	= 24,

	//kK2RegXptSelectGroup24
	kK2RegShiftXptLUT6InputSelect	= 0,
	kK2RegShiftXptLUT7InputSelect	= 8,
	kK2RegShiftXptLUT8InputSelect	= 16,

	//kK2RegXptSelectGroup25
	kK2RegShiftDuallinkIn5InputSelect	= 0,
	kK2RegShiftDuallinkIn5DSInputSelect	= 8,
	kK2RegShiftDuallinkIn6InputSelect	= 16,
	kK2RegShiftDuallinkIn6DSInputSelect	= 24,

	//kK2RegXptSelectGroup26
	kK2RegShiftDuallinkIn7InputSelect	= 0,
	kK2RegShiftDuallinkIn7DSInputSelect	= 8,
	kK2RegShiftDuallinkIn8InputSelect	= 16,
	kK2RegShiftDuallinkIn8DSInputSelect	= 24,

	//kK2RegXptSelectGroup27
	kK2RegShiftDuallinkOut6InputSelect	= 0,
	kK2RegShiftDuallinkOut7InputSelect	= 8,
	kK2RegShiftDuallinkOut8InputSelect	= 16,

	//kK2RegXptSelectGroup28
	kK2RegShiftMixer3FGVidInputSelect	= 0,
	kK2RegShiftMixer3FGKeyInputSelect	= 8,
	kK2RegShiftMixer3BGVidInputSelect	= 16,
	kK2RegShiftMixer3BGKeyInputSelect	= 24,

	//kK2RegXptSelectGroup29
	kK2RegShiftMixer4FGVidInputSelect	= 0,
	kK2RegShiftMixer4FGKeyInputSelect	= 8,
	kK2RegShiftMixer4BGVidInputSelect	= 16,
	kK2RegShiftMixer4BGKeyInputSelect	= 24,

	//kRegXptSelectGroup31
	kK2RegShift425Mux1AInputSelect = 0,
	kK2RegShift425Mux1BInputSelect = 8,
	kK2RegShift425Mux2AInputSelect = 16,
	kK2RegShift425Mux2BInputSelect = 24,

	//kRegXptSelectGroup32
	kK2RegShift425Mux3AInputSelect = 0,
	kK2RegShift425Mux3BInputSelect = 8,
	kK2RegShift425Mux4AInputSelect = 16,
	kK2RegShift425Mux4BInputSelect = 24,

	//kRegXptSelectGroup33
	kK2RegShiftFrameBuffer1BInputSelect = 0,
	kK2RegShiftFrameBuffer2BInputSelect = 8,
	kK2RegShiftFrameBuffer3BInputSelect = 16,
	kK2RegShiftFrameBuffer4BInputSelect = 24,

	//kRegXptSelectGroup34
	kK2RegShiftFrameBuffer5BInputSelect = 0,
	kK2RegShiftFrameBuffer6BInputSelect = 8,
	kK2RegShiftFrameBuffer7BInputSelect = 16,
	kK2RegShiftFrameBuffer8BInputSelect = 24,

	//kK2RegCSCoefficients1_2
	kK2RegShiftVidKeySyncStatus			= 28,
	kK2RegShiftMakeAlphaFromKeySelect	= 29,
	kK2RegShiftColorSpaceMatrixSelect	= 30,
	kK2RegShiftUseCustomCoefSelect		= 31,
	//kK2RegCSCoefficients3_4
	kK2RegShiftXena2RGBRange			= 31,	
	
	kK2RegShiftCustomCoefficientLow		= 0,
	kK2RegShiftCustomCoefficientHigh	= 16,
	
	// Xena2K and Konax Video Processing.
	kK2RegShiftXena2FgVidProcInputControl	= 20,
	kK2RegShiftXena2BgVidProcInputControl	= 22,
	kK2RegShiftXena2VidProcMode				= 24,
	kK2RegShiftXena2VidProcSplitStd 		= 28,
	kK2RegShiftVidProcSubtitleEnable		= 31,

	// the newer 13(12) bit coefficients end on bit 14 unlike the
	// 11(10) bit ones on 16 - jac
	kK2RegShiftCustomCoefficient12BitLow  = 0,
	kK2RegShiftCustomCoefficient12BitHigh = 14,
	
	//kRegLTCStatusControl
	kRegShiftLTC1InPresent	= 0,
	kRegShiftLTC2InPresent	= 8,

	#if !defined (NTV2_DEPRECATE)
		//
		// Borg Fusion Registers
		//

		// Boot FPGA and BoardID
		kRegShiftBorgFusionBootFPGAVer = 4,
		kRegShiftBorgFusionBoardID = 0,

		// Codec and convert FPGA configuration control
		kRegShiftBorgFusionCodecFPGAProgram = 0,
		kRegShiftBorgFusionCodecFPGAInit = 1,
		kRegShiftBorgFusionCodecFPGADone = 2,

		kRegShiftBorgFusionConvertFPGAProgram = 4,
		kRegShiftBorgFusionConvertFPGAInit = 5,
		kRegShiftBorgFusionConvertFPGADone = 6,

		// Panel Push buttons debounced and SATA drive present state
		kRegShiftBorgFusionPushButtonStatusDebounced = 0,
		kRegShiftBorgFusionPushButtonConfigDebounced = 1,
		kRegShiftBorgFusionPushButtonMediaDebounced = 2,
		kRegShiftBorgFusionPushButtonRewindDebounced = 3,
		kRegShiftBorgFusionPushButtonStopDebounced = 4,
		kRegShiftBorgFusionPushButtonPlayDebounced = 5,
		kRegShiftBorgFusionPushButtonRecordDebounced = 6,
		kRegShiftBorgFusionPushButtonFastFwdDebounced = 7,
		kRegShiftBorgFusionPushButtonSelectUpDebounced = 8,
		kRegShiftBorgFusionPushButtonSelectDownDebounced = 9,
		kRegShiftBorgFusionPushButtonDeleteClipDebounced = 10,
		kRegShiftBorgFusionPushButtonAdjustUpDebounced = 11,
		kRegShiftBorgFusionPushButtonAdjustDownDebounced = 12,
		kRegShiftBorgFusionPushButtonSlotDebounced = 13,
		kRegShiftBorgFusionPushButtonSATADrivePresentDebounced = 14,

		// Panel Push buttons and SATA drive present changes
		kRegShiftBorgFusionPushButtonStatusChange = 0,
		kRegShiftBorgFusionPushButtonConfigChange = 1,
		kRegShiftBorgFusionPushButtonMediaChange = 2,
		kRegShiftBorgFusionPushButtonRewindChange = 3,
		kRegShiftBorgFusionPushButtonStopChange = 4,
		kRegShiftBorgFusionPushButtonPlayChange = 5,
		kRegShiftBorgFusionPushButtonRecordChange = 6,
		kRegShiftBorgFusionPushButtonFastFwdChange = 7,
		kRegShiftBorgFusionPushButtonSelectUpChange = 8,
		kRegShiftBorgFusionPushButtonSelectDownChange = 9,
		kRegShiftBorgFusionPushButtonDeleteClipChange = 10,
		kRegShiftBorgFusionPushButtonAdjustUpChange = 11,
		kRegShiftBorgFusionPushButtonAdjustDownChange = 12,
		kRegShiftBorgFusionPushButtonSlotChange = 13,
		kRegShiftBorgFusionPushButtonSATADrivePresentChange = 14,

		// LED Pulse Width Modulation Threshholds
		kRegShiftBorgFusionPWMThreshExpressCard2 = 0,
		kRegShiftBorgFusionPWMThreshExpressCard1 = 4,
		kRegShiftBorgFusionPWMThreshPower = 8,
		kRegShiftBorgFusionPWMThreshLCDBacklightLED = 12,


		// Power control - System
		kRegShiftBorgFusionPowerCtrlWiFiReset = 0,
		kRegShiftBorgFusionPowerCtrlFirewirePower = 1,
		kRegShiftBorgFusionPowerCtrlGigEthReset = 2,
		kRegShiftBorgFusionPowerCtrlPCIExpClockStop = 3,

		// Power control - Storage devices
		kRegShiftBorgFusionPowerCtrlPCIExpCard1_3_3vPower = 8,	// Express Card 1 3.3v power
		kRegShiftBorgFusionPowerCtrlPCIExpCard1_1_5vPower = 9,	// Express Card 1 1.5v power
		kRegShiftBorgFusionPowerCtrlPCIExpCard2_3_3vPower = 10,	// Express Card 2 3.3v power
		kRegShiftBorgFusionPowerCtrlPCIExpCard2_1_5vPower = 11,	// Express Card 2 1.5v power
		kRegShiftBorgFusionPowerCtrlSata_12vPower = 12,			// SATA Drive 12v power

		kRegShiftBonesActelPowerCtrlCFSlot2_BridgeReset = 8,
		kRegShiftBonesActelPowerCtrlCFSlot2_Power = 9,   // Compact Flash S2 Power
		kRegShiftBonesActelPowerCtrlCFSlot1_Power = 10,  // Compact Flash S1 Power
		kRegShiftBonesActelPowerCtrlCFSlot1_BridgeReset = 11,

		// Power control - Pushbutton LEDs
		kRegShiftBorgFusionPowerCtrlPCIExpCard2LED = 13,
		kRegShiftBorgFusionPowerCtrlPCIExpCard1LED = 14,
		kRegShiftBorgFusionPowerCtrlPowerButtonLED = 15,

		// IRQ3n Interrupt control
		kRegShiftBorgFusionIRQ3nIntCtrlPushButtonChangeEnable = 0,
		kRegShiftBorgFusionIRQ3nIntCtrlInputVoltageLow9vEnable = 1,
		kRegShiftBorgFusionIRQ3nIntCtrlDisplayFIFOFullEnable = 2,
		kRegShiftBorgFusionIRQ3nIntCtrlSATAPresentChangeEnable = 3,
		kRegShiftBorgFusionIRQ3nIntCtrlTemp1HighEnable = 4,
		kRegShiftBorgFusionIRQ3nIntCtrlTemp2HighEnable = 5,
		kRegShiftBorgFusionIRQ3nIntCtrlPowerButtonChangeEnable = 6,

		// IRQ3n Interrupt source
		kRegShiftBorgFusionIRQ3nIntSrcPushButtonChange= 0,
		kRegShiftBorgFusionIRQ3nIntSrcInputVoltageLow9v= 1,
		kRegShiftBorgFusionIRQ3nIntSrcDisplayFIFOFull= 2,
		kRegShiftBorgFusionIRQ3nIntSrcSATAPresentChange= 3,
		kRegShiftBorgFusionIRQ3nIntSrcTemp1High= 4,
		kRegShiftBorgFusionIRQ3nIntSrcTemp2High= 5,
		kRegShiftBorgFusionIRQ3nIntSrcPowerButtonChange= 6,

		// Noritake Display Control/Status
		kRegShiftBorgFusionDisplayCtrlReset = 0,
		kRegShiftBorgFusionDisplayStatusBusyRaw = 1,		// Not needed by CPU, used internally by FPGA
		kRegShiftBorgFusionDisplayStatusInterfaceBusy = 7,	// FIFO full

		// Analog ADC flags - battery
		kRegShiftBorgFusionAnalogFlagsPowerLTE9v = 0,	// +12 v supply <= 9.0 v battery critical
		kRegShiftBorgFusionAnalogFlagsPowerLTE10v = 1,	// +12 v supply <= 10.0 v battery depleting
		kRegShiftBorgFusionAnalogFlagsPowerLTE11v = 2,	// +12 v supply <= 11.0 v battery depleting
		kRegShiftBorgFusionAnalogFlagsPowerGTE13v = 3,	// +12 v supply >= 13.0 v battery charging

		// Analog ADC flags - temperature sensor
		kRegShiftBorgFusionAnalogFlagsPowerTemp1High = 4,	// Temp sensor 1 > 65 C
		kRegShiftBorgFusionAnalogFlagsPowerTemp2High = 5,	// Temp sensor 2 > 65 C
	#endif	//	!defined (NTV2_DEPRECATE)

	// kRegAudioOutputSourceMap
	kRegShiftMonitorSource					= 16,
	kRegShiftHDMIOutAudioSource				= 24,
	
	#if !defined (NTV2_DEPRECATE)
		// kRegSDIInput3GStatus
		kLHIRegShiftSDIIn3GbpsMode 				= 0,
		kLHIRegShiftSDIIn3GbpsSMPTELevelBMode 	= 1,
		kLHIRegShiftSDIInVPIDLinkAValid 		= 4,
		kLHIRegShiftSDIInVPIDLinkBValid 		= 5,
		kLHIRegShiftSDIIn23GbpsMode 			= 8,
		kLHIRegShiftSDIIn23GbpsSMPTELevelBMode 	= 9,
		kLHIRegShiftSDIIn2VPIDLinkAValid 		= 12,
		kLHIRegShiftSDIIn2VPIDLinkBValid 		= 13,

		// kRegSDIInput3GStatus2
		kLHIRegShiftSDIIn33GbpsMode 			= 0,
		kLHIRegShiftSDIIn33GbpsSMPTELevelBMode 	= 1,
		kLHIRegShiftSDIIn3VPIDLinkAValid 		= 4,
		kLHIRegShiftSDIIn3VPIDLinkBValid 		= 5,
		kLHIRegShiftSDIIn43GbpsMode 			= 8,
		kLHIRegShiftSDIIn43GbpsSMPTELevelBMode 	= 9,
		kLHIRegShiftSDIIn4VPIDLinkAValid 		= 12,
		kLHIRegShiftSDIIn4VPIDLinkBValid 		= 13,
	#endif	//	!defined (NTV2_DEPRECATE)

	// kRegSDIInput3GStatus
	kRegShiftSDIIn3GbpsMode 			= 0,
	kRegShiftSDIIn3GbpsSMPTELevelBMode 	= 1,
	kRegShiftSDIIn1LevelBtoLevelA		= 2,
	kRegShiftSDIInVPIDLinkAValid 		= 4,
	kRegShiftSDIInVPIDLinkBValid 		= 5,
	kRegShiftSDIIn23GbpsMode 			= 8,
	kRegShiftSDIIn23GbpsSMPTELevelBMode = 9,
	kRegShiftSDIIn2LevelBtoLevelA		= 10,
	kRegShiftSDIIn2VPIDLinkAValid 		= 12,
	kRegShiftSDIIn2VPIDLinkBValid 		= 13,

	// kRegSDIInput3GStatus2
	kRegShiftSDIIn33GbpsMode 			= 0,
	kRegShiftSDIIn33GbpsSMPTELevelBMode	= 1,
	kRegShiftSDIIn3LevelBtoLevelA		= 2,
	kRegShiftSDIIn3VPIDLinkAValid 		= 4,
	kRegShiftSDIIn3VPIDLinkBValid 		= 5,
	kRegShiftSDIIn43GbpsMode 			= 8,
	kRegShiftSDIIn43GbpsSMPTELevelBMode = 9,
	kRegShiftSDIIn4LevelBtoLevelA		= 10,
	kRegShiftSDIIn4VPIDLinkAValid 		= 12,
	kRegShiftSDIIn4VPIDLinkBValid 		= 13,

	// kRegSDI5678Input3GStatus
	kRegShiftSDIIn53GbpsMode			= 0,
	kRegShiftSDIIn53GbpsSMPTELevelBMode	= 1,
	kRegShiftSDIIn5LevelBtoLevelA		= 2,
	kRegShiftSDIIn5VPIDLinkAValid		= 4,
	kRegShiftSDIIn5VPIDLinkBValid		= 5,
	kRegShiftSDIIn63GbpsMode			= 8,
	kRegShiftSDIIn63GbpsSMPTELevelBMode	= 9,
	kRegShiftSDIIn6LevelBtoLevelA		= 10,
	kRegShiftSDIIn6VPIDLinkAValid		= 12,
	kRegShiftSDIIn6VPIDLinkBValid		= 13,
	kRegShiftSDIIn73GbpsMode			= 16,
	kRegShiftSDIIn73GbpsSMPTELevelBMode	= 17,
	kRegShiftSDIIn7LevelBtoLevelA		= 18,
	kRegShiftSDIIn7VPIDLinkAValid		= 20,
	kRegShiftSDIIn7VPIDLinkBValid		= 21,
	kRegShiftSDIIn83GbpsMode			= 24,
	kRegShiftSDIIn83GbpsSMPTELevelBMode	= 25,
	kRegShiftSDIIn8LevelBtoLevelA		= 26,
	kRegShiftSDIIn8VPIDLinkAValid		= 28,
	kRegShiftSDIIn8VPIDLinkBValid		= 29,

	// kRegVPID
	kRegShiftVPIDBitDepth				= 0,
	kRegShiftVPIDDynamicRange			= 3,
	kRegShiftVPIDChannel				= 6,
	kRegShiftVPIDSampling				= 8,
	kRegShiftVPIDHorizontalSampling		= 14,
	kRegShiftVPIDImageAspect16x9		= 15,
	kRegShiftVPIDPictureRate			= 16,
	kRegShiftVPIDProgressivePicture		= 22,
	kRegShiftVPIDProgressiveTransport	= 23,
	kRegShiftVPIDStandard				= 24,
	kRegShiftVPIDVersionID				= 31,

	// Borg Test Pattern Generator
	kRegShiftTPGChromaSample            = 0,
	kRegShiftTPGLineBuffer              = 10,
	kRegShiftTPGFrameRate               = 12,
	kRegShiftTPGLuma                    = 16,
	kRegShiftTPGMulti                   = 26,
	kRegShiftTPGReset                   = 27,
	kRegShiftTPGStandard                = 28,
	kRegShiftTPGWriteEnable             = 31,

	// Audio Channel Control 2 or 8 channel playback shift
	kRegAudControlShift					= 5,

	// Stereo Compressor control shift
	kRegShiftStereoCompressorOutputMode		= 0,
	kRegShiftStereoCompressorFlipMode		= 4,
	kRegShiftStereoCompressorFlipLeftHorz	= 4,
	kRegShiftStereoCompressorFlipLeftVert	= 5,
	kRegShiftStereoCompressorFlipRightHorz	= 6,
	kRegShiftStereoCompressorFlipRightVert	= 7,
	kRegShiftStereoCompressorFormat			= 8,
	kRegShiftStereoCompressorLeftSource		= 16,
	kRegShiftStereoCompressorRightSource	= 24,

	// SDI Direction Control Shift
	kRegShiftSDI5Transmit	= 24,
	kRegShiftSDI6Transmit	= 25,
	kRegShiftSDI7Transmit	= 26,
	kRegShiftSDI8Transmit	= 27,
	kRegShiftSDI1Transmit	= 28,
	kRegShiftSDI2Transmit	= 29,
	kRegShiftSDI3Transmit	= 30,
	kRegShiftSDI4Transmit	= 31,
	
	// SDI watchdog control
	kRegShiftSDIRelayControl12		= 0,
	kRegShiftSDIRelayControl34		= 1,
	kRegShiftSDIWatchdogEnable12	= 4,
	kRegShiftSDIWatchdogEnable34	= 5,
	kRegShiftSDIRelayPosition12		= 8,
	kRegShiftSDIRelayPosition34		= 9,
	kRegShiftSDIWatchdogStatus		= 12,

	kShiftDisplayMode		= 4,

	// 4K Down Convert
	kRegShift4KDCRGBMode	= 0,
	kRegShift4KDCYCC444Mode	= 1,
	kRegShift4KDCPSFInMode	= 2,
	kRegShift4KDCPSFOutMode	= 3,

	// Quadrant Rasterizer Control
	kRegShiftRasterMode		= 0,
	kRegShiftRasterLevelB	= 4,
	kRegShiftRasterDecimate	= 8,

	kRegShiftTRSErrorSDI1		= 0,
	kRegShiftTRSErrorSDI2		= 1,
	kRegShiftTRSErrorSDI3		= 2,
	kRegShiftTRSErrorSDI4		= 3,
	kRegShiftTRSErrorSDI5		= 4,
	kRegShiftTRSErrorSDI6		= 5,
	kRegShiftTRSErrorSDI7		= 6,
	kRegShiftTRSErrorSDI8		= 7,
	kRegShiftLockSDI1			= 16,
	kRegShiftLockSDI2			= 17,
	kRegShiftLockSDI3			= 18,
	kRegShiftLockSDI4			= 19,
	kRegShiftLockSDI5			= 20,
	kRegShiftLockSDI6			= 21,
	kRegShiftLockSDI7			= 22,
	kRegShiftLockSDI8			= 23,

	kRegShift2MFrameSize		= 0,
	kRegShiftChannelBar			= 16,

	kRegShiftPCMControlA1P1_2		= 0,
	kRegShiftPCMControlA1P3_4		= 1,
	kRegShiftPCMControlA1P5_6		= 2,
	kRegShiftPCMControlA1P7_8		= 3,
	kRegShiftPCMControlA1P9_10		= 4,
	kRegShiftPCMControlA1P11_12		= 5,
	kRegShiftPCMControlA1P13_14		= 6,
	kRegShiftPCMControlA1P15_16		= 7,

	kRegShiftPCMControlA2P1_2		= 8,
	kRegShiftPCMControlA2P3_4		= 9,
	kRegShiftPCMControlA2P5_6		= 10,
	kRegShiftPCMControlA2P7_8		= 11,
	kRegShiftPCMControlA2P9_10		= 12,
	kRegShiftPCMControlA2P11_12		= 13,
	kRegShiftPCMControlA2P13_14		= 14,
	kRegShiftPCMControlA2P15_16		= 15,

	kRegShiftPCMControlA3P1_2		= 16,
	kRegShiftPCMControlA3P3_4		= 17,
	kRegShiftPCMControlA3P5_6		= 18,
	kRegShiftPCMControlA3P7_8		= 19,
	kRegShiftPCMControlA3P9_10		= 20,
	kRegShiftPCMControlA3P11_12		= 21,
	kRegShiftPCMControlA3P13_14		= 22,
	kRegShiftPCMControlA3P15_16		= 23,

	kRegShiftPCMControlA4P1_2		= 24,
	kRegShiftPCMControlA4P3_4		= 25,
	kRegShiftPCMControlA4P5_6		= 26,
	kRegShiftPCMControlA4P7_8		= 27,
	kRegShiftPCMControlA4P9_10		= 28,
	kRegShiftPCMControlA4P11_12		= 29,
	kRegShiftPCMControlA4P13_14		= 30,
	kRegShiftPCMControlA4P15_16		= 31,

	kRegShiftPCMControlA5P1_2		= 0,
	kRegShiftPCMControlA5P3_4		= 1,
	kRegShiftPCMControlA5P5_6		= 2,
	kRegShiftPCMControlA5P7_8		= 3,
	kRegShiftPCMControlA5P9_10		= 4,
	kRegShiftPCMControlA5P11_12		= 5,
	kRegShiftPCMControlA5P13_14		= 6,
	kRegShiftPCMControlA5P15_16		= 7,

	kRegShiftPCMControlA6P1_2		= 8,
	kRegShiftPCMControlA6P3_4		= 9,
	kRegShiftPCMControlA6P5_6		= 10,
	kRegShiftPCMControlA6P7_8		= 11,
	kRegShiftPCMControlA6P9_10		= 12,
	kRegShiftPCMControlA6P11_12		= 13,
	kRegShiftPCMControlA6P13_14		= 14,
	kRegShiftPCMControlA6P15_16		= 15,

	kRegShiftPCMControlA7P1_2		= 16,
	kRegShiftPCMControlA7P3_4		= 17,
	kRegShiftPCMControlA7P5_6		= 18,
	kRegShiftPCMControlA7P7_8		= 19,
	kRegShiftPCMControlA7P9_10		= 20,
	kRegShiftPCMControlA7P11_12		= 21,
	kRegShiftPCMControlA7P13_14		= 22,
	kRegShiftPCMControlA7P15_16		= 23,

	kRegShiftPCMControlA8P1_2		= 24,
	kRegShiftPCMControlA8P3_4		= 25,
	kRegShiftPCMControlA8P5_6		= 26,
	kRegShiftPCMControlA8P7_8		= 27,
	kRegShiftPCMControlA8P9_10		= 28,
	kRegShiftPCMControlA8P11_12		= 29,
	kRegShiftPCMControlA8P13_14		= 30,
	kRegShiftPCMControlA8P15_16		= 31
	
} RegisterShift;


// NWL Registers

// For the Mac we define an offset.  This is done so that read/write register can easily identify an NWL register from a VP regiser.  
// This is necessary since the NWL registers lives in another PCI BAR and these offets conflict with normal VP registers.  With this 
// offset the driver knows which mapped BAR to use.  Windows maps individual registers at start so this isn't necessary for Windows.
#ifdef AJAMac
	#define NWL_REG_START 12000
#else
	#define NWL_REG_START 0
#endif

typedef enum
{
	kRegNwlS2C1Capabilities					= 0x0000+(NWL_REG_START),
	kRegNwlS2C1ControlStatus				= 0x0001+(NWL_REG_START),
	kRegNwlS2C1ChainStartAddressLow			= 0x0002+(NWL_REG_START),
	kRegNwlS2C1ChainStartAddressHigh		= 0x0003+(NWL_REG_START),
	kRegNwlS2C1HardwareTime					= 0x0004+(NWL_REG_START),
	kRegNwlS2C1ChainCompleteByteCount		= 0x0005+(NWL_REG_START),

	kRegNwlS2C2Capabilities					= 0x0040+(NWL_REG_START),
	kRegNwlS2C2ControlStatus				= 0x0041+(NWL_REG_START),
	kRegNwlS2C2ChainStartAddressLow			= 0x0042+(NWL_REG_START),
	kRegNwlS2C2ChainStartAddressHigh		= 0x0043+(NWL_REG_START),
	kRegNwlS2C2HardwareTime					= 0x0044+(NWL_REG_START),
	kRegNwlS2C2ChainCompleteByteCount		= 0x0045+(NWL_REG_START),

	kRegNwlC2S1Capabilities					= 0x0800+(NWL_REG_START),
	kRegNwlC2S1ControlStatus				= 0x0801+(NWL_REG_START),
	kRegNwlC2S1ChainStartAddressLow			= 0x0802+(NWL_REG_START),
	kRegNwlC2S1ChainStartAddressHigh		= 0x0803+(NWL_REG_START),
	kRegNwlC2S1HardwareTime					= 0x0804+(NWL_REG_START),
	kRegNwlC2S1ChainCompleteByteCount		= 0x0805+(NWL_REG_START),

	kRegNwlC2S2Capabilities					= 0x0840+(NWL_REG_START),
	kRegNwlC2S2ControlStatus				= 0x0841+(NWL_REG_START),
	kRegNwlC2S2ChainStartAddressLow			= 0x0842+(NWL_REG_START),
	kRegNwlC2S2ChainStartAddressHigh		= 0x0843+(NWL_REG_START),
	kRegNwlC2S2HardwareTime					= 0x0844+(NWL_REG_START),
	kRegNwlC2S2ChainCompleteByteCount		= 0x0845+(NWL_REG_START),

	kRegNwlCommonControlStatus				= 0x1000+(NWL_REG_START),
	kRegNwlCommonBackEndCoreVersion			= 0x1001+(NWL_REG_START),
	kRegNwlCommonPCIExpressCoreVersion		= 0x1002+(NWL_REG_START),
	kRegNwlCommonUserVersion		   		= 0x1003+(NWL_REG_START)

} NwlRegisterNum;

// This is an aid for utility routines that maintain tables indexed by a register number, like spinlocks
typedef enum
{
	kRegNwlS2C1CapabilitiesIndex,				// 0
	kRegNwlS2C1ControlStatusIndex,				// 1
	kRegNwlS2C1ChainStartAddressLowIndex,		// 2
	kRegNwlS2C1ChainStartAddressHighIndex,		// 3
	kRegNwlS2C1HardwareTimeIndex,				// 4
	kRegNwlS2C1ChainCompleteByteCountIndex,		// 5

	kRegNwlS2C2CapabilitiesIndex,				// 6
	kRegNwlS2C2ControlStatusIndex,				// 7
	kRegNwlS2C2ChainStartAddressLowIndex,		// 8
	kRegNwlS2C2ChainStartAddressHighIndex,		// 9
	kRegNwlS2C2HardwareTimeIndex,				// 10
	kRegNwlS2C2ChainCompleteByteCountIndex,		// 11

	kRegNwlC2S1CapabilitiesIndex,				// 12
	kRegNwlC2S1ControlStatusIndex,				// 13
	kRegNwlC2S1ChainStartAddressLowIndex,		// 14
	kRegNwlC2S1ChainStartAddressHighIndex,		// 15
	kRegNwlC2S1HardwareTimeIndex,				// 16
	kRegNwlC2S1ChainCompleteByteCountIndex,		// 17

	kRegNwlC2S2CapabilitiesIndex,				// 18
	kRegNwlC2S2ControlStatusIndex,				// 19
	kRegNwlC2S2ChainStartAddressLowIndex,		// 20
	kRegNwlC2S2ChainStartAddressHighIndex,		// 21
	kRegNwlC2S2HardwareTimeIndex,				// 22
	kRegNwlC2S2ChainCompleteByteCountIndex,		// 23

	kRegNwlCommonControlStatusIndex,			// 24
	kRegNwlCommonBackEndCoreVersionIndex,		// 25
	kRegNwlCommonPCIExpressCoreVersionIndex,	// 26
	kRegNwlCommonUserVersionIndex,		   		// 27

	NUM_NWL_REGS
} NwlRegisterIndex;

typedef enum
{
	kRegMaskNwlCapabilitiesPresent				= BIT(0),
	kRegMaskNwlCapabilitiesEngineDirection		= BIT(1),
	kRegMaskNwlCapabilitiesEngineType			= (BIT(2)+BIT(3)),
	kRegMaskNwlCapabilitiesEngineNumber			= (BIT(8)+BIT(9)+BIT(10)+BIT(11)+BIT(12)+BIT(13)+BIT(14)+BIT(15)),
	kRegMaskNwlCapabilitiesAddressSize			= (BIT(16)+BIT(17)+BIT(18)+BIT(19)+BIT(20)+BIT(21)+BIT(22)+BIT(23)),

	kRegMaskNwlControlStatusInterruptEnable		= BIT(0),
	kRegMaskNwlControlStatusInterruptActive		= BIT(1),
	kRegMaskNwlControlStatusChainStart			= BIT(8),
	kRegMaskNwlControlStatusDmaResetRequest		= BIT(9),
	kRegMaskNwlControlStatusChainRunning		= BIT(10),
	kRegMaskNwlControlStatusChainComplete		= BIT(11),
	kRegMaskNwlControlStatusChainErrorShort		= BIT(12),
	kRegMaskNwlControlStatusChainSoftwareShort	= BIT(13),
	kRegMaskNwlControlStatusChainHardwareShort	= BIT(14),
	kRegMaskNwlControlStatusAlignmentError		= BIT(15),
	kRegMaskNwlControlStatusDmaReset			= BIT(23),

	kRegMaskNwlCommonDmaInterruptEnable			= BIT(0),
	kRegMaskNwlCommonDmaInterruptActive 		= BIT(1),
	kRegMaskNwlCommonDmaInterruptPending 		= BIT(2),
	kRegMaskNwlCommonInterruptMode 				= BIT(3),
	kRegMaskNwlCommonUserInterruptEnable 		= BIT(4),
	kRegMaskNwlCommonUserInterruptActive 		= BIT(5),
	kRegMaskNwlCommonMaxPayloadSize 			= (BIT(8)+BIT(9)+BIT(10)),
	kRegMaskNwlCommonMaxReadRequestSize 		= (BIT(12)+BIT(13) +BIT(14)),
	kRegMaskNwlCommonS2CInterruptStatus0 		= BIT(16),
	kRegMaskNwlCommonS2CInterruptStatus1 		= BIT(17),
	kRegMaskNwlCommonS2CInterruptStatus2 		= BIT(18),
	kRegMaskNwlCommonS2CInterruptStatus3 		= BIT(19),
	kRegMaskNwlCommonS2CInterruptStatus4 		= BIT(20),
	kRegMaskNwlCommonS2CInterruptStatus5 		= BIT(21),
	kRegMaskNwlCommonS2CInterruptStatus6 		= BIT(22),
	kRegMaskNwlCommonS2CInterruptStatus7 		= BIT(23),
	kRegMaskNwlCommonC2SInterruptStatus0 		= BIT(24),
	kRegMaskNwlCommonC2SInterruptStatus1 		= BIT(25),
	kRegMaskNwlCommonC2SInterruptStatus2 		= BIT(26),
	kRegMaskNwlCommonC2SInterruptStatus3 		= BIT(27),
	kRegMaskNwlCommonC2SInterruptStatus4 		= BIT(28),
	kRegMaskNwlCommonC2SInterruptStatus5 		= BIT(29),
	kRegMaskNwlCommonC2SInterruptStatus6 		= BIT(30),
	kRegMaskNwlCommonC2SInterruptStatus7 		= BIT(31)
   
} NwlRegisterMask;

typedef enum
{
	kRegShiftNwlCapabilitiesPresent				= 0,
	kRegShiftNwlCapabilitiesEngineDirection		= 1,
	kRegShiftNwlCapabilitiesEngineType			= 2,
	kRegShiftNwlCapabilitiesEngineNumber		= 8,
	kRegShiftNwlCapabilitiesAddressSize			= 16,

	kRegShiftNwlControlStatusInterruptEnable	= 0,
	kRegShiftNwlControlStatusInterruptActive	= 1,
	kRegShiftNwlControlStatusChainStart			= 8,
	kRegShiftNwlControlStatusDmaResetRequest	= 9,
	kRegShiftNwlControlStatusChainRunning		= 10,
	kRegShiftNwlControlStatusChainComplete		= 11,
	kRegShiftNwlControlStatusChainErrorShort	= 12,
	kRegShiftNwlControlStatusChainSoftwareShort	= 13,
	kRegShiftNwlControlStatusChainHardwareShort	= 14,
	kRegShiftNwlControlStatusAlignmentError		= 15,
	kRegShiftNwlControlStatusDmaReset			= 23,

	kRegShiftNwlCommonDmaInterruptEnable		= 0,
	kRegShiftNwlCommonDmaInterruptActive 		= 1,
	kRegShiftNwlCommonDmaInterruptPending 		= 2,
	kRegShiftNwlCommonInterruptMode 			= 3,
	kRegShiftNwlCommonUserInterruptEnable 		= 4,
	kRegShiftNwlCommonUserInterruptActive 		= 5,
	kRegShiftNwlCommonMaxPayloadSize 			= 8,
	kRegShiftNwlCommonMaxReadRequestSize 		= 12,
	kRegShiftNwlCommonS2CInterruptStatus0 		= 16,
	kRegShiftNwlCommonS2CInterruptStatus1 		= 17,
	kRegShiftNwlCommonS2CInterruptStatus2 		= 18,
	kRegShiftNwlCommonS2CInterruptStatus3 		= 19,
	kRegShiftNwlCommonS2CInterruptStatus4 		= 20,
	kRegShiftNwlCommonS2CInterruptStatus5 		= 21,
	kRegShiftNwlCommonS2CInterruptStatus6 		= 22,
	kRegShiftNwlCommonS2CInterruptStatus7 		= 23,
	kRegShiftNwlCommonC2SInterruptStatus0 		= 24,
	kRegShiftNwlCommonC2SInterruptStatus1 		= 25,
	kRegShiftNwlCommonC2SInterruptStatus2 		= 26,
	kRegShiftNwlCommonC2SInterruptStatus3 		= 27,
	kRegShiftNwlCommonC2SInterruptStatus4 		= 28,
	kRegShiftNwlCommonC2SInterruptStatus5 		= 29,
	kRegShiftNwlCommonC2SInterruptStatus6 		= 30,
	kRegShiftNwlCommonC2SInterruptStatus7 		= 31

} NwlRegisterShift;


typedef enum
{
	kRegMaskMessageInterruptStatusChannel1		= BIT(0),
	kRegMaskMessageInterruptStatusChannel2		= BIT(1),
	kRegMaskMessageInterruptStatusChannel3		= BIT(2),
	kRegMaskMessageInterruptStatusChannel4		= BIT(3),
	kRegMaskMessageInterruptControlEnable1		= BIT(0),
	kRegMaskMessageInterruptControlEnable2		= BIT(1),
	kRegMaskMessageInterruptControlEnable3		= BIT(2),
	kRegMaskMessageInterruptControlEnable4		= BIT(3),
	kRegMaskMessageInterruptControlEnable5		= BIT(4),
	kRegMaskMessageInterruptControlEnable6		= BIT(5),
	kRegMaskMessageInterruptControlEnable7		= BIT(6),
	kRegMaskMessageInterruptControlEnable8		= BIT(7),
	kRegMaskMessageInterruptControlClear1		= BIT(16),
	kRegMaskMessageInterruptControlClear2		= BIT(17),
	kRegMaskMessageInterruptControlClear3		= BIT(18),
	kRegMaskMessageInterruptControlClear4		= BIT(19),
	kRegMaskMessageInterruptControlClear5		= BIT(20),
	kRegMaskMessageInterruptControlClear6		= BIT(21),
	kRegMaskMessageInterruptControlClear7		= BIT(22),
	kRegMaskMessageInterruptControlClear8		= BIT(23)
} MessageRegisterMask;


typedef enum
{
	kRegShiftMessageInterruptStatusChannel1		= 0,
	kRegShiftMessageInterruptStatusChannel2		= 1,
	kRegShiftMessageInterruptStatusChannel3		= 2,
	kRegShiftMessageInterruptStatusChannel4		= 3,
	kRegShiftMessageInterruptControlEnable1		= 0,
	kRegShiftMessageInterruptControlEnable2		= 1,
	kRegShiftMessageInterruptControlEnable3		= 2,
	kRegShiftMessageInterruptControlEnable4		= 3,
	kRegShiftMessageInterruptControlClear1		= 16,
	kRegShiftMessageInterruptControlClear2		= 17,
	kRegShiftMessageInterruptControlClear3		= 18,
	kRegShiftMessageInterruptControlClear4		= 19
} MessageRegisterShift;


#define	kTransferFlagVideoDMA1			(BIT(0))	// use dma channel 1 for video transfer
#define	kTransferFlagVideoDMA2			(BIT(1))	// use dma channel 2 for video transfer
#define	kTransferFlagVideoDMA3			(BIT(2))	// use dma channel 3 for video tranfer
#define	kTransferFlagVideoDMA4			(BIT(3))	// use dma channel 4 for video transfer
#define	kTransferFlagVideoDMAAny		(BIT(0)+BIT(1)+BIT(2)+BIT(3))
#define	kTransferFlagAudioDMA1			(BIT(4))	// use dma channel 1 for audio transfer
#define	kTransferFlagAudioDMA2			(BIT(5))	// use dma channel 2 for audio transfer
#define	kTransferFlagAudioDMA3			(BIT(6))	// use dma channel 3 for audio transfer
#define	kTransferFlagAudioDMA4			(BIT(7))	// use dma channel 4 for audio transfer
#define	kTransferFlagAudioDMAAny		(BIT(4)+BIT(5)+BIT(6)+BIT(7))
#define kTransferFlagDMAAny				(BIT(0)+BIT(1)+BIT(2)+BIT(3)+BIT(4)+BIT(5)+BIT(6)+BIT(7))
#define kTransferFlagQuadFrame			(BIT(8))	// transfer a quad hd or 4k frame
#define kTransferFlagP2PPrepare			(BIT(9))	// prepare p2p target for synchronous transfer (no message)
#define kTransferFlagP2PComplete		(BIT(10))	// complete synchronous p2p transfer
#define kTransferFlagP2PTarget			(BIT(11))	// prepare p2p target for asynchronous transfer (with message)
#define kTransferFlagP2PTransfer		(BIT(12))	// transfer to p2p sync or async target

#define MAX_FRAMEBUFFERS                111			// Max for Corvid88

////////////////////////////////////////////////////////////////////////////////////

#if !defined (NTV2_DEPRECATE)
	//----------------------- HDNTV ---------------------------------

	// Board-type-specific defines  NOTE: Either KSD, KHD, or HDNTV needs to be defined.
	#define HDNTV_NTV2_DEVICENAME                 ("hdntv")

		// Offset in Base Address 1 Space to Channel 2 Frame Buffer
	#define HDNTV_CHANNEL2_OFFSET                 (0x2000000)              // 32 MBytes

		// Size of each frame buffer
	#define HDNTV_FRAMEBUFFER_SIZE                (0x800000)               //  8 MBytes

	#define HDNTV_NUM_FRAMEBUFFERS                (8)

	#define HDNTV_NTV2_VERTICALINTERRUPT_GLOBAL_EVENT_NAME "_HDVerticalInterruptSignalEvent"
		// the event name shared among all Windows NT
		// This name to be appended to the actual Win32Name
	#define HDNTV_NTV2_CHANGE_GLOBAL_EVENT_NAME "_HDChangeSignalEvent"


	//----------------------- Xena KHD ---------------------------------

		// Offset in Base Address 1 Space to Channel 2 Frame Buffer
	#define KHD_CHANNEL2_OFFSET                 (0x2000000)              // 32 MBytes

		// Size of each frame buffer
	#define KHD_FRAMEBUFFER_SIZE                (0x800000)               //  8 MBytes

	#define KHD_NUM_FRAMEBUFFERS                (32)

	#define KHD_NTV2_VERTICALINTERRUPT_GLOBAL_EVENT_NAME "_HDVerticalInterruptSignalEvent"
		// the event name shared among all Windows NT
		// This name to be appended to the actual Win32Name
	#define KHD_NTV2_CHANGE_GLOBAL_EVENT_NAME "_HDChangeSignalEvent"

	//
	// 2K Specific....only on HD22(for now) boards when in 2K standard
	// 
	#define XENA_FRAMEBUFFERSIZE_2K				(0x1000000) 
	#define XENA_NUMFRAMEBUFFERS_2K				(16) 

	//----------------------- Xena KSD ---------------------------------

		// Offset in Base Address 1 Space to Channel 2 Frame Buffer
	#define KSD_CHANNEL2_OFFSET                 (0x800000)               // 8 MBytes

		// Size of each frame buffer
	#define KSD_FRAMEBUFFER_SIZE                (0x200000)               //  2 MBytes

	#define KSD_NUM_FRAMEBUFFERS                (64)

	#define KSD_NTV2_VERTICALINTERRUPT_GLOBAL_EVENT_NAME "_SDVerticalInterruptSignalEvent"
		// the event name shared among all Windows NT
		// This name to be appended to the actual Win32Name
	#define KSD_NTV2_CHANGE_GLOBAL_EVENT_NAME "_SDChangeSignalEvent"


	//----------------------- AJA KONA ---------------------------------

		// Offset in Base Address 1 Space to Channel 2 Frame Buffer
	#define KONA_CHANNEL2_OFFSET                 (0x2000000)              // 32 MBytes

		// Size of each frame buffer
	#define KONA_FRAMEBUFFER_SIZE                (0x800000)               //  8 MBytes

	#define KONA_NUM_FRAMEBUFFERS                (32)

	#define KHD_NTV2_VERTICALINTERRUPT_GLOBAL_EVENT_NAME "_HDVerticalInterruptSignalEvent"
		// the event name shared among all Windows NT
		// This name to be appended to the actual Win32Name
	#define KHD_NTV2_CHANGE_GLOBAL_EVENT_NAME "_HDChangeSignalEvent"


	//----------------------- AJA XENALS ---------------------------------

		// Offset in Base Address 1 Space to Channel 2 Frame Buffer
	#define XENALS_CHANNEL2_OFFSET                 (0x2000000)              // 32 MBytes..not applicable

		// Size of each frame buffer
	#define XENALS_FRAMEBUFFER_SIZE                (0x800000)               //  8 MBytes

		// the event name shared among all Windows NT
		// This name to be appended to the actual Win32Name
	#define XENALS_NUM_FRAMEBUFFERS                (32)

	//----------------------- AJA FS1  ---------------------------------

		// Offset in Base Address 1 Space to Channel 2 Frame Buffer
	#define FS1_CHANNEL2_OFFSET                 (0x0)						// Framebuffers not accessible to processor

		// Size of each frame buffer
	#define FS1_FRAMEBUFFER_SIZE                (0x0)               		//  Framebuffers not accessible to processor

		// the event name shared among all Windows NT
		// This name to be appended to the actual Win32Name
	#define FS1_NUM_FRAMEBUFFERS                (0)
	#define FS1_NTV2_VERTICALINTERRUPT_GLOBAL_EVENT_NAME "_FS1VerticalInterruptSignalEvent"

		// the event name shared among all Windows NT
		// This name to be appended to the actual Win32Name
	#define FS1_NTV2_CHANGE_GLOBAL_EVENT_NAME "_FS1ChangeSignalEvent"

	//----------------------- AJA BORG ---------------------------------

		// Offset in Base Address 1 Space to Channel 2 Frame Buffer
	// #define BORG_CHANNEL2_OFFSET                 (0)              // not applicable

	//-------------------- Defines for 32 1MB frame buffers (28 1MB video frame buffers and 1 4MB audio buffer)
		// Size of each frame buffer
	//#define BORG_FRAMEBUFFER_SIZE                (0x100000)               //  1 MBytes
	//#define BORG_NUM_FRAMEBUFFERS                (32)

	//-------------------- Defines for 24 1.125MB frame buffers (24 1.125MB video frame buffers and 1 4MB audio buffer)
	//                     this totals 27 MB video frame, 1 MB unused gap, and 1 4MB audio buffer based 4 MB below top of memory

	#define BORG_FRAMEBUFFER_SIZE                (0x120000)                 // 1.125 MBytes
		// this define is left at 32 for practical purposes to locate audio buffer at 4 MB below top of memory (32 - 4 = 28)
		// in reality, there are only 24 video frame buffers, but if you look at usage of this define, it is best left at 32
	#define BORG_NUM_FRAMEBUFFERS                (32)						// 32 * 1.125 = 36
	#define BONES_NUM_FRAMEBUFFERS               (99)						// 99 * 1.125 = ~112 MB
#endif	//	NTV2_DEPRECATE

//----------------------- AJA XENA2 ---------------------------------

	// Offset in Base Address 1 Space to Channel 2 Frame Buffer
#define XENA2_CHANNEL2_OFFSET                 (0x2000000)              // 32 MBytes..not applicable

	// Size of each frame buffer
#define XENA2_FRAMEBUFFER_SIZE                (0x800000)               //  8 MBytes

	// the event name shared among all Windows NT
	// This name to be appended to the actual Win32Name
#define XENA2_NUM_FRAMEBUFFERS                (16)
#define XENA2_NTV2_VERTICALINTERRUPT_GLOBAL_EVENT_NAME "_Xena2VerticalInterruptSignalEvent"

	// the event name shared among all Windows NT
	// This name to be appended to the actual Win32Name
#define KHD_NTV2_CHANGE_GLOBAL_EVENT_NAME "_HDChangeSignalEvent"


//
// Special defines
//
#define NTV2_MIN_FRAMEBUFFERSIZE KSD_FRAMEBUFFER_SIZE
#define NTV2_MAX_FRAMEBUFFERSIZE KHD_FRAMEBUFFER_SIZE
#define NTV2_MIN_FRAMEBUFFERS HDNTV_NUM_FRAMEBUFFERS
#if defined(XENA2)
#define NTV2_MAX_FRAMEBUFFERS MAX_FRAMEBUFFERS
#else
#define NTV2_MAX_FRAMEBUFFERS BONES_NUM_FRAMEBUFFERS 
#endif

#define NTV2_UART_FIFO_SIZE (127)

#define NTV2_PROGRAM_READY_BIT   	( BIT_8 )
#define NTV2_PROGRAM_DONE_BIT    	( BIT_9 )
#define NTV2_PROGRAM_RESET_BIT   	( BIT_10 )

/* PORT C(7) is output (default) */
#define NTV2_FS1_FALLBACK_MODE_BIT  ( BIT_11 )	

/* PORT C(7) is input/3-state */
#define NTV2_FS1_CPLD_ENH_MODE_BIT  ( BIT_12 )	

//////////////////////////////////////////////////////////////////////////////////////
// Enums used to specify Property actions with interrupts
//////////////////////////////////////////////////////////////////////////////////////

typedef enum _INTERRUPT_ENUMS_
{
	eVerticalInterrupt,
	eOutput1				= eVerticalInterrupt,
	eInterruptMask,
	eInput1,
	eInput2,
	eAudio,
	eAudioInWrap,
	eAudioOutWrap,
	eDMA1,
	eDMA2,
	eDMA3,
	eDMA4,
	eChangeEvent,
	eGetIntCount,
	eWrapRate,
    eUartTx,
	eUart1Tx				= eUartTx,
    eUartRx,
	eUart1Rx				= eUartRx,
	eAuxVerticalInterrupt,
	ePushButtonChange,
	eLowPower,
	eDisplayFIFO,
	eSATAChange,
	eTemp1High,
	eTemp2High,
	ePowerButtonChange,
	eInput3,
	eInput4,
    eUartTx2,
	eUart2Tx				= eUartTx2,
    eUartRx2,
	eUart2Rx				= eUartRx2,
	eHDMIRxV2HotplugDetect,
	eInput5,
	eInput6,
	eInput7,
	eInput8,
	eInterruptMask2,
	eOutput2,
	eOutput3,
	eOutput4,
	eOutput5,
	eOutput6,
	eOutput7,
	eOutput8,
	eNumInterruptTypes	//	This must be last
} INTERRUPT_ENUMS;


#define	MAX_NUM_EVENT_CODES		(eNumInterruptTypes)


// Some Mac only ENUMS that had to be moved over to get Win/Linux code to compile,
// so these are only used by the Mac.
typedef enum
{
	kFreeRun,
	kReferenceIn,
	kVideoIn
} ReferenceSelect;


/**
	@brief	This structure completely describes the register, mask, shift, and values to use
			to get or set a signal route for a specific crosspoint.
**/
typedef struct {
   ULWord registerNum;
   ULWord mask;
   ULWord shift;
   ULWord value;
} NTV2RoutingEntry;

#define MAX_ROUTING_ENTRIES 32

/**
	@brief	This structure completely describes a complete routing configuration for a device
			(up to 32 crosspoints).
**/
typedef struct {
	ULWord				numEntries;
	NTV2RoutingEntry	routingEntry [MAX_ROUTING_ENTRIES];
} NTV2RoutingTable;

#if !defined (NTV2_DEPRECATE)
	typedef	NTV2RoutingEntry	Xena2RoutingEntry;
	typedef	NTV2RoutingTable	Xena2RoutingTable;
#endif	//	if !defined (NTV2_DEPRECATE)


//
//
// Color Space Convert Custom Coefficients
//  ...See Xena2kRegisters.pdf for more info
typedef struct {
	ULWord Coefficient1;
	ULWord Coefficient2;
	ULWord Coefficient3;
	ULWord Coefficient4;
	ULWord Coefficient5;
	ULWord Coefficient6;
	ULWord Coefficient7;
	ULWord Coefficient8;
	ULWord Coefficient9;
	ULWord Coefficient10;
} ColorSpaceConverterCustomCoefficients;

/////////////////////////////////////////////////////////////////////////////////////
// RP188 data structure used in AutoCirculate
/////////////////////////////////////////////////////////////////////////////////////

typedef struct {
	 ULWord DBB;
	 ULWord Low;		//  |  BG 4  | Secs10 |  BG 3  | Secs 1 |  BG 2  | Frms10 |  BG 1  | Frms 1 |
	 ULWord High;		//  |  BG 8  | Hrs 10 |  BG 7  | Hrs  1 |  BG 6  | Mins10 |  BG 5  | Mins 1 |
} RP188_STRUCT;	

	// convenience masks to extract fields in .Low and .High words 
#define RP188_FRAMEUNITS_MASK   0x0000000F		// Frames (units digit)  in bits  3- 0 of .Low word
#define RP188_FRAMETENS_MASK	0x00000300		// Frames (tens digit)   in bits  9- 8 of .Low word
#define RP188_SECONDUNITS_MASK	0x000F0000		// Seconds (units digit) in bits 19-16 of .Low word
#define RP188_SECONDTENS_MASK	0x07000000		// Seconds (tens digit)  in bits 26-24 of .Low word
#define RP188_LOW_TIME_MASK		(RP188_FRAMEUNITS_MASK | RP188_FRAMETENS_MASK | RP188_SECONDUNITS_MASK | RP188_SECONDTENS_MASK)

#define RP188_MINUTESUNITS_MASK 0x0000000F		// Minutes (units digit) in bits  3- 0 of .High word
#define RP188_MINUTESTENS_MASK	0x00000700		// Minutes (tens digit)  in bits 10- 8 of .High word
#define RP188_HOURUNITS_MASK	0x000F0000		// Hours (units digit)   in bits 19-16 of .High word
#define RP188_HOURTENS_MASK		0x03000000		// Hours (tens digit)    in bits 25-24 of .High word
#define RP188_HIGH_TIME_MASK	(RP188_MINUTESUNITS_MASK | RP188_MINUTESTENS_MASK | RP188_HOURUNITS_MASK | RP188_HOURTENS_MASK)

	// private bit flags added to the RP188 DBB word
#define NEW_RP188_RCVD			0x00010000		// new RP188 data was received on ANY of the channels (LTC, VITC, etc.) (capture only)
#define NEW_SELECT_RP188_RCVD   0x00020000		// new RP188 data was received on the selected channel (capture only)
#define RP188_720P_FRAMEID		0x00400000		// 720p FrameID (capture only - set by driver software)
#define RP188_CHANGED_FLAG		0x00800000		// RP188 data changed compared to last frame (capture only - set by driver software)


/////////////////////////////////////////////////////////////////////////////////////
// Color Correction data structure used in AutoCirculate
/////////////////////////////////////////////////////////////////////////////////////
// Color Corrector has 3 tables(usually R, G and B). Each table has 1024 entries
// with 2 entries per 32 bit word....therefore 512 32 bit words per table.
#define NTV2_COLORCORRECTOR_WORDSPERTABLE	(512)										// number of ULONG words in EACH color table
#define NTV2_COLORCORRECTOR_TOTALWORDS		(NTV2_COLORCORRECTOR_WORDSPERTABLE * 3)		// total number of ULONG words in all 3 tables
#define NTV2_COLORCORRECTOR_TABLESIZE		(NTV2_COLORCORRECTOR_TOTALWORDS * 4)		// total length in bytes of all 3 tables: numWords * numColors * bytes/word

typedef struct {
   NTV2ColorCorrectionMode mode;
   UWord_  saturationValue;    /// only used in 3way color correction mode.
   Pointer64 ccLookupTables;   /// R,G, and B lookup tables already formated for our hardware.
						       /// Buffer needs to be NTV2_COLORCORRECTOR_TABLESIZE.
} NTV2ColorCorrectionInfo_64;


typedef struct {
   NTV2ColorCorrectionMode mode;
   UWord_  saturationValue;    /// only used in 3way color correction mode.
   ULWord* ccLookupTables; /// R,G, and B lookup tables already formated for our hardware.
						   /// Buffer needs to be NTV2_COLORCORRECTOR_TABLESIZE.
} NTV2ColorCorrectionInfo;

typedef struct {
   NTV2ColorCorrectionMode mode;
   UWord_   saturationValue;    /// only used in 3way color correction mode.
   ULWord* POINTER_32 ccLookupTables; /// R,G, and B lookup tables already formated for our hardware.
						   /// Buffer needs to be NTV2_COLORCORRECTOR_TABLESIZE.
} NTV2ColorCorrectionInfo_32;

	// within each 32-bit LUT word, bits <31:22> = LUT[2i+1], bits <15:6> = LUT[2i] 
#define kRegColorCorrectionLUTOddShift	22
#define kRegColorCorrectionLUTEvenShift	 6

	// the base BYTE offsets (from PCI Config Base Address 0) of the three Color Correction LUTs
	// Note: if using these with GetRegisterBaseAddress() be sure to divide by 4 to get WORD offset!
#define kColorCorrectionLUTOffset_Red	(0x0800)
#define kColorCorrectionLUTOffset_Green	(0x1000)
#define kColorCorrectionLUTOffset_Blue	(0x1800)

	// Note: there is code that assumes that the three LUTs are contiguous. So if this relationship 
	//       changes (i.e. there are "gaps" between tables) then code will need to change!
#define kColorCorrectionLUTOffset_Base	(0x0800)	// BYTE offset


/////////////////////////////////////////////////////////////////////////////////////
// VidProc data structure used in AutoCirculate
/////////////////////////////////////////////////////////////////////////////////////

typedef enum {
	AUTOCIRCVIDPROCMODE_MIX,
	AUTOCIRCVIDPROCMODE_HORZWIPE,
	AUTOCIRCVIDPROCMODE_VERTWIPE,
	AUTOCIRCVIDPROCMODE_KEY	
} AutoCircVidProcMode;

typedef  struct {
   AutoCircVidProcMode mode;
   NTV2Crosspoint      foregroundVideoCrosspoint;
   NTV2Crosspoint      backgroundVideoCrosspoint;
   NTV2Crosspoint      foregroundKeyCrosspoint;
   NTV2Crosspoint      backgroundKeyCrosspoint;
   Fixed_              transitionCoefficient;       // 0-0x10000
   Fixed_              transitionSoftness;         // 0-0x10000
} AutoCircVidProcInfo;

/////////////////////////////////////////////////////////////////////////////////////
// CustomAncData data structure used in AutoCirculate
/////////////////////////////////////////////////////////////////////////////////////
typedef struct {
	 ULWord Group1;
	 ULWord Group2;
	 ULWord Group3;
	 ULWord Group4;
} CUSTOM_ANC_STRUCT;
#define OBSOLETE_ANC_STRUCT	CUSTOM_ANC_STRUCT

// #if !defined (NTV2_DEPRECATE)
// typedef OBSOLETE_ANC_STRUCT CUSTOM_ANC_STRUCT;
// #endif
	
////////////////////////////////////////////////////////////////////////////////////
// Control
////////////////////////////////////////////////////////////////////////////////////

typedef enum _AutoCircCommand_ {
	eInitAutoCirc, 
	eStartAutoCirc, 
	eStopAutoCirc, 
	ePauseAutoCirc, 
	eGetAutoCirc,
	eGetFrameStamp, 
	eFlushAutoCirculate, 
	ePrerollAutoCirculate,
	eTransferAutoCirculate, 
	eAbortAutoCirc , 
	eStartAutoCircAtTime,
	eTransferAutoCirculateEx,
	eTransferAutoCirculateEx2,
	eGetFrameStampEx2,
	eSetCaptureTask,
	eSetActiveFrame
} AUTO_CIRC_COMMAND;


/////////////////////////////////////////////////////////////////////////////////////
// GetAutoCirculate
/////////////////////////////////////////////////////////////////////////////////////

// Structure used to request autoCirculate functions
typedef enum {
	NTV2_AUTOCIRCULATE_DISABLED=0,
	NTV2_AUTOCIRCULATE_INIT,
	NTV2_AUTOCIRCULATE_STARTING,
	NTV2_AUTOCIRCULATE_PAUSED,
	NTV2_AUTOCIRCULATE_STOPPING,
	NTV2_AUTOCIRCULATE_RUNNING,
	NTV2_AUTOCIRCULATE_STARTING_AT_TIME
} NTV2AutoCirculateState;


/////////////////////////////////////////////////////////////////////////////////////
// EveryFrame Services
/////////////////////////////////////////////////////////////////////////////////////

typedef enum {
	NTV2_DISABLE_TASKS			= 0,
	NTV2_STANDARD_TASKS			= BIT(0),
	NTV2_OEM_TASKS				= BIT(1)
} NTV2EveryFrameTaskMode;


typedef enum {
	NTV2_FREEZE_BITFILE			= BIT(30),
	NTV2_DRIVER_TASKS			= BIT(28)
} NTV2DebugReg;


#ifdef AJAMac
#pragma pack(4)	// removes 64 bit alignment on non-64 bit fields
#endif

// Structure used for GetAutoCirculate
typedef struct {
	NTV2Crosspoint			channelSpec;			// Not used by Windows.
	NTV2AutoCirculateState  state;
	LWord                   startFrame;
	LWord                   endFrame;
	LWord                   activeFrame;            // Current Frame# actually being output (or input), -1, if not active
	ULWord64                rdtscStartTime;         // Performance Counter at start
	ULWord64				audioClockStartTime;    // Register 28 with Wrap Logic
	ULWord64				rdtscCurrentTime;		// Performance Counter at time of call
	ULWord64				audioClockCurrentTime;	// Register 28 with Wrap Logic
	ULWord					framesProcessed;
	ULWord					framesDropped;
	ULWord					bufferLevel;    		// how many buffers ready to record or playback in driver
	BOOL_					bWithAudio;
	BOOL_					bWithRP188;
    BOOL_					bFbfChange;
    BOOL_					bFboChange ;
	BOOL_					bWithColorCorrection;
	BOOL_					bWithVidProc;          
	BOOL_					bWithCustomAncData;          
#ifdef BORG
	BOOL_                   bWithLTC;
#endif

} AUTOCIRCULATE_STATUS_STRUCT;

typedef struct {
	AUTO_CIRC_COMMAND	eCommand;
	NTV2Crosspoint		channelSpec;

	LWord				lVal1;
	LWord				lVal2;
    LWord               lVal3;
    LWord               lVal4;
    LWord               lVal5;
    LWord               lVal6;

	BOOL_				bVal1;
	BOOL_				bVal2;
    BOOL_               bVal3;
    BOOL_               bVal4;
    BOOL_               bVal5;
    BOOL_               bVal6;
    BOOL_               bVal7;
    BOOL_               bVal8;

	Pointer64           pvVal1;
	Pointer64           pvVal2;
	Pointer64           pvVal3;
	Pointer64           pvVal4;

} AUTOCIRCULATE_DATA_64;

typedef struct {
	AUTO_CIRC_COMMAND	eCommand;
	NTV2Crosspoint		channelSpec;

	LWord				lVal1;
	LWord				lVal2;
    LWord               lVal3;
    LWord               lVal4;
    LWord               lVal5;
    LWord               lVal6;

	BOOL_				bVal1;
	BOOL_				bVal2;
    BOOL_               bVal3;
    BOOL_               bVal4;
    BOOL_               bVal5;
    BOOL_               bVal6;
    BOOL_               bVal7;
    BOOL_               bVal8;

	void*				pvVal1;
	void*				pvVal2;
	void*				pvVal3;
	void*				pvVal4;

} AUTOCIRCULATE_DATA;

typedef struct {
	AUTO_CIRC_COMMAND	eCommand;
	NTV2Crosspoint		channelSpec;

	LWord				lVal1;
	LWord				lVal2;
    LWord               lVal3;
    LWord               lVal4;
    LWord               lVal5;
    LWord               lVal6;

	BOOL_				bVal1;
	BOOL_				bVal2;
    BOOL_               bVal3;
    BOOL_               bVal4;
    BOOL_               bVal5;
    BOOL_               bVal6;
    BOOL_               bVal7;
    BOOL_               bVal8;

	void* POINTER_32	pvVal1;
	void* POINTER_32	pvVal2;
	void* POINTER_32	pvVal3;
	void* POINTER_32	pvVal4;

} AUTOCIRCULATE_DATA_32;

/////////////////////////////////////////////////////////////////////////////////////////
// GetFrameStamp
/////////////////////////////////////////////////////////////////////////////////////////

typedef struct {
	NTV2Crosspoint		channelSpec;	// Ignored in Windows

	//
	// Information from the requested frame (#FRAMESTAMP_CONTROL_STRUCT:frameNum
	//

	// Clock (System PerformanceCounter under Windows) at time of play or record.
    // audioClockTimeStamp is preferred, but not available on all boards.  (See comments below at 'currentTime' member.)
	LWord64				frameTime;

	//! The frame requested or -1 if not available
	ULWord				frame;

	//! 48kHz clock (in reg 28, extened to 64 bits) at time of play or record.
	ULWord64			audioClockTimeStamp;    // Register 28 with Wrap Logic

	//! The address that was used to transfer
	ULWord				audioExpectedAddress;

	//! For record - first position in buffer of audio (includes base offset)
	ULWord				audioInStartAddress;    // AudioInAddress at the time this Frame was stamped.

	//! For record - end position (exclusive) in buffer of audio (includes base offset)
	ULWord				audioInStopAddress;        // AudioInAddress at the Frame AFTER this Frame was stamped.

	//! For play - first position in buffer of audio
	ULWord				audioOutStopAddress;    // AudioOutAddress at the time this Frame was stamped.

	//! For play - end position (exclusive) in buffer of audio
	ULWord				audioOutStartAddress;    // AudioOutAddress at the Frame AFTER this Frame was stamped.

	//! Total audio and video bytes transfered
	ULWord				bytesRead;

	/** The actaul start sample when this frame was started in VBI
	* This may be used to check sync against audioInStartAddress (Play) or
	* audioOutStartAddress (Record).  In record it will always be equal, but
	* in playback if the clocks drift or the user supplies non aligned
	* audio sizes, then this will give the current difference from expected
	* vs actual position.  To be useful, play audio must be clocked in at
	* the correct rate.
	*/
	ULWord				startSample;

	//
	// Information from the current (active) frame
	//

	//! Current processor time ... on Windows, this is derived from KeQueryPerformanceCounter.
    //  This is the finest-grained counter available from the OS.
    //  The granularity of this counter can vary depending on the PC's HAL.
	//  audioClockCurrentTime is the recommended time-stamp to use instead of this (but is not available on all boards)!
	LWord64			    currentTime;               //! Last vertical blank frame for this channel's auto-circulate. (at the time of the IOCTL_NTV2_GET_FRAMESTAMP)
	ULWord				currentFrame;

	//! Last vertical blank timecode (RP-188)
	RP188_STRUCT		currentRP188;                   // ignored if withRP188 is false

	//! Vertical blank start of current frame
	LWord64			    currentFrameTime;

	//! 48kHz clock in reg 28 extened to 64 bits
	ULWord64			audioClockCurrentTime;      // Register 28 with Wrap Logic
	                                           // audioClockCurrentTime (from 48 kHz on-board clock) is consistent and accurate!
                                               // but is not available on the XenaSD-22.

	//! As set by play
	ULWord				currentAudioExpectedAddress;

	//! As found by isr
	ULWord				currentAudioStartAddress;

	//! At Call Field0 or Field1 _currently_ being OUTPUT (at the time of the IOCTL_NTV2_GET_FRAMESTAMP)
	ULWord				currentFieldCount;         //! At Call Line# _currently_ being OUTPUT (at the time of the IOCTL_NTV2_GET_FRAMESTAMP)
	ULWord				currentLineCount;          //! Contains validCount (Play - reps remaining, Record - drops on frame)
	ULWord				currentReps;               //! User cookie at last vblank
	ULWord				currenthUser;            
} FRAME_STAMP_STRUCT;

typedef struct {
	NTV2Crosspoint          channelSpec;    // specify Input or Output channel for desired Frame
	NTV2AutoCirculateState  state;          // current state
	LWord                   transferFrame;  // framebuffer number the frame transferred to, -1 on error
	ULWord                  bufferLevel;    // how many buffers ready to record or playback
	ULWord                  framesProcessed;
	ULWord                  framesDropped;
	FRAME_STAMP_STRUCT      frameStamp;     // record. framestramp for that frame,playback
	ULWord					audioBufferSize;
	ULWord					audioStartSample;
} AUTOCIRCULATE_TRANSFER_STATUS_STRUCT, *PAUTOCIRCULATE_TRANSFER_STATUS_STRUCT;

typedef struct {
	NTV2Crosspoint  channelSpec;             // specify Input or Output channel for desired Frame
	Pointer64       videoBuffer;			 // Keep 64 bit aligned for performance reasons
	ULWord          videoBufferSize;
    ULWord          videoDmaOffset;          // must be initialized, 64 bit aligned   
	Pointer64       audioBuffer;			 // Keep 64 bit aligned for performance reasons  
	ULWord          audioBufferSize;
	ULWord          audioStartSample;        // to ensure correct alignment in audio buffer .. NOT USED in Windows... audio now always starts at sample zero.
	ULWord          audioNumChannels;        // 1-6 NOTE!!! only 6 supported at this time
	ULWord          frameRepeatCount;        // NOTE!!! not supported yet.

	RP188_STRUCT    rp188;                   // ignored if withRP188 is false

	LWord           desiredFrame;            // -1 if you want driver to find next available
	ULWord			hUser;					 // A user cookie returned by frame stamp
	ULWord			transferFlags;           // disableAudioDMA is no longer used
	BOOL_			bDisableExtraAudioInfo;	 // No 24 byte 0 at front or size info in buffer
    NTV2FrameBufferFormat frameBufferFormat; // should be initialized, but can be overridden
	NTV2VideoFrameBufferOrientation frameBufferOrientation;
	NTV2ColorCorrectionInfo_64 colorCorrectionInfo;
	AutoCircVidProcInfo vidProcInfo;
	//OBSOLETE_ANC_STRUCT    customAncInfo;     
	CUSTOM_ANC_STRUCT customAncInfo;

	
		// The following params are for cases when you need to DMA multiple discontiguous "segments" of a video frame. One example
		// would be when a frame in Host memory is not "packed", i.e. there are extra "padding" bytes at the end of each row.
		// In this case you would set videoBufferSize to the number of active bytes per row, videoNumSegments to the number of rows,
		// and videoSegmentHostPitch to the number of bytes from the beginning of one row to the next. In this example,
		// videoSegmentCardPitch would be equal to videoBufferSize (i.e. the frame is packed in board memory).
		//   Another example would be DMAing a sub-section of a frame. In this case set videoBufferSize to the number of bytes in
		// one row of the subsection, videoNumSegments to the number of rows in the subsection, videoSegmentHostPitch to the rowBytes
		// of the entire frame in Host Memory, and videoSegmentCardPitch to the rowBytes of the entire frame in board memory.
		// Note: setting videoNumSegments to 0 or 1 defaults to original behavior (i.e. DMA one complete packed frame)
	ULWord			videoNumSegments;		// number of segments of size videoBufferSize to DMA (i.e. numLines)
	ULWord			videoSegmentHostPitch;	// offset (in bytes) between the beginning of one host segment and the beginning of the next host segment (i.e. host rowBytes)
	ULWord			videoSegmentCardPitch;	// offset (in bytes) between the beginning of one board segment and the beginning of the next board segment (i.e. board memory rowBytes)
	
		// This turns on the "quarter-size expand" (2x H + 2x V) hardware
	NTV2QuarterSizeExpandMode	videoQuarterSizeExpand;

// 	ULWord*			ancBuffer;
// 	ULWord			ancBufferSize;
	
} AUTOCIRCULATE_TRANSFER_STRUCT_64, *PAUTOCIRCULATE_TRANSFER_STRUCT_64;

typedef struct {
	NTV2Crosspoint  channelSpec;             // specify Input or Output channel for desired Frame
	ULWord  *       videoBuffer;			 // Keep 64 bit aligned for performance reasons
	ULWord          videoBufferSize;
    ULWord          videoDmaOffset;          // must be initialized, 64 bit aligned   
	ULWord  *       audioBuffer;			 // Keep 64 bit aligned for performance reasons  
	ULWord          audioBufferSize;
	ULWord          audioStartSample;        // to ensure correct alignment in audio buffer .. NOT USED in Windows... audio now always starts at sample zero.
	ULWord          audioNumChannels;        // 1-6 NOTE!!! only 6 supported at this time
	ULWord          frameRepeatCount;        // NOTE!!! not supported yet.

	RP188_STRUCT    rp188;                   // ignored if withRP188 is false

	LWord           desiredFrame;            // -1 if you want driver to find next available
	ULWord			hUser;					 // A user cookie returned by frame stamp
	ULWord			transferFlags;           // disableAudioDMA is no longer used
	BOOL_			bDisableExtraAudioInfo;	 // No 24 byte 0 at front or size info in buffer
    NTV2FrameBufferFormat frameBufferFormat; // should be initialized, but can be overridden
	NTV2VideoFrameBufferOrientation frameBufferOrientation;
	NTV2ColorCorrectionInfo colorCorrectionInfo;
	AutoCircVidProcInfo vidProcInfo;
	//OBSOLETE_ANC_STRUCT    customAncInfo; 	
	CUSTOM_ANC_STRUCT customAncInfo;
		// The following params are for cases when you need to DMA multiple discontiguous "segments" of a video frame. One example
		// would be when a frame in Host memory is not "packed", i.e. there are extra "padding" bytes at the end of each row.
		// In this case you would set videoBufferSize to the number of active bytes per row, videoNumSegments to the number of rows,
		// and videoSegmentHostPitch to the number of bytes from the beginning of one row to the next. In this example,
		// videoSegmentCardPitch would be equal to videoBufferSize (i.e. the frame is packed in board memory).
		//   Another example would be DMAing a sub-section of a frame. In this case set videoBufferSize to the number of bytes in
		// one row of the subsection, videoNumSegments to the number of rows in the subsection, videoSegmentHostPitch to the rowBytes
		// of the entire frame in Host Memory, and videoSegmentCardPitch to the rowBytes of the entire frame in board memory.
		// Note: setting videoNumSegments to 0 or 1 defaults to original behavior (i.e. DMA one complete packed frame)
	ULWord			videoNumSegments;		// number of segments of size videoBufferSize to DMA (i.e. numLines)
	ULWord			videoSegmentHostPitch;	// offset (in bytes) between the beginning of one host segment and the beginning of the next host segment (i.e. host rowBytes)
	ULWord			videoSegmentCardPitch;	// offset (in bytes) between the beginning of one board segment and the beginning of the next board segment (i.e. board memory rowBytes)
	
		// This turns on the "quarter-size expand" (2x H + 2x V) hardware
	NTV2QuarterSizeExpandMode	videoQuarterSizeExpand;

// 	ULWord  *       ancBuffer;			 // Keep 64 bit aligned for performance reasons  
// 	ULWord          ancBufferSize;
	
} AUTOCIRCULATE_TRANSFER_STRUCT, *PAUTOCIRCULATE_TRANSFER_STRUCT;

typedef struct {
	NTV2Crosspoint  channelSpec;             // specify Input or Output channel for desired Frame
	ULWord  * POINTER_32      videoBuffer;   // Keep 64 bit aligned for performance reasons
	ULWord          videoBufferSize;
    ULWord          videoDmaOffset;          // must be initialized, 64 bit aligned   
	ULWord  * POINTER_32      audioBuffer;   // Keep 64 bit aligned for performance reasons
	ULWord          audioBufferSize;
	ULWord          audioStartSample;        // to ensure correct alignment in audio buffer .. NOT USED in Windows... audio now always starts at sample zero.
	ULWord          audioNumChannels;        // 1-6 NOTE!!! only 6 supported at this time
	ULWord          frameRepeatCount;        // NOTE!!! not supported yet.

	RP188_STRUCT    rp188;                   // ignored if withRP188 is false

	LWord           desiredFrame;            // -1 if you want driver to find next available
	ULWord			hUser;					 // A user cookie returned by frame stamp
	ULWord			transferFlags;           // disableAudioDMA is no longer used
	BOOL_			bDisableExtraAudioInfo;	 // No 24 byte 0 at front or size info in buffer .. NOT USED in Windows, extra audio no longer supported
    NTV2FrameBufferFormat frameBufferFormat; // should be initialized, but can be overridden
	NTV2VideoFrameBufferOrientation frameBufferOrientation;
	NTV2ColorCorrectionInfo_32 colorCorrectionInfo;
	AutoCircVidProcInfo vidProcInfo;
	//OBSOLETE_ANC_STRUCT    customAncInfo;                   
	CUSTOM_ANC_STRUCT customAncInfo;
		// The following params are for cases when you need to DMA multiple discontiguous "segments" of a video frame. One example
		// would be when a frame in Host memory is not "packed", i.e. there are extra "padding" bytes at the end of each row.
		// In this case you would set videoBufferSize to the number of active bytes per row, videoNumSegments to the number of rows,
		// and videoSegmentHostPitch to the number of bytes from the beginning of one row to the next. In this example,
		// videoSegmentCardPitch would be equal to videoBufferSize (i.e. the frame is packed in board memory).
		//   Another example would be DMAing a sub-section of a frame. In this case set videoBufferSize to the number of bytes in
		// one row of the subsection, videoNumSegments to the number of rows in the subsection, videoSegmentHostPitch to the rowBytes
		// of the entire frame in Host Memory, and videoSegmentCardPitch to the rowBytes of the entire frame in board memory.
		// Note: setting videoNumSegments to 0 or 1 defaults to original behavior (i.e. DMA one complete packed frame)
	ULWord			videoNumSegments;		// number of segments of size videoBufferSize to DMA (i.e. numLines)
	ULWord			videoSegmentHostPitch;	// offset (in bytes) between the beginning of one host segment and the beginning of the next host segment (i.e. host rowBytes)
	ULWord			videoSegmentCardPitch;	// offset (in bytes) between the beginning of one board segment and the beginning of the next board segment (i.e. board memory rowBytes)
	
		// This turns on the "quarter-size expand" (2x H + 2x V) hardware
	NTV2QuarterSizeExpandMode	videoQuarterSizeExpand;

// 	ULWord  *       ancBuffer;			 // Keep 64 bit aligned for performance reasons  
// 	ULWord          ancBufferSize;

} AUTOCIRCULATE_TRANSFER_STRUCT_32, *PAUTOCIRCULATE_TRANSFER_STRUCT_32;


// Structure for autocirculate peer to peer transfers.  For p2p target specify kTransferFlagP2PPrepare
// for completion using kTransferFlagP2PComplete or kTransferFlagP2PTarget for completion with message transfer.  
// Autocirculate will write an AUTOCIRCULATE_P2P_STRUCT to the video buffer specified to the target.  Pass this 
// buffer as the video buffer to the autocirculate p2p source (kTransferFlagP2PTransfer) to do the p2p transfer.  
// For completion with kTransferFlagP2PComplete specify the transferFrame from the kTransferFlagP2PPrepare.
typedef struct {
	ULWord		p2pSize;					// size of p2p structure
	ULWord		p2pflags;					// p2p transfer flags
	ULWord64	videoBusAddress;			// frame buffer bus address
	ULWord64	messageBusAddress;			// message register bus address (0 if not required)
	ULWord		videoBusSize;				// size of the video aperture (bytes)
	ULWord		messageData;				// message data (write to message bus address to complete video transfer)
} AUTOCIRCULATE_P2P_STRUCT, *PAUTOCIRCULATE_P2P_STRUCT, CHANNEL_P2P_STRUCT, *PCHANNEL_P2P_STRUCT;


#define AUTOCIRCULATE_TASK_VERSION		0x00000001
#define AUTOCIRCULATE_TASK_MAX_TASKS	128

/**
	@brief	These are the available AutoCirculate task types.
**/
typedef enum
{
	eAutoCircTaskNone,
	eAutoCircTaskRegisterWrite,		// AutoCircRegisterTask
	eAutoCircTaskRegisterRead,		// AutoCircRegisterTask
	eAutoCircTaskTimeCodeWrite,		// AutoCircTimeCodeTask
	eAutoCircTaskTimeCodeRead,		// AutoCircTimeCodeTask
	MAX_NUM_AutoCircTaskTypes
} AutoCircTaskType;


#define	NTV2_IS_VALID_TASK_TYPE(_x_)			((_x_) > eAutoCircTaskNone && (_x_) < MAX_NUM_AutoCircTaskTypes)

#define	NTV2_IS_REGISTER_READ_TASK(_x_)			((_x_) == eAutoCircTaskRegisterRead)
#define	NTV2_IS_REGISTER_WRITE_TASK(_x_)		((_x_) == eAutoCircTaskRegisterWrite)
#define	NTV2_IS_REGISTER_TASK(_x_)				(NTV2_IS_REGISTER_WRITE_TASK (_x_) || NTV2_IS_REGISTER_READ_TASK (_x_))

#define	NTV2_IS_TIMECODE_READ_TASK(_x_)			((_x_) == eAutoCircTaskTimeCodeRead)
#define	NTV2_IS_TIMECODE_WRITE_TASK(_x_)		((_x_) == eAutoCircTaskTimeCodeWrite)
#define	NTV2_IS_TIMECODE_TASK(_x_)				(NTV2_IS_TIMECODE_WRITE_TASK (_x_) || NTV2_IS_TIMECODE_READ_TASK (_x_))


typedef struct  
{
	ULWord regNum;
	ULWord mask;
	ULWord shift;
	ULWord value;
} AutoCircRegisterTask;

typedef struct  
{
	RP188_STRUCT TCInOut1;
	RP188_STRUCT TCInOut2;
	RP188_STRUCT LTCEmbedded;
	RP188_STRUCT LTCAnalog;
	RP188_STRUCT LTCEmbedded2;
	RP188_STRUCT LTCAnalog2;
	RP188_STRUCT TCInOut3;
	RP188_STRUCT TCInOut4;
	RP188_STRUCT TCInOut5;
	RP188_STRUCT TCInOut6;
	RP188_STRUCT TCInOut7;
	RP188_STRUCT TCInOut8;
	RP188_STRUCT LTCEmbedded3;
	RP188_STRUCT LTCEmbedded4;
	RP188_STRUCT LTCEmbedded5;
	RP188_STRUCT LTCEmbedded6;
	RP188_STRUCT LTCEmbedded7;
	RP188_STRUCT LTCEmbedded8;
} AutoCircTimeCodeTask;

typedef struct
{
	AutoCircTaskType taskType;
	union
	{
		AutoCircRegisterTask	registerTask;
		AutoCircTimeCodeTask	timeCodeTask;
	} u;
} AutoCircGenericTask;

typedef struct
{
	ULWord taskVersion;
	ULWord taskSize;
	ULWord numTasks;
	ULWord maxTasks;
	Pointer64 taskArray;
	ULWord reserved0;
	ULWord reserved1;
	ULWord reserved2;
	ULWord reserved3;
} AUTOCIRCULATE_TASK_STRUCT_64, *PAUTOCIRCULATE_TASK_STRUCT_64;

typedef struct
{
	ULWord taskVersion;
	ULWord taskSize;
	ULWord numTasks;
	ULWord maxTasks;
	AutoCircGenericTask* taskArray;
	ULWord reserved0;
	ULWord reserved1;
	ULWord reserved2;
	ULWord reserved3;
} AUTOCIRCULATE_TASK_STRUCT, *PAUTOCIRCULATE_TASK_STRUCT;

typedef struct
{
	ULWord taskVersion;
	ULWord taskSize;
	ULWord numTasks;
	ULWord maxTasks;
	AutoCircGenericTask* POINTER_32 taskArray;
	ULWord reserved0;
	ULWord reserved1;
	ULWord reserved2;
	ULWord reserved3;
} AUTOCIRCULATE_TASK_STRUCT_32, *PAUTOCIRCULATE_TASK_STRUCT_32;


// Information about the currently programmed Xilinx .bit file
#define NTV2_BITFILE_DATETIME_STRINGLENGTH	(16)
#define NTV2_BITFILE_DESIGNNAME_STRINGLENGTH	(20)
#define NTV2_BITFILE_PARTNAME_STRINGLENGTH	(16)
// Increment this when you change the bitfile information structure
// And be sure to update the driver so it can handle the new version.
#define NTV2_BITFILE_STRUCT_VERSION			(4)

// There is room for up to 4kbytes after the audio in the last frame,
// but a 4KB data struct overflows the stack in the ioctl routine in
// the driver under Linux.  
//#define NTV2_BITFILE_RESERVED_ULWORDS		(244)
//#define NTV2_BITFILE_RESERVED_ULWORDS		(243)    // added bitFileType
//#define NTV2_BITFILE_RESERVED_ULWORDS		(239)	 // added designName
//#define NTV2_BITFILE_RESERVED_ULWORDS		(235)	 // added partName
#define NTV2_BITFILE_RESERVED_ULWORDS		(234)	 // added whichFPGA

typedef struct {
	ULWord			checksum;										// Platform-dependent.  Deprecated on Linux.
	ULWord			structVersion;									// Version of this structure	
	ULWord			structSize;										// Total size of this structure

    ULWord          numBytes;										// Xilinx bitfile bytecount
	char			dateStr[NTV2_BITFILE_DATETIME_STRINGLENGTH];	// Date Xilinx bitfile compiled
	char			timeStr[NTV2_BITFILE_DATETIME_STRINGLENGTH];	// Time Xilinx bitfile compiled
	char			designNameStr[NTV2_BITFILE_DESIGNNAME_STRINGLENGTH];
	
	ULWord			bitFileType;										// NTV2BitfileType
	
	char			partNameStr[NTV2_BITFILE_PARTNAME_STRINGLENGTH];	// Part name (v4)
	NTV2XilinxFPGA	whichFPGA;

	ULWord			reserved[NTV2_BITFILE_RESERVED_ULWORDS];	

} BITFILE_INFO_STRUCT,*PBITFILE_INFO_STRUCT;


typedef struct {
	NTV2DMAEngine	dmaEngine;
	ULWord			dmaFlags;						// flags passed into DMA currently bit 1 is set for to indicate weird 4096 10bit YUV 4K frame
	
	Pointer64		dmaHostBuffer;					// vitrual address of host buffer
	ULWord			dmaSize;						// number of bytes to DMA
	ULWord			dmaCardFrameNumber;				// card frame number 
	ULWord			dmaCardFrameOffset;				// offset (in bytes) into card frame to begin DMA
	ULWord			dmaNumberOfSegments;			// number of segments of size videoBufferSize to DMA
	ULWord			dmaSegmentSize;					// size of each segment (if videoNumSegments > 1)
	ULWord			dmaSegmentHostPitch;			// offset (in bytes) between the beginning of one host-memory segment and the beginning of the next host-memory segment
	ULWord			dmaSegmentCardPitch;			// offset (in bytes) between the beginning of one Kona-memory segment and the beginning of the next Kona-memory segment

	BOOL_			dmaToCard;						// direction of DMA transfer
	
} DMA_TRANSFER_STRUCT_64 ,*PDMA_TRANSFER_STRUCT_64;


// NOTE: Max bitfilestruct size was NTV2_AUDIO_READBUFFEROFFSET - NTV2_AUDIO_WRAPADDRESS
// but is now practically unlimited.

// The following structure is used to retrieve the timestamp values of the last video 
// interrupts. Use GetInterruptTimeStamps(&
typedef struct {
	LWord64				lastOutputVerticalTimeStamp;
	LWord64				lastInput1VerticalTimeStamp;
	LWord64				lastInput2VerticalTimeStamp;
} INTERRUPT_TIMESTAMP_STRUCT,*PINTERRUPT_TIMESTAMP_STRUCT;

// System status calls and structs associated with specific opcodes 

typedef enum
{
	SSC_GetFirmwareProgress,				// return firmware progress informaiton
	SSC_End									// end of list
} SystemStatusCode;

typedef enum
{
	kProgramStateEraseMainFlashBlock,
	kProgramStateEraseSecondFlashBlock,
	kProgramStateEraseFailSafeFlashBlock,
	kProgramStateProgramFlash,
	kProgramStateVerifyFlash,
	kProgramStateFinished
} ProgramState;

typedef enum {
	kProgramCommandReadID=0x9F,
	kProgramCommandWriteEnable=0x06,
	kProgramCommandWriteDisable=0x04,
	kProgramCommandReadStatus=0x05,
	kProgramCommandWriteStatus=0x01,
	kProgramCommandReadFast=0x0B,
	kProgramCommandPageProgram=0x02,
	kProgramCommandSectorErase=0xD8,
	kProgramCommandBankWrite=0x17
} ProgramCommand;

typedef struct {
	ULWord			programTotalSize;		
	ULWord			programProgress;
	ProgramState	programState;
} SSC_GET_FIRMWARE_PROGRESS_STRUCT;

// System control calls and structs associated with specific opcodes 

typedef enum
{
	SCC_Test,								// just a test for now
	SCC_End									// end of list	
} SystemControlCode;

typedef struct {
	ULWord			param1;					// test parameter 1
	ULWord			param2;					// test parameter 2
} SCC_TEST_STRUCT;


// Build information 
#define NTV2_BUILD_STRINGLENGTH		(1024)
#define NTV2_BUILD_STRUCT_VERSION	(0)

#define NTV2_BUILD_RESERVED_BYTES	(1016)

typedef struct {
	ULWord			structVersion;						// Version of this structure	
	ULWord			structSize;							// Total size of this structure

	char			buildStr[NTV2_BUILD_STRINGLENGTH];	// User-defined build string
	unsigned char	reserved[NTV2_BUILD_RESERVED_BYTES];	

} BUILD_INFO_STRUCT,*PBUILD_INFO_STRUCT;


#ifdef AJAMac
#pragma options align=reset
#endif

	// used to filter the vout menu display 
typedef enum
{
	kUndefinedFilterFormats		= 0,		// Undefined
	kDropFrameFormats			= BIT(0),	// 23.98 / 29.97 / 59.94
	kNonDropFrameFormats		= BIT(1),	// 24 / 30 / 60
	kEuropeanFormats			= BIT(2),	// 25 / 50
	k1080ProgressiveFormats		= BIT(3),	// 1080p 23.98/24/29.97/30 (exclude 1080psf)
	kREDFormats					= BIT(4),	// RED's odd geometries
	k2KFormats					= BIT(5),	// 2K formats
	k4KFormats					= BIT(6)	// 4K formats
	
} ActiveVideoOutSelect;

// STUFF moved from ntv2macinterface.h that is now common
typedef enum DesktopFrameBuffStatus
{
	kDesktopFBIniting	= 0,		// waiting for Finder? Window Mgr? to discover us
	kDesktopFBOff		= 1,		// Running - not in use
	kDesktopFBOn		= 2			// Running - in-use as Mac Desktop
	
} DesktopFrameBuffStatus;


typedef enum SharedPrefsPermissions
{
	kSharedPrefsRead			= 0,
	kSharedPrefsReadWrite		= 1
	
} SharedPrefsPermissions;


typedef enum TimelapseUnits
{
	kTimelapseFrames			= 0,		// frames
	kTimelapseSeconds			= 1,		// seconds
	kTimelapseMinutes			= 2,		// minutes
	kTimelapseHours				= 3			// hours

} TimelapseUnits;

typedef enum
{
	kDefaultModeDesktop,					// deprecated
	kDefaultModeVideoIn,
	kDefaultModeBlack,						// deprecated
	kDefaultModeHold,
	kDefaultModeTestPattern,
	kDefaultModeUnknown
} DefaultVideoOutMode;

typedef enum
{
	kHDMIOutCSCAutoDetect,
	kHDMIOutCSCRGB8bit,
	kHDMIOutCSCRGB10bit,
	kHDMIOutCSCYCbCr8bit,
	kHDMIOutCSCYCbCr10bit
} HDMIOutColorSpaceMode;

typedef enum
{
	kHDMIOutProtocolAutoDetect,
	kHDMIOutProtocolHDMI,
	kHDMIOutProtocolDVI
} HDMIOutProtocolMode;

typedef enum
{
	kHDMIOutStereoOff,
	kHDMIOutStereoAuto,
	kHDMIOutStereoSideBySide,
	kHDMIOutStereoTopBottom,
	kHDMIOutStereoFramePacked
	
} HDMIOutStereoSelect;

typedef enum
{
	kRP188SourceEmbeddedLTC		= 0x0,		// NOTE these values are same as RP188 DBB channel select
	kRP188SourceEmbeddedVITC1	= 0x1,
	kRP188SourceEmbeddedVITC2	= 0x2,
	kRP188SourceLTCPort			= 0xFE		// used in ioHD
} RP188SourceSelect;

// note: this order is determined by NTV2TestPatternSegments in testpatterndata.h
typedef enum
{
	kTestPatternColorBar100,		// 100% Bars
	kTestPatternColorBar75,			// 75% Bars
	kTestPatternRamp,				// Ramp
	kTestPatternMultiburst,			// Mulitburst
	kTestPatternLinesweep,			// Line Sweep
	kTestPatternPathological,		// Pathogical
	kTestPatternFlatField,			// Flat Field (50%)
	kTestPatternMultiPattern,		// a swath of everything
	kTestPatternBlack,				// Black
	kTestPatternBorder,				// Border
	kTestPatternCustom				// Custom ("Load File...")
	
} TestPatternSelect;

enum TestPatternFormat
{
	kPatternFormatYUV10b,
	kPatternFormatRGB10b,
	kPatternFormatYUV8b,
	kPatternFormatRGB8b
};

// Masks
enum
{
	// kRegUserState1
	kMaskInputFormatSelect		= BIT(0)  + BIT(1)  + BIT(2)  + BIT(3)  + BIT(4)  + BIT(5)  + BIT(6)  + BIT(7), 
	kMaskPrimaryFormatSelect	= BIT(8)  + BIT(9)  + BIT(10) + BIT(11) + BIT(12) + BIT(13) + BIT(14) + BIT(15), 
	kMaskSecondaryFormatSelect	= BIT(16) + BIT(17) + BIT(18) + BIT(19) + BIT(20) + BIT(21) + BIT(22) + BIT(23),
	kMaskAnalogInBlackLevel		= BIT(24),
	kMaskAnalogInputType		= BIT(28) + BIT(29) + BIT(30) + BIT(31),
	
	//kRegIoHDGlobalStatus
	kMaskStandBusyStatus		= BIT(2),
	
	// kRegIoHDGlobalControl
	kMaskStandAloneMode			= BIT(0) + BIT(1) + BIT(2),
	kMaskDisplayMode			= BIT(4) + BIT(5) + BIT(6)  + BIT(7),
	kMaskDisplayModeTCType		= BIT(8) + BIT(9) + BIT(10) + BIT(11),	// TimecodeFormat - when set to zero, Timecode type follows primary format
	
	// kStartupStatusFlags
	kMaskStartComplete			= BIT(0),
	kMaskDesktopDisplayReady    = BIT(1),
	kMaskDaemonInitialized		= BIT(2)
};

// isoch streams (channels)
enum DriverStartPhase
{
	kStartPhase1				= 1,				// These start out at 1 because they become a bit setting
	kStartPhase2				= 2
};

typedef enum
{
	kPrimarySecondaryDisplayMode,
	kPrimaryTimecodeDisplayMode
} IoHDDisplayMode;

#define KONA_DEBUGFILTER_STRINGLENGTH 128
typedef struct
{
	char includeString[KONA_DEBUGFILTER_STRINGLENGTH];
	char excludeString[KONA_DEBUGFILTER_STRINGLENGTH];
} KonaDebugFilterStringInfo;

typedef struct
{
	NTV2RelayState	manualControl12;
	NTV2RelayState	manualControl34;
	NTV2RelayState	relayPosition12;
	NTV2RelayState	relayPosition34;
	NTV2RelayState	watchdogStatus;
	bool			watchdogEnable12;
	bool			watchdogEnable34;
	ULWord			watchdogTimeout;
} NTV2SDIWatchdogState;


#if !defined (NTV2_DEPRECATE)
	#define		kRegFlatMattleValue		kRegFlatMatteValue
	#define		kRegFlatMattle2Value	kRegFlatMatte2Value
#endif	//	NTV2_DEPRECATE

#endif	//	NTV2PUBLICINTERFACE_H
