/*
 *  ntv2debug.cpp
 *
 *  Created by Richard Jackson on Wed Apr 28 2004.
 *
 *	Copyright (C) 2004, 2005, 2006, 2007, 2008, 2009 AJA Video Systems, Inc.  
 *	Proprietary and Confidential information.
 *	All Rights Reserved.
 *
 */

#include "ntv2publicinterface.h"
#include "ntv2debug.h"


const char *NTV2DeviceTypeString (NTV2DeviceType type)
{
	const char *result = "";

	switch (type)
	{
		case DEVICETYPE_NTV2:	result = "DEVICETYPE_NTV2";	break;
		default:				result = "Unknown Device Type";	break;
	}

	return (result);
}


const char * NTV2DeviceIDString (const NTV2DeviceID id)
{
	switch (id)
	{
	#if !defined (NTV2_DEPRECATE)
		case BOARD_ID_XENA_SD:
		case BOARD_ID_XENA_SD22:
		case BOARD_ID_XENA_HD:
		case BOARD_ID_XENA_HD22:
		case BOARD_ID_HDNTV2:
		case BOARD_ID_KSD11:
		//case BOARD_ID_XENA_SD_MM:
		case BOARD_ID_KSD22:
		//case BOARD_ID_XENA_SD22_MM:
		case BOARD_ID_KHD11:
		//case BOARD_ID_XENA_HD_MM:
		case BOARD_ID_XENA_HD22_MM:
		case BOARD_ID_HDNTV2_MM:
		case BOARD_ID_KONA_SD:
		case BOARD_ID_KONA_HD:
		case BOARD_ID_KONA_HD2:
		case BOARD_ID_KONAR:
		case BOARD_ID_KONAR_MM:
		case BOARD_ID_KONA2:
		case BOARD_ID_HDNTV:
		case BOARD_ID_KONALS:
		//case BOARD_ID_XENALS:
		case BOARD_ID_KONAHDS:
		//case BOARD_ID_KONALH:
		//case BOARD_ID_XENALH:
		case BOARD_ID_XENADXT:
		//case BOARD_ID_XENAHS:
		case BOARD_ID_KONAX:
		case BOARD_ID_XENAX:
		case BOARD_ID_XENAHS2:
		case BOARD_ID_FS1:
		case BOARD_ID_FS2:
		case BOARD_ID_MOAB:
		case BOARD_ID_XENAX2:
		case BOARD_ID_BORG:
		case BOARD_ID_BONES:
		case BOARD_ID_BARCLAY:
		case BOARD_ID_KIPRO_QUAD:
		case BOARD_ID_KIPRO_SPARE1:
		case BOARD_ID_KIPRO_SPARE2:
		case BOARD_ID_FORGE:			break;

		//case BOARD_ID_KONA3:
		case BOARD_ID_XENA2:			return "BOARD_ID_KONA3";		break;
		case BOARD_ID_LHI_DVI:			return "BOARD_ID_LHI_DVI";		break;
		case BOARD_ID_LHI_T:			return "BOARD_ID_LHI_T";		break;
	#endif	//	!defined (NTV2_DEPRECATE)
		case DEVICE_ID_LHI:				return "DEVICE_ID_LHI";			break;
		case DEVICE_ID_IOEXPRESS:		return "DEVICE_ID_IOEXPRESS";	break;
		case DEVICE_ID_CORVID1:			return "DEVICE_ID_CORVID1";		break;
		case DEVICE_ID_CORVID22:		return "DEVICE_ID_CORVID22";	break;
		case DEVICE_ID_CORVID3G:		return "DEVICE_ID_CORVID3G";	break;
		case DEVICE_ID_KONA3G:			return "DEVICE_ID_KONA3G";		break;
		case DEVICE_ID_KONA3GQUAD:		return "DEVICE_ID_KONA3GQUAD";	break;
		case DEVICE_ID_LHE_PLUS:		return "DEVICE_ID_LHE_PLUS";	break;
		case DEVICE_ID_IOXT:			return "DEVICE_ID_IOXT";		break;
		case DEVICE_ID_CORVID24:		return "DEVICE_ID_CORVID24";	break;
		case DEVICE_ID_TTAP:			return "DEVICE_ID_TTAP";		break;
		case DEVICE_ID_IO4K:			return "DEVICE_ID_IO4K";		break;
		case DEVICE_ID_IO4KUFC:			return "DEVICE_ID_IO4KUFC";		break;
		case DEVICE_ID_KONA4:			return "DEVICE_ID_KONA4";		break;
		case DEVICE_ID_KONA4UFC:		return "DEVICE_ID_KONA4UFC";	break;
		case DEVICE_ID_CORVID88:		return "DEVICE_ID_CORVID88";	break;
		case DEVICE_ID_CORVID44:		return "DEVICE_ID_CORVID44";	break;
		case DEVICE_ID_NOTFOUND:		return "Unknown Board ID";		break;
	}

	return "";
}

 
const char * NTV2DeviceString (const NTV2DeviceID id)
{
	switch (id)
	{
	#if !defined (NTV2_DEPRECATE)
		case BOARD_ID_XENA_SD:
		case BOARD_ID_XENA_SD22:
		case BOARD_ID_XENA_HD:
		case BOARD_ID_XENA_HD22:
		case BOARD_ID_HDNTV2:
		case BOARD_ID_KSD11:
		//case BOARD_ID_XENA_SD_MM:
		case BOARD_ID_KSD22:
		//case BOARD_ID_XENA_SD22_MM:
		case BOARD_ID_KHD11:
		//case BOARD_ID_XENA_HD_MM:
		case BOARD_ID_XENA_HD22_MM:
		case BOARD_ID_HDNTV2_MM:
		case BOARD_ID_KONA_SD:
		case BOARD_ID_KONA_HD:
		case BOARD_ID_KONA_HD2:
		case BOARD_ID_KONAR:
		case BOARD_ID_KONAR_MM:
		case BOARD_ID_KONA2:
		case BOARD_ID_HDNTV:
		case BOARD_ID_KONALS:
		//case BOARD_ID_XENALS:
		case BOARD_ID_KONAHDS:
		//case BOARD_ID_KONALH:
		//case BOARD_ID_XENALH:
		case BOARD_ID_XENADXT:
		//case BOARD_ID_XENAHS:
		case BOARD_ID_KONAX:
		case BOARD_ID_XENAX:
		case BOARD_ID_XENAHS2:
		case BOARD_ID_FS1:
		case BOARD_ID_FS2:
		case BOARD_ID_MOAB:
		case BOARD_ID_XENAX2:
		case BOARD_ID_BORG:
		case BOARD_ID_BONES:
		case BOARD_ID_BARCLAY:
		case BOARD_ID_KIPRO_QUAD:
		case BOARD_ID_KIPRO_SPARE1:
		case BOARD_ID_KIPRO_SPARE2:
		case BOARD_ID_FORGE:			break;

		//case BOARD_ID_KONA3:
		case BOARD_ID_XENA2:			return "Kona3";			break;
		case BOARD_ID_LHI_DVI:			return "KonaLHiDVI";	break;
		case BOARD_ID_LHI_T:			return "KonaLHiT";		break;
	#endif	//	!defined (NTV2_DEPRECATE)
		case DEVICE_ID_LHI:				return "KonaLHi";		break;
		case DEVICE_ID_IOEXPRESS:		return "IoExpress";		break;
		case DEVICE_ID_CORVID1:			return "Corvid1";		break;
		case DEVICE_ID_CORVID22:		return "Corvid22";		break;
		case DEVICE_ID_CORVID3G:		return "Corvid3G";		break;
		case DEVICE_ID_KONA3G:			return "Kona3G";		break;
		case DEVICE_ID_KONA3GQUAD:		return "Kona3GQuad";	break;
		case DEVICE_ID_LHE_PLUS:		return "KonaLHePlus";	break;
		case DEVICE_ID_IOXT:			return "IoXT";			break;
		case DEVICE_ID_CORVID24:		return "Corvid24";		break;
		case DEVICE_ID_TTAP:			return "TTap";			break;
		case DEVICE_ID_IO4K:			return "Io4K";			break;
		case DEVICE_ID_IO4KUFC:			return "Io4KUfc";		break;
		case DEVICE_ID_KONA4:			return "Kona4";			break;
		case DEVICE_ID_KONA4UFC:		return "Kona4Ufc";		break;
		case DEVICE_ID_CORVID88:		return "Corvid88";		break;
		case DEVICE_ID_CORVID44:		return "Corvid44";		break;
		case DEVICE_ID_NOTFOUND:		return "Unknown";		break;
	}

	return "";
}

 
const char *NTV2StandardString (NTV2Standard std)
{
	const char *result = "";

	switch (std)
	{
		case NTV2_STANDARD_1080:	result = "NTV2_STANDARD_1080";		break;
		case NTV2_STANDARD_720:		result = "NTV2_STANDARD_720";		break;
		case NTV2_STANDARD_525:		result = "NTV2_STANDARD_525";		break;
		case NTV2_STANDARD_625:		result = "NTV2_STANDARD_625";		break;
		case NTV2_STANDARD_1080p:	result = "NTV2_STANDARD_1080p";		break;
		case NTV2_STANDARD_2K:		result = "NTV2_STANDARD_2K";		break;
		default:					result = "Unknown Standard";		break;
	}

	return (result);
}
 
 
const char *NTV2FrameBufferFormatString (NTV2FrameBufferFormat fmt)
{
	const char *result = "";
	
	switch (fmt)
	{
		case NTV2_FBF_10BIT_YCBCR:				result = "NTV2_FBF_10BIT_YCBCR";		break;
		case NTV2_FBF_8BIT_YCBCR:				result = "NTV2_FBF_8BIT_YCBCR";			break;
		case NTV2_FBF_ARGB:						result = "NTV2_FBF_ARGB";				break;
		case NTV2_FBF_RGBA:						result = "NTV2_FBF_RGBA";				break;
		case NTV2_FBF_10BIT_RGB:				result = "NTV2_FBF_10BIT_RGB";			break;
		case NTV2_FBF_8BIT_YCBCR_YUY2:			result = "NTV2_FBF_8BIT_YCBCR_YUY2";	break;
		case NTV2_FBF_ABGR:						result = "NTV2_FBF_ABGR";				break;
		case NTV2_FBF_10BIT_DPX:				result = "NTV2_FBF_10BIT_DPX";			break;
		case NTV2_FBF_10BIT_YCBCR_DPX:			result = "NTV2_FBF_10BIT_YCBCR_DPX";	break;
		case NTV2_FBF_8BIT_DVCPRO:				result = "NTV2_FBF_8BIT_DVCPRO";		break;
		case NTV2_FBF_8BIT_QREZ:				result = "NTV2_FBF_8BIT_QREZ";			break;
		case NTV2_FBF_8BIT_HDV:					result = "NTV2_FBF_8BIT_HDV";			break;
		case NTV2_FBF_24BIT_RGB:				result = "NTV2_FBF_24BIT_RGB";			break;
		case NTV2_FBF_24BIT_BGR:				result = "NTV2_FBF_24BIT_BGR";			break;
		case NTV2_FBF_10BIT_YCBCRA:				result = "NTV2_FBF_10BIT_YCBCRA";		break;
		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:	result = "NTV2_FBF_10BIT_DPX_LITTLEENDIAN";	break;
		case NTV2_FBF_48BIT_RGB:				result = "NTV2_FBF_48BIT_RGB";			break;
		case NTV2_FBF_PRORES:					result = "NTV2_FBF_PRORES";				break;
		case NTV2_FBF_PRORES_DVCPRO:			result = "NTV2_FBF_PRORES_DVCPRO";		break;
		case NTV2_FBF_PRORES_HDV:				result = "NTV2_FBF_PRORES_HDV";			break;
		case NTV2_FBF_10BIT_RGB_PACKED:			result = "NTV2_FBF_10BIT_RGB_PACKED";	break;
		case NTV2_FBF_10BIT_ARGB:				result = "NTV2_FBF_10BIT_ARGB";			break;
		case NTV2_FBF_16BIT_ARGB:				result = "NTV2_FBF_16BIT_ARGB";			break;
		case NTV2_FBF_10BIT_RAW_RGB:			result = "NTV2_FBF_10BIT_RAW_RGB";		break;
		case NTV2_FBF_10BIT_RAW_YCBCR:			result = "NTV2_FBF_10BIT_RAW_YCBCR";	break;

		default:								result = "Unknown Frame Buffer Fmt";	break;
	}
	
	return (result);
}

 
const char *NTV2FrameGeometryString (NTV2FrameGeometry geom)
{
	const char *result = "";
	
	switch (geom)
	{
		case NTV2_FG_1920x1080:		result = "NTV2_FG_1920x1080";		break;
		case NTV2_FG_1280x720:		result = "NTV2_FG_1280x720";		break;
		case NTV2_FG_720x486:		result = "NTV2_FG_720x486";			break;
		case NTV2_FG_720x576:		result = "NTV2_FG_720x576";			break;
		case NTV2_FG_720x508:		result = "NTV2_FG_720x508";			break;
		case NTV2_FG_720x598:		result = "NTV2_FG_720x598";			break;
		case NTV2_FG_1920x1112:		result = "NTV2_FG_1920x1112";		break;
		case NTV2_FG_1920x1114:		result = "NTV2_FG_1920x1114";		break;
		case NTV2_FG_1280x740:		result = "NTV2_FG_1280x740";		break;
		case NTV2_FG_2048x1080:		result = "NTV2_FG_2048x1080";		break;
		case NTV2_FG_2048x1556:		result = "NTV2_FG_2048x1556";		break;
		case NTV2_FG_2048x1588:		result = "NTV2_FG_2048x1588";		break;
		case NTV2_FG_2048x1112:		result = "NTV2_FG_2048x1112";		break;
		case NTV2_FG_2048x1114:		result = "NTV2_FG_2048x1114";		break;
		case NTV2_FG_720x514:		result = "NTV2_FG_720x514";			break;
		case NTV2_FG_720x612:		result = "NTV2_FG_720x612";			break;
		case NTV2_FG_4x1920x1080:	result = "NTV2_FG_4x1920x1080";		break;
		case NTV2_FG_4x2048x1080:	result = "NTV2_FG_4x2048x1080";		break;
		default:					result = "Unknown Geometry";		break;
	}
	
	return (result);
}
 
 
const char *NTV2FrameRateString (NTV2FrameRate rate)
{
	const char *result = "";
	
	switch (rate)
	{
		case NTV2_FRAMERATE_12000:	result = "NTV2_FRAMERATE_12000";	break;
		case NTV2_FRAMERATE_11988:	result = "NTV2_FRAMERATE_11900";	break;
		case NTV2_FRAMERATE_6000:	result = "NTV2_FRAMERATE_6000";		break;
		case NTV2_FRAMERATE_5994:	result = "NTV2_FRAMERATE_5994";		break;
		case NTV2_FRAMERATE_4800:	result = "NTV2_FRAMERATE_4800";		break;
		case NTV2_FRAMERATE_4795:	result = "NTV2_FRAMERATE_4795";		break;
		case NTV2_FRAMERATE_3000:	result = "NTV2_FRAMERATE_3000";		break;
		case NTV2_FRAMERATE_2997:	result = "NTV2_FRAMERATE_2997";		break;
		case NTV2_FRAMERATE_2500:	result = "NTV2_FRAMERATE_2500";		break;
		case NTV2_FRAMERATE_2400:	result = "NTV2_FRAMERATE_2400";		break;
		case NTV2_FRAMERATE_2398:	result = "NTV2_FRAMERATE_2398";		break;
		case NTV2_FRAMERATE_1500:	result = "NTV2_FRAMERATE_1500";		break;
		case NTV2_FRAMERATE_1498:	result = "NTV2_FRAMERATE_1498";		break;
		default:					result = "Unknown Frame Rate";		break;
	}
	
	return (result);
}


const char *NTV2VideoFormatString (NTV2VideoFormat fmt)
{
	const char *result = "";
	
	switch (fmt)
	{
//		case NTV2_FORMAT_1080psf_2500:
		case NTV2_FORMAT_1080i_5000:			result = "NTV2_FORMAT_1080i_5000  /  NTV2_FORMAT_1080psf_2500";			break;
//		case NTV2_FORMAT_1080psf_2997:
		case NTV2_FORMAT_1080i_5994:			result = "NTV2_FORMAT_1080i_5994  /  NTV2_FORMAT_1080psf_2997";			break;
//		case NTV2_FORMAT_1080psf_3000:
		case NTV2_FORMAT_1080i_6000:			result = "NTV2_FORMAT_1080i_6000  /  NTV2_FORMAT_1080psf_3000";			break;
		case NTV2_FORMAT_720p_5994:				result = "NTV2_FORMAT_720p_5994";			break;
		case NTV2_FORMAT_720p_6000:				result = "NTV2_FORMAT_720p_6000";			break;
		case NTV2_FORMAT_1080psf_2398:			result = "NTV2_FORMAT_1080psf_2398";		break;
		case NTV2_FORMAT_1080psf_2400:			result = "NTV2_FORMAT_1080psf_2400";		break;
		case NTV2_FORMAT_1080p_2997:			result = "NTV2_FORMAT_1080p_2997";			break;
		case NTV2_FORMAT_1080p_3000:			result = "NTV2_FORMAT_1080p_3000";			break;
		case NTV2_FORMAT_1080p_2500:			result = "NTV2_FORMAT_1080p_2500";			break;
		case NTV2_FORMAT_1080p_2398:			result = "NTV2_FORMAT_1080p_2398";			break;
		case NTV2_FORMAT_1080p_2400:			result = "NTV2_FORMAT_1080p_2400";			break;
		case NTV2_FORMAT_1080p_2K_2398:			result = "NTV2_FORMAT_1080p_2K_2398";		break;
		case NTV2_FORMAT_1080p_2K_2400:			result = "NTV2_FORMAT_1080p_2K_2400";		break;
		case NTV2_FORMAT_1080p_2K_2500:			result = "NTV2_FORMAT_1080p_2K_2500";		break;
		case NTV2_FORMAT_1080p_2K_2997:			result = "NTV2_FORMAT_1080p_2K_2997";		break;
		case NTV2_FORMAT_1080p_2K_3000:			result = "NTV2_FORMAT_1080p_2K_3000";		break;
		case NTV2_FORMAT_1080p_2K_4795:			result = "NTV2_FORMAT_1080p_2K_4795  /  NTV2_FORMAT_1080p_2K_4795_A";	break;
		case NTV2_FORMAT_1080p_2K_4800:			result = "NTV2_FORMAT_1080p_2K_4800  /  NTV2_FORMAT_1080p_2K_4800_A";	break;
		case NTV2_FORMAT_1080p_2K_5000:			result = "NTV2_FORMAT_1080p_2K_5000  /  NTV2_FORMAT_1080p_2K_5000_A";	break;
		case NTV2_FORMAT_1080p_2K_5994:			result = "NTV2_FORMAT_1080p_2K_5994  /  NTV2_FORMAT_1080p_2K_5994_A";	break;
		case NTV2_FORMAT_1080p_2K_6000:			result = "NTV2_FORMAT_1080p_2K_6000  /  NTV2_FORMAT_1080p_2K_6000_A";	break;
		case NTV2_FORMAT_1080psf_2K_2398:		result = "NTV2_FORMAT_1080psf_2K_2398";		break;
		case NTV2_FORMAT_1080psf_2K_2400:		result = "NTV2_FORMAT_1080psf_2K_2400";		break;
		case NTV2_FORMAT_1080psf_2K_2500:		result = "NTV2_FORMAT_1080psf_2K_2500";		break;
		case NTV2_FORMAT_1080psf_2500_2:		result = "NTV2_FORMAT_1080psf_2500_2";		break;
		case NTV2_FORMAT_1080psf_2997_2:		result = "NTV2_FORMAT_1080psf_2997_2";		break;
		case NTV2_FORMAT_1080psf_3000_2:		result = "NTV2_FORMAT_1080psf_3000_2";		break;
		case NTV2_FORMAT_720p_5000:				result = "NTV2_FORMAT_720p_5000";			break;
		case NTV2_FORMAT_1080p_5000_B:			result = "NTV2_FORMAT_1080p_5000_B  /  NTV2_FORMAT_1080p_5000";		break;
		case NTV2_FORMAT_1080p_5994_B:			result = "NTV2_FORMAT_1080p_5994_B  /  NTV2_FORMAT_1080p_5994";		break;
		case NTV2_FORMAT_1080p_6000_B:			result = "NTV2_FORMAT_1080p_6000_B  /  NTV2_FORMAT_1080p_6000";		break;
		case NTV2_FORMAT_1080p_5000_A:			result = "NTV2_FORMAT_1080p_5000_A";		break;
		case NTV2_FORMAT_1080p_5994_A:			result = "NTV2_FORMAT_1080p_5994_A";		break;
		case NTV2_FORMAT_1080p_6000_A:			result = "NTV2_FORMAT_1080p_6000_A";		break;
//		case NTV2_FORMAT_END_HIGH_DEF_FORMATS:	result = "NTV2_FORMAT_END_HIGH_DEF_FORMATS";	break;
		case NTV2_FORMAT_720p_2398:				result = "NTV2_FORMAT_720p_2398";			break;
		case NTV2_FORMAT_720p_2500:				result = "NTV2_FORMAT_720p_2500";			break;
		case NTV2_FORMAT_525_5994:				result = "NTV2_FORMAT_525_5994";			break;
		case NTV2_FORMAT_625_5000:				result = "NTV2_FORMAT_625_5000";			break;
		case NTV2_FORMAT_525_2398:				result = "NTV2_FORMAT_525_2398";			break;
		case NTV2_FORMAT_525_2400:				result = "NTV2_FORMAT_525_2400";			break;
		case NTV2_FORMAT_525psf_2997:			result = "NTV2_FORMAT_525psf_2997";			break;
		case NTV2_FORMAT_625psf_2500:			result = "NTV2_FORMAT_625psf_2500";			break;
//		case NTV2_FORMAT_END_STANDARD_DEF_FORMATS:	result = "NTV2_FORMAT_END_STANDARD_DEF_FORMATS";	break;
		case NTV2_FORMAT_2K_1498:				result = "NTV2_FORMAT_2K_1498";				break; 
		case NTV2_FORMAT_2K_1500:				result = "NTV2_FORMAT_2K_1500";				break;
		case NTV2_FORMAT_2K_2398:				result = "NTV2_FORMAT_2K_2398";				break; 
		case NTV2_FORMAT_2K_2400:				result = "NTV2_FORMAT_2K_2400";				break;
		case NTV2_FORMAT_2K_2500:				result = "NTV2_FORMAT_2K_2500";				break;
//		case NTV2_FORMAT_END_2K_DEF_FORMATS:	result = "NTV2_FORMAT_END_2K_DEF_FORMATS";	break;
		case NTV2_FORMAT_4x1920x1080psf_2398:	result = "NTV2_FORMAT_4x1920x1080psf_2398";	break;
		case NTV2_FORMAT_4x1920x1080psf_2400:	result = "NTV2_FORMAT_4x1920x1080psf_2400";	break;
		case NTV2_FORMAT_4x1920x1080psf_2500:	result = "NTV2_FORMAT_4x1920x1080psf_2500";	break;
		case NTV2_FORMAT_4x1920x1080psf_2997:	result = "NTV2_FORMAT_4x1920x1080psf_2997";	break;
		case NTV2_FORMAT_4x1920x1080psf_3000:	result = "NTV2_FORMAT_4x1920x1080psf_3000";	break;
		case NTV2_FORMAT_4x1920x1080p_2398:		result = "NTV2_FORMAT_4x1920x1080p_2398";	break;
		case NTV2_FORMAT_4x1920x1080p_2400:		result = "NTV2_FORMAT_4x1920x1080p_2400";	break;
		case NTV2_FORMAT_4x1920x1080p_2500:		result = "NTV2_FORMAT_4x1920x1080p_2500";	break;
		case NTV2_FORMAT_4x1920x1080p_2997:		result = "NTV2_FORMAT_4x1920x1080p_2997";	break;
		case NTV2_FORMAT_4x1920x1080p_3000:		result = "NTV2_FORMAT_4x1920x1080p_3000";	break;
		case NTV2_FORMAT_4x2048x1080psf_2398:	result = "NTV2_FORMAT_4x2048x1080psf_2398";	break;
		case NTV2_FORMAT_4x2048x1080psf_2400:	result = "NTV2_FORMAT_4x2048x1080psf_2400";	break;
		case NTV2_FORMAT_4x2048x1080psf_2500:	result = "NTV2_FORMAT_4x2048x1080psf_2500";	break;
		case NTV2_FORMAT_4x2048x1080psf_2997:	result = "NTV2_FORMAT_4x2048x1080psf_2997";	break;
		case NTV2_FORMAT_4x2048x1080psf_3000:	result = "NTV2_FORMAT_4x2048x1080psf_3000";	break;
		case NTV2_FORMAT_4x2048x1080p_2398:		result = "NTV2_FORMAT_4x2048x1080p_2398";	break;
		case NTV2_FORMAT_4x2048x1080p_2400:		result = "NTV2_FORMAT_4x2048x1080p_2400";	break;
		case NTV2_FORMAT_4x2048x1080p_2500:		result = "NTV2_FORMAT_4x2048x1080p_2500";	break;
		case NTV2_FORMAT_4x2048x1080p_2997:		result = "NTV2_FORMAT_4x2048x1080p_2997";	break;
		case NTV2_FORMAT_4x2048x1080p_3000:		result = "NTV2_FORMAT_4x2048x1080p_3000";	break;
		case NTV2_FORMAT_4x2048x1080p_4795:		result = "NTV2_FORMAT_4x2048x1080p_4795";	break;
		case NTV2_FORMAT_4x2048x1080p_4800:		result = "NTV2_FORMAT_4x2048x1080p_4800";	break;
		case NTV2_FORMAT_4x1920x1080p_5000:		result = "NTV2_FORMAT_4x1920x1080p_5000";	break;
		case NTV2_FORMAT_4x1920x1080p_5994:		result = "NTV2_FORMAT_4x1920x1080p_5994";	break;
		case NTV2_FORMAT_4x1920x1080p_6000:		result = "NTV2_FORMAT_4x1920x1080p_6000";	break;
		case NTV2_FORMAT_4x2048x1080p_5000:		result = "NTV2_FORMAT_4x2048x1080p_5000";	break;
		case NTV2_FORMAT_4x2048x1080p_5994:		result = "NTV2_FORMAT_4x2048x1080p_5994";	break;
		case NTV2_FORMAT_4x2048x1080p_6000:		result = "NTV2_FORMAT_4x2048x1080p_6000";	break;
		case NTV2_FORMAT_4x2048x1080p_11988:	result = "NTV2_FORMAT_4x2048x1080p_11988";	break;
		case NTV2_FORMAT_4x2048x1080p_12000:	result = "NTV2_FORMAT_4x2048x1080p_12000";	break;
//		case NTV2_FORMAT_END_4K_DEF_FORMATS:	result = "NTV2_FORMAT_END_4K_DEF_FORMATS";	break;
		default:								result = "Unknown Video Format";			break;
	}
	
	return (result);

}	//	NTV2VideoFormatString


// indexed by RegisterNum
const char *ntv2RegStrings[] = 
{
"RegGlobalControl",				//   0
"RegCh1Control",				//   1
"RegCh1PCIAccessFrame",			//   2
"RegCh1OutputFrame",			//   3
"RegCh1InputFrame",				//   4
"RegCh2Control",				//   5
"RegCh2PCIAccessFrame",			//   6
"RegCh2OutputFrame",			//   7
"RegCh2InputFrame",				//   8
"RegVidProcControl",			//   9
"RegVidProcXptControl",			//  10
"RegMixerCoefficient",			//  11
"RegSplitControl",				//  12
"RegFlatMatteValue",			//  13
"RegOutputTimingControl",		//  14
"RegPanControl",				//  15
"RegReserved2",					//  16
"RegFlashProgramReg",			//  17
"RegLineCount",					//  18
"RegReserved3",					//  19
"RegVidIntControl",				//  20
"RegStatus",					//  21
"RegInputStatus",				//  22
"RegAudDetect",					//  23
"RegAudControl",				//  24
"RegAudSourceSelect",			//  25
"RegAudOutputLastAddr",			//  26
"RegAudInputLastAddr",			//  27
"RegAudCounter",				//  28
"RegRP188InOut1DBB",			//  29
"RegRP188InOut1Bits0_31",		//  30
"RegRP188InOut1Bits32_63",		//  31
"RegDMA1HostAddr",				//  32
"RegDMA1LocalAddr",				//  33
"RegDMA1XferCount",				//  34
"RegDMA1NextDesc",				//  35
"RegDMA2HostAddr",				//  36
"RegDMA2LocalAddr",				//  37
"RegDMA2XferCount",				//  38
"RegDMA2NextDesc",				//  39
"RegDMA3HostAddr",				//  40
"RegDMA3LocalAddr",				//  41
"RegDMA3XferCount",				//  42
"RegDMA3NextDesc",				//  43
"RegDMA4HostAddr",				//  44
"RegDMA4LocalAddr",				//  45
"RegDMA4XferCount",				//  46
"RegDMA4NextDesc",				//  47
"RegDMAControl",				//  48
"RegDMAIntControl",				//  49
"RegBoardID",					//  50
"RegReserved51",				//  51
"RegReserved52",				//  52
"RegReserved53",				//  53
"RegReserved54",				//  54
"RegReserved55",				//  55
"RegReserved56",				//  56
"RegReserved57",				//  57
"RegReserved58",				//  58
"RegReserved59",				//  59
"RegReserved60",				//  60
"RegReserved61",				//  61
"RegReserved62",				//  62
"RegReserved63",				//  63
"RegRP188InOut2DBB",			//  64
"RegRP188InOut2Bits0_31",		//  65
"RegRP188InOut2Bits32_63",		//  66
"RegReserved67",				//  67
"RegCh1ColorCorrectioncontrol",	//  68
"RegCh2ColorCorrectioncontrol",	//  69
"RegRS422Transmit",				//  70
"RegRS422Receive",				//  71
"RegRS422Control",				//  72
"RegReserved73",				//  73
"RegReserved74",				//  74
"RegReserved75",				//  75
"RegReserved76",				//  76
"RegReserved77",				//  77
"RegReserved78",				//  78
"RegReserved79",				//  79
"RegReserved80",				//  80
"RegFS1AnalogInputStatus",		//  81
"RegAnalogInputFormatSelection",//	82
"RegReserved83",				//  83
"RegFS1ProcAmpC1Y_C1CB",		//  84
"RegFS1ProcAmpC1CR_C2CB",		//  85
"RegFS1ProcAmpC2CROffsetY",		//  86
"RegFS1AudioDelay",				//  87
"RegAuxInterruptDelay",			//  88
"RegReserved89",				//  89
"RegFS1I2CControl",				//  90
"RegFS1I2C1Address",			//  91
"RegFS1I2C1Data",				//  92
"RegFS1I2C2Address",			//  93
"RegFS1I2C2Data",				//  94
"RegFS1ReferenceSelect",		//  95
"RegAverageAudioLevelChan1_2",	//  96
"RegAverageAudioLevelChan3_4",	//  97
"RegAverageAudioLevelChan5_6",	//  98
"RegAverageAudioLevelChan7_8",	//  99
"RegDMA1HostAddrHigh",			// 100
"RegDMA1NextDescHigh",			// 101
"RegDMA2HostAddrHigh",			// 102
"RegDMA2NextDescHigh",			// 103
"RegDMA3HostAddrHigh",			// 104
"RegDMA3NextDescHigh",			// 105
"RegDMA4HostAddrHigh",			// 106
"RegDMA4NextDescHigh",			// 107
"RegReserved108",				// 108
"RegReserved109",				// 109
"RegReserved110",				// 110
"RegReserved111",				// 111
"RegReserved112",				// 112
"RegReserved113",				// 113
"RegReserved114",				// 114
"RegReserved115",				// 115
"RegSysmonControl",				// 116
"RegSysmonConfig1_0",			// 117
"RegSysmonConfig2",				// 118
"RegSysmonVccIntDieTemp",		// 119
"kRegInternalExternalVoltage",	// 120
"RegFlashProgramReg2",			// 121
"kRegHDMIOut3DStatus1",			// 122
"kRegHDMIOut3DStatus2",			// 123
"kRegHDMIOut3DControl",			// 124
"kRegHDMIOutControl",			// 125
"kRegHDMIInputStatus",			// 126
"kRegHDMIInputControl",			// 127
"RegK2AnalogOutControl",		// 128
"RegK2SDIOut1Control",			// 129
"RegK2SDIOut2Control",			// 130
"RegK2ConversionControl",		// 131
"RegK2FrameSync1Control",		// 132
"RegReserved133",				// 133
"RegK2FrameSync2Control",		// 134
"RegReserved135",				// 135
"Xpt1 COMP CONV CSC LUT",		// 136
"Xpt2 DLO FS2 FS1 FB1",			// 137
"Xpt3 X OUT2 OUT1 AVID",		// 138
"Xpt4 MBK MBV MFK MFV",			// 139
"Xpt5 CSC2K CSC2 LUT2 FB2",		// 140
"Xpt6 CONV2 HDMI ICT Water",	// 141
"RegK2CSCoefficients1_2",		// 142
"RegK2CSCoefficients3_4",		// 143
"RegK2CSCoefficients5_6",		// 144
"RegK2CSCoefficients7_8",		// 145
"RegK2CSCoefficients9",			// 146
"RegK2CS2Coefficients1_2",		// 147
"RegK2CS2Coefficients3_4",		// 148
"RegK2CS2Coefficients5_6",		// 149
"RegK2CS2Coefficients7_8",		// 150
"RegK2CS2Coefficients9",		// 151
"RegField1Line21CaptionDecode",	// 152
"RegField2Line21CaptionDecode",	// 153
"RegField1Line21CaptionEncode",	// 154
"RegField2Line21CaptionEncode",	// 155
"RegVANCGrabberSetup",			// 156
"RegVANCGrabberStatus1",		// 157
"RegVANCGrabberStatus2",		// 158
"RegVANCGrabberDataBuffer",		// 159
"RegVANCInserterSetup1",		// 160
"RegVANCInserterSetup2",		// 161
"RegVANCInserterDataBuffer",	// 162
"RegReserved163",				// 163
"RegReserved164",				// 164
"RegCh1ControlExtended",		// 165
"RegCh2ControlExtended",		// 166
"RegAFDVANCGrabber",			// 167
"RegFS1DownConverter2Control",	// 168
"RegReserved169",				// 169
"RegReserved170",				// 170
"RegAFDVANCInserterSDI1",		// 171
"RegAFDVANCInserterSDI2",		// 172
"RegAudioChannelMappingCh1",	// 173
"RegAudioChannelMappingCh2",	// 174
"RegAudioChannelMappingCh3",	// 175
"RegAudioChannelMappingCh4",	// 176
"RegAudioChannelMappingCh5",	// 177
"RegAudioChannelMappingCh6",	// 178
"RegAudioChannelMappingCh7",	// 179
"RegAudioChannelMappingCh8",	// 180
"RegReserved181",				// 181
"RegReserved182",				// 182
"RegReserved183",				// 183
"RegReserved184",				// 184
"RegReserved185",				// 185
"RegReserved186",				// 186
"RegReserved187",				// 187
"RegVideoPayloadIDLinkA",		// 188
"RegVideoPayloadIDLinkB",		// 189
"kRegAudioOutputSourceMap",		// 190
"kK2RegXptSelectGroup11",		// 191
"RegStereoCompressor",			// 192
"kK2RegXptSelectGroup12",		// 193
"RegReserved194",				// 194
"RegReserved195",				// 195
"RegReserved196",				// 196
"RegReserved197",				// 197
"RegReserved198",				// 198
"RegReserved199",				// 199
"RegReserved200",				// 200
"RegReserved201",				// 201
"RegReserved202",				// 202
"RegReserved203",				// 203
"RegReserved204",				// 204
"RegReserved205",				// 205
"RegReserved206",				// 206
"RegReserved207",				// 207
"RegReserved208",				// 208
"RegReserved209",				// 209
"RegReserved210",				// 210
"RegReserved211",				// 211
"RegReserved212",				// 212
"RegReserved213",				// 213
"RegReserved214",				// 214
"RegReserved215",				// 215
"RegReserved216",				// 216
"RegReserved217",				// 217
"RegReserved218",				// 218
"RegReserved219",				// 219
"RegReserved220",				// 220
"RegReserved221",				// 221
"RegReserved222",				// 222
"RegReserved223",				// 223
"RegReserved224",				// 224
"RegReserved225",				// 225
"RegReserved226",				// 226
"RegReserved227",				// 227
"RegReserved228",				// 228
"RegReserved229",				// 229
"RegReserved230",				// 230
"RegReserved231",				// 231
"RegSDIInput3GStatus",			// 232
"RegSDIInput3GStatus2",			// 233
"kRegSDIOut1VPIDA",				// 234
"kRegSDIOut1VPIDB",				// 235
"kRegSDIOut2VPIDA",				// 236
"kRegSDIOut2VPIDB",				// 237
"kRegSDIIn2VPIDA",				// 238
"kRegSDIIn2VPIDB",				// 239
"kRegAud2Control",				// 240
"kRegAud2SourceSelect",			// 241
"kRegAud2OutputLastAddr",		// 242
"kRegAud2InputLastAddr",		// 243
"kRegRS4222Transmit",			// 244
"kRegRS4222Receive",			// 245
"kRegRS4222Control",			// 246
"kRegVidProc2Control",			// 247
"kRegMixer2Coefficient",		// 248
"kRegFlatMatte2Value",			// 249
"kK2RegXptSelectGroup9",		// 250
"kK2RegXptSelectGroup10",		// 251
"kRegLTC2OutBits0_31",			// 252
"kRegLTC2OutBits32_63",			// 253
"kRegLTC2InBits0_31",			// 254
"kRegLTC2InBits32_63",			// 255
"kRegSDIDirectionControl",		// 256
"kRegCh3Control",				// 257
"kRegCh3OutputFrame",			// 258
"kRegCh3InputFrame",			// 259
"kRegCh4Control",				// 260
"kRegCh4OutputFrame",			// 261
"kRegCh4InputFrame",			// 262
"kK2RegXptSelectGroup13",		// 263
"kK2RegXptSelectGroup14",		// 264
"kRegStatus2",					// 265
"kRegVidIntControl2",			// 266
"kRegGlobalControl2",			// 267
"kRegRP188InOut3DBB",			// 268
"kRegRP188InOut3Bits0_31",		// 269
"kRegRP188InOut3Bits32_63",		// 270
"kRegSDIOut3VPIDA",				// 271
"kRegSDIOut3VPIDB",				// 272
"kRegRP188InOut4DBB",			// 273
"kRegRP188InOut4Bits0_31",		// 274
"kRegRP188InOut4Bits32_63",		// 275
"kRegSDIOut4VPIDA",				// 276
"kRegSDIOut4VPIDB",				// 277
"kRegAud3Control",				// 278
"kRegAud4Control",				// 279
"kRegAud3SourceSelect",			// 280
"kRegAud4SourceSelect",			// 281
"kRegAudDetect2",				// 282
"kRegAud3OutputLastAddr",		// 283
"kRegAud3InputLastAddr",		// 284
"kRegAud4OutputLastAddr",		// 285
"kRegAud4InputLastAddr",		// 286
"kRegSDIInput3GStatus2",		// 287
"kRegInputStatus2",				// 288
"kRegCh3PCIAccessFrame",		// 289
"kRegCh4PCIAccessFrame",		// 290
"kK2RegCS3Coefficients1_2",		// 291
"kK2RegCS3Coefficients3_4",		// 292
"kK2RegCS3Coefficients5_6",		// 293
"kK2RegCS3Coefficients7_8",		// 294
"kK2RegCS3Coefficients9_10",	// 295
"kK2RegCS4Coefficients1_2",		// 296
"kK2RegCS4Coefficients3_4",		// 297
"kK2RegCS4Coefficients5_6",		// 298
"kK2RegCS4Coefficients7_8",		// 299
"kK2RegCS4Coefficients9_10",	// 300
"kK2RegXptSelectGroup17",		// 301
"kK2RegXptSelectGroup15",		// 302
"kK2RegXptSelectGroup16",		// 303
"kRegReserved304",				// 304
"kRegReserved305",				// 305
"kRegSDIIn3VPIDA",				// 306
"kRegSDIIn3VPIDB",				// 307
"kRegSDIIn4VPIDA",				// 308
"kRegSDIIn4VPIDB",				// 309
"kRegSDIWatchdogControlStatus",	// 310
"kRegSDIWatchdogTimeout",		// 311
"kRegSDIWatchdogKick1",			// 312
"kRegSDIWatchdogKick2",			// 313
"kRegReserved314",				// 314
"kRegReserved315",				// 315
"kRegLTC3EmbeddedBits0_31",     // 316
"kRegLTC3EmbeddedBits32_63",    // 317
"kRegLTC4EmbeddedBits0_31",     // 318
"kRegLTC4EmbeddedBits32_63",    // 319
"kRegReserved320",				// 320
"kRegReserved321",				// 321
"kRegReserved322",				// 322
"kRegReserved323",				// 323
"kRegReserved324",				// 324
"kRegReserved325",				// 325
"kRegReserved326",				// 326
"kRegReserved327",				// 327
"kRegReserved328",				// 328
"kRegHDMITimeCode",				// 329
"kRegHDMIReserved330",			// 330
"kRegHDMIReserved331",			// 331
"kRegHDMIReserved332",			// 332
"kRegHDMIReserved333",			// 333
"kRegHDMIReserved334",			// 334
"kRegHDMIReserved335",			// 335
"kRegHDMIReserved336",			// 336
"kK2RegSDIOut5Control",			// 337
"kRegSDIOut5VPIDA",				// 338
"kRegSDIOut5VPIDB",				// 339
"kRegRP188InOut5Bits0_31",		// 340
"kRegRP188InOut5Bits32_63",		// 341
"kRegRP188InOut5DBB",			// 342
"kRegReserved343",				// 343
"kRegLTC5EmbeddedBits0_31",     // 344
"kRegLTC5EmbeddedBits32_63",    // 345
"kRegCh5Control",				// 346
"kK2RegCS5Coefficients1_2",		// 347
"kK2RegCS5Coefficients3_4",		// 348
"kK2RegCS5Coefficients5_6",		// 349
"kK2RegCS5Coefficients7_8",		// 350
"kK2RegCS5Coefficients9_10",	// 351
"kK2RegXptSelectGroup18",		// 352
"kRegReserved353",				// 353
"kRegDC1",						// 354
"kRegDC2",						// 355
"kK2RegXptSelectGroup19",		// 356
"kK2RegXptSelectGroup20",		// 357
"kRegRasterizerControl",		// 358
"kRegHDMIV2I2C1Control",			// 360
"kRegHDMIV2I2C1Data",				// 361
"kRegHDMIV2VideoSetup",			// 362
"kRegHDMIV2HSyncDurationAndBackPorch",	// 363
"kRegHDMIV2HActive",				// 364
"kRegHDMIV2VSyncDurationAndBackPorchField1",	// 365
"kRegHDMIV2VSyncDurationAndBackPorchField2",	// 366
"kRegHDMIV2VActiveField1",		// 367
"kRegHDMIV2VActiveField2",		// 368
"kRegHDMIV2VideoStatus",			// 369
"kRegHDMIV2HorizontalMeasurements",		// 370
"kRegHDMIV2HBlankingMeasurements",		// 371
"kRegHDMIV2HBlankingMeasurements1",		// 372
"kRegHDMIV2VerticalMeasurementsField0",	// 373
"kRegHDMIV2VerticalMeasurementsField1",	// 374
"kRegHDMIV2i2c2Control",			// 375
"kRegHDMIV2i2c2Data",				// 376
"kRegReserved376",				// 376

//Scott: New Dax/Multi-channel Registers
"kRegGlobalControlCh2",			// 377
"kRegGlobalControlCh3",			// 378
"kRegGlobalControlCh4",			// 379
"kRegGlobalControlCh5",			// 380
"kRegGlobalControlCh6",			// 381
"kRegGlobalControlCh7",			// 382
"kRegGlobalControlCh8",			// 383

"kRegCh5Control",				// 384
"kRegCh5OutputFrame",			// 385
"kRegCh5InputFrame",			// 386
"kRegCh5PCIAccessFrame",		// 387

"kRegCh6Control",				// 388
"kRegCh6OutputFrame",			// 389
"kRegCh6InputFrame",			// 390
"kRegCh6PCIAccessFrame",		// 391

"kRegCh7Control",				// 392
"kRegCh7OutputFrame",			// 393
"kRegCh7InputFrame",			// 394
"kRegCh7PCIAccessFrame",		// 395

"kRegCh8Control",				// 396
"kRegCh8OutputFrame",			// 397
"kRegCh8InputFrame",			// 398
"kRegCh8PCIAccessFrame",		// 399

"kRegXptSelectGroup21",			// 400
"kRegXptSelectGroup22",			// 401
"kRegXptSelectGroup30",			// 402
"kRegXptSelectGroup23",			// 403
"kRegXptSelectGroup24",			// 404
"kRegXptSelectGroup25",			// 405
"kRegXptSelectGroup26",			// 406
"kRegXptSelectGroup27",			// 407
"kRegXptSelectGroup28",			// 408
"kRegXptSelectGroup29",			// 409

"kRegSDIIn5VPIDA",				// 410
"kRegSDIIn5VPIDB",				// 411

"kRegSDIIn6VPIDA",				// 412
"kRegSDIIn6VPIDB",				// 413
"kRegSDIOut6VPIDA",				// 414
"kRegSDIOut6VPIDB",				// 415
"kRegRP188InOut6Bits0_31",		// 416
"kRegRP188InOut6Bits32_63",		// 417
"kRegRP188InOut6DBB",			// 418
"kRegLTC6EmbeddedBits0_31",		// 419
"kRegLTC6EmbeddedBits32_63",	// 420

"kRegSDIIn7VPIDA",				// 421
"kRegSDIIn7VPIDB",				// 422
"kRegSDIOut7VPIDA",				// 423
"kRegSDIOut7VPIDB",				// 424
"kRegRP188InOut7Bits0_31",		// 425
"kRegRP188InOut7Bits32_63",		// 426
"kRegRP188InOut7DBB",			// 427
"kRegLTC7EmbeddedBits0_31",		// 428
"kRegLTC7EmbeddedBits32_63",	// 429

"kRegSDIIn8VPIDA",				// 430
"kRegSDIIn8VPIDB",				// 431
"kRegSDIOut8VPIDA",				// 432
"kRegSDIOut8VPIDB",				// 433
"kRegRP188InOut8Bits0_31",		// 434
"kRegRP188InOut8Bits32_63",		// 435
"kRegRP188InOut8DBB",			// 436
"kRegLTC8EmbeddedBits0_31",		// 437
"kRegLTC8EmbeddedBits32_63",	// 438

"kRegXptSelectGroup31",			// 439

"kRegAud5Control",				// 440
"kRegAud5SourceSelect",			// 441
"kRegAud5OutputLastAddr",		// 442
"kRegAud5InputLastAddr",		// 443

"kRegAud6Control",				// 444
"kRegAud6SourceSelect",			// 445
"kRegAud6OutputLastAddr",		// 446
"kRegAud6InputLastAddr",		// 447

"kRegAud7Control",				// 448
"kRegAud7SourceSelect",			// 449
"kRegAud7OutputLastAddr",		// 450
"kRegAud7InputLastAddr",		// 451

"kRegAud8Control",				// 452
"kRegAud8SourceSelect",			// 453
"kRegAud8OutputLastAddr",		// 454
"kRegAud8InputLastAddr",		// 455

"kRegAudioDetect5678",			// 456

"kRegSDI5678Input3GStatus",		// 457

"kRegInput56Status",			// 458
"kRegInput78Status",			// 459

"kK2RegCS6Coefficients1_2",		// 460
"kK2RegCS6Coefficients3_4",		// 461
"kK2RegCS6Coefficients5_6",		// 462
"kK2RegCS6Coefficients7_8",		// 463
"kK2RegCS6Coefficients9_10",	// 464

"kK2RegCS7Coefficients1_2",		// 465
"kK2RegCS7Coefficients3_4",		// 466
"kK2RegCS7Coefficients5_6",		// 467
"kK2RegCS7Coefficients7_8",		// 468
"kK2RegCS7Coefficients9_10",	// 469

"kK2RegCS8Coefficients1_2",		// 470
"kK2RegCS8Coefficients3_4",		// 471
"kK2RegCS8Coefficients5_6",		// 472
"kK2RegCS8Coefficients7_8",		// 473
"kK2RegCS8Coefficients9_10",	// 474

"kK2RegSDIOut6Control",			// 475
"kK2RegSDIOut7Control",			// 476
"kK2RegSDIOut8Control",			// 477

"kRegOutputTimingControlch2",	// 478
"kRegOutputTimingControlch3",	// 479
"kRegOutputTimingControlch4",	// 480
"kRegOutputTimingControlch5",	// 481
"kRegOutputTimingControlch6",	// 482
"kRegOutputTimingControlch7",	// 483
"kRegOutputTimingControlch8",	// 484

"kRegVidProc3Control",			// 485
"kRegMixer3Coefficient",		// 486
"kRegFlatMatte3Value",			// 487

"kRegVidProc4Control",			// 488
"kRegMixer4Coefficient",		// 489
"kRegFlatMatte4Value",			// 490

"kRegTRSErrorStatus",			// 491

"kRegGoAskLarsen1",				// 492
"kRegGoAskLarsen2",				// 493
"kRegGoAskLarsen3",				// 494
"kRegGoAskLarsen4",				// 495

"kRegPCMControl4321",			// 496
"kRegPCMControl8765",			// 497

"kRegReserved498",				// 498
"kRegReserved499",				// 499
"kRegReserved500",				// 500
"kRegReserved501",				// 501
"kRegReserved502",				// 502
"kRegReserved503",				// 503
"kRegReserved504",				// 504
"kRegReserved505",				// 505
"kRegReserved506",				// 506
"kRegReserved507",				// 507
"kRegReserved508",				// 508
"kRegReserved509",				// 509
"kRegReserved510",				// 510
"kRegReserved511"				// 511
};

const char *ntv2BorgFusionRegStrings[] = 
{
"RegBorgFusionBootFPGAVerBoardID",
"RegBorgFusionFPGAConfigCtrl",
"RegBorgFusionPanelPushButtonDebounced",
"RegBorgFusionPanelPushButtonChanges",
"RegBorgFusionLEDPWMThreshholds",
"RegBorgFusionPowerCtrl",
"RegBorgFusionIRQ3nIntCtrl",
"RegBorgFusionIRQ3nIntSrc",
"RegBorgFusionDisplayCtrlStatus",
"RegBorgFusionDisplayWriteData",
"RegBorgFusionAnalogFlags",
"RegBorgFusionReserved11",	
"RegBorgFusionReserved12",	
"RegBorgFusionReserved13",	
"RegBorgFusionReserved14",	
"RegBorgFusionReserved15",	
"RegBorgFusionTempSensor1",
"RegBorgFusionTempSensor2",
"RegBorgFusion5_0vPowerRail",
"RegBorgFusion2_5vPowerRail",
"RegBorgFusion1_95vPowerRail",
"RegBorgFusion1_8vPowerRail",
"RegBorgFusion1_5vPowerRail",
"RegBorgFusion1_0vPowerRail",
"RegBorgFusion12vPowerRail",
"RegBorgFusion3_3vPowerRail",
"RegBorgFusionReserved26",	
"RegBorgFusionReserved27",	
"RegBorgFusionReserved28",	
"RegBorgFusionReserved29",	
"RegBorgFusionReserved30",	
"RegBorgFusionReserved31",	
"RegBorgFusionReserved32",	
"RegBorgFusionReserved33",	
"RegBorgFusionReserved34",	
"RegBorgFusionReserved35",	
"RegBorgFusionReserved36",	
"RegBorgFusionReserved37",	
"RegBorgFusionReserved38",	
"RegBorgFusionReserved39",	
"RegBorgFusionReserved40",	
"RegBorgFusionReserved41",	
"RegBorgFusionReserved42",	
"RegBorgFusionReserved43",	
"RegBorgFusionReserved44",	
"RegBorgFusionReserved45",	
"RegBorgFusionReserved46",	
"RegBorgFusionReserved47",	
"RegBorgFusionReserved48",	
"RegBorgFusionReserved49",	
"RegBorgFusionProdIDLo",	
"RegBorgFusionProdIDHi",	
};

const char *ntv2DNXRegStrings[] =
{
"RegDNX_DeviceID",
"RegDNX_Revision",
"RegDNX_Diagnostics",
"RegDNX_Reset",
"RegDNX_NestorControl",
"RegDNX_NestorAutoloadControl",
"RegDNX_Status",
"RegDNX_NestorInterrupt",
"RegDNX_NestorInterruptEnable",
"RegDNX_EncoderM1AStatus",
"RegDNX_EncoderM2AStatus",
"RegDNX_DecoderM1BStatus",
"RegDNX_DecoderM2BStatus",
"RegDNX_TempSensorReadData",
"RegDNX_Timeout",             // Register Index 14

"RegDNX_Field1StartEnd",
"RegDNX_Field1ActiveEnd",
"RegDNX_Field2StartEnd",
"RegDNX_Field2ActiveEnd",
"RegDNX_HorizontalStartEnd",
"RegDNX_FormatChromaClipping",
"RegDNX_FormatLumaClipping",
"RegDNX_Reserved1",           // Register Index 22

"RegDNX_A0Parameter",
"RegDNX_A1Parameter",
"RegDNX_A2Parameter",
"RegDNX_A3Parameter",
"RegDNX_A4Parameter",
"RegDNX_A5Parameter",
"RegDNX_A6Parameter",
"RegDNX_A7Parameter",
"RegDNX_A8Parameter",
"RegDNX_A9Parameter",         // Register Index 32
"RegDNX_TOFHorizontalResetValue",
"RegDNX_TOFVeritcalResetValue",
"RegDNX_HorizontalStartOfHANCCode",
"RegDNX_HorizontalStartOfSAVCode",
"RegDNX_HorizontalStartOfActiveVideo",
"RegDNX_HorizontalEndOfLine", // Register Index 38

"RegDNX_EncoderInterrupt",
"RegDNX_EncoderControl",
"RegDNX_EncoderInterruptEnable",
"RegDNX_RateControlAddress",
"RegDNX_RateControlReadData",
"RegDNX_MacroblockLineNumber",
"RegDNX_RateControlIndex",
"RegDNX_DCTPackerIndex",
"RegDNX_EncodeTableWrite",
"RegDNX_EncoderDebug",

"RegDNX_EncoderVCIDRegister",

"RegDNX_EncoderParameterRAMLocation0_0",
"RegDNX_EncoderParameterRAMLocation1_0",
"RegDNX_EncoderParameterRAMLocation0_1",
"RegDNX_EncoderParameterRAMLocation1_1",
"RegDNX_EncoderParameterRAMLocation0_2",
"RegDNX_EncoderParameterRAMLocation1_2",
"RegDNX_EncoderParameterRAMLocation0_3",
"RegDNX_EncoderParameterRAMLocation1_3",
"RegDNX_EncoderParameterRAMLocation0_4",
"RegDNX_EncoderParameterRAMLocation1_4",
"RegDNX_EncoderParameterRAMLocation0_5",
"RegDNX_EncoderParameterRAMLocation1_5",
"RegDNX_EncoderParameterRAMLocation0_6",
"RegDNX_EncoderParameterRAMLocation1_6",

"RegDNX_DecoderInterrupt",
"RegDNX_DecoderControl",
"RegDNX_DecoderInterruptEnable",
"RegDNX_DecodeTime",
"RegDNX_FrameCount",
"RegDNX_DecodeTableWrite",
"RegDNX_DecoderDebug",
"RegDNX_Pipe1StallStatus1",
"RegDNX_Pipe1StallStatus2",
"RegDNX_Pipe2StallStatus1",
"RegDNX_Pipe2StallStatus2",

"RegDNX_DecoderVCIDRegister",

"RegDNX_DecoderParameterRAMLocation0_0",
"RegDNX_DecoderParameterRAMLocation1_0",
"RegDNX_DecoderParameterRAMLocation0_1",
"RegDNX_DecoderParameterRAMLocation1_1",
"RegDNX_DecoderParameterRAMLocation0_2",
"RegDNX_DecoderParameterRAMLocation1_2",
"RegDNX_DecoderParameterRAMLocation0_3",
"RegDNX_DecoderParameterRAMLocation1_3",
"RegDNX_DecoderParameterRAMLocation0_4",
"RegDNX_DecoderParameterRAMLocation1_4",
"RegDNX_DecoderParameterRAMLocation0_5",
"RegDNX_DecoderParameterRAMLocation1_5",
"RegDNX_DecoderParameterRAMLocation0_6",
"RegDNX_DecoderParameterRAMLocation1_6"
};

const char *ntv2IoHDRegStrings[] = 
{
"RegIsochGlobalStatus",
"RegIsochGlobalControl",
"RegLocalRegBaseAddrHi",
"RegLocalRegBaseAddrLo",
"RegIsochVideoCh1Status",
"RegIsochVideoCh1Control",
"RegIsochVideoCh1Packets",
"RegReserved2055",
"RegIsochAudioCh1Status",
"RegIsochAudioCh1Control",
"RegIsochAudioCh1Packets",
"RegReserved2059",
"RegIsochAudioCh2Status",
"RegIsochAudioCh2Control",
"RegIsochAudioCh2Packets",
"RegReserved2063",
"RegFireWireErrors",
"RegAudioBufferSize",
"RegAudioPlaybackPtr",
"RegAudioPlaybackControl",
"RegAVBitFileCSR",
"RegBitFileBusy",
"RegGetBitFileInfoSelect",
"RegReserved2071",
"RegReserved2072",
"RegReserved2073",
"RegReserved2074",
"RegReserved2075",
"RegReserved2076",
"RegReserved2077",
"RegReserved2078",
"RegReserved2079",
"RegReserved2080",
"RegReserved2081",
"RegReserved2082",
"RegReserved2083",
"RegReserved2084",
"RegReserved2085",
"RegReserved2086",
"RegReserved2087",
"RegReserved2088",
"RegReserved2089",
"RegReserved2090",
"RegReserved2091",
"RegReserved2092",
"RegReserved2093",
"RegReserved2094",
"RegReserved2095",
"RegReserved2096",
"RegReserved2097",
"RegReserved2098",
"RegReserved2099",
"RegReserved2100",
"RegReserved2101",
"RegReserved2102",
"RegReserved2103",
"RegReserved2104",
"RegReserved2105",
"RegReserved2106",
"RegReserved2107",
"RegReserved2108",
"RegReserved2109",
"RegReserved2110",
"RegReserved2111",
"RegUpdateControl",
"RegUpdateSoftwareVersion",
"RegUpdateProductVersion",
"RegUpdateBitFileCommand",
"RegUpdateCRC",
"RegUpdateStatus",
"RegSerialNumber",
"RegUpdateData",
};

#ifdef MSWindows
#include <windows.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>

void __cdecl odprintf(const char *format, ...)
{
//#ifdef _DEBUG
char	buf[4096], *p = buf;
va_list	args;

        va_start(args, format);
        p += _vsnprintf_s(buf, sizeof buf - 1, format, args);
        va_end(args);

        while ( p > buf  &&  isspace(p[-1]) )
                *--p = '\0';

        *p++ = '\r';
        *p++ = '\n';
        *p   = '\0';

        ::OutputDebugStringA(buf);
//#endif
}
#endif

#if !defined (NTV2_DEPRECATE)
const char * NTV2BoardTypeString (NTV2BoardType type)	{return NTV2DeviceTypeString (type);}
const char * NTV2BoardIDString (NTV2DeviceID id)		{return NTV2DeviceIDString (id);}
#endif	//	!defined (NTV2_DEPRECATE)
