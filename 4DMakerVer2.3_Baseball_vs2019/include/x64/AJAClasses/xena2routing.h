/**
	@brief		Declared the now-obsolete CXena2Routing class.
	@deprecate	Please #include "ntv2signalrouter.h" instead.
**/

#ifndef XENA2ROUTING_H
#define XENA2ROUTING_H
	#if defined (NTV2_DEPRECATE)
		#error	"'xena2routing.h' is deprecated. Please #include 'ntv2signalrouter.h' instead"
	#else
		#include "ntv2signalrouter.h"
	#endif	//	else !defined (NTV2_DEPRECATE)

#endif	//	XENA2ROUTING_H
