/////////////////////////////////////////////////////////////////////////////
// xenaserialcontrol.h
//
// Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef _XENASERIALCONTROL_H
#define _XENASERIALCONTROL_H

#include "ajaexport.h"
#include "basemachinecontrol.h"
#include "ntv2card.h"	// Added by ClassView

#define XENA_SERIAL_RESPONSE_SIZE 64

typedef struct {
	UByte length;
	UByte buffer[XENA_SERIAL_RESPONSE_SIZE];
} SerialMachineResponse;

/////////////////////////////////////////////////////////////////////////////
// CXenaSerialControl


class AJAExport CXenaSerialControl : public CBaseMachineControl
{
	//	Instance Methods
	public:
		//	Constructors & Destructors
		CXenaSerialControl (UWord inSerialPortIndexNum = 0);
		CXenaSerialControl (UWord inDeviceIndex, NTV2BoardType boardType, UWord inSerialPortIndexNum = 0);
		virtual ~CXenaSerialControl ();

		//	Control Methods
		virtual bool			Open (void);
		virtual void			Close (void);
		virtual ULWord			Play (void);
		virtual ULWord			ReversePlay (void);
		virtual ULWord			Stop (void);
		virtual ULWord			FastForward (void);
		virtual ULWord			Rewind (void);
		virtual ULWord			AdvanceFrame (void);
		virtual ULWord			BackFrame (void);
		virtual ULWord			GotoFrameByHoursMinutesSeconds (UByte hours, UByte minutes, UByte seconds, UByte frames);
		virtual ULWord			GetTimecodeString (SByte * timecodeString);

		//	Accessors
		virtual inline const SerialMachineResponse &	GetLastResponse (void) const	{return _serialMachineResponse;}	//	Warning: Not thread safe!

		virtual bool			WriteCommand (const UByte * txBuffer, bool response = true);

		virtual bool			WriteTxBuffer (const UByte * txBuffer, UWord length);
		virtual bool			ReadRxBuffer (UByte * rxBuffer, UWord & actualLength, UWord maxlength);

		virtual bool			GotACK (void);

		virtual bool			WaitForRxInterrupt (void);
		virtual bool			WaitForTxInterrupt (void);

	//	Instance Variables
	protected:
		CNTV2Card				_ntv2Card;
		SerialMachineResponse	_serialMachineResponse;
		RegisterNum				_controlRegisterNum;	///	Which UART control register to use:  kRegRS422Control (72) or kRegRS4222Control (246)
		RegisterNum				_receiveRegisterNum;	///	Which UART receive data register to use:  kRegRS422Receive (71) or kRegRS4222Receive (245)
		RegisterNum				_transmitRegisterNum;	///	Which UART transmit data register to use:  kRegRS422Transmit (70) or kRegRS4222Transmit (244)

};	//	CXenaSerialControl

/////////////////////////////////////////////////////////////////////////////
#endif
