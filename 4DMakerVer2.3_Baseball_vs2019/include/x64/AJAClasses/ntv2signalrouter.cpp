/**
	@brief		CNTV2SignalRouter implementation.
	@copyright	(C) 2014 AJA Video Systems, Inc.	Proprietary and confidential.
**/
#include "ntv2signalrouter.h"
#include "ntv2debug.h"
#include "ntv2utils.h"
#include <memory.h>
#include <stdio.h>


using namespace std;


ostream & operator << (ostream & inOutStream, const NTV2CrosspointIDSet & inObj)
{
	NTV2CrosspointIDSetConstIter	iter (inObj.begin ());
	while (iter != inObj.end ())
	{
		inOutStream << ::NTV2CrosspointIDToString (*iter);
		++iter;
		if (iter == inObj.end ())
			break;
		inOutStream << ", ";
	}
	return inOutStream;
}


void CNTV2SignalRouter::clear (void)
{
	::memset (reinterpret_cast <void *> (&mRoutingTable), 0, sizeof (NTV2RoutingTable));
}


bool CNTV2SignalRouter::add (const NTV2RoutingEntry & inEntry)
{
	if (mRoutingTable.numEntries >= MAX_ROUTING_ENTRIES)
		return false;
	
	mRoutingTable.routingEntry [mRoutingTable.numEntries++] = inEntry;

	return true;  
}


bool CNTV2SignalRouter::addWithValue (const NTV2RoutingEntry & inEntry, const ULWord inValue)
{
	if (mRoutingTable.numEntries >= MAX_ROUTING_ENTRIES)
		return false;

	NTV2RoutingEntry &	rtEntry	(mRoutingTable.routingEntry [mRoutingTable.numEntries++]);
	rtEntry = inEntry;
	rtEntry.value = inValue;

	return true;
}


bool CNTV2SignalRouter::addWithRegisterAndValue (const NTV2RoutingEntry & inEntry, const ULWord inRegisterNum, const ULWord inValue, const ULWord inMask, const ULWord inShift)
{
	(void) inEntry;
	if (mRoutingTable.numEntries >= MAX_ROUTING_ENTRIES)
		return false;

	NTV2RoutingEntry &	rtEntry	(mRoutingTable.routingEntry [mRoutingTable.numEntries++]);
	rtEntry.registerNum = inRegisterNum;
	rtEntry.mask = inMask;
	rtEntry.shift = inShift;
	rtEntry.value = inValue;

	return true;
}


const NTV2RoutingEntry & CNTV2SignalRouter::GetRoutingEntry (const ULWord inIndex) const
{
	if (inIndex >= MAX_ROUTING_ENTRIES)
		return mRoutingTable.routingEntry [0]; ///ERROR
	else
		return mRoutingTable.routingEntry [inIndex];
}



//////////	Stream Operators


std::ostream & operator << (std::ostream & inOutStream, const NTV2RoutingEntry & inObj)
{
	return inOutStream << ntv2RegStrings [inObj.registerNum] << " (" << inObj.registerNum << ") mask=0x" << inObj.mask << " shift=" << inObj.shift << " value=0x" << inObj.value;
}


std::ostream & operator << (std::ostream & inOutStream, const NTV2RoutingTable & inObj)
{
	for (ULWord num (0);  num < inObj.numEntries;  num++)
		inOutStream << num << ":  " << inObj.routingEntry [num] << endl;
	return inOutStream;
}


std::ostream & operator << (std::ostream & inOutStream, const CNTV2SignalRouter & inObj)
{
	const NTV2RoutingTable &	routingTable	(inObj);
	return inOutStream << routingTable;
}



//////////	Per-widget input crosspoint selection register/mask/shift values
#if defined (NTV2_DEPRECATE)
	//	The new way:	make the NTV2RoutingEntrys invisible to SDK clients (must use Getter functions):
	#define	AJA_LOCAL_STATIC	static
#else	//	!defined (NTV2_DEPRECATE)
	//	The old way:	keep the NTV2RoutingEntrys visible to SDK clients (don't need to use Getter functions):
	#define	AJA_LOCAL_STATIC
#endif	//	!defined (NTV2_DEPRECATE)


AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer1InputSelectEntry		=	{kK2RegXptSelectGroup2,		kK2RegMaskFrameBuffer1InputSelect,			kK2RegShiftFrameBuffer1InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer1BInputSelectEntry	=	{kRegXptSelectGroup34,		kK2RegMaskFrameBuffer1BInputSelect,			kK2RegShiftFrameBuffer1BInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer2InputSelectEntry		=	{kK2RegXptSelectGroup5,		kK2RegMaskFrameBuffer2InputSelect,			kK2RegShiftFrameBuffer2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer2BInputSelectEntry	=	{kRegXptSelectGroup34,		kK2RegMaskFrameBuffer2BInputSelect,			kK2RegShiftFrameBuffer2BInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer3InputSelectEntry		=	{kK2RegXptSelectGroup13,	kK2RegMaskFrameBuffer3InputSelect,			kK2RegShiftFrameBuffer3InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer3BInputSelectEntry	=	{kRegXptSelectGroup34,		kK2RegMaskFrameBuffer3BInputSelect,			kK2RegShiftFrameBuffer3BInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer4InputSelectEntry		=	{kK2RegXptSelectGroup13,	kK2RegMaskFrameBuffer4InputSelect,			kK2RegShiftFrameBuffer4InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer4BInputSelectEntry	=	{kRegXptSelectGroup34,		kK2RegMaskFrameBuffer4BInputSelect,			kK2RegShiftFrameBuffer4BInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer5InputSelectEntry		=	{kRegXptSelectGroup21,		kK2RegMaskFrameBuffer5InputSelect,			kK2RegShiftFrameBuffer5InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer5BInputSelectEntry	=	{kRegXptSelectGroup35,		kK2RegMaskFrameBuffer5BInputSelect,			kK2RegShiftFrameBuffer5BInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer6InputSelectEntry		=	{kRegXptSelectGroup21,		kK2RegMaskFrameBuffer6InputSelect,			kK2RegShiftFrameBuffer6InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer6BInputSelectEntry	=	{kRegXptSelectGroup35,		kK2RegMaskFrameBuffer6BInputSelect,			kK2RegShiftFrameBuffer6BInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer7InputSelectEntry		=	{kRegXptSelectGroup21,		kK2RegMaskFrameBuffer7InputSelect,			kK2RegShiftFrameBuffer7InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer7BInputSelectEntry	=	{kRegXptSelectGroup35,		kK2RegMaskFrameBuffer7BInputSelect,			kK2RegShiftFrameBuffer7BInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer8InputSelectEntry		=	{kRegXptSelectGroup21,		kK2RegMaskFrameBuffer8InputSelect,			kK2RegShiftFrameBuffer8InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameBuffer8BInputSelectEntry	=	{kRegXptSelectGroup35,		kK2RegMaskFrameBuffer8BInputSelect,			kK2RegShiftFrameBuffer8BInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC1VidInputSelectEntry			=	{kK2RegXptSelectGroup1,		kK2RegMaskColorSpaceConverterInputSelect,	kK2RegShiftColorSpaceConverterInputSelect,	0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC1KeyInputSelectEntry			=	{kK2RegXptSelectGroup3,		kK2RegMaskCSC1KeyInputSelect,				kK2RegShiftCSC1KeyInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC2VidInputSelectEntry			=	{kK2RegXptSelectGroup5,		kK2RegMaskCSC2VidInputSelect,				kK2RegShiftCSC2VidInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC2KeyInputSelectEntry			=	{kK2RegXptSelectGroup5,		kK2RegMaskCSC2KeyInputSelect,				kK2RegShiftCSC2KeyInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC3VidInputSelectEntry			=	{kK2RegXptSelectGroup17,	kK2RegMaskCSC3VidInputSelect,				kK2RegShiftCSC3VidInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC3KeyInputSelectEntry			=	{kK2RegXptSelectGroup17,	kK2RegMaskCSC3KeyInputSelect,				kK2RegShiftCSC3KeyInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC4VidInputSelectEntry			=	{kK2RegXptSelectGroup17,	kK2RegMaskCSC4VidInputSelect,				kK2RegShiftCSC4VidInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC4KeyInputSelectEntry			=	{kK2RegXptSelectGroup17,	kK2RegMaskCSC4KeyInputSelect,				kK2RegShiftCSC4KeyInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC5VidInputSelectEntry			=	{kK2RegXptSelectGroup18,	kK2RegMaskCSC5VidInputSelect,				kK2RegShiftCSC5VidInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC5KeyInputSelectEntry			=	{kK2RegXptSelectGroup18,	kK2RegMaskCSC5KeyInputSelect,				kK2RegShiftCSC5KeyInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC6VidInputSelectEntry			=	{kRegXptSelectGroup30,		kK2RegMaskCSC6VidInputSelect,				kK2RegShiftCSC6VidInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC6KeyInputSelectEntry			=	{kRegXptSelectGroup30,		kK2RegMaskCSC6KeyInputSelect,				kK2RegShiftCSC6KeyInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC7VidInputSelectEntry			=	{kRegXptSelectGroup23,		kK2RegMaskCSC7VidInputSelect,				kK2RegShiftCSC7VidInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC7KeyInputSelectEntry			=	{kRegXptSelectGroup23,      kK2RegMaskCSC7KeyInputSelect,				kK2RegShiftCSC7KeyInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC8VidInputSelectEntry			=	{kRegXptSelectGroup23,      kK2RegMaskCSC8VidInputSelect,				kK2RegShiftCSC8VidInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC8KeyInputSelectEntry			=	{kRegXptSelectGroup23,      kK2RegMaskCSC8KeyInputSelect,				kK2RegShiftCSC8KeyInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry XptLUTInputSelectEntry			=	{kK2RegXptSelectGroup1,		kK2RegMaskXptLUTInputSelect,				kK2RegShiftXptLUTInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry XptLUT2InputSelectEntry			=	{kK2RegXptSelectGroup5,		kK2RegMaskXptLUT2InputSelect,				kK2RegShiftXptLUT2InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry XptLUT3InputSelectEntry			=	{kK2RegXptSelectGroup12,	kK2RegMaskXptLUT3InputSelect,				kK2RegShiftXptLUT3InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry XptLUT4InputSelectEntry			=	{kK2RegXptSelectGroup12,	kK2RegMaskXptLUT4InputSelect,				kK2RegShiftXptLUT4InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry XptLUT5InputSelectEntry			=	{kK2RegXptSelectGroup12,	kK2RegMaskXptLUT5InputSelect,				kK2RegShiftXptLUT5InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry XptLUT6InputSelectEntry			=	{kRegXptSelectGroup24,		kK2RegMaskXptLUT6InputSelect,				kK2RegShiftXptLUT6InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry XptLUT7InputSelectEntry			=	{kRegXptSelectGroup24,		kK2RegMaskXptLUT7InputSelect,				kK2RegShiftXptLUT7InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry XptLUT8InputSelectEntry			=	{kRegXptSelectGroup24,		kK2RegMaskXptLUT8InputSelect,				kK2RegShiftXptLUT8InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut1StandardSelectEntry		=	{kK2RegSDIOut1Control,      kK2RegMaskSDIOutStandard,					kK2RegShiftSDIOutStandard,					0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut2StandardSelectEntry		=	{kK2RegSDIOut2Control,      kK2RegMaskSDIOutStandard,					kK2RegShiftSDIOutStandard,					0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut3StandardSelectEntry		=	{kK2RegSDIOut3Control,      kK2RegMaskSDIOutStandard,					kK2RegShiftSDIOutStandard,					0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut4StandardSelectEntry		=	{kK2RegSDIOut4Control,      kK2RegMaskSDIOutStandard,					kK2RegShiftSDIOutStandard,					0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut1InputSelectEntry			=	{kK2RegXptSelectGroup3,		kK2RegMaskSDIOut1InputSelect,				kK2RegShiftSDIOut1InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut1InputDS2SelectEntry		=	{kK2RegXptSelectGroup10,	kK2RegMaskSDIOut1DS2InputSelect,			kK2RegShiftSDIOut1DS2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut2InputSelectEntry			=	{kK2RegXptSelectGroup3,		kK2RegMaskSDIOut2InputSelect,				kK2RegShiftSDIOut2InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut2InputDS2SelectEntry		=	{kK2RegXptSelectGroup10,	kK2RegMaskSDIOut2DS2InputSelect,			kK2RegShiftSDIOut2DS2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut3InputSelectEntry			=	{kK2RegXptSelectGroup8,		kK2RegMaskSDIOut3InputSelect,				kK2RegShiftSDIOut3InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut3InputDS2SelectEntry		=	{kK2RegXptSelectGroup14,	kK2RegMaskSDIOut3DS2InputSelect,			kK2RegShiftSDIOut3DS2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut4InputSelectEntry			=	{kK2RegXptSelectGroup8,		kK2RegMaskSDIOut4InputSelect,				kK2RegShiftSDIOut4InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut4InputDS2SelectEntry		=	{kK2RegXptSelectGroup14,	kK2RegMaskSDIOut4DS2InputSelect,			kK2RegShiftSDIOut4DS2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut5InputSelectEntry			=	{kK2RegXptSelectGroup8,		kK2RegMaskSDIOut5InputSelect,				kK2RegShiftSDIOut5InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut5InputDS2SelectEntry		=	{kK2RegXptSelectGroup14,	kK2RegMaskSDIOut5DS2InputSelect,			kK2RegShiftSDIOut5DS2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut6InputSelectEntry			=	{kRegXptSelectGroup22,		kK2RegMaskSDIOut6InputSelect,				kK2RegShiftSDIOut6InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut6InputDS2SelectEntry		=	{kRegXptSelectGroup22,		kK2RegMaskSDIOut6DS2InputSelect,			kK2RegShiftSDIOut6DS2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut7InputSelectEntry			=	{kRegXptSelectGroup22,		kK2RegMaskSDIOut7InputSelect,				kK2RegShiftSDIOut7InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut7InputDS2SelectEntry		=	{kRegXptSelectGroup22,		kK2RegMaskSDIOut7DS2InputSelect,			kK2RegShiftSDIOut7DS2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut8InputSelectEntry			=	{kRegXptSelectGroup30,		kK2RegMaskSDIOut8InputSelect,				kK2RegShiftSDIOut8InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry SDIOut8InputDS2SelectEntry		=	{kRegXptSelectGroup30,		kK2RegMaskSDIOut8DS2InputSelect,			kK2RegShiftSDIOut8DS2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn1InputSelectEntry		=	{kK2RegXptSelectGroup11,	kK2RegMaskDuallinkIn1InputSelect,			kK2RegShiftDuallinkIn1InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn1DSInputSelectEntry	=	{kK2RegXptSelectGroup11,	kK2RegMaskDuallinkIn1DSInputSelect,			kK2RegShiftDuallinkIn1DSInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn2InputSelectEntry		=	{kK2RegXptSelectGroup11,	kK2RegMaskDuallinkIn2InputSelect,			kK2RegShiftDuallinkIn2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn2DSInputSelectEntry	=	{kK2RegXptSelectGroup11,	kK2RegMaskDuallinkIn2DSInputSelect,			kK2RegShiftDuallinkIn2DSInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn3InputSelectEntry		=	{kK2RegXptSelectGroup15,	kK2RegMaskDuallinkIn3InputSelect,			kK2RegShiftDuallinkIn3InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn3DSInputSelectEntry	=	{kK2RegXptSelectGroup15,	kK2RegMaskDuallinkIn3DSInputSelect,			kK2RegShiftDuallinkIn3DSInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn4InputSelectEntry		=	{kK2RegXptSelectGroup15,	kK2RegMaskDuallinkIn4InputSelect,			kK2RegShiftDuallinkIn4InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn4DSInputSelectEntry	=	{kK2RegXptSelectGroup15,	kK2RegMaskDuallinkIn4DSInputSelect,			kK2RegShiftDuallinkIn4DSInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn5InputSelectEntry		=	{kRegXptSelectGroup25,		kK2RegMaskDuallinkIn5InputSelect,			kK2RegShiftDuallinkIn5InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn5DSInputSelectEntry	=	{kRegXptSelectGroup25,		kK2RegMaskDuallinkIn5DSInputSelect,			kK2RegShiftDuallinkIn5DSInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn6InputSelectEntry		=	{kRegXptSelectGroup25,		kK2RegMaskDuallinkIn6InputSelect,			kK2RegShiftDuallinkIn6InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn6DSInputSelectEntry	=	{kRegXptSelectGroup25,		kK2RegMaskDuallinkIn6DSInputSelect,			kK2RegShiftDuallinkIn6DSInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn7InputSelectEntry		=	{kRegXptSelectGroup26,		kK2RegMaskDuallinkIn7InputSelect,			kK2RegShiftDuallinkIn7InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn7DSInputSelectEntry	=	{kRegXptSelectGroup26,		kK2RegMaskDuallinkIn7DSInputSelect,			kK2RegShiftDuallinkIn7DSInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn8InputSelectEntry		=	{kRegXptSelectGroup26,		kK2RegMaskDuallinkIn8InputSelect,			kK2RegShiftDuallinkIn8InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkIn8DSInputSelectEntry	=	{kRegXptSelectGroup26,		kK2RegMaskDuallinkIn8DSInputSelect,			kK2RegShiftDuallinkIn8DSInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkOut1InputSelectEntry		=	{kK2RegXptSelectGroup2,		kK2RegMaskDuallinkOutInputSelect,			kK2RegShiftDuallinkOutInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkOut2InputSelectEntry		=	{kK2RegXptSelectGroup7,		kK2RegMaskDuallinkOut2InputSelect,			kK2RegShiftDuallinkOut2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkOut3InputSelectEntry		=	{kK2RegXptSelectGroup16,	kK2RegMaskDuallinkOut3InputSelect,			kK2RegShiftDuallinkOut3InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkOut4InputSelectEntry		=	{kK2RegXptSelectGroup16,	kK2RegMaskDuallinkOut4InputSelect,			kK2RegShiftDuallinkOut4InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkOut5InputSelectEntry		=	{kK2RegXptSelectGroup16,	kK2RegMaskDuallinkOut5InputSelect,			kK2RegShiftDuallinkOut5InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkOut6InputSelectEntry		=	{kRegXptSelectGroup27,		kK2RegMaskDuallinkOut6InputSelect,			kK2RegShiftDuallinkOut6InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkOut7InputSelectEntry		=	{kRegXptSelectGroup27,		kK2RegMaskDuallinkOut7InputSelect,			kK2RegShiftDuallinkOut7InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry DualLinkOut8InputSelectEntry		=	{kRegXptSelectGroup27,		kK2RegMaskDuallinkOut8InputSelect,			kK2RegShiftDuallinkOut8InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer1BGKeyInputSelectEntry		=	{kK2RegXptSelectGroup4,		kK2RegMaskMixerBGKeyInputSelect,			kK2RegShiftMixerBGKeyInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer1BGVidInputSelectEntry		=	{kK2RegXptSelectGroup4,		kK2RegMaskMixerBGVidInputSelect,			kK2RegShiftMixerBGVidInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer1FGKeyInputSelectEntry		=	{kK2RegXptSelectGroup4,		kK2RegMaskMixerFGKeyInputSelect,			kK2RegShiftMixerFGKeyInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer1FGVidInputSelectEntry		=	{kK2RegXptSelectGroup4,		kK2RegMaskMixerFGVidInputSelect,			kK2RegShiftMixerFGVidInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer2BGKeyInputSelectEntry		=	{kK2RegXptSelectGroup9,		kK2RegMaskMixer2BGKeyInputSelect,			kK2RegShiftMixer2BGKeyInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer2BGVidInputSelectEntry		=	{kK2RegXptSelectGroup9,		kK2RegMaskMixer2BGVidInputSelect,			kK2RegShiftMixer2BGVidInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer2FGKeyInputSelectEntry		=	{kK2RegXptSelectGroup9,		kK2RegMaskMixer2FGKeyInputSelect,			kK2RegShiftMixer2FGKeyInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer2FGVidInputSelectEntry		=	{kK2RegXptSelectGroup9,		kK2RegMaskMixer2FGVidInputSelect,			kK2RegShiftMixer2FGVidInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer3BGKeyInputSelectEntry		=	{kRegXptSelectGroup28,		kK2RegMaskMixer3BGKeyInputSelect,			kK2RegShiftMixer3BGKeyInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer3BGVidInputSelectEntry		=	{kRegXptSelectGroup28,		kK2RegMaskMixer3BGVidInputSelect,			kK2RegShiftMixer3BGVidInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer3FGKeyInputSelectEntry		=	{kRegXptSelectGroup28,		kK2RegMaskMixer3FGKeyInputSelect,			kK2RegShiftMixer3FGKeyInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer3FGVidInputSelectEntry		=	{kRegXptSelectGroup28,		kK2RegMaskMixer3FGVidInputSelect,			kK2RegShiftMixer3FGVidInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer4BGKeyInputSelectEntry		=	{kRegXptSelectGroup29,		kK2RegMaskMixer4BGKeyInputSelect,			kK2RegShiftMixer4BGKeyInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer4BGVidInputSelectEntry		=	{kRegXptSelectGroup29,		kK2RegMaskMixer4BGVidInputSelect,			kK2RegShiftMixer4BGVidInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer4FGKeyInputSelectEntry		=	{kRegXptSelectGroup29,		kK2RegMaskMixer4FGKeyInputSelect,			kK2RegShiftMixer4FGKeyInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Mixer4FGVidInputSelectEntry		=	{kRegXptSelectGroup29,		kK2RegMaskMixer4FGVidInputSelect,			kK2RegShiftMixer4FGVidInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry HDMIOutInputSelectEntry			=	{kK2RegXptSelectGroup6,		kK2RegMaskHDMIOutInputSelect,				kK2RegShiftHDMIOutInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry HDMIOutQ1InputSelectEntry		=	{kK2RegXptSelectGroup6,		kK2RegMaskHDMIOutInputSelect,				kK2RegShiftHDMIOutInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry HDMIOutQ2InputSelectEntry		=	{kK2RegXptSelectGroup20,	kK2RegMaskHDMIOutV2Q2InputSelect,			kK2RegShiftHDMIOutV2Q2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry HDMIOutQ3InputSelectEntry		=	{kK2RegXptSelectGroup20,	kK2RegMaskHDMIOutV2Q3InputSelect,			kK2RegShiftHDMIOutV2Q3InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry HDMIOutQ4InputSelectEntry		=	{kK2RegXptSelectGroup20,	kK2RegMaskHDMIOutV2Q4InputSelect,			kK2RegShiftHDMIOutV2Q4InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Xpt4KDCQ1InputSelectEntry		=	{kK2RegXptSelectGroup19,	kK2RegMask4KDCQ1InputSelect,				kK2RegShift4KDCQ1InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Xpt4KDCQ2InputSelectEntry		=	{kK2RegXptSelectGroup19,	kK2RegMask4KDCQ2InputSelect,				kK2RegShift4KDCQ2InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Xpt4KDCQ3InputSelectEntry		=	{kK2RegXptSelectGroup19,	kK2RegMask4KDCQ3InputSelect,				kK2RegShift4KDCQ3InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Xpt4KDCQ4InputSelectEntry		=	{kK2RegXptSelectGroup19,	kK2RegMask4KDCQ4InputSelect,				kK2RegShift4KDCQ4InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Xpt425Mux1AInputSelectEntry		=	{kRegXptSelectGroup32,		kK2RegMask425Mux1AInputSelect,				kK2RegShift425Mux1AInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Xpt425Mux1BInputSelectEntry		=	{kRegXptSelectGroup32,		kK2RegMask425Mux1BInputSelect,				kK2RegShift425Mux1BInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Xpt425Mux2AInputSelectEntry		=	{kRegXptSelectGroup32,		kK2RegMask425Mux2AInputSelect,				kK2RegShift425Mux2AInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Xpt425Mux2BInputSelectEntry		=	{kRegXptSelectGroup32,		kK2RegMask425Mux2BInputSelect,				kK2RegShift425Mux2BInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Xpt425Mux3AInputSelectEntry		=	{kRegXptSelectGroup33,		kK2RegMask425Mux3AInputSelect,				kK2RegShift425Mux3AInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Xpt425Mux3BInputSelectEntry		=	{kRegXptSelectGroup33,		kK2RegMask425Mux3BInputSelect,				kK2RegShift425Mux3BInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Xpt425Mux4AInputSelectEntry		=	{kRegXptSelectGroup33,		kK2RegMask425Mux4AInputSelect,				kK2RegShift425Mux4AInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry Xpt425Mux4BInputSelectEntry		=	{kRegXptSelectGroup33,		kK2RegMask425Mux4BInputSelect,				kK2RegShift425Mux4BInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CompressionModInputSelectEntry	=	{kK2RegXptSelectGroup1,		kK2RegMaskCompressionModInputSelect,		kK2RegShiftCompressionModInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry ConversionModInputSelectEntry	=	{kK2RegXptSelectGroup1,		kK2RegMaskConversionModInputSelect,			kK2RegShiftConversionModInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry ConversionMod2InputSelectEntry	=	{kK2RegXptSelectGroup6,		kK2RegMaskSecondConverterInputSelect,		kK2RegShiftSecondConverterInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry CSC1KeyFromInput2SelectEntry		=	{kK2RegCSCoefficients1_2,	kK2RegMaskMakeAlphaFromKeySelect,			kK2RegShiftMakeAlphaFromKeySelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameSync2InputSelectEntry		=	{kK2RegXptSelectGroup2,		kK2RegMaskFrameSync2InputSelect,			kK2RegShiftFrameSync2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry FrameSync1InputSelectEntry		=	{kK2RegXptSelectGroup2,		kK2RegMaskFrameSync1InputSelect,			kK2RegShiftFrameSync1InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry AnalogOutInputSelectEntry		=	{kK2RegXptSelectGroup3,		kK2RegMaskAnalogOutInputSelect,				kK2RegShiftAnalogOutInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry AnalogOutCompositeOutSelectEntry	=	{kRegFS1ReferenceSelect,	kFS1RegMaskSecondAnalogOutInputSelect,		kFS1RegShiftSecondAnalogOutInputSelect,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry ProAmpInputSelectEntry			=	{kRegFS1ReferenceSelect,	kFS1RegMaskProcAmpInputSelect,				kFS1RegShiftProcAmpInputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry IICT1InputSelectEntry			=	{kK2RegXptSelectGroup6,		kK2RegMaskIICTInputSelect,					kK2RegShiftIICTInputSelect,					0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry IICT2InputSelectEntry			=	{kK2RegXptSelectGroup7,		kK2RegMaskIICT2InputSelect,					kK2RegShiftIICT2InputSelect,				0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry WaterMarker1InputSelectEntry		=	{kK2RegXptSelectGroup6,		kK2RegMaskWaterMarkerInputSelect,			kK2RegShiftWaterMarkerInputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry WaterMarker2InputSelectEntry		=	{kK2RegXptSelectGroup7,		kK2RegMaskWaterMarker2InputSelect,			kK2RegShiftWaterMarker2InputSelect,			0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry StereoLeftInputSelectEntry		=	{kRegStereoCompressor,		kRegMaskStereoCompressorLeftSource,			kRegShiftStereoCompressorLeftSource,		0};
AJA_LOCAL_STATIC	const NTV2RoutingEntry StereoRightInputSelectEntry		=	{kRegStereoCompressor,		kRegMaskStereoCompressorRightSource,		kRegShiftStereoCompressorRightSource,		0};

//	Make this one a read-only register in case of accident...
AJA_LOCAL_STATIC	const NTV2RoutingEntry UpdateRegisterSelectEntry		=	{kRegStatus,				0xFFFFFFFF,									0,											0};


const NTV2RoutingEntry &	GetFrameBuffer1InputSelectEntry		(void)	{return FrameBuffer1InputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer1BInputSelectEntry	(void)	{return FrameBuffer1BInputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer2InputSelectEntry		(void)	{return FrameBuffer2InputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer2BInputSelectEntry	(void)	{return FrameBuffer2BInputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer3InputSelectEntry		(void)	{return FrameBuffer3InputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer3BInputSelectEntry	(void)	{return FrameBuffer3BInputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer4InputSelectEntry		(void)	{return FrameBuffer4InputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer4BInputSelectEntry	(void)	{return FrameBuffer4BInputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer5InputSelectEntry		(void)	{return FrameBuffer5InputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer5BInputSelectEntry	(void)	{return FrameBuffer5BInputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer6InputSelectEntry		(void)	{return FrameBuffer6InputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer6BInputSelectEntry	(void)	{return FrameBuffer6BInputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer7InputSelectEntry		(void)	{return FrameBuffer7InputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer7BInputSelectEntry	(void)	{return FrameBuffer7BInputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer8InputSelectEntry		(void)	{return FrameBuffer8InputSelectEntry;}
const NTV2RoutingEntry &	GetFrameBuffer8BInputSelectEntry	(void)	{return FrameBuffer8BInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC1VidInputSelectEntry			(void)	{return CSC1VidInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC1KeyInputSelectEntry			(void)	{return CSC1KeyInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC2VidInputSelectEntry			(void)	{return CSC2VidInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC2KeyInputSelectEntry			(void)	{return CSC2KeyInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC3VidInputSelectEntry			(void)	{return CSC3VidInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC3KeyInputSelectEntry			(void)	{return CSC3KeyInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC4VidInputSelectEntry			(void)	{return CSC4VidInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC4KeyInputSelectEntry			(void)	{return CSC4KeyInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC5VidInputSelectEntry			(void)	{return CSC5VidInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC5KeyInputSelectEntry			(void)	{return CSC5KeyInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC6VidInputSelectEntry			(void)	{return CSC6VidInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC6KeyInputSelectEntry			(void)	{return CSC6KeyInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC7VidInputSelectEntry			(void)	{return CSC7VidInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC7KeyInputSelectEntry			(void)	{return CSC7KeyInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC8VidInputSelectEntry			(void)	{return CSC8VidInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC8KeyInputSelectEntry			(void)	{return CSC8KeyInputSelectEntry;}
const NTV2RoutingEntry &	GetXptLUT1InputSelectEntry			(void)	{return XptLUTInputSelectEntry;}
const NTV2RoutingEntry &	GetXptLUT2InputSelectEntry			(void)	{return XptLUT2InputSelectEntry;}
const NTV2RoutingEntry &	GetXptLUT3InputSelectEntry			(void)	{return XptLUT3InputSelectEntry;}
const NTV2RoutingEntry &	GetXptLUT4InputSelectEntry			(void)	{return XptLUT4InputSelectEntry;}
const NTV2RoutingEntry &	GetXptLUT5InputSelectEntry			(void)	{return XptLUT5InputSelectEntry;}
const NTV2RoutingEntry &	GetXptLUT6InputSelectEntry			(void)	{return XptLUT6InputSelectEntry;}
const NTV2RoutingEntry &	GetXptLUT7InputSelectEntry			(void)	{return XptLUT7InputSelectEntry;}
const NTV2RoutingEntry &	GetXptLUT8InputSelectEntry			(void)	{return XptLUT8InputSelectEntry;}
const NTV2RoutingEntry &	GetSDIOut1StandardSelectEntry		(void)	{return SDIOut1StandardSelectEntry;}
const NTV2RoutingEntry &	GetSDIOut2StandardSelectEntry		(void)	{return SDIOut2StandardSelectEntry;}
const NTV2RoutingEntry &	GetSDIOut3StandardSelectEntry		(void)	{return SDIOut3StandardSelectEntry;}
const NTV2RoutingEntry &	GetSDIOut4StandardSelectEntry		(void)	{return SDIOut4StandardSelectEntry;}
const NTV2RoutingEntry &	GetSDIOut1InputSelectEntry			(void)	{return SDIOut1InputSelectEntry;}
const NTV2RoutingEntry &	GetSDIOut1InputDS2SelectEntry		(void)	{return SDIOut1InputDS2SelectEntry;}
const NTV2RoutingEntry &	GetSDIOut2InputSelectEntry			(void)	{return SDIOut2InputSelectEntry;}
const NTV2RoutingEntry &	GetSDIOut2InputDS2SelectEntry		(void)	{return SDIOut2InputDS2SelectEntry;}
const NTV2RoutingEntry &	GetSDIOut3InputSelectEntry			(void)	{return SDIOut3InputSelectEntry;}
const NTV2RoutingEntry &	GetSDIOut3InputDS2SelectEntry		(void)	{return SDIOut3InputDS2SelectEntry;}
const NTV2RoutingEntry &	GetSDIOut4InputSelectEntry			(void)	{return SDIOut4InputSelectEntry;}
const NTV2RoutingEntry &	GetSDIOut4InputDS2SelectEntry		(void)	{return SDIOut4InputDS2SelectEntry;}
const NTV2RoutingEntry &	GetSDIOut5InputSelectEntry			(void)	{return SDIOut5InputSelectEntry;}
const NTV2RoutingEntry &	GetSDIOut5InputDS2SelectEntry		(void)	{return SDIOut5InputDS2SelectEntry;}
const NTV2RoutingEntry &	GetSDIOut6InputSelectEntry			(void)	{return SDIOut6InputSelectEntry;}
const NTV2RoutingEntry &	GetSDIOut6InputDS2SelectEntry		(void)	{return SDIOut6InputDS2SelectEntry;}
const NTV2RoutingEntry &	GetSDIOut7InputSelectEntry			(void)	{return SDIOut7InputSelectEntry;}
const NTV2RoutingEntry &	GetSDIOut7InputDS2SelectEntry		(void)	{return SDIOut7InputDS2SelectEntry;}
const NTV2RoutingEntry &	GetSDIOut8InputSelectEntry			(void)	{return SDIOut8InputSelectEntry;}
const NTV2RoutingEntry &	GetSDIOut8InputDS2SelectEntry		(void)	{return SDIOut8InputDS2SelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn1InputSelectEntry		(void)	{return DualLinkIn1InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn1DSInputSelectEntry	(void)	{return DualLinkIn1DSInputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn2InputSelectEntry		(void)	{return DualLinkIn2InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn2DSInputSelectEntry	(void)	{return DualLinkIn2DSInputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn3InputSelectEntry		(void)	{return DualLinkIn3InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn3DSInputSelectEntry	(void)	{return DualLinkIn3DSInputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn4InputSelectEntry		(void)	{return DualLinkIn4InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn4DSInputSelectEntry	(void)	{return DualLinkIn4DSInputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn5InputSelectEntry		(void)	{return DualLinkIn5InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn5DSInputSelectEntry	(void)	{return DualLinkIn5DSInputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn6InputSelectEntry		(void)	{return DualLinkIn6InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn6DSInputSelectEntry	(void)	{return DualLinkIn6DSInputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn7InputSelectEntry		(void)	{return DualLinkIn7InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn7DSInputSelectEntry	(void)	{return DualLinkIn7DSInputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn8InputSelectEntry		(void)	{return DualLinkIn8InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkIn8DSInputSelectEntry	(void)	{return DualLinkIn8DSInputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkOut1InputSelectEntry		(void)	{return DualLinkOut1InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkOut2InputSelectEntry		(void)	{return DualLinkOut2InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkOut3InputSelectEntry		(void)	{return DualLinkOut3InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkOut4InputSelectEntry		(void)	{return DualLinkOut4InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkOut5InputSelectEntry		(void)	{return DualLinkOut5InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkOut6InputSelectEntry		(void)	{return DualLinkOut6InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkOut7InputSelectEntry		(void)	{return DualLinkOut7InputSelectEntry;}
const NTV2RoutingEntry &	GetDualLinkOut8InputSelectEntry		(void)	{return DualLinkOut8InputSelectEntry;}
const NTV2RoutingEntry &	GetMixer1BGKeyInputSelectEntry		(void)	{return Mixer1BGKeyInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer1BGVidInputSelectEntry		(void)	{return Mixer1BGVidInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer1FGKeyInputSelectEntry		(void)	{return Mixer1FGKeyInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer1FGVidInputSelectEntry		(void)	{return Mixer1FGVidInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer2BGKeyInputSelectEntry		(void)	{return Mixer2BGKeyInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer2BGVidInputSelectEntry		(void)	{return Mixer2BGVidInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer2FGKeyInputSelectEntry		(void)	{return Mixer2FGKeyInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer2FGVidInputSelectEntry		(void)	{return Mixer2FGVidInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer3BGKeyInputSelectEntry		(void)	{return Mixer3BGKeyInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer3BGVidInputSelectEntry		(void)	{return Mixer3BGVidInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer3FGKeyInputSelectEntry		(void)	{return Mixer3FGKeyInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer3FGVidInputSelectEntry		(void)	{return Mixer3FGVidInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer4BGKeyInputSelectEntry		(void)	{return Mixer4BGKeyInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer4BGVidInputSelectEntry		(void)	{return Mixer4BGVidInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer4FGKeyInputSelectEntry		(void)	{return Mixer4FGKeyInputSelectEntry;}
const NTV2RoutingEntry &	GetMixer4FGVidInputSelectEntry		(void)	{return Mixer4FGVidInputSelectEntry;}
const NTV2RoutingEntry &	GetXpt4KDCQ1InputSelectEntry		(void)	{return Xpt4KDCQ1InputSelectEntry;}
const NTV2RoutingEntry &	GetXpt4KDCQ2InputSelectEntry		(void)	{return Xpt4KDCQ2InputSelectEntry;}
const NTV2RoutingEntry &	GetXpt4KDCQ3InputSelectEntry		(void)	{return Xpt4KDCQ3InputSelectEntry;}
const NTV2RoutingEntry &	GetXpt4KDCQ4InputSelectEntry		(void)	{return Xpt4KDCQ4InputSelectEntry;}
const NTV2RoutingEntry &	GetHDMIOutInputSelectEntry			(void)	{return HDMIOutInputSelectEntry;}
const NTV2RoutingEntry &	GetHDMIOutQ1InputSelectEntry		(void)	{return HDMIOutQ1InputSelectEntry;}
const NTV2RoutingEntry &	GetHDMIOutQ2InputSelectEntry		(void)	{return HDMIOutQ2InputSelectEntry;}
const NTV2RoutingEntry &	GetHDMIOutQ3InputSelectEntry		(void)	{return HDMIOutQ3InputSelectEntry;}
const NTV2RoutingEntry &	GetHDMIOutQ4InputSelectEntry		(void)	{return HDMIOutQ4InputSelectEntry;}

const NTV2RoutingEntry &	Get425Mux1AInputSelectEntry			(void)	{return Xpt425Mux1AInputSelectEntry;}
const NTV2RoutingEntry &	Get425Mux1BInputSelectEntry			(void)	{return Xpt425Mux1BInputSelectEntry;}
const NTV2RoutingEntry &	Get425Mux2AInputSelectEntry			(void)	{return Xpt425Mux2AInputSelectEntry;}
const NTV2RoutingEntry &	Get425Mux2BInputSelectEntry			(void)	{return Xpt425Mux2BInputSelectEntry;}
const NTV2RoutingEntry &	Get425Mux3AInputSelectEntry			(void)	{return Xpt425Mux3AInputSelectEntry;}
const NTV2RoutingEntry &	Get425Mux3BInputSelectEntry			(void)	{return Xpt425Mux3BInputSelectEntry;}
const NTV2RoutingEntry &	Get425Mux4AInputSelectEntry			(void)	{return Xpt425Mux4AInputSelectEntry;}
const NTV2RoutingEntry &	Get425Mux4BInputSelectEntry			(void)	{return Xpt425Mux4BInputSelectEntry;}

const NTV2RoutingEntry &	GetCompressionModInputSelectEntry	(void)	{return CompressionModInputSelectEntry;}
const NTV2RoutingEntry &	GetConversionModInputSelectEntry	(void)	{return ConversionModInputSelectEntry;}
const NTV2RoutingEntry &	GetCSC1KeyFromInput2SelectEntry		(void)	{return CSC1KeyFromInput2SelectEntry;}
const NTV2RoutingEntry &	GetFrameSync2InputSelectEntry		(void)	{return FrameSync2InputSelectEntry;}
const NTV2RoutingEntry &	GetFrameSync1InputSelectEntry		(void)	{return FrameSync1InputSelectEntry;}
const NTV2RoutingEntry &	GetAnalogOutInputSelectEntry		(void)	{return AnalogOutInputSelectEntry;}
const NTV2RoutingEntry &	GetProAmpInputSelectEntry			(void)	{return ProAmpInputSelectEntry;}
const NTV2RoutingEntry &	GetIICT1InputSelectEntry			(void)	{return IICT1InputSelectEntry;}
const NTV2RoutingEntry &	GetWaterMarker1InputSelectEntry		(void)	{return WaterMarker1InputSelectEntry;}
const NTV2RoutingEntry &	GetWaterMarker2InputSelectEntry		(void)	{return WaterMarker2InputSelectEntry;}
const NTV2RoutingEntry &	GetUpdateRegisterSelectEntry		(void)	{return UpdateRegisterSelectEntry;}
const NTV2RoutingEntry &	GetConversionMod2InputSelectEntry	(void)	{return ConversionMod2InputSelectEntry;}
const NTV2RoutingEntry &	GetIICT2InputSelectEntry			(void)	{return IICT2InputSelectEntry;}
const NTV2RoutingEntry &	GetAnalogOutCompositeOutSelectEntry	(void)	{return AnalogOutCompositeOutSelectEntry;}
const NTV2RoutingEntry &	GetStereoLeftInputSelectEntry		(void)	{return StereoLeftInputSelectEntry;}
const NTV2RoutingEntry &	GetStereoRightInputSelectEntry		(void)	{return StereoRightInputSelectEntry;}
