/////////////////////////////////////////////////////////////////////////////
// NTV2ColorCorrection.h
//
// Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
/////////////////////////////////////////////////////////////////////////////
#ifndef NTV2ColorCorrection_H
#define NTV2ColorCorrection_H

#include "ajaexport.h"
#include "ntv2status.h"
#include <fstream>
#include <vector>


class AJAExport CNTV2ColorCorrection : public CNTV2Status
{
public:  // Constructors
    CNTV2ColorCorrection();
	CNTV2ColorCorrection(UWord boardNumber, bool displayErrorMessage = false, 
		UWord ulBoardType = DEVICETYPE_NTV2);
	virtual ~CNTV2ColorCorrection();

#ifdef AJAMac
	bool SetupColorCorrectionPointers(bool ajamac = true);
#else
	bool SetupColorCorrectionPointers(bool ajamac = false);
#endif

    typedef enum {
        RED,
        GREEN,
        BLUE,
        NUM_COLORS
    } ColorCorrectionColor ;

public:  // Methods
	#if !defined (NTV2_DEPRECATE)
		virtual bool SetBoard (UWord boardNumber);
	#endif	//	!defined (NTV2_DEPRECATE)
	void SetChannel(NTV2Channel channel) { _channel = channel;}
    void SetColorCorrectionEnable(bool enable);
	void PingPongColorCorrectionTable();
    void SetColorCorrectionValues(ColorCorrectionColor color,double gamma, double gain, double offset);
    void SetColorCorrectionGamma(ColorCorrectionColor colorChoice,double gamma);
    void SetColorCorrectionGain(ColorCorrectionColor colorChoice,double gain);
    void SetColorCorrectionOffset(ColorCorrectionColor colorChoice,double offset);

    
	ULWord* GetHWTableBaseAddress(ColorCorrectionColor colorChoice);
    UWord* GetTableBaseAddress(ColorCorrectionColor colorChoice);
    void BuildTables();
    void TransferTablesToHardware();

	// TransferTablesToBuffer
	// ccBuffer needs to be 512*4*3 bytes long.
	// This is suitable to pass to transferutocirculate
	void TransferTablesToBuffer(ULWord* ccBuffer);

	// Copy external LUTs (each double LUT[1024]) to/from internal buffers
	void SetTables(double *redLUT, double *greenLUT, double *blueLUT);
	void GetTables(double *redLUT, double *greenLUT, double *blueLUT);

	// Copy external LUTs (each double LUT[1024]) direct to/from hardware
	void SetTablesToHardware  (double *redLUT, double *greenLUT, double *blueLUT);
	void GetTablesFromHardware(double *redLUT, double *greenLUT, double *blueLUT);

protected:  // Methods


protected:  // Data

    // On Board Base Address
    ULWord* _pHWTableBaseAddress[CNTV2ColorCorrection::NUM_COLORS];

    // Tables for holding calculations.
    UWord* _pColorCorrectionTable[CNTV2ColorCorrection::NUM_COLORS];
 
    double _Gamma[CNTV2ColorCorrection::NUM_COLORS];
    double _Gain[CNTV2ColorCorrection::NUM_COLORS];
    double _Offset[CNTV2ColorCorrection::NUM_COLORS];

	NTV2Channel _channel;
};


#endif
