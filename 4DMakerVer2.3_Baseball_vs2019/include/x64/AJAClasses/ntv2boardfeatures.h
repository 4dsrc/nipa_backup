/**
	@file		ntv2boardfeatures.h
	@deprecated	Please include ntv2devicefeatures.h instead.
	@copyright	(C) 2004-2014 AJA Video Systems, Inc.	Proprietary and confidential information.
**/

#ifndef NTV2BOARDFEATURES_H
#define NTV2BOARDFEATURES_H

#include "ntv2devicefeatures.h"

#endif	//	NTV2BOARDFEATURES_H
