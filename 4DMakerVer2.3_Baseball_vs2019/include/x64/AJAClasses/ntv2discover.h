//////////////////////////////////////////////////////////////////////////////////////////
// 
// Copyright (C) 2006, 2010 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
// ntv2discover.h - hunt for ntv2nubs on the local network.
//
//////////////////////////////////////////////////////////////////////////////////////////

#ifndef NTV2DISCOVER_H
#define NTV2DISCOVER_H
#if defined(AJALinux ) || defined(AJAMac)
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include "ajaexport.h"
#include "ntv2nubtypes.h"

#define DISCOVER_SUCCESS		(0)
#define DISCOVER_RECVERR		(-1)
#define DISCOVER_FULL	 		(-2)
#define DISCOVER_FOUND_NOBODY	(-3)

// Discover nubs on all interfaces
AJAExport int
ntv2DiscoverNubs(	UWord boardMask,
					int maxNubs, // Maximum number of nubs (size of sockaddr_in and boardInventory arrays)
					struct sockaddr_in their_addr[],			// Array of responders
					NTV2DiscoverRespPayload boardInventory[],	// Available boards for each responder
					int &nubsFound,
					int timeout,	// How long to wait for discover responses In seconds
					int sendto_count = 5); // default

AJAExport int
ntv2DiscoverNubs(	char *hostname,
					UWord boardMask,
					int maxNubs, 	// Maximum number of nubs (size of sockaddr_in and boardInventory arrays)
					struct sockaddr_in their_addr[],			// Array of responders
					NTV2DiscoverRespPayload boardInventory[],	// Available boards for each responder
					int &nubsFound,	// How many nubs were found (no more than maxNubs above)
					int timeout,		// How long to wait for discover responses In seconds
					int sendto_count = 5,	// How many UDP queries to send (can be lost)
					bool appendNubs = false);	// If true, leave existing nubs in boardInventory and add new ones

#endif
