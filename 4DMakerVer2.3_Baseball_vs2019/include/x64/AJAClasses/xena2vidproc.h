/////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
/////////////////////////////////////////////////////////////////////////////
#ifndef XENA2VIDPROC_H
#define XENA2VIDPROC_H

#include "ajaexport.h"
#include "ntv2testpattern.h"

class AJAExport CXena2VidProc : public CNTV2TestPattern
{
public:  // Constructor - Deconstructor
        CXena2VidProc() {};
	CXena2VidProc(UWord inDeviceIndex, bool displayErrorMessage = false, 
		UWord ulBoardType = DEVICETYPE_NTV2);

	virtual ~CXena2VidProc();

public: // Methods


/// Use the follow base class memebers to set inputs to xena2 video processing widget
//	bool SetK2Xpt4MixerBGKeyInputSelect(NTV2K2CrosspointSelections value);
//	bool GetK2Xpt4MixerBGKeyInputSelect(NTV2K2CrosspointSelections* value);
//	bool SetK2Xpt4MixerBGVidInputSelect(NTV2K2CrosspointSelections value);
//	bool GetK2Xpt4MixerBGVidInputSelect(NTV2K2CrosspointSelections* value);
//	bool SetK2Xpt4MixerFGKeyInputSelect(NTV2K2CrosspointSelections value);
//	bool GetK2Xpt4MixerFGKeyInputSelect(NTV2K2CrosspointSelections* value);
//	bool SetK2Xpt4MixerFGVidInputSelect(NTV2K2CrosspointSelections value);
//	bool GetK2Xpt4MixerFGVidInputSelect(NTV2K2CrosspointSelections* value);
	
	// This tells the video processing whether to
	// ignore key 
	// use key and assumed it has already been shaped
	// or not
	bool SetXena2VidProcInputControl(NTV2Channel channel,Xena2VidProcInputControl inputControl);
	bool GetXena2VidProcInputControl(NTV2Channel channel,Xena2VidProcInputControl* inputControl);
	bool SetXena2VidProc2InputControl(NTV2Channel channel,Xena2VidProcInputControl inputControl);
	bool GetXena2VidProc2InputControl(NTV2Channel channel,Xena2VidProcInputControl* inputControl);
	bool SetXena2VidProc3InputControl(NTV2Channel channel,Xena2VidProcInputControl inputControl);
	bool GetXena2VidProc3InputControl(NTV2Channel channel,Xena2VidProcInputControl* inputControl);
	bool SetXena2VidProc4InputControl(NTV2Channel channel,Xena2VidProcInputControl inputControl);
	bool GetXena2VidProc4InputControl(NTV2Channel channel,Xena2VidProcInputControl* inputControl);

	bool SetXena2VidProcMode(Xena2VidProcMode mode);
	bool GetXena2VidProcMode(Xena2VidProcMode* mode);
	bool SetXena2VidProc2Mode(Xena2VidProcMode mode);
	bool GetXena2VidProc2Mode(Xena2VidProcMode* mode);
	bool SetXena2VidProc3Mode(Xena2VidProcMode mode);
	bool GetXena2VidProc3Mode(Xena2VidProcMode* mode);
	bool SetXena2VidProc4Mode(Xena2VidProcMode mode);
	bool GetXena2VidProc4Mode(Xena2VidProcMode* mode);

	void SetSplitMode(NTV2SplitMode splitMode);
	NTV2SplitMode GetSplitMode();
	void SetSplitParameters(Fixed_ position, Fixed_ softness);
	void SetSlitParameters(Fixed_ start, Fixed_ width);

	void SetMixCoefficient(Fixed_ coefficient);
	Fixed_ GetMixCoefficient();
	void SetMix2Coefficient(Fixed_ coefficient);
	Fixed_ GetMix2Coefficient();
	void SetMix3Coefficient(Fixed_ coefficient);
	Fixed_ GetMix3Coefficient();
	void SetMix4Coefficient(Fixed_ coefficient);
	Fixed_ GetMix4Coefficient();


protected:  // Methods

protected:  // Data

};


#endif
