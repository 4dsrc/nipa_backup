/////////////////////////////////////////////////////////////////////////////

// ntv2AutoCirculate.cpp

// 

// Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.

//

//

// Non-OS specific auto circulate methods

/////////////////////////////////////////////////////////////////////////////

#include "ntv2card.h"

static const NTV2Channel gCrosspointToChannel [] = {	/* NTV2CROSSPOINT_CHANNEL1	==>	*/	NTV2_CHANNEL1,
														/* NTV2CROSSPOINT_CHANNEL2	==>	*/	NTV2_CHANNEL2,
														/* NTV2CROSSPOINT_INPUT1	==>	*/	NTV2_CHANNEL1,
														/* NTV2CROSSPOINT_INPUT2	==>	*/	NTV2_CHANNEL2,
														/* NTV2CROSSPOINT_MATTE		==>	*/	NTV2_MAX_NUM_CHANNELS,
														/* NTV2CROSSPOINT_FGKEY		==>	*/	NTV2_MAX_NUM_CHANNELS,
														/* NTV2CROSSPOINT_CHANNEL3	==>	*/	NTV2_CHANNEL3,
														/* NTV2CROSSPOINT_CHANNEL4	==>	*/	NTV2_CHANNEL4,
														/* NTV2CROSSPOINT_INPUT3	==>	*/	NTV2_CHANNEL3,
														/* NTV2CROSSPOINT_INPUT4	==>	*/	NTV2_CHANNEL4,
														/* NTV2CROSSPOINT_CHANNEL5	==>	*/	NTV2_CHANNEL5,
														/* NTV2CROSSPOINT_CHANNEL6	==>	*/	NTV2_CHANNEL6,
														/* NTV2CROSSPOINT_CHANNEL7	==>	*/	NTV2_CHANNEL7,
														/* NTV2CROSSPOINT_CHANNEL8	==>	*/	NTV2_CHANNEL8,
														/* NTV2CROSSPOINT_INPUT5	==>	*/	NTV2_CHANNEL5,
														/* NTV2CROSSPOINT_INPUT6	==>	*/	NTV2_CHANNEL6,
														/* NTV2CROSSPOINT_INPUT7	==>	*/	NTV2_CHANNEL7,
														/* NTV2CROSSPOINT_INPUT8	==>	*/	NTV2_CHANNEL8,
																							NTV2_MAX_NUM_CHANNELS};
static const bool		gIsCrosspointInput [] = {		/* NTV2CROSSPOINT_CHANNEL1	==> */	false,
														/* NTV2CROSSPOINT_CHANNEL2	==>	*/	false,
														/* NTV2CROSSPOINT_INPUT1	==>	*/	true,
														/* NTV2CROSSPOINT_INPUT2	==>	*/	true,
														/* NTV2CROSSPOINT_MATTE		==>	*/	false,
														/* NTV2CROSSPOINT_FGKEY		==>	*/	false,
														/* NTV2CROSSPOINT_CHANNEL3	==>	*/	false,
														/* NTV2CROSSPOINT_CHANNEL4	==>	*/	false,
														/* NTV2CROSSPOINT_INPUT3	==>	*/	true,
														/* NTV2CROSSPOINT_INPUT4	==>	*/	true,
														/* NTV2CROSSPOINT_CHANNEL5	==>	*/	false,
														/* NTV2CROSSPOINT_CHANNEL6	==>	*/	false,
														/* NTV2CROSSPOINT_CHANNEL7	==>	*/	false,
														/* NTV2CROSSPOINT_CHANNEL8	==>	*/	false,
														/* NTV2CROSSPOINT_INPUT5	==>	*/	true,
														/* NTV2CROSSPOINT_INPUT6	==>	*/	true,
														/* NTV2CROSSPOINT_INPUT7	==>	*/	true,
														/* NTV2CROSSPOINT_INPUT8	==>	*/	true,
																							false};

/** \brief InitAutoCirculate configures the API, driver and hardware
 *  for subsequent AutoCirculate operations.
 *
 *  Detailed description of InitiAutoCirculate goes here...
 *
 *  @param channelSpec specifies which input/output frames to cycle through.
 *  @param startFrameNumber starting frame for the autocirculate operation.
 *  @param endFrameNumber ending frame for the autocirculate operation.
 *  @param bWithAudio tells the driver whether or not to handle audio.
 *  @param bWithRP188 tells the driver whether or not to handle RP188, an SMPTE standard for embedded timecode.
 *  @param bFbfChange controls whether the frame buffer format should be dynamically changed by the driver
 *         during playback the autocirculate operation.
 *  @param bFboChange tells the driver whether or not to handle FrameBuffer Orientation changes.
 *         This controls the �Flip Vertical� bit in Channel 1 (or Channel 2) Control Register.
 *  @param bWithColorCorrection tells the driver whether or not to handle Color-Correction parameter changes.
 *  @param bWithVidProc tells the driver whether or not to handle Video Processing parameter changes.
 *  @param bWithCustomAncData tells the driver ... TBD
 *
 *	NTV2CROSSPOINT_CHANNEL1 - Driver Cycles through Ch1OutputFrame from StartFrameNumber to EndFrameNumber continously
 *	NTV2CROSSPOINT_CHANNEL2 - Driver Cycles through Ch2OutputFrame from StartFrameNumber to EndFrameNumber continously
 *	NTV2CROSSPOINT_CHANNEL3 - Driver Cycles through Ch3OutputFrame from StartFrameNumber to EndFrameNumber continously
 *	NTV2CROSSPOINT_CHANNEL4 - Driver Cycles through Ch4OutputFrame from StartFrameNumber to EndFrameNumber continously
 *	NTV2CROSSPOINT_INPUT1   - Driver Cycles through Ch1InputFrame from StartFrameNumber to EndFrameNumber continously
 *	NTV2CROSSPOINT_INPUT2   - Driver Cycles through Ch2InputFrame from StartFrameNumber to EndFrameNumber continously
 *	NTV2CROSSPOINT_INPUT3   - Driver Cycles through Ch3InputFrame from StartFrameNumber to EndFrameNumber continously
 *	NTV2CROSSPOINT_INPUT4   - Driver Cycles through Ch4InputFrame from StartFrameNumber to EndFrameNumber continously
 *  ... until StopAutoCirculate on this ChannelSpec
 *  so _CHANNEL1, CHANNEL2, _CHANNEL3 and CHANNEL4 are used for playing 
 *	and INPUT1, INPUT2, INPUT3 and INPUT4 are used for recording.
 *
 *  if bWithAudio is true, then it will start audio playback when first frame is played out. only useful if 
 *  channelSpec is NTV2CROSSPOINT_CHANNEL1, NTV2CROSSPOINT_CHANNEL2, NTV2CROSSPOINT_CHANNEL3 or NTV2CROSSPOINT_CHANNEL4
 *
 * Note, no error checking as to whether this channelspec already in use, as well
 * as whether startFrameNumber < endFrameNumber etc.
 * Also, there is no provision to automatically stop autocirculating when ntv2card class is
 * destructed....so the driver will just continually autocirclate. You can use GetAutoCirculate
 * to determine if a channelSpec is currently autocirculating.
 *
 */
bool   CNTV2Card::InitAutoCirculate(NTV2Crosspoint channelSpec,
                                    LWord startFrameNumber, 
                                    LWord endFrameNumber,
                                    bool bWithAudio,
                                    bool bWithRP188,
                                    bool bFbfChange,
                                    bool bFboChange,
									bool bWithColorCorrection ,
									bool bWithVidProc,
	                                bool bWithCustomAncData,
	                                bool bWithLTC,
									bool bWithAudio2)
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );
 
	// If ending frame number has been defaulted, calculate it
	if (endFrameNumber < 0) {	
		endFrameNumber = 10;	// All boards have at least 10 frames	
	}

	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = eInitAutoCirc;
	autoCircData.channelSpec = channelSpec;
	autoCircData.lVal1		 = startFrameNumber;
	autoCircData.lVal2		 = endFrameNumber;
	autoCircData.lVal3		 = bWithAudio2?1:0;
	autoCircData.lVal4		 = 1;
	autoCircData.bVal1		 = bWithAudio;
	autoCircData.bVal2		 = bWithRP188;
    autoCircData.bVal3       = bFbfChange;
    autoCircData.bVal4       = bFboChange;
    autoCircData.bVal5       = bWithColorCorrection;
    autoCircData.bVal6       = bWithVidProc;
    autoCircData.bVal7       = bWithCustomAncData;
    autoCircData.bVal8       = bWithLTC;

	// Call the OS specific method
	bool bRet = AutoCirculate (autoCircData);

    return bRet;
}


bool   CNTV2Card::InitAutoCirculate(NTV2Crosspoint channelSpec,
                                    LWord startFrameNumber, 
                                    LWord endFrameNumber,
									LWord channelCount,
									NTV2AudioSystem audioSystem,
                                    bool bWithAudio,
                                    bool bWithRP188,
                                    bool bFbfChange,
                                    bool bFboChange,
									bool bWithColorCorrection ,
									bool bWithVidProc,
	                                bool bWithCustomAncData,
	                                bool bWithLTC)
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );
 
	// If ending frame number has been defaulted, calculate it
	if (endFrameNumber < 0) {	
		endFrameNumber = 10;	// All boards have at least 10 frames	
	}

	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = eInitAutoCirc;
	autoCircData.channelSpec = channelSpec;
	autoCircData.lVal1		 = startFrameNumber;
	autoCircData.lVal2		 = endFrameNumber;
	autoCircData.lVal3		 = audioSystem;
	autoCircData.lVal4		 = channelCount;
	autoCircData.bVal1		 = bWithAudio;
	autoCircData.bVal2		 = bWithRP188;
    autoCircData.bVal3       = bFbfChange;
    autoCircData.bVal4       = bFboChange;
    autoCircData.bVal5       = bWithColorCorrection;
    autoCircData.bVal6       = bWithVidProc;
    autoCircData.bVal7       = bWithCustomAncData;
    autoCircData.bVal8       = bWithLTC;

	// Call the OS specific method
	bool bRet = AutoCirculate (autoCircData);

    return bRet;
}

bool   CNTV2Card::StartAutoCirculate (NTV2Crosspoint channelSpec, const ULWord64 inStartTime)
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

 	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	::memset (&autoCircData, 0, sizeof (AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = inStartTime ? eStartAutoCircAtTime : eStartAutoCirc;
	autoCircData.channelSpec = channelSpec;
	autoCircData.lVal1		 = static_cast <LWord> (inStartTime >> 32);
	autoCircData.lVal2		 = static_cast <LWord> (inStartTime & 0xFFFFFFFF);

	// Call the OS specific method
	return AutoCirculate (autoCircData);
}


// StopAutoCirculate(NTV2Crosspoint channelSpec)
// Stops associated AutoCirculate based on channelSpec.
bool   CNTV2Card::StopAutoCirculate(NTV2Crosspoint channelSpec)
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

  	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = eStopAutoCirc;
	autoCircData.channelSpec = channelSpec;

	// Call the OS specific method
	bool bRet =  AutoCirculate (autoCircData);

    // Wait to insure driver changes state to DISABLED
    if (bRet)
    {
		const NTV2Channel	channel	(gCrosspointToChannel [channelSpec]);
		bRet = gIsCrosspointInput [channelSpec] ? WaitForInputFieldID (NTV2_FIELD0, channel) : WaitForOutputFieldID (NTV2_FIELD0, channel);
// 		bRet = gIsCrosspointInput [channelSpec] ? WaitForInputVerticalInterrupt (channel) : WaitForOutputVerticalInterrupt (channel);
// 		bRet = gIsCrosspointInput [channelSpec] ? WaitForInputVerticalInterrupt (channel) : WaitForOutputVerticalInterrupt (channel);
		AUTOCIRCULATE_STATUS_STRUCT acStatus;
		memset(&acStatus, 0, sizeof(AUTOCIRCULATE_STATUS_STRUCT));
		GetAutoCirculate(channelSpec, &acStatus );

		if ( acStatus.state != NTV2_AUTOCIRCULATE_DISABLED)
		{
			// something is wrong so abort.
			bRet = AbortAutoCirculate(channelSpec);
		}

    }

    return bRet;
}

// AbortAutoCirculate(NTV2Crosspoint channelSpec)
// Abort associated AutoCirculate based on channelSpec.
// 
bool   CNTV2Card::AbortAutoCirculate(NTV2Crosspoint channelSpec)
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

  	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = eAbortAutoCirc;
	autoCircData.channelSpec = channelSpec;

	// Call the OS specific method
	bool bRet =  AutoCirculate (autoCircData);

    return bRet;
}


// PauseAutoCirculate(NTV2Crosspoint channelSpec)
// Pauses associated AutoCirculate based on channelSpec.
bool   CNTV2Card::PauseAutoCirculate(NTV2Crosspoint channelSpec, bool pPlayToPause /*= true*/)
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = ePauseAutoCirc;
	autoCircData.channelSpec = channelSpec;
	autoCircData.bVal1		 = pPlayToPause;

	// Call the OS specific method
	return AutoCirculate (autoCircData);
}

// GetAutoCirculate(NTV2Crosspoint channelSpec,AUTOCIRCULATE_STATUS_STRUCT* autoCirculateStatus )
// Returns true if communication with the driver was successful.
// Passes back: whether associated channelSpec is currently autocirculating;
//              Frame Range (Start and End); and Current Active Frame.
//
//              Note that Current Active Frame is reliable,
//              whereas reading, for example, the Ch1OutputFrame register is not reliable,
//              because the latest-written value may or may-not have been clocked-in to the hardware.
//              Note also that this value is valid only if bIsCirculating is true.
bool   CNTV2Card::GetAutoCirculate(NTV2Crosspoint channelSpec, AUTOCIRCULATE_STATUS_STRUCT* autoCirculateStatus )
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

	// The following is ignored by Windows.
	autoCirculateStatus -> channelSpec = channelSpec;
	
	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = eGetAutoCirc;
	autoCircData.channelSpec = channelSpec;
	autoCircData.pvVal1		 = (PVOID) autoCirculateStatus;

	// Call the OS specific method
	return AutoCirculate (autoCircData);
}



//GetFrameStamp(NTV2Crosspoint channelSpec, ULONG frameNum, FRAME_STAMP_STRUCT* pFrameStamp)
//When a channelSpec is autocirculating, the ISR or DPC will continously fill in a
// FRAME_STAMP_STRUCT for the frame it is working on.
// The framestamp structure is intended to give enough information to determine if frames
// have been dropped either on input or output. It also allows for synchronization of 
// audio and video by stamping the audioinputaddress at the start and end of a video frame.
bool   CNTV2Card::GetFrameStamp (NTV2Crosspoint channelSpec, ULWord frameNum, FRAME_STAMP_STRUCT* pFrameStamp)
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = eGetFrameStamp;
	autoCircData.channelSpec = channelSpec;
	autoCircData.lVal1		 = frameNum;
	autoCircData.pvVal1		 = (PVOID) pFrameStamp;

	// The following is ignored by Windows; it looks at the 
	// channel spec and frame num in the autoCircData instead.
	pFrameStamp->channelSpec = channelSpec;
	pFrameStamp->frame = frameNum;

	// Call the OS specific method
	return AutoCirculate (autoCircData);
}

bool   CNTV2Card::GetFrameStampEx2 (NTV2Crosspoint channelSpec, ULWord frameNum, 
									FRAME_STAMP_STRUCT* pFrameStamp,
									PAUTOCIRCULATE_TASK_STRUCT pTaskStruct)
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = eGetFrameStampEx2;
	autoCircData.channelSpec = channelSpec;
	autoCircData.lVal1		 = frameNum;
	autoCircData.pvVal1		 = (PVOID) pFrameStamp;
	autoCircData.pvVal2		 = (PVOID) pTaskStruct;

	// The following is ignored by Windows; it looks at the 
	// channel spec and frame num in the autoCircData instead.
	pFrameStamp->channelSpec = channelSpec;
	pFrameStamp->frame = frameNum;

	// Call the OS specific method
	return AutoCirculate (autoCircData);
}

bool CNTV2Card::FlushAutoCirculate (NTV2Crosspoint channelSpec)
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = eFlushAutoCirculate;
	autoCircData.channelSpec = channelSpec;

	// Call the OS specific method
	return AutoCirculate (autoCircData);
}

bool CNTV2Card::SetActiveFrameAutoCirculate (NTV2Crosspoint channelSpec, ULWord lActiveFrame)
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = eSetActiveFrame;
	autoCircData.channelSpec = channelSpec;
	autoCircData.lVal1 = lActiveFrame;

	// Call the OS specific method
	return AutoCirculate (autoCircData);
}

bool CNTV2Card::PrerollAutoCirculate (NTV2Crosspoint channelSpec, ULWord lPrerollFrames)
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = ePrerollAutoCirculate;
	autoCircData.channelSpec = channelSpec;
	autoCircData.lVal1 = lPrerollFrames;

	// Call the OS specific method
	return AutoCirculate (autoCircData);
}




//  Notes:  This interface (TransferWithAutoCirculate) gives the driver control
//  over advancing the frame # to be transferred.
//  This allows the driver to assign the sequential frame # appropriately,
//  even in the face of dropped frames caused by CPU overload, etc.   
bool CNTV2Card::TransferWithAutoCirculate(PAUTOCIRCULATE_TRANSFER_STRUCT pTransferStruct,
                                          PAUTOCIRCULATE_TRANSFER_STATUS_STRUCT pTransferStatusStruct)                                        
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

    // Sanity check with new parameter videoOffset
    if (pTransferStruct->videoDmaOffset)
    {
        if (pTransferStruct->videoBufferSize < pTransferStruct->videoDmaOffset)
            return false;
    }
	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = eTransferAutoCirculate;
	autoCircData.pvVal1		 = (PVOID) pTransferStruct;
	autoCircData.pvVal2		 = (PVOID) pTransferStatusStruct;

	// Call the OS specific method
	return AutoCirculate (autoCircData);
}

//
// TransferWithAutoCirculateEx
// Same as TransferWithAutoCirculate
// but adds the ability to pass routing register setups for xena2....
// actually you can send over any register setups you want. These
// will be set by the driver at the appropriate frame.
bool CNTV2Card::TransferWithAutoCirculateEx(PAUTOCIRCULATE_TRANSFER_STRUCT pTransferStruct,
                                          PAUTOCIRCULATE_TRANSFER_STATUS_STRUCT pTransferStatusStruct,
										  NTV2RoutingTable* pXena2RoutingTable)
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

    // Sanity check with new parameter videoOffset
    if (pTransferStruct->videoDmaOffset)
    {
        if (pTransferStruct->videoBufferSize < pTransferStruct->videoDmaOffset)
            return false;
    }
	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = eTransferAutoCirculateEx;
	autoCircData.pvVal1		 = (PVOID) pTransferStruct;
	autoCircData.pvVal2		 = (PVOID) pTransferStatusStruct;
	autoCircData.pvVal3		 = (PVOID) pXena2RoutingTable;

	// Call the OS specific method
	return AutoCirculate (autoCircData);
}


//
// TransferWithAutoCirculateEx2
// Same as TransferWithAutoCirculate
// adds flexible frame accurate task list
bool CNTV2Card::TransferWithAutoCirculateEx2(PAUTOCIRCULATE_TRANSFER_STRUCT pTransferStruct,
											 PAUTOCIRCULATE_TRANSFER_STATUS_STRUCT pTransferStatusStruct,
											 NTV2RoutingTable* pXena2RoutingTable,
											 PAUTOCIRCULATE_TASK_STRUCT pTaskStruct)                                        
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

	// Sanity check with new parameter videoOffset
	if (pTransferStruct->videoDmaOffset)
	{
		if (pTransferStruct->videoBufferSize < pTransferStruct->videoDmaOffset)
			return false;
	}
	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = eTransferAutoCirculateEx2;
	autoCircData.pvVal1		 = (PVOID) pTransferStruct;
	autoCircData.pvVal2		 = (PVOID) pTransferStatusStruct;
	autoCircData.pvVal3		 = (PVOID) pXena2RoutingTable;
	autoCircData.pvVal4		 = (PVOID) pTaskStruct;

	// Call the OS specific method
	return AutoCirculate (autoCircData);
}

//
// SetAutoCirculateCaptureTask
// add frame accurate task list for each captured frame
bool CNTV2Card::SetAutoCirculateCaptureTask(NTV2Crosspoint channelSpec, PAUTOCIRCULATE_TASK_STRUCT pTaskStruct)
{
	// Insure that the CNTV2Card has been 'open'ed
	assert( _boardOpened );

	// Fill in our OS independent data structure 
	AUTOCIRCULATE_DATA autoCircData;
	memset(&autoCircData, 0, sizeof(AUTOCIRCULATE_DATA));
	autoCircData.eCommand	 = eSetCaptureTask;
	autoCircData.channelSpec = channelSpec;
	autoCircData.pvVal1		 = (PVOID) pTaskStruct;

	// Call the OS specific method
	return AutoCirculate (autoCircData);
}

#if !defined (NTV2_DEPRECATE)
bool   CNTV2Card::StartAutoCirculateAtTime (NTV2Crosspoint channelSpec, ULWord64 startTime)                                  
{
	return StartAutoCirculate (channelSpec, startTime);
}
#endif	//	!NTV2_DEPRECATE
