/////////////////////////////////////////////////////////////////////////////
// ntv2avDma.cpp
// 
// Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
// Non-device specific CNTV2Card class DMA routines.
/////////////////////////////////////////////////////////////////////////////

#include "ntv2card.h"
#include "ntv2devicefeatures.h"


// DmaRead
bool CNTV2Card::DmaRead (const NTV2DMAEngine inDMAEngine, const ULWord inFrameNumber, ULWord * pOutFrameBuffer,
						 const ULWord inOffsetBytes, const ULWord inByteCount, const bool inSynchronous)
{
	return DmaTransfer (inDMAEngine, true, inFrameNumber, pOutFrameBuffer, inOffsetBytes, inByteCount, inSynchronous);
}

//////////////////////////////////
// DmaWrite
bool CNTV2Card::DmaWrite (const NTV2DMAEngine inDMAEngine, const ULWord inFrameNumber, ULWord * pInFrameBuffer,
						  const ULWord inOffsetBytes, const ULWord inByteCount, const bool inSynchronous)
{
	return DmaTransfer (inDMAEngine, false, inFrameNumber, pInFrameBuffer, inOffsetBytes, inByteCount, inSynchronous);
}

// DmaReadFrame
bool CNTV2Card::DmaReadFrame (NTV2DMAEngine DMAEngine, ULWord frameNumber, ULWord * pFrameBuffer,
                              ULWord bytes, bool bSync)
{
	return DmaTransfer (DMAEngine, true, frameNumber, pFrameBuffer, (ULWord) 0, bytes, bSync);
}

//////////////////////////////////
// DmaWriteFrame
bool CNTV2Card::DmaWriteFrame (NTV2DMAEngine DMAEngine, ULWord frameNumber, ULWord * pFrameBuffer,
                                 ULWord bytes, bool bSync)
{
	return DmaTransfer (DMAEngine, false, frameNumber, pFrameBuffer, (ULWord) 0, bytes, bSync);
}


/////////////////////////////////
// DmaReadSegment
bool CNTV2Card::DmaReadSegment (NTV2DMAEngine DMAEngine, ULWord frameNumber, ULWord * pFrameBuffer,
								ULWord offsetBytes, ULWord bytes,
								ULWord numSegments, ULWord segmentHostPitch, ULWord segmentCardPitch,
								bool bSync)
{
	return DmaTransfer (DMAEngine, true, frameNumber, pFrameBuffer, offsetBytes, bytes, 
						numSegments, segmentHostPitch, segmentCardPitch, bSync);
}

/////////////////////////////////
// DmaWriteSegment
bool CNTV2Card::DmaWriteSegment (NTV2DMAEngine DMAEngine, ULWord frameNumber, ULWord * pFrameBuffer,
								ULWord offsetBytes, ULWord bytes,
								ULWord numSegments, ULWord segmentHostPitch, ULWord segmentCardPitch,
								bool bSync)
{
	return DmaTransfer (DMAEngine, false, frameNumber, pFrameBuffer, offsetBytes, bytes, 
						numSegments, segmentHostPitch, segmentCardPitch, bSync);
}

/////////////////////////////////
// P2PTargetFrame
bool CNTV2Card::DmaP2PTargetFrame(NTV2Channel channel,
								  ULWord frameNumber,
								  ULWord frameOffset,
								  PCHANNEL_P2P_STRUCT pP2PData)
{
	return DmaTransfer(NTV2_PIO,
					   channel,
					   true,
					   frameNumber,
					   frameOffset,
					   0, 0, 0, 0,
					   pP2PData);
}

/////////////////////////////////
// P2PTransferFrame
bool CNTV2Card::DmaP2PTransferFrame(NTV2DMAEngine DMAEngine, 
									ULWord frameNumber,
									ULWord frameOffset,
									ULWord transferSize,
									ULWord numSegments,
									ULWord segmentTargetPitch,
									ULWord segmentCardPitch,
									PCHANNEL_P2P_STRUCT pP2PData)
{
	return DmaTransfer(DMAEngine,
					   NTV2_CHANNEL1,
					   false,
					   frameNumber,
					   frameOffset,
					   transferSize,
					   numSegments,
					   segmentTargetPitch,
					   segmentCardPitch,
					   pP2PData);
}

// DmaAudioRead
bool CNTV2Card::DmaAudioRead (const NTV2DMAEngine inDMAEngine, const NTV2AudioSystem inAudioEngine, ULWord * pOutAudioBuffer,
						 const ULWord inOffsetBytes, const ULWord inByteCount, const bool inSynchronous)
{
	if(!::NTV2DeviceCanDoStackedAudio(GetDeviceID()))
		return false;

	WriteRegister(kRegAdvancedIndexing, 1);
	ULWord memSize = NTV2DeviceGetActiveMemorySize(GetDeviceID());
	ULWord engineOffset = memSize - (((ULWord)inAudioEngine + 1) * 0x800000);
	ULWord audioOffset = inOffsetBytes + engineOffset;

	return DmaTransfer (inDMAEngine, true, 0, pOutAudioBuffer, audioOffset, inByteCount, inSynchronous);
}

//////////////////////////////////
// DmaAudioWrite
bool CNTV2Card::DmaAudioWrite (const NTV2DMAEngine inDMAEngine, const NTV2AudioSystem inAudioEngine, ULWord * pInAudioBuffer,
						  const ULWord inOffsetBytes, const ULWord inByteCount, const bool inSynchronous)
{
	if(!::NTV2DeviceCanDoStackedAudio(GetDeviceID()))
		return false;

	WriteRegister(kRegAdvancedIndexing, 1);
	ULWord memSize = NTV2DeviceGetActiveMemorySize(GetDeviceID());
	ULWord engineOffset = memSize - (((ULWord)inAudioEngine + 1) * 0x800000);
	ULWord audioOffset = inOffsetBytes + engineOffset;

	return DmaTransfer (inDMAEngine, false, 0, pInAudioBuffer, audioOffset, inByteCount, inSynchronous);
}

#if !defined (NTV2_DEPRECATE)
bool CNTV2Card::DmaReadField (NTV2DMAEngine DMAEngine, ULWord frameNumber, NTV2FieldID fieldID,
							  ULWord * pFrameBuffer, ULWord bytes, bool bSync)
{
	ULWord ulOffset;

	if ( fieldID == NTV2_FIELD0)
	{
		ulOffset = (ULWord)0;
	}
	else
	{
		ulOffset = (ULWord)(_ulFrameBufferSize / 2);
	}

	return DmaTransfer (DMAEngine, true, frameNumber, (ULWord *) pFrameBuffer, ulOffset, bytes, bSync);
}

bool CNTV2Card::DmaWriteField (NTV2DMAEngine DMAEngine, ULWord frameNumber, NTV2FieldID fieldID,
							   ULWord * pFrameBuffer, ULWord bytes, bool bSync)
{
	ULWord ulOffset;

	if ( fieldID == NTV2_FIELD0)
	{
		ulOffset = (ULWord)0;
	}
	else
	{
		ulOffset = (ULWord)(_ulFrameBufferSize / 2);
	}

	return DmaTransfer (DMAEngine, false, frameNumber, pFrameBuffer, ulOffset, bytes, bSync);
}
#endif	//	!defined (NTV2_DEPRECATE)
