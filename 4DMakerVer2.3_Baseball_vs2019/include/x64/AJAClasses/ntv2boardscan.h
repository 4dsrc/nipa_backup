/**
	@file		ntv2boardscan.h
	@deprecated	Include ntv2devicescanner.h instead.
	@copyright	(C) 2004-2014 AJA Video Systems, Inc.	Proprietary and confidential information.
**/

#ifndef NTV2BOARDSCAN_H
#define NTV2BOARDSCAN_H

#include "ntv2devicescanner.h"

#endif	//	NTV2BOARDSCAN_H
