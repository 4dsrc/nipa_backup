/////////////////////////////////////////////////////////////////////////////
// CNTV2Status
//
// Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
/////////////////////////////////////////////////////////////////////////////


#ifndef NTV2STATUS_H
#define NTV2STATUS_H
#include <string>

#include "ajaexport.h"
#include "ntv2card.h"

#if defined (NTV2_DEPRECATE)
	typedef	CNTV2Card	CNTV2Status;
#else
	class AJAExport CNTV2Status : public CNTV2Card
	{
	public:
		inline CNTV2Status ()	{}

		CNTV2Status (UWord			inDeviceIndex,
					bool			displayErrorMessage	= false,
					UWord			ulBoardType			= BOARDTYPE_NTV2,
					const char *	pInHostname			= NULL	//	Non-null: device on remote host
		);

		virtual ~CNTV2Status();

	public:
		AJA_VIRTUAL void GetBoardString						(std::string & outString);
		AJA_VIRTUAL void GetFrameBufferVideoFormatString	(std::string & outString);

		AJA_VIRTUAL void GetInput1VideoFormatString			(std::string & outString);
		AJA_VIRTUAL void GetInput2VideoFormatString			(std::string & outString);
		AJA_VIRTUAL void GetInput3VideoFormatString			(std::string & outString);
		AJA_VIRTUAL void GetInput4VideoFormatString			(std::string & outString);
		AJA_VIRTUAL void GetInput5VideoFormatString			(std::string & outString);
		AJA_VIRTUAL void GetInput6VideoFormatString			(std::string & outString);
		AJA_VIRTUAL void GetInput7VideoFormatString			(std::string & outString);
		AJA_VIRTUAL void GetInput8VideoFormatString			(std::string & outString);

		AJA_VIRTUAL void GetHDMIInputVideoFormatString		(std::string & outString);
		AJA_VIRTUAL void GetAnalogInputVideoFormatString	(std::string & outString);
		AJA_VIRTUAL void GetReferenceVideoFormatString		(std::string & outString);

		AJA_VIRTUAL void GetInputVideoFormatString			(int inputNum, std::string & outString);

		static void GetVideoFormatString (NTV2VideoFormat format, std::string & outString);
		static void GetVideoStandardString (NTV2Standard standard, std::string & outString);
		static void GetFrameRateString (NTV2FrameRate frameRate, std::string & outString);

	//protected:
		//NTV2VideoFormat GetNTV2VideoFormat (UByte status, UByte frameRateHiBit = 0);		** MrBill **	MISSING ?!?!?!

	};	//	CNTV2Status
#endif	//	!defined (NTV2_DEPRECATE)

#endif	//	NTV2STATUS_H
