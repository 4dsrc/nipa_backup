/**
	@file		ntv2card.cpp
	@brief		Partially implements the CNTV2Card class. Other implementation files are 'ntv2audio.cpp', 'ntv2dma.cpp', and 'ntv2register.cpp'.
	@copyright	(C) 2004-2014 AJA Video Systems, Inc.	Proprietary and confidential information.
**/

#include "ntv2devicefeatures.h"
#include "ntv2card.h"
#include "ntv2debug.h"
#include "ntv2utils.h"
#include <sstream>

using namespace std;


// Default Constructor
CNTV2Card::CNTV2Card ()
{
	_boardOpened = false;
}

// Constructor that Opens Board
CNTV2Card::CNTV2Card (const UWord boardNumber, const bool displayErrorMessage, const UWord ulBoardType, const char *hostname)
{
	_boardOpened = false;
	const NTV2DeviceType	eUseBoardType	(static_cast <NTV2DeviceType> (ulBoardType));
//	const NTV2BoardType	eBoardType		(BOARDTYPE_NTV2);

//	if (eBoardType & eUseBoardType)
		Open (boardNumber, displayErrorMessage, eUseBoardType, hostname);

	if (IsBufferSizeSetBySW ())
	{
		NTV2Framesize fbSize;
		GetFrameBufferSize (NTV2_CHANNEL1, &fbSize);
		SetFrameBufferSize (fbSize);
	}
	else
	{
		NTV2FrameGeometry fg;
		NTV2FrameBufferFormat format;

		GetFrameGeometry (&fg);
		GetFrameBufferFormat (NTV2_CHANNEL1, &format);

		_ulFrameBufferSize = ::NTV2DeviceGetFrameBufferSize (GetDeviceID (), fg, format);
		_ulNumFrameBuffers = ::NTV2DeviceGetNumberFrameBuffers (GetDeviceID (), fg, format);
	}

 }	//	constructor


// Destructor
CNTV2Card::~CNTV2Card ()
{
	if (IsOpen ())
		Close ();

}	//	destructor


NTV2DeviceID CNTV2Card::GetDeviceID (void)
{
	ULWord	value	(0);
	if (_boardOpened && ReadRegister (kRegBoardID, &value))
		return static_cast <NTV2DeviceID> (value);
	else
		return DEVICE_ID_NOTFOUND;
}


Word CNTV2Card::GetDeviceVersion (void)
{
	ULWord	status	(0);
	return ReadRegister (kRegStatus, &status) ? (status & 0xF) : -1;
}


string CNTV2Card::GetDeviceVersionString (void)
{
	ostringstream	oss;
	oss << ::NTV2DeviceIDToString (GetDeviceID ());
	return oss.str ();
}


string CNTV2Card::GetFPGAVersionString (const NTV2XilinxFPGA inFPGA)
{
	ULWord			numBytes	(0);
	string			dateStr, timeStr;
	ostringstream	oss;

	if (inFPGA == eFPGAVideoProc && GetInstalledBitfileInfo (numBytes, dateStr, timeStr))
		oss << dateStr << " at " << timeStr;
	else
		oss << "Unavailable";

	return oss.str ();
}


Word CNTV2Card::GetPCIFPGAVersion (void)
{
	ULWord	status	(0);
	return ReadRegister (48, &status) ? ((status >> 8) & 0xFF) : -1;
}


string CNTV2Card::GetPCIFPGAVersionString (void)
{
	const UWord		version	(GetPCIFPGAVersion ());
	ostringstream	oss;
	oss << hex << version;
	return oss.str ();
}


string CNTV2Card::GetDriverVersionString (void)
{
	stringstream	oss;
	ULWord			versionInfo	(0);

	GetDriverVersion (&versionInfo);

	#if defined (MSWindows)
		// Bits 3-0		minor version
		// Bits 7-4		major version
		// Bits 11-8	point version
		// Bit	12		reserved
		// Bit	13		64 bit flag
		// Bit  14		beta flag
		// Bit  15		debug flag
		// Bits	31-16	build version

		const ULWord	ulMajor	((versionInfo >>  4) & 0xf);
		const ULWord	ulMinor	((versionInfo >>  0) & 0xf);
		const ULWord	ulPoint	((versionInfo >>  8) & 0xf);
		const ULWord	ulBuild	((versionInfo >> 16) & 0xffff);

		if ((ulMajor < 6) ||
			((ulMajor == 6) && (ulMinor < 5)) ||
			((ulMajor == 6) && (ulMinor == 5) && (ulBuild == 0)))
		{
			oss	<< dec << ulMajor
				<< "." << dec << ulMinor
				<< ((versionInfo & (BIT_14)) ? " beta " : ".") << dec << ulPoint
				<< ((versionInfo & (BIT_13)) ? " 64bit" : "")
				<< ((versionInfo & (BIT_15)) ? " debug" : "");
		}
		else
			oss << dec << ulMajor
				<< "." << dec << ulMinor
				<< "." << dec << ulPoint
				<< ((versionInfo & (BIT_14)) ? " beta " : " build ") << dec << ulBuild
				<< ((versionInfo & (BIT_13)) ? " 64bit" : "")
				<< ((versionInfo & (BIT_15)) ? " debug" : "");

	#elif defined (AJALinux)

		// Bit  15     Debug
		// Bit  14     Beta Version flag (Bits 13-8 interpreted as "beta x"
		// Bits 13-8   sub-minor Version (or beta version #)
		// Bits 7-4    Major Version  - new hardware types to support, etc.
		// Bits 3-0    Minor Version  

		oss	<< dec << ((versionInfo >> 4) & 0xF) << "." << dec << (versionInfo & 0xF)	//	Major and Minor version
			<< "." << dec << ((versionInfo >> 8) & 0x3F);								//	Point version

		if (versionInfo & 0xFFFF000)
			oss << "." << dec << ((versionInfo >> 16) & 0xFFFF);						//	Build Version, if present

		if (versionInfo & (BIT_14))
			oss << " Beta";																//	Beta Version

		if (versionInfo & BIT_15)
			oss << " Debug";															//	Debug Version

	#elif defined (AJAMac)

		NumVersion	version	=	{0, 0, 0, 0};
		GetDriverVersion (&version);
		oss	<< uint32_t (version.majorRev) << "." << uint32_t (version.minorAndBugRev) << "." << uint32_t (version.stage)
			<< ", Interface " << uint32_t (AJA_MAC_DRIVER_INTERFACE_VERSION);

	#else

		// Bit  15     Debug
		// Bit  14     Beta Version flag (Bits 13-8 interpreted as "beta x"
		// Bits 13-8   sub-minor Version (or beta version #)
		// Bits 7-4    Major Version  - new hardware types to support, etc.
		// Bits 3-0    Minor Version

		oss	<< dec << ((versionInfo >> 4) & 0xF) << "." << dec << (versionInfo & 0xF);	//	Major and Minor version

		if (versionInfo & (BIT_14))
			oss << " Beta " << dec << ((versionInfo >> 8) & 0x1F);						//	Beta Version
		else if (versionInfo & (BIT_13 + BIT_12 + BIT_11 + BIT_10 + BIT_9 + BIT_8))
			oss << "." << dec << ((versionInfo >> 8) & 0x3F);							//	Sub-minor Version

		if (versionInfo & BIT_15)
			oss << " Debug";															//	Debug Version

	#endif

	return oss.str ();

}	//	GetDriverVersionString


ULWord CNTV2Card::GetSerialNumberLow (void)
{
	ULWord	serialNum	(0);
	return ReadRegister (54, &serialNum) ? serialNum : 0;	//	Read EEPROM shadow of Serial Number
}


ULWord CNTV2Card::GetSerialNumberHigh (void)
{
	ULWord	serialNum	(0);
	return ReadRegister (55, &serialNum) ? serialNum : 0;	//	Read EEPROM shadow of Serial Number
}


uint64_t CNTV2Card::GetSerialNumber (void)
{
	const uint64_t	lo (GetSerialNumberLow ()),  hi (GetSerialNumberHigh ());
	uint64_t		result	((hi << 32) | lo);
	return result;
}


bool CNTV2Card::GetSerialNumberString (string & outSerialNumberString)
{
	bool			bValidSerial	(true);
	const ULWord	serialNumHigh	(GetSerialNumberHigh ());
	const ULWord	serialNumLow	(GetSerialNumberLow ());
    char			serialNum [9];

	serialNum[0] = ((serialNumLow  & 0x000000FF)      );
	serialNum[1] = ((serialNumLow  & 0x0000FF00) >>  8);
	serialNum[2] = ((serialNumLow  & 0x00FF0000) >> 16);
	serialNum[3] = ((serialNumLow  & 0xFF000000) >> 24);
	serialNum[4] = ((serialNumHigh & 0x000000FF)      );
	serialNum[5] = ((serialNumHigh & 0x0000FF00) >>  8);
	serialNum[6] = ((serialNumHigh & 0x00FF0000) >> 16);
	serialNum[7] = ((serialNumHigh & 0xFF000000) >> 24);
	serialNum[8] = '\0';

	for (int validator = 0;  validator < 8;  validator++)
	{
		if (serialNum [validator] == 0)
		{
			//	No characters: no serial number.
			if (validator == 0)
				bValidSerial = false;
			break;	//	End of string -- stop looking
        }

		// Allow only 0-9, A-Z, a-z, blank, and dash only.
		if ( ! ( ( (serialNum[validator] >= '0') && (serialNum[validator] <= '9') ) ||
 				 ( (serialNum[validator] >= 'A') && (serialNum[validator] <= 'Z') ) ||
				 ( (serialNum[validator] >= 'a') && (serialNum[validator] <= 'z') ) ||
				   (serialNum[validator] == ' ') || (serialNum[validator] == '-') ) )
		{
			//	Invalid character -- assume no Serial Number programmed...
			bValidSerial = false;
		}
	}

	if (!bValidSerial)
	{
		serialNum[0] = 'I';
		serialNum[1] = 'N';
		serialNum[2] = 'V';
		serialNum[3] = 'A';
		serialNum[4] = 'L';
		serialNum[5] = 'I';
		serialNum[6] = 'D';
		serialNum[7] = '?';
	}

	outSerialNumberString = serialNum;
	return bValidSerial;

}	//	GetSerialNumberString


bool CNTV2Card::GetInstalledBitfileInfo (ULWord & outNumBytes, std::string & outDateStr, std::string & outTimeStr)
{
	outDateStr.clear ();
	outTimeStr.clear ();
	outNumBytes = 0;

	if (!_boardOpened)
		return false;	//	Bail if I'm not open

	BITFILE_INFO_STRUCT		bitFileInfo;
	::memset (&bitFileInfo, 0, sizeof (bitFileInfo));
	bitFileInfo.whichFPGA = eFPGAVideoProc;

	//	Call the OS specific method...
	if (!DriverGetBitFileInformation (bitFileInfo))
		return false;

	//	Fill in our OS independent data structure...
	outDateStr = reinterpret_cast <char *> (&bitFileInfo.dateStr [0]);
	outTimeStr = reinterpret_cast <char *> (&bitFileInfo.timeStr [0]);
	outNumBytes = bitFileInfo.numBytes;
	return true;
}


bool CNTV2Card::GetInput1Autotimed (void)
{
	ULWord	status	(0);
	ReadRegister (kRegInputStatus, &status);
	return !(status & BIT_3);
}


bool CNTV2Card::GetInput2Autotimed (void)
{
	ULWord	status	(0);
	ReadRegister (kRegInputStatus, &status);
	return !(status & BIT_11);
}


bool CNTV2Card::GetAnalogInputAutotimed (void)
{
	ULWord	value	(0);
	ReadRegister (kRegAnalogInputStatus, &value, kRegMaskInputStatusLock, kRegShiftInputStatusLock);
	return value == 1;
}


bool CNTV2Card::GetHDMIInputAutotimed (void)
{
	ULWord	value	(0);
	ReadRegister (kRegHDMIInputStatus, &value, kRegMaskInputStatusLock, kRegShiftInputStatusLock);
	return value == 1;
}


bool CNTV2Card::GetInputAutotimed (int inInputNum)
{
	bool bResult = false;

	ULWord status;
	ReadInputStatusRegister(&status);

	switch (inInputNum)
	{
		case 0:	bResult = !(status & BIT_3);	break;
		case 1: bResult = !(status & BIT_11);	break;
	}
	
	return bResult;
}


//	Implementation of NTV2VideoFormatSet's ostream writer...

std::ostream & operator << (std::ostream & inOStream, const NTV2VideoFormatSet & inFormats)
{
	NTV2VideoFormatSet::const_iterator	iter	(inFormats.begin ());

	inOStream	<< inFormats.size ()
				<< (inFormats.size () == 1 ? " format: {" : " format(s): {");

	while (iter != inFormats.end ())
	{
		inOStream << std::string (::NTV2VideoFormatToString (*iter));
		inOStream << (++iter == inFormats.end () ? "}" : ", ");
	}

	return inOStream;

}	//	operator <<


//	Implementation of NTV2AutoCirculateStateToString...

string NTV2AutoCirculateStateToString (const NTV2AutoCirculateState inState)
{
	static const char *	sStateStrings []	= {	"Disabled", "Initialized", "Starting", "Paused", "Stopping", "Running", "StartingAtTime", NULL};
	if (inState >= NTV2_AUTOCIRCULATE_DISABLED && inState <= NTV2_AUTOCIRCULATE_STARTING_AT_TIME)
		return string (sStateStrings [inState]);
	else
		return "<invalid>";
}

#if !defined (NTV2_DEPRECATE)
	bool CNTV2Card::GetBitFileInformation (ULWord &	outNumBytes, string & outDateStr, string & outTimeStr, const NTV2XilinxFPGA inFPGA)
	{
		return inFPGA == eFPGAVideoProc ? GetInstalledBitfileInfo (outNumBytes, outDateStr, outTimeStr) : false;
	}

	Word CNTV2Card::GetFPGAVersion (const NTV2XilinxFPGA inFPGA)
	{
		(void) inFPGA;
		return -1;
	}

	UWord CNTV2Card::GetNumNTV2Boards()
	{
		ULWord numBoards = 0;
		CNTV2Card ntv2Card;

		for (ULWord boardCount = 0;  boardCount < NTV2_MAXBOARDS;  boardCount++)
		{
			if (ntv2Card.Open (boardCount))
			{
				numBoards++;
				ntv2Card.Close ();
			}
			else
				break;
		}
		return numBoards;
	}

	NTV2BoardType CNTV2Card::GetBoardType (void) const
	{
		return _boardType;
	}

	NTV2BoardSubType CNTV2Card::GetBoardSubType (void)
	{
		return BOARDSUBTYPE_NONE;
	}

	bool CNTV2Card::SetBoard (UWord inDeviceIndexNumber)
	{
		return Open (inDeviceIndexNumber);
	}

	string CNTV2Card::GetBoardIDString (void)
	{
		const ULWord	boardID	(static_cast <ULWord> (GetDeviceID ()));
		ostringstream	oss;
		oss << hex << boardID;
		return oss.str ();
	}
#endif	//	!defined (NTV2_DEPRECATE)
