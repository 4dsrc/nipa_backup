//
//
// ntv2vpid.cpp
//
//	see SMPTE 352
//
//	Copyright (C) 2012 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
//

#include "ntv2vpid.h"
#include "ntv2debug.h"

#include <string.h>

#ifndef NULL
#define NULL (0)
#endif

CNTV2VPID::CNTV2VPID()
{
	Init();
}

CNTV2VPID::~CNTV2VPID()
{
}

void CNTV2VPID::Init()
{
	m_uVPID = 0;
}

void CNTV2VPID::SetVPID(ULWord data)
{
	m_uVPID = data;
}

ULWord CNTV2VPID::GetVPID()
{
	return m_uVPID;
}

bool CNTV2VPID::SetVPID(NTV2VideoFormat videoFormat,
						NTV2FrameBufferFormat frameBufferFormat,
						bool progressive,
						bool aspect16x9,
						VPIDChannel channel)
{
	return SetVPIDData(&m_uVPID, videoFormat, frameBufferFormat, progressive, aspect16x9, channel);
}

bool CNTV2VPID::SetVPID(NTV2VideoFormat videoFormat,
						bool progressive,
						bool aspect16x9,
						bool dualLink,
						bool format48Bit,
						bool output3Gb,
						VPIDChannel channel)
{
	return SetVPIDData(&m_uVPID, videoFormat, progressive, aspect16x9,
					   dualLink, format48Bit, output3Gb, channel);
}

void CNTV2VPID::SetVersion(VPIDVersion version)
{
	m_uVPID = (m_uVPID & ~kRegMaskVPIDVersionID) |
		(((ULWord)version << kRegShiftVPIDVersionID) & kRegMaskVPIDVersionID);
}

VPIDVersion CNTV2VPID::GetVersion()
{
	return (VPIDVersion)((m_uVPID & kRegMaskVPIDVersionID) >> kRegShiftVPIDVersionID); 
}

void CNTV2VPID::SetStandard(VPIDStandard standard)
{
	m_uVPID = (m_uVPID & ~kRegMaskVPIDStandard) |
		(((ULWord)standard << kRegShiftVPIDStandard) & kRegMaskVPIDStandard);
}

VPIDStandard CNTV2VPID::GetStandard()
{
	return (VPIDStandard)((m_uVPID & kRegMaskVPIDStandard) >> kRegShiftVPIDStandard); 
}

void CNTV2VPID::SetProgressiveTransport(bool pt)
{
	m_uVPID = (m_uVPID & ~kRegMaskVPIDProgressiveTransport) |
		(((pt?1:0) << kRegShiftVPIDProgressiveTransport) & kRegMaskVPIDProgressiveTransport);
}

bool CNTV2VPID::GetProgressiveTransport()
{
	 return (m_uVPID & kRegMaskVPIDProgressiveTransport) != 0; 
}

void CNTV2VPID::SetProgressivePicture(bool pp)
{
	m_uVPID = (m_uVPID & ~kRegMaskVPIDProgressivePicture) |
		(((pp?1:0) << kRegShiftVPIDProgressivePicture) & kRegMaskVPIDProgressivePicture);
}

bool CNTV2VPID::GetProgressivePicture()
{
	return (m_uVPID & kRegMaskVPIDProgressivePicture) != 0; 
}

void CNTV2VPID::SetPictureRate(VPIDPictureRate rate)
{
	m_uVPID = (m_uVPID & ~kRegMaskVPIDPictureRate) |
		(((ULWord)rate << kRegShiftVPIDPictureRate) & kRegMaskVPIDPictureRate);
}

VPIDPictureRate CNTV2VPID::GetPictureRate()
{
	return (VPIDPictureRate)((m_uVPID & kRegMaskVPIDPictureRate) >> kRegShiftVPIDPictureRate); 
}

void CNTV2VPID::SetImageAspect16x9(bool aspect16x9)
{
	m_uVPID = (m_uVPID & ~kRegMaskVPIDImageAspect16x9) |
		(((aspect16x9?1:0) << kRegShiftVPIDImageAspect16x9) & kRegMaskVPIDImageAspect16x9);
}

bool CNTV2VPID::GetImageAspect16x9()
{
	return (m_uVPID & kRegMaskVPIDImageAspect16x9) != 0; 
}

void CNTV2VPID::SetSampling(VPIDSampling sampling)
{
	m_uVPID = (m_uVPID & ~kRegMaskVPIDSampling) |
		(((ULWord)sampling << kRegShiftVPIDSampling) & kRegMaskVPIDSampling);
}

VPIDSampling CNTV2VPID::GetSampling()
{
	return (VPIDSampling)((m_uVPID & kRegMaskVPIDSampling) >> kRegShiftVPIDSampling); 
}

void CNTV2VPID::SetChannel(VPIDChannel channel)
{
	m_uVPID = (m_uVPID & ~kRegMaskVPIDChannel) |
		(((ULWord)channel << kRegShiftVPIDChannel) & kRegMaskVPIDChannel);
}

VPIDChannel CNTV2VPID::GetChannel()
{
	return (VPIDChannel)((m_uVPID & kRegMaskVPIDChannel) >> kRegShiftVPIDChannel); 
}

void CNTV2VPID::SetDynamicRange(VPIDDynamicRange range)
{
	m_uVPID = (m_uVPID & ~kRegMaskVPIDDynamicRange) |
		(((ULWord)range << kRegShiftVPIDDynamicRange) & kRegMaskVPIDDynamicRange);
}

VPIDDynamicRange CNTV2VPID::GetDynamicRange()
{
	return (VPIDDynamicRange)((m_uVPID & kRegMaskVPIDDynamicRange) >> kRegShiftVPIDDynamicRange); 
}

void CNTV2VPID::SetBitDepth(VPIDBitDepth depth)
{
	m_uVPID = (m_uVPID & ~kRegMaskVPIDBitDepth) |
		(((ULWord)depth << kRegShiftVPIDBitDepth) & kRegMaskVPIDBitDepth);
}

VPIDBitDepth CNTV2VPID::GetBitDepth()
{
	return (VPIDBitDepth)((m_uVPID & kRegMaskVPIDBitDepth) >> kRegShiftVPIDBitDepth); 
}

// deprecated - unsupported 3Gb
bool CNTV2VPID::SetVPIDData(ULWord* pVPID,
							NTV2VideoFormat outputFormat,
							NTV2FrameBufferFormat frameBufferFormat,
							bool progressive,
							bool aspect16x9,
							VPIDChannel channel)
{
	bool bRGB48Bit;
	bool bDualLinkRGB;
	const bool kOutput3Gb = false;
	
	switch(frameBufferFormat)	
	{
	case NTV2_FBF_48BIT_RGB:
		bRGB48Bit = true;
		bDualLinkRGB = true;			// RGB 12 bit
		break;
	case NTV2_FBF_ARGB:
	case NTV2_FBF_RGBA:
	case NTV2_FBF_ABGR:
	case NTV2_FBF_10BIT_RGB:
	case NTV2_FBF_10BIT_DPX:
	case NTV2_FBF_24BIT_RGB:
	case NTV2_FBF_24BIT_BGR:
	case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
	case NTV2_FBF_10BIT_RGB_PACKED:
	case NTV2_FBF_10BIT_ARGB:
	case NTV2_FBF_16BIT_ARGB:
		bDualLinkRGB = true;			// RGB 8+10 bit;
		bRGB48Bit = false;
		break;
	default:
		bDualLinkRGB = false;			// All other
		bRGB48Bit = false;
		break;
	}
	
	// should use this call directly
	return SetVPIDData(	pVPID,
						outputFormat,
						progressive,
						aspect16x9,
						bDualLinkRGB,
						bRGB48Bit,
						kOutput3Gb,
						channel);
}

bool CNTV2VPID::SetVPIDData(ULWord* pVPID,
							NTV2VideoFormat outputFormat,
							bool progressive,
							bool aspect16x9,
							bool dualLinkRGB,
							bool RGB48Bit,
							bool output3Gb,
							VPIDChannel channel)
{
	(void) channel;
	VPIDVersion version = VPIDVersion_1;
	VPIDStandard standard = VPIDStandard_Unknown;
	VPIDPictureRate rate = VPIDPictureRate_None;
	VPIDSampling sampling = VPIDSampling_YUV_422;
	VPIDBitDepth depth = VPIDBitDepth_10;
	ULWord uVPID = 0;
	//SMPTE 425-5 2014 all channels of square division
	//4k/uhd are labeled channel 1
	VPIDChannel SMPTEChannel = VPIDChannel_1;
	
	bool output3Ga = false;
	bool is2K = false;
	bool transport = false;

	if(pVPID == NULL)
	{
		return false;
	}
	
	if(dualLinkRGB)
	{
		sampling = VPIDSampling_GBRA_4444;
		if(RGB48Bit)
		{
			sampling = VPIDSampling_GBR_444;
			depth = VPIDBitDepth_12;
		} 
	}
	else
	{
		sampling = VPIDSampling_YUV_422;
	}

	switch(outputFormat)
	{
	case NTV2_FORMAT_4x1920x1080psf_2398:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2398;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_4x1920x1080psf_2400:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2400;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_4x1920x1080psf_2500:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2500;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_4x1920x1080psf_2997:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2997;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_4x1920x1080psf_3000:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_3000;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_4x2048x1080psf_2398:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2398;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		is2K = true;
		break;
	case NTV2_FORMAT_4x2048x1080psf_2400:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2400;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		is2K = true;
		break;
	case NTV2_FORMAT_4x2048x1080psf_2500:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2500;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		is2K = true;
		break;
	case NTV2_FORMAT_4x2048x1080psf_2997:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2997;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		is2K = true;
		break;
	case NTV2_FORMAT_4x2048x1080psf_3000:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_3000;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		is2K = true;
		break;
	case NTV2_FORMAT_4x1920x1080p_2398:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2398;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		break;
	case NTV2_FORMAT_4x1920x1080p_2400:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2400;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		break;
	case NTV2_FORMAT_4x1920x1080p_2500:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2500;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		break;
	case NTV2_FORMAT_4x1920x1080p_2997:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2997;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		break;
	case NTV2_FORMAT_4x1920x1080p_3000:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_3000;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		break;
	case NTV2_FORMAT_4x1920x1080p_5000:
		standard = (dualLinkRGB || output3Gb) ? VPIDStandard_1080_DualLink : VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_5000;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_4x1920x1080p_5994:
		standard = (dualLinkRGB || output3Gb) ? VPIDStandard_1080_DualLink : VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_5994;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_4x1920x1080p_6000:
		standard = (dualLinkRGB || output3Gb) ? VPIDStandard_1080_DualLink : VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_6000;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_4x2048x1080p_2398:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2398;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		break;
	case NTV2_FORMAT_4x2048x1080p_2400:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2400;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		break;
	case NTV2_FORMAT_4x2048x1080p_2500:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2500;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		break;
	case NTV2_FORMAT_4x2048x1080p_2997:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2997;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		break;
	case NTV2_FORMAT_4x2048x1080p_3000:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_3000;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		break;
	case NTV2_FORMAT_4x2048x1080p_4795:
		standard = (dualLinkRGB || output3Gb) ? VPIDStandard_1080_DualLink : VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_4795;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_4x2048x1080p_4800:
		standard = (dualLinkRGB || output3Gb) ? VPIDStandard_1080_DualLink : VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_4800;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_4x2048x1080p_5000:
		standard = (dualLinkRGB || output3Gb) ? VPIDStandard_1080_DualLink : VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_5000;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_4x2048x1080p_5994:
		standard = (dualLinkRGB || output3Gb) ? VPIDStandard_1080_DualLink : VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_5994;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_4x2048x1080p_6000:
		standard = (dualLinkRGB || output3Gb) ? VPIDStandard_1080_DualLink : VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_6000;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_1080i_5000:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2500;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_1080psf_2500_2:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2500;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_1080i_5994:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2997;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_1080psf_2997_2:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2997;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_1080i_6000:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_3000;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_1080psf_3000_2:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_3000;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_720p_5994:
		standard = VPIDStandard_720;
		rate = VPIDPictureRate_5994;
		progressive = true;
		aspect16x9 = false;
		transport = false;  // not used for 720
		break;
	case NTV2_FORMAT_720p_6000:
		standard = VPIDStandard_720;
		rate = VPIDPictureRate_6000;
		progressive = true;
		aspect16x9 = false;
		transport = false;  // not used for 720
		break;
	case NTV2_FORMAT_1080psf_2398:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2398;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_1080psf_2400:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2400;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_1080p_2997:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2997;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		break;
	case NTV2_FORMAT_1080p_3000:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_3000;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		break;
	case NTV2_FORMAT_1080p_2500:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2500;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		break;
	case NTV2_FORMAT_1080p_2398:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2398;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		break;
	case NTV2_FORMAT_1080p_2400:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2400;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		break;
	case NTV2_FORMAT_1080p_2K_2398:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2398;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		break;
	case NTV2_FORMAT_1080p_2K_2400:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2400;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		break;
	case NTV2_FORMAT_1080p_2K_2500:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2500;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		break;
	case NTV2_FORMAT_1080p_2K_2997:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2997;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		break;
	case NTV2_FORMAT_1080p_2K_3000:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_3000;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		break;
	case NTV2_FORMAT_1080p_2K_4795:
		standard = (dualLinkRGB || output3Gb) ? VPIDStandard_1080_DualLink : VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_4795;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_1080p_2K_4800:
		standard = (dualLinkRGB || output3Gb) ? VPIDStandard_1080_DualLink : VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_4800;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_1080p_2K_5000:
		standard = (dualLinkRGB || output3Gb) ? VPIDStandard_1080_DualLink : VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_5000;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_1080p_2K_5994:
		standard = (dualLinkRGB || output3Gb) ? VPIDStandard_1080_DualLink : VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_5994;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_1080p_2K_6000:
		standard = (dualLinkRGB || output3Gb) ? VPIDStandard_1080_DualLink : VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_6000;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		is2K = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_1080psf_2K_2398:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2398;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		is2K = true;
		break;
	case NTV2_FORMAT_1080psf_2K_2400:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2400;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		is2K = true;
		break;
	case NTV2_FORMAT_1080psf_2K_2500:
		standard = dualLinkRGB ? VPIDStandard_1080_DualLink : VPIDStandard_1080;
		rate = VPIDPictureRate_2500;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		is2K = true;
		break;
	case NTV2_FORMAT_720p_5000:
		standard = VPIDStandard_720;
		rate = VPIDPictureRate_5000;
		progressive = true;
		aspect16x9 = false;
		transport = false;  // not used for 720
		break;
	case NTV2_FORMAT_1080p_5000_B:
		standard = VPIDStandard_1080_DualLink;
		rate = VPIDPictureRate_5000;
		sampling = VPIDSampling_YUV_422;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_1080p_5994_B:
		standard = VPIDStandard_1080_DualLink;
		rate = VPIDPictureRate_5994;
		sampling = VPIDSampling_YUV_422;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_1080p_6000_B:
		standard = VPIDStandard_1080_DualLink;
		rate = VPIDPictureRate_6000;
		sampling = VPIDSampling_YUV_422;
		progressive = true;
		aspect16x9 = false;
		transport = false;
		break;
	case NTV2_FORMAT_1080p_5000_A:
		standard = VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_5000;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_1080p_5994_A:
		standard = VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_5994;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_1080p_6000_A:
		standard = VPIDStandard_1080_3Ga;
		rate = VPIDPictureRate_6000;
		progressive = true;
		aspect16x9 = false;
		transport = true;
		output3Ga = true;
		break;
	case NTV2_FORMAT_525_5994:
		standard = VPIDStandard_483_576;
		rate = VPIDPictureRate_2997;
		progressive = false;
		transport = false;
		break;
	case NTV2_FORMAT_525psf_2997:
		standard = VPIDStandard_483_576;
		rate = VPIDPictureRate_2997;
		progressive = true;
		transport = false;
		break;
	case NTV2_FORMAT_625_5000:
		standard = VPIDStandard_483_576;
		rate = VPIDPictureRate_2500;
		progressive = false;
		transport = false;
		break;
	case NTV2_FORMAT_625psf_2500:
		standard = VPIDStandard_483_576;
		rate = VPIDPictureRate_2500;
		progressive = true;
		transport = false;
		break;
	default:
		return false;
	}

	if(output3Gb)
	{
		switch(standard)
		{
		case VPIDStandard_720:
			standard = VPIDStandard_720_3Gb;
			break;
		case VPIDStandard_1080:
			standard = VPIDStandard_1080_3Gb;
			break;
		case VPIDStandard_483_576_1485Mbs:
			standard = VPIDStandard_483_576_3Gb;
			break;
		case VPIDStandard_1080_DualLink:
			standard = VPIDStandard_1080_DualLink_3Gb;
			break;
		case VPIDStandard_Unknown:
		case VPIDStandard_483_576:
		case VPIDStandard_483_576_DualLink:
		case VPIDStandard_483_576_540Mbs:
		case VPIDStandard_720_3Ga:
		case VPIDStandard_1080_3Ga:
		case VPIDStandard_1080_DualLink_3Gb:
		case VPIDStandard_720_3Gb:
		case VPIDStandard_1080_3Gb:
		case VPIDStandard_483_576_3Gb:
		case VPIDStandard_1080_QuadLink:
		default:
			break;
		}
	}

	uVPID |= ((ULWord)version << kRegShiftVPIDVersionID) & kRegMaskVPIDVersionID;
	uVPID |= ((ULWord)standard << kRegShiftVPIDStandard) & kRegMaskVPIDStandard;
	uVPID |= ((transport?1UL:0UL) << kRegShiftVPIDProgressiveTransport) & kRegMaskVPIDProgressiveTransport;
	uVPID |= ((progressive?1UL:0UL) << kRegShiftVPIDProgressivePicture) & kRegMaskVPIDProgressivePicture;
	uVPID |= ((aspect16x9?1UL:0UL) << kRegShiftVPIDImageAspect16x9) & kRegMaskVPIDImageAspect16x9;
	uVPID |= ((ULWord)rate << kRegShiftVPIDPictureRate) & kRegMaskVPIDPictureRate;
	uVPID |= ((ULWord)sampling << kRegShiftVPIDSampling) & kRegMaskVPIDSampling;
	uVPID |= ((is2K?1UL:0UL) << kRegShiftVPIDHorizontalSampling) & kRegMaskVPIDHorizontalSampling;

	if(NTV2_IS_4K_VIDEO_FORMAT(outputFormat) && dualLinkRGB)
		uVPID |= ((ULWord)SMPTEChannel << 5) & (kRegMaskVPIDChannel+BIT(5)); // if octalink
	else
		uVPID |= ((ULWord)SMPTEChannel << kRegShiftVPIDChannel) & kRegMaskVPIDChannel;
	uVPID |= ((ULWord)depth << kRegShiftVPIDBitDepth) & kRegMaskVPIDBitDepth;

	*pVPID = uVPID;

	return true;
}
