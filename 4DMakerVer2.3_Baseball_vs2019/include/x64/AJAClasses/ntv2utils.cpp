//
// Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
#include "ntv2utils.h"
#include "videodefines.h"
#include "audiodefines.h"
#include "ntv2endian.h"
#if defined(AJALinux)
	#include <string.h>  // For memset
	#include <stdint.h>
#elif defined(MSWindows)
	#include <windows.h>
#endif
#include <assert.h>
#include <sstream>
#include <iomanip>
#include <map>


using namespace std;

#if defined (NTV2_DEPRECATE)
	#define	AJA_LOCAL_STATIC	static
#else	//	!defined (NTV2_DEPRECATE)
	#define	AJA_LOCAL_STATIC
#endif	//	!defined (NTV2_DEPRECATE)


AJA_LOCAL_STATIC NTV2FormatDescriptor	formatDescriptorTable [NTV2_NUM_STANDARDS] [NTV2_FBF_NUMFRAMEBUFFERFORMATS] =
{
	// NTV2_STANDARD_1080
	{
		// NTV2_FBF_10BIT_YCBCR
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_YCBCRLINEPITCH_1080,
			0
		},
		// NTV2_FBF_8BIT_YCBCR
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080*2/4,
			0
		},
		// NTV2_FBF_ARGB
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},
		// NTV2_FBF_RGBA
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},
		// NTV2_FBF_10BIT_RGB
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},
		// NTV2_FBF_8BIT_YCBCR_YUY2
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080*2/4,
            0
		},
		// NTV2_FBF_ABGR
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},
		// NTV2_FBF_10BIT_DPX
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},
		// NTV2_FBF_10BIT_YCBCR_DPX
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_YCBCRLINEPITCH_1080,
			0
		},
		// NTV2_FBF_8BIT_DVCPRO
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080_DVCPRO,
			HD_YCBCRLINEPITCH_1080_DVCPRO,
			0
		},
		// NTV2_FBF_8BIT_QREZ
		{
			HD_NUMACTIVELINES_1080_QREZ,
			HD_NUMCOMPONENTPIXELS_1080_QREZ,
			HD_YCBCRLINEPITCH_1080_QREZ,
			0
		},
		// NTV2_FBF_8BIT_HDV
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080_HDV,
			HD_YCBCRLINEPITCH_1080_HDV,
            0
		},
		// NTV2_FBF_24BIT_RGB
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			RGB24LINEPITCH_1080,
			0
		},
		// NTV2_FBF_24BIT_BGR
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			RGB24LINEPITCH_1080,
			0
		},
		// NTV2_FBF_10BIT_YCBCRA
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},
		// NTV2_FBF_10BIT_DPX_LITTLEENDIAN
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},
		// NTV2_FBF_48BIT_RGB
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			RGB48LINEPITCH_1080,
			0
		},
		// NTV2_FBF_PRORES
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},
		// NTV2_FBF_PRORES_DVCPRO
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},
		// NTV2_FBF_PRORES_HDV
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},
		// NTV2_FBF_10BIT_RGB_PACKED
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},
		// NTV2_FBF_10BIT_ARGB
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080*4/3,
            0
		},
		// NTV2_FBF_16BIT_ARGB
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080*2,
			0
		},

		// NTV2_FBF_UNUSED_23
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RAW_RGB
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RAW_YCBCR
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

	},
	// NTV2_STANDARD_720
	{
		// NTV2_FBF_10BIT_YCBCR
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_YCBCRLINEPITCH_720,
			0
		},
		// NTV2_FBF_8BIT_YCBCR
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_NUMCOMPONENTPIXELS_720*2/4,
			0
		},
		// NTV2_FBF_ARGB
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_NUMCOMPONENTPIXELS_720,
			0
		},
		// NTV2_FBF_RGBA
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_NUMCOMPONENTPIXELS_720,
			0
		},

		// NTV2_FBF_10BIT_RGB
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_NUMCOMPONENTPIXELS_720,
			0
		},

		// NTV2_FBF_8BIT_YCBCR_YUY2
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_NUMCOMPONENTPIXELS_720*2/4,
			0
		},

		// NTV2_FBF_ABGR
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_NUMCOMPONENTPIXELS_720,
			0
		},

		// NTV2_FBF_10BIT_DPX
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_NUMCOMPONENTPIXELS_720,
			0
		},

		// NTV2_FBF_10BIT_YCBCR_DPX
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_YCBCRLINEPITCH_720,
			0
		},

		// NTV2_FBF_8BIT_DVCPRO
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720_DVCPRO,
			HD_YCBCRLINEPITCH_720_DVCPRO,
			0
		},

		// NTV2_FBF_8BIT_QREZ
		{
			HD_NUMACTIVELINES_720_QREZ,
			HD_NUMCOMPONENTPIXELS_720_QREZ,
			HD_YCBCRLINEPITCH_720_QREZ,
			0
		},

		// NTV2_FBF_8BIT_HDV
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720_HDV,
			HD_YCBCRLINEPITCH_720_HDV,
			0
		},

		// NTV2_FBF_24BIT_RGB
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			RGB24LINEPITCH_720,
			0
		},

		// NTV2_FBF_24BIT_BGR
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			RGB24LINEPITCH_720,
			0
		},

		// NTV2_FBF_10BIT_YCBCRA
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_NUMCOMPONENTPIXELS_720,
			0
		},

		// NTV2_FBF_10BIT_DPX_LITTLEENDIAN
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_NUMCOMPONENTPIXELS_720,
			0
		},

		// NTV2_FBF_48BIT_RGB
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			RGB48LINEPITCH_720,
			0
		},

		// NTV2_FBF_PRORES
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_PRORES_DVCPRO
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_PRORES_HDV
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RGB_PACKED
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_NUMCOMPONENTPIXELS_720,
			0
		},

		// NTV2_FBF_10BIT_ARGB
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_NUMCOMPONENTPIXELS_720*4/3,
			0
		},

		// NTV2_FBF_16BIT_ARGB
		{
			HD_NUMACTIVELINES_720,
			HD_NUMCOMPONENTPIXELS_720,
			HD_NUMCOMPONENTPIXELS_720*2,
			0
		},

		// NTV2_FBF_UNUSED_23
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RAW_RGB
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RAW_YCBCR
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

	},
	// NTV2_STANDARD_525
	{
		// NTV2_FBF_10BIT_YCBCR
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			YCBCRLINEPITCH_SD,
			0
		},

		// NTV2_FBF_8BIT_YCBCR
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS*2/4,
			0
		},

		// NTV2_FBF_ARGB
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_RGBA
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_10BIT_RGB
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_8BIT_YCBCR_YUY2
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS*2/4,
			0
		},

		// NTV2_FBF_ABGR
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_10BIT_DPX
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_10BIT_YCBCR_DPX
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			YCBCRLINEPITCH_SD,
			0
		},

		// NTV2_FBF_8BIT_DVCPRO
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_8BIT_QREZ
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_8BIT_HDV
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_24BIT_RGB
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			RGB24LINEPITCH_525,
			0
		},

		// NTV2_FBF_24BIT_BGR
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			RGB24LINEPITCH_525,
			0
		},

		// NTV2_FBF_10BIT_YCBCRA
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_10BIT_DPX_LITTLEENDIAN
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_48BIT_RGB
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			RGB48LINEPITCH_525,
			0
		},

		// NTV2_FBF_PRORES
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_PRORES_DVCPRO
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_PRORES_HDV
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},


		// NTV2_FBF_10BIT_RGB_PACKED
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_10BIT_ARGB
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS*4/3,
			0
		},

		// NTV2_FBF_16BIT_ARGB
		{
			NUMACTIVELINES_525,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS*2,
			0
		},

		// NTV2_FBF_UNUSED_23
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RAW_RGB
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RAW_YCBCR
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

	},
	// NTV2_STANDARD_625
	{
		// NTV2_FBF_10BIT_YCBCR
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			YCBCRLINEPITCH_SD,
			0
		},

		// NTV2_FBF_8BIT_YCBCR
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS*2/4,
			0
		},

		// NTV2_FBF_ARGB
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_RGBA
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_10BIT_RGB
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_8BIT_YCBCR_YUY2
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS*2/4,
			0
		},

		// NTV2_FBF_ABGR
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_10BIT_DPX
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_10BIT_YCBCR_DPX
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			YCBCRLINEPITCH_SD,
			0
		},

		// NTV2_FBF_8BIT_DVCPRO
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_8BIT_QREZ
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_8BIT_HDV
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_24BIT_RGB
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			RGB24LINEPITCH_625,
			0
		},

		// NTV2_FBF_24BIT_BGR
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			RGB24LINEPITCH_625,
			0
		},

		// NTV2_FBF_10BIT_YCBCRA
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_10BIT_DPX_LITTLEENDIAN
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_48BIT_RGB
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			RGB48LINEPITCH_625,
			0
		},

		// NTV2_FBF_PRORES
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_PRORES_DVCPRO
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_PRORES_HDV
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RGB_PACKED
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS,
			0
		},

		// NTV2_FBF_10BIT_ARGB
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS*4/3,
			0
		},

		// NTV2_FBF_16BIT_ARGB
		{
			NUMACTIVELINES_625,
			NUMCOMPONENTPIXELS,
			NUMCOMPONENTPIXELS*2,
			0
		},

		// NTV2_FBF_UNUSED_23
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RAW_RGB
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RAW_YCBCR
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

	},
	// NTV2_STANDARD_1080p
	{
		// NTV2_FBF_10BIT_YCBCR
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_YCBCRLINEPITCH_1080,
			0
		},

		// NTV2_FBF_8BIT_YCBCR
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080*2/4,
			0
		},

		// NTV2_FBF_ARGB
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},

		// NTV2_FBF_RGBA
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},

		// NTV2_FBF_10BIT_RGB
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},

		// NTV2_FBF_8BIT_YCBCR_YUY2
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080*2/4,
			0
		},

		// NTV2_FBF_ABGR
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},

		// NTV2_FBF_10BIT_DPX
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},

		// NTV2_FBF_10BIT_YCBCR_DPX
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_YCBCRLINEPITCH_1080,
			0
		},

		// NTV2_FBF_8BIT_DVCPRO
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080_DVCPRO,
			HD_YCBCRLINEPITCH_1080_DVCPRO,
			0
		},

		// NTV2_FBF_8BIT_QREZ
		{
			HD_NUMACTIVELINES_1080_QREZ,
			HD_NUMCOMPONENTPIXELS_1080_QREZ,
			HD_YCBCRLINEPITCH_1080_QREZ,
			0
		},

		// NTV2_FBF_8BIT_HDV
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080_HDV,
			HD_YCBCRLINEPITCH_1080_HDV,
			0
		},

		// NTV2_FBF_24BIT_RGB
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			RGB24LINEPITCH_1080,
			0
		},

		// NTV2_FBF_24BIT_BGR
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			RGB24LINEPITCH_1080,
			0
		},

		// NTV2_FBF_10BIT_YCBCRA
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},

		// NTV2_FBF_10BIT_DPX_LITTLEENDIAN
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},

		// NTV2_FBF_48BIT_RGB
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			RGB48LINEPITCH_1080,
			0
		},

		// NTV2_FBF_PRORES
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_PRORES_DVCPRO
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_PRORES_HDV
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RGB_PACKED
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			0
		},

		// NTV2_FBF_10BIT_ARGB
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080*4/3,
			0
		},

		// NTV2_FBF_16BIT_ARGB
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			HD_NUMCOMPONENTPIXELS_1080*2,
			0
		},

		// NTV2_FBF_UNUSED_23
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RAW_RGB
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			((HD_NUMCOMPONENTPIXELS_1080*10)/8)/4,
			0
		},

		// NTV2_FBF_10BIT_RAW_YCBCR
		{
			HD_NUMACTIVELINES_1080,
			HD_NUMCOMPONENTPIXELS_1080,
			((HD_NUMCOMPONENTPIXELS_1080*10)/8)/4,
			0
		},

	},
	// NTV2_STANDARD_2K
	{
		// NTV2_FBF_10BIT_YCBCR
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_YCBCRLINEPITCH_2K,
			0
		},

		// NTV2_FBF_8BIT_YCBCR
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_NUMCOMPONENTPIXELS_2K*2/4,
			0
		},

		// NTV2_FBF_ARGB
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			0
		},

		// NTV2_FBF_RGBA
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			0
		},

		// NTV2_FBF_10BIT_RGB
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			0
		},

		// NTV2_FBF_8BIT_YCBCR_YUY2
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_NUMCOMPONENTPIXELS_2K*2/4,
			0
		},

		// NTV2_FBF_ABGR
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			0
		},

		// NTV2_FBF_10BIT_DPX
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			0
		},

		// NTV2_FBF_10BIT_YCBCR_DPX
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			0
		},

		// NTV2_FBF_8BIT_DVCPRO
		{
			0,// NOT SUPPORTED
			0,// NOT SUPPORTED
			0,// NOT SUPPORTED
			0
		},

		// NTV2_FBF_8BIT_QREZ
		{
			0,// NOT SUPPORTED
			0,// NOT SUPPORTED
			0,// NOT SUPPORTED
			0
		},

		// NTV2_FBF_8BIT_HDV
		{
			0,// NOT SUPPORTED
			0,// NOT SUPPORTED
			0,// NOT SUPPORTED
			0
		},

		// NTV2_FBF_24BIT_RGB
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			RGB24LINEPITCH_2048,
			0
		},

		// NTV2_FBF_24BIT_BGR
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			RGB24LINEPITCH_2048,
			0
		},

		// NTV2_FBF_10BIT_YCBCRA
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			0
		},

		// NTV2_FBF_10BIT_DPX_LITTLEENDIAN
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			0
		},

		// NTV2_FBF_48BIT_RGB
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			RGB48LINEPITCH_2048,
			0
		},

		// NTV2_FBF_PRORES
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_PRORES_DVCPRO
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_PRORES_HDV
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RGB_PACKED
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			0
		},

		// NTV2_FBF_10BIT_ARGB
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_NUMCOMPONENTPIXELS_2K*4/3,
			0
		},

		// NTV2_FBF_16BIT_ARGB
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			HD_NUMCOMPONENTPIXELS_2K*2,
			0
		},

		// NTV2_FBF_UNUSED_23
		{
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0, // NOT SUPPORTED
			0
		},

		// NTV2_FBF_10BIT_RAW_RGB
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			((HD_NUMCOMPONENTPIXELS_2K*10)/8)/4,
			0
		},

		// NTV2_FBF_10BIT_RAW_YCBCR
		{
			HD_NUMACTIVELINES_2K,
			HD_NUMCOMPONENTPIXELS_2K,
			((HD_NUMCOMPONENTPIXELS_2K*10)/8)/4,
			0
		},

	}

};


void UnpackLine_10BitYUVto16BitYUV (const ULWord * pIn10BitYUVLine, UWord * pOut16BitYUVLine, const ULWord inNumPixels)
#if !defined (NTV2_DEPRECATE)
{
	::UnPackLineData (pIn10BitYUVLine, pOut16BitYUVLine, inNumPixels);
}

void UnPackLineData (const ULWord * pIn10BitYUVLine, UWord * pOut16BitYUVLine, const ULWord inNumPixels)
#endif	//	!defined (NTV2_DEPRECATE)
{
	assert (pIn10BitYUVLine && pOut16BitYUVLine && "UnpackLine_10BitYUVto16BitYUV -- NULL buffer pointer(s)");
	assert (inNumPixels && "UnpackLine_10BitYUVto16BitYUV -- Zero pixel count");

	for (ULWord outputCount = 0,  inputCount = 0;
		 outputCount < (inNumPixels * 2);
		 outputCount += 3,  inputCount++)
	{
		pOut16BitYUVLine [outputCount    ] =  pIn10BitYUVLine [inputCount]        & 0x3FF;
		pOut16BitYUVLine [outputCount + 1] = (pIn10BitYUVLine [inputCount] >> 10) & 0x3FF;
		pOut16BitYUVLine [outputCount + 2] = (pIn10BitYUVLine [inputCount] >> 20) & 0x3FF;
	}
}


void PackLine_16BitYUVto10BitYUV (const UWord * pIn16BitYUVLine, ULWord * pOut10BitYUVLine, const ULWord inNumPixels)
#if !defined (NTV2_DEPRECATE)
{
	::PackLineData (pIn16BitYUVLine, pOut10BitYUVLine, inNumPixels);
}

void PackLineData (const UWord * pIn16BitYUVLine, ULWord * pOut10BitYUVLine, const ULWord inNumPixels)
#endif	//	!defined (NTV2_DEPRECATE)
{
	assert (pIn16BitYUVLine && pOut10BitYUVLine && "PackLine_16BitYUVto10BitYUV -- NULL buffer pointer(s)");
	assert (inNumPixels && "PackLine_16BitYUVto10BitYUV -- Zero pixel count");

	for (ULWord inputCount = 0,  outputCount = 0;
		  inputCount < (inNumPixels * 2);
		  outputCount += 4,  inputCount += 12)
	{
		pOut10BitYUVLine [outputCount    ] = pIn16BitYUVLine [inputCount + 0] + (pIn16BitYUVLine [inputCount + 1] << 10) + (pIn16BitYUVLine [inputCount + 2] << 20);
		pOut10BitYUVLine [outputCount + 1] = pIn16BitYUVLine [inputCount + 3] + (pIn16BitYUVLine [inputCount + 4] << 10) + (pIn16BitYUVLine [inputCount + 5] << 20);
		pOut10BitYUVLine [outputCount + 2] = pIn16BitYUVLine [inputCount + 6] + (pIn16BitYUVLine [inputCount + 7] << 10) + (pIn16BitYUVLine [inputCount + 8] << 20);
		pOut10BitYUVLine [outputCount + 3] = pIn16BitYUVLine [inputCount + 9] + (pIn16BitYUVLine [inputCount +10] << 10) + (pIn16BitYUVLine [inputCount +11] << 20);
	}	//	for each component in the line
}


// RePackLineDataForYCbCrDPX
void RePackLineDataForYCbCrDPX(ULWord *packedycbcrLine, ULWord numULWords )
{
	for ( UWord count = 0; count < numULWords; count++)
	{
		ULWord value = (packedycbcrLine[count])<<2;
		value = (value<<24) + ((value>>24)&0x000000FF) + ((value<<8)&0x00FF0000) + ((value>>8)&0x0000FF00);

		packedycbcrLine[count] = value;
	}
}
// UnPack 10 Bit DPX Format linebuffer to RGBAlpha10BitPixel linebuffer.
void UnPack10BitDPXtoRGBAlpha10BitPixel(RGBAlpha10BitPixel* rgba10BitBuffer,ULWord* DPXLinebuffer ,ULWord numPixels, bool bigEndian)
{
	for ( ULWord pixel=0;pixel<numPixels;pixel++)
	{
		ULWord value = DPXLinebuffer[pixel];
		if ( bigEndian)
		{
			rgba10BitBuffer[pixel].Red = ((value&0xC0)>>14) + ((value&0xFF)<<2);
			rgba10BitBuffer[pixel].Green = ((value&0x3F00)>>4) + ((value&0xF00000)>>20);
			rgba10BitBuffer[pixel].Blue = ((value&0xFC000000)>>26) + ((value&0xF0000)>>12);
		}
		else
		{
			rgba10BitBuffer[pixel].Red = (value>>22)&0x3FF;
			rgba10BitBuffer[pixel].Green = (value>>12)&0x3FF;
			rgba10BitBuffer[pixel].Blue = (value>>2)&0x3FF;

		}
	}
}

void UnPack10BitDPXtoForRP215withEndianSwap(UWord* rawrp215Buffer,ULWord* DPXLinebuffer ,ULWord numPixels)
{
	// gets the green component.
	for ( ULWord pixel=0;pixel<numPixels;pixel++)
	{
		ULWord value = DPXLinebuffer[pixel];
		rawrp215Buffer[pixel] = ((value&0x3F00)>>4) + ((value&0xF00000)>>20);
	}
}

void UnPack10BitDPXtoForRP215(UWord* rawrp215Buffer,ULWord* DPXLinebuffer ,ULWord numPixels)
{
	// gets the green component.
	for ( ULWord pixel=0;pixel<numPixels;pixel++)
	{
		ULWord value = DPXLinebuffer[pixel];
		rawrp215Buffer[pixel] = ((value&0x3F)>>4) + ((value&0xF00000)>>20);
	}
}

// MaskYCbCrLine
// Mask Data In place based on signalMask
void MaskYCbCrLine(UWord* ycbcrLine, UWord signalMask , ULWord numPixels)
{
	ULWord pixelCount;

	// Not elegant but fairly fast.
	switch ( signalMask )
	{
	case NTV2_SIGNALMASK_NONE:          // Output Black
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrLine[pixelCount]   = CCIR601_10BIT_CHROMAOFFSET;     // Cb
			ycbcrLine[pixelCount+1] = CCIR601_10BIT_BLACK;            // Y
			ycbcrLine[pixelCount+2] = CCIR601_10BIT_CHROMAOFFSET;     // Cr
			ycbcrLine[pixelCount+3] = CCIR601_10BIT_BLACK;            // Y
		}
		break;
	case NTV2_SIGNALMASK_Y:
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrLine[pixelCount]   = CCIR601_10BIT_CHROMAOFFSET;     // Cb
			ycbcrLine[pixelCount+2] = CCIR601_10BIT_CHROMAOFFSET;     // Cr
		}

		break;
	case NTV2_SIGNALMASK_Cb:
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrLine[pixelCount+1] = CCIR601_10BIT_BLACK;            // Y
			ycbcrLine[pixelCount+2] = CCIR601_10BIT_CHROMAOFFSET;     // Cr
			ycbcrLine[pixelCount+3] = CCIR601_10BIT_BLACK;            // Y
		}

		break;
	case NTV2_SIGNALMASK_Y + NTV2_SIGNALMASK_Cb:
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrLine[pixelCount+2] = CCIR601_10BIT_CHROMAOFFSET;     // Cr
		}

		break;

	case NTV2_SIGNALMASK_Cr:
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrLine[pixelCount]   = CCIR601_10BIT_CHROMAOFFSET;     // Cb
			ycbcrLine[pixelCount+1] = CCIR601_10BIT_BLACK;            // Y
			ycbcrLine[pixelCount+3] = CCIR601_10BIT_BLACK;            // Y
		}


		break;
	case NTV2_SIGNALMASK_Y + NTV2_SIGNALMASK_Cr:
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrLine[pixelCount]   = CCIR601_10BIT_CHROMAOFFSET;     // Cb
		}


		break;
	case NTV2_SIGNALMASK_Cb + NTV2_SIGNALMASK_Cr:
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrLine[pixelCount+1] = CCIR601_10BIT_BLACK;            // Y
			ycbcrLine[pixelCount+3] = CCIR601_10BIT_BLACK;            // Y
		}


		break;
	case NTV2_SIGNALMASK_Y + NTV2_SIGNALMASK_Cb + NTV2_SIGNALMASK_Cr:
		// Do nothing
		break;
	}

}

void Make10BitBlackLine(UWord* lineData,UWord numPixels)
{
	// Assume 1080 format
	for ( int count = 0; count < numPixels*2; count+=2 )
	{
		lineData[count] = (UWord)CCIR601_10BIT_CHROMAOFFSET;
		lineData[count+1] = (UWord)CCIR601_10BIT_BLACK;
	}
}

void Make10BitWhiteLine(UWord* lineData,UWord numPixels)
{
	// assumes lineData is large enough for numPixels
	for ( int count = 0; count < numPixels*2; count+=2 )
	{
		lineData[count] = (UWord)CCIR601_10BIT_CHROMAOFFSET;
		lineData[count+1] = (UWord)CCIR601_10BIT_WHITE;
	}
}

void Make10BitLine(UWord* lineData, UWord Y , UWord Cb , UWord Cr,UWord numPixels)
{
	// assumes lineData is large enough for numPixels
	for ( int count = 0; count < numPixels*2; count+=4 )
	{
		lineData[count] = Cb;
		lineData[count+1] = Y;
		lineData[count+2] = Cr;
		lineData[count+3] = Y;
	}
}

void Fill10BitYCbCrVideoFrame(PULWord _baseVideoAddress,
							 NTV2Standard standard,
							 NTV2FrameBufferFormat frameBufferFormat,
							 YCbCr10BitPixel color,
							 bool vancEnabled,
							 bool twoKby1080,
							 bool wideVANC)
{
	NTV2FormatDescriptor fd = GetFormatDescriptor(standard,frameBufferFormat,vancEnabled,twoKby1080,wideVANC);
	UWord lineBuffer[2048*2];
	Make10BitLine(lineBuffer,color.y,color.cb,color.cr,fd.numPixels);
	for ( UWord i= 0; i<fd.numLines; i++)
	{
		::PackLine_16BitYUVto10BitYUV(lineBuffer,_baseVideoAddress,fd.numPixels);
		_baseVideoAddress += fd.linePitch;
	}
}

void Make8BitBlackLine(UByte* lineData,UWord numPixels,NTV2FrameBufferFormat fbFormat)
{
	// assumes lineData is large enough for numPixels
	if ( fbFormat == NTV2_FBF_8BIT_YCBCR )
	{
		for ( int count = 0; count < numPixels*2; count+=2 )
		{
			lineData[count] = (UWord)CCIR601_8BIT_CHROMAOFFSET;
			lineData[count+1] = (UWord)CCIR601_8BIT_BLACK;
		}
	}
	else
	{
		// NTV2_FBF_8BIT_YCBCR_YUY2
		for ( int count = 0; count < numPixels*2; count+=2 )
		{
			lineData[count] = (UWord)CCIR601_8BIT_BLACK;
			lineData[count+1] = (UWord)CCIR601_8BIT_CHROMAOFFSET;
		}
	}
}

void Make8BitWhiteLine(UByte* lineData,UWord numPixels,NTV2FrameBufferFormat fbFormat)
{
	// assumes lineData is large enough for numPixels
	// assumes lineData is large enough for numPixels
	if ( fbFormat == NTV2_FBF_8BIT_YCBCR )
	{
		for ( int count = 0; count < numPixels*2; count+=2 )
		{
			lineData[count] = (UWord)CCIR601_8BIT_CHROMAOFFSET;
			lineData[count+1] = (UWord)CCIR601_8BIT_WHITE;
		}
	}
	else
	{
		// NTV2_FBF_8BIT_YCBCR_YUY2
		for ( int count = 0; count < numPixels*2; count+=2 )
		{
			lineData[count] = (UWord)CCIR601_8BIT_WHITE;
			lineData[count+1] = (UWord)CCIR601_8BIT_CHROMAOFFSET;
		}
	}

}

void Make8BitLine(UByte* lineData, UByte Y , UByte Cb , UByte Cr,ULWord numPixels,NTV2FrameBufferFormat fbFormat)
{
	// assumes lineData is large enough for numPixels

	if ( fbFormat == NTV2_FBF_8BIT_YCBCR )
	{
		for ( ULWord count = 0; count < numPixels*2; count+=4 )
		{
			lineData[count] = Cb;
			lineData[count+1] = Y;
			lineData[count+2] = Cr;
			lineData[count+3] = Y;
		}
	}
	else
	{
		for ( ULWord count = 0; count < numPixels*2; count+=4 )
		{
			lineData[count] = Y;
			lineData[count+1] = Cb;
			lineData[count+2] = Y;
			lineData[count+3] = Cr;
		}

	}
}

void Fill8BitYCbCrVideoFrame(PULWord _baseVideoAddress,
							 NTV2Standard standard,
							 NTV2FrameBufferFormat frameBufferFormat,
							 YCbCrPixel color,
							 bool vancEnabled,
							 bool twoKby1080,
							 bool wideVANC)
{
	NTV2FormatDescriptor fd = GetFormatDescriptor(standard,frameBufferFormat,vancEnabled,twoKby1080,wideVANC);

	for ( UWord i= 0; i<fd.numLines; i++)
	{
		Make8BitLine((UByte*)_baseVideoAddress,color.y,color.cb,color.cr,fd.numPixels,frameBufferFormat);
		_baseVideoAddress += fd.linePitch;
	}
}

void Fill4k8BitYCbCrVideoFrame(PULWord _baseVideoAddress,
							 NTV2FrameBufferFormat frameBufferFormat,
							 YCbCrPixel color,
							 bool vancEnabled,
							 bool b4k,
							 bool wideVANC)
{
	(void) vancEnabled;
	(void) wideVANC;
	NTV2FormatDescriptor fd;
	if(b4k)
	{
		fd.numLines = 2160;
		fd.numPixels = 4096;
		fd.firstActiveLine = 0;
		fd.linePitch = 4096*2/4;
	}
	else
	{
		fd.numLines = 2160;
		fd.numPixels = 3840;
		fd.firstActiveLine = 0;
		fd.linePitch = 3840*2/4;
	}

	Make8BitLine((UByte*)_baseVideoAddress,color.y,color.cb,color.cr,fd.numPixels*fd.numLines,frameBufferFormat);
}


// Copy arbrary-sized source image buffer to arbitrary-sized destination frame buffer.
// It will automatically clip and/or pad the source image to center it in the destination frame.
// This will work with any RGBA/RGB frame buffer formats with 4 Bytes/pixel size
void CopyRGBAImageToFrame(ULWord* pSrcBuffer, ULWord srcWidth, ULWord srcHeight,
						  ULWord* pDstBuffer, ULWord dstWidth, ULWord dstHeight)
{
	// all variables are in pixels
	ULWord topPad = 0, bottomPad = 0, leftPad = 0, rightPad = 0;
	ULWord contentHeight = 0;
	ULWord contentWidth = 0;
	ULWord* pSrc = pSrcBuffer;
	ULWord* pDst = pDstBuffer;

	if (dstHeight > srcHeight)
	{
		topPad = (dstHeight - srcHeight) / 2;
		bottomPad = dstHeight - topPad - srcHeight;
	}
	else
		pSrc += ((srcHeight - dstHeight) / 2) * srcWidth;

	if (dstWidth > srcWidth)
	{
		leftPad = (dstWidth - srcWidth) / 2;
		rightPad = dstWidth - srcWidth - leftPad;
	}
	else
		pSrc += (srcWidth - dstWidth) / 2;

	// content
	contentHeight = dstHeight - topPad - bottomPad;
	contentWidth = dstWidth - leftPad - rightPad;

	// top pad
	memset(pDst, 0, topPad * dstWidth * 4);
	pDst += topPad * dstWidth;

	// content
	while (contentHeight--)
	{
		// left
		memset(pDst, 0, leftPad * 4);
		pDst += leftPad;

		// content
		memcpy(pDst, pSrc, contentWidth * 4);
		pDst += contentWidth;
		pSrc += srcWidth;

		// right
		memset(pDst, 0, rightPad * 4);
		pDst += rightPad;
	}

	// bottom pad
	memset(pDst, 0, bottomPad * dstWidth * 4);
}


// GetFramesPerSecond(NTV2FrameRate frameRate)
// seconds per frame.
double GetFramesPerSecond(NTV2FrameRate frameRate)
{
	double framesPerSecond;

    switch (frameRate)
    {
    case NTV2_FRAMERATE_12000:
        framesPerSecond = 120.0;
        break;
    case NTV2_FRAMERATE_11988:
        framesPerSecond = 120.0/1.001;
        break;
    case NTV2_FRAMERATE_6000:
        framesPerSecond = 60.0;
        break;
    case NTV2_FRAMERATE_5994:
        framesPerSecond = 60.0/1.001;
        break;
    case NTV2_FRAMERATE_5000:
        framesPerSecond = 50.0;
        break;
    case NTV2_FRAMERATE_4800:
        framesPerSecond = 48.0;
        break;
    case NTV2_FRAMERATE_4795:
        framesPerSecond = 48.0/1.001;
        break;
    case NTV2_FRAMERATE_3000:
        framesPerSecond = 30.0;
        break;
    case NTV2_FRAMERATE_2997:
	default:
        framesPerSecond = 30.0/1.001;
        break;
    case NTV2_FRAMERATE_2500:
        framesPerSecond = 25.0;
        break;
    case NTV2_FRAMERATE_2400:
        framesPerSecond = 24.0;
        break;
    case NTV2_FRAMERATE_2398:
        framesPerSecond = 24.0/1.001;
        break;
    case NTV2_FRAMERATE_1500:
        framesPerSecond = 15.0;
        break;
    case NTV2_FRAMERATE_1498:
        framesPerSecond = 15.0/1.001;
        break;
	}

	return framesPerSecond;
}

// GetFrameTime(NTV2FrameRate frameRate)
// seconds per frame.
double GetFrameTime(NTV2FrameRate frameRate)
{
	double frameTime;

    switch (frameRate)
    {
    case NTV2_FRAMERATE_12000:
        frameTime = 1.0/120.0;
        break;
    case NTV2_FRAMERATE_11988:
        frameTime = 1.001/120.0;
        break;
    case NTV2_FRAMERATE_6000:
        frameTime = 1.0/60.0;
        break;
    case NTV2_FRAMERATE_5994:
        frameTime = 1.001/60.0;
        break;
	case NTV2_FRAMERATE_5000:
		frameTime = 1.0/50.0;
		break;
    case NTV2_FRAMERATE_4800:
        frameTime = 1.0/48.0;
        break;
    case NTV2_FRAMERATE_4795:
        frameTime = 1.001/48.0;
        break;
    case NTV2_FRAMERATE_3000:
        frameTime = 1.0/30.0;
        break;
    case NTV2_FRAMERATE_2997:
	default:
        frameTime = 1.001/30.0;
        break;
    case NTV2_FRAMERATE_2500:
        frameTime = 1.0/25.0;
        break;
    case NTV2_FRAMERATE_2400:
        frameTime = 1.0/24.0;
        break;
    case NTV2_FRAMERATE_2398:
        frameTime = 1.001/24.0;
        break;
    case NTV2_FRAMERATE_1500:
        frameTime = 1.0/15.0;
        break;
    case NTV2_FRAMERATE_1498:
        frameTime = 1.001/15.0;
        break;
	}

	return frameTime;
}

NTV2Standard GetNTV2StandardFromScanGeometry(UByte geometry, bool progressiveTransport)
{
	NTV2Standard standard = NTV2_NUM_STANDARDS;

	switch ( geometry )
	{
	case 1:
		standard = NTV2_STANDARD_525;
		break;
	case 2:
		standard = NTV2_STANDARD_625;
		break;
	case 3:
		standard = NTV2_STANDARD_720;
		break;
	case 4:
	case 8:
		if ( progressiveTransport )
			standard = NTV2_STANDARD_1080p;
		else
			standard = NTV2_STANDARD_1080;
		break;
	case 9:
		standard = NTV2_STANDARD_2K;
		break;
	}

	return standard;
}


NTV2VideoFormat GetQuarterSizedVideoFormat(NTV2VideoFormat videoFormat)
{
	NTV2VideoFormat quaterSizedFormat;

	switch (videoFormat)
	{
		case NTV2_FORMAT_4x1920x1080psf_2398:	quaterSizedFormat = NTV2_FORMAT_1080psf_2398;   break;
		case NTV2_FORMAT_4x1920x1080psf_2400:	quaterSizedFormat = NTV2_FORMAT_1080psf_2400;   break;
		case NTV2_FORMAT_4x1920x1080psf_2500:	quaterSizedFormat = NTV2_FORMAT_1080psf_2500_2; break;
		case NTV2_FORMAT_4x1920x1080psf_2997:	quaterSizedFormat = NTV2_FORMAT_1080psf_2997;   break;
		case NTV2_FORMAT_4x1920x1080psf_3000:	quaterSizedFormat = NTV2_FORMAT_1080psf_3000;   break;
            
		case NTV2_FORMAT_4x2048x1080psf_2398:	quaterSizedFormat = NTV2_FORMAT_1080psf_2K_2398; break;
		case NTV2_FORMAT_4x2048x1080psf_2400:	quaterSizedFormat = NTV2_FORMAT_1080psf_2K_2400; break;
		case NTV2_FORMAT_4x2048x1080psf_2500:	quaterSizedFormat = NTV2_FORMAT_1080psf_2K_2500; break;
		//case NTV2_FORMAT_4x2048x1080psf_2997:	quaterSizedFormat = NTV2_FORMAT_1080psf_2K_2997; break;
		//case NTV2_FORMAT_4x2048x1080psf_3000:	quaterSizedFormat = NTV2_FORMAT_1080psf_2K_3000; break;
            
		case NTV2_FORMAT_4x1920x1080p_2398:		quaterSizedFormat = NTV2_FORMAT_1080p_2398; break;
		case NTV2_FORMAT_4x1920x1080p_2400:		quaterSizedFormat = NTV2_FORMAT_1080p_2400; break;
		case NTV2_FORMAT_4x1920x1080p_2500:		quaterSizedFormat = NTV2_FORMAT_1080p_2500; break;
		case NTV2_FORMAT_4x1920x1080p_2997:		quaterSizedFormat = NTV2_FORMAT_1080p_2997; break;
		case NTV2_FORMAT_4x1920x1080p_3000:		quaterSizedFormat = NTV2_FORMAT_1080p_3000; break;
		case NTV2_FORMAT_4x1920x1080p_5000:		quaterSizedFormat = NTV2_FORMAT_1080p_5000_A; break;
		case NTV2_FORMAT_4x1920x1080p_5994:		quaterSizedFormat = NTV2_FORMAT_1080p_5994_A; break;
		case NTV2_FORMAT_4x1920x1080p_6000:		quaterSizedFormat = NTV2_FORMAT_1080p_6000_A; break;
            
		case NTV2_FORMAT_4x2048x1080p_2398:		quaterSizedFormat = NTV2_FORMAT_1080p_2K_2398; break;
		case NTV2_FORMAT_4x2048x1080p_2400:		quaterSizedFormat = NTV2_FORMAT_1080p_2K_2400; break;
		case NTV2_FORMAT_4x2048x1080p_2500:		quaterSizedFormat = NTV2_FORMAT_1080p_2K_2500; break;
		case NTV2_FORMAT_4x2048x1080p_2997:		quaterSizedFormat = NTV2_FORMAT_1080p_2K_2997; break;
		case NTV2_FORMAT_4x2048x1080p_3000:		quaterSizedFormat = NTV2_FORMAT_1080p_2K_3000; break;
		case NTV2_FORMAT_4x2048x1080p_4795:		quaterSizedFormat = NTV2_FORMAT_1080p_2K_4795; break;
		case NTV2_FORMAT_4x2048x1080p_4800:		quaterSizedFormat = NTV2_FORMAT_1080p_2K_4800; break;
		case NTV2_FORMAT_4x2048x1080p_5000:		quaterSizedFormat = NTV2_FORMAT_1080p_2K_5000_A; break;
		case NTV2_FORMAT_4x2048x1080p_5994:		quaterSizedFormat = NTV2_FORMAT_1080p_2K_5994_A; break;
		case NTV2_FORMAT_4x2048x1080p_6000:		quaterSizedFormat = NTV2_FORMAT_1080p_2K_6000_A; break;
		// No quarter sized formats for 119.88 or 120 Hz

		default:								quaterSizedFormat = videoFormat; break;
	}

	return quaterSizedFormat;
}


NTV2FrameGeometry GetQuarterSizedGeometry(NTV2FrameGeometry geometry)
{
	switch ( geometry )
	{
		case NTV2_FG_4x1920x1080:
			return NTV2_FG_1920x1080;
		case NTV2_FG_4x2048x1080:
			return NTV2_FG_2048x1080;
		default:
			return geometry;
	}
}


NTV2FrameGeometry Get4xSizedGeometry(NTV2FrameGeometry geometry)
{
	switch ( geometry )
	{
		case NTV2_FG_1920x1080:
			return NTV2_FG_4x1920x1080;
		case NTV2_FG_2048x1080:
			return NTV2_FG_4x2048x1080;
		default:
			return geometry;
	}
}



NTV2Standard GetNTV2StandardFromVideoFormat(NTV2VideoFormat videoFormat)
{
	NTV2Standard standard=NTV2_NUM_STANDARDS;

	switch ( videoFormat )
    {
    case NTV2_FORMAT_1080i_5000:
    case NTV2_FORMAT_1080i_5994:
    case NTV2_FORMAT_1080i_6000:
	case NTV2_FORMAT_1080p_5000_B:
	case NTV2_FORMAT_1080p_5994_B:
	case NTV2_FORMAT_1080p_6000_B:
    case NTV2_FORMAT_1080psf_2398:
    case NTV2_FORMAT_1080psf_2400:
    case NTV2_FORMAT_1080psf_2500_2:
    case NTV2_FORMAT_1080psf_2997_2:
    case NTV2_FORMAT_1080psf_3000_2:
	case NTV2_FORMAT_1080psf_2K_2398:
	case NTV2_FORMAT_1080psf_2K_2400:
	case NTV2_FORMAT_1080psf_2K_2500:
	case NTV2_FORMAT_4x1920x1080psf_2398:
	case NTV2_FORMAT_4x1920x1080psf_2400:
	case NTV2_FORMAT_4x1920x1080psf_2500:
	case NTV2_FORMAT_4x1920x1080psf_2997:
	case NTV2_FORMAT_4x1920x1080psf_3000:
	case NTV2_FORMAT_4x2048x1080psf_2398:
	case NTV2_FORMAT_4x2048x1080psf_2400:
	case NTV2_FORMAT_4x2048x1080psf_2500:
	case NTV2_FORMAT_4x2048x1080psf_2997:
	case NTV2_FORMAT_4x2048x1080psf_3000:
		standard = NTV2_STANDARD_1080;
		break;
    case NTV2_FORMAT_1080p_2500:
    case NTV2_FORMAT_1080p_2997:
    case NTV2_FORMAT_1080p_3000:
    case NTV2_FORMAT_1080p_2398:
    case NTV2_FORMAT_1080p_2400:
	case NTV2_FORMAT_1080p_2K_2398:
	case NTV2_FORMAT_1080p_2K_2400:
	case NTV2_FORMAT_1080p_2K_2500:
	case NTV2_FORMAT_1080p_2K_2997:
	case NTV2_FORMAT_1080p_2K_3000:
	case NTV2_FORMAT_1080p_2K_4795:
	case NTV2_FORMAT_1080p_2K_4800:
	case NTV2_FORMAT_1080p_2K_5000:
	case NTV2_FORMAT_1080p_2K_5994:
	case NTV2_FORMAT_1080p_2K_6000:
	case NTV2_FORMAT_1080p_5000_A:
	case NTV2_FORMAT_1080p_5994_A:
	case NTV2_FORMAT_1080p_6000_A:
	case NTV2_FORMAT_4x1920x1080p_2398:
	case NTV2_FORMAT_4x1920x1080p_2400:
	case NTV2_FORMAT_4x1920x1080p_2500:
	case NTV2_FORMAT_4x2048x1080p_2398:
	case NTV2_FORMAT_4x2048x1080p_2400:
	case NTV2_FORMAT_4x2048x1080p_2500:
	case NTV2_FORMAT_4x1920x1080p_2997:
	case NTV2_FORMAT_4x1920x1080p_3000:
	case NTV2_FORMAT_4x2048x1080p_2997:
	case NTV2_FORMAT_4x2048x1080p_3000:
	case NTV2_FORMAT_4x2048x1080p_4795:
	case NTV2_FORMAT_4x2048x1080p_4800:
	case NTV2_FORMAT_4x1920x1080p_5000:
	case NTV2_FORMAT_4x1920x1080p_5994:
	case NTV2_FORMAT_4x1920x1080p_6000:
	case NTV2_FORMAT_4x2048x1080p_5000:
	case NTV2_FORMAT_4x2048x1080p_5994:
	case NTV2_FORMAT_4x2048x1080p_6000:
	case NTV2_FORMAT_4x2048x1080p_11988:
	case NTV2_FORMAT_4x2048x1080p_12000:
		standard = NTV2_STANDARD_1080p;
        break;
	case NTV2_FORMAT_720p_2398:
	case NTV2_FORMAT_720p_5000:
    case NTV2_FORMAT_720p_5994:
    case NTV2_FORMAT_720p_6000:
	case NTV2_FORMAT_720p_2500:
		standard = NTV2_STANDARD_720;
		break;
    case NTV2_FORMAT_525_5994:
	case NTV2_FORMAT_525_2398:
	case NTV2_FORMAT_525_2400:
	case NTV2_FORMAT_525psf_2997:
		standard = NTV2_STANDARD_525;
        break;
    case NTV2_FORMAT_625_5000:
	case NTV2_FORMAT_625psf_2500:
		standard = NTV2_STANDARD_625 ;
		break;
	case NTV2_FORMAT_2K_1498:
	case NTV2_FORMAT_2K_1500:
	case NTV2_FORMAT_2K_2398:
	case NTV2_FORMAT_2K_2400:
	case NTV2_FORMAT_2K_2500:
		standard = NTV2_STANDARD_2K ;
		break;
	default:
		standard = NTV2_NUM_STANDARDS;
		break;	// Unsupported

    }

	return standard;
}

NTV2V2Standard GetHdmiV2StandardFromVideoFormat(NTV2VideoFormat videoFormat)
{
	NTV2V2Standard standard = NTV2_V2_STANDARD_UNDEFINED;
	
	switch (videoFormat)
    {
		case NTV2_FORMAT_1080i_5000:
		case NTV2_FORMAT_1080i_5994:
		case NTV2_FORMAT_1080i_6000:
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080psf_2400:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_3000_2:
			standard = NTV2_V2_STANDARD_1080;
			break;
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
		case NTV2_FORMAT_1080p_5000_B:
		case NTV2_FORMAT_1080p_5994_B:
		case NTV2_FORMAT_1080p_6000_B:
			standard = NTV2_V2_STANDARD_1080p;
			break;
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080p_2K_2997:
		case NTV2_FORMAT_1080p_2K_3000:
		case NTV2_FORMAT_1080p_2K_4795:
		case NTV2_FORMAT_1080p_2K_4800:
		case NTV2_FORMAT_1080p_2K_5000:
		case NTV2_FORMAT_1080p_2K_5994:
		case NTV2_FORMAT_1080p_2K_6000:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2500:
			standard = NTV2_V2_STANDARD_2Kx1080p;
			break;
		case NTV2_FORMAT_720p_2398:
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_720p_5994:
		case NTV2_FORMAT_720p_6000:
		case NTV2_FORMAT_720p_2500:
			standard = NTV2_V2_STANDARD_720;
			break;
		case NTV2_FORMAT_525_5994:
		case NTV2_FORMAT_525_2398:
		case NTV2_FORMAT_525_2400:
		case NTV2_FORMAT_525psf_2997:
			standard = NTV2_V2_STANDARD_525;
			break;
		case NTV2_FORMAT_625_5000:
		case NTV2_FORMAT_625psf_2500:
			standard = NTV2_V2_STANDARD_625 ;
			break;
		case NTV2_FORMAT_2K_1498:
		case NTV2_FORMAT_2K_1500:
		case NTV2_FORMAT_2K_2398:
		case NTV2_FORMAT_2K_2400:
		case NTV2_FORMAT_2K_2500:
			standard = NTV2_V2_STANDARD_2K ;
			break;
		case NTV2_FORMAT_4x1920x1080psf_2398:
		case NTV2_FORMAT_4x1920x1080psf_2400:
		case NTV2_FORMAT_4x1920x1080psf_2500:
		case NTV2_FORMAT_4x1920x1080psf_2997:
		case NTV2_FORMAT_4x1920x1080psf_3000:
		case NTV2_FORMAT_4x1920x1080p_2398:
		case NTV2_FORMAT_4x1920x1080p_2400:
		case NTV2_FORMAT_4x1920x1080p_2500:
		case NTV2_FORMAT_4x1920x1080p_2997:
		case NTV2_FORMAT_4x1920x1080p_3000:
			standard = NTV2_V2_STANDARD_3840x2160p;
			break;
		case NTV2_FORMAT_4x1920x1080p_5000:
		case NTV2_FORMAT_4x1920x1080p_5994:
		case NTV2_FORMAT_4x1920x1080p_6000:
			standard = NTV2_V2_STANDARD_3840HFR;
			break;
		case NTV2_FORMAT_4x2048x1080psf_2398:
		case NTV2_FORMAT_4x2048x1080psf_2400:
		case NTV2_FORMAT_4x2048x1080psf_2500:
		case NTV2_FORMAT_4x2048x1080psf_2997:
		case NTV2_FORMAT_4x2048x1080psf_3000:
		case NTV2_FORMAT_4x2048x1080p_2398:
		case NTV2_FORMAT_4x2048x1080p_2400:
		case NTV2_FORMAT_4x2048x1080p_2500:
		case NTV2_FORMAT_4x2048x1080p_2997:
		case NTV2_FORMAT_4x2048x1080p_3000:
		case NTV2_FORMAT_4x2048x1080p_4795:
		case NTV2_FORMAT_4x2048x1080p_4800:
			standard = NTV2_V2_STANDARD_4096x2160p;
			break;
		case NTV2_FORMAT_4x2048x1080p_5000:
		case NTV2_FORMAT_4x2048x1080p_5994:
		case NTV2_FORMAT_4x2048x1080p_6000:
			standard = NTV2_V2_STANDARD_4096HFR;
			break;
		default:
			standard = NTV2_V2_STANDARD_UNDEFINED;
			break;	// Unsupported
    }
	
	return standard;
}


// GetVideoActiveSize: returns the number of bytes of active video
// TODO:
// NOTE: Throwing an exception might be a better thing to do here than asserting.
ULWord GetVideoActiveSize(NTV2VideoFormat videoFormat,NTV2FrameBufferFormat format,bool VANCenabled,bool wideVANC )
{
	NTV2Standard standard = GetNTV2StandardFromVideoFormat(videoFormat);
	if ( standard == NTV2_NUM_STANDARDS)
		return 0;

	bool twoKby1080=false;
	switch ( videoFormat )
	{
	case NTV2_FORMAT_1080psf_2K_2398:
	case NTV2_FORMAT_1080psf_2K_2400:
	case NTV2_FORMAT_1080psf_2K_2500:
	case NTV2_FORMAT_1080p_2K_2398:
	case NTV2_FORMAT_1080p_2K_2400:
	case NTV2_FORMAT_1080p_2K_2500:
	case NTV2_FORMAT_1080p_2K_2997:
	case NTV2_FORMAT_1080p_2K_3000:
	case NTV2_FORMAT_1080p_2K_4795:
	case NTV2_FORMAT_1080p_2K_4800:
	case NTV2_FORMAT_1080p_2K_5000:
	case NTV2_FORMAT_1080p_2K_5994:
	case NTV2_FORMAT_1080p_2K_6000:
	case NTV2_FORMAT_4x2048x1080psf_2398:
	case NTV2_FORMAT_4x2048x1080psf_2400:
	case NTV2_FORMAT_4x2048x1080psf_2500:
	case NTV2_FORMAT_4x2048x1080p_2398:
	case NTV2_FORMAT_4x2048x1080p_2400:
	case NTV2_FORMAT_4x2048x1080p_2500:
	case NTV2_FORMAT_4x2048x1080p_2997:
	case NTV2_FORMAT_4x2048x1080p_3000:
	case NTV2_FORMAT_4x2048x1080p_4795:
	case NTV2_FORMAT_4x2048x1080p_4800:
	case NTV2_FORMAT_4x2048x1080p_5000:
	case NTV2_FORMAT_4x2048x1080p_5994:
	case NTV2_FORMAT_4x2048x1080p_6000:
		twoKby1080 = true;
	break;
	default:
		twoKby1080 = false;
	break;
	}

	uint32_t fourKMultiplier = 1;
	switch ( videoFormat )
	{
	case NTV2_FORMAT_4x2048x1080psf_2398:
	case NTV2_FORMAT_4x2048x1080psf_2400:
	case NTV2_FORMAT_4x2048x1080psf_2500:
	case NTV2_FORMAT_4x2048x1080p_2398:
	case NTV2_FORMAT_4x2048x1080p_2400:
	case NTV2_FORMAT_4x2048x1080p_2500:
	case NTV2_FORMAT_4x1920x1080psf_2398:	
	case NTV2_FORMAT_4x1920x1080psf_2400:	
	case NTV2_FORMAT_4x1920x1080psf_2500:	
	case NTV2_FORMAT_4x1920x1080p_2398:		
	case NTV2_FORMAT_4x1920x1080p_2400:		
	case NTV2_FORMAT_4x1920x1080p_2500:
	case NTV2_FORMAT_4x1920x1080p_2997:
	case NTV2_FORMAT_4x1920x1080p_3000:
	case NTV2_FORMAT_4x1920x1080p_5000:
	case NTV2_FORMAT_4x1920x1080p_5994:
	case NTV2_FORMAT_4x1920x1080p_6000:
	case NTV2_FORMAT_4x1920x1080psf_2997:
	case NTV2_FORMAT_4x1920x1080psf_3000:
	case NTV2_FORMAT_4x2048x1080p_2997:
	case NTV2_FORMAT_4x2048x1080p_3000:
	case NTV2_FORMAT_4x2048x1080psf_2997:
	case NTV2_FORMAT_4x2048x1080psf_3000:
	case NTV2_FORMAT_4x2048x1080p_4795:
	case NTV2_FORMAT_4x2048x1080p_4800:
	case NTV2_FORMAT_4x2048x1080p_5000:
	case NTV2_FORMAT_4x2048x1080p_5994:
	case NTV2_FORMAT_4x2048x1080p_6000:
		fourKMultiplier = 4;
		break;
	default:
		fourKMultiplier = 1;
		break;
	}	
	NTV2FormatDescriptor fd = GetFormatDescriptor(standard,format,VANCenabled,twoKby1080,wideVANC);

    return fd.linePitch*fd.numLines*4*fourKMultiplier;
}


// GetVideoWriteSize
// At least in Windows, to get bursting to work between our board and the disk
// system  without going through the file manager cache, you need to open the file
// with FILE_FLAG_NO_BUFFERING flag. With this you must do reads and writes
// on 4096 byte boundaries with most modern disk systems. You could actually
// do 512 on some systems though.
// So this function takes in the videoformat and the framebufferformat
// and gets the framesize you need to write to meet this requirement.
//
ULWord GetVideoWriteSize(NTV2VideoFormat videoFormat, NTV2FrameBufferFormat format,bool VANCenabled,bool wideVANC )
{
	if (videoFormat == NTV2_FORMAT_UNKNOWN)
		return 0;

    ULWord ulSize = GetVideoActiveSize (videoFormat, format,VANCenabled,wideVANC);
    if (ulSize % 4096)
        ulSize = ((ulSize / 4096) + 1) * 4096;

    return ulSize;
}

// GetAudioSamplesPerFrame(NTV2FrameRate frameRate, NTV2AudioRate audioRate)
// For a given framerate and audiorate, returns how many audio samples there
// will be in a frame's time. cadenceFrame is only used for 5994 or 2997 @ 48k.
// smpte372Enabled indicates that you are doing 1080p60,1080p5994 or 1080p50
// in this mode the boards framerate might be NTV2_FRAMERATE_3000, but since
// 2 links are coming out, the video rate is actually NTV2_FRAMERATE_6000
ULWord GetAudioSamplesPerFrame(NTV2FrameRate frameRate, NTV2AudioRate audioRate, ULWord cadenceFrame,bool smpte372Enabled)
{
	ULWord audioSamplesPerFrame=0;
	cadenceFrame %= 5;

	if( smpte372Enabled )
	{
		// the video is actually coming out twice as fast as the board rate
		// since there are 2 links.
		switch ( frameRate )
		{
		case NTV2_FRAMERATE_3000:
			frameRate = NTV2_FRAMERATE_6000;
			break;
		case NTV2_FRAMERATE_2997:
			frameRate = NTV2_FRAMERATE_5994;
			break;
		case NTV2_FRAMERATE_2500:
			frameRate = NTV2_FRAMERATE_5000;
			break;
		case NTV2_FRAMERATE_2400:
			frameRate = NTV2_FRAMERATE_4800;
			break;
		case NTV2_FRAMERATE_2398:
			frameRate = NTV2_FRAMERATE_4795;
			break;
		default:
			break;
		}
	}

	if ( audioRate == NTV2_AUDIO_48K)
	{
		switch ( frameRate)
		{
			case NTV2_FRAMERATE_12000:
				audioSamplesPerFrame = 400;
				break;
			case NTV2_FRAMERATE_11988:
				switch ( cadenceFrame )
				{
				case 0:
				case 2:
				case 4:
					audioSamplesPerFrame = 400;
					break;
				case 1:
				case 3:
					audioSamplesPerFrame = 401;
					break;
				}
				break;
			case NTV2_FRAMERATE_6000:
				audioSamplesPerFrame = 800;
				break;
			case NTV2_FRAMERATE_5994:
				switch ( cadenceFrame )
				{
				case 0:
					audioSamplesPerFrame = 800;
					break;
				case 1:
				case 2:
				case 3:
				case 4:
					audioSamplesPerFrame = 801;
					break;
				}
				break;
			case NTV2_FRAMERATE_5000:
				audioSamplesPerFrame = 1920/2;
				break;
			case NTV2_FRAMERATE_4800:
				audioSamplesPerFrame = 1000;
				break;
			case NTV2_FRAMERATE_4795:
				audioSamplesPerFrame = 1001;
				break;
			case NTV2_FRAMERATE_3000:
				audioSamplesPerFrame = 1600;
				break;
			case NTV2_FRAMERATE_2997:
				// depends on cadenceFrame;
				switch ( cadenceFrame )
				{
				case 0:
				case 2:
				case 4:
					audioSamplesPerFrame = 1602;
					break;
				case 1:
				case 3:
					audioSamplesPerFrame = 1601;
					break;
				}
				break;
			case NTV2_FRAMERATE_2500:
				audioSamplesPerFrame = 1920;
				break;
			case NTV2_FRAMERATE_2400:
				audioSamplesPerFrame = 2000;
				break;
			case NTV2_FRAMERATE_2398:
				audioSamplesPerFrame = 2002;
				break;
			case NTV2_FRAMERATE_1500:
				audioSamplesPerFrame = 3200;
				break;
			case NTV2_FRAMERATE_1498:
				// depends on cadenceFrame;
				switch ( cadenceFrame )
				{
				case 0:
					audioSamplesPerFrame = 3204;
					break;
				case 1:
				case 2:
				case 3:
				case 4:
					audioSamplesPerFrame = 3203;
					break;
				}
				break;
			case NTV2_FRAMERATE_1900:	// Not supported yet
			case NTV2_FRAMERATE_1898:	// Not supported yet
			case NTV2_FRAMERATE_1800: 	// Not supported yet
			case NTV2_FRAMERATE_1798:	// Not supported yet
			case NTV2_FRAMERATE_UNKNOWN:
			case NTV2_NUM_FRAMERATES:
				audioSamplesPerFrame = 0;
				break;
		}
	}
	else
	if ( audioRate == NTV2_AUDIO_96K)
	{
		switch ( frameRate)
		{
			case NTV2_FRAMERATE_12000:
				audioSamplesPerFrame = 800;
				break;
			case NTV2_FRAMERATE_11988:
				switch ( cadenceFrame )
				{
				case 0:
				case 1:
				case 2:
				case 3:
					audioSamplesPerFrame = 901;
					break;
				case 4:
					audioSamplesPerFrame = 800;
					break;
				}
				break;
			case NTV2_FRAMERATE_6000:
				audioSamplesPerFrame = 800*2;
				break;
			case NTV2_FRAMERATE_5994:
				switch ( cadenceFrame )
				{
				case 0:
				case 2:
				case 4:
					audioSamplesPerFrame = 1602;
					break;
				case 1:
				case 3:
					audioSamplesPerFrame = 1601;
					break;
				}
				break;
			case NTV2_FRAMERATE_5000:
				audioSamplesPerFrame = 1920;
				break;
			case NTV2_FRAMERATE_4800:
				audioSamplesPerFrame = 2000;
				break;
			case NTV2_FRAMERATE_4795:
				audioSamplesPerFrame = 2002;
				break;
			case NTV2_FRAMERATE_3000:
				audioSamplesPerFrame = 1600*2;
				break;
			case NTV2_FRAMERATE_2997:
				// depends on cadenceFrame;
				switch ( cadenceFrame )
				{
				case 0:
					audioSamplesPerFrame = 3204;
					break;
				case 1:
				case 2:
				case 3:
				case 4:
					audioSamplesPerFrame = 3203;
					break;
				}
				break;
			case NTV2_FRAMERATE_2500:
				audioSamplesPerFrame = 1920*2;
				break;
			case NTV2_FRAMERATE_2400:
				audioSamplesPerFrame = 2000*2;
				break;
			case NTV2_FRAMERATE_2398:
				audioSamplesPerFrame = 2002*2;
				break;
			case NTV2_FRAMERATE_1500:
				audioSamplesPerFrame = 3200*2;
				break;
			case NTV2_FRAMERATE_1498:
				// depends on cadenceFrame;
				switch ( cadenceFrame )
				{
				case 0:
					audioSamplesPerFrame = 3204*2;
					break;
				case 1:
				case 2:
				case 3:
				case 4:
					audioSamplesPerFrame = 3203*2;
					break;
				}
				break;
			case NTV2_FRAMERATE_1900:	// Not supported yet
			case NTV2_FRAMERATE_1898:	// Not supported yet
			case NTV2_FRAMERATE_1800: 	// Not supported yet
			case NTV2_FRAMERATE_1798:	// Not supported yet
			case NTV2_FRAMERATE_UNKNOWN:
			case NTV2_NUM_FRAMERATES:
				audioSamplesPerFrame = 0*2; //haha
				break;
		}
	}

	return audioSamplesPerFrame;
}


// GetTotalAudioSamplesFromFrameNbrZeroUpToFrameNbr(NTV2FrameRate frameRate, NTV2AudioRate audioRate, ULWord frameNbrNonInclusive)
// For a given framerate and audiorate and ending frame number (non-inclusive), returns the total number of audio samples over
// the range of video frames starting at frame number zero up to and not including the passed in frame number, frameNbrNonInclusive.
// Utilizes cadence patterns in function immediately above,  GetAudioSamplesPerFrame().
// No smpte372Enabled support
LWord64 GetTotalAudioSamplesFromFrameNbrZeroUpToFrameNbr(NTV2FrameRate frameRate, NTV2AudioRate audioRate, ULWord frameNbrNonInclusive)
{
	LWord64 numTotalAudioSamples;
	LWord64 numAudioSamplesFromWholeGroups;

	ULWord numWholeGroupsOfFive;
	ULWord numAudioSamplesFromRemainder;
	ULWord remainder;

	numWholeGroupsOfFive = frameNbrNonInclusive/5;
	remainder = frameNbrNonInclusive % 5;

	numTotalAudioSamples = 0;
	numAudioSamplesFromWholeGroups = 0;
	numAudioSamplesFromRemainder = 0;

	if (audioRate == NTV2_AUDIO_48K)
	{
		switch (frameRate)
		{
			case NTV2_FRAMERATE_12000:
				numTotalAudioSamples = 400 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_11988:
				numAudioSamplesFromWholeGroups = ((2*401) + (3*400)) * numWholeGroupsOfFive;
				numAudioSamplesFromRemainder = (remainder == 0) ? 0 : ((400 * remainder) + remainder/2);
				numTotalAudioSamples = numAudioSamplesFromWholeGroups + numAudioSamplesFromRemainder;
				break;
			case NTV2_FRAMERATE_6000:
				numTotalAudioSamples = 800 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_5994:
				// depends on cadenceFrame;
				numAudioSamplesFromWholeGroups = ((1*800) + (4*801)) * numWholeGroupsOfFive;
				numAudioSamplesFromRemainder = (remainder == 0) ? 0 : ((801 * remainder) - 1);
				numTotalAudioSamples = numAudioSamplesFromWholeGroups + numAudioSamplesFromRemainder;
				break;
			case NTV2_FRAMERATE_5000:
				numTotalAudioSamples = 1920/2 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_4800:
				numTotalAudioSamples = 1000 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_4795:
				numTotalAudioSamples = 1001 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_3000:
				numTotalAudioSamples = 1600 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_2997:
				// depends on cadenceFrame;
				numAudioSamplesFromWholeGroups = ((3*1602) + (2*1601)) * numWholeGroupsOfFive;
				numAudioSamplesFromRemainder = (remainder == 0) ? 0 : ((1602 * remainder) - remainder/2);
				numTotalAudioSamples = numAudioSamplesFromWholeGroups + numAudioSamplesFromRemainder;
				break;
			case NTV2_FRAMERATE_2500:
				numTotalAudioSamples = 1920 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_2400:
				numTotalAudioSamples = 2000 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_2398:
				numTotalAudioSamples = 2002 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_1500:
				numTotalAudioSamples = 3200 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_1498:
				// depends on cadenceFrame;
				numAudioSamplesFromWholeGroups = ((1*3204) + (4*3203)) * numWholeGroupsOfFive;
				numAudioSamplesFromRemainder = (remainder == 0) ? 0 : ((3203 * remainder) + 1);
				numTotalAudioSamples = numAudioSamplesFromWholeGroups + numAudioSamplesFromRemainder;
				break;
			case NTV2_FRAMERATE_1900:	// Not supported yet
			case NTV2_FRAMERATE_1898:	// Not supported yet
			case NTV2_FRAMERATE_1800: 	// Not supported yet
			case NTV2_FRAMERATE_1798:	// Not supported yet
			case NTV2_FRAMERATE_UNKNOWN:
			case NTV2_NUM_FRAMERATES:
				numTotalAudioSamples = 0;
				break;
		}
	}
	else
	if (audioRate == NTV2_AUDIO_96K)
	{
		switch (frameRate)
		{
			case NTV2_FRAMERATE_12000:
				numTotalAudioSamples = 800 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_11988:
				numAudioSamplesFromWholeGroups = ((4*801) + (1*800)) * numWholeGroupsOfFive;
				numAudioSamplesFromRemainder = (remainder == 0) ? 0 : (801 * remainder);
				numTotalAudioSamples = numAudioSamplesFromWholeGroups + numAudioSamplesFromRemainder;
				break;
			case NTV2_FRAMERATE_6000:
				numTotalAudioSamples = (800*2) * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_5994:
				numAudioSamplesFromWholeGroups = ((3*1602) + (2*1601)) * numWholeGroupsOfFive;
				numAudioSamplesFromRemainder = (remainder == 0) ? 0 : ((1602 * remainder) - remainder/2);
				numTotalAudioSamples = numAudioSamplesFromWholeGroups + numAudioSamplesFromRemainder;
				break;
			case NTV2_FRAMERATE_5000:
				numTotalAudioSamples = 1920 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_4800:
				numTotalAudioSamples = 2000 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_4795:
				numTotalAudioSamples = 2002 * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_3000:
				numTotalAudioSamples = (1600*2) * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_2997:
				// depends on cadenceFrame;
				numAudioSamplesFromWholeGroups = ((1*3204) + (4*3203)) * numWholeGroupsOfFive;
				numAudioSamplesFromRemainder = (remainder == 0) ? 0 : ((3203 * remainder) + 1);
				numTotalAudioSamples = numAudioSamplesFromWholeGroups + numAudioSamplesFromRemainder;
				break;
			case NTV2_FRAMERATE_2500:
				numTotalAudioSamples = (1920*2) * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_2400:
				numTotalAudioSamples = (2000*2) * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_2398:
				numTotalAudioSamples = (2002*2) * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_1500:
				numTotalAudioSamples = (3200*2) * frameNbrNonInclusive;
				break;
			case NTV2_FRAMERATE_1498:
				// depends on cadenceFrame;
				numAudioSamplesFromWholeGroups = ((1*3204*2) + (4*3203*2)) * numWholeGroupsOfFive;
				numAudioSamplesFromRemainder = (remainder == 0) ? 0 : (((3203*2) * remainder) + 2);
				numTotalAudioSamples = numAudioSamplesFromWholeGroups + numAudioSamplesFromRemainder;
				break;
			case NTV2_FRAMERATE_1900:	// Not supported yet
			case NTV2_FRAMERATE_1898:	// Not supported yet
			case NTV2_FRAMERATE_1800: 	// Not supported yet
			case NTV2_FRAMERATE_1798:	// Not supported yet
			case NTV2_FRAMERATE_UNKNOWN:
			case NTV2_NUM_FRAMERATES:
				numTotalAudioSamples = 0*2; //haha
				break;
		}
	}

	return numTotalAudioSamples;
}

// GetVaricamRepeatCount(NTV2FrameRate sequenceRate, NTV2FrameRate playRate, ULWord cadenceFrame)
// For a given sequenceRate and playRate, given the cadenceFrame it returns how many times we
// repeate the frame to output varicam.  If the result is zero then this is an unsupported varicam
// rate.
ULWord GetVaricamRepeatCount(NTV2FrameRate sequenceRate, NTV2FrameRate playRate, ULWord cadenceFrame)
{
	ULWord result = 0;

	switch (playRate)
	{
		case NTV2_FRAMERATE_6000:
			switch (sequenceRate)
			{
				case NTV2_FRAMERATE_1500:
					result = 4;
					break;
				case NTV2_FRAMERATE_2400:			// 24 -> 60					2:3|2:3|2:3 ...
					cadenceFrame %= 2;
					switch (cadenceFrame)
					{
						case 0:
							result = 2;
							break;
						case 1:
							result = 3;
							break;
					}
					break;
				case NTV2_FRAMERATE_2500:			// 25 -> 60					2:3:2:3:2|2:3:2:3:2 ...
					cadenceFrame %= 5;
					switch (cadenceFrame)
					{
						case 0:
						case 2:
						case 4:
							result = 2;
							break;
						case 1:
						case 3:
							result = 3;
							break;
					}
					break;
				case NTV2_FRAMERATE_3000:			// 30 -> 60					2|2|2|2|2|2 ...
					result = 2;
					break;
				case NTV2_FRAMERATE_4800:			// 48 -> 60					2:1:1:1|2:1:1:1 ...
					cadenceFrame %= 4;
					switch (cadenceFrame)
					{
						case 0:
							result = 2;
							break;
						case 1:
						case 2:
						case 3:
							result = 1;
							break;
					}
					break;
				case NTV2_FRAMERATE_5000:			// 50 -> 60					2:1:1:1:1|2:1:1:1:1: ...
					cadenceFrame %= 5;
					switch (cadenceFrame)
					{
						case 0:
							result = 2;
							break;
						case 1:
						case 2:
						case 3:
						case 4:
							result = 1;
							break;
					}
					break;
				case NTV2_FRAMERATE_6000:			// 60 -> 60					1|1|1|1|1|1 ...
					result = 1;
					break;
				default:
					break;
			}
			break;

		case NTV2_FRAMERATE_5994:
			switch (sequenceRate)
			{
				case NTV2_FRAMERATE_1498:
					result = 4;
					break;
				case NTV2_FRAMERATE_2398:			// 23.98 -> 59.94			2:3|2:3|2:3 ...
					cadenceFrame %= 2;
					switch (cadenceFrame)
					{
						case 0:
							result = 2;
							break;
						case 1:
							result = 3;
							break;
					}
					break;
				case NTV2_FRAMERATE_2997:			// 29.97 -> 59.94			2|2|2|2|2|2 ...
					result = 2;
					break;
				case NTV2_FRAMERATE_4795:			// 47.95 -> 59.94			2:1:1:1|2:1:1:1 ...
					cadenceFrame %= 4;
					switch (cadenceFrame)
					{
						case 0:
							result = 2;
							break;
						case 1:
						case 2:
						case 3:
							result = 1;
							break;
					}
					break;
				case NTV2_FRAMERATE_5994:			// 59.94 -> 59.94			1|1|1|1|1|1 ...
					result = 1;
					break;
				default:
					break;
			}
			break;

		case NTV2_FRAMERATE_5000:
			switch (sequenceRate)
			{
				case NTV2_FRAMERATE_2500:			// 25 -> 50					2|2|2|2|2| ...
					result = 2;
					break;
				default:
					break;
			}
			break;

		default:
			break;
	}
	return result;
}

ULWord GetScaleFromFrameRate(NTV2FrameRate frameRate)
{
	switch (frameRate)
	{
	case NTV2_FRAMERATE_12000:
		return 12000;
	case NTV2_FRAMERATE_11988:
		return 11988;
	case NTV2_FRAMERATE_6000:
		return 6000;
	case NTV2_FRAMERATE_5994:
		return 5994;
	case NTV2_FRAMERATE_5000:
		return 5000;
	case NTV2_FRAMERATE_4800:
		return 4800;
	case NTV2_FRAMERATE_4795:
		return 4795;
	case NTV2_FRAMERATE_3000:
		return 3000;
	case NTV2_FRAMERATE_2997:
		return 2997;
	case NTV2_FRAMERATE_2500:
		return 2500;
	case NTV2_FRAMERATE_2400:
		return 2400;
	case NTV2_FRAMERATE_2398:
		return 2398;
	case NTV2_FRAMERATE_1900:
		return 1900;
	case NTV2_FRAMERATE_1898:
		return 1898;
	case NTV2_FRAMERATE_1800:
		return 1800;
	case NTV2_FRAMERATE_1798:
		return 1798;
	case NTV2_FRAMERATE_1500:
		return 1500;
	case NTV2_FRAMERATE_1498:
		return 1498;
	default:
		return 0;
	}
}

// GetFrameRateFromScale(long scale, long duration, NTV2FrameRate playFrameRate)
// For a given scale value it returns the associated frame rate.  This routine is
// used to calculate and decipher the sequence frame rate.
NTV2FrameRate GetFrameRateFromScale(long scale, long duration, NTV2FrameRate playFrameRate)
{
	NTV2FrameRate result = NTV2_FRAMERATE_6000;

	// Generally the duration is 100 and in that event the scale will tell us for sure what the
	// sequence rate is.
	if (duration == 100)
	{
		switch (scale)
		{
			case 12000:
				result = NTV2_FRAMERATE_12000;
				break;
			case 11988:
				result = NTV2_FRAMERATE_11988;
				break;
			case 6000:
				result = NTV2_FRAMERATE_6000;
				break;
			case 5994:
				result = NTV2_FRAMERATE_5994;
				break;
			case 5000:
				result = NTV2_FRAMERATE_5000;
				break;
			case 4800:
				result = NTV2_FRAMERATE_4800;
				break;
			case 4795:
				result = NTV2_FRAMERATE_4795;
				break;
			case 3000:
				result = NTV2_FRAMERATE_3000;
				break;
			case 2997:
				result = NTV2_FRAMERATE_2997;
				break;
			case 2500:
				result = NTV2_FRAMERATE_2500;
				break;
			case 2400:
				result = NTV2_FRAMERATE_2400;
				break;
			case 2398:
				result = NTV2_FRAMERATE_2398;
				break;
			case 1500:
				result = NTV2_FRAMERATE_1500;
				break;
			case 1498:
				result = NTV2_FRAMERATE_1498;
				break;
		}
	}
	else if (duration == 0)
	{
		result = playFrameRate;
	}
	else
	{
		float scaleFloat = scale / duration * (float)100.0;
		long scaleInt = (long) scaleFloat;

		// In this case we need to derive the sequence rate based on scale, duration and what
		// our playback rate is.  So first we break down what we might expect based on our
		// playback rate.  This gives us some room to look at values that are returned and
		// which are not exact based on rounding errors.  We can break this check up into two
		// camps because the assumption is we don't have to worry about playing back 23.98 fps
		// sequence on a 60 fps output and conversly playing back 30 fps sequences on a 59.94
		// fps output.
		switch (playFrameRate)
		{
			case NTV2_FRAMERATE_12000:
			case NTV2_FRAMERATE_6000:
			case NTV2_FRAMERATE_5000:
			case NTV2_FRAMERATE_4800:
			case NTV2_FRAMERATE_3000:
			case NTV2_FRAMERATE_2500:
			case NTV2_FRAMERATE_2400:
			case NTV2_FRAMERATE_1500:
				if (scaleInt <= 1500 + 100)
					result = NTV2_FRAMERATE_1500;
				else if (scaleInt <= 2400 + 50)
					result = NTV2_FRAMERATE_2400;
				else if (scaleInt <= 2500 + 100)
					result = NTV2_FRAMERATE_2500;
				else if (scaleInt <= 3000 + 100)
					result = NTV2_FRAMERATE_3000;
				else if (scaleInt <= 4800 + 100)
					result = NTV2_FRAMERATE_4800;
				else if (scaleInt <= 5000 + 100)
					result = NTV2_FRAMERATE_5000;
				else if (scaleInt <= 6000 + 100)
					result = NTV2_FRAMERATE_6000;
				else
					result = NTV2_FRAMERATE_12000;
				break;

			case NTV2_FRAMERATE_11988:
			case NTV2_FRAMERATE_5994:
			case NTV2_FRAMERATE_4795:
			case NTV2_FRAMERATE_2997:
			case NTV2_FRAMERATE_2398:
			case NTV2_FRAMERATE_1498:
				if (scaleInt <= 1498 + 100)				// add some fudge factor for rounding errors
					result = NTV2_FRAMERATE_1498;
				else if (scaleInt <= 2398 + 100)
					result = NTV2_FRAMERATE_2398;
				else if (scaleInt <= 2997 + 100)
					result = NTV2_FRAMERATE_2997;
				else if (scaleInt <= 4795 + 100)
					result = NTV2_FRAMERATE_4795;
				else if (scaleInt <= 5994 + 100)
					result = NTV2_FRAMERATE_5994;
				else
					result = NTV2_FRAMERATE_11988;
				break;
			default:
				break;
		}
	}
	return result;
}

NTV2FrameRate GetNTV2FrameRateFromVideoFormat(NTV2VideoFormat videoFormat)
{
	NTV2FrameRate frameRate = NTV2_FRAMERATE_2997;
	switch ( videoFormat )
	{
	case NTV2_FORMAT_1080i_5000:
	case NTV2_FORMAT_1080psf_2500_2:
	case NTV2_FORMAT_1080p_2500:
	case NTV2_FORMAT_625_5000:
	case NTV2_FORMAT_625psf_2500:
	case NTV2_FORMAT_720p_2500:
	case NTV2_FORMAT_1080psf_2K_2500:
	case NTV2_FORMAT_1080p_2K_2500:
	case NTV2_FORMAT_4x1920x1080psf_2500:
	case NTV2_FORMAT_4x1920x1080p_2500:
	case NTV2_FORMAT_4x2048x1080psf_2500:
	case NTV2_FORMAT_4x2048x1080p_2500:
		frameRate = NTV2_FRAMERATE_2500;
		break;

	case NTV2_FORMAT_1080p_5994_A:
	case NTV2_FORMAT_1080p_5994_B:
	case NTV2_FORMAT_720p_5994:
	case NTV2_FORMAT_1080p_2K_5994:
	case NTV2_FORMAT_4x1920x1080p_5994:
	case NTV2_FORMAT_4x2048x1080p_5994:
		frameRate = NTV2_FRAMERATE_5994;
		break;

	case NTV2_FORMAT_720p_6000:
	case NTV2_FORMAT_1080p_6000_A:
	case NTV2_FORMAT_1080p_6000_B:
	case NTV2_FORMAT_1080p_2K_6000:
	case NTV2_FORMAT_4x1920x1080p_6000:
	case NTV2_FORMAT_4x2048x1080p_6000:
		frameRate = NTV2_FRAMERATE_6000;
		break;

	case NTV2_FORMAT_1080i_6000:
	case NTV2_FORMAT_1080p_3000:
	case NTV2_FORMAT_1080psf_3000_2:
	case NTV2_FORMAT_1080p_2K_3000:
	case NTV2_FORMAT_4x1920x1080p_3000:
	case NTV2_FORMAT_4x1920x1080psf_3000:
	case NTV2_FORMAT_4x2048x1080p_3000:
	case NTV2_FORMAT_4x2048x1080psf_3000:
		frameRate = NTV2_FRAMERATE_3000;
		break;

	case NTV2_FORMAT_1080p_5000_A:
	case NTV2_FORMAT_1080p_5000_B:
	case NTV2_FORMAT_720p_5000:
	case NTV2_FORMAT_1080p_2K_5000:
	case NTV2_FORMAT_4x1920x1080p_5000:
	case NTV2_FORMAT_4x2048x1080p_5000:
		frameRate = NTV2_FRAMERATE_5000;
		break;

	case NTV2_FORMAT_1080i_5994:
	case NTV2_FORMAT_1080psf_2997_2:
	case NTV2_FORMAT_1080p_2997:
	case NTV2_FORMAT_525_5994:
	case NTV2_FORMAT_525psf_2997:
	case NTV2_FORMAT_1080p_2K_2997:
	case NTV2_FORMAT_4x1920x1080p_2997:
	case NTV2_FORMAT_4x1920x1080psf_2997:
	case NTV2_FORMAT_4x2048x1080p_2997:
	case NTV2_FORMAT_4x2048x1080psf_2997:
		frameRate = NTV2_FRAMERATE_2997;
		break;

	case NTV2_FORMAT_525_2398:
	case NTV2_FORMAT_720p_2398:
	case NTV2_FORMAT_1080psf_2K_2398:
	case NTV2_FORMAT_1080psf_2398:
	case NTV2_FORMAT_1080p_2398:
	case NTV2_FORMAT_1080p_2K_2398:
	case NTV2_FORMAT_4x1920x1080psf_2398:
	case NTV2_FORMAT_4x1920x1080p_2398:
	case NTV2_FORMAT_4x2048x1080psf_2398:
	case NTV2_FORMAT_4x2048x1080p_2398:
		frameRate = NTV2_FRAMERATE_2398;
		break;

	case NTV2_FORMAT_525_2400:
	case NTV2_FORMAT_1080psf_2400:
	case NTV2_FORMAT_1080psf_2K_2400:
	case NTV2_FORMAT_1080p_2400:
	case NTV2_FORMAT_1080p_2K_2400:
	case NTV2_FORMAT_4x1920x1080psf_2400:
	case NTV2_FORMAT_4x1920x1080p_2400:
	case NTV2_FORMAT_4x2048x1080psf_2400:
	case NTV2_FORMAT_4x2048x1080p_2400:
		frameRate = NTV2_FRAMERATE_2400;
		break;
	case NTV2_FORMAT_2K_1498:
		frameRate = NTV2_FRAMERATE_1498;
		break;
	case NTV2_FORMAT_2K_1500:
		frameRate = NTV2_FRAMERATE_1500;
		break;
	case NTV2_FORMAT_2K_2398:
		frameRate = NTV2_FRAMERATE_2398;
		break;
	case NTV2_FORMAT_2K_2400:
		frameRate = NTV2_FRAMERATE_2400;
		break;
	case NTV2_FORMAT_2K_2500:
		frameRate = NTV2_FRAMERATE_2500;
		break;

	case NTV2_FORMAT_1080p_2K_4795:
	case NTV2_FORMAT_4x2048x1080p_4795:
		frameRate = NTV2_FRAMERATE_4795;
		break;
	case NTV2_FORMAT_1080p_2K_4800:
	case NTV2_FORMAT_4x2048x1080p_4800:
		frameRate = NTV2_FRAMERATE_4800;
		break;

	case NTV2_FORMAT_4x2048x1080p_11988:
		frameRate = NTV2_FRAMERATE_11988;
		break;
	case NTV2_FORMAT_4x2048x1080p_12000:
		frameRate = NTV2_FRAMERATE_12000;
		break;

	/**	To reveal missing values, comment out the "default:" below, then uncomment out these cases:
	case NTV2_FORMAT_UNKNOWN:
	case NTV2_FORMAT_END_HIGH_DEF_FORMATS:
	case NTV2_FORMAT_END_STANDARD_DEF_FORMATS:
	case NTV2_FORMAT_END_2K_DEF_FORMATS:
	case NTV2_FORMAT_END_4K_DEF_FORMATS:
	case NTV2_FORMAT_END_HIGH_DEF_FORMATS2:**/
	default:
		// should error out.
		break;	// Unsupported

	}

	return frameRate;

}	//	GetNTV2FrameRateFromVideoFormat


ULWord GetNTV2FrameGeometryHeight(NTV2FrameGeometry geometry)
{
	switch (geometry)
	{
		case NTV2_FG_1920x1080:		return 1080;
		case NTV2_FG_1280x720:		return 720;
		case NTV2_FG_720x486:		return 486;
		case NTV2_FG_720x576:		return 576;
		case NTV2_FG_720x508:		return 508;
		case NTV2_FG_720x598:		return 598;
		case NTV2_FG_1920x1112:		return 1112;
		case NTV2_FG_1920x1114:		return 1114;
		case NTV2_FG_1280x740:		return 740;
		case NTV2_FG_2048x1080:		return 1080;
        case NTV2_FG_2048x1556:		return 1556;
		case NTV2_FG_2048x1588:		return 1588;
		case NTV2_FG_2048x1112:		return 1112;
		case NTV2_FG_2048x1114:		return 1114;
		case NTV2_FG_720x514:		return 514;
		case NTV2_FG_720x612:		return 612;
		case NTV2_FG_4x1920x1080:	return 2160;
		case NTV2_FG_4x2048x1080:	return 2160;
        default:					return 0;
	}
}

ULWord GetNTV2FrameGeometryWidth(NTV2FrameGeometry geometry)
{
	switch ( geometry )
	{
        case NTV2_FG_720x486:
        case NTV2_FG_720x576:
        case NTV2_FG_720x508:
        case NTV2_FG_720x598:
		case NTV2_FG_720x514:
		case NTV2_FG_720x612:
			return 720;
        case NTV2_FG_1280x720:
        case NTV2_FG_1280x740:
			return 1280;
        case NTV2_FG_1920x1080:
        case NTV2_FG_1920x1112:
		case NTV2_FG_1920x1114:
			return 1920;
        case NTV2_FG_2048x1080:
		case NTV2_FG_2048x1112:
		case NTV2_FG_2048x1114:
        case NTV2_FG_2048x1556:
		case NTV2_FG_2048x1588:
			return 2048;
		case NTV2_FG_4x1920x1080:
			return 3840;
		case NTV2_FG_4x2048x1080:
			return 4096;
        default:
			return 0;
	}
}


//	Displayable width of format, not counting HANC/VANC
ULWord GetDisplayWidth (const NTV2VideoFormat videoFormat)
{
	int width = 0;

	switch (videoFormat)
	{
		case NTV2_FORMAT_525_2398:
		case NTV2_FORMAT_525_2400:
		case NTV2_FORMAT_525_5994:
		case NTV2_FORMAT_525psf_2997:
		case NTV2_FORMAT_625_5000:
		case NTV2_FORMAT_625psf_2500:
			width = 720;
			break;
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_720p_5994:
		case NTV2_FORMAT_720p_6000:
		case NTV2_FORMAT_720p_2398:
		case NTV2_FORMAT_720p_2500:
			width = 1280;
			break;
		case NTV2_FORMAT_1080i_5000:
		case NTV2_FORMAT_1080i_5994:
		case NTV2_FORMAT_1080i_6000:
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080psf_2400:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
		case NTV2_FORMAT_1080p_5000_B:
		case NTV2_FORMAT_1080p_5994_B:
		case NTV2_FORMAT_1080p_6000_B:
			width = 1920;
			break;
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080p_2K_2997:
		case NTV2_FORMAT_1080p_2K_3000:
		case NTV2_FORMAT_1080p_2K_4795:
		case NTV2_FORMAT_1080p_2K_4800:
		case NTV2_FORMAT_1080p_2K_5000:
		case NTV2_FORMAT_1080p_2K_5994:
		case NTV2_FORMAT_1080p_2K_6000:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_2K_1498:
		case NTV2_FORMAT_2K_1500:
		case NTV2_FORMAT_2K_2398:
		case NTV2_FORMAT_2K_2400:
		case NTV2_FORMAT_2K_2500:
			width = 2048;
			break;
		case NTV2_FORMAT_4x1920x1080psf_2398:
		case NTV2_FORMAT_4x1920x1080psf_2400:
		case NTV2_FORMAT_4x1920x1080psf_2500:
		case NTV2_FORMAT_4x1920x1080psf_2997:
		case NTV2_FORMAT_4x1920x1080psf_3000:
		case NTV2_FORMAT_4x1920x1080p_2398:
		case NTV2_FORMAT_4x1920x1080p_2400:
		case NTV2_FORMAT_4x1920x1080p_2500:
		case NTV2_FORMAT_4x1920x1080p_2997:
		case NTV2_FORMAT_4x1920x1080p_3000:
		case NTV2_FORMAT_4x1920x1080p_5000:
		case NTV2_FORMAT_4x1920x1080p_5994:
		case NTV2_FORMAT_4x1920x1080p_6000:
			width = 3840;
			break;
		case NTV2_FORMAT_4x2048x1080psf_2398:
		case NTV2_FORMAT_4x2048x1080psf_2400:
		case NTV2_FORMAT_4x2048x1080psf_2500:
		case NTV2_FORMAT_4x2048x1080psf_2997:
		case NTV2_FORMAT_4x2048x1080psf_3000:
		case NTV2_FORMAT_4x2048x1080p_2398:
		case NTV2_FORMAT_4x2048x1080p_2400:
		case NTV2_FORMAT_4x2048x1080p_2500:
		case NTV2_FORMAT_4x2048x1080p_2997:
		case NTV2_FORMAT_4x2048x1080p_3000:
		case NTV2_FORMAT_4x2048x1080p_4795:
		case NTV2_FORMAT_4x2048x1080p_4800:
		case NTV2_FORMAT_4x2048x1080p_5000:
		case NTV2_FORMAT_4x2048x1080p_5994:
		case NTV2_FORMAT_4x2048x1080p_6000:
		case NTV2_FORMAT_4x2048x1080p_11988:
		case NTV2_FORMAT_4x2048x1080p_12000:
			width = 4096;
			break;
		/**	To reveal missing values, comment out the "default:" below, then uncomment out these cases:
		case NTV2_FORMAT_UNKNOWN:
		case NTV2_FORMAT_END_HIGH_DEF_FORMATS:
		case NTV2_FORMAT_END_STANDARD_DEF_FORMATS:
		case NTV2_FORMAT_END_2K_DEF_FORMATS:
		case NTV2_FORMAT_END_4K_DEF_FORMATS:
		case NTV2_FORMAT_END_HIGH_DEF_FORMATS2:**/
		default:
			width = 0;
			break;
	}

	return width;

}	//	GetDisplayWidth


//	Displayable height of format, not counting HANC/VANC
ULWord GetDisplayHeight (const NTV2VideoFormat videoFormat)
{
	int height = 0;

	switch (videoFormat)
	{
		case NTV2_FORMAT_525_2398:
		case NTV2_FORMAT_525_2400:
		case NTV2_FORMAT_525_5994:
		case NTV2_FORMAT_525psf_2997:
			height = 486;
			break;
		case NTV2_FORMAT_625_5000:
		case NTV2_FORMAT_625psf_2500:
			height = 576;
			break;
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_720p_5994:
		case NTV2_FORMAT_720p_6000:
		case NTV2_FORMAT_720p_2398:
		case NTV2_FORMAT_720p_2500:
			height = 720;
			break;
		case NTV2_FORMAT_1080i_5000:
		case NTV2_FORMAT_1080i_5994:
		case NTV2_FORMAT_1080i_6000:
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080psf_2400:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
		case NTV2_FORMAT_1080p_5000_B:
		case NTV2_FORMAT_1080p_5994_B:
		case NTV2_FORMAT_1080p_6000_B:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080p_2K_2997:
		case NTV2_FORMAT_1080p_2K_3000:
		case NTV2_FORMAT_1080p_2K_4795:
		case NTV2_FORMAT_1080p_2K_4800:
		case NTV2_FORMAT_1080p_2K_5000:
		case NTV2_FORMAT_1080p_2K_5994:
		case NTV2_FORMAT_1080p_2K_6000:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2500:
			height = 1080;
			break;
		case NTV2_FORMAT_2K_1498:
		case NTV2_FORMAT_2K_1500:
		case NTV2_FORMAT_2K_2398:
		case NTV2_FORMAT_2K_2400:
		case NTV2_FORMAT_2K_2500:
			height = 1556;
			break;
		case NTV2_FORMAT_4x1920x1080psf_2398:
		case NTV2_FORMAT_4x1920x1080psf_2400:
		case NTV2_FORMAT_4x1920x1080psf_2500:
		case NTV2_FORMAT_4x1920x1080psf_2997:
		case NTV2_FORMAT_4x1920x1080psf_3000:
		case NTV2_FORMAT_4x1920x1080p_2398:
		case NTV2_FORMAT_4x1920x1080p_2400:
		case NTV2_FORMAT_4x1920x1080p_2500:
		case NTV2_FORMAT_4x1920x1080p_2997:
		case NTV2_FORMAT_4x1920x1080p_3000:
		case NTV2_FORMAT_4x2048x1080psf_2398:
		case NTV2_FORMAT_4x2048x1080psf_2400:
		case NTV2_FORMAT_4x2048x1080psf_2500:
		case NTV2_FORMAT_4x2048x1080psf_2997:
		case NTV2_FORMAT_4x2048x1080psf_3000:
		case NTV2_FORMAT_4x2048x1080p_2398:
		case NTV2_FORMAT_4x2048x1080p_2400:
		case NTV2_FORMAT_4x2048x1080p_2500:
		case NTV2_FORMAT_4x2048x1080p_2997:
		case NTV2_FORMAT_4x2048x1080p_3000:
		case NTV2_FORMAT_4x1920x1080p_5000:
		case NTV2_FORMAT_4x1920x1080p_5994:
		case NTV2_FORMAT_4x1920x1080p_6000:
		case NTV2_FORMAT_4x2048x1080p_4795:
		case NTV2_FORMAT_4x2048x1080p_4800:
		case NTV2_FORMAT_4x2048x1080p_5000:
		case NTV2_FORMAT_4x2048x1080p_5994:
		case NTV2_FORMAT_4x2048x1080p_6000:
		case NTV2_FORMAT_4x2048x1080p_11988:
		case NTV2_FORMAT_4x2048x1080p_12000:
			height = 2160;
			break;
		case NTV2_FORMAT_UNKNOWN:
		case NTV2_FORMAT_END_HIGH_DEF_FORMATS:
		case NTV2_FORMAT_END_STANDARD_DEF_FORMATS:
		case NTV2_FORMAT_END_2K_DEF_FORMATS:
		//case NTV2_FORMAT_END_4K_DEF_FORMATS:	//	duplicate of NTV2_FORMAT_FIRST_HIGH_DEF_FORMAT2 and NTV2_FORMAT_1080p_2K_6000
		case NTV2_FORMAT_END_HIGH_DEF_FORMATS2:
			height = 0;
			break;
	}

	return height;

}	//	GetDisplayHeight


NTV2FormatDescriptor GetFormatDescriptor (const NTV2Standard		inVideoStandard,
										const NTV2FrameBufferFormat	inFrameBufferFormat,
										const bool					inVANCenabled,
										const bool					in2Kby1080,
										const bool					inWideVANC)
{
	static NTV2FormatDescriptor nullDescriptor = {0,0,0,0};

	if (inVideoStandard >= NTV2_NUM_STANDARDS)
		return nullDescriptor;
	if (inFrameBufferFormat >= NTV2_FBF_NUMFRAMEBUFFERFORMATS)
		return nullDescriptor;

	NTV2FormatDescriptor	result			(formatDescriptorTable[inVideoStandard][inFrameBufferFormat]);
	ULWord					numActiveLines	(result.numLines);

	// VANC is not properly handled in the table.
	if (inVANCenabled)
	{
		switch (inVideoStandard)
		{
			case NTV2_STANDARD_1080:
			case NTV2_STANDARD_1080p:	result.numLines = inWideVANC ? 1114 : 1112;											break;

			case NTV2_STANDARD_720:		result.numLines = 740;							result.firstActiveLine = 740 - 720;	break;

			case NTV2_STANDARD_525:		result.numLines = inWideVANC ? 514 : 508;											break;

			case NTV2_STANDARD_625:		result.numLines = inWideVANC ? 612 : 598;											break;

			case NTV2_STANDARD_2K:		result.numLines = 1588;																break;

			default:					return nullDescriptor;
		}
	}
	result.firstActiveLine = result.numLines - numActiveLines;

	if (in2Kby1080)
	{
		switch (inFrameBufferFormat)
		{
			case NTV2_FBF_8BIT_YCBCR:
			case NTV2_FBF_8BIT_YCBCR_YUY2:
				result.numPixels = HD_NUMCOMPONENTPIXELS_2K;
				result.linePitch = HD_NUMCOMPONENTPIXELS_2K*2/4;
				break;

			case NTV2_FBF_10BIT_YCBCR:
				result.numPixels = HD_NUMCOMPONENTPIXELS_2K;
				result.linePitch = HD_YCBCRLINEPITCH_2K;
				break;

			case NTV2_FBF_ARGB:
			case NTV2_FBF_RGBA:
			case NTV2_FBF_ABGR:
			case NTV2_FBF_10BIT_RGB:
			case NTV2_FBF_10BIT_RGB_PACKED:
			case NTV2_FBF_10BIT_DPX:
			case NTV2_FBF_10BIT_YCBCRA	:
			case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
				result.numPixels = HD_NUMCOMPONENTPIXELS_2K;
				result.linePitch = HD_NUMCOMPONENTPIXELS_2K;
				break;

			case NTV2_FBF_24BIT_RGB:
			case NTV2_FBF_24BIT_BGR:
				result.numPixels = HD_NUMCOMPONENTPIXELS_2K;
				result.linePitch = RGB24LINEPITCH_2048;
				break;

			case NTV2_FBF_48BIT_RGB:
				result.numPixels = HD_NUMCOMPONENTPIXELS_2K;
				result.linePitch = RGB48LINEPITCH_2048;
				break;

			case NTV2_FBF_10BIT_ARGB:
				result.numPixels = HD_NUMCOMPONENTPIXELS_2K;
				result.linePitch = HD_NUMCOMPONENTPIXELS_2K*4/3;
				break;

			case NTV2_FBF_16BIT_ARGB:
				result.numPixels = HD_NUMCOMPONENTPIXELS_2K;
				result.linePitch = HD_NUMCOMPONENTPIXELS_2K*2;
				break;

			default:
				return nullDescriptor;
		}
	}	//	if in2Kby1080

	return result;

}	//	GetFormatDescriptor (NTV2Standard)


NTV2FormatDescriptor GetFormatDescriptor (const NTV2VideoFormat			inVideoFormat,
											const NTV2FrameBufferFormat	inFrameBufferFormat,
											const bool					inVANCenabled,
											const bool					inWideVANC)
{
	NTV2FormatDescriptor	result	(::GetFormatDescriptor (::GetNTV2StandardFromVideoFormat (inVideoFormat),
															inFrameBufferFormat,
															inVANCenabled,
															NTV2_IS_2K_1080_VIDEO_FORMAT (inVideoFormat) || NTV2_IS_4K_4096_VIDEO_FORMAT (inVideoFormat),
															inWideVANC));
	if (NTV2_IS_QUAD_FRAME_FORMAT (inVideoFormat))
	{
		result.numLines  *= 2;
		result.numPixels *= 2;
		result.linePitch *= 2;
	}

	return result;

}	//	GetFormatDescriptor (NTV2VideoFormat)


// SMPTE line numbers
NTV2SmpteLineNumber GetSmpteLineNumber(NTV2Standard standard)
{
	NTV2SmpteLineNumber lineNumber;
	lineNumber.firstFieldTop = true;

	switch ( standard )
	{
	case NTV2_STANDARD_525:
		lineNumber.smpteFirstActiveLine = 21;
		lineNumber.smpteSecondActiveLine = 283;
		lineNumber.firstFieldTop = false;
		break;

	case NTV2_STANDARD_625:
		lineNumber.smpteFirstActiveLine = 23;
		lineNumber.smpteSecondActiveLine = 336;
		break;

	case NTV2_STANDARD_720:
		lineNumber.smpteFirstActiveLine = 26;
		lineNumber.smpteSecondActiveLine = 27;
		break;

	case NTV2_STANDARD_1080:
		lineNumber.smpteFirstActiveLine = 21;
		lineNumber.smpteSecondActiveLine = 584;
		break;

	case NTV2_STANDARD_1080p:
		lineNumber.smpteFirstActiveLine = 42;
		lineNumber.smpteSecondActiveLine = 43;
		break;

	case NTV2_STANDARD_2K:
		lineNumber.smpteFirstActiveLine = 211;
		lineNumber.smpteSecondActiveLine = 1201;
		break;

	default:
		lineNumber.smpteFirstActiveLine = 0;
		lineNumber.smpteSecondActiveLine = 0;
		break;
	}

	return lineNumber;
}


AJA_LOCAL_STATIC const char * NTV2VideoFormatStrings [NTV2_MAX_NUM_VIDEO_FORMATS] =
{
	"",							//	NTV2_FORMAT_UNKNOWN						//	0		//	not used
	"1080i 50.00",				//	NTV2_FORMAT_1080i_5000					//	1
	"1080i 59.94",				//	NTV2_FORMAT_1080i_5994					//	2
	"1080i 60.00",				//	NTV2_FORMAT_1080i_6000					//	3
	"720p 59.94",				//	NTV2_FORMAT_720p_5994					//	4
	"720p 60.00",				//	NTV2_FORMAT_720p_6000					//	5
	"1080psf 23.98",			//	NTV2_FORMAT_1080psf_2398				//	6
	"1080psf 24.00",			//	NTV2_FORMAT_1080psf_2400				//	7
	"1080p 29.97",				//	NTV2_FORMAT_1080p_2997					//	8
	"1080p 30.00",				//	NTV2_FORMAT_1080p_3000					//	9
	"1080p 25.00",				//	NTV2_FORMAT_1080p_2500					//	10
	"1080p 23.98",				//	NTV2_FORMAT_1080p_2398					//	11
	"1080p 24.00",				//	NTV2_FORMAT_1080p_2400					//	12
	"1080p 2K 23.98",			//	NTV2_FORMAT_1080p_2K_2398				//	13
	"1080p 2K 24.00",			//	NTV2_FORMAT_1080p_2K_2400				//	14
	"1080psf 2K 23.98",			//	NTV2_FORMAT_1080psf_2K_2398				//	15
	"1080psf 2K 24.00",			//	NTV2_FORMAT_1080psf_2K_2400				//	16
	"720p 50",					//	NTV2_FORMAT_720p_5000					//	17
	"1080p 50.00b",				//	NTV2_FORMAT_1080p_5000					//	18
	"1080p 59.94b",				//	NTV2_FORMAT_1080p_5994					//	19
	"1080p 60.00b",				//	NTV2_FORMAT_1080p_6000					//	20
	"720p 23.98",				//	NTV2_FORMAT_720p_2398					//	21
	"720p 25.00",				//	NTV2_FORMAT_720p_2500					//	22
	"1080p 50.00a",				//	NTV2_FORMAT_1080p_5000_A				//	23
	"1080p 59.94a",				//	NTV2_FORMAT_1080p_5994_A				//	24
	"1080p 60.00a",				//	NTV2_FORMAT_1080p_6000_A				//	25
	"1080p 2K 25",				//	NTV2_FORMAT_1080p_2K_2500				//	26
	"1080psf 2K 25",			//	NTV2_FORMAT_1080psf_2K_2500				//	27
	"1080psf 25",				//	NTV2_FORMAT_1080psf_2500_2				//	28
	"1080psf 29.97",			//	NTV2_FORMAT_1080psf_2997_2				//	29
	"1080psf 30",				//	NTV2_FORMAT_1080psf_3000_2				//	30
	"",							//	NTV2_FORMAT_END_HIGH_DEF_FORMATS		//	31		// not used
	"525 59.94",				//	NTV2_FORMAT_525_5994					//	32
	"625 50.00",				//	NTV2_FORMAT_625_5000					//	33
	"525 23.98",				//	NTV2_FORMAT_525_2398					//	34
	"525 24.00",				//	NTV2_FORMAT_525_2400					//	35		// not used
	"525psf 29.97",				//	NTV2_FORMAT_525psf_2997					//	36
	"625psf 25",				//	NTV2_FORMAT_625psf_2500					//	37
	"",							//	NTV2_FORMAT_END_STANDARD_DEF_FORMATS	//	38		// not used
	"",							//											//	39		// not used
	"",							//											//	40		// not used
	"",							//											//	41		// not used
	"",							//											//	42		// not used
	"",							//											//	43		// not used
	"",							//											//	44		// not used
	"",							//											//	45		// not used
	"",							//											//	46		// not used
	"",							//											//	47		// not used
	"",							//											//	48		// not used
	"",							//											//	49		// not used
	"",							//											//	50		// not used
	"",							//											//	51		// not used
	"",							//											//	52		// not used
	"",							//											//	53		// not used
	"",							//											//	54		// not used
	"",							//											//	55		// not used
	"",							//											//	56		// not used
	"",							//											//	57		// not used
	"",							//											//	58		// not used
	"",							//											//	59		// not used
	"",							//											//	60		// not used
	"",							//											//	61		// not used
	"",							//											//	62		// not used
	"",							//											//	63		// not used
	"1556psf 2K 14.98",			//	NTV2_FORMAT_2K_1498						//	64
	"1556psf 2K 15.00",			//	NTV2_FORMAT_2K_1500						//	65
	"1556psf 2K 23.98",			//	NTV2_FORMAT_2K_2398						//	66
	"1556psf 2K 24.00",			//	NTV2_FORMAT_2K_2400						//	67
	"1556psf 2K 25.00",			//	NTV2_FORMAT_2K_2500						//	68
	"",							//	NTV2_FORMAT_END_2K_DEF_FORMATS			//	69		// not used
	"",							//											//	70		// not used
	"",							//											//	71		// not used
	"",							//											//	72		// not used
	"",							//											//	73		// not used
	"",							//											//	74		// not used
	"",							//											//	75		// not used
	"",							//											//	76		// not used
	"",							//											//	77		// not used
	"",							//											//	78		// not used
	"",							//											//	79		// not used
	"4x1920x1080psf 23.98",		//	NTV2_FORMAT_4x1920x1080psf_2398			//	80
	"4x1920x1080psf 24.00",		//	NTV2_FORMAT_4x1920x1080psf_2400			//	81
	"4x1920x1080psf 25.00",		//	NTV2_FORMAT_4x1920x1080psf_2500			//	82
	"4x1920x1080p 23.98",		//	NTV2_FORMAT_4x1920x1080p_2398			//	83
	"4x1920x1080p 24.00",		//	NTV2_FORMAT_4x1920x1080p_2400			//	84
	"4x1920x1080p 25.00",		//	NTV2_FORMAT_4x1920x1080p_2500			//	85
	"4x2048x1080psf 23.98",		//	NTV2_FORMAT_4x2048x1080psf_2398			//	86
	"4x2048x1080psf 24.00",		//	NTV2_FORMAT_4x2048x1080psf_2400			//	87
	"4x2048x1080psf 25.00",		//	NTV2_FORMAT_4x2048x1080psf_2500			//	88
	"4x2048x1080p 23.98",		//	NTV2_FORMAT_4x2048x1080p_2398			//	89
	"4x2048x1080p 24.00",		//	NTV2_FORMAT_4x2048x1080p_2400			//	90
	"4x2048x1080p 25.00",		//	NTV2_FORMAT_4x2048x1080p_2500			//	91
	"4x1920x1080p 29.97",		//	NTV2_FORMAT_4x1920x1080p_2997			//	92
	"4x1920x1080p 30.00",		//	NTV2_FORMAT_4x1920x1080p_3000			//	93
	"4x1920x1080psf 29.97",		//	NTV2_FORMAT_4x1920x1080psf_2997			//	94		//	not supported
	"4x1920x1080psf 30.00",		//	NTV2_FORMAT_4x1920x1080psf_3000			//	95		//	not supported
	"4x2048x1080p 29.97",		//	NTV2_FORMAT_4x2048x1080p_2997			//	96
	"4x2048x1080p 30.00",		//	NTV2_FORMAT_4x2048x1080p_3000			//	97
	"4x2048x1080psf 29.97",		//	NTV2_FORMAT_4x2048x1080psf_2997			//	98		//	not supported
	"4x2048x1080psf 30.00",		//	NTV2_FORMAT_4x2048x1080psf_3000			//	99		//	not supported
	"4x1920x1080p 50.00",		//	NTV2_FORMAT_4x1920x1080p_5000			//	100
	"4x1920x1080p 59.94",		//	NTV2_FORMAT_4x1920x1080p_5994			//	101
	"4x1920x1080p 60.00",		//	NTV2_FORMAT_4x1920x1080p_6000			//	102
	"4x2048x1080p 50.00",		//	NTV2_FORMAT_4x2048x1080p_5000			//	103
	"4x2048x1080p 59.94",		//	NTV2_FORMAT_4x2048x1080p_5994			//	104
	"4x2048x1080p 60.00",		//	NTV2_FORMAT_4x2048x1080p_6000			//	105
	"4x2048x1080p 47.95",		//	NTV2_FORMAT_4x2048x1080p_4795			//	106
	"4x2048x1080p 48.00",		//	NTV2_FORMAT_4x2048x1080p_4800			//	107
	"4x2048x1080p 119.88",		//	NTV2_FORMAT_4x2048x1080p_11988			//	108
	"4x2048x1080p 120.00",		//	NTV2_FORMAT_4x2048x1080p_12000			//	109
	"2048x1080p 60.00a",		//	NTV2_FORMAT_1080p_2K_6000				//	110
	"2048x1080p 59.94a",		//	NTV2_FORMAT_1080p_2K_5994				//	111
	"2048x1080p 29.97",			//	NTV2_FORMAT_1080p_2K_2997				//	112
	"2048x1080p 30.00",			//	NTV2_FORMAT_1080p_2K_3000				//	113
	"2048x1080p 50.00a",		//	NTV2_FORMAT_1080p_2K_5000				//	114
	"2048x1080p 47.95a",		//	NTV2_FORMAT_1080p_2K_4795				//	115
	"2048x1080p 48.00a"			//	NTV2_FORMAT_1080p_2K_4800				//	116
};

#if !defined (NTV2_DEPRECATE)
	AJA_LOCAL_STATIC const char * NTV2VideoStandardStrings [NTV2_NUM_STANDARDS] =
	{
		"1080i",					//	NTV2_STANDARD_1080						//	0
		"720p",						//	NTV2_STANDARD_720						//	1
		"525",						//	NTV2_STANDARD_525						//	2
		"625",						//	NTV2_STANDARD_625						//	3
		"1080p",					//	NTV2_STANDARD_1080p						//	4
		"2k"						//	NTV2_STANDARD_2K						//	5
	};


	AJA_LOCAL_STATIC const char * NTV2PixelFormatStrings [NTV2_FBF_NUMFRAMEBUFFERFORMATS] =
	{
		"10BIT_YCBCR",						//	NTV2_FBF_10BIT_YCBCR			//	0
		"8BIT_YCBCR",						//	NTV2_FBF_8BIT_YCBCR				//	1
		"ARGB",								//	NTV2_FBF_ARGB					//	2
		"RGBA",								//	NTV2_FBF_RGBA					//	3
		"10BIT_RGB",						//	NTV2_FBF_10BIT_RGB				//	4
		"8BIT_YCBCR_YUY2",					//	NTV2_FBF_8BIT_YCBCR_YUY2		//	5
		"ABGR",								//	NTV2_FBF_ABGR					//	6
		"10BIT_DPX",						//	NTV2_FBF_10BIT_DPX				//	7
		"10BIT_YCBCR_DPX",					//	NTV2_FBF_10BIT_YCBCR_DPX		//	8
		"",									//	NTV2_FBF_8BIT_DVCPRO			//	9
		"",									//	NTV2_FBF_8BIT_QREZ				//	10
		"",									//	NTV2_FBF_8BIT_HDV				//	11
		"24BIT_RGB",						//	NTV2_FBF_24BIT_RGB				//	12
		"24BIT_BGR",						//	NTV2_FBF_24BIT_BGR				//	13
		"",									//	NTV2_FBF_10BIT_YCBCRA			//	14
		"DPX_LITTLEENDIAN",					//	NTV2_FBF_10BIT_DPX_LITTLEENDIAN	//	15
		"48BIT_RGB",						//	NTV2_FBF_48BIT_RGB				//	16
		"",									//	NTV2_FBF_PRORES					//	17
		"",									//	NTV2_FBF_PRORES_DVCPRO			//	18
		"",									//	NTV2_FBF_PRORES_HDV				//	19
		"",									//	NTV2_FBF_10BIT_RGB_PACKED		//	20
		"",									//	NTV2_FBF_10BIT_ARGB				//	21
		"",									//	NTV2_FBF_16BIT_ARGB				//	22
		"",									//	NTV2_FBF_UNUSED_23				//	23
		"10BIT_RAW_RGB",					//	NTV2_FBF_10BIT_RAW_RGB			//	24
		"10BIT_RAW_YCBCR"					//	NTV2_FBF_10BIT_RAW_YCBCR		//	25
	};
#endif	//	!defined (NTV2_DEPRECATE)



//	More UI-friendly versions of above (used in Cables app)...
AJA_LOCAL_STATIC const char * frameBufferFormats [NTV2_FBF_NUMFRAMEBUFFERFORMATS] =
{
	"10 Bit YCbCr",						//	NTV2_FBF_10BIT_YCBCR			//	0
	"8 Bit YCbCr - UYVY",				//	NTV2_FBF_8BIT_YCBCR				//	1
	"8 Bit ARGB",						//	NTV2_FBF_ARGB					//	2
	"8 Bit RGBA",						//	NTV2_FBF_RGBA					//	3
	"10 Bit RGB",						//	NTV2_FBF_10BIT_RGB				//	4
	"8 Bit YCbCr - YUY2",				//	NTV2_FBF_8BIT_YCBCR_YUY2		//	5
	"8 Bit ABGR",						//	NTV2_FBF_ABGR					//	6
	"10 Bit RGB - DPX compatible",		//	NTV2_FBF_10BIT_DPX				//	7
	"10 Bit YCbCr - DPX compatible",	//	NTV2_FBF_10BIT_YCBCR_DPX		//	8
	"8 Bit DVCPro YCbCr - UYVY",		//	NTV2_FBF_8BIT_DVCPRO			//	9
	"8 Bit QRez YCbCr - UYVY",			//	NTV2_FBF_8BIT_QREZ				//	10
	"8 Bit HDV YCbCr - UYVY",			//	NTV2_FBF_8BIT_HDV				//	11
	"8 Bit RGB",						//	NTV2_FBF_24BIT_RGB				//	12
	"8 Bit BGR",						//	NTV2_FBF_24BIT_BGR				//	13
	"10 Bit YCbCrA",					//	NTV2_FBF_10BIT_YCBCRA			//	14
	"10 Bit RGB - DPX Little Endian",	//	NTV2_FBF_10BIT_DPX_LITTLEENDIAN	//	15
	"48 Bit RGB",						//	NTV2_FBF_48BIT_RGB				//	16
	"10 Bit YCbCr - Compressed",		//	NTV2_FBF_PRORES					//	17
	"10 Bit YCbCr DVCPro - Compressed",	//	NTV2_FBF_PRORES_DVCPRO			//	18
	"10 Bit YCbCr HDV - Compressed",	//	NTV2_FBF_PRORES_HDV				//	19
	"10 Bit RGB Packed",				//	NTV2_FBF_10BIT_RGB_PACKED		//	20
	"10 Bit ARGB",						//	NTV2_FBF_10BIT_ARGB				//	21
	"16 Bit ARGB",						//	NTV2_FBF_16BIT_ARGB				//	22
	"Unknown 23",						//	NTV2_FBF_UNKNOWN_23				//	23
	"10 Bit Raw RGB",					//	NTV2_FBF_10BIT_RGB				//	24
	"10 Bit Raw YCbCr",					//	NTV2_FBF_10BIT_YCBCR			//	25
};


AJA_LOCAL_STATIC const char * NTV2FrameRateStrings [NTV2_NUM_FRAMERATES] =
{
	"Unknown",							//	NTV2_FRAMERATE_UNKNOWN			//	0
	"60.00",							//	NTV2_FRAMERATE_6000				//	1
	"59.94",							//	NTV2_FRAMERATE_5994				//	2
	"30.00",							//	NTV2_FRAMERATE_3000				//	3
	"29.97",							//	NTV2_FRAMERATE_2997				//	4
	"25.00",							//	NTV2_FRAMERATE_2500				//	5
	"24.00",							//	NTV2_FRAMERATE_2400				//	6
	"23.98",							//	NTV2_FRAMERATE_2398				//	7
	"50.00",							//	NTV2_FRAMERATE_5000				//	8
	"48.00",							//	NTV2_FRAMERATE_4800				//	9
	"47.95",							//	NTV2_FRAMERATE_4795				//	10
	"120.00",							//	NTV2_FRAMERATE_12000			//	11
	"119.88",							//	NTV2_FRAMERATE_11988			//	12
	"15.00",							//	NTV2_FRAMERATE_1500				//	13
	"14.98",							//	NTV2_FRAMERATE_1498				//	14
	"19.00",							//	NTV2_FRAMERATE_1900				//	15
	"18.98",							//	NTV2_FRAMERATE_1898				//	16
	"18.00",							//	NTV2_FRAMERATE_1800				//	17
	"17.98"								//	NTV2_FRAMERATE_1798				//	18
};

// Extracts a channel pair or all channels from the
// NTV2 channel buffer that is retrieved from the hardware.
int RecordCopyAudio(PULWord pAja, PULWord pSR, int iStartSample, int iNumBytes, int iChan0,
                    int iNumChans, bool bKeepAudio24Bits)
{
    const int SAMPLE_SIZE = NTV2_NUMAUDIO_CHANNELS * NTV2_AUDIOSAMPLESIZE;

    // Insurance to prevent bogus array sizes causing havoc
//    if (iNumBytes > 48048)      // 23.98 == 2002 * 24
//        iNumBytes = 48048;

    // Adjust the offset of the first valid channel
    if (iStartSample)
    {
        iChan0 += (NTV2_NUMAUDIO_CHANNELS - iStartSample);
    }

    // Driver records audio to offset 24 bytes
    PULWord pIn = &pAja[NTV2_NUMAUDIO_CHANNELS];
    UWord * puwOut = (UWord *) pSR;

    // If our transfer size has a remainder and our chans are in it,
    // adjust number samples
    int iNumSamples = iNumBytes / SAMPLE_SIZE;
    int iMod = (iNumBytes % SAMPLE_SIZE) / 4;
    if (iMod > iChan0)
        iNumSamples++;
    // else if we have remainder with chans && chans total > number of chans
    // reduce start offset by the number of chans
    else if (iMod && iChan0 >= NTV2_NUMAUDIO_CHANNELS)
    {
        iNumSamples++;
        iChan0 -= NTV2_NUMAUDIO_CHANNELS;
    }
    // else if no remainder but start sample adjustment gives more chans
    // than number of chans, drop the start offset back by num chans
    else if (iChan0 >= NTV2_NUMAUDIO_CHANNELS)
    {
        iChan0 -= NTV2_NUMAUDIO_CHANNELS;
    }

    // Copy incoming audio to the outgoing array
    if (bKeepAudio24Bits)
    {
        for (int s = 0; s < iNumSamples; s++)
        {
            for (int c = iChan0; c < iChan0 + iNumChans; c++)
            {
                *pSR++ = pIn[c];
            }

            pIn += NTV2_NUMAUDIO_CHANNELS;
        }
    }
    else    // convert audio to 16 bits
    {
        for (int s = 0; s < iNumSamples; s++)
        {
            for (int c = iChan0; c < iChan0 + iNumChans; c++)
            {
                *puwOut++ = (UWord) (pIn[c] >> 16);
            }

            pIn += NTV2_NUMAUDIO_CHANNELS;
        }
    }

    return iNumSamples;
}

#include "math.h"
// M_PI is defined on RedHat Linux 9 in math.h
#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif


ULWord	AddAudioTone (ULWord*             audioBuffer,
					 ULWord&             currentSample,
					 ULWord              numSamples,
					 double              inSampleRate,
					 double              amplitude,
					 double              frequency,
					 ULWord              numBits,
					 bool                endianConvert,
					 ULWord	   		     numChannels)
{
	double j = currentSample;
	double cycleLength = inSampleRate / frequency;
	double scale = (double)((ULWord)(1<<(numBits-1)));
	scale -= 1.0;

	if (audioBuffer)
	{
		for (ULWord i = 0; i <numSamples; i++)
		{
			float nextFloat = (float)(sin(j / cycleLength * (M_PI * 2.0)) * amplitude);
			ULWord value = static_cast<ULWord>((nextFloat * scale) + (float)0.5);

			if ( endianConvert )
				value = NTV2EndianSwap32(value);
			//odprintf("%f",(float)(LWord)value/(float)0x7FFFFFFF);

			for (ULWord channel = 0; channel< numChannels; channel++)
				*audioBuffer++ = value;

			j += 1.0;
			if (j > cycleLength)
				j -= cycleLength;
			currentSample++;
		}	//	for each sample
	}	//	if audioBuffer

	return numSamples * 4 * numChannels;

}	//	AddAudioTone (ULWord)


ULWord	AddAudioTone (UWord*             audioBuffer,
					 ULWord&             currentSample,
					 ULWord              numSamples,
					 double              inSampleRate,
					 double              amplitude,
					 double              frequency,
					 ULWord              numBits,
					 bool                endianConvert,
					 ULWord	   		     numChannels)
{
	double j = currentSample;
	double cycleLength = inSampleRate / frequency;
	double scale = (double)((ULWord)(1<<(numBits-1)));
	scale -= 1.0;

	if (audioBuffer)
	{
		for (ULWord i = 0; i <numSamples; i++)
		{
			float nextFloat = (float)(sin(j / cycleLength * (M_PI * 2.0)) * amplitude);
			UWord value = static_cast<ULWord>((nextFloat * scale) + 0.5);

			if ( endianConvert )
				value = NTV2EndianSwap16(value);

			for (ULWord channel = 0; channel< numChannels; channel++)
				*audioBuffer++ = value;

			j += 1.0;
			if (j > cycleLength)
				j -= cycleLength;
			currentSample++;
		}	//	for each sample
	}	//	if audioBuffer

	return numSamples*2*numChannels;

}	//	AddAudioTone (UWord)


ULWord	AddAudioTone (ULWord*             audioBuffer,
					  ULWord&             currentSample,	// C++ doesn't allow arrays of references.
					  ULWord              numSamples,
					  double              inSampleRate,
					  double              amplitude,
					  double*             frequency,		// C++ doesn't allow arrays of references.
					  ULWord              numBits,
					  bool                endianConvert,
					  ULWord   		      numChannels)
{
	double j[kNumAudioChannelsMax];
	double cycleLength[kNumAudioChannelsMax];
	double scale = (double)((ULWord)(1<<(numBits-1)));
	scale -= 1.0;

	for (ULWord channel = 0; channel< numChannels; channel++)
	{
		cycleLength[channel] = inSampleRate / frequency[channel];
		j[channel] = currentSample;
	}

	for (ULWord i = 0; i <numSamples; i++)
	{

		for (ULWord channel = 0; channel< numChannels; channel++)
		{
			float nextFloat = (float)(sin(j[channel] / cycleLength[channel] * (M_PI * 2.0)) * amplitude);
			ULWord value = static_cast<ULWord>((nextFloat * scale) + 0.5);

			if ( endianConvert )
				value = NTV2EndianSwap32(value);

			*audioBuffer++ = value;

			j[channel] += 1.0;
			if (j[channel] > cycleLength[channel])
				j[channel] -= cycleLength[channel];

		}
		currentSample++;
	}
	return numSamples*4*numChannels;
}

ULWord	AddAudioTestPattern (ULWord*            audioBuffer,
							 ULWord&            currentSample,
							 ULWord             numSamples,
							 ULWord             modulus,
							 bool               endianConvert,
							 ULWord   		    numChannels)
{

	for (ULWord i = 0; i <numSamples; i++)
	{
		ULWord value = (currentSample%modulus)<<16;
		if ( endianConvert )
			value = NTV2EndianSwap32(value);
		for (ULWord channel = 0; channel< numChannels; channel++)
		{
			*audioBuffer++ = value;
		}
		currentSample++;
	}
	return numSamples*4*numChannels;

}


std::string NTV2DeviceIDToString (const NTV2DeviceID inValue,	const bool inForRetailDisplay)
{
	switch (inValue)
	{
	#if defined (AJAMac) || defined (MSWindows)
		#if !defined (NTV2_DEPRECATE)
		case BOARD_ID_XENA2:		return inForRetailDisplay ?	"KONA 3"		: "Kona3";
		case BOARD_ID_LHI_DVI:		return inForRetailDisplay ?	"KONA LHi DVI"	: "KonaLHiDVI";
		#endif	//	!defined (NTV2_DEPRECATE)
		case DEVICE_ID_LHI:			return inForRetailDisplay ?	"KONA LHi"		: "KonaLHi";
	#endif
	#if defined (AJAMac)
		case DEVICE_ID_IOEXPRESS:	return inForRetailDisplay ?	"IoExpress"		: "IoExpress";
	#elif defined (MSWindows)
		case DEVICE_ID_IOEXPRESS:	return inForRetailDisplay ?	"KONA IoExpress": "IoExpress";
	#else
		#if !defined (NTV2_DEPRECATE)
		case BOARD_ID_XENA2:		return inForRetailDisplay ?	"Kona3"			: "OEM 2K";
		case BOARD_ID_XENALH:		return inForRetailDisplay ?	"Xena LH"		: "OEM LH";
		case BOARD_ID_XENALS:		return inForRetailDisplay ?	"Xena LS"		: "OEM LS";
		case BOARD_ID_XENAHS:		return inForRetailDisplay ?	"Xena HS"		: "OEM HS";
		case BOARD_ID_XENAHS2:		return inForRetailDisplay ?	"Xena HS2"		: "OEM HS2";
		case BOARD_ID_LHI_DVI:		return inForRetailDisplay ?	"KONA LHi DVI"	: "OEM LHi DVI";
		#endif	//	!defined (NTV2_DEPRECATE)
		case DEVICE_ID_LHI:			return inForRetailDisplay ?	"KONA LHi"		: "OEM LHi";
		case DEVICE_ID_IOEXPRESS:	return inForRetailDisplay ?	"IoExpress"		: "OEM IoExpress";
	#endif
		case DEVICE_ID_NOTFOUND:	return inForRetailDisplay ?	"DEVICE NOT FOUND" : "(Not Found)";
		case DEVICE_ID_CORVID1:		return inForRetailDisplay ?	"Corvid 1"		: "Corvid";
		case DEVICE_ID_CORVID22:	return inForRetailDisplay ?	"Corvid 22"		: "Corvid22";
		case DEVICE_ID_CORVID3G:	return inForRetailDisplay ?	"Corvid 3G"		: "Corvid3G";
		case DEVICE_ID_KONA3G:		return inForRetailDisplay ?	"KONA 3G"		: "Kona3G";
		case DEVICE_ID_KONA3GQUAD:	return inForRetailDisplay ?	"KONA 3G QUAD"	: "Kona3GQuad";	//	Used to be "KONA 3G" for retail display
		case DEVICE_ID_LHE_PLUS:	return inForRetailDisplay ?	"KONA LHe+"		: "KonaLHe+";
		case DEVICE_ID_IOXT:		return inForRetailDisplay ?	"IoXT"			: "IoXT";
		case DEVICE_ID_CORVID24:	return inForRetailDisplay ?	"Corvid 24"		: "Corvid24";
		case DEVICE_ID_TTAP:		return inForRetailDisplay ?	"T-Tap"			: "TTap";
		#if !defined (NTV2_DEPRECATE)
		case BOARD_ID_LHI_T:		return inForRetailDisplay ?	"KONA LHi T"	: "KonaLHiT";
		#endif	//	!defined (NTV2_DEPRECATE)
		case DEVICE_ID_IO4K:		return inForRetailDisplay ?	"Io4K"			: "Io4K";
		case DEVICE_ID_IO4KUFC:		return inForRetailDisplay ?	"Io4K UFC"		: "Io4KUfc";
		case DEVICE_ID_KONA4:		return inForRetailDisplay ?	"KONA 4"		: "Kona4";
		case DEVICE_ID_KONA4UFC:	return inForRetailDisplay ?	"KONA 4 UFC"	: "Kona4Ufc";
		case DEVICE_ID_CORVID88:	return inForRetailDisplay ?	"Corvid 88"		: "Corvid88";
		case DEVICE_ID_CORVID44:	return inForRetailDisplay ?	"Corvid 44"		: "Corvid44";
	    default:					return inForRetailDisplay ?	"Unknown"		: "???";
	}
}


#if !defined (NTV2_DEPRECATE)
	void GetNTV2RetailBoardString (NTV2BoardID inBoardID, std::string & outString)
	{
		outString = ::NTV2DeviceIDToString (inBoardID, true);
	}

	NTV2BoardType GetNTV2BoardTypeForBoardID (NTV2BoardID inBoardID)
	{
		if (inBoardID == BOARD_ID_XENA2)
			return BOARDTYPE_AJAXENA2;
		else
			return BOARDTYPE_NTV2;
	}

	void GetNTV2BoardString (NTV2BoardID inBoardID, string & outName)
	{
		outName = ::NTV2DeviceIDToString (inBoardID);
	}

	std::string NTV2BoardIDToString (const NTV2BoardID inValue,	const bool inForRetailDisplay)
	{
		return NTV2DeviceIDToString (inValue, inForRetailDisplay);
	}

	string frameBufferFormatString (const NTV2FrameBufferFormat inFrameBufferFormat)
	{
		string	result;
		if (inFrameBufferFormat >= 0 && inFrameBufferFormat < NTV2_FBF_NUMFRAMEBUFFERFORMATS)
			result = frameBufferFormats [inFrameBufferFormat];
		return result;
	}
#endif	//	!defined (NTV2_DEPRECATE)


NTV2Channel GetNTV2ChannelForIndex (const ULWord index)
{
	switch(index)
	{
	default:
	case 0:	return NTV2_CHANNEL1;
	case 1:	return NTV2_CHANNEL2;
	case 2:	return NTV2_CHANNEL3;
	case 3:	return NTV2_CHANNEL4;
	case 4: return NTV2_CHANNEL5;
	case 5: return NTV2_CHANNEL6;
	case 6: return NTV2_CHANNEL7;
	case 7: return NTV2_CHANNEL8;
	}
}

ULWord GetIndexForNTV2Channel (const NTV2Channel channel)
{
	switch(channel)
	{
	default:
	case NTV2_CHANNEL1:	return 0;
	case NTV2_CHANNEL2:	return 1;
	case NTV2_CHANNEL3:	return 2;
	case NTV2_CHANNEL4:	return 3;
	case NTV2_CHANNEL5: return 4;
	case NTV2_CHANNEL6: return 5;
	case NTV2_CHANNEL7: return 6;
	case NTV2_CHANNEL8: return 7;
	}
}

NTV2Crosspoint GetNTV2CrosspointChannelForIndex (const ULWord index)
{
	switch(index)
	{
	default:
	case 0:	return NTV2CROSSPOINT_CHANNEL1;
	case 1:	return NTV2CROSSPOINT_CHANNEL2;
	case 2:	return NTV2CROSSPOINT_CHANNEL3;
	case 3:	return NTV2CROSSPOINT_CHANNEL4;
	case 4:	return NTV2CROSSPOINT_CHANNEL5;
	case 5:	return NTV2CROSSPOINT_CHANNEL6;
	case 6:	return NTV2CROSSPOINT_CHANNEL7;
	case 7:	return NTV2CROSSPOINT_CHANNEL8;
	}
}

ULWord GetIndexForNTV2CrosspointChannel (const NTV2Crosspoint channel)
{
	switch(channel)
	{
	default:
	case NTV2CROSSPOINT_CHANNEL1:	return 0;
	case NTV2CROSSPOINT_CHANNEL2:	return 1;
	case NTV2CROSSPOINT_CHANNEL3:	return 2;
	case NTV2CROSSPOINT_CHANNEL4:	return 3;
	case NTV2CROSSPOINT_CHANNEL5:	return 4;
	case NTV2CROSSPOINT_CHANNEL6:	return 5;
	case NTV2CROSSPOINT_CHANNEL7:	return 6;
	case NTV2CROSSPOINT_CHANNEL8:	return 7;
	}
}

NTV2Crosspoint GetNTV2CrosspointInputForIndex (const ULWord index)
{
	switch(index)
	{
	default:
	case 0:	return NTV2CROSSPOINT_INPUT1;
	case 1:	return NTV2CROSSPOINT_INPUT2;
	case 2:	return NTV2CROSSPOINT_INPUT3;
	case 3:	return NTV2CROSSPOINT_INPUT4;
	case 4:	return NTV2CROSSPOINT_INPUT5;
	case 5:	return NTV2CROSSPOINT_INPUT6;
	case 6:	return NTV2CROSSPOINT_INPUT7;
	case 7:	return NTV2CROSSPOINT_INPUT8;
	}
}

ULWord GetIndexForNTV2CrosspointInput (const NTV2Crosspoint channel)
{
	switch(channel)
	{
	default:
	case NTV2CROSSPOINT_INPUT1:	return 0;
	case NTV2CROSSPOINT_INPUT2:	return 1;
	case NTV2CROSSPOINT_INPUT3:	return 2;
	case NTV2CROSSPOINT_INPUT4:	return 3;
	case NTV2CROSSPOINT_INPUT5:	return 4;
	case NTV2CROSSPOINT_INPUT6:	return 5;
	case NTV2CROSSPOINT_INPUT7:	return 6;
	case NTV2CROSSPOINT_INPUT8:	return 7;
	}
}

NTV2Crosspoint GetNTV2CrosspointForIndex (const ULWord index)
{
	switch(index)
	{
	default:
	case 0:	return NTV2CROSSPOINT_CHANNEL1;
	case 1:	return NTV2CROSSPOINT_CHANNEL2;
	case 2:	return NTV2CROSSPOINT_CHANNEL3;
	case 3:	return NTV2CROSSPOINT_CHANNEL4;
	case 4:	return NTV2CROSSPOINT_INPUT1;
	case 5:	return NTV2CROSSPOINT_INPUT2;
	case 6:	return NTV2CROSSPOINT_INPUT3;
	case 7:	return NTV2CROSSPOINT_INPUT4;
	case 8:	return NTV2CROSSPOINT_CHANNEL5;
	case 9:	return NTV2CROSSPOINT_CHANNEL6;
	case 10:return NTV2CROSSPOINT_CHANNEL7;
	case 11:return NTV2CROSSPOINT_CHANNEL8;
	case 12:return NTV2CROSSPOINT_INPUT5;
	case 13:return NTV2CROSSPOINT_INPUT6;
	case 14:return NTV2CROSSPOINT_INPUT7;
	case 15:return NTV2CROSSPOINT_INPUT8;
	}
}

ULWord GetIndexForNTV2Crosspoint (const NTV2Crosspoint channel)
{
	switch(channel)
	{
	default:
	case NTV2CROSSPOINT_CHANNEL1:	return 0;
	case NTV2CROSSPOINT_CHANNEL2:	return 1;
	case NTV2CROSSPOINT_CHANNEL3:	return 2;
	case NTV2CROSSPOINT_CHANNEL4:	return 3;
	case NTV2CROSSPOINT_INPUT1:		return 4;
	case NTV2CROSSPOINT_INPUT2:		return 5;
	case NTV2CROSSPOINT_INPUT3:		return 6;
	case NTV2CROSSPOINT_INPUT4:		return 7;
	case NTV2CROSSPOINT_CHANNEL5:	return 8;
	case NTV2CROSSPOINT_CHANNEL6:	return 9;
	case NTV2CROSSPOINT_CHANNEL7:	return 10;
	case NTV2CROSSPOINT_CHANNEL8:	return 11;
	case NTV2CROSSPOINT_INPUT5:		return 12;
	case NTV2CROSSPOINT_INPUT6:		return 13;
	case NTV2CROSSPOINT_INPUT7:		return 14;
	case NTV2CROSSPOINT_INPUT8:		return 15;
	}
}


bool IsNTV2CrosspointInput (const NTV2Crosspoint inChannel)
{
	return NTV2_IS_INPUT_CROSSPOINT (inChannel);
}


bool IsNTV2CrosspointOutput (const NTV2Crosspoint inChannel)
{
	return NTV2_IS_OUTPUT_CROSSPOINT (inChannel);
}


NTV2EmbeddedAudioInput NTV2ChannelToEmbeddedAudioInput (const NTV2Channel inChannel)
{
	assert ((NTV2Channel)NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_1 == NTV2_CHANNEL1);
	assert ((NTV2Channel)NTV2_MAX_NUM_EmbeddedAudioInputs == NTV2_MAX_NUM_CHANNELS);
	return static_cast <NTV2EmbeddedAudioInput> (inChannel);
}


NTV2AudioSystem NTV2ChannelToAudioSystem (const NTV2Channel inChannel)
{
	assert ((NTV2Channel)NTV2_AUDIOSYSTEM_1 == NTV2_CHANNEL1);
	assert ((NTV2Channel)NTV2_MAX_NUM_AudioSystemEnums == NTV2_MAX_NUM_CHANNELS);
	return static_cast <NTV2AudioSystem> (inChannel);
}


NTV2EmbeddedAudioInput NTV2InputSourceToEmbeddedAudioInput (const NTV2InputSource inInputSource)
{
	#if defined (NTV2_DEPRECATE)
		static const NTV2EmbeddedAudioInput	gInputSourceToEmbeddedAudioInputs []	= {	NTV2_MAX_NUM_EmbeddedAudioInputs,	NTV2_MAX_NUM_EmbeddedAudioInputs,	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_1,
																						NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_2,	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_3,	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_4,
																						NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_5,	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_6,	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_7,
																						NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_8,	NTV2_MAX_NUM_EmbeddedAudioInputs};
	#else	//	else !defined (NTV2_DEPRECATE)
		static const NTV2EmbeddedAudioInput	gInputSourceToEmbeddedAudioInputs []	= {	/* NTV2_INPUTSOURCE_SDI1 */			NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_1,
																						/* NTV2_INPUTSOURCE_ANALOG */		NTV2_MAX_NUM_EmbeddedAudioInputs,
																						/* NTV2_INPUTSOURCE_SDI2 */			NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_2,
																						/* NTV2_INPUTSOURCE_HDMI */			NTV2_MAX_NUM_EmbeddedAudioInputs,
																						/* NTV2_INPUTSOURCE_DUALLINK1 */	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_1,
																						/* NTV2_INPUTSOURCE_DUALLINK2 */	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_2,
																						/* NTV2_INPUTSOURCE_SDI3 */			NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_3,
																						/* NTV2_INPUTSOURCE_SDI4 */			NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_4,
																						/* NTV2_INPUTSOURCE_DUALLINK3 */	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_3,
																						/* NTV2_INPUTSOURCE_DUALLINK4 */	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_4,
																						/* NTV2_INPUTSOURCE_SDI1_DS2 */		NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_1,
																						/* NTV2_INPUTSOURCE_SDI2_DS2 */		NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_2,
																						/* NTV2_INPUTSOURCE_SDI3_DS2 */		NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_3,
																						/* NTV2_INPUTSOURCE_SDI4_DS2 */		NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_4,
																						/* NTV2_INPUTSOURCE_SDI5 */			NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_5,
																						/* NTV2_INPUTSOURCE_SDI6 */			NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_6,
																						/* NTV2_INPUTSOURCE_SDI7 */			NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_7,
																						/* NTV2_INPUTSOURCE_SDI8 */			NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_8,
																						/* NTV2_INPUTSOURCE_SDI5_DS2 */		NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_5,
																						/* NTV2_INPUTSOURCE_SDI6_DS2 */		NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_6,
																						/* NTV2_INPUTSOURCE_SDI7_DS2 */		NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_7,
																						/* NTV2_INPUTSOURCE_SDI8_DS2 */		NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_8,
																						/* NTV2_INPUTSOURCE_DUALLINK5 */	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_5,
																						/* NTV2_INPUTSOURCE_DUALLINK6 */	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_6,
																						/* NTV2_INPUTSOURCE_DUALLINK7 */	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_7,
																						/* NTV2_INPUTSOURCE_DUALLINK8 */	NTV2_EMBEDDED_AUDIO_INPUT_VIDEO_8,	NTV2_MAX_NUM_EmbeddedAudioInputs};
	#endif	//	else !defined (NTV2_DEPRECATE)

	if (inInputSource < NTV2_NUM_INPUTSOURCES  &&  inInputSource < (int)(sizeof (gInputSourceToEmbeddedAudioInputs) / sizeof (NTV2EmbeddedAudioInput)))
		return gInputSourceToEmbeddedAudioInputs [inInputSource];
	else
		return NTV2_MAX_NUM_EmbeddedAudioInputs;

}	//	InputSourceToEmbeddedAudioInput


NTV2Crosspoint NTV2ChannelToInputChannelSpec (const NTV2Channel inChannel)
{
	static const NTV2Crosspoint	gChannelToInputChannelSpec []	= {	NTV2CROSSPOINT_INPUT1,	NTV2CROSSPOINT_INPUT2,	NTV2CROSSPOINT_INPUT3,	NTV2CROSSPOINT_INPUT4,
																	NTV2CROSSPOINT_INPUT5,	NTV2CROSSPOINT_INPUT6,	NTV2CROSSPOINT_INPUT7,	NTV2CROSSPOINT_INPUT8, NTV2_NUM_CROSSPOINTS};
	if (inChannel >= NTV2_CHANNEL1 && inChannel < NTV2_MAX_NUM_CHANNELS)
		return gChannelToInputChannelSpec [inChannel];
	else
		return NTV2_NUM_CROSSPOINTS;
}


NTV2Crosspoint NTV2ChannelToOutputChannelSpec (const NTV2Channel inChannel)
{
	static const NTV2Crosspoint	gChannelToOutputChannelSpec []	= {	NTV2CROSSPOINT_CHANNEL1,	NTV2CROSSPOINT_CHANNEL2,	NTV2CROSSPOINT_CHANNEL3,	NTV2CROSSPOINT_CHANNEL4,
																	NTV2CROSSPOINT_CHANNEL5,	NTV2CROSSPOINT_CHANNEL6,	NTV2CROSSPOINT_CHANNEL7,	NTV2CROSSPOINT_CHANNEL8, NTV2_NUM_CROSSPOINTS};
	if (inChannel >= NTV2_CHANNEL1 && inChannel < NTV2_MAX_NUM_CHANNELS)
		return gChannelToOutputChannelSpec [inChannel];
	else
		return NTV2_NUM_CROSSPOINTS;
}


NTV2Crosspoint NTV2InputSourceToChannelSpec (const NTV2InputSource inInputSource)
{
#if defined (NTV2_DEPRECATE)
	static const NTV2Crosspoint	gInputSourceToChannelSpec []	= { /* NTV2_INPUTSOURCE_ANALOG */		NTV2CROSSPOINT_INPUT1,
																	/* NTV2_INPUTSOURCE_HDMI */			NTV2CROSSPOINT_INPUT1,
																	/* NTV2_INPUTSOURCE_SDI1 */			NTV2CROSSPOINT_INPUT1,
																	/* NTV2_INPUTSOURCE_SDI2 */			NTV2CROSSPOINT_INPUT2,
																	/* NTV2_INPUTSOURCE_SDI3 */			NTV2CROSSPOINT_INPUT3,
																	/* NTV2_INPUTSOURCE_SDI4 */			NTV2CROSSPOINT_INPUT4,
																	/* NTV2_INPUTSOURCE_SDI5 */			NTV2CROSSPOINT_INPUT5,
																	/* NTV2_INPUTSOURCE_SDI6 */			NTV2CROSSPOINT_INPUT6,
																	/* NTV2_INPUTSOURCE_SDI7 */			NTV2CROSSPOINT_INPUT7,
																	/* NTV2_INPUTSOURCE_SDI8 */			NTV2CROSSPOINT_INPUT8,
																	/* NTV2_NUM_INPUTSOURCES */			NTV2_NUM_CROSSPOINTS};
	
#else	//	else !defined (NTV2_DEPRECATE)
	static const NTV2Crosspoint	gInputSourceToChannelSpec []	= {	/* NTV2_INPUTSOURCE_SDI1 */			NTV2CROSSPOINT_INPUT1,
																	/* NTV2_INPUTSOURCE_ANALOG */		NTV2CROSSPOINT_INPUT1,
																	/* NTV2_INPUTSOURCE_SDI2 */			NTV2CROSSPOINT_INPUT2,
																	/* NTV2_INPUTSOURCE_HDMI */			NTV2CROSSPOINT_INPUT1,
																	/* NTV2_INPUTSOURCE_DUALLINK1 */	NTV2CROSSPOINT_INPUT1,
																	/* NTV2_INPUTSOURCE_DUALLINK2 */	NTV2CROSSPOINT_INPUT2,
																	/* NTV2_INPUTSOURCE_SDI3 */			NTV2CROSSPOINT_INPUT3,
																	/* NTV2_INPUTSOURCE_SDI4 */			NTV2CROSSPOINT_INPUT4,
																	/* NTV2_INPUTSOURCE_DUALLINK3 */	NTV2CROSSPOINT_INPUT3,
																	/* NTV2_INPUTSOURCE_DUALLINK4 */	NTV2CROSSPOINT_INPUT4,
																	/* NTV2_INPUTSOURCE_SDI1_DS2 */		NTV2CROSSPOINT_INPUT1,
																	/* NTV2_INPUTSOURCE_SDI2_DS2 */		NTV2CROSSPOINT_INPUT2,
																	/* NTV2_INPUTSOURCE_SDI3_DS2 */		NTV2CROSSPOINT_INPUT3,
																	/* NTV2_INPUTSOURCE_SDI4_DS2 */		NTV2CROSSPOINT_INPUT4,
																	/* NTV2_INPUTSOURCE_SDI5 */			NTV2CROSSPOINT_INPUT5,
																	/* NTV2_INPUTSOURCE_SDI6 */			NTV2CROSSPOINT_INPUT6,
																	/* NTV2_INPUTSOURCE_SDI7 */			NTV2CROSSPOINT_INPUT7,
																	/* NTV2_INPUTSOURCE_SDI8 */			NTV2CROSSPOINT_INPUT8,
																	/* NTV2_INPUTSOURCE_SDI5_DS2 */		NTV2CROSSPOINT_INPUT5,
																	/* NTV2_INPUTSOURCE_SDI6_DS2 */		NTV2CROSSPOINT_INPUT6,
																	/* NTV2_INPUTSOURCE_SDI7_DS2 */		NTV2CROSSPOINT_INPUT7,
																	/* NTV2_INPUTSOURCE_SDI8_DS2 */		NTV2CROSSPOINT_INPUT8,
																	/* NTV2_INPUTSOURCE_DUALLINK5 */	NTV2CROSSPOINT_INPUT5,
																	/* NTV2_INPUTSOURCE_DUALLINK6 */	NTV2CROSSPOINT_INPUT6,
																	/* NTV2_INPUTSOURCE_DUALLINK7 */	NTV2CROSSPOINT_INPUT7,
																	/* NTV2_INPUTSOURCE_DUALLINK8 */	NTV2CROSSPOINT_INPUT8,
																	/* NTV2_NUM_INPUTSOURCES */			NTV2_NUM_CROSSPOINTS};
#endif	//	else !defined (NTV2_DEPRECATE)
	
	if (inInputSource < NTV2_NUM_INPUTSOURCES  &&  size_t (inInputSource) < sizeof (gInputSourceToChannelSpec) / sizeof (NTV2Channel))
		return gInputSourceToChannelSpec [inInputSource];
	else
		return NTV2_NUM_CROSSPOINTS;
	
}	//	NTV2InputSourceToChannelSpec


NTV2ReferenceSource NTV2InputSourceToReferenceSource (const NTV2InputSource inInputSource)
{
#if defined (NTV2_DEPRECATE)
	static const NTV2ReferenceSource	gInputSourceToReferenceSource []	= { /* NTV2_INPUTSOURCE_ANALOG */		NTV2_REFERENCE_ANALOG_INPUT,
																				/* NTV2_INPUTSOURCE_HDMI */			NTV2_REFERENCE_HDMI_INPUT,
																				/* NTV2_INPUTSOURCE_SDI1 */			NTV2_REFERENCE_INPUT1,
																				/* NTV2_INPUTSOURCE_SDI2 */			NTV2_REFERENCE_INPUT2,
																				/* NTV2_INPUTSOURCE_SDI3 */			NTV2_REFERENCE_INPUT3,
																				/* NTV2_INPUTSOURCE_SDI4 */			NTV2_REFERENCE_INPUT4,
																				/* NTV2_INPUTSOURCE_SDI5 */			NTV2_REFERENCE_INPUT5,
																				/* NTV2_INPUTSOURCE_SDI6 */			NTV2_REFERENCE_INPUT6,
																				/* NTV2_INPUTSOURCE_SDI7 */			NTV2_REFERENCE_INPUT7,
																				/* NTV2_INPUTSOURCE_SDI8 */			NTV2_REFERENCE_INPUT8,
																				/* NTV2_NUM_INPUTSOURCES */			NTV2_NUM_REFERENCE_INPUTS};

#else	//	else !defined (NTV2_DEPRECATE)
	static const NTV2ReferenceSource	gInputSourceToReferenceSource []	= {	/* NTV2_INPUTSOURCE_SDI1 */			NTV2_REFERENCE_INPUT1,
																				/* NTV2_INPUTSOURCE_ANALOG */		NTV2_REFERENCE_ANALOG_INPUT,
																				/* NTV2_INPUTSOURCE_SDI2 */			NTV2_REFERENCE_INPUT2,
																				/* NTV2_INPUTSOURCE_HDMI */			NTV2_REFERENCE_HDMI_INPUT,
																				/* NTV2_INPUTSOURCE_DUALLINK1 */	NTV2_REFERENCE_INPUT1,
																				/* NTV2_INPUTSOURCE_DUALLINK2 */	NTV2_REFERENCE_INPUT2,
																				/* NTV2_INPUTSOURCE_SDI3 */			NTV2_REFERENCE_INPUT3,
																				/* NTV2_INPUTSOURCE_SDI4 */			NTV2_REFERENCE_INPUT4,
																				/* NTV2_INPUTSOURCE_DUALLINK3 */	NTV2_REFERENCE_INPUT3,
																				/* NTV2_INPUTSOURCE_DUALLINK4 */	NTV2_REFERENCE_INPUT4,
																				/* NTV2_INPUTSOURCE_SDI1_DS2 */		NTV2_REFERENCE_INPUT1,
																				/* NTV2_INPUTSOURCE_SDI2_DS2 */		NTV2_REFERENCE_INPUT2,
																				/* NTV2_INPUTSOURCE_SDI3_DS2 */		NTV2_REFERENCE_INPUT3,
																				/* NTV2_INPUTSOURCE_SDI4_DS2 */		NTV2_REFERENCE_INPUT4,
																				/* NTV2_INPUTSOURCE_SDI5 */			NTV2_REFERENCE_INPUT5,
																				/* NTV2_INPUTSOURCE_SDI6 */			NTV2_REFERENCE_INPUT6,
																				/* NTV2_INPUTSOURCE_SDI7 */			NTV2_REFERENCE_INPUT7,
																				/* NTV2_INPUTSOURCE_SDI8 */			NTV2_REFERENCE_INPUT8,
																				/* NTV2_INPUTSOURCE_SDI5_DS2 */		NTV2_REFERENCE_INPUT5,
																				/* NTV2_INPUTSOURCE_SDI6_DS2 */		NTV2_REFERENCE_INPUT6,
																				/* NTV2_INPUTSOURCE_SDI7_DS2 */		NTV2_REFERENCE_INPUT7,
																				/* NTV2_INPUTSOURCE_SDI8_DS2 */		NTV2_REFERENCE_INPUT8,
																				/* NTV2_INPUTSOURCE_DUALLINK5 */	NTV2_REFERENCE_INPUT5,
																				/* NTV2_INPUTSOURCE_DUALLINK6 */	NTV2_REFERENCE_INPUT6,
																				/* NTV2_INPUTSOURCE_DUALLINK7 */	NTV2_REFERENCE_INPUT7,
																				/* NTV2_INPUTSOURCE_DUALLINK8 */	NTV2_REFERENCE_INPUT8,
																				/* NTV2_NUM_INPUTSOURCES */			NTV2_NUM_REFERENCE_INPUTS};
#endif	//	else !defined (NTV2_DEPRECATE)

	if (NTV2_INPUT_SOURCE_IS_VALID (inInputSource)  &&  size_t (inInputSource) < sizeof (gInputSourceToReferenceSource) / sizeof (NTV2ReferenceSource))
		return gInputSourceToReferenceSource [inInputSource];
	else
		return NTV2_NUM_REFERENCE_INPUTS;

}	//	NTV2InputSourceToReferenceSource


NTV2Channel NTV2InputSourceToChannel (const NTV2InputSource inInputSource)
{
	#if defined (NTV2_DEPRECATE)
		static const NTV2Channel	gInputSourceToChannel []	= { /* NTV2_INPUTSOURCE_ANALOG */		NTV2_MAX_NUM_CHANNELS,
																	/* NTV2_INPUTSOURCE_HDMI */			NTV2_MAX_NUM_CHANNELS,
																	/* NTV2_INPUTSOURCE_SDI1 */			NTV2_CHANNEL1,
																	/* NTV2_INPUTSOURCE_SDI2 */			NTV2_CHANNEL2,
																	/* NTV2_INPUTSOURCE_SDI3 */			NTV2_CHANNEL3,
																	/* NTV2_INPUTSOURCE_SDI4 */			NTV2_CHANNEL4,
																	/* NTV2_INPUTSOURCE_SDI5 */			NTV2_CHANNEL5,
																	/* NTV2_INPUTSOURCE_SDI6 */			NTV2_CHANNEL6,
																	/* NTV2_INPUTSOURCE_SDI7 */			NTV2_CHANNEL7,
																	/* NTV2_INPUTSOURCE_SDI8 */			NTV2_CHANNEL8,
																	/* NTV2_NUM_INPUTSOURCES */			NTV2_MAX_NUM_CHANNELS};
	
	#else	//	else !defined (NTV2_DEPRECATE)
		static const NTV2Channel	gInputSourceToChannel []	= {	/* NTV2_INPUTSOURCE_SDI1 */			NTV2_CHANNEL1,
																	/* NTV2_INPUTSOURCE_ANALOG */		NTV2_MAX_NUM_CHANNELS,
																	/* NTV2_INPUTSOURCE_SDI2 */			NTV2_CHANNEL2,
																	/* NTV2_INPUTSOURCE_HDMI */			NTV2_MAX_NUM_CHANNELS,
																	/* NTV2_INPUTSOURCE_DUALLINK1 */	NTV2_CHANNEL1,
																	/* NTV2_INPUTSOURCE_DUALLINK2 */	NTV2_CHANNEL2,
																	/* NTV2_INPUTSOURCE_SDI3 */			NTV2_CHANNEL3,
																	/* NTV2_INPUTSOURCE_SDI4 */			NTV2_CHANNEL4,
																	/* NTV2_INPUTSOURCE_DUALLINK3 */	NTV2_CHANNEL3,
																	/* NTV2_INPUTSOURCE_DUALLINK4 */	NTV2_CHANNEL4,
																	/* NTV2_INPUTSOURCE_SDI1_DS2 */		NTV2_CHANNEL1,
																	/* NTV2_INPUTSOURCE_SDI2_DS2 */		NTV2_CHANNEL2,
																	/* NTV2_INPUTSOURCE_SDI3_DS2 */		NTV2_CHANNEL3,
																	/* NTV2_INPUTSOURCE_SDI4_DS2 */		NTV2_CHANNEL4,
																	/* NTV2_INPUTSOURCE_SDI5 */			NTV2_CHANNEL5,
																	/* NTV2_INPUTSOURCE_SDI6 */			NTV2_CHANNEL6,
																	/* NTV2_INPUTSOURCE_SDI7 */			NTV2_CHANNEL7,
																	/* NTV2_INPUTSOURCE_SDI8 */			NTV2_CHANNEL8,
																	/* NTV2_INPUTSOURCE_SDI5_DS2 */		NTV2_CHANNEL5,
																	/* NTV2_INPUTSOURCE_SDI6_DS2 */		NTV2_CHANNEL6,
																	/* NTV2_INPUTSOURCE_SDI7_DS2 */		NTV2_CHANNEL7,
																	/* NTV2_INPUTSOURCE_SDI8_DS2 */		NTV2_CHANNEL8,
																	/* NTV2_INPUTSOURCE_DUALLINK5 */	NTV2_CHANNEL5,
																	/* NTV2_INPUTSOURCE_DUALLINK6 */	NTV2_CHANNEL6,
																	/* NTV2_INPUTSOURCE_DUALLINK7 */	NTV2_CHANNEL7,
																	/* NTV2_INPUTSOURCE_DUALLINK8 */	NTV2_CHANNEL8,
																	/* NTV2_NUM_INPUTSOURCES */			NTV2_MAX_NUM_CHANNELS};
	#endif	//	else !defined (NTV2_DEPRECATE)

	if (inInputSource < NTV2_NUM_INPUTSOURCES  &&  size_t (inInputSource) < sizeof (gInputSourceToChannel) / sizeof (NTV2Channel))
		return gInputSourceToChannel [inInputSource];
	else
		return NTV2_MAX_NUM_CHANNELS;

}	//	NTV2InputSourceToChannel


NTV2AudioSystem NTV2InputSourceToAudioSystem (const NTV2InputSource inInputSource)
{
	#if defined (NTV2_DEPRECATE)
		static const NTV2AudioSystem	gInputSourceToAudioSystem []	= {	/* NTV2_INPUTSOURCE_ANALOG */		NTV2_AUDIOSYSTEM_1,
																			/* NTV2_INPUTSOURCE_HDMI */			NTV2_AUDIOSYSTEM_1,
																			/* NTV2_INPUTSOURCE_SDI1 */			NTV2_AUDIOSYSTEM_1,
																			/* NTV2_INPUTSOURCE_SDI2 */			NTV2_AUDIOSYSTEM_2,
																			/* NTV2_INPUTSOURCE_SDI3 */			NTV2_AUDIOSYSTEM_3,
																			/* NTV2_INPUTSOURCE_SDI4 */			NTV2_AUDIOSYSTEM_4,
																			/* NTV2_INPUTSOURCE_SDI5 */			NTV2_AUDIOSYSTEM_5,
																			/* NTV2_INPUTSOURCE_SDI6 */			NTV2_AUDIOSYSTEM_6,
																			/* NTV2_INPUTSOURCE_SDI7 */			NTV2_AUDIOSYSTEM_7,
																			/* NTV2_INPUTSOURCE_SDI8 */			NTV2_AUDIOSYSTEM_8,
																			/* NTV2_NUM_INPUTSOURCES */			NTV2_NUM_AUDIOSYSTEMS};
	#else	//	else !defined (NTV2_DEPRECATE)
		static const NTV2AudioSystem	gInputSourceToAudioSystem []	= {	/* NTV2_INPUTSOURCE_SDI1 */			NTV2_AUDIOSYSTEM_1,
																			/* NTV2_INPUTSOURCE_ANALOG */		NTV2_AUDIOSYSTEM_1,
																			/* NTV2_INPUTSOURCE_SDI2 */			NTV2_AUDIOSYSTEM_2,
																			/* NTV2_INPUTSOURCE_HDMI */			NTV2_AUDIOSYSTEM_1,
																			/* NTV2_INPUTSOURCE_DUALLINK1 */	NTV2_AUDIOSYSTEM_1,
																			/* NTV2_INPUTSOURCE_DUALLINK2 */	NTV2_AUDIOSYSTEM_2,
																			/* NTV2_INPUTSOURCE_SDI3 */			NTV2_AUDIOSYSTEM_3,
																			/* NTV2_INPUTSOURCE_SDI4 */			NTV2_AUDIOSYSTEM_4,
																			/* NTV2_INPUTSOURCE_DUALLINK3 */	NTV2_AUDIOSYSTEM_3,
																			/* NTV2_INPUTSOURCE_DUALLINK4 */	NTV2_AUDIOSYSTEM_4,
																			/* NTV2_INPUTSOURCE_SDI1_DS2 */		NTV2_AUDIOSYSTEM_1,
																			/* NTV2_INPUTSOURCE_SDI2_DS2 */		NTV2_AUDIOSYSTEM_2,
																			/* NTV2_INPUTSOURCE_SDI3_DS2 */		NTV2_AUDIOSYSTEM_3,
																			/* NTV2_INPUTSOURCE_SDI4_DS2 */		NTV2_AUDIOSYSTEM_4,
																			/* NTV2_INPUTSOURCE_SDI5 */			NTV2_AUDIOSYSTEM_5,
																			/* NTV2_INPUTSOURCE_SDI6 */			NTV2_AUDIOSYSTEM_6,
																			/* NTV2_INPUTSOURCE_SDI7 */			NTV2_AUDIOSYSTEM_7,
																			/* NTV2_INPUTSOURCE_SDI8 */			NTV2_AUDIOSYSTEM_8,
																			/* NTV2_INPUTSOURCE_SDI5_DS2 */		NTV2_AUDIOSYSTEM_5,
																			/* NTV2_INPUTSOURCE_SDI6_DS2 */		NTV2_AUDIOSYSTEM_6,
																			/* NTV2_INPUTSOURCE_SDI7_DS2 */		NTV2_AUDIOSYSTEM_7,
																			/* NTV2_INPUTSOURCE_SDI8_DS2 */		NTV2_AUDIOSYSTEM_8,
																			/* NTV2_INPUTSOURCE_DUALLINK5 */	NTV2_AUDIOSYSTEM_5,
																			/* NTV2_INPUTSOURCE_DUALLINK6 */	NTV2_AUDIOSYSTEM_6,
																			/* NTV2_INPUTSOURCE_DUALLINK7 */	NTV2_AUDIOSYSTEM_7,
																			/* NTV2_INPUTSOURCE_DUALLINK8 */	NTV2_AUDIOSYSTEM_8,
																			/* NTV2_NUM_INPUTSOURCES */			NTV2_NUM_AUDIOSYSTEMS};
	#endif	//	else !defined (NTV2_DEPRECATE)

	if (inInputSource < NTV2_NUM_INPUTSOURCES  &&  inInputSource < (int)(sizeof (gInputSourceToAudioSystem) / sizeof (NTV2AudioSystem)))
		return gInputSourceToAudioSystem [inInputSource];
	else
		return NTV2_MAX_NUM_AudioSystemEnums;

}	//	NTV2InputSourceToAudioSystem


NTV2InputSource NTV2ChannelToInputSource (const NTV2Channel inChannel)
{
	#if defined (NTV2_DEPRECATE)
		static const NTV2InputSource	gChannelToInputSource []	=	{	NTV2_INPUTSOURCE_SDI1,	NTV2_INPUTSOURCE_SDI2,	NTV2_INPUTSOURCE_SDI3,	NTV2_INPUTSOURCE_SDI4,
																			NTV2_INPUTSOURCE_SDI5,	NTV2_INPUTSOURCE_SDI6,	NTV2_INPUTSOURCE_SDI7,	NTV2_INPUTSOURCE_SDI8,
																			NTV2_NUM_INPUTSOURCES	};
		return gChannelToInputSource [inChannel];
	#else	//	else !defined (NTV2_DEPRECATE)
		switch (inChannel)
		{
			default:
			case NTV2_CHANNEL1:		return NTV2_INPUTSOURCE_SDI1;
			case NTV2_CHANNEL2:		return NTV2_INPUTSOURCE_SDI2;
			case NTV2_CHANNEL3:		return NTV2_INPUTSOURCE_SDI3;
			case NTV2_CHANNEL4:		return NTV2_INPUTSOURCE_SDI4;
			case NTV2_CHANNEL5:		return NTV2_INPUTSOURCE_SDI5;
			case NTV2_CHANNEL6:		return NTV2_INPUTSOURCE_SDI6;
			case NTV2_CHANNEL7:		return NTV2_INPUTSOURCE_SDI7;
			case NTV2_CHANNEL8:		return NTV2_INPUTSOURCE_SDI8;
		}
	#endif	//	else !defined (NTV2_DEPRECATE)
}


NTV2OutputDestination NTV2ChannelToOutputDestination (const NTV2Channel inChannel)
{
	#if defined (NTV2_DEPRECATE)
		static const NTV2OutputDestination	gChannelToOutputDest []	=	{	NTV2_OUTPUTDESTINATION_SDI1,	NTV2_OUTPUTDESTINATION_SDI2,	NTV2_OUTPUTDESTINATION_SDI3,	NTV2_OUTPUTDESTINATION_SDI4,
																			NTV2_OUTPUTDESTINATION_SDI5,	NTV2_OUTPUTDESTINATION_SDI6,	NTV2_OUTPUTDESTINATION_SDI7,	NTV2_OUTPUTDESTINATION_SDI8,
																			NTV2_NUM_OUTPUTDESTINATIONS	};
		return gChannelToOutputDest [inChannel];
	#else	//	else !defined (NTV2_DEPRECATE)
		switch (inChannel)
		{
			default:
			case NTV2_CHANNEL1:		return NTV2_OUTPUTDESTINATION_SDI1;
			case NTV2_CHANNEL2:		return NTV2_OUTPUTDESTINATION_SDI2;
			case NTV2_CHANNEL3:		return NTV2_OUTPUTDESTINATION_SDI3;
			case NTV2_CHANNEL4:		return NTV2_OUTPUTDESTINATION_SDI4;
			case NTV2_CHANNEL5:		return NTV2_OUTPUTDESTINATION_SDI5;
			case NTV2_CHANNEL6:		return NTV2_OUTPUTDESTINATION_SDI6;
			case NTV2_CHANNEL7:		return NTV2_OUTPUTDESTINATION_SDI7;
			case NTV2_CHANNEL8:		return NTV2_OUTPUTDESTINATION_SDI8;
		}
	#endif	//	else !defined (NTV2_DEPRECATE)
}


// if formats are transport equivalent (e.g. 1080i30 / 1080psf30) return the target version of the format
NTV2VideoFormat GetTransportCompatibleFormat (const NTV2VideoFormat inFormat, const NTV2VideoFormat inTargetFormat)
{
	// compatible return target version
	if (::IsTransportCompatibleFormat (inFormat, inTargetFormat))
		return inTargetFormat;

	// not compatible, return original format
	return inFormat;
}


// determine if 2 formats are transport compatible (e.g. 1080i30 / 1080psf30)
bool IsTransportCompatibleFormat (const NTV2VideoFormat inFormat1, const NTV2VideoFormat inFormat2)
{
	if (inFormat1 == inFormat2)
		return true;

	switch (inFormat1)
	{
		case NTV2_FORMAT_1080i_5000:		return inFormat2 == NTV2_FORMAT_1080psf_2500_2;
		case NTV2_FORMAT_1080i_5994:		return inFormat2 == NTV2_FORMAT_1080psf_2997_2;
		case NTV2_FORMAT_1080i_6000:		return inFormat2 == NTV2_FORMAT_1080psf_3000_2;
		case NTV2_FORMAT_1080psf_2500_2:	return inFormat2 == NTV2_FORMAT_1080i_5000;
		case NTV2_FORMAT_1080psf_2997_2:	return inFormat2 == NTV2_FORMAT_1080i_5994;
		case NTV2_FORMAT_1080psf_3000_2:	return inFormat2 == NTV2_FORMAT_1080i_6000;
		default:							break;
	}
	return false;
}


NTV2InputSource GetNTV2InputSourceForIndex (const ULWord inIndex0)
{
	static const NTV2InputSource	sInputSources []	= {	NTV2_INPUTSOURCE_SDI1,	NTV2_INPUTSOURCE_SDI2,	NTV2_INPUTSOURCE_SDI3,	NTV2_INPUTSOURCE_SDI4,
															NTV2_INPUTSOURCE_SDI5,	NTV2_INPUTSOURCE_SDI6,	NTV2_INPUTSOURCE_SDI7,	NTV2_INPUTSOURCE_SDI8};
	if (inIndex0 < sizeof (sInputSources) / sizeof (NTV2InputSource))
		return sInputSources [inIndex0];
	else
		return NTV2_NUM_INPUTSOURCES;
}


ULWord GetIndexForNTV2InputSource (const NTV2InputSource inValue)
{
	static const ULWord	sInputSourcesIndexes []	= {
												#if defined (NTV2_DEPRECATE)
													0xFFFFFFFF,	//	NTV2_INPUTSOURCE_ANALOG,
													0xFFFFFFFF,	//	NTV2_INPUTSOURCE_HDMI,
													0,	1,	2,	3,	4,	5,	6,	7	//	NTV2_INPUTSOURCE_SDI1 ... NTV2_INPUTSOURCE_SDI8
												#else
													0,			//	NTV2_INPUTSOURCE_SDI,
													0xFFFFFFFF,	//	NTV2_INPUTSOURCE_ANALOG,
													1,			//	NTV2_INPUTSOURCE_SDI2,
													0xFFFFFFFF,	//	NTV2_INPUTSOURCE_HDMI,
													0,			//	NTV2_INPUTSOURCE_DUALLINK,
													1,			//	NTV2_INPUTSOURCE_DUALLINK2,
													2,			//	NTV2_INPUTSOURCE_SDI3,
													3,			//	NTV2_INPUTSOURCE_SDI4,
													2,			//	NTV2_INPUTSOURCE_DUALLINK3,
													3,			//	NTV2_INPUTSOURCE_DUALLINK4,
													0,			//	NTV2_INPUTSOURCE_SDI1_DS2,
													1,			//	NTV2_INPUTSOURCE_SDI2_DS2,
													2,			//	NTV2_INPUTSOURCE_SDI3_DS2,
													3,			//	NTV2_INPUTSOURCE_SDI4_DS2,
													4,			//	NTV2_INPUTSOURCE_SDI5,
													5,			//	NTV2_INPUTSOURCE_SDI6,
													6,			//	NTV2_INPUTSOURCE_SDI7,
													7,			//	NTV2_INPUTSOURCE_SDI8,
													4,			//	NTV2_INPUTSOURCE_SDI5_DS2,
													5,			//	NTV2_INPUTSOURCE_SDI6_DS2,
													6,			//	NTV2_INPUTSOURCE_SDI7_DS2,
													7,			//	NTV2_INPUTSOURCE_SDI8_DS2,
													4,			//	NTV2_INPUTSOURCE_DUALLINK5,
													5,			//	NTV2_INPUTSOURCE_DUALLINK6,
													6,			//	NTV2_INPUTSOURCE_DUALLINK7,
													7			//	NTV2_INPUTSOURCE_DUALLINK8,
												#endif	//	!defined (NTV2_DEPRECATE)
												};
	if (static_cast <size_t> (inValue) < sizeof (sInputSourcesIndexes) / sizeof (ULWord))
		return sInputSourcesIndexes [inValue];
	else
		return 0xFFFFFFFF;

}	//	GetIndexForNTV2InputSource


ULWord NTV2FramesizeToByteCount (const NTV2Framesize inFrameSize)
{
	static ULWord	gFrameSizeToByteCount []	= {	2 /* NTV2_FRAMESIZE_2MB */,		4 /* NTV2_FRAMESIZE_4MB */,		8 /* NTV2_FRAMESIZE_8MB */,		16 /* NTV2_FRAMESIZE_16MB */,	0};

	if (inFrameSize < NTV2_MAX_NUM_Framesizes && inFrameSize < (int)(sizeof (gFrameSizeToByteCount) / sizeof (ULWord)))
		return gFrameSizeToByteCount [inFrameSize] * 1024 * 1024;
	else
		return 0;

}	//	NTV2FramesizeToByteCount


bool IsMultiFormatCompatible (const NTV2FrameRate inFrameRate1, const NTV2FrameRate inFrameRate2)
{
	if (inFrameRate1 == inFrameRate2)
		return true;

	ULWord scale1 = GetScaleFromFrameRate (inFrameRate1);
	ULWord scale2 = GetScaleFromFrameRate (inFrameRate2);

	if (scale1 < scale2)
		return (scale2 % scale1) == 0;
	else
		return (scale1 % scale2) == 0;

}	//	IsMultiFormatCompatible (NTV2FrameRate)


AJAExport bool IsMultiFormatCompatible (const NTV2VideoFormat inFormat1, const NTV2VideoFormat inFormat2)
{
	return ::IsMultiFormatCompatible (::GetNTV2FrameRateFromVideoFormat (inFormat1), ::GetNTV2FrameRateFromVideoFormat (inFormat2));

}	//	IsMultiFormatCompatible (NTV2VideoFormat)


AJAExport bool IsPSF(NTV2VideoFormat format)
{
	return NTV2_IS_PSF_VIDEO_FORMAT (format);
}


AJAExport bool IsProgressivePicture(NTV2VideoFormat format)
{
	return NTV2_VIDEO_FORMAT_HAS_PROGRESSIVE_PICTURE (format);
}


AJAExport bool IsProgressiveTransport(NTV2VideoFormat format)
{
	NTV2Standard standard = GetNTV2StandardFromVideoFormat(format);
	return IsProgressiveTransport(standard);
}


AJAExport bool IsProgressiveTransport(NTV2Standard standard)
{
	return IS_PROGRESSIVE_NTV2Standard (standard);
}


AJAExport bool IsRGBFormat(NTV2FrameBufferFormat format)
{
	return NTV2_IS_FBF_RGB (format);
}


AJAExport bool IsYCbCrFormat(NTV2FrameBufferFormat format)
{
	return !NTV2_IS_FBF_RGB (format);	// works for now
}


AJAExport bool IsAlphaChannelFormat(NTV2FrameBufferFormat format)
{
	return NTV2_FBF_HAS_ALPHA (format);
}


AJAExport bool Is2KFormat(NTV2VideoFormat format)
{
	return NTV2_IS_2K_1080_VIDEO_FORMAT (format) || NTV2_IS_2K_VIDEO_FORMAT (format);
}


AJAExport bool Is4KFormat(NTV2VideoFormat format)
{
	return NTV2_IS_4K_4096_VIDEO_FORMAT (format) || NTV2_IS_4K_QUADHD_VIDEO_FORMAT (format);
}


AJAExport bool IsRaw(NTV2FrameBufferFormat frameBufferFormat)
{
	return NTV2_FBF_IS_RAW (frameBufferFormat);
}


AJAExport bool Is8BitFrameBufferFormat(NTV2FrameBufferFormat format)
{
	return NTV2_IS_FBF_8BIT (format);
}


AJAExport bool IsVideoFormatA (NTV2VideoFormat format)
{
	return NTV2_VIDEO_FORMAT_IS_A (format);
}


AJAExport bool IsVideoFormatB(NTV2VideoFormat format)
{
	return NTV2_IS_3Gb_FORMAT (format);
}


#if !defined (NTV2_DEPRECATE)
	//
	// BuildRoutingTableForOutput
	// Relative to FrameStore
	// NOTE: convert ignored for now do to excessive laziness
	//
	// Its possible that if you are using this for channel 1 and
	// 2 you could use up resources and unexpected behavior will ensue.
	// for instance, if channel 1 routing uses up both color space converters
	// and then you try to setup channel 2 to use a color space converter.
	// it will overwrite channel 1's routing.
	bool BuildRoutingTableForOutput(CNTV2SignalRouter & outRouter,
									NTV2Channel channel,
									NTV2FrameBufferFormat fbf,
									bool convert, // ignored
									bool lut,
									bool dualLink,
									bool keyOut)	// only check for RGB Formats
									// NOTE: maybe add key out???????
	{
		(void) convert;
		bool status = false;
		bool canHaveKeyOut = false;
		switch (fbf)
		{
		case NTV2_FBF_8BIT_QREZ:
			// not supported
			break;

		case NTV2_FBF_ARGB:
		case NTV2_FBF_RGBA:
		case NTV2_FBF_ABGR:
		case NTV2_FBF_10BIT_ARGB:
		case NTV2_FBF_16BIT_ARGB:
			canHaveKeyOut = true;
		case NTV2_FBF_10BIT_RGB:
		case NTV2_FBF_10BIT_RGB_PACKED:
		case NTV2_FBF_10BIT_DPX:
		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
		case NTV2_FBF_24BIT_BGR:
		case NTV2_FBF_24BIT_RGB:
		case NTV2_FBF_48BIT_RGB:
			if ( channel == NTV2_CHANNEL1)
			{
				if ( lut && dualLink )
				{
					// first hook up framestore 1 to lut1,
					outRouter.addWithValue(GetXptLUTInputSelectEntry(),NTV2_XptFrameBuffer1RGB);

					// then hook up lut1 to dualLink
					outRouter.addWithValue(GetDuallinkOutInputSelectEntry(),NTV2_XptLUT1RGB);

					// then hook up dualLink to SDI 1 and SDI 2
					outRouter.addWithValue(GetSDIOut1InputSelectEntry(),NTV2_XptDuallinkOut);
					status = outRouter.addWithValue(GetSDIOut2InputSelectEntry(),NTV2_XptDuallinkOut);

				}
				else
					if ( lut )
					{
						// first hook up framestore 1 to lut1,
						outRouter.addWithValue(GetXptLUTInputSelectEntry(),NTV2_XptFrameBuffer1RGB);

						// then hook up lut1 to csc1
						outRouter.addWithValue(GetCSC1VidInputSelectEntry(),NTV2_XptLUT1RGB);

						// then hook up csc1 to sdi 1
						status = outRouter.addWithValue(GetSDIOut1InputSelectEntry(),NTV2_XptCSC1VidYUV);
						if ( keyOut && canHaveKeyOut)
						{
							status = outRouter.addWithValue(GetSDIOut1InputSelectEntry(),NTV2_XptCSC1KeyYUV);
						}
					}
					else
						if ( dualLink)
						{
							// hook up framestore 1 lut duallink
							outRouter.addWithValue(GetDuallinkOutInputSelectEntry(),NTV2_XptFrameBuffer1RGB);

							// then hook up dualLink to SDI 1 and SDI 2
							outRouter.addWithValue(GetSDIOut1InputSelectEntry(),NTV2_XptDuallinkOut);
							status = outRouter.addWithValue(GetSDIOut2InputSelectEntry(),NTV2_XptDuallinkOut);
						}
						else
						{
							outRouter.addWithValue(::GetCSC1VidInputSelectEntry(),NTV2_XptFrameBuffer1RGB);
							status = outRouter.addWithValue(::GetSDIOut1InputSelectEntry(),NTV2_XptCSC1VidYUV);
							if ( keyOut && canHaveKeyOut)
							{
								status = outRouter.addWithValue(::GetSDIOut1InputSelectEntry(),NTV2_XptCSC1KeyYUV);

							}

						}
			}
			else // if channel 2
			{

				// arbitrarily don't support duallink on channel 2
				if ( lut )
				{
					// first hook up framestore 2 to lut2,
					outRouter.addWithValue(::GetXptLUT2InputSelectEntry(),NTV2_XptFrameBuffer2RGB);

					// then hook up lut2 to csc2
					outRouter.addWithValue(::GetCSC2VidInputSelectEntry(),NTV2_XptLUT2RGB);

					// then hook up csc2 to sdi 2 and any other output
					outRouter.addWithValue(::GetSDIOut1InputSelectEntry(), NTV2_XptCSC2VidYUV);
					outRouter.addWithValue(::GetHDMIOutInputSelectEntry(), NTV2_XptCSC2VidYUV);
					outRouter.addWithValue(::GetAnalogOutInputSelectEntry(), NTV2_XptCSC2VidYUV);
					status = outRouter.addWithValue(::GetSDIOut2InputSelectEntry(),NTV2_XptCSC2VidYUV);
				}
				else
				{
					outRouter.addWithValue(::GetCSC2VidInputSelectEntry(),NTV2_XptFrameBuffer2RGB);
					outRouter.addWithValue(::GetSDIOut1InputSelectEntry(), NTV2_XptCSC2VidRGB);
					outRouter.addWithValue(::GetHDMIOutInputSelectEntry(), NTV2_XptCSC2VidRGB);
					outRouter.addWithValue(::GetAnalogOutInputSelectEntry(), NTV2_XptCSC2VidRGB);
					status = outRouter.addWithValue(::GetSDIOut2InputSelectEntry(),NTV2_XptCSC2VidRGB);
				}
			}
			break;

		case NTV2_FBF_8BIT_DVCPRO:
		case NTV2_FBF_8BIT_HDV:
			if ( channel == NTV2_CHANNEL1)
			{
				if ( lut && dualLink )
				{
					// first uncompress it.
					outRouter.addWithValue(::GetCompressionModInputSelectEntry(),NTV2_XptFrameBuffer1YUV);

					// then send it to color space converter 1
					outRouter.addWithValue(::GetCSC1VidInputSelectEntry(),NTV2_XptCompressionModule);

					// color space convert 1 to lut 1
					outRouter.addWithValue(::GetXptLUTInputSelectEntry(),NTV2_XptCSC1VidRGB);

					// lut 1 to duallink in
					outRouter.addWithValue(::GetDuallinkOutInputSelectEntry(),NTV2_XptLUT1RGB);

					// then hook up dualLink to SDI 1 and SDI 2
					outRouter.addWithValue(::GetSDIOut1InputSelectEntry(),NTV2_XptDuallinkOut);
					status = outRouter.addWithValue(::GetSDIOut2InputSelectEntry(),NTV2_XptDuallinkOut);

				}
				else  // if channel 2
					if ( lut )
					{
						// first uncompress it.
						outRouter.addWithValue(::GetCompressionModInputSelectEntry(),NTV2_XptFrameBuffer1YUV);

						// then send it to color space converter 1
						outRouter.addWithValue(::GetCSC1VidInputSelectEntry(),NTV2_XptCompressionModule);

						// color space convert 1 to lut 1
						outRouter.addWithValue(::GetXptLUTInputSelectEntry(),NTV2_XptCSC1VidRGB);

						// lut 1 to color space convert 2
						outRouter.addWithValue(::GetCSC2VidInputSelectEntry(),NTV2_XptLUT1RGB);

						// color space converter 2 to outputs
						outRouter.addWithValue(::GetSDIOut2InputSelectEntry(),NTV2_XptCSC2VidYUV);
						outRouter.addWithValue(::GetHDMIOutInputSelectEntry(), NTV2_XptCSC2VidYUV);
						outRouter.addWithValue(::GetAnalogOutInputSelectEntry(), NTV2_XptCSC2VidYUV);
						status = outRouter.addWithValue(::GetSDIOut1InputSelectEntry(),NTV2_XptCSC2VidYUV);
					}
					else
					{
						// just send it straight out SDI 1 and other outputs
						outRouter.addWithValue(::GetCompressionModInputSelectEntry(),NTV2_XptFrameBuffer1YUV);
						outRouter.addWithValue(::GetSDIOut2InputSelectEntry(), NTV2_XptCompressionModule);
						outRouter.addWithValue(::GetHDMIOutInputSelectEntry(), NTV2_XptCompressionModule);
						outRouter.addWithValue(::GetAnalogOutInputSelectEntry(), NTV2_XptCompressionModule);
						status = outRouter.addWithValue(::GetSDIOut1InputSelectEntry(),NTV2_XptCompressionModule);
					}
			}
			else
			{
				// channel 2....an excersize for the user:)
				outRouter.addWithValue(::GetCompressionModInputSelectEntry(),NTV2_XptFrameBuffer1YUV);
				outRouter.addWithValue(::GetSDIOut2InputSelectEntry(), NTV2_XptFrameBuffer2YUV);
				outRouter.addWithValue(::GetHDMIOutInputSelectEntry(), NTV2_XptFrameBuffer2YUV);
				outRouter.addWithValue(::GetAnalogOutInputSelectEntry(), NTV2_XptFrameBuffer2YUV);
				status = outRouter.addWithValue(::GetSDIOut2InputSelectEntry(),NTV2_XptFrameBuffer2YUV);
			}
			break;

		default:
			// YCbCr Stuff
			if ( channel == NTV2_CHANNEL1)
			{
				if ( lut && dualLink )
				{
					// frame store 1 to color space converter 1
					outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptFrameBuffer1YUV);

					// color space convert 1 to lut 1
					outRouter.addWithValue (::GetXptLUTInputSelectEntry (), NTV2_XptCSC1VidRGB);

					// lut 1 to duallink in
					outRouter.addWithValue (::GetDuallinkOutInputSelectEntry (), NTV2_XptLUT1RGB);

					// then hook up dualLink to SDI 1 and SDI 2
					outRouter.addWithValue (::GetSDIOut1InputSelectEntry (), NTV2_XptDuallinkOut);
					status = outRouter.addWithValue (::GetSDIOut2InputSelectEntry (), NTV2_XptDuallinkOut);

				}
				else
				if ( lut )
				{
					// frame store 1 to color space converter 1
					outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptFrameBuffer1YUV);

					// color space convert 1 to lut 1
					outRouter.addWithValue (::GetXptLUTInputSelectEntry (), NTV2_XptCSC1VidRGB);

					// lut 1 to color space convert 2
					outRouter.addWithValue (::GetCSC2VidInputSelectEntry (), NTV2_XptLUT1RGB);

					// color space converter 2 to outputs
					outRouter.addWithValue (::GetSDIOut2InputSelectEntry (), NTV2_XptCSC2VidYUV);
					outRouter.addWithValue (::GetHDMIOutInputSelectEntry (), NTV2_XptCSC2VidYUV);
					outRouter.addWithValue (::GetAnalogOutInputSelectEntry (), NTV2_XptCSC2VidYUV);
					status = outRouter.addWithValue (::GetSDIOut1InputSelectEntry (), NTV2_XptCSC2VidYUV);
				}
				else
				{
					// just send it straight outputs
					outRouter.addWithValue (::GetHDMIOutInputSelectEntry (), NTV2_XptFrameBuffer1YUV);
					outRouter.addWithValue (::GetAnalogOutInputSelectEntry (), NTV2_XptFrameBuffer1YUV);
					status = outRouter.addWithValue (::GetSDIOut1InputSelectEntry (), NTV2_XptFrameBuffer1YUV);
				}
			}

			else if ( channel == NTV2_CHANNEL2)
			{
				// channel 2....an excersize for the user:)
				outRouter.addWithValue (::GetAnalogOutInputSelectEntry (), NTV2_XptFrameBuffer2YUV);
				outRouter.addWithValue (::GetHDMIOutInputSelectEntry (), NTV2_XptFrameBuffer2YUV);
				status = outRouter.addWithValue (::GetSDIOut2InputSelectEntry (), NTV2_XptFrameBuffer2YUV);
			}

			else if ( channel == NTV2_CHANNEL3)
			{
				// channel 3....an excersize for the user:)
				outRouter.addWithValue (::GetAnalogOutInputSelectEntry (), NTV2_XptFrameBuffer3YUV);
				outRouter.addWithValue (::GetHDMIOutInputSelectEntry (), NTV2_XptFrameBuffer3YUV);
				status = outRouter.addWithValue (::GetSDIOut3InputSelectEntry (), NTV2_XptFrameBuffer3YUV);
			}

			else if ( channel == NTV2_CHANNEL4)
			{
				// channel 4....an excersize for the user:)
				outRouter.addWithValue (::GetAnalogOutInputSelectEntry (), NTV2_XptFrameBuffer4YUV);
				outRouter.addWithValue (::GetHDMIOutInputSelectEntry (), NTV2_XptFrameBuffer4YUV);
				status = outRouter.addWithValue (::GetSDIOut4InputSelectEntry (), NTV2_XptFrameBuffer4YUV);
			}
			break;
		}


		return status;
	}

	//
	// BuildRoutingTableForInput
	// Relative to FrameStore
	//
	bool BuildRoutingTableForInput(CNTV2SignalRouter & outRouter,
								   NTV2Channel channel,
								   NTV2FrameBufferFormat fbf,
								   bool withKey,  // only supported for NTV2_CHANNEL1 for rgb formats with alpha
								   bool lut,	  // not supported
								   bool dualLink, // assume coming in RGB(only checked for NTV2_CHANNEL1
								   bool EtoE)
	{
		(void) lut;
		bool status = false;
		if (fbf == NTV2_FBF_8BIT_QREZ)
			return status;

		if ( EtoE)
		{
			if ( dualLink )
			{
				outRouter.addWithValue (::GetSDIOut1InputSelectEntry (), NTV2_XptSDIIn1);
				outRouter.addWithValue (::GetSDIOut2InputSelectEntry (), NTV2_XptSDIIn2);
			}
			else if ( channel == NTV2_CHANNEL1)
			{
				outRouter.addWithValue (::GetSDIOut1InputSelectEntry (), NTV2_XptSDIIn1);
			}
			else if ( channel == NTV2_CHANNEL2)
			{
				outRouter.addWithValue (::GetSDIOut2InputSelectEntry (), NTV2_XptSDIIn2);
			}
			else if ( channel == NTV2_CHANNEL3)
			{
				outRouter.addWithValue (::GetSDIOut2InputSelectEntry (), NTV2_XptSDIIn3);
			}
			else if ( channel == NTV2_CHANNEL4)
			{
				outRouter.addWithValue (::GetSDIOut2InputSelectEntry (), NTV2_XptSDIIn4);
			}
		}

		switch (fbf)
		{

			case NTV2_FBF_ARGB:
			case NTV2_FBF_RGBA:
			case NTV2_FBF_ABGR:
			case NTV2_FBF_10BIT_ARGB:
			case NTV2_FBF_16BIT_ARGB:
				if ( channel == NTV2_CHANNEL1)
				{
					if ( withKey)
					{
						outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn1);
						outRouter.addWithValue (::GetCSC1KeyInputSelectEntry (), NTV2_XptSDIIn2);
						outRouter.addWithValue (::GetCSC1KeyFromInput2SelectEntry (), 1);
						status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptCSC1VidRGB);
					}
					else
					{
						outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn1);
						outRouter.addWithValue (::GetCSC1KeyFromInput2SelectEntry (), 0);
						status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptCSC1VidRGB);
					}
				}
				else
				{
					outRouter.addWithValue (::GetCSC2VidInputSelectEntry (), NTV2_XptSDIIn2);
					status = outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptCSC2VidRGB);
				}
				break;
			case NTV2_FBF_10BIT_RGB:
			case NTV2_FBF_10BIT_RGB_PACKED:
			case NTV2_FBF_10BIT_DPX:
			case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
			case NTV2_FBF_24BIT_BGR:
			case NTV2_FBF_24BIT_RGB:
				if ( channel == NTV2_CHANNEL1)
				{
					if ( dualLink )
					{
						status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptDuallinkIn);
					}
					else
					{
						outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn1);
						status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptCSC1VidRGB);
					}
				}
				else
				{
					outRouter.addWithValue (::GetCSC2VidInputSelectEntry (), NTV2_XptSDIIn2);
					status = outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptCSC2VidRGB);
				}
				break;
			case NTV2_FBF_8BIT_DVCPRO:
			case NTV2_FBF_8BIT_HDV:
				if ( channel == NTV2_CHANNEL1)
				{
					status = outRouter.addWithValue (::GetCompressionModInputSelectEntry (), NTV2_XptSDIIn1);
					status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptCompressionModule);
				}
				else
				{
					status = outRouter.addWithValue (::GetCompressionModInputSelectEntry (), NTV2_XptSDIIn2);
					status = outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptCompressionModule);
				}
				break;

			default:
				if( dualLink )
				{
					status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptSDIIn1);
					status = outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptSDIIn2);
				}
				else if ( channel == NTV2_CHANNEL1)
				{
					status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptSDIIn1);
				}
				else if ( channel == NTV2_CHANNEL2)
				{
					status = outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptSDIIn2);
				}
				else if ( channel == NTV2_CHANNEL3)
				{
					status = outRouter.addWithValue (::GetFrameBuffer3InputSelectEntry (), NTV2_XptSDIIn3);
				}
				else if ( channel == NTV2_CHANNEL4)
				{
					status = outRouter.addWithValue (::GetFrameBuffer4InputSelectEntry (), NTV2_XptSDIIn4);
				}
				break;
		}

		return status;

	}

	//
	// BuildRoutingTableForInput
	// Relative to FrameStore
	//
	bool BuildRoutingTableForInput(CNTV2SignalRouter & outRouter,
								   NTV2Channel channel,
								   NTV2FrameBufferFormat fbf,
								   bool convert,  // Turn on the conversion module
								   bool withKey,  // only supported for NTV2_CHANNEL1 for rgb formats with alpha
								   bool lut,	  // not supported
								   bool dualLink, // assume coming in RGB(only checked for NTV2_CHANNEL1
								   bool EtoE)
	{
		(void) convert;
		(void) lut;
		bool status = false;
		if (fbf == NTV2_FBF_8BIT_QREZ)
			return status;

		if ( EtoE)
		{
			if ( dualLink)
			{
				outRouter.addWithValue (::GetSDIOut1InputSelectEntry (), NTV2_XptSDIIn1);
				outRouter.addWithValue (::GetSDIOut2InputSelectEntry (), NTV2_XptSDIIn2);

			}
			else
				if ( channel == NTV2_CHANNEL1)
				{
					outRouter.addWithValue (::GetSDIOut1InputSelectEntry (), NTV2_XptSDIIn1);
				}
				else
				{
					outRouter.addWithValue (::GetSDIOut2InputSelectEntry (), NTV2_XptSDIIn2);
				}
		}

		switch (fbf)
		{

		case NTV2_FBF_ARGB:
		case NTV2_FBF_RGBA:
		case NTV2_FBF_ABGR:
		case NTV2_FBF_10BIT_ARGB:
		case NTV2_FBF_16BIT_ARGB:
			if ( channel == NTV2_CHANNEL1)
			{
				if ( withKey)
				{
					outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn1);
					outRouter.addWithValue (::GetCSC1KeyInputSelectEntry (), NTV2_XptSDIIn2);
					outRouter.addWithValue (::GetCSC1KeyFromInput2SelectEntry (), 1);
					status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptCSC1VidRGB);

				}
				else
				{
					outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn1);
					outRouter.addWithValue (::GetCSC1KeyFromInput2SelectEntry (), 0);
					status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptCSC1VidRGB);
				}
			}
			else
			{
				outRouter.addWithValue (::GetCSC2VidInputSelectEntry (), NTV2_XptSDIIn2);
				status = outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptCSC2VidRGB);
			}
			break;
		case NTV2_FBF_10BIT_RGB:
		case NTV2_FBF_10BIT_RGB_PACKED:
		case NTV2_FBF_10BIT_DPX:
		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
		case NTV2_FBF_24BIT_BGR:
		case NTV2_FBF_24BIT_RGB:
			if ( channel == NTV2_CHANNEL1)
			{
				if ( dualLink )
				{
					status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptDuallinkIn);
				}
				else
				{
					outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn1);
					status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptCSC1VidRGB);
				}
			}
			else
			{
				outRouter.addWithValue (::GetCSC2VidInputSelectEntry (), NTV2_XptSDIIn2);
				status = outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptCSC2VidRGB);
			}
			break;
		case NTV2_FBF_8BIT_DVCPRO:
		case NTV2_FBF_8BIT_HDV:
			if ( channel == NTV2_CHANNEL1)
			{
				status = outRouter.addWithValue (::GetCompressionModInputSelectEntry (), NTV2_XptSDIIn1);
				status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptCompressionModule);
			}
			else
			{
				status = outRouter.addWithValue (::GetCompressionModInputSelectEntry (), NTV2_XptSDIIn2);
				status = outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptCompressionModule);
			}
			break;

		default:
			if ( channel == NTV2_CHANNEL1)
			{
				status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptSDIIn1);
			}
			else
			{
				status = outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptSDIIn2);
			}
			break;
		}

		return status;

	}

	//
	// BuildRoutingTableForInput
	// Includes input and channel now
	//
	bool BuildRoutingTableForInput(CNTV2SignalRouter & outRouter,
								   NTV2InputSource inputSource,
								   NTV2Channel channel,
								   NTV2FrameBufferFormat fbf,
								   bool convert,  // Turn on the conversion module
								   bool withKey,  // only supported for NTV2_CHANNEL1 for rgb formats with alpha
								   bool lut,	  // not supported
								   bool dualLink, // assume coming in RGB(only checked for NTV2_CHANNEL1
								   bool EtoE)
	{
		(void) convert;
		(void) lut;
		if ( dualLink )
			inputSource = NTV2_INPUTSOURCE_DUALLINK;
		bool status = false;
		if (fbf == NTV2_FBF_8BIT_QREZ)
			return status;

		if ( EtoE)
		{
			if ( dualLink)
			{
				outRouter.addWithValue (::GetSDIOut1InputSelectEntry (), NTV2_XptSDIIn1);
				outRouter.addWithValue (::GetSDIOut2InputSelectEntry (), NTV2_XptSDIIn2);

			}
			else
			{
				switch ( inputSource )
				{
				case NTV2_INPUTSOURCE_SDI1:
					outRouter.addWithValue (::GetSDIOut1InputSelectEntry (), NTV2_XptSDIIn1);
					break;
				case NTV2_INPUTSOURCE_SDI2:
					outRouter.addWithValue (::GetSDIOut2InputSelectEntry (), NTV2_XptSDIIn2);
					break;
				case NTV2_INPUTSOURCE_ANALOG:
					outRouter.addWithValue (::GetAnalogOutInputSelectEntry (), NTV2_XptAnalogIn);
					break;
				case NTV2_INPUTSOURCE_HDMI:
					outRouter.addWithValue (::GetHDMIOutInputSelectEntry (), NTV2_XptHDMIIn);
					break;
				case NTV2_INPUTSOURCE_DUALLINK:
					outRouter.addWithValue (::GetSDIOut1InputSelectEntry (), NTV2_XptSDIIn1);
					outRouter.addWithValue (::GetSDIOut2InputSelectEntry (), NTV2_XptSDIIn2);
					break;
				default:
					return false;
				}
			}
		}

		switch (fbf)
		{

		case NTV2_FBF_ARGB:
		case NTV2_FBF_RGBA:
		case NTV2_FBF_ABGR:
		case NTV2_FBF_10BIT_ARGB:
		case NTV2_FBF_16BIT_ARGB:
			if ( channel == NTV2_CHANNEL1)
			{
				if ( withKey)
				{
					outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn1);
					outRouter.addWithValue (::GetCSC1KeyInputSelectEntry (), NTV2_XptSDIIn2);
					outRouter.addWithValue (::GetCSC1KeyFromInput2SelectEntry (), 1);
					status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptCSC1VidRGB);

				}
				else
				{
					//outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn1);
					switch ( inputSource )
					{
					case NTV2_INPUTSOURCE_SDI1:
						outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn1);
						break;
					case NTV2_INPUTSOURCE_SDI2:
						outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn2);
						break;
					case NTV2_INPUTSOURCE_ANALOG:
						outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptAnalogIn);
						break;
					case NTV2_INPUTSOURCE_HDMI:
						outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptHDMIIn);
						break;
					default:
						return false;
					}

					outRouter.addWithValue (::GetCSC1KeyFromInput2SelectEntry (), 0);
					status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptCSC1VidRGB);
				}
			}
			else
			{
				switch ( inputSource )
				{
				case NTV2_INPUTSOURCE_SDI1:
					outRouter.addWithValue (::GetCSC2VidInputSelectEntry (), NTV2_XptSDIIn1);
					break;
				case NTV2_INPUTSOURCE_SDI2:
					outRouter.addWithValue (::GetCSC2VidInputSelectEntry (), NTV2_XptSDIIn2);
					break;
				case NTV2_INPUTSOURCE_ANALOG:
					outRouter.addWithValue (::GetCSC2VidInputSelectEntry (), NTV2_XptAnalogIn);
					break;
				case NTV2_INPUTSOURCE_HDMI:
					outRouter.addWithValue (::GetCSC2VidInputSelectEntry (), NTV2_XptHDMIIn);
					break;
				default:
					return false;
				}

				status = outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptCSC2VidRGB);
			}
			break;
		case NTV2_FBF_10BIT_RGB:
		case NTV2_FBF_10BIT_RGB_PACKED:
		case NTV2_FBF_10BIT_DPX:
		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
		case NTV2_FBF_24BIT_BGR:
		case NTV2_FBF_24BIT_RGB:
			if ( channel == NTV2_CHANNEL1)
			{
				if ( dualLink )
				{
					status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (),NTV2_XptDuallinkIn);
				}
				else
				{
					switch ( inputSource )
					{
					case NTV2_INPUTSOURCE_SDI1:
						outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn1);
						break;
					case NTV2_INPUTSOURCE_SDI2:
						outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn2);
						break;
					case NTV2_INPUTSOURCE_ANALOG:
						outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptAnalogIn);
						break;
					case NTV2_INPUTSOURCE_HDMI:
						outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptHDMIIn);
						break;
					default:
						return false;
					}
					status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptCSC1VidRGB);
				}
			}
			else
			{
				switch ( inputSource )
				{
				case NTV2_INPUTSOURCE_SDI1:
					outRouter.addWithValue (::GetCSC2VidInputSelectEntry (), NTV2_XptSDIIn1);
					break;
				case NTV2_INPUTSOURCE_SDI2:
					outRouter.addWithValue (::GetCSC2VidInputSelectEntry (), NTV2_XptSDIIn2);
					break;
				case NTV2_INPUTSOURCE_ANALOG:
					outRouter.addWithValue (::GetCSC2VidInputSelectEntry (), NTV2_XptAnalogIn);
					break;
				case NTV2_INPUTSOURCE_HDMI:
					outRouter.addWithValue (::GetCSC2VidInputSelectEntry (), NTV2_XptHDMIIn);
					break;
				case NTV2_INPUTSOURCE_DUALLINK:
					outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn1);
					outRouter.addWithValue (::GetCSC1VidInputSelectEntry (), NTV2_XptSDIIn2);
					break;
				default:
					return false;
				}
				status = outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptCSC2VidRGB);
			}
			break;
		case NTV2_FBF_8BIT_DVCPRO:
		case NTV2_FBF_8BIT_HDV:
			switch ( inputSource )
			{
			case NTV2_INPUTSOURCE_SDI1:
				outRouter.addWithValue (::GetCompressionModInputSelectEntry (), NTV2_XptSDIIn1);
				break;
			case NTV2_INPUTSOURCE_SDI2:
				outRouter.addWithValue (::GetCompressionModInputSelectEntry (), NTV2_XptSDIIn2);
				break;
			case NTV2_INPUTSOURCE_ANALOG:
				outRouter.addWithValue (::GetCompressionModInputSelectEntry (), NTV2_XptAnalogIn);
				break;
			case NTV2_INPUTSOURCE_HDMI:
				outRouter.addWithValue (::GetCompressionModInputSelectEntry (), NTV2_XptHDMIIn);
				break;
			default:
				return false;
			}
			if ( channel == NTV2_CHANNEL1)
			{
				status = outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptCompressionModule);
			}
			else
			{
				status = outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptCompressionModule);
			}
			break;

		default:
			if ( channel == NTV2_CHANNEL1)
			{
				//status = outRouter.addWithValue(FrameBuffer1InputSelectEntry,NTV2_XptSDIIn1);
				switch ( inputSource )
				{
				case NTV2_INPUTSOURCE_SDI1:
					outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptSDIIn1);
					break;
				case NTV2_INPUTSOURCE_SDI2:
					outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptSDIIn2);
					break;
				case NTV2_INPUTSOURCE_ANALOG:
					outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptAnalogIn);
					break;
				case NTV2_INPUTSOURCE_HDMI:
					outRouter.addWithValue (::GetFrameBuffer1InputSelectEntry (), NTV2_XptHDMIIn);
					break;
				default:
					return false;
				}
			}
			else
			{
				switch ( inputSource )
				{
				case NTV2_INPUTSOURCE_SDI1:
					outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptSDIIn1);
					break;
				case NTV2_INPUTSOURCE_SDI2:
					outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptSDIIn2);
					break;
				case NTV2_INPUTSOURCE_ANALOG:
					outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptAnalogIn);
					break;
				case NTV2_INPUTSOURCE_HDMI:
					outRouter.addWithValue (::GetFrameBuffer2InputSelectEntry (), NTV2_XptHDMIIn);
					break;
				default:
					return false;
				}
			}
			break;
		}

		return status;
	 }


	// The 10-bit value converted from hex to decimal represents degrees Kelvin.
	// However, the system generates a value that is 5 deg C high. The decimal value
	// minus 273 deg minus 5 deg should be the degrees Centigrade. This is only accurate
	// to 5 deg C, supposedly.

	// These functions used to be a lot more complicated.  Hardware changes
	// reduced them to simple offset & scaling. - STC
	//
	ULWord ConvertFusionAnalogToTempCentigrade(ULWord adc10BitValue)
	{
		// Convert kelvin to centigrade and subtract 5 degrees hot part reports
		// and add empirical 8 degree fudge factor.
		return adc10BitValue -286;

	}

	ULWord ConvertFusionAnalogToMilliVolts(ULWord adc10BitValue, ULWord millivoltsResolution)
	{
		// Different rails have different mv/unit scales.
		return adc10BitValue * millivoltsResolution;
	}
#endif	//	!defined (NTV2_DEPRECATE)


NTV2ConversionMode GetConversionMode( NTV2VideoFormat inFormat, NTV2VideoFormat outFormat)
{
	NTV2ConversionMode cMode = NTV2_CONVERSIONMODE_UNKNOWN;

	switch( inFormat )
	{
		case NTV2_FORMAT_720p_5994:
			if ( outFormat == NTV2_FORMAT_525_5994 )
				cMode = NTV2_720p_5994to525_5994;
			else if ( outFormat == NTV2_FORMAT_1080i_5994)
				cMode = NTV2_720p_5994to1080i_5994;
			else if ( outFormat == NTV2_FORMAT_1080psf_2997_2)
				cMode = NTV2_720p_5994to1080i_5994;
			break;

		case NTV2_FORMAT_720p_5000:
			if ( outFormat == NTV2_FORMAT_625_5000 )
				cMode = NTV2_720p_5000to625_2500;
			else if ( outFormat == NTV2_FORMAT_1080psf_2500 )
				cMode = NTV2_720p_5000to1080i_2500;
			else if ( outFormat == NTV2_FORMAT_1080psf_2500_2)
				cMode = NTV2_720p_5000to1080i_2500;
			break;

		case NTV2_FORMAT_525_2398:
			if ( outFormat == NTV2_FORMAT_1080psf_2398 )
				cMode = NTV2_525_2398to1080i_2398;
			break;

		case NTV2_FORMAT_525_5994:
			if ( outFormat == NTV2_FORMAT_1080i_5994 )
				cMode = NTV2_525_5994to1080i_5994;
			else if (outFormat == NTV2_FORMAT_1080psf_2997_2)
				cMode = NTV2_525_5994to1080i_5994;
			else if ( outFormat == NTV2_FORMAT_720p_5994 )
				cMode = NTV2_525_5994to720p_5994;
			else if ( outFormat == NTV2_FORMAT_525_5994 )
				cMode = NTV2_525_5994to525_5994;
			else if ( outFormat == NTV2_FORMAT_525psf_2997 )
				cMode = NTV2_525_5994to525psf_2997;
			break;

		case NTV2_FORMAT_625_5000:
			if ( outFormat == NTV2_FORMAT_1080psf_2500)
				cMode = NTV2_625_2500to1080i_2500;
			else if ( outFormat == NTV2_FORMAT_1080psf_2500_2)
				cMode = NTV2_625_2500to1080i_2500;
			else if ( outFormat == NTV2_FORMAT_720p_5000 )
				cMode = NTV2_625_2500to720p_5000;
			else if ( outFormat == NTV2_FORMAT_625_5000 )
				cMode = NTV2_625_2500to625_2500;
			else if ( outFormat == NTV2_FORMAT_625psf_2500 )
				cMode = NTV2_625_5000to625psf_2500;
			break;

		case NTV2_FORMAT_720p_6000:
			if ( outFormat == NTV2_FORMAT_1080psf_3000 )
				cMode = NTV2_720p_6000to1080i_3000;
			else if (outFormat == NTV2_FORMAT_1080psf_3000_2 )
				cMode = NTV2_720p_6000to1080i_3000;
			break;

		case NTV2_FORMAT_1080psf_2398:
			if ( outFormat == NTV2_FORMAT_525_2398 )
				cMode = NTV2_1080i2398to525_2398;
			else if ( outFormat == NTV2_FORMAT_525_5994 )
				cMode = NTV2_1080i2398to525_2997;
			else if ( outFormat == NTV2_FORMAT_720p_2398 )
				cMode = NTV2_1080i_2398to720p_2398;
			else if ( outFormat == NTV2_FORMAT_1080i_5994 )
				cMode = NTV2_1080psf_2398to1080i_5994;
			break;

		case NTV2_FORMAT_1080psf_2400:
			if ( outFormat == NTV2_FORMAT_1080i_6000 )
				cMode = NTV2_1080psf_2400to1080i_3000;
			break;
			
		case NTV2_FORMAT_1080psf_2500_2:
			if ( outFormat == NTV2_FORMAT_625_5000 )
				cMode = NTV2_1080i_2500to625_2500;
			else if ( outFormat == NTV2_FORMAT_720p_5000 )
				cMode = NTV2_1080i_2500to720p_5000;
			else if ( outFormat == NTV2_FORMAT_1080psf_2500_2 )
				cMode = NTV2_1080i_5000to1080psf_2500;
			else if ( outFormat == NTV2_FORMAT_1080psf_2500_2 )
				cMode = NTV2_1080psf_2500to1080i_2500;
			break;
		
		case NTV2_FORMAT_1080p_2398:
			if ( outFormat == NTV2_FORMAT_1080i_5994 )
				cMode = NTV2_1080p_2398to1080i_5994;
			break;
		
		case NTV2_FORMAT_1080p_2400:
			if ( outFormat == NTV2_FORMAT_1080i_6000 )
				cMode = NTV2_1080p_2400to1080i_3000;
			break;
			
		case NTV2_FORMAT_1080p_2500:
			if ( outFormat == NTV2_FORMAT_1080i_5000 )
				cMode = NTV2_1080p_2500to1080i_2500;
			break;

		case NTV2_FORMAT_1080i_5000:
			if ( outFormat == NTV2_FORMAT_625_5000 )
				cMode = NTV2_1080i_2500to625_2500;
			else if ( outFormat == NTV2_FORMAT_720p_5000 )
				cMode = NTV2_1080i_2500to720p_5000;
			else if ( outFormat == NTV2_FORMAT_1080psf_2500_2 )
				cMode = NTV2_1080i_5000to1080psf_2500;
			break;

		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080i_5994:
			if ( outFormat == NTV2_FORMAT_525_5994 )
				cMode = NTV2_1080i_5994to525_5994;
			else if ( outFormat == NTV2_FORMAT_720p_5994 )
				cMode = NTV2_1080i_5994to720p_5994;
			else if ( outFormat == NTV2_FORMAT_1080psf_2997_2 )
				cMode = NTV2_1080i_5994to1080psf_2997;
			break;

		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080i_6000:
			if ( outFormat == NTV2_FORMAT_720p_6000 )
				cMode = NTV2_1080i_3000to720p_6000;
			else if ( outFormat == NTV2_FORMAT_1080psf_3000_2 )
				cMode = NTV2_1080i_6000to1080psf_3000;
			break;

		case NTV2_FORMAT_720p_2398:
			if ( outFormat == NTV2_FORMAT_1080psf_2398 )
				cMode = NTV2_720p_2398to1080i_2398;
			break;

		case NTV2_FORMAT_1080p_3000:
			if ( outFormat == NTV2_FORMAT_720p_6000 )
				cMode = NTV2_1080p_3000to720p_6000;
			break;

		default:
			break;
	}

	return cMode;
}

NTV2VideoFormat GetInputForConversionMode(NTV2ConversionMode conversionMode)
{
	NTV2VideoFormat inputFormat = NTV2_FORMAT_UNKNOWN;

	switch( conversionMode )
	{
		case NTV2_525_5994to525_5994: inputFormat = NTV2_FORMAT_525_5994; break;
		case NTV2_525_5994to720p_5994: inputFormat = NTV2_FORMAT_525_5994; break;
		case NTV2_525_5994to1080i_5994: inputFormat = NTV2_FORMAT_525_5994; break;
		case NTV2_525_2398to1080i_2398: inputFormat = NTV2_FORMAT_525_2398; break;
		case NTV2_525_5994to525psf_2997: inputFormat = NTV2_FORMAT_525_5994; break;

		case NTV2_625_2500to625_2500: inputFormat = NTV2_FORMAT_625_5000; break;
		case NTV2_625_2500to720p_5000: inputFormat = NTV2_FORMAT_625_5000; break;
		case NTV2_625_2500to1080i_2500: inputFormat = NTV2_FORMAT_625_5000; break;
		case NTV2_625_5000to625psf_2500: inputFormat = NTV2_FORMAT_625_5000; break;

		case NTV2_720p_5000to625_2500: inputFormat = NTV2_FORMAT_720p_5000; break;
		case NTV2_720p_5000to1080i_2500: inputFormat = NTV2_FORMAT_720p_5000; break;
		case NTV2_720p_5994to525_5994: inputFormat = NTV2_FORMAT_720p_5994; break;
		case NTV2_720p_5994to1080i_5994: inputFormat = NTV2_FORMAT_720p_5994; break;
		case NTV2_720p_6000to1080i_3000: inputFormat = NTV2_FORMAT_720p_6000; break;
		case NTV2_720p_2398to1080i_2398: inputFormat = NTV2_FORMAT_720p_2398; break;

		case NTV2_1080i2398to525_2398: inputFormat = NTV2_FORMAT_1080psf_2398; break;
		case NTV2_1080i2398to525_2997: inputFormat = NTV2_FORMAT_1080psf_2398; break;
		case NTV2_1080i_2398to720p_2398: inputFormat = NTV2_FORMAT_1080psf_2398; break;

		case NTV2_1080i_2500to625_2500: inputFormat = NTV2_FORMAT_1080i_5000; break;
		case NTV2_1080i_2500to720p_5000: inputFormat = NTV2_FORMAT_1080i_5000; break;
		case NTV2_1080i_5994to525_5994: inputFormat = NTV2_FORMAT_1080i_5994; break;
		case NTV2_1080i_5994to720p_5994: inputFormat = NTV2_FORMAT_1080i_5994; break;
		case NTV2_1080i_3000to720p_6000: inputFormat = NTV2_FORMAT_1080i_6000; break;
		case NTV2_1080i_5000to1080psf_2500: inputFormat = NTV2_FORMAT_1080i_5000; break;
		case NTV2_1080i_5994to1080psf_2997: inputFormat = NTV2_FORMAT_1080i_5994; break;
		case NTV2_1080i_6000to1080psf_3000: inputFormat = NTV2_FORMAT_1080i_6000; break;
		case NTV2_1080p_3000to720p_6000: inputFormat = NTV2_FORMAT_1080p_3000; break;

		default: inputFormat = NTV2_FORMAT_UNKNOWN; break;
	}
	return inputFormat;
}


NTV2VideoFormat GetOutputForConversionMode(NTV2ConversionMode conversionMode)
{
	NTV2VideoFormat outputFormat = NTV2_FORMAT_UNKNOWN;

	switch( conversionMode )
	{
		case NTV2_525_5994to525_5994: outputFormat = NTV2_FORMAT_525_5994; break;
		case NTV2_525_5994to720p_5994: outputFormat = NTV2_FORMAT_720p_5994; break;
		case NTV2_525_5994to1080i_5994: outputFormat = NTV2_FORMAT_1080i_5994; break;
		case NTV2_525_2398to1080i_2398: outputFormat = NTV2_FORMAT_1080psf_2398; break;
		case NTV2_525_5994to525psf_2997: outputFormat = NTV2_FORMAT_525psf_2997; break;

		case NTV2_625_2500to625_2500: outputFormat = NTV2_FORMAT_625_5000; break;
		case NTV2_625_2500to720p_5000: outputFormat = NTV2_FORMAT_720p_5000; break;
		case NTV2_625_2500to1080i_2500: outputFormat = NTV2_FORMAT_1080i_5000; break;
		case NTV2_625_5000to625psf_2500: outputFormat = NTV2_FORMAT_625psf_2500; break;

		case NTV2_720p_5000to625_2500: outputFormat = NTV2_FORMAT_625_5000; break;
		case NTV2_720p_5000to1080i_2500: outputFormat = NTV2_FORMAT_1080i_5000; break;
		case NTV2_720p_5994to525_5994: outputFormat = NTV2_FORMAT_525_5994; break;
		case NTV2_720p_5994to1080i_5994: outputFormat = NTV2_FORMAT_1080i_5994; break;
		case NTV2_720p_6000to1080i_3000: outputFormat = NTV2_FORMAT_1080i_6000; break;
		case NTV2_720p_2398to1080i_2398: outputFormat = NTV2_FORMAT_1080psf_2398; break;

		case NTV2_1080i2398to525_2398: outputFormat = NTV2_FORMAT_525_2398; break;
		case NTV2_1080i2398to525_2997: outputFormat = NTV2_FORMAT_525_5994; break;
		case NTV2_1080i_2398to720p_2398: outputFormat = NTV2_FORMAT_720p_2398; break;
		//case NTV2_1080i2400to525_2400: outputFormat = NTV2_FORMAT_525_2400; break;

		//case NTV2_1080p2398to525_2398: outputFormat = NTV2_FORMAT_525_2398; break;
		//case NTV2_1080p2398to525_2997: outputFormat = NTV2_FORMAT_525_5994; break;
		//case NTV2_1080p2400to525_2400: outputFormat = NTV2_FORMAT_525_2400; break;

		case NTV2_1080i_2500to625_2500: outputFormat = NTV2_FORMAT_625_5000; break;
		case NTV2_1080i_2500to720p_5000: outputFormat = NTV2_FORMAT_720p_5000; break;
		case NTV2_1080i_5994to525_5994: outputFormat = NTV2_FORMAT_525_5994; break;
		case NTV2_1080i_5994to720p_5994: outputFormat = NTV2_FORMAT_720p_5994; break;
		case NTV2_1080i_3000to720p_6000: outputFormat = NTV2_FORMAT_720p_6000; break;
		case NTV2_1080i_5000to1080psf_2500: outputFormat = NTV2_FORMAT_1080psf_2500_2; break;
		case NTV2_1080i_5994to1080psf_2997: outputFormat = NTV2_FORMAT_1080psf_2997_2; break;
		case NTV2_1080i_6000to1080psf_3000: outputFormat = NTV2_FORMAT_1080psf_3000_2; break;
		case NTV2_1080p_3000to720p_6000: outputFormat = NTV2_FORMAT_720p_6000; break;
		default: outputFormat = NTV2_FORMAT_UNKNOWN; break;
	}
	return outputFormat;
}


ostream & operator << (ostream & inOutStream, const NTV2FormatDescriptor & inFormatDesc)
{
	return inOutStream	<< inFormatDesc.numLines << " lines, "
						<< inFormatDesc.numPixels << " pixels/line, "
						<< inFormatDesc.linePitch << " longwords/line, 1st video line="
						<< inFormatDesc.firstActiveLine;

}	//	NTV2FormatDescriptor operator <<


string NTV2ChannelToString (const NTV2Channel inValue, const bool inForRetailDisplay)
{
	switch (inValue)
	{
		case NTV2_CHANNEL1:	return inForRetailDisplay ? "Ch1" : "NTV2_CHANNEL1";
		case NTV2_CHANNEL2:	return inForRetailDisplay ? "Ch2" : "NTV2_CHANNEL2";
		case NTV2_CHANNEL3:	return inForRetailDisplay ? "Ch3" : "NTV2_CHANNEL3";
		case NTV2_CHANNEL4:	return inForRetailDisplay ? "Ch4" : "NTV2_CHANNEL4";
		case NTV2_CHANNEL5:	return inForRetailDisplay ? "Ch5" : "NTV2_CHANNEL5";
		case NTV2_CHANNEL6:	return inForRetailDisplay ? "Ch6" : "NTV2_CHANNEL6";
		case NTV2_CHANNEL7:	return inForRetailDisplay ? "Ch7" : "NTV2_CHANNEL7";
		case NTV2_CHANNEL8:	return inForRetailDisplay ? "Ch8" : "NTV2_CHANNEL8";
		case NTV2_MAX_NUM_CHANNELS:	break;
	}
	return "";
}


string NTV2CrosspointToString (const NTV2Crosspoint inChannel)
{
	std::ostringstream	oss;
	oss	<< (::IsNTV2CrosspointInput (inChannel) ? "Capture " : "Playout ")
		<< (::IsNTV2CrosspointInput (inChannel) ? ::GetIndexForNTV2CrosspointInput (inChannel) : ::GetIndexForNTV2CrosspointChannel (inChannel)) + 1;
	return oss.str ();
}


string NTV2CrosspointIDToString	(const NTV2CrosspointID	inValue, const bool inForRetailDisplay)
{
	switch (inValue)
	{
		case NTV2_XptBlack:						return inForRetailDisplay ? "Black"						: "NTV2_XptBlack";
		case NTV2_XptSDIIn1:					return inForRetailDisplay ? "SDI In 1"					: "NTV2_XptSDIIn1";
		case NTV2_XptSDIIn1DS2:					return inForRetailDisplay ? "SDI In 1 DS2"				: "NTV2_XptSDIIn1DS2";
		case NTV2_XptSDIIn2:					return inForRetailDisplay ? "SDI In 2"					: "NTV2_XptSDIIn2";
		case NTV2_XptSDIIn2DS2:					return inForRetailDisplay ? "SDI In 2 DS2"				: "NTV2_XptSDIIn2DS2";
		case NTV2_XptLUT1YUV:					return inForRetailDisplay ? "LUT 1 YUV"					: "NTV2_XptLUT1YUV";
		case NTV2_XptCSC1VidYUV:				return inForRetailDisplay ? "CSC 1 Vid YUV"				: "NTV2_XptCSC1VidYUV";
		case NTV2_XptConversionModule:			return inForRetailDisplay ? "Conversion Module" 		: "NTV2_XptConversionModule";
		case NTV2_XptCompressionModule:			return inForRetailDisplay ? "Compression Module"		: "NTV2_XptCompressionModule";
		case NTV2_XptFrameBuffer1YUV:			return inForRetailDisplay ? "FB 1 YUV"					: "NTV2_XptFrameBuffer1YUV";
		case NTV2_XptFrameSync1YUV:				return inForRetailDisplay ? "FS 1 YUV"					: "NTV2_XptFrameSync1YUV";
		case NTV2_XptFrameSync2YUV:				return inForRetailDisplay ? "FS 2 YUV"					: "NTV2_XptFrameSync2YUV";
		case NTV2_XptDuallinkOut:				return inForRetailDisplay ? "DL Out"					: "NTV2_XptDuallinkOut";
		case NTV2_XptDuallinkOutDS2:			return inForRetailDisplay ? "DL Out DS2"				: "NTV2_XptDuallinkOutDS2";
		case NTV2_XptDuallinkOut2:				return inForRetailDisplay ? "DL Out 2"					: "NTV2_XptDuallinkOut2";
		case NTV2_XptDuallinkOut2DS2:			return inForRetailDisplay ? "DL Out 2 DS2"				: "NTV2_XptDuallinkOut2DS2";
		case NTV2_XptDuallinkOut3:				return inForRetailDisplay ? "DL Out 3"					: "NTV2_XptDuallinkOut3";
		case NTV2_XptDuallinkOut3DS2:			return inForRetailDisplay ? "DL Out 3 DS2"				: "NTV2_XptDuallinkOut3DS2";
		case NTV2_XptDuallinkOut4:				return inForRetailDisplay ? "DL Out 4"					: "NTV2_XptDuallinkOut4";
		case NTV2_XptDuallinkOut4DS2:			return inForRetailDisplay ? "DL Out 4 DS2"				: "NTV2_XptDuallinkOut4DS2";
		case NTV2_XptAlphaOut:					return inForRetailDisplay ? "Alpha Out"					: "NTV2_XptAlphaOut";
		case NTV2_XptAnalogIn:					return inForRetailDisplay ? "Analog In"					: "NTV2_XptAnalogIn";
		case NTV2_XptHDMIIn:					return inForRetailDisplay ? "HDMI In"					: "NTV2_XptHDMIIn";
		case NTV2_XptHDMIInQ2:					return inForRetailDisplay ? "HDMI In Q2"				: "NTV2_XptHDMIInQ2";
		case NTV2_XptHDMIInQ3:					return inForRetailDisplay ? "HDMI In Q3"				: "NTV2_XptHDMIInQ3";
		case NTV2_XptHDMIInQ4:					return inForRetailDisplay ? "HDMI In Q4"				: "NTV2_XptHDMIInQ4";
		case NTV2_XptHDMIInRGB:					return inForRetailDisplay ? "HDMI In RGB"				: "NTV2_XptHDMIInRGB";
		case NTV2_XptHDMIInQ2RGB:				return inForRetailDisplay ? "HDMI In Q2 RGB"			: "NTV2_XptHDMIInQ2RGB";
		case NTV2_XptHDMIInQ3RGB:				return inForRetailDisplay ? "HDMI In Q3 RGB"			: "NTV2_XptHDMIInQ3RGB";
		case NTV2_XptHDMIInQ4RGB:				return inForRetailDisplay ? "HDMI In Q4 RGB"			: "NTV2_XptHDMIInQ4RGB";
		case NTV2_XptDuallinkIn:				return inForRetailDisplay ? "DL In"						: "NTV2_XptDuallinkIn";
		case NTV2_XptDuallinkIn2:				return inForRetailDisplay ? "DL In 2"					: "NTV2_XptDuallinkIn2";
		case NTV2_XptDuallinkIn3:				return inForRetailDisplay ? "DL In 3"					: "NTV2_XptDuallinkIn3";
		case NTV2_XptDuallinkIn4:				return inForRetailDisplay ? "DL In 4"					: "NTV2_XptDuallinkIn4";
		case NTV2_XptLUT1RGB:					return inForRetailDisplay ? "LUT 1 RGB"					: "NTV2_XptLUT1RGB";
		case NTV2_XptCSC1VidRGB:				return inForRetailDisplay ? "CSC 1 Vid RGB"				: "NTV2_XptCSC1VidRGB";
		case NTV2_XptFrameBuffer1RGB:			return inForRetailDisplay ? "FB 1 RGB"					: "NTV2_XptFrameBuffer1RGB";
		case NTV2_XptFrameSync1RGB:				return inForRetailDisplay ? "FS 1 RGB"					: "NTV2_XptFrameSync1RGB";
		case NTV2_XptFrameSync2RGB:				return inForRetailDisplay ? "FS 2 RGB"					: "NTV2_XptFrameSync2RGB";
		case NTV2_XptLUT2RGB:					return inForRetailDisplay ? "LUT 2 RGB"					: "NTV2_XptLUT2RGB";
		case NTV2_XptCSC1KeyYUV:				return inForRetailDisplay ? "CSC 1 Key YUV"				: "NTV2_XptCSC1KeyYUV";
		case NTV2_XptFrameBuffer2YUV:			return inForRetailDisplay ? "FB 2 YUV"					: "NTV2_XptFrameBuffer2YUV";
		case NTV2_XptFrameBuffer2RGB:			return inForRetailDisplay ? "FB 2 RGB"					: "NTV2_XptFrameBuffer2RGB";
		case NTV2_XptCSC2VidYUV:				return inForRetailDisplay ? "CSC 2 Vid YUV"				: "NTV2_XptCSC2VidYUV";
		case NTV2_XptCSC2VidRGB:				return inForRetailDisplay ? "CSC 2 Vid RGB"				: "NTV2_XptCSC2VidRGB";
		case NTV2_XptCSC2KeyYUV:				return inForRetailDisplay ? "CSC 2 Key YUV"				: "NTV2_XptCSC2KeyYUV";
		case NTV2_XptMixerVidYUV:				return inForRetailDisplay ? "Mixer Vid YUV"				: "NTV2_XptMixerVidYUV";
		case NTV2_XptMixerKeyYUV:				return inForRetailDisplay ? "Mixer Key YUV"				: "NTV2_XptMixerKeyYUV";
		case NTV2_XptWaterMarkerRGB:			return inForRetailDisplay ? "Water Marker RGB"			: "NTV2_XptWaterMarkerRGB";
		case NTV2_XptWaterMarkerYUV:			return inForRetailDisplay ? "Water Marker YUV"			: "NTV2_XptWaterMarkerYUV";
		case NTV2_XptWaterMarker2RGB:			return inForRetailDisplay ? "Water Marker 2 RGB"		: "NTV2_XptWaterMarker2RGB";
		case NTV2_XptWaterMarker2YUV:			return inForRetailDisplay ? "Water Marker 2 YUV"		: "NTV2_XptWaterMarker2YUV";
		case NTV2_XptIICTRGB:					return inForRetailDisplay ? "IICT RGB"					: "NTV2_XptIICTRGB";
		case NTV2_XptIICT2RGB:					return inForRetailDisplay ? "IICT 2 RGB"				: "NTV2_XptIICT2RGB";
		case NTV2_XptTestPatternYUV:			return inForRetailDisplay ? "Test Pattern YUV"			: "NTV2_XptTestPatternYUV";
		case NTV2_XptDCIMixerVidYUV:			return inForRetailDisplay ? "DCI Mixer Vid YUV"			: "NTV2_XptDCIMixerVidYUV";
		case NTV2_XptDCIMixerVidRGB:			return inForRetailDisplay ? "DCI Mixer Vid RGB"			: "NTV2_XptDCIMixerVidRGB";
		case NTV2_XptMixer2VidYUV:				return inForRetailDisplay ? "Mixer 2 Vid YUV"			: "NTV2_XptMixer2VidYUV";
		case NTV2_XptMixer2KeyYUV:				return inForRetailDisplay ? "Mixer 2 Key YUV"			: "NTV2_XptMixer2KeyYUV";
		case NTV2_XptStereoCompressorOut:		return inForRetailDisplay ? "Stereo Compressor Out"		: "NTV2_XptStereoCompressorOut";
		case NTV2_XptLUT3Out:					return inForRetailDisplay ? "LUT 3 Out"					: "NTV2_XptLUT3Out";
		case NTV2_XptLUT4Out:					return inForRetailDisplay ? "LUT 4 Out"					: "NTV2_XptLUT4Out";
		case NTV2_XptFrameBuffer3YUV:			return inForRetailDisplay ? "FB 3 YUV"					: "NTV2_XptFrameBuffer3YUV";
		case NTV2_XptFrameBuffer3RGB:			return inForRetailDisplay ? "FB 3 RGB"					: "NTV2_XptFrameBuffer3RGB";
		case NTV2_XptFrameBuffer4YUV:			return inForRetailDisplay ? "FB 4 YUV"					: "NTV2_XptFrameBuffer4YUV";
		case NTV2_XptFrameBuffer4RGB:			return inForRetailDisplay ? "FB 4 RGB"					: "NTV2_XptFrameBuffer4RGB";
		case NTV2_XptSDIIn3:					return inForRetailDisplay ? "SDI In 3"					: "NTV2_XptSDIIn3";
		case NTV2_XptSDIIn3DS2:					return inForRetailDisplay ? "SDI In 3 DS2"				: "NTV2_XptSDIIn3DS2";
		case NTV2_XptSDIIn4:					return inForRetailDisplay ? "SDI In 4"					: "NTV2_XptSDIIn4";
		case NTV2_XptSDIIn4DS2:					return inForRetailDisplay ? "SDI In 4 DS2"				: "NTV2_XptSDIIn4DS2";
		case NTV2_XptCSC3VidYUV:				return inForRetailDisplay ? "CSC 3 Vid YUV"				: "NTV2_XptCSC3VidYUV";
		case NTV2_XptCSC3VidRGB:				return inForRetailDisplay ? "CSC 3 Vid RGB"				: "NTV2_XptCSC3VidRGB";
		case NTV2_XptCSC3KeyYUV:				return inForRetailDisplay ? "CSC 3 Key YUV"				: "NTV2_XptCSC3KeyYUV";
		case NTV2_XptCSC4VidYUV:				return inForRetailDisplay ? "CSC 4 Vid YUV"				: "NTV2_XptCSC4VidYUV";
		case NTV2_XptCSC4VidRGB:				return inForRetailDisplay ? "CSC 4 Vid RGB"				: "NTV2_XptCSC4VidRGB";
		case NTV2_XptCSC4KeyYUV:				return inForRetailDisplay ? "CSC 4 Key YUV"				: "NTV2_XptCSC4KeyYUV";
		case NTV2_XptCSC5VidYUV:				return inForRetailDisplay ? "CSC 5 Vid YUV"				: "NTV2_XptCSC5VidYUV";
		case NTV2_XptCSC5VidRGB:				return inForRetailDisplay ? "CSC 5 Vid RGB"				: "NTV2_XptCSC5VidRGB";
		case NTV2_XptCSC5KeyYUV:				return inForRetailDisplay ? "CSC 5 Key YUV"				: "NTV2_XptCSC5KeyYUV";
		case NTV2_XptLUT5Out:					return inForRetailDisplay ? "LUT 5 Out"					: "NTV2_XptLUT5Out";
		case NTV2_XptDuallinkOut5:				return inForRetailDisplay ? "DL Out 5"					: "NTV2_XptDuallinkOut5";
		case NTV2_XptDuallinkOut5DS2:			return inForRetailDisplay ? "DL Out 5 DS2"				: "NTV2_XptDuallinkOut5DS2";
		case NTV2_Xpt4KDownConverterOut:		return inForRetailDisplay ? "4K DownConverter Out"		: "NTV2_Xpt4KDownConverterOut";
		case NTV2_Xpt4KDownConverterOutRGB:		return inForRetailDisplay ? "4K DownConverter Out RGB"	: "NTV2_Xpt4KDownConverterOutRGB";
		case NTV2_XptFrameBuffer5YUV:			return inForRetailDisplay ? "FB 5 YUV"					: "NTV2_XptFrameBuffer5YUV";
		case NTV2_XptFrameBuffer5RGB:			return inForRetailDisplay ? "FB 5 RGB"					: "NTV2_XptFrameBuffer5RGB";
		case NTV2_XptFrameBuffer6YUV:			return inForRetailDisplay ? "FB 6 YUV"					: "NTV2_XptFrameBuffer6YUV";
		case NTV2_XptFrameBuffer6RGB:			return inForRetailDisplay ? "FB 6 RGB"					: "NTV2_XptFrameBuffer6RGB";
		case NTV2_XptFrameBuffer7YUV:			return inForRetailDisplay ? "FB 7 YUV"					: "NTV2_XptFrameBuffer7YUV";
		case NTV2_XptFrameBuffer7RGB:			return inForRetailDisplay ? "FB 7 RGB"					: "NTV2_XptFrameBuffer7RGB";
		case NTV2_XptFrameBuffer8YUV:			return inForRetailDisplay ? "FB 8 YUV"					: "NTV2_XptFrameBuffer8YUV";
		case NTV2_XptFrameBuffer8RGB:			return inForRetailDisplay ? "FB 8 RGB"					: "NTV2_XptFrameBuffer8RGB";
		case NTV2_XptSDIIn5:					return inForRetailDisplay ? "SDI In 5"					: "NTV2_XptSDIIn5";
		case NTV2_XptSDIIn5DS2:					return inForRetailDisplay ? "SDI In 5 DS2"				: "NTV2_XptSDIIn5DS2";
		case NTV2_XptSDIIn6:					return inForRetailDisplay ? "SDI In 6"					: "NTV2_XptSDIIn6";
		case NTV2_XptSDIIn6DS2:					return inForRetailDisplay ? "SDI In 6 DS2"				: "NTV2_XptSDIIn6DS2";
		case NTV2_XptSDIIn7:					return inForRetailDisplay ? "SDI In 7"					: "NTV2_XptSDIIn7";
		case NTV2_XptSDIIn7DS2:					return inForRetailDisplay ? "SDI In 7 DS2"				: "NTV2_XptSDIIn7DS2";
		case NTV2_XptSDIIn8:					return inForRetailDisplay ? "SDI In 8"					: "NTV2_XptSDIIn8";
		case NTV2_XptSDIIn8DS2:					return inForRetailDisplay ? "SDI In 8 DS2"				: "NTV2_XptSDIIn8DS2";
		case NTV2_XptCSC6VidYUV:				return inForRetailDisplay ? "CSC 6 Vid YUV"				: "NTV2_XptCSC6VidYUV";
		case NTV2_XptCSC6VidRGB:				return inForRetailDisplay ? "CSC 6 Vid RGB"				: "NTV2_XptCSC6VidRGB";
		case NTV2_XptCSC6KeyYUV:				return inForRetailDisplay ? "CSC 6 Key YUV"				: "NTV2_XptCSC6KeyYUV";
		case NTV2_XptCSC7VidYUV:				return inForRetailDisplay ? "CSC 7 Vid YUV"				: "NTV2_XptCSC7VidYUV";
		case NTV2_XptCSC7VidRGB:				return inForRetailDisplay ? "CSC 7 Vid RGB"				: "NTV2_XptCSC7VidRGB";
		case NTV2_XptCSC7KeyYUV:				return inForRetailDisplay ? "CSC 7 Key YUV"				: "NTV2_XptCSC7KeyYUV";
		case NTV2_XptCSC8VidYUV:				return inForRetailDisplay ? "CSC 8 Vid YUV"				: "NTV2_XptCSC8VidYUV";
		case NTV2_XptCSC8VidRGB:				return inForRetailDisplay ? "CSC 8 Vid RGB"				: "NTV2_XptCSC8VidRGB";
		case NTV2_XptCSC8KeyYUV:				return inForRetailDisplay ? "CSC 8 Key YUV"				: "NTV2_XptCSC8KeyYUV";
		case NTV2_XptLUT6Out:					return inForRetailDisplay ? "LUT 6 Out"					: "NTV2_XptLUT6Out";
		case NTV2_XptLUT7Out:					return inForRetailDisplay ? "LUT 7 Out"					: "NTV2_XptLUT7Out";
		case NTV2_XptLUT8Out:					return inForRetailDisplay ? "LUT 8 Out"					: "NTV2_XptLUT8Out";
		case NTV2_XptDuallinkOut6:				return inForRetailDisplay ? "DL Out 6"					: "NTV2_XptDuallinkOut6";
		case NTV2_XptDuallinkOut6DS2:			return inForRetailDisplay ? "DL Out 6 DS2"				: "NTV2_XptDuallinkOut6DS2";
		case NTV2_XptDuallinkOut7:				return inForRetailDisplay ? "DL Out 7"					: "NTV2_XptDuallinkOut7";
		case NTV2_XptDuallinkOut7DS2:			return inForRetailDisplay ? "DL Out 7 DS2"				: "NTV2_XptDuallinkOut7DS2";
		case NTV2_XptDuallinkOut8:				return inForRetailDisplay ? "DL Out 8"					: "NTV2_XptDuallinkOut8";
		case NTV2_XptDuallinkOut8DS2:			return inForRetailDisplay ? "DL Out 8 DS2"				: "NTV2_XptDuallinkOut8DS2";
		case NTV2_XptMixer3VidYUV:				return inForRetailDisplay ? "Mixer 3 Vid YUV"			: "NTV2_XptMixer3VidYUV";
		case NTV2_XptMixer3KeyYUV:				return inForRetailDisplay ? "Mixer 3 Key YUV"			: "NTV2_XptMixer3KeyYUV";
		case NTV2_XptMixer4VidYUV:				return inForRetailDisplay ? "Mixer 4 Vid YUV"			: "NTV2_XptMixer4VidYUV";
		case NTV2_XptMixer4KeyYUV:				return inForRetailDisplay ? "Mixer 4 Key YUV"			: "NTV2_XptMixer4KeyYUV";
		case NTV2_XptDuallinkIn5:				return inForRetailDisplay ? "DL In 5"					: "NTV2_XptDuallinkIn5";
		case NTV2_XptDuallinkIn6:				return inForRetailDisplay ? "DL In 6"					: "NTV2_XptDuallinkIn6";
		case NTV2_XptDuallinkIn7:				return inForRetailDisplay ? "DL In 7"					: "NTV2_XptDuallinkIn7";
		case NTV2_XptDuallinkIn8:				return inForRetailDisplay ? "DL In 8"					: "NTV2_XptDuallinkIn8";
		case NTV2_Xpt425Mux1AYUV:				return inForRetailDisplay ? "425Mux1AYUV"				: "NTV2_Xpt425Mux1AYUV";
		case NTV2_Xpt425Mux1ARGB:				return inForRetailDisplay ? "425Mux1ARGB"				: "NTV2_Xpt425Mux1ARGB";
		case NTV2_Xpt425Mux1BYUV:				return inForRetailDisplay ? "425Mux1BYUV"				: "NTV2_Xpt425Mux1BYUV";
		case NTV2_Xpt425Mux1BRGB:				return inForRetailDisplay ? "425Mux1BRGB"				: "NTV2_Xpt425Mux1BRGB";
		case NTV2_Xpt425Mux2AYUV:				return inForRetailDisplay ? "425Mux2AYUV"				: "NTV2_Xpt425Mux2AYUV";
		case NTV2_Xpt425Mux2ARGB:				return inForRetailDisplay ? "425Mux2ARGB"				: "NTV2_Xpt425Mux2ARGB";
		case NTV2_Xpt425Mux2BYUV:				return inForRetailDisplay ? "425Mux2BYUV"				: "NTV2_Xpt425Mux2BYUV";
		case NTV2_Xpt425Mux2BRGB:				return inForRetailDisplay ? "425Mux2BRGB"				: "NTV2_Xpt425Mux2BRGB";
		case NTV2_Xpt425Mux3AYUV:				return inForRetailDisplay ? "425Mux3AYUV"				: "NTV2_Xpt425Mux3AYUV";
		case NTV2_Xpt425Mux3ARGB:				return inForRetailDisplay ? "425Mux3ARGB"				: "NTV2_Xpt425Mux3ARGB";
		case NTV2_Xpt425Mux3BYUV:				return inForRetailDisplay ? "425Mux3BYUV"				: "NTV2_Xpt425Mux3BYUV";
		case NTV2_Xpt425Mux3BRGB:				return inForRetailDisplay ? "425Mux3BRGB"				: "NTV2_Xpt425Mux3BRGB";
		case NTV2_Xpt425Mux4AYUV:				return inForRetailDisplay ? "425Mux4AYUV"				: "NTV2_Xpt425Mux4AYUV";
		case NTV2_Xpt425Mux4ARGB:				return inForRetailDisplay ? "425Mux4ARGB"				: "NTV2_Xpt425Mux4ARGB";
		case NTV2_Xpt425Mux4BYUV:				return inForRetailDisplay ? "425Mux4BYUV"				: "NTV2_Xpt425Mux4BYUV";
		case NTV2_Xpt425Mux4BRGB:				return inForRetailDisplay ? "425Mux4BRGB"				: "NTV2_Xpt425Mux4BRGB";
		case NTV2_XptFrameBuffer1_425YUV:		return inForRetailDisplay ? "425FB 1 YUV"				: "NTV2_XptFrameBuffer1_425YUV";
		case NTV2_XptFrameBuffer1_425RGB:		return inForRetailDisplay ? "425FB 1 RGB"				: "NTV2_XptFrameBuffer1_425RGB";
		case NTV2_XptFrameBuffer2_425YUV:		return inForRetailDisplay ? "425FB 2 YUV"				: "NTV2_XptFrameBuffer2_425YUV";
		case NTV2_XptFrameBuffer2_425RGB:		return inForRetailDisplay ? "425FB 2 RGB"				: "NTV2_XptFrameBuffer2_425RGB";
		case NTV2_XptFrameBuffer3_425YUV:		return inForRetailDisplay ? "425FB 3 YUV"				: "NTV2_XptFrameBuffer3_425YUV";
		case NTV2_XptFrameBuffer3_425RGB:		return inForRetailDisplay ? "425FB 3 RGB"				: "NTV2_XptFrameBuffer3_425RGB";
		case NTV2_XptFrameBuffer4_425YUV:		return inForRetailDisplay ? "425FB 4 YUV"				: "NTV2_XptFrameBuffer4_425YUV";
		case NTV2_XptFrameBuffer4_425RGB:		return inForRetailDisplay ? "425FB 4 RGB"				: "NTV2_XptFrameBuffer4_425RGB";
		case NTV2_XptFrameBuffer5_425YUV:		return inForRetailDisplay ? "425FB 5 YUV"				: "NTV2_XptFrameBuffer5_425YUV";
		case NTV2_XptFrameBuffer5_425RGB:		return inForRetailDisplay ? "425FB 5 RGB"				: "NTV2_XptFrameBuffer5_425RGB";
		case NTV2_XptFrameBuffer6_425YUV:		return inForRetailDisplay ? "425FB 6 YUV"				: "NTV2_XptFrameBuffer6_425YUV";
		case NTV2_XptFrameBuffer6_425RGB:		return inForRetailDisplay ? "425FB 6 RGB"				: "NTV2_XptFrameBuffer6_425RGB";
		case NTV2_XptFrameBuffer7_425YUV:		return inForRetailDisplay ? "425FB 7 YUV"				: "NTV2_XptFrameBuffer7_425YUV";
		case NTV2_XptFrameBuffer7_425RGB:		return inForRetailDisplay ? "425FB 7 RGB"				: "NTV2_XptFrameBuffer7_425RGB";
		case NTV2_XptFrameBuffer8_425YUV:		return inForRetailDisplay ? "425FB 8 YUV"				: "NTV2_XptFrameBuffer8_425YUV";
		case NTV2_XptFrameBuffer8_425RGB:		return inForRetailDisplay ? "425FB 8 RGB"				: "NTV2_XptFrameBuffer8_425RGB";
		case NTV2_XptRuntimeCalc:				return inForRetailDisplay ? "Runtime Calc"				: "NTV2_XptRuntimeCalc";
		#if !defined (NTV2_DEPRECATE)
			case NTV2_XptFS1SecondConverter:	return inForRetailDisplay ? "FS 1 2nd Conv"				: "NTV2_XptFS1SecondConverter";
			case NTV2_XptFS1ProcAmp:			return inForRetailDisplay ? "FS 1 ProcAmp"				: "NTV2_XptFS1ProcAmp";
		#endif	//	!defined (NTV2_DEPRECATE)
		default: break;
	}	//	switch on inValue
	return string ();
}	//	NTV2CrosspointIDToString


string NTV2VideoFormatToString (const NTV2VideoFormat inFormat)
{
	return (inFormat < NTV2_FORMAT_END_HIGH_DEF_FORMATS2) ? NTV2VideoFormatStrings [inFormat] : "";

/*	const char *result = "";
	
	switch (fmt)
	{
		//case NTV2_FORMAT_1080psf_2500:
		case NTV2_FORMAT_1080i_5000:			result = "1080i50/1080psf25";			break;
		//case NTV2_FORMAT_1080psf_2997:
		case NTV2_FORMAT_1080i_5994:			result = "1080i_5994/1080psf_2997";		break;
		//case NTV2_FORMAT_1080psf_3000:
		case NTV2_FORMAT_1080i_6000:			result = "1080i60/1080psf_30";			break;
		case NTV2_FORMAT_720p_5994:				result = "720p5994";					break;
		case NTV2_FORMAT_720p_6000:				result = "720p60";						break;
		case NTV2_FORMAT_1080psf_2398:			result = "1080psf2398";					break;
		case NTV2_FORMAT_1080psf_2400:			result = "1080psf24";					break;
		case NTV2_FORMAT_1080p_2997:			result = "1080p2997";					break;
		case NTV2_FORMAT_1080p_3000:			result = "1080p30";						break;
		case NTV2_FORMAT_1080p_2500:			result = "1080p25";						break;
		case NTV2_FORMAT_1080p_2398:			result = "1080p2398";					break;
		case NTV2_FORMAT_1080p_2400:			result = "1080p24";						break;
		case NTV2_FORMAT_1080p_2K_2398:			result = "1080p2K2398";					break;
		case NTV2_FORMAT_1080p_2K_2400:			result = "1080p2K24";					break;
		case NTV2_FORMAT_1080p_2K_2500:			result = "1080p2K25";					break;
		case NTV2_FORMAT_1080p_2K_2997:			result = "1080p2K2997";					break;
		case NTV2_FORMAT_1080p_2K_3000:			result = "1080p2K30";					break;
		case NTV2_FORMAT_1080p_2K_4795:			result = "1080p2K4795/1080p2K4795a";	break;
		case NTV2_FORMAT_1080p_2K_4800:			result = "1080p2K48/1080p2K48a";		break;
		case NTV2_FORMAT_1080p_2K_5000:			result = "1080p2K50/1080p2K50a";		break;
		case NTV2_FORMAT_1080p_2K_5994:			result = "1080p2K5994/1080p2K5994a";	break;
		case NTV2_FORMAT_1080p_2K_6000:			result = "1080p2K60/1080p2K60a";		break;
		case NTV2_FORMAT_1080psf_2K_2398:		result = "1080psf2K2398";				break;
		case NTV2_FORMAT_1080psf_2K_2400:		result = "1080psf2K24";					break;
		case NTV2_FORMAT_1080psf_2K_2500:		result = "1080psf2K25";					break;
		case NTV2_FORMAT_1080psf_2500_2:		result = "1080psf25-2";					break;
		case NTV2_FORMAT_1080psf_2997_2:		result = "1080psf2997-2";				break;
		case NTV2_FORMAT_1080psf_3000_2:		result = "1080psf30-2";					break;
		case NTV2_FORMAT_720p_5000:				result = "720p50";						break;
		case NTV2_FORMAT_1080p_5000_B:			result = "1080p50b/1080p50";			break;
		case NTV2_FORMAT_1080p_5994_B:			result = "1080p5994b/1080p5994";		break;
		case NTV2_FORMAT_1080p_6000_B:			result = "1080p60b/1080p60";			break;
		case NTV2_FORMAT_1080p_5000_A:			result = "1080p50a";					break;
		case NTV2_FORMAT_1080p_5994_A:			result = "1080p5994a";					break;
		case NTV2_FORMAT_1080p_6000_A:			result = "1080p60a";					break;
		case NTV2_FORMAT_END_HIGH_DEF_FORMATS:	result = "";							break;
		case NTV2_FORMAT_720p_2398:				result = "720p2398";					break;
		case NTV2_FORMAT_720p_2500:				result = "720p25";						break;
		case NTV2_FORMAT_525_5994:				result = "525@5994";					break;
		case NTV2_FORMAT_625_5000:				result = "625@50";						break;
		case NTV2_FORMAT_525_2398:				result = "525@2398";					break;
		case NTV2_FORMAT_525_2400:				result = "525@24";						break;
		case NTV2_FORMAT_525psf_2997:			result = "525psf2997";					break;
		case NTV2_FORMAT_625psf_2500:			result = "625psf25";					break;
		case NTV2_FORMAT_END_STANDARD_DEF_FORMATS:	result = "";						break;
		case NTV2_FORMAT_2K_1498:				result = "2K@1498";						break;
		case NTV2_FORMAT_2K_1500:				result = "2K@15";						break;
		case NTV2_FORMAT_2K_2398:				result = "2K@2398";						break;
		case NTV2_FORMAT_2K_2400:				result = "2K@24";						break;
		case NTV2_FORMAT_2K_2500:				result = "2K@25";						break;
		case NTV2_FORMAT_END_2K_DEF_FORMATS:	result = "";							break;
		case NTV2_FORMAT_4x1920x1080psf_2398:	result = "4x1920x1080psf2398";			break;
		case NTV2_FORMAT_4x1920x1080psf_2400:	result = "4x1920x1080psf24";			break;
		case NTV2_FORMAT_4x1920x1080psf_2500:	result = "4x1920x1080psf25";			break;
		case NTV2_FORMAT_4x1920x1080psf_2997:	result = "4x1920x1080psf2997";			break;
		case NTV2_FORMAT_4x1920x1080psf_3000:	result = "4x1920x1080psf30";			break;
		case NTV2_FORMAT_4x1920x1080p_2398:		result = "4x1920x1080p2398";			break;
		case NTV2_FORMAT_4x1920x1080p_2400:		result = "4x1920x1080p24";				break;
		case NTV2_FORMAT_4x1920x1080p_2500:		result = "4x1920x1080p25";				break;
		case NTV2_FORMAT_4x1920x1080p_2997:		result = "4x1920x1080p2997";			break;
		case NTV2_FORMAT_4x1920x1080p_3000:		result = "4x1920x1080p30";				break;
		case NTV2_FORMAT_4x2048x1080psf_2398:	result = "4x2048x1080psf2398";			break;
		case NTV2_FORMAT_4x2048x1080psf_2400:	result = "4x2048x1080psf24";			break;
		case NTV2_FORMAT_4x2048x1080psf_2500:	result = "4x2048x1080psf25";			break;
		case NTV2_FORMAT_4x2048x1080psf_2997:	result = "4x2048x1080psf2997";			break;
		case NTV2_FORMAT_4x2048x1080psf_3000:	result = "4x2048x1080psf30";			break;
		case NTV2_FORMAT_4x2048x1080p_2398:		result = "4x2048x1080p2398";			break;
		case NTV2_FORMAT_4x2048x1080p_2400:		result = "4x2048x1080p24";				break;
		case NTV2_FORMAT_4x2048x1080p_2500:		result = "4x2048x1080p25";				break;
		case NTV2_FORMAT_4x2048x1080p_2997:		result = "4x2048x1080p2997";			break;
		case NTV2_FORMAT_4x2048x1080p_3000:		result = "4x2048x1080p30";				break;
		case NTV2_FORMAT_4x2048x1080p_4795:		result = "4x2048x1080p4795";			break;
		case NTV2_FORMAT_4x2048x1080p_4800:		result = "4x2048x1080p48";				break;
		case NTV2_FORMAT_4x1920x1080p_5000:		result = "4x1920x1080p50";				break;
		case NTV2_FORMAT_4x1920x1080p_5994:		result = "4x1920x1080p5994";			break;
		case NTV2_FORMAT_4x1920x1080p_6000:		result = "4x1920x1080p60";				break;
		case NTV2_FORMAT_4x2048x1080p_5000:		result = "4x2048x1080p50";				break;
		case NTV2_FORMAT_4x2048x1080p_5994:		result = "4x2048x1080p5994";			break;
		case NTV2_FORMAT_4x2048x1080p_6000:		result = "4x2048x1080p60";				break;
		case NTV2_FORMAT_4x2048x1080p_11988:	result = "4x2048x1080p11988";			break;
		case NTV2_FORMAT_4x2048x1080p_12000:	result = "4x2048x1080p120";				break;
		case NTV2_FORMAT_END_HIGH_DEF_FORMATS2:	result = "";							break;
		//case NTV2_FORMAT_END_4K_DEF_FORMATS:	result = "";							break;
		case NTV2_FORMAT_UNKNOWN:				result = "";							break;
	}
	
	return (result);
*/
}	//	NTV2VideoFormatToString


string NTV2StandardToString (const NTV2Standard inValue, const bool inForRetailDisplay)
{
	switch (inValue)
	{
		case NTV2_STANDARD_1080:	return inForRetailDisplay ? "1080i"	: "NTV2_STANDARD_1080";
		case NTV2_STANDARD_720:		return inForRetailDisplay ? "720p"	: "NTV2_STANDARD_720";
		case NTV2_STANDARD_525:		return inForRetailDisplay ? "525i"	: "NTV2_STANDARD_525";
		case NTV2_STANDARD_625:		return inForRetailDisplay ? "625i"	: "NTV2_STANDARD_625";
		case NTV2_STANDARD_1080p:	return inForRetailDisplay ? "1080p"	: "NTV2_STANDARD_1080p";
		case NTV2_STANDARD_2K:		return inForRetailDisplay ? "2K"	: "NTV2_STANDARD_2K";
		case NTV2_NUM_STANDARDS:	break;
	}
	return string ();
}


string NTV2FrameBufferFormatToString (const NTV2FrameBufferFormat inValue,	const bool inForRetailDisplay)
{
	if (inForRetailDisplay)
		return frameBufferFormats [inValue];	//	frameBufferFormatString (inValue);

	switch (inValue)
	{
		case NTV2_FBF_10BIT_YCBCR:				return "NTV2_FBF_10BIT_YCBCR";
		case NTV2_FBF_8BIT_YCBCR:				return "NTV2_FBF_8BIT_YCBCR";
		case NTV2_FBF_ARGB:						return "NTV2_FBF_ARGB";
		case NTV2_FBF_RGBA:						return "NTV2_FBF_RGBA";
		case NTV2_FBF_10BIT_RGB:				return "NTV2_FBF_10BIT_RGB";
		case NTV2_FBF_8BIT_YCBCR_YUY2:			return "NTV2_FBF_8BIT_YCBCR_YUY2";
		case NTV2_FBF_ABGR:						return "NTV2_FBF_ABGR";
		case NTV2_FBF_10BIT_DPX:				return "NTV2_FBF_10BIT_DPX";
		case NTV2_FBF_10BIT_YCBCR_DPX:			return "NTV2_FBF_10BIT_YCBCR_DPX";
		case NTV2_FBF_8BIT_DVCPRO:				return "NTV2_FBF_8BIT_DVCPRO";
		case NTV2_FBF_8BIT_QREZ:				return "NTV2_FBF_8BIT_QREZ";
		case NTV2_FBF_8BIT_HDV:					return "NTV2_FBF_8BIT_HDV";
		case NTV2_FBF_24BIT_RGB:				return "NTV2_FBF_24BIT_RGB";
		case NTV2_FBF_24BIT_BGR:				return "NTV2_FBF_24BIT_BGR";
		case NTV2_FBF_10BIT_YCBCRA:				return "NTV2_FBF_10BIT_YCBCRA";
		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:	return "NTV2_FBF_10BIT_DPX_LITTLEENDIAN";
		case NTV2_FBF_48BIT_RGB:				return "NTV2_FBF_48BIT_RGB";
		case NTV2_FBF_PRORES:					return "NTV2_FBF_PRORES";
		case NTV2_FBF_PRORES_DVCPRO:			return "NTV2_FBF_PRORES_DVCPRO";
		case NTV2_FBF_PRORES_HDV:				return "NTV2_FBF_PRORES_HDV";
		case NTV2_FBF_10BIT_RGB_PACKED:			return "NTV2_FBF_10BIT_RGB_PACKED";
		case NTV2_FBF_10BIT_ARGB:				return "NTV2_FBF_10BIT_ARGB";
		case NTV2_FBF_16BIT_ARGB:				return "NTV2_FBF_16BIT_ARGB";
		case NTV2_FBF_UNUSED_23:				return "NTV2_FBF_UNUSED_23";
		case NTV2_FBF_10BIT_RAW_RGB:			return "NTV2_FBF_10BIT_RGB";
		case NTV2_FBF_10BIT_RAW_YCBCR:			return "NTV2_FBF_10BIT_YCBCR";
		case NTV2_FBF_NUMFRAMEBUFFERFORMATS:	break;
	}
	return string ();
}


string NTV2FrameGeometryToString (const NTV2FrameGeometry inValue, const bool inForRetailDisplay)
{
	switch (inValue)
	{
		case NTV2_FG_1920x1080:				return inForRetailDisplay ? "1920x1080"		: "NTV2_FG_1920x1080";
		case NTV2_FG_1280x720:				return inForRetailDisplay ?	"1280x720"		: "NTV2_FG_1280x720";
		case NTV2_FG_720x486:				return inForRetailDisplay ?	"720x486"		: "NTV2_FG_720x486";
		case NTV2_FG_720x576:				return inForRetailDisplay ?	"720x576"		: "NTV2_FG_720x576";
		case NTV2_FG_1920x1114:				return inForRetailDisplay ?	"1920x1114"		: "NTV2_FG_1920x1114";
		case NTV2_FG_2048x1114:				return inForRetailDisplay ?	"2048x1114"		: "NTV2_FG_2048x1114";
		case NTV2_FG_720x508:				return inForRetailDisplay ?	"720x508"		: "NTV2_FG_720x508";
		case NTV2_FG_720x598:				return inForRetailDisplay ?	"720x598"		: "NTV2_FG_720x598";
		case NTV2_FG_1920x1112:				return inForRetailDisplay ?	"1920x1112"		: "NTV2_FG_1920x1112";
		case NTV2_FG_1280x740:				return inForRetailDisplay ?	"1280x740"		: "NTV2_FG_1280x740";
		case NTV2_FG_2048x1080:				return inForRetailDisplay ?	"2048x1080"		: "NTV2_FG_2048x1080";
		case NTV2_FG_2048x1556:				return inForRetailDisplay ?	"2048x1556"		: "NTV2_FG_2048x1556";
		case NTV2_FG_2048x1588:				return inForRetailDisplay ?	"2048x1588"		: "NTV2_FG_2048x1588";
		case NTV2_FG_2048x1112:				return inForRetailDisplay ?	"2048x1112"		: "NTV2_FG_2048x1112";
		case NTV2_FG_720x514:				return inForRetailDisplay ?	"720x514"		: "NTV2_FG_720x514";
		case NTV2_FG_720x612:				return inForRetailDisplay ?	"720x612"		: "NTV2_FG_720x612";
		case NTV2_FG_4x1920x1080:			return inForRetailDisplay ?	"4x1920x1080"	: "NTV2_FG_4x1920x1080";
		case NTV2_FG_4x2048x1080:			return inForRetailDisplay ?	"4x2048x1080"	: "NTV2_FG_4x2048x1080";
		case NTV2_FG_NUMFRAMEGEOMETRIES:	break;
	}
	return string ();
}


string NTV2FrameRateToString (const NTV2FrameRate inValue,	const bool inForRetailDisplay)
{
	if (inForRetailDisplay)
		return NTV2FrameRateStrings [inValue];
	switch (inValue)
	{
		case NTV2_FRAMERATE_UNKNOWN:	return "NTV2_FRAMERATE_UNKNOWN";
		case NTV2_FRAMERATE_6000:		return "NTV2_FRAMERATE_6000";
		case NTV2_FRAMERATE_5994:		return "NTV2_FRAMERATE_5994";
		case NTV2_FRAMERATE_3000:		return "NTV2_FRAMERATE_3000";
		case NTV2_FRAMERATE_2997:		return "NTV2_FRAMERATE_2997";
		case NTV2_FRAMERATE_2500:		return "NTV2_FRAMERATE_2500";
		case NTV2_FRAMERATE_2400:		return "NTV2_FRAMERATE_2400";
		case NTV2_FRAMERATE_2398:		return "NTV2_FRAMERATE_2398";
		case NTV2_FRAMERATE_5000:		return "NTV2_FRAMERATE_5000";
		case NTV2_FRAMERATE_4800:		return "NTV2_FRAMERATE_4800";
		case NTV2_FRAMERATE_4795:		return "NTV2_FRAMERATE_4795";
		case NTV2_FRAMERATE_12000:		return "NTV2_FRAMERATE_12000";
		case NTV2_FRAMERATE_11988:		return "NTV2_FRAMERATE_11988";
		case NTV2_FRAMERATE_1500:		return "NTV2_FRAMERATE_1500";
		case NTV2_FRAMERATE_1498:		return "NTV2_FRAMERATE_1498";
		case NTV2_FRAMERATE_1900:		return "NTV2_FRAMERATE_1900";
		case NTV2_FRAMERATE_1898:		return "NTV2_FRAMERATE_1898";
		case NTV2_FRAMERATE_1800:		return "NTV2_FRAMERATE_1800";
		case NTV2_FRAMERATE_1798:		return "NTV2_FRAMERATE_1798";
		case NTV2_NUM_FRAMERATES:		break;
	}
	return string ();
}


string NTV2InputSourceToString (const NTV2InputSource inValue,	const bool inForRetailDisplay)
{
	switch (inValue)
	{
		case NTV2_INPUTSOURCE_ANALOG:			return inForRetailDisplay ? "Analog"	: "NTV2_INPUTSOURCE_ANALOG";
		case NTV2_INPUTSOURCE_HDMI:				return inForRetailDisplay ? "HDMI"		: "NTV2_INPUTSOURCE_HDMI";
		case NTV2_INPUTSOURCE_SDI1:				return inForRetailDisplay ? "SDI1"		: "NTV2_INPUTSOURCE_SDI1";
		case NTV2_INPUTSOURCE_SDI2:				return inForRetailDisplay ? "SDI2"		: "NTV2_INPUTSOURCE_SDI2";
		case NTV2_INPUTSOURCE_SDI3:				return inForRetailDisplay ? "SDI3"		: "NTV2_INPUTSOURCE_SDI3";
		case NTV2_INPUTSOURCE_SDI4:				return inForRetailDisplay ? "SDI4"		: "NTV2_INPUTSOURCE_SDI4";
		case NTV2_INPUTSOURCE_SDI5:				return inForRetailDisplay ? "SDI5"		: "NTV2_INPUTSOURCE_SDI5";
		case NTV2_INPUTSOURCE_SDI6:				return inForRetailDisplay ? "SDI6"		: "NTV2_INPUTSOURCE_SDI6";
		case NTV2_INPUTSOURCE_SDI7:				return inForRetailDisplay ? "SDI7"		: "NTV2_INPUTSOURCE_SDI7";
		case NTV2_INPUTSOURCE_SDI8:				return inForRetailDisplay ? "SDI8"		: "NTV2_INPUTSOURCE_SDI8";
		#if !defined (NTV2_DEPRECATE)
			case NTV2_INPUTSOURCE_DUALLINK1:	return inForRetailDisplay ? "SDI1 DL"	: "NTV2_INPUTSOURCE_DUALLINK1";
			case NTV2_INPUTSOURCE_DUALLINK2:	return inForRetailDisplay ? "SDI2 DL"	: "NTV2_INPUTSOURCE_DUALLINK2";
			case NTV2_INPUTSOURCE_DUALLINK3:	return inForRetailDisplay ? "SDI3 DL"	: "NTV2_INPUTSOURCE_DUALLINK3";
			case NTV2_INPUTSOURCE_DUALLINK4:	return inForRetailDisplay ? "SDI4 DL"	: "NTV2_INPUTSOURCE_DUALLINK4";
			case NTV2_INPUTSOURCE_SDI1_DS2:		return inForRetailDisplay ? "SDI1 DS2"	: "NTV2_INPUTSOURCE_SDI1_DS2";
			case NTV2_INPUTSOURCE_SDI2_DS2:		return inForRetailDisplay ? "SDI2 DS2"	: "NTV2_INPUTSOURCE_SDI2_DS2";
			case NTV2_INPUTSOURCE_SDI3_DS2:		return inForRetailDisplay ? "SDI3 DS2"	: "NTV2_INPUTSOURCE_SDI3_DS2";
			case NTV2_INPUTSOURCE_SDI4_DS2:		return inForRetailDisplay ? "SDI4 DS2"	: "NTV2_INPUTSOURCE_SDI4_DS2";
			case NTV2_INPUTSOURCE_SDI5_DS2:		return inForRetailDisplay ? "SDI5 DS2"	: "NTV2_INPUTSOURCE_SDI5_DS2";
			case NTV2_INPUTSOURCE_SDI6_DS2:		return inForRetailDisplay ? "SDI6 DS2"	: "NTV2_INPUTSOURCE_SDI6_DS2";
			case NTV2_INPUTSOURCE_SDI7_DS2:		return inForRetailDisplay ? "SDI7 DS2"	: "NTV2_INPUTSOURCE_SDI7_DS2";
			case NTV2_INPUTSOURCE_SDI8_DS2:		return inForRetailDisplay ? "SDI8 DS2"	: "NTV2_INPUTSOURCE_SDI8_DS2";
			case NTV2_INPUTSOURCE_DUALLINK5:	return inForRetailDisplay ? "SDI5 DL"	: "NTV2_INPUTSOURCE_DUALLINK5";
			case NTV2_INPUTSOURCE_DUALLINK6:	return inForRetailDisplay ? "SDI6 DL"	: "NTV2_INPUTSOURCE_DUALLINK6";
			case NTV2_INPUTSOURCE_DUALLINK7:	return inForRetailDisplay ? "SDI7 DL"	: "NTV2_INPUTSOURCE_DUALLINK7";
			case NTV2_INPUTSOURCE_DUALLINK8:	return inForRetailDisplay ? "SDI8 DL"	: "NTV2_INPUTSOURCE_DUALLINK8";
		#endif	//	!defined (NTV2_DEPRECATE)
		case NTV2_NUM_INPUTSOURCES:				break;
	}
	return string ();
}


string NTV2OutputDestinationToString (const NTV2OutputDestination inValue, const bool inForRetailDisplay)
{
	switch (inValue)
	{
		case NTV2_OUTPUTDESTINATION_ANALOG:			return inForRetailDisplay ? "Analog"	: "NTV2_OUTPUTDESTINATION_ANALOG";
		case NTV2_OUTPUTDESTINATION_HDMI:			return inForRetailDisplay ? "HDMI"		: "NTV2_OUTPUTDESTINATION_HDMI";
		case NTV2_OUTPUTDESTINATION_SDI1:			return inForRetailDisplay ? "SDI 1"		: "NTV2_OUTPUTDESTINATION_SDI1";
		case NTV2_OUTPUTDESTINATION_SDI2:			return inForRetailDisplay ? "SDI 2"		: "NTV2_OUTPUTDESTINATION_SDI2";
		case NTV2_OUTPUTDESTINATION_SDI3:			return inForRetailDisplay ? "SDI3"		: "NTV2_OUTPUTDESTINATION_SDI3";
		case NTV2_OUTPUTDESTINATION_SDI4:			return inForRetailDisplay ? "SDI4"		: "NTV2_OUTPUTDESTINATION_SDI4";
		case NTV2_OUTPUTDESTINATION_SDI5:			return inForRetailDisplay ? "SDI5"		: "NTV2_OUTPUTDESTINATION_SDI5";
		case NTV2_OUTPUTDESTINATION_SDI6:			return inForRetailDisplay ? "SDI6"		: "NTV2_OUTPUTDESTINATION_SDI6";
		case NTV2_OUTPUTDESTINATION_SDI7:			return inForRetailDisplay ? "SDI7"		: "NTV2_OUTPUTDESTINATION_SDI7";
		case NTV2_OUTPUTDESTINATION_SDI8:			return inForRetailDisplay ? "SDI8"		: "NTV2_OUTPUTDESTINATION_SDI8";
		#if !defined (NTV2_DEPRECATE)
			case NTV2_OUTPUTDESTINATION_HDMI_14:	return inForRetailDisplay ? "HDMI 1.4"	: "NTV2_OUTPUTDESTINATION_HDMI_14";
			case NTV2_OUTPUTDESTINATION_DUALLINK1:	return inForRetailDisplay ? "SDI1 DL"	: "NTV2_OUTPUTDESTINATION_DUALLINK1";
			case NTV2_OUTPUTDESTINATION_DUALLINK2:	return inForRetailDisplay ? "SDI2 DL"	: "NTV2_OUTPUTDESTINATION_DUALLINK2";
			case NTV2_OUTPUTDESTINATION_DUALLINK3:	return inForRetailDisplay ? "SDI3 DL"	: "NTV2_OUTPUTDESTINATION_DUALLINK3";
			case NTV2_OUTPUTDESTINATION_DUALLINK4:	return inForRetailDisplay ? "SDI4 DL"	: "NTV2_OUTPUTDESTINATION_DUALLINK4";
			case NTV2_OUTPUTDESTINATION_DUALLINK5:	return inForRetailDisplay ? "SDI5 DL"	: "NTV2_OUTPUTDESTINATION_DUALLINK5";
			case NTV2_OUTPUTDESTINATION_DUALLINK6:	return inForRetailDisplay ? "SDI6 DL"	: "NTV2_OUTPUTDESTINATION_DUALLINK6";
			case NTV2_OUTPUTDESTINATION_DUALLINK7:	return inForRetailDisplay ? "SDI7 DL"	: "NTV2_OUTPUTDESTINATION_DUALLINK7";
			case NTV2_OUTPUTDESTINATION_DUALLINK8:	return inForRetailDisplay ? "SDI8 DL"	: "NTV2_OUTPUTDESTINATION_DUALLINK8";
		#endif	//	!defined (NTV2_DEPRECATE)
		case NTV2_NUM_OUTPUTDESTINATIONS:			break;
	}
	return string ();
}


string NTV2ReferenceSourceToString (const NTV2ReferenceSource inValue, const bool inForRetailDisplay)
{
	switch (inValue)
	{
		case NTV2_REFERENCE_EXTERNAL:		return inForRetailDisplay ? "Reference In"	: "NTV2_REFERENCE_EXTERNAL";
		case NTV2_REFERENCE_INPUT1:			return inForRetailDisplay ? "Input 1"		: "NTV2_REFERENCE_INPUT1";
		case NTV2_REFERENCE_INPUT2:			return inForRetailDisplay ? "Input 2"		: "NTV2_REFERENCE_INPUT2";
		case NTV2_REFERENCE_FREERUN:		return inForRetailDisplay ? "Free Run"		: "NTV2_REFERENCE_FREERUN";
		case NTV2_REFERENCE_ANALOG_INPUT:	return inForRetailDisplay ? "Analog In"		: "NTV2_REFERENCE_ANALOG_INPUT";
		case NTV2_REFERENCE_HDMI_INPUT:		return inForRetailDisplay ? "HDMI In"		: "NTV2_REFERENCE_HDMI_INPUT";
		case NTV2_REFERENCE_INPUT3:			return inForRetailDisplay ? "Input 3"		: "NTV2_REFERENCE_INPUT3";
		case NTV2_REFERENCE_INPUT4:			return inForRetailDisplay ? "Input 4"		: "NTV2_REFERENCE_INPUT4";
		case NTV2_REFERENCE_INPUT5:			return inForRetailDisplay ? "Input 5"		: "NTV2_REFERENCE_INPUT5";
		case NTV2_REFERENCE_INPUT6:			return inForRetailDisplay ? "Input 6"		: "NTV2_REFERENCE_INPUT6";
		case NTV2_REFERENCE_INPUT7:			return inForRetailDisplay ? "Input 7"		: "NTV2_REFERENCE_INPUT7";
		case NTV2_REFERENCE_INPUT8:			return inForRetailDisplay ? "Input 8"		: "NTV2_REFERENCE_INPUT8";
		case NTV2_NUM_REFERENCE_INPUTS:		break;
	}
	return string ();
}



string NTV2RegisterWriteModeToString (const NTV2RegisterWriteMode inValue, const bool inForRetailDisplay)
{
	switch (inValue)
	{
		case NTV2_REGWRITE_SYNCTOFIELD:		return inForRetailDisplay ? "Sync To Field"	: "NTV2_REGWRITE_SYNCTOFIELD";
		case NTV2_REGWRITE_SYNCTOFRAME:		return inForRetailDisplay ? "Sync To Frame" : "NTV2_REGWRITE_SYNCTOFRAME";
		case NTV2_REGWRITE_IMMEDIATE:		return inForRetailDisplay ? "Immediate"		: "NTV2_REGWRITE_IMMEDIATE";
		case NTV2_REGWRITE_SYNCTOFIELD_AFTER10LINES:	break;
	}
	return string ();
}


static const char * NTV2InterruptEnumStrings (const INTERRUPT_ENUMS inInterruptEnumValue)
{
	static const char *	sInterruptEnumStrings []	= {	"eOutput1",
														"eInterruptMask",
														"eInput1",
														"eInput2",
														"eAudio",
														"eAudioInWrap",
														"eAudioOutWrap",
														"eDMA1",
														"eDMA2",
														"eDMA3",
														"eDMA4",
														"eChangeEvent",
														"eGetIntCount",
														"eWrapRate",
														"eUart1Tx",
														"eUart1Rx",
														"eAuxVerticalInterrupt",
														"ePushButtonChange",
														"eLowPower",
														"eDisplayFIFO",
														"eSATAChange",
														"eTemp1High",
														"eTemp2High",
														"ePowerButtonChange",
														"eInput3",
														"eInput4",
														"eUart2Tx",
														"eUart2Rx",
														"eHDMIRxV2HotplugDetect",
														"eInput5",
														"eInput6",
														"eInput7",
														"eInput8",
														"eInterruptMask2",
														"eOutput2",
														"eOutput3",
														"eOutput4",
														"eOutput5",
														"eOutput6",
														"eOutput7",
														"eOutput8",
														"<invalid>",
														"<invalid>",
														"<invalid>"};
	if (inInterruptEnumValue >= eOutput1 && inInterruptEnumValue <= eNumInterruptTypes)
		return sInterruptEnumStrings [inInterruptEnumValue];
	else
		return NULL;

}	//	NTV2InterruptEnumStrings


std::string NTV2InterruptEnumToString (const INTERRUPT_ENUMS inInterruptEnumValue)
{
	const char *	pString	(::NTV2InterruptEnumStrings (inInterruptEnumValue));
	return std::string (pString ? pString : "");
}


ostream & operator << (ostream & inOutStream, const RP188_STRUCT & inObj)
{
	return inOutStream	<< "RP188 DBB=0x" << hex << setw (8) << setfill ('0') << inObj.DBB
						<< ", HI=0x" << hex << setw (8) << setfill ('0') << inObj.High
						<< ", LO=" << hex << setw (8) << setfill ('0') << inObj.Low
						<< dec;
}	//	RP188_STRUCT ostream operator


string NTV2GetBitfileName (const NTV2DeviceID inBoardID)
{
	#if defined (MSWindows)
		switch (inBoardID)
		{
			case DEVICE_ID_NOTFOUND:	break;
			case DEVICE_ID_CORVID1:		return "corvid1_pcie.bit";
			case DEVICE_ID_CORVID22:	return "corvid22_pcie.bit";
			case DEVICE_ID_CORVID24:	return "corvid24_pcie.bit";
			case DEVICE_ID_CORVID3G:	return "corvid3G_pcie.bit";
			case DEVICE_ID_CORVID44:	return "corvid_44.bit";
			case DEVICE_ID_CORVID88:	return "corvid_88.bit";
			case DEVICE_ID_IO4K:		return "io4k_pcie.bit";
			case DEVICE_ID_IO4KUFC:		return "io4k_ufc_pcie.bit";
			case DEVICE_ID_IOEXPRESS:	return "ioexpress_pcie.bit";
			case DEVICE_ID_IOXT:		return "ioxt_pcie.bit";
			case DEVICE_ID_KONA3G:		return "kona3g_pcie.bit";
			case DEVICE_ID_KONA3GQUAD:	return "kona3g_quad_pcie.bit";
			case DEVICE_ID_KONA4:		return "kona4_pcie.bit";
			case DEVICE_ID_KONA4UFC:	return "kona4_ufc_pcie.bit";
			case DEVICE_ID_LHE_PLUS:	return "lheplus_pcie.bit";
			case DEVICE_ID_LHI:			return "lhi_pcie.bit";
			case DEVICE_ID_TTAP:		return "ttap_pcie.bit";
			default:					return "";
		}
	#else
		switch (inBoardID)
		{
		#if !defined (NTV2_DEPRECATE)
			case BOARD_ID_XENA_SD:
			case BOARD_ID_XENA_SD22:
			case BOARD_ID_XENA_HD:
			case BOARD_ID_XENA_HD22:
			case BOARD_ID_HDNTV2:
			case BOARD_ID_KSD11:
			//case BOARD_ID_XENA_SD_MM:
			case BOARD_ID_KSD22:
			//case BOARD_ID_XENA_SD22_MM:
			case BOARD_ID_KHD11:
			//case BOARD_ID_XENA_HD_MM:
			case BOARD_ID_XENA_HD22_MM:
			case BOARD_ID_HDNTV2_MM:
			case BOARD_ID_KONA_SD:
			case BOARD_ID_KONA_HD:
			case BOARD_ID_KONA_HD2:
			case BOARD_ID_KONAR:
			case BOARD_ID_KONAR_MM:
			case BOARD_ID_KONA2:
			case BOARD_ID_HDNTV:
			case BOARD_ID_KONALS:
			//case BOARD_ID_XENALS:
			case BOARD_ID_KONAHDS:
			//case BOARD_ID_KONALH:
			//case BOARD_ID_XENALH:
			case BOARD_ID_XENADXT:
			//case BOARD_ID_XENAHS:
			case BOARD_ID_KONAX:
			case BOARD_ID_XENAX:
			case BOARD_ID_XENAHS2:
			case BOARD_ID_FS1:
			case BOARD_ID_FS2:
			case BOARD_ID_MOAB:
			case BOARD_ID_XENAX2:
			case BOARD_ID_BORG:
			case BOARD_ID_BONES:
			case BOARD_ID_BARCLAY:
			case BOARD_ID_KIPRO_QUAD:
			case BOARD_ID_KIPRO_SPARE1:
			case BOARD_ID_KIPRO_SPARE2:
			case BOARD_ID_FORGE:
			case BOARD_ID_XENA2:
			//case BOARD_ID_KONA3:
			case BOARD_ID_LHI_DVI:
			case BOARD_ID_LHI_T:
		#endif	//	!defined (NTV2_DEPRECATE)
			case DEVICE_ID_NOTFOUND:	break;
			case DEVICE_ID_CORVID1:		return "corvid1pcie.bit";
			case DEVICE_ID_CORVID22:	return "Corvid22.bit";
			case DEVICE_ID_CORVID24:	return "corvid24_quad.bit";
			case DEVICE_ID_CORVID3G:	return "corvid1_3gpcie.bit";
			case DEVICE_ID_CORVID44:	return "corvid_44.bit";
			case DEVICE_ID_CORVID88:	return "corvid_88.bit";
			case DEVICE_ID_IO4K:		return "IO_XT_4K.bit";
			case DEVICE_ID_IO4KUFC:		return "IO_XT_4K_UFC.bit";
			case DEVICE_ID_IOEXPRESS:	return "chekov_00_pcie.bit";
			case DEVICE_ID_IOXT:		return "top_io_tx.bit";
			case DEVICE_ID_KONA3G:		return "k3g_top.bit";
			case DEVICE_ID_KONA3GQUAD:	return "k3g_quad.bit";
			case DEVICE_ID_KONA4:		return "kona_4_quad.bit";
			case DEVICE_ID_KONA4UFC:	return "kona_4_ufc.bit";
			case DEVICE_ID_LHE_PLUS:	return "lhe_12_pcie.bit";
			case DEVICE_ID_LHI:			return "top_pike.bit";
			case DEVICE_ID_TTAP:		return "t_tap_top.bit";
			default:					return "";
		}
	#endif
	return "";
}	//	NTV2GetBitfileName


bool NTV2IsCompatibleBitfileName (const string & inBitfileName, const NTV2DeviceID inDeviceID)
{
	const string	deviceBitfileName	(::NTV2GetBitfileName (inDeviceID));
	if (inBitfileName == deviceBitfileName)
		return true;

	switch (inDeviceID)
	{
		case DEVICE_ID_KONA3GQUAD:	return ::NTV2GetBitfileName (DEVICE_ID_KONA3G) == inBitfileName;
		case DEVICE_ID_KONA3G:		return ::NTV2GetBitfileName (DEVICE_ID_KONA3GQUAD) == inBitfileName;

		case DEVICE_ID_KONA4:		return ::NTV2GetBitfileName (DEVICE_ID_KONA4UFC) == inBitfileName;
		case DEVICE_ID_KONA4UFC:	return ::NTV2GetBitfileName (DEVICE_ID_KONA4) == inBitfileName;

		case DEVICE_ID_IO4K:		return ::NTV2GetBitfileName (DEVICE_ID_IO4KUFC) == inBitfileName;
		case DEVICE_ID_IO4KUFC:		return ::NTV2GetBitfileName (DEVICE_ID_IO4K) == inBitfileName;

		default:					break;
	}
	return false;

}	//	IsCompatibleBitfileName


NTV2DeviceID NTV2GetDeviceIDFromBitfileName (const string & inBitfileName)
{
	typedef map <string, NTV2DeviceID>	BitfileName2DeviceID;
	static BitfileName2DeviceID			sBitfileName2DeviceID;
	if (sBitfileName2DeviceID.empty ())
	{
		static	NTV2DeviceID	sDeviceIDs [] =	{	DEVICE_ID_KONA3GQUAD,	DEVICE_ID_KONA3G,	DEVICE_ID_KONA4,		DEVICE_ID_KONA4UFC,	DEVICE_ID_LHI,
													DEVICE_ID_LHE_PLUS,		DEVICE_ID_TTAP,		DEVICE_ID_CORVID1,		DEVICE_ID_CORVID22,	DEVICE_ID_CORVID24,
													DEVICE_ID_CORVID3G,		DEVICE_ID_IOXT,		DEVICE_ID_IOEXPRESS,	DEVICE_ID_IO4K,		DEVICE_ID_IO4KUFC,
													DEVICE_ID_NOTFOUND };
		for (unsigned ndx (0);  ndx < sizeof (sDeviceIDs) / sizeof (NTV2DeviceID);  ndx++)
			sBitfileName2DeviceID [::NTV2GetBitfileName (sDeviceIDs [ndx])] = sDeviceIDs [ndx];
	}
	return sBitfileName2DeviceID [inBitfileName];
}


string NTV2GetFirmwareFolderPath (void)
{
	#if defined (AJAMac)
		return "/Library/Application Support/AJA/Firmware";
	#elif defined (MSWindows)
		HKEY	hKey		(NULL);
		DWORD	bufferSize	(1024);
		char *	lpData		(new char [bufferSize]);

		if (RegOpenKeyExA (HKEY_CURRENT_USER, "Software\\AJA", NULL, KEY_READ, &hKey) == ERROR_SUCCESS
			&& RegQueryValueExA (hKey, "firmwarePath", NULL, NULL, (LPBYTE) lpData, &bufferSize) == ERROR_SUCCESS)
				return string (lpData);
		RegCloseKey (hKey);
		return "";
	#else
		return "";
	#endif
}


NTV2DeviceIDSet NTV2GetSupportedDevices (void)
{
	static const NTV2DeviceID	sValidDeviceIDs []	= {	DEVICE_ID_CORVID1,
														DEVICE_ID_CORVID22,
														DEVICE_ID_CORVID24,
														DEVICE_ID_CORVID3G,
														DEVICE_ID_CORVID44,
														DEVICE_ID_CORVID88,
														DEVICE_ID_IO4K,
														DEVICE_ID_IO4KUFC,
														DEVICE_ID_IOEXPRESS,
														DEVICE_ID_IOXT,
														DEVICE_ID_KONA3G,
														DEVICE_ID_KONA3GQUAD,
														DEVICE_ID_KONA4,
														DEVICE_ID_KONA4UFC,
														DEVICE_ID_LHE_PLUS,
														DEVICE_ID_LHI,
														DEVICE_ID_TTAP,
														DEVICE_ID_NOTFOUND	};
	NTV2DeviceIDSet	result;
	for (unsigned ndx (0);  ndx < sizeof (sValidDeviceIDs) / sizeof (NTV2DeviceID);  ndx++)
		if (sValidDeviceIDs [ndx] != DEVICE_ID_NOTFOUND)
			result.insert (sValidDeviceIDs [ndx]);
	return result;
}


std::ostream & operator << (std::ostream & inOutStr, const NTV2DeviceIDSet & inSet)
{
	for (NTV2DeviceIDSetConstIter iter (inSet.begin ());  iter != inSet.end ();  ++iter)
		inOutStr << (iter != inSet.begin () ? ", " : "") << ::NTV2DeviceIDToString (*iter);
	return inOutStr;
}


std::string NTV2GetVersionString (const bool inDetailed)
{
	ostringstream	oss;

	oss << AJA_NTV2_SDK_VERSION_MAJOR << "." << AJA_NTV2_SDK_VERSION_MINOR << "." << AJA_NTV2_SDK_VERSION_POINT;
	if (!string (AJA_NTV2_SDK_BUILD_TYPE).empty ())
		oss << " " << AJA_NTV2_SDK_BUILD_TYPE << AJA_NTV2_SDK_BUILD_NUMBER;
	#if defined (NTV2_DEPRECATE)
		if (inDetailed)
			oss << " (NTV2_DEPRECATE)";
	#endif
	if (inDetailed)
		oss	<< " built on " << AJA_NTV2_SDK_BUILD_DATETIME;
	return oss.str ();
}
