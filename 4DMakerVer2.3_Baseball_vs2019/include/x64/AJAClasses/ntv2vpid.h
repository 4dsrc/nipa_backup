//
//
// ntv2vpid.h
//
//	see SMPTE 352
//
//	Copyright (C) 2012 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
//

#ifndef NTV2VPID_H
#define NTV2VPID_H

#include "ajaexport.h"
#include "ntv2publicinterface.h"

#if defined(AJALinux)
#include <stdio.h>
#include "ntv2winlinhacks.h"
#endif

class AJAExport CNTV2VPID
{
public:

	// constructors
	CNTV2VPID();
	CNTV2VPID(const CNTV2VPID& other);
	virtual ~CNTV2VPID();

	// initialize the VPID data
	void Init();

	void SetVPID(ULWord data);
	ULWord GetVPID();

	bool SetVPID(NTV2VideoFormat videoFormat,
		NTV2FrameBufferFormat frameBufferFormat,
		bool progressive,
		bool aspect16x9,
		VPIDChannel channel);

	bool SetVPID(NTV2VideoFormat outputFormat,
				 bool progressive,
				 bool aspect16x9,
				 bool dualLink,
				 bool RGB48Bit,
				 bool output3Gb,
				 VPIDChannel channel);

	void SetVersion(VPIDVersion version);
	VPIDVersion GetVersion();

	void SetStandard(VPIDStandard standard);
	VPIDStandard GetStandard();

	void SetProgressiveTransport(bool pt);
	bool GetProgressiveTransport();

	void SetProgressivePicture(bool pp);
	bool GetProgressivePicture();

	void SetPictureRate(VPIDPictureRate rate);
	VPIDPictureRate GetPictureRate();

	void SetImageAspect16x9(bool aspect16x9);
	bool GetImageAspect16x9();

	void SetSampling(VPIDSampling sampling);
	VPIDSampling GetSampling();

	void SetChannel(VPIDChannel channel);
	VPIDChannel GetChannel();

	void SetDynamicRange(VPIDDynamicRange range);
	VPIDDynamicRange GetDynamicRange();

	void SetBitDepth(VPIDBitDepth depth);
	VPIDBitDepth GetBitDepth();

	static bool SetVPIDData(ULWord* pVPID,
		NTV2VideoFormat outputFormat,
		NTV2FrameBufferFormat frameBufferFormat,
		bool progressive,
		bool aspect16x9,
		VPIDChannel channel);

	static bool SetVPIDData(ULWord* pVPID,
		NTV2VideoFormat outputFormat,
		bool progressive,
		bool aspect16x9,
		bool dualLinkRGB,
		bool format48Bit,
		bool output3Gb,
		VPIDChannel channel);

protected:
	
	ULWord m_uVPID;
};

#endif
