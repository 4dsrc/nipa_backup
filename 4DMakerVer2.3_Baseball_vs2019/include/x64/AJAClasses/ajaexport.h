//
//	ajaexport.h
//
//	Copyright (C) 2008 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
//	Define import/export and library for sdk dll
//

#ifndef AJAEXPORT_H
#define AJAEXPORT_H

#ifdef MSWindows
	#ifndef AJASTATIC
		#ifdef AJADLL
			#pragma warning (disable : 4251)
			#ifdef AJADLL_BUILD
				#define AJAExport __declspec(dllexport)
			#else
				#define AJAExport __declspec(dllimport)
			#endif
		#else
			#define AJAExport
			#ifndef AJA_NO_AUTOIMPORT
			#endif
		#endif
	#else
		#define AJAExport
	#endif
#else
	#define AJAExport
#endif

#endif
