/////////////////////////////////////////////////////////////////////////////
// NTV2TestPattern.cpp 
//
//

#include "ntv2testpattern.h"
#include "testpatterndata.h"
#include "ntv2utils.h"
#include "transcode.h"
#include "resample.h"
#include "ntv2devicefeatures.h"
#include "ntv2signalrouter.h"
#ifdef MSWindows
#include "winbase.h"
#endif
#include "ntv2nubtypes.h"
#include "ntv2nubaccess.h"
#include "math.h"


CNTV2TestPattern::CNTV2TestPattern(	UWord boardNumber,
	  								bool displayErrorMessage, 
									UWord dwCardTypes,
									bool autoRouteOnXena2, 
									const char hostname[],
									NTV2Channel channel)
:   CNTV2Status(boardNumber, displayErrorMessage,dwCardTypes, hostname) ,_autoRouteOnXena2(autoRouteOnXena2)
{
	_channel = channel;
	Init();
	
}

CNTV2TestPattern::CNTV2TestPattern()

{
	_autoRouteOnXena2 = false;
	Init();
}

void CNTV2TestPattern::Init()
{
	//_channel = NTV2_CHANNEL1;
	for ( int tpCount = 0; tpCount < numSegmentTestPatterns; tpCount++ )
	{
		_testPatternList.push_back(NTV2TestPatternSegments[tpCount].name);
	}
	_testPatternList.push_back("Black");
	_testPatternList.push_back("Border");
	_testPatternList.push_back("Slant Ramp");
	_testPatternList.push_back("Vertical Sweep");
	_testPatternList.push_back("Zone Plate");
	_signalMask = NTV2_SIGNALMASK_ALL;
	_fbFormat = NTV2_FBF_10BIT_YCBCR;
	_dualLinkOutputEnable = false;
	_bDMAtoBoard = true;
	_flipFlopPage = true;
	_clientDownloadBuffer = NULL;
}

CNTV2TestPattern::~CNTV2TestPattern()
{
	// board closed in CNTV2Card destructor
}


void CNTV2TestPattern::SetChannel( NTV2Channel channel)
{	
	_channel = channel;
}

NTV2Channel CNTV2TestPattern::GetChannel()
{
	return	_channel;
}

void CNTV2TestPattern::SetSignalMask( UWord signalMask )
{
	_signalMask = signalMask&0x7;
}

UWord CNTV2TestPattern::GetSignalMask()
{
	return _signalMask;
}

void CNTV2TestPattern::SetTestPatternFrameBufferFormat( NTV2FrameBufferFormat fbFormat )
{
	_fbFormat = fbFormat;
}

NTV2FrameBufferFormat CNTV2TestPattern::GetTestPatternFrameBufferFormat()
{
	return _fbFormat;
}


void CNTV2TestPattern::SetDualLinkTestPatternOutputEnable(bool enable)
{
	_dualLinkOutputEnable = enable;
}
bool CNTV2TestPattern::GetDualLinkTestPatternOutputEnable()
{
	return _dualLinkOutputEnable;
}

TestPatternList& CNTV2TestPattern::GetTestPatternList()
{
	return _testPatternList;
}

bool CNTV2TestPattern::DownloadTestPattern(UWord testPatternNumber )
{
	if (_remoteHandle != INVALID_NUB_HANDLE)
	{
		if (!NTV2DownloadTestPatternRemote(	
					_sockfd,
					_remoteHandle,
					_nubProtocolVersion,
					GetChannel(),
					GetTestPatternFrameBufferFormat(),
					GetSignalMask(),
					GetTestPatternDMAEnable(),
					testPatternNumber))
		{
			DisplayNTV2Error("NTV2DownloadTestPatternRemote failed");
			return false;
		}
		return true;
	}

	if ( testPatternNumber >= numSegmentTestPatterns )
	{
		switch ( testPatternNumber - numSegmentTestPatterns )
		{
		case 0:
			// Black
			DownloadBlackTestPattern();
			break;
		case 1:
			// Border
			DownloadBorderTestPattern();
			break;
		case 2:
			// Slant Ramp
			DownloadSlantRampTestPattern();
			break;
		case 3:
			// Vertical Sweep
			DownloadVerticalSweepTestPattern();
			break;
		case 4:
			// Zone Plate
			DownloadZonePlateTestPattern();
			break;
		}
	}
	else
	{
		DownloadSegmentedTestPattern(&NTV2TestPatternSegments[testPatternNumber]);
	}
	return true;
}

void CNTV2TestPattern::DownloadTestPattern(char* testPatternName )
{
	TestPatternList::iterator tpIter;
	UWord testPatternNumber = 0;
	for ( tpIter = _testPatternList.begin(); tpIter != _testPatternList.end(); tpIter++ )
	{

		if ( strcmp(*tpIter,testPatternName) == 0)	
		{
			DownloadTestPattern(testPatternNumber);
			break;
		}
		testPatternNumber++;
	}


}

void CNTV2TestPattern::LocalLoadBarsTestPattern( UWord testPatternNumber, NTV2Standard standard)
{
	SegmentTestPatternData *pTestPatternSegmentData = &NTV2TestPatternSegments[testPatternNumber];

	/*This is mostly excerpted from DownloadSegmentedTestPattern().
	*In general many ops in CNTV2TestPattern use hw register reads to manage local state. e.g. GetStandard().
	*This caused problems when InConverter is involved, etc. while also using test pattern.
	*So here, for this one narrow use case, set up is local context without reads from ntvcard.
	*/

	ULWord* baseAddress=0;
	ULWord  packedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*4]; // extra room
	UWord   unPackedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*2];
	::Make10BitBlackLine(unPackedBuffer,HD_NUMCOMPONENTPIXELS_1080_2K);
	::PackLine_16BitYUVto10BitYUV(unPackedBuffer, packedBuffer,HD_NUMCOMPONENTPIXELS_1080_2K);

	bool twoKby1080 = false;
	bool vancEnabled =  false;
	bool wideVANCSet = false;
	NTV2FrameBufferFormat 	fbFormat = NTV2_FBF_10BIT_YCBCR; //always NTV2_FBF_10BIT_YCBCR				= 0,

	//GetEnableVANCData(&vancEnabled,&wideVANCSet);
	NTV2FormatDescriptor formatDescriptor = GetFormatDescriptor(standard, fbFormat,vancEnabled,twoKby1080,wideVANCSet);

	ULWord numPixels = formatDescriptor.numPixels;
	ULWord linePitch = formatDescriptor.linePitch;
	//ULWord numLines = formatDescriptor.numLines;
	ULWord firstActiveLine = formatDescriptor.firstActiveLine;
	ULWord numOutputPixels = formatDescriptor.numPixels;
	ULWord dataLinePitch;

	if ( standard == NTV2_STANDARD_1080p)
		standard = NTV2_STANDARD_1080;    // no diffence for test patterns.
	switch(standard)
	{
	case NTV2_STANDARD_1080:
		dataLinePitch = HD_YCBCRLINEPITCH_1080;
		break;
	case NTV2_STANDARD_720:
		dataLinePitch = HD_YCBCRLINEPITCH_720;
		break;
	case NTV2_STANDARD_525:
		dataLinePitch = YCBCRLINEPITCH_SD;
		break;
	case NTV2_STANDARD_625:
		dataLinePitch = YCBCRLINEPITCH_SD;
		break;
	case NTV2_STANDARD_2K:
		//dataLinePitch = HD_YCBCRLINEPITCH_2K;
		//break;
	default:
		DisplayNTV2Error("Signal Generator Unsupported video standard format.\n");
		return;
		break;
	}

	if ( numOutputPixels == 0 || numPixels == 0 || linePitch == 0)
	{
		DisplayNTV2Error("Signal Generator Unsupported video standard/framebuffer format.\n");
		return;

	}
	// determine where we're going to render the frame to
	//ULWord *hostBuff = NULL;
	ULWord *currentAddress;
	//if (_clientDownloadBuffer)
	//{
		currentAddress = _clientDownloadBuffer;
		baseAddress = currentAddress;
	//}

	// Assume 1080 format
	// Start with Black.
	Make10BitBlackLine(unPackedBuffer,HD_NUMCOMPONENTPIXELS_1080_2K);
	ULWord line = 0;
	for ( ;line < firstActiveLine; line++)
	{
		// convert line to current frame buffer format
		ConvertLinePixelFormat(unPackedBuffer, packedBuffer, numPixels);

		// copy to formatted line to the frame buffer
		CopyMemory(currentAddress,packedBuffer,linePitch*4);
		currentAddress += linePitch;

	}

	for ( UWord segmentCount = 0; segmentCount < NumTestPatternSegments; segmentCount++ )
	{

		SegmentDescriptor* segmentDescriptor = &pTestPatternSegmentData->segmentDescriptor[standard][segmentCount];
		ULWord* data = segmentDescriptor->data;
		if ( data != NULL )
		{
			UWord startLine = segmentDescriptor->startLine;
			UWord numLines = (segmentDescriptor->endLine - startLine) + 1;
			currentAddress = baseAddress + ((firstActiveLine+startLine)*linePitch);
			// go through hoops to mask out undesired components
			memcpy(packedBuffer,data,dataLinePitch*4);
			// We are using the 1920 standard test patterns for 2k
			if (numPixels == HD_NUMCOMPONENTPIXELS_2K)
				::UnpackLine_10BitYUVto16BitYUV(packedBuffer, unPackedBuffer, HD_NUMCOMPONENTPIXELS_1080);
			else
				::UnpackLine_10BitYUVto16BitYUV(packedBuffer, unPackedBuffer, numPixels);

			if ( _signalMask != NTV2_SIGNALMASK_ALL)
				MaskYCbCrLine(unPackedBuffer, _signalMask , numPixels);

            bool  bIsSD;
            IsSDStandard(&bIsSD);
			switch(_fbFormat)
			{
	            case NTV2_FBF_10BIT_YCBCR:
		            ::PackLine_16BitYUVto10BitYUV(unPackedBuffer, packedBuffer,numPixels);
                    break;
				default:
					DisplayNTV2Error("ERROR: Signal Generator not set up for this format.\n");
					break;
			}

			for ( UWord lineCount = 0; lineCount < numLines; lineCount++ )
			{
				CopyMemory(currentAddress,packedBuffer,linePitch*4);
				currentAddress += linePitch;
			}

		}
	}

}

void CNTV2TestPattern::DownloadSegmentedTestPattern(SegmentTestPatternData* pTestPatternSegmentData )
{
	ULWord* baseAddress=0;
	ULWord  packedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*4]; // extra room
	UWord   unPackedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*2];
	::Make10BitBlackLine(unPackedBuffer,HD_NUMCOMPONENTPIXELS_1080_2K);
	::PackLine_16BitYUVto10BitYUV(unPackedBuffer, packedBuffer,HD_NUMCOMPONENTPIXELS_1080_2K);


	NTV2Standard standard;
	GetStandard(&standard);
	NTV2FrameGeometry fg;
	GetFrameGeometry(&fg);
	bool twoKby1080 = false;
	if ( fg == NTV2_FG_2048x1080)
		twoKby1080 = true;


	bool vancEnabled;
	bool wideVANCSet;
	GetEnableVANCData(&vancEnabled,&wideVANCSet);
	NTV2FormatDescriptor formatDescriptor = GetFormatDescriptor(standard,_fbFormat,vancEnabled,twoKby1080,wideVANCSet);
	ULWord numPixels = formatDescriptor.numPixels;
	ULWord linePitch = formatDescriptor.linePitch;
	ULWord numLines = formatDescriptor.numLines;
	ULWord firstActiveLine = formatDescriptor.firstActiveLine;
	ULWord numOutputPixels = formatDescriptor.numPixels;
	ULWord dataLinePitch;

	// Avoid divide by 0 error/floating point exception for unsupported formats.
	if ( 
			(standard == NTV2_STANDARD_525 || standard == NTV2_STANDARD_625)
		&&	(_fbFormat == NTV2_FBF_8BIT_DVCPRO || _fbFormat == NTV2_FBF_8BIT_QREZ || _fbFormat == NTV2_FBF_8BIT_HDV)
	   )
	{
		DisplayNTV2Error("DVCPro, HDV, and QRez framebuffer formats not supported in 525 and 625.");
		return;
	}

	
	if ( standard == NTV2_STANDARD_1080p)
		standard = NTV2_STANDARD_1080;    // no diffence for test patterns.
	switch(standard) {
	case NTV2_STANDARD_1080:
		dataLinePitch = HD_YCBCRLINEPITCH_1080;
		break;
	case NTV2_STANDARD_720:
		dataLinePitch = HD_YCBCRLINEPITCH_720;
		break;
	case NTV2_STANDARD_525:
		dataLinePitch = YCBCRLINEPITCH_SD;
		break;
	case NTV2_STANDARD_625:
		dataLinePitch = YCBCRLINEPITCH_SD;
		break;
	case NTV2_STANDARD_2K:
		dataLinePitch = HD_YCBCRLINEPITCH_2K;
		break;
	default:
		return;
		break;
	}

	// Special cases for  DVCPRO and HDV
	if ( _fbFormat == NTV2_FBF_8BIT_DVCPRO  )
	{
		if ( standard == NTV2_STANDARD_1080 )
		{
			numPixels = HD_NUMCOMPONENTPIXELS_1080;
		}
		else
		if ( standard == NTV2_STANDARD_720 )
		{
			numPixels = HD_NUMCOMPONENTPIXELS_720;
		}
	}
	if ( _fbFormat == NTV2_FBF_8BIT_HDV  )
	{
		if ( standard == NTV2_STANDARD_1080 )
		{
			numPixels = HD_NUMCOMPONENTPIXELS_1080;
		}
		else
		if ( standard == NTV2_STANDARD_720 )
		{
			numPixels = HD_NUMCOMPONENTPIXELS_720;
		}
	}

	// Kludge for now.....and probably forever
	if ( fg == NTV2_FG_2048x1080)
	{
		AdjustFor2048x1080(numPixels,linePitch);
	}

	if ( numOutputPixels == 0 || numPixels == 0 || linePitch == 0)
	{
		DisplayNTV2Error("Unsupported video standard/framebuffer format.");
		return;

	}

	// determine where we're going to render the frame to
	ULWord *hostBuff = NULL;
	ULWord *currentAddress;
	if (_clientDownloadBuffer)
	{
		currentAddress = _clientDownloadBuffer;
		baseAddress = currentAddress;
	}
	else if (_bDMAtoBoard)
	{
		hostBuff = new ULWord[linePitch*numLines];			// render the test pattern to a host buffer, then DMA it
		assert(hostBuff);
		currentAddress = hostBuff;
		baseAddress = currentAddress;
	}
	else
	{
		GetBaseAddress(_channel,&baseAddress);
		assert (baseAddress);
		currentAddress = baseAddress;
	}


	// Assume 1080 format
	// Start with Black.
	Make10BitBlackLine(unPackedBuffer,HD_NUMCOMPONENTPIXELS_1080_2K);
	ULWord line = 0; 
	for ( ;line < firstActiveLine; line++)
	{
		// convert line to current frame buffer format
		ConvertLinePixelFormat(unPackedBuffer, packedBuffer, numPixels);

		// copy to formatted line to the frame buffer
		CopyMemory(currentAddress,packedBuffer,linePitch*4);
		currentAddress += linePitch;

	}	

	for ( UWord segmentCount = 0; segmentCount < NumTestPatternSegments; segmentCount++ )
	{

		SegmentDescriptor* segmentDescriptor = &pTestPatternSegmentData->segmentDescriptor[standard][segmentCount];
		ULWord* data = segmentDescriptor->data;
		if ( data != NULL )
		{
			UWord startLine = segmentDescriptor->startLine;
			UWord numLines = (segmentDescriptor->endLine - startLine) + 1;
			currentAddress = baseAddress + ((firstActiveLine+startLine)*linePitch);
			// go through hoops to mask out undesired components
			memcpy(packedBuffer,data,dataLinePitch*4);
			// We are using the 1920 standard test patterns for 2k 
			if (numPixels == HD_NUMCOMPONENTPIXELS_2K)
				::UnpackLine_10BitYUVto16BitYUV(packedBuffer, unPackedBuffer, HD_NUMCOMPONENTPIXELS_1080);
			else
				::UnpackLine_10BitYUVto16BitYUV(packedBuffer, unPackedBuffer, numPixels);

			if ( _signalMask != NTV2_SIGNALMASK_ALL)
				MaskYCbCrLine(unPackedBuffer, _signalMask , numPixels);
					
            bool  bIsSD;
            IsSDStandard(&bIsSD);
			switch(_fbFormat) 
			{
	            case NTV2_FBF_10BIT_YCBCR:
		            ::PackLine_16BitYUVto10BitYUV(unPackedBuffer, packedBuffer,numPixels);
                    break;
				case NTV2_FBF_8BIT_YCBCR:
					ConvertLineto8BitYCbCr(unPackedBuffer,(UByte*)packedBuffer,numPixels);
					break;
				case NTV2_FBF_ARGB:
					ConvertLinetoRGB(unPackedBuffer,(RGBAlphaPixel*)packedBuffer,numPixels, bIsSD);
					break;
				case NTV2_FBF_RGBA:
					ConvertLinetoRGB(unPackedBuffer,(RGBAlphaPixel*)packedBuffer,numPixels, bIsSD);
					ConvertARGBYCbCrToRGBA((UByte*)packedBuffer,numPixels);
					break;
				case NTV2_FBF_10BIT_RGB:
					ConvertLineto10BitRGB(unPackedBuffer,(RGBAlpha10BitPixel*)packedBuffer,numPixels, bIsSD);
					PackRGB10BitFor10BitRGB((RGBAlpha10BitPixel*)packedBuffer,numPixels);
					break;
				case NTV2_FBF_10BIT_RGB_PACKED:
					ConvertLineto10BitRGB(unPackedBuffer,(RGBAlpha10BitPixel*)packedBuffer,numPixels, bIsSD);
					PackRGB10BitFor10BitRGBPacked((RGBAlpha10BitPixel*)packedBuffer,numPixels);
					break;
				case NTV2_FBF_8BIT_YCBCR_YUY2:
					ConvertLineto8BitYCbCr(unPackedBuffer,(UByte*)packedBuffer,numPixels);
					Convert8BitYCbCrToYUY2((UByte*)packedBuffer,numPixels);
					break;
				case NTV2_FBF_ABGR:
					ConvertLinetoRGB(unPackedBuffer,(RGBAlphaPixel*)packedBuffer,numPixels, bIsSD);
					ConvertARGBYCbCrToABGR((UByte*)packedBuffer,numPixels);
					break;
				case NTV2_FBF_10BIT_DPX:
					ConvertLineto10BitRGB(unPackedBuffer,(RGBAlpha10BitPixel*)packedBuffer,numPixels, bIsSD);
					PackRGB10BitFor10BitDPX((RGBAlpha10BitPixel*)packedBuffer,numPixels);
					break;
				case NTV2_FBF_10BIT_YCBCR_DPX:
					::PackLine_16BitYUVto10BitYUV(unPackedBuffer, packedBuffer,numPixels);
					RePackLineDataForYCbCrDPX(packedBuffer,linePitch);
					break;
				case NTV2_FBF_8BIT_DVCPRO:
				case NTV2_FBF_8BIT_HDV:
					// need to squeeze test pattern to # of pixels.
					ReSampleYCbCrSampleLine((Word*)unPackedBuffer,(Word*)unPackedBuffer,numPixels,numOutputPixels);
					ConvertLineto8BitYCbCr(unPackedBuffer,(UByte*)packedBuffer,numOutputPixels);
					break;
				case NTV2_FBF_8BIT_QREZ:
					// need to squeeze test pattern to 1/2 pixels and 1/2 lines
					ReSampleYCbCrSampleLine((Word*)unPackedBuffer,(Word*)unPackedBuffer,numPixels,numOutputPixels);
					ConvertLineto8BitYCbCr(unPackedBuffer,(UByte*)packedBuffer,numPixels);
					Convert8BitYCbCrToYUY2((UByte*)packedBuffer,numPixels);
					numLines = (numLines/2);
					//linePitch = 1280*2/4;
					currentAddress = baseAddress + (startLine/2*linePitch);
					break;
				case NTV2_FBF_24BIT_RGB:
					ConvertLinetoRGB(unPackedBuffer,(RGBAlphaPixel*)packedBuffer,numPixels, bIsSD);
					ConvertARGBToRGB((UByte*)packedBuffer ,(UByte *) packedBuffer, numPixels);
					break;
				case NTV2_FBF_24BIT_BGR:
					ConvertLinetoRGB(unPackedBuffer,(RGBAlphaPixel*)packedBuffer,numPixels, bIsSD);
					ConvertARGBToBGR((UByte*)packedBuffer ,(UByte *) packedBuffer, numPixels);
					break;
				case NTV2_FBF_10BIT_YCBCRA:
					ConvertLineto10BitYCbCrA(unPackedBuffer,packedBuffer,numPixels);
					break;
				case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
					ConvertLineto10BitRGB(unPackedBuffer,(RGBAlpha10BitPixel*)packedBuffer,numPixels, bIsSD);
					PackRGB10BitFor10BitDPX((RGBAlpha10BitPixel*)packedBuffer,numPixels,false);
					break;
				case NTV2_FBF_48BIT_RGB:
					ConvertLineto16BitRGB(unPackedBuffer,(RGBAlpha16BitPixel*)packedBuffer,numPixels, bIsSD);
					Convert16BitARGBTo16BitRGB((RGBAlpha16BitPixel*)packedBuffer ,(UWord *) packedBuffer, numPixels);
					break;
				default:
					DisplayNTV2Error("Unsupported framebuffer standard requested.");
					break;
			}
			
			for ( UWord lineCount = 0; lineCount < numLines; lineCount++ )
			{
				CopyMemory(currentAddress,packedBuffer,linePitch*4);
				currentAddress += linePitch;
			}

		}
	}
	
		// DMA the resulting frame to Kona memory
	if (_bDMAtoBoard && hostBuff != NULL)
	{
			// kind of a kludge - we're still assuming the current PIO frame is the frame we want to download to...
		ULWord pciAccessFrame;
		GetPCIAccessFrame (_channel, &pciAccessFrame);
		if ( pciAccessFrame > GetNumFrameBuffers())
			pciAccessFrame = 0;
			
		// TODO: fix me
//		if ( _fbFormat == NTV2_FBF_48BIT_RGB )
//			pciAccessFrame = 0;

		DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE, pciAccessFrame, hostBuff, (numLines * linePitch * 4), true);
		
		delete [] hostBuff;
	}

	if ( _clientDownloadBuffer == NULL)
	{
		SetDualLinkOutputEnable(_dualLinkOutputEnable );
		SetFrameBufferFormat(_channel,_fbFormat);
		SetFrameBufferOrientation(_channel,NTV2_FRAMEBUFFER_ORIENTATION_TOPDOWN);
	}

	#if !defined (AJAMac) && !defined (NTV2_DEPRECATE)
	if ( (NTV2BoardNeedsRoutingSetup(GetBoardID())) && _autoRouteOnXena2)
	{
		CNTV2SignalRouter router;
		BuildRoutingTableForOutput(router,_channel,_fbFormat,false,false,_dualLinkOutputEnable);
		ApplySignalRoute(router);
	}
	#endif	//	!defined (AJAMac) && !defined (NTV2_DEPRECATE)
	
	if( _flipFlopPage)
		FlipFlopPage(_channel);
}

void CNTV2TestPattern::DownloadBlackTestPattern(  )
{
	ULWord* baseAddress=0;
	ULWord  packedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K];
	UWord   unPackedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*2];

	NTV2Standard standard;
	GetStandard(&standard);
	NTV2FrameGeometry fg;
	GetFrameGeometry(&fg);
	bool twoKby1080 = false;
	if ( fg == NTV2_FG_2048x1080)
		twoKby1080 = true;
	bool vancEnabled;
	bool wideVANCSet;
	GetEnableVANCData(&vancEnabled,&wideVANCSet);
	NTV2FormatDescriptor formatDescriptor = GetFormatDescriptor(standard,_fbFormat,vancEnabled,twoKby1080,wideVANCSet);
	ULWord numPixels = formatDescriptor.numPixels;
	ULWord linePitch = formatDescriptor.linePitch;
	ULWord numLines = formatDescriptor.numLines;


// Kludge for now.....
	if ( fg == NTV2_FG_2048x1080 )
	{
		AdjustFor2048x1080(numPixels,linePitch);
	}
	
		// determine where we're going to render the frame to
	ULWord *hostBuff = NULL;
	ULWord *currentAddress;
	if (_clientDownloadBuffer)
	{
		currentAddress = (ULWord *)_clientDownloadBuffer;
	}
	else if (_bDMAtoBoard)
	{
		hostBuff = new ULWord[linePitch*numLines];			// render the test pattern to a host buffer, then DMA it
		assert(hostBuff);
		currentAddress = hostBuff;
	}
	else
	{
		GetBaseAddress(_channel,&baseAddress);
		assert (baseAddress);
		currentAddress = baseAddress;
	}


	// or if fbFormat is not 10 Bit YCbCr
	switch(_fbFormat) 
	{
	case NTV2_FBF_10BIT_YCBCR:
	case NTV2_FBF_8BIT_YCBCR:
	case NTV2_FBF_8BIT_YCBCR_YUY2:
	case NTV2_FBF_10BIT_YCBCR_DPX:
	case NTV2_FBF_8BIT_DVCPRO:
	case NTV2_FBF_8BIT_HDV:
	case NTV2_FBF_8BIT_QREZ:
	case NTV2_FBF_10BIT_YCBCRA:
		{
		// Assume 1080 format
			for ( int count = 0; count < HD_NUMCOMPONENTPIXELS_1080_2K*2; count+=2 )
			{
				unPackedBuffer[count] = (UWord)CCIR601_10BIT_CHROMAOFFSET;
				unPackedBuffer[count+1] = (UWord)CCIR601_10BIT_BLACK;
			}

			if ( _fbFormat == NTV2_FBF_10BIT_YCBCR)
			{
				::PackLine_16BitYUVto10BitYUV(unPackedBuffer, packedBuffer,numPixels);
			}
			else
			if ( _fbFormat == NTV2_FBF_10BIT_YCBCR_DPX)
			{
				::PackLine_16BitYUVto10BitYUV(unPackedBuffer, packedBuffer,numPixels);
				RePackLineDataForYCbCrDPX(packedBuffer,linePitch);
			}
			else
			if ( _fbFormat == NTV2_FBF_10BIT_YCBCRA)
			{
				ConvertLineto10BitYCbCrA(unPackedBuffer,packedBuffer,numPixels);
			}
			else
			{
				ConvertLineto8BitYCbCr(unPackedBuffer,(UByte*)packedBuffer,numPixels);
				if ( _fbFormat == NTV2_FBF_8BIT_YCBCR_YUY2 || _fbFormat == NTV2_FBF_8BIT_QREZ)
					Convert8BitYCbCrToYUY2((UByte*)packedBuffer,numPixels);
			}

			for ( ULWord lineCount = 0; lineCount < numLines; lineCount++ )
			{
				CopyMemory(currentAddress,packedBuffer,linePitch*4);
				currentAddress += linePitch;
			}	
		}
		break;
	case NTV2_FBF_ARGB:
	case NTV2_FBF_RGBA:
	case NTV2_FBF_10BIT_RGB:
	case NTV2_FBF_10BIT_RGB_PACKED:
	case NTV2_FBF_ABGR:
	case NTV2_FBF_10BIT_DPX:
	case NTV2_FBF_24BIT_RGB:
	case NTV2_FBF_24BIT_BGR:
	case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
	case NTV2_FBF_48BIT_RGB:
		memset(currentAddress,0,linePitch*numLines*4);
		break;
	default:
		DisplayNTV2Error("Unsupported framebuffer standard requested.");
		break;
	}

		// DMA the resulting frame to Kona memory
	if (_bDMAtoBoard && hostBuff != NULL)
	{
			// kind of a kludge - we're still assuming the current PIO frame is the frame we want to download to...
		ULWord outputFrame;
		if ( _flipFlopPage )
			GetPCIAccessFrame (_channel, &outputFrame);
		else
			GetOutputFrame (_channel, &outputFrame);
		if ( outputFrame > GetNumFrameBuffers())
			outputFrame = 0;
			
		// TODO: fix me
//		if ( _fbFormat == NTV2_FBF_48BIT_RGB )
//			outputFrame = 0;

		DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE, outputFrame, (ULWord *)hostBuff, (numLines * linePitch * 4), true);
		
		delete [] hostBuff;
	}

	SetDualLinkOutputEnable(_dualLinkOutputEnable );
	SetFrameBufferFormat(_channel,_fbFormat);
	SetFrameBufferOrientation(_channel,NTV2_FRAMEBUFFER_ORIENTATION_TOPDOWN);

	#if !defined (AJAMac) && !defined (NTV2_DEPRECATE)
	if ( (NTV2BoardNeedsRoutingSetup(GetBoardID())) && _autoRouteOnXena2)
	{
		CNTV2SignalRouter router;
		BuildRoutingTableForOutput(router,_channel,_fbFormat,false,false,_dualLinkOutputEnable);
		ApplySignalRoute(router);
	}
	#endif	//	!defined (AJAMac) && !defined (NTV2_DEPRECATE)
	
	if ( _flipFlopPage)
		FlipFlopPage(_channel);
}	

void CNTV2TestPattern::DownloadBorderTestPattern(  )
{
	ULWord* baseAddress=0;
	ULWord  packedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*4];
	UWord   unPackedEdgeBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*2];
	UWord   unPackedWhiteBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*2];

	NTV2Standard standard;
	GetStandard(&standard);
	NTV2FrameGeometry fg;
	GetFrameGeometry(&fg);
	bool twoKby1080 = false;
	if ( fg == NTV2_FG_2048x1080)
		twoKby1080 = true;

	ULWord linePitch;
	ULWord numPixels;
	UWord   numLines;
	UWord Y,Cb,Cr;
	
	bool vancEnabled;
	bool wideVANCSet;
	GetEnableVANCData(&vancEnabled,&wideVANCSet);
	NTV2FormatDescriptor formatDescriptor = GetFormatDescriptor(standard,_fbFormat,vancEnabled,twoKby1080,wideVANCSet);
	numPixels = formatDescriptor.numPixels;
	linePitch = formatDescriptor.linePitch;
	numLines = formatDescriptor.numLines;
	
	Y = CCIR601_10BIT_BLACK; Cb = CCIR601_10BIT_CHROMAOFFSET; Cr = CCIR601_10BIT_CHROMAOFFSET;
	if ( _signalMask & NTV2_SIGNALMASK_Y)
		Y = CCIR601_10BIT_WHITE;
	if ( _signalMask & NTV2_SIGNALMASK_Cb)
		Cb  = 0x1C0;
	if ( _signalMask & NTV2_SIGNALMASK_Cr)
		Cr  = 0x3C0;



// Kludge for now.....
	if ( twoKby1080 )
	{
		AdjustFor2048x1080(numPixels,linePitch);
	}


	// trap quartersize mode 
	ULWord quarterSizeMode = 0;
	ReadRegister(kRegCh2Control, &quarterSizeMode, kRegMaskQuarterSizeMode, kRegShiftQuarterSizeMode);
	if ( _fbFormat == NTV2_FBF_10BIT_YCBCR  && quarterSizeMode )
	{
		numLines /= 2;
		numPixels /= 2;

		switch ( standard )
		{
		case NTV2_STANDARD_720:
			linePitch = 1728/4;
			break;
		case NTV2_STANDARD_1080:
		case NTV2_STANDARD_1080p:
		case NTV2_STANDARD_525:
		case NTV2_STANDARD_625:
		case NTV2_STANDARD_2K:
			linePitch /= 2;
			break;

		default:
			DisplayNTV2Error("DownloadBorderTestPattern(): Unsupported framebuffer standard requested.");
			break;
		}
	}

	
	
	// determine where we're going to render the frame to
	ULWord *hostBuff = NULL;
	ULWord *currentAddress;
	if (_clientDownloadBuffer)
	{
		currentAddress = (ULWord *)_clientDownloadBuffer;
	}
	else if (_bDMAtoBoard)
	{
		hostBuff = new ULWord[linePitch*numLines];			// render the test pattern to a host buffer, then DMA it
		assert(hostBuff);
		currentAddress = hostBuff;
	}
	else
	{
		GetBaseAddress(_channel,&baseAddress);
		assert (baseAddress);
		currentAddress = baseAddress;
	}
	
	// Assume 1080 format
	// Start with Black.
	Make10BitBlackLine(unPackedEdgeBuffer,HD_NUMCOMPONENTPIXELS_1080_2K);

	// Put in White Edge.
	unPackedEdgeBuffer[0] = (UWord)Cb;
	unPackedEdgeBuffer[+1] = (UWord)Y;
	unPackedEdgeBuffer[+2] = (UWord)Cr;
	unPackedEdgeBuffer[(numPixels)*2-1] = (UWord)Y;
	unPackedEdgeBuffer[(numPixels)*2-2] = (UWord)Cr;
	if ( _signalMask == NTV2_SIGNALMASK_Y)
		unPackedEdgeBuffer[(numPixels)*2-3] = (UWord)CCIR601_10BIT_BLACK;
	else
		unPackedEdgeBuffer[(numPixels)*2-3] = (UWord)Y;
	
	unPackedEdgeBuffer[(numPixels)*2-4] = (UWord)Cb;


	Make10BitLine(unPackedWhiteBuffer, Y , Cb , Cr,HD_NUMCOMPONENTPIXELS_1080_2K);

	for (UWord line=0; line < numLines; line++)
	{
		UWord* unPackedBuffer;
		if ( line == 0 || line == (numLines-1) || line == formatDescriptor.firstActiveLine )
			unPackedBuffer = unPackedWhiteBuffer;
		else if ( standard == NTV2_STANDARD_2K && line == HD_FIRSTACTIVELINE_2K)
			unPackedBuffer = unPackedWhiteBuffer;
		else
			unPackedBuffer = unPackedEdgeBuffer;


		// or if fbFormat is not 10 Bit YCbCr
			// convert line to current frame buffer format
		ConvertLinePixelFormat(unPackedBuffer, packedBuffer, numPixels);

			// copy to formatted line to the frame buffer
		CopyMemory(currentAddress,packedBuffer,linePitch*4);
		currentAddress += linePitch;

	}

		// DMA the resulting frame to Kona memory
	if (_bDMAtoBoard && hostBuff != NULL)
	{
			// kind of a kludge - we're still assuming the current PIO frame is the frame we want to download to...
		ULWord pciAccessFrame;
		GetPCIAccessFrame (_channel, &pciAccessFrame);
		if ( pciAccessFrame > GetNumFrameBuffers())
			pciAccessFrame = 0;
			
		// TODO: fix me
//		if ( _fbFormat == NTV2_FBF_48BIT_RGB )
//			pciAccessFrame = 0;

		DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE, pciAccessFrame, (ULWord *)hostBuff, (numLines * linePitch * 4), true);
		
		delete [] hostBuff;
	}
	
	SetDualLinkOutputEnable(_dualLinkOutputEnable );
	SetFrameBufferFormat(_channel,_fbFormat);
	SetFrameBufferOrientation(_channel,NTV2_FRAMEBUFFER_ORIENTATION_TOPDOWN);

	#if !defined (AJAMac) && !defined (NTV2_DEPRECATE)
	if ( (NTV2BoardNeedsRoutingSetup(GetBoardID())) && _autoRouteOnXena2)
	{
		CNTV2SignalRouter router;
		BuildRoutingTableForOutput(router,_channel,_fbFormat,false,false,_dualLinkOutputEnable);
		ApplySignalRoute(router);
	}
	#endif	//	!defined (AJAMac) && !defined (NTV2_DEPRECATE)

	FlipFlopPage(_channel);
}	

void CNTV2TestPattern::DownloadSlantRampTestPattern(  )
{
	switch ( _fbFormat )
	{
	case NTV2_FBF_48BIT_RGB:
		Download48BitRGBSlantRampTestPattern();
		break;
	default:
		DownloadYCbCrSlantRampTestPattern(  );
	}

}	
void CNTV2TestPattern::Download48BitRGBSlantRampTestPattern()
{
	ULWord* baseAddress=0;
	RGBAlpha16BitPixel rgbPixels[HD_NUMCOMPONENTPIXELS_1080_2K*2];
	ULWord   packedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*4];

	NTV2Standard standard;
	GetStandard(&standard);
	NTV2FrameGeometry fg;
	GetFrameGeometry(&fg);
	bool twoKby1080 = false;
	if ( fg == NTV2_FG_2048x1080)
		twoKby1080 = true;


	bool vancEnabled;
	bool wideVANCSet;
	GetEnableVANCData(&vancEnabled,&wideVANCSet);
	NTV2FormatDescriptor formatDescriptor = GetFormatDescriptor(standard,_fbFormat,vancEnabled,twoKby1080,wideVANCSet);
	ULWord numPixels = formatDescriptor.numPixels;
	ULWord linePitch = formatDescriptor.linePitch;
	ULWord numLines = formatDescriptor.numLines;
	ULWord firstActiveLine = formatDescriptor.firstActiveLine;



	// determine where we're going to render the frame to
	ULWord *hostBuff = NULL;
	ULWord *currentAddress;
	if (_clientDownloadBuffer)
	{
		currentAddress = (ULWord *)_clientDownloadBuffer;
	}
	else if (_bDMAtoBoard)
	{
		hostBuff = new ULWord[linePitch*numLines];			// render the test pattern to a host buffer, then DMA it
		assert(hostBuff);
		currentAddress = hostBuff;
	}
	else
	{
		GetBaseAddress(_channel,&baseAddress);
		assert (baseAddress);
		currentAddress = baseAddress;
	}

	memset(rgbPixels,0,sizeof(RGBAlpha16BitPixel)*HD_NUMCOMPONENTPIXELS_1080_2K);

	ULWord line = 0; 
	Convert16BitARGBTo16BitRGB(rgbPixels, (UWord *) packedBuffer, numPixels);
	for ( ;line < firstActiveLine; line++)
	{
		// copy to formatted line to the frame buffer
		CopyMemory(currentAddress,(ULWord *)rgbPixels,linePitch*4);
		currentAddress += linePitch;
	}

	for (; line < numLines; line++)
	{
		UWord startPixelValue = line*8;
		for ( ULWord pixelNumber= 0; pixelNumber < numPixels; pixelNumber++ )
		{
			UWord currentValue = startPixelValue+pixelNumber;
			if ( currentValue > 0x4095 ) currentValue = 0;
			rgbPixels[pixelNumber].Alpha = 0x4095-currentValue;
			rgbPixels[pixelNumber].Red = currentValue<<4;
			rgbPixels[pixelNumber].Blue = currentValue<<4;
			rgbPixels[pixelNumber].Green = currentValue<<4;

		}
		Convert16BitARGBTo16BitRGB(rgbPixels, (UWord *) packedBuffer, numPixels);
		CopyMemory(currentAddress,packedBuffer,linePitch*4);
		currentAddress += linePitch;

	}

	// DMA the resulting frame to Kona memory
	if (_bDMAtoBoard && hostBuff != NULL)
	{
		// kind of a kludge - we're still assuming the current PIO frame is the frame we want to download to...
		ULWord pciAccessFrame;
		GetPCIAccessFrame (_channel, &pciAccessFrame);
		if ( pciAccessFrame > GetNumFrameBuffers())
			pciAccessFrame = 0;

		DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE, pciAccessFrame, (ULWord *)hostBuff, (numLines * linePitch * 4), true);

		delete [] hostBuff;
	}

	SetDualLinkOutputEnable(_dualLinkOutputEnable );
	SetFrameBufferFormat(_channel,_fbFormat);
	SetFrameBufferOrientation(_channel,NTV2_FRAMEBUFFER_ORIENTATION_TOPDOWN);

	#if !defined (AJAMac) && !defined (NTV2_DEPRECATE)
	if ( (NTV2BoardNeedsRoutingSetup(GetBoardID())) && _autoRouteOnXena2)
	{
		CNTV2SignalRouter router;
		BuildRoutingTableForOutput(router,_channel,_fbFormat,false,false,_dualLinkOutputEnable);
		ApplySignalRoute(router);
	}
	#endif	//	!defined (AJAMac) && !defined (NTV2_DEPRECATE)

	FlipFlopPage(_channel);
}


void CNTV2TestPattern::DownloadYCbCrSlantRampTestPattern(  )
{
	ULWord* baseAddress=0;
	ULWord  packedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*4];
	UWord   unPackedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*2];


	NTV2Standard standard;
	GetStandard(&standard);
	NTV2FrameGeometry fg;
	GetFrameGeometry(&fg);
	bool twoKby1080 = false;
	if ( fg == NTV2_FG_2048x1080)
		twoKby1080 = true;


	bool vancEnabled;
	bool wideVANCSet;
	GetEnableVANCData(&vancEnabled,&wideVANCSet);
	NTV2FormatDescriptor formatDescriptor = GetFormatDescriptor(standard,_fbFormat,vancEnabled,twoKby1080,wideVANCSet);
	ULWord numPixels = formatDescriptor.numPixels;
	ULWord linePitch = formatDescriptor.linePitch;
	ULWord numLines = formatDescriptor.numLines;
	ULWord firstActiveLine = formatDescriptor.firstActiveLine;

	// Kludge for now.....
	if ( twoKby1080)
	{
		AdjustFor2048x1080(numPixels,linePitch);
	}

	// determine where we're going to render the frame to
	ULWord *hostBuff = NULL;
	ULWord *currentAddress;
	if (_clientDownloadBuffer)
	{
		currentAddress = (ULWord *)_clientDownloadBuffer;
	}
	else if (_bDMAtoBoard)
	{
		hostBuff = new ULWord[linePitch*numLines];			// render the test pattern to a host buffer, then DMA it
		assert(hostBuff);
		currentAddress = hostBuff;
	}
	else
	{
		GetBaseAddress(_channel,&baseAddress);
		assert (baseAddress);
		currentAddress = baseAddress;
	}

	// Assume 1080 format
	// Start with Black.
	Make10BitBlackLine(unPackedBuffer,HD_NUMCOMPONENTPIXELS_1080_2K);
	ULWord line = 0; 
	for ( ;line < firstActiveLine; line++)
	{
		// convert line to current frame buffer format
		ConvertLinePixelFormat(unPackedBuffer, packedBuffer, numPixels);

		// copy to formatted line to the frame buffer
		CopyMemory(currentAddress,packedBuffer,linePitch*4);
		currentAddress += linePitch;

	}

	for (; line < numLines; line++)
	{
		LWord value = (line-firstActiveLine)%0x36C;
		for ( unsigned int pixelCount = 0; pixelCount < numPixels*2; pixelCount++ )
		{	
			if ( pixelCount & BIT_0 )
			{
				// luma
				if ( _signalMask & NTV2_SIGNALMASK_Y)				
					unPackedBuffer[pixelCount] = 0x40+value;
				else
					unPackedBuffer[pixelCount] = CCIR601_10BIT_BLACK;


				if (++value > 0x36C) value = 0x00;
			}
			else
			{
				// chroma 
				if ( pixelCount & BIT_1 )
				{
					// Cr
					if ( _signalMask & NTV2_SIGNALMASK_Cr)				
						unPackedBuffer[pixelCount] = 0x40+value;
					else
						unPackedBuffer[pixelCount] = CCIR601_10BIT_CHROMAOFFSET;
				}
				else
				{
					// Cb
					if ( _signalMask & NTV2_SIGNALMASK_Cb)				
						unPackedBuffer[pixelCount] = 0x40+value;
					else
						unPackedBuffer[pixelCount] = CCIR601_10BIT_CHROMAOFFSET;
				}

			}
		}

		// convert line to current frame buffer format
		ConvertLinePixelFormat(unPackedBuffer, packedBuffer, numPixels);

		// copy to formatted line to the frame buffer
		CopyMemory(currentAddress,packedBuffer,linePitch*4);
		currentAddress += linePitch;

	}

	// DMA the resulting frame to Kona memory
	if (_bDMAtoBoard && hostBuff != NULL)
	{
		// kind of a kludge - we're still assuming the current PIO frame is the frame we want to download to...
		ULWord pciAccessFrame;
		GetPCIAccessFrame (_channel, &pciAccessFrame);
		if ( pciAccessFrame > GetNumFrameBuffers())
			pciAccessFrame = 0;

		// TODO: fix me
		//		if ( _fbFormat == NTV2_FBF_48BIT_RGB )
		//			pciAccessFrame = 0;

		DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE, pciAccessFrame, (ULWord *)hostBuff, (numLines * linePitch * 4), true);

		delete [] hostBuff;
	}

	SetDualLinkOutputEnable(_dualLinkOutputEnable );
	SetFrameBufferFormat(_channel,_fbFormat);
	SetFrameBufferOrientation(_channel,NTV2_FRAMEBUFFER_ORIENTATION_TOPDOWN);

	#if !defined (AJAMac) && !defined (NTV2_DEPRECATE)
	if ( (NTV2BoardNeedsRoutingSetup(GetBoardID())) && _autoRouteOnXena2)
	{
		CNTV2SignalRouter router;
		BuildRoutingTableForOutput(router,_channel,_fbFormat,false,false,_dualLinkOutputEnable);
		ApplySignalRoute(router);
	}
	#endif	//	!defined (AJAMac) && !defined (NTV2_DEPRECATE)

	FlipFlopPage(_channel);
}



void CNTV2TestPattern::DownloadVerticalSweepTestPattern(  )
{
	ULWord* baseAddress=0;
	ULWord  packedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*4];
	UWord   unPackedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*2];


	NTV2Standard standard;
	GetStandard(&standard);
	NTV2FrameGeometry fg;
	GetFrameGeometry(&fg);
	bool twoKby1080 = false;
	if ( fg == NTV2_FG_2048x1080)
		twoKby1080 = true;

	bool vancEnabled;
	bool wideVANCSet;
	GetEnableVANCData(&vancEnabled,&wideVANCSet);
	NTV2FormatDescriptor formatDescriptor = GetFormatDescriptor(standard,_fbFormat,vancEnabled,twoKby1080,wideVANCSet);
	ULWord numPixels = formatDescriptor.numPixels;
	ULWord linePitch = formatDescriptor.linePitch;
	ULWord numLines = formatDescriptor.numLines;
	ULWord firstActiveLine = formatDescriptor.firstActiveLine;

	// Kludge for now.....
	if ( twoKby1080)
	{
		AdjustFor2048x1080(numPixels,linePitch);
	}

	// determine where we're going to render the frame to
	ULWord *hostBuff = NULL;
	ULWord *currentAddress;
	if (_clientDownloadBuffer)
	{
		currentAddress = (ULWord *)_clientDownloadBuffer;
	}
	else if (_bDMAtoBoard)
	{
		hostBuff = new ULWord[linePitch*numLines];			// render the test pattern to a host buffer, then DMA it
		assert(hostBuff);
		currentAddress = hostBuff;
	}
	else
	{
		GetBaseAddress(_channel,&baseAddress);
		assert (baseAddress);
		currentAddress = baseAddress;
	}

	// Assume 1080 format
	// Start with Black.
	Make10BitBlackLine(unPackedBuffer,HD_NUMCOMPONENTPIXELS_1080_2K);
	ULWord line = 0; 
	for ( ;line < firstActiveLine; line++)
	{
		// convert line to current frame buffer format
		ConvertLinePixelFormat(unPackedBuffer, packedBuffer, numPixels);

		// copy to formatted line to the frame buffer
		CopyMemory(currentAddress,packedBuffer,linePitch*4);
		currentAddress += linePitch;

	}


	double pi = 3.1415926535898;
	double freq = 0.0;
	double wavelength = 0.0;
	double accum = -pi / 2.0;		// start with a phase offset of "-Pi/2" to start in Black

	UWord pixY, pixCb, pixCr;
	UWord markerY, markerCb, markerCr;

	unsigned int markerWidth = numPixels / 10;		// devote 10% of line width to frequency markers

	numLines = numLines-firstActiveLine;
	for ( line=0; line < numLines; line++)
	{
			// for vertical sweep, pixel values are constant for entire line
		freq = (double)line / (double)numLines;					// sweeping freq from 0.0 -> 1.0 (1.0 = fs/2)
		if (line > 0)
			wavelength = pi * freq;	// how many radians do we step over at this freq? (max freq = Pi radians/pixel)
			

			// output marker at each 0.1 freq multiples
			// is this a "marker" line?
		int mark = numLines / 10;
		int curr = line % mark;

			// what video to use during "marker zone"
		if (curr == 0)
		{
			markerY  = (_signalMask & NTV2_SIGNALMASK_Y )  ? 940 : 502;
			markerCr = (_signalMask & NTV2_SIGNALMASK_Cr)  ? 940 : 512;
			markerCb = (_signalMask & NTV2_SIGNALMASK_Cb)  ? 940 : 512;
		}
		else if (curr == 1 || curr == (mark - 1))
		{
			markerY  = (_signalMask & NTV2_SIGNALMASK_Y )  ? 740 : 502;
			markerCr = (_signalMask & NTV2_SIGNALMASK_Cr)  ? 850 : 512;
			markerCb = (_signalMask & NTV2_SIGNALMASK_Cb)  ? 850 : 512;
		}
		else if (curr == 2 || curr == (mark - 2))
		{
			markerY  = (_signalMask & NTV2_SIGNALMASK_Y )  ? 260 : 502;
			markerCr = (_signalMask & NTV2_SIGNALMASK_Cr)  ? 620 : 512;
			markerCb = (_signalMask & NTV2_SIGNALMASK_Cb)  ? 620 : 512;
		}
		else
		{
			markerY  = (_signalMask & NTV2_SIGNALMASK_Y )  ?  64 : 502;
			markerCr = (_signalMask & NTV2_SIGNALMASK_Cr)  ? 512 : 512;
			markerCb = (_signalMask & NTV2_SIGNALMASK_Cb)  ? 512 : 512;
		}


			// fill entire line
		for (unsigned int pixelCount = 0; pixelCount < numPixels; pixelCount++)
		{	
				// in order to get a better "fuzz" display on scopes and waveform monitors,
				// we're going to add a slight offset to the current sine wave as it goes
				// across the line. Technically we are making a diagonal wavefront, but we're
				// only varying by 1/2 cycle across the entire width, so the amount of
				// skew is slight.
			double offset = (pixelCount * pi) / numPixels;

			if (pixelCount > markerWidth)
			{
					// not marker: do sweep video
				pixY  = (_signalMask & NTV2_SIGNALMASK_Y)  ? MakeSineWaveVideo(accum+offset, false) : (CCIR601_10BIT_WHITE + CCIR601_10BIT_BLACK) / 2;	// Y
				pixCr = (_signalMask & NTV2_SIGNALMASK_Cr) ? MakeSineWaveVideo(accum+offset, true ) : CCIR601_10BIT_CHROMAOFFSET;	// Cr
				pixCb = (_signalMask & NTV2_SIGNALMASK_Cb) ? MakeSineWaveVideo(accum+offset, true ) : CCIR601_10BIT_CHROMAOFFSET;	// Cb

			}
			else
			{
					// do marker
				pixY  = markerY;	// Y
				pixCr = markerCr;	// Cr
				pixCb = markerCb;	// Cb
			}

			unPackedBuffer[(pixelCount*2)+1] = pixY;		// Y

			if ((pixelCount & BIT_0) == 0)
			{
					// remember: both chroma samples "belong" to the even Y samples
				unPackedBuffer[(pixelCount*2)+0] = pixCb;	// Cb
				unPackedBuffer[(pixelCount*2)+2] = pixCr;	// Cr
			}
		}
		
			// convert line to current frame buffer format
		ConvertLinePixelFormat(unPackedBuffer, packedBuffer, numPixels);

			// copy to formatted line to the frame buffer
		CopyMemory(currentAddress, packedBuffer, linePitch*4);
		currentAddress += linePitch;

			// setup for the next line's "frequency"
		accum += wavelength;
	}

		// DMA the resulting frame to Kona memory
	if (_bDMAtoBoard && hostBuff != NULL)
	{
			// kind of a kludge - we're still assuming the current PIO frame is the frame we want to download to...
		ULWord pciAccessFrame;
		GetPCIAccessFrame (_channel, &pciAccessFrame);
		if ( pciAccessFrame > GetNumFrameBuffers())
			pciAccessFrame = 0;
			
		DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE, pciAccessFrame, (ULWord *)hostBuff, ((numLines+firstActiveLine) * linePitch * 4), true);
		
		delete [] hostBuff;
	}
	
	SetDualLinkOutputEnable(_dualLinkOutputEnable );
	SetFrameBufferFormat(_channel,_fbFormat);
	SetFrameBufferOrientation(_channel,NTV2_FRAMEBUFFER_ORIENTATION_TOPDOWN);

	#if !defined (AJAMac) && !defined (NTV2_DEPRECATE)
	if ( (NTV2BoardNeedsRoutingSetup(GetBoardID())) && _autoRouteOnXena2)
	{
		CNTV2SignalRouter router;
		BuildRoutingTableForOutput(router,_channel,_fbFormat,false,false,_dualLinkOutputEnable);
		ApplySignalRoute(router);
	}
	#endif	//	!defined (AJAMac) && !defined (NTV2_DEPRECATE)
	
	FlipFlopPage(_channel);
}	



void CNTV2TestPattern::DownloadZonePlateTestPattern(  )
{
	ULWord* baseAddress=0;
	ULWord  packedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*4];
	UWord   unPackedBuffer[HD_NUMCOMPONENTPIXELS_1080_2K*2];

	NTV2Standard standard;
	GetStandard(&standard);
	NTV2FrameGeometry fg;
	GetFrameGeometry(&fg);
	bool twoKby1080 = false;
	if ( fg == NTV2_FG_2048x1080)
		twoKby1080 = true;

	bool vancEnabled;
	bool wideVANCSet;
	GetEnableVANCData(&vancEnabled,&wideVANCSet);
	NTV2FormatDescriptor formatDescriptor = GetFormatDescriptor(standard,_fbFormat,vancEnabled,twoKby1080,wideVANCSet);
	ULWord numPixels = formatDescriptor.numPixels;
	ULWord linePitch = formatDescriptor.linePitch;
	ULWord numLines = formatDescriptor.numLines;
	ULWord firstActiveLine = formatDescriptor.firstActiveLine;

	// Kludge for now.....
	if ( twoKby1080)
	{
		AdjustFor2048x1080(numPixels,linePitch);
	}

	// determine where we're going to render the frame to
	ULWord *hostBuff = NULL;
	ULWord *currentAddress;
	if (_clientDownloadBuffer)
	{
		currentAddress = (ULWord *)_clientDownloadBuffer;
	}
	else if (_bDMAtoBoard)
	{
		hostBuff = new ULWord[linePitch*numLines];			// render the test pattern to a host buffer, then DMA it
		assert(hostBuff);
		currentAddress = hostBuff;
	}
	else
	{
		GetBaseAddress(_channel,&baseAddress);
		assert (baseAddress);
		currentAddress = baseAddress;
	}

	// Assume 1080 format
	// Start with Black.
	Make10BitBlackLine(unPackedBuffer,HD_NUMCOMPONENTPIXELS_1080_2K);
	ULWord line = 0; 
	for ( ;line < firstActiveLine; line++)
	{
		// convert line to current frame buffer format
		ConvertLinePixelFormat(unPackedBuffer, packedBuffer, numPixels);

		// copy to formatted line to the frame buffer
		CopyMemory(currentAddress,packedBuffer,linePitch*4);
		currentAddress += linePitch;

	}

		//	set the scale so that the H frequency just gets to Nyquist (Fs/2) at the left and right edges of the picture.
		//  (you can use "numLines" instead to have it hit Nyquist at the top and bottom, but left and right will go past
		//  Nyquist...)
	double pattScale = 3.1415926535898 / (numPixels + 1);

		// if ONLY chroma channels are enabled, drop the frequency by another 1/2 since there are 1/2 the number of H pixels
	if (!(_signalMask & NTV2_SIGNALMASK_Y) && ((_signalMask & NTV2_SIGNALMASK_Cb) || (_signalMask & NTV2_SIGNALMASK_Cr)) )
		pattScale *= 0.5;

		// calculate each line
	numLines = numLines-firstActiveLine;
	for (line=0; line < numLines; line++)
	{
			// calculate each pixel
		for (unsigned int pixelCount = 0; pixelCount < numPixels; pixelCount++)
		{	
			double xDist = (double)pixelCount - ((double)numPixels  / 2.0);
			double yDist = (double)line - ((double)numLines / 2.0);
			double r = ((xDist * xDist) + (yDist * yDist)) * pattScale;

				// Y
			unPackedBuffer[(pixelCount*2)+1] = (_signalMask & NTV2_SIGNALMASK_Y)  ? MakeSineWaveVideo(r, false) : (CCIR601_10BIT_WHITE + CCIR601_10BIT_BLACK) / 2;

				// C
			if ((pixelCount & BIT_0) == 0)
			{
					// remember: both chroma samples "belong" to the even Y samples
				unPackedBuffer[(pixelCount*2)+0] = (_signalMask & NTV2_SIGNALMASK_Cb) ? MakeSineWaveVideo(r, true) : CCIR601_10BIT_CHROMAOFFSET;	// Cb
				unPackedBuffer[(pixelCount*2)+2] = (_signalMask & NTV2_SIGNALMASK_Cr) ? MakeSineWaveVideo(r, true) : CCIR601_10BIT_CHROMAOFFSET;	// Cr
			}
		}
		
			// convert line to current frame buffer format
		ConvertLinePixelFormat(unPackedBuffer, packedBuffer, numPixels);

			// copy to formatted line to the frame buffer
		CopyMemory(currentAddress, packedBuffer, linePitch*4);
		currentAddress += linePitch;
	}

		// DMA the resulting frame to Kona memory
	if (_bDMAtoBoard && hostBuff != NULL)
	{
			// kind of a kludge - we're still assuming the current PIO frame is the frame we want to download to...
		ULWord pciAccessFrame;
		GetPCIAccessFrame (_channel, &pciAccessFrame);
		if ( pciAccessFrame > GetNumFrameBuffers())
			pciAccessFrame = 0;
			
		DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE, pciAccessFrame, (ULWord *)hostBuff, ((numLines+firstActiveLine) * linePitch * 4), true);
		
		delete [] hostBuff;
	}
	
	SetDualLinkOutputEnable(_dualLinkOutputEnable );
	SetFrameBufferFormat(_channel,_fbFormat);
	SetFrameBufferOrientation(_channel,NTV2_FRAMEBUFFER_ORIENTATION_TOPDOWN);

	#if !defined (AJAMac) && !defined (NTV2_DEPRECATE)
	if ( (NTV2BoardNeedsRoutingSetup(GetBoardID())) && _autoRouteOnXena2)
	{
		CNTV2SignalRouter router;
		BuildRoutingTableForOutput(router,_channel,_fbFormat,false,false,_dualLinkOutputEnable);
		ApplySignalRoute(router);
	}
	#endif	//	!defined (AJAMac) && !defined (NTV2_DEPRECATE)

	FlipFlopPage(_channel);
}	


void CNTV2TestPattern::RenderTestPatternToBuffer(UWord testPatternNumber, ULWord *buffer)
{
	SetClientDownloadBuffer(buffer);
	DownloadTestPattern(testPatternNumber);
	SetClientDownloadBuffer(NULL);
}

bool CNTV2TestPattern::RenderTestPatternBuffer(NTV2Channel channel, UByte *buffer, NTV2VideoFormat videoFormat, NTV2FrameBufferFormat fbFormat, ULWord width, ULWord height, ULWord rowBytes)
{
	(void) videoFormat;
	(void) width;
	(void) height;
	(void) rowBytes;

	// initial setup
	SetMode(channel ,NTV2_MODE_DISPLAY);
	SetFrameBufferFormat(channel, fbFormat);
	SetTestPatternFrameBufferFormat(fbFormat);
	SetTestPatternDMAEnable(true);
	SetChannel(channel);
	SetFrameBufferQuarterSizeMode(channel, NTV2_QuarterSizeExpandOff);

	// now download it to device
	DownloadTestPatternBuffer((ULWord*)buffer);

	return true;
}


void CNTV2TestPattern::DownloadTestPatternBuffer(ULWord *buffer, ULWord size)
{
	if (size == 0)
		size = GetPatternBufferSize();
    
    NTV2FrameGeometry fg;
    GetFrameGeometry(&fg);
    
    SetDualLinkOutputEnable(_dualLinkOutputEnable );
    if (NTV2_IS_QUAD_FRAME_GEOMETRY(fg))
    {
        SetFrameBufferFormat(NTV2_CHANNEL1,_fbFormat);
        SetFrameBufferFormat(NTV2_CHANNEL2,_fbFormat);
        SetFrameBufferFormat(NTV2_CHANNEL3,_fbFormat);
        SetFrameBufferFormat(NTV2_CHANNEL4,_fbFormat);
    }
    else
    {
        SetFrameBufferFormat(_channel,_fbFormat);
    }
	SetFrameBufferOrientation(_channel,NTV2_FRAMEBUFFER_ORIENTATION_TOPDOWN);

		// DMA the resulting frame to Kona memory
	if (_bDMAtoBoard)
	{
			// kind of a kludge - we're still assuming the current PIO frame is the frame we want to download to...
		ULWord pciAccessFrame;
		GetPCIAccessFrame (_channel, &pciAccessFrame);
		if ( pciAccessFrame > GetNumFrameBuffers())
			pciAccessFrame = 0;

		// Transfer the test pattern into the active area of the frame buffer
		NTV2Standard standard;
		GetStandard(&standard);
		NTV2FormatDescriptor formatDescriptor = GetFormatDescriptor(standard,_fbFormat,false,false,false);

		ULWord totalHeight = formatDescriptor.numLines;
		if ( fg == NTV2_FG_1920x1112 )
			totalHeight = 1112;
		if ( fg == NTV2_FG_1920x1114 )
			totalHeight = 1114;

		ULWord dmaOffset = 0;
		dmaOffset = (totalHeight - formatDescriptor.numLines) * formatDescriptor.linePitch * 4;
		DmaWrite (NTV2_DMA_FIRST_AVAILABLE, pciAccessFrame, (ULWord *)buffer, dmaOffset, size, true);
	}
	else
	{
		ULWord* baseAddress=0;
		GetBaseAddress(_channel,&baseAddress);
		assert (baseAddress);
		CopyMemory(baseAddress, buffer, size);
	}

	#if !defined (AJAMac) && !defined (NTV2_DEPRECATE)
	if ( (NTV2BoardNeedsRoutingSetup(GetBoardID())) && _autoRouteOnXena2)
	{
		CNTV2SignalRouter router;
		BuildRoutingTableForOutput(router,_channel,_fbFormat,false,false,_dualLinkOutputEnable);
		ApplySignalRoute(router);
	}
	#endif	//	!defined (AJAMac) && !defined (NTV2_DEPRECATE)	

	FlipFlopPage(_channel);
}


ULWord CNTV2TestPattern::GetPatternBufferSize(ULWord *width, ULWord *height, ULWord *rowBytes, ULWord *firstLine)
{
	NTV2Standard standard;
	GetStandard(&standard);
	NTV2FrameGeometry fg;
	GetFrameGeometry(&fg);
	bool twoKby1080 = false;
	bool fourKby2160 = false;
	bool quadKby2160 = false;
	if ( fg == NTV2_FG_2048x1080 || fg == NTV2_FG_2048x1112 || fg == NTV2_FG_2048x1114 )
		twoKby1080 = true;
	if ( fg == NTV2_FG_4x1920x1080 )
		quadKby2160 = true;
	if ( fg == NTV2_FG_4x2048x1080 )
		fourKby2160 = true;

	// Ignore VANC and just use the size of the active area
	NTV2FormatDescriptor formatDescriptor = GetFormatDescriptor(standard,_fbFormat,false,false,false);
	ULWord numPixels = formatDescriptor.numPixels;
	ULWord linePitch = formatDescriptor.linePitch;
	ULWord numLines = formatDescriptor.numLines;
	ULWord firstActiveLine = formatDescriptor.firstActiveLine;

	// Kludge for now.....
	if ( twoKby1080)
		AdjustFor2048x1080(numPixels,linePitch);
	if ( quadKby2160 )
		AdjustFor3840x2160(numPixels, linePitch, numLines);
	if ( fourKby2160 )
		AdjustFor4096x2160(numPixels, linePitch, numLines);
	
	// buffer size
	if (width) *width = numPixels;
	if (height) *height = numLines;
	if (rowBytes) *rowBytes = linePitch * 4;
	if (firstLine) *firstLine = firstActiveLine;
	
	return numLines * linePitch * 4; 
}


// MakeSineWaveVideo()
//		Generate a 10-bit video level based on the input angle (in radians). If bChroma is false, the output level
//		varies from Black (64) to White (940), centered at the 50% luminance level (502). An input of "0.0" produces
//		a mid-gray output, so you'll need to start sweeps at -Pi/2 (or 3Pi/2) if you want it to start at Black level.
//		If bChroma is true, the output level varies from 64 to 960, centered at 0 chroma (512). An input of 0.0 produces
//		a zero chroma output ("512").
//
int CNTV2TestPattern::MakeSineWaveVideo(double radians, bool bChroma)
{
	int result;

//	double Gain   = 1.0;		// 1.0 = full amplitude, 0.5 = 25% -> 75%, etc.
	double Gain   = 0.65;
	
	double Scale  = (940.0 - 64.0) / 2.0;
	
	double Offset = (940.0 + 64.0) / 2.0;		// zero-level of sine waves sits at 50% gray

	if (!bChroma)
	{
		Scale  = (940.0 - 64.0) / 2.0;
		Offset = (940.0 + 64.0) / 2.0;

			// calculate -cosine value to start Y at minimum value
		result = (int)((sin(radians) * Scale * Gain) + Offset + 0.5);	// convert to 10-bit luma video levels
	}
	else
	{
		Scale  = (960.0 - 64.0) / 2.0;
		Offset = (960.0 + 64.0) / 2.0;

			// calculate sine value to start C at "zero" value
		result = (int)((sin(radians) * Scale * Gain) + Offset + 0.5);	// convert to 10-bit chroma video levels
	}

	return result;
}


// ConvertLinePixelFormat()
//		Converts a line of "unpacked" 10-bit Y/Cb/Cr pixels into a "packed" line in the pixel format
//	for the current frame buffer format.
void CNTV2TestPattern::ConvertLinePixelFormat(UWord *unPackedBuffer, ULWord *packedBuffer, int numPixels)
{
	bool  bIsSD;
    IsSDStandard(&bIsSD);

	switch(_fbFormat) 
	{
		case NTV2_FBF_10BIT_YCBCR:
			::PackLine_16BitYUVto10BitYUV(unPackedBuffer, packedBuffer, numPixels);
			break;

		case NTV2_FBF_8BIT_YCBCR:
			ConvertLineto8BitYCbCr(unPackedBuffer,(UByte*)packedBuffer, numPixels);
			break;

		case NTV2_FBF_ARGB:
			ConvertLinetoRGB(unPackedBuffer,(RGBAlphaPixel*)packedBuffer, numPixels, bIsSD);
			break;

		case NTV2_FBF_RGBA:
			ConvertLinetoRGB(unPackedBuffer,(RGBAlphaPixel*)packedBuffer, numPixels, bIsSD);
			ConvertARGBYCbCrToRGBA((UByte*)packedBuffer,numPixels);
			break;

		case NTV2_FBF_10BIT_RGB:
			ConvertLineto10BitRGB(unPackedBuffer, (RGBAlpha10BitPixel*)packedBuffer, numPixels, bIsSD);
			PackRGB10BitFor10BitRGB((RGBAlpha10BitPixel*)packedBuffer, numPixels);
			break;
		case NTV2_FBF_10BIT_RGB_PACKED:
			ConvertLineto10BitRGB(unPackedBuffer,(RGBAlpha10BitPixel*)packedBuffer,numPixels, bIsSD);
			PackRGB10BitFor10BitRGBPacked((RGBAlpha10BitPixel*)packedBuffer,numPixels);
			break;

		case NTV2_FBF_8BIT_YCBCR_YUY2:
			ConvertLineto8BitYCbCr(unPackedBuffer, (UByte*)packedBuffer, numPixels);
			Convert8BitYCbCrToYUY2((UByte*)packedBuffer, numPixels);
			break;

		case NTV2_FBF_ABGR:
			ConvertLinetoRGB(unPackedBuffer, (RGBAlphaPixel*)packedBuffer, numPixels, bIsSD);
			ConvertARGBYCbCrToABGR((UByte*)packedBuffer,numPixels);
			break;

		case NTV2_FBF_10BIT_DPX:
			ConvertLineto10BitRGB(unPackedBuffer, (RGBAlpha10BitPixel*)packedBuffer, numPixels, bIsSD);
			PackRGB10BitFor10BitDPX((RGBAlpha10BitPixel*)packedBuffer, numPixels);
			break;

		case NTV2_FBF_10BIT_YCBCR_DPX:
			::PackLine_16BitYUVto10BitYUV(unPackedBuffer, packedBuffer, numPixels);
			RePackLineDataForYCbCrDPX(packedBuffer, numPixels);
			break;

		case NTV2_FBF_8BIT_DVCPRO:
		case NTV2_FBF_8BIT_HDV:
			ConvertLineto8BitYCbCr(unPackedBuffer, (UByte*)packedBuffer, numPixels);
			break;

		case NTV2_FBF_8BIT_QREZ:
			ConvertLineto8BitYCbCr(unPackedBuffer, (UByte*)packedBuffer, numPixels);
			Convert8BitYCbCrToYUY2((UByte*)packedBuffer, numPixels);
			break;

		case NTV2_FBF_24BIT_RGB:
			ConvertLinetoRGB(unPackedBuffer, (RGBAlphaPixel*)packedBuffer, numPixels, bIsSD);
			ConvertARGBToRGB((UByte*)packedBuffer ,(UByte *) packedBuffer, numPixels);
			break;

		case NTV2_FBF_24BIT_BGR:
			ConvertLinetoRGB(unPackedBuffer, (RGBAlphaPixel*)packedBuffer, numPixels, bIsSD);
			ConvertARGBToBGR((UByte*)packedBuffer, (UByte *) packedBuffer, numPixels);
			break;

		case NTV2_FBF_10BIT_YCBCRA:
			{
				ConvertLineto10BitYCbCrA(unPackedBuffer, packedBuffer, numPixels);
				// make A as inverted Y
				for (int count = 0; count < numPixels; count++)
				{
					ULWord Y = packedBuffer[count] & 0x3FF;
					ULWord A = ((CCIR601_10BIT_WHITE-CCIR601_10BIT_BLACK) - (Y-CCIR601_10BIT_BLACK) + CCIR601_10BIT_BLACK);
					packedBuffer[count] &= (0x3ff << 10);
					packedBuffer[count] |= ((Y << 20) + A);
				}
			}
			break;

		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
			ConvertLineto10BitRGB(unPackedBuffer, (RGBAlpha10BitPixel*)packedBuffer, numPixels, bIsSD);
			PackRGB10BitFor10BitDPX((RGBAlpha10BitPixel*)packedBuffer, numPixels, false);
			break;

		case NTV2_FBF_48BIT_RGB:
			ConvertLineto16BitRGB(unPackedBuffer, (RGBAlpha16BitPixel*)packedBuffer, numPixels, bIsSD);
			Convert16BitARGBTo16BitRGB((RGBAlpha16BitPixel*)packedBuffer, (UWord *) packedBuffer, numPixels);
			break;

		default:
			DisplayNTV2Error("Unsupported framebuffer standard requested.");
			break;
	}
}


void CNTV2TestPattern::AdjustFor2048x1080(ULWord& numPixels,ULWord& linePitch)
{
	switch ( _fbFormat )
	{	
		case NTV2_FBF_8BIT_YCBCR:
		case NTV2_FBF_8BIT_YCBCR_YUY2:
			numPixels = HD_NUMCOMPONENTPIXELS_2K;
			linePitch = HD_NUMCOMPONENTPIXELS_2K*2/4;
			break;
		
		case NTV2_FBF_10BIT_YCBCR:
			numPixels = HD_NUMCOMPONENTPIXELS_2K;
			linePitch = HD_YCBCRLINEPITCH_2K;
			break; 
		
		case NTV2_FBF_ARGB:
		case NTV2_FBF_RGBA:
		case NTV2_FBF_ABGR:
		case NTV2_FBF_10BIT_RGB:
		case NTV2_FBF_10BIT_RGB_PACKED:
		case NTV2_FBF_10BIT_DPX:
		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
			numPixels = HD_NUMCOMPONENTPIXELS_2K;
			linePitch = HD_NUMCOMPONENTPIXELS_2K;
			break;
		
		case NTV2_FBF_24BIT_RGB:
		case NTV2_FBF_24BIT_BGR:
			numPixels = HD_NUMCOMPONENTPIXELS_2K;
			linePitch = RGB24LINEPITCH_2048;
			break;
	
		case NTV2_FBF_48BIT_RGB:
			numPixels = HD_NUMCOMPONENTPIXELS_2K;
			linePitch = RGB48LINEPITCH_2048;
			break;
	
		// Formats not yet supported, kill warning
		case NTV2_FBF_10BIT_YCBCR_DPX:		
		case NTV2_FBF_8BIT_DVCPRO:
		case NTV2_FBF_8BIT_QREZ:
		case NTV2_FBF_8BIT_HDV:
		case NTV2_FBF_10BIT_YCBCRA: 
		case NTV2_FBF_PRORES:
		case NTV2_FBF_PRORES_DVCPRO:
		case NTV2_FBF_PRORES_HDV:
		case NTV2_FBF_10BIT_ARGB:
		case NTV2_FBF_16BIT_ARGB:
		case NTV2_FBF_NUMFRAMEBUFFERFORMATS:
		default:
			numPixels = 0;
			linePitch = 0;
			break;
	}
}

void CNTV2TestPattern::AdjustFor3840x2160(ULWord& numPixels,ULWord& linePitch, ULWord& numLines)
{
	switch ( _fbFormat )
	{	
	case NTV2_FBF_8BIT_YCBCR:
	case NTV2_FBF_8BIT_YCBCR_YUY2:
		numPixels = HD_NUMCOMPONENTPIXELS_QUADHD;
		linePitch = HD_NUMCOMPONENTPIXELS_QUADHD*2/4;
		numLines = HD_NUMLINES_4K;
		break;

	case NTV2_FBF_10BIT_YCBCR:
		numPixels = HD_NUMCOMPONENTPIXELS_QUADHD;
		linePitch = HD_NUMCOMPONENTPIXELS_QUADHD*16/24;
		numLines = HD_NUMLINES_4K;
		break; 

	case NTV2_FBF_ARGB:
	case NTV2_FBF_RGBA:
	case NTV2_FBF_ABGR:
	case NTV2_FBF_10BIT_RGB:
	case NTV2_FBF_10BIT_RGB_PACKED:
	case NTV2_FBF_10BIT_DPX:
	case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
		numPixels = HD_NUMCOMPONENTPIXELS_QUADHD;
		linePitch = HD_NUMCOMPONENTPIXELS_QUADHD;
		numLines = HD_NUMLINES_4K;
		break;

	case NTV2_FBF_24BIT_RGB:
	case NTV2_FBF_24BIT_BGR:
		numPixels = HD_NUMCOMPONENTPIXELS_QUADHD;
		linePitch = RGB24LINEPITCH_3840;
		numLines = HD_NUMLINES_4K;
		break;

	case NTV2_FBF_48BIT_RGB:
		numPixels = HD_NUMCOMPONENTPIXELS_QUADHD;
		linePitch = RGB48LINEPITCH_3840;
		numLines = HD_NUMLINES_4K;
		break;

		// Formats not yet supported, kill warning
	case NTV2_FBF_10BIT_YCBCR_DPX:		
	case NTV2_FBF_8BIT_DVCPRO:
	case NTV2_FBF_8BIT_QREZ:
	case NTV2_FBF_8BIT_HDV:
	case NTV2_FBF_10BIT_YCBCRA: 
	case NTV2_FBF_PRORES:
	case NTV2_FBF_PRORES_DVCPRO:
	case NTV2_FBF_PRORES_HDV:
	case NTV2_FBF_10BIT_ARGB:
	case NTV2_FBF_16BIT_ARGB:
	case NTV2_FBF_NUMFRAMEBUFFERFORMATS:
	default:
		numPixels = 0;
		linePitch = 0;
		numLines = 0;
		break;
	}
}

void CNTV2TestPattern::AdjustFor4096x2160(ULWord& numPixels,ULWord& linePitch, ULWord& numLines)
{
	switch ( _fbFormat )
	{	
	case NTV2_FBF_8BIT_YCBCR:
	case NTV2_FBF_8BIT_YCBCR_YUY2:
		numPixels = HD_NUMCOMPONENTPIXELS_4K;
		linePitch = HD_NUMCOMPONENTPIXELS_4K*2/4;
		numLines = HD_NUMLINES_4K;
		break;

	case NTV2_FBF_10BIT_YCBCR:
		numPixels = HD_NUMCOMPONENTPIXELS_4K;
		linePitch = HD_YCBCRLINEPITCH_2K*2;
		numLines = HD_NUMLINES_4K;
		break; 

	case NTV2_FBF_ARGB:
	case NTV2_FBF_RGBA:
	case NTV2_FBF_ABGR:
	case NTV2_FBF_10BIT_RGB:
	case NTV2_FBF_10BIT_RGB_PACKED:
	case NTV2_FBF_10BIT_DPX:
	case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
		numPixels = HD_NUMCOMPONENTPIXELS_4K;
		linePitch = HD_NUMCOMPONENTPIXELS_4K;
		numLines = HD_NUMLINES_4K;
		break;

	case NTV2_FBF_24BIT_RGB:
	case NTV2_FBF_24BIT_BGR:
		numPixels = HD_NUMCOMPONENTPIXELS_4K;
		linePitch = RGB24LINEPITCH_4096;
		numLines = HD_NUMLINES_4K;
		break;

	case NTV2_FBF_48BIT_RGB:
		numPixels = HD_NUMCOMPONENTPIXELS_4K;
		linePitch = RGB48LINEPITCH_4096;
		numLines = HD_NUMLINES_4K;
		break;

		// Formats not yet supported, kill warning
	case NTV2_FBF_10BIT_YCBCR_DPX:		
	case NTV2_FBF_8BIT_DVCPRO:
	case NTV2_FBF_8BIT_QREZ:
	case NTV2_FBF_8BIT_HDV:
	case NTV2_FBF_10BIT_YCBCRA: 
	case NTV2_FBF_PRORES:
	case NTV2_FBF_PRORES_DVCPRO:
	case NTV2_FBF_PRORES_HDV:
	case NTV2_FBF_10BIT_ARGB:
	case NTV2_FBF_16BIT_ARGB:
	case NTV2_FBF_NUMFRAMEBUFFERFORMATS:
	default:
		numPixels = 0;
		linePitch = 0;
		numLines = 0;
		break;
	}
}

#ifdef AJAMac		// 'til proven OK (or useful) for non-Mac users...
	// download a Mac ARGB picture to the "current PCI" frame
void CNTV2TestPattern::DownloadRGBPicture(char *pSrc, ULWord srcWidthPixels, ULWord srcHeightPixels, ULWord srcRowBytes)
{
	ULWord* baseAddress=0;

		// this only works as an ARGB frame
	SetTestPatternFrameBufferFormat(NTV2_FBF_RGBA);
	
		// get current frame buffer size
	NTV2Standard standard;
	GetStandard(&standard);
	NTV2FormatDescriptor formatDescriptor = GetFormatDescriptor(standard, _fbFormat);
	ULWord numPixels = formatDescriptor.numPixels;
	ULWord linePitch = formatDescriptor.linePitch;		// note: linePitch is in ULWords, not bytes!
	ULWord numLines  = formatDescriptor.numLines;

// Kludge for now..... (2K isn't part of the table yet)
	NTV2FrameGeometry fg;
	GetFrameGeometry(&fg);
	if ( fg == NTV2_FG_2048x1080)
	{
		AdjustFor2048x1080(numPixels,linePitch);
	}
	
	// determine where we're going to render the frame to
	ULWord *hostBuff = NULL;
	ULWord *currentAddress;
	if (_clientDownloadBuffer)
	{
		currentAddress = (ULWord *)_clientDownloadBuffer;
	}
	else if (_bDMAtoBoard)
	{
		hostBuff = new ULWord[linePitch * numLines];			// render the test pattern to a host buffer, then DMA it
		assert(hostBuff);
		currentAddress = hostBuff;
	}
	else
	{
		GetBaseAddress(_channel, &baseAddress);
		assert (baseAddress);
		currentAddress = baseAddress;
	}
	
		// if the source frame is bigger (in either dimension) than the frame buffer, clip it
	if (srcWidthPixels > numPixels)
		srcWidthPixels = numPixels;
		
	if (srcHeightPixels > numLines)
		srcHeightPixels = numLines;
		
		// if the source frame is smaller than the frame buffer (in either dimension), center it by padding with black
	ULWord topPad = 0, bottomPad = 0, leftPad = 0, rightPad = 0;
	
	if (srcWidthPixels < numPixels)
	{
		leftPad = (numPixels - srcWidthPixels) / 2;
		rightPad = numPixels - srcWidthPixels - leftPad;
	}
	
	if (srcHeightPixels < numLines)
	{
		topPad   = (numLines - srcHeightPixels) / 2;
		bottomPad = numLines - srcHeightPixels - topPad;
	}
	
		// fill "topPad" space with black
	bzero(currentAddress, (linePitch * topPad * 4) );
	currentAddress += (linePitch * topPad);

		// copy the picture
	UByte *pixelPtr;
	for ( UWord lineCount = 0; lineCount < srcHeightPixels; lineCount++ )
	{
		pixelPtr = (UByte *) currentAddress;
		
			// fill "leftPad" with black
		bzero(pixelPtr, (leftPad * 4) );
		pixelPtr += (leftPad * 4);
		
			// copy picture
		CopyMemory((ULWord *)pixelPtr, (ULWord *)pSrc, srcWidthPixels*4);
		pixelPtr += srcWidthPixels*4;
		
			// fill "rightPad" with black
		bzero(pixelPtr, (rightPad * 4) );
		
			// beginning of next line
		currentAddress += linePitch;		// dest
		pSrc += srcRowBytes;				// src
	}	

		// fill "bottomPad" space with black
	bzero(currentAddress, (linePitch * bottomPad) );
	
	
		// DMA the resulting frame to Kona memory
	if (_bDMAtoBoard && hostBuff != NULL)
	{
			// kind of a kludge - we're still assuming the current PIO frame is the frame we want to download to...
		ULWord pciAccessFrame;
		GetPCIAccessFrame (_channel, &pciAccessFrame);
		if ( pciAccessFrame > GetNumFrameBuffers())
			pciAccessFrame = 0;
			
		DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE, pciAccessFrame, (ULWord *)hostBuff, (numLines * linePitch * 4), true);
		
		delete [] hostBuff;
	}

	SetDualLinkOutputEnable(_dualLinkOutputEnable );
	SetFrameBufferFormat(_channel,NTV2_FBF_RGBA);
	SetFrameBufferOrientation(_channel,NTV2_FRAMEBUFFER_ORIENTATION_TOPDOWN);
#ifndef AJAMac
	NTV2BoardID boardID = GetBoardID();
	if ( (NTV2BoardNeedsRoutingSetup(boardID)) && _autoRouteOnXena2)
	{
		CNTV2SignalRouter router;
		BuildRoutingTableForOutput(router,_channel,_fbFormat,false,false,false);
		ApplySignalRoute(router,router.getCurrentIndex());
	}
#endif

	FlipFlopPage(_channel);
}	
#endif	//	defined (AJAMac)
