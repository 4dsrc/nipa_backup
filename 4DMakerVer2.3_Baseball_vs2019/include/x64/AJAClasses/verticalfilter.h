/////////////////////////////////////////////////////////////////////////////
// VerticalFilter.h
//
// Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
// Vertically filters interlace video with
// 1/4,1/2,1/4 filter.
/////////////////////////////////////////////////////////////////////////////

#ifndef VERTICALFILTER_H
#define VERTICALFILTER_H

#include "ajaexport.h"
#include "ajatypes.h"
#include "videodefines.h"
#include "fixed.h"


AJAExport
void VerticalFilterLine(RGBAlphaPixel * topLine, 
								 RGBAlphaPixel * midLine, 
								 RGBAlphaPixel * bottomLine,
								 RGBAlphaPixel * destLine,
								 LWord numPixels );

AJAExport
void FieldInterpolateLine(RGBAlphaPixel * topLine, 
						  RGBAlphaPixel * bottomLine,
						  RGBAlphaPixel * destLine,
						  LWord numPixels );

#endif
