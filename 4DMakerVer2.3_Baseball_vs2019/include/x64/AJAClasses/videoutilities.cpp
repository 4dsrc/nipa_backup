//---------------------------------------------------------------------------------------------------------------------
//	videoutils.cpp
//
//	Copyright (C) 2010 AJA Video Systems, Inc.  Proprietary and Confidential information.  All rights reserved.
//---------------------------------------------------------------------------------------------------------------------

#include <string.h>
#include "transcode.h"
#include "videoutilities.h"
#include "ntv2utils.h"

uint32_t CalcRowBytesForFormat(NTV2FrameBufferFormat format, uint32_t width)
{
	int rowBytes = 0;

	switch (format)
	{
	case NTV2_FBF_8BIT_YCBCR:
	case NTV2_FBF_8BIT_YCBCR_YUY2:	
		rowBytes = width * 2;
		break;

	case NTV2_FBF_10BIT_YCBCR:	
	case NTV2_FBF_10BIT_YCBCR_DPX:
		rowBytes = (( width % 48 == 0 ) ? width : (((width / 48 ) + 1) * 48)) * 8 / 3;
		break;
	
	case NTV2_FBF_10BIT_RGB:
	case NTV2_FBF_10BIT_DPX:
	case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
	case NTV2_FBF_10BIT_RGB_PACKED:
	case NTV2_FBF_ARGB:	
	case NTV2_FBF_RGBA:
	case NTV2_FBF_ABGR:
		rowBytes = width * 4;
		break;

	case NTV2_FBF_24BIT_RGB:
	case NTV2_FBF_24BIT_BGR:
		rowBytes = width * 3;
		break;

 	case NTV2_FBF_8BIT_DVCPRO:
 		rowBytes = width * 2/4;
		break;

	case NTV2_FBF_48BIT_RGB:
		rowBytes = width * 6;
		break;
		
	default:
		// TO DO.....add more
		break;

	}

	return rowBytes;
}

// UnPack10BitYCbCrBuffer
// UnPack 10 Bit YCbCr Data to 16 bit Word per component
void UnPack10BitYCbCrBuffer( uint32_t* packedBuffer, uint16_t* ycbcrBuffer, uint32_t numPixels )
{
	for (  uint32_t sampleCount = 0, dataCount = 0; 
		sampleCount < (numPixels*2) ; 
		sampleCount+=3,dataCount++ )
	{
		ycbcrBuffer[sampleCount]   =  packedBuffer[dataCount]&0x3FF;  
		ycbcrBuffer[sampleCount+1] = (packedBuffer[dataCount]>>10)&0x3FF;  
		ycbcrBuffer[sampleCount+2] = (packedBuffer[dataCount]>>20)&0x3FF;  

	}
}

// PackTo10BitYCbCrBuffer
// Pack 16 bit Word per component to 10 Bit YCbCr Data 
void PackTo10BitYCbCrBuffer( uint16_t *ycbcrBuffer, uint32_t *packedBuffer,uint32_t numPixels )
{
	for ( uint32_t inputCount=0, outputCount=0; 
		inputCount < (numPixels*2);
		outputCount += 4,inputCount += 12 )
	{
		packedBuffer[outputCount]   = ycbcrBuffer[inputCount+0] + (ycbcrBuffer[inputCount+1]<<10) + (ycbcrBuffer[inputCount+2]<<20);
		packedBuffer[outputCount+1] = ycbcrBuffer[inputCount+3] + (ycbcrBuffer[inputCount+4]<<10) + (ycbcrBuffer[inputCount+5]<<20);
		packedBuffer[outputCount+2] = ycbcrBuffer[inputCount+6] + (ycbcrBuffer[inputCount+7]<<10) + (ycbcrBuffer[inputCount+8]<<20);
		packedBuffer[outputCount+3] = ycbcrBuffer[inputCount+9] + (ycbcrBuffer[inputCount+10]<<10) + (ycbcrBuffer[inputCount+11]<<20);
	}
}

void MakeUnPacked10BitYCbCrBuffer( uint16_t* buffer, uint16_t Y , uint16_t Cb , uint16_t Cr,uint32_t numPixels )
{
	// assumes lineData is large enough for numPixels
	for ( uint32_t count = 0; count < numPixels*2; count+=4 )
	{
		buffer[count] = Cb;
		buffer[count+1] = Y;
		buffer[count+2] = Cr;
		buffer[count+3] = Y;
	}	
}


// ConvertLineTo8BitYCbCr
// 10 Bit YCbCr to 8 Bit YCbCr
void ConvertLineTo8BitYCbCr(uint16_t * ycbcr10BitBuffer, uint8_t * ycbcr8BitBuffer,	uint32_t numPixels)
{
	for ( uint32_t pixel=0;pixel<numPixels*2;pixel++)
	{
		ycbcr8BitBuffer[pixel] = ycbcr10BitBuffer[pixel]>>2;
	}

}

//***********************************************************************************************************

// ConvertUnpacked10BitYCbCrToPixelFormat()
//		Converts a line of "unpacked" 10-bit Y/Cb/Cr pixels into a "packed" line in the pixel format
//	for the current frame buffer format.
void ConvertUnpacked10BitYCbCrToPixelFormat(uint16_t *unPackedBuffer, uint32_t *packedBuffer, uint32_t numPixels, NTV2FrameBufferFormat pixelFormat,
											bool bUseSmpteRange, bool bAlphaFromLuma)
{
	bool  bIsSD = false;
	if(numPixels < 1280)
		bIsSD = true;

	switch(pixelFormat) 
	{
		case NTV2_FBF_10BIT_YCBCR:
			PackTo10BitYCbCrBuffer(unPackedBuffer, packedBuffer, numPixels);
			break;

		case NTV2_FBF_10BIT_YCBCR_DPX:
			RePackLineDataForYCbCrDPX(packedBuffer, CalcRowBytesForFormat(NTV2_FBF_10BIT_YCBCR_DPX, numPixels));
			break;

		case NTV2_FBF_8BIT_YCBCR:
			ConvertLineTo8BitYCbCr(unPackedBuffer,(uint8_t*)packedBuffer, numPixels);
			break;

		case NTV2_FBF_8BIT_YCBCR_YUY2:
			ConvertLineTo8BitYCbCr(unPackedBuffer,(uint8_t*)packedBuffer, numPixels);
			Convert8BitYCbCrToYUY2((uint8_t*)packedBuffer, numPixels);
			break;
			
		case NTV2_FBF_10BIT_RGB:
			ConvertLineto10BitRGB(unPackedBuffer,(RGBAlpha10BitPixel*)packedBuffer,numPixels, bIsSD, bUseSmpteRange);
			PackRGB10BitFor10BitRGB((RGBAlpha10BitPixel*)packedBuffer,numPixels);
			break;

		case NTV2_FBF_10BIT_RGB_PACKED:
			ConvertLineto10BitRGB(unPackedBuffer, (RGBAlpha10BitPixel*)packedBuffer, numPixels, bIsSD, bUseSmpteRange);
			PackRGB10BitFor10BitRGBPacked((RGBAlpha10BitPixel*)packedBuffer, numPixels);
			break;
			
		case NTV2_FBF_10BIT_DPX:
			ConvertLineto10BitRGB(unPackedBuffer,(RGBAlpha10BitPixel*)packedBuffer,numPixels, bIsSD, bUseSmpteRange);
			PackRGB10BitFor10BitDPX((RGBAlpha10BitPixel*)packedBuffer,numPixels);
			break;

		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
			ConvertLineto10BitRGB(unPackedBuffer,(RGBAlpha10BitPixel*)packedBuffer,numPixels, bIsSD, bUseSmpteRange);
			PackRGB10BitFor10BitDPX((RGBAlpha10BitPixel*)packedBuffer,numPixels, false);
			break;
		
		case NTV2_FBF_ARGB:
			ConvertLinetoRGB(unPackedBuffer,(RGBAlphaPixel*)packedBuffer,numPixels, bIsSD, bUseSmpteRange, bAlphaFromLuma);
			break;
		
		case NTV2_FBF_RGBA:
			ConvertLinetoRGB(unPackedBuffer,(RGBAlphaPixel*)packedBuffer,numPixels, bIsSD, bUseSmpteRange, bAlphaFromLuma);
			ConvertARGBYCbCrToRGBA((UByte*)packedBuffer,numPixels);
			break;
			
		case NTV2_FBF_ABGR:
			ConvertLinetoRGB(unPackedBuffer,(RGBAlphaPixel*)packedBuffer,numPixels, bIsSD, bUseSmpteRange, bAlphaFromLuma);
			ConvertARGBYCbCrToABGR((UByte*)packedBuffer,numPixels);
			break;

		case NTV2_FBF_24BIT_BGR:
			ConvertLinetoRGB(unPackedBuffer,(RGBAlphaPixel*)packedBuffer,numPixels, bIsSD, bUseSmpteRange);
			ConvertARGBToBGR((UByte*)packedBuffer, (UByte*)packedBuffer, numPixels);
			break;

		case NTV2_FBF_24BIT_RGB:
			ConvertLinetoRGB(unPackedBuffer,(RGBAlphaPixel*)packedBuffer,numPixels, bIsSD, bUseSmpteRange);
			ConvertARGBToRGB((UByte*)packedBuffer, (UByte*)packedBuffer, numPixels);
			break;

		case NTV2_FBF_48BIT_RGB:
			ConvertLineto16BitRGB(unPackedBuffer, (RGBAlpha16BitPixel*)packedBuffer, numPixels, bIsSD, bUseSmpteRange);
			Convert16BitARGBTo16BitRGB((RGBAlpha16BitPixel*)packedBuffer, (UWord*)packedBuffer, numPixels);
		
		default:
			// TO DO: add all other formats.

			break;
	}
}

// MaskUnPacked10BitYCbCrBuffer
// Mask Data In place based on signalMask
void MaskUnPacked10BitYCbCrBuffer( uint16_t* ycbcrUnPackedBuffer, uint16_t signalMask , uint32_t numPixels )
{
	uint32_t pixelCount;

	// Not elegant but fairly fast.
	switch ( signalMask )
	{
	case NTV2_SIGNALMASK_NONE:          // Output Black
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrUnPackedBuffer[pixelCount]   = CCIR601_10BIT_CHROMAOFFSET;     // Cb
			ycbcrUnPackedBuffer[pixelCount+1] = CCIR601_10BIT_BLACK;            // Y
			ycbcrUnPackedBuffer[pixelCount+2] = CCIR601_10BIT_CHROMAOFFSET;     // Cr
			ycbcrUnPackedBuffer[pixelCount+3] = CCIR601_10BIT_BLACK;            // Y
		}
		break;
	case NTV2_SIGNALMASK_Y:
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrUnPackedBuffer[pixelCount]   = CCIR601_10BIT_CHROMAOFFSET;     // Cb
			ycbcrUnPackedBuffer[pixelCount+2] = CCIR601_10BIT_CHROMAOFFSET;     // Cr
		}

		break;
	case NTV2_SIGNALMASK_Cb:
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrUnPackedBuffer[pixelCount+1] = CCIR601_10BIT_BLACK;            // Y
			ycbcrUnPackedBuffer[pixelCount+2] = CCIR601_10BIT_CHROMAOFFSET;     // Cr
			ycbcrUnPackedBuffer[pixelCount+3] = CCIR601_10BIT_BLACK;            // Y
		}

		break;
	case NTV2_SIGNALMASK_Y + NTV2_SIGNALMASK_Cb:
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrUnPackedBuffer[pixelCount+2] = CCIR601_10BIT_CHROMAOFFSET;     // Cr
		}

		break; 

	case NTV2_SIGNALMASK_Cr:
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrUnPackedBuffer[pixelCount]   = CCIR601_10BIT_CHROMAOFFSET;     // Cb
			ycbcrUnPackedBuffer[pixelCount+1] = CCIR601_10BIT_BLACK;            // Y
			ycbcrUnPackedBuffer[pixelCount+3] = CCIR601_10BIT_BLACK;            // Y
		}


		break;
	case NTV2_SIGNALMASK_Y + NTV2_SIGNALMASK_Cr:
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrUnPackedBuffer[pixelCount]   = CCIR601_10BIT_CHROMAOFFSET;     // Cb
		}


		break; 
	case NTV2_SIGNALMASK_Cb + NTV2_SIGNALMASK_Cr:
		for ( pixelCount = 0; pixelCount < (numPixels*2); pixelCount += 4 )
		{
			ycbcrUnPackedBuffer[pixelCount+1] = CCIR601_10BIT_BLACK;            // Y
			ycbcrUnPackedBuffer[pixelCount+3] = CCIR601_10BIT_BLACK;            // Y
		}


		break; 
	case NTV2_SIGNALMASK_Y + NTV2_SIGNALMASK_Cb + NTV2_SIGNALMASK_Cr:
		// Do nothing
		break; 
	}

}



//--------------------------------------------------------------------------------------------------------------------
//	StackQuadrants()
//
//	Take a 4K source, cut it into 4 quandrants and stack it into the destination. Also handle cases where
//	where source/destination rowBytes/widths are mismatched (eg 4096 -> 3840)
//--------------------------------------------------------------------------------------------------------------------
void StackQuadrants(uint8_t* pSrc, uint32_t srcWidth, uint32_t srcHeight, uint32_t srcRowBytes, 
					 uint8_t* pDst)
{
	(void) srcWidth;
	uint32_t dstSample;
	uint32_t srcSample;
	uint32_t copyRowBytes = srcRowBytes/2;
	uint32_t copyHeight = srcHeight/2;
	uint32_t dstRowBytes = copyRowBytes;
	uint32_t dstHeight = srcHeight/2;
	//uint32_t dstWidth = srcWidth/2;

	// rowbytes for left hand side quadrant
	uint32_t srcLHSQuadrantRowBytes = srcRowBytes/2;

	for (uint32_t quadrant=0; quadrant<4; quadrant++)
	{
		// starting point for source quadrant
		switch (quadrant)
		{
		default:
		case 0: srcSample = 0; break;													// quadrant 0, upper left
		case 1: srcSample = srcLHSQuadrantRowBytes; break;								// quadrant 1, upper right
		case 2: srcSample = (srcRowBytes*copyHeight); break;							// quadrant 2, lower left
		case 3: srcSample = (srcRowBytes*copyHeight) + srcLHSQuadrantRowBytes; break;	// quadrant 3, lower right
		}

		// starting point for destination stack
		dstSample = quadrant * dstRowBytes * dstHeight;

		for (uint32_t row=0; row<copyHeight; row++)
		{
			memcpy(&pDst[dstSample], &pSrc[srcSample], copyRowBytes);
			dstSample += dstRowBytes;
			srcSample += srcRowBytes;
		}
	}
}

// Copy a quater-sized quadrant from a source buffer to a destination buffer
// quad13Offset is almost always zero, but can be used for Quadrants 1, 3 for special offset frame buffers. (e.g. 4096x1080 10Bit YCbCr frame buffers)
void CopyFromQuadrant(uint8_t* srcBuffer, uint32_t srcHeight, uint32_t srcRowBytes, uint32_t srcQuadrant, uint8_t* dstBuffer, uint32_t quad13Offset)
{
	ULWord dstSample = 0;
	ULWord srcSample = 0;
	ULWord dstHeight = srcHeight / 2;
	ULWord dstRowBytes = srcRowBytes / 2;

	// calculate starting point for source of copy, based on source quadrant
	switch (srcQuadrant)
	{
	default:
	case 0: srcSample = 0; break;													// quadrant 0, upper left
	case 1: srcSample = dstRowBytes - quad13Offset; break;							// quadrant 1, upper right
	case 2: srcSample = srcRowBytes*dstHeight; break;								// quadrant 2, lower left
	case 3: srcSample = srcRowBytes*dstHeight + dstRowBytes - quad13Offset; break;	// quadrant 3, lower right
	}

	// for each row
	for (ULWord i=0; i<dstHeight; i++)
	{
		memcpy(&dstBuffer[dstSample], &srcBuffer[srcSample], dstRowBytes);
		dstSample += dstRowBytes;
		srcSample += srcRowBytes;
	}
}

// Copy a source buffer to a quadrant of a 4x-sized destination buffer
// quad13Offset is almost always zero, but can be used for Quadrants 1, 3 for special offset frame buffers. (e.g. 4096x1080 10Bit YCbCr frame buffers)
void CopyToQuadrant(uint8_t* srcBuffer, uint32_t srcHeight, uint32_t srcRowBytes, uint32_t dstQuadrant, uint8_t* dstBuffer, uint32_t quad13Offset)
{
	ULWord dstSample = 0;
	ULWord srcSample = 0;
	ULWord dstRowBytes = srcRowBytes * 2;

	// calculate starting point for destination of copy, based on destination quadrant
	switch (dstQuadrant)
	{
	default:
	case 0: dstSample = 0; break;													// quadrant 0, upper left
	case 1: dstSample = srcRowBytes - quad13Offset; break;							// quadrant 1, upper right
	case 2: dstSample = dstRowBytes*srcHeight; break;								// quadrant 2, lower left
	case 3: dstSample = dstRowBytes*srcHeight + srcRowBytes - quad13Offset; break;	// quadrant 3, lower right
	}

	// for each row
	for (ULWord i=0; i<srcHeight; i++)
	{
		memcpy(&dstBuffer[dstSample], &srcBuffer[srcSample], srcRowBytes);
		dstSample += dstRowBytes;
		srcSample += srcRowBytes;
	}
}

