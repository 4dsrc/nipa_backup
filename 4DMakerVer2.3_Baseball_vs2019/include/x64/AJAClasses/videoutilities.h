/**
	@file		videoutilities.h
	@brief		Interface to the NTV2 video utilities.
	@copyright	(C) 2010-2014 AJA Video Systems, Inc.  Proprietary and Confidential information.  All rights reserved.
**/

#ifndef AJA_VIDEOUTILS_H
#define AJA_VIDEOUTILS_H

#ifndef MSWindows
	#include "stdint.h"
#endif
#include "ajatypes.h"
#include "ntv2enums.h"
#include "videodefines.h"

#define DEFAULT_PATT_GAIN  0.9		// some patterns pay attention to this...
#define HD_NUMCOMPONENTPIXELS_2K  2048
#define HD_NUMCOMPONENTPIXELS_1080_2K  2048
#define HD_NUMCOMPONENTPIXELS_1080  1920
#define CCIR601_10BIT_BLACK  64
#define CCIR601_10BIT_WHITE  940
#define CCIR601_10BIT_CHROMAOFFSET  512

#define CCIR601_8BIT_BLACK  16
#define CCIR601_8BIT_WHITE  235
#define CCIR601_8BIT_CHROMAOFFSET  128

// line pitch is in bytes.
#define FRAME_0_BASE (0x0)
#define FRAME_1080_10BIT_LINEPITCH (1280*4)
#define FRAME_1080_8BIT_LINEPITCH (1920*2)
#define FRAME_QUADHD_10BIT_SIZE (FRAME_1080_10BIT_LINEPITCH*2160)
#define FRAME_QUADHD_8BIT_SIZE (FRAME_1080_8BIT_LINEPITCH*2160)
#define FRAME_BASE(frameNumber,frameSize) (frameNumber*frameSize) 

uint32_t CalcRowBytesForFormat(NTV2FrameBufferFormat format, uint32_t width);
void UnPack10BitYCbCrBuffer(uint32_t* packedBuffer, uint16_t* ycbcrBuffer, uint32_t numPixels);
void PackTo10BitYCbCrBuffer(uint16_t *ycbcrBuffer, uint32_t *packedBuffer,uint32_t numPixels);
void MakeUnPacked10BitYCbCrBuffer(uint16_t* buffer, uint16_t Y , uint16_t Cb , uint16_t Cr,uint32_t numPixels);
void ConvertLineTo8BitYCbCr(uint16_t * ycbcr10BitBuffer, uint8_t * ycbcr8BitBuffer,	uint32_t numPixels);
void ConvertUnpacked10BitYCbCrToPixelFormat(uint16_t *unPackedBuffer, uint32_t *packedBuffer, uint32_t numPixels, NTV2FrameBufferFormat pixelFormat,
											bool bUseSmpteRange=false, bool bAlphaFromLuma=false);
void MaskUnPacked10BitYCbCrBuffer(uint16_t* ycbcrUnPackedBuffer, uint16_t signalMask , uint32_t numPixels);
void StackQuadrants(uint8_t* pSrc, uint32_t srcWidth, uint32_t srcHeight, uint32_t srcRowBytes, uint8_t* pDst);
void CopyFromQuadrant(uint8_t* srcBuffer, uint32_t srcHeight, uint32_t srcRowBytes, uint32_t srcQuadrant, uint8_t* dstBuffer,  uint32_t quad13Offset=0);
void CopyToQuadrant(uint8_t* srcBuffer, uint32_t srcHeight, uint32_t srcRowBytes, uint32_t dstQuadrant, uint8_t* dstBuffer, uint32_t quad13Offset=0);
#endif
