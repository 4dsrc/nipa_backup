/*
  Module: CNTV2FrameDelay.h

  Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.

  For a description of how to use this class please refer to NTV2CaptureToDisk.cpp
*/


#ifndef _NTV2FRAMEDELAY_H
#define _NTV2FRAMEDELAY_H

#include "AJATypes.h"
#include "NTV2Enums.h"
#include "ntv2active.h"
//#include "ddntv2.h"
#include "ntv2card.h"


class CNTV2FrameDelay : public ActiveObject
{
public:
	CNTV2FrameDelay(UWord boardNumber=0,
					NTV2Channel channel=NTV2_CHANNEL1,
					UWord delay=2,
					UWord numFrames=(NTV2_MIN_FRAMEBUFFERS-2),
		      bool displayErrorMessage=false, 
			  ULWord shareMode = FILE_SHARE_READ | FILE_SHARE_WRITE,
			  DWORD dwCardTypes = BOARDTYPE_AS_COMPILED
			)
		: _boardNumber(boardNumber),
		  _channel(channel),
		  _delay(delay),
		  _numFrames(numFrames),
		  _active(true)

	{
		_thread.Resume ();
		InitBoardInfo();
        
	}

    ~CNTV2FrameDelay() { Kill(); }; 
	void Quit();
	bool Active() { return _active; }

	void SetDelay(UWord delay ); 

	void SetNumFrames(UWord numFrames);
	UWord GetNumFrames();

protected:
    virtual void InitThread ();
    virtual void Loop ();
    virtual void FlushThread () {_active=false;}

	UWord						_boardNumber;
	NTV2Channel					_channel;
	UWord						_delay;
	UWord						_numFrames;
	bool						_active;
		
private:
	void InitBoardInfo();

};

#endif
