/*
 *  ntv2driverinterface.cpp
 *
 *	Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2011 AJA Video Systems, Inc.
 *	Proprietary and Confidential information.
 *
 */
#include "ajatypes.h"
#include "ntv2enums.h"
#include "ntv2driverinterface.h"
#include "ntv2devicefeatures.h"
#include "ntv2nubaccess.h"

#include <string.h>
#include <assert.h>
#include <iostream>
#include <map>

using namespace std;

typedef map <INTERRUPT_ENUMS, string>	InterruptEnumStringMap;
static InterruptEnumStringMap			gInterruptNames;

class DriverInterfaceGlobalInitializer
{
	public:
		DriverInterfaceGlobalInitializer ()
		{
			gInterruptNames [eOutput1]				= "eOutput1";
			gInterruptNames [eInterruptMask]		= "eInterruptMask";
			gInterruptNames [eInput1]				= "eInput1";
			gInterruptNames [eInput2]				= "eInput2";
			gInterruptNames [eAudio]				= "eAudio";
			gInterruptNames [eAudioInWrap]			= "eAudioInWrap";
			gInterruptNames [eAudioOutWrap]			= "eAudioOutWrap";
			gInterruptNames [eDMA1]					= "eDMA1";
			gInterruptNames [eDMA2]					= "eDMA2";
			gInterruptNames [eDMA3]					= "eDMA3";
			gInterruptNames [eDMA4]					= "eDMA4";
			gInterruptNames [eChangeEvent]			= "eChangeEvent";
			gInterruptNames [eGetIntCount]			= "eGetIntCount";
			gInterruptNames [eWrapRate]				= "eWrapRate";
			gInterruptNames [eUart1Tx]				= "eUart1Tx";
			gInterruptNames [eUart1Rx]				= "eUart1Rx";
			gInterruptNames [eAuxVerticalInterrupt]	= "eAuxVerticalInterrupt";
			gInterruptNames [ePushButtonChange]		= "ePushButtonChange";
			gInterruptNames [eLowPower]				= "eLowPower";
			gInterruptNames [eDisplayFIFO]			= "eDisplayFIFO";
			gInterruptNames [eSATAChange]			= "eSATAChange";
			gInterruptNames [eTemp1High]			= "eTemp1High";
			gInterruptNames [eTemp2High]			= "eTemp2High";
			gInterruptNames [ePowerButtonChange]	= "ePowerButtonChange";
			gInterruptNames [eInput3]				= "eInput3";
			gInterruptNames [eInput4]				= "eInput4";
			gInterruptNames [eUart2Tx]				= "eUart2Tx";
			gInterruptNames [eUart2Rx]				= "eUart2Rx";
			gInterruptNames [eHDMIRxV2HotplugDetect]= "eHDMIRxV2HotplugDetect";
			gInterruptNames [eInput5]				= "eInput5";
			gInterruptNames [eInput6]				= "eInput6";
			gInterruptNames [eInput7]				= "eInput7";
			gInterruptNames [eInput8]				= "eInput8";
			gInterruptNames [eInterruptMask2]		= "eInterruptMask2";
			gInterruptNames [eOutput2]				= "eOutput2";
			gInterruptNames [eOutput3]				= "eOutput3";
			gInterruptNames [eOutput4]				= "eOutput4";
			gInterruptNames [eOutput5]				= "eOutput5";
			gInterruptNames [eOutput6]				= "eOutput6";
			gInterruptNames [eOutput7]				= "eOutput7";
			gInterruptNames [eOutput8]				= "eOutput8";
		}
};

static DriverInterfaceGlobalInitializer	gInitializerSingleton;


CNTV2DriverInterface::CNTV2DriverInterface ()
	:	_boardNumber					(0),
		_boardOpened					(false),
		_boardType						(DEVICETYPE_NTV2),
		_boardID						(DEVICE_ID_NOTFOUND),
		_displayErrorMessage			(false),
		_pciSlot						(0),
		_programStatus					(0),
		_quadFrameEnabled				(false),
		_sockfd							(-1),
		_remoteHandle					(INVALID_NUB_HANDLE),
		_nubProtocolVersion				(ntv2NubProtocolVersionNone),
		_pFrameBaseAddress				(NULL),
		_pCh1FrameBaseAddress			(NULL),
		_pCh2FrameBaseAddress			(NULL),
		_pRegisterBaseAddress			(NULL),
		_pRegisterBaseAddressLength		(0),
		_pFS1FPGARegisterBaseAddress	(NULL),
		_pXena2FlashBaseAddress			(NULL),
		_ulChannelTwoOffset				(0),
		_ulNumFrameBuffers				(0),
		_ulFrameBufferSize				(0)
{
	::memset (&_sockAddr, 0, sizeof(struct sockaddr_in));
	::memset (mInterruptEventHandles, 0, sizeof (mInterruptEventHandles));
	::memset (mEventCounts, 0, sizeof (mEventCounts));

}	//	constructor


CNTV2DriverInterface::~CNTV2DriverInterface ()
{
	if (_sockfd != -1)
	{
		#ifdef MSWindows
			closesocket (_sockfd);
		#else
			close (_sockfd);
		#endif
		_sockfd = (AJASocket) -1;
	}
}	//	destructor


bool CNTV2DriverInterface::ConfigureSubscription (bool bSubscribe, INTERRUPT_ENUMS eInterruptType, PULWord & hSubscription)
{
	if (bSubscribe)							//	If subscribing,
		mEventCounts [eInterruptType] = 0;	//		clear this interrupt's event counter
// 	#if defined (_DEBUG)
// 	else
// 		cerr << "## DEBUG:  Unsubscribing '" << gInterruptNames [eInterruptType] << "' (" << eInterruptType << "), " << mEventCounts [eInterruptType] << " event(s) received" << endl;
// 	#endif
	(void) hSubscription;
	return true;

}	//	ConfigureSubscription

bool RetailOpen(UWord inDeviceIndex, bool displayError, NTV2DeviceType eDeviceType, const char *hostname)
{
	return true;
}


bool CNTV2DriverInterface::OpenRemote (UWord boardNumber, bool displayErrorMessage, UWord ulBoardType, const char *hostname)
{
	(void) displayErrorMessage;
	if (!hostname)
	{
		if (_sockfd != -1)
		{
			#ifdef MSWindows
				closesocket (_sockfd);
			#else
				close (_sockfd);
			#endif
		}
		_sockfd = -1;

		return false;
	}

	// If connected and hostname the same, remain open, else close and reconnect.
	if (_sockfd != -1 && !strcmp(hostname, GetHostName()))
	{
		// Already connected to desired host.
	}
	else
	{
		// Disconnect if connected.
		if (_sockfd != -1)
		{
			#ifdef MSWindows
				closesocket (_sockfd);
			#else
				close (_sockfd);
			#endif
		}
		_sockfd = -1;

		// Establish connection
		if(NTV2ConnectToNub(hostname, &_sockfd) >= 0)
		{
			_hostname = hostname;
		}
		else
		{
			if (_sockfd != -1)
			{
				#ifdef MSWindows
					closesocket (_sockfd);
				#else
					close (_sockfd);
				#endif
				_sockfd = -1;
			}
			return false;
		}
	}

	// Open the card on the remote system.
	switch(NTV2OpenRemoteCard(_sockfd, boardNumber, ulBoardType, &_remoteHandle, &_nubProtocolVersion))
	{
		case NTV2_REMOTE_ACCESS_SUCCESS:
			// printf("_remoteHandle came back as 0x%08x\n", _remoteHandle);
			return true;

		case NTV2_REMOTE_ACCESS_CONNECTION_CLOSED:
			#ifdef MSWindows
				closesocket (_sockfd);
			#else
				close (_sockfd);
			#endif
			_sockfd = -1;
			// printf("_remoteHandle came back as 0x%08x\n", _remoteHandle);
			_remoteHandle = (LWord)INVALID_NUB_HANDLE;
			break;

		case NTV2_REMOTE_ACCESS_NOT_CONNECTED:
		case NTV2_REMOTE_ACCESS_OUT_OF_MEMORY:
		case NTV2_REMOTE_ACCESS_SEND_ERR:
		case NTV2_REMOTE_ACCESS_RECV_ERR:
		case NTV2_REMOTE_ACCESS_TIMEDOUT:
		case NTV2_REMOTE_ACCESS_NO_CARD:
		case NTV2_REMOTE_ACCESS_NOT_OPEN_RESP:
		case NTV2_REMOTE_ACCESS_NON_NUB_PKT:
		default:
			// Failed, but don't close connection, can try with another card on same connection.
			// printf("_remoteHandle came back as 0x%08x\n", _remoteHandle);
			_remoteHandle = (LWord)INVALID_NUB_HANDLE;
	}

	return false;
}


bool CNTV2DriverInterface::CloseRemote()
{
	if (_sockfd != -1)
	{
		#ifdef MSWindows
			closesocket(_sockfd);
		#else
			close(_sockfd);
		#endif
		_remoteHandle = (LWord) INVALID_NUB_HANDLE;
		_sockfd = -1;
		_boardOpened = false;
		_nubProtocolVersion = ntv2NubProtocolVersionNone;
		return true;
	}
	// Wasn't open.
	_boardOpened = false;
	return false;
}


CNTV2DriverInterface & CNTV2DriverInterface::operator = (const CNTV2DriverInterface & inRHS)
{
	(void) inRHS;
	assert (false && "These are not assignable");
	return *this;
}	//	operator =


CNTV2DriverInterface::CNTV2DriverInterface (const CNTV2DriverInterface & inObjToCopy)
{
	(void) inObjToCopy;
	assert (false && "These are not copyable");
}	//	copy constructor


// Common remote card read register.  Subclasses have overloaded function
// that does platform-specific read of register on local card.
bool CNTV2DriverInterface::ReadRegister (ULWord inRegisterNumber, ULWord * pOutRegisterValue, ULWord inRegisterMask, ULWord inRegisterShift)
{
	assert( _remoteHandle != INVALID_NUB_HANDLE);

	return !NTV2ReadRegisterRemote(_sockfd,
								_remoteHandle,
								_nubProtocolVersion,
								inRegisterNumber,
								pOutRegisterValue,
								inRegisterMask,
								inRegisterShift);
}


// Common remote card read multiple registers.  Subclasses have overloaded function
// that does platform-specific read of multiple register on local card.
bool CNTV2DriverInterface::ReadRegisterMulti (ULWord numRegs, ULWord *whichRegisterFailed, NTV2ReadWriteRegisterSingle aRegs[])
{
#if 0
	assert( _remoteHandle != INVALID_NUB_HANDLE);
#endif

	if (_remoteHandle != INVALID_NUB_HANDLE)
	{
		return !NTV2ReadRegisterMultiRemote(_sockfd,
								_remoteHandle,
								_nubProtocolVersion,
								numRegs,
								whichRegisterFailed,
								aRegs);
	}

	// TODO: Make platform specific versions that pass the whole shebang
	// down to the drivers which fills in the desired values in a single
	// context switch OR just get them from register set mapped into userspace
	for (ULWord i = 0; i < numRegs; i++)
	{
		if (!ReadRegister(	aRegs[i].registerNumber,
							&aRegs[i].registerValue,
							aRegs[i].registerMask,
							aRegs[i].registerShift)
							)
		{
			*whichRegisterFailed = aRegs[i].registerNumber;
			return false;
		}
	}
	return true;
}

// Remote card GetDriverVersion.  Tested on Mac only, other platforms use
// a virtual register for the driver version.
bool
CNTV2DriverInterface::GetDriverVersion(ULWord *driverVersion)
{
	assert( _remoteHandle != INVALID_NUB_HANDLE);

	return !NTV2GetDriverVersionRemote(	_sockfd,
										_remoteHandle,
										_nubProtocolVersion,
										driverVersion);
}


// Common remote card write register.  Subclasses have overloaded function
// that does platform-specific write of register on local card.
bool
CNTV2DriverInterface::WriteRegister(
	ULWord registerNumber,
	ULWord registerValue,
	ULWord registerMask,
	ULWord registerShift)
{
	assert( _remoteHandle != INVALID_NUB_HANDLE);

	return !NTV2WriteRegisterRemote(_sockfd,
								_remoteHandle,
								_nubProtocolVersion,
								registerNumber,
								registerValue,
								registerMask,
								registerShift);
}

// Common remote card waitforinterrupt.  Subclasses have overloaded function
// that does platform-specific waitforinterrupt on local cards.
bool
CNTV2DriverInterface::WaitForInterrupt (INTERRUPT_ENUMS eInterrupt, ULWord timeOutMs)
{
	assert( _remoteHandle != INVALID_NUB_HANDLE);

	return !NTV2WaitForInterruptRemote(	_sockfd,
										_remoteHandle,
										_nubProtocolVersion,
										eInterrupt,
										timeOutMs);
}

// Common remote card autocirculate.  Subclasses have overloaded function
// that does platform-specific autocirculate on local cards.
bool
CNTV2DriverInterface::AutoCirculate (AUTOCIRCULATE_DATA &autoCircData)
{
	assert( _remoteHandle != INVALID_NUB_HANDLE);

	switch(autoCircData.eCommand)
	{
		case eStartAutoCirc:
		case eAbortAutoCirc:
		case ePauseAutoCirc:
		case eFlushAutoCirculate:
		case eGetAutoCirc:
		case eStopAutoCirc:
			return !NTV2AutoCirculateRemote(_sockfd, _remoteHandle,	_nubProtocolVersion, autoCircData);
		default:	// Others not handled
			return false;
	}
}

// Common remote card DriverGetBitFileInformation.  Subclasses have overloaded function
// that does platform-specific function on local cards.
bool
CNTV2DriverInterface::DriverGetBitFileInformation(
		BITFILE_INFO_STRUCT &bitFileInfo,
		NTV2BitFileType bitFileType)
{
	assert( _remoteHandle != INVALID_NUB_HANDLE);

	return ! NTV2DriverGetBitFileInformationRemote(	_sockfd,
													_remoteHandle,
													_nubProtocolVersion,
													bitFileInfo,
													bitFileType);
}

// Common remote card DriverGetBuildInformation.  Subclasses have overloaded function
// that does platform-specific function on local cards.
bool
CNTV2DriverInterface::DriverGetBuildInformation(
		BUILD_INFO_STRUCT &buildInfo)
{
	assert( _remoteHandle != INVALID_NUB_HANDLE);

	return ! NTV2DriverGetBuildInformationRemote(	_sockfd,
													_remoteHandle,
													_nubProtocolVersion,
													buildInfo);
}


//
// InitMemberVariablesOnOpen
// NOTE _boardID must be set before calling this routine.
void CNTV2DriverInterface::InitMemberVariablesOnOpen(NTV2FrameGeometry frameGeometry,NTV2FrameBufferFormat frameFormat)
{

	_ulChannelTwoOffset = ::NTV2DeviceGetCh2Offset(_boardID);

	_ulFrameBufferSize = ::NTV2DeviceGetFrameBufferSize(_boardID,frameGeometry,frameFormat);
	_ulNumFrameBuffers = ::NTV2DeviceGetNumberFrameBuffers(_boardID,frameGeometry,frameFormat);

	ULWord returnVal1 = false;
	ULWord returnVal2 = false;
	if(::NTV2DeviceCanDo4KVideo(_boardID))
		ReadRegister(kRegGlobalControl2, &returnVal1, kRegMaskQuadMode, kRegShiftQuadMode);
	if(::NTV2DeviceCanDo425Mux(_boardID))
		ReadRegister(kRegGlobalControl2, &returnVal2, kRegMask425FB12, kRegShift425FB12);

	_quadFrameEnabled = (returnVal1 || returnVal2) ? true: false;


    _pFrameBaseAddress = 0;
    // for old KSD and KHD boards
    _pCh1FrameBaseAddress = 0;
    _pCh2FrameBaseAddress = 0;

    _pRegisterBaseAddress = 0;
	_pRegisterBaseAddressLength = 0;

	_pFS1FPGARegisterBaseAddress = 0;
	_pXena2FlashBaseAddress  = 0;

}	//	InitMemberVariablesOnOpen


bool CNTV2DriverInterface::ParseFlashHeader(BITFILE_INFO_STRUCT &bitFileInfo)
{
	ULWord baseAddress = 0;
	ULWord* bitFilePtr =  new ULWord[256/4];
	ULWord dwordSizeCount = 256/4;
	for ( ULWord count = 0; count < dwordSizeCount; count++, baseAddress += 4 )
	{
		WriteRegister(kRegXenaxFlashAddress, baseAddress);
		WriteRegister(kRegXenaxFlashControlStatus, 0x0B);
		bool busy = true;
		do
		{
			ULWord regValue;
			ReadRegister(kRegXenaxFlashControlStatus, &regValue);
			if ( regValue & BIT(8))
				busy = true;
			else
				busy = false;
		} while(busy == true);
		ReadRegister(kRegXenaxFlashDOUT, &bitFilePtr[count]);
	}

	bool headerOK = false;
	UWord fieldLen;
	char *p = (char*)bitFilePtr;
	UByte signature[] = {0xFF,0xFF,0xFF,0xFF,0xAA,0x99,0x55,0x66};
	UByte head13[]   = { 0x00, 0x09, 0x0f, 0xf0, 0x0f, 0xf0, 0x0f, 0xf0, 0x0f, 0xf0, 0x00, 0x00, 0x01 };
	ULWord _numBytes (0);
	std::string _designName, _partName, _date, _time;

	do
	{
		// make sure 1st 13 bytes are what we expect
		if ( strncmp(p, (char*)&head13, 13) != 0 )
			break;

		p += 13;	// skip over header header

		// the next byte should be 'a'
		if (*p++ != 'a')
			break;

		fieldLen = htons(*((UWord *)p));		// the next 2 bytes are the length of the FileName (including /0)

		p += 2;							// now pointing at the beginning of the file name

		_designName = p;					// grab design name

		p += fieldLen;					// skip over file name - now pointing to beginning of 'b' field

		// the next byte should be 'b'
		if (*p++ != 'b')
			break;

		fieldLen = htons(*((UWord *)p));		// the next 2 bytes are the length of the Part Name (including /0)

		p += 2;							// now pointing at the beginning of the part name

		_partName = p;						// grab part name

		p += fieldLen;					// skip over part name - now pointing to beginning of 'c' field

		// the next byte should be 'c'
		if (*p++ != 'c')
			break;

		fieldLen = htons(*((UWord *)p));		// the next 2 bytes are the length of the date string (including /0)

		p += 2;							// now pointing at the beginning of the date string

		_date = p;						// grab date string

		p += fieldLen;					// skip over date string - now pointing to beginning of 'd' field

		// the next byte should be 'd'
		if (*p++ != 'd')
			break;

		fieldLen = htons(*((UWord *)p));		// the next 2 bytes are the length of the time string (including /0)

		p += 2;							// now pointing at the beginning of the time string

		_time = p;						// grab time string

		p += fieldLen;					// skip over time string - now pointing to beginning of 'e' field

		// the next byte should be 'e'
		if (*p++ != 'e')
			break;

		_numBytes = htonl(*((ULWord *)p));		// the next 4 bytes are the length of the raw program data

		if ( _partName[0] == '5' || _partName[0] == '6' || _partName[0] == '7')
		{
			// still waiting for xilinx to explain this fully
			if(_partName[0] == '5' || (_partName[0] == '6' && _partName[1] == 'v'))
				p += 48;							// now pointing at the beginning of the identifier
			else if(_partName[0] == '7' && _partName[1] == 'k')
				p += 48;
			else
				p += 16;
		}
		else
		{
			p += 4;							// now pointing at the beginning of the identifier
		}

		// make sure the sync word is what we expect
		if ( strncmp(p, (char*)&signature, 8) != 0 )
			break;

		// if we made it this far it must be an OK Header - and it is parsed
		headerOK = true;

	} while(0);

	if (headerOK == true)
	{
		strncpy(bitFileInfo.dateStr, _date.c_str(), NTV2_BITFILE_DATETIME_STRINGLENGTH);
		strncpy(bitFileInfo.timeStr, _time.c_str(), NTV2_BITFILE_DATETIME_STRINGLENGTH);
		strncpy(bitFileInfo.designNameStr, _designName.c_str(), NTV2_BITFILE_DESIGNNAME_STRINGLENGTH);
		strncpy(bitFileInfo.partNameStr, _partName.c_str(), NTV2_BITFILE_PARTNAME_STRINGLENGTH);
		bitFileInfo.numBytes = _numBytes;
	}

	delete [] bitFilePtr;

	return headerOK;
}


void CNTV2DriverInterface::BumpEventCount (const INTERRUPT_ENUMS eInterruptType)
{
	mEventCounts [eInterruptType] = mEventCounts [eInterruptType] + 1;

}	//	BumpEventCount

#if !defined (NTV2_DEPRECATE)
NTV2BoardType CNTV2DriverInterface::GetCompileFlag ()
{
	NTV2BoardType eBoardType = BOARDTYPE_UNKNOWN;

#ifdef HDNTV
	eBoardType = BOARDTYPE_HDNTV;
#elif defined KSD
	eBoardType = BOARDTYPE_KSD;
#elif defined KHD
	eBoardType = BOARDTYPE_KHD;
#elif defined XENA2
	eBoardType = BOARDTYPE_AJAXENA2;
#elif defined BORG
	eBoardType = BOARDTYPE_BORG;
#endif

	return eBoardType;
}
#endif	//	!NTV2_DEPRECATE

