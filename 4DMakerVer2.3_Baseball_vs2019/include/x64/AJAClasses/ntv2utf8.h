////////////////////////////////////////////////////////
//
// ntv2utf8.h : Extremely minimal UTF8 support
//
////////////////////////////////////////////////////////
//
// Copyright (C) 2008 AJA Video Systems, Inc.  
// Proprietary and Confidential information. 
//
////////////////////////////////////////////////////////

#ifndef __NTV2UTF8_H
#define __NTV2UTF8_H

#include "ajaexport.h"

AJAExport bool map_utf8_to_codepage437(const char *src, int u8_len, unsigned char *cp437equiv);
AJAExport void strncpyasutf8(char *dest, const char *src, int dest_buf_size);
AJAExport void strncpyasutf8_map_cp437(char *dest, const char *src, int dest_buf_size);

#endif
