/*

  Module : NTV2FrameDelay.cpp

  Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.
  
*/

#include "ntv2framedelay.h"
#include "ntv2Card.h"


void CNTV2FrameDelay::InitThread()
{
    CNTV2Card ntv2Card(_boardNumber);
    
    if ( ntv2Card.BoardOpened() )
    {
		SetNumFrames(ntv2Card.GetNumFrameBuffers());
	}
}


void CNTV2FrameDelay::InitBoardInfo()
{
    CNTV2Card ntv2Card(_boardNumber);
    
    if ( ntv2Card.BoardOpened() )
    {
		SetNumFrames(ntv2Card.GetNumFrameBuffers());
	}
}


void CNTV2FrameDelay::Loop()
{
    
    CNTV2Card ntv2Card(_boardNumber);
    
    if ( ntv2Card.BoardOpened() )
    {
        LWord currentInputFrame = 0;
        LWord currentOutputFrame = 0;
        NTV2Channel outputChannel;
        NTV2ReferenceSource referenceSource;
        ULWord videoProcessingControl;
        ULWord videoProcessingControlCrosspoint;
        ULWord mixerCoefficient;
        bool progressive = true;
		
        ntv2Card.SetMode(_channel,NTV2_MODE_CAPTURE);
        ntv2Card.SetFrameBufferFormat(NTV2_CHANNEL1,NTV2_FBF_10BIT_YCBCR);
        ntv2Card.GetReferenceSource(&referenceSource);
        ntv2Card.ReadVideoProcessingControl(&videoProcessingControl);
        ntv2Card.ReadVideoProcessingControlCrosspoint(&videoProcessingControlCrosspoint);
        ntv2Card.ReadMixerCoefficient(&mixerCoefficient);
        
		ntv2Card.IsProgressiveStandard(&progressive);
        ntv2Card.SubscribeOutputVerticalEvent();
        
        if ( _channel == NTV2_CHANNEL1 )
        {
            outputChannel = NTV2_CHANNEL2;
            ntv2Card.SetReferenceSource(NTV2_REFERENCE_INPUT1);
        }
        else
        {
            outputChannel = NTV2_CHANNEL1;
            ntv2Card.SetReferenceSource(NTV2_REFERENCE_INPUT1); // on a KSD-22, you could use INPUT2 as the ref.
        }
        ntv2Card.SetMode(outputChannel,NTV2_MODE_DISPLAY);
        ntv2Card.SetFrameBufferFormat(outputChannel,NTV2_FBF_10BIT_YCBCR);

        ntv2Card.WriteVideoProcessingControl(0x5);
        ntv2Card.WriteMixerCoefficient(0x10000);
        
        if ( outputChannel == NTV2_CHANNEL1 )
        {
            ntv2Card.WriteVideoProcessingControlCrosspoint(0x0);
        }
        else
        {
            ntv2Card.WriteVideoProcessingControlCrosspoint(0x1);
        }
        
        ntv2Card.SetInputFrame(_channel,currentInputFrame);
        ntv2Card.SetOutputFrame(outputChannel,currentOutputFrame);
        for ( int i=0; i < _delay; i++ )
        {
			if ( progressive == true)
				ntv2Card.WaitForVerticalInterrupt();
			else
				ntv2Card.WaitForFieldID(NTV2_FIELD1);

            ntv2Card.SetInputFrame(_channel,currentInputFrame++);
            ntv2Card.SetOutputFrame(outputChannel,currentOutputFrame++);
        }

        // Set up for Missed-vertical check
        // we have not missed any verticals initially
        ULWord ulwNumVerticalsHappened;
        ULWord ulwNumVerticalsSeen;
        int cMissedVerticals = 0;
        ntv2Card.GetOutputVerticalInterruptCount( &ulwNumVerticalsHappened );
        ntv2Card.SetOutputVerticalEventCount( ulwNumVerticalsHappened );
        ulwNumVerticalsSeen = ulwNumVerticalsHappened; 

        bool done = false;
        while ( !done )
        {
            
			if ( progressive == true)
				ntv2Card.WaitForVerticalInterrupt();
			else
				ntv2Card.WaitForFieldID(NTV2_FIELD1);

            // Missed-vertical check
            ntv2Card.GetOutputVerticalInterruptCount( &ulwNumVerticalsHappened );
            ntv2Card.SetOutputVerticalEventCount( ulwNumVerticalsSeen );
            if (ulwNumVerticalsSeen != ulwNumVerticalsHappened)
            {
                cMissedVerticals += ulwNumVerticalsHappened - ulwNumVerticalsSeen;
                ntv2Card.SetOutputVerticalEventCount( ulwNumVerticalsHappened );
            }


            currentInputFrame++;
            if ( currentInputFrame == _numFrames )
            {
                currentInputFrame = 0;
            }
            currentOutputFrame = currentInputFrame-_delay;
            if ( currentOutputFrame < 0 )
            {
                currentOutputFrame = _numFrames+currentOutputFrame;
                
            }
            ntv2Card.SetInputFrame(_channel,currentInputFrame);
            ntv2Card.SetOutputFrame(outputChannel,currentOutputFrame);
            
            if ( _isDying )
                done = true;
            
        }
        
        ntv2Card.SetMode(NTV2_CHANNEL1,NTV2_MODE_DISPLAY);
        
        // Put back video processing, mixer, and reference settings.
        ntv2Card.WriteVideoProcessingControl(videoProcessingControl);
        ntv2Card.WriteVideoProcessingControlCrosspoint(videoProcessingControlCrosspoint);
        ntv2Card.SetReferenceSource(referenceSource);
        ntv2Card.WriteMixerCoefficient(mixerCoefficient);
        
        ntv2Card.UnsubscribeOutputVerticalEvent();  // not really necessary; ntv2Card destructor will take care of it anyway!
    }
    
    _active = false;
}

void CNTV2FrameDelay::SetDelay(UWord delay ) 
{ 
    _delay = delay; 
} // need boundary checking};

UWord CNTV2FrameDelay::GetNumFrames() { return _numFrames; }

void CNTV2FrameDelay::SetNumFrames(UWord numFrames) 
{ 
    _numFrames = numFrames; 
}

void CNTV2FrameDelay::Quit()
{
    _isDying++;
    while ( _active )
        Sleep(100);
}
