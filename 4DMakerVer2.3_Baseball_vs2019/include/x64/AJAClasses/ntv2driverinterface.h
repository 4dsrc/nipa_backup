/*
 *
	ntv2driverinterface.h

	Copyright (C) 2004, 2005, 2006, 2007, 2009, 2011 AJA Video Systems, Inc.
	Proprietary and Confidential information.

	Purpose:

	This file contains the base class for the interfaces to the NTV2
	cards from different platforms...currently Windows,Mac OS X ,and
	Linux.
	See ntv2wininterface.[h,cpp],ntv2macinterface.[h,cpp] and
	ntv2linuxinterface.[h,cpp] for actual implementations.

        Note that most of the interfaces are pure virtual functions.

 */
#ifndef NTV2DRIVERINTERFACE_H
#define NTV2DRIVERINTERFACE_H

#include "ajaexport.h"

#include "ajatypes.h"
#include "ntv2enums.h"
#include "videodefines.h"
#include "audiodefines.h"

#include "ntv2nubtypes.h"
#include "ntv2publicinterface.h"

#include <string>

#if defined(AJALinux ) || defined(AJAMac)
	#include <sys/types.h>
	#include <netinet/in.h>
	#include <unistd.h>
#endif

#ifdef MSWindows
	#include <WinSock.h>
	#include <assert.h>
#endif


#if !defined (AJA_VIRTUAL)
	#define	AJA_VIRTUAL		virtual
#endif


/**
	@brief	I'm the base class that undergirds the platform-specific derived classes (from which CNTV2Card is ultimately derived).
**/
class AJAExport CNTV2DriverInterface
{
public:
	CNTV2DriverInterface ();
	virtual ~CNTV2DriverInterface ();

public:
	/**
		@brief		Opens an AJA device so that it can be monitored and/or controlled.
		@result		True if successful; otherwise false.
		@param[in]	inDeviceIndex		Optionally specifies a zero-based index number of the AJA device to open.
										Defaults to zero, which is the first AJA device found.
		@param[in]	displayError		Optionally specifies if an alert dialog should be displayed if a failure occurs
										when attempting to open the AJA device (on host platforms that support alert dialogs).
										Defaults to false.
		@param[in]	eDeviceType			Optionally specifies the type of AJA device to look for.
										Defaults to DEVICETYPE_UNKNOWN (recommended), which looks for all KONA or Io devices
										that are locally attached to the host.
		@param[in]	hostName			Optionally specifies the name of a host machine on the local area network that has
										one or more AJA devices attached to it. Defaults to NULL, which attempts to open AJA
										devices on the local host. If not NULL, must be a valid pointer to a buffer containing
										a zero-terminated character string.
	**/
	virtual bool Open(UWord inDeviceIndex=0, bool displayError = false,
					  NTV2DeviceType eDeviceType = DEVICETYPE_UNKNOWN,
					  const char *hostName = 0) = 0;

	bool RetailOpen(UWord inDeviceIndex=0, bool displayError = false,
		NTV2DeviceType eDeviceType = DEVICETYPE_UNKNOWN,
		const char *hostname = 0   // Non-null: card on remote host
		);


	// call this before Open to set the shareable feature of the Card
	virtual bool SetShareMode (bool bShared) = 0;

	// call this before Open to set the overlapped feature of the Card
	virtual bool SetOverlappedMode (bool bOverlapped) = 0;

	/**
		@brief		Closes the AJA device, releasing host resources that were allocated in a previous Open call.
		@result		True if successful; otherwise false.
		@details	This function closes the CNTV2Card instance's connection to the AJA device.
					Once closed, the device can no longer be queried or controlled by the CNTV2Card instance.
					The CNTV2Card instance can be "reconnected" to another AJA device by calling its Open member function again.
	**/
	virtual bool Close() = 0;

	/**
		@brief		Updates or replaces all or part of the 32-bit contents of a specific register on the AJA device.
		@result		True if successful; otherwise false.
		@param[in]	registerNumber			Specifies the register on the AJA device to be changed.
											Normally this should be a RegisterNum or VirtualRegisterNum enum value.
		@param[in]	registerValue			Specifies the new register value.
		@param[in]	registerMask			Optionally specifies a bit mask to be applied to the new value before
											updating the register. Defaults to 0xFFFFFFFF, which does not perform any masking.
		@param[in]	registerShift			Optionally specifies the number of bits to left-shift the specified value before
											updating the register. Defaults to zero, which does not perform any shifting.
		@details	This function enables the calling program to update or change any real or virtual register on the AJA device.
					Using the optional inRegisterMask and inRegisterShift parameters, it's possible to set or clear a single
					specific bit in a register without altering any of the register's other bits.
		@note		Use this function only when there is no higher-level function available to accomplish the desired task.
	**/
	virtual bool WriteRegister(ULWord registerNumber,
							   ULWord registerValue,
							   ULWord registerMask = 0xFFFFFFFF,
							   ULWord registerShift = 0);
protected:
	/**
		@brief		Reads all or part of the 32-bit contents of a specific register on the AJA device.
		@result		True if successful; otherwise false.
		@param[in]	inRegisterNumber	Specifies the register on the AJA device to be read. Normally this should be
										a RegisterNum or VirtualRegisterNum enum value.
		@param[out]	pOutRegisterValue	Specifies a valid, non-NULL address of the ULWord that is to receive the
										register value obtained from the device.
		@param[in]	inRegisterMask		Optionally specifies a bit mask to be applied after reading the device register.
										Defaults to 0xFFFFFFFF, which does not perform any masking.
										A zero mask (0x00000000) is also ignored.
		@param[in]	inRegisterShift		Optionally specifies the number of bits to right-shift the value obtained
										from the device register after any mask has been applied.
										Defaults to zero, which performs no bit shift.
		@details	This function enables the calling program to read any real or virtual register on the AJA device.
					Using the optional inRegisterMask and inRegisterShift parameters, it's possible to read a single
					specific bit in a register while ignoring the register's other bits.
		@note		Use this function only when there is no higher-level function available to accomplish the desired task.
	**/
	virtual bool ReadRegister ( ULWord		inRegisterNumber,
								ULWord *	pOutRegisterValue,
								ULWord		inRegisterMask		= 0xFFFFFFFF,
								ULWord		inRegisterShift		= 0);
public:
	/**
		@brief		Reads all or part of the 32-bit contents of a specific register on the AJA device.
		@result		True if successful; otherwise false.
		@param[in]	inRegisterNumber	Specifies the register on the AJA device to be read. Normally this should be
										a RegisterNum or VirtualRegisterNum enum value.
		@param[out]	outRegisterValue	Receives the register value obtained from the device.
		@param[in]	inRegisterMask		Optionally specifies a bit mask to be applied after reading the device register.
										Defaults to 0xFFFFFFFF, which does not perform any masking.
										A zero mask (0x00000000) is also ignored.
		@param[in]	inRegisterShift		Optionally specifies the number of bits to right-shift the value obtained
										from the device register after any mask has been applied.
										Defaults to zero, which performs no bit shift.
		@details	This function enables the calling program to read any real or virtual register on the AJA device.
					Using the optional inRegisterMask and inRegisterShift parameters, it's possible to read a single
					specific bit in a register while ignoring the register's other bits.
		@note		Use this function only when there is no higher-level function available to accomplish the desired task.
	**/
	virtual inline bool ReadRegister (  const ULWord	inRegisterNumber,
										ULWord &		outRegisterValue,
										const ULWord	inRegisterMask	= 0xFFFFFFFF,
										const ULWord	inRegisterShift	= 0)
	{
		return ReadRegister (inRegisterNumber, &outRegisterValue, inRegisterMask, inRegisterShift);
	}

	// Read multiple registers at once.
	virtual bool ReadRegisterMulti(	ULWord numRegs,
									ULWord *whichRegisterFailed,
							  		NTV2ReadWriteRegisterSingle aRegs[]);

	virtual bool RestoreHardwareProcampRegisters() = 0;

	virtual bool DmaTransfer (NTV2DMAEngine DMAEngine,
		bool bRead,
		ULWord frameNumber,
		ULWord * pFrameBuffer,
		ULWord offsetBytes,
		ULWord bytes,
		bool bSync = true) = 0;

	virtual bool DmaUnlock   (void)  = 0;

	virtual bool CompleteMemoryForDMA (ULWord * pFrameBuffer)  = 0;


	virtual bool PrepareMemoryForDMA (ULWord * pFrameBuffer, ULWord ulNumBytes)  = 0;


	virtual bool ConfigureInterrupt (bool bEnable,  INTERRUPT_ENUMS eInterruptType) = 0;

	virtual bool MapFrameBuffers (void)  = 0;

	virtual bool UnmapFrameBuffers (void)  = 0;

	virtual bool MapRegisters (void)  = 0;

	virtual bool UnmapRegisters (void)  = 0;

	virtual bool MapXena2Flash (void)  = 0;

	virtual bool UnmapXena2Flash (void)  = 0;

	virtual bool ConfigureSubscription (bool bSubscribe, INTERRUPT_ENUMS eInterruptType, PULWord & hSubcription);

	virtual bool GetInterruptCount (INTERRUPT_ENUMS eInterrupt,
								    ULWord *pCount)  = 0;

	virtual bool WaitForInterrupt (INTERRUPT_ENUMS eInterrupt, ULWord timeOutMs = 68);

	virtual bool AutoCirculate (AUTOCIRCULATE_DATA &autoCircData);

	virtual bool ControlDriverDebugMessages(NTV2_DriverDebugMessageSet msgSet,
		  									bool enable ) = 0;

    virtual bool GetDriverVersion(ULWord* driverVersion) = 0;


    // Utility methods:
	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL NTV2BoardType	GetCompileFlag (void);
		virtual inline bool			BoardOpened (void) const						{ return IsOpen (); }
	#endif	//	!NTV2_DEPRECATE

	/**
		@brief	Returns true if I'm able to communicate with the device I represent.
		@return	True if I'm able to communicate with the device I represent;  otherwise false.
	**/
	virtual inline bool			IsOpen (void) const									{ return _boardOpened; }

	AJA_VIRTUAL void			InitMemberVariablesOnOpen (NTV2FrameGeometry frameGeometry, NTV2FrameBufferFormat frameFormat);

	virtual inline const char *	GetHostName (void) const							{ return _hostname.c_str (); }

	virtual inline ULWord		GetPCISlotNumber (void) const						{ return _pciSlot; }

	virtual inline Word			SleepMs (LWord msec) const							{ (void) msec; return 0; }

	virtual inline ULWord		GetNumFrameBuffers (void) const						{ return _ulNumFrameBuffers; }
	virtual inline ULWord		GetAudioFrameBufferNumber (void) const				{ return (GetNumFrameBuffers () - 1); }
	virtual inline ULWord		GetFrameBufferSize (void) const						{ return _ulFrameBufferSize; }

	virtual bool DriverGetBitFileInformation (BITFILE_INFO_STRUCT & bitFileInfo,
												NTV2BitFileType bitFileType);		//	DEPRECATION_CANDIDATE

	virtual bool DriverGetBuildInformation (BUILD_INFO_STRUCT & outBuildInfo);

	// Functions for cards that support more than one bitfile

	virtual bool SwitchBitfile(NTV2DeviceID boardID, NTV2BitfileType bitfile)		//	DEPRECATION_CANDIDATE
		{ (void) boardID; (void) bitfile; return false; }

	virtual inline NTV2NubProtocolVersion	GetNubProtocolVersion (void) const		{return _nubProtocolVersion;}

	virtual inline bool						IsRemote (void) const					{return _remoteHandle != INVALID_NUB_HANDLE;}

protected:

	virtual bool				DisplayNTV2Error (const char * str) { (void) str; return  false;}
	virtual bool				OpenRemote (UWord inDeviceIndex,
											bool displayErrorMessage,
											UWord ulBoardType,
											const char * hostname);
	virtual bool				CloseRemote (void);

	AJA_VIRTUAL bool			ParseFlashHeader (BITFILE_INFO_STRUCT &bitFileInfo);

	/**
		@brief	Private method that increments the event count tally for the given interrupt type.
		@param[in]	eInterruptType	Specifies the type of interrupt that occurred, which determines
									the counter to be incremented.
	**/
	virtual void	BumpEventCount (const INTERRUPT_ENUMS eInterruptType);

private:
	/**
		@brief	My assignment operator.
		@note	We have intentionally disabled this capability because it was never implemented
				and is currently broken. If/when it gets fixed, we'll make this a public method.
		@param[in]	inRHS	The rvalue to be assigned to the lvalue.
		@return	A non-constant reference to the lvalue.
	**/
	virtual CNTV2DriverInterface & operator = (const CNTV2DriverInterface & inRHS);

	/**
		@brief	My copy constructor.
		@note	We have intentionally disabled this because it was never implemented and is
				currently broken. If/when it gets fixed, we'll make this a public method.
		@param[in]	inObjToCopy		The object to be copied.
	**/
	CNTV2DriverInterface (const CNTV2DriverInterface & inObjToCopy);

protected:

    UWord					_boardNumber;
    bool					_boardOpened;
    NTV2BoardType			_boardType;
	NTV2DeviceID			_boardID;
    bool					_displayErrorMessage;
	ULWord					_pciSlot;
	ULWord					_programStatus;
	bool					_quadFrameEnabled;

	struct sockaddr_in		_sockAddr;			// If card is on a remote host
	AJASocket				_sockfd;			// If card is on a remote host

	std::string				_hostname;			// If card is on a remote host
	LWord					_remoteHandle;		// If card is on a remote host

	NTV2NubProtocolVersion	_nubProtocolVersion;// If card is on a remote host

	PULWord					mInterruptEventHandles	[eNumInterruptTypes];	//	For subscribing to each possible event, one for each interrupt type
	ULWord					mEventCounts			[eNumInterruptTypes];	//	My event tallies, one for each interrupt type. Note that these
																			//	tallies are different from the interrupt tallies kept by the driver.
	ULWord *				_pFrameBaseAddress;
	// for old KSD and KHD boards
	ULWord *				_pCh1FrameBaseAddress;
	ULWord *				_pCh2FrameBaseAddress;

	ULWord *				_pRegisterBaseAddress;
	ULWord					_pRegisterBaseAddressLength;
	ULWord *				_pFS1FPGARegisterBaseAddress;

	ULWord *				_pXena2FlashBaseAddress;

	ULWord					_ulChannelTwoOffset;
	ULWord					_ulNumFrameBuffers;
	ULWord					_ulFrameBufferSize;

};	//	CNTV2DriverInterface

#endif	//	NTV2DRIVERINTERFACE_H
