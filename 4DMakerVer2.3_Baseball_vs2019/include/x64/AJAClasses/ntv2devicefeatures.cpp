/**
	@file		ntv2devicefeatures.cpp
	@brief		Implementations of all device capability functions.
	@copyright	(C) 2004-2014 AJA Video Systems, Inc.	Proprietary and confidential information.
**/

#include "ntv2devicefeatures.h"


// static utility functions
bool NTV2DeviceIsDirectAddressable(NTV2DeviceID boardID)
{
	switch (boardID) 
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;

	default:
		break;
	}
	
	return false;
}

bool NTV2DeviceIs64Bit(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}

bool NTV2DeviceCanDoFormat(NTV2DeviceID		boardID,
						  NTV2FrameRate		framerate,
  			              NTV2FrameGeometry framegeometry, 
						  NTV2Standard		standard)
{
	switch (boardID)
	{
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_LHI:
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
		switch (standard)
		{
		case NTV2_STANDARD_1080p:
			switch (framerate)
			{
			case NTV2_FRAMERATE_6000:
			case NTV2_FRAMERATE_5994:
			case NTV2_FRAMERATE_5000:
			case NTV2_FRAMERATE_3000:
			case NTV2_FRAMERATE_2997:
			case NTV2_FRAMERATE_2500:
			case NTV2_FRAMERATE_2400:
			case NTV2_FRAMERATE_2398:	
				return (framegeometry == NTV2_FG_1920x1080 || framegeometry == NTV2_FG_1920x1112 || 
						framegeometry == NTV2_FG_2048x1080 || framegeometry == NTV2_FG_2048x1112);
			default:					
				return false;
			}
		case NTV2_STANDARD_1080:
			switch (framerate)
			{
			case NTV2_FRAMERATE_3000:
			case NTV2_FRAMERATE_2997:
			case NTV2_FRAMERATE_2500:
			case NTV2_FRAMERATE_2400:
			case NTV2_FRAMERATE_2398:	
				return (framegeometry == NTV2_FG_1920x1080 || framegeometry == NTV2_FG_1920x1112 || 
						framegeometry == NTV2_FG_2048x1080 || framegeometry == NTV2_FG_2048x1112);
			default:					
				return false;
			}
		case NTV2_STANDARD_720:
			switch (framerate)
			{
			case NTV2_FRAMERATE_6000:
			case NTV2_FRAMERATE_5994:
			case NTV2_FRAMERATE_5000:
			case NTV2_FRAMERATE_2500:
			case NTV2_FRAMERATE_2398:
				return (framegeometry == NTV2_FG_1280x720 || framegeometry == NTV2_FG_1280x740);
			default:
				return false;
			}
		case NTV2_STANDARD_525:
			switch (framerate)
			{
			case NTV2_FRAMERATE_2997:
				return (framegeometry == NTV2_FG_720x486 || framegeometry == NTV2_FG_720x508 || framegeometry == NTV2_FG_720x514);
			default:
				return false;
			}
		case NTV2_STANDARD_625:
			switch (framerate)
			{
			case NTV2_FRAMERATE_2500:
				return (framegeometry == NTV2_FG_720x576 || framegeometry == NTV2_FG_720x598 || framegeometry == NTV2_FG_720x612);
			default:
				return false;
			}
		default:
			return false;
		}
		
	case DEVICE_ID_LHE_PLUS:
		switch (standard)
		{
		case NTV2_STANDARD_1080p:
			switch (framerate)
			{
			case NTV2_FRAMERATE_3000:
			case NTV2_FRAMERATE_2997:
			case NTV2_FRAMERATE_2500:
			case NTV2_FRAMERATE_2400:
			case NTV2_FRAMERATE_2398:	
				return (framegeometry == NTV2_FG_1920x1080 || framegeometry == NTV2_FG_1920x1112 || 
						framegeometry == NTV2_FG_2048x1080 || framegeometry == NTV2_FG_2048x1112);
			default:					
				return false;
			}
		case NTV2_STANDARD_1080:
			switch (framerate)
			{
			case NTV2_FRAMERATE_3000:
			case NTV2_FRAMERATE_2997:
			case NTV2_FRAMERATE_2500:
			case NTV2_FRAMERATE_2400:
			case NTV2_FRAMERATE_2398:	
				return (framegeometry == NTV2_FG_1920x1080 || framegeometry == NTV2_FG_1920x1112 || 
						framegeometry == NTV2_FG_2048x1080 || framegeometry == NTV2_FG_2048x1112);
			default:					
				return false;
			}
		case NTV2_STANDARD_720:
			switch (framerate)
			{
			case NTV2_FRAMERATE_6000:
			case NTV2_FRAMERATE_5994:
			case NTV2_FRAMERATE_5000:
				return (framegeometry == NTV2_FG_1280x720 || framegeometry == NTV2_FG_1280x740);
			default:
				return false;
			}
		case NTV2_STANDARD_525:
			switch (framerate)
			{
			case NTV2_FRAMERATE_2997:
				return (framegeometry == NTV2_FG_720x486 || framegeometry == NTV2_FG_720x508 || framegeometry == NTV2_FG_720x514);
			default:
				return false;
			}
		case NTV2_STANDARD_625:
			switch (framerate)
			{
			case NTV2_FRAMERATE_2500:
				return (framegeometry == NTV2_FG_720x576 || framegeometry == NTV2_FG_720x598 || framegeometry == NTV2_FG_720x612);
			default:
				return false;
			}
		default:
			return false;
		}
		
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_TTAP:
		switch (standard)
		{
		case NTV2_STANDARD_1080p:
		case NTV2_STANDARD_1080:
			switch (framerate)
			{
			case NTV2_FRAMERATE_3000:
			case NTV2_FRAMERATE_2997:
			case NTV2_FRAMERATE_2500:
			case NTV2_FRAMERATE_2400:
			case NTV2_FRAMERATE_2398:	
				return (framegeometry == NTV2_FG_1920x1080 || framegeometry == NTV2_FG_1920x1112 || 
						framegeometry == NTV2_FG_2048x1080 || framegeometry == NTV2_FG_2048x1112);
			default:					
				return false;
			}
		case NTV2_STANDARD_720:
			switch (framerate)
			{
			case NTV2_FRAMERATE_6000:
			case NTV2_FRAMERATE_5994:
			case NTV2_FRAMERATE_5000:
				return (framegeometry == NTV2_FG_1280x720 || framegeometry == NTV2_FG_1280x740);
			default:
				return false;
			}
		case NTV2_STANDARD_525:
			switch (framerate)
			{
			case NTV2_FRAMERATE_2997:
				return (framegeometry == NTV2_FG_720x486 || framegeometry == NTV2_FG_720x508 || framegeometry == NTV2_FG_720x514);
			default:
				return false;
			}
		case NTV2_STANDARD_625:
			switch (framerate)
			{
			case NTV2_FRAMERATE_2500:
				return (framegeometry == NTV2_FG_720x576 || framegeometry == NTV2_FG_720x598 || framegeometry == NTV2_FG_720x612);
			default:
				return false;
			}
		default:
			return false;
		}

	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:	// defined by what it can't do for now
		switch (standard)
		{
		case NTV2_STANDARD_1080p:
			switch (framerate)
			{
			case NTV2_FRAMERATE_6000:	// can't do 1080p60 "a" formats
			case NTV2_FRAMERATE_5994:
			case NTV2_FRAMERATE_5000:
				return false;
			default:					
				return true;
			}
		default:
			return true;
		}
	#endif	//	!defined (NTV2_DEPRECATE)

	case DEVICE_ID_KONA3G:
		switch (framegeometry)
		{
		case NTV2_FG_4x1920x1080:
		case NTV2_FG_4x2048x1080:
			return false;
		default:
			return true;
		}

	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
		switch (framegeometry)
		{
		case NTV2_FG_4x1920x1080:
		case NTV2_FG_4x2048x1080:
		case NTV2_FG_2048x1556:
		case NTV2_FG_2048x1588:
			return false;
		default:
			return true;
		}
		
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		switch (standard)
		{
		case NTV2_STANDARD_525:
			return (framerate != NTV2_FRAMERATE_2398 && framerate != NTV2_FRAMERATE_2400);	// eg NTV2_FORMAT_525_2398 requires conversion which is not available
		case NTV2_STANDARD_720:
			return (framerate != NTV2_FRAMERATE_2398 && framerate != NTV2_FRAMERATE_2500);	// eg NTV2_FORMAT_720p_2398 requires conversion which is not available
		default:
			return true;		// pretty much everything else
		}

	default:
		return false;
	}
}

// static utility functions
bool NTV2DeviceCanDoVideoFormat(NTV2DeviceID boardID, NTV2VideoFormat format)
{
	switch (boardID)
	{
	case DEVICE_ID_LHE_PLUS:
		switch(format)
		{
		case NTV2_FORMAT_525_5994: 
		case NTV2_FORMAT_625_5000:
		case NTV2_FORMAT_720p_6000: 
		case NTV2_FORMAT_720p_5994: 
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_1080i_6000: 
		case NTV2_FORMAT_1080i_5994: 
		case NTV2_FORMAT_1080i_5000: 
		case NTV2_FORMAT_1080psf_3000_2: 
		case NTV2_FORMAT_1080psf_2997_2: 
		case NTV2_FORMAT_1080psf_2500_2: 
		case NTV2_FORMAT_1080psf_2400: 
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080p_2K_2500:
			return true;
	    default:
			return false;
		}

	case DEVICE_ID_CORVID22:
		switch(format)
		{
		case NTV2_FORMAT_1080i_6000: 
		case NTV2_FORMAT_1080i_5994: 
		case NTV2_FORMAT_1080i_5000: 
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2400: 
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_720p_6000: 
		case NTV2_FORMAT_720p_5994: 
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_720p_2398:
		case NTV2_FORMAT_720p_2500:
		case NTV2_FORMAT_525_2398:
		case NTV2_FORMAT_525_2400:
		case NTV2_FORMAT_525_5994: 
		case NTV2_FORMAT_625_5000: 
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_5000_B:
		case NTV2_FORMAT_1080p_5994_B:
		case NTV2_FORMAT_1080p_6000_B:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_CORVID3G:
		switch(format)
		{
		case NTV2_FORMAT_1080i_6000: 
		case NTV2_FORMAT_1080i_5994: 
		case NTV2_FORMAT_1080i_5000: 
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2400: 
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_720p_6000: 
		case NTV2_FORMAT_720p_5994: 
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_720p_2398:
		case NTV2_FORMAT_720p_2500:
		case NTV2_FORMAT_525_2398:
		case NTV2_FORMAT_525_2400:
		case NTV2_FORMAT_525_5994: 
		case NTV2_FORMAT_625_5000: 
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_5000_B:
		case NTV2_FORMAT_1080p_5994_B:
		case NTV2_FORMAT_1080p_6000_B:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_LHI:
		switch(format)
		{
		case NTV2_FORMAT_1080i_6000: 
		case NTV2_FORMAT_1080i_5994: 
		case NTV2_FORMAT_1080i_5000: 
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2400: 
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_720p_6000: 
		case NTV2_FORMAT_720p_5994: 
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_720p_2398:
		case NTV2_FORMAT_720p_2500:
		case NTV2_FORMAT_525_2398:
		case NTV2_FORMAT_525_2400:
		case NTV2_FORMAT_525_5994: 
		case NTV2_FORMAT_625_5000: 
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_5000_B:
		case NTV2_FORMAT_1080p_5994_B:
		case NTV2_FORMAT_1080p_6000_B:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_IOEXPRESS:
		switch(format)
		{
		case NTV2_FORMAT_1080i_6000: 
		case NTV2_FORMAT_1080i_5994: 
		case NTV2_FORMAT_1080i_5000: 
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2400: 
		case NTV2_FORMAT_1080psf_2398: 
		case NTV2_FORMAT_720p_6000: 
		case NTV2_FORMAT_720p_5994: 
		case NTV2_FORMAT_720p_5000: 
		case NTV2_FORMAT_525_5994: 
		case NTV2_FORMAT_625_5000: 
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_CORVID1:
		switch(format)
		{
		case NTV2_FORMAT_1080i_6000: 
		case NTV2_FORMAT_1080i_5994: 
		case NTV2_FORMAT_1080i_5000: 
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2400: 
		case NTV2_FORMAT_1080psf_2398: 
		case NTV2_FORMAT_720p_6000:
		case NTV2_FORMAT_720p_5994: 
		case NTV2_FORMAT_720p_5000: 
		case NTV2_FORMAT_525_5994: 
		case NTV2_FORMAT_625_5000: 
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_KONA3G:
		switch(format)
		{
		case NTV2_FORMAT_1080i_5000:
		case NTV2_FORMAT_1080i_5994:
		case NTV2_FORMAT_1080i_6000:
		case NTV2_FORMAT_720p_5994:
		case NTV2_FORMAT_720p_6000:
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080psf_2400:
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_1080p_5000:
		case NTV2_FORMAT_1080p_5994:
		case NTV2_FORMAT_1080p_6000:
		case NTV2_FORMAT_720p_2398:
		case NTV2_FORMAT_720p_2500:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_525_5994:
		case NTV2_FORMAT_625_5000:
		case NTV2_FORMAT_525_2398:
		case NTV2_FORMAT_525_2400:
		case NTV2_FORMAT_525psf_2997:
		case NTV2_FORMAT_625psf_2500:
		case NTV2_FORMAT_2K_1498:
		case NTV2_FORMAT_2K_1500:
		case NTV2_FORMAT_2K_2398:
		case NTV2_FORMAT_2K_2400:
		case NTV2_FORMAT_2K_2500:
		case NTV2_FORMAT_1080p_2K_6000:
		case NTV2_FORMAT_1080p_2K_5994:
		case NTV2_FORMAT_1080p_2K_2997:
		case NTV2_FORMAT_1080p_2K_3000:
		case NTV2_FORMAT_1080p_2K_5000:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_TTAP:
		switch(format)
		{
		case NTV2_FORMAT_1080i_6000: 
		case NTV2_FORMAT_1080i_5994: 
		case NTV2_FORMAT_1080i_5000: 
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2400: 
		case NTV2_FORMAT_1080psf_2398: 
		case NTV2_FORMAT_720p_6000: 
		case NTV2_FORMAT_720p_5994: 
		case NTV2_FORMAT_720p_5000: 
		case NTV2_FORMAT_525_5994: 
		case NTV2_FORMAT_625_5000: 
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2398:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_IOXT:
		switch(format)
		{
		case NTV2_FORMAT_1080i_6000: 
		case NTV2_FORMAT_1080i_5994: 
		case NTV2_FORMAT_1080i_5000: 
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2400: 
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_720p_6000: 
		case NTV2_FORMAT_720p_5994: 
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_720p_2398:
		case NTV2_FORMAT_720p_2500:
		case NTV2_FORMAT_525_2398:
		case NTV2_FORMAT_525_2400:
		case NTV2_FORMAT_525_5994: 
		case NTV2_FORMAT_625_5000: 
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_5000_B:
		case NTV2_FORMAT_1080p_5994_B:
		case NTV2_FORMAT_1080p_6000_B:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_IO4KUFC:
		switch(format)
		{
		case NTV2_FORMAT_1080i_6000: 
		case NTV2_FORMAT_1080i_5994: 
		case NTV2_FORMAT_1080i_5000: 
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2400: 
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_720p_6000: 
		case NTV2_FORMAT_720p_5994: 
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_720p_2398:
		case NTV2_FORMAT_720p_2500:
		case NTV2_FORMAT_525_2398:
		case NTV2_FORMAT_525_2400:
		case NTV2_FORMAT_525_5994: 
		case NTV2_FORMAT_625_5000: 
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_5000_B:
		case NTV2_FORMAT_1080p_5994_B:
		case NTV2_FORMAT_1080p_6000_B:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_KONA4UFC:
		switch(format)
		{
		case NTV2_FORMAT_1080i_6000: 
		case NTV2_FORMAT_1080i_5994: 
		case NTV2_FORMAT_1080i_5000: 
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2400: 
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_720p_6000: 
		case NTV2_FORMAT_720p_5994: 
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_720p_2398:
		case NTV2_FORMAT_720p_2500:
		case NTV2_FORMAT_525_2398:
		case NTV2_FORMAT_525_2400:
		case NTV2_FORMAT_525_5994: 
		case NTV2_FORMAT_625_5000: 
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_5000_B:
		case NTV2_FORMAT_1080p_5994_B:
		case NTV2_FORMAT_1080p_6000_B:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_IO4K:
		switch(format)
		{
		case NTV2_FORMAT_1080i_5000:
		case NTV2_FORMAT_1080i_5994:
		case NTV2_FORMAT_1080i_6000:
		case NTV2_FORMAT_720p_5994:
		case NTV2_FORMAT_720p_6000:
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080psf_2400:
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_1080p_5000:
		case NTV2_FORMAT_1080p_5994:
		case NTV2_FORMAT_1080p_6000:
		case NTV2_FORMAT_720p_2500:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_525_5994:
		case NTV2_FORMAT_625_5000:
		case NTV2_FORMAT_525psf_2997:
		case NTV2_FORMAT_625psf_2500:
		case NTV2_FORMAT_4x1920x1080psf_2398:
		case NTV2_FORMAT_4x1920x1080psf_2400:
		case NTV2_FORMAT_4x1920x1080psf_2500:
		case NTV2_FORMAT_4x1920x1080p_2398:
		case NTV2_FORMAT_4x1920x1080p_2400:
		case NTV2_FORMAT_4x1920x1080p_2500:
		case NTV2_FORMAT_4x2048x1080psf_2398:
		case NTV2_FORMAT_4x2048x1080psf_2400:
		case NTV2_FORMAT_4x2048x1080psf_2500:
		case NTV2_FORMAT_4x2048x1080p_2398:
		case NTV2_FORMAT_4x2048x1080p_2400:
		case NTV2_FORMAT_4x2048x1080p_2500:
		case NTV2_FORMAT_4x1920x1080p_2997:
		case NTV2_FORMAT_4x1920x1080p_3000:
		case NTV2_FORMAT_4x2048x1080p_2997:
		case NTV2_FORMAT_4x2048x1080p_3000:
		case NTV2_FORMAT_4x1920x1080p_5000:
		case NTV2_FORMAT_4x1920x1080p_5994:
		case NTV2_FORMAT_4x1920x1080p_6000:
		case NTV2_FORMAT_4x2048x1080p_5000:
		case NTV2_FORMAT_4x2048x1080p_5994:
		case NTV2_FORMAT_4x2048x1080p_6000:
		case NTV2_FORMAT_4x2048x1080p_4795:
		case NTV2_FORMAT_4x2048x1080p_4800:
		case NTV2_FORMAT_1080p_2K_6000:
		case NTV2_FORMAT_1080p_2K_5994:
		case NTV2_FORMAT_1080p_2K_2997:
		case NTV2_FORMAT_1080p_2K_3000:
		case NTV2_FORMAT_1080p_2K_5000:
		case NTV2_FORMAT_1080p_2K_4795:
		case NTV2_FORMAT_1080p_2K_4800:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_CORVID88:
		switch(format)
		{
		case NTV2_FORMAT_1080i_5000:
		case NTV2_FORMAT_1080i_5994:
		case NTV2_FORMAT_1080i_6000:
		case NTV2_FORMAT_720p_5994:
		case NTV2_FORMAT_720p_6000:
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080psf_2400:
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_1080p_5000:
		case NTV2_FORMAT_1080p_5994:
		case NTV2_FORMAT_1080p_6000:
		case NTV2_FORMAT_720p_2500:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_525_5994:
		case NTV2_FORMAT_625_5000:
		case NTV2_FORMAT_525psf_2997:
		case NTV2_FORMAT_625psf_2500:
		case NTV2_FORMAT_2K_1498:
		case NTV2_FORMAT_2K_1500:
		case NTV2_FORMAT_2K_2398:
		case NTV2_FORMAT_2K_2400:
		case NTV2_FORMAT_2K_2500:
		case NTV2_FORMAT_4x1920x1080psf_2398:
		case NTV2_FORMAT_4x1920x1080psf_2400:
		case NTV2_FORMAT_4x1920x1080psf_2500:
		case NTV2_FORMAT_4x1920x1080p_2398:
		case NTV2_FORMAT_4x1920x1080p_2400:
		case NTV2_FORMAT_4x1920x1080p_2500:
		case NTV2_FORMAT_4x2048x1080psf_2398:
		case NTV2_FORMAT_4x2048x1080psf_2400:
		case NTV2_FORMAT_4x2048x1080psf_2500:
		case NTV2_FORMAT_4x2048x1080p_2398:
		case NTV2_FORMAT_4x2048x1080p_2400:
		case NTV2_FORMAT_4x2048x1080p_2500:
		case NTV2_FORMAT_4x1920x1080p_2997:
		case NTV2_FORMAT_4x1920x1080p_3000:
		case NTV2_FORMAT_4x2048x1080p_2997:
		case NTV2_FORMAT_4x2048x1080p_3000:
		case NTV2_FORMAT_4x1920x1080p_5000:
		case NTV2_FORMAT_4x1920x1080p_5994:
		case NTV2_FORMAT_4x1920x1080p_6000:
		case NTV2_FORMAT_4x2048x1080p_5000:
		case NTV2_FORMAT_4x2048x1080p_5994:
		case NTV2_FORMAT_4x2048x1080p_6000:
		case NTV2_FORMAT_4x2048x1080p_4795:
		case NTV2_FORMAT_4x2048x1080p_4800:
		case NTV2_FORMAT_1080p_2K_6000:
		case NTV2_FORMAT_1080p_2K_5994:
		case NTV2_FORMAT_1080p_2K_2997:
		case NTV2_FORMAT_1080p_2K_3000:
		case NTV2_FORMAT_1080p_2K_5000:
		case NTV2_FORMAT_1080p_2K_4795:
		case NTV2_FORMAT_1080p_2K_4800:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_CORVID44:
		switch(format)
		{
		case NTV2_FORMAT_1080i_5000:
		case NTV2_FORMAT_1080i_5994:
		case NTV2_FORMAT_1080i_6000:
		case NTV2_FORMAT_720p_5994:
		case NTV2_FORMAT_720p_6000:
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080psf_2400:
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_1080p_5000:
		case NTV2_FORMAT_1080p_5994:
		case NTV2_FORMAT_1080p_6000:
		case NTV2_FORMAT_720p_2500:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_525_5994:
		case NTV2_FORMAT_625_5000:
		case NTV2_FORMAT_525psf_2997:
		case NTV2_FORMAT_625psf_2500:
		case NTV2_FORMAT_2K_1498:
		case NTV2_FORMAT_2K_1500:
		case NTV2_FORMAT_2K_2398:
		case NTV2_FORMAT_2K_2400:
		case NTV2_FORMAT_2K_2500:
		case NTV2_FORMAT_4x1920x1080psf_2398:
		case NTV2_FORMAT_4x1920x1080psf_2400:
		case NTV2_FORMAT_4x1920x1080psf_2500:
		case NTV2_FORMAT_4x1920x1080p_2398:
		case NTV2_FORMAT_4x1920x1080p_2400:
		case NTV2_FORMAT_4x1920x1080p_2500:
		case NTV2_FORMAT_4x2048x1080psf_2398:
		case NTV2_FORMAT_4x2048x1080psf_2400:
		case NTV2_FORMAT_4x2048x1080psf_2500:
		case NTV2_FORMAT_4x2048x1080p_2398:
		case NTV2_FORMAT_4x2048x1080p_2400:
		case NTV2_FORMAT_4x2048x1080p_2500:
		case NTV2_FORMAT_4x1920x1080p_2997:
		case NTV2_FORMAT_4x1920x1080p_3000:
		case NTV2_FORMAT_4x2048x1080p_2997:
		case NTV2_FORMAT_4x2048x1080p_3000:
		case NTV2_FORMAT_4x1920x1080p_5000:
		case NTV2_FORMAT_4x1920x1080p_5994:
		case NTV2_FORMAT_4x1920x1080p_6000:
		case NTV2_FORMAT_4x2048x1080p_5000:
		case NTV2_FORMAT_4x2048x1080p_5994:
		case NTV2_FORMAT_4x2048x1080p_6000:
		case NTV2_FORMAT_4x2048x1080p_4795:
		case NTV2_FORMAT_4x2048x1080p_4800:
		case NTV2_FORMAT_1080p_2K_6000:
		case NTV2_FORMAT_1080p_2K_5994:
		case NTV2_FORMAT_1080p_2K_2997:
		case NTV2_FORMAT_1080p_2K_3000:
		case NTV2_FORMAT_1080p_2K_5000:
		case NTV2_FORMAT_1080p_2K_4795:
		case NTV2_FORMAT_1080p_2K_4800:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_KONA4:
		switch(format)
		{
		case NTV2_FORMAT_1080i_5000:
		case NTV2_FORMAT_1080i_5994:
		case NTV2_FORMAT_1080i_6000:
		case NTV2_FORMAT_720p_5994:
		case NTV2_FORMAT_720p_6000:
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080psf_2400:
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_1080p_5000:
		case NTV2_FORMAT_1080p_5994:
		case NTV2_FORMAT_1080p_6000:
		case NTV2_FORMAT_720p_2500:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_6000_A:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_525_5994:
		case NTV2_FORMAT_625_5000:
		case NTV2_FORMAT_525psf_2997:
		case NTV2_FORMAT_625psf_2500:
		case NTV2_FORMAT_2K_1498:
		case NTV2_FORMAT_2K_1500:
		case NTV2_FORMAT_2K_2398:
		case NTV2_FORMAT_2K_2400:
		case NTV2_FORMAT_2K_2500:
		case NTV2_FORMAT_4x1920x1080psf_2398:
		case NTV2_FORMAT_4x1920x1080psf_2400:
		case NTV2_FORMAT_4x1920x1080psf_2500:
		case NTV2_FORMAT_4x1920x1080p_2398:
		case NTV2_FORMAT_4x1920x1080p_2400:
		case NTV2_FORMAT_4x1920x1080p_2500:
		case NTV2_FORMAT_4x2048x1080psf_2398:
		case NTV2_FORMAT_4x2048x1080psf_2400:
		case NTV2_FORMAT_4x2048x1080psf_2500:
		case NTV2_FORMAT_4x2048x1080p_2398:
		case NTV2_FORMAT_4x2048x1080p_2400:
		case NTV2_FORMAT_4x2048x1080p_2500:
		case NTV2_FORMAT_4x1920x1080p_2997:
		case NTV2_FORMAT_4x1920x1080p_3000:
		case NTV2_FORMAT_4x2048x1080p_2997:
		case NTV2_FORMAT_4x2048x1080p_3000:
		case NTV2_FORMAT_4x1920x1080p_5000:
		case NTV2_FORMAT_4x1920x1080p_5994:
		case NTV2_FORMAT_4x1920x1080p_6000:
		case NTV2_FORMAT_4x2048x1080p_5000:
		case NTV2_FORMAT_4x2048x1080p_5994:
		case NTV2_FORMAT_4x2048x1080p_6000:
		case NTV2_FORMAT_4x2048x1080p_4795:
		case NTV2_FORMAT_4x2048x1080p_4800:
		case NTV2_FORMAT_1080p_2K_6000:
		case NTV2_FORMAT_1080p_2K_5994:
		case NTV2_FORMAT_1080p_2K_2997:
		case NTV2_FORMAT_1080p_2K_3000:
		case NTV2_FORMAT_1080p_2K_5000:
		case NTV2_FORMAT_1080p_2K_4795:
		case NTV2_FORMAT_1080p_2K_4800:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_KONA3GQUAD:
		switch(format)
		{
		case NTV2_FORMAT_525_5994:
		case NTV2_FORMAT_625_5000:
		case NTV2_FORMAT_720p_2398:
		case NTV2_FORMAT_720p_2500:
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_720p_5994:
		case NTV2_FORMAT_720p_6000:  
		case NTV2_FORMAT_1080i_5000:
		case NTV2_FORMAT_1080i_5994:
		case NTV2_FORMAT_1080i_6000:
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080psf_2400:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5000_B:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_5994_B:
		case NTV2_FORMAT_1080p_6000_A:
		case NTV2_FORMAT_1080p_6000_B:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080p_2K_2997:
		case NTV2_FORMAT_1080p_2K_3000:
		case NTV2_FORMAT_4x1920x1080psf_2398:
		case NTV2_FORMAT_4x1920x1080psf_2400:
		case NTV2_FORMAT_4x1920x1080psf_2500:
		case NTV2_FORMAT_4x1920x1080p_2398:
		case NTV2_FORMAT_4x1920x1080p_2400:
		case NTV2_FORMAT_4x1920x1080p_2500:
		case NTV2_FORMAT_4x1920x1080p_2997:
		case NTV2_FORMAT_4x1920x1080p_3000:
		case NTV2_FORMAT_4x2048x1080psf_2398:
		case NTV2_FORMAT_4x2048x1080psf_2400:
		case NTV2_FORMAT_4x2048x1080psf_2500:
		case NTV2_FORMAT_4x2048x1080p_2398:
		case NTV2_FORMAT_4x2048x1080p_2400:
		case NTV2_FORMAT_4x2048x1080p_2500:
		case NTV2_FORMAT_4x2048x1080p_2997:
		case NTV2_FORMAT_4x2048x1080p_3000:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_CORVID24:
		switch(format)
		{
		case NTV2_FORMAT_525_5994:
		case NTV2_FORMAT_625_5000:
		case NTV2_FORMAT_720p_2398:
		case NTV2_FORMAT_720p_2500:
		case NTV2_FORMAT_720p_5000:
		case NTV2_FORMAT_720p_5994:
		case NTV2_FORMAT_720p_6000:  
		case NTV2_FORMAT_1080i_5000:
		case NTV2_FORMAT_1080i_5994:
		case NTV2_FORMAT_1080i_6000:
		case NTV2_FORMAT_1080psf_2398:
		case NTV2_FORMAT_1080psf_2400:
		case NTV2_FORMAT_1080psf_2500_2:
		case NTV2_FORMAT_1080psf_2997_2:
		case NTV2_FORMAT_1080psf_3000_2:
		case NTV2_FORMAT_1080p_2398:
		case NTV2_FORMAT_1080p_2400:
		case NTV2_FORMAT_1080p_2500:
		case NTV2_FORMAT_1080p_2997:
		case NTV2_FORMAT_1080p_3000:
		case NTV2_FORMAT_1080p_5000_A:
		case NTV2_FORMAT_1080p_5000_B:
		case NTV2_FORMAT_1080p_5994_A:
		case NTV2_FORMAT_1080p_5994_B:
		case NTV2_FORMAT_1080p_6000_A:
		case NTV2_FORMAT_1080p_6000_B:
		case NTV2_FORMAT_1080psf_2K_2398:
		case NTV2_FORMAT_1080psf_2K_2400:
		case NTV2_FORMAT_1080psf_2K_2500:
		case NTV2_FORMAT_1080p_2K_2398:
		case NTV2_FORMAT_1080p_2K_2400:
		case NTV2_FORMAT_1080p_2K_2500:
		case NTV2_FORMAT_1080p_2K_2997:
		case NTV2_FORMAT_1080p_2K_3000:
		case NTV2_FORMAT_4x1920x1080psf_2398:
		case NTV2_FORMAT_4x1920x1080psf_2400:
		case NTV2_FORMAT_4x1920x1080psf_2500:
		case NTV2_FORMAT_4x1920x1080p_2398:
		case NTV2_FORMAT_4x1920x1080p_2400:
		case NTV2_FORMAT_4x1920x1080p_2500:
		case NTV2_FORMAT_4x1920x1080p_2997:
		case NTV2_FORMAT_4x1920x1080p_3000:
		case NTV2_FORMAT_4x2048x1080psf_2398:
		case NTV2_FORMAT_4x2048x1080psf_2400:
		case NTV2_FORMAT_4x2048x1080psf_2500:
		case NTV2_FORMAT_4x2048x1080p_2398:
		case NTV2_FORMAT_4x2048x1080p_2400:
		case NTV2_FORMAT_4x2048x1080p_2500:
		case NTV2_FORMAT_4x2048x1080p_2997:
		case NTV2_FORMAT_4x2048x1080p_3000:
			return true;
		default:
			return false;
		}

	default:
		return false;
	}

}	//	NTV2DeviceCanDoVideoFormat


bool NTV2DeviceCanDoFrameBufferFormat (NTV2DeviceID boardID, NTV2FrameBufferFormat fmt)
{
	switch (boardID)
	{
	default:
		return false;

	case DEVICE_ID_CORVID1:
		switch (fmt)
		{
		case NTV2_FBF_10BIT_YCBCR:
		case NTV2_FBF_8BIT_YCBCR:
		case NTV2_FBF_8BIT_YCBCR_YUY2:
			return true;
		default:
			return false;
		}

	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IOXT:
		switch (fmt)
		{
		case NTV2_FBF_10BIT_YCBCR:
		case NTV2_FBF_8BIT_YCBCR:
		case NTV2_FBF_ARGB:
		case NTV2_FBF_RGBA:
		case NTV2_FBF_10BIT_RGB:
		case NTV2_FBF_8BIT_YCBCR_YUY2:
		case NTV2_FBF_ABGR:
		case NTV2_FBF_10BIT_DPX:
		case NTV2_FBF_8BIT_DVCPRO:
		case NTV2_FBF_8BIT_HDV:
		case NTV2_FBF_24BIT_RGB:
		case NTV2_FBF_24BIT_BGR:
		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
		case NTV2_FBF_48BIT_RGB:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
		switch (fmt)
		{
		case NTV2_FBF_10BIT_YCBCR:
		case NTV2_FBF_8BIT_YCBCR:
		case NTV2_FBF_ARGB:
		case NTV2_FBF_RGBA:
		case NTV2_FBF_10BIT_RGB:
		case NTV2_FBF_8BIT_YCBCR_YUY2:
		case NTV2_FBF_ABGR:
		case NTV2_FBF_10BIT_DPX:
		case NTV2_FBF_24BIT_RGB:
		case NTV2_FBF_24BIT_BGR:
		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
		switch (fmt)
		{
		case NTV2_FBF_10BIT_YCBCR:
		case NTV2_FBF_8BIT_YCBCR:
		case NTV2_FBF_ARGB:
		case NTV2_FBF_RGBA:
		case NTV2_FBF_10BIT_RGB:
		case NTV2_FBF_8BIT_YCBCR_YUY2:
		case NTV2_FBF_ABGR:
		case NTV2_FBF_10BIT_DPX:
		case NTV2_FBF_8BIT_DVCPRO:
		case NTV2_FBF_8BIT_HDV:
		case NTV2_FBF_24BIT_RGB:
		case NTV2_FBF_24BIT_BGR:
		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
		case NTV2_FBF_48BIT_RGB:
			return true;
		default:
			return false;
		}
		
	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
		switch (fmt)
		{
		case NTV2_FBF_10BIT_YCBCR:
		case NTV2_FBF_8BIT_YCBCR:
		case NTV2_FBF_ARGB:
		case NTV2_FBF_RGBA:
		case NTV2_FBF_10BIT_RGB:
		case NTV2_FBF_8BIT_YCBCR_YUY2:
		case NTV2_FBF_ABGR:
		case NTV2_FBF_10BIT_DPX:
		case NTV2_FBF_8BIT_DVCPRO:
		case NTV2_FBF_8BIT_HDV:
		case NTV2_FBF_24BIT_RGB:
		case NTV2_FBF_24BIT_BGR:
		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
		case NTV2_FBF_48BIT_RGB:
		case NTV2_FBF_10BIT_RAW_RGB:
		case NTV2_FBF_10BIT_RAW_YCBCR:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		switch(fmt)
		{
		case NTV2_FBF_10BIT_YCBCR:
		case NTV2_FBF_8BIT_YCBCR:
		case NTV2_FBF_ARGB:
		case NTV2_FBF_RGBA:
		case NTV2_FBF_10BIT_RGB:
		case NTV2_FBF_8BIT_YCBCR_YUY2:
		case NTV2_FBF_ABGR:
		case NTV2_FBF_10BIT_DPX:
		case NTV2_FBF_8BIT_DVCPRO:
		case NTV2_FBF_8BIT_HDV:
		case NTV2_FBF_24BIT_RGB:
		case NTV2_FBF_24BIT_BGR:
		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
			return true;
		default:
			return false;
		}

	case DEVICE_ID_CORVID24:
		switch (fmt)
		{
		case NTV2_FBF_10BIT_YCBCR:
		case NTV2_FBF_8BIT_YCBCR:
		case NTV2_FBF_ARGB:
		case NTV2_FBF_RGBA:
		case NTV2_FBF_10BIT_RGB:
		case NTV2_FBF_8BIT_YCBCR_YUY2:
		case NTV2_FBF_ABGR:
		case NTV2_FBF_10BIT_DPX:
		case NTV2_FBF_24BIT_RGB:
		case NTV2_FBF_24BIT_BGR:
		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
		case NTV2_FBF_48BIT_RGB:
			return true;
		default:
			return false;
		}
		
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_TTAP:
		switch (fmt)
		{
		case NTV2_FBF_10BIT_YCBCR:
		case NTV2_FBF_8BIT_YCBCR:
		case NTV2_FBF_8BIT_YCBCR_YUY2:
		case NTV2_FBF_8BIT_DVCPRO:
		case NTV2_FBF_8BIT_HDV:
			return true;
		default:
			return false;
		}
	}
	
	return false;
}


bool NTV2DeviceCanDoConversionMode( NTV2DeviceID boardID, NTV2ConversionMode conversionMode )
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
		switch (conversionMode)
		{
		case NTV2_1080i_5994to525_5994:
		case NTV2_1080i_2500to625_2500:
		case NTV2_720p_5994to525_5994:
		case NTV2_720p_5000to625_2500:
		case NTV2_525_5994to1080i_5994:
		case NTV2_525_5994to720p_5994:
		case NTV2_625_2500to1080i_2500:
		case NTV2_625_2500to720p_5000:
		case NTV2_720p_5000to1080i_2500:
		case NTV2_720p_5994to1080i_5994:
		case NTV2_720p_6000to1080i_3000:
		case NTV2_1080i2398to525_2398:
		case NTV2_1080i2398to525_2997:
		case NTV2_1080i_2500to720p_5000:
		case NTV2_1080i_5994to720p_5994:
		case NTV2_1080i_3000to720p_6000:
		case NTV2_1080i_2398to720p_2398:
		case NTV2_720p_2398to1080i_2398:
		case NTV2_525_2398to1080i_2398:
		case NTV2_525_5994to525_5994:
		case NTV2_625_2500to625_2500:
			return true;
		
		default:
			return false;
		}
	#endif	//	!defined (NTV2_DEPRECATE)

	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
		switch (conversionMode)
		{
		case NTV2_1080i_5994to525_5994:
		case NTV2_1080i_2500to625_2500:
		case NTV2_720p_5994to525_5994:
		case NTV2_720p_5000to625_2500:
		case NTV2_525_5994to1080i_5994:
		case NTV2_525_5994to720p_5994:
		case NTV2_625_2500to1080i_2500:
		case NTV2_625_2500to720p_5000:
		case NTV2_720p_5000to1080i_2500:
		case NTV2_720p_5994to1080i_5994:
		case NTV2_720p_6000to1080i_3000:
		case NTV2_1080i2398to525_2398:
		case NTV2_1080i2398to525_2997:
		case NTV2_1080i2400to525_2400:
		case NTV2_1080p2398to525_2398:
		case NTV2_1080p2398to525_2997:
		case NTV2_1080p2400to525_2400:
		case NTV2_1080i_2500to720p_5000:
		case NTV2_1080i_5994to720p_5994:
		case NTV2_1080i_3000to720p_6000:
		case NTV2_1080i_2398to720p_2398:
		case NTV2_720p_2398to1080i_2398:
		case NTV2_525_2398to1080i_2398:
		case NTV2_525_5994to525_5994:
		case NTV2_625_2500to625_2500:
		case NTV2_525_5994to525psf_2997:
		case NTV2_625_5000to625psf_2500:
			return true;
			
		case NTV2_1080i_5000to1080psf_2500:
		case NTV2_1080i_5994to1080psf_2997:
		case NTV2_1080i_6000to1080psf_3000:
	#if !defined (NTV2_DEPRECATE)
			return (boardID != DEVICE_ID_LHI && boardID != BOARD_ID_LHI_T && boardID != DEVICE_ID_IOXT);		// disable for LHi/IoXT
	#else
			return (boardID != DEVICE_ID_LHI && boardID != DEVICE_ID_IOXT);		// disable for LHi/IoXT
	#endif

		default:
			return false;
		}

	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
		switch (conversionMode)
		{
		case NTV2_1080i_5994to525_5994:
		case NTV2_1080i_2500to625_2500:
		case NTV2_720p_5994to525_5994:
		case NTV2_720p_5000to625_2500:
		case NTV2_525_5994to1080i_5994:
		case NTV2_525_5994to720p_5994:
		case NTV2_625_2500to1080i_2500:
		case NTV2_625_2500to720p_5000:
		case NTV2_720p_5000to1080i_2500:
		case NTV2_720p_5994to1080i_5994:
		case NTV2_720p_6000to1080i_3000:
		case NTV2_1080i2398to525_2398:
		case NTV2_1080i2398to525_2997:
		case NTV2_1080i2400to525_2400:
		case NTV2_1080p2398to525_2398:
		case NTV2_1080p2398to525_2997:
		case NTV2_1080p2400to525_2400:
		case NTV2_1080i_2500to720p_5000:
		case NTV2_1080i_5994to720p_5994:
		case NTV2_1080i_3000to720p_6000:
		case NTV2_1080i_2398to720p_2398:
		case NTV2_720p_2398to1080i_2398:
		case NTV2_525_2398to1080i_2398:
		case NTV2_525_5994to525_5994:
		case NTV2_625_2500to625_2500:
		case NTV2_525_5994to525psf_2997:
		case NTV2_625_5000to625psf_2500:
		case NTV2_1080p_3000to720p_6000:
			return true;
		default:
			return false;
		}
	#endif	//	!defined (NTV2_DEPRECATE)

	case DEVICE_ID_LHE_PLUS:
		switch (conversionMode)
		{
		case NTV2_1080i_5994to525_5994:
		case NTV2_1080i2398to525_2997:
		case NTV2_1080i_2500to625_2500:
		case NTV2_720p_5994to525_5994:
		case NTV2_720p_5000to625_2500:
			return true;
		
		default:
			return false;
		}
		
	case DEVICE_ID_IOEXPRESS:
		switch (conversionMode)
		{
		case NTV2_1080i_5994to525_5994:
		case NTV2_1080i2398to525_2997:
		case NTV2_1080i_2500to625_2500:
		case NTV2_720p_5994to525_5994:
		case NTV2_720p_5000to625_2500:
			return true;
		
		default:
			return false;
		}

	case DEVICE_ID_TTAP:
		switch (conversionMode)
		{
		case NTV2_1080psf_2398to1080i_5994:
		case NTV2_1080psf_2400to1080i_3000:
		case NTV2_1080psf_2500to1080i_2500:
		case NTV2_1080p_2398to1080i_5994:
		case NTV2_1080p_2400to1080i_3000:
		case NTV2_1080p_2500to1080i_2500:
		//	return true;					// TBD: enabled at a later date
		default:
			return false;
		}
		
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
	default:
		return false;
	}
}

bool NTV2DeviceCanDoPIO(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHI:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return false;
	default:
		return true;
	}
}						

bool NTV2DeviceCanDoMSI(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_IOXT:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}

bool NTV2DeviceCanDoThunderbolt(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_IOXT:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
		return true;
	default:
		return false;
	}
}

bool NTV2DeviceIsExternalToHost(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_IOEXPRESS:
		return true;
	default:
		return NTV2DeviceCanDoThunderbolt (boardID);
	}
}

bool NTV2DeviceCanDoQREZ(NTV2DeviceID boardID)
{
	(void) boardID;
	return false;
}

	// Can the board hardware squeeze and stretch between
	// 1920x1080 <=> 1280x1080, and 1280x720 <=> 960x720.
bool NTV2DeviceCanDoDVCProHD(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
		return true;
	default:
		return false;
	}

}

	// Can the board hardware squeeze and stretch between
	// 1920x1080 <=> 1440x1080
bool NTV2DeviceCanDoHDV(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_T:
	case BOARD_ID_LHI_DVI:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_TTAP:
		return true;
	default:
		return false;
	}

}	


	// Can the board do ProRes (compressed buffer format)
bool NTV2DeviceCanDoProRes(NTV2DeviceID boardID)
{
	#if !defined (NTV2_DEPRECATE)
		switch (boardID)
		{
		case BOARD_ID_BORG:
		case BOARD_ID_BONES:
		case BOARD_ID_BARCLAY:
		case BOARD_ID_MOAB:
			return true;
		default:
			return false;
		}
	#else
		(void) boardID;
		return false;
	#endif
}					


	// Can the board pixel-double and line-double quarter-sized frames (not
	// the same as QRez, which is a distinct frame buffer format. "Dynamic RT"
	// works with ANY frame buffer format and is controlled by a separate
	// register bit) 
bool NTV2DeviceCanDoQuarterExpand(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}

}						

bool NTV2DeviceCanDoVideoProcessing(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	// they all do so far.
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_TTAP:
		return false;
	default:
		return true;
	}
}	

// Does the board have DualLink (10-bit RGB) I/O
bool NTV2DeviceCanDoDualLink(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}

	// Does the board have programmable LUTs
bool NTV2DeviceCanDoColorCorrection(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}	

	// Does the board have programmable Color-Space Converter coefficients
bool NTV2DeviceCanDoProgrammableCSC(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}	


bool NTV2DeviceCanDoProgrammableRS422(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_KONA4:
		return true;
	default:
		return false;
	}
}


bool NTV2DeviceCanDoRP188(NTV2DeviceID boardID)
{
	(void) boardID;
	return true;
}

	// for ARGB frame buffer formats, can the board send RGB to one output
	// while sending the alpha channel (as a "key") to a second output
bool NTV2DeviceCanDoRGBPlusAlphaOut(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}

UWord NTV2DeviceGetNumVideoInputs(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_CORVID88:
		return 8;
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_CORVID44:
		return 4;
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_IO4KUFC:
		return 2;
	case DEVICE_ID_TTAP:
		return 0;
	case DEVICE_ID_CORVID3G:
	default:
		return 1;
	}
}

UWord NTV2DeviceGetNumVideoOutputs(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_CORVID88:
		return 8;
	case DEVICE_ID_IO4K:
		return 5;
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_CORVID44:
		return 4;
	case DEVICE_ID_IO4KUFC:
		return 3;
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_KONA4UFC:
		return 2;
	default:
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_TTAP:
		return 1;
	}
}

UWord NTV2DeviceGetNumAnlgVideoOutputs(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
		return 1;
	default:
		return 0;
	}
}

UWord NTV2DeviceGetNumAnlgVideoInputs(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_LHE_PLUS:
		return 1;
	default:
		return 0;
	}
}

UWord NTV2DeviceGetNumHDMIVideoOutputs(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
		return 1;
	default:
		return 0;
	}
}

UWord NTV2DeviceGetNumHDMIVideoInputs(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
		return 1;
	default:
		return 0;
	}
}

UWord NTV2DeviceGetNumReferenceVideoInputs(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_TTAP:
		return 0;
	default:
		return 1;
	}
}

UWord NTV2DeviceGetNumInputConverters(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
		return 1;
	default:
		return 0;
	}
}

UWord NTV2DeviceGetNumOutputConverters(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
		return 1;
	default:
		return 0;
	}
}

UWord NTV2DeviceGetNumUpConverters(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
		return 1;
	default:
		return 0;
	}
}


UWord NTV2DeviceGetNumCrossConverters(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
		return 1;
	default:
		return 0;
	}
}


UWord NTV2DeviceGetNumDownConverters(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
		return 1;
	default:
		return 0;
	}
}

UWord NTV2DeviceGetNum4kQuarterSizeConverters(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
		return 1;
	default:
		return 0;
	}
}


UWord NTV2DeviceGetNumFrameStores(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_CORVID88:
		return 8;
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_CORVID44:
		return 4;
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_TTAP:
		return 1;
	default:
		return 2;
	}
}

UWord NTV2DeviceGetNumCSCs(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_CORVID88:
		return 8;
	case DEVICE_ID_IO4K:
		return 5;
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_CORVID44:
		return 4;
	case DEVICE_ID_IO4KUFC:
		return 3;
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_KONA3G:
		return 2;
	case DEVICE_ID_LHE_PLUS:
		return 1;
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_TTAP:
	default:
		return 0;
	}
}

UWord NTV2DeviceGetNumLUTs(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_CORVID88:
		return 8;
	case DEVICE_ID_IO4K:
		return 5;
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_CORVID44:
		return 4;
	case DEVICE_ID_IO4KUFC:
		return 3;
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_KONA4UFC:
		return 2;
	case DEVICE_ID_LHE_PLUS:
		return 1;
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_TTAP:
	default:
		return 0;
	}
}

UWord NTV2DeviceGetNumFrameSyncs(NTV2DeviceID boardID)
{
	#if !defined (NTV2_DEPRECATE)
	if (boardID == BOARD_ID_XENA2)
		return 2;
	#else
		(void) boardID;
	#endif	//	!defined (NTV2_DEPRECATE)
	return 0;
}

UWord NTV2DeviceGetDownConverterDelay(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
		return 1;
	default:
		return 0;
	}
}


bool NTV2DeviceCanDoSDVideo(NTV2DeviceID boardID)
{
	switch ( boardID )
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
		
	default:
		return false;
	}
}

bool NTV2DeviceCanDoHDVideo(NTV2DeviceID boardID)
{
	switch ( boardID )
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
		
	default:
		return false;
	}
}

// 2kx1556
bool NTV2DeviceCanDo2KVideo(NTV2DeviceID boardID)
{
	switch ( boardID )
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
		return true;
		
	default:
		return false;
	}
}


bool NTV2DeviceCanDo4KVideo(NTV2DeviceID boardID)
{
	switch ( boardID )
	{
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
		
	default:
		return false;
	}
}


bool NTV2DeviceCanDoAESAudioIn(NTV2DeviceID boardID)
{
	bool result = false;
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
		result = true;
		break;

	default:
		result = false;
		break;
	}
	return result;
}

bool NTV2DeviceCanDoProAudio(NTV2DeviceID boardID)
{
	(void) boardID;
	return false;
}

bool NTV2DeviceCanDoAnalogAudio(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
		case BOARD_ID_LHI_DVI:
		case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
		case DEVICE_ID_LHE_PLUS:
		case DEVICE_ID_LHI:
		case DEVICE_ID_IOEXPRESS:
		case DEVICE_ID_KONA3G:		//	AES/EBU Audio in & out
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_IOXT:			//	Output only
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
			return true;
			
		default:
			return false;
	}
}

bool NTV2DeviceCanDoAnalogVideoIn(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_LHI:
		return true;

	default:
		return false;
	}
}

bool NTV2DeviceCanDoAnalogVideoOut(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
		return true;

	default:
		return false;
	}
}

UWord NTV2GetDACVersion(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
		return 1;
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
		return 2;
	default:
		return 0;
	}
}

UWord NTV2GetHDMIVersion(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_TTAP:
		return 1;
	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
		return 2;
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
		return 3;
	default:
		return 0;
	}
}

bool NTV2DeviceCanDoAudio96K(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
			
	default:
		return false;
	}
}

bool NTV2DeviceCanDoAudio2Channels(NTV2DeviceID boardID)
{
	(void) boardID;
	return false;
}

bool NTV2DeviceCanDoAudio6Channels(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return false;
		break;
		
	default:
		return true;
		break;
	}
}

bool NTV2DeviceCanDoAudio8Channels(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
		
	default:
		return false;
	}
}

// note: this specifies whether a given board type MIGHT have a breakout
// box - you'll need to check Reg 24 bit 27 to see if it is ACTUALLY present
bool NTV2DeviceCanDoBreakoutBox(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
		return true;
		
	default:
		return false;
	}
}

bool NTV2DeviceCanDoIsoConvert(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
		return true;
		
	default:
		return false;
	}
}

bool NTV2DeviceCanDoRateConvert(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_TTAP:
	//	return true;			// TBD: enabled at later date
	default:
		return false;
	}
}

bool NTV2DeviceCanDoStackedAudio(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_TTAP:
		return false;

	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;

	default:
		return false;
	}
}

bool NTV2DeviceCanDo3GOut (NTV2DeviceID boardID, UWord index0)
{
	switch (index0)
	{
	case 0:
		switch (boardID)
		{
		#if !defined (NTV2_DEPRECATE)
		case BOARD_ID_LHI_DVI:
		case BOARD_ID_LHI_T:
		#endif	//	!defined (NTV2_DEPRECATE)
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_CORVID3G:
		case DEVICE_ID_LHI:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_IOXT:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_CORVID44:
			return true;
		default:
			return false;
		}

	case 1:
		switch (boardID)
		{
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_IOXT:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_CORVID44:
			return true;
		default:
			return false;
		}

	case 2:
	case 3:
		switch (boardID)
		{
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_CORVID44:
			return true;
		default:
			return false;
		}

	case 4:
		switch (boardID)
		{
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_CORVID88:
			return true;
		default:
			return false;
		}


	case 5:
	case 6:
	case 7:
		switch (boardID)
		{
		case DEVICE_ID_CORVID88:
			return true;
		default:
			return false;
		}

	default:
		return false;
	}
}	//	NTV2DeviceCanDo3GOut


bool NTV2DeviceCanDoLTCInN (NTV2DeviceID boardID, UWord index0)
{
	switch (index0)
	{
	case 0:
		switch (boardID)
		{
		#if !defined (NTV2_DEPRECATE)
		case BOARD_ID_LHI_DVI:
		case BOARD_ID_LHI_T:
		#endif	//	!defined (NTV2_DEPRECATE)
		case DEVICE_ID_LHI:
		case DEVICE_ID_IOEXPRESS:
		case DEVICE_ID_CORVID1:
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_CORVID3G:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_IOXT:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_CORVID44:
			return true;
		default:
			return false;
		}
		break;

	case 1:
		switch (boardID)
		{
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
			return true;
		default:
			return false;
		}
		break;

	default:
		return false;
	}
}	//	NTV2DeviceCanDoLTCInN


bool NTV2DeviceCanDoLTCOutN(NTV2DeviceID boardID, UWord index0)
{
	switch (index0)
	{
	case 0:
		switch (boardID)
		{
		#if !defined (NTV2_DEPRECATE)
		case BOARD_ID_LHI_DVI:
		case BOARD_ID_LHI_T:
		#endif	//	!defined (NTV2_DEPRECATE)
		case DEVICE_ID_LHI:
		case DEVICE_ID_IOEXPRESS:
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_IOXT:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_CORVID44:
			return true;
		default:
			return false;
		}

	case 1:
		switch (boardID)
		{
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
			return true;
		default:
			return false;
		}

	default:
		return false;
	}
}	//	NTV2DeviceCanDoLTCOutN


// LTC In and Ref In share the same BNC
bool NTV2DeviceCanDoLTCInOnRefPort(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}


// supports dual stream SDI video out
bool NTV2DeviceCanDoStereoOut(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}


// supports dual stream SDI video in
bool NTV2DeviceCanDoStereoIn(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}


// supports HDMI out stereo
bool NTV2DeviceCanDoHDMIOutStereo(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
		return true;
	default:
		return false;
	}
}


bool NTV2DeviceCanDoAudioN (NTV2DeviceID boardID, UWord index0)
{
	switch (index0)
	{
	case 0:
		return true;

	case 1:
		switch (boardID)
		{
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_IOXT:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_CORVID44:
			return true;
		default:
			return false;
		}

	case 2:
	case 3:
		switch (boardID)
		{
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_CORVID44:
			return true;
		default:
			return false;
		}

	case 4:
	case 5:
	case 6:
	case 7:
		switch (boardID)
		{
		case DEVICE_ID_CORVID88:
			return true;
		default:
			return false;
		}

	default:
		return false;
	}
}	//	NTV2DeviceCanDoAudioN


UWord NTV2DeviceGetNumMixers (NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_LHI:
	default:
		return 0;
		break;

	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID3G:
		return 1;
		break;

	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_CORVID44:
		return 2;
		break;

	case DEVICE_ID_CORVID88:
		return 4;
		break;
	}
}	//	NTV2DeviceGetNumMixers


bool NTV2DeviceCanDoRS422N (NTV2DeviceID boardID, NTV2Channel channel)
{
	switch (channel)
	{
	case NTV2_CHANNEL1:
		switch (boardID)
		{
		case DEVICE_ID_TTAP:
			return false;
		default:
			return true;
		}

	case NTV2_CHANNEL2:
		switch (boardID)
		{
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
			return true;
		default:
			return false;
		}

	default:
		return false;
	}
}	//	NTV2DeviceCanDoRS422N


#if !defined (NTV2_DEPRECATE)
	bool NTV2DeviceCanDoUARTN (NTV2DeviceID boardID, UWord index0)
	{
		return NTV2DeviceCanDoRS422N (boardID, (NTV2Channel) index0);
	}
#endif	//	!defined (NTV2_DEPRECATE)


bool NTV2DeviceCanDoLTCEmbeddedN (NTV2DeviceID boardID, UWord index0)
{
	switch (index0)
	{
	case 0:
		switch (boardID)
		{
		#if !defined (NTV2_DEPRECATE)
		case BOARD_ID_XENA2:
		case BOARD_ID_LHI_DVI:
		case BOARD_ID_LHI_T:
		#endif	//	!defined (NTV2_DEPRECATE)
		case DEVICE_ID_LHE_PLUS:
		case DEVICE_ID_LHI:
		case DEVICE_ID_CORVID1:
		case DEVICE_ID_IOEXPRESS:
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_CORVID3G:
		case DEVICE_ID_IOXT:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_CORVID44:
			return true;
		default:
			return false;
		}

	case 1:
		switch (boardID)
		{
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_CORVID44:
			return true;
		default:
			return false;
		}

	case 2:
	case 3:
		switch (boardID)
		{
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_CORVID44:
			return true;
		default:
			return false;
		}

	case 4:
	case 5:
	case 6:
	case 7:
		switch (boardID)
		{
		case DEVICE_ID_CORVID88:
			return true;
		default:
			return false;
		}

	default:
		return false;
	}
}	//	NTV2DeviceCanDoLTCEmbeddedN


bool NTV2DeviceCanDoCapture(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_TTAP:
		return false;
	default:
		return true;
	}
}


bool NTV2DeviceCanDoPlayback(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_TTAP:	// This to remove warning about default with no case
	default:
		return true;
	}
}


UWord NTV2DeviceGetNumAnalogAudioInputChannels(NTV2DeviceID boardID)
{
	UWord result = 0;
	
	switch (boardID)
	{
	case DEVICE_ID_LHE_PLUS:
		result = 2;
		break;
	
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
		result = 2;
		break;

	default:
		result = 0;
		break;
	}

	return result;
}

UWord NTV2DeviceGetNumAESAudioInputChannels(NTV2DeviceID boardID)
{
	UWord result = 0;
	
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
		result = 8;
		break;
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHE_PLUS:
		result = 2;
		break;
	
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
		result = 16;
		break;

	default:
		result = 0;
		break;
	}

	return result;
}

UWord NTV2DeviceGetNumEmbeddedAudioInputChannels(NTV2DeviceID boardID)
{
	UWord result = 0;
	
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHE_PLUS:
		result = 8;
		break;
	
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		result = 16;
		break;
		
	default:
		result = 0;
		break;
	}

	return result;
}

UWord NTV2DeviceGetNumHDMIAudioInputChannels(NTV2DeviceID boardID)
{
	UWord result = 0;
	
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
		result = 8;
		break;

	default:
		result = 0;
		break;
	}

	return result;
}

UWord NTV2DeviceGetNumAnalogAudioOutputChannels(NTV2DeviceID boardID)
{
	UWord result = 0;
	
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHE_PLUS:
		result = 2;
		break;
	
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
		result = 8;
		break;
		
	default:
		result = 0;
		break;
	}

	return result;
}

UWord NTV2DeviceGetNumAESAudioOutputChannels(NTV2DeviceID boardID)
{
	UWord result = 0;
	
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
		result = 8;
		break;
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHE_PLUS:
		result = 2;
		break;

	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
		result = 16;
		break;

	default:
		result = 0;
		break;
	}

	return result;
}

UWord NTV2DeviceGetNumEmbeddedAudioOutputChannels(NTV2DeviceID boardID)
{
	UWord result = 0;
	
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_TTAP:
		result = 8;
		break;

	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		result = 16;
		break;
		
	default:
		result = 0;
		break;
	}

	return result;
}

UWord NTV2DeviceGetNumHDMIAudioOutputChannels(NTV2DeviceID boardID)
{
	UWord result = 0;
	
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
		result = 8;
		break;

	default:
		result = 0;
		break;
	}

	return result;
}

UWord NTV2DeviceGetMaxAudioChannels(NTV2DeviceID boardID)
{
	switch ( boardID )
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_TTAP:
		return 8;
		
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return 16;
		
	default:
		return 0;
	}

}

ULWord NTV2DeviceGetCh2Offset(NTV2DeviceID boardID)
{
	#if !defined (NTV2_DEPRECATE)
	if (boardID == BOARD_ID_XENA2)
		return XENA2_CHANNEL2_OFFSET;
	#else
		(void) boardID;
	#endif	//	!defined (NTV2_DEPRECATE)
	return 0;
}

ULWord NTV2DeviceGetActiveMemorySize(NTV2DeviceID boardID)
{
	// TODO: Add sizes of unstacked boards
	switch(boardID)
	{
	case DEVICE_ID_IOXT:
		return 0xC000000;

	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
		return 0x20000000;

	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
		return 0x37800000;

	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return 0x40000000;

	default:
		return 0;
	}
}

// Overloading not supported by the ANSI C compiler used for Linux drivers.
#if defined(__CPLUSPLUS__) || defined(__cplusplus)
ULWord NTV2DeviceGetFrameBufferSize(NTV2DeviceID boardID)
#else
ULWord NTV2DeviceGetFrameBufferSize_Ex(NTV2DeviceID boardID)
#endif
{
	ULWord frameBufferSize = 0;
	switch ( boardID )
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		frameBufferSize = XENA2_FRAMEBUFFER_SIZE;
		break;
	default:
		frameBufferSize = 0;
		break;
	}
	
	return frameBufferSize;
}

ULWord NTV2DeviceGetFrameBufferSize(NTV2DeviceID boardID, NTV2FrameGeometry frameGeometry, NTV2FrameBufferFormat frameFormat)
{
	ULWord multiplier = 1; // default

	switch ( boardID )
	{
	// based on FrameGeometry
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_TTAP:
		switch ( frameGeometry )
		{
		case NTV2_FG_2048x1080:
		case NTV2_FG_2048x1556:
		case NTV2_FG_2048x1588:
		case NTV2_FG_1920x1112:
		case NTV2_FG_2048x1112:
			multiplier = 2;
			break;
		default:
			break;
		}
		break;
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		switch ( frameGeometry )
		{
		case NTV2_FG_4x1920x1080:
			if(frameFormat == NTV2_FBF_48BIT_RGB || frameFormat == NTV2_FBF_10BIT_ARGB)
				multiplier = 8;
			else
				multiplier = 4;
			break;
		case NTV2_FG_1920x1080:
			if(	frameFormat == NTV2_FBF_10BIT_ARGB || 
				frameFormat == NTV2_FBF_16BIT_ARGB ||
				frameFormat == NTV2_FBF_48BIT_RGB)
				multiplier = 2;
			break;
		case NTV2_FG_4x2048x1080:
			switch(frameFormat)
			{
			case NTV2_FBF_48BIT_RGB:
			case NTV2_FBF_10BIT_ARGB:
			case NTV2_FBF_ARGB:
			case NTV2_FBF_RGBA:
			case NTV2_FBF_10BIT_RGB:
			case NTV2_FBF_ABGR:
			case NTV2_FBF_10BIT_DPX:
			case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:
				multiplier = 8;
				break;
			default:
				multiplier = 4;
				break;
			}
			break;
		case NTV2_FG_2048x1080:
		case NTV2_FG_2048x1556:
		case NTV2_FG_2048x1588:
		case NTV2_FG_1920x1112:
		case NTV2_FG_1920x1114:
		case NTV2_FG_2048x1112:
		case NTV2_FG_2048x1114:
			if(frameFormat == NTV2_FBF_16BIT_ARGB)
				multiplier = 4;
			else
				multiplier = 2;
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}

#if defined(__CPLUSPLUS__) || defined(__cplusplus)
	return (NTV2DeviceGetFrameBufferSize(boardID)* multiplier);
#else
	return (NTV2DeviceGetFrameBufferSize_Ex(boardID)* multiplier);
#endif
}

ULWord NTV2GetNumDMAEngines(NTV2DeviceID boardID)
{
	switch ( boardID )
	{
	#if !defined (NTV2_DEPRECATE)
		case BOARD_ID_XENA2:
			return 4;

		case BOARD_ID_LHI_DVI:
		case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
		case DEVICE_ID_LHI:
		case DEVICE_ID_CORVID22:
			return 3;
				
		case DEVICE_ID_CORVID1:
		case DEVICE_ID_CORVID3G:
		case DEVICE_ID_LHE_PLUS:
		case DEVICE_ID_IOEXPRESS:
		case DEVICE_ID_KONA3G:			// actually has 4 unidirectional engines (two in, two out), when we pair them up there logically two bi directional engines
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_IOXT:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_TTAP:
		case DEVICE_ID_CORVID44:
			return 2;
				
		default:
			return 3;
	}
}

#if defined(__CPLUSPLUS__) || defined(__cplusplus)
ULWord NTV2DeviceGetNumberFrameBuffers(NTV2DeviceID boardID)
#else
ULWord NTV2DeviceGetNumberFrameBuffers_Ex(NTV2DeviceID boardID)
#endif
{
	switch ( boardID )
	{
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_TTAP:
		return 16;
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID3G:
		return 32;
	case DEVICE_ID_IOXT:
		return 24;
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_KONA3GQUAD: //Took this from below quad has no ufc but it still reserves 8 buffers for 3D feature.
	case DEVICE_ID_CORVID24:
		return 64;
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return 111;
	case DEVICE_ID_KONA3G:
		return 56; // ufc uses 8 
	default:
		return 0;
	}
}

ULWord NTV2DeviceGetNumberFrameBuffers(NTV2DeviceID boardID, NTV2FrameGeometry frameGeometry, NTV2FrameBufferFormat frameFormat)
{
	ULWord divisor = 1; // default
	switch ( boardID )
	{
		// based on FrameGeometry
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_TTAP:
		switch ( frameGeometry )
		{
		case NTV2_FG_2048x1080:
		case NTV2_FG_2048x1556:
		case NTV2_FG_2048x1588:
		case NTV2_FG_1920x1112:
		case NTV2_FG_2048x1112:
			divisor = 2;
			break;
		default:
			break;
		}
		break;
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		switch ( frameGeometry )
		{
		case NTV2_FG_4x1920x1080:
		case NTV2_FG_1920x1080:
			if(frameFormat == NTV2_FBF_10BIT_ARGB || frameFormat == NTV2_FBF_16BIT_ARGB
				|| frameFormat == NTV2_FBF_48BIT_RGB)
				divisor = 2;
			break;
		case NTV2_FG_4x2048x1080:
		case NTV2_FG_2048x1080:
		case NTV2_FG_2048x1556:
		case NTV2_FG_2048x1588:
		case NTV2_FG_1920x1112:
		case NTV2_FG_1920x1114:
		case NTV2_FG_2048x1112:
		case NTV2_FG_2048x1114:
			if(frameFormat == NTV2_FBF_16BIT_ARGB)
				divisor = 4;
			else
				divisor = 2;
			break;
		default:
			break;
		}
		if(frameGeometry == NTV2_FG_4x1920x1080 || frameGeometry == NTV2_FG_4x2048x1080)
			divisor *= 4;
		break;
	default:
		break;
	}

	if ( frameFormat == NTV2_FBF_48BIT_RGB )
	{
		divisor = 2;
	}

#if defined(__CPLUSPLUS__) || defined(__cplusplus)
	return (NTV2DeviceGetNumberFrameBuffers(boardID)/divisor);
#else
	return (NTV2DeviceGetNumberFrameBuffers_Ex(boardID)/divisor);
#endif
}

#if defined(__CPLUSPLUS__) || defined(__cplusplus)
ULWord NTV2DeviceGetAudioFrameBuffer(NTV2DeviceID boardID)
#else
ULWord NTV2DeviceGetAudioFrameBuffer_Ex(NTV2DeviceID boardID)
#endif
{
#if defined(__CPLUSPLUS__) || defined(__cplusplus)

	return (NTV2DeviceGetNumberFrameBuffers(boardID) - NTV2DeviceGetNumAudioStreams(boardID));		// audio base is 2 MB buffer at top - 2MB (16 - 1 for 2 MB buffers)

#else		// #if defined(__CPLUSPLUS__) || defined(__cplusplus)

	return (NTV2DeviceGetNumberFrameBuffers_Ex(boardID) - NTV2DeviceGetNumAudioStreams(boardID));		// audio base is 2 MB buffer at top - 2MB (16 - 1 for 2 MB buffers)

#endif		// #if defined(__CPLUSPLUS__) || defined(__cplusplus)
}

#if defined(__CPLUSPLUS__) || defined(__cplusplus)
ULWord NTV2DeviceGetAudioFrameBuffer2(NTV2DeviceID boardID)
#else
ULWord NTV2DeviceGetAudioFrameBuffer2_Ex(NTV2DeviceID boardID)
#endif
{
#if defined(__CPLUSPLUS__) || defined(__cplusplus)
	return (NTV2DeviceGetNumberFrameBuffers(boardID)-2);

#else		// #if defined(__CPLUSPLUS__) || defined(__cplusplus)
	return (NTV2DeviceGetNumberFrameBuffers_Ex(boardID)-2);

#endif		// #if defined(__CPLUSPLUS__) || defined(__cplusplus)
}

ULWord NTV2DeviceGetAudioFrameBuffer(NTV2DeviceID boardID, NTV2FrameGeometry frameGeometry, NTV2FrameBufferFormat frameFormat)
{
	return (NTV2DeviceGetNumberFrameBuffers(boardID,frameGeometry,frameFormat)-1);	// audio base is 2 MB buffer at top  -2MB (16-1 for 2MB buffers)
}

ULWord NTV2DeviceGetAudioFrameBuffer2(NTV2DeviceID boardID, NTV2FrameGeometry frameGeometry, NTV2FrameBufferFormat frameFormat)
{
	return (NTV2DeviceGetNumberFrameBuffers(boardID,frameGeometry,frameFormat)-2);
}

UWord NTV2DeviceGetNumAudioStreams(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_CORVID88:
		return 8;
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_CORVID44:
		return 4;
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_IO4KUFC:
		return 2;
	default:
		return 1;
	}
}

// BUGBUG: This function actually returns the maximum register number, 
// NOT the number of registers!
// Should probably also be ...GetNumRegisters... instead of GetNumberRegisters.
ULWord NTV2DeviceGetNumberRegisters(NTV2DeviceID boardID)
{
	switch ( boardID )
	{
	case DEVICE_ID_CORVID1:
		return kK2RegCS2Coefficients9_10;

	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
		return kRegSDIOut2VPIDB;

	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_TTAP:
		return kRegSDIOut1VPIDB;

	case DEVICE_ID_LHE_PLUS:
		return kRegSDIIn2VPIDB;

	case DEVICE_ID_CORVID3G:
		return kK2RegXptSelectGroup10;

	case DEVICE_ID_CORVID22:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_IOXT:
		return kRegLTC2AnalogBits32_63;

	case DEVICE_ID_KONA3GQUAD:
		return kRegSDIIn4VPIDB;

	case DEVICE_ID_CORVID24:
		return kRegSDIWatchdogKick2;

	case DEVICE_ID_IO4KUFC:
		return kRegHDMIV2i2c2Data;

	case DEVICE_ID_KONA4UFC:
		return kRegRasterizerControl;

	case DEVICE_ID_KONA4:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return kRegReserved511;

	default:
		return 0xA40;
	}
}

bool NTV2DeviceHasColorSpaceConverterOnChannel2 (NTV2DeviceID boardID)
{
	#if !defined (NTV2_DEPRECATE)
	if (boardID == BOARD_ID_XENA2)
		return true;
	#else
		(void) boardID;
	#endif	//	!defined (NTV2_DEPRECATE)
	return false;
}

// Max # of 32 bit words a DMA engine can move at a time
ULWord NTV2DeviceGetMaxTransferCount(NTV2DeviceID boardID)
{
	switch ( boardID )
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:	// Value supposed to be increased for Xena2 to support 2048x1556
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
	default:
		return BIT(21) - 1;
	}
}

bool NTV2DeviceHasNWL (NTV2DeviceID boardID)
{
	switch (boardID)
	{
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_IOXT:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_TTAP:
		case DEVICE_ID_CORVID44:
		case DEVICE_ID_CORVID88:
			return true;
		default:
			return false;
	}
}

// TODO: FS1 ??
// the "LED" bits within the Global Control Register
#define kPingLED_0  BIT(16)
#define kPingLED_1  BIT(17)
#define kPingLED_2  BIT(18)
#define kPingLED_3  BIT(19)

ULWord NTV2DeviceGetPingLED(NTV2DeviceID boardID)
{
	ULWord result = 0;
	
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:		// Xena 2 has only 2 LEDs
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		result = kPingLED_0;	break;
	
	default:
		result = kPingLED_3;	break;		// assume the last of 4 LEDs
	}
	
	return result;
}						


#if !defined (NTV2_DEPRECATE)
	bool NTV2BoardCanDoProcAmp(NTV2DeviceID boardID)
	{
		(void) boardID;
		return false;
	}

	bool NTV2BoardCanDoBrightnessAdjustment(NTV2DeviceID boardID, NTV2LSVideoADCMode videoADCMode)
	{
		(void) boardID;
		(void) videoADCMode;
		return false;
	}

	bool NTV2BoardCanDoContrastAdjustment(NTV2DeviceID boardID, NTV2LSVideoADCMode videoADCMode)
	{
		(void) boardID;
		(void) videoADCMode;
		return false;
	}

	bool NTV2BoardCanDoSaturationAdjustment(NTV2DeviceID boardID, NTV2LSVideoADCMode videoADCMode)
	{
		(void) boardID;
		(void) videoADCMode;
		return false;
	}

	bool NTV2BoardCanDoHueAdjustment(NTV2DeviceID boardID, NTV2LSVideoADCMode videoADCMode)
	{
		(void) boardID;
		(void) videoADCMode;
		return false;
	}
#endif	//	!defined (NTV2_DEPRECATE)


bool NTV2DeviceCanDoDSKMode(NTV2DeviceID boardID, NTV2DSKMode mode)
{
	// compare with NTV2DeviceCanDoVideoProcessing
	bool canDo = false;
	
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		canDo = true;
		break;
	
	case DEVICE_ID_LHE_PLUS:
		canDo = !(	 mode == NTV2_DSKModeGraphicOverMatte
				  || mode == NTV2_DSKModeGraphicOverVideoIn
				  || mode == NTV2_DSKModeGraphicOverFB );
		break;
		
	default:
		canDo = false;
	}
	
	return canDo;
}


bool NTV2DeviceCanDoDSKOpacity(NTV2DeviceID boardID)
{
	bool canDo = false;

	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
		canDo = true;
		break;
		
	default:
		canDo = false;
	}
	
	return canDo;
}

bool NTV2DeviceNeedsRoutingSetup(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}

}

bool NTV2DeviceCanChangeFrameBufferSize(NTV2DeviceID boardID)
{
	switch (boardID)
	{
		// based on FrameGeometry
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}

}


bool NTV2DeviceSoftwareCanChangeFrameBufferSize(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}


AJAExport bool NTV2DeviceGetVideoFormatFromState (NTV2VideoFormat*	value,
												 NTV2FrameRate		framerate,
												 NTV2FrameGeometry	framegeometry, 
												 NTV2Standard		standard,
												 ULWord				smpte372Enabled)
{
	return NTV2DeviceGetVideoFormatFromState_Ex (value, framerate, framegeometry, standard, smpte372Enabled, false);
}


bool NTV2DeviceGetVideoFormatFromState_Ex(	NTV2VideoFormat*	value,
											NTV2FrameRate		frameRate,
											NTV2FrameGeometry	frameGeometry, 
											NTV2Standard		standard,
											ULWord				smpte372Enabled,
											bool				progressivePicture)
{
	*value = NTV2_FORMAT_UNKNOWN;

	switch (standard)
	{
	case NTV2_STANDARD_1080:
		switch (frameRate)
		{
		case NTV2_FRAMERATE_3000:
			if (smpte372Enabled)
				*value = NTV2_FORMAT_1080p_6000_B;
			else if (frameGeometry == NTV2_FG_4x1920x1080)
				*value = NTV2_FORMAT_4x1920x1080psf_3000;
			else if (frameGeometry == NTV2_FG_4x2048x1080)
				*value = NTV2_FORMAT_4x2048x1080psf_3000;
			else
				*value = progressivePicture ? NTV2_FORMAT_1080psf_3000_2 : NTV2_FORMAT_1080i_6000;
			break;
		case NTV2_FRAMERATE_2997:
			if (smpte372Enabled)
				*value = NTV2_FORMAT_1080p_5994_B;
			else if (frameGeometry == NTV2_FG_4x1920x1080)
				*value = NTV2_FORMAT_4x1920x1080psf_2997;
			else if (frameGeometry == NTV2_FG_4x2048x1080)
				*value = NTV2_FORMAT_4x2048x1080psf_2997;
			else
				*value = progressivePicture ? NTV2_FORMAT_1080psf_2997_2: NTV2_FORMAT_1080i_5994;
			break;
		case NTV2_FRAMERATE_2500:
			if (smpte372Enabled)
				*value = NTV2_FORMAT_1080p_5000_B;
			else if (frameGeometry == NTV2_FG_2048x1080 || frameGeometry == NTV2_FG_2048x1112 || frameGeometry == NTV2_FG_2048x1114)
				*value = NTV2_FORMAT_1080psf_2K_2500;
			else if (frameGeometry == NTV2_FG_4x1920x1080)
				*value = NTV2_FORMAT_4x1920x1080psf_2500;
			else if (frameGeometry == NTV2_FG_4x2048x1080)
				*value = NTV2_FORMAT_4x2048x1080psf_2500;
			else
				*value = progressivePicture ? NTV2_FORMAT_1080psf_2500_2 : NTV2_FORMAT_1080i_5000;
			break;
		case NTV2_FRAMERATE_2400:
			if ( frameGeometry == NTV2_FG_2048x1080 || frameGeometry == NTV2_FG_2048x1112 || frameGeometry == NTV2_FG_2048x1114)
				*value = NTV2_FORMAT_1080psf_2K_2400;
			else if (frameGeometry == NTV2_FG_4x1920x1080)
				*value = NTV2_FORMAT_4x1920x1080psf_2400;
			else if (frameGeometry == NTV2_FG_4x2048x1080)
				*value = NTV2_FORMAT_4x2048x1080psf_2400;
			else
				*value = NTV2_FORMAT_1080psf_2400;
			break;
		case NTV2_FRAMERATE_2398:
			if ( frameGeometry == NTV2_FG_2048x1080 || frameGeometry == NTV2_FG_2048x1112 || frameGeometry == NTV2_FG_2048x1114)
				*value = NTV2_FORMAT_1080psf_2K_2398;
			else if (frameGeometry == NTV2_FG_4x1920x1080)
				*value = NTV2_FORMAT_4x1920x1080psf_2398;
			else if (frameGeometry == NTV2_FG_4x2048x1080)
				*value = NTV2_FORMAT_4x2048x1080psf_2398;
			else
				*value = NTV2_FORMAT_1080psf_2398;
			break;
		default:
			return false;
		}
		break;
		
	case NTV2_STANDARD_1080p:
		switch (frameRate)
		{
		case NTV2_FRAMERATE_3000:
			if ( frameGeometry == NTV2_FG_2048x1080 || frameGeometry == NTV2_FG_2048x1112 || frameGeometry == NTV2_FG_2048x1114)
				*value = NTV2_FORMAT_1080p_2K_3000;
			else if (frameGeometry == NTV2_FG_4x1920x1080)
				*value = NTV2_FORMAT_4x1920x1080p_3000;
			else if (frameGeometry == NTV2_FG_4x2048x1080)
				*value = NTV2_FORMAT_4x2048x1080p_3000;
			else
				*value = NTV2_FORMAT_1080p_3000;
			break;
		case NTV2_FRAMERATE_2997:
			if ( frameGeometry == NTV2_FG_2048x1080 || frameGeometry == NTV2_FG_2048x1112 || frameGeometry == NTV2_FG_2048x1114)
				*value = NTV2_FORMAT_1080p_2K_2997;
			else if (frameGeometry == NTV2_FG_4x1920x1080)
				*value = NTV2_FORMAT_4x1920x1080p_2997;
			else if (frameGeometry == NTV2_FG_4x2048x1080)
				*value = NTV2_FORMAT_4x2048x1080p_2997;
			else
				*value = NTV2_FORMAT_1080p_2997;
			break;
		case NTV2_FRAMERATE_2500:
			if ( frameGeometry == NTV2_FG_2048x1080 || frameGeometry == NTV2_FG_2048x1112 || frameGeometry == NTV2_FG_2048x1114)
				*value = NTV2_FORMAT_1080p_2K_2500;
			else if (frameGeometry == NTV2_FG_4x1920x1080)
				*value = NTV2_FORMAT_4x1920x1080p_2500;
			else if (frameGeometry == NTV2_FG_4x2048x1080)
				*value = NTV2_FORMAT_4x2048x1080p_2500;
			else
				*value = NTV2_FORMAT_1080p_2500;
			break;
		case NTV2_FRAMERATE_2400:
			if ( frameGeometry == NTV2_FG_2048x1080 || frameGeometry == NTV2_FG_2048x1112 || frameGeometry == NTV2_FG_2048x1114)
				*value = NTV2_FORMAT_1080p_2K_2400;
			else if (frameGeometry == NTV2_FG_4x1920x1080)
				*value = NTV2_FORMAT_4x1920x1080p_2400;
			else if (frameGeometry == NTV2_FG_4x2048x1080)
				*value = NTV2_FORMAT_4x2048x1080p_2400;
			else
				*value = NTV2_FORMAT_1080p_2400;
			break;
		case NTV2_FRAMERATE_2398:
			if ( frameGeometry == NTV2_FG_2048x1080 || frameGeometry == NTV2_FG_2048x1112 || frameGeometry == NTV2_FG_2048x1114)
				*value = NTV2_FORMAT_1080p_2K_2398;
			else if (frameGeometry == NTV2_FG_4x1920x1080)
				*value = NTV2_FORMAT_4x1920x1080p_2398;
			else if (frameGeometry == NTV2_FG_4x2048x1080)
				*value = NTV2_FORMAT_4x2048x1080p_2398;
			else
				*value = NTV2_FORMAT_1080p_2398;
			break;
		case NTV2_FRAMERATE_4795:
			if ( frameGeometry == NTV2_FG_4x2048x1080 )
				*value = NTV2_FORMAT_4x2048x1080p_4795;
			else
				*value = NTV2_FORMAT_1080p_2K_4795;
			break;
		case NTV2_FRAMERATE_4800:
			if ( frameGeometry == NTV2_FG_4x2048x1080 )
				*value = NTV2_FORMAT_4x2048x1080p_4800;
			else
				*value = NTV2_FORMAT_1080p_2K_4800;
			break;
		case NTV2_FRAMERATE_5000:
			if ( frameGeometry == NTV2_FG_4x1920x1080 )
				*value = NTV2_FORMAT_4x1920x1080p_5000;
			else if ( frameGeometry == NTV2_FG_4x2048x1080 )
				*value = NTV2_FORMAT_4x2048x1080p_5000;
			else if ( frameGeometry == NTV2_FG_2048x1080 || frameGeometry == NTV2_FG_2048x1112 || frameGeometry == NTV2_FG_2048x1114)
				*value = NTV2_FORMAT_1080p_2K_5000;
			else
				*value = NTV2_FORMAT_1080p_5000_A;
			break;
		case NTV2_FRAMERATE_5994:
			if ( frameGeometry == NTV2_FG_4x1920x1080 )
				*value = NTV2_FORMAT_4x1920x1080p_5994;
			else if ( frameGeometry == NTV2_FG_4x2048x1080 )
				*value = NTV2_FORMAT_4x2048x1080p_5994;
			else if ( frameGeometry == NTV2_FG_2048x1080 || frameGeometry == NTV2_FG_2048x1112 || frameGeometry == NTV2_FG_2048x1114)
				*value = NTV2_FORMAT_1080p_2K_5994;
			else
				*value = NTV2_FORMAT_1080p_5994_A;
			break;
		case NTV2_FRAMERATE_6000:
			if ( frameGeometry == NTV2_FG_4x1920x1080 )
				*value = NTV2_FORMAT_4x1920x1080p_6000;
			else if ( frameGeometry == NTV2_FG_4x2048x1080 )
				*value = NTV2_FORMAT_4x2048x1080p_6000;
			else if ( frameGeometry == NTV2_FG_2048x1080 || frameGeometry == NTV2_FG_2048x1112 || frameGeometry == NTV2_FG_2048x1114)
				*value = NTV2_FORMAT_1080p_2K_6000;
			else
				*value = NTV2_FORMAT_1080p_6000_A;
			break;
		case NTV2_FRAMERATE_11988:
			if ( frameGeometry == NTV2_FG_4x2048x1080 )
				*value = NTV2_FORMAT_4x2048x1080p_11988;
			break;
		case NTV2_FRAMERATE_12000:
			if ( frameGeometry == NTV2_FG_4x2048x1080 )
				*value = NTV2_FORMAT_4x2048x1080p_12000;
			break;
		default:
			return false;
		}
		break;
		
	case NTV2_STANDARD_720:
		switch (frameRate)
		{
		case NTV2_FRAMERATE_6000:
			*value = NTV2_FORMAT_720p_6000;
			break;
		case NTV2_FRAMERATE_5994:
			*value = NTV2_FORMAT_720p_5994;
			break;
		case NTV2_FRAMERATE_5000:
			*value = NTV2_FORMAT_720p_5000;
			break;
		case NTV2_FRAMERATE_2500:
			*value = NTV2_FORMAT_720p_2500;
			break;            
		case NTV2_FRAMERATE_2398:
			*value = NTV2_FORMAT_720p_2398;
			break;
		default:
			return false;
		}
		break;
		
	case NTV2_STANDARD_525:
		switch ( frameRate )
		{
		case NTV2_FRAMERATE_2997:
			*value = progressivePicture ? NTV2_FORMAT_525psf_2997 : NTV2_FORMAT_525_5994;
			break;
		case NTV2_FRAMERATE_2400:
			*value = NTV2_FORMAT_525_2400;
			break;
		case NTV2_FRAMERATE_2398:
			*value = NTV2_FORMAT_525_2398;
			break;

		default:
			return false;
		}
		break;
		
	case NTV2_STANDARD_625:
		*value = progressivePicture ? NTV2_FORMAT_625psf_2500 : NTV2_FORMAT_625_5000;
		break;
	case NTV2_STANDARD_2K:
		switch (frameRate)
		{
		case NTV2_FRAMERATE_1498:
			*value = NTV2_FORMAT_2K_1498;
			break;
		case NTV2_FRAMERATE_1500:
			*value = NTV2_FORMAT_2K_1500;
			break;
		case NTV2_FRAMERATE_2398:
			*value = NTV2_FORMAT_2K_2398;
			break;
		case NTV2_FRAMERATE_2400:
			*value = NTV2_FORMAT_2K_2400;
			break;
		case NTV2_FRAMERATE_2500:
			*value = NTV2_FORMAT_2K_2500;
			break;

		default:
			return false;
		}
		break;

	default: 
		return false;
	}

	return true;
}

bool NTV2DeviceCanDoFreezeOutput(NTV2DeviceID boardID)
{
	(void) boardID;
	return false;
}

//
// Can change embedded audio clock source.
bool NTV2DeviceCanChangeEmbeddedAudioClock(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
		case BOARD_ID_XENA2:
		case BOARD_ID_LHI_DVI:
		case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
		case DEVICE_ID_LHI:
		case DEVICE_ID_IOEXPRESS:
		case DEVICE_ID_CORVID1:
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_CORVID3G:
		case DEVICE_ID_LHE_PLUS:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_IOXT:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_TTAP:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_CORVID44:
			return true;

		case DEVICE_ID_NOTFOUND:
	#if !defined (NTV2_DEPRECATE)
		default:
	#endif	//	!defined (NTV2_DEPRECATE)
			break;
	}
	return false;
}


bool NTV2DeviceHasSPIFlash(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
		case BOARD_ID_LHI_DVI:
		case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
		case DEVICE_ID_LHI:
		case DEVICE_ID_IOEXPRESS:
		case DEVICE_ID_CORVID1:
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_CORVID3G:
		case DEVICE_ID_LHE_PLUS:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_IOXT:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_TTAP:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_CORVID44:
			return true;

		case DEVICE_ID_NOTFOUND:
	#if !defined (NTV2_DEPRECATE)
		default:
	#endif	//	!defined (NTV2_DEPRECATE)
			break;
	}
	return false;
}

bool NTV2DeviceHasSPIFlashSerial(NTV2DeviceID boardID)
{
	switch(boardID)
	{
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_LHE_PLUS:
		case DEVICE_ID_IOXT:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_TTAP:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_CORVID44:
			return true;

		case DEVICE_ID_CORVID1:
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_CORVID3G:
		case DEVICE_ID_LHI:
		case DEVICE_ID_IOEXPRESS:
		case DEVICE_ID_NOTFOUND:
	#if !defined (NTV2_DEPRECATE)
		default:
	#endif	//	!defined (NTV2_DEPRECATE)
			break;
	}
	return false;
}

bool NTV2DeviceHasSPIv3(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}

bool NTV2DeviceHasSPIv2(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_TTAP:
		return true;
	default:
		return false;
	}
}

bool NTV2DeviceCanDoInputSource(NTV2DeviceID boardID, NTV2InputSource sourceID)
{
	switch(boardID)
	{
	case DEVICE_ID_LHE_PLUS:
		switch(sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
		case NTV2_INPUTSOURCE_ANALOG:
			return true;
		default:
			return false;
		}
		break;

	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
		switch(sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
		case NTV2_INPUTSOURCE_SDI2: 
		case NTV2_INPUTSOURCE_DUALLINK:
			return true;
		default: 
			return false;
		}
		break;
	#endif	//	!defined (NTV2_DEPRECATE)

	case DEVICE_ID_LHI:
		switch (sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
		case NTV2_INPUTSOURCE_ANALOG:
		case NTV2_INPUTSOURCE_HDMI:
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_IOEXPRESS:
		switch (sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
		case NTV2_INPUTSOURCE_HDMI:
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID3G:
		switch(sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_CORVID22:
		switch(sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
		case NTV2_INPUTSOURCE_SDI2:
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_IO4KUFC:
		switch(sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
		case NTV2_INPUTSOURCE_SDI2:
		#if !defined (NTV2_DEPRECATE)
		case NTV2_INPUTSOURCE_DUALLINK:
		case NTV2_INPUTSOURCE_DUALLINK2:
		#endif	//	!defined (NTV2_DEPRECATE)
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_KONA4:
		switch(sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
		case NTV2_INPUTSOURCE_SDI2:
		case NTV2_INPUTSOURCE_SDI3:
		case NTV2_INPUTSOURCE_SDI4:
		#if !defined (NTV2_DEPRECATE)
		case NTV2_INPUTSOURCE_DUALLINK:
		case NTV2_INPUTSOURCE_DUALLINK2:
		case NTV2_INPUTSOURCE_DUALLINK3:
		case NTV2_INPUTSOURCE_DUALLINK4:
		#endif	//	!defined (NTV2_DEPRECATE)
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_IOXT:
		switch(sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
		case NTV2_INPUTSOURCE_SDI2:
		case NTV2_INPUTSOURCE_HDMI:
			return true;
		default:
			return false;
		}
		break;

	case DEVICE_ID_IO4K:
		switch(sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
		case NTV2_INPUTSOURCE_SDI2:
		case NTV2_INPUTSOURCE_SDI3:
		case NTV2_INPUTSOURCE_SDI4:
		case NTV2_INPUTSOURCE_HDMI:
		#if !defined (NTV2_DEPRECATE)
		case NTV2_INPUTSOURCE_DUALLINK:
		case NTV2_INPUTSOURCE_DUALLINK2:
		case NTV2_INPUTSOURCE_DUALLINK3:
		case NTV2_INPUTSOURCE_DUALLINK4:
		#endif	//	!defined (NTV2_DEPRECATE)
			return true;
		default:
			return false;
		}
		break;

	case DEVICE_ID_CORVID24:
		switch(sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
		case NTV2_INPUTSOURCE_SDI2:
		case NTV2_INPUTSOURCE_SDI3:
		case NTV2_INPUTSOURCE_SDI4:
		#if !defined (NTV2_DEPRECATE)
		case NTV2_INPUTSOURCE_DUALLINK:
		case NTV2_INPUTSOURCE_DUALLINK2:
		case NTV2_INPUTSOURCE_DUALLINK3:
		case NTV2_INPUTSOURCE_DUALLINK4:
		#endif	//	!defined (NTV2_DEPRECATE)
			return true;
		default: 
			return false;
		}
		break;

	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
		switch (sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
		case NTV2_INPUTSOURCE_ANALOG:
		case NTV2_INPUTSOURCE_HDMI:
			return true;
		default: 
			return false;
		}
		break;
	#endif	//	!defined (NTV2_DEPRECATE)

	case DEVICE_ID_CORVID88:
		switch (sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
		case NTV2_INPUTSOURCE_SDI2:
		case NTV2_INPUTSOURCE_SDI3:
		case NTV2_INPUTSOURCE_SDI4:
		case NTV2_INPUTSOURCE_SDI5:
		case NTV2_INPUTSOURCE_SDI6:
		case NTV2_INPUTSOURCE_SDI7:
		case NTV2_INPUTSOURCE_SDI8:
		#if !defined (NTV2_DEPRECATE)
		case NTV2_INPUTSOURCE_DUALLINK:
		case NTV2_INPUTSOURCE_DUALLINK2:
		case NTV2_INPUTSOURCE_DUALLINK3:
		case NTV2_INPUTSOURCE_DUALLINK4:
		case NTV2_INPUTSOURCE_DUALLINK5:
		case NTV2_INPUTSOURCE_DUALLINK6:
		case NTV2_INPUTSOURCE_DUALLINK7:
		case NTV2_INPUTSOURCE_DUALLINK8:
		#endif	//	!defined (NTV2_DEPRECATE)
			return true;
		default:
			return false;
		}

	case DEVICE_ID_CORVID44:
		switch (sourceID)
		{
		case NTV2_INPUTSOURCE_SDI1:
		case NTV2_INPUTSOURCE_SDI2:
		case NTV2_INPUTSOURCE_SDI3:
		case NTV2_INPUTSOURCE_SDI4:
		#if !defined (NTV2_DEPRECATE)
		case NTV2_INPUTSOURCE_DUALLINK:
		case NTV2_INPUTSOURCE_DUALLINK2:
		case NTV2_INPUTSOURCE_DUALLINK3:
		case NTV2_INPUTSOURCE_DUALLINK4:
		#endif	//	!defined (NTV2_DEPRECATE)
			return true;
		default:
			return false;
		}

	default:	// board not supported
		break;
	}
	return false;

}


bool NTV2DeviceCanDoWidget(NTV2DeviceID boardID, NTV2WidgetID widgetID)
{
	switch(boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_XENA2:
		switch(widgetID)
		{
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtLUT1:
		case NTV2_WgtLUT2:
		case NTV2_WgtMixer1:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_WgtSDIIn1:
		case NTV2_WgtSDIIn2: 
		case NTV2_WgtDualLinkIn1:
		case NTV2_WgtUpDownConverter1:
		case NTV2_WgtSDIOut1:
		case NTV2_WgtSDIOut2:
		case NTV2_WgtAnalogOut1:
		case NTV2_WgtDualLinkOut1:
		case NTV2_WgtCompression1:
		case NTV2_WgtGenLock:
			return true;
		//#ifdef AJAMac
			case NTV2_WgtFrameSync1:
			case NTV2_WgtFrameSync2:
				return true;
		//#endif
		default: 
			return false;
		}
		break;
	#endif	//	!defined (NTV2_DEPRECATE)

	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
		switch (widgetID)
		{
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtLUT1:
		case NTV2_WgtLUT2:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_WgtMixer1:
		case NTV2_WgtUpDownConverter1:
		case NTV2_WgtCompression1:
		case NTV2_WgtSDIIn1:
		case NTV2_WgtSDIIn2:
		case NTV2_WgtAnalogIn1:
		case NTV2_WgtHDMIIn1:
		case NTV2_WgtSDIOut1:
		case NTV2_WgtSDIOut2:
		case NTV2_WgtAnalogOut1:
		case NTV2_WgtHDMIOut1:
		case NTV2_WgtGenLock:
			return true;
       //#ifdef AJAMac
		   case NTV2_WgtFrameSync1:
		   case NTV2_WgtFrameSync2:
			  return true;
       //#endif
		default: 
			return false;
		}
		break;
		
	case DEVICE_ID_IOEXPRESS:
		switch (widgetID)
		{
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtCompression1:
		case NTV2_WgtSDIIn1:
		case NTV2_WgtHDMIIn1:
		case NTV2_WgtSDIOut1:
		case NTV2_WgtAnalogOut1:
		case NTV2_WgtHDMIOut1:
		case NTV2_WgtGenLock:
		case NTV2_WgtUpDownConverter1:
			return true;
		//#ifdef AJAMac
			case NTV2_WgtFrameSync1:
			case NTV2_WgtFrameSync2:
				return true;
		//#endif
		default: 
			return false;
		}
		break;

	case DEVICE_ID_CORVID1:
		switch(widgetID)
		{
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_WgtSDIIn1:
		case NTV2_WgtSDIOut1:
		case NTV2_WgtGenLock:
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_CORVID3G:
		switch(widgetID)
		{
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_Wgt3GSDIIn1:
		case NTV2_Wgt3GSDIOut1:
		case NTV2_WgtGenLock:
		case NTV2_WgtMixer1:
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_LHE_PLUS:
		switch(widgetID)
		{
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_WgtSDIIn1:
		case NTV2_WgtSDIOut1:
		case NTV2_WgtSDIOut2:
		case NTV2_WgtGenLock:
		case NTV2_WgtMixer1:
		case NTV2_WgtCSC1:
		case NTV2_WgtCompression1:
		case NTV2_WgtAnalogIn1:
		case NTV2_WgtAnalogOut1:
		case NTV2_WgtUpDownConverter1:
		case NTV2_WgtLUT1:
			return true;
		default:
			return false;
		}
		break;

	case DEVICE_ID_CORVID22:
		switch(widgetID)
		{
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_WgtMixer1:
		case NTV2_WgtMixer2:
		case NTV2_Wgt3GSDIIn1:
		case NTV2_Wgt3GSDIIn2:
		case NTV2_Wgt3GSDIOut1:
		case NTV2_Wgt3GSDIOut2:
		case NTV2_WgtGenLock:
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_KONA3G:
		switch(widgetID)
		{
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtLUT1:
		case NTV2_WgtLUT2:
		case NTV2_WgtMixer1:
		case NTV2_WgtMixer2:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_Wgt3GSDIIn1:
		case NTV2_Wgt3GSDIIn2:
		case NTV2_Wgt3GSDIOut1:
		case NTV2_Wgt3GSDIOut2:
		case NTV2_WgtDualLinkV2In1:
		case NTV2_WgtDualLinkV2In2:
		case NTV2_WgtUpDownConverter1:
		case NTV2_WgtAnalogOut1:
		case NTV2_WgtDualLinkV2Out1:
		case NTV2_WgtDualLinkV2Out2:
		case NTV2_WgtCompression1:
		case NTV2_WgtGenLock:
		case NTV2_WgtHDMIOut1:
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_KONA3GQUAD:
		switch(widgetID)
		{
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtCSC3:
		case NTV2_WgtCSC4:
		case NTV2_WgtLUT1:
		case NTV2_WgtLUT2:
		case NTV2_WgtLUT3:
		case NTV2_WgtLUT4:
		case NTV2_WgtMixer1:
		case NTV2_WgtMixer2:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_WgtFrameBuffer3:
		case NTV2_WgtFrameBuffer4:
		case NTV2_Wgt3GSDIIn1:
		case NTV2_Wgt3GSDIIn2:
		case NTV2_Wgt3GSDIIn3:
		case NTV2_Wgt3GSDIIn4:
		case NTV2_Wgt3GSDIOut1:
		case NTV2_Wgt3GSDIOut2:
		case NTV2_Wgt3GSDIOut3:
		case NTV2_Wgt3GSDIOut4:
		case NTV2_WgtDualLinkV2In1:
		case NTV2_WgtDualLinkV2In2:
		case NTV2_WgtDualLinkV2In3:
		case NTV2_WgtDualLinkV2In4:
		case NTV2_WgtAnalogOut1:
		case NTV2_WgtDualLinkV2Out1:
		case NTV2_WgtDualLinkV2Out2:
		case NTV2_WgtDualLinkV2Out3:
		case NTV2_WgtDualLinkV2Out4:
		case NTV2_WgtGenLock:
		case NTV2_WgtHDMIOut1:
		case NTV2_WgtCompression1:
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_IOXT:
		switch(widgetID)
		{
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtLUT1:
		case NTV2_WgtLUT2:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_Wgt3GSDIIn1:
		case NTV2_Wgt3GSDIIn2:
		case NTV2_Wgt3GSDIOut1:
		case NTV2_Wgt3GSDIOut2:
		case NTV2_WgtUpDownConverter1:
		case NTV2_WgtAnalogOut1:
		case NTV2_WgtCompression1:
		case NTV2_WgtGenLock:
		case NTV2_WgtHDMIIn1:
		case NTV2_WgtHDMIOut1:
		case NTV2_WgtMixer1:
		case NTV2_WgtDualLinkV2In1:
		case NTV2_WgtDualLinkV2In2:
		case NTV2_WgtDualLinkV2Out1:
		case NTV2_WgtDualLinkV2Out2:
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_CORVID24:
		switch(widgetID)
		{
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtCSC3:
		case NTV2_WgtCSC4:
		case NTV2_WgtLUT1:
		case NTV2_WgtLUT2:
		case NTV2_WgtLUT3:
		case NTV2_WgtLUT4:
		case NTV2_WgtMixer1:
		case NTV2_WgtMixer2:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_WgtFrameBuffer3:
		case NTV2_WgtFrameBuffer4:
		case NTV2_Wgt3GSDIIn1:
		case NTV2_Wgt3GSDIIn2:
		case NTV2_Wgt3GSDIIn3:
		case NTV2_Wgt3GSDIIn4:
		case NTV2_Wgt3GSDIOut1:
		case NTV2_Wgt3GSDIOut2:
		case NTV2_Wgt3GSDIOut3:
		case NTV2_Wgt3GSDIOut4:
		case NTV2_WgtDualLinkV2In1:
		case NTV2_WgtDualLinkV2In2:
		case NTV2_WgtDualLinkV2In3:
		case NTV2_WgtDualLinkV2In4:
		case NTV2_WgtDualLinkV2Out1:
		case NTV2_WgtDualLinkV2Out2:
		case NTV2_WgtDualLinkV2Out3:
		case NTV2_WgtDualLinkV2Out4:
		case NTV2_WgtGenLock:
			return true;
		default: 
			return false;
		}
		break;

	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
		switch (widgetID)
		{
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtLUT1:
		case NTV2_WgtLUT2:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_WgtMixer1:
		case NTV2_WgtUpDownConverter1:
		case NTV2_WgtSDIIn1:
		case NTV2_WgtSDIIn2:
		case NTV2_WgtAnalogIn1:
		case NTV2_WgtHDMIIn1:
		case NTV2_WgtSDIOut1:
		case NTV2_WgtSDIOut2:
		case NTV2_WgtAnalogOut1:
		case NTV2_WgtHDMIOut1:
		case NTV2_WgtGenLock:
			return true;
			//#ifdef AJAMac
		case NTV2_WgtFrameSync1:
		case NTV2_WgtFrameSync2:
			return true;
			//#endif
		default: 
			return false;
		}
		break;
	#endif	//	!defined (NTV2_DEPRECATE)

	case DEVICE_ID_TTAP:
		switch (widgetID)
		{
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtCompression1:
		case NTV2_WgtSDIOut1:
		case NTV2_WgtHDMIOut1:
		case NTV2_WgtGenLock:
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_IO4K:
		switch(widgetID)
		{
		case NTV2_Wgt4KDownConverter:
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtCSC3:
		case NTV2_WgtCSC4:
		case NTV2_WgtCSC5:
		case NTV2_WgtLUT1:
		case NTV2_WgtLUT2:
		case NTV2_WgtLUT3:
		case NTV2_WgtLUT4:
		case NTV2_WgtLUT5:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_WgtFrameBuffer3:
		case NTV2_WgtFrameBuffer4:
		case NTV2_Wgt3GSDIIn1:
		case NTV2_Wgt3GSDIIn2:
		case NTV2_Wgt3GSDIIn3:
		case NTV2_Wgt3GSDIIn4:
		case NTV2_Wgt3GSDIOut1:
		case NTV2_Wgt3GSDIOut2:
		case NTV2_Wgt3GSDIOut3:
		case NTV2_Wgt3GSDIOut4:
		case NTV2_WgtGenLock:
		case NTV2_WgtHDMIIn1v2:
		case NTV2_WgtHDMIOut1v2:
		case NTV2_WgtMixer1:
		case NTV2_WgtMixer2:
		case NTV2_WgtDualLinkV2In1:
		case NTV2_WgtDualLinkV2In2:
		case NTV2_WgtDualLinkV2In3:
		case NTV2_WgtDualLinkV2In4:
		case NTV2_WgtDualLinkV2Out1:
		case NTV2_WgtDualLinkV2Out2:
		case NTV2_WgtDualLinkV2Out3:
		case NTV2_WgtDualLinkV2Out4:
		case NTV2_WgtDualLinkV2Out5:
		case NTV2_WgtSDIMonOut1:
		case NTV2_Wgt425Mux1:
		case NTV2_Wgt425Mux2:
		case NTV2_Wgt425Mux3:
		case NTV2_Wgt425Mux4:
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_KONA4:
		switch(widgetID)
		{
		case NTV2_Wgt4KDownConverter:
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtCSC3:
		case NTV2_WgtCSC4:
		case NTV2_WgtLUT1:
		case NTV2_WgtLUT2:
		case NTV2_WgtLUT3:
		case NTV2_WgtLUT4:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_WgtFrameBuffer3:
		case NTV2_WgtFrameBuffer4:
		case NTV2_Wgt3GSDIIn1:
		case NTV2_Wgt3GSDIIn2:
		case NTV2_Wgt3GSDIIn3:
		case NTV2_Wgt3GSDIIn4:
		case NTV2_Wgt3GSDIOut1:
		case NTV2_Wgt3GSDIOut2:
		case NTV2_Wgt3GSDIOut3:
		case NTV2_Wgt3GSDIOut4:
		case NTV2_WgtGenLock:
		case NTV2_WgtHDMIOut1v2:
		case NTV2_WgtMixer1:
		case NTV2_WgtMixer2:
		case NTV2_WgtDualLinkV2In1:
		case NTV2_WgtDualLinkV2In2:
		case NTV2_WgtDualLinkV2In3:
		case NTV2_WgtDualLinkV2In4:
		case NTV2_WgtDualLinkV2Out1:
		case NTV2_WgtDualLinkV2Out2:
		case NTV2_WgtDualLinkV2Out3:
		case NTV2_WgtDualLinkV2Out4:
		case NTV2_WgtAnalogOut1:
		case NTV2_Wgt425Mux1:
		case NTV2_Wgt425Mux2:
		case NTV2_Wgt425Mux3:
		case NTV2_Wgt425Mux4:
			return true;
		default: 
			return false;
		}
		break;

	case DEVICE_ID_CORVID88:
		switch(widgetID)
		{
		case NTV2_WgtGenLock:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_WgtFrameBuffer3:
		case NTV2_WgtFrameBuffer4:
		case NTV2_WgtFrameBuffer5:
		case NTV2_WgtFrameBuffer6:
		case NTV2_WgtFrameBuffer7:
		case NTV2_WgtFrameBuffer8:
		case NTV2_Wgt3GSDIIn1:
		case NTV2_Wgt3GSDIIn2:
		case NTV2_Wgt3GSDIIn3:
		case NTV2_Wgt3GSDIIn4:
		case NTV2_Wgt3GSDIIn5:
		case NTV2_Wgt3GSDIIn6:
		case NTV2_Wgt3GSDIIn7:
		case NTV2_Wgt3GSDIIn8:
		case NTV2_WgtDualLinkV2In1:
		case NTV2_WgtDualLinkV2In2:
		case NTV2_WgtDualLinkV2In3:
		case NTV2_WgtDualLinkV2In4:
		case NTV2_WgtDualLinkV2In5:
		case NTV2_WgtDualLinkV2In6:
		case NTV2_WgtDualLinkV2In7:
		case NTV2_WgtDualLinkV2In8:
		case NTV2_Wgt3GSDIOut1:
		case NTV2_Wgt3GSDIOut2:
		case NTV2_Wgt3GSDIOut3:
		case NTV2_Wgt3GSDIOut4:
		case NTV2_Wgt3GSDIOut5:
		case NTV2_Wgt3GSDIOut6:
		case NTV2_Wgt3GSDIOut7:
		case NTV2_Wgt3GSDIOut8:
		case NTV2_WgtDualLinkV2Out1:
		case NTV2_WgtDualLinkV2Out2:
		case NTV2_WgtDualLinkV2Out3:
		case NTV2_WgtDualLinkV2Out4:
		case NTV2_WgtDualLinkV2Out5:
		case NTV2_WgtDualLinkV2Out6:
		case NTV2_WgtDualLinkV2Out7:
		case NTV2_WgtDualLinkV2Out8:
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtCSC3:
		case NTV2_WgtCSC4:
		case NTV2_WgtCSC5:
		case NTV2_WgtCSC6:
		case NTV2_WgtCSC7:
		case NTV2_WgtCSC8:
		case NTV2_WgtLUT1:
		case NTV2_WgtLUT2:
		case NTV2_WgtLUT3:
		case NTV2_WgtLUT4:
		case NTV2_WgtLUT5:
		case NTV2_WgtLUT6:
		case NTV2_WgtLUT7:
		case NTV2_WgtLUT8:
		case NTV2_WgtMixer1:
		case NTV2_WgtMixer2:
		case NTV2_WgtMixer3:
		case NTV2_WgtMixer4:
		case NTV2_Wgt425Mux1:
		case NTV2_Wgt425Mux2:
		case NTV2_Wgt425Mux3:
		case NTV2_Wgt425Mux4:
			return true;
		default:
			return false;
		}
		break;

	case DEVICE_ID_CORVID44:
		switch(widgetID)
		{
		case NTV2_WgtGenLock:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_WgtFrameBuffer3:
		case NTV2_WgtFrameBuffer4:
		case NTV2_Wgt3GSDIIn1:
		case NTV2_Wgt3GSDIIn2:
		case NTV2_Wgt3GSDIIn3:
		case NTV2_Wgt3GSDIIn4:
		case NTV2_WgtDualLinkV2In1:
		case NTV2_WgtDualLinkV2In2:
		case NTV2_WgtDualLinkV2In3:
		case NTV2_WgtDualLinkV2In4:
		case NTV2_Wgt3GSDIOut1:
		case NTV2_Wgt3GSDIOut2:
		case NTV2_Wgt3GSDIOut3:
		case NTV2_Wgt3GSDIOut4:
		case NTV2_WgtDualLinkV2Out1:
		case NTV2_WgtDualLinkV2Out2:
		case NTV2_WgtDualLinkV2Out3:
		case NTV2_WgtDualLinkV2Out4:
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtCSC3:
		case NTV2_WgtCSC4:
		case NTV2_WgtLUT1:
		case NTV2_WgtLUT2:
		case NTV2_WgtLUT3:
		case NTV2_WgtLUT4:
		case NTV2_WgtMixer1:
		case NTV2_WgtMixer2:
		case NTV2_Wgt425Mux1:
		case NTV2_Wgt425Mux2:
		case NTV2_Wgt425Mux3:
		case NTV2_Wgt425Mux4:
			return true;
		default:
			return false;
		}
		break;

	case DEVICE_ID_IO4KUFC:
		switch(widgetID)
		{
		case NTV2_WgtLUT5:
		case NTV2_WgtCSC5:
		case NTV2_WgtSDIMonOut1:
		case NTV2_WgtHDMIIn1v3:
		case NTV2_WgtUpDownConverter1:
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtLUT1:
		case NTV2_WgtLUT2:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_Wgt3GSDIIn1:
		case NTV2_Wgt3GSDIIn2:
		case NTV2_Wgt3GSDIOut1:
		case NTV2_Wgt3GSDIOut2:
		case NTV2_WgtCompression1:
		case NTV2_WgtGenLock:
		case NTV2_WgtHDMIOut1v3:
		case NTV2_WgtMixer1:
		case NTV2_WgtMixer2:
		case NTV2_WgtDualLinkV2In1:
		case NTV2_WgtDualLinkV2In2:
		case NTV2_WgtDualLinkV2Out1:
		case NTV2_WgtDualLinkV2Out2:
		case NTV2_WgtDualLinkV2Out5:
			return true;
		default:
			return false;
		}
		break;

	case DEVICE_ID_KONA4UFC:
		switch(widgetID)
		{
		case NTV2_WgtUpDownConverter1:
		case NTV2_WgtCSC1:
		case NTV2_WgtCSC2:
		case NTV2_WgtLUT1:
		case NTV2_WgtLUT2:
		case NTV2_WgtFrameBuffer1:
		case NTV2_WgtFrameBuffer2:
		case NTV2_Wgt3GSDIIn1:
		case NTV2_Wgt3GSDIIn2:
		case NTV2_Wgt3GSDIOut1:
		case NTV2_Wgt3GSDIOut2:
		case NTV2_WgtCompression1:
		case NTV2_WgtGenLock:
		case NTV2_WgtHDMIOut1v3:
		case NTV2_WgtMixer1:
		case NTV2_WgtMixer2:
		case NTV2_WgtDualLinkV2In1:
		case NTV2_WgtDualLinkV2In2:
		case NTV2_WgtDualLinkV2Out1:
		case NTV2_WgtDualLinkV2Out2:
		case NTV2_WgtDualLinkV2Out5:
		case NTV2_WgtAnalogOut1:
			return true;

		default: 
			return false;
		}
		break;

		default:	// board not supported
			break;
	}
	return false;
}

bool NTV2DeviceCanDoLTC(NTV2DeviceID boardID)
{
	switch (boardID)
	{
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;

	default:
		return false;
	}
}

bool NTV2DeviceHasPCIeGen2(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_TTAP:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}


bool NTV2DeviceHasBiDirectionalSDI(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}


ULWord NTV2DeviceGetNumVideoChannels(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_TTAP:
		return 1;
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_CORVID44:
		return 4;
	case DEVICE_ID_CORVID88:
		return 8;
	default:
		return 2;
	}
}

ULWord NTV2DeviceGetHDMIVersion(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_IO4KUFC:
		return 3;
	case DEVICE_ID_IO4K:
	case DEVICE_ID_KONA4:
		return 2;
	#if !defined (NTV2_DEPRECATE)
	case BOARD_ID_LHI_DVI:
	case BOARD_ID_LHI_T:
	#endif	//	!defined (NTV2_DEPRECATE)
	case DEVICE_ID_LHI:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_TTAP:
		return 1;
	default:
		return 0;
	}
}

bool NTV2DeviceCanDisableUFC(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_KONA3G:
		return true;
	default:
		return false;
	}
}


ULWord NTV2DeviceGetUFCVersion(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_KONA4UFC:
		return 2;
	default:
		return 1;
	}
}

bool NTV2DeviceHasSDIRelays(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_CORVID24:
		return true;
	default:
		return false;
	}
}

UWord NTV2DeviceGetNumSerialPorts (NTV2DeviceID boardID)
{
	UWord result = 0;
	
	switch (boardID)
	{
		case DEVICE_ID_TTAP:
			break;

		case DEVICE_ID_CORVID22:
		case DEVICE_ID_CORVID24:
			result = 2;
			break;

		default:
			result = 1;
			break;
	}

	return result;
}

bool NTV2DeviceCanDoMultiFormat (NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}

ULWord NTV2DeviceGetLUTVersion(NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return 2;
	default:
		return 1;
	}
}

bool NTV2DeviceCanDo3GLevelConversion (NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_CORVID44:
	case DEVICE_ID_CORVID88:
		return true;
	default:
		return false;
	}
}

bool NTV2DeviceCanDoAudioDelay (NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}

bool NTV2DeviceCanDoPCMControl (NTV2DeviceID boardID)
{
	switch(boardID)
	{
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
		return true;
	default:
		return false;
	}
}

bool NTV2DeviceCanDo425Mux (NTV2DeviceID deviceID)
{
	switch(deviceID)
	{
	case DEVICE_ID_KONA4:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_CORVID44:
	case DEVICE_ID_IO4K:
		return true;
	default:
		return false;
	}
}


bool NTV2DeviceIsSupported (NTV2DeviceID deviceID)
{
	switch (deviceID)
	{
	case DEVICE_ID_CORVID1:
	case DEVICE_ID_CORVID22:
	case DEVICE_ID_CORVID24:
	case DEVICE_ID_CORVID3G:
	case DEVICE_ID_CORVID44:
	case DEVICE_ID_CORVID88:
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_LHI:
	case DEVICE_ID_TTAP:
		return true;
	default:
		return false;
	}
}

bool NTV2DeviceHasRetailSupport (NTV2DeviceID deviceID)
{
	switch (deviceID)
	{
	case DEVICE_ID_IO4K:
	case DEVICE_ID_IO4KUFC:
	case DEVICE_ID_IOEXPRESS:
	case DEVICE_ID_IOXT:
	case DEVICE_ID_KONA3G:
	case DEVICE_ID_KONA3GQUAD:
	case DEVICE_ID_KONA4:
	case DEVICE_ID_KONA4UFC:
	case DEVICE_ID_LHE_PLUS:
	case DEVICE_ID_LHI:
	case DEVICE_ID_TTAP:
		return true;
	default:
		return false;
	}
}

bool NTV2DeviceCanReportFrameSize (NTV2DeviceID boardID)
{
	switch(boardID)
	{
		case DEVICE_ID_CORVID88:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_CORVID44:
			return true;
		default:
			return false;
	}
}


#if !defined (NTV2_DEPRECATE)
	bool NTV2BoardCanDoAudio (NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoAudioN (boardID, 0);
	}

	bool NTV2BoardCanDoAudio2 (NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoAudioN (boardID, 1);
	}

	bool NTV2BoardCanDoAudio3(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoAudioN (boardID, 2);
	}

	bool NTV2BoardCanDoAudio4(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoAudioN (boardID, 3);
	}

	bool NTV2BoardCanDoAudio5(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoAudioN (boardID, 4);
	}

	bool NTV2BoardCanDoAudio6(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoAudioN (boardID, 5);
	}

	bool NTV2BoardCanDoAudio7(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoAudioN (boardID, 6);
	}

	bool NTV2BoardCanDoAudio8(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoAudioN (boardID, 7);
	}

	bool NTV2BoardCanDoMixer2(NTV2DeviceID boardID)
	{
		return NTV2DeviceGetNumMixers (boardID) >= 2;
	}

	bool NTV2BoardCanDoMixer3(NTV2DeviceID boardID)
	{
		return NTV2DeviceGetNumMixers (boardID) >= 3;
	}

	bool NTV2BoardCanDoMixer4(NTV2DeviceID boardID)
	{
		return NTV2DeviceGetNumMixers (boardID) >= 4;
	}

	bool NTV2BoardCanDoUART (NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoUARTN (boardID, 0);
	}

	bool NTV2BoardCanDoUART2 (NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoUARTN (boardID, 1);
	}

	bool NTV2BoardCanDoLTCEmbedded(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoLTCEmbeddedN (boardID, 0);
	}

	bool NTV2BoardCanDoLTCEmbedded2(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoLTCEmbeddedN (boardID, 1);
	}

	bool NTV2BoardCanDoLTCEmbedded3(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoLTCEmbeddedN (boardID, 2);
	}

	bool NTV2BoardCanDoLTCEmbedded4(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoLTCEmbeddedN (boardID, 3);
	}

	bool NTV2BoardCanDoLTCEmbedded5(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoLTCEmbeddedN (boardID, 4);
	}

	bool NTV2BoardCanDoLTCEmbedded6(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoLTCEmbeddedN (boardID, 5);
	}

	bool NTV2BoardCanDoLTCEmbedded7(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoLTCEmbeddedN (boardID, 6);
	}

	bool NTV2BoardCanDoLTCEmbedded8(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoLTCEmbeddedN (boardID, 7);
	}

	bool NTV2BoardCanDo3G(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDo3GOut (boardID, 0);
	}

	bool NTV2BoardCanDo3GOut2(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDo3GOut (boardID, 1);
	}

	bool NTV2BoardCanDo3GOut3(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDo3GOut (boardID, 2);
	}

	bool NTV2BoardCanDo3GOut4(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDo3GOut (boardID, 3);
	}

	bool NTV2BoardCanDo3GOut5(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDo3GOut (boardID, 4);
	}

	bool NTV2BoardCanDo3GOut6(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDo3GOut (boardID, 5);
	}

	bool NTV2BoardCanDo3GOut7(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDo3GOut (boardID, 6);
	}

	bool NTV2BoardCanDo3GOut8(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDo3GOut (boardID, 7);
	}

	bool NTV2BoardCanDoLTCIn(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoLTCInN (boardID, 0);
	}

	bool NTV2BoardCanDoLTCIn2 (NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoLTCInN (boardID, 1);
	}

	bool NTV2BoardCanDoLTCOut (NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoLTCOutN (boardID, 0);
	}

	bool NTV2BoardCanDoLTCOut2(NTV2DeviceID boardID)
	{
		return NTV2DeviceCanDoLTCOutN (boardID, 1);
	}
#endif	//	NTV2_DEPRECATE
