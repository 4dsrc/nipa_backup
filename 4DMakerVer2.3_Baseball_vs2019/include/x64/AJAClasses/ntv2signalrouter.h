/**
	@brief		Declares CNTV2SignalRouter class.
	@copyright	(C) 2014 AJA Video Systems, Inc.	Proprietary and confidential.
**/

#ifndef NTV2SIGNALROUTER_H
#define NTV2SIGNALROUTER_H
#include "ajaexport.h"
#include "ntv2publicinterface.h"
#include <stddef.h>
#include <sstream>
#include <set>


/**
	@brief	This class makes it a bit easier to build an NTV2RoutingTable.
**/
typedef std::set <NTV2CrosspointID>			NTV2CrosspointIDSet;
typedef NTV2CrosspointIDSet::iterator		NTV2CrosspointIDSetIter;
typedef NTV2CrosspointIDSet::const_iterator	NTV2CrosspointIDSetConstIter;

std::ostream & operator << (std::ostream & inOutStream, const NTV2CrosspointIDSet & inObj);



/**
	@brief	This class makes it a bit easier to build an NTV2RoutingTable.
**/
class AJAExport CNTV2SignalRouter
{
	public:
		inline										CNTV2SignalRouter ()								{clear ();}
		virtual inline								~CNTV2SignalRouter ()								{}

		virtual inline operator						NTV2RoutingTable () const							{return mRoutingTable;}
		#if !defined (NTV2_DEPRECATE)
			virtual inline operator					NTV2RoutingTable* ()								{return &mRoutingTable;}
			virtual inline const NTV2RoutingEntry &	getRoutingEntry (const ULWord inIndex) const		{return GetRoutingEntry (inIndex);}
			virtual inline ULWord					getCurrentIndex (void) const						{return mRoutingTable.numEntries;}
		#endif	//	!defined (NTV2_DEPRECATE)

		virtual void								clear (void);
		virtual bool								add (const NTV2RoutingEntry & inEntry);
		virtual bool								addWithValue (const NTV2RoutingEntry & inEntry, const ULWord inValue);
		virtual bool								addWithRegisterAndValue (const NTV2RoutingEntry & inEntry, const ULWord inRegisterNum, const ULWord inValue, const ULWord inMask = 0xFFFFFFFF, const ULWord inShift = 0);
		virtual inline ULWord						GetNumberOfRoutes (void) const						{return mRoutingTable.numEntries;}
		virtual const NTV2RoutingEntry &			GetRoutingEntry (const ULWord inIndex) const;

	protected:
		NTV2RoutingTable    						mRoutingTable;		//	My routing table

};	//	CNTV2SignalRouter


//	Stream operators
std::ostream & operator << (std::ostream & inOutStream, const NTV2RoutingEntry & inObj);
std::ostream & operator << (std::ostream & inOutStream, const NTV2RoutingTable & inObj);
std::ostream & operator << (std::ostream & inOutStream, const CNTV2SignalRouter & inObj);


//	Per-widget input crosspoint selection register/mask/shift values
AJAExport const NTV2RoutingEntry &	GetFrameBuffer1InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer1BInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer2InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer2BInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer3InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer3BInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer4InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer4BInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer5InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer5BInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer6InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer6BInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer7InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer7BInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer8InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameBuffer8BInputSelectEntry (void);

AJAExport const NTV2RoutingEntry &	GetCSC1VidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC1KeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC2VidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC2KeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC3VidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC3KeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC4VidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC4KeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC5VidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC5KeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC6VidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC6KeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC7VidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC7KeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC8VidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC8KeyInputSelectEntry (void);

AJAExport const NTV2RoutingEntry &	GetXptLUT1InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetXptLUT2InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetXptLUT3InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetXptLUT4InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetXptLUT5InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetXptLUT6InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetXptLUT7InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetXptLUT8InputSelectEntry (void);

AJAExport const NTV2RoutingEntry &	GetSDIOut1StandardSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut2StandardSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut3StandardSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut4StandardSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut1InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut1InputDS2SelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut2InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut2InputDS2SelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut3InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut3InputDS2SelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut4InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut4InputDS2SelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut5InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut5InputDS2SelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut6InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut6InputDS2SelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut7InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut7InputDS2SelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut8InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetSDIOut8InputDS2SelectEntry (void);

AJAExport const NTV2RoutingEntry &	GetDualLinkIn1InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn1DSInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn2InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn2DSInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn3InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn3DSInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn4InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn4DSInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn5InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn5DSInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn6InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn6DSInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn7InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn7DSInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn8InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkIn8DSInputSelectEntry (void);

AJAExport const NTV2RoutingEntry &	GetDualLinkOut1InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkOut2InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkOut3InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkOut4InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkOut5InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkOut6InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkOut7InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetDualLinkOut8InputSelectEntry (void);

AJAExport const NTV2RoutingEntry &	GetMixer1BGKeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer1BGVidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer1FGKeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer1FGVidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer2BGKeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer2BGVidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer2FGKeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer2FGVidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer3BGKeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer3BGVidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer3FGKeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer3FGVidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer4BGKeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer4BGVidInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer4FGKeyInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetMixer4FGVidInputSelectEntry (void);

AJAExport const NTV2RoutingEntry &	GetHDMIOutInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetHDMIOutQ1InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetHDMIOutQ2InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetHDMIOutQ3InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetHDMIOutQ4InputSelectEntry (void);

AJAExport const NTV2RoutingEntry &	GetXpt4KDCQ1InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetXpt4KDCQ2InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetXpt4KDCQ3InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetXpt4KDCQ4InputSelectEntry (void);

AJAExport const NTV2RoutingEntry &	Get425Mux1AInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	Get425Mux1BInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	Get425Mux2AInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	Get425Mux2BInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	Get425Mux3AInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	Get425Mux3BInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	Get425Mux4AInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	Get425Mux4BInputSelectEntry (void);

AJAExport const NTV2RoutingEntry &	GetAnalogOutInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetIICT2InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetAnalogOutCompositeOutSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetStereoLeftInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetStereoRightInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetProAmpInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetIICT1InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetWaterMarker1InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetWaterMarker2InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetUpdateRegisterSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetConversionMod2InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCompressionModInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetConversionModInputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetCSC1KeyFromInput2SelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameSync2InputSelectEntry (void);
AJAExport const NTV2RoutingEntry &	GetFrameSync1InputSelectEntry (void);


#if !defined (NTV2_DEPRECATE)
	typedef CNTV2SignalRouter	CXena2Routing;						///< @deprecated	Use CNTV2SignalRouter instead.

	#define	GetXptLUTInputSelectEntry			GetXptLUT1InputSelectEntry
	#define GetDuallinkIn1InputSelectEntry		GetDualLinkIn1InputSelectEntry
	#define DuallinkIn1InputSelectEntry			DualLinkIn1InputSelectEntry
	#define	DuallinkOutInputSelectEntry			DualLinkOut1InputSelectEntry
	#define	GetDuallinkOutInputSelectEntry		GetDualLinkOut1InputSelectEntry
	#define	DuallinkOut2InputSelectEntry		DualLinkOut2InputSelectEntry
	#define	GetDuallinkOut2InputSelectEntry		GetDualLinkOut2InputSelectEntry
	#define	DuallinkOut3InputSelectEntry		DualLinkOut3InputSelectEntry
	#define	GetDuallinkOut3InputSelectEntry		GetDualLinkOut3InputSelectEntry
	#define	DuallinkOut4InputSelectEntry		DualLinkOut4InputSelectEntry
	#define	GetDuallinkOut4InputSelectEntry		GetDualLinkOut4InputSelectEntry
	#define	DuallinkOut5InputSelectEntry		DualLinkOut5InputSelectEntry
	#define	GetDuallinkOut5InputSelectEntry		GetDualLinkOut5InputSelectEntry
	#define	DuallinkOut6InputSelectEntry		DualLinkOut6InputSelectEntry
	#define	GetDuallinkOut6InputSelectEntry		GetDualLinkOut6InputSelectEntry
	#define	DuallinkOut7InputSelectEntry		DualLinkOut7InputSelectEntry
	#define	GetDuallinkOut7InputSelectEntry		GetDualLinkOut7InputSelectEntry
	#define	DuallinkOut8InputSelectEntry		DualLinkOut8InputSelectEntry
	#define	GetDuallinkOut8InputSelectEntry		GetDualLinkOut8InputSelectEntry
	#define	MixerBGKeyInputSelectEntry			Mixer1BGKeyInputSelectEntry
	#define	MixerBGVidInputSelectEntry			Mixer1BGVidInputSelectEntry
	#define	MixerFGKeyInputSelectEntry			Mixer1FGKeyInputSelectEntry
	#define	MixerFGVidInputSelectEntry			Mixer1FGVidInputSelectEntry
	#define	GetMixerBGKeyInputSelectEntry		GetMixer1BGKeyInputSelectEntry
	#define	GetMixerBGVidInputSelectEntry		GetMixer1BGVidInputSelectEntry
	#define	GetMixerFGKeyInputSelectEntry		GetMixer1FGKeyInputSelectEntry
	#define	GetMixerFGVidInputSelectEntry		GetMixer1FGVidInputSelectEntry

	extern const NTV2RoutingEntry FrameBuffer1InputSelectEntry;		///< @deprecated	Use GetFrameBuffer1InputSelectEntry instead.
	extern const NTV2RoutingEntry FrameBuffer2InputSelectEntry;		///< @deprecated	Use GetFrameBuffer2InputSelectEntry instead.
	extern const NTV2RoutingEntry FrameBuffer3InputSelectEntry;		///< @deprecated	Use GetFrameBuffer3InputSelectEntry instead.
	extern const NTV2RoutingEntry FrameBuffer4InputSelectEntry;		///< @deprecated	Use GetFrameBuffer4InputSelectEntry instead.
	extern const NTV2RoutingEntry FrameBuffer5InputSelectEntry;		///< @deprecated	Use GetFrameBuffer5InputSelectEntry instead.
	extern const NTV2RoutingEntry FrameBuffer6InputSelectEntry;		///< @deprecated	Use GetFrameBuffer6InputSelectEntry instead.
	extern const NTV2RoutingEntry FrameBuffer7InputSelectEntry;		///< @deprecated	Use GetFrameBuffer7InputSelectEntry instead.
	extern const NTV2RoutingEntry FrameBuffer8InputSelectEntry;		///< @deprecated	Use GetFrameBuffer8InputSelectEntry instead.

	extern const NTV2RoutingEntry CSC1VidInputSelectEntry;			///< @deprecated	Use GetCSC1VidInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC1KeyInputSelectEntry;			///< @deprecated	Use GetCSC1KeyInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC2VidInputSelectEntry;			///< @deprecated	Use GetCSC2VidInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC2KeyInputSelectEntry;			///< @deprecated	Use GetCSC2KeyInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC3VidInputSelectEntry;			///< @deprecated	Use GetCSC3VidInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC3KeyInputSelectEntry;			///< @deprecated	Use GetCSC3KeyInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC4VidInputSelectEntry;			///< @deprecated	Use GetCSC4VidInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC4KeyInputSelectEntry;			///< @deprecated	Use GetCSC4KeyInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC5VidInputSelectEntry;			///< @deprecated	Use GetCSC5VidInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC5KeyInputSelectEntry;			///< @deprecated	Use GetCSC5KeyInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC6VidInputSelectEntry;			///< @deprecated	Use GetCSC6VidInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC6KeyInputSelectEntry;			///< @deprecated	Use GetCSC6KeyInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC7VidInputSelectEntry;			///< @deprecated	Use GetCSC7VidInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC7KeyInputSelectEntry;			///< @deprecated	Use GetCSC7KeyInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC8VidInputSelectEntry;			///< @deprecated	Use GetCSC8VidInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC8KeyInputSelectEntry;			///< @deprecated	Use GetCSC8KeyInputSelectEntry instead.

	extern const NTV2RoutingEntry XptLUTInputSelectEntry;			///< @deprecated	Use GetXptLUT1InputSelectEntry instead.
	extern const NTV2RoutingEntry XptLUT2InputSelectEntry;			///< @deprecated	Use GetXptLUT2InputSelectEntry instead.
	extern const NTV2RoutingEntry XptLUT3InputSelectEntry;			///< @deprecated	Use GetXptLUT3InputSelectEntry instead.
	extern const NTV2RoutingEntry XptLUT4InputSelectEntry;			///< @deprecated	Use GetXptLUT4InputSelectEntry instead.
	extern const NTV2RoutingEntry XptLUT5InputSelectEntry;			///< @deprecated	Use GetXptLUT5InputSelectEntry instead.
	extern const NTV2RoutingEntry XptLUT6InputSelectEntry;			///< @deprecated	Use GetXptLUT6InputSelectEntry instead.
	extern const NTV2RoutingEntry XptLUT7InputSelectEntry;			///< @deprecated	Use GetXptLUT7InputSelectEntry instead.
	extern const NTV2RoutingEntry XptLUT8InputSelectEntry;			///< @deprecated	Use GetXptLUT8InputSelectEntry instead.

	extern const NTV2RoutingEntry SDIOut1StandardSelectEntry;		///< @deprecated	Use GetSDIOut1StandardSelectEntry instead.
	extern const NTV2RoutingEntry SDIOut2StandardSelectEntry;		///< @deprecated	Use GetSDIOut2StandardSelectEntry instead.
	extern const NTV2RoutingEntry SDIOut3StandardSelectEntry;		///< @deprecated	Use GetSDIOut3StandardSelectEntry instead.
	extern const NTV2RoutingEntry SDIOut4StandardSelectEntry;		///< @deprecated	Use GetSDIOut4StandardSelectEntry instead.
	extern const NTV2RoutingEntry SDIOut1InputSelectEntry;			///< @deprecated	Use GetSDIOut1InputSelectEntry instead.
	extern const NTV2RoutingEntry SDIOut1InputDS2SelectEntry;		///< @deprecated	Use GetSDIOut1InputDS2SelectEntry instead.
	extern const NTV2RoutingEntry SDIOut2InputSelectEntry;			///< @deprecated	Use GetSDIOut2InputSelectEntry instead.
	extern const NTV2RoutingEntry SDIOut2InputDS2SelectEntry;		///< @deprecated	Use GetSDIOut2InputDS2SelectEntry instead.
	extern const NTV2RoutingEntry SDIOut3InputSelectEntry;			///< @deprecated	Use GetSDIOut3InputSelectEntry instead.
	extern const NTV2RoutingEntry SDIOut3InputDS2SelectEntry;		///< @deprecated	Use GetSDIOut3InputDS2SelectEntry instead.
	extern const NTV2RoutingEntry SDIOut4InputSelectEntry;			///< @deprecated	Use GetSDIOut4InputSelectEntry instead.
	extern const NTV2RoutingEntry SDIOut4InputDS2SelectEntry;		///< @deprecated	Use GetSDIOut4InputDS2SelectEntry instead.
	extern const NTV2RoutingEntry SDIOut5InputSelectEntry;			///< @deprecated	Use GetSDIOut5InputSelectEntry instead.
	extern const NTV2RoutingEntry SDIOut5InputDS2SelectEntry;		///< @deprecated	Use GetSDIOut5InputDS2SelectEntry instead.
	extern const NTV2RoutingEntry SDIOut6InputSelectEntry;			///< @deprecated	Use GetSDIOut6InputSelectEntry instead.
	extern const NTV2RoutingEntry SDIOut6InputDS2SelectEntry;		///< @deprecated	Use GetSDIOut6InputDS2SelectEntry instead.
	extern const NTV2RoutingEntry SDIOut7InputSelectEntry;			///< @deprecated	Use GetSDIOut7InputSelectEntry instead.
	extern const NTV2RoutingEntry SDIOut7InputDS2SelectEntry;		///< @deprecated	Use GetSDIOut7InputDS2SelectEntry instead.
	extern const NTV2RoutingEntry SDIOut8InputSelectEntry;			///< @deprecated	Use GetSDIOut8InputSelectEntry instead.
	extern const NTV2RoutingEntry SDIOut8InputDS2SelectEntry;		///< @deprecated	Use GetSDIOut8InputDS2SelectEntry instead.

	extern const NTV2RoutingEntry DualLinkIn1InputSelectEntry;		///< @deprecated	Use GetDualLinkIn1InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn1DSInputSelectEntry;	///< @deprecated	Use GetDualLinkIn1DSInputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn2InputSelectEntry;		///< @deprecated	Use GetDualLinkIn2InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn2DSInputSelectEntry;	///< @deprecated	Use GetDualLinkIn2DSInputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn3InputSelectEntry;		///< @deprecated	Use GetDualLinkIn3InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn3DSInputSelectEntry;	///< @deprecated	Use GetDualLinkIn3DSInputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn4InputSelectEntry;		///< @deprecated	Use GetDualLinkIn4InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn4DSInputSelectEntry;	///< @deprecated	Use GetDualLinkIn4DSInputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn5InputSelectEntry;		///< @deprecated	Use GetDualLinkIn5InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn5DSInputSelectEntry;	///< @deprecated	Use GetDualLinkIn5DSInputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn6InputSelectEntry;		///< @deprecated	Use GetDualLinkIn6InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn6DSInputSelectEntry;	///< @deprecated	Use GetDualLinkIn6DSInputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn7InputSelectEntry;		///< @deprecated	Use GetDualLinkIn7InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn7DSInputSelectEntry;	///< @deprecated	Use GetDualLinkIn7DSInputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn8InputSelectEntry;		///< @deprecated	Use GetDualLinkIn8InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkIn8DSInputSelectEntry;	///< @deprecated	Use GetDualLinkIn8DSInputSelectEntry instead.

	extern const NTV2RoutingEntry DualLinkOut1InputSelectEntry;		///< @deprecated	Use GetDualLinkOut1InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkOut2InputSelectEntry;		///< @deprecated	Use GetDualLinkOut2InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkOut3InputSelectEntry;		///< @deprecated	Use GetDualLinkOut3InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkOut4InputSelectEntry;		///< @deprecated	Use GetDualLinkOut4InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkOut5InputSelectEntry;		///< @deprecated	Use GetDualLinkOut5InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkOut6InputSelectEntry;		///< @deprecated	Use GetDualLinkOut6InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkOut7InputSelectEntry;		///< @deprecated	Use GetDualLinkOut7InputSelectEntry instead.
	extern const NTV2RoutingEntry DualLinkOut8InputSelectEntry;		///< @deprecated	Use GetDualLinkOut8InputSelectEntry instead.

	extern const NTV2RoutingEntry Mixer1BGKeyInputSelectEntry;		///< @deprecated	Use GetMixer1BGKeyInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer1BGVidInputSelectEntry;		///< @deprecated	Use GetMixer1BGVidInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer1FGKeyInputSelectEntry;		///< @deprecated	Use GetMixer1FGKeyInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer1FGVidInputSelectEntry;		///< @deprecated	Use GetMixer1FGVidInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer2BGKeyInputSelectEntry;		///< @deprecated	Use GetMixer2BGKeyInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer2BGVidInputSelectEntry;		///< @deprecated	Use GetMixer2BGVidInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer2FGKeyInputSelectEntry;		///< @deprecated	Use GetMixer2FGKeyInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer2FGVidInputSelectEntry;		///< @deprecated	Use GetMixer2FGVidInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer3BGKeyInputSelectEntry;		///< @deprecated	Use GetMixer3BGKeyInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer3BGVidInputSelectEntry;		///< @deprecated	Use GetMixer3BGVidInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer3FGKeyInputSelectEntry;		///< @deprecated	Use GetMixer3FGKeyInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer3FGVidInputSelectEntry;		///< @deprecated	Use GetMixer3FGVidInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer4BGKeyInputSelectEntry;		///< @deprecated	Use GetMixer4BGKeyInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer4BGVidInputSelectEntry;		///< @deprecated	Use GetMixer4BGVidInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer4FGKeyInputSelectEntry;		///< @deprecated	Use GetMixer4FGKeyInputSelectEntry instead.
	extern const NTV2RoutingEntry Mixer4FGVidInputSelectEntry;		///< @deprecated	Use GetMixer4FGVidInputSelectEntry instead.

	extern const NTV2RoutingEntry Xpt4KDCQ1InputSelectEntry;		///< @deprecated	Use GetXpt4KDCQ1InputSelectEntry instead.
	extern const NTV2RoutingEntry Xpt4KDCQ2InputSelectEntry;		///< @deprecated	Use GetXpt4KDCQ2InputSelectEntry instead.
	extern const NTV2RoutingEntry Xpt4KDCQ3InputSelectEntry;		///< @deprecated	Use GetXpt4KDCQ3InputSelectEntry instead.
	extern const NTV2RoutingEntry Xpt4KDCQ4InputSelectEntry;		///< @deprecated	Use GetXpt4KDCQ4InputSelectEntry instead.

	extern const NTV2RoutingEntry HDMIOutQ1InputSelectEntry;		///< @deprecated	Use GetHDMIOutQ1InputSelectEntry instead.
	extern const NTV2RoutingEntry HDMIOutQ2InputSelectEntry;		///< @deprecated	Use GetHDMIOutQ2InputSelectEntry instead.
	extern const NTV2RoutingEntry HDMIOutQ3InputSelectEntry;		///< @deprecated	Use GetHDMIOutQ3InputSelectEntry instead.
	extern const NTV2RoutingEntry HDMIOutQ4InputSelectEntry;		///< @deprecated	Use GetHDMIOutQ4InputSelectEntry instead.

	extern const NTV2RoutingEntry CompressionModInputSelectEntry;	///< @deprecated	Use GetCompressionModInputSelectEntry instead.
	extern const NTV2RoutingEntry ConversionModInputSelectEntry;	///< @deprecated	Use GetConversionModInputSelectEntry instead.
	extern const NTV2RoutingEntry CSC1KeyFromInput2SelectEntry;		///< @deprecated	Use GetCSC1KeyFromInput2SelectEntry instead.
	extern const NTV2RoutingEntry FrameSync2InputSelectEntry;		///< @deprecated	Use GetFrameSync2InputSelectEntry instead.
	extern const NTV2RoutingEntry FrameSync1InputSelectEntry;		///< @deprecated	Use GetFrameSync1InputSelectEntry instead.
	extern const NTV2RoutingEntry AnalogOutInputSelectEntry;		///< @deprecated	Use GetAnalogOutInputSelectEntry instead.
	extern const NTV2RoutingEntry ProAmpInputSelectEntry;			///< @deprecated	Use GetProAmpInputSelectEntry instead.
	extern const NTV2RoutingEntry IICT1InputSelectEntry;			///< @deprecated	Use GetIICT1InputSelectEntry instead.
	extern const NTV2RoutingEntry WaterMarker1InputSelectEntry;		///< @deprecated	Use GetWaterMarker1InputSelectEntry instead.
	extern const NTV2RoutingEntry WaterMarker2InputSelectEntry;		///< @deprecated	Use GetWaterMarker2InputSelectEntry instead.
	extern const NTV2RoutingEntry UpdateRegisterSelectEntry;		///< @deprecated	Use GetUpdateRegisterSelectEntry instead.
	extern const NTV2RoutingEntry ConversionMod2InputSelectEntry;	///< @deprecated	Use GetConversionMod2InputSelectEntry instead.
	extern const NTV2RoutingEntry HDMIOutInputSelectEntry;			///< @deprecated	Use GetHDMIOutInputSelectEntry instead.
	extern const NTV2RoutingEntry IICT2InputSelectEntry;			///< @deprecated	Use GetIICT2InputSelectEntry instead.
	extern const NTV2RoutingEntry AnalogOutCompositeOutSelectEntry;	///< @deprecated	Use GetAnalogOutCompositeOutSelectEntry instead.
	extern const NTV2RoutingEntry StereoLeftInputSelectEntry;		///< @deprecated	Use GetStereoLeftInputSelectEntry instead.
	extern const NTV2RoutingEntry StereoRightInputSelectEntry;		///< @deprecated	Use GetStereoRightInputSelectEntry instead.
#endif	//	!defined (NTV2_DEPRECATE)

#endif	//	NTV2SIGNALROUTER_H
