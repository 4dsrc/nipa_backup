 /*
 *
	ntv2windriverinterface.cpp

	Copyright (C) 2003, 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.

	Purpose:

	Platform dependent implementations for NTV2 Driver Interface
	on Windows.

 */

#include "ntv2windriverinterface.h"
#include "ntv2winpublicinterface.h"
#include "ntv2nubtypes.h"
#include "ntv2debug.h"
using namespace std;

#ifdef _AJA_COMPILE_WIN2K_SOFT_LINK
typedef WINSETUPAPI
HDEVINFO
(WINAPI *
pfcnSetupDiGetClassDevsA)(
    IN CONST GUID *ClassGuid,  OPTIONAL
    IN PCSTR       Enumerator, OPTIONAL
    IN HWND        hwndParent, OPTIONAL
    IN DWORD       Flags
    );

typedef WINSETUPAPI
HDEVINFO
(WINAPI *
pfcnSetupDiGetClassDevsW)(
    IN CONST GUID *ClassGuid,  OPTIONAL
    IN PCWSTR      Enumerator, OPTIONAL
    IN HWND        hwndParent, OPTIONAL
    IN DWORD       Flags
    );
#ifdef UNICODE
#define pfcnSetupDiGetClassDevs pfcnSetupDiGetClassDevsW
#else
#define pfcnSetupDiGetClassDevs pfcnSetupDiGetClassDevsA
#endif

typedef WINSETUPAPI
BOOL
(WINAPI *
pfcnSetupDiEnumDeviceInterfaces)(
    IN  HDEVINFO                   DeviceInfoSet,
    IN  PSP_DEVINFO_DATA           DeviceInfoData,     OPTIONAL
    IN  CONST GUID                *InterfaceClassGuid,
    IN  DWORD                      MemberIndex,
    OUT PSP_DEVICE_INTERFACE_DATA  DeviceInterfaceData
    );


typedef WINSETUPAPI
BOOL
(WINAPI *
pfcnSetupDiGetDeviceInterfaceDetailA)(
    IN  HDEVINFO                           DeviceInfoSet,
    IN  PSP_DEVICE_INTERFACE_DATA          DeviceInterfaceData,
    OUT PSP_DEVICE_INTERFACE_DETAIL_DATA_A DeviceInterfaceDetailData,     OPTIONAL
    IN  DWORD                              DeviceInterfaceDetailDataSize,
    OUT PDWORD                             RequiredSize,                  OPTIONAL
    OUT PSP_DEVINFO_DATA                   DeviceInfoData                 OPTIONAL
    );

typedef WINSETUPAPI
BOOL
(WINAPI *
pfcnSetupDiGetDeviceInterfaceDetailW)(
    IN  HDEVINFO                           DeviceInfoSet,
    IN  PSP_DEVICE_INTERFACE_DATA          DeviceInterfaceData,
    OUT PSP_DEVICE_INTERFACE_DETAIL_DATA_W DeviceInterfaceDetailData,     OPTIONAL
    IN  DWORD                              DeviceInterfaceDetailDataSize,
    OUT PDWORD                             RequiredSize,                  OPTIONAL
    OUT PSP_DEVINFO_DATA                   DeviceInfoData                 OPTIONAL
	);
#ifdef UNICODE
#define pfcnSetupDiGetDeviceInterfaceDetail pfcnSetupDiGetDeviceInterfaceDetailW
#else
#define pfcnSetupDiGetDeviceInterfaceDetail pfcnSetupDiGetDeviceInterfaceDetailA
#endif

typedef WINSETUPAPI
BOOL
(WINAPI *
 pfcnSetupDiDestroyDeviceInfoList)(
 IN HDEVINFO DeviceInfoSet
 );

typedef WINSETUPAPI
BOOL
(WINAPI *
 pfcnSetupDiDestroyDriverInfoList)(
 IN HDEVINFO         DeviceInfoSet,
 IN PSP_DEVINFO_DATA DeviceInfoData, OPTIONAL
 IN DWORD            DriverType
 );

static pfcnSetupDiGetClassDevs pSetupDiGetClassDevs = NULL;
static pfcnSetupDiEnumDeviceInterfaces pSetupDiEnumDeviceInterfaces = NULL;
static pfcnSetupDiGetDeviceInterfaceDetail pSetupDiGetDeviceInterfaceDetail = NULL;
static pfcnSetupDiDestroyDeviceInfoList pSetupDiDestroyDeviceInfoList = NULL;
static pfcnSetupDiDestroyDriverInfoList pSetupDiDestroyDriverInfoList = NULL;

HINSTANCE hSetupAPI = NULL;

#endif

static const ULWord	gChannelToTSLastOutputVertLo []		= {	kRegTimeStampLastOutputVerticalLo, kRegTimeStampLastOutput2VerticalLo, kRegTimeStampLastOutput3VerticalLo, kRegTimeStampLastOutput4VerticalLo,
															kRegTimeStampLastOutput5VerticalLo, kRegTimeStampLastOutput6VerticalLo, kRegTimeStampLastOutput7VerticalLo, kRegTimeStampLastOutput8VerticalLo, 0};

static const ULWord	gChannelToTSLastOutputVertHi []		= {	kRegTimeStampLastOutputVerticalHi, kRegTimeStampLastOutput2VerticalHi, kRegTimeStampLastOutput3VerticalHi, kRegTimeStampLastOutput4VerticalHi,
															kRegTimeStampLastOutput5VerticalHi, kRegTimeStampLastOutput6VerticalHi, kRegTimeStampLastOutput7VerticalHi, kRegTimeStampLastOutput8VerticalHi, 0};

static const ULWord	gChannelToTSLastInputVertLo []		= {	kRegTimeStampLastInput1VerticalLo, kRegTimeStampLastInput2VerticalLo, kRegTimeStampLastInput3VerticalLo, kRegTimeStampLastInput4VerticalLo,
															kRegTimeStampLastInput5VerticalLo, kRegTimeStampLastInput6VerticalLo, kRegTimeStampLastInput7VerticalLo, kRegTimeStampLastInput8VerticalLo, 0};

static const ULWord	gChannelToTSLastInputVertHi []		= {	kRegTimeStampLastInput1VerticalHi, kRegTimeStampLastInput2VerticalHi, kRegTimeStampLastInput3VerticalHi, kRegTimeStampLastInput4VerticalHi,
															kRegTimeStampLastInput5VerticalHi, kRegTimeStampLastInput6VerticalHi, kRegTimeStampLastInput7VerticalHi, kRegTimeStampLastInput8VerticalHi, 0};

/////////////////////////////////////////////////////////////////////////////////////
// Board Open / Close methods
/////////////////////////////////////////////////////////////////////////////////////

// Method: Open
// Input:  UWord boardNumber(starts at 0)
//         bool displayErrorMessage
//         ULWord shareMode  Specifies how the object can be shared.
//            If dwShareMode is 0, the object cannot be shared.
//            Subsequent open operations on the object will fail,
//            until the handle is closed.
//            defaults to FILE_SHARE_READ | FILE_SHARE_WRITE
//            which allows multiple opens on the same device.
// Output: bool - true if opened ok.
bool CNTV2WinDriverInterface::Open (UWord boardNumber, bool displayError, NTV2BoardType eBoardType, const char *hostname)
{
	// Check if already opened
	if (IsOpen())
	{
		if ( _boardNumber == boardNumber )
			return true;

		Close();   // Close current board and open desired board
	}

	_remoteHandle = (LWord)INVALID_NUB_HANDLE;
	_hDevice = INVALID_HANDLE_VALUE;
	//_boardType = eBoardType;
	//eBoardType = _boardType;
	ULWord deviceID = 0x0;

	_displayErrorMessage = displayError;
#define BOARDSTRMAX	32
	char boardStr[BOARDSTRMAX];
	if (_boardType != DEVICETYPE_NTV2)
	{
		if (_displayErrorMessage)	DisplayNTV2Error ("Open not called with BOARDTYPE_NTV2");
		return false;
	}

	if (hostname && hostname[0] != '\0')	// Non-null: card on remote host
	{
		_snprintf_s(boardStr, BOARDSTRMAX - 1, "%s:ntv2%d", hostname, _boardNumber);

		if ( !OpenRemote(boardNumber, displayError, _boardType, hostname))
		{
			DisplayNTV2Error("Failed to open board on remote host.");
		}
	}
	else
	{

		switch (_boardType)
		{
//		case BOARDTYPE_AJAXENA2:
//			{
//				DEFINE_GUIDSTRUCT("2BFA1669-17F7-4cf9-8E05-500B8CB81497", AJAVIDEO_XENA2_PROPSET);
//#define AJAVIDEO_XENA2_PROPSET DEFINE_GUIDNAMED(AJAVIDEO_XENA2_PROPSET)
//				_GUID_PROPSET = AJAVIDEO_XENA2_PROPSET;
//			}
//			break;
		case DEVICETYPE_NTV2:
			{
				DEFINE_GUIDSTRUCT("844B39E5-C98E-45a1-84DE-3BAF3F4F9F14", AJAVIDEO_NTV2_PROPSET);
#define AJAVIDEO_NTV2_PROPSET DEFINE_GUIDNAMED(AJAVIDEO_NTV2_PROPSET)
				_GUID_PROPSET = AJAVIDEO_NTV2_PROPSET;
			}
			break;
		case DEVICETYPE_UNKNOWN:
			return false;

		default: // if it's something we don't know about yet, don't open it
			return false;
		}

		REFGUID refguid = _GUID_PROPSET;
		_boardNumber = boardNumber;

		DWORD dwShareMode;
		if (_bOpenShared)
			dwShareMode = FILE_SHARE_READ | FILE_SHARE_WRITE;
		else
			dwShareMode = 0x0;

		DWORD dwFlagsAndAttributes;
		if (_bOpenOverlapped)
			dwFlagsAndAttributes = FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED;
		else
			dwFlagsAndAttributes = 0x0;

		switch (_boardType)
		{
#if 0	//	** MrBill **
		case BOARDTYPE_AJAXENA2:
			{
				DWORD dwReqSize=0;
				SP_DEVICE_INTERFACE_DATA spDevIFaceData;
#ifndef _AJA_COMPILE_WIN2K_SOFT_LINK
				GUID myguid = refguid;  // an un-const guid for compiling with new Platform SDK!
				_hDevInfoSet = SetupDiGetClassDevs(&myguid,NULL,NULL,DIGCF_PRESENT|DIGCF_DEVICEINTERFACE);
				if(_hDevInfoSet==INVALID_HANDLE_VALUE)
				{
					return false;
				}

				spDevIFaceData.cbSize=sizeof(SP_DEVICE_INTERFACE_DATA);
				myguid = refguid;
				if(!SetupDiEnumDeviceInterfaces(_hDevInfoSet,NULL,&myguid,_boardNumber,&spDevIFaceData))
				{
					SetupDiDestroyDeviceInfoList(_hDevInfoSet);
					return false;
				}

				if(SetupDiGetDeviceInterfaceDetail(_hDevInfoSet,&spDevIFaceData,NULL,0,&dwReqSize,NULL))
				{
					SetupDiDestroyDeviceInfoList(_hDevInfoSet);
					return false; //should have failed!
				}
				if(GetLastError()!=ERROR_INSUFFICIENT_BUFFER)
				{
					SetupDiDestroyDeviceInfoList(_hDevInfoSet);
					return false;
				}
				_pspDevIFaceDetailData=(PSP_DEVICE_INTERFACE_DETAIL_DATA) new BYTE[dwReqSize];
				if(!(_pspDevIFaceDetailData))
				{
					SetupDiDestroyDeviceInfoList(_hDevInfoSet);
					return false; // out of memory
				}

				_spDevInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
				_pspDevIFaceDetailData->cbSize=sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
				//now we are setup to get the info we want!
				if(!SetupDiGetDeviceInterfaceDetail(_hDevInfoSet ,&spDevIFaceData,_pspDevIFaceDetailData, dwReqSize,NULL,&_spDevInfoData))
				{
					delete _pspDevIFaceDetailData;
					_pspDevIFaceDetailData=NULL;
					SetupDiDestroyDeviceInfoList(_hDevInfoSet);
					return false; // out of memory
				}

				ULONG deviceInstanceSize = 0;
				CM_Get_Device_ID_Size(&deviceInstanceSize, _spDevInfoData.DevInst, 0);
				char* deviceInstance = (char*)new BYTE[deviceInstanceSize*2];
				CM_Get_Device_IDA(_spDevInfoData.DevInst, deviceInstance, deviceInstanceSize*2, 0);
				string sDeviceInstance(deviceInstance);
				delete deviceInstance;
				if(sDeviceInstance.find("DB") != string::npos)
				{
					string sDeviceID = sDeviceInstance.substr(sDeviceInstance.find("DB"),4);
					if(sDeviceID.compare("DB01") == 0)
						deviceID = 0xDB01;
					else if(sDeviceID.compare("DB02") == 0)
						deviceID = 0xDB02;
					else if(sDeviceID.compare("DB03") == 0)
						deviceID = 0xDB03;
					else if(sDeviceID.compare("DB04") == 0)
						deviceID = 0xDB04;
					else if(sDeviceID.compare("DB05") == 0)
						deviceID = 0xDB05;
					else if(sDeviceID.compare("DB06") == 0)
						deviceID = 0xDB06;
					else if(sDeviceID.compare("DB07") == 0)
						deviceID = 0xDB07;
					else if(sDeviceID.compare("DB08") == 0)
						deviceID = 0xDB08;
					else if(sDeviceID.compare("DB09") == 0)
						deviceID = 0xDB09;
					else if(sDeviceID.compare("DB10") == 0)
						deviceID = 0xDB10;
					else if(sDeviceID.compare("DB11") == 0)
						deviceID = 0xDB11;
					else
						deviceID = 0x0;
				}
				else
					deviceID = 0x0;

#else
				if(!hSetupAPI || !pSetupDiGetClassDevs) {
					// Get Interface
					hSetupAPI = LoadLibrary("SetupAPI.dll");
					if(hSetupAPI != NULL) {
						pSetupDiEnumDeviceInterfaces = (pfcnSetupDiEnumDeviceInterfaces)GetProcAddress(hSetupAPI, "SetupDiEnumDeviceInterfaces");
						pSetupDiDestroyDeviceInfoList = (pfcnSetupDiDestroyDeviceInfoList)GetProcAddress(hSetupAPI, "SetupDiDestroyDeviceInfoList");
#ifdef UNICODE
						pSetupDiGetClassDevs = (pfcnSetupDiGetClassDevs)GetProcAddress(hSetupAPI, "SetupDiGetClassDevsW");
						pSetupDiGetDeviceInterfaceDetail = (pfcnSetupDiGetDeviceInterfaceDetail)GetProcAddress(hSetupAPI, "SetupDiGetDeviceInterfaceDetailW");
#else
						pSetupDiGetClassDevs = (pfcnSetupDiGetClassDevs)GetProcAddress(hSetupAPI, "SetupDiGetClassDevsA");
						pSetupDiGetDeviceInterfaceDetail = (pfcnSetupDiGetDeviceInterfaceDetail)GetProcAddress(hSetupAPI, "SetupDiGetDeviceInterfaceDetailA");
#endif
					}
					if(hSetupAPI == NULL ||
						pSetupDiGetClassDevs == NULL ||
						pSetupDiEnumDeviceInterfaces == NULL ||
						pSetupDiGetDeviceInterfaceDetail == NULL ||
						pSetupDiDestroyDeviceInfoList == NULL) {
							// FAILED
							pSetupDiGetClassDevs = NULL;
							pSetupDiEnumDeviceInterfaces = NULL;
							pSetupDiGetDeviceInterfaceDetail = NULL;
							pSetupDiDestroyDeviceInfoList = NULL;
							if(hSetupAPI) {
								FreeLibrary(hSetupAPI);
							}
							hSetupAPI = NULL;
							return false;
						}
				}
				_hDevInfoSet = pSetupDiGetClassDevs(&refguid,NULL,NULL,DIGCF_PRESENT|DIGCF_DEVICEINTERFACE);
				if(_hDevInfoSet==INVALID_HANDLE_VALUE)
				{
					return false;
				}

				spDevIFaceData.cbSize=sizeof(SP_DEVICE_INTERFACE_DATA);
				if(!pSetupDiEnumDeviceInterfaces(_hDevInfoSet,NULL,&refguid,_boardNumber,&spDevIFaceData))
					return false; //no more or a problem!
				if(pSetupDiGetDeviceInterfaceDetail(_hDevInfoSet,&spDevIFaceData,NULL,0,&dwReqSize,NULL))
					return false; //should have failed!
				if(GetLastError()!=ERROR_INSUFFICIENT_BUFFER)
					return false; //should have reported buffer too small!
				_pspDevIFaceDetailData=(PSP_DEVICE_INTERFACE_DETAIL_DATA) new BYTE[dwReqSize];
				if(!(_pspDevIFaceDetailData))
					return false; //out of memory!
				//now we are setup to get the info we want!
				_pspDevIFaceDetailData->cbSize=sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
				if(!pSetupDiGetDeviceInterfaceDetail(_hDevInfoSet ,&spDevIFaceData,_pspDevIFaceDetailData, dwReqSize,NULL,NULL))
					return false;
#endif
				_hDevice = CreateFile(_pspDevIFaceDetailData->DevicePath,
					GENERIC_READ | GENERIC_WRITE,
					dwShareMode,
					NULL,
					OPEN_EXISTING,
					dwFlagsAndAttributes,
					NULL);

				if(_hDevice == INVALID_HANDLE_VALUE)
				{
					delete _pspDevIFaceDetailData;
					_pspDevIFaceDetailData=NULL;
#ifndef _AJA_COMPILE_WIN2K_SOFT_LINK
					SetupDiDestroyDeviceInfoList(_hDevInfoSet);
#else
					if(pSetupDiDestroyDeviceInfoList != NULL) {
						pSetupDiDestroyDeviceInfoList(_hDevInfoSet);
					}
#endif
					_hDevInfoSet=NULL;
					return false;
				}
			}
			break;
#endif	//	** MrBill **
		case DEVICETYPE_NTV2:
			{
				DWORD dwReqSize=0;
				SP_DEVICE_INTERFACE_DATA spDevIFaceData;
				GUID myguid = refguid;  // an un-const guid for compiling with new Platform SDK!
				_hDevInfoSet = SetupDiGetClassDevs(&myguid,NULL,NULL,DIGCF_PRESENT|DIGCF_DEVICEINTERFACE);
				if(_hDevInfoSet==INVALID_HANDLE_VALUE)
				{
					return false;
				}

				spDevIFaceData.cbSize=sizeof(SP_DEVICE_INTERFACE_DATA);
				myguid = refguid;
				if(!SetupDiEnumDeviceInterfaces(_hDevInfoSet,NULL,&myguid,_boardNumber,&spDevIFaceData))
				{
					SetupDiDestroyDeviceInfoList(_hDevInfoSet);
					return false;
				}

				if(SetupDiGetDeviceInterfaceDetail(_hDevInfoSet,&spDevIFaceData,NULL,0,&dwReqSize,NULL))
				{
					SetupDiDestroyDeviceInfoList(_hDevInfoSet);
					return false; //should have failed!
				}
				if(GetLastError()!=ERROR_INSUFFICIENT_BUFFER)
				{
					SetupDiDestroyDeviceInfoList(_hDevInfoSet);
					return false;
				}
				_pspDevIFaceDetailData=(PSP_DEVICE_INTERFACE_DETAIL_DATA) new BYTE[dwReqSize];
				if(!(_pspDevIFaceDetailData))
				{
					SetupDiDestroyDeviceInfoList(_hDevInfoSet);
					return false; // out of memory
				}

				_spDevInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
				_pspDevIFaceDetailData->cbSize=sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
				//now we are setup to get the info we want!
				if(!SetupDiGetDeviceInterfaceDetail(_hDevInfoSet ,&spDevIFaceData,_pspDevIFaceDetailData, dwReqSize,NULL,&_spDevInfoData))
				{
					delete _pspDevIFaceDetailData;
					_pspDevIFaceDetailData=NULL;
					SetupDiDestroyDeviceInfoList(_hDevInfoSet);
					return false; // out of memory
				}

				ULONG deviceInstanceSize = 0;
				CM_Get_Device_ID_Size(&deviceInstanceSize, _spDevInfoData.DevInst, 0);
				char* deviceInstance = (char*)new BYTE[deviceInstanceSize*2];
				CM_Get_Device_IDA(_spDevInfoData.DevInst, deviceInstance, deviceInstanceSize*2, 0);
				string sDeviceInstance(deviceInstance);
				delete deviceInstance;
				if(sDeviceInstance.find("DB") != string::npos)
				{
					string sDeviceID = sDeviceInstance.substr(sDeviceInstance.find("DB"),4);
					if(sDeviceID.compare("DB01") == 0)
						deviceID = 0xDB01;
					else if(sDeviceID.compare("DB02") == 0)
						deviceID = 0xDB02;
					else if(sDeviceID.compare("DB03") == 0)
						deviceID = 0xDB03;
					else if(sDeviceID.compare("DB04") == 0)
						deviceID = 0xDB04;
					else if(sDeviceID.compare("DB05") == 0)
						deviceID = 0xDB05;
					else if(sDeviceID.compare("DB06") == 0)
						deviceID = 0xDB06;
					else if(sDeviceID.compare("DB07") == 0)
						deviceID = 0xDB07;
					else if(sDeviceID.compare("DB08") == 0)
						deviceID = 0xDB08;
					else if(sDeviceID.compare("DB09") == 0)
						deviceID = 0xDB09;
					else if(sDeviceID.compare("DB10") == 0)
						deviceID = 0xDB10;
					else if(sDeviceID.compare("DB11") == 0)
						deviceID = 0xDB11;
					else
						deviceID = 0x0;
				}
				else
					deviceID = 0x0;

				_hDevice = CreateFile(_pspDevIFaceDetailData->DevicePath,
					GENERIC_READ | GENERIC_WRITE,
					dwShareMode,
					NULL,
					OPEN_EXISTING,
					dwFlagsAndAttributes,
					NULL);

				if(_hDevice == INVALID_HANDLE_VALUE)
				{
					delete _pspDevIFaceDetailData;
					_pspDevIFaceDetailData=NULL;
					SetupDiDestroyDeviceInfoList(_hDevInfoSet);
					_hDevInfoSet=NULL;
					return false;
				}
			}
			break;
		}
	}
	_boardOpened = true;
	ReadRegister(kRegBoardID, reinterpret_cast<ULWord*>(&_boardID));
	//Kludge Warning.....
	//These frame geometries required greater than 8 megs/ frame
	//so these frame geometries go to 16 megs/frame
	NTV2FrameGeometry fg;
	ReadRegister (kRegGlobalControl,
				  (ULWord*)&fg,
				  kRegMaskGeometry,
				  kRegShiftGeometry);

	// More of the same Kludge... (from ntv2Register.cpp)
	ULWord returnVal1,returnVal2;
	ReadRegister (kRegCh1Control,&returnVal1,kRegMaskFrameFormat,kRegShiftFrameFormat);
	ReadRegister (kRegCh1Control,&returnVal2,kRegMaskFrameFormatHiBit,kRegShiftFrameFormatHiBit);
	NTV2FrameBufferFormat format = (NTV2FrameBufferFormat)((returnVal1&0x0f) | ((returnVal2&0x1)<<4));

	// Write the device ID
	WriteRegister(kRegPCIDeviceID, deviceID);

	InitMemberVariablesOnOpen(fg,format);    // in the base class

	return true;
}

// Method:	SetShareMode
// Input:	bool mode
bool CNTV2WinDriverInterface::SetShareMode (bool bShared)
{
	_bOpenShared = bShared;
	return true;
}

// Method:	SetOverlappedMode
// Input:	bool mode
bool CNTV2WinDriverInterface::SetOverlappedMode (bool bOverlapped)
{
	_bOpenOverlapped = bOverlapped;
	return true;
}

// Method: Close
// Input:  NONE
// Output: NONE
bool CNTV2WinDriverInterface::Close()
{
	if (_remoteHandle != INVALID_NUB_HANDLE)
	{
		return CloseRemote();
	}

    // If board already closed, return true
    if (!_boardOpened)
        return true;

	if ( _remoteHandle != (LWord)INVALID_NUB_HANDLE )
	{

	}
	else
	{
		assert( _hDevice );
		if(_pspDevIFaceDetailData)
		{
			delete _pspDevIFaceDetailData;
			_pspDevIFaceDetailData=NULL;
		}
		if(_hDevInfoSet)
		{
#ifndef _AJA_COMPILE_WIN2K_SOFT_LINK
			SetupDiDestroyDeviceInfoList(_hDevInfoSet);
#else
			if(pSetupDiDestroyDeviceInfoList != NULL) {
				pSetupDiDestroyDeviceInfoList(_hDevInfoSet);
			}
#endif
			_hDevInfoSet=NULL;
		}

		// oem additions
		UnmapFrameBuffers ();
		ConfigureSubscription (false, eOutput1, mInterruptEventHandles [eOutput1]);
		ConfigureSubscription (false, eOutput2, mInterruptEventHandles [eOutput2]);
		ConfigureSubscription (false, eOutput3, mInterruptEventHandles [eOutput3]);
		ConfigureSubscription (false, eOutput4, mInterruptEventHandles [eOutput4]);
		ConfigureSubscription (false, eOutput5, mInterruptEventHandles [eOutput5]);
		ConfigureSubscription (false, eOutput6, mInterruptEventHandles [eOutput6]);
		ConfigureSubscription (false, eOutput7, mInterruptEventHandles [eOutput7]);
		ConfigureSubscription (false, eOutput8, mInterruptEventHandles [eOutput8]);
		ConfigureSubscription (false, eInput1, mInterruptEventHandles [eInput1]);
		ConfigureSubscription (false, eInput2, mInterruptEventHandles [eInput2]);
		ConfigureSubscription (false, eInput3, mInterruptEventHandles [eInput3]);
		ConfigureSubscription (false, eInput4, mInterruptEventHandles [eInput4]);
		ConfigureSubscription (false, eInput5, mInterruptEventHandles [eInput5]);
		ConfigureSubscription (false, eInput6, mInterruptEventHandles [eInput6]);
		ConfigureSubscription (false, eInput7, mInterruptEventHandles [eInput7]);
		ConfigureSubscription (false, eInput8, mInterruptEventHandles [eInput8]);
		ConfigureSubscription (false, eChangeEvent, mInterruptEventHandles [eChangeEvent]);
		ConfigureSubscription (false, eAudio, mInterruptEventHandles [eAudio]);
		ConfigureSubscription (false, eAudioInWrap, mInterruptEventHandles [eAudioInWrap]);
		ConfigureSubscription (false, eAudioOutWrap, mInterruptEventHandles [eAudioOutWrap]);
		ConfigureSubscription (false, eUartTx, mInterruptEventHandles [eUartTx]);
		ConfigureSubscription (false, eUartRx, mInterruptEventHandles [eUartRx]);
		ConfigureSubscription (false, eHDMIRxV2HotplugDetect, mInterruptEventHandles [eHDMIRxV2HotplugDetect]);
		ConfigureSubscription (false, eDMA1, mInterruptEventHandles [eDMA1]);
		ConfigureSubscription (false, eDMA2, mInterruptEventHandles [eDMA2]);
		ConfigureSubscription (false, eDMA3, mInterruptEventHandles [eDMA3]);
		ConfigureSubscription (false, eDMA4, mInterruptEventHandles [eDMA4]);
		DmaUnlock ();
		UnmapRegisters();

		if ( _hDevice != INVALID_HANDLE_VALUE )
			CloseHandle(_hDevice);

	}

	_hDevice = INVALID_HANDLE_VALUE;
	_remoteHandle = (LWord)INVALID_NUB_HANDLE;
	_boardOpened = false;

	return true;
}

// DmaUnlock
// Called from avclasses destructor to insure the process isn't terminating
//	with memory still locked - a guaranteed cause of a blue screen
bool CNTV2WinDriverInterface::DmaUnlock (void)
{
	// For every locked entry, try to free it
	for (unsigned int i = 0; i < _vecDmaLocked.size (); i++)
		CompleteMemoryForDMA (_vecDmaLocked[i]);

	// Free up any memory held in the stl vector
	_vecDmaLocked.clear ();

	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////
// PrepareMemoryForDMA
// Passes the address of a user space allocated frame buffer and the buffer's size to the
//	kernel, where the kernel
//	creates a MDL and probes & locks the memory.  The framebuffer's userspace address is saved
//	in the kernel, and all subsequent DMA calls to this address will avoid the time penalties
//	of the MDL creation and the probe & lock.
// NOTE: When this method is used to lock new()ed memory, this memory *should*
//	be unlocked with the CompleteMemoryForDMA() method before calling delete(). The avclasses
//	destructor does call DmaUnlock() which attempts to unlock all locked pages with calls
//	to CompleteMemoryForDMA().
// NOTE: Any memory that is new()ed *should* be delete()ed before the process goes out of
//	scope.
bool CNTV2WinDriverInterface::PrepareMemoryForDMA (ULWord * pFrameBuffer, ULWord ulNumBytes)
{
	// Use NTV2_PIO as an overloaded flag to indicate this is not a DMA transfer
	bool bRet = DmaTransfer (NTV2_PIO, true, 0, pFrameBuffer, 0, ulNumBytes, false);
	// If succeeded, add pFrameBuffer to the avclasses' vector of locked memory
	if (bRet)
		_vecDmaLocked.push_back (pFrameBuffer);

	return bRet;
}

//////////////////////////////////////////////////////////////////////////////////////////////
// CompleteMemoryForDMA: unlock and free resources
// The inverse of PrepareMemoryForDMA.  It passes the framebuffer address to the kernel, where
//	the kernel looks up this address and unlocks the memory, unmaps the memory, and deletes the
//	MDL.
// NOTE: this method does not cleanup the call to new() which created the memory.  It is the new()
//	caller's responsibility to call delete() after calling this method to finish the cleanup.
bool CNTV2WinDriverInterface::CompleteMemoryForDMA (ULWord * pFrameBuffer)
{
	// Use NTV2_PIO as an overloaded flag to indicate this is not a DMA transfer
	bool bRet = DmaTransfer (NTV2_PIO, false, 0, pFrameBuffer, 0, 0);
	// If succeeded, remove pFrameBuffer from the avclasses' vector of locked memory
	if (bRet)
	{
		// Find the entry in the avclasses vector that holds this framebuffer's address
		for (vecIter = _vecDmaLocked.begin (); vecIter != _vecDmaLocked.end (); vecIter++)
		{
			// If we've found one, erase (delete) it
			if (*vecIter == pFrameBuffer)
			{
				_vecDmaLocked.erase (vecIter);
				break;
			}
		}
	}

	return bRet;
}

///////////////////////////////////////////////////////////////////////////////////
// Read and Write Register methods
///////////////////////////////////////////////////////////////////////////////////


bool CNTV2WinDriverInterface::ReadRegister(ULWord registerNumber, ULWord *registerValue, ULWord registerMask,
							   ULWord registerShift)
{
	if (_remoteHandle != INVALID_NUB_HANDLE)
	{
		if (!CNTV2DriverInterface::ReadRegister(
					registerNumber,
					registerValue,
					registerMask,
					registerShift))
		{
			DisplayNTV2Error("NTV2ReadRegisterRemote failed");
			return false;
		}
		return true;
	}
	else
	{

		KSPROPERTY_AJAPROPS_GETSETREGISTER_S propStruct;
		DWORD dwBytesReturned = 0;

		ZeroMemory(&propStruct,sizeof(KSPROPERTY_AJAPROPS_GETSETREGISTER_S));
		propStruct.Property.Set = _GUID_PROPSET;
		propStruct.Property.Id  = KSPROPERTY_AJAPROPS_GETSETREGISTER;
		propStruct.Property.Flags	= KSPROPERTY_TYPE_GET;
		propStruct.RegisterID		= registerNumber;
		propStruct.ulRegisterMask	= registerMask;
		propStruct.ulRegisterShift	= registerShift;

		BOOL fRet = FALSE;
		if(_boardType != DEVICETYPE_NTV2)
		{
			fRet = DeviceIoControl(
					 _hDevice,
					 IOCTL_KS_PROPERTY,
					 &propStruct,
					 sizeof(KSPROPERTY_AJAPROPS_GETSETREGISTER_S),
					 &propStruct,
					 sizeof(KSPROPERTY_AJAPROPS_GETSETREGISTER_S),
					 &dwBytesReturned,
					 NULL);
		}
		else
		{
			fRet = DeviceIoControl(
					_hDevice,
					IOCTL_AJAPROPS_GETSETREGISTER,
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_GETSETREGISTER_S),
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_GETSETREGISTER_S),
					&dwBytesReturned,
					NULL);
		}
		if (fRet)
		{
			*registerValue=propStruct.ulRegisterValue;
			return true;
		}
		else
		{
	#if defined (_DEBUG)
			LPVOID lpMsgBuf;
			FormatMessage(
				FORMAT_MESSAGE_ALLOCATE_BUFFER |
				FORMAT_MESSAGE_FROM_SYSTEM |
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				GetLastError(),
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				(LPTSTR) &lpMsgBuf,
				0,
				NULL
				);

			_tprintf (_T("ReadRegister regNumber=%d, regMask=0x%X, regShift=%d failed: %s\n"), registerNumber,
				registerMask, registerShift, (TCHAR *) lpMsgBuf);
			LocalFree (lpMsgBuf);
	#endif
			DisplayNTV2Error ("ReadRegister failed");

			return false;
		}
	}
}


bool CNTV2WinDriverInterface::WriteRegister (ULWord registerNumber,ULWord registerValue, ULWord registerMask,
								 ULWord registerShift)
{
	if (_remoteHandle != INVALID_NUB_HANDLE)
	{
		if (!CNTV2DriverInterface::WriteRegister(
					registerNumber,
					registerValue,
					registerMask,
					registerShift))
		{
			DisplayNTV2Error("NTV2WriteRegisterRemote failed");
			return false;
		}
		return true;
	}
	else
	{
		KSPROPERTY_AJAPROPS_GETSETREGISTER_S propStruct;
		DWORD dwBytesReturned = 0;

		ZeroMemory(&propStruct,sizeof(KSPROPERTY_AJAPROPS_GETSETREGISTER_S));
		propStruct.Property.Set	= _GUID_PROPSET;
		propStruct.Property.Id	= KSPROPERTY_AJAPROPS_GETSETREGISTER;
		propStruct.Property.Flags  = KSPROPERTY_TYPE_SET;
		propStruct.RegisterID	   = registerNumber;
		propStruct.ulRegisterValue = registerValue;
		propStruct.ulRegisterMask  = registerMask;
		propStruct.ulRegisterShift = registerShift;

		BOOL fRet =  FALSE;
		if(_boardType != DEVICETYPE_NTV2)
		{
			fRet = DeviceIoControl(
						 _hDevice,
						 IOCTL_KS_PROPERTY,
						 &propStruct,
						 sizeof(KSPROPERTY_AJAPROPS_GETSETREGISTER_S),
						 &propStruct,
						 sizeof(KSPROPERTY_AJAPROPS_GETSETREGISTER_S),
						 &dwBytesReturned,
						 NULL);
		}
		else
		{
			fRet = DeviceIoControl(
						_hDevice,
						IOCTL_AJAPROPS_GETSETREGISTER,
						&propStruct,
						sizeof(KSPROPERTY_AJAPROPS_GETSETREGISTER_S),
						&propStruct,
						sizeof(KSPROPERTY_AJAPROPS_GETSETREGISTER_S),
						&dwBytesReturned,
						NULL);
		}

		if (fRet)
			return true;
		else
			return false;
	}
}

/////////////////////////////////////////////////////////////////////////////
// Interrupt enabling / disabling method
/////////////////////////////////////////////////////////////////////////////

// Method:	ConfigureInterrupt
// Input:	bool bEnable (turn on/off interrupt), INTERRUPT_ENUMS eInterruptType
// Output:	bool status
// Purpose:	Provides a 1 point connection to driver for interrupt calls
bool CNTV2WinDriverInterface::ConfigureInterrupt (bool bEnable, INTERRUPT_ENUMS eInterruptType)
{
	assert( (_hDevice != INVALID_HANDLE_VALUE) && (_hDevice != 0) );

	KSPROPERTY_AJAPROPS_INTERRUPTS_S propStruct;	// boilerplate AVStream Property structure
	DWORD dwBytesReturned = 0;

	ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_INTERRUPTS_S));
	propStruct.Property.Set= _GUID_PROPSET;
	propStruct.Property.Id = KSPROPERTY_AJAPROPS_INTERRUPTS;
	if (bEnable)
		propStruct.Property.Flags = KSPROPERTY_TYPE_GET;	// driver enable is a "GET"
	else
		propStruct.Property.Flags = KSPROPERTY_TYPE_SET;	// driver disable is a "SET"
	propStruct.eInterrupt = eInterruptType;

	BOOL
	 fRet =  FALSE;
	if(_boardType != DEVICETYPE_NTV2)
	{
		fRet = DeviceIoControl(
					 _hDevice,
					 IOCTL_KS_PROPERTY,
					 &propStruct,
					 sizeof(KSPROPERTY_AJAPROPS_INTERRUPTS_S),
					 &propStruct,
					 sizeof(KSPROPERTY_AJAPROPS_INTERRUPTS_S),
					 &dwBytesReturned,
					 NULL);
	}
	else
	{
		fRet = DeviceIoControl(
					_hDevice,
					IOCTL_AJAPROPS_INTERRUPTS,
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_INTERRUPTS_S),
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_INTERRUPTS_S),
					&dwBytesReturned,
					NULL);

	}

	if (fRet)
		return true;
	else
	{
#if defined (_DEBUG)
		LPVOID lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL
			);

		_tprintf (_T("ConfigureInterrupt(bool bEnable = %d, Type = %d) failed: %s\n"), (int) bEnable,
			(int) eInterruptType, (TCHAR *) lpMsgBuf);
		LocalFree (lpMsgBuf);
#endif
		DisplayNTV2Error ("ConfigureInterrupt failed");

		return false;
	}
}

// Method: ConfigureSubscriptions
// Input:  bool bSubscribe (true if subscribing), INTERRUPT_ENUMS eInterruptType,
//		HANDLE & hSubcription
// Output: HANDLE & hSubcription (if subscribing)
// Notes:  collects all driver calls for subscriptions in one place
bool CNTV2WinDriverInterface::ConfigureSubscription (bool bSubscribe, INTERRUPT_ENUMS eInterruptType,
			PULWord & ulSubscription)
{
	CNTV2DriverInterface::ConfigureSubscription (bSubscribe, eInterruptType, ulSubscription);
	ulSubscription = mInterruptEventHandles [eInterruptType];

	// Check for previouse call to subscribe
	if ( bSubscribe && ulSubscription )
		return true;

	// Check for valid handle to unsubscribe
	if ( !bSubscribe && !ulSubscription )
		return true;

	// Assure that the avCard has been properly opened
	HANDLE hSubscription = (HANDLE) ulSubscription;
    assert( (_hDevice != INVALID_HANDLE_VALUE) && (_hDevice != 0) );

	KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS_S propStruct;
	DWORD dwBytesReturned = 0;

	BOOL bRet;
	ULWord driverVersion;
	GetDriverVersion(&driverVersion);
	if ( driverVersion & BIT_13 || _boardType == DEVICETYPE_NTV2)  // 64 bit driver.
	{
		// for support of 64 bit driver only in 5.0...
		ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS_S));
		propStruct.Property.Set= _GUID_PROPSET;
		propStruct.Property.Id = KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS;
		if (bSubscribe)
		{
			hSubscription = CreateEvent (NULL, FALSE, FALSE, NULL);
			propStruct.Property.Flags = KSPROPERTY_TYPE_GET;
		}
		else
			propStruct.Property.Flags = KSPROPERTY_TYPE_SET;
		propStruct.Handle = hSubscription;
		propStruct.eInterrupt = eInterruptType;

		if(_boardType != DEVICETYPE_NTV2)
		{
			bRet = DeviceIoControl(
						 _hDevice,
						 IOCTL_KS_PROPERTY,
						 &propStruct,
						 sizeof(KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS_S),
						 &propStruct,
						 sizeof(KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS_S),
						 &dwBytesReturned,
						 NULL);
		}
		else
		{
			bRet = DeviceIoControl(
						_hDevice,
						IOCTL_AJAPROPS_NEWSUBSCRIPTIONS,
						&propStruct,
						sizeof(KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS_S),
						&propStruct,
						sizeof(KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS_S),
						&dwBytesReturned,
						NULL);
		}
	}
	else
	{
		KSPROPERTY_AJAPROPS_SUBSCRIPTIONS_S propStruct;
		DWORD dwBytesReturned = 0;

		ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_SUBSCRIPTIONS_S));
		propStruct.Property.Set= _GUID_PROPSET;
		propStruct.Property.Id = KSPROPERTY_AJAPROPS_SUBSCRIPTIONS;
		if (bSubscribe)
		{
			hSubscription = CreateEvent (NULL, FALSE, FALSE, NULL);
			propStruct.Property.Flags = KSPROPERTY_TYPE_GET;
		}
		else
			propStruct.Property.Flags = KSPROPERTY_TYPE_SET;
		propStruct.pHandle = &hSubscription;
		propStruct.eInterrupt = eInterruptType;

		if(_boardType != DEVICETYPE_NTV2)
		{
			bRet = DeviceIoControl(
						 _hDevice,
						 IOCTL_KS_PROPERTY,
						 &propStruct,
						 sizeof(KSPROPERTY_AJAPROPS_SUBSCRIPTIONS_S),
						 &propStruct,
						 sizeof(KSPROPERTY_AJAPROPS_SUBSCRIPTIONS_S),
						 &dwBytesReturned,
						 NULL);
		}
		else
		{
			bRet = DeviceIoControl(
						_hDevice,
						IOCTL_AJAPROPS_SUBSCRIPTIONS,
						&propStruct,
						sizeof(KSPROPERTY_AJAPROPS_SUBSCRIPTIONS_S),
						&propStruct,
						sizeof(KSPROPERTY_AJAPROPS_SUBSCRIPTIONS_S),
						&dwBytesReturned,
						NULL);

		}

	}

	if (( !bSubscribe && bRet ) || ( bSubscribe && !bRet ))
	{
		CloseHandle (hSubscription);
		ulSubscription = 0;
	}

	if (!bRet)
	{
#if defined (_DEBUG)
		_tprintf (_T("ConfigureSubscription failed (bSubsribe = %d, SubscriptionType = %d\n"),
			(int) bSubscribe, (int) eInterruptType);
#endif
		DisplayNTV2Error("ConfigureSubscription failed");

	    return false;
    }

	if (bSubscribe)
		ulSubscription = (PULWord) hSubscription;

	return true;
}

// Method: getInterruptCount
// Input:  NONE
// Output: ULONG or equivalent(i.e. ULWord).
bool CNTV2WinDriverInterface::GetInterruptCount(INTERRUPT_ENUMS eInterruptType, ULWord *pCount)
{
	assert( (_hDevice != INVALID_HANDLE_VALUE) && (_hDevice != 0) );

	ULWord driverVersion;
	GetDriverVersion(&driverVersion);
	if ( driverVersion & BIT_13 )  // 64 bit driver.
	{
		KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS_S propStruct;
		DWORD dwBytesReturned = 0;

		ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS_S));
		propStruct.Property.Set	= _GUID_PROPSET;
		propStruct.Property.Id	= KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS;
		propStruct.Property.Flags = KSPROPERTY_TYPE_GET;
		propStruct.eInterrupt = eGetIntCount;
		propStruct.ulIntCount = (ULONG) eInterruptType;

		BOOL ioctlSuccess = FALSE;
		if(_boardType != DEVICETYPE_NTV2)
		{
			ioctlSuccess = DeviceIoControl(
								 _hDevice,
								 IOCTL_KS_PROPERTY,
								 &propStruct,
								 sizeof(KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS_S),
								 &propStruct,
								 sizeof(KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS_S),
								 &dwBytesReturned,
								 NULL);
		}
		else
		{
			ioctlSuccess = DeviceIoControl(
								_hDevice,
								IOCTL_AJAPROPS_NEWSUBSCRIPTIONS,
								&propStruct,
								sizeof(KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS_S),
								&propStruct,
								sizeof(KSPROPERTY_AJAPROPS_NEWSUBSCRIPTIONS_S),
								&dwBytesReturned,
								NULL);
		}

		if (!ioctlSuccess)
		{
			DisplayNTV2Error("getInterruptCount() failed");
			return false;
		}

		*pCount = propStruct.ulIntCount;
	}
	else
	{
		// This IOCTL gives us the number of actual interrupts that have happened
		// since the driver started up!
		KSPROPERTY_AJAPROPS_SUBSCRIPTIONS_S propStruct;
		DWORD dwBytesReturned = 0;

		ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_SUBSCRIPTIONS_S));
		propStruct.Property.Set	= _GUID_PROPSET;
		propStruct.Property.Id	= KSPROPERTY_AJAPROPS_SUBSCRIPTIONS;
		propStruct.Property.Flags = KSPROPERTY_TYPE_GET;
		propStruct.eInterrupt = eGetIntCount;
		propStruct.ulIntCount = (ULONG) eInterruptType;

		BOOL ioctlSuccess = FALSE;
		if(_boardType != DEVICETYPE_NTV2)
		{
			ioctlSuccess = DeviceIoControl(
								 _hDevice,
								 IOCTL_KS_PROPERTY,
								 &propStruct,
								 sizeof(KSPROPERTY_AJAPROPS_SUBSCRIPTIONS_S),
								 &propStruct,
								 sizeof(KSPROPERTY_AJAPROPS_SUBSCRIPTIONS_S),
								 &dwBytesReturned,
								 NULL);
		}
		else
		{
			ioctlSuccess = DeviceIoControl(
								_hDevice,
								IOCTL_AJAPROPS_SUBSCRIPTIONS,
								&propStruct,
								sizeof(KSPROPERTY_AJAPROPS_SUBSCRIPTIONS_S),
								&propStruct,
								sizeof(KSPROPERTY_AJAPROPS_SUBSCRIPTIONS_S),
								&dwBytesReturned,
								NULL);

		}

		if (!ioctlSuccess)
		{
			DisplayNTV2Error("getInterruptCount() failed");
			return false;
		}

		*pCount = propStruct.ulIntCount;
	}

	return true;
}

HANDLE CNTV2WinDriverInterface::GetInterruptEvent( INTERRUPT_ENUMS eInterruptType )
{
	return mInterruptEventHandles [eInterruptType];
}

bool CNTV2WinDriverInterface::WaitForInterrupt (INTERRUPT_ENUMS eInterruptType,
												ULWord timeOutMs)
{
	if (_remoteHandle != INVALID_NUB_HANDLE)
	{
		return CNTV2DriverInterface::WaitForInterrupt(eInterruptType,timeOutMs);
	}

    bool bInterruptHappened = false;    // return value

	HANDLE hEvent = mInterruptEventHandles [eInterruptType];

	if (NULL == hEvent)
	{
		// no interrupt hooked up so just use Sleep function
		Sleep (timeOutMs);
	}
	else
    {
        // interrupt hooked up. Wait
        DWORD status = WaitForSingleObject(hEvent, timeOutMs);
        if ( status == WAIT_OBJECT_0 )
        {
            bInterruptHappened = true;
            BumpEventCount (eInterruptType);
        }
		else
		{
			;//MessageBox (0, "WaitForInterrupt timed out", "CNTV2WinDriverInterface", MB_ICONERROR | MB_OK);
		}
    }

    return bInterruptHappened;
}

//////////////////////////////////////////////////////////////////////////////
// OEM Mapping to Userspace Methods
//////////////////////////////////////////////////////////////////////////////

// Method:	MapFrameBuffers
// Input:	None
// Output:	bool, and sets member _pBaseFrameAddress
bool CNTV2WinDriverInterface::MapFrameBuffers (void)
{
	assert( (_hDevice != INVALID_HANDLE_VALUE) && (_hDevice != 0) );

	KSPROPERTY_AJAPROPS_MAPMEMORY_S propStruct;
	DWORD dwBytesReturned = 0;

	_pFrameBaseAddress = 0;
    _pCh1FrameBaseAddress = 0;
	ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S));
	propStruct.Property.Set= _GUID_PROPSET;
	propStruct.Property.Id = KSPROPERTY_AJAPROPS_MAPMEMORY;
	propStruct.Property.Flags = KSPROPERTY_TYPE_GET;
	propStruct.bMapType = NTV2_MAPMEMORY_FRAMEBUFFER;

	BOOL fRet = FALSE;
	if(_boardType != DEVICETYPE_NTV2)
	{
		fRet = DeviceIoControl(
				 _hDevice,
				 IOCTL_KS_PROPERTY,
				 &propStruct,
				 sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
				 &propStruct,
				 sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
				 &dwBytesReturned,
				 NULL);
	}
	else
	{
		fRet = DeviceIoControl(
				_hDevice,
				IOCTL_AJAPROPS_MAPMEMORY,
				&propStruct,
				sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
				&propStruct,
				sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
				&dwBytesReturned,
				NULL);
	}

	if (fRet)
	{
		ULWord boardIDRegister;
		ReadRegister(kRegBoardID, &boardIDRegister);	//unfortunately GetBoardID is in ntv2card...ooops.
		_pFrameBaseAddress = (ULWord *) propStruct.mapMemory.Address;
		_pCh1FrameBaseAddress = _pFrameBaseAddress;
		_pCh2FrameBaseAddress = _pFrameBaseAddress;
        return true;
	}
	else
	{
#if defined (_DEBUG)
		LPVOID lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL
			);

		_tprintf (_T("MapFrameBuffers failed: %s\n"), (TCHAR *) lpMsgBuf);
		LocalFree (lpMsgBuf);
#endif
		DisplayNTV2Error ("MapFrameBuffers failed");

		return false;
	}
}

// Method:	UnmapFrameBuffers
// Input:	None
// Output:	bool status
bool CNTV2WinDriverInterface::UnmapFrameBuffers (void)
{
    ULWord boardIDRegister;
    ReadRegister(kRegBoardID, &boardIDRegister);	//unfortunately GetBoardID is in ntv2card...ooops.
    ULWord * pFrameBaseAddress;
    pFrameBaseAddress = _pFrameBaseAddress;
    if (pFrameBaseAddress == 0)
        return true;

    assert( (_hDevice != INVALID_HANDLE_VALUE) && (_hDevice != 0) );

    KSPROPERTY_AJAPROPS_MAPMEMORY_S propStruct;
    DWORD dwBytesReturned = 0;

    ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S));
	propStruct.Property.Set=_GUID_PROPSET;
	propStruct.Property.Id=KSPROPERTY_AJAPROPS_MAPMEMORY;
	propStruct.Property.Flags = KSPROPERTY_TYPE_SET;
	propStruct.bMapType = NTV2_MAPMEMORY_FRAMEBUFFER;
	propStruct.mapMemory.Address = pFrameBaseAddress;

	BOOL fRet = FALSE;
	if(_boardType != DEVICETYPE_NTV2)
	{
		fRet = DeviceIoControl(
			_hDevice,
			IOCTL_KS_PROPERTY,
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&dwBytesReturned,
			NULL);
	}
	else
	{
		fRet = DeviceIoControl(
			_hDevice,
			IOCTL_AJAPROPS_MAPMEMORY,
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&dwBytesReturned,
			NULL);
	}

	_pFrameBaseAddress = 0;
    _pCh1FrameBaseAddress = 0;
    _pCh2FrameBaseAddress = 0;

	if (fRet)
		return true;
	else
	{
#if defined (_DEBUG)
		LPVOID lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL
			);

		_tprintf (_T("UnmapFrameBuffers failed: %s\n"), (char *) lpMsgBuf);
		LocalFree (lpMsgBuf);
#endif
		DisplayNTV2Error ("UnmapFrameBuffers failed");


		return false;
	}
}

// Method:	MapRegisters
// Input:	None
// Output:	bool, and sets member _pBaseFrameAddress
bool CNTV2WinDriverInterface::MapRegisters (void)
{
	assert( (_hDevice != INVALID_HANDLE_VALUE) && (_hDevice != 0) );

	KSPROPERTY_AJAPROPS_MAPMEMORY_S propStruct;
	DWORD dwBytesReturned = 0;

	_pRegisterBaseAddress = 0;
	_pRegisterBaseAddressLength = 0;
	ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S));
	propStruct.Property.Set=_GUID_PROPSET;
	propStruct.Property.Id =KSPROPERTY_AJAPROPS_MAPMEMORY;
	propStruct.Property.Flags = KSPROPERTY_TYPE_GET;
	propStruct.bMapType = NTV2_MAPMEMORY_REGISTER;

	BOOL fRet = FALSE;
	if(_boardType != DEVICETYPE_NTV2)
	{
		fRet = DeviceIoControl(
			_hDevice,
			IOCTL_KS_PROPERTY,
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&dwBytesReturned,
			NULL);
	}
	else
	{
		fRet = DeviceIoControl(
			_hDevice,
			IOCTL_AJAPROPS_MAPMEMORY,
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&dwBytesReturned,
			NULL);
	}

	if (fRet)
	{
		_pRegisterBaseAddress = (ULWord *) propStruct.mapMemory.Address;
		_pRegisterBaseAddressLength = propStruct.mapMemory.Length;

		return true;
	}
	else
		return false;
}

// Method:	UnmapRegisters
// Input:	None
// Output:	bool status
bool CNTV2WinDriverInterface::UnmapRegisters (void)
{
	if (_pRegisterBaseAddress == 0)
		return true;

	assert( (_hDevice != INVALID_HANDLE_VALUE) && (_hDevice != 0) );

	KSPROPERTY_AJAPROPS_MAPMEMORY_S propStruct;
	DWORD dwBytesReturned = 0;

	ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S));
	propStruct.Property.Set=_GUID_PROPSET;
	propStruct.Property.Id=KSPROPERTY_AJAPROPS_MAPMEMORY;
	propStruct.Property.Flags = KSPROPERTY_TYPE_SET;
	propStruct.bMapType = NTV2_MAPMEMORY_REGISTER;
	propStruct.mapMemory.Address = _pRegisterBaseAddress;

	BOOL fRet = FALSE;
	if(_boardType != DEVICETYPE_NTV2)
	{
		fRet = DeviceIoControl(
			_hDevice,
			IOCTL_KS_PROPERTY,
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&dwBytesReturned,
			NULL);
	}
	else
	{
		fRet = DeviceIoControl(
			_hDevice,
			IOCTL_AJAPROPS_MAPMEMORY,
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&dwBytesReturned,
			NULL);
	}

	_pRegisterBaseAddress = 0;
	_pRegisterBaseAddressLength = 0;
	if (fRet)
		return true;
	else
		return false;
}



// Method:	MapRegisters
// Input:	None
// Output:	bool, and sets member _pBaseFrameAddress
bool CNTV2WinDriverInterface::MapXena2Flash (void)
{
	assert( (_hDevice != INVALID_HANDLE_VALUE) && (_hDevice != 0) );

	KSPROPERTY_AJAPROPS_MAPMEMORY_S propStruct;
	DWORD dwBytesReturned = 0;

	_pRegisterBaseAddress = 0;
	_pRegisterBaseAddressLength = 0;
	ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S));
	propStruct.Property.Set=_GUID_PROPSET;
	propStruct.Property.Id =KSPROPERTY_AJAPROPS_MAPMEMORY;
	propStruct.Property.Flags = KSPROPERTY_TYPE_GET;
	propStruct.bMapType = NTV2_MAPMEMORY_PCIFLASHPROGRAM;

	BOOL fRet = FALSE;
	if(_boardType != DEVICETYPE_NTV2)
	{
		fRet = DeviceIoControl(
			_hDevice,
			IOCTL_KS_PROPERTY,
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&dwBytesReturned,
			NULL);
	}
	else
	{
		fRet = DeviceIoControl(
			_hDevice,
			IOCTL_AJAPROPS_MAPMEMORY,
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&dwBytesReturned,
			NULL);
	}

	if (fRet)
	{
		_pXena2FlashBaseAddress = (ULWord *) propStruct.mapMemory.Address;
		_pRegisterBaseAddressLength = propStruct.mapMemory.Length;

		return true;
	}
	else
		return false;
}

// Method:	UnmapRegisters
// Input:	None
// Output:	bool status
bool CNTV2WinDriverInterface::UnmapXena2Flash (void)
{
	if (_pRegisterBaseAddress == 0)
		return true;

	assert( (_hDevice != INVALID_HANDLE_VALUE) && (_hDevice != 0) );

	KSPROPERTY_AJAPROPS_MAPMEMORY_S propStruct;
	DWORD dwBytesReturned = 0;

	ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S));
	propStruct.Property.Set=_GUID_PROPSET;
	propStruct.Property.Id=KSPROPERTY_AJAPROPS_MAPMEMORY;
	propStruct.Property.Flags = KSPROPERTY_TYPE_SET;
	propStruct.bMapType = NTV2_MAPMEMORY_PCIFLASHPROGRAM;
	propStruct.mapMemory.Address = _pRegisterBaseAddress;

	BOOL fRet = FALSE;
	if(_boardType != DEVICETYPE_NTV2)
	{
		fRet = DeviceIoControl(
			_hDevice,
			IOCTL_KS_PROPERTY,
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&dwBytesReturned,
			NULL);
	}
	else
	{
		fRet = DeviceIoControl(
			_hDevice,
			IOCTL_AJAPROPS_MAPMEMORY,
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&propStruct,
			sizeof(KSPROPERTY_AJAPROPS_MAPMEMORY_S),
			&dwBytesReturned,
			NULL);
	}

	_pRegisterBaseAddress = 0;
	_pRegisterBaseAddressLength = 0;
	if (fRet)
		return true;
	else
		return false;
}
//////////////////////////////////////////////////////////////////////////////////////
// DMA

bool CNTV2WinDriverInterface::DmaTransfer (NTV2DMAEngine DMAEngine, bool bRead, ULWord frameNumber,
							   ULWord * pFrameBuffer, ULWord offsetBytes, ULWord bytes,
							   bool bSync)
{
	assert( (_hDevice != INVALID_HANDLE_VALUE) && (_hDevice != 0) );

	KSPROPERTY_AJAPROPS_DMA_S propStruct;
	DWORD dwBytesReturned = 0;

	ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_DMA_S));
	propStruct.Property.Set   = _GUID_PROPSET;
	propStruct.Property.Id    = KSPROPERTY_AJAPROPS_DMA;
	if (bRead)
		propStruct.Property.Flags = KSPROPERTY_TYPE_GET;
	else
		propStruct.Property.Flags = KSPROPERTY_TYPE_SET;

	propStruct.dmaEngine = DMAEngine;
	propStruct.pvVidUserVa   = (PVOID) pFrameBuffer;
	propStruct.ulFrameNumber = frameNumber;
	propStruct.ulFrameOffset = offsetBytes;
	propStruct.ulVidNumBytes = bytes;
	propStruct.ulAudNumBytes = 0;
	propStruct.bSync		 = bSync;

	BOOL fRet = FALSE;
	if(_boardType != DEVICETYPE_NTV2)
	{
		fRet = DeviceIoControl(
					_hDevice,
					IOCTL_KS_PROPERTY,
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_S),
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_S),
					&dwBytesReturned,
					NULL);
	}
	else
	{
		fRet = DeviceIoControl(
					_hDevice,
					IOCTL_AJAPROPS_DMA,
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_S),
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_S),
					&dwBytesReturned,
					NULL);

	}

	if (fRet)
		return true;
	else
	{
#if defined (_DEBUG)
		LPVOID lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL
			);

		printf ("DmaTransfer failed: %s\n", (char *) lpMsgBuf);
		LocalFree (lpMsgBuf);
#endif
		DisplayNTV2Error ("DmaTransfer failed");

		return false;
	}
}

 bool CNTV2WinDriverInterface::DmaTransfer (NTV2DMAEngine DMAEngine, bool bRead, ULWord frameNumber,
							   ULWord* pFrameBuffer, ULWord offsetBytes, ULWord bytes,
							   ULWord videoNumSegments, ULWord videoSegmentHostPitch, ULWord videoSegmentCardPitch,
							   bool bSync)
{
	assert( (_hDevice != INVALID_HANDLE_VALUE) && (_hDevice != 0) );

	KSPROPERTY_AJAPROPS_DMA_EX_S propStruct;
	DWORD dwBytesReturned = 0;

	ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_DMA_EX_S));
	propStruct.Property.Set   = _GUID_PROPSET;
	propStruct.Property.Id    = KSPROPERTY_AJAPROPS_DMA_EX;
	if (bRead)
		propStruct.Property.Flags = KSPROPERTY_TYPE_GET;
	else
		propStruct.Property.Flags = KSPROPERTY_TYPE_SET;

	propStruct.dmaEngine = DMAEngine;
	propStruct.pvVidUserVa = (PVOID) pFrameBuffer;
	propStruct.ulFrameNumber = frameNumber;
	propStruct.ulFrameOffset = offsetBytes;
	propStruct.ulVidNumBytes = bytes;
	propStruct.ulAudNumBytes = 0;
	propStruct.ulVidNumSegments = videoNumSegments;
	propStruct.ulVidSegmentHostPitch = videoSegmentHostPitch;
	propStruct.ulVidSegmentCardPitch = videoSegmentCardPitch;
	propStruct.bSync = bSync;

  	BOOL fRet = FALSE;
	if(_boardType != DEVICETYPE_NTV2)
	{
		fRet = DeviceIoControl(
					_hDevice,
					IOCTL_KS_PROPERTY,
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_EX_S),
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_EX_S),
					&dwBytesReturned,
					NULL);
	}
	else
	{
		fRet = DeviceIoControl(
					_hDevice,
					IOCTL_AJAPROPS_DMA_EX,
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_EX_S),
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_EX_S),
					&dwBytesReturned,
					NULL);
	}

	if (fRet)
		return true;
	else
	{
#if defined (_DEBUG)
		LPVOID lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL
			);

		_tprintf (_T("DmaTransfer failed: %s\n"), (char *) lpMsgBuf);
		LocalFree (lpMsgBuf);
#endif
		DisplayNTV2Error ("DmaTransfer failed");

		return false;
	}
}

bool CNTV2WinDriverInterface::DmaTransfer (NTV2DMAEngine DMAEngine,
										   NTV2Channel DMAChannel,
										   bool bTarget,
										   ULWord frameNumber,
										   ULWord frameOffset,
										   ULWord videoSize,
										   ULWord videoNumSegments,
										   ULWord videoSegmentHostPitch,
										   ULWord videoSegmentCardPitch,
										   PCHANNEL_P2P_STRUCT pP2PData)
{
	assert( (_hDevice != INVALID_HANDLE_VALUE) && (_hDevice != 0) );

	if (pP2PData == NULL)
	{
		printf ("DmaTransfer failed: pP2PData == NULL");
		return false;
	}

	KSPROPERTY_AJAPROPS_DMA_P2P_S propStruct;
	DWORD dwBytesReturned = 0;

	ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_DMA_P2P_S));
	propStruct.Property.Set   = _GUID_PROPSET;
	propStruct.Property.Id    = KSPROPERTY_AJAPROPS_DMA_P2P;
	if (bTarget)
	{
		// reset p2p struct
		memset(pP2PData, 0, sizeof(CHANNEL_P2P_STRUCT));
		pP2PData->p2pSize = sizeof(CHANNEL_P2P_STRUCT);

		// get does target
		propStruct.Property.Flags = KSPROPERTY_TYPE_GET;
	}
	else
	{
		// check for valid p2p struct
		if (pP2PData->p2pSize != sizeof(CHANNEL_P2P_STRUCT))
		{
			printf ("DmaTransfer failed: pP2PData->p2pSize != sizeof(CHANNEL_P2P_STRUCT)");
			return false;
		}

		// set does transfer
		propStruct.Property.Flags = KSPROPERTY_TYPE_SET;
	}

	propStruct.dmaEngine = DMAEngine;
	propStruct.dmaChannel = DMAChannel;
	propStruct.ulFrameNumber = frameNumber;
	propStruct.ulFrameOffset = frameOffset;
	propStruct.ulVidNumBytes = videoSize;
	propStruct.ulVidNumSegments = videoNumSegments;
	propStruct.ulVidSegmentHostPitch = videoSegmentHostPitch;
	propStruct.ulVidSegmentCardPitch = videoSegmentCardPitch;
	propStruct.ullVideoBusAddress = pP2PData->videoBusAddress;
	propStruct.ullMessageBusAddress = pP2PData->messageBusAddress;
	propStruct.ulVideoBusSize = pP2PData->videoBusSize;
	propStruct.ulMessageData = pP2PData->messageData;

	BOOL fRet = FALSE;
	if(_boardType != DEVICETYPE_NTV2)
	{
		fRet = DeviceIoControl(
					_hDevice,
					IOCTL_KS_PROPERTY,
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_P2P_S),
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_P2P_S),
					&dwBytesReturned,
					NULL);
	}
	else
	{
		fRet = DeviceIoControl(
					_hDevice,
					IOCTL_AJAPROPS_DMA_P2P,
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_P2P_S),
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_P2P_S),
					&dwBytesReturned,
					NULL);
	}

	if (fRet)
	{
		if (bTarget)
		{
			// check for data returned
			if (dwBytesReturned != sizeof(KSPROPERTY_AJAPROPS_DMA_P2P_S))
			{
				return false;
			}

			// fill in p2p data
			pP2PData->videoBusAddress = propStruct.ullVideoBusAddress;
			pP2PData->messageBusAddress = propStruct.ullMessageBusAddress;
			pP2PData->videoBusSize = propStruct.ulVideoBusSize;
			pP2PData->messageData = propStruct.ulMessageData;
		}
		return true;
	}
	else
	{
#if defined (_DEBUG)
		LPVOID lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL
			);

		printf ("DmaTransfer failed: %s\n", (char *) lpMsgBuf);
		LocalFree (lpMsgBuf);
#endif
		DisplayNTV2Error ("DmaTransfer failed");

		return false;
	}
}

bool CNTV2WinDriverInterface::MapMemory (PVOID pvUserVa, ULWord ulNumBytes, bool bMap, ULWord* ulUser)
{
	assert( (_hDevice != INVALID_HANDLE_VALUE) && (_hDevice != 0) );

	KSPROPERTY_AJAPROPS_DMA_S propStruct;
	DWORD dwBytesReturned = 0;

	ZeroMemory (&propStruct,sizeof(KSPROPERTY_AJAPROPS_DMA_S));
	propStruct.Property.Set   = _GUID_PROPSET;
	propStruct.Property.Id    = KSPROPERTY_AJAPROPS_DMA;
	if (bMap)
		propStruct.Property.Flags = KSPROPERTY_TYPE_GET;
	else
		propStruct.Property.Flags = KSPROPERTY_TYPE_SET;

	propStruct.dmaEngine = NTV2_PIO;
	propStruct.pvVidUserVa   = pvUserVa;
	propStruct.ulVidNumBytes = ulNumBytes;

	BOOL fRet = FALSE;
	if(_boardType != DEVICETYPE_NTV2)
	{
		fRet = DeviceIoControl(
					_hDevice,
					IOCTL_KS_PROPERTY,
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_S),
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_S),
					&dwBytesReturned,
					NULL);
	}
	else
	{
		fRet = DeviceIoControl(
					_hDevice,
					IOCTL_AJAPROPS_DMA,
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_S),
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_DMA_S),
					&dwBytesReturned,
					NULL);
	}

	if (fRet)
	{
		if(ulUser != NULL)
		{
			*ulUser = propStruct.ulFrameOffset;
		}
		return true;
	}
	else
	{
#if defined (_DEBUG)
		LPVOID lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL
			);

		_tprintf (_T("DmaTransfer failed: %s\n"), (TCHAR *) lpMsgBuf);
		LocalFree (lpMsgBuf);
#endif
		DisplayNTV2Error ("DmaTransfer failed");

		return false;
	}
}

///////////////////////////////////////////////////////////////////////////
// AutoCirculate
bool CNTV2WinDriverInterface::AutoCirculate (AUTOCIRCULATE_DATA &autoCircData)
{
	if (_remoteHandle != INVALID_NUB_HANDLE)
	{
		if (!CNTV2DriverInterface::AutoCirculate(autoCircData))
		{
			DisplayNTV2Error("NTV2AutoCirculateRemote failed");
			return false;
		}
		return true;
	}
	else
	{
		bool bRes = true;
		DWORD	dwBytesReturned = 0;

		switch (autoCircData.eCommand)
		{
		case eInitAutoCirc:
			if(autoCircData.lVal4 <= 1)
			{
				KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_S autoCircControl;
				memset(&autoCircControl, 0, sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_S));
				autoCircControl.Property.Set	= _GUID_PROPSET;
				autoCircControl.Property.Id		= KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL;
				autoCircControl.Property.Flags	= KSPROPERTY_TYPE_SET;
				autoCircControl.channelSpec		= autoCircData.channelSpec;
				autoCircControl.eCommand		= autoCircData.eCommand;

				autoCircControl.lVal1 = autoCircData.lVal1;
				autoCircControl.lVal2 = autoCircData.lVal2;
				autoCircControl.lVal3 = autoCircData.lVal3;
				autoCircControl.bVal1 = autoCircData.bVal1;
				autoCircControl.bVal2 = autoCircData.bVal2;
				autoCircControl.bVal3 = autoCircData.bVal3;
				autoCircControl.bVal4 = autoCircData.bVal4;
				autoCircControl.bVal5 = autoCircData.bVal5;
				autoCircControl.bVal6 = autoCircData.bVal6;
				autoCircControl.bVal7 = autoCircData.bVal7;
				autoCircControl.bVal8 = autoCircData.bVal8;

				BOOL fRet = FALSE;
				if(_boardType != DEVICETYPE_NTV2)
				{
					fRet = DeviceIoControl(
								_hDevice,
								IOCTL_KS_PROPERTY,
								&autoCircControl,
								sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_S),
								&autoCircControl,
								sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_S),
								&dwBytesReturned,
								NULL);
				}
				else
				{
					fRet = DeviceIoControl(
								_hDevice,
								IOCTL_AJAPROPS_AUTOCIRC_CONTROL,
								&autoCircControl,
								sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_S),
								&autoCircControl,
								sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_S),
								&dwBytesReturned,
								NULL);
				}

				if (fRet)
				{
					bRes = true;
				}
				else
				{
					bRes = false;
#if defined (_DEBUG)
					LPVOID lpMsgBuf;
					FormatMessage(
						FORMAT_MESSAGE_ALLOCATE_BUFFER |
						FORMAT_MESSAGE_FROM_SYSTEM |
						FORMAT_MESSAGE_IGNORE_INSERTS,
						NULL,
						GetLastError(),
						MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
						(LPTSTR) &lpMsgBuf,
						0,
						NULL
						);

					printf ("AutoCirculate initialize failed: %s\n", (char *) lpMsgBuf);
					LocalFree (lpMsgBuf);
#endif
				}
			}
			else
			{
				KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_EX_S autoCircControl;
				memset(&autoCircControl, 0, sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_EX_S));
				autoCircControl.Property.Set	= _GUID_PROPSET;
				autoCircControl.Property.Id		= KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_EX;
				autoCircControl.Property.Flags	= KSPROPERTY_TYPE_SET;
				autoCircControl.channelSpec		= autoCircData.channelSpec;
				autoCircControl.eCommand		= autoCircData.eCommand;

				autoCircControl.lVal1 = autoCircData.lVal1;
				autoCircControl.lVal2 = autoCircData.lVal2;
				autoCircControl.lVal3 = autoCircData.lVal3;
				autoCircControl.lVal4 = autoCircData.lVal4;
				autoCircControl.bVal1 = autoCircData.bVal1;
				autoCircControl.bVal2 = autoCircData.bVal2;
				autoCircControl.bVal3 = autoCircData.bVal3;
				autoCircControl.bVal4 = autoCircData.bVal4;
				autoCircControl.bVal5 = autoCircData.bVal5;
				autoCircControl.bVal6 = autoCircData.bVal6;
				autoCircControl.bVal7 = autoCircData.bVal7;
				autoCircControl.bVal8 = autoCircData.bVal8;

				BOOL fRet = FALSE;
				if(_boardType != DEVICETYPE_NTV2)
				{
					fRet = DeviceIoControl(
								_hDevice,
								IOCTL_KS_PROPERTY,
								&autoCircControl,
								sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_EX_S),
								&autoCircControl,
								sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_EX_S),
								&dwBytesReturned,
								NULL);
				}
				else
				{
					fRet = DeviceIoControl(
								_hDevice,
								IOCTL_AJAPROPS_AUTOCIRC_CONTROL_EX,
								&autoCircControl,
								sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_EX_S),
								&autoCircControl,
								sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_EX_S),
								&dwBytesReturned,
								NULL);
				}

				if (fRet)
				{
					bRes = true;
				}
				else
				{
					bRes = false;
#if defined (_DEBUG)
					LPVOID lpMsgBuf;
					FormatMessage(
						FORMAT_MESSAGE_ALLOCATE_BUFFER |
						FORMAT_MESSAGE_FROM_SYSTEM |
						FORMAT_MESSAGE_IGNORE_INSERTS,
						NULL,
						GetLastError(),
						MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
						(LPTSTR) &lpMsgBuf,
						0,
						NULL
						);

					printf ("AutoCirculate initialize failed: %s\n", (char *) lpMsgBuf);
					LocalFree (lpMsgBuf);
#endif
				}
			}
			break;
		case eStartAutoCirc:
		case eStopAutoCirc:
		case eAbortAutoCirc:
		case ePauseAutoCirc:
		case eFlushAutoCirculate:
		case ePrerollAutoCirculate:
		case eStartAutoCircAtTime:
			{
				KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_S autoCircControl;
				memset(&autoCircControl, 0, sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_S));
				autoCircControl.Property.Set	= _GUID_PROPSET;
				autoCircControl.Property.Id		= KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL;
				autoCircControl.Property.Flags	= KSPROPERTY_TYPE_SET;
				autoCircControl.channelSpec		= autoCircData.channelSpec;
				autoCircControl.eCommand		= autoCircData.eCommand;

				switch (autoCircData.eCommand)
				{
				case ePauseAutoCirc:
					autoCircControl.bVal1 = autoCircData.bVal1;
					break;

				case ePrerollAutoCirculate:
					autoCircControl.lVal1 = autoCircData.lVal1;
					break;

				case eStartAutoCircAtTime:
					autoCircControl.lVal1 = autoCircData.lVal1;
					autoCircControl.lVal2 = autoCircData.lVal2;
					break;
				}

				BOOL fRet = FALSE;
				if(_boardType != DEVICETYPE_NTV2)
				{
					fRet = DeviceIoControl(
								_hDevice,
								IOCTL_KS_PROPERTY,
								&autoCircControl,
								sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_S),
								&autoCircControl,
								sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_S),
								&dwBytesReturned,
								NULL);
				}
				else
				{
					fRet = DeviceIoControl(
								_hDevice,
								IOCTL_AJAPROPS_AUTOCIRC_CONTROL,
								&autoCircControl,
								sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_S),
								&autoCircControl,
								sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_CONTROL_S),
								&dwBytesReturned,
								NULL);
				}

				if (fRet)
				{
					bRes = true;
				}
				else
				{
					bRes = false;
#if defined (_DEBUG)
					LPVOID lpMsgBuf;
					FormatMessage(
						FORMAT_MESSAGE_ALLOCATE_BUFFER |
						FORMAT_MESSAGE_FROM_SYSTEM |
						FORMAT_MESSAGE_IGNORE_INSERTS,
						NULL,
						GetLastError(),
						MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
						(LPTSTR) &lpMsgBuf,
						0,
						NULL
						);

					printf ("AutoCirculate command failed: %s\n", (char *) lpMsgBuf);
					LocalFree (lpMsgBuf);
#endif
				}
			}
			break;

		case eGetAutoCirc:
			{
				KSPROPERTY_AJAPROPS_AUTOCIRC_STATUS_S autoCircStatus;
				memset(&autoCircStatus, 0, sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_STATUS_S));
				autoCircStatus.Property.Set	  = _GUID_PROPSET;
				autoCircStatus.Property.Id	  = KSPROPERTY_AJAPROPS_AUTOCIRC_STATUS;
				autoCircStatus.Property.Flags = KSPROPERTY_TYPE_GET;
				autoCircStatus.channelSpec		= autoCircData.channelSpec;
				autoCircStatus.eCommand			= autoCircData.eCommand;

				if(autoCircData.pvVal1 != NULL)
				{
					BOOL fRet = FALSE;
					if(_boardType != DEVICETYPE_NTV2)
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_KS_PROPERTY,
									&autoCircStatus,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_STATUS_S),
									&autoCircStatus,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_STATUS_S),
									&dwBytesReturned,
									NULL);
					}
					else
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_AJAPROPS_AUTOCIRC_STATUS,
									&autoCircStatus,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_STATUS_S),
									&autoCircStatus,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_STATUS_S),
									&dwBytesReturned,
									NULL);
					}

					if (fRet)
					{
						*(AUTOCIRCULATE_STATUS_STRUCT *)autoCircData.pvVal1 = autoCircStatus.autoCircStatus;
					}
					else
					{
						bRes = false;
					}
				}
				else
				{
					bRes = false;
				}
			}
			break;

		case eGetFrameStamp:
			{
				KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_S autoCircFrame;
				memset(&autoCircFrame, 0, sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_S));
				autoCircFrame.Property.Set	  = _GUID_PROPSET;
				autoCircFrame.Property.Id	  = KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME;
				autoCircFrame.Property.Flags  = KSPROPERTY_TYPE_GET;
				autoCircFrame.channelSpec	= autoCircData.channelSpec;
				autoCircFrame.eCommand		= autoCircData.eCommand;
				autoCircFrame.lFrameNum		= autoCircData.lVal1;

				if(autoCircData.pvVal1 != NULL)
				{
					autoCircFrame.frameStamp	= *(FRAME_STAMP_STRUCT *) autoCircData.pvVal1;

					BOOL fRet = FALSE;
					if(_boardType != DEVICETYPE_NTV2)
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_KS_PROPERTY,
									&autoCircFrame,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_S),
									&autoCircFrame,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_S),
									&dwBytesReturned,
									NULL);
					}
					else
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_AJAPROPS_AUTOCIRC_FRAME,
									&autoCircFrame,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_S),
									&autoCircFrame,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_S),
									&dwBytesReturned,
									NULL);
					}

					if (fRet)
					{
						*(FRAME_STAMP_STRUCT *)autoCircData.pvVal1 = autoCircFrame.frameStamp;
					}
					else
					{
						bRes = false;
					}
				}
				else
				{
					bRes = false;
				}
			}
			break;

		case eGetFrameStampEx2:
			{
				KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_EX2_S autoCircFrame;
				memset(&autoCircFrame, 0, sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_EX2_S));
				autoCircFrame.Property.Set	  = _GUID_PROPSET;
				autoCircFrame.Property.Id	  = KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_EX2;
				autoCircFrame.Property.Flags  = KSPROPERTY_TYPE_GET;
				autoCircFrame.channelSpec	= autoCircData.channelSpec;
				autoCircFrame.eCommand		= autoCircData.eCommand;
				autoCircFrame.lFrameNum		= autoCircData.lVal1;

				if(autoCircData.pvVal1 != NULL)
				{
					autoCircFrame.frameStamp	= *(FRAME_STAMP_STRUCT *) autoCircData.pvVal1;
					if(autoCircData.pvVal2 != NULL)
					{
						autoCircFrame.acTask = *(AUTOCIRCULATE_TASK_STRUCT *) autoCircData.pvVal2;
					}

					BOOL fRet = FALSE;
					if(_boardType != DEVICETYPE_NTV2)
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_KS_PROPERTY,
									&autoCircFrame,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_EX2_S),
									&autoCircFrame,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_EX2_S),
									&dwBytesReturned,
									NULL);
					}
					else
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_AJAPROPS_AUTOCIRC_FRAME_EX2,
									&autoCircFrame,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_EX2_S),
									&autoCircFrame,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_EX2_S),
									&dwBytesReturned,
									NULL);
					}

					if (fRet)
					{
						*(FRAME_STAMP_STRUCT *)autoCircData.pvVal1 = autoCircFrame.frameStamp;
						if(autoCircData.pvVal2 != NULL)
						{
							*(AUTOCIRCULATE_TASK_STRUCT *) autoCircData.pvVal2 = autoCircFrame.acTask;
						}
					}
					else
					{
						bRes = false;
					}
				}
				else
				{
					bRes = false;
				}
			}
			break;

		case eTransferAutoCirculate:
			{
				KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_S autoCircTransfer;
				memset(&autoCircTransfer, 0, sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_S));
				autoCircTransfer.Property.Set	= _GUID_PROPSET;
				autoCircTransfer.Property.Id	= KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER;
				autoCircTransfer.Property.Flags = KSPROPERTY_TYPE_GET;
				autoCircTransfer.eCommand		= autoCircData.eCommand;

				if((autoCircData.pvVal1 != NULL) &&
					(autoCircData.pvVal2 != NULL))
				{
					autoCircTransfer.acTransfer = *(PAUTOCIRCULATE_TRANSFER_STRUCT) autoCircData.pvVal1;
					// if doing audio, insure buffer alignment is OK
					if (autoCircTransfer.acTransfer.audioBufferSize)
					{
						if (autoCircTransfer.acTransfer.audioBufferSize % 4)
						{
							bRes = false;
							DisplayNTV2Error ("TransferAutoCirculate failed - audio buffer size not mod 4");
							break;
						}

						if ((ULWord64) autoCircTransfer.acTransfer.audioBuffer % 4)
						{
							bRes = false;
							DisplayNTV2Error ("TransferAutoCirculate failed - audio buffer address not mod 4");
							break;
						}
					}

					AUTOCIRCULATE_TRANSFER_STATUS_STRUCT acStatus = *(PAUTOCIRCULATE_TRANSFER_STATUS_STRUCT) autoCircData.pvVal2;

					BOOL fRet = FALSE;
					if(_boardType != DEVICETYPE_NTV2)
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_KS_PROPERTY,
									&autoCircTransfer,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_S),
									&acStatus,
									sizeof (AUTOCIRCULATE_TRANSFER_STATUS_STRUCT),
									&dwBytesReturned,
									NULL);
					}
					else
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_AJAPROPS_AUTOCIRC_TRANSFER,
									&autoCircTransfer,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_S),
									&acStatus,
									sizeof (AUTOCIRCULATE_TRANSFER_STATUS_STRUCT),
									&dwBytesReturned,
									NULL);
					}

					if (fRet)
					{
						*(PAUTOCIRCULATE_TRANSFER_STATUS_STRUCT)autoCircData.pvVal2 = acStatus;
					}
					else
					{
						if ( _displayErrorMessage )
						{
							LPVOID lpMsgBuf;
							FormatMessage(
								FORMAT_MESSAGE_ALLOCATE_BUFFER |
								FORMAT_MESSAGE_FROM_SYSTEM |
								FORMAT_MESSAGE_IGNORE_INSERTS,
								NULL,
								GetLastError(),
								MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
								(LPTSTR) &lpMsgBuf,
								0,
								NULL
								);
							MessageBox (0, (const TCHAR*)lpMsgBuf, _T("CNTV2WinDriverInterface"), MB_ICONERROR | MB_OK);

							_tprintf (_T("AutoCirculate transfer failed: %s\n"), (const TCHAR *) lpMsgBuf);
							LocalFree (lpMsgBuf);
						}
						bRes = false;
					}
				}
				else
				{
					bRes = false;
				}
			}
			break;

		case eTransferAutoCirculateEx:
			{
				KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_EX_S autoCircTransfer;
				memset(&autoCircTransfer, 0, sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_EX_S));
				autoCircTransfer.Property.Set	= _GUID_PROPSET;
				autoCircTransfer.Property.Id	= KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_EX;
				autoCircTransfer.Property.Flags = KSPROPERTY_TYPE_GET;
				autoCircTransfer.eCommand		= autoCircData.eCommand;

				if((autoCircData.pvVal1 != NULL) &&
					(autoCircData.pvVal2 != NULL))
				{
					autoCircTransfer.acTransfer = *(PAUTOCIRCULATE_TRANSFER_STRUCT) autoCircData.pvVal1;
					if(autoCircData.pvVal3 != NULL)
					{
						autoCircTransfer.acTransferRoute = *(NTV2RoutingTable*) autoCircData.pvVal3;
					}
					// if doing audio, insure buffer alignment is OK
					if (autoCircTransfer.acTransfer.audioBufferSize)
					{
						if (autoCircTransfer.acTransfer.audioBufferSize % 4)
						{
							bRes = false;
							DisplayNTV2Error ("TransferAutoCirculate failed - audio buffer size not mod 4");
							break;
						}

						if ((ULWord64) autoCircTransfer.acTransfer.audioBuffer % 4)
						{
							bRes = false;
							DisplayNTV2Error ("TransferAutoCirculate failed - audio buffer address not mod 4");
							break;
						}
					}

					AUTOCIRCULATE_TRANSFER_STATUS_STRUCT acStatus = *(PAUTOCIRCULATE_TRANSFER_STATUS_STRUCT) autoCircData.pvVal2;

					BOOL fRet = FALSE;
					if(_boardType != DEVICETYPE_NTV2)
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_KS_PROPERTY,
									&autoCircTransfer,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_EX_S),
									&acStatus,
									sizeof (AUTOCIRCULATE_TRANSFER_STATUS_STRUCT),
									&dwBytesReturned,
									NULL);
					}
					else
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_AJAPROPS_AUTOCIRC_TRANSFER_EX,
									&autoCircTransfer,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_EX_S),
									&acStatus,
									sizeof (AUTOCIRCULATE_TRANSFER_STATUS_STRUCT),
									&dwBytesReturned,
									NULL);
					}

					if (fRet)
					{
						*(PAUTOCIRCULATE_TRANSFER_STATUS_STRUCT)autoCircData.pvVal2 = acStatus;
					}
					else
					{
						if ( _displayErrorMessage )
						{
							LPVOID lpMsgBuf;
							FormatMessage(
								FORMAT_MESSAGE_ALLOCATE_BUFFER |
								FORMAT_MESSAGE_FROM_SYSTEM |
								FORMAT_MESSAGE_IGNORE_INSERTS,
								NULL,
								GetLastError(),
								MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
								(LPTSTR) &lpMsgBuf,
								0,
								NULL
								);
							MessageBox (0, (const TCHAR*)lpMsgBuf, _T("CNTV2WinDriverInterface"), MB_ICONERROR | MB_OK);

							_tprintf (_T("AutoCirculate transfer failed: %s\n"), (const TCHAR *) lpMsgBuf);
							LocalFree (lpMsgBuf);
						}
						bRes = false;
					}
				}
				else
				{
					bRes = false;
				}
			}
			break;

		case eTransferAutoCirculateEx2:
			{
				KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_EX2_S autoCircTransfer;
				memset(&autoCircTransfer, 0, sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_EX2_S));
				autoCircTransfer.Property.Set	= _GUID_PROPSET;
				autoCircTransfer.Property.Id	= KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_EX2;
				autoCircTransfer.Property.Flags = KSPROPERTY_TYPE_GET;
				autoCircTransfer.eCommand		= autoCircData.eCommand;

				if((autoCircData.pvVal1 != NULL) &&
					(autoCircData.pvVal2 != NULL))
				{
					autoCircTransfer.acTransfer = *(PAUTOCIRCULATE_TRANSFER_STRUCT) autoCircData.pvVal1;
					if(autoCircData.pvVal3 != NULL)
					{
						autoCircTransfer.acTransferRoute = *(NTV2RoutingTable*) autoCircData.pvVal3;
					}
					if(autoCircData.pvVal4 != NULL)
					{
						autoCircTransfer.acTask = *(PAUTOCIRCULATE_TASK_STRUCT) autoCircData.pvVal4;
					}
					// if doing audio, insure buffer alignment is OK
					if (autoCircTransfer.acTransfer.audioBufferSize)
					{
						if (autoCircTransfer.acTransfer.audioBufferSize % 4)
						{
							bRes = false;
							DisplayNTV2Error ("TransferAutoCirculate failed - audio buffer size not mod 4");
							break;
						}

						if ((ULWord64) autoCircTransfer.acTransfer.audioBuffer % 4)
						{
							bRes = false;
							DisplayNTV2Error ("TransferAutoCirculate failed - audio buffer address not mod 4");
							break;
						}
					}

					AUTOCIRCULATE_TRANSFER_STATUS_STRUCT acStatus = *(PAUTOCIRCULATE_TRANSFER_STATUS_STRUCT) autoCircData.pvVal2;

					BOOL fRet = FALSE;
					if(_boardType != DEVICETYPE_NTV2)
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_KS_PROPERTY,
									&autoCircTransfer,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_EX2_S),
									&acStatus,
									sizeof (AUTOCIRCULATE_TRANSFER_STATUS_STRUCT),
									&dwBytesReturned,
									NULL);
					}
					else
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_AJAPROPS_AUTOCIRC_TRANSFER_EX2,
									&autoCircTransfer,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_TRANSFER_EX2_S),
									&acStatus,
									sizeof (AUTOCIRCULATE_TRANSFER_STATUS_STRUCT),
									&dwBytesReturned,
									NULL);
					}

					if (fRet)
					{
						*(PAUTOCIRCULATE_TRANSFER_STATUS_STRUCT)autoCircData.pvVal2 = acStatus;
					}
					else
					{
						if ( _displayErrorMessage )
						{
							LPVOID lpMsgBuf;
							FormatMessage(
								FORMAT_MESSAGE_ALLOCATE_BUFFER |
								FORMAT_MESSAGE_FROM_SYSTEM |
								FORMAT_MESSAGE_IGNORE_INSERTS,
								NULL,
								GetLastError(),
								MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
								(LPTSTR) &lpMsgBuf,
								0,
								NULL
								);
							MessageBox (0, (const TCHAR*)lpMsgBuf, _T("CNTV2WinDriverInterface"), MB_ICONERROR | MB_OK);

							_tprintf (_T("AutoCirculate transfer failed: %s\n"), (const TCHAR *) lpMsgBuf);
							LocalFree (lpMsgBuf);
						}
						bRes = false;
					}
				}
				else
				{
					bRes = false;
				}
			}
			break;

		case eSetCaptureTask:
			{
				KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_EX2_S autoCircFrame;
				memset(&autoCircFrame, 0, sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_EX2_S));
				autoCircFrame.Property.Set	  = _GUID_PROPSET;
				autoCircFrame.Property.Id	  = KSPROPERTY_AJAPROPS_AUTOCIRC_CAPTURE_TASK;
				autoCircFrame.Property.Flags  = KSPROPERTY_TYPE_SET;
				autoCircFrame.channelSpec	= autoCircData.channelSpec;
				autoCircFrame.eCommand		= autoCircData.eCommand;
				autoCircFrame.lFrameNum		= 0;

				if(autoCircData.pvVal1 != NULL)
				{
					autoCircFrame.acTask = *(AUTOCIRCULATE_TASK_STRUCT *) autoCircData.pvVal1;

					BOOL fRet = FALSE;
					if(_boardType != DEVICETYPE_NTV2)
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_KS_PROPERTY,
									&autoCircFrame,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_EX2_S),
									&autoCircFrame,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_EX2_S),
									&dwBytesReturned,
									NULL);
					}
					else
					{
						fRet = DeviceIoControl(
									_hDevice,
									IOCTL_AJAPROPS_AUTOCIRC_CAPTURE_TASK,
									&autoCircFrame,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_EX2_S),
									&autoCircFrame,
									sizeof(KSPROPERTY_AJAPROPS_AUTOCIRC_FRAME_EX2_S),
									&dwBytesReturned,
									NULL);
					}

					if (!fRet)
					{
						bRes = false;
					}
				}
				else
				{
					bRes = false;
				}
			}
			break;
		}

		return bRes;
	}
}


bool CNTV2WinDriverInterface::ControlDriverDebugMessages(NTV2_DriverDebugMessageSet msgSet,
								bool enable )
{
    return false;
}


bool CNTV2WinDriverInterface::SetRelativeVideoPlaybackDelay(ULWord frameDelay)
{
	return WriteRegister (kRegRelativeVideoPlaybackDelay, frameDelay);
}

bool CNTV2WinDriverInterface::GetRelativeVideoPlaybackDelay(ULWord* frameDelay)
{
	return ReadRegister (kRegRelativeVideoPlaybackDelay, frameDelay);
}

bool CNTV2WinDriverInterface::SetAudioRecordPinDelay(ULWord millisecondDelay)
{
	return WriteRegister (kRegAudioRecordPinDelay, millisecondDelay);
}

bool CNTV2WinDriverInterface::GetStrictTiming(ULWord* strictTiming)
{
	return ReadRegister (kRegStrictTiming, strictTiming);
}

bool CNTV2WinDriverInterface::SetStrictTiming(ULWord strictTiming)
{
	return WriteRegister (kRegStrictTiming, strictTiming);
}


bool CNTV2WinDriverInterface::GetAudioRecordPinDelay(ULWord* millisecondDelay)
{
	return ReadRegister (kRegAudioRecordPinDelay, millisecondDelay);
}

bool CNTV2WinDriverInterface::GetDriverVersion(ULWord* driverVersion)
{
	return ReadRegister (kRegDriverVersion, driverVersion);
}

bool CNTV2WinDriverInterface::SetAudioOutputMode(NTV2_GlobalAudioPlaybackMode mode)
{
	return WriteRegister(kRegGlobalAudioPlaybackMode,mode);
}

bool CNTV2WinDriverInterface::GetAudioOutputMode(NTV2_GlobalAudioPlaybackMode* mode)
{
	return ReadRegister(kRegGlobalAudioPlaybackMode,(ULWord*)mode);
}

bool CNTV2WinDriverInterface::DisplayNTV2Error (const char *str)
{
	if ( _displayErrorMessage )
	{
        TCHAR* buf = 0;
#if defined (UNICODE) || defined (_UNICODE)
        int len = MultiByteToWideChar(CP_UTF8, 0, str, -1, 0, 0);
        buf = new TCHAR[len];
        MultiByteToWideChar(CP_UTF8, 0, str, -1, buf, len);
        const TCHAR* msg = buf;
#else
        const TCHAR* msg = str;
#endif
#if defined (_CONSOLE)
		_tprintf (_T("%s\n"), msg);
#else
		MessageBox (0, msg, _T("CNTV2WinDriverInterface"), MB_ICONERROR | MB_OK);
#endif
        delete[] buf;
        return true;
	}

	return false;
}

//
// Management of downloaded Xilinx bitfile
//
//
bool
CNTV2WinDriverInterface::DriverGetBitFileInformation(
		BITFILE_INFO_STRUCT &bitFileInfo,
		NTV2BitFileType bitFileType)
{
	if (_remoteHandle != INVALID_NUB_HANDLE)
	{
		if (!CNTV2DriverInterface::DriverGetBitFileInformation(
				bitFileInfo,
				bitFileType))
		{
			DisplayNTV2Error("NTV2DriverGetBitFileInformationRemote failed");
			return false;
		}
		return true;
	}
	else
	{
		//The boards below only have one bitfile and there is no need to query the driver
		if(NTV2BoardHasSPIFlash(_boardID))
		{
			ParseFlashHeader(bitFileInfo);
			bitFileInfo.bitFileType = 0;
			switch(_boardID)
			{
			case DEVICE_ID_CORVID1:
				bitFileInfo.bitFileType = NTV2_BITFILE_CORVID1_MAIN;
				break;
			case DEVICE_ID_LHI:
				bitFileInfo.bitFileType = NTV2_BITFILE_LHI_MAIN;
				break;
			case DEVICE_ID_IOEXPRESS:
				bitFileInfo.bitFileType = NTV2_BITFILE_IOEXPRESS_MAIN;
				break;
			case DEVICE_ID_CORVID22:
				bitFileInfo.bitFileType = NTV2_BITFILE_CORVID22_MAIN;
				break;
			case DEVICE_ID_KONA3G:
				bitFileInfo.bitFileType = NTV2_BITFILE_KONA3G_MAIN;
				break;
			case DEVICE_ID_CORVID3G:
				bitFileInfo.bitFileType = NTV2_BITFILE_CORVID3G_MAIN;
				break;
			case DEVICE_ID_LHE_PLUS:
				bitFileInfo.bitFileType = NTV2_BITFILE_KONALHE_PLUS;
				break;
			case DEVICE_ID_KONA3GQUAD:
				bitFileInfo.bitFileType = NTV2_BITFILE_KONA3G_QUAD;
				break;
			case DEVICE_ID_IOXT:
				bitFileInfo.bitFileType = NTV2_BITFILE_IOXT_MAIN;
				break;
			case DEVICE_ID_CORVID24:
				bitFileInfo.bitFileType = NTV2_BITFILE_CORVID24_MAIN;
				break;
			case DEVICE_ID_TTAP:
				bitFileInfo.bitFileType = NTV2_BITFILE_TTAP_MAIN;
				break;
			case DEVICE_ID_IO4K:
				bitFileInfo.bitFileType = NTV2_BITFILE_IO4K_MAIN;
				break;
			case DEVICE_ID_IO4KUFC:
				bitFileInfo.bitFileType = NTV2_BITFILE_IO4KUFC_MAIN;
				break;
			case DEVICE_ID_KONA4:
				bitFileInfo.bitFileType = NTV2_BITFILE_KONA4_MAIN;
				break;
			case DEVICE_ID_KONA4UFC:
				bitFileInfo.bitFileType = NTV2_BITFILE_KONA4UFC_MAIN;
				break;
			case DEVICE_ID_CORVID88:
				bitFileInfo.bitFileType = NTV2_BITFILE_CORVID88;
				break;
			case DEVICE_ID_CORVID44:
				bitFileInfo.bitFileType = NTV2_BITFILE_CORVID44;
				break;
			}
			bitFileInfo.checksum = 0;
			bitFileInfo.structVersion = 0;
			bitFileInfo.structSize = sizeof(BITFILE_INFO_STRUCT);
			bitFileInfo.whichFPGA = eFPGAVideoProc;

			return true;
		}
		else
		{
			KSPROPERTY_AJAPROPS_GETSETBITFILEINFO_S propStruct;
			DWORD dwBytesReturned = 0;

			ZeroMemory(&propStruct,sizeof(KSPROPERTY_AJAPROPS_GETSETBITFILEINFO_S));
			propStruct.Property.Set = _GUID_PROPSET;
			propStruct.Property.Id  = KSPROPERTY_AJAPROPS_GETSETBITFILEINFO;
			propStruct.Property.Flags	= KSPROPERTY_TYPE_GET;

			BOOL fRet = FALSE;
			if(_boardType != DEVICETYPE_NTV2)
			{
				fRet = DeviceIoControl(
							 _hDevice,
							 IOCTL_KS_PROPERTY,
							 &propStruct,
							 sizeof(KSPROPERTY_AJAPROPS_GETSETBITFILEINFO_S),
							 &propStruct,
							 sizeof(KSPROPERTY_AJAPROPS_GETSETBITFILEINFO_S),
							 &dwBytesReturned,
							 NULL);
			}
			else
			{
				fRet = DeviceIoControl(
							_hDevice,
							IOCTL_AJAPROPS_GETSETBITFILEINFO,
							&propStruct,
							sizeof(KSPROPERTY_AJAPROPS_GETSETBITFILEINFO_S),
							&propStruct,
							sizeof(KSPROPERTY_AJAPROPS_GETSETBITFILEINFO_S),
							&dwBytesReturned,
							NULL);
			}
			if (fRet)
			{
				bitFileInfo = propStruct.bitFileInfoStruct;
				return true;
			}
			else
			{
				DisplayNTV2Error ("DriverGetBitFileInformation failed");
				return false;
			}
		}
	}
}

bool
CNTV2WinDriverInterface::DriverSetBitFileInformation(
		BITFILE_INFO_STRUCT &bitFileInfo)
{
 	KSPROPERTY_AJAPROPS_GETSETBITFILEINFO_S propStruct;
	DWORD dwBytesReturned = 0;

	ZeroMemory(&propStruct,sizeof(KSPROPERTY_AJAPROPS_GETSETBITFILEINFO_S));
	propStruct.Property.Set = _GUID_PROPSET;
	propStruct.Property.Id  = KSPROPERTY_AJAPROPS_GETSETBITFILEINFO;
	propStruct.Property.Flags	= KSPROPERTY_TYPE_SET;
	propStruct.bitFileInfoStruct = bitFileInfo;

	BOOL fRet = FALSE;
	if(_boardType != DEVICETYPE_NTV2)
	{
		fRet = DeviceIoControl(
					 _hDevice,
					 IOCTL_KS_PROPERTY,
					 &propStruct,
					 sizeof(KSPROPERTY_AJAPROPS_GETSETBITFILEINFO_S),
					 &propStruct,
					 sizeof(KSPROPERTY_AJAPROPS_GETSETBITFILEINFO_S),
					 &dwBytesReturned,
					 NULL);
	}
	else
	{
		fRet = DeviceIoControl(
					_hDevice,
					IOCTL_AJAPROPS_GETSETBITFILEINFO,
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_GETSETBITFILEINFO_S),
					&propStruct,
					sizeof(KSPROPERTY_AJAPROPS_GETSETBITFILEINFO_S),
					&dwBytesReturned,
					NULL);
	}
	if (fRet)
	{
		return true;
	}
	else
	{
		DisplayNTV2Error ("DriverGetBitFileInformation failed");
		return false;
	}
}


#include <ntv2boardfeatures.h>
bool CNTV2WinDriverInterface::SwitchBitfile(NTV2DeviceID boardID, NTV2BitfileType bitfile)
{
		// nothing to do...
	if (bitfile == NTV2_BITFILE_NO_CHANGE)
		return true;

	ULWord numRegisters = NTV2BoardGetNumberRegisters(boardID);
	ULWord *regValues = new ULWord[numRegisters];

	if (regValues == NULL)
	{
		DisplayNTV2Error("CNTV2WinDriverInterface::SwitchBitfile() failed to download new bitfile: out of memory.");
		return false;
	}


	for (ULWord i = 0; i < numRegisters; i ++)
		ReadRegister(i, &regValues[i]);

	// Disable all interrupts.
	for ( ULWord i=0; i<eNumInterruptTypes; i++)
	{
		ConfigureInterrupt(false,INTERRUPT_ENUMS(i));
	}

	// Tell the driver to switch bitfiles
	// The driver will check to see if this is a ok bitfile for the current board.
	bool status = WriteRegister(kRegBitFileDownload,(ULWord)bitfile);

	for (ULWord i = 0; i < numRegisters; i ++)
	{
		if ( i == kRegDMAControl )		// don't inadvertantly set 'go' bit.
			continue;

		if ( i == kRegFlashProgramReg)  // or muck with the flash.
			continue;

		if ( i >= kRegReserved51  && i < kRegRP188InOut2DBB )
			continue;

		if ( i >= kRegRS422Transmit  && i < kRegHDMIInputStatus )
			continue;

		WriteRegister(i, regValues[i]);
	}


	delete [] regValues;

	return status;
}

bool CNTV2WinDriverInterface::RestoreHardwareProcampRegisters()
{
	bool status = WriteRegister(kRegRestoreHardwareProcampRegisters, 0);

	return status;
}

PerfCounterTimestampMode CNTV2WinDriverInterface::GetPerfCounterTimestampMode()
{
	ULWord mode;
	ReadRegister(kRegTimeStampMode,&mode);

	return (PerfCounterTimestampMode)mode;
}

void CNTV2WinDriverInterface::SetPerfCounterTimestampMode(PerfCounterTimestampMode mode)
{
	WriteRegister(kRegTimeStampMode,mode);
}

LWord64 CNTV2WinDriverInterface::GetLastOutputVerticalTimestamp()
{
	LWord64 value;
	ULWord loValue;
	ULWord hiValue;
	ReadRegister(kRegTimeStampLastOutputVerticalLo,&loValue);
	ReadRegister(kRegTimeStampLastOutputVerticalHi,&hiValue);
	value = loValue | ((LWord64)hiValue<<32);

	return value;
}

LWord64 CNTV2WinDriverInterface::GetLastInput1VerticalTimestamp()
{
	LWord64 value;
	ULWord loValue;
	ULWord hiValue;
	ReadRegister(kRegTimeStampLastInput1VerticalLo,&loValue);
	ReadRegister(kRegTimeStampLastInput1VerticalHi,&hiValue);
	value = loValue | ((LWord64)hiValue<<32);

	return value;
}

LWord64 CNTV2WinDriverInterface::GetLastInput2VerticalTimestamp()
{
	LWord64 value;
	ULWord loValue;
	ULWord hiValue;
	ReadRegister(kRegTimeStampLastInput2VerticalLo,&loValue);
	ReadRegister(kRegTimeStampLastInput2VerticalHi,&hiValue);
	value = loValue | ((LWord64)hiValue<<32);

	return value;
}

LWord64 CNTV2WinDriverInterface::GetLastOutputVerticalTimestamp(NTV2Channel channel)
{
	LWord64 value;
	ULWord loValue;
	ULWord hiValue;
	ReadRegister(gChannelToTSLastOutputVertLo[channel],&loValue);
	ReadRegister(gChannelToTSLastOutputVertHi[channel],&hiValue);
	value = loValue | ((LWord64)hiValue<<32);

	return value;
}

LWord64 CNTV2WinDriverInterface::GetLastInputVerticalTimestamp(NTV2Channel channel)
{
	LWord64 value;
	ULWord loValue;
	ULWord hiValue;
	ReadRegister(gChannelToTSLastInputVertLo[channel],&loValue);
	ReadRegister(gChannelToTSLastInputVertHi[channel],&hiValue);
	value = loValue | ((LWord64)hiValue<<32);

	return value;
}


bool CNTV2WinDriverInterface::NeedToLimitDMAToOneMegabyte()
{

	ULWord driverVersion;
	GetDriverVersion(&driverVersion);
	bool status = false;
	if ( driverVersion & BIT_13 )  // 64 bit driver.
	{
		UWord majorVersion = (driverVersion>>4) & 0xF;
		UWord minorVersion = (driverVersion & 0xF);
		if ( majorVersion >= 6 && minorVersion >= 2)
		{
			status = false; // handled in driver
		}
		else
		{
			// if memory > 2 gigs need to limit
			PERFORMANCE_INFORMATION pi;
			GetPerformanceInfo(&pi,sizeof(PERFORMANCE_INFORMATION));
			ULWord64 memoryInstalled = pi.PhysicalTotal*pi.PageSize;
			if ( memoryInstalled > ULWord64((ULWord64)2*(ULWord64)1024*(ULWord64)1024*(ULWord64)1024)) // 2 gigs
				status = true;

		}
	}

	return status;

}

bool CNTV2WinDriverInterface::ReadRP188Registers( NTV2Channel /*channel-not-used*/, RP188_STRUCT* pRP188Data )
{
	bool bSuccess = false;
	RP188_STRUCT rp188;
	NTV2DeviceID boardID = DEVICE_ID_NOTFOUND;
	RP188SourceSelect source = kRP188SourceEmbeddedLTC;
	ULWord dbbReg, msReg, lsReg;

	ReadRegister(kRegBoardID, (ULWord *)&boardID);
	ReadRegister(kRP188SourceSelect, (ULWord *)&source);
	bool bLTCPort = (source == kRP188SourceLTCPort);

	// values come from LTC port registers
	if (bLTCPort)
	{
		ULWord ltcPresent;
		ReadRegister (kRegStatus, &ltcPresent, kRegMaskLTCInPresent, kRegShiftLTCInPresent);

		// there is no equivalent DBB for LTC port - we synthesize it here
		rp188.DBB = (ltcPresent) ? 0xFE000000 | NEW_SELECT_RP188_RCVD : 0xFE000000;

		// LTC port registers
		dbbReg = 0; // don't care - does not exist
		msReg = kRegLTCAnalogBits0_31;
		lsReg  = kRegLTCAnalogBits32_63;
	}

	// values come from RP188 registers
	else
	{
		NTV2Channel channel = NTV2_CHANNEL1;
		NTV2InputVideoSelect inputSelect = NTV2_Input1Select;

		switch (boardID)
		{
		case DEVICE_ID_LHI:
		case DEVICE_ID_IOXT:
		case DEVICE_ID_CORVID22:
		case DEVICE_ID_KONA3G:
		case DEVICE_ID_KONA3GQUAD:
		case DEVICE_ID_CORVID24:
		case DEVICE_ID_IO4K:
		case DEVICE_ID_IO4KUFC:
		case DEVICE_ID_KONA4:
		case DEVICE_ID_KONA4UFC:
			ReadRegister (kK2RegInputSelect, (ULWord *)&inputSelect);
			channel = (inputSelect == NTV2_Input1Select) ? NTV2_CHANNEL1 : NTV2_CHANNEL2;
			break;

		case DEVICE_ID_IOEXPRESS:
		case DEVICE_ID_CORVID1:
		default:
			channel = NTV2_CHANNEL1;
			break;
		}

		// rp188 registers
		dbbReg = (channel == NTV2_CHANNEL1 ? kRegRP188InOut1DBB       : kRegRP188InOut2DBB      );
		msReg  = (channel == NTV2_CHANNEL1 ? kRegRP188InOut1Bits0_31  : kRegRP188InOut2Bits0_31 );
		lsReg  = (channel == NTV2_CHANNEL1 ? kRegRP188InOut1Bits32_63 : kRegRP188InOut2Bits32_63);
	}

	// initialize values
	if (!bLTCPort)
		ReadRegister (dbbReg, &rp188.DBB );
	ReadRegister (msReg,  &rp188.Low );
	ReadRegister (lsReg,  &rp188.High);

	// register stability filter
	do
	{
		// struct copy to result
		*pRP188Data = rp188;

		// read again into local struct
		if (!bLTCPort)
			ReadRegister (dbbReg, &rp188.DBB );
		ReadRegister (msReg,  &rp188.Low );
		ReadRegister (lsReg,  &rp188.High);

		// if the new read equals the previous read, consider it done
		if ( (rp188.DBB  == pRP188Data->DBB) &&
			(rp188.Low  == pRP188Data->Low) &&
			(rp188.High == pRP188Data->High) )
		{
			bSuccess = true;
		}

	} while (bSuccess == false);

	return true;
}

//--------------------------------------------------------------------------------------------------------------------
//	Get/Set Output Timecode settings
//--------------------------------------------------------------------------------------------------------------------
bool CNTV2WinDriverInterface::SetOutputTimecodeOffset( ULWord frames )
{
	return WriteRegister(kRegOutputTimecodeOffset, frames);
}

bool CNTV2WinDriverInterface::GetOutputTimecodeOffset( ULWord* pFrames )
{
	return ReadRegister(kRegOutputTimecodeOffset, pFrames);
}

bool CNTV2WinDriverInterface::SetOutputTimecodeType( ULWord type )
{
	return WriteRegister( kRegOutputTimecodeType, type );
}

bool CNTV2WinDriverInterface::GetOutputTimecodeType( ULWord* pType )
{
	return ReadRegister(kRegOutputTimecodeType, pType);
}

//--------------------------------------------------------------------------------------------------------------------
//	LockFormat
//
//	For Kona this is currently a no-op
//	For IoHD this will for bitfile swaps / Isoch channel rebuilds based on vidoe mode / video format
//--------------------------------------------------------------------------------------------------------------------
bool CNTV2WinDriverInterface::LockFormat( void )
{
	//stub
	return false;
}

//--------------------------------------------------------------------------------------------------------------------
//	StartDriver
//
//	For IoHD this is currently a no-op
//	For Kona this starts the driver after all of the bitFiles have been sent to the driver.
//--------------------------------------------------------------------------------------------------------------------
bool CNTV2WinDriverInterface::StartDriver( DriverStartPhase phase )
{
	//stub
	return false;
}

bool CNTV2WinDriverInterface::GetQuicktimeUsingBoard( ULWord* qtApp )
{
	return ReadRegister(kRegQuicktimeUsingBoard, qtApp);
}

bool CNTV2WinDriverInterface::SetDefaultVideoOutMode ( uint32_t mode,
													   uint32_t channel,
													   uint32_t frameNum,
													   uint32_t initializing )
{
	return WriteRegister(kRegDefaultVideoOutMode, mode);
}

bool CNTV2WinDriverInterface::GetDefaultVideoOutMode( ULWord* pMode )
{
	return ReadRegister(kRegDefaultVideoOutMode, pMode);
}

//--------------------------------------------------------------------------------------------------------------------
//	Get/Set Debug Levels
//--------------------------------------------------------------------------------------------------------------------
bool CNTV2WinDriverInterface::SetUserModeDebugLevel( ULWord level )
{
	//stub
	return false;
}

bool CNTV2WinDriverInterface::GetUserModeDebugLevel( ULWord* level )
{
	//stub
	return false;
}

bool CNTV2WinDriverInterface::SetKernelModeDebugLevel( ULWord level )
{
	//stub
	return false;
}

bool CNTV2WinDriverInterface::GetKernelModeDebugLevel( ULWord* level )
{
	//stub
	return false;
}

//--------------------------------------------------------------------------------------------------------------------
//	Get/Set Ping Levels
//--------------------------------------------------------------------------------------------------------------------
bool CNTV2WinDriverInterface::SetUserModePingLevel( ULWord level )
{
	//stub
	return false;
}

bool CNTV2WinDriverInterface::GetUserModePingLevel( ULWord* level )
{
	//stub
	return false;
}

bool CNTV2WinDriverInterface::SetKernelModePingLevel( ULWord level )
{
	//stub
	return false;
}

bool CNTV2WinDriverInterface::GetKernelModePingLevel( ULWord* level )
{
	//stub
	return false;
}

//--------------------------------------------------------------------------------------------------------------------
//	Get/Set Latency Timer
//--------------------------------------------------------------------------------------------------------------------
bool CNTV2WinDriverInterface::SetLatencyTimerValue( ULWord value )
{
	//stub
	return false;
}

bool CNTV2WinDriverInterface::GetLatencyTimerValue( ULWord* value )
{
	//stub
	return false;
}

//--------------------------------------------------------------------------------------------------------------------
//	Get/Set include/exclude debug filter strings
//--------------------------------------------------------------------------------------------------------------------
bool CNTV2WinDriverInterface::SetDebugFilterStrings( const char* includeString,const char* excludeString )
{
	//stub
	return false;
}

//--------------------------------------------------------------------------------------------------------------------
//	GetDebugFilterStrings
//--------------------------------------------------------------------------------------------------------------------
bool CNTV2WinDriverInterface::GetDebugFilterStrings( char* includeString,char* excludeString )
{
	//stub
	return false;
}

bool CNTV2WinDriverInterface::ProgramXilinx( void* dataPtr, uint32_t dataSize )
{
	//stub
	return false;
}

bool CNTV2WinDriverInterface::LoadBitFile( void* dataPtr, uint32_t dataSize, NTV2BitfileType bitFileType )
{
	//stub
	return false;
}

//--------------------------------------------------------------------------------------------------------------------
//	Application acquire and release stuff
//--------------------------------------------------------------------------------------------------------------------
bool CNTV2WinDriverInterface::AcquireStreamForApplicationWithReference( ULWord appCode, int32_t pid )
{
	ULWord currentCode, currentPID;
	ReadRegister(kRegApplicationCode, &currentCode);
	ReadRegister(kRegApplicationPID, &currentPID);

	HANDLE pH = OpenProcess(READ_CONTROL, false, (DWORD)currentPID);
	if(INVALID_HANDLE_VALUE != pH && NULL != pH)
	{
		CloseHandle(pH);
	}
	else
	{
		ReleaseStreamForApplication(currentCode, currentPID);
	}

	ReadRegister(kRegApplicationCode, &currentCode);
	ReadRegister(kRegApplicationPID, &currentPID);

	for(int count = 0; count < 20; count++)
	{
		if(currentPID == 0)
		{
			//Nothing has the board
			if(!WriteRegister(kRegApplicationCode, appCode))
			{
				return false;
			}
			else
			{
				//Just in case this is not zero?
				WriteRegister(kRegAcquireReferenceCount, 0);
				WriteRegister(kRegAcquireReferenceCount, 1);
				return WriteRegister(kRegApplicationPID, (ULWord)pid);
			}
		}
		else if(currentCode == appCode && currentPID == (ULWord)pid)
		{
			//This process has already acquired so bump the count
			return WriteRegister(kRegAcquireReferenceCount, 1);
		}
		else
		{
			::Sleep(50);
			//Something has the board
//			return false;
		}
	}
	return false;
}

bool CNTV2WinDriverInterface::ReleaseStreamForApplicationWithReference( ULWord appCode, int32_t pid )
{
	ULWord currentCode, currentPID, currentCount;
	ReadRegister(kRegApplicationCode, &currentCode);
	ReadRegister(kRegApplicationPID, &currentPID);
	ReadRegister(kRegAcquireReferenceCount, &currentCount);

	if(currentCode == appCode && currentPID == (ULWord)pid)
	{
		if(currentCount > 1)
		{
			return WriteRegister(kRegReleaseReferenceCount, 1);
		}
		else if(currentCount == 1)
		{
			return ReleaseStreamForApplication(appCode, pid);
		}
		else
		{
			return true;
		}
	}
	else
		return false;
}

bool CNTV2WinDriverInterface::AcquireStreamForApplication( ULWord appCode, int32_t pid )
{
	for(int count = 0; count < 20; count++)
	{
		if(!WriteRegister(kRegApplicationCode, appCode))
			::Sleep(50);
		else
			return WriteRegister(kRegApplicationPID, (ULWord)pid);
	}

	ULWord currentCode, currentPID;
	ReadRegister(kRegApplicationCode, &currentCode);
	ReadRegister(kRegApplicationPID, &currentPID);
	HANDLE pH = OpenProcess(READ_CONTROL, false, (DWORD)currentPID);

	if(INVALID_HANDLE_VALUE != pH && NULL != pH)
	{
		CloseHandle(pH);
	}
	else
	{
		ReleaseStreamForApplication(currentCode, currentPID);
		for(int count = 0; count < 20; count++)
		{
			if(!WriteRegister(kRegApplicationCode, appCode))
				::Sleep(50);
			else
				return WriteRegister(kRegApplicationPID, (ULWord)pid);
		}
	}

	return false;
}

bool CNTV2WinDriverInterface::ReleaseStreamForApplication( ULWord appCode, int32_t pid )
{
	if(WriteRegister(kRegReleaseApplication, (ULWord)pid))
	{
		WriteRegister(kRegAcquireReferenceCount, 0);
		return true;//We don't really care if the count fails
	}
	else
		return false;
}

bool CNTV2WinDriverInterface::SetStreamingApplication( ULWord appCode, int32_t pid )
{
	if(!WriteRegister(kRegForceApplicationCode, appCode))
		return false;
	else
		return WriteRegister(kRegForceApplicationPID, (ULWord)pid);
}

bool CNTV2WinDriverInterface::GetStreamingApplication( ULWord *appCode, int32_t  *pid )
{
	if(!ReadRegister(kRegApplicationCode, appCode))
		return false;
	else
		 return ReadRegister(kRegApplicationPID, (ULWord*)pid);
}

bool CNTV2WinDriverInterface::SetDefaultDeviceForPID( int32_t pid )
{
	//stub
	return false;
}

bool CNTV2WinDriverInterface::IsDefaultDeviceForPID( int32_t pid )
{
	//stub
	return false;
}
