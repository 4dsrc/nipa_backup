////////////////////////////////////////////////////////////
//
//	Copyright (C) 2003, 2004, 2005, 2006, 2007 AJA Video Systems, Inc.
//	Proprietary and Confidential information.
//
////////////////////////////////////////////////////////////
#ifndef NTV2WINDRIVERINTERFACE_H
#define NTV2WINDRIVERINTERFACE_H

#include "ajaexport.h"

#include <windows.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <ks.h>
#include <ksmedia.h>
#include <Setupapi.h>
#include <cfgmgr32.h>
#include "psapi.h"

#include "ntv2driverinterface.h"
#include "ntv2winpublicinterface.h"
#include "ntv2boardfeatures.h"


// oem dma: save locked pages in a stl::vector
#include <vector>
using namespace std;
typedef vector<ULWord *> DMA_LOCKED_VEC;


class AJAExport CNTV2WinDriverInterface : public CNTV2DriverInterface
{
public:
	CNTV2WinDriverInterface()
	{
		_bOpenShared = true;
		_bOpenOverlapped = false;
		_hDevInfoSet = INVALID_HANDLE_VALUE;
		_pspDevIFaceDetailData = NULL;
	}

	virtual ~CNTV2WinDriverInterface()
	{
	}

public:
	// Interfaces
	// Call this method to set the Open share mode
	bool SetShareMode (bool bShared);

	// Call this method to set the Open overlapped mode
	bool SetOverlappedMode (bool bOverlapped);

	bool Open(UWord boardNumber=0, bool displayError = false,
		NTV2BoardType eBoardType = DEVICETYPE_UNKNOWN,
		const char *hostname = 0   // Non-null: card on remote host
		);

	bool Close();

	//NTV2BoardType   GetCompileFlag (void);

	bool WriteRegister(ULWord registerNumber,
							   ULWord registerValue,
							   ULWord registerMask = 0xFFFFFFFF,
							   ULWord registerShift = 0x0);
	bool ReadRegister(ULWord registerNumber,
							  ULWord *registerValue,
							  ULWord registerMask = 0xFFFFFFFF,
							  ULWord registerShift = 0x0);

    bool DmaTransfer (NTV2DMAEngine DMAEngine,
							  bool bRead,
							  ULWord frameNumber,
							  ULWord * pFrameBuffer,
							  ULWord offsetBytes,
							  ULWord bytes,
							  bool bSync = true);

	bool DmaTransfer (NTV2DMAEngine DMAEngine,
						bool bRead,
						ULWord frameNumber,
						ULWord * pFrameBuffer,
						ULWord offsetBytes,
						ULWord bytes,
						ULWord videoNumSegments,
						ULWord videoSegmentHostPitch,
						ULWord videoSegmentCardPitch,
						bool bSync);

	bool DmaTransfer (NTV2DMAEngine DMAEngine,
						NTV2Channel DMAChannel,
						bool bTarget,
						ULWord frameNumber,
						ULWord frameOffset,
						ULWord videoSize,
						ULWord videoNumSegments,
						ULWord videoSegmentHostPitch,
						ULWord videoSegmentCardPitch,
						PCHANNEL_P2P_STRUCT pP2PData);

	bool MapMemory (PVOID pvUserVa, ULWord ulNumBytes, bool bMap, ULWord* ulUser = NULL);

	bool DmaUnlock   (void);

	bool CompleteMemoryForDMA (ULWord * pFrameBuffer);

	bool PrepareMemoryForDMA (ULWord * pFrameBuffer, ULWord ulNumBytes);

	bool ConfigureInterrupt (bool bEnable,
									 INTERRUPT_ENUMS eInterruptType);

	bool MapFrameBuffers (void);

	bool UnmapFrameBuffers (void);

	bool MapRegisters (void);

	bool UnmapRegisters (void);

	bool MapXena2Flash (void);

	bool UnmapXena2Flash (void);

	bool ConfigureSubscription (bool bSubscribe,
	                                    INTERRUPT_ENUMS eInterruptType,
				                        PULWord & hSubcription);

	bool GetInterruptCount (INTERRUPT_ENUMS eInterrupt,
								    ULWord *pCount);

	bool WaitForInterrupt (INTERRUPT_ENUMS eInterruptType, ULWord timeOutMs = 50);

	HANDLE GetInterruptEvent (INTERRUPT_ENUMS eInterruptType);

	bool AutoCirculate (AUTOCIRCULATE_DATA &autoCircData);

  	bool ControlDriverDebugMessages(NTV2_DriverDebugMessageSet msgSet,
									bool enable );

    bool GetDriverVersion(ULWord* driverVersion);

	virtual Word SleepMs(LWord msec) { Sleep(msec); return (Word)msec;}

public:
//
//  virtual register access.
//
    bool SetRelativeVideoPlaybackDelay (ULWord frameDelay);
    bool GetRelativeVideoPlaybackDelay (ULWord* frameDelay);
    bool SetAudioPlaybackPinDelay (ULWord millisecondDelay);
    bool GetAudioPlaybackPinDelay (ULWord* millisecondDelay);
    bool SetAudioRecordPinDelay (ULWord millisecondDelay);
    bool GetAudioRecordPinDelay (ULWord* millisecondDelay);
	bool SetAudioOutputMode(NTV2_GlobalAudioPlaybackMode mode);
	bool GetAudioOutputMode(NTV2_GlobalAudioPlaybackMode* mode);
    bool SetStrictTiming (ULWord strictTiming);
    bool GetStrictTiming (ULWord* strictTiming);

	// These are only implemented in Mac code but need to be here to satisfy Win/Linux build
	bool		SuspendAudio () { return true; }
	bool		ResumeAudio (ULWord frameBufferSize) {(void)frameBufferSize; return true; }


	PerfCounterTimestampMode GetPerfCounterTimestampMode();
	void SetPerfCounterTimestampMode(PerfCounterTimestampMode mode);

	LWord64 GetLastOutputVerticalTimestamp();
	LWord64 GetLastInput1VerticalTimestamp();
	LWord64 GetLastInput2VerticalTimestamp();

	LWord64 GetLastOutputVerticalTimestamp(NTV2Channel channel);
	LWord64 GetLastInputVerticalTimestamp(NTV2Channel channel);

	bool DisplayNTV2Error (const char *str);

// Management of downloaded Xilinx bitfile
    bool DriverGetBitFileInformation(BITFILE_INFO_STRUCT &bitFileInfo,
		                             NTV2BitFileType bitFileType=NTV2_VideoProcBitFile);

    bool DriverSetBitFileInformation(BITFILE_INFO_STRUCT &bitFileInfo);

	// Functions for cards that support more than one bitfile

	bool SwitchBitfile(NTV2DeviceID boardID, NTV2BitfileType bitfile);

	bool RestoreHardwareProcampRegisters();

	bool NeedToLimitDMAToOneMegabyte();

	bool ReadRP188Registers( NTV2Channel channel, RP188_STRUCT* pRP188Data );

	bool SetOutputTimecodeOffset( ULWord frames );
	bool GetOutputTimecodeOffset( ULWord* pFrames );
	bool SetOutputTimecodeType( ULWord type );
	bool GetOutputTimecodeType( ULWord* pType );
	bool LockFormat( void );
	bool StartDriver( DriverStartPhase phase );
	bool GetQuicktimeUsingBoard( ULWord* value );
	bool SetDefaultVideoOutMode ( uint32_t mode,
								  uint32_t channel = 1,
								  uint32_t frameNum = 14,
								  uint32_t initializing = 0 );

	bool GetDefaultVideoOutMode( ULWord* pMode );

	bool AcquireStreamForApplicationWithReference( ULWord appType, int32_t pid );
	bool ReleaseStreamForApplicationWithReference( ULWord appType, int32_t pid );

	/**
		@brief		Reserves exclusive use of the AJA device for a given process, preventing other processes on the host
					from acquiring it until subsequently released.
		@result		True if successful; otherwise false.
		@param[in]	appType			An unsigned 32-bit value that uniquely and globally identifies the calling
									application or process that is requesting exclusive use of the device.
		@param[in]	pid				Specifies the OS-specific process identifier that uniquely identifies the
									running process on the host machine that is requesting exclusive use of the device.
		@details	This method asks the AJA device driver to reserve exclusive use of the AJA device by the given running host process.
					The AJA device driver records the "process ID" of the process granted this exclusive access,
					and also records the 4-byte "application type" that was specified by the caller. This "application
					type" is interpreted as a four-byte (ASCII) identifier, and is displayed by the AJA Control Panel
					application to show the device is being used exclusively by the given application.
					If another host process has already reserved the device, the function will fail and return false.
		@note		AJA recommends saving the device's NTV2EveryFrameTaskMode at the time AcquireStreamForApplication
					is called, and restoring it after releasing the device.
		@note		Device acquisition and release is greatly simplified using the new CNTV2AutoRelease class.
	**/
	bool AcquireStreamForApplication( ULWord appType, int32_t pid );

	/**
		@brief		Releases exclusive use of the AJA device for a given process, permitting other processes to acquire it.
		@result		True if successful; otherwise false.
		@param[in]	appType			A 32-bit value that uniquely and globally identifies the calling application or
									process that is releasing exclusive use of the device. This value must match the
									appType that was specified in the prior call to AcquireStreamForApplication.
		@param[in]	pid				Specifies the OS-specific process identifier that uniquely identifies the running
									process on the host machine that is releasing exclusive use of the device. This
									value must match the "process ID" that was specified in the prior call to AcquireStreamForApplication.
		@details	This method asks the AJA device driver to release exclusive use of the AJA device by the given running host process.
					The AJA device driver records the "process ID" of the process granted this exclusive access, and
					also records the 4-byte "application type" that was specified by the caller.
					This method will fail and return false if the specified application type or process ID values
					don't match those used in the prior call to AcquireStreamForApplication.
		@note		AJA recommends saving the device's NTV2EveryFrameTaskMode at the time AcquireStreamForApplication
					is called, and restoring it after releasing the device.
		@note		Device acquisition and release is greatly simplified using the new CNTV2AutoRelease class.
	**/
	bool ReleaseStreamForApplication( ULWord appType, int32_t pid );

	bool SetStreamingApplication( ULWord appType, int32_t pid );
	bool GetStreamingApplication( ULWord *appType, int32_t  *pid );
	bool SetDefaultDeviceForPID( int32_t pid );
	bool IsDefaultDeviceForPID( int32_t pid );

	bool SetUserModeDebugLevel( ULWord  level );
	bool GetUserModeDebugLevel( ULWord* level );
	bool SetKernelModeDebugLevel( ULWord  level );
	bool GetKernelModeDebugLevel( ULWord* level );

	bool SetUserModePingLevel( ULWord  level );
	bool GetUserModePingLevel( ULWord* level );
	bool SetKernelModePingLevel( ULWord  level );
	bool GetKernelModePingLevel( ULWord* level );

	bool SetLatencyTimerValue( ULWord value );
	bool GetLatencyTimerValue( ULWord* value );

	bool SetDebugFilterStrings( const char* includeString,const char* excludeString );
	bool GetDebugFilterStrings( char* includeString,char* excludeString );

	bool ProgramXilinx( void* dataPtr, uint32_t dataSize );
	bool LoadBitFile( void* dataPtr, uint32_t dataSize, NTV2BitfileType bitFileType );

protected:

	PSP_DEVICE_INTERFACE_DETAIL_DATA _pspDevIFaceDetailData;
	SP_DEVINFO_DATA _spDevInfoData;
	HDEVINFO		_hDevInfoSet;
	HANDLE			_hDevice;
	bool			_bOpenShared;
	bool			_bOpenOverlapped;
	GUID			_GUID_PROPSET;

	// OEM save locked memory addresses in vector
	DMA_LOCKED_VEC _vecDmaLocked;
	DMA_LOCKED_VEC::iterator vecIter;

};

#endif
