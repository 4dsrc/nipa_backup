/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file       SdiSdk.h
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 \date	2013-11-01
 \for multi connection
 \version 1.1
 */

#pragma once

#include <vector>
#include "SdiDefines.h"
#define SDI_EXPORT __declspec(dllexport)

class CSdiSingleMgr;

class SDI_EXPORT CSdiSdk
{

public:   
	CSdiSingleMgr *g_pCSdiSingleMgr;
	//************************************
    // Returns:   
    // Qualifier:
    // Parameter: int nDeviceNum
    // Parameter: CString strVendor
    // Parameter: CString strProduct
    //************************************
    CSdiSdk(int nDeviceNum, CString strVendor, CString strProduct);
	CSdiSdk(int nDeviceNum, CString strVendor, CString strProduct, LARGE_INTEGER swFreq, LARGE_INTEGER swStart);
    //************************************
    // Returns:   
    // Qualifier:
    // Parameter: void
    //************************************
    ~CSdiSdk(void);

public:
    //************************************
    // Returns:   SdiResult
    // Qualifier:
    // Parameter: SdiStrArray *pArList
    //************************************
    static SdiResult    SdiGetDeviceHandleList(SdiStrArray *pArList);

    //************************************
    // Returns:   SdiResult
    // Qualifier:
    // Parameter: SdiChar * strDevName
    //************************************
    SdiResult           SdiInitSession(SdiChar *strDevName);
    //************************************
    // Returns:   SdiResult
    // Qualifier:
    //************************************
    SdiResult           SdiCloseSession();    

    //************************************
    // Returns:   SdiVoid
    // Qualifier:
    // Parameter: HWND hRecvWnd
    //************************************
    SdiVoid             SdiSetReceiveHandler(HWND hRecvWnd);
    
    //************************************
    // Returns:   SdiResult
    // Qualifier:
    // Parameter: SdiMessage * pMsg
    //************************************
    SdiResult           SdiAddMsg(SdiMessage* pMsg);

    //************************************
    // Returns:   SdiVoid
    // Qualifier:
    //************************************
	SdiVoid				SdiLiveviewSet(SdiBool bLiveview);
	SdiVoid				SdiCheckFunction();
	SdiString			SdiGetInfo(int nCommand);

	SdiString			SdiGetDeviceModel();
	SdiResult			SdiGetModel();
	//************************************
    // 2015/01/Returns:   SdiVoid
    // Qualifier:
    //************************************
	SdiResult           SdiCheckWifi();

	
	SdiResult			SdiGetDeviceID(SdiChar *strDevName);
	SdiResult			SdiSetDeviceID(CString strUUID);

	SdiVoid	SdiInit();
	SdiInt SdiGetDSCTime();
	SdiInt SdiGetPCTime();
	SdiVoid SdiSetParents();
	SdiVoid SdiSetInitPreRec(BOOL b);
	SdiBool SdiGetInitPreRec();

	int m_nFrameCount;
	CString m_strDSCID;
	int m_nSensorStart;
	int m_nStartTime;
};

