**** yolov3(test)용 배포 파일*****
2019.12.18 skHong

1. 내용물
\data
yolov3.cfg
yolov3.weights
coco.data

2. 사용법
 1) 2019/12 테스트용 4DAP
    F 드라이브에 복사~ (별도 경로 이용시 소스 경로 수정 요망)
 2) 그외
    c 드라이브에 복사~ (소스상 경로 확인 요망)