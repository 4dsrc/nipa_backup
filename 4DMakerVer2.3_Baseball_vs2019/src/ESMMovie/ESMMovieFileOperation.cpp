////////////////////////////////////////////////////////////////////////////////
//
//	ESMFileOperation.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "shlwapi.h"
#include "ESMMovieFileOperation.h" 
#include "ESMDirRead.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#pragma comment(lib, "version.lib")
#pragma warning(disable:4996)


//--**********************************************************************************************************
CFExeption::CFExeption(DWORD dwErrCode)
{
	LPVOID lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			      NULL, dwErrCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
	m_sError = (LPTSTR)lpMsgBuf;
	LocalFree(lpMsgBuf);
	m_dwError = dwErrCode;
}


CFExeption::CFExeption(CString sErrText)
{
	m_sError = sErrText;
	m_dwError = 0;
}


//--**********************************************************************************************************

CESMMovieFileOperation::CESMMovieFileOperation()
{
	Initialize();
}


void CESMMovieFileOperation::Initialize()
{
	m_sError = _T("No error");
	m_dwError = 0;
	m_bAskIfReadOnly = TRUE;
	m_bOverwriteMode = FALSE;
	m_bAborted = FALSE;
	m_iRecursionLimit = -1;
}


void CESMMovieFileOperation::DoDelete(CString sPathName, BOOL bRemoveFolder)
{
	CFileFind ff;
	CString sPath = sPathName;

	if (CheckPath(sPath) == PATH_IS_FILE)
	{
		if (!CanDelete(sPath)) 
		{
			m_bAborted = TRUE;
			return;
		}
		if (!DeleteFile(sPath)) 
			throw new CFExeption(GetLastError());
		return;
	}

	PreparePath(sPath);
	sPath += "*.*";

	BOOL bRes = ff.FindFile(sPath);
	while(bRes)
	{
		bRes = ff.FindNextFile();
		if (ff.IsDots()) 
			continue;
		if (ff.IsDirectory())
		{
			sPath = ff.GetFilePath();
			DoDelete(sPath);
		}
		else
			DoDelete(ff.GetFilePath());
	}
	ff.Close();

	if(bRemoveFolder)
		if(!RemoveDirectory(sPathName)&& !m_bAborted) 
			throw new CFExeption(GetLastError());		
}


void CESMMovieFileOperation::DoFolderCopy(CString sSourceFolder, CString sDestFolder, BOOL bDelteAfterCopy, BOOL bCreateFolder)
{
	CFileFind ff;
	CString sPathSource = sSourceFolder;
	BOOL bRes = ff.FindFile(sPathSource);
	while (bRes)
	{
		bRes = ff.FindNextFile();
		if (ff.IsDots()) continue;
		if (ff.IsDirectory()) // source is a folder
		{
			if (m_iRecursionLimit == 0) 
				continue;
			sPathSource = ff.GetFilePath() + CString("\\") + CString("*.*");
			CString sPathDest = sDestFolder + ff.GetFileName() + CString("\\");
			
			//-- 2008-07-25
			if(bCreateFolder)
			{
				if (CheckPath(sPathDest) == PATH_NOT_FOUND) 
				{
					if (!CreateDirectory(sPathDest, NULL))
					{
						ff.Close();
						throw new CFExeption(GetLastError());
					}
				}
			}

			if (m_iRecursionLimit > 0) 
				m_iRecursionLimit --;			
			DoFolderCopy(sPathSource, sPathDest, bDelteAfterCopy);
		}
		else // source is a file
		{
			CString sNewFileName = sDestFolder + ff.GetFileName();
			DoFileCopy(ff.GetFilePath(), sNewFileName, bDelteAfterCopy);
		}
	}
	ff.Close();
}


BOOL CESMMovieFileOperation::Delete(CString sPathName, BOOL bRemoveFolder)
{
	try
	{
		DoDelete(sPathName, bRemoveFolder);
	}
	catch(CFExeption* e)
	{
		m_sError = e->GetErrorText();
		m_dwError = e->GetErrorCode();
		delete e;
		e = NULL;
		if (m_dwError == 0) return TRUE;
		return FALSE;
	}
	return TRUE;
}


BOOL CESMMovieFileOperation::Rename(CString sSource, CString sDest)
{
	try
	{
		DoRename(sSource, sDest);
	}
	catch(CFExeption* e)
	{
		m_sError = e->GetErrorText();
		m_dwError = e->GetErrorCode();
		delete e;
		e = NULL;
		return FALSE;
	}
	return TRUE;
}


void CESMMovieFileOperation::DoRename(CString sSource, CString sDest)
{
	if (!MoveFile(sSource, sDest)) 
		throw new CFExeption(GetLastError());
}

void CESMMovieFileOperation::CreateFolder(CString strDestDir)
{
	CStringArray arParse;
	CString strNowDir, strCreateDir;
	strNowDir = strDestDir;
	DWORD dwAttr;

	while(1)
	{
		dwAttr = GetFileAttributes(strNowDir);
		if (dwAttr == 0xffffffff) 
		{
			CString sFolderName = strNowDir;
			int pos = sFolderName.ReverseFind('\\');
			if (pos != -1) 
			{
				sFolderName = sFolderName.Right(sFolderName.GetLength() - pos - 1);
				strNowDir .Delete(pos, strNowDir .GetLength() - pos);
			}			
			if( sFolderName == _T(""))
				break;

			arParse.Add(sFolderName);			
		}
		else
			break;
	}

	//-- CREATE NEW DIRECTORY
	int nAll = (int)arParse.GetCount ();
	while(nAll--)
	{
		strCreateDir = arParse.GetAt(nAll);
		strNowDir.Format(_T("%s\\%s"),strNowDir, strCreateDir);
		::CreateDirectory (strNowDir, NULL);
	}

	//-- 2012-05-22 hongsu
	nAll = (int)arParse.GetSize();
	while(nAll--)
		arParse.RemoveAt(nAll);
	arParse.RemoveAll();
}

void CESMMovieFileOperation::DoCopy(CString sSource, CString sDest, BOOL bDelteAfterCopy, BOOL bCreateFolder)
{
	CheckSelfRecursion(sSource, sDest);
	// source not found
	if (CheckPath(sSource) == PATH_NOT_FOUND)
	{
		CString sError = sSource + CString(" not found");
		throw new CFExeption(sError);
	}
	// dest not found
	if (CheckPath(sDest) == PATH_NOT_FOUND)
	{
		CString sError = sDest + CString(" not found");
		//-- 2008-06-19
		//throw new CFExeption(sError);
		//-- 2008-07-24
		
		//-- 2008-08-11
		//-- CHECK FILE OR FOLDER
		CString strTemp;
		strTemp = sDest;
		int nPath = strTemp.ReverseFind ('\\');
		int nFile = strTemp.ReverseFind ('.');
		if(nPath > nFile)
			CreateFolder(sDest);
	}
	// folder to file
	if (CheckPath(sSource) == PATH_IS_FOLDER && CheckPath(sDest) == PATH_IS_FILE) 
	{
		throw new CFExeption(_T("Wrong operation"));
	}
	// folder to folder
	if (CheckPath(sSource) == PATH_IS_FOLDER && CheckPath(sDest) == PATH_IS_FOLDER) 
	{
		CFileFind ff;
		CString sError = sSource + CString(" not found");
		PreparePath(sSource);
		PreparePath(sDest);
		sSource += "*.*";
		if (!ff.FindFile(sSource)) 
		{
			ff.Close();
			throw new CFExeption(sError);
		}
		if (!ff.FindNextFile()) 
		{
			ff.Close();
			throw new CFExeption(sError);
		}
		//-- 2008-07-25
		if(bCreateFolder)
		{
			CString sFolderName = ParseFolderName(sSource);		
			if (!sFolderName.IsEmpty()) // the source is not drive
			{
				sDest += sFolderName;
				PreparePath(sDest);
				if (!CreateDirectory(sDest, NULL))
				{
					DWORD dwErr = GetLastError();
					if (dwErr != 183)
					{
						ff.Close();
						throw new CFExeption(dwErr);
					}
				}
			}
		}

		ff.Close();
		DoFolderCopy(sSource, sDest, bDelteAfterCopy, bCreateFolder);
	}
	// file to file
	//int nTest =  CheckPath(sDest);
	//int nTest2 = CheckPath(sSource);

	if (CheckPath(sSource) == PATH_IS_FILE && 
		( CheckPath(sDest) == PATH_IS_FILE || CheckPath(sDest) == PATH_NOT_FOUND) ) 
	{
		DoFileCopy(sSource, sDest);
	}
	// file to folder
	if (CheckPath(sSource) == PATH_IS_FILE &&  CheckPath(sDest) == PATH_IS_FOLDER ) 
	{
		PreparePath(sDest);
		//char drive[MAX_PATH], dir[MAX_PATH], name[MAX_PATH], ext[MAX_PATH];
		//_splitpath(sSource, drive, dir, name, ext);
		TCHAR drive[MAX_PATH], dir[MAX_PATH], name[MAX_PATH], ext[MAX_PATH];
		_tsplitpath(sSource, drive, dir, name, ext);
		sDest = sDest + CString(name) + CString(ext);
		DoFileCopy(sSource, sDest);
	}
}


void CESMMovieFileOperation::DoFileCopy(CString sSourceFile, CString sDestFile, BOOL bDelteAfterCopy)
{
	BOOL bOvrwriteFails = FALSE;
	if (!m_bOverwriteMode)
	{
		while (IsFileExist(sDestFile)) 
		{
			sDestFile = ChangeFileName(sDestFile);
		}
		bOvrwriteFails = TRUE;
	}
	
	if (!CopyFile(sSourceFile, sDestFile, bOvrwriteFails)) 
		throw new CFExeption(GetLastError());

	if (bDelteAfterCopy)
	{
		DoDelete(sSourceFile);
	}
}


BOOL CESMMovieFileOperation::Copy(CString sSource, CString sDest, BOOL bCreateFolder)
{
	if (CheckSelfCopy(sSource, sDest)) 
		return TRUE;

	BOOL bRes;
	try
	{
		DoCopy(sSource, sDest, FALSE, bCreateFolder);
		bRes = TRUE;
	}
	catch(CFExeption* e)
	{
		m_sError = e->GetErrorText();
		m_dwError = e->GetErrorCode();
		delete e;
		e = NULL;
		if (m_dwError == 0) bRes = TRUE;
		bRes = FALSE;
	}
	m_iRecursionLimit = -1;
	return bRes;
}


BOOL CESMMovieFileOperation::Replace(CString sSource, CString sDest)
{
	if (CheckSelfCopy(sSource, sDest)) return TRUE;
	BOOL bRes;
	try
	{
		BOOL b = m_bAskIfReadOnly;
		m_bAskIfReadOnly = FALSE;
		DoCopy(sSource, sDest, TRUE);
		DoDelete(sSource);
		m_bAskIfReadOnly = b;
		bRes = TRUE;
	}
	catch(CFExeption* e)
	{
		m_sError = e->GetErrorText();
		m_dwError = e->GetErrorCode();
		delete e;
		e = NULL;
		if (m_dwError == 0) bRes = TRUE;
		bRes = FALSE;
	}
	m_iRecursionLimit = -1;
	return bRes;
}


CString CESMMovieFileOperation::ChangeFileName(CString sFileName)
{
	CString sName, sNewName, sResult;
	//char drive[MAX_PATH];
	//char dir  [MAX_PATH];
	//char name [MAX_PATH];
	//char ext  [MAX_PATH];
	TCHAR drive[MAX_PATH];
	TCHAR dir  [MAX_PATH];
	TCHAR name [MAX_PATH];
	TCHAR ext  [MAX_PATH];

	_tsplitpath((LPCTSTR)sFileName, drive, dir, name, ext);
	sName = name;

	int pos = sName.Find(_T("Copy "));
	if (pos == -1)
	{
		sNewName = CString("Copy of ") + sName + CString(ext);
	}
	else
	{
		int pos1 = sName.Find('(');
		if (pos1 == -1)
		{
			sNewName = sName;
			sNewName.Delete(0, 8);
			sNewName = CString("Copy (1) of ") + sNewName + CString(ext);
		}
		else
		{
			CString sCount;
			int pos2 = sName.Find(')');
			if (pos2 == -1)
			{
				sNewName = CString("Copy of ") + sNewName + CString(ext);
			}
			else
			{
				sCount = sName.Mid(pos1 + 1, pos2 - pos1 - 1);
				sName.Delete(0, pos2 + 5);
				int iCount = _ttoi((LPCTSTR)sCount);
				iCount ++;
				sNewName.Format(_T("Copy (%d) of "),iCount);
				sNewName.Append(sName);
				sNewName.Append(CString(ext));
			}
		}
	}

	sResult = CString(drive) + CString(dir) + sNewName;
	return sResult;
}


BOOL CESMMovieFileOperation::IsFileExist(CString sPathName)
{
	HANDLE hFile;
	hFile = CreateFile(sPathName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, NULL, NULL);
	if (hFile == INVALID_HANDLE_VALUE) 
		return FALSE;
	CloseHandle(hFile);
	return TRUE;
}

BOOL CESMMovieFileOperation::IsFileFolder(CString sPath)
{
	DWORD dwAttr = GetFileAttributes(sPath);
	if (dwAttr == 0xffffffff) 
		return FALSE;
	if (dwAttr & FILE_ATTRIBUTE_DIRECTORY)
		return TRUE;
	return FALSE;
}


int CESMMovieFileOperation::CheckPath(CString sPath)
{
	DWORD dwAttr = GetFileAttributes(sPath);
	if (dwAttr == 0xffffffff) 
	{
		if (GetLastError() == ERROR_FILE_NOT_FOUND || GetLastError() == ERROR_PATH_NOT_FOUND) 
			return PATH_NOT_FOUND;
		return PATH_ERROR;
	}
	if (dwAttr & FILE_ATTRIBUTE_DIRECTORY)
		return PATH_IS_FOLDER;
	return PATH_IS_FILE;
}


void CESMMovieFileOperation::PreparePath(CString &sPath)
{
	if(sPath.Right(1) != "\\") sPath += "\\";
}


BOOL CESMMovieFileOperation::CanDelete(CString sPathName)
{
	DWORD dwAttr = GetFileAttributes(sPathName);
	if (dwAttr == -1) 
		return FALSE;
	if (dwAttr & FILE_ATTRIBUTE_READONLY)
	{
		if (m_bAskIfReadOnly)
		{
			CString sTmp = sPathName;
			int pos = sTmp.ReverseFind('\\');
			if (pos != -1) sTmp.Delete(0, pos + 1);
			CString sText = sTmp + CString(" is read only. Do you want delete it?");
			int iRes = MessageBox(NULL, sText, _T("Warning"), MB_YESNOCANCEL | MB_ICONQUESTION);
			switch (iRes)
			{
				case IDYES:
				{
					if (!SetFileAttributes(sPathName, FILE_ATTRIBUTE_NORMAL)) return FALSE;
					return TRUE;
				}
				case IDNO:
				{
					return FALSE;
				}
				case IDCANCEL:
				{
					m_bAborted = TRUE;
					throw new CFExeption(0);
					return FALSE;
				}
			}
		}
		else
		{
			if (!SetFileAttributes(sPathName, FILE_ATTRIBUTE_NORMAL)) return FALSE;
			return TRUE;
		}
	}
	return TRUE;
}


CString CESMMovieFileOperation::ParseFolderName(CString sPathName)
{
	CString sFolderName = sPathName;
	int pos = sFolderName.ReverseFind('\\');
	if (pos != -1) 
		sFolderName.Delete(pos, sFolderName.GetLength() - pos);
	pos = sFolderName.ReverseFind('\\');
	if (pos != -1) 
		sFolderName = sFolderName.Right(sFolderName.GetLength() - pos - 1);
	else sFolderName.Empty();
	return sFolderName;
}


void CESMMovieFileOperation::CheckSelfRecursion(CString sSource, CString sDest)
{
	if (sDest.Find(sSource) != -1)
	{
		int i = 0, count1 = 0, count2 = 0;
		for(i = 0; i < sSource.GetLength(); i ++)	if (sSource[i] == '\\') count1 ++;
		for(i = 0; i < sDest.GetLength(); i ++)	if (sDest[i] == '\\') count2 ++;
		if (count2 >= count1) m_iRecursionLimit = count2 - count1;
	}
}


BOOL CESMMovieFileOperation::CheckSelfCopy(CString sSource, CString sDest)
{
	BOOL bRes = FALSE;
	if (CheckPath(sSource) == PATH_IS_FOLDER)
	{
		CString sTmp = sSource;
		int pos = sTmp.ReverseFind('\\');
		if (pos != -1)
		{
			sTmp.Delete(pos, sTmp.GetLength() - pos);
			if (sTmp.CompareNoCase(sDest) == 0) bRes = TRUE;
		}
	}
	return bRes;
}


//-- 2012-06-20 hongsu@esmlab.com
//-- Get File with include string
CString CESMMovieFileOperation::SearchFile(CString strFolder, CString strFile, CString strInclude)
{
	CString strFileName;

	CESMDirRead dr;
	//dr.Recurse() = true;		// scan subdirs ?
	dr.ClearDirs();				// start clean
	dr.GetDirs(strFolder, true);	// get all folders under c:\temp

	// get the dir array
	CESMDirRead::DirVector &dirs = dr.Dirs();

	dr.ClearFiles();        // start clean
	dr.GetFiles(strFile);   // get all *.JPG files in c:\temp and below
	// get the file array
	CESMDirRead::FileVector &files = dr.Files();   

	// dump it...
	CString strLine;
	for (CESMDirRead::FileVector::const_iterator fit = files.begin(); fit!=files.end(); fit++)
	{
		strFileName.Format(_T("%s"),fit->m_sName);

		//-- Search Line
		strLine = SearchLine(strFileName,strInclude);
		if(strLine.GetLength())
			break;
	}

	dirs.clear();
	files.clear();	

	return strFileName;
}

CString CESMMovieFileOperation::SearchLine(CString strFile, CString strInclude)
{
	CString strLine;
	CStdioFile file;

	if(!file.Open(strFile, CFile::modeRead))
		return _T("");

	while(file.ReadString(strLine) != NULL)
	{
		if(strLine.Find(strInclude) == 0)
			return strLine;
	}

	return _T("");
}

char* CESMMovieFileOperation::GetFileVersionInfo(CString strFullPath, CString strFileName)
{
	char* pszFileVersion = NULL;
	DWORD dwInfoSize = 0;
	//-- Is File Exists
	CString strFileFullPath = strFullPath + _T("\\") + strFileName;
	if(!PathFileExists(strFileFullPath))
		return NULL;

	dwInfoSize = GetFileVersionInfoSize(strFileFullPath, 0);
	if(!dwInfoSize)
		return NULL;

	pszFileVersion = new char[dwInfoSize];
	if(pszFileVersion)
	{
		if(::GetFileVersionInfo(strFileFullPath, 0, dwInfoSize, pszFileVersion) != 0)
		{
			VS_FIXEDFILEINFO* pFineInfo = NULL;
			UINT bufLen = 0;
			if(VerQueryValue(pszFileVersion, _T("\\"), (LPVOID*)&pFineInfo, &bufLen) !=0)
			{    
				WORD majorVer = 0, minorVer = 0, buildNum = 0, revisionNum = 0;
				majorVer	= HIWORD(pFineInfo->dwFileVersionMS);
				minorVer	= LOWORD(pFineInfo->dwFileVersionMS);
				buildNum	= HIWORD(pFineInfo->dwFileVersionLS);
				revisionNum = LOWORD(pFineInfo->dwFileVersionLS);
				delete[] pszFileVersion;
				pszFileVersion = NULL;
				
				//-- Return Value
				CString strTemp;
				strTemp.Format(_T("Ver:%d.%d.%d.%d"), majorVer, minorVer, buildNum, revisionNum);
				char* pszVerion = new char[strTemp.GetLength()+1];
				size_t CharactersConverted = 0;
				wcstombs_s(&CharactersConverted, pszVerion, strTemp.GetLength()+1, strTemp.GetBuffer(), _TRUNCATE);
				strTemp.ReleaseBuffer();

				return pszVerion;
			}
		}
	}
	if(pszFileVersion)
	{
		delete[] pszFileVersion;
		pszFileVersion = NULL;
	}
	return NULL;
}

CString CESMMovieFileOperation::GetPreviousFolder(CString strFolder)
{	
	int nReverse = strFolder.ReverseFind('\\');
	return strFolder.Left(nReverse);
}


//------------------------------------------------------------------------------
//! @brief		Get File Count in Folder
//! @date		2013-10-18
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//------------------------------------------------------------------------------
int CESMMovieFileOperation::GetFileCount(CString strFolder, CString strExtention)
{
	int nCount = 0;
	CESMDirRead dr;
	//dr.Recurse() = true;		// scan subdirs ?
	dr.ClearDirs();				// start clean
	dr.GetDirs(strFolder, true);	// get all folders under c:\temp

	// get the dir array
	CESMDirRead::DirVector &dirs = dr.Dirs();

	dr.ClearFiles();        // start clean
	dr.GetFiles(strExtention);   // get all *.JPG files in c:\temp and below
	// get the file array
	CESMDirRead::FileVector &files = dr.Files();   
	for (CESMDirRead::FileVector::const_iterator fit = files.begin(); fit!=files.end(); fit++)
		nCount++;

	dirs.clear();
	files.clear();	
	return nCount;
}
