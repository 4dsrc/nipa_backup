#pragma once
#include "ESMMovieData.h"

class ESMMovieArray
{
public:
	ESMMovieArray(void);
	~ESMMovieArray(void);

	void ClearMovie();
	void SetDscCount(int nDscCount);
	int SetMovieFile(CString strMovieId, CString strMoviePath);
	CString GetMovieFile(int nMovieIndex);
	int GetMovieCount(){ return m_nDscCount; } 
	ESMMovieData* GetMovieData(int nIndex);
	ESMMovieData* GetMovieData(CString strMovieID);
	stAdjustInfo GetAdjustData(int nIndex);
	void SetAdjustData(int nIndex, stAdjustInfo* AdjustData);

private:
	ESMMovieData** m_arrMovieData;
	int m_nDscCount;
};

