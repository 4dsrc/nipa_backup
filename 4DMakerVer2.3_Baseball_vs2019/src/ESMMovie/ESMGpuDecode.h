#pragma once

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#define WINDOWS_LEAN_AND_MEAN
#include <windows.h>
#include <windowsx.h>
#endif

// CUDA Header includes
#include "dynlink_nvcuvid.h"  // <nvcuvid.h>
#include "dynlink_cuda.h"     // <cuda.h>
#include "dynlink_cudaD3D11.h" // <cudaD3D11.h>
#include "dynlink_builtin_types.h"	  // <builtin_types.h>

// CUDA utilities and system includes
#include "helper_functions.h"
#include "helper_cuda_drvapi.h"

// cudaDecodeD3D11 related helper functions
#include "FrameQueue.h"
#include "VideoSource.h"
#include "VideoParser.h"
#include "VideoDecoder.h"
#include "ImageDX.h"

#include "cudaProcessFrame.h"
#include "cudaModuleMgr.h"

// Include files
#include <math.h>
#include <memory>
#include <iostream>
#include <cassert>

#include "ESMIndexStructure.h"
#include "ESMDefine.h"
#include "ESMMovieThreadManager.h"

class CESMGpuDecode
{
public:
	CESMGpuDecode(void);
	~CESMGpuDecode(void);


public:
	CString m_strWindowName;
	vector<ESMFrameArray*> m_pImgArr;
public:
	void initValue();

	BOOL GPUDecoding(CString strPath, int nStartIndex, int nEndIndex, int nType, vector<MakeFrameInfo>* pArrFrameInfo = NULL,int nFrameSize = 30,BOOL bIsLoad = FALSE);
	void renderVideoFrame(HWND hWnd, bool bUseInterop, int nType, vector<MakeFrameInfo>* pArrFrameInfo = NULL);
	bool copyDecodedFrameToTexture(unsigned int &nRepeats, int bUseInterop, int *pbIsProgressive, int nType, ThreadFrameData* pFrameData = NULL);

	BOOL GPUDecoding(CString strPath, int nIndex, vector<ESMFrameArray*> pArray, int nType, vector<MakeFrameInfo>* pArrFrameInfo = NULL);
	void renderVideoFrame(HWND hWnd, bool bUseInterop,vector<ESMFrameArray*> pImgArr, int nType, vector<MakeFrameInfo>* pArrFrameInfo = NULL);
	bool copyDecodedFrameToTexture(unsigned int &nRepeats, int bUseInterop, int *pbIsProgressive, vector<ESMFrameArray*> pImgArr, int nType, ThreadFrameData* pFrameData = NULL);

	void parseCommand(CString strPath);
	bool loadVideoSource(const char *video_file,unsigned int &width    , unsigned int &height,	unsigned int &dispWidth, unsigned int &dispHeight);
	bool initD3D11(HWND hWnd,int *pbTCC);
	HRESULT initCudaResources(int bUseInterop, int bTCC,BOOL bIsLoad = FALSE);
	BOOL initCudaVideo();
	HRESULT initD3D11Surface(unsigned int nWidth, unsigned int nHeight);
	
	
	void cudaPostProcessFrame(CUdeviceptr *ppDecodedFrame, size_t nDecodedPitch,	CUdeviceptr *ppTextureData,  size_t nTexturePitch,CUmodule cuModNV12toARGB,	CUfunction fpCudaKernel, CUstream streamID);
	void cudaPostProcessFrame(CUdeviceptr *ppDecodedFrame, size_t nDecodedPitch,	CUarray array,CUmodule cuModNV12toARGB,CUfunction fpCudaKernel, CUstream streamID);
	void SaveFrameAsYUV(unsigned char *pdst,const unsigned char *psrc,int width, int height, int pitch);
	HRESULT reinitCudaResources();
	HRESULT cleanup(bool bDestroyContext);
	void freeCudaResources(bool bDestroyContext);
	HRESULT CESMGpuDecode::drawScene(int field_num);
	void computeFPS(HWND hWnd, bool bUseInterop);
	void printStatistics();
	void yuv420_to_rgb( unsigned char *in, unsigned char *out, int w, int h );
	void yuv420_to_rgb( BYTE* in, ESMFrameArray* out, int w, int h );
	void imageAdjust(ThreadFrameData* pFrameData);
	static unsigned WINAPI  imageAdjust(LPVOID param);
	void YUV_lookup_table();
	
	void ConvertYUV2RGB(uint32 *srcImage,     size_t nSourcePitch, uint32 *dstImage,     size_t nDestPitch,	uint32 width,         uint32 height);
	void Passthru_drvapi(uint32 *srcImage,   size_t nSourcePitch,	uint32 *dstImage,   size_t nDestPitch,	uint32 width,       uint32 height);
	uint32 RGBAPACK_10bit(float red, float green, float blue, uint32 alpha);
	uint32 RGBAPACK_8bit(float red, float green, float blue, uint32 alpha);
	void YUV2RGB(uint32 *yuvi, float *red, float *green, float *blue);

	static unsigned WINAPI  ConvertColor(LPVOID param);
};