#pragma once
#include <afx.h>
#include <WinSock2.h>
#include <process.h>
#include "define.h"
#include "RTSPServer_ImageSender.h"
#include "RTSPServer_SessionMgr.h"
#include "DllFunc.h"
class CRTSPImageStreamer
{
public:
	CRTSPImageStreamer(SOCKET sock,CRTSPServerSessionMgr* pSessionMgr,std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*> mpImageSender,BOOL bMulticast = FALSE);
	CRTSPImageStreamer(CRTSPServerImageSender* pImageSender,RTSP_PLAY_METHOD method);
	~CRTSPImageStreamer();
	
	RTSP_PLAY_METHOD GetRTSPPlayMethod(RTSP_PLAY_METHOD method){return m_RTSPPlayMethod;}
	void SetRTSPPlayMethod(RTSP_PLAY_METHOD method){m_RTSPPlayMethod = method;}

	void LaunchImageStreamer();
	static unsigned WINAPI _ImageStreamer(LPVOID param);

	int SendPacket(CRTSPServerImageSender*pSender,int nIdx,int*pSecIdx,int*pFrameIdx);
	
	BOOL GetThreadStop(){return m_bThreadStop;}
	void SetThreadStop(BOOL b){m_bThreadStop = b;}

	static unsigned WINAPI _ImageSender(LPVOID param);

	void SetThreadRun(BOOL b){m_bThreadRun = b;}
	BOOL GetThreadRun(){return m_bThreadRun;}

	FILE* m_pFile;

	void LaunchMulticastStreamer();
	static unsigned WINAPI _MulticastStreamer(LPVOID param);
	BOOL GetUseMulticast(){return m_bMulticast;}
	void SetUseMulticast(BOOL b){m_bMulticast = b;}

private:
	std::vector<RTSP_PACKETIZE_INFO*>m_ArrSendArr;
	BOOL m_bThreadStop;
	RTSP_PLAY_METHOD m_RTSPPlayMethod;
	CRTSPServerSessionMgr* m_pSessionMgr;
	SOCKET m_ClinetSock;
	std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*> m_ArrImageSender;
	CRTSPServerImageSender* m_pImageSender;
	//CRITICAL_SECTION m_criSend;

	std::vector<int>m_ArrIndex;
	BOOL m_bThreadRun;
	void SetWait(DWORD dwMillisec);
	BOOL m_bMulticast;
	/*Inline Func*/
public:
#if 0
	void AddSendData(int nIdx)
	{
		EnterCriticalSection(&m_criSend);
		m_ArrIndex.push_back(nIdx);
		LeaveCriticalSection(&m_criSend);
	}
	int GetSendData()
	{
		int n;
		EnterCriticalSection(&m_criSend);
		n = m_ArrIndex.at(0);
		LeaveCriticalSection(&m_criSend);
		return n;
	}
	int GetSendDataSize()
	{
		int nSize = 0;
		EnterCriticalSection(&m_criSend);
		nSize = m_ArrIndex.size();
		LeaveCriticalSection(&m_criSend);
		return nSize;
	}
	void DeleteSendData()
	{
		EnterCriticalSection(&m_criSend);
		m_ArrIndex.erase(m_ArrIndex.begin());
		LeaveCriticalSection(&m_criSend);
	}
#else
	int SendMulticastPacket(CRTSPServerImageSender*pSender,int nIdx,int*pSecIdx,int*pFrameIdx);
	CRITICAL_SECTION m_criMulticast;
	vector<RTSP_SEND_INFO>m_stMultiSendInfo;
	void AddMultiSendInfo(RTSP_SEND_INFO info)
	{
		EnterCriticalSection(&m_criMulticast);
		m_stMultiSendInfo.push_back(info);
		LeaveCriticalSection(&m_criMulticast);
	}
	RTSP_SEND_INFO GetMultiSendInfo()
	{
		RTSP_SEND_INFO info;
		EnterCriticalSection(&m_criMulticast);
		info = m_stMultiSendInfo.at(0);
		LeaveCriticalSection(&m_criMulticast);

		return info;
	}
	void DeleteMultiSendInfo()
	{
		EnterCriticalSection(&m_criMulticast);
		m_stMultiSendInfo.erase(m_stMultiSendInfo.begin());
		LeaveCriticalSection(&m_criMulticast);
	}
	int GetMultiSendCount()
	{
		int nCount = 0;
		EnterCriticalSection(&m_criMulticast);
		nCount = m_stMultiSendInfo.size();
		LeaveCriticalSection(&m_criMulticast);
		
		return nCount;
	}
	
	/*void AddSendData(RTSP_PACKETIZE_INFO* pak)
	{
		EnterCriticalSection(&m_criSend);
		m_ArrSendArr.push_back(pak);
		LeaveCriticalSection(&m_criSend);
	}
	RTSP_PACKETIZE_INFO* GetSendData()
	{
		RTSP_PACKETIZE_INFO* pak;
		EnterCriticalSection(&m_criSend);
		pak = m_ArrSendArr.at(0);
		LeaveCriticalSection(&m_criSend);
		return pak;
	}
	int GetSendDataSize()
	{
		int nSize = 0;
		EnterCriticalSection(&m_criSend);
		nSize = m_ArrSendArr.size();
		LeaveCriticalSection(&m_criSend);
		return nSize;
	}
	void DeleteSendData()
	{
		EnterCriticalSection(&m_criSend);
		m_ArrSendArr.erase(m_ArrSendArr.begin());
		LeaveCriticalSection(&m_criSend);
	}*/
#endif
};
struct RTSP_MULTICAST_STREAM
{
	CRTSPImageStreamer* pStreamer;
	CRTSPServerImageSender *pSender;
};