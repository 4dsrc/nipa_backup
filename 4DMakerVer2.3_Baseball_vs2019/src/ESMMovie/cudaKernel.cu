#include "cudaKernel.cuh"
#include "cuda.h"

__constant__ double cRotMat[6];
#define LINEAR_VALUE(I1,I2,I3,I4,sr,sc) (I4 * sc * sr) + (I3 * (1-sc) * sr) + (I2 * sc * (1-sr)) + (I1 * (1-sc) * (1-sr))
#define CUBIC_VALUE(v1,v2,v3,v4,d) v2 + d*((-v1 + v3) + d*((2*(v1 - v2)  + v3 - v4)+ d *(-v1 + v2 - v3 + v4)))
__device__ float2 calcCoord1(int x, int y,double MovX, double MovY)
{
	const float xcoo = cRotMat[0] * x + cRotMat[1] * y + cRotMat[2];
	const float ycoo = cRotMat[3] * x + cRotMat[4] * y + cRotMat[5];
	
	float2 t;
	t.x = xcoo - MovX;
	t.y = ycoo - MovY;
	return t;
}
__device__ float2 calcCoord(int x, int y)
{
	const float xcoo = cRotMat[0] * x + cRotMat[1] * y + cRotMat[2];
	const float ycoo = cRotMat[3] * x + cRotMat[4] * y + cRotMat[5];
	
	float2 t;
	t.x = (xcoo);
	t.y = (ycoo);
	return t;
}
__device__ double cubic_interpolation(double v1,double v2,double v3, double v4, double d)
{
	double v, p1, p2, p3, p4;
	p1 = v2;
	p2 = -v1 + v3;
	p3 = 2*(v1 - v2)  + v3 - v4;
	p4 = -v1 + v2 - v3 + v4;

	v = p1 + d*(p2 + d*(p3 + d*p4));
	if(v > 255) v = 255;
	else if(v < 0) v = 0;
	return v;
}
__global__ void fnRotMov(Mat img,Mat result,int nWidth,int nHeight,double MovX,double MovY)
{
	int i = blockDim.y * blockIdx.y + threadIdx.y;
	int j = blockDim.x * blockIdx.x + threadIdx.x;

	int nWhere;
	int x1, y1, x2, y2, x3, y3, x4, y4; 
	double v1, v2, v3, v4;
	double rx, ry, p, q;
	double nValue;
	
	float2 coo;
		coo = calcCoord(j,i);

	double sr,sc;

	int nX =floor(coo.x);
	int nY =floor(coo.y);

	sr = coo.y - nY;
	sc = coo.x - nX;

	if(i < nHeight && j < nWidth)
	{
		if(nX > 0 && nX < nWidth && nY > 0 && nY <nHeight)
		{
			for(int x = 0 ; x < 3; x++)
			{
				double I1 = (double) img.data[nWidth* (nY*3)+    (nX*3) + x ]  ;
				double I2 = (double) img.data[nWidth* (nY*3)+   ((nX+1)*3)+ x] ;
				double I3 = (double) img.data[nWidth*((nY+1)*3)+ (nX*3)+ x]    ;
				double I4 = (double) img.data[nWidth*((nY+1)*3)+((nX+1)*3)+ x] ;

				double nValue = LINEAR_VALUE(I1,I2,I3,I4,sr,sc);

				nWhere = nWidth * (i*3) + (j*3) + x;
				result.data[nWhere] = nValue;
			}
		}
	}
}
__global__ void fnInterCubic(Mat img,Mat Result,int oW,int oH,int nW,int nH, double fx, double fy)
{
	int i = blockDim.y * blockIdx.y + threadIdx.y;
	int j = blockDim.x * blockIdx.x + threadIdx.x;


	int x1, y1, x2, y2, x3, y3, x4, y4; 
	double v1_B, v2_B, v3_B, v4_B;

	double rx, ry, p, q;
	double B =0 ,G =0 ,R= 0;

	if(i<nH && j < nW)
	{
		rx = (double)fx * j;
		ry = (double)fy * i;
		x2 = (int)rx; // 정수부분.
		y2 = (int)ry;
		x1 = x2 - 1; if(x1 < 0) x1 = 0;			else if(x1 >= oW) x1 = oW-1;
		y1 = y2 - 1; if(y1 < 0) y1 = 0;			else if(y1 >= oH) y1 = oH-1;
		x3 = x2 + 1; if(x3 >= oW) x3 = oW-1;	else if(x3 < 0) x1 = 0;
		y3 = y2 + 1; if(y3 >= oH) y3 = oH-1;	else if(y3 < 0) y1 = 0;
		x4 = x2 + 2; if(x4 >= oW) x4 = oW-1;	else if(x4 < 0) x1 = 0;
		y4 = y2 + 2; if(y4 >= oH) y4 = oH-1;	else if(y4 < 0) y1 = 0;

		p = rx - x2;
		q = ry - y2;

		for(int n=0;n<3;n++)
		{
			v1_B = cubic_interpolation((double)img.data[oW*(y1*3)+(x1*3)+n], (double)img.data[oW*(y1*3)+(x2*3)+n], (double)img.data[oW*(y1*3)+(x3*3)+n], (double)img.data[oW*(y1*3)+(x4*3)+n], p);//, a);
			v2_B = cubic_interpolation((double)img.data[oW*(y2*3)+(x1*3)+n], (double)img.data[oW*(y2*3)+(x2*3)+n], (double)img.data[oW*(y2*3)+(x3*3)+n], (double)img.data[oW*(y2*3)+(x4*3)+n], p);//, a);
			v3_B = cubic_interpolation((double)img.data[oW*(y3*3)+(x1*3)+n], (double)img.data[oW*(y3*3)+(x2*3)+n], (double)img.data[oW*(y3*3)+(x3*3)+n], (double)img.data[oW*(y3*3)+(x4*3)+n], p);//, a);
			v4_B = cubic_interpolation((double)img.data[oW*(y4*3)+(x1*3)+n], (double)img.data[oW*(y4*3)+(x2*3)+n], (double)img.data[oW*(y4*3)+(x3*3)+n], (double)img.data[oW*(y4*3)+(x4*3)+n], p);//, a);
			B	 = cubic_interpolation(v1_B, v2_B, v3_B, v4_B, q );//, a);

			Result.data[nW*(i*3)+(j*3)+n]   = B;			
		}
	}

}
__global__ void fnAdjustAll(Mat img,Mat Result,int oW,int oH,int nW,int nH)
{
	int i = blockDim.y * blockIdx.y + threadIdx.y;
	int j = blockDim.x * blockIdx.x + threadIdx.x;

	int x1, y1, x2, y2, x3, y3, x4, y4; 
	double v1_B, v2_B, v3_B, v4_B;

	double rx, ry, p, q;
	double B =0;
	float2 axis;
	
#if 0
	axis = calcCoord_scale(j,i,fx,fy);
#else
	axis = calcCoord(j,i);
#endif
	double sr,sc;
	
	if(i < nH && j < nW)
	{
		if(axis.x > 0 && axis.x < nW && axis.y > 0 && axis.y <nH)
		{
#if 1
			int nX = (int)floor(axis.x);
			int nY = (int)floor(axis.y);

			sr = axis.y - nY;
			sc = axis.x - nX;
			for(int x = 0 ; x < 3; x++)
			{
				double I1 = (double) img.data[oW*(nY*3)     + (nX*3)     + x ];
				double I2 = (double) img.data[oW*(nY*3)     + ((nX+1)*3) + x ];
				double I3 = (double) img.data[oW*((nY+1)*3) + (nX*3)      + x ];
				double I4 = (double) img.data[oW*((nY+1)*3) + ((nX+1)*3)  + x ];

				double nValue = LINEAR_VALUE(I1,I2,I3,I4,sr,sc);

				Result.data[nW*(i*3)+(j*3)+x] = (int)nValue;
			}
#else 
			rx = axis.x;
			ry = axis.y;

			x2 = (int)rx;
			y2 = (int)ry;

			x1 = x2 - 1; if(x1 < 0) x1 = 0;				else if(x1 >= oW) x1 = oW-1;
			y1 = y2 - 1; if(y1 < 0) y1 = 0;				else if(y1 >= oH) y1 = oH-1;

			x3 = x2 + 1; if(x3 >= oW) x3 = oW-1;	else if(x3 < 0) x1 = 0;
			y3 = y2 + 1; if(y3 >= oH) y3 = oH-1;	else if(y3 < 0) y1 = 0;

			x4 = x2 + 2; if(x4 >= oW) x4 = oW-1;	else if(x4 < 0) x1 = 0;
			y4 = y2 + 2; if(y4 >= oH) y4 = oH-1;	else if(y4 < 0) y1 = 0;

			p = rx - x2;
			q = ry - y2;

			for(int n = 0; n < 3; n++)
			{
				v1_B = cubic_interpolation((double)img.data[oW*(y1*3)+(x1*3)+n], (double)img.data[oW*(y1*3)+(x2*3)+n], (double)img.data[oW*(y1*3)+(x3*3)+n], (double)img.data[oW*(y1*3)+(x4*3)+n], p);
				v2_B = cubic_interpolation((double)img.data[oW*(y2*3)+(x1*3)+n], (double)img.data[oW*(y2*3)+(x2*3)+n], (double)img.data[oW*(y2*3)+(x3*3)+n], (double)img.data[oW*(y2*3)+(x4*3)+n], p);
				v3_B = cubic_interpolation((double)img.data[oW*(y3*3)+(x1*3)+n], (double)img.data[oW*(y3*3)+(x2*3)+n], (double)img.data[oW*(y3*3)+(x3*3)+n], (double)img.data[oW*(y3*3)+(x4*3)+n], p);
				v4_B = cubic_interpolation((double)img.data[oW*(y4*3)+(x1*3)+n], (double)img.data[oW*(y4*3)+(x2*3)+n], (double)img.data[oW*(y4*3)+(x3*3)+n], (double)img.data[oW*(y4*3)+(x4*3)+n], p);
				B	 = cubic_interpolation(v1_B, v2_B, v3_B, v4_B, q );//, a);

				Result.data[nW*(i*3)+(j*3)+n] = (int)B;
			}
#endif
		}
	}
}
void CCudaFunc::cuMatMalloc(Mat img,int nSize)
{
	cudaMalloc(&img.data,nSize);
	cuMatFree(img);
}
void CCudaFunc::cuMatFree(Mat img)
{
	cudaFree(img.data);
}
void CCudaFunc::cuSetProperty(int h,int w,int nH,int nW,double theta, double RotX, double RotY,double dScale,double MovX,double MovY)
{
	nWidth = w;
	nHeight = h;
	nImageSize = w*h*3;
	
	nReWidth = nW;
	nReHeight = nH;
	nReImageSize = nW*nH*3;
	
	fx = (double) w / (double) nW;
	fy = (double) h / (double) nH;

	double nCos = cos(theta) / dScale;
	double nSin = sin(theta) / dScale;
#if 1
	MovX /= dScale;
	MovY /= dScale;
	double hRotMat[6]
	= {  nCos*fx, nSin*fx,   (1-nCos)*RotX - nSin*RotY - MovX,
		-nSin*fy, nCos*fy, nSin*RotX + (1-nCos)*RotY - MovY	};
#else
	double hRotMat[6]
	= {nCos, nSin, (1-nCos)*RotX - nSin*RotY,
		-nSin, nCos, nSin*RotX + (1-nCos)*RotY	};
#endif
	cudaMemcpyToSymbol(cRotMat,hRotMat,6*sizeof(double));
	cuSetImageMemory(h,w,nH,nW);
}
void CCudaFunc::cuSetImageMemory(int h,int w,int nH,int nW)
{
	cuThread.x = 32;
	cuThread.y = 32;
	
	cuBlock.x = divUp(w,cuThread.x);
	cuBlock.y = divUp(h,cuThread.y);

	cuReThread.x = 32;
	cuReThread.y = 32;
		
	cuReBlock.x = divUp(nW,cuThread.x);
	cuReBlock.y = divUp(nH,cuThread.y);

	cudaMalloc(&cuImg.data,nImageSize);
	cudaMalloc(&cuRotate.data,nImageSize);
	cudaMalloc(&cuResize.data,nReImageSize);
}
int CCudaFunc::divUp(int total, int grain)
{
	return (total + grain - 1) / grain;
}
void CCudaFunc::cuFreeProperty()
{
	cudaFree(cuImg.data);
	cudaFree(cuRotate.data);
	cudaFree(cuResize.data);
}
void CCudaFunc::cuDipAll(Mat Frame,double MovX,double MovY,Mat Resize)
{
	cudaMemcpy(cuImg.data,Frame.data,nImageSize,cudaMemcpyHostToDevice);
	fnRotMov<<<cuBlock,cuThread>>>(cuImg,cuRotate,nWidth,nHeight,MovX,MovY);
	fnInterCubic<<<cuReBlock,cuReThread>>>(cuRotate,cuResize,nWidth,nHeight,nReWidth,nReHeight,fx,fy);
	cudaMemcpy(Resize.data,cuResize.data,nReImageSize,cudaMemcpyDeviceToHost);
}
void CCudaFunc::cuDipAll(Mat Frame,Mat Resize)
{
	cudaMemcpy(cuImg.data,Frame.data,nImageSize,cudaMemcpyHostToDevice);
	fnAdjustAll<<<cuReBlock,cuReThread>>>(cuImg,cuResize,nWidth,nHeight,nReWidth,nReHeight);
	cudaMemcpy(Resize.data,cuResize.data,nReImageSize,cudaMemcpyDeviceToHost);
}
/*
__constant__ double cRotMat[6];
__device__ float2 calcCoord(int x, int y)
{
	const float xcoo = cRotMat[0] * x + cRotMat[1] * y + cRotMat[2];
	const float ycoo = cRotMat[3] * x + cRotMat[4] * y + cRotMat[5];
	
	float2 t;
	t.x = xcoo;
	t.y = ycoo;
	return t;
}
__device__ double cubic_interpolation(double v1,double v2,double v3, double v4, double d)
{
	double v, p1, p2, p3, p4;
	p1 = v2;
	p2 = -v1 + v3;
	p3 = 2*(v1 - v2)  + v3 - v4;
	p4 = -v1 + v2 - v3 + v4;

	v = p1 + d*(p2 + d*(p3 + d*p4));
	if(v > 255) v = 255;
	else if(v < 0) v = 0;
	return v;
}
__global__ void cuTest(Mat img,Mat result,int h,int w)
{
	int y = blockDim.y * blockIdx.y + threadIdx.y;
	int x = blockDim.x * blockIdx.x + threadIdx.x;

	if(x<w && y<h)
	{
		result.data[w*(y*3)+(x*3)]	 = 255- img.data[w*(y*3)+(x*3)];
		result.data[w*(y*3)+(x*3)+1] = 255- img.data[w*(y*3)+(x*3)+1];
		result.data[w*(y*3)+(x*3)+2] = 255- img.data[w*(y*3)+(x*3)+2];
	}
}
__global__ void cuInterNN(Mat img,int w,int h)
{
	int i = blockDim.y * blockIdx.y + threadIdx.y;
	int j = blockDim.x * blockIdx.x + threadIdx.x;
	
	if(i>0 && i<h-1 && j>0 && j<w-1)
	{
		int left_B  = img.data[w*(i*3)+(j*3)-3];
		int right_B = img.data[w*(i*3)+(j*3)+3];
		int left_G  = img.data[w*(i*3)+(j*3)-2];
		int right_G = img.data[w*(i*3)+(j*3)+4];
		int left_R  = img.data[w*(i*3)+(j*3)-1];
		int right_R = img.data[w*(i*3)+(j*3)+5];

		if(left_B != 0 && right_B != 0 && img.data[w*(i*3)+(j*3)+0]==0)
		{
			img.data[w*(i*3)+(j*3)+0] = (left_B+right_B)/2;
		}

		if(left_G != 0 && right_G != 0 && img.data[w*(i*3)+(j*3)+1]==0)
		{
			img.data[w*(i*3)+(j*3)+1] = (left_G+right_G)/2;
		}

		if(left_R != 0 && right_R != 0 && img.data[w*(i*3)+(j*3)+2]==0)
		{
			img.data[w*(i*3)+(j*3)+2] = (left_R+right_R)/2;
		}
	}
}
__global__ void cuFRotate(Mat img, Mat result, double theta, int center_x, int center_y, int width, int height)
{
	int y = blockDim.y * blockIdx.y + threadIdx.y;
	int x = blockDim.x * blockIdx.x + threadIdx.x;
	double new_x;
	double new_y;
	int nX;
	int nY;
	if(x < width && y < height)
	{
		if(theta == 0)
		{
			new_x = x;
			new_y = y;
			nX = x;
			nY = y;
		}
		else
		{
			new_x = (double) (center_x + ((double)y - center_y)*sin(theta) + ((double) x - center_x) * cos(theta));
			new_y = (double) (center_y + ((double)y - center_y)*cos(theta) - ((double) x - center_x) * sin(theta));
			nX = (int) (new_x + 0.5);
			nY = (int) (new_y + 0.5);
		}

		if(nY >= 0 && nY < height && nX >= 0 && nX < width)
		{			
			double nidx = width * (nY*3)+(nX*3);
			double oidx = width * (y*3)+(x*3);
			result.data[(int)oidx  ] = img.data[(int)nidx  ];
			result.data[(int)oidx+1] = img.data[(int)nidx+1];
			result.data[(int)oidx+2] = img.data[(int)nidx+2];
		}
		else
		{
			double idx = width * (nY*3)+(nX*3);
			result.data[(int)idx  ] = 0;
			result.data[(int)idx+1] = 0;
			result.data[(int)idx+2] = 0;
		}
	}
}
__global__ void cuMoveImage(Mat img,Mat result,int nWidth,int nHeight,int nTop,int nBottom,int nLeft,int nRight)
{
	int i = blockDim.y * blockIdx.y + threadIdx.y;
	int j = blockDim.x * blockIdx.x + threadIdx.x; 

	if(nTop <= i && i < nHeight-nBottom && nLeft <= j && j < nWidth-nRight)
	{
		result.data[nWidth * (i*3) + (j*3)]   = img.data[nWidth * (i*3) + (j*3)];
		result.data[nWidth * (i*3) + (j*3)+1] = img.data[nWidth * (i*3) + (j*3)+1];
		result.data[nWidth * (i*3) + (j*3)+2] = img.data[nWidth * (i*3) + (j*3)+2];
	}
}
__global__ void cuInterCubic(Mat img,Mat Result,int oW,int oH,int nW,int nH)
{
	int i = blockDim.y * blockIdx.y + threadIdx.y;
	int j = blockDim.x * blockIdx.x + threadIdx.x;

	int x1, y1, x2, y2, x3, y3, x4, y4; 
	double v1_B, v2_B, v3_B, v4_B;
	double v1_G, v2_G, v3_G, v4_G;
	double v1_R, v2_R, v3_R, v4_R;

	double rx, ry, p, q;
	double B =0 ,G =0 ,R= 0;

	rx = (double)oW*j/nW;
	ry = (double)oH*i/nH;
	x2 = (int)rx; // 정수부분.
	y2 = (int)ry;
	x1 = x2 - 1; if(x1 < 0) x1 = 0;			else if(x1 >= oW) x1 = oW-1;
	y1 = y2 - 1; if(y1 < 0) y1 = 0;			else if(y1 >= oH) y1 = oH-1;
	x3 = x2 + 1; if(x3 >= oW) x3 = oW-1;	else if(x3 < 0) x1 = 0;
	y3 = y2 + 1; if(y3 >= oH) y3 = oH-1;	else if(y3 < 0) y1 = 0;
	x4 = x2 + 2; if(x4 >= oW) x4 = oW-1;	else if(x4 < 0) x1 = 0;
	y4 = y2 + 2; if(y4 >= oH) y4 = oH-1;	else if(y4 < 0) y1 = 0;

	p = rx - x2;
	q = ry - y2;

	for(int n=0;n<3;n++)
	{
		v1_B = cubic_interpolation((double)img.data[oW*(y1*3)+(x1*3)+n], (double)img.data[oW*(y1*3)+(x2*3)+n], (double)img.data[oW*(y1*3)+(x3*3)+n], (double)img.data[oW*(y1*3)+(x4*3)+n], p);//, a);
		v2_B = cubic_interpolation((double)img.data[oW*(y2*3)+(x1*3)+n], (double)img.data[oW*(y2*3)+(x2*3)+n], (double)img.data[oW*(y2*3)+(x3*3)+n], (double)img.data[oW*(y2*3)+(x4*3)+n], p);//, a);
		v3_B = cubic_interpolation((double)img.data[oW*(y3*3)+(x1*3)+n], (double)img.data[oW*(y3*3)+(x2*3)+n], (double)img.data[oW*(y3*3)+(x3*3)+n], (double)img.data[oW*(y3*3)+(x4*3)+n], p);//, a);
		v4_B = cubic_interpolation((double)img.data[oW*(y4*3)+(x1*3)+n], (double)img.data[oW*(y4*3)+(x2*3)+n], (double)img.data[oW*(y4*3)+(x3*3)+n], (double)img.data[oW*(y4*3)+(x4*3)+n], p);//, a);
		B	 = cubic_interpolation(v1_B, v2_B, v3_B, v4_B, q );//, a);

		if(i<nH && j < nW)
		{
			Result.data[nW*(i*3)+(j*3)+n]   = B;			
		}
	}
}
__global__ void fnRotate2(Mat img,Mat result,int nWidth,int nHeight)
{
	int i = blockDim.y * blockIdx.y + threadIdx.y;
	int j = blockDim.x * blockIdx.x + threadIdx.x;

	int nWhere;
	int x1, y1, x2, y2, x3, y3, x4, y4; 
	double v1, v2, v3, v4;
	double rx, ry, p, q;
	double nValue;

	float2 coo;
	coo = calcCoord(j,i);

	double sr,sc;

	if(i < nHeight && j < nWidth)
	{
		if(coo.x > -1 && coo.x < nWidth && coo.y > -1 && coo.y <nHeight)
		{
			int nX = (int) floor(coo.x);
			int nY = (int) floor(coo.y);
			sr = coo.y - nY;
			sc = coo.x - nX;


			for(int x = 0 ; x < 3; x++)
			{
				double I1 = (double) img.data[nWidth* (nY*3)+    (nX*3) + x ]  ;
				double I2 = (double) img.data[nWidth* (nY*3)+   ((nX+1)*3)+ x] ;
				double I3 = (double) img.data[nWidth*((nY+1)*3)+ (nX*3)+ x]    ;
				double I4 = (double) img.data[nWidth*((nY+1)*3)+((nX+1)*3)+ x] ;

				double nValue = (I4 * sc * sr) + (I3 * (1-sc) * sr) + (I2 * sc * (1-sr)) + (I1 * (1-sc) * (1-sr));

				nWhere = nWidth * (i*3) + (j*3) + x;
				result.data[nWhere] = nValue;
			}
		}
	}
//	int i = blockDim.y * blockIdx.y + threadIdx.y;
//	int j = blockDim.x * blockIdx.x + threadIdx.x;
//
//	int nWhere;
//	int x1, y1, x2, y2, x3, y3, x4, y4; 
//	double v1, v2, v3, v4;
//	double rx, ry, p, q;
//	double nValue;
//	
//	float2 coo;
//	coo = calcCoord(j,i);
//
//	double sr,sc;
//
//	if(i < nHeight && j < nWidth)
//	{
//		if(coo.x < 0 || coo.x > nWidth-1 || coo.y < 0 || coo.y >nHeight-1)
//		{
//			for(int x = 0; x < 3 ; x++)
//			{
//				nWhere = nWidth * (i*3) + (j*3) + x;
//				result.data[nWhere] = 255;
//			}
//		}
//		else
//		{	
//#if 1
//			int nX = (int) floor(coo.x+0.5);
//			int nY = (int) floor(coo.y+0.5);
//
//			for(int x = 0 ; x < 3; x++)
//			{
//				double I1 = img.data[nWidth* (nY*3)+    (nX*3) + x ];
//				double I2 = img.data[nWidth* (nY*3)+   ((nX+1)*3)+ x];
//				double I3 = img.data[nWidth*((nY+1)*3)+ (nX*3)+ x];
//				double I4 = img.data[nWidth*((nY+1)*3)+((nX+1)*3)+ x];
//				double nValue = (I1 * (1-sc) * (1-sr)) + (I2 * sc * (1-sr))
// 					+ (I3 * sc * sr) + (I4 * (1-sc) * sr);
//				if(nValue > 255) nValue = 255;
//				if(nValue < 0)   nValue = 0;
//
//				nWhere = nWidth * (i*3) + (j*3)+x;
//				result.data[nWhere] = nValue;
//			}
//#else
//			rx = coo.x* nWidth / nWidth;
//			ry = coo.y* nHeight / nHeight;
//
//			x2 = (int)rx; // 정수부분.
//			y2 = (int)ry;
//			x1 = x2 - 1; if(x1 < 0) x1 = 0;
//			y1 = y2 - 1; if(y1 < 0) y1 = 0;
//			x3 = x2 + 1; if(x3 >= nWidth) x3 = nWidth-1;
//			y3 = y2 + 1; if(y3 >= nHeight) y3 = nHeight-1;
//			x4 = x2 + 2; if(x4 >= nWidth) x4 = nWidth-1;
//			y4 = y2 + 2; if(y4 >= nHeight) y4 = nHeight-1;
//			p = rx - x2; // 실수부분.
//			q = ry - y2;
//			
//			for(int x = 0 ; x < 3; x++)
//			{
//				v1 = cubic_interpolation((double)img.data[nWidth*(y1*3)+(x1*3)+x], (double)img.data[nWidth*(y1*3)+(x2*3)+x], (double)img.data[nWidth*(y1*3)+(x3*3)+x], (double)img.data[nWidth*(y1*3)+(x4*3)+x], p);
//				v2 = cubic_interpolation((double)img.data[nWidth*(y2*3)+(x1*3)+x], (double)img.data[nWidth*(y2*3)+(x2*3)+x], (double)img.data[nWidth*(y2*3)+(x3*3)+x], (double)img.data[nWidth*(y2*3)+(x4*3)+x], p);
//				v3 = cubic_interpolation((double)img.data[nWidth*(y3*3)+(x1*3)+x], (double)img.data[nWidth*(y3*3)+(x2*3)+x], (double)img.data[nWidth*(y3*3)+(x3*3)+x], (double)img.data[nWidth*(y3*3)+(x4*3)+x], p);
//				v4 = cubic_interpolation((double)img.data[nWidth*(y4*3)+(x1*3)+x], (double)img.data[nWidth*(y4*3)+(x2*3)+x], (double)img.data[nWidth*(y4*3)+(x3*3)+x], (double)img.data[nWidth*(y4*3)+(x4*3)+x], p);
//
//				double nValue = cubic_interpolation(v1, v2, v3, v4, q);
//				if(nValue > 255) nValue = 255;
//				if(nValue < 0)   nValue = 0;
//				nWhere = nWidth * (i*3) + (j*3)+x;
//				result.data[nWhere] = nValue;
//			}
//#endif
//		}
//	}
}
*/