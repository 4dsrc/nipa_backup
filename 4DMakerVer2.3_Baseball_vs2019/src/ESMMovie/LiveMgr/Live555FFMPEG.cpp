//==========================================================================
// Live555FFMPEG.cpp : Defines the entry point for the console application.
//==========================================================================
// Main Example for Live555 and FFMPEG
//
//==========================================================================

#include "stdafx.h"
#include "FFMPEGClass.h"
#include "libavcodec\avcodec.h"
#include "opencv2\core.hpp"
#include "opencv2\highgui.hpp"
#include "opencv2\imgproc.hpp"
using namespace cv;

int main(int argc, char* argv[])
{
	VideoCapture vc(0);
	/*Mat frame;
	vc>>frame;*/

	int nWidth  = 1920;
	int nHeight = 1080;

	//Some local variables
	int mVidWidth = nWidth;//frame.cols;
	int mVidHeight = nHeight;//frame.rows;

	//Create Class
	FFMPEG * mFFMPEG = new FFMPEG();
	
	//Setup Encoder
	mFFMPEG->SetVideoResolution(mVidWidth, mVidHeight);	//Set Frame Resolution
	//Set Bitrate (this is extremely important to get correct)
	mFFMPEG->m_AVIMOV_BPS = 1024*1024*10;

	
	//--------------------
	//Optional settings -- Uncomment to use
	//--------------------
	mFFMPEG->SetRTSPPort(8554);
	mFFMPEG->SetRTSPUserandPassword("", "");
	mFFMPEG->SetRTSPAddress("multicast");
	mFFMPEG->SetRTSPDescription("This is the stream description");
	mFFMPEG->SetRtpPortNum(20000);

	//Encoder options
	mFFMPEG->SetEncoder(AV_CODEC_ID_H264);
	//Other encoders
	// AV_CODEC_ID_H265
	// AV_CODEC_ID_H264
	// AV_CODEC_ID_MPEG4
	// AV_CODEC_ID_MPEG2VIDEO

	mFFMPEG->SetMulticast();
	mFFMPEG->SetUnicast();
	//--------------------
	

	//Start Encoding and Streaming
	mFFMPEG->Start();

	//Temporary Frame
	unsigned char * TempRGBFrame = new unsigned char[mVidWidth * mVidHeight * 3];
	cv::namedWindow("frame",CV_WINDOW_FREERATIO);
	//Main Random Frame Generation loop
	Mat frame,temp;
	while (1) 
	{
		vc>>frame;
		//flip(frame,temp,1);
		imshow("frame",frame);
		//cv::cvtColor(frame,frame,CV_BGR2RGB);
		
		/*for (int i = 0; i < mVidWidth * mVidHeight * 3; i++) 
		{
			TempRGBFrame[i] = rand() % 256;
		}
		mFFMPEG->SendNewFrame((char*)TempRGBFrame);*/
		resize(frame,frame,cv::Size(nWidth,nHeight),0,0,CV_INTER_LINEAR);
		mFFMPEG->SendNewFrame((char*)frame.data);
		if(cv::waitKey(1)==27)
			break;
		//Sleep(1);
	}

	//Clean up
	mFFMPEG->Stop();
	while (!mFFMPEG->IsDone()) {
		Sleep(1);
	}
	delete mFFMPEG;
	delete[] TempRGBFrame;
	return 0;
}

