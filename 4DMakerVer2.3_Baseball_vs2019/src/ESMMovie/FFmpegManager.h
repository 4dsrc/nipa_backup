#pragma once

extern "C"{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/mem.h>
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
#include "libavutil/mathematics.h"
};

#include "ESMMovieData.h"
#include "ESMMovieMetadataMgr.h"
#include "opencv2/core/core.hpp"
#include "opencv2/core/core_c.h"
#include "ESMDefine.h"
#include <queue>
#include <stack>
#include <iterator>
#include "ESMIndexStructure.h"
//#include "ImageRevisionMgr.h"

using namespace std;
#define FFMPEGFPS	30

enum ImageProcessState{ PS_NON, PS_POSITIONING, PS_POSITION, PS_COLORING, PS_COLORREF, PS_COLORREFOK, PS_COLOR};

enum FrameRateState{
	RS_MOVIE_FPS_UHD_25P = 0,
	RS_MOVIE_FPS_UHD_30P,
	RS_MOVIE_FPS_FHD_50P,
	RS_MOVIE_FPS_FHD_30P,
	RS_MOVIE_FPS_FHD_25P,
	RS_MOVIE_FPS_FHD_60P,
	RS_MOVIE_FPS_FHD_120P,
	RS_MOVIE_FPS_UHD_30P_X2,
	RS_MOVIE_FPS_UHD_60P_X2,
	RS_MOVIE_FPS_UHD_30P_X1,
	RS_MOVIE_FPS_UHD_60P_X1,
	RS_MOVIE_FPS_UHD_60P,
	RS_MOVIE_FPS_UHD_50P,
	RS_MOVIE_FPS_UNKNOWN,
};

//enum DeviceModelState{
//	RS_DSC_DEVICE_MODEL_NX1	= 0,
//	RS_DSC_DEVICE_MODEL_GH5	,
//	RS_DSC_DEVICE_MODEL_CNT	,
//};

struct AdjustInfo
{
	void* m_pImgMgr;

	CString m_AdjstrSrcFile, m_AdjstrAdjFile, m_AdjstrTargetFile, m_strDSC;
	CvPoint2D32f m_AdjptAdj, m_AdjptLT, m_AdjptRB, m_AdjptRotate;
	double m_AdjdbAngleAdjust, m_AdjdbMaxAngle, m_AdjdAdjustSize;


	BOOL m_b3D;
};

struct stImageBuffer
{
	stImageBuffer()
	{
		cvImage = NULL;
		WakingImage = NULL;
		pData = NULL;
		nIndex = 0;
		nProcessState = PS_NON;
		nCamIndex = 0;
		nFrameIndex = 0;
		bLogo = FALSE;
	}
	CString strDscName;
	IplImage* WakingImage;		// 작업과정의 Temp용 Image
	IplImage* cvImage;			// 
	char* pData;
	int nIndex;
	int nProcessState;
	int nCamIndex;
	AdjustInfo pAdjustInfo;
	int nFrameIndex ;
	BOOL bLogo;
};


struct StCvBuffer
{
	StCvBuffer()
	{
		pTarImageBuffer = NULL;
		Ref1IplImage = NULL;
		Ref2IplImage = NULL;
//		ImageMgr = NULL;
	}
	stImageBuffer* pTarImageBuffer;
	stImageBuffer* Ref1IplImage;
	IplImage* Ref2IplImage;
	//ImageRevisionMgr* ImageMgr;
	CRITICAL_SECTION* crQueue;
};
class AFX_EXT_CLASS FFmpegManager
{
public:
	FFmpegManager(BOOL bMutex = TRUE);
	~FFmpegManager(void);

	BOOL CheckCudaSupport();
	BOOL InitEncode(CString strFileName, int nFrmSpeed, int codec_id, int nGopSize, int nWidth, int nHeight,BOOL bRefereeMode = FALSE,BOOL bLowBitrate = FALSE, int nBitrate = 2147483647, BOOL bHighQualityForH264 = FALSE);
	int InsertMovieData(CString strMoviePath, int nStartFrm, int nEndFrm, ESMMovieData* pMovieData);
	int InsertMovieDataDS(vector<MakeFrameInfo>* pArrFrameInfo, int nStartFrm, int nEndFrm, int nArrayIndex, BOOL bGPUMakeFile = FALSE);
	void PushEncodeData(AVFrame *pFrame);
	void EncodePushImage(BYTE* pImage, BOOL nIsEndFrame = FALSE, BOOL bGPUUse = FALSE);
	void CloseEncode(BOOL bClose = TRUE);
	int	 GetRunningTime(CString strMovieFile);
	BOOL GetMovieInfo(CString strMovieFile, int *nRunningTime, int *nFrameCount, int *nWidth, int *nHight, int *nChannel, int *nFrameRate);
	BOOL GetCaptureImage(CString strMovieFile, int nCaptureTime, int nWidth, int nHeight, vector<ESMFrameArray*> pImgArr,int& nNextLoad);
	BOOL GetCaptureImage(CString strMovieFile, BYTE** pBmpBits,int nCaptureTime, int* nImgWidth, int* nImgHight, int nGopSize,BOOL bReverse = FALSE);
	BOOL GetCaptureImage(CString strMovieFile, IplImage* iplImage,int nCaptureTime);
	BOOL GetCaptureImage(CString strMovieFile, vector<ST_IMAGELIST>& iplImageList,int nCaptureTime, int nGetCount);	//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가
	BOOL GetCaptureImage(CString strMovieFile, int nCaptureTime, vector<ESMFrameArray*> pImgArr,int& nNextLoad); //2016-08-19 hjcho 추가
	void BitmapCpy(BYTE* pSrcBits, BYTE* pBmpBits, int srcImgWidthStep, int width, int nHeight);
	void BmpCpy(BYTE* pSrcBits, BYTE* pBmpBits, int srcImgWidthStep, int width, int height);
	int WriteJPEG (AVCodecContext *pCodecCtx, AVFrame *pFrame, int FrameNo);
	void SaveFrame(AVFrame *pFrame, int width, int height, int iFrame) ;
	void SeekFrame(AVFormatContext *pFormatCtx, AVCodecContext  *pCodecCtx, int videoStreamIdx, int nGopSize, int nSearchIndex);
	void SeekFrame(AVFormatContext *pFormatCtx, AVCodecContext  *pCodecCtx, int videoStreamIdx, int nGopSize, int nSearchIndex, vector<ESMFrameArray*> pImgArr, int& nNextLoad);
	void SeekNextFrame(AVFormatContext *pFormatCtx, AVCodecContext  *pCodecCtx, int videoStreamIdx, int nGopSize, int nSearchIndex, vector<ESMFrameArray*> pImgArr, int nNextLoad);
	int GetFrames(CString strDSC, int videoStreamIdx, AVFormatContext *pFormatCtx, AVCodecContext  *pCodecCtx, AVCodec *pCodec, int nStartFrame, int nEndFrame, ESMMovieData* pMovieData);
	int GetFramesDS(int videoStreamIdx, AVFormatContext *pFormatCtx, AVCodecContext  *pCodecCtx, AVCodec *pCodec, int nStartFrame, int nEndFrame, vector<MakeFrameInfo>* pArrFrameInfo, int nArrayIndex);

	int GetReverseFrames(CString strDSC,int videoStreamIdx, AVFormatContext *pFormatCtx, AVCodecContext  *pCodecCtx, AVCodec *pCodec, int nStartFrame, int nEndFrame, ESMMovieData* pMovieData);
	void AVFrameToIplImage(AVFrame* avFrame, IplImage* iplImage);
	void IplImageToAVFrame(IplImage* iplImage, AVFrame* avFrame, int frameWidth, int frameHeight, enum PixelFormat pix_fmt);
	void SetMakeMovie(CString strMovieFile, CString strDSC, int nStartFrame, int nEndFrame);
	void SetAdjust(AdjustInfo* AdjInfo);
	int GetReverseMovieJumpPos(int nStartFrame, int nEndFrame, int nGopSize, int nCurFramePos);

	static unsigned WINAPI OpserverBufferThread(LPVOID param);
	static unsigned WINAPI AdjustPositionThread(LPVOID param);
	static unsigned WINAPI AdjustColorThread(LPVOID param);
	static unsigned WINAPI CloseEncodeThread(LPVOID param);
	static int m_nFrameRate;
	//CMiLRe 20151020 VMCC Effect(ZOOM) 기능 추가
	static int m_nDuration;


	void TimeCheck(CString str);

	void ConvertYUV( unsigned char *in, unsigned char *out, int w, int h );

	//16-10-17 hjcho cvt YUV to Gray
	void ConvertGray( unsigned char *in, unsigned char *out, int w, int h );

	//170223 hjcho Change the encoding as OpenCV
	VideoWriter* m_VideoWriter;
	BOOL SetWriter(CString strPath, int nHeight, int nWidth);
	void PushImage(BYTE* pImage,int nHeight,int nWidth);
	void ReleaseWriter();

	void SetFrameRate(int nRate);
	int GetFrameRate();

	//170724 wgkim Metadata Insert	
	void CloseEncode(BOOL bClose, CESMMovieMetadataMgr *pMovieMetadataMgr);
	void EncodePushUsingYUV(cv::Mat Y, cv::Mat U, cv::Mat V,int nWidth,int nHeight);
	void EncodePushUsingYUV(BYTE* Y, BYTE* U, BYTE* V,int nWidth,int nHeight);

	//170905 wgkim Mpeg2Ts Transcoding
	AVStream* m_pVideoStream;
	AVFormatContext* m_pFormatCtx;
	BOOL InitEncodeTS(CString strFileName, int nFrmSpeed, int codec_id, int nGopSize, int nWidth, int nHeight, int nBitrate = 10240000, BOOL bHighQualityForH264 = FALSE);
	void EncodePushImageTS(BYTE* pImage, BOOL nIsEndFrame = FALSE, BOOL bGPUUse = FALSE);
	void PushEncodeDataTS(AVFrame *pFrame);
	void CloseEncodeTS();

	//180102 hjcho
	int GetFrameCount(CString strPath);
	BOOL GetFrameSize(CString strPath,int &nWidth,int &nHeight);

	void SetDevice(int nDevice);
	int GetDevice();
protected:
	BOOL		m_bOpserverThread;
	HANDLE		m_hOpserverBufferThread;
	vector<stImageBuffer*> m_pArrImage;

	CRITICAL_SECTION crQueue;
	AdjustInfo m_Adjustinfo;

	static HANDLE hMutex;

	// Encode
	AVCodecContext *m_pCodecCtx;
	int m_nEncodeIndex;
	FILE *m_pFile;
	AVFrame *m_pFrame;
	int m_nFrmCountToSpeed;	
	int m_nFrmCount;
	int m_nFrmFinishCount;
	int m_nGopSize;
	int m_bClose;
	int m_nStartFrame;
	int m_nEndFrame;
	
	int m_nFrameType;

	int m_nDeviceType;

public:
	//180406 hjcho
	BOOL m_bYUV422;
};

