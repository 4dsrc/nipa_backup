////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieThread.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-26
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ESMDefine.h"
#include "SdiDefines.h"

class AFX_EXT_CLASS CESMMovieThread: public CWinThread
{
	DECLARE_DYNCREATE(CESMMovieThread)

public:
	CESMMovieThread(void) { m_pParent = NULL; };
	CESMMovieThread(CWinThread* pParent, short nMessage, ESMMovieMsg* pMsg);
	virtual ~CESMMovieThread();
	CWinThread* m_pParent ;
protected:
	
	ESMMovieMsg* m_pMsg;
	short		m_nMessage;
	short m_nProcessOrder;

protected:
	virtual BOOL InitInstance();
	virtual int ExitInstance();	
	void StopThread();
	void FinishThread();
	void SetOrder(short nMessage);
	BOOL WaitOrder(TCHAR* pMovieName, TCHAR* pMovieId, int nFrameNum);

public:	
	virtual int Run(void) {return 0;}
	short GetMessage() {return m_nMessage;}

	int m_nFrameRate;
	void SetFrameRate(int nRate);
	int GetFrameRate();

	void SetModel(SDI_MODEL model);
	SDI_MODEL GetModel();
	SDI_MODEL m_model;
};


