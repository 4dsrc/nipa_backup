#pragma once
#include "stdafx.h"
#include "ESMMovieThread.h"
#include "ESMMovieThreadImage.h"
#include <vector>
#include "ESMDefine.h"

using namespace std;

class AFX_EXT_CLASS CESMMovieRTSend60p
{
public:
	CESMMovieRTSend60p();
	~CESMMovieRTSend60p();

	/*Variable*/
	CWnd * m_pWnd;
	CStringArray m_strArrPath;
	CString m_strIP;
	CString m_strDSC;
	BOOL m_bOpenClose;
	int m_nWidth;
	int m_nHeight;
	int m_nUVWidth;
	int m_nUVHeight;
	int m_nMarginX;
	int m_nMarginY;
	BOOL m_bSetProperty;
	CString m_str4DMakerPath;
	FILE* m_pFile;
	vector<stAdjustInfo> m_AdjustData;

	/*Inline Func*/
	void SetParent(CWnd * pWnd) { m_pWnd = pWnd; }
	void SetRTSendPath(CString strPath){m_strArrPath.Add(strPath);}
	CString GetRTSendPath(int nIdx){return m_strArrPath.GetAt(nIdx);}
	void Set4DAInfo(CString strIP,CString strDSC){m_strIP = strIP; m_strDSC = strDSC;};
	void Set4DMakerPath(CString strPath){m_str4DMakerPath = strPath;}
	void SetMargin(int nMarX,int nMarY){m_nMarginX = nMarX;m_nMarginY = nMarY;}

	/*Func*/
	void Run();
	void ExecuteConsole(CString strPath,int nSecIdx);
	void SetAdjustData(stAdjustInfo* AdjustData);
	stAdjustInfo GetAdjustData(CString strDscId);
	void SaveAdjDataAsTxt(stAdjustInfo AdjustData,int nWidth,int nHeight,double dRatio=1);
	int Round(double dData);

	/*Thread*/
	static unsigned WINAPI RTDoConsole(LPVOID param);
	static unsigned WINAPI RTRunThread(LPVOID param);
	static unsigned WINAPI RTMergeThread(LPVOID param);

	/*Merge Func*/
	int			 m_nCurIdx;
	CStringArray m_strArrProcEnd;
	CStringArray m_strArrOriPath;

	void MergeMovie(int nSecIdx);
	void AddFinishFile(CString strPath){m_strArrProcEnd.Add(strPath);}
	CString GetFinishFilePath(int nIdx)
	{
		if(m_strArrProcEnd.GetCount()== 0)
			return _T("");
		else
			return m_strArrProcEnd.GetAt(nIdx);
	}
	int GetFinishFileCount(){return m_strArrProcEnd.GetCount();}
	void DeleteFinishFile(int nIdx){m_strArrProcEnd.RemoveAt(nIdx);}
	int GetSecIdxFromPath(CString strPath);
	void RemoveAllSavePath(){m_strArrProcEnd.RemoveAll();}
};
struct RTSend60pConsoleData
{
	CESMMovieRTSend60p* pParent;
	CString strPath;
	CString strSavePath;
	int nGPUIdx;
	int nSecIdx;	
	int nMakeFrame;
};