#include "StdAfx.h"
#include "ESMMovieInfoArray.h"
#include "DllFunc.h"

ESMMovieInfoArray::ESMMovieInfoArray(void)
{
	m_nMarginX = 0;
	m_nMarginY = 0;
	m_pMovieInfoArray.clear();
}


ESMMovieInfoArray::~ESMMovieInfoArray(void)
{
	/*if(m_pMovieInfoArray.size())
	{
		m_pMovieInfoArray.clear();

	}*/
}

int ESMMovieInfoArray::GetMovieCount()
{
	return (int)m_pMovieInfoArray.size();
}

stMovieInfo ESMMovieInfoArray::GetMoiveInfo(CString strMovieId)
{
	stMovieInfo Movieinfo;
	int nFrameCount = 0, nWidth = 0, nHeight = 0, nChannel = 0;
	int i =0;
	for( i =0 ;i < m_pMovieInfoArray.size(); i++)
	{
		Movieinfo = m_pMovieInfoArray.at(i);
		if( Movieinfo.strMovieID == strMovieId )
		{
			return Movieinfo;
		}
	}
	//-- 2014-07-16 hongsu@esmlab.com
	//-- Exception
	return Movieinfo;
}

void ESMMovieInfoArray::SetMoiveInfo(stMovieInfo MovieData)
{
	stMovieInfo Movieinfo;
	for( int i =0 ;i < m_pMovieInfoArray.size(); i++)
	{
		Movieinfo = m_pMovieInfoArray.at(i);
		if( Movieinfo.strMovieID == MovieData.strMovieID )
		{
			m_pMovieInfoArray.at(i) = MovieData;
			break;
		}
	}
}

void ESMMovieInfoArray::ReSetMoiveInfo()
{
	for( int i =0 ;i < m_pMovieInfoArray.size(); i++)
	{
		m_pMovieInfoArray.at(i).nHeight = 0;
		m_pMovieInfoArray.at(i).nWidth = 0;
		m_pMovieInfoArray.at(i).nFrameState = 0;
		m_pMovieInfoArray.at(i).nFrameCount = 0;
	}
}

CString ESMMovieInfoArray::GetMoviePath(CString strMovieId)
{
	stMovieInfo Movieinfo;
	CString strMoviePath;
	for( int i =0 ;i < m_pMovieInfoArray.size(); i++)
	{
		Movieinfo = m_pMovieInfoArray.at(i);
		if( Movieinfo.strMovieID == strMovieId )
		{
			strMoviePath = m_pMovieInfoArray.at(i).strMoviePath;
			break;
		}
	}
	return strMoviePath;
}

CString ESMMovieInfoArray::GetMovieID(int nIndex)
{
	CString strMOvieID;
	if(nIndex < m_pMovieInfoArray.size())
		strMOvieID = m_pMovieInfoArray.at(nIndex).strMovieID;
	
	return strMOvieID;
}

int ESMMovieInfoArray::SetMoviePath(CString strMovieId, CString strMoviePath)
{
// 	int nMovieIndex = 0;
// 	stMovieInfo* Movieinfo;
// 	int nFrameCount = 0, nWidth = 0, nHeight = 0, nChannel = 0;
// 	int i =0;
// 	CString strTok1, strTok2;
// 	AfxExtractSubString( strTok1, strMoviePath, 0, '@');
// 	AfxExtractSubString( strTok2, strMoviePath, 1, '@');
// 
// 	for( i =0 ;i < m_pMovieInfoArray.size(); i++)
// 	{
// 		Movieinfo = &(m_pMovieInfoArray.at(i));
// 		if( Movieinfo->strMovieID == strMovieId )
// 		{
// 			Movieinfo->strMoviePath = strTok1;
// 			Movieinfo->strMoviePathBackup = strTok2;
// 			nMovieIndex = i;
// 			break;
// 		}
// 	}
// 	if( m_pMovieInfoArray.size() == i)
// 	{
// 		stMovieInfo AddMovieinfo;
// 		AddMovieinfo.strMovieID = strMovieId;
// 		AddMovieinfo.strMoviePath = strTok1;
// 		AddMovieinfo.strMoviePathBackup = strTok2;
// 		m_pMovieInfoArray.push_back(AddMovieinfo);
// 	}
//	return nMovieIndex;
	return 0;
}

stAdjustInfo ESMMovieInfoArray::GetAdjustData(CString strMovieId)
{
	stMovieInfo* Movieinfo;
	stAdjustInfo AdjustData;
	int nFrameCount = 0, nWidth = 0, nHeight = 0, nChannel = 0;
	int i =0;

	for( i =0 ;i < m_pMovieInfoArray.size(); i++)
	{
		Movieinfo = &(m_pMovieInfoArray.at(i));
		if( Movieinfo->strMovieID == strMovieId )
		{
			AdjustData = Movieinfo->AdjustData;
			break;
		}
	}
	return AdjustData;
}

BOOL g_bAdj = TRUE;

void ESMMovieInfoArray::SetAdjustData(CString strMovieId, stAdjustInfo* AdjustData)
{
	int nFrameCount = 0, nWidth = 0, nHeight = 0, nChannel = 0;
	int nLoop =0;
	BOOL bAdd = FALSE;

	for(nLoop =0 ;nLoop < m_pMovieInfoArray.size(); nLoop++)
	{
		stMovieInfo Movieinfo;

		if(m_pMovieInfoArray.at(nLoop).nFrameRate < 0)
		{
			TRACE(_T("Set Adjust Fail"));
			continue;
		}

		//wgkim 20181017 Adjust 수정되지 않던 부분 변경
		//Movieinfo = m_pMovieInfoArray.at(nLoop);
		//if( Movieinfo.strMovieID == AdjustData->strDSC )
		if( m_pMovieInfoArray[nLoop].strMovieID == AdjustData->strDSC )
		{
			m_pMovieInfoArray[nLoop].AdjustData = *AdjustData;
			bAdd = TRUE;
			break;
		}/*else
		{
			stMovieInfo stMovieinfo;
			stMovieinfo.strMovieID = AdjustData->strDSC;
			stMovieinfo.AdjustData = *AdjustData;

			if(g_bAdj)
			{
				g_bAdj = FALSE;
				m_pMovieInfoArray.push_back(stMovieinfo);
				g_bAdj = TRUE;
			}
		}*/
	}
	if( nLoop == m_pMovieInfoArray.size()) //없을 경우
	{
		TRACE(_T("Add Adjust [%s]%%%%%%%%%%%%%%%%%%%%%%\n"), strMovieId);
		stMovieInfo stMovieinfo;
		stMovieinfo.strMovieID = AdjustData->strDSC;
		stMovieinfo.AdjustData = *AdjustData;

		if(g_bAdj)
		{
			g_bAdj = FALSE;
			m_pMovieInfoArray.push_back(stMovieinfo);
			bAdd = TRUE;
			g_bAdj = TRUE;
		}

	}

	if(bAdd == FALSE)
	{
		stMovieInfo stMovieinfo;
		stMovieinfo.strMovieID = AdjustData->strDSC;
		stMovieinfo.AdjustData = *AdjustData;

		if(g_bAdj)
		{
			g_bAdj = FALSE;
			m_pMovieInfoArray.push_back(stMovieinfo);
			g_bAdj = TRUE;
		}
	}
	//stAdjustInfo adj = GetAdjustData(AdjustData->strDSC);
	//CString strLog;
	//strLog.Format(_T("[%s]%f(%f,%f)(%f,%f)%f"),AdjustData->strDSC,
	//							AdjustData->AdjAngle,AdjustData->AdjptRotate.x,AdjustData->AdjptRotate.y,
	//							AdjustData->AdjMove.x,AdjustData->AdjMove.y,AdjustData->AdjSize);
	//SendLog(5,strLog);
}