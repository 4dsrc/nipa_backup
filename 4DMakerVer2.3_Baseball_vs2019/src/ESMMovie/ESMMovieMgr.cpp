////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieMgr.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-22
//
////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "ESMMovieMgr.h"
//#include "DSCMgr.h"
#include "DllFunc.h"

//-- 2014-05-26 hongsu@esmlab.com
//-- Each Thread
#include "ESMMovieThreadManager.h"
#include "ESMMovieThreadImage.h"
#include "ESMMovieThreadCodec.h"
#include "ESMMovieThreadFilter.h"
#include "ImageManager.h"
#include "SdiDefines.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CESMMovieMgr, CWinThread)

//------------------------------------------------------------------------------ 
//! @brief		CESMMovieMgr(HWND hParentFrm)
//! @date		2014-05-24
//! @author		hongsu.jung
//! @note	 	constructor
//------------------------------------------------------------------------------
CESMMovieMgr::CESMMovieMgr(HWND hParentFrm)
{
	InitializeCriticalSection (&m_CS);	
	m_bThreadRun = FALSE;
	m_bThreadWork = FALSE;
	m_hParentWnd = hParentFrm;
	m_nMarginX = 0;
	m_nMarginY = 0;
	m_bMakingStop = FALSE;
	m_nMovieSize = ESM_MOVIESIZE_FHD;
	ParentHWndReg(hParentFrm);
	m_bGPULoad = FALSE;
	m_model = SDI_MODEL_NX1;

	m_nFrameRateIndex = 0;
}
//------------------------------------------------------------------------------ 
//! @brief		~CESMMovieMgr()
//! @date		2014-05-24
//! @author		hongsu.jung
//! @note	 	destructor
//------------------------------------------------------------------------------
CESMMovieMgr::~CESMMovieMgr(void)
{
	ExitInstance();
	DeleteCriticalSection (&m_CS);
	ClearMovie();
	//-- 2014-09-17 hongsu@esmlab.com
	//-- Remove All Valid Frame Infos 
	RemoveAllValid();
	CloseHandle(hSemaphore);
}

BOOL CESMMovieMgr::InitInstance()
{
	m_bThreadWork = TRUE;
	hSemaphore = CreateSemaphore(NULL, 20, 20, NULL);

	return TRUE;
}

int CESMMovieMgr::ExitInstance()
{
	StopThread();	
	return CWinThread::ExitInstance();
}

//------------------------------------------------------------------------------ 
//! @brief		StopThread
//! @date		2010-06-29
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CESMMovieMgr::StopThread()
{
	if(!m_bThreadRun)
		return;

	m_bThreadWork = FALSE;
	while(m_bThreadRun)
		Sleep(1);

	RemoveAll();
	//-- 2013-12-01 hongsu@esmlab.com
	//-- Confirm Stop Thread
	WaitForSingleObject(this->m_hThread, INFINITE);
}

BEGIN_MESSAGE_MAP(CESMMovieMgr, CWinThread)
END_MESSAGE_MAP()


void CESMMovieMgr::AddEvent(ESMMovieMsg* pMsg)		
{ 
	//-- 2014-05-26 hongsu@esmlab.com
	//-- Don't Get New Message After Stop Thread	
	EnterCS();
	if(m_bThreadWork || pMsg->nMsg == ESM_MOVIE_THREAD_TERMINATE)
		m_arMsg.Add((CObject*)pMsg);
	LeaveCS();

	
}

//------------------------------------------------------------------------------ 
//! @brief		RemoveAll()
//! @date		2010-05-24
//! @author	hongsu.jung
//! @note	 	delete all array
//------------------------------------------------------------------------------
void CESMMovieMgr::RemoveAll()
{
	//-- Remove Message
	int nAll = (int)m_arMsg.GetSize();
	while(nAll--)
	{
		ESMMovieMsg* pMsg = (ESMMovieMsg*)m_arMsg.GetAt(nAll);
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
			m_arMsg.RemoveAt(nAll);
		}		
		else
		{
			m_arMsg.RemoveAll();
			nAll = 0;
		}
	}
}

//--------------------------------------------------------------------------
//!@brief	RUN THREAD
//!@param   
//!@return	
//--------------------------------------------------------------------------
int CESMMovieMgr::Run(void)
{
	int nMsgIndex;
	ESMMovieMsg* pImgMsg = NULL;	

	m_bThreadRun = TRUE;
	//-- Delete Thread by User
	m_bAutoDelete = FALSE;

	while(1)
	{
		//-- 2013-03-01 honsgu.jung
		//-- For CPU
		Sleep(1);

		//-- 2014-05-26 hongsu@esmlab.com
		//-- Get Message 
		nMsgIndex = (int)m_arMsg.GetSize();
		while(nMsgIndex--)
		{
			//-- For CPU
			Sleep(1);

			//-- Get First Message
			pImgMsg = (ESMMovieMsg*)m_arMsg.GetAt(0);
			if(!pImgMsg)
			{
				continue;
				m_arMsg.RemoveAt(0);
			}

			//-- Critical Section 
			EnterCS();
			if(m_bThreadWork || (pImgMsg->nMsg == ESM_MOVIE_THREAD_TERMINATE))
			{
				//--------------------------------------------------------------
				//-- 2014-05-26 hongsu@esmlab.com
				//-- Create Each Image Manager Thread
				//--------------------------------------------------------------
				if(!CheckMovieEvent(pImgMsg))
					TRACE(_T("[ERROR] Unknown Message [%04x]"),pImgMsg->nMsg);
			}

			//-- Delete Message
			//-- 2014-05-29 KCD
			//-- Message Each Class delete
			//delete pImgMsg;
			m_arMsg.RemoveAt(0);

			//-- Critical Section 
			LeaveCS();
		}		

		//-- 2014-05-26 hongsu@esmlab.com
		//-- Check Until Finish Thread
		if(!m_bThreadWork && !m_arThread.GetSize())
			break;
	}	

	m_bThreadRun = FALSE;
	return 0;
}

//--------------------------------------------------------------------------
//!@brief	ParceMessage
//!@param   
//!@return	
//--------------------------------------------------------------------------
BOOL CESMMovieMgr::ParceMessage(short & nMsg, char& nType, char& nMessage)
{
	nType = nMsg / ESM_MOVIE_PARCER;
	nMessage = nMsg % ESM_MOVIE_PARCER;
	return TRUE;
}

//--------------------------------------------------------------------------
//!@brief	CreateEvent
//!@param   
//!@return	
//--------------------------------------------------------------------------
BOOL CESMMovieMgr::CheckMovieEvent(ESMMovieMsg* pMsg)
{
	char nType;
	char nMessage;
	CESMMovieThread* pESMMovieThread = NULL;

	if ( pMsg == NULL )
		return FALSE;

	if(!ParceMessage(pMsg->nMsg, nType, nMessage))
	{
		TRACE(_T("[Error] Unknown Message [%04x]\n"),pMsg->nMsg);
		return FALSE;
	}

	//GPU 동시 접근 
	int nCnt = 0;
	
	/*while(m_bGPULoad)
	{
		//Wating
		if(!m_bGPULoad)
		{
			CString strTime;
			strTime.Format(_T("Waiting Time is %d"),nCnt*10);
			//SendLog(5,strTime);
			break;
		}

		nCnt++;
		Sleep(10);
	}*/

	switch(nType)
	{
	case ESM_MOVIE_MANAGER:	
		pESMMovieThread = new CESMMovieThreadManager(this, pMsg->nMsg, pMsg);
		pESMMovieThread->SetFrameRate(m_nFrameRate);
		pESMMovieThread->SetModel(GetDeviceModel());
		break;
	case ESM_MOVIE_IMAGE:
		pESMMovieThread = new CESMMovieThreadImage(this, pMsg->nMsg, pMsg);
		break;
	case ESM_MOVIE_CODEC:
		pESMMovieThread = new CESMMovieThreadCodec(this, pMsg->nMsg, pMsg);			
		break;
	case ESM_MOVIE_FILTER:
		pESMMovieThread = new CESMMovieThreadFilter(this, pMsg->nMsg, pMsg);
		break;
	case ESM_MOVIE_THREAD:
		TerminateThread(pMsg);
		return TRUE;
	default:
		return FALSE;
	}

	//-- 2014-05-26 hongsu@esmlab.com
	//-- Add List & Create Thread
	AddThread(pESMMovieThread);
	return TRUE;
}

//--------------------------------------------------------------------------
//!@brief	AddThread
//!@param   
//!@return	
//--------------------------------------------------------------------------
void CESMMovieMgr::AddThread(CESMMovieThread* pESMMovieThread)
{
	//-- Add Thread List
	m_arThread.Add((CObject*)pESMMovieThread);
	//-- Start Run
	pESMMovieThread->CreateThread();
	//TRACE(_T("Create Thread\n"));
}

//--------------------------------------------------------------------------
//!@brief	TerminateThread
//!@param   
//!@return	
//--------------------------------------------------------------------------
void CESMMovieMgr::TerminateThread(ESMMovieMsg* pMsg)
{
	CESMMovieThread* pThread = NULL;
	int nAll = (int)m_arThread.GetSize();
	while(nAll--)
	{
		pThread = (CESMMovieThread*)m_arThread.GetAt(nAll);
		if(pThread == (CESMMovieThread*)pMsg->pValue1)
		{
			//TRACE(_T("Terminate Thread [0x%04x] : Remain Thread [%d]\n"), pThread->GetMessage(), nAll);
			delete pThread;
			m_arThread.RemoveAt(nAll);
		}
	}

	if( pMsg )
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CESMMovieMgr::ClearMovie()
{
	for( int i=0 ;i < m_pMovieArray.size(); i++)
	{
		ESMMovieObject* pObject = m_pMovieArray.at(i);
		if(pObject)
		{
			pObject->ClearMovie();
		}
	}
}

void CESMMovieMgr::SetDscCount(int nDscCount)
{
	ClearMovie();
	//m_MovieArray.SetDscCount(nDscCount);	// 생성할때에 m_MovieInfoArray 보내주고 셋팅하는 것으로 변경.
}

void CESMMovieMgr::SetMoviePath(CString strMovieId, CString strMoviePath)
{
	int nMovieIndex = m_MovieInfoArray.SetMoviePath(strMovieId, strMoviePath);
	TCHAR* pMovieId;
	pMovieId = new TCHAR[20];
	ESMMovieMsg* pMsg = new ESMMovieMsg;
	_tcscpy(pMovieId, strMovieId);
	pMsg->pValue1 = pMovieId;
	pMsg->nMsg = ESM_MOVIE_CODEC_GETMOVIEINFO;
	AddEvent(pMsg);
}

void  CESMMovieMgr::DeleteMovie()
{
	EnterCS();
	ESMMovieObject* pMovieObject = NULL;
	for( int i =0 ;i < m_pMovieArray.size(); i++) 
	{
		pMovieObject = m_pMovieArray.at(i);
		delete pMovieObject;
		pMovieObject = NULL;
		break;
	}
	m_pMovieArray.erase(m_pMovieArray.begin(), m_pMovieArray.end());
	LeaveCS();
}

void  CESMMovieMgr::DeleteMovie(CString strMovieName)
{
	ESMMovieObject* pMovieObject;
	int nDeleteIndex = 0;
	EnterCS();
	for( int i =0 ;i < m_pMovieArray.size(); i++) 
	{
		pMovieObject = m_pMovieArray.at(i);
		if(pMovieObject->GetMoiveName() == strMovieName)
		{
			delete pMovieObject;
			pMovieObject = NULL;
			nDeleteIndex = i;
			break;
		}
	}
	m_pMovieArray.erase(m_pMovieArray.begin() + nDeleteIndex);
	LeaveCS();
}

void CESMMovieMgr::AddMovieObject(CString strMovieName)
{
	ESMMovieObject* pMovieObject;
	pMovieObject = new ESMMovieObject(strMovieName);
	pMovieObject->SetMovieInfo(&m_MovieInfoArray);
	EnterCS();
	m_pMovieArray.push_back(pMovieObject);
	LeaveCS();
}

CString CESMMovieMgr::GetMoviePath(CString strMovieId)
{
 	CString strMoviePath;
	m_MovieInfoArray.GetMoviePath(strMovieId);
	//m_MovieArray.GetMovieFile(nMovieIndex);
 	return strMoviePath;
}

stMovieInfo CESMMovieMgr::GetMovieInfo(CString strMovieId)
{
	return m_MovieInfoArray.GetMoiveInfo(strMovieId);
	//return m_MovieArray.GetMovieData(nIndex);
}

void CESMMovieMgr::SetMovieInfo(stMovieInfo MovieInfo)
{
	m_MovieInfoArray.SetMoiveInfo(MovieInfo);
}

void CESMMovieMgr::ReSetMovieInfo()
{
	m_MovieInfoArray.ReSetMoiveInfo();
}

ESMMovieData* CESMMovieMgr::GetMovieData(CString MovieName, int nIndex)
{
	ESMMovieObject* pMovieObject = NULL;
	ESMMovieData* pMovieData = NULL;
	EnterCS();
	for( int i =0 ;i < m_pMovieArray.size(); i++)
	{
		pMovieObject = m_pMovieArray.at(i);
		if(pMovieObject->GetMoiveName() == MovieName)
		{
			pMovieData = pMovieObject->GetMovieData(nIndex);
			break;
		}
	}
	LeaveCS();
	return pMovieData;
}

ESMMovieData* CESMMovieMgr::GetMovieData(CString MovieName, CString strMovieID)
{
	ESMMovieObject* pMovieObject = NULL;
	ESMMovieData* pMovieData = NULL;
	EnterCS();
	for( int i =0 ;i < m_pMovieArray.size(); i++)
	{
		pMovieObject = m_pMovieArray.at(i);
		if(pMovieObject->GetMoiveName() == MovieName)
		{
			pMovieData = pMovieObject->GetMovieData(strMovieID);
			break;
		}
	}
	LeaveCS();
	return pMovieData;
}

stAdjustInfo CESMMovieMgr::GetAdjustData(CString strMovieID)
{
	return  m_MovieInfoArray.GetAdjustData(strMovieID);
	//return  m_MovieArray.GetAdjustData(nIndex);
}

void CESMMovieMgr::SetAdjustData(CString strMovieID, stAdjustInfo* AdjustData)
{
	m_MovieInfoArray.SetAdjustData(strMovieID, AdjustData);
	//m_MovieArray.SetAdjustData(nIndex, AdjustData);
}

void CESMMovieMgr::GetMovieIndex(int nIndex, int& nMovieIndex, int& nRealFrameIndex, int nMoiveSize, int nFrameRate)
{
	int nFrame = 0;
	if( nMoiveSize != -1)
		m_nMovieSize = nMoiveSize;

	//180308
	if(GetDeviceModel() == SDI_MODEL_NX1)
	{
		if(nFrameRate == RS_MOVIE_FPS_UHD_25P || nFrameRate == RS_MOVIE_FPS_FHD_25P)
		{
			nFrame = 24;
		}else if(nFrameRate == RS_MOVIE_FPS_UHD_30P || nFrameRate == RS_MOVIE_FPS_FHD_30P)
		{
			nFrame = 30;
		}else if(nFrameRate == RS_MOVIE_FPS_UHD_50P || nFrameRate == RS_MOVIE_FPS_FHD_50P)
		{
			nFrame = 25;
		}else if(nFrameRate == RS_MOVIE_FPS_FHD_60P || nFrameRate == RS_MOVIE_FPS_FHD_120P)
		{
			nFrame = 30;
		}
		else if(nFrameRate == RS_MOVIE_FPS_UHD_60P)
		{
			nFrame = 60;
		}else
		{
			nFrame = 30;
		}
	}
	else if(GetDeviceModel() == SDI_MODEL_GH5)
	{
		if(nFrameRate == RS_MOVIE_FPS_UHD_25P || nFrameRate == RS_MOVIE_FPS_FHD_25P)
		{
			nFrame = 24/*25*/;
		}else if(nFrameRate == RS_MOVIE_FPS_UHD_30P || nFrameRate == RS_MOVIE_FPS_FHD_30P)
		{
			nFrame = 30;
		}else if(nFrameRate == RS_MOVIE_FPS_UHD_50P || nFrameRate == RS_MOVIE_FPS_FHD_50P)
		{
			nFrame = 48/*50*/;
		}else if(nFrameRate == RS_MOVIE_FPS_UHD_60P || nFrameRate == RS_MOVIE_FPS_FHD_60P)
		{
			nFrame = 60;
		}else if(nFrameRate == RS_MOVIE_FPS_UHD_30P_X2)
		{
			nFrame = 60;
		}else if(nFrameRate == RS_MOVIE_FPS_UHD_60P_X2)
		{
			nFrame = 120;
		}else if(nFrameRate == RS_MOVIE_FPS_UHD_30P_X1) //180202_C
		{
			nFrame = 30;
		}else if(nFrameRate == RS_MOVIE_FPS_UHD_60P_X1) //180202_C
		{
			nFrame = 60;
		}else
		{
			nFrame = 30;
		}
	}
	else
	{
		nFrame = 30;
	}
		
	nMovieIndex = nIndex/nFrame;	
	nRealFrameIndex = nIndex%nFrame;

	TRACE(_T("Real Index = %d nIndex = %d\n"), nRealFrameIndex, nIndex);
	if(nRealFrameIndex < 0)
		nRealFrameIndex = 0;
}

void CESMMovieMgr::SetMargin(int nMx ,int nMy )
{
	m_MovieInfoArray.SetMaginX(nMx);
	m_MovieInfoArray.SetMaginY(nMy);
	CString strLog;
	strLog.Format(_T("Margin: %d,%d"),nMx,nMy);
	SendLog(5,strLog);
}

void CESMMovieMgr::SetMovieSize(int nMovieSize )
{
	m_nMovieSize = nMovieSize;
}

int CESMMovieMgr::GetMovieSize()
{
	return m_nMovieSize;
}

void CESMMovieMgr::SetUHDtoFHD(int nFlag )
{
	m_bUHDtoFHD = nFlag;
}
BOOL CESMMovieMgr::GetUHDtoFHD()
{
	return m_bUHDtoFHD;
}

//wgkim 170727
void CESMMovieMgr::SetLightWeight(int nFlag)
{
	m_bLightWeight = nFlag;
}
BOOL CESMMovieMgr::GetLightWeight()
{
	return m_bLightWeight;
}

//wgkim 180629
void CESMMovieMgr::SetFrameRateIndex(int nFrameRateIndex)
{
	m_nFrameRateIndex = nFrameRateIndex;
}
int CESMMovieMgr::GetFrameRateIndex()
{
	return m_nFrameRateIndex;
}


void CESMMovieMgr::SetViewInfo(int nFlag)
{
	m_bViewInfo= nFlag;
}
BOOL CESMMovieMgr::GetViewInfo()
{
	return m_bViewInfo;
}

void CESMMovieMgr::SetVMCCFlag(int nFlag )
{
	m_bVMCCFlag = nFlag;
}

BOOL CESMMovieMgr::GetVMCCFlag()
{
	return m_bVMCCFlag;
}

void CESMMovieMgr::GetMovieSizeData(int& nSrcWidth, int& nSrcHeight, int& nOutputWidth, int& nOutputHeight)
{
	if(GetMovieSize() == ESM_MOVIESIZE_FHD)
	{
		nSrcWidth = FULL_HD_WIDTH;
		nSrcHeight = FULL_HD_HEIGHT;
		nOutputWidth = MOVIE_OUTPUT_FHD_WIDTH;
		nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
	}
	else
	{
		if(GetUHDtoFHD())
		{
			nSrcWidth = FULL_UHD_WIDTH;
			nSrcHeight = FULL_UHD_HEIGHT;
			nOutputWidth = MOVIE_OUTPUT_FHD_WIDTH;
			nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
		}
		else
		{
			nSrcWidth = FULL_UHD_WIDTH;
			nSrcHeight = FULL_UHD_HEIGHT;
			nOutputWidth = MOVIE_OUTPUT_UHD_WIDTH;
			nOutputHeight = MOVIE_OUTPUT_UHD_HEIGHT;
		}

	}
}
//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-09-17
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
ValidFrame* CESMMovieMgr::GetPrevFrame(CString strID, int nIndex)
{
	ValidFrame* pExist = NULL;
	int nAll = m_arCorrectFrame.GetCount();
	while(nAll--)
	{
		pExist = (ValidFrame*)m_arCorrectFrame.GetAt(nAll);
		if( (pExist->strDSC			== strID	) &&
			(pExist->nCurrentIndex	== nIndex	))
		{
			return pExist;
		}
	}
	return NULL;
}

void CESMMovieMgr::AddValidFrame(ValidFrame* pValidFrame)
{
	ValidFrame* pExist = NULL;
	ValidFrame* pExistPrev = NULL;

	CString strID	= pValidFrame->strDSC;
	int nIndex		= pValidFrame->nCurrentIndex;

	//-- 2014-09-17 hongsu@esmlab.com
	//-- Check Valid Items
	int nAll = m_arCorrectFrame.GetCount();
	while(nAll--)
	{
		pExist = (ValidFrame*)m_arCorrectFrame.GetAt(nAll);
		if( (pExist->strDSC			== strID	) &&
			(pExist->nCurrentIndex	== nIndex	))
		{
			//-- 2014-09-17 hongsu@esmlab.com
			//-- Change to Correct
			if(pExist->nCorrectIndex == -1)
			{
				pExistPrev = GetPrevFrame(strID,nIndex-1);
				//-- 2014-09-21 hongsu@esmlab.com
				//-- Check Previous Error Frame 
				if(pExistPrev)
				{
					pExist->nCorrectIndex = pValidFrame->nCorrectIndex;
				}
			}

			//-- 2014-09-17 hongsu@esmlab.com
			//-- break
			if(pValidFrame)
			{
				delete pValidFrame;
				pValidFrame = NULL;
			}
			return;			
		}
	}

	//-- 2014-09-17 hongsu@esmlab.com
	//-- Non Exist Frame Info
	m_arCorrectFrame.Add((CObject*)pValidFrame);
}

int CESMMovieMgr::CheckValidIndex(CString strID, int nFrame)
{
	ValidFrame* pExist = NULL;
	//-- Check Valid Items
	int nAll = m_arCorrectFrame.GetCount();
	while(nAll--)
	{
		pExist = (ValidFrame*)m_arCorrectFrame.GetAt(nAll);
		if( (pExist->strDSC			== strID	) &&
			(pExist->nCurrentIndex	== nFrame	))
		{
			//-- 2014-09-17 hongsu@esmlab.com
			//-- Skip Frame 
			TRACE(_T("[VALID FRAME] DSC[%s] Change Frame [%d]->[%d]\n"), strID, nFrame, pExist->nCorrectIndex);
			return pExist->nCorrectIndex;
		}
	}	
	//-- 2014-09-17 hongsu@esmlab.com
	//-- Non Exist Correct Frame Info
	return nFrame;
}

void CESMMovieMgr::RemoveAllValid()
{
	ValidFrame* pExist = NULL;
	//-- Check Valid Items
	int nAll = m_arCorrectFrame.GetCount();
	while(nAll--)
	{
		pExist = (ValidFrame*)m_arCorrectFrame.GetAt(nAll);
		if(pExist)
		{
			delete pExist;
			pExist = NULL;
		}
	}
	m_arCorrectFrame.RemoveAll();
}

void CESMMovieMgr::SetFrameRate(int nRate)
{
	m_nFrameRate = nRate;
}

void CESMMovieMgr::SetDeviceModel(SDI_MODEL model)
{
	m_model = model;
}

SDI_MODEL CESMMovieMgr::GetDeviceModel()
{
	return m_model;
}
int CESMMovieMgr::GetMovieFrameCount(int nRate)
{
	int nFrame;
	//180308
	if(GetDeviceModel() == SDI_MODEL_NX1)
	{
		if(nRate == RS_MOVIE_FPS_UHD_25P || nRate == RS_MOVIE_FPS_FHD_25P)
		{
			nFrame = 24;
		}else if(nRate == RS_MOVIE_FPS_UHD_30P || nRate == RS_MOVIE_FPS_FHD_30P  || nRate == RS_MOVIE_FPS_FHD_60P || nRate == RS_MOVIE_FPS_FHD_120P)
		{
			nFrame = 30;
		}else if(nRate == RS_MOVIE_FPS_UHD_50P || nRate == RS_MOVIE_FPS_FHD_50P)
		{
			nFrame = 25;
		}else if(nRate == RS_MOVIE_FPS_UHD_60P)
		{
			nFrame = 60;
		}
	}
	else if(GetDeviceModel() == SDI_MODEL_GH5)
	{
		if(nRate == RS_MOVIE_FPS_UHD_25P || nRate == RS_MOVIE_FPS_FHD_25P)
		{
			nFrame = 24/*25*/;
		}else if(nRate == RS_MOVIE_FPS_UHD_30P || nRate == RS_MOVIE_FPS_FHD_30P)
		{
			nFrame = 30;
		}else if(nRate == RS_MOVIE_FPS_UHD_50P || nRate == RS_MOVIE_FPS_FHD_50P)
		{
			nFrame = 48/*50*/;
		}else if(nRate == RS_MOVIE_FPS_UHD_60P || nRate == RS_MOVIE_FPS_FHD_60P)
		{
			nFrame = 60;
		}else if(nRate == RS_MOVIE_FPS_UHD_30P_X2)
		{
			nFrame = 60;
		}else if(nRate == RS_MOVIE_FPS_UHD_60P_X2)
		{
			nFrame = 120;
		}else if(nRate == RS_MOVIE_FPS_UHD_30P_X1) //180202_C
		{
			nFrame = 30;
		}else if(nRate == RS_MOVIE_FPS_UHD_60P_X1) //180202_C
		{
			nFrame = 60;
		}
	}
	else
	{
		nFrame = 30;
	}

	return nFrame;
}

CESMMovieKZoneMgr* CESMMovieMgr::GetMovieKZoneMgr()
{
	return &m_pMovieKZoneMgr;
}
