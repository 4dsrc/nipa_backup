#include "StdAfx.h"
#include "ESMFunc.h"
#include "FFmpegManager.h"
#include "DllFunc.h"
#include "ESMGpuDecode.h"
#include "ESMDefine.h"
#include <vector>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define MSG_PRE_FRAME_DECODING 99
#define MSG_NEXT_FRAME_DECODING 100



int FFmpegManager::m_nFrameRate = 0;
int FFmpegManager::m_nDuration = 0;
HANDLE FFmpegManager::hMutex = NULL;
FFmpegManager::FFmpegManager(BOOL bMutex)
{
	av_register_all();
	m_bOpserverThread= TRUE;

	// Encode
	m_pCodecCtx = NULL;
	m_nEncodeIndex = 0;
	m_pFile = NULL;
	m_pFrame = NULL;
	m_nFrmCountToSpeed = 0;
	m_nFrmCount = 0;
	m_nGopSize = 15;
	m_nFrmFinishCount = 0;
	m_bClose = FALSE;
	m_hOpserverBufferThread = NULL;
	m_nFrameType = 0;
	m_nDeviceType = 0;
	InitializeCriticalSection (&crQueue);
	if(hMutex == NULL && bMutex == TRUE)
		hMutex = CreateMutex(NULL, TRUE, _T("Unique Name of Mutex"));
	
	m_bYUV422 = FALSE;
}


FFmpegManager::~FFmpegManager(void)
{
	m_bOpserverThread = FALSE;
	WaitForSingleObject(m_hOpserverBufferThread, 20000);
	DeleteCriticalSection (&crQueue);
}

int FFmpegManager::WriteJPEG (AVCodecContext *pCodecCtx, AVFrame *pFrame, int FrameNo)
{ 
	AVCodecContext         *pOCodecCtx; 
	AVCodec                *pOCodec; 
	uint8_t                *Buffer; 
	int                     BufSiz; 
	int                     BufSizActual; 
	int                     ImgFmt = PIX_FMT_YUVJ420P; //for the 	newer ffmpeg version, this int to pixelformat 
	FILE                   *JPEGFile; 
	char                    JPEGFName[256]; 

	BufSiz = avpicture_get_size ( (AVPixelFormat)ImgFmt, pCodecCtx->width,pCodecCtx->height ); 

	Buffer = (uint8_t *)malloc ( BufSiz ); 
	if ( Buffer == NULL ) 
		return ( 0 ); 
	memset ( Buffer, 0, BufSiz ); 

	pOCodecCtx = avcodec_alloc_context3(NULL); 
	if ( !pOCodecCtx ) { 
		free ( Buffer ); 
		return ( 0 ); 
	} 

	pOCodecCtx->bit_rate      = pCodecCtx->bit_rate; 
	pOCodecCtx->width         = pCodecCtx->width; 
	pOCodecCtx->height        = pCodecCtx->height; 
	pOCodecCtx->pix_fmt       = (AVPixelFormat)ImgFmt; 
	pOCodecCtx->codec_id      = CODEC_ID_MJPEG; 
	pOCodecCtx->codec_type    = AVMEDIA_TYPE_VIDEO; 
	pOCodecCtx->time_base.num = pCodecCtx->time_base.num; 
	pOCodecCtx->time_base.den = pCodecCtx->time_base.den; 

	pOCodec = avcodec_find_encoder ( pOCodecCtx->codec_id ); 
	if ( !pOCodec ) 
	{ 
		free ( Buffer ); 
		return ( 0 ); 
	} 
	if ( avcodec_open2 ( pOCodecCtx, pOCodec, NULL ) < 0 ) 
	{ 
		free ( Buffer ); 
		return ( 0 ); 
	} 

	pOCodecCtx->mb_lmin        = pOCodecCtx->lmin = pOCodecCtx->qmin * FF_QP2LAMBDA; 
	pOCodecCtx->mb_lmax        = pOCodecCtx->lmax = pOCodecCtx->qmax * FF_QP2LAMBDA; 
	pOCodecCtx->flags          = CODEC_FLAG_QSCALE; 
	pOCodecCtx->global_quality = pOCodecCtx->qmin * FF_QP2LAMBDA; 

	pFrame->pts     = 1; 
	pFrame->quality = pOCodecCtx->global_quality; 
	BufSizActual = avcodec_encode_video( pOCodecCtx,Buffer,BufSiz,pFrame ); 

 	sprintf ( JPEGFName, "C:\\asdf\\aa\\%06d.jpg", FrameNo ); 
 	JPEGFile = fopen ( JPEGFName, "wb" ); 
 	fwrite ( Buffer, 1, BufSizActual, JPEGFile ); 
 	fclose ( JPEGFile ); 
 
 	avcodec_close ( pOCodecCtx ); 
	free ( Buffer ); 
	return ( BufSizActual ); 
} 


void FFmpegManager::SaveFrame(AVFrame *pFrame, int width, int height, int iFrame) 
{ 
	FILE *pFile; 
	char szFilename[32]; 
	int  y; 

	// Open file 
	sprintf(szFilename, "aa\\%dframe.ppm", iFrame); 
	pFile=fopen(szFilename, "wb"); 
	if(pFile==NULL) 
		return; 

	// Write header 
	fprintf(pFile, "P6\n%d %d\n255\n", width, height); 

	// Write pixel data 
	for(y=0; y<height; y++) 
		fwrite(pFrame->data[0]+y*pFrame->linesize[0], 1, width*3, pFile); 

	// Close file 
	fclose(pFile); 
} 

void FFmpegManager::SeekNextFrame(AVFormatContext *pFormatCtx, AVCodecContext  *pCodecCtx, int videoStreamIdx, int nGopSize, int nSearchIndex, vector<ESMFrameArray*> pImgArr, int nNextLoad)
{
	//pCodecCtx->flags2 |= CODEC_FLAG2_FAST;
	int nSeekIndex = 0;
	AVPacket        packet;
	AVFrame        *pFrame;
	int frameFinished = 0;
	int nQuotient, nRemain = nSearchIndex;
	/// Allocate video frame
	pFrame = avcodec_alloc_frame();
	int nTpData = 1;
	AVStream		*pVideoStream = NULL;


	if( m_nFrameRate == 0 || m_nDuration == 0)
	{
		/// Find the first video stream
		int i = 0;
		videoStreamIdx=-1;
		for(i=0; i < pFormatCtx->nb_streams; i++)
		{
			if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) 
			{ //CODEC_TYPE_VIDEO
				videoStreamIdx=i;
				break;
			}
		}
		pVideoStream = pFormatCtx->streams[videoStreamIdx];

		m_nFrameRate =  pVideoStream->r_frame_rate.num / (double)pVideoStream->r_frame_rate.den + 0.5 ;
		int nIndex = pFormatCtx->duration / pVideoStream->r_frame_rate.den / 1000;
		if(pFormatCtx->duration / pVideoStream->r_frame_rate.den % 1000 <= 0)		nIndex--;
		m_nDuration = pVideoStream->duration / (nIndex * m_nFrameRate + pVideoStream->nb_frames);

	}
	int nSearchGab = 0;
	// 2016-02-05 60 Frame 적용
	m_nGopSize = nGopSize = 15;
	//m_nGopSize = nGopSize = m_nFrameRate/2;
	nQuotient = nSearchIndex / nGopSize;
	nRemain = nSearchIndex % nGopSize;
	nSearchGab = nQuotient * nGopSize * m_nDuration;
	av_seek_frame(pFormatCtx, videoStreamIdx,  nSearchGab,  AVSEEK_FLAG_ANY);

	TRACE(_T("nSearch Index : %d \n"), nSearchIndex);

	IplImage* iplImage = NULL;
	BOOL bFirst = TRUE;
	BOOL bEnd = FALSE;
	
	switch(nNextLoad)
	{
	case MSG_PRE_FRAME_DECODING:
		{
			for(int i=0; av_read_frame(pFormatCtx, &packet) >= 0;) 
			{
				if(packet.stream_index==videoStreamIdx)
				{
					avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);
					av_free_packet(&packet);
					
					if(!pImgArr.at(i)->bLoad)
					{
						if(bFirst)
							bFirst = FALSE;
						else
						{
							iplImage = (IplImage*)pImgArr.at(i)->pImg;
							AVFrameToIplImage(pFrame, iplImage);

							pImgArr.at(i)->pImg = (BYTE*)iplImage;
							pImgArr.at(i)->bLoad = TRUE;
						}
					}

					if(frameFinished) 
					{
						i++;
						if( i == nRemain + 1)
						{
							break;
						}
					}
				}
			}
		}
		break;
	case MSG_NEXT_FRAME_DECODING:
		{
			for(int i=0; av_read_frame(pFormatCtx, &packet) >= 0;) 
			{
				if(packet.stream_index==videoStreamIdx)
				{
					avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);
					av_free_packet(&packet);

					if(!pImgArr.at(i)->bLoad)
					{
						if(pImgArr.at(i+1)->bLoad)
						{
							bEnd = TRUE;
							break;
						}

						iplImage = (IplImage*)pImgArr.at(i)->pImg;
						AVFrameToIplImage(pFrame, iplImage);

						pImgArr.at(i)->pImg = (BYTE*)iplImage;
						pImgArr.at(i)->bLoad = TRUE;
					}

					if(frameFinished) 
					{
						i++;
						if( i == nRemain - 1)
						{
							break;
						}
					}
				}
			}
		}
		break;
	}
	
	//pCodecCtx->flags2 = 0;
	av_free(pFrame);
}

void FFmpegManager::SeekFrame(AVFormatContext *pFormatCtx, AVCodecContext  *pCodecCtx, int videoStreamIdx, int nGopSize, int nSearchIndex, vector<ESMFrameArray*> pImgArr,int& nLextLoad)
{
	//pCodecCtx->flags2 |= CODEC_FLAG2_FAST;
	int nSeekIndex = 0;
	AVPacket        packet;
	AVFrame        *pFrame;
	int frameFinished = 0;
	int nQuotient, nRemain = nSearchIndex;
	/// Allocate video frame
	pFrame = avcodec_alloc_frame();
	int nTpData = 1;
	AVStream		*pVideoStream = NULL;


	if( m_nFrameRate == 0 || m_nDuration == 0)
	{
		/// Find the first video stream
		int i = 0;
		videoStreamIdx=-1;
		for(i=0; i < pFormatCtx->nb_streams; i++)
		{
			if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) 
			{ //CODEC_TYPE_VIDEO
				videoStreamIdx=i;
				break;
			}
		}
		pVideoStream = pFormatCtx->streams[videoStreamIdx];

		m_nFrameRate =  pVideoStream->r_frame_rate.num / (double)pVideoStream->r_frame_rate.den + 0.5 ;
		int nIndex = pFormatCtx->duration / pVideoStream->r_frame_rate.den / 1000;
		if(pFormatCtx->duration / pVideoStream->r_frame_rate.den % 1000 <= 0)		nIndex--;
		m_nDuration = pVideoStream->duration / (nIndex * m_nFrameRate + pVideoStream->nb_frames);

	}
	int nSearchGab = 0;
	// 2016-02-05 60 Frame 적용
	m_nGopSize = nGopSize = 15;
	//m_nGopSize = nGopSize = m_nFrameRate/2;
	nQuotient = nSearchIndex / nGopSize;
	nRemain = nSearchIndex % nGopSize;
	nSearchGab = nQuotient * nGopSize * m_nDuration;
	av_seek_frame(pFormatCtx, videoStreamIdx,  nSearchGab,  AVSEEK_FLAG_ANY);

	TRACE(_T("nSearch Index : %d \n"), nSearchIndex);
	/*if( nRemain == 0)
	{
		av_free(pFrame);
		//TRACE(_T("nQuotient : %d, nRemain : %d\n"), nQuotient, nRemain);
		return ;
	}*/

	IplImage* iplImage = NULL;
	BOOL bMemory = FALSE;
	int nMin = nRemain-5;
	if(nMin < 0)
		nMin = 0;

	//int nStart, nEnd;
	//nStart = GetTickCount();
	//SendLog(1, _T("SeekFrame Start "));
	for(int i=0; av_read_frame(pFormatCtx, &packet) >= 0;) 
	{
		if(packet.stream_index==videoStreamIdx)
		{
			//SendLog(1,_T("[%d]Start time"),i);
			avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);
			//SendLog(1,_T("[%d]End time"),i);

			av_free_packet(&packet);

			if(i >= nMin )
				bMemory = TRUE;


			if(bMemory && i < pImgArr.size())
			{
				iplImage = (IplImage*)pImgArr.at(i)->pImg;
				AVFrameToIplImage(pFrame, iplImage);

				pImgArr.at(i)->pImg = (BYTE*)iplImage;
				pImgArr.at(i)->bLoad = TRUE;
			}


			if(frameFinished) 
			{
				nLextLoad = i;
				i++;
				if( i == nRemain+6)
				{
					//TRACE(_T("nQuotient : %d, nRemain : %d\n"), nQuotient, nRemain);
					break;
				}
			}
		}
	}
	//nEnd = GetTickCount();
	//SendLog(1, _T("SeekFrame End"));
	//pCodecCtx->flags2 = 0;
	av_free(pFrame);
}

void FFmpegManager::SeekFrame(AVFormatContext *pFormatCtx, AVCodecContext  *pCodecCtx, int videoStreamIdx, int nGopSize, int nSearchIndex)
{
	//pCodecCtx->flags2 |= CODEC_FLAG2_FAST;
	int nSeekIndex = 0;
	AVPacket        packet;
	AVFrame        *pFrame;
	int frameFinished = 0;
	int nQuotient, nRemain = nSearchIndex;
	/// Allocate video frame
	pFrame = avcodec_alloc_frame();
	int nTpData = 1;
	AVStream		*pVideoStream = NULL;

	static int64_t nSeekTime = 0;
	if( m_nFrameRate == 0 || m_nDuration == 0)
	{
		/// Find the first video stream
		int i = 0;
		videoStreamIdx=-1;
		for(i=0; i < pFormatCtx->nb_streams; i++)
		{
			if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) 
			{ //CODEC_TYPE_VIDEO
				videoStreamIdx=i;
				break;
			}
		}
		pVideoStream = pFormatCtx->streams[videoStreamIdx];

		m_nFrameRate =  pVideoStream->r_frame_rate.num / (double)pVideoStream->r_frame_rate.den + 0.5 ;
		int nIndex = pFormatCtx->duration / pVideoStream->r_frame_rate.den / 1000;
		if(pFormatCtx->duration / pVideoStream->r_frame_rate.den % 1000 <= 0)		nIndex--;
		m_nDuration = pVideoStream->duration / (nIndex * m_nFrameRate + pVideoStream->nb_frames);

		//wgkim 171128
		int64_t frameTime 
			= ((int64_t)pVideoStream->r_frame_rate.den * pVideoStream->time_base.den)
			/ ((int64_t)pVideoStream->r_frame_rate.num * pVideoStream->time_base.num);

		double timeTest = 1./frameTime * AV_TIME_BASE;
		nSeekTime = frameTime;
	}
	int nSearchGab = 0;
	// 2016-02-05 60 Frame 적용	
	//m_nGopSize = nGopSize = 24 /*15 , 24*/; //jhhan 170713
	//m_nGopSize = nGopSize = m_nFrameRate/2;
	nQuotient = nSearchIndex / nGopSize;
	nRemain = nSearchIndex % nGopSize;
	nSearchGab = nQuotient * nGopSize * nSeekTime/*m_nDuration*/;

	int ret = av_seek_frame(pFormatCtx, videoStreamIdx,  nSearchGab,  AVSEEK_FLAG_ANY);
	
	TRACE(_T("nSearch Index : %d \n"), nSearchIndex);
	if( nRemain == 0)
	{
		av_free(pFrame);
		//TRACE(_T("nQuotient : %d, nRemain : %d\n"), nQuotient, nRemain);
		return ;
	}

	for(int i=0; av_read_frame(pFormatCtx, &packet) >= 0;) 
	{
		if(packet.stream_index==videoStreamIdx)
		{
			avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);
			av_free_packet(&packet);
			if(frameFinished) 
			{
				i++;
				if( i == nRemain)
				{
					TRACE(_T("nQuotient : %d, nRemain : %d\n"), nQuotient, nRemain);
					break;
				}
			}
		}
	}
	//pCodecCtx->flags2 = 0;
	av_free(pFrame);
}

int FFmpegManager::GetReverseFrames(CString strDSC,int videoStreamIdx, AVFormatContext *pFormatCtx, AVCodecContext  *pCodecCtx, AVCodec *pCodec, int nStartFrame, int nEndFrame, ESMMovieData* pMovieData)
{
	int             y = 0, nInsertFrmCount = 0;
	AVPacket        packet;
	int             frameFinished = 0;
	AVFrame        *pFrame = NULL;
	int				nGopSize = m_nGopSize, nSetPosision =0;
	stack<stImageBuffer*> ImageReversStack;
	int nFrameIndex = 0;
	/// Allocate video frame
	pFrame = avcodec_alloc_frame();

	int nQuotient = 0, nRemain = 0;
	nQuotient = nStartFrame / nGopSize;
	nRemain = nStartFrame % nGopSize;
	int nFramePosition = 0;
	if( nQuotient * nGopSize <= nEndFrame)
		nFramePosition = nEndFrame;
	else
		nFramePosition = nQuotient * nGopSize;

	SeekFrame(pFormatCtx, pCodecCtx, videoStreamIdx, nGopSize, nFramePosition);
	for(nInsertFrmCount=0; av_read_frame(pFormatCtx, &packet) >= 0;) 
	{
		if(packet.stream_index==videoStreamIdx)
		{
			avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);

			if(frameFinished) 
			{
				stImageBuffer* pImageBuffer;
				pImageBuffer = new stImageBuffer;
				IplImage* iplImage = cvCreateImage(cvSize(pFrame->width, pFrame->height), IPL_DEPTH_8U, 3);
				if( iplImage == NULL)
				{
					av_free_packet(&packet);
					break;
				}

				AVFrameToIplImage(pFrame, iplImage);
				pImageBuffer->cvImage = iplImage;
				nFrameIndex = pFrame->best_effort_timestamp / pFrame->pkt_duration;
				ImageReversStack.push(pImageBuffer);
			}
			if( nInsertFrmCount == nStartFrame - nEndFrame + 1)
			{
				av_free_packet(&packet);
				break;
			}

			nSetPosision = GetReverseMovieJumpPos(nStartFrame, nEndFrame, nGopSize, nFramePosition);
			if( nSetPosision != 0)
			{
				nFramePosition = nSetPosision;
				SeekFrame(pFormatCtx, pCodecCtx, videoStreamIdx, nGopSize, nFramePosition);
				int nStackSize = ImageReversStack.size();
				for( int i =0 ;i < nStackSize; i++)
				{
					EnterCriticalSection (&crQueue);
					IplImage* iplImage = ImageReversStack.top()->cvImage;
					nFrameIndex = ImageReversStack.top()->nIndex;
					pMovieData->SetImage(nFrameIndex, (BYTE*)iplImage->imageData);
					pMovieData->OrderAdd(nFrameIndex);
					cvReleaseImage(&iplImage);
					ImageReversStack.pop();
					m_nFrmCount++;
					nInsertFrmCount++;

					LeaveCriticalSection (&crQueue);
				}
			}
			else
				nFramePosition++;
		}
		av_free_packet(&packet);
	}
	av_free(pFrame);
	return 0;
}

int FFmpegManager::GetReverseMovieJumpPos(int nStartFrame, int nEndFrame, int nGopSize, int nCurFramePos)
{
	int nSetPostion = 0;
	int nRemain = nCurFramePos % nGopSize;
	if( nCurFramePos == nStartFrame || nRemain == nGopSize -1)	// 전체의 끝지점.
	{
		int nPrevSectionPos = nCurFramePos - nRemain - nGopSize;
		if( nPrevSectionPos  < nEndFrame)
			nSetPostion = nEndFrame;
		else
			nSetPostion = nPrevSectionPos;
	}
	else if(nRemain == nGopSize -1)// 구간의 끝지점
	{
		int nPrevSectionPos = nCurFramePos - (nGopSize * 2 - 1);
		if( nPrevSectionPos  < nEndFrame)
			nSetPostion = nEndFrame;
		else
			nSetPostion = nPrevSectionPos;
	}
	return nSetPostion;
}

int FFmpegManager::GetFrames(CString strDSC,int videoStreamIdx, AVFormatContext *pFormatCtx, AVCodecContext  *pCodecCtx, AVCodec *pCodec, int nStartFrame, int nEndFrame, ESMMovieData* pMovieData)
{
	int             i = 0;
	AVPacket        packet;
	int             frameFinished;
	AVFrame        *pFrame;
	int nFrameIndex= nStartFrame;
	/// Allocate video frame
	pFrame = avcodec_alloc_frame();

	SeekFrame(pFormatCtx, pCodecCtx, videoStreamIdx, m_nGopSize, nStartFrame);
	for(i=0; av_read_frame(pFormatCtx, &packet) >= 0;) 
	{
		//av_seek_frame(pFormatCtx, videoStreamIdx, 25113, AVSEEK_FLAG_FRAME);
		// Is this a packet from the video stream?
		if(packet.stream_index==videoStreamIdx)
		{
			/// Decode video frame
			avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);

			// Did we get a video frame?
			if(frameFinished) 
			{
				i++;
				IplImage* iplImage = cvCreateImage(cvSize(pFrame->width, pFrame->height), IPL_DEPTH_8U, 3);
				if( iplImage == NULL)
					break;

				AVFrameToIplImage(pFrame, iplImage);
				EnterCriticalSection (&crQueue);

				pMovieData->SetImage(nFrameIndex, (BYTE*)iplImage->imageData);
				pMovieData->OrderAdd(nFrameIndex);
				m_nFrmCount++;
				nFrameIndex++;
				LeaveCriticalSection (&crQueue);
				cvReleaseImage(&iplImage);
			}
			if( i == nEndFrame - nStartFrame + 1)
				break;
		}

		av_free_packet(&packet);
	}

	/// Free the Y frame
	av_free(pFrame);

	return 0;
}

int FFmpegManager::GetFramesDS(int videoStreamIdx, AVFormatContext *pFormatCtx, AVCodecContext  *pCodecCtx, AVCodec *pCodec, int nStartFrame, int nEndFrame, vector<MakeFrameInfo>* pArrFrameInfo, int nArrayIndex)
{
	int             i = 0;
	AVPacket        packet;
	int             frameFinished;
	AVFrame        *pFrame;
	int nFrameIndex= nStartFrame;
	/// Allocate video frame
	pFrame = avcodec_alloc_frame();

	if(GetDevice() == SDI_MODEL_NX1)
	{
		if(GetFrameRate() == RS_MOVIE_FPS_UHD_25P || GetFrameRate() == RS_MOVIE_FPS_FHD_25P)
		{
			m_nGopSize = 24;
		}else if (GetFrameRate() == RS_MOVIE_FPS_UHD_30P || GetFrameRate() == RS_MOVIE_FPS_FHD_30P)
		{
			m_nGopSize = 15;
		}
		else if(GetFrameRate() == RS_MOVIE_FPS_FHD_50P)
		{
			m_nGopSize = 25;
		}
		else if(GetFrameRate() == RS_MOVIE_FPS_FHD_60P)
		{
			m_nGopSize = 30;
		}
		else if(GetFrameRate() == RS_MOVIE_FPS_FHD_120P)
		{
			m_nGopSize = 30;
		}
		else
		{
			m_nGopSize = 15;
		}
	}else if(GetDevice() == SDI_MODEL_GH5)
	{
		if(GetFrameRate() == RS_MOVIE_FPS_FHD_30P || GetFrameRate() == RS_MOVIE_FPS_UHD_30P || GetFrameRate() == RS_MOVIE_FPS_UHD_30P_X1 || GetFrameRate() == RS_MOVIE_FPS_UHD_30P_X2)
		{
			m_nGopSize = 10;
		}
		else if(GetFrameRate() == RS_MOVIE_FPS_FHD_60P || GetFrameRate() == RS_MOVIE_FPS_UHD_60P || GetFrameRate() == RS_MOVIE_FPS_UHD_60P_X1 || GetFrameRate() == RS_MOVIE_FPS_UHD_60P_X2)
		{
			m_nGopSize = 20;
		}
		else if(GetFrameRate() == RS_MOVIE_FPS_FHD_25P || GetFrameRate() == RS_MOVIE_FPS_UHD_25P)
		{
			m_nGopSize = 8;
		}
		else if(GetFrameRate() == RS_MOVIE_FPS_FHD_50P || GetFrameRate() == RS_MOVIE_FPS_UHD_50P)
		{
			m_nGopSize = 16;
		}
		else
		{
			m_nGopSize = 10;
		}
	}
		
	SeekFrame(pFormatCtx, pCodecCtx, videoStreamIdx, m_nGopSize, nStartFrame);

	for(i=0; av_read_frame(pFormatCtx, &packet) >= 0;) 
	{
		//av_seek_frame(pFormatCtx, videoStreamIdx, 25113, AVSEEK_FLAG_FRAME);
		// Is this a packet from the video stream?
		if(packet.stream_index==videoStreamIdx)
		{
			/// Decode video frame
			avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);

			// Did we get a video frame?
			if(frameFinished) 
			{
				i++;

				EnterCriticalSection (&crQueue);
				//if(m_bYUV422 == TRUE)
				//{
				//	pFrame->width * pFrame->height;
				//	MakeFrameInfo* pFrameInfo = &(pArrFrameInfo->at(nArrayIndex++));
				//	pFrameInfo->nWidth = pFrame->width;
				//	pFrameInfo->nHeight = pFrame->height;

				//	pFrameInfo->pY = new BYTE[pFrame->width * pFrame->height];
				//	pFrameInfo->pU = new BYTE[pFrame->width / 2 * pFrame->height / 2];
				//	pFrameInfo->pV = new BYTE[pFrame->width / 2 * pFrame->height / 2];

				//	memcpy(pFrameInfo->pY,pFrame->data[0],pFrame->width * pFrame->height);
				//	memcpy(pFrameInfo->pU,pFrame->data[1],pFrame->width / 2 * pFrame->height / 2);
				//	memcpy(pFrameInfo->pV,pFrame->data[2],pFrame->width / 2 * pFrame->height / 2);


				//	//pFrameInfo->nImageSize = nImageSize;
				//	pFrameInfo->bComplete = 1;
				//	m_nFrmCount++;
				//	nFrameIndex++;
				//}
				//else
				{
					TRACE("RGB Proc\n");
					IplImage* iplImage = cvCreateImage(cvSize(pFrame->width, pFrame->height), IPL_DEPTH_8U, 3);
					if( iplImage == NULL)
						break;

					AVFrameToIplImage(pFrame, iplImage);
					//EnterCriticalSection (&crQueue);
					//pMovieData->SetImage(nFrameIndex, (BYTE*)iplImage->imageData);
					int nImageSize = iplImage->imageSize;
					MakeFrameInfo* pFrameInfo = &(pArrFrameInfo->at(nArrayIndex++));
					pFrameInfo->Image = new BYTE[nImageSize];
					memcpy(pFrameInfo->Image, (BYTE*)iplImage->imageData, nImageSize);
					pFrameInfo->nImageSize = nImageSize;
					pFrameInfo->bComplete = 1;
					m_nFrmCount++;
					nFrameIndex++;
					//LeaveCriticalSection (&crQueue);
					cvReleaseImage(&iplImage);
				}
				//AVFrameToIplImage(pFrame, iplImage);
				LeaveCriticalSection (&crQueue);

				//pMovieData->OrderAdd(nFrameIndex);
			}
			if( i == nEndFrame - nStartFrame + 1)
			{
				break;
			}
		}

		av_free_packet(&packet);
	}
	while(i != nEndFrame - nStartFrame + 1)
	{
		i++;
		pArrFrameInfo->at(nArrayIndex++).bComplete = -100;
	}

	/// Free the Y frame
	av_free(pFrame);

	return 0;
}

void FFmpegManager::IplImageToAVFrame(IplImage* iplImage, AVFrame* avFrame, int frameWidth, int frameHeight, enum PixelFormat pix_fmt)
{
	struct SwsContext* img_convert_ctx = 0;
	int linesize[4] = {0, 0, 0, 0};

	img_convert_ctx = sws_getContext(iplImage->width, iplImage->height,	PIX_FMT_BGR24,	frameWidth,	frameHeight,pix_fmt, SWS_LANCZOS, 0, 0, 0);
	if (img_convert_ctx != 0)
	{
		linesize[0] = 3 * iplImage->width;
		sws_scale(img_convert_ctx, (const uint8_t *const*)(&(iplImage->imageData)), linesize, 0, iplImage->height, avFrame->data, avFrame->linesize);
		sws_freeContext(img_convert_ctx);
	}
}


void FFmpegManager::AVFrameToIplImage(AVFrame* avFrame, IplImage* iplImage)
{
	struct SwsContext* img_convert_ctx = 0;
	int linesize[4] = {0, 0, 0, 0};

	img_convert_ctx = sws_getContext(avFrame->width, avFrame->height,(PixelFormat)avFrame->format,iplImage->width,iplImage->height,	PIX_FMT_BGR24, SWS_LANCZOS, 0, 0, 0);
	if (img_convert_ctx != 0)
	{
		linesize[0] = iplImage->widthStep;
		sws_scale(img_convert_ctx, avFrame->data,avFrame->linesize,0,avFrame->height, (uint8_t *const*)(&(iplImage->imageData)), linesize);
		sws_freeContext(img_convert_ctx);
	}
}

int FFmpegManager::InitEncode(CString strFileName, int nFrmSpeed, int codec_id, int nGopSize, int nWidth, int nHeight,BOOL bRefereeMode /*= FALSE*/,BOOL bLowBitrate /*= FALSE*/, int nBitrate, BOOL bHighQualityForH264)
{
	//char *filename = new char[strlen(CT2A(strFileName)) + 1];
	//strcpy(filename, CT2A(strFileName));

	char filename[MAX_PATH];
	sprintf(filename,"%S",strFileName);

	m_nGopSize = nGopSize;
	m_nFrmCount = 0;
	m_nEncodeIndex = 0;
	m_nFrmFinishCount = 0;
	m_nFrmCountToSpeed = 30 + int(1000.0 /FFMPEGFPS/2);
	if(m_nFrmCountToSpeed < 1000.0 /FFMPEGFPS)
		m_nFrmCountToSpeed = 1000.0 /FFMPEGFPS + 1;
	m_nFrmCountToSpeed = int((double)m_nFrmCountToSpeed / ((double)1000 /(double)FFMPEGFPS));
	AVCodec *pCodec;
	int nRet = 0;
	/* find the mpeg1 video encoder */
	WaitForSingleObject(hMutex, INFINITE);

	pCodec = avcodec_find_encoder((AVCodecID)codec_id);
    if (!pCodec) 
	{
		SendLog(0, _T("Codec not found"));
		ReleaseMutex(hMutex);
        return FALSE;
    }

    m_pCodecCtx = avcodec_alloc_context3(pCodec);
    if (!m_pCodecCtx) 
	{
		SendLog(0, _T("Could not allocate video pCodec context"));
		ReleaseMutex(hMutex);
		return FALSE;
    }

    /* resolution must be a multiple of two */
    m_pCodecCtx->width = nWidth;
    m_pCodecCtx->height = nHeight;

	/* put sample parameters */
	//m_pCodecCtx->bit_rate = m_pCodecCtx->width * m_pCodecCtx->height * 40;
#if 0
	int nBitrate = 74*1024*1024;

	m_pCodecCtx->bit_rate = nBitrate;			//Bits Per Second 
	m_pCodecCtx->rc_min_rate = nBitrate;// / (1024*1024) * 18;
	m_pCodecCtx->rc_max_rate = nBitrate;// / (1024*1024) * 23;	
	m_pCodecCtx->bit_rate_tolerance = nBitrate;
	m_pCodecCtx->rc_buffer_size = nBitrate;
	m_pCodecCtx->rc_initial_buffer_occupancy = m_pCodecCtx->rc_buffer_size * 3 / 4;
	m_pCodecCtx->rc_buffer_aggressivity = (float) 1.0;
	m_pCodecCtx->rc_initial_cplx = 0.5;
#else
	if(bRefereeMode == TRUE)
		m_pCodecCtx->bit_rate = 1024*1024*5;
	else
	{
		if(bLowBitrate)
		{
			m_pCodecCtx->qmin = 16; // qmin=10

			m_pCodecCtx->qmax = 16; // qmax=51
			//m_pCodecCtx->bit_rate = (int)(128000.f * 0.80f);
			m_pCodecCtx->bit_rate	= 11520000 * 2;
			m_pCodecCtx->bit_rate_tolerance = 11520000 * 2;//(int) (128000.f * 0.20f);

		}
		else
		{
			if(codec_id == AV_CODEC_ID_MPEG4 )
			{
				m_pCodecCtx->qmax		= 2;
				m_pCodecCtx->qmin		= 2;
			}

			m_pCodecCtx->bit_rate	= nBitrate;
		}
	}
#endif
	CString strLog;
	strLog.Format(_T("[Encoding] Bitrate: %d"),m_pCodecCtx->bit_rate);
	//SendLog(5,strLog);

    /* frames per second */
    m_pCodecCtx->time_base.num = 1;
	m_pCodecCtx->time_base.den = nFrmSpeed;//FFMPEGFPS;
	
	//hjcho GOP 1로 변경(170228)
    m_pCodecCtx->gop_size = nGopSize; /* emit one intra frame every ten frames */
    m_pCodecCtx->max_b_frames=1;
    m_pCodecCtx->pix_fmt = AV_PIX_FMT_YUV420P;
	m_pCodecCtx->thread_type = FF_THREAD_SLICE;
	m_pCodecCtx->thread_count = 16;
	m_pCodecCtx->codec_type = AVMEDIA_TYPE_VIDEO;

	if(codec_id == AV_CODEC_ID_MPEG4 )
	{
		m_pCodecCtx->profile = FF_PROFILE_MPEG4_SIMPLE;	
		m_pCodecCtx->bit_rate_tolerance = 1;				
	}
	if(codec_id == AV_CODEC_ID_H264)
	{
		if(bHighQualityForH264 == FALSE)
		{
			av_opt_set(m_pCodecCtx->priv_data, "preset", "ultrafast", 0);
			av_opt_set(m_pCodecCtx->priv_data, "profile", "baseline", 0);
		}
		else if(bHighQualityForH264 == TRUE)
			av_opt_set(m_pCodecCtx->priv_data, "preset", "veryslow", 0);
	}

    /* open it */
    if (avcodec_open2(m_pCodecCtx, pCodec, NULL) < 0) 
	{
		SendLog(0, _T("Could not open codec"));
		ReleaseMutex(hMutex);
		delete[] filename;
        return FALSE;
    }

    m_pFile = fopen(filename, "wb");
    if (!m_pFile) 
	{	
		SendLog(0, _T("Could not open  %s"), filename);
		ReleaseMutex(hMutex);
		//delete[] filename;
		return FALSE;
    }

	//delete[] filename;

    m_pFrame = avcodec_alloc_frame();
    if (!m_pFrame) 
	{
		//ESMLog(0, _T("Could not allocate video frame"));
		ReleaseMutex(hMutex);
        return -1;
    }
    m_pFrame->format = m_pCodecCtx->pix_fmt;
    m_pFrame->width  = m_pCodecCtx->width;
    m_pFrame->height = m_pCodecCtx->height;

    /* the image can be allocated by any means and av_image_alloc() is
     * just the most convenient way if av_malloc() is to be used */
    nRet = av_image_alloc(m_pFrame->data, m_pFrame->linesize, m_pCodecCtx->width, m_pCodecCtx->height, m_pCodecCtx->pix_fmt, 32);
    if (nRet < 0) 
	{
		SendLog(0, _T("Could not allocate raw picture buffer"));
		ReleaseMutex(hMutex);
        return FALSE;
    }
	ReleaseMutex(hMutex);
	return TRUE;
}

void FFmpegManager::EncodePushImage(BYTE* pImage, BOOL nIsEndFrame, BOOL bGPUUse)
{
	if((CheckCudaSupport()) && bGPUUse && bGPUUse != -100)
	{
		IplImage* img = (IplImage*)pImage;
		IplImage* cvImage;	
		cvImage =cvCreateImage(cvSize(m_pFrame->width, m_pFrame->height), IPL_DEPTH_8U, 3);
		memcpy(cvImage->imageData, img->imageData, m_pFrame->width * m_pFrame->height * 3);
		IplImageToAVFrame(cvImage, m_pFrame, m_pFrame->width, m_pFrame->height, AV_PIX_FMT_YUV420P);
		for( int i =0 ;i < m_nFrmCountToSpeed; i++)
			PushEncodeData(m_pFrame);

		if( nIsEndFrame == TRUE)
			PushEncodeData(m_pFrame);

		cvReleaseImage(&cvImage);
	}
	else
	{
		IplImage* cvImage;	
		cvImage =cvCreateImage(cvSize(m_pFrame->width, m_pFrame->height), IPL_DEPTH_8U, 3);
		memcpy(cvImage->imageData, pImage, m_pFrame->width * m_pFrame->height * 3);
		IplImageToAVFrame(cvImage, m_pFrame, m_pFrame->width, m_pFrame->height, AV_PIX_FMT_YUV420P);
		for( int i =0 ;i < m_nFrmCountToSpeed; i++)
		{
			PushEncodeData(m_pFrame);
		}

		if( nIsEndFrame == TRUE)
			PushEncodeData(m_pFrame);

		cvReleaseImage(&cvImage);
	}
}
void FFmpegManager::PushEncodeData(AVFrame *pFrame)
{
#if 0
	uint8_t *outbuf, *picture_buf;
	int i = 0, out_size, size, x, y, outbuf_size;
	outbuf_size = 100000;
	outbuf = (uint8_t*)malloc(outbuf_size);
	size = m_pCodecCtx->width * m_pCodecCtx->height;
	picture_buf = (uint8_t*)malloc((size * 3) / 2); /* size for YUV 420 */
	pFrame->pts = m_nEncodeIndex++;

	out_size  = avcodec_encode_video(m_pCodecCtx,outbuf,outbuf_size,pFrame);
	fwrite(outbuf, 1, out_size, m_pFile);
	//for(i=0; i<1; i++)
	//{
	//	fflush(stdout);
	//	pFrame->pts = m_nEncodeIndex++;
	//	out_size  = avcodec_encode_video(m_pCodecCtx,outbuf,outbuf_size,pFrame);
	//	fwrite(outbuf, 1, out_size, m_pFile);
	//}

	//for(int i = 0; i < 2 ; i++)
	//{
	//	out_size  = avcodec_encode_video(m_pCodecCtx,outbuf,outbuf_size,pFrame);
	//	fwrite(outbuf, 1, out_size, m_pFile);
	//}
	/**/
	//int nF
	//for(; out_size; i++)
	//{
	//	fflush(stdout);

	//	out_size  = avcodec_encode_video(m_pCodecCtx,outbuf,outbuf_size,pFrame);
	//	printf("write frame %3d (size=%5d)\n", i, out_size);
	//	fwrite(outbuf, 1, out_size, m_pFile);
	//}
#else
	AVPacket Packet;
	int nRet = 0, nGotOutput = 0;
	av_init_packet(&Packet);
	Packet.data = NULL;    // packet data will be allocated by the encoder
	Packet.size = 0;
	fflush(stdout);
	pFrame->pts = m_nEncodeIndex++;
	/* encode the image */
	nRet = avcodec_encode_video2(m_pCodecCtx, &Packet, pFrame, &nGotOutput);
	if (nRet < 0)
	{
		exit(1);
	}

	if (nGotOutput)
	{
		fwrite(Packet.data, 1, Packet.size, m_pFile);
		av_free_packet(&Packet);
	}
	m_nFrmFinishCount = m_nEncodeIndex;
#endif
}

void FFmpegManager::CloseEncode(BOOL bClose)
{
	m_bClose = TRUE;
// 	while (1)
// 	{
// 		if( m_nFrmCount * m_nFrmCountToSpeed == m_nFrmFinishCount)
// 			break;
// 		Sleep(100);
// 	}
	uint8_t endcode[] = { 0, 0, 1, 0xb7 };
	/* add sequence end code to have a real mpeg file */
	if( m_pFile)
	{
		fwrite(endcode, 1, sizeof(endcode), m_pFile);		
		fclose(m_pFile);
	}

	WaitForSingleObject(hMutex, INFINITE);
	avcodec_close(m_pCodecCtx);
	av_free(m_pCodecCtx);

	if( bClose )
	{
		av_freep(&m_pFrame->data[0]);
		avcodec_free_frame(&m_pFrame);
	}
	ReleaseMutex(hMutex);
}

void FFmpegManager::CloseEncode(BOOL bClose, CESMMovieMetadataMgr *pMovieMetadataMgr)
{
	m_bClose = TRUE;

	uint8_t endcode[] = { 0, 0, 1, 0xb7 };
	/* add sequence end code to have a real mpeg file */
	if( m_pFile)
	{	
		SendLog(5, _T("[Metadata]Set Metadata Start"));
		fwrite(endcode, 1, sizeof(endcode), m_pFile);
		//metadataTest	
		pMovieMetadataMgr->SetMetadata(m_pFile);
		SendLog(5, _T("[Metadata]Set Metadata End"));
		fclose(m_pFile);	
		SendLog(5, _T("[Metadata]Close File"));
	}

	WaitForSingleObject(hMutex, INFINITE);
	avcodec_close(m_pCodecCtx);
	av_free(m_pCodecCtx);

	if( bClose )
	{
		av_freep(&m_pFrame->data[0]);
		avcodec_free_frame(&m_pFrame);
	}
	ReleaseMutex(hMutex);
}

int	 FFmpegManager::GetRunningTime(CString strMovieFile)
{
	char* FileName;
	int nSize = strMovieFile.GetLength() + 1;
	FileName = new char[MAX_PATH];
	strcpy(FileName,CT2A(strMovieFile));

	AVFormatContext *pFormatCtx;
	int             videoStreamIdx;
	AVStream		*pVideoStream = NULL;
	int				nRunningTime = 0 ;
	WaitForSingleObject(hMutex, INFINITE);

	pFormatCtx = avformat_alloc_context();

	// Open video file
	if(avformat_open_input(&pFormatCtx, FileName, 0, NULL) != 0)
	{
		//ESMLog(0, _T("avformat_open_input failed: Couldn't open file"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return -1; // Couldn't open file
	}

	/// Retrieve stream information
	if(avformat_find_stream_info(pFormatCtx, NULL) < 0)
	{
		//ESMLog(0, _T("avformat_find_stream_info failed: Couldn't find stream information"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return -1; // Couldn't find stream information
	}

	/// Dump information about file onto standard error
	av_dump_format(pFormatCtx, 0, FileName, 0);

	delete[] FileName;
	/// Find the first video stream
	int i = 0;
	videoStreamIdx=-1;
	for(i=0; i < pFormatCtx->nb_streams; i++)
	{
		if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) 
		{ //CODEC_TYPE_VIDEO
			pVideoStream = pFormatCtx->streams[i];
			videoStreamIdx=i;
			break;
		}
	}
	double durationOfVideo = (double) pVideoStream->duration / pVideoStream->time_base.den;
	durationOfVideo = durationOfVideo * 1000;
	//-- 2014-07-17 hongsu@esmlab.com
	//-- Change to raising from round off
	//nRunningTime = (double)(durationOfVideo + 0.5);	// 반올림.
	nRunningTime = (double)(durationOfVideo + 0.999);	// 올림.

	/// Check if video stream is found
	if(videoStreamIdx==-1)
	{
		ReleaseMutex(hMutex);
		return -1; // Didn't find a video stream
	}

	/// Close the video file
	avformat_close_input(&pFormatCtx);
	ReleaseMutex(hMutex);
	
	return nRunningTime;
}

BOOL  FFmpegManager::GetMovieInfo(CString strMovieFile, int *nRunningTime, int *nFrameCount, int *nWidth, int *nHight, int *nChannel, int *nFrameRate)
{
	char* FileName;
	FileName = new char[MAX_PATH];
	strcpy(FileName,CT2A(strMovieFile));

	AVFormatContext *pFormatCtx;
	int             videoStreamIdx;
	AVStream		*pVideoStream = NULL;
	WaitForSingleObject(hMutex, INFINITE);

	pFormatCtx = avformat_alloc_context();

	// Open video file
	if(avformat_open_input(&pFormatCtx, FileName, 0, NULL) != 0)
	{
		//ESMLog(0, _T("avformat_open_input failed: Couldn't open file"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return -1; // Couldn't open file
	}

	/// Retrieve stream information
	if(avformat_find_stream_info(pFormatCtx, NULL) < 0)
	{
		//ESMLog(0, _T("avformat_find_stream_info failed: Couldn't find stream information"));
		ReleaseMutex(hMutex);
		avformat_close_input(&pFormatCtx);
		delete[] FileName;
		return -1; // Couldn't find stream information
	}

	/// Dump information about file onto standard error
	av_dump_format(pFormatCtx, 0, FileName, 0);

	delete[] FileName;
	/// Find the first video stream
	int i = 0;
	videoStreamIdx=-1;
	for(i=0; i < pFormatCtx->nb_streams; i++)
	{
		if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) 
		{ //CODEC_TYPE_VIDEO
			pVideoStream = pFormatCtx->streams[i];
			videoStreamIdx=i;
			break;
		}
	}

	//-- 2014-09-06 hongsu
	//-- Debug
	if(!pVideoStream)
	{
		TRACE(_T("!pVideoStream\n"));
		avformat_close_input(&pFormatCtx);
		return -1;
	}

	double durationOfVideo = (double) pVideoStream->duration / pVideoStream->time_base.den;
	durationOfVideo = durationOfVideo * 1000;
	*nRunningTime = (double)(durationOfVideo + 0.5);	// 반올림.

	*nFrameCount = pVideoStream->nb_frames;

	*nWidth = pVideoStream->codec->width;
	*nHight = pVideoStream->codec->height;
	*nFrameRate =  pVideoStream->r_frame_rate.num / (double)pVideoStream->r_frame_rate.den;
	*nChannel = 3;
	m_nFrameRate = *nFrameRate;

	/// Check if video stream is found
	if(videoStreamIdx==-1)
	{
		ReleaseMutex(hMutex);
		return FALSE; // Didn't find a video stream
	}

	/// Close the video file
	avformat_close_input(&pFormatCtx);
	ReleaseMutex(hMutex);
	return TRUE;
}

BOOL FFmpegManager::GetCaptureImage(CString strMovieFile, BYTE** pBmpBits,int nCaptureTime, int* nImgWidth, int* nImgHight, int nGopSize,BOOL bReverse /*= FALSE*/)
{
	char* FileName;
	FileName = new char[MAX_PATH];
	strcpy(FileName,CT2A(strMovieFile));
	AVFormatContext *pFormatCtx;
	int             videoStreamIdx;
	AVCodecContext  *pCodecCtx;
	AVCodec         *pCodec;
	int nWidth = 0, nHeight = 0;
	WaitForSingleObject(hMutex, INFINITE);

	pFormatCtx = avformat_alloc_context();

	// Open video file
	if(avformat_open_input(&pFormatCtx, FileName, 0, NULL) != 0)
	{
		//ESMLog(0, _T("avformat_open_input failed: Couldn't open file"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return FALSE; // Couldn't open file
	}

	/// Retrieve stream information
	if(avformat_find_stream_info(pFormatCtx, NULL) < 0)
	{
		//ESMLog(0, _T("avformat_find_stream_info failed: Couldn't find stream information"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return FALSE; // Couldn't find stream information
	}

	/// Dump information about file onto standard error
	av_dump_format(pFormatCtx, 0, FileName, 0);

	delete[] FileName;
	/// Find the first video stream
	int i = 0;
	videoStreamIdx=FALSE;
	for(i=0; i < pFormatCtx->nb_streams; i++)
	{
		if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) 
		{ //CODEC_TYPE_VIDEO
			videoStreamIdx=i;
			break;
		}
	}
	/// Check if video stream is found
	if(videoStreamIdx== -1)
	{
		ReleaseMutex(hMutex);
		return FALSE; // Didn't find a video stream
	}

	/// Get a pointer to the codec context for the video stream
	pCodecCtx = pFormatCtx->streams[videoStreamIdx]->codec;
	pCodecCtx->thread_type = FF_THREAD_SLICE;
	pCodecCtx->thread_count = 16;


	/// Find the decoder for the video stream
	pCodec = avcodec_find_decoder( pCodecCtx->codec_id);
	if(pCodec==NULL) 
	{
		ReleaseMutex(hMutex);
		return FALSE; // Codec not found
	}


	/// Open codec
	if( avcodec_open2(pCodecCtx, pCodec, NULL) < 0 )
	{
		ReleaseMutex(hMutex);
		return FALSE; // Could not open codec
	}

	ReleaseMutex(hMutex);

	AVPacket        packet;
	int             frameFinished;
	AVFrame        *pFrame;
	IplImage* iplImage = NULL;

	/// Allocate video frame
	pFrame = avcodec_alloc_frame();
		
	SeekFrame(pFormatCtx, pCodecCtx, videoStreamIdx, nGopSize, nCaptureTime);
	for(i=0; av_read_frame(pFormatCtx, &packet) >= 0;) 
	{
		//av_seek_frame(pFormatCtx, videoStreamIdx, 25113, AVSEEK_FLAG_FRAME);
		// Is this a packet from the video stream?
		if(packet.stream_index==videoStreamIdx)
		{
			i++;

			/// Decode video frame
			avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);

			// Did we get a video frame?
			if(frameFinished) 
			{
				av_free_packet(&packet);

				nWidth = pFrame->width;
				nHeight = pFrame->height;
				*nImgWidth = nWidth;
				*nImgHight = nHeight;
				iplImage = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 3);

				if( iplImage == NULL)
					break;

				AVFrameToIplImage(pFrame, iplImage);
				break;
			}
		}

		// Free the packet that was allocated by av_read_frame
		av_free_packet(&packet);
	}

	/// Free the Y frame
	av_free(pFrame);
	WaitForSingleObject(hMutex, INFINITE);
	/// Close the codec
	avcodec_close(pCodecCtx);	

	/// Close the video file
	avformat_close_input(&pFormatCtx);
	//av_free(&pFormatCtx);	

	ReleaseMutex(hMutex);

	if( iplImage == NULL)
		return FALSE;

	if(bReverse)
		cvFlip(iplImage,iplImage,-1);

	/*cvWaitKey(0);
	cvDestroyAllWindows();*/
	BYTE* pSrcBits = (BYTE*)iplImage->imageData;
	*pBmpBits = (BYTE*)calloc(sizeof(BYTE), nWidth*nHeight*4);

	BmpCpy(pSrcBits, *pBmpBits, iplImage->widthStep, nWidth, nHeight);
	cvReleaseImage( &iplImage );
	delete iplImage;
	return TRUE;
}

BOOL FFmpegManager::GetCaptureImage(CString strMovieFile, int nCaptureTime, int nWidth, int nHeight, vector<ESMFrameArray*> pImgArr,int& nNextLoad)
{
	char* FileName;
	FileName = new char[MAX_PATH];
	strcpy(FileName,CT2A(strMovieFile));
	AVFormatContext *pFormatCtx;
	int             videoStreamIdx;
	AVCodecContext  *pCodecCtx;
	AVCodec         *pCodec;
	WaitForSingleObject(hMutex, INFINITE);

	pFormatCtx = avformat_alloc_context();

	// Open video file
	if(avformat_open_input(&pFormatCtx, FileName, 0, NULL) != 0)
	{
		//ESMLog(0, _T("avformat_open_input failed: Couldn't open file"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return FALSE; // Couldn't open file
	}

	/// Retrieve stream information
	if(avformat_find_stream_info(pFormatCtx, NULL) < 0)
	{
		//ESMLog(0, _T("avformat_find_stream_info failed: Couldn't find stream information"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return FALSE; // Couldn't find stream information
	}

	/// Dump information about file onto standard error
	av_dump_format(pFormatCtx, 0, FileName, 0);

	delete[] FileName;

	/// Find the first video stream
	int i = 0;
	videoStreamIdx=FALSE;
	for(i=0; i < pFormatCtx->nb_streams; i++)
	{
		if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) 
		{ //CODEC_TYPE_VIDEO
			videoStreamIdx=i;
			break;
		}
	}
	/// Check if video stream is found
	if(videoStreamIdx== -1)
	{
		ReleaseMutex(hMutex);
		return FALSE; // Didn't find a video stream
	}

	/// Get a pointer to the codec context for the video stream
	pCodecCtx = pFormatCtx->streams[videoStreamIdx]->codec;
	pCodecCtx->thread_type = FF_THREAD_SLICE;
	pCodecCtx->thread_count = 16;


	/// Find the decoder for the video stream
	pCodec = avcodec_find_decoder( pCodecCtx->codec_id);
	if(pCodec==NULL) 
	{
		ReleaseMutex(hMutex);
		return FALSE; // Codec not found
	}


	/// Open codec
	if( avcodec_open2(pCodecCtx, pCodec, NULL) < 0 )
	{
		ReleaseMutex(hMutex);
		return FALSE; // Could not open codec
	}

	ReleaseMutex(hMutex);

	AVPacket        packet;
	int             frameFinished;
	AVFrame        *pFrame;
	IplImage* iplImage = NULL;

	/// Allocate video frame
	pFrame = avcodec_alloc_frame();

	if(nNextLoad == MSG_PRE_FRAME_DECODING || nNextLoad == MSG_NEXT_FRAME_DECODING) // Next Seek Frame
		SeekNextFrame(pFormatCtx, pCodecCtx, videoStreamIdx, m_nGopSize, nCaptureTime, pImgArr, nNextLoad);
	else
		SeekFrame(pFormatCtx, pCodecCtx, videoStreamIdx, m_nGopSize, nCaptureTime, pImgArr, nNextLoad);

	/*for(i=0; av_read_frame(pFormatCtx, &packet) >= 0;) 
	{
		//av_seek_frame(pFormatCtx, videoStreamIdx, 25113, AVSEEK_FLAG_FRAME);
		// Is this a packet from the video stream?
		if(packet.stream_index==videoStreamIdx)
		{
			i++;

			/// Decode video frame
			avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);

			// Did we get a video frame?
			if(frameFinished) 
			{
				av_free_packet(&packet);
				iplImage = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 3);
				if( iplImage == NULL)
					break;

				AVFrameToIplImage(pFrame, iplImage);
				break;
			}
		}

		// Free the packet that was allocated by av_read_frame
		av_free_packet(&packet);
	}*/

	/// Free the Y frame
	av_free(pFrame);
	WaitForSingleObject(hMutex, INFINITE);
	/// Close the codec
	avcodec_close(pCodecCtx);

	/// Close the video file
	avformat_close_input(&pFormatCtx);
	ReleaseMutex(hMutex);

	/*if( iplImage == NULL)
		return FALSE;*/

	/*BYTE* pSrcBits = (BYTE*)iplImage->imageData;
	BYTE* pBmpBits = (BYTE*)calloc(sizeof(BYTE), nWidth*nHeight*4);

	BitmapCpy(pSrcBits, pBmpBits, iplImage->widthStep, nWidth, nHeight);
	pImage->SetBitmapBits(nWidth*nHeight*4, pBmpBits);
	free(pBmpBits);*/
	cvReleaseImage( &iplImage );

	return TRUE;
}

BOOL FFmpegManager::GetCaptureImage(CString strMovieFile, int nCaptureTime, vector<ESMFrameArray*> pImgArr, int& nNextLoad)
{
	char* FileName;
	FileName = new char[MAX_PATH];
	strcpy(FileName,CT2A(strMovieFile));
	AVFormatContext *pFormatCtx;
	int             videoStreamIdx;
	AVCodecContext  *pCodecCtx;
	AVCodec         *pCodec;
	WaitForSingleObject(hMutex, INFINITE);

	pFormatCtx = avformat_alloc_context();

	// Open video file
	if(avformat_open_input(&pFormatCtx, FileName, 0, NULL) != 0)
	{
		//ESMLog(0, _T("avformat_open_input failed: Couldn't open file"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return FALSE; // Couldn't open file
	}

	/// Retrieve stream information
	if(avformat_find_stream_info(pFormatCtx, NULL) < 0)
	{
		//ESMLog(0, _T("avformat_find_stream_info failed: Couldn't find stream information"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return FALSE; // Couldn't find stream information
	}

	/// Dump information about file onto standard error
	av_dump_format(pFormatCtx, 0, FileName, 0);

	delete[] FileName;

	/// Find the first video stream
	int i = 0;
	videoStreamIdx=FALSE;
	for(i=0; i < pFormatCtx->nb_streams; i++)
	{
		if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) 
		{ //CODEC_TYPE_VIDEO
			videoStreamIdx=i;
			break;
		}
	}
	/// Check if video stream is found
	if(videoStreamIdx== -1)
	{
		ReleaseMutex(hMutex);
		return FALSE; // Didn't find a video stream
	}

	/// Get a pointer to the codec context for the video stream
	pCodecCtx = pFormatCtx->streams[videoStreamIdx]->codec;
	pCodecCtx->thread_type = FF_THREAD_SLICE;
	pCodecCtx->thread_count = 16;


	/// Find the decoder for the video stream
	pCodec = avcodec_find_decoder( pCodecCtx->codec_id);
	if(pCodec==NULL) 
	{
		ReleaseMutex(hMutex);
		return FALSE; // Codec not found
	}


	/// Open codec
	if( avcodec_open2(pCodecCtx, pCodec, NULL) < 0 )
	{
		ReleaseMutex(hMutex);
		return FALSE; // Could not open codec
	}

	ReleaseMutex(hMutex);

	AVPacket        packet;
	int             frameFinished;
	AVFrame        *pFrame;
	IplImage* iplImage = NULL;

	/// Allocate video frame
	pFrame = avcodec_alloc_frame();

	if(nNextLoad == MSG_PRE_FRAME_DECODING || nNextLoad == MSG_NEXT_FRAME_DECODING) // Next Seek Frame
	{
		SeekNextFrame(pFormatCtx, pCodecCtx, videoStreamIdx, m_nGopSize, nCaptureTime, pImgArr,nNextLoad);
	}
	else
	{
		SeekFrame(pFormatCtx, pCodecCtx, videoStreamIdx, m_nGopSize, nCaptureTime, pImgArr, nNextLoad);
	}
	/*for(i=0; av_read_frame(pFormatCtx, &packet) >= 0;) 
	{
		//av_seek_frame(pFormatCtx, videoStreamIdx, 25113, AVSEEK_FLAG_FRAME);
		// Is this a packet from the video stream?
		if(packet.stream_index==videoStreamIdx)
		{
			i++;

			/// Decode video frame
			avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);

			// Did we get a video frame?
			if(frameFinished) 
			{
				av_free_packet(&packet);
				iplImage = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 3);
				if( iplImage == NULL)
					break;

				AVFrameToIplImage(pFrame, iplImage);
				break;
			}
		}

		// Free the packet that was allocated by av_read_frame
		av_free_packet(&packet);
	}*/

	/// Free the Y frame
	av_free(pFrame);
	WaitForSingleObject(hMutex, INFINITE);
	/// Close the codec
	avcodec_close(pCodecCtx);

	/// Close the video file
	avformat_close_input(&pFormatCtx);
	ReleaseMutex(hMutex);

	/*if( iplImage == NULL)
		return FALSE;*/

	/*BYTE* pSrcBits = (BYTE*)iplImage->imageData;
	BYTE* pBmpBits = (BYTE*)calloc(sizeof(BYTE), nWidth*nHeight*4);

	BitmapCpy(pSrcBits, pBmpBits, iplImage->widthStep, nWidth, nHeight);
	pImage->SetBitmapBits(nWidth*nHeight*4, pBmpBits);
	free(pBmpBits);*/
	cvReleaseImage( &iplImage );

	return TRUE;

	// GPU_DECODING
	/*int nStart = GetTickCount();
	CESMGpuDecode decoder;
	decoder.GPUDecoding(strMovieFile, nCaptureTime, pImgArr, DECODING_TYPE_FRAME);
	int nEnd = GetTickCount();

	CString strTime;
	strTime.Format(_T("Total Decoding = %d ms\n"), nEnd - nStart);
	TRACE(strTime);
	//AfxMessageBox(strTime);
	return TRUE;*/
}

BOOL FFmpegManager::GetCaptureImage(CString strMovieFile, IplImage* iplImage,int nCaptureTime)
{
	char* FileName;
	FileName = new char[MAX_PATH];
	strcpy(FileName,CT2A(strMovieFile));
	AVFormatContext *pFormatCtx;
	int             videoStreamIdx;
	AVCodecContext  *pCodecCtx;
	AVCodec         *pCodec;
	WaitForSingleObject(hMutex, INFINITE);

	pFormatCtx = avformat_alloc_context();

	// Open video file
	if(avformat_open_input(&pFormatCtx, FileName, 0, NULL) != 0)
	{
		//ESMLog(0, _T("avformat_open_input failed: Couldn't open file"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return FALSE; // Couldn't open file
	}

	/// Retrieve stream information
	if(avformat_find_stream_info(pFormatCtx, NULL) < 0)
	{
		//ESMLog(0, _T("avformat_find_stream_info failed: Couldn't find stream information"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return FALSE; // Couldn't find stream information
	}

	/// Dump information about file onto standard error
	av_dump_format(pFormatCtx, 0, FileName, 0);

	delete[] FileName;
	/// Find the first video stream
	int i = 0;
	videoStreamIdx=FALSE;
	for(i=0; i < pFormatCtx->nb_streams; i++)
	{
		if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) 
		{ //CODEC_TYPE_VIDEO
			videoStreamIdx=i;
			break;
		}
	}
	/// Check if video stream is found
	if(videoStreamIdx== -1)
	{
		ReleaseMutex(hMutex);
		return FALSE; // Didn't find a video stream
	}

	/// Get a pointer to the codec context for the video stream
	pCodecCtx = pFormatCtx->streams[videoStreamIdx]->codec;
	pCodecCtx->thread_type = FF_THREAD_SLICE;
	pCodecCtx->thread_count = 16;


	/// Find the decoder for the video stream
	pCodec = avcodec_find_decoder( pCodecCtx->codec_id);
	if(pCodec==NULL) 
	{
		//-- 2014-07-24 hongsu@esmlab.com
		//-- Check HEVC
		if(pCodecCtx->width > 1920 && pCodecCtx->height > 1080)
			pCodecCtx->codec_id = AV_CODEC_ID_HEVC;
		else
		{
			ReleaseMutex(hMutex);
			return FALSE; // Codec not found
		}		
	}


	/// Open codec
	if( avcodec_open2(pCodecCtx, pCodec, NULL) < 0 )
	{
		ReleaseMutex(hMutex);
		return FALSE; // Could not open codec
	}

	ReleaseMutex(hMutex);

	AVPacket        packet;
	int             frameFinished;
	AVFrame        *pFrame;

	/// Allocate video frame
	pFrame = avcodec_alloc_frame();

	SeekFrame(pFormatCtx, pCodecCtx, videoStreamIdx, m_nGopSize, nCaptureTime);
	for(i=0; av_read_frame(pFormatCtx, &packet) >= 0;) 
	{
		//av_seek_frame(pFormatCtx, videoStreamIdx, 25113, AVSEEK_FLAG_FRAME);
		// Is this a packet from the video stream?
		if(packet.stream_index==videoStreamIdx)
		{
			i++;

			/// Decode video frame
			avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);

			// Did we get a video frame?
			if(frameFinished) 
			{
				av_free_packet(&packet);
				if( iplImage == NULL)
					break;

				AVFrameToIplImage(pFrame, iplImage);
				break;
			}
		}

		// Free the packet that was allocated by av_read_frame
		av_free_packet(&packet);
	}

	/// Free the Y frame
	av_free(pFrame);
	WaitForSingleObject(hMutex, INFINITE);
	/// Close the codec
	avcodec_close(pCodecCtx);

	/// Close the video file
	avformat_close_input(&pFormatCtx);
	ReleaseMutex(hMutex);
	return TRUE;
}

//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가
BOOL FFmpegManager::GetCaptureImage(CString strMovieFile, vector<ST_IMAGELIST>& iplImageList, int nCaptureTime, int nGetCount)
{
	char* FileName;
	FileName = new char[MAX_PATH];
	strcpy(FileName,CT2A(strMovieFile));
	AVFormatContext *pFormatCtx;
	int             videoStreamIdx;
	AVCodecContext  *pCodecCtx;
	AVCodec         *pCodec;
	WaitForSingleObject(hMutex, INFINITE);

	pFormatCtx = avformat_alloc_context();

	// Open video file
	if(avformat_open_input(&pFormatCtx, FileName, 0, NULL) != 0)
	{
		//ESMLog(0, _T("avformat_open_input failed: Couldn't open file"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return FALSE; // Couldn't open file
	}

	/// Retrieve stream information
	if(avformat_find_stream_info(pFormatCtx, NULL) < 0)
	{
		//ESMLog(0, _T("avformat_find_stream_info failed: Couldn't find stream information"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return FALSE; // Couldn't find stream information
	}

	/// Dump information about file onto standard error
	av_dump_format(pFormatCtx, 0, FileName, 0);

	delete[] FileName;
	/// Find the first video stream
	int i = 0;
	videoStreamIdx=FALSE;
	for(i=0; i < pFormatCtx->nb_streams; i++)
	{
		if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) 
		{ //CODEC_TYPE_VIDEO
			videoStreamIdx=i;
			break;
		}
	}
	/// Check if video stream is found
	if(videoStreamIdx== -1)
	{
		ReleaseMutex(hMutex);
		return FALSE; // Didn't find a video stream
	}

	/// Get a pointer to the codec context for the video stream
	pCodecCtx = pFormatCtx->streams[videoStreamIdx]->codec;
	pCodecCtx->thread_type = FF_THREAD_SLICE;
	pCodecCtx->thread_count = 16;


	/// Find the decoder for the video stream
	pCodec = avcodec_find_decoder( pCodecCtx->codec_id);
	if(pCodec==NULL) 
	{
		//-- 2014-07-24 hongsu@esmlab.com
		//-- Check HEVC
		if(pCodecCtx->width > 1920 && pCodecCtx->height > 1080)
			pCodecCtx->codec_id = AV_CODEC_ID_HEVC;
		else
		{
			ReleaseMutex(hMutex);
			return FALSE; // Codec not found
		}		
	}


	/// Open codec
	if( avcodec_open2(pCodecCtx, pCodec, NULL) < 0 )
	{
		ReleaseMutex(hMutex);
		return FALSE; // Could not open codec
	}

	ReleaseMutex(hMutex);

	AVPacket        packet;
	int             frameFinished;
	AVFrame        *pFrame;

	/// Allocate video frame
	pFrame = avcodec_alloc_frame();

	SeekFrame(pFormatCtx, pCodecCtx, videoStreamIdx, m_nGopSize, nCaptureTime);
	BOOL bFind = FALSE;
	int nCount = 0;

	for(i=0; av_read_frame(pFormatCtx, &packet) >= 0;) 
	{
		//av_seek_frame(pFormatCtx, videoStreamIdx, 25113, AVSEEK_FLAG_FRAME);
		// Is this a packet from the video stream?
		if(packet.stream_index==videoStreamIdx)
		{
			i++;
			/// Decode video frame
			avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);

			// Did we get a video frame?
			if(frameFinished)
			{
				av_free_packet(&packet);
				bFind = TRUE;
				IplImage* iplImage = cvCreateImage(cvSize(pFrame->width, pFrame->height), IPL_DEPTH_8U, 3);
				if( iplImage == NULL)
				{
					av_free_packet(&packet);
					break;
				}
				AVFrameToIplImage(pFrame, iplImage);
				ST_IMAGELIST elementImgList(nCaptureTime++, iplImage);
				iplImageList.push_back(elementImgList);

				nCount++;
				if( nCount == nGetCount)
					break;
			}
		}
		// Free the packet that was allocated by av_read_frame
		av_free_packet(&packet);
	}
	av_free(pFrame);
	WaitForSingleObject(hMutex, INFINITE);
	/// Close the codec
	avcodec_close(pCodecCtx);

	/// Close the video file
	avformat_close_input(&pFormatCtx);
	ReleaseMutex(hMutex);
	return TRUE;
}


void FFmpegManager::BitmapCpy(BYTE* pSrcBits, BYTE* pBmpBits, int srcImgWidthStep, int width, int height) 
{
	for(int h=0; h<height; h++)
	{
		BYTE* pSrc = pSrcBits + srcImgWidthStep * h;
		BYTE* pDst = pBmpBits + width * 4 * h;
		for(int w=0; w<width; w++)
		{
			*(pDst++) = *(pSrc++);
			*(pDst++) = *(pSrc++);
			*(pDst++) = *(pSrc++);
			*(pDst++) = 0;
		}
	}
}

void FFmpegManager::BmpCpy(BYTE* pSrcBits, BYTE* pBmpBits, int srcImgWidthStep, int width, int height) 
{
	for(int h=0; h<height; h++)
	{
		BYTE* pSrc = pSrcBits + srcImgWidthStep * h;
		BYTE* pDst = pBmpBits + width * 3 * h;
		for(int w=0; w<width; w++)
		{
			*(pDst++) = *(pSrc++);
			*(pDst++) = *(pSrc++);
			*(pDst++) = *(pSrc++);
		}
	}
}

int FFmpegManager::InsertMovieData(CString strMoviePath, int nStartFrm, int nEndFrm, ESMMovieData* pMovieData)
{
	BOOL bReverse = FALSE;
	if( nEndFrm < nStartFrm)
		bReverse = TRUE;

	char* FileName;
	FileName = new char[MAX_PATH];
	strcpy(FileName,CT2A(strMoviePath));

	AVFormatContext *pFormatCtx;
	int             videoStreamIdx;
	AVCodecContext  *pCodecCtx;
	AVCodec         *pCodec;
	WaitForSingleObject(hMutex, INFINITE);

	pFormatCtx = avformat_alloc_context();

	// Open video file
	if(avformat_open_input(&pFormatCtx, FileName, 0, NULL) != 0)
	{
		//ESMLog(0, _T("avformat_open_input failed: Couldn't open file"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return -1; // Couldn't open file
	}

	/// Retrieve stream information
	if(avformat_find_stream_info(pFormatCtx, NULL) < 0)
	{
		//ESMLog(0, _T("avformat_find_stream_info failed: Couldn't find stream information"));
		ReleaseMutex(hMutex);
		delete[] FileName;
		return -1; // Couldn't find stream information
	}

	/// Dump information about file onto standard error
	av_dump_format(pFormatCtx, 0, FileName, 0);

	delete[] FileName;
	/// Find the first video stream
	int i = 0;
	videoStreamIdx=-1;
	for(i=0; i < pFormatCtx->nb_streams; i++)
	{
		if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) 
		{ //CODEC_TYPE_VIDEO
			videoStreamIdx=i;
			break;
		}
	}
	/// Check if video stream is found
	if(videoStreamIdx==-1)
	{
		ReleaseMutex(hMutex);
		return -1; // Didn't find a video stream
	}
	
	/// Get a pointer to the codec context for the video stream
	pCodecCtx = pFormatCtx->streams[videoStreamIdx]->codec;
	pCodecCtx->thread_type = FF_THREAD_SLICE;
	pCodecCtx->thread_count = 16;


	/// Find the decoder for the video stream
	pCodec = avcodec_find_decoder( pCodecCtx->codec_id);
	if(pCodec==NULL) 
	{
		ReleaseMutex(hMutex);
		return -1; // Codec not found
	}


	/// Open codec
	if( avcodec_open2(pCodecCtx, pCodec, NULL) < 0 )
	{
		ReleaseMutex(hMutex);
		return -1; // Could not open codec
	}

	ReleaseMutex(hMutex);
	// Read frames and save them to disk
	if( bReverse == TRUE)
	{
		if ( GetReverseFrames(strMoviePath, videoStreamIdx, pFormatCtx, pCodecCtx, pCodec, nStartFrm, nEndFrm, pMovieData) < 0)
		{
			return -1;
		}
	}
	else
	{
		if ( GetFrames(strMoviePath, videoStreamIdx, pFormatCtx, pCodecCtx, pCodec, nStartFrm, nEndFrm, pMovieData) < 0)
		{
			return -1;
		}
	}

	WaitForSingleObject(hMutex, INFINITE);
	/// Close the codec
	avcodec_close(pCodecCtx);

	/// Close the video file
	avformat_close_input(&pFormatCtx);
	ReleaseMutex(hMutex);
	return -1;
}

BOOL FFmpegManager::CheckCudaSupport()
{
	DISPLAY_DEVICE Display;
	WCHAR GraphicCard[128];

	ZeroMemory( GraphicCard, sizeof( GraphicCard ) );
	memset( &Display, 0, sizeof( DISPLAY_DEVICE ) );
	Display.cb = sizeof( Display );
	EnumDisplayDevices( NULL, 0, &Display, 0 );
	
	int nCnt = 0;
	CString strGraphic;// = Display.DeviceString;

	while(1)
	{
		EnumDisplayDevices( NULL, nCnt, &Display, 0 );
		strGraphic = Display.DeviceString;

		if(strGraphic.GetLength() == FALSE)
			break;
		if(strGraphic.Find(_T("NVIDIA GeForce")) != -1)
			break;

		nCnt++;
	}
#if 0
	if(strGraphic.CompareNoCase(GRAPIC_BOARD_NAME) == 0 || strGraphic.CompareNoCase(GRAPIC_BOARD_NAME2)==0 || strGraphic.CompareNoCase(GRAPIC_BOARD_NAME3) == 0 || strGraphic.CompareNoCase(GRAPIC_BOARD_NAME4) == 0 )
#else
	if(strGraphic.Find(_T("NVIDIA GeForce")) != -1)
#endif
	{
		return TRUE;
	}

	return FALSE;
}

int FFmpegManager::InsertMovieDataDS(vector<MakeFrameInfo>* pArrFrameInfo, int nStartFrm, int nEndFrm, int nArrayIndex, BOOL bGPUUse)
{
	//if(CheckCudaSupport()) //GTX 1060 Use
	if(bGPUUse)
	{
		CESMGpuDecode decoder;
		CString strPath = pArrFrameInfo->at(nArrayIndex).strFramePath;
		vector<ESMFrameArray*> arry;
		
		if(decoder.GPUDecoding(strPath, nStartFrm, nEndFrm, DECODING_TYPE_FRAME, pArrFrameInfo))
		{
			TRACE(_T("Gpu Decoding End"));
		}
		else
		{
			SendLog(0,_T("Decoding Error"));
		}
		
		return -1;
	}
	else
	{
		BOOL bReverse = FALSE;
		if( nEndFrm < nStartFrm)
			bReverse = TRUE;
		CString strMoviePath = pArrFrameInfo->at(nArrayIndex).strFramePath;
		//SendLog(5,strMoviePath);
		char* FileName;
		FileName = new char[MAX_PATH];
		strcpy(FileName,CT2A(pArrFrameInfo->at(nArrayIndex).strFramePath));

		/*if(pArrFrameInfo->at(nArrayIndex).nDscIndex > -1 
			|| pArrFrameInfo->at(nArrayIndex).nPriIndex > -1 || pArrFrameInfo->at(nArrayIndex).nPrinumIndex > 0)
			m_bYUV422 = FALSE;
		else
			m_bYUV422 = TRUE;*/

		AVFormatContext *pFormatCtx;
		int             videoStreamIdx;
		AVCodecContext  *pCodecCtx;
		AVCodec         *pCodec;
		//WaitForSingleObject(hMutex, INFINITE);

		pFormatCtx = avformat_alloc_context();

		// Open video file
		if(avformat_open_input(&pFormatCtx, FileName, 0, NULL) != 0)
		{
			//ESMLog(0, _T("avformat_open_input failed: Couldn't open file"));
			ReleaseMutex(hMutex);
			delete[] FileName;
			int i = 0;
			while(i != nEndFrm - nStartFrm + 1)
			{
				i++;
				TRACE(_T("ERROR avformat_open_input nStartFrm[] nEndFrm[] nArrayIndex[]"),nStartFrm, nEndFrm, nArrayIndex);
				pArrFrameInfo->at(nArrayIndex++).bComplete = -100;
			}
			return -1; // Couldn't open file
		}

		/// Retrieve stream information
		if(avformat_find_stream_info(pFormatCtx, NULL) < 0)
		{
			//ESMLog(0, _T("avformat_find_stream_info failed: Couldn't find stream information"));
			ReleaseMutex(hMutex);
			delete[] FileName;
			int i = 0;
			while(i != nEndFrm - nStartFrm + 1)
			{
				i++;
				TRACE(_T("ERROR avformat_find_stream_info nStartFrm[] nEndFrm[] nArrayIndex[]"),nStartFrm, nEndFrm, nArrayIndex);
				pArrFrameInfo->at(nArrayIndex++).bComplete = -100;
			}
			return -1; // Couldn't find stream information
		}

		/// Dump information about file onto standard error
		av_dump_format(pFormatCtx, 0, FileName, 0);

		delete[] FileName;
		/// Find the first video stream
		int i = 0;
		videoStreamIdx=-1;
		for(i=0; i < pFormatCtx->nb_streams; i++)
		{
			if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) 
			{ //CODEC_TYPE_VIDEO
				videoStreamIdx=i;
				break;
			}
		}
		/// Check if video stream is found
		if(videoStreamIdx==-1)
		{
			ReleaseMutex(hMutex);
			int i = 0;
			while(i != nEndFrm - nStartFrm + 1)
			{
				i++;
				TRACE(_T("ERROR videoStreamIdx==-1 nStartFrm[] nEndFrm[] nArrayIndex[]"),nStartFrm, nEndFrm, nArrayIndex);
				pArrFrameInfo->at(nArrayIndex++).bComplete = -100;
			}
			return -1; // Didn't find a video stream
		}

		/// Get a pointer to the codec context for the video stream
		pCodecCtx = pFormatCtx->streams[videoStreamIdx]->codec;
		pCodecCtx->thread_type = FF_THREAD_SLICE;
		pCodecCtx->thread_count = 16;


		/// Find the decoder for the video stream
		pCodec = avcodec_find_decoder( pCodecCtx->codec_id);
		if(pCodec==NULL) 
		{
			ReleaseMutex(hMutex);
			int i = 0;
			while(i != nEndFrm - nStartFrm + 1)
			{
				i++;
				TRACE(_T("ERROR avcodec_find_decoder nStartFrm[] nEndFrm[] nArrayIndex[]"),nStartFrm, nEndFrm, nArrayIndex);
				pArrFrameInfo->at(nArrayIndex++).bComplete = -100;
			}
			return -1; // Codec not found
		}

		/// Open codec
		if( avcodec_open2(pCodecCtx, pCodec, NULL) < 0 )
		{
			ReleaseMutex(hMutex);
			int i = 0;
			while(i != nEndFrm - nStartFrm + 1)
			{
				i++;
				TRACE(_T("ERROR avcodec_open2 nStartFrm[] nEndFrm[] nArrayIndex[]"),nStartFrm, nEndFrm, nArrayIndex);
				pArrFrameInfo->at(nArrayIndex++).bComplete = -100;
			}
			return -1; // Could not open codec
		}

		ReleaseMutex(hMutex);
		// Read frames and save them to disk
		// 	if( bReverse == TRUE)
		// 	{
		// 		if ( GetReverseFrames(strMoviePath, videoStreamIdx, pFormatCtx, pCodecCtx, pCodec, nStartFrm, nEndFrm, pArrFrameInfo) < 0)
		// 		{
		// 			return -1;
		// 		}
		// 	}
		// 	else
		// 	{
		if ( GetFramesDS(videoStreamIdx, pFormatCtx, pCodecCtx, pCodec, nStartFrm, nEndFrm, pArrFrameInfo, nArrayIndex) < 0)
		{
			return -1;
		}
		//	}

		WaitForSingleObject(hMutex, INFINITE);
		/// Close the codec
		avcodec_close(pCodecCtx);

		/// Close the video file
		avformat_close_input(&pFormatCtx);
		ReleaseMutex(hMutex);
		return -1;
	}
}

void FFmpegManager::SetMakeMovie(CString strMovieFile, CString strDSC, int nStartFrame, int nEndFrame)
{
	//InsertMovieData(strMovieFile, strDSC, nStartFrame, nEndFrame, &m_pArrImage);
}

void FFmpegManager::SetAdjust(AdjustInfo* AdjInfo)
{
	m_Adjustinfo = *AdjInfo;
}

unsigned WINAPI FFmpegManager::CloseEncodeThread(LPVOID param)
{
	FFmpegManager* pFFmpegManager = (FFmpegManager*)param;
	while (1)
	{
		if( pFFmpegManager->m_nFrmCount == pFFmpegManager->m_nFrmFinishCount)
			break;
	}
	return 0;
}
unsigned WINAPI FFmpegManager::OpserverBufferThread(LPVOID param)
{
	FFmpegManager* pFFmpegManager = (FFmpegManager*)param;
	stImageBuffer* pImageBuffer;
//	ImageRevisionMgr ImageMgr;
	StCvBuffer* arrBuffer;
	int nPositionIndex = 0, nColorIndex = 0;
	while(1)
	{
		// 1. 위치 보정
		int nVectorSize = pFFmpegManager->m_pArrImage.size();
		for( int i =0 ;i < nVectorSize; i++)
		{
			pImageBuffer = pFFmpegManager->m_pArrImage.at(i);
			if( pImageBuffer->nProcessState == PS_NON)
			{
				EnterCriticalSection (&pFFmpegManager->crQueue);
				pImageBuffer->nProcessState = PS_POSITIONING;
				LeaveCriticalSection (&pFFmpegManager->crQueue);
				HANDLE hHandle = NULL;
				hHandle = (HANDLE) _beginthreadex(NULL, 0, AdjustPositionThread, (void *)pImageBuffer, 0, NULL);
				CloseHandle(hHandle);
			}
		}

		//  2. Color 보정
// 		if( ESMGetValue(ESM_VALUE_ADJ_REVISION_USE) )
// 		{
			for( int i =0 ;i < nVectorSize; i++) 
			{
				pImageBuffer = pFFmpegManager->m_pArrImage.at(i);
				if( pImageBuffer->nProcessState == PS_POSITION)
				{
					if( pFFmpegManager->m_nFrmCount == 1 && pFFmpegManager->m_bClose == TRUE )	// 한장 밖에 없을경우.
					{
						pFFmpegManager->m_pArrImage.at(i)->nProcessState = PS_COLOR;
					}

					if( pImageBuffer->nIndex == 0  && nVectorSize > 1)		// 시작 부분
					{
						if( pFFmpegManager->m_pArrImage.at(i + 1)->nProcessState != PS_POSITION && pFFmpegManager->m_pArrImage.at(i + 1)->nProcessState != PS_COLOR)
							continue;

						if( pFFmpegManager->m_pArrImage.at(i + 1)->strDscName ==  pImageBuffer->strDscName)
						{
							pImageBuffer->nProcessState = PS_COLORREF;
							continue;
						}
						arrBuffer = new StCvBuffer;
						//arrBuffer->ImageMgr = &ImageMgr;
						arrBuffer->pTarImageBuffer = pImageBuffer;
						arrBuffer->Ref2IplImage = pFFmpegManager->m_pArrImage.at(i + 1)->WakingImage;
						arrBuffer->crQueue = &pFFmpegManager->crQueue;

						EnterCriticalSection (&pFFmpegManager->crQueue);
						pImageBuffer->nProcessState = PS_COLORING;
						LeaveCriticalSection (&pFFmpegManager->crQueue);
						HANDLE hHandle = NULL;						
						hHandle = (HANDLE) _beginthreadex(NULL, 0, AdjustColorThread, (void *)arrBuffer, 0, NULL);
						CloseHandle(hHandle);
					}
					if( pImageBuffer->nIndex != 0 )						// 중간 Frame 부분
					{
						if( pFFmpegManager->m_pArrImage.at(i - 1)->nProcessState < PS_POSITION)
							break;

						if( pFFmpegManager->m_bClose == FALSE )		// Output 영상이 열린 상태
						{
							if( pFFmpegManager->m_pArrImage.size() <= i && pFFmpegManager->m_pArrImage.at(i + 1)->nProcessState < PS_POSITION )
								break;
						}
						else										// Output 영상이 닫힌 상태
						{	
							if( pFFmpegManager->m_nFrmCount - 1 != pImageBuffer->nIndex)		// 마지막 Frame 아닐때
							{
								if( pFFmpegManager->m_pArrImage.size() <= i && pFFmpegManager->m_pArrImage.at(i + 1)->nProcessState < PS_POSITION )
									break;
							}

						}

						BOOL bSameDsc = FALSE;
						if( pFFmpegManager->m_pArrImage.at(i - 1)->strDscName == pImageBuffer->strDscName)
						{
							if(nVectorSize > i + 1 && pFFmpegManager->m_pArrImage.at(i + 1)->nProcessState <= PS_POSITION)
							{
								if( pFFmpegManager->m_pArrImage.at(i - 1)->strDscName == pImageBuffer->strDscName)
									bSameDsc = TRUE;
							}
							else
								bSameDsc = TRUE;
						}
						if( bSameDsc == TRUE)
						{
							if( pFFmpegManager->m_pArrImage.at(i - 1)->nProcessState != PS_COLORREF)
								pFFmpegManager->m_pArrImage.at(i - 1)->nProcessState = PS_COLORREFOK;
							else
								pFFmpegManager->m_pArrImage.at(i - 1)->nProcessState = PS_COLOR;

							if( pImageBuffer->nProcessState == PS_COLORREFOK)
								pImageBuffer->nProcessState = PS_COLOR;
							else
								pImageBuffer->nProcessState = PS_COLORREF;

							break;
						}

						arrBuffer = new StCvBuffer;
//						arrBuffer->ImageMgr = &ImageMgr;
						arrBuffer->pTarImageBuffer = pImageBuffer;
						arrBuffer->Ref1IplImage = pFFmpegManager->m_pArrImage.at(i - 1);
						if(nVectorSize > i + 1 && pFFmpegManager->m_pArrImage.at(i + 1)->nProcessState <= PS_POSITION)
							arrBuffer->Ref2IplImage = pFFmpegManager->m_pArrImage.at(i + 1)->WakingImage;
						arrBuffer->crQueue = &pFFmpegManager->crQueue;

						EnterCriticalSection (&pFFmpegManager->crQueue);
						pImageBuffer->nProcessState = PS_COLORING;
						LeaveCriticalSection (&pFFmpegManager->crQueue);
						HANDLE hHandle = NULL;
						(HANDLE) _beginthreadex(NULL, 0, AdjustColorThread, (void *)arrBuffer, 0, NULL);
						CloseHandle(hHandle);
					}
				}
				if( pFFmpegManager->m_nFrmCount - 1 == pImageBuffer->nIndex && pFFmpegManager->m_bClose == TRUE )		// 마지막 부분
				{
					if( nVectorSize == 1 && pFFmpegManager->m_nFrmCount != 1 )	// 한장이 아닐때.
						pImageBuffer->nProcessState = PS_COLOR;
				}
			}
// 		}
// 		else
// 		{
// 			for( int i =0 ;i < nVectorSize; i++) 
// 			{
// 				pImageBuffer = pFFmpegManager->m_pArrImage.at(i);
// 				if( pImageBuffer->nProcessState == PS_POSITION)	
// 				{
// 					pImageBuffer->nProcessState = PS_COLOR;
// 					break;
// 				}
// 			}
// 		}

		// 3. 영상 제작
		EnterCriticalSection (&pFFmpegManager->crQueue);
		vector<stImageBuffer*>::iterator iter;
		for ( iter = pFFmpegManager->m_pArrImage.begin(); iter != pFFmpegManager->m_pArrImage.end();)
		{
			pImageBuffer =  *iter;
			if( pImageBuffer->nProcessState == PS_COLOR)
			{
				iter = pFFmpegManager->m_pArrImage.erase(iter);
				pFFmpegManager->IplImageToAVFrame(pImageBuffer->cvImage, pFFmpegManager->m_pFrame, pImageBuffer->cvImage->width, pImageBuffer->cvImage->height, AV_PIX_FMT_YUV420P);
				for( int i =0 ;i < pFFmpegManager->m_nFrmCountToSpeed; i++)
					pFFmpegManager->PushEncodeData(pFFmpegManager->m_pFrame);
				cvReleaseImage(&(pImageBuffer->cvImage));
				cvReleaseImage(&(pImageBuffer->WakingImage));
				delete pImageBuffer;
			}
			else
			{
				iter++;
				break;
			}
		}
		LeaveCriticalSection (&pFFmpegManager->crQueue);

		if( pFFmpegManager->m_bOpserverThread == FALSE)
			break;
		Sleep(10);
	}
	return 0;
}

unsigned WINAPI FFmpegManager::AdjustPositionThread(LPVOID param)
{
	stImageBuffer* pImageBuffer = (stImageBuffer*)param;
	//pImageBuffer->pAdjustInfo.m_pImgMgr->SetAdjust2(pImageBuffer);
	pImageBuffer->nProcessState = PS_POSITION;
	return 0;
}

unsigned WINAPI FFmpegManager::AdjustColorThread(LPVOID param)
{
	StCvBuffer* pImageBuffer = (StCvBuffer*)param;
	IplImage* pCvCompareImage1 = NULL;
	if( pImageBuffer->Ref1IplImage != NULL)
	{
		pCvCompareImage1 =cvCreateImage(cvGetSize(pImageBuffer->Ref1IplImage->WakingImage), pImageBuffer->Ref1IplImage->WakingImage->depth, pImageBuffer->Ref1IplImage->WakingImage->nChannels);
		cvCopy(pImageBuffer->Ref1IplImage->WakingImage, pCvCompareImage1);
	}

//	pImageBuffer->ImageMgr->MakeAvaregeImage(pImageBuffer->pTarImageBuffer->cvImage, pCvCompareImage1, pImageBuffer->Ref2IplImage, 0, 0);
	EnterCriticalSection (pImageBuffer->crQueue);
	if( pImageBuffer->Ref1IplImage != NULL && pImageBuffer->Ref1IplImage->nProcessState != PS_COLORREF)
		pImageBuffer->Ref1IplImage->nProcessState = PS_COLORREFOK;
	else if( pImageBuffer->Ref1IplImage != NULL && pImageBuffer->Ref1IplImage->nProcessState == PS_COLORREF)
		pImageBuffer->Ref1IplImage->nProcessState = PS_COLOR;

	if( pImageBuffer->pTarImageBuffer->nProcessState == PS_COLORREFOK)
		pImageBuffer->pTarImageBuffer->nProcessState = PS_COLOR;
	else
		pImageBuffer->pTarImageBuffer->nProcessState = PS_COLORREF;
	LeaveCriticalSection (pImageBuffer->crQueue);

	delete pImageBuffer;
	cvReleaseImage(&pCvCompareImage1);
	return 0;
}
void FFmpegManager::TimeCheck(CString str)
{
	CString timeStr;
	CString timeValue;
	SYSTEMTIME *SystemTime;
	SystemTime = new SYSTEMTIME;
	GetLocalTime(SystemTime);

	timeStr.Format(_T("%d : %d : %d.%d %s"),SystemTime->wHour,SystemTime->wMinute,SystemTime->wSecond,SystemTime->wMilliseconds,str);

	FILE *f;
	char tstr[MAX_PATH];

	sprintf(tstr,"%S",timeStr);

	f = fopen("c:\\Text.txt","at");
	if (f != NULL)
	{
		fputs(tstr, f);
		fclose(f);
	}
}

void FFmpegManager::ConvertYUV( unsigned char *in, unsigned char *out, int w, int h )
{
	CESMGpuDecode decoder;
	//decoder.YUV_lookup_table();
	decoder.yuv420_to_rgb(in, out, w, h);

	/*Mat YUV(h+h/2,w,CV_8UC1,(uchar*)in);
	Mat BGR(h,w,CV_8UC3);
	int sCount,fCount;
	sCount = GetTickCount();
	cvtColor(YUV,BGR,CV_YUV2BGR_IYUV);
	fCount = GetTickCount();
	CString str_log;
	str_log.Format(_T("[%d] OPENCV %d\n"),GetTickCount(), fCount-sCount);
	SendLog(0,str_log);
	TRACE(str_log);*/
}
void FFmpegManager::ConvertGray( unsigned char *in, unsigned char *out, int w, int h )
{
	Mat YUV(h+h/2,w,CV_8UC1,(uchar*)in);
	Mat Gray(h,w,CV_8UC1);
	cvtColor(YUV,Gray,cv::COLOR_YUV2GRAY_IYUV);
	memcpy(out,Gray.data,h*w);
}

BOOL FFmpegManager::SetWriter(CString strPath, int nHeight, int nWidth)
{
	char filename[MAX_PATH];
	sprintf(filename,"%S",strPath);

	double fps = 30;
	int fourcc =cv::VideoWriter::fourcc('m','p','4','v');/*'m','p','g','4'*/
	BOOL bIsColor = TRUE;
	m_VideoWriter = new VideoWriter;
	cv::Size nSize(nWidth,nHeight);
	m_VideoWriter->open(filename,fourcc,fps,nSize,bIsColor);
	if(!m_VideoWriter->isOpened())
	{
		SendLog(5,_T("Cannot open VideoFile"));
		return FALSE;
	}
	return TRUE;
}
void FFmpegManager::PushImage(BYTE* pImage,int nHeight,int nWidth)
{
	Mat frame(nHeight,nWidth,CV_8UC3,pImage);

	m_VideoWriter->write(frame);
}
void FFmpegManager::ReleaseWriter()
{
	delete m_VideoWriter;
}

void FFmpegManager::SetFrameRate(int nRate)
{
	m_nFrameType = nRate;
}
int FFmpegManager::GetFrameRate()
{
	return m_nFrameType;
}

void FFmpegManager::EncodePushUsingYUV(cv::Mat Y, cv::Mat U, cv::Mat V,int nWidth,int nHeight)
{
	//AvFra
	/*struct SwsContext* img_convert_ctx = 0;
	int linesize[4] = {0, 0, 0, 0};
*/
	//AVFrame* _pFrame = av_frame_alloc();
/*
	m_pFrame->linesize[0] = nWidth*nHeight;
	m_pFrame->linesize[1] = nWidth*nHeight/4;
	m_pFrame->linesize[2] = nWidth*nHeight/4;*/

	memcpy(m_pFrame->data[0],Y.data,nWidth*nHeight);
	memcpy(m_pFrame->data[1],U.data,nWidth*nHeight/4);
	memcpy(m_pFrame->data[2],V.data,nWidth*nHeight/4);

	/*m_pFrame->data[0] = Y.data;
	m_pFrame->data[1] = U.data;
	m_pFrame->data[2] = V.data;*/
	PushEncodeData(m_pFrame);
/*
	if (img_convert_ctx != 0)
	{
		linesize[0] = 3 * nWidth;
		sws_scale(img_convert_ctx, (const uint8_t *const*)(&(iplImage->imageData)), linesize, 0, iplImage->height, avFrame->data, avFrame->linesize);
		sws_freeContext(img_convert_ctx);
	}*/
}

//170905 wgkim Mpeg2Ts Transcoding
int FFmpegManager::InitEncodeTS(CString strFileName, int nFrmSpeed, int codec_id, int nGopSize, int nWidth, int nHeight, int nBitrate, BOOL bHighQualityForH264)
{
	char filename[MAX_PATH];
	sprintf(filename,"%S",strFileName);

	m_nGopSize = nGopSize;
	m_nFrmCount = 0;
	m_nEncodeIndex = 0;
	m_nFrmFinishCount = 0;
	m_nFrmCountToSpeed = nFrmSpeed + int(1000.0 /FFMPEGFPS/2);
	if(m_nFrmCountToSpeed < 1000.0 /FFMPEGFPS)
		m_nFrmCountToSpeed = 1000.0 /FFMPEGFPS + 1;
	m_nFrmCountToSpeed = int((double)m_nFrmCountToSpeed / ((double)1000 /(double)FFMPEGFPS));
	
	int nRet = 0;
	/* find the mpeg1 video encoder */
	AVCodec *pCodec;
	AVOutputFormat* pOutputFormat;

	avformat_alloc_output_context2(&m_pFormatCtx, NULL, "mpegts", NULL);	
	if(!m_pFormatCtx) return FALSE;

	pOutputFormat = m_pFormatCtx->oformat;
	if(!m_pFormatCtx) return FALSE;

	pOutputFormat->video_codec = AV_CODEC_ID_H264;	
	if(pOutputFormat->video_codec == AV_CODEC_ID_NONE) return FALSE;
	
	pCodec = avcodec_find_encoder(pOutputFormat->video_codec);
	if(!pCodec) return FALSE;

	m_pVideoStream = avformat_new_stream(m_pFormatCtx, pCodec);
	if(!m_pVideoStream) return FALSE;

	m_pVideoStream->id = m_pFormatCtx->nb_streams - 1;
	m_pVideoStream->sample_aspect_ratio.den = 1;
	m_pVideoStream->sample_aspect_ratio.num= 1;
	m_pVideoStream->display_aspect_ratio.den = 16;
	m_pVideoStream->display_aspect_ratio.num = 9;

	m_pCodecCtx = m_pVideoStream->codec;
	if (!m_pCodecCtx) 
	{
		SendLog(0, _T("Could not allocate video pCodec context"));		
		return FALSE;
	}

    /* resolution must be a multiple of two */
    m_pCodecCtx->width = nWidth;
    m_pCodecCtx->height = nHeight;

	/* put sample parameters */
	//m_pCodecCtx->bit_rate = m_pCodecCtx->width * m_pCodecCtx->height * 40;
	m_pCodecCtx->codec_id = pOutputFormat->video_codec;
	m_pCodecCtx->bit_rate = nBitrate;//10240000;
    /* frames per second */
    m_pCodecCtx->time_base.num = 1;
	m_pCodecCtx->time_base.den = FFMPEGFPS;
	
	//hjcho GOP 1로 변경(170228)
    m_pCodecCtx->gop_size = nGopSize; /* emit one intra frame every ten frames */

	if( nGopSize >= 6 )
		m_pCodecCtx->max_b_frames = 2;
	else
		m_pCodecCtx->max_b_frames = 0;

    m_pCodecCtx->pix_fmt = AV_PIX_FMT_YUV420P;
	m_pCodecCtx->thread_type = FF_THREAD_SLICE;
	m_pCodecCtx->thread_count = 16;
	//m_pCodecCtx->codec_type = AVMEDIA_TYPE_VIDEO;

	if(m_pFormatCtx->oformat->flags & AVFMT_GLOBALHEADER)
		m_pCodecCtx->flags |= CODEC_FLAG_GLOBAL_HEADER;

	if(codec_id == AV_CODEC_ID_H264)
	{
		if(bHighQualityForH264 == FALSE)
			av_opt_set(m_pCodecCtx->priv_data, "preset", "ultrafast", 0);
		else if(bHighQualityForH264 == TRUE)
			av_opt_set(m_pCodecCtx->priv_data, "preset", "veryslow", 0);
	}

    /* open it */
    if (avcodec_open2(m_pCodecCtx, pCodec, NULL) < 0) 
	{
		SendLog(0, _T("Could not open codec"));
		delete[] filename;
        return FALSE;
    }
    m_pFrame = avcodec_alloc_frame();

    m_pFrame->format = m_pCodecCtx->pix_fmt;
    m_pFrame->width  = m_pCodecCtx->width;
    m_pFrame->height = m_pCodecCtx->height;
	
	if(m_pVideoStream)
	{
		avcodec_open2(m_pCodecCtx, pCodec, NULL);
		avio_open(&m_pFormatCtx->pb, filename, AVIO_FLAG_WRITE);
		avformat_write_header(m_pFormatCtx, NULL);
	} else return FALSE;

    /* the image can be allocated by any means and av_image_alloc() is
     * just the most convenient way if av_malloc() is to be used */
    nRet = av_image_alloc(m_pFrame->data, m_pFrame->linesize, m_pCodecCtx->width, m_pCodecCtx->height, m_pCodecCtx->pix_fmt, 32);

	return TRUE;
}

void FFmpegManager::EncodePushImageTS(BYTE* pImage, BOOL nIsEndFrame, BOOL bGPUUse)
{
	if((CheckCudaSupport()) && bGPUUse && bGPUUse != -100)
	{
		IplImage* img = (IplImage*)pImage;
		IplImage* cvImage;	
		cvImage =cvCreateImage(cvSize(m_pFrame->width, m_pFrame->height), IPL_DEPTH_8U, 3);
		memcpy(cvImage->imageData, img->imageData, m_pFrame->width * m_pFrame->height * 3);
		IplImageToAVFrame(cvImage, m_pFrame, m_pFrame->width, m_pFrame->height, AV_PIX_FMT_YUV420P);
		for( int i =0 ;i < m_nFrmCountToSpeed; i++)
			PushEncodeDataTS(m_pFrame);

		if( nIsEndFrame == TRUE)
			PushEncodeDataTS(m_pFrame);

		cvReleaseImage(&cvImage);
	}
	else
	{
		IplImage* cvImage;	
		cvImage =cvCreateImage(cvSize(m_pFrame->width, m_pFrame->height), IPL_DEPTH_8U, 3);
		memcpy(cvImage->imageData, pImage, m_pFrame->width * m_pFrame->height * 3);
		IplImageToAVFrame(cvImage, m_pFrame, m_pFrame->width, m_pFrame->height, AV_PIX_FMT_YUV420P);
		for( int i =0 ;i < m_nFrmCountToSpeed; i++)
		{
			PushEncodeDataTS(m_pFrame);
		}

		if( nIsEndFrame == TRUE)
			PushEncodeDataTS(m_pFrame);

		cvReleaseImage(&cvImage);
	}
}

void FFmpegManager::PushEncodeDataTS(AVFrame *pFrame)
{
	AVPacket Packet;
	int nRet = 0, nGotOutput = 0;
	av_init_packet(&Packet);
	Packet.data = NULL;    // packet data will be allocated by the encoder
	Packet.size = 0;
	fflush(stdout);
	pFrame->pts = m_nEncodeIndex++;
	/* encode the image */
	nRet = avcodec_encode_video2(m_pCodecCtx, &Packet, pFrame, &nGotOutput);
	if (nRet < 0)
	{
		return;//exit(1);
	}

	if (nGotOutput)
	{
		Packet.pts = av_rescale_q_rnd(
			Packet.pts,
			m_pCodecCtx->time_base,
			m_pVideoStream->time_base,
			AVRounding(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
		Packet.dts = av_rescale_q_rnd(
			Packet.dts,
			m_pCodecCtx->time_base,
			m_pVideoStream->time_base,
			AVRounding(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
		Packet.duration= av_rescale_q(
			Packet.duration,
			m_pCodecCtx->time_base,
			m_pVideoStream->time_base);

		av_write_frame(m_pFormatCtx, &Packet);
		av_free_packet(&Packet);
	}
	m_nFrmFinishCount = m_nEncodeIndex;
}

void FFmpegManager::CloseEncodeTS()
{
	avcodec_close(m_pCodecCtx);	m_pCodecCtx = NULL;
	avformat_close_input(&m_pFormatCtx); m_pFormatCtx = NULL;
}
int FFmpegManager::GetFrameCount(CString strPath)
{
	int nStart = GetTickCount();
	CT2CA pszConvertedAnsiString (strPath);
	std::string strDst(pszConvertedAnsiString);

	av_register_all();
	AVFormatContext* pFormatCtx = nullptr;
	AVCodecContext  *pCodecCtx;

	if(avformat_open_input(&pFormatCtx,strDst.c_str(),0,NULL) != 0)
	{
		return -1;
	}
	if(avformat_find_stream_info(pFormatCtx,NULL) < 0)
		return -1;

	int nVideoStream = -1;
	for(int i = 0 ; i < pFormatCtx->nb_streams; i++)
	{
		//pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO
		if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			nVideoStream = i;
			break;
		}
	}
	int nFrameCount = 0;
	nFrameCount = pFormatCtx->streams[nVideoStream]->nb_frames;

	/*pCodecCtx = pFormatCtx->streams[nVideoStream]->codec;
	pCodecCtx->thread_type = FF_THREAD_SLICE;
	pCodecCtx->thread_count = 16;

	int nWidth = pCodecCtx->width;
	int nHeight = pCodecCtx->height;*/

	CString strLog;
	//strLog.Format(_T("FFMPEG Time: %d(%d,%d)"),GetTickCount() - nStart,nWidth,nHeight);
	strLog.Format(_T("FFMPEG Time: %d"),GetTickCount() - nStart);
	
	//SendLog(5,strLog);

	//avcodec_close(pCodecCtx);
	avformat_close_input(&pFormatCtx);
	
	return nFrameCount;
}
BOOL FFmpegManager::GetFrameSize(CString strPath,int &nWidth,int &nHeight)
{
	int nStart = GetTickCount();
	CT2CA pszConvertedAnsiString (strPath);
	std::string strDst(pszConvertedAnsiString);

	av_register_all();
	AVFormatContext* pFormatCtx = nullptr;
	AVCodecContext  *pCodecCtx;

	if(avformat_open_input(&pFormatCtx,strDst.c_str(),0,NULL) != 0)
	{
		return FALSE;
	}
	if(avformat_find_stream_info(pFormatCtx,NULL) < 0)
		return FALSE;

	int nVideoStream = -1;
	for(int i = 0 ; i < pFormatCtx->nb_streams; i++)
	{
		//pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO
		if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			nVideoStream = i;
			break;
		}
	}
	int nFrameCount = 0;
	nFrameCount = pFormatCtx->streams[nVideoStream]->nb_frames;

	pCodecCtx = pFormatCtx->streams[nVideoStream]->codec;
	pCodecCtx->thread_type = FF_THREAD_SLICE;
	pCodecCtx->thread_count = 16;

	nWidth = pCodecCtx->width;
	nHeight = pCodecCtx->height;

	avcodec_close(pCodecCtx);
	avformat_close_input(&pFormatCtx);

	return TRUE;
}

void FFmpegManager::SetDevice(int nDevice)
{
	m_nDeviceType = nDevice;
}
int FFmpegManager::GetDevice()
{
	return m_nDeviceType;
}
void FFmpegManager::EncodePushUsingYUV(BYTE* Y,BYTE* U, BYTE* V,int nWidth,int nHeight)
{
	//AvFra
	/*struct SwsContext* img_convert_ctx = 0;
	int linesize[4] = {0, 0, 0, 0};
*/
	//AVFrame* _pFrame = av_frame_alloc();
/*
	m_pFrame->linesize[0] = nWidth*nHeight;
	m_pFrame->linesize[1] = nWidth*nHeight/4;
	m_pFrame->linesize[2] = nWidth*nHeight/4;*/

	memcpy(m_pFrame->data[0],Y,nWidth*nHeight);
	memcpy(m_pFrame->data[1],U,nWidth*nHeight/4);
	memcpy(m_pFrame->data[2],V,nWidth*nHeight/4);

	/*m_pFrame->data[0] = Y.data;
	m_pFrame->data[1] = U.data;
	m_pFrame->data[2] = V.data;*/
	if(m_pFrame)
		PushEncodeData(m_pFrame);
/*
	if (img_convert_ctx != 0)
	{
		linesize[0] = 3 * nWidth;
		sws_scale(img_convert_ctx, (const uint8_t *const*)(&(iplImage->imageData)), linesize, 0, iplImage->height, avFrame->data, avFrame->linesize);
		sws_freeContext(img_convert_ctx);
	}*/
}