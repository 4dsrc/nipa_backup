////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieMgr.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-22
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include <Afxtempl.h>
#include "ESMMovie.h"
#include "ESMDefine.h"
#include "ESMMovieThread.h"
#include "ESMMovieData.h"
#include "ESMIndex.h"
#include "ESMMovieObject.h"
#include "ESMMovieInfoArray.h"
#include "ESMMovieKZoneMgr.h"
#include "SdiDefines.h"

#include <vector>
using namespace std;


class AFX_EXT_CLASS CESMMovieMgr : public CWinThread
{
	DECLARE_DYNCREATE(CESMMovieMgr)
public:
	CESMMovieMgr(void) {};
	CESMMovieMgr(HWND hParentFrm);
	~CESMMovieMgr(void);

public:	
	virtual int Run(void);	

protected:
	DECLARE_MESSAGE_MAP()

	virtual BOOL InitInstance();
	virtual int ExitInstance();	
	void StopThread();
	void EnterCS()	{ EnterCriticalSection (&m_CS);	}
	void LeaveCS()	{ LeaveCriticalSection (&m_CS);	}

	void RemoveAll();

private:
	CRITICAL_SECTION m_CS;
	HWND	m_hParentWnd;
	BOOL	m_bThreadWork;	//-- Is Working?
	BOOL	m_bThreadRun;	//-- Is Finish?
	CObArray	m_arMsg;
	CObArray	m_arThread;
	CString m_strSaveFolderPath;
	vector<ESMMovieObject*> m_pMovieArray;
	ESMMovieInfoArray m_MovieInfoArray;
	int		m_nMarginX, m_nMarginY;
	BOOL	m_bMakingStop;
	int		m_nMovieSize;
	BOOL	m_bUHDtoFHD;
	BOOL	m_bVMCCFlag;
	//wgkim 170727
	BOOL	m_bLightWeight;
	BOOL	m_bViewInfo;

	//wgkim 180629
	int		m_nFrameRateIndex;

	//-- 2014-05-27 hongsu@esmlab.com
	//-- CUDA Memory
public:
	HANDLE hSemaphore;
	BOOL CheckMovieEvent(ESMMovieMsg* pMsg);
	BOOL ParceMessage(short& nMsg, char& nType, char& nMessage);
	void TerminateThread(ESMMovieMsg* pMsg);

	//1. Dsc갯수 설정
	void SetDscCount(int nDscCount);							// 최초 Dsc Count 설정
	//2. Movie Path 설정
	void GetMoviePath(CString strMovieId, CString strMoviePath);	// Movie 순서대로 설정 
	CString GetMoviePath(CString strMovieId);	
	void SetMoviePath(CString strMovieId, CString strMoviePath);


	void AddMovieObject(CString strMovieName);

	void ClearMovie();
	//int GetMovieCount(){ return m_nDscCount; } 
	HWND GetParentWnd(){ return m_hParentWnd; }
	ESMMovieData* GetMovieData(CString MovieName, int nIndex);
	ESMMovieData* GetMovieData(CString MovieName, CString strMovieID);
	void DeleteMovie(CString strMovieName);
	void DeleteMovie();

	stMovieInfo GetMovieInfo(CString strMovieId);
	void SetMovieInfo(stMovieInfo MovieInfo);
	void ReSetMovieInfo();
	stAdjustInfo GetAdjustData(CString strMovieID);
	void SetAdjustData(CString strMovieID, stAdjustInfo* AdjustData);
	void SetMargin(int nMx ,int nMy );
	void SetSaveFolderPath(CString strPath){ m_strSaveFolderPath = strPath; }
	CString GetSaveFolderPath(){ return m_strSaveFolderPath; }
	void GetMovieIndex(int nIndex, int& nMovieIndex, int& nRealFrameIndex, int nMoiveSize = -1, int nFrameRate = 30);
	void SetMovieSize(int nMovieSize );
	int GetMovieSize();
	void GetMovieSizeData(int& nSrcWidth, int& nSrcHeight, int& nOutputWidth, int& nOutputHeight);
	void SetUHDtoFHD(int nFlag );
	void SetVMCCFlag(int nFlag );
	BOOL GetVMCCFlag();
	BOOL GetUHDtoFHD();
	void SetMakingStop(BOOL bMakingStop){m_bMakingStop = bMakingStop;}
	BOOL GetMakingStop(){return m_bMakingStop;}

	//wgkim 170727
	void SetLightWeight(int nFlag);
	BOOL GetLightWeight();

	//wgkim 180629
	void SetFrameRateIndex(int nFlag);
	int GetFrameRateIndex();

	void SetViewInfo(int nFlag);
	BOOL GetViewInfo();

	void AddEvent(ESMMovieMsg* pMsg);
	void AddThread(CESMMovieThread* pESMMovieThread);

	int GetMarginX(){ return m_MovieInfoArray.GetMaginX(); };
	int GetMarginY(){ return m_MovieInfoArray.GetMaginY(); };

	//------------------------------------------------------------------------------
	//! @function			
	//! @brief				
	//! @date		2014-09-17
	//! @owner		Hongsu Jung (hongsu@esmlab.com)
	//! @return			
	//! @revision		
	//------------------------------------------------------------------------------	
private:
	CObArray	m_arCorrectFrame;

	//181010 wgkim
	CESMMovieKZoneMgr m_pMovieKZoneMgr;

public:
	void AddValidFrame(ValidFrame* pValidFrame);
	ValidFrame* GetPrevFrame(CString strID, int nIndex);
	int CheckValidIndex(CString strID, int nFrame);	
	void RemoveAllValid();

	int GetMovieCnt(){return m_arThread.GetSize();};

	//170420 hjcho: GPU 동시 실행 막기용
	BOOL m_bGPULoad;
	BOOL GetGPUState(){return m_bGPULoad;}
	void SetGPUState(BOOL bS){m_bGPULoad = bS;}

	void SetFrameRate(int nRate);
	int m_nFrameRate;

	void SetDeviceModel(SDI_MODEL model);
	SDI_MODEL GetDeviceModel();
	SDI_MODEL m_model;

	int GetMovieFrameCount(int nRate);

	//181010 wgkim
	CESMMovieKZoneMgr* GetMovieKZoneMgr();
};
