#pragma once
#include "stdafx.h"
#include <vector>
#include "ESMDefine.h"
#include "FFmpegManager.h"
#include "ESMMovieThread.h"
#include "ESMMovieThreadImage.h"
#include "StopWatch.h"

#include "LiveMgr\FFMPEGClass.h"
#include "libavcodec\avcodec.h"
#include "ESMMovieLiveDefine.h"
using namespace std;

struct stLiveFrameData
{
	stLiveFrameData()
	{
		nFinish = -100;
	}
	Mat frame;
	int nFinish;
};
/*
struct stLiveData
{
	stLiveData()
	{
		nState = LIVE_WAIT;
		nSecIdx = -1;
		nTotalFrame = -1;
		mArrImage = NULL;
	}
	int nState;
	int nSecIdx;
	int nTotalFrame;
	vector<Mat>*mArrImage;
	CString strPath;
	CString strFileName;
};
*/

class AFX_EXT_CLASS CESMMovieRTLiveMgr
{
public:
	CESMMovieRTLiveMgr();
	~CESMMovieRTLiveMgr();

	//OpenClose
	void SetMovieState(BOOL b);//{m_bMovieState = b;}
	BOOL GetMovieState(){return m_bMovieState;}
	
	//Finish Data
	void SetLiveData(int nState,int nSecIdx,int nTotalFrame);
	void ReceiveLiveData(int nState,int nSecIdx,int nTotalFrame,CString strPath,CString strFileName);
	void DecodingFileData(int nSecIdx);
	CString GetInIpAddress();

	//Management
	int GetClock();
	CStopWatch* m_pClock;
	void SetClock(CStopWatch* stopwatch){m_pClock = stopwatch;}
	void LaunchLiveMgr(int nWidth,int nHeight);
	static unsigned WINAPI _LiveMgr(LPVOID param);
	void LaunchSyncTimeMgr();
	static unsigned WINAPI _SyncTimeMgr(LPVOID param);

	void SetThreadStop(BOOL b){m_bThreadStop = b;}
	BOOL GetThreadStop(){return m_bThreadStop;}

	//About map data
	void ClearLiveData(){m_mapLiveData.clear();}

	//Setup RTSP Server
	FFMPEG * mFFMPEG;//[MULTICASE_MAX];
	double PCFreq;
	__int64 CounterStart;
	void StartCounter();
	double GetCounter();
	void SetLiveStart(BOOL b){m_bLiveStart = b;}
	BOOL GetLiveStart(){return m_bLiveStart;}

	void SetSyncTime(int nTime){m_nSyncTime = nTime;}
	int  GetSyncTime(){return m_nSyncTime;}

	void SetESMTick(int nTick){m_nESMTick = nTick;}
	int	 GetESMTick(){return m_nESMTick;}

	void Set4DMName(CString strName){m_str4DMName = strName;}
	CString Get4DMName(){return m_str4DMName;}
private:
	//std::map<int,stLiveData>m_mapLiveData;
	BOOL m_bMovieState;
	BOOL m_bThreadStop;
	BOOL m_bLiveStart;
	int	 m_nSyncTime;
	int	 m_nESMTick;
	CString m_str4DMName;
	int m_nWidth;
	int m_nHeight;
};