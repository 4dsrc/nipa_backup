////////////////////////////////////////////////////////////////////////////////
//
//	ESMIni.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMMovieIni.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CESMMovieIni::CESMMovieIni()
{
	m_strIniFilename.Empty();
}

CESMMovieIni::CESMMovieIni(LPCTSTR ESMIniFilename)
{
	if(!SetIniFilename( ESMIniFilename ))
	{
		//ESMLog(0,_T("Ini File Name"));
	}
}

CESMMovieIni::~CESMMovieIni()
{
   // Flush .ini file
   ::WritePrivateProfileString( NULL, NULL, NULL, m_strIniFilename );
}


//////////////////////////////////////////////////////////////////////
// Methods
//////////////////////////////////////////////////////////////////////

#define MAX_INI_BUFFER 1048576   // Defines the maximum number of chars we can
								// read from the ini file 


//--
//-- DELETE PREVIOUS FILE
//-- 2008-07-24
//-- 
#include "ESMMovieFileOperation.h"

BOOL CESMMovieIni::SetIniFilename(LPCTSTR strFile, BOOL bReset)
{
	ASSERT(AfxIsValidString(strFile));
	m_strIniFilename = strFile;
	if( m_strIniFilename.IsEmpty() ) 
	   return FALSE;

	//-- Check File Exist
	CESMMovieFileOperation fo;
	if(!fo.IsFileExist(strFile))
		return FALSE;

	//--
	//-- DELETE PREVIOUS FILE
	//-- 2008-07-24
	//-- 
	if(bReset)
	{
	   CESMMovieFileOperation fo;
	   fo.Delete(strFile);
	}

	return TRUE;
};


UINT CESMMovieIni::GetInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault)
{
   ASSERT(AfxIsValidString(lpszSection));
   ASSERT(AfxIsValidString(lpszEntry));
   if( m_strIniFilename.IsEmpty() ) return 0; // error
   CString sDefault;
   sDefault.Format( _T("%d"), nDefault );
   CString s = GetString( lpszSection, lpszEntry, sDefault );
   return _ttol( s );
};

CString CESMMovieIni::GetString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszDefault)
{
   ASSERT(AfxIsValidString(lpszSection));
   ASSERT(AfxIsValidString(lpszEntry));
   if( m_strIniFilename.IsEmpty() ) return CString();
   //TCHAR pBuffer[2048] = {0};
   TCHAR* pBuffer = new TCHAR[2048];
   CString strSection = lpszSection;
   CString strEntry = lpszEntry;
   CString strDefault = lpszDefault;
   long ret = ::GetPrivateProfileString( strSection.GetBuffer(), strEntry.GetBuffer(), strDefault.GetBuffer(), pBuffer, 2047, m_strIniFilename );
   strSection.ReleaseBuffer();
   strEntry.ReleaseBuffer();
   strDefault.ReleaseBuffer();

   CString s = pBuffer;
    delete pBuffer;

   if( ret==0 ) return CString(lpszDefault);
   return s;
};

CString CESMMovieIni::GetStringSection(LPCTSTR lpszSection)
{
	TCHAR buff[10240]={0};
	
	memset(buff, 0x00, 10240);
	ASSERT(AfxIsValidString(lpszSection));
	if( m_strIniFilename.IsEmpty() ) return CString();
	CString s;
	CString strTemp;

	long ret = ::GetPrivateProfileSection(lpszSection, (LPTSTR)buff, 10239, m_strIniFilename );
	
	if( ret==0 ) return CString(lpszSection);

	// buffer out of access
	//for(int i=0 ; i<10239 ; i++)
	for(int i=0 ; i<10237 ; i++)
	{
		if(buff[i] == NULL){
			s.Append(_T("~"));
		}else{
			strTemp.Format(_T("%c"),buff[i]);
			s.Append(strTemp);
		}

		if((buff[i+1]=='=' && buff[i+2]=='=') || (buff[i+1] == NULL && buff[i+2] == NULL))
		{
			return s;
		}
	}

	return s;
};

CString CESMMovieIni::GetStringValuetoKey(LPCTSTR lpszSection, LPCTSTR lpszEntry)
{
	int nPos = 0;
	CString strSection;
	CString strToken;
	CString strEntry;
	CString strTemp;

	ASSERT(AfxIsValidString(lpszSection));
	ASSERT(AfxIsValidString(lpszEntry));

	strSection = GetStringSection(lpszSection);
	strEntry = _T("=");
	strEntry.Append(lpszEntry);
	
	CString strTpValue, sLine;
	while((strToken = strSection.Tokenize(_T("~"),nPos)) != "")
	{

		AfxExtractSubString(strTpValue, strToken, 1, '=');
		if( strTpValue == lpszEntry)
		{
			AfxExtractSubString(strTpValue, strToken, 0, '=');
			strToken = strTpValue;
			return strToken;
		}
	}
	
	strToken.Empty();
	return strToken;
};
CString CESMMovieIni::GetNextStringValue(LPCTSTR lpszSection,LPCTSTR lpszEntry)
{
	int nPos = 0, nIndex = 0;
	CString strSection;
	CString strToken;
	CString strEntry;
	CString strTemp;
	bool flag;
	flag = false;

	ASSERT(AfxIsValidString(lpszSection));
	ASSERT(AfxIsValidString(lpszEntry));

	strSection = GetStringSection(lpszSection);
	strEntry = _T("=");
	strEntry.Append(lpszEntry);

	CString strTpValue, sLine;
	while((strToken = strSection.Tokenize(_T("~"),nPos)) != "")
	{
		if(flag)
		{
			AfxExtractSubString(strTpValue, strToken, 1, '=');
			return strTpValue;
		}

		AfxExtractSubString(strTpValue, strToken, 1, '=');
		if( strTpValue == lpszEntry)
		{
			AfxExtractSubString(strTpValue, strToken, 0, '=');
			flag = true;
		}
		nIndex++;
	}



	strToken.Empty();
	return strTpValue;
};
CString CESMMovieIni::GetBeforeStringValue(LPCTSTR lpszSection,LPCTSTR lpszEntry)
{
	int nPos = 0, nIndex = 0;
	CString strSection;
	CString strToken;
	CString strEntry;
	CString strTemp;
	bool flag;
	flag = false;

	ASSERT(AfxIsValidString(lpszSection));
	ASSERT(AfxIsValidString(lpszEntry));

	strSection = GetStringSection(lpszSection);
	strEntry = _T("=");
	strEntry.Append(lpszEntry);
	
	CString strTpValue, strValue;
	while((strToken = strSection.Tokenize(_T("~"),nPos)) != "")
	{
		
		AfxExtractSubString(strTpValue, strToken, 1, '=');
		if( strTpValue == lpszEntry)
		{
			AfxExtractSubString(strTpValue, strToken, 0, '=');
			flag = true;
		}
		if(flag)
		{
			return strValue;
		}
		strValue = strTpValue;

		nIndex++;
	}



	strToken.Empty();
	return strValue;
};
 
int CESMMovieIni::GetStringValuetoKeyIndex(LPCTSTR lpszSection, LPCTSTR lpszEntry)
{
	int nPos = 0, nIndex = 0;
	CString strSection;
	CString strToken;
	CString strEntry;
	CString strTemp;

	ASSERT(AfxIsValidString(lpszSection));
	ASSERT(AfxIsValidString(lpszEntry));

	strSection = GetStringSection(lpszSection);
	strEntry = _T("=");
	strEntry.Append(lpszEntry);
	
	CString strTpValue, sLine;
	while((strToken = strSection.Tokenize(_T("~"),nPos)) != "")
	{

		AfxExtractSubString(strTpValue, strToken, 1, '=');
		if( strTpValue == lpszEntry)
		{
			AfxExtractSubString(strTpValue, strToken, 0, '=');
			return nIndex;
		}
		nIndex++;
	}
	
	strToken.Empty();
	return nIndex;
};
//-------------------------------------------------------------
//-- Get Count Items
//-------------------------------------------------------------
int CESMMovieIni::GetSectionCount(LPCTSTR lpszSection)
{
	int nPos = 0;
	int nCnt = 0;
	CString strSection;
	CString strToken;
	CString strEntry;
	CString strTemp;

	ASSERT(AfxIsValidString(lpszSection));
	strSection = GetStringSection(lpszSection);

	if( strSection == lpszSection)
		return 0;

	while((strToken = strSection.Tokenize(_T("~"),nPos)) != "")
		nCnt++;
	
	return nCnt;
};

void CESMMovieIni::GetSectionList(LPCTSTR lpszSection, CStringArray& arList)
{
	int nPos = 0, nPos1;
	CString strSection;
	CString strToken;
	CString strEntry;
	CString strTemp;

	ASSERT(AfxIsValidString(lpszSection));
	strSection = GetStringSection(lpszSection);

	while((strToken = strSection.Tokenize(_T("~"),nPos)) != "")
	{
		nPos1 = 0;
		strToken = strToken.Tokenize(_T("="),nPos1);
		strToken.Replace(_T(" "), _T(""));
		arList.Add(strToken);
	}
};

//-------------------------------------------------------------
//-- 12.07.13 cy.gil 
//-- Get Key and Value Items List
// ex) KEY=VALUE1;VALE2;VALUE3
//-------------------------------------------------------------
void CESMMovieIni::GetSectionLineList(LPCTSTR lpszSection, CStringArray& arList)
{
	int nPos = 0, nPos1;
	CString strSection;
	CString strToken;
	CString strEntry;
	CString strTemp;

	ASSERT(AfxIsValidString(lpszSection));
	strSection = GetStringSection(lpszSection);

	while((strToken = strSection.Tokenize(_T("~"),nPos)) != "")
	{
		nPos1 = 0;
		strToken.Replace(_T(" "), _T(""));
		arList.Add(strToken);
	}
};


BOOL CESMMovieIni::GetBoolean(LPCTSTR lpszSection, LPCTSTR lpszEntry, BOOL bDefault)
{
   CString s = GetString(lpszSection,lpszEntry);
   if( s.IsEmpty() ) return bDefault;
   TCHAR c = (TCHAR)_totupper( s[0] );
   switch( c ) {
   case _T('Y'): // YES
   case _T('1'): // 1 (binary)
   case _T('O'): // OK
      return TRUE;
   default:
      return FALSE;
   };
};


BOOL CESMMovieIni::WriteInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nValue)
{
   ASSERT(AfxIsValidString(lpszSection));
   ASSERT(AfxIsValidString(lpszEntry));
   CString s;
   s.Format( _T("%d"), nValue );
   return WriteString( lpszSection, lpszEntry, s );
};

BOOL CESMMovieIni::WriteIntAdd(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nValue)
{
   ASSERT(AfxIsValidString(lpszSection));
   ASSERT(AfxIsValidString(lpszEntry));

   CString strGet;
   strGet = GetString(lpszSection, lpszEntry);

   CString s;
   if(strGet == "")
		s.Format( _T("%d"), nValue );
   else
		s.Format( _T(",%d"), nValue );

   strGet.Append(s);
	
   return WriteString( lpszSection, lpszEntry, strGet );
};

BOOL CESMMovieIni::WriteBoolean(LPCTSTR lpszSection, LPCTSTR lpszEntry, BOOL bValue)
{
   CString s;
   bValue ? s=_T("Y") : s=_T("N");
   return WriteString( lpszSection, lpszEntry, s );
};

BOOL CESMMovieIni::WriteTrueFalse(LPCTSTR lpszSection, LPCTSTR lpszEntry, BOOL bValue)
{
   CString s;
   bValue ? s=_T("True") : s=_T("False");
   return WriteString( lpszSection, lpszEntry, s );
};

BOOL CESMMovieIni::WriteString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszValue)
{
   ASSERT(AfxIsValidString(lpszSection));
   ASSERT(AfxIsValidString(lpszEntry));
   if( m_strIniFilename.IsEmpty() ) 
	   return FALSE;
   return ::WritePrivateProfileString( lpszSection, lpszEntry, lpszValue, m_strIniFilename );
};


BOOL CESMMovieIni::DeleteKey(LPCTSTR lpszSection, LPCTSTR lpszEntry)
{
   ASSERT(AfxIsValidString(lpszSection));
   ASSERT(AfxIsValidString(lpszEntry));
   if( m_strIniFilename.IsEmpty() ) 
	   return FALSE;
   return ::WritePrivateProfileString( lpszSection, lpszEntry, NULL, m_strIniFilename );
};

BOOL CESMMovieIni::DeleteSection(LPCTSTR lpszSection)
{
   ASSERT(AfxIsValidString(lpszSection));
   if( m_strIniFilename.IsEmpty() ) return FALSE;
   return ::WritePrivateProfileString( lpszSection, NULL, NULL, m_strIniFilename );
};


//------------------------------------------------------------------------------
//! @function			
//! @brief		Get all section names from an ini file
//! @date		2013-04-27
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @revision		
//------------------------------------------------------------------------------
#define DEF_PROFILE_THRESHOLD	512 // temporary string length

void CESMMovieIni::GetSectionNames(CStringArray *pArray) const
{
	if (pArray != NULL)
		pArray->RemoveAll();

	const DWORD LEN = GetSectionNames(NULL, 0);
	if (LEN == 0)
		return;

	LPTSTR psz = new TCHAR[LEN + 1];
	GetSectionNames(psz, LEN);
	ParseDNTString(psz, __SubStrAdd, pArray);
	delete [] psz;
	psz= NULL;
}


// Get all section names from an ini file
DWORD CESMMovieIni::GetSectionNames(LPTSTR lpBuffer, DWORD dwBufSize) const
{
	if (lpBuffer == NULL)
	{
		// just calculate the required buffer size
		DWORD dwLen = DEF_PROFILE_THRESHOLD;
		LPTSTR psz = new TCHAR[dwLen + 1];
		DWORD dwCopied = ::GetPrivateProfileSectionNames(psz, dwLen, m_strIniFilename);
		while (dwCopied + 2 >= dwLen)
		{
			dwLen += DEF_PROFILE_THRESHOLD;
			delete [] psz;
			psz = NULL;
			psz = new TCHAR[dwLen + 1];
			dwCopied = ::GetPrivateProfileSectionNames(psz, dwLen, m_strIniFilename);
		}

		delete [] psz;
		psz = NULL;
		return dwCopied + 2;
	}
	else
	{
		return ::GetPrivateProfileSectionNames(lpBuffer, dwBufSize, m_strIniFilename);
	}
}


// Parse a "double null terminated string", pass each sub string to a user-defined
// callback function
BOOL CESMMovieIni::ParseDNTString(LPCTSTR lpString, SUBSTRPROC lpFnStrProc, LPVOID lpParam)
{
	if (lpString == NULL || lpFnStrProc == NULL)
		return FALSE;

	LPCTSTR p = lpString;
	DWORD dwLen = (DWORD)_tcslen(p);

	while (dwLen > 0)
	{
		if (!lpFnStrProc(p, lpParam))
			return FALSE;

		p = &p[dwLen + 1];
		dwLen = (DWORD)_tcslen(p);
	}
	return TRUE;
}

BOOL CALLBACK CESMMovieIni::__SubStrAdd(LPCTSTR lpString, LPVOID lpParam)
{
	CStringArray* pArray = (CStringArray*)lpParam;
	if (pArray == NULL || lpString == NULL)
		return FALSE;

	pArray->Add(lpString);
	return TRUE;
}