#pragma once
#include "ESMDefine.h"
#include "ESMMovieDefine.h"
#include "opencv2/opencv.hpp"   
#include "opencv2/core/cuda.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/cudaarithm.hpp"
#include "opencv2/cudaimgproc.hpp"
using namespace cv;

class ESMMovieData
{
public:
	ESMMovieData(void);
	~ESMMovieData(void);

	void InitData(int nFrameCount, int nWidth, int nHeight, int nChannel);
	BOOL SetImage(int nIndex, BYTE* pImage);
	BOOL SetImage(int nIndex,  Mat srcImage);
	BOOL SetImage(int nIndex,  cuda::GpuMat* src);
	void GetImage(Mat* srcImage, int nIndex);
	cuda::GpuMat* GetImage(int nIndex);
	void DeleteBuffer();
	void DeleteImage(int nFramePos);

	int GetSizeWidth();
	int GetSizeHight();
	void SetMoviePath(CString strMoviePath);
	CString GetMoviePath();
	void SetDscId(CString strMovieId);
	CString GetDscId();
	int GetFrameState(int nFrame);
	int SetFrameState(int nFrame, int nState);
	void OrderAdd(int nFrame);
	stAdjustInfo GetAdjustinfo();
	void SetAdjustinfo(stAdjustInfo* AdjustData);
	int GetBufferSize() { return m_nFrameCount;}

private:
	CudaFrameData** m_pFrameData;
	EffectInfo** m_pEffectData;

	int m_nFrameState;
	int m_nWidth;
	int m_nHeight;
	int m_nChannel;
	int m_nFrameCount;
	int m_nLoadedFrameCount;
	CString m_strMoviePath;
	CString m_strMovieID;
	stAdjustInfo m_AdjustData;
};

