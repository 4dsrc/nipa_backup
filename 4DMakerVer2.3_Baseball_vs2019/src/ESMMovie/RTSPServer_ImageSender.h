#pragma once
#include <afx.h>
#include <WinSock2.h>
#include <process.h>
#include "define.h"

#include "opencv2/opencv.hpp"

extern "C"
{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <libavutil/opt.h>
#include <libavutil/mathematics.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/imgutils.h>
#include <libavcodec/avcodec.h>
};

#include "DllFunc.h"
//#include "CARMScrambler.h"
using namespace cv;

struct FRAME_PACKET 
{
	FRAME_PACKET()
	{
		pPacket		= 0;
		nSize		= -1;
		nSecIdx		= -1;
		nFrameIdx	= -1;
	}
	unsigned char* pPacket;
	int nSize;
	int nSecIdx;
	int nFrameIdx;
};
struct FRAME_PACKET_INFO
{
	FRAME_PACKET_INFO()
	{
		nSecIdx = -1;
		nFrameIdx = -1;
		nDSCIdx = 0;
		nStopIdx = 0;
	}
	cv::Mat img;
	cv::Mat Y;
	cv::Mat U;
	cv::Mat V;
	int nSecIdx;
	int nFrameIdx;
	int nDSCIdx;
	int nStopIdx;
	CString str4DM;
	MR_INFO mr_info;
};
class CRTSPServerImageSender
{
public:
	CRTSPServerImageSender(RTSP_PLAY_METHOD method,CString strIP,int nPort,int nFrameRate = 30);
	~CRTSPServerImageSender();

	/*Thread*/
	void SetThreadStop(BOOL b){m_bThreadStop = b;}
	BOOL GetThreadStop(){return m_bThreadStop;}

	/*Encoder*/
	void SetEncodeParameter(int nFrameRate);
	void InitEncode(int codec_id);

	/*Frame Sender*/
	void LaunchFrameSender();
	static unsigned WINAPI _LaunchFrameSender(LPVOID param);

	/*Copy AVFrame & Packetizing Image */
	void WriteFrame(unsigned char* RGBFrame,int nWidth,int nHeight,int nSecIdx,int nFrameIdx,int nDSCIndex);
	void WriteFrame(unsigned char* Y,unsigned char* U,unsigned char* V,int nWidth,int nHeight,int nSecIdx,int nFrameIdx,int nDSCIndex,MR_INFO mr_info=MR_INFO());
	void WriteFrame(int nSecIdx, int nFrameIdx, int nDSCIndex, MR_INFO mr_info=MR_INFO());
	void PacketizeSubData(unsigned char* pPacket,int nStart,int nEnd,int nSecIdx,int nFrameIdx,int nDSCIndex,MR_INFO mr_info);
	void PacketizeEncodeFrame(unsigned char* pPacket,int nStart,int nEnd,int nSecIdx,int nFrameIdx,int nDSCIndex,MR_INFO mr_info,BOOL bEnd = FALSE);
	void AddPacketArray(unsigned char* pPacket,int nStart,int nEnd,int nSecIdx,int nFrameIdx,BOOL bEnd = FALSE);
	RTSP_PACKETIZE_INFO GetPacket(int nIdx);
	void SetLastIdx(int n){m_nLastPacket = n;}
	void SetCurIdx(int n){m_nCurPacket = n;}
	
	int GetLastIdx(){return m_nLastPacket;}
	int GetCurIdx(){return m_nCurPacket;}

	/*Movie Encode Info*/
	void SetSPS(CString str){m_strSPS = str;}
	CString GetSPS(){return m_strSPS;}
	void SetPPS(CString str){m_strPPS = str;}
	CString GetPPS(){return m_strPPS;}
	void CreateSDPInfo();
	void SetSDP(CString str){m_strSDP = str;}
	CString GetSDP(){return m_strSDP;}
	int MakeRTPHeader(unsigned char* xbuf,char naltype,int nSize,int nMakeMethod,int nSecIdx,int nFrameIdx,int nDSCIndex);
	int MakeRTPHeader(unsigned char* xbuf,BOOL bEnd = FALSE,BOOL bFirst = FALSE);
	int MakeRTPExtendHeader(unsigned char*xbuf,int nIdx,int nSecIdx,int nFrameIdx,int nDSCIndex,MR_INFO mr_info=MR_INFO());
	CString GetUTCTime();
	/*Sync*/
	void SetSyncStart(BOOL b){m_bSyncStart = b;}
	BOOL GetSyncStart(){return m_bSyncStart;}

	/**/
	cv::Size GetImageSize()
	{
		cv::Size sz(m_nWidth,m_nHeight);
		return sz;
	}
	void SetWait(DWORD dwMillisec);
	void SetDSCIdx(int n){m_nDSCIndex = n;}
	int GetDSCIdx(){return m_nDSCIndex;}
	void SetSendReady(BOOL b){m_bReady = b;}
	BOOL GetSendReady(){return m_bReady;}

	//RTSPServer_ImageSender.h
   void SetStopTime(int n)
   {
	   EnterCriticalSection(&m_criStopTime);
	   m_nSendFinishIdx = n;
	   LeaveCriticalSection(&m_criStopTime);
	   CString strLog;
	   strLog.Format(_T("Set SendTime: %d"),m_nSendFinishIdx);
	   SendLog(5,strLog);
   }
   
   int GetStopTime()
   {
	   EnterCriticalSection(&m_criStopTime);
	   int nSec = m_nSendFinishIdx;
	   LeaveCriticalSection(&m_criStopTime);
	   
	   return nSec;
   }
   CRITICAL_SECTION m_criStopTime;
private:
	RTSP_PLAY_METHOD m_RTSPPlayMethod;
	BOOL m_bThreadStop;

	//FFMPEG Management
	BOOL m_bSetEncode;
	int m_sws_flags;
	AVStream *m_video_st;
	AVCodec *m_video_codec;
	AVCodecContext *m_c;
	AVFrame* m_frame;
	AVPacket pkt;
	void *sws_ctx_void;
	AVFormatContext* m_pFormatCtx;
	//Movie Info
	int m_nWidth;
	int m_nHeight;
	int m_nBitrate;
	int m_nFrameRate;
	int m_nGOP;
	int m_nPayloadSize;
	int m_nFrameCount;

	//Encoding Info
	CString m_strSDP;
	CString m_strPPS;
	CString m_strSPS;

	//Server Info
	CString m_strIP;
	int m_nPort;

	//Movie
	int m_nCurPacket;
	int m_nLastPacket;
	int m_nPacketLoc;
	RTSP_PACKETIZE_INFO* m_pArrPacketizeInfo;
	int m_nSequenceNum;
	int m_nRepeatCnt;
	int m_nTimeStamp;

	//Image Management
	std::vector<FRAME_PACKET_INFO>m_ArrFrameYUV;
	CRITICAL_SECTION m_criImage;

	//Sync Start
	BOOL m_bSyncStart;
	
	//Extend information
	int m_nDSCIndex;

	BOOL m_bReady;

	//DRM
	int m_nErrorCode;
	int m_nDisk;
	BOOL bLoad;

	//FrameCount
	int m_nSendFrameCount;
	int m_nSendRepeatCount;

	//SendStopIdx
	int m_nSendFinishIdx;
public: 
	/*Inline func - Image input*/
	void InsertFrameImage(FRAME_PACKET_INFO info)
	{
		EnterCriticalSection(&m_criImage);
		m_ArrFrameYUV.push_back(info);
		LeaveCriticalSection(&m_criImage);
	}
	FRAME_PACKET_INFO GetFrameImage()
	{
		FRAME_PACKET_INFO info;
		EnterCriticalSection(&m_criImage);
		info = m_ArrFrameYUV.at(0);
		LeaveCriticalSection(&m_criImage);

		return info;
	}
	int GetFrameImageSize()
	{
		int nSize = 0;
		EnterCriticalSection(&m_criImage);
		nSize = m_ArrFrameYUV.size();
		LeaveCriticalSection(&m_criImage);
		return nSize;
	}
	void DeleteFrameImage()
	{
		EnterCriticalSection(&m_criImage);
		m_ArrFrameYUV.at(0).Y.release();
		m_ArrFrameYUV.at(0).U.release();
		m_ArrFrameYUV.at(0).V.release();
		m_ArrFrameYUV.erase(m_ArrFrameYUV.begin());
		LeaveCriticalSection(&m_criImage);
	}

	void MemoryTest(unsigned char*src,int nSrcSize,int nSrcStart,unsigned char*dst,int nDstStart);

	double GetCounter()
	{
		LARGE_INTEGER li;
		QueryPerformanceCounter(&li);
		return double(li.QuadPart-CounterStart)/PCFreq;
	}
	void StartCounter()
	{
		PCFreq = 0.0;
		CounterStart  = 0;

		LARGE_INTEGER li;
		if(!QueryPerformanceFrequency(&li))
			cout << "QueryPerformanceFrequency failed!\n";

		PCFreq = double(li.QuadPart)/1000.0;

		QueryPerformanceCounter(&li);
		CounterStart = li.QuadPart;
	}
	double GetProcessCounter()
	{
		LARGE_INTEGER li;
		QueryPerformanceCounter(&li);
		return double(li.QuadPart-ProcCounterStart)/ProcPCFreq;
	}
	void StartProcessCounter()
	{
		ProcPCFreq = 0.0;
		ProcCounterStart  = 0;

		LARGE_INTEGER li;
		if(!QueryPerformanceFrequency(&li))
			cout << "QueryPerformanceFrequency failed!\n";

		ProcPCFreq = double(li.QuadPart)/1000.0;

		QueryPerformanceCounter(&li);
		ProcCounterStart = li.QuadPart;
	}
	void SendRTSPStateMessage(FRAME_PACKET_INFO info,BOOL bFrame = FALSE);

	void SetMainWnd(CWnd* pWnd){m_pWnd = pWnd;}
	CWnd* GetMainWnd(){return m_pWnd;}
private:
	__int64 CounterStart;
	double PCFreq;

	__int64 ProcCounterStart;
	double ProcPCFreq;
	CWnd* m_pWnd;
};