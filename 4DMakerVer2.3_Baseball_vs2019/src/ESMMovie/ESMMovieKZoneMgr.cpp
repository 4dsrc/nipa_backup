#include "StdAfx.h"
#include "DllFunc.h"
#include "ESMMovieKZoneMgr.h"


CESMMovieKZoneMgr::CESMMovieKZoneMgr(void)
{
	m_nKZoneDscCount	= 0;
	m_bOptKZone			= FALSE;
	m_pKZoneDataInfo	= NULL;

	InitKZoneImage();
}

CESMMovieKZoneMgr::~CESMMovieKZoneMgr(void)
{

}

void CESMMovieKZoneMgr::SetKZoneDataInfo(KZoneDataInfo* pKZoneDataInfo)
{
	m_pKZoneDataInfo = pKZoneDataInfo;
}

void CESMMovieKZoneMgr::SetKZoneDscCount(int nKZoneDscCount)
{
	m_nKZoneDscCount = nKZoneDscCount;
}

int CESMMovieKZoneMgr::GetKZoneDscCount()
{
	return m_nKZoneDscCount;
}

void CESMMovieKZoneMgr::SetOptKZone(BOOL bOptKZone)
{
	m_bOptKZone = bOptKZone;
}

BOOL CESMMovieKZoneMgr::GetOptKZone()
{
	return m_bOptKZone;
}

int CESMMovieKZoneMgr::GetKZoneDscIndexAt(CString strDscId)
{
	int nKZoneIndex = -1;
	for(int j = 0; j < GetKZoneDscCount(); j++)
	{
		CString strKZoneDscId(m_pKZoneDataInfo[j].szDscID);
		//strKZoneDscId.Format(_T("%05d"), m_pKZoneDataInfo[j].nDscId);
		
		if(strKZoneDscId.Compare(strDscId) == 0)
		{
			nKZoneIndex = j;
			break;
		}
	}
	return nKZoneIndex;
}

void CESMMovieKZoneMgr::DoDrawKZone(CString strDscId, Mat& srcImage)
{
	if(GetOptKZone())
	{
		if(GetKZoneDscCount() > 0 && m_pKZoneDataInfo != NULL)
		{
			int nKZoneIndex = GetKZoneDscIndexAt(strDscId);

			if(nKZoneIndex != -1 && m_pKZoneDataInfo[nKZoneIndex].bIsEnabled == TRUE)
			{
				if(m_bStateKZoneImage)
				{
					DrawKZoneAsGradient(srcImage, &m_pKZoneDataInfo[nKZoneIndex]);
				}
				else
				{
					DrawKZoneAsSkeleton(srcImage, &m_pKZoneDataInfo[nKZoneIndex]);
				}
			}
		}
		//else
		//{
		//	SendLog(0, _T("Do not Draw K-Zone. KZoneDataInfo is NULL or K-Zone DscCount <= 0"));
		//}
	}
}

void CESMMovieKZoneMgr::DoDrawKZoneUsingGPU(CString strDscId, cuda::GpuMat& srcImage, BOOL bIsStrike)
{
	//if (GetOptKZone())
	{
		if (GetKZoneDscCount() > 0 && m_pKZoneDataInfo != NULL)
		{
			int nKZoneIndex = GetKZoneDscIndexAt(strDscId);

			if (nKZoneIndex != -1 && m_pKZoneDataInfo[nKZoneIndex].bIsEnabled == TRUE)
			{
				if (m_bStateKZoneImage)
				{
					if(bIsStrike)
						DrawKZoneAsGradientUsingGPU(srcImage, &m_pKZoneDataInfo[nKZoneIndex]);
					else
						DrawKZoneAsGradientUsingGPUNegative(srcImage, &m_pKZoneDataInfo[nKZoneIndex]);
				}
			}
		}
		else
		{
			SendLog(0, _T("Do not Draw K-Zone. KZoneDataInfo is NULL or K-Zone DscCount <= 0"));
		}
	}
}

void CESMMovieKZoneMgr::DrawKZoneAsSkeleton(Mat& srcImage, KZoneDataInfo* pKZoneDataInfo)
{
	if (pKZoneDataInfo == NULL)
		return;

	Point2f vecPtKZonePoints[10];
	int nLineThickness = 1;
	for(int j = 0; j < 10; j++)
	{
		vecPtKZonePoints[j] = Point2f(pKZoneDataInfo->ptKZonePoint[j].x, pKZoneDataInfo->ptKZonePoint[j].y);
	}
	Scalar scRGB = Scalar(128, 128, 128);
	line(srcImage, vecPtKZonePoints[0], vecPtKZonePoints[1], scRGB, nLineThickness, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[1], vecPtKZonePoints[2], scRGB, nLineThickness, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[2], vecPtKZonePoints[3], scRGB, nLineThickness, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[3], vecPtKZonePoints[0], scRGB, nLineThickness, cv::LINE_AA, 0);

	line(srcImage, vecPtKZonePoints[0], vecPtKZonePoints[4], scRGB, nLineThickness, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[1], vecPtKZonePoints[5], scRGB, nLineThickness, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[2], vecPtKZonePoints[6], scRGB, nLineThickness, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[3], vecPtKZonePoints[7], scRGB, nLineThickness, cv::LINE_AA, 0);

	line(srcImage, vecPtKZonePoints[4], vecPtKZonePoints[8], scRGB, nLineThickness, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[5], vecPtKZonePoints[8], scRGB, nLineThickness, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[6], vecPtKZonePoints[9], scRGB, nLineThickness, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[7], vecPtKZonePoints[9], scRGB, nLineThickness, cv::LINE_AA, 0);

	line(srcImage, vecPtKZonePoints[4], vecPtKZonePoints[7], scRGB, nLineThickness, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[5], vecPtKZonePoints[6], scRGB, nLineThickness, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[8], vecPtKZonePoints[9], scRGB, nLineThickness, cv::LINE_AA, 0);
}

void CESMMovieKZoneMgr::DrawKZoneAsGradient(Mat& srcImage, KZoneDataInfo* pKZoneDataInfo)
{
	Point2f vecPtKZonePoints[10];
	for(int j = 0; j < 10; j++)
	{
		vecPtKZonePoints[j] = Point2f(pKZoneDataInfo->ptKZonePoint[j].x, pKZoneDataInfo->ptKZonePoint[j].y);
	}

	//Face
	vector<Point2f> vecPtKZoneFace(4);
	vecPtKZoneFace[0] = vecPtKZonePoints[0];
	vecPtKZoneFace[1] = vecPtKZonePoints[1];
	vecPtKZoneFace[2] = vecPtKZonePoints[2];
	vecPtKZoneFace[3] = vecPtKZonePoints[3];
	DrawKZonePerspective(srcImage, m_imKZoneFace, vecPtKZoneFace);

	//Side
	vector<Point2f> vecPtKZoneLeftSide(4);
	vecPtKZoneLeftSide[0] = vecPtKZonePoints[0];
	vecPtKZoneLeftSide[1] = vecPtKZonePoints[4];
	vecPtKZoneLeftSide[2] = vecPtKZonePoints[7];
	vecPtKZoneLeftSide[3] = vecPtKZonePoints[3];
	DrawKZonePerspective(srcImage, m_imKZoneSide, vecPtKZoneLeftSide);

	vector<Point2f> vecPtKZoneRightSide(4);
	vecPtKZoneRightSide[0] = vecPtKZonePoints[1];
	vecPtKZoneRightSide[1] = vecPtKZonePoints[5];
	vecPtKZoneRightSide[2] = vecPtKZonePoints[6];
	vecPtKZoneRightSide[3] = vecPtKZonePoints[2];
	DrawKZonePerspective(srcImage, m_imKZoneSide, vecPtKZoneRightSide);

	//Top Pentagon
	Point2f ptDeltaLeftTopLine	= (vecPtKZonePoints[4] - vecPtKZonePoints[0])*2;
	Point2f ptDeltaRightTopLine = (vecPtKZonePoints[5] - vecPtKZonePoints[1])*2;
	Point2f ptLeftTopLine		= ptDeltaLeftTopLine + vecPtKZonePoints[0];
	Point2f ptRightTopLine		= ptDeltaRightTopLine + vecPtKZonePoints[1];

	vector<Point2f> vecPtKZoneTopPentagon(4);
	vecPtKZoneTopPentagon[0] = ptLeftTopLine;//vecPtKZonePoints[4];
	vecPtKZoneTopPentagon[1] = vecPtKZonePoints[0];
	vecPtKZoneTopPentagon[2] = vecPtKZonePoints[1];
	vecPtKZoneTopPentagon[3] = ptRightTopLine;//vecPtKZonePoints[5];
	DrawKZonePerspective(srcImage, m_imKZonePentagon, vecPtKZoneTopPentagon);

	//Bottom Pentagon
	Point2f ptDeltaLeftBottomLine	= (vecPtKZonePoints[7] - vecPtKZonePoints[3])*2;
	Point2f ptDeltaRightBottomLine	= (vecPtKZonePoints[6] - vecPtKZonePoints[2])*2;
	Point2f ptLeftBottomLine		= ptDeltaLeftBottomLine + vecPtKZonePoints[3];
	Point2f ptRightBottomLine		= ptDeltaRightBottomLine + vecPtKZonePoints[2];

	vector<Point2f> vecPtKZoneBottomPentagon(4);
	vecPtKZoneBottomPentagon[0] = ptLeftBottomLine;//vecPtKZonePoints[7];
	vecPtKZoneBottomPentagon[1] = vecPtKZonePoints[3];
	vecPtKZoneBottomPentagon[2] = vecPtKZonePoints[2];
	vecPtKZoneBottomPentagon[3] = ptRightBottomLine;//vecPtKZonePoints[6];
	DrawKZonePerspective(srcImage, m_imKZonePentagon, vecPtKZoneBottomPentagon);

	//Draw Line
	Scalar scRGB = Scalar(0, 0, 0);
	line(srcImage, vecPtKZonePoints[0], vecPtKZonePoints[1], scRGB, 1, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[1], vecPtKZonePoints[2], scRGB, 1, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[2], vecPtKZonePoints[3], scRGB, 1, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[3], vecPtKZonePoints[0], scRGB, 1, cv::LINE_AA, 0);

	line(srcImage, vecPtKZonePoints[0], vecPtKZonePoints[4], scRGB, 1, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[1], vecPtKZonePoints[5], scRGB, 1, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[2], vecPtKZonePoints[6], scRGB, 1, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[3], vecPtKZonePoints[7], scRGB, 1, cv::LINE_AA, 0);

	line(srcImage, vecPtKZonePoints[4], vecPtKZonePoints[8], scRGB, 1, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[5], vecPtKZonePoints[8], scRGB, 1, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[6], vecPtKZonePoints[9], scRGB, 1, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[7], vecPtKZonePoints[9], scRGB, 1, cv::LINE_AA, 0);

	line(srcImage, vecPtKZonePoints[4], vecPtKZonePoints[7], scRGB, 1, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[5], vecPtKZonePoints[6], scRGB, 1, cv::LINE_AA, 0);
	line(srcImage, vecPtKZonePoints[8], vecPtKZonePoints[9], scRGB, 1, cv::LINE_AA, 0);
}

void CESMMovieKZoneMgr::DrawKZoneAsGradientUsingGPU(cuda::GpuMat& srcImage, KZoneDataInfo* pKZoneDataInfo)
{
	if (pKZoneDataInfo == NULL)
		return;

	Point2f vecPtKZonePoints[10];
	for (int j = 0; j < 10; j++)
	{
		vecPtKZonePoints[j] = Point2f(pKZoneDataInfo->ptKZonePoint[j].x, pKZoneDataInfo->ptKZonePoint[j].y);
	}

	//Face
	vector<Point2f> vecPtKZoneFace(4);
	vecPtKZoneFace[0] = vecPtKZonePoints[0];
	vecPtKZoneFace[1] = vecPtKZonePoints[1];
	vecPtKZoneFace[2] = vecPtKZonePoints[2];
	vecPtKZoneFace[3] = vecPtKZonePoints[3];
	DrawKZonePerspectiveUsingGPU(srcImage, m_gimKZoneFace, vecPtKZoneFace);

	//Side
	vector<Point2f> vecPtKZoneLeftSide(4);
	vecPtKZoneLeftSide[0] = vecPtKZonePoints[0];
	vecPtKZoneLeftSide[1] = vecPtKZonePoints[4];
	vecPtKZoneLeftSide[2] = vecPtKZonePoints[7];
	vecPtKZoneLeftSide[3] = vecPtKZonePoints[3];
	DrawKZonePerspectiveUsingGPU(srcImage, m_gimKZoneSide, vecPtKZoneLeftSide);

	vector<Point2f> vecPtKZoneRightSide(4);
	vecPtKZoneRightSide[0] = vecPtKZonePoints[1];
	vecPtKZoneRightSide[1] = vecPtKZonePoints[5];
	vecPtKZoneRightSide[2] = vecPtKZonePoints[6];
	vecPtKZoneRightSide[3] = vecPtKZonePoints[2];
	DrawKZonePerspectiveUsingGPU(srcImage, m_gimKZoneSide, vecPtKZoneRightSide);

	//Top Pentagon
	Point2f ptDeltaLeftTopLine = (vecPtKZonePoints[4] - vecPtKZonePoints[0]) * 2;
	Point2f ptDeltaRightTopLine = (vecPtKZonePoints[5] - vecPtKZonePoints[1]) * 2;
	Point2f ptLeftTopLine = ptDeltaLeftTopLine + vecPtKZonePoints[0];
	Point2f ptRightTopLine = ptDeltaRightTopLine + vecPtKZonePoints[1];

	vector<Point2f> vecPtKZoneTopPentagon(4);
	vecPtKZoneTopPentagon[0] = ptLeftTopLine;//vecPtKZonePoints[4];
	vecPtKZoneTopPentagon[1] = vecPtKZonePoints[0];
	vecPtKZoneTopPentagon[2] = vecPtKZonePoints[1];
	vecPtKZoneTopPentagon[3] = ptRightTopLine;//vecPtKZonePoints[5];
	DrawKZonePerspectiveUsingGPU(srcImage, m_gimKZonePentagon, vecPtKZoneTopPentagon);

	//Bottom Pentagon
	Point2f ptDeltaLeftBottomLine = (vecPtKZonePoints[7] - vecPtKZonePoints[3]) * 2;
	Point2f ptDeltaRightBottomLine = (vecPtKZonePoints[6] - vecPtKZonePoints[2]) * 2;
	Point2f ptLeftBottomLine = ptDeltaLeftBottomLine + vecPtKZonePoints[3];
	Point2f ptRightBottomLine = ptDeltaRightBottomLine + vecPtKZonePoints[2];

	vector<Point2f> vecPtKZoneBottomPentagon(4);
	vecPtKZoneBottomPentagon[0] = ptLeftBottomLine;//vecPtKZonePoints[7];
	vecPtKZoneBottomPentagon[1] = vecPtKZonePoints[3];
	vecPtKZoneBottomPentagon[2] = vecPtKZonePoints[2];
	vecPtKZoneBottomPentagon[3] = ptRightBottomLine;//vecPtKZonePoints[6];
	DrawKZonePerspectiveUsingGPU(srcImage, m_gimKZonePentagon, vecPtKZoneBottomPentagon);
}

void CESMMovieKZoneMgr::DrawKZoneAsGradientUsingGPUNegative(cuda::GpuMat& srcImage, KZoneDataInfo* pKZoneDataInfo)
{
	if (pKZoneDataInfo == NULL)
		return;

	Point2f vecPtKZonePoints[10];
	for (int j = 0; j < 10; j++)
	{
		vecPtKZonePoints[j] = Point2f(pKZoneDataInfo->ptKZonePoint[j].x, pKZoneDataInfo->ptKZonePoint[j].y);
	}

	//Face
	vector<Point2f> vecPtKZoneFace(4);
	vecPtKZoneFace[0] = vecPtKZonePoints[0];
	vecPtKZoneFace[1] = vecPtKZonePoints[1];
	vecPtKZoneFace[2] = vecPtKZonePoints[2];
	vecPtKZoneFace[3] = vecPtKZonePoints[3];
	DrawKZonePerspectiveUsingGPU(srcImage, m_gimKZoneFaceX, vecPtKZoneFace);

	//Side
	vector<Point2f> vecPtKZoneLeftSide(4);
	vecPtKZoneLeftSide[0] = vecPtKZonePoints[0];
	vecPtKZoneLeftSide[1] = vecPtKZonePoints[4];
	vecPtKZoneLeftSide[2] = vecPtKZonePoints[7];
	vecPtKZoneLeftSide[3] = vecPtKZonePoints[3];
	DrawKZonePerspectiveUsingGPU(srcImage, m_gimKZoneSideX, vecPtKZoneLeftSide);

	vector<Point2f> vecPtKZoneRightSide(4);
	vecPtKZoneRightSide[0] = vecPtKZonePoints[1];
	vecPtKZoneRightSide[1] = vecPtKZonePoints[5];
	vecPtKZoneRightSide[2] = vecPtKZonePoints[6];
	vecPtKZoneRightSide[3] = vecPtKZonePoints[2];
	DrawKZonePerspectiveUsingGPU(srcImage, m_gimKZoneSideX, vecPtKZoneRightSide);

	//Top Pentagon
	Point2f ptDeltaLeftTopLine = (vecPtKZonePoints[4] - vecPtKZonePoints[0]) * 2;
	Point2f ptDeltaRightTopLine = (vecPtKZonePoints[5] - vecPtKZonePoints[1]) * 2;
	Point2f ptLeftTopLine = ptDeltaLeftTopLine + vecPtKZonePoints[0];
	Point2f ptRightTopLine = ptDeltaRightTopLine + vecPtKZonePoints[1];

	vector<Point2f> vecPtKZoneTopPentagon(4);
	vecPtKZoneTopPentagon[0] = ptLeftTopLine;//vecPtKZonePoints[4];
	vecPtKZoneTopPentagon[1] = vecPtKZonePoints[0];
	vecPtKZoneTopPentagon[2] = vecPtKZonePoints[1];
	vecPtKZoneTopPentagon[3] = ptRightTopLine;//vecPtKZonePoints[5];
	DrawKZonePerspectiveUsingGPU(srcImage, m_gimKZonePentagonX, vecPtKZoneTopPentagon);

	//Bottom Pentagon
	Point2f ptDeltaLeftBottomLine = (vecPtKZonePoints[7] - vecPtKZonePoints[3]) * 2;
	Point2f ptDeltaRightBottomLine = (vecPtKZonePoints[6] - vecPtKZonePoints[2]) * 2;
	Point2f ptLeftBottomLine = ptDeltaLeftBottomLine + vecPtKZonePoints[3];
	Point2f ptRightBottomLine = ptDeltaRightBottomLine + vecPtKZonePoints[2];

	vector<Point2f> vecPtKZoneBottomPentagon(4);
	vecPtKZoneBottomPentagon[0] = ptLeftBottomLine;//vecPtKZonePoints[7];
	vecPtKZoneBottomPentagon[1] = vecPtKZonePoints[3];
	vecPtKZoneBottomPentagon[2] = vecPtKZonePoints[2];
	vecPtKZoneBottomPentagon[3] = ptRightBottomLine;//vecPtKZonePoints[6];
	DrawKZonePerspectiveUsingGPU(srcImage, m_gimKZonePentagonX, vecPtKZoneBottomPentagon);
}

void CESMMovieKZoneMgr::DrawKZonePerspective(Mat& srcImage, Mat& addImage, vector<Point2f> vecPtKzonePoint)
{
	vector<Point2f> vecPtKZoneInSrcImage(4);
	vecPtKZoneInSrcImage[0] = Point2f(0, 0);
	vecPtKZoneInSrcImage[1] = Point2f(addImage.cols, 0);
	vecPtKZoneInSrcImage[2] = Point2f(addImage.cols, addImage.rows);
	vecPtKZoneInSrcImage[3] = Point2f(0, addImage.rows);

	vector<Point2f> vecPtKZone(4);
	vecPtKZone[0] = vecPtKzonePoint[0];
	vecPtKZone[1] = vecPtKzonePoint[1];
	vecPtKZone[2] = vecPtKzonePoint[2];
	vecPtKZone[3] = vecPtKzonePoint[3];

	Mat mKZonePerspective= getPerspectiveTransform(vecPtKZoneInSrcImage, vecPtKZone);
	Mat mKZone;
	warpPerspective(addImage, mKZone, mKZonePerspective, srcImage.size());

	add(mKZone, srcImage, srcImage);
}

void CESMMovieKZoneMgr::DrawKZonePerspectiveUsingGPU(cuda::GpuMat& srcImage, cuda::GpuMat& addImage, vector<Point2f> vecPtKzonePoint)
{
	vector<Point2f> vecPtKZoneInSrcImage(4);
	vecPtKZoneInSrcImage[0] = Point2f(0, 0);
	vecPtKZoneInSrcImage[1] = Point2f(addImage.cols, 0);
	vecPtKZoneInSrcImage[2] = Point2f(addImage.cols, addImage.rows);
	vecPtKZoneInSrcImage[3] = Point2f(0, addImage.rows);

	vector<Point2f> vecPtKZone(4);
	vecPtKZone[0] = vecPtKzonePoint[0];
	vecPtKZone[1] = vecPtKzonePoint[1];
	vecPtKZone[2] = vecPtKzonePoint[2];
	vecPtKZone[3] = vecPtKzonePoint[3];

	Mat mKZonePerspective = getPerspectiveTransform(vecPtKZoneInSrcImage, vecPtKZone);
	cuda::GpuMat mKZone;
	cuda::warpPerspective(addImage, mKZone, mKZonePerspective, srcImage.size());

	cuda::add(mKZone, srcImage, srcImage);
}

void CESMMovieKZoneMgr::InitKZoneImage()
{
	m_imKZonePentagon	= imread("C:\\Program Files\\ESMLab\\4DMaker\\img\\06.Prism\\pentagon_o.png", 1);
	m_imKZoneSide		= imread("C:\\Program Files\\ESMLab\\4DMaker\\img\\06.Prism\\square0_o.png", 1);
	m_imKZoneFace		= imread("C:\\Program Files\\ESMLab\\4DMaker\\img\\06.Prism\\square1_o.png", 1);

	m_imKZonePentagonX = imread("C:\\Program Files\\ESMLab\\4DMaker\\img\\06.Prism\\pentagon_x.png", 1);
	m_imKZoneSideX = imread("C:\\Program Files\\ESMLab\\4DMaker\\img\\06.Prism\\square0_x.png", 1);
	m_imKZoneFaceX = imread("C:\\Program Files\\ESMLab\\4DMaker\\img\\06.Prism\\square1_x.png", 1);

	if (!m_imKZonePentagon.empty() &&
		!m_imKZoneSide.empty() &&
		!m_imKZoneFace.empty() &&
		!m_imKZonePentagonX.empty() &&
		!m_imKZoneSideX.empty()&&
		!m_imKZoneSideX.empty())
	{
		m_bStateKZoneImage = TRUE;
	}
	else
	{
		SendLog(0, _T("K-Zone Image Load Error!"));
		m_bStateKZoneImage = FALSE;
		return;
	}

	m_gimKZonePentagon.upload(m_imKZonePentagon);
	m_gimKZoneSide.upload(m_imKZoneSide);
	m_gimKZoneFace.upload(m_imKZoneFace);

	m_gimKZonePentagonX.upload(m_imKZonePentagonX);
	m_gimKZoneSideX.upload(m_imKZoneSideX);
	m_gimKZoneFaceX.upload(m_imKZoneFaceX);
}
