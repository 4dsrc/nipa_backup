////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieThread.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-26
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMMovieThread.h"
#include "ESMMovieMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CESMMovieThread, CWinThread)

CESMMovieThread::CESMMovieThread(CWinThread* pParent, short nMessage, ESMMovieMsg* pMsg)
{
	m_pParent = pParent;
	m_nMessage = nMessage;
	m_pMsg = pMsg;
	m_nProcessOrder = ESM_CREATE_MOVIE_ORDER;
	SetOrder(nMessage);	

	m_nFrameRate = 1;
	m_model = SDI_MODEL_UNKNOWN;
}

// CESMMovieEncoder
CESMMovieThread::~CESMMovieThread()
{
	ExitInstance();
}

BOOL CESMMovieThread::InitInstance()
{
	m_bAutoDelete = FALSE;
	return TRUE;
}

int CESMMovieThread::ExitInstance()
{
	StopThread();	
	return CWinThread::ExitInstance();
}

//------------------------------------------------------------------------------ 
//! @brief		StopThread
//! @date		2010-06-29
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CESMMovieThread::StopThread()
{
	//-- 2013-12-01 hongsu@esmlab.com
	//-- Confirm Stop Thread
	WaitForSingleObject(this->m_hThread, INFINITE);
}

void CESMMovieThread::FinishThread()
{
	ESMMovieMsg* pMsg = new ESMMovieMsg;
	pMsg->nMsg = ESM_MOVIE_THREAD_TERMINATE;
	pMsg->pValue1 = (PVOID*)this;

	//-- AddEvent
	((CESMMovieMgr*)m_pParent)->AddEvent(pMsg);
}

BOOL CESMMovieThread::WaitOrder(TCHAR* pMovieName, TCHAR* pMovieId, int nFrameNum)
{
	ESMMovieData* pMovieData = NULL;
	pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(pMovieName, pMovieId);
	if( pMovieData->GetBufferSize() <= nFrameNum)
		return FALSE;


	while(1)
	{
		Sleep(1);
		if(m_nProcessOrder <= pMovieData->GetFrameState(nFrameNum) + 1)
			break;
// 		if( ((CESMMovieMgr*)m_pParent)->GetMakingStop())
// 			break;
	}
	return TRUE;
}

void CESMMovieThread::SetOrder(short nMessage)
{
	switch(m_nMessage)
	{
	case ESM_MOVIE_IMAGE_RESIZE			: m_nProcessOrder = ESM_CREATE_MOIVE_RESIZE;		break;
	case ESM_MOVIE_IMAGE_ROTATE			: m_nProcessOrder = ESM_CREATE_MOIVE_ROTATE;		break;
	case ESM_MOVIE_IMAGE_MOVE			: m_nProcessOrder = ESM_CREATE_MOIVE_MOVE;			break;
	case ESM_MOVIE_IMAGE_CUT			: m_nProcessOrder = ESM_CREATE_MOIVE_CUT;			break;
	case ESM_MOVIE_IMAGE_EFFECT			: m_nProcessOrder = ESM_CREATE_MOIVE_EFFECT;		break;
	//case ESM_MOVIE_FILTER_COLORADJUST	: m_nProcessOrder = ESM_CREATE_MOIVE_COLORCALIB;	break;
	case ESM_MOVIE_IMAGE_LOGO			: m_nProcessOrder = ESM_CREATE_MOIVE_lOGO;			break;
	}	
}

void CESMMovieThread::SetFrameRate(int nRate)
{
	m_nFrameRate = nRate;
}
int CESMMovieThread::GetFrameRate()
{
	return m_nFrameRate;
}

void CESMMovieThread::SetModel(SDI_MODEL model)
{
	m_model = model;
}

SDI_MODEL CESMMovieThread::GetModel()
{
	return m_model;
}
