#include "stdafx.h"
#include "ESMGpuDecode.h"
#include "opencv2/highgui.hpp"
#include "DllFunc.h"
#include "cudaProcessFrame.h"
#include "opencv2\xphoto.hpp"

using namespace cv::xphoto;

//char *sAppName     = "NVDECODE/D3D11 Video Decoder";
#define CLIPING(data)  ( (data) < 0 ? 0 : ( (data) > 255 ) ? 255 : ( data ) )
char *sAppName[2]     = {"test1", "test2" };

char *sAppFilename = "NVDecodeD3D11";
char *sSDKname     = "NVDecodeD3D11";

__constant__ uint32 constAlpha;

#define MUL(x,y)    (x*y)
__constant__ float  constHueColorSpaceMat_[9];

#define VIDEO_SOURCE_FILE "SAM_0603.MP4"

#ifdef _DEBUG
#define ENABLE_DEBUG_OUT    0
#else
#define ENABLE_DEBUG_OUT    0
#endif

//StopWatchInterface *frame_timer  = NULL;
//StopWatchInterface *global_timer = NULL;

int                 g_DeviceID    = 0;
bool                g_bWindowed   = false;
bool                g_bDone       = false;
bool                g_bRunning    = false;
bool                g_bAutoQuit   = false;
bool                g_bUseVsync   = false;
bool                g_bFrameRepeat= false;
bool                g_bFrameStep  = false;
bool                g_bQAReadback = false;
bool                g_bFirstFrame = true;
bool                g_bLoop       = false;
bool                g_bUpdateCSC  = true;
bool                g_bUpdateAll  = false;
bool                g_bUseDisplay = true; // this flag enables/disables video on the window
bool                g_bUseInterop = true;
//bool                g_bReadback   = false; // this flag enables/disables reading back of a video from a window
bool                g_bReadback   = true; 
bool                g_bWriteFile  = true; // this flag enables/disables writing of a file
bool                g_bPSNR       = false; // if this flag is set true, then we want to compute the PSNR
bool                g_bIsProgressive = true; // assume it is progressive, unless otherwise noted
bool                g_bException  = false;
bool                g_bWaived     = false;
int                 g_iRepeatFactor = 1; // 1:1 assumes no frame repeats
long                g_nFrameStart = -1;
long                g_nFrameEnd = -1;

HWND                g_hWnd = NULL;
WNDCLASSEX          *g_wc = NULL;

int   *pArgc = NULL;
char **pArgv = NULL;

FILE *fpWriteYUV = NULL;
FILE *fpRefYUV = NULL;

cudaVideoCreateFlags g_eVideoCreateFlags = cudaVideoCreate_PreferCUVID;
CUvideoctxlock       g_CtxLock = NULL;

float present_fps, decoded_fps, total_time = 0.0f;

ID3D11Device  *g_pD3DDevice;
ID3D11DeviceContext *g_pContext;
IDXGISwapChain *g_pSwapChain;

// These are CUDA function pointers to the CUDA kernels
CUmoduleManager   *g_pCudaModule;

CUmodule           cuModNV12toARGB       = 0;
CUfunction         g_kernelNV12toARGB    = 0;
CUfunction         g_kernelPassThru      = 0;

CUcontext          g_oContext = 0;
CUdevice           g_oDevice  = 0;

CUstream           g_ReadbackSID = 0, g_KernelSID = 0;

eColorSpace        g_eColorSpace = ITU709;
float              g_nHue        = 0.0f;

// System Memory surface we want to readback to
BYTE          *g_pFrameYUV[6] = { 0, 0, 0, 0, 0, 0 };
FrameQueue    *g_pFrameQueue   = 0;
VideoSource   *g_pVideoSource  = 0;
VideoParser   *g_pVideoParser  = 0;
VideoDecoder  *g_pVideoDecoder = 0;

ImageDX       *g_pImageDX      = 0;
CUdeviceptr    g_pInteropFrame[3] = { 0, 0, 0 }; // if we're using CUDA malloc
CUdeviceptr    g_pRgba = 0;
CUarray        g_backBufferArray = 0;

CUVIDEOFORMAT g_stFormat;

std::string sFileName;

char exec_path[256];

unsigned int g_nWindowWidth  = 0;
unsigned int g_nWindowHeight = 0;

unsigned int g_nVideoWidth  = 0;
unsigned int g_nVideoHeight = 0;

unsigned int g_FrameCount = 0;
unsigned int g_DecodeFrameCount = 0;
unsigned int g_fpsCount = 0;      // FPS count for averaging
unsigned int g_fpsLimit = 16;     // FPS limit for sampling timer;

unsigned int g_nStartIndex = 0;
unsigned int g_nEndIndex = 0;

/*
double YY[256], BU[256], GV[256], GU[256], RV[256];
unsigned char YUV_B[256][256];
unsigned char YUV_R[256][256];
unsigned char YUV_G[256][256][256];*/

LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

CESMGpuDecode::CESMGpuDecode(void)
{
	/*int nStart = GetTickCount();
	YUV_lookup_table();
	int nEnd = GetTickCount();

	TRACE("YUV_lookup_table %d ms", nEnd - nStart);*/
}


CESMGpuDecode::~CESMGpuDecode(void)
{
	/*for(int i = 0; i < 6; i++)
	{
		if(g_pFrameYUV[i])
		{
			cuMemFreeHost((void *)g_pFrameYUV[i]);
			g_pFrameYUV[i] = NULL;
		}
		
	}*/
}

void CESMGpuDecode::YUV_lookup_table()
{
	/*int i, j, k;
	double i_value;
	for( i=255; i>=0; i-- )
	{
		YY[i] = (1.164*(i-16.0));
		BU[i] = (2.018*(i-128.0));
		GV[i] = (0.831*(i-128.0));
		GU[i] = (0.391*(i-128.0));
		RV[i] = (1.596*(i-128.0));
	}

	for( i=255; i>=0; i-- ){
		for( j=255; j>=0; j-- )
		{
			i_value = YY[i] + BU[j];
			if ( i_value > 255 ) i_value=255;
			else if ( i_value < 0 ) i_value=0;
			YUV_B[i][j]=(int)i_value;
			i_value = YY[i] + RV[j];
			if ( i_value > 255 ) i_value=255;
			else if ( i_value < 0 ) i_value=0;
			YUV_R[i][j]=(int)i_value;
			for( k=0; k<256; k++ )
			{
				i_value = YY[i] - (GU[j] + GV[k]);
				if ( i_value > 255 ) i_value=255;
				else if ( i_value < 0 ) i_value=0;
				YUV_G[i][j][k] =(int)i_value;
			}
		}
	}*/
}


void CESMGpuDecode::parseCommand(CString strPath)
{
	CString strOutput;
	strOutput.Format(_T("F:\\test.mp4"));

	char video_file[256], yuv_file[256], ref_yuv[256];
	strcpy(video_file,CT2A(strPath));
	strcpy(yuv_file,CT2A(strOutput));

	bool bUseDefaultInputFile = true;

	if (g_bLoop == false)
	{
		g_bAutoQuit = true;
	}

	// Now verify the input video file is legit
	FILE *fp = NULL;
	FOPEN(fp, video_file, "r");
	if (video_file == NULL && fp == NULL)
	{
		printf("[%s]: unable to find file: [%s]\nExiting...\n", sAppFilename, VIDEO_SOURCE_FILE);
		exit(EXIT_FAILURE);
	}

	if (fp)
	{
		printf("[%s]: input file:  [%s]\n", sAppFilename, video_file);
		fclose(fp);
	}

	// Now verify the input reference YUV file is legit
	FOPEN(fpRefYUV, ref_yuv, "r");
	if (ref_yuv == NULL && fpRefYUV == NULL)
	{
		printf("[%s]: unable to find file: [%s]\nExiting...\n", sAppFilename, ref_yuv);
		exit(EXIT_FAILURE);
	}

	// default video file loaded by this sample
	sFileName = video_file;

	if (g_bWriteFile && strlen(yuv_file) > 0)
	{
		printf("[%s]: output file: [%s]\n", sAppFilename, yuv_file);

		FOPEN(fpWriteYUV, yuv_file, "wb");
		if (fpWriteYUV == NULL)
		{
			printf("Error opening file [%s]\n", yuv_file);
		}
	}

	// store the current path so we can reinit the CUDA context
	//strcpy(exec_path, argv[0]);
	//strcpy(exec_path, NULL);
}

int g_nIndex = 0;

void CESMGpuDecode::initValue()
{
	g_DeviceID    = 0;
	g_bWindowed   = true;
	g_bDone       = false;
	g_bRunning    = false;
	g_bAutoQuit   = false;
	g_bUseVsync   = false;
	g_bFrameRepeat= false;
	g_bFrameStep  = false;
	g_bQAReadback = false;
	g_bFirstFrame = true;
	g_bLoop       = false;
	g_bUpdateCSC  = false;
	g_bUpdateAll  = false;
	g_bUseDisplay = true; // this flag enables/disables video on the window
	g_bUseInterop = false;
	g_bReadback   = false; // this flag enables/disables reading back of a video from a window
	g_bReadback   = true; 
	g_bWriteFile  = true; // this flag enables/disables writing of a file
	g_bPSNR       = false; // if this flag is set true, then we want to compute the PSNR
	g_bIsProgressive = true; // assume it is progressive, unless otherwise noted
	g_bException  = false;
	g_bWaived     = false;
	g_iRepeatFactor = 1; // 1:1 assumes no frame repeats
	g_nFrameStart = -1;
	g_nFrameEnd = -1;

	g_hWnd = NULL;
	g_wc = NULL;

	pArgc = NULL;
	pArgv = NULL;

	fpWriteYUV = NULL;
	fpRefYUV = NULL;

	g_eVideoCreateFlags = cudaVideoCreate_PreferCUVID;
	g_CtxLock = NULL;

	present_fps = 0.0f;
	decoded_fps = 0.0f;
	total_time = 0.0f;

	cuModNV12toARGB       = 0;
	g_kernelNV12toARGB    = 0;
	g_kernelPassThru      = 0;

	g_oContext = 0;
	g_oDevice  = 0;

	g_ReadbackSID = 0;
	g_KernelSID = 0;

	g_eColorSpace = ITU601;
	g_nHue        = 0.0f;

	// System Memory surface we want to readback to
	for(int i = 0; i < 6; i++)
		g_pFrameYUV[i] = 0;
	g_pFrameQueue   = 0;
	g_pVideoSource  = 0;
	g_pVideoParser  = 0;
	g_pVideoDecoder = 0;

	g_pImageDX      = 0;
	for(int i = 0; i < 3; i++)
		g_pInteropFrame[i] = 0; // if we're using CUDA malloc
	g_pRgba = 0;
	g_backBufferArray = 0;

	g_nWindowWidth  = 0;
	g_nWindowHeight = 0;

	g_nVideoWidth  = 0;
	g_nVideoHeight = 0;

	g_FrameCount = 0;
	g_DecodeFrameCount = 0;
	g_fpsCount = 0;      // FPS count for averaging
	g_fpsLimit = 16;     // FPS limit for sampling timer;

	g_nStartIndex = 0;
	g_nEndIndex = 0;
}

BOOL CESMGpuDecode::GPUDecoding(CString strPath, int nStartIndex, int nEndIndex, int nType, vector<MakeFrameInfo>* pArrFrameInfo,int nFrameSize,BOOL bIsLoad)
{
	g_nStartIndex = nStartIndex;
	g_nEndIndex = nEndIndex;
	g_nIndex = nStartIndex;

	BOOL bRet = TRUE;
	
	char video_file[256];

	initValue();
	
	strcpy(video_file,CT2A(strPath));
	sFileName = video_file;
	
	// Initialize the CUDA and NVDECODE
	typedef HMODULE CUDADRIVER;
	CUDADRIVER hHandleDriver = 0;
	CUresult cuResult;
	cuResult = cuInit    (0, __CUDA_API_VERSION, hHandleDriver);
	cuResult = cuvidInit (0);

	int gStart = GetTickCount();
	g_bIsProgressive = loadVideoSource(sFileName.c_str(),
		g_nVideoWidth, g_nVideoHeight,
		g_nWindowWidth, g_nWindowHeight);
	int gEnd = GetTickCount();

	CString str;
	str.Format(_T("Loading MP4 File : %d"),gEnd - gStart);
	SendLog(5,str);

	if(!g_bIsProgressive)
	{
		CString strLog;
		strLog.Format(_T("Load VideoSource Error [%s]"), strPath);
		SendLog(0,strLog);

		for(int i = 0; i < pArrFrameInfo->size(); i++)
			pArrFrameInfo->at(i).bComplete = -100;

		bRet = FALSE;
		return bRet;
	}
	
	int bTCC = 0;
	
	if (!g_bUseInterop && g_eVideoCreateFlags == cudaVideoCreate_PreferDXVA)
	{
		// preferDXVA will not work with -nointerop mode. Overwrite it.
		g_eVideoCreateFlags = cudaVideoCreate_PreferCUVID;
	}

	// If we are using TCC driver, then graphics interop must be disabled
	if (bTCC)
	{
		g_bUseInterop = false;
	}


	// Initialize CUDA/D3D11 context and other video memory resources
	//if (initCudaResources(g_bUseInterop, bTCC) == E_FAIL)
	if (initCudaResources(FALSE, bTCC,bIsLoad) == E_FAIL)
	{
		g_bAutoQuit  = true;
		g_bException = true;
		g_bWaived    = true;
		CString strLog;
		strLog.Format(_T("initCudaResources Error"));
		SendLog(0,strLog);
		bRet = FALSE;

		cleanup(TRUE);

		return bRet;
	}


	g_pVideoSource->start();
	g_bRunning = true;

	
	if (!g_bUseInterop)
		{
			// On this case we drive the display with a while loop (no openGL calls)
			while (!g_bDone)
			{
				renderVideoFrame(g_hWnd, g_bUseInterop, nType, pArrFrameInfo);
			}
		}
		else
		{
			// Standard windows loop
			while (!g_bDone)
			{
				printf("#1\n");
				MSG msg;
				ZeroMemory(&msg, sizeof(msg));
	
				while (msg.message!=WM_QUIT)
				{
					if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
					else
					{
						renderVideoFrame(g_hWnd, g_bUseInterop, nType, pArrFrameInfo);
					}
	
					if (g_bAutoQuit && g_bDone)
					{
						break;
					}
				}
			} // while loop
		}

	
	g_pFrameQueue->endDecode();
	g_pVideoSource->stop();

#if 1
	if(nFrameSize != g_DecodeFrameCount)
	{
		CString strLog;
		strLog.Format(_T("End File Skip [%d - %d]"),nFrameSize,g_DecodeFrameCount);
		SendLog(1,strLog);
		bRet = FALSE;
	}
#else
	if(g_DecodeFrameCount != pArrFrameInfo->size())
	{
		SendLog(1,_T("####### End File Skip"));
		//for(int n = 0; n < pArrFrameInfo->size(); n++)
		//	pArrFrameInfo->at(n).bComplete = -100;
		bRet = FALSE;
	}
#endif
	if (fpWriteYUV != NULL)
	{
		fflush(fpWriteYUV);
		fclose(fpWriteYUV);
		fpWriteYUV = NULL;
	}

	printStatistics();
	g_bWaived = false;

	//Release
	cleanup(TRUE);

	return bRet;
}



BOOL CESMGpuDecode::GPUDecoding(CString strPath, int nIndex, vector<ESMFrameArray*> pImgArr, int nType, vector<MakeFrameInfo>* pArrFrameInfo)
{
#if 1
	g_nIndex = nIndex;
	char video_file[256];

	initValue();
	
	strcpy(video_file,CT2A(strPath));
	sFileName = video_file;
	
	// Initialize the CUDA and NVDECODE
	typedef HMODULE CUDADRIVER;
	CUDADRIVER hHandleDriver = 0;
	CUresult cuResult;
	cuResult = cuInit    (0, __CUDA_API_VERSION, hHandleDriver);
	cuResult = cuvidInit (0);

	int gStart = GetTickCount();
	g_bIsProgressive = loadVideoSource(sFileName.c_str(),
		g_nVideoWidth, g_nVideoHeight,
		g_nWindowWidth, g_nWindowHeight);
	int gEnd = GetTickCount();

	CString str;
	str.Format(_T("Loading MP4 File : %d"),gEnd - gStart);
	SendLog(5,str);

	if(!g_bIsProgressive)
	{
		CString strLog;
		strLog.Format(_T("Load VideoSource Error [%s]"), strPath);
		SendLog(0,strLog);
		return FALSE;
	}
	
	int bTCC = 0;
	/*if (g_bUseInterop)
	{
		if (initD3D11(g_hWnd, &bTCC) == false)
		{
			g_bAutoQuit = true;
			g_bWaived   = true;
			return;
		}
	}*/

	if (!g_bUseInterop && g_eVideoCreateFlags == cudaVideoCreate_PreferDXVA)
	{
		// preferDXVA will not work with -nointerop mode. Overwrite it.
		g_eVideoCreateFlags = cudaVideoCreate_PreferCUVID;
	}

	// If we are using TCC driver, then graphics interop must be disabled
	if (bTCC)
	{
		g_bUseInterop = false;
	}


	// Initialize CUDA/D3D11 context and other video memory resources
	//if (initCudaResources(g_bUseInterop, bTCC) == E_FAIL)
	if (initCudaResources(FALSE, bTCC) == E_FAIL)
	{
		g_bAutoQuit  = true;
		g_bException = true;
		g_bWaived    = true;
		return FALSE;
	}

	g_pVideoSource->start();
	g_bRunning = true;

	
	if (!g_bUseInterop)
	{
		// On this case we drive the display with a while loop (no openGL calls)
		while (!g_bDone)
		{
			renderVideoFrame(g_hWnd, g_bUseInterop, pImgArr, nType, pArrFrameInfo);
		}
	}
	else
	{
		// Standard windows loop
		while (!g_bDone)
		{
			printf("#1\n");
			MSG msg;
			ZeroMemory(&msg, sizeof(msg));

			while (msg.message!=WM_QUIT)
			{
				if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
				else
				{
					renderVideoFrame(g_hWnd, g_bUseInterop, pImgArr, nType, pArrFrameInfo);
				}

				if (g_bAutoQuit && g_bDone)
				{
					break;
				}
			}
		} // while loop
	}

	// we only want to record this once
	//if (total_time == 0.0f)
	//{
	//	CString strTime;
	//	total_time = sdkGetTimerValue(&global_timer);
	//	strTime.Format(_T("Total Time %d ms\n"), total_time);
	//	TRACE(strTime);
	//}
	//sdkStopTimer(&global_timer);

	g_pFrameQueue->endDecode();
	g_pVideoSource->stop();

	if (fpWriteYUV != NULL)
	{
		fflush(fpWriteYUV);
		fclose(fpWriteYUV);
		fpWriteYUV = NULL;
	}

	printStatistics();

	g_bWaived = false;

	//Release
	cleanup(TRUE);
	
	return TRUE;
#else
	initValue();
	
	void* _this = this;
	g_nIndex = nIndex;

	// parse the command line arguments
	parseCommand(strPath);

	int iRequiredSize = ::MultiByteToWideChar(CP_ACP, NULL, sAppName[0], -1, NULL, 0);
	WCHAR* pwchString = new WCHAR[iRequiredSize];
	::MultiByteToWideChar(CP_ACP, NULL, sAppName[0] , -1, pwchString, iRequiredSize);



	// create window (after we know the size of the input file size)
	WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
		GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
		pwchString, NULL
	};

	
	RegisterClassEx(&wc);
	g_wc = &wc;

	// figure out the window size we must create to get a *client* area
	// that is of the size requested by m_dimensions.
	RECT adjustedWindowSize;
	DWORD dwWindowStyle;

	// Initialize the CUDA and NVDECODE
	typedef HMODULE CUDADRIVER;
	CUDADRIVER hHandleDriver = 0;
	CUresult cuResult;
	cuResult = cuInit    (0, __CUDA_API_VERSION, hHandleDriver);
	cuResult = cuvidInit (0);

	//Time
	int nStart[10], nEnd[10];
	// Find out the video size
	nStart[0] = GetTickCount();
	g_bIsProgressive = loadVideoSource(sFileName.c_str(),
		g_nVideoWidth, g_nVideoHeight,
		g_nWindowWidth, g_nWindowHeight);

	if(!g_bIsProgressive)
	{
		CString strLog;
		strLog.Format(_T("Load VideoSource Error [%s]"), strPath);
		SendLog(0,strLog);
		return ;
	}
	nEnd[0] = GetTickCount();
	

	// Create the Windows
	if (g_bUseDisplay)
	{
		dwWindowStyle = WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
		SetRect(&adjustedWindowSize, 0, 0, g_nVideoWidth  , g_nVideoHeight);
		AdjustWindowRect(&adjustedWindowSize, dwWindowStyle, false);

		g_nWindowWidth  = adjustedWindowSize.right  - adjustedWindowSize.left;
		g_nWindowHeight = adjustedWindowSize.bottom - adjustedWindowSize.top;

		// Create the application's window
		

		g_hWnd = CreateWindow(wc.lpszClassName, pwchString,
		//g_hWnd = CreateWindow(_T("test"), _T("test"),
			dwWindowStyle,
			0, 0,
			g_nWindowWidth,
			g_nWindowHeight,
			NULL, NULL, wc.hInstance, NULL);

		CString strTemp;
		strTemp.Format(_T("%s %s 0x%x"), wc.lpszClassName, pwchString, g_hWnd);
		//AfxMessageBox(strTemp);
	}

	if(pwchString)
		delete pwchString;

	int bTCC = 0;


	if (g_bUseInterop)
	{
		nStart[1] = GetTickCount();
		// Initialize Direct3D
		if (initD3D11(g_hWnd, &bTCC) == false)
		{
			g_bAutoQuit = true;
			g_bWaived   = true;
			return;
		}
		nEnd[1] = GetTickCount();
	}

	if (!g_bUseInterop && g_eVideoCreateFlags == cudaVideoCreate_PreferDXVA)
	{
		// preferDXVA will not work with -nointerop mode. Overwrite it.
		g_eVideoCreateFlags = cudaVideoCreate_PreferCUVID;
	}

	// If we are using TCC driver, then graphics interop must be disabled
	if (bTCC)
	{
		g_bUseInterop = false;
	}


	nStart[2] = GetTickCount();
	// Initialize CUDA/D3D11 context and other video memory resources
	if (initCudaResources(g_bUseInterop, bTCC) == E_FAIL)
	{
		g_bAutoQuit  = true;
		g_bException = true;
		g_bWaived    = true;
		return;
	}
	nEnd[2] = GetTickCount();


	nStart[3] = GetTickCount();
	g_pVideoSource->start();
	g_bRunning = true;

	/*if (!g_bQAReadback && !bTCC)
	{
		ShowWindow(g_hWnd, SW_SHOWDEFAULT);
		UpdateWindow(g_hWnd);
	}*/

	// the main loop
	//sdkStartTimer(&frame_timer);
	//sdkStartTimer(&global_timer);
	//sdkResetTimer(&global_timer);

	if (!g_bUseInterop)
	{
		// On this case we drive the display with a while loop (no openGL calls)
		while (!g_bDone)
		{
			renderVideoFrame(g_hWnd, g_bUseInterop, pImgArr, nType);
		}
	}
	else
	{
		// Standard windows loop
		while (!g_bDone)
		{
			printf("#1\n");
			MSG msg;
			ZeroMemory(&msg, sizeof(msg));

			while (msg.message!=WM_QUIT)
			{
				if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
				else
				{
					renderVideoFrame(g_hWnd, g_bUseInterop, pImgArr, nType);
				}

				if (g_bAutoQuit && g_bDone)
				{
					break;
				}
			}
		} // while loop
	}

	// we only want to record this once
	//if (total_time == 0.0f)
	//{
	//	CString strTime;
	//	total_time = sdkGetTimerValue(&global_timer);
	//	strTime.Format(_T("Total Time %d ms\n"), total_time);
	//	TRACE(strTime);
	//}
	//sdkStopTimer(&global_timer);

	g_pFrameQueue->endDecode();
	g_pVideoSource->stop();

	nEnd[3] = GetTickCount();
	if (fpWriteYUV != NULL)
	{
		fflush(fpWriteYUV);
		fclose(fpWriteYUV);
		fpWriteYUV = NULL;
	}

	printStatistics();

	g_bWaived = false;

	//Release
	cleanup(TRUE);

	CString strTime;
	strTime.Format(_T("Load %d ms, InitD311 %d ms, InitResource %d ms, Decoding = %d ms\n"), nEnd[0]-nStart[0], nEnd[1]-nStart[1], nEnd[2]-nStart[2], nEnd[3]-nStart[3] );
	TRACE(strTime);

	return;
#endif
}


void CESMGpuDecode::printStatistics()
{
	int   hh, mm, ss, msec;

	present_fps = 1.f / (total_time / (g_FrameCount * 1000.f));
	decoded_fps = 1.f / (total_time / (g_DecodeFrameCount * 1000.f));

	msec = ((int)total_time % 1000);
	ss   = (int)(total_time/1000) % 60;
	mm   = (int)(total_time/(1000*60)) % 60;
	hh   = (int)(total_time/(1000*60*60)) % 60;

	printf("\n[%s] statistics\n", sSDKname);
	printf("\t Video Length (hh:mm:ss.msec)   = %02d:%02d:%02d.%03d\n", hh, mm, ss, msec);

	printf("\t Frames Presented (inc repeats) = %d\n", g_FrameCount);
	printf("\t Average Present Rate     (fps) = %4.2f\n", present_fps);

	printf("\t Frames Decoded   (hardware)    = %d\n", g_DecodeFrameCount);
	printf("\t Average Rate of Decoding (fps) = %4.2f\n", decoded_fps);
}

// This is the CUDA stage for Video Post Processing.  Last stage takes care of the NV12 to ARGB
void CESMGpuDecode::cudaPostProcessFrame(CUdeviceptr *ppDecodedFrame, size_t nDecodedPitch,	CUdeviceptr *ppTextureData,  size_t nTexturePitch,CUmodule cuModNV12toARGB,	CUfunction fpCudaKernel, CUstream streamID)
{
	uint32 nWidth  = g_pVideoDecoder->targetWidth();
	uint32 nHeight = g_pVideoDecoder->targetHeight();

	// Upload the Color Space Conversion Matrices
	if (g_bUpdateCSC)
	{
		// CCIR 601/709
		float hueColorSpaceMat[9];
		setColorSpaceMatrix(g_eColorSpace,    hueColorSpaceMat, g_nHue);
		updateConstantMemory_drvapi(cuModNV12toARGB, hueColorSpaceMat);

		if (!g_bUpdateAll)
		{
			g_bUpdateCSC = false;
		}
	}

	// TODO: Stage for handling video post processing

	// Final Stage: NV12toARGB color space conversion
	CUresult eResult;
	eResult = cudaLaunchNV12toARGBDrv(*ppDecodedFrame, nDecodedPitch,
		*ppTextureData, nTexturePitch,
		nWidth, nHeight, fpCudaKernel, streamID);
}

// This is the CUDA stage for Video Post Processing.  Last stage takes care of the NV12 to ARGB
void CESMGpuDecode::cudaPostProcessFrame(CUdeviceptr *ppDecodedFrame, size_t nDecodedPitch,	CUarray array,CUmodule cuModNV12toARGB,CUfunction fpCudaKernel, CUstream streamID)
{
	uint32 nWidth  = g_pVideoDecoder->targetWidth();
	uint32 nHeight = g_pVideoDecoder->targetHeight();

	// Upload the Color Space Conversion Matrices
	if (g_bUpdateCSC)
	{
		// CCIR 601/709
		float hueColorSpaceMat[9];
		setColorSpaceMatrix(g_eColorSpace,    hueColorSpaceMat, g_nHue);
		updateConstantMemory_drvapi(cuModNV12toARGB, hueColorSpaceMat);

		if (!g_bUpdateAll)
		{
			g_bUpdateCSC = false;
		}
	}

	// TODO: Stage for handling video post processing

	// Final Stage: NV12toARGB color space conversion
	CUresult eResult;
	eResult = cudaLaunchNV12toARGBDrv(*ppDecodedFrame, nDecodedPitch,
		g_pRgba, nWidth * 4,
		nWidth, nHeight, fpCudaKernel, streamID);

	CUDA_MEMCPY2D memcpy2D = { 0 };
	memcpy2D.srcMemoryType = CU_MEMORYTYPE_DEVICE;
	memcpy2D.srcDevice = g_pRgba;
	memcpy2D.srcPitch = nWidth * 4;
	memcpy2D.dstMemoryType = CU_MEMORYTYPE_ARRAY;
	memcpy2D.dstArray = array;
	memcpy2D.dstPitch = nWidth * 4;
	memcpy2D.WidthInBytes = nWidth * 4;
	memcpy2D.Height = nHeight;

	// clear the surface to solid white
	checkCudaErrors(cuMemcpy2D(&memcpy2D));
}

void YUV_TO_RGB( BYTE* in, ESMFrameArray* pFrame, int nWidth, int nHeight )
{
	Mat YUV(nHeight+nHeight/2,nWidth,CV_8UC1,(uchar*)in);
	Mat BGR(nHeight,nWidth,CV_8UC3);
	int sCount,fCount;
	cvtColor(YUV,BGR,cv::COLOR_YUV2BGR_IYUV);


	IplImage* img = (IplImage*)pFrame->pImg;
	if(img)
	{
		if(img->width != nWidth || img->height != nHeight)
		{
			cvReleaseImage(&img);
			pFrame->pImg = (BYTE*)cvCreateImage(cvSize(nWidth,nHeight), IPL_DEPTH_8U, 3);
			img = (IplImage*)pFrame->pImg;
		}

		IplImage* copy_img = new IplImage(BGR);
		memcpy(img->imageData, copy_img->imageData, nWidth*nHeight*3);	
		delete copy_img;
	}
	else
	{
		img = cvCreateImage(cvSize(nWidth,nHeight), IPL_DEPTH_8U, 3);
		IplImage* copy_img = new IplImage(BGR);
		memcpy(img->imageData, copy_img->imageData, nWidth*nHeight*3);
		pFrame->pImg = (BYTE*)img;
		delete copy_img;
	}
}

void YUV_TO_RGB( BYTE* in, BYTE* out, int nWidth, int nHeight )
{
	Mat YUV(nHeight+nHeight/2,nWidth,CV_8UC1,(uchar*)in);
	Mat BGR(nHeight,nWidth,CV_8UC3);
	int sCount,fCount;

	cvtColor(YUV,BGR,cv::COLOR_YUV2BGR_IYUV);
	IplImage* copy_img = new IplImage(BGR);
	memcpy(out, copy_img->imageData, nWidth*nHeight*3);	
	delete copy_img;
	//cvReleaseImage(&copy_img);
}

void RGB_TO_YUV( BYTE* in, BYTE* out, int nWidth, int nHeight )
{
	IplImage* pImg = cvCreateImage(cvSize(nWidth,nHeight),IPL_DEPTH_8U, 3);
	memcpy(pImg->imageData, in, nWidth*nHeight*3);
	Mat RGB = cvarrToMat(pImg);
	Mat YUV(nHeight+nHeight/2,nWidth,CV_8UC1, out);


	cvtColor(RGB , YUV , cv::COLOR_RGB2YUV_IYUV);
	IplImage* copy_img = new IplImage(YUV);
	memcpy(out, copy_img->imageData, nWidth*nHeight*1.5);
	delete copy_img;
	cvReleaseImage(&pImg);
}

unsigned WINAPI CESMGpuDecode::imageAdjust(LPVOID param)
{
	//ThreadFrameData* pFrameData = (ThreadFrameData*)param;
	MakeFrameInfo* pFrameInfo = (MakeFrameInfo*)param;
	BOOL bSaveImg = FALSE;
	BOOL bReverse = FALSE;
	CString strPath;
	bSaveImg = FALSE;//pFrameData->bSaveImg;
	bReverse = pFrameInfo->bReverse;
	
	CESMMovieThreadManager *pMovieThreadManager = (CESMMovieThreadManager *)pFrameInfo->pParent;
	EffectInfo pEffectData = pFrameInfo->stEffectData;


	//YUV_TO_RGB(pFrameInfo->pYUVImg, (ESMFrameArray*)pFrameInfo->pFrame, pFrameInfo->nWidth, pFrameInfo->nHeight);
	IplImage* img = (IplImage*)pFrameInfo->Image;
	YUV_TO_RGB((BYTE*)pFrameInfo->pYUVImg, (BYTE*)img->imageData, pFrameInfo->nWidth, pFrameInfo->nHeight);
	//SendLog(1, _T("Convert YUV"));


	//ESMFrameArray* pData = (ESMFrameArray*)pFrameInfo->pFrame;
	//pFrameInfo->Image = pData->pImg;

	CESMMovieThreadImage MovieImage;
	int nSrcWidth = 0, nSrcHeight = 0, nOutputWidth = 0, nOutputHeight = 0 ;

	//Output 형식에 따른 데이터 변경.
	((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMovieSizeData(nSrcWidth, nSrcHeight, nOutputWidth, nOutputHeight);
	
	CString strDscId;
	CString strFramePath = pFrameInfo->strFramePath;
	int nIndex = strFramePath.ReverseFind('\\') + 1;
	CString strTpPath = strFramePath.Right(strFramePath.GetLength() - nIndex);
	strDscId = strTpPath.Left(5);
	stAdjustInfo AdjustData = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetAdjustData(strDscId);
	
	if(AdjustData.AdjAngle == 0 && AdjustData.AdjMove.x == 0 && AdjustData.AdjMove.y == 0)
	{
		CString strLog;
		strLog.Format(_T("[%s] Cannot Load Adjust"),strDscId);
		SendLog(5,strLog);
	}

	if(nSrcHeight == 1080 || nSrcWidth == 1920)
		TRACE(_T("==============Image Data Size Height = %d Width = %d\n"), nSrcHeight, nSrcWidth);

	int m_imagesize;
	m_imagesize = nSrcHeight*nSrcWidth*3;
	if(pFrameInfo->nImageSize != m_imagesize)
	{
		if(pFrameInfo->nImageSize == 1920 * 1080 * 3)
		{
			nSrcWidth = 1920;
			nSrcHeight = 1080;
		}
		else if(pFrameInfo->nImageSize == 3840 * 2160 * 3)
		{
			nSrcWidth = 3840;
			nSrcHeight = 2160;
		}
		m_imagesize = nSrcHeight*nSrcWidth*3;
	}

	Mat src(nSrcHeight, nSrcWidth, CV_8UC3);


	if(pFrameInfo->nImageSize != m_imagesize)
	{


		CString strMsg;
		strMsg.Format(_T("%s  record mode error"), pFrameInfo->strCameraID);
		pFrameInfo->bComplete++;
		return 0;
	}
	else
	{
		src = cvarrToMat(pFrameInfo->Image);
	}
	cuda::GpuMat *pImageMat = new cuda::GpuMat;//(nOutputHeight,nOutputWidth,CV_8UC3);

	if(!((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetVMCCFlag())
	{	
#if 0
		pImageMat->upload(src);

		if(pImageMat == NULL)
		{
			TRACE(_T("Image Error\n"));
			return -1;
		}

		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = 1;

		// Rotate 구현
		nRotateX = MovieImage.Round (AdjustData.AdjptRotate.x  * dRatio);
		nRotateY = MovieImage.Round (AdjustData.AdjptRotate.y  * dRatio);
		MovieImage.GpuRotateImage(pImageMat,nRotateX,nRotateY,AdjustData.AdjSize,AdjustData.AdjAngle,0);
		
		//-- MOVE EVENT
		int nMoveX = 0, nMoveY = 0;
		nMoveX = MovieImage.Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = MovieImage.Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
		MovieImage.GpuMoveImage(pImageMat, nMoveX,  nMoveY);

		//-- IMAGE CUT EVENT
		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
		{
			ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
			TRACE(_T("Image Adjust Error 2\n"));
			pFrameInfo->bComplete++;
			return 0;
		}

		if( pImageMat->cols > nOutputWidth)	
		{
			int nModifyMarginX = (pImageMat->cols - nOutputWidth) /2;
			int nModifyMarginY = (pImageMat->rows - nOutputHeight) /2;
			cuda::GpuMat pMatCut =  (*pImageMat)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(*pImageMat);
		}
		else
		{
			//MovieImage.GpuMakeMargin(pImageMat, nMarginX, nMarginY);

			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );
			cuda::GpuMat* pMatResize = new cuda::GpuMat;
			cuda::resize(*pImageMat, *pMatResize, Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
			cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(*pImageMat);
			delete pMatResize;
		}

		//-- Reverse Movie
		if(bReverse)
		{
			nRotateX = pImageMat->cols / 2;
			nRotateY = pImageMat->rows / 2;
			MovieImage.GpuRotateImage(pImageMat, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
		}

		Mat srcImage(pImageMat->rows, pImageMat->cols, CV_8UC3);
		pImageMat->download(srcImage);

#else	//Making Adjust Image using GPU

		if(nOutputWidth != nSrcWidth)
		{
			Mat d_resize;
			resize(src, d_resize, Size(nOutputWidth, nOutputHeight), 0,0,INTER_CUBIC );
			d_resize.copyTo(src);
		}
		//Cpu Rotate
		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
		nRotateX = MovieImage.Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
		nRotateY = MovieImage.Round(AdjustData.AdjptRotate.y  * dRatio );
		MovieImage.CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle,0);

		//Cpu Move
		int nMoveX = 0, nMoveY = 0;
		nMoveX = MovieImage.Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = MovieImage.Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
		MovieImage.CpuMoveImage(&src, nMoveX,  nMoveY);

		//Cpu Move
		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
		{
			RGB_TO_YUV((BYTE*)img->imageData, pFrameInfo->pYUVImg,  pFrameInfo->nWidth, pFrameInfo->nHeight);
			TRACE(_T("Image Adjust Error 2\n"));
			pFrameInfo->bComplete++;
			return 0;
		}
		if(src.cols > nOutputWidth)
		{
			int nLeft = (int)((src.cols - nOutputWidth)/2);
			int nTop  = (int)((src.rows - nOutputHeight)/2);
			Mat pMatCut =  (src)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(src);
		}
		else
		{
			//MovieImage.CpuMakeMargin(&src,nMarginX,nMarginY);

			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );	
			Mat  pMatResize;
			resize(src, pMatResize, cv::Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC);
			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
			Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(src);
			pMatResize = NULL;
		}
		if(bReverse)
		{
			nRotateX = src.cols / 2;
			nRotateY = src.rows / 2;
			MovieImage.CpuRotateImage(src, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
		}
		Mat srcImage = src.clone();
#endif
		//Insert Logo
		if( pFrameInfo->nDscIndex > -1)
		{
			// insert Logo 구현
			MovieImage.GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
		}
		if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
		{
			// insert Logo 구현
			MovieImage.KzonePrism(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->nPriValue,pFrameInfo->nPriValue2,pFrameInfo->nPrinumIndex);
		}

#if 1 //-- 카메라 ID 표시
		if( pFrameInfo->strCameraID[0] != _T('\0'))
			MovieImage.GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bColorRevision);
#endif
		if(pFrameInfo->bInsertWB)
		{
			Ptr<xphoto::WhiteBalancer> wb;
			wb = xphoto::createGrayworldWB();
			wb->balanceWhite(srcImage, srcImage);
		}

		int size = srcImage.total() * srcImage.elemSize();
		memcpy( img->imageData, srcImage.data, size);
		pFrameInfo->bComplete++;
			
	}
	else if(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetVMCCFlag() && nSrcWidth == FULL_HD_WIDTH && nSrcHeight == FULL_HD_HEIGHT)
	{
#if 0
		cuda::GpuMat *pImageMat = new cuda::GpuMat;
		pImageMat->upload(src);

		int nPreWidth = nOutputWidth;
		int nPreHeight = nOutputHeight;

		nOutputWidth = nSrcWidth;
		nOutputHeight = nSrcHeight;

		// Rotate 구현
		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
		nRotateX = MovieImage.Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
		nRotateY = MovieImage.Round(AdjustData.AdjptRotate.y  * dRatio );
		MovieImage.GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);

		//-- MOVE EVENT
		int nMoveX = 0, nMoveY = 0;
		nMoveX = MovieImage.Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = MovieImage.Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
		MovieImage.GpuMoveImage(pImageMat, nMoveX,  nMoveY);

		//-- IMAGE CUT EVENT
		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
		{
			ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
			TRACE(_T("Image Adjust Error 2\n"));
			pFrameInfo->bComplete++;
			return 0;
		}

		if( pImageMat->cols > nOutputWidth)	
		{
			int nModifyMarginX = (pImageMat->cols - nOutputWidth) /2;
			int nModifyMarginY = (pImageMat->rows - nOutputHeight) /2;
			cuda::GpuMat pMatCut =  (*pImageMat)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(*pImageMat);
		}
		else
		{
			//MovieImage.GpuMakeMargin(pImageMat, nMarginX, nMarginY);
			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );
			cuda::GpuMat* pMatResize = new cuda::GpuMat;
			cuda::resize(*pImageMat, *pMatResize, Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
			cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(*pImageMat);
			delete pMatResize;
		}

		if(pFrameInfo->bLogoZoom)
		{
//#ifdef LOGO_ZOOM //Logo Zoom 전
			if( pFrameInfo->nDscIndex > -1)
			{
				Mat srcLogo(pImageMat->rows, pImageMat->cols, CV_8UC3);
				pImageMat->download(srcLogo);
				// insert Logo 구현
				MovieImage.GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
				pImageMat->upload(srcLogo);
			}
			if( pFrameInfo->nPriIndex > -1)
			{
				Mat srcPrism(pImageMat->rows, pImageMat->cols, CV_8UC3);
				pImageMat->download(srcPrism);
				MovieImage.KzonePrism(&srcPrism,pFrameInfo->nPriIndex);
				pImageMat->upload(srcPrism);
			}
//#endif
		}

		//Zoom Move
		double dScale = (double)pEffectData.nZoomRatio / 100.0;

		if(dScale == 0)
		{
			//Template 오류
			TRACE(_T("Template Error"));
			return 0;
		}

		cuda::GpuMat gMatCut, gMatResize;

		int nLeft = (pEffectData.nPosX- nSrcWidth/dScale/2);
		int nTop = (pEffectData.nPosY - nSrcHeight/dScale/2);
		int nWidth = (nSrcWidth/dScale - 1);
		if((nLeft + nWidth) > nSrcWidth)
			nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
		else if(nLeft < 0)
			nLeft = 0;
		int nHeight = (nSrcHeight/dScale - 1);
		if((nTop + nHeight) > nSrcHeight)
			nTop = nTop - ((nTop + nHeight) - nSrcHeight);
		else if (nTop < 0)
			nTop = 0;
		gMatCut =  (*pImageMat)(Rect(nLeft, nTop, nWidth, nHeight));
		gMatCut.copyTo(*pImageMat);

		//VMCC FHD->FHD
		cuda::resize(*pImageMat, gMatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );

		gMatResize.copyTo(*pImageMat);


		//-- Reverse Movie
		if(bReverse)
		{
			nRotateX = pImageMat->cols / 2;
			nRotateY = pImageMat->rows / 2;
			MovieImage.GpuRotateImage(pImageMat, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
		}

		Mat srcImage(pImageMat->rows, pImageMat->cols, CV_8UC3);
		pImageMat->download(srcImage);

#else
		int nPreWidth = nOutputWidth;
		int nPreHeight = nOutputHeight;

		nOutputWidth = nSrcWidth;
		nOutputHeight = nSrcHeight;

		//Cpu Rotate
		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
		nRotateX = MovieImage.Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
		nRotateY = MovieImage.Round(AdjustData.AdjptRotate.y  * dRatio );
		MovieImage.CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle,0);

		//Cpu Move
		int nMoveX = 0, nMoveY = 0;
		nMoveX = MovieImage.Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = MovieImage.Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
		MovieImage.CpuMoveImage(&src, nMoveX,  nMoveY);

		//Cpu Margin
		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
		{
			RGB_TO_YUV((BYTE*)img->imageData, pFrameInfo->pYUVImg,  pFrameInfo->nWidth, pFrameInfo->nHeight);
			TRACE(_T("Image Adjust Error 2\n"));
			pFrameInfo->bComplete++;
			return 0;
		}

		if( src.cols > nOutputWidth)	
		{
			int nModifyMarginX = (src.cols - nOutputWidth) /2;
			int nModifyMarginY = (src.rows - nOutputHeight) /2;
			Mat pMatCut =  (src)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
			src = pMatCut.clone();
		}
		else
		{
			//MovieImage.CpuMakeMargin(&src,nMarginX,nMarginY);

			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );	
			Mat  pMatResize;
			resize(src, pMatResize, cv::Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC);
			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
			Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(src);
			pMatResize = NULL;
		}

		if(pFrameInfo->bLogoZoom)
		{
//#ifdef LOGO_ZOOM //Logo Zoom 전
			if( pFrameInfo->nDscIndex > -1)
			{
				Mat srcLogo(src.rows, src.cols, CV_8UC3);
				srcLogo = src.clone();
				// insert Logo 구현
				MovieImage.GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
				src = srcLogo.clone();
			}
			if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
			{
				Mat srcPrism(src.rows, src.cols, CV_8UC3);
				srcPrism = src.clone();
				MovieImage.KzonePrism(&srcPrism,pFrameInfo->nPriIndex,pFrameInfo->nPriValue,pFrameInfo->nPriValue2,pFrameInfo->nPrinumIndex);
				src = srcPrism.clone();
			}
//#endif
		}
		
		double dScale = (double)pEffectData.nZoomRatio / 100.0;

		if(dScale == 0)
		{
			RGB_TO_YUV((BYTE*)img->imageData, pFrameInfo->pYUVImg,  pFrameInfo->nWidth, pFrameInfo->nHeight);
			pFrameInfo->bComplete++;

			return 0;
		}

		Mat MatCut, MatResize;

		int nLeft = (pEffectData.nPosX- nSrcWidth/dScale/2);
		int nTop = (pEffectData.nPosY - nSrcHeight/dScale/2);
		int nWidth = (nSrcWidth/dScale - 1);
		if((nLeft + nWidth) > nSrcWidth)
			nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
		else if(nLeft < 0)
			nLeft = 0;
		int nHeight = (nSrcHeight/dScale - 1);
		if((nTop + nHeight) > nSrcHeight)
			nTop = nTop - ((nTop + nHeight) - nSrcHeight);
		else if (nTop < 0)
			nTop = 0;
		MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
		src = MatCut.clone();

		////VMCC FHD->FHD
		resize(src, MatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
		MatResize.copyTo(src);


		////-- Reverse Movie
		if(bReverse)
		{
			nRotateX = src.cols / 2;
			nRotateY = src.rows / 2;
			MovieImage.CpuRotateImage(src, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
		}

		Mat srcImage = src.clone();
#endif
		

		if(!pFrameInfo->bLogoZoom)
		{
//#ifndef LOGO_ZOOM //Logo Zoom 후
			//Insert Logo
			if( pFrameInfo->nDscIndex > -1)
			{
				// insert Logo 구현
				MovieImage.GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
			}
//#endif
		}


#if 1 //-- 카메라 ID 표시
		if( pFrameInfo->strCameraID[0] != _T('\0'))
			MovieImage.GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bColorRevision);
#endif
		if(pFrameInfo->bInsertWB)
		{
			Ptr<xphoto::WhiteBalancer> wb;
			wb = xphoto::createGrayworldWB();
			wb->balanceWhite(srcImage, srcImage);
		}
		int size = srcImage.total() * srcImage.elemSize();
		memcpy( img->imageData, srcImage.data, size);
		pFrameInfo->bComplete++;

	}
	else
	{
#if 0
		cuda::GpuMat *pImageMat = new cuda::GpuMat;
		int nPreWidth = nOutputWidth;
		int nPreHeight = nOutputHeight;

		nOutputWidth = nSrcWidth;
		nOutputHeight = nSrcHeight;

		// Rotate 구현
		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
		nRotateX = MovieImage.Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
		nRotateY = MovieImage.Round(AdjustData.AdjptRotate.y  * dRatio );
		MovieImage.GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);

		//-- MOVE EVENT
		int nMoveX = 0, nMoveY = 0;
		nMoveX = MovieImage.Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = MovieImage.Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
		MovieImage.GpuMoveImage(pImageMat, nMoveX,  nMoveY);

		//-- IMAGE CUT EVENT
		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
		{
			ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
			TRACE(_T("Image Adjust Error 2\n"));
			pFrameInfo->bComplete++;
			return 0;
		}

		if( pImageMat->cols > nOutputWidth)	
		{
			int nModifyMarginX = (pImageMat->cols - nOutputWidth) /2;
			int nModifyMarginY = (pImageMat->rows - nOutputHeight) /2;
			cuda::GpuMat pMatCut =  (*pImageMat)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(*pImageMat);
		}
		else
		{
			//MovieImage.GpuMakeMargin(pImageMat, nMarginX, nMarginY);
			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );
			cuda::GpuMat* pMatResize = new cuda::GpuMat;
			cuda::resize(*pImageMat, *pMatResize, Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
			cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(*pImageMat);
			delete pMatResize;
		}

		if(pFrameInfo->bLogoZoom)
		{
//#ifdef LOGO_ZOOM //Logo Zoom 전
			if( pFrameInfo->nDscIndex > -1)
			{
				Mat srcLogo(pImageMat->rows, pImageMat->cols, CV_8UC3);
				pImageMat->download(srcLogo);
				// insert Logo 구현
				MovieImage.GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
				pImageMat->upload(srcLogo);
			}
			if( pFrameInfo->nPriIndex > -1)
			{
				Mat srcLogo(pImageMat->rows, pImageMat->cols, CV_8UC3);
				pImageMat->download(srcLogo);
				MovieImage.KzonePrism(&srcLogo,pFrameInfo->nPriIndex);
			}
//#endif
		}


		//Zoom Move
		double dScale = (double)pEffectData.nZoomRatio / 100.0;

		if(dScale == 0)
		{
			//Template 오류
			TRACE(_T("Template Error"));
			return 0;
		}

		cuda::GpuMat gMatCut, gMatResize;

		int nLeft = (pEffectData.nPosX- nSrcWidth/dScale/2);
		int nTop = (pEffectData.nPosY - nSrcHeight/dScale/2);
		int nWidth = (nSrcWidth/dScale - 1);
		if((nLeft + nWidth) > nSrcWidth)
			nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
		else if(nLeft < 0)
			nLeft = 0;
		int nHeight = (nSrcHeight/dScale - 1);
		if((nTop + nHeight) > nSrcHeight)
			nTop = nTop - ((nTop + nHeight) - nSrcHeight);
		else if (nTop < 0)
			nTop = 0;
		gMatCut =  (*pImageMat)(Rect(nLeft, nTop, nWidth, nHeight));
		gMatCut.copyTo(*pImageMat);

		//VMCC UHD->UHD
		if(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetUHDtoFHD())
			cuda::resize(*pImageMat, gMatResize, Size(nPreWidth ,nPreHeight), 0.0, 0.0, INTER_CUBIC );
		else
			cuda::resize(*pImageMat, gMatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );

		gMatResize.copyTo(*pImageMat);

		//
		//-- Reverse Movie
		if(bReverse)
		{
			nRotateX = pImageMat->cols / 2;
			nRotateY = pImageMat->rows / 2;
			MovieImage.GpuRotateImage(pImageMat, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
		}

		Mat srcImage(pImageMat->rows, pImageMat->cols, CV_8UC3);
		pImageMat->download(srcImage);


#else
		int nPreWidth = nOutputWidth;
		int nPreHeight = nOutputHeight;

		nOutputWidth = nSrcWidth;
		nOutputHeight = nSrcHeight;

		//Cpu Rotate
		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
		nRotateX = MovieImage.Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
		nRotateY = MovieImage.Round(AdjustData.AdjptRotate.y  * dRatio );
		MovieImage.CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle,0);

		//Cpu Move
		int nMoveX = 0, nMoveY = 0;
		nMoveX = MovieImage.Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = MovieImage.Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
		MovieImage.CpuMoveImage(&src, nMoveX,  nMoveY);

		//Cpu Margin
		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
		{
			RGB_TO_YUV((BYTE*)img->imageData, pFrameInfo->pYUVImg,  pFrameInfo->nWidth, pFrameInfo->nHeight);
			TRACE(_T("Image Adjust Error 2\n"));
			pFrameInfo->bComplete++;
			return 0;
		}

		if( src.cols > nOutputWidth)	
		{
			int nModifyMarginX = (src.cols - nOutputWidth) /2;
			int nModifyMarginY = (src.rows - nOutputHeight) /2;
			Mat pMatCut =  (src)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
			src = pMatCut.clone();
		}
		else
		{
			//MovieImage.CpuMakeMargin(&src,nMarginX,nMarginY);

			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );	
			Mat  pMatResize;
			resize(src, pMatResize, cv::Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC);
			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
			Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(src);
			pMatResize = NULL;
		}

		if(pFrameInfo->bLogoZoom)
		{
//#ifdef LOGO_ZOOM //Logo Zoom 전
			if( pFrameInfo->nDscIndex > -1)
			{
				Mat srcLogo(src.rows, src.cols, CV_8UC3);
				srcLogo = src.clone();
				// insert Logo 구현
				MovieImage.GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
				src = srcLogo.clone();
			}
			if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
			{
				Mat srcPrism(src.rows, src.cols, CV_8UC3);
				srcPrism = srcPrism.clone();
				MovieImage.KzonePrism(&srcPrism,pFrameInfo->nPriIndex,pFrameInfo->nPriValue,pFrameInfo->nPriValue2,pFrameInfo->nPrinumIndex);
				src = srcPrism.clone();
			}
//#endif
		}

		double dScale = (double)pEffectData.nZoomRatio / 100.0;

		if(dScale == 0)
		{
			RGB_TO_YUV((BYTE*)img->imageData, pFrameInfo->pYUVImg,  pFrameInfo->nWidth, pFrameInfo->nHeight);
			pFrameInfo->bComplete++;
			return 0;
		}

		Mat MatCut, MatResize;

		int nLeft = (pEffectData.nPosX- nSrcWidth/dScale/2);
		int nTop = (pEffectData.nPosY - nSrcHeight/dScale/2);
		int nWidth = (nSrcWidth/dScale - 1);
		if((nLeft + nWidth) > nSrcWidth)
			nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
		else if(nLeft < 0)
			nLeft = 0;
		int nHeight = (nSrcHeight/dScale - 1);
		if((nTop + nHeight) > nSrcHeight)
			nTop = nTop - ((nTop + nHeight) - nSrcHeight);
		else if (nTop < 0)
			nTop = 0;
		MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
		src = MatCut.clone();

		if(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetUHDtoFHD())
		{
			resize(src, MatResize, Size(nPreWidth ,nPreHeight), 0.0, 0.0, INTER_CUBIC );
			MatResize.copyTo(src);
		}
		else
		{
			resize(src, MatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
			MatResize.copyTo(src);
		}



		////-- Reverse Movie
		if(bReverse)
		{
			nRotateX = src.cols / 2;
			nRotateY = src.rows/ 2;
			MovieImage.CpuRotateImage(src, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
		}

		Mat srcImage = src.clone();
#endif
		
#if 1 //-- 카메라 ID 표시
		if( pFrameInfo->strCameraID[0] != _T('\0'))
			MovieImage.GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bColorRevision);
#endif


		if(!pFrameInfo->bLogoZoom)
		{
//#ifndef LOGO_ZOOM //Logo Zoom 후
			//Insert Logo
			if( pFrameInfo->nDscIndex > -1)
			{
				// insert Logo 구현
				MovieImage.GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
			}
//#endif
		}

		if(pFrameInfo->bInsertWB)
		{
			Ptr<xphoto::WhiteBalancer> wb;
			wb = xphoto::createGrayworldWB();
			wb->balanceWhite(srcImage, srcImage);
		}
		int size = srcImage.total() * srcImage.elemSize();
		memcpy( img->imageData, srcImage.data, size);
		pFrameInfo->bComplete++;
	}

	//RGB_TO_YUV((BYTE*)img->imageData, pFrameInfo->pYUVImg,  pFrameInfo->nWidth, pFrameInfo->nHeight);
	//cvReleaseImage(&img);
	//pFrameInfo->Image = NULL;
	delete pImageMat;
	return 0;
}

void CESMGpuDecode::imageAdjust(ThreadFrameData* pFrameData)
{
	BOOL bSaveImg = FALSE;
	BOOL bReverse = FALSE;
	CString strPath;
	bSaveImg = FALSE;//pFrameData->bSaveImg;
	strPath = pFrameData->strPath;
	bReverse = pFrameData->bReverse;
	MakeFrameInfo* pFrameInfo = pFrameData->pFrameInfo ;
	CESMMovieThreadManager *pMovieThreadManager = pFrameData->pMovieThreadManager;
	delete pFrameData;
	EffectInfo pEffectData = pFrameInfo->stEffectData;

	
	CESMMovieThreadImage MovieImage;
	int nSrcWidth = 0, nSrcHeight = 0, nOutputWidth = 0, nOutputHeight = 0 ;

	//Output 형식에 따른 데이터 변경.
	((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMovieSizeData(nSrcWidth, nSrcHeight, nOutputWidth, nOutputHeight);
	
	CString strDscId;
	CString strFramePath = pFrameInfo->strFramePath;
	int nIndex = strFramePath.ReverseFind('\\') + 1;
	CString strTpPath = strFramePath.Right(strFramePath.GetLength() - nIndex);
	strDscId = strTpPath.Left(5);
	stAdjustInfo AdjustData = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetAdjustData(strDscId);
		
	
	if(nSrcHeight == 1080 || nSrcWidth == 1920)
		TRACE(_T("==============Image Data Size Height = %d Width = %d\n"), nSrcHeight, nSrcWidth);

	int m_imagesize;
	m_imagesize = nSrcHeight*nSrcWidth*3;
	if(pFrameInfo->nImageSize != m_imagesize)
	{
		if(pFrameInfo->nImageSize == 1920 * 1080 * 3)
		{
			nSrcWidth = 1920;
			nSrcHeight = 1080;
		}
		else if(pFrameInfo->nImageSize == 3840 * 2160 * 3)
		{
			nSrcWidth = 3840;
			nSrcHeight = 2160;
		}
		m_imagesize = nSrcHeight*nSrcWidth*3;
	}

	Mat src(nSrcHeight, nSrcWidth, CV_8UC3);
	

	if(pFrameInfo->nImageSize != m_imagesize)
	{
		CString strMsg;
		strMsg.Format(_T("%s  record mode error"), pFrameInfo->strCameraID);
		pFrameInfo->bComplete++;
		return;
	}
	else
	{
		src = cvarrToMat(pFrameInfo->Image);
	}

	if(!((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetVMCCFlag())
	{	
		if(nOutputWidth != nSrcWidth)
		{
			Mat d_resize;
			resize(src, d_resize, Size(nOutputWidth, nOutputHeight), 0,0,INTER_CUBIC );
			d_resize.copyTo(src);
		}
		//Cpu Rotate
		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
		nRotateX = MovieImage.Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
		nRotateY = MovieImage.Round(AdjustData.AdjptRotate.y  * dRatio );
		MovieImage.CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle,0);

		//Cpu Move
		int nMoveX = 0, nMoveY = 0;
		nMoveX = MovieImage.Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = MovieImage.Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
		MovieImage.CpuMoveImage(&src, nMoveX,  nMoveY);

		//Cpu Move
		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
		{
			TRACE(_T("Image Adjust Error 2\n"));
			pFrameInfo->bComplete++;
			return;
		}
		if(src.cols > nOutputWidth)
		{
			int nLeft = (int)((src.cols - nOutputWidth)/2);
			int nTop  = (int)((src.rows - nOutputHeight)/2);
			Mat pMatCut =  (src)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(src);
		}
		else
		{
			//MovieImage.CpuMakeMargin(&src,nMarginX,nMarginY);

			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );	
			Mat  pMatResize;
			resize(src, pMatResize, cv::Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC);
			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
			Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(src);
			pMatResize = NULL;
		}
		if(bReverse)
		{
			nRotateX = src.cols / 2;
			nRotateY = src.rows / 2;
			MovieImage.CpuRotateImage(src, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
		}
		Mat srcImage = src.clone();
		//Insert Logo

		if( pFrameInfo->nDscIndex > -1)
		{
			// insert Logo 구현
			MovieImage.GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
		}
		if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
		{
			// insert Logo 구현
			MovieImage.KzonePrism(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->nPriValue,pFrameInfo->nPriValue2,pFrameInfo->nPrinumIndex);
		}

#if 1 //-- 카메라 ID 표시
		if( pFrameInfo->strCameraID[0] != _T('\0'))
			MovieImage.GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bColorRevision);
#endif


		delete[] pFrameInfo->Image;
		pFrameInfo->Image = NULL;

		int size = srcImage.total() * srcImage.elemSize();
		pFrameInfo->Image = new BYTE[size];
		memcpy( pFrameInfo->Image, srcImage.data, size);
		//delete pImageMat;
		pFrameInfo->bComplete++;

		//BMP Save
		if(bSaveImg)
		{
			//IplImage*	img = new IplImage(srcImage);
			cv::Mat mImg(srcImage);
			CString strData , strTemp;
			char path[MAX_PATH];
			strData.Format(_T("%s"), strPath);
			CreateDirectory(strData,NULL);
			strTemp.Format(_T("\\%s_%d_%d.bmp"), strDscId, nIndex, pFrameInfo->nFrameIndex);
			strData.Append(strTemp);
			sprintf(path, "%S", strData);
			imwrite(path, mImg);
			//cvSaveImage(path, img);
		}
	}
	else if(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetVMCCFlag() && nSrcWidth == FULL_HD_WIDTH && nSrcHeight == FULL_HD_HEIGHT)
	{
		int nPreWidth = nOutputWidth;
		int nPreHeight = nOutputHeight;

		nOutputWidth = nSrcWidth;
		nOutputHeight = nSrcHeight;

		//Cpu Rotate
		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
		nRotateX = MovieImage.Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
		nRotateY = MovieImage.Round(AdjustData.AdjptRotate.y  * dRatio );
		MovieImage.CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle,0);

		//Cpu Move
		int nMoveX = 0, nMoveY = 0;
		nMoveX = MovieImage.Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = MovieImage.Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
		MovieImage.CpuMoveImage(&src, nMoveX,  nMoveY);

		//Cpu Margin
		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
		{
			TRACE(_T("Image Adjust Error 2\n"));
			pFrameInfo->bComplete++;
			return;
		}

		if( src.cols > nOutputWidth)	
		{
			int nModifyMarginX = (src.cols - nOutputWidth) /2;
			int nModifyMarginY = (src.rows - nOutputHeight) /2;
			Mat pMatCut =  (src)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
			src = pMatCut.clone();
		}
		else
		{
			//MovieImage.CpuMakeMargin(&src,nMarginX,nMarginY);

			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );	
			Mat  pMatResize;
			resize(src, pMatResize, cv::Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC);
			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
			Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(src);
			pMatResize = NULL;
		}

		if(pFrameInfo->bLogoZoom)
		{
//#ifdef LOGO_ZOOM //Logo Zoom 전
			if( pFrameInfo->nDscIndex > -1)
			{
				Mat srcLogo(src.rows, src.cols, CV_8UC3);
				srcLogo = src.clone();
				// insert Logo 구현
				MovieImage.GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
				src = srcLogo.clone();
			}
			if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
			{
				Mat srcPrism(src.rows, src.cols, CV_8UC3);
				srcPrism = src.clone();
				MovieImage.KzonePrism(&srcPrism,pFrameInfo->nPriIndex,pFrameInfo->nPriValue,pFrameInfo->nPriValue2,pFrameInfo->nPrinumIndex);
				src = srcPrism.clone();
			}
//#endif
		}

		double dScale = (double)pEffectData.nZoomRatio / 100.0;

		if(dScale == 0)
		{
			//Template 오류
			pFrameInfo->bComplete++;
			TRACE(_T("Template Error"));
			return;
		}

		Mat MatCut, MatResize;

		int nLeft = (pEffectData.nPosX- nSrcWidth/dScale/2);
		int nTop = (pEffectData.nPosY - nSrcHeight/dScale/2);
		int nWidth = (nSrcWidth/dScale - 1);
		if((nLeft + nWidth) > nSrcWidth)
			nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
		else if(nLeft < 0)
			nLeft = 0;
		int nHeight = (nSrcHeight/dScale - 1);
		if((nTop + nHeight) > nSrcHeight)
			nTop = nTop - ((nTop + nHeight) - nSrcHeight);
		else if (nTop < 0)
			nTop = 0;
		MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
		src = MatCut.clone();

		////VMCC FHD->FHD
		resize(src, MatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
		MatResize.copyTo(src);


		////-- Reverse Movie
		if(bReverse)
		{
			nRotateX = src.cols / 2;
			nRotateY = src.rows / 2;
			MovieImage.CpuRotateImage(src, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
		}

		Mat srcImage = src.clone();

		if(!pFrameInfo->bLogoZoom)
		{
//#ifndef LOGO_ZOOM //Logo Zoom 후
			//Insert Logo
			if( pFrameInfo->nDscIndex > -1)
			{
				// insert Logo 구현
				MovieImage.GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
			}
//#endif
		}


#if 1 //-- 카메라 ID 표시
		if( pFrameInfo->strCameraID[0] != _T('\0'))
			MovieImage.GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bColorRevision);
#endif

		delete[] pFrameInfo->Image;
		pFrameInfo->Image = NULL;

		int size = srcImage.total() * srcImage.elemSize();
		pFrameInfo->Image = new BYTE[size];
		memcpy( pFrameInfo->Image, srcImage.data, size);
		//delete pImageMat;
		pFrameInfo->bComplete++;

		//BMP Save
		if(bSaveImg)
		{
			//IplImage*	img = new IplImage(srcImage);
			cv::Mat mImg(srcImage);
			CString strData , strTemp;
			char path[MAX_PATH];
			strData.Format(_T("%s"), strPath);
			CreateDirectory(strData,NULL);
			strTemp.Format(_T("\\%s_%d_%d.bmp"), strDscId, nIndex, pFrameInfo->nFrameIndex);
			strData.Append(strTemp);
			sprintf(path, "%S", strData);
			imwrite(path, mImg);
		}
	}
	else
	{
		int nPreWidth = nOutputWidth;
		int nPreHeight = nOutputHeight;

		nOutputWidth = nSrcWidth;
		nOutputHeight = nSrcHeight;

		//Cpu Rotate
		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
		nRotateX = MovieImage.Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
		nRotateY = MovieImage.Round(AdjustData.AdjptRotate.y  * dRatio );
		MovieImage.CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle,0);

		//Cpu Move
		int nMoveX = 0, nMoveY = 0;
		nMoveX = MovieImage.Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = MovieImage.Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
		MovieImage.CpuMoveImage(&src, nMoveX,  nMoveY);

		//Cpu Margin
		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
		{
			TRACE(_T("Image Adjust Error 2\n"));
			pFrameInfo->bComplete++;
			return;
		}

		if( src.cols > nOutputWidth)	
		{
			int nModifyMarginX = (src.cols - nOutputWidth) /2;
			int nModifyMarginY = (src.rows - nOutputHeight) /2;
			Mat pMatCut =  (src)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
			src = pMatCut.clone();
		}
		else
		{
			//MovieImage.CpuMakeMargin(&src,nMarginX,nMarginY);

			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );	
			Mat  pMatResize;
			resize(src, pMatResize, cv::Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC);
			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
			Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(src);
			pMatResize = NULL;
		}

		if(pFrameInfo->bLogoZoom)
		{
//#ifdef LOGO_ZOOM //Logo Zoom 전
			if( pFrameInfo->nDscIndex > -1)
			{
				Mat srcLogo(src.rows, src.cols, CV_8UC3);
				srcLogo = src.clone();
				// insert Logo 구현
				MovieImage.GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
				src = srcLogo.clone();
			}
			if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
			{
				Mat srcPrism(src.rows, src.cols, CV_8UC3);
				srcPrism = src.clone();
				MovieImage.KzonePrism(&srcPrism,pFrameInfo->nPriIndex,pFrameInfo->nPriValue,pFrameInfo->nPriValue2,pFrameInfo->nPrinumIndex);
				src = srcPrism.clone();
			}
//#endif
		}
		
		double dScale = (double)pEffectData.nZoomRatio / 100.0;

		if(dScale == 0)
		{
			//Template 오류
			TRACE(_T("Template Error"));
			return;
		}

		Mat MatCut, MatResize;

		int nLeft = (pEffectData.nPosX- nSrcWidth/dScale/2);
		int nTop = (pEffectData.nPosY - nSrcHeight/dScale/2);
		int nWidth = (nSrcWidth/dScale - 1);
		if((nLeft + nWidth) > nSrcWidth)
			nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
		else if(nLeft < 0)
			nLeft = 0;
		int nHeight = (nSrcHeight/dScale - 1);
		if((nTop + nHeight) > nSrcHeight)
			nTop = nTop - ((nTop + nHeight) - nSrcHeight);
		else if (nTop < 0)
			nTop = 0;
		MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
		src = MatCut.clone();

		if(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetUHDtoFHD())
		{
			resize(src, MatResize, Size(nPreWidth ,nPreHeight), 0.0, 0.0, INTER_CUBIC );
			MatResize.copyTo(src);
		}
		else
		{
			resize(src, MatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
			MatResize.copyTo(src);
		}
		


		////-- Reverse Movie
		if(bReverse)
		{
			nRotateX = src.cols / 2;
			nRotateY = src.rows/ 2;
			MovieImage.CpuRotateImage(src, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
		}

		Mat srcImage = src.clone();
#if 1 //-- 카메라 ID 표시
		if( pFrameInfo->strCameraID[0] != _T('\0'))
			MovieImage.GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bColorRevision);
#endif


		if(!pFrameInfo->bLogoZoom)
		{
//#ifndef LOGO_ZOOM //Logo Zoom 후
			//Insert Logo
			if( pFrameInfo->nDscIndex > -1)
			{
				// insert Logo 구현
				MovieImage.GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
			}
//#endif
		}




		delete[] pFrameInfo->Image;
		pFrameInfo->Image = NULL;

		int size = srcImage.total() * srcImage.elemSize();
		pFrameInfo->Image = new BYTE[size];
		memcpy( pFrameInfo->Image, srcImage.data, size);
		//delete pImageMat;
		pFrameInfo->bComplete++;

		//BMP Save
		if(bSaveImg)
		{
			//IplImage*	img = new IplImage(srcImage);
			cv::Mat mImg(srcImage);
			CString strData , strTemp;
			char path[MAX_PATH];
			strData.Format(_T("%s"), strPath);
			CreateDirectory(strData,NULL);
			strTemp.Format(_T("\\%s_%d_%d.bmp"), strDscId, nIndex, pFrameInfo->nFrameIndex);
			strData.Append(strTemp);
			sprintf(path, "%S", strData);
			imwrite(path, mImg);
			//cvSaveImage(path, img);
		}
	}
	return;
}

// YUV 영상을 RGB 영상으로 바꾸는 함수
void CESMGpuDecode::yuv420_to_rgb( BYTE* in, ESMFrameArray* pFrame, int nWidth, int nHeight )
{
	Mat YUV(nHeight+nHeight/2,nWidth,CV_8UC1,(uchar*)in);
	Mat BGR(nHeight,nWidth,CV_8UC3);
	int sCount,fCount;
	cvtColor(YUV,BGR,cv::COLOR_YUV2BGR_IYUV);


	IplImage* img = (IplImage*)pFrame->pImg;
	if(img)
	{
		if(img->width != nWidth || img->height != nHeight)
		{
			cvReleaseImage(&img);
			pFrame->pImg = (BYTE*)cvCreateImage(cvSize(nWidth,nHeight), IPL_DEPTH_8U, 3);
			img = (IplImage*)pFrame->pImg;
		}

		IplImage* copy_img = new IplImage(BGR);
		memcpy(img->imageData, copy_img->imageData, nWidth*nHeight*3);	
		delete copy_img;
	}
	else
	{
		img = cvCreateImage(cvSize(nWidth,nHeight), IPL_DEPTH_8U, 3);
		IplImage* copy_img = new IplImage(BGR);
		memcpy(img->imageData, copy_img->imageData, nWidth*nHeight*3);
		pFrame->pImg = (BYTE*)img;
		delete copy_img;
	}
}

void CESMGpuDecode::yuv420_to_rgb( unsigned char *in, unsigned char *out, int w, int h )
{
    int x,y;
    double imgsize = w*h;
    int w3 = w*3;
    double uvsize = imgsize/4.0;
	unsigned char *pY = in;
	unsigned char *pV = in + (int)imgsize;
    unsigned char *pU = in + (int)imgsize + (int)uvsize;
    int y00, y01, y10, y11;
    int u,v;    
    unsigned char *p;

	// 윈도우에서는 영상의 상하가 거꾸로 저장되지 때문에 아래와 같이 코드 작성.
    /*for( y=0; y<=h-2; y+=2 )
    {
        for( x=0; x<=w-2; x+=2 )
        {
            p = out + w3*(h-y-1) + x*3;
            u = *pU;
            v = *pV;
            y00 = *pY;
            y01 = *(pY+1);
            y10 = *(pY+w);
            y11 = *(pY+w+1);
            *(p)        = YUV_B[y00][u];
            *(p+1)      = YUV_G[y00][u][v];
            *(p+2)      = YUV_R[y00][v];
            *(p+3)      = YUV_B[y01][u];
            *(p+3+1)    = YUV_G[y01][u][v];
            *(p+3+2)    = YUV_R[y01][v];
            *(p-w3)     = YUV_B[y10][u];
            *(p-w3+1)   = YUV_G[y10][u][v];
            *(p-w3+2)   = YUV_R[y10][v];
            *(p-w3+3)   = YUV_B[y11][u];
            *(p-w3+3+1) = YUV_G[y11][u][v];
            *(p-w3+3+2) = YUV_R[y11][v];
	
			pU++;
            pV++;
            pY = pY + 2;
        }
        pY = pY + w;
    }*/
    // 일반적인 경우 아래의 코드 사용함.
	//#pragma omp parallel for
	/*for( y=0; y<=h-2; y+=2 )
    {
		//#pragma omp parallel for
        for( x=0; x<=w-2; x+=2 )
        {
            p = out + w3*y + x*3;
            u = *pU;
            v = *pV;

            y00 = *pY;
            y01 = *(pY+1);
            y10 = *(pY+w);
            y11 = *(pY+w+1);

            *(p)        = YUV_B[y00][v];
            *(p+1)      = YUV_G[y00][u][v];
            *(p+2)      = YUV_R[y00][u]; 
            *(p+3)      = YUV_B[y01][v];
            *(p+3+1)    = YUV_G[y01][u][v];
            *(p+3+2)    = YUV_R[y01][u];
            *(p+w3)     = YUV_B[y10][v];
            *(p+w3+1)   = YUV_G[y10][u][v];
            *(p+w3+2)   = YUV_R[y10][u];
            *(p+w3+3)   = YUV_B[y11][v];
            *(p+w3+3+1) = YUV_G[y11][u][v];
            *(p+w3+3+2) = YUV_R[y11][u];

            pU++;
            pV++;
            pY = pY + 2;
        }
        pY = pY + w;
    }*/
}

int g_nThreadCount = 0;

bool CESMGpuDecode::copyDecodedFrameToTexture(unsigned int &nRepeats, int bUseInterop, int *pbIsProgressive, int nType, ThreadFrameData* pFrameData)
{
    CUVIDPARSERDISPINFO oDisplayInfo;
	BOOL bEndDecode = FALSE;

    if (g_pFrameQueue->dequeue(&oDisplayInfo))
    {
        CCtxAutoLock lck(g_CtxLock);
        // Push the current CUDA context (only if we are using CUDA decoding path)
        CUresult result = cuCtxPushCurrent(g_oContext);

        CUdeviceptr  pDecodedFrame[3] = { 0, 0, 0 };
        CUdeviceptr  pInteropFrame[3] = { 0, 0, 0 };

        *pbIsProgressive = oDisplayInfo.progressive_frame;
        g_bIsProgressive = oDisplayInfo.progressive_frame ? true : false;

        int num_fields = 1;
        if (g_bUseVsync) {            
            num_fields = std::min(2 + oDisplayInfo.repeat_first_field, 3);            
        }
        nRepeats = num_fields;

        CUVIDPROCPARAMS oVideoProcessingParameters;
        memset(&oVideoProcessingParameters, 0, sizeof(CUVIDPROCPARAMS));

        oVideoProcessingParameters.progressive_frame = oDisplayInfo.progressive_frame;
        oVideoProcessingParameters.top_field_first = oDisplayInfo.top_field_first;
        oVideoProcessingParameters.unpaired_field = (oDisplayInfo.repeat_first_field < 0);

        for (int active_field = 0; active_field < num_fields; active_field++)
        {
            unsigned int nDecodedPitch = 0;
            unsigned int nWidth = 0;
            unsigned int nHeight = 0;

            oVideoProcessingParameters.second_field = active_field;

            // map decoded video frame to CUDA surfae
            g_pVideoDecoder->mapFrame(oDisplayInfo.picture_index, &pDecodedFrame[active_field], &nDecodedPitch, &oVideoProcessingParameters);
            nWidth  = g_pVideoDecoder->targetWidth();
            nHeight = g_pVideoDecoder->targetHeight();
            // map DirectX texture to CUDA surface
            size_t nTexturePitch = 0;

            // If we are Encoding and this is the 1st Frame, we make sure we allocate system memory for readbacks
            if (g_bReadback && g_bFirstFrame && g_ReadbackSID)
            {
				//SendLog(5,_T("cuMemAllocHost"));
                CUresult result;
                checkCudaErrors(result = cuMemAllocHost((void **)&g_pFrameYUV[0], (nDecodedPitch * nHeight + nDecodedPitch*nHeight/2)));
                checkCudaErrors(result = cuMemAllocHost((void **)&g_pFrameYUV[1], (nDecodedPitch * nHeight + nDecodedPitch*nHeight/2)));
                checkCudaErrors(result = cuMemAllocHost((void **)&g_pFrameYUV[2], (nDecodedPitch * nHeight + nDecodedPitch*nHeight/2)));
                checkCudaErrors(result = cuMemAllocHost((void **)&g_pFrameYUV[3], (nDecodedPitch * nHeight + nDecodedPitch*nHeight/2)));
                checkCudaErrors(result = cuMemAllocHost((void **)&g_pFrameYUV[4], (nDecodedPitch * nHeight + nDecodedPitch*nHeight / 2)));
                checkCudaErrors(result = cuMemAllocHost((void **)&g_pFrameYUV[5], (nDecodedPitch * nHeight + nDecodedPitch*nHeight / 2)));

                g_bFirstFrame = false;

                if (result != CUDA_SUCCESS)
                {
                    printf("cuMemAllocHost returned %d\n", (int)result);
                    checkCudaErrors(result);
                }
            }

            // If streams are enabled, we can perform the readback to the host while the kernel is executing
            if (g_bReadback && g_ReadbackSID)
            {
                CUresult result = cuMemcpyDtoHAsync(g_pFrameYUV[active_field], pDecodedFrame[active_field], (nDecodedPitch * nHeight * 3 / 2), g_ReadbackSID);

                if (result != CUDA_SUCCESS)
                {
                    printf("cuMemAllocHost returned %d\n", (int)result);
                    checkCudaErrors(result);
                }
            }

#if ENABLE_DEBUG_OUT
            printf("%s = %02d, PicIndex = %02d, OutputPTS = %08d\n",
                   (oDisplayInfo.progressive_frame ? "Frame" : "Field"),
                   g_DecodeFrameCount, oDisplayInfo.picture_index, oDisplayInfo.timestamp);
#endif

            if (g_pImageDX)
            {
                // map the texture surface
                g_pImageDX->map(&g_backBufferArray, active_field);
				cudaPostProcessFrame(&pDecodedFrame[active_field], nDecodedPitch, g_backBufferArray, g_pCudaModule->getModule(), g_kernelNV12toARGB, g_KernelSID);


                // unmap the texture surface
                g_pImageDX->unmap(active_field);
            }
            else
            {
                pInteropFrame[active_field] = g_pInteropFrame[active_field];
                nTexturePitch = g_pVideoDecoder->targetWidth() * 2;

				int nStart = GetTickCount();
				cudaPostProcessFrame(&pDecodedFrame[active_field], nDecodedPitch, &pInteropFrame[active_field], 
									 nTexturePitch, g_pCudaModule->getModule(), g_kernelNV12toARGB, g_KernelSID);
				int nEnd = GetTickCount();
				CString strLog;
				strLog.Format(_T("##cudaPostProcessFrame %d\n"), nEnd-nStart);
				//TRACE(strLog);

            }

            // unmap video frame
            // unmapFrame() synchronizes with the VideoDecode API (ensures the frame has finished decoding)
            g_pVideoDecoder->unmapFrame(pDecodedFrame[active_field]); 
			
 			
            if (g_bWriteFile)
            {
                cuStreamSynchronize(g_ReadbackSID);
                SaveFrameAsYUV(g_pFrameYUV[active_field + 3],
                    g_pFrameYUV[active_field],
                    nWidth, nHeight, nDecodedPitch);
            }
	

			///////////////////////이미지 저장 test code 
#if 0
			if(pFrameData && g_DecodeFrameCount == pFrameData->pFrameInfo->nFrameIndex)
			{
				MakeFrameInfo* pFrame = pFrameData->pFrameInfo;
				if(pFrame->pYUVImg == NULL)
				{
					int nSize = nWidth*nHeight*1.5;
					pFrame->pYUVImg = new BYTE[nSize];
				}
				
				memcpy(pFrame->pYUVImg, g_pFrameYUV[active_field+3], nWidth*nHeight*1.5);
				pFrame->nWidth = nWidth;
				pFrame->nHeight = nHeight;
				
				//Image Process
				if(pFrame->Image == NULL)
				{
					pFrame->Image = (BYTE*)cvCreateImage(cvSize(nWidth,nHeight), IPL_DEPTH_8U, 3);
				}
				pFrame->nImageSize = nWidth*nHeight*3;
	
				HANDLE hSyncTime = NULL;
				/*CString strLog;
				strLog.Format(_T("ImageAdjust Thread Start %d"), g_nThreadCount);
				g_nThreadCount++;
				SendLog(1, strLog);*/
				hSyncTime = (HANDLE) _beginthreadex(NULL, 0, imageAdjust, (void *)pFrame, 0, NULL);
				CloseHandle(hSyncTime);

				/*delete pFrame->pYUVImg;
				IplImage* pdes = (IplImage*)pFrame->Image;
				cvReleaseImage(&pdes);
				pFrame->bComplete = TRUE;*/
				
				//imageAdjust((LPVOID)pFrame);
				g_nIndex++;
			}
#else		
			//KT Logic Test
			//Mat img(nHeight+nHeight/2,nWidth,CV_8UC1,g_pFrameYUV[active_field+3]);
			if(pFrameData && g_DecodeFrameCount == pFrameData->pFrameInfo->nFrameIndex)
			{
				Mat frame;

				MakeFrameInfo* pFrame = pFrameData->pFrameInfo;
				pFrame->nWidth = nWidth;
				pFrame->nHeight = nHeight;
				pFrame->nImageSize = nWidth * nHeight * 1.5;

				pFrame->pYUVImg = new BYTE[pFrame->nImageSize];
				memcpy(pFrame->pYUVImg,g_pFrameYUV[active_field+3],pFrame->nImageSize);

				g_nIndex++;
			}
#endif

			if(nType == DECODING_TYPE_MOVIE)
			{
				if(g_DecodeFrameCount == g_nIndex)
				{
					g_pFrameQueue->endDecode();
					bEndDecode = TRUE;
					break;
				}
			}
			g_DecodeFrameCount++;
        }
        // Detach from the Current thread
        checkCudaErrors(cuCtxPopCurrent(NULL));
        // release the frame, so it can be re-used in decoder
        g_pFrameQueue->releaseFrame(&oDisplayInfo);
    }
    else
    {
        // Frame Queue has no frames, we don't compute FPS until we start
        return false;
    }

    // check if decoding has come to an end.
    // if yes, signal the app to shut down.
	

    if ((!g_pVideoSource->isStarted() && g_pFrameQueue->isEndOfDecode() && g_pFrameQueue->isEmpty()) || bEndDecode)
    {
        // Let's free the Frame Data
        if (g_ReadbackSID && g_pFrameYUV)
        {
			SendLog(5,_T("DELETE IN END OF FUNC"));
            cuMemFreeHost((void *)g_pFrameYUV[0]);
            cuMemFreeHost((void *)g_pFrameYUV[1]);
            cuMemFreeHost((void *)g_pFrameYUV[2]);
            cuMemFreeHost((void *)g_pFrameYUV[3]);
            cuMemFreeHost((void *)g_pFrameYUV[4]);
            cuMemFreeHost((void *)g_pFrameYUV[5]);
            g_pFrameYUV[0] = NULL;
            g_pFrameYUV[1] = NULL;
            g_pFrameYUV[2] = NULL;
            g_pFrameYUV[3] = NULL;
            g_pFrameYUV[4] = NULL;
            g_pFrameYUV[5] = NULL;
        }

        // Let's just stop, and allow the user to quit, so they can at least see the results
        g_pVideoSource->stop();

        // If we want to loop reload the video file and restart
        if (g_bLoop && !g_bAutoQuit)
        {
            reinitCudaResources();
            g_FrameCount = 0;
            g_DecodeFrameCount = 0;
            g_pVideoSource->start();
        }

		g_bAutoQuit = true;
        if (g_bAutoQuit)
        {
            g_bDone = true;
        }
    }

    return true;
}


// Run the Cuda part of the computation (if g_pFrameQueue is empty, then return false)
bool CESMGpuDecode::copyDecodedFrameToTexture(unsigned int &nRepeats, int bUseInterop, int *pbIsProgressive, vector<ESMFrameArray*> pImgArr, int nType, ThreadFrameData* pFrameData)
{
    /*CUVIDPARSERDISPINFO oDisplayInfo;
	BOOL bEndDecode = FALSE;

    if (g_pFrameQueue->dequeue(&oDisplayInfo))
    {
        CCtxAutoLock lck(g_CtxLock);
        // Push the current CUDA context (only if we are using CUDA decoding path)
        CUresult result = cuCtxPushCurrent(g_oContext);

        CUdeviceptr  pDecodedFrame[3] = { 0, 0, 0 };
        CUdeviceptr  pInteropFrame[3] = { 0, 0, 0 };

        *pbIsProgressive = oDisplayInfo.progressive_frame;
        g_bIsProgressive = oDisplayInfo.progressive_frame ? true : false;

        int num_fields = 1;
        if (g_bUseVsync) {            
            num_fields = std::min(2 + oDisplayInfo.repeat_first_field, 3);            
        }
        nRepeats = num_fields;

        CUVIDPROCPARAMS oVideoProcessingParameters;
        memset(&oVideoProcessingParameters, 0, sizeof(CUVIDPROCPARAMS));

        oVideoProcessingParameters.progressive_frame = oDisplayInfo.progressive_frame;
        oVideoProcessingParameters.top_field_first = oDisplayInfo.top_field_first;
        oVideoProcessingParameters.unpaired_field = (oDisplayInfo.repeat_first_field < 0);

        for (int active_field = 0; active_field < num_fields; active_field++)
        {
            unsigned int nDecodedPitch = 0;
            unsigned int nWidth = 0;
            unsigned int nHeight = 0;

            oVideoProcessingParameters.second_field = active_field;

            // map decoded video frame to CUDA surfae
            g_pVideoDecoder->mapFrame(oDisplayInfo.picture_index, &pDecodedFrame[active_field], &nDecodedPitch, &oVideoProcessingParameters);
            nWidth  = g_pVideoDecoder->targetWidth();
            nHeight = g_pVideoDecoder->targetHeight();
            // map DirectX texture to CUDA surface
            size_t nTexturePitch = 0;

            // If we are Encoding and this is the 1st Frame, we make sure we allocate system memory for readbacks
            if (g_bReadback && g_bFirstFrame && g_ReadbackSID)
            {
                CUresult result;
                checkCudaErrors(result = cuMemAllocHost((void **)&g_pFrameYUV[0], (nDecodedPitch * nHeight + nDecodedPitch*nHeight/2)));
                checkCudaErrors(result = cuMemAllocHost((void **)&g_pFrameYUV[1], (nDecodedPitch * nHeight + nDecodedPitch*nHeight/2)));
                checkCudaErrors(result = cuMemAllocHost((void **)&g_pFrameYUV[2], (nDecodedPitch * nHeight + nDecodedPitch*nHeight/2)));
                checkCudaErrors(result = cuMemAllocHost((void **)&g_pFrameYUV[3], (nDecodedPitch * nHeight + nDecodedPitch*nHeight/2)));
                checkCudaErrors(result = cuMemAllocHost((void **)&g_pFrameYUV[4], (nDecodedPitch * nHeight + nDecodedPitch*nHeight / 2)));
                checkCudaErrors(result = cuMemAllocHost((void **)&g_pFrameYUV[5], (nDecodedPitch * nHeight + nDecodedPitch*nHeight / 2)));

                g_bFirstFrame = false;

                if (result != CUDA_SUCCESS)
                {
                    printf("cuMemAllocHost returned %d\n", (int)result);
                    checkCudaErrors(result);
                }
            }

            // If streams are enabled, we can perform the readback to the host while the kernel is executing
            if (g_bReadback && g_ReadbackSID)
            {
				CUresult result = cuMemcpyDtoHAsync(g_pFrameYUV[active_field], pDecodedFrame[active_field], (nDecodedPitch * nHeight * 3 / 2), g_ReadbackSID);

				if (result != CUDA_SUCCESS)
				{
					printf("cuMemAllocHost returned %d\n", (int)result);
					checkCudaErrors(result);
				}
            }

#if ENABLE_DEBUG_OUT
            printf("%s = %02d, PicIndex = %02d, OutputPTS = %08d\n",
                   (oDisplayInfo.progressive_frame ? "Frame" : "Field"),
                   g_DecodeFrameCount, oDisplayInfo.picture_index, oDisplayInfo.timestamp);
#endif

            if (g_pImageDX)
            {
                // map the texture surface
                g_pImageDX->map(&g_backBufferArray, active_field);
				cudaPostProcessFrame(&pDecodedFrame[active_field], nDecodedPitch, g_backBufferArray, g_pCudaModule->getModule(), g_kernelNV12toARGB, g_KernelSID);


                // unmap the texture surface
                g_pImageDX->unmap(active_field);
			}
            else
            {
                pInteropFrame[active_field] = g_pInteropFrame[active_field];
                nTexturePitch = g_pVideoDecoder->targetWidth() * 2;

				int nStart = GetTickCount();
				cudaPostProcessFrame(&pDecodedFrame[active_field], nDecodedPitch, &pInteropFrame[active_field], 
									 nTexturePitch, g_pCudaModule->getModule(), g_kernelNV12toARGB, g_KernelSID);
				int nEnd = GetTickCount();
				CString strLog;
				strLog.Format(_T("##cudaPostProcessFrame %d\n"), nEnd-nStart);
				//TRACE(strLog);


				//printf("Aa");
				//if(memresult == CUDA_SUCCESS)
				//{
				//	Mat RGBA(nHeight,nWidth,CV_8UC3,a);
				//	namedWindow("RGBA",CV_WINDOW_FREERATIO);
				//	imshow("RGBA",RGBA);
				//	waitKey(0);
				//	destroyAllWindows();
				//}
			}

            // unmap video frame
            // unmapFrame() synchronizes with the VideoDecode API (ensures the frame has finished decoding)
            g_pVideoDecoder->unmapFrame(pDecodedFrame[active_field]); 
			*/
            
			

/*          if (g_bPSNR)
            {
                int w2 = nWidth;
                int h2 = nHeight;
                int luma_size = w2*h2;
                int chroma_size = w2*(h2 >> 1);

                unsigned char *iyuv = new unsigned char[w2*(h2 + (h2 >> 1)) + 16];

//                NV12toIYUV(pRawNV12, pRawNV12 + state.dci.ulTargetHeight*pitch, iyuv, w2, h2, pitch);

                unsigned char *iyuv_ref = new unsigned char[w2*(h2 + (h2 >> 1)) + 16];
                long long frm_mse_y = 0, frm_mse_uv = 0;

                if (fread(iyuv_ref, w2, h2 + (h2 >> 1), fpRefYUV) > 0)
                {
                    double psnr;

                    frm_mse_y = SumSquareError(iyuv, iyuv_ref, luma_size);
                    frm_mse_uv = SumSquareError(iyuv + luma_size, iyuv_ref + luma_size, chroma_size);
                    mse_luma += frm_mse_y;
                    mse_chroma += frm_mse_uv;
                    mse_luma_count += luma_size;
                    mse_chroma_count += chroma_size;
                    psnr = PSNR(frm_mse_y + frm_mse_uv, luma_size + chroma_size);
                    if ((psnr_threshold) && (psnr < (double)psnr_threshold))
                    {
                        printf("  [%d(%d)] PSNR=%6.3fdB (Y=%6.3fdB, UV=%6.3fdB) [size=%d]\n", pic_cnt,
                            state.frmStats[PicIdx].pic_num_in_decode_order,
                            psnr, PSNR(frm_mse_y, luma_size), PSNR(frm_mse_uv, chroma_size),
                            state.frmStats[PicIdx].bytes_in_picture);
                        fflush(stdout);
                    }
                }
                delete iyuv_ref;
            }
            delete iyuv;
*/
		/*	CString strTime;
			int nStart, nEnd1, nEnd2, nEnd3, nEnd4, nEnd5;

            if (g_bWriteFile)
            {
                cuStreamSynchronize(g_ReadbackSID);
                SaveFrameAsYUV(g_pFrameYUV[active_field + 3],
                    g_pFrameYUV[active_field],
                    nWidth, nHeight, nDecodedPitch);
            }
	

			///////////////////////이미지 저장 test code 
			ESMFrameArray* pFrame = NULL;
			switch(nType)
			{
			case DECODING_TYPE_MOVIE:
				{
					if(g_DecodeFrameCount == g_nIndex)
					{
						pFrame = pImgArr.at(0);
						pFrame->pYUV = NULL;
						pFrame->pImg = NULL;
					}
				}
				break;
			case DECODING_TYPE_FRAME:
				{
					pFrame = pImgArr.at(g_DecodeFrameCount);
				}
				break;
			}

			if(pFrame)
			{
				pFrame->bLoad = TRUE;
				if(pFrame->pYUV)
				{
					delete pFrame->pYUV;
					pFrame->pYUV = NULL;

					int nSize = nWidth*nHeight*1.5;
					pFrame->pYUV = new BYTE[nSize];
				}
				else
				{
					int nSize = nWidth*nHeight*1.5;
					pFrame->pYUV = new BYTE[nSize];
				}

				CString strTemp;
				strTemp.Format(_T("Addr = %x"), pFrame->pYUV);
				//AfxMessageBox(strTemp);

				memcpy(pFrame->pYUV, g_pFrameYUV[active_field+3], nWidth*nHeight*1.5);
				

				pFrame->nWidth = nWidth;
				pFrame->nHeight = nHeight;
				*/

				//YUV to RGB
				/*int nStart = GetTickCount();
				yuv420_to_rgb(g_pFrameYUV[active_field+3],pFrame, nWidth, nHeight);
				int nEnd = GetTickCount();
				CString strLog;
				strLog.Format(_T("YUV Gap = %d"), nEnd - nStart);
				SendLog(1, strLog);*/

				//Image Process
				/*if(pFrameData->pFrameInfo->pYUVImg == NULL)
				{
					int nYUVSize = nWidth*nHeight*1.5;
					pFrameData->pFrameInfo->pYUVImg = new BYTE[nYUVSize];
				}

				memcpy(pFrameData->pFrameInfo->pYUVImg, g_pFrameYUV[active_field+3], nWidth*nHeight*1.5);
				if(pFrame->pImg == NULL)
				{
					pFrame->pImg = (BYTE*)cvCreateImage(cvSize(nWidth,nHeight), IPL_DEPTH_8U, 3);
				}
				pFrameData->pFrameInfo->pFrame = (void*)pFrame;
				pFrameData->pFrameInfo->nWidth = nWidth;
				pFrameData->pFrameInfo->nHeight = nHeight;



				pFrameData->pFrameInfo->Image = pFrame->pImg;
				pFrameData->pFrameInfo->nImageSize = nWidth*nHeight*3;
				

				
				HANDLE hSyncTime = NULL;
				hSyncTime = (HANDLE) _beginthreadex(NULL, 0, imageAdjust, (void *)pFrameData, 0, NULL);
				CloseHandle(hSyncTime);
				
				//imageAdjust(pFrameData);
			}

			if(nType == DECODING_TYPE_MOVIE)
			{
				if(g_DecodeFrameCount == g_nIndex)
				{
					g_pFrameQueue->endDecode();
					bEndDecode = TRUE;
					break;
				}
			}
			g_DecodeFrameCount++;
			
        }
        // Detach from the Current thread
        checkCudaErrors(cuCtxPopCurrent(NULL));
        // release the frame, so it can be re-used in decoder
        g_pFrameQueue->releaseFrame(&oDisplayInfo);
    }
    else
    {
        // Frame Queue has no frames, we don't compute FPS until we start
        return false;
    }

    // check if decoding has come to an end.
    // if yes, signal the app to shut down.
	

    if ((!g_pVideoSource->isStarted() && g_pFrameQueue->isEndOfDecode() && g_pFrameQueue->isEmpty()) || bEndDecode)
    {
        // Let's free the Frame Data
        if (g_ReadbackSID && g_pFrameYUV)
        {
			cuMemFreeHost((void *)g_pFrameYUV[0]);
			cuMemFreeHost((void *)g_pFrameYUV[1]);
			cuMemFreeHost((void *)g_pFrameYUV[2]);
			cuMemFreeHost((void *)g_pFrameYUV[3]);
			cuMemFreeHost((void *)g_pFrameYUV[4]);
			cuMemFreeHost((void *)g_pFrameYUV[5]);
			g_pFrameYUV[0] = NULL;
            g_pFrameYUV[1] = NULL;
            g_pFrameYUV[2] = NULL;
            g_pFrameYUV[3] = NULL;
            g_pFrameYUV[4] = NULL;
            g_pFrameYUV[5] = NULL;
        }

        // Let's just stop, and allow the user to quit, so they can at least see the results
        g_pVideoSource->stop();

        // If we want to loop reload the video file and restart
        if (g_bLoop && !g_bAutoQuit)
        {
            reinitCudaResources();
            g_FrameCount = 0;
            g_DecodeFrameCount = 0;
            g_pVideoSource->start();
        }

		g_bAutoQuit = true;
        if (g_bAutoQuit)
        {
            g_bDone = true;
        }
    }
	*/
    return true;
}




void CESMGpuDecode::YUV2RGB(uint32 *yuvi, float *red, float *green, float *blue)
{
	float luma, chromaCb, chromaCr;

	// Prepare for hue adjustment
	luma     = (float)yuvi[0];
	chromaCb = (float)((int32)yuvi[1] - 512.0f);
	chromaCr = (float)((int32)yuvi[2] - 512.0f);

	// Convert YUV To RGB with hue adjustment
	*red  = MUL(luma,     constHueColorSpaceMat_[0]) +
		MUL(chromaCb, constHueColorSpaceMat_[1]) +
		MUL(chromaCr, constHueColorSpaceMat_[2]);
	*green= MUL(luma,     constHueColorSpaceMat_[3]) +
		MUL(chromaCb, constHueColorSpaceMat_[4]) +
		MUL(chromaCr, constHueColorSpaceMat_[5]);
	*blue = MUL(luma,     constHueColorSpaceMat_[6]) +
		MUL(chromaCb, constHueColorSpaceMat_[7]) +
		MUL(chromaCr, constHueColorSpaceMat_[8]);
}


uint32 CESMGpuDecode::RGBAPACK_8bit(float red, float green, float blue, uint32 alpha)
{
	uint32 ARGBpixel = 0;

	// Clamp final 10 bit results
	red   = min(max(red,   0.0f), 255.0f);
	green = min(max(green, 0.0f), 255.0f);
	blue  = min(max(blue,  0.0f), 255.0f);

	// Convert to 8 bit unsigned integers per color component
	ARGBpixel = (((uint32)blue) |
		(((uint32)green) << 8)  |
		(((uint32)red) << 16) | (uint32)alpha);

	return  ARGBpixel;
}

uint32 CESMGpuDecode::RGBAPACK_10bit(float red, float green, float blue, uint32 alpha)
{
	uint32 ARGBpixel = 0;

	// Clamp final 10 bit results
	red   = min(max(red,   0.0f), 1023.f);
	green = min(max(green, 0.0f), 1023.f);
	blue  = min(max(blue,  0.0f), 1023.f);

	// Convert to 8 bit unsigned integers per color component
	ARGBpixel = (((uint32)blue  >> 2) |
		(((uint32)green >> 2) << 8)  |
		(((uint32)red   >> 2) << 16) | (uint32)alpha);

	return  ARGBpixel;
}


// CUDA kernel for outputing the final ARGB output from NV12;
void CESMGpuDecode::Passthru_drvapi(uint32 *srcImage,   size_t nSourcePitch,	uint32 *dstImage,   size_t nDestPitch,	uint32 width,       uint32 height)
{
	int32 x, y;
	uint32 yuv101010Pel[2];
	uint32 processingPitch = ((width) + 63) & ~63;
	uint32 dstImagePitch   = nDestPitch >> 2;
	uint8 *srcImageU8     = (uint8 *)srcImage;

	processingPitch = nSourcePitch;

	// Pad borders with duplicate pixels, and we multiply by 2 because we process 2 pixels per thread
	//x = blockIdx.x * (blockDim.x << 1) + (threadIdx.x << 1);
	//y = blockIdx.y *  blockDim.y       +  threadIdx.y;
	x = width;
	y = height;

	/*if (x >= width)
		return; //x = width - 1;

	if (y >= height)
		return; // y = height - 1;*/

	// Read 2 Luma components at a time, so we don't waste processing since CbCr are decimated this way.
	// if we move to texture we could read 4 luminance values
	yuv101010Pel[0] = (srcImageU8[y * processingPitch + x    ]);
	yuv101010Pel[1] = (srcImageU8[y * processingPitch + x + 1]);

	// this steps performs the color conversion
	float luma[2];

	luma[0]   = (yuv101010Pel[0]        & 0x00FF);
	luma[1]   = (yuv101010Pel[1]        & 0x00FF);

	// Clamp the results to RGBA
	dstImage[y * dstImagePitch + x     ] = RGBAPACK_8bit(luma[0], luma[0], luma[0], constAlpha);
	dstImage[y * dstImagePitch + x + 1 ] = RGBAPACK_8bit(luma[1], luma[1], luma[1], constAlpha);
}


// CUDA kernel for outputing the final ARGB output from NV12;
void CESMGpuDecode::ConvertYUV2RGB(uint32 *srcImage,     size_t nSourcePitch, uint32 *dstImage,     size_t nDestPitch,	uint32 width,         uint32 height)
{
	int32 x, y;
	uint32 yuv101010Pel[2];
	uint32 processingPitch = ((width) + 63) & ~63;
	uint32 dstImagePitch   = nDestPitch >> 2;
	uint8 *srcImageU8     = (uint8 *)srcImage;

	processingPitch = nSourcePitch;

	// Pad borders with duplicate pixels, and we multiply by 2 because we process 2 pixels per thread
	//x = blockIdx.x * (blockDim.x << 1) + (threadIdx.x << 1);
	//y = blockIdx.y *  blockDim.y       +  threadIdx.y;
	x = width;
	y = height;

	/*if (x >= width)
		return; //x = width - 1;

	if (y >= height)
		return; // y = height - 1;*/

	// Read 2 Luma components at a time, so we don't waste processing since CbCr are decimated this way.
	// if we move to texture we could read 4 luminance values
	yuv101010Pel[0] = (srcImageU8[y * processingPitch + x    ]) << 2;
	yuv101010Pel[1] = (srcImageU8[y * processingPitch + x + 1]) << 2;

	uint32 chromaOffset    = processingPitch * height;
	int32 y_chroma = y >> 1;

	if (y & 1)  // odd scanline ?
	{
		uint32 chromaCb;
		uint32 chromaCr;

		chromaCb = srcImageU8[chromaOffset + y_chroma * processingPitch + x    ];
		chromaCr = srcImageU8[chromaOffset + y_chroma * processingPitch + x + 1];

		if (y_chroma < ((height >> 1) - 1)) // interpolate chroma vertically
		{
			chromaCb = (chromaCb + srcImageU8[chromaOffset + (y_chroma + 1) * processingPitch + x    ] + 1) >> 1;
			chromaCr = (chromaCr + srcImageU8[chromaOffset + (y_chroma + 1) * processingPitch + x + 1] + 1) >> 1;
		}

		yuv101010Pel[0] |= (chromaCb << (COLOR_COMPONENT_BIT_SIZE       + 2));
		yuv101010Pel[0] |= (chromaCr << ((COLOR_COMPONENT_BIT_SIZE << 1) + 2));

		yuv101010Pel[1] |= (chromaCb << (COLOR_COMPONENT_BIT_SIZE       + 2));
		yuv101010Pel[1] |= (chromaCr << ((COLOR_COMPONENT_BIT_SIZE << 1) + 2));
	}
	else
	{
		yuv101010Pel[0] |= ((uint32)srcImageU8[chromaOffset + y_chroma * processingPitch + x    ] << (COLOR_COMPONENT_BIT_SIZE       + 2));
		yuv101010Pel[0] |= ((uint32)srcImageU8[chromaOffset + y_chroma * processingPitch + x + 1] << ((COLOR_COMPONENT_BIT_SIZE << 1) + 2));

		yuv101010Pel[1] |= ((uint32)srcImageU8[chromaOffset + y_chroma * processingPitch + x    ] << (COLOR_COMPONENT_BIT_SIZE       + 2));
		yuv101010Pel[1] |= ((uint32)srcImageU8[chromaOffset + y_chroma * processingPitch + x + 1] << ((COLOR_COMPONENT_BIT_SIZE << 1) + 2));
	}

	// this steps performs the color conversion
	uint32 yuvi[6];
	float red[2], green[2], blue[2];

	yuvi[0] = (yuv101010Pel[0] &   COLOR_COMPONENT_MASK);
	yuvi[1] = ((yuv101010Pel[0] >>  COLOR_COMPONENT_BIT_SIZE)       & COLOR_COMPONENT_MASK);
	yuvi[2] = ((yuv101010Pel[0] >> (COLOR_COMPONENT_BIT_SIZE << 1)) & COLOR_COMPONENT_MASK);

	yuvi[3] = (yuv101010Pel[1] &   COLOR_COMPONENT_MASK);
	yuvi[4] = ((yuv101010Pel[1] >>  COLOR_COMPONENT_BIT_SIZE)       & COLOR_COMPONENT_MASK);
	yuvi[5] = ((yuv101010Pel[1] >> (COLOR_COMPONENT_BIT_SIZE << 1)) & COLOR_COMPONENT_MASK);

	// YUV to RGB Transformation conversion
	YUV2RGB(&yuvi[0], &red[0], &green[0], &blue[0]);
	YUV2RGB(&yuvi[3], &red[1], &green[1], &blue[1]);

	// Clamp the results to RGBA
	dstImage[y * dstImagePitch + x     ] = RGBAPACK_10bit(red[0], green[0], blue[0], constAlpha);
	dstImage[y * dstImagePitch + x + 1 ] = RGBAPACK_10bit(red[1], green[1], blue[1], constAlpha);
}

HRESULT CESMGpuDecode::reinitCudaResources()
{
	// Free resources
	cleanup(false);

	// Reinit VideoSource and Frame Queue
	g_bIsProgressive = loadVideoSource(sFileName.c_str(),
		g_nVideoWidth, g_nVideoHeight,
		g_nWindowWidth, g_nWindowHeight);

	/////////////////Change///////////////////////////
	initCudaVideo();
	initD3D11Surface(g_pVideoDecoder->targetWidth(),
		g_pVideoDecoder->targetHeight());
	/////////////////////////////////////////

	return S_OK;
}

// Release all previously initd objects
HRESULT CESMGpuDecode::cleanup(bool bDestroyContext)
{
	/*if (!g_bQAReadback)
	{
		// Unregister windows class
		UnregisterClass(g_wc->lpszClassName, g_wc->hInstance);
	}*/


	if (g_bAutoQuit)
	{
		PostQuitMessage(0);
	}

	if (g_hWnd)
	{
		DestroyWindow(g_hWnd);
	}

	if (g_bWaived)
	{
		exit(EXIT_WAIVED);
	}
	/*else
	{
		exit(g_bException ? EXIT_FAILURE : EXIT_SUCCESS);
	}*/
	if(g_pFrameYUV)
	{
		//SendLog(5,_T("DELETE IN CLEANUP"));
		for(int i = 0 ; i < 6 ; i++)
		{
			//cuMemFreeHost((void *)g_pFrameYUV[i]);
			delete g_pFrameYUV[i];
			g_pFrameYUV[i] = NULL;
		}
	}
	if (fpWriteYUV != NULL)
	{
		fflush(fpWriteYUV);
		fclose(fpWriteYUV);
		fpWriteYUV = NULL;
	}

	if (fpRefYUV != NULL)
	{
		fflush(fpRefYUV);
		fclose(fpRefYUV);
		fpRefYUV = NULL;
	}

	if (bDestroyContext)
	{
		// Attach the CUDA Context (so we may properly free memroy)
		checkCudaErrors(cuCtxPushCurrent(g_oContext));

		if (g_pInteropFrame[0])
		{
			checkCudaErrors(cuMemFree(g_pInteropFrame[0]));
			g_pInteropFrame[0] = NULL;
		}

		if (g_pInteropFrame[1])
		{
			checkCudaErrors(cuMemFree(g_pInteropFrame[1]));
			g_pInteropFrame[1] = NULL;
		}

		if (g_pInteropFrame[2])
		{
			checkCudaErrors(cuMemFree(g_pInteropFrame[2]));
			g_pInteropFrame[2] = NULL;
		}

		if (g_pRgba) {
			checkCudaErrors(cuMemFree(g_pRgba));
		}

		// Detach from the Current thread
		checkCudaErrors(cuCtxPopCurrent(NULL));

		if(cuCtxPopCurrent)
			cuCtxPopCurrent = NULL;
	}

	if (g_pImageDX)
	{
		delete g_pImageDX;
		g_pImageDX = NULL;
	}

	freeCudaResources(bDestroyContext);

	// destroy the D3D device
	if (g_pD3DDevice)
	{
		g_pD3DDevice->Release();
		g_pD3DDevice = NULL;
	}

	if (g_pContext) {
		g_pContext->Release();
		g_pContext = NULL;
	}

	if (g_pSwapChain) {
		g_pSwapChain->Release();
		g_pSwapChain = NULL;
	}

	return S_OK;
}

void CESMGpuDecode::freeCudaResources(bool bDestroyContext)
{
	//g_pImageDX->clear();

	if(g_pD3DDevice)
	{
		delete g_pD3DDevice;
		g_pD3DDevice = NULL;
	}
	if(g_pCudaModule)
	{
		delete g_pCudaModule;
		g_pCudaModule = NULL;
	}
	if (g_pVideoDecoder)
	{
		delete g_pVideoDecoder;
		g_pVideoDecoder = NULL;
	}//VideoDecoder

	if (g_pVideoSource)
	{
		delete g_pVideoSource;
		g_pVideoSource = NULL;
	}//VideoSource

	if (g_pFrameQueue)
	{
		delete g_pFrameQueue;
		g_pFrameQueue = NULL;
	}//FrameQueue

	if (g_ReadbackSID)
	{
		cuStreamDestroy(g_ReadbackSID);
		g_ReadbackSID = NULL;
		//CUstream
	}
	if (g_pVideoParser)
	{
		//cuvidDestroyVideoParser(g_pVideoParser);
		delete g_pVideoParser;
		g_pVideoParser = NULL;
	}//VideoParser

	if (g_KernelSID)
	{
		cuStreamDestroy(g_KernelSID);
		g_KernelSID = NULL;
	}

	if (g_CtxLock)
	{
		checkCudaErrors(cuvidCtxLockDestroy(g_CtxLock));
		g_CtxLock = NULL;
	}

	if (g_oContext && bDestroyContext)
	{
		checkCudaErrors(cuCtxDestroy(g_oContext));
		//delete g_oContext;
		g_oContext = NULL;
	}
}

void CESMGpuDecode::SaveFrameAsYUV(unsigned char *pdst,const unsigned char *psrc,int width, int height, int pitch)
{
	int x, y, width_2, height_2;
	int xy_offset = width*height;
	int uvoffs = (width / 2)*(height / 2);
	const unsigned char *py = psrc;
	const unsigned char *puv = psrc + height*pitch;

	if ( ((long)g_DecodeFrameCount >= g_nFrameStart) && 
		((long)g_DecodeFrameCount <= g_nFrameEnd)
		)
	{
		//      printf(" Saving YUV Frame %d (start,end)=(%d,%d)\n", g_DecodeFrameCount, g_nFrameStart, g_nFrameEnd);
		printf("%d+", g_DecodeFrameCount);
	}
	else if ((g_nFrameStart == -1) && (g_nFrameEnd == -1))
	{
		printf("+");
	} 
	else // we do nothing and exit
	{
		return;
	}

	// luma
	for (y = 0; y<height; y++)
	{
		memcpy(&pdst[y*width], py, width);
		py += pitch;
	}

	// De-interleave chroma
	width_2  = width >> 1;
	height_2 = height >> 1;
	for (y = 0; y<height_2; y++)
	{
		for (x = 0; x<width_2; x++)
		{
			pdst[ xy_offset          + y*(width_2) + x ] = puv[x * 2];
			pdst[ xy_offset + uvoffs + y*(width_2) + x ] = puv[x * 2 + 1];
		}
		puv += pitch;
	}

	//fwrite(pdst, 1, width*height+(width*height)/2, fpWriteYUV);
}

void CESMGpuDecode::renderVideoFrame(HWND hWnd, bool bUseInterop, int nType, vector<MakeFrameInfo>* pArrFrameInfo)
{
	static unsigned int nRepeatFrame = 0;
	int repeatFactor = g_iRepeatFactor;
	int bIsProgressive = 1, bFPSComputed = 0;
	bool bFramesDecoded = false;

	if (0 != g_pFrameQueue)
	{
		// if not running, we simply don't copy new frames from the decoder
		if (g_bRunning)
		{
			MakeFrameInfo* FrameData;
			ThreadFrameData *pFrameData = NULL;
			if(pArrFrameInfo->size() > g_nIndex)
			{
				FrameData = &pArrFrameInfo->at(g_nIndex);
				pFrameData = new ThreadFrameData;
				pFrameData->pFrameInfo = FrameData;
				pFrameData->pMovieThreadManager = (CESMMovieThreadManager*)FrameData->pParent;
				pFrameData->bReverse = FrameData->bReverse;
			}
			
			bFramesDecoded = copyDecodedFrameToTexture(nRepeatFrame, true, &bIsProgressive, nType, pFrameData);

			

			//메모리 해제
			if(pFrameData)
			{
				delete pFrameData;
				pFrameData = NULL;
			}
		}
	}
	else
	{
		return;
	}

	if (bFramesDecoded)
	{
		while (repeatFactor-- > 0)
		{
			// draw the scene using the copied textures
			if (g_bUseDisplay && bUseInterop)
			{
				for (int i = 0; i < nRepeatFrame; i++) {
					//drawScene(i);
					if (!repeatFactor)
					{
						computeFPS(hWnd, bUseInterop);
					}
				}

				bFPSComputed = 1;
			}
		}

		// Pass the Windows handle to show Frame Rate on the window title
		if (!bFPSComputed)
		{
			computeFPS(hWnd, bUseInterop);
		}
	}

	if (bFramesDecoded && g_bFrameStep)
	{
		if (g_bRunning)
		{
			g_bRunning = false;
		}
	}
}


// Launches the CUDA kernels to fill in the texture data
void CESMGpuDecode::renderVideoFrame(HWND hWnd, bool bUseInterop, vector<ESMFrameArray*> pImgArr, int nType, vector<MakeFrameInfo>* pArrFrameInfo)
{
	static unsigned int nRepeatFrame = 0;
	int repeatFactor = g_iRepeatFactor;
	int bIsProgressive = 1, bFPSComputed = 0;
	bool bFramesDecoded = false;

	if (0 != g_pFrameQueue)
	{
		// if not running, we simply don't copy new frames from the decoder
		if (g_bRunning)
		{
			MakeFrameInfo* FrameData = &pArrFrameInfo->at(g_DecodeFrameCount);
			ThreadFrameData *pFrameData = new ThreadFrameData;
			pFrameData->pFrameInfo = FrameData;
			pFrameData->pMovieThreadManager = (CESMMovieThreadManager*)FrameData->pParent;
			pFrameData->bReverse = FrameData->bReverse;

			bFramesDecoded = copyDecodedFrameToTexture(nRepeatFrame, true, &bIsProgressive, pImgArr, nType, pFrameData);
		}
	}
	else
	{
		return;
	}

	if (bFramesDecoded)
	{
		while (repeatFactor-- > 0)
		{
			// draw the scene using the copied textures
			if (g_bUseDisplay && bUseInterop)
			{
				for (int i = 0; i < nRepeatFrame; i++) {
					//drawScene(i);
					if (!repeatFactor)
					{
						computeFPS(hWnd, bUseInterop);
					}
				}

				bFPSComputed = 1;
			}
		}

		// Pass the Windows handle to show Frame Rate on the window title
		if (!bFPSComputed)
		{
			computeFPS(hWnd, bUseInterop);
		}
	}

	if (bFramesDecoded && g_bFrameStep)
	{
		if (g_bRunning)
		{
			g_bRunning = false;
		}
	}
}

void CESMGpuDecode::computeFPS(HWND hWnd, bool bUseInterop)
{
	/*sdkStopTimer(&frame_timer);

	if (g_bRunning)
	{
		g_fpsCount++;

		if (!(g_pFrameQueue->isEndOfDecode() && g_pFrameQueue->isEmpty()))
		{
			g_FrameCount++;
		}
	}

	char sFPS[256];
	std::string sDecodeStatus;

	if (g_pFrameQueue->isEndOfDecode() && g_pFrameQueue->isEmpty())
	{
		sDecodeStatus = "STOP (End of File)\0";

		// we only want to record this once
		if (total_time == 0.0f)
		{
			total_time = sdkGetTimerValue(&global_timer);
		}

		sdkStopTimer(&global_timer);

		if (g_bAutoQuit)
		{
			g_bRunning = false;
			g_bDone    = true;
		}
	}
	else
	{
		if (!g_bRunning)
		{
			sDecodeStatus = "PAUSE\0";
			sprintf(sFPS, "%s [%s] - [%s %d] - Video Display %s / Vsync %s",
				sAppName[0], sDecodeStatus.c_str(),
				(g_bIsProgressive ? "Frame" : "Field"), g_DecodeFrameCount,
				g_bUseDisplay ? "ON" : "OFF",
				g_bUseVsync   ? "ON" : "OFF");

			if (bUseInterop && (!g_bQAReadback))
			{
				//SetWindowText(hWnd, (LPCWSTR)sFPS);
				UpdateWindow(hWnd);
			}
		}
		else
		{
			if (g_bFrameStep)
			{
				sDecodeStatus = "STEP\0";
			}
			else
			{
				sDecodeStatus = "PLAY\0";
			}
		}

		if (g_fpsCount == g_fpsLimit)
		{
			float ifps = 1.f / (sdkGetAverageTimerValue(&frame_timer) / 1000.f);

			sprintf(sFPS, "[%s] [%s] - [%3.1f fps, %s %d] - Video Display %s / Vsync %s",
				sAppName[0], sDecodeStatus.c_str(), ifps,
				(g_bIsProgressive ? "Frame" : "Field"), g_DecodeFrameCount,
				g_bUseDisplay ? "ON" : "OFF",
				g_bUseVsync   ? "ON" : "OFF");

			if (bUseInterop && (!g_bQAReadback))
			{
				//SetWindowText(hWnd, (LPCWSTR)sFPS);
				UpdateWindow(hWnd);
			}

			printf("[%s] - [%s: %04d, %04.1f fps, time: %04.2f (ms) ]\n",
				sSDKname, (g_bIsProgressive ? "Frame" : "Field"), g_FrameCount, ifps, 1000.f/ifps);

			sdkResetTimer(&frame_timer);
			g_fpsCount = 0;
		}
	}

	sdkStartTimer(&frame_timer);*/
}

// Draw the final result on the screen
HRESULT CESMGpuDecode::drawScene(int field_num)
{
	HRESULT hr = S_OK;

	// init the scene
	if (g_bUseDisplay)
	{
		// render image
		g_pImageDX->render(field_num);
	}

	hr = g_pSwapChain->Present(g_bUseVsync ? DXGI_SWAP_EFFECT_SEQUENTIAL : DXGI_SWAP_EFFECT_DISCARD, 0);

	return S_OK;
}


HRESULT CESMGpuDecode::initCudaResources(int bUseInterop, int bTCC,BOOL bIsLoad)
{
	HRESULT hr = S_OK;

	CUdevice cuda_device;

	int device_count;
	checkCudaErrors(cuDeviceGetCount(&device_count));

	cuda_device = gpuGetMaxGflopsDeviceIdDRV();
	checkCudaErrors(cuDeviceGet(&g_oDevice, cuda_device));
	

	// get compute capabilities and the devicename
	int major, minor;
	size_t totalGlobalMem;
	char deviceName[256];
	checkCudaErrors(cuDeviceComputeCapability(&major, &minor, g_oDevice));
	checkCudaErrors(cuDeviceGetName(deviceName, 256, g_oDevice));
	printf("> Using GPU Device %d: %s has SM %d.%d compute capability\n", cuda_device, deviceName, major, minor);

	checkCudaErrors(cuDeviceTotalMem(&totalGlobalMem, g_oDevice));
	printf("  Total amount of global memory:     %4.4f MB\n", (float)totalGlobalMem/(1024*1024));

	// Create CUDA Device w/ D3D11 interop (if WDDM), otherwise CUDA w/o interop (if TCC)
	// (use CU_CTX_BLOCKING_SYNC for better CPU synchronization)
	if (bUseInterop)
	{
		SendLog(5,_T("cuD3D11CtxCreate"));
		checkCudaErrors(cuD3D11CtxCreate(&g_oContext, &g_oDevice, CU_CTX_BLOCKING_SYNC, g_pD3DDevice));
	}
	else
	{
		//SendLog(5,_T("cuD3D11CtxCreate - ELSE"));
		checkCudaErrors(cuCtxCreate(&g_oContext, CU_CTX_BLOCKING_SYNC, g_oDevice));
	}

	try
	{
		// Initialize CUDA releated Driver API (32-bit or 64-bit), depending the platform running
		if (sizeof(void *) == 4)
		{
			SendLog(5,_T("NV12ToARGB_drvapi_Win32"));
			g_pCudaModule = new CUmoduleManager("NV12ToARGB_drvapi_Win32.ptx", exec_path, 2, 2, 2);
		}
		else
		{
			//SendLog(5,_T("NV12ToARGB_drvapi_x64"));		
			g_pCudaModule = new CUmoduleManager("NV12ToARGB_drvapi_x64.ptx", exec_path, 2, 2, 2);
		}
	}
	catch (char const *p_file)
	{
		// If the CUmoduleManager constructor fails to load the PTX file, it will throw an exception
		printf("\n>> CUmoduleManager::Exception!  %s not found!\n", p_file);
		printf(">> Please rebuild NV12ToARGB_drvapi.cu or re-install this sample.\n");
		return E_FAIL;
	}

	g_pCudaModule->GetCudaFunction("NV12ToARGB_drvapi", &g_kernelNV12toARGB);
	g_pCudaModule->GetCudaFunction("Passthru_drvapi",   &g_kernelPassThru);

	/////////////////Change///////////////////////////
	// Now we create the CUDA resources and the CUDA decoder context
	if(!initCudaVideo())
	{
		SendLog(0, _T("Init Cuda Video Error"));
	}

	if (bUseInterop)
	{
		SendLog(5,_T("initD3D11Surface"));
		initD3D11Surface(g_pVideoDecoder->targetWidth(),
			g_pVideoDecoder->targetHeight());
		checkCudaErrors(cuMemAlloc(&g_pRgba, g_pVideoDecoder->targetWidth() * g_pVideoDecoder->targetHeight() * 4));
	}
	else
	{
		//SendLog(5,_T("initD3D11Surface - ELSE"));
		checkCudaErrors(cuMemAlloc(&g_pInteropFrame[0], g_pVideoDecoder->targetWidth() * g_pVideoDecoder->targetHeight() * 2));
		checkCudaErrors(cuMemAlloc(&g_pInteropFrame[1], g_pVideoDecoder->targetWidth() * g_pVideoDecoder->targetHeight() * 2));
	}

	CUcontext cuCurrent = NULL;
	CUresult result = cuCtxPopCurrent(&cuCurrent);

	if (result != CUDA_SUCCESS)
	{
		SendLog(0, _T("CUDA_SUCCESS Error"));
		printf("cuCtxPopCurrent: %d\n", result);
		assert(0);
	}

	/////////////////////////////////////////
	return ((g_pCudaModule && g_pVideoDecoder && (g_pImageDX || g_pInteropFrame[0])) ? S_OK : E_FAIL);
}

// Initialize Direct3D Textures (allocation and initialization)
HRESULT CESMGpuDecode::initD3D11Surface(unsigned int nWidth, unsigned int nHeight)
{
	g_pImageDX = new ImageDX(g_pD3DDevice, g_pContext, g_pSwapChain,
		nWidth, nHeight,
		nWidth, nHeight,
		g_bUseVsync,
		ImageDX::BGRA_PIXEL_FORMAT); // ImageDX::LUMINANCE_PIXEL_FORMAT
	g_pImageDX->clear(0x80);

	g_pImageDX->setCUDAcontext(g_oContext);
	g_pImageDX->setCUDAdevice(g_oDevice);

	return S_OK;
}

BOOL CESMGpuDecode::initCudaVideo()
{
	// bind the context lock to the CUDA context
	CUresult result = cuvidCtxLockCreate(&g_CtxLock, g_oContext);
	CUVIDEOFORMATEX oFormatEx;
	memset(&oFormatEx, 0, sizeof(CUVIDEOFORMATEX));
	oFormatEx.format = g_stFormat;

	if (result != CUDA_SUCCESS)
	{
		printf("cuvidCtxLockCreate failed: %d\n", result);
		assert(0);
		return FALSE;
	}

	if(!g_pVideoSource)
	{
		return FALSE;
	}
	std::auto_ptr<VideoDecoder> apVideoDecoder(new VideoDecoder(g_pVideoSource->format(), g_oContext, g_eVideoCreateFlags, g_CtxLock));
	std::auto_ptr<VideoParser> apVideoParser(new VideoParser(apVideoDecoder.get(), g_pFrameQueue, &oFormatEx));
	g_pVideoSource->setParser(*apVideoParser.get());

	g_pVideoParser  = apVideoParser.release();
	g_pVideoDecoder = apVideoDecoder.release();

	if(!g_pVideoParser || !g_pVideoDecoder)
	{
		return FALSE;
	}
	// Create a Stream ID for handling Readback
	if (g_bReadback)
	{
		checkCudaErrors(cuStreamCreate(&g_ReadbackSID, 0));
		checkCudaErrors(cuStreamCreate(&g_KernelSID,   0));
		printf("> initCudaVideo()\n");
		printf("  CUDA Streams (%s) <g_ReadbackSID = %p>\n", ((g_ReadbackSID == 0) ? "Disabled" : "Enabled"), g_ReadbackSID);
		printf("  CUDA Streams (%s) <g_KernelSID   = %p>\n", ((g_KernelSID   == 0) ? "Disabled" : "Enabled"), g_KernelSID);
	}

	return TRUE;
}

// Initialize Direct3D
bool CESMGpuDecode::initD3D11(HWND hWnd,int *pbTCC)
{
	int dev, device_count = 0;
	bool bSpecifyDevice=false;
	char device_name[256];

	// Check for a min spec of Compute 1.1 capability before running
	checkCudaErrors(cuDeviceGetCount(&device_count));
		
	// If deviceID == 0, and there is more than 1 device, let's find the first available graphics GPU
	if (!bSpecifyDevice && device_count > 0)
	{
		for (int i=0; i < device_count; i++)
		{
			checkCudaErrors(cuDeviceGet(&dev, i));
			checkCudaErrors(cuDeviceGetName(device_name, 256, dev));

			int bSupported = checkCudaCapabilitiesDRV(1, 1, i);

			if (!bSupported)
			{
				printf("  -> GPU: \"%s\" does not meet the minimum spec of SM 1.1\n", device_name);
				printf("  -> A GPU with a minimum compute capability of SM 1.1 or higher is required.\n");
				return false;
			}

			checkCudaErrors(cuDeviceGetAttribute(pbTCC ,  CU_DEVICE_ATTRIBUTE_TCC_DRIVER, dev));
			printf("  -> GPU %d: < %s > driver mode is: %s\n", dev, device_name, *pbTCC ? "TCC" : "WDDM");

			if (*pbTCC)
			{
				g_bUseInterop = false;
				continue;
			}
			else
			{
				g_DeviceID = i; // we choose an available WDDM display device
			}

			printf("\n");
		}
	}
	else
	{
		if ((g_DeviceID > (device_count-1)) || (g_DeviceID < 0))
		{
			printf(" >>> Invalid GPU Device ID=%d specified, only %d GPU device(s) are available.<<<\n", g_DeviceID, device_count);
			printf(" >>> Valid GPU ID (n) range is between [%d,%d]...  Exiting... <<<\n", 0, device_count-1);
			return false;
		}

		// We are specifying a GPU device, check to see if it is TCC or not
		checkCudaErrors(cuDeviceGet(&dev, g_DeviceID));
		checkCudaErrors(cuDeviceGetName(device_name, 256, dev));

		checkCudaErrors(cuDeviceGetAttribute(pbTCC ,  CU_DEVICE_ATTRIBUTE_TCC_DRIVER, dev));
		printf("  -> GPU %d: < %s > driver mode is: %s\n", dev, device_name, *pbTCC ? "TCC" : "WDDM");

		if (*pbTCC)
		{
			g_bUseInterop = false;
		}
	}

	HRESULT eResult = S_OK;

	if (g_bUseInterop)
	{

		bool bDeviceFound = false;
		int device;

		// Find the first CUDA capable device
		CUresult cuStatus;
		IDXGIAdapter *pAdapter = NULL;
		IDXGIFactory1 *pFactory = NULL;
		CreateDXGIFactory1(__uuidof(IDXGIFactory1), (void **)&pFactory);
		for (unsigned int g_iAdapter = 0; pFactory->EnumAdapters(g_iAdapter, &pAdapter) == S_OK; g_iAdapter++)
		{
			DXGI_ADAPTER_DESC desc;
			pAdapter->GetDesc(&desc);

			cuStatus = cuD3D11GetDevice(&device, pAdapter);
			
			if (cudaSuccess == cuStatus)
			{
				bDeviceFound = true;
				break;
			}
		}
		pFactory->Release();

		// we check to make sure we have found a cuda-compatible D3D device to work on
		if (!bDeviceFound)
		{
			printf("\n");
			printf("  No CUDA-compatible Direct3D9 device available\n");
			// destroy the D3D device
			return false;
		}

		// Create the D3D Display Device
		/* Initialize D3D */
		DXGI_SWAP_CHAIN_DESC sc = { 0 };
		sc.BufferCount = 1;
		sc.BufferDesc.Width = g_nVideoWidth;
		sc.BufferDesc.Height = g_nVideoHeight;
		sc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		sc.BufferDesc.RefreshRate.Numerator = 0;
		sc.BufferDesc.RefreshRate.Denominator = 1;
		sc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sc.OutputWindow = hWnd;
		sc.SampleDesc.Count = 1;
		sc.SampleDesc.Quality = 0;
		sc.Windowed = TRUE;

		HRESULT hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE,
			NULL, 0, NULL, 0, D3D11_SDK_VERSION, &sc, &g_pSwapChain, &g_pD3DDevice, NULL, &g_pContext);
		if (FAILED(hr)) {
			printf("Unable to create DX11 device and swapchain, hr=0x%x", hr);
			return false;
		}
	}
	else
	{
		fprintf(stderr, "> %s is decoding w/o visualization\n", sSDKname);
		eResult = S_OK;
	}

	return (eResult == S_OK);
}

bool CESMGpuDecode::loadVideoSource(const char *video_file,unsigned int &width    , unsigned int &height,	unsigned int &dispWidth, unsigned int &dispHeight)
{
	VideoSource *pSource = NULL;
	std::auto_ptr<FrameQueue> apFrameQueue(new FrameQueue);
	try
	{
		pSource = new VideoSource(video_file, apFrameQueue.get());
	}
	catch (CUresult val)
	{
		printf("VideoSource returned an error (Video Codec is not supported), exiting...\n", val);
		g_bWaived = true;
		return FALSE;
	}
	std::auto_ptr<VideoSource> apVideoSource(pSource);

	// retrieve the video source (width,height)
	apVideoSource->getDisplayDimensions(width, height);
	apVideoSource->getDisplayDimensions(dispWidth, dispHeight);

	memset(&g_stFormat, 0, sizeof(CUVIDEOFORMAT));
	std::cout << (g_stFormat = apVideoSource->format()) << std::endl;

	if (g_bFrameRepeat)
	{
		if (apVideoSource->format().frame_rate.denominator > 0)
		{
			g_iRepeatFactor = (int)(60.0f / ceil((float)apVideoSource->format().frame_rate.numerator / (float)apVideoSource->format().frame_rate.denominator));
		}
	}

	printf("Frame Rate Playback Speed = %d fps\n", 60 / g_iRepeatFactor);

	g_pFrameQueue  = apFrameQueue.release();
	g_pVideoSource = apVideoSource.release();

	if (g_pVideoSource->format().codec == cudaVideoCodec_JPEG)
	{
		g_eVideoCreateFlags = cudaVideoCreate_PreferCUDA;
	}

	bool IsProgressive = 0;
	g_pVideoSource->getProgressive(IsProgressive);

	return IsProgressive;
}

// The window's message handler
static LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
			// use ESC to quit application
		case VK_ESCAPE:
			{
				g_bDone = true;
				PostQuitMessage(0);
				return 0;
			}
			break;

			// use space to pause playback
		case VK_SPACE:
			{
				g_bRunning = !g_bRunning;
			}
			break;
		}

		break;

	case WM_DESTROY:
		g_bDone = true;
		PostQuitMessage(0);
		return 0;

	case WM_PAINT:
		ValidateRect(hWnd, NULL);
		return 0;
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}
unsigned WINAPI CESMGpuDecode::ConvertColor(LPVOID param)
{
	MakeFrameInfo* pFrameInfo = (MakeFrameInfo*)param;
	int nHeight = pFrameInfo->nHeight;
	int nWidth = pFrameInfo->nWidth;
	
	Mat tmpYUV(nHeight+nHeight/2,nWidth,CV_8UC1,pFrameInfo->pYUVImg);
	Mat frame;
	cvtColor(tmpYUV,frame,cv::COLOR_YUV2BGR_I420);

	memcpy(pFrameInfo->Image,frame.data,nWidth*nHeight*3);

	pFrameInfo->bComplete++;

	return TRUE;
}