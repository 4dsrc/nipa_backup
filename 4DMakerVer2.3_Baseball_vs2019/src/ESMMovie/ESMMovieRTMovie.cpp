#pragma once
#include "stdafx.h"
#include "ESMMovieRTMovie.h"
#include "ESMMovieMgr.h"
#include "FFmpegManager.h"
#include "ESMFunc.h"
#include "DllFunc.h"
#include "ESMMovieFileOperation.h"
#include "ESMIni.h"

#define CPUDECODE

int ccc = 0;
CESMMovieRTMovie::CESMMovieRTMovie()
{
	m_nDSCIdx = 0;
	m_nFrameRate = 30;
	m_nMarginX = 0;
	m_nMarginY = 0;
	
	m_bMovieStart = FALSE;
	m_bThreadRun   = FALSE;

	m_pRTSPServerMgr = NULL;
	m_pClock		 = NULL;
	m_pWnd			 = NULL;

	//wgkim 191211
	m_nKZoneValidTime = 0;
	m_pMovieKZoneMgr.InitKZoneImage();
	m_pArapahoV2 = new ArapahoV2();

	InitializeCriticalSection(&m_criImage);
	InitializeCriticalSection(&m_criPathMgr);
}
CESMMovieRTMovie::~CESMMovieRTMovie()
{
	SetThreadRun(TRUE);

	DeleteCriticalSection(&m_criImage);
	DeleteCriticalSection(&m_criPathMgr);
}
void CESMMovieRTMovie::Run()
{
	m_pRTSPServerMgr = new CRTSPServerMgr;
	m_pRTSPServerMgr->SetDSCIndex(m_nDSCIdx);
	m_pRTSPServerMgr->SetFrameRate(m_nFrameRate);
	m_pRTSPServerMgr->SetMainWnd(m_pWnd);
	m_pRTSPServerMgr->InitRTSPServer();
	/*HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,RTRunThread,this,0,NULL);
	CloseHandle(hSyncTime);*/

}
unsigned WINAPI CESMMovieRTMovie::RTRunThread(LPVOID param)
{
	/*CESMMovieRTMovie* pRTMovie = (CESMMovieRTMovie*)param;

	while(!pRTMovie->GetThreadRun())
	{
		Sleep(1);

		if(pRTMovie->GetRTSendPathSize())
		{
			CString strPath = pRTMovie->GetRTSendPath();

			CString strGet4DMName = pRTMovie->Get4DMNameFromPath(strPath);

			if()

			pRTMovie->DeleteRTSendPath();
		}
	}*/

	return FALSE;
}
CString CESMMovieRTMovie::Get4DMNameFromPath(CString strPath)
{
	CString str = strPath;
	int nFind = str.ReverseFind(_T('\\'));

	str = str.Mid(0,nFind);

	nFind = str.ReverseFind(_T('\\'));
	str = str.Mid(nFind+1,str.GetLength()-nFind);

	return str;
}
void CESMMovieRTMovie::SetAdjustData(stAdjustInfo* AdjustData)
{
	CString strDSCID = AdjustData->strDSC;

	m_mapAdjData[strDSCID] = *AdjustData;
}
stAdjustInfo CESMMovieRTMovie::GetAdjustData(CString strDSCID)
{
	stAdjustInfo adjinfo = m_mapAdjData[strDSCID];

	return adjinfo;
}
void CESMMovieRTMovie::SetMargin(int nMarginX,int nMarginY)
{
	m_nMarginX = nMarginX;
	m_nMarginY = nMarginY;
}
void CESMMovieRTMovie::SetSyncTime(CString str4DM,int nTime,int nESMTick)
{
	CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DM);
	
	pRTRun->SetSyncTime(nTime);
}
void CESMMovieRTMovie::SetSavePath(CString strPath)
{
	CString str4DM = Get4DMNameFromPath(strPath);
	CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DM);

	pRTRun->SetSavePath(strPath);
	SendLog(5,_T("[ESMMovieRTMovie] - Save Path:") + strPath);
}
void CESMMovieRTMovie::SetStopIdx(CString str4DM,int n)
{
	CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DM);
	pRTRun->SetStopIdx(n);
}
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

CESMMovieRTMovie::CESMMovieRTRun::CESMMovieRTRun(CESMMovieRTMovie* pRTMovie,CString str4DM,CStopWatch* pStopWatch)
{
	avcodec_register_all();
	av_register_all();

	m_pParent = pRTMovie;
	m_str4DM = str4DM;
	m_pClock = pStopWatch; 

	m_nSyncTime = 0;
	m_nFrameRate = 30;
	
	m_nWidth = MOVIE_OUTPUT_FHD_WIDTH;
	m_nHeight = MOVIE_OUTPUT_FHD_HEIGHT;

	m_nYSize = m_nWidth * m_nHeight;
	m_nUVSize = m_nYSize / 4;

	m_nYWidth = m_nWidth;
	m_nYHeight = m_nHeight;
	m_nUVWidth = m_nWidth / 2;
	m_nUVHeight = m_nHeight / 2;

	m_nYTop = 0;
	m_nYLeft = 0;
	m_nUVTop = 0;
	m_nUVLeft = 0;

	m_bSetProperty = FALSE;
	m_bLoadAdj	   = FALSE;
	m_bPathMgrRun  = FALSE;
	m_bImageMgrRun = FALSE;
	m_bEncodeMgrRun = FALSE;
	m_bUseRect = FALSE;

	m_nStopIdx = 9999999;

	InitializeCriticalSection(&m_criPathMgr);
	InitializeCriticalSection(&m_criDecode);
	InitializeCriticalSection(&m_criSyncMgr);
	InitializeCriticalSection(&m_criEncode);
	InitializeCriticalSection(&m_criStopTime);

	m_matLogoImg = imread("C:\\Program Files\\ESMLab\\4DMaker\\img\\WaitImg.png",IMREAD_UNCHANGED);
	if(m_matLogoImg.empty())
		SendLog(5,_T("Do not loaded background image...."));
	else
	{
		CString strLog;
		strLog.Format(_T("Logo Image Loaded(%dx%dx%d)"),m_matLogoImg.cols,m_matLogoImg.rows,m_matLogoImg.channels());
		SendLog(5,strLog);
	}

	RunThread();
}
CESMMovieRTMovie::CESMMovieRTRun::~CESMMovieRTRun()
{
	DeleteCriticalSection(&m_criPathMgr);
	DeleteCriticalSection(&m_criDecode);
	DeleteCriticalSection(&m_criSyncMgr);
	DeleteCriticalSection(&m_criEncode);
	DeleteCriticalSection(&m_criStopTime);
	//Delete
}
void CESMMovieRTMovie::CESMMovieRTRun::RunThread()
{
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,_PathControlMgr,this,0,NULL);
	CloseHandle(hSyncTime);

	hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,_ImageProcessingThread,this,0,NULL);
	CloseHandle(hSyncTime);

	hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,_ImageSendThread,this,0,NULL);
	CloseHandle(hSyncTime);
	
	hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,_ImageEncodeThread,this,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI CESMMovieRTMovie::CESMMovieRTRun::_PathControlMgr(LPVOID param)
{
	CESMMovieRTRun* pRTRun = (CESMMovieRTRun*)param;
	CESMMovieRTMovie* pRTMovie = pRTRun->m_pParent;

	pRTRun->SetPathMgrFinish(TRUE);

	SendLog(5,_T("[ESMMovieRun] - ")+ pRTRun->m_str4DM + _T("_PathControlMgr Create"));

	BOOL bThreadStop = FALSE;
	int nCnt = 0;

	//wgkim 191211 Baseball Video For Detecting Test
	//int nCount = 0;

	BOOL bOpen = FALSE;
	while(!pRTMovie->GetThreadRun())
	{
		if(pRTRun->GetMoviePathSize())
		{
			CString strPath = pRTRun->GetMoviePath();

			//wgkim 191211 Baseball Video For Detecting Test skHong
			//CString strPath; 
			//strPath.Format(_T("F:\\RecordSave\\SK_\\Decoding_Test\\Test\\30148_%d.mp4"), nCount % 72);
			//strPath.Format(_T("F:\\RecordSave\\test\\02108_%d.mp4"), nCount % 254);
			//nCount++;

			int nSecIdx = pRTRun->GetSecIdxFromPath(strPath);
			pRTRun->DecodeImageFrame(strPath,nSecIdx);

			pRTRun->DeleteMoviePath();

			if(bOpen == FALSE)
			{
				bOpen = TRUE;
			}
			nCnt = 0;
		}
		else
		{
			Sleep(100);

			if(++nCnt > 75)
			{
				break;
			}
		}
	}
	pRTRun->SetPathMgrFinish(FALSE);

	SendLog(5,_T("[ESMMovieRun] - ")+ pRTRun->m_str4DM + _T("_PathControlMgr Finish"));
	//pRTMovie->DeleteRTRun(pRTRun->m_str4DM);
	return FALSE;
}
unsigned WINAPI CESMMovieRTMovie::CESMMovieRTRun::_ImageProcessingThread(LPVOID param)
{
	CESMMovieRTRun* pRTRun = (CESMMovieRTRun*)param;
	CESMMovieRTMovie* pRTMovie = pRTRun->m_pParent;
	CRTSPServerMgr* pRTSPServer = pRTMovie->GetRTSPServer();
	
	pRTRun->SetImageMgrFinish(TRUE);
	SendLog(5,_T("[ESMMovieRun] - ")+ pRTRun->m_str4DM + _T("_ImageProcessingThread Create"));

	int nCnt = 0,nWaitCnt = 0;
	vector<ENCODE_FRAME_INFO>ArrFrames;
	char str[MAX_PATH];

	//wgkim 191216
	int nRand = 0;

	bool ret = false;
	int expectedW = 0;
	int expectedH = 0;

	//ArapahoV2* p = new ArapahoV2();
	if (!pRTMovie->m_pArapahoV2)
	{
		return -1;
	}

	ArapahoV2Params ap;
	ap.datacfg = INPUT_DATA_FILE;
	ap.cfgfile = INPUT_CFG_FILE;
	ap.weightfile = INPUT_WEIGHTS_FILE;
	ap.nms = 0.4;
	ap.maxClasses = 10;
	
	ret = pRTMovie-> m_pArapahoV2->Setup(ap, expectedW, expectedH);
	if (ret == false)
	{
		if (pRTMovie->m_pArapahoV2) delete pRTMovie->m_pArapahoV2;
		pRTMovie->m_pArapahoV2 = NULL;
		return -1;
	}
	
	while(!pRTMovie->GetThreadRun())
	{
		if(pRTRun->GetFrameCount())
		{
			Mat frameFHD,frameXHD;
			MAKE_FRAME_INFO info = pRTRun->GetFrameInfo();
			
			/*if(info.nSecIdx == 0 && info.nFrameIdx == 0)
				imwrite("M:\\NoAdj.jpg",info.Y);*/
			//Image Process

			if(pRTRun->m_bSetProperty)
			{
				if (pRTRun->m_bLoadAdj)
				{
					//Change Resolution For MR
					LARGE_INTEGER start, end, freq;
					QueryPerformanceFrequency(&freq);

					QueryPerformanceCounter(&start);
					pRTRun->ImageProcessFrame(&info, frameFHD, frameXHD);

					QueryPerformanceCounter(&end);

					CString strProcessTime;
					strProcessTime.Format(_T("ProcessTime(%s) : %d"), pRTMovie->m_strDSC, (int)((end.QuadPart - start.QuadPart) / (freq.QuadPart / 1000000.0)));
					SendLog(5, strProcessTime);

					// wslee test code
					info.mr_info.speed_begin = 14020 + nRand;
					info.mr_info.speed_mid = 13012 + nRand;
					info.mr_info.speed_end = 11087 + nRand;
					info.mr_info.hit_speed = 10001 + nRand;
					info.mr_info.hit_direction = 7620 + nRand;
					info.mr_info.entry_angle = 1801 + nRand;
					info.mr_info.launch_angle = 4710 + nRand;

					if (nCnt == 0)
					{
						nRand = rand() % 1000;
					}
					////////////////////////////
				}

				else
				{
					pRTRun->ImageResizeFrame(&info);
				}
			}

			/*if(info.nSecIdx == 0 && info.nFrameIdx == 0)
				imwrite("M:\\Adj.jpg",info.Y);*/

			//sprintf(str,"[%d]%d_%d",pRTMovie->m_nDSCIdx,info.nSecIdx,info.nFrameIdx);
			//putText(info.Y,str,cv::Point(160,90),cv::FONT_HERSHEY_PLAIN,5,cv::Scalar(255),3,8);
			
			if(!pRTRun->m_matLogoImg.empty())
				pRTRun->CreateLogoImage(info);

			pRTRun->SetAddFrame(info);

			ENCODE_FRAME_INFO frameInfo;
			frameInfo.Y = info.Y.clone();
			frameInfo.U = info.U.clone();
			frameInfo.V = info.V.clone();
			ArrFrames.push_back(frameInfo);

			if(nCnt == pRTRun->m_nFrameRate - 1)
			{
				CString strTemp;
				strTemp.Format(_T("%s%s_%d.mp4"),pRTRun->GetSavePath(),pRTMovie->m_strDSC,info.nSecIdx);

				ENCODE_DATA_INFO ENCInfo;
				ENCInfo.nSecIdx = info.nSecIdx;
				ENCInfo.strPath = strTemp;
				for(int i = 0 ; i < ArrFrames.size() ; i++)
					ENCInfo.ArrEncodeFrames.push_back(ArrFrames.at(i));

				pRTRun->SetEncodeInfo(ENCInfo);
				ArrFrames.clear();

				CString strLog;
				strLog.Format(_T("[CESMMovieRTRun] - Remain (%d)"),pRTSPServer->GetImageSize(PLAY_FHD));
				SendLog(5,strLog);

				//if(info.nFrameIdx == 0)
				pRTRun->SendRTSPStateMessage(RTSP_MESSAGE_STATE_REMAIN,pRTSPServer->GetImageSize(PLAY_FHD));
			}

			pRTRun->DeleteFrameInfo();

			nCnt++;
			if(nCnt == pRTRun->m_nFrameRate)
				nCnt = 0;

			nWaitCnt = 0;
		}
		else
		{
			Sleep(10);
			if(++nWaitCnt > 500)
			{
				if(pRTRun->GetPathMgrFinish() == TRUE)
					nWaitCnt = 0;
				else
				{
					if(pRTRun->GetFrameCount() == 0)
						break;
				}
			}
		}
	}

	pRTRun->SetImageMgrFinish(FALSE);
	SendLog(5,_T("[ESMMovieRun] - ")+ pRTRun->m_str4DM + _T("_ImageProcessingThread Finish"));

	return FALSE;
}
unsigned WINAPI CESMMovieRTMovie::CESMMovieRTRun::_ImageEncodeThread(LPVOID param)
{
	CESMMovieRTRun* pRTRun = (CESMMovieRTRun*)param;
	CESMMovieRTMovie* pRTMovie = pRTRun->m_pParent;
	CRTSPServerMgr* pRTSPServer = pRTMovie->GetRTSPServer();

	pRTRun->SetEncodeMgrFinish(FALSE);
	SendLog(5,_T("[ESMMovieRun] - ")+ pRTRun->m_str4DM + _T("_ImageEncodeThread Create"));

	CESMMovieFileOperation fo;
	int nCnt = 0;
	while(!pRTMovie->GetThreadRun())
	{
		if(pRTRun->GetEncodeCount())
		{
			ENCODE_DATA_INFO info = pRTRun->GetEncodeInfo();
			CString strPath = info.strPath;
			int nSecIdx = info.nSecIdx;

			SendLog(5,_T("[ESMMovieRun] - ") + strPath + _T("- Encode"));

			CString strTempPath;
			strTempPath.Format(_T("M:\\%d.mp4"),nSecIdx);

			FFmpegManager ffmpeg(FALSE);
			BOOL bEncode = ffmpeg.InitEncode(strTempPath,pRTRun->m_nFrameRate,
				AV_CODEC_ID_H264,1, /*pRTRun->m_nWidth, pRTRun->m_nHeight//191209*/ ENCODE_MR_WIDTH, ENCODE_MR_HEIGHT, FALSE,FALSE,ENCODE_FHD_BITRATE,FALSE);

			Mat Y,U,V;
			if(bEncode == TRUE)
			{
				for(int i = 0 ; i < info.ArrEncodeFrames.size() ; i++)
				{
					Y = info.ArrEncodeFrames.at(i).Y;
					U = info.ArrEncodeFrames.at(i).U;
					V = info.ArrEncodeFrames.at(i).V;

					if(Y.data && U.data && V.data)
					{
						ffmpeg.EncodePushUsingYUV(Y.data,U.data,V.data,Y.cols,Y.rows);
					}

					info.ArrEncodeFrames.at(i).Y.release();
					info.ArrEncodeFrames.at(i).U.release();
					info.ArrEncodeFrames.at(i).V.release();
				}
				if(Y.data && U.data && V.data)
					ffmpeg.EncodePushUsingYUV(Y.data,U.data,V.data,Y.cols,Y.rows);

				ffmpeg.CloseEncode();

				if(fo.IsFileExist(strTempPath))
				{
					fo.Copy(strTempPath,strPath);
					fo.Delete(strTempPath);
				}
			}
			else
			{
				for(int i = 0 ; i < info.ArrEncodeFrames.size() ; i++)
				{
					info.ArrEncodeFrames.at(i).Y.release();
					info.ArrEncodeFrames.at(i).U.release();
					info.ArrEncodeFrames.at(i).V.release();
				}
			}
			info.ArrEncodeFrames.clear();
			
			CString strLog;
			strLog.Format(_T("[%d] Encode - %s"),nSecIdx,strPath);
			SendLog(5,strLog);

			pRTRun->DeleteEncodeInfo();

			Y.release();
			U.release();
			V.release();
		}
		else
		{
			Sleep(10);
			if(++nCnt > 400)
			{
				if(pRTRun->GetImageMgrFinish() == TRUE)
				{
					CString strLog;
					strLog.Format(_T("[EncodeMgr] - %s - check"),pRTRun->m_str4DM);
					SendLog(5,strLog);
					nCnt = 0;
				}
				else
				{
					CString strLog;
					strLog.Format(_T("[EncodeMgr] - %s - Remain(%d)"),pRTRun->m_str4DM,pRTRun->GetEncodeCount());
					SendLog(5,strLog);

					if(pRTRun->GetAddFrameCount() == 0)
						break;
				}
			}
		}
	}

	pRTRun->SetEncodeMgrFinish(TRUE);
	SendLog(5,_T("[ESMMovieRun] - ")+ pRTRun->m_str4DM + _T("_ImageEncodeThread Finish"));

	return FALSE;
}
unsigned WINAPI CESMMovieRTMovie::CESMMovieRTRun::_ImageSendThread(LPVOID param)
{
	CESMMovieRTRun* pRTRun = (CESMMovieRTRun*)param;
	CESMMovieRTMovie* pRTMovie = pRTRun->m_pParent;
	CRTSPServerMgr* pRTSPServer = pRTMovie->GetRTSPServer();

	SendLog(5,_T("[ESMMovieRun] - ")+ pRTRun->m_str4DM + _T("_ImageSendThread Start"));

	int nCnt = 0;
	BOOL bFinish = FALSE;
	while(!pRTMovie->GetThreadRun())
	{
		if(pRTRun->GetClock() >= pRTRun->GetSyncTime() && bFinish == FALSE)
		{
			if(pRTRun->GetAddFrameCount())
			{
				MAKE_FRAME_INFO info = pRTRun->GetAddFrame();
				info.str4DM.Format(_T("%s"),pRTRun->m_str4DM);
				info.nStopIdx = pRTRun->GetStopIdx();
				pRTSPServer->AddImage(info,pRTRun->GetStopIdx());

				if(info.nFrameIdx == 0)
					pRTRun->SendRTSPStateMessage(RTSP_MESSAGE_STATE_PROCESS_IN,info.nSecIdx);

				if(info.nFrameIdx == pRTRun->m_nFrameRate - 1)
				{
					CString strLog;
					strLog.Format(_T("[_ImageSendThread] - Send: %d / %d"),info.nSecIdx,info.nStopIdx);
					SendLog(5,strLog);
				}

				pRTRun->DeleteAddFrame();

				nCnt = 0;
			}
			else
			{
				if(pRTRun->GetImageMgrFinish() == TRUE)
					Sleep(1);
				else
					bFinish = TRUE;
			}
		}
		else
		{
			Sleep(10);
			if(++nCnt > 400)
			{
				if(pRTRun->GetImageMgrFinish() == TRUE/* && pRTRun->GetEncodeMgrFinish() == TRUE*/)
				{
					CString strLog;
					strLog.Format(_T("[ESMMovieRun] - %s - check"),pRTRun->m_str4DM);
					SendLog(5,strLog);
					nCnt = 0;
				}
				else
				{
					CString strLog;
					strLog.Format(_T("[ESMMovieRun] - %s - Remain(%d)"),pRTRun->m_str4DM,pRTRun->GetAddFrameCount());
					SendLog(5,strLog);

					if(pRTRun->GetAddFrameCount() == 0)
						break;
				}
			}
		}
	}
	if(pRTRun->GetEncodeMgrFinish() == FALSE)
	{
		while(1)
		{
			if(pRTRun->GetEncodeMgrFinish() == TRUE)
				break;

			Sleep(1);
		}
	}
	SendLog(5,_T("[ESMMovieRun] - ")+ pRTRun->m_str4DM + _T("_ImageSendThread Finish"));
	pRTMovie->DeleteRTRun(pRTRun->m_str4DM);
	return FALSE;
}
int CESMMovieRTMovie::CESMMovieRTRun::GetSecIdxFromPath(CString strPath)
{
	int nFind = strPath.ReverseFind('\\') + 1;
	CString csFile = strPath.Right(strPath.GetLength()-nFind);
	csFile.Trim();
	//csFile.Trim(_T("\\"));
	nFind = csFile.Find('.');
	CString csCam = csFile.Left(nFind);
	csCam.Trim();
	nFind = csCam.ReverseFind('_') + 1;

	CString strSecIdx = csCam.Right(csCam.GetLength()-nFind);

	return _ttoi(strSecIdx);
}
int CESMMovieRTMovie::CESMMovieRTRun::GetClock()
{
	m_pClock->End();

	return (int)(m_pClock->GetDurationMilliSecond() * 10);
}
int CESMMovieRTMovie::CESMMovieRTRun::DecodeImageFrame(CString strPath,int nSecIdx)
{
	SendRTSPStateMessage(RTSP_MESSAGE_STATE_RECORD_IN,nSecIdx);

	if(m_bLoadAdj == FALSE && m_bSetProperty == FALSE)
	{
		LoadAdjustData(m_pParent->m_strDSC,strPath);
	}

	char FileName[MAX_PATH];
	sprintf(FileName,"%S",strPath);

	AVFormatContext *pFormatCtx;
	int             videoStreamIdx;
	AVCodecContext  *pCodecCtx;
	AVCodec         *pCodec;
	//WaitForSingleObject(hMutex, INFINITE);

	pFormatCtx = avformat_alloc_context();

	// Open video file
	if(avformat_open_input(&pFormatCtx, FileName, 0, NULL) != 0)
	{
		SendLog(0,_T("[CESMMovieRTRun] avformat_open_input ERROR"));
		return -1; // Couldn't open file
	}

	/// Retrieve stream information
	if(avformat_find_stream_info(pFormatCtx, NULL) < 0)
	{
		SendLog(0,_T("[CESMMovieRTRun] avformat_find_stream_info ERROR"));
		return -1; // Couldn't find stream information
	}

	av_dump_format(pFormatCtx, 0, FileName, 0);

	int i = 0;
	videoStreamIdx=-1;
	for(i=0; i < pFormatCtx->nb_streams; i++)
	{
		if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) 
		{ //CODEC_TYPE_VIDEO
			videoStreamIdx=i;
			break;
		}
	}

	if(videoStreamIdx==-1)
	{
		SendLog(0,_T("[CESMMovieRTRun] videoStreamIdx ERROR"));
		return -1; // Didn't find a video stream
	}

	/// Get a pointer to the codec context for the video stream
	pCodecCtx = pFormatCtx->streams[videoStreamIdx]->codec;
	pCodecCtx->thread_type = FF_THREAD_SLICE;
	pCodecCtx->thread_count = 16;

	/// Find the decoder for the video stream
	pCodec = avcodec_find_decoder( pCodecCtx->codec_id);
	if(pCodec==NULL) 
	{
		SendLog(0,_T("[CESMMovieRTRun] pCodec ERROR"));
		return -1; // Codec not found
	}

	if( avcodec_open2(pCodecCtx, pCodec, NULL) < 0 )
	{
		SendLog(0,_T("[CESMMovieRTRun] avcodec_open2 ERROR"));
		return -1; // Could not open codec
	}

	AVPacket        packet;
	int             frameFinished;
	AVFrame        *pFrame;
	pFrame = avcodec_alloc_frame();

	int nCnt = 0;
	for(i=0; av_read_frame(pFormatCtx, &packet) >= 0;) 
	{
		// Is this a packet from the video stream?
		if(packet.stream_index==videoStreamIdx)
		{
			/// Decode video frame
			avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);

			// Did we get a video frame?
			if(frameFinished) 
			{
				Mat Y(pFrame->height,pFrame->width,CV_8UC1,pFrame->data[0]);
				Mat U(pFrame->height/2,pFrame->width/2,CV_8UC1,pFrame->data[1]);
				Mat V(pFrame->height/2,pFrame->width/2,CV_8UC1,pFrame->data[2]);

				MAKE_FRAME_INFO info;

				info.Y = Y.clone();
				info.U = U.clone();
				info.V = V.clone();

				info.nSecIdx = nSecIdx;
				info.nFrameIdx = nCnt++;

				SetFrameInfo(info);

			}
		}

		av_free_packet(&packet);
	}
	/// Close the codec
	avcodec_close(pCodecCtx);

	/// Close the video file
	avformat_close_input(&pFormatCtx);
	SendRTSPStateMessage(RTSP_MESSAGE_STATE_DECODE_IN,nSecIdx);

	SendLog(5,_T("[CESMMovieRTRun] - ")+strPath+_T(" Decoding!"));
}
void CESMMovieRTMovie::CESMMovieRTRun::LoadAdjustData(CString strDSC,CString strPath)
{
	FFmpegManager ffmpeg(FALSE);

	m_nFrameRate = ffmpeg.GetFrameCount(strPath);
	ffmpeg.GetFrameSize(strPath,m_nWidth,m_nHeight);

	m_bSetProperty = TRUE;
	SendLog(5,_T("[ESMMovieRun] - Set Property Finish"));

	stAdjustInfo adjinfo = m_pParent->GetAdjustData(strDSC);
	/*
	adjinfo.AdjAngle = -89.0452;
	adjinfo.AdjSize = 0.8273;
	adjinfo.AdjMove.x = -2.0000;
	adjinfo.AdjMove.y = 17.9153;
	adjinfo.AdjptRotate.x = 962.0000;
	adjinfo.AdjptRotate.y = 516.0000;
	m_pParent->m_nMarginX = 195;
	m_pParent->m_nMarginY = 110;
	adjinfo.stMargin.nMarginX = 195;
	adjinfo.stMargin.nMarginY = 110;
	adjinfo.rtMargin = CRect(213,120,1724,970);

	adjinfo.nWidth = 1920;
	adjinfo.nHeight = 1080;*/

	if(adjinfo.AdjptRotate.x == 0 || adjinfo.AdjptRotate.y == 0)
		return;

	//Create
	SetYAdjustData(adjinfo,m_nWidth,m_nHeight,1);
	SetUVAdjustData(adjinfo,m_nWidth/2,m_nHeight/2,0.5);

	m_bLoadAdj = TRUE;
	SendLog(5,_T("[ESMMovieRun] - Set Adjust Finish"));
}
int CESMMovieRTMovie::CESMMovieRTRun::Round(double dData)
{
	int nResult = 0;

	if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);
	else
		nResult = 0;

	return nResult;
}
void CESMMovieRTMovie::CESMMovieRTRun::SetYAdjustData(stAdjustInfo AdjustData,int nWidth,int nHeight,double dRatio,BOOL bLoad /* = TRUE */)
{
	double dbScale = AdjustData.AdjSize;

	double degree = AdjustData.AdjAngle;

	double dbRotX = Round(AdjustData.AdjptRotate.x * dRatio);
	double dbRotY = Round(AdjustData.AdjptRotate.y * dRatio);
	double dbMovX = Round(AdjustData.AdjMove.x * dRatio);
	double dbMovY = Round(AdjustData.AdjMove.y * dRatio);

	int nMarginX = m_pParent->m_nMarginX * dRatio;
	int nMarginY = m_pParent->m_nMarginY * dRatio;

	if(bLoad == FALSE)
	{
		dbScale = 1.0;
		degree = -90.0;
		dbRotX = nWidth/2;
		dbRotY = nHeight/2;
		dbMovX = 0.0;
		dbMovY = 0.0;
		nMarginX = 0.0;
		nMarginY = 0.0;
	}
	double dbAngleAdjust = -1*(degree+90);

	Mat RotMat(2,3,CV_64FC1);
	RotMat = getRotationMatrix2D(Point2f(dbRotX,dbRotY),dbAngleAdjust,dbScale);

	RotMat.at<double>(0,2) += (dbMovX/* / dbScale*/);
	RotMat.at<double>(1,2) += (dbMovY/* / dbScale*/);

	double dbMarginScale = 1 / ((double)(nWidth - nMarginX * 2)/ nWidth);

	if(AdjustData.rtMargin.left == 0 && AdjustData.rtMargin.top == 0
		&& AdjustData.rtMargin.right == 0 && AdjustData.rtMargin.bottom == 0)
	{
		m_matYAdj = RotMat.clone();
		m_nYWidth = nWidth;
		m_nYHeight = nHeight;
		m_szYRe.width = nWidth * dbMarginScale;
		m_szYRe.height = nHeight * dbMarginScale;
		m_nYLeft = (nWidth  * dbMarginScale - nWidth)/2;
		m_nYTop  = (nHeight * dbMarginScale - nHeight)/2;
	}
	else
	{
		m_matYAdj = RotMat.clone();
		m_nYWidth = nWidth;
		m_nYHeight = nHeight;
		m_szYRe.width  = AdjustData.rtMargin.right - AdjustData.rtMargin.left + 1;
		m_szYRe.height = AdjustData.rtMargin.bottom - AdjustData.rtMargin.top + 1;
		m_nYLeft = AdjustData.rtMargin.left;
		m_nYTop  = AdjustData.rtMargin.top;
		m_bUseRect = TRUE;
	}


	m_nYSize = nWidth * nHeight;
	CString str;
	str.Format(_T("%d - %d - %d - %d"),m_nYLeft,m_nYTop,m_szYRe.width,m_szYRe.height);
	SendLog(5,str);
	/*CString strLog,strTemp;
	for(int i = 0 ; i < RotMat.rows; i++)
		for(int j = 0 ; j < RotMat.cols ; j++)
		{
			strTemp.Format(_T(".2f% "),RotMat.at<double>(i,j));
			strLog.Append(strTemp);
		}

	SendLog(5,strLog);*/
}
void CESMMovieRTMovie::CESMMovieRTRun::SetUVAdjustData(stAdjustInfo AdjustData,int nWidth,int nHeight,double dRatio,BOOL bLoad /* = TRUE */)
{
	double dbScale = AdjustData.AdjSize;

	double degree = AdjustData.AdjAngle;

	double dbRotX = Round(AdjustData.AdjptRotate.x * dRatio);
	double dbRotY = Round(AdjustData.AdjptRotate.y * dRatio);
	double dbMovX = Round(AdjustData.AdjMove.x * dRatio);
	double dbMovY = Round(AdjustData.AdjMove.y * dRatio);

	int nMarginX = m_pParent->m_nMarginX * dRatio;
	int nMarginY = m_pParent->m_nMarginY * dRatio;

	if(bLoad == FALSE)
	{
		dbScale = 1.0;
		degree = -90.0;
		dbRotX = nWidth/2;
		dbRotY = nHeight/2;
		dbMovX = 0.0;
		dbMovY = 0.0;
		nMarginX = 0.0;
		nMarginY = 0.0;
	}
	double dbAngleAdjust = -1*(degree+90);

	Mat RotMat(2,3,CV_64FC1);
	RotMat = getRotationMatrix2D(Point2f(dbRotX,dbRotY),dbAngleAdjust,dbScale);

	RotMat.at<double>(0,2) += (dbMovX /*/ dbScale*/);
	RotMat.at<double>(1,2) += (dbMovY /*/ dbScale*/);

	double dbMarginScale = 1 / ((double)(nWidth - nMarginX * 2)/ nWidth);

	if(AdjustData.rtMargin.left == 0 && AdjustData.rtMargin.top == 0
		&& AdjustData.rtMargin.right == 0 && AdjustData.rtMargin.bottom == 0)
	{
		m_matUVAdj = RotMat.clone();
		m_nUVWidth = nWidth;
		m_nUVHeight = nHeight;
		m_szUVRe.width = nWidth * dbMarginScale;
		m_szUVRe.height = nHeight * dbMarginScale;
		m_nUVLeft = (nWidth  * dbMarginScale - nWidth)/2;
		m_nUVTop  = (nHeight * dbMarginScale - nHeight)/2;
	}
	else
	{
		m_matUVAdj = RotMat.clone();
		m_nUVWidth = nWidth;
		m_nUVHeight = nHeight;
		m_szUVRe.width  = ((AdjustData.rtMargin.right - AdjustData.rtMargin.left)+1)/2;
		m_szUVRe.height = ((AdjustData.rtMargin.bottom - AdjustData.rtMargin.top)+1)/2;
		m_nUVLeft = AdjustData.rtMargin.left/2;
		m_nUVTop  = AdjustData.rtMargin.top/2;
		m_bUseRect = TRUE;
	}
	CString str;
	str.Format(_T("%d - %d - %d - %d"),m_nUVLeft,m_nUVTop,m_szUVRe.width,m_szUVRe.height);
	SendLog(5,str);
	m_nUVSize = nWidth * nHeight;
}
BOOL CESMMovieRTMovie::CESMMovieRTRun::ImageProcessFrame(MAKE_FRAME_INFO* info,Mat &frameFHD,Mat &frameXHD)
{
	static int nCount = 0;
	nCount++;

	int nInterFlag = cv::INTER_LINEAR;

	cv::cuda::GpuMat gpuY, gpuYRotation, gpuYResize;
	cv::cuda::GpuMat gpuU, gpuURotation, gpuUResize;
	cv::cuda::GpuMat gpuV, gpuVRotation, gpuVResize;
	cv::cuda::GpuMat gpuYCrop, gpuUCrop, gpuVCrop;

	//Adjust
	if(m_bUseRect)
	{
		cv::cuda::GpuMat gpuYDualImage(cv::Size(ENCODE_MR_WIDTH, ENCODE_MR_HEIGHT), info->Y.type());
		cv::cuda::GpuMat gpuUDualImage(cv::Size(ENCODE_MR_WIDTH / 2, ENCODE_MR_HEIGHT / 2), info->U.type());
		cv::cuda::GpuMat gpuVDualImage(cv::Size(ENCODE_MR_WIDTH / 2, ENCODE_MR_HEIGHT / 2), info->V.type());

		gpuY.upload(info->Y);
		gpuU.upload(info->U);
		gpuV.upload(info->V);

		cv::cuda::warpAffine(gpuY, gpuYRotation, m_matYAdj, cv::Size(m_nYWidth, m_nYHeight), nInterFlag);
		cv::cuda::warpAffine(gpuU, gpuURotation, m_matUVAdj, cv::Size(m_nUVWidth, m_nUVHeight), nInterFlag);
		cv::cuda::warpAffine(gpuV, gpuVRotation, m_matUVAdj, cv::Size(m_nUVWidth, m_nUVHeight), nInterFlag);

		gpuYCrop = gpuYRotation(cv::Rect(m_nYLeft, m_nYTop, m_szYRe.width, m_szYRe.height));
		gpuUCrop = gpuURotation(cv::Rect(m_nUVLeft, m_nUVTop, m_szUVRe.width, m_szUVRe.height));
		gpuVCrop = gpuVRotation(cv::Rect(m_nUVLeft, m_nUVTop, m_szUVRe.width, m_szUVRe.height));
		
		cv::cuda::resize(gpuYCrop, gpuYResize, cv::Size(m_nYWidth, m_nYHeight), 0, 0, nInterFlag);
		cv::cuda::resize(gpuUCrop, gpuUResize, cv::Size(m_nUVWidth, m_nUVHeight), 0, 0, nInterFlag);
		cv::cuda::resize(gpuVCrop, gpuVResize, cv::Size(m_nUVWidth, m_nUVHeight), 0, 0, nInterFlag);

		gpuYResize.copyTo(gpuYDualImage(cv::Rect(0, 0, ENCODE_MR_WIDTH / 2, ENCODE_MR_HEIGHT)));
		gpuUResize.copyTo(gpuUDualImage(cv::Rect(0, 0, ENCODE_MR_WIDTH / 4, ENCODE_MR_HEIGHT / 2)));
		gpuVResize.copyTo(gpuVDualImage(cv::Rect(0, 0, ENCODE_MR_WIDTH / 4, ENCODE_MR_HEIGHT / 2)));

		//YUV420->BGR->ImageProcessing->YUV420 
		{
			//191212 YUV420->BGR
			cuda::GpuMat gpuU444; cuda::resize(gpuUCrop, gpuU444, gpuYResize.size(), INTER_NEAREST);
			cuda::GpuMat gpuV444; cuda::resize(gpuVCrop, gpuV444, gpuYResize.size(), INTER_NEAREST);
			vector<cuda::GpuMat> vecYuvTest = { gpuYResize, gpuU444, gpuV444 };

			cuda::GpuMat gpuYUVImage;
			cuda::GpuMat gpuBGRImage;
			cuda::merge(vecYuvTest, gpuYUVImage);
			cuda::cvtColor(gpuYUVImage, gpuBGRImage, COLOR_YUV2BGR);

			// 191219 skHong
			//cuda::GpuMat gpuBGRImageh; cuda::resize(gpuBGRImage, gpuBGRImageh, Size(gpuBGRImage.cols / 4, gpuBGRImage.rows / 4));
			//cuda::GpuMat gpuBGRImageh; cuda::resize(gpuBGRImage, gpuBGRImageh, Size(412, 412));

			//Todo Detecting from gpuBGRImage skHong

			//loop
			//{
			//	setup arapahoImage;
			//	p->Detect(arapahoImage);
			//	p->GetBoxex;
			//}

			box* boxes = 0;
			float* probs = 0;
			std::string* labels;

			ArapahoV2ImageBuff ImageBuffer4Yolo;
			Mat image;
			gpuBGRImage.download(image);

			int imageWidthPixels = 0;
			int imageHeightPixels = 0;
			if (image.empty())
			{				
				waitKey();
				return 0;
			}
			else
			{
				imageWidthPixels = image.cols;
				imageHeightPixels = image.rows;

				//auto detectionStareTime = std::chrono::system_clock::now();
				LARGE_INTEGER start, end, freq;
				QueryPerformanceFrequency(&freq);

				QueryPerformanceCounter(&start);

				ImageBuffer4Yolo.bgr = image.data;
				ImageBuffer4Yolo.w = imageWidthPixels;
				ImageBuffer4Yolo.h = imageHeightPixels;


				ImageBuffer4Yolo.channels = 3;

				int numObjects = 0;
				m_pParent->m_pArapahoV2->Detect(image, 0.24, 0.5, numObjects);

				//std::chrono::duration<double> detectionTime = (std::chrono::system_clock::now() - detectionStareTime);

				//printf("==> detected %d objects in %f seconds", numObjects, detectionTime);

				QueryPerformanceCounter(&end);

				CString strProcessTime;
				strProcessTime.Format(_T("DNN Process : %d"), (int)((end.QuadPart - start.QuadPart) / (freq.QuadPart / 1000000.0)));
				SendLog(5, strProcessTime);

				if (numObjects > 0 && numObjects < MAX_OBJECTS_PER_FRAME)
				{
					boxes = new box[numObjects];
					probs = new float[numObjects];
					labels = new std::string[numObjects];

					if (!boxes)
					{						
						return -1;

					}
					if (!labels)
					{						
						if (boxes)
						{
							delete[] boxes;
							boxes = NULL;
						}

						if (probs)
						{
							delete[] probs;
							probs = NULL;
						}
						return -1;
					}

					m_pParent->m_pArapahoV2->GetBoxes(boxes, labels, probs, numObjects);

					int objId = 0;
					int leftTopX = 0, leftTopY = 0, rightBotX = 0, rightBotY = 0;

					for (objId = 0; objId < numObjects; objId++)
					{
						leftTopX = 1 + imageWidthPixels * (boxes[objId].x - boxes[objId].w / 2);
						leftTopY = 1 + imageHeightPixels * (boxes[objId].y - boxes[objId].h / 2);
						rightBotX = 1 + imageWidthPixels * (boxes[objId].x + boxes[objId].w / 2);
						rightBotY = 1 + imageHeightPixels * (boxes[objId].y + boxes[objId].h / 2);
						DPRINTF("Box #%d : center {x,y}, box {w,h} = [%f, %f, %f, %f]\n", objId, boxes[objId].x, boxes[objId].y, boxes[objId].w, boxes[objId].h);

						rectangle(image, cvPoint(leftTopX, leftTopY), cvPoint(rightBotX, rightBotY), CV_RGB(255, 0, 0), 1, 8, 0);

						if (labels[objId].c_str())
						{
							char probAsString[200];
							snprintf(probAsString, 200, "%s : %f", labels[objId].c_str(), probs[objId]);
							DPRINTF("Label : %s, Probability : %s\n\n", labels[objId].c_str(), probAsString);
							putText(image, probAsString, cvPoint(leftTopX, leftTopY), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(200, 200, 250), 1, cv::LINE_AA);

						}
					}

					if (boxes)
					{
						delete[] boxes;
						boxes = NULL;
					}
					if (labels)
					{
						delete[] labels;
						boxes = NULL;
					}
					if (probs)
					{
						delete[] probs;
						probs = NULL;
					}
				}

			}

			////////////////////////////////

			//Draw K-Zone
			if (m_pParent->m_pMovieKZoneMgr.GetStateKZoneImage())
			{
				//if(nCount > 20 * 30 - 15 && nCount < 23 * 30 - 15 || nCount > 46 * 30 - 5 && nCount < 49 * 30 - 5)
				if(m_pParent->GetKZoneValidJudgment())
					m_pParent->m_pMovieKZoneMgr.DoDrawKZoneUsingGPU(m_pParent->m_strDSC, gpuBGRImage, FALSE);
				else
					m_pParent->m_pMovieKZoneMgr.DoDrawKZoneUsingGPU(m_pParent->m_strDSC, gpuBGRImage, TRUE);
			}

			//Mat mBGRImage;
			//gpuBGRImage.download(mBGRImage);

			imshow("BGRImage", image);
			waitKey(1);

			//BGR->YUV420
			cuda::cvtColor(gpuBGRImage, gpuYUVImage, COLOR_BGR2YUV);
			cuda::split(gpuYUVImage, vecYuvTest);
			vecYuvTest[0].copyTo(gpuYResize);
			cuda::resize(vecYuvTest[1], gpuUResize, cv::Size(m_nUVWidth, m_nUVHeight), INTER_NEAREST);
			cuda::resize(vecYuvTest[2], gpuVResize, cv::Size(m_nUVWidth, m_nUVHeight), INTER_NEAREST);
		}				

		gpuYResize.copyTo(gpuYDualImage(cv::Rect(ENCODE_MR_WIDTH / 2, 0, ENCODE_MR_WIDTH / 2, ENCODE_MR_HEIGHT)));
		gpuUResize.copyTo(gpuUDualImage(cv::Rect(ENCODE_MR_WIDTH / 4, 0, ENCODE_MR_WIDTH / 4, ENCODE_MR_HEIGHT / 2)));
		gpuVResize.copyTo(gpuVDualImage(cv::Rect(ENCODE_MR_WIDTH / 4, 0, ENCODE_MR_WIDTH / 4, ENCODE_MR_HEIGHT / 2)));

		gpuYDualImage.download(info->Y);
		//m_pParent->m_pMovieKZoneMgr.DrawKZoneAsSkeleton(info->Y, m_pParent->m_pMovieKZoneMgr.GetKZoneDataInfo());
		gpuUDualImage.download(info->U);
		gpuVDualImage.download(info->V);

		//Ball Draw
		//circle(info->Y, Point(960, 480), 5, Scalar(255), 3, 8);
		//circle(info->Y, Point(960, 520), 5, Scalar(255), 3, 8);
		//circle(info->Y, Point(960, 560), 5, Scalar(255), 3, 8);
		//circle(info->Y, Point(960, 600), 5, Scalar(255), 3, 8);
		//circle(info->Y, Point(960, 640), 5, Scalar(255), 3, 8);
	}
	else
	{
		gpuY.upload(info->Y);
		cv::cuda::warpAffine(gpuY,gpuYRotation,m_matYAdj,cv::Size(m_nYWidth,m_nYHeight),nInterFlag);
		cv::cuda::resize(gpuYRotation,gpuYResize,m_szYRe,0,0,nInterFlag);
		cv::cuda::GpuMat gpuYCrop = (gpuYResize)
			(cv::Rect(m_nYLeft,m_nYTop,m_nYWidth,m_nYHeight));
		//cv::cuda::resize(gpuYCrop,gpuHDY,szY_HD,0,0,nInterFlag);

		gpuYCrop.download(info->Y);
		//gpuHDY.download(HDY);
		//info->HD_Y = HDY.clone();

		//cv::cuda::GpuMat gpuU,gpuURotation,gpuUResize;
		gpuU.upload(info->U);
		cv::cuda::warpAffine(gpuU,gpuURotation,m_matUVAdj,cv::Size(m_nUVWidth,m_nUVHeight),nInterFlag);
		cv::cuda::resize(gpuURotation,gpuUResize,m_szUVRe,0,0,nInterFlag);
		cv::cuda::GpuMat gpuUCrop = (gpuUResize)
			(cv::Rect(m_nUVLeft,m_nUVTop,m_nUVWidth,m_nUVHeight));
		//cv::cuda::resize(gpuUCrop,gpuHDU,szUV_HD,0,0,nInterFlag);

		gpuUCrop.download(info->U);
		//gpuHDU.download(HDU);
		//info->HD_U = HDU.clone();

		//cv::cuda::GpuMat gpuV,gpuURotation,gpuVResize;
		gpuV.upload(info->V);
		cv::cuda::warpAffine(gpuV,gpuURotation,m_matUVAdj,cv::Size(m_nUVWidth,m_nUVHeight),nInterFlag);
		cv::cuda::resize(gpuURotation,gpuVResize,m_szUVRe,0,0,nInterFlag);
		cv::cuda::GpuMat gpuVCrop = (gpuVResize)
			(cv::Rect(m_nUVLeft,m_nUVTop,m_nUVWidth,m_nUVHeight));
		//cv::cuda::resize(gpuVCrop,gpuHDV,szUV_HD,0,0,nInterFlag);

		gpuVCrop.download(info->V);
		//gpuHDV.download(HDV);
		//info->HD_V = HDV.clone();
	}


	return TRUE;
}
BOOL CESMMovieRTMovie::CESMMovieRTRun::ImageResizeFrame(MAKE_FRAME_INFO* info)
{
	int nInterFlag = cv::INTER_LINEAR;
	int nHDWidth = ENCODE_HD_WIDTH,nHDHeight = ENCODE_HD_HEIGHT;

	cv::Size szY_HD;
	szY_HD.width = nHDWidth;
	szY_HD.height = nHDHeight;

	cv::Size szUV_HD;
	szUV_HD.width = nHDWidth/2;
	szUV_HD.height = nHDHeight/2;

	cv::cuda::GpuMat gpuY,gpuU,gpuV;
	cv::cuda::GpuMat gpuHDY,gpuHDU,gpuHDV;
	Mat HDY,HDU,HDV;

	gpuY.upload(info->Y);
	cv::cuda::resize(gpuY,gpuHDY,szY_HD,0,0,nInterFlag);
	gpuHDY.download(HDY);
	info->HD_Y = HDY.clone();

	gpuU.upload(info->U);
	cv::cuda::resize(gpuU,gpuHDU,szUV_HD,0,0,nInterFlag);
	gpuHDU.download(HDU);
	info->HD_U = HDU.clone();

	gpuV.upload(info->V);
	cv::cuda::resize(gpuV,gpuHDV,szUV_HD,0,0,nInterFlag);
	gpuHDV.download(HDV);
	info->HD_V = HDV.clone();

	HDY.release();
	HDU.release();
	HDV.release();

	return TRUE;
}
void CESMMovieRTMovie::CESMMovieRTRun::SendRTSPStateMessage(int nState,int nData)
{
	//190320 hjcho - LGU+ �ӽ� �ڵ�
	int nIdx = m_pParent->GetDSCIndex()/* - 30*/;
	/*if(nIdx != 60)
	{
		nIdx = 60 - nIdx;
	}*/
	CString* pstr4DM = new CString;
	pstr4DM->Format(_T("%s"),m_str4DM);

	ESMEvent* pEvent = new ESMEvent;
	pEvent->message  = WM_ESM_NET_RTSP_STATE_CHECK;
	pEvent->nParam1  = nState;
	pEvent->nParam2  = nIdx/*m_pParent->GetDSCIndex()*/;
	pEvent->nParam3  = nData;
	pEvent->pParam	 = (LPARAM)pstr4DM;
	//::SendMessage(m_pParent->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEvent);
	::SendMessage(m_pParent->m_pWnd->m_hWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pEvent);
}
void CESMMovieRTMovie::CESMMovieRTRun::CreateLogoImage(MAKE_FRAME_INFO info)
{
	int nStart = GetTickCount();

	if(m_matLogoImg.channels() != 4)
		return;

	if(info.Y.cols != m_matLogoImg.cols)
		return;

	Mat temp(info.Y.rows*1.5,info.Y.cols,CV_8UC1);

	memcpy(temp.data							  ,info.Y.data,info.Y.total());
	memcpy(temp.data+info.Y.total()				  ,info.U.data,info.U.total());
	memcpy(temp.data+info.Y.total()+info.U.total(),info.V.data,info.V.total());

	//Create BGR Image
	Mat BGR;//(info.Y.rows,info.Y.cols,CV_8UC3);
	cvtColor(temp,BGR,cv::COLOR_YUV2BGR_I420);

	int nWidth = BGR.cols;
	for(int i = 0 ; i < m_matLogoImg.rows ; i++)
	{
		for(int j = 0 ; j < m_matLogoImg.cols ; j++)
		{
			if(m_matLogoImg.data[nWidth * (i*4) + (j*4) + 3] == 255)
			{
				BGR.data[nWidth * (i*3) + (j*3) + 0] = m_matLogoImg.data[nWidth * (i*4) + (j*4) + 0];
				BGR.data[nWidth * (i*3) + (j*3) + 1] = m_matLogoImg.data[nWidth * (i*4) + (j*4) + 1];
				BGR.data[nWidth * (i*3) + (j*3) + 2] = m_matLogoImg.data[nWidth * (i*4) + (j*4) + 2];
			}
		}
	}

	cvtColor(BGR,temp,cv::COLOR_BGR2YUV_I420);

	memcpy(info.Y.data,temp.data							  ,info.Y.total());
	memcpy(info.U.data,temp.data+info.Y.total()				  ,info.U.total());
	memcpy(info.V.data,temp.data+info.Y.total()+info.U.total(),info.V.total());

	//CString strLog;
	//strLog.Format(_T("BGR Proc Time: %d"),GetTickCount() - nStart);
	//SendLog(5,strLog);
}

//void CESMMovieRTMovie::DrawKZoneAsSkeleton(Mat& srcImage, KZoneDataInfo* pKZoneDataInfo)
//{
//	if (pKZoneDataInfo == NULL)
//	{
//		SendLog(0, _T("Did not Set K-Zone Points"));
//		return;
//	}
//
//	Point2f vecPtKZonePoints[10];
//	for (int j = 0; j < 10; j++)
//	{
//		vecPtKZonePoints[j] = Point2f(pKZoneDataInfo->ptKZonePoint[j].x, pKZoneDataInfo->ptKZonePoint[j].y);
//	}
//	Scalar scRGB = Scalar(255, 255, 255);
//	line(srcImage, vecPtKZonePoints[0], vecPtKZonePoints[1], scRGB, 3, cv::LINE_AA, 0);
//	line(srcImage, vecPtKZonePoints[1], vecPtKZonePoints[2], scRGB, 3, cv::LINE_AA, 0);
//	line(srcImage, vecPtKZonePoints[2], vecPtKZonePoints[3], scRGB, 3, cv::LINE_AA, 0);
//	line(srcImage, vecPtKZonePoints[3], vecPtKZonePoints[0], scRGB, 3, cv::LINE_AA, 0);
//
//	line(srcImage, vecPtKZonePoints[0], vecPtKZonePoints[4], scRGB, 3, cv::LINE_AA, 0);
//	line(srcImage, vecPtKZonePoints[1], vecPtKZonePoints[5], scRGB, 3, cv::LINE_AA, 0);
//	line(srcImage, vecPtKZonePoints[2], vecPtKZonePoints[6], scRGB, 3, cv::LINE_AA, 0);
//	line(srcImage, vecPtKZonePoints[3], vecPtKZonePoints[7], scRGB, 3, cv::LINE_AA, 0);
//
//	line(srcImage, vecPtKZonePoints[4], vecPtKZonePoints[8], scRGB, 3, cv::LINE_AA, 0);
//	line(srcImage, vecPtKZonePoints[5], vecPtKZonePoints[8], scRGB, 3, cv::LINE_AA, 0);
//	line(srcImage, vecPtKZonePoints[6], vecPtKZonePoints[9], scRGB, 3, cv::LINE_AA, 0);
//	line(srcImage, vecPtKZonePoints[7], vecPtKZonePoints[9], scRGB, 3, cv::LINE_AA, 0);
//
//	line(srcImage, vecPtKZonePoints[4], vecPtKZonePoints[7], scRGB, 3, cv::LINE_AA, 0);
//	line(srcImage, vecPtKZonePoints[5], vecPtKZonePoints[6], scRGB, 3, cv::LINE_AA, 0);
//	line(srcImage, vecPtKZonePoints[8], vecPtKZonePoints[9], scRGB, 3, cv::LINE_AA, 0);
//}


BOOL CESMMovieRTMovie::GetKZoneValidJudgment()
{
	m_nKZoneValidTime--;
	if (m_nKZoneValidTime > 0)
		return TRUE;

	else
		return FALSE;
};