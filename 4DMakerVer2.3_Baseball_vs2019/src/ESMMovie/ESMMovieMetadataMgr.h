#pragma once

#include <io.h>

class CESMMovieMetadataMgr
{
public:
	CESMMovieMetadataMgr(void);
	~CESMMovieMetadataMgr(void);

//set Metadata For 4DA, 4DP
private:
	int m_nRepeatCount;
	int m_nPrevRepeatCount;
	CString m_strPrevDscId;
	CString m_strFrameMetadata;

public:
	void SetFrameCountInMetadata(CString dscId, int count, int sizeCount, int nRepeatCount);
	void SetMetadata(FILE *pFile);	
};

