////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieThreadCodec.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-26
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMMovieThreadCodec.h"
#include "ESMMovieMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CESMMovieThreadCodec, CESMMovieThread)


// CESMMovieEncoder
CESMMovieThreadCodec::CESMMovieThreadCodec(CWinThread* pParent, short nMessage, ESMMovieMsg* pMsg)
:CESMMovieThread(pParent, nMessage, pMsg)
{
}

CESMMovieThreadCodec::~CESMMovieThreadCodec()
{
	
}

//--------------------------------------------------------------------------
//!@brief	RUN THREAD
//!@param   
//!@return	
//--------------------------------------------------------------------------
int CESMMovieThreadCodec::Run(void)
{
	switch(m_nMessage)
	{
	case ESM_MOVIE_CODEC_H264_DECODE : 
		DoDecoding(m_pMsg);
		break;
	case ESM_MOVIE_CODEC_H264_ENCODE : 
		{
			TCHAR* pPath = (TCHAR*)m_pMsg->pValue2;
			CString strFilePath = pPath;
			TCHAR* pMovieName = (TCHAR*)m_pMsg->pValue3;
			CString strMovieName = pMovieName;

			DoEncoding(m_pMsg, strFilePath, strMovieName);
			delete[] pPath;
			delete[] pMovieName;
			//TRACE(_T("[ESM CODEC] H.264 Encode\n"));
			break;
		}
	case ESM_MOVIE_CODEC_H265_DECODE : 
		//TRACE(_T("[ESM CODEC] H.265 Decode\n"));
		break;
	case ESM_MOVIE_CODEC_H265_ENCODE : 
		//TRACE(_T("[ESM CODEC] H.265 Encode\n"));
		break;
	case ESM_MOVIE_CODEC_GETMOVIEINFO : 
		{
			TCHAR* pMovieId = (TCHAR*)m_pMsg->pValue1;
			CString strMovieId = pMovieId;
			GetMovieInfo(strMovieId);
			delete[] pMovieId;
		}
		TRACE(_T("[ESM CODEC]ESM_MOVIE_CODEC_GETMOVIEINFO\n"));
		break;
	default:
		TRACE(_T("[ERROR] Unknown Message on Thread Codec\n"));
		break;
	}

	if( m_pMsg )
	{
		delete m_pMsg;
		m_pMsg = NULL;
	}
	//-- 2014-05-26 hongsu@esmlab.com
	//-- Should be called FinishThread
	FinishThread();
	return 1;
}

void CESMMovieThreadCodec::DoDecoding(ESMMovieMsg* m_pMsg)
{
	CString strMovieID, strMovieName;
	int nStartFrm= m_pMsg->nParam1;
	int nEndFrm= m_pMsg->nParam2;

	strMovieName.Format(_T("%s"), (TCHAR*)m_pMsg->pValue1);
	strMovieID.Format(_T("%s"), (TCHAR*)m_pMsg->pValue2);
	delete[] (TCHAR*)m_pMsg->pValue1;
	delete[] (TCHAR*)m_pMsg->pValue2;
	ESMMovieData* pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(strMovieName, strMovieID);
	if( pMovieData == NULL)
		return ;

	FFmpegManager FFmpegMgr;
	FFmpegMgr.InsertMovieData(pMovieData->GetMoviePath(), nStartFrm, nEndFrm, pMovieData);

}


void CESMMovieThreadCodec::DoEncoding(ESMMovieMsg* m_pMsg, CString strFilePath, CString strMovieName)
{
	//TRACE(_T("\n DoDecoding Start \n"));
	FrameInfo FrameData;
	CString strMovieId;
	int nFramePos = 0;
	ESMMovieData* pMovieData = NULL;
	vector<FrameInfo>* pArrFramePosition = (vector<FrameInfo>*)m_pMsg->pValue1;

	// 1. Incoding 준비
	// incodeing 파일 열기.

	int nGopTime = 15;
	CString strTargetFile = strFilePath;
	int nFrameSpeed = (int)m_pMsg->nParam1;

	// width, Height, 어떻게 넣을지 추후 고민해 보자.
#ifdef CONSERTSAVE
	FFmpegManager FFmpegMgr;

	BOOL bEncode =FALSE;
	int nOutputWidht = MOVIE_OUTPUT_FHD_WIDTH, nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
	if(((CESMMovieMgr*)m_pParent)->GetMovieSize() == ESM_MOVIESIZE_FHD)
	{
		nOutputWidht = MOVIE_OUTPUT_FHD_WIDTH;
		nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
	}
	else
	{
		if(((CESMMovieMgr*)m_pParent)->GetUHDtoFHD())
		{
			nOutputWidht = MOVIE_OUTPUT_FHD_WIDTH;
			nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
		}
		else
		{
			nOutputWidht = MOVIE_OUTPUT_UHD_WIDTH;
			nOutputHeight = MOVIE_OUTPUT_UHD_HEIGHT;
		}
	}
	bEncode = FFmpegMgr.InitEncode(strTargetFile, nFrameSpeed, AV_CODEC_ID_MPEG2VIDEO, nGopTime, nOutputWidht, nOutputHeight);

	if( bEncode )
	{
		for( int i =0 ; i< pArrFramePosition->size();i ++)
		{
			FrameData = pArrFramePosition->at(i);
			strMovieId.Format(_T("%s"), FrameData.strMovieID);
			nFramePos = FrameData.nFramePos;
			pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(strMovieName, strMovieId);
			if( pMovieData->GetBufferSize() <= nFramePos)
				continue;

			if(pMovieData->GetFrameState(nFramePos) < ESM_CREATE_MOIVE_DONE - 1)
			{
				i--;
				Sleep(10);
				continue;
			}
			pMovieData->SetFrameState(nFramePos, ESM_CREATE_MOVIE_ORDER);
			Mat srcImage(pMovieData->GetSizeHight(), pMovieData->GetSizeWidth(), CV_8UC3);
			pMovieData->GetImage(&srcImage, nFramePos);
			FFmpegMgr.EncodePushImage((BYTE*)srcImage.data, FrameData.nIsEndFrame);

			pMovieData->DeleteImage(nFramePos);
		}
		FFmpegMgr.CloseEncode(bEncode);
	}
	else
	{
		TRACE(_T("DoEncoding Fail.\n"));
		ESMEvent* pMsg = new ESMEvent();
		pMsg->nParam1 = ESM_ERR_MSG_FFMPEG_INIT;
		pMsg->message = WM_MAKEMOVIE_MSG;
		::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	}
	
#else
	for( int i =0 ; i< pArrFramePosition->size();i ++)
	{
		FrameData = pArrFramePosition->at(i);
		strMovieId.Format(_T("%s"), FrameData.strMovieID);
		nFramePos = FrameData.nFramePos;
		pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(strMovieName, strMovieId);
		if( pMovieData->GetBufferSize() <= nFramePos)
			continue;

		if(pMovieData->GetFrameState(nFramePos) < ESM_CREATE_MOIVE_DONE - 1)
		{
			i--;
			Sleep(1);
			continue;
		}
		pMovieData->SetFrameState(nFramePos, ESM_CREATE_MOVIE_ORDER);
		Mat srcImage(pMovieData->GetSizeHight(), pMovieData->GetSizeWidth(), CV_8UC3);
		pMovieData->GetImage(&srcImage, nFramePos);
		//FFmpegMgr.EncodePushImage((BYTE*)srcImage.data, FrameData.nIsEndFrame);

		IplImage* pImg;
		pImg=&IplImage(srcImage);

		char strPath[MAX_PATH];
		char strtp[MAX_PATH];
		WideCharToMultiByte(CP_ACP, NULL, strMovieName, -1, strtp,  10, NULL, FALSE);
		sprintf(strPath, "M:\\TEMP\\Image%s_%d.jpg", strtp, i);
		cvSaveImage(strPath, pImg);

		pMovieData->DeleteImage(nFramePos);
	}
#endif
	delete pArrFramePosition;

	TCHAR* cPath = new TCHAR[1+strFilePath.GetLength()];
	_tcscpy(cPath, strFilePath);

	ESMEvent* pMsg = new ESMEvent();
	pMsg->pParam = (LPARAM)cPath;
	pMsg->message = WM_ESM_MOVIE_MAKE_FINISH_FROM_MOVIEMGR;
	::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);

	((CESMMovieMgr*)m_pParent)->DeleteMovie(strMovieName);
	TRACE(_T("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@   Encoding End   @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n"));
	// incodeing 파일 닫기
}

void CESMMovieThreadCodec::GetMovieInfo(CString strMovieId)
{
	FFmpegManager FFmpegMgr;

	stMovieInfo pMovieInfo = ((CESMMovieMgr*)m_pParent)->GetMovieInfo(strMovieId);
	//ESMMovieData* pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(nMovieIndex);
	CString strMoviePath = pMovieInfo.strMoviePath;
	CString strMovieBackupPath = pMovieInfo.strMoviePathBackup;
	if( strMovieBackupPath != _T(""))
		strMoviePath = strMovieBackupPath;
	int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3, nFrameRate =0 ;
	FFmpegMgr.GetMovieInfo(strMoviePath, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel, &nFrameRate);
	pMovieInfo.nChannel = nChannel;
	pMovieInfo.nWidth = nWidth;
	pMovieInfo.nHeight = nHight;
	pMovieInfo.nFrameCount = nFrameCount;
	pMovieInfo.nFrameRate = nFrameRate;
	((CESMMovieMgr*)m_pParent)->SetMovieInfo(pMovieInfo);
	if( nWidth == 0)
	{
		// 영상 로드 실패.
		//	AfxMessageBox(pMovieInfo.strMovieID);
	}
}
/*
void CESMMovieThreadCodec::GetMovieInfo(int nMovieIndex)
{
	FFmpegManager FFmpegMgr;

	ESMMovieData* pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(nMovieIndex);
	CString strMoviePath = pMovieData->GetMoviePath();
	int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3;
	FFmpegMgr.GetMovieInfo(strMoviePath, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel);
	pMovieData->InitData(nFrameCount, nWidth, nHight, nChannel);
	if( nWidth == 0)
		AfxMessageBox(pMovieData->GetDscId());
}
*/