#pragma once
#include "stdafx.h"
#include "ESMMovieRTSend.h"
#include "ESMMovieMgr.h"
#include "FFmpegManager.h"
#include "ESMFunc.h"
#include "DllFunc.h"

#include "opencv2\xphoto.hpp"
#include "ESMMovieFileOperation.h"
#include "ESMIni.h"


CESMMovieRTSend::CESMMovieRTSend()
{
	m_bOpenClose   = FALSE;
	m_bSetProperty = FALSE;
	m_bAdjLoad	   = FALSE;
	m_pFile = NULL;

	m_pArrReceiveTime = new vector<int>;

	InitializeCriticalSection(&m_criPathMgr);
	m_pffmpegMgr = new FFmpegManager(FALSE);
	m_bRefereeMode = FALSE;
}
CESMMovieRTSend::~CESMMovieRTSend()
{
	m_nPrevIdx = 0;
	if(m_pArrReceiveTime)
	{
		delete m_pArrReceiveTime;
		m_pArrReceiveTime = NULL;
	}
	DeleteCriticalSection(&m_criPathMgr);
	if(m_pffmpegMgr)
	{
		delete m_pffmpegMgr;
		m_pffmpegMgr = FALSE;
	}
}
unsigned WINAPI CESMMovieRTSend::RTRunThread(LPVOID param)
{
	CESMMovieRTSend* pRTSend = (CESMMovieRTSend*)param;

	CESMMovieFileOperation _file;
	int _nPrevIdx = 0;
	UINT nCnt = 0;

#if 0
	while(1)
	{
		Sleep(300);

		EnterCriticalSection(&pRTSend->m_criPathMgr);
		int nCount = pRTSend->GetRTSendPathSize();
		LeaveCriticalSection(&pRTSend->m_criPathMgr);

		if(nCount == 0)
		{
			continue;
		}

		if(nCount > _nPrevIdx)
		{
			_nPrevIdx++;
			nCnt = 0;

			if(pRTSend->m_bOpenClose == FALSE)
			{
				pRTSend->m_bOpenClose = TRUE;

				ESMEvent* pMsg	= new ESMEvent;
				pMsg->message = WM_MAKEMOVIE_GPU_FILE_OPEN_CLOSE;
				pMsg->nParam1	= (int)pRTSend->m_bOpenClose;
				::SendMessage(pRTSend->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
			}
		}
		else
		{
			if(++nCnt > 15)
			{
				if(pRTSend->m_bOpenClose == TRUE)
				{
					pRTSend->m_bOpenClose = FALSE;

					ESMEvent* pMsg	= new ESMEvent;
					pMsg->message = WM_MAKEMOVIE_GPU_FILE_OPEN_CLOSE;
					pMsg->nParam1	= (int)pRTSend->m_bOpenClose;
					::SendMessage(pRTSend->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

					_file.Delete(_T("M:\\Movie"), FALSE);

					_nPrevIdx = 0;
					nCnt = 0;
					pRTSend->m_nPrevIdx = 0;

					pRTSend->m_strArrPath.RemoveAll();

					pRTSend->ClearReceiveArr();
				}
			}
		}
	}
#else
	while(1)
	{
		EnterCriticalSection(&pRTSend->m_criPathMgr);
		int nCount = pRTSend->GetRTSendPathSize();
		LeaveCriticalSection(&pRTSend->m_criPathMgr);

		if(nCount == 0)
		{
			Sleep(100);
			if(_nPrevIdx > 0)
			{
				CString strLog;
				strLog.Format(_T("Strange"));
				SendLog(5,strLog);
			}
			continue;
		}

		if(nCount >= 1 && nCount == _nPrevIdx+1)
		{
			Sleep(10);

			CString strLog;

			EnterCriticalSection(&pRTSend->m_criPathMgr);
			CString strPath = pRTSend->GetRTSendPath(_nPrevIdx);
			LeaveCriticalSection(&pRTSend->m_criPathMgr);
			strLog.Format(_T("[%d] File Input - %s"),_nPrevIdx,strPath);
			SendLog(5,strLog);

			if(pRTSend->m_bOpenClose == FALSE)
			{
				if(pRTSend->m_bAdjLoad == FALSE)
					pRTSend->m_bSetProperty = FALSE;

				pRTSend->m_bOpenClose = TRUE;

				ESMEvent* pMsg	= new ESMEvent;
				pMsg->message = WM_MAKEMOVIE_GPU_FILE_OPEN_CLOSE;
				pMsg->nParam1	= (int)pRTSend->m_bOpenClose;
				::SendMessage(pRTSend->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
			}

			
			pRTSend->ExecuteConsole(strPath,_nPrevIdx);
			_nPrevIdx++;

			nCnt = 0;
		}	
		else
		{
			//nCnt++;
			Sleep(300);
			if(++nCnt > 15)	{
				if(pRTSend->m_bOpenClose == TRUE)
				{
					pRTSend->m_bOpenClose = FALSE;

					ESMEvent* pMsg	= new ESMEvent;
					pMsg->message = WM_MAKEMOVIE_GPU_FILE_OPEN_CLOSE;
					pMsg->nParam1	= (int)pRTSend->m_bOpenClose;
					::SendMessage(pRTSend->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

					_file.Delete(_T("M:\\Movie"), FALSE);

					_nPrevIdx = 0;
					nCnt = 0;
					pRTSend->m_strArrPath.RemoveAll();

					pRTSend->ClearReceiveArr();
				}
				//Kill Thread
				/*ESMEvent* pMsg	= new ESMEvent;
				pMsg->message = WM_MAKEMOVIE_GPU_FILE_CMD_CLOSE;
				::SendMessage(pRTSend->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);*/
			}
			TRACE(_T("ELSE\n"));
		}
	}
#endif
}
void CESMMovieRTSend::Run()
{
	HANDLE hSyncTime = NULL;
	if(GetRefereeMode() == TRUE)
		hSyncTime = (HANDLE)_beginthreadex(NULL, 0, RTRefThread, (void *)this, 0, NULL);
	else
		hSyncTime = (HANDLE)_beginthreadex(NULL, 0, RTRunThread, (void *)this, 0, NULL);

	CloseHandle(hSyncTime);
}
void CESMMovieRTSend::ExecuteConsole(CString strPath,int nSecIdx)
{
	CString strLog;
	strLog.Format(_T("[%d] ExecuteConsole"),nSecIdx);
	SendLog(5,strLog);
	
	CString strSavePath;
	strSavePath.Format(_T("M:\\Movie\\%s__%d.mp4"),m_strDSC,nSecIdx);
	int nFrameCount = 0;

#if 1
	nFrameCount = m_pffmpegMgr->GetFrameCount(strPath);

	if(!m_bSetProperty)
	{
		int nWidth = 0, nHeight = 0;
		if(m_pffmpegMgr->GetFrameSize(strPath,nWidth,nHeight))
		{
			m_nWidth    = nWidth;
			m_nHeight   = nHeight;
			m_nUVWidth  = nWidth/2;
			m_nUVHeight = nHeight/2;
		}
	}
	if(nFrameCount < 0)
		return;

#else	
	CT2CA pszConvertedAnsiString (strPath);
	string strDst(pszConvertedAnsiString);

	VideoCapture vc(strDst);
	if(!vc.isOpened())
	{
		Sleep(50);
		vc.open(strDst);
		if(!vc.isOpened())
		{
			Sleep(100);
			vc.open(strDst);
			if(!vc.isOpened())
			{
				CString strLog;
				strLog.Format(_T("[File Error] ---------- %s"),strPath);
				SendLog(1,strLog);

				return;
			}
		}
	}
	if(!m_bSetProperty)
	{
		m_nWidth    = (int)vc.get(cv::CAP_PROP_FRAME_WIDTH);
		m_nHeight   = (int)vc.get(cv::CAP_PROP_FRAME_HEIGHT);
		m_nUVWidth  = (int)vc.get(cv::CAP_PROP_FRAME_WIDTH)/2;
		m_nUVHeight = (int)vc.get(cv::CAP_PROP_FRAME_HEIGHT)/2;
	}
	nFrameCount = vc.get(cv::CAP_PROP_FRAME_COUNT);
	vc.release();
#endif
	int nGPUIdx = nSecIdx%2;
	RTSendConsoleData* pConsoleData = new RTSendConsoleData;
	pConsoleData->pParent		= this;
	pConsoleData->strPath		= strPath;
	pConsoleData->strSavePath	= strSavePath;
	pConsoleData->nGPUIdx		= nGPUIdx;
	pConsoleData->nSecIdx		= nSecIdx;
	pConsoleData->nMakeFrame	= nFrameCount;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL, 0, RTDoConsole, (void *)pConsoleData, 0, NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI CESMMovieRTSend::RTDoConsole(LPVOID param)
{
	RTSendConsoleData* pConsoleData = (RTSendConsoleData*)param;
	
	CESMMovieRTSend* pParent		  = (CESMMovieRTSend*)pConsoleData->pParent;
	CString strPath					  = pConsoleData->strPath;
	CString strSavePath				  = pConsoleData->strSavePath;
	int nGPUIdx						  = pConsoleData->nGPUIdx;
	int nSecIdx						  = pConsoleData->nSecIdx;
	int nMakeFrame					  = pConsoleData->nMakeFrame;
	delete pConsoleData;

	CString strCmd,strOpt,strLog,strTime;

	strTime.Format(_T("%d"),nSecIdx);
	strCmd.Format(_T("%s\\bin\\ESMGPUProcess.exe"),pParent->m_str4DMakerPath);

	int nReveiveTime = 1000 - pParent->GetReceiveTime(nSecIdx);
	/*CString strTemp;
	strTemp.Format(_T("ReceiveTime : %d"),pParent->GetReceiveTime(nSecIdx));
	SendLog(5,strTemp);*/

	strOpt.Format(_T("0 %s %s %d %d %d %d"),strPath,strSavePath,nGPUIdx,nMakeFrame,pParent->m_nWidth,pParent->m_nHeight);

	/*if(nSecIdx == 0)
		strOpt.Format(_T("0 %s %s %d %d 0"),strPath,strSavePath,nGPUIdx,nMakeFrame);
	else
		strOpt.Format(_T("0 %s %s %d %d %d"),strPath,strSavePath,nGPUIdx,nMakeFrame,nReveiveTime);*/

	
	strLog.Format(_T("[%d] - Cmd Processing Start[%d]"),nSecIdx,nMakeFrame);
	SendLog(5,strLog);
	if(!pParent->m_bSetProperty)
	{
		SendLog(5,strCmd);
		SendLog(5,strOpt);
		pParent->m_pFile = fopen("M:\\AdjData.txt","w");
		CString strCamID = pParent->m_strDSC;
		stAdjustInfo adjinfo = pParent->GetAdjustData(strCamID);
		if(pParent->m_bAdjLoad)
		{
			pParent->SaveAdjDataAsTxt(adjinfo,pParent->m_nWidth,pParent->m_nHeight);
			pParent->SaveAdjDataAsTxt(adjinfo,pParent->m_nUVWidth,pParent->m_nUVHeight,0.5);
		}
		else
		{
			pParent->SaveAdjDataAsTxt(adjinfo,pParent->m_nWidth,pParent->m_nHeight,1.0,FALSE);
			pParent->SaveAdjDataAsTxt(adjinfo,pParent->m_nUVWidth,pParent->m_nUVHeight,0.5,FALSE);
		}
		pParent->m_bSetProperty = TRUE;
		fclose(pParent->m_pFile);
		CString strTemp;
		strTemp.Format(_T("Set Property Finish[%d]"),pParent->m_bAdjLoad);
		SendLog(5,strTemp);
	}

	int nStart = GetTickCount();
	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;
	//lpExecInfo.nShow	=SW_SHOW;
	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	lpExecInfo.lpFile = _T("exit");
	lpExecInfo.lpVerb = L"close";
	lpExecInfo.lpParameters = NULL;

	if(TerminateProcess(lpExecInfo.hProcess,0))
	{
		lpExecInfo.hProcess = 0;
		SendLog(5,_T("End"));
	}
	
	//while(1)
	//{
	//	if(nSecIdx == 0)
	//		break;

	//	if(GetTickCount() - nStart > 2000 + nReveiveTime)
	//	{
	//		strLog.Format(_T("[%d][RTSend] Exit Time - %d"),nSecIdx,GetTickCount() - nStart);
	//		//SendLog(5,strLog);
	//		break;
	//	}
	//	Sleep(1);
	//}
	int nEnd = GetTickCount();

	strLog.Format(_T("[%d] - Cmd Processing Time : %d"),nSecIdx,nEnd - nStart);
	SendLog(5,strLog);

	CStringArray *pstrArrFrameSend = new CStringArray;
	pstrArrFrameSend->Add(pParent->m_strDSC);
	pstrArrFrameSend->Add(strTime);
	pstrArrFrameSend->Add(strSavePath);
	pstrArrFrameSend->Add(strPath);

	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message = WM_MAKEMOVIE_GPU_FILE_END;
	pMsg->pParam	= (LPARAM)pstrArrFrameSend;
	pMsg->nParam1	= pParent->GetRefereeMode();
	::SendMessage(pParent->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

	return TRUE;
}
void CESMMovieRTSend::SetAdjustData(stAdjustInfo *AdjustData)
{
	m_AdjustData.push_back(*AdjustData);
}
void CESMMovieRTSend::SaveAdjDataAsTxt(stAdjustInfo AdjustData,int nWidth,int nHeight,double dRatio,BOOL bLoad/*=TRUE*/)
{
	double dbScale = AdjustData.AdjSize;

	double degree = AdjustData.AdjAngle;

	double dbRotX = Round(AdjustData.AdjptRotate.x * dRatio);
	double dbRotY = Round(AdjustData.AdjptRotate.y * dRatio);
	double dbMovX = Round(AdjustData.AdjMove.x * dRatio);
	double dbMovY = Round(AdjustData.AdjMove.y * dRatio);

	int nMarginX = m_nMarginX * dRatio;
	int nMarginY = m_nMarginY * dRatio;

	if(bLoad == FALSE)
	{
		dbScale = 1.0;
		degree = -90.0;
		dbRotX = nWidth/2;
		dbRotY = nHeight/2;
		dbMovX = 0.0;
		dbMovY = 0.0;
		nMarginX = 0.0;
		nMarginY = 0.0;
	}
	double dbAngleAdjust = -1*(degree+90);

	Mat RotMat(2,3,CV_64FC1);
	RotMat = getRotationMatrix2D(Point2f(dbRotX,dbRotY),dbAngleAdjust,dbScale);

	/*	strLog.Format(_T("%f %f %f %f %f %f %f"),dbScale,RotMat->at<double>(0,0),RotMat->at<double>(0,1),RotMat->at<double>(0,2)
	,RotMat->at<double>(1,0),RotMat->at<double>(1,1),RotMat->at<double>(1,2));
	SendLog(5,strLog);*/

	RotMat.at<double>(0,2) += (dbMovX / dbScale);
	RotMat.at<double>(1,2) += (dbMovY / dbScale);
	/*CString strLog;
	strLog.Format(_T("%f %f %f %f %f %f %f"),dbScale,RotMat->at<double>(0,0),RotMat->at<double>(0,1),RotMat->at<double>(0,2)
	,RotMat->at<double>(1,0),RotMat->at<double>(1,1),RotMat->at<double>(1,2));
	SendLog(5,strLog);*/

	double dbMarginScale = 1 / ((double)(nWidth - nMarginX * 2)/ nWidth);

	(int)((nWidth  * dbMarginScale - nWidth)/2);
	(int)((nHeight * dbMarginScale - nHeight)/2);

	//RotMatrix
	for(int i = 0 ; i < 2; i++)
		for(int j = 0 ; j < 3; j++)
			fprintf(m_pFile,"%f\n",RotMat.at<double>(i,j));

	//Resize
	fprintf(m_pFile,"%d\n",(int)(nWidth  * dbMarginScale));
	fprintf(m_pFile,"%d\n",(int)(nHeight  * dbMarginScale));

	//Rect Point
	fprintf(m_pFile,"%d\n",(int)((nWidth  * dbMarginScale - nWidth)/2));
	fprintf(m_pFile,"%d\n",(int)((nHeight * dbMarginScale - nHeight)/2));

	//OriSize
	fprintf(m_pFile,"%d\n",nWidth);
	fprintf(m_pFile,"%d\n",nHeight);

}
int CESMMovieRTSend::Round(double dData)
{
	int nResult = 0;

	if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);
	else
		nResult = 0;

	return nResult;
}
stAdjustInfo CESMMovieRTSend::GetAdjustData(CString strDscId)
{
	stAdjustInfo AdjustData;
	
	for(int i =0 ;i < m_AdjustData.size(); i++)
	{
		if(m_AdjustData.at(i).strDSC == strDscId)
		{
			AdjustData = m_AdjustData.at(i);
			m_bAdjLoad = TRUE;
			break;
		}
		//AdjustData = (m_AdjustData.at(i));
		//if( AdjustData.strDSC == strDscId )
		//{
		//	break;
		//}
	}
	return AdjustData;
}
void CESMMovieRTSend::SetRTSendPath(CString strPath)
{
	EnterCriticalSection(&m_criPathMgr);
	m_strArrPath.Add(strPath);
	LeaveCriticalSection(&m_criPathMgr);

	/*ExecuteConsole(strPath,m_nPrevIdx);
	m_nPrevIdx++;*/
}
unsigned WINAPI CESMMovieRTSend::RTRefThread(LPVOID param)
{
	CESMMovieRTSend* pRTSend = (CESMMovieRTSend*)param;

	CESMMovieFileOperation _file;
	int _nPrevIdx = 0;
	UINT nCnt = 0;

	while(1)
	{
		EnterCriticalSection(&pRTSend->m_criPathMgr);
		int nCount = pRTSend->GetRTSendPathSize();
		LeaveCriticalSection(&pRTSend->m_criPathMgr);

		if(nCount == 0)
		{
			Sleep(100);
			if(_nPrevIdx > 0)
			{
				CString strLog;
				strLog.Format(_T("Strange"));
				SendLog(5,strLog);
			}
			continue;
		}

		if(nCount >= 1 && nCount == _nPrevIdx+1 /*&& pRTSend->GetRecordingStatus() == TRUE*/)
		{
			Sleep(10);

			CString strLog;

			EnterCriticalSection(&pRTSend->m_criPathMgr);
			CString strPath = pRTSend->GetRTSendPath(_nPrevIdx);
			LeaveCriticalSection(&pRTSend->m_criPathMgr);
			strLog.Format(_T("[%d] File Input - %s"),_nPrevIdx,strPath);
			SendLog(5,strLog);
			
			if(pRTSend->m_bOpenClose == FALSE)
			{
				pRTSend->m_bOpenClose = TRUE;

				//SendLog(5,_T("Open!"));
				ESMEvent* pMsg	= new ESMEvent;
				pMsg->message = WM_ESM_NET_REFEREE_OPENCLOSE;
				pMsg->nParam1	= (int)pRTSend->m_bOpenClose;
				::SendMessage(pRTSend->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
			}

			pRTSend->ExcuteTranscode(strPath,_nPrevIdx);
			_nPrevIdx++;

			nCnt = 0;
		}	
		else
		{
			//nCnt++;
			Sleep(300);
			if(++nCnt > 15)	{
				//SendLog(5,_T("Close!"));

				pRTSend->m_bOpenClose = FALSE;

				ESMEvent* pMsg	= new ESMEvent;
				pMsg->message = WM_ESM_NET_REFEREE_OPENCLOSE;
				pMsg->nParam1	= (int)pRTSend->m_bOpenClose;
				::SendMessage(pRTSend->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

				if(pRTSend->GetRecordingStatus() == TRUE)
				{
					while(1)
					{
						if(pRTSend->GetRecordingStatus() == FALSE)
						{
							Sleep(pRTSend->GetRTSendPathSize()*30);
							break;
						}

						Sleep(1);
					}
				}
				_file.Delete(_T("M:\\Movie"), FALSE);
				
				_nPrevIdx = 0;
				nCnt = 0;
				pRTSend->m_strArrPath.RemoveAll();
			}
			TRACE(_T("ELSE\n"));
		}
	}

	return TRUE;
}
void CESMMovieRTSend::ExcuteTranscode(CString strPath,int nIdx)
{
	CString strLog;
	strLog.Format(_T("[%d] ExecuteConsole"),nIdx);
	SendLog(5,strLog);

	CString strSavePath;
	strSavePath.Format(_T("M:\\Movie\\%s__%d.mp4"),m_strDSC,nIdx);
	int nFrameCount = 0;

	nFrameCount = m_pffmpegMgr->GetFrameCount(strPath);

	if(!m_bSetProperty)
	{
		int nWidth = 0, nHeight = 0;
		if(m_pffmpegMgr->GetFrameSize(strPath,nWidth,nHeight))
		{
			m_nWidth    = nWidth;
			m_nHeight   = nHeight;
			m_nUVWidth  = nWidth/2;
			m_nUVHeight = nHeight/2;
		}
	}
	if(nFrameCount < 0)
		return;

	int nGPUIdx = nIdx%2;
	RTSendConsoleData* pConsoleData = new RTSendConsoleData;
	pConsoleData->pParent		= this;
	pConsoleData->strPath		= strPath;
	pConsoleData->strSavePath	= strSavePath;
	pConsoleData->nGPUIdx		= nGPUIdx;
	pConsoleData->nSecIdx		= nIdx;
	pConsoleData->nMakeFrame	= nFrameCount;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL, 0, RTRefDoConsole, (void *)pConsoleData, 0, NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI CESMMovieRTSend::RTRefDoConsole(LPVOID param)
{
	RTSendConsoleData* pConsoleData = (RTSendConsoleData*)param;
	
	CESMMovieRTSend* pParent		  = (CESMMovieRTSend*)pConsoleData->pParent;
	CString strPath					  = pConsoleData->strPath;
	CString strSavePath				  = pConsoleData->strSavePath;
	int nGPUIdx						  = pConsoleData->nGPUIdx;
	int nSecIdx						  = pConsoleData->nSecIdx;
	int nMakeFrame					  = pConsoleData->nMakeFrame;
	delete pConsoleData;

	CString strCmd,strOpt,strLog,strTime;
	strTime.Format(_T("%d"),nSecIdx);
	strCmd.Format(_T("%s\\bin\\ESMGPUProcess.exe"),pParent->m_str4DMakerPath);
	strOpt.Format(_T("2 %s %s %d %d %d %d"),strPath,strSavePath,nGPUIdx,nMakeFrame,1920,1080);

	strLog.Format(_T("[%d] - Cmd Processing Start[%d]"),nSecIdx,nMakeFrame);
	SendLog(5,strLog);

	int nStart = GetTickCount();
	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;
	lpExecInfo.nShow = SW_HIDE;
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	lpExecInfo.lpFile = _T("exit");
	lpExecInfo.lpVerb = L"close";
	lpExecInfo.lpParameters = NULL;

	if(TerminateProcess(lpExecInfo.hProcess,0))
	{
		lpExecInfo.hProcess = 0;
		SendLog(5,_T("End"));
	}
	strLog.Format(_T("[%d] - Transcode Time : %d"),nSecIdx,GetTickCount() - nStart);
	SendLog(5,strLog);

	//CStringArray *pstrArrFrameSend = new CStringArray;
	//pstrArrFrameSend->Add(pParent->m_strDSC);
	//pstrArrFrameSend->Add(strTime);
	//pstrArrFrameSend->Add(strSavePath);
	//pstrArrFrameSend->Add(strPath);

	pParent->SendFinishData(strSavePath,nSecIdx);

	CESMMovieFileOperation fo;
	if(fo.IsFileExist(strPath))
	{
		fo.Delete(strPath);
	}
	if(fo.IsFileExist(strSavePath))
	{
		fo.Delete(strSavePath);
	}
	//ESMEvent* pMsg	= new ESMEvent;
	//pMsg->message = WM_ESM_NET_REFEREE_TRANSCODE_FINISH;
	//pMsg->pParam	= (LPARAM)pstrArrFrameSend;
	//pMsg->nParam1	= pParent->GetRefereeMode();
	//::SendMessage(pParent->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

	return TRUE;
}
void CESMMovieRTSend::SendFinishData(CString strPath,int nIdx)
{
	FILE *fp;
	CString strLog;

	char filename[255] = {0,};	
	sprintf(filename, "%S", strPath);

	fp = fopen(filename, "rb");

	if(fp == NULL)
	{
		strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), strPath);
		SendLog(0,strLog);
		return;
	}

	strLog.Format(_T("[#NETWORK] Send File [%s]"), strPath);
	//SendLog(1,strLog);

	fseek(fp, 0L, SEEK_END);
	int nLength = ftell(fp);
	char* buf = new char[nLength];
	fseek(fp, 0, SEEK_SET);
	fread(buf, sizeof(char), nLength, fp);

	CString *pPath = new CString;
	pPath->Format(_T("%s"),strPath);

	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_REFEREE_TRANSCODE_FINISH;
	pMsg->pParam = (LPARAM)pPath;
	pMsg->pDest = (LPARAM)buf;
	pMsg->nParam1 = nIdx;
	pMsg->nParam3 = nLength;
	::SendMessage(m_pWnd->m_hWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	
	fclose(fp);
}