#pragma once
#include "ESMDefine.h"
#include "opencv2/opencv.hpp"   
#include "opencv2/core/cuda.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/cudaarithm.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/text/ocr.hpp"
#include "opencv2/ml/ml.hpp"

using namespace cv;

#define IMAGEHIGHTDIVEDECOUNT	100
#define IMAGEWIDTHDIVIDECOUNT	100
#define IMAGELOAD_LIMIT_COUNT	800

#define MOVIE_OUTPUT_FHD_WIDTH		1920
#define MOVIE_OUTPUT_FHD_HEIGHT	1080

//Change Resolution For MR
#define MOVIE_OUTPUT_MR_WIDTH	3840
#define MOVIE_OUTPUT_MR_HEIGHT	1080

#define MOVIE_OUTPUT_UHD_WIDTH		3840
#define MOVIE_OUTPUT_UHD_HEIGHT		2160

enum{ IMAGEBUFFER_CUDA, IMAGEBUFFER_MEMORY};

struct CudaFrameData
{
	CudaFrameData()
	{
		m_nProcessOrder = ESM_CREATE_MOVIE_ORDER;
	}

	cuda::GpuMat* m_pCvImageBuffer;
	int m_nProcessOrder;
};

struct stMovieInfo
{	
	stMovieInfo()
	{
		nFrameState = 0;
		nWidth = 0;
		nHeight = 0;
		nChannel = 0;
		nFrameCount = 0;
		nFrameRate = 0;
	}
	int nFrameState;
	int nWidth;
	int nHeight;
	int nChannel;
	int nFrameCount;
	int nFrameRate;
	CString strMovieID;
	CString strMoviePath;
	CString strMoviePathBackup;
	stAdjustInfo AdjustData;
};