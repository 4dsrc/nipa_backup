#include "StdAfx.h"
#include "ESMMovieMetadataMgr.h"


CESMMovieMetadataMgr::CESMMovieMetadataMgr(void)
{
	//17-07-24 wgkim Using Metadata
	m_nRepeatCount= 0;
	m_nPrevRepeatCount = -1;
	m_strFrameMetadata = _T("");
}


CESMMovieMetadataMgr::~CESMMovieMetadataMgr(void)
{
}

void CESMMovieMetadataMgr::SetFrameCountInMetadata(CString dscId, int count, int sizeCount, int nRepeatCount)
{
	if(m_nPrevRepeatCount == -1)
	{		
		m_nPrevRepeatCount = nRepeatCount;
		m_strPrevDscId = dscId;

		m_nRepeatCount++;

		if(count == sizeCount - 1)
		{
			m_strFrameMetadata = m_strFrameMetadata+dscId+_T("-");			
			CString temp;
			temp.Format(_T("%d:%d/"), m_nPrevRepeatCount, m_nRepeatCount);
			m_strFrameMetadata = m_strFrameMetadata + temp;		
		}
	}

	else if(count == sizeCount-1)
	{
		m_strFrameMetadata = m_strFrameMetadata+dscId+_T("-");
		m_nRepeatCount++;
		CString temp;
		temp.Format(_T("%d:%d/"), m_nPrevRepeatCount, m_nRepeatCount);
		m_strFrameMetadata = m_strFrameMetadata + temp;		
	}

	else if(m_nPrevRepeatCount == nRepeatCount)
	{
		m_nRepeatCount++;
	}

	else if(m_nPrevRepeatCount != nRepeatCount)
	{
		m_strFrameMetadata = m_strFrameMetadata+m_strPrevDscId+_T("-");

		CString temp;
		temp.Format(_T("%d:%d/"), m_nPrevRepeatCount, m_nRepeatCount);
		m_strFrameMetadata = m_strFrameMetadata + temp;

		m_nRepeatCount = 1;		

		m_nPrevRepeatCount = nRepeatCount;
		m_strPrevDscId = dscId;
	}	
}

void CESMMovieMetadataMgr::SetMetadata(FILE *pFile)
{	
	char buffer[2047] = {0,};
	sprintf(buffer, "%S", m_strFrameMetadata);
	fwrite (buffer , 1 , sizeof(buffer) , pFile );	
}
