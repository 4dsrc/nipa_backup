////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieThreadFilter.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-26
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMMovieThreadFilter.h"
#include "ESMMovieMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CESMMovieThreadFilter, CESMMovieThread)


// CESMMovieEncoder
CESMMovieThreadFilter::CESMMovieThreadFilter(CWinThread* pParent, short nMessage, ESMMovieMsg* pMsg)
:CESMMovieThread(pParent, nMessage, pMsg)
{
	
}

CESMMovieThreadFilter::~CESMMovieThreadFilter()
{
	
}

//--------------------------------------------------------------------------
//!@brief	RUN THREAD
//!@param   
//!@return	
//--------------------------------------------------------------------------
int CESMMovieThreadFilter::Run(void)
{
	int nRet = 0;
	switch(m_nMessage)
	{
	case ESM_MOVIE_FILTER_DENOISE:
		TRACE(_T("[ESM FILTER] Denoise\n"));
		break;

	case ESM_MOVIE_FILTER_COLORADJUST:
		{
			TRACE(_T("[ESM FILTER] ColorAdjust\n"));
			TCHAR* pMovieName = (TCHAR*)m_pMsg->pValue2;

			DoColorAdjust(m_pMsg, pMovieName);
			break;
		}
	default:
		TRACE(_T("[ERROR] Unknown Message on Thread Codec\n"));
		break;

	}
	if( m_pMsg )
	{
		delete m_pMsg;
		m_pMsg = NULL;
	}
	//-- 2014-05-26 hongsu@esmlab.com
	//-- Should be called FinishThread
	FinishThread();
	return 1;
}

void CESMMovieThreadFilter::DoColorAdjust(ESMMovieMsg* m_pMsg, TCHAR* pMovieName)
{
	ImageManager ImageMgr;
	TCHAR strPrevMovieID[15];
	memset(strPrevMovieID, 0, 15);
	FrameInfo PrevFrameData, FrameData, NextFrameData;
	CString strMovieName = pMovieName, strMovieId;
	int nFramePos = 0;
	ESMMovieData* pPrevMovieData = NULL, *pMovieData = NULL, *pNextMovieData;
	vector<FrameInfo>* pArrFramePosition = (vector<FrameInfo>*)m_pMsg->pValue1;
	cuda::GpuMat* pNextGpuMat = NULL;
	IplImage* pPrevImg = NULL, *pCurImg = NULL, *pNextImg = NULL;
	Mat PrevImage , CurImage, NextImage;
	PrevImage.create(MOVIE_OUTPUT_FHD_HEIGHT, MOVIE_OUTPUT_FHD_WIDTH, CV_8UC3);
	CurImage.create(MOVIE_OUTPUT_FHD_HEIGHT, MOVIE_OUTPUT_FHD_WIDTH, CV_8UC3);
	NextImage.create(MOVIE_OUTPUT_FHD_HEIGHT, MOVIE_OUTPUT_FHD_WIDTH, CV_8UC3);

	for( int i =0 ;i < pArrFramePosition->size(); i++)
	{
		FrameData = pArrFramePosition->at(i);
		strMovieId.Format(_T("%s"), FrameData.strMovieID);
		nFramePos = FrameData.nFramePos;
		pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(strMovieName, strMovieId);
		if( pMovieData->GetBufferSize() <= nFramePos)
			continue;

		WaitOrder(pMovieName, FrameData.strMovieID, FrameData.nFramePos);
		
		// Frame Count 1 은 처리하지 않는다.
		if( pArrFramePosition->size() == 1)	
		{
			pMovieData->OrderAdd(nFramePos);
			break;
		}

		// Color Adjust
		if( i ==  pArrFramePosition->size() - 1) // 마지막 지점
		{
			pNextGpuMat = NULL;
		}
		else
		{
			NextFrameData = pArrFramePosition->at(i + 1);
			WaitOrder(pMovieName, NextFrameData.strMovieID, NextFrameData.nFramePos);
			if( _tcscmp(FrameData.strMovieID, NextFrameData.strMovieID) == 0 && _tcscmp(FrameData.strMovieID, strPrevMovieID) == 0)
			{
				pMovieData->OrderAdd(nFramePos);
				continue;
			}
			pNextMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(strMovieName, NextFrameData.strMovieID); 
			if(pNextMovieData->GetBufferSize() != 0)
			{
				pNextMovieData->GetImage(&NextImage, FrameData.nFramePos);
				NextImage.create(MOVIE_OUTPUT_FHD_HEIGHT, MOVIE_OUTPUT_FHD_WIDTH, CV_8UC3);
			}
		}

		pMovieData->GetImage(&CurImage, FrameData.nFramePos);
		CurImage.create(MOVIE_OUTPUT_FHD_HEIGHT, MOVIE_OUTPUT_FHD_WIDTH, CV_8UC3);

		pPrevImg = &IplImage(PrevImage);
		pCurImg = &IplImage(CurImage);
		pNextImg = &IplImage(NextImage);

		ImageMgr.MakeAvaregeImage(pPrevImg, pCurImg, pNextImg, 0, 0);

		Mat matCon = cv::cvarrToMat(pCurImg);		
		pMovieData->SetImage(FrameData.nFramePos, matCon);
		CurImage.copyTo(PrevImage);
		_tcscpy(strPrevMovieID, FrameData.strMovieID);
		
		pMovieData->OrderAdd(nFramePos);

	}
	delete[] pMovieName;
}