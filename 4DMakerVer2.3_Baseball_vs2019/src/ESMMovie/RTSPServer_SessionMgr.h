#pragma once
#include <afx.h>
#include <WinSock2.h>
#include <ws2ipdef.h>
#include <process.h>
#include "define.h"
#include "RTSPServer_ImageSender.h"
#include "DllFunc.h"

class CRTSPServerSessionMgr
{
public:
	CRTSPServerSessionMgr(SOCKET socMaster,SOCKET soc,std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*> mpImageSender);
	~CRTSPServerSessionMgr();

	//Parse RTSP Response
	char const * DateHeader();
	CString GetDateHeader();
	void Init();
	RTSP_CMD_TYPE HandleRTSPRequests(char const* aRequest,unsigned aRequestSize);
	RTSP_ERROR_STATE ParseRTSPPacket(char const* aRequest,unsigned aRequestSize);
	void Handle_RTSP_Options();
	void Handle_RTSP_Describe();
	BOOL Handle_RTSP_Setup();
	void Handle_RTSP_Play();
	void Handle_RTSP_Teardown();

	void SetPlayMethod(RTSP_PLAY_METHOD method){m_RTSPPlayMethod = method;}
 	RTSP_PLAY_METHOD GetPlayMethod(){return m_RTSPPlayMethod;}
	RTSP_CMD_TYPE GetCmdType(){return m_RTSPCmdType;}
	
	BOOL GetPlay(){return m_bPlay;}
	void SetPlay(BOOL b){m_bPlay = b;}

	CString CharToCString(char* str, BOOL bDelete = FALSE);
	//CString GetServerIP();
	CString GetMulticastIP();
	int GetMulticastPort(){return m_nMulticastPort;}
	void SetMulticastIPandPort(CString str,int nPort)
	{
		m_nMulticastPort = nPort;
		m_strMulticastIP	 = str;
	}
	CString m_strMulticastIP;
	int m_nMulticastPort;
	static unsigned WINAPI _SendThread(LPVOID param);
	void SetThreadStop(BOOL b){m_bThreadStop = b;}
	BOOL GetThreadStop(){return m_bThreadStop;}
	BOOL m_bThreadRun;
	void SetThreadRun(BOOL b){m_bThreadRun = b;}
	BOOL GetThreadRun(){return m_bThreadRun;}
	CRITICAL_SECTION m_criSendInfo;
	vector<RTSP_SEND_INFO>m_stArrSendInfo;

	void AddSendInfo(RTSP_SEND_INFO info)
	{
		if(GetThreadStop() && m_RTSPClient)
		{
			EnterCriticalSection(&m_criSendInfo);
			m_stArrSendInfo.push_back(info);
			LeaveCriticalSection(&m_criSendInfo);
		}
		else
		{
			if(info.pData)
			{
				delete info.pData;
				info.pData = NULL;
			}
		}
	}
	int GetSendInfoCount()
	{
		if(GetThreadStop() && m_RTSPClient)
		{
			int nCount = 0;
			EnterCriticalSection(&m_criSendInfo);
			nCount = m_stArrSendInfo.size();
			LeaveCriticalSection(&m_criSendInfo);

			return nCount;
		}
	}
	RTSP_SEND_INFO GetSendInfo()
	{
		if(GetThreadStop() && m_RTSPClient)
		{
			RTSP_SEND_INFO info;
			EnterCriticalSection(&m_criSendInfo);
			info = m_stArrSendInfo.at(0);
			LeaveCriticalSection(&m_criSendInfo);
			return info;
		}
	}
	void DeleteSendInfo()
	{
		if(GetThreadStop())
		{
			EnterCriticalSection(&m_criSendInfo);
			m_stArrSendInfo.erase(m_stArrSendInfo.begin());
			LeaveCriticalSection(&m_criSendInfo);
		}
	}
	void ClearSendInfo()
	{
		for(int i = 0 ; i < m_stArrSendInfo.size() ; i++)
		{
			RTSP_SEND_INFO info = m_stArrSendInfo.at(i);
#if 0
			if(info.pData[0] == '$' || info.pData[0] == 0x24)
			{
				delete info.pData;
				info.pData = NULL;
			}
			else
			{
				SendLog(5,_T("[SessionMgr] delete error"));
			}
#else
			if (&info == NULL) {
				SendLog(5, _T("[SessionMgr] delete error"));
				continue;
			}
			if(info.pData != NULL)
			{
				//wslee - sometime delete error occur when exit program 
				try {
					delete info.pData;
					info.pData = NULL;
				}
				catch (Exception e) {
					SendLog(5, _T("[SessionMgr] delete error"));
				}
			}
#endif
		}

		m_stArrSendInfo.clear();
	}
	BOOL InitTransport(u_short aRtpPort, u_short aRtcpPort, bool TCP);
	BOOL GetMulticast(){return m_bMulticast;}
	void SetMulticast(BOOL b){m_bMulticast = b;}
	BOOL GetTCP(){return m_bTCP;}
	void SetTCP(BOOL b){m_bTCP = b;}
	CString GetServerIP(){return m_strServerIP;}
	void SetServerIP(CString str){m_strServerIP = str;}
private:
	//Thread
	BOOL m_bThreadStop;

	//RTSP Socket
	SOCKET m_RTSPClient;
	SOCKET m_MasterSoc;
	//RTSP Method
	RTSP_PLAY_METHOD m_RTSPPlayMethod;
	RTSP_CMD_TYPE	 m_RTSPCmdType;
	char			 m_URLPreSuffix[RTSP_PARAM_STRING_MAX];     // stream name pre suffix 
	char			 m_URLSuffix[RTSP_PARAM_STRING_MAX];// stream name suffix
	char			 m_URLType[RTSP_PARAM_STRING_MAX];
	char			 m_CSeq[RTSP_PARAM_STRING_MAX];             // RTSP command sequence number
	char			 m_URLHostPort[RTSP_BUFFER_SIZE];           // host:port part of the URL
	unsigned		 m_ContentLength;                           // SDP string size

	std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*> m_mpImageSender;

	BOOL m_bPlay;
	BOOL m_bTCP;
	BOOL m_bMulticast;
	//UDP
	sockaddr_in m_sockRecvAddr;
	int m_nRecvLen;
	SOCKET  m_RtpSocket;          // RTP socket for streaming RTP packets to client
	SOCKET  m_RtcpSocket;         // RTCP socket for sending/receiving RTCP packages
	u_short m_ClientRTPPort;      // client port for UDP based RTP transport
	u_short m_ClientRTCPPort;     // client port for UDP based RTCP transport  
	u_short m_RtpClientPort;      // RTP receiver port on client 
	u_short m_RtcpClientPort;     // RTCP receiver port on client
	u_short m_RtpServerPort;      // RTP sender port on server 
	u_short m_RtcpServerPort;     // RTCP sender port on server                        // client port for UDP based RTCP transport
	
	CString m_strServerIP;
};
struct RTSP_RECEIVE
{
	RTSP_RECEIVE()
	{
		pRTSPServer = NULL;

	}
	CRTSPServerSessionMgr* pRTSPServer;
	SOCKET sock;
};