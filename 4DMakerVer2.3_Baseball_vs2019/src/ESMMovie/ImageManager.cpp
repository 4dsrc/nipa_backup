#include "StdAfx.h"
#include "ImageManager.h"


ImageManager::ImageManager(void)
{
}

ImageManager::~ImageManager(void)
{
}

BOOL ImageManager::MakeAvaregeImage(IplImage* pCvImage, IplImage* pCvCompareImage1, IplImage* pCvCompareImage2, int nXMargin, int nYMargin)
{
	if(pCvImage == NULL ||(pCvCompareImage1 == NULL && pCvCompareImage2 == NULL))
	{
		return FALSE;
	}

	char pchPath[MAX_PATH] = {0};

	int arrGabBlue[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrGabRed[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrGabGreen[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT];
	memset(arrGabBlue, 0, sizeof(arrGabBlue));
	memset(arrGabRed, 0, sizeof(arrGabRed));
	memset(arrGabGreen, 0, sizeof(arrGabGreen));

	int arrTarBlue[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrTarRed[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrTarGreen[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT];
	memset(arrTarBlue, 0, sizeof(arrTarBlue));
	memset(arrTarRed, 0, sizeof(arrTarRed));
	memset(arrTarGreen, 0, sizeof(arrTarGreen));

	int arrCom1Blue[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrCom1Red[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrCom1Green[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT];
	memset(arrCom1Blue, 0, sizeof(arrCom1Blue));
	memset(arrCom1Red, 0, sizeof(arrCom1Red));
	memset(arrCom1Green, 0, sizeof(arrCom1Green));

	int arrCom2Blue[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrCom2Red[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrCom2Green[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT];
	memset(arrCom2Blue, 0, sizeof(arrCom2Blue));
	memset(arrCom2Red, 0, sizeof(arrCom2Red));
	memset(arrCom2Green, 0, sizeof(arrCom2Green));

	int nImageCount = 1;
	GetAvgRGB(pCvImage, IMAGEHIGHTDIVEDECOUNT, IMAGEWIDTHDIVIDECOUNT, arrTarBlue, arrTarRed, arrTarGreen, nXMargin, nYMargin);
	if( pCvCompareImage1 != NULL)
	{
		if(GetAvgRGB(pCvCompareImage1, IMAGEHIGHTDIVEDECOUNT, IMAGEWIDTHDIVIDECOUNT, arrCom1Blue, arrCom1Red, arrCom1Green, nXMargin, nYMargin))
			nImageCount++;
	}
	if( pCvCompareImage2 != NULL)
	{
		if(GetAvgRGB(pCvCompareImage2, IMAGEHIGHTDIVEDECOUNT, IMAGEWIDTHDIVIDECOUNT, arrCom2Blue, arrCom2Red, arrCom2Green, nXMargin, nYMargin))
			nImageCount++;
	}

	for( int i =0  ;i < IMAGEHIGHTDIVEDECOUNT; i++)
	{
		for (int j =0 ; j< IMAGEWIDTHDIVIDECOUNT; j++)
		{
			//세영상의 구간별 RGB 평균
			arrGabBlue[i][j] = (arrTarBlue[i][j] + arrCom1Blue[i][j] + arrCom2Blue[i][j])  / nImageCount;
			arrGabRed[i][j] = (arrTarRed[i][j] + arrCom1Red[i][j] + arrCom2Red[i][j])  / nImageCount;
			arrGabGreen[i][j] = (arrTarGreen[i][j] + arrCom1Green[i][j] + arrCom2Green[i][j])  / nImageCount;

			// 구간별 원영상과 평균의 Gab 
			arrGabBlue[i][j] = arrGabBlue[i][j] - arrTarBlue[i][j];
			arrGabRed[i][j] = arrGabRed[i][j] - arrTarRed[i][j];
			arrGabGreen[i][j] = arrGabGreen[i][j] - arrTarGreen[i][j];
		}
	}
	int j = 0;
	int nPixel = 0, bNextPixel = 0;
	int nMaxGab = 1;
	for( int i =0  ;i < IMAGEHIGHTDIVEDECOUNT; i++)
	{
		for (j =0 ; j< IMAGEWIDTHDIVIDECOUNT-1; j++)
		{
			nPixel = arrGabBlue[i][j];
			bNextPixel = arrGabBlue[i][j + 1];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabBlue[i][j + 1] = nPixel - nMaxGab;
				else
					arrGabBlue[i][j + 1] = nPixel + nMaxGab;
			}

			nPixel = arrGabRed[i][j];
			bNextPixel = arrGabRed[i][j + 1];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabRed[i][j + 1] = nPixel - nMaxGab;
				else
					arrGabRed[i][j + 1] = nPixel + nMaxGab;
			}

			nPixel = arrGabGreen[i][j];
			bNextPixel = arrGabGreen[i][j + 1];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabGreen[i][j + 1] = nPixel - nMaxGab;
				else
					arrGabGreen[i][j + 1] = nPixel + nMaxGab;
			}

		}
		if( i != IMAGEHIGHTDIVEDECOUNT - 1)
		{
			if(abs(arrGabBlue[i][j]) - abs(arrGabBlue[i + 1][j]) > nMaxGab)
			{
				if( arrGabBlue[i][j] > arrGabBlue[i + 1][j ])
					arrGabBlue[i][j + 1] = arrGabBlue[i][j] - nMaxGab;
				else
					arrGabBlue[i][j + 1] = arrGabBlue[i][j] + nMaxGab;
			}

			if(abs(arrGabRed[i][j]) - abs(arrGabRed[i + 1][j]) > nMaxGab)
			{
				if( arrGabRed[i][j] > arrGabRed[i + 1][j])
					arrGabRed[i][j + 1] = arrGabRed[i][j] - nMaxGab;
				else
					arrGabRed[i][j + 1] = arrGabRed[i][j] + nMaxGab;
			}

			if(abs(arrGabGreen[i][j]) - abs(arrGabGreen[i + 1][j]) > nMaxGab)
			{
				if( arrGabGreen[i][j] > arrGabGreen[i + 1][j])
					arrGabGreen[i][j + 1] = arrGabGreen[i][j] - nMaxGab;
				else
					arrGabGreen[i][j + 1] = arrGabGreen[i][j] + nMaxGab;
			}
		}
	}
	for( int i =0  ;i < IMAGEHIGHTDIVEDECOUNT-1; i++)
	{
		for (j =0 ; j< IMAGEWIDTHDIVIDECOUNT; j++)
		{
			nPixel = arrGabBlue[i][j];
			bNextPixel = arrGabBlue[i + 1][j];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabBlue[i + 1][j] = nPixel - nMaxGab;
				else
					arrGabBlue[i + 1][j] = nPixel + nMaxGab;
			}

			nPixel = arrGabRed[i][j];
			bNextPixel = arrGabRed[i + 1][j];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabRed[i + 1][j] = nPixel - nMaxGab;
				else
					arrGabRed[i + 1][j] = nPixel + nMaxGab;
			}

			nPixel = arrGabGreen[i][j];
			bNextPixel = arrGabGreen[i + 1][j];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabGreen[i + 1][j] = nPixel - nMaxGab;
				else
					arrGabGreen[i + 1][j] = nPixel + nMaxGab;
			}

		}
		if( j != IMAGEHIGHTDIVEDECOUNT - 1)
		{
			if(abs(arrGabBlue[i][j]) - abs(arrGabBlue[i][j + 1]) > nMaxGab)
			{
				if( arrGabBlue[i][j] > arrGabBlue[i][j + 1 ])
					arrGabBlue[i + 1][j] = arrGabBlue[i][j] - nMaxGab;
				else
					arrGabBlue[i + 1][j] = arrGabBlue[i][j] + nMaxGab;
			}

			if(abs(arrGabRed[i][j]) - abs(arrGabRed[i][j + 1]) > nMaxGab)
			{
				if( arrGabRed[i][j] > arrGabRed[i][j + 1])
					arrGabRed[i + 1][j] = arrGabRed[i][j] - nMaxGab;
				else
					arrGabRed[i + 1][j] = arrGabRed[i][j] + nMaxGab;
			}

			if(abs(arrGabGreen[i][j]) - abs(arrGabGreen[i][j + 1]) > nMaxGab)
			{
				if( arrGabGreen[i][j] > arrGabGreen[i][j + 1])
					arrGabGreen[i + 1][j] = arrGabGreen[i][j] - nMaxGab;
				else
					arrGabGreen[i + 1][j] = arrGabGreen[i][j] + nMaxGab;
			}
		}
	}
	MakeResultImage(pCvImage, IMAGEHIGHTDIVEDECOUNT, IMAGEWIDTHDIVIDECOUNT, arrGabBlue, arrGabRed, arrGabGreen, nXMargin, nYMargin);
	return TRUE;
}

BOOL ImageManager::GetAvgRGB(IplImage*	pPicture, int nHightDivCount, int nWidhtDivCount, 
	int nAvgBlue[][IMAGEWIDTHDIVIDECOUNT], int nAvgRed[][IMAGEWIDTHDIVIDECOUNT], int nAvgGreen[][IMAGEWIDTHDIVIDECOUNT] , int nXMargin, int nYMargin)
{
	double nHightCoeff = double(pPicture->height - nYMargin * 2)/ (double)nHightDivCount;
	double nWidthCoeff = double(pPicture->width - nXMargin * 2) / (double)nWidhtDivCount;
	for(int nHiDivIndex = 0; nHiDivIndex < nHightDivCount; nHiDivIndex++)
	{
		for(int nWiDivIndex = 0; nWiDivIndex < nWidhtDivCount; nWiDivIndex++)
		{
			for (int nHightIndex = int(nHightCoeff * nHiDivIndex) + nYMargin; nHightIndex < int(nHightCoeff * (nHiDivIndex + 1)) + nYMargin; nHightIndex++ )
			{
				for (int nWidthIndex = int(nWidthCoeff * nWiDivIndex) + nXMargin; nWidthIndex < int(nWidthCoeff * (nWiDivIndex + 1))  + nXMargin; nWidthIndex++ )
				{
					nAvgBlue[nHiDivIndex][nWiDivIndex] += (unsigned char)pPicture->imageData[nHightIndex * pPicture->widthStep + nWidthIndex *3 ];		// Blue
					nAvgRed[nHiDivIndex][nWiDivIndex] +=(unsigned char)pPicture->imageData[nHightIndex * pPicture->widthStep + nWidthIndex * 3 + 2];	// Red
					nAvgGreen[nHiDivIndex][nWiDivIndex] +=(unsigned char)pPicture->imageData[nHightIndex * pPicture->widthStep + nWidthIndex * 3 + 1];	// Green
				}
			}
			nAvgBlue[nHiDivIndex][nWiDivIndex] = nAvgBlue[nHiDivIndex][nWiDivIndex] / (int)(nHightCoeff * nWidthCoeff);
			nAvgRed[nHiDivIndex][nWiDivIndex] = nAvgRed[nHiDivIndex][nWiDivIndex] / (int)(nHightCoeff * nWidthCoeff);
			nAvgGreen[nHiDivIndex][nWiDivIndex] = nAvgGreen[nHiDivIndex][nWiDivIndex] / (int)(nHightCoeff * nWidthCoeff);
		}
	}
	return TRUE;
}

BOOL ImageManager::MakeResultImage(IplImage*	pPicture, int nHightDivCount, int nWidhtDivCount, 
	int arrGabBlue[][IMAGEWIDTHDIVIDECOUNT], int arrGabRed[][IMAGEWIDTHDIVIDECOUNT], int arrGabGreen[][IMAGEWIDTHDIVIDECOUNT] , int nXMargin, int nYMargin )
{
	double nHightCoeff = double(pPicture->height - nYMargin * 2)/ (double)nHightDivCount;
	double nWidthCoeff = double(pPicture->width - nXMargin * 2) / (double)nWidhtDivCount;
	for(int nHiDivIndex = 0; nHiDivIndex < nHightDivCount; nHiDivIndex++)
	{
		for(int nWiDivIndex = 0; nWiDivIndex < nWidhtDivCount; nWiDivIndex++)
		{
			for (int nHightIndex = int(nHightCoeff * nHiDivIndex) + nYMargin; nHightIndex < int(nHightCoeff * (nHiDivIndex + 1)) + nYMargin; nHightIndex++ )
			{
				for (int nWidthIndex = int(nWidthCoeff * nWiDivIndex) + nXMargin; nWidthIndex < int(nWidthCoeff * (nWiDivIndex + 1))  + nXMargin; nWidthIndex++ )
				{
					int nBlue = (unsigned char)pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3];
					if( nBlue + arrGabBlue[nHiDivIndex][nWiDivIndex] > 255)
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 ] = (char)255;
					else if(nBlue + arrGabBlue[nHiDivIndex][nWiDivIndex] < 0)
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 ] = (char)0;
					else
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 ] = nBlue + arrGabBlue[nHiDivIndex][nWiDivIndex];			//Blue

					int nGreen = (unsigned char)pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3+ 1];
					if(nGreen + arrGabGreen[nHiDivIndex][nWiDivIndex] > 255 )
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 1] = (char)255;
					else if(nGreen + arrGabGreen[nHiDivIndex][nWiDivIndex] < 0)
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 1] = (char)0;
					else
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 1] = nGreen + arrGabGreen[nHiDivIndex][nWiDivIndex];		//Green

					int nRed = (unsigned char)pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3+ 2];
					if( nRed + arrGabRed[nHiDivIndex][nWiDivIndex] > 255 )
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 2] = (char)255;
					else if(nRed + arrGabRed[nHiDivIndex][nWiDivIndex] < 0)
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 2] = (char)0;
					else
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 2] = nRed + arrGabRed[nHiDivIndex][nWiDivIndex];		//Red

				}
			}
		}
	}
	return TRUE;
}