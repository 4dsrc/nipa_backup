#ifdef CREATE_EXPORTS
#define MYDLL __declspec(dllexport)
#else
#define MYDLL __declspec(dllimport)
#endif

#ifndef _ENABLE_ARAPAHO
#define _ENABLE_ARAPAHO

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <string>

#include "darknet.h"
#include <opencv2/opencv.hpp>
#include "..\darknet\network.h"
#include "..\darknet\detection_layer.h"
#include "..\darknet\cost_layer.h"
#include "..\darknet\utils.h"
#include "..\darknet\parser.h"
#include "..\darknet\image.h"
#include "..\darknet\box.h"
#include "..\darknet\region_layer.h"
#include "..\darknet\option_list.h"

//////////////////////////////////////////////////////////////////////////

#define ARAPAHO_MAX_CLASSES (200)

#ifdef _DEBUG
#define DPRINTF printf
#define EPRINTF printf
#else
#define DPRINTF printf
#define EPRINTF printf
#endif

    //////////////////////////////////////////////////////////////////////////

    struct ArapahoV2Params
    {
        char*			datacfg;
        char*			cfgfile;
        char*			weightfile;
        float			nms;
        int				maxClasses;
    };

    struct ArapahoV2ImageBuff
    {
        unsigned char*	bgr;
        int				w;
        int				h;
        int				channels;
    };
    //////////////////////////////////////////////////////////////////////////

    class ArapahoV2
    {
    public:
		MYDLL ArapahoV2();
		MYDLL ~ArapahoV2();
		
		MYDLL bool Setup(ArapahoV2Params & p, int & expectedWidth, int & expectedHeight);
		MYDLL bool Detect(ArapahoV2ImageBuff& imageBuff, float thresh, float hier_thresh, int& objectCount);
		MYDLL bool Detect(const cv::Mat& inputMat, float thresh, float hier_thresh, int& objectCount);
		MYDLL bool GetBoxes(box* outBoxes, std::string* outLabels, float* outProbs, int boxCount);
 
	private:
        detection		*dets;
        int				nboxes;
        char			**classNames;
        bool			bSetup;
        network*		net;
        layer			l;
        float			nms;
        int				maxClasses;
        float			threshold;

		MYDLL void __Detect(float* inData, int w, int h, float thresh, float hier_thresh, int & objectCount);
    };

    //////////////////////////////////////////////////////////////////////////


#endif // _ENABLE_ARAPAHO
