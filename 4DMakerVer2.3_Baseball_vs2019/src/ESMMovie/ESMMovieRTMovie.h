#pragma once
#include "stdafx.h"
#include <vector>
#include "ESMDefine.h"
#include "FFmpegManager.h"
#include "ESMMovieThread.h"
#include "ESMMovieThreadImage.h"
#include "RTSPServerManager.h"
#include "StopWatch.h"
#include "darknet.h"
#include "arapaho.hpp"

using namespace std;

static char INPUT_DATA_FILE[] = "F:\\coco.data";
static char INPUT_CFG_FILE[] = "F:\\yolov3-tiny.cfg";
static char INPUT_WEIGHTS_FILE[] = "F:\\yolov3-tiny.weights";

#define MAX_OBJECTS_PER_FRAME	50
#define TARGET_SHOW_FPS			30

struct ENCODE_FRAME_INFO
{
	Mat Y;
	Mat U;
	Mat V;
};
struct ENCODE_DATA_INFO
{
	CString strPath;
	vector<ENCODE_FRAME_INFO>ArrEncodeFrames;
	int nSecIdx;
};

class AFX_EXT_CLASS CESMMovieRTMovie
{
	//DECLARE_DYNCREATE(CESMMovieRTSend)
public:
	CESMMovieRTMovie();
	~CESMMovieRTMovie();
	void Set4DAInfo(CString strIP,CString strDSC,int nIdx,int nFrameRate)
	{m_strIP = strIP; m_strDSC = strDSC;m_nDSCIdx = nIdx;m_nFrameRate = nFrameRate;}
	void Run();
	
	/*Thread*/
	static unsigned WINAPI RTRunThread(LPVOID param);

	/*Functions*/
	CString Get4DMNameFromPath(CString strPath);
public:/*Adjust Data*/
	void SetAdjustData(stAdjustInfo* AdjustData);
	stAdjustInfo GetAdjustData(CString strDSCID);
	void SetMargin(int nMarginX,int nMarginY);
private:
	map<CString,stAdjustInfo>m_mapAdjData;

public:
	/*Inline func - Set clock*/
	void SetClock(CStopWatch* stopwatch)
	{
		m_pClock = stopwatch;
	}
	/*Inline func - main*/
	void SetParent(CWnd * pWnd) 
	{ 
		m_pWnd = pWnd; 
	}
	/*Inline func - Load check*/
	void SetMovieStart(BOOL b)
	{
		m_bMovieStart = b;
	}
	BOOL GetMovieStart()
	{
		return m_bMovieStart;
	}
	/*Inline func - Thread run*/
	void SetThreadRun(BOOL b)
	{
		m_bThreadRun = b;
	}
	BOOL GetThreadRun()
	{
		return m_bThreadRun;
	}
	/*Inline func - RTSPServerMgr*/
	CRTSPServerMgr* GetRTSPServer()
	{
		return m_pRTSPServerMgr;
	}
private:/*Critical section*/
	CRITICAL_SECTION m_criImage;
	CRITICAL_SECTION m_criPathMgr;

private:/*Valiable*/
	vector<CString> m_strArrPath;

	CString m_strIP;
	CString m_strDSC;
	
	int m_nDSCIdx;
	int m_nFrameRate;
	int m_nMarginX;
	int m_nMarginY;

	BOOL m_bMovieStart;
	BOOL m_bThreadRun;
private:/*classes*/
	CWnd * m_pWnd;

	CRTSPServerMgr* m_pRTSPServerMgr;
	CStopWatch *m_pClock;
	
public:
	class CESMMovieRTRun
	{

	public:
		CESMMovieRTRun(CESMMovieRTMovie* pRTMovie,CString str4DM,CStopWatch* pStopWatch);
		~CESMMovieRTRun();

		CString m_str4DM;
		/*Thread*/
		void RunThread();
		static unsigned WINAPI _PathControlMgr(LPVOID param);
		int GetSecIdxFromPath(CString strPath);
		/*Message management*/
		void SendRTSPStateMessage(int nState,int nData);
		/*Image Process*/
		int Round(double dData);
		void CreateLogoImage(MAKE_FRAME_INFO info);
		void SetYAdjustData(stAdjustInfo AdjustData,int nWidth,int nHeight,double dRatio,BOOL bLoad = TRUE);
		void SetUVAdjustData(stAdjustInfo AdjustData,int nWidth,int nHeight,double dRatio,BOOL bLoad = TRUE);
		void LoadAdjustData(CString strDSC,CString strPath);
		int DecodeImageFrame(CString strPath,int nSecIdx);
		BOOL ImageProcessFrame(MAKE_FRAME_INFO* info,Mat &frameFHD,Mat &frameXHD);
		BOOL ImageResizeFrame(MAKE_FRAME_INFO* info);
		void SetSavePath(CString strSavePath)
		{
			m_strSavePath = strSavePath;
		}
		CString GetSavePath()
		{
			return m_strSavePath;
		}
		/*Inline func - Path*/
		vector<CString>m_strArrPath;
		void AddMoviePath(CString strPath)
		{
			EnterCriticalSection(&m_criPathMgr);
			m_strArrPath.push_back(strPath);
			LeaveCriticalSection(&m_criPathMgr);
		}
		void DeleteMoviePath()
		{
			EnterCriticalSection(&m_criPathMgr);
			m_strArrPath.erase(m_strArrPath.begin());
			LeaveCriticalSection(&m_criPathMgr);
		}
		CString GetMoviePath()
		{
			CString str;
			EnterCriticalSection(&m_criPathMgr);
			str = m_strArrPath.at(0);
			LeaveCriticalSection(&m_criPathMgr);
			return str;
		}
		int GetMoviePathSize()
		{
			int n = 0;
			EnterCriticalSection(&m_criPathMgr);
			n = m_strArrPath.size();
			LeaveCriticalSection(&m_criPathMgr);
			return n;
		}
		BOOL GetPathMgrFinish()
		{
			return m_bPathMgrRun;
		}
		void SetPathMgrFinish(BOOL b)
		{
			m_bPathMgrRun = b;
		}
		/*Inline func - Decode Array*/
		static unsigned WINAPI _ImageProcessingThread(LPVOID param);
		vector<MAKE_FRAME_INFO>m_stArrFrameInfo;
		int GetFrameCount()
		{
			EnterCriticalSection(&m_criDecode);
			int nSize = m_stArrFrameInfo.size();
			LeaveCriticalSection(&m_criDecode);
			return nSize;
		}
		MAKE_FRAME_INFO GetFrameInfo()
		{
			EnterCriticalSection(&m_criDecode);
			MAKE_FRAME_INFO info = m_stArrFrameInfo.at(0);
			LeaveCriticalSection(&m_criDecode);
			return info;
		}
		void DeleteFrameInfo()
		{
			EnterCriticalSection(&m_criDecode);
			m_stArrFrameInfo.erase(m_stArrFrameInfo.begin());
			LeaveCriticalSection(&m_criDecode);
		}
		void SetFrameInfo(MAKE_FRAME_INFO info)
		{
			EnterCriticalSection(&m_criDecode);
			m_stArrFrameInfo.push_back(info);
			LeaveCriticalSection(&m_criDecode);
		}
		void SetImageMgrFinish(BOOL b)
		{
			m_bImageMgrRun = b;
		}
		BOOL GetImageMgrFinish()
		{
			return m_bImageMgrRun;
		}
		void SetEncodeMgrFinish(BOOL b)
		{
			m_bEncodeMgrRun = b;
		}
		BOOL GetEncodeMgrFinish()
		{
			return m_bEncodeMgrRun;
		}
		/*Inline func - Encode Array*/
		static unsigned WINAPI _ImageEncodeThread(LPVOID param);
		vector<ENCODE_DATA_INFO>m_stEncodeFrameInfo;
		vector<ENCODE_DATA_INFO>stArrEncodeInfo;
		int GetEncodeCount()
		{
			EnterCriticalSection(&m_criEncode);
			int nSize = stArrEncodeInfo.size();
			LeaveCriticalSection(&m_criEncode);
			return nSize;
		}
		ENCODE_DATA_INFO GetEncodeInfo()
		{
			EnterCriticalSection(&m_criEncode);
			ENCODE_DATA_INFO info = stArrEncodeInfo.at(0);
			LeaveCriticalSection(&m_criEncode);
			return info;
		}
		void DeleteEncodeInfo()
		{
			EnterCriticalSection(&m_criEncode);
			stArrEncodeInfo.erase(stArrEncodeInfo.begin());
			LeaveCriticalSection(&m_criEncode);
		}
		void SetEncodeInfo(ENCODE_DATA_INFO info)
		{
			EnterCriticalSection(&m_criEncode);
			stArrEncodeInfo.push_back(info);
			LeaveCriticalSection(&m_criEncode);
		}
		
		/*Inline func - Add Frame to RTSPServerMgr*/
		static unsigned WINAPI _ImageSendThread(LPVOID param);
		vector<MAKE_FRAME_INFO>m_stAddFrameInfo;
		int GetAddFrameCount()
		{
			int nCnt = 0;
			EnterCriticalSection(&m_criSyncMgr);
			nCnt = m_stAddFrameInfo.size();
			LeaveCriticalSection(&m_criSyncMgr);
			return nCnt;
		}
		MAKE_FRAME_INFO GetAddFrame()
		{
			MAKE_FRAME_INFO info;
			EnterCriticalSection(&m_criSyncMgr);
			info = m_stAddFrameInfo.at(0);
			LeaveCriticalSection(&m_criSyncMgr);
			return info;
		}
		void DeleteAddFrame()
		{
			EnterCriticalSection(&m_criSyncMgr);
			m_stAddFrameInfo.erase(m_stAddFrameInfo.begin());
			LeaveCriticalSection(&m_criSyncMgr);
		}
		void SetAddFrame(MAKE_FRAME_INFO info)
		{
			MAKE_FRAME_INFO AddFrameInfo;
			AddFrameInfo.Y.create(info.Y.rows,info.Y.cols,CV_8UC1);
			AddFrameInfo.U.create(info.U.rows,info.U.cols,CV_8UC1);
			AddFrameInfo.V.create(info.V.rows,info.V.cols,CV_8UC1);

			AddFrameInfo.HD_Y.create(info.HD_Y.rows,info.HD_Y.cols,CV_8UC1);
			AddFrameInfo.HD_U.create(info.HD_U.rows,info.HD_U.cols,CV_8UC1);
			AddFrameInfo.HD_V.create(info.HD_V.rows,info.HD_V.cols,CV_8UC1);
			
			memcpy(AddFrameInfo.Y.data,info.Y.data,info.Y.rows*info.Y.cols);
			memcpy(AddFrameInfo.U.data,info.U.data,info.U.rows*info.U.cols);
			memcpy(AddFrameInfo.V.data,info.V.data,info.V.rows*info.V.cols);

			memcpy(AddFrameInfo.HD_Y.data,info.HD_Y.data,info.HD_Y.rows*info.HD_Y.cols);
			memcpy(AddFrameInfo.HD_U.data,info.HD_U.data,info.HD_U.rows*info.HD_U.cols);
			memcpy(AddFrameInfo.HD_V.data,info.HD_V.data,info.HD_V.rows*info.HD_V.cols);

			AddFrameInfo.nSecIdx = info.nSecIdx;
			AddFrameInfo.nFrameIdx = info.nFrameIdx;
			// wslee - point for add MR information 
			memcpy(&AddFrameInfo.mr_info, &info.mr_info, sizeof(info.mr_info));
			////////////////////////////////////////
			EnterCriticalSection(&m_criSyncMgr);
			m_stAddFrameInfo.push_back(AddFrameInfo);
			LeaveCriticalSection(&m_criSyncMgr);
		}
		
		/*Sync Manage*/
		void SetSyncTime(int nTime)
		{
			m_nSyncTime = nTime;
		}
		int GetSyncTime(){return m_nSyncTime;}
		int GetClock();

		void SetStopIdx(int n)
		{
			EnterCriticalSection(&m_criStopTime);
			m_nStopIdx = n;
			LeaveCriticalSection(&m_criStopTime);

			CString strLog;
			strLog.Format(_T("[%s] Stop Time: %d"),m_str4DM,n);
			SendLog(5,strLog);
		}
		int GetStopIdx()
		{
			int n = 99999;
			EnterCriticalSection(&m_criStopTime);
			n = m_nStopIdx;
			LeaveCriticalSection(&m_criStopTime);

			return n;
		}

	private:
		/*Critical section*/
		CRITICAL_SECTION m_criPathMgr;
		CRITICAL_SECTION m_criDecode;
		CRITICAL_SECTION m_criSyncMgr;
		CRITICAL_SECTION m_criEncode;
		CRITICAL_SECTION m_criStopTime;
		CESMMovieRTMovie* m_pParent;
		CStopWatch* m_pClock;

		CString m_strSavePath;

		int m_nSyncTime;
		int m_nFrameRate;
		int m_nWidth;
		int m_nHeight;
		int m_nYSize;
		int m_nUVSize;
		int m_nYWidth;
		int m_nYHeight;
		int m_nUVWidth;
		int m_nUVHeight;
		int m_nYTop;
		int m_nYLeft;
		int m_nUVTop;
		int m_nUVLeft;

		BOOL m_bSetProperty;
		BOOL m_bLoadAdj;
		BOOL m_bPathMgrRun;
		BOOL m_bImageMgrRun;
		BOOL m_bEncodeMgrRun;
		Mat m_matYAdj;
		Mat m_matUVAdj;

		cv::Size m_szYRe;
		cv::Size m_szUVRe;

		//Logo Image
		cv::Mat m_matLogoImg;

		int m_nStopIdx;

		BOOL m_bUseRect;
	};
	int GetDSCIndex(){return m_nDSCIdx;}
	void SetSyncTime(CString str4DM,int nTime,int nESMTick);
	map<CString,CESMMovieRTRun*> m_mapRun;
	CESMMovieRTRun* GetESMMovieRun(CString str4DM)
	{
		CESMMovieRTRun* pRTRun = m_mapRun[str4DM];

		if(pRTRun == NULL)
		{
			pRTRun = new CESMMovieRTRun(this,str4DM,m_pClock);
			m_mapRun[str4DM] = pRTRun;

			//m_pRTSPServerMgr->SetRTSPStopTime(999999);
			SendLog(5,_T("[ESMRTMovie] - Create: ")+str4DM);
		}

		return pRTRun;
	}
	/*Inline func - Path*/
	void SetRTSendPath(CString strPath)
	{
		SendLog(5,_T("[SetRTSendPath]")+strPath);
		CString str4DMName = Get4DMNameFromPath(strPath);
		CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DMName);
		pRTRun->AddMoviePath(strPath);
		/*EnterCriticalSection(&m_criPathMgr);
		m_strArrPath.push_back(strPath);
		LeaveCriticalSection(&m_criPathMgr);*/
	}
	void DeleteRTRun(CString str4DM)
	{
		CESMMovieRTRun* pRTRun = m_mapRun[str4DM];
		if(pRTRun)
		{
			delete pRTRun;
			pRTRun = NULL;
		}
		m_mapRun.erase(str4DM);
		SendLog(5,_T("[ESMRTMovie]-Delete ")+str4DM);
	}
	void SetSavePath(CString strPath);

	//ESMMovieRTMovie.h
	void SetStopIdx(CString str4DM,int n);

	//wgkim 191210
	CESMMovieKZoneMgr m_pMovieKZoneMgr;
	void SetKZoneDataInfo(KZoneDataInfo* pKZoneDataInfo) { m_pMovieKZoneMgr.SetKZoneDataInfo(pKZoneDataInfo); };
	void SetKZoneDscCount(int nDscCount) { m_pMovieKZoneMgr.SetKZoneDscCount(nDscCount); };

	void SetKZoneValidTime() {m_nKZoneValidTime = 90;};	
	BOOL GetKZoneValidJudgment();

private:	
	int m_nKZoneValidTime;

	//skhong 191218
	ArapahoV2* m_pArapahoV2;
};

struct RTMovieConsoleData
{
	CESMMovieRTMovie* pParent;
	CString strPath;
	CString strSavePath;
	CString strSaveFileName;
	int nGPUIdx;
	int nSecIdx;	
	int nMakeFrame;
};