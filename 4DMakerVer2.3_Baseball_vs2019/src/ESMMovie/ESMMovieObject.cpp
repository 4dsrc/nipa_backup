#include "StdAfx.h"
#include "ESMMovieObject.h"

ESMMovieObject::ESMMovieObject()
{
	m_arrMovieData = NULL;
	m_nDscCount = 0;
}

ESMMovieObject::ESMMovieObject(CString strMovieName)
{
	m_arrMovieData = NULL;
	m_nDscCount = 0;
	m_strMovieName = strMovieName;
}


ESMMovieObject::~ESMMovieObject(void)
{
	ClearMovie();
}

void ESMMovieObject::ClearMovie()
{
	if( m_arrMovieData)
	{
		for( int i =0 ;i < m_nDscCount; i++)
		{
			if( m_arrMovieData[i] != NULL)
			{
				delete m_arrMovieData[i];
				m_arrMovieData[i] = NULL;
			}
		}
		delete m_arrMovieData;
	}
}

void ESMMovieObject::SetDscCount(int nDscCount)
{
	ClearMovie();
	m_nDscCount = nDscCount;
	m_arrMovieData = new ESMMovieData*[m_nDscCount];
	for( int i =0 ;i < m_nDscCount; i++)
	{
		m_arrMovieData[i] = new ESMMovieData;
	}
}

int ESMMovieObject::SetMovieFile(CString strMovieId, CString strMoviePath)
{
	int nMovieIndex = 0;
	for( int i =0 ;i < GetMovieCount(); i++)
	{
		int nFrameCount = 0, nWidth = 0, nHeight = 0, nChannel = 0;
		if( m_arrMovieData[i]->GetDscId() == _T("") || m_arrMovieData[i]->GetDscId() == strMovieId)
		{
			m_arrMovieData[i]->SetMoviePath(strMoviePath);
			m_arrMovieData[i]->SetDscId(strMovieId);

			nMovieIndex = i;
			break;
		}
	}
	return nMovieIndex;
}

CString ESMMovieObject::GetMovieFile(int nMovieIndex)
{
	CString strMoviePath;
	if( m_nDscCount <= nMovieIndex)
		return strMoviePath;

	int nFrameCount = 0, nWidth = 0, nHeight = 0, nChannel = 0;

	if( m_arrMovieData[nMovieIndex] != NULL)
	{
		strMoviePath = m_arrMovieData[nMovieIndex]->GetMoviePath();
	}
	return strMoviePath;
}


ESMMovieData* ESMMovieObject::GetMovieData(int nIndex)
{
	if( m_nDscCount <= nIndex)
		return NULL;

	return m_arrMovieData[nIndex];
}

ESMMovieData* ESMMovieObject::GetMovieData(CString strMovieID)
{
	for( int i =0 ;i < m_nDscCount; i++)
	{
		if( m_arrMovieData[i]->GetDscId() == strMovieID)
			return m_arrMovieData[i];
	}
	return NULL;
}

stAdjustInfo ESMMovieObject::GetAdjustData(int nIndex)
{
	stAdjustInfo AdjustData;
	if( m_nDscCount <= nIndex)
		return AdjustData;

	AdjustData = m_arrMovieData[nIndex]->GetAdjustinfo();
	return  AdjustData;
}

void ESMMovieObject::SetAdjustData(int nIndex, stAdjustInfo* AdjustData)
{
	if( m_nDscCount <= nIndex)
		return ;

	m_arrMovieData[nIndex]->SetAdjustinfo(AdjustData);
}

void ESMMovieObject::SetMovieInfo(ESMMovieInfoArray *pMovieInfoArray)
{
	m_nDscCount = pMovieInfoArray->GetMovieCount();
	SetDscCount(m_nDscCount);
	ESMMovieData* pMovieData = NULL;
	stMovieInfo MovieInfo;
	for(int i =0 ;i < m_nDscCount; i++)
	{
		CString strMovieID = pMovieInfoArray->GetMovieID(i);
		CString strMoviePath = pMovieInfoArray->GetMoviePath(strMovieID);
		SetMovieFile(strMovieID, strMoviePath);
		pMovieData = GetMovieData(strMovieID);
		MovieInfo = pMovieInfoArray->GetMoiveInfo(strMovieID);
		pMovieData->InitData(MovieInfo.nFrameCount , MovieInfo.nWidth, MovieInfo.nHeight, MovieInfo.nChannel);
		pMovieData->SetAdjustinfo(&(pMovieInfoArray->GetAdjustData(strMovieID)));
	}

}

CString ESMMovieObject::GetMoiveName()
{
	return m_strMovieName;
}