#include "stdafx.h"
#include "RTSPServerManager.h"
#include "ESMMovieFileOperation.h"

CRTSPServerMgr::CRTSPServerMgr()
{
	m_bThreadStop = TRUE;
	m_nDSCIdx = 0;
	m_nFrameRate = 30;
	m_bMovieState = FALSE;
	m_pWnd	= NULL;
}
CRTSPServerMgr::~CRTSPServerMgr()
{
	m_bThreadStop = FALSE;
	WSACleanup();
	closesocket(m_MasterSock);

	std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*>::iterator iter;

	for(iter = m_mpImageSender.begin() ; iter != m_mpImageSender.end() ; ++iter)
	{
		CRTSPServerImageSender* pSender = iter->second;

		if(pSender)
		{
			delete pSender;
			pSender = NULL;
		}
	}
}
BOOL CRTSPServerMgr::InitRTSPServer()
{
	sockaddr_in ServerAddr;                                   // server address parameters
	sockaddr_in ClientAddr;                                   // address parameters of a new RTSP client
	int         ClientAddrLen = sizeof(ClientAddr);  

	int ret = WSAStartup(0x101,&WsaData);

	if (ret != 0) 
		return 0;  

	ServerAddr.sin_family      = AF_INET;   
	ServerAddr.sin_addr.s_addr = INADDR_ANY;   
	ServerAddr.sin_port        = htons(8554);                 // listen on RTSP port 8554
	m_MasterSock               = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

	char name[255];
	CString strIP;
	if(gethostname(name,sizeof(name)) == 0)
	{
		PHOSTENT hostinfo = gethostbyname(name);
		if(hostinfo != NULL)
		{
			strIP = inet_ntoa(*(struct in_addr*)*hostinfo->h_addr_list);
			m_strIP = strIP;
		}
	}
	
	// bind our master socket to the RTSP port and listen for a client connection
	if (bind(m_MasterSock,(const sockaddr*)&ServerAddr,(int)sizeof(ServerAddr)) != 0) 
		return 0;  
	if (listen(m_MasterSock,5) != 0) 
		return 0;

	SetMulticastIPandPort();

	for(int i = PLAY_FHD ; i < PLAY_TOTAL - 1 ; i++)
	{
		RTSP_PLAY_METHOD method = RTSP_PLAY_METHOD(i);
		CRTSPServerImageSender* pImageSender = new CRTSPServerImageSender(method,strIP,8554,GetFrameRate());
		m_mpImageSender[method] = pImageSender;
		pImageSender->SetSyncStart(FALSE);
		pImageSender->SetDSCIdx(GetDSCIndex());

		if(pImageSender->GetSendReady() == FALSE)
		{
			while(pImageSender->GetSendReady() == TRUE)
				break;

			Sleep(1);
		}
		if(method == PLAY_FHD)
			LaunchMulticastThread(method);

		pImageSender->SetMainWnd(GetMainWnd());
		//pArrImageSender.push_back(pImageSender);
	}

	//Sleep(1000);

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,_LaunchConnectThread,(void*)this,0,NULL);
	CloseHandle(hSyncTime);

	CString str;
	str.Format(_T("[%d] - RTSP Server Open!!!!!!!!!"),GetDSCIndex());
	SendLog(5,str);

	return TRUE;
}
unsigned WINAPI CRTSPServerMgr::_LaunchConnectThread(LPVOID param)
{
	CRTSPServerMgr* pServerMgr = (CRTSPServerMgr*)param;
	SOCKET MasterSock = pServerMgr->m_MasterSock;
	SOCKET ClientSoc;
	sockaddr_in ClientAddr;
	int         ClientAddrLen = sizeof(ClientAddr);  
	while(pServerMgr->GetThreadStop())
	{
		ClientSoc = accept(MasterSock,(struct sockaddr*)&ClientAddr,&ClientAddrLen);

		RTSP_IMAGE_SENDER* stImageSend = new RTSP_IMAGE_SENDER;
		stImageSend->pServerMgr	= pServerMgr;
		stImageSend->mpArrImage = pServerMgr->m_mpImageSender;
		stImageSend->sock		= ClientSoc;
		stImageSend->socMaster	= MasterSock;
		stImageSend->strServerIP= pServerMgr->GetServerIP();

		CString strLog;
		strLog.Format(_T("client - [%d] input"),ClientSoc);
		SendLog(5,strLog);
		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE) _beginthreadex(NULL,0,pServerMgr->_LaunchClientThread,stImageSend,0,NULL);
		CloseHandle(hSyncTime);
	}

	return FALSE;
}
unsigned WINAPI CRTSPServerMgr::_LaunchClientThread(LPVOID param)
{
	RTSP_IMAGE_SENDER* stSend = (RTSP_IMAGE_SENDER*) param;
	CRTSPServerMgr* pServerMgr = stSend->pServerMgr;

	SOCKET soc = stSend->sock;
	SOCKET socMaster = stSend->socMaster;
	//std::vector<CRTSPServerImageSender*>pArrImageSender = stSend->pArrImageSender;
	std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*> mpArrImage = stSend->mpArrImage;
	CString strServerIP = stSend->strServerIP;
	delete stSend;stSend = NULL;

	CRTSPServerSessionMgr* pRTSPServerSession = new CRTSPServerSessionMgr(socMaster,soc,mpArrImage);
	pRTSPServerSession->SetMulticastIPandPort(pServerMgr->GetMulticastIP(),pServerMgr->GetMulticastPort());
	pRTSPServerSession->SetServerIP(strServerIP); 
	CRTSPImageStreamer* pRTSPStreamer = NULL;
	BOOL bStreamStart = FALSE;
	BOOL bStop = TRUE;

	CString strLog;
	strLog.Format(_T("[ClientThread] - %d "),soc);
	SendLog(5,strLog+_T("Open!"));
		
	char RecvBuf[2000];
	while(bStop)
	{
		memset(RecvBuf,0x00,2000);
		int res = recv(soc,RecvBuf,2000,0);

		if(res > 0)
		{
			SendLog(5,_T("[ClientThread]")+pRTSPServerSession->CharToCString(RecvBuf));

			if((RecvBuf[0] == 'O') || (RecvBuf[0] == 'D') || 
				(RecvBuf[0] == 'S') || (RecvBuf[0] == 'P') ||
				(RecvBuf[0] == 'T') )
			{
				RTSP_CMD_TYPE RTSPType = pRTSPServerSession->HandleRTSPRequests(RecvBuf,res);

				if(RTSPType == RTSP_PLAY)
				{
					if(pRTSPServerSession->GetMulticast() == FALSE)
						pRTSPStreamer = new CRTSPImageStreamer(soc,pRTSPServerSession,mpArrImage);

					bStreamStart = TRUE;
				}
				else if(RTSPType == RTSP_TEARDOWN)//Set Streamer Stop
				{
					bStop = FALSE;
				}
			}
		}
		else
		{
			bStop = FALSE;
		}
	}

	if(pRTSPStreamer)
	{
		pRTSPStreamer->SetThreadStop(FALSE);
		delete pRTSPStreamer;
		pRTSPStreamer = NULL;
	}

	if(pRTSPServerSession)
	{
		//pRTSPServerSession->SetThreadStop(FALSE);
		delete pRTSPServerSession;
		pRTSPServerSession = NULL;
	}
	SendLog(5,strLog+_T("Close!"));

	return FALSE;
}
void CRTSPServerMgr::SetLiveData(int nState,int nSecIdx,int nTotalFrame)
{
	m_mapLiveData[nSecIdx].nState		= nState;
	m_mapLiveData[nSecIdx].nSecIdx		= nSecIdx;
	m_mapLiveData[nSecIdx].nTotalFrame  = nTotalFrame;
	m_mapLiveData[nSecIdx].mArrImage	= new vector<Mat>(nTotalFrame);
}
void CRTSPServerMgr::ReceiveLiveData(int nState,int nSecIdx,int nTotalFrame,CString strPath,CString strFileName)
{
	m_mapLiveData[nSecIdx].nState			= nState;
	m_mapLiveData[nSecIdx].strPath		= strPath;
	m_mapLiveData[nSecIdx].strFileName = strFileName;
}
void CRTSPServerMgr::DecodingFileData(int nSecIdx)
{
	//stLiveData* pLiveData = &m_mapLiveData[nSecIdx];
	int nStart = GetTickCount();
	CString strPath = m_mapLiveData[nSecIdx].strPath;
	CT2CA pszConvertedAnsiString (strPath);
	std::string strDst(pszConvertedAnsiString);

	VideoCapture vc(strDst);
	Mat frame;

	int nCnt = 0;
	char str[100];
	Mat YUV;
	CString strTemp;
	while(1)
	{
		vc>>frame;
		if(frame.empty())
			break;

		sprintf(str,"[%S]%d_%d",m_strIP,nSecIdx,nCnt);

		putText(frame,str,cv::Point(50,100),
			cv::FONT_HERSHEY_PLAIN,5,cv::Scalar(0,0,0),3,8);

		std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*>::iterator iter;
		
		for(iter = m_mpImageSender.begin() ; iter != m_mpImageSender.end() ; ++iter)
		{
			CRTSPServerImageSender* pSender = iter->second;

			if(frame.cols != pSender->GetImageSize().width)
			{
				cv::cuda::GpuMat gpuImg,gpuResult;
				gpuImg.upload(frame);
				cv::cuda::resize(gpuImg,gpuResult,pSender->GetImageSize(),0,0,cv::INTER_CUBIC);
				gpuResult.download(frame);
				//resize(frame,frame,pSender->GetImageSize(),0,0,cv::INTER_CUBIC);
			}

			cvtColor(frame,YUV,cv::COLOR_BGR2YUV_IYUV);

			FRAME_PACKET_INFO packetInfo;
			packetInfo.img = YUV.clone();
			packetInfo.nSecIdx = nSecIdx;
			packetInfo.nFrameIdx = nCnt;

			pSender->InsertFrameImage(packetInfo);
		}
		nCnt++;
/*
		if(nCnt == 15)
			break;*/
	}

	m_mapLiveData[nSecIdx].nState = LIVE_INSERT_FINISH;
}
unsigned WINAPI CRTSPServerMgr::_LiveMgr(LPVOID param)
{
	CRTSPServerMgr* pRTSPMgr = (CRTSPServerMgr*)param;
	int nSecIdx = 0;

	while(pRTSPMgr->GetThreadStop())
	{
		if(pRTSPMgr->GetMovieState())//Open
		{
			if(pRTSPMgr->m_mapLiveData[nSecIdx].nState == LIVE_ADJ_FINISH)
			{
				//File Decoding
				pRTSPMgr->DecodingFileData(nSecIdx);
				CESMMovieFileOperation fo;
				//CString strPath = pRTSPMgr->m_mapLiveData[nSecIdx].strPath;
				//fo.Delete(strPath);
				CString strPath = pRTSPMgr->m_mapLiveData[nSecIdx].strPath;
				CString strFileName = pRTSPMgr->m_mapLiveData[nSecIdx].strFileName;
				CString strSavePath;
				strSavePath.Format(_T("%s\%s"),pRTSPMgr->Get4DMName(),strFileName);
				if(fo.IsFileExist(strPath))
				{
					fo.Copy(strPath,strSavePath,1);
					SendLog(5,strSavePath);
					fo.Delete(strPath);
				}
				nSecIdx++;
			}
		}
		else//Close
		{
			nSecIdx = 0;
		}
		Sleep(1);
	}
	SendLog(5,_T("RTSPServer Open..!"));
	return FALSE;
}
unsigned WINAPI CRTSPServerMgr::_SyncTimeMgr(LPVOID param)
{
	CRTSPServerMgr* pRTSPMgr = (CRTSPServerMgr*)param;
	std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*> mpImageSender = pRTSPMgr->m_mpImageSender;
	BOOL bSetSync = FALSE;
	CString strLog;
	int nTime = 0;

	while(pRTSPMgr->GetThreadStop())
	{
		Sleep(1);

		if(pRTSPMgr->GetMovieState())//Open
		{
			if(bSetSync == FALSE)
			{
				TRACE("bSync: %d\n",bSetSync);

				if(pRTSPMgr->GetClock() >= pRTSPMgr->GetSyncTime())
				{
					std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*>::iterator iter;

					for(iter = mpImageSender.begin() ; iter != mpImageSender.end() ; ++iter)
					{
						CRTSPServerImageSender* pSender = iter->second;
						pSender->SetSyncStart(TRUE);
						strLog.Format(_T("Movie Start - %d"),pSender->GetFrameImageSize());
						SendLog(5,strLog);
					}
					bSetSync = TRUE;
				}
			}
		}
		else//Close
		{
			std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*>::iterator iter;

			while(1)
			{
				int nCnt = 0;
				for(iter = mpImageSender.begin() ; iter != mpImageSender.end() ; ++iter)
				{
					CRTSPServerImageSender* pSender = iter->second;
					if(pSender->GetFrameImageSize() == 0)
					{
						pSender->SetSyncStart(FALSE);
						nCnt++;
					}
				}
				if(nCnt == mpImageSender.size())
					break;

				Sleep(1);
			}
			
			pRTSPMgr->SetESMTick(0);
			bSetSync = FALSE;
			nTime = 0;
		}
	}

	return FALSE;
}
int CRTSPServerMgr::GetClock()
{
	m_pClock->End();

	return (int)(m_pClock->GetDurationMilliSecond() * 10);
}
void CRTSPServerMgr::AddImage(Mat frameFHD,Mat frameXHD,int nSecIdx,int nFrameIdx)
{
	std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*>::iterator iter;

	for(iter = m_mpImageSender.begin() ; iter != m_mpImageSender.end() ; ++iter)
	{
		CRTSPServerImageSender* pSender = iter->second;
		Mat YUV;
		/*if(frame.cols != pSender->GetImageSize().width)
			resize(frame,frame,pSender->GetImageSize(),0,0,cv::INTER_CUBIC);*/
		if(nFrameIdx != 0)
		{
			if(pSender->GetImageSize().width == frameFHD.cols)
				cvtColor(frameFHD,YUV,cv::COLOR_BGR2YUV_IYUV);
			else if(pSender->GetImageSize().width == frameXHD.cols)
				cvtColor(frameXHD,YUV,cv::COLOR_BGR2YUV_IYUV);
		}
		else
		{
			YUV.create(pSender->GetImageSize(),CV_8UC3);
			YUV = cv::Scalar(255,255,255);
			cvtColor(YUV,YUV,cv::COLOR_BGR2YUV_IYUV);
		}
		

		FRAME_PACKET_INFO packetInfo;
		packetInfo.img = YUV.clone();
		packetInfo.nSecIdx = nSecIdx;
		packetInfo.nFrameIdx = nFrameIdx;

		pSender->InsertFrameImage(packetInfo);

		CString strLog;
		strLog.Format(_T("pSender[%d][%d] - %d"),nSecIdx,nFrameIdx,pSender->GetFrameImageSize());
		//SendLog(5,strLog);
	}
}
void CRTSPServerMgr::AddImage(MAKE_FRAME_INFO info,int nStopIdx)
{
	std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*>::iterator iter;

	for(iter = m_mpImageSender.begin() ; iter != m_mpImageSender.end() ; ++iter)
	{
		CRTSPServerImageSender* pSender = iter->second;

		Mat Y,U,V;

		//Change Resolution For MR
		//if(pSender->GetImageSize().width== info.Y.cols)
		if(pSender->GetImageSize().height == info.Y.rows)
		{
			Y = info.Y;
			U = info.U;
			V = info.V;
		}
		else
		{
			Y = info.HD_Y;
			U = info.HD_U;
			V = info.HD_V;
		}
		FRAME_PACKET_INFO packetInfo;
		packetInfo.Y = Y.clone();
		packetInfo.U = U.clone();
		packetInfo.V = V.clone();
		packetInfo.nSecIdx = info.nSecIdx;
		packetInfo.nFrameIdx = info.nFrameIdx;
		packetInfo.nDSCIdx = GetDSCIndex();
		packetInfo.str4DM.Format(_T("%s"),info.str4DM);
		packetInfo.nStopIdx = nStopIdx;//info.nStopIdx;
		// wslee - point for add MR information 
		memcpy(&packetInfo.mr_info, &info.mr_info, sizeof(info.mr_info));
		///////
		pSender->InsertFrameImage(packetInfo);

		CString strLog;
		strLog.Format(_T("pSender[%d][%d] - %d"),info.nSecIdx,info.nFrameIdx,pSender->GetFrameImageSize());
		//SendLog(5,strLog);

		Y.release();
		U.release();
		V.release();
	}
}
int CRTSPServerMgr::GetImageSize(RTSP_PLAY_METHOD method)
{
	int nSize = 0;
	CRTSPServerImageSender* pSender = m_mpImageSender[method];//m_mpImageSender[].GetFrameImageSize();
	nSize = pSender->GetFrameImageSize();

	return nSize;
}
CString CRTSPServerMgr::GetServerIP()
{
	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	CString strIP;
	PHOSTENT hostinfo;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 )
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				strIP = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}
		}      
		WSACleanup( );
	} 
	return strIP;
}
void CRTSPServerMgr::SetMulticastIPandPort()
{
#ifdef DEV_LGU
	int nBase = 60;
	m_strMulticastIP.Format(_T("239.253.5.%d"),m_nDSCIdx);
	m_nMulticastPort = 5000 + ((m_nDSCIdx-1) * 2);
#else
	// 사이트 별로 확인 필요....
	//m_strMulticastIP = _T("225.0.0.10");
	//m_nMulticastPort = m_nDSCIdx*2 + 5000;
	// MR Channel : ip : 239.222.222.222, port : 2000 + camera index
	m_strMulticastIP = _T("239.222.222.222");
	m_nMulticastPort = m_nDSCIdx + 2000;
#endif

	CString strLog;
	strLog.Format(_T("[Multicast] %s/%d"),m_strMulticastIP,m_nMulticastPort);
	SendLog(5,strLog);
}
CString CRTSPServerMgr::GetMulticastIP()
{
#if 1
	return m_strMulticastIP;
#else
	CString strIP = GetServerIP();

	CString strMulticastIP,strLastIP;
	int nFind = strIP.ReverseFind('.');
	int nCount = strIP.GetLength() - nFind;
	int nSize = strIP.GetLength();
	strLastIP = strIP.Mid(nFind,nCount);

	strMulticastIP.Format(_T("224.0%s.5"),strLastIP);
	//strMulticastIP.Format(_T("224.0.0%s"),strLastIP);

	return strMulticastIP;
#endif
}
void CRTSPServerMgr::LaunchMulticastThread(RTSP_PLAY_METHOD method)
{
	RTSP_MULTICAST_THREAD* pRTSPMutlticast = new RTSP_MULTICAST_THREAD;
	pRTSPMutlticast->pRTSPServerMgr = this;
	pRTSPMutlticast->pImageSender	= m_mpImageSender[method];
	pRTSPMutlticast->method			= method;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_MulticastThread,(void*)pRTSPMutlticast,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI CRTSPServerMgr::_MulticastThread(LPVOID param)
{
	RTSP_MULTICAST_THREAD* pRTSPMutlticast = (RTSP_MULTICAST_THREAD*)param;
	CRTSPServerMgr* pRTSPServer = pRTSPMutlticast->pRTSPServerMgr;
	CRTSPServerImageSender* pImageSender = pRTSPMutlticast->pImageSender;
	RTSP_PLAY_METHOD method = pRTSPMutlticast->method;
	delete pRTSPMutlticast;pRTSPMutlticast = NULL;

	SendLog(5,_T("[Multicast] Thread Init...."));
	//pRTSPServer->SetMulticastIP();
	CString strLog;
	strLog.Format(_T("[Multicast] Create Multicast IP: %s(%d)")
		,pRTSPServer->GetMulticastIP(),pRTSPServer->GetMulticastPort());

	//Create Multicast socket
	SOCKET RtpSocket = socket(AF_INET, SOCK_DGRAM, 0);

	if (RtpSocket < 0) 
	{
		CString strLog;
		strLog.Format(_T("[Multicast] Sock Error - %d"),(int)method);
		SendLog(5,strLog);

		return FALSE;
	}

	CString strMulticastIP = pRTSPServer->GetMulticastIP();
	char *servAddr = new char[strMulticastIP.GetLength()];
	sprintf(servAddr,"%S",strMulticastIP);

	int nPort;
#if 1
	nPort = pRTSPServer->GetMulticastPort();
#else
	if(method == PLAY_FHD)
		nPort = RTSP_MULTICAST_FHD_PORT;
	else if(method == PLAY_HD)
		nPort = RTSP_MULTICAST_XHD_PORT;
	else
	{
		CString strLog;
		strLog.Format(_T("[Multicast] Sock Error - %d"),(int)method);
		SendLog(5,strLog);

		return FALSE;
	}
#endif
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(servAddr);
	addr.sin_port = htons(nPort);

	int nMultiTTL = RTSP_MULTICAST_TTL;

	int nState = setsockopt(RtpSocket,IPPROTO_IP,IP_MULTICAST_TTL,(char*)&nMultiTTL,sizeof(nMultiTTL));
	if(nState == SOCKET_ERROR)
		SendLog(0,_T("[Multicast] setsockopt ERROR"));

	//Launch streamer
	CRTSPImageStreamer* pStreamer = new CRTSPImageStreamer(pImageSender,method);
	
	//Sleep(1000);

	SendLog(5,_T("[Multicast] Thread Start...."));

	int nStart = GetTickCount();
	while(pRTSPServer->GetThreadStop())
	{
		/*if(GetTickCount() - nStart >= 5000)
		{
			nStart = GetTickCount();
			CString strLog;
			strLog.Format(_T("[Multicast] [%d] Remain size - %d"),(int)method,pStreamer->GetMultiSendCount());
			SendLog(5,strLog);
		}*/
		if(pStreamer->GetMultiSendCount())
		{
			TRACE("[%d]SendCnt: %d\n",(int)method,pStreamer->GetMultiSendCount());

			RTSP_SEND_INFO info = pStreamer->GetMultiSendInfo();
			if(info.nDataLength && info.pData != NULL)
			{
#if 1
				int ret = sendto(RtpSocket,info.pData,info.nDataLength,0,(struct sockaddr*)&addr,sizeof(addr));
#else
				int ret = sendto(RtpSocket,info.pData+4,info.nDataLength - 4,0,(struct sockaddr*)&addr,sizeof(addr));
#endif
				if(ret != info.nDataLength)
				{
					CString strLog;
					strLog.Format(_T("SEND SIZE ERROR - Total: %d, Send: %d"),info.nDataLength,ret);
					SendLog(0,strLog);
				}
				if(info.pData)
				{
					delete info.pData;
					info.pData = NULL;
				}

			}
			pStreamer->DeleteMultiSendInfo();
		}
		else
			Sleep(1);
	}

	if(pStreamer)
	{
		pStreamer->SetThreadStop(FALSE);
		delete pStreamer;
		pStreamer = NULL;
	}

	closesocket(RtpSocket);
	SendLog(5,_T("MulticastThread Finish...."));
	return FALSE;
}
//RTSPServerManager.cpp
void CRTSPServerMgr::SetRTSPStopTime(int n)
{
   CRTSPServerImageSender* pSender = m_mpImageSender[PLAY_FHD];

   if(pSender)
   {
      pSender->SetStopTime(n);
   }
}