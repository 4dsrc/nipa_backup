#include "StdAfx.h"
#include "ESMMovieData.h"

ESMMovieData::ESMMovieData(void)
{
	m_nWidth = 0;
	m_nHeight = 0;
	m_nChannel= 0;
	m_nFrameCount = 0;
	m_pFrameData	= NULL;
	//m_pMemFrameData = NULL;
	m_nFrameState = 0;
	m_nLoadedFrameCount = 0;
}


ESMMovieData::~ESMMovieData(void)
{
	DeleteBuffer();
}

void ESMMovieData::DeleteBuffer()
{
	if( m_pFrameData != NULL)
	{
		for( int i =0 ;i < m_nFrameCount; i++)
		{
			if( m_pFrameData[i] != NULL)
			{
				if(m_pFrameData[i]->m_pCvImageBuffer != NULL)
					delete m_pFrameData[i]->m_pCvImageBuffer;

				delete m_pFrameData[i];
				m_pFrameData[i] = NULL;
			}
		}
	}
}

void ESMMovieData::DeleteImage(int nFramePos)
{
	if( m_pFrameData[nFramePos] != NULL)
	{
		if(m_pFrameData[nFramePos]->m_pCvImageBuffer != NULL)
			delete m_pFrameData[nFramePos]->m_pCvImageBuffer;

		delete m_pFrameData[nFramePos];
		m_pFrameData[nFramePos] = NULL;
		m_nLoadedFrameCount--;
	}
}

void ESMMovieData::InitData(int nFrameCount, int nWidth, int nHeight, int nChannel)
{
	DeleteBuffer();
	m_pFrameData = new CudaFrameData*[nFrameCount];
	for( int i =0 ;i< nFrameCount; i++)
	{
		m_pFrameData[i] = new CudaFrameData;
		m_pFrameData[i]->m_pCvImageBuffer = NULL;
	}
	m_nWidth = nWidth;
	m_nHeight = nHeight;
	m_nChannel = nChannel;
	m_nFrameCount = nFrameCount;
}

BOOL ESMMovieData::SetImage(int nIndex,  BYTE* pImage)
{
	if( pImage == NULL)
		return FALSE;

	if( nIndex >= m_nFrameCount)
		return FALSE;

	while(1)
	{
		if ( m_nLoadedFrameCount < IMAGELOAD_LIMIT_COUNT )
		{
			break;
		}
		Sleep(1);
	}

	CudaFrameData* pFrameData = m_pFrameData[nIndex];
	if( pFrameData->m_pCvImageBuffer != NULL)
	{
		delete pFrameData->m_pCvImageBuffer;
		pFrameData->m_pCvImageBuffer = NULL;
	}

	Mat src(m_nHeight, m_nWidth, CV_8UC3);
	memcpy(src.data, pImage, m_nWidth * m_nHeight * m_nChannel);
	pFrameData->m_pCvImageBuffer = new cuda::GpuMat;
	pFrameData->m_pCvImageBuffer->upload(src);
	m_nLoadedFrameCount++;
	return	TRUE;
}

BOOL ESMMovieData::SetImage(int nIndex,  Mat src)
{
	if( nIndex >= m_nFrameCount)
		return FALSE;

	CudaFrameData* pFrameData = m_pFrameData[nIndex];
	if( pFrameData->m_pCvImageBuffer != NULL)
	{
		delete pFrameData->m_pCvImageBuffer;
		pFrameData->m_pCvImageBuffer = NULL;
	}

	pFrameData->m_pCvImageBuffer = new cuda::GpuMat;
	pFrameData->m_pCvImageBuffer->upload(src);
	return	TRUE;
}

BOOL ESMMovieData::SetImage(int nIndex,  cuda::GpuMat* src)
{
	if( nIndex >= m_nFrameCount)
		return FALSE;

	CudaFrameData* pFrameData = m_pFrameData[nIndex];
	if( pFrameData->m_pCvImageBuffer != NULL)
	{
		pFrameData->m_pCvImageBuffer->release();
		delete pFrameData->m_pCvImageBuffer;
		pFrameData->m_pCvImageBuffer = NULL;
	}

	pFrameData->m_pCvImageBuffer = src;
	return	TRUE;
}

void ESMMovieData::GetImage(Mat* srcImage, int nIndex)
 {
	 if( nIndex >= m_nFrameCount)
		 return ;

	 if( m_pFrameData[nIndex]->m_pCvImageBuffer != NULL)
 		m_pFrameData[nIndex]->m_pCvImageBuffer->download(*srcImage);

 }

cuda::GpuMat* ESMMovieData::GetImage(int nIndex)
{
	cuda::GpuMat* pGpuMat = NULL;

	if( nIndex >= m_nFrameCount)
		return NULL;

	if( m_pFrameData[nIndex]->m_pCvImageBuffer != NULL)
		pGpuMat = m_pFrameData[nIndex]->m_pCvImageBuffer;

	return pGpuMat;
}

int ESMMovieData::GetFrameState(int nFrame) 
{ 
	CudaFrameData* pFrameData = m_pFrameData[nFrame];

	int nRet = -1;
	if( pFrameData != NULL)
		nRet = pFrameData->m_nProcessOrder;

	return nRet;
}

int ESMMovieData::SetFrameState(int nFrame, int nState) 
{ 
	CudaFrameData* pFrameData = m_pFrameData[nFrame];

	int nRet = -1;
	if( pFrameData != NULL)
		pFrameData->m_nProcessOrder = nState;

	return nRet;
}

void ESMMovieData::OrderAdd(int nFrame)
{
	CudaFrameData* pFrameData = m_pFrameData[nFrame];

	int nRet = -1;
	if( pFrameData != NULL)
		pFrameData->m_nProcessOrder++;
}

int ESMMovieData::GetSizeWidth()
{
	return m_nWidth;
}

int ESMMovieData::GetSizeHight()
{
	return m_nHeight;
}

stAdjustInfo ESMMovieData::GetAdjustinfo() 
{
	return m_AdjustData;
}

void ESMMovieData::SetAdjustinfo(stAdjustInfo* AdjustData)
{
	m_AdjustData.AdjAngle = AdjustData->AdjAngle; 
	m_AdjustData.AdjSize= AdjustData->AdjSize; 
	m_AdjustData.AdjMove.x = AdjustData->AdjMove.x; 
	m_AdjustData.AdjMove.y = AdjustData->AdjMove.y; 
	m_AdjustData.AdjptRotate.x = AdjustData->AdjptRotate.x; 
	m_AdjustData.AdjptRotate.y = AdjustData->AdjptRotate.y; 
	m_AdjustData.strDSC = AdjustData->strDSC; 
} 


void ESMMovieData::SetMoviePath(CString strMoviePath) 
{ 
	m_strMoviePath = strMoviePath;
}

CString ESMMovieData::GetMoviePath()
{ 
	return m_strMoviePath; 
}

void ESMMovieData::SetDscId(CString strMovieId)
{
	m_strMovieID = strMovieId;
}

CString ESMMovieData::GetDscId()
{
	return m_strMovieID;
}