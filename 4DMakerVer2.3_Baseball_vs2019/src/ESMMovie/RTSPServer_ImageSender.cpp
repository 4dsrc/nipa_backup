#include "stdafx.h"
#include "RTSPServer_ImageSender.h"

CRTSPServerImageSender::CRTSPServerImageSender(RTSP_PLAY_METHOD method,CString strIP,int nPort,int nFrameRate/* = 30*/)
{
	m_nSequenceNum = 0;
	m_nTimeStamp = 0;

	m_nFrameCount = 0;
	m_nPacketLoc = 0;
	m_nLastPacket = 0;
	m_nCurPacket = 0;
	m_strIP = strIP;
	m_nPort = nPort;
	m_RTSPPlayMethod = method;
	m_bThreadStop = TRUE;
	m_bSetEncode = FALSE;
	m_pArrPacketizeInfo = new RTSP_PACKETIZE_INFO[RTSP_PACKET_MAX];
	SetEncodeParameter(nFrameRate);
	InitializeCriticalSection(&m_criImage);
	m_bSyncStart = FALSE;
	//pFileTest = fopen("M:\\1.mp4","wb");
	m_nDSCIndex = 0;
	m_bReady = FALSE;

	m_nErrorCode = 0;
	m_nDisk = 0;
	m_nSendFrameCount = 0;
	m_nSendRepeatCount = 0;
	m_nSendFinishIdx = 999999;
	//bLoad = TRUE;

	//int ret = CT_Initialize("SCR-L-001","192.168.0.34",7412,"M:\\",&m_nErrorCode);
	//if(ret != 0)
	//{
	//	bLoad = FALSE;
	//	
	//}
	PCFreq			= 0.0;
	CounterStart	= 0;
	ProcCounterStart = 0;
	ProcPCFreq = 0.0;
	m_pWnd = NULL;

	StartCounter();

	InitializeCriticalSection(&m_criStopTime);
}
CRTSPServerImageSender::~CRTSPServerImageSender()
{
	if(m_pArrPacketizeInfo)
	{
		delete m_pArrPacketizeInfo;
		m_pArrPacketizeInfo = NULL;
	}
	DeleteCriticalSection(&m_criImage);
	/*if(pFileTest)
	{
		uint8_t endcode[] = { 0, 0, 1, 0xb7 };
		fwrite(endcode,1,sizeof(endcode),pFileTest);
		fclose(pFileTest);
	}*/
	DeleteCriticalSection(&m_criStopTime);
}
void CRTSPServerImageSender::SetEncodeParameter(int nFrameRate)
{
	BOOL b = TRUE;
	m_nFrameRate = nFrameRate;
	m_nGOP = ENCODE_FRAME_GOP;m_nPayloadSize = ENCODE_FRAME_PAYLOAD;

	switch(m_RTSPPlayMethod)
	{
	case PLAY_FHD:
		{
		//Change Resolution For MR
			m_nWidth = ENCODE_MR_WIDTH;
			m_nHeight = ENCODE_MR_HEIGHT;
			m_nBitrate = ENCODE_MR_BITRATE;
		}
		break;
	case PLAY_HD:
		{
			m_nWidth = ENCODE_HD_WIDTH;
			m_nHeight = ENCODE_HD_HEIGHT;
			m_nBitrate = ENCODE_HD_BITRATE;
		}
		break;
	case PLAY_NONE:
	default:
		{
			b = FALSE;
		}
		break;
	}
	
	if(b)
	{
		//Init Encode
		InitEncode(AV_CODEC_ID_H264);

		//Create SDP Info
		CreateSDPInfo();
		//Launch FrameSender
		LaunchFrameSender();
	}
}
void CRTSPServerImageSender::InitEncode(int codec_id)
{
	printf("Init FHD Encoder..\n");
	if(m_bSetEncode){return;}

	//Some inits
	int ret;
	m_sws_flags = SWS_BICUBIC;

	//You must call these subroutines otherwise you get errors!!
	avcodec_register_all();
	av_register_all();
#if 0
	AVCodec *pCodec;
	AVOutputFormat* pOutputFormat;

	avformat_alloc_output_context2(&m_pFormatCtx, NULL, "mpegts", NULL);	
	if(!m_pFormatCtx) 
	{
		return;
	}

	pOutputFormat = m_pFormatCtx->oformat;
	if(!m_pFormatCtx)
	{
		return;
	}

	pOutputFormat->video_codec = AV_CODEC_ID_H264;	
	if(pOutputFormat->video_codec == AV_CODEC_ID_NONE) 
		return;
	
	pCodec = avcodec_find_encoder(pOutputFormat->video_codec);
	if(!pCodec) 
		return;

	m_video_st = avformat_new_stream(m_pFormatCtx, pCodec);
	if(!m_video_st) 
		return ;

	m_video_st->id = m_pFormatCtx->nb_streams - 1;
	m_video_st->sample_aspect_ratio.den = 1;
	m_video_st->sample_aspect_ratio.num= 1;
	m_video_st->display_aspect_ratio.den = 16;
	m_video_st->display_aspect_ratio.num = 9;

	m_c = m_video_st->codec;
	if (!m_c) 
	{
		SendLog(0, _T("Could not allocate video pCodec context"));		
		return;
	}

    /* resolution must be a multiple of two */
    m_c->width = m_nWidth;
    m_c->height = m_nHeight;

	/* put sample parameters */
	//m_pCodecCtx->bit_rate = m_pCodecCtx->width * m_pCodecCtx->height * 40;
	m_c->codec_id = pOutputFormat->video_codec;
	int nBitrate = 74*1024*1024;

	m_c->bit_rate = nBitrate;			//Bits Per Second 
	m_c->rc_min_rate = nBitrate;// / (1024*1024) * 18;
	m_c->rc_max_rate = nBitrate;// / (1024*1024) * 23;	
	m_c->bit_rate_tolerance = nBitrate;
	m_c->rc_buffer_size = nBitrate;
	m_c->rc_initial_buffer_occupancy = m_c->rc_buffer_size * 3 / 4;
	m_c->rc_buffer_aggressivity = (float) 1.0;
	m_c->rc_initial_cplx = 0.5;

    /* frames per second */
    m_c->time_base.num = 1;
	m_c->time_base.den = m_nFrameRate;
	
	//hjcho GOP 1로 변경(170228)
    m_c->gop_size = m_nGOP; /* emit one intra frame every ten frames */
    m_c->max_b_frames=2;
    m_c->pix_fmt = AV_PIX_FMT_YUV420P;
	m_c->rtp_payload_size = ENCODE_FRAME_PAYLOAD;//m_nFrameRate;// 30000 This is the NAL unit size!
	m_c->thread_type = FF_THREAD_SLICE;
	m_c->thread_count = 16;
	//m_pCodecCtx->codec_type = AVMEDIA_TYPE_VIDEO;

	if(m_pFormatCtx->oformat->flags & AVFMT_GLOBALHEADER)
		m_c->flags |= CODEC_FLAG_GLOBAL_HEADER;

	if(codec_id == AV_CODEC_ID_H264)
		av_opt_set(m_c->priv_data, "preset", "ultrafast", 0);

    /* open it */
    if (avcodec_open2(m_c, pCodec, NULL) < 0) 
	{
		SendLog(0, _T("Could not open codec"));
        return;
    }
    m_frame = avcodec_alloc_frame();

    m_frame->format = m_c->pix_fmt;
    m_frame->width  = m_c->width;
    m_frame->height = m_c->height;
	
	if(!m_video_st)
	{
		return;
		/*avcodec_open2(m_c, pCodec, NULL);
		avio_open(&m_pFormatCtx->pb, filename, AVIO_FLAG_WRITE);
		avformat_write_header(m_pFormatCtx, NULL);*/
	}
    /* the image can be allocated by any means and av_image_alloc() is
     * just the most convenient way if av_malloc() is to be used */
    int nRet = av_image_alloc(m_frame->data, m_frame->linesize, m_c->width, m_c->height
		, m_c->pix_fmt, 32);


#else
	// Add the audio and video streams using the default format codecs
	// and initialize the codecs.
	m_video_st = NULL;

	//find the encoder
	m_video_codec = avcodec_find_encoder((AVCodecID)codec_id);  //avcodec_find_encoder(m_fmt->video_codec);
	if (!(m_video_codec)) {
		return;
	}

	m_c = avcodec_alloc_context3(m_video_codec);
	if (!(m_c)) 
	{
		return;
	}
	m_c->max_b_frames = 1;
	//Setup fundumental video stream parameters
	m_c->width = m_nWidth;			//Note Resolution must be a multiple of 2!!
	m_c->height = m_nHeight;			//Note Resolution must be a multiple of 2!!
	m_c->time_base.den = m_nFrameRate;	//Frames per second
	m_c->time_base.num = 1;
	m_c->gop_size = m_nGOP;		// Intra frames per x P frames

	m_c->pix_fmt       = AV_PIX_FMT_YUV420P;//Do not change this, H264 needs YUV format not RGB

	m_c->bit_rate = m_nBitrate;
	m_c->rc_min_rate = m_nBitrate;
	m_c->rc_max_rate = m_nBitrate;
	m_c->bit_rate_tolerance = m_nBitrate;
	m_c->rc_buffer_size = m_nBitrate;
	m_c->rc_initial_buffer_occupancy = m_c->rc_buffer_size - 1 / 4;
	m_c->rc_buffer_aggressivity = (float) 1.0;
	m_c->rc_initial_cplx = 0.5;
	
	if ((codec_id == AV_CODEC_ID_H264) || (codec_id == AV_CODEC_ID_H265))
	{
		av_opt_set(m_c->priv_data, "preset", "ultrafast", 0); //change between ultrafast,superfast, veryfast, faster, fast, medium, slow, slower, veryslow, placebo
		av_opt_set(m_c->priv_data, "profile", "baseline", 0);//baseline, main, high,high10,high422,high444
	}

	if(m_nWidth == 1920)
	{
		m_c->rtp_payload_size = ENCODE_FRAME_PAYLOAD*2;//m_nFrameRate;// 30000 This is the NAL unit size!
		av_opt_set_int(m_c->priv_data, "slice-max-size", ENCODE_FRAME_PAYLOAD*2, 0); //Replaces RTP Payload size
	}
	else
	{
		m_c->rtp_payload_size = ENCODE_FRAME_PAYLOAD/2;
		av_opt_set_int(m_c->priv_data, "slice-max-size", ENCODE_FRAME_PAYLOAD/2, 0); //Replaces RTP Payload size
	}

	//Open the codec
	ret = avcodec_open2(m_c, m_video_codec, NULL);
	if (ret < 0) 
	{
		return;
	}

	//allocate and init a re-usable frame
	m_frame = avcodec_alloc_frame();
	m_frame = av_frame_alloc();
	if (!m_frame) 
	{
		return;
	}

	//Actually use the frame
	m_frame->format = m_c->pix_fmt;
	m_frame->width = m_c->width;
	m_frame->height = m_c->height;

	ret = av_image_alloc(m_frame->data, m_frame->linesize, m_c->width, m_c->height,
		m_c->pix_fmt, 32);
	if (ret < 0) 
	{
		return;
	}
	//Set frame count to zero
	if (m_frame)
		m_frame->pts = 0;
#endif
	//Frame is initalised!
	m_bSetEncode = TRUE;

	printf("Init Encode Finish\n");
}
void CRTSPServerImageSender::LaunchFrameSender()
{
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,_LaunchFrameSender,(void*)this,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI CRTSPServerImageSender::_LaunchFrameSender(LPVOID param)
{
	//Sleep(1000);
	CRTSPServerImageSender* pImageSender = (CRTSPServerImageSender*) param;

	/*for(int i = 1 ; i < 10 ; i++)
	{
		char strPath[200];
		sprintf(strPath,"F:\\FHD\\300%02d.ts",i);
		VideoCapture vc(strPath);
		Mat frame,YUV;

		while(1)
		{
			vc>>frame;
			if(frame.empty())
				break;

			cvtColor(frame,YUV,cv::COLOR_BGR2YUV_I420);
			pImageSender->WriteFrame((unsigned char*)YUV.data,frame.cols,frame.rows,-1,-1,0);

			Sleep(10);
		}
	}*/

	Mat background = imread("C:\\Program Files\\ESMLab\\4DMaker\\img\\4DBack.png");
	if(background.empty())
	{
		background.create(pImageSender->m_nHeight,pImageSender->m_nWidth,CV_8UC3);
		background = Scalar(0x66,0xCC,0xFF);
		
		char str[100];
		sprintf(str,"BACKGROUND\r\nLoad\r\nError");
		
		putText(background,str,cv::Point(pImageSender->m_nWidth*2/3,pImageSender->m_nHeight/5),
			cv::FONT_HERSHEY_PLAIN,2,Scalar(0xFF,0,0),2,8);
	}
	else
	{
		if(background.cols != pImageSender->m_nWidth)
		{
			resize(background,background,cv::Size(pImageSender->m_nWidth,pImageSender->m_nHeight)
					,0,0,cv::INTER_LANCZOS4);
		}
	}
	int nWidth = background.cols;
	int nHeight = background.rows;

	Mat YUV(nHeight*1.5,nWidth,CV_8UC1);
	cvtColor(background,YUV,cv::COLOR_BGR2YUV_I420);

	SendLog(5,_T("Send Thread Start!"));
	/*namedWindow("YUV",CV_WINDOW_FREERATIO);
	imshow("YUV",YUV);
	namedWindow("back",CV_WINDOW_FREERATIO);
	imshow("back",background);
	
	waitKey(0);*/
	/*VideoCapture vc("C:\\Program Files\\ESMLab\\4DMaker\\img\\2.mp4");
	std::vector<Mat>arrFrame;
	while(1)
	{
		Mat frame;
		vc>>frame;
		if(frame.empty())
			break;
		resize(frame,frame,cv::Size(nWidth,nHeight),0,0,cv::INTER_LANCZOS4);
		cvtColor(frame,frame,cv::COLOR_BGR2YUV_I420);

		arrFrame.push_back(frame);
	}
	vc.release();

	int nFrameTotal = arrFrame.size();*/
	int nStart = GetTickCount();
	//int nFrameCnt[3];
	vector<double>nArrFrameCnt;
	int nFrameDelayCnt;
	int nFrameRate = pImageSender->m_nFrameRate;
	
	if(nFrameRate == 30)
	{
#if 1
		nArrFrameCnt.push_back(33.3667000333667);
#else
		nArrFrameCnt.push_back(33.3333);
		nArrFrameCnt.push_back(33.3333);
		nArrFrameCnt.push_back(33.3334);
#endif
	}
	else if(nFrameRate == 25)
		nArrFrameCnt.push_back(40.0);
	else//Default
	{
#if 1
		nArrFrameCnt.push_back(33.3667000333667);
#else
		nArrFrameCnt.push_back(33);
		nArrFrameCnt.push_back(33);
		nArrFrameCnt.push_back(34);
#endif
	}
	double dbDelayTime = 33.3667000333667;

	nFrameDelayCnt = nArrFrameCnt.size();

	int nCnt = 0;
	int nFrame = 0;

	RTSP_PLAY_METHOD method = pImageSender->m_RTSPPlayMethod;
	char strTemp[200];
	Mat tempY,tempU,tempV;

	double dbSysTimeStart = pImageSender->GetCounter();
	double dbProcTime = 0;
#ifdef DEV_LGU
	int nDSCIndex = 60 - pImageSender->GetDSCIdx();
#else
	int nDSCIndex = pImageSender->GetDSCIdx();
#endif

	while(pImageSender->GetThreadStop())
	{
		if(pImageSender->GetCounter() >= dbSysTimeStart + nArrFrameCnt.at(nCnt))
		{
			if(pImageSender->GetFrameImageSize())
			{
				FRAME_PACKET_INFO info = pImageSender->GetFrameImage();

				if(info.Y.data)
				{
#if 1
					if(info.nSecIdx <= info.nStopIdx)
#else
					if(info.nSecIdx <= pImageSender->GetStopTime())
#endif
					{
						/*sprintf(strTemp,"[%02d]-(%d)%d(%d)",
						nDSCIndex,
						info.nSecIdx,
						pImageSender->m_nSendFrameCount,
						pImageSender->m_nSendRepeatCount);
						putText(info.Y,strTemp,cv::Point(50,120),cv::FONT_HERSHEY_PLAIN,3,Scalar(255,255,0),3,8);*/

						//sprintf(strTemp,"%S",pImageSender->GetUTCTime());
						//putText(info.Y,strTemp,cv::Point(50,720),cv::FONT_HERSHEY_PLAIN,6,Scalar(255,255,0),3,8);

						//191209
						if (info.Y.cols!= ENCODE_MR_WIDTH)
							continue;

						pImageSender->WriteFrame((unsigned char*)info.Y.data,(unsigned char*)info.U.data,(unsigned char*)info.V.data,
							nWidth,nHeight,info.nSecIdx,info.nFrameIdx,pImageSender->GetDSCIdx(),info.mr_info);

						tempY = info.Y.clone();
						tempU = info.U.clone();
						tempV = info.V.clone();

						if(info.nFrameIdx == nFrameRate -1)
						{
							pImageSender->SendRTSPStateMessage(info);
							pImageSender->SendRTSPStateMessage(info,TRUE);
							//Send Info....
							//pImageSender->
						}
					}
					else if(tempY.empty())
					{
						pImageSender->WriteFrame((unsigned char*)YUV.data,nWidth,nHeight,-1,-1,pImageSender->GetDSCIdx());
					}
					else
					{
						pImageSender->WriteFrame((unsigned char*)tempY.data,(unsigned char*)tempU.data,(unsigned char*)tempV.data,
							nWidth,nHeight,-1,-1,pImageSender->GetDSCIdx(),info.mr_info);
					}

					pImageSender->DeleteFrameImage();
				}
				else if(tempY.empty())
				{
					pImageSender->WriteFrame((unsigned char*)YUV.data,nWidth,nHeight,-1,-1,pImageSender->GetDSCIdx());
				}
				else
				{
					pImageSender->WriteFrame((unsigned char*)tempY.data,(unsigned char*)tempU.data,(unsigned char*)tempV.data,
						nWidth,nHeight,-1,-1,pImageSender->GetDSCIdx(),info.mr_info);
				}
			}
			else if(tempY.empty())
			{
				pImageSender->WriteFrame((unsigned char*)YUV.data,nWidth,nHeight,-1,-1,pImageSender->GetDSCIdx());
			}
			else
			{
				pImageSender->WriteFrame((unsigned char*)tempY.data,(unsigned char*)tempU.data,(unsigned char*)tempV.data,
					nWidth,nHeight,-1,-1,pImageSender->GetDSCIdx());
			}

			dbSysTimeStart += nArrFrameCnt.at(nCnt);

			/*if(pImageSender->GetProcessCounter() < nArrFrameCnt.at(nCnt))
			dbProcTime = 0;
			else*/
			//dbProcTime = pImageSender->GetProcessCounter();

			nCnt++;

			if(nCnt >= nFrameDelayCnt)
				nCnt = 0;
		}
	}


//#if 1
//	char str[200];
//	while(pImageSender->GetThreadStop())
//	{
//		if(GetTickCount() >= nStart + nArrFrameCnt.at(nCnt))
//		{
//			if(pImageSender->GetFrameImageSize())//Send Image
//			{
//				FRAME_PACKET_INFO info = pImageSender->GetFrameImage();
//
//				if(info.nSecIdx <= pImageSender->GetStopTime())
//				{
//					//CString strLog;
//					//strLog.Format(_T("%d - %d"),info.nSecIdx,pImageSender->GetStopTime());
//					//SendLog(5,strLog);
//
//					if(info.Y.data)
//					{
//						//190320 hjcho LGU+ 임시
//						int nIdx = pImageSender->m_nDSCIndex;
//						if(nIdx != 60)
//							nIdx = 60 - pImageSender->m_nDSCIndex;
//
//						sprintf(str,"[%d]-%d(%d)",nIdx,pImageSender->m_nSendFrameCount,pImageSender->m_nSendRepeatCount);
//						putText(info.Y,str,cv::Point(160,90),cv::FONT_HERSHEY_PLAIN,5,cv::Scalar(255),3,8);
//
//						pImageSender->WriteFrame((unsigned char*)info.Y.data,(unsigned char*)info.U.data,(unsigned char*)info.V.data,
//							nWidth,nHeight,info.nSecIdx,info.nFrameIdx,pImageSender->GetDSCIdx());
//
//						tempY = info.Y.clone();
//						tempU = info.U.clone();
//						tempV = info.V.clone();
//					}
//					else
//						pImageSender->WriteFrame((unsigned char*)YUV.data,nWidth,nHeight,-1,-1,pImageSender->GetDSCIdx());
//				}
//				else
//				{
//					//CString strLog;
//					//strLog.Format(_T("[RTSPImageSender] - %d / %d"),info.nSecIdx,pImageSender->GetStopTime());
//					//SendLog(5,strLog);
//				}
//				pImageSender->DeleteFrameImage();
//			}
//			else if(!tempY.empty())//background
//			{
//				pImageSender->WriteFrame((unsigned char*)tempY.data,(unsigned char*)tempU.data,(unsigned char*)tempV.data,
//					nWidth,nHeight,-1,-1,pImageSender->GetDSCIdx());
//			}
//			else//Default Image
//			{
//				pImageSender->WriteFrame((unsigned char*)YUV.data,nWidth,nHeight,-1,-1,pImageSender->GetDSCIdx());
//			}
//
//			nStart += nArrFrameCnt.at(nCnt);
//
//			nCnt++;
//			if(nCnt >= nFrameDelayCnt)
//				nCnt = 0;
//
//			nFrame++;
//		}
//
//	}
//#else
//	while(pImageSender->GetThreadStop())
//	{
//#if 1
//		if(GetTickCount() >= nStart + nArrFrameCnt.at(nCnt)/ *nFrameCnt[nCnt]* /)
//		{
//			//SendLog(5,_T("AAA"));
//			
//#if 0			
//			if(/ *pImageSender->GetFrameImageSize() && * /pImageSender->GetSyncStart())//sync + ImageSize
//			{
//
//				pImageSender->WriteFrame((unsigned char*)arrFrame.at(nFrame).data,nWidth,nHeight,0,nFrame);
//#else
//#if 1
//			if(pImageSender->GetFrameImageSize() && pImageSender->GetSyncStart())//sync + ImageSize
//			{
//				//Mat frame = pImageSender->GetFrameImage().clone();
//				FRAME_PACKET_INFO info = pImageSender->GetFrameImage();
//				Mat Y= info.Y;
//				if(Y.data)
//				{
//					pImageSender->WriteFrame((unsigned char*)info.Y.data,(unsigned char*)info.U.data,(unsigned char*)info.V.data,
//					nWidth,nHeight,info.nSecIdx,info.nFrameIdx,pImageSender->GetDSCIdx());
//				
//					tempY = info.Y.clone();
//					tempU = info.U.clone();
//					tempV = info.V.clone();
//				}
//
//				pImageSender->DeleteFrameImage();
//#else
//			if(pImageSender->GetFrameImageSize() && pImageSender->GetSyncStart())//sync + ImageSize
//			{
//				//Mat frame = pImageSender->GetFrameImage().clone();
//				FRAME_PACKET_INFO info = pImageSender->GetFrameImage();
//				Mat frame = info.img.clone();
//				if(frame.data)
//					pImageSender->WriteFrame((unsigned char*)frame.data,nWidth,nHeight,info.nSecIdx,info.nFrameIdx);
//
//				pImageSender->DeleteFrameImage();
//#endif
//#endif
//			}
//			else if(!tempY.empty())//background
//			{
//				pImageSender->WriteFrame((unsigned char*)tempY.data,(unsigned char*)tempU.data,(unsigned char*)tempV.data,
//					nWidth,nHeight,info.nSecIdx,info.nFrameIdx,pImageSender->GetDSCIdx());
//			}
//			else
//			{
//				//if(pImageSender->GetSyncStart() == FALSE)
//				pImageSender->WriteFrame((unsigned char*)YUV.data,nWidth,nHeight,-1,-1,pImageSender->GetDSCIdx());
//			}
//			nStart += nArrFrameCnt.at(nCnt);
//			
//			/ *CString strLog;
//			strLog.Format(_T("[%d] %d"),nCnt,nStart);
//			SendLog(5,strLog);* /
//			
//			nCnt++;
//			if(nCnt >= nFrameDelayCnt)
//				nCnt = 0;
//
//			nFrame++;
//
//
//			/ *if(nFrame >= nFrameTotal)
//				nFrame = 0;* /
//			
//			//Sleep(1);
//		}
//		else
//			Sleep(1);//pImageSender->SetWait(1);
//#endif
//	}
//
//#endif
//
	return FALSE;
}
void CRTSPServerImageSender::CreateSDPInfo()
{
	CString strSDP;
#if 0
	av_sdp_create(m_c,)
#else
	strSDP.Format(_T("v=0\r\n"));
	strSDP.Append(_T("o=- 1544060676845802 1 IN IP4 192.168.0.34\r\n"));
	strSDP.Append(_T("s=4DReplayLive\r\n"));
	strSDP.Append(_T("t=0 0\r\n"));
	strSDP.Append(_T("a=tool:4DReplay\r\n"));
	strSDP.Append(_T("a=type:broadcast\r\n"));
	strSDP.Append(_T("a=control:*\r\n"));
	strSDP.Append(_T("a=range:npt=0-\r\n"));
	strSDP.Append(_T("m=video 0 RTP/AVP 96\r\n"));
	strSDP.Append(_T("c=IN IP 0.0.0.0\r\n"));
	CString strTemp;
	strTemp.Format(_T("b=AS:%d\r\n"),m_c->bit_rate/1024);

	strSDP.Append(strTemp);
	strSDP.Append(_T("a=rtpmap:96 H264/90000\r\n"));
	strSDP.Append(_T("a=fmtp=96 packetization-mode=1;\r\n"));
	strSDP.Append(_T("a=control:"));

	strTemp.Format(_T("rtsp://%s:%d/4DReplayLIVE?FHD/track0\r\n\r\n"),m_strIP,m_nPort);
	strSDP.Append(strTemp);
#endif
	SetSDP(strSDP);
}
void CRTSPServerImageSender::WriteFrame(unsigned char* RGBFrame,int nWidth,int nHeight,int nSecIdx,int nFrameIdx,int nDSCIndex)
{
	if(!m_bSetEncode)
		return;

	memcpy(m_frame->data[0],RGBFrame,nWidth*nHeight*1.5);
	WriteFrame(nSecIdx,nFrameIdx,nDSCIndex);
}
void CRTSPServerImageSender::WriteFrame(unsigned char* Y,unsigned char* U,unsigned char* V,
							int nWidth,int nHeight,int nSecIdx,int nFrameIdx,int nDSCIndex,MR_INFO mr_info/*=MR_INFO()*/)
{
	if(!m_bSetEncode)
		return;

	memcpy(m_frame->data[0],Y,nWidth*nHeight);
	memcpy(m_frame->data[1],U,nWidth*nHeight/4);
	memcpy(m_frame->data[2],V,nWidth*nHeight/4);

	WriteFrame(nSecIdx,nFrameIdx,nDSCIndex,mr_info);
}
void CRTSPServerImageSender::WriteFrame(int nSecIdx,int nFrameIdx,int nDSCIndex,MR_INFO mr_info/*=MR_INFO()*/)
{
	int nStart = GetTickCount();
	//Inits
	int ret;

	//struct SwsContext *sws_ctx = (struct SwsContext *)sws_ctx_void;

	//Some inits for encoding the frame
	int got_packet;
	av_init_packet(&pkt);
	pkt.data = NULL;    // packet data will be allocated by the encoder
	pkt.size = 0;

	//Encode the frame
	ret = avcodec_encode_video2(m_c, &pkt, m_frame, &got_packet);
	if (ret < 0) {
		char temp2[1024];
		av_strerror(ret,temp2,1024);
		//mMainDlg->mSystemLog->AddEntry( "FFMPEG::WriteFrame - Failed to encode the frame" );
		SendLog(5,_T("Encoding Error"));
		printf("ERROR\n");
		return;
	}
	
	//pkt.pos = m_frame_count;
	AVRational timebase_;
	timebase_.num = 1,timebase_.den = 90001;

	pkt.pts = av_rescale_q_rnd(pkt.pts,m_c->time_base,timebase_,AVRounding(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
	pkt.dts = av_rescale_q_rnd(pkt.dts,m_c->time_base,timebase_,AVRounding(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
	pkt.duration = av_rescale_q(pkt.duration,m_c->time_base,timebase_);

	//If size of encoded frame is zero, it means the image was buffered.
#if 0
	if (!ret && got_packet && pkt.size) 
	{
		unsigned char* pPacket = NULL;
		pPacket = new unsigned char[pkt.size];

		if(pPacket)
			memcpy(pPacket,pkt.data,pkt.size);

		BOOL bLoop = true;

		int nSize = pkt.size;
		int nStartIdx = 0;
		while(bLoop)
		{
			bLoop = FALSE;
			int NalStart = -1;

			for(int i = nStartIdx; i < nSize - 4 ; i++)
			{
				if((pPacket[i] == 0x00) && (pPacket[i+1] == 0x00) && (pPacket[i+2] == 0x01))
				{
					NalStart = i + 3;
					break;
				}
			}

			int NalEnd = -1;

			if(NalStart != -1)
			{
				for(int i = NalStart ; i < nSize - 6 ; i++)
				{
					if(((pPacket[i+1] == 0x00) && (pPacket[i+2] == 0x00) && (pPacket[i+3] == 0x01) )/*||
						((pPacket[i+1] == 0x00) && (pPacket[i+2] == 0x00) && (pPacket[i+3] == 0x00))*/ )
					{
						if((pPacket[i+1] == 0x00) && (pPacket[i+2] == 0x00) && (pPacket[i+3] == 0x01))
						{
							if((pPacket[i+4] == 0x65) && (pPacket[i+5] == 0x00))//25,45
							{
								continue;
							}
						}
						NalEnd = i+1;
						bLoop = TRUE;
						break;
					}
				}
			}

			if(bLoop == TRUE)
			{
				int nNalSize = NalEnd - NalStart;
				nStartIdx = NalEnd;
				if(GetSPS() == _T("") || GetPPS() == _T(""))
				{
					if(nNalSize == 20) 
					{
						//memcpy(m_cSPS,pPacket+NalStart,20);	
						SetSPS(_T("SPS"));
					}
					else if(nNalSize == 4)
					{
						//char strPPS[4];
						//memcpy(strPPS,pPacket+NalStart,4);
						//memcpy(m_cPPS,pPacket+NalStart,4);
						SetPPS(_T("PPS"));
					}
				}
				//nSize -= NalEnd;
				//memmove(&pPacket[0],&pPacket[NalEnd],nSize);
			}
			else
			{
				NalEnd = nSize;
				int nNalSize = NalEnd - NalStart;
				//AddFramePacket(pPacket,nNalSize,NalStart,NalEnd,0,0);
			}
		}
		av_packet_unref(&pkt);

		if(pPacket)
		{
			memset(pPacket,0x00,pkt.size);
			delete pPacket;
			pPacket = NULL;
		}
	} 
	else 
	{
		ret = 0;
	}
	m_frame->pts = m_nFrameCount++;
#else
	if (!ret && got_packet && pkt.size) 
	{
		if(GetSendReady() == FALSE)
		{
			SetSendReady(TRUE);
		}
		unsigned char* pPacket = NULL;
		pPacket = new unsigned char[pkt.size];

		if(pPacket)
			memcpy(pPacket,pkt.data,pkt.size);

		BOOL bLoop = true;

		int nSize = pkt.size;
		int nStartIdx = 0;
		while(bLoop)
		{
			bLoop = FALSE;
			int NalStart = -1;

			for(int i = nStartIdx; i < nSize - 4 ; i++)
			{
				if((pPacket[i] == 0x00) && (pPacket[i+1] == 0x00) && (pPacket[i+2] == 0x01))
				{
					NalStart = i + 3;
					break;
				}
			}

			int NalEnd = -1;

			if(NalStart != -1)
			{
				for(int i = NalStart ; i < nSize - 6 ; i++)
				{
					if(((pPacket[i+1] == 0x00) && (pPacket[i+2] == 0x00) && (pPacket[i+3] == 0x01)) ||
						((pPacket[i+1] == 0x00) && (pPacket[i+2] == 0x00) && (pPacket[i+3] == 0x00)) )
					{
						if((pPacket[i+1] == 0x00) && (pPacket[i+2] == 0x00) && (pPacket[i+3] == 0x01))
						{
							if((pPacket[i+4] == 0x65) && (pPacket[i+5] == 0x00))//25,45
							{
								continue;
							}
						}
						NalEnd = i+1;
						bLoop = TRUE;
						break;
					}
				}
			}

			if(bLoop == TRUE)
			{
				int nNalSize = NalEnd - NalStart;
				nStartIdx = NalEnd;
				//AddPacketArray(pPacket,NalStart,NalEnd);
				if(nNalSize < 100)
					PacketizeSubData(pPacket,NalStart,NalEnd,nSecIdx,nFrameIdx,nDSCIndex,mr_info);
			}
			else
			{
				NalEnd = nSize;
				int nNalSize = NalEnd - NalStart;
				PacketizeEncodeFrame(pPacket,NalStart,NalEnd,nSecIdx,nFrameIdx,nDSCIndex,mr_info);
				//AddFramePacket(pPacket,nNalSize,NalStart,NalEnd,0,0);
			}
		}
		//if (got_packet)
		//{
		//	fwrite(pkt.data, 1, pkt.size, pFileTest);
		//	//av_free_packet(&pkt);
		//}

		av_packet_unref(&pkt);

		if(pPacket)
		{
			memset(pPacket,0x00,pkt.size);
			delete pPacket;
			pPacket = NULL;
		}
	} 
	else 
	{
		/*ret = 0;
		CString strLog;
		strLog.Format(_T("[%d][%d] Error"),nSecIdx,nFrameIdx);
		SendLog(5,strLog);*/
	}
	m_frame->pts = m_nFrameCount++;
	
	/*CString strLog;
	strLog.Format(_T("Process - %d"),GetTickCount()-nStart);
	SendLog(5,strLog);*/

#endif

	/*if(pPacket)
	{
	delete pPacket;
	pPacket = NULL;
	}*/
	//Increment Frame counter
	//m_nFrameCount++;
}
void CRTSPServerImageSender::AddPacketArray(unsigned char* pPacket,int nStart,int nEnd,int nSecIdx,int nFrameIdx,BOOL bEnd /*= FALSE*/)
{
	int nSize = nEnd - nStart;
	m_pArrPacketizeInfo[m_nPacketLoc].nSize = nSize;
	m_pArrPacketizeInfo[m_nPacketLoc].nSecIdx = nSecIdx;
	m_pArrPacketizeInfo[m_nPacketLoc].nFrameIdx = nFrameIdx;
	memset(m_pArrPacketizeInfo[m_nPacketLoc].ArrData,0x00,1500);
	memcpy(m_pArrPacketizeInfo[m_nPacketLoc].ArrData,pPacket+nStart,nSize);
	m_pArrPacketizeInfo[m_nPacketLoc].bTail = bEnd;

	if(bEnd)
	{
		SetLastIdx(m_nCurPacket % RTSP_PACKET_MAX);
		SetCurIdx(m_nCurPacket = (m_nPacketLoc % RTSP_PACKET_MAX));
		//m_nLastPacket = (m_nCurPacket % RTSP_PACKET_MAX);
		//m_nCurPacket = (m_nPacketLoc % RTSP_PACKET_MAX);
	}
	m_nPacketLoc = (++m_nPacketLoc % RTSP_PACKET_MAX);
}
void CRTSPServerImageSender::PacketizeEncodeFrame(unsigned char* pPacket,int nStart,int nEnd,
				int nSecIdx,int nFrameIdx,int nDSCIndex,MR_INFO mr_info,BOOL bEnd /*= FALSE*/)
{
	int nSize = nEnd - nStart;
	unsigned char* pData = new unsigned char[nSize];
	memset(pData,0x00,nSize);

	memcpy(pData,pPacket+nStart,nSize);

	int nTickStart = GetTickCount();

	int firstPacket = 0;
	unsigned char xbuf[2000];
	//unsigned char* buf = pak.pPacket;

	//printf("nTotalSize= %d\n",nTotalSize);
	BOOL bFirst = TRUE;
	int nCurIdx = 1;
	int nAddSize = 16;

	int nTotalSize = nSize - 1;
	int i = 1;
	unsigned nalType = pData[0]&0x1f;
	unsigned char* x;

	char fu_a_startbit = 0x80;
	char fu_a_endbit = 0x40;
	char fu_a = 28;

	//pData[nSize - 1] = '!';

#if 1
	int nRTPHeaderSize = 12 + 2;//RTP Header + NAL Header
	int nTCPHeaderSize = 4;
	int nHeaderSize = 0;
	//SendLog(5,_T("##############################"));
	
	while(nTotalSize)
	{
		memset(xbuf,0x00,2000);

#if 1
		int nSendByte = 0;
		int nHeaderSize = 0;
		if (bFirst) {
			nHeaderSize = MakeRTPHeader(xbuf);
			nHeaderSize = MakeRTPExtendHeader(xbuf, nHeaderSize, nSecIdx, nFrameIdx, nDSCIndex, mr_info);
		}
		else {
			nHeaderSize = MakeRTPHeader(xbuf, FALSE, TRUE);
		}
		xbuf[nHeaderSize++] = H264_FU_A_INDICATOR;

		if(nTotalSize > MTU_FRAME_SIZE)
		{
			if(bFirst)
			{
				bFirst = FALSE;
				xbuf[nHeaderSize++] = H264_FU_A_START_BIT;
			}
			else
			{
				xbuf[nHeaderSize++] = H264_FU_A_MID_BIT;
			}
			memcpy(xbuf+nHeaderSize,pData+nCurIdx,MTU_FRAME_SIZE);
			//MemoryTest(xbuf,nSendByte,nHeaderSize,pData,nCurIdx);
			nTotalSize -= MTU_FRAME_SIZE;
			nCurIdx += MTU_FRAME_SIZE;
			nSendByte = MTU_FRAME_SIZE + nHeaderSize;

			AddPacketArray(xbuf,0,nSendByte,nSecIdx,nFrameIdx);
		}
		else
		{
			xbuf[1] = RTSP_HEADER_ME_PT;
			xbuf[nHeaderSize++] = H264_FU_A_END_BIT;
			//nTotalSize = nSize - nCurIdx;

			memcpy(xbuf+nHeaderSize,pData+nCurIdx,nTotalSize);
			//MemoryTest(xbuf,nSendByte,nHeaderSize,pData,nCurIdx);
			nSendByte = nTotalSize + nHeaderSize;
			nCurIdx += nTotalSize;
			nTotalSize = 0;

			AddPacketArray(xbuf,0,nSendByte,nSecIdx,nFrameIdx,TRUE);
		}
		CString strLog;
		strLog.Format(_T("[%d][%d]	%d	%d	%d	%d"),nSecIdx,nFrameIdx,nCurIdx,nTotalSize,nSendByte,nSize);
		//SendLog(5,strLog);
#else
		if(bFirst)// 첫 프레임
		{
			bFirst = FALSE;

			int nHeaderSize = MakeRTPHeader(xbuf,'-',MTU_FRAME_SIZE,FUA_START,nSecIdx,nFrameIdx,nDSCIndex);

			memcpy(xbuf+nHeaderSize,pData+nCurIdx,MTU_FRAME_SIZE-(nHeaderSize-nTCPHeaderSize));
			AddPacketArray(xbuf,0,MTU_FRAME_SIZE+nTCPHeaderSize,nSecIdx,nFrameIdx);

			nTotalSize -= (MTU_FRAME_SIZE-(nHeaderSize-nTCPHeaderSize));
			nCurIdx += (MTU_FRAME_SIZE-(nHeaderSize-nTCPHeaderSize));
			nRTPHeaderSize = nHeaderSize /*- nTCPHeaderSize*/;
		}	
		else if(nTotalSize + nRTPHeaderSize - nTCPHeaderSize/*+ 14*/ <= MTU_FRAME_SIZE)//마지막 프레임
		{
			int nSendByte = nTotalSize + nRTPHeaderSize;
			int nHeaderSize = MakeRTPHeader(xbuf,'-',nSendByte+nRTPHeaderSize,FUA_END,nSecIdx,nFrameIdx,nDSCIndex);

			CString strLog;
			strLog.Format(_T("[%d][%d][END] - %d/%d(%d) - %d"),nSecIdx,nFrameIdx,nTotalSize,nCurIdx,nSize,nSendByte);
			SendLog(5,strLog);

			memcpy(xbuf+nHeaderSize,pData+nCurIdx,nSendByte);
			AddPacketArray(xbuf,0,nSendByte+nTCPHeaderSize+nRTPHeaderSize,nSecIdx,nFrameIdx,TRUE);

			nCurIdx += nSendByte;
			nTotalSize -= nTotalSize;
		}
		else//중간 프레임
		{
			int nHeaderSize = MakeRTPHeader(xbuf,'-',MTU_FRAME_SIZE,FUA_MID,nSecIdx,nFrameIdx,nDSCIndex);

			memcpy(xbuf+nHeaderSize,pData+nCurIdx,MTU_FRAME_SIZE-(nHeaderSize-nTCPHeaderSize));
			AddPacketArray(xbuf,0,MTU_FRAME_SIZE+nTCPHeaderSize,nSecIdx,nFrameIdx);

			nTotalSize -= (MTU_FRAME_SIZE-(nHeaderSize-nTCPHeaderSize));
			nCurIdx += (MTU_FRAME_SIZE-(nHeaderSize-nTCPHeaderSize));
			nRTPHeaderSize = nHeaderSize /*- nTCPHeaderSize*/;
		}
	
#endif
	}
#endif

	m_nTimeStamp += (90000/m_nFrameRate);

	if(nSecIdx >= 0 && nFrameIdx >= 0)
	{
		m_nSendFrameCount++;

		if(m_nSendFrameCount == 65536)
		{
			m_nSendFrameCount = 0;
			m_nSendRepeatCount++;
		}

		if(m_nSendRepeatCount == 256)
			m_nSendRepeatCount = 0;
	}
	if(pData)
	{
		delete pData;
		pData = NULL;
	}
	//TRACE("Proc = %d\n",GetTickCount()-nStart);
}
int CRTSPServerImageSender::MakeRTPHeader(unsigned char* xbuf,char naltype,int nSize,int nMakeMethod,int nSecIdx,int nFrameIdx,int nDSCIndex)
{
	//TCP Header
	int nIdx = 0;
	xbuf[nIdx++] = TCP_HEADER_RTSP;//TCP_HEADER_RTSP;// '$'
	xbuf[nIdx++] = 0x00;
	xbuf[nIdx++] = (nSize & 0xFF00) >> 8;
	xbuf[nIdx++] = (nSize & 0xFF);

	//RtpHeader - 12byte 
	xbuf[nIdx++] = RTSP_HEADER_VERSIONEX; // RTSP Version

	if(nMakeMethod == FUA_END)
		xbuf[nIdx++] = RTSP_HEADER_ME_PT;
	else
		xbuf[nIdx++] = RTSP_HEADER_M_PT;

	xbuf[nIdx++] = (m_nSequenceNum & 0xFF00) >> 8;
	xbuf[nIdx++] = (m_nSequenceNum & 0x00FF);

	xbuf[nIdx++]  = (m_nTimeStamp & 0xFF000000) >> 24;
	xbuf[nIdx++]  = (m_nTimeStamp & 0x00FF0000) >> 16;
	xbuf[nIdx++] = (m_nTimeStamp & 0x0000FF00) >> 8;
	xbuf[nIdx++] = (m_nTimeStamp & 0x000000FF);

	memcpy(xbuf+nIdx,g_strSSRC,sizeof(g_strSSRC));//12

	nIdx += sizeof(g_strSSRC);

	nIdx = MakeRTPExtendHeader(xbuf,nIdx,nSecIdx,nFrameIdx,nDSCIndex);

	//NAL(FU-A Type) - 2byte
	xbuf[nIdx++] = H264_FU_A_INDICATOR;
	switch(nMakeMethod)
	{
	case FUA_START:
		{
			xbuf[nIdx++] = H264_FU_A_START_BIT;
			break;
		}
	case FUA_MID:
		{
			xbuf[nIdx++] = H264_FU_A_MID_BIT;
			break;
		}
	case FUA_END:
		{
			xbuf[nIdx++] = H264_FU_A_END_BIT;
			break;
		}
	}
	m_nSequenceNum++;

	return nIdx;
}
RTSP_PACKETIZE_INFO CRTSPServerImageSender::GetPacket(int nIdx)
{
	return m_pArrPacketizeInfo[nIdx];
}
void CRTSPServerImageSender::PacketizeSubData(unsigned char* pPacket,int nStart,int nEnd,int nSecIdx,int nFrameIdx,int nDSCIndex,MR_INFO mr_info)
{
#if 0
	int nSize = nEnd-nStart;

	unsigned char xbuf[1000];
	memset(xbuf,0x00,1000);

	int nRTPSize = 12;
	int nTCPSize = 4;

	xbuf[0] = '$';
	xbuf[1] = 0x00;
	xbuf[2] = (char)(((nSize+12) & 0xFF00) >> 8);
	xbuf[3] = (char)((nSize+12) & 0xFF);

	//RtpHeader Start
	xbuf[4] = 0x80; // RTSP Version 16'b 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
	xbuf[5] = 0x60; 

	//Sequence Number
	xbuf[6] = (char)((m_nSequenceNum&0xFF00) >> 8);
	xbuf[7] = (char)((m_nSequenceNum & 0xFF));

	//timestamp
	xbuf[8]  = (char)((m_nTimeStamp & 0xFF000000) >> 24);
	xbuf[9]  = (char)((m_nTimeStamp & 0x00FF0000) >> 16);
	xbuf[10] = (char)((m_nTimeStamp & 0x0000FF00) >> 8);
	xbuf[11] = (char)((m_nTimeStamp & 0x000000FF));

	memcpy(xbuf+12,g_strSSRC,sizeof(g_strSSRC)); //SSRC

	if(nSize < 5)
		pPacket[0] = H264_FU_A_PPS;
	else
		pPacket[0] = H264_FU_A_SPS;

	memcpy(xbuf+16,pPacket+nStart,nSize);//Data

	AddPacketArray(xbuf,0,nSize+16,nSecIdx,nFrameIdx);

	m_nSequenceNum++;
#else
	int nSize = nEnd-nStart;

	unsigned char xbuf[1500];
	memset(xbuf,0x00,1500);

	int nRTPSize = 12;
	int nTCPSize = 4;
	int nIdx = 0;

	//xbuf[nIdx++] = '$';
	//xbuf[nIdx++] = 0x00;

	//RTSPHeader - Size info(temporary)
	//xbuf[nIdx++] = 0x00;//(char)(((nSize+20) & 0xFF00) >> 8);
	//xbuf[nIdx++] = 0x00;//(char)((nSize+20) & 0xFF);

	//RtpHeader Start
	xbuf[nIdx++] = RTSP_HEADER_VERSIONEX; // RTSP Version 16'b 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
	xbuf[nIdx++] = RTSP_HEADER_M_PT; 

	//Sequence Number
	xbuf[nIdx++] = (char)(m_nSequenceNum >> 8);
	xbuf[nIdx++] = (char)(m_nSequenceNum & 0xFF);

	//timestamp
	xbuf[nIdx++]  = (char)((m_nTimeStamp & 0xFF000000) >> 24);
	xbuf[nIdx++]  = (char)((m_nTimeStamp & 0x00FF0000) >> 16);
	xbuf[nIdx++] = (char)((m_nTimeStamp & 0x0000FF00) >> 8);
	xbuf[nIdx++] = (char)((m_nTimeStamp & 0x000000FF));

	memcpy(xbuf+nIdx,g_strSSRC,sizeof(g_strSSRC)); //SSRC
	
	nIdx += sizeof(g_strSSRC);

	nIdx = MakeRTPExtendHeader(xbuf,nIdx,nSecIdx,nFrameIdx,nDSCIndex,mr_info);

	memcpy(xbuf+nIdx,pPacket+nStart,nSize);//Data

	nIdx += nSize;

	//RTSPHeader - Size info
	//xbuf[2] = ((nIdx-4) & 0xFF00) >> 8;
	//xbuf[3] = ((nIdx-4) & 0xFF);

	AddPacketArray(xbuf,0,/*nSize+*/nIdx,nSecIdx,nFrameIdx);

	TRACE("[%d][%d] %d\t%d\t%d\n",nSecIdx,nFrameIdx,nSize,nIdx,nSize+nIdx);
	m_nSequenceNum++;
#endif
}
void CRTSPServerImageSender::SetWait(DWORD dwMillisec)
{
	MSG msg;
	DWORD dwStart;
	dwStart = GetTickCount();
	while(GetTickCount() - dwStart < dwMillisec)
	{
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}
int CRTSPServerImageSender::MakeRTPHeader(unsigned char* xbuf,BOOL bEnd /*= FALSE*/,BOOL bFirst /*=FALSE*/)
{
	int nIdx = 0;
	//RtpHeader - 12byte 
	if (bFirst) 
		xbuf[nIdx++] = RTSP_HEADER_VERSION; // RTSP Version for Normal
	else
		xbuf[nIdx++] = RTSP_HEADER_VERSIONEX; // RTSP Version for extension header

	if(bEnd)
		xbuf[nIdx++] = RTSP_HEADER_ME_PT;
	else
		xbuf[nIdx++] = RTSP_HEADER_M_PT;

	xbuf[nIdx++] = (m_nSequenceNum & 0x0000FF00) >> 8;
	xbuf[nIdx++] = (m_nSequenceNum & 0x000000FF);

	xbuf[nIdx++]  = (m_nTimeStamp & 0xFF000000) >> 24;
	xbuf[nIdx++]  = (m_nTimeStamp & 0x00FF0000) >> 16;
	xbuf[nIdx++] = (m_nTimeStamp & 0x0000FF00) >> 8;
	xbuf[nIdx++] = (m_nTimeStamp & 0x000000FF);

	memcpy(xbuf+nIdx,g_strSSRC,sizeof(g_strSSRC));//12

	nIdx += sizeof(g_strSSRC);

	//NAL(FU-A Type) - 2byte
	/*xbuf[nIdx++] = H264_FU_A_INDICATOR;
	switch(nMakeMethod)
	{
	case FUA_START:
		{
			xbuf[nIdx++] = H264_FU_A_START_BIT;
			break;
		}
	case FUA_MID:
		{
			xbuf[nIdx++] = H264_FU_A_MID_BIT;
			break;
		}
	case FUA_END:
		{
			xbuf[nIdx++] = H264_FU_A_END_BIT;
			break;
		}
	}*/
	m_nSequenceNum++;

	return nIdx;
}
int CRTSPServerImageSender::MakeRTPExtendHeader(unsigned char*xbuf,int nIdx,int nSecIdx,int nFrameIdx,int nDSCIndex,MR_INFO mr_info)
{
	//Extend header
#ifdef DEV_LGU
	int nExtendSize = 9;
#else
	int nExtendSize = 8;
#endif
	int nService = 0,nFrameCnt = 0,nRepeatCnt = 0;
	if(nSecIdx == -1 && nFrameIdx == -1)
	{
		nService = 0;
		nFrameCnt = 0;
	}
	else
	{
		nService = 1;
#if 0
		nFrameCnt = nSecIdx * m_nFrameRate + nFrameIdx;
		if(nFrameCnt >= 65536)
		{
			nFrameCnt = nFrameCnt - 65536;
			nRepeatCnt = 1;
		}
#else
		nRepeatCnt = m_nSendRepeatCount;
		nFrameCnt = m_nSendFrameCount;
		
#endif
	}
#ifdef DEV_LGU
	//190320 hjcho LGU+ 임시
	if(nDSCIndex != 60)
	{
		nDSCIndex = 60 - nDSCIndex;
	}
	nDSCIndex -= 1;
#endif

	//Extend header - service
	xbuf[nIdx++] = (char)((nService & 0xFF00) >> 8);
	xbuf[nIdx++] = (char)(nService & 0xFF);
	//Extend header - extend size
	xbuf[nIdx++] = (char)((nExtendSize & 0xFF00) >> 8);
	xbuf[nIdx++] = (char)(nExtendSize & 0xFF);
	//Extend header - Channel
	xbuf[nIdx++] = (char)(nDSCIndex & 0xFF);

#if 1
	//Extend header - Repeat Count
	xbuf[nIdx++] = (char)(nRepeatCnt&0xFF);
	//Extend header - Frame Count
	xbuf[nIdx++] = ((nFrameCnt & 0xFF00) >> 8);
	xbuf[nIdx++] = (nFrameCnt & 0xFF);
#else
	//Extend header - Repeat Count
	xbuf[nIdx++] = (char)(m_nSendRepeatCount&0xFF);
	//Extend header - Frame Count
	xbuf[nIdx++] = (char)((m_nSendFrameCount & 0xFF00) >> 8);
	xbuf[nIdx++] = (char)(m_nSendFrameCount & 0xFF);
#endif

#ifdef DEV_LGU
	CString strUTCTime = GetUTCTime();
	char strTime[MAX_PATH];
	sprintf(strTime,"%S",strUTCTime);

	int nTimeLen = strlen(strTime);
	memcpy(xbuf+nIdx,strTime,nTimeLen*sizeof(char));

	nIdx+= nTimeLen;

	xbuf[nIdx++] = 0x00;
	xbuf[nIdx++] = 0x00;
	xbuf[nIdx++] = 0x00;
#else
	// wslee - extend header for MR
/*	// test code
	mr_info.speed_begin = 14020;
	mr_info.speed_mid = 13012;
	mr_info.speed_end = 11087;
	mr_info.hit_speed = 10001;
	mr_info.hit_direction = 7620;
	mr_info.entry_angle = 1801;
	mr_info.launch_angle = 4710;
	//////////////////////////// */
	//Extend header - speed begin
	xbuf[nIdx++] = ((mr_info.speed_begin & 0xFF000000) >> 24);
	xbuf[nIdx++] = ((mr_info.speed_begin & 0x00FF0000) >> 16);
	xbuf[nIdx++] = ((mr_info.speed_begin & 0x0000FF00) >> 8);
	xbuf[nIdx++] = (mr_info.speed_begin & 0x000000FF);
	//Extend header - speed mid
	xbuf[nIdx++] = ((mr_info.speed_mid & 0xFF000000) >> 24);
	xbuf[nIdx++] = ((mr_info.speed_mid & 0x00FF0000) >> 16);
	xbuf[nIdx++] = ((mr_info.speed_mid & 0x0000FF00) >> 8);
	xbuf[nIdx++] = (mr_info.speed_mid & 0x000000FF);
	//Extend header - speed end
	xbuf[nIdx++] = ((mr_info.speed_end & 0xFF000000) >> 24);
	xbuf[nIdx++] = ((mr_info.speed_end & 0x00FF0000) >> 16);
	xbuf[nIdx++] = ((mr_info.speed_end & 0x0000FF00) >> 8);
	xbuf[nIdx++] = (mr_info.speed_end & 0x000000FF);
	//Extend header - Hit Speed
	xbuf[nIdx++] = ((mr_info.hit_speed & 0xFF000000) >> 24);
	xbuf[nIdx++] = ((mr_info.hit_speed & 0x00FF0000) >> 16);
	xbuf[nIdx++] = ((mr_info.hit_speed & 0x0000FF00) >> 8);
	xbuf[nIdx++] = (mr_info.hit_speed & 0x000000FF);
	//Extend header - Hit Direction
	xbuf[nIdx++] = ((mr_info.hit_direction & 0xFF000000) >> 24);
	xbuf[nIdx++] = ((mr_info.hit_direction & 0x00FF0000) >> 16);
	xbuf[nIdx++] = ((mr_info.hit_direction & 0x0000FF00) >> 8);
	xbuf[nIdx++] = (mr_info.hit_direction & 0x000000FF);
	//Extend header - entry angle
	xbuf[nIdx++] = ((mr_info.entry_angle & 0xFF000000) >> 24);
	xbuf[nIdx++] = ((mr_info.entry_angle & 0x00FF0000) >> 16);
	xbuf[nIdx++] = ((mr_info.entry_angle & 0x0000FF00) >> 8);
	xbuf[nIdx++] = (mr_info.entry_angle & 0x000000FF);
	//Extend header - launch angle
	xbuf[nIdx++] = ((mr_info.launch_angle & 0xFF000000) >> 24);
	xbuf[nIdx++] = ((mr_info.launch_angle & 0x00FF0000) >> 16);
	xbuf[nIdx++] = ((mr_info.launch_angle & 0x0000FF00) >> 8);
	xbuf[nIdx++] = (mr_info.launch_angle & 0x000000FF);

#endif

	return nIdx;
}

CString CRTSPServerImageSender::GetUTCTime()
{
	SYSTEMTIME st;
	GetLocalTime(&st);

	CString strTime;

	strTime.Format(_T("%04d-%02d-%02dT%02d:%02d:%02d.%03d+%s"),
		st.wYear,
		st.wMonth,
		st.wDay,
		st.wHour,
		st.wMinute,
		st.wSecond,
		st.wMilliseconds,
		_T("09:00"));

	int nSecIdx = st.wSecond;
	int nMin	= st.wMinute;
	int nHour	= st.wHour;

	nSecIdx -= 12;
	if(nSecIdx < 0)
	{
		nSecIdx = 60 + nSecIdx;
		nMin --;
	}
	if(nMin < 0)
	{
		nMin = 60 + nMin;
		nHour--;
	}

	strTime.Format(_T("%04d-%02d-%02dT%02d:%02d:%02d.%03d+%s"),
		st.wYear,
		st.wMonth,
		st.wDay,
		nHour,
		nMin,
		nSecIdx,
		st.wMilliseconds,
		_T("09:00"));
	return strTime;
}
void CRTSPServerImageSender::MemoryTest(unsigned char*src,int nSrcSize,int nSrcStart,unsigned char*dst,int nDstStart)
{
	for(int i = 0; i < nSrcSize - nSrcStart ; i++)
	{
		if(src[i+nSrcStart] != dst[i+nDstStart])
			TRACE("[%d] : %c - %c\n",i,src[i+nSrcStart],dst[i+nDstStart]);
	}
}
void CRTSPServerImageSender::SendRTSPStateMessage(FRAME_PACKET_INFO info,BOOL bFrame /*= FALSE*/)
{
	if(m_RTSPPlayMethod == PLAY_FHD)
	{
		if(bFrame == FALSE)
		{
			CString strLog;
			//strLog.Format(_T("[ImageSender][%d/%d] - %s"),info.nSecIdx,info.nStopIdx,info.str4DM);
			SendLog(5,strLog);

			ESMEvent* pEvent = new ESMEvent;
			pEvent->message  = WM_ESM_NET_RTSP_STATE_CHECK;
			pEvent->nParam1  = RTSP_MESSAGE_STATE_PLAY_IN;
			pEvent->nParam2  = GetDSCIdx();
			pEvent->nParam3  = info.nSecIdx;

			CString* pstr4DM = new CString;
			pstr4DM->Format(_T("%s"),info.str4DM);
			pEvent->pParam	 = (LPARAM)pstr4DM;

			::SendMessage(m_pWnd->m_hWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pEvent);
		}
		else
		{
			CString strLog;
			//strLog.Format(_T("[ImageSender][%d] - %s"),m_nSendFrameCount,info.str4DM);
			SendLog(5,strLog);

			ESMEvent* pEvent = new ESMEvent;
			pEvent->message  = WM_ESM_NET_RTSP_STATE_CHECK;
			pEvent->nParam1  = RTSP_MESSAGE_STATE_FRAME_COUNT;
			pEvent->nParam2  = GetDSCIdx();
			pEvent->nParam3  = m_nSendFrameCount;

			CString* pstr4DM = new CString;
			pstr4DM->Format(_T("%s"),info.str4DM);
			pEvent->pParam	 = (LPARAM)pstr4DM;

			::SendMessage(m_pWnd->m_hWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pEvent);
		}
		
	}
}