#pragma once
#include "stdafx.h"
#include "ESMMovieThread.h"
#include "ESMMovieThreadImage.h"
#include <vector>
#include "ESMDefine.h"
#include "FFmpegManager.h"
using namespace std;

class AFX_EXT_CLASS CESMMovieRTSend
{
	//DECLARE_DYNCREATE(CESMMovieRTSend)
public:
	CESMMovieRTSend();
	~CESMMovieRTSend();

	/*Variable*/
	CWnd * m_pWnd;
	CStringArray m_strArrPath;
	CString m_strIP;
	CString m_strDSC;
	BOOL m_bOpenClose;
	int m_nWidth;
	int m_nHeight;
	int m_nUVWidth;
	int m_nUVHeight;
	int m_nMarginX;
	int m_nMarginY;
	BOOL m_bSetProperty;
	CString m_str4DMakerPath;
	FILE* m_pFile;
	vector<stAdjustInfo> m_AdjustData;

	/*Inline Func*/
	CRITICAL_SECTION m_criPathMgr;
	void SetParent(CWnd * pWnd) { m_pWnd = pWnd; }
	void SetRTSendPath(CString strPath);
	int m_nPrevIdx;
	/*{
		EnterCriticalSection(&m_criPathMgr);
		m_strArrPath.Add(strPath);
		LeaveCriticalSection(&m_criPathMgr);
	}*/
	CString GetRTSendPath(int nIdx){return m_strArrPath.GetAt(nIdx);}
	void Set4DAInfo(CString strIP,CString strDSC){m_strIP = strIP; m_strDSC = strDSC;};
	void Set4DMakerPath(CString strPath){m_str4DMakerPath = strPath;}
	void SetMargin(int nMarX,int nMarY){m_nMarginX = nMarX;m_nMarginY = nMarY;}
	int GetRTSendPathSize(){return m_strArrPath.GetCount();}
	/*Func*/
	void Run();
	void ExecuteConsole(CString strPath,int nSecIdx);
	void SetAdjustData(stAdjustInfo* AdjustData);
	stAdjustInfo GetAdjustData(CString strDscId);
	void SaveAdjDataAsTxt(stAdjustInfo AdjustData,int nWidth,int nHeight,double dRatio=1,BOOL bLoad=TRUE);
	int Round(double dData);

	/*Thread*/
	static unsigned WINAPI RTDoConsole(LPVOID param);
	static unsigned WINAPI RTRunThread(LPVOID param);

	/*Time Sync*/
	vector<int>*m_pArrReceiveTime;
	void AddReceiveTime(int nTime){m_pArrReceiveTime->push_back(nTime);}
	int  GetReceiveTime(int nIndex){return m_pArrReceiveTime->at(nIndex);}
	void ClearReceiveArr(){m_pArrReceiveTime->clear();}

	FFmpegManager* m_pffmpegMgr;

	BOOL m_bAdjLoad;


	void SetRefereeMode(BOOL b){m_bRefereeMode = b;}
	BOOL GetRefereeMode(){return m_bRefereeMode;}

	static unsigned WINAPI RTRefThread(LPVOID param);
	static unsigned WINAPI RTRefDoConsole(LPVOID param);
	void ExcuteTranscode(CString strPath,int nIdx);
	void SendFinishData(CString strPath,int nIdx);

	void SetRecordingStatus(BOOL b){m_bRecordingStatus = b;}
	BOOL GetRecordingStatus(){return m_bRecordingStatus;}

private:
	//180719 hjcho
	BOOL m_bRefereeMode;
	BOOL m_bRecordingStatus;
};
struct RTSendConsoleData
{
	CESMMovieRTSend* pParent;
	CString strPath;
	CString strSavePath;
	int nGPUIdx;
	int nSecIdx;	
	int nMakeFrame;
};