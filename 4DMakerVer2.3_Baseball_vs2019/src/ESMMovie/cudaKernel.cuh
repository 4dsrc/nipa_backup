#include  <iostream>
//#include "cuda.h"
#include  "opencv\cv.h"
#include  "opencv\highgui.h"
#include "opencv2/opencv_modules.hpp"

using namespace cv;

class CCudaFunc{

public :
	void cuMatFree(Mat img);
	void cuMatMalloc(Mat img,int nSize);
	void cuSetProperty(int h,int w,int nH,int nW,double theta, double RotX, double RotY,double dScale,double MovX,double MovY);
	void cuSetImageMemory(int h,int w,int nH, int nW);
	void cuFreeProperty();
	int divUp(int total, int grain);
	void cuDipAll(Mat Frame,double MovX,double MovY,Mat Resize);
	void cuDipAll(Mat Frame,Mat Resize);//170516 hjcho

private:
	int nWidth;
	int nHeight;
	int nImageSize;

	int nReWidth;
	int nReHeight;
	int nReImageSize;

	double fx;
	double fy;

	cv::Mat cuImg;
	cv::Mat cuRotate;
	cv::Mat cuResize;

	dim3 cuThread;
	dim3 cuBlock;
	dim3 cuReThread;
	dim3 cuReBlock;
};
