#include <stdio.h>
#include <stdlib.h>
#include "define.h"

#include "RTSPServer_SessionMgr.h"
#include "RTSPServer_ImageSender.h"
#include "RTSPServer_Streamer.h"
#include "StopWatch.h"
#include "DllFunc.h"
struct stLiveData
{
	stLiveData()
	{
		nState = LIVE_WAIT;
		nSecIdx = -1;
		nTotalFrame = -1;
		mArrImage = NULL;
	}
	int nState;
	int nSecIdx;
	int nTotalFrame;
	vector<Mat>*mArrImage;
	CString strPath;
	CString strFileName;
};

class CRTSPServerMgr
{
public:
	CRTSPServerMgr();
	~CRTSPServerMgr();

	/*Server*/
	BOOL InitRTSPServer();
	static unsigned WINAPI _LaunchConnectThread(LPVOID param);
	
	/*Client manage*/
	static unsigned WINAPI _LaunchClientThread(LPVOID param);

	/*Thread*/
	void SetThreadStop(BOOL b){m_bThreadStop = b;}
	BOOL GetThreadStop(){return m_bThreadStop;}

	/*Sync Manage*/
	static unsigned WINAPI _SyncTimeMgr(LPVOID param);
	CStopWatch* m_pClock;
	int GetClock();
	void SetClock(CStopWatch* stopwatch){m_pClock = stopwatch;}
	void SetESMTick(int nTick){m_nESMTick = nTick;}
	int	 GetESMTick(){return m_nESMTick;}
	void SetSyncTime(int nTime){m_nSyncTime = nTime;}
	int  GetSyncTime(){return m_nSyncTime;}
	void SetDSCIndex(int n){m_nDSCIdx = n;}
	int GetDSCIndex(){return m_nDSCIdx;}
	void SetFrameRate(int n){m_nFrameRate = n;}
	int GetFrameRate(){return m_nFrameRate;}
	/*Open / Close*/
	void SetMovieState(BOOL b)
	{
		if(b)
			m_mapLiveData.clear();

		m_bMovieState = b;
	}
	BOOL GetMovieState(){return m_bMovieState;}

	/*Movie state*/
	void SetLiveData(int nState,int nSecIdx,int nTotalFrame);
	void ReceiveLiveData(int nState,int nSecIdx,int nTotalFrame,CString strPath,CString strFileName);
	void DecodingFileData(int nSecIdx);
	void AddImage(Mat frameFHD,Mat frameXHD,int nSecIdx,int nFrameIdx);
	void AddImage(MAKE_FRAME_INFO info,int nStopIdx);
	int GetImageSize(RTSP_PLAY_METHOD method);
	/*File I/O*/
	void Set4DMName(CString strName){m_str4DMName = strName;}
	CString Get4DMName(){return m_str4DMName;}

	/*Movie Management*/
	static unsigned WINAPI _LiveMgr(LPVOID param);

	/*Multicast Thread*/
	void LaunchMulticastThread(RTSP_PLAY_METHOD method);
	static unsigned WINAPI _MulticastThread(LPVOID param);

	/*IP*/
	CString GetServerIP();
	CString GetMulticastIP();
	void SetMulticastIPandPort();
	CString m_strMulticastIP;
	int m_nMulticastPort;
	int GetMulticastPort(){return m_nMulticastPort;}

	/*vector<CString>m_ArrMulticastIP;
	void CreateMulticastArr();*/
	
	//RTSPServerManager.h
	void SetRTSPStopTime(int n);

	void SetMainWnd(CWnd* pWnd){m_pWnd = pWnd;}
	CWnd* GetMainWnd(){return m_pWnd;}

private:
	CWnd * m_pWnd;

	int	 m_nESMTick;
	CString m_str4DMName;
	int	 m_nSyncTime;
	BOOL m_bMovieState;
	CString m_strIP;
	BOOL m_bThreadStop;
	SOCKET m_MasterSock;
	WSADATA WsaData;
	std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*> m_mpImageSender;
	std::map<int,stLiveData>m_mapLiveData;
	int m_nDSCIdx;
	int m_nFrameRate;
};
struct RTSP_IMAGE_SENDER
{
	RTSP_IMAGE_SENDER()
	{
		pServerMgr = NULL;
	}
	CRTSPServerMgr* pServerMgr;
	SOCKET sock;
	SOCKET socMaster;
	std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*> mpArrImage;
	CString strServerIP;

	//std::vector<CRTSPServerImageSender*> pArrImageSender;
};
struct RTSP_MULTICAST_THREAD
{
	RTSP_MULTICAST_THREAD()
	{
		pRTSPServerMgr = NULL;
		pImageSender = NULL;
		method = PLAY_NONE;
	}
	CRTSPServerMgr* pRTSPServerMgr;
	CRTSPServerImageSender* pImageSender;
	RTSP_PLAY_METHOD method;
};
