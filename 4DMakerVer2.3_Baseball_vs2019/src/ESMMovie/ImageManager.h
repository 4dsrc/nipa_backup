#pragma once

#include "ESMDefine.h"

class ImageManager
{
public:
	ImageManager(void);
	~ImageManager(void);

	BOOL MakeAvaregeImage(IplImage* pCvImage, IplImage* pCvCompareImage1, IplImage* pCvCompareImage2, int nXMargin, int nYMargin);
	BOOL MakeResultImage(IplImage*	pPicture, int nHightDivCount, int nWidhtDivCount, 
		int nAvgBlue[][IMAGEWIDTHDIVIDECOUNT], int nAvgRed[][IMAGEWIDTHDIVIDECOUNT], int nAvgGreen[][IMAGEWIDTHDIVIDECOUNT] , int nXMargin, int nYMargin );
	BOOL GetAvgRGB(IplImage*	pPicture, int nHightDivCount, int nWidhtDivCount, 
		int nAvgBlue[][IMAGEWIDTHDIVIDECOUNT], int nAvgRed[][IMAGEWIDTHDIVIDECOUNT], int nAvgGreen[][IMAGEWIDTHDIVIDECOUNT] , int nXMargin, int nYMargin);
};