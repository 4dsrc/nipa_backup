#pragma once
class CESMMovieKZoneMgr
{
public:
	CESMMovieKZoneMgr(void);
	~CESMMovieKZoneMgr(void);

//wgkim 181001
private:
	KZoneDataInfo*	m_pKZoneDataInfo;
	int				m_nKZoneDscCount;
	BOOL			m_bOptKZone;

public:
	void			SetKZoneDataInfo(KZoneDataInfo* pKZoneDataInfo);
	int				GetKZoneDscCount();
	void			SetKZoneDscCount(int nKZoneDscCount);
	BOOL			GetOptKZone();
	void			SetOptKZone(BOOL bOptKZone);

	int				GetKZoneDscIndexAt(CString strDscId);
	BOOL			GetStateKZoneImage() { return m_bStateKZoneImage; };

private:
	//181010 wgkim
	Mat		m_imKZonePentagon;
	Mat		m_imKZoneSide;
	Mat		m_imKZoneFace;
	
	cuda::GpuMat	m_gimKZonePentagon;
	cuda::GpuMat	m_gimKZoneSide;
	cuda::GpuMat	m_gimKZoneFace;

	Mat		m_imKZonePentagonX;
	Mat		m_imKZoneSideX;
	Mat		m_imKZoneFaceX;

	cuda::GpuMat	m_gimKZonePentagonX;
	cuda::GpuMat	m_gimKZoneSideX;
	cuda::GpuMat	m_gimKZoneFaceX;

	BOOL	m_bStateKZoneImage;

public:
	void InitKZoneImage();

	void DoDrawKZone(CString strDscId, Mat& srcImage);
	void DrawKZoneAsSkeleton(Mat& srcImage, KZoneDataInfo* pKZoneDataInfo);
	void DrawKZoneAsGradient(Mat& srcImage, KZoneDataInfo* pKZoneDataInfo);
	void DrawKZoneAsGradientUsingGPU(cuda::GpuMat& srcImage, KZoneDataInfo* pKZoneDataInfo);
	void DrawKZonePerspective(Mat& srcImage, Mat& addImage, vector<Point2f> vecPtKzonePoint);
	void DrawKZonePerspectiveUsingGPU(cuda::GpuMat& srcImage, cuda::GpuMat& addImage, vector<Point2f> vecPtKzonePoint);
	void DrawKZoneAsGradientUsingGPUNegative(cuda::GpuMat& srcImage, KZoneDataInfo* pKZoneDataInfo);
	void DoDrawKZoneUsingGPU(CString strDscId, cuda::GpuMat& srcImage, BOOL bIsStrike = TRUE);
};

