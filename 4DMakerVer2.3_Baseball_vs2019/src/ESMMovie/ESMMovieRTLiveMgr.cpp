#include "StdAfx.h"
#include "ESMMovieRTLiveMgr.h"
#include "ESMMovieFileOperation.h"
#include "DllFunc.h"
#include <WinSock2.h>
#define DESIRED_WINSOCK_VERSION        0x0101
#define MINIMUM_WINSOCK_VERSION        0x0001

CESMMovieRTLiveMgr::CESMMovieRTLiveMgr()
{
	m_bMovieState = FALSE;
	m_bThreadStop = FALSE;

	mFFMPEG = new FFMPEG;
	PCFreq = 0.0;
	CounterStart = 0;

	m_bLiveStart = FALSE;
}
CESMMovieRTLiveMgr::~CESMMovieRTLiveMgr()
{
	SetThreadStop(FALSE);
	if(mFFMPEG)
	{
		delete mFFMPEG;
		mFFMPEG = NULL;
	}
}
void CESMMovieRTLiveMgr::LaunchLiveMgr(int nWidth,int nHeight)
{
	SetThreadStop(TRUE);
	mFFMPEG->SetVideoResolution(nWidth,nHeight);
	CString strLog;
	strLog.Format(_T("Output ( %d, %d )"),nWidth,nHeight);
	SendLog(5,strLog);

	//mFFMPEG->m_AVIMOV_BPS = 1024*1024*25;//10Mbps
	mFFMPEG->SetRTSPPort(8554);
	mFFMPEG->SetRTSPUserandPassword("", "");
	mFFMPEG->SetRTSPAddress("multicast");
	mFFMPEG->SetRTSPDescription("This is the stream description");
	mFFMPEG->SetRtpPortNum(20000);
	mFFMPEG->SetEncoder(AV_CODEC_ID_H264);
	mFFMPEG->SetMulticast();
	mFFMPEG->SetUnicast();
	mFFMPEG->Start();

	SendLog(5,_T("RTSP Server is running...!"));

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_LiveMgr,(void*)this,0,NULL);
	CloseHandle(hSyncTime);

	LaunchSyncTimeMgr();
}
void CESMMovieRTLiveMgr::SetMovieState(BOOL b)
{
	if(b)
	{
		/*for(int i = 0 ; i < m_mapLiveData.size() ; i++)
		{
			for(int j = 0 ; j < m_mapLiveData[i].mArrImage->size() ; j++)
			{
				if(m_mapLiveData[i].mArrImage->at(j).empty() == FALSE)
					m_mapLiveData[i].mArrImage->at(j).release();
			}
			delete m_mapLiveData[i].mArrImage;
			m_mapLiveData[i].mArrImage = NULL;
		}*/
		m_mapLiveData.clear();
	}
	m_bMovieState = b;
	//SetLiveStart(b);
}
unsigned WINAPI CESMMovieRTLiveMgr::_LiveMgr(LPVOID param)
{
	CESMMovieRTLiveMgr* pLiveMgr = (CESMMovieRTLiveMgr*)param;
	FFMPEG* mFFMPEG = pLiveMgr->mFFMPEG;
	int nSecIdx = 0;

	while(pLiveMgr->GetThreadStop())
	{
		if(pLiveMgr->GetMovieState())//Open
		{
			if(pLiveMgr->m_mapLiveData[nSecIdx].nState == LIVE_ADJ_FINISH)
			{
				/*if(nSecIdx == 0)
				mFFMPEG->SetWaitTime(2000);
				else
				mFFMPEG->SetWaitTime(-1);*/

				//File Decoding
				pLiveMgr->DecodingFileData(nSecIdx);
				/*for(int i = 0 ; i < 30 ; i++)
				{
				Mat frame;
				vc>>frame;

				resize(frame,frame,cv::Size(1920,1080),0,0,cv::INTER_LINEAR);
				cvtColor(frame,YUV,cv::COLOR_BGR2YUV_IYUV);
				pLiveMgr->mFFMPEG->InsertImageData(YUV);
				}*/
				
				CESMMovieFileOperation fo;
				CString strPath = pLiveMgr->m_mapLiveData[nSecIdx].strPath;
				CString strFileName = pLiveMgr->m_mapLiveData[nSecIdx].strFileName;
				CString strSavePath;
				strSavePath.Format(_T("%s\%s"),pLiveMgr->Get4DMName(),strFileName);
				if(fo.IsFileExist(strPath))
				{
					fo.Copy(strPath,strSavePath,1);
					SendLog(5,strSavePath);
					fo.Delete(strPath);
				}

				nSecIdx++;
			}
		}
		else//Close
		{
			//mFFMPEG->SetSyncStart(FALSE);
			nSecIdx = 0;
		}
		Sleep(1);
	}

	return FALSE;
}
void CESMMovieRTLiveMgr::LaunchSyncTimeMgr()
{
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_SyncTimeMgr,(void*)this,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI CESMMovieRTLiveMgr::_SyncTimeMgr(LPVOID param)
{
	CESMMovieRTLiveMgr* pLiveMgr = (CESMMovieRTLiveMgr*)param;
	FFMPEG* mFFMPEG = pLiveMgr->mFFMPEG;
	BOOL bSetSync = FALSE;
	CString strLog;
	int nTime = 0;
	//Mat YUV;
	////if(vc.isOpened())
	//{
	//	//mFFMPEG->SetSyncStart(TRUE);
	//	for(int i = 0 ; i < 10 ; i ++)
	//	{
	//		VideoCapture vc("C:\\Program Files\\ESMLab\\4DMaker\\img\\Intro1.mp4");
	//		while(1)
	//		{
	//			Mat frame;
	//			vc>>frame;
	//			if(frame.empty())
	//				break;

	//			cvtColor(frame,YUV,cv::COLOR_BGR2YUV_IYUV);
	//			//m_mapLiveData[nSecIdx].mArrImage->at(nCnt) = frame.clone();
	//			mFFMPEG->InsertImageData(YUV);
	//		}
	//	}
	//	//mFFMPEG->SetSyncStart(FALSE);
	//}

	//mFFMPEG->SetSyncStart(TRUE);
	//pLiveMgr->StartCounter();
	//while(1)
	//{
	//	//if(pLiveMgr->GetCounter() > 15000)
	//		if(mFFMPEG->GetImageDataSize() == 0)
	//			break;
	//}
	//mFFMPEG->SetSyncStart(FALSE);

	while(pLiveMgr->GetThreadStop())
	{
		Sleep(1);

		if(pLiveMgr->GetMovieState())//Open
		{
			if(bSetSync == FALSE)
			{
				TRACE("bSync: %d\n",bSetSync);

				if(pLiveMgr->GetClock() >= pLiveMgr->GetSyncTime())
				{
					strLog.Format(_T("Movie Start - %d"),mFFMPEG->GetImageDataSize());
					SendLog(5,strLog);

					mFFMPEG->SetSyncStart(TRUE);
					bSetSync = TRUE;
				}
			}
			//SetSyncTime
			//int nTime = GetTickCount();// / 1000;
			//strLog.Format(_T("%d - %d\r\n"),nTime,pLiveMgr->GetSyncTime());
			//TRACE(strLog);
			/*pLiveMgr->m_nESMTick += 30;*/

			/*pLiveMgr->SetESMTick(pLiveMgr->GetESMTick()+300);
			strLog.Format(_T("%d - %d\r\n"),
				pLiveMgr->GetESMTick(),pLiveMgr->GetSyncTime());
			TRACE(strLog);
			if(pLiveMgr->GetESMTick() >= pLiveMgr->GetSyncTime() && bSetSync == FALSE)
			{
				strLog.Format(_T("Movie Start - %d(%d - %d)"),
					mFFMPEG->GetImageDataSize(),pLiveMgr->GetESMTick(),pLiveMgr->GetSyncTime());
				SendLog(5,strLog);

				mFFMPEG->SetSyncStart(TRUE);
				bSetSync = TRUE;
			}*/
		}
		else//Close
		{
			if(mFFMPEG->GetImageDataSize() == 0)
			{
				mFFMPEG->SetSyncStart(FALSE);
				pLiveMgr->SetESMTick(0);
				bSetSync = FALSE;
				nTime = 0;
			}
		}
	}

	return FALSE;
}
void CESMMovieRTLiveMgr::SetLiveData(int nState,int nSecIdx,int nTotalFrame)
{
	m_mapLiveData[nSecIdx].nState		= nState;
	m_mapLiveData[nSecIdx].nSecIdx		= nSecIdx;
	m_mapLiveData[nSecIdx].nTotalFrame  = nTotalFrame;
	m_mapLiveData[nSecIdx].mArrImage	= new vector<Mat>(nTotalFrame);
}
void CESMMovieRTLiveMgr::ReceiveLiveData(int nState,int nSecIdx,int nTotalFrame,CString strPath,CString strFileName)
{
	m_mapLiveData[nSecIdx].nState			= nState;
	m_mapLiveData[nSecIdx].strPath		= strPath;
	m_mapLiveData[nSecIdx].strFileName = strFileName;
}
void CESMMovieRTLiveMgr::DecodingFileData(int nSecIdx)
{
	//stLiveData* pLiveData = &m_mapLiveData[nSecIdx];
	int nStart = GetTickCount();
	CString strPath = m_mapLiveData[nSecIdx].strPath;
	CT2CA pszConvertedAnsiString (strPath);
	std::string strDst(pszConvertedAnsiString);

	VideoCapture vc(strDst);
	Mat frame;

	int nCnt = 0;
	char str[100];
	Mat YUV;
	CString strTemp;
	while(1)
	{
		vc>>frame;
		if(frame.empty())
			break;
		//
		//strTemp.Format(_T("[%s] %d_%d"),GetInIpAddress(),nSecIdx,nCnt);
		sprintf(str,"[%S] %d_%d",GetInIpAddress(),nSecIdx,nCnt);
		//memcpy(str, (unsigned char*)(LPCTSTR)strTemp,i);

		//putText(frame,str,cv::Point(100,100),CV_FONT_BOLD,10,cv::Scalar(0,0,0),1,8);
		putText(frame,str,cv::Point(50,100),
			cv::FONT_HERSHEY_PLAIN,5,cv::Scalar(0,0,0),3,8);

		/*if(nCnt == 0)
			frame = cv::Scalar(0,0,0);*/
		//cv::resize(frame,frame,cv::Size(1280,720),0,0,cv::INTER_CUBIC);
		cvtColor(frame,YUV,cv::COLOR_BGR2YUV_IYUV);
		//m_mapLiveData[nSecIdx].mArrImage->at(nCnt) = frame.clone();
		mFFMPEG->InsertImageData(YUV);
		//mFFMPEG->SendNewFrame((char*)YUV.data);
		nCnt++;
	}

	m_mapLiveData[nSecIdx].nState = LIVE_INSERT_FINISH;
	//TRACE("[%d] Decoding time: %d\n",nSecIdx,GetTickCount()-nStart);
}
void CESMMovieRTLiveMgr::StartCounter()
{
	LARGE_INTEGER li;
	if (!QueryPerformanceFrequency(&li)) {
		//		TRACE("\nPerformance Counter Failed\n");
	}
	PCFreq = double(li.QuadPart)/1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}
double CESMMovieRTLiveMgr::GetCounter()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart-CounterStart)/PCFreq;
}
int CESMMovieRTLiveMgr::GetClock()
{
	m_pClock->End();
	
	return (int)(m_pClock->GetDurationMilliSecond() * 10);
}
CString CESMMovieRTLiveMgr::GetInIpAddress()
{
	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	CString strIP;
	PHOSTENT hostinfo;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 )
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				strIP = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}
		}      
		WSACleanup( );
	} 
	return strIP;
}
