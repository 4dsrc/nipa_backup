#pragma once
#include "stdafx.h"
#include "ESMMovieRTSend60p.h"
#include "ESMMovieMgr.h"
#include "FFmpegManager.h"
#include "ESMFunc.h"
#include "DllFunc.h"

#include "opencv2\xphoto.hpp"
#include "ESMMovieFileOperation.h"
#include "ESMIni.h"


CESMMovieRTSend60p::CESMMovieRTSend60p()
{
	m_bOpenClose   = FALSE;
	m_bSetProperty = FALSE;
	m_pFile = NULL;
	m_nCurIdx	  = 0;
}
CESMMovieRTSend60p::~CESMMovieRTSend60p()
{

}
unsigned WINAPI CESMMovieRTSend60p::RTRunThread(LPVOID param)
{
	CESMMovieRTSend60p* pRTSend = (CESMMovieRTSend60p*)param;

	CESMMovieFileOperation _file;
	int _nPrevIdx = 0;
	UINT nCnt = 0;

	while(1)
	{
		if(pRTSend->m_strArrPath.GetCount() == 0)
		{
			Sleep(10);
			continue;
		}
		if(pRTSend->m_strArrPath.GetCount() >= 1 && pRTSend->m_strArrPath.GetCount() == _nPrevIdx+1)
		{
			Sleep(10);

			pRTSend->ExecuteConsole(pRTSend->GetRTSendPath(_nPrevIdx),_nPrevIdx);
			//pRTSend->AddFinishFile(pRTSend->GetRTSendPath(_nPrevIdx));
			_nPrevIdx++;

			nCnt = 0;
			if(pRTSend->m_bOpenClose == FALSE)
			{
				pRTSend->m_bOpenClose = TRUE;

				ESMEvent* pMsg	= new ESMEvent;
				pMsg->message = WM_MAKEMOVIE_GPU_FILE_OPEN_CLOSE;
				pMsg->nParam1	= (int)pRTSend->m_bOpenClose;
				::SendMessage(pRTSend->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
			}
		}	
		else
		{
			nCnt++;
			Sleep(10);
			if(nCnt > 250)
			{
				if(pRTSend->m_bOpenClose == TRUE)
				{
					pRTSend->m_bOpenClose = FALSE;
					pRTSend->m_nCurIdx	  = 0;
					pRTSend->RemoveAllSavePath();
					pRTSend->m_strArrOriPath.RemoveAll();
					//m_nCurIdx = 0;
					ESMEvent* pMsg	= new ESMEvent;
					pMsg->message = WM_MAKEMOVIE_GPU_FILE_OPEN_CLOSE;
					pMsg->nParam1	= (int)pRTSend->m_bOpenClose;
					::SendMessage(pRTSend->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

					_file.Delete(_T("M:\\Movie"), FALSE);

					_nPrevIdx = 0;
					nCnt = 0;
					pRTSend->m_strArrPath.RemoveAll();
				}
			}
			TRACE(_T("ELSE\n"));
		}
	}
}
void CESMMovieRTSend60p::Run()
{
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL, 0, RTRunThread, (void *)this, 0, NULL);
	CloseHandle(hSyncTime);

	HANDLE hSyncTime1 = NULL;
	hSyncTime1 = (HANDLE)_beginthreadex(NULL, 0, RTMergeThread, (void *)this, 0, NULL);
	CloseHandle(hSyncTime1);
}
void CESMMovieRTSend60p::ExecuteConsole(CString strPath,int nSecIdx)
{
	CT2CA pszConvertedAnsiString (strPath);
	string strDst(pszConvertedAnsiString);

	CString strSavePath;
	strSavePath.Format(_T("M:\\Movie\\%s__%d.mp4"),m_strDSC,nSecIdx);

	VideoCapture vc(strDst);
	if(!vc.isOpened())
	{
		Sleep(50);
		vc.open(strDst);
		if(!vc.isOpened())
		{
			Sleep(100);
			vc.open(strDst);
			if(!vc.isOpened())
			{
				CString strLog;
				strLog.Format(_T("[File Error] ---------- %s"),strPath);
				SendLog(1,strLog);

				return;
			}
		}
	}

	if(!m_bSetProperty)
	{
		m_nWidth = (int)vc.get(cv::CAP_PROP_FRAME_WIDTH);
		m_nHeight = (int)vc.get(cv::CAP_PROP_FRAME_HEIGHT);
		m_nUVWidth = (int)vc.get(cv::CAP_PROP_FRAME_WIDTH)/2;
		m_nUVHeight = (int)vc.get(cv::CAP_PROP_FRAME_HEIGHT)/2;
	}

	int nGPUIdx = nSecIdx%2;
	RTSend60pConsoleData* pConsoleData = new RTSend60pConsoleData;
	pConsoleData->pParent		= this;
	pConsoleData->strPath		= strPath;
	pConsoleData->strSavePath	= strSavePath;
	pConsoleData->nGPUIdx		= nGPUIdx;
	pConsoleData->nSecIdx		= nSecIdx;
	pConsoleData->nMakeFrame	= (int)vc.get(cv::CAP_PROP_FRAME_COUNT);

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL, 0, RTDoConsole, (void *)pConsoleData, 0, NULL);
	CloseHandle(hSyncTime);

	vc.release();
}
unsigned WINAPI CESMMovieRTSend60p::RTDoConsole(LPVOID param)
{
	RTSend60pConsoleData* pConsoleData = (RTSend60pConsoleData*)param;

	CESMMovieRTSend60p* pParent		  = (CESMMovieRTSend60p*)pConsoleData->pParent;
	CString strPath					  = pConsoleData->strPath;
	CString strSavePath				  = pConsoleData->strSavePath;
	int nGPUIdx						  = pConsoleData->nGPUIdx;
	int nSecIdx						  = pConsoleData->nSecIdx;
	int nMakeFrame					  = pConsoleData->nMakeFrame;
	
	delete pConsoleData;

	CString strCmd,strOpt,strLog,strTime;

	strTime.Format(_T("%d"),nSecIdx);
	strCmd.Format(_T("%s\\bin\\ESMGPUProcess.exe"),pParent->m_str4DMakerPath);
	strOpt.Format(_T("1 %s %s %d %d"),strPath,strSavePath,nGPUIdx,nMakeFrame);
	
	strLog.Format(_T("[%d] - Cmd Processing Start"),nSecIdx);
	//SendLog(5,strLog);
	if(!pParent->m_bSetProperty)
	{
		SendLog(5,strCmd);
		SendLog(5,strOpt);
		pParent->m_pFile = fopen("M:\\AdjData.txt","w");
		CString strCamID = pParent->m_strDSC;
		stAdjustInfo adjinfo = pParent->GetAdjustData(strCamID);
		pParent->SaveAdjDataAsTxt(adjinfo,pParent->m_nWidth,pParent->m_nHeight);
		pParent->SaveAdjDataAsTxt(adjinfo,pParent->m_nUVWidth,pParent->m_nUVHeight,0.5);

		pParent->m_bSetProperty = TRUE;
		fclose(pParent->m_pFile);
		SendLog(5,_T("Set Property Finish"));
	}

	int nStart = GetTickCount();
	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;
	//lpExecInfo.nShow	=SW_SHOW;
	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	lpExecInfo.lpFile = _T("exit");
	lpExecInfo.lpVerb = L"close";
	lpExecInfo.lpParameters = NULL;

	if(TerminateProcess(lpExecInfo.hProcess,0))
	{
		lpExecInfo.hProcess = 0;
		SendLog(5,_T("End"));
	}
	int nEnd = GetTickCount();

	strLog.Format(_T("[%d] - Cmd Processing Time : %d"),nSecIdx/2,nEnd - nStart);
	pParent->AddFinishFile(strSavePath);
	pParent->m_strArrOriPath.Add(strPath);
	SendLog(5,strLog);
	
	/*CStringArray *pstrArrFrameSend = new CStringArray;
	pstrArrFrameSend->Add(pParent->m_strDSC);
	pstrArrFrameSend->Add(strTime);
	pstrArrFrameSend->Add(strSavePath);
	pstrArrFrameSend->Add(strPath);

	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message = WM_MAKEMOVIE_GPU_FILE_END;
	pMsg->pParam	= (LPARAM)pstrArrFrameSend;
	::SendMessage(pParent->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);*/

	return TRUE;
}
void CESMMovieRTSend60p::SetAdjustData(stAdjustInfo *AdjustData)
{
	m_AdjustData.push_back(*AdjustData);
}
void CESMMovieRTSend60p::SaveAdjDataAsTxt(stAdjustInfo AdjustData,int nWidth,int nHeight,double dRatio)
{
	double dbScale = AdjustData.AdjSize;
	
	double degree = AdjustData.AdjAngle;
	double dbAngleAdjust = -1*(degree+90);

	double dbRotX = Round(AdjustData.AdjptRotate.x * dRatio);
	double dbRotY = Round(AdjustData.AdjptRotate.y * dRatio);
	double dbMovX = Round(AdjustData.AdjMove.x * dRatio);
	double dbMovY = Round(AdjustData.AdjMove.y * dRatio);
	
	int nMarginX = m_nMarginX * dRatio;
	int nMarginY = m_nMarginY * dRatio;

	Mat RotMat(2,3,CV_64FC1);
	RotMat = getRotationMatrix2D(Point2f(dbRotX,dbRotY),dbAngleAdjust,dbScale);
	
/*	strLog.Format(_T("%f %f %f %f %f %f %f"),dbScale,RotMat->at<double>(0,0),RotMat->at<double>(0,1),RotMat->at<double>(0,2)
		,RotMat->at<double>(1,0),RotMat->at<double>(1,1),RotMat->at<double>(1,2));
	SendLog(5,strLog);*/

	RotMat.at<double>(0,2) += (dbMovX / dbScale);
	RotMat.at<double>(1,2) += (dbMovY / dbScale);
	/*CString strLog;
	strLog.Format(_T("%f %f %f %f %f %f %f"),dbScale,RotMat->at<double>(0,0),RotMat->at<double>(0,1),RotMat->at<double>(0,2)
		,RotMat->at<double>(1,0),RotMat->at<double>(1,1),RotMat->at<double>(1,2));
	SendLog(5,strLog);*/

	double dbMarginScale = 1 / ((double)(nWidth - nMarginX * 2)/ nWidth);

	(int)((nWidth  * dbMarginScale - nWidth)/2);
	(int)((nHeight * dbMarginScale - nHeight)/2);

	//RotMatrix
	for(int i = 0 ; i < 2; i++)
		for(int j = 0 ; j < 3; j++)
			fprintf(m_pFile,"%f\n",RotMat.at<double>(i,j));

	//Resize
	fprintf(m_pFile,"%d\n",(int)(nWidth  * dbMarginScale));
	fprintf(m_pFile,"%d\n",(int)(nHeight  * dbMarginScale));

	//Rect Point
	fprintf(m_pFile,"%d\n",(int)((nWidth  * dbMarginScale - nWidth)/2));
	fprintf(m_pFile,"%d\n",(int)((nHeight * dbMarginScale - nHeight)/2));

	//OriSize
	fprintf(m_pFile,"%d\n",nWidth);
	fprintf(m_pFile,"%d\n",nHeight);
}
int CESMMovieRTSend60p::Round(double dData)
{
	int nResult = 0;

	if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);
	else
		nResult = 0;

	return nResult;
}
stAdjustInfo CESMMovieRTSend60p::GetAdjustData(CString strDscId)
{
	stAdjustInfo AdjustData;

	for(int i =0 ;i < m_AdjustData.size(); i++)
	{
		if(m_AdjustData.at(i).strDSC == strDscId)
		{
			AdjustData = m_AdjustData.at(i);
			break;
		}
		//AdjustData = (m_AdjustData.at(i));
		//if( AdjustData.strDSC == strDscId )
		//{
		//	break;
		//}
	}
	return AdjustData;
}
unsigned WINAPI CESMMovieRTSend60p::RTMergeThread(LPVOID param)
{
	CESMMovieRTSend60p* pParent = (CESMMovieRTSend60p*) param;

	int nCurIdx = 0;
	CString strOpt,strOpt1;
	strOpt.Format(_T("-i \"concat:"));
	strOpt1.Format(_T("-c copy -bsf:a acc_adtstoasc \""));
	CString strDSCID = pParent->m_strDSC;
	while(1)
	{
		//if(pParent->GetFinishFileCount() >= 1 && pParent->m_bOpenClose == FALSE)
		//{
		//	SendLog(5,_T("DeleteFunc"));
		//	pParent->RemoveAllSavePath();
		//	pParent->m_strArrOriPath.RemoveAll();
		//	m_nCurIdx = 0;
		//	//SendLog(5,_T("Delete All Path- %d %d %d"),nCurIdx,pParent->GetFinishFileCount(),
		//	//	pParent->m_strArrOriPath.GetCount());
		//	continue;
		//}

		if(pParent->GetFinishFileCount() == 0)
		{
			Sleep(10);
			continue;
		}

		if(pParent->GetFinishFileCount() >= 2)
		{
			pParent->MergeMovie(pParent->m_nCurIdx);
			pParent->m_nCurIdx++;
			//nCurIdx++;
		}
		else
		{
			Sleep(10);
			continue;
		}
	}
}
void CESMMovieRTSend60p::MergeMovie(int nSecIdx)
{
	CString strOpt,strCmd,strTime,strLog,strOutput;

	strTime.Format(_T("%d"),nSecIdx);
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),m_str4DMakerPath);
	strOutput.Format(_T("M:\\Movie\\(MERGE)%s__%d.mp4"),m_strDSC,nSecIdx);
	strOpt.Format(_T("-i \"concat:%s|%s\" -c copy -bsf:a accadtstoasc \"%s\""),
		GetFinishFilePath(0),GetFinishFilePath(1),strOutput);

	//SendLog(5,strOpt);

	int nStart = GetTickCount();
	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;
	//lpExecInfo.nShow	=SW_SHOW;
	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	lpExecInfo.lpFile = _T("exit");
	lpExecInfo.lpVerb = L"close";
	lpExecInfo.lpParameters = NULL;

	if(TerminateProcess(lpExecInfo.hProcess,0))
	{
		lpExecInfo.hProcess = 0;
		SendLog(5,_T("End"));
	}
	int nEnd = GetTickCount();

	strLog.Format(_T("[%d] - Cmd Merge Time : %d"),nSecIdx,nEnd - nStart);
	//SendLog(5,strLog);

	CStringArray *pstrArrFrameSend = new CStringArray;
	pstrArrFrameSend->Add(m_strDSC);//0
	pstrArrFrameSend->Add(strTime);//1
	pstrArrFrameSend->Add(strOutput);//2
	pstrArrFrameSend->Add(m_strArrOriPath.GetAt(0));//3 Original file (0)
	pstrArrFrameSend->Add(m_strArrOriPath.GetAt(1));//4 Original file (1)
	pstrArrFrameSend->Add(GetFinishFilePath(0));//5 Encoding file (0)
	pstrArrFrameSend->Add(GetFinishFilePath(1));//6 Encoding file (1)

	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message = WM_MAKEMOVIE_GPU_FILE_END;
	pMsg->pParam	= (LPARAM)pstrArrFrameSend;
	::SendMessage(m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

	for(int i = 0 ; i < 2 ; i++)
	{
		DeleteFinishFile(0);
		m_strArrOriPath.RemoveAt(0);
	}
}
int CESMMovieRTSend60p::GetSecIdxFromPath(CString strPath)
{
	int nFind = strPath.ReverseFind('\\') + 1;
	CString csFile = strPath.Right(strPath.GetLength()-nFind);
	csFile.Trim();
	//csFile.Trim(_T("\\"));
	nFind = csFile.Find('.');
	CString csCam = csFile.Left(nFind);
	csCam.Trim();
	nFind = csCam.ReverseFind('_') + 1;

	CString strSecIdx = csCam.Right(csCam.GetLength()-nFind);

	return _ttoi(strSecIdx);
}