#pragma once
enum RTSP_PLAY_METHOD
{
	PLAY_NONE = 0,
	PLAY_FHD ,
	PLAY_HD,
	PLAY_TOTAL
};
enum RTSP_CMD_TYPE
{
	RTSP_ERROR = 0,
	RTSP_OPTIONS,
	RTSP_DESCRIBE,
	RTSP_SETUP,
	RTSP_PLAY,
	RTSP_TEARDOWN,
	RTSP_UNKNOWN,
};
enum RTSP_STATE
{
	RTSP_STATE_FAIL = 0,
	RTSP_STATE_WAIT,
	RTSP_STATE_OPTION,
	RTSP_STATE_DESCRIBE,
	RTSP_STATE_SETUP,
	RTSP_STATE_PLAY,
	RTSP_STATE_TEARDOWN,
	RTSP_STATE_TOTAL,
};
enum ENUM_FUA_SEND
{
	FUA_START = 0,
	FUA_MID,
	FUA_END,
};
enum RTSP_ERROR_STATE
{
	RTSP_ERROR_NONE = 0,
	RTSP_ERROR_461,
	RTSP_ERROR_400,
	RTSP_ERROR_401,
	RTSP_ERROR_404,
	RTSP_ERROR_459,
	RTSP_ERROR_451,
	RTSP_ERROR_405,
};
enum LIVE_STATE
{
	LIVE_WAIT = 0,
	LIVE_FILE_INPUT,
	LIVE_ADJ_FINISH,
	LIVE_ADJ_FAIL,
	LIVE_INSERT_FINISH
};
struct RTSP_EXTENTION_INFO
{
	char CameraIdx;
	char SecIdx;
	char FrameIdx;
	char reserved;
};
struct RTSP_PACKETIZE_INFO
{
	RTSP_PACKETIZE_INFO()
	{
		//memset(ArrData,0x00,1500);
		nSize = 0;
		bTail = false;
		nFrameIdx = -1;
		nSecIdx = -1;
	}
	char ArrData[2000];
	int nSize;
	bool bTail;
	int nFrameIdx;
	int nSecIdx;
};
struct RTSP_MOVIE_TAIL
{
	RTSP_MOVIE_TAIL(int last,int current)
	{
		nLast = last;
		nCur = current;
	}
	int nLast;
	int nCur;
};
struct RTSP_SEND_INFO
{
	RTSP_SEND_INFO()
	{
		pData = NULL;
		nDataLength = 0;
	}
	char* pData;
	int nDataLength;
};
typedef struct _MR_INFO
{
	_MR_INFO()
	{
		speed_begin = -1;
		speed_mid = -1;
		speed_end = -1;
		hit_speed = -1;
		hit_direction = -1;
		entry_angle = -1;
		launch_angle = -1;
	}
	int speed_begin;
	int speed_mid;
	int speed_end;
	int hit_speed;
	int hit_direction;
	int entry_angle;
	int launch_angle;
} MR_INFO;

struct MAKE_FRAME_INFO
{
	Mat Y;
	Mat U;
	Mat V;
	Mat HD_Y;
	Mat HD_U;
	Mat HD_V;
	int nSecIdx;
	int nFrameIdx;
	CString str4DM;
	int nStopIdx;
	MR_INFO mr_info;
};

#define RTSP_MULTISTREAM
#define RTSP_SEND_MESSAGE_TO_4DC

#define RTSP_BUFFER_SIZE       10000    // for incoming requests, and outgoing responses
#define RTSP_PARAM_STRING_MAX  200  
#define RTSP_PACKET_MAX			9000
/**/

//Change Resolution For MR
#define ENCODE_UHD_WIDTH		3840
#define ENCODE_UHD_HEIGHT		2160
#define ENCODE_UHD_BITRATE		1024*1024*20

#define ENCODE_MR_WIDTH			3840
#define ENCODE_MR_HEIGHT		1080
#define ENCODE_MR_BITRATE		1024*1024*20

#define ENCODE_FHD_WIDTH		1920
#define ENCODE_FHD_HEIGHT		1080
#define ENCODE_FHD_BITRATE		1024*1024*20
#if 0
#define ENCODE_HD_WIDTH			1280
#define ENCODE_HD_HEIGHT		720
#define ENCODE_HD_BITRATE		1024*1024*10
#else
#define ENCODE_HD_WIDTH			320
#define ENCODE_HD_HEIGHT		180
#define ENCODE_HD_BITRATE		1024*1024*0.5
#endif
#define ENCODE_LOW_WIDTH		176
#define ENCODE_LOW_HEIGHT		99
#define ENCODE_LOW_BITRATE		1024*1024*0.5
#define ENCODE_FRAME_RATE		30
#define ENCODE_FRAME_GOP		1
#define ENCODE_FRAME_PAYLOAD	100000

/*H264 FU-A type*/
#define H264_FU_A_INDICATOR		0x7C
#define H264_FU_A_SPS			0x67
#define H264_FU_A_PPS			0x68
#define H264_FU_A_START_BIT		0x85
#define H264_FU_A_MID_BIT		0x05
#define H264_FU_A_END_BIT		0x45
#define H264_FU_A				28
#define MTU_FRAME_SIZE			1352//0x05A8

/*RTP Header*/
#define RTSP_HEADER_VERSION		0x80
#define RTSP_HEADER_VERSIONEX	0x90
#define RTSP_HEADER_M_PT		0x60
#define RTSP_HEADER_ME_PT		0xE0

/*TCP Header*/
#define TCP_HEADER_RTSP			0x24

/*SSRC*/
static char g_strSSRC[4] = {0x81,0xF5,0xAB,0x8D};

/*TEST*/
#define RTSP_TEST_MODE

/*MULTICAST DEFINES*/
#ifdef DEV_LGU
#define RTSP_MULTICAST_TTL		64
#else
#define RTSP_MULTICAST_TTL		1
#endif
#define RTSP_MULTICAST_FHD_PORT	2326
#define RTSP_MULTICAST_XHD_PORT	2328