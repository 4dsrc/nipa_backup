#pragma once

class ESMMovieInfoArray
{
public:
	ESMMovieInfoArray(void);
	~ESMMovieInfoArray(void);

	int GetMovieCount();
	int SetMoviePath(CString strMovieId, CString strMoviePath);
	stMovieInfo GetMoiveInfo(CString strMovieId);
	void SetMoiveInfo(stMovieInfo MovieData);
	void ReSetMoiveInfo();
	CString GetMoviePath(CString strMovieId);
	CString GetMovieID(int nIndex);

	stAdjustInfo GetAdjustData(CString strMovieId);
	void SetAdjustData(CString strMovieId, stAdjustInfo* AdjustData);
	int GetMaginX(){return m_nMarginX; }
	int GetMaginY(){return m_nMarginY; }
	void SetMaginX(int nMarginX){m_nMarginX = nMarginX; }
	void SetMaginY(int nMarginY){m_nMarginY = nMarginY; }

private:
	vector<stMovieInfo> m_pMovieInfoArray;
	int m_nMarginX;
	int m_nMarginY;
};

