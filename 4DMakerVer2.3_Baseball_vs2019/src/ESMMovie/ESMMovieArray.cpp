#include "StdAfx.h"
#include "ESMMovieArray.h"


ESMMovieArray::ESMMovieArray(void)
{
	m_arrMovieData = NULL;
	m_nDscCount = 0;
}


ESMMovieArray::~ESMMovieArray(void)
{
}

void ESMMovieArray::ClearMovie()
{
	if( m_arrMovieData)
	{
		for( int i =0 ;i < m_nDscCount; i++)
		{
			if( m_arrMovieData[i] != NULL)
			{
				delete m_arrMovieData[i];
				m_arrMovieData[i] = NULL;
			}
		}
		delete m_arrMovieData;
	}
}

void ESMMovieArray::SetDscCount(int nDscCount)
{
	ClearMovie();
	m_nDscCount = nDscCount;
	m_arrMovieData = new ESMMovieData*[m_nDscCount];
	for( int i =0 ;i < m_nDscCount; i++)
	{
		m_arrMovieData[i] = new ESMMovieData;
	}
}

int ESMMovieArray::SetMovieFile(CString strMovieId, CString strMoviePath)
{
	int nMovieIndex = 0;
	for( int i =0 ;i < GetMovieCount(); i++)
	{
		int nFrameCount = 0, nWidth = 0, nHeight = 0, nChannel = 0;
		if( m_arrMovieData[i]->GetDscId() == _T("") || m_arrMovieData[i]->GetDscId() == strMovieId)
		{
			m_arrMovieData[i]->SetMoviePath(strMoviePath);
			m_arrMovieData[i]->SetDscId(strMovieId);

			nMovieIndex = i;
			break;
		}
	}
	return nMovieIndex;
}

CString ESMMovieArray::GetMovieFile(int nMovieIndex)
{
	CString strMoviePath;
	if( m_nDscCount <= nMovieIndex)
		return strMoviePath;

	int nFrameCount = 0, nWidth = 0, nHeight = 0, nChannel = 0;

	if( m_arrMovieData[nMovieIndex] != NULL)
	{
		strMoviePath = m_arrMovieData[nMovieIndex]->GetMoviePath();
	}
	return strMoviePath;
}


ESMMovieData* ESMMovieArray::GetMovieData(int nIndex)
{
	if( m_nDscCount <= nIndex)
		return NULL;

	return m_arrMovieData[nIndex];
}

ESMMovieData* ESMMovieArray::GetMovieData(CString strMovieID)
{
	for( int i =0 ;i < m_nDscCount; i++)
	{
		if( m_arrMovieData[i]->GetDscId() == strMovieID)
			return m_arrMovieData[i];
	}
	return NULL;
}

stAdjustInfo ESMMovieArray::GetAdjustData(int nIndex)
{
	stAdjustInfo AdjustData;
	if( m_nDscCount <= nIndex)
		return AdjustData;

	AdjustData = m_arrMovieData[nIndex]->GetAdjustinfo();
	return  AdjustData;
}

void ESMMovieArray::SetAdjustData(int nIndex, stAdjustInfo* AdjustData)
{
	if( m_nDscCount <= nIndex)
		return ;

	m_arrMovieData[nIndex]->SetAdjustinfo(AdjustData);
}