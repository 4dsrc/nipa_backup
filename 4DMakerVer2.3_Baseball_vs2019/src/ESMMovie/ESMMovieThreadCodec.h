////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieThreadCodec.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-26
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ESMMovieThread.h"
#include "FFmpegManager.h"
#include "opencv2/highgui.hpp"

class CESMMovieThreadCodec: public CESMMovieThread
{
	DECLARE_DYNCREATE(CESMMovieThreadCodec)

public:
	CESMMovieThreadCodec(void) {};
	CESMMovieThreadCodec(CWinThread* pParent, short nMessage, ESMMovieMsg* pMsg);	
	virtual ~CESMMovieThreadCodec();

private:
	//FFmpegManager m_FFmpegMgr;
public:	
	virtual int Run(void);
	//void GetMovieInfo(int nMovieIndex);
	void GetMovieInfo(CString strMovieId);
	void DoDecoding(ESMMovieMsg* m_pMsg);
	void DoEncoding(ESMMovieMsg* m_pMsg, CString strFilePath, CString strMovieName);

};


