////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieRTThread.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-28
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ESMMovieThread.h"
#include "ESMMovieThreadImage.h"
#include <vector>
#include "ESMDefine.h"

using namespace std;

struct DecodingFrameInfo{
	DecodingFrameInfo()
	{
		nFinish = 0;
	}
	vector<Mat>*pArrMatInfo;
	vector<MakeFrameInfo>* pArrFrameInfo;
	int nFinish;
};

struct CalcValue{
	CalcValue()
	{
		angle = -90.0;
		theta = 0.0;
		RotX = 0.0;
		RotY = 0.0;
		AdjX = 0.0;
		AdjY = 0.0;
		nH = 1080;
		nW = 1920;
		nLeft = 0;
		nTop = 0;
	}
	double angle;
	double theta;
	double RotX;
	double RotY;
	double AdjX;
	double AdjY;
	int nH;
	int nW;
	int nLeft;
	int nTop;
};
class AFX_EXT_CLASS CESMMovieRTThread: public CESMMovieThread
{
	DECLARE_DYNCREATE(CESMMovieRTThread)

public:
	CESMMovieRTThread(void);
	virtual ~CESMMovieRTThread();

public:	
	virtual int Run(void);

	static unsigned WINAPI RTMakeGPUFrame(LPVOID param);
	static unsigned WINAPI RTMakeGPUAdjust(LPVOID param);


	CString FileReadDone();
	CString GetDSCIDFromPath(CString strPath);
	CString GetSecIdxFromPath(CString strPath);
	void SetAdjustData(stAdjustInfo* AdjustData);
	stAdjustInfo GetAdjustData(CString strDscId);
	void SetMargin(int nMarX,int nMarY);
	void CalcAdjust(stAdjustInfo AdjustData);
	int Round(double dData);

	CalcValue m_CalcData;
	BOOL m_bGPUUse;
	BOOL m_bSetProperty;
	CString m_ArrstrPath[500];
	int m_Index;

	int m_nFrameinfoCnt;

	vector<stAdjustInfo> m_AdjustData;
	vector<Mat*> m_DecodingData;
	vector<DecodingFrameInfo*>m_DecodingMain;
	int m_nDecoding;

	int nWidth;
	int nHeight;
	int nImageSize;
	int nYUVSize;
	int nDecodingCnt;
	int nAdjustCnt;
	BOOL bAdjust;

	//Adjust Data
	int nMarginX;
	int nMarginY;
	int nReWidth;
	int nReHeight;
	double theta;

	CWnd * m_pWnd;
	void SetParent(CWnd * pWnd) { m_pWnd = pWnd; }

	BOOL m_bOpenClose;
	CString m_strIP;
	CString m_strDSC;
	void Set4DAInfo(CString strIP, CString strDSC){m_strIP = strIP; m_strDSC = strDSC;}
	
	CStringArray m_strArrPath;
	void SetRTSendPath(CString strPath){m_strArrPath.Add(strPath);}
	CString GetRTSendPath(int nIdx){return m_strArrPath.GetAt(nIdx);}
};

struct RTThreadDecodingData
{
	RTThreadDecodingData()
	{
		pMovieThreadManager = NULL;
		pArrFrameInfo = NULL;
		nArrayStartIndex = 0;
		nStartIndex = 0;
		nEndIndex = 0;
		bGPUMakeFile = FALSE;
		bRotate = FALSE;
	}
	CESMMovieRTThread* pMovieThreadManager;
	vector<MakeFrameInfo>* pArrFrameInfo;
	int nArrayStartIndex;
	int nStartIndex;
	int nEndIndex;
	BOOL bGPUMakeFile;
	BOOL bRotate;
};