#include "stdafx.h"
#include "RTSPServer_SessionMgr.h"

CRTSPServerSessionMgr::CRTSPServerSessionMgr(SOCKET socMaster,SOCKET soc,std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*> mpImageSender)
{
	m_MasterSoc = socMaster;
	m_RTSPClient = soc;
	//m_pArrImageSender = pArrImageSender;
	m_mpImageSender = mpImageSender;

	m_RTSPPlayMethod = PLAY_NONE;
	m_bThreadStop = TRUE;
	m_bPlay = FALSE;
	m_bTCP = FALSE;
	m_bMulticast = FALSE;
	m_ClientRTPPort = 0;
	m_ClientRTCPPort = 0;

	InitializeCriticalSection(&m_criSendInfo);
	m_bThreadRun = FALSE;
	SetThreadRun(FALSE);
#ifdef RTSP_TEST_MODE
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_SendThread,(void*)this,0,NULL);
	CloseHandle(hSyncTime);
#endif
}
CRTSPServerSessionMgr::~CRTSPServerSessionMgr()
{
	m_bThreadStop = FALSE;
	SetThreadStop(FALSE);
	//SendLog(5,_T("[CRTSPServerSessionMgr] ThreadStop"));
	while(1)
	{
		if(GetThreadRun() == FALSE)
			break;

		Sleep(1);
	}
	DeleteCriticalSection(&m_criSendInfo);
	//Sleep(1000);
	closesocket(m_RTSPClient);
	/*for(int i = 0 ; i < 10 ; i++)
		SendLog(5,_T("[CRTSPServerSessionMgr] Destructor"));*/
	//WSACleanup();
}
void CRTSPServerSessionMgr::Init()
{
	memset(m_URLPreSuffix,0x00,RTSP_PARAM_STRING_MAX);
	memset(m_URLSuffix,0x00,RTSP_PARAM_STRING_MAX);
	memset(m_URLType,0x00,RTSP_PARAM_STRING_MAX);
	memset(m_CSeq,0x00,RTSP_PARAM_STRING_MAX);
	memset(m_URLHostPort,0x00,RTSP_BUFFER_SIZE); 
}
RTSP_CMD_TYPE CRTSPServerSessionMgr::HandleRTSPRequests(char const* aRequest,unsigned aRequestSize)
{
	RTSP_ERROR_STATE RTSPError = ParseRTSPPacket(aRequest,aRequestSize);
	if(RTSPError == RTSP_ERROR_NONE)
	{
		switch(m_RTSPCmdType)
		{
		case RTSP_OPTIONS:{Handle_RTSP_Options();break;}
		case RTSP_DESCRIBE:{Handle_RTSP_Describe();break;}
		case RTSP_SETUP:
			{
				if(Handle_RTSP_Setup() == FALSE)
				{
					char Response[1024];

					sprintf(Response,
						"RTSP/1.0 400 Bad Request\r\n"
						"CSeq: %s\r\n"
						"%S\r\n\r\n",
						m_CSeq,
						GetDateHeader());
#ifdef RTSP_TEST_MODE
					RTSP_SEND_INFO info;
					info.nDataLength = strlen(Response);
					info.pData = new char[info.nDataLength];
					memset(info.pData,0x00,info.nDataLength);
					memcpy(info.pData,Response,info.nDataLength);

					AddSendInfo(info);
#else
					int ret = send(m_RTSPClient,Response,strlen(Response),0);
#endif
				}
				break;
			}
		case RTSP_PLAY:{Handle_RTSP_Play();break;}
		case RTSP_TEARDOWN:{Handle_RTSP_Teardown();break;}
		}
	}
	else
	{
		SendLog(0,_T("RTSP ERROR LOG"));
		char Response[1024];

		switch(RTSPError)
		{
		case RTSP_ERROR_461:
			{
				sprintf(Response,
					"RTSP/1.0 461 Unsupported Transport\r\n"
					"CSeq: %s\r\n"
					"%S\r\n\r\n",
					m_CSeq,
					GetDateHeader());
			}
			break;
		case RTSP_ERROR_400:
			{
				sprintf(Response,
					"RTSP/1.0 400 Bad Request\r\n"
					"CSeq: %s\r\n"
					"%S\r\n\r\n",
					m_CSeq,
					GetDateHeader());
			}
			break;
		case RTSP_ERROR_401:
			{
				sprintf(Response,
					"RTSP/1.0 401 Unauthorized\r\n"
					"CSeq: %s\r\n"
					"%S\r\n\r\n",
					m_CSeq,
					GetDateHeader());
			}
			break;
		case RTSP_ERROR_404:
			{
				sprintf(Response,
					"RTSP/1.0 404 Not Found\r\n"
					"CSeq: %s\r\n"
					"%S\r\n\r\n",
					m_CSeq,
					GetDateHeader());
			}
			break;
		case RTSP_ERROR_459:
			{
				sprintf(Response,
					"RTSP/1.0 459 Aggregate operation not allowed\r\n"
					"CSeq: %s\r\n"
					"%S\r\n\r\n",
					m_CSeq,
					GetDateHeader());
			}
			break;
		case RTSP_ERROR_451:
			{
				sprintf(Response,
					"RTSP/1.0 451 Invalid Parameter\r\n\r\n");
			}
			break;
		case RTSP_ERROR_405:
			{

			}
			break;
		}
#ifdef RTSP_TEST_MODE
		RTSP_SEND_INFO info;
		info.nDataLength = strlen(Response);
		info.pData = new char[info.nDataLength];
		memset(info.pData,0x00,info.nDataLength);
		memcpy(info.pData,Response,info.nDataLength);

		AddSendInfo(info);
#else
		int ret = send(m_RTSPClient,Response,strlen(Response),0);
#endif
	}

	return m_RTSPCmdType;
}
RTSP_ERROR_STATE CRTSPServerSessionMgr::ParseRTSPPacket(char const* aRequest,unsigned aRequestSize)
{
	char     CmdName[RTSP_PARAM_STRING_MAX];
	char     CurRequest[RTSP_BUFFER_SIZE];
	unsigned CurRequestSize; 

	Init();
	CurRequestSize = aRequestSize;
	memcpy(CurRequest,aRequest,aRequestSize);

	// check whether the request contains information about the RTP/RTCP UDP client ports (SETUP command)
	char * ClientPortPtr;
	char * TmpPtr;
	char   CP[1024];
	char * pCP;
	ClientPortPtr = strstr(CurRequest,"client_port");
	if (ClientPortPtr != nullptr)
	{
		TmpPtr = strstr(ClientPortPtr,"\r\n");
		if (TmpPtr != nullptr) 
		{
			TmpPtr[0] = 0x00;
			strcpy(CP,ClientPortPtr);
			pCP = strstr(CP,"=");
			if (pCP != nullptr)
			{
				pCP++;
				strcpy(CP,pCP);
				pCP = strstr(CP,"-");
				if (pCP != nullptr)
				{
					pCP[0] = 0x00;
					m_ClientRTPPort  = atoi(CP);
					m_ClientRTCPPort = m_ClientRTPPort + 1;
				};
			};
		};
	};

	// Read everything up to the first space as the command name
	bool parseSucceeded = false;
	unsigned i;
	for (i = 0; i < sizeof(CmdName)-1 && i < CurRequestSize; ++i) 
	{
		char c = CurRequest[i];
		if (c == ' ' || c == '\t') 
		{
			parseSucceeded = true;
			break;
		}
		CmdName[i] = c;
	}
	CmdName[i] = '\0';
	if (!parseSucceeded) 
		return RTSP_ERROR_401;

	// find out the command type
	if (strstr(CmdName,"OPTIONS")   != nullptr) m_RTSPCmdType = RTSP_OPTIONS;  
	else if (strstr(CmdName,"DESCRIBE")  != nullptr) m_RTSPCmdType = RTSP_DESCRIBE; 
	else if (strstr(CmdName,"SETUP")     != nullptr) m_RTSPCmdType = RTSP_SETUP;
	else if (strstr(CmdName,"PLAY")      != nullptr) m_RTSPCmdType = RTSP_PLAY;
	else if (strstr(CmdName,"TEARDOWN")  != nullptr) m_RTSPCmdType = RTSP_TEARDOWN;
	else if (strstr(CmdName,"SET_PARAMETER") != nullptr) return RTSP_ERROR_451;
	else if (strstr(CmdName,"GET_PARAMETER") != nullptr) return RTSP_ERROR_404;
	else if (strstr(CmdName,"PAUSE") != nullptr) return RTSP_ERROR_404;

	// check whether the request contains transport information (UDP or TCP)
	if (m_RTSPCmdType == RTSP_SETUP)
	{
		TmpPtr = strstr(CurRequest,"RTP/AVP/TCP");
		if (TmpPtr == nullptr) 
			SetTCP(FALSE);//m_bTCP = FALSE;
		else
			SetTCP(TRUE);//m_bTCP = TRUE;
			//return RTSP_ERROR_461;

		TmpPtr = strstr(CurRequest,"multicast");
		if(TmpPtr == nullptr)
			SetMulticast(FALSE);//m_bMulticast = FALSE;
		else
			SetMulticast(TRUE);//m_bMulticast = TRUE;

		if(GetTCP() == TRUE && GetMulticast() == TRUE)
			return RTSP_ERROR_459;
	};
	// check whether the request contains sdp
	if(m_RTSPCmdType == RTSP_DESCRIBE)
	{
		TmpPtr = strstr(CurRequest,"sdp");
		if (TmpPtr == nullptr) 
			return RTSP_ERROR_461;
	}
	// Skip over the prefix of any "rtsp://" or "rtsp:/" URL that follows:
	unsigned j = i+1;
	while (j < CurRequestSize && (CurRequest[j] == ' ' || CurRequest[j] == '\t')) ++j; // skip over any additional white space
	for (; (int)j < (int)(CurRequestSize-8); ++j) 
	{
		if ((CurRequest[j]   == 'r' || CurRequest[j]   == 'R')   && 
			(CurRequest[j+1] == 't' || CurRequest[j+1] == 'T') && 
			(CurRequest[j+2] == 's' || CurRequest[j+2] == 'S') && 
			(CurRequest[j+3] == 'p' || CurRequest[j+3] == 'P') && 
			CurRequest[j+4] == ':' && CurRequest[j+5] == '/') 
		{
			j += 6;
			if (CurRequest[j] == '/') 
			{   // This is a "rtsp://" URL; skip over the host:port part that follows:
				++j;
				unsigned uidx = 0;
				while (j < CurRequestSize && CurRequest[j] != '/' && CurRequest[j] != ' ') 
				{   // extract the host:port part of the URL here
					m_URLHostPort[uidx] = CurRequest[j];
					uidx++;
					++j;
				};
			} 
			else --j;
			i = j;
			break;
		}
	}

	// Look for the URL suffix (before the following "RTSP/"):
	parseSucceeded = false;
	for (unsigned k = i+1; (int)k < (int)(CurRequestSize-5); ++k) 
	{
		if (CurRequest[k]   == 'R'   && CurRequest[k+1] == 'T'   && 
			CurRequest[k+2] == 'S'   && CurRequest[k+3] == 'P'   && 
			CurRequest[k+4] == '/') 
		{
			while (--k >= i && CurRequest[k] == ' ') {}
			
			unsigned k1 = k;
			while (k1 > i && CurRequest[k1] != '?') --k1;
			if (k - k1 + 1 > sizeof(m_URLSuffix)) 
				return RTSP_ERROR_400;

			unsigned n = 0, k2 = k1+1;
			while (k2 <= k) m_URLSuffix[n++] = CurRequest[k2++];
			m_URLSuffix[n] = '\0';

			if (k1 - i > sizeof(m_URLPreSuffix)) 
				return RTSP_ERROR_400;

			n = 0; k2 = i + 1;
			while (k2 <= k1 - 1) m_URLPreSuffix[n++] = CurRequest[k2++];
			m_URLPreSuffix[n] = '\0';
			i = k + 7; 
			parseSucceeded = true;
			break;
		}
	}
	if (!parseSucceeded) 
		return RTSP_ERROR_400;

	// Look for "CSeq:", skip whitespace, then read everything up to the next \r or \n as 'CSeq':
	parseSucceeded = false;
	for (j = i; (int)j < (int)(CurRequestSize-5); ++j) 
	{
		if (CurRequest[j]   == 'C' && CurRequest[j+1] == 'S' && 
			CurRequest[j+2] == 'e' && CurRequest[j+3] == 'q' && 
			CurRequest[j+4] == ':') 
		{
			j += 5;
			while (j < CurRequestSize && (CurRequest[j] ==  ' ' || CurRequest[j] == '\t')) ++j;
			unsigned n;
			for (n = 0; n < sizeof(m_CSeq)-1 && j < CurRequestSize; ++n,++j) 
			{
				char c = CurRequest[j];
				if (c == '\r' || c == '\n') 
				{
					parseSucceeded = true;
					break;
				}
				m_CSeq[n] = c;
			}
			m_CSeq[n] = '\0';
			break;
		}
	}
	if (!parseSucceeded) 
		return RTSP_ERROR_404;

	BOOL bFind = FALSE;
	for(int n = 0 ; n < strlen(m_URLSuffix) ; n++)
	{
		if(bFind == FALSE)
		{
			if((m_URLSuffix[n] == 't' || m_URLSuffix[n] == 'T') && 
				(m_URLSuffix[n+1] == 'y' || m_URLSuffix[n+1] == 'Y') && 
				(m_URLSuffix[n+2] == 'p' || m_URLSuffix[n+2] == 'P') && 
				(m_URLSuffix[n+3] == 'e' || m_URLSuffix[n+2] == 'E'))
			{
				bFind = TRUE;
				int n1 = n+4+1;
				int nCnt = 0;
				while(n1 < strlen(m_URLSuffix))
				{
					m_URLType[nCnt++] = m_URLSuffix[n1++];
				}
				m_URLType[nCnt] = '\0';
			}
		}
	}

#if 1
	if(strstr("FHD",m_URLType) != nullptr)
		m_RTSPPlayMethod = PLAY_FHD;
#else
	if(strstr("FHD",m_URLType) != nullptr)
		m_RTSPPlayMethod = PLAY_FHD;
	else if(strstr("XHD",m_URLType)!= nullptr)
		m_RTSPPlayMethod = PLAY_HD;
#endif
	if(m_RTSPPlayMethod == PLAY_NONE)
		return RTSP_ERROR_404;

	SendLog(5,_T("Received - ")+CharToCString(m_CSeq));
	//// Also: Look for "Content-Length:" (optional)
	//for (j = i; (int)j < (int)(CurRequestSize-15); ++j) 
	//{
	//	if (CurRequest[j]    == 'C'  && CurRequest[j+1]  == 'o'  && 
	//		CurRequest[j+2]  == 'n'  && CurRequest[j+3]  == 't'  && 
	//		CurRequest[j+4]  == 'e'  && CurRequest[j+5]  == 'n'  && 
	//		CurRequest[j+6]  == 't'  && CurRequest[j+7]  == '-'  && 
	//		(CurRequest[j+8] == 'L' || CurRequest[j+8]   == 'l') &&
	//		CurRequest[j+9]  == 'e'  && CurRequest[j+10] == 'n' && 
	//		CurRequest[j+11] == 'g' && CurRequest[j+12]  == 't' && 
	//		CurRequest[j+13] == 'h' && CurRequest[j+14] == ':') 
	//	{
	//		j += 15;
	//		while (j < CurRequestSize && (CurRequest[j] ==  ' ' || CurRequest[j] == '\t')) ++j;
	//		unsigned num;
	//		if (sscanf(&CurRequest[j], "%u", &num) == 1) m_ContentLength = num;
	//	}
	//}
	return RTSP_ERROR_NONE;
};
void CRTSPServerSessionMgr::Handle_RTSP_Options()
{
	char Response[1024];
	sprintf(Response,
		"RTSP/1.0 200 OK\r\n"
		"CSeq: %s\r\n"
		"Public: OPTIONS, DESCRIBE, SETUP, TEARDOWN, PLAY\r\n\r\n",
		//"Public: DESCRIBE, SETUP, TEARDOWN, PLAY\r\n\r\n",
		//"public: DESCRIBE, SETUP, TEARDOWN, PLAY, PAUSE\r\n\r\n",
		m_CSeq);

#ifdef RTSP_TEST_MODE
	RTSP_SEND_INFO info;
	info.nDataLength = strlen(Response);
	info.pData = new char[info.nDataLength];
	memset(info.pData,0x00,info.nDataLength);
	memcpy(info.pData,Response,info.nDataLength);

	AddSendInfo(info);
#else
	int ret = send(m_RTSPClient,Response,strlen(Response),0);
#endif

	SendLog(5,CharToCString(Response));
}
void CRTSPServerSessionMgr::Handle_RTSP_Describe()
{
	char Response[1024];
	
	//m_pArrImageSender->at(m_RTSPPlayMethod).GetSDP();
	/*CString strSDP = m_pImageStreamer[nMethod]->GetSDP();*/
	CString strSDP = m_mpImageSender[m_RTSPPlayMethod]->GetSDP();
	int nSDPLength = strSDP.GetLength();

	sprintf(Response,
		"RTSP/1.0 200 OK\r\n"
		"CSeq: %s\r\n"
		"%S\r\n"
		"Content-Type: application/sdp\r\n"
		"Content-Length: %d\r\n\r\n"
		"%S",
		m_CSeq,
		GetDateHeader(),
		nSDPLength,
		strSDP);

#ifdef RTSP_TEST_MODE
	RTSP_SEND_INFO info;
	info.nDataLength = strlen(Response);
	info.pData = new char[info.nDataLength];
	memset(info.pData,0x00,info.nDataLength);
	memcpy(info.pData,Response,info.nDataLength);

	AddSendInfo(info);
#else
	int ret = send(m_RTSPClient,Response,strlen(Response),0);
#endif

	SendLog(5,CharToCString(Response));
}
BOOL CRTSPServerSessionMgr::Handle_RTSP_Setup()
{
	char Response[1024];
	char Transport[255];

	if(InitTransport(m_ClientRTPPort,m_ClientRTCPPort,GetTCP()) == FALSE)
		return FALSE;

	if(GetTCP() == TRUE)
	{
		sprintf(Transport,
			"RTP/AVP/TCP;unicast;"
			"destination=%S;source=%S;interleaved=0-1",
			GetServerIP(),
			GetServerIP());
	}
	else if(GetTCP() == FALSE)
	{
		if(GetMulticast() == TRUE)
		{
			sprintf(Transport,
				"RTP/AVP;multicast;destination=%S;port=%i-%i;ttl=%d",
				GetMulticastIP(),
				m_RtpServerPort,
				m_RtcpServerPort,
				RTSP_MULTICAST_TTL
				);
		}
		else
		{
			sprintf(Transport,
				"RTP/AVP;unicast;client_port=%i-%i;server_port=%i-%i",
				m_ClientRTPPort,
				m_ClientRTCPPort,
				m_RtpServerPort,
				m_RtcpServerPort);
		}
	}
	sprintf(Response,
		"RTSP/1.0 200 OK\r\n"
		"CSeq: %s\r\n"
		"%S\r\n"
		"Transport: %s\r\n"
		"Session:%s\r\n\r\n",
		m_CSeq,
		GetDateHeader(),
		Transport,
		"1Q2W3E4R");

#ifdef RTSP_TEST_MODE
	RTSP_SEND_INFO info;
	info.nDataLength = strlen(Response);
	info.pData = new char[info.nDataLength];
	memset(info.pData,0x00,info.nDataLength);
	memcpy(info.pData,Response,info.nDataLength);

	AddSendInfo(info);
#else
	int ret = send(m_RTSPClient,Response,strlen(Response),0);
#endif

	SendLog(5,CharToCString(Response));

	return TRUE;
}
void CRTSPServerSessionMgr::Handle_RTSP_Play()
{
	char Response[1024];
	sprintf(Response,
		"RTSP/1.0 200 OK\r\n"
		"CSeq: %s\r\n"
		"%S\r\n"
		"RTP-Info: url=rtsp://%S:8554/4DReplayLIVE?FHD/track0;"
		"seq=864;rtptime=\r\n"
		"Session:%s\r\n\r\n",
		m_CSeq,
		GetDateHeader(),
		GetServerIP(),
		"1Q2W3E4R");

#ifdef RTSP_TEST_MODE
	RTSP_SEND_INFO info;
	info.nDataLength = strlen(Response);
	info.pData = new char[info.nDataLength];
	memset(info.pData,0x00,info.nDataLength);
	memcpy(info.pData,Response,info.nDataLength);

	AddSendInfo(info);
#else
	int ret = send(m_RTSPClient,Response,strlen(Response),0);
#endif
	
	SetPlay(TRUE);

	SendLog(5,CharToCString(Response));
}
void CRTSPServerSessionMgr::Handle_RTSP_Teardown()
{
	char   Response[1024];

	_snprintf(Response,sizeof(Response),
		"RTSP/1.0 200 OK\r\nCSeq: %s\r\n\r\n",m_CSeq);


#ifdef RTSP_TEST_MODE
	RTSP_SEND_INFO info;
	info.nDataLength = strlen(Response);
	info.pData = new char[info.nDataLength];
	memset(info.pData,0x00,info.nDataLength);
	memcpy(info.pData,Response,info.nDataLength);

	AddSendInfo(info);
#else
	int ret = send(m_RTSPClient,Response,strlen(Response),0);
#endif

	SendLog(5,CharToCString(Response));
}
char const * CRTSPServerSessionMgr::DateHeader() 
{
	char buf[200];    
	time_t tt = time(NULL);
	strftime(buf, sizeof buf, "Date: %a, %b %d %Y %H:%M:%S GMT", gmtime(&tt));
	return buf;
}
CString CRTSPServerSessionMgr::GetDateHeader()
{
	char buf[200];    
	time_t tt = time(NULL);
	strftime(buf, sizeof buf, "Date: %a, %b %d %Y %H:%M:%S GMT", gmtime(&tt));

	CString strTemp;
	strTemp.Format(_T("%S"),buf);
	return strTemp;
}
CString CRTSPServerSessionMgr::CharToCString(char* str, BOOL bDelete)
{
	if( str == NULL)
		return _T("");

	CString strString;
	int len; 
	BSTR buf; 
	len = MultiByteToWideChar(CP_ACP, 0, str, strlen(str), NULL, NULL); 
	buf = SysAllocStringLen(NULL, len); 
	MultiByteToWideChar(CP_ACP, 0, str, strlen(str), buf, len); 

	strString.Format(_T("%s"), buf); 
	if( bDelete == TRUE && str)
	{
		delete[] str;
		str = NULL;
	}
	strString.Trim();
	return strString;
}
CString CRTSPServerSessionMgr::GetMulticastIP()
{
#if 1
	return m_strMulticastIP;
#else
	CString strIP = GetServerIP();

	CString strMulticastIP,strLastIP;
	int nFind = strIP.ReverseFind('.');
	int nCount = strIP.GetLength() - nFind;
	int nSize = strIP.GetLength();
	strLastIP = strIP.Mid(nFind,nCount);

	strMulticastIP.Format(_T("224.0%s.5"),strLastIP);

	return strMulticastIP;
#endif
}
BOOL CRTSPServerSessionMgr::InitTransport(u_short aRtpPort, u_short aRtcpPort, bool TCP)
{
	sockaddr_in Server;  

	m_RtpClientPort  = aRtpPort;
	m_RtcpClientPort = aRtcpPort;
	//m_TCPTransport   = TCP;

	if (GetTCP() == FALSE)
	{   // allocate port pairs for RTP/RTCP ports in UDP transport mode
		if(GetMulticast() == FALSE)
		{
			Server.sin_family      = AF_INET;   
			Server.sin_addr.s_addr = INADDR_ANY;   
			for (u_short P = 6970; P < 0xFFFE ; P += 2)
			{
				m_RtpSocket     = socket(AF_INET, SOCK_DGRAM, 0);                     
				Server.sin_port = htons(P);
				if (bind(m_RtpSocket,(const sockaddr*)&Server,(int)sizeof(Server)) == 0)
				{   // Rtp socket was bound successfully. Lets try to bind the consecutive Rtsp socket
					m_RtcpSocket = socket(AF_INET, SOCK_DGRAM, 0);
					Server.sin_port = htons(P + 1);
					if (bind(m_RtcpSocket,(const sockaddr*)&Server,(int)sizeof(Server)) == 0) 
					{
						m_RtpServerPort  = P;
						m_RtcpServerPort = P+1;
						break; 
					}
					else
					{
						closesocket(m_RtpSocket);
						closesocket(m_RtcpSocket);
					}
				}
				else 
					closesocket(m_RtpSocket);
			}

			m_nRecvLen = sizeof(m_sockRecvAddr);
			getpeername(m_RTSPClient,(struct sockaddr*)&m_sockRecvAddr,&m_nRecvLen);
			m_sockRecvAddr.sin_family = AF_INET;
			m_sockRecvAddr.sin_port = htons(m_RtpClientPort);
		}
		else // multicast
		{
			int nPort;
#if 1
			if(m_RTSPPlayMethod == PLAY_FHD)
			{
				nPort = GetMulticastPort();
			}
			else
				return FALSE;
#else
			if(m_RTSPPlayMethod == PLAY_FHD)
				nPort = RTSP_MULTICAST_FHD_PORT;
			else if(m_RTSPPlayMethod == PLAY_HD)
				nPort = RTSP_MULTICAST_XHD_PORT;
			else
				return FALSE;
#endif
			m_RtpServerPort  = nPort;
			m_RtcpServerPort = m_RtpServerPort+1;
			/*m_RtpSocket = socket(AF_INET, SOCK_DGRAM, 0);

			if (m_RtpSocket < 0) 
			{
				SendLog(5,_T("Multicast Sock Error"));
				return FALSE;
			}

			CString strMulticastIP = GetMulticastIP();
			char *servAddr = new char[strMulticastIP.GetLength()];
			sprintf(servAddr,"%S",strMulticastIP);
			m_nRecvLen = sizeof(m_sockRecvAddr);

			int nPort;
			if(m_RTSPPlayMethod == PLAY_FHD)
				nPort = RTSP_MULTICAST_FHD_PORT;
			else if(nPort == PLAY_HD)
				nPort = RTSP_MULTICAST_XHD_PORT;

			struct sockaddr_in addr;
			memset(&addr, 0, sizeof(addr));
			m_sockRecvAddr.sin_family = AF_INET;
			m_sockRecvAddr.sin_addr.s_addr = inet_addr(servAddr);
			m_sockRecvAddr.sin_port = htons(nPort);

			m_RtpServerPort  = nPort;
			m_RtcpServerPort = m_RtpServerPort+1;*/

			/*setsockopt(m_RTSPClient,IPPROTO_IP,IP_MULTICAST_TTL,
				/ *(char*)&* /RTSP_MULTICAST_TTL,sizeof(RTSP_MULTICAST_TTL));*/
			//if(setsockopt(m_RTSPClient,IPPROTO_IP,IP_MULTICAST_TTL,
			//	/*(char*)&*/RTSP_MULTICAST_TTL,sizeof(RTSP_MULTICAST_TTL)) == SOCKET_ERROR)
			//{
			//	SendLog(0,_T("MULTICAST ERROR"));
			//	return FALSE;
			//}
		}
	}

//	if(m_bMulticast)
//	{
//#if 0
//		struct addrinfo *resif = NULL;
//		resif = ResolveAddress
//#else
//		CString strMulticastIP = GetMulticastIP();
//		char *servAddr = new char[strMulticastIP.GetLength()];
//		sprintf(servAddr,"%S",strMulticastIP);
//		m_nRecvLen = sizeof(m_sockRecvAddr);
//		//getpeername(m_RTSPClient,(struct sockaddr*)&m_sockRecvAddr,&m_nRecvLen);
//
//		m_sockRecvAddr.sin_family = AF_INET;
//		m_sockRecvAddr.sin_addr.s_addr = inet_addr(servAddr);
//		m_sockRecvAddr.sin_port = htons(1001);
//
//		/*if(servAddr)
//		{
//			delete[] servAddr;
//			servAddr = NULL;
//		}*/
//		//int multiTTL = RTSP_MULTICAST_TTL;
//		/*if(setsockopt(m_RTSPClient,IPPROTO_IP,IP_MULTICAST_TTL,/ *(char*)&* /RTSP_MULTICAST_TTL,sizeof(RTSP_MULTICAST_TTL)) == SOCKET_ERROR)
//		{
//			SendLog(0,_T("MULTICAST ERROR"));
//			return FALSE;
//		}*/
//#endif
//	}
//	else
//	{
//		
//	}
	return TRUE;
}
unsigned WINAPI CRTSPServerSessionMgr::_SendThread(LPVOID param)
{
	CRTSPServerSessionMgr* pSessionMgr = (CRTSPServerSessionMgr*)param;
	SOCKET socClient = pSessionMgr->m_RTSPClient;

	pSessionMgr->SetThreadRun(TRUE);
	
	while(pSessionMgr->GetThreadStop())
	{
		if(pSessionMgr->GetSendInfoCount())
		{
			RTSP_SEND_INFO info = pSessionMgr->GetSendInfo();

			if(info.nDataLength && info.pData != NULL)
			{
				if(info.pData[0] == 'R')
				{
					int ret = send(socClient,info.pData,info.nDataLength,0);
					if(info.pData)
					{
						delete info.pData;
						info.pData = NULL;
					}
				}
				else
				{
					if(pSessionMgr->GetTCP())
					{
						int ret = 0;
#if 0
						char dollar = '$';
						send(socClient,&dollar,1,0);
						char streamidx = 0x00;
						send(socClient,&streamidx,1,0);

						char netPacketSize[2];
						int nSize = info.nDataLength;
						netPacketSize[0] = (nSize & 0xFF00) >> 8;
						netPacketSize[1] = (nSize & 0xFF);
						send(socClient,netPacketSize,2,0);
#else
						char netPacketSize[4];
						netPacketSize[0] = '$';
						netPacketSize[1] = 0x00;
						int nSize = info.nDataLength;
						netPacketSize[2] = (nSize & 0xFF00) >> 8;
						netPacketSize[3] = (nSize & 0xFF);
						ret = send(socClient,netPacketSize,4,0);
#endif
						ret = send(socClient,info.pData,info.nDataLength,0);
					}
					else
					{
						int ret = sendto(pSessionMgr->m_RtpSocket,info.pData,info.nDataLength,0,
							(struct sockaddr*)&pSessionMgr->m_sockRecvAddr,sizeof(pSessionMgr->m_sockRecvAddr));
					}

					if(info.pData)
					{
						delete info.pData;
						info.pData = NULL;
					}
				}
			}

			pSessionMgr->DeleteSendInfo();
		}
		else
			Sleep(1);
	}

	pSessionMgr->ClearSendInfo();
	pSessionMgr->SetThreadRun(FALSE);
	/*for(int i = 0 ; i < 10 ; i++)
		SendLog(5,_T("[SessionMgr] thread finish"));*/
	return FALSE;
}