#pragma once
#include "ESMMovieData.h"
#include "ESMMovieInfoArray.h"

class ESMMovieObject		//ESMMovieObject
{
public:
	ESMMovieObject();
	ESMMovieObject(CString strMovieName);
	~ESMMovieObject(void);

	void ClearMovie();
	void SetDscCount(int nDscCount);
	int SetMovieFile(CString strMovieId, CString strMoviePath);
	CString GetMovieFile(int nMovieIndex);
	int GetMovieCount(){ return m_nDscCount; } 
	ESMMovieData* GetMovieData(int nIndex);
	ESMMovieData* GetMovieData(CString strMovieID);
	stAdjustInfo GetAdjustData(int nIndex);
	void SetAdjustData(int nIndex, stAdjustInfo* AdjustData);
	void SetMovieInfo(ESMMovieInfoArray *pMovieInfoArray);
	CString GetMoiveName();

private:
	ESMMovieData** m_arrMovieData;
	int m_nDscCount;		//MovieCount
	CString m_strMovieName;
};

