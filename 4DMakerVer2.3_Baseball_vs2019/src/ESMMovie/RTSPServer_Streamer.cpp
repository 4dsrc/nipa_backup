#include "stdafx.h"
#include "RTSPServer_Streamer.h"

CRTSPImageStreamer::CRTSPImageStreamer(SOCKET sock,CRTSPServerSessionMgr* pSessionMgr,std::map<RTSP_PLAY_METHOD,CRTSPServerImageSender*> mpImageSender,BOOL bMulticast /*= FALSE*/)
{
	m_ClinetSock = sock;
	m_pSessionMgr = pSessionMgr;
	m_ArrImageSender = mpImageSender;
	m_bThreadStop = TRUE;
	//InitializeCriticalSection(&m_criSend);
	m_bThreadRun = FALSE;
	m_bMulticast = bMulticast;
	SetUseMulticast(m_bMulticast);

	InitializeCriticalSection(&m_criMulticast);
	LaunchImageStreamer();
	//m_pFile = fopen("M:\\temp.txt","w");
}
CRTSPImageStreamer::CRTSPImageStreamer(CRTSPServerImageSender* pImageSender,RTSP_PLAY_METHOD method)
{
	m_bThreadStop = TRUE;
	InitializeCriticalSection(&m_criMulticast);
	m_RTSPPlayMethod = method;

	SetUseMulticast(TRUE);
	m_pImageSender = pImageSender;
	LaunchMulticastStreamer();
}
CRTSPImageStreamer::~CRTSPImageStreamer()
{
	//DeleteCriticalSection(&m_criSend);
	m_bThreadStop = FALSE;
	while(1)
	{
		if(GetThreadRun() == FALSE)
			break;

		Sleep(1);
	}
	DeleteCriticalSection(&m_criMulticast);
	/*for(int i = 0 ; i < 10 ; i++)
		SendLog(5,_T("[CRTSPImageStreamer] Destructor"));*/
	//fclose(m_pFile);
}
void CRTSPImageStreamer::LaunchImageStreamer()
{
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_ImageStreamer,(void*)this,0,NULL);
	SetThreadPriority(hSyncTime,THREAD_PRIORITY_HIGHEST);
	CloseHandle(hSyncTime);

	/*hSyncTime = (HANDLE)_beginthreadex(NULL,0,_ImageSender,(void*)this,0,NULL);
	CloseHandle(hSyncTime);*/
}
unsigned WINAPI CRTSPImageStreamer::_ImageStreamer(LPVOID param)
{
	CRTSPImageStreamer* pStreamer = (CRTSPImageStreamer*) param;
	CRTSPServerImageSender* pSender = NULL;
	CRTSPServerSessionMgr* pSessionMgr = pStreamer->m_pSessionMgr;

	pStreamer->SetThreadRun(TRUE);
	RTSP_PLAY_METHOD method = PLAY_NONE;
	BOOL bPlay = FALSE;
	int nSendTime = 0;
	while(pStreamer->GetThreadStop())
	{
		if(pSessionMgr->GetPlayMethod() != PLAY_NONE)
		{
			if(pSessionMgr->GetPlay() == TRUE)
			{
				bPlay = pSessionMgr->GetPlay();
				method = pSessionMgr->GetPlayMethod();
				break;
			}
		}
		Sleep(1);
	}

	pSender = pStreamer->m_ArrImageSender[method];

	int nLast,nCur;

	int nArrFrameCnt[3] = {33,33,34};
	int nCnt = 0;
	int nStart = GetTickCount();
	
	if(pStreamer->GetThreadStop())
	{
		nLast = pSender->GetLastIdx();
		nCur  = pSender->GetCurIdx();
	}

	while(pStreamer->GetThreadStop())
	{
		if(pSessionMgr->GetPlay()/*pSessionMgr->GetCmdType() == RTSP_PLAY*/)
		{
#if 0
			if(GetTickCount() >= nStart + nArrFrameCnt[nCnt])
			{
				int nRecvCurIdx = pSender->GetCurIdx();
				
				nCur = pSender->GetCurIdx();
				nLast = pSender->GetLastIdx() + 1;
				int nSecIdx = -1,nFrameIdx = -1;
				CString strLog;
				if(nLast > nRecvCurIdx/*pSender->GetCurIdx()*/)
				{
					for(int i = nLast ; i < RTSP_PACKET_MAX ; i++)
					{
						strLog.Format(_T("[%d] - %d"),i,pStreamer->SendPacket(pSender,i,&nSecIdx,&nFrameIdx));
						TRACE(strLog);
						//SendLog(5,strLog);
						//nSize += pStreamer->SendPacket(pSender,i,&nSecIdx,&nFrameIdx);
					}

					for(int i = 0 ; i <= nCur ; i++)
					{
						strLog.Format(_T("[%d] - %d"),i,pStreamer->SendPacket(pSender,i,&nSecIdx,&nFrameIdx));
						TRACE(strLog);
						//SendLog(5,strLog);
						//nSize += pStreamer->SendPacket(pSender,i,&nSecIdx,&nFrameIdx);
					}
				}
				else
				{
					for(int i = nLast ; i <= nCur ; i++)
					{
						strLog.Format(_T("[%d] - %d\n"),i,pStreamer->SendPacket(pSender,i,&nSecIdx,&nFrameIdx));
						TRACE(strLog);
						//SendLog(5,strLog);
						//nSize += pStreamer->SendPacket(pSender,i,&nSecIdx,&nFrameIdx);
					}
				}
				strLog.Format(_T("[%d][%d] Send(%d ~ %d)"),nSecIdx,nFrameIdx,nLast,nCur);
				SendLog(5,strLog);

				nStart = GetTickCount();
				nCnt++;
				if(nCnt == 3)
					nCnt = 0;
			}
#else
			int nRecvCurIdx = pSender->GetCurIdx();
			if(nCur != nRecvCurIdx/*pSender->GetCurIdx()*/)
			{
				//SendLog(5,_T("################################################"));
				//TRACE("################################################\n");
				int nStart = GetTickCount();
				int nSize = 0;
				nCur = pSender->GetCurIdx();
				nLast = pSender->GetLastIdx() + 1;
				int nSecIdx = -1,nFrameIdx = -1;
				CString strLog;
				if(nLast > nRecvCurIdx/*pSender->GetCurIdx()*/)
				{
					for(int i = nLast ; i < RTSP_PACKET_MAX ; i++)
					{
						strLog.Format(_T("[%d] - %d"),i,pStreamer->SendPacket(pSender,i,&nSecIdx,&nFrameIdx));
						//TRACE(strLog);
						//SendLog(5,strLog);
						//nSize += pStreamer->SendPacket(pSender,i,&nSecIdx,&nFrameIdx);
					}

					for(int i = 0 ; i <= nCur ; i++)
					{
						strLog.Format(_T("[%d] - %d"),i,pStreamer->SendPacket(pSender,i,&nSecIdx,&nFrameIdx));
						//TRACE(strLog);
						//SendLog(5,strLog);
						//nSize += pStreamer->SendPacket(pSender,i,&nSecIdx,&nFrameIdx);
					}
				}
				else
				{
					for(int i = nLast ; i <= nCur ; i++)
					{
						strLog.Format(_T("[%d] - %d\n"),i,pStreamer->SendPacket(pSender,i,&nSecIdx,&nFrameIdx));
						//TRACE(strLog);
						//SendLog(5,strLog);
						//nSize += pStreamer->SendPacket(pSender,i,&nSecIdx,&nFrameIdx);
					}
				}
				strLog.Format(_T("[%d][%d] Send\t%d\t%d"),nSecIdx,nFrameIdx,nLast,nCur);
				//SendLog(5,strLog);

				strLog.Format(_T("[%d][%d]Proc: %d - %d"),
					nSecIdx,nFrameIdx,nLast,nCur);
				nSendTime = GetTickCount();
				//SendLog(5,strLog);
				/*CString strLog;
				strLog.Format(_T("Send - %d to %d"),nLast,nCur);
				SendLog(5,strLog);*/
			}
			else
				Sleep(1);
#endif
		}
		else
			Sleep(1);//pStreamer->SetWait(1);
	}
	pStreamer->SetThreadRun(FALSE);

	/*for(int i = 0 ; i < 10 ; i++)
		SendLog(5,_T("[Streamer] thread finish"));*/
	return FALSE;
}
unsigned WINAPI CRTSPImageStreamer::_ImageSender(LPVOID param)
{
	/*CRTSPImageStreamer* pStreamer = (CRTSPImageStreamer*) param;
	CRTSPServerSessionMgr* pSessionMgr = pStreamer->m_pSessionMgr;
	CRTSPServerImageSender* pSender = NULL;
	SOCKET soc = pStreamer->m_ClinetSock;

	BOOL bPlay = FALSE;
	RTSP_PLAY_METHOD method = PLAY_NONE;

	while(pStreamer->GetThreadStop())
	{
		if(pSessionMgr->GetPlayMethod() != PLAY_NONE)
		{
			if(pSessionMgr->GetPlay() == TRUE)
			{
				bPlay = pSessionMgr->GetPlay();
				method = pSessionMgr->GetPlayMethod();
				break;
			}
		}
		Sleep(1);
	}

	pSender = pStreamer->m_ArrImageSender[method];

	while(pStreamer->GetThreadStop())
	{
		if(pStreamer->GetSendDataSize() > 0 )
		{
			RTSP_PACKETIZE_INFO* pPak = pStreamer->GetSendData();
			int ret = send(soc,(char*)pPak->ArrData,pPak->nSize,0);

			pStreamer->DeleteSendData();
		}
	}*/

	return FALSE;
}
int CRTSPImageStreamer::SendPacket(CRTSPServerImageSender*pSender,int nIdx,int*pSecIdx,int*pFrameIdx)
{
	RTSP_PACKETIZE_INFO* pak = &pSender->GetPacket(nIdx);
	/*CString strLog,strTemp;
	strTemp.Format(_T("[%d][%d]"),pak->nSecIdx,pak->nFrameIdx);
	strLog.Append(strTemp);
	for(int i = 4 ; i < 8 ; i++)
	{
	strTemp.Format(_T("%02X-"),pak->ArrData[i]&0xFF);
	strLog.Append(strTemp);
	}
	strTemp.Format(_T("(%d)"),pak->nSize);
	strLog.Append(strTemp);
	SendLog(5,strLog);*/

#ifdef RTSP_TEST_MODE
	RTSP_SEND_INFO info;
	info.nDataLength = pak->nSize;
	info.pData = new char[pak->nSize];
	memset(info.pData,0x00,info.nDataLength);
	memcpy(info.pData,pak->ArrData,sizeof(char)*pak->nSize);

	m_pSessionMgr->AddSendInfo(info);
#else
	int ret = send(m_ClinetSock,(char*)pak->ArrData,pak->nSize,0);

	if(ret != pak->nSize)
	{
		CString strLog;
		strLog.Format(_T("Send - %d Loss"),pak->nSize - ret);
		SendLog(5,strLog);
	}
#endif
	*pSecIdx = pak->nSecIdx;
	*pFrameIdx	= pak->nFrameIdx;
	//fprintf(m_pFile,"[%d] - SEND(%d-%d)\n",GetTickCount(),pak->nSecIdx,pak->nFrameIdx);

	return pak->nSize;
	/*if(ret != pak->nSize)
	{
	CString strLog;
	strLog.Format(_T("Send - %d Loss"),pak->nSize - ret);
	SendLog(5,strLog);

	int nMissPacket = pak->nSize - ret;
	int ret = send(m_ClinetSock,(char*)pak->ArrData+ret,nMissPacket,0);
	}*/
}
void CRTSPImageStreamer::SetWait(DWORD dwMillisec)
{
	MSG msg;
	DWORD dwStart;
	dwStart = GetTickCount();
	while(GetTickCount() - dwStart < dwMillisec)
	{
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}
void CRTSPImageStreamer::LaunchMulticastStreamer()
{
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_MulticastStreamer,(void*)this,0,NULL);
	SetThreadPriority(hSyncTime,THREAD_PRIORITY_HIGHEST);
	CloseHandle(hSyncTime);
}
unsigned WINAPI CRTSPImageStreamer::_MulticastStreamer(LPVOID param)
{
	CRTSPImageStreamer* pStreamer = (CRTSPImageStreamer*) param;
	CRTSPServerImageSender* pSender = pStreamer->m_pImageSender;
	RTSP_PLAY_METHOD method = pStreamer->m_RTSPPlayMethod;
	CString strLog;

	pStreamer->SetThreadRun(TRUE);
	
	int nLast,nCur;

	int nCnt = 0;
	int nStart = GetTickCount();
	
	if(pStreamer->GetThreadStop())
	{
		nLast = pSender->GetLastIdx();
		nCur  = pSender->GetCurIdx();
	}
	strLog.Format(_T("[MulticastStramer][%d] SendPacket Start(%d-%d)"),(int)method,nLast,nCur);
	SendLog(5,strLog);

	while(pStreamer->GetThreadStop())
	{
		int nRecvCurIdx = pSender->GetCurIdx();
		/*strLog.Format(_T("[%d] Current: %d, Received: %d"),(int)method,nCur,nRecvCurIdx);
		SendLog(5,strLog);*/

		if(nCur != nRecvCurIdx)
		{
			//TRACE("################################################\n");
			int nStart = GetTickCount();
			int nSize = 0;
			nCur = pSender->GetCurIdx();
			nLast = pSender->GetLastIdx() + 1;
			int nSecIdx = -1,nFrameIdx = -1;

			if(nLast > nRecvCurIdx)
			{
				for(int i = nLast ; i < RTSP_PACKET_MAX ; i++)
				{
					pStreamer->SendMulticastPacket(pSender,i,&nSecIdx,&nFrameIdx);
				}

				for(int i = 0 ; i <= nCur ; i++)
				{
					pStreamer->SendMulticastPacket(pSender,i,&nSecIdx,&nFrameIdx);
				}
			}
			else
			{
				for(int i = nLast ; i <= nCur ; i++)
				{
					pStreamer->SendMulticastPacket(pSender,i,&nSecIdx,&nFrameIdx);
				}
			}
			strLog.Format(_T("[%d][%d] Send\t%d\t%d"),nSecIdx,nFrameIdx,nLast,nCur);
			//SendLog(5,strLog);
		}
		else
			Sleep(1);
	}
	pStreamer->SetThreadRun(FALSE);

	/*for(int i = 0 ; i < 10 ; i++)
		SendLog(5,_T("[Streamer] thread finish"));*/
	return FALSE;
}
int CRTSPImageStreamer::SendMulticastPacket(CRTSPServerImageSender*pSender,int nIdx,int*pSecIdx,int*pFrameIdx)
{
	RTSP_PACKETIZE_INFO* pak = &pSender->GetPacket(nIdx);

	RTSP_SEND_INFO info;
	info.nDataLength = pak->nSize;
	info.pData = new char[pak->nSize];
	memcpy(info.pData,pak->ArrData,sizeof(char)*pak->nSize);

	AddMultiSendInfo(info);

	*pSecIdx = pak->nSecIdx;
	*pFrameIdx	= pak->nFrameIdx;
	//fprintf(m_pFile,"[%d] - SEND(%d-%d)\n",GetTickCount(),pak->nSecIdx,pak->nFrameIdx);

	return pak->nSize;
}