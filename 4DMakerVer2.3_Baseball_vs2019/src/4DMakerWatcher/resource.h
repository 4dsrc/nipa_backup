//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 4DMakerWatcher.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_MY4DMAKERWATCHER_FORM       101
#define IDR_MAINFRAME                   128
#define IDR_My4DMakerWatcheTYPE         130
#define IDR_MENU_TRY                    312
#define IDC_BTN_CAMERAOFF               1000
#define IDC_BTN_CAMERAON                1001
#define IDC_BTN_TEST                    1002
#define IDC_STATIC_VERSION              1006
#define ID_MENU_OPEN                    32773
#define ID_MENU_FILE_EXIT               32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        310
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
