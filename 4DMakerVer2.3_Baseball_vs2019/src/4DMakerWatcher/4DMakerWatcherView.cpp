
// 4DMakerWatcherView.cpp : CMy4DMakerWatcherView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "4DMakerWatcher.h"
#endif

#include "4DMakerWatcherDoc.h"
#include "4DMakerWatcherView.h"

#include <Dbt.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMy4DMakerWatcherView

IMPLEMENT_DYNCREATE(CMy4DMakerWatcherView, CFormView)

BEGIN_MESSAGE_MAP(CMy4DMakerWatcherView, CFormView)
	ON_COMMAND(ID_MENU_OPEN, &CMy4DMakerWatcherView::OnMenuOpen)
	ON_COMMAND(ID_MENU_FILE_EXIT, &CMy4DMakerWatcherView::OnMenuClose)
	ON_BN_CLICKED(IDC_BTN_CAMERAON, &CMy4DMakerWatcherView::OnBnClickedButtonOn)
	ON_BN_CLICKED(IDC_BTN_CAMERAOFF, &CMy4DMakerWatcherView::OnBnClickedButtonOff)
	ON_BN_CLICKED(IDC_BTN_TEST, &CMy4DMakerWatcherView::OnBnClickedButton4)
	ON_WM_TIMER()
	ON_MESSAGE(WM_DEVICECHANGE, OnMyDeviceChange)
END_MESSAGE_MAP()
// CMy4DMakerWatcherView 생성/소멸

CMy4DMakerWatcherView::CMy4DMakerWatcherView()
	: CFormView(CMy4DMakerWatcherView::IDD)
{
	m_bUSBCheck = FALSE;
	
}

CMy4DMakerWatcherView::~CMy4DMakerWatcherView()
{
}

void CMy4DMakerWatcherView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_VERSION, m_ctrlVersion);
}

BOOL CMy4DMakerWatcherView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void CMy4DMakerWatcherView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();
	m_WatcherManager.CheckModel();
	//SetTimer(TIMER_USB_CHECK, 1000, NULL);
	m_ctrlVersion.SetWindowText(_T("4DWatcher v1.3"));		
	//jhhan 180421 
	//v1.2 : 로그 기능 추가
	//v1.3 : 4DA 2.0, 3.0 별도 로직 수행

	AfxGetMainWnd()->PostMessage(UM_TRAYHIDE);
}

#include "MainFrm.h"
void CMy4DMakerWatcherView::OnMenuOpen()
{
	((CMainFrame*)AfxGetMainWnd())->ShowWindow(SW_SHOW);
	((CMainFrame*)AfxGetMainWnd())->SetForegroundWindow();
}	

void CMy4DMakerWatcherView::OnMenuClose()
{
	if (AfxMessageBox(_T("4DMaker Watcher을 종료합니다."), MB_OKCANCEL | MB_ICONQUESTION) != IDOK)
		return;

	((CMainFrame*)AfxGetMainWnd())->m_bExit = TRUE;

	AfxGetMainWnd()->SendMessage(WM_CLOSE);

}

// CMy4DMakerWatcherView 진단

#ifdef _DEBUG
void CMy4DMakerWatcherView::AssertValid() const
{
	CFormView::AssertValid();
}

void CMy4DMakerWatcherView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CMy4DMakerWatcherDoc* CMy4DMakerWatcherView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMy4DMakerWatcherDoc)));
	return (CMy4DMakerWatcherDoc*)m_pDocument;
}
#endif //_DEBUG


// CMy4DMakerWatcherView 메시지 처리기

void CMy4DMakerWatcherView::OnBnClickedButtonOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_WatcherManager.ONPower();
}


void CMy4DMakerWatcherView::OnBnClickedButtonOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_WatcherManager.OFFPower();
}


void CMy4DMakerWatcherView::OnBnClickedButton4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strPortName;
	m_WatcherManager.GetPortName(strPortName);
	m_WatcherManager.CheckRegistyStartProgram();
}

void CMy4DMakerWatcherView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	switch(nIDEvent)
	{
	case TIMER_USB_CHECK:
		{
			if(m_WatcherManager.GetCheckUSB())
			{
				if(m_bUSBCheck == FALSE)
				{
					m_WatcherManager.ONPower();
					m_bUSBCheck = TRUE;
				}
			}
			else
			{
				if(m_bUSBCheck == TRUE)
				{
					m_WatcherManager.OFFPower();
					m_bUSBCheck = FALSE;
				}
			}
		}
		break;
	default:
		break;
	}

	CFormView::OnTimer(nIDEvent);
}

LRESULT CMy4DMakerWatcherView::OnMyDeviceChange(WPARAM wParam, LPARAM lParam)
{
	if ( DBT_DEVICEARRIVAL == wParam || DBT_DEVICEREMOVECOMPLETE == wParam ) {
		PDEV_BROADCAST_HDR pHdr = (PDEV_BROADCAST_HDR)lParam;
		PDEV_BROADCAST_DEVICEINTERFACE pDevInf;
		PDEV_BROADCAST_HANDLE pDevHnd;
		PDEV_BROADCAST_OEM pDevOem;
		PDEV_BROADCAST_PORT pDevPort;
		PDEV_BROADCAST_VOLUME pDevVolume;
		switch( pHdr->dbch_devicetype ) {
		case DBT_DEVTYP_DEVICEINTERFACE:
			pDevInf = (PDEV_BROADCAST_DEVICEINTERFACE)pHdr;
			CheckUSB();
			break;

		case DBT_DEVTYP_HANDLE:
			pDevHnd = (PDEV_BROADCAST_HANDLE)pHdr;
			break;

		case DBT_DEVTYP_OEM:
			pDevOem = (PDEV_BROADCAST_OEM)pHdr;
			break;

		case DBT_DEVTYP_PORT:
			pDevPort = (PDEV_BROADCAST_PORT)pHdr;
			break;

		case DBT_DEVTYP_VOLUME:
			pDevVolume = (PDEV_BROADCAST_VOLUME)pHdr;
			break;
		}
	}
	return 0;
}

void CMy4DMakerWatcherView::CheckUSB()
{
	if(m_WatcherManager.GetCheckUSB())
	{
		if(m_bUSBCheck == FALSE)
		{
			m_WatcherManager.ONPower();
			m_bUSBCheck = TRUE;
		}
	}
	else
	{
		if(m_bUSBCheck == TRUE)
		{
			m_WatcherManager.OFFPower();
			m_bUSBCheck = FALSE;
		}
	}
}