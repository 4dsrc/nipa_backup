#pragma once

#include "MAServerManager.h"
#include "CommThread.h"

enum USBCONTROLERTYPE
{	USB_CONTROLER_KOREA,
	USB_CONTROLER_CHINA,
	USB_CONTROLER_V30
};

class WatcherManager
{
public:
	WatcherManager(void);
	~WatcherManager(void);

	MAServerManager* m_ServerSocket;
	
	int m_bFileDownMode;
	int m_nFileTotalSize;
	CString m_strFileName;
	int m_nReceiveSize;
	CFile* m_WriteFile;	
	int m_nTotalReceiveSize;
	int m_nControlerType;
	void ReciveData(char* pBup, int nBufSize);
	BOOL ExecuteProcess(CString strAppPath, CString strAppName, CString strParam = _T(""));
	BOOL IsExistProcess(CString strProcess);
	BOOL KillProcess(CString strProcess);
	BOOL FileDownLoadStart(char* pBuf, CString* strFileName, int* nFileSize);
	BOOL FileDownLoadStart(char* pBuf, CString* strFileName, int* nFileSize, CString* strFilePath);
	BOOL FileDownLoad(char* buf, int nBufSize);
	BOOL SendCommPort(char* buf);
	BOOL ONPower();
	BOOL OFFPower();
	BOOL CheckPower();
	BOOL OpenCommPort();
	BOOL CloseCommPort();
	BOOL GetIsFile(CString strAppPath, CString strAppName);
	BOOL GetPortName(CString& strPort);
	BOOL SetRegistyStartProgram(BOOL bAutoExec, CString strValueName, CString strExeFileName);
	BOOL CheckRegistyStartProgram();
	BOOL SystemShutdown();
	BOOL SystemReBoot();
	void CheckModel();

	CString m_strSerialName;
	CString m_strFilePath;

	// 2016-12-19 yongmin
	// 4DMaker response Check
	int CheckProc();
	BOOL GetCheckUSB();

	//171130 wgkim	
	BOOL CheckExistBackupDrive();
	int CheckBackupProgram();
	void CreateBackupFolder();
	int CheckBackupFolderCurrentFileCount(const char * parm_search_path);
	int CheckBackupFolderCurrentSize(CString Path);
	int m_nFileCount;	

	CCommThread m_pSerial;
};

