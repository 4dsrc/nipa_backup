
// 4DMakerWatcherView.h : CMy4DMakerWatcherView 클래스의 인터페이스
//

#pragma once

#include "resource.h"
#include "WatcherManager.h"
#include "afxwin.h"

#define TIMER_USB_CHECK	WM_USER+0x0010

class CMy4DMakerWatcherView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	CMy4DMakerWatcherView();
	DECLARE_DYNCREATE(CMy4DMakerWatcherView)

public:
	enum{ IDD = IDD_MY4DMAKERWATCHER_FORM };

// 특성입니다.
public:
	CMy4DMakerWatcherDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

	afx_msg void OnMenuOpen();
	afx_msg void OnMenuClose();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CMy4DMakerWatcherView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	WatcherManager m_WatcherManager;
	BOOL m_bUSBCheck;

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonOn();
	afx_msg void OnBnClickedButtonOff();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CStatic m_ctrlVersion;
	afx_msg LRESULT OnMyDeviceChange(WPARAM wParam, LPARAM lParam);
	void CheckUSB();
};

#ifndef _DEBUG  // 4DMakerWatcherView.cpp의 디버그 버전
inline CMy4DMakerWatcherDoc* CMy4DMakerWatcherView::GetDocument() const
   { return reinterpret_cast<CMy4DMakerWatcherDoc*>(m_pDocument); }
#endif

