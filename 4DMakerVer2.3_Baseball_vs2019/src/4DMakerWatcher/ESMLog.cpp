#include "stdafx.h"
#include "MainFrm.h"
#include "ESMLog.h"


CMainFrame* g_pESMMainWnd = NULL;

void ESMRegist(CWnd * ptr)
{
	//-- GET MAINFRAME POINT
	g_pESMMainWnd = (CMainFrame*)ptr;
}
void ESMLog(int verbosity, LPCTSTR lpszFormat, ...)
{	
	if(NULL == lpszFormat)	
		return;

	TCHAR pszMsg[ESMLOG_LINE_SIZE] = {0};

	va_list args;
	va_start(args, lpszFormat);
	_vstprintf(pszMsg, lpszFormat, args);
	va_end(args);

	int nLength = (int)_tcslen(pszMsg);
	if(!nLength)
		return;

	CString strMsg;


	if(g_pESMMainWnd)
	{		
		if(nLength > ESMLOG_LINE_SIZE)
			strMsg.Format(_T("[Too Long Message]"), pszMsg);
		else
			strMsg.Format(_T("[%d] %s"),GetTickCount(), pszMsg );
		g_pESMMainWnd->PrintLog(verbosity, strMsg);		
	}	
}

void ESMCreateAllDirectories(CString csPath)
{
	::PathRemoveBackslash(csPath.GetBuffer(_MAX_PATH));
	csPath.ReleaseBuffer(_MAX_PATH);

	if(::PathFileExists(csPath))
		return;
	int nFound = csPath.ReverseFind(_T('\\'));
	ESMCreateAllDirectories(csPath.Left(nFound));

	CreateDirectory(csPath, NULL);
}

CString ESMGetDate()
{
	CString strDate = _T("");

	CTime time = CTime::GetCurrentTime();
	//time = CTime::GetCurrentTime();
	int nYear = time.GetYear();
	int nMonth = time.GetMonth();
	int nDay = time.GetDay();

	strDate.Format(_T("%04d-%02d-%02d"), nYear, nMonth, nDay);
	return strDate;
}