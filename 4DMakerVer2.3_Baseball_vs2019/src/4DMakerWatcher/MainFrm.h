
// MainFrm.h : CMainFrame 클래스의 인터페이스
//

#pragma once


#include "LogMgr.h"

class CMainFrame : public CFrameWnd
{
	
protected: // serialization에서만 만들어집니다.
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// 특성입니다.
public:

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// 구현입니다.
public:
	virtual ~CMainFrame();
	BOOL		m_bExit;
	UINT		m_uShellRestart;
	void GotoTray(void);
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 컨트롤 모음이 포함된 멤버입니다.
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
// 생성된 메시지 맵 함수
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg LRESULT OnTray(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
	afx_msg LRESULT TrayIconHide(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnClose();
	LogMgr* m_pLogManager;
	void PrintLog(int nVerbosity, CString strLog);
};


