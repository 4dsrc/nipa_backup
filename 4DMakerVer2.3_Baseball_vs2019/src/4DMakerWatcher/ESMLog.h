#define	ESMLOG_LINE_SIZE		4096

extern void ESMRegist(CWnd * prt);
extern void ESMLog (int nVerbosity, LPCTSTR lpszFormat, ...);
extern void ESMCreateAllDirectories(CString csPath);
extern CString ESMGetDate();