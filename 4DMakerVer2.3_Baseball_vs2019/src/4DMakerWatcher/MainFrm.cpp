
// MainFrm.cpp : CMainFrame 클래스의 구현
//

#include "stdafx.h"
#include "4DMakerWatcher.h"

#include "MainFrm.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_MESSAGE(UM_TRAY, &CMainFrame::OnTray)
	ON_WM_CLOSE()
	ON_MESSAGE(UM_TRAYHIDE, &CMainFrame::TrayIconHide)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 상태 줄 표시기
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame 생성/소멸

CMainFrame::CMainFrame()
	:
m_pLogManager	(NULL)
{
	m_uShellRestart = ::RegisterWindowMessage(__TEXT("4DMakerTrayCreated"));
	m_bExit = FALSE;
}

CMainFrame::~CMainFrame()
{
	if(m_pLogManager)
	{
		delete m_pLogManager;
		m_pLogManager = NULL;
	}	
}

LRESULT CMainFrame::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if(message == m_uShellRestart)		GotoTray();
	return CFrameWnd::WindowProc(message, wParam, lParam);
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	ESMRegist(this);

	if(m_pLogManager)
	{
		delete m_pLogManager;
		m_pLogManager= NULL;
	}
	CString strLogName;
	strLogName.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\log\\%s"), ESMGetDate());

	m_pLogManager = new LogMgr(	strLogName, 
		_T("4DWatcher.log"), 
		5, 
		5);

	if(m_pLogManager)
	{
		m_pLogManager->Start();
		ESMLog(1, _T("[ESM] Create Log \"%s\""), strLogName);	
	}

	//GotoTray();

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if(cs.hMenu != NULL)
	{
		::DestroyMenu(cs.hMenu);
		cs.hMenu = NULL;
	}
	cs.style = cs.style&(~FWS_ADDTOTITLE);

	return CFrameWnd::PreCreateWindow(cs);
}

// CMainFrame 진단

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG


void CMainFrame::GotoTray(void)
{
	NOTIFYICONDATA nid;
	::ZeroMemory(&nid, sizeof(nid));
	nid.cbSize				= sizeof(nid);
	nid.hWnd				= m_hWnd;
	nid.uID					= IDR_MAINFRAME;
	nid.uFlags				= NIF_MESSAGE | NIF_ICON | NIF_TIP;
	nid.uCallbackMessage	= UM_TRAY;
	nid.hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	wsprintf(nid.szTip, _T("%s"), _T("4DMakerWatcher"));

	::Shell_NotifyIcon(NIM_ADD, &nid);

	SendMessage(WM_SETICON, (WPARAM)TRUE, (LPARAM)nid.hIcon);
	ShowWindow(SW_HIDE);
}

LRESULT CMainFrame::OnTray(WPARAM wParam, LPARAM lParam)
{
	if(lParam == WM_RBUTTONDOWN)
	{
		SetCapture();
	}
	else if(lParam == WM_RBUTTONUP)
	{
		CMenu menu, *pMenu = NULL;
		CPoint pt;

		menu.LoadMenu(IDR_MENU_TRY);
		pMenu = menu.GetSubMenu(0);

		GetCursorPos(&pt);
		SetForegroundWindow();
		pMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON, pt.x, pt.y, this);
		::PostMessage(m_hWnd, WM_NULL, 0, 0);

		ReleaseCapture();
	}
	else if(lParam == WM_LBUTTONDBLCLK)
	{
		ShowWindow(SW_SHOW);
		SetForegroundWindow();
	}
	return 0;
}

void CMainFrame::OnClose()
{
	if( m_bExit == FALSE)
	{
		ShowWindow(HIDE_WINDOW);
		return;
	}

	CFrameWnd::OnClose();
}

void CMainFrame::PrintLog(int nVerbosity, CString strLog)
{
	if(!strLog.GetLength())
		return;
	if(!m_pLogManager)
		return;

	if(nVerbosity)
		m_pLogManager->OutputLog(nVerbosity, strLog);
	else
		m_pLogManager->OutputErrLog(strLog);
}
LRESULT CMainFrame::TrayIconHide(WPARAM wParam, LPARAM lParam)
{
	GotoTray();

	return 0;
}