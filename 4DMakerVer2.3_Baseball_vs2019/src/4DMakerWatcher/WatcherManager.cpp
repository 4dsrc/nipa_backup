﻿#include "StdAfx.h"
#include "WatcherManager.h"
#include <tlhelp32.h>
//#include "CommThread.h"
#include <windows.h>
#include <setupapi.h>

#include "ESMUtil.h"

#include <strsafe.h>
#include <setupapi.h>
#pragma comment (lib , "setupapi.lib" )
#define SDI_VID_KEYLOCK	_T("090c")

WatcherManager::WatcherManager(void)
{
	m_bFileDownMode = FALSE;
	m_WriteFile = NULL;	
	m_nFileTotalSize = 0;
	m_nReceiveSize = 0;
	m_ServerSocket = NULL;
	m_nTotalReceiveSize = 0;
	m_nControlerType = USB_CONTROLER_KOREA;
	m_ServerSocket = new MAServerManager();
	if( m_ServerSocket != NULL)
		m_ServerSocket->SetParent((void*)this);	

//#ifndef _DEBUG
	if ( CheckRegistyStartProgram() == FALSE )
	{
		TCHAR chrFileName[500];
		GetModuleFileName(NULL, chrFileName, MAX_PATH);
		CString strFileName;
		strFileName.Format(_T("\"%s\""), chrFileName);

		SetRegistyStartProgram(TRUE, REG_NAME, strFileName);
	}
//#endif		
}

WatcherManager::~WatcherManager(void)
{
	//AfxMessageBox(_T("CLOSE!"));
	m_pSerial.ClosePort();

	if( m_ServerSocket != NULL)
	{	
		delete m_ServerSocket;
		m_ServerSocket = NULL;
	}
}


BOOL WatcherManager::GetPortName(CString& strPort)
{
	HKEY  hSerialCom;
	TCHAR buffer[_MAX_PATH], data[_MAX_PATH];
	DWORD len, type, dataSize;
	long  i;

	//CMiLRe 20160202 Watcher 레지스트리 읽기 옵션 변경
	if (::RegOpenKeyEx(HKEY_LOCAL_MACHINE, REG_ADDRESS_SERIAL, 0, KEY_QUERY_VALUE, &hSerialCom) == ERROR_SUCCESS)
	{
		for (i=0, len=dataSize=_MAX_PATH; ::RegEnumValue(hSerialCom, i, buffer, &len, NULL, &type, (unsigned char*)data, &dataSize)==ERROR_SUCCESS; i++, len=dataSize=_MAX_PATH)
		{
			//CString strSerialName =(LPCTSTR)buffer;
			m_strSerialName =(LPCTSTR)buffer;
			
			if(m_strSerialName == SERIAL_PORT_NAME || m_strSerialName == SERIAL_PORT_NAME_4DA3)
			{
				strPort = (LPCTSTR)data;
				return TRUE;
			}
		}
		
		::RegCloseKey(hSerialCom);
	}

	return FALSE;
}

BOOL WatcherManager::CheckPower()
{
	//CCommThread pSerial;
	if( m_nControlerType == USB_CONTROLER_KOREA)
	{
		DWORD nRead = 1;
		/*
		CString strCOM;
		if (GetPortName(strCOM) == FALSE)
			return FALSE;

		if( !pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
			return FALSE;
		*/
		char pBuf[8];
		memset(pBuf,0,8);
		while (nRead)
		{
			//nRead = pSerial.ReadComm((BYTE *)&pBuf ,8);
			nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,8);
		}

		pBuf[0] = 0x55;
		pBuf[1] = 0x01;
		pBuf[2] = 0x00;
		pBuf[3] = 0x01;
		pBuf[4] = 0x00;
		pBuf[5] = 0x00;
		pBuf[6] = 0x00;
		pBuf[7] = 0x57;

		//pSerial.WriteComm((BYTE *)&pBuf,8);
		m_pSerial.WriteComm((BYTE *)&pBuf,8);

		Sleep(200);
		//nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,4);
		nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,4);
		//pSerial.ClosePort();

		if ( pBuf[3] == 0x02 )
			return TRUE;
		else
			return FALSE;
	}
	else if( m_nControlerType == USB_CONTROLER_CHINA )
	{
		DWORD nRead = 1;
		/*
		CString strCOM;
		if (GetPortName(strCOM) == FALSE)
			return FALSE;
		
		if( !pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
			return FALSE;
		*/
		char pBuf[17];		  
		memset(pBuf,0,17);		  
		while (nRead)
		{
		  	//nRead = pSerial.ReadComm((BYTE *)&pBuf ,8);
			nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,8);
		}
		  
		// OUT1 ON
		pBuf[0] = 0x3A;
		pBuf[1] = 0x46;
		pBuf[2] = 0x45;
		pBuf[3] = 0x30;
		pBuf[4] = 0x31;
		pBuf[5] = 0x30;
		pBuf[6] = 0x30;
		  
		pBuf[7] = 0x30;
		pBuf[8] = 0x30;
		pBuf[9] = 0x30;
		pBuf[10] = 0x30;
		pBuf[11] = 0x30;
		  
		pBuf[12] = 0x31;
		pBuf[13] = 0x30;
		pBuf[14] = 0x30;
		pBuf[15] = 0x0D;
		pBuf[16] = 0x0A;
		  
		//pSerial.WriteComm((BYTE *)&pBuf,17);
		m_pSerial.WriteComm((BYTE *)&pBuf,17);
		  
		Sleep(200);
		//nRead = pSerial.ReadComm((BYTE *)&pBuf ,13);
		nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,13);
		//pSerial.ClosePort();
		  
		if ( pBuf[8] == 0x31 )
		  	return TRUE;
		else
		  	return FALSE;
	}
}

BOOL WatcherManager::ONPower()
{
	DWORD dwRet = 0;
	//CCommThread pSerial;
	if( m_nControlerType == USB_CONTROLER_KOREA)
	{
		char pBuf[8];
		memset(pBuf,0,8);
		/*
		CString strCOM;
		if (GetPortName(strCOM) == FALSE)
			return FALSE;

		if( !pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
			return FALSE;
		*/
		// OUT1 ON
		pBuf[0] = 0x55;
		pBuf[1] = 0x01;
		pBuf[2] = 0x01;
		pBuf[3] = 0x02;
		pBuf[4] = 0x00;
		pBuf[5] = 0x00;
		pBuf[6] = 0x00;
		pBuf[7] = 0x59;

		//pSerial.WriteComm((BYTE *)pBuf,8);
		dwRet = m_pSerial.WriteComm((BYTE *)pBuf,8);
	} 
	else if( m_nControlerType == USB_CONTROLER_CHINA)
	{
		char pBuf[17];
		memset(pBuf,0,17);
		
		CString strCOM;
		if (GetPortName(strCOM) == FALSE)
			return FALSE;
				
		if( !m_pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
			return FALSE;
		
 		// OUT1 ON
 		pBuf[0] = 0x3A;
 		pBuf[1] = 0x46;
 		pBuf[2] = 0x45;
 		pBuf[3] = 0x30;
 		pBuf[4] = 0x35;
 		pBuf[5] = 0x30;
 		pBuf[6] = 0x30;
 		pBuf[7] = 0x30;
 		pBuf[8] = 0x30;
 		pBuf[9] = 0x46;
 		pBuf[10] = 0x46;
 		pBuf[11] = 0x30;
 		pBuf[12] = 0x30;
 		pBuf[13] = 0x46;
 		pBuf[14] = 0x45;
 		pBuf[15] = 0x0D;
 		pBuf[16] = 0x0A;
 
 		dwRet = m_pSerial.WriteComm((BYTE *)pBuf,17);
		
		m_pSerial.ClosePort();
	}
	else if(m_nControlerType == USB_CONTROLER_V30)
	{
		char pBuf[17];
		memset(pBuf,0,17);
		
 		// OUT1 ON
 		pBuf[0] = 0x3A;
 		pBuf[1] = 0x46;
 		pBuf[2] = 0x45;
 		pBuf[3] = 0x30;
 		pBuf[4] = 0x35;
 		pBuf[5] = 0x30;
 		pBuf[6] = 0x30;
 		pBuf[7] = 0x30;
 		pBuf[8] = 0x30;
 		pBuf[9] = 0x46;
 		pBuf[10] = 0x46;
 		pBuf[11] = 0x30;
 		pBuf[12] = 0x30;
 		pBuf[13] = 0x46;
 		pBuf[14] = 0x45;
 		pBuf[15] = 0x0D;
 		pBuf[16] = 0x0A;
 
 		dwRet = m_pSerial.WriteComm((BYTE *)pBuf,17);
		
	}
 	//pSerial.ClosePort();
	ESMLog(5, _T("ONPower[%d] : %d"), m_nControlerType, dwRet);

 	return TRUE;
}

BOOL WatcherManager::OFFPower()
{
	DWORD dwRet = 0;
	//CCommThread pSerial;
	if( m_nControlerType == USB_CONTROLER_KOREA)
	{
		char pBuf[8];
		memset(pBuf,0,8);
		/*
		CString strCOM;
		if (GetPortName(strCOM) == FALSE)
			return FALSE;

		if( !pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
			return FALSE;
		*/
		// OUT1 OFF
		pBuf[0] = 0x55;
		pBuf[1] = 0x01;
		pBuf[2] = 0x01;
		pBuf[3] = 0x01;
		pBuf[4] = 0x00;
		pBuf[5] = 0x00;
		pBuf[6] = 0x00;
		pBuf[7] = 0x58;

		//pSerial.WriteComm((BYTE *)pBuf,8);
		dwRet = m_pSerial.WriteComm((BYTE *)pBuf,8);
	}
	else if( m_nControlerType == USB_CONTROLER_CHINA)
	{
		char pBuf[17];
		memset(pBuf,0,17);
		
		CString strCOM;
		if (GetPortName(strCOM) == FALSE)
			return FALSE;

		if( !m_pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
			return FALSE;
		
		pBuf[0] = 0x3A;
		pBuf[1] = 0x46;
		pBuf[2] = 0x45;
		pBuf[3] = 0x30;
		pBuf[4] = 0x35;
		pBuf[5] = 0x30;
		pBuf[6] = 0x30;
		pBuf[7] = 0x30;
		pBuf[8] = 0x30;
		pBuf[9] = 0x30;
		pBuf[10] = 0x30;
		pBuf[11] = 0x30;
		pBuf[12] = 0x30;
		pBuf[13] = 0x46;
		pBuf[14] = 0x44;
		pBuf[15] = 0x0D;
		pBuf[16] = 0x0A;
		
		dwRet = m_pSerial.WriteComm((BYTE *)pBuf,17);
		m_pSerial.ClosePort();
	}
	else if( m_nControlerType == USB_CONTROLER_V30)
	{
		char pBuf[17];
		memset(pBuf,0,17);
		
		pBuf[0] = 0x3A;
		pBuf[1] = 0x46;
		pBuf[2] = 0x45;
		pBuf[3] = 0x30;
		pBuf[4] = 0x35;
		pBuf[5] = 0x30;
		pBuf[6] = 0x30;
		pBuf[7] = 0x30;
		pBuf[8] = 0x30;
		pBuf[9] = 0x30;
		pBuf[10] = 0x30;
		pBuf[11] = 0x30;
		pBuf[12] = 0x30;
		pBuf[13] = 0x46;
		pBuf[14] = 0x44;
		pBuf[15] = 0x0D;
		pBuf[16] = 0x0A;
				
		dwRet = m_pSerial.WriteComm((BYTE *)pBuf,17);
	}
	ESMLog(5, _T("OFFPower[%d] : %d"), m_nControlerType, dwRet);
	//pSerial.ClosePort();
	return TRUE;
}

void WatcherManager::ReciveData(char* pBup, int nBufSize)
{
	TCHAR strPath[MAX_PATH];
	CString strFile;
	if( m_bFileDownMode == TRUE )
	{
		FileDownLoad(pBup, nBufSize );
	}
	else if( pBup[0] == '5')
	{
		ESMLog(5, _T("ReciveData : %c"), pBup[0]);

		m_bFileDownMode = TRUE;
		pBup += 1;
		if( FileDownLoadStart(pBup, &m_strFileName, &m_nFileTotalSize))
		{
			m_bFileDownMode = TRUE;
			m_ServerSocket->SendData(_T("1"));// 파일 받을준비 완료. message
		}
		else
			m_ServerSocket->SendData(_T("0"));
	}
	else if( pBup[0] == '0' )		// Watcher Program Alive 확인 
	{
		m_ServerSocket->SendData(_T("1"));
	}
	else if(pBup[0] == '1')
	{		
		GetModuleFileName( NULL, strPath, _MAX_PATH);
		PathRemoveFileSpec(strPath);
		strFile = ESMUtil::CharToCString(&pBup[1]);
		if(GetIsFile(strPath, strFile))
		{
			if(ExecuteProcess(strPath, strFile))
			{
				/*Sleep(1000);
				strFile.Append(_T(" -t -e -h"));
				ExecuteProcess(strPath, _T("procdump64.exe"), strFile);*/
				m_ServerSocket->SendData(_T("1"));
			}
			else
				m_ServerSocket->SendData(_T("0"));
		}
/*
		if(pBup[1] == '1')
		{
			if(GetIsFile(strPath, _T("4DMakerD.exe")))
			{
				if(ExecuteProcess(strPath, _T("4DMakerD.exe")))
					m_ServerSocket->SendData(_T("1"));
				else
					m_ServerSocket->SendData(_T("0"));
			}
			else
			{
				if(ExecuteProcess(strPath, _T("4DModelD.exe")))
					m_ServerSocket->SendData(_T("1"));
				else
					m_ServerSocket->SendData(_T("0"));
			}
		}
		if(pBup[1] == '2')
		{
			if(GetIsFile(strPath, _T("4DMaker.exe")))
			{
				if(ExecuteProcess(strPath, _T("4DMaker.exe")))
					m_ServerSocket->SendData(_T("1"));
				else
					m_ServerSocket->SendData(_T("0"));
			}
			else
			{
				if(ExecuteProcess(strPath, _T("4DModel.exe")))
					m_ServerSocket->SendData(_T("1"));
				else
					m_ServerSocket->SendData(_T("0"));
			}
		}
*/
		// 프로그램 실행.
	}
	else if( pBup[0] == '2')
	{

		CString strFile2, strTp;
		CString strFileName1, strFileName2;
		strFile = ESMUtil::CharToCString(&pBup[1]);
		if(strFile.Find(_T("D.exe")) == -1)
		{
			strFileName1 = strFile;
			strTp = strFile.Left(strFile.Find(_T(".exe"))); 
			strFileName2 = strTp + _T("D.exe");
		}
		else
		{
			strFileName2 = strFile;
			strTp = strFile.Left(strFile.Find(_T("D.exe"))); 
			strFileName1 = strTp + _T(".exe");
		}

		//KillProcess(_T("procdump64.exe"));

		if(IsExistProcess(strFileName1))
		{
			if(KillProcess(strFileName1))
				m_ServerSocket->SendData(_T("1"));
		}
		else if(IsExistProcess(strFileName2))
		{
			if(KillProcess(strFileName2))
				m_ServerSocket->SendData(_T("1"));
		}
		else
			m_ServerSocket->SendData(_T("0"));
/*
		if(IsExistProcess(_T("4DMakerD.exe")))
		{
			if(KillProcess(_T("4DMakerD.exe")))
				m_ServerSocket->SendData(_T("1"));
		}
		else if(IsExistProcess(_T("4DMaker.exe")))
		{
			if(KillProcess(_T("4DMaker.exe")))
				m_ServerSocket->SendData(_T("1"));
		}
		if(IsExistProcess(_T("4DModelD.exe")))
		{
			if(KillProcess(_T("4DModelD.exe")))
				m_ServerSocket->SendData(_T("1"));
		}
		else if(IsExistProcess(_T("4DModel.exe")))
		{
			if(KillProcess(_T("4DModel.exe")))
				m_ServerSocket->SendData(_T("1"));
		}
		else
			m_ServerSocket->SendData(_T("0"));
*/
	}
	/*
	else if( pBup[0] == '3')
	{
		CString strFile2, strTp;
		CString strFileName1, strFileName2;
		strFile = ESMUtil::CharToCString(&pBup[1]);
		if(strFile.Find(_T("D.exe")) == -1)
		{
			strFileName1 = strFile;
			strTp = strFile.Left(strFile.Find(_T(".exe"))); 
			strFileName2 = strTp + _T("D.exe");
		}
		else
		{
			strFileName2 = strFile;
			strTp = strFile.Left(strFile.Find(_T("D.exe"))); 
			strFileName1 = strTp + _T(".exe");
		}

		if(IsExistProcess(strFileName1) || IsExistProcess(strFileName2))
			m_ServerSocket->SendData(_T("1"));
		else
			m_ServerSocket->SendData(_T("0"));

/*
		if(IsExistProcess(_T("4DMakerD.exe")))
			m_ServerSocket->SendData(_T("1"));
		else if(IsExistProcess(_T("4DMaker.exe")))
			m_ServerSocket->SendData(_T("1"));
		if(IsExistProcess(_T("4DModelD.exe")))
			m_ServerSocket->SendData(_T("1"));
		else if(IsExistProcess(_T("4DModel.exe")))
			m_ServerSocket->SendData(_T("1"));
		else
			m_ServerSocket->SendData(_T("0"));
	}
	*/
	else if( pBup[0] == '3')
	{	
		int nRet = 0;
		nRet = CheckProc();

		if(nRet == -1) // 0ff
			m_ServerSocket->SendData(_T("0"));
		else if(nRet == 0) // On
			m_ServerSocket->SendData(_T("1"));
		else if(nRet == 1) // No Response 
			m_ServerSocket->SendData(_T("2"));
		else
			m_ServerSocket->SendData(_T("3"));
	}
	else if( pBup[0] == '4')
	{	
		ULARGE_INTEGER FreeByteAvailable;
		ULARGE_INTEGER unTotalSize;
		ULARGE_INTEGER unFreeSize;

		CString strSize = _T("");

		GetDiskFreeSpaceEx(TEXT("M:") ,&FreeByteAvailable, &unTotalSize, &unFreeSize);
		
		LONGLONG nTotalSize = (UINT)((unTotalSize.QuadPart)/1024/1024);
		LONGLONG nFreeSize = nTotalSize - ((UINT)((unFreeSize.QuadPart)/1024/1024));

		strSize.Format(_T("%d/%d"),nFreeSize,nTotalSize);
		m_ServerSocket->SendData(strSize);
	}
	else if( pBup[0] == '6')
	{	
		BOOL bRet;
		if(pBup[1] == 0x00)
			bRet = OFFPower();
		else
			bRet = ONPower();

		if(bRet)
			m_ServerSocket->SendData(_T("1"));
		else 
			m_ServerSocket->SendData(_T("0"));
	}
	else if( pBup[0] == '7')
	{
		if (CheckPower())
			m_ServerSocket->SendData(_T("1"));
		else 
			m_ServerSocket->SendData(_T("0"));
	}
	else if( pBup[0] == '8')
	{
		BOOL bRet;
		if(pBup[1] == 0x31)
			bRet = SystemReBoot();
		else
			bRet = SystemShutdown();

		if(bRet)
			m_ServerSocket->SendData(_T("1"));
		else 
			m_ServerSocket->SendData(_T("0"));
	}
	else if( pBup[0] == '9')
	{
		m_bFileDownMode = TRUE;
		pBup += 1;
		if( FileDownLoadStart(pBup, &m_strFileName, &m_nFileTotalSize, &m_strFilePath))
		{
			m_bFileDownMode = TRUE;
			m_ServerSocket->SendData(_T("1"));// 파일 받을준비 완료. message
		}
		else
			m_ServerSocket->SendData(_T("0"));
	}

	else if(pBup[0] == 'a')
	{		
		GetModuleFileName( NULL, strPath, _MAX_PATH);
		PathRemoveFileSpec(strPath);
		strFile = ESMUtil::CharToCString(&pBup[1]);
		if(GetIsFile(strPath, strFile))
		{
			if(ExecuteProcess(strPath, strFile))
			{
				/*Sleep(1000);
				strFile.Append(_T(" -t -e -h"));
				ExecuteProcess(strPath, _T("procdump64.exe"), strFile);*/
				m_ServerSocket->SendData(_T("1"));
			}
			else
				m_ServerSocket->SendData(_T("0"));
		}
	}

	else if(pBup[0] == 'b')
	{			
		CString strFileName;
		strFileName = ESMUtil::CharToCString(&pBup[1]);

		if(IsExistProcess(strFileName))
		{
			if(KillProcess(strFileName))
				m_ServerSocket->SendData(_T("1"));
		}
		else
			m_ServerSocket->SendData(_T("0"));
	}

	else if( pBup[0] == 'c')
	{	
		int nRet = 0;
		nRet = CheckBackupProgram();

		if(nRet == -1) // 0ff
			m_ServerSocket->SendData(_T("0"));
		else if(nRet == 0) // On		
			m_ServerSocket->SendData(_T("1"));					
		else if(nRet == 1) // No Response 
			m_ServerSocket->SendData(_T("2"));
		else if(nRet == 2) // No Exist Backup Drive
			m_ServerSocket->SendData(_T("3"));
	}

	else if( pBup[0] == 'd')
	{	
		int nRet = 0;
		m_nFileCount = 0;
		//nRet = CheckBackupFolderCurrentFileCount("D:\\Record_Backup");
		nRet = CheckBackupFolderCurrentSize(_T("D:\\Record_Backup"));		

		char fileCount[255];
		itoa(nRet, fileCount, 10);
		CString strFileCount(fileCount);		
		m_ServerSocket->SendData(strFileCount);		
		m_nFileCount = 0;
	}
	else
	{
		m_ServerSocket->SendData(_T("-"));
	}
	if(m_bFileDownMode != TRUE)
		ESMLog(5, _T("ReciveData : %c"), pBup[0]);
}

BOOL WatcherManager::SendCommPort(char* buf)
{
	CCommThread pSerial[4];
	CString strCOM;
	for(int i=0 ; i < 4 ; i++)
	{
		strCOM.Format(_T("COM%d"),i+1);
		pSerial[i].OpenPort(strCOM, 9600, 8, 0, 0);
		pSerial[i].WriteComm((BYTE *)buf,5);
		pSerial[i].ClosePort();
	}
	return TRUE;
}

BOOL WatcherManager::GetIsFile(CString strAppPath, CString strAppName)
{
	CString strFullPath;
	strFullPath = strAppPath + _T("\\") + strAppName;
	CFileStatus status;
	if( CFile::GetStatus( strFullPath, status ) == NULL )
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

BOOL WatcherManager::ExecuteProcess(CString strAppPath, CString strAppName, CString strParam)
{
	SHELLEXECUTEINFO shInfo	= { 0 };
	shInfo.cbSize			= sizeof(SHELLEXECUTEINFO);
	shInfo.lpFile			= strAppName;
	shInfo.nShow			= SW_SHOW;
	//shInfo.lpParameters		= _T("");
	shInfo.lpParameters		= strParam;
	shInfo.lpDirectory		= strAppPath;
	shInfo.lpVerb			= _T("");
	shInfo.fMask			= SEE_MASK_NOCLOSEPROCESS;
	shInfo.hwnd				= NULL;
	shInfo.hInstApp			= AfxGetInstanceHandle();

	if ( ShellExecuteEx(&shInfo) )
		return TRUE;
	else
		return FALSE;
}

BOOL WatcherManager::IsExistProcess(CString strProcess)
{
	HANDLE hndl = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
	DWORD dwsma = GetLastError();

	DWORD dwExitCode = 0;

	PROCESSENTRY32  procEntry={0};
	procEntry.dwSize = sizeof( PROCESSENTRY32 );
	Process32First(hndl,&procEntry);
	do
	{
		if(!_tcsicmp(procEntry.szExeFile,strProcess))
		{
			return TRUE;	
		}
	}while(Process32Next(hndl,&procEntry));

	if(hndl != INVALID_HANDLE_VALUE)
		CloseHandle(hndl);
	hndl = INVALID_HANDLE_VALUE;

	return FALSE;
}

BOOL WatcherManager::KillProcess(CString strProcess)
{
	HANDLE hndl = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
	DWORD dwsma = GetLastError();
	BOOL bTerminate = FALSE;
	DWORD dwExitCode = 0;

	PROCESSENTRY32  procEntry={0};
	procEntry.dwSize = sizeof( PROCESSENTRY32 );
	Process32First(hndl,&procEntry);
	do
	{
		if(!_tcsicmp(procEntry.szExeFile,strProcess))
		{
			HANDLE hHandle;
			hHandle = ::OpenProcess(PROCESS_TERMINATE, FALSE, procEntry.th32ProcessID);
			if(hHandle)
			{
				::GetExitCodeProcess(hHandle,&dwExitCode);
				bTerminate = ::TerminateProcess(hHandle, dwExitCode);

				CloseHandle(hHandle);
				break;
			}
		}
	}while(Process32Next(hndl,&procEntry));	

	if(hndl != INVALID_HANDLE_VALUE)
		CloseHandle(hndl);
	hndl = INVALID_HANDLE_VALUE;

	return bTerminate;
}

BOOL WatcherManager::FileDownLoadStart(char* pBuf, CString* strFileName, int* nFileSize)
{
	char delims[] = "_";
	char *result = NULL;
	int i = 0;
	m_nTotalReceiveSize = 0;
	CString  strFileSize;

	result = strtok( pBuf, delims );
	while( result != NULL )
	{
		i++;
		int len = 0;
		BSTR buf;
		len = MultiByteToWideChar(CP_ACP, 0, result, (int)strlen(result), NULL, NULL);
		buf = SysAllocStringLen(NULL, len);
		MultiByteToWideChar(CP_ACP, 0, result, (int)strlen(result), buf, len);

		if ( i == 1)
			strFileName->Format(_T("%s"), buf);
		if ( i == 2)
			strFileSize.Format(_T("%s"), buf);
		result = strtok( NULL, delims );
	}
	*nFileSize = _ttoi(strFileSize);

	if( m_WriteFile != NULL )
	{
		m_WriteFile->Close();
		m_WriteFile = NULL;
		return FALSE;
	}

	CString strFullPath;
	m_WriteFile = new CFile;
	TCHAR strPath[MAX_PATH];
	GetModuleFileName( NULL, strPath, _MAX_PATH);
	PathRemoveFileSpec(strPath);

	strFullPath.Format(_T("%s\\%s"), strPath, *strFileName);

	if (!m_WriteFile->Open(strFullPath, CFile::modeWrite | CFile::modeCreate ))
	{
		return FALSE;
	}
	//AfxMessageBox(strFullPath);
	return TRUE;
}

BOOL WatcherManager::FileDownLoadStart(char* pBuf, CString* strFileName, int* nFileSize, CString* strFilePath)
{
	char delims[] = "_";
	char *result = NULL;
	int i = 0;
	m_nTotalReceiveSize = 0;
	CString  strFileSize;

	result = strtok( pBuf, delims );
	while( result != NULL )
	{
		i++;
		int len = 0;
		BSTR buf;
		len = MultiByteToWideChar(CP_ACP, 0, result, (int)strlen(result), NULL, NULL);
		buf = SysAllocStringLen(NULL, len);
		MultiByteToWideChar(CP_ACP, 0, result, (int)strlen(result), buf, len);

		if ( i == 1)
			strFileName->Format(_T("%s"), buf);
		if ( i == 2)
			strFileSize.Format(_T("%s"), buf);
		if ( i == 3)
			strFilePath->Format(_T("%s"), buf);
		result = strtok( NULL, delims );
	}
	*nFileSize = _ttoi(strFileSize);

	if( m_WriteFile != NULL )
	{
		m_WriteFile->Close();
		m_WriteFile = NULL;
		return FALSE;
	}

	CString strFullPath;
	m_WriteFile = new CFile;
	TCHAR strPath[MAX_PATH];
	GetModuleFileName( NULL, strPath, _MAX_PATH);
	PathRemoveFileSpec(strPath);

	if(strFilePath->IsEmpty())
	{
		strFullPath.Format(_T("%s\\%s"), strPath, *strFileName);
	}else
	{
		strFullPath.Format(_T("%s\\%s"), *strFilePath, *strFileName);
	}
	
	if (!m_WriteFile->Open(strFullPath, CFile::modeWrite | CFile::modeCreate ))
	{
		return FALSE;
	}
	//AfxMessageBox(strFullPath);
	return TRUE;
}

BOOL WatcherManager::FileDownLoad(char* buf, int nBufSize)
{
	BOOL bReturn = FALSE;
	if(m_WriteFile == NULL)
		return bReturn;

	m_WriteFile->Write(buf, nBufSize);
	m_nTotalReceiveSize += nBufSize;

	if( m_nTotalReceiveSize == m_nFileTotalSize)
	{
		if( m_WriteFile == NULL )
		{
			m_ServerSocket->SendData(_T("0"));
			bReturn = FALSE;
		}
		else
		{
			m_WriteFile->Close();
			m_WriteFile = NULL;
			m_ServerSocket->SendData(_T("1"));
		}
		m_nTotalReceiveSize = 0;
		m_bFileDownMode = FALSE;
	}
	else if( m_nTotalReceiveSize > m_nFileTotalSize )
	{
		m_WriteFile->Close();
		m_ServerSocket->SendData(_T("0"));
		bReturn = FALSE;
		m_nTotalReceiveSize = 0;
		m_bFileDownMode = FALSE;
	}
	return bReturn;
}

BOOL WatcherManager::CheckRegistyStartProgram()
{
	HKEY  hSerialCom;
	TCHAR buffer[_MAX_PATH], data[_MAX_PATH];
	DWORD len = MAX_PATH, type, dataSize;
	long  i;

	//CMiLRe 20160202 Watcher 레지스트리 읽기 옵션 변경
	if (::RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"), 0, KEY_QUERY_VALUE, &hSerialCom) == ERROR_SUCCESS)
	{
		for (i=0; 1; i++)
		{
			len = MAX_PATH;
			dataSize=_MAX_PATH;
			long lResult = ::RegEnumValue(hSerialCom, i, buffer, &len, NULL, &type, (unsigned char*)data, &dataSize);
			if ( lResult == ERROR_NO_MORE_ITEMS)
				break;

			CString strSerialName =(LPCTSTR)buffer;
 			if ( strSerialName == REG_NAME)
			{
				CString strPath = data;

				TCHAR chrFileName[500];
				GetModuleFileName(NULL, chrFileName, MAX_PATH);
				CString strFileName;
				strFileName.Format(_T("\"%s\""), chrFileName);

				if ( strPath == strFileName)
					return TRUE;
			}
		}

		::RegCloseKey(hSerialCom);
	}
	return FALSE;

}

/*********************************************************************************************
/* Parameters 
/*  bAutoExec[in]     : TRUE이면 시작프로그램 레지스트리에 등록, FALSE면 해제
/*  strValueName[in]   : 설정할 값의 이름
/*  strExeFileName[in] : 실행시킬 프로그램 Full 경로 (NULL 일수 있음, 단, bAutoExec값이 FALSE이여야 함)
/*
/* Return Values
/*  시작프로그램 레지스트리에 등록/헤제 성공이면 TRUE, 실패면 FALSE
*********************************************************************************************/
BOOL WatcherManager::SetRegistyStartProgram(BOOL bAutoExec, CString strValueName, CString strExeFileName)
{
	BOOL _result;
	HKEY hKey;
	LONG lRes;

	USES_CONVERSION;                       // CString -> LPCWSTR
	LPCWSTR wstrFile;
	LPCSTR con_tMsg = W2CA(strExeFileName);
	wstrFile = A2W(con_tMsg);

	if(bAutoExec)
	{
		if(strValueName == _T("") || strExeFileName == _T(""))
			return FALSE;
		lRes = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"), 0L, KEY_WRITE, &hKey);
		if( lRes == ERROR_SUCCESS )
		{
			int a = lstrlen(strExeFileName);
			lRes = ::RegSetValueEx(hKey, strValueName, 0, REG_SZ, (BYTE*)wstrFile, lstrlen(wstrFile)*2); 
			::RegCloseKey(hKey);
			if(lRes == ERROR_SUCCESS) 
				_result = TRUE;
			else
				_result = FALSE;
		}
		else if(lRes == ERROR_ACCESS_DENIED)
			_result = FALSE;
		else
			_result = FALSE;
	}
	else                 // Run 에서 제거
	{
		//CMiLRe 20160202 Watcher 레지스트리 읽기 옵션 변경
		lRes = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"), 0 , KEY_QUERY_VALUE, &hKey);
		if( lRes != ERROR_SUCCESS )
			_result = FALSE;

		lRes = RegDeleteValue(hKey, strValueName);      
		RegCloseKey(hKey);
		if(lRes == ERROR_SUCCESS)
			_result = TRUE;
		else if(lRes == ERROR_FILE_NOT_FOUND)
			_result = FALSE;
		else
			_result = FALSE;
	}
	return _result;
}

BOOL WatcherManager::SystemShutdown()
{
	HANDLE hToken; 
	TOKEN_PRIVILEGES tkp; 
	// Get a token for this process. 

	if(!OpenProcessToken(GetCurrentProcess(), 
		TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) 
		return FALSE; 
	
	// Get the LUID for the shutdown privilege. 
	LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid); 

	tkp.PrivilegeCount = 1; // one privilege to set 
	tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
	// Get the shutdown privilege for this process. 
	AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0); 
	if(GetLastError() != ERROR_SUCCESS) 
		return FALSE; 

	// Shut down the system and force all applications to close. 
	if(!ExitWindowsEx(EWX_SHUTDOWN | EWX_FORCE, 0)) 
		return FALSE; 

	return TRUE; 
}

BOOL WatcherManager::SystemReBoot()
{
	//CString copystr;
	//copystr.Format(_T("shutdown -f -r -t 3"));
	ShellExecute(NULL,_T("open"), _T("cmd.exe"), _T("/C shutdown -f -r -t 3"),  NULL, SW_SHOW);

	return TRUE;
}

void WatcherManager::CheckModel()
{
	DWORD nRead = 1;

	//CCommThread pSerial;
	CString strCOM;
	if (GetPortName(strCOM) == FALSE)
		return ;

	if(m_strSerialName == SERIAL_PORT_NAME_4DA3)
	{
		/*m_nControlerType = USB_CONTROLER_CHINA;*/
		m_nControlerType = USB_CONTROLER_V30;
		//jhhan 180421
		m_pSerial.OpenPort(strCOM, 9600, 8, 0, 0);		//4DA 3.0의 경우 초기 OPEN 이후 최종 종료전 CLOSE 수행
		return;
	}

	//if( !pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
	if( !m_pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
		return ;

	char pBuf[8];

	memset(pBuf,0,8);

	while (nRead)
	{
		//nRead = pSerial.ReadComm((BYTE *)&pBuf ,8);
		nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,8);
	}

	pBuf[0] = 0x55;
	pBuf[1] = 0x01;
	pBuf[2] = 0x00;
	pBuf[3] = 0x01;
	pBuf[4] = 0x00;
	pBuf[5] = 0x00;
	pBuf[6] = 0x00;
	pBuf[7] = 0x57;

	//pSerial.WriteComm((BYTE *)&pBuf,8);
	m_pSerial.WriteComm((BYTE *)&pBuf,8);

	Sleep(200);
	//nRead = pSerial.ReadComm((BYTE *)&pBuf ,4);
	nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,4);
	//pSerial.ClosePort();
	if(pBuf[0] == 34)
		m_nControlerType = USB_CONTROLER_KOREA;
	else
		m_nControlerType = USB_CONTROLER_CHINA;
	//jhhan 180421
	m_pSerial.ClosePort();		//4DA 2.0의 경우 OPEN & CLOSE 수행

}

int WatcherManager::CheckProc()
{
	HWND _hWnd = NULL;
	DWORD dwPid = 0;

	dwPid = ESMUtil::GetProcessPID(_T("4DMaker.exe"));

	if(dwPid == 0)
		dwPid = ESMUtil::GetProcessPID(_T("4DMakerD.exe"));

	if (dwPid == 0)
		return -1;

	_hWnd = ESMUtil::GetWndHandle(dwPid);

	return IsHungAppWindow(_hWnd); 
}

BOOL WatcherManager::GetCheckUSB ()
{
	GUID guidDeviceInterface = { 0xA5DCBF10, 0x6530, 0x11D2, { 0x90, 0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED } };
	if (guidDeviceInterface == GUID_NULL)
	{
		return FALSE;
	}

	BOOL bResult = TRUE;
	BOOL bResult2 = FALSE;
	HDEVINFO hDevInfo;
	SP_DEVINFO_DATA DevInfoData;

	SP_DEVICE_INTERFACE_DATA DevInterfaceData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA pInterfaceDetailData = NULL;

	ULONG requiredLength=0;

	LPTSTR lpDevPath = NULL;
	CString strTemp = _T("");

	DWORD index = 0;

	hDevInfo = SetupDiGetClassDevs(&guidDeviceInterface, NULL, NULL,DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);

	if (hDevInfo == INVALID_HANDLE_VALUE) 
	{ 
		printf("Error SetupDiGetClassDevs: %d.\n", GetLastError());
		goto done;
	}

	DevInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
	
	for (index = 0; SetupDiEnumDeviceInfo(hDevInfo, index, &DevInfoData); index++)
	{
		if (lpDevPath)
		{
			LocalFree(lpDevPath);
			lpDevPath = NULL;
		}
		if (pInterfaceDetailData)
		{
			LocalFree(pInterfaceDetailData);
			pInterfaceDetailData = NULL;
		}

 		DevInterfaceData.cbSize = sizeof(SP_INTERFACE_DEVICE_DATA);
 
 		bResult = SetupDiEnumDeviceInterfaces(hDevInfo, NULL, &guidDeviceInterface, index, &DevInterfaceData);
 
		DWORD dwError;

		dwError = GetLastError();

 		if (GetLastError () == ERROR_NO_MORE_ITEMS)
 		{
 			break;
 		}

		if (!bResult) 
		{
			printf("Error SetupDiEnumDeviceInterfaces: %d.\n", GetLastError());
			goto done;
		}

		bResult = SetupDiGetDeviceInterfaceDetail(hDevInfo,	&DevInterfaceData, NULL, 0,	&requiredLength, NULL);


		if (!bResult) 
		{
			if ((ERROR_INSUFFICIENT_BUFFER==GetLastError()) && (requiredLength>0))
			{
				pInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)LocalAlloc(LPTR, requiredLength);

				if (!pInterfaceDetailData) 
				{ 
					// ERROR 
					printf("Error allocating memory for the device detail buffer.\n");
					goto done;
				}
			}
			else
			{
				printf("Error SetupDiEnumDeviceInterfaces: %d.\n", GetLastError());
				goto done;
			}
		}

		if(pInterfaceDetailData)
			pInterfaceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

		bResult = SetupDiGetDeviceInterfaceDetail(hDevInfo, &DevInterfaceData, pInterfaceDetailData, requiredLength, NULL, &DevInfoData);

		if (!bResult) 
		{
			printf("Error SetupDiGetDeviceInterfaceDetail: %d.\n", GetLastError());
			goto done;
		}
		else
		{
			size_t nLength = 0;
			if(pInterfaceDetailData)
				nLength = wcslen (pInterfaceDetailData->DevicePath) + 1;

			lpDevPath = (TCHAR *) LocalAlloc (LPTR, nLength * sizeof(TCHAR));
			if(!lpDevPath)
			{
				printf("Error %d.", GetLastError());
				goto done;
			}

			StringCchCopy(lpDevPath, nLength, pInterfaceDetailData->DevicePath);
			lpDevPath[nLength-1] = 0;

			printf("Device path: %S\n", lpDevPath);
			CString temp;
			temp.Format(_T("%s"),lpDevPath);

			if(1<temp.Find(SDI_VID_KEYLOCK))
				bResult2 = true;

			if(!lpDevPath)
			{
				printf("Error %d.", GetLastError());
				goto done;
			}
		}
	}	

done:
	if(lpDevPath)
		LocalFree(lpDevPath);
	if(pInterfaceDetailData)
		LocalFree(pInterfaceDetailData);    
	
	bResult = SetupDiDestroyDeviceInfoList(hDevInfo);
	
	return bResult2;
}

BOOL WatcherManager::CheckExistBackupDrive()
{		
	int nDrivePos = 0;
	CString strDrive = _T("?");
	CString strBackupDrive = _T("D");
	DWORD dwDriveList = ::GetLogicalDrives();
	BOOL bIsExistBackupDrive = FALSE;

	while(dwDriveList)
	{
		if(dwDriveList & 1)  
		{
			strDrive.SetAt(0,'A'+nDrivePos);
			if(!strDrive.CompareNoCase(strBackupDrive))
				bIsExistBackupDrive = TRUE;
		}		
		dwDriveList >>= 1;
		nDrivePos++;
	}

	return bIsExistBackupDrive;
}

int WatcherManager::CheckBackupProgram()
{
	HWND _hWnd = NULL;
	DWORD dwPid = 0;

	if(!CheckExistBackupDrive())
		return 2;
	
	CreateBackupFolder();

	dwPid = ESMUtil::GetProcessPID(_T("GoodSync-v10.exe"));

	if (dwPid == 0)
		return -1;

	_hWnd = ESMUtil::GetWndHandle(dwPid);

	return IsHungAppWindow(_hWnd); 	
}

void WatcherManager::CreateBackupFolder()
{
	CString strBackupFolder = _T("D:\\Record_Backup");

	DWORD dwAttr = GetFileAttributes(strBackupFolder);
	if(dwAttr == 0xffffffff)
	{		
		::CreateDirectory (strBackupFolder, NULL);
	}
}

int WatcherManager::CheckBackupFolderCurrentFileCount(const char * Path)
{
	HANDLE hSrch;

	WIN32_FIND_DATAA wfd;
	BOOL bResult = TRUE;

	char newpath[MAX_PATH];
	char drive[_MAX_DRIVE];
	char dir[MAX_PATH];
	char ext[_MAX_EXT] = {0,};

	hSrch=FindFirstFileA(Path, &wfd);
	while(bResult)
	{
		_splitpath(Path,drive,dir,NULL,ext);

		if(wfd.dwFileAttributes& FILE_ATTRIBUTE_DIRECTORY)
		{
			if(wfd.cFileName[0] != '.')
			{
				sprintf(newpath,"%s%s%s\\*.*",drive,dir,wfd.cFileName);
				CheckBackupFolderCurrentFileCount(newpath);
			}
		}
		else
			m_nFileCount++;

		bResult = FindNextFileA(hSrch, &wfd);
	}
	FindClose(hSrch);
	return m_nFileCount;
}

int WatcherManager::CheckBackupFolderCurrentSize(CString Path)
{
	BOOL bExsit;
	CFileFind c_FileFind;

	bExsit = c_FileFind.FindFile(Path);
	while (bExsit)
	{
		bExsit = c_FileFind.FindNextFileW();

		if (c_FileFind.IsDots())
			continue;

		if (c_FileFind.IsDirectory())
		{
			CString strNextDir = _T("");
			strNextDir =  c_FileFind.GetFilePath() + _T("\\*.*");
			CheckBackupFolderCurrentSize(strNextDir);
		}
		else
		{
			m_nFileCount += c_FileFind.GetLength()/1048576.;
		}
	}
	c_FileFind.Close();

	return m_nFileCount;
}
