////////////////////////////////////////////////////////////////////////////////
//! 
//! ESMLab, Inc. PROPRIETARY INFORMATION.
//! The following contains information proprietary to
//! ESMLab, Inc., and may not be copied
//! nor disclosed except upon written agreement
//! by ESMLab, Inc..
//! Copyright (C) 2013 ESMLab, Inc. All rights reserved.
//! 
//! @File     maservermanager.cpp
//! @brief    
//! @Author   KimChangDo
//! @Date     2013-03-19
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "MAServerManager.h"
#include "ServerSock.h"
#include "..\WatcherManager.h"


// MAServerManager
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(MAServerManager, CWnd)

MAServerManager::MAServerManager()
{
	m_pServerSock = NULL;
	InitSocket();
	m_pView = NULL;
}

MAServerManager::~MAServerManager()
{
	delete m_pServerSock;
	POSITION pos = m_clientSocks.GetHeadPosition();
	while (pos != NULL)
		delete m_clientSocks.GetNext(pos);
}

BEGIN_MESSAGE_MAP(MAServerManager, CWnd)
END_MESSAGE_MAP()

void MAServerManager::InitSocket()
{
	if (!AfxSocketInit())
	{
		return;
	}

	if( m_pServerSock != NULL)
		return;

	m_pServerSock = new CServerSock();


	if( m_pServerSock->Create(MAKERWATCHERPORT) == FALSE)
	{
		delete m_pServerSock;
		m_pServerSock = NULL;
		return ;
	}
	if( m_pServerSock->Listen(0) == FALSE)
	{
		delete m_pServerSock;
		m_pServerSock = NULL;
		return ;
	}

	m_pServerSock->SetParent((void*)this);
}

// Control Receive
void MAServerManager::OnReceive(CClientSock* pSock)
{
	// Notwork Buffer ���´�.
	char buf[PACKETDATASIZE];
	memset(buf, 0, PACKETDATASIZE);
	int nBufSize = pSock->Receive(buf,PACKETDATASIZE);
	

	if( nBufSize == 0)
	{
		pSock->Close();
		return;
	}
	else if( nBufSize == SOCKET_ERROR)
	{
		int nErr = GetLastError();
		if( nErr != WSAEWOULDBLOCK)
		{
			pSock->Close();
			return;
		}
	}
	((WatcherManager*)m_pParent)->ReciveData(buf, nBufSize);
}

void MAServerManager::OnClose(CClientSock* pSock)
{
	POSITION pos = m_clientSocks.GetHeadPosition();
	POSITION tpPos;
	while( pos != NULL )
	{
		tpPos = pos;
		CClientSock* ptpSock = m_clientSocks.GetNext( pos );
		if( pSock == ptpSock)
		{
			m_clientSocks.RemoveAt(tpPos);
			ptpSock->Close();
			delete ptpSock;
			return;
		}
	}
}

void MAServerManager::OnClose()
{
	POSITION pos = m_clientSocks.GetHeadPosition();
	POSITION tpPos;
	while( pos != NULL )
	{
		tpPos = pos;
		CClientSock* ptpSock = m_clientSocks.GetNext( pos );

		m_clientSocks.RemoveAt(tpPos);
		ptpSock->Close();
		delete ptpSock;
	}
	return;
}

void MAServerManager::OnAccept()
{
	CClientSock *pClientSock = new CClientSock;
	m_pServerSock->Accept(*pClientSock);

	struct linger zl;  //zero-linger	
	zl.l_onoff  = 1;	
	zl.l_linger = 0;
	if(SOCKET_ERROR == pClientSock->SetSockOpt(SO_LINGER, (char*)&zl, sizeof(zl)))
	{
		pClientSock->Close();
		delete pClientSock;
		return;
	}

 	if( m_clientSocks.GetCount() >= 1)
	{
		pClientSock->Close();
		delete pClientSock;
 		return;
	}
	m_clientSocks.AddTail(pClientSock);
	pClientSock->SetParent(this);
}

void MAServerManager::Send(char *buf, int nSendDataSize)
{
	POSITION pos = m_clientSocks.GetHeadPosition();
	while (pos != NULL)
	{
		CClientSock *pClientSock= m_clientSocks.GetNext(pos);
		pClientSock->Send(buf, nSendDataSize);
	}
}

void MAServerManager::SetParent(void* pThis)
{
	m_pParent = pThis;
}


BOOL MAServerManager::SendData(CString strInfoData)
{
	char* strSendData, * strSendBuffer;
	strSendData = CStringToChar(strInfoData);
	int dwPayloadSize = strlen(strSendData);
	Send(strSendData, dwPayloadSize);
	delete[] strSendData;
	return TRUE;
}	

char* MAServerManager::CStringToChar(CString tpStr)
{
	CString strLine = tpStr + _T("\0");
	char * str_value = NULL;
	int length = WideCharToMultiByte(CP_ACP, 0, strLine.GetBuffer(), -1, NULL, 0, NULL,NULL); 
	str_value = new char[length]; 
	WideCharToMultiByte(CP_ACP, 0, strLine.GetBuffer(), -1, str_value, length, NULL, NULL); 
	return str_value;
}