#pragma once

/////////////////////////////////////////////////////////////////////////////
// CClientSock command target

class CClientSock : public CAsyncSocket
{

public:
	CClientSock();
	virtual ~CClientSock();
	void SetParent(void* pParent) ;
	virtual void OnReceive(int nErrorCode);
	virtual void OnClose(int nErrorCode);

protected:
	void* m_pParent;
};
