// ClientSock.cpp : implementation file
//

#include "stdafx.h"
#include "ClientSock.h"
#include "MAServerManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClientSock

CClientSock::CClientSock()
{
	m_pParent = NULL;
}

CClientSock::~CClientSock()
{
}


void CClientSock::SetParent(void* pParent) 
{
	m_pParent = pParent;
}


void CClientSock::OnReceive(int nErrorCode) 
{
	((MAServerManager*)m_pParent)->OnReceive(this);
	CAsyncSocket::OnReceive(nErrorCode);
}

void CClientSock::OnClose(int nErrorCode)
{
	((MAServerManager*)m_pParent)->OnClose(this);
	CAsyncSocket::OnClose(nErrorCode);
}