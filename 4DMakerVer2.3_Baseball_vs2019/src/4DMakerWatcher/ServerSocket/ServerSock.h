#pragma once

/////////////////////////////////////////////////////////////////////////////
// CServerSock command target

class CServerSock : public CAsyncSocket
{
public:

public:
	CServerSock();
	virtual ~CServerSock();

public:
	public:
	virtual void OnAccept(int nErrorCode);
	void SetParent(void* pParent) ;


protected:
	void* m_pParent;
};
