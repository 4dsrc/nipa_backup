#pragma once


// MAServerManager
#include "ServerSock.h"
#include "ClientSock.h"

class MAServerManager : public CWnd
{
	DECLARE_DYNAMIC(MAServerManager)

public:

	MAServerManager();
	virtual ~MAServerManager();

	void OnReceive(CClientSock* pSock);
	void OnClose(CClientSock* pSock);
	void OnClose();
	void OnAccept();
	void InitSocket();
	void SetParent(void* pThis);
	BOOL SendData(CString strInfoData = _T(""));
	char* CStringToChar( CString tpStr);

protected:
	CServerSock* m_pServerSock, *m_pFileServerSock;

	void *m_pView;
	void* m_pParent;
	CTypedPtrList<CObList, CClientSock*> m_clientSocks;

	void Send(char *buf, int nSendDataSize);

	DECLARE_MESSAGE_MAP()
};


