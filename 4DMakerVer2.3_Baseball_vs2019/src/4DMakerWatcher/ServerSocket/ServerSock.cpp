// ServerSock.cpp : implementation file
//

#include "stdafx.h"
#include "ServerSock.h"
#include "MAServerManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CServerSock

CServerSock::CServerSock()
{
}

CServerSock::~CServerSock()
{
}


void CServerSock::SetParent(void* pParent) 
{
	m_pParent = pParent;
}


void CServerSock::OnAccept(int nErrorCode) 
{
	((MAServerManager*)m_pParent)->OnAccept();
	CAsyncSocket::OnAccept(nErrorCode);
}
