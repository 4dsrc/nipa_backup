#pragma comment(lib, "Userenv.lib")
#pragma comment(lib, "Wtsapi32.lib")

#define MAKERWATCHERPORT			20500
#define PACKETDATASIZE				1024 * 64
#define SERIAL_PORT_NAME				_T("\\Device\\ProlificSerial0")
#define SERIAL_PORT_NAME_4DA3			_T("\\Device\\USBSER000")
#define SERIAL_PORT_NAME2				_T("\\Device\\USBSER000")
#define REG_ADDRESS_SERIAL				_T("HARDWARE\\DEVICEMAP\\SERIALCOMM")
#define REG_NAME						_T("4DMakerWatcher")