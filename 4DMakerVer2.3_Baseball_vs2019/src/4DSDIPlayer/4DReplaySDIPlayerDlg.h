#pragma once
#include "resource.h"
#include "4DReplaySDIPlayerList.h"
#include "4DReplaySDIPlayerDecodingMgr.h"
#include "4DReplaySDIPlayerLog.h"
#include "4DSDITCPProcess.h"
#include "4DReplaySDIPlayerOptDlg.h"
#include "ntv2outputtestpattern.h"
#include "4DSDIPlayerIndexStructure.h"
#include "4DReplayGPUProcess.h"
#include "4DReplaySDIPlayerAutoAdd.h"
#include "afxcmn.h"
#include "afxwin.h"

class C4DReplaySDIPlayerList;
class C4DReplayDecodeMgr;
class C4DReplaySDIPlayerDivMgr;
class C4DSDITCPProcess;
class C4DReplaySDIPlayerLog;

class C4DReplaySDIPlayerDlg : public CDialogEx
{
// 생성입니다.
public:
	C4DReplaySDIPlayerDlg(CWnd* pParent = NULL,C4DReplaySDIPlayerLog* pLog =NULL);	// 표준 생성자입니다.
	~C4DReplaySDIPlayerDlg();
// 대화 상자 데이터입니다.
	enum { IDD = IDD_MY4DREPLAYSDIPLAYER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//afx_msg void OnBnClickedBtnLoadmovielist();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_MESSAGE_MAP()

public:
	//보드 연결 상태
	BOOL StartBoardConnection();
	AJAStatus ConnectBoard();
	BOOL m_bBoardState;
	void SetBoardState(BOOL m_b){m_bBoardState = m_b;}
	BOOL GetBoardState(){return m_bBoardState;}
	//파일경로
	void InsertMovieData(CString strPath,vector<MakeFrameInfo>*pArrFrameInfo,int nIdx,BOOL bCPU,BOOL bDiv,BOOL bSet = FALSE,int nFinish = -1);
	void DoFileDecode(CString strPath,int nIndex,BOOL bDivFrame=FALSE);
	//영상정보
	CString m_strPlayTime;
	CString m_strPlayFileName;

	//Variable delete
	void DeletePointer();

	//List 확장 뷰어
	BOOL m_bListViewOpen;
	CRect m_rcClientRect;
	CRect m_rcListRect;
	CRect m_rcProcessRect;
public: //Classes
	C4DReplaySDIPlayerList  *m_cList;
	C4DReplayDecodeMgr		*m_pDecodeMgr;
	C4DReplaySDIPlayerLog   *m_cLog;
	NTV2OutputTestPattern   *m_pBoard;
	C4DSDITCPProcess		*m_pTCP;
	C4DReplayGPUProcess*m_pGPU;

	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//afx_msg void OnBnClickedBtnLoadprocess();
	afx_msg void OnBnClickedButtonStop();
	afx_msg void OnBnClickedButtonPlayfile();

private:
	BOOL m_bPauseState;
	CString m_strLoadPath;
	int m_nPlayMethod;
public:
	afx_msg void OnBnClickedButtonAddmovie();
	afx_msg void OnBnClickedButtonRemovemovie();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	
	afx_msg void OnBnClickedRadioPlayOnce(UINT msg);
	BOOL m_bCheckDivPlay;
	afx_msg void OnBnClickedCheckDivplay();

	//Connection 
	CString m_strServerIP;
	CIPAddressCtrl m_ctrlServerIP;
	//BOOL GetConnectionCheck(CString strIP);
	//char* CStringToChar(CString str);
	CString GetIP();

	//AJAOption
	CString GetLocalIPAdress();
	AJAOption m_stAJAOpt;
	void InitializeOption(AJAOption* Opt);
	void SaveOption(AJAOption* Opt);
	void SetOptInfoString(CString strKeyName,CString strValue);
	CString GetOptInfoString(CString strKeyName,CString strDefault);
	afx_msg void OnBnClickedButtonOption();
	
	afx_msg void OnBnClickedButtonReconnect();

	//IP
	CString m_strIP;
	BOOL m_bInsertLogo;
	BOOL m_bLogoSave;
	afx_msg void OnBnClickedCheckInsertLogo();
	afx_msg void OnEnChangeEditWaitTime();

	//FrameSpeed
	CComboBox m_cbFrameSpeed;
	int	m_nFrameSel;
	CString m_strFPS;
	void SetFrameSpeed(int nIdx);
	afx_msg void OnCbnSelchangeComboFrameSpeed();
	afx_msg void OnEnChangeEditFrameSpeed();
	afx_msg void OnIpnFieldchangedIpaddress1(NMHDR *pNMHDR, LRESULT *pResult);
	BOOL m_bScreen;
	afx_msg void OnBnClickedCheckScreenshot();
	static unsigned WINAPI _ScreenShotThread(LPVOID param);

	void LaunchGPUThread();
	static unsigned WINAPI _GPULoadThread(LPVOID param);
//	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBnClickedCheckLogoSave();
	BOOL m_bEncode;
	BOOL m_bTransTS;
	afx_msg void OnBnClickedCheckEncode();
	afx_msg void OnBnClickedCheckAutoadd();

	//AutoAdd
	BOOL m_bAutoAddThread;
	BOOL m_bAutoAdd;
	BOOL m_bAutoAddDlgOpen;
	void SetAutoAdd(BOOL b){m_bAutoAdd = b;}
	BOOL GetAutoAdd(){return m_bAutoAdd;}
	void SetAutoDlgOpen(BOOL b){m_bAutoAddDlgOpen = b;}
	BOOL GetAutoDlgOpen(){return m_bAutoAddDlgOpen;}
	BOOL GetCheckConnection(CString strIP);
	void ReadInIFile(CString strIP);
	static unsigned WINAPI _AutoAddThread(LPVOID param);
	static unsigned WINAPI _AutoPlayThread(LPVOID param);
	vector<CString>m_arrAutoPlayPath;
	void AddAutoPlay(CString str)
	{
		m_arrAutoPlayPath.push_back(str);
	}
	CString GetAutoPlay()
	{
		CString str = m_arrAutoPlayPath.at(0);
		return str;
	}
	int GetAutoPlaySize()
	{
		int nSize = m_arrAutoPlayPath.size();
		return nSize;
	}
	void DeleteAutoPlay()
	{
		m_arrAutoPlayPath.erase(m_arrAutoPlayPath.begin());
	}
	afx_msg void OnBnClickedButtonAutoadd();	
	afx_msg void OnEnChangeEditEncodeBitrate();

	//190107 hjcho
	int m_nEncodeBitrate;
	afx_msg void OnBnClickedCheckTransts();
};