//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 4DReplaySDIPlayer.rc
//
#define IDD_MY4DREPLAYSDIPLAYER_DIALOG  102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_OPT                  129
#define IDR_MENU_MOVIELIST              130
#define IDD_DIALOG_GPULOAD              131
#define IDD_AUTOADD                     132
#define IDC_LISTCTRL_MOVIE_LIST         1002
#define IDC_STATIC_BOARDSTATE           1005
#define IDC_BUTTON_PREVFILE             1006
#define IDC_BUTTON_PLAYFILE             1007
#define IDC_BUTTON_NEXTFILE             1008
#define IDC_BUTTON_STOP                 1009
#define IDC_BUTTON_ADDMOVIE             1010
#define IDC_BUTTON_REMOVEMOVIE          1011
#define IDC_RADIO_PLAY_ONCE             1012
#define IDC_RADIO_PLAY_INFINITE         1013
#define IDC_RADIO_PLAY_ALL              1014
#define IDC_LISTCTRL_LOG                1016
#define IDC_CHECK1                      1017
#define IDC_CHECK_DIVPLAY               1017
#define IDC_IPADDRESS1                  1018
#define IDC_BUTTON_OPTION               1019
#define IDC_OPT_EDIT_OUTPUT             1020
#define IDC_EDIT2                       1021
#define IDC_OPT_EDIT_BACK               1022
#define IDC_OPT_EDIT_LOGO               1023
#define IDC_OPT_EDIT_LOG                1024
#define IDC_OPT_EDIT_INFO               1025
#define IDC_BUTTON1                     1026
#define IDC_BUTTON_RECONNECT            1026
#define IDC_OPT_BTN_INFO                1026
#define IDC_AUTOADD_ADDIP               1026
#define IDC_CHECK_INSERT_LOGO           1027
#define IDC_EDIT_WAIT_TIME              1028
#define IDC_COMBO_FRAME_SPEED           1029
#define IDC_EDIT_FRAME_SPEED            1030
#define IDC_OPT_BTN_OUTPUT              1031
#define IDC_OPT_BTN_LOG                 1033
#define IDC_OPT_BTN_BACK                1034
#define IDC_OPT_BTN_LOGO                1035
#define IDC_CHECK_SCREENSHOT            1036
#define IDC_STATIC_GPU_LOAD             1037
#define IDC_CHECK3                      1039
#define IDC_CHECK_LOGO_SAVE             1039
#define IDC_CHECK2                      1040
#define IDC_CHECK_ENCODE                1040
#define IDC_CHECK_AUTOADD               1041
#define IDC_BUTTON_AUTOADD              1042
#define IDC_AUTOADD_LIST                1043
#define IDC_AUTOADD_IPADDRESS           1044
#define IDC_BUTTON2                     1046
#define IDC_AUTOADD_DELETE              1046
#define IDC_EDIT1                       1047
#define IDC_EDIT_ENCODE_BITRATE         1047
#define IDC_CHECK4                      1048
#define IDC_CHECK_TRANSTS               1048
#define ID_LIST_ADDAUTOPLAY             32771
#define ID_LIST_ADDBACKUPLIST           32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1049
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
