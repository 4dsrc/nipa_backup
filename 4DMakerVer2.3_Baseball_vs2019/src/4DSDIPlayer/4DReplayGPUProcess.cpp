// C4DReplayGPUProcess.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DReplaySDIPlayer.h"
#include "4DReplayGPUProcess.h"
#include "afxdialogex.h"


// C4DReplayGPUProcess 대화 상자입니다.

IMPLEMENT_DYNAMIC(C4DReplayGPUProcess, CDialogEx)

C4DReplayGPUProcess::C4DReplayGPUProcess(CWnd* pParent /*=NULL*/)
	: CDialogEx(C4DReplayGPUProcess::IDD, pParent)
{

}

C4DReplayGPUProcess::~C4DReplayGPUProcess()
{
}

void C4DReplayGPUProcess::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(C4DReplayGPUProcess, CDialogEx)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// C4DReplayGPUProcess 메시지 처리기입니다.


BOOL C4DReplayGPUProcess::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void C4DReplayGPUProcess::LaunchModal()
{
	DISPLAY_DEVICE Display;
	WCHAR GraphicCard[128];

	ZeroMemory( GraphicCard, sizeof( GraphicCard ) );
	memset( &Display, 0, sizeof( DISPLAY_DEVICE ) );
	Display.cb = sizeof( Display );

	int nCnt = 0;
	CString strGraphic;// = Display.DeviceString;

	while(1)
	{
		Sleep(1);
		EnumDisplayDevices( NULL, nCnt, &Display, 0 );
		strGraphic = Display.DeviceString;

		if(strGraphic.GetLength() == FALSE)
			break;

		//ESMLog(5,strGraphic);
		if(strGraphic.Find(_T("NVIDIA GeForce")) != -1)
			break;

		if(strGraphic.Find(_T("NVIDIA")) != -1)
			break;

		nCnt++;
	}

	if(strGraphic.GetLength())
	{
		m_bTimeLoadFinish = FALSE;

		/*HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)_beginthreadex(NULL,0,_LaunchTimeThread,(void*)this,0,NULL);
		CloseHandle(hSyncTime);*/

		//ShowWindow(SW_SHOW);
		//G4DShowLog(5,strGraphic);
		cv::Mat *frame = new cv::Mat(1,1,CV_8UC3);
		cv::cuda::GpuMat pMat(*frame);
		//ShowWindow(SW_HIDE);
		if(frame)
		{
			delete frame;
			frame = NULL;
		}
		m_bTimeLoadFinish = TRUE;
		G4DShowLog(0,_T("Graphic Card Load Finish"));
	}
	else
		G4DShowLog(0,_T("No Graphic Card...."));
}
unsigned WINAPI C4DReplayGPUProcess::_LaunchTimeThread(LPVOID param)
{
	C4DReplayGPUProcess* pGPU = (C4DReplayGPUProcess*) param;

	int nStart = GetTickCount();
	while(1)
	{
		if(pGPU->m_bTimeLoadFinish == FALSE)
		{
			pGPU->ChangeTime(GetTickCount() - nStart);
		}
		else
			break;

		Sleep(10);
	}
	return FALSE;
}
void C4DReplayGPUProcess::ChangeTime(int nTime)
{
	CString str;
	str.Format(_T("Loading: %d"),nTime);
	//GetDlgItem(IDC_STATIC_GPU_LOAD)->SetWindowText(str);
}
void C4DReplayGPUProcess::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	if(bShow)
		ShowWindow(SW_SHOW);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}
