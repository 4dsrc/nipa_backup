#include "stdafx.h"
#include "4DReplayDivMgr.h"
#include "ESMFileOperation.h"
#include "ESMIni.h"
#include "ESMUtil.h"

C4DReplaySDIPlayerDivMgr::C4DReplaySDIPlayerDivMgr()
{
	//m_bThreadState = FALSE;
	//m_pParent	   = NULL;
	m_pDlg = NULL;
	m_pParent = NULL;
	m_bGPURun = FALSE;
	m_nFPS	  = 33;
	m_nFrame  = 30;
	m_bGPUProcess = FALSE;
	m_nInsertCnt = 0;

	InitializeCriticalSection(&m_criInsert);
	m_nGPUMakingCnt = 99999;
}
C4DReplaySDIPlayerDivMgr::C4DReplaySDIPlayerDivMgr(C4DReplayDecodeMgr*pParent)
{
	m_pParent = pParent;
	m_bGPURun = FALSE;
	m_nFPS	  = 33;
	m_nFrame  = 30;
	CounterStart = 0;
	PCFreq		= 0;

	m_nActiveCoreNum = 0;

	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);
	m_nActiveCoreNum = sysinfo.dwNumberOfProcessors / 2;

	CString strLog;
	strLog.Format(_T("Will use core num: %d"),m_nActiveCoreNum);
	G4DShowLog(5,strLog);

	m_bGPUProcess = FALSE;
	m_nInsertCnt = 0;

	InitializeCriticalSection(&m_criInsert);

	m_nGPUMakingCnt = 99999;
}
C4DReplaySDIPlayerDivMgr::~C4DReplaySDIPlayerDivMgr()
{

}
void C4DReplaySDIPlayerDivMgr::SetParent(C4DReplaySDIPlayerDlg*pDlg,C4DReplayDecodeMgr*pParent)
{
	m_pDlg = pDlg;
	m_pParent = pParent;
}
void C4DReplaySDIPlayerDivMgr::ClearAllAdjust()
{
	m_mpAdjData.clear();
	m_mpRotMat.clear();
}
void C4DReplaySDIPlayerDivMgr::SetAdjData(stAdjustInfo* pAdj,CString strDSCID)
{
	m_mpAdjData.insert(pair<CString,stAdjustInfo>(strDSCID,*pAdj));
	SetRotMat(strDSCID,*pAdj);
}
stAdjustInfo C4DReplaySDIPlayerDivMgr::GetAdjData(CString strDSCID)
{
	stAdjustInfo Adjinfo;
	Adjinfo = m_mpAdjData[strDSCID];

	return Adjinfo;
}
void C4DReplaySDIPlayerDivMgr::SetRotMat(CString strDSCID,stAdjustInfo info)
{
	double dScale = info.AdjSize;
	double dAngle = info.AdjAngle;

	int nCenterX = Round(info.AdjptRotate.x);
	int nCenterY = Round(info.AdjptRotate.y);

	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;

	Mat rot_mat(cv::Size(2, 3), CV_32FC1);
	cv::Point rot_center( nCenterX, nCenterY);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust = -1 * (dAngle + 90);   //Normal Movie;

	rot_mat = getRotationMatrix2D(rot_center, dbAngleAdjust, dScale);
	
	int nMovX = Round(info.AdjMove.x / dScale);
	int nMovY = Round(info.AdjMove.y / dScale);
	rot_mat.at<double>(0,2) += nMovX;
	rot_mat.at<double>(1,2) += nMovY;

	m_mpRotMat.insert(pair<CString,Mat>(strDSCID,rot_mat));
}
Mat C4DReplaySDIPlayerDivMgr::GetRotMat(CString strDSCID)
{
	Mat rot_mat;
	rot_mat = m_mpRotMat[strDSCID].clone();

	return rot_mat;
}
void C4DReplaySDIPlayerDivMgr::StartDivCPUFrame(CString strPath,stDivideFrame* pDiv,int nIdx)
{
	//G4DShowLog(5,_T("StartDivCPUFrame"));
	FRAME_CPU_INFO *info = new FRAME_CPU_INFO;
	info->pDiv			 = pDiv;
	info->strPah		 = strPath;
	info->pParent		 = m_pParent;
	info->nIdx			 = nIdx;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,_FrameCPUThread,(void*)info,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI C4DReplaySDIPlayerDivMgr::_FrameCPUThread(LPVOID param)
{
	FRAME_CPU_INFO* info = (FRAME_CPU_INFO*) param;
	stDivideFrame* pDiv	 = info->pDiv;
	C4DReplayDecodeMgr* pParent = info->pParent;
	CString strPath		 = info->strPah;
	int nIdx			 = info->nIdx;
	delete info;

	//CString csLog;
	//csLog.Format(_T("[%d]Start div frame"),nIdx);
	//G4DShowLog(5,csLog);

	CT2CA pszConvertedAnsiString (strPath);
	string strDst(pszConvertedAnsiString);
	
	CString str;
	//str.Format(_T("[%d] CPU Start!"),nIdx);
	//G4DShowLog(5,str);
	Mat frame;
	VideoCapture vc(strDst);
	if(!vc.isOpened())
	{
		BOOL bLoad = FALSE;
		for(int i = 0 ; i < 100; i++)
		{
			Sleep(10);
			if(vc.open(strDst))
			{
				bLoad = TRUE;
				break;
			}
		}
		if(bLoad == FALSE)
		{
			pDiv->nFinish = MAKING_FAIL;
			str.Format(_T("[%d] CPU FAIL!"),nIdx);
			G4DShowLog(0,str);
			return FALSE;
		}
	}
	
	Mat Logo(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);
	Mat NoLogo(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);
	int nFrameCnt  = 0;
	
	char strData[100];
	BOOL bTest = pParent->GetTest();
	while(1)
	{
		vc>>frame;
		if(frame.empty())
			break;

		if(bTest)
		{
			sprintf(strData,"%d - %d",nIdx,nFrameCnt);
			putText(frame,strData,cv::Point(480,540),CV_FONT_ITALIC,5.,Scalar(255,255,255),2,8);
		}

		if(G4DGetTargetHeight() != frame.rows)
			resize(frame,frame,cv::Size(G4DGetTargetWidth(),G4DGetTargetHeight()),0,0,1);

		if(G4DGetStabilization())
		{
			//Stabilizer 회전 구간 관심영역
			Mat roiImage;
			Rect2d rect = Rect2d(frame.cols/4*1,frame.rows/3*1,frame.cols/4*2,frame.rows/3*1);
			frame(rect).copyTo(roiImage);

			blur(roiImage, roiImage, cv::Size(41, 41));

			stLoadFrame stLoad;
			stLoad.StabFrame = frame.clone();
			stLoad.roiImage = roiImage.clone();
			stLoad.bFinish = TRUE;
			stLoad.nFps = 1;
			pParent->InsertMovieData(nIdx,-1,stLoad,TRUE);
			
			/*char strPath[100];
			sprintf(strPath,"M:\\Movie\\ORI_[%d]_%d.png",nIdx,nFrameCnt);
			imwrite(strPath,frame);*/
		}
		else
		{
			/*pParent->BGR2YUV422(frame,YUV422,G4DGetTargetHeight(),G4DGetTargetWidth(),bInsertLogo);*/
			pParent->BGRCC2YUV422Logo(frame,Logo,NoLogo,G4DGetTargetHeight(),G4DGetTargetWidth(),FALSE,FrameRGBInfo());
			stLoadFrame stLoad;
			stLoad.Logo= Logo.clone();
			stLoad.NoLogo= NoLogo.clone();
			stLoad.bFinish = TRUE;
			stLoad.nFps = 1;
			pParent->InsertMovieData(nIdx,-1,stLoad,TRUE);
		}
		nFrameCnt++;
	}
	pDiv->nFinish = G4DGetStabilization() ? MAKING_STABILIZATION : MAKING_FINISH;

	/*if(pDiv->nFinish == MAKING_FINISH)
		str.Format(_T("[%d] CPU Finish![%d]"),nIdx,nFrameCnt);*/

	str.Format(_T("[%d] CPU Finish![%d][%d]"),nIdx,nFrameCnt,pDiv->nFinish);
	G4DShowLog(5,str);
	vc.release();

	CESMFileOperation fo;
	if(fo.IsFileExist(strPath))
	{
		while(1)
		{
			if(fo.Delete(strPath))
				break;

			Sleep(1);
		}
	}
	//csLog.Format(_T("[%d]Finish div frame"),nIdx);
	//G4DShowLog(5,csLog);
	return TRUE;
}
void C4DReplaySDIPlayerDivMgr::StartDivGPUFrame(vector<MakeFrameInfo>*pArrFrameInfo,stDivideFrame* pDiv,int nIdx)
{
	FRAME_GPU_DECODE_INFO* info = new FRAME_GPU_DECODE_INFO;
	info->pDiv				= pDiv;
	info->pParent			= m_pParent;
	info->pArrFrameInfo		= pArrFrameInfo;
	info->nIdx				= nIdx;
	
	if(G4DGetStabilization())
	{
		BOOL bStabilFrame = FALSE;
		int nSize = pArrFrameInfo->size();
		int nFirst = pArrFrameInfo->at(0).nStabilizerFrame;
		int nLast = pArrFrameInfo->at(nSize-1).nStabilizerFrame;
		if(nFirst != 0)
			bStabilFrame = TRUE;
		if(nLast != 0)
			bStabilFrame = TRUE;

		m_pParent->SetStabilIncludeFrame(nIdx,bStabilFrame);
		info->bStabilObject = bStabilFrame;

		/*CString strLog;
		strLog.Format(_T("[%d] GPU Input"),nIdx);
		G4DShowLog(5,strLog);*/

		EnterCriticalSection(&m_criInsert);
		m_pArrDecodeInfo.push_back(info);
		LeaveCriticalSection(&m_criInsert);

		m_nInsertCnt++;
	}
	else
	{
		m_pArrDecodeInfo.push_back(info);
	}
	if(m_bGPURun == FALSE)
	{
		m_bGPURun = TRUE;
		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE) _beginthreadex(NULL,0,_FrameGPUArrThread,(void*)this,0,NULL);
		CloseHandle(hSyncTime);
	}
}
unsigned WINAPI C4DReplaySDIPlayerDivMgr::_FrameGPUArrThread(LPVOID param)
{
	C4DReplaySDIPlayerDivMgr* pDivMgr = (C4DReplaySDIPlayerDivMgr*) param;
	int nCnt = 0;
	BOOL bFind = FALSE;
	BOOL bFirst = FALSE;
	pDivMgr->m_strStabPrevDSC = _T("");
	int nStabilIndex = 0;
	
	if(G4DGetStabilization())
	{
		while(1)
		{
			if(pDivMgr->m_nInsertCnt  == G4DGetGPUTotalMakingCnt())
				break;

			Sleep(1);
		}
	}
	while(1)
	{
		Sleep(1);
		if(pDivMgr->m_pArrDecodeInfo.size())
		{
			//FrameDecode
			pDivMgr->SetGPUProcess(TRUE);
			BOOL bStabilFind = FALSE;
			
			EnterCriticalSection(&pDivMgr->m_criInsert);
			if(G4DGetStabilization())
			{
				nStabilIndex = 0;
				for(int i = 0 ; i < pDivMgr->m_pArrDecodeInfo.size(); i++)
				{
					if(pDivMgr->m_pArrDecodeInfo.at(i)->bStabilObject)
					{
						nStabilIndex = i;
						break;
					}
				}
			}
			FRAME_GPU_DECODE_INFO* pGPUInfo = pDivMgr->m_pArrDecodeInfo.at(nStabilIndex);
			LeaveCriticalSection(&pDivMgr->m_criInsert);
			//int nSize = pGPUInfo->pArrFrameInfo->size();
			/*CString strLog;
			strLog.Format(_T("[%d] GPU Decoding Start"),pGPUInfo->nIdx);
			G4DShowLog(5,strLog);*/
			if(bFind == FALSE)
			{
				pDivMgr->m_nHalfIndex = pGPUInfo->pArrFrameInfo->at(0).nFrameIndex;
				bFind = TRUE;
			}
			if(bFirst == FALSE)
			{
				CString strPath;
				strPath.Format(_T("%s"),pGPUInfo->pArrFrameInfo->at(0).strFramePath);
				CString strDSCID = pDivMgr->GetDSCIDFromPath(strPath);

				pDivMgr->m_strStabPrevDSC = strDSCID;
				
				bFirst = TRUE;
			}

			pDivMgr->DoDecodeGPUArrFrame(pGPUInfo);

			//strLog.Format(_T("[%d] GPU Decoding Finish[%d]"),pGPUInfo->nIdx,nSize);
			//G4DShowLog(5,strLog);

			delete pGPUInfo;
			pGPUInfo = NULL;

			EnterCriticalSection(&pDivMgr->m_criInsert);
			pDivMgr->m_pArrDecodeInfo.erase(pDivMgr->m_pArrDecodeInfo.begin()+nStabilIndex);
			LeaveCriticalSection(&pDivMgr->m_criInsert);
		}

		nCnt++;
		if(nCnt > 100 && pDivMgr->m_pArrDecodeInfo.size() == 0)
		{
			pDivMgr->m_bGPURun = FALSE;
			pDivMgr->m_nInsertCnt = 0;
			break;
		}
	}
	pDivMgr->SetGPUProcess(FALSE);
	return TRUE;
}
void C4DReplaySDIPlayerDivMgr::DoDecodeGPUArrFrame(FRAME_GPU_DECODE_INFO* info)
{
	int nIdx = info->nIdx;
	vector<MakeFrameInfo>*pArrFrameInfo = info->pArrFrameInfo;
	int nTotalArrSize = pArrFrameInfo->size() - 1;

	CString strPath;
	strPath.Format(_T("%s"),pArrFrameInfo->at(0).strFramePath);
	CString strDSCID = GetDSCIDFromPath(strPath);

	if(G4DGetStabilization())
	{
		if(m_strStabPrevDSC == strDSCID)
			m_pParent->SetStabilValue(nIdx,FALSE);
			//m_pParent->m_pMapMovieArray[nIdx]->bIsLastFrame = FALSE;
		else
			m_pParent->SetStabilValue(nIdx,TRUE);
			//m_pParent->m_pMapMovieArray[nIdx]->bIsLastFrame = TRUE;
	}

	stAdjustInfo Adj = GetAdjData(strDSCID);

	CT2CA pszConvertedAnsiString (strPath);
	string strDst(pszConvertedAnsiString);

	CString strTempLog;
	strTempLog.Format(_T("[%d] Decode: %s"),nIdx,strPath);
	G4DShowLog(5,strTempLog);

	int nSkipCnt = 0;
	Mat frame;
	VideoCapture vc(strDst);
	int nWaitCnt = 0;
	if(!vc.isOpened())
	{
		BOOL bOpen = FALSE;
		while(1)
		{
			if(vc.isOpened())
			{
				bOpen = TRUE;
				break;
			}
			if(nWaitCnt == 200)
				break;

			nWaitCnt++;
			Sleep(10);
		}
		if(bOpen == FALSE)
		{
			info->pDiv->nFinish = MAKING_FAIL;
			
			for(int i = 0 ; i < pArrFrameInfo->size(); i++)
			{
				pArrFrameInfo->at(i).bComplete = -100;
			}
			CString strLog;
			strLog.Format(_T("[%d] Decoding Error : %s"),nIdx,strPath);
			G4DShowLog(0,strLog);
			
			RunAdjustThread(nIdx,1920,1080,strDSCID,pArrFrameInfo);

			return;
		}
	}
	int nWidth  = vc.get(CV_CAP_PROP_FRAME_WIDTH);
	int nHeight = vc.get(CV_CAP_PROP_FRAME_HEIGHT);

	RunAdjustThread(nIdx,nWidth,nHeight,strDSCID,pArrFrameInfo);

	int nStartIdx = pArrFrameInfo->at(0).nFrameIndex;
	int nEndIdx	  = pArrFrameInfo->at(nTotalArrSize).nFrameIndex;
	int nCnt = 0;
	
	BOOL bStart = FALSE;
	BOOL bColor = FALSE;
	FrameRGBInfo RGBinfo;

	//if(nStartIdx < nEndIdx)
	//{
	//	vector<Mat> arrFrame;
	//	while(1)
	//	{
	//		vc>>frame;
	//		if(frame.empty())
	//			break;

	//		arrFrame.push_back(frame);
	//	}
	//	for(int i = 0 ; i < pArrFrameInfo->size() ; i++)
	//	{
	//		int nIdx = pArrFrameInfo->at(i).nFrameIndex;
	//		Mat frame = arrFrame.at(nIdx).clone();

	//		MakeFrameInfo* pFrame = &pArrFrameInfo->at(i);
	//		if(pFrame->bMovieReverse)
	//			flip(frame,frame,-1);

	//		if(pFrame->bColorRevision && bColor == FALSE)
	//		{
	//			GetColorAvg(frame,&RGBinfo,pFrame->stColorInfo,
	//				pFrame->stEffectData.nPosX,pFrame->stEffectData.nPosY,pFrame->stEffectData.nZoomRatio);
	//			bColor = TRUE;
	//			//info = &pFrame->stAvgColorInfo;
	//			/*FrameRGBInfo info = pFrame->stColorInfo;
	//			DoColorRevision(&frame,info);*/
	//		}
	//		pFrame->stAvgColorInfo = RGBinfo;
	//		pFrame->Image = new BYTE[nWidth*nHeight*3];
	//		memcpy(pFrame->Image,frame.data,nWidth*nHeight*3);
	//		pFrame->bComplete ++;
	//	}
	//}
	//else
	{
		while(1)
		{
			//StartCounter();
			vc>>frame;

			if(nCnt != nStartIdx && !bStart)
			{
				nCnt++;
				continue;
			}
			if(frame.empty())	
				break;

			if(m_pParent->GetMovieSignal() == MOVIE_STOP/* && nIdx != 0*/)
				break;

			MakeFrameInfo *pFrame = &(pArrFrameInfo->at(nCnt-nStartIdx));

			bStart = TRUE;

			/*if(pFrame->nFrameSpeed/G4DGetRecordingFrameCnt() < 1)
			{
			if(nCnt%2 == m_nHalfIndex%2)
			{
			pFrame->bComplete  = -100;
			nCnt++;
			continue;
			}
			}*/

			if(pFrame->bMovieReverse)
				flip(frame,frame,-1);

			if(pFrame->bColorRevision && bColor == FALSE)
			{
				GetColorAvg(frame,&RGBinfo,pFrame->stColorInfo,
					pFrame->stEffectData.nPosX,pFrame->stEffectData.nPosY,pFrame->stEffectData.nZoomRatio);
				bColor = TRUE;
				//info = &pFrame->stAvgColorInfo;
				/*FrameRGBInfo info = pFrame->stColorInfo;
				DoColorRevision(&frame,info);*/
			}
			pFrame->stAvgColorInfo = RGBinfo;
			pFrame->Image = new BYTE[nWidth*nHeight*3];
			memcpy(pFrame->Image,frame.data,nWidth*nHeight*3);
			pFrame->bComplete ++;
			/*CString strLog;
			strLog.Format(_T("[%d][%d] Decoding Finish - %.2f"),nIdx,nCnt-nStartIdx,GetCounter());
			G4DShowLog(5,strLog);*/

			nCnt++;
			if(nCnt > nEndIdx)
				break;
			if(nCnt-nStartIdx >= pArrFrameInfo->size())
				break;
		}
	}

	if(pArrFrameInfo)
	{
		for(int i = 0 ; i < pArrFrameInfo->size(); i++)
		{
			if(pArrFrameInfo->at(i).bComplete == TRUE)
				continue;
			else
				pArrFrameInfo->at(i).bComplete = -100;
		}
	}
	vc.release();

	//if(pArrFrameInfo)
	//{
	//	pArrFrameInfo->clear();
	//	delete  pArrFrameInfo;
	//	pArrFrameInfo = NULL;
	//}
}
void C4DReplaySDIPlayerDivMgr::RunAdjustThread(int nIdx,int nWidth,int nHeight,CString strDSCID,vector<MakeFrameInfo>*pArrFrameInfo)
{
	FRAME_ADJUST_INFO* pAdjInfo = new FRAME_ADJUST_INFO;
	pAdjInfo->strDSCID	= strDSCID;
	pAdjInfo->nFrameWidth = nWidth;
	pAdjInfo->nFrameHeight = nHeight;
	pAdjInfo->pArrFrameInfo = pArrFrameInfo;
	pAdjInfo->pMgr					= this;
	pAdjInfo->pParent				= m_pParent;
	pAdjInfo->nIdx					= nIdx;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_FrameAdjustThread,(void*)pAdjInfo,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI C4DReplaySDIPlayerDivMgr::_FrameAdjustThread(LPVOID param)
{
	FRAME_ADJUST_INFO* pInfo = (FRAME_ADJUST_INFO*)param;
	vector<MakeFrameInfo>*pArrFrameInfo = pInfo->pArrFrameInfo;
	C4DReplaySDIPlayerDivMgr* pMgr		 = pInfo->pMgr;
	C4DReplayDecodeMgr* pParent			= pInfo->pParent;
	int nWidth = pInfo->nFrameWidth;
	int nHeight = pInfo->nFrameHeight;
	int nIdx			= pInfo->nIdx;
	CString strDSCID = pInfo->strDSCID;
	delete pInfo;pInfo = NULL;

	stAdjustInfo Adj = pMgr->GetAdjData(strDSCID);
	Mat RotMat = pMgr->GetRotMat(strDSCID);
	Mat Logo(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);
	Mat NoLogo(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);

	int nRotCenterX = nWidth/2;
	int nRotCenterY = nHeight/2;

	//Calc Margin
	int nMarginX  = Adj.stMargin.nMarginX;
	int nMarginY  = Adj.stMargin.nMarginY;
	double dbMarginScale = 1 / ( (double)( nWidth - nMarginX * 2) /(double) nWidth);
	int nLeft = (int)((nWidth  * dbMarginScale - nWidth)/2);
	int nTop  = (int)((nHeight * dbMarginScale - nHeight)/2);

	int nH = nHeight*dbMarginScale;
	int nW = nWidth*dbMarginScale;

	cuda::GpuMat gpuReFrame,gMatCut,gpuResize;
	BOOL bInsertLogo = G4DGetInsertLogo();

	char strData[100];

	BOOL bTest = pParent->GetTest();
	//stabilizer 정보 탐색
	int nFirst = 0;

	for(int i = 0 ; i < pArrFrameInfo->size(); i++)
	{
		MakeFrameInfo* pFrameInfo = &(pArrFrameInfo->at(i));
		pMgr->StartCounter();
	
		EffectInfo stEffectData;
		stEffectData = pArrFrameInfo->at(i).stEffectData;
		
		double dRatio = (double)stEffectData.nZoomRatio / 100.0;
		if(dRatio < 1)
			dRatio = 1.;
		int nZoomLeft  = stEffectData.nPosX - nWidth/dRatio/2;
		int nZoomTop   = stEffectData.nPosY - nHeight/dRatio/2;

		int nZoomWidth = nWidth/dRatio - 1;
		if((nZoomLeft + nZoomWidth) > nWidth)
			nZoomLeft = nZoomLeft - ((nZoomLeft + nZoomWidth) - nWidth);
		else if(nZoomLeft < 0)
			nZoomLeft = 0;

		int nZoomHeight = nHeight/dRatio - 1;
		if((nZoomTop + nZoomHeight) > nHeight)
			nZoomTop = nZoomTop - ((nZoomTop+nZoomHeight) - nHeight);
		else if(nZoomTop < 0)
			nZoomTop = 0;

		while(1)
		{
			Sleep(1);

			if(pFrameInfo->bComplete == TRUE)
				break;

			if(pFrameInfo->bComplete == -100)
				break;
		}
		if(pFrameInfo->bComplete == -100/* || pFrameInfo->Image == NULL*/)
		{
			stLoadFrame stLoad;
			stLoad.Logo = NULL;
			stLoad.NoLogo = NULL;
			stLoad.bFinish = -100;
			stLoad.nFps = 1;
			if(pParent->GetMovieSignal() == MOVIE_STOP)
				break;
			pParent->InsertMovieData(nIdx,i,stLoad,TRUE);

			continue;
		}
		
		Mat frame(nHeight,nWidth,CV_8UC3,pFrameInfo->Image);
		Mat temp(G4DGetTargetHeight(),G4DGetTargetWidth(),CV_8UC3);
		/*if(pFrameInfo->bColorRevision)
		{
		pMgr->DoColorRevision(&frame,pFrameInfo->stAvgColorInfo);
		}*/
		if(pFrameInfo->nVertical == 0)//Adapt adjust
		{
			double dbColor = pMgr->GetCounter();
			
			if(pParent->GetMovieSignal() == MOVIE_STOP)
				break;

			/*GPU Making*/
			cuda::GpuMat gpuFrame(frame);
			//pMgr->GpuRotateImage(&gpuFrame,RotMat);
			pMgr->GpuRotateImage(&gpuFrame,pMgr->Round(Adj.AdjptRotate.x),pMgr->Round(Adj.AdjptRotate.y),Adj.AdjSize,Adj.AdjAngle);
			pMgr->GpuMoveImage(&gpuFrame,pMgr->Round(Adj.AdjMove.x),pMgr->Round(Adj.AdjMove.y));

			cuda::resize(gpuFrame,gpuResize,cv::Size(nWidth*dbMarginScale,nHeight*dbMarginScale),0,0,CV_INTER_CUBIC);
			gpuReFrame = (gpuResize)(cv::Rect(nLeft,nTop,nWidth,nHeight));

			if(G4DGetReverseMovie())
			{
				//Mat tempRot;
				//gpuReFrame.download(tempRot);
				//flip(tempRot,tempRot,-1);
				//gpuReFrame.upload(tempRot);
				pMgr->GpuRotateImage(&gpuReFrame,nRotCenterX,nRotCenterY,1,Adj.AdjAngle,TRUE);
			}
			gpuReFrame.copyTo(gpuFrame);

			gMatCut = (gpuFrame)(cv::Rect(nZoomLeft,nZoomTop,nZoomWidth,nZoomHeight));
			cuda::resize(gMatCut,gpuResize,cv::Size(G4DGetTargetWidth(),G4DGetTargetHeight()),0,0,CV_INTER_CUBIC);

			gpuResize.download(temp);
			if(pParent->GetMovieSignal() == MOVIE_STOP)
				break;
		}
		else
		{
			if(G4DGetReverseMovie())
				flip(frame,frame,-1);

			double degree = 0;
			double resizeRatio = (16./9);

			Mat rotateImage(frame.cols, frame.rows, CV_8UC3);
			if(pFrameInfo->nVertical == -1)
			{
				transpose(frame, rotateImage);
				flip(rotateImage, rotateImage, 1);
			}

			else if(pFrameInfo->nVertical == 1)
			{
				transpose(frame, rotateImage);
				flip(rotateImage, rotateImage, 0);
			}

			Point2f ptVerticalCenterPoint;
			ptVerticalCenterPoint.x = pFrameInfo->ptVerticalCenterPoint.x;
			ptVerticalCenterPoint.y = pFrameInfo->ptVerticalCenterPoint.y;

			Rect2d rtRoiRect;
			rtRoiRect.width = frame.rows;
			rtRoiRect.height = frame.rows* (9./16);
			rtRoiRect.x = 0;
			rtRoiRect.y = (frame.cols * (ptVerticalCenterPoint.y / frame.rows)) - (rtRoiRect.height/2);

			if(rtRoiRect.y < 0)
				rtRoiRect.y = 0;
			if(rtRoiRect.y + rtRoiRect.height > frame.cols)
				rtRoiRect.y = frame.cols - rtRoiRect.height-1;
			rotateImage(rtRoiRect).copyTo(frame);

			Mat MatCut;

			//Template/////////////////////
			//////////////
			//resize(src, src, Size(nSrcWidth, nSrcHeight), CV_INTER_LINEAR);

			double dScale = (double)stEffectData.nZoomRatio / 100.0;

			pFrameInfo->nWidth = frame.cols;
			pFrameInfo->nHeight= frame.rows;

			//180524
			ptVerticalCenterPoint.x *= (double)frame.cols/nWidth;
			ptVerticalCenterPoint.y = (double)frame.rows/2;

			//180525
			double nWidthBoundRatio_Left =  ((double)frame.cols/2) - ((double)frame.cols/2) * pow((double)frame.rows/frame.cols, 2);
			double nWidthBoundRatio_Right = ((double)frame.cols/2) + ((double)frame.cols/2) * pow((double)frame.rows/frame.cols, 2);

			if( ptVerticalCenterPoint.x < nWidthBoundRatio_Left)
				ptVerticalCenterPoint.x = nWidthBoundRatio_Left;
			if( ptVerticalCenterPoint.x > nWidthBoundRatio_Right)
				ptVerticalCenterPoint.x = nWidthBoundRatio_Right;

			ptVerticalCenterPoint.x = 
				abs(ptVerticalCenterPoint.x - nWidthBoundRatio_Left) * 
				((double)frame.cols/(nWidthBoundRatio_Right- nWidthBoundRatio_Left));

			if(dScale == 0)
			{
				dScale = 1;
				G4DShowLog(5,_T("Vertical Error"));
			}

			if(stEffectData.nPosX == 0 && stEffectData.nPosY == 0)
			{
				stEffectData.nPosX = frame.cols * (ptVerticalCenterPoint.x/frame.cols);//nSrcWidth/2;
				stEffectData.nPosY = frame.rows/2;
			}
			stEffectData.nPosX = frame.cols * (ptVerticalCenterPoint.x/frame.cols);//nSrcWidth/2;
			stEffectData.nPosY = ptVerticalCenterPoint.y;

			int nLeft = (stEffectData.nPosX- frame.cols/dScale/2);
			int nTop = (stEffectData.nPosY - frame.rows/dScale/2);
			int nWidth = (frame.cols/dScale - 1);
			if((nLeft + nWidth) > frame.cols)
				nLeft = nLeft - ((nLeft + nWidth) - frame.cols);
			else if(nLeft < 0)
				nLeft = 0;

			int nHeight = (frame.rows/dScale - 1);
			if((nTop + nHeight) > frame.rows)
				nTop = nTop - ((nTop + nHeight) - frame.rows);
			else if (nTop < 0)
				nTop = 0;
			MatCut =  (frame)(Rect(nLeft, nTop, nWidth, nHeight));
			frame = MatCut.clone();

			pFrameInfo->stEffectData.nPosX = stEffectData.nPosX;
			pFrameInfo->stEffectData.nPosY = stEffectData.nPosY;

			resize(frame,temp,cv::Size(G4DGetTargetWidth(),G4DGetTargetHeight()),0,0,1);
		}
		if(bTest)
		{
			sprintf(strData,"%d - %d",nIdx,i);
			putText(temp,strData,cv::Point(480,540),CV_FONT_ITALIC,5.,Scalar(255,255,255),2,8);
		}

		int nFrameSpeed = pFrameInfo->nFrameSpeed / G4DGetRecordingFrameCnt();
		if(nFrameSpeed < 1)
			nFrameSpeed = 1;

		if(G4DGetStabilization())
		{
			if(pFrameInfo->bColorRevision)
				pParent->DoColorRevision(&temp,pFrameInfo->stAvgColorInfo);

			EnterCriticalSection(&pMgr->m_pParent->m_criInsertData);
			stLoadFrame stLoad;
			stLoad.StabFrame = temp.clone();
			stLoad.bFinish = TRUE;
			stLoad.nFps = nFrameSpeed;
			stLoad.bStable = FALSE;
			if(pFrameInfo->nStabilizerFrame)
			{
				//Roi Image
				stLoad.bStabilInclude = TRUE;

			}
			LeaveCriticalSection(&pMgr->m_pParent->m_criInsertData);

			if(pParent->GetMovieSignal() == MOVIE_STOP && nIdx != 0)
				break;
			pParent->InsertMovieData(nIdx,i,stLoad,TRUE);
			

			/*char strPath[100];
			sprintf(strPath,"M:\\Movie\\ORI_[%d]_%d.png",nIdx,i);
			imwrite(strPath,temp);*/
		}
		else
		{
			/*if(pFrameInfo->bColorRevision)
				pParent->DoColorRevision(&temp,pFrameInfo->stAvgColorInfo);*/
			pFrameInfo->stAvgColorInfo;
			pParent->BGRCC2YUV422Logo(temp,Logo,NoLogo,G4DGetTargetHeight(),G4DGetTargetWidth(),
				pFrameInfo->bColorRevision,pFrameInfo->stAvgColorInfo);
			/*if(pFrameInfo->bColorRevision)
				pParent->BGRCC2YUV422(temp,YUV422,G4DGetTargetHeight(),G4DGetTargetWidth(),pFrameInfo->stAvgColorInfo,bInsertLogo);
			else
				pParent->BGR2YUV422(temp,YUV422,G4DGetTargetHeight(),G4DGetTargetWidth(),bInsertLogo);*/

			EnterCriticalSection(&pMgr->m_pParent->m_criInsertData);
			stLoadFrame stLoad;
			stLoad.Logo = Logo.clone();
			stLoad.NoLogo = NoLogo.clone();
			stLoad.bFinish = TRUE;
			stLoad.nFps = nFrameSpeed;
			if(pFrameInfo->bColorRevision)
			{
				stLoad.bColorRevision = TRUE;
				stLoad.dColorB = pFrameInfo->stAvgColorInfo.dBlue;
				stLoad.dColorG = pFrameInfo->stAvgColorInfo.dGreen;
				stLoad.dColorR = pFrameInfo->stAvgColorInfo.dRed;
			}
			LeaveCriticalSection(&pMgr->m_pParent->m_criInsertData);
			if(pParent->GetMovieSignal() == MOVIE_STOP && nIdx != 0)
				break;
			pParent->InsertMovieData(nIdx,i,stLoad,TRUE);
		}
	}
	CString strLog;
	strLog.Format(_T("[%d] GPU Adjust Finish[%d]"),nIdx,pArrFrameInfo->size());
	G4DShowLog(5,strLog);
	for(int i = 0 ; i < pArrFrameInfo->size(); i++)
	{
		MakeFrameInfo* pFrameInfo = &(pArrFrameInfo->at(i));
		if(pFrameInfo->Image)
		{
			delete pFrameInfo->Image;
			pFrameInfo->Image = NULL;
		}
	}
	if(pArrFrameInfo)
	{
		pArrFrameInfo->clear();
		delete pArrFrameInfo;
		pArrFrameInfo = NULL;
	}
	return TRUE;
}
void C4DReplaySDIPlayerDivMgr::GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle,BOOL bReverse/* = FALSE*/)
{
	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;

	Mat rot_mat(cv::Size(2, 3), CV_32FC1);
	cv::Point rot_center( nCenterX, nCenterY);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust;
	if(bReverse)
		dbAngleAdjust = -1 * (180);   //Normal Movie;
	else
		dbAngleAdjust = -1 * (dAngle + 90);

	rot_mat = getRotationMatrix2D(rot_center, dbAngleAdjust, dScale);

	//cuda::warpAffine(*gMat,d_rotate, rot_mat, Size(2500, 1500), CV_INTER_CUBIC+CV_WARP_FILL_OUTLIERS);
	cuda::GpuMat d_rotate;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- ? CvPoint2D32f srcTri[3], dstTri[3];
	cuda::warpAffine(*gMat, d_rotate, rot_mat, Size(gMat->cols, gMat->rows), CV_INTER_LINEAR + CV_WARP_FILL_OUTLIERS);
	
	d_rotate.copyTo(*gMat);
}
void C4DReplaySDIPlayerDivMgr::GpuRotateImage(cuda::GpuMat* gMat,Mat rot_mat)
{
	cuda::GpuMat d_rotate;
	cuda::warpAffine(*gMat, d_rotate, rot_mat, Size(gMat->cols, gMat->rows), CV_INTER_LINEAR);
	d_rotate.copyTo(*gMat);
}
void C4DReplaySDIPlayerDivMgr::GpuMoveImage(cuda::GpuMat* gMat,int nX,int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
CString C4DReplaySDIPlayerDivMgr::GetDSCIDFromPath(CString strPath)
{
	int nFind = strPath.ReverseFind('\\') + 1;
	CString csFile = strPath.Right(strPath.GetLength()-nFind);
	csFile.Trim();
	//csFile.Trim(_T("\\"));
	nFind = csFile.Find('.');
	CString csCam = csFile.Left(nFind);
	csCam.Trim();
	nFind = csCam.Find('_');

	CString strCamID = csCam.Mid(0, nFind);
	
	return strCamID;
}
double C4DReplaySDIPlayerDivMgr::GetCounter()
{
	//TRACE(_T("GET COUNTER\n"));

	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart-CounterStart)/PCFreq;
}
void C4DReplaySDIPlayerDivMgr::StartCounter()
{
	//TRACE(_T("START COUNTER\n"));
	PCFreq = 0.0;
	CounterStart  = 0;

	LARGE_INTEGER li;
	if(!QueryPerformanceFrequency(&li))
		cout << "QueryPerformanceFrequency failed!\n";

	PCFreq = double(li.QuadPart)/1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}
int C4DReplaySDIPlayerDivMgr::Round(double dData)
{
	int nResult = 0;
	if( dData == 0)
		nResult = 0;
	else if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);

	return nResult;
}
unsigned WINAPI C4DReplaySDIPlayerDivMgr::_FrameGPUThread(LPVOID param)
{
#if 1
	C4DReplaySDIPlayerDivMgr* pDivMgr = (C4DReplaySDIPlayerDivMgr*) param;
	int nCnt = 0;
	while(1)
	{
		Sleep(1);
		if(pDivMgr->m_stArrPlayInfo.size())
		{
			//FrameDecode
			FRAME_GPU_INFO* pGPUInfo = pDivMgr->m_stArrPlayInfo.at(0);

			pDivMgr->DoDecodeGPUFrame(pGPUInfo);
			pDivMgr->m_stArrPlayInfo.erase(pDivMgr->m_stArrPlayInfo.begin());
		}

		nCnt++;
		if(nCnt > 1000 && pDivMgr->m_stArrPlayInfo.size() == 0)
		{
			pDivMgr->m_bGPURun = FALSE;
			break;
		}
	}
	return TRUE;
#else
	FRAME_GPU_INFO* info = (FRAME_GPU_INFO*) param;
	stDivideFrame* pDiv	 = info->pDiv;
	C4DReplayDecodeMgr* pParent = info->pParent;
	AJASendGPUFrameInfo*stAJA = info->stInfo;
	int nIdx	= stAJA->nMakingIndex;
	delete info;

	CString strPath;
	strPath.Format(_T("%s"),stAJA->strPath);
	
	CT2CA pszConvertedAnsiString (strPath);
	string strDst(pszConvertedAnsiString);

	Mat frame;
	VideoCapture vc(strDst);
	if(!vc.isOpened())
	{
		pDiv->nFinish = MAKING_FAIL;
		return FALSE;
	}

	Mat YUV422(1080,1920*2,CV_8UC1);
	Mat re;
	//namedWindow("GPU",CV_WINDOW_FREERATIO);

	int nStartIdx = stAJA->nStartIndex;
	int nEndIdx	  = stAJA->nEndIndex;
	int nCnt	  = 0;

	BOOL bStart = FALSE;
	while(1)
	{
		vc>>frame;
		if(nCnt != nStartIdx && !bStart)
		{
			nCnt++;
			continue;
		}
		if(frame.empty())
			break;

		bStart = TRUE;

		resize(frame,re,cv::Size(G4DGetTargetWidth(),G4DGetTargetHeight()),0,0,CV_INTER_CUBIC);
		pParent->BGR2YUV422(re,YUV422,1080,1920);

		stLoadFrame stLoad;
		stLoad.frame = YUV422.clone();
		stLoad.bFinish = TRUE;
		stLoad.nFps = 1;
		/*imshow("GPU",re);
		waitKey(0);*/
		pParent->InsertMovieData(nIdx,nCnt-nStartIdx,stLoad,TRUE);

		nCnt++;
		
		if(nCnt > nEndIdx)
			break;
	}
	G4DShowLog(5,_T("GPU Finish..!"));
	//pDiv->nFinish = MAKING_FINISH;

	delete stAJA;

	stAJA = NULL;

	return TRUE;
#endif
}
void C4DReplaySDIPlayerDivMgr::StartDivGPUFrame(AJASendGPUFrameInfo* AJAInfo,stDivideFrame* pDiv)
{
	FRAME_GPU_INFO *info = new FRAME_GPU_INFO;
	info->pDiv			 = pDiv;
	info->pParent		 = m_pParent;
	info->stInfo		 = AJAInfo;

	m_stArrPlayInfo.push_back(info);
	if(m_bGPURun == FALSE)
	{
		m_bGPURun = TRUE;
		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE) _beginthreadex(NULL,0,_FrameGPUThread,(void*)this,0,NULL);
		CloseHandle(hSyncTime);
	}
}
void C4DReplaySDIPlayerDivMgr::DoDecodeGPUFrame(FRAME_GPU_INFO* info)
{
	//DoNot Use


	//AJASendGPUFrameInfo* pAJAInfo = info->stInfo;
	//CString strPath;
	//strPath.Format(_T("%s"),pAJAInfo->strPath);
	//CString strDSCID = GetDSCIDFromPath(strPath);

	//stAdjustInfo Adj = GetAdjData(strDSCID);

	//CT2CA pszConvertedAnsiString (strPath);
	//string strDst(pszConvertedAnsiString);

	//Mat frame;
	//VideoCapture vc(strDst);
	//if(!vc.isOpened())
	//{
	//	info->pDiv->nFinish = MAKING_FAIL;
	//	return;
	//}
	//int nWidth  = vc.get(CV_CAP_PROP_FRAME_WIDTH);
	//int nHeight = vc.get(CV_CAP_PROP_FRAME_HEIGHT);

	//Mat YUV422(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);

	//int nStartIdx = pAJAInfo->nStartIndex;
	//int nEndIdx	  = pAJAInfo->nEndIndex;
	//int nIdx	  = pAJAInfo->nMakingIndex;
	//int nCnt	  = 0;

	////Calc Margin
	//int nMarginX  = Adj.stMargin.nMarginX;
	//int nMarginY  = Adj.stMargin.nMarginY;
	//double dbMarginScale = 1 / ( (double)( nWidth - nMarginX * 2) /(double) nWidth);
	//int nLeft = (int)((nWidth  * dbMarginScale - nWidth)/2);
	//int nTop  = (int)((nHeight * dbMarginScale - nHeight)/2);

	//int nH = nHeight*dbMarginScale;
	//int nW = nWidth*dbMarginScale;

	////Calc Zoom value
	//double dRatio = pAJAInfo->nZoomRatio / 100.0;
	//if(dRatio < 1)
	//	dRatio = 1.;
	//int nZoomLeft  = pAJAInfo->nPosX - nWidth/dRatio/2;
	//int nZoomTop   = pAJAInfo->nPosY - nHeight/dRatio/2;

	//int nZoomWidth = nWidth/dRatio - 1;
	//if((nZoomLeft + nZoomWidth) > nWidth)
	//	nZoomLeft = nZoomLeft - ((nZoomLeft + nZoomWidth) - nWidth);
	//else if(nZoomLeft < 0)
	//	nZoomLeft = 0;

	//int nZoomHeight = nHeight/dRatio - 1;
	//if((nZoomTop + nZoomHeight) > nHeight)
	//	nZoomTop = nZoomTop - ((nZoomTop+nHeight) - nHeight);
	//else if(nZoomTop < 0)
	//	nZoomTop = 0;

	//BOOL bStart = FALSE;
	//cuda::GpuMat gpuReFrame,gMatCut;

	//int nFrameSpeed = pAJAInfo->nFrameRate / G4DGetRecordingFrameCnt();
	//if(nFrameSpeed < 1)
	//	nFrameSpeed = 1;

	//int nReverse = pAJAInfo->nReverse;
	//while(1)
	//{
	//	StartCounter();
	//	vc>>frame;
	//	if(nCnt != nStartIdx && !bStart)
	//	{
	//		nCnt++;
	//		continue;
	//	}
	//	if(frame.empty())
	//		break;

	//	bStart = TRUE;

	//	double dbDecoding = GetCounter();
	//	/*GPU Making*/
	//	cuda::GpuMat gpuFrame(frame);
	//	GpuRotateImage(&gpuFrame,Adj.AdjptRotate.x,Adj.AdjptRotate.y,Adj.AdjSize,Adj.AdjAngle);
	//	GpuMoveImage(&gpuFrame,Adj.AdjMove.x,Adj.AdjMove.y);
	//	cuda::resize(gpuFrame,gpuFrame,cv::Size(nW,nH),0,0,CV_INTER_LINEAR);
	//	gpuReFrame = (gpuFrame)(cv::Rect(nLeft,nTop,nWidth,nHeight));

	//	gMatCut = (gpuReFrame)(cv::Rect(nZoomLeft,nZoomTop,nZoomWidth,nZoomHeight));
	//	cuda::resize(gMatCut,gpuReFrame,cv::Size(G4DGetTargetWidth(),G4DGetTargetHeight()),0,0,CV_INTER_LINEAR);
	//	gpuReFrame.download(frame);

	//	if(nReverse)
	//		flip(frame,frame,-1);

	//	m_pParent->BGR2YUV422(frame,YUV422,G4DGetTargetHeight(),G4DGetTargetWidth());

	//	stLoadFrame stLoad;
	//	stLoad.frame = YUV422.clone();
	//	stLoad.bFinish = TRUE;
	//	stLoad.nFps = nFrameSpeed;

	//	double dbAdj = GetCounter() - dbDecoding;
	//	CString strTestLog;
	//	strTestLog.Format(_T("[Decoding]: %.2f [Adjust]: %.2f"),dbDecoding,dbAdj);
	//	G4DShowLog(5,strTestLog);
	//	m_pParent->InsertMovieData(nIdx,nCnt-nStartIdx,stLoad,TRUE);

	//	nCnt++;

	//	if(nCnt > nEndIdx)
	//		break;
	//}
	//CString str;
	//str.Format(_T("[%d] GPU Finish!"),nIdx);
	//G4DShowLog(5,str);
	//if(info->stInfo)
	//{
	//	delete info->stInfo;
	//	info->stInfo = NULL;
	//}
}
void C4DReplaySDIPlayerDivMgr::DoColorRevision(Mat* pFrame,FrameRGBInfo info)
{
#if 0
	int nIdx = 0;
	double dB = 0,dG =0,dR = 0;
	for(int i = 0 ; i < pFrame->total(); i++)
	{
		dB += pFrame->data[nIdx++];
		dG += pFrame->data[nIdx++];
		dR += pFrame->data[nIdx++];
	}

	dB = dB/pFrame->total();
	dG = dG/pFrame->total();
	dR = dR/pFrame->total();

	int nBlue	= (int)(dB - info.dBlue);
	int nGreen = (int)(dG - info.dGreen);
	int nRed = (int)(dR - info.dRed);
#endif
	int nBlue	= (int)(info.dBlue);
	int nGreen = (int)(info.dGreen);
	int nRed = (int)(info.dRed);

	if(nBlue == 0 && nGreen == 0 && nRed == 0)
		return;
	
	omp_set_num_threads(m_nActiveCoreNum);
	int nTotal = pFrame->rows*pFrame->cols * 3;
#pragma omp parallel for
	for(int i = 0 ; i < nTotal; i++)
	{
		int nIdx = i%3;
		if(nIdx == 0)
			pFrame->data[i] = CalcTransValue(pFrame->data[i],nBlue);
		else if(nIdx == 1)
			pFrame->data[i] = CalcTransValue(pFrame->data[i],nGreen);
		else
			pFrame->data[i] = CalcTransValue(pFrame->data[i],nRed);
	}
	/*for(int i = 0 ; i < pFrame->rows; i++)
	{
		for(int j = 0 ; j < pFrame->cols; j++)
		{
			int nIdx = pFrame->cols * (i*3) + (j*3);

			pFrame->data[nIdx] = CalcTransValue(pFrame->data[nIdx],nBlue);
			pFrame->data[nIdx+1] = CalcTransValue(pFrame->data[nIdx+1],nGreen);
			pFrame->data[nIdx+2] = CalcTransValue(pFrame->data[nIdx+2],nRed);
		}
	}*/
}
int C4DReplaySDIPlayerDivMgr::CalcTransValue(int nPixel,int nRefValue)
{
	int nResult = nPixel - nRefValue;

	if(nResult > 255)
		nResult = 255;
	
	if(nResult < 0)
		nResult = 0;

	return nResult;
}
void C4DReplaySDIPlayerDivMgr::GetColorAvg(Mat frame,FrameRGBInfo* pstavg,FrameRGBInfo info,int nPosX,int nPosY,int nZoom)
{
	int nIdx = 0;
	double dB = 0,dG =0,dR = 0;

#if 0
	int nWidth = frame.cols;
	int nHeight = frame.rows;
	double dRatio = nZoom / 100.0;
	if(dRatio < 1)
		dRatio = 1.;
	int nZoomLeft  = nPosX - nWidth/dRatio/2;
	int nZoomTop   = nPosY - nHeight/dRatio/2;

	int nZoomWidth = nWidth/dRatio - 1;
	if((nZoomLeft + nZoomWidth) > nWidth)
		nZoomLeft = nZoomLeft - ((nZoomLeft + nZoomWidth) - nWidth);
	else if(nZoomLeft < 0)
		nZoomLeft = 0;

	int nZoomHeight = nHeight/dRatio - 1;
	if((nZoomTop + nZoomHeight) > nHeight)
		nZoomTop = nZoomTop - ((nZoomTop+nHeight) - nHeight);
	else if(nZoomTop < 0)
		nZoomTop = 0;

	//nZoomLeft,nZoomTop,nZoomWidth,nZoomHeight
	for(int i = nZoomTop ; i < nZoomHeight; i++)
	{
		int ridx = nWidth * (i*3);
		for(int j = nZoomLeft ; j < nZoomWidth; j++)
		{
			int cidx = ridx + j*3;
			dB += frame.data[cidx];
			dG += frame.data[cidx+1];
			dR += frame.data[cidx+2];

			nIdx++;
		}
	}
	dB = dB/nIdx;
	dG = dG/nIdx;
	dR = dR/nIdx;
#else
	for(int i = 0 ; i < frame.total(); i++)
	{
		dB +=frame.data[nIdx++];
		dG +=frame.data[nIdx++];
		dR +=frame.data[nIdx++];
	}

	dB = dB/frame.total();
	dG = dG/frame.total();
	dR = dR/frame.total();
#endif
	pstavg->dBlue = (int)(dB - info.dBlue);
	pstavg->dGreen = (int)(dG - info.dGreen);
	pstavg->dRed = (int)(dR - info.dRed);
	/*int nBlue	= (int)(dB - info.dBlue);
	int nGreen = (int)(dG - info.dGreen);
	int nRed = (int)(dR - info.dRed);*/
}
//void C4DReplaySDIPlayerDivMgr::StartMoviePlay(CString strIP)
//{
//	/*if(GetThreadPlay() == TRUE)
//	{
//	AfxMessageBox(_T("기존 작업이 끝나지 않았다."));
//	return;
//	}
//	SetThreadPlay(TRUE);
//
//	m_strIP = strIP;
//
//	HANDLE hSyncTime = NULL;
//	hSyncTime = (HANDLE) _beginthreadex(NULL,0,_ThreadAutoPlay,this,0,NULL);
//	CloseHandle(hSyncTime);*/
//}
//unsigned WINAPI C4DReplaySDIPlayerDivMgr::_ThreadAutoPlay(LPVOID param)
//{
//	C4DReplaySDIPlayerDivMgr* pDivMgr = (C4DReplaySDIPlayerDivMgr*) param;
//	CESMIni ini;
//	CString strIP = pDivMgr->m_strIP;
//	//Read Ini
//	CString strPath = G4DGetPath(strIP,CONFIG_PATH);
//	strPath.Append(_T("\\SDIDIVframe.ini"));
//
//	G4DShowLog(5,_T("Start Div Thread"));
//
//	int nReceiveCnt = 0;
//	CString strTime;
//	BOOL bPlay = FALSE;
//
//	if(ini.SetIniFilename(strPath,0))
//	{
//		while(1)//Check INI File every 300ms
//		{
//			CString strState = ini.GetString(STR_SECTION_SDIDIVFRAME,STR_SECTION_STATE,_T(""));
//			
//			if(strState == _T("O"))
//			{
//				CString strRevCnt = ini.GetString(STR_SECTION_SDIDIVFRAME,STR_SECTION_SENDCNT,_T("")); 
//				nReceiveCnt = _ttoi(strRevCnt);
//
//				strTime = ini.GetString(STR_SECTION_SDIDIVFRAME,STR_SECTION_TIME,_T(""));
//
//				ini.WriteString(STR_SECTION_SDIDIVFRAME,STR_SECTION_STATE,_T("W"));
//
//				pDivMgr->m_pArrDecodeFrame = new vector<stDivideFrame>(nReceiveCnt);
//
//				int nCnt = 0;
//				
//				while(1)
//				{
//					for(int i = 0; i < nReceiveCnt; i++)
//					{
//						CString strNum;
//						strNum.Format(_T("%d"),i);
//						if(ini.GetString(STR_SECTION_SDIDIVFRAME,strNum,_T("")) != _T("X"))
//						{
//							int nFrameCnt = _ttoi(ini.GetString(STR_SECTION_SDIDIVFRAME,strNum,_T("")));
//							CString strLog;
//							strLog.Format(_T("input - [%d/%d]"),i,nFrameCnt);
//							pDivMgr->DoDecode(strTime,i,nCnt);
//							G4DShowLog(5,strLog);
//
//							ini.WriteString(STR_SECTION_SDIDIVFRAME,strNum,_T("X"));
//							nCnt++;
//
//							if(bPlay == FALSE)
//							{
//								bPlay = TRUE;
//								pDivMgr->DoPlay();
//							}
//						}
//						else
//							continue;
//
//						if(nCnt == nReceiveCnt)
//							break;
//					}
//					
//					if(nCnt == nReceiveCnt)
//						break;
//
//					Sleep(1);
//				}
//				ini.WriteString(STR_SECTION_SDIDIVFRAME,STR_SECTION_STATE,_T("X"));
//				
//				continue;
//			}
//			Sleep(300);
//		}
//		//Set the memory
//	}
//	else
//	{
//		AfxMessageBox(_T("Check your network connection or sharing path"));
//	}
//
//	pDivMgr->SetThreadPlay(FALSE);
//	
//	G4DShowLog(5,_T("Finish Div Thread"));
//
//	return TRUE;
//}
//void C4DReplaySDIPlayerDivMgr::DoDecode(CString strTime,int nIdx,int nFrameCnt)
//{
//	//m_pArrDecodeFrame->at(nIdx).stArrFrame = new vector<stLoadFrame>(nFrameCnt);
//
//	ThreadDecodeInfo* pThreadDecodeInfo = new ThreadDecodeInfo;
//	pThreadDecodeInfo->strTime = strTime;
//	pThreadDecodeInfo->nIdx	   = nIdx;
//	pThreadDecodeInfo->pParent = this;
//	pThreadDecodeInfo->nFrameCnt = nFrameCnt;
//
//	HANDLE hSyncTime = NULL;
//	hSyncTime = (HANDLE) _beginthreadex(NULL,0,_ThreadDecode,pThreadDecodeInfo,0,NULL);
//	CloseHandle(hSyncTime);
//}
//unsigned WINAPI C4DReplaySDIPlayerDivMgr::_ThreadDecode(LPVOID param)
//{
//	ThreadDecodeInfo* pThreadInfo = (ThreadDecodeInfo*) param;
////	C4DReplaySDIPlayerDivMgr* pDiv = pThreadInfo->pParent;
////	CString strTime = pThreadInfo->strTime;
////	int nIdx = pThreadInfo->nIdx;
////	int nFrameCnt = pThreadInfo->nFrameCnt;
////
////	delete pThreadInfo;
////
////	char strPath[256];
////	sprintf(strPath,"M:\\Movie\\%S\\%d.mp4",strTime,nIdx);
////	
////	//stDivideFrame* pDivFrame = new stDivideFrame;
////	
////	VideoCapture vc(strPath);
////
////	if(vc.isOpened() == false)
////	{
////		CString strLog;
////		strLog.Format(_T("%S - Cannot Open"),strPath);
////		//pDivFrame->nFinish = -100;
////
////		G4DShowLog(0,strLog);
////		return FALSE;
////	}
////
////	int nCnt = 0;
////	while(1)
////	{
////		Mat frame;
////		vc >> frame;
////		if(frame.empty())
////			break;
////		stLoadFrame Load;
////		Load.bFinish = TRUE;
////		Load.frame = frame.clone();
////		
////		//pDivFrame->stArrFrame.push_back(Load);// = frame.clone();
////	}
//////	pDivFrame->nFinish = 1;
////	//pDiv->m_pArrDecodeFrame->at(nIdx) = *pDivFrame;
//	
//	
//	return TRUE;
//}
//void C4DReplaySDIPlayerDivMgr::DoPlay()
//{
//	HANDLE hSyncTime = NULL;
//	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_ThreadPlay,this,0,NULL);
//}
//unsigned WINAPI C4DReplaySDIPlayerDivMgr::_ThreadPlay(LPVOID param)
//{
//	/*C4DReplaySDIPlayerDivMgr* pDivMgr = (C4DReplaySDIPlayerDivMgr*)param;
//
//	int nSize = pDivMgr->m_pArrDecodeFrame->size();
//	int nPlayCnt = 0;
//	
//	namedWindow("FRAME",CV_WINDOW_FREERATIO);
//	
//	for(int i = 0 ; i < nSize; i++)
//	{
//		stDivideFrame* pDivFrame = &pDivMgr->m_pArrDecodeFrame->at(i);
//
//		while(1)
//		{
//			if(pDivFrame->nFinish == 1)
//				break;
//
//			if(pDivFrame->nFinish == -100)
//				break;
//		}
//		if(pDivFrame->nFinish == -100)
//		{
//			G4DShowLog(5,_T("Decoding Error"));
//			continue;
//		}
//		char str[2];
//		sprintf(str,"%d",i);
//		for(int j = 0 ; j < pDivFrame->stArrFrame.size(); j++)
//		{
//			putText(pDivFrame->stArrFrame.at(j).frame,str,cv::Point(960,540),CV_FONT_HERSHEY_PLAIN,2,
//				Scalar(0,0,0),1,8);
//			imshow("FRAME",pDivFrame->stArrFrame.at(j).frame);
//			waitKey(0);
//		}
//		nPlayCnt++;
//	}
//
//	destroyAllWindows();
//
//	G4DShowLog(5,_T("Play Finish"));*/
//	return TRUE;
//}