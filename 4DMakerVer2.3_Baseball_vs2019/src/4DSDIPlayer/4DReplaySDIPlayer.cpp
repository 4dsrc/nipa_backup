
// 4DReplaySDIPlayer.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "4DReplaySDIPlayer.h"
#include "4DReplaySDIPlayerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// C4DReplaySDIPlayerApp

BEGIN_MESSAGE_MAP(C4DReplaySDIPlayerApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// C4DReplaySDIPlayerApp 생성

C4DReplaySDIPlayerApp::C4DReplaySDIPlayerApp()
{
	// 다시 시작 관리자 지원
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	m_pDlg = NULL;
}
C4DReplaySDIPlayerApp::~C4DReplaySDIPlayerApp()
{
	if(m_cLog)
	{
		delete m_cLog;
		m_cLog = NULL;
	}
	if(m_pDlg)
	{
		delete m_pDlg;
		m_pDlg = NULL;
	}
}

C4DReplaySDIPlayerApp theApp;

BOOL C4DReplaySDIPlayerApp::InitInstance()
{

	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);

	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	CShellManager *pShellManager = new CShellManager;

	if(!TerminatePreviousInstance())
		return FALSE;

	SetRegistryKey(_T("로컬 응용 프로그램 마법사에서 생성된 응용 프로그램"));

	G4DAppReg(this);
	
	m_cLog = new C4DReplaySDIPlayerLog;

	m_pDlg = new C4DReplaySDIPlayerDlg(NULL,m_cLog);
	G4DPlayerReg(m_pDlg);	
	
	//Launch dump
	TCHAR strPath[MAX_PATH];
	GetModuleFileName( NULL, strPath, _MAX_PATH);
	PathRemoveFileSpec(strPath);
	ESMUtil::ExecuteProcess(strPath,_T("procdump64.exe"), _T("4DReplaySDIPlayer.exe -t -e -h"), FALSE);

	m_pDlg->DoModal();
	/*C4DReplaySDIPlayerDlg dlg;
	m_pMainWnd = &dlg;*/
	/*INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{

	}
	else if (nResponse == IDCANCEL)
	{

	}*/
	ESMUtil::KillProcess(_T("procdump64.exe"));
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}
	

	return FALSE;
}
BOOL C4DReplaySDIPlayerApp::TerminatePreviousInstance()
{
	//Take a snapshot of all processes in the system
	HANDLE hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if(INVALID_HANDLE_VALUE == hProcessSnap)
	{
		//ERROR
		return FALSE;
	}

	PROCESSENTRY32 pe32;
	pe32.dwSize =  sizeof(PROCESSENTRY32);

	//Retrieve information about the first process, and exit if unsuccessful
	if(!Process32First(hProcessSnap, &pe32))
	{
		//ERROR
		CloseHandle(hProcessSnap); //clean the snapshot object
		return FALSE;
	}

	CString strExeFileName1 = _T("4DReplaySDIPlayer.exe");
	CString strExeFileName2 = _T("4DReplaySDIPlayerD.exe");

	//-- 2013-09-26 hongsu@esmlab.com
	//-- kill Process ffmepg 
	CString strExeFFMpeg	= _T("ffmpeg.exe");

	BOOL bExe1 = FALSE;
	BOOL bExe2 = FALSE;
	BOOL bExe3 = FALSE;
	do
	{
		bExe1 = strExeFileName1.CompareNoCase(pe32.szExeFile);
		bExe2 = strExeFileName2.CompareNoCase(pe32.szExeFile);
		bExe3 = strExeFFMpeg.CompareNoCase(pe32.szExeFile);

		//-- 2013-09-26 hongsu@esmlab.com
		//-- Kill Process ffmpeg 
		if(!bExe3)
		{
			if(GetCurrentProcessId() != pe32.th32ProcessID)
			{
				DWORD dwExitCode = 0;
				HANDLE hHandle = OpenProcess(PROCESS_ALL_ACCESS, 0,pe32.th32ProcessID);
				GetExitCodeProcess(hHandle, &dwExitCode);
				TerminateProcess(hHandle, dwExitCode);
			}
		}

		if(!bExe1 || !bExe2 )
		{

			if(GetCurrentProcessId() != pe32.th32ProcessID)
			{
				//-- Terminate the previous instance.
				//-- Message Box
				CString strMsg;
				if(!bExe1)	strMsg.Format(_T("Already %s is running\nDo you want to kill previous process?"),strExeFileName1);
				if(!bExe2)	strMsg.Format(_T("Already %s is running\nDo you want to kill previous process?"),strExeFileName2);

				int nReturn = AfxMessageBox(strMsg,MB_YESNO);
				switch(nReturn)
				{
				case IDYES:		
					if(GetCurrentProcessId() != pe32.th32ProcessID)
					{
						DWORD dwExitCode = 0;
						HANDLE hHandle = OpenProcess(PROCESS_ALL_ACCESS, 0,pe32.th32ProcessID);
						GetExitCodeProcess(hHandle, &dwExitCode);
						TerminateProcess(hHandle, dwExitCode);
					}
					break;
				default:
					//-- permit multi process
					return FALSE;
					break;
				}
			}			
		}
	} while(Process32Next(hProcessSnap,&pe32));

	CloseHandle(hProcessSnap);
	return TRUE;
}
