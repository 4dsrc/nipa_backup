#include "stdafx.h"
#include "4DReplaySDIPlayerList.h"
#include "ESMIni.h"
C4DReplaySDIPlayerList::C4DReplaySDIPlayerList(void)
{
	m_pParent = NULL;
	m_nSelectedItem = -1;
	m_bPlayState = FALSE;
}
C4DReplaySDIPlayerList::~C4DReplaySDIPlayerList()
{
	
}
void C4DReplaySDIPlayerList::Init(C4DReplaySDIPlayerDlg* pParent,C4DReplayDecodeMgr* pDecoder)
{
	m_pParent = pParent;
	m_pDecoder = pDecoder;

	SetExtendedStyle(LVS_EX_DOUBLEBUFFER | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );

	InsertColumn(LIST_NUM     ,_T("#")	     ,LVCFMT_LEFT,30);	
	InsertColumn(LIST_FILENAME,_T("FileName"),LVCFMT_LEFT,310);
	InsertColumn(LIST_FILETIME,_T("Time")    ,LVCFMT_LEFT,60);
	InsertColumn(LIST_FILEPATH,_T("FilePath"),LVCFMT_LEFT,1);
	InsertColumn(LIST_DIVPLAY ,_T("DivPlay") ,LVCFMT_LEFT,1);
	DragAcceptFiles();

	AddFile(_T("TEST"),_T("C:\\Users\\4DReplay\\Desktop\\11111.mp4"),FALSE);
#ifdef TEST
	CString str,strNum,strPath;
	//m_pParent->OnBnClickedButtonAddmovie();
	//for(int i = 0; i < 20; i++){ 
	//	//str.Format("Item %2d", i + 1); 
	//	strNum.Format(_T("%d"),i);
	//	str.Format(_T("Item %2d"), i + 1);
	//	strPath.Format(_T("M:\\Movie\\%d.mp4"),i + 1);
	//	InsertItem(i,strNum);
	//	SetItem(i,LIST_FILEPATH,LVIF_TEXT,strPath,0,0,0,NULL);
	//	SetItem(i,LIST_FILENAME,LVIF_TEXT,str,0,0,0,NULL);
	//}
#endif
}
BEGIN_MESSAGE_MAP(C4DReplaySDIPlayerList, CListCtrl)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &C4DReplaySDIPlayerList::OnNMDblclk)
	ON_NOTIFY_REFLECT(NM_CLICK, &C4DReplaySDIPlayerList::OnNMClick)
	ON_WM_DROPFILES()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(ID_LIST_ADDAUTOPLAY, &C4DReplaySDIPlayerList::OnListAddautoplay)
END_MESSAGE_MAP()
void C4DReplaySDIPlayerList::DoDataExchange(CDataExchange* pDX)
{

}
void C4DReplaySDIPlayerList::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	CListCtrl::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}
void C4DReplaySDIPlayerList::OnDestroy()
{
	CListCtrl::OnDestroy();
}
void C4DReplaySDIPlayerList::OnDropFiles(HDROP hDropInfo)
{
	int NumDropFiles = 0;
	char FilePath[MAX_PATH]; 

	NumDropFiles = DragQueryFileA(hDropInfo,0xFFFFFFFF,FilePath,MAX_PATH);

	CString strFileName,strFilePath,strEx;

	for(int i = 0 ; i < NumDropFiles; i++)
	{
		DragQueryFileA(hDropInfo,i,FilePath,MAX_PATH);

		strFilePath = (LPSTR)FilePath;

		strFileName = strFilePath.Right(strFilePath.GetLength() - strFilePath.ReverseFind(_T('\\')) -1);
		strEx = PathFindExtension(strFilePath);//strFilePath.Right(5);

		if(strEx == _T(".mp4") || strEx == _T(".avi") || strEx == _T(".wmv"))
			AddFile(strFileName,strFilePath/*,TRUE*/);
		//strFileName.Format(_T("%S"), (LPSTR)FilePath);
	}

	CListCtrl::OnDropFiles(hDropInfo);
}
void C4DReplaySDIPlayerList::AddFile(CString strFileName,CString strFilePath,BOOL bDiv /*= FALSE*/)
{
	int nTotal = GetItemCount();
	CString strNum,strDiv;
	strDiv = bDiv ? _T("O") : _T("X");
	strNum.Format(_T("%d"),nTotal);
	InsertItem(nTotal, strNum);
	SetItem(nTotal,LIST_NUM		,LVIF_TEXT,strNum,0,0,0,NULL);
	SetItem(nTotal,LIST_FILENAME,LVIF_TEXT,strFileName,0,0,0,NULL);
	SetItem(nTotal,LIST_FILEPATH,LVIF_TEXT,strFilePath,0,0,0,NULL);
	SetItem(nTotal,LIST_DIVPLAY,LVIF_TEXT,strDiv,0,0,0,NULL);

	if(bDiv == FALSE)
	{
		CString strPath;
		strPath = GetItemText(nTotal,LIST_FILEPATH);
		//m_pParent->InsertMovieData(strPath,NULL,nTotal,TRUE,FALSE);
	}
	//CT2CA pszConvertedAnsiString(strFilePath);
	//std::string strLoadPath(pszConvertedAnsiString);

	//VideoCapture vc(strLoadPath);
	//CString strTimeInfo;
	//double dbTime = 0.0;
	//if(vc.isOpened())
	//{
	//	double dFps = vc.get(CV_CAP_PROP_FPS);
	//	double dTotalFrame = vc.get(CV_CAP_PROP_FRAME_COUNT);

	//	dbTime = (1/dFps) * dTotalFrame;
	//	strTimeInfo.Format(_T("%02d:%02d:%02d"),0,(int)dbTime/60,(int)dbTime%60);
	//}
	//else
	//	strTimeInfo.Format(_T("-"));

	//SetItem(nTotal,LIST_FILETIME,LVIF_TEXT,strTimeInfo,0,0,0,NULL);
}
void C4DReplaySDIPlayerList::DeleteSelList()
{
	if(G4DGetEncodingState() == TRUE)
	{
		G4DShowLog(0,_T("Cannot delete while making"));

		return;
	}

	int nSelectedCount = GetSelectedCount();

	if(nSelectedCount == 1)
	{
		DeleteItem(m_nSelectedItem);
	}
	else
	{
		CString *pStrSel = new CString[nSelectedCount];
		int nCnt = 0;


		for(int i = nSelectedCount; i >= 0; i--)//while(pos != NULL)
		{
			POSITION pos = GetFirstSelectedItemPosition();

			int nDelId = GetNextSelectedItem(pos);
			CString str = GetItemText(nDelId,0);
			str.Append(_T("\n"));
			TRACE(str);

			DeleteItem(nDelId);
		}
		//순번재정렬
		for(int i = 0 ; i < GetItemCount(); i++)
		{
			if(i == _ttoi(GetPlayNum(i)))
				continue;

			CString strNum;
			strNum.Format(_T("%d"),i);
			SetItemText(i,LIST_NUM,strNum);
		}

	}

}
void C4DReplaySDIPlayerList::GetFile(int nIndex)
{
	int nTotalCnt = GetItemCount();

	if(nIndex > nTotalCnt-1)
		nIndex = 0;	

	if(nIndex < 0 || nTotalCnt == 0)
		nIndex = nTotalCnt-1;	

	m_nSelectedIndex = nIndex;

	CString strPath;
	strPath = GetItemText(nIndex,LIST_FILEPATH);

	m_pParent->InsertMovieData(strPath,NULL,nIndex,TRUE,FALSE);
}
void C4DReplaySDIPlayerList::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	if(G4DGetSelectFile() == TRUE)
	{
		G4DShowLog(0,_T("After play, select the file"));
		return;
	}

	int nSelectedItem= GetNextItem( -1, LVNI_SELECTED );
	CString strPath = GetItemText(nSelectedItem,LIST_FILEPATH);//FilePath

	m_nSelectedIndex = nSelectedItem;

	*pResult = 0;

	m_pParent->InsertMovieData(strPath,NULL,nSelectedItem,TRUE,FALSE);
}
void C4DReplaySDIPlayerList::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	m_nSelectedItem= GetNextItem( -1, LVNI_SELECTED );
	//EnsureVisible(m_nSelectedItem,false);
	*pResult = 0;
}
void C4DReplaySDIPlayerList::RunAllPlay()
{
	if(GetAllPlayState() == TRUE)
		return;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,AllPlayThread,this,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI C4DReplaySDIPlayerList::AllPlayThread(LPVOID param)
{
	C4DReplaySDIPlayerList* pMgr = (C4DReplaySDIPlayerList*)param;
	
	C4DReplaySDIPlayerDlg* pParent = pMgr->m_pParent;
	C4DReplayDecodeMgr* pDecoder = pMgr->m_pDecoder;

	while(1)
	{
		Sleep(500);

		if(pMgr->GetAllPlayState() == FALSE)
			break;

		if(pDecoder->GetPlayState() == TRUE)
			continue;

		if(pDecoder->GetPlayState() == FALSE)
		{
			int nCount = pMgr->GetListTotal();
			int nNextPlay = pMgr->GetCurPlayIdx() + 1;

			if(nNextPlay > nCount - 1)
				nNextPlay = 0;

			pMgr->SetCurPlayIdx(nNextPlay);

			Sleep(30);

			pParent->InsertMovieData(pMgr->GetPlayPath(nNextPlay),NULL,nNextPlay,TRUE,FALSE);
				//DoFileDecode(pMgr->GetPlayPath(nNextPlay),nNextPlay);
		}
	}

	return TRUE;
}

BOOL C4DReplaySDIPlayerList::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
		if(pMsg->wParam == !VK_CONTROL)
			return TRUE;

		switch(pMsg->wParam)
		{
		case VK_DELETE:
			{
				if(G4DGetEncodingState() == TRUE)
				{
					G4DShowLog(0,_T("Cannot delete while making"));

					return CListCtrl::PreTranslateMessage(pMsg);
				}
				DeleteSelList();
			}
			break;
		//case VK_PRIOR:
		//	{
		//		int nCurPlay = m_nSelectedIndex - 1;
		//		GetFile(nCurPlay);
		//	}
		//	break;
		//case VK_NEXT:
		//	{
		//		int nCurPlay = m_nSelectedIndex + 1;
		//		GetFile(nCurPlay);
		//	}
		//	break;
		//case VK_RETURN:
		//	{
		//		POSITION pos = GetFirstSelectedItemPosition();
		//		int nItem = GetNextSelectedItem(pos);
		//		GetFile(nItem);
		//	}
		//	break;
		}
	}
	return CListCtrl::PreTranslateMessage(pMsg);
}


void C4DReplaySDIPlayerList::OnRButtonDown(UINT nFlags, CPoint point)
{
	//m_nSelectedItem= GetNextItem( -1, LVNI_SELECTED );
	//int nSelectedCount = GetSelectedCount();
	if(m_nSelectedItem < 0)
		return;

	CMenu popup;
	CMenu *MyMenu;
	CPoint pt;
	GetCursorPos(&pt);
	popup.LoadMenu(IDR_MENU_MOVIELIST);
	MyMenu = popup.GetSubMenu(0);

	CString strState = GetItemText(m_nSelectedItem,LIST_DIVPLAY);
	if(strState != _T("X") && strState != _T("O"))
	{
		MyMenu->EnableMenuItem(ID_LIST_ADDAUTOPLAY,TRUE);//비활성화
	}
	
	MyMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, this);

	CListCtrl::OnRButtonDown(nFlags, point);
}


void C4DReplaySDIPlayerList::OnListAddautoplay()
{
	int nSelectedItem= GetNextItem( -1, LVNI_SELECTED );

	CString strPath = GetItemText(nSelectedItem,LIST_FILEPATH);
	//Send Info
	CString str4DCPath;
	str4DCPath.Format(_T("\\\\%s\\4DMaker\\Recorder.ini"),G4DGetDistributeIP());
	CESMIni ini;
	CString strTempIniPath;
	if(ini.SetIniFilename(str4DCPath))
	{
		strTempIniPath = strPath.Mid(2,strPath.GetLength());
		ini.WriteString(_T("Recording"),_T("AutoMakingPath"),strTempIniPath);
		ini.WriteString(_T("Recording"),_T("AutoMakingFlag"),_T("1"));
	}
}
void C4DReplaySDIPlayerList::ReplaceListData(int nIdx,CString strFileName,CString strFilePath,BOOL bDiv /* = FALSE */)
{
	CString strDiv;
	strDiv = bDiv ? _T("O") : _T("X");
	SetItemText(nIdx,LIST_FILEPATH,strFilePath);
	SetItemText(nIdx,LIST_FILENAME,strFileName);
	SetItemText(nIdx,LIST_DIVPLAY,strDiv);
}