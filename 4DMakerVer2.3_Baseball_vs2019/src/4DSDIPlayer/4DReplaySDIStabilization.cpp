#include "stdafx.h"
#include "4DReplaySDIStabilization.h"


C4DReplaySDIStabilizer::C4DReplaySDIStabilizer(bool isUHDtoFHD)
{
	_imageSetForStabilization = makePtr<videostab::ImageSetForStabilization>();
	_motionEstimation = makePtr<videostab::MotionEstimatorRansacL2>(videostab::MM_TRANSLATION); 		
	_ransac = _motionEstimation->ransacParams(); 
	_twoPassStabilizer = new videostab::TwoPassStabilizer(); 

	_motionEstBuilder = makePtr<videostab::KeypointBasedMotionEstimatorGpu>(_motionEstimation); 
	_outlierRejector = makePtr<videostab::NullOutlierRejector>(); 

	_minInlierRatio = 0.1; 

	_stabilizerStartFrameNumber = 0;
	_stabilizerEndFrameNumber	= 0;

	_width  = G4DGetTargetWidth();
	_height = G4DGetTargetHeight();
	/*if(isUHDtoFHD)
	{
		_width = 1920;
		_height = 1080;
	}
	else
	{
		_width = 3840;
		_height = 2160;
	}*/

	_dFrameRate = 30.f;
}


C4DReplaySDIStabilizer::~C4DReplaySDIStabilizer(void)
{
}

bool C4DReplaySDIStabilizer::VideoDecoding(string strMovieFile, int nStabilizationStartFrameNumber, int nStabilizationEndFrameNumber)
{	
	VideoCapture videoCapture(strMovieFile);
	if (!videoCapture.isOpened())
	{
		G4DShowLog(5, _T("Stabilization Error"));
		return false;
	}
	_stabilizerEndFrameNumber = videoCapture.get(CAP_PROP_FRAME_COUNT)-1 - nStabilizationEndFrameNumber;
	_stabilizerStartFrameNumber = nStabilizationStartFrameNumber;
	_dFrameRate = videoCapture.get(CAP_PROP_FPS);

		
	//CalcFrontAndRearFrameCount(templateStruct);
	CString strLog;
	strLog.Format(_T("Start Frame : %d, End Frame : %d"), _stabilizerStartFrameNumber, _stabilizerEndFrameNumber);
	G4DShowLog(5, strLog);
	if(_stabilizerStartFrameNumber >= _stabilizerEndFrameNumber)
	{
		G4DShowLog(5, _T("Do not need to Stabilization"));
		return false;
	}

	Mat image;	

	int nStartPushCount = 0;
	int nMiddlePushCount = 0;
	int nEndPushCount = 0;
	
	int cnt = 0;
	for(;;)
	{			
		videoCapture >> image;
		if(image.empty()) break;

		_width = image.cols;
		_height = image.rows;

		Mat roiImage;
		Rect2d rect = Rect2d(
			/*image.cols/5*2, 
			image.rows/3*1, 
			image.cols/5*1, 
			image.rows/3);*/
			image.cols/4*1, 
			image.rows/3*1, 
			image.cols/4*2, 
			image.rows/3*1);//baseball

		image(rect).copyTo(roiImage);
				
		if(cnt > _stabilizerEndFrameNumber)
		{
			Mat tempImage = image.clone();
			rearFrame_.push_back(tempImage);

			nStartPushCount++;
		}
		else if(cnt < _stabilizerStartFrameNumber)
		{
			Mat tempImage = image.clone();
			frontFrame_.push_back(tempImage);

			nMiddlePushCount++;
		}
		else
		{			
			blur(roiImage, roiImage, cv::Size(41, 41));

			_imageSetForStabilization->inputImageForGeneratingTransformationMatrix(roiImage);
			_imageSetForStabilization->inputImageForApplyingTransformationMatrix(image);

			nEndPushCount++;
		}
		cnt++;
	}	

	CString strPushCount;	
	strPushCount.Format(_T("Start Push:%d, Middle Push:%d, End Push:%d"), nStartPushCount, nMiddlePushCount, nEndPushCount);
	G4DShowLog(5, strPushCount);

	return true;
	
}

void C4DReplaySDIStabilizer::CreateStabilizatedMovie(string outputPath, double outputFps)
{
	_imageSetForStabilization->inputImageForGeneratingTransformationMatrix(Mat());
	_imageSetForStabilization->inputImageForApplyingTransformationMatrix(Mat());

	try {
		_ransac.size = 3; 
		_ransac.thresh = 5; 
		_ransac.eps = 0.5; 

		_motionEstimation->setRansacParams(_ransac); 
		_motionEstimation->setMinInlierRatio(_minInlierRatio); // second, create a feature detector 		

		_motionEstBuilder->setOutlierRejector(_outlierRejector); // 3-Prepare the stabilizer 

		int radius_pass = 15;		
		bool est_trim = true; 

		_twoPassStabilizer->setEstimateTrimRatio(est_trim); 
		_twoPassStabilizer->setMotionStabilizer(makePtr<videostab::GaussianMotionFilter>(radius_pass)); 		

		int radius = 15; 
		double trim_ratio = 0.1; 
		bool incl_constr = false; 						

		_twoPassStabilizer->setFrameSource(_imageSetForStabilization); 		
		_twoPassStabilizer->setMotionEstimator(_motionEstBuilder); 
		_twoPassStabilizer->setRadius(radius); 
		_twoPassStabilizer->setTrimRatio(trim_ratio); 
		_twoPassStabilizer->setCorrectionForInclusion(incl_constr); 
		_twoPassStabilizer->setBorderMode(BORDER_REPLICATE);		

		_stabilizedFrames.reset(dynamic_cast<videostab::IFrameSource*>(_twoPassStabilizer)); // 4-Processing the stabilized frames. The results are showed and saved. 

		IncodingWithStabilizedImage(_stabilizedFrames, outputPath, _dFrameRate); 
	} 
	catch (const exception &e) 
	{ 
		CString strError;
		strError.Format(_T("Stabilization Error : %s"), e.what());

		G4DShowLog(5, strError);
		//cout << "error: " << e.what() << endl; 
		_stabilizedFrames.release(); 		
	}
}

void C4DReplaySDIStabilizer::TranslationAndWarpPerspective(Mat& srcFrame, Mat& dstFrame, Mat& motion, cv::Size size)
{
	int deltaX = (_width - size.width)/2.;
	int deltaY = (_height - size.height)/2.;

	Rect2d rect = Rect2d(
		deltaX,
		deltaY,
		_width - deltaX*2., 
		_height - deltaY*2.);
#if 1
	Mat temp;
	warpAffine(srcFrame, temp, motion(cv::Rect(0, 0, 3, 2)), cv::Size(_width,_height), cv::INTER_CUBIC, BORDER_REPLICATE);
	temp(rect).copyTo(dstFrame);
#else
	if(G4DGetGPUProcess() == TRUE)//GPU Run
	{
		CString strLog;
		strLog.Format(_T("[CPU] Affine"));
		G4DShowLog(5,strLog);
		Mat temp;
		warpAffine(srcFrame, temp, motion(cv::Rect(0, 0, 3, 2)), cv::Size(_width,_height), cv::INTER_CUBIC, BORDER_REPLICATE);
		temp(rect).copyTo(dstFrame);
	}
	else
	{
		CString strLog;
		strLog.Format(_T("[GPU] Affine"));
		G4DShowLog(5,strLog);
		cuda::GpuMat gpuSrcImage(srcFrame);
		cuda::GpuMat gpuDstImage;

		cuda::warpAffine(gpuSrcImage, gpuDstImage, motion(cv::Rect(0, 0, 3, 2)), cv::Size(_width,_height), cv::INTER_CUBIC, BORDER_REPLICATE);
		gpuDstImage(rect).copyTo(gpuDstImage);

		gpuDstImage.download(dstFrame);	
	}
#endif
}

void C4DReplaySDIStabilizer::ResizeFrameToFHD(Mat& srcFrame, Mat& dstFrame)
{
	cuda::GpuMat gpuSrcImage(srcFrame);
	cuda::GpuMat gpuDstImage;

	cuda::resize(gpuSrcImage, gpuDstImage, cv::Size(_width, _height), cv::INTER_CUBIC);	

	gpuDstImage.download(dstFrame);		
}

void C4DReplaySDIStabilizer::IncodingWithStabilizedImage(Ptr<videostab::IFrameSource> stabilizedFrames, string outputPath, double outputFps)
{		
	int nStartWarpFrame = 0;
	int nMiddleWarpFrame = 0;
	int nEndWarpFrame = 0;

	int nStartEncodingFrame = 0;
	int nMiddleEncodingFrame = 0;
	int nEndEncodingFrame = 0;

	VideoWriter writer; 
	Mat stabilizedFrame; 				
	cv::Size stabilizedSize;

	vector<Mat> stabilizedFrame_; 	

	while (!(stabilizedFrame = stabilizedFrames->nextFrameForGeneratingTransformationMatrix()).empty()) 	
	{
		stabilizedSize = stabilizedFrame.size();

		ResizeFrameToFHD(stabilizedFrame,stabilizedFrame);
		stabilizedFrame_.push_back(stabilizedFrame);

		nStartWarpFrame++;
	}		

	vector<Mat> testMotion = _twoPassStabilizer->getMotions();	

	for(int i = 0; i < frontFrame_.size(); i++)
	{			
		TranslationAndWarpPerspective(frontFrame_[i], frontFrame_[i], testMotion[0], stabilizedSize);
		ResizeFrameToFHD(frontFrame_[i],frontFrame_[i]);		

		nMiddleWarpFrame++;
	}

	for(int i = 0; i < rearFrame_.size(); i++)
	{				
		TranslationAndWarpPerspective(rearFrame_[i], rearFrame_[i], testMotion[_stabilizerEndFrameNumber - _stabilizerStartFrameNumber - 1], stabilizedSize);
		ResizeFrameToFHD(rearFrame_[i],rearFrame_[i]);

		nEndWarpFrame++;
	}
	G4DShowLog(5, _T("Stabilization Time"));
	for(int i = 0; i < frontFrame_.size(); i++)
	{
		if (!writer.isOpened())
		{
			bool bCheckFileOpen = writer.open(outputPath, VideoWriter::fourcc('m','p','e','g'), outputFps, cv::Size(_width, _height));
			if(bCheckFileOpen)
				G4DShowLog(5, _T("Encoding Stabilization File Open Success(Start)"));
			else
				G4DShowLog(5, _T("Encoding Stabilization File Open Fail(Start)"));
		}
		
		writer << frontFrame_[i]; 
		nStartEncodingFrame++;
	}

	for(int i = 0; i < stabilizedFrame_.size(); i++)	
	{
		if (!writer.isOpened()) 
		{
			bool bCheckFileOpen = writer.open(outputPath, VideoWriter::fourcc('m','p','e','g'), outputFps, cv::Size(_width, _height));
			if(bCheckFileOpen)
				G4DShowLog(5, _T("Encoding Stabilization File Open Success(Middle)"));
			else
				G4DShowLog(5, _T("Encoding Stabilization File Open Fail(Middle)"));
		}
		
		//if(ESMGetStabilizationRemoveFrameParse(i))
		//{
		//	continue;
		//}
		/*if( i == 9 || i == 10 || i == 11 || i == 12 ||
			i == 50 || i == 51 || i == 52)
			continue;*/

		writer << stabilizedFrame_[i];
		nMiddleEncodingFrame++;
	}

	for(int i = 0; i < rearFrame_.size(); i++)	
	{
		if (!writer.isOpened()) 	
		{
			bool bCheckFileOpen = writer.open(outputPath, VideoWriter::fourcc('m','p','e','g'), outputFps, cv::Size(_width, _height));		
			if(bCheckFileOpen)
				G4DShowLog(5, _T("Encoding Stabilization File Open Success(End)"));
			else
				G4DShowLog(5, _T("Encoding Stabilization File Open Fail(End)"));
		}

		writer << rearFrame_[i]; 	
		nEndEncodingFrame++;
	}
	CString strWarpCount;	
	strWarpCount.Format(_T("Start Push:%d, Middle Push:%d, End Push:%d"), nStartWarpFrame, nMiddleWarpFrame, nEndWarpFrame);
	G4DShowLog(5, strWarpCount);

	CString strEncodingCount;	
	strEncodingCount.Format(_T("Start Push:%d, Middle Push:%d, End Push:%d"), nStartEncodingFrame, nMiddleEncodingFrame, nEndEncodingFrame);
	G4DShowLog(5, strEncodingCount);
}
void C4DReplaySDIStabilizer::CreateStabilizerMovie(vector<stDivideFrame*>*pArrDivInfo,int nStabilCnt)
{
	_imageSetForStabilization->inputImageForGeneratingTransformationMatrix(Mat());
	_imageSetForStabilization->inputImageForApplyingTransformationMatrix(Mat());

	try {
		_ransac.size = 3; 
		_ransac.thresh = 5; 
		_ransac.eps = 0.5; 

		_motionEstimation->setRansacParams(_ransac); 
		_motionEstimation->setMinInlierRatio(_minInlierRatio); // second, create a feature detector 		

		_motionEstBuilder->setOutlierRejector(_outlierRejector); // 3-Prepare the stabilizer 

		int radius_pass = 15;		
		bool est_trim = true; 

		_twoPassStabilizer->setEstimateTrimRatio(est_trim); 
		_twoPassStabilizer->setMotionStabilizer(makePtr<videostab::GaussianMotionFilter>(radius_pass)); 		

		int radius = 15; 
		double trim_ratio = 0.1; 
		bool incl_constr = false; 						

		_twoPassStabilizer->setFrameSource(_imageSetForStabilization); 		
		_twoPassStabilizer->setMotionEstimator(_motionEstBuilder); 
		_twoPassStabilizer->setRadius(radius); 
		_twoPassStabilizer->setTrimRatio(trim_ratio); 
		_twoPassStabilizer->setCorrectionForInclusion(incl_constr); 
		_twoPassStabilizer->setBorderMode(BORDER_REPLICATE);		

		_stabilizedFrames.reset(dynamic_cast<videostab::IFrameSource*>(_twoPassStabilizer)); // 4-Processing the stabilized frames. The results are showed and saved. 

		AdaptStabilizer(_stabilizedFrames,pArrDivInfo,nStabilCnt);
	} 
	catch (const exception &e) 
	{ 
		CString strError;
		strError.Format(_T("Stabilization Error : %S"), e.what());

		G4DShowLog(5, strError);
		SetRectSize(cv::Size(G4DGetTargetWidth(),G4DGetTargetHeight()));
		for(int i = 0 ; i < pArrDivInfo->size() ; i++)
		{
			if(pArrDivInfo->at(i)->nFinish == MAKING_GPU)
				continue;

			pArrDivInfo->at(i)->nFinish = MAKING_WAIT;
		}
		//cout << "error: " << e.what() << endl; 
		_stabilizedFrames.release(); 		
	}
}
void C4DReplaySDIStabilizer::AdaptStabilizer(Ptr<videostab::IFrameSource> stabilizedFrames,vector<stDivideFrame*>*pArrDivInfo,int nStabilCnt)
{
	int nStartWarpFrame = 0;
	int nMiddleWarpFrame = 0;
	int nEndWarpFrame = 0;

	int nStartEncodingFrame = 0;
	int nMiddleEncodingFrame = 0;
	int nEndEncodingFrame = 0;

	VideoWriter writer; 
	Mat stabilizedFrame; 				
	cv::Size stabilizedSize;

	vector<Mat> stabilizedFrame_; 	

	int nObjectIndex = 0;
	int nFrameCount = 0;

	while (!(stabilizedFrame = stabilizedFrames->nextFrameForGeneratingTransformationMatrix()).empty()) 	
	{
		stabilizedSize = stabilizedFrame.size();

		//ResizeFrameToFHD(stabilizedFrame,stabilizedFrame);
		resize(stabilizedFrame,stabilizedFrame,cv::Size(G4DGetTargetWidth(),G4DGetTargetHeight()),0,0,CV_INTER_CUBIC);
		
		if(nStartWarpFrame == 0)
		{
			nStartWarpFrame ++;
			nObjectIndex++;
			continue;
		}

		if(nStabilCnt-1 == nStartWarpFrame)
		{
			continue;
		}

		//Insert data
		stDivideFrame* pDivFrame = pArrDivInfo->at(nObjectIndex);
		if(nFrameCount >= pArrDivInfo->at(nObjectIndex)->pArrFrameData->size())
		{
			//pArrDivInfo->at(nObjectIndex)->nFinish = MAKING_WAIT;
			nObjectIndex++;
			nFrameCount = 0;
		}
		pArrDivInfo->at(nObjectIndex)->pArrFrameData->at(nFrameCount++).StabFrame = stabilizedFrame.clone();
		nStartWarpFrame++;
	}		

	vector<Mat> testMotion = _twoPassStabilizer->getMotions();

	SetFirstMovieRotMat(testMotion[0]);//First frame
	SetLastMovieRotMat(testMotion[testMotion.size() - 2]); // Last
	SetRectSize(stabilizedSize);
}
//void C4DReplaySDIStabilizer::CalcFrontAndRearFrameCount(vector<TEMPLATE_STRUCT> templateStruct)
//{
//	double nFrame = 33.3333;
//	if(ESMGetFrameRate() == 0 || ESMGetFrameRate() == 4)
//	{
//		nFrame = 40;
//	}else if(ESMGetFrameRate() == 1 || ESMGetFrameRate() == 3)
//	{
//		nFrame = 33.3333;
//	}else if(ESMGetFrameRate() == 2)
//	{
//		nFrame = 20;
//	}else if(ESMGetFrameRate() == 8)
//	{
//		nFrame = 33.3333;
//	}
//	else
//	{
//		nFrame = 33.3333;
//	}
//
//	CString strNFrame;
//	strNFrame.Format(_T("Stablization nFrame = %lf"), nFrame);
//	G4DShowLog(5, strNFrame);
//
//	int frontTemplate = 0;
//	int rearTemplate = templateStruct.size() - 1;
//	int frontFrame = 0;
//	int rearFrame = _stabilizerEndFrameNumber;
//
//	for(int i = 0; i < templateStruct.size(); i++)
//	{
//		int nFrameRate = templateStruct[i].nFrameRate;
//		double frameVolume = nFrameRate/nFrame;
//		if(frameVolume == 0)
//			frameVolume = 1;
//
//		if(templateStruct[i].nStrartDSC != templateStruct[i].nEndDSC)
//		{
//			frontTemplate = i;
//			break;
//		}
//		else
//		{
//			_stabilizerStartFrameNumber += 
//				((templateStruct[i].nEndTime - templateStruct[i].nStartTime) / (double)(nFrame))
//				* (frameVolume);
//		}
//	}
//
//	for(int i = templateStruct.size()-1; i >= 0; i--)
//	{
//		double frameVolume = templateStruct[i].nFrameRate/nFrame;
//		if(frameVolume == 0)
//			frameVolume = 1;
//
//		if(templateStruct[i].nStrartDSC != templateStruct[i].nEndDSC)
//		{
//			rearTemplate = i;				
//			break;
//		}
//		else
//		{
//			_stabilizerEndFrameNumber -=
//				((templateStruct[i].nEndTime - templateStruct[i].nStartTime) / (double)(nFrame))
//				* (frameVolume) - 1;
//		}
//	}
//	_stabilizerStartFrameNumber++;
//	_stabilizerEndFrameNumber--;
//	_stabilizerEndFrameNumber--;
//}
//
