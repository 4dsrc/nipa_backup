// 4DReplaySDIPlayerOptDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DReplaySDIPlayer.h"
#include "4DReplaySDIPlayerOptDlg.h"
#include "afxdialogex.h"


// C4DReplaySDIPlayerOptDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(C4DReplaySDIPlayerOptDlg, CDialogEx)

C4DReplaySDIPlayerOptDlg::C4DReplaySDIPlayerOptDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(C4DReplaySDIPlayerOptDlg::IDD, pParent)
{

}

C4DReplaySDIPlayerOptDlg::~C4DReplaySDIPlayerOptDlg()
{
}

void C4DReplaySDIPlayerOptDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(C4DReplaySDIPlayerOptDlg, CDialogEx)
	ON_BN_CLICKED(IDC_OPT_BTN_INFO, &C4DReplaySDIPlayerOptDlg::OnBnClickedOptBtnInfo)
	ON_BN_CLICKED(IDC_OPT_BTN_OUTPUT, &C4DReplaySDIPlayerOptDlg::OnBnClickedOptBtnOutput)
	ON_BN_CLICKED(IDC_OPT_BTN_LOG, &C4DReplaySDIPlayerOptDlg::OnBnClickedOptBtnLog)
	ON_BN_CLICKED(IDC_OPT_BTN_BACK, &C4DReplaySDIPlayerOptDlg::OnBnClickedOptBtnBack)
	ON_BN_CLICKED(IDC_OPT_BTN_LOGO, &C4DReplaySDIPlayerOptDlg::OnBnClickedOptBtnLogo)
	ON_BN_CLICKED(IDOK, &C4DReplaySDIPlayerOptDlg::OnBnClickedOk)
END_MESSAGE_MAP()

void C4DReplaySDIPlayerOptDlg::Init(AJAOption* Opt)
{
	m_pOpt = Opt;
}

void C4DReplaySDIPlayerOptDlg::OnBnClickedOptBtnInfo()
{
	
}

void C4DReplaySDIPlayerOptDlg::OnBnClickedOptBtnOutput()
{
	BROWSEINFO BrInfo;
	TCHAR szBuffer[512];                                      // 경로저장 버퍼 

	::ZeroMemory(&BrInfo,sizeof(BROWSEINFO));
	::ZeroMemory(szBuffer, 512); 

	BrInfo.hwndOwner = GetSafeHwnd(); 
	BrInfo.lpszTitle = _T("파일이 저장될 폴더를 선택하세요");
	BrInfo.ulFlags = BIF_NEWDIALOGSTYLE | BIF_EDITBOX | BIF_RETURNONLYFSDIRS;
	LPITEMIDLIST pItemIdList = ::SHBrowseForFolder(&BrInfo);
	::SHGetPathFromIDList(pItemIdList, szBuffer);               // 파일경로 읽어오기

	CString str;
	str.Format(_T("%s"),szBuffer);

	if(str.IsEmpty())
	{
		return;
	}
	else
	{
		m_pOpt->strBack = str;
		GetDlgItem(IDC_OPT_EDIT_OUTPUT)->SetWindowText(str);
	}
	
	
}

void C4DReplaySDIPlayerOptDlg::OnBnClickedOptBtnLog()
{
	
}

void C4DReplaySDIPlayerOptDlg::OnBnClickedOptBtnBack()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(m_pOpt->strBack);

	CString szFilter = _T("Image File (*.png,*.bmp,*.jpg)|*.png;*.bmp;*.jpg|");
	const BOOL bVistaStyle = TRUE; 

	CString strMoviePath;
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  

	const int c_cMaxFiles = 20000;
	const int c_cbBufSize = (c_cMaxFiles * (MAX_PATH + 1)) + 1;

	dlg.GetOFN().lpstrFile = strMoviePath.GetBuffer(c_cbBufSize);
	dlg.GetOFN().nMaxFile  = c_cbBufSize;
	dlg.m_ofn.lpstrTitle = _T("Background Image");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName,strFilePath;
	if(dlg.DoModal() == IDOK)
	{
		CString csPath = dlg.GetPathName();
		m_pOpt->strBack = csPath;
		GetDlgItem(IDC_OPT_EDIT_BACK)->SetWindowText(csPath);
	}
	else
		return;
}

void C4DReplaySDIPlayerOptDlg::OnBnClickedOptBtnLogo()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(m_pOpt->strLogo);

	CString szFilter = _T("Image File (*.png,*.bmp,*.jpg)|*.png;*.bmp;*.jpg|");
	const BOOL bVistaStyle = TRUE; 

	CString strMoviePath;
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  

	const int c_cMaxFiles = 20000;
	const int c_cbBufSize = (c_cMaxFiles * (MAX_PATH + 1)) + 1;

	dlg.GetOFN().lpstrFile = strMoviePath.GetBuffer(c_cbBufSize);
	dlg.GetOFN().nMaxFile  = c_cbBufSize;
	dlg.m_ofn.lpstrTitle = _T("Logo Image");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName,strFilePath;
	if(dlg.DoModal() == IDOK)
	{
		CString csPath = dlg.GetPathName();
		m_pOpt->strBack = csPath;
		GetDlgItem(IDC_OPT_EDIT_LOGO)->SetWindowText(csPath);
	}
	else
		return;
}

BOOL C4DReplaySDIPlayerOptDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	GetDlgItem(IDC_OPT_EDIT_INFO)->SetWindowText(m_pOpt->strInfo);
	GetDlgItem(IDC_OPT_EDIT_OUTPUT)->SetWindowText(m_pOpt->strOutput);
	GetDlgItem(IDC_OPT_EDIT_LOG)->SetWindowText(m_pOpt->strLog);
	GetDlgItem(IDC_OPT_EDIT_LOGO)->SetWindowText(m_pOpt->strLogo);
	GetDlgItem(IDC_OPT_EDIT_BACK)->SetWindowText(m_pOpt->strBack);

	GetDlgItem(IDC_OPT_EDIT_INFO)->EnableWindow(FALSE);
	//GetDlgItem(IDC_OPT_EDIT_OUTPUT)->EnableWindow(FALSE);
	return TRUE; 
}


void C4DReplaySDIPlayerOptDlg::OnBnClickedOk()
{
	GetDlgItem(IDC_OPT_EDIT_INFO)->GetWindowText(m_pOpt->strInfo);
	GetDlgItem(IDC_OPT_EDIT_OUTPUT)->GetWindowText(m_pOpt->strOutput);
	GetDlgItem(IDC_OPT_EDIT_LOG)->GetWindowText(m_pOpt->strLog);
	GetDlgItem(IDC_OPT_EDIT_LOGO)->GetWindowText(m_pOpt->strLogo);
	GetDlgItem(IDC_OPT_EDIT_BACK)->GetWindowText(m_pOpt->strBack);

	CDialogEx::OnOK();
}
