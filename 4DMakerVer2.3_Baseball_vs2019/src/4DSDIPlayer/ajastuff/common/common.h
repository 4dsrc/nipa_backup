/**	@file common.h
 *	Private include file for all ajastuff api code files.
 *
 *	Copyright (C) 2009 AJA Video Systems, Inc.  Proprietary and Confidential information.  All rights reserved.
 */

#ifndef AJA_COMMON_H
#define AJA_COMMON_H

#if defined(AJA_WINDOWS)
	#pragma warning(disable:4996)
	#pragma warning(disable:4800)
#endif

#include "ajastuff/common/public.h"
#include "ajastuff/system/debug.h"

#endif
