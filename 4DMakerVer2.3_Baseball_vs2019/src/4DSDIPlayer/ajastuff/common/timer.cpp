//---------------------------------------------------------------------------------------------------------------------
//	timer.cpp
//
//	Copyright (C) 2009 AJA Video Systems, Inc.  Proprietary and Confidential information.  All rights reserved.
//---------------------------------------------------------------------------------------------------------------------

#include "ajastuff/common/common.h"
#include "ajastuff/common/timer.h"
#include "ajastuff/system/systemtime.h"
#include "ajastuff/system/system.h"


AJATimer::AJATimer()
{
	mStartTime = 0;
	mStopTime = 0;
	mRun = false;
}


AJATimer::~AJATimer()
{
}


void 
AJATimer::Start()
{
	// save the time at start
	mStartTime = AJATime::GetSystemTime();

	mRun = true;
}


void 
AJATimer::Stop()
{
	// save the time at stop
	mStopTime = AJATime::GetSystemTime();

	mRun = false;
}


void 
AJATimer::Reset()
{
	// clear the start and stop time
	mStartTime = 0;
	mStopTime = 0;
	mRun = false;
}


uint32_t 
AJATimer::ElapsedTime()
{
	uint32_t elapsedTime = 0;

	// compute the elapsed time when running
	if (mRun)
	{
		elapsedTime = (uint32_t)(AJATime::GetSystemTime() - mStartTime);
	}
	// compute the elapsed time when stopped
	else
	{
		elapsedTime = (uint32_t)(mStopTime - mStartTime);
	}

	return elapsedTime;
}


bool 
AJATimer::Timeout(uint32_t interval)
{
	// timeout is true if greater than specified interval
	if (ElapsedTime() >= interval)
	{
		return true;
	}

	return false;
}


bool
AJATimer::IsRunning(void)
{
	return mRun;
}
