/**	@file threadimpl.h
 *	Interface to system dependent thread implementation.
 *
 *	Copyright (C) 2009 AJA Video Systems, Inc.  Proprietary and Confidential information.  All rights reserved.
 */

#ifndef AJA_THREAD_IMPL_H
#define AJA_THREAD_IMPL_H

#include <pthread.h>
#include "ajastuff/system/system.h"
#include "ajastuff/common/common.h"
#include "ajastuff/system/thread.h"
#include "ajastuff/system/lock.h"


class AJAThreadImpl
{
public:

	AJAThreadImpl(AJAThread* pThreadContext);
	virtual ~AJAThreadImpl();

	AJAStatus Start();
	AJAStatus Stop(uint32_t timeout = 0xffffffff);

	AJAStatus Kill(uint32_t exitCode);

	bool Active();
	bool IsCurrentThread();

	AJAStatus SetPriority(AJAThreadPriority threadPriority);
	AJAStatus GetPriority(AJAThreadPriority* pThreadPriority);

	AJAStatus Attach(AJAThreadFunction* pThreadFunction, void* pUserContext);

	static void*  ThreadProcStatic(void* pThreadImplContext);
	AJAStatus SetThreadName(const char *name);

	AJAThread* mpThreadContext;
	pthread_t mThread;
	AJAThreadPriority mPriority;
	AJAThreadFunction* mThreadFunc;
	void* mpUserContext;
	AJALock mLock;
	bool mTerminate;
    bool mExiting;
	pthread_mutex_t mExitMutex;
	pthread_cond_t mExitCond;
};


#endif
