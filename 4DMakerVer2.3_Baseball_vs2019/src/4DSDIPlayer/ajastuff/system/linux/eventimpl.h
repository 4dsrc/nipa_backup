/**	@file eventimpl.h
 *	Interface to system dependent event implementation.
 *
 *	Copyright (C) 2010 AJA Video Systems, Inc.  Proprietary and Confidential information.  All rights reserved.
 */

#ifndef AJA_EVENT_IMPL_H
#define AJA_EVENT_IMPL_H

#include "ajastuff/system/system.h"
#include "ajastuff/common/common.h"
#include "ajastuff/system/event.h"


class AJAEventImpl
{
public:

	AJAEventImpl(bool manualReset, const std::string& name);
	virtual ~AJAEventImpl(void);

	AJAStatus			Signal(void);
	AJAStatus			Clear(void);

	AJAStatus			SetState(bool signaled = true);
	AJAStatus			GetState(bool* pSignaled);
	
	AJAStatus			SetManualReset(bool manualReset);
	AJAStatus			GetManualReset(bool* pManualReset);

	AJAStatus			WaitForSignal(uint32_t timeout = 0xffffffff);

	virtual AJAStatus	GetEventObject(uint64_t* pEventObject);

private:
	pthread_mutex_t		mMutex;
	pthread_cond_t		mCondVar;
	bool				mSignaled;
	bool				mManualReset;
};


#endif

