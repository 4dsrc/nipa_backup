//---------------------------------------------------------------------------------------------------------------------
//	lock.cpp
//
//	Copyright (C) 2009 AJA Video Systems, Inc.  Proprietary and Confidential information.  All rights reserved.
//---------------------------------------------------------------------------------------------------------------------

// include the system dependent implementation class
#if defined(AJA_WINDOWS)
	#include "ajastuff/system/windows/lockimpl.h"
#endif

#if defined(AJA_LINUX)
	#include "ajastuff/system/linux/lockimpl.h"
#endif

#if defined(AJA_MAC)
	#include "ajastuff/system/mac/lockimpl.h"
#endif

AJALock::AJALock(const char* pName)
{
	mpImpl = NULL;
	// create the implementation class
	mpImpl = new AJALockImpl(pName);
}


AJALock::~AJALock()
{
	if(mpImpl)
		delete mpImpl;
}

// interface to the implementation class

AJAStatus
AJALock::Lock(uint32_t timeout)
{
	return mpImpl->Lock(timeout);
}


AJAStatus
AJALock::Unlock()
{
	return mpImpl->Unlock();
}


AJAAutoLock::AJAAutoLock(AJALock* pLock)
{
	mpLock = pLock;
	if (mpLock)
	{
		mpLock->Lock();
	}
}


AJAAutoLock::~AJAAutoLock()
{
	if (mpLock)
	{
		mpLock->Unlock();
	}
}

