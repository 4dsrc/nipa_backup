#pragma once
#include "stdafx.h"
#include "4DReplaySDIPlayerDlg.h"
#include "4DReplaySDIPlayerDecodingMgr.h"
#include "4DSDIPlayerIndexStructure.h"

class C4DReplaySDIPlayerDlg;
class C4DReplayDecodeMgr;

class C4DReplaySDIPlayerList : public CListCtrl
{
public:
	C4DReplaySDIPlayerList(void);
	~C4DReplaySDIPlayerList();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP();
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnDestroy();
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDropFiles(HDROP hDropInfo);

public:
	void Init(C4DReplaySDIPlayerDlg* pParent,C4DReplayDecodeMgr* pDecoder);
	void AddFile(CString strFileName,CString strFilePath,BOOL bDiv = FALSE);
	void DeleteSelList();
	void GetFile(int nIndex);
	int GetItemTotal(){return GetItemCount();}
	
	void SetAllPlayState(BOOL b){m_bPlayState = b;}
	BOOL GetAllPlayState(){return m_bPlayState;}
	void RunAllPlay();
	void ReplaceListData(int nIdx,CString strFileName,CString strFilePath,BOOL bDiv = FALSE);
	static unsigned WINAPI AllPlayThread(LPVOID param);
	
	
	CString GetPlayPath(int nIndex){return GetItemText(nIndex,LIST_FILEPATH);}
	CString GetPlayNum(int nIndex){return GetItemText(nIndex,LIST_NUM);}

private:
	C4DReplaySDIPlayerDlg* m_pParent;
	C4DReplayDecodeMgr*	   m_pDecoder;
	int m_nSelectedItem;
	int m_nSelectedIndex;
	BOOL m_bPlayState;
public:
	int GetListTotal(){return GetItemTotal();}
	int GetCurPlayIdx(){return m_nSelectedIndex;}
	void SetCurPlayIdx(int n){m_nSelectedIndex = n;}

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnListAddautoplay();
};