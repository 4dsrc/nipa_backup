#include "4DReplaySDIPlayer.h"
#include "4DReplaySDIPlayerDlg.h"
#include "4DSDIPlayerIndexStructure.h"
#include "ESMAJANetworkDefine.h"
#include "ESMDefine.h"

class C4DReplaySDIPlayerDlg;
class C4DReplaySDIPlayerApp;

extern int g_nTargetWidth;
extern int g_nTargetHeight;

extern void G4DPlayerReg(C4DReplaySDIPlayerDlg* pParent);
extern void G4DAppReg(C4DReplaySDIPlayerApp* pApp);
extern void G4DShowLog(int nPriority,CString strLog);
extern CString G4DGetPath(CString strIP,int nPath);
extern CString G4DGetFileName(int nPath);
extern void G4DSetTargetWidth(int nWidth);
extern void G4DSetTargetHeight(int nHeight);
extern int G4DGetTargetWidth();
extern int G4DGetTargetHeight();
extern void G4DSendBoardState();
extern void G4DAddEvent(ESMEvent* pEvent);
extern BOOL G4DGetBoardState();
extern void G4DSetAdjData(stAdjustInfo* pAdj,CString strDSCID);
extern stAdjustInfo G4DGetAdjData(CString strDSCID);
extern void G4DClearAllAdjust();
extern void G4DDoMakeFrame(AJASendGPUFrameInfo* AJASendInfo);
extern CString G4DGet4DCState(int n);
extern void G4DMakeFrameInfo(vector<MakeFrameInfo>*pArrMovieIndo,int nMakingIdx);
extern void G4DDivMakeStart(CString str,int nTotalCnt,int nCPUCnt);
extern void G4DSetCPUMakingIdx(int nIdx);
extern void G4DSetStartDecoding(CString strPath,int nIdx,int nFinish);

extern void G4DSetRecordingInfo(int nFPS,int nFrameCnt);
extern int G4DGetRecordingFPS();
extern int G4DGetRecordingFrameCnt();
extern BOOL G4DGetReverseMovie();
extern void G4DSetReverseMovie(BOOL b);
extern CString G4DGet4DMName();
extern BOOL G4DGetDivPlay();
extern void G4DSetDivPlay(BOOL b);
extern BOOL G4DCreateAllDirectory(CString strPath);
extern void G4DSetInsertLogo(BOOL b);
extern BOOL G4DGetInsertLogo();
extern CString G4DGetDistributeIP();
extern void G4DSetDistributeIP(CString str);
extern CString G4DGetFileNameFromPath(CString strPath);
extern void G4DSetStabilization(BOOL b);
extern BOOL G4DGetStabilization();
extern void G4DSetGPUProcess(BOOL b);
extern BOOL G4DGetGPUProcess();
extern void G4DSetGPUTotalMakingCnt(int n);
extern int G4DGetGPUTotalMakingCnt();
extern BOOL G4DGetEncodingState();
extern void G4DSetSelectFile(BOOL b);
extern BOOL G4DGetSelectFile();
extern void G4DSetLogoSave(BOOL b);
extern BOOL G4DGetLogoSave();
extern BOOL G4DGetEncode();
extern int G4DGetAutoPlaySize();
extern int G4DGetEncodeBitrate();
extern void G4DSetEncodeFrameRate(int n);
extern int G4DGetEncodeFrameRate();
extern BOOL G4DMemcpy(void * _Dst, const void * _Src, size_t _Size);
extern BOOL G4DGetEncodeTS();
extern CString G4DGetEncodePath();