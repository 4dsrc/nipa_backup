#pragma once
#include "StdAfx.h"
#include "4DReplaySDIPlayerDlg.h"
#include "4DReplaySDIPlayerFunc.h"


C4DReplaySDIPlayerApp* g_pWnd = NULL;
C4DReplaySDIPlayerDlg* g_pDlg = NULL;

void G4DPlayerReg(C4DReplaySDIPlayerDlg* pParent)
{
	//g_pWnd = (C4DReplaySDIPlayerApp*)AfxGetMainWnd();
	g_pDlg = pParent;
}
void G4DAppReg(C4DReplaySDIPlayerApp* pApp)
{
	g_pWnd = pApp;
}
void G4DShowLog(int nPriority,CString strLog)
{
	if(!g_pDlg)
		return;

	C4DReplaySDIPlayerLog* pLog = g_pWnd->m_cLog;

	if(pLog)
		pLog->AddLog(nPriority,strLog);
}
CString G4DGetFileName(int nPath)
{
	switch(nPath)
	{
	case BIN_PATH:
		return _T("bin");
	case CONFIG_PATH:
		return _T("config");
	case IMAGE_PATH:
		return _T("img");
	case SETUP_PATH:
		return _T("setup");
	}
}
CString G4DGetPath(CString strIP,int nPath)
{
	CString str4DMakerPath;
	str4DMakerPath.Format(_T("\\\\%s\\4DMaker\\%s"),strIP,G4DGetFileName(nPath));

	return str4DMakerPath;
}
void G4DSetTargetWidth(int nWidth)
{
	g_pWnd->m_nWidth = nWidth;
	//g_nTargetWidth = nWidth;
}
void G4DSetTargetHeight(int nHeight)
{
	g_pWnd->m_nHeight = nHeight;
	//g_nTargetHeight = nHeight;
}
int G4DGetTargetWidth()
{
	return g_pWnd->m_nWidth;
	//return 1;//g_nTargetWidth;
}
int G4DGetTargetHeight()
{
	return g_pWnd->m_nHeight;

	//return 1;//g_nTargetHeight;
}
extern void G4DAddEvent(ESMEvent* pEvent)
{
	g_pDlg->m_pTCP->m_arMsg.push_back(pEvent);
}
extern void G4DSendBoardState()
{
	CString* pStr = new CString;
	ESMEvent* pEvent = new ESMEvent;
	pEvent->message = AJA_BOARDSTATE;
	pEvent->nParam1 = G4DGetBoardState();

	G4DAddEvent(pEvent);
}
extern BOOL G4DGetBoardState()
{
	return g_pDlg->GetBoardState();
}
extern void G4DSetAdjData(stAdjustInfo* pAdj,CString strDSCID)
{
	g_pDlg->m_pDecodeMgr->SetAdjustData(pAdj,strDSCID);
}
extern stAdjustInfo G4DGetAdjData(CString strDSCID)
{
	stAdjustInfo adj = g_pDlg->m_pDecodeMgr->GetAdjustData(strDSCID);

	return adj;
}
extern void G4DClearAllAdjust()
{
	g_pDlg->m_pDecodeMgr->m_pDivMgr->ClearAllAdjust();
}
extern CString G4DGet4DCState(int n)
{
	switch(n)
	{
	case 1:case 2:
		{
			return _T("Recording Start");
		}
		break;
	case 9:
		{
			return _T("Recording Finish");
		}
		break;
	case VK_NUMPAD1:case VK_NUMPAD2:case VK_NUMPAD3:
	case VK_NUMPAD4:case VK_NUMPAD5:case VK_NUMPAD6:
	case VK_NUMPAD7:case VK_NUMPAD8:case VK_NUMPAD9:
		{
			return _T("Making Start!");
		}
		break;
	default:
		return _T("Don't know message");
		break;
	}
}
extern void G4DDivMakeStart(CString str,int nTotalCnt,int nCPUCnt)
{
#if 0
	CString strPath = g_pDlg->m_pDecodeMgr->AddMovieList(str);
	g_pDlg->m_cList->AddFile(_T("[AJA]")+str,strPath,TRUE);
#endif
	int nIdx = g_pDlg->m_cList->GetItemCount();

	g_pDlg->m_pDecodeMgr->Set4DMName(str);
	g_pDlg->m_cList->AddFile(_T("[AJA]-Making...."),_T(""),TRUE);

	if(G4DGetStabilization())
		g_pDlg->m_pDecodeMgr->StartStabilizationPlay(str,nTotalCnt,nCPUCnt,nIdx);
	else
		g_pDlg->m_pDecodeMgr->StartDistributePlay(str,nTotalCnt,nIdx);

	G4DSetGPUTotalMakingCnt(nTotalCnt - nCPUCnt);

	CString strTotalCnt;
	strTotalCnt.Format(_T("Making Total Cnt: %d"),nTotalCnt);
	G4DShowLog(5,strTotalCnt);
}
extern void G4DSetCPUMakingIdx(int nIdx)
{
	g_pDlg->m_pDecodeMgr->DoWaitDivData(nIdx);
}
extern void G4DSetStartDecoding(CString strPath,int nIdx,int nFinish)
{
	g_pDlg->m_pDecodeMgr->DoCPUDecoding(strPath,nIdx,nFinish);
}
extern void G4DDoMakeFrame(AJASendGPUFrameInfo* AJASendInfo)
{
	g_pDlg->m_pDecodeMgr->DoGPUDecoding(AJASendInfo);
}
extern void G4DSetRecordingInfo(int nFPS,int nFrameCnt)
{
	g_pDlg->m_pDecodeMgr->m_pDivMgr->m_nFPS		= nFPS;
	g_pDlg->m_pDecodeMgr->m_pDivMgr->m_nFrame	= nFrameCnt;
}
extern int G4DGetRecordingFPS()
{
	return g_pDlg->m_pDecodeMgr->m_pDivMgr->m_nFPS;
}
extern int G4DGetRecordingFrameCnt()
{
	return g_pDlg->m_pDecodeMgr->m_pDivMgr->m_nFrame;
}
extern void G4DMakeFrameInfo(vector<MakeFrameInfo>*pArrMovieIndo,int nMakingIdx)
{
	//g_pDlg->m_pDecodeMgr->DoGPUDecoding(AJASendInfo);
	g_pDlg->m_pDecodeMgr->DoGPUDecoding(pArrMovieIndo,nMakingIdx);
}
extern void G4DSetReverseMovie(BOOL b)
{
	g_pDlg->m_pDecodeMgr->m_bReverse = b;
}
extern BOOL G4DGetReverseMovie()
{
	return g_pDlg->m_pDecodeMgr->m_bReverse;
}
extern CString G4DGet4DMName()
{
	return g_pDlg->m_pTCP->m_str4DMName;
}
extern void G4DSetDivPlay(BOOL b)
{
	g_pDlg->m_pDecodeMgr->SetPlayDivFrame(b);

	if(g_pDlg->m_pDecodeMgr->GetMovieSignal() == MOVIE_PAUSE ||
		g_pDlg->m_pDecodeMgr->GetMovieSignal() == MOVIE_PLAY)
		g_pDlg->m_pDecodeMgr->SetMovieSignal(MOVIE_STOP);
}
extern BOOL G4DGetDivPlay()
{
	return g_pDlg->m_pDecodeMgr->GetPlayDivFrame();
}
CString RemoveSlash(LPCTSTR Path)
{
	CString cs = Path;
	::PathRemoveBackslash(cs.GetBuffer(_MAX_PATH));
	cs.ReleaseBuffer(_MAX_PATH);
	return cs;
}
BOOL FileExists(LPCTSTR Path)
{
	return (::PathFileExists(Path));
}
extern BOOL G4DCreateAllDirectory(CString strPath)
{

		if(strPath.IsEmpty())
			return FALSE;

		//Remove ending / if exists
		RemoveSlash(strPath);

		//If this folder already exists no need to create it
		if(FileExists(strPath))
			return TRUE;

		//Recursive call, one fewer folders
		int nFound = strPath.ReverseFind(_T('\\'));
		G4DCreateAllDirectory(strPath.Left(nFound));

		//Actually create a folder
		CreateDirectory(strPath, NULL);
}
extern void G4DSetInsertLogo(BOOL b)
{
	g_pDlg->m_pDecodeMgr->m_bInsertLogo = b;
}
extern BOOL G4DGetInsertLogo()
{
	return g_pDlg->m_pDecodeMgr->m_bInsertLogo;
}
extern CString G4DGetDistributeIP()
{
	return g_pDlg->m_pTCP->m_strIP;
}
extern void G4DSetDistributeIP(CString str)
{
	g_pDlg->m_pTCP->SetIP(str,1);
}
extern CString G4DGetFileNameFromPath(CString strPath)
{
	CString strFileName;
	strFileName = strPath.Right(strPath.GetLength() - strPath.ReverseFind(_T('\\')) -1);

	return strFileName;
}
extern void G4DSetStabilization(BOOL b)
{
	g_pDlg->m_pDecodeMgr->SetStabilization(b);
}
extern BOOL G4DGetStabilization()
{
	return g_pDlg->m_pDecodeMgr->GetStabilization();
}
extern void G4DSetGPUProcess(BOOL b)
{
	g_pDlg->m_pDecodeMgr->m_pDivMgr->SetGPUProcess(b);
}
extern BOOL G4DGetGPUProcess()
{
	return g_pDlg->m_pDecodeMgr->m_pDivMgr->GetGPUProcess();
}
extern void G4DSetGPUTotalMakingCnt(int n)
{
	g_pDlg->m_pDecodeMgr->m_pDivMgr->m_nGPUMakingCnt = n;
}
extern int G4DGetGPUTotalMakingCnt()
{
	return g_pDlg->m_pDecodeMgr->m_pDivMgr->m_nGPUMakingCnt;
}
extern BOOL G4DGetEncodingState()
{
	BOOL b;
	C4DReplayDecodeMgr* pDecodeMgr = g_pDlg->m_pDecodeMgr;

	if(pDecodeMgr->GetDivState() == TRUE || pDecodeMgr->GetEncodingFlag() == TRUE)
		b = TRUE;
	else
		b = FALSE;

	return b;
}
extern void G4DSetSelectFile(BOOL b)
{
	g_pDlg->m_pDecodeMgr->SetSelectFile(b);
}
extern BOOL G4DGetSelectFile()
{
	return g_pDlg->m_pDecodeMgr->GetSelectFile();
}
extern void G4DSetLogoSave(BOOL b)
{
	g_pDlg->m_bLogoSave = b;
}
extern BOOL G4DGetLogoSave()
{
	return g_pDlg->m_bLogoSave;
}
extern BOOL G4DGetEncode()
{
	return g_pDlg->m_bEncode;
}
extern int G4DGetAutoPlaySize()
{
	return g_pDlg->GetAutoPlaySize();
}
extern int G4DGetEncodeBitrate()
{
	return g_pDlg->m_nEncodeBitrate;
}
extern void G4DSetEncodeFrameRate(int n)
{
	g_pDlg->m_pDecodeMgr->SetFrameRate(n);
}
extern int G4DGetEncodeFrameRate()
{
	return g_pDlg->m_pDecodeMgr->GetFrameRate();
}
BOOL G4DMemcpy(void * _Dst, const void * _Src, size_t _Size)
{
	__try
	{
		memcpy(_Dst, _Src, _Size);		
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		//int nDataSize = _Size;
		return FALSE;
	}

	return TRUE;
}
BOOL G4DGetEncodeTS()
{
	return g_pDlg->m_bTransTS;
}
CString G4DGetEncodePath()
{
	CString str = g_pDlg->m_stAJAOpt.strOutput;
	return	str;
}