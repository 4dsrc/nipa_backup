#include "StdAfx.h"
#include "4DReplaySDIPlayerLog.h"

C4DReplaySDIPlayerLog::C4DReplaySDIPlayerLog(void)
{
	InitializeCriticalSection(&m_csLock);
	m_arLog = new C4DReplayArray;
	m_bThreadStop = FALSE;
	LaunchLogoThread();
	PCFreq					= 0.0;
	CounterStart			= 0;
	StartCounter();
}
C4DReplaySDIPlayerLog::~C4DReplaySDIPlayerLog()
{
	m_bThreadStop = TRUE;
	if(m_arLog)
	{
		delete m_arLog;
		m_arLog = NULL;
	}
	DeleteCriticalSection(&m_csLock);
}
void C4DReplaySDIPlayerLog::Init(CString strPath)
{
	SYSTEMTIME systime;
	GetLocalTime(&systime);
	CString strFolder;
	strFolder.Format(_T("%04d-%02d-%02d\\"),systime.wYear,systime.wMonth,systime.wDay);
	

	CString strFileName;//2018_05_15_11_26_28_4DMaker
	strFileName.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d_SDIPlayer.log"),systime.wYear,systime.wMonth,systime.wDay,
																													systime.wHour,systime.wMinute,systime.wSecond);
	//2018_05_15_11_26_28
	m_strFile.Format(_T("%s\\%s"),	strPath,strFolder);
	G4DCreateAllDirectory(m_strFile);
	m_strFile.Append(strFileName);

	SetExtendedStyle(LVS_EX_DOUBLEBUFFER | LVS_EX_FULLROWSELECT );

	InsertColumn(LIST_LOG_TIME,_T("TIME"),LVCFMT_LEFT,150);	
	InsertColumn(LIST_LOG_NAME,_T("List"),LVCFMT_LEFT,500);
	InsertColumn(LIST_LOG_PRIORITY,_T("Priority"),LVCFMT_LEFT,1);
}
void C4DReplaySDIPlayerLog::AddLog(int nPriority,CString strName)
{
	//int nTotal = GetItemCount();
	int nTime = 0;
	CString strTime = GetTime(nTime);
	//CString strPriority;
	//strPriority.Format(_T("%d"),nPriority);

	//InsertItem(nTotal, strTime);
	//SetItem(nTotal,LIST_LOG_TIME,LVIF_TEXT,strTime,0,0,0,NULL);
	//SetItem(nTotal,LIST_LOG_NAME,LVIF_TEXT,strName,0,0,0,NULL);
	//SetItem(nTotal,LIST_LOG_PRIORITY,LVIF_TEXT,strPriority,0,0,0,NULL);

	//SetItemState(nTotal,LVIS_FOCUSED|LVIS_SELECTED,LVIS_FOCUSED|LVIS_SELECTED);
	//EnsureVisible(nTotal,FALSE);

	LogMsg* pLog = new LogMsg;
	pLog->strTime = strTime;
	pLog->nTick	  = nTime;
	pLog->strName = strName;
	pLog->nPriority = nPriority;

	m_arLog->Add((CObject*)pLog);
//	TRACE("[%d]%S %S\n",nPriority,strTime,strName);
	//text file save
	//WriteLog(strTime,strName,nPriority);
	//if(nTotal > 500)
	//	DeleteAllItems();
}
CString C4DReplaySDIPlayerLog::GetTime(int &nTime)
{
	CString strDate = _T("");

	SYSTEMTIME st;

	GetLocalTime(&st);
	int nHour = st.wHour;
	int nMin  = st.wMinute;
	int nSec = st.wSecond;
	int nMilli = st.wMilliseconds;
	
	nTime = nHour * 3600000 + nMin * 60000 + nSec * 1000 + nMilli;
	strDate.Format(_T("[%02d:%02d:%02d:%03d]"), nHour,nMin,nSec,nMilli);
	return strDate;
}

BOOL C4DReplaySDIPlayerLog::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{

	return CListCtrl::OnNotify(wParam, lParam, pResult);
}
BEGIN_MESSAGE_MAP(C4DReplaySDIPlayerLog, CListCtrl)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &C4DReplaySDIPlayerLog::OnNMCustomdraw)
END_MESSAGE_MAP()


void C4DReplaySDIPlayerLog::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	
	*pResult = 0;

	LPNMLVCUSTOMDRAW pLVCD = reinterpret_cast<LPNMLVCUSTOMDRAW>(pNMHDR);

	if(pLVCD->nmcd.dwDrawStage == CDDS_PREPAINT)
		*pResult = CDRF_NOTIFYITEMDRAW;
	else if(pLVCD->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
	{
		int nItem = static_cast<int>(pLVCD->nmcd.dwItemSpec);
		CString strTemp = GetItemText(nItem, LIST_LOG_PRIORITY/*pLVCD->iSubItem*/);
		if(!strTemp.IsEmpty())
		{
			if(strTemp == _T("0"))
			{
				pLVCD->clrText		= RGB(0,0,0);
				pLVCD->clrTextBk	= RGB(242,112,96); //red
			}else if(strTemp == _T("1")){
				pLVCD->clrText		= RGB(0,0,0);
				pLVCD->clrTextBk	= RGB(128,255,128); //red
			}else if(strTemp == _T("2")){
				pLVCD->clrText		= RGB(0,0,0);
				pLVCD->clrTextBk	= RGB(51,153,255); //red
			}else
			{
				pLVCD->clrText		= RGB(0,0,0);
				pLVCD->clrTextBk	= RGB(255,255,255); //green
			}
		}
		//else
		//{
		//	//기본색상 - 화이트 배경 / 검정 텍스트
		//	pLVCD->clrText = RGB(0,0,0);
		//	pLVCD->clrTextBk = RGB(255,255,255);
		//}
		*pResult = CDRF_DODEFAULT;
	}
}
void C4DReplaySDIPlayerLog::WriteLog(CString strTime,CString strName,int nPriority)
{
	CString strWriteData;
	strWriteData.Format(_T("%s [%d] [%d]  %s"),strTime,GetTickCount(),nPriority,strName);

	EnterCriticalSection(&m_csLock);
	if(m_fWriteFile.m_hFile != CFile::hFileNull)
	{
		strWriteData = strWriteData + _T("\r\n");
		m_fWriteFile.Write(strWriteData, strWriteData.GetLength() * 2);

		m_fWriteFile.Flush();
	}else
	{
		if (m_fWriteFile.Open(m_strFile, CFile::modeWrite | CFile::modeCreate | CFile::modeNoTruncate))
		{
			m_fWriteFile.SeekToEnd();
			strWriteData = strWriteData + _T("\r\n");
			m_fWriteFile.Write(strWriteData, strWriteData.GetLength() * 2);

			m_fWriteFile.Flush();
		}
	}
	m_fWriteFile.Close();
	LeaveCriticalSection(&m_csLock);
}
void C4DReplaySDIPlayerLog::WriteLogMsg(LogMsg* pLog)
{
	int nTotal = GetItemCount();
	CString strPriority;
	strPriority.Format(_T("%d"),pLog->nPriority);

	InsertItem(nTotal, pLog->strTime);
	SetItem(nTotal,LIST_LOG_TIME,LVIF_TEXT,pLog->strTime,0,0,0,NULL);
	SetItem(nTotal,LIST_LOG_NAME,LVIF_TEXT,pLog->strName,0,0,0,NULL);
	SetItem(nTotal,LIST_LOG_PRIORITY,LVIF_TEXT,strPriority,0,0,0,NULL);

	SetItemState(nTotal,LVIS_FOCUSED|LVIS_SELECTED,LVIS_FOCUSED|LVIS_SELECTED);
	EnsureVisible(nTotal,FALSE);

	CString strWriteData;
	strWriteData.Format(_T("%s [%.4f] [%d]  %s"),pLog->strTime,GetCounter(),pLog->nPriority,pLog->strName);
	//strWriteData.Format(_T("%s [%d] [%d]  %s"),pLog->strTime,pLog->nTick/*GetCounter()*/,pLog->nPriority,pLog->strName);
	EnterCriticalSection(&m_csLock);
	if(m_fWriteFile.m_hFile != CFile::hFileNull)
	{
		strWriteData = strWriteData + _T("\r\n");
		m_fWriteFile.Write(strWriteData, strWriteData.GetLength() * 2);

		m_fWriteFile.Flush();
	}else
	{
		if (m_fWriteFile.Open(m_strFile, CFile::modeWrite | CFile::modeCreate | CFile::modeNoTruncate))
		{
			m_fWriteFile.SeekToEnd();
			strWriteData = strWriteData + _T("\r\n");
			m_fWriteFile.Write(strWriteData, strWriteData.GetLength() * 2);

			m_fWriteFile.Flush();
		}
	}
	m_fWriteFile.Close();
	if(pLog)
	{
		delete pLog;
		pLog = NULL;
	}
	if(nTotal > 500)
		DeleteAllItems();
	LeaveCriticalSection(&m_csLock);
}
void C4DReplaySDIPlayerLog::LaunchLogoThread()
{
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_WriteThread,this,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI C4DReplaySDIPlayerLog::_WriteThread(LPVOID param)
{
	C4DReplaySDIPlayerLog* pLogMgr = (C4DReplaySDIPlayerLog*) param;

	LogMsg* pLog = NULL;
	while(!pLogMgr->m_bThreadStop)
	{
		if(pLogMgr->m_bThreadStop == FALSE)
		{
			int nAll = pLogMgr->m_arLog->GetSize();
			while(nAll--)
			{
				pLog = (LogMsg*)pLogMgr->m_arLog->GetAt(0);
				if(!pLog)
				{
					pLogMgr->m_arLog->RemoveAt(0);
					continue;
				}

				pLogMgr->WriteLogMsg(pLog);

				pLogMgr->m_arLog->RemoveAt(0);
			}
		}
		Sleep(1);
	}

	return TRUE;
}
double C4DReplaySDIPlayerLog::GetCounter()
{
	//TRACE(_T("GET COUNTER\n"));

	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart-CounterStart)/PCFreq;
}
void C4DReplaySDIPlayerLog::StartCounter()
{
	//TRACE(_T("START COUNTER\n"));
	PCFreq = 0.0;
	CounterStart  = 0;

	LARGE_INTEGER li;
	if(!QueryPerformanceFrequency(&li))
		cout << "QueryPerformanceFrequency failed!\n";

	PCFreq = double(li.QuadPart)/1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}