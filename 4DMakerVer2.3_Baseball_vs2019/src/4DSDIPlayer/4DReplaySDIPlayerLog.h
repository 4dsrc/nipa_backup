#pragma once
#include "4DReplaySDIPlayerFunc.h"
enum
{
	LIST_LOG_TIME,
	LIST_LOG_NAME,
	LIST_LOG_PRIORITY,
};
typedef struct _LogMsg
{
	CString strTime;
	CString strName;
	int nTick;
	int nPriority;
}LogMsg;
class C4DReplaySDIPlayerLog : public CListCtrl
{
public:
	C4DReplaySDIPlayerLog(void);
	~C4DReplaySDIPlayerLog();
	
	void Init(CString strPath);
	void AddLog(int nPriority,CString strName);
	CString m_strTime;
	CString GetTime(int &nTime);//{return m_strTime;}
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	DECLARE_MESSAGE_MAP()
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
	void WriteLog(CString strTime,CString strName,int nPriority);
	CFile m_fWriteFile;
	CString m_strFile;

	CRITICAL_SECTION m_csLock;

	//180612 hjcho
	BOOL m_bThreadStop;
	void LaunchLogoThread();
	static unsigned WINAPI _WriteThread(LPVOID param);
	void WriteLogMsg(LogMsg* pLog);
	C4DReplayArray* m_arLog;

	//180614 hjcho
	double GetCounter();
	void StartCounter();
	__int64 CounterStart;
	double PCFreq;
};