#include "stdafx.h"
#include "4DReplayDivPlay.h"
#include "ESMIni.h"
#include "ESMUtil.h"

C4DReplaySDIPlayerDivMgr::C4DReplaySDIPlayerDivMgr()
{
	m_bThreadState = FALSE;
	m_pParent	   = NULL;
}
C4DReplaySDIPlayerDivMgr::~C4DReplaySDIPlayerDivMgr()
{

}
void C4DReplaySDIPlayerDivMgr::StartMoviePlay(CString strIP)
{
	if(GetThreadPlay() == TRUE)
	{
		AfxMessageBox(_T("기존 작업이 끝나지 않았다."));
		return;
	}
	SetThreadPlay(TRUE);

	m_strIP = strIP;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,_ThreadAutoPlay,this,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI C4DReplaySDIPlayerDivMgr::_ThreadAutoPlay(LPVOID param)
{
	C4DReplaySDIPlayerDivMgr* pDivMgr = (C4DReplaySDIPlayerDivMgr*) param;
	CESMIni ini;
	CString strIP = pDivMgr->m_strIP;
	//Read Ini
	CString strPath = GetPath(strIP,CONFIG_PATH);
	strPath.Append(_T("\\SDIDIVframe.ini"));

	ShowLog(5,_T("Start Div Thread"));

	int nReceiveCnt = 0;
	CString strTime;
	BOOL bPlay = FALSE;

	if(ini.SetIniFilename(strPath,0))
	{
		while(1)//Check INI File every 300ms
		{
			CString strState = ini.GetString(STR_SECTION_SDIDIVFRAME,STR_SECTION_STATE,_T(""));
			
			if(strState == _T("O"))
			{
				CString strRevCnt = ini.GetString(STR_SECTION_SDIDIVFRAME,STR_SECTION_SENDCNT,_T("")); 
				nReceiveCnt = _ttoi(strRevCnt);

				strTime = ini.GetString(STR_SECTION_SDIDIVFRAME,STR_SECTION_TIME,_T(""));

				ini.WriteString(STR_SECTION_SDIDIVFRAME,STR_SECTION_STATE,_T("W"));

				pDivMgr->m_pArrDecodeFrame = new vector<stDivideFrame>(nReceiveCnt);

				int nCnt = 0;
				
				while(1)
				{
					for(int i = 0; i < nReceiveCnt; i++)
					{
						CString strNum;
						strNum.Format(_T("%d"),i);
						if(ini.GetString(STR_SECTION_SDIDIVFRAME,strNum,_T("")) != _T("X"))
						{
							int nFrameCnt = _ttoi(ini.GetString(STR_SECTION_SDIDIVFRAME,strNum,_T("")));
							CString strLog;
							strLog.Format(_T("input - [%d/%d]"),i,nFrameCnt);
							pDivMgr->DoDecode(strTime,i,nCnt);
							ShowLog(5,strLog);

							ini.WriteString(STR_SECTION_SDIDIVFRAME,strNum,_T("X"));
							nCnt++;

							if(bPlay == FALSE)
							{
								bPlay = TRUE;
								pDivMgr->DoPlay();
							}
						}
						else
							continue;

						if(nCnt == nReceiveCnt)
							break;
					}
					
					if(nCnt == nReceiveCnt)
						break;

					Sleep(1);
				}
				ini.WriteString(STR_SECTION_SDIDIVFRAME,STR_SECTION_STATE,_T("X"));
				
				continue;
			}
			Sleep(300);
		}
		//Set the memory
	}
	else
	{
		AfxMessageBox(_T("Check your network connection or sharing path"));
	}

	pDivMgr->SetThreadPlay(FALSE);
	
	ShowLog(5,_T("Finish Div Thread"));

	return TRUE;
}
void C4DReplaySDIPlayerDivMgr::DoDecode(CString strTime,int nIdx,int nFrameCnt)
{
	//m_pArrDecodeFrame->at(nIdx).stArrFrame = new vector<stLoadFrame>(nFrameCnt);

	ThreadDecodeInfo* pThreadDecodeInfo = new ThreadDecodeInfo;
	pThreadDecodeInfo->strTime = strTime;
	pThreadDecodeInfo->nIdx	   = nIdx;
	pThreadDecodeInfo->pParent = this;
	pThreadDecodeInfo->nFrameCnt = nFrameCnt;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,_ThreadDecode,pThreadDecodeInfo,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI C4DReplaySDIPlayerDivMgr::_ThreadDecode(LPVOID param)
{
	ThreadDecodeInfo* pThreadInfo = (ThreadDecodeInfo*) param;
	C4DReplaySDIPlayerDivMgr* pDiv = pThreadInfo->pParent;
	CString strTime = pThreadInfo->strTime;
	int nIdx = pThreadInfo->nIdx;
	int nFrameCnt = pThreadInfo->nFrameCnt;

	delete pThreadInfo;

	char strPath[256];
	sprintf(strPath,"M:\\Movie\\%S\\%d.mp4",strTime,nIdx);
	
	stDivideFrame* pDivFrame = new stDivideFrame;
	
	VideoCapture vc(strPath);

	if(vc.isOpened() == false)
	{
		CString strLog;
		strLog.Format(_T("%S - Cannot Open"),strPath);
		pDivFrame->nFinish = -100;

		ShowLog(0,strLog);
		return FALSE;
	}

	int nCnt = 0;
	while(1)
	{
		Mat frame;
		vc >> frame;
		if(frame.empty())
			break;
		stLoadFrame Load;
		Load.bFinish = TRUE;
		Load.frame = frame.clone();
		
		pDivFrame->stArrFrame.push_back(Load);// = frame.clone();
	}
	pDivFrame->nFinish = 1;
	pDiv->m_pArrDecodeFrame->at(nIdx) = *pDivFrame;
	
	
	return TRUE;
}
void C4DReplaySDIPlayerDivMgr::DoPlay()
{
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_ThreadPlay,this,0,NULL);
}
unsigned WINAPI C4DReplaySDIPlayerDivMgr::_ThreadPlay(LPVOID param)
{
	C4DReplaySDIPlayerDivMgr* pDivMgr = (C4DReplaySDIPlayerDivMgr*)param;

	int nSize = pDivMgr->m_pArrDecodeFrame->size();
	int nPlayCnt = 0;
	
	namedWindow("FRAME",CV_WINDOW_FREERATIO);
	
	for(int i = 0 ; i < nSize; i++)
	{
		stDivideFrame* pDivFrame = &pDivMgr->m_pArrDecodeFrame->at(i);

		while(1)
		{
			if(pDivFrame->nFinish == 1)
				break;

			if(pDivFrame->nFinish == -100)
				break;
		}
		if(pDivFrame->nFinish == -100)
		{
			ShowLog(5,_T("Decoding Error"));
			continue;
		}
		char str[2];
		sprintf(str,"%d",i);
		for(int j = 0 ; j < pDivFrame->stArrFrame.size(); j++)
		{
			putText(pDivFrame->stArrFrame.at(j).frame,str,cv::Point(960,540),CV_FONT_HERSHEY_PLAIN,2,
				Scalar(0,0,0),1,8);
			imshow("FRAME",pDivFrame->stArrFrame.at(j).frame);
			waitKey(0);
		}
		nPlayCnt++;
	}

	destroyAllWindows();

	ShowLog(5,_T("Play Finish"));
	return TRUE;
}
void C4DReplaySDIPlayerDivMgr::SetAdjData(stAdjustInfo* pAdj,CString strDSCID)
{
	m_mpAdjData.insert(pair<CString,stAdjustInfo>(strDSCID,*pAdj));
}
stAdjustInfo C4DReplaySDIPlayerDivMgr::GetAdjData(CString strDSCID)
{
	stAdjustInfo Adjinfo;
	Adjinfo = m_mpAdjData[strDSCID];

	return Adjinfo;
}