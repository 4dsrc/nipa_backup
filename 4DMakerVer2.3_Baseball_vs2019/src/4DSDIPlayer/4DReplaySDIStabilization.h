#pragma once
#include <opencv2/highgui.hpp>

#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/videostab.hpp>

#include <opencv2/cudawarping.hpp>

#include "4DReplaySDIPlayer.h"
#include "4DReplaySDIPlayerDlg.h"
#include "ntv2outputtestpattern.h"
#include "4DSDIPlayerIndexStructure.h"
#include "ESMAJANetworkDefine.h"
#include "4DReplayDivMgr.h"
#include "FFMpegMgr.h"

using namespace std;
using namespace cv;

class C4DReplaySDIStabilizer
{
public:
	C4DReplaySDIStabilizer(bool isUHDtoFHD);
	~C4DReplaySDIStabilizer(void);

public:
	Ptr<videostab::IFrameSource>					_stabilizedFrames; 
	Ptr<videostab::ImageSetForStabilization>		_imageSetForStabilization;		
	Ptr<videostab::MotionEstimatorRansacL2>			_motionEstimation; 		
	Ptr<videostab::KeypointBasedMotionEstimatorGpu> _motionEstBuilder;
	Ptr<videostab::IOutlierRejector>				_outlierRejector;	

	videostab::RansacParams _ransac; 
	videostab::TwoPassStabilizer *_twoPassStabilizer;

	double _minInlierRatio;

	int _width;
	int _height;
	
	Mat RotFirstMat,RotLastMat;
	cv::Size m_szOutput;
	void SetFirstMovieRotMat(Mat rot){RotFirstMat = rot.clone();}
	Mat GetFirstMovieRotMat(){return RotFirstMat;}
	void SetLastMovieRotMat(Mat rot){RotLastMat = rot.clone();}
	Mat GetLastMovieRotMat(){return RotLastMat;}
	void SetRectSize(cv::Size sz){m_szOutput = sz;}
	cv::Size GetRectSize(){return m_szOutput;}
public:
	//void VideoDecoding(string strMovieFile, vector<TEMPLATE_STRUCT> templateStruct);
	bool VideoDecoding(string strMovieFile, int nStabilizationStartFrameNumber, int nStabilizationEndFrameNumber);
	void CreateStabilizatedMovie(string outputPath, double outputFps);
	void IncodingWithStabilizedImage(Ptr<videostab::IFrameSource> stabilizedFrames, string outputPath, double outputFps);

	void CreateStabilizerMovie(vector<stDivideFrame*>*pArrDivInfo,int nStabilCnt);
	void AdaptStabilizer(Ptr<videostab::IFrameSource> stabilizedFrames,vector<stDivideFrame*>*pArrDivInfo,int nStabilCnt);
public:
	vector<Mat> frontFrame_;
	vector<Mat> rearFrame_;
	vector<Mat> stabilizedFrame_; 	

	int _stabilizerStartFrameNumber;
	int _stabilizerEndFrameNumber;
	double _dFrameRate;

public:	
	void TranslationAndWarpPerspective(Mat& srcFrame, Mat& dstFrame, Mat& motion, cv::Size size);
	void ResizeFrameToFHD(Mat& srcFrame, Mat& dstFrame);
	//	void CalcFrontAndRearFrameCount(vector<TEMPLATE_STRUCT> templateStruct);	
};

