#pragma once
#include "4DReplaySDIPlayer.h"
#include "4DReplaySDIPlayerDlg.h"
#include "ntv2outputtestpattern.h"
#include "4DSDIPlayerIndexStructure.h"
#include "ESMAJANetworkDefine.h"
#include "4DReplayDivMgr.h"
#include "FFMpegMgr.h"
#include "4DReplaySDIStabilization.h"
using namespace cv;
class C4DReplaySDIPlayerDlg;
class C4DReplaySDIPlayerDivMgr;
class C4DReplaySDIStabilizer;

class C4DReplayDecodeMgr
{
public:
	C4DReplayDecodeMgr();
	C4DReplayDecodeMgr(AJAOption* pOption);
	~C4DReplayDecodeMgr();
public:
	void Init(C4DReplaySDIPlayerDlg* pParent,NTV2OutputTestPattern* pBoard = NULL)
	{m_pParent = pParent;m_pBoard   = pBoard;}

	double m_dbPlayTime;
	int m_nWaitTime;
	//Basic Play
	void SelectFileDecode(CString strPath);
	BOOL ShowDivFrame(vector<Mat>*pArrFrame);
	static unsigned WINAPI MovieDecodingThread(LPVOID param);
	static unsigned WINAPI MoviePlayThread(LPVOID param);
	void DoMoviePlay();//MoviePlayThread
	void DoPlayUsingIndex(int i);
	void ClearDecodedFile(BOOL bDiv);/*{m_pArrMovieArray->clear();}*/
	void CreateMovieArray(int nTotalFrame);
	void InsertMovieData(int nIdx,int nFrameIdx,stLoadFrame stLoad,BOOL bDiv);
	CRITICAL_SECTION m_criInsertData;

	//Distribute play
	BOOL m_bDivState;
	BOOL ShowDivMovie(int nIdx);
	BOOL DoDivPlayUsingIndex(int nIdx,int nFrameIdx);
	BOOL GetDivState(){return m_bDivState;}
	void SetDivState(BOOL b){m_bDivState = b;}
	C4DReplaySDIPlayerDivMgr* m_pDivMgr;
	void SetAdjustData(stAdjustInfo* stAdj,CString strDSC);
	stAdjustInfo GetAdjustData(CString strDSC);
	void StartDistributePlay(CString strName,int nTotalCnt,int nListIdx);
	
	//Play Signal
	void SetMovieSignal(int m_n){m_nStateSignal = m_n;}//m_nStateSignal
	int  GetMovieSignal(){return m_nStateSignal;}
	
	//Decoding state
	void SetDecodeState(BOOL b){m_bDecodeState = b;}//m_bDecodeState
	BOOL GetDecodeState(){return m_bDecodeState;}
	
	//Play state
	void SetPlayState(BOOL b){m_bPlayState = b;}//m_bPlayState
	BOOL GetPlayState(){return m_bPlayState;}

	//Play Method
	void SetPlayMethod(int n){m_nPlayMethod = n;}//m_nPlayMethod
	int  GetPlayMethod(){return m_nPlayMethod;}
	
	//Background Opt.
	void ShowBackGroundAsOurs();//m_matBackGround
	void ShowBackGroundAsBlack();
	void DoStopAllThread();
	void SetPlayFileInfo(int nFPS,int nTotalFrame);
	void BGR2YUV422(Mat frame,Mat YUV422,int nHeight,int nWidth,BOOL bInsertLogo=FALSE);
	void BGRCC2YUV422(Mat frame,Mat YUV422,int nHeight,int nWidth,FrameRGBInfo info,BOOL bInsertLogo=FALSE);
	void ScreenToYUV422(BYTE* pImg,Mat YUV422,int nHeight,int nWidth);
	int	CalcTransValue(int nPixel,int nRefValue);
	void DoColorRevision(cv::Mat* frame,FrameRGBInfo stColorInfo);
	void BGRCC2YUV422Logo(Mat frame,Mat Logo,Mat NoLogo,int nHeight,int nWidth,BOOL bColorRevision,FrameRGBInfo info);
	//Divide play
	int m_nTotalMakingCnt;
	BOOL m_bDivFrame;
	void SetPlayDivFrame(BOOL b);//{m_bDivFrame = b;}
	BOOL GetPlayDivFrame();//{return m_bDivFrame;}
	void DoInsertFrameData(int nIdx,stLoadFrame stLoad);
	void DoWaitDivData(int nIdx,BOOL bMode = TRUE,int nFrameCnt = 0);
	void DoCPUDecoding(CString strPath,int nIdx,int nFinish);
	void DoGPUDecoding(AJASendGPUFrameInfo* stAJAInfo);
	static unsigned WINAPI MovieDivPlayThread(LPVOID param);
	void DoGPUDecoding(vector<MakeFrameInfo>*pArrFrameInfo,int nMakingIdx);
	BOOL ShowDivMovie(int nIdx,vector<Mat>* ArrLogo,vector<Mat>*ArrNoLogo,double dbDelay = 0.0);

	/*Function*/
	double GetCounter();
	void StartCounter();
	__int64 CounterStart;
	double PCFreq;

	/*Function*/
	double GetDivCounter();
	void StartDivCounter();
	__int64 m_nDivCounterStart;
	double m_dbDivPCFreq;

	BOOL m_bReverse;
	BOOL m_bInsertLogo;
	/**/
	CString AddMovieList(CString &str);
	CString m_strOutputName;
	CString m_strFileName;
	void RunEncodeShell(CString strTemp,int nListIdx);
	BOOL EncodingMovie(vector<Mat>*pArrFrameInfo,int nListIdx);
	static unsigned WINAPI _EncodingThread(LPVOID param);
	void DoEncoding(CString strCmd,CString strPath,CString strFileName);
	BOOL m_bEncoding;
	void SetEncodingFlag(BOOL b){m_bEncoding = b;}
	BOOL GetEncodingFlag(){return m_bEncoding;}

	void SetBackgroundImage(CString strPath);
	void SetLogoImage(CString strPath);

	void StartStabilizationPlay(CString strName,int nTotalCnt,int nCPUMakingCnt,int nListIdx);
	static unsigned WINAPI MovieStablizationThread(LPVOID param);
	int m_nCPUMakingCnt;
	void SetStabilization(BOOL b){m_bStabilization = b;}
	BOOL GetStabilization(){return m_bStabilization;}
	void LaunchAffineThread(C4DReplaySDIStabilizer* pStable,int nTotalCnt,BOOL bSucess);
	static unsigned WINAPI MovieAdaptAffine(LPVOID param);
	void SetStabilValue(int nIdx,BOOL b)
	{
		if(m_pMapMovieArray[nIdx])
			m_pMapMovieArray[nIdx]->bIsLastFrame = b;
	}
	void SetStabilIncludeFrame(int nIdx,BOOL b)
	{
		if(m_pMapMovieArray[nIdx])
			m_pMapMovieArray[nIdx]->bStabilInclude = b;
	}
	BOOL m_bStabilization;
	void Set4DMName(CString str){m_str4DMPath = str;}

	void SetSelectFile(BOOL b){m_bSelectFile = b;}
	BOOL GetSelectFile(){return m_bSelectFile;}

	void SetSecondPath(CString str){m_strSecondSavePath = str;}
	CString GetSecondPath(){return m_strSecondSavePath;}

	void SetTest(BOOL b){m_bTest = b;}
	BOOL GetTest(){return m_bTest;}

	void SetFrameRate(int n){m_nFrameRate = n;}
	int GetFrameRate(){return m_nFrameRate;}

	void SetAffineFinish(BOOL b){m_bAffineFinish = b;}
	BOOL GetAffineFinish(){return m_bAffineFinish;}
private:
	int m_nStateSignal;
	BOOL m_bDecodeState;
	BOOL m_bPlayState;
	int m_nPlayMethod;

	CString m_strMoviePath;
	CString m_str4DMPath;
	Mat m_matBackGround;
	Mat m_matBlack;
	Mat m_matLogo;
	//vector<stLoadFrame>* m_pArrMovieArray;
	map<int,stDivideFrame*>m_pMapMovieArray;
	C4DReplaySDIPlayerDlg *m_pParent;
	NTV2OutputTestPattern *m_pBoard;

	BOOL m_bSelectFile;

	CString m_strSecondSavePath;
	BOOL m_bTest;
	int m_nFrameRate;
	BOOL m_bAffineFinish;
};
struct FRAME_ENCODING_INFO
{
	FRAME_ENCODING_INFO()
	{
		pArrFrameInfo = NULL;
		pMgr				  = NULL;
		nListIdx			  = 0;
	}
	vector<Mat>*pArrFrameInfo;
	C4DReplayDecodeMgr* pMgr;
	int nListIdx;
};
struct THREAD_STABILIZER_INFO
{
	THREAD_STABILIZER_INFO()
	{
		pMgr = NULL;
		pStable = NULL;
		nTotalCnt = 0;
		bSucess = TRUE;
	}
	C4DReplayDecodeMgr* pMgr;
	C4DReplaySDIStabilizer* pStable;
	BOOL bSucess;
	int nTotalCnt;
};
struct THREAD_PLAY_INFO
{
	THREAD_PLAY_INFO()
	{
		pMgr = NULL;
		nListIdx = 0;
	}
	C4DReplayDecodeMgr* pMgr;
	int nListIdx;
};