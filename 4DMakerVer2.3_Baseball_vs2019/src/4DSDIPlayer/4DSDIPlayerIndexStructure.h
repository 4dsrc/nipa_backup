#pragma once
#include "opencv2\imgproc.hpp"
#include "opencv2\highgui.hpp"
#include "opencv2\core.hpp"
#include "opencv2\opencv.hpp"

#include "opencv2\cudaimgproc.hpp"
#include "opencv2\cudawarping.hpp"
#include "opencv2\cudaarithm.hpp"

#include "ajastuff/common/types.h"
#include "ajastuff/common/options_popt.h"

#include <vector>
#include "ESMDefine.h"

using namespace std;
//using namespace cv;
#define PORT 1015

enum{
	LIST_NUM,
	LIST_FILENAME,
	LIST_FILETIME,
	LIST_FILEPATH,
	LIST_DIVPLAY,
};
enum{
	MOVIE_PLAY,
	MOVIE_PAUSE,
	MOVIE_STOP,
};
enum{
	PLAY_ONCE,
	PLAY_INFINITE,
	PLAY_ALL,
};
enum{
	BIN_PATH,
	CONFIG_PATH,
	IMAGE_PATH,
	SETUP_PATH,
	RAM_PATH,
};
enum{
	MAKING_WAIT,
	MAKING_SUCCESS,
	MAKING_FAIL,
	MAKING_GPU,
	MAKING_STABILIZATION,
	MAKING_FINISH
};
enum{
	METHOD_CPU = 0,
	METHOD_GPU,
};
#define STR_SECTION_SDIDIVFRAME _T("SDIDivFrame")
#define STR_SECTION_STATE		_T("State")
#define STR_SECTION_TIME		_T("Time")
#define STR_SECTION_SENDCNT		_T("SendCount")

//Option info
#define OPT_SECTION									_T("Option")
#define OPT_SECTION_INFO						_T("Info")
#define OPT_SECTION_OUTPUTPATH		_T("Output")
#define OPT_SECTION_LOGPATH				_T("Log")
#define OPT_SECTION_BACKPATH			_T("Back")
#define OPT_SECTION_LOGOPATH			 _T("Logo")
#define OPT_SECTION_IP								 _T("IP")
#define OPT_SECTION_PLAYMETHOD		 _T("Play Method")
#define OPT_SECTION_INSERTLOGO		 _T("Insert Logo")
#define OPT_SECTION_WAITTIME				_T("Wait Time")
#define OPT_SECTION_FRAMEMETHOD	_T("Frame Method")
#define OPT_SECTION_FRAMECOUNT		_T("Frame Count")
#define OPT_SECTION_LOGOSAVE			_T("Logo Save")
#define OPT_SECTION_ENCODE			_T("Encode")
#define OPT_IP_LIST					_T("IPList")
#define OPT_IP_SIZE					_T("Size")
#define OPT_ENCODE_BITRATE				_T("Bitrate")
#define OPT_ENCODE_TS				_T("TransTS")
#define ESM_4DMAKER_SDIPLAYER			_T("SDIPlayer.ini")
#define STR_SECTION_SDIPLAYER			_T("SDIPlayer")
#define STR_STATE				_T("State")
#define STR_FILENAME			_T("FileName")
#define STR_FILEPATH			_T("FilePath")

#define GETREDVALUE(rgb) ((BYTE)rgb);
#define GETGREENVALUE(rgb) ((BYTE)rgb>>8);
#define GETBLUEVALUE(rgb) ((BYTE)rgb>>16);

struct _RCP_ADJUST_INFO
{
	_RCP_ADJUST_INFO()
	{
		AdjAngle = 0.0;
		AdjSize = 0.0;
		nWidth = 0;
		nHeight = 0;
	}
	stFPoint AdjMove, AdjptRotate;
	double AdjAngle, AdjSize;
	int nWidth, nHeight;
	stMarginInfo stMargin;
};
struct stLoadFrame{
	stLoadFrame()
	{
		bFinish = FALSE;
		nFps = 1;
		bStable = FALSE;
		//bStepFrame = FALSE;
		bStabilInclude = FALSE;
		dColorB = 0;
		dColorG = 0;
		dColorR = 0;
		bColorRevision = FALSE;
	}
	BOOL bFinish;
	cv::Mat Logo;
	cv::Mat NoLogo;
	cv::Mat StabFrame;
	cv::Mat roiImage;
	int nFps;
	BOOL bStable;
	BOOL bStabilInclude;
	double dColorB;
	double dColorG;
	double dColorR;
	BOOL bColorRevision;
	//BOOL bStepFrame;
	//vector<cv::Mat>mArrFrame;
};
struct stDivideFrame{
	stDivideFrame()
	{
		nFinish   = -1;
		bMode = FALSE;
		pArrFrameData = NULL;
		nMethod = -1;
		bIsLastFrame = FALSE;
		bStable = FALSE;
		bStabilInclude = FALSE;
	}
	vector<stLoadFrame>*pArrFrameData;
	int nFinish;
	BOOL bMode;//FALSE : nomal, TRUE: dist
	int nMethod;
	BOOL bStable;
	BOOL bIsLastFrame;
	BOOL bStabilInclude;
};
typedef struct _RCPHeader
{
	_RCPHeader() {	Init(); }
	void Init()
	{
		memset(protocol, 0, sizeof(char) * 8);
		command = -1;
		memset(Camera, 0, sizeof(char) * 8);
		param1 = -1;
		param2 = -1;
		param3 = -1;
		bodysize = 0;
	}
	char protocol[8]; //always "RCP1.0"
	DWORD command;
	char Camera[8];
	DWORD param1;
	DWORD param2;
	DWORD param3;
	DWORD bodysize;
} RCPHeader, *PRCPHeader;
typedef struct _RCP_PROPERTY_INFO
{
	_RCP_PROPERTY_INFO()
	{
		memset(szProp, 0, 24);
	}
	DWORD	nProp;
	char	szProp[24];
} RCP_Property_Info, *pRCP_Property_Info;
typedef struct _ESMEVENTMSG {

	_ESMEVENTMSG ()
	{
		message = 0;
		nParam1 = 0;
		nParam2 = 0;
		nParam3 = 0;
		pParam	= NULL;
		pDest	= NULL;
		pParent = NULL;
	}
	UINT	message;
	UINT	nParam1;
	UINT	nParam2;
	UINT	nParam3;
	LPARAM	pParam;
	LPARAM	pDest;	// Multi Managing
	LPARAM	pParent; // 4DMaker DSCItem Pointer
} ESMEvent;
struct AJAOption
{
	AJAOption()
	{
		strInfo		= _T("");
		strOutput	= _T("");
		strLog		= _T("");
		strBack		= _T("");
		strLogo		= _T("");
		strIP				= _T("192.168.0.70");
		nPlayMethod = -1;
		bInsertLogo	= FALSE;
		nWaitTime		= 4000;
		nFrameMethod    = 4;
		dbFrameRate	= 33.3667;
		bEncode		= FALSE;
		nBitrate			= 5;
		bTransTS	= FALSE;
	}
	CString strInfo;
	CString strOutput;
	CString strLog;
	CString strBack;
	CString strLogo;
	CString strIP;
	int nPlayMethod;
	BOOL bInsertLogo;
	int nWaitTime;
	int nFrameMethod;
	double dbFrameRate;
	BOOL bEncode;
	vector<CString>ArrIP;
	int nBitrate;
	BOOL bTransTS;
};
class C4DReplayArray : public CObArray
{
public:
	C4DReplayArray(){InitializeCriticalSection(&_4DReplayArray);}
	~C4DReplayArray(){DeleteCriticalSection(&_4DReplayArray);}

private:
	CRITICAL_SECTION _4DReplayArray;

public:
	int Add(CObject* p)
	{
		int nReturn;
		EnterCriticalSection(&_4DReplayArray);
		nReturn = (int)CObArray::Add(p);
		LeaveCriticalSection(&_4DReplayArray);
		return nReturn;
	}
	int GetSize()
	{
		int nReturn;
		EnterCriticalSection(&_4DReplayArray);
		nReturn = (int)CObArray::GetSize();
		LeaveCriticalSection(&_4DReplayArray);
		return nReturn;
	}
	CObject* GetAt(int nIndex)
	{
		CObject* pReturn = NULL;
		EnterCriticalSection(&_4DReplayArray);
		pReturn = (CObject*)CObArray::GetAt(nIndex);
		LeaveCriticalSection(&_4DReplayArray);
		return pReturn;
	}
	void RemoveAt(int nIndex)
	{
		EnterCriticalSection(&_4DReplayArray);
		CObArray::RemoveAt(nIndex);
		LeaveCriticalSection(&_4DReplayArray);
	}
	void RemoveAll()
	{
		EnterCriticalSection(&_4DReplayArray);
		CObArray::RemoveAll();
		LeaveCriticalSection(&_4DReplayArray);
	}
};