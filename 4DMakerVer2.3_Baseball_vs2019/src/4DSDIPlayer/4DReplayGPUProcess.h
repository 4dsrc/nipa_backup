#pragma once


// C4DReplayGPUProcess 대화 상자입니다.

class C4DReplayGPUProcess : public CDialogEx
{
	DECLARE_DYNAMIC(C4DReplayGPUProcess)

public:
	C4DReplayGPUProcess(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~C4DReplayGPUProcess();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_GPULOAD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	void LaunchModal();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

	BOOL m_bTimeLoadFinish;
	static unsigned WINAPI _LaunchTimeThread(LPVOID param);
	void ChangeTime(int nTime);
};
