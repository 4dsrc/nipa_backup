#pragma once
#include "afxcmn.h"

// C4DReplaySDIPlayerAutoAdd 대화 상자입니다.

class C4DReplaySDIPlayerAutoAdd : public CDialogEx
{
	DECLARE_DYNAMIC(C4DReplaySDIPlayerAutoAdd)

public:
	C4DReplaySDIPlayerAutoAdd(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~C4DReplaySDIPlayerAutoAdd();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_AUTOADD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_cIPList;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedAutoaddAddip();
	void Init(vector<CString>ArrList);
	CIPAddressCtrl m_ipv;
	vector<CString> m_ArrIPList;
	int m_nListSelect;
	afx_msg void OnNMClickAutoaddList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedAutoaddDelete();
};
