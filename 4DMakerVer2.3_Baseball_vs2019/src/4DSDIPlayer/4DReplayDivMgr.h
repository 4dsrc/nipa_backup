#pragma once
#include "4DReplaySDIPlayer.h"
#include "4DReplaySDIPlayerDlg.h"
#include "ESMDefine.h"
#include "ntv2outputtestpattern.h"
#include "4DSDIPlayerIndexStructure.h"
#include "4DReplaySDIPlayerDecodingMgr.h"
#include <omp.h>
using namespace cv;
class C4DReplaySDIPlayerDlg;
class C4DReplayDecodeMgr;

struct FRAME_CPU_INFO
{
	FRAME_CPU_INFO()
	{
		bStabilization = FALSE;
		nIdx = -1;
		pDiv = NULL;
		pParent = NULL;
	}
	BOOL bStabilization;
	int nIdx;
	stDivideFrame* pDiv;
	CString strPah;
	C4DReplayDecodeMgr* pParent;
};

struct FRAME_GPU_INFO
{
	FRAME_GPU_INFO()
	{
		pDiv = NULL;
		pParent = NULL;
	}
	stDivideFrame* pDiv;
	C4DReplayDecodeMgr* pParent;
	AJASendGPUFrameInfo* stInfo;
};

struct FRAME_GPU_DECODE_INFO
{
	FRAME_GPU_DECODE_INFO()
	{
		pDiv = NULL;
		pParent = NULL;
		pArrFrameInfo = NULL;
		nIdx	= -1;
		bStabilObject = FALSE;
	}
	stDivideFrame* pDiv;
	vector<MakeFrameInfo>*pArrFrameInfo;
	C4DReplayDecodeMgr* pParent;
	int nIdx;
	BOOL bStabilObject;
};
class C4DReplaySDIPlayerDivMgr
{
public:
	C4DReplaySDIPlayerDivMgr();
	C4DReplaySDIPlayerDivMgr(C4DReplayDecodeMgr*pParent);
	~C4DReplaySDIPlayerDivMgr();

	C4DReplaySDIPlayerDlg* m_pDlg;
	C4DReplayDecodeMgr* m_pParent;
	void SetParent(C4DReplaySDIPlayerDlg*pDlg,C4DReplayDecodeMgr*pParent);

	void ClearAllAdjust();
	map<CString,stAdjustInfo>m_mpAdjData;
	void SetAdjData(stAdjustInfo* pAdj,CString strDSCID);
	stAdjustInfo GetAdjData(CString strDSCID);

	void StartDivGPUFrame(AJASendGPUFrameInfo* AJAInfo,stDivideFrame* pDiv);
	void StartDivCPUFrame(CString strPath,stDivideFrame* pDiv,int nIdx);
	static unsigned WINAPI _FrameGPUThread(LPVOID param);
	static unsigned WINAPI _FrameCPUThread(LPVOID param);
	CString GetDSCIDFromPath(CString strPath);

	BOOL m_bGPURun;
	vector<FRAME_GPU_INFO*>m_stArrPlayInfo;
	void DoDecodeGPUFrame(FRAME_GPU_INFO* info);
	void GpuMoveImage(cuda::GpuMat* gMat,int nX,int nY);
	void GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle,BOOL bReverse = FALSE);
	void GpuRotateImage(cuda::GpuMat* gMat,Mat rot_mat);
	int Round(double dData);
	//matrix
	map<CString,Mat>m_mpRotMat;
	void SetRotMat(CString strDSCID,stAdjustInfo info);
	Mat GetRotMat(CString strDSCID);

	void StartDivGPUFrame(vector<MakeFrameInfo>*pArrFrameInfo,stDivideFrame* pDiv,int nIdx);
	static unsigned WINAPI _FrameGPUArrThread(LPVOID param);
	void RunAdjustThread(int nIdx,int nWidth,int nHeight,CString strDSCID,vector<MakeFrameInfo>*pArrFrameInfo);
	static unsigned WINAPI _FrameAdjustThread(LPVOID param);

	vector<FRAME_GPU_DECODE_INFO*>m_pArrDecodeInfo;
	void DoDecodeGPUArrFrame(FRAME_GPU_DECODE_INFO* info);
	vector<int> m_nArrIdx;
	BOOL m_bAdjust;
	//촬영 정보
	int	 m_nFPS;
	int	 m_nFrame;

	/*Function*/
	double GetCounter();
	void StartCounter();
	__int64 CounterStart;
	double PCFreq;

	//Color
	void GetColorAvg(Mat frame,FrameRGBInfo* pstavg,FrameRGBInfo info,int nPosX,int nPosY,int nZoom);
	void DoColorRevision(Mat* pFrame,FrameRGBInfo info);
	int	CalcTransValue(int nPixel,int nRefValue);

	//Core 수 조정
	int m_nActiveCoreNum;
	
	BOOL m_nHalfIndex;

	CString m_strStabPrevDSC;
	
	BOOL m_bGPUProcess;
	void SetGPUProcess(BOOL b){m_bGPUProcess = b;}
	BOOL GetGPUProcess(){return m_bGPUProcess;}
	int m_nInsertCnt;
	CRITICAL_SECTION m_criInsert;

	int m_nGPUMakingCnt;
//	void StartMoviePlay(CString strIP);
//	BOOL m_bThreadState;
//	void SetThreadPlay(BOOL b){m_bThreadState = b;}
//	BOOL GetThreadPlay(){return m_bThreadState;}
//	CString m_strIP;
//
//	static unsigned WINAPI _ThreadAutoPlay(LPVOID param);
//	void DoDecode(CString strTime,int nIdx,int nFrameCnt);
//	static unsigned WINAPI _ThreadDecode(LPVOID param);
//	void BGR2YUV422(Mat img);
//	void DoPlay();
//	static unsigned WINAPI _ThreadPlay(LPVOID param);
//	//vector<stAdjustInfo>m_arrAdjust;
//private:
//	int m_nReceiveCnt;
//	vector<stDivideFrame>*m_pArrDecodeFrame;
};
struct FRAME_ADJUST_INFO
{
	FRAME_ADJUST_INFO()
	{
		pArrFrameInfo	= NULL;
		pMgr					= NULL;
		pParent				= NULL;
		nFrameWidth		= 0;
		nFrameHeight	= 0;
		nIdx						= -1;
	}
	vector<MakeFrameInfo>* pArrFrameInfo;
	C4DReplaySDIPlayerDivMgr *pMgr;
	C4DReplayDecodeMgr *pParent;
	CString strDSCID;
	int		  nFrameWidth;
	int		  nFrameHeight;
	int		  nIdx;
};