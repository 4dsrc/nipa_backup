#include "stdafx.h"
#include "4DReplaySDIPlayerDecodingMgr.h"
#include "ESMFileOperation.h"
#include "ESMIni.h"
C4DReplayDecodeMgr::C4DReplayDecodeMgr()
{
	m_pParent = NULL;
	m_pBoard  = NULL;

	m_bDecodeState = FALSE;
	m_bPlayState   = FALSE;
	//m_pArrMovieArray = NULL;

	Mat img = imread("4DBack.png",1);
	if(img.cols != 0)
	{
		m_matBackGround.create(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);
		BGR2YUV422(img,m_matBackGround,G4DGetTargetHeight(),G4DGetTargetWidth());
	}
	else
		G4DShowLog(0,_T("Background Image Error"));

	Mat tmp(G4DGetTargetHeight(),G4DGetTargetWidth(),CV_8UC3);
	tmp = Scalar(0,0,0);
	m_matBlack.create(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);
	BGR2YUV422(tmp,m_matBlack,G4DGetTargetHeight(),G4DGetTargetWidth());

	m_matLogo = imread("Logo.png",IMREAD_UNCHANGED);
	int nChannel = m_matLogo.channels();
	if(m_matLogo.cols == 0 || m_matLogo.channels() != 4)
	{
		G4DShowLog(0,_T("Logo Image Error"));
	}
	m_pDivMgr = new C4DReplaySDIPlayerDivMgr(this);
	m_bDivState = FALSE;
	PCFreq = 0.0;//(0.0),
	CounterStart = 0;//(0)
	m_bEncoding = FALSE;
	m_bDivFrame = FALSE;
	m_dbPlayTime = 33.3667;
	m_bInsertLogo = FALSE;
	m_nWaitTime   = 4000;
	m_nCPUMakingCnt = -1;

	InitializeCriticalSection(&m_criInsertData);
	m_bSelectFile = FALSE;
	m_bStabilization = FALSE;
	m_bTest = FALSE;

	m_nFrameRate = 30;
	m_bAffineFinish = FALSE;
}
C4DReplayDecodeMgr::C4DReplayDecodeMgr(AJAOption* pOption)
{
	m_pParent = NULL;
	m_pBoard  = NULL;

	m_bDecodeState = FALSE;
	m_bPlayState   = FALSE;
	//m_pArrMovieArray = NULL;

	SetBackgroundImage(pOption->strBack);
	SetLogoImage(pOption->strLogo);
	SetSecondPath(pOption->strOutput);

	Mat tmp(G4DGetTargetHeight(),G4DGetTargetWidth(),CV_8UC3);
	tmp = Scalar(0,0,0);
	m_matBlack.create(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);
	BGR2YUV422(tmp,m_matBlack,G4DGetTargetHeight(),G4DGetTargetWidth());
	
	m_pDivMgr = new C4DReplaySDIPlayerDivMgr(this);
	m_bDivState = FALSE;
	PCFreq = 0.0;//(0.0),
	CounterStart = 0;//(0)
	m_bEncoding = FALSE;
	m_bDivFrame = FALSE;
	m_dbPlayTime = 33.3667;
	m_bInsertLogo = FALSE;
	m_nWaitTime   = 4000;
	m_nCPUMakingCnt = -1;
	InitializeCriticalSection(&m_criInsertData);

	m_bSelectFile = FALSE;
	m_bStabilization = FALSE;
	m_bTest = FALSE;

	m_nFrameRate = 30;
	m_bAffineFinish = FALSE;
}
C4DReplayDecodeMgr::~C4DReplayDecodeMgr()
{
	DeleteCriticalSection(&m_criInsertData);
	/*if(m_pArrMovieArray)
	{
		ClearDecodedFile();
	}*/
}
void C4DReplayDecodeMgr::SetBackgroundImage(CString strPath)
{
	CT2CA pszBack(strPath);
	string strBack(pszBack);

	Mat img = imread(strBack,1);
	if(img.cols != 0)
	{
		Mat Re;
		resize(img,Re,cv::Size(G4DGetTargetWidth(),G4DGetTargetHeight()),0,0,CV_INTER_LANCZOS4);

		m_matBackGround.create(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);
		BGR2YUV422(Re,m_matBackGround,G4DGetTargetHeight(),G4DGetTargetWidth());
	}
	else
		G4DShowLog(0,_T("Background Image Error"));
}
void C4DReplayDecodeMgr::SetLogoImage(CString strPath)
{
	CT2CA pszLogo(strPath);
	string strLogo(pszLogo);

	m_matLogo = imread(strLogo,IMREAD_UNCHANGED);
	int nChannel = m_matLogo.channels();
	if(m_matLogo.cols == 0 || m_matLogo.channels() != 4)
	{
		G4DShowLog(0,_T("Logo Image Error"));
	}
}
void C4DReplayDecodeMgr::DoStopAllThread()
{
	if(GetMovieSignal() == MOVIE_PAUSE || GetMovieSignal() == MOVIE_PLAY)
	{
		SetMovieSignal(MOVIE_STOP);
		//G4DShowLog(5,_T("DoStopAllThread"));
		while(1)
		{
			if(GetDecodeState() == FALSE && GetPlayState() == FALSE)
				break;

			Sleep(10);
		}
	}
	TRACE(_T("DoStopAllThread\n"));
} 
void C4DReplayDecodeMgr::ShowBackGroundAsBlack()
{
	if(m_pBoard)
	{
		m_pBoard->EmitPattern(m_matBlack,0,0);
	}
}
void C4DReplayDecodeMgr::ShowBackGroundAsOurs()
{
	if(m_pBoard)
	{
		m_pBoard->EmitPattern(m_matBackGround,0,0);
	}
}
void C4DReplayDecodeMgr::SetAdjustData(stAdjustInfo* stAdj,CString strDSC)
{
	m_pDivMgr->SetAdjData(stAdj,strDSC);
}
stAdjustInfo C4DReplayDecodeMgr::GetAdjustData(CString strDSC)
{
	return m_pDivMgr->GetAdjData(strDSC);
}
void C4DReplayDecodeMgr::SetPlayFileInfo(int nFPS,int nTotalFrame)
{
	//m_pParent
}
void C4DReplayDecodeMgr::BGR2YUV422(Mat frame,Mat YUV422,int nHeight,int nWidth,BOOL bInsertLogo/* =FALSE */)
{
	int i,j;
	int B,G,R;
	int Y,U,V;
	int rIdx,cIdx,yIdx,xIdx;

	int start = GetTickCount();

	int nChannel = m_matLogo.channels();
	for(i=0;i<nHeight;i++)
	{
		rIdx = nWidth*(i*3);
		yIdx = (nWidth*2)*i;
		for(j=0;j<nWidth;j++)
		{
			cIdx = rIdx + (j*3);
			xIdx = yIdx +j*2;

			if(bInsertLogo && nChannel == 4)
			{
#if 1
				int nAlphaValue = m_matLogo.data[nWidth * (i*4) + (j*4) + 3];

				B = frame.data[cIdx];
				G = frame.data[cIdx+1];
				R = frame.data[cIdx+2];

				if(nAlphaValue != 0)
				{
					int nIdx = nWidth * (i*4) + (j*4);

					B = (int)((double)B *(double)(255 -nAlphaValue) / 255.)+
						(int)((double)m_matLogo.data[nIdx] *(double)(nAlphaValue) / 255.);

					G = (int)((double)G *(double)(255 -nAlphaValue) / 255.)+
						(int)((double)m_matLogo.data[nIdx+1] *(double)(nAlphaValue) / 255.);

					R = (int)((double)R *(double)(255 -nAlphaValue) / 255.)+
						(int)((double)m_matLogo.data[nIdx+2] *(double)(nAlphaValue) / 255.);;
				}
#else
				int nBlue = m_mLogo.data[cIdx];
				int nGreen = m_mLogo.data[cIdx+1];
				int nRed = m_mLogo.data[cIdx+2];

				if(m_mLogo.data[cIdx] == 255 && m_mLogo.data[cIdx+1] == 255 && m_mLogo.data[cIdx+2] == 255)
				{
					B = frame.data[cIdx];
					G = frame.data[cIdx+1];
					R = frame.data[cIdx+2];
				}
				else
				{
					B = m_mLogo.data[cIdx];
					G = m_mLogo.data[cIdx+1];
					R = m_mLogo.data[cIdx+2];
				}
#endif
			}
			else
			{
				B = frame.data[cIdx];
				G = frame.data[cIdx+1];
				R = frame.data[cIdx+2];
			}		

			Y =  (0.257*R) + (0.504*G) + (0.098*B) + 16;  //CuttingValue(Y);


			if(j%2 == 0)
			{
				U = -(0.148*R) - (0.291*G) + (0.439*B) + 128; //CuttingValue(U);
				YUV422.data[xIdx] = U;//1
				YUV422.data[xIdx+1] = Y;//0
			}
			else
			{
				V =  (0.439*R) - (0.368*G) - (0.071*B) + 128; //CuttingValue(V);
				YUV422.data[xIdx] = V;//2
				YUV422.data[xIdx+1] = Y;//0
			}
		}
	}
}
/****************************************************************************************/
//Basic Play																												/
/****************************************************************************************/
void C4DReplayDecodeMgr::SelectFileDecode(CString strPath)
{
	m_strMoviePath = strPath;

	SetMovieSignal(MOVIE_PLAY);

	//Load Decode Signal
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,MovieDecodingThread,this,0,NULL);
	CloseHandle(hSyncTime);
}
void C4DReplayDecodeMgr::DoMoviePlay()
{
	HANDLE hSyncTime1 = NULL;
	hSyncTime1 = (HANDLE) _beginthreadex(NULL,0,MoviePlayThread,this,0,NULL);
	CloseHandle(hSyncTime1);
}
unsigned WINAPI C4DReplayDecodeMgr::MovieDecodingThread(LPVOID param)
{
	C4DReplayDecodeMgr* pMgr = (C4DReplayDecodeMgr*) param;
	CString strPath = pMgr->m_strMoviePath;
	CT2CA pszConvertedAnsiString(strPath);
	std::string strLoadPath(pszConvertedAnsiString);

	pMgr->SetDecodeState(TRUE);
	TRACE(_T("DECODING START\n"));
	
	VideoCapture vc(strLoadPath);
	if(!vc.isOpened())
	{	
		pMgr->SetDecodeState(FALSE);
		AfxMessageBox(_T("VideoFile Error"));
		return FALSE;
	}
	int nTotalFrame = (int)vc.get(CV_CAP_PROP_FRAME_COUNT);
	int nFPS = (int)vc.get(CV_CAP_PROP_FPS);

	Mat YUV422(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);

	pMgr->SetPlayFileInfo(nFPS,nTotalFrame);

	pMgr->CreateMovieArray(nTotalFrame);

	pMgr->DoMoviePlay();
	/*if(pMgr->m_pArrMovieArray == NULL)
		pMgr->m_pArrMovieArray = new vector<stLoadFrame>(nTotalFrame);*/

	BOOL bInsertLogo = G4DGetInsertLogo();
	for(int i = 0 ; i < nTotalFrame; i++)
	{
		Mat frame;
		vc>>frame;
		if(frame.empty())
		{
			stLoadFrame stLoad;
			stLoad.Logo = YUV422.clone();
			stLoad.bFinish = -100;
			stLoad.nFps = 1;

			pMgr->InsertMovieData(0,i,stLoad,FALSE);

			continue;
		}
		if(frame.cols != G4DGetTargetWidth())
			resize(frame,frame,cv::Size(G4DGetTargetWidth(),G4DGetTargetHeight()),0,0,CV_INTER_CUBIC);

		pMgr->BGR2YUV422(frame,YUV422,G4DGetTargetHeight(),G4DGetTargetWidth(),bInsertLogo);

		stLoadFrame stLoad;
		stLoad.Logo = YUV422.clone();
		stLoad.bFinish = TRUE;
		stLoad.nFps = 1;

		if(pMgr->GetMovieSignal() == MOVIE_STOP)
		{
			TRACE(_T("DECODE STOP\n"));
			break;
		}
		pMgr->InsertMovieData(0,i,stLoad,FALSE);
		//pMgr->m_pArrMovieArray->at(i) = stLoad;

		Sleep(10);
	}

	TRACE(_T("DECODING FINISH\n"));
	pMgr->SetDecodeState(FALSE);
	
	return TRUE;
}
unsigned WINAPI C4DReplayDecodeMgr::MoviePlayThread(LPVOID param)
{
	Sleep(500);
	C4DReplayDecodeMgr* pMgr = (C4DReplayDecodeMgr*) param;
	pMgr->SetPlayState(TRUE);
	TRACE(_T("PLAY START\n"));

	int nTotalFrame = pMgr->m_pMapMovieArray[0]->pArrFrameData->size();
	BOOL bStop = FALSE;
	for(int i = 0 ; i < nTotalFrame; i++)
	{
		if(pMgr->GetMovieSignal() == MOVIE_STOP)
		{
			TRACE(_T("PLAY STOP[Original]\n"));
			bStop = TRUE;
			break;
		}

		pMgr->DoPlayUsingIndex(i);

		if(pMgr->GetMovieSignal() == MOVIE_STOP)
		{
			TRACE(_T("PLAY STOP[Original]\n"));
			bStop = TRUE;
			break;
		}
	}
	if(pMgr->GetPlayMethod() == PLAY_INFINITE && bStop == FALSE)
	{
		if(G4DGetAutoPlaySize() == 0)
		{
			while(1)
			{
				Sleep(1);
				for(int i = 0 ; i < nTotalFrame; i++)
				{
					if(pMgr->GetMovieSignal() == MOVIE_STOP)
					{
						TRACE(_T("PLAY STOP[Infinite play1]\n"));
						break;
					}

					pMgr->DoPlayUsingIndex(i);

					if(pMgr->GetMovieSignal() == MOVIE_STOP)
					{
						TRACE(_T("PLAY STOP[Infinite play1]\n"));
						break;
					}
				}
				if(G4DGetAutoPlaySize() > 0)
					break;

				if(pMgr->GetMovieSignal() == MOVIE_STOP)
				{
					TRACE(_T("PLAY STOP[Infinite play2]\n"));
					break;
				}
			}
		}
	}
	pMgr->ClearDecodedFile(FALSE);
	pMgr->SetPlayState(FALSE);
	pMgr->ShowBackGroundAsOurs();

	TRACE(_T("PLAY FINISH\n"));
	return TRUE;
}
void C4DReplayDecodeMgr::DoPlayUsingIndex(int i)
{
	//stLoadFrame stLoad = m_pMapMovieArray[0]->pArrFrameData->at(i);
	if(GetMovieSignal() == MOVIE_STOP)
	{
		TRACE(_T("PLAY STOP[Infinite play1]\n"));
		return;
	}

	if(m_pMapMovieArray[0]->pArrFrameData->at(i).bFinish == FALSE)
	{
		while(1)
		{
			Sleep(1);

			if(m_pMapMovieArray[0]->pArrFrameData->at(i).bFinish == TRUE)
				break;

			if(m_pMapMovieArray[0]->pArrFrameData->at(i).bFinish == -100)
				break;

			if(GetMovieSignal() == MOVIE_STOP)
				break;


		}
	}

	if(m_pMapMovieArray[0]->pArrFrameData->at(i).bFinish == -100)
		return;

	if(GetMovieSignal() == MOVIE_STOP)
		return;

	if(GetMovieSignal() == MOVIE_PAUSE)
	{
		while(1)
		{
			if(GetMovieSignal() == MOVIE_STOP)
				break;

			if(GetMovieSignal() == MOVIE_PLAY)
				break;

			Sleep(1);
		}
		if(GetMovieSignal() == MOVIE_STOP)
			return;
	}
	Mat frame = m_pMapMovieArray[0]->pArrFrameData->at(i).Logo.clone();
	m_pBoard->EmitPattern(frame,0,m_dbPlayTime);
}
void C4DReplayDecodeMgr::ClearDecodedFile(BOOL bDiv)
{
	if(bDiv == FALSE)
	{
		stDivideFrame* pDivFrame = m_pMapMovieArray[0];
		if(pDivFrame->pArrFrameData->size())
		{
			pDivFrame->pArrFrameData->clear();
			delete pDivFrame->pArrFrameData;
			pDivFrame->pArrFrameData = NULL;
		}
		if(pDivFrame)
		{
			delete pDivFrame;
			pDivFrame = NULL;
		}
		m_pMapMovieArray.clear();
	}
	else
	{
		for(int i = 0 ; i < m_nTotalMakingCnt; i++)
		{
			stDivideFrame* pDivFrame = m_pMapMovieArray[i];
			if(pDivFrame->pArrFrameData)
			{
				pDivFrame->pArrFrameData->clear();
				delete pDivFrame->pArrFrameData;
				pDivFrame->pArrFrameData = NULL;
			}
			if(pDivFrame)
			{
				delete pDivFrame;
				pDivFrame = NULL;
			}
		}
		m_pMapMovieArray.clear();
	}
	/*m_pArrMovieArray->clear();

	delete m_pArrMovieArray;
	m_pArrMovieArray = NULL;*/
}
void C4DReplayDecodeMgr::CreateMovieArray(int nTotalFrame)
{
	if(m_pMapMovieArray[0])
	{
		m_pMapMovieArray[0]->pArrFrameData->clear();
		delete m_pMapMovieArray[0]->pArrFrameData;
		m_pMapMovieArray[0]->pArrFrameData = NULL;
	}
	m_pMapMovieArray[0] = new stDivideFrame;
	m_pMapMovieArray[0]->pArrFrameData = new vector<stLoadFrame>(nTotalFrame);
}
void C4DReplayDecodeMgr::InsertMovieData(int nIdx,int nFrameIdx,stLoadFrame stLoad,BOOL bDiv)
{
	EnterCriticalSection(&m_criInsertData);
	CString str;
	stDivideFrame*pDivFrame = m_pMapMovieArray[nIdx];

	if(nFrameIdx == -1)
	{
		if(GetMovieSignal() == MOVIE_STOP)
			return;
		if(pDivFrame)
			pDivFrame->pArrFrameData->push_back(stLoad);
	}
	else
	{
		if(GetMovieSignal() == MOVIE_STOP)
			return;
		if(pDivFrame)
			pDivFrame->pArrFrameData->at(nFrameIdx) = stLoad;
	}
	LeaveCriticalSection(&m_criInsertData);
	//aa->pArrFrameData->push_back(stLoad);
	//stDivideFrame* aa = m_pMapMovieArray[nIdx];
	//map<int,stDivideFrame*>::iterator;
	//stDivideFrame* stDiv = (stDivideFrame*)m_pMapMovieArray[nIdx];//->ArrFrameData;
	//stDiv.pArrFrameData->push_back(stLoad);
}
/****************************************************************************************/
//Distribute Play																											/
/****************************************************************************************/
void C4DReplayDecodeMgr::StartDistributePlay(CString strName,int nTotalCnt,int nListIdx)
{
	THREAD_PLAY_INFO* pPlayInfo = new THREAD_PLAY_INFO;
	pPlayInfo->nListIdx = nListIdx;
	pPlayInfo->pMgr	 = this;

	m_nTotalMakingCnt = nTotalCnt;
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,MovieDivPlayThread,(void*)pPlayInfo,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI C4DReplayDecodeMgr::MovieDivPlayThread(LPVOID param)
{
	THREAD_PLAY_INFO* pPlayInfo = (THREAD_PLAY_INFO*)param;
	C4DReplayDecodeMgr* pMgr = pPlayInfo->pMgr;
	int nListIdx = pPlayInfo->nListIdx;
	delete pPlayInfo; pPlayInfo = NULL;

	int nMakingCnt = pMgr->m_nTotalMakingCnt;
	pMgr->SetMovieSignal(MOVIE_PLAY);
	
	pMgr->StartCounter();

	pMgr->SetPlayState(TRUE);

	pMgr->SetPlayDivFrame(TRUE);
	CString strTempLog;
	strTempLog.Format(_T("Wait Time: %d"),pMgr->m_nWaitTime);
	G4DShowLog(1,strTempLog);
	//pMgr->SetDivState(TRUE);
	
	G4DSetSelectFile(TRUE);
	while(1)
	{
		if(pMgr->GetCounter() >pMgr->m_nWaitTime)
			break;
	}
	BOOL bStop = TRUE;
	vector<Mat>*pArrLogo = new vector<Mat>; //Logo
	
	//181019 hjcho
	vector<Mat>*pArrNoLogo = new vector<Mat>; // NoLogo

	for(int i = 0 ; i < nMakingCnt; i++)
	{
		if(pMgr->GetMovieSignal() == MOVIE_STOP)
		{
			bStop = FALSE;
			break;
		}
		if(i != 0)
		{
			if(pMgr->ShowDivMovie(i,pArrLogo,pArrNoLogo,pMgr->GetDivCounter()) == FALSE)
			{
				bStop = FALSE;
				break;
			}
		}
		else
		{
			if(pMgr->ShowDivMovie(i,pArrLogo,pArrNoLogo) == FALSE)
			{
				bStop = FALSE;
				break;
			}
		}

		pMgr->StartDivCounter();

		CString str;
		str.Format(_T("[%d] Play Finish!"),i);
		G4DShowLog(1,str);
//#if 1
//		bStop = pMgr->ShowDivMovie(i);
//		if(bStop == FALSE  || pMgr->GetMovieSignal() == MOVIE_STOP)
//			break;
//
//		CString str;
//		str.Format(_T("[%d] Play Finish!"),i);
//		G4DShowLog(5,str);
//		//int nCnt = pMgr->m_pMapMovieArray[i]->pArrFrameData->size();
//		//vector<stLoadFrame>*pArrLoadFrame = pMgr->m_pMapMovieArray[i]->pArrFrameData;
//#else
//		while(1)
//		{
//			Sleep(1);
//			if(pMgr->m_pMapMovieArray[i]->nFinish == MAKING_FINISH)
//				break;
//		}
//		
//		if(pMgr->m_pMapMovieArray[i]->nFinish == MAKING_FINISH)
//		{
//			int nCnt = pMgr->m_pMapMovieArray[i]->pArrFrameData->size();
//			vector<stLoadFrame>*pArrLoadFrame = pMgr->m_pMapMovieArray[i]->pArrFrameData;
//			for(int n = 0 ; n < nCnt; n++)
//			{
//				Mat frame = pArrLoadFrame->at(n).frame.clone();
//				/*imshow("frame",frame);
//				waitKey(0);*/
//				pMgr->m_pBoard->EmitPattern(frame,0,30);
//			}
//		}
//#endif
	}
	pMgr->SetDivState(FALSE);
	pMgr->SetPlayDivFrame(FALSE);

	pMgr->ClearDecodedFile(TRUE);
	//pMgr->ShowBackGroundAsOurs();

	//Replay & Encoding
	if(bStop == TRUE)
	{
		//Run Encoding Thread
		if(G4DGetLogoSave() == FALSE) // NoLogoSave
		{
			if(pMgr->EncodingMovie(pArrNoLogo,nListIdx) == FALSE)
			{
				G4DShowLog(0,_T("Encoding Error!!!!!!"));
			}
		}
		else//LogoSave
		{
			if(pMgr->EncodingMovie(pArrLogo,nListIdx) == FALSE)
			{
				G4DShowLog(0,_T("Encoding Error!!!!!!"));
			}
		}

		//if(G4DGetLogoSave() == FALSE)
		//	pMgr->EncodingMovie(pArrReplay,nListIdx);
		//else
		//	pMgr->EncodingMovie(pArrLogoMovie,nListIdx);

		if(pMgr->GetPlayMethod() == PLAY_INFINITE)
		{
			if(G4DGetAutoPlaySize() == 0)
			{
				while(1)
				{
					if(G4DGetInsertLogo())
						pMgr->ShowDivFrame(pArrLogo);
					else
						pMgr->ShowDivFrame(pArrNoLogo);

					//pMgr->ShowDivFrame(pArrLogo);
					if(pMgr->GetMovieSignal() == MOVIE_STOP)
						break;

					if(G4DGetAutoPlaySize() > 0)
						break;
				}
			}
		}
	}

	if(pArrLogo)
	{
		pArrLogo->clear();
		delete pArrLogo;
		pArrLogo = NULL;
	}
	if(pArrNoLogo)
	{
		pArrNoLogo->clear();
		delete pArrNoLogo;
		pArrNoLogo = NULL;
	}
	/*if(bStop == TRUE)
	{
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = AJA_MAKING_FINISH;
		G4DAddEvent(pMsg);
		pMgr->EncodingMovie();
	}

	pMgr->SetPlayDivFrame(FALSE);

	if(pMgr->GetPlayMethod() == PLAY_INFINITE && bStop == TRUE)
	{
		while(1)
		{
			Sleep(1);

			for(int i = 0 ; i < nMakingCnt; i++)
			{
				bStop = pMgr->ShowDivMovie(i);
				if(pMgr->GetMovieSignal() == MOVIE_STOP)
					break;
			}

			if(pMgr->GetMovieSignal() == MOVIE_STOP)
			{
				break;
			}
		}
	}*/
	//pMgr->SetPlayDivFrame(FALSE);
	pMgr->SetPlayState(FALSE);
	pMgr->ShowBackGroundAsOurs();
	G4DShowLog(5,_T("Play Finish!"));
	return TRUE;
}
BOOL C4DReplayDecodeMgr::ShowDivFrame(vector<Mat>*pArrFrame)
{
	int nCnt = pArrFrame->size();
	for(int i = 0 ; i < nCnt ; i ++)
	{
		StartCounter();
		if(GetMovieSignal() == MOVIE_PAUSE)
		{
			while(1)
			{
				Sleep(1);
				if(GetMovieSignal() == MOVIE_STOP)
					break;

				if(GetMovieSignal() == MOVIE_PLAY)
					break;
			}
		}
		if(GetMovieSignal() == MOVIE_STOP)
			break;
		
		Mat frame = pArrFrame->at(i).clone();
		m_pBoard->EmitPattern(frame,GetCounter(),m_dbPlayTime);
	}
	if(GetMovieSignal() == MOVIE_STOP)
		return FALSE;
	else
		return TRUE;
}
BOOL C4DReplayDecodeMgr::ShowDivMovie(int nIdx,vector<Mat>* ArrLogo,vector<Mat>*ArrNoLogo,double dbDelay /*= 0.0*/)
{
	StartCounter();
	stDivideFrame* pDivFrame = m_pMapMovieArray[nIdx];

	if(pDivFrame->nFinish == MAKING_GPU)
	{
		int nCnt = pDivFrame->pArrFrameData->size();
		vector<stLoadFrame>*pArrLoadFrame = pDivFrame->pArrFrameData;
		
		for(int i = 0 ; i < pArrLoadFrame->size() ; i++)
		{
			if(G4DGetStabilization())
			{
				if(pDivFrame->pArrFrameData->at(i).bFinish == TRUE || pDivFrame->pArrFrameData->at(i).bFinish == FALSE)
				{
					while(1)
					{
						if(pDivFrame->pArrFrameData->at(i).bFinish == 100)
							break;

						if(pDivFrame->pArrFrameData->at(i).bFinish == -200)
							break;

						if(GetMovieSignal() == MOVIE_STOP)
							break;

						Sleep(1);
					}
				}
				/*CString strLog;
				strLog.Format(_T("[%d][%d] Play Finish"),nIdx,i);
				G4DShowLog(5,strLog);*/
			}
			else
			{
				if(pDivFrame->pArrFrameData->at(i).bFinish == FALSE)
				{
					while(1)
					{
						if(pDivFrame->pArrFrameData->at(i).bFinish == TRUE)
							break;

						if(pDivFrame->pArrFrameData->at(i).bFinish == -100)
							break;

						if(GetMovieSignal() == MOVIE_STOP)
							break;

						Sleep(1);
					}
				}
			}

			if(GetMovieSignal() == MOVIE_STOP)
				return FALSE;

			if(pDivFrame->pArrFrameData->at(i).bFinish < 0 )
				continue;

			BOOL bInsertLogo = G4DGetInsertLogo();
			Mat YUV422;// = pDivFrame->pArrFrameData->at(i).frame.clone();
			YUV422 = bInsertLogo == TRUE ? 
				pDivFrame->pArrFrameData->at(i).Logo.clone() : pDivFrame->pArrFrameData->at(i).NoLogo.clone();
			int nCnt  = pDivFrame->pArrFrameData->at(i).nFps;

			if(nIdx == 0)
			{
				if(i == 0)
				{
					G4DSetSelectFile(FALSE);
					G4DShowLog(1,_T("Play Start!!!!"));
					ESMEvent* pMsg = new ESMEvent;
					pMsg->message = AJA_MAKING_START;
					G4DAddEvent(pMsg);
				}
			}

			for(int j = 0 ;j < nCnt; j++)
			{
				if(i!= 0 && j == 0)
					m_pBoard->EmitPattern(YUV422,GetCounter()+dbDelay,m_dbPlayTime);
				else
					m_pBoard->EmitPattern(YUV422,GetCounter(),m_dbPlayTime);

				ArrLogo->push_back(pDivFrame->pArrFrameData->at(i).Logo.clone());
				ArrNoLogo->push_back(pDivFrame->pArrFrameData->at(i).NoLogo.clone());//No Logo

				//if(bLogoSave)
				//{
				//	if(i!= 0 && j == 0)
				//		m_pBoard->EmitPattern(YUV422,GetCounter()+dbDelay,m_dbPlayTime);
				//	else
				//		m_pBoard->EmitPattern(YUV422,GetCounter(),m_dbPlayTime);

				//	pArrLogoFrame->push_back(YUVLogo);
				//	pArrFrame->push_back(YUV422);//No Logo
				//}
				//else
				//{
				//	if(i!= 0 && j == 0)
				//		m_pBoard->EmitPattern(YUV422,GetCounter()+dbDelay,m_dbPlayTime);
				//	else
				//		m_pBoard->EmitPattern(YUV422,GetCounter(),m_dbPlayTime);

				//	//pArrLogoFrame->push_back(YUVLogo);
				//	pArrFrame->push_back(YUV422);//No Logo
				//}
				StartCounter();
				Sleep(1);
			}
		}
	}
	else
	{
		//StartCounter();
		if(m_pMapMovieArray[nIdx]->nFinish != MAKING_FINISH)
		{
			while(1)
			{
				Sleep(1);
				if(m_pMapMovieArray[nIdx]->nFinish == MAKING_FINISH)
					break;

				if(m_pMapMovieArray[nIdx]->nFinish == MAKING_FAIL)
					break;

				if(GetMovieSignal() == MOVIE_STOP)
					break;
			}
		}
		if(m_pMapMovieArray[nIdx]->nFinish == MAKING_FAIL)
			return TRUE;

		for(int i = 0 ; i < m_pMapMovieArray[nIdx]->pArrFrameData->size(); i++)
		{
			Mat frame;
			frame = G4DGetInsertLogo() ? pDivFrame->pArrFrameData->at(i).Logo : pDivFrame->pArrFrameData->at(i).NoLogo;

			if(i != 0)
			{
				//Mat frame = pDivFrame->pArrFrameData->at(i).frame;//.clone();
				m_pBoard->EmitPattern(frame,GetCounter(),m_dbPlayTime);
				StartCounter();
				ArrLogo->push_back(pDivFrame->pArrFrameData->at(i).Logo.clone());
				ArrNoLogo->push_back(pDivFrame->pArrFrameData->at(i).NoLogo.clone());
			}
			else
			{
				//Mat frame = pDivFrame->pArrFrameData->at(i).frame;//.clone();
				m_pBoard->EmitPattern(frame,GetCounter()+dbDelay,m_dbPlayTime);
				StartCounter();
				ArrLogo->push_back(pDivFrame->pArrFrameData->at(i).Logo.clone());
				ArrNoLogo->push_back(pDivFrame->pArrFrameData->at(i).NoLogo.clone());
			}

			if(nIdx == 0 && i == 0)
			{
				G4DSetSelectFile(FALSE);
				G4DShowLog(1,_T("Play Start!!!!"));
				ESMEvent* pMsg = new ESMEvent;
				pMsg->message = AJA_MAKING_START;
				G4DAddEvent(pMsg);
			}

			if(GetMovieSignal() == MOVIE_STOP)
				break;
		}
		if(GetMovieSignal() == MOVIE_STOP)
			return FALSE;
	}

	return TRUE;
}
BOOL C4DReplayDecodeMgr::ShowDivMovie(int nIdx)
{
	BOOL bStop = TRUE;

	if(m_pMapMovieArray[nIdx]->nFinish == MAKING_GPU)
	{
		int nCnt = m_pMapMovieArray[nIdx]->pArrFrameData->size();
		vector<stLoadFrame>*pArrLoadFrame = m_pMapMovieArray[nIdx]->pArrFrameData;

		for(int n = 0 ; n < nCnt ; n++)
		{
			/*while(1)
			{
				Sleep(1);
				if(m_pMapMovieArray[nIdx]->pArrFrameData->at(n).bFinish == TRUE)
					break;

			}*/
			bStop = DoDivPlayUsingIndex(nIdx,n);
			if(bStop == FALSE)
				break;
		}
	}
	else
	{
		if(m_pMapMovieArray[nIdx]->nFinish != MAKING_FINISH)
		{
			while(1)
			{
				if(m_pMapMovieArray[nIdx]->nFinish == MAKING_FINISH)
					break;

				if(m_pMapMovieArray[nIdx]->nFinish == MAKING_FAIL)
					break;

				if(GetMovieSignal() == MOVIE_STOP)
					break;

				Sleep(1);
			}
		}
		
		if(m_pMapMovieArray[nIdx]->nFinish == MAKING_FAIL)
		{
			CString str;
			str.Format(_T("%d cannot making"),nIdx);
			G4DShowLog(5,str);

			return TRUE;
		}
		if(GetMovieSignal() == MOVIE_STOP)
			return FALSE;
		//int nCnt = m_pMapMovieArray[nIdx]->pArrFrameData->size();

		for(int n = 0 ; n < m_pMapMovieArray[nIdx]->pArrFrameData->size() ; n++)
		{
			bStop = DoDivPlayUsingIndex(nIdx,n);
		
			if(bStop == FALSE)
				break;
		}
	}
	return bStop;
}
BOOL C4DReplayDecodeMgr::DoDivPlayUsingIndex(int nIdx,int nFrameIdx)//Do Not Use
{
	/*StartCounter();
	if(m_pMapMovieArray[nIdx]->pArrFrameData->at(nFrameIdx).bFinish == FALSE)
	{
		while(1)
		{
			Sleep(1);

			if(m_pMapMovieArray[nIdx]->pArrFrameData->at(nFrameIdx).bFinish == TRUE)
				break;
		}
	}
	if(nIdx == 0 && nFrameIdx == 0)
		G4DShowLog(5,_T("Play First Check Point"));

	if(GetMovieSignal() == MOVIE_PAUSE)
	{
		while(1)
		{
			Sleep(1);
			
			if(GetMovieSignal() == MOVIE_STOP)
				break;
			
			if(GetMovieSignal() == MOVIE_PLAY)
				break;
		}
		if(GetMovieSignal() == MOVIE_STOP)
			return FALSE;
	}
	Mat frame = m_pMapMovieArray[nIdx]->pArrFrameData->at(nFrameIdx).frame;
	int nCnt  = m_pMapMovieArray[nIdx]->pArrFrameData->at(nFrameIdx).nFps;
	
#if 0
	m_pBoard->EmitPattern(frame,GetCounter(),33.3667 * nCnt);
#else
	for(int i = 0 ; i < nCnt; i++)
	{
		if(i >= 1)
		{
			StartCounter();
			m_pBoard->EmitPattern(frame,GetCounter(),m_dbPlayTime);
		}
		else
			m_pBoard->EmitPattern(frame,GetCounter(),m_dbPlayTime);

	}
#endif*/
	return TRUE;
	
}
void C4DReplayDecodeMgr::DoWaitDivData(int nIdx,BOOL bMode /*= FALSE*/,int nFrameCnt/* = 0*/)
{
	CString csLog;
	stDivideFrame* stDivFrame = new stDivideFrame;
	
	if(bMode)
	{
		stDivFrame->nFinish		  = MAKING_WAIT;
		stDivFrame->pArrFrameData = new vector<stLoadFrame>;
		//csLog.Format(_T("[%d] CPU Input"),nIdx);

		//m_pMapMovieArray[nIdx] = stDivFrame;
	}
	else
	{
		stDivFrame->nFinish		  = MAKING_GPU;
		stDivFrame->pArrFrameData = new vector<stLoadFrame>(nFrameCnt);
		//csLog.Format(_T("[%d] GPU Input(%d)"),nIdx,nFrameCnt);
	}
	//m_pMapMovieArray.insert(std::pair<int,stDivideFrame*>(0,&stDivFrame));
	if(G4DGetStabilization())
		stDivFrame->bStable = FALSE;
	m_pMapMovieArray[nIdx] = stDivFrame;
	
	//G4DShowLog(3,csLog);
}
void C4DReplayDecodeMgr::DoCPUDecoding(CString strPath,int nIdx,int nFinish)
{
	//if(GetDecodeState() == TRUE ||	GetPlayState() == TRUE)
	//{
	//	if(GetPlayDivFrame() == FALSE)
	//	{
	//		while(1)
	//		{
	//			Sleep(1);
	//			if(GetDecodeState() == FALSE && GetPlayState() == FALSE)
	//				break;
	//		}
	//	}
	//}
	int nState;
	if(nFinish == TRUE)
		nState = MAKING_SUCCESS;
	else
		nState = MAKING_FAIL;

	stDivideFrame* pDivFrame = m_pMapMovieArray[nIdx];
	if(pDivFrame)
	{
		pDivFrame->nFinish		 = nState;
		pDivFrame->nMethod		 = METHOD_CPU;
		if(nState == MAKING_SUCCESS)
		{
			//Decoding Start
			m_pDivMgr->StartDivCPUFrame(strPath,pDivFrame,nIdx);
		}
	}
}
void C4DReplayDecodeMgr::DoGPUDecoding(vector<MakeFrameInfo>*pArrFrameInfo,int nMakingIdx)
{
	//if(GetDecodeState() == TRUE ||	GetPlayState() == TRUE)
	//{
	//	if(GetPlayDivFrame() == FALSE)
	//	{
	//		while(1)
	//		{
	//			Sleep(1);
	//			if(GetDecodeState() == FALSE && GetPlayState() == FALSE)
	//				break;
	//		}
	//	}
	//}

	int nIdx = nMakingIdx;

	int nMakingCnt = pArrFrameInfo->size();//stAJAInfo->nEndIndex - stAJAInfo->nStartIndex + 1;

	DoWaitDivData(nIdx,FALSE,nMakingCnt);

	stDivideFrame* pDivFrame = m_pMapMovieArray[nIdx];
	pDivFrame->nMethod		 = METHOD_GPU;
	m_pDivMgr->StartDivGPUFrame(pArrFrameInfo,pDivFrame,nIdx);
}
void C4DReplayDecodeMgr::DoInsertFrameData(int nIdx,stLoadFrame stLoad)
{
	stDivideFrame* pDivFrame = m_pMapMovieArray[nIdx];

	pDivFrame->pArrFrameData->push_back(stLoad);
}
void C4DReplayDecodeMgr::SetPlayDivFrame(BOOL b)
{
	m_bDivFrame = b;
	//TRUE: making start;
	//FALSE: making finish
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message  = AJA_MAKING_FINISH; 
	pMsg->nParam1  = b;
	G4DAddEvent(pMsg);
}
BOOL C4DReplayDecodeMgr::GetPlayDivFrame()
{
	return m_bDivFrame;
}
/****************************************************************************************/
//Stabilizaion Play																											/
/****************************************************************************************/
void C4DReplayDecodeMgr::StartStabilizationPlay(CString strName,int nTotalCnt,int nCPUMakingCnt,int nListIdx)
{
	//G4DShowLog(5,_T("Stablization Movie Mode..!"));
	THREAD_PLAY_INFO* pPlayInfo = new THREAD_PLAY_INFO;
	pPlayInfo->pMgr = this;
	pPlayInfo->nListIdx = nListIdx;

	m_nTotalMakingCnt = nTotalCnt;
	m_nCPUMakingCnt	  = nCPUMakingCnt;
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,MovieStablizationThread,(void*)pPlayInfo,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI C4DReplayDecodeMgr::MovieStablizationThread(LPVOID param)
{
	THREAD_PLAY_INFO* pPlayInfo = (THREAD_PLAY_INFO*)param;
	C4DReplayDecodeMgr* pMgr = pPlayInfo->pMgr;
	int nListIdx = pPlayInfo->nListIdx;
	delete pPlayInfo; pPlayInfo = NULL;

	int nCPUMakingCnt = pMgr->m_nCPUMakingCnt;
	int nTotalMakingCnt = pMgr->m_nTotalMakingCnt;

	pMgr->SetMovieSignal(MOVIE_PLAY);
	pMgr->SetPlayState(TRUE);
	pMgr->SetPlayDivFrame(TRUE);
	G4DSetSelectFile(TRUE);
	BOOL bStabilizer = FALSE;
	while(1)
	{
		int nCnt = 0;
		for(int i = 0 ; i < nTotalMakingCnt; i++)
		{
			EnterCriticalSection(&pMgr->m_criInsertData);
			stDivideFrame* pDivFrame = pMgr->m_pMapMovieArray[i];
			if(pDivFrame->nMethod == METHOD_CPU)
			{
				if(pDivFrame->nFinish == MAKING_STABILIZATION || pDivFrame->nFinish == MAKING_FAIL)
					nCnt++;
			}
			if(pMgr->GetMovieSignal() == MOVIE_STOP)
			{
				LeaveCriticalSection(&pMgr->m_criInsertData);
				break;
			}
			LeaveCriticalSection(&pMgr->m_criInsertData);
		}
		if(pMgr->GetMovieSignal() == MOVIE_STOP)
			break;

		if(nCnt == nCPUMakingCnt)
		{
			if(nCPUMakingCnt == 0)
				bStabilizer = FALSE;
			else
				bStabilizer = TRUE;

			break;
		}

		Sleep(1);
	}
	/*if(pMgr->GetMovieSignal() == MOVIE_STOP)
		return FALSE;*/
	//Launch Stabilization

	BOOL bStableSucess = FALSE;
	C4DReplaySDIStabilizer* pStabilizer = new C4DReplaySDIStabilizer(TRUE);
	if(bStabilizer && pMgr->GetMovieSignal() != MOVIE_STOP)
	{
		G4DShowLog(5,_T("Stablization Search Start..!"));
		vector<stDivideFrame*> *pArrDivFrameInfo = new vector<stDivideFrame*>;
		int nStabilCnt = 0;
		for(int i = 0 ; i < nTotalMakingCnt; i++)
		{
			stDivideFrame* pDivFrame = pMgr->m_pMapMovieArray[i];

			if(pMgr->GetMovieSignal() == MOVIE_STOP)
				break;

			if(pDivFrame->nMethod == METHOD_GPU)
			{
				if(pDivFrame->bStabilInclude == TRUE)
				{
					int nSize = pDivFrame->pArrFrameData->size();
					int nFirst = pDivFrame->pArrFrameData->at(0).bStabilInclude;
					int nLast = pDivFrame->pArrFrameData->at(nSize-1).bStabilInclude;

					int nArrIdx;
					if(nFirst == TRUE)
						nArrIdx = 0;
					else
						nArrIdx = nSize - 1;

					if(pDivFrame->pArrFrameData->at(nArrIdx).bFinish == FALSE)
					{
						while(1)
						{
							if(pDivFrame->pArrFrameData->at(nArrIdx).bFinish == TRUE)
								break;

							if(pDivFrame->pArrFrameData->at(nArrIdx).bFinish == -100)
								continue;

							if(pMgr->GetMovieSignal() == MOVIE_STOP)
								break;

							Sleep(1);
						}
					}
					if(pMgr->GetMovieSignal() == MOVIE_STOP)
						break;
					Mat frame = pDivFrame->pArrFrameData->at(nArrIdx).StabFrame.clone();
					//Stabilizer 회전 구간 관심영역
					Mat roiImage;
					Rect2d rect = Rect2d(frame.cols/4*1,frame.rows/3*1,frame.cols/4*2,frame.rows/3*1);
					frame(rect).copyTo(roiImage);

					blur(roiImage, roiImage, cv::Size(41, 41));

					pStabilizer->_imageSetForStabilization->inputImageForGeneratingTransformationMatrix(roiImage);
					pStabilizer->_imageSetForStabilization->inputImageForApplyingTransformationMatrix(frame);

					pArrDivFrameInfo->push_back(pDivFrame);
					nStabilCnt++;
				}
			}
			else if(pDivFrame->nMethod == METHOD_CPU)
			{
				if(pDivFrame->nFinish == MAKING_STABILIZATION)
				{
					pDivFrame->nFinish = MAKING_WAIT;

					for(int j = 0 ; j < pDivFrame->pArrFrameData->size() ; j++)
					{
						Mat frame = pDivFrame->pArrFrameData->at(j).StabFrame.clone();
						Mat roiImage = pDivFrame->pArrFrameData->at(j).roiImage.clone();

						pStabilizer->_imageSetForStabilization->inputImageForGeneratingTransformationMatrix(roiImage);
						pStabilizer->_imageSetForStabilization->inputImageForApplyingTransformationMatrix(frame);
						nStabilCnt++;
					}
					pArrDivFrameInfo->push_back(pDivFrame);
				}
			}
		}

		if(pMgr->GetMovieSignal() != MOVIE_STOP)
		{
			pStabilizer->CreateStabilizerMovie(pArrDivFrameInfo,nStabilCnt);
			Mat matFirstFrame = pStabilizer->GetFirstMovieRotMat().clone();
			Mat matLastFrame = pStabilizer->GetLastMovieRotMat().clone();

			if(matFirstFrame.empty() || matLastFrame.empty())
				bStableSucess = FALSE;
			else
				bStableSucess = TRUE;
		}
		G4DShowLog(5,_T("Stablization Search Finish..!"));
	}

	//Adapt All GPU Frame
	if(pMgr->GetMovieSignal() != MOVIE_STOP)
	{
		pMgr->LaunchAffineThread(pStabilizer,nTotalMakingCnt,bStableSucess);

		//Play!
		int nWaitTime = pMgr->m_nWaitTime / 2;
		CString strTempLog;
		strTempLog.Format(_T("Wait Time: %d"),nWaitTime);
		G4DShowLog(1,strTempLog);
		//pMgr->SetDivState(TRUE);
		pMgr->StartCounter();
		while(1)
		{
			if(pMgr->GetCounter() >nWaitTime)
				break;
		}
	}

	BOOL bStop = TRUE;
	vector<Mat>*pArrLogo = new vector<Mat>;
	//181019 hjcho
	vector<Mat>*pArrNoLogo = new vector<Mat>;

	//if(G4DGetInsertLogo() == TRUE && G4DGetLogoSave() == FALSE)
	//{
	//	pArrLogoMovie = new vector<Mat>;
	//}
	//else
	//	G4DShowLog(5,_T("LogoSave Start"));

	for(int i = 0 ; i < nTotalMakingCnt; i++)
	{
		if(pMgr->GetMovieSignal() == MOVIE_STOP)
		{
			bStop = FALSE;
			break;
		}
		if(i != 0)
		{
			if(pMgr->ShowDivMovie(i,pArrLogo,pArrNoLogo,pMgr->GetDivCounter()) == FALSE)
			{
				bStop = FALSE;
				break;
			}
		}
		else
		{
			if(pMgr->ShowDivMovie(i,pArrLogo,pArrNoLogo) == FALSE)
			{
				bStop = FALSE;
				break;
			}
		}

		pMgr->StartDivCounter();

		CString str;
		str.Format(_T("[%d] Play Finish!"),i);
		G4DShowLog(1,str);
	}
	pMgr->SetDivState(FALSE);
	pMgr->SetPlayDivFrame(FALSE);
	if(pMgr->GetAffineFinish() == TRUE)
	{
		while(1)
		{
			if(pMgr->GetAffineFinish() == FALSE)
				break;

			Sleep(1);
		}
	}
	if(bStop == TRUE)
	{
		//Run Encoding Thread
		if(G4DGetLogoSave() == FALSE) // NoLogoSave
		{
			if(pMgr->EncodingMovie(pArrNoLogo,nListIdx) == FALSE)
			{
				G4DShowLog(0,_T("Encoding Error!!!!!!"));
			}
		}
		else//LogoSave
		{
			if(pMgr->EncodingMovie(pArrLogo,nListIdx)==FALSE)
			{
				G4DShowLog(0,_T("Encoding Error!!!!!!"));
			}
		}

		if(pMgr->GetPlayMethod() == PLAY_INFINITE)
		{
			if(G4DGetAutoPlaySize() == 0)
			{
				while(1)
				{
					if(G4DGetInsertLogo())
						pMgr->ShowDivFrame(pArrLogo);
					else
						pMgr->ShowDivFrame(pArrNoLogo);

					//pMgr->ShowDivFrame(pArrLogo);
					if(pMgr->GetMovieSignal() == MOVIE_STOP)
						break;

					if(G4DGetAutoPlaySize() > 0)
						break;
				}
			}
		}
	}

	pMgr->ClearDecodedFile(TRUE);

	if(pArrLogo)
	{
		pArrLogo->clear();
		delete pArrLogo;
		pArrLogo = NULL;
	}
	if(pArrNoLogo)
	{
		pArrNoLogo->clear();
		delete pArrNoLogo;
		pArrNoLogo = NULL;
	}
//#if 1
//	//Replay & Encoding
//	if(bStop == TRUE)
//	{
//		//Run Encoding Thread
//		if(G4DGetLogoSave() == FALSE) // NoLogoSave
//			pMgr->EncodingMovie(pArrReplay,nListIdx);
//		else//LogoSave
//			pMgr->EncodingMovie(pArrLogoMovie,nListIdx);
//
//		if(pMgr->GetPlayMethod() == PLAY_INFINITE)
//		{
//			while(1)
//			{
//				pMgr->ShowDivFrame(pArrReplay);
//				if(pMgr->GetMovieSignal() == MOVIE_STOP)
//					break;
//			}
//		}
//	}
//#else
//	if(bStop == TRUE)
//	{
//		//Run Encoding Thread
//		if(G4DGetLogoSave())
//			pMgr->EncodingMovie(pArrReplay,nListIdx);
//		else
//			pMgr->EncodingMovie(pArrLogoMovie,nListIdx);
//
//		if(pMgr->GetPlayMethod() == PLAY_INFINITE)
//		{
//			while(1)
//			{
//				int nSize = pArrReplay->size();
//				pMgr->ShowDivFrame(pArrReplay);
//				if(pMgr->GetMovieSignal() == MOVIE_STOP)
//					break;
//			}
//		}
//	}
//#endif
//	if(pArrReplay)
//	{
//		pArrReplay->clear();
//		delete pArrReplay;
//		pArrReplay = NULL;
//	}
//	if(pArrLogoMovie)
//	{
//		pArrLogoMovie->clear();
//		delete pArrLogoMovie;
//		pArrLogoMovie = NULL;
//	}
	pMgr->SetPlayState(FALSE);
	pMgr->ShowBackGroundAsOurs();
	G4DShowLog(5,_T("Stablization Play Finish!"));

	return TRUE;
}
void C4DReplayDecodeMgr::LaunchAffineThread(C4DReplaySDIStabilizer* pStable,int nTotalCnt,BOOL bSucess)
{
	THREAD_STABILIZER_INFO* pStabInfo = new THREAD_STABILIZER_INFO;
	pStabInfo->pMgr = this;
	pStabInfo->pStable = pStable;
	pStabInfo->nTotalCnt = nTotalCnt;
	pStabInfo->bSucess = bSucess;
	HANDLE hSyncTime;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,MovieAdaptAffine,(void*)pStabInfo,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI C4DReplayDecodeMgr::MovieAdaptAffine(LPVOID param)
{
	THREAD_STABILIZER_INFO* pStabInfo = (THREAD_STABILIZER_INFO*)param;
	C4DReplayDecodeMgr* pMgr = pStabInfo->pMgr;
	C4DReplaySDIStabilizer* pStable = pStabInfo->pStable;
	int nTotalCnt = pStabInfo->nTotalCnt;
	BOOL bSucess = pStabInfo->bSucess;
	delete pStabInfo;pStabInfo = NULL;
	
	pMgr->SetAffineFinish(TRUE);

	BOOL bInsertLogo = G4DGetInsertLogo();
	//Mat YUV422(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);
	Mat Logo(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);
	Mat NoLogo(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);
	CString strLog;
	strLog.Format(_T("Affine Start"));
	G4DShowLog(5,strLog);

	for(int i = 0 ; i < nTotalCnt ; i++)
	{
		BOOL bStop = FALSE;

		EnterCriticalSection(&pMgr->m_criInsertData);
		stDivideFrame* pDivFrame = pMgr->m_pMapMovieArray[i];
		LeaveCriticalSection(&pMgr->m_criInsertData);
		
		if(pMgr->GetMovieSignal() == MOVIE_STOP)
			break;
		
		if(pDivFrame->nMethod == METHOD_GPU)
		{
			vector<stLoadFrame>*pArrLoadFrame = pDivFrame->pArrFrameData;
			BOOL bFirst = pDivFrame->bIsLastFrame;
			
			for(int j = 0 ; j < pArrLoadFrame->size(); j++)
			{
				if(pDivFrame->pArrFrameData->at(j).bFinish == FALSE)
				{
					while(1)
					{
						if(pDivFrame->pArrFrameData->at(j).bFinish == TRUE)
							break;

						if(pDivFrame->pArrFrameData->at(j).bFinish == -100)
						{
							bStop = TRUE;
							break;
						}
						if(pMgr->GetMovieSignal() == MOVIE_STOP)
						{
							bStop = TRUE;
							break;
						}
					}
				}
				BOOL bColorRevision = pDivFrame->pArrFrameData->at(j).bColorRevision;
				
				EnterCriticalSection(&pMgr->m_criInsertData);
				stLoadFrame* stLoad = &pDivFrame->pArrFrameData->at(j);
				LeaveCriticalSection(&pMgr->m_criInsertData);

				if(bStop == FALSE && bSucess == TRUE)
				{
					if(pMgr->GetMovieSignal() == MOVIE_STOP)
						break;

					Mat frame = stLoad->StabFrame.clone();
					if(frame.empty())
					{
						pDivFrame->pArrFrameData->at(j).bFinish == -100;
						continue;
					}
					if(bFirst == FALSE)
					{
						Mat rotMat = pStable->GetFirstMovieRotMat().clone();
						cv::Size sz = pStable->GetRectSize();
						pStable->TranslationAndWarpPerspective(frame,frame,rotMat,sz);
					}
					else
					{
						Mat rotMat = pStable->GetLastMovieRotMat().clone();
						cv::Size sz = pStable->GetRectSize();
						pStable->TranslationAndWarpPerspective(frame,frame,rotMat,sz);
					}
					resize(frame,frame,cv::Size(G4DGetTargetWidth(),G4DGetTargetHeight()),0,0,CV_INTER_CUBIC);
					//pMgr->BGR2YUV422(frame,YUV422,G4DGetTargetHeight(),G4DGetTargetWidth(),bInsertLogo);
					pMgr->BGRCC2YUV422Logo(frame,Logo,NoLogo,G4DGetTargetHeight(),G4DGetTargetWidth(),FALSE,FrameRGBInfo());

					/*char strPath[100];
					sprintf(strPath,"M:\\Movie\\[%d]_%d.png",i,j);
					imwrite(strPath,frame);*/
					if(!stLoad->StabFrame.empty())
					{
						stLoad->StabFrame.release();
					}
					stLoad->Logo  = Logo.clone();
					stLoad->NoLogo= NoLogo.clone();
					Sleep(1);
					stLoad->bFinish = 100;

					/*CString strLog;
					strLog.Format(_T("[%d][%d] Affine"),i,j);
					G4DShowLog(5,strLog);*/
					//pDivFrame->
				}
				else if(bStop == FALSE && bSucess == FALSE)
				{
					if(pMgr->GetMovieSignal() == MOVIE_STOP)
						break;

					Mat frame = stLoad->StabFrame.clone();
					if(frame.empty())
					{
						pDivFrame->pArrFrameData->at(j).bFinish == -100;
						continue;
					}
					//pMgr->BGR2YUV422(frame,YUV422,G4DGetTargetHeight(),G4DGetTargetWidth(),bInsertLogo);
					pMgr->BGRCC2YUV422Logo(frame,Logo,NoLogo,G4DGetTargetHeight(),G4DGetTargetWidth(),FALSE,FrameRGBInfo());
					/*char strPath[100];
					sprintf(strPath,"M:\\Movie\\[%d]_%d.png",i,j);
					imwrite(strPath,frame);*/
					stLoad->Logo   = Logo.clone();
					stLoad->NoLogo = NoLogo.clone();
					Sleep(1);
					stLoad->bFinish = 100;
				}
				else if(bStop == TRUE)
				{
					stLoad->bFinish  = -200;
					Sleep(1);
				}
			}

			
			/*if(bStop)
							break;*/
			

			if(pMgr->GetMovieSignal() == MOVIE_STOP)
				break;
		}
		else
		{
			if(pDivFrame->nFinish == MAKING_WAIT)
			{
				vector<stLoadFrame>* pArrLoadFrame = pDivFrame->pArrFrameData;
				for(int j = 0 ; j < pArrLoadFrame->size() ; j++)
				{
					if(pMgr->GetMovieSignal() == MOVIE_STOP)
						break;

					stLoadFrame* stLoad = &pArrLoadFrame->at(j);

					Mat frame = stLoad->StabFrame.clone();
					if(frame.empty())
					{
						stLoad->bFinish = -100;
						continue;
					}

					if(pMgr->GetMovieSignal() == MOVIE_STOP)
						break;

					//pMgr->BGR2YUV422(frame,YUV422,G4DGetTargetHeight(),G4DGetTargetWidth(),bInsertLogo);
					pMgr->BGRCC2YUV422Logo(frame,Logo,NoLogo,G4DGetTargetHeight(),G4DGetTargetWidth(),FALSE,FrameRGBInfo());
					/*char strPath[100];
					sprintf(strPath,"M:\\Movie\\[%d]_%d.png",i,j);
					imwrite(strPath,frame);*/

					if(pMgr->GetMovieSignal() == MOVIE_STOP)
						break;

					stLoad->Logo = Logo.clone();
					stLoad->NoLogo = NoLogo.clone();
					Sleep(1);
					stLoad->bFinish = 100;
				}
			}
			pMgr->m_pMapMovieArray[i]->nFinish = MAKING_FINISH;
		}

		strLog.Format(_T("[%d] Affine Finish"),i);
		G4DShowLog(5,strLog);
		Sleep(1);
	}

	if(pStable)
	{
		delete pStable;
		pStable = NULL;
	}
	pMgr->SetAffineFinish(FALSE);
	G4DShowLog(5,_T("Affine Thread Finish"));
	return TRUE;
}
//Not use
void C4DReplayDecodeMgr::DoGPUDecoding(AJASendGPUFrameInfo* stAJAInfo)
{
	int nIdx = stAJAInfo->nMakingIndex;

	int nMakingCnt = stAJAInfo->nEndIndex - stAJAInfo->nStartIndex + 1;

	DoWaitDivData(nIdx,FALSE,nMakingCnt);

	stDivideFrame* pDivFrame = m_pMapMovieArray[nIdx];
	m_pDivMgr->StartDivGPUFrame(stAJAInfo,pDivFrame);
}
/****************************************************************************************/
//Time Check & Encoding Method																											/
/****************************************************************************************/
double C4DReplayDecodeMgr::GetCounter()
{
	//TRACE(_T("GET COUNTER\n"));

	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart-CounterStart)/PCFreq;
}
void C4DReplayDecodeMgr::StartCounter()
{
	//TRACE(_T("START COUNTER\n"));
	PCFreq = 0.0;
	CounterStart  = 0;

	LARGE_INTEGER li;
	if(!QueryPerformanceFrequency(&li))
		cout << "QueryPerformanceFrequency failed!\n";

	PCFreq = double(li.QuadPart)/1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}
double C4DReplayDecodeMgr::GetDivCounter()
{
	//TRACE(_T("GET COUNTER\n"));

	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart-m_nDivCounterStart)/m_dbDivPCFreq;
}
void C4DReplayDecodeMgr::StartDivCounter()
{
	//TRACE(_T("START COUNTER\n"));
	m_dbDivPCFreq = 0.0;
	m_nDivCounterStart  = 0;

	LARGE_INTEGER li;
	if(!QueryPerformanceFrequency(&li))
		cout << "QueryPerformanceFrequency failed!\n";

	m_dbDivPCFreq = double(li.QuadPart)/1000.0;

	QueryPerformanceCounter(&li);
	m_nDivCounterStart = li.QuadPart;
}
CString C4DReplayDecodeMgr::AddMovieList(CString &str)
{
	CListCtrl* pList = m_pParent->m_cList;

	int nTotal = pList->GetItemCount();

	BOOL bExist = FALSE;

	SYSTEMTIME st;
	GetLocalTime(&st);
	CString strDate = _T("");
	strDate.Format(_T("%04d%02d%02d"),st.wYear,st.wMonth,st.wDay);

	if(G4DGetStabilization())
		m_strFileName.Format(_T("[AJA]%s.mp4"),G4DGet4DMName());
	else
		m_strFileName.Format(_T("[AJA]%s.mp4"),G4DGet4DMName());

	CESMFileOperation fo;
	m_strOutputName.Format(_T("\\\\%s\\RecordSave\\Output\\2D\\%s\\"),m_pParent->m_strIP,strDate);
	G4DCreateAllDirectory(m_strOutputName);
	
	m_strOutputName.Append(m_strFileName);
	
	int nCount = 0;

	while(fo.IsFileExist(m_strOutputName))
	{
		m_strOutputName.Format(_T("\\\\%s\\RecordSave\\Output\\2D\\%s\\[AJA]%s_%d.mp4"),
			m_pParent->m_strIP,strDate,G4DGet4DMName(),nCount);
		str.Format(_T("%s_%d"),G4DGet4DMName(),nCount);

		/*if(GetEncodingFlag() == TRUE)
			m_strOutputName.Format(_T("\\\\%s\\RecordSave\\Output\\2D\\%s\\[AJA]%s_%d.mp4"),
			m_pParent->m_strIP,strDate,G4DGet4DMName(),++nCount);
		else
			m_strOutputName.Format(_T("\\\\%s\\RecordSave\\Output\\2D\\%s\\[AJA]%s_%d.mp4"),
			m_pParent->m_strIP,strDate,G4DGet4DMName(),nCount);*/
		nCount++;
	}
	/*if(G4DGetStabilization())
	{
	m_strOutputName.Append(_T("_stab.mp4"));
	str.Append(_T("_stab"));

	nCount = 0;

	while(fo.IsFileExist(m_strOutputName))
	{
	m_strOutputName.Format(_T("\\\\%s\\RecordSave\\Output\\2D\\%s\\[AJA]%s_%d.mp4_%d_stab.mp4"),
	m_pParent->m_strIP,strDate,G4DGet4DMName(),nCount);
	str.Format(_T("%s_%d"),G4DGet4DMName(),nCount);

	nCount++;
	}

	}*/

	return str;
}
void C4DReplayDecodeMgr::RunEncodeShell(CString strTemp,int nListIdx)
{
	CString strCmd;
	strCmd.Format(_T("\"C:\\Program Files\\ESMLab\\4DMaker\\bin\\ffmpeg.exe\""));
	
	//CString strName;
	//strName.Format(_T("M:\\Movie\\%s"),m_strFileName);

	CString strFileName;
	strFileName = AddMovieList(m_str4DMPath);

	CString strOpt;
	strOpt.Format(_T("-i %s"),strTemp);
	strOpt.Append(_T(" -c copy -bsf:a aac_adtstoasc "));
	strOpt.Append(_T("\""));
	strOpt.Append(m_strOutputName);
	strOpt.Append(_T("\""));

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;
	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	lpExecInfo.lpFile = _T("exit");
	lpExecInfo.lpVerb = L"close";
	lpExecInfo.lpParameters = NULL;

	if(TerminateProcess(lpExecInfo.hProcess,0))
	{
		lpExecInfo.hProcess = 0;
	}
	//Send Info
	CString str4DCPath;
	str4DCPath.Format(_T("\\\\%s\\4DMaker\\Recorder.ini"),G4DGetDistributeIP());
	CESMIni ini;
	CString strTempIniPath;
	if(ini.SetIniFilename(str4DCPath))
	{
		ini.WriteString(_T("Recording"),_T("AutoMakingFlag"),_T("1"));
		strTempIniPath = m_strOutputName.Mid(2,m_strOutputName.GetLength());
		ini.WriteString(_T("Recording"),_T("AutoMakingPath"),strTempIniPath);
	}

	m_pParent->m_cList->ReplaceListData(nListIdx,_T("[AJA]")+strFileName,m_strOutputName,TRUE);

	CString *pstrOutputPath = new CString;
	pstrOutputPath->Format(_T("%s"),m_strOutputName);

	ESMEvent*pMsg1 = new ESMEvent;
	pMsg1->message = AJA_ENCODING_FINISH;
	pMsg1->pDest = (LPARAM)pstrOutputPath;
	G4DAddEvent(pMsg1);

	if(G4DGetEncode())
		//DoEncoding(strCmd,m_strOutputName);
		DoEncoding(strCmd,m_strOutputName,strFileName);
}
BOOL C4DReplayDecodeMgr::EncodingMovie(vector<Mat>*pArrFrameInfo,int nListIdx)
{
	G4DShowLog(5,_T("Encoding Start [1]"));
#if 0
	FFMpegMgr ffmpeg;
	CString strTemp = _T("M:\\Movie\\0.mp4");

	int nCnt = 0;
	CESMFileOperation fo;
	while(fo.IsFileExist(strTemp))
	{
		strTemp.Format(_T("M:\\Movie\\%d.mp4"),nCnt);
		nCnt++;
	}
	/*CString strTempPath;
	strTempPath.Format(_T("M:\\Movie\\%s"),m_strFileName);*/

	ESMEvent*pMsg = new ESMEvent;
	pMsg->message = AJA_ENCODING_START;
	G4DAddEvent(pMsg);

	BOOL bEncode = ffmpeg.InitEncode(strTemp,30,AV_CODEC_ID_MPEG2VIDEO,1,1920,1080);

	if(bEncode)
	{
		Mat frame;
		int nTotalFrame = 0;
#if 1
		for(int i = 0 ; i < pArrFrameInfo->size(); i++)
		{
			frame = pArrFrameInfo->at(i).clone();
			ffmpeg.EncodePushImage(frame,1920,1080);
		}
#else
		for(int i = 0 ; i < m_nTotalMakingCnt; i++)
		{
			stDivideFrame* pDivFrame = m_pMapMovieArray[i];
			for(int j = 0 ; j < pDivFrame->pArrFrameData->size(); j++)
			{
				for(int i = 0 ; i < pDivFrame->pArrFrameData->at(j).nFps; i++)
				{
					frame = pDivFrame->pArrFrameData->at(j).frame.clone();
					ffmpeg.EncodePushImage(frame,1920,1080);
					nTotalFrame++;
				}
			}
		}
#endif
		ffmpeg.EncodePushImage(frame,1920,1080);
		ffmpeg.CloseEncode(TRUE);

		//Shell
		RunEncodeShell(strTemp);
		fo.Delete(strTemp);
	}
	ESMEvent*pMsg1 = new ESMEvent;
	pMsg1->message = AJA_ENCODING_FINISH;
	G4DAddEvent(pMsg1);
	G4DShowLog(5,_T("Encoding Finish"));
#else
	int nSize = pArrFrameInfo->size();
	CString strLog;
	strLog.Format(_T("Encoding Start [1.5] - %d"),nSize);
	G4DShowLog(5,strLog);

	vector<Mat>*tempArrInfo;
	//vector<BYTE*>pArrByteData;
	try
	{
		tempArrInfo = new vector<Mat>(nSize);

		CString strLog;
		strLog.Format(_T("Encoding Start [1.5] - %d"),nSize);
		G4DShowLog(5,strLog);

		for(int i = 0 ; i < nSize; i++)
		{
#if 1
/*
			int nImageSize = frame.rows*frame.cols*frame.channels();
			BYTE* pData = new BYTE[nImageSize];
			memcpy(pData,pArrFrameInfo->at(i).data,nImageSize);
			pArrByteData->push_back(pData);*/

			Mat frame = pArrFrameInfo->at(i).clone();
			if(!frame.empty())
			{
				tempArrInfo->at(i).create(frame.rows,frame.cols,CV_8UC1);
				if(G4DMemcpy(tempArrInfo->at(i).data,frame.data,frame.rows*frame.cols*frame.channels()) == FALSE)
				{
					CString strLog;
					strLog.Format(_T("[%d] Frame memcpy ERROR - %d"),i,frame.rows*frame.cols*frame.channels());
					G4DShowLog(0,strLog);
				}
			}
			else
			{
				CString strLog;
				strLog.Format(_T("[%d] Frame is Empty!!!!!"),i);
				G4DShowLog(0,strLog);
			}
			
#else
			tempArrInfo->push_back(pArrFrameInfo->at(i));
#endif
		}
		G4DShowLog(5,_T("Encoding Start [2]"));

		FRAME_ENCODING_INFO* pInfo = new FRAME_ENCODING_INFO;
		pInfo->pMgr										= this;
		pInfo->pArrFrameInfo						= tempArrInfo;//new vector<Mat>;
		pInfo->nListIdx									= nListIdx;

		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)_beginthreadex(NULL,0,_EncodingThread,(void*)pInfo,0,NULL);
		CloseHandle(hSyncTime);


		return TRUE;
	}
	catch(exception& e)
	{
		//int nDataSize = _Size;
		return FALSE;
	}

#endif
}
unsigned WINAPI C4DReplayDecodeMgr::_EncodingThread(LPVOID param)
{
	FRAME_ENCODING_INFO*pInfo = (FRAME_ENCODING_INFO*)param;
	C4DReplayDecodeMgr* pMgr = pInfo->pMgr;
	vector<Mat>*pArrFrameInfo   = pInfo->pArrFrameInfo;
	int nListIdx								= pInfo->nListIdx;

	if(pInfo)
	{
		delete pInfo;
		pInfo = NULL;
	}
	pMgr->SetEncodingFlag(TRUE);
	FFMpegMgr ffmpeg;
	CString strTemp = _T("M:\\Movie\\0.mp4");

	int nCnt = 0;
	CESMFileOperation fo;
	while(fo.IsFileExist(strTemp))
	{
		strTemp.Format(_T("M:\\Movie\\%d.mp4"),nCnt);
		nCnt++;
	}
	/*CString strTempPath;
	strTempPath.Format(_T("M:\\Movie\\%s"),m_strFileName);*/

	G4DShowLog(5,_T("Encoding Start [3]"));
	CString strLog;
	strLog.Format(_T("Encode frame rate: %d"),G4DGetEncodeFrameRate());
	G4DShowLog(5,strLog);

	BOOL bEncode = ffmpeg.InitEncode(strTemp,G4DGetEncodeFrameRate(),AV_CODEC_ID_MPEG2VIDEO,1,
									G4DGetTargetWidth(),G4DGetTargetHeight());

	if(bEncode)
	{
		G4DShowLog(5,_T("Encoding Start [4]"));
		Mat frame;
		int nTotalFrame = 0;
#if 1
		int nSize = pArrFrameInfo->size();
		for(int i = 0 ; i < nSize; i++)
		{
#if 1
			frame = pArrFrameInfo->at(i).clone();
			if(!frame.empty())
				ffmpeg.EncodePushImage(frame,G4DGetTargetWidth(),G4DGetTargetHeight());

			pArrFrameInfo->at(i).release();
#else
			frame = pArrFrameInfo->at(i).clone();
			ffmpeg.EncodePushImage(frame,G4DGetTargetWidth(),G4DGetTargetHeight());
#endif
		}
		if(!frame.empty())
			ffmpeg.EncodePushImage(frame,G4DGetTargetWidth(),G4DGetTargetHeight());
#else
		for(int i = 0 ; i < pMgr->m_nTotalMakingCnt; i++)
		{
			stDivideFrame* pDivFrame = pMgr->m_pMapMovieArray[i];
			for(int j = 0 ; j < pDivFrame->pArrFrameData->size(); j++)
			{
				for(int i = 0 ; i < pDivFrame->pArrFrameData->at(j).nFps; i++)
				{
					frame = pDivFrame->pArrFrameData->at(j).frame.clone();
					ffmpeg.EncodePushImage(frame,1920,1080);
					nTotalFrame++;
				}
			}
		}
		ffmpeg.EncodePushImage(frame,1920,1080);
#endif
		G4DShowLog(5,_T("Encoding Start [5]"));
		ffmpeg.CloseEncode(TRUE);

		//Shell
		G4DShowLog(5,_T("Encoding Start [6]"));
		pMgr->RunEncodeShell(strTemp,nListIdx);
		fo.Delete(strTemp);
	}
	G4DShowLog(5,_T("Encoding Finish"));

	if(pArrFrameInfo)
	{
		pArrFrameInfo->clear();
		delete pArrFrameInfo;
		pArrFrameInfo = NULL;
	}

	pMgr->SetEncodingFlag(FALSE);
	return TRUE;
}
void C4DReplayDecodeMgr::DoEncoding(CString strCmd,CString strPath,CString strFileName)
{
	CString strRePath;// = m_strOutputName;
	strRePath.Format(_T("%s\\%s@x264"),GetSecondPath(),strFileName);
	if(G4DGetEncodeTS())
		strRePath.Append(_T(".ts"));
	else
		strRePath.Append(_T(".mp4"));

	//strRePath.Append(_T("@x264.mp4"));

	int nBitrate = G4DGetEncodeBitrate();
	if(nBitrate <= 0)
	{
		G4DShowLog(0,_T("ENCODING BITRATE ERROR!!!"));
		G4DShowLog(0,_T("SET 5Mbps!!!"));
		nBitrate = 5;
	}
	CString strBitrate;
	strBitrate.Format(_T(" -b:v %dM "),nBitrate);

	//190321 hjcho Sound Opt
	CString strReEncodingOpt;
	CESMFileOperation fo;
	CString strSound;
	strSound.Format(_T("%s\\AudioSilence.mp3"),G4DGetEncodePath());
	G4DShowLog(5,strSound);
	//strSound.Format(_T("\"C:\\Program Files\\ESMLab\\4DMaker\\bin\\AudioSilence.mp3\""));
	if(fo.IsFileExist(strSound))
	{
		G4DShowLog(5,_T("Insert Sound..!"));
		strReEncodingOpt.Append(_T(" -i \""));
		strReEncodingOpt.Append(strSound);
		strReEncodingOpt.Append(_T("\""));
	}

	strReEncodingOpt.Append(_T(" -i \""));
	strReEncodingOpt.Append(strPath);
	strReEncodingOpt.Append(_T("\""));
	
	//strReEncodingOpt.Append(_T(" -c:v libx264 -g 6 -preset ultrafast "));		
	strReEncodingOpt.Append(_T(" -c:v libx264 -g 6 -profile:v main "));		
	strReEncodingOpt.Append(strBitrate);
	strReEncodingOpt.Append(_T("\""));
	strReEncodingOpt.Append(strRePath);
	strReEncodingOpt.Append(_T("\""));

	for(int i = 0 ; i < 2 ; i++)
	{
		CString strTemp;
		strTemp.Format(_T(" -map %d"),i);
		strReEncodingOpt.Append(strTemp);
	}

	SHELLEXECUTEINFO lpExecInfoReEncoding;
	lpExecInfoReEncoding.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfoReEncoding.lpFile = strCmd;
	lpExecInfoReEncoding.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfoReEncoding.hwnd = NULL;  
	lpExecInfoReEncoding.lpVerb = L"open";
	lpExecInfoReEncoding.lpParameters = strReEncodingOpt;
	lpExecInfoReEncoding.lpDirectory = NULL;

	lpExecInfoReEncoding.nShow = SW_HIDE; // hide shell during execution
	lpExecInfoReEncoding.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfoReEncoding);

	// wait until the process is finished
	if (lpExecInfoReEncoding.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfoReEncoding.hProcess, INFINITE);
		::CloseHandle(lpExecInfoReEncoding.hProcess);
	}
	//G4DShowLog(5,strReEncodingOpt);
	G4DShowLog(5,_T("[RE-ENCODE]")+strRePath);
}

void C4DReplayDecodeMgr::BGRCC2YUV422(Mat frame,Mat YUV422,int nHeight,int nWidth,FrameRGBInfo info,BOOL bInsertLogo/* =FALSE */)
{
	int i,j;
	int B,G,R;
	int Y,U,V;
	int rIdx,cIdx,yIdx,xIdx;

	int start = GetTickCount();

	int nChannel = m_matLogo.channels();

	for(i=0;i<nHeight;i++)
	{
		rIdx = nWidth*(i*3);
		yIdx = (nWidth*2)*i;
		
		for(j=0;j<nWidth;j++)
		{
			cIdx = rIdx + (j*3);
			xIdx = yIdx +j*2;
			
			B = CalcTransValue(frame.data[cIdx]		,info.dBlue);
			G = CalcTransValue(frame.data[cIdx+1],info.dGreen);
			R = CalcTransValue(frame.data[cIdx+2],info.dRed);

			if(bInsertLogo && nChannel == 4)
			{
				int nAlphaValue = m_matLogo.data[nWidth * (i*4) + (j*4) + 3];

				if(nAlphaValue != 0)
				{
					int nIdx = nWidth * (i*4) + (j*4);

					B = (int)((double)B *(double)(255 -nAlphaValue) / 255.)+
						(int)((double)m_matLogo.data[nIdx] *(double)(nAlphaValue) / 255.);

					G = (int)((double)G *(double)(255 -nAlphaValue) / 255.)+
						(int)((double)m_matLogo.data[nIdx+1] *(double)(nAlphaValue) / 255.);

					R = (int)((double)R *(double)(255 -nAlphaValue) / 255.)+
						(int)((double)m_matLogo.data[nIdx+2] *(double)(nAlphaValue) / 255.);
				}
			}

			Y =  (0.257*R) + (0.504*G) + (0.098*B) + 16;  //CuttingValue(Y);


			if(j%2 == 0)
			{
				U = -(0.148*R) - (0.291*G) + (0.439*B) + 128; //CuttingValue(U);
				YUV422.data[xIdx] = U;//1
				YUV422.data[xIdx+1] = Y;//0
			}
			else
			{
				V =  (0.439*R) - (0.368*G) - (0.071*B) + 128; //CuttingValue(V);
				YUV422.data[xIdx] = V;//2
				YUV422.data[xIdx+1] = Y;//0
			}
		}
	}
}
int C4DReplayDecodeMgr::CalcTransValue(int nPixel,int nRefValue)
{
	int nResult = nPixel - nRefValue;

	if(nResult > 255)
		nResult = 255;

	if(nResult < 0)
		nResult = 0;

	return nResult;
}
void C4DReplayDecodeMgr::ScreenToYUV422(BYTE *pImg,Mat YUV422,int nHeight,int nWidth)
{
	int i,j;
	int B,G,R;
	int Y,U,V;
	int rIdx,cIdx,yIdx,xIdx;

	int start = GetTickCount();

	int nChannel = m_matLogo.channels();

	for(i=0;i<nHeight;i++)
	{
		rIdx = nWidth*(i*4);
		yIdx = (nWidth*2)*i;

		for(j=0;j<nWidth;j++)
		{
			cIdx = rIdx + (j*4);
			xIdx = yIdx +j*2;

			B = pImg[cIdx];
			G = pImg[cIdx+1];
			R = pImg[cIdx+2];

			Y =  (0.257*R) + (0.504*G) + (0.098*B) + 16;  //CuttingValue(Y);


			if(j%2 == 0)
			{
				U = -(0.148*R) - (0.291*G) + (0.439*B) + 128; //CuttingValue(U);
				YUV422.data[xIdx] = U;//1
				YUV422.data[xIdx+1] = Y;//0
			}
			else
			{
				V =  (0.439*R) - (0.368*G) - (0.071*B) + 128; //CuttingValue(V);
				YUV422.data[xIdx] = V;//2
				YUV422.data[xIdx+1] = Y;//0
			}
		}
	}
}
void C4DReplayDecodeMgr::DoColorRevision(cv::Mat* frame,FrameRGBInfo stColorInfo)
{
	int nWidth = frame->cols;
	int nHeight = frame->rows;

	
	for(int i = 0 ; i < nHeight ; i++)
	{
		for(int j = 0 ; j < nWidth ; j++)
		{
			int nIdx = nWidth * (i*3) + (j*3);

			frame->data[nIdx] = CalcTransValue(frame->data[nIdx],stColorInfo.dBlue);
			frame->data[nIdx+1] = CalcTransValue(frame->data[nIdx+1],stColorInfo.dBlue);
			frame->data[nIdx+2] = CalcTransValue(frame->data[nIdx+2],stColorInfo.dBlue);
		}
	}
} 
void C4DReplayDecodeMgr::BGRCC2YUV422Logo(Mat frame,Mat Logo,Mat NoLogo,int nHeight,int nWidth,BOOL bColorRevision,FrameRGBInfo info)
{
	int i,j;
	int B,G,R;
	int Y,U,V;
	int rIdx,cIdx,yIdx,xIdx;

	int start = GetTickCount();

	int nChannel = m_matLogo.channels();

	for(i=0;i<nHeight;i++)
	{
		rIdx = nWidth*(i*3);
		yIdx = (nWidth*2)*i;
		for(j=0;j<nWidth;j++)
		{
			cIdx = rIdx + (j*3);
			xIdx = yIdx +j*2;

			B = bColorRevision == TRUE ? CalcTransValue(frame.data[cIdx]	 ,info.dBlue) : frame.data[cIdx];
			G = bColorRevision == TRUE ? CalcTransValue(frame.data[cIdx+1],info.dBlue) : frame.data[cIdx+1];
			R = bColorRevision == TRUE ? CalcTransValue(frame.data[cIdx+2],info.dBlue) : frame.data[cIdx+2];

			//NoLogo
			Y =  (0.257*R) + (0.504*G) + (0.098*B) + 16;  //CuttingValue(Y);

			if(j%2 == 0)
			{
				U = -(0.148*R) - (0.291*G) + (0.439*B) + 128; //CuttingValue(U);
				NoLogo.data[xIdx] = U;//1
				NoLogo.data[xIdx+1] = Y;//0
			}
			else
			{
				V =  (0.439*R) - (0.368*G) - (0.071*B) + 128; //CuttingValue(V);
				NoLogo.data[xIdx] = V;//2
				NoLogo.data[xIdx+1] = Y;//0
			}

			if(nChannel == 4)
			{
				int nAlphaValue = m_matLogo.data[nWidth * (i*4) + (j*4) + 3];

				if(nAlphaValue != 0)
				{
					int nIdx = nWidth * (i*4) + (j*4);

					B = (int)((double)B *(double)(255 -nAlphaValue) / 255.)+
						(int)((double)m_matLogo.data[nIdx] *(double)(nAlphaValue) / 255.);

					G = (int)((double)G *(double)(255 -nAlphaValue) / 255.)+
						(int)((double)m_matLogo.data[nIdx+1] *(double)(nAlphaValue) / 255.);

					R = (int)((double)R *(double)(255 -nAlphaValue) / 255.)+
						(int)((double)m_matLogo.data[nIdx+2] *(double)(nAlphaValue) / 255.);;
				}

				Y =  (0.257*R) + (0.504*G) + (0.098*B) + 16;  //CuttingValue(Y);


				if(j%2 == 0)
				{
					U = -(0.148*R) - (0.291*G) + (0.439*B) + 128; //CuttingValue(U);
					Logo.data[xIdx] = U;//1
					Logo.data[xIdx+1] = Y;//0
				}
				else
				{
					V =  (0.439*R) - (0.368*G) - (0.071*B) + 128; //CuttingValue(V);
					Logo.data[xIdx] = V;//2
					Logo.data[xIdx+1] = Y;//0
				}
			}
		}
	}
}