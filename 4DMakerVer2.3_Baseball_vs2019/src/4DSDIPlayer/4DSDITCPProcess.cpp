#include "stdafx.h"
#include "4DSDITCPProcess.h"
#include "ESMFileOperation.h"
C4DSDITCPProcess::C4DSDITCPProcess(C4DReplaySDIPlayerDlg* pParent)
{
	m_pParent = pParent;
	m_bConnect = FALSE;
	m_bInsertMovie = TRUE;
	m_bSendThreadRun	= FALSE;
	StartSendThread();
}
C4DSDITCPProcess::~C4DSDITCPProcess()
{

}
void C4DSDITCPProcess::Init()
{
	WSADATA wsadata;
	WSAStartup(MAKEWORD(2,2),&wsadata);

	m_sock = SetTCPServer(PORT,5);
	if(m_sock == -1)
	{
		AfxMessageBox(_T("Wait socket Err"));
		WSACleanup();
		return;
	}

#if 1
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_RunAcceptThread,(void*)this,0,NULL);
	CloseHandle(hSyncTime);
#else
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_RunTCPThread,this,0,NULL);
	CloseHandle(hSyncTime);
#endif
}
void C4DSDITCPProcess::CloseSocket()
{
	m_mpIP.clear();
	closesocket(m_sock);
}
void C4DSDITCPProcess::SetIP(CString strIP,int nIndex)
{
	m_mpIP.insert(pair<CString,int>(strIP,nIndex));
	m_strIP = strIP;
}
int C4DSDITCPProcess::GetRegistedIP(CString strIP)
{
	int nState = m_mpIP[strIP];

	return nState;
}
void C4DSDITCPProcess::Create(int af, int type, int protocol, int sendtimeout, int recvtimeout)
{

}
SOCKET C4DSDITCPProcess::SetTCPServer(short pnum,int blog)
{
	SOCKET sock;
	sock = socket(AF_INET, SOCK_STREAM,IPPROTO_TCP);//소켓 생성
	if(sock == -1)
		return -1;   

	SOCKADDR_IN servaddr={0};//소켓 주소
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr = GetDefaultMyIP();
	servaddr.sin_port = htons(PORT);

	int re = 0;
	re = bind(sock,(struct sockaddr *)&servaddr,sizeof(servaddr));//소켓 주소와 네트워크 인터페이스 결합

	if(re == -1)
		return -1;    


	re = listen(sock,blog);//백 로그 큐 설정
	if(re == -1)
		return -1;    

	return sock;
}
IN_ADDR C4DSDITCPProcess::GetDefaultMyIP()
{
	char localhostname[MAX_PATH];
	IN_ADDR addr={0,};
	if(gethostname(localhostname, MAX_PATH) == SOCKET_ERROR)
		return addr;

	HOSTENT *ptr = gethostbyname(localhostname);
	while(ptr && ptr->h_name)
	{
		if(ptr->h_addrtype == PF_INET)
		{
			memcpy(&addr, ptr->h_addr_list[0], ptr->h_length);
			break;
		}
		ptr++;
	}

	return addr;
}
unsigned WINAPI C4DSDITCPProcess::_RunTCPThread(LPVOID param)
{
	C4DSDITCPProcess* TCPProcess = (C4DSDITCPProcess*) param;
	SOCKET mainsock = TCPProcess->m_sock;
	SOCKET dosock;   
	SOCKADDR_IN cliaddr={0};

	int len = sizeof(cliaddr);
	G4DShowLog(5,_T("TCP Start..."));
	dosock = accept(mainsock,(SOCKADDR *)&cliaddr,&len);//연결 수락
	TCPProcess->SetSocket(dosock);
	CString strIP = TCPProcess->CharToCString(inet_ntoa(cliaddr.sin_addr));
	G4DShowLog(2,_T("Connected![")+strIP+_T("]"));
	TCPProcess->SetConnect(TRUE);
	G4DSendBoardState();
	int nCnt = 0;
	while(true)
	{
		Sleep(1);	

		TRACE("Wait...\n");
		if(dosock == -1)
		{
			perror("Accept 실패");
			break;
		}
		
		TRACE("%s:%d의 연결 요청 수락\n",inet_ntoa(cliaddr.sin_addr),ntohs(cliaddr.sin_port));
		/*if(nCnt == 0)
		{
			SendTCPData();
		}*/
		int nIdx = TCPProcess->GetRegistedIP(strIP);
		if(nIdx > 0)//4DC 명령용
		{
			BOOL bConnect = TCPProcess->ReceiveData(dosock,nIdx);
			if(bConnect == FALSE)
				break;
		}
	}
	TCPProcess->SetConnect(FALSE);
	closesocket(dosock);
	//closesocket(mainsock);//소켓 닫기
	TCPProcess->CloseSocket();
	G4DShowLog(0,_T("TCP Close..."));
	G4DShowLog(0,_T("Do restart TCP..."));
	TCPProcess->ChangeConnectState(FALSE);
	return TRUE;
}
CString C4DSDITCPProcess::CharToCString(char* str)
{
	if( str == NULL)
		return _T("");

	CString strString;
	int len; 
	BSTR buf; 
	len = MultiByteToWideChar(CP_ACP, 0, str, strlen(str), NULL, NULL); 
	buf = SysAllocStringLen(NULL, len); 
	MultiByteToWideChar(CP_ACP, 0, str, strlen(str), buf, len); 

	strString.Format(_T("%s"), buf); 
	strString.Trim();

	return strString;
}
BOOL C4DSDITCPProcess::ReceiveData(SOCKET sock,BOOL bDiv)
{
	char *recvbodybuf;//[5128]= "";
	int recved;
	RCPHeader recvheadbuf;

	//recved = recv(sock,msg,sizeof(msg),0);
	//stAJAInfo info;
	//memcpy(&info,msg,sizeof(stAJAInfo));
	//G4DShowLog(5,_T("TCP TEST"));
	/*char strMsg[100];
	sprintf(strMsg,"[%02d:%02d]\n",time.GetMinute(),time.GetSecond());
	TRACE(strMsg);
	TRACE(info.strOutputPath);
	TRACE("\n");
	TRACE(info.str4DMPath);
	TRACE("\n");*/

	//	send(sock,msg,sizeof(stAJAInfo),0);
	//	DoAutoAdd(info,nIdx);
	//while(recv(sock,msg,sizeof(msg),0)>0)//수신
	//{
	//	G4DShowLog(5,ESMUtil::CharToCString(msg));
	//	TRACE("recv:%s\n",msg);
	//	//send(sock,msg,sizeof(msg),0);//송신
	//}   
	recved = recv(sock,(char*)&recvheadbuf,sizeof(RCPHeader),0);
	if(recved <= 0)
	{
		G4DShowLog(5,_T("Recv Err"));
		return FALSE;
	}
	if(strcmp(recvheadbuf.protocol,"RCP1.0"))
	{
		G4DShowLog(0, _T("Unknown Protocol"));
		return FALSE;
	}
	if(recvheadbuf.bodysize)
	{
		DWORD dwBodySize = recvheadbuf.bodysize;
		recvbodybuf = new char[dwBodySize];
		memset(recvbodybuf, 0x0, dwBodySize);
		int nIndex = 0;

		while(1)
		{
			recved = 0;
			recved = recv(sock,&recvbodybuf[nIndex],((dwBodySize >= 8192) ? 8192 : dwBodySize),0);

			if((recved == dwBodySize) || (dwBodySize == 0))
				break;

			nIndex += recved;
			dwBodySize -= recved;
		}
		ParsePacket((char*)&recvheadbuf, recvbodybuf, recvheadbuf.bodysize,bDiv);

		delete []recvbodybuf;
		recvbodybuf = NULL;
	}
	else //if(recvheadbuf.bodysize == 0 && recvheadbuf.command >= 0)
		ParsePacket((char*)&recvheadbuf, NULL, 0,bDiv);



	//csLog.Format(_T("[%d] Receive Finish"),recvheadbuf.command);
	//G4DShowLog(5,csLog);
	return TRUE;
	//closesocket(sock);//소켓 닫기
}
CString C4DSDITCPProcess::FindFileName(CString strPath)
{
	CString strFileName;
	strFileName = strPath.Right(strPath.GetLength() - strPath.ReverseFind(_T('\\')) -1);

	return strFileName;
}
void C4DSDITCPProcess::ParsePacket(char* headerbuf, char* bodybuf, int packetsize,BOOL bDiv)
{
	RCPHeader* pHeader = (RCPHeader*)headerbuf;

	DWORD bodysize = packetsize;
	DWORD command  = pHeader->command;

	ParsePacketServer( headerbuf,  bodybuf, packetsize,bDiv);
}
void C4DSDITCPProcess::ParsePacketServer(char* headerbuf, char* bodybuf, int packetsize,BOOL bDiv)
{
	char* bSendbuf = NULL;
	RCPHeader *packetheader = (RCPHeader*)headerbuf;
	DWORD command = packetheader->command;
	char* pBodyData = NULL, *pTpData = NULL;

	int nTpData1 = 0, nTpData2 = 0; 
	RCP_Property_Info	*pPropBody = NULL;

	TRACE("Command: %d\n",command);
	if(bDiv)
	{
		switch(command)
		{
		case F4DC_PROGRAM_STATE: 
			{
				//G4DShowLog(5,G4DGet4DCState(packetheader->param1));
				//G4DSetRecordingInfo(packetheader->param2,packetheader->param3);
			}
			break;
		case F4DC_MAKING_START:
			{
				int nBodySize = packetheader->bodysize;
				char* str = new char[nBodySize + 1];
				memset(str,0,nBodySize + 1);
				memcpy(str,bodybuf,nBodySize);

				CString str4DMName = ESMUtil::CharToCString(str);
				//G4DShowLog(5,G4DGet4DCState(packetheader->param1));
				G4DSetRecordingInfo(packetheader->param2,packetheader->param3);

				CString strLog;
				strLog.Format(_T("frame_time: %d,per_second: %d "),packetheader->param2,packetheader->param3);
				G4DShowLog(5,strLog);

				G4DShowLog(1,str4DMName +_T(" List Up"));
				if(str)
				{
					delete str;
					str = NULL;
				}

				m_str4DMName = str4DMName;
				G4DSetStabilization(packetheader->param1);
				G4DSetGPUTotalMakingCnt(9999);
				m_pParent->m_pDecodeMgr->SetMovieSignal(MOVIE_STOP);

				/*m_strFilePath = m_pParent->m_pDecodeMgr->AddMovieList(str4DMName);
				m_pParent->m_cList->AddFile(str4DMName,m_strFilePath);
				int nIdx = m_pParent->m_cList->GetItemCount()-1;
				m_pParent->DoFileDecode(m_strFilePath,nIdx,TRUE);*/
			}
			break;
		case F4DC_MAKING_TOTALCNT:
			{
				G4DDivMakeStart(m_str4DMName,packetheader->param1,packetheader->param2);
			}
			break;
		case F4DC_ADJUST_SEND:
			{
				G4DClearAllAdjust();

				UINT nSize = 0;
				size_t nSizeUint = sizeof(UINT);
				UINT nSizeAdj = sizeof(_RCP_ADJUST_INFO);
				memcpy(&nSize, bodybuf, sizeof(UINT));
				char* pLoc = bodybuf + sizeof(UINT);

				for ( int i=0 ; i<nSize ; i++ )
				{
					UINT nSizeDsc=0;
					memcpy(&nSizeDsc, pLoc, nSizeUint);
					pLoc = pLoc + nSizeUint;

					char *cDSC = new char[nSizeDsc + 1];
					memset(cDSC, 0, nSizeDsc + 1);
					memcpy(cDSC, pLoc, nSizeDsc);
					pLoc = pLoc + nSizeDsc;

					CString strDSC;
					strDSC = CA2T(cDSC);
					delete[] cDSC;

					_RCP_ADJUST_INFO pAdjInfoRecv;
					memcpy(&pAdjInfoRecv, pLoc, nSizeAdj);
					pLoc = pLoc + nSizeAdj;

					stAdjustInfo* pAdjInfo = new stAdjustInfo;
					pAdjInfo ->AdjAngle = pAdjInfoRecv.AdjAngle;
					pAdjInfo ->AdjMove.x = pAdjInfoRecv.AdjMove.x;
					pAdjInfo ->AdjMove.y = pAdjInfoRecv.AdjMove.y;
					pAdjInfo ->AdjptRotate.x = pAdjInfoRecv.AdjptRotate.x;
					pAdjInfo ->AdjptRotate.y = pAdjInfoRecv.AdjptRotate.y;
					pAdjInfo->AdjSize= pAdjInfoRecv.AdjSize;
					pAdjInfo->nHeight = pAdjInfoRecv.nHeight;
					pAdjInfo->nWidth = pAdjInfoRecv.nWidth;
					pAdjInfo->strDSC = strDSC;
					pAdjInfo->stMargin = pAdjInfoRecv.stMargin;

					G4DSetAdjData(pAdjInfo,strDSC);
				}
				G4DShowLog(5,_T("Adjust Load Finish!"));
			}
			break;
		case F4DC_GPUFRAMEPATH_SEND:
			{
#if 1
				vector<MakeFrameInfo>* pArrMovieInfo = new vector<MakeFrameInfo>;
				//pArrMovieInfo->at(0).strTime;
				for(int i =0 ;i < packetheader->param1 ; i++)
				{
					MakeFrameInfo stMakeFrameInfo;
					memcpy(&stMakeFrameInfo, bodybuf, sizeof(MakeFrameInfo));
					bodybuf += sizeof(MakeFrameInfo);

					pArrMovieInfo->push_back(stMakeFrameInfo);
				}
				G4DSetReverseMovie(packetheader->param3);
				m_pParent->InsertMovieData(NULL,pArrMovieInfo,packetheader->param2,FALSE,TRUE);
				/*CString csLog;
				csLog.Format(_T("[%d] Input"),packetheader->param2);
				G4DShowLog(3,csLog);*/
				//G4DMakeFrameInfo(pArrMovieInfo,packetheader->param2);

#else
				AJASendGPUFrameInfo* AJASendInfo = new AJASendGPUFrameInfo;
				memcpy(AJASendInfo,bodybuf,sizeof(AJASendGPUFrameInfo));

				G4DDoMakeFrame(AJASendInfo);
#endif
			}
			break;
		case F4DC_CPUFRAME_INFO:
			{
				CString str;
				str.Format(_T("CPUMaingFrame: %d"),packetheader->param1);

				m_pParent->InsertMovieData(NULL,NULL,packetheader->param1,TRUE,TRUE);
				//G4DSetCPUMakingIdx(packetheader->param1);
				//G4DShowLog(5,str);
			}
			break;
		case F4DC_CPUFRAME_MAKE_FINISH:
			{
				CString str;
				int nBodySize = packetheader->bodysize;
				char* str1 = new char[nBodySize + 1];
				memset(str1,0,nBodySize + 1);
				memcpy(str1,bodybuf,nBodySize);

				str = ESMUtil::CharToCString(str1);
				m_pParent->InsertMovieData(str,NULL,packetheader->param1,TRUE,TRUE,TRUE,packetheader->param2);
				/*G4DShowLog(5,str);

				str.Format(_T("%d - %d"),packetheader->param1,packetheader->param2);
				G4DShowLog(5,str);*/

				//G4DSetStartDecoding(str,packetheader->param1,packetheader->param2);
			}
			break;
		case F4DC_AUTO_ADD:
			{
				CString str;
				int nBodySize = packetheader->bodysize;
				char* str1 = new char[nBodySize + 1];
				memset(str1,0,nBodySize + 1);
				memcpy(str1,bodybuf,nBodySize);

				str = ESMUtil::CharToCString(str1);
				CString strFileName = G4DGetFileNameFromPath(str);
				m_pParent->m_cList->AddFile(strFileName,str);
			}
			break;
		case F4DC_SEND_CPU_FILE:
			{
				//param1 : data size
				//param2 : Index

				//csPath.
				CString csLog;
				csLog.Format(_T("[%d] Receive Start"),packetheader->param2);
				G4DShowLog(0,csLog);

				CString strSavePath = _T("M:\\Movie\\1");
				int nCnt = 1;
				CESMFileOperation fo;

				while(1)
				{
					if(G4DCreateAllDirectory(strSavePath))
						break;

					strSavePath.Format(_T("M:\\Movie\\%d"),++nCnt);
				}
				//strSavePath.Format(_T("M:\\Movie\\%d.mp4"),packetheader->param2);
				CString strFileName;
				strFileName.Format(_T("\\%d.mp4"),packetheader->param2);
				strSavePath.Append(strFileName);

				CFile file;
				if(file.Open(strSavePath,CFile::modeCreate|CFile::modeReadWrite))
				{
					char* pBuf = new char[packetheader->param1];
					memcpy(pBuf,bodybuf,packetheader->param1);
					file.Write(pBuf,packetheader->param1);
					if(pBuf)
					{
						delete[] pBuf;
						pBuf = NULL;
					}
				}
				file.Close();
				m_pParent->InsertMovieData(strSavePath,NULL,packetheader->param2,TRUE,TRUE,TRUE,TRUE);

				csLog.Format(_T("[%d][%d] Receive Finish"),packetheader->command,packetheader->param2);
				G4DShowLog(0,csLog);
			}
			break;
		}
	}
	else
	{
		switch(command)
		{
		case F4DC_AUTO_ADD:
			{
				CString str;
				int nBodySize = packetheader->bodysize;
				char* str1 = new char[nBodySize + 1];
				memset(str1,0,nBodySize + 1);
				memcpy(str1,bodybuf,nBodySize);

				str = ESMUtil::CharToCString(str1);
				CString strFileName = G4DGetFileNameFromPath(str);
				m_pParent->m_cList->AddFile(strFileName,str);
			}
			break;
		}
	}
}
void C4DSDITCPProcess::StartSendThread()
{
	if(m_bSendThreadRun == FALSE)
	{
		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)_beginthreadex(NULL,0,_RunSendThread,this,0,NULL);
		CloseHandle(hSyncTime);
		m_bSendThreadRun = TRUE;
	}
}
unsigned WINAPI C4DSDITCPProcess::_RunSendThread(LPVOID param)
{
	C4DSDITCPProcess* pMgr = (C4DSDITCPProcess*)param;	
	if (!pMgr) return FALSE;
	ESMEvent* pMsg	= NULL;

	TRACE(_T("_SendThread START\r\n"));
	while(1)
	{
		Sleep(1);
		if (pMgr->GetConnect())
		{
			int nAll = pMgr->m_arMsg.size();
			while(nAll--)
			{
				if(pMgr->GetConnect() == FALSE)
					break;

				pMsg = pMgr->m_arMsg.at(0);
				if(!pMsg)
				{
					pMgr->m_arMsg.erase(pMgr->m_arMsg.begin());
					continue;
				}
				if (pMgr->GetConnect())
					pMgr->SendSodketData(pMsg);

				pMgr->m_arMsg.erase(pMgr->m_arMsg.begin());

				if(pMgr->GetConnect() == FALSE)
					break;
			}
			if(pMgr->GetConnect() == FALSE)
				break;
		}
	}
	G4DShowLog(5,_T("SendThread Finish.."));
	TRACE(_T("_SendThread END\r\n"));
	_endthread();

	return TRUE;
}
void C4DSDITCPProcess::SendSodketData(ESMEvent* pMsg)
{
	if(GetConnect() == FALSE)
		return;

	char* bSendbuf = NULL;
	RCPHeader packetheader;
	char* pBodyData = NULL, *pTpData = NULL;
	int nTpData1 = 0, nTpData2 = 0; 
	RCP_Property_Info	*pPropBody = NULL;

	switch(pMsg->message)
	{
	case AJA_BOARDSTATE:
		{
			CString strState;
			if(pMsg->nParam1 == TRUE)
				strState.Format(_T("Connection Success"));
			else
				strState.Format(_T("Connection Fail..."));
			
			packetheader.bodysize = strState.GetLength()*sizeof(char);
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			memset(bSendbuf,0,packetheader.bodysize);
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char* strtemp = ESMUtil::CStringToChar(strState);
			memcpy(pBodyData,strtemp,sizeof(char)*strState.GetLength());
			packetheader.command = AJA_BOARDSTATE;
			packetheader.param1  = m_pParent->m_bScreen;

			if(strtemp)
			{
				delete strtemp;
				strtemp = NULL;
			}
		}
		break;
	case AJA_MAKING_START:
		{
			CString strState = _T("Send Finish");

			packetheader.bodysize = strState.GetLength()*sizeof(char);
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			memset(bSendbuf,0,packetheader.bodysize);
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char* strtemp = ESMUtil::CStringToChar(strState);
			memcpy(pBodyData,strtemp,sizeof(char)*strState.GetLength());
			packetheader.command = AJA_MAKING_START;

			if(strtemp)
			{
				delete strtemp;
				strtemp = NULL;
			}
			G4DShowLog(5,strState);
		}
		break;
	case AJA_MAKING_FINISH:
		{
			CString strState = _T("Send Finish");

			packetheader.bodysize = strState.GetLength()*sizeof(char);
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			memset(bSendbuf,0,packetheader.bodysize);
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char* strtemp = ESMUtil::CStringToChar(strState);
			memcpy(pBodyData,strtemp,sizeof(char)*strState.GetLength());
			packetheader.command = AJA_MAKING_FINISH;
			packetheader.param1 = pMsg->nParam1;
			if(strtemp)
			{
			delete strtemp;
			strtemp = NULL;
			}
			G4DShowLog(5,strState);
		}
		break;
	case AJA_ENCODING_FINISH:
		{
			CString* pstrPath = (CString*)pMsg->pDest;
			//G4DShowLog(5,*pstrPath);
			//CString strPath = m_pParent->m_pDecodeMgr->m_strOutputName;
			packetheader.bodysize = pstrPath->GetLength()*sizeof(char);
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			memset(bSendbuf,0,packetheader.bodysize);
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char* strtemp = ESMUtil::CStringToChar(*pstrPath);
			memcpy(pBodyData,strtemp,sizeof(char)*pstrPath->GetLength());

			packetheader.command = AJA_ENCODING_FINISH;

			if(strtemp)
			{
				delete strtemp;
				strtemp = NULL;
			}
		}
		break;
	case AJA_ENCODING_START:
		{
			CString strState = _T("Send Finish");

			packetheader.bodysize = strState.GetLength()*sizeof(char);
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			memset(bSendbuf,0,packetheader.bodysize);
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char* strtemp = ESMUtil::CStringToChar(strState);
			memcpy(pBodyData,strtemp,sizeof(char)*strState.GetLength());
			packetheader.command = AJA_ENCODING_START;
			packetheader.param1 = pMsg->nParam1;
			if(strtemp)
			{
				delete strtemp;
				strtemp = NULL;
			}
		}
		break;
	case AJA_PLAY_SCREEN:
		{
			CString strState = _T("Send Finish");

			packetheader.bodysize = strState.GetLength()*sizeof(char);
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			memset(bSendbuf,0,packetheader.bodysize);
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char* strtemp = ESMUtil::CStringToChar(strState);
			memcpy(pBodyData,strtemp,sizeof(char)*strState.GetLength());
			packetheader.command = AJA_PLAY_SCREEN;
			packetheader.param1 = pMsg->nParam1;

			if(strtemp)
			{
				delete strtemp;
				strtemp = NULL;
			}
			G4DShowLog(5,strState);
		}
		break;
	}
	strcpy(packetheader.protocol, "RCP1.0");
	memcpy(bSendbuf,&packetheader,sizeof(RCPHeader));

	int sent = 0;
	int length = sizeof(RCPHeader) + packetheader.bodysize;
	
	if(m_sock)
		sent = Send(bSendbuf, length);
}
int C4DSDITCPProcess::Send(const char* sendbuf, int buflen)
{
	if( m_sock == NULL)
		return 0;
	int sent = send(m_sock1, sendbuf, buflen, 0);
	if(SOCKET_ERROR == sent)
	{
		G4DShowLog(0,_T("SendError"));
		return (-WSAGetLastError());
	}

	return sent;
}
void C4DSDITCPProcess::ChangeConnectState(BOOL b)
{
	if(b == FALSE)
	{
		m_pParent->m_bCheckDivPlay = FALSE;
		m_pParent->CheckDlgButton(IDC_CHECK_DIVPLAY,FALSE);
	}
}
unsigned WINAPI C4DSDITCPProcess::_RunAcceptThread(LPVOID param)
{
	C4DSDITCPProcess* TCPProcess = (C4DSDITCPProcess*)param;
	SOCKET mainsock = TCPProcess->m_sock;  
	SOCKADDR_IN cliaddr={0};

	G4DShowLog(5,_T("Accpet Thread Start..."));
	while(1)
	{
		int len = sizeof(cliaddr);
		SOCKET *dosock = new SOCKET; 
		*dosock = accept(mainsock,(SOCKADDR *)&cliaddr,&len);//연결 수락

		if(*dosock == -1)
		{
			perror("Accept 실패");
			continue;
		}
		
		TRACE("%s:%d의 연결 요청 수락\n",inet_ntoa(cliaddr.sin_addr),ntohs(cliaddr.sin_port));
		/*if(nCnt == 0)
		{
			SendTCPData();
		}*/
		CString strIP = TCPProcess->CharToCString(inet_ntoa(cliaddr.sin_addr));

		if(strIP == _T("0.0.0.0"))
			continue;

		int nIP = TCPProcess->GetLastIP(strIP);
		THREAD_RECEIVE* pReceive = new THREAD_RECEIVE;
		pReceive->TCPProcess				= TCPProcess;
		pReceive->sock							= dosock;
		pReceive->nIP								= nIP;

		if(strIP == G4DGetDistributeIP())
		{
			pReceive->bDiv						= TRUE;
			TCPProcess->SetSocket(*dosock);	
			TCPProcess->SetConnect(TRUE);
		}
		else
		{
			pReceive->bDiv						= FALSE;
		}
		TCPProcess->SendBoardState(*dosock,pReceive->bDiv);

		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)_beginthreadex(NULL,0,TCPProcess->_RunReceiveThread,pReceive,0,NULL);
		CloseHandle(hSyncTime);
		//TCPProcess->SetSocket(dosock);
	}

	G4DShowLog(5,_T("Accpet Thread Finish..."));

	return TRUE;
}
unsigned WINAPI C4DSDITCPProcess::_RunReceiveThread(LPVOID param)
{
	THREAD_RECEIVE* pReceive = (THREAD_RECEIVE*)param;
	C4DSDITCPProcess*TCPProcess = pReceive->TCPProcess;
	SOCKET* dosock		= pReceive->sock;
	BOOL bDiv				= pReceive->bDiv;
	int nIP						= pReceive->nIP;
	delete pReceive; pReceive = NULL;
	CString strIP;
	strIP.Format(_T("[%d]"),nIP);
	G4DShowLog(2,_T("Connect")+strIP);
	while(1)
	{
		if(TCPProcess->ReceiveData(*dosock,bDiv) == FALSE)
			break;
	}
	TCPProcess->Disconnect(*dosock);

	if(bDiv)
		TCPProcess->SetConnect(FALSE);

	delete dosock;
	G4DShowLog(0,_T("Disconnect")+strIP);

	return TRUE;
}
int C4DSDITCPProcess::GetLastIP(CString strIP)
{
	int nIP = 0;

	int nFind = strIP.ReverseFind('.');
	CString strLastIP = strIP.Right(strIP.GetLength() - nFind - 1);

	nIP = _ttoi(strLastIP);

	return nIP;
}
void C4DSDITCPProcess::SendBoardState(SOCKET sock,BOOL bDiv)
{
	CString strState;
	if(G4DGetBoardState() == TRUE)
		strState.Format(_T("Connection Success"));
	else
		strState.Format(_T("Connection Fail..."));
	
	char* bSendbuf = NULL;
	RCPHeader packetheader;
	char* pBodyData = NULL, *pTpData = NULL;
	int nTpData1 = 0, nTpData2 = 0; 

	
	packetheader.bodysize = strState.GetLength()*sizeof(char);
	bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
	memset(bSendbuf,0,packetheader.bodysize);
	pBodyData = bSendbuf + sizeof(RCPHeader);
	char* strtemp = ESMUtil::CStringToChar(strState);
	memcpy(pBodyData,strtemp,sizeof(char)*strState.GetLength());
	packetheader.command = AJA_BOARDSTATE;
	packetheader.param1  = m_pParent->m_bScreen;
	packetheader.param2 = bDiv;

	if(strtemp)
	{
		delete strtemp;
		strtemp = NULL;
	}
	strcpy(packetheader.protocol, "RCP1.0");
	memcpy(bSendbuf,&packetheader,sizeof(RCPHeader));

	int length = sizeof(RCPHeader) + packetheader.bodysize;

	if(m_sock)
	{
		int sent = send(sock, bSendbuf, length, 0);
		if(SOCKET_ERROR == sent)
		{
			G4DShowLog(0,_T("SendError"));
			return ;
		}
	}
}
void C4DSDITCPProcess::Disconnect(SOCKET sock)
{
	closesocket(sock);
}