#pragma once


// C4DReplaySDIPlayerOptDlg 대화 상자입니다.

class C4DReplaySDIPlayerOptDlg : public CDialogEx
{
	DECLARE_DYNAMIC(C4DReplaySDIPlayerOptDlg)

public:
	C4DReplaySDIPlayerOptDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~C4DReplaySDIPlayerOptDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_OPT };

	void Init(AJAOption* Opt);
	AJAOption* m_pOpt;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOptBtnInfo();
	afx_msg void OnBnClickedOptBtnOutput();
	afx_msg void OnBnClickedOptBtnLog();
	afx_msg void OnBnClickedOptBtnBack();
	afx_msg void OnBnClickedOptBtnLogo();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
};
