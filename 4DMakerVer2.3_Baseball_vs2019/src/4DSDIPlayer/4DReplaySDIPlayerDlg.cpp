#pragma once
#include "stdafx.h"
#include "4DReplaySDIPlayer.h"
#include "4DReplaySDIPlayerDlg.h"
#include "4DReplaySDIPlayerFunc.h"
#include "ESMTCPSocket.h"
#include "ESMIni.h"
#include "ESMFileOperation.h"
#include "afxdialogex.h"
#include "4DReplayGPUProcess.h"
#include "ESMIni.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

C4DReplaySDIPlayerDlg::C4DReplaySDIPlayerDlg(CWnd* pParent /*=NULL*/,C4DReplaySDIPlayerLog* pLog)
	: CDialogEx(C4DReplaySDIPlayerDlg::IDD, pParent)
	, m_nPlayMethod(0)
	, m_bInsertLogo(FALSE)
	, m_bScreen(FALSE)
	, m_bLogoSave(FALSE)
	, m_bEncode(FALSE)
	, m_bTransTS(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_cList = new C4DReplaySDIPlayerList;
	//m_cLog = new C4DReplaySDIPlayerLog;
	m_cLog = pLog;
	m_pTCP	  = new C4DSDITCPProcess(this);

	m_pBoard	 = NULL;
	m_bBoardState = FALSE;
	m_bListViewOpen = FALSE;
	m_bPauseState	= FALSE;
	m_strLoadPath = _T("F:\\RecordSave\\Output\\2D");
	m_bCheckDivPlay = FALSE;
	m_bAutoAdd = FALSE;
	m_bAutoAddDlgOpen = FALSE;
	m_bAutoAddThread = FALSE;
}
C4DReplaySDIPlayerDlg::~C4DReplaySDIPlayerDlg()
{
	SaveOption(&m_stAJAOpt);
	G4DShowLog(5,_T("Finish"));
	DeletePointer();
}
void C4DReplaySDIPlayerDlg::DeletePointer()
{
	if(m_cList)
	{
		delete m_cList;
		m_cList = NULL;
	}
	if(m_pDecodeMgr)
	{
		m_pDecodeMgr->DoStopAllThread();
		m_pDecodeMgr->ShowBackGroundAsBlack();

		delete m_pDecodeMgr;
		m_pDecodeMgr = NULL;
	}
	if(m_pBoard)
	{
		delete m_pBoard;
		m_pBoard = NULL;
	}
	if(m_pTCP)
	{
		delete m_pTCP;
		m_pTCP = NULL;
	}
	/*if(m_cLog)
	{
		delete m_cLog;
		m_cLog = NULL;
	}*/
}
void C4DReplaySDIPlayerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RADIO_PLAY_ONCE, (int&)m_nPlayMethod);
	DDX_Control(pDX,IDC_LISTCTRL_MOVIE_LIST,*m_cList);
	DDX_Control(pDX,IDC_LISTCTRL_LOG,*m_cLog);
	DDX_Control(pDX, IDC_IPADDRESS1, m_ctrlServerIP);
	DDX_Control(pDX, IDC_COMBO_FRAME_SPEED, m_cbFrameSpeed);
}

BEGIN_MESSAGE_MAP(C4DReplaySDIPlayerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//ON_BN_CLICKED(IDC_BTN_LOADMOVIELIST, &C4DReplaySDIPlayerDlg::OnBnClickedBtnLoadmovielist)
	ON_WM_SIZE()
	ON_WM_MOUSEMOVE()
	//ON_BN_CLICKED(IDC_BTN_LOADPROCESS, &C4DReplaySDIPlayerDlg::OnBnClickedBtnLoadprocess)
	ON_BN_CLICKED(IDC_BUTTON_STOP, &C4DReplaySDIPlayerDlg::OnBnClickedButtonStop)
	ON_BN_CLICKED(IDC_BUTTON_PLAYFILE, &C4DReplaySDIPlayerDlg::OnBnClickedButtonPlayfile)
	ON_BN_CLICKED(IDC_BUTTON_ADDMOVIE, &C4DReplaySDIPlayerDlg::OnBnClickedButtonAddmovie)
	ON_BN_CLICKED(IDC_BUTTON_REMOVEMOVIE, &C4DReplaySDIPlayerDlg::OnBnClickedButtonRemovemovie)
	ON_CONTROL_RANGE(BN_CLICKED,IDC_RADIO_PLAY_ONCE,IDC_RADIO_PLAY_ALL, &C4DReplaySDIPlayerDlg::OnBnClickedRadioPlayOnce)
	ON_BN_CLICKED(IDC_CHECK_DIVPLAY, &C4DReplaySDIPlayerDlg::OnBnClickedCheckDivplay)
	ON_BN_CLICKED(IDC_BUTTON_OPTION, &C4DReplaySDIPlayerDlg::OnBnClickedButtonOption)
	ON_BN_CLICKED(IDC_BUTTON_RECONNECT, &C4DReplaySDIPlayerDlg::OnBnClickedButtonReconnect)
	ON_BN_CLICKED(IDC_CHECK_INSERT_LOGO, &C4DReplaySDIPlayerDlg::OnBnClickedCheckInsertLogo)
	ON_EN_CHANGE(IDC_EDIT_WAIT_TIME, &C4DReplaySDIPlayerDlg::OnEnChangeEditWaitTime)
	ON_CBN_SELCHANGE(IDC_COMBO_FRAME_SPEED, &C4DReplaySDIPlayerDlg::OnCbnSelchangeComboFrameSpeed)
	ON_EN_CHANGE(IDC_EDIT_FRAME_SPEED, &C4DReplaySDIPlayerDlg::OnEnChangeEditFrameSpeed)
	ON_NOTIFY(IPN_FIELDCHANGED, IDC_IPADDRESS1, &C4DReplaySDIPlayerDlg::OnIpnFieldchangedIpaddress1)
	ON_BN_CLICKED(IDC_CHECK_SCREENSHOT, &C4DReplaySDIPlayerDlg::OnBnClickedCheckScreenshot)
//	ON_WM_CREATE()
ON_BN_CLICKED(IDC_CHECK_LOGO_SAVE, &C4DReplaySDIPlayerDlg::OnBnClickedCheckLogoSave)
ON_BN_CLICKED(IDC_CHECK_ENCODE, &C4DReplaySDIPlayerDlg::OnBnClickedCheckEncode)
ON_BN_CLICKED(IDC_CHECK_AUTOADD, &C4DReplaySDIPlayerDlg::OnBnClickedCheckAutoadd)
ON_BN_CLICKED(IDC_BUTTON_AUTOADD, &C4DReplaySDIPlayerDlg::OnBnClickedButtonAutoadd)
ON_EN_CHANGE(IDC_EDIT_ENCODE_BITRATE, &C4DReplaySDIPlayerDlg::OnEnChangeEditEncodeBitrate)
ON_BN_CLICKED(IDC_CHECK_TRANSTS, &C4DReplaySDIPlayerDlg::OnBnClickedCheckTransts)
END_MESSAGE_MAP()

void C4DReplaySDIPlayerDlg::InitializeOption(AJAOption* Opt)
{
	UpdateData(TRUE);
	CString strPath;
	strPath.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\%s"),_T("AJAInfo.ini"));

	BOOL bCreate = FALSE;
	CESMFileOperation fo;
	if(!fo.IsFileExist(strPath))
	{
		CFile file;
		file.Open(strPath,CFile::modeCreate);
		file.Close();
		bCreate  =  TRUE;
	}
	
	CESMIni ini;
	if(bCreate)
	{
		if(ini.SetIniFilename(strPath))
		{
			ini.WriteString(OPT_SECTION,OPT_SECTION_INFO					,_T("C:\\Program Files\\ESMLab\\4DMaker"));
			ini.WriteString(OPT_SECTION,OPT_SECTION_OUTPUTPATH	,_T(""));
			ini.WriteString(OPT_SECTION,OPT_SECTION_LOGPATH			,_T("C:\\Program Files\\ESMLab\\4DMaker\\Log"));
			ini.WriteString(OPT_SECTION,OPT_SECTION_BACKPATH		,_T("C:\\Program Files\\ESMLab\\4DMaker\\Img\\4DBack.png"));
			ini.WriteString(OPT_SECTION,OPT_SECTION_LOGOPATH		,_T("C:\\Program Files\\ESMLab\\4DMaker\\Img\\Logo.png"));
			ini.WriteString(OPT_SECTION,OPT_SECTION_IP							,Opt->strIP);
			
			ini.WriteInt(OPT_SECTION,OPT_SECTION_PLAYMETHOD		,0);
			ini.WriteInt(OPT_SECTION,OPT_SECTION_INSERTLOGO			,0);
			ini.WriteInt(OPT_SECTION,OPT_SECTION_WAITTIME					,4000);
			ini.WriteInt(OPT_SECTION,OPT_SECTION_FRAMEMETHOD	,4);
			ini.WriteString(OPT_SECTION,OPT_SECTION_FRAMECOUNT	,_T("33.3667"));

			Opt->strInfo			 = ini.GetString(OPT_SECTION,OPT_SECTION_INFO					);
			Opt->strOutput		 = ini.GetString(OPT_SECTION,OPT_SECTION_OUTPUTPATH	);
			Opt->strLog			 = ini.GetString(OPT_SECTION,OPT_SECTION_LOGPATH			);
			Opt->strBack			 = ini.GetString(OPT_SECTION,OPT_SECTION_BACKPATH		);
			Opt->strLogo			 = ini.GetString(OPT_SECTION,OPT_SECTION_LOGOPATH		);
			Opt->strIP				 = ini.GetString(OPT_SECTION,OPT_SECTION_IP						);


			Opt->nPlayMethod	= ini.GetInt(OPT_SECTION,OPT_SECTION_PLAYMETHOD		);
			m_nPlayMethod		= Opt->nPlayMethod;
			Opt->bInsertLogo		= ini.GetInt(OPT_SECTION,OPT_SECTION_INSERTLOGO		);
			m_bInsertLogo			= Opt->bInsertLogo;
			Opt->nWaitTime		= ini.GetInt(OPT_SECTION,OPT_SECTION_WAITTIME				);
			Opt->nFrameMethod= ini.GetInt(OPT_SECTION,OPT_SECTION_FRAMEMETHOD	);
			CString strFrameRate = ini.GetString(OPT_SECTION,OPT_SECTION_FRAMECOUNT);
			Opt->dbFrameRate		= _wtof(strFrameRate);

			//181018 hjcho
			if(m_bInsertLogo == FALSE)
				GetDlgItem(IDC_CHECK_LOGO_SAVE) -> EnableWindow(FALSE);
			else
			{
				m_bLogoSave = ini.GetInt(OPT_SECTION,OPT_SECTION_LOGOSAVE);
				
				if(m_bLogoSave)
					CheckDlgButton(IDC_CHECK_LOGO_SAVE,TRUE);
				else
					CheckDlgButton(IDC_CHECK_LOGO_SAVE,FALSE);
			}
		}
	}
	else
	{
		if(ini.SetIniFilename(strPath))
		{
			Opt->strInfo			 = ini.GetString(OPT_SECTION,OPT_SECTION_INFO);
			Opt->strOutput			 = ini.GetString(OPT_SECTION,OPT_SECTION_OUTPUTPATH);
			Opt->strLog				 = ini.GetString(OPT_SECTION,OPT_SECTION_LOGPATH);
			Opt->strBack			 = ini.GetString(OPT_SECTION,OPT_SECTION_BACKPATH);
			Opt->strLogo			 = ini.GetString(OPT_SECTION,OPT_SECTION_LOGOPATH);
			Opt->strIP				 = ini.GetString(OPT_SECTION,OPT_SECTION_IP);


			Opt->nPlayMethod		 = ini.GetInt(OPT_SECTION,OPT_SECTION_PLAYMETHOD);
			m_nPlayMethod			 = Opt->nPlayMethod;
			Opt->bInsertLogo		 = ini.GetInt(OPT_SECTION,OPT_SECTION_INSERTLOGO);
			m_bInsertLogo			 = Opt->bInsertLogo;
			Opt->nWaitTime			 = ini.GetInt(OPT_SECTION,OPT_SECTION_WAITTIME);
			Opt->nFrameMethod		 = ini.GetInt(OPT_SECTION,OPT_SECTION_FRAMEMETHOD);
			CString strFrameRate	 = ini.GetString(OPT_SECTION,OPT_SECTION_FRAMECOUNT);
			Opt->dbFrameRate		 = _wtof(strFrameRate);
			Opt->bEncode			 = ini.GetInt(OPT_SECTION,OPT_SECTION_ENCODE);
			m_bEncode				 = Opt->bEncode;

			Opt->bTransTS			 = ini.GetInt(OPT_SECTION,OPT_ENCODE_TS);
			m_bTransTS				 = Opt->bTransTS;

			//181018 hjcho
			if(m_bInsertLogo == FALSE)
				GetDlgItem(IDC_CHECK_LOGO_SAVE) -> EnableWindow(FALSE);
			else
			{
				m_bLogoSave = ini.GetInt(OPT_SECTION,OPT_SECTION_LOGOSAVE);

				if(m_bLogoSave)
					CheckDlgButton(IDC_CHECK_LOGO_SAVE,TRUE);
				else
					CheckDlgButton(IDC_CHECK_LOGO_SAVE,FALSE);
			}

			int nSize				= ini.GetInt(OPT_SECTION_IP,OPT_IP_SIZE);
			for(int i = 0 ; i < nSize ; i++)
			{
				CString strSection;
				strSection.Format(_T("%s_%d"),OPT_IP_LIST,i);


				CString str;
				str = ini.GetString(OPT_SECTION_IP,strSection);
				Opt->ArrIP.push_back(str);
			}

			Opt->nBitrate				= ini.GetInt(OPT_SECTION,OPT_ENCODE_BITRATE);
			m_nEncodeBitrate		= Opt->nBitrate;
		}
	}
	/*CESMIni ini;

	CFile file;
	if(file.Open(strPath,CFile::modeReadWrite))
	{
		
	}
	else
	{
		G4DShowLog(0,_T("Option Read Fail... Making temp Option Data"));
		if(file.Open(strPath,CFile::modeCreate | CFile::modeReadWrite))
		{
			G4DShowLog(5,_T("Option file make sucess"));
		}
	}*/

	UpdateData(FALSE);
}
void C4DReplaySDIPlayerDlg::SaveOption(AJAOption* Opt)
{
	CString strPath;
	strPath.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\%s"),_T("AJAInfo.ini"));

	CESMIni ini;
	if(ini.SetIniFilename(strPath))
	{
		ini.WriteString(OPT_SECTION,OPT_SECTION_INFO		,Opt->strInfo);
		ini.WriteString(OPT_SECTION,OPT_SECTION_OUTPUTPATH	,Opt->strOutput);
		ini.WriteString(OPT_SECTION,OPT_SECTION_LOGPATH		,Opt->strLog);
		ini.WriteString(OPT_SECTION,OPT_SECTION_BACKPATH	,Opt->strBack);
		ini.WriteString(OPT_SECTION,OPT_SECTION_LOGOPATH	,Opt->strLogo);

		//
		ini.WriteString(OPT_SECTION,OPT_SECTION_IP			,m_strServerIP);
		ini.WriteString(OPT_SECTION,OPT_SECTION_FRAMECOUNT	,m_strFPS);
		ini.WriteInt(OPT_SECTION,OPT_SECTION_WAITTIME		,m_pDecodeMgr->m_nWaitTime);
		ini.WriteInt(OPT_SECTION,OPT_SECTION_FRAMEMETHOD	,m_nFrameSel);
		ini.WriteInt(OPT_SECTION,OPT_SECTION_PLAYMETHOD		,m_nPlayMethod);
		ini.WriteInt(OPT_SECTION,OPT_SECTION_INSERTLOGO		,m_bInsertLogo);
		ini.WriteInt(OPT_SECTION,OPT_SECTION_ENCODE			,m_bEncode);

		ini.WriteInt(OPT_SECTION_IP,OPT_IP_SIZE				,Opt->ArrIP.size());

		ini.WriteInt(OPT_SECTION,OPT_ENCODE_TS,m_bTransTS);

		for(int i = 0 ; i < Opt->ArrIP.size() ; i++)
		{
			CString str;
			str.Format(_T("%s_%d"),OPT_IP_LIST,i);
			ini.WriteString(OPT_SECTION_IP,str,Opt->ArrIP.at(i));
		}

		ini.WriteInt(OPT_SECTION,OPT_ENCODE_BITRATE, m_nEncodeBitrate);
	}
}
BOOL C4DReplaySDIPlayerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	//Option read..
	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	InitializeOption(&m_stAJAOpt);

	//Log
	m_cLog->Init(m_stAJAOpt.strLog);
	G4DShowLog(5,_T("Create Log..."));

	SetIcon(m_hIcon, TRUE);	
	SetIcon(m_hIcon, FALSE);
	
	//Upload Board
	SetBoardState(StartBoardConnection());

	m_strServerIP = m_stAJAOpt.strIP;
	char pchPath[MAX_PATH] = {0,};
	int nRequiredSize = (int)wcstombs(pchPath, m_strServerIP, MAX_PATH);
	m_ctrlServerIP.SetAddress(htonl(inet_addr(pchPath)));

	/*CRect rect;
	GetWindowRect(&m_rcClientRect);
	GetDlgItem(IDC_BTN_LOADMOVIELIST)->GetWindowRect(&m_rcListRect);
	GetDlgItem(IDC_BTN_LOADMOVIELIST)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BTN_LOADPROCESS)->GetWindowRect(&m_rcProcessRect);
	GetDlgItem(IDC_BTN_LOADPROCESS)->ShowWindow(SW_HIDE);

	SetWindowPos(NULL,0,0,m_rcListRect.right,m_rcListRect.bottom,SWP_NOMOVE);*/

	//List Control Load
	//DecodingMgr
	if(GetBoardState() == TRUE)
	{
		G4DShowLog(5,_T("Board Load Success!"));
		GetDlgItem(IDC_STATIC_BOARDSTATE)->SetWindowTextW(_T("O"));

		m_pDecodeMgr = new C4DReplayDecodeMgr(&m_stAJAOpt);
		m_pDecodeMgr->Init(this,m_pBoard);
		m_pDecodeMgr->ShowBackGroundAsOurs();
	}
	else
	{
		G4DShowLog(0,_T("Board Load Fail!"));

		GetDlgItem(IDC_STATIC_BOARDSTATE)->SetWindowTextW(_T("X"));
		m_pDecodeMgr = new C4DReplayDecodeMgr;
		m_pDecodeMgr->Init(this);
	}

	CString strLog;
	strLog.Format(_T("Play method: %d"),m_nPlayMethod);
	G4DShowLog(5,strLog);

	m_pDecodeMgr->SetPlayMethod(m_nPlayMethod);

	if(m_nPlayMethod == 2)
	{
		m_cList->SetAllPlayState(TRUE);
		m_cList->RunAllPlay();
	}
	else
		m_cList->SetAllPlayState(FALSE);

	if(m_bInsertLogo)
	{
		CheckDlgButton(IDC_CHECK_INSERT_LOGO,TRUE);
		GetDlgItem(IDC_CHECK_LOGO_SAVE)->EnableWindow(TRUE);
		G4DSetLogoSave(m_bLogoSave);		
	}
	else
	{
		CheckDlgButton(IDC_CHECK_INSERT_LOGO,FALSE);
		GetDlgItem(IDC_CHECK_LOGO_SAVE)->EnableWindow(FALSE);
		m_bLogoSave = FALSE;
	}

	G4DSetInsertLogo(m_bInsertLogo);

	m_cList->Init(this,m_pDecodeMgr);
	G4DShowLog(5,_T("List Load Success!"));
	m_pDecodeMgr->SetPlayMethod(m_nPlayMethod);
	G4DShowLog(5,_T("Decoder Load Success!"));

	CString strOpt;
	strOpt.Format(_T("%d"),m_stAJAOpt.nWaitTime);
	GetDlgItem(IDC_EDIT_WAIT_TIME)->SetWindowText(strOpt);

	strOpt.Format(_T("%f"),m_stAJAOpt.dbFrameRate);
	GetDlgItem(IDC_EDIT_FRAME_SPEED)->SetWindowText(strOpt);
	//GetDlgItem(IDC_EDIT_FRAME_SPEED)->EnableWindow(FALSE);

	m_cbFrameSpeed.AddString(_T("NTSC 2398"));
	m_cbFrameSpeed.AddString(_T("NTSC 2400"));
	m_cbFrameSpeed.AddString(_T("NTSC 2997"));
	m_cbFrameSpeed.AddString(_T("NTSC 3000"));
	m_cbFrameSpeed.AddString(_T("NTSC 5994"));
	m_cbFrameSpeed.AddString(_T("NTSC 6000"));
	m_cbFrameSpeed.AddString(_T("PAL 2500"));
	m_cbFrameSpeed.AddString(_T("PAL 5000"));
	m_cbFrameSpeed.AddString(_T("Custom"));
	
	SetFrameSpeed(m_stAJAOpt.nFrameMethod);
	G4DShowLog(5,_T("Set FrameSpeed Finish!"));

	G4DShowLog(5,_T("GPULoading..."));
	LaunchGPUThread();

	if(m_bEncode)
	{
		CheckDlgButton(IDC_CHECK_ENCODE,TRUE);
		if(m_bTransTS)
			CheckDlgButton(IDC_CHECK_TRANSTS,TRUE);
	}
	else
	{
		GetDlgItem(IDC_CHECK_TRANSTS)->EnableWindow(FALSE);
		CheckDlgButton(IDC_CHECK_TRANSTS,FALSE);
	}
	if(m_nEncodeBitrate > 0)
	{
		CString strBit;
		strBit.Format(_T("%d"),m_nEncodeBitrate);
		GetDlgItem(IDC_EDIT_ENCODE_BITRATE)->SetWindowText(strBit);
	}
	else
	{
		m_nEncodeBitrate = 5;
		GetDlgItem(IDC_EDIT_ENCODE_BITRATE)->SetWindowText(_T("5"));
	}

	GetDlgItem(IDC_BUTTON_AUTOADD)->EnableWindow(FALSE);

	return TRUE;
}

BOOL C4DReplaySDIPlayerDlg::StartBoardConnection()
{
	AJAStatus status = AJA_STATUS_FAIL;
	if(AJA_FAILURE(status))
	{
		status = ConnectBoard();

		if(AJA_SUCCESS(status))
			status = AJA_STATUS_SUCCESS;

		if(AJA_SUCCESS(status))
		{
			/*m_nSettedHeight = m_Process->nHeight;
			m_nSettedWidth = m_Process->nWidth;

			m_mYUVBackGround.create(m_nSettedHeight,m_nSettedWidth*2,CV_8UC1);

			if(m_mBackGround.empty())
				return TRUE;

			BGR2YUV(m_mBackGround,m_mYUVBackGround,m_nSettedHeight,m_nSettedWidth);
			m_Process->EmitPattern(m_mYUVBackGround,0,0);

			m_bBoardState = TRUE;
			GetDlgItem(IDC_STATIC_BOARDSTATE)->SetWindowText(_T("OK"));
			*/
			return TRUE;
		}
		else
			return FALSE;
	}
	else
		return FALSE;
}
AJAStatus C4DReplaySDIPlayerDlg::ConnectBoard()
{
	AJAStatus	status				(AJA_STATUS_SUCCESS);
	uint32_t	deviceIndex			(0);
	uint32_t	channelNumber		(1);
	uint32_t	testPatternIndex	(0);					//	Which test pattern to display

	const NTV2Channel	channel	(static_cast <NTV2Channel> (channelNumber - 1));
	if (!IS_VALID_NTV2Channel (channel))
	{
		cerr << "## ERROR:  Invalid channel number " << channelNumber << " -- expected 1 thru 8" << endl;
		AfxMessageBox(_T("AAAAAAAAAAAAAA"));
	}

	m_pBoard = new NTV2OutputTestPattern(deviceIndex,channel);
	if(AJA_SUCCESS(status))
		status = m_pBoard->Init();

	//if(status == AJA_STATUS_BUSY)
	//{
	//	//m_Process->
	//}

	if(AJA_SUCCESS(status))
		status = m_pBoard->SetOperation();

	return status;
}
void C4DReplaySDIPlayerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}
HCURSOR C4DReplaySDIPlayerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
void C4DReplaySDIPlayerDlg::InsertMovieData(CString strPath,vector<MakeFrameInfo>*pArrFrameInfo,int nIdx,BOOL bCPU,BOOL bDiv,BOOL bSet,int nFinish)
{
	if(m_bScreen == TRUE)
	{
		G4DShowLog(0,_T("Shut down screen view First!!!!!"));
		return;
	}
	else
	{
		//Kill Thread
		if(m_pDecodeMgr->GetDivState() == FALSE || bDiv == FALSE)
		{
			m_pDecodeMgr->SetMovieSignal(MOVIE_STOP);
			/*for(int i = 0 ; i < 15 ; i++)
			{
				
				Sleep(30);
			}*/
		}

		//Wait Thread until killing thread
		if(bDiv == FALSE || m_pDecodeMgr->GetDivState() == FALSE)
		{
			if(m_pDecodeMgr->GetDivState() == TRUE)
			{
				while(1)
				{
					Sleep(1);
					if(m_pDecodeMgr->GetPlayDivFrame() == FALSE)
						break;
				}
			}
			if(m_pDecodeMgr->GetDecodeState() == TRUE ||
				m_pDecodeMgr->GetPlayState() == TRUE)
			{
				while(1)
				{
					if(m_pDecodeMgr->GetDecodeState() == FALSE &&
						m_pDecodeMgr->GetPlayState() == FALSE)
						break;

					Sleep(10);
				}
			}
			m_pDecodeMgr->SetPlayState(MOVIE_PLAY);
		}
		/*else if(bDiv == TRUE || m_pDecodeMgr->GetPlayState() == TRUE)
		{
			m_pDecodeMgr->SetMovieSignal(MOVIE_STOP);

			while(1)
			{
				if(m_pDecodeMgr->GetDecodeState() == FALSE &&
					m_pDecodeMgr->GetPlayState() == FALSE)
					break;

				Sleep(1);
			}
		}*/

		if(bDiv == FALSE)
		{
			DoFileDecode(strPath,0);
		}
		else
		{
			m_pDecodeMgr->SetDivState(TRUE);
			if(bCPU)
			{
				if(bSet == FALSE)
					G4DSetCPUMakingIdx(nIdx);
				else
					if(m_pDecodeMgr->GetPlayDivFrame() == TRUE)
						G4DSetStartDecoding(strPath,nIdx,nFinish);
			}
			else
			{
				G4DMakeFrameInfo(pArrFrameInfo,nIdx);
			}
		}
	}
}
void C4DReplaySDIPlayerDlg::DoFileDecode(CString strPath,int nIndex,BOOL bDivFrame)
{
#if 1
	//Board State
	if(GetBoardState() == FALSE)
	{
		AfxMessageBox(_T("Board Load First!"));
		return;
	}

	if(bDivFrame == FALSE)
	{
		//State Update
		m_strPlayTime = m_cList->GetItemText(nIndex,LIST_FILETIME);
		m_strPlayFileName = strPath;

		CString strLog;
		strLog.Format(_T("Play %s"),strPath);
		G4DShowLog(5,strLog);

		//AddPath
		m_pDecodeMgr->SelectFileDecode(strPath);
	}
	else
	{
			//DoDivFrames
	}
	
#else
	if(m_bCheckDivPlay)
	{
		m_pDivMgr;
	}
	
#endif
	
}
void C4DReplaySDIPlayerDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);
}
void C4DReplaySDIPlayerDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//if(point.x >= m_rcListRect.left && point.x < m_rcListRect.right)
	//	GetDlgItem(IDC_BTN_LOADMOVIELIST)->ShowWindow(SW_SHOW);
	//else
	//	GetDlgItem(IDC_BTN_LOADMOVIELIST)->ShowWindow(SW_HIDE);

	//if(m_bListViewOpen)
	//{
	//	CRect rc;
	//	GetDlgItem(IDC_BTN_LOADPROCESS)->GetClientRect(&rc);
	//	
	//	/*TRACE1("%d\t",point.y + 25);
	//	TRACE2("%d - %d\n",m_rcProcessRect.top,m_rcProcessRect.bottom);*/
	//	
	//	if(point.y + 25 >= m_rcProcessRect.top  && point.y + 25 < m_rcProcessRect.bottom)
	//		GetDlgItem(IDC_BTN_LOADPROCESS)->ShowWindow(SW_SHOW);
	//	else
	//		GetDlgItem(IDC_BTN_LOADPROCESS)->ShowWindow(SW_HIDE);
	//}
	CDialogEx::OnMouseMove(nFlags, point);
}
void C4DReplaySDIPlayerDlg::OnBnClickedButtonStop()
{
	if(G4DGetEncodingState() == TRUE)
	{
		G4DShowLog(0,_T("Cannot stop while making"));

		return;
	}

	m_pDecodeMgr->SetMovieSignal(MOVIE_STOP);
	G4DShowLog(5,_T("OnBnClickedButtonStop"));
}
void C4DReplaySDIPlayerDlg::OnBnClickedButtonPlayfile()
{
	if(m_pDecodeMgr->GetPlayState() == TRUE)
	{
		if(!m_bPauseState)
		{
			m_pDecodeMgr->SetMovieSignal(MOVIE_PAUSE);
			GetDlgItem(IDC_BUTTON_PLAYFILE)->SetWindowTextW(_T("||"));
			m_bPauseState = TRUE;
		}
		else
		{
			m_pDecodeMgr->SetMovieSignal(MOVIE_PLAY);
			GetDlgItem(IDC_BUTTON_PLAYFILE)->SetWindowTextW(_T("▶"));
			m_bPauseState = FALSE;
		}
	}
}
void C4DReplaySDIPlayerDlg::OnBnClickedButtonAddmovie()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(m_strLoadPath);

	CString szFilter = _T("Video File (*.mp4,*.avi,*.wmv)|*.mp4;*.avi;*.wmv|");
	const BOOL bVistaStyle = TRUE; 

	CString strMoviePath;
	CFileDialog dlg(TRUE, NULL, NULL, OFN_ALLOWMULTISELECT, szFilter, NULL, 0, bVistaStyle );  

	const int c_cMaxFiles = 20000;
	const int c_cbBufSize = (c_cMaxFiles * (MAX_PATH + 1)) + 1;

	dlg.GetOFN().lpstrFile = strMoviePath.GetBuffer(c_cbBufSize);
	dlg.GetOFN().nMaxFile  = c_cbBufSize;
	dlg.m_ofn.lpstrTitle = _T("AddMovie");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName,strFilePath;
	if(dlg.DoModal() == IDOK)
	{
		for(POSITION pos=dlg.GetStartPosition(); pos != NULL;)
		{
			strFilePath =dlg.GetNextPathName(pos);

			strFileName = strFilePath.Right(strFilePath.GetLength() - strFilePath.ReverseFind(_T('\\')) -1);

			//m_cList.AddString(strFile);
			m_cList->AddFile(strFileName,strFilePath,TRUE);
		}
	}
	else
		return;
}
void C4DReplaySDIPlayerDlg::OnBnClickedButtonRemovemovie()
{
	m_cList->DeleteSelList();
}
BOOL C4DReplaySDIPlayerDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
		if(pMsg->wParam == !VK_CONTROL)
			return TRUE;

		switch(pMsg->wParam)
		{
		case VK_SPACE:
			{
				OnBnClickedButtonPlayfile();
			}
			break;
		/*case VK_NUMPAD0:
			{
				G4DSendBoardState();
			}
			break;*/
		case 68:
			{
				if(m_pDecodeMgr->GetPlayState() == TRUE && m_bPauseState == TRUE)
				{
					m_pDecodeMgr;
				}
			}
			break;
		case 70:
			{
				if(m_pDecodeMgr->GetPlayState() == TRUE && m_bPauseState == TRUE)
				{
					m_pDecodeMgr;
				}
			}
			break;
		case 0x54://T
			{
				if(m_pDecodeMgr->GetTest())
				{
					m_pDecodeMgr->SetTest(FALSE);
					G4DShowLog(5,_T("TEST Finish"));
				}
				else
				{
					m_pDecodeMgr->SetTest(TRUE);
					G4DShowLog(5,_T("TEST Start"));
				}
			}
			break;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}
void C4DReplaySDIPlayerDlg::OnBnClickedRadioPlayOnce(UINT msg)
{
	UpdateData(TRUE);
	CString strLog;
	strLog.Format(_T("Play method: %d"),m_nPlayMethod);
	G4DShowLog(5,strLog);

	m_pDecodeMgr->SetPlayMethod(m_nPlayMethod);

	if(m_nPlayMethod == 2)
	{
		m_cList->SetAllPlayState(TRUE);
		m_cList->RunAllPlay();
	}
	else
	{
		m_cList->SetAllPlayState(FALSE);
	}
}
CString C4DReplaySDIPlayerDlg::GetIP()
{
	DWORD dwIP;
	m_ctrlServerIP.GetAddress(dwIP);
	m_strServerIP.Format(_T("%d.%d.%d.%d"),
		FIRST_IPADDRESS(dwIP),SECOND_IPADDRESS(dwIP),THIRD_IPADDRESS(dwIP),FOURTH_IPADDRESS(dwIP));

	return m_strServerIP;
}
void C4DReplaySDIPlayerDlg::OnBnClickedCheckDivplay()
{
	//TCP Module
	cv::Mat img;
	if(m_bCheckDivPlay == FALSE)
	{
		m_pTCP->Init();
		m_pTCP->SetIP(GetIP(),1);
		G4DSetDistributeIP(GetIP());
		m_bCheckDivPlay = TRUE;
		m_strIP = GetIP();
	}
	else
	{
		//m_pTCP->SetConnect(FALSE);
		m_pTCP->CloseSocket();
		m_bCheckDivPlay = FALSE;
	}
//#if 1
//	if(m_bCheckDivPlay == FALSE)
//	{
//		//Connection Check From 4DMakerWatcher
//		if(TRUE/*GetConnectionCheck(GetIP())*/)
//		{
//			G4DShowLog(5,_T("Start Div Frame!"));
//			m_bCheckDivPlay = TRUE;
//			GetDlgItem(IDC_IPADDRESS1)->EnableWindow(FALSE);
//			//Read INI & Ready to play
//			m_pDivMgr->StartMoviePlay(m_strServerIP);
//		}
//		else
//		{
//			AfxMessageBox(_T("Check Server IP or\n Turn On 4DMakerWatcher at Server"));
//			m_bCheckDivPlay = FALSE;
//			//GetDlgItem(IDC_CHECK_DIVPLAY)->SetCheck(FALSE);
//			CheckDlgButton(IDC_CHECK_DIVPLAY,FALSE);
//		}
//	}
//	else
//	{
//		m_bCheckDivPlay = FALSE;
//		GetDlgItem(IDC_IPADDRESS1)->EnableWindow(TRUE);
//	}
//#endif
	/*CString strLog;
	strLog.Format(_T("%d"),m_bCheckDivPlay);
	G4DShowLog(5,strLog);*/
}
void C4DReplaySDIPlayerDlg::OnBnClickedButtonOption()
{
	C4DReplaySDIPlayerOptDlg OptDlg;
	OptDlg.Init(&m_stAJAOpt);

	if(OptDlg.DoModal() == IDOK)
	{
		m_pDecodeMgr->SetBackgroundImage(m_stAJAOpt.strBack);
		m_pDecodeMgr->SetLogoImage(m_stAJAOpt.strLogo);
		m_pDecodeMgr->ShowBackGroundAsOurs();
		m_pDecodeMgr->SetSecondPath(m_stAJAOpt.strOutput);
	}
	else
	{

	}
}
void C4DReplaySDIPlayerDlg::OnBnClickedButtonReconnect()
{
	if(m_bBoardState)
		AfxMessageBox(_T("Already Loaded"));
	else
	{
		//m_Process->ReleaseDevice();
		delete m_pBoard;
		m_pBoard = NULL;

		SetBoardState(StartBoardConnection());
		if(GetBoardState())
		{
			GetDlgItem(IDC_STATIC_BOARDSTATE)->SetWindowTextW(_T("O"));
			G4DShowLog(5,_T("Connected success!"));
		}
	}
}
void C4DReplaySDIPlayerDlg::OnBnClickedCheckInsertLogo()
{
	UpdateData(FALSE);
	m_bInsertLogo = IsDlgButtonChecked(IDC_CHECK_INSERT_LOGO);//GetDlgItem(IDC_CHECK_INSERT_LOGO);
	G4DSetInsertLogo(m_bInsertLogo);

	if(m_bInsertLogo == FALSE)
	{
		GetDlgItem(IDC_CHECK_LOGO_SAVE) -> EnableWindow(FALSE);
		CheckDlgButton(IDC_CHECK_LOGO_SAVE,FALSE);
		m_bLogoSave = FALSE;

	}
	else
		GetDlgItem(IDC_CHECK_LOGO_SAVE)->EnableWindow(TRUE);
}
void C4DReplaySDIPlayerDlg::OnEnChangeEditWaitTime()
{
	UpdateData(TRUE);
	CString str ;
	GetDlgItem(IDC_EDIT_WAIT_TIME)->GetWindowText(str);
	int nCnt = _ttoi(str);

	m_pDecodeMgr->m_nWaitTime = nCnt;
	UpdateData(FALSE);
}
void C4DReplaySDIPlayerDlg::SetFrameSpeed(int nIdx)
{
	m_cbFrameSpeed.SetCurSel(nIdx);
	m_nFrameSel = nIdx;

	GetDlgItem(IDC_EDIT_FRAME_SPEED)->EnableWindow(FALSE);

	switch(nIdx)
	{
	case 0://2398
		{
			m_pDecodeMgr->m_dbPlayTime = 41.7014;
			m_strFPS.Format(_T("%.4f"),41.7014);
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->SetWindowText(m_strFPS);
			G4DSetEncodeFrameRate(24);
		}
		break;
	case 1://2400
		{
			m_pDecodeMgr->m_dbPlayTime = 41.6666;
			m_strFPS.Format(_T("%.4f"),41.6666);
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->SetWindowText(m_strFPS);
			G4DSetEncodeFrameRate(24);
		}
		break;
	case 2://2997
	case 4://5994
		{
			m_pDecodeMgr->m_dbPlayTime = 33.3667;
			m_strFPS.Format(_T("%.4f"),33.3667);
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->SetWindowText(m_strFPS);
			G4DSetEncodeFrameRate(30);
		}
		break;
	case 3://3000
	case 5://6000
		{
			m_pDecodeMgr->m_dbPlayTime = 33.3333;
			m_strFPS.Format(_T("%.4f"),33.3333);
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->SetWindowText(m_strFPS);
			G4DSetEncodeFrameRate(30);
		}
		break;
	case 6://2500
	case 7://5000
		{
			m_pDecodeMgr->m_dbPlayTime = 40.0000;
			m_strFPS.Format(_T("%.4f"),40.0000);
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->SetWindowText(m_strFPS);
			G4DSetEncodeFrameRate(25);
		}
		break;
	case 8://Custom
		{
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->EnableWindow(TRUE);
			CString str;
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->GetWindowText(str);
			m_pDecodeMgr->m_dbPlayTime = _wtof(str);

			if(m_pDecodeMgr->m_dbPlayTime > 0)
			{
				int n = 1000 / m_pDecodeMgr->m_dbPlayTime;
				G4DSetEncodeFrameRate(n);
			}
			else
			{
				AfxMessageBox(_T("ERROR SET FRAMERATE <= 0 \nSET FRAME RATE > 0"));
			}
		}
		break;
	}
}
void C4DReplaySDIPlayerDlg::OnCbnSelchangeComboFrameSpeed()
{
	int nIdx = m_cbFrameSpeed.GetCurSel();
	m_nFrameSel = nIdx;
	GetDlgItem(IDC_EDIT_FRAME_SPEED)->EnableWindow(FALSE);
	switch(nIdx)
	{
	case 0://2398
		{
			m_pDecodeMgr->m_dbPlayTime = 41.7014;
			m_strFPS.Format(_T("%.4f"),41.7014);
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->SetWindowText(m_strFPS);
			G4DSetEncodeFrameRate(24);
		}
		break;
	case 1://2400
		{
			m_pDecodeMgr->m_dbPlayTime = 41.6666;
			m_strFPS.Format(_T("%.4f"),41.6666);
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->SetWindowText(m_strFPS);
			G4DSetEncodeFrameRate(24);
		}
		break;
	case 2://2997
	case 4://5994
		{
			m_pDecodeMgr->m_dbPlayTime = 33.3667;
			m_strFPS.Format(_T("%.4f"),33.3667);
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->SetWindowText(m_strFPS);
			G4DSetEncodeFrameRate(30);
		}
		break;
	case 3://3000
	case 5://6000
		{
			m_pDecodeMgr->m_dbPlayTime = 33.3333;
			m_strFPS.Format(_T("%.4f"),33.3333);
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->SetWindowText(m_strFPS);
			G4DSetEncodeFrameRate(30);
		}
		break;
	case 6://2500
	case 7://5000
		{
			m_pDecodeMgr->m_dbPlayTime = 40.0000;
			m_strFPS.Format(_T("%.4f"),40.0000);
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->SetWindowText(m_strFPS);
			G4DSetEncodeFrameRate(25);
		}
		break;
	case 8://Custom
		{
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->EnableWindow(TRUE);
			CString str;
			GetDlgItem(IDC_EDIT_FRAME_SPEED)->GetWindowText(str);
			m_pDecodeMgr->m_dbPlayTime = _wtof(str);
			if(m_pDecodeMgr->m_dbPlayTime > 0)
			{
				int n = 1000 / m_pDecodeMgr->m_dbPlayTime;
				G4DSetEncodeFrameRate(n);
			}
			else
			{
				AfxMessageBox(_T("ERROR SET FRAMERATE <= 0 \nSET FRAME RATE > 0"));
				GetDlgItem(IDC_EDIT_FRAME_SPEED)->SetWindowText(_T("33.3667"));
			}
		}
		break;
	}
}
void C4DReplaySDIPlayerDlg::OnEnChangeEditFrameSpeed()
{
	CString str;
	GetDlgItem(IDC_EDIT_FRAME_SPEED)->GetWindowText(str);
	m_strFPS = str;
	m_pDecodeMgr->m_dbPlayTime = _wtof(str);
}
void C4DReplaySDIPlayerDlg::SetOptInfoString(CString strKeyName,CString strValue)
{
	
}
CString C4DReplaySDIPlayerDlg::GetLocalIPAdress()
{
	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	CString ip;
	PHOSTENT hostinfo;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 ) 
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				ip = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}
		}      
		WSACleanup( );
	} 
	return ip;
}
void C4DReplaySDIPlayerDlg::OnIpnFieldchangedIpaddress1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMIPADDRESS pIPAddr = reinterpret_cast<LPNMIPADDRESS>(pNMHDR);
	DWORD dwIP;
	m_ctrlServerIP.GetAddress(dwIP);
	m_strServerIP.Format(_T("%d.%d.%d.%d"),
		FIRST_IPADDRESS(dwIP),SECOND_IPADDRESS(dwIP),THIRD_IPADDRESS(dwIP),FOURTH_IPADDRESS(dwIP));
	*pResult = 0;
}
void C4DReplaySDIPlayerDlg::OnBnClickedCheckScreenshot()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	if(m_bScreen == FALSE)
	{
		m_bScreen = TRUE;
		m_pDecodeMgr->SetMovieSignal(MOVIE_STOP);
		Sleep(500);
		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)_beginthreadex(NULL,0,_ScreenShotThread,this,0,NULL);
		CloseHandle(hSyncTime);
	}
	else
	{
		m_bScreen = FALSE;
	}

	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = AJA_PLAY_SCREEN;
	pMsg->nParam1 = m_bScreen;
	G4DAddEvent(pMsg);

	UpdateData(FALSE);
}
unsigned WINAPI C4DReplaySDIPlayerDlg::_ScreenShotThread(LPVOID param)
{
	C4DReplaySDIPlayerDlg* pMgr = (C4DReplaySDIPlayerDlg*) param;
	
	Mat YUV422(G4DGetTargetHeight(),G4DGetTargetWidth()*2,CV_8UC1);
	BYTE *pData = NULL;
	int nTotal = G4DGetTargetHeight() * G4DGetTargetWidth() * 4;
	pData = new BYTE[nTotal];
	while(1)
	{
		if(pMgr->m_bScreen == FALSE || pMgr->m_pDecodeMgr->GetPlayState() == MOVIE_STOP)
			break;

		pMgr->m_pDecodeMgr->StartCounter();
		CDC memDC; 
		CBitmap bitmap; 
		int cx, cy;	
		cx = GetSystemMetrics(SM_CXSCREEN); 
		cy = GetSystemMetrics(SM_CYSCREEN);

		CClientDC ScreenDC(GetDesktopWindow());	// 스크린DC 선언 
		memDC.CreateCompatibleDC(&ScreenDC);	// 스크린DC와 호환되는 DC생성 
		bitmap.CreateCompatibleBitmap(&ScreenDC, cx, cy);// 스크린DC와 호환되는 비트맵 생성 
		
		CBitmap* pOldBitmap = memDC.SelectObject(&bitmap);// Bitmap 포인터를 넘겨줌 	
		memDC.StretchBlt(0, 0, cx, cy, &ScreenDC, 0, 0, cx, cy, SRCCOPY);	// 스크린DC에 저장된화면을 memDC에 copy, bitmap에도 기록됨 
		double dbGetScreen = pMgr->m_pDecodeMgr->GetCounter();

		bitmap.GetBitmapBits(nTotal,pData);
		Mat img(1080,1920,CV_8UC4,pData);
		//imshow("img",img);
		//waitKey(0);
		pMgr->m_pDecodeMgr->ScreenToYUV422(pData,YUV422,G4DGetTargetHeight(),G4DGetTargetWidth());
		double dbGetConvert = pMgr->m_pDecodeMgr->GetCounter();// - dbGetScreen;

		pMgr->m_pBoard->EmitPattern(YUV422,dbGetConvert,33);

		//CString strTime;
		//strTime.Format(_T("one frame: %.2f - %.2f"),dbGetScreen,dbGetConvert - dbGetScreen);
		//	// 본래의 비트맵 복구??
		//G4DShowLog(5,strTime);
		//free(pData);
		memDC.SelectObject(pOldBitmap);
	}
	pMgr->m_pDecodeMgr->ShowBackGroundAsOurs();
	return TRUE;
}
void C4DReplaySDIPlayerDlg::LaunchGPUThread()
{
	//m_pGPU = new C4DReplayGPUProcess;
	//m_pGPU->Create(IDD_DIALOG_GPULOAD,this);

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_GPULoadThread,this,0,NULL);
	CloseHandle(hSyncTime);

	//delete m_pGPU;
	//m_pGPU = NULL;
	//C4DReplayGPUProcess* pGpuProc = new C4DReplayGPUProcess;
	//pGpuProc->LaunchModal();

}
unsigned WINAPI C4DReplaySDIPlayerDlg::_GPULoadThread(LPVOID param)
{
	DISPLAY_DEVICE Display;
	WCHAR GraphicCard[128];

	ZeroMemory( GraphicCard, sizeof( GraphicCard ) );
	memset( &Display, 0, sizeof( DISPLAY_DEVICE ) );
	Display.cb = sizeof( Display );

	int nCnt = 0;
	CString strGraphic;// = Display.DeviceString;

	while(1)
	{
		Sleep(1);
		EnumDisplayDevices( NULL, nCnt, &Display, 0 );
		strGraphic = Display.DeviceString;

		if(strGraphic.GetLength() == FALSE)
			break;

		//ESMLog(5,strGraphic);
		if(strGraphic.Find(_T("NVIDIA GeForce")) != -1)
			break;

		if(strGraphic.Find(_T("NVIDIA")) != -1)
			break;

		nCnt++;
	}

	if(strGraphic.GetLength())
	{


		/*HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)_beginthreadex(NULL,0,_LaunchTimeThread,(void*)this,0,NULL);
		CloseHandle(hSyncTime);*/

		//ShowWindow(SW_SHOW);
		G4DShowLog(5,strGraphic);
		cv::Mat frame(1,1,CV_8UC3);
		cv::cuda::GpuMat pMat(frame);
		//ShowWindow(SW_HIDE);
		
		G4DShowLog(0,_T("Graphic Card Load Finish"));
	}
	else
		G4DShowLog(0,_T("No Graphic Card...."));

	/*C4DReplaySDIPlayerDlg* pDlg = (C4DReplaySDIPlayerDlg*) param;
	pDlg->m_pGPU->ShowWindow(SW_SHOW);
	pDlg->m_pGPU->LaunchModal();
	pDlg->m_pGPU->ShowWindow(SW_HIDE);*/

	return FALSE;
}
//BOOL C4DReplaySDIPlayerDlg::GetConnectionCheck(CString strIP)
//{
//	return TRUE;
//	/*char szHostIP[50];
//	char pRecvBuf[1024] = {0};
//	int nPort = 20500;
//
//	char* strId = CStringToChar(strIP);
//	memcpy(szHostIP, strId, sizeof(char) * 50);
//	if( strId)
//	{
//		delete strId;
//		strId = NULL;
//	}
//	CESMTCPSocket* pSocket = new CESMTCPSocket;
//	pSocket->Create(AF_INET, SOCK_STREAM, IPPROTO_TCP, 300);
//	if(pSocket->Connect(szHostIP, nPort, 1) > 0)
//	{
//		pSocket->Send("0", 2);
//		pSocket->Recv(pRecvBuf, 1024,1);
//	}
//	pSocket->Close();
//
//	if(pSocket)
//	{
//		delete pSocket;
//		pSocket = NULL;
//	}
//
//	if(strcmp(pRecvBuf,"1") == 0)
//		return TRUE;
//	else
//		return FALSE;*/
//}
//void C4DReplaySDIPlayerDlg::OnBnClickedBtnLoadmovielist()
//{
//	if(!m_bListViewOpen)
//	{
//		int nHeight = m_rcClientRect.bottom - m_rcListRect.bottom;
//		SetWindowPos(NULL,0,0,m_rcClientRect.right,m_rcClientRect.bottom,SWP_NOMOVE);
//		//GetWindowRect(&m_rcClientRect);
//		m_bListViewOpen = TRUE;
//	}
//	else
//	{
//		SetWindowPos(NULL,0,0,m_rcListRect.right,m_rcListRect.bottom,SWP_NOMOVE);
//		m_bListViewOpen = FALSE;
//	}
//}
//void C4DReplaySDIPlayerDlg::OnBnClickedBtnLoadprocess()
//{
//	if(m_bListViewOpen)
//	{
//
//	}
//}

//int C4DReplaySDIPlayerDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
//{
//	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
//		return -1;
//
//	C4DReplayGPUProcess* pGpuProc = new C4DReplayGPUProcess;
//	pGpuProc->LaunchModal();
//	return 0;
//}


void C4DReplaySDIPlayerDlg::OnBnClickedCheckLogoSave()
{
	UpdateData(FALSE);
	
	BOOL bCheck = IsDlgButtonChecked(IDC_CHECK_LOGO_SAVE);

	G4DSetLogoSave(bCheck);
}


void C4DReplaySDIPlayerDlg::OnBnClickedCheckEncode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	if(m_bEncode)
	{
		m_bEncode = FALSE;
		CheckDlgButton(IDC_CHECK_ENCODE,FALSE);

		CheckDlgButton(IDC_CHECK_TRANSTS,FALSE);
		m_bTransTS = FALSE;
		GetDlgItem(IDC_CHECK_TRANSTS)->EnableWindow(FALSE);
	}
	else
	{
		m_bEncode = TRUE;
		CheckDlgButton(IDC_CHECK_ENCODE,TRUE);

		GetDlgItem(IDC_CHECK_TRANSTS)->EnableWindow(TRUE);
	}

	CString strLog;
	strLog.Format(_T("Encode: %d"),m_bEncode);
	G4DShowLog(5,strLog);
}


void C4DReplaySDIPlayerDlg::OnBnClickedCheckAutoadd()
{
	UpdateData(TRUE);

	m_bAutoAdd = IsDlgButtonChecked(IDC_CHECK_AUTOADD);//GetDlgItem(IDC_CHECK_INSERT_LOGO);
	
	if(m_bAutoAdd)
	{
		GetDlgItem(IDC_BUTTON_AUTOADD)->EnableWindow(TRUE);
		OnBnClickedButtonAutoadd();
	}
	else
	{
		GetDlgItem(IDC_BUTTON_AUTOADD)->EnableWindow(FALSE);
		SetAutoAdd(FALSE);
	}
}


void C4DReplaySDIPlayerDlg::OnBnClickedButtonAutoadd()
{
	C4DReplaySDIPlayerAutoAdd* dlg = new C4DReplaySDIPlayerAutoAdd;
	
	dlg->Init(m_stAJAOpt.ArrIP);
	SetAutoDlgOpen(TRUE);
	if(dlg->DoModal() == IDOK)
	{
		m_stAJAOpt.ArrIP.clear();

		for(int i = 0 ; i < dlg->m_ArrIPList.size() ; i++)
		{
			CString str;
			str = dlg->m_ArrIPList.at(i);
			m_stAJAOpt.ArrIP.push_back(str);
		}

		//Launch ini state
		if(m_bAutoAddThread == FALSE)
		{
			m_bAutoAddThread = TRUE;

			HANDLE hSyncTime = NULL;
			hSyncTime = (HANDLE)_beginthreadex(NULL,0,_AutoAddThread,(void*)this,0,NULL);
			CloseHandle(hSyncTime);

			//HANDLE hSyncTime = NULL;
			hSyncTime = (HANDLE)_beginthreadex(NULL,0,_AutoPlayThread,(void*)this,0,NULL);
			CloseHandle(hSyncTime);
		}
	}
	SetAutoDlgOpen(FALSE);

	delete dlg;
	dlg = NULL;
}
unsigned WINAPI C4DReplaySDIPlayerDlg::_AutoAddThread(LPVOID param)
{
	C4DReplaySDIPlayerDlg* pDlg = (C4DReplaySDIPlayerDlg*) param;
	G4DShowLog(5,_T("Start AutoAdd Thread"));
	while(pDlg->GetAutoAdd())
	{
		if(pDlg->GetAutoDlgOpen())
		{
			while(pDlg->GetAutoAdd())
			{
				if(pDlg->GetAutoDlgOpen() == FALSE)
					break;

				Sleep(1);
			}
		}
		if(pDlg->GetAutoAdd() == FALSE)
			break;

		int nSize = pDlg->m_stAJAOpt.ArrIP.size();
		
		for(int i = 0 ; i < nSize ; i++)
		{
			CString strIP = pDlg->m_stAJAOpt.ArrIP.at(i);

			//4DWatcher check
			if(pDlg->GetCheckConnection(strIP))
			{
				pDlg->ReadInIFile(strIP);
			}
		}

		Sleep(10);
	}

	pDlg->m_bAutoAddThread = FALSE;

	G4DShowLog(5,_T("Finish AutoAdd Thread"));

	return FALSE;
}
BOOL C4DReplaySDIPlayerDlg::GetCheckConnection(CString strIP)
{
	char szHostIP[50];
	char pRecvBuf[1024] = {0};
	int nPort = 20500;

	char* strId = ESMUtil::CStringToChar(strIP);
	memcpy(szHostIP, strId, sizeof(char) * 50);
	if( strId)
	{
		delete strId;
		strId = NULL;
	}
	CESMTCPSocket* pSocket = new CESMTCPSocket;
	pSocket->Create(AF_INET, SOCK_STREAM, IPPROTO_TCP, 300);
	if(pSocket->Connect(szHostIP, nPort, 1) > 0)
	{
		pSocket->Send("0", 2);
		pSocket->Recv(pRecvBuf, 1024,1);
	}
	pSocket->Close();

	if(pSocket)
	{
		delete pSocket;
		pSocket = NULL;
	}

	if(strcmp(pRecvBuf,"1") == 0)
	{
		//AfxMessageBox(_T("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
		return TRUE;
	}
	else
		return FALSE;
}
void C4DReplaySDIPlayerDlg::ReadInIFile(CString strIP)
{
	CString strINIPath;
	strINIPath.Format(_T("\\\\%s\\4DMaker\\config\\SDIPlayer.ini"),strIP);

	CESMFileOperation fo;
	if(!fo.IsFileExist(strINIPath))
	{
		CFile file;
		file.Open(strINIPath, CFile::modeCreate);
		file.Close();
	}

	CESMIni ini;
	if(ini.SetIniFilename(strINIPath))
	{
		CString strFileName,strFilePath,strState;

		strState = ini.GetString(STR_SECTION_SDIPLAYER,STR_STATE,_T(""));

		if(strState == _T("O"))
		{
			strFileName = ini.GetString(STR_SECTION_SDIPLAYER,STR_FILENAME,_T(""));
			strFilePath = ini.GetString(STR_SECTION_SDIPLAYER,STR_FILEPATH,_T(""));

			m_cList->AddFile(strFileName,strFilePath,1);

			//Index 받고 재생
			int nIndex = m_cList->GetItemCount()-1;

			AddAutoPlay(strFilePath);
			//Add 후 State Change
			ini.WriteString(STR_SECTION_SDIPLAYER,STR_STATE,_T("X"));
		}
	}
}
unsigned WINAPI C4DReplaySDIPlayerDlg::_AutoPlayThread(LPVOID param)
{
	C4DReplaySDIPlayerDlg* pDlg = (C4DReplaySDIPlayerDlg*) param;
	G4DShowLog(5,_T("Start AutoPlay Thread"));
	while(pDlg->GetAutoAdd())
	{
		if(pDlg->GetAutoDlgOpen())
		{
			while(pDlg->GetAutoAdd())
			{
				if(pDlg->GetAutoDlgOpen() == FALSE)
					break;

				Sleep(1);
			}
		}
		if(pDlg->GetAutoAdd() == FALSE)
			break;

		int nSize = pDlg->GetAutoPlaySize();
		if(nSize)
		{
			CString str = pDlg->GetAutoPlay();

			while(1)
			{
				if(G4DGetDivPlay() == FALSE && pDlg->m_pDecodeMgr->GetPlayState() == FALSE)
					break;

				Sleep(1);
			}
			G4DShowLog(5,_T("AUTOPLAY: ")+str);
			pDlg->InsertMovieData(str,NULL,0,TRUE,FALSE);
			pDlg->DeleteAutoPlay();
			Sleep(500);
		}

		Sleep(10);
	}

	pDlg->m_bAutoAddThread = FALSE;

	G4DShowLog(5,_T("Finish AutoPlay Thread"));

	return FALSE;
}

void C4DReplaySDIPlayerDlg::OnEnChangeEditEncodeBitrate()
{
	UpdateData(TRUE);
	CString str ;
	GetDlgItem(IDC_EDIT_ENCODE_BITRATE)->GetWindowText(str);
	int nCnt = _ttoi(str);

	m_nEncodeBitrate = nCnt;
	UpdateData(FALSE);
}


void C4DReplaySDIPlayerDlg::OnBnClickedCheckTransts()
{
	UpdateData(TRUE);
	if(m_bEncode)
	{
		if(m_bTransTS)
		{
			CheckDlgButton(IDC_CHECK_TRANSTS,FALSE);
			m_bTransTS = FALSE;
		}
		else
		{
			m_bTransTS = TRUE;
			CheckDlgButton(IDC_CHECK_TRANSTS,TRUE);
		}
	}

	CString strLog;
	strLog.Format(_T("TS: %d"),m_bTransTS);
	G4DShowLog(5,strLog);
}
