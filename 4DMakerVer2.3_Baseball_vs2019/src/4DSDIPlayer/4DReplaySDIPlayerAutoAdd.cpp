// 4DReplaySDIPlayerAutoAdd.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DReplaySDIPlayer.h"
#include "4DReplaySDIPlayerAutoAdd.h"
#include "afxdialogex.h"


// C4DReplaySDIPlayerAutoAdd 대화 상자입니다.

IMPLEMENT_DYNAMIC(C4DReplaySDIPlayerAutoAdd, CDialogEx)

C4DReplaySDIPlayerAutoAdd::C4DReplaySDIPlayerAutoAdd(CWnd* pParent /*=NULL*/)
	: CDialogEx(C4DReplaySDIPlayerAutoAdd::IDD, pParent)
{
	m_nListSelect = 0;
}

C4DReplaySDIPlayerAutoAdd::~C4DReplaySDIPlayerAutoAdd()
{
}

void C4DReplaySDIPlayerAutoAdd::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_AUTOADD_LIST, m_cIPList);
	DDX_Control(pDX, IDC_AUTOADD_IPADDRESS, m_ipv);
}


BEGIN_MESSAGE_MAP(C4DReplaySDIPlayerAutoAdd, CDialogEx)
	ON_BN_CLICKED(IDC_AUTOADD_ADDIP, &C4DReplaySDIPlayerAutoAdd::OnBnClickedAutoaddAddip)
	ON_NOTIFY(NM_CLICK, IDC_AUTOADD_LIST, &C4DReplaySDIPlayerAutoAdd::OnNMClickAutoaddList)
	ON_BN_CLICKED(IDC_AUTOADD_DELETE, &C4DReplaySDIPlayerAutoAdd::OnBnClickedAutoaddDelete)
END_MESSAGE_MAP()


// C4DReplaySDIPlayerAutoAdd 메시지 처리기입니다.


BOOL C4DReplaySDIPlayerAutoAdd::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_cIPList.SetExtendedStyle(LVS_EX_DOUBLEBUFFER | LVS_EX_FULLROWSELECT );
	m_cIPList.InsertColumn(LIST_NUM     ,_T("#"),LVCFMT_CENTER,30);	
	m_cIPList.InsertColumn(LIST_FILENAME,_T("IP"),LVCFMT_CENTER,310);

	int nSize = m_ArrIPList.size();
	for(int i = 0 ; i < nSize ; i++)
	{
		CString str;
		str = m_ArrIPList.at(i);

		CString strNum;
		strNum.Format(_T("%d"),i);
		m_cIPList.InsertItem(i,strNum);
		m_cIPList.SetItem(i,LIST_NUM,LVIF_TEXT,strNum,0,0,0,NULL);
		m_cIPList.SetItem(i,LIST_FILENAME,LVIF_TEXT,str,0,0,0,NULL);
		//m_ArrIPList.push_back(str);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
}

void C4DReplaySDIPlayerAutoAdd::Init(vector<CString>ArrList)
{
	int nSize = ArrList.size();
	for(int i = 0 ; i < nSize ; i++)
	{
		CString str;
		str = ArrList.at(i);
		m_ArrIPList.push_back(str);
	}
}
void C4DReplaySDIPlayerAutoAdd::OnBnClickedAutoaddAddip()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str;
	BYTE ip_a,ip_b,ip_c,ip_d;
	m_ipv.GetAddress(ip_a,ip_b,ip_c,ip_d);
	
//	if(ip_a != NULL && ip_b != NULL && ip_c != NULL && ip_d != NULL )
		str.Format(_T("%d.%d.%d.%d"),ip_a,ip_b,ip_c,ip_d);

	/*if(str.Find(_T("..")))
		return;*/
	if(str.GetLength())
	{
		int nItemCount = m_cIPList.GetItemCount();
		
		CString strNum;
		strNum.Format(_T("%d"),nItemCount);
		m_cIPList.InsertItem(nItemCount,strNum);
		m_cIPList.SetItem(nItemCount,LIST_NUM,LVIF_TEXT,strNum,0,0,0,NULL);
		m_cIPList.SetItem(nItemCount,LIST_FILENAME,LVIF_TEXT,str,0,0,0,NULL);
		m_ArrIPList.push_back(str);
	}
	
	Invalidate();
}


void C4DReplaySDIPlayerAutoAdd::OnNMClickAutoaddList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nListSelect = m_cIPList.GetNextItem(-1,LVNI_SELECTED);
	*pResult = 0;
}


void C4DReplaySDIPlayerAutoAdd::OnBnClickedAutoaddDelete()
{
	int nSelectedCount = m_cIPList.GetSelectedCount();

	m_ArrIPList.clear();

	if(nSelectedCount == 1)
	{
		m_cIPList.DeleteItem(m_nListSelect);
	}
	else
	{
		for(int i = nSelectedCount; i >= 0; i--)//while(pos != NULL)
		{
			POSITION pos = m_cIPList.GetFirstSelectedItemPosition();

			int nDelId = m_cIPList.GetNextSelectedItem(pos);
			CString str = m_cIPList.GetItemText(nDelId,0);
			str.Append(_T("\n"));
			TRACE(str);

			m_cIPList.DeleteItem(nDelId);
		}
	}
	//순번재정렬
	for(int i = 0 ; i < m_cIPList.GetItemCount(); i++)
	{
		CString strNum = m_cIPList.GetItemText(i,LIST_NUM);
		CString strIP;
		if(i == _ttoi(strNum))
		{
			strIP = m_cIPList.GetItemText(i,LIST_FILENAME);
			m_ArrIPList.push_back(strIP);
			continue;
		}

		strNum.Format(_T("%d"),i);
		m_cIPList.SetItemText(i,LIST_NUM,strNum);
		strIP = m_cIPList.GetItemText(i,LIST_FILENAME);
		m_ArrIPList.push_back(strIP);
	}
}
