#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <process.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>

#include "4DReplaySDIPlayerDlg.h"
#include "4DSDIPlayerIndexStructure.h"
#include "4DReplaySDIPlayerFunc.h"
#include "ESMUtil.h"
#include "ESMDefine.h"
class C4DReplaySDIPlayerDlg;

class C4DSDITCPProcess
{
public:
	C4DSDITCPProcess(C4DReplaySDIPlayerDlg* pParent);
	~C4DSDITCPProcess();
	void Init();
	void CloseSocket();
	SOCKET SetTCPServer(short pnum,int blog);
	IN_ADDR GetDefaultMyIP();

	void Create(int af, int type, int protocol, int sendtimeout, int recvtimeout);
	static unsigned WINAPI _RunTCPThread(LPVOID param);
	static unsigned WINAPI _RunSendThread(LPVOID param);

	void StartSendThread();
	void SendSodketData(ESMEvent*pMsg);
	int Send(const char* sendbuf, int buflen);
	vector<ESMEvent*>m_arMsg;
	BOOL m_bConnect;
	BOOL GetConnect(){return m_bConnect;}
	void SetConnect(BOOL b){m_bConnect = b;}

	BOOL ReceiveData(SOCKET sock,BOOL bDiv = TRUE);

	void SetInputData();
	CString CharToCString(char* str);
	SOCKET m_sock;
	SOCKET m_sock1;
	void SetSocket(SOCKET socket){m_sock1 = socket;}
	BOOL m_bConnect4DMaker;
	BOOL m_bConnectWatcher;

	C4DReplaySDIPlayerDlg* m_pParent;
	map<CString,int>m_mpIP;
	void SetIP(CString strIP,int nIndex);
	CString m_strIP;
	int GetRegistedIP(CString strIP);

	CString FindFileName(CString strPath);
	void AddBackUpFiles();

	void ParsePacket(char* headerbuf, char* bodybuf, int packetsize,BOOL bDiv = TRUE);
	void ParsePacketServer(char* headerbuf, char* bodybuf, int packetsize,BOOL bDiv = TRUE);

	CString m_str4DMName;
	CString m_strFilePath;
	BOOL m_bInsertMovie;
	BOOL m_bSendThreadRun;
	
	void ChangeConnectState(BOOL b);

	//180611 hjcho
	static unsigned WINAPI _RunAcceptThread(LPVOID param);
	static unsigned WINAPI _RunReceiveThread(LPVOID param);
	int GetLastIP(CString strIP);
	void SendBoardState(SOCKET sock,BOOL bDiv);
	void Disconnect(SOCKET sock);
};
struct THREAD_RECEIVE
{
	THREAD_RECEIVE()
	{
		TCPProcess = NULL;
		sock = NULL;
		bDiv = FALSE;
		nIP = 0;
	}
	C4DSDITCPProcess* TCPProcess;
	SOCKET* sock;
	BOOL bDiv;
	int nIP;
};