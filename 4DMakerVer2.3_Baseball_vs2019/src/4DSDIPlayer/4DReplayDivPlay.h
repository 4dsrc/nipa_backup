#pragma once
#include "4DReplaySDIPlayer.h"
#include "4DReplaySDIPlayerDlg.h"
#include "ESMDefine.h"
#include "ntv2outputtestpattern.h"
#include "4DSDIPlayerIndexStructure.h"

using namespace cv;
class C4DReplaySDIPlayerDlg;

class C4DReplaySDIPlayerDivMgr
{
public:
	C4DReplaySDIPlayerDivMgr();
	~C4DReplaySDIPlayerDivMgr();

	C4DReplaySDIPlayerDlg* m_pParent;
	void SetParent(C4DReplaySDIPlayerDlg*pParent){m_pParent = pParent;}
	void StartMoviePlay(CString strIP);
	BOOL m_bThreadState;
	void SetThreadPlay(BOOL b){m_bThreadState = b;}
	BOOL GetThreadPlay(){return m_bThreadState;}
	CString m_strIP;

	static unsigned WINAPI _ThreadAutoPlay(LPVOID param);
	void DoDecode(CString strTime,int nIdx,int nFrameCnt);
	static unsigned WINAPI _ThreadDecode(LPVOID param);
	void BGR2YUV422(Mat img);
	void DoPlay();
	static unsigned WINAPI _ThreadPlay(LPVOID param);
	//vector<stAdjustInfo>m_arrAdjust;
	map<CString,stAdjustInfo>m_mpAdjData;
	void SetAdjData(stAdjustInfo* pAdj,CString strDSCID);
	stAdjustInfo GetAdjData(CString strDSCID);
private:
	int m_nReceiveCnt;
	vector<stDivideFrame>*m_pArrDecodeFrame;
};
struct ThreadDecodeInfo{
	ThreadDecodeInfo()
	{
		pParent = NULL;
		nIdx = -1;
		nFrameCnt = -1;
	}
	C4DReplaySDIPlayerDivMgr* pParent;
	CString strTime;
	int nIdx;
	int nFrameCnt;
};