/**
	@file		ntv2outputtestpattern.cpp
	@brief		Header file for NTV2OutputTestPattern demonstration class
	@copyright	Copyright (C) 2013-2015 AJA Video Systems, Inc.  All rights reserved.
**/


#ifndef _NTV2OUTPUT_TEST_PATTERN_H
#define _NTV2OUTPUT_TEST_PATTERN_H

#include"stdafx.h"
#include "ntv2enums.h"
#include "ntv2devicefeatures.h"
#include "ntv2devicescanner.h"
#include "ntv2utils.h"
#include "ajastuff/common/types.h"
#include "testpatterngen.h"
#include "ajastuff/common/videotypes.h"
#include "ajastuff/system/process.h"
#include "opencv2\imgproc.hpp"
#include "opencv2\highgui.hpp"
#include "opencv2\core.hpp"
#include "opencv2\opencv.hpp"

using namespace std;
/**
	@brief	I DMA a test pattern into an AJA device's frame store for steady-state playout.
			This demonstrates how to perform direct DMA from a host frame buffer.
	@note	I do not use AutoCirculate to transfer frame data to the device.
**/

class NTV2OutputTestPattern
{
	//	Public Instance Methods
	public:

		/**
			@brief		Constructs me using the given configuration settings.
			@note		I'm not completely initialized and ready for use until after my Init method has been called.
			@param[in]	inDeviceIndex		Specifies the zero-based index number of the AJA device to use.
											Defaults to zero, the first device found.
			@param[in]	inChannel			Specifies the channel (frame store) to use. Defaults to NTV2_CHANNEL1.
		**/
		NTV2OutputTestPattern (const UWord			inDeviceIndex	= 0,
							   const NTV2Channel	inChannel		= NTV2_CHANNEL1);

		~NTV2OutputTestPattern (void);

		/**
			@brief	Initializes me and prepares me to Run.
			@return	AJA_STATUS_SUCCESS if successful; otherwise another AJAStatus code if unsuccessful.
		**/
		AJAStatus		Init (void);

		/**
			@brief		Displays the requested test pattern on the output.
			@param[in]	testPatternIndex	Specifies which test pattern to display.
											Defaults to 0 (100% color bars).
			@return	AJA_STATUS_SUCCESS if successful; otherwise another AJAStatus code if unsuccessful.
			@note		Do not call this method without first calling my Init method.
		**/
		AJAStatus		EmitPattern (const UWord testPatternIndex,CString strPath);
		AJAStatus		EmitPattern(cv::Mat YUV422,double dbTimeDelay,double dbSetSpeed);
		AJAStatus		SetOperation();
		
		/*Function*/
		double GetCounter();
		void StartCounter();
		__int64 CounterStart;
		double PCFreq;
		double m_dbUploadTime;
		double dSum;
		/*Variable*/
		int nWidth;
		int nHeight;
		int nPitch;
		int nTotalMakingCnt;
		BOOL m_bFinish;
		int m_nUploadSpeed;
	//	Protected Instance Methods
	protected:
		/**
			@brief	Sets up my AJA device to play video.
			@return	AJA_STATUS_SUCCESS if successful; otherwise another AJAStatus code if unsuccessful.
		**/
		AJAStatus		SetUpVideo (void);

		/**
			@brief	Sets up board routing for playout.
		**/
		void			RouteOutputSignal (void);


	//	Protected Class Methods
	protected:
		/**
			@brief		Converts an NTV2FrameBufferFormat into an equivalent AJA_PixelFormat.
			@param[in]	format	Specifies the NTV2FrameBufferFormat that is to be converted into the equivalent AJA_PixelFormat.
			@return		The AJA_PixelFormat that is equivalent to the given NTV2FrameBufferFormat.
		**/
		static AJA_PixelFormat	GetAJAPixelFormat (const NTV2FrameBufferFormat format);


	//	Private Member Data
	private:
		CNTV2Card					mDevice;			///	My CNTV2Card instance
		NTV2DeviceID				mDeviceID;			///	My device identifier
		const uint32_t				mDeviceIndex;		///	The index number of the board I'm using
		const NTV2Channel			mChannel;			///	The channel I'm using
		NTV2VideoFormat				mVideoFormat;		///	My video format
		const NTV2FrameBufferFormat	mPixelFormat;		///	My pixel format
		NTV2EveryFrameTaskMode		mSavedTaskMode;		/// Used to restore the previous task mode
		bool						mVancEnabled;		///	VANC enabled?
		bool						mWideVanc;			///	Wide VANC?

};	//	NTV2OutputTestPattern
#endif	//	_NTV2OUTPUT_TEST_PATTERN_H
