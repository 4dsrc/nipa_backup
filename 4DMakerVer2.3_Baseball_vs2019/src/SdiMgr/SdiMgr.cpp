/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        SdiManager.cpp
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */


#include "stdafx.h"
#include <afxdllx.h>
#ifdef _MANAGED
#error /clr를 사용하여 컴파일하려면 PTP.cpp에 있는 지침을 따르십시오.
// 프로젝트에 /clr를 추가하려면 다음 단계를 따르십시오.
//	1. #include <afxdllx.h> 지시문을 지웁니다.
//	2. /crl를 throw하지 않고 미리 컴파일된 헤더가 비활성화되어 있는
//	   프로젝트에 다음 텍스트를 사용하여 .cpp 파일을 추가합니다.
//			#include <afxwin.h>
//			#include <afxdllx.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


static AFX_EXTENSION_MODULE PTPMANAGERDLL = { NULL, NULL };

#ifdef _MANAGED
#pragma managed(push, off)
#endif

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// lpReserved를 사용하는 경우 다음을 제거하십시오.
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		// 확장 DLL을 한 번만 초기화합니다.
		if (!AfxInitExtensionModule(PTPMANAGERDLL, hInstance))
			return 0;
		new CDynLinkLibrary(PTPMANAGERDLL);
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		AfxTermExtensionModule(PTPMANAGERDLL);
	}
	return 1;   // 확인
}

#ifdef _MANAGED
#pragma managed(pop)
#endif

