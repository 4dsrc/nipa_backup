/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		LiveviewConverter.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version 1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */


#include "StdAfx.h"
#include "LiveviewConverter.h"
#include "PTP_Base.h"
#include "SRSIndex.h"
#include "SdiDefines.h"
#include "SdiMgr.h"

IMPLEMENT_DYNCREATE(CLiveviewConverter, CWinThread)
//------------------------------------------------------------------------------ 
//! @brief		CLiveviewConverter()
//! @date		2010-05-24
//! @author	hongsu.jung
//! @note	 	constructor
//------------------------------------------------------------------------------
CLiveviewConverter::CLiveviewConverter(void)	
{
	m_nDeviceID = -1;
	m_hRecvWnd = NULL;			//-- 2011-7-12 jeansu
	m_bLCDDisplay = TRUE;
	m_bOff = FALSE;
	Init();
}

CLiveviewConverter::CLiveviewConverter(int nDeviceNumber)	
{
	m_nDeviceID = nDeviceNumber;	
	m_hRecvWnd = NULL;			//-- 2011-7-12 jeansu
	m_bLCDDisplay = TRUE;
	m_bOff = FALSE;
	Init();
}

void CLiveviewConverter::Init()
{
	//-- 2013-12-02 hongsu@esmlab.com
	//-- Critical Section 
	InitializeCriticalSection (&m_pCS_LC);	

	m_pLiveviewImage = NULL;
	m_bThreadFlag = TRUE;
	SetSetting(FALSE);	
}

CLiveviewConverter::~CLiveviewConverter(void)
{
	StopThread();
	ExitInstance();

	DeleteCriticalSection (&m_pCS_LC);
}

BOOL CLiveviewConverter::InitInstance() 
{ 
	return TRUE; 
}

int CLiveviewConverter::ExitInstance()				
{
	return CWinThread::ExitInstance();
}

//------------------------------------------------------------------------------ 
//! @brief		StopThread
//! @date		2010-06-29
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveviewConverter::StopThread()
{
	if(!m_bThreadRun)		
		return;

	m_bThreadFlag = FALSE;	
	while(m_bThreadRun)
		Sleep(1);

	RemoveAll();
	//-- 2013-12-01 hongsu.jung
	WaitForSingleObject(m_hThread, INFINITE) ;
}

BOOL CLiveviewConverter::ReleaseImageBuffer()	
{
	EnterCriticalSection (&m_pCS_LC);
	if(m_pLiveviewImage)
	{
		cvReleaseImage(&m_pLiveviewImage);
		m_pLiveviewImage = NULL;
	}
	LeaveCriticalSection (&m_pCS_LC);
	return TRUE; 
}


void CLiveviewConverter::SetModel(int nModel) 
{	
	m_nModel = nModel;
	if(m_nModel == SDI_MODEL_NX2000)
		SetSrcSize(NX2000_SRC_WIDTH, NX2000_SRC_HEIGHT);
	else if( m_nModel == SDI_MODEL_NX1 )
		SetSrcSize(NX1_SRC_WIDTH, NX1_SRC_HEIGHT);
	else
		SetSrcSize(SRC_WIDTH,SRC_HEIGHT);	
	CreateImageBuffer();	
}

BOOL CLiveviewConverter::CreateImageBuffer()	
{
	if(m_pLiveviewImage)
	{
		cvReleaseImage(&m_pLiveviewImage);
		m_pLiveviewImage = NULL;			//-- 2011-7-28	 chlee
	}
	//-- Create Image
	if(m_nModel == SDI_MODEL_NX2000)
		m_pLiveviewImage = cvCreateImage( cvSize ( m_szSrc.cx, m_szSrc.cy-2 ), IPL_DEPTH_8U, 3 );
	else if( m_nModel == SDI_MODEL_NX1 )
		m_pLiveviewImage = cvCreateImage( cvSize ( m_szSrc.cx, m_szSrc.cy ), IPL_DEPTH_8U, 3 );
	else if( m_nModel == SDI_MODEL_NX500 )
	{
		m_pLiveviewImage = cvCreateImage( cvSize ( m_szSrc.cx, m_szSrc.cy ), IPL_DEPTH_8U, 3 );
		m_pHistogramImage = cvCreateImage( cvSize ( 256, 101 ), IPL_DEPTH_8U, 3 );

		m_nCreate = 9;
	}
	else
		m_pLiveviewImage = cvCreateImage( cvSize ( m_szSrc.cx, m_szSrc.cy ), IPL_DEPTH_8U, 3 );

	return TRUE; 
}




//------------------------------------------------------------------------------ 
//! @brief		Run
//! @date		2010-05-30
//! @author	hongsu.jung
//! @note	 	get message
//------------------------------------------------------------------------------ 
int CLiveviewConverter::Run(void)
{
	RSEvent* pEvent = NULL;
	
	m_bThreadRun = TRUE;
	m_bAutoDelete = FALSE;

	int nAll;

	while(m_bThreadFlag)
	{
		//-- 2013-03-01 honsgu.jung
		//-- For CPU
		Sleep(1);
		nAll = m_arMsg.GetSize();
		while(nAll--)
		{
			pEvent = (RSEvent*)m_arMsg.GetAt(0);
			if(!pEvent)
				continue;
			
			//-- 2013-12-02 hongsu@esmlab.com
			//-- Critical Section 
			EnterCriticalSection (&m_pCS_LC);
			
			switch(pEvent->message)
			{
			case WM_RS_EVENT_LIVEVIEW_IMAGE:
				//-- Get Buffer
				if(ISSet())
				{
					SendLiveviewMessage((unsigned char*)pEvent->pParam, (RSLiveviewInfo*)pEvent->pDest, pEvent->nParam1, pEvent->nParam2, pEvent->nParam3);
				}
				break;
			case WM_LIVEVIEW_EVENT_SIZE:
				{
					SetSrcSize( ((RSLiveviewInfo*)pEvent->pParam)->imageSizeWidth,		((RSLiveviewInfo*)pEvent->pParam)->imageSizeHeight);
					SetOffsetSize( ((RSLiveviewInfo*)pEvent->pParam)->offsetSizeWidth,	((RSLiveviewInfo*)pEvent->pParam)->offsetSizeHeight);
					//delete (RSLiveviewInfo*)pEvent->pParam;

					//-- SendMessage SrcSize
					RSEvent* pMsgToMain = new RSEvent();
					pMsgToMain->nParam1 = m_szSrc.cx;
					pMsgToMain->nParam2= m_szSrc.cy;					
					::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_RS_SET_CHANGE_LIVEVIEW_SIZE, (LPARAM)pMsgToMain);
				}
				break;
			default:
				TRACE("None Defined Message in Liveview Thread\n");
				break;
			}
			//-- Remote
			if( pEvent)
			{
				delete pEvent;
				pEvent = NULL;
			}
			m_arMsg.RemoveAt(0);
			
			//-- 2013-12-02 hongsu@esmlab.com
			//-- Critical Section 
			LeaveCriticalSection (&m_pCS_LC);
		}
	}

	ReleaseImageBuffer();//- 2013-11-27  thread 종료전 Release
	m_bThreadRun = FALSE;

	return 0;
}

void CLiveviewConverter::RemoveAll()
{
	// [11/27/2013 Administrator]
	// Remove Array
	RSEvent* pEvent = NULL;
	int nAll = m_arMsg.GetSize();
	while(nAll--)
	{
		pEvent = (RSEvent*)m_arMsg.GetAt(nAll);
		if(pEvent)
		{
			delete pEvent;
			pEvent = NULL;
		}
		m_arMsg.RemoveAt(nAll);
	}
	m_arMsg.RemoveAll();
}

int CLiveviewConverter::AddBuffer( unsigned char* pYUV, RSLiveviewInfo* pLiveviewInfo, int nType, BOOL bRecord, BOOL bEnd)
{
	RSEvent* pEvent = new RSEvent();
	pEvent->message = WM_RS_EVENT_LIVEVIEW_IMAGE;	
	pEvent->pParam = (LPARAM)pYUV;
	pEvent->pDest = (LPARAM)pLiveviewInfo;
	pEvent->nParam1 = nType;
	pEvent->nParam2 = (int)bRecord;
	pEvent->nParam3 = (int)bEnd;
	if(m_arMsg.Add((CObject*)pEvent) == -1){
		return SDI_ERR_INTERNAL;
	}	
	return SDI_ERR_OK;
}

int CLiveviewConverter::ChangeSize(RSLiveviewInfo* pLiveviewInfo)
{
	RSEvent* pEvent	= new RSEvent();
	pEvent->message	= WM_LIVEVIEW_EVENT_SIZE;
	pEvent->pParam	= (LPARAM)pLiveviewInfo;
	return m_arMsg.Add((CObject*)pEvent);
}

//------------------------------------------------------------------------------ 
//! @brief		GetLiveview(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//! @note	 	get liveview from ptp
//------------------------------------------------------------------------------ 
int  CLiveviewConverter::SendLiveviewMessage(unsigned char* pYUV, RSLiveviewInfo* pLiveviewInfo, int nType, int nRecord, int nEnd)
{
	if(m_nModel == SDI_MODEL_NX2000 )//|| m_nModel == SDI_MODEL_NX1)
	{
		NX2000_ConvertYuv420(NX2000_vga_width, NX2000_vga_height, pYUV);		//-- VGA
	}else if(m_nModel == SDI_MODEL_NX1)
	{
		//NX2000_ConvertYuv420(NX1_vga_width, NX1_vga_height, pYUV);		//-- VGA
		NX1_ConvertYuv420(NX1_vga_width, NX1_vga_height, pYUV, pLiveviewInfo, nType, nRecord, nEnd);		//-- VGA
	}
	else
	{
		if(m_szSrc.cx == qvga_width )		
		{
			ConvertYuv422(qvga_width, qvga_height, pYUV);	//-- QVGA
		}
		else									
		{
			ConvertYuv420(vga_width, vga_height, pYUV);		//-- VGA
		}
	}

	//-- Send Message To Liveview Frame
	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_SDI_EVENT_LIVEVIEW_RECEIVED;
	pEvent ->nParam1	= m_nDeviceID;
	pEvent ->pParam	= (LPARAM)m_pLiveviewImage;
	
	//-- 2013-11-21 hongsu@esmlab.com
	//-- For Multi 
	pEvent ->pDest	= (LPARAM)m_pParent;

	if(m_hRecvWnd!=NULL)
	{
		::PostMessage(m_hRecvWnd,
					WM_RS,
					(WPARAM)WM_SDI_EVENT_LIVEVIEW_RECEIVED,
					(LPARAM)pEvent);
	}
	else
	{
		delete pEvent;
		pEvent = NULL;
	}
	return RS_OK;
}

//------------------------------------------------------------------------------ 
//! @brief		ConvertYuv420(unsigned char *pYUV, int height, int width)
//! @date		2010-05-23
//! @author	hongsu.jung
//! @note	 	convert YUV (640 X 424)
//------------------------------------------------------------------------------ 
void CLiveviewConverter::ConvertYuv420(int width, int height, unsigned char* pYUV)
{	
	//width = width + m_szOffset.cx * 2;
	//height = height + m_szOffset.cy * 2;

	//-- Test Code
	/*
	static int n = 0;
	if(height==360){
		CFile file;
		CString strFilepath;
		strFilepath.Format(_T("C:\\test%d.raw"),n++);
		file.Open(strFilepath, CFile::modeWrite |	CFile::shareExclusive | CFile::modeCreate,NULL);
		file.Write(pYUV, height*width*1.5);
		file.Close();	
	}
	*/
	

	unsigned char	y, cb, cr;
	double			r, g, b;
	int					rgb_index		= 0;
	int					Y_oddPos		= 0 ;				// Y0
	int					C_oddPos		= height * width ;	// C0
	int					Y_evenPos	= C_oddPos / 2 ;		// Y1
	int					C_evenPos	= C_oddPos * 5 / 4 ;	// C1

	/*
	if(height==360){
		Y_evenPos = width * 240;
		C_oddPos = width * 480;
		C_evenPos = C_oddPos + width * 120;
	}
	*/

	int Y_index,	Y_Offset;
	int Cb_index,	C_Offset;
	int Cr_index;	

	for ( int i = 0 ; i < height; i++ )
	{
		for ( int j=0; j< width; j++ )
		{	
			//-- Check OffSet
			if(m_szOffset.cx>0 && ( j < m_szOffset.cx || j > width - m_szOffset.cx ))
			{
				m_pLiveviewImage->imageData[rgb_index++]= 0;//(byte)CLIPING(r);			
				m_pLiveviewImage->imageData[rgb_index++]= 0;//(byte)CLIPING(g);
				m_pLiveviewImage->imageData[rgb_index++]= 0;//(byte)CLIPING(b);	
			}			
			else if(m_szOffset.cy > 0 && ( i < m_szOffset.cy || i > height - m_szOffset.cy ))
			{
				m_pLiveviewImage->imageData[rgb_index++]= 0;//(byte)CLIPING(r);			
				m_pLiveviewImage->imageData[rgb_index++]= 0;//(byte)CLIPING(g);
				m_pLiveviewImage->imageData[rgb_index++]= 0;//(byte)CLIPING(b);	
			}							
			else
			{
				//-- Get Y Offset
				Y_Offset = i/2 * width  + j;				
				//-- Get CbCr Offset
				if( j%2 == 0 )	C_Offset = i/4 * width  + j ;
				else			C_Offset = i/4 * width  + j - 1;
			
				if( i%2 == 0 )	
				{
					Y_index = Y_oddPos + Y_Offset;				//-- Even
					Cb_index = C_oddPos + C_Offset;
				}
				else
				{
					Y_index = Y_evenPos + Y_Offset;				//-- Odd
					Cb_index = C_evenPos + C_Offset;
				}			
			
				Cr_index = Cb_index +1;

				y	= pYUV[Y_index];
				cb = pYUV[Cb_index];
				cr  = pYUV[Cr_index ];

				r = 1.164 * ( y - 16 ) + 2.018 * ( cb - 128 );			
				g = 1.164 * ( y - 16 ) - 0.813 * ( cr - 128 ) - 0.391 * ( cb - 128 );
				b = 1.164 * ( y - 16 ) + 1.596 * ( cr - 128 );

				// RGB를 저장하는 부분
				m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(r);			
				m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(g);
				m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(b);	
			}
		}
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		NX2000_ConvertYuv420(unsigned char *pYUV, int height, int width)
//! @date		2010-05-23
//! @author	hongsu.jung
//! @note	 	convert YUV (640 X 424)
//------------------------------------------------------------------------------ 
void CLiveviewConverter::NX2000_ConvertYuv420(int width, int height, unsigned char* pYUV)
{	
	unsigned char	y, cb, cr;
	double			r, g, b;
	unsigned long					rgb_index		= 0;
	int					Y_oddPos		= 0 ;				// Y0
	int					C_oddPos		= height * width ;	// C0
	int					Y_evenPos	= C_oddPos / 2 ;		// Y1
	int					C_evenPos	= C_oddPos * 5 / 4 ;	// C1

	int Y_index,	Y_Offset;
	int Cb_index,	C_Offset;
	int Cr_index;
	Y_index = 0;
	Cb_index = C_oddPos;

	for ( int i = 0 ; i < height-2; i++ )
	{
		if(i%2 == 1)
			Cb_index -= width;
		for ( int j=0; j< width; j++ )
		{
			if(j%2 == 0)
			{
				Cb_index = Cb_index + 2;
				Cr_index = Cb_index + 1;
			}

			y  = pYUV[Y_index];
			cb = pYUV[Cb_index];
			cr = pYUV[Cr_index];


			if(j < m_szOffset.cx || j > (width-m_szOffset.cx) )
			{
				r = 0;
				g = 0;
				b = 0;
			}
			else
			{
				r = 1.164 * ( y - 16 ) + 2.018 * ( cb - 128 );			
				g = 1.164 * ( y - 16 ) - 0.813 * ( cr - 128 ) - 0.391 * ( cb - 128 );
				b = 1.164 * ( y - 16 ) + 1.596 * ( cr - 128 );
			}


			Y_index++;
			// RGB를 저장하는 부분
			if( rgb_index < (m_pLiveviewImage->width * m_pLiveviewImage->height * 3) )
			{
				m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(r);
				m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(g);
				m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(b);
			}
		}
	}
}
LiveviewOffset CLiveviewConverter::GetPhotoSizeOffset(int nType, RSLiveviewInfo* pLiveInfo)
{
	LiveviewOffset offset;
	switch(nType)
	{
	case LIVE_SIZE_EVF:
	case LIVE_SIZE_28:
	case LIVE_SIZE_13_9:
	case LIVE_SIZE_7_1:
	case LIVE_SIZE_3:
		offset.nOffsetX = 0;
		offset.nOffsetY = 0;
		break;
	case LIVE_SIZE_23:
	case LIVE_SIZE_11_9:
	case LIVE_SIZE_6_2:
	case LIVE_SIZE_2_4:
		offset.nOffsetX = 0;
		offset.nOffsetY = 40;
		break;
	case LIVE_SIZE_18_7:
	case LIVE_SIZE_9_5:
	case LIVE_SIZE_4_7:
	case LIVE_SIZE_2:
		offset.nOffsetX = 120;
		offset.nOffsetY = 0;
		break;
	case LIVE_SIZE_RECORD:
		//if(pLiveInfo->imageSizeHeight < 404)
		//{
		//	offset.nOffsetX = 0;
		//	offset.nOffsetY = 50;
		//}
		//else
		{
			offset.nOffsetX = 0;
			offset.nOffsetY = 0;
		}
		break;
	default:
		offset.nOffsetX = 0;
		offset.nOffsetY = 0;
		break;
	}

	return offset;
}
//------------------------------------------------------------------------------ 
//! @brief		NX1_ConvertYuv420(unsigned char *pYUV, int height, int width)
//! @date		2010-05-23
//! @author	hongsu.jung
//! @note	 	convert YUV (640 X 424)
//------------------------------------------------------------------------------ 
int g_nLiveCount = 0;
int g_nPreLive;

void CLiveviewConverter::NX1_ConvertYuv420(int width, int height, unsigned char* pYUV, RSLiveviewInfo* pLiveviewInfo, int nType, int nRecord, int nEnd)
{
	//int g_nStart = GetTickCount();

	LiveviewOffset offset;
	unsigned char	y, cb, cr;
	double			r, g, b;
	unsigned long					rgb_index		= 0;
	int					Y_oddPos		= 0 ;				// Y0
	int					C_oddPos		= height * width ;	// C0
	int					Y_evenPos	= C_oddPos / 2 ;		// Y1
	int					C_evenPos	= C_oddPos * 5 / 4 ;	// C1

	int Y_index,	Y_Offset;
	int Cb_index,	C_Offset;
	int Cr_index;
	Y_index = 0;
	Cb_index = C_oddPos;

	int nOffsetSize = 0;
	int nGap = 0;
	int nOffset = 0;

	BOOL bOrigin = FALSE;


	//Live Change Check
	/*if(g_nPreLive != nType)
	{
		g_nLiveCount++;
		if(g_nLiveCount > 1)
		{
			g_nPreLive = nType;
			g_nLiveCount = 0;
		}
		return;
	}
	else
		g_nPreLive = nType;*/


	if(g_nPreLive != nType)
	{
		g_nPreLive = nType;
		return;
	}
	
	

	if(nRecord == 1 && nEnd == 0)
	{
		offset = GetPhotoSizeOffset(LIVE_SIZE_RECORD, pLiveviewInfo);
	}
	else
		offset = GetPhotoSizeOffset(nType);

		

	if(1) //720*480
	{
		C_oddPos	= pLiveviewInfo->imageSizeHeight * pLiveviewInfo->imageSizeWidth ;	// C0
		Y_evenPos	= C_oddPos / 2 ;		// Y1
		C_evenPos	= C_oddPos * 5 / 4 ;	// C1
		Cb_index = C_oddPos;
		//nOffsetSize = (NX1_vga_width - pLiveviewInfo->imageSizeWidth)/2;

		for ( int i = 0 ; i < pLiveviewInfo->imageSizeHeight; i++ )
		{
			if(i%2 == 1)
				Cb_index -= pLiveviewInfo->imageSizeWidth;
			for ( int j=0; j< width; j++ )
			{
				if(j%2 == 0)
				{
					if(i == 0 && j == 0)
					{
						Cb_index = Cb_index;
						Cr_index = Cb_index + 1;
						bOrigin = TRUE;
					}
					else if(!bOrigin)
					{
						if(i == offset.nOffsetY+1 && j == 0)
						{
							Cb_index = Cb_index;
							Cr_index = Cb_index + 1;
							bOrigin = TRUE;
						}
						else
						{
							Cb_index = Cb_index + 2;
							Cr_index = Cb_index + 1;
						}
					}
					else
					{
						Cb_index = Cb_index + 2;
						Cr_index = Cb_index + 1;
					}
				}

				if(Y_index < 0 || Cb_index < 0 || Cr_index < 0)// memory leak
					return;
				
				y  = pYUV[Y_index];
				cb = pYUV[Cb_index];
				cr = pYUV[Cr_index];


				if(j < nOffsetSize || j >= (pLiveviewInfo->imageSizeWidth + nOffsetSize)) //-- joonho.kim
				{
					r = 0;
					g = 0;
					b = 0;

					if(j%2 == 0)
					{
						Cb_index = Cb_index - 2;
						Cr_index = Cb_index - 1;
					}
				}
				else
				{
					int nOffsetY; 
					if(nRecord)
						nOffsetY = 480-offset.nOffsetY;
					else
						nOffsetY = pLiveviewInfo->imageSizeHeight-offset.nOffsetY;

					if( i < offset.nOffsetY || i > nOffsetY)
					{
						r = 0;
						g = 0;
						b = 0;
					}
					else if( j < offset.nOffsetX || j >  (pLiveviewInfo->imageSizeWidth-offset.nOffsetX))
					{
						r = 0;
						g = 0;
						b = 0;
					}
					else
					{
						r = 1.164 * ( y - 16 ) + 1.596 * ( cr - 128 );					
						g = 1.164 * ( y - 16 ) - 0.813 * ( cr - 128 ) - 0.391 * ( cb - 128 );
						b = 1.164 * ( y - 16 ) + 2.018 * ( cb - 128 );	
					}
					Y_index++;
				}

				// RGB를 저장하는 부분
				if( rgb_index < (m_pLiveviewImage->width * m_pLiveviewImage->height * 3) )
				{
					m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(b);
					m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(g);
					m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(r);
				}
			}
		}
	}
	else //720x404 
	{
		for ( int i = 0 ; i < 404; i++ )
		{
			if(nRecord == 1 && nEnd == 0) // Recording 화면 Center
			{
				if( i > offset.nOffsetY && i <  (404-offset.nOffsetY))
				{
					if(i%2 == 1)
						Cb_index -= pLiveviewInfo->imageSizeWidth/*width*/;
				}
			}
			else
			{
				if(i%2 == 1)
					Cb_index -= pLiveviewInfo->imageSizeWidth/*width*/;
			}

			for ( int j=0; j< pLiveviewInfo->imageSizeWidth/*width*/; j++ )
			{
				

				if(nRecord == 1 && nEnd == 0) // Recording 화면 Center
				{
					if( i > offset.nOffsetY && i <  (404-offset.nOffsetY))
					{
						if(j%2 == 0)
						{
							if(i == 0 && j == 0)
							{
								Cb_index = Cb_index;
								Cr_index = Cb_index + 1;
								bOrigin = TRUE;
							}
							else if(!bOrigin)
							{
								if(i == offset.nOffsetY+1 && j == 0)
								{
									Cb_index = Cb_index;
									Cr_index = Cb_index + 1;
									bOrigin = TRUE;
								}
								else
								{
									Cb_index = Cb_index + 2;
									Cr_index = Cb_index + 1;
								}
							}
							else
							{
								Cb_index = Cb_index + 2;
								Cr_index = Cb_index + 1;
							}
						}

						y  = pYUV[Y_index];
						cb = pYUV[Cb_index];
						cr = pYUV[Cr_index];

						r = 1.164 * ( y - 16 ) + 1.596 * ( cr - 128 );
						g = 1.164 * ( y - 16 ) - 0.813 * ( cr - 128 ) - 0.391 * ( cb - 128 );
						b = 1.164 * ( y - 16 ) + 2.018 * ( cb - 128 );			
						Y_index++;
					}
					else
					{
						r = 0;
						g = 0;
						b = 0;
					}
				}
				else
				{
					if(j%2 == 0)
					{
						if(i == 0 && j == 0)
						{
							Cb_index = Cb_index;
							Cr_index = Cb_index + 1;
						}
						else
						{
							Cb_index = Cb_index + 2;
							Cr_index = Cb_index + 1;
						}
					}

					y  = pYUV[Y_index];
					cb = pYUV[Cb_index];
					cr = pYUV[Cr_index];

					if(!pLiveviewInfo)
						return;
				
					if(i > pLiveviewInfo->imageSizeHeight ) //-- joonho.kim
					{
						r = 0;
						g = 0;
						b = 0;
					}
					else
					{
						if( i < offset.nOffsetY || i >  (404-offset.nOffsetY))
						{
							r = 0;
							g = 0;
							b = 0;
						}
						else if( j < offset.nOffsetX || j >  (pLiveviewInfo->imageSizeWidth/*width*/-offset.nOffsetX))
						{
							r = 0;
							g = 0;
							b = 0;
						}
						
						else
						{
							r = 1.164 * ( y - 16 ) + 1.596 * ( cr - 128 );
							g = 1.164 * ( y - 16 ) - 0.813 * ( cr - 128 ) - 0.391 * ( cb - 128 );
							b = 1.164 * ( y - 16 ) + 2.018 * ( cb - 128 );			
						}
					}

					Y_index++;
				}

				// RGB를 저장하는 부분
				if( rgb_index < (m_pLiveviewImage->width * m_pLiveviewImage->height * 3) )
				{
					m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(b);
					m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(g);
					m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(r);
				}
			}
		}
	}

	//int g_nEnd = GetTickCount();
	//TRACE(_T("Liveview Convert Time gap : %d \n"), g_nEnd - g_nStart);

	/*if(pLiveviewInfo)
	{
		delete pLiveviewInfo;
		pLiveviewInfo = NULL;
	}*/
}

//------------------------------------------------------------------------------ 
//! @brief		ConvertYuv422(unsigned char *pYUV, int height, int width)
//! @date		2010-07-04
//! @author	hongsu.jung
//! @note	 	convert YUV	(320 X 240)
//------------------------------------------------------------------------------ 
void CLiveviewConverter::ConvertYuv422(int width, int height, unsigned char* pYUV)
{	
	//-- Test Code
	/*
	CFile file;
	file.Open(_T("C:\\test2.raw"), CFile::modeWrite |	CFile::shareExclusive | CFile::modeCreate,NULL);
	file.Write(pYUV, height*width*1.5);
	file.Close();
	*/

	int					i, j;					// row, column
	unsigned char	y, cb, cr;
	double			r, g, b;
	int					rgb_index = 0;	

	int C_initPos = height * width;

	int Y_index;
	int Cb_index;
	int Cr_index;

	for ( i = 0 ; i < height ; i++ )
	{
		for ( j=0; j< width; j++ )
		{	
			//-- Check OffSet
			if(m_szOffset.cx>0 && ( j < m_szOffset.cx || j > m_szSrc.cx - m_szOffset.cx ))
			{
				m_pLiveviewImage->imageData[rgb_index++]= 0;//(byte)CLIPING(r);			
				m_pLiveviewImage->imageData[rgb_index++]= 0;//(byte)CLIPING(g);
				m_pLiveviewImage->imageData[rgb_index++]= 0;//(byte)CLIPING(b);	
			}			
			else if(m_szOffset.cy > 0 && ( i < m_szOffset.cy || i > m_szSrc.cy - m_szOffset.cy ))
			{
				m_pLiveviewImage->imageData[rgb_index++]= 0;//(byte)CLIPING(r);			
				m_pLiveviewImage->imageData[rgb_index++]= 0;//(byte)CLIPING(g);
				m_pLiveviewImage->imageData[rgb_index++]= 0;//(byte)CLIPING(b);	
			}							
			else
			{
				Y_index = i*width  + j;
				y	= pYUV[Y_index];

				Cb_index = C_initPos + i/2 * width + j - (j%2);
				Cr_index = Cb_index+1;

				cb = pYUV[Cb_index];
				cr	= pYUV[Cr_index ];

				r = 1.164 * ( y - 16 ) + 2.018 * ( cb - 128 );			
				g = 1.164 * ( y - 16 ) - 0.813 * ( cr - 128 ) - 0.391 * ( cb - 128 );
				b = 1.164 * ( y - 16 ) + 1.596 * ( cr - 128 );
			
				// RGB를 저장하는 부분
				m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(r);			
				m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(g);
				m_pLiveviewImage->imageData[rgb_index++]= (byte)CLIPING(b);	
			}
		}
	}	
}
