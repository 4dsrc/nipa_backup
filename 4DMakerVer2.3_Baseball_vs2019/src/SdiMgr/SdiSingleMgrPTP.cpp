/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        SdiSingleMgr.h
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */
#include "StdAfx.h"
#include "SdiCore.h"
#include "SdiCoreWin7.h"
#include "SdiCoreWinXp.h"
#include "SdiSingleMgr.h"
#include "PTP_Base.h"		//--2011-6-29		chlee
#include "SdiDefines.h"

#ifndef _NonOpenCV
#include "LiveviewConverter.h"
#else
#include "LiveviewConverterPure.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//--------------------------------------------------------------------------
//!@brief	PTP_InitSession
//
//!@param	none
//!@return	RS_OK on success, any other value means failure.	
//--------------------------------------------------------------------------
int CSdiSingleMgr::PTP_InitSession(CString strDevName)
{
	char buff[150];
	sprintf(buff,"%S",strDevName);
	m_pSdiCore->SdiInitSession(buff);
	return RS_OK;
}

//--------------------------------------------------------------------------
//!@brief PTP_OpenSession
//
//!@param	none
//!@return	RS_OK on success, any other value means failure.	
//--------------------------------------------------------------------------
int CSdiSingleMgr::PTP_OpenSession()
{	
	TRACE(_T("[SdiSingleMgr][Start] PTP_OpenSession \n"));
	SdiResult nRet = m_pSdiCore->SdiOpenSession();
	if(nRet != SDI_ERR_OK)
	{
		SdiMessage* pMessage = NULL;
		pMessage = new SdiMessage();
		pMessage->message = WM_SDI_OP_STATUS_PTPOPEN;
		pMessage->nParam1 = FALSE;
		SdiAddMsg(pMessage);
		m_bConnect = FALSE;
		TRACE(_T("[SdiSingleMgr][Fail] PTP_OpenSession \n"));
		return nRet;
	}
	else
	{
		SdiMessage* pMessage = NULL;
		pMessage = new SdiMessage();
		pMessage->message = WM_SDI_OP_STATUS_PTPOPEN;
		pMessage->nParam1 = TRUE;
		SdiAddMsg(pMessage);
		m_bConnect = TRUE;
		TRACE(_T("[SdiSingleMgr][End] PTP_OpenSession \n"));
	}

	HRESULT hr = S_OK;	
	if(!m_pYUV)
	{
		if(m_strModel=="NX2000")
		{
			//YUV420 format
			m_pYUV = new unsigned char[ NX2000_SRC_WIDTH * NX2000_SRC_HEIGHT * 1.5];
			memset(m_pYUV, 0, sizeof(unsigned char) * NX2000_SRC_WIDTH * NX2000_SRC_HEIGHT * 1.5);
		}
		else if( m_strModel=="NX1" || m_strModel == "NX500" || m_strModel == "GH5")
		{
			//YUV420 format
			m_pYUV = new unsigned char[ NX1_SRC_WIDTH * NX1_SRC_HEIGHT * 1.5];
			int nTemp = NX1_SRC_WIDTH * NX1_SRC_HEIGHT * 1.5;
			memset(m_pYUV, 0, sizeof(unsigned char) * NX1_SRC_WIDTH * NX1_SRC_HEIGHT * 1.5);
		}
		else
		{
			//YUV420 format
			m_pYUV = new unsigned char[ SRC_WIDTH * SRC_HEIGHT * 1.5];	
			memset(m_pYUV, 0, sizeof(unsigned char) * SRC_WIDTH * SRC_HEIGHT * 1.5);
		}	
	}
	return PTP_GetDevUniqueID();
}
//--------------------------------------------------------------------------
//!@brief	Close session	
//
//!@param	none
//!@return	RS_OK(1) on success, any other value means failure.	
//--------------------------------------------------------------------------
int CSdiSingleMgr::PTP_CloseSession()
{
	//-- 2013-02-13 hongsu.jung
	//-- Stop Liveview
	SetLiveview(FALSE);

	m_pSdiCore->SdiCloseSession();
	return RS_OK;
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_SendCapture_Require()
//! @date		2011-06-01
//! @author		chlee
//! @note	 	capture require operation send
//------------------------------------------------------------------------------
int  CSdiSingleMgr::PTP_SendCapture_Require(int nShutter)
{
	return m_pSdiCore->SdiSendShutterCapture(nShutter);
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_SendCapture_Once()
//! @date		2011-06-01
//! @author		chlee
//! @note	 	capture require operation send
//------------------------------------------------------------------------------
int  CSdiSingleMgr::PTP_SendCapture_Once(int nModel)
{
	SdiResult result = SDI_ERR_OK;

	result = m_pSdiCore->SdiSendAdjRequireCaptureOnce(nModel);

	if(result==SDI_ERR_OK){
		//-- Start Liveview	
		//SetLiveview(TRUE);	//--2011-07-21 jeansu : liveview 는 pre-state 에 의존하도록 변경.
	}
	return result;
}


//------------------------------------------------------------------------------ 
//! @brief		PTP_SendCapture_Complete()
//! @date		2011-06-01
//! @author		chlee
//! @note	 	capture require operation send
//------------------------------------------------------------------------------ 

int  CSdiSingleMgr::PTP_SendCapture_Complete(CString strFileName, bool &bIsLast, int nFileNum, int nClose)
{
	SdiResult result = SDI_ERR_OK;
	result = m_pSdiCore->SdiSendAdjCompleteCapture(strFileName, bIsLast, nFileNum, nClose);
#if 0
	//--2011-07-21 jeansu : liveview 는 pre-state 에 의존하도록 변경.
	if(result==SDI_ERR_OK){
		//-- 2011-06-29 hongsu.jung
		//-- Start Liveview	
		SetLiveview(TRUE);
	}
#endif
	return result;
}

//NX3000 20150119 ImageTransfer
int  CSdiSingleMgr::PTP_SendCapture_Complete(CString strFileName, bool &bIsLast)
{
	SdiResult result = SDI_ERR_OK;
	result = m_pSdiCore->SdiSendAdjCompleteCapture(strFileName, bIsLast);
	return result;
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_SendMovie_Complete()
//! @date		2011-06-01
//! @author		chlee
//! @note	 	capture require operation send
//------------------------------------------------------------------------------ 

int  CSdiSingleMgr::PTP_SendMovie_Complete(CString strFileName, int nfilenum, int nClose)
{
	SdiResult result = SDI_ERR_OK;
	result = m_pSdiCore->SdiSendAdjMovieTransfer(strFileName, nfilenum, nClose);
#if 0
	//--2011-07-21 jeansu : liveview 는 pre-state 에 의존하도록 변경.
	if(result==SDI_ERR_OK){
		//-- 2011-06-29 hongsu.jung
		//-- Start Liveview	
		SetLiveview(TRUE);
	}
#endif
	return result;
}

int  CSdiSingleMgr::PTP_SendMovie_Complete(CString strFileName, int nWaitSaveDSC, int nFileNum, int nClose)
{
	SdiResult result = SDI_ERR_OK;
	result = m_pSdiCore->SdiSendAdjCompleteMovie(strFileName, nWaitSaveDSC, nFileNum, nClose);
#if 0
	//--2011-07-21 jeansu : liveview 는 pre-state 에 의존하도록 변경.
	if(result==SDI_ERR_OK){
		//-- 2011-06-29 hongsu.jung
		//-- Start Liveview	
		SetLiveview(TRUE);
	}
#endif
	return result;
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_SendLiveview()
//! @date		2010-05-24
//! @author	hongsu.jung
//------------------------------------------------------------------------------
int  CSdiSingleMgr::PTP_SendLiveview()
{
	int nResult = SDI_ERR_OK;
	nResult = m_pSdiCore->SdiSendAdjLiveview(m_pYUV);
	if(nResult != SDI_ERR_OK && nResult != SDI_ERR_PTP_OK)
		return nResult;
	
	//-- Convert Image via Converter Thread
	if(!m_pLiveviewConverter)
		return SDI_ERR_LIVEVIEW_CREATE_COMMAND;

	return m_pLiveviewConverter->AddBuffer(m_pYUV);
}
int  CSdiSingleMgr::PTP_SendLiveview(RSLiveviewInfo* pLiveviewInfo, int nType, BOOL bRecord, BOOL bEnd)
{
	
	/*int nResult = SDI_ERR_OK;
	nResult = m_pSdiCore->SdiSendAdjLiveview(m_pYUV);
	if(nResult != SDI_ERR_OK && nResult != SDI_ERR_PTP_OK)
		return nResult;*/
	
	//-- Convert Image via Converter Thread
	if(!m_pLiveviewConverter)
		return SDI_ERR_LIVEVIEW_CREATE_COMMAND;

	m_pLiveviewConverter->m_bLCDDisplay = m_bLCDDisplay;

	return m_pLiveviewConverter->AddBuffer(m_pYUV, pLiveviewInfo, nType, bRecord, bEnd);
}


//------------------------------------------------------------------------------ 
//! @brief		PTP_LiveviewInfo()
//! @date		2010-06-13
//! @author	hongsu.jung

//------------------------------------------------------------------------------
int CSdiSingleMgr::PTP_LiveviewInfo(RSLiveviewInfo* pLiveviewInfo)
{
	if(m_strModel == "NX1"|| m_strModel == "GH5")
	{
		m_pSdiCore->SdiSendAdjLiveview(m_pYUV);
	}
	return m_pSdiCore->SdiSendAdjLiveviewInfo(pLiveviewInfo);

}

//------------------------------------------------------------------------------ 
//! @brief		PTP_SetDevPropDesc
//! @date		2011-8-9
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
SdiResult CSdiSingleMgr::PTP_SetDevPropDesc(int nPTPCode, int nType, LPARAM lpValue)
{
	return m_pSdiCore->SdiSetDevPropValue(nPTPCode, nType, lpValue);
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_GetDevPropDesc()
//! @date		2010-06-209
//! @author	hongsu.jung
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_GetDevPropDesc(int nPTPCode,  int nAllFlag)
{
	return m_pSdiCore->SdiGetDevPropDesc(nPTPCode, nAllFlag);
}

int CSdiSingleMgr::PTP_SetDevUniqueID(CString strUUID)
{
	int ret = m_pSdiCore->SdiSetDevUniqueID(PTP_CODE_SAMSUNG_UNIQUEID, strUUID);	
	return ret;
}

int CSdiSingleMgr::PTP_GetDevUniqueID()
{
	/*
	char **chUniqueID;	
	if(GetOSVersion()==OS_WINDOWS_XP)
	{
		chUniqueID = (char**)malloc(1);
	}
	else
	{
		chUniqueID = (char**) CoTaskMemAlloc(1);
	}
	*/

	TRACE(_T("[SdiSingleMgr][Start] Get Dev Unique ID\n"));
	char *chUniqueID = '\0';
	int ret = m_pSdiCore->SdiGetDevUniqueID(PTP_CODE_SAMSUNG_UNIQUEID, &chUniqueID);	
	if( (ret != SDI_ERR_OK) || (chUniqueID=='\0'))
	{
		TRACE(_T("[SdiSingleMgr][Fail] Get Dev Unique ID\n"));
		return ret;
	}

	int usize = MultiByteToWideChar(CP_ACP,0,chUniqueID,-1,NULL,NULL);
	WCHAR *ustr = new WCHAR[usize];
	MultiByteToWideChar(CP_ACP,0,chUniqueID,strlen(chUniqueID)+1,ustr,usize);
	m_strUniqueID.Format(_T("%s"),ustr);
	
	if( ustr )
		delete ustr;
	if(GetOSVersion()==OS_WINDOWS_XP)
	{
		free(chUniqueID);
	}
	else
	{
		CoTaskMemFree(chUniqueID);		
	}

	TRACE(_T("[SdiSingleMgr][End] Get Dev Unique ID = %s\n"), m_strUniqueID);
	return ret;
};

int CSdiSingleMgr::PTP_GetDevModeIndex()
{
	return m_pSdiCore->SdiGetDevModeIndex(PTP_CODE_FUNCTIONALMODE, m_nModeIndex);
};
//------------------------------------------------------------------------------ 
//! @brief		Focus Position
//! @date		2011-6-24
//! @author		Lee JungTaek
//! @note	 	Get/Set Focus Position
//------------------------------------------------------------------------------ 
int CSdiSingleMgr::PTP_SetFocusPosition(FocusPosition * ptFocusPos, int nType)
{
	return m_pSdiCore->SdiSetFocusPosition(ptFocusPos, nType);
}

//------------------------------------------------------------------------------ 
//! @brief		Focus Position
//! @date		2013-11-21
//! @author		Hongsu Jung
//! @note	 	Set Focus Position
//------------------------------------------------------------------------------ 
int CSdiSingleMgr::PTP_SetFocusPosition(int nCurrent)
{
	return m_pSdiCore->SdiSetFocusPosition(nCurrent);
}

//------------------------------------------------------------------------------ 
//! @brief		Focus Position
//! @date		2013-11-21
//! @author		Hongsu Jung
//! @note	 	Set Focus Position
//------------------------------------------------------------------------------ 
int CSdiSingleMgr::PTP_GetFocusPosition(FocusPosition * ptFocusPos)
{
	return m_pSdiCore->SdiGetFocusPosition(ptFocusPos);
}


//------------------------------------------------------------------------------ 
//! @brief		PTP_CheckEvent()
//! @date		2010-06-29
//! @author	chlee
//!
//------------------------------------------------------------------------------
int CSdiSingleMgr::PTP_CheckEvent(SdiInt32& nEventExist, SdiUIntArray* pArList)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiCheckEvent(nEventExist, pArList);
}

void CSdiSingleMgr::TickSyncStop()
{
	m_pSdiCore->SdiSyncStop();
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_GetEvent()
//! @date		2010-06-29
//! @author	chlee
//!
//------------------------------------------------------------------------------
int CSdiSingleMgr::PTP_GetEvent(BYTE** pData, int* size)
{
	return m_pSdiCore->SdiGetEvent(pData, (SdiInt32*)size);
}


//------------------------------------------------------------------------------ 
//! @brief		PTP_ResetDevice / PTP_FormatDevice / PTP_FWUpdate
//! @date		2011-7-18
//! @author		Lee JungTaek
//! @note	 	Control Camera
//------------------------------------------------------------------------------ 
int CSdiSingleMgr::PTP_FormatDevice()
{
	return m_pSdiCore->SdiFormatDevice();
}


int CSdiSingleMgr::PTP_FWUpdate()
{
	return m_pSdiCore->SdiFWUpdate();
}
int CSdiSingleMgr::PTP_ResetDevice(int nParam)
{
	return m_pSdiCore->SdiResetDevice(nParam);
	//-- Change Status
}
//dh0.seo 2014-11-04
int CSdiSingleMgr::PTP_SensorCleaningDevice()
{
	return m_pSdiCore->SdiSensorCleaningDevice();
	//-- Change Status
}

//CMiLRe 20141113 IntervalCapture Stop 추가
int CSdiSingleMgr::PTP_IntervalCaptureStop()
{
	return m_pSdiCore->SdiIntervalCaptureStop();
}

//CMiLRe 20141127 Display Save Mode 에서 Wake up
int CSdiSingleMgr::PTP_DisplaySaveModeWakeUp()
{
	return m_pSdiCore->SdiDisplaySaveModeWakeUp();
}

//20150128 CMiLRe NX3000
//CMiLRe 2015129 펌웨어 다운로드 경로 추가
int CSdiSingleMgr::PTP_FWDownload(CString strPath)
{
	return m_pSdiCore->SdiFWDownload(strPath);
}

int CSdiSingleMgr::PTP_DeviceReboot()
{
	return m_pSdiCore->SdiDeviceReboot();
}

int CSdiSingleMgr::PTP_ExportLog()
{
	return m_pSdiCore->SdiDeviceExportLog();
}

int CSdiSingleMgr::PTP_DevicePowerOff()
{
	return m_pSdiCore->SdiDevicePowerOff();
}

SdiResult CSdiSingleMgr::PTP_HalfShutter(int nRelease)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiHalfShutter(nRelease);
}

SdiResult CSdiSingleMgr::PTP_SetFocusFrame( int nX, int nY, int nMagnification )
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiSetFocusFrame(nX, nY, nMagnification);
}

SdiResult CSdiSingleMgr::PTP_GetFocusFrame(BYTE **pData, int *size)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiGetFocusFrame(pData, (SdiInt32*)size);
}


//------------------------------------------------------------------------------ 
//! @brief		Focus Position
//! @date		2011-6-24
//! @author		Lee JungTaek
//! @note	 	Get/Set Focus Position
//------------------------------------------------------------------------------ 
int CSdiSingleMgr::PTP_GetPTPDeviceInfo()
{
	int nRet = SDI_ERR_OK;

	PTPDeviceInfo* ptDeviceInfo = new PTPDeviceInfo();
	nRet = m_pSdiCore->SdiGetPTPDeviceInfo(ptDeviceInfo);

	if(nRet == SDI_ERR_OK)		
		m_ptPTPDevInfo = ptDeviceInfo;

	return nRet;
}

SdiResult CSdiSingleMgr::PTP_InitGH5()
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiInitGH5();
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_GetTick()
//! @date		2013-10-22
//! @author		yongmin
//!
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_GetTick(int& tPC, double& tDSC, int& nCheckCnt, int& nCheckTime, int nDSCSyncTime, HWND handle )
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;

	SdiResult ret = SDI_ERR_OK;

	EnterCS();
	ret = m_pSdiCore->SdiGetTick((SdiInt&)tPC, (SdiDouble&)tDSC, (SdiInt&)nCheckCnt, (SdiInt&)nCheckTime, (SdiInt)nDSCSyncTime, handle);
	TRACE(_T("GetTick [%s] - Pc[%d], DSC[%.4lf]\r\n"), GetDeviceDSCID(), tPC, tDSC);
	LeaveCS();
	return ret;
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_SetPause(int nPause)
//! @date		2013-10-22
//! @author		yongmin
//!
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_SetPause(int nPause)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;

	SdiResult ret = SDI_ERR_OK;

	EnterCS();
	ret = m_pSdiCore->SdiSetPause(nPause);
	//ret = m_pSdiCore->SdiSetResumeMode(	nPause);
	LeaveCS();
	return ret;
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_SetResume()
//! @date		2013-10-22
//! @author		yongmin
//!
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_SetResume(int nResume)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;

	SdiResult ret = SDI_ERR_OK;

	EnterCS();
	//ret = m_pSdiCore->SdiSetResumeFrame(nResume);
	ret = m_pSdiCore->SdiSetResume(nResume);
	LeaveCS();
	return ret;
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_SetTick()
//! @date		2013-10-22
//! @author		yongmin
//!
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_SetTick(double nTick, int nIntervalSensorOn, SdiUIntArray* pArList)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	SdiResult ret = SDI_ERR_OK;

	EnterCS();
	ret = m_pSdiCore->SdiSetResumeFrame(0);
	ret = m_pSdiCore->SdiSetTick((SdiUInt32)nTick, nIntervalSensorOn, pArList);
	TRACE(_T("SetTick [%s] - %.4lf\r\n"), GetDeviceDSCID(), nTick);
	LeaveCS();
	return ret;
}

SdiResult CSdiSingleMgr::PTP_MovieCancle()
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiMovieCancle();
}

SdiResult CSdiSingleMgr::PTP_ControlTouchAF(int nX, int nY)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiControlTouchAF( nX, nY);
}

SdiResult CSdiSingleMgr::PTP_TrackingAFStop()
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiTrackingAFStop();
}
//------------------------------------------------------------------------------ 
//! @brief		PTP_HiddenCommand()
//! @date		2013-10-22
//! @author		yongmin
//!
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_HiddenCommand(int nCommand,int nValue)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiHiddenCommand((SdiUInt32)nCommand, (SdiUInt32)nValue);
}

SdiResult CSdiSingleMgr::PTP_FocusSave()
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiFocusSave();
}
//------------------------------------------------------------------------------ 
//! @brief		PTP_GetRecordStatus()
//! @date		2014-07-21
//! @author		joonho.kim
//!
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_GetRecordStatus()
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiGetRecordStatus();
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_GetRecordStatus()
//! @date		2014-07-21
//! @author		joonho.kim
//!
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_GetCaptureCount()
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiGetCaptureCount();
}

SdiResult CSdiSingleMgr::PTP_GetImagePath(CString& strPath)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiGetImagePath(strPath);
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_FileTransfer()
//! @date		2014-07-21
//! @author		joonho.kim
//!
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_FileTransfer(char* pPath, int nfilenum, int nmdat, int nOffset,int* nFileSize, int nMovieSize)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;	
	return m_pSdiCore->SdiFileTransfer( pPath,  nfilenum, nmdat, nOffset, nFileSize, nMovieSize);
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_MovieTransfer()
//! @date		2014-07-21
//! @author		joonho.kim
//!
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_MovieTransfer(int nfilenum, int nClose)
{
/*	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiMovieTransfer( nfilenum, nClose);*/
	return 1;
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_ImageTransfer()
//! @date		2014-07-21
//! @author		joonho.kim
//!
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_ImageTransfer(CString strName, int nfilenum, int nClose)
{
	SdiResult Ret;
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;

	if(m_strModel == "GH5")
		Ret = m_pSdiCore->SdiImageTransferEX( strName, nfilenum, nClose);
	else
		Ret = m_pSdiCore->SdiImageTransfer( strName, nfilenum, nClose);

	return Ret;
}

//! @brief		PTP_SetEnLarg()
//! @date		2014-08-18
//! @author		changdo.kim
//!
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_SetEnLarg(int nLarge)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiSetEnLarge( nLarge);
}
//------------------------------------------------------------------------------ 
//! @brief		PTP_SetRecordPause(int nPause)
//! @date		2014-09-04
//! @author		joonho.kim
//!
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_SetRecordPause(int nPause)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiSetRecordPause( nPause);
}

//------------------------------------------------------------------------------ 
//! @brief		PTP_SetRecordResume(int nSkip)
//! @date		2014-09-04
//! @author		joonho.kim
//!
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::PTP_SetRecordResume(int nSkip)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiSetRecordResume( nSkip);
}




SdiResult CSdiSingleMgr::PTP_SetLiveView(int nCommand)
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiSetLiveView( nCommand);
}

SdiResult CSdiSingleMgr::PTP_FileReceiveComplete()
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiFileReceiveComplete();
}

SdiResult CSdiSingleMgr::PTP_ImageReceiveComplete()
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiImageReceiveComplete();
}
SdiResult CSdiSingleMgr::PTP_LiveReceiveComplete()
{
	if(!m_pSdiCore)
		return SDI_ERR_DEVICE_DISCONNECTED;
	return m_pSdiCore->SdiLiveReceiveComplete();
}
