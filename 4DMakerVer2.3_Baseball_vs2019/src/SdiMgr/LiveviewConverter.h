/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		LiveviewConverter.h
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version 1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */

#pragma once
#include <Afxtempl.h>
//-- 2011-06-14
//-- time measure
#include <sys/timeb.h>
#include <time.h>

//-- 2011-05-23 hongsu.jung
#include "cv.h"
#include "highgui.h"

//-- 2011-05-23 hongsu.jung
#include "RSFunc.h"

//-- 2011-07-20 jean
#include "SdiDefines.h"
#include "SetupAPI.h"


static const int liveview_main_speed			=	 100;
static const int liveview_connected_speed		=	 250;
static const int liveview_quick_speed			=	  35;
static const int event_polling_time				=	50;
static const int qvga_width						=	 320;
static const int qvga_height					=	 212;
static const int vga_width						=	 640;
static const int vga_height						=	 424;
static const int NX2000_vga_width				=	 800;
static const int NX2000_vga_height				=	 482;
static const int NX1_vga_width					=	1024;
static const int NX1_vga_height					=	684;
static const int vga_wide_height				=	 360;

class AFX_EXT_CLASS CLiveviewConverter: public CWinThread
{
	DECLARE_DYNCREATE(CLiveviewConverter)
public:
	BOOL m_bThreadFlag;
private:
	
	
	BOOL m_bThreadRun;

	//-- 2013-12-02 hongsu@esmlab.com
	//-- Critical Section 
	CRITICAL_SECTION	m_pCS_LC;

	typedef struct LIVEVIEW_SIZE
	{
		int nWidth;
		int nHeight;
		int nOffsetWidth;
		int nOffsetHeight;		
	};	

public:
	CLiveviewConverter(void);	
	CLiveviewConverter(int nDeviceNumber);	
	~CLiveviewConverter(void);
	
protected:
	HWND		m_hRecvWnd;			//-- 2011-7-12 jeansu
	CRSArray	m_arMsg;	
	int			m_nDeviceID;
	BOOL		m_bLoaded;
	
	CSize		m_szSrc;
	CSize		m_szOffset;
	IplImage*	m_pLiveviewImage;
	//-- 2013-11-21 hongsu@esmlab.com
	//-- For Multi
	PVOID		m_pParent;

protected:
	void Init();
	virtual BOOL InitInstance();	
	virtual int Run(void);

	void ConvertYuv420(int , int , unsigned char*);
	void ConvertYuv422(int , int , unsigned char*);
	void NX2000_ConvertYuv420(int, int, unsigned char*);
	void NX1_ConvertYuv420(int, int, unsigned char*, RSLiveviewInfo* , int nType = 0 , int nRecord = 0, int nEnd = 0);
	int  SendLiveviewMessage(unsigned char*, RSLiveviewInfo*, int nType = 0, int nRecord = 0, int nEnd = 0);	
	BOOL ReleaseImageBuffer();
	void SetOffsetSize(int nOffsetWidth, int  nOffsetHeight )	{	m_szOffset = CSize(nOffsetWidth, nOffsetHeight); }
	void SetSrcSize(int nWidth, int nHeight)						{	m_szSrc	= CSize(nWidth, nHeight);}

public:
	void StopThread();
	virtual int ExitInstance();
	
	int AddBuffer(unsigned char*);
	int AddBuffer(unsigned char*, RSLiveviewInfo* pLiveviewInfo, int nType = 0, BOOL bRecord = FALSE, BOOL bEnd = FALSE);
	void RemoveAll();
	int ChangeSize(RSLiveviewInfo* pLiveviewInfo);
	void SetSetting(BOOL b)			{ m_bLoaded = b;}
	BOOL ISSet()					{ return m_bLoaded;}
	BOOL CreateImageBuffer();	
	IplImage* GetLiveviewBuffer()	{ return m_pLiveviewImage;}
	CSize	GetSrcSize()			{ return m_szSrc;	}
	CSize	GetOffsetSize()			{ return m_szOffset;	}
	void	ReorderID()				{ m_nDeviceID--; }	
	LiveviewOffset GetPhotoSizeOffset(int nType, RSLiveviewInfo* pLiveInfo = NULL);

	//-- 2011-7-12 jeansu
	void SdiSetReceiveHandler(HWND hRecvWnd) { m_hRecvWnd = hRecvWnd; }

	//-- 2013-6-20 yongmin.lee
	void SetModel(int nModel);
	void SetParent(PVOID pParent) {m_pParent = pParent;}
	int m_nModel;
	BOOL m_bLCDDisplay;
	BOOL m_bOff;
};

