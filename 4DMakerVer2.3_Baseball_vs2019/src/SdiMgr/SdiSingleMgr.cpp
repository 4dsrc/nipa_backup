/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        SdiSingleMgr.h
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */

#include "StdAfx.h"
#include "SdiCore.h"
#include "SdiCoreWin7.h"
#include "SdiCoreWinXp.h"
#include "SdiSingleMgr.h"
#include "PTP_Base.h"		//--2011-6-29		chlee
#include "SdiDefines.h"

//#define SAMPLE

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifndef _NonOpenCV
#include "LiveviewConverter.h"
#else
#include "LiveviewConverterPure.h"
#endif

#include <MMSystem.h>
#pragma comment(lib, "winmm")

static const GUID GUID_DEVINTERFACE_USB = 
{ 0xA5DCBF10, 0x6530, 0x11D2, { 0x90, 0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED } };

IMPLEMENT_DYNCREATE(CSdiSingleMgr, CWinThread)
//------------------------------------------------------------------------------ 
//! @brief		CSdiSingleMgr()
//! @date		2010-05-24
//! @author	hongsu.jung
//! @note	 	constructor
//------------------------------------------------------------------------------
CSdiSingleMgr::CSdiSingleMgr(void) {m_bRecordLiveView = FALSE; m_bEnd = FALSE; m_bErr = FALSE;}
CSdiSingleMgr::CSdiSingleMgr(int nID, CString strVendor, CString strProduct)
{
	Init(nID, strVendor, strProduct);	
	m_bConnect = FALSE;
	m_bErr = FALSE;
}

///
/// For Sync Time
///
CSdiSingleMgr::CSdiSingleMgr(int nID, CString strVendor, CString strProduct, LARGE_INTEGER swFreq, LARGE_INTEGER swStart)
{
	m_swFreq = swFreq ;
	m_swStart = swStart ;
	m_bErr = FALSE;
	Init(nID, strVendor, strProduct);	
}

void CSdiSingleMgr::Init(int nID, CString strVendor, CString strProduct)
{
	InitializeCriticalSection (&m_pCS_SM);	

	m_nMovieSize = 1;

	m_bInitPreRec = FALSE;
	m_bCloseSession = FALSE;
	m_pSdiCore = NULL;	
	m_pParent = NULL;
	m_nDeviceID	= nID;
	m_strVendor = strVendor;
	m_strProduct = strProduct;

	//-- 2013-02-12 hongsu.jung
	//-- GET MODEL NAME	
	//-- NX200
	if(1<m_strVendor.Find(SDI_PID_NX200))			{ m_strModel = _T("NX200");		m_strProduct = SDI_PID_NX200;	m_nModel = SDI_MODEL_NX200; }
	//-- NX1000
	else if(1<m_strVendor.Find(SDI_PID_NX1000))		{ m_strModel = _T("NX1000");	m_strProduct = SDI_PID_NX1000;	m_nModel = SDI_MODEL_NX1000; }
	//-- NX2000
	else if(1<m_strVendor.Find(SDI_PID_NX2000))		{ m_strModel = _T("NX2000");	m_strProduct = SDI_PID_NX2000;	m_nModel = SDI_MODEL_NX2000; }//-- 2013-6-20 yongmin.lee
	//-- NX1
	//CMiLRe 20140724 NX1 Add
	else if(1<m_strVendor.Find(SDI_PID_NX1))		{ m_strModel = _T("NX1");	m_strProduct = SDI_PID_NX1;	m_nModel = SDI_MODEL_NX1; }
	else if(1<m_strVendor.Find(SDI_PID_NX3000))		{ m_strModel = _T("NX3000");	m_strProduct = SDI_PID_NX3000;	m_nModel = SDI_MODEL_NX3000; }
	else if(1<m_strVendor.Find(SDI_PID_NX500))		{ m_strModel = _T("NX500");	m_strProduct = SDI_PID_NX500;	m_nModel = SDI_MODEL_NX500; }
	else if(1<m_strVendor.Find(SDI_PID_GH5))		{ m_strModel = _T("GH5");	m_strProduct = SDI_PID_GH5;	m_nModel = SDI_MODEL_GH5; }
	else if(1<m_strVendor.Find(SDI_PID_GH5_2))		{ m_strModel = _T("GH5");	m_strProduct = SDI_PID_GH5;	m_nModel = SDI_MODEL_GH5; }
	//else if(1<m_strVendor.Find(SDI_VID_PANASONIC))		{ m_strModel = _T("GH5");	m_strProduct = SDI_PID_GH5;	m_nModel = SDI_MODEL_GH5; }
	//-- Unknown
	else											{ m_strModel = _T("Unknown");	m_strProduct = _T("");			m_nModel = SDI_MODEL_UNKNOWN; }
	
	//-- 2011-06-13 hongsu.jung
	switch(GetOSVersion())
	{
	case OS_WINDOWS_EIGHT:
	case OS_WINDOWS_SEVEN:
	case OS_WINDOWS_VISTA:
		//-- 2011-06-13 hongsu.jung
		m_pSdiCore = new CSdiCoreWin7(m_nModel, m_swFreq, m_swStart);
		break;
	case OS_WINDOWS_XP:
		m_pSdiCore = new CSdiCoreWinXp();
		break;
	default:
		break;
	}
	m_pYUV = NULL;
	//-- 2011-05-30
	m_pLiveviewConverter = NULL;
	//-- 2011-06-01 hongsu.jung
	m_bLiveview = FALSE;
	m_bPreLiveview = FALSE;
	//-- 2011-07-21
	m_nLiveviewSpeed = liveview_main_speed;
	//-- 2011-8-23 Lee JungTaek
	m_ptPTPDevInfo = NULL;
	//-- 2011-08-25 jeansu
	m_bIsCapturing = FALSE;
	//-- 2013-04-24
	SetDSCStatus(SDI_STATUS_NORMAL);

	m_nPhysicalMode = -1;
	m_bThreadRun = FALSE;
	//-- 2013-09-24 hongsu@esmlab.com
	//-- NULL Check
	m_strSavedFile = _T("");

	//m_hResetThreadHandle = NULL;
	//m_DscResetTime = CTime::GetCurrentTime();
	//m_hResetThreadHandle = (HANDLE) _beginthreadex(NULL, 0, DscResetThread, (void *)this, 0, NULL);
	//CloseHandle(m_hResetThreadHandle);
	SetDeviceLocation();	
// 	if(m_strModel == _T("NX1"))
// 		sync_frame_time_after_sensor_on = 10000;
// 	if(m_strModel == _T("NX500"))
// 		sync_frame_time_after_sensor_on = 15000;

}

//------------------------------------------------------------------------------ 
//! @brief		CSdiSingleMgr()
//! @date		2010-05-24
//! @author	hongsu.jung
//! @note	 	destructor
//------------------------------------------------------------------------------
CSdiSingleMgr::~CSdiSingleMgr(void)
{
	SetLiveview(FALSE);	

	//-- 2013-12-01 hongsu@esmlab.com
	StopThread();
	
	//-- 2013-04-25 hongsu.jung
	//-- Thread Stop	
	if(m_pLiveviewConverter)
	{
		//-- Delete Thread
		delete m_pLiveviewConverter;
		m_pLiveviewConverter = NULL;
	}

// 	TRACE(_T("[SdiMgr] Close Session :: Start\n"));
// 	if(m_pSdiCore)
// 	m_pSdiCore->SdiCloseSession();			//-- 2013-12-02 hongsu@esmlab.com
// 	TRACE(_T("[SdiMgr] Close Session :: Stop\n"));

	//-- 2013-12-01 hongsu@esmlab.com
	//-- Stop Thread 
	TRACE(_T("[SdiMgr] Stop Thread :: Start[%s]\n"), m_strUniqueID);
	StopThread();
	TRACE(_T("[SdiMgr] Stop Thread :: Stop[%s]\n"), m_strUniqueID);

	//-- debugging
	//-- 2013-02-04 hongsu.jung	
	if(m_pYUV)
	{
		//CMiLRe 20141113 메모리 릭 제거
		delete m_pYUV;
		m_pYUV = NULL;
	}
	TRACE(_T("[SdiMgr] Check 1[%s]\n"), m_strUniqueID);
	if(m_ptPTPDevInfo)
	{
		delete m_ptPTPDevInfo;
		m_ptPTPDevInfo = NULL;
	}
	TRACE(_T("[SdiMgr] Check 2[%s]\n"), m_strUniqueID);
	if(m_pSdiCore)
	{
		if(m_pSdiCore->m_bStop)
			return;

		m_pSdiCore->m_bStop = TRUE;		
		TRACE(_T("[SdiMgr] Check 3[%s]\n"), m_strUniqueID);
		m_pSdiCore->SdiCloseSession();			//-- 2013-12-02 hongsu@esmlab.com
		TRACE(_T("[SdiMgr] Check 4[%s]\n"), m_strUniqueID);
		//m_pSdiCore->RemoveThread();
		if( m_pSdiCore)
		{
			TRACE(_T("[SdiMgr] Check 5[%s]\n"), m_strUniqueID);
			CSdiCoreWin7* sdicore = (CSdiCoreWin7*)m_pSdiCore;
			sdicore->RemoveThread();
			TRACE(_T("[SdiMgr] Close SdiCore:: Start\n"));
			delete m_pSdiCore;
			TRACE(_T("[SdiMgr] Close SdiCore:: Stop\n"));
			m_pSdiCore = NULL;		
		}
	}

	DeleteCriticalSection (&m_pCS_SM);
	ExitInstance();
}

//------------------------------------------------------------------------------ 
//! @brief		StopThread
//! @date		2010-06-29
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CSdiSingleMgr::StopThread()
{
	if(!m_bThreadRun)		
		return;

	m_bThreadFlag = FALSE;

	while(m_bThreadRun)
		Sleep(1);

	RemoveAll();

	//-- 2013-12-01 hongsu@esmlab.com
	//-- Confirm Stop Thread
	WaitForSingleObject(this->m_hThread, INFINITE);
}

BOOL CSdiSingleMgr::InitInstance()
{
	m_bThreadFlag = TRUE;

	//-- For Speedy Live view Processing
	m_pLiveviewConverter = new CLiveviewConverter(GetConnectID());
	if(m_pLiveviewConverter)
	{
		TRACE(_T("[Success] Create Livevive Converter\n"));
		//-- 2013-11-04 hongsu@esmlab.com
		//-- Create Thread 
		m_pLiveviewConverter->CreateThread(CREATE_SUSPENDED);
		m_pLiveviewConverter->ResumeThread() ;

		//-- 2011-7-12 jeansu
		m_pLiveviewConverter->SdiSetReceiveHandler(m_hRecvWnd);
		m_pLiveviewConverter->SetModel(m_nModel);
		//-- 2013-11-21 hongsu@esmlab.com		
		m_pLiveviewConverter->SetParent(this);
		return TRUE;
	}
	else		
	{
		TRACE(_T("[Fail] Create Livevive Converter\n"));
	}

	return FALSE;
}

int CSdiSingleMgr::ExitInstance()
{
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CSdiSingleMgr, CWinThread)
END_MESSAGE_MAP()

//------------------------------------------------------------------------------ 
//! @brief		GetOSVersion()
//! @date		2011-7-7 chlee
//! @author	hongsu.jung
//! @note	 	delete all array
//------------------------------------------------------------------------------
int CSdiSingleMgr::GetOSVersion()
{
    OSVERSIONINFO osvi;
    BOOL bIsWindowsXPorLater;

    ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

    GetVersionEx(&osvi);

    bIsWindowsXPorLater = 
       ( (osvi.dwMajorVersion > 5) ||
       ( (osvi.dwMajorVersion == 5) && (osvi.dwMinorVersion >= 1) ));

	if(osvi.dwMajorVersion == 5 && osvi.dwMinorVersion==1)
	{
		m_nOSVersion = OS_WINDOWS_XP;			    // Windows XP
	}
	else if(osvi.dwMajorVersion == 6 && osvi.dwMinorVersion==0)
	{
		m_nOSVersion = OS_WINDOWS_VISTA;			// Windows Vista
	}
	else if(osvi.dwMajorVersion == 6 && osvi.dwMinorVersion==1)
	{
		m_nOSVersion = OS_WINDOWS_SEVEN;			// Windows 7
	}
	else if(osvi.dwMajorVersion == 6 && osvi.dwMinorVersion==2)
	{
		m_nOSVersion = OS_WINDOWS_EIGHT;			// Windows 8
	}
	else if(osvi.dwMajorVersion == 6 && osvi.dwMinorVersion==3)
	{
		m_nOSVersion = OS_WINDOWS_EIGHT;			// Windows 8.1
	}
	else
	{
		m_nOSVersion = OS_UNKNOWN;
	}

	return m_nOSVersion;
}

//------------------------------------------------------------------------------ 
//! @brief		RemoveAll()
//! @date		2010-05-24
//! @author	hongsu.jung
//! @note	 	delete all array
//------------------------------------------------------------------------------
void CSdiSingleMgr::RemoveAll(BOOL bCheckConnect)
{
	int nAll = m_arMsg.GetSize();
	while(nAll--)
	{
		RSEvent* pMsg = (RSEvent*)m_arMsg.GetAt(nAll);
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
			m_arMsg.RemoveAt(nAll);
		}		
		else
		{
			m_arMsg.RemoveAll();
			nAll = 0;
		}
	}	
}


//------------------------------------------------------------------------------ 
//! @brief		Time Gap Measure
//! @date		2011-6-29
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
int CSdiSingleMgr::GetTimeGap(struct _timeb* pTimeStart, struct _timeb* pTimeEnd )
{
	int nSec	= pTimeEnd->time		- pTimeStart->time;
	int nMilli	= pTimeEnd->millitm	- pTimeStart->millitm;
	return nSec*1000 + nMilli;
}

//------------------------------------------------------------------------------ 
//! @brief		SetLiveviewSpeed()
//! @date		2010-07-04
//! @author	hongsu.jung
//------------------------------------------------------------------------------
void CSdiSingleMgr::SetLiveviewSpeed(BOOL bMain)	
{
	if(bMain)
	{
		//CSize sz = m_pLiveviewConverter->GetSrcSize();
		//if(sz.cx == qvga_width )	m_nLiveviewSpeed = liveview_quick_speed;
		//else							m_nLiveviewSpeed = liveview_main_speed;
		m_nLiveviewSpeed = liveview_main_speed;
	}
	else
		m_nLiveviewSpeed = liveview_connected_speed;
}

//------------------------------------------------------------------------------ 
//! @brief		IsRaw()
//! @date		2010-07-07
//! @author	hongsu.jung
//------------------------------------------------------------------------------
int CSdiSingleMgr::GetQualityType()
{
	CDeviceInfoMgr* pInfo = NULL;
	pInfo = m_pSdiCore->GetDeviceInfo();
	int nValue = pInfo->GetCurrentEnum(PTP_CODE_COMPRESSIONSETTING);
	switch(nValue)
	{
	case eUCS_CAP_QUALITY_SUPER_FINE		:	
	case eUCS_CAP_QUALITY_FINE				:		
	case eUCS_CAP_QUALITY_NORMAL			:		return RS_QUALITY_TYPE_JPG;
	case eUCS_CAP_QUALITY_RAW				:		
	case eUCS_CAP_QUALITY_SRAW				:		return RS_QUALITY_TYPE_RAW;
	case eUCS_CAP_QUALITY_RAW_S_FINE		:
	case eUCS_CAP_QUALITY_RAW_FINE			:	
	case eUCS_CAP_QUALITY_RAW_NORMAL		:		return RS_QUALITY_TYPE_JPGRAW;	
	}

	return RS_QUALITY_TYPE_NULL;
}
					
//--------------------------------------------------------------------------
//!@brief	SdiGetDeviceHandleList
//
//!@param	none
//!@return	SDI_ERR_OK on success, any other value means failure.
//--------------------------------------------------------------------------
SdiResult CSdiSingleMgr::SdiGetDeviceHandleList (SdiStrArray* pArList)
{
	GUID guidDeviceInterface = GUID_DEVINTERFACE_USB;

	BOOL bResult = TRUE;
	HDEVINFO hDeviceInfo;
	SP_DEVINFO_DATA DeviceInfoData;

	SP_DEVICE_INTERFACE_DATA deviceInterfaceData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA pInterfaceDetailData = NULL;

	ULONG requiredLength=0;

	LPTSTR lpDevicePath = NULL;
	CString strTemp = _T("");
	DWORD index = 0;

	//--2011-7-21 jeansu - error handling
	if(pArList==NULL) return SDI_ERR_INVALID_PARAMETER;

	// Get information about all the installed devices for the specified
	// device interface class.
	hDeviceInfo = SetupDiGetClassDevs( 
		&guidDeviceInterface,
		NULL, 
		NULL,
		DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);

	if (hDeviceInfo == INVALID_HANDLE_VALUE) 
	{ 
		// ERROR 
		printf("Error SetupDiGetClassDevs: %d.\n", GetLastError());
		goto done;
	}

	//Enumerate all the device interfaces in the device information set.
	DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
	
	for (index = 0; SetupDiEnumDeviceInfo(hDeviceInfo, index, &DeviceInfoData); index++)
	{
		//Reset for this iteration
		if (lpDevicePath)
		{
			LocalFree(lpDevicePath);
			lpDevicePath = NULL;
		}
		if (pInterfaceDetailData)
		{
			LocalFree(pInterfaceDetailData);
			pInterfaceDetailData = NULL;
		}

 		deviceInterfaceData.cbSize = sizeof(SP_INTERFACE_DEVICE_DATA);
 
 		//Get information about the device interface.
 		bResult = SetupDiEnumDeviceInterfaces( 
 			hDeviceInfo,
 			NULL, //&DeviceInfoData,
 			&guidDeviceInterface,
 			index, 
 			&deviceInterfaceData);
 
 		// Check if last item
		DWORD dwError;

		dwError = GetLastError();

 		if (GetLastError () == ERROR_NO_MORE_ITEMS)
 		{
 			break;
 		}

		//Check for some other error
		if (!bResult) 
		{
			printf("Error SetupDiEnumDeviceInterfaces: %d.\n", GetLastError());
			goto done;
		}

		//Interface data is returned in SP_DEVICE_INTERFACE_DETAIL_DATA
		//which we need to allocate, so we have to call this function twice.
		//First to get the size so that we know how much to allocate
		//Second, the actual call with the allocated buffer

		bResult = SetupDiGetDeviceInterfaceDetail(
			hDeviceInfo,
			&deviceInterfaceData,
			NULL, 0,
			&requiredLength,
			NULL);


		//Check for some other error
		if (!bResult) 
		{
			if ((ERROR_INSUFFICIENT_BUFFER==GetLastError()) && (requiredLength>0))
			{
				//we got the size, allocate buffer
				pInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)LocalAlloc(LPTR, requiredLength);

				if (!pInterfaceDetailData) 
				{ 
					// ERROR 
					printf("Error allocating memory for the device detail buffer.\n");
					goto done;
				}
			}
			else
			{
				printf("Error SetupDiEnumDeviceInterfaces: %d.\n", GetLastError());
				goto done;
			}
		}

		//get the interface detailed data
		// CodeSonar
		if(!pInterfaceDetailData)
			goto done;

		pInterfaceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

		//Now call it with the correct size and allocated buffer
		bResult = SetupDiGetDeviceInterfaceDetail(
			hDeviceInfo,
			&deviceInterfaceData,
			pInterfaceDetailData,
			requiredLength,
			NULL,
			&DeviceInfoData);

		//Check for some other error
		if (!bResult) 
		{
			printf("Error SetupDiGetDeviceInterfaceDetail: %d.\n", GetLastError());
			goto done;
		}

		//copy device path
 		size_t nLength = wcslen (pInterfaceDetailData->DevicePath) + 1;  
 		lpDevicePath = (TCHAR *) LocalAlloc (LPTR, nLength * sizeof(TCHAR));
		
		//-- Code Sonar
		if(!lpDevicePath)
		{
			//Error.
			printf("Error %d.", GetLastError());
			goto done;
		}
		
		StringCchCopy(lpDevicePath, nLength, pInterfaceDetailData->DevicePath);
		lpDevicePath[nLength-1] = 0;


		printf("Device path: %S\n", lpDevicePath);
		CString temp;
		temp.Format(_T("%s"),lpDevicePath);

		//-- 2013-02-05 hongsu.jung
		//-- Search Samsung (Vendor)
		if(1<temp.Find(SDI_VID_SAMSUNG))	// vendor samsung
		{
			//-- Search Product
			//-- NX200
				 if(1<temp.Find(SDI_PID_NX200))		pArList->Add(temp);
			//-- NX1000
			else if(1<temp.Find(SDI_PID_NX1000))	pArList->Add(temp);
			//-- NX2000
			else if(1<temp.Find(SDI_PID_NX2000))	pArList->Add(temp);
			//-- NX3000
			else if(1<temp.Find(SDI_PID_NX1))		pArList->Add(temp);
			else if(1<temp.Find(SDI_PID_NX3000))	pArList->Add(temp);
			else if(1<temp.Find(SDI_PID_NX500))		pArList->Add(temp);
		}

		if(1<temp.Find(SDI_VID_PANASONIC))	// vendor panasonic
		{
			/*if(1<temp.Find(SDI_PID_GH5))		
				pArList->Add(temp);
			else if(1<temp.Find(SDI_PID_GH5_2))		
				pArList->Add(temp);
			else if(1<temp.Find(SDI_PID_GH5_3))		
				pArList->Add(temp);*/
			pArList->Add(temp);
		}
	}	

done:
	if(lpDevicePath)
		LocalFree(lpDevicePath);
	if(pInterfaceDetailData)
		LocalFree(pInterfaceDetailData);    
	bResult = SetupDiDestroyDeviceInfoList(hDeviceInfo);	
	if(bResult) 
		return SDI_ERR_OK;
	return bResult;
}

//--------------------------------------------------------------------------
//!@brief	SdiInitSession
//
//!@param	none
//!@return	SDI_ERR_OK on success, any other value means failure.	
//--------------------------------------------------------------------------
SdiResult CSdiSingleMgr::SdiInitSession(SdiChar *strDevName)
{
	if(strDevName==NULL) 
		return SDI_ERR_INVALID_PARAMETER;
	return m_pSdiCore->SdiInitSession(strDevName);
}

//--------------------------------------------------------------------------
//!@brief	SdiInitSession
//
//!@param	none
//!@return	SDI_ERR_OK on success, any other value means failure.	
//--------------------------------------------------------------------------
SdiResult CSdiSingleMgr::GetProductInfo(SdiString* strModel, SdiString* strID)
{
	*strModel	= GetDeviceProduct();
	*strID		= GetDeviceUniqueID();
	return SDI_ERR_OK;
}

//------------------------------------------------------------------------------ 
//! @brief		SdiCloseSession
//! @date		2010-05-24
//! @author		hongsu.jung
//! @note	 	Terminate session.
//------------------------------------------------------------------------------
SdiResult CSdiSingleMgr::SdiCloseSession()
{
	return m_pSdiCore->SdiCloseSession();
}

//------------------------------------------------------------------------------ 
//! @brief		SdiSetReceiveHandler
//! @date		2011-07-12
//! @author		jeansu.kim
//! @note	 	Set Window Handler that Receive Message.
//------------------------------------------------------------------------------ 

SdiVoid CSdiSingleMgr::SdiSetReceiveHandler(HWND hRecvWnd)
{
	m_hRecvWnd = hRecvWnd;
	if(m_pLiveviewConverter!=NULL) 
		m_pLiveviewConverter->SdiSetReceiveHandler(hRecvWnd);
	return;
}

//--------------------------------------------------------------------------
//!@brief	SdiAddMsg
//!@param   
//!@return	
//--------------------------------------------------------------------------
SdiResult CSdiSingleMgr::SdiAddMsg(SdiMessage* pMsg)
{
	if(!m_bThreadFlag)
		return SDI_ERR_SEMAPHORE_TIMEOUT;

	//-- 2011-07-22 jeansu : for string parameter copy.
	switch(pMsg->message)
	{
	//case WM_SDI_OP_MOVIE_IMAGE_RECEIVE:
	//case WM_SDI_OP_CAPTURE_IMAGE_RECEIVE:
	//	if(pMsg->pParam)
	//	{
	//		CString strTemp = (TCHAR *)pMsg->pParam;
	//		TCHAR *pTChar = new TCHAR[1+ strTemp.GetLength()];
	//		_tcscpy(pTChar, strTemp);
	//		pMsg->pParam = (LPARAM)pTChar;			
	//	}
	//	break;
	case WM_SDI_OP_SET_PROPERTY_VALUE:
		{
			if(m_nModel != SDI_MODEL_GH5) 
			{
				switch(pMsg->nParam1)
				{
			
				case SDI_DPC_IMAGESIZE:			
				case SDI_DPC_SAMSUNG_FW_VERSION:
				case SDI_DPC_SAMSUNG_DATETIMETIMEZONE:
				case SDI_DPC_DATETIME:
					if(pMsg->pParam)
					{
						CString strTemp;
						strTemp.Format(_T("%s") , pMsg->pParam);

						if(pMsg->pParam)
						{
							delete (void*)pMsg->pParam;
							pMsg->pParam = NULL;
						}
						/*TRACE(_T("######### SET IMAGE_SIZE %d #########\n"), 1+ strTemp.GetLength());

						if(1+ strTemp.GetLength() == 3)
							TRACE(_T("######### SET IMAGE_SIZE %s #########\n"), strTemp);*/
						TCHAR *pTChar = new TCHAR[1+ strTemp.GetLength()];
						_tcscpy(pTChar, strTemp);
						pMsg->pParam = (LPARAM)pTChar;
					}
					break;
				}
			}
		}
		break;
	default:
		break;
	}
	
	if(m_arMsg.Add((CObject *)pMsg) == -1)
		return SDI_ERR_INTERNAL;
	return SDI_ERR_OK;
}

//------------------------------------------------------------------------------ 
//! @brief		GetDateMilliTime
//! @date		2011-07-07
//! @author	hongsu.jung
//! @note	 	Get Milli Second Time
//------------------------------------------------------------------------------ 
CString CSdiSingleMgr::GetDateMilliTime()
{
	struct _timeb milli_time;
	_ftime( &milli_time );	

	CString strDate = _T("");
	CTime time  = CTime::GetCurrentTime();
	int nYear = time.GetYear();
	int nMonth = time.GetMonth();
	int nDay = time.GetDay();
	int nHour = time.GetHour();
	int nMin = time.GetMinute();
	int nSec = time.GetSecond();
	int nMilliSec = milli_time.millitm;

	strDate.Format(_T("%d%02d%02d_%02d%02d%02d.%03d"), nYear, nMonth, nDay, nHour, nMin ,nSec, nMilliSec);
	return strDate;
}

void CSdiSingleMgr::SetLiveview(BOOL b)
{
	TRACE(_T("Set Liveview [%d]\n"),b);
	if(m_pLiveviewConverter)
	{
		TRACE(_T("Set Liveview Converter [%d]\n"),b);
		m_pLiveviewConverter->SetSetting(b); 
	}
	else
	{
		TRACE(_T("[Fail ]Set Liveview Converter [%d]\n"),b);
	}
	m_bLiveview = b;	
	m_bPreLiveview = b;	
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-05-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CSdiSingleMgr::SetDeviceLocation()
{
	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	CString strIP;
	PHOSTENT hostinfo;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 )
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				strIP = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}
		}      
		WSACleanup( );
	} 
	
	//-- 2013-05-06 hongsu@esmlab.com
	//-- Set Device IP Address 
	SetDeviceLocation(strIP);
}
//--------------------------------------------------------------------------
//!@brief	RUN THREAD
//!@param   
//!@return	
//--------------------------------------------------------------------------
int g_nCount = 0;
int CSdiSingleMgr::Run(void)
{
	WriteLog(_T("CSdiSingleMgr::Run(void)"));
	int nMsgIndex;	
	int nResult;
	RSEvent* pMsg = NULL;
	RSEvent* pMsgToMain = NULL;
	RSEvent* pMsgSub = NULL;

	int nLiveviewCnt = 0;	
	int nCount = 0;
	int nTimeGap;
	BOOL bEventPolling = FALSE;
	struct _timeb timeLive[2];
	struct _timeb timeEvent[2];

	m_bThreadRun = TRUE;
	m_bAutoDelete = FALSE;
	//-- 2013-12-01 hongsu@esmlab.com
	m_bStop	= FALSE;

	//TRACE(_T("[SdiSingleMgr][%d] Start Run\n"),GetConnectID());

	_ftime( &timeEvent[0] );
	_ftime( &timeLive[0] );	

	while(m_bThreadFlag)
	{
		//-- 2013-03-01 honsgu.jung
		//-- For CPU
		Sleep(10);

		//-- 2013-12-05 hongsu@esmlab.com
		//-- Critical Section 
		EnterCS();

		nMsgIndex = m_arMsg.GetSize();
		while(nMsgIndex--)
		{
			Sleep(10);
			//-- Get First Message
			pMsg = (RSEvent*)m_arMsg.GetAt(0);
			if(!pMsg)
			{
				m_arMsg.RemoveAt(0);
				continue;
			}

			// TRACE(_T("CSdiSingleMgr::Run [<---] [%s][%04x] \n"), GetDeviceDSCID(), pMsg->message);
			switch(pMsg->message)
			{
			case WM_RS_MC_OPEN_SESSION:
				PTP_OpenSession();
				break;
			case WM_RS_MC_CLOSE_SESSION:
				PTP_CloseSession();
				break;
			case WM_RS_MC_GET_PTP_DEVICEINFO:
				nResult = PTP_GetPTPDeviceInfo();
				break;
			case WM_RS_MC_GET_RECORD_STATUS:
				nResult = PTP_GetRecordStatus();
				pMsgToMain = new RSEvent();	
				pMsgToMain->message	= WM_RS_GET_RECORD_STATUS;
				pMsgToMain->nParam1 = nResult;
				::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_GET_RECORD_STATUS, (LPARAM)pMsgToMain);

				break;
			case WM_RS_MC_GET_CAPTURE_COUNT:
				nResult = PTP_GetCaptureCount();
				pMsgToMain = new RSEvent();	
				pMsgToMain->message	= WM_RS_GET_CAPTURE_COUNT;
				pMsgToMain->nParam1 = nResult;
				::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_GET_CAPTURE_COUNT, (LPARAM)pMsgToMain);

				break;
			case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1:		
				//if(m_strModel != _T("NX3000"))
					PTP_SendCapture_Require(S1_PRESS);		
				break;
			case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2:		
				//if(m_strModel != _T("NX3000"))
					PTP_SendCapture_Require(S2_PRESS);		
				break;
			case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_3:		
				PTP_SendCapture_Require(S2_RELEASE);	
				break;
			case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4:		
				//if(m_strModel != _T("NX3000"))
					PTP_SendCapture_Require(S1_RELEASE);	
				break;
			case WM_RS_MC_CAPTURE_IMAGE_RECORD:		// gh5
				{
					pMsgToMain = new RSEvent();
					pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_RECORD;
					pMsgToMain->pParent = (LPARAM)m_pParent;
					pMsgToMain->nParam3 = FALSE;		// PTP_SendCapture_Require complete result status
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_RECORD, (LPARAM)pMsgToMain);

					int nStatus = pMsg->nParam1;
					int nInitGH5Result = 0;
					int nResult = PTP_GetRecordStatus();	

#if 0
					if(nResult == 0)
					{
						nInitGH5Result = -1;
						nResult = PTP_SendCapture_Require(RECORD_STOP);
						TRACE(_T("###Recording\n"));
					}
					else
					{
						nInitGH5Result = PTP_InitGH5();

						nResult = PTP_SendCapture_Require(RECORD_START);
						TRACE(_T("###Stand by\n"));
					}
#else
					if (nStatus == 1)		// start
					{
						if(nResult != 0)
						{
							nInitGH5Result = PTP_InitGH5();

							nResult = PTP_SendCapture_Require(RECORD_START);
							TRACE(_T("###Stand by\n"));
						}
 						else
 						{
 							nResult = -1;
 						}
					}
					else
					{
						nInitGH5Result = -1;
						nResult = PTP_SendCapture_Require(RECORD_STOP);
						TRACE(_T("###Recording\n"));
					}
#endif

					pMsgToMain = new RSEvent();
					pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_RECORD;
					pMsgToMain->pParent = (LPARAM)m_pParent;
					pMsgToMain->nParam1 = nResult;
					pMsgToMain->nParam2 = nInitGH5Result;		// GH5 Init rec result
					pMsgToMain->nParam3 = TRUE;			// PTP_SendCapture_Require complete result status
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_RECORD, (LPARAM)pMsgToMain);
				}			
				break;
				//-- 2013-04-24 hongsu.jung
				//-- movie rec button
			case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_5:		
				{
					PTP_HiddenCommand(10,2);
					int nResult = PTP_SendCapture_Require(S3_PRESS);	
					if(nResult != SDI_ERR_OK)
					{
						pMsgToMain = new RSEvent();
						pMsgToMain->message	= WM_SDI_RESULT_SENDCAPTUREFAIL;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent	= (LPARAM)m_pParent;
						::PostMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_SENDCAPTUREFAIL, (LPARAM)pMsgToMain);	
					}
					//TRACE(_T("WM_RS_MC_CAPTURE_IMAGE_REQUIRE_5 [%s]: %d\n"), GetDeviceDSCID(), nResult);
					break;
				}
			case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_6:		
				{
					//-- 2014-09-10 hongsu@esmlab.com
					//-- No more Capture 6
					nResult = PTP_SendCapture_Require(S3_RELEASE);	
					TRACE(_T("[No more] WM_RS_MC_CAPTURE_IMAGE_REQUIRE_6 [%s]: %d\n"), GetDeviceDSCID(), nResult);
					
					break;
				}
			case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_ONCE:	PTP_SendCapture_Once();					break;
			case WM_RS_MC_RECORD_CANCLE:				PTP_MovieCancle();						break;
			case WM_RS_MC_RECORD_MOVIE:
				{

					//-- 2011-7-18 Lee JungTaek
					//-- Revision To Set File Name
					TCHAR* cValue = (TCHAR*)pMsg->pParam;
					CString strName= _T("");
					strName.Format(_T("%s"), cValue);
					if( cValue)
					{
						delete cValue;
						cValue = NULL;
					}

					bool bIsLast = false;
					//nResult = PTP_SendMovie_Complete(strName, bIsLast); //-- 2014-09-19 joonho.kim
					nResult = PTP_SendMovie_Complete(strName, bIsLast, pMsg->nParam1, pMsg->nParam2);
					Sleep(100);
					PTP_FileReceiveComplete(); //-- 2014-09-20 joonho.kim // Receive Complete Event Message

					if(nResult==SDI_ERR_OK)
					{
						//-- 2011-09-01 jeansu : Set Capturing False and Restart Liveview
						if(bIsLast) 
							SetCapturing(FALSE);
						//-- 2011-09-01 jeansu : MagicFrame 의 경우, 다른 모드와 다르게 Liveview 처리 및 생성 시간이 좀 더 걸리므로 고려하여 Delay 줌.
						if(this->GetCurModeIndex()==eUCS_UI_MODE_MAGIC_MAGIC_FRAME) 
							Sleep(1000);
						else 
							Sleep(500);
					}
					else if(nResult==SDI_ERR_SEMAPHORE_TIMEOUT)
					{	//--2011-08-31 jeansu : ERROR_SEM_TIMEOUT Disconnect 처리.
						m_bThreadFlag = FALSE;
						if(m_pLiveviewConverter)
						{
							m_pLiveviewConverter->ExitInstance();
							delete m_pLiveviewConverter;
							m_pLiveviewConverter = NULL;
						}
					}

			/*		//-- 2011-08-31 jeansu : Image 저장시 Parameter 로 Filename 넘겨주기.
					CString *retName = new CString(strName);
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_RS_LV_UPDATE_TUMBNAIL;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					pMsgToMain->pParam = (LPARAM)retName;
					//-- 2011-05-31 hongsu.jung
					//-- Send To Main (Refresh Thumbnail)
					::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_LV_UPDATE_TUMBNAIL, (LPARAM)pMsgToMain);		*/			
				}
				break;
			case WM_RS_MC_CAPTURE_IMAGE:
				{
					//-- 2011-7-18 Lee JungTaek
					//-- Revision To Set File Name
					//TCHAR* cValue = (TCHAR*)pMsg->pParam;
					CString strName= _T("");
					//strName.Format(_T("%s"), cValue);

					/*switch(GetQualityType())
					{
					case RS_QUALITY_TYPE_JPG:
						strName.Format(_T("%s.jpg"), pMsg->pParam);									
						break;
					case RS_QUALITY_TYPE_RAW:
					case RS_QUALITY_TYPE_JPGRAW:
						strName.Format(_T("%s.srw"), pMsg->pParam);
						break;
					default:
						strName.Format(_T("%s"), pMsg->pParam);
					}*/	

					if(pMsg->nParam3 == 0)
						strName.Format(_T("%s.jpg"), pMsg->pParam);									
					else
						strName.Format(_T("%s.srw"), pMsg->pParam);									

					//CMiLRe 20141113 메모리 릭 제거
					TCHAR* cValue = (TCHAR*)pMsg->pParam;
					if( cValue)
					{
						delete cValue;
						cValue = NULL;
					}

					bool bIsLast = false;
					//nResult = PTP_SendCapture_Complete(strName, bIsLast);
					nResult = PTP_ImageTransfer( strName, pMsg->nParam1, pMsg->nParam2);
					Sleep(100);
					PTP_ImageReceiveComplete();
					Sleep(100);
					PTP_ImageReceiveComplete();

					if(nResult==SDI_ERR_OK)
					{
						//-- 2011-09-01 jeansu : Set Capturing False and Restart Liveview
						if(bIsLast) 
							SetCapturing(FALSE);
						//-- 2011-09-01 jeansu : MagicFrame 의 경우, 다른 모드와 다르게 Liveview 처리 및 생성 시간이 좀 더 걸리므로 고려하여 Delay 줌.
						if(this->GetCurModeIndex()==eUCS_UI_MODE_MAGIC_MAGIC_FRAME) 
							Sleep(1000);
						else 
							Sleep(500);
					}
					else if(nResult==SDI_ERR_SEMAPHORE_TIMEOUT)
					{	//--2011-08-31 jeansu : ERROR_SEM_TIMEOUT Disconnect 처리.
						m_bThreadFlag = FALSE;
						if(m_pLiveviewConverter)
						{
							m_pLiveviewConverter->ExitInstance();
							delete m_pLiveviewConverter;
							m_pLiveviewConverter = NULL;
						}
					}

					//-- 2011-08-31 jeansu : Image 저장시 Parameter 로 Filename 넘겨주기.
					/*CString *retName = new CString(strName);
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_RS_LV_UPDATE_TUMBNAIL;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					pMsgToMain->pParam = (LPARAM)retName;
					//-- 2011-05-31 hongsu.jung
					//-- Send To Main (Refresh Thumbnail)
					::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_LV_UPDATE_TUMBNAIL, (LPARAM)pMsgToMain);*/					
				}
				break;
				//-- 2011-06-10 hongsu.jung
			case WM_RS_MC_LIVEVIEW_INFO:
				{
					//-- 2013-05-01 hongsu@esmlab.com
					//-- Wait Change Mode 
					Sleep(200);

					//-- Create LiveviewInfo* pInfo 
					RSLiveviewInfo* pLiveviewInfo = new RSLiveviewInfo;
					PTP_LiveviewInfo(pLiveviewInfo);

					//-- Set Liveview Infomation
					if(m_pLiveviewConverter!=NULL) 
						m_pLiveviewConverter->ChangeSize	(pLiveviewInfo);

					//-- 2011-08-25 jeansu : Photo Size 변경시 Liveview 에 줄이 생기는 문제. RemoteStudioDlg 에서 False 한것을 풀어줌.
					// SetLiveview(TRUE);

					//-- Sen Liveview Info
					pMsgToMain = new RSEvent();	
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_RS_LV_GET_INFO;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					::SendMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_LV_GET_INFO, (LPARAM)pMsgToMain);
				}
				break;			
				//-- 2011-06-09 hongsu.jung
			case WM_RS_GET_PROPERTY_INFO:
				//-- Get Property
				PTP_GetDevPropDesc(pMsg->nParam1);
				if(pMsg->nParam1==PTP_CODE_FUNCTIONALMODE){
					IDeviceInfo *pDeviceInfo = NULL;
					m_pSdiCore->SdiGetDeviceInfo(pMsg->nParam1, &pDeviceInfo);
					this->m_nModeIndex = pDeviceInfo->GetCurrentEnum();
				}
				else if(pMsg->nParam1 == PTP_CODE_SAMSUNG_PHYSICAL_MODE){
					IDeviceInfo *pDeviceInfo = NULL;
					m_pSdiCore->SdiGetDeviceInfo(pMsg->nParam1, &pDeviceInfo);
					this->m_nPhysicalMode = pDeviceInfo->GetCurrentEnum();
				}

				//-- Send Update Message
				pMsgToMain = new RSEvent();
				//-- 2013-04-24 hongsu.jung
				//-- Indicator (message)
				pMsgToMain->message	= WM_RS_UPDATE_PROPERTY_VALUE;
				pMsgToMain->pDest	= (LPARAM)this;
				pMsgToMain->pParent	= (LPARAM)m_pParent;
				pMsgToMain->nParam1 = pMsg->nParam1;				
				::SendMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_UPDATE_PROPERTY_VALUE, (LPARAM)pMsgToMain);
				break;			
			case WM_RS_SP_SET_PROPERTY_VALUE:
				PTP_SetDevPropDesc((int)pMsg->nParam1, (int)pMsg->nParam2, (LPARAM)pMsg->pParam);
				break;

			//CMiLRe 20141001 Date, Time 추가
			case WM_RS_OPT_DATE_N_TIME_UPDATE:
				{
					IDeviceInfo *pDeviceInfo = NULL;
					m_pSdiCore->SdiGetDeviceInfo(pMsg->nParam1, &pDeviceInfo);

					this->m_nModeIndex = pDeviceInfo->GetCurrentEnum();
					pMsgToMain = new RSEvent();
					pMsgToMain->message	= WM_RS_UPDATE_PROPERTY_VALUE;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = pMsg->nParam1;				
					::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_UPDATE_PROPERTY_VALUE, (LPARAM)pMsgToMain);
				}
				break;
				//-- 2013-11-21 hongsu@esmlab.com
				//-- FOCUS VALUE WM_RS_SET_FOCUS_VALUE
			case WM_RS_SET_FOCUS_VALUE:
				PTP_SetFocusPosition((int)pMsg->nParam1);
				break;
			case WM_RS_LIVEVIEW_SET_FOCUSPOSITION:
				{
					//-- Create Focus For Setting
					FocusPosition* pFocus = new FocusPosition();
					PTP_SetFocusPosition(pFocus, (int)pMsg->nParam1);
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_RS_LIVEVIEW_GET_FOCUSPOSITION;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->pParam = (LPARAM)pFocus;
					::SendMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_LIVEVIEW_GET_FOCUSPOSITION, (LPARAM)pMsgToMain);					
				}
				break;
			case WM_RS_OPT_FORMAT_DEVICE:
				PTP_FormatDevice();
				break;
			case WM_RS_OPT_RESET_DEVICE:
				PTP_ResetDevice(pMsg->nParam1);
				break;
			//dh0.seo 2014-11-04
			case WM_RS_OPT_SENSOR_CLEANING_DEVICE:
				PTP_SensorCleaningDevice();
				break;
			//CMiLRe 20141113 IntervalCapture Stop 추가
			case WM_RS_STOP_INTERVAL_CAPUTRE:
				PTP_IntervalCaptureStop();
				break;
			//CMiLRe 20141127 Display Save Mode 에서 Wake up
			case WM_RS_DISPLAY_SAVE_MODE_WAKE_UP:
				PTP_DisplaySaveModeWakeUp();
				break;
				//-- 2014-9-4 hongsu@esmlab.com
				//-- F/W Update
			case WM_RS_OPT_FW_UPDATE:
				PTP_FWUpdate();
				break;
			case WM_RS_OPT_DEVICE_REBOOT:
				PTP_DeviceReboot();
			//20150128 CMiLRe NX3000
			//CMiLRe 2015129 펌웨어 다운로드 경로 추가
			case WM_RS_OPT_FW_DOWNLOAD:
				{
					CString strPath; 
					strPath.Format(_T("%s"), pMsg->pParam);
					PTP_FWDownload(strPath);
				}
				break;
			case WM_RS_OPT_DEVICE_POWER_OFF:
				PTP_DevicePowerOff();
				break;
				//-- 2013-10-23 hongsu@esmlab.com
				//-- Waiting Event 
			case WM_RS_SET_TICK:
				{	
					int nResult = PTP_SetTick((double)pMsg->nParam1, pMsg->nParam2);
					TRACE(_T("PTP_SetTick [%s]: %d\n"), GetDeviceDSCID(), nResult);
				}
				break;
			case WM_RS_SET_PAUSE:
				{
					int nResult = PTP_SetPause(pMsg->nParam1);
					TRACE(_T("PTP_SetPause [%s]: %d\n"), GetDeviceDSCID(), nResult);
				}
				break;
			case WM_RS_SET_RESUME:
				{
					int nResult = PTP_SetResume(pMsg->nParam1);
					TRACE(_T("PTP_SetResume [%s]: %d\n"), GetDeviceDSCID(), nResult);
				}
				break;
			case WM_RS_SLEEP:
				Sleep(pMsg->nParam1);
				break;
			case WM_RS_CONTROL_TOUCH_AF:
				PTP_ControlTouchAF(pMsg->nParam1, pMsg->nParam2);
				break;
			case WM_RS_TRACKING_AF_STOP:
				PTP_TrackingAFStop();
				break;
				//--------------------------------------------------------------------
				//-- 
				//-- 
				//-- For SdiSDK
				//-- 
				//-- 
				//--------------------------------------------------------------------
			case WM_SDI_OP_OPEN_SESSION:
				nResult = PTP_OpenSession();				
				if(m_hRecvWnd!=NULL)
				{
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_OPEN_SESSION;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();					
					pMsgToMain->nParam2 = nResult;

					//-- 2013-11-04 hongsu@esmlab.com
					//-- Model Name
					CString *retModel = new CString(GetDeviceModel());
					pMsgToMain->pParam = (LPARAM)retModel;
					::PostMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_OPEN_SESSION, (LPARAM)pMsgToMain);	
				}
				break;

			case WM_SDI_OP_CLOSE_SESSION:
				nResult = PTP_CloseSession();
				if(m_hRecvWnd!=NULL)
				{
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_CLOSE_SESSION;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CLOSE_SESSION, (LPARAM)pMsgToMain);	
				}
				break;

			case WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_1:
				nResult = PTP_SendCapture_Require(S1_PRESS);
				if(m_hRecvWnd!=NULL)
				{
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_1;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_1, (LPARAM)pMsgToMain);	
				}
				break;

			case WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_2:
				//--2011-07-22 jeansu
				m_bLiveview = FALSE;
				if(m_pLiveviewConverter) 
					m_pLiveviewConverter->SetSetting(FALSE);
				//-- 2013-11-05 hongsu@esmlab.com
				//-- NX2000 --> Need S1 Press
				if(m_strModel == "NX2000")
					PTP_SendCapture_Require(S1_PRESS);
				nResult = PTP_SendCapture_Require(S2_PRESS);
				if(m_hRecvWnd!=NULL)
				{
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_2;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_2, (LPARAM)pMsgToMain);	
				}	
				break;

			case WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_3:
				nResult = PTP_SendCapture_Require(S2_RELEASE);
				if(m_hRecvWnd!=NULL)
				{
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_3;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_3, (LPARAM)pMsgToMain);	
				}
				break;

			case WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_4:
				SetAFDetect(SDI_AF_NULL);
				nResult = PTP_SendCapture_Require(S1_RELEASE);
				if(m_hRecvWnd!=NULL)
				{
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_4;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_4, (LPARAM)pMsgToMain);	
				}
				break;

			case WM_SDI_OP_CAPTURE_IMAGE_ONCE:
				nResult = PTP_SendCapture_Once(pMsg->nParam1);
				if(m_hRecvWnd!=NULL)
				{
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_ONCE;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_ONCE, (LPARAM)pMsgToMain);	
				}
				break;
			case WM_SDI_OP_GH5_INITIALIZE:
				{
					int nRet = PTP_InitGH5();

					if(m_hRecvWnd!=NULL)
					{
						pMsgToMain = new RSEvent();
						pMsgToMain->message	= WM_SDI_RESULT_GH5_INITIALIZE;
						pMsgToMain->pParent = (LPARAM)m_pParent;
						pMsgToMain->nParam1 = nRet;		// response code
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_GH5_INITIALIZE, (LPARAM)pMsgToMain);
					}		
				}
				break;
			case WM_SDI_OP_GET_TICK:
				{
					int tPC = 0;
					double tDSC = 0.0;
					int nCheckCnt, nCheckTime;
					//int nRet = PTP_GetTick(tPC, tDSC, nCheckCnt, nCheckTime, 5);
					int nRet = PTP_GetTick(tPC, tDSC, nCheckCnt, nCheckTime, 5, m_hRecvWnd);
					m_nPCTime = tPC;
					m_nDSCTime = tDSC;

#ifdef SAMPLE
					// sample result log
					RSEvent* _pMsgToMain = new RSEvent();
					_pMsgToMain->message	= WM_SDI_RESULT_GET_TICK_2;
					_pMsgToMain->pParent = (LPARAM)m_pParent;
					_pMsgToMain->nParam1 = nRet;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_GET_TICK_2, (LPARAM)_pMsgToMain);	
#endif

					/*if(m_hRecvWnd!=NULL)
					{
						m_nPCTime = tPC;
						m_nDSCTime = tDSC;
						pMsgToMain = new RSEvent();
						//-- 2013-04-24 hongsu.jung
						//-- Indicator (message)
						pMsgToMain->message	= WM_SDI_RESULT_GET_TICK;
						pMsgToMain->nParam1 = tPC;
						pMsgToMain->nParam2 = tDSC;
						pMsgToMain->nParam3 = nCheckTime;
						pMsgToMain->pParam = (LPARAM)nCheckCnt;
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_GET_TICK, (LPARAM)pMsgToMain);	
					}*/
					//tPC = ESMGetTick();
				}
				break;
			case WM_SDI_OP_SET_TICK:
				{
					//UINT nResponseP1 = 0;
					//SdiUIntArray* pArList[2] = {NULL, NULL};
					SdiUIntArray* pArList = new SdiUIntArray;
					pArList->SetSize(2);
					int nRet = PTP_SetTick(pMsg->nParam1, pMsg->nParam2, pArList);
					UINT nParam1 = pArList->GetAt(0);
					UINT nParam2 = pArList->GetAt(1);

					if(m_hRecvWnd!=NULL)
					{
						pMsgToMain = new RSEvent();
						pMsgToMain->message	= WM_SDI_RESULT_SET_TICK;
						pMsgToMain->pParent = (LPARAM)m_pParent;
						pMsgToMain->nParam1 = nRet;		// response code
						pMsgToMain->nParam2 = (UINT)nParam1;	// response param1 > tick -1
						pMsgToMain->nParam3 = (UINT)nParam2;	// response param2 > settick code
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_SET_TICK, (LPARAM)pMsgToMain);
					}					
					
 					if (pArList != NULL)
 					{
						pArList->RemoveAll();
 						delete pArList;
 					}
				}
				break;
			case WM_SDI_OP_CAPTURE_IMAGE_RECORD:
				{				
					m_bErr = FALSE;
					//PTP_InitGH5();
					//Sleep(1000);
					int nResultStatus = PTP_GetRecordStatus();

					
					nResult = PTP_SendCapture_Require(RECORD_START);

					if(m_hRecvWnd!=NULL)
					{
						pMsgToMain = new RSEvent();
						pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_RECORD;
						pMsgToMain->pParent = (LPARAM)m_pParent;
						pMsgToMain->nParam1 = nResult;
						pMsgToMain->nParam2 = nResultStatus;
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_RECORD, (LPARAM)pMsgToMain);

						TRACE(_T("##### WM_SDI_OP_CAPTURE_IMAGE_RECORD...2222 %d"), nResult);
					}
					else
					{
						TRACE(_T("###### WM_SDI_OP_CAPTURE_IMAGE_RECORD...3333 %d"), nResult);
					}
				}
				break;
			case WM_SDI_OP_CAPTURE_IMAGE_RECORD_STOP:
				{
					int nResultStatus = PTP_GetRecordStatus();
					nResult = PTP_SendCapture_Require(RECORD_STOP);

					if(m_hRecvWnd!=NULL)
					{
						pMsgToMain = new RSEvent();
						pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_RECORD_STOP;
						pMsgToMain->pParent = (LPARAM)m_pParent;
						pMsgToMain->nParam1 = nResult;
						pMsgToMain->nParam2 = nResultStatus;
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_RECORD_STOP, (LPARAM)pMsgToMain);
					}
				}
				break;
				//-- 2013-04-24 hongsu.jung
				//-- MOVIE REC BUTTON PRESS
			case WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_5:
				nResult = PTP_SendCapture_Require(S3_PRESS);
				if(m_hRecvWnd!=NULL)
				{
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_5;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_5, (LPARAM)pMsgToMain);	
				}
				break;

				//-- 2013-04-24 hongsu.jung
				//-- MOVIE REC BUTTON RELEASE
			case WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_6:
				nResult = PTP_SendCapture_Require(S3_RELEASE);
				if(m_hRecvWnd!=NULL)
				{
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_6;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::PostMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_6, (LPARAM)pMsgToMain);
				}
				break;

			case WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_7:
				nResult = PTP_SendCapture_Require(WAIT_MODE);
				if(m_hRecvWnd!=NULL)
				{
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_7;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_7, (LPARAM)pMsgToMain);
				}
				break;

			case WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_8:
				nResult = PTP_SendCapture_Require(WAIT_EXIT);
				if(m_hRecvWnd!=NULL)
				{
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_8;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_8, (LPARAM)pMsgToMain);
				}
				break;
			/*case WM_SDI_OP_FILE_TRANSFER:
				//nResult = PTP_FileTransfer( pMsg->nParam1, pMsg->nParam2 , pMsg->nParam3);
				if(m_hRecvWnd!=NULL)
				{
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_FILE_TRANSFER;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::PostMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_FILE_TRANSFER, (LPARAM)pMsgToMain);	
				}
				break;*/
			case WM_SDI_OP_MOVIE_IMAGE_RECEIVE:
				/*nResult = PTP_MovieTransfer( pMsg->nParam1, pMsg->nParam2 );
				if(m_hRecvWnd!=NULL)
				{
					pMsgToMain = new RSEvent();
					pMsgToMain->message	= WM_SDI_RESULT_MOVIE_TRANSFER;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::PostMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_MOVIE_TRANSFER, (LPARAM)pMsgToMain);	
				}
				break;*/
				{
					//-- 2011-7-18 Lee JungTaek
					//-- Revision To Set File Name
					TCHAR* cValue = (TCHAR*)pMsg->pParam;
					CString strName= _T("");
					strName.Format(_T("%s.mp4"), cValue);
					if( cValue)
					{
						delete cValue;
						cValue = NULL;
					}

					bool bIsLast = false;
					//nResult = PTP_SendMovie_Complete(strName, bIsLast); //-- 2014-09-19 joonho.kim
					nResult = PTP_SendMovie_Complete(strName, bIsLast, pMsg->nParam1, pMsg->nParam2);
					Sleep(100);
					PTP_FileReceiveComplete(); //-- 2014-09-20 joonho.kim // Receive Complete Event Message

					if(nResult==SDI_ERR_OK)
					{
						//-- 2011-09-01 jeansu : Set Capturing False and Restart Liveview
						if(bIsLast) 
							SetCapturing(FALSE);
						//-- 2011-09-01 jeansu : MagicFrame 의 경우, 다른 모드와 다르게 Liveview 처리 및 생성 시간이 좀 더 걸리므로 고려하여 Delay 줌.
						if(this->GetCurModeIndex()==eUCS_UI_MODE_MAGIC_MAGIC_FRAME) 
							Sleep(1000);
						else 
							Sleep(500);
					}
					else if(nResult==SDI_ERR_SEMAPHORE_TIMEOUT)
					{	//--2011-08-31 jeansu : ERROR_SEM_TIMEOUT Disconnect 처리.
						m_bThreadFlag = FALSE;
						
						if(m_pLiveviewConverter)
						{
							m_pLiveviewConverter->ExitInstance();
							delete m_pLiveviewConverter;
							m_pLiveviewConverter = NULL;
						}
					}	
				}
				break;
			case WM_SDI_OP_CAPTURE_IMAGE_RECEIVE:
				{
					//-- Create File Name
					CString strFileName;
					strFileName.Format(_T("%s"),pMsg->pParam);					
					BOOL bFullName = (BOOL)pMsg->nParam1;					

					if(pMsg->pParam==NULL)
					{
						nResult = SDI_ERR_INVALID_PARAMETER;
					}
					else
					{						
						bool bIsLast = false;
						if(pMsg->message == WM_SDI_OP_CAPTURE_IMAGE_RECEIVE)
						{
							if(m_pSdiCore->m_nModel == SDI_MODEL_GH5)
							{
								switch(pMsg->nParam3)
								{
								case RS_QUALITY_TYPE_JPG:
									strFileName.Format(_T("%s.JPG"), pMsg->pParam);
									strFileName.Replace(_T("@"), _T(".JPG@"));
									break;
								case RS_QUALITY_TYPE_RAW:
									strFileName.Format(_T("%s.RW2"), pMsg->pParam);
									strFileName.Replace(_T("@"), _T(".RW2@"));
									break;
								default:
									strFileName.Format(_T("%s.JPG"), pMsg->pParam);
									strFileName.Replace(_T("@"), _T(".JPG@"));
									break;
								}
							}
							else //NX Series
							{
								switch(GetQualityType())
								{
								case RS_QUALITY_TYPE_JPG:
									strFileName.Format(_T("%s.jpg"), pMsg->pParam);
									strFileName.Replace(_T("@"), _T(".jpg@"));
									/*if(bFullName)	strFileName.Format(_T("%s.jpg"), pMsg->pParam);
									else			strFileName.Format(_T("%s\\%s_#%d.jpg"), pMsg->pParam, GetDateMilliTime(), GetConnectID());		*/
									TRACE(_T("###############RS_QUALITY_TYPE_JPG\r\n"));
									break;
								case RS_QUALITY_TYPE_RAW:
								case RS_QUALITY_TYPE_JPGRAW:
									strFileName.Format(_T("%s.srw"), pMsg->pParam);
									strFileName.Replace(_T("@"), _T(".srw@"));
									/*if(bFullName)	strFileName.Format(_T("%s.srw"), pMsg->pParam);
									else			strFileName.Format(_T("%s\\%s_#%d.srw"), pMsg->pParam, GetDateMilliTime(), GetConnectID());*/
									TRACE(_T("###############RS_QUALITY_TYPE_JPGRAW\r\n"));
									break;
								default:
									{
										strFileName.Format(_T("%s.jpg"), pMsg->pParam);
										strFileName.Replace(_T("@"), _T(".jpg@"));
										TRACE(_T("###############defaultW\r\n"));
									}
								}	
							}
							
							//NX3000 20150119 ImageTransfer
							if(m_strModel == "NX3000")
							{
								nResult = PTP_SendCapture_Complete(strFileName, bIsLast);
								//WriteLog(strFileName);
							}
							else if(m_strModel == "NX1")
							{
								nResult = PTP_ImageTransfer( strFileName, pMsg->nParam2, pMsg->nParam3 );
								Sleep(1000);
								PTP_ImageReceiveComplete();
							}
							else if(m_strModel == "GH5")
							{
								nResult = PTP_ImageTransfer( strFileName, 0, 0 );
								//WriteLog(strFileName);
							}
							else
								nResult = PTP_SendCapture_Complete(strFileName, bIsLast, pMsg->nParam2, pMsg->nParam3);
						}
						else // WM_SDI_OP_MOVIE_IMAGE_RECEIVE
						{
							//if(bFullName)	strFileName.Format(_T("%s.mp4"), pMsg->pParam);
							//else			strFileName.Format(_T("%s\\%s_#%d.mp4"), pMsg->pParam, GetDateMilliTime(), GetConnectID());
							strFileName.Format(_T("%s.mp4"), pMsg->pParam);
							//nResult = PTP_SendMovie_Complete(strFileName, pMsg->nParam1, pMsg->nParam2);
							nResult = PTP_SendMovie_Complete(strFileName, pMsg->nParam2);
						}

						//-- 2013-04-25 hongsu.jung
						//-- Set Saved FileName
						SetSavedFile(strFileName);

						//-- 2013-02-17 hongsu.jung
						//-- Finish Save File
						if(m_hRecvWnd)
						{
							pMsgToMain = new RSEvent();
							//-- 2013-04-24 hongsu.jung
							//-- Indicator (message)
							pMsgToMain->message	= WM_SDI_EVENT_SAVE_DONE;
							pMsgToMain->pDest	= (LPARAM)this;
							pMsgToMain->pParent	= (LPARAM)m_pParent;							
							pMsgToMain->nParam1 = GetConnectID();
							//-- 2013-10-02 hongsu@esmlab.com
							//-- Indicate MOVIE/JPEG 
							if(pMsg->message == WM_SDI_OP_MOVIE_IMAGE_RECEIVE)
								pMsgToMain->nParam2 = FALSE;
							else
								pMsgToMain->nParam2 = TRUE;

							pMsgToMain->pParam = (LPARAM)this;
							::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_EVENT_SAVE_DONE, (LPARAM)pMsgToMain);
						}

						m_bLiveview = m_bPreLiveview;
						//-- 2011-07-22 jeansu : Liveview restart
						if(m_pLiveviewConverter) 
							m_pLiveviewConverter->SetSetting(TRUE);
						//-- 2011-07-22 jeansu - delete string parameter
						delete ((TCHAR *)pMsg->pParam);					
					}

					if(m_hRecvWnd)
					{
						//-- 2011-05-31 hongsu.jung
						//-- Send To Main (Refresh Thumbnail)
						CString *retName = new CString(strFileName);
						pMsgToMain = new RSEvent();
						//-- 2013-04-24 hongsu.jung
						//-- Indicator (message)
						pMsgToMain->message	= WM_SDI_RESULT_CAPTURE_IMAGE_RECEIVE;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent	= (LPARAM)m_pParent;
						pMsgToMain->nParam1 = GetConnectID();
						pMsgToMain->nParam2 = nResult;
						pMsgToMain->pParam = (LPARAM)retName;
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CAPTURE_IMAGE_RECEIVE, (LPARAM)pMsgToMain);							
					}					
				}
				break;
				//-- 2011-06-10 hongsu.jung
			case WM_SDI_OP_LIVEVIEW_INFO:
				{
					//-- Create LiveviewInfo* pInfo 
					RSLiveviewInfo* pLiveviewInfo = new RSLiveviewInfo;
					//-- 2013-12-09 kcd
					//-- ChangeSize 에서 삭제될 Info 정보.
					RSLiveviewInfo* pLiveviewInfo2 = new RSLiveviewInfo;
					nResult = PTP_LiveviewInfo(pLiveviewInfo);
					memcpy(pLiveviewInfo2, pLiveviewInfo, sizeof(RSLiveviewInfo));
					//-- Set Liveview Information					
					if(nResult==SDI_ERR_OK) 
						m_pLiveviewConverter->ChangeSize(pLiveviewInfo2);

					//-- Sen Liveview Info
					//-- 2011-7-12 jeansu
					if(m_hRecvWnd)
					{
						pMsgToMain = new RSEvent;
						pMsgToMain->message	= WM_SDI_RESULT_LIVEVIEW_INFO;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent	= (LPARAM)m_pParent;
						pMsgToMain->nParam1 = GetConnectID();
						pMsgToMain->nParam2 = nResult;
						pMsgToMain->pParam = (LPARAM)pLiveviewInfo;
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_LIVEVIEW_INFO, (LPARAM)pMsgToMain);
					}
				}
				break;			
				//-- 2011-06-09 hongsu.jung
			case WM_SDI_OP_GET_PROPERTY_DESC:
				{
					if(m_pSdiCore->m_nModel == SDI_MODEL_GH5)
					{
						if(pMsg->nParam1 == PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN)
							break;
						if(pMsg->nParam1 == PTP_CODE_SAMSUNG_PICTUREWIZARD)
							break;
					}
					else // NX Series
					{
						if(pMsg->nParam1 == PTP_DPC_WB_DETAIL_KELVIN)
							break;
					}
					//-- Get Property
					nResult = PTP_GetDevPropDesc(pMsg->nParam1);
					//--2011-7-12 jeansu
					IDeviceInfo *pDeviceInfo = NULL;
					if(nResult == SDI_ERR_OK){
						if(!m_pSdiCore->SdiGetDeviceInfo(pMsg->nParam1, &pDeviceInfo)==SDI_ERR_OK) 
							TRACE(_T("[Error] SdiGetDeviceInfo\n"));
					}
					int nFps = FRAMERATE30;
					if( pDeviceInfo != NULL)
					{
						int nCurCode = pDeviceInfo->GetCurrentEnum();

						if(m_nModel == SDI_MODEL_NX1 && pDeviceInfo->GetPropCode() == PTP_CODE_SAMSUNG_MOV_SIZE)
						{
							if( nCurCode == 146)
								m_pSdiCore->SetFps(FRAMERATE12);
							else if( nCurCode == 134)
								m_pSdiCore->SetFps(FRAMERATE30);
						}
					}

					//-- Send Update Message
					if(m_hRecvWnd!=NULL){
						pMsgToMain = new RSEvent();
						//-- 2013-04-24 hongsu.jung
						//-- Indicator (message)
						pMsgToMain->message	= WM_SDI_RESULT_GET_PROPERTY_DESC;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent	= (LPARAM)m_pParent;
						pMsgToMain->nParam1 = GetConnectID();
						pMsgToMain->nParam2 = nResult;
						pMsgToMain->pParam = (LPARAM) pDeviceInfo;
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_GET_PROPERTY_DESC, (LPARAM)pMsgToMain);						
					}					
				}
				break;

			case WM_SDI_OP_SET_PROPERTY_VALUE:
				
				{
					int nType = pMsg->nParam2;
					if(m_nModel == SDI_MODEL_GH5)
					{
						Sleep(1000); //joonho.kim ?????
						if(pMsg->nParam2 <= PTP_VALUE_UINT_32)
							nType = PTP_DPV_UINT32;
						else if(pMsg->nParam2 == PTP_VALUE_STRING)
							nType = PTP_DPV_STR;
					}

					//TRACE(_T("WM_SDI_OP_SET_PROPERTY_VALUE %x %x %x\n"),pMsg->nParam1, pMsg->nParam2, (int)pMsg->pParam);
					//nResult = PTP_SetDevPropDesc((int)pMsg->nParam1, (int)pMsg->nParam2, (LPARAM)pMsg->pParam);

					TRACE(_T("WM_SDI_OP_SET_PROPERTY_VALUE %x %x %x\n"),pMsg->nParam1, nType, (int)pMsg->pParam);
					nResult = PTP_SetDevPropDesc((int)pMsg->nParam1, (int)nType, (LPARAM)pMsg->pParam);

					if(m_hRecvWnd!=NULL){
						pMsgToMain = new RSEvent();
						//-- 2013-04-24 hongsu.jung
						//-- Indicator (message)
						pMsgToMain->message	= WM_SDI_RESULT_SET_PROPERTY_VALUE;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent	= (LPARAM)m_pParent;
						pMsgToMain->nParam1 = GetConnectID();
						pMsgToMain->nParam2 = nResult;
						pMsgToMain->pParam = pMsg->nParam1;
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_SET_PROPERTY_VALUE, (LPARAM)pMsgToMain);
					}
				}
				break;
			case WM_SDI_OP_SET_FOCUS_GET_EVENT:
				{
					//-- Set Value
					nResult = PTP_SetFocusPosition((int)pMsg->nParam2);
					Sleep(500);
					//-- Get Value
					FocusPosition* pFocus = new FocusPosition();
					int nFocusValue = PTP_GetFocusPosition(pFocus);
					if( pFocus->min <  pMsg->nParam1)
					{
						pMsgToMain = new RSEvent();
						//-- 2013-04-24 hongsu.jung
						//-- Indicator (message)
						pMsgToMain->message	= WM_SDI_RESULT_FOCUS;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent	= (LPARAM)m_pParent;
						pMsgToMain->nParam1 = GetConnectID();
						pMsgToMain->nParam2 = nResult;
						pMsgToMain->pParam = (LPARAM)pFocus;
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_FOCUS, (LPARAM)pMsgToMain);
					}
				}
				break;
			case WM_SDI_OP_SET_FOCUS_VALUE:
				{
					//-- Set Value
					nResult = PTP_SetFocusPosition((int)pMsg->nParam1);
					Sleep(500);
					//-- Get Value
					FocusPosition* pFocus = new FocusPosition();
					int nFocusValue = PTP_GetFocusPosition(pFocus);
					if( pFocus->min <  pMsg->nParam1)
					{
						pMsgToMain = new RSEvent();
						//-- 2013-04-24 hongsu.jung
						//-- Indicator (message)
						pMsgToMain->message	= WM_SDI_RESULT_FOCUS;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent	= (LPARAM)m_pParent;
						pMsgToMain->nParam1 = GetConnectID();
						pMsgToMain->nParam2 = nResult;
						pMsgToMain->pParam = (LPARAM)pFocus;
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_FOCUS, (LPARAM)pMsgToMain);
					}
				}
				break;

			case WM_SDI_OP_SET_FOCUS:
				{
					//-- Create Focus For Setting
					FocusPosition* pFocus = new FocusPosition();
					nResult = PTP_SetFocusPosition(pFocus, (int)pMsg->nParam1);
					if(m_hRecvWnd!=NULL)
					{
						pMsgToMain = new RSEvent();
						//-- 2013-04-24 hongsu.jung
						//-- Indicator (message)
						pMsgToMain->message	= WM_SDI_RESULT_FOCUS;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent	= (LPARAM)m_pParent;
						pMsgToMain->nParam1 = GetConnectID();
						pMsgToMain->nParam2 = nResult;
						pMsgToMain->pParam = (LPARAM)pFocus;
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_FOCUS, (LPARAM)pMsgToMain);
					}				
				}
				break;

			case WM_SDI_OP_GET_FOCUS_VALUE:
				{
					//-- Create Focus For Setting
					FocusPosition* pFocus = new FocusPosition();
					int nFocusValue = PTP_GetFocusPosition(pFocus);
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_FOCUS;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
					pMsgToMain->nParam3 = pMsg->nParam1;
					pMsgToMain->pParam = (LPARAM)pFocus;					

					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_FOCUS, (LPARAM)pMsgToMain);
				}
				break;

			case WM_SDI_OP_RESET:
				nResult = PTP_ResetDevice();				
				if(m_hRecvWnd!=NULL){
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_RESET;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_RESET, (LPARAM)pMsgToMain);
				}
				break;
				//20150128 CMiLRe NX3000
				//CMiLRe 2015129 펌웨어 다운로드 경로 추가
			case WM_SDI_OP_FW_DOWNLOAD:		
				{
					TCHAR* strTpPath = (TCHAR*)pMsg->pParam;
					CString strPath = strTpPath ; 
					delete []strTpPath;
					nResult = PTP_FWDownload(strPath);
					if(m_hRecvWnd!=NULL)
					{
						pMsgToMain = new RSEvent();
						pMsgToMain->message	= WM_SDI_RESULT_FW_DOWNLOAD;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent	= (LPARAM)m_pParent;
						pMsgToMain->nParam1 = GetConnectID();
						pMsgToMain->nParam2 = nResult;
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_FW_DOWNLOAD, (LPARAM)pMsgToMain);
					}
				}
				break;
			case WM_SDI_OP_FWUPDATE:
				nResult = PTP_FWUpdate();
				if(m_hRecvWnd!=NULL){
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_FWUPDATE;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_FWUPDATE, (LPARAM)pMsgToMain);
				}
				break;
			case WM_SDI_OP_SENSORCLEANING:
				nResult = PTP_SensorCleaningDevice();
				if(m_hRecvWnd!=NULL){
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_SENSORCLEANING;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::PostMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_SENSORCLEANING, (LPARAM)pMsgToMain);
				}
				break;
			case WM_SDI_OP_EXPORT_LOG:
				nResult = PTP_ExportLog();
				break;
			case WM_SDI_OP_HALF_SHUTTER:
				nResult = PTP_HalfShutter((int)pMsg->nParam1);
				break;
			case WM_SDI_OP_SET_FOCUS_FRAME:
				{
					nResult = PTP_SetFocusFrame((int)pMsg->nParam1, (int)pMsg->nParam2, (int)pMsg->nParam3);

					if(m_hRecvWnd!=NULL)
					{
						pMsgToMain = new RSEvent();
						pMsgToMain->message	= WM_SDI_RESULT_SET_FOCUS_FRAME;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent = (LPARAM)m_pParent;
						pMsgToMain->nParam1 = nResult;		// response code
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_SET_FOCUS_FRAME, (LPARAM)pMsgToMain);
					}			
				}
				break;
			case WM_SDI_OP_GET_FOCUS_FRAME:
				{
					SdiUIntArray* pArList = new SdiUIntArray;
					pArList->SetSize(5);

					BYTE *pData = NULL;
					int size;
					int nRet = PTP_GetFocusFrame(&pData, &size);					
					int offSet = 0;
					int nX, nY, nW, nH, nMagnification;

					if (pData != NULL)
					{
						memcpy(&nX, &pData[offSet], 4); offSet+=4;
						memcpy(&nY, &pData[offSet], 4); offSet+=4;
						memcpy(&nW, &pData[offSet], 4); offSet+=4;
						memcpy(&nH, &pData[offSet], 4); offSet+=4;
						memcpy(&nMagnification,&pData[offSet], 4); offSet+=4;

 						pArList->SetAt(0, (int)nX);		
 						pArList->SetAt(1, (int)nY);		
 						pArList->SetAt(2, (int)nW);		
 						pArList->SetAt(3, (int)nH);	
 						pArList->SetAt(4, (int)nMagnification);	
					}

					if(m_hRecvWnd!=NULL)
					{
						pMsgToMain = new RSEvent();
						pMsgToMain->message	= WM_SDI_RESULT_GET_FOCUS_FRAME;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent = (LPARAM)m_pParent;
						pMsgToMain->nParam1 = nRet;		// response code
						pMsgToMain->pParam = (LPARAM)pArList;	
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_GET_FOCUS_FRAME, (LPARAM)pMsgToMain);
					}						
				}
				break;
			case WM_SDI_OP_DEVICE_REBOOT_GH5:
				nResult = PTP_DeviceReboot();
				if(m_hRecvWnd!=NULL){
					pMsgToMain = new RSEvent();
					pMsgToMain->message	= WM_SDI_RESULT_DEVICE_REBOOT;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_DEVICE_REBOOT, (LPARAM)pMsgToMain);
				}
				break;
			case WM_SDI_OP_DEVICE_REBOOT:
				//CMiLRe 2015129 리부팅전에 설정값들 저장 (마지막 설정된 줌과 포커스 저장하는 부분)
				PTP_HiddenCommand(1,2);
				nResult = PTP_DeviceReboot();
				//nResult = PTP_SetDevPropDesc(PTP_OC_SAMSUNG_FWUpdate, SDI_TYPE_UINT_8, 0xFF);
				if(m_hRecvWnd!=NULL){
					pMsgToMain = new RSEvent();
					pMsgToMain->message	= WM_SDI_RESULT_DEVICE_REBOOT;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_DEVICE_REBOOT, (LPARAM)pMsgToMain);
				}
				break;
			case WM_SDI_OP_DEVICE_POWER_OFF:
				nResult = PTP_DevicePowerOff();
				if(m_hRecvWnd!=NULL){
					pMsgToMain = new RSEvent();
					pMsgToMain->message	= WM_SDI_RESULT_DEVICE_POWER_OFF;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_DEVICE_POWER_OFF, (LPARAM)pMsgToMain);
				}
				break;
				
			case WM_SDI_OP_FORMAT:
				nResult = PTP_FormatDevice();
				if(m_hRecvWnd!=NULL){
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message)
					pMsgToMain->message	= WM_SDI_RESULT_FORMAT;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_FORMAT, (LPARAM)pMsgToMain);
				}
				break;
			case WM_SDI_OP_FW_UPDATE:
				PTP_FWUpdate();
				break;
			case WM_SDI_OP_HIDDEN_COMMAND:
				//PTP_HiddenCommand(0,2);  테스트용 param 값은 0, 2로 보내면 설정값들이 저장됨
				PTP_HiddenCommand((int)pMsg->nParam1,(int)pMsg->pParam);
				break;
			case WM_SDI_OP_FOCUS_LOCATE_SAVE:
				PTP_FocusSave();
				break;
			case WM_SDI_OP_SET_ENLARGE:
				PTP_SetEnLarg((int)pMsg->nParam1);
				break;
			case WM_SDI_OP_GET_RECORD_STATUS:
				{
					nResult = PTP_GetRecordStatus();
					if(m_hRecvWnd!=NULL){
						pMsgToMain = new RSEvent();
						pMsgToMain->message	= WM_SDI_RESULT_GET_RECORD_STATUS;
						pMsgToMain->pParent = (LPARAM)m_pParent;
						pMsgToMain->nParam1 = nResult;
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_GET_RECORD_STATUS, (LPARAM)pMsgToMain);
					}
				}
				break;
			case WM_SDI_OP_SET_FRAMERATE:
				m_nMovieSize = pMsg->nParam1;
				break;
			case WM_SDI_OP_FILE_TRANSFER:
				{
					int nFileSize = 0;
					int bClose = 0;

					int nMovieSize = m_nMovieSize;
					nResult = PTP_FileTransfer( (char *)pMsg->pParam, pMsg->nParam1, pMsg->nParam2 , pMsg->nParam3, &nFileSize, nMovieSize);
					/*
					if( nResult == SDI_ERR_SEMAPHORE_TIMEOUT)
					{
						pMsgToMain = new RSEvent();
						pMsgToMain->message	= WM_SDI_RESULT_FILE_TRANSFERFAIL;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent	= (LPARAM)m_pParent;
						pMsgToMain->nParam1 = GetConnectID();
						pMsgToMain->nParam2 = nResult;
						::PostMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_FILE_TRANSFERFAIL, (LPARAM)pMsgToMain);	
					}
					*/
					
					if(m_hRecvWnd!=NULL)
					{
						pMsgToMain = new RSEvent();
						pMsgToMain->message	= WM_SDI_RESULT_FILE_TRANSFER_2;
						pMsgToMain->pParent = (LPARAM)m_pParent;
						pMsgToMain->nParam1 = nResult;
						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_FILE_TRANSFER_2, (LPARAM)pMsgToMain);
					}


					if(nResult==SDI_ERR_OK)
					{
						pMsgToMain = new RSEvent();
						//-- 2013-04-24 hongsu.jung
						//-- Indicator (message)
						pMsgToMain->message	= WM_SDI_RESULT_FILE_TRANSFER;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent	= (LPARAM)m_pParent;
						pMsgToMain->nParam1 = nFileSize;
						if( pMsg->nParam2 > 0)
							bClose = 1;
						pMsgToMain->nParam2 = bClose;
						::PostMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_FILE_TRANSFER, (LPARAM)pMsgToMain);	
					}
					else
						TRACE(_T("What's the solution????????????????\n"));

					break;
				}
			case WM_SDI_OP_SETVIEWLARGER:
				nResult = PTP_SetEnLarg( pMsg->nParam1);
				break;

			case WM_SDI_OP_IMAGERECEIVECOMPLETE:
				nResult = PTP_ImageReceiveComplete();
				break;

			default:
				//-- 2011-8-9 jeansu : 이상한 값이 흘러들어서 NULL 처리하고 밑에서 확인 후 delete.
				TRACE("Unknown Message[%x]\r\n", pMsg->message);
				pMsg = NULL;
				break;
			}
			// TRACE(_T("CSdiSingleMgr::Run [--->] [%s][%04x] \n"), GetDeviceDSCID(), pMsg->message);
			//-- 2010-7-23 hongsu.jung
			//-- Exception 
			if(pMsg)
			{
				delete pMsg;
				pMsg = NULL;
			}	
			m_arMsg.RemoveAt(0);
						
		}		

		if(!m_bThreadFlag)
			break;

		//-- Event
		_ftime( &timeEvent[1] );
		nTimeGap = GetTimeGap( &timeEvent[0], &timeEvent[1]);
		if(nTimeGap > event_polling_time)
		{
			//-- 2013-11-04 hongsu@esmlab.com
			//-- Check Thread Stop
			if(!m_bThreadFlag)
				break;

			//-- Add Event Polling Code
			SdiUIntArray* pArList = new SdiUIntArray;		
			pArList->SetSize(2);
			SdiInt32 nEventExist = 0;			
			CString strLog;
			Sleep(10);
			nResult = PTP_CheckEvent(nEventExist, pArList);			

			int nContainerSize = 20;
			if(m_nModel == SDI_MODEL_GH5) // GH5
			{

#ifdef SAMPLE
				//////////////////////////////////////////////////////////////////////////
				// sample log

 				//if (nResult != SDI_ERR_OK)
 				{
 					if(m_hRecvWnd!=NULL)		
 					{
 						pMsgToMain = new RSEvent();
 						pMsgToMain->message	= WM_SDI_RESULT_CHECK_EVENT;
 						pMsgToMain->pParent = (LPARAM)m_pParent;
 						pMsgToMain->nParam1 = nResult;
 						pMsgToMain->nParam2 = (UINT)nEventExist;
 						pMsgToMain->pParam = (LPARAM)pArList;
 						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CHECK_EVENT, (LPARAM)pMsgToMain);
 					}
 				}
				//////////////////////////////////////////////////////////////////////////
#endif

				nContainerSize = 18;

				
#ifdef SAMPLE
				if (nEventExist > 0)
				{
					//////////////////////////////////////////////////////////////////////////
					// sample log
 					if(m_hRecvWnd!=NULL)		
 					{
 						pMsgToMain = new RSEvent();
 						pMsgToMain->message	= WM_SDI_RESULT_CHECK_EVENT;
 						pMsgToMain->pParent = (LPARAM)m_pParent;
 						pMsgToMain->nParam1 = nResult;
 						pMsgToMain->nParam2 = (UINT)nEventExist;
 						pMsgToMain->pParam = (LPARAM)pArList;
 						::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_CHECK_EVENT, (LPARAM)pMsgToMain);
 					}
					//////////////////////////////////////////////////////////////////////////
				}
#endif
				if(nEventExist > 0)
					nResult = 0x1;
			}
			else		// NX1
			{
				if(nResult==SDI_ERR_OK) 
					nResult = nEventExist;
			}

			if (pArList != NULL)
			{
				pArList->RemoveAll();
				delete pArList;
			}

			if(nResult==0x1)	// 0x1 : Event Exist, 0x2 : Event None.
			{
				BYTE *pData = NULL;
				int size;
				WORD nPTPCode;
				WORD nEventId;
				WORD nStatus;

				//-- 2013-11-04 hongsu@esmlab.com
				//-- Check Thread Stop
				if(!m_bThreadFlag)
					break;

				Sleep(10);
				nResult = PTP_GetEvent(&pData,&size);				
				CString strLog;
// 				strLog.Format(_T("1701 PTP_GetEvent nResult : %X"), nResult);
// 				WriteLog(strLog);

#ifdef SAMPLE
				//////////////////////////////////////////////////////////////////////////
				// sample log
				if(nResult != SDI_ERR_OK)
				{
					if(m_nModel == SDI_MODEL_GH5) // GH5
					{			
						SdiUInt16 nEventId	= -1;
						SdiUInt32 nPTPCode	= -1;
						SdiUInt32 nParam1	= -1;
						SdiUInt32 nParam2	= -1;
						SdiUInt32 nParam3	= -1;
						if (pData != NULL)
						{
							PTPEventContainer_GH5 *pEvent_GH5 = NULL;										
							pEvent_GH5 = (PTPEventContainer_GH5*) pData;	
							nEventId = pEvent_GH5->TransactionID;
							nPTPCode = pEvent_GH5->Code;
							nParam1 = pEvent_GH5->Param1;
							nParam2 = pEvent_GH5->Param2;
							nParam3 = pEvent_GH5->Param3;
						}
						
						SdiUIntArray* pArList = new SdiUIntArray;
						pArList->SetSize(5);
						pArList->SetAt(0, nEventId);
						pArList->SetAt(1, nPTPCode);
						pArList->SetAt(2, nParam1);
						pArList->SetAt(3, nParam2);
						pArList->SetAt(4, nParam3);

						if(m_hRecvWnd!=NULL)		
						{
							pMsgToMain = new RSEvent();
							pMsgToMain->message	= WM_SDI_RESULT_GET_EVENT;
							pMsgToMain->pParent = (LPARAM)m_pParent;
							pMsgToMain->nParam1 = nResult;
							pMsgToMain->pParam = (LPARAM)pArList;
							::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_GET_EVENT, (LPARAM)pMsgToMain);
						}

						if (pArList != NULL)
						{
							pArList->RemoveAll();
							delete pArList;
						}
					}
				}
				//////////////////////////////////////////////////////////////////////////
#endif


				if(nResult == SDI_ERR_OK)
				{
					PTPEventContainer *pEvent = NULL;
					PTPEventContainer_GH5 *pEvent_GH5 = NULL;
					int nParam1, nParam2, nParam3;
					if(m_nModel == SDI_MODEL_GH5) // GH5
					{
						pEvent_GH5 = (PTPEventContainer_GH5*) pData;	
					}
					else
					{
						pEvent = (PTPEventContainer*) pData;
					}

					
					
					for(int i=0;i<size/nContainerSize;i++)
					{
						Sleep(10);

						if(m_nModel == SDI_MODEL_GH5) // GH5
						{
							nEventId = pEvent_GH5[i].Code;
							nPTPCode = pEvent_GH5[i].Param1;
							nParam1 = pEvent_GH5[i].Param1;
							nParam2 = pEvent_GH5[i].Param2;
							nParam3 = pEvent_GH5[i].Param3;

#ifdef SAMPLE
							//////////////////////////////////////////////////////////////////////////
							// sample log
							SdiUIntArray* pArList = new SdiUIntArray;
							pArList->SetSize(5);
							pArList->SetAt(0, nEventId);
							pArList->SetAt(1, nPTPCode);
							pArList->SetAt(2, nParam1);
							pArList->SetAt(3, nParam2);
							pArList->SetAt(4, nParam3);


							if(m_hRecvWnd!=NULL)		
							{
								pMsgToMain = new RSEvent();
								pMsgToMain->message	= WM_SDI_RESULT_GET_EVENT;
								pMsgToMain->pParent = (LPARAM)m_pParent;
								pMsgToMain->nParam1 = nResult;
								pMsgToMain->pParam = (LPARAM)pArList;
								::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_GET_EVENT, (LPARAM)pMsgToMain);
							}

							if (pArList != NULL)
							{
								pArList->RemoveAll();
								delete pArList;
							}
							//////////////////////////////////////////////////////////////////////////
#endif
						}
						else
						{
							nEventId = pEvent[i].Code;
							nPTPCode = pEvent[i].Param1;
							nParam1 = pEvent[i].Param1;
							nParam2 = pEvent[i].Param2;
							nParam3 = pEvent[i].Param3;
						}
						//nEventId = pEvent[i].Code;
						//nPTPCode = pEvent[i].Param1;




						switch(nEventId)
						{
						case PTP_EC_SAMSUNG_IntervalCapture:
							pMsgToMain = new RSEvent();
							pMsgToMain->message	= WM_RS_GET_INTERVAL_CAPTURE;
							pMsgToMain->nParam1	= nParam1;//pEvent[i].Param1;	
							pMsgToMain->nParam2	= nParam2;//pEvent[i].Param2;	
							pMsgToMain->nParam3	= nParam3;//pEvent[i].Param3;	
							::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_GET_INTERVAL_CAPTURE, (LPARAM)pMsgToMain);
							break;
						case PTP_EC_SAMSUNG_IntervalCaptureValue:
							pMsgToMain = new RSEvent();
							pMsgToMain->message	= WM_RS_GET_INTERVAL_CAPTURE_VALUE;
							pMsgToMain->nParam1	= nParam1;//pEvent[i].Param1;
							pMsgToMain->nParam2	= nParam2;//pEvent[i].Param2;
							pMsgToMain->nParam3	= nParam3;//pEvent[i].Param3;
							::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_GET_INTERVAL_CAPTURE_VALUE, (LPARAM)pMsgToMain);
							break;
						case PTP_EC_SAMSUNG_EventBufferFull:
							pMsgToMain = new RSEvent();
							pMsgToMain->message	= WM_SDI_EVENT_EVENT_BUFFER_FULL;
							pMsgToMain->pDest	= (LPARAM)this;
							pMsgToMain->pParent	= (LPARAM)m_pParent;
							pMsgToMain->nParam1	= GetConnectID();
							pMsgToMain->nParam2 = nParam1;//pEvent[i].Param1;
							pMsgToMain->nParam3 = nParam2;//pEvent[i].Param2;
							::SendMessage(m_hRecvWnd, WM_RS,  (WPARAM)WM_SDI_EVENT_EVENT_BUFFER_FULL, (LPARAM)pMsgToMain);	
							break;
						case PTP_EC_SAMSUNG_EVF_LCD_Display:
							/*pMsgToMain = new RSEvent();
							pMsgToMain->message	= WM_SDI_EVENT_LCD_DISPLAY;		
							pMsgToMain->nParam1 = pEvent[i].Param1;
							::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_SDI_EVENT_LCD_DISPLAY, (LPARAM)pMsgToMain);*/
							break;
						case PTP_EC_SAMSUNG_IntervalKeyStop:
							pMsgToMain = new RSEvent();
							pMsgToMain->message	= WM_SDI_EVENT_INTERVAL_KEY_STOP;							
							::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_SDI_EVENT_INTERVAL_KEY_STOP, (LPARAM)pMsgToMain);
							break;
						//-- 2014-10-14 joonho.kim
						//-- Err Msg
						case PTP_EC_SAMSUNG_ErrorMsg:
							{
								if(GetDeviceModel().CompareNoCase(_T("GH5")) == 0 && nParam1 == ERROR_CODE_OVERWRITE)
									m_bErr = TRUE;
								pMsgToMain = new RSEvent();

								if(GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
									pMsgToMain->message	= WM_SDI_EVENT_ERR_MSG_GH5;							
								else
									pMsgToMain->message	= WM_SDI_EVENT_ERR_MSG;							

								pMsgToMain->nParam1	= nParam1;//pEvent[i].Param1;	
								pMsgToMain->nParam2 = nParam2;
								//pMsgToMain->nParam2	= GetConnectID();
								
								pMsgToMain->nParam3	= nParam3;
								pMsgToMain->pParent	= (LPARAM)m_pParent;
								::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)pMsgToMain->message, (LPARAM)pMsgToMain);
							}
							break;
						case PTP_EC_SAMSUNG_DevicePropChanged:
							pMsgToMain = new RSEvent();
							//-- 2013-04-24 hongsu.jung
							//-- Indicator (message)
							pMsgToMain->message	= WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED;							
							pMsgToMain->pDest	= (LPARAM)this;							
							pMsgToMain->pParent	= (LPARAM)m_pParent;
							pMsgToMain->nParam1	= GetConnectID();
							pMsgToMain->nParam2	= nPTPCode;
							pMsgToMain->pParam	= (LPARAM)this;

							if(nPTPCode == PTP_OC_SAMSUNG_SetFocusPosition)
							{
								SdiMessage *pSendMsgs = new SdiMessage();
								pSendMsgs->message = WM_SDI_OP_SET_FOCUS_GET_EVENT;
								pSendMsgs->nParam1 = nPTPCode;
								pSendMsgs->nParam2 = nParam2;//pEvent[i].Param2;
								m_arMsg.Add((CObject *)pSendMsgs);
							}
							//CMiLRe 2015129 줌 컨트롤
							else if(nPTPCode == PTP_DPC_SAMSUNG_ZOOM_CTRL_SET)
							{
								SdiMessage *pSendMsgs = new SdiMessage();
								pSendMsgs->message = WM_SDI_OP_SET_PROPERTY_VALUE;
								pSendMsgs->nParam1 = SDI_DPC_SAMSUNG_ZOOM_CTRL_SET;
								pSendMsgs->nParam2 = SDI_TYPE_UINT_16;
								pSendMsgs->pParam = nParam2;//pEvent[i].Param2;
								m_arMsg.Add((CObject *)pSendMsgs);
							}
							else if(nPTPCode == PTP_DPC_SAMSUNG_ZOOM_CTRL_GET)
							{
								SdiMessage *pSendMsgs = new SdiMessage();
								pSendMsgs->message = WM_SDI_OP_GET_PROPERTY_DESC;
								pSendMsgs->nParam1 = nPTPCode;
								m_arMsg.Add((CObject *)pSendMsgs);
							}
							else if(nPTPCode == PTP_OC_SAMSUNG_GetFocusPosition)
							{
								SdiMessage *pSendMsgs = new SdiMessage();
								pSendMsgs->message = WM_SDI_OP_GET_FOCUS_VALUE;
								pSendMsgs->nParam1 = nPTPCode;
								m_arMsg.Add((CObject *)pSendMsgs);
							}
							else 
								::SendMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED, (LPARAM)pMsgToMain);
							break;
						case PTP_EC_SAMSUNG_RequestObjectTransfer:
							{

							

							//TRACE(_T("[Event][%d] PTP_EC_SAMSUNG_RequestObjectTransfer Code[%x]\n"),GetConnectID(), nEventId);
							pMsgToMain = new RSEvent();
							//-- 2013-04-24 hongsu.jung
							//-- Indicator (message & SdiSingleMgr)
							pMsgToMain->message	= WM_SDI_EVENT_CAPTURE_DONE;
							//pMsgToMain->message	= pEvent[i].Param3;
							pMsgToMain->pDest	= (LPARAM)this;
							pMsgToMain->pParent	= (LPARAM)m_pParent;
							pMsgToMain->nParam1	= GetConnectID();
							pMsgToMain->nParam2 = nParam1;//pEvent[i].Param1;
							pMsgToMain->nParam3	= nParam2;//pEvent[i].Param2;	
							pMsgToMain->pParam = (int)nParam3;//pEvent[i].Param3;
							::SendMessage(m_hRecvWnd, WM_RS,  (WPARAM)WM_SDI_EVENT_CAPTURE_DONE, (LPARAM)pMsgToMain);							
							break;


							}
							//-- FD Box
						case PTP_EC_SAMSUNG_FDBoxInfo1:
							pMsgToMain = new RSEvent();
							pMsgToMain->message	= WM_RS_GET_FD_BOX_INFO1;
							pMsgToMain->nParam1	= nParam1;//pEvent[i].Param1;// x
							pMsgToMain->nParam2	= nParam2;//pEvent[i].Param2;// y
							::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_GET_FD_BOX_INFO1, (LPARAM)pMsgToMain);
							break;
						case PTP_EC_SAMSUNG_FDBoxInfo2:
							pMsgToMain = new RSEvent();
							pMsgToMain->message	= WM_RS_GET_FD_BOX_INFO2;
							pMsgToMain->nParam1	= nParam1;//pEvent[i].Param1;	// width
							pMsgToMain->nParam2	= nParam2;//pEvent[i].Param2;	// height
							pMsgToMain->nParam3	= nParam3;//pEvent[i].Param3;	// detect
							::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_GET_FD_BOX_INFO2, (LPARAM)pMsgToMain);
							break;
							//-- Tracking Box
						case PTP_EC_SAMSUNG_TrackingBoxInfo1:
							pMsgToMain = new RSEvent();
							pMsgToMain->message	= WM_RS_GET_TRACKING_BOX_INFO1;
							pMsgToMain->nParam1	= nParam1;//pEvent[i].Param1;	// x
							pMsgToMain->nParam2	= nParam2;//pEvent[i].Param2;	// y
							::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_GET_TRACKING_BOX_INFO1, (LPARAM)pMsgToMain);
							break;
						case PTP_EC_SAMSUNG_TrackingBoxInfo2:
							pMsgToMain = new RSEvent();
							pMsgToMain->message	= WM_RS_GET_TRACKING_BOX_INFO2;
							pMsgToMain->nParam1	= nParam1;//pEvent[i].Param1;	// width
							pMsgToMain->nParam2	= nParam2;//pEvent[i].Param2;	// height
							pMsgToMain->nParam3	= nParam3;//pEvent[i].Param3;	// detect
							::PostMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_GET_TRACKING_BOX_INFO2, (LPARAM)pMsgToMain);
							break;
						case PTP_EC_SAMSUNG_ImagePath:
							PTP_GetImagePath(m_strImagePath);

							pMsgToMain = new RSEvent();
							pMsgToMain->message	= WM_RS_GET_IMAGE_FILE_NAME;
							pMsgToMain->pParam	= (LPARAM)(LPSTR)(LPCTSTR)m_strImagePath;
							::SendMessage(m_hRecvWnd, WM_RS, (WPARAM)WM_RS_GET_IMAGE_FILE_NAME, (LPARAM)pMsgToMain);
							break;
						case PTP_EC_SAMSUNG_MovieTransfer:
							{
								//TRACE(_T("[PTP_EC_SAMSUNG_MovieTransfer] filenum[%d]  close[%d]  size[%d] \n"),pEvent[i].Param1, pEvent[i].Param2, pEvent[i].Param3);
								TRACE(_T("[PTP_EC_SAMSUNG_MovieTransfer] filenum[%d]  close[%d]  size[%d] \n"),nParam1, nParam2, nParam3);

								//-- 2014-09-29 joonho.kim
								pMsgSub = new RSEvent();
								pMsgSub->message	= WM_SDI_RESULT_FILE_SIZE;
								pMsgSub->nParam1 = nParam1;//pEvent[i].Param1;
								pMsgSub->nParam2 = nParam3;//pEvent[i].Param3;
								::SendMessage(m_hRecvWnd, WM_SDI, (WPARAM)WM_SDI_RESULT_FILE_SIZE, (LPARAM)pMsgSub);

								pMsgToMain = new RSEvent();
								//-- 2013-04-24 hongsu.jung
								//-- Indicator (message & SdiSingleMgr)
								pMsgToMain->message	= WM_SDI_EVENT_MOVIE_DONE;
								pMsgToMain->pDest	= (LPARAM)this;
								pMsgToMain->pParent	= (LPARAM)m_pParent;
								pMsgToMain->nParam1	= GetConnectID();
								pMsgToMain->nParam2 = nParam1;//pEvent[i].Param1;
								pMsgToMain->nParam3	= nParam2;//pEvent[i].Param2;
								::SendMessage(m_hRecvWnd, WM_RS,  (WPARAM)WM_SDI_EVENT_MOVIE_DONE, (LPARAM)pMsgToMain);							
								//TRACE(_T("[SEND MSG] WM_SDI_EVENT_MOVIE_DONE %s %d:%d\n"), GetDeviceDSCID(), pMsgToMain->nParam1, pMsgToMain->nParam3);
							}
							break;
						case PTP_EC_SAMSUNG_FileTransfer: 
							{
								/*g_nCount++;
								if(g_nCount > 5 && g_nCount < 10)
									break;*/
								
								/*if(m_bErr)
									break;*/

								pMsgToMain = new RSEvent();
								//-- 2013-04-24 hongsu.jung
								//-- Indicator (message & SdiSingleMgr)
								pMsgToMain->message	= WM_SDI_EVENT_FILE_TRANSFER;
								pMsgToMain->pDest	= (LPARAM)this;
								pMsgToMain->pParent	= (LPARAM)m_pParent;
								//pMsgToMain->nParam1	= GetConnectID();
								pMsgToMain->nParam1	= GetMovieSize();
								pMsgToMain->nParam2 = nParam1;//pEvent[i].Param1;
								pMsgToMain->nParam3	= nParam2;//pEvent[i].Param2;
								pMsgToMain->pParam	= (LPARAM)nParam3;//pEvent[i].Param3;
								
								
								::SendMessage(m_hRecvWnd, WM_RS,  (WPARAM)WM_SDI_EVENT_FILE_TRANSFER, (LPARAM)pMsgToMain);	
								//TRACE(_T("[SEND MSG] WM_SDI_EVENT_FILE_TRANSFER %s %d:%d\n"), GetDeviceDSCID(), pMsgToMain->nParam1, pMsgToMain->nParam3);
							}
							break;
						case PTP_EC_SAMSUNG_AFDetect:
							{
								pMsgToMain = new RSEvent();
								pMsgToMain->message	= WM_SDI_EVENT_AF_DETECT;
								pMsgToMain->pDest	= (LPARAM)this;
								pMsgToMain->pParent	= (LPARAM)m_pParent;
								pMsgToMain->nParam1	= GetConnectID();
								pMsgToMain->nParam2 = nPTPCode;		//	Event Param1 : AF Pass/Fail/Cancel							
								SetAFDetect(nPTPCode);
								::SendMessage(m_hRecvWnd, WM_RS,  (WPARAM)WM_SDI_EVENT_AF_DETECT, (LPARAM)pMsgToMain);
							}
							break;
						//-- 2014-09-15 hongsu@esmlab.com
						//-- Add Frame Event
						case PTP_EC_SAMSUNG_AddFrame:
							{
								pMsgToMain = new RSEvent();
								pMsgToMain->message	= WM_SDI_EVENT_ADD_FRAME;
								pMsgToMain->pDest	= (LPARAM)this;
								pMsgToMain->pParent	= (LPARAM)m_pParent;
								pMsgToMain->nParam1	= GetConnectID();

								//해당이벤트가 체크되면 Add Frame 발생
								//	Param1 =  Recording 시작후 발생한 프레임카운트
								//	Param2 =  Recording 시작후 발생한 프레임Time Gap (Frame Time stamp Gap)
								//TRACE(_T("PTP_EC_SAMSUNG_AddFrame  %d\n"),(int)pEvent[i].Param1);
								TRACE(_T("PTP_EC_SAMSUNG_AddFrame  %d\n"),(int)nParam1);
								pMsgToMain->nParam2 = nParam1 - 1;	//-- Frame Count
								pMsgToMain->nParam3	= nParam2;	//-- Interval Time (milli second)								
								::SendMessage(m_hRecvWnd, WM_RS,  (WPARAM)WM_SDI_EVENT_ADD_FRAME, (LPARAM)pMsgToMain);
							}
							break;
						//-- 2014-09-15 hongsu@esmlab.com
						//-- Skip Frame Event
						case PTP_EC_SAMSUNG_SkipFrame:
							{
								pMsgToMain = new RSEvent();
								pMsgToMain->message	= WM_SDI_EVENT_SKIP_FRAME;
								pMsgToMain->pDest	= (LPARAM)this;
								pMsgToMain->pParent	= (LPARAM)m_pParent;
								pMsgToMain->nParam1	= GetConnectID();

								//해당이벤트가 체크되면 Skip Frame 발생
								//	Param1 =  Recording 시작후 발생한 프레임카운트
								//	Param2 =  Recording 시작후 발생한 프레임Time Gap (Frame Time stamp Gap)
								pMsgToMain->nParam2 = nParam1 - 1;	//-- Frame Count
								pMsgToMain->nParam3	= nParam2;	//-- Interval Time (milli second)								
								::SendMessage(m_hRecvWnd, WM_RS,  (WPARAM)WM_SDI_EVENT_SKIP_FRAME, (LPARAM)pMsgToMain);
							}
							break;
						//-- 2014-09-16 hongsu@esmlab.com
						//-- Error Frame
						case PTP_EC_SAMSUNG_ErrorFrame:
							{
								pMsgToMain = new RSEvent();
								pMsgToMain->message	= WM_SDI_EVENT_ERROR_FRAME;
								//pMsgToMain->pDest	= (LPARAM)this;
								pMsgToMain->pParent	= (LPARAM)m_pParent;
								pMsgToMain->nParam1	= GetConnectID();
								pMsgToMain->nParam2 = nParam1 - 1;	//-- Frame Count
								pMsgToMain->nParam3	= nParam2;	//-- Interval Time (milli second)
								//-- 2014-09-21 hongsu@esmlab.com
								//-- Get Tick/Timestamp Interval 
								int nInterval = (int)nParam3;
								pMsgToMain->pDest	= (LPARAM)nInterval;		

								::SendMessage(m_hRecvWnd, WM_RS,  (WPARAM)WM_SDI_EVENT_ERROR_FRAME, (LPARAM)pMsgToMain);
							}
							break;
						//-- 2014-09-15 hongsu@esmlab.com
						//-- Get Time After Sync Frame
						case PTP_EC_SAMSUNG_StartTime:
							{
								g_nCount = 0;
								int nMsg = WM_SDI_EVENT_SENSOR_TIME;
								//m_pSdiCore->SdiFileMgrSetStartTime(pEvent[i].Param2);
								// Camera Signal -> Image Time Stamp
								/*if(pEvent[i].Param2 == 0 )
								{
									nMsg = WM_SDI_EVENT_SENSOR_ERROR_TIME;
								}*/

								//if(pEvent[i].Param1 == 0 || pEvent[i].Param2 == 0)
								if(nParam1 == 0 || nParam2 == 0)
								{
									TRACE(_T("Camera TimeStamp 0"));
#ifdef SAMPLE
									// SAMPLE log
									CString* pStrData = NULL;
									pStrData = new CString;
									pStrData->Format(_T("%s"), GetDeviceUniqueID());

									pMsgToMain = new RSEvent();
									pMsgToMain->message	= nMsg;
									pMsgToMain->pDest	= (LPARAM)this;
									pMsgToMain->pParent	= (LPARAM)m_pParent;
									pMsgToMain->nParam1	= GetConnectID();
									pMsgToMain->nParam2 = nParam1;//pEvent[i].Param1;
									pMsgToMain->nParam3 = nParam2;//pEvent[i].Param2;	//-- Start Sensor Time (After sync_frame_time_after_sensor_on time)
									pMsgToMain->pParam = (LPARAM)pStrData;
									::SendMessage(m_hRecvWnd, WM_RS,  (WPARAM)nMsg, (LPARAM)pMsgToMain);
#endif
								}
								else
								{
									CString* pStrData = NULL;
									pStrData = new CString;
									pStrData->Format(_T("%s"), GetDeviceUniqueID());

									pMsgToMain = new RSEvent();
									pMsgToMain->message	= nMsg;
									pMsgToMain->pDest	= (LPARAM)this;
									pMsgToMain->pParent	= (LPARAM)m_pParent;
									pMsgToMain->nParam1	= GetConnectID();
									pMsgToMain->nParam2 = nParam1;//pEvent[i].Param1;
									pMsgToMain->nParam3 = nParam2;//pEvent[i].Param2;	//-- Start Sensor Time (After sync_frame_time_after_sensor_on time)
									pMsgToMain->pParam = (LPARAM)pStrData;
									::SendMessage(m_hRecvWnd, WM_RS,  (WPARAM)nMsg, (LPARAM)pMsgToMain);
								}							
							}
							break;
						case PTP_EC_SAMSUNG_RecodingStop:
							{
								CSdiCoreWin7* pCore = (CSdiCoreWin7*)m_pSdiCore;
								pCore->m_pSdiFileMgr->m_bRecordEnd = TRUE;

								TRACE(_T("##########PTP_EC_SAMSUNG_RecodingStop\n"));

								pMsgToMain = new RSEvent();
								//-- 2013-04-24 hongsu.jung
								//-- Indicator (message & SdiSingleMgr)
								pMsgToMain->message	= WM_SDI_EVENT_RECORD_STOP;
								pMsgToMain->pDest	= (LPARAM)this;
								pMsgToMain->pParent	= (LPARAM)m_pParent;
								pMsgToMain->nParam1	= GetConnectID();
								pMsgToMain->nParam2 = nParam1;//pEvent[i].Param1;
								pMsgToMain->nParam3	= nParam2;//pEvent[i].Param2;
								pMsgToMain->pParam	= (LPARAM)nParam3;//pEvent[i].Param3;
								::SendMessage(m_hRecvWnd, WM_RS,  (WPARAM)WM_SDI_EVENT_RECORD_STOP, (LPARAM)pMsgToMain);	


								//int nLastTimeStamp = m_pSdiCore->SdiFileMgrGetEndTime()
								//TRACE(_T("#$#$#$#$ StopTime:[%d] LastTimeStamp[%d] Gap[%d]"), m_nRecStopTime, nLastTimeStamp, nLastTimeStamp - m_nRecStopTime);
							}
							break;
						

						case PTP_EC_SAMSUNG_KeyEvent:
							//nStatus = pEvent[i].Param2;
							nStatus = nParam2;
							TRACE(_T(" KEY EVENT : %d and %d\n"), nPTPCode, nStatus);
#ifndef IFA
							if(GetCurModeIndex()==eUCS_UI_MODE_PANORAMA || GetCurModeIndex()==eUCS_UI_MODE_MOVIE) 
								break;
#endif
							if(nPTPCode==1103 && nStatus==1){	// case of FullShutter Press
								int nAfMode;
								IDeviceInfo *pDeviceInfo = NULL;
								m_pSdiCore->SdiGetDeviceInfo(PTP_CODE_FOCUSMODE, &pDeviceInfo);
								nAfMode = pDeviceInfo->GetCurrentEnum();

								if(SDI_DPV_AF_MODE_MF != nAfMode)
								{
									int nFocus = GetAFDetect();
									if(AF_FAIL != nFocus){
										if(IsCapturing()) SetCapturing(FALSE);
										else SetCapturing(TRUE);
									}
								}
								else{
									if(IsCapturing()) SetCapturing(FALSE);
									else SetCapturing(TRUE);
								}
							}
							pMsgToMain = new RSEvent();
							//-- 2013-04-24 hongsu.jung
							//-- Indicator (message & SdiSingleMgr)
							pMsgToMain->message	= WM_SDI_EVENT_KEY_EVENT;
							pMsgToMain->pDest	= (LPARAM)this;
							pMsgToMain->pParent	= (LPARAM)m_pParent;
							pMsgToMain->nParam1	= GetConnectID();
							pMsgToMain->nParam2 = nPTPCode;
							pMsgToMain->pParam = nStatus;							
							::SendMessage(m_hRecvWnd,WM_RS, (WPARAM)WM_SDI_EVENT_KEY_EVENT, (LPARAM)pMsgToMain);
							break;
						default:
							TRACE(_T("Event ID default ???  [%d]"), nEventId);
							break;
						}
					}
				}
				else if(nResult == -5)
				{
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message & SdiSingleMgr)
					pMsgToMain->message	= WM_SDI_EVENT_USBCONNECTFAIL;
					pMsgToMain->pDest	= (LPARAM)this;
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1	= GetConnectID();					
					::SendMessage(m_hRecvWnd,WM_RS, (WPARAM)WM_SDI_EVENT_USBCONNECTFAIL, (LPARAM)pMsgToMain);
					TRACE(_T("else if(nResult == -5)\r\n"));
				}
				else
				{
					/*
					// case of GetEvent error
					if(m_hRecvWnd!=NULL)
					{
						pMsgToMain = new RSEvent();
						//-- 2013-04-24 hongsu.jung
						//-- Indicator (message & SdiSingleMgr)
						pMsgToMain->message	= WM_SDI_EVENT_EVENT_ERROR_OCCURED;
						pMsgToMain->pDest	= (LPARAM)this;
						pMsgToMain->pParent	= (LPARAM)m_pParent;
						pMsgToMain->nParam1 = GetConnectID();
						pMsgToMain->nParam2 = nResult;
						::PostMessage(m_hRecvWnd ,WM_SDI, (WPARAM)WM_SDI_EVENT_EVENT_ERROR_OCCURED, (LPARAM)pMsgToMain);
					}
					*/
				}
				if(GetOSVersion()==OS_WINDOWS_XP)
				{
					if(pData!=NULL) free(pData);
				}
				else
				{
					if(pData!=NULL) CoTaskMemFree(pData);
				}
			}
			else if(nResult==0x2){}		// case of event-none.
			else{
				// case of Event Error.
				if(m_hRecvWnd!=NULL)
				{
					/*
					pMsgToMain = new RSEvent();
					//-- 2013-04-24 hongsu.jung
					//-- Indicator (message & SdiSingleMgr)
					pMsgToMain->message	= WM_SDI_EVENT_EVENT_ERROR_OCCURED;
					pMsgToMain->pDest	= (LPARAM)this;	
					pMsgToMain->pParent	= (LPARAM)m_pParent;
					pMsgToMain->nParam1 = GetConnectID();
					pMsgToMain->nParam2 = nResult;
					::SendMessage(m_hRecvWnd ,WM_SDI, (WPARAM)WM_SDI_EVENT_EVENT_ERROR_OCCURED, (LPARAM)pMsgToMain);
					*/
				}
			}
			_ftime( &timeEvent[0] );	
		}

		//-- 2013-11-04 hongsu@esmlab.com
		//-- Check Thread Stop
		if(!m_bThreadFlag)
		{
			//-- 2013-12-05 hongsu@esmlab.com
			//-- Critical Section 
			LeaveCS();
			break;
		}

		//-- Liveview
		if((m_strModel != _T("NX3000")) && m_bLiveview && m_pLiveviewConverter)
		{
			Sleep(10);
			if(m_pLiveviewConverter->ISSet())
			{	
				//-- Send Liveview
				if(m_nModel == SDI_MODEL_NX1 || m_nModel == SDI_MODEL_GH5)
				{
					RSLiveviewInfo liveviewInfo;
					nResult = PTP_LiveviewInfo(&liveviewInfo);
					Sleep(1);
					nResult = PTP_SendLiveview(&liveviewInfo, m_nPhotoType, m_bRecordLiveView, m_bEnd);
				}
				else
					nResult = PTP_SendLiveview();

				if( nResult == SDI_ERR_OK)
				{
					//-- Liveview Speed Guarantee
					_ftime( &timeLive[1] );
					int nTimeGap =GetTimeGap( &timeLive[0], &timeLive[1]);
					int nDelay = m_nLiveviewSpeed - nTimeGap;
					if(nDelay <0)
						nDelay = 0;

					//-- Check Tread
					if(!m_bThreadFlag)
						break;

					Sleep(nDelay);

					//-- Check Tread
					if(!m_bThreadFlag)
						break;

					_ftime( &timeLive[0] );
				}
				else
				{
					//-- something wrong
					TRACE(_T("[SdiSingleMgr][%d] MTP Code =%x\n"),GetConnectID(), nResult);					
					m_bThreadFlag = FALSE;					
					if(m_hRecvWnd)
					{
						pMsgToMain = new RSEvent();
						//-- 2013-04-24 hongsu.jung
						//-- Indicator (message & SdiSingleMgr)
						pMsgToMain->message	= WM_SDI_EVENT_LIVEVIEW_ERROR_OCCURED;
						pMsgToMain->pDest	= (LPARAM)this;	
						pMsgToMain->pParent	= (LPARAM)m_pParent;
						pMsgToMain->nParam1 = GetConnectID();
						pMsgToMain->nParam2 = nResult;
						::SendMessage(m_hRecvWnd ,WM_SDI, (WPARAM)WM_SDI_EVENT_LIVEVIEW_ERROR_OCCURED, (LPARAM)pMsgToMain);
					}		
				}
			}			
		}	

		//-- 2013-12-05 hongsu@esmlab.com
		//-- Critical Section 
		LeaveCS();
	}	

	// 20150807 yongmin
	//RemoveAll();
	m_bThreadRun = FALSE;	

	return 0;
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-12-01
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
BOOL CSdiSingleMgr::CheckGetFile()
{
	return m_pSdiCore->SdiCheckGetFile();
}

unsigned WINAPI CSdiSingleMgr::DscResetThread(LPVOID param)
{
	/*
	CSdiSingleMgr* pSdiSingleMgr = (CSdiSingleMgr*)param;
	CTime CompareTime;
	while(1)
	{
		CompareTime = CTime::GetCurrentTime();
		CTimeSpan ts = CompareTime - pSdiSingleMgr->m_DscResetTime; 
		int nMinute = ts.GetTotalMinutes();
		Sleep(3000);
		if( nMinute >= SDI_DSC_RESETTIME)
		{
			RSEvent* pMsg = new RSEvent();
			//-- Release Focusing
			pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1;
			pSdiSingleMgr->m_arMsg.Add((CObject *)pMsg);
			pSdiSingleMgr->m_DscResetTime = CTime::GetCurrentTime();

			Sleep(100);
			pMsg = new RSEvent();
			//-- Release Focusing
			pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4;
			pSdiSingleMgr->m_arMsg.Add((CObject *)pMsg);
		} 
		Sleep(1000);
	}
	*/
	return 0;
}

void CSdiSingleMgr::WriteLog(CString strLog)
{
// 	CString strFolderName = _T("C:\\4DLog"), strFileName;
// 
// 	CTime cTime = CTime::GetCurrentTime();
// 	strFileName.Format(_T("%04d-%02d-%02d_%s_PTP_EC_SAMSUNG_RequestObjectTransfer.log"), cTime.GetYear(), cTime.GetMonth(), cTime.GetDay(), m_strUniqueID);
// 
// 	CreateDirectory(strFolderName, NULL);
// 	CFile logFile;
// 	if(logFile.Open(strFolderName + _T("\\") + strFileName, CFile::modeCreate | CFile::modeReadWrite |CFile::modeNoTruncate))
// 	{
// 		logFile.SeekToEnd();
// 		CString strLogData;
// 		strLogData.Format(_T("[%02d_%02d_%02d] %s \r\n"), cTime.GetHour(), cTime.GetMinute(), cTime.GetSecond(), strLog);
// 		logFile.Write(strLogData, strLogData.GetLength() * 2);
// 		logFile.Close();
// 	}
}

void CSdiSingleMgr::CheckFunction()
{
	GetConnectID();
}

int CSdiSingleMgr::GetDevinfoType(int nCode)
{ 
	CDeviceInfoMgr* pDevMgr = NULL;
	pDevMgr = m_pSdiCore->GetDeviceInfo();

	return pDevMgr->GetType(nCode); 
}

void CSdiSingleMgr::SetDevinfoCurrentEnum(int nCode, int nValue)
{ 
	CDeviceInfoMgr* pDevMgr = NULL;
	pDevMgr = m_pSdiCore->GetDeviceInfo();

	return pDevMgr->SetCurrentEnum(nCode, nValue); 
}

bool CSdiSingleMgr::GetDevinfoNextEnum(int nCode, int& nValue)
{ 
	CDeviceInfoMgr* pDevMgr = NULL;
	pDevMgr = m_pSdiCore->GetDeviceInfo();

	CDeviceInfoBase* pDevInfo = NULL;
	pDevInfo = pDevMgr->GetDeviceInfo(nCode);

	return pDevInfo->GetNextEnum(nValue); 
}

bool CSdiSingleMgr::GetDevinfoPrevEnum(int nCode, int& nValue)
{
	CDeviceInfoMgr* pDevMgr = NULL;
	pDevMgr = m_pSdiCore->GetDeviceInfo();

	CDeviceInfoBase* pDevInfo = NULL;
	pDevInfo = pDevMgr->GetDeviceInfo(nCode);

	return pDevInfo->GetPrevEnum(nValue);
}

bool CSdiSingleMgr::GetDevinfoNextStr(int nCode, CString& str)
{
	CDeviceInfoMgr* pDevMgr = NULL;
	pDevMgr = m_pSdiCore->GetDeviceInfo();

	CDeviceInfoBase* pDevInfo = NULL;
	pDevInfo = pDevMgr->GetDeviceInfo(nCode);

	return pDevInfo->GetNextStr(str); 
}

bool CSdiSingleMgr::GetDevinfoPrevStr(int nCode, CString& str)
{
	CDeviceInfoMgr* pDevMgr = NULL;
	pDevMgr = m_pSdiCore->GetDeviceInfo();

	CDeviceInfoBase* pDevInfo = NULL;
	pDevInfo = pDevMgr->GetDeviceInfo(nCode);

	return pDevInfo->GetPrevStr(str); 
}

void CSdiSingleMgr::StopFileTransfer()
{
	CSdiCoreWin7* pCore = (CSdiCoreWin7*)m_pSdiCore;
	pCore->m_pSdiFileMgr->m_bFileTransfer = FALSE;
}

void CSdiSingleMgr::StartFileTransfer()
{
	CSdiCoreWin7* pCore = (CSdiCoreWin7*)m_pSdiCore;
	pCore->m_pSdiFileMgr->m_bFileTransfer = TRUE;
}