#include "StdAfx.h"
#include "SdiSdk.h"
#include "SdiSingleMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//CSdiSingleMgr *g_pCSdiSingleMgr = NULL;

CSdiSdk::CSdiSdk(int nDeviceNum, CString strVendor, CString strProduct)
{
	g_pCSdiSingleMgr = new CSdiSingleMgr(nDeviceNum, strVendor, strProduct);
}

CSdiSdk::CSdiSdk(int nDeviceNum, CString strVendor, CString strProduct, LARGE_INTEGER swFreq, LARGE_INTEGER swStart)
{
	m_nStartTime = 0;
	m_nSensorStart = 0;
	g_pCSdiSingleMgr = new CSdiSingleMgr(nDeviceNum, strVendor, strProduct, swFreq, swStart);
}

CSdiSdk::~CSdiSdk(void)
{
	if(g_pCSdiSingleMgr)
	{
		if(g_pCSdiSingleMgr->m_bStop)
			return;
		g_pCSdiSingleMgr->m_bStop = TRUE;
		if( g_pCSdiSingleMgr)
		{
			delete g_pCSdiSingleMgr;
			g_pCSdiSingleMgr = NULL;
		}
	}
}

SdiResult CSdiSdk::SdiGetDeviceHandleList(SdiStrArray *pArList)
{
    return CSdiSingleMgr::SdiGetDeviceHandleList(pArList);
}

SdiVoid	CSdiSdk::SdiInit()
{
	g_pCSdiSingleMgr->m_nPCTime = 0;
	g_pCSdiSingleMgr->m_nDSCTime = 0;
	g_pCSdiSingleMgr->m_bInitPreRec = FALSE;
}

SdiInt CSdiSdk::SdiGetDSCTime()
{
	return g_pCSdiSingleMgr->m_nDSCTime;
}

SdiInt CSdiSdk::SdiGetPCTime()
{
	return g_pCSdiSingleMgr->m_nPCTime;
}

SdiVoid CSdiSdk::SdiSetParents()
{
	g_pCSdiSingleMgr->SetParent(this);
}

SdiVoid CSdiSdk::SdiSetInitPreRec( BOOL b )
{
	g_pCSdiSingleMgr->SetInitPreRec(b);
}

SdiBool CSdiSdk::SdiGetInitPreRec()
{
	return g_pCSdiSingleMgr->GetInitPreRec();
}

SdiResult CSdiSdk::SdiGetDeviceID(SdiChar *strDevName)
{
	int nCount = 0;
	CString strDSCID;
	
	SdiResult nResult = g_pCSdiSingleMgr->PTP_GetDevUniqueID();
	strDSCID = g_pCSdiSingleMgr->GetDeviceUniqueID();

	if(strDSCID.IsEmpty())
		AfxMessageBox(_T("Get UUID Fail"));

	
	//wsprintf((LPWSTR)strDevName,_T("%s"), CT2A(strDSCID));
	//sprintf((char*)strDevName, "%s", CT2A(strDSCID));
	memset(strDevName,0,13);
	int nSize = strDSCID.GetLength();
	memcpy(strDevName, CT2A(strDSCID), nSize);

	m_strDSCID = CString(strDevName,13);

	return SDI_ERR_OK;
}

SdiResult CSdiSdk::SdiSetDeviceID(CString strUUID)
{
	SdiResult nResult = g_pCSdiSingleMgr->PTP_SetDevUniqueID(strUUID);
	if(nResult == S_OK)
		return SDI_ERR_OK;
	return nResult;
}

SdiResult CSdiSdk::SdiInitSession(SdiChar *strDevName)
{
	if(!g_pCSdiSingleMgr)
		return SDI_ERR_INTERNAL;

	//-- 1.Init Session
	SdiResult nResult = g_pCSdiSingleMgr->SdiInitSession(strDevName);
	if (nResult != SDI_ERR_OK)
	{
		TRACE(L"[Fail] Init Session \n");
		return nResult;
	}

	TRACE(L"[Success] Init Session \n");
	//-- 2.Open Session
	nResult = g_pCSdiSingleMgr->PTP_OpenSession();
	TRACE(L"[Success] Open Session \n");
	if(nResult == SDI_ERR_OK)
	{
		TRACE(L"[Success] Check Get File \n");
		g_pCSdiSingleMgr->CreateThread(CREATE_SUSPENDED);
		g_pCSdiSingleMgr->ResumeThread() ;
		TRACE(L"[Success] Run SdiMgr Thread\n");

		//-- 2013-12-01 hongsu@esmlab.com
		//-- 3.Check Connection
		TRACE(L"[Start] Check Get File\n");
#ifdef NX3000_TEST
		if(!g_pCSdiSingleMgr->CheckGetFile())
		{
			TRACE(L"[Fail] Check Get File\n");
			return SDI_ERR_SEMAPHORE_TIMEOUT;
		}
#endif
	}
	return nResult;
}


SdiResult CSdiSdk::SdiGetModel()
{
	return g_pCSdiSingleMgr->GetModel();
}

SdiString CSdiSdk::SdiGetDeviceModel()
{
	CString m_strModel = g_pCSdiSingleMgr->GetDeviceModel();
	return m_strModel;
}

SdiResult CSdiSdk::SdiCloseSession()
{
	if(!g_pCSdiSingleMgr)
		return SDI_ERR_INTERNAL;
	SdiResult nResult = g_pCSdiSingleMgr->SdiCloseSession();
	return nResult;
}

SdiVoid CSdiSdk::SdiSetReceiveHandler(HWND hRecvWnd)
{
    g_pCSdiSingleMgr->SdiSetReceiveHandler(hRecvWnd);
}

SdiResult CSdiSdk::SdiAddMsg(SdiMessage* pMsg)
{
	if(!g_pCSdiSingleMgr)
	{
		if(pMsg->message == WM_SDI_OP_CONNECT_WIFI)
			return SdiCheckWifi();
		else
			return SDI_ERR_INTERNAL;
	}
    return g_pCSdiSingleMgr->SdiAddMsg(pMsg);
}

SdiVoid CSdiSdk::SdiLiveviewSet(SdiBool bLiveview)
{
    if(g_pCSdiSingleMgr)
		g_pCSdiSingleMgr->SetLiveview(bLiveview);
}
SdiVoid CSdiSdk::SdiCheckFunction()
{
	g_pCSdiSingleMgr->CheckFunction();
}

SdiString CSdiSdk::SdiGetInfo(int nCommand)
{
	switch(nCommand)
	{
	case 1:
		return g_pCSdiSingleMgr->GetDeviceUniqueID();
		break;
	case 2:
		return g_pCSdiSingleMgr->GetDeviceModel();
		break;
	case 3:
//		return g_pCSdiSingleMgr->GetConnectID();
		break;
	case 4:
		break;
	case 5:
		break;
	default:
		break;
	}
}


SdiResult CSdiSdk::SdiCheckWifi()
{
	return SDI_ERR_OK;
}

