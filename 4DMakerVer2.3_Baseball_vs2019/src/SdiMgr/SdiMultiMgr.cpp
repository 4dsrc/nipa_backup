/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        SdiMultiMgr.cpp
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */

#include "StdAfx.h"
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <strsafe.h>
#include <setupapi.h>

#include "SdiMultiMgr.h"
#include "SdiSingleMgr.h"
#include "SdiCoreWin7.h"
#include "DeviceInfoMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#pragma comment (lib , "setupapi.lib" )

static const GUID GUID_DEVINTERFACE_USB = 
{ 0xA5DCBF10, 0x6530, 0x11D2, { 0x90, 0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED } };

//------------------------------------------------------------------------------ 
//! @brief		CSdiMultiMgr()
//! @date		2011-05-24
//! @author	hongsu.jung
//! @note	 	
//------------------------------------------------------------------------------
CSdiMultiMgr::CSdiMultiMgr(void)	
{
	m_nPhotoType = 0;
	m_bRecordLiveView = FALSE;
	m_bEnd = FALSE;
	m_bLCDDisplay = TRUE;
	m_nFocusMode = 0;
}
CSdiMultiMgr::~CSdiMultiMgr(void)	
{
	int nAll = m_arPTP.GetCount();
	while(nAll--)
		RemovePTP(nAll);

	
}

//------------------------------------------------------------------------------ 
//! @brief		GetDevice()
//! @date		2011-05-24
//! @author	hongsu.jung
//! @note	 	GetDevice
//------------------------------------------------------------------------------
CSdiSingleMgr* CSdiMultiMgr::GetDevice(int nIndex)
{
	CSdiSingleMgr* pExistPTPESM = NULL;
	for(int i = 0; i < m_arPTP.GetSize(); i++)
	{
		pExistPTPESM = (CSdiSingleMgr*)m_arPTP.GetAt(i);
		if(pExistPTPESM->GetConnectID() == nIndex)
			break;
	}	
	return pExistPTPESM;
}

//------------------------------------------------------------------------------ 
//! @brief		GetDevice()
//! @date		2011-05-24
//! @author	hongsu.jung
//! @note	 	GetDevice
//------------------------------------------------------------------------------
int CSdiMultiMgr::GetConnectID(int nIndex)
{
	CSdiSingleMgr* pExist = NULL;
	pExist = (CSdiSingleMgr*)m_arPTP.GetAt(nIndex);
	if(pExist)
		return pExist->GetConnectID();
	return -1;
}
//------------------------------------------------------------------------------ 
//! @brief		GetDevice(char*)
//! @date		2011-06-011
//! @author	hongsu.jung
//! @note	 	GetDevice
//------------------------------------------------------------------------------
CSdiSingleMgr* CSdiMultiMgr::GetDevice(char* strName)
{
	if(m_arPTP.GetSize()==0)
		return 0L;

	int nLen;
	CString str;
	BSTR buf;


	nLen = MultiByteToWideChar(CP_ACP, 0, strName, strlen(strName), NULL, NULL);
	buf = SysAllocStringLen(NULL, nLen );
	MultiByteToWideChar(CP_ACP, 0, strName, strlen(strName), buf, nLen);
	str.Format(_T("%s"),buf);
	int cut_point = str.Find(_T("{"));
	str = str.Left(cut_point);
	int ret=-1;
	CSdiSingleMgr* pExistPTPESM = NULL;
	
	for(int i = 0; i < m_arPTP.GetSize(); i++)
	{
		pExistPTPESM = (CSdiSingleMgr*)m_arPTP.GetAt(i);
		ret = pExistPTPESM->GetDeviceVendor().Find(str);
		if(ret==0)
			break;
	}
	if(buf)
	{
		delete buf;
		buf = NULL;
	}
	
	return pExistPTPESM;
}

//--------------------------------------------------------------------------
//!@brief	Open AddPTPConnect	
//!@param   [out]   devNum  : Device Numberdl
//!@param   [out]   devInfo : Manufacturer, model , delimiter("")
//!@return	RS_OK on success, any other value means failure.	
//--------------------------------------------------------------------------
CSdiSingleMgr* CSdiMultiMgr::AddPTP(CString strVendor, CString strProduct)
{
	//-- [2011-2-15 Lee JungTaek] 
	//-- Check Exist PTP Connect
	int nAddPTP;
	for(int i = 0; i < m_arPTP.GetSize(); i++)
	{
		CSdiSingleMgr* pExistPTPESM = (CSdiSingleMgr*)m_arPTP.GetAt(i);
		if(pExistPTPESM->GetDeviceVendor() == strVendor)
			return NULL;
	}

	int nAll = GetPTPCount();
	CSdiSingleMgr* pPTP = new CSdiSingleMgr(nAll, strVendor, strProduct);
	if(1 > nAll) // dh0.seo 2014-12-17 Multi Connect 안됨
	{
		if(!pPTP)
			return NULL;	

		pPTP->SdiSetReceiveHandler(m_hRecvWnd);

		nAddPTP = m_arPTP.Add((CObArray*)pPTP);
	
		TRACE(_T("[MTP] PTP_InitSession Start [%d] \n"),m_arPTP.GetSize());
		int nRet = pPTP->PTP_InitSession(strVendor);
		//if(nRet < 1)
		//{
		//	AfxMessageBox(_T("PTP_INITIAL ERROR"));
		//	return NULL;
		//}
	
		//-- 2011-05-24 hongsu.jung
		//-- OpenSession
		TRACE(_T("[MTP] PTP_OpenSession \n"));
		pPTP->PTP_OpenSession();
		pPTP->m_bCloseSession = FALSE;

		//-- 2011-8-9 Lee JungTaek
		//-- Get AF Mode State
		pPTP->PTP_GetDevPropDesc(PTP_CODE_FOCUSMODE);
		//-- Get Dev Mode
		pPTP->PTP_GetDevModeIndex();

		//-- 2011-05-24 hongsu.jung
		//-- Create Thread	
		pPTP->CreateThread(CREATE_SUSPENDED);	
		pPTP->ResumeThread() ;	

		return pPTP;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		RemovePTP()
//! @date		2011-05-24
//! @author	hongsu.jung
//! @note	 	RemovePTP
//------------------------------------------------------------------------------
void CSdiMultiMgr::RemovePTP(int nIndex)
{ 
	CSdiSingleMgr* pExist = NULL;
	//-- Reorder PTP
	int nAll = m_arPTP.GetCount();	
	
	if(nIndex >= nAll)
		return;

	for(int i = nIndex + 1; i < nAll; i ++)
	{
		pExist = GetPTP(i);
		if(pExist)	pExist->ReorderID();
	}

	pExist = GetPTP(nIndex); 
	if(!pExist) return;
	TRACE(_T("[MTP] Remove PTP[%d] : %s\n"),nIndex,pExist->GetDeviceUniqueID());	
	
	//-- 2011-08-08 jeansu - RemoveAt() 이 잘 먹지 않는 이유로 RemoveAt 먼저 하고, delete 하도록 변경
	m_arPTP.RemoveAt(nIndex);
	/*if(pExist)
	{
		pExist->m_pLiveviewConverter->m_bThreadFlag = FALSE;
		delete pExist;
		pExist = NULL;
	}*/

	if( pExist)
	{
		delete pExist;
		pExist = NULL;				
	}
}

//------------------------------------------------------------------------------ 
//! @brief		RemovePTPAll()
//! @date		2011-05-24
//! @author	hongsu.jung
//------------------------------------------------------------------------------
void CSdiMultiMgr::RemovePTPAll()
{
	int nAll = GetPTPCount();
	while(nAll--)
		RemovePTP(nAll);
}

//------------------------------------------------------------------------------ 
//! @brief		InitDeviceList()
//! @date		2011-05-24
//! @author	hongsu.jung
//! @note	 	Initializing Device List
//------------------------------------------------------------------------------
int CSdiMultiMgr::InitDeviceList(void)
{
	int nChange = CONNECT_INFO_NULL;

	TRACE(_T("[MTP] InitDeviceList Start\n"));
    GUID guidDeviceInterface = GUID_DEVINTERFACE_USB; //in the INF file
    BOOL bResult = TRUE;
    HANDLE hDeviceHandle = INVALID_HANDLE_VALUE;
    ULONG cbSize = 0;	

	CStringArray arDeviceName;
    bResult = GetDeviceHandle(&arDeviceName, guidDeviceInterface, &hDeviceHandle);
	if(!bResult)
		return CONNECT_INFO_ERR;

	int nAll = arDeviceName.GetSize();
	CString strName;	
	for(int i = 0 ; i < nAll ; i ++)
	{
		strName = arDeviceName.GetAt(i);
		if(!AddPTP(strName,NULL))
			nChange = CONNECT_INFO_ERR;
		else
		{
			nChange = CONNECT_INFO_ADD;
		}
	}

	//-- Release Device Name
	arDeviceName.RemoveAll();
	return nChange;
}
//------------------------------------------------------------------------------ 
//! @brief		ChangeDeviceList()
//! @date		2011-05-24
//! @author	hongsu.jung
//! @note	 	Initializing Connector
//------------------------------------------------------------------------------
#ifndef _NonOpenCV
int CSdiMultiMgr::ChangeDeviceList(int nSelectedItem, IplImage **pLiveviewBuffer)
{
	TRACE(_T("[MTP] ChangeDeviceList \n"));
	GUID guidDeviceInterface = GUID_DEVINTERFACE_USB; //in the INF file
    BOOL bResult = TRUE;
	int		nChange = CONNECT_INFO_NULL;
    HANDLE hDeviceHandle = INVALID_HANDLE_VALUE;
    ULONG cbSize = 0;		

	CStringArray arDeviceName;
    bResult = GetDeviceHandle(&arDeviceName, guidDeviceInterface, &hDeviceHandle);
	if(!bResult)
		return CONNECT_INFO_ERR;

	int i,j;
	BOOL bExist;
	CString strDeviceName;
	CSdiSingleMgr* pExist = NULL;	
	int nDetectAll = arDeviceName.GetSize();
	int nExistAll = m_arPTP.GetSize();
	
	
	//--
	//-- Add New ComboBox
	for(i = 0 ; i < nDetectAll ; i++)
	{
		strDeviceName = arDeviceName.GetAt(i);
		bExist = FALSE;
		//-- Check Exist
		for(j = 0; j < nExistAll ; j++)
		{
			pExist = (CSdiSingleMgr*)m_arPTP.GetAt(j);
			if(strDeviceName == pExist->GetDeviceVendor())
			{
				bExist = TRUE;
				break;			
			}
		}
		//-- Add New Item
		if(!bExist)
		{
			TRACE(_T("[MTP] Add PTP [%s]\n"),strDeviceName);
			Sleep(5000);
			if(!AddPTP(strDeviceName,NULL))
				nChange = CONNECT_INFO_ERR;
			else
			{
				nChange = CONNECT_INFO_ADD;	
			}
			break;
		}
	}
	//--
	//-- Delete Removed Item
	for(j = 0 ; j < nExistAll; j++)
	{
		pExist = (CSdiSingleMgr*)m_arPTP.GetAt(j);
		if(pExist)
		{
			//-- Check Exist
			bExist = FALSE;
			for(i = 0; i < nDetectAll ; i++)
			{
				strDeviceName = arDeviceName.GetAt(i);
				if(strDeviceName == pExist->GetDeviceVendor())
				{
					bExist = TRUE;
					break;
				}
			}		
			//-- Not Exist -> Removed
			if(!bExist)
			{
				//-- Stop Liveview
				//TRACE(_T("[MTP] Stop Liveview [%d]\n"),i);
				SetLiveView(nSelectedItem, i, FALSE);

				//-- 2011-8-4 jeansu : 멀티 연결 상태에서 연결 해제시 해당 기기의 liveview 멈춘 후, buffer 초기화.
				if(pLiveviewBuffer!=NULL)
					pLiveviewBuffer[j] = NULL;

				//-- Remove Liveview
				RemovePTP(j);
				if(nSelectedItem == j)
					nChange = CONNECT_INFO_REMOVE_THIS;				
				else if(nSelectedItem < j)		
					nChange = CONNECT_INFO_REMOVE_AFTER;
				else if(nSelectedItem > j)	
					nChange = CONNECT_INFO_REMOVE_BEFORE;
				break;
			}
		}
	}	
	return nChange;
}
#endif
//------------------------------------------------------------------------------ 
//! @brief		SetConnect()
//! @date		2011-05-24
//! @author	hongsu.jung
//! @note	 	SetConnect
//------------------------------------------------------------------------------
BOOL CSdiMultiMgr::SetConnect(UINT nStatus, UINT nIndex)
{
	CSdiSingleMgr* pPTP = NULL;
	pPTP = GetPTP(nIndex);
	if(pPTP)
	{
		//-- ` SESSION
		if(nStatus == PTP_STAT_DISCONNECT)
		{
			if(RS_NG == pPTP->PTP_CloseSession())
				return FALSE;			
		}
		//-- OPEN SESSION
		else
		{
			if(RS_OK == pPTP->PTP_OpenSession())
				return TRUE;
		}
	}
	return FALSE;
}
//------------------------------------------------------------------------------ 
//! @brief		GetDeviceHandle()
//! @date		2011-05-24
//! @update	2011-07-18
//! @author	hongsu.jung
//! @note	 	GetDeviceHandle // CStringArray* pArList
//------------------------------------------------------------------------------
BOOL CSdiMultiMgr::GetDeviceHandle (CStringArray* pArList, GUID guidDeviceInterface, PHANDLE hDeviceHandle)
{
	if (guidDeviceInterface==GUID_NULL)
	{
		return FALSE;
	}

	BOOL bResult = TRUE;
	HDEVINFO hDevInfo;
	SP_DEVINFO_DATA DevInfoData;

	SP_DEVICE_INTERFACE_DATA DevInterfaceData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA pInterfaceDetailData = NULL;

	ULONG requiredLength=0;

	LPTSTR lpDevPath = NULL;
	CString strTemp = _T("");

	DWORD index = 0;

	hDevInfo = SetupDiGetClassDevs(&guidDeviceInterface, NULL, NULL,DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);

	if (hDevInfo == INVALID_HANDLE_VALUE) 
	{ 
		printf("Error SetupDiGetClassDevs: %d.\n", GetLastError());
		goto done;
	}

	DevInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
	
	for (index = 0; SetupDiEnumDeviceInfo(hDevInfo, index, &DevInfoData); index++)
	{
		if (lpDevPath)
		{
			LocalFree(lpDevPath);
			lpDevPath = NULL;
		}
		if (pInterfaceDetailData)
		{
			LocalFree(pInterfaceDetailData);
			pInterfaceDetailData = NULL;
		}

 		DevInterfaceData.cbSize = sizeof(SP_INTERFACE_DEVICE_DATA);
 
 		bResult = SetupDiEnumDeviceInterfaces(hDevInfo, NULL, &guidDeviceInterface, index, &DevInterfaceData);
 
		DWORD dwError;

		dwError = GetLastError();

 		if (GetLastError () == ERROR_NO_MORE_ITEMS)
 		{
 			break;
 		}

		if (!bResult) 
		{
			printf("Error SetupDiEnumDeviceInterfaces: %d.\n", GetLastError());
			goto done;
		}

		bResult = SetupDiGetDeviceInterfaceDetail(hDevInfo,	&DevInterfaceData, NULL, 0,	&requiredLength, NULL);


		if (!bResult) 
		{
			if ((ERROR_INSUFFICIENT_BUFFER==GetLastError()) && (requiredLength>0))
			{
				pInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)LocalAlloc(LPTR, requiredLength);

				if (!pInterfaceDetailData) 
				{ 
					// ERROR 
					printf("Error allocating memory for the device detail buffer.\n");
					goto done;
				}
			}
			else
			{
				printf("Error SetupDiEnumDeviceInterfaces: %d.\n", GetLastError());
				goto done;
			}
		}

		if(pInterfaceDetailData)
			pInterfaceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

		bResult = SetupDiGetDeviceInterfaceDetail(hDevInfo, &DevInterfaceData, pInterfaceDetailData, requiredLength, NULL, &DevInfoData);

		if (!bResult) 
		{
			printf("Error SetupDiGetDeviceInterfaceDetail: %d.\n", GetLastError());
			goto done;
		}
		else
		{
			size_t nLength = 0;
			if(pInterfaceDetailData)
				nLength = wcslen (pInterfaceDetailData->DevicePath) + 1;

			lpDevPath = (TCHAR *) LocalAlloc (LPTR, nLength * sizeof(TCHAR));
			if(!lpDevPath)
			{
				printf("Error %d.", GetLastError());
				goto done;
			}

			StringCchCopy(lpDevPath, nLength, pInterfaceDetailData->DevicePath);
			lpDevPath[nLength-1] = 0;

			printf("Device path: %S\n", lpDevPath);
			CString temp;
			temp.Format(_T("%s"),lpDevPath);

			if(1<temp.Find(SDI_VID_SAMSUNG))
			{
				if(1<temp.Find(SDI_PID_NX200))		pArList->Add(temp);	
				else if(1<temp.Find(SDI_PID_NX1000))	pArList->Add(temp);
				else if(1<temp.Find(SDI_PID_NX2000))	pArList->Add(temp);
				else if(1<temp.Find(SDI_PID_NX1))	pArList->Add(temp);	
				else if(1<temp.Find(SDI_PID_NX500))	pArList->Add(temp);	
			}

			if(1<temp.Find(SDI_VID_PANASONIC))
			{
				if(1<temp.Find(SDI_PID_GH5))		pArList->Add(temp);	
				else if(1<temp.Find(SDI_PID_GH5_2))		pArList->Add(temp);	
			}

			if(!lpDevPath)
			{
				printf("Error %d.", GetLastError());
				goto done;
			}
		}
	}	

done:
	if(lpDevPath)
		LocalFree(lpDevPath);
	if(pInterfaceDetailData)
		LocalFree(pInterfaceDetailData);    
	
	bResult = SetupDiDestroyDeviceInfoList(hDevInfo);
	
	return bResult;
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateInfomation()
//! @date		2011-05-24
//! @author	hongsu.jung
//! @note	 	nIndex == -1 ; all device
//------------------------------------------------------------------------------
void CSdiMultiMgr::GetConnectList(CStringArray* pArray)
{
	//-- Get Connected Divece.
	int nAll = GetPTPCount();
	TRACE(_T("[Boot] Update Connection List [%d]\n"),nAll);
	CSdiSingleMgr* pPTP = NULL;
	//-- Add All List
	for(int i = 0 ; i < nAll ; i++)
	{
		pPTP = GetDevice(i);
		if(pPTP)
			pArray->Add(pPTP->GetDeviceUniqueID());
	}		
}

BOOL CSdiMultiMgr::UpdateComboBox(CComboBox* pCmb)
{
	//-- Reset Combo
	int nCmbAll = pCmb->GetCount();
	//-- Get Connected Divece.
	int nAll = GetPTPCount();
	//-- Test
	TRACE(">> Get Connection [%d] Device\n", nAll);

	int i, j, nAddedCmb = 0;
	BOOL bExist = FALSE;
	
	CSdiSingleMgr* pPTP = NULL;
	CSdiSingleMgr* pCmbExist = NULL;


	//-- Add New ComboBox
	for(i = 0 ; i < nAll ; i++)
	{
		pPTP = GetDevice(i);
		if(!pPTP)
		{
			TRACE(_T("None Exist PTP\n"));
			continue;
		}
		else
		{
			bExist = FALSE;
			//-- Check Exist
			for(j = 0; j < nCmbAll ; j++)
			{
				pCmbExist = (CSdiSingleMgr*)pCmb->GetItemDataPtr(j);
				if(pPTP == pCmbExist)
				{
					bExist = TRUE;
					break;
				}
			}
			//-- Add New Item
			if(!bExist)
			{	
				//-- 2011-6-27 Lee JungTaek
				//-- Set Unique ID -- Temp Code
				//nAddedCmb = pCmb->AddString(pPTP->GetDeviceVendor());
				nAddedCmb = pCmb->AddString(pPTP->GetDeviceUniqueID());
				
				CString strDevUserName = _T("");
				//strDevUserName = GetDevUserName(pPTP);
				//	nAddedCmb = pCmb->AddString(strDevUserName);
								
				pCmb->SetItemData(nAddedCmb, (DWORD_PTR)pPTP);
			}
		}		
	}		

	//-- 2011-05-31 hongsu.jung
	//-- Delete Removed Item
	for(j = 0 ; j < nCmbAll; j++)
	{
		pCmbExist = (CSdiSingleMgr*)pCmb->GetItemDataPtr(j);
		if(!pCmbExist)
		{
			TRACE(_T("None Exist Combo Item\n"));
			continue;
		}
		else
		{
			//-- Check Exist
			bExist = FALSE;
			for(i = 0; i < nAll ; i++)
			{
				pPTP = GetDevice(i);
				if(pPTP == pCmbExist)
				{
					bExist = TRUE;
					break;
				}
			}
			//-- Not Exist -> Removed
			if(!bExist)
				pCmb->DeleteString(j);
		}		
	}		

	if(pCmb->GetCount())
		pCmb->SetCurSel(0);
	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief		GetModeIndex
//! @date		2011-6-25
//! @author		Lee JungTaek
//! @note	 	Get and Set Unique ID
//------------------------------------------------------------------------------ 
void CSdiMultiMgr::GetModeIndex(int nDevIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nDevIndex);
	if(!pDevice)
		return;

	pDevice->PTP_GetDevModeIndex();
	return;
}

//------------------------------------------------------------------------------ 
//! @brief		GetDevUniqueID
//! @date		2011-6-25
//! @author		Lee JungTaek
//! @note	 	Get Unique ID
//------------------------------------------------------------------------------ 
CString CSdiMultiMgr::GetDevUniqueID(int nDevIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nDevIndex);
	if(!pDevice)
		return _T("");

	return pDevice->GetDeviceUniqueID();
}
int CSdiMultiMgr::GetCurModeIndex(int nDevIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nDevIndex);
	if(!pDevice)
		return 0;

	return pDevice->GetCurModeIndex();
}

int CSdiMultiMgr::GetCurPhysicalMode(int nDevIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nDevIndex);
	if(!pDevice)
		return 0;

	return pDevice->GetCurPhysicalMode();
}

//------------------------------------------------------------------------------ 
//! @brief		GetCurModel
//! @date		2013-6-27
//! @author		dh9.seo
//! @note	 	Get Model
//------------------------------------------------------------------------------ 
CString CSdiMultiMgr::GetCurModel(int nDevIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nDevIndex);
	if(!pDevice)
		return _T("");

	return pDevice->GetDeviceModel();
}

//------------------------------------------------------------------------------ 
//! @brief		GetPropDesc()
//! @date		2011-06-09
//! @author	hongsu jung
//! @note	 	Get Property Info
//------------------------------------------------------------------------------
void CSdiMultiMgr::GetPropDesc(int nDevIndex, int nPTPCode)
{
//	m_strName.Format(_T("SdiMutiMgr GetPropDesc nDevIndex= %d, nPTPCode = %d"), nDevIndex, nPTPCode);

	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nDevIndex);
	if(!pDevice)
		return;
	
	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_RS_GET_PROPERTY_INFO;
	pEvent->nParam1	= nPTPCode;	
	pEvent->nParam2	= nDevIndex;
	pDevice->AddMsg((CObject *)pEvent);
	
/*	pEvent->pParam = (LPARAM)(LPCTSTR)m_strName;
	pEvent->message = WM_LOG_MSG;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_LOG_MSG, (WPARAM)pEvent->message, NULL);*/

	return;
}

//------------------------------------------------------------------------------ 
//! @brief		GetType()
//! @date		2011-06-09
//! @author	hongsu.jung
//------------------------------------------------------------------------------
int CSdiMultiMgr::GetType(int nIndex, int nPTPCode)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return PTP_VALUE_NONE;

	CDeviceInfoMgr* pInfo = NULL;
	pInfo = pDevice->m_pSdiCore->GetDeviceInfo();
	return pInfo->GetType(nPTPCode);
}

//CMiLRe 20141029 White Balnace Detail 종류별로 R/W
int CSdiMultiMgr::GetProcSub(int nIndex, int nPTPCode)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return PTP_VALUE_NONE;

	CDeviceInfoMgr* pInfo = NULL;
	pInfo = pDevice->m_pSdiCore->GetDeviceInfo();
	return pInfo->GetPropCode_Sub(nPTPCode);
}

//------------------------------------------------------------------------------ 
//! @brief		GetCurrentPropValueStr()
//! @date		2011-06-09
//! @author	hongsu.jung
//------------------------------------------------------------------------------
CString CSdiMultiMgr::GetCurrentPropValueStr(int nIndex, int nPTPCode)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return _T("");

	CString strValue;
	CDeviceInfoMgr* pInfo = NULL;
	pInfo = pDevice->m_pSdiCore->GetDeviceInfo();
	strValue = pInfo->GetCurrentStr(nPTPCode);
	return strValue;
}
//------------------------------------------------------------------------------ 
//! @brief		GetCurrentPropValueEnum()
//! @date		2011-06-09
//! @author	hongsu.jung
//------------------------------------------------------------------------------
int CSdiMultiMgr::GetCurrentPropValueEnum(int nIndex, int nPTPCode)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return -1;

	int nValue;
	CDeviceInfoMgr* pInfo = NULL;
	pInfo = pDevice->m_pSdiCore->GetDeviceInfo();
	nValue = pInfo->GetCurrentEnum(nPTPCode);
	return nValue;
}

//------------------------------------------------------------------------------ 
//! @brief		GetPropValueCntEnum()
//! @date		2011-06-15
//! @author		Lee JungTaek
//------------------------------------------------------------------------------
int CSdiMultiMgr::GetPropValueCntStr(int nIndex, int nPTPCode)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return 0;

	int nCnt;
	CDeviceInfoMgr* pInfo = NULL;
	pInfo = pDevice->m_pSdiCore->GetDeviceInfo();
	nCnt = pInfo->GetPropValueCntStr(nPTPCode);
	return nCnt;
}
//------------------------------------------------------------------------------ 
//! @brief		GetPropValueCntEnum()
//! @date		2011-06-15
//! @author		Lee JungTaek
//------------------------------------------------------------------------------
int CSdiMultiMgr::GetPropValueCntEnum(int nIndex, int nPTPCode)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return 0;

	int nCnt;
	CDeviceInfoMgr* pInfo = NULL;
	pInfo = pDevice->m_pSdiCore->GetDeviceInfo();
	nCnt = pInfo->GetPropValueCntEnum(nPTPCode);
	if(nPTPCode == PTP_CODE_FOCUSMODE)
		m_nFocusMode = nCnt;
	return nCnt;
}

//------------------------------------------------------------------------------ 
//! @brief		GetPropValueListEnum()
//! @date		2011-06-15
//! @author		Lee JungTaek
//------------------------------------------------------------------------------
int CSdiMultiMgr::GetPropValueStatusStr(int nIndex, int nPTPCode, CString strPropValue)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return NULL;

	int nStatus = PROPERTY_STATUS_DISABLE;
	CDeviceInfoMgr* pInfo = NULL;
	pInfo = pDevice->m_pSdiCore->GetDeviceInfo();
	nStatus = pInfo->GetPorpValueStatusStr(nPTPCode, strPropValue);
	return nStatus;
}
//------------------------------------------------------------------------------ 
//! @brief		GetPropValueListEnum()
//! @date		2011-06-15	
//! @author		Lee JungTaek
//------------------------------------------------------------------------------
int CSdiMultiMgr::GetPropValueStatusEnum(int nIndex, int nPTPCode, int nPropValue)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return NULL;

	int nStauts = PROPERTY_STATUS_DISABLE;
	CDeviceInfoMgr* pInfo = NULL;
	pInfo = pDevice->m_pSdiCore->GetDeviceInfo();
	nStauts = pInfo->GetPorpValueStatusEnum(nPTPCode, nPropValue);
	return nStauts;
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CUIntArray* CSdiMultiMgr::GetPropValueListEnum(int nIndex, int nPTPCode)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return NULL;

	CUIntArray *parEnum = FALSE;
	CDeviceInfoMgr* pInfo = NULL;
	pInfo = pDevice->m_pSdiCore->GetDeviceInfo();
	parEnum = pInfo->GetPropValueListEnum(nPTPCode);
	return parEnum;
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropertyValueStr
//! @date		2011-6-15
//! @author		Lee JungTaek
//! @note	 	Set String Property (PhotoSize)
//------------------------------------------------------------------------------ 
void CSdiMultiMgr::SetPropertyValueStr(int nIndex, int nPTPCode, int nType, CString strValue)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;
	
	TCHAR* cValue = new TCHAR[1+strValue.GetLength()];
	_tcscpy(cValue, strValue);
	
	RSEvent* pEvent = new RSEvent();
	pEvent->message = WM_RS_SP_SET_PROPERTY_VALUE;
	pEvent->nParam1 = (int)nPTPCode;
	pEvent->nParam2	= nType;
	pEvent->pParam	= (LPARAM)cValue;

	pDevice->AddMsg((CObject *)pEvent);

	return;
}
//------------------------------------------------------------------------------ 
//! @brief		SetPropertyValueEnum
//! @date		2011-6-15
//! @author		Lee JungTaek
//! @note	 	Set Enum Property (Quality ...)
//------------------------------------------------------------------------------ 
void CSdiMultiMgr::SetPropertyValueEnum(int nIndex, int nPTPCode, int nType, int nPropValue)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;
		
	RSEvent* pEvent = new RSEvent();
	pEvent->message = WM_RS_SP_SET_PROPERTY_VALUE;
	pEvent->nParam1 = (int)nPTPCode;
	pEvent->nParam2	= nType;
	pEvent->pParam	= (LPARAM)(int)nPropValue;

	pDevice->AddMsg((CObject *)pEvent);

	return;
}

//------------------------------------------------------------------------------ 
//! @brief		GetLiveviewInfo()
//! @date		2011-06-09
//! @author	hongsu.jung
//------------------------------------------------------------------------------
int CSdiMultiMgr::GetLiveviewInfo(int nIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return -1;

	//-- 2014-09-30 joonho.kim
	//-- update photosize
	pDevice->m_nPhotoType = m_nPhotoType;
	pDevice->m_bRecordLiveView = m_bRecordLiveView;
	pDevice->m_bEnd = m_bEnd;
	pDevice->m_bLCDDisplay = m_bLCDDisplay;

	RSEvent* pEvent		= new RSEvent();
	pEvent->message	= WM_RS_MC_LIVEVIEW_INFO;
	pEvent->nParam1	= nIndex;	
	return pDevice->AddMsg((CObject *)pEvent);
}

void CSdiMultiMgr::SetDeviceLCDDisplay(int nIndex, BOOL bDisplay)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return ;

	pDevice->m_bLCDDisplay = bDisplay;
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-6-24
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CSdiMultiMgr::SetFocusPosition(int nIndex, int nType)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return -1;

	RSEvent* pEvent		= new RSEvent();
	pEvent->message	= WM_RS_LIVEVIEW_SET_FOCUSPOSITION;	
	pEvent->nParam1	= nType;
	return pDevice->AddMsg((CObject *)pEvent);
}

//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
int CSdiMultiMgr::SetFocusPositionValue(int nIndex, int nValue)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return -1;

	RSEvent* pEvent		= new RSEvent();
	pEvent->message	= WM_RS_SET_FOCUS_VALUE;	
	pEvent->nParam1	= nValue;
	return pDevice->AddMsg((CObject *)pEvent);
}

//------------------------------------------------------------------------------ 
//! @brief		GetSrcSize()
//! @date		2011-06-13
//! @author	hongsu.jung
//------------------------------------------------------------------------------
CSize	CSdiMultiMgr::GetSrcSize(int nIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return CSize(SRC_WIDTH,SRC_HEIGHT);
	return pDevice->GetSrcSize();
}

//------------------------------------------------------------------------------ 
//! @brief		GetSrcSize()
//! @date		2011-07-15
//! @author	hongsu.jung
//------------------------------------------------------------------------------
CSize	CSdiMultiMgr::GetOffsetSize(int nIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return CSize(0,0);
	return pDevice->GetOffsetSize();
}

BOOL CSdiMultiMgr::IsReadyDraw(int nIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return FALSE;

	pDevice->m_nPhotoType = m_nPhotoType;
	
	return pDevice->ISSet();
}

void CSdiMultiMgr::IsSetRecordLiveView(int nIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return ;

	pDevice->m_bRecordLiveView = m_bRecordLiveView;
	pDevice->m_bEnd = m_bEnd;
}
//------------------------------------------------------------------------------ 
//! @brief		GetCaptureImage()
//! @date		2011-05-31
//! @author		chlee
//! @note	 	Get Catpure Image
//------------------------------------------------------------------------------
void CSdiMultiMgr::GetCaptureImage(int nIndex, CString strFile)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;

	RSEvent* pEvent = new RSEvent();
	pEvent->message = WM_RS_MC_CAPTURE_IMAGE;
	pEvent->nParam1	= nIndex;

	TRACE(_T("GetCaptureImage:: Get Capture Finish Event [%d]\n"),nIndex);

	//-- 2011-05-31 hongsu.jung
	//-- Send String Message -> char
	/*
	int length = strFile.GetLength();
	char *buffer = new char[length];
	//char buffer[128];
	strcpy(buffer,CT2A(strFile));
	pEvent->pParam	= (LPARAM)buffer;
	*/
	pDevice->AddMsg((CObject *)pEvent);
	return;
}
//------------------------------------------------------------------------------ 
//! @brief		GetCaptureImageComplete()
//! @date		2011-05-31
//! @author		chlee
//! @note	 	Get Catpure Image
//------------------------------------------------------------------------------
void CSdiMultiMgr::GetCaptureImageComplete(int nIndex, TCHAR* strPath, int nFilenum, int nClose, int nType)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);
	if(!pDevice)
		return;

	TRACE(_T("GetCaptureImageComplete:: Get Capture Finish Event [%d]\n"),nIndex);

	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_RS_MC_CAPTURE_IMAGE;	
	pEvent->pParam		= (LPARAM)strPath;
	pEvent->nParam1 = nFilenum;
	pEvent->nParam2 = nClose;
	pEvent->nParam3 = nType;
	pDevice->AddMsg((CObject *)pEvent);

	pDevice->SetLiveview(TRUE);

	return;
}
void CSdiMultiMgr::SetMovieCancle(int nIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);
	if(!pDevice)
		return;

	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_RS_MC_RECORD_CANCLE;	
	pDevice->AddMsg((CObject *)pEvent);

	return;
}
void CSdiMultiMgr::GetRecordMovieComplete(int nIndex, TCHAR* strPath,int nFilenum, int nClose)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);
	if(!pDevice)
		return;

	TRACE(_T("GetCaptureImageComplete:: Get Capture Finish Event [%d]\n"),nIndex);

	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_RS_MC_RECORD_MOVIE;	
	pEvent->pParam		= (LPARAM)strPath;
	pEvent->nParam1 = nFilenum;
	pEvent->nParam2 = nClose;
	pDevice->AddMsg((CObject *)pEvent);

	pDevice->SetLiveview(TRUE);

	return;
}

void CSdiMultiMgr::GetRecordStatus(int nSelected, int nIndex)
{
	//-- STOP LIVEVIEW
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;
	
	RSEvent* pEvent = new RSEvent();
	pEvent->message = WM_RS_MC_GET_RECORD_STATUS;
	pDevice->AddMsg((CObject *)pEvent);
}

void CSdiMultiMgr::GetCaptureCount(int nSelected, int nIndex)
{
	//-- STOP LIVEVIEW
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;
	
	RSEvent* pEvent = new RSEvent();
	pEvent->message = WM_RS_MC_GET_CAPTURE_COUNT;
	pDevice->AddMsg((CObject *)pEvent);
}
//------------------------------------------------------------------------------ 
//! @brief		GetCaptureImageRequire()
//! @date		2011-06-01
//! @author		chlee
//! @note	 	Get Catpure Image Require operation
//------------------------------------------------------------------------------
void CSdiMultiMgr::GetCaptureOrder(int nSelected, int nIndex, int nOrder)
{
	//-- STOP LIVEVIEW
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;
	
	//-- Check AF
	if(nOrder == S2_PRESS)
	{
		int nAfMode = pDevice->m_pSdiCore->GetDeviceInfo()->GetCurrentEnum(SDI_DPC_FOCUSMODE);
		if(SDI_DPV_AF_MODE_MF != nAfMode)
		{
			int nFocus = pDevice->GetAFDetect();
			if(AF_DETECT == nFocus)
				//pDevice->SetCapturing(TRUE);
				pDevice->SetCapturing(FALSE); //dh0.seo Captureing Processing...중에 m_bLiveview 를 False -> True로 바꿔주기 위함.
		}
		else pDevice->SetCapturing(TRUE);

		//-- 2013-03-01 hongsu.jung
		//pDevice->SetLiveview(FALSE);
	}

	RSEvent* pEvent = new RSEvent();
	switch(nOrder)
	{
	case S1_PRESS:			pEvent->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1;			TRACE(_T("DSC[%d] S1 PRESS\n"),nIndex);		break;
	case S2_PRESS:			pEvent->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2;			TRACE(_T("DSC[%d] S2 PRESS\n"),nIndex);		break; 
	case S2_RELEASE:		pEvent->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_3;			TRACE(_T("DSC[%d] S2 RELEASE\n"),nIndex);	break;
	case S1_RELEASE:		pEvent->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4;			TRACE(_T("DSC[%d] S1 RELEASE\n"),nIndex);	break;
	//NX1 dh0.seo 2014-08-28
	case S3_PRESS:			pEvent->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_5;			TRACE(_T("DSC[%d] S3 PRESS\n"),nIndex);	break;
	case S3_RELEASE:		pEvent->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_6;			TRACE(_T("DSC[%d] S3 RELEASE\n"),nIndex);	break;
	case CAPTURE_ONCE:		pEvent->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_ONCE;		TRACE(_T("DSC[%d] Once Capture\n"),nIndex);		break;
		break;
	}
	pDevice->AddMsg((CObject *)pEvent);

	return;
}

//------------------------------------------------------------------------------ 
//! @brief		SaveBank
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSdiMultiMgr::SaveBank(int nIndex, CString strBankName)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;

	CDeviceInfoMgr* pInfo = NULL;
	pInfo = pDevice->m_pSdiCore->GetDeviceInfo();
	pInfo->SaveBank(strBankName);
}

//------------------------------------------------------------------------------ 
//! @brief		LoadBank
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSdiMultiMgr::LoadBank(int nIndex, CString strBankName)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;

	CDeviceInfoMgr* pInfo = NULL;
	pInfo = pDevice->m_pSdiCore->GetDeviceInfo();

	if(pInfo)
		pInfo->LoadBank(strBankName);
}

//------------------------------------------------------------------------------ 
//! @brief		LoadBankMulti
//! @date		2011-8-9
//! @author		Lee JungTaek
//! @note	 	Load Bank Multi
//------------------------------------------------------------------------------ 
void CSdiMultiMgr::LoadBankMulti(int nIndex, int nSelected)
{
	//-- Only Different Camera
	if(nIndex == nSelected)
		return;

	//-- Get Other Camera
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;

	CDeviceInfoMgr* pInfo = NULL;
	pInfo = pDevice->m_pSdiCore->GetDeviceInfo();

	//-- Get Main Camera
	CSdiSingleMgr* pDeviceMain = NULL;
	pDeviceMain = GetDevice(nSelected);		
	if(!pDeviceMain)
		return;

	CDeviceInfoMgr* pInfoMain = NULL;
	pInfoMain = pDeviceMain->m_pSdiCore->GetDeviceInfo();

	for(int i = 0; i < PTP_TYPE_SIZE; i++)
	{
		CDeviceInfoBase* pInfoBaseMain = pInfoMain->GetDeviceInfoByIndex(i);
		CDeviceInfoBase* pInfoBaseSub = pInfo->GetDeviceInfoByIndex(i);

		//-- Set Current Value
		pInfoBaseSub->SetPropCode(pInfoBaseMain->GetPropCode());
		pInfoBaseSub->SetType(pInfoBaseMain->GetType());
		pInfoBaseSub->SetCurrentEnum(pInfoBaseMain->GetCurrentEnum());
		pInfoBaseSub->SetCurrentStr(pInfoBaseMain->GetCurrentStr());
	}
}

//------------------------------------------------------------------------------ 
//! @brief		SetLiveViewAll()
//! @date		2011-07-01
//! @author	hongsu.jung
//! @note	 	Liveview Change All
//------------------------------------------------------------------------------
void CSdiMultiMgr::SetLiveViewAll(int nSelected, BOOL bLiveview)
{
	CSdiSingleMgr* pDevice = NULL;
	int nAll = GetPTPCount();
	while(nAll--)
		SetLiveView(nSelected, nAll, bLiveview);
}

//------------------------------------------------------------------------------ 
//! @brief		SetLiveView
//! @date		2011-07-01
//! @author	hongsu.jung
//! @note	 	Liveview Change
//------------------------------------------------------------------------------
void CSdiMultiMgr::SetLiveView(int nSelected, int nIndex, BOOL bLiveview)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;
	
	BOOL bSelected = (nIndex == nSelected)	?	TRUE :  FALSE;
	pDevice->SetPollLiveview(bSelected, bLiveview);
}


void CSdiMultiMgr::FormatDevice(int nIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;

	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_RS_OPT_FORMAT_DEVICE;	
	pDevice->AddMsg((CObject *)pEvent);
}

void CSdiMultiMgr::ResetDevice(int nIndex, int nParam)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;

	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_RS_OPT_RESET_DEVICE;
	pEvent->nParam1 = nParam;
	pDevice->AddMsg((CObject *)pEvent);
}

//dh0.seo 2014-11-04
void CSdiMultiMgr::SensorCleaningDevice(int nIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;

	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_RS_OPT_SENSOR_CLEANING_DEVICE;	
	pDevice->AddMsg((CObject *)pEvent);
}

void CSdiMultiMgr::TrackingAFStop(int nIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;

	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_RS_TRACKING_AF_STOP;	
	pDevice->AddMsg((CObject *)pEvent);
}

void CSdiMultiMgr::ControlTouchAF(int nIndex, int nX, int nY)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;

	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_RS_CONTROL_TOUCH_AF;	
	pEvent->nParam1 = nX;
	pEvent->nParam2 = nY;
	pDevice->AddMsg((CObject *)pEvent);
}

BOOL CSdiMultiMgr::IsCapturing(int nIndex){
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return FALSE;
	return pDevice->IsCapturing();
}

//CMiLRe 20141113 IntervalCapture Stop 추가
void CSdiMultiMgr::IntervalCaptureStop(int nIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;

	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_RS_STOP_INTERVAL_CAPUTRE;	
	pDevice->AddMsg((CObject *)pEvent);
}

//CMiLRe 20141127 Display Save Mode 에서 Wake up
void CSdiMultiMgr::DisplaySaveModeWakeUp(int nIndex)
{
	CSdiSingleMgr* pDevice = NULL;
	pDevice = GetDevice(nIndex);		
	if(!pDevice)
		return;

	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_RS_DISPLAY_SAVE_MODE_WAKE_UP;	
	pDevice->AddMsg((CObject *)pEvent);
}
