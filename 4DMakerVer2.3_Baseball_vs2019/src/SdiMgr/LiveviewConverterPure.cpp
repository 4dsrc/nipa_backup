/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		LiveviewConverter.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version 1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */

#include "StdAfx.h"
#include "LiveviewConverterPure.h"
#include "PTP_Base.h"
#include "SRSIndex.h"
#include "SdiDefines.h"
#include "SdiMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CLiveviewConverter, CWinThread)
//------------------------------------------------------------------------------ 
//! @brief		CLiveviewConverter()
//! @date		2010-05-24
//! @author	hongsu.jung
//! @note	 	constructor
//------------------------------------------------------------------------------
CLiveviewConverter::CLiveviewConverter(void)	
{
	m_nDeviceID = -1;
	m_hRecvWnd = NULL;			//-- 2011-7-12 jeansu
	m_bLCDDisplay = TRUE;
	m_bOff = FALSE;
	Init();
}

CLiveviewConverter::CLiveviewConverter(int nDeviceNumber)	
{
	m_nDeviceID = nDeviceNumber;	
	m_hRecvWnd = NULL;			//-- 2011-7-12 jeansu
	m_bLCDDisplay = TRUE;
	m_bOff = FALSE;
	Init();
}

void CLiveviewConverter::Init()
{
	//-- 2013-12-02 hongsu@esmlab.com
	//-- Critical Section 
	InitializeCriticalSection (&m_pCS_LC);	

	m_pFrameData = NULL;
	m_bThreadFlag = TRUE;
	SetSetting(FALSE);	
}

CLiveviewConverter::~CLiveviewConverter(void)
{
	StopThread();
	ExitInstance();

	DeleteCriticalSection (&m_pCS_LC);
}

BOOL CLiveviewConverter::InitInstance() 
{ 
	return TRUE; 
}

int CLiveviewConverter::ExitInstance()				
{
	return CWinThread::ExitInstance();
}

//------------------------------------------------------------------------------ 
//! @brief		StopThread
//! @date		2010-06-29
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveviewConverter::StopThread()
{
	if(!m_bThreadRun)		
		return;

	m_bThreadFlag = FALSE;	
	while(m_bThreadRun)
		Sleep(1);

	RemoveAll();
	//-- 2013-12-01 hongsu.jung
	WaitForSingleObject(m_hThread, INFINITE) ;
}

BOOL CLiveviewConverter::ReleaseImageBuffer()	
{
	EnterCriticalSection (&m_pCS_LC);
	delete[] m_pFrameData->nFrameData;
	delete[] m_pFrameData;
	m_pFrameData = NULL;
	LeaveCriticalSection (&m_pCS_LC);
	return TRUE; 
}


void CLiveviewConverter::SetModel(int nModel) 
{	
	m_nModel = nModel;
	if(m_nModel == SDI_MODEL_NX2000)
		SetSrcSize(NX2000_SRC_WIDTH, NX2000_SRC_HEIGHT);
	else if( m_nModel == SDI_MODEL_NX1 )
		SetSrcSize(NX1_SRC_WIDTH, NX1_SRC_HEIGHT);
	else if(m_nModel == SDI_MODEL_NX3000)
		SetSrcSize(NX2000_SRC_WIDTH, NX2000_SRC_HEIGHT);
	else if( m_nModel == SDI_MODEL_NX500 )
		SetSrcSize(NX500_SRC_WIDTH, NX500_SRC_HEIGHT);
	else if( m_nModel ==  SDI_MODEL_GH5)
		SetSrcSize(GH5_SRC_WIDTH, GH5_SRC_HEIGHT);
	else
		SetSrcSize(SRC_WIDTH,SRC_HEIGHT);
	CreateImageBuffer();	
}

BOOL CLiveviewConverter::CreateImageBuffer()	
{
	if(m_pFrameData)
	{
		ReleaseImageBuffer();
		m_pFrameData = NULL;
	}
	//-- Create Image
	m_pFrameData = new FrameData;
	if(m_nModel == SDI_MODEL_NX2000)
	{
		m_pFrameData->nFrameData = new BYTE[m_szSrc.cx * (m_szSrc.cy-2) * 3];
		m_pFrameData->nHeight = m_szSrc.cy-2;
		m_pFrameData->nWidth = m_szSrc.cx;
	}
	else if( m_nModel == SDI_MODEL_NX1 )
	{
		m_pFrameData->nFrameData = new BYTE[m_szSrc.cx * m_szSrc.cy* 3];
		m_pFrameData->nHeight = m_szSrc.cy;
		m_pFrameData->nWidth = m_szSrc.cx;
	}
	else if(m_nModel == SDI_MODEL_NX3000)
	{
		m_pFrameData->nFrameData = new BYTE[m_szSrc.cx * (m_szSrc.cy-2) * 3];
		m_pFrameData->nHeight = m_szSrc.cy-2;
		m_pFrameData->nWidth = m_szSrc.cx;
	}
	if(m_nModel == SDI_MODEL_NX500)
	{
		m_pFrameData->nFrameData = new BYTE[m_szSrc.cx * m_szSrc.cy* 3];
		m_pFrameData->nHeight = m_szSrc.cy;
		m_pFrameData->nWidth = m_szSrc.cx;
	}
	else if( m_nModel ==  SDI_MODEL_GH5)
	{
		m_pFrameData->nFrameData = new BYTE[m_szSrc.cx * m_szSrc.cy* 3];
		m_pFrameData->nHeight = m_szSrc.cy;
		m_pFrameData->nWidth = m_szSrc.cx;
	}
	else 
	{
		m_pFrameData->nFrameData = new BYTE[m_szSrc.cx * m_szSrc.cy* 3];
		m_pFrameData->nHeight = m_szSrc.cy;
		m_pFrameData->nWidth = m_szSrc.cx;
	}

	return TRUE; 
}




//------------------------------------------------------------------------------ 
//! @brief		Run
//! @date		2010-05-30
//! @author	hongsu.jung
//! @note	 	get message
//------------------------------------------------------------------------------ 
int CLiveviewConverter::Run(void)
{
	RSEvent* pEvent = NULL;
	
	m_bThreadRun = TRUE;
	m_bAutoDelete = FALSE;

	int nAll;

	while(m_bThreadFlag)
	{
		//-- 2013-03-01 honsgu.jung
		//-- For CPU
		Sleep(1);
		nAll = m_arMsg.GetSize();
		while(nAll--)
		{
			pEvent = (RSEvent*)m_arMsg.GetAt(0);			
			if(!pEvent)
			{
				m_arMsg.RemoveAt(0);
				continue;
			}
			
			//-- 2013-12-02 hongsu@esmlab.com
			//-- Critical Section 
			EnterCriticalSection (&m_pCS_LC);
			
			switch(pEvent->message)
			{
			case WM_RS_EVENT_LIVEVIEW_IMAGE:
				//-- Get Buffer
				if(ISSet())
					SendLiveviewMessage((unsigned char*)pEvent->pParam, (RSLiveviewInfo*)pEvent->pDest, pEvent->nParam1, pEvent->nParam2, pEvent->nParam3);
				delete pEvent;
				break;
			case WM_LIVEVIEW_EVENT_SIZE:
				{
					SetSrcSize( ((RSLiveviewInfo*)pEvent->pParam)->imageSizeWidth,		((RSLiveviewInfo*)pEvent->pParam)->imageSizeHeight);
					SetOffsetSize( ((RSLiveviewInfo*)pEvent->pParam)->offsetSizeWidth,	((RSLiveviewInfo*)pEvent->pParam)->offsetSizeHeight);
					RSLiveviewInfo *pInfo = (RSLiveviewInfo*)pEvent->pParam;
					delete pInfo;//(RSLiveviewInfo*)pEvent->pParam;

					//-- SendMessage SrcSize
					RSEvent* pMsgToMain = new RSEvent();
					pMsgToMain->nParam1 = m_szSrc.cx;
					pMsgToMain->nParam2= m_szSrc.cy;					
					::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_RS_SET_CHANGE_LIVEVIEW_SIZE, (LPARAM)pMsgToMain);
				}
				delete pEvent;
				break;
			default:
				TRACE("None Defined Message in Liveview Thread\n");
				break;
			}
			//-- Remote
			
			m_arMsg.RemoveAt(0);
			
			//-- 2013-12-02 hongsu@esmlab.com
			//-- Critical Section 
			LeaveCriticalSection (&m_pCS_LC);
		}
	}

	ReleaseImageBuffer();//- 2013-11-27  thread 종료전 Release
	m_bThreadRun = FALSE;

	return 0;
}

void CLiveviewConverter::RemoveAll()
{
	// [11/27/2013 Administrator]
	// Remove Array
	RSEvent* pEvent = NULL;
	int nAll = m_arMsg.GetSize();
	while(nAll--)
	{
		pEvent = (RSEvent*)m_arMsg.GetAt(nAll);
		if(pEvent)
			delete pEvent;
		m_arMsg.RemoveAt(nAll);
	}
	m_arMsg.RemoveAll();
}

int CLiveviewConverter::AddBuffer( unsigned char* pYUV)
{
	RSEvent* pEvent = new RSEvent();
	pEvent->message = WM_RS_EVENT_LIVEVIEW_IMAGE;	
	pEvent->pParam = (LPARAM)pYUV;
	//TRACE(_T("AddBuffer1\n"));
	if(m_arMsg.Add((CObject*)pEvent) == -1){
		return SDI_ERR_INTERNAL;
	}	
	return SDI_ERR_OK;
}
int CLiveviewConverter::AddBuffer( unsigned char* pYUV, RSLiveviewInfo* pLiveviewInfo, int nType, BOOL bRecord, BOOL bEnd)
{
	RSEvent* pEvent = new RSEvent();
	pEvent->message = WM_RS_EVENT_LIVEVIEW_IMAGE;	
	pEvent->pParam = (LPARAM)pYUV;
	pEvent->pDest = (LPARAM)pLiveviewInfo;
	pEvent->nParam1 = nType;
	pEvent->nParam2 = (UINT)bRecord;
	pEvent->nParam3 = (UINT)bEnd;
	//TRACE(_T("AddBuffer\n"));
	if(m_arMsg.Add((CObject*)pEvent) == -1){
		return SDI_ERR_INTERNAL;
	}	
	return SDI_ERR_OK;
}

int CLiveviewConverter::ChangeSize(RSLiveviewInfo* pLiveviewInfo)
{
	RSEvent* pEvent	= new RSEvent();
	pEvent->message	= WM_LIVEVIEW_EVENT_SIZE;
	pEvent->pParam	= (LPARAM)pLiveviewInfo;
	TRACE(_T("ChangeSize\n"));
	return m_arMsg.Add((CObject*)pEvent);
}

//------------------------------------------------------------------------------ 
//! @brief		GetLiveview(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//! @note	 	get liveview from ptp
//------------------------------------------------------------------------------ 
int  CLiveviewConverter::SendLiveviewMessage(unsigned char* pYUV, RSLiveviewInfo* pLiveviewInfo, int nType, int nRecord, int nEnd)
{
	if(m_nModel == SDI_MODEL_NX2000 )//|| m_nModel == SDI_MODEL_NX1)
	{
		NX2000_ConvertYuv420(NX2000_vga_width, NX2000_vga_height, pYUV);		//-- VGA
	}else if(m_nModel == SDI_MODEL_NX1)
	{		
		if(pYUV != NULL)
			NX1_ConvertYuv420(NX1_vga_width, NX1_vga_height, pYUV, pLiveviewInfo, nType, 0, nEnd);//RGB		
	}
	else if(m_nModel == SDI_MODEL_NX500)
	{
		if(pYUV[0] != 0)
			NX500_ConvertYuv420(NX500_vga_width, NX500_vga_height, pYUV);
	}
	else if(m_nModel == SDI_MODEL_GH5)
	{
		if(pYUV != NULL)
			GH5_YUV422ToBGR(GH5_vga_width, GH5_vga_height, pYUV, pLiveviewInfo);
	}
	else
	{
		if(m_szSrc.cx == qvga_width )		
		{
			
			ConvertYuv422(qvga_width, qvga_height, pYUV);	//-- QVGA
		}
		else									
		{
			ConvertYuv420(vga_width, vga_height, pYUV);		//-- VGA
		}
	}

	//-- Send Message To Liveview Frame
	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_SDI_EVENT_LIVEVIEW_RECEIVED;
	pEvent ->nParam1	= m_nDeviceID;
	pEvent ->pParam	= (LPARAM)m_pFrameData;
	
	//-- 2013-11-21 hongsu@esmlab.com
	//-- For Multi 
	pEvent ->pDest	= (LPARAM)m_pParent;

	if(m_hRecvWnd!=NULL)
	{
		::PostMessage(m_hRecvWnd,
					WM_RS,
					(WPARAM)WM_SDI_EVENT_LIVEVIEW_RECEIVED,
					(LPARAM)pEvent);
	}
	else
	{
		delete pEvent;
		pEvent = NULL;
	}
	return RS_OK;
}

//------------------------------------------------------------------------------ 
//! @brief		ConvertYuv420(unsigned char *pYUV, int height, int width)
//! @date		2010-05-23
//! @author	hongsu.jung
//! @note	 	convert YUV (640 X 424)
//------------------------------------------------------------------------------ 
void CLiveviewConverter::ConvertYuv420(int width, int height, unsigned char* pYUV)
{	
	//width = width + m_szOffset.cx * 2;
	//height = height + m_szOffset.cy * 2;

	//-- Test Code
	/*
	static int n = 0;
	if(height==360){
		CFile file;
		CString strFilepath;
		strFilepath.Format(_T("C:\\test%d.raw"),n++);
		file.Open(strFilepath, CFile::modeWrite |	CFile::shareExclusive | CFile::modeCreate,NULL);
		file.Write(pYUV, height*width*1.5);
		file.Close();	
	}
	*/
	

	unsigned char	y, cb, cr;
	double			r, g, b;
	int					rgb_index		= 0;
	int					Y_oddPos		= 0 ;				// Y0
	int					C_oddPos		= height * width ;	// C0
	int					Y_evenPos	= C_oddPos / 2 ;		// Y1
	int					C_evenPos	= C_oddPos * 5 / 4 ;	// C1

	int Y_index,	Y_Offset;
	int Cb_index,	C_Offset;
	int Cr_index;	

	for ( int i = 0 ; i < height; i++ )
	{
		for ( int j=0; j< width; j++ )
		{	
			//-- Check OffSet
			if(m_szOffset.cx>0 && ( j < m_szOffset.cx || j > width - m_szOffset.cx ))
			{
				m_pFrameData->nFrameData[rgb_index++] = (BYTE)0;
				m_pFrameData->nFrameData[rgb_index++] = (BYTE)0;
				m_pFrameData->nFrameData[rgb_index++] = (BYTE)0;
			}			
			else if(m_szOffset.cy > 0 && ( i < m_szOffset.cy || i > height - m_szOffset.cy ))
			{
				m_pFrameData->nFrameData[rgb_index++] = (BYTE)0;
				m_pFrameData->nFrameData[rgb_index++] = (BYTE)0;
				m_pFrameData->nFrameData[rgb_index++] = (BYTE)0;
			}							
			else
			{
				//-- Get Y Offset
				Y_Offset = i/2 * width  + j;				
				//-- Get CbCr Offset
				if( j%2 == 0 )	C_Offset = i/4 * width  + j ;
				else			C_Offset = i/4 * width  + j - 1;
			
				if( i%2 == 0 )	
				{
					Y_index = Y_oddPos + Y_Offset;				//-- Even
					Cb_index = C_oddPos + C_Offset;
				}
				else
				{
					Y_index = Y_evenPos + Y_Offset;				//-- Odd
					Cb_index = C_evenPos + C_Offset;
				}			
			
				Cr_index = Cb_index +1;

				y	= pYUV[Y_index];
				cb = pYUV[Cb_index];
				cr  = pYUV[Cr_index ];

				r = 1.164 * ( y - 16 ) + 2.018 * ( cb - 128 );			
				g = 1.164 * ( y - 16 ) - 0.813 * ( cr - 128 ) - 0.391 * ( cb - 128 );
				b = 1.164 * ( y - 16 ) + 1.596 * ( cr - 128 );

				m_pFrameData->nFrameData[rgb_index++] = (byte)CLIPING(r);
				m_pFrameData->nFrameData[rgb_index++] = (byte)CLIPING(g);
				m_pFrameData->nFrameData[rgb_index++] = (byte)CLIPING(b);
			}
		}
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		NX2000_ConvertYuv420(unsigned char *pYUV, int height, int width)
//! @date		2010-05-23
//! @author	hongsu.jung
//! @note	 	convert YUV (640 X 424)
//------------------------------------------------------------------------------ 
void CLiveviewConverter::NX2000_ConvertYuv420(int width, int height, unsigned char* pYUV)
{	
	unsigned char	y, cb, cr;
	double			r, g, b;
	unsigned long					rgb_index		= 0;
	int					Y_oddPos		= 0 ;				// Y0
	int					C_oddPos		= height * width ;	// C0
	int					Y_evenPos	= C_oddPos / 2 ;		// Y1
	int					C_evenPos	= C_oddPos * 5 / 4 ;	// C1

	int Y_index = 0, Cb_index = 0, Cr_index = 0;
	Cb_index = C_oddPos;

	for ( int i = 0 ; i < height-2; i++ )
	{
		if(i%2 == 1)
			Cb_index -= width;
		for ( int j=0; j< width; j++ )
		{
			if(j%2 == 0)
			{
				Cb_index = Cb_index + 2;
				Cr_index = Cb_index + 1;
			}

			y  = pYUV[Y_index];
			cb = pYUV[Cb_index];
			cr = pYUV[Cr_index];


			if(j < m_szOffset.cx || j > (width-m_szOffset.cx) )
			{
				r = 0;
				g = 0;
				b = 0;
			}
			else
			{
				r = 1.164 * ( y - 16 ) + 2.018 * ( cb - 128 );			
				g = 1.164 * ( y - 16 ) - 0.813 * ( cr - 128 ) - 0.391 * ( cb - 128 );
				b = 1.164 * ( y - 16 ) + 1.596 * ( cr - 128 );
			}


			Y_index++;
			// RGB를 저장하는 부분
			if( rgb_index < (width * height * 3) )
			{

				m_pFrameData->nFrameData[rgb_index++] = (byte)CLIPING(r);
				m_pFrameData->nFrameData[rgb_index++] = (byte)CLIPING(g);
				m_pFrameData->nFrameData[rgb_index++] = (byte)CLIPING(b);
			}
		}
	}
}

void CLiveviewConverter::NX500_ConvertYuv420(int width, int height, unsigned char* pYUV)
{	
	unsigned char	y, cb, cr;
	double			r, g, b;
	unsigned long					rgb_index		= 0;
	int					Y_oddPos		= 0 ;				// Y0
	int					C_oddPos		= height * width ;	// C0
	int					Y_evenPos	= C_oddPos / 2 ;		// Y1
	int					C_evenPos	= C_oddPos * 5 / 4 ;	// C1

	int Y_index,	Y_Offset;
	int Cb_index,	C_Offset;
	int Cr_index;
	Y_index = 0;
	Cb_index = C_oddPos;

	for ( int i = 0 ; i < height-2; i++ )
	{
		if(i%2 == 1)
			Cb_index -= width;
		for ( int j=0; j< width; j++ )
		{
			if(j%2 == 0)
			{
				Cb_index = Cb_index + 2;
				Cr_index = Cb_index + 1;
			}

			y  = pYUV[Y_index];
			cb = pYUV[Cb_index];
			cr = pYUV[Cr_index];


			if(j < m_szOffset.cx || j > (width-m_szOffset.cx) )
			{
				r = 0;
				g = 0;
				b = 0;
			}
			else
			{
				r = 1.164 * ( y - 16 ) + 2.018 * ( cb - 128 );			
				g = 1.164 * ( y - 16 ) - 0.813 * ( cr - 128 ) - 0.391 * ( cb - 128 );
				b = 1.164 * ( y - 16 ) + 1.596 * ( cr - 128 );
			}


			Y_index++;
			// RGB를 저장하는 부분
			if( rgb_index < (width * height * 3) )
			{

				m_pFrameData->nFrameData[rgb_index++] = (byte)CLIPING(r);
				m_pFrameData->nFrameData[rgb_index++] = (byte)CLIPING(g);
				m_pFrameData->nFrameData[rgb_index++] = (byte)CLIPING(b);
			}			
		}
	}
}

//------------------------------------------------------------------------------ 
//! @brief		ConvertYuv422(unsigned char *pYUV, int height, int width)
//! @date		2010-07-04
//! @author	hongsu.jung
//! @note	 	convert YUV	(320 X 240)
//------------------------------------------------------------------------------ 
void CLiveviewConverter::ConvertYuv422(int width, int height, unsigned char* pYUV)
{	

	int					i, j;					// row, column
	unsigned char	y, cb, cr;
	double			r, g, b;
	int					rgb_index = 0;	

	int C_initPos = height * width;

	int Y_index;
	int Cb_index;
	int Cr_index;

	for ( i = 0 ; i < height ; i++ )
	{
		for ( j=0; j< width; j++ )
		{	
			//-- Check OffSet
			if(m_szOffset.cx>0 && ( j < m_szOffset.cx || j > m_szSrc.cx - m_szOffset.cx ))
			{
				m_pFrameData->nFrameData[rgb_index++] = (BYTE)0;
				m_pFrameData->nFrameData[rgb_index++] = (BYTE)0;
				m_pFrameData->nFrameData[rgb_index++] = (BYTE)0;
			}			
			else if(m_szOffset.cy > 0 && ( i < m_szOffset.cy || i > m_szSrc.cy - m_szOffset.cy ))
			{
				m_pFrameData->nFrameData[rgb_index++] = (BYTE)0;
				m_pFrameData->nFrameData[rgb_index++] = (BYTE)0;
				m_pFrameData->nFrameData[rgb_index++] = (BYTE)0;
			}							
			else
			{
				Y_index = i*width  + j;
				y	= pYUV[Y_index];

				Cb_index = C_initPos + i/2 * width + j - (j%2);
				Cr_index = Cb_index+1;

				cb = pYUV[Cb_index];
				cr	= pYUV[Cr_index ];

				r = 1.164 * ( y - 16 ) + 2.018 * ( cb - 128 );			
				g = 1.164 * ( y - 16 ) - 0.813 * ( cr - 128 ) - 0.391 * ( cb - 128 );
				b = 1.164 * ( y - 16 ) + 1.596 * ( cr - 128 );
			
				// RGB를 저장하는 부분

				m_pFrameData->nFrameData[rgb_index++] = (byte)CLIPING(r);
				m_pFrameData->nFrameData[rgb_index++] = (byte)CLIPING(g);
				m_pFrameData->nFrameData[rgb_index++] = (byte)CLIPING(b);
			}
		}
	}	
}

LiveviewOffset CLiveviewConverter::GetPhotoSizeOffset(int nType, FrameData* pData)
{
	LiveviewOffset offset;
	switch(nType)
	{
	case LIVE_SIZE_EVF:
	case LIVE_SIZE_28:
	case LIVE_SIZE_13_9:
	case LIVE_SIZE_7_1:
	case LIVE_SIZE_3:
		offset.nOffsetX = 0;
		offset.nOffsetY = 0;
		break;
	case LIVE_SIZE_23:
	case LIVE_SIZE_11_9:
	case LIVE_SIZE_6_2:
	case LIVE_SIZE_2_4:
		offset.nOffsetX = 0;
		offset.nOffsetY = 40;
		break;
	case LIVE_SIZE_18_7:
	case LIVE_SIZE_9_5:
	case LIVE_SIZE_4_7:
	case LIVE_SIZE_2:
		offset.nOffsetX = 120;
		offset.nOffsetY = 0;
		break;
	case LIVE_SIZE_RECORD:
		//if(pLiveInfo->imageSizeHeight < 404)
		//{
		//	offset.nOffsetX = 0;
		//	offset.nOffsetY = 50;
		//}
		//else
		{
			offset.nOffsetX = 0;
			offset.nOffsetY = 0;
		}
		break;
	default:
		offset.nOffsetX = 0;
		offset.nOffsetY = 0;
		break;
	}

	return offset;
}
//------------------------------------------------------------------------------ 
//! @brief		NX1_ConvertYuv420(unsigned char *pYUV, int height, int width)
//! @date		2010-05-23
//! @author	hongsu.jung
//! @note	 	convert YUV (640 X 424)
//------------------------------------------------------------------------------ 
int g_nLiveCount = 0;
int g_nPreLive;

void CLiveviewConverter::NX1_ConvertYuv420(int width, int height, unsigned char* pYUV, RSLiveviewInfo* pLiveviewInfo, int nType, int nRecord, int nEnd)
{
	LiveviewOffset offset;
	unsigned char	y, cb, cr;
	double			r, g, b;
	unsigned long					rgb_index		= 0;
	int					Y_oddPos		= 0 ;				// Y0
	int					C_oddPos		= height * width ;	// C0
	int					Y_evenPos	= C_oddPos / 2 ;		// Y1
	int					C_evenPos	= C_oddPos * 5 / 4 ;	// C1

	int Y_index,	Y_Offset;
	int Cb_index,	C_Offset;
	int Cr_index;
	Y_index = 0;
	Cb_index = C_oddPos;

	int nOffsetSize = 0;
	int nGap = 0;
	int nOffset = 0;

	BOOL bOrigin = FALSE;

	if(g_nPreLive != nType)
	{
		g_nPreLive = nType;
		return;
	}	
	m_pFrameData->nHeight = pLiveviewInfo->imageSizeHeight;
	m_pFrameData->nWidth = pLiveviewInfo->imageSizeWidth;
	if(nRecord == 1 && nEnd == 0)
	{
		offset = GetPhotoSizeOffset(LIVE_SIZE_RECORD, m_pFrameData);
	}
	else
		offset = GetPhotoSizeOffset(nType);		

	C_oddPos	= pLiveviewInfo->imageSizeHeight * pLiveviewInfo->imageSizeWidth ;	// C0
	Y_evenPos	= C_oddPos / 2 ;		// Y1
	C_evenPos	= C_oddPos * 5 / 4 ;	// C1
	Cb_index = C_oddPos;
	//nOffsetSize = (NX1_vga_width - pLiveviewInfo->imageSizeWidth)/2;

	for ( int i = 0 ; i < pLiveviewInfo->imageSizeHeight; i++ )
	{
		if(i%2 == 1)
			Cb_index -= pLiveviewInfo->imageSizeWidth;

		for ( int j=0; j< width; j++ )
		{
			if(j%2 == 0)
			{
				Cb_index = Cb_index + 2;
				Cr_index = Cb_index + 1;
			}

			y  = pYUV[Y_index];
			cb = pYUV[Cb_index];
			cr = pYUV[Cr_index];


			if(j < nOffsetSize || j >= (pLiveviewInfo->imageSizeWidth + nOffsetSize)) //-- joonho.kim
			{
				r = 0;
				g = 0;
				b = 0;

				if(j%2 == 0)
				{
					Cb_index = Cb_index - 2;
					Cr_index = Cb_index - 1;
				}
			}
			else
			{
				int nOffsetY; 
				if(nRecord)
					nOffsetY = 480-offset.nOffsetY;
				else
					nOffsetY = pLiveviewInfo->imageSizeHeight-offset.nOffsetY;

				if( i < offset.nOffsetY || i > nOffsetY)
				{
					r = 0;
					g = 0;
					b = 0;
				}
				else if( j < offset.nOffsetX || j >  (pLiveviewInfo->imageSizeWidth-offset.nOffsetX))
				{
					r = 0;
					g = 0;
					b = 0;
				}
				else
				{
					r = 1.164 * ( y - 16 ) + 1.596 * ( cr - 128 );					
					g = 1.164 * ( y - 16 ) - 0.813 * ( cr - 128 ) - 0.391 * ( cb - 128 );
					b = 1.164 * ( y - 16 ) + 2.018 * ( cb - 128 );	
				}
				Y_index++;
			}

			// RGB를 저장하는 부분
			if( rgb_index < width * height * 3 && j < pLiveviewInfo->imageSizeWidth)
			{
				m_pFrameData->nFrameData[rgb_index++]= (byte)CLIPING(b);
				m_pFrameData->nFrameData[rgb_index++]= (byte)CLIPING(g);
				m_pFrameData->nFrameData[rgb_index++]= (byte)CLIPING(r);
			}
		}
	}
}

void CLiveviewConverter::GH5_YUV422ToBGR(int nWidth,int nHeight,BYTE* data, RSLiveviewInfo* pLiveviewInfo)
{
	//This Function for GH5 made by panasonic.
	int _byte_per_line		= pLiveviewInfo->imageFormat;	
	int _active_width_pixel	= pLiveviewInfo->imageSizeWidth;// = nWidth;
	int _active_height_pixel= pLiveviewInfo->imageSizeHeight;// = nHeight;
	int _ydata_offset_byte	= pLiveviewInfo->offsetSizeWidth;
	int _cdata_offset_byte	= pLiveviewInfo->offsetSizeHeight;

	int _heightWithoutOffset = _cdata_offset_byte/_byte_per_line + _active_height_pixel;

	byte* pYCbCrImage	= (byte*)malloc(_active_width_pixel * _active_height_pixel * _heightWithoutOffset * sizeof(byte));
	
	byte* pYImage		= (byte*)malloc(_active_width_pixel * _active_height_pixel * sizeof(byte));
	byte* pCbImage		= (byte*)malloc(_active_width_pixel/2 * _active_height_pixel * sizeof(byte));
	byte* pCrImage		= (byte*)malloc(_active_width_pixel/2 * _active_height_pixel * sizeof(byte));

	byte* pRgbImage		= (byte*)malloc(_active_width_pixel * _active_height_pixel * sizeof(byte) * 3);
	byte* pCbCrImage	= (byte*)malloc(_active_width_pixel * _active_height_pixel * sizeof(byte));

	if( m_pFrameData->nWidth != pLiveviewInfo->imageSizeWidth ||
		m_pFrameData->nHeight!= pLiveviewInfo->imageSizeHeight)
	{
		ReleaseImageBuffer();
		m_pFrameData = NULL;
		m_pFrameData = new FrameData;

		m_pFrameData->nFrameData = new BYTE[pLiveviewInfo->imageSizeWidth * pLiveviewInfo->imageSizeHeight* 3];
		m_pFrameData->nHeight = pLiveviewInfo->imageSizeHeight;
		m_pFrameData->nWidth = pLiveviewInfo->imageSizeWidth;
	}

	if( pYCbCrImage == NULL ||
		pYImage		== NULL ||
		pCbImage	== NULL ||
		pCrImage	== NULL || 
		pRgbImage	== NULL ||
		pCbCrImage	== NULL )
	{
		int	i, j;					// row, column
		for ( i = 0 ; i < _active_height_pixel ; i++ )
		{
			for ( j=0; j< _active_width_pixel; j++ )
			{	
				// RGB를 저장하는 부분
				m_pFrameData->nFrameData[i*_active_width_pixel*3 + j*3+0] = (BYTE)0;
				m_pFrameData->nFrameData[i*_active_width_pixel*3 + j*3+1] = (BYTE)0;
				m_pFrameData->nFrameData[i*_active_width_pixel*3 + j*3+2] = (BYTE)0;
			}
		}

		delete pYCbCrImage;
		delete pYImage;
		delete pCbImage;
		delete pCrImage;
		delete pCbCrImage;
		delete pRgbImage;

		return;
	}

	for(int i = 0; i < _active_width_pixel; i++)
	{
		for(int j = 0; j < _heightWithoutOffset; j++)
		{
			pYCbCrImage[j*_active_width_pixel + i] = data[j*_byte_per_line + i];
			pYCbCrImage[j*_active_width_pixel + i] = data[j*_byte_per_line + i];
			pYCbCrImage[j*_active_width_pixel + i] = data[j*_byte_per_line + i];
		}
	}	

	for(int i = 0; i < _active_width_pixel; i++)
	{
		for(int j = 0; j < _active_height_pixel; j++)
		{
			pYImage[j*_active_width_pixel + i] = pYCbCrImage[j*_active_width_pixel + i];
			pYImage[j*_active_width_pixel + i] = pYCbCrImage[j*_active_width_pixel + i];
			pYImage[j*_active_width_pixel + i] = pYCbCrImage[j*_active_width_pixel + i];

			pCbCrImage[j*_active_width_pixel + i] = pYCbCrImage[(_heightWithoutOffset-_active_height_pixel+j)*_active_width_pixel + i];
			pCbCrImage[j*_active_width_pixel + i] = pYCbCrImage[(_heightWithoutOffset-_active_height_pixel+j)*_active_width_pixel + i];
			pCbCrImage[j*_active_width_pixel + i] = pYCbCrImage[(_heightWithoutOffset-_active_height_pixel+j)*_active_width_pixel + i];
		}
	}

	for(int i = 0; i < _active_width_pixel*_active_height_pixel; i++)
	{
		byte pPixel = pCbCrImage[i]+128;

		if(pPixel >= 255)
			pPixel = 255;
		if(pPixel <= 0)
			pPixel = 0;		

		if(i%2)
			pCrImage[i/2] = pPixel;		
		else		
			pCbImage[i/2] = pPixel;		
	}

	for(int i = 0; i < _active_width_pixel; i++)
	{
		for(int j = 0; j < _active_height_pixel; j++)
		{			
			int temp;

			temp = (1024*pYImage[j*_active_width_pixel + i] - 1	*(pCbImage[j*_active_width_pixel/2 + i/2] - 128) + 1436*(pCrImage[j*_active_width_pixel/2 + i/2] - 128)) >> 10;	
			if( temp < 0 )
				temp = 0;
			if(temp > 255 )
				temp = 255;
			pRgbImage[j*_active_width_pixel*3 + i*3+2] = (byte)temp;

			temp = (1024*pYImage[j*_active_width_pixel + i] - 353 *(pCbImage[j*_active_width_pixel/2 + i/2] - 128) -	731*(pCrImage[j*_active_width_pixel/2 + i/2] - 128)) >> 10;
			if( temp < 0 )
				temp = 0;
			if(temp > 255 )
				temp = 255;
			pRgbImage[j*_active_width_pixel*3 + i*3+1] = (byte)temp;

			temp = (1024*pYImage[j*_active_width_pixel + i] + 1814*(pCbImage[j*_active_width_pixel/2 + i/2] - 128) + 1*(pCrImage[j*_active_width_pixel/2 + i/2] - 128)) >> 10;
			if( temp < 0 )
				temp = 0;
			if(temp > 255 )
				temp = 255;
			pRgbImage[j*_active_width_pixel*3 + i*3+0] = (byte)temp;
		}
	}	

	int	i, j;					// row, column
	for ( i = 0 ; i < _active_height_pixel ; i++ )
	{
		for ( j=0; j< _active_width_pixel; j++ )
		{	
			// RGB를 저장하는 부분
			m_pFrameData->nFrameData[i*_active_width_pixel*3 + j*3+0] = pRgbImage[i*_active_width_pixel*3 + j*3+0];
			m_pFrameData->nFrameData[i*_active_width_pixel*3 + j*3+1] = pRgbImage[i*_active_width_pixel*3 + j*3+1];
			m_pFrameData->nFrameData[i*_active_width_pixel*3 + j*3+2] = pRgbImage[i*_active_width_pixel*3 + j*3+2];
		}
	}

	if(pYCbCrImage)
		delete pYCbCrImage;

	if(pYImage)
		delete pYImage;

	if(pCbImage)
		delete pCbImage;

	if(pCrImage)
		delete pCrImage;

	if(pCbCrImage)
		delete pCbCrImage;

	if(pRgbImage)
		delete pRgbImage;
}
void CLiveviewConverter::GH5_BGR2YUV422(int nWidth,int nHeight,BYTE* data,BYTE* pResult)
{
	int nUVIdx = nWidth * nHeight;

	int i,j;
	int B,G,R;
	double Y,U,V;
	int rIdx,cIdx,yIdx,xIdx;
	for(int i = 0 ; i < nHeight; i++)
	{
		rIdx = nWidth*(i*3);
		for(int j = 0 ; j < nWidth; j++)
		{
			cIdx = rIdx + (j*3);
			xIdx = nWidth*i+j;

			B = data[cIdx];
			G = data[cIdx+1];
			R = data[cIdx+2];

			Y =  (0.299*R)  + (0.587*G) + (0.114*B) + 0;   
			Y = CLIPING(Y);

			U = -(0.169*R)  - (0.331*G) + (0.499*B) + 128; 
			U = CLIPING(U);

			V =  (0.499*R) - (0.418*G) - (0.0813*B) + 128; 
			V = CLIPING(V);

			pResult[xIdx] = Y;

			if(j%2 == 0)
			{
				pResult[xIdx + nUVIdx]   = U;
				pResult[xIdx + nUVIdx+1] = V;
			}
			else
			{
				continue;
				//result.data[xIdx + nUVIdx] = U;				
			}
		}
	}
}