/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        SdiMultiMgr.h
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */

#pragma once
#ifndef _NonOpenCV
#include <cv.h>
#endif

#include <vector>
#include "ptpIndex.h"
#include "SdiDefines.h"

using namespace std;

class CSdiSingleMgr;
/*
struct DevChannel                              // 인덱스와 디바이스 이름 저장을 구조체
{
	int num;
	CString DevUsbName;
};
*/
class AFX_EXT_CLASS CSdiMultiMgr
{
public:
	CSdiMultiMgr(void);
	~CSdiMultiMgr(void);

	int m_nPhotoType;
	BOOL m_bRecordLiveView;
	BOOL m_bEnd;
	BOOL m_bLCDDisplay;
	int m_nFocusMode;

private:
	CObArray m_arPTP;	
	

public:
	HWND	 m_hRecvWnd;							// chlee 이벤트를 보내기 위한 윈도우 핸들 저장

	CSdiSingleMgr* GetDevice(int nIndex);
	CSdiSingleMgr* GetDevice(char*);
	CSdiSingleMgr* AddPTP(CString , CString);
	void RemovePTP(int nIndex);
	void RemovePTPAll();	
	int GetPTPCount()				{ return (int)m_arPTP.GetCount(); }
	CSdiSingleMgr* GetPTP(int nIndex)	{return (CSdiSingleMgr*)m_arPTP.GetAt(nIndex); }
	int GetFocusModeCount()	{return m_nFocusMode;}

public: 
	void SetDeviceLCDDisplay(int nIndex, BOOL bDisplay);
	void GetModeIndex(int nDevIndex);
	int		InitDeviceList(void);

#ifndef _NonOpenCV
	int		ChangeDeviceList(int, IplImage **);
#endif
	BOOL GetDeviceHandle (CStringArray* pArList, GUID guidDeviceInterface, PHANDLE hDeviceHandle);
	BOOL SetConnect(UINT nStatus, UINT nIndex);

	int GetConnectID(int nIndex);
	//-- Control
	BOOL UpdateComboBox(CComboBox* pCmb);
	//-- 2011-06-28 hongsu.jung
	void GetConnectList(CStringArray* pArray);
	//-- 2011-5-30 Lee JungTaek
	void GetCaptureImage(int , CString);
	//-- 2011-06-11 hongsu.jung
	int GetLiveviewInfo(int nIndex);
	//-- 2011-6-27 Lee JungTaek
	CString GetDevUniqueID(int nDevIndex);
	CString GetCurModel(int nDevIndex);
	int GetCurModeIndex(int nDevIndex);
	int GetCurPhysicalMode(int nDevIndex);
	//-- 2011-06-09 hongsu.jung
	int GetType(int nIndex, int nPTPCode);
	//CMiLRe 20141029 White Balnace Detail 종류별로 R/W
	int GetProcSub(int nIndex, int nPTPCode);	
	void GetPropDesc(int nDevIndex, int nPTPCode);
	CString GetCurrentPropValueStr(int nIndex, int nPTPCode);
	int GetCurrentPropValueEnum(int nIndex, int nPTPCode);
	
	//-- 2011-6-15 Lee JungTaek
	//-- Get Prop Value Count
	int GetPropValueCntEnum(int nIndex, int nPTPCode);
	int GetPropValueCntStr(int nIndex, int nPTPCode);

	//-- Get Prop Value Status
	int GetPropValueStatusEnum(int nIndex, int nPTPCode, int nPropValue);
	int GetPropValueStatusStr(int nIndex, int nPTPCode, CString strPorpValue);
	
	//-- 2011-6-21 Lee JungTaek
	//-- Get Enum List
	CUIntArray* GetPropValueListEnum(int nIndex, int nPTPCode);

	//-- 2011-6-10 Lee JungTaek
	//-- Set Property
	void SetPropertyValueEnum(int nIndex, int nPTPCode, int nType, int nPropValue);
	void SetPropertyValueStr(int nIndex, int nPTPCode, int nType, CString strValue);
	
	void SetLiveView(int , int , BOOL );
	void SetLiveViewAll(int , BOOL );
	//-- 2011-06-13 hongsu.jung
	CSize	GetSrcSize(int );
	CSize	GetOffsetSize(int );
	BOOL IsReadyDraw(int );
	void IsSetRecordLiveView(int );

	//-- 2011-5-31 chlee
	void GetCaptureImageComplete(int, TCHAR*, int, int, int);
	void GetRecordMovieComplete(int, TCHAR*, int, int);
	void GetCaptureOrder(int, int , int );
	void GetRecordStatus(int, int);
	void GetCaptureCount(int, int);

	void SetMovieCancle(int nIndex);
	
	//-- 2011-6-24 Lee JungTaek
	int SetFocusPosition(int nIndex, int nType);
	//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
	int SetFocusPositionValue(int nIndex, int nValue);

	//-- 2011-7-14 Lee JungTaek
	void SaveBank(int nIndex, CString strBankName);
	void LoadBank(int nIndex, CString strBankName);
	void LoadBankMulti(int nIndex, int nSelected);

	//-- 2011-7-18 Lee JungTaek
	void FormatDevice(int nIndex);
	void ResetDevice(int nIndex, int nParam = 5);
	//dh0.seo 2014-11-04
	void SensorCleaningDevice(int nIndex);
	void ControlTouchAF(int nIndex, int nX, int nY);
	void TrackingAFStop(int nIndex);
	//-- 2011-09-01 jeansu
	BOOL IsCapturing(int nIndex);
	//CMiLRe 20141113 IntervalCapture Stop 추가
	void IntervalCaptureStop(int nIndex);
	//CMiLRe 20141127 Display Save Mode 에서 Wake up
	void DisplaySaveModeWakeUp(int nIndex);
	CString m_strName;
};
