/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        SdiSingleMgr.h
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */

#pragma once
#include <Afxtempl.h>
#include "SdiCoreWin7.h"
#include "SdiCoreWinXp.h"
#include "SdiMgr.h"
//-- 2011-06-14
//-- time measure
#include <sys/timeb.h>
#include <time.h>

//-- 2011-05-23 hongsu.jung
#include "RSFunc.h"

//-- 2011-07-20 jean
#include "SdiDefines.h"
#include "SetupAPI.h"

#ifndef _NonOpenCV
#include "LiveviewConverter.h"
#else
#include "LiveviewConverterPure.h"
#endif

class CSdiCoreWin7;
class CSdiCoreWinXp;
class CSdiCore;


class AFX_EXT_CLASS CSdiSingleMgr : public CWinThread
{
	DECLARE_DYNCREATE(CSdiSingleMgr)
public:
	CSdiSingleMgr(void);
	CSdiSingleMgr(int, CString, CString);
	CSdiSingleMgr(int, CString, CString, LARGE_INTEGER, LARGE_INTEGER);	//- 2013-10-25 hongsu.jung
	void Init(int nID, CString strVendor, CString strProduct);	
	~CSdiSingleMgr(void);

public:	
	virtual BOOL InitInstance();
	virtual int ExitInstance();	
	virtual int Run(void);	

protected:
	DECLARE_MESSAGE_MAP()
	//-- 2013-12-05 hongsu@esmlab.com
	//-- Critical Section 
	CRITICAL_SECTION	m_pCS_SM;

private:
	BOOL				m_bThreadFlag;
	BOOL				m_bThreadRun;	
	BOOL				m_bConnect;
	

private:
	void*				m_pParent;			//-- 2012-09-12 yongmin.lee : 4DMaker Event Send DSCItem Pointer
	CRSArray			m_arMsg;		
	BOOL				m_bLiveview;		//-- 2011-05-23 hongsu.jung
	BOOL				m_bIsCapturing;		//-- 2011-08-25 jeansu
	
	int					m_nDeviceID;	
	int					m_nModeIndex;
	int					m_nOSVersion;	    //-- 2011-7-7 chlee	 1:Windows XP, 2:Windows Vista , 3: Windows 7
	int					m_DSCStatus;		//-- 2013-04-24 hongsu.jung	| Define at SDIDefine.h
	int					m_nPhysicalMode;	//-- 2011-08-26 jeansu : Physical Dial Position
	int					m_nAFDetect;		//-- 2011-08-01 jeansu : AF Detect/Fail
	int					m_nLiveviewSpeed;
	int					m_nModel;			//-- 2013-11-27 hongsu.jung
	unsigned char *		m_pYUV;
	PTPDeviceInfo *		m_ptPTPDevInfo;		//-- 2011-8-23 Lee JungTaek
	CString				m_strVendor;
	CString				m_strModel;
	CString				m_strProduct;
	CString				m_strUniqueID;
	CString				m_strSavedFile;		//-- 2013-04-25 hongsu.jung | Saved Picture or Movie File Full Path
	CString				m_strLocation;		//-- 2013-05-06 hongsu@esmlab.com | Agent Concept
	CString				m_strImagePath;

	LARGE_INTEGER		m_swFreq;
	LARGE_INTEGER		m_swStart;

	HANDLE				m_hResetThreadHandle;
	CTime				m_DscResetTime;		//-- 2014-04-29 kcd SleepMode 방지 코드

private:
	static unsigned WINAPI DscResetThread(LPVOID param);	

public:
	BOOL				m_bErr;
	CSdiCore*			m_pSdiCore;
	CLiveviewConverter*	m_pLiveviewConverter;	
	int	 m_nMovieSize;
	int	 m_nPhotoType;
	BOOL m_bRecordLiveView;
	BOOL m_bEnd;
	BOOL m_bLCDDisplay;
	BOOL m_bCloseSession;
	int  m_nRecStopTime;

	int m_nPCTime;
	unsigned int m_nDSCTime;

	BOOL m_bInitPreRec;			// GH5 record start prev //  [2018/5/14/ stim]

public:

	void CheckFunction();

	void EnterCS()							{ EnterCriticalSection (&m_pCS_SM);	}
	void LeaveCS()							{ LeaveCriticalSection (&m_pCS_SM);		}

	unsigned char *GetBuffer()				{ return m_pYUV;}
	int		GetMovieSize()					{ return m_nMovieSize; }
	int		GetConnectID()					{ return m_nDeviceID; }
	int		GetCurModeIndex()				{ return m_nModeIndex;}				//-- 2011-7-5 Lee JungTaek	
	int		GetOSVersion();														//-- 2011-7-7 chlee | OS 버전 가져오기
	int		GetAFDetect()					{ return m_nAFDetect;}
	int		GetDSCStatus()					{ return m_DSCStatus; }
	int		GetQualityType();
	int		SendLiveviewMessage();
	int		AddMsg(CObject* pMsg)			{ return m_arMsg.Add(pMsg); }
	int		GetCurPhysicalMode()			{ return m_nPhysicalMode; }			//-- 2011-08-26 jeansu
	int		GetTimeGap(struct _timeb* , struct _timeb* );		
	int		GetModel()						{ return m_nModel;}
	BOOL	ISSet()							{ return (m_pLiveviewConverter==NULL)? FALSE : m_pLiveviewConverter->ISSet();			}
	BOOL	IsCapturing()					{ return m_bIsCapturing; }			//-- 2011-08-25 jeansu
	CString GetDeviceVendor()				{ return m_strVendor; }
	CString GetDeviceModel()				{ return m_strModel; }				//-- 2013-02-12 hongsu.jung | Get Model NX200, NX1000	
	CString GetDeviceProduct()				{ return m_strProduct; }
	CString GetDeviceUniqueID()				{ return m_strUniqueID; }
	CString GetDeviceDSCID()				{ return m_strUniqueID.Right(5); }	//-- 2013-04-22 hongsu.jung | Get Just 5 char 
	CString GetDeviceLocation()				{ return m_strLocation; }			//-- 2013-05-06 hongsu.jung | Get DSC Location
	CString GetSavedFile()					{ return m_strSavedFile; }				//-- 2013-04-25 hongsu.jung | Get Saved File Name
	CString GetDateMilliTime();	
	CSize	GetSrcSize()					{ return (m_pLiveviewConverter==NULL)? CSize(0,0) : m_pLiveviewConverter->GetSrcSize();	}
	CSize	GetOffsetSize()					{ return (m_pLiveviewConverter==NULL)? CSize(0,0) : m_pLiveviewConverter->GetOffsetSize();	}
	void	SetDeviceDSCID(CString strID)	{ m_strUniqueID = strID; }	//-- 2013-04-22 hongsu.jung | Get Just 5 char 

	void	ReorderID()						{ m_nDeviceID--; if(m_pLiveviewConverter!=NULL) m_pLiveviewConverter->ReorderID(); }
	void	StopThread();	
	void	SetAFDetect(int nAFDetect)		{ m_nAFDetect = nAFDetect;}	
	void	SetDSCStatus(int nStatus)		{ m_DSCStatus = nStatus; }			//-- 2013-04-24 hongsu.jung		
	void	SetSavedFile(CString str)		{ m_strSavedFile = str; }			//-- 2013-04-25 hongsu.jung | Set Saved File Name
	void	SetDeviceLocation(CString str)	{ m_strLocation = str; }			//-- 2013-05-06 hongsu.jung | Set DSC Location
	void	SetDeviceLocation();												//-- 2013-05-06 hongsu.jung | Set DSC Location
	void	SetLiveview(BOOL b);
	void	SetLiveviewSpeed(BOOL bMain);
	void	SetCapturing(BOOL bCapture)		{ m_bIsCapturing=bCapture; if(m_pLiveviewConverter) m_pLiveviewConverter->SetSetting(!bCapture); m_bLiveview = !bCapture; }	
	void	SetPollLiveview(BOOL bSelect, BOOL bLiveview)		{	SetLiveviewSpeed(bSelect);	 SetLiveview(bLiveview);}	
	void	RemoveAll(BOOL bCheckConnect = FALSE);
	void	SetParent(void* pParent)		{ m_pParent = pParent; }
	void*	GetParent()						{ return  m_pParent; }

	void	SetInitPreRec(BOOL b)			{ m_bInitPreRec = b; }
	BOOL	GetInitPreRec()					{ return m_bInitPreRec; }			// GH5 record start prev //  [2018/5/14/ stim]

	//--------------------------------------------------------
	//						PTP Function						
	//--------------------------------------------------------
	int PTP_InitSession(CString strDevName);
	int PTP_OpenSession();
	int PTP_CloseSession();
	int PTP_SendCapture_Require(int );
	int PTP_SendCapture_Once(int nModel = 0);
	int PTP_SendCapture_Complete(CString,bool&, int nFileNum, int nClose);							// 촬영 이미지 전송 완료
	//NX3000 20150119 ImageTransfer
	int PTP_SendCapture_Complete(CString,bool&);
	int PTP_SendMovie_Complete(CString strFileName, int nWaitSaveDSC, int nFileNum = 0, int nClose = 0); //-- 2014-09-19 joonho.kim
	int PTP_SendLiveview(RSLiveviewInfo* pLiveviewInfo, int nType  = 0, BOOL bRecord = FALSE, BOOL bEnd = FALSE);
	int PTP_SendMovie_Complete(CString strFileName, int nfilenum, int nClose);		//  동영상 전송 완료
	int PTP_SendLiveview();
	int PTP_LiveviewInfo(RSLiveviewInfo* pLiveviewInfo);					//-- 2011-06-10 hongsu.jung
	int PTP_GetPTPDeviceInfo();												//-- 2011-08-23 Lee JungTaek
	int PTP_CheckEvent(SdiInt32 &nEventExist, SdiUIntArray* pArList=NULL);
	int PTP_GetDevUniqueID();												//-- 2011-06-09 hongsu.jung	
	int PTP_SetDevUniqueID(CString strUUID);
	int PTP_GetDevModeIndex();
	int	PTP_GetEvent(BYTE **pData,int* size);	
	int PTP_SetFocusPosition(FocusPosition * ptFocusPos, int nType);
	int PTP_SetFocusPosition(int nCurrent);
	int PTP_GetFocusPosition(FocusPosition * ptFocusPos);
	void TickSyncStop();
	
	int PTP_FormatDevice();													//-- 2011-7-18 Lee JungTaek
	int PTP_ResetDevice(int nParam = 5);
	//dh0.seo 2014-11-04
	int PTP_SensorCleaningDevice();
	//CMiLRe 20141113 IntervalCapture Stop 추가
	int PTP_IntervalCaptureStop();
	//CMiLRe 20141127 Display Save Mode 에서 Wake up
	int PTP_DisplaySaveModeWakeUp();
	//CMiLRe NX2000 FirmWare Update CMD 추가
	int PTP_FWUpdate();														//-- 2014-9-4 hongsu@esmlab.com
	SdiResult PTP_GetImagePath(CString& strPath);
	//20150128 CMiLRe NX3000
	//CMiLRe 2015129 펌웨어 다운로드 경로 추가
	int PTP_FWDownload(CString strPath = _T("d:/DATANX3000.bin"));	
	
	SdiResult PTP_HalfShutter(int nRelease);											
	SdiResult PTP_SetFocusFrame(int nX, int nY, int nMagnification);											
	SdiResult PTP_GetFocusFrame(BYTE **pData,int *size);											
	int PTP_ExportLog();	
	int PTP_DeviceReboot();	
	int PTP_DevicePowerOff();	
	SdiResult PTP_MovieCancle();	
	SdiResult PTP_InitGH5();
	SdiResult PTP_GetTick(int& tPC, double& tDSC, int& nCheckCnt, int& nCheckTime, int nDSCSyncTime, HWND handle = NULL);							//-- 2013-10-22 yongmin
	SdiResult PTP_SetTick(double nTick, int nIntervalSensorOn, SdiUIntArray* pArList=NULL);									//-- 2013-10-22 yongmin
	SdiResult PTP_HiddenCommand(int nCommand, int nValue);	//-- 2014-01-06 yongmin
	SdiResult PTP_FocusSave();
	SdiResult PTP_GetRecordStatus();										//-- 2014-07-21 joonho.kim
	SdiResult PTP_GetCaptureCount();										//-- 2014-07-21 joonho.kim
	SdiResult PTP_FileTransfer(char* pPath, int nfilenum, int nmdat, int nOffset,int* nFileSize, int nMovieSize);		//-- 2014-07-21 joonho.kim
	SdiResult PTP_MovieTransfer(int nfilenum, int nmdat);					//-- 2014-07-21 joonho.kim
	SdiResult PTP_ImageTransfer(CString strName, int nfilenum, int nClose);
	SdiResult PTP_SetEnLarg(int nLarge);
	SdiResult PTP_SetRecordPause(int nPause);
	SdiResult PTP_SetRecordResume(int nSkip);
	SdiResult PTP_SetLiveView(int nCommand);
	SdiResult PTP_FileReceiveComplete();
	SdiResult PTP_ImageReceiveComplete();
	SdiResult PTP_LiveReceiveComplete();
	SdiResult PTP_ControlTouchAF(int nX, int nY);
	SdiResult PTP_TrackingAFStop();
	SdiResult PTP_SetPause(int nPause);	
	SdiResult PTP_SetResume(int nResume);	
	
	SdiResult PTP_GetDevPropDesc(int nPTPCode, int nAllFlag = 1);								//-- 2011-5-28 Lee JungTaek
	SdiResult PTP_SetDevPropDesc(int nPTPCode, int nType, LPARAM lpValue);	
#ifndef _NonOpenCV
	IplImage* GetLiveviewImage() {return m_pLiveviewConverter->GetLiveviewBuffer();}
#else
	FrameData* GetLiveviewImage() {return m_pLiveviewConverter->GetLiveviewBuffer();}	
#endif
	
	//--------------------------------------------------------
	//						SDI Function						
	//--------------------------------------------------------
	HWND		m_hRecvWnd;
	BOOL		m_bPreLiveview;
	static		SdiResult SdiGetDeviceHandleList(SdiStrArray* pArList);			//-- 2011-7-18 jeansu
	SdiVoid		SdiSetReceiveHandler(HWND hRecvWnd);							//-- 2011-7-12 jeansu
	SdiResult	SdiInitSession(SdiChar *strDevName);
	SdiResult	SdiCloseSession();		
	SdiResult	SdiAddMsg(SdiMessage* pMsg);	
	SdiResult	GetProductInfo(SdiString* strModel, SdiString* strID);			//-- 2013-02-06 hongsu.jung	| Get Product Information	

	//--------------------------------------------------------
	//	2013-12-01
	//	Checking
	//--------------------------------------------------------
	BOOL		m_bStop;
	BOOL		CheckGetFile();

	void WriteLog(CString strLog);

	int GetDevinfoType(int nCode);
	void SetDevinfoCurrentEnum(int nCode, int nValue);
	bool GetDevinfoNextEnum(int nCode, int& nValue);
	bool GetDevinfoPrevEnum(int nCode, int& nValue);
	bool GetDevinfoNextStr(int nCode, CString& str);
	bool GetDevinfoPrevStr(int nCode, CString& str);

	void StopFileTransfer();
	void StartFileTransfer();
};
