////////////////////////////////////////////////////////////////////////////////
//
//	MainFrmDSC.cpp : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-23
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "4DMaker.h"
#include "MainFrm.h"
#include "ESMUtil.h"
#include "ESMBackupDlg.h"
#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-04-27
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @revision		
//------------------------------------------------------------------------------
void CMainFrame::LoadRecordProfileInit(BOOL bBackup)
{
	//-- 2014-07-15 hongsu@esmlab.com
	//-- Change N on Execute Mode 
	m_wndDSCViewer.SetExecuteMovieMode(FALSE);

	//-- 2014-07-15 hongsu@esmlab.com
	//-- Get Record Path

	CString strRecordFolder;

	//-- Record Mode (Movie Files)
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strRecordFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE));
	//-- Capture Mode (Picture Files)
	else
		strRecordFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE));

	//jhhan 181023
	if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING && ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		CString strHome = ESMGetPath(ESM_PATH_FILESERVER);
		if(strRecordFolder.Find(_T("F:")) != -1)
		{
			strHome.Replace(_T("4DMaker"), _T(""));
			strRecordFolder.Replace(_T("F:\\"),strHome);
		}
	}
	
	
	//-- 2014-07-14 hongsu@esmlab.com
	//--  Exception for CFileDialog
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	CFileDialog dlg(TRUE, NULL, NULL, OFN_NOCHANGEDIR | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,_T("Record Files (*.4dm)|*.4dm|"), NULL, 0, bVistaStyle );  
	//strRecordFolder.Replace(_T("\\\\"), _T("\\"));	
	dlg.m_ofn.lpstrInitialDir = strRecordFolder;
	
	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();
		//-- 2013-09-04 hongsu@esmlab.com
		//-- Load Record Profile
		LoadRecordProfile(strFileName);
		
		//-- Set Windows Text
		CString strText;
		strText.LoadString(IDR_MAINFRAME);
		CString strTitle = strText + _T(" - ") + strFileName;
		SetWindowText(strTitle);
		
		//Save Adjust Movie Path
		int nPos = 0;
		CString strToken;
		CString strDate;
		while((strToken = strFileName.Tokenize(_T("\\"),nPos)) != "")
		{
			strDate = strToken;
		}
		strDate.Replace(_T(".4dm"), _T(""));
		ESMSetAdjustPath(strDate);

		//Load Select Point
		ESMLoadProfileSelectPoint(strFileName);
		
		//2016-08-02
		if(bBackup)
		{
			//jhhan
			ESMSet4DMPath(strFileName);

			CESMFileOperation fo;

			if( fo.CheckPath(ESMGetManageMent(ESM_MANAGEMENT_SAVE_PATH)) != PATH_IS_FOLDER)
			{
				AfxMessageBox(_T("DESTINATION FOLDER ["+ESMGetManageMent(ESM_MANAGEMENT_SAVE_PATH)+"]"+" NOT FOUND!!"));
				return;
			}
			BackupRecordFile();
		}

		//wgkim@esmlab.com
		LoadTemplateSquaresProfile();
	}
}

//wgkim@esmlab.com
void CMainFrame::LoadTemplateSquaresProfile()
{
	CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;

	if(arDSCList.GetSize() < 1)
		return;

	m_pTemplateMgr->DscClear();
	for( int i =0 ;i < arDSCList.GetSize(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		m_pTemplateMgr->AddDscInfo(pItem->GetDeviceDSCID(), pItem->m_strInfo[DSC_INFO_LOCATION]);

		m_pTemplateMgr->SetMovieSIze(pItem->m_nWidth, pItem->m_nHeight);
		if(pItem->m_nWidth == 0 || pItem->m_nHeight)
			m_pTemplateMgr->SetMovieSIze(3840, 2160);
	}
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	m_pTemplateMgr->LoadSquarePointData(strFileName);

	int nValidKZone = 0;
	if(ESMGetValue(ESM_VALUE_AUTOADJUST_KZONE))
	{
		nValidKZone= m_pAutoAdjustMgr->LoadSquarePointDataForKZone(strFileName);
	}

	pMainWnd->m_wndTimeLineEditor.SetTemplateMgr(m_pTemplateMgr);
	pMainWnd->m_wndFrame.m_pDlgFrameList->SetTemplateMgr(m_pTemplateMgr);
//	pMainWnd->m_wndFrameEx.m_pRemoteCtrlDlg->SetTemplateMgr(m_pTemplateMgr);

	//wgkim 181002
	if(ESMGetValue(ESM_VALUE_AUTOADJUST_KZONE))
	{
		if(nValidKZone == 1)
		{
			if(m_pAutoAdjustMgr->GetDscCount() > 0)
			{
				KZoneDataInfo* pKZoneDataInfo = m_pAutoAdjustMgr->GetKZonePoint();

				//wgkim 191213
				if (pKZoneDataInfo->bIsEnabled != 0)
				{
					ESMEvent* pSendNetKZoneMsg = new ESMEvent();
					pSendNetKZoneMsg->message = WM_ESM_NET_KZONE_POINTS;
					pSendNetKZoneMsg->nParam1 = m_pAutoAdjustMgr->GetDscCount();
					pSendNetKZoneMsg->pParam = (LPARAM)pKZoneDataInfo;
					::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pSendNetKZoneMsg);

					ESMEvent* pSendLibKZoneMsg = new ESMEvent();
					pSendLibKZoneMsg->message = WM_ESM_MOVIE_SET_KZONE_POINTS;
					pSendLibKZoneMsg->nParam1 = m_pAutoAdjustMgr->GetDscCount();
					pSendLibKZoneMsg->pParam = (LPARAM)pKZoneDataInfo;
					::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pSendLibKZoneMsg);
				}
			}
		}
		else if(nValidKZone == 0)
		{
			ESMLog(nValidKZone, _T("[K-Zone]Do not Find \"TemplateTemp.pta\""));
		}
		else
		{
			ESMLog(nValidKZone, _T("[K-Zone]Unknown Error"));
		}
	}
}

void CMainFrame::BackupRecordFile()
{
	CESMBackupDlg *pDlg = new CESMBackupDlg();
	CString strPath;
	pDlg->Create(this);
	pDlg->ShowWindow(SW_SHOW);
	strPath = ESMGetAdjustPath();

	//jhhan
	CString str4DM;
	str4DM = ESMGet4DMPath();
	pDlg->Backup4DMFile(str4DM);

	pDlg->BackupRecordFile(strPath);	
}

void CMainFrame::OnLoadRecordProfile()
{
	(ESMSetValue(ESM_VALUE_LOADFILE_DIRECT, FALSE));
	LoadRecordProfileInit();
	
	//jhhan 16-09-06
	ESMSetLoadRecord(TRUE);
}

void CMainFrame::OnLoadRecordProfileDirect()
{
	(ESMSetValue(ESM_VALUE_LOADFILE_DIRECT, TRUE));
	LoadRecordProfileInit();
	
	//jhhan 16-09-06
	ESMSetLoadRecord(TRUE);
}
void CMainFrame::OnLoadRecordProfilenBackup()
{
	(ESMSetValue(ESM_VALUE_LOADFILE_DIRECT, FALSE));
	LoadRecordProfileInit(TRUE);
	
	//jhhan 16-09-06
	ESMSetLoadRecord(TRUE);
}

void CMainFrame::OnSaveRecordProfile()
{
	CString strRecordFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strRecordFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE));
	else
		strRecordFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE));

	CString szFilter = _T("Record File (*.4dm)|*.4dm|");
	CString strTitle = _T("Record Files Save");

	//-- File Dialog Open
	// CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter);

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	CFileDialog dlg(FALSE, NULL, NULL, OFN_NOCHANGEDIR | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,szFilter, NULL, 0, bVistaStyle );  
	//strRecordFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = strRecordFolder;	

	//-- 2013-10-02 hongsu@esmlab.com
	//-- Default Save Name
	CTime time = CTime::GetCurrentTime();
	CString strFile;
	strFile.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());

	int length = 1024;
	LPWSTR pwsz = strFile.GetBuffer(length);
	dlg.m_ofn.lpstrFile = pwsz;
	strFile.ReleaseBuffer();

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetFileTitle();		
		//-- 2013-09-04 hongsu@esmlab.com
		SaveRecordProfile(strFileName);
	}
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-10-14
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CMainFrame::OnLoadAdjustProfile()
{
	CString strAdjustFolder;

	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_CONF));
	else
		strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_CONF));

	//jhhan 181023
	if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING && ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		CString strHome = ESMGetPath(ESM_PATH_FILESERVER);
		if(strAdjustFolder.Find(_T("F:")) != -1)
		{
			strHome.Replace(_T("4DMaker"), _T(""));
			strAdjustFolder.Replace(_T("F:\\"),strHome);
		}
	}

	CString szFilter = _T("Adjust Files (*.adj)|*.adj|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		//-- 2013-09-04 hongsu@esmlab.com
		LoadAdjustProfile(strFileName);
		CString strTitle;
		GetWindowText(strTitle);
		strTitle = strTitle.Right( strTitle.GetLength() - strTitle.ReverseFind('\\') - 1);
		strTitle = strTitle.Left(strTitle.GetLength() - 4);
		SaveRecordProfile(strTitle);

		/*ESMEvent* pMsg = new ESMEvent;		
		pMsg->message = WM_ESM_MOVIE_SET_ADJUSTINFO;
		pMsg->nParam1 = m_wndDSCViewer.GetMarginX();
		pMsg->nParam2 = m_wndDSCViewer.GetMarginY();
		::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);*/
	}
}

void CMainFrame::OnSaveAdjustProfile()
{
	CString strAdjustFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_CONF));
	else
		strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_CONF));

	CString szFilter = _T("Adjust File (*.adj)|*.adj|");
	CString strTitle = _T("Adjust Files Save");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	//-- 2013-10-02 hongsu@esmlab.com
	//-- Default Save Name
	CTime time = CTime::GetCurrentTime();
	CString strFile;
	strFile.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());

	int length = 1024;
	LPWSTR pwsz = strFile.GetBuffer(length);
	dlg.m_ofn.lpstrFile = pwsz;
	strFile.ReleaseBuffer();

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();		
		//-- 2013-09-04 hongsu@esmlab.com
		SaveAdjustProfile(strFileName);
	}
}

void CMainFrame::OnSaveSyncTestResult()
{
	CString szFilter = _T("Sync Test File (*.csv)|*.csv|");
	CString strTitle = _T("Sync Test File Save");


	const BOOL bVistaStyle = TRUE;  

	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = ESMGetPath(ESM_PATH_LOG_DATE);	

	//-- Default Save Name
	CTime time = CTime::GetCurrentTime();
	CString strFile;
	strFile.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());

	int length = 1024;
	LPWSTR pwsz = strFile.GetBuffer(length);
	dlg.m_ofn.lpstrFile = pwsz;
	strFile.ReleaseBuffer();

	if(m_bSyncTestAuto)
	{
		CString strPath;
		strPath.Format(_T("%s\\%s.csv"), ESMGetPath(ESM_PATH_LOG_DATE), strFile);
		SaveSyncTestFile(strPath);
	}
	else
	{
		if( dlg.DoModal() == IDOK )
		{
			CString strFileName;
			strFileName = dlg.GetPathName();		
			SaveSyncTestFile(strFileName);
		}
	}
}

void CMainFrame::OnSaveAdjustProfile2nd(void* pAdj)
{
	CString strAdjustFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_CONF));
	else
		strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_CONF));

	CString szFilter = _T("Adjust File (*.adj)|*.adj|");
	CString strTitle = _T("Adjust Files Save");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	//-- 2013-10-02 hongsu@esmlab.com
	//-- Default Save Name
	CTime time = CTime::GetCurrentTime();
	CString strFile;
	strFile.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());

	int length = 1024;
	LPWSTR pwsz = strFile.GetBuffer(length);
	dlg.m_ofn.lpstrFile = pwsz;
	strFile.ReleaseBuffer();

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();		
		//-- 2013-09-04 hongsu@esmlab.com
		SaveAdjustProfile(strFileName, pAdj);
	}
}

void CMainFrame::InitTitleBar()
{
	CString strText;
	strText.LoadString(IDR_MAINFRAME);
	SetWindowText(strText);
}