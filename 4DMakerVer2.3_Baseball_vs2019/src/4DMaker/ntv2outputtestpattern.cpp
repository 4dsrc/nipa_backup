/**
	@file		ntv2outputtestpattern.cpp
	@brief		Implementation of NTV2OutputTestPattern demonstration class.
	@copyright	Copyright (C) 2013-2015 AJA Video Systems, Inc.  All rights reserved.
**/
  

//	Includes
#include"stdafx.h"
#include "ntv2outputtestpattern.h"

#include "ajastuff/common/types.h"
#include "ajastuff/common/options_popt.h"
#include "ntv2card.h"
#include <signal.h>
#include <iostream>
#include <iomanip>
#include "ajastuff/system/systemtime.h"

#include "ESMFunc.h"


const uint32_t	kAppSignature	(AJA_FOURCC ('T','e','s','t'));


NTV2OutputTestPattern::NTV2OutputTestPattern (const UWord		boardNumber,
	const NTV2Channel	channel)
	:	mDeviceID		(DEVICE_ID_NOTFOUND),
	mDeviceIndex	(boardNumber),
	mChannel		(channel),
	mVideoFormat	(NTV2_FORMAT_UNKNOWN),
	mPixelFormat	(NTV2_FBF_8BIT_YCBCR),
	mVancEnabled	(false),
	mWideVanc		(false),

	PCFreq(0.0),
 CounterStart(0)
{
	m_bFinish = TRUE;
}	//	constructor

NTV2OutputTestPattern::~NTV2OutputTestPattern ()
{
	m_bFinish = FALSE;
	//	Restore the prior service level...
	mDevice.SetEveryFrameServices (mSavedTaskMode);													//	Restore prior service level
	mDevice.ReleaseStreamForApplication (kAppSignature, static_cast <uint32_t> (AJAProcess::GetPid ()));	//	Release the device

}	//	destructor

AJAStatus NTV2OutputTestPattern::Init ()
{
	//	Open the board...
	if (!CNTV2DeviceScanner::GetDeviceAtIndex (mDeviceIndex, mDevice))
		return AJA_STATUS_OPEN;
#if 1
	if (!mDevice.AcquireStreamForApplication (kAppSignature, static_cast <uint32_t> (AJAProcess::GetPid ())))
	{
		cerr << "## ERROR:  Unable to acquire device because another app owns it" << endl;
		//RunMyApplicationUsingDevice(mDevice);
		
		mDevice.ReleaseStreamForApplication(kAppSignature,static_cast <uint32_t> (AJAProcess::GetPid ()));
		return AJA_STATUS_BUSY;		//	Some other app is using the device
	}
#endif
	mDevice.GetEveryFrameServices (&mSavedTaskMode);	//	Save the current state before changing it
	mDevice.SetEveryFrameServices (NTV2_OEM_TASKS);		//	Since this is an OEM demo, use the OEM service level

	mDeviceID = mDevice.GetDeviceID ();		//	Keep this handy since it will be used frequently...

	//	At this point, the device can be used without fear of messing up another application.
	//	The only way to relinquish the device to another application is to manually set the
	//	task mode back to "Standard Tasks" using the 'cables' utility (in the "Control" tab).

	//  Get the board's current video format and use it later for writing the test pattern...
	if (!mDevice.GetVideoFormat (&mVideoFormat, mChannel))
		return AJA_STATUS_FAIL;

	//  Read the current VANC settings so an NTV2FormatDescriptor can be constructed...
	if (!mDevice.GetEnableVANCData (&mVancEnabled, &mWideVanc, mChannel))
		return AJA_STATUS_FAIL;

	return AJA_STATUS_SUCCESS;

}	//	Init

AJAStatus NTV2OutputTestPattern::SetUpVideo (void)
{
	//	This is a "playback" application, so set the board reference to free run...
	if (!mDevice.SetReferenceSource (NTV2_REFERENCE_FREERUN, false))
		return AJA_STATUS_FAIL;

	//	Set the video format for the channel's Frame Store to 8-bit YCbCr...
	if (!mDevice.SetFrameBufferFormat (mChannel, mPixelFormat))
		return AJA_STATUS_FAIL;

	//	Enable the Frame Buffer, just in case it's not currently enabled...
	mDevice.EnableChannel (mChannel);

	//	Set the channel mode to "playout" (not capture)...
	if (!mDevice.SetMode (mChannel, NTV2_MODE_DISPLAY))
		return AJA_STATUS_FAIL;

	return AJA_STATUS_SUCCESS;

}	//	SetUpVideo

void NTV2OutputTestPattern::RouteOutputSignal (void)
{
	const bool			isRGB			(IsRGBFormat (mPixelFormat));
	const NTV2Standard	videoStandard	(::GetNTV2StandardFromVideoFormat (mVideoFormat));
	CNTV2SignalRouter	router;
	ULWord				outputXpt;

	switch (mChannel)
	{
		default:
		case NTV2_CHANNEL1:		outputXpt = isRGB ? NTV2_XptFrameBuffer1RGB : NTV2_XptFrameBuffer1YUV;	break;
		case NTV2_CHANNEL2:		outputXpt = isRGB ? NTV2_XptFrameBuffer2RGB : NTV2_XptFrameBuffer2YUV;	break;
		case NTV2_CHANNEL3:		outputXpt = isRGB ? NTV2_XptFrameBuffer3RGB : NTV2_XptFrameBuffer3YUV;	break;
		case NTV2_CHANNEL4:		outputXpt = isRGB ? NTV2_XptFrameBuffer4RGB : NTV2_XptFrameBuffer4YUV;	break;
		case NTV2_CHANNEL5:		outputXpt = isRGB ? NTV2_XptFrameBuffer5RGB : NTV2_XptFrameBuffer5YUV;	break;
		case NTV2_CHANNEL6:		outputXpt = isRGB ? NTV2_XptFrameBuffer6RGB : NTV2_XptFrameBuffer6YUV;	break;
		case NTV2_CHANNEL7:		outputXpt = isRGB ? NTV2_XptFrameBuffer7RGB : NTV2_XptFrameBuffer7YUV;	break;
		case NTV2_CHANNEL8:		outputXpt = isRGB ? NTV2_XptFrameBuffer8RGB : NTV2_XptFrameBuffer8YUV;	break;
	}

	if (isRGB)
	{
		//	Connect the frame buffer to the desired SDI output PLUS every non-SDI output.
		//	RGB must go through a Color Space Converter to get YUV output...
		router.addWithValue (::GetCSC2VidInputSelectEntry (), outputXpt);
		if (mChannel == NTV2_CHANNEL1)
		{
			router.addWithValue (::GetSDIOut1InputSelectEntry (), NTV2_XptCSC1VidYUV);
		}

		if ((mChannel == NTV2_CHANNEL2)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut2) || ::NTV2DeviceCanDoWidget (mDeviceID, NTV2_WgtSDIOut2)))
			{
				router.addWithValue (::GetSDIOut2InputSelectEntry (), NTV2_XptCSC2VidYUV);
			}

		if ((mChannel == NTV2_CHANNEL3)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut3) || ::NTV2DeviceCanDoWidget (mDeviceID, NTV2_WgtSDIOut3)))
			{
				router.addWithValue (::GetSDIOut3InputSelectEntry (), NTV2_XptCSC3VidYUV);
			}

		if ((mChannel == NTV2_CHANNEL4)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut4) || ::NTV2DeviceCanDoWidget (mDeviceID, NTV2_WgtSDIOut4)))
			{
				router.addWithValue (::GetSDIOut4InputSelectEntry (), NTV2_XptCSC4VidYUV);
			}
		if ((mChannel == NTV2_CHANNEL5)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut5)))
			{
				router.addWithValue (::GetSDIOut5InputSelectEntry (), NTV2_XptCSC5VidYUV);
			}
		if ((mChannel == NTV2_CHANNEL6)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut6)))
			{
				router.addWithValue (::GetSDIOut6InputSelectEntry (), NTV2_XptCSC6VidYUV);
			}
		if ((mChannel == NTV2_CHANNEL7)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut7)))
			{
				router.addWithValue (::GetSDIOut7InputSelectEntry (), NTV2_XptCSC7VidYUV);
			}
		if ((mChannel == NTV2_CHANNEL8)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut8)))
			{
				router.addWithValue (::GetSDIOut8InputSelectEntry (), NTV2_XptCSC8VidYUV);
			}

		if (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_WgtHDMIOut1))
			router.addWithValue (::GetHDMIOutInputSelectEntry (), NTV2_XptCSC2VidYUV);
		if (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_WgtAnalogOut1))
			router.addWithValue (::GetAnalogOutInputSelectEntry (), NTV2_XptCSC2VidYUV);
	}	//	if RGB frame buffer format
	else
	{
		//	Connect the frame buffer to the desired SDI output PLUS every non-SDI output.
		//	YUV needs no color space conversion...
		if (mChannel == NTV2_CHANNEL1)
		{
			router.addWithValue (::GetSDIOut1InputSelectEntry (), outputXpt);
		}

		if ((mChannel == NTV2_CHANNEL2)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut2) || ::NTV2DeviceCanDoWidget (mDeviceID, NTV2_WgtSDIOut2)))
			{
				router.addWithValue (::GetSDIOut2InputSelectEntry (), outputXpt);
			}

		if ((mChannel == NTV2_CHANNEL3)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut3) || ::NTV2DeviceCanDoWidget (mDeviceID, NTV2_WgtSDIOut3)))
			{
				router.addWithValue (::GetSDIOut3InputSelectEntry (), outputXpt);
			}

		if ((mChannel == NTV2_CHANNEL4)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut4) || ::NTV2DeviceCanDoWidget (mDeviceID, NTV2_WgtSDIOut4)))
			{
				router.addWithValue (::GetSDIOut4InputSelectEntry (), outputXpt);
			}

		if ((mChannel == NTV2_CHANNEL5)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut5)))
			{
				router.addWithValue (::GetSDIOut5InputSelectEntry (), outputXpt);
			}

		if ((mChannel == NTV2_CHANNEL6)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut6)))
			{
				router.addWithValue (::GetSDIOut6InputSelectEntry (), outputXpt);
			}

		if ((mChannel == NTV2_CHANNEL7)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut7)))
			{
				router.addWithValue (::GetSDIOut7InputSelectEntry (), outputXpt);
			}

		if ((mChannel == NTV2_CHANNEL8)
			&& (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_Wgt3GSDIOut8)))
			{
				router.addWithValue (::GetSDIOut8InputSelectEntry (), outputXpt);
			}

		if (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_WgtHDMIOut1))
			router.addWithValue (::GetHDMIOutInputSelectEntry (), outputXpt);
		if (::NTV2DeviceCanDoWidget (mDeviceID, NTV2_WgtAnalogOut1))
			router.addWithValue (::GetAnalogOutInputSelectEntry (), outputXpt);
	}	//	else YUV frame buffer format

	mDevice.SetSDIOutputStandard (mChannel, videoStandard);

	mDevice.ApplySignalRoute (router, true);	//	"true" means "replace current signal routing" on the device

	//	Enable SDI output from the channel being used, but only if the device supports bi-directional SDI...
	if (::NTV2DeviceHasBiDirectionalSDI (mDeviceID))
		mDevice.SetSDITransmitEnable (mChannel, true);

}	//	RouteOutputSignal

int NTV2OutputTestPattern::CuttingValue(int value)
{
	if(value>255) value = 255;
	if(value < 0)    value = 0;

	return value;
}

int NTV2OutputTestPattern::BGR2YUV422(cv::Mat BGR,cv::Mat YUV,int nHeight,int nWidth)
{
	int i,j;
	int B,G,R;
	int Y,U,V;
	int rIdx,cIdx,yIdx,xIdx;

	int start = GetTickCount();
	for(i=0;i<nHeight;i++)
	{
		rIdx = nWidth*(i*3);
		yIdx = (nWidth*2)*i;
		for(j=0;j<nWidth;j++)
		{
			cIdx = rIdx + (j*3);
			xIdx = yIdx +j*2;

			B = BGR.data[cIdx];
			G = BGR.data[cIdx+1];
			R = BGR.data[cIdx+2];

			Y = (0.257*R) + (0.504*G) + (0.098*B) +16;  CuttingValue(Y);
			U = -(0.148*R) - (0.291*G) + (0.439*B) + 128; CuttingValue(U);
			V = (0.439*R ) - (0.368*G) - (0.071*B) + 128; CuttingValue(V);

			if(j%2 == 0)
			{
				YUV.data[xIdx] = U;
				YUV.data[xIdx+1] = Y;
			}
			else
			{
				YUV.data[xIdx] = V;
				YUV.data[xIdx+1] = Y;
			}
		}
	}

	int end = GetTickCount();

	//return Image Processing Time
	return (end-start);
}

AJAStatus NTV2OutputTestPattern::EmitPattern (const UWord testPatternIndex,CString strPath)
{
//	ESMLog(5,_T("Play Start!"));
	AJAStatus	status	(AJA_STATUS_SUCCESS);

	//  Set up the desired video configuration...
	status = SetUpVideo ();

	//  Connect the frame buffer to the video output...
	if (AJA_SUCCESS (status))
		RouteOutputSignal ();

	//	Get information about current video format...
	NTV2FormatDescriptor	fd	(GetFormatDescriptor (mVideoFormat,
		mPixelFormat,
		mVancEnabled,
		mWideVanc)); 

	////	Write the requested test pattern into host buffer...
	//AJATestPatternGen		testPatternGen;
	//AJATestPatternBuffer	testPatternBuffer;
	//testPatternGen.DrawTestPattern ((AJATestPatternSelect) testPatternIndex,
	//	fd.numPixels,
	//	fd.numLines,
	//	GetAJAPixelFormat (mPixelFormat),
	//	testPatternBuffer);

	////	Find out which frame is currently being output from the frame store...
	//uint32_t	currentOutputFrame	(0);
	//if (AJA_SUCCESS (status))
	//	if (!mDevice.GetOutputFrame (mChannel, &currentOutputFrame))
	//		status = AJA_STATUS_FAIL;

	////	Now simply DMA the test pattern buffer to the currentOutputFrame...
	//if (AJA_SUCCESS (status))
	//	if (!mDevice.DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE,
	//		currentOutputFrame,
	//		reinterpret_cast <uint32_t *> (&testPatternBuffer[0]),
	//		(uint32_t)testPatternBuffer.size ()))
	//		status = AJA_STATUS_FAIL;
	//AJATestPatternBuffer	testPatternBuffer;
	//nHeight = fd.numLines;
	//nWidth  = fd.numPixels;
	nPitch   = nHeight * nWidth * 2;
	int nTime    = 0;

	cv::Mat YUV422(nHeight,nWidth*2,CV_8UC1);
	int nTotal = 0;

	uint32_t	currentOutputFrame	(0);
	vector<uint8_t> pTest;
	pTest.resize(nPitch);

	if (AJA_SUCCESS (status))
		if (!mDevice.GetOutputFrame (mChannel, &currentOutputFrame))
		{
			status = AJA_STATUS_FAIL;
		}

		for(int i = 0 ; i < pPlayArrInfo.size(); i ++)
		{
			YUV422= pPlayArrInfo.at(i);
			if(YUV422.empty()) break;

			memcpy(pTest.data(),YUV422.data,nPitch);

			if (AJA_SUCCESS (status))
				if (!mDevice.DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE,
					currentOutputFrame,
					reinterpret_cast <uint32_t *> (&pTest[0]),
					(uint32_t)pTest.size ()))
				{
					status = AJA_STATUS_FAIL;
					break;
				}
		}
	ESMLog(5,_T("Play Finish!"));


	/*ThreadDisplayData *pDisplayData = new ThreadDisplayData;
	pDisplayData->pPlayArrInfo = pFrameData;
	pDisplayData->nTotal			  = nTotal;
	pDisplayData->nPitch          = nPitch;
	pDisplayData->mChannel     = &mChannel;
	pDisplayData->status			  = AJA_STATUS_SUCCESS;
	pDisplayData->mDevice       = &mDevice;
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,DisplayFrame,(void*)pDisplayData,0,NULL);
	CloseHandle(hSyncTime);

	for(int i = 0 ; i < nTotal ; i++)
	{
	DisplayFrameData pData = pPlayArrInfo->at(i);

	while(1)
	{
	if(pData.bComplete)
	{
	delete[] pData.YUV422;
	break;
	}
	}
	}
	return status;*/

	return status;
}	//	EmitPattern

AJAStatus NTV2OutputTestPattern::SetOperation ()
{
//	ESMLog(5,_T("Play Start!"));
	AJAStatus	status	(AJA_STATUS_SUCCESS);

	//  Set up the desired video configuration...
	status = SetUpVideo ();

	//  Connect the frame buffer to the video output...
	if (AJA_SUCCESS (status))
		RouteOutputSignal ();

	//	Get information about current video format...
	NTV2FormatDescriptor	fd	(GetFormatDescriptor (mVideoFormat,
		mPixelFormat,
		mVancEnabled,
		mWideVanc)); 
	nHeight = fd.numLines;
	nWidth  = fd.numPixels;
	nPitch   = nHeight * nWidth * 2;


	return status;
}

AJAStatus NTV2OutputTestPattern::EmitPattern(vector<DisplayMovieData>*pArrDisplayData,int nStartIndex,int nEndIndex)
{
	vector<uint8_t> pTest;
	pTest.resize(nPitch);

	if(!nPitch)
		return AJA_STATUS_FAIL;

	uint32_t	currentOutputFrame	(0);
	AJAStatus status(AJA_STATUS_SUCCESS);

	for(int i = nStartIndex; i < nEndIndex ; i++)
	{
		DisplayMovieData* pData = &(pArrDisplayData->at(i));
		int nFrameSize = pData->pPlayData.size();

		for(int j = 0 ; j<nFrameSize ; j++)
		{
			StartCounter();
			memcpy(pTest.data(),pData->pPlayData.at(j).data,nPitch);

			while(1)
			{
				if(GetCounter()>ESMGetValue(ESM_VALUE_AJASAMPLECNT))
					break;
			}
			if(!m_bFinish)
				return AJA_STATUS_FAIL;

			if (AJA_SUCCESS (status) && m_bFinish)
				if (!mDevice.DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE,
					currentOutputFrame,
					reinterpret_cast <uint32_t *> (&pTest[0]),
					(uint32_t)pTest.size ()) && m_bFinish)
				{
					status = AJA_STATUS_FAIL;
					break;
				}
		}
	}

	pTest.clear();

	return status;
}

AJAStatus NTV2OutputTestPattern::EmitPattern(vector<DisplayMovieData>*pArrDisplayData)
{
	vector<uint8_t> pTest;
	pTest.resize(nPitch);

	uint32_t	currentOutputFrame	(0);
	AJAStatus status(AJA_STATUS_SUCCESS);

	for(int i = 0; i < pArrDisplayData->size() ; i++)
	{
		DisplayMovieData* pData = &(pArrDisplayData->at(i));
		int nFrameSize = pData->pPlayData.size();

		for(int j = 0 ; j<nFrameSize ; j++)
		{
			StartCounter();

			memcpy(pTest.data(),pData->pPlayData.at(j).data,nPitch);

			while(1)
			{
				if(GetCounter() > ESMGetValue(ESM_VALUE_AJASAMPLECNT))
					break;
			}

			if (AJA_SUCCESS (status))
				if (!mDevice.DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE,
					currentOutputFrame,
					reinterpret_cast <uint32_t *> (&pTest[0]),
					(uint32_t)pTest.size ()))
				{
					status = AJA_STATUS_FAIL;
					break;
				}
		}
	}

	pTest.clear();

	return status;
}

AJAStatus NTV2OutputTestPattern::EmitPattern(cv::Mat YUV422,double dbTimeDelay)
{
	vector<uint8_t> pTest;
	if(nPitch == YUV422.total())
		pTest.resize(nPitch);
	else
		return AJA_STATUS_FAIL;

	uint32_t	currentOutputFrame	(0);
	AJAStatus status(AJA_STATUS_SUCCESS);

	
	StartCounter();
	if(nPitch == YUV422.total())
		memcpy(pTest.data(),YUV422.data,nPitch);
	else
		return AJA_STATUS_FAIL;

	while(1)
	{
		if(/*dbTimeDelay*/ GetCounter() > ESMGetValue(ESM_VALUE_AJASAMPLECNT))
			break;
	}

	if (AJA_SUCCESS (status))
	{
		if(!m_bFinish)
			return AJA_STATUS_FAIL;

		if(m_bFinish)
		{
			if (!mDevice.DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE,
				currentOutputFrame,
				reinterpret_cast <uint32_t *> (&pTest[0]),
				(uint32_t)pTest.size ()) && (nPitch == YUV422.total()))
			{
				status = AJA_STATUS_FAIL;
			}
		}	
	}


	pTest.clear();

	return status;
}

unsigned WINAPI NTV2OutputTestPattern::DisplayFrame(LPVOID param)
{
	////Read Param
	//ThreadDisplayData *pDisplayData = (ThreadDisplayData*) param;
	//vector<DisplayFrameData>*pPlayArrInfo = pDisplayData->pPlayArrInfo;
	//int nPitch = pDisplayData->nPitch;
	//int nTotal = pDisplayData->nTotal;
	//const NTV2Channel nChannel = *pDisplayData->mChannel;
	//AJAStatus status = pDisplayData->status;
	//CNTV2Card *mDevice = pDisplayData->mDevice;
	////Set a variable
	//uint32_t	currentOutputFrame	(0);
	//vector<uint8_t> pDisplayFrame;
	//pDisplayFrame.resize(nPitch);

	//if (AJA_SUCCESS (status))
	//	if (!mDevice->GetOutputFrame (nChannel, &currentOutputFrame))
	//	{
	//		status = AJA_STATUS_FAIL;
	//		return status;
	//	}

	//for(int i = 0 ; i <nTotal ; i++)
	//{
	//	DisplayFrameData pData = pPlayArrInfo->at(i);
	//	memcpy(pDisplayFrame.data(),pData.YUV422,nPitch);

	//	if (AJA_SUCCESS (status))
	//		if (!mDevice->DmaWriteFrame (NTV2_DMA_FIRST_AVAILABLE,
	//			currentOutputFrame,
	//			reinterpret_cast <uint32_t *> (&pDisplayFrame[0]),
	//			(uint32_t)pDisplayFrame.size ()))
	//		{
	//			status = AJA_STATUS_FAIL;
	//			break;
	//		}
	//	pData.bComplete++;
	//}

	//pPlayArrInfo->clear();
	return -1;
}

AJA_PixelFormat NTV2OutputTestPattern::GetAJAPixelFormat (const NTV2FrameBufferFormat format)
{
	switch (format)
	{
		case NTV2_FBF_NUMFRAMEBUFFERFORMATS:		return AJA_PixelFormat_Unknown;
		case NTV2_FBF_10BIT_YCBCR:					return AJA_PixelFormat_YCbCr10;		
		case NTV2_FBF_8BIT_YCBCR:					return AJA_PixelFormat_YCbCr8;			
		case NTV2_FBF_ARGB:							return AJA_PixelFormat_ARGB8;			
		case NTV2_FBF_RGBA:							return AJA_PixelFormat_RGBA8;			
		case NTV2_FBF_10BIT_RGB:					return AJA_PixelFormat_RGB10;			
		case NTV2_FBF_8BIT_YCBCR_YUY2:				return AJA_PixelFormat_YUY28;			
		case NTV2_FBF_ABGR:							return AJA_PixelFormat_ABGR8;			
		case NTV2_FBF_10BIT_DPX:					return AJA_PixelFormat_RGB_DPX;		
		case NTV2_FBF_10BIT_YCBCR_DPX:				return AJA_PixelFormat_YCbCr_DPX;		
		case NTV2_FBF_8BIT_DVCPRO:					return AJA_PixelFormat_DVCPRO;			
		case NTV2_FBF_8BIT_QREZ:					return AJA_PixelFormat_QREZ;			
		case NTV2_FBF_8BIT_HDV:						return AJA_PixelFormat_HDV;			
		case NTV2_FBF_24BIT_RGB:					return AJA_PixelFormat_RGB8_PACK;		
		case NTV2_FBF_24BIT_BGR:					return AJA_PixelFormat_BGR8_PACK;		
		case NTV2_FBF_10BIT_YCBCRA:					return AJA_PixelFormat_YCbCrA10;		
		case NTV2_FBF_10BIT_DPX_LITTLEENDIAN:		return AJA_PixelFormat_RGB_DPX_LE;		
		case NTV2_FBF_48BIT_RGB:					return AJA_PixelFormat_RGB12;			
		case NTV2_FBF_PRORES:						return AJA_PixelFormat_PRORES;			
		case NTV2_FBF_PRORES_DVCPRO:				return AJA_PixelFormat_PRORES_DVPRO;	
		case NTV2_FBF_PRORES_HDV:					return AJA_PixelFormat_PRORES_HDV;		
		case NTV2_FBF_10BIT_RGB_PACKED:				return AJA_PixelFormat_RGB10_PACK;		
		default:									return AJA_PixelFormat_Unknown;
	}
}	//	GetAJAPixelFormat

double NTV2OutputTestPattern::GetCounter()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart-CounterStart)/PCFreq;
}
void NTV2OutputTestPattern::StartCounter()
{
	PCFreq = 0.0;
	CounterStart  = 0;

	LARGE_INTEGER li;
	if(!QueryPerformanceFrequency(&li))
		cout << "QueryPerformanceFrequency failed!\n";

	PCFreq = double(li.QuadPart)/1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}