#include "stdafx.h"
#include "4DMaker.h"
#include "ESMUtilRemoteCtrl.h"
#include "ESMFileOperation.h"
#include "ESMFunc.h"
#include "afxdialogex.h"


// CESMUtilRemoteCtrl 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMUtilRemoteCtrl, CDialog)

CESMUtilRemoteCtrl::CESMUtilRemoteCtrl(CWnd* pParent /*=NULL*/)
	: CDialog(CESMUtilRemoteCtrl::IDD, pParent)
{
	m_nFrameCnt = 0;
	m_pRemoteFrameDlg = NULL;
	m_bRecordState = FALSE;
	m_nFinishTimeMovieIdx = -1;
	m_nFocusID = 0;
	m_bTimeThreadRun = FALSE;
	m_bCreate = FALSE;
	m_bRecordProcess = FALSE;
	m_nTimeIndex = -1;
	for(int i = 0 ; i < MAX_VIEW ; i ++)
	{
		m_bArrPlayState[i]	  = TRUE;
		m_nArrSyncMovieIdx[i] = -1;
		m_nArrSyncFrameIdx[i] = -1;
	}

	//wgkim
	m_pTemplateMgr = NULL;
	m_nZoomGuide = 150;
}

CESMUtilRemoteCtrl::~CESMUtilRemoteCtrl()
{
	/*
	for(int i = 0 ; i < m_nFrameCnt; i++)
		{
			if(m_pRemoteFrame[i])
			{
				delete m_pRemoteFrame;
				m_pRemoteFrame = NULL;
			}
		}*/
	m_nControlWidth = 0;
	m_nControlHeight = 0;
	m_nFrameWidth = 0;
	m_nFrameHeight = 0;
	
	if(m_pRemoteFrameDlg)
	{
		delete[] m_pRemoteFrameDlg;
		m_pRemoteFrameDlg = NULL;
	}
}

void CESMUtilRemoteCtrl::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CESMUtilRemoteCtrl, CDialog)
	ON_BN_CLICKED(IDC_REMOTE_CTRL_BTN_FRAMESET, &CESMUtilRemoteCtrl::OnBnClickedRemoteBtnFrameset)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDOK, &CESMUtilRemoteCtrl::OnBnClickedOk)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CESMUtilRemoteCtrl 메시지 처리기입니다.


BOOL CESMUtilRemoteCtrl::OnInitDialog()
{
	CDialog::OnInitDialog();	
	//GetViewResolution();
	//SetControlRepotision();

	/*GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM3)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM4)->ShowWindow(SW_HIDE);*/

	return TRUE;
}

void CESMUtilRemoteCtrl::SetControlRepotision()
{
	int nXStart = m_szResolution.cx - 120;
	int nYStart = m_szResolution.cy - 120;
	CRect rc;
	//GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM1)->GetClientRect(&rc);
	//GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM1)->MoveWindow()
	GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM1)->MoveWindow(nXStart,nYStart-80,100,20);
	GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM2)->MoveWindow(nXStart,nYStart-60,100,20);
	GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM3)->MoveWindow(nXStart,nYStart-40,100,20);
	GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM4)->MoveWindow(nXStart,nYStart-20,100,20);

	GetDlgItem(IDC_REMOTE_CTRL_STATIC_CAM1)->MoveWindow(nXStart-40,nYStart-80,30,13);
	GetDlgItem(IDC_REMOTE_CTRL_STATIC_CAM2)->MoveWindow(nXStart-40,nYStart-60,30,13);
	GetDlgItem(IDC_REMOTE_CTRL_STATIC_CAM3)->MoveWindow(nXStart-40,nYStart-40,30,13);
	GetDlgItem(IDC_REMOTE_CTRL_STATIC_CAM4)->MoveWindow(nXStart-40,nYStart-20,30,13);

	GetDlgItem(IDC_REMOTE_CTRL_STATIC_FRAMECNT)->MoveWindow(nXStart,nYStart,100,10);
	GetDlgItem(IDC_REMOTE_CTRL_EDIT_FRAMECNT)->MoveWindow(nXStart,nYStart+10,100,20);
	GetDlgItem(IDC_REMOTE_CTRL_BTN_FRAMESET)->MoveWindow(nXStart,nYStart+30,100,20);

	GetDlgItem(IDOK)->MoveWindow(nXStart,nYStart+50,100,20);
	GetDlgItem(IDCANCEL)->MoveWindow(nXStart,nYStart+70,100,20);

	GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM1)->SetWindowText(_T("20101"));
	GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM2)->SetWindowText(_T("20404"));
	GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM3)->SetWindowText(_T("30106"));
	GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM4)->SetWindowText(_T("30603"));

	GetDlgItem(IDC_REMOTE_CTRL_EDIT_FRAMECNT)->SetWindowText(_T("2"));
}

BOOL CESMUtilRemoteCtrl::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}
	if(pMsg->message == WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
		case '1':case '2':	case '9':
		case VK_F1:	case VK_F2:	case VK_F3:
		case VK_F4:	case VK_F5:
		case VK_RETURN:
			{
				ESMEvent* pEvent = new ESMEvent;
				pEvent->message  = WM_ESM_ENV_REMOTE_RECORDSTATE;
				pEvent->nParam1  = pMsg->wParam;
				::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pEvent);
			}
			break;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void CESMUtilRemoteCtrl::OnBnClickedRemoteBtnFrameset()
{
	return ;

	//CString strCnt;
	//GetDlgItem(IDC_REMOTE_CTRL_EDIT_FRAMECNT)->GetWindowText(strCnt);

	//m_nFrameCnt = _ttoi(strCnt);

	//if(m_nFrameCnt > 4)
	//{
	//	//AfxMessageBox(_T("No No"));
	//	return;
	//}

	//if(m_pRemoteFrameDlg)
	//{
	//	//AfxMessageBox(_T("No No"));
	//	delete[] m_pRemoteFrameDlg;
	//	m_pRemoteFrameDlg = NULL;
	//}
	//vector<int> nArrCamID;
	//for(int i = IDC_REMOTE_CTRL_EDIT_CAM1; i<= IDC_REMOTE_CTRL_EDIT_CAM4; i++)
	//{
	//	CString str;
	//	GetDlgItem(i)->GetWindowText(str);
	//	nArrCamID.push_back(_ttoi(str));
	//}
	//m_pRemoteFrameDlg = new CESMUtilRemoteFrame[m_nFrameCnt];

	//for(int i = 0 ; i <m_nFrameCnt; i++)
	//{
	//	CESMUtilRemoteFrame* pRemote = &m_pRemoteFrameDlg[i];
	//	pRemote->Create(IDD_UTIL_REMOTEFRAME,this);
	//	pRemote->SetSize(m_ptArrFrameStart[i].x,m_ptArrFrameStart[i].y,m_nFrameWidth,m_nFrameHeight);
	//	pRemote->SetCamID(_ttoi(m_strDSCID.at(i)));
	//	//pRemote->SetWindow(m_ptArrFrameStart[i].x,m_ptArrFrameStart[i].y);
	//	
	//	pRemote->m_nID	   = i;
	//	/*CString strDscID;
	//	strDscID.Format(_T("%d"),nArrCamID.at(i));*/
	//	m_mapDSCID.insert(pair<CString,int>(m_strDSCID.at(i),i));

	//	pRemote->Init(this);
	//	pRemote->SetRecordState(TRUE);
	//	//pRemote->m_pParent = this;
	//}

	//for(int i = 0 ; i < m_nFrameCnt; i++)
	//{
	//	CESMUtilRemoteFrame* pRemote = &m_pRemoteFrameDlg[i];
	//	//Size 위치, 크기
	//	pRemote->ShowWindow(SW_SHOW);
	//	pRemote->Run();
	//}
}

void CESMUtilRemoteCtrl::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	CDialog::OnLButtonDblClk(nFlags, point);
}

void CESMUtilRemoteCtrl::SetWindowState(int nID,BOOL bShow)
{
	//RemoteControl
	for(int i = 0 ; i < m_nFrameCnt; i++)
	{
		CESMUtilRemoteFrame* pRemote = &m_pRemoteFrameDlg[i];

		if(i == nID)
		{
			if(bShow)
			{
				CRect rc;
				GetClientRect(&rc);
				//ESMLog(5,_T("WindowRect %d - %d"),rc.Width(),rc.Height());

				pRemote->MoveWindow(rc);

				m_nCurFullIdx = nID;
			}
			else
			{
				CPoint ptPosition = GetControlPotision(i);
				CRect rc(ptPosition.x,ptPosition.y,m_nFrameWidth+ptPosition.x,m_nFrameHeight+ptPosition.y);
				pRemote->MoveWindow(rc);
			}

			continue;
		}

		if(bShow)
			pRemote->ShowWindow(SW_HIDE);
		else
			pRemote->ShowWindow(SW_SHOW);
	}
}

void CESMUtilRemoteCtrl::SetPlayState(int m_nID,BOOL bState)
{
	m_bArrPlayState[m_nID] = bState;

		/*CESMUtilRemoteFrame* pRemote = &m_pRemoteFrameDlg[i];
		if(i == m_nID)
			continue;*/
		//pRemote->SetPlayState(bState);

}

void CESMUtilRemoteCtrl::AddDSCPath(CString strPath)
{
	CString strDSC = ESMGetDSCIDFromPath(strPath);
	strDSC = strDSC.Right(5);

	int nID = m_mapDSCID[strDSC];

	CESMUtilRemoteFrame* pRemote = &(m_pRemoteFrameDlg[nID]);
	pRemote->AddMoviePath(strPath);
}

void CESMUtilRemoteCtrl::SetPlayFinish(int nID,BOOL bFinish)
{
	m_bArrPlayState[nID] = bFinish;
}

void CESMUtilRemoteCtrl::CreateRemoteFrameDlg()
{
	int nDSCNum = m_strArrDSCID.size();
	
	if(nDSCNum > MAX_VIEW)
		nDSCNum = MAX_VIEW;

	CString strCnt;
	strCnt.Format(_T("%d"),nDSCNum);
	
	m_nFrameCnt = nDSCNum;

	CRect rc;
	GetClientRect(&rc);

	GetDlgItem(IDC_REMOTE_CTRL_EDIT_FRAMECNT)->SetWindowText(strCnt);
	GetDlgItem(IDC_REMOTE_CTRL_EDIT_FRAMECNT)->EnableWindow(FALSE);

	m_pRemoteFrameDlg = new CESMUtilRemoteFrame[nDSCNum];

	SetTimeIndex();

#ifdef REVISION
	m_nFrameWidth = m_nControlWidth;
	m_nFrameHeight = m_nControlHeight;
#else
	m_nFrameWidth  = m_nControlWidth/2;
	m_nFrameHeight = m_nControlHeight/2;
	
	double dbMargin = 0;
	BOOL bVertical = FALSE;
	/*nHeight - ((double)nHeight/ 16.0 * 9.0);
	dbMargin /= 2;*/

	if(rc.right >= rc.bottom)
	{
		m_nFrameWidth  = m_nControlWidth/2;
		m_nFrameHeight = m_nControlHeight;
	}
	else
	{
		m_nFrameWidth  = m_nControlWidth;
		m_nFrameHeight = m_nControlHeight/2;
		bVertical = TRUE;
	}
#endif
	for(int i = 0 ; i < nDSCNum; i++)
	{
		CString strCAMID = m_strArrDSCID.at(i); 

		//GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM1+i)->SetWindowText(strCAMID);
		//GetDlgItem(IDC_REMOTE_CTRL_EDIT_CAM1+i)->EnableWindow(FALSE);

		m_mapDSCID.insert(pair<CString,int>(strCAMID,i));

		CESMUtilRemoteFrame* pRemote = &m_pRemoteFrameDlg[i];
		pRemote->Create(IDD_UTIL_REMOTEFRAME,this);
		pRemote->Init(this);
		pRemote->SetAdjustInfo(strCAMID);
		pRemote->SetFullScreenSize(rc);

		CPoint ptPoint = (CPoint) m_ptArrFrameStart[i];
		//pRemote->SetReposition(ptPoint,m_rcDiv,m_rcFull);
		//pRemote->SetFullScreenSize(m_rcFull);

#ifdef REVISION
		pRemote->MoveWindow(0,0,m_nFrameWidth,m_nFrameHeight);
#else
		if(bVertical == FALSE)
		{
			pRemote->MoveWindow(m_nFrameWidth * i, 0, m_nFrameWidth, m_nFrameHeight);
			
			CPoint ptPosition(m_nFrameWidth*i,0);
			SetControlPosition(i,ptPosition);
		}
		else
		{
			pRemote->MoveWindow(0 , m_nFrameHeight * i, m_nFrameWidth, m_nFrameHeight);
		
			CPoint ptPosition(0,m_nFrameHeight*i);
			SetControlPosition(i,ptPosition);
		}
#endif
		//pRemote->SetSize(m_ptArrFrameStart[i].x,m_ptArrFrameStart[i].y,m_nFrameWidth,m_nFrameHeight);
		
		pRemote->SetCamID(_ttoi(strCAMID));

		pRemote->m_nID	   = i;

		if(i==0)
			pRemote->ShowWindow(SW_SHOW);
		else
			pRemote->ShowWindow(SW_HIDE);
#ifndef TEST
		pRemote->Run();
#endif
	}

	if(m_bTimeThreadRun == FALSE)
	{
		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)_beginthreadex(NULL,0,_GetCurPlayTimeThread,(void*)this,0,NULL);
		CloseHandle(hSyncTime);
	}
}

void CESMUtilRemoteCtrl::SetRecordInit(BOOL bState)
{
	if(bState == TRUE) // Record 시작
	{
		CESMFileOperation fo;
		fo.Delete(_T("M:\\Movie"));
		//Delete All Image
		for(int i = 0 ; i < m_nFrameCnt; i++)
		{
			ThreadDeleteFrame* pDeleteFrame = new ThreadDeleteFrame;
			pDeleteFrame->pRemoteFrame		= &(m_pRemoteFrameDlg[i]);
			pDeleteFrame->nIndex			= i;
			pDeleteFrame->bFinish			= FALSE;
			HANDLE hSyncTime = NULL;
			hSyncTime = (HANDLE)_beginthreadex(NULL,0,_DeleteFrameThread,(void*)pDeleteFrame,0,NULL);
			CloseHandle(hSyncTime);
		}
	}
	else if(bState == FALSE)
	{
		int nTime = ESMGetTick();
		int nCurTime = (nTime - ESMGetRecordTime())/10000;
		
		/*for(int i = 0 ; i < m_nFrameCnt ; i++)
		{
		CESMUtilRemoteFrame* pRemote = &(m_pRemoteFrameDlg[i]);

		pRemote->SetRecordState(FALSE);
		}*/
		SetCurFinishTimeIdx(nCurTime);
	}
}
unsigned WINAPI CESMUtilRemoteCtrl::_DeleteFrameThread(LPVOID param)
{
	ThreadDeleteFrame* pDeleteFrame = (ThreadDeleteFrame*) param;
	CESMUtilRemoteFrame* pRemoteFrame = pDeleteFrame->pRemoteFrame;
	int nIndex = pDeleteFrame->nIndex;
	BOOL bFinish = pDeleteFrame->bFinish;
	delete pDeleteFrame;

	pRemoteFrame->SetRecordState(TRUE);
	pRemoteFrame->SetThreadFinish(TRUE);

	for(int i = 0 ; i < THREAD_TOTAL; i++)
	{
		if(pRemoteFrame->m_bArrThreadState[i] == TRUE)
		{
			while(1)
			{
				if(pRemoteFrame->m_bArrThreadState[i] == FALSE)
					break;

				Sleep(1);
			}
		}
	}
	/*delete pRemoteFrame->m_pArrImageMgr*/
	//pRemoteFrame->SetRecordState(FALSE);
	if(bFinish == FALSE)
		pRemoteFrame->Run();

	pRemoteFrame->SetThreadFinish(FALSE);

	_endthreadex(1);
	return TRUE;
}
unsigned WINAPI CESMUtilRemoteCtrl::_GetCurPlayTimeThread(LPVOID param)
{
	CESMUtilRemoteCtrl* pRemote = (CESMUtilRemoteCtrl*) param;
	while(1)
	{
		int nTime = ESMGetTick();
		int nCurTime = (nTime - ESMGetRecordTime())/10000;

		pRemote->SetCurRunningTime(nCurTime);

		Sleep(10);
	}
	return TRUE;
}
void CESMUtilRemoteCtrl::SetCurFinishTimeIdx(int nTime)
{
	for(int i = 0 ; i < m_nFrameCnt ; i++)
	{
		CESMUtilRemoteFrame* pRemote = &(m_pRemoteFrameDlg[i]);

		pRemote->SetFinishRecordTime(nTime + 1);

		pRemote->SetRecordState(FALSE);
	}
}

void CESMUtilRemoteCtrl::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	HWND hWND = GetSafeHwnd();
	::SetFocus(hWND);

	CDialog::OnLButtonUp(nFlags, point);
}

void CESMUtilRemoteCtrl::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	for(int i = 0 ; i < m_nFrameCnt ; i++)
	{
		CESMUtilRemoteFrame* pRemoteFrame = &(m_pRemoteFrameDlg[i]);

		ThreadDeleteFrame* pDeleteFrame = new ThreadDeleteFrame;
		pDeleteFrame->pRemoteFrame		= &(m_pRemoteFrameDlg[i]);
		pDeleteFrame->nIndex			= i;
		pDeleteFrame->bFinish			= TRUE;
		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)_beginthreadex(NULL,0,_DeleteFrameThread,(void*)pDeleteFrame,0,NULL);
		CloseHandle(hSyncTime);

		/*pRemoteFrame->SetRecordState(TRUE);

		for(int j = 0 ; j < THREAD_TOTAL; j++)
		{
		if(pRemoteFrame->m_bArrThreadState[j] == TRUE)
		{
		while(1)
		{
		if(pRemoteFrame->m_bArrThreadState[j] == FALSE)
		break;

		Sleep(1);
		}
		}
		}*/
	}

	/*if(m_pRemoteFrameDlg)
	{
	delete[] m_pRemoteFrameDlg;
	m_pRemoteFrameDlg = NULL;
	}
	m_bCreate = FALSE;*/

	CDialog::OnOK();
}

void CESMUtilRemoteCtrl::SetSyncFrameIdx(int nID,int nMovieIdx,int nIdx)
{
	if(m_bArrPlayState[nID] == TRUE)
	{
		m_nArrSyncMovieIdx[nID] = nMovieIdx;
		m_nArrSyncFrameIdx[nID] = nIdx;
	}
	//m_nArrSyncMovieIdx[nID] = nMovieIdx;
	//m_nArrSyncFrameIdx[nID] = nIdx;
}

int CESMUtilRemoteCtrl::GetSyncFrameIdx(int nIdx)
{
	SetTimeIndex();

	int nCurPlayIdx = -1;
	for(int i = 0 ; i < MAX_VIEW; i++)
	{
		if(i == nIdx)
			continue;

		if(m_bArrPlayState[i] == TRUE)
		{
			nCurPlayIdx = i;
			break;
		}
	}
	BOOL bFinish[MAX_VIEW] ={FALSE,};
	BOOL bErr = FALSE;
	
	CESMUtilRemoteFrame* pBaseFrame = &(m_pRemoteFrameDlg[nIdx]);

	if(nCurPlayIdx != -1)
	{
		while(1)
		{
			
			if(m_nArrSyncFrameIdx[nCurPlayIdx] == m_nTimeIndex)
				break;

			
			for(int i = 0 ;i < MAX_VIEW; i++)
			{
				CESMUtilRemoteFrame* pFrame = &(m_pRemoteFrameDlg[i]);
				if(pFrame->GetPlayState() == FALSE)
					bFinish[i] = TRUE;
			}

			if(bFinish[0] == TRUE && bFinish[1] == TRUE)
			{
				bErr = TRUE;
				break;
			}

			if(pBaseFrame->GetThreadFinish() == TRUE)
			{
				bErr = TRUE;
				break;
			}

			Sleep(1);
		}

		if(bErr)
			return -100;
		else
			return m_nArrSyncMovieIdx[nCurPlayIdx];
	}	
	else
		return -100;
}

void CESMUtilRemoteCtrl::SetFocusState(int nID,BOOL bFocus)
{
	CESMUtilRemoteFrame* pRemote = &m_pRemoteFrameDlg[nID];
	pRemote->SetIsFocus(bFocus);
	if(bFocus)
		m_nFocusID = nID;
	//for(int i = 0 ; i < m_nFrameCnt; i++)
	//{
	//	CESMUtilRemoteFrame* pRemote = &m_pRemoteFrameDlg[i];
	//	
	//	if(pRemote->m_bActiveFrame == FALSE)
	//		continue;

	//	if(nID == i)
	//	{
	//		//ESMLog(5,_T("FOCUS - %d"),i);
	//		pRemote->SetMouseClicked(TRUE);
	//		pRemote->SetItAsFocus();
	//		//pRemote->OnLButtonUp(0,CPoint(0,0));
	//	}
	//	else
	//	{
	//		//ESMLog(5,_T("NON FOCUS - %d"),i);
	//		pRemote->SetMouseClicked(FALSE);
	//		//pRemote->OnLButtonUp(0,CPoint(0,0));
	//	}		
	//}
}

void CESMUtilRemoteCtrl::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if(cx == 0 || cy == 0)
		return;

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	CRect rc;
	GetClientRect(rc);

	int nWidth  = rc.right;
	int nHeight = rc.bottom - rc.top + 2;
	m_nControlWidth  = nWidth;
	m_nControlHeight = nHeight;

#ifdef REVISION
	if(m_pRemoteFrameDlg)
	{
		for(int i = 0 ; i < MAX_VIEW; i++)
		{
			CESMUtilRemoteFrame* pFrame = &(m_pRemoteFrameDlg[i]);

			pFrame->MoveWindow(0,0,m_nControlWidth,m_nControlHeight);

		}
	}
#else
	if(m_pRemoteFrameDlg)
	{
		if(m_nFrameCnt==1)
		{
			nWidth  = cx;
			nHeight = cy;
			m_nFrameWidth  = nWidth;
			m_nFrameHeight = nHeight;
			CPoint ptPosition;
			ptPosition.x = 0;
			ptPosition.y = 0;
			ESMLog(5,_T("[REMOTE] %d %d"),nWidth,nHeight);
			SetControlPosition(0,ptPosition);
		}
		else if (m_nFrameCnt == 2)
		{
			// bottom -> height, left -> width
			if(cx >= cy)
			{
				//가로 2개씩
				CPoint ptPosition;	
				m_nFrameWidth  = cx / 2;
				m_nFrameHeight = cy;
				//m_rcDiv.right  = m_nFrameWidth;
				//m_rcDiv.bottom = m_nFrameHeight;

				for(int i = 0 ; i < m_nFrameCnt; i++)
				{
					ptPosition.x = m_nFrameWidth * i;
					ptPosition.y = 0;
					SetControlPosition(i,ptPosition);
				}
			}
			else if(cx < cy)
			{
				//세로 세팅
				CPoint ptPosition;
				m_nFrameWidth = cx;
				m_nFrameHeight = cy / 2;

				//m_rcDiv.right  = m_nFrameWidth;
				//m_rcDiv.bottom = m_nFrameHeight;
				for(int i = 0 ; i < m_nFrameCnt; i++)
				{
					ptPosition.x = 0;
					ptPosition.y = m_nFrameHeight * (i);
					SetControlPosition(i,ptPosition);
				}
			}
		}
		for(int i = 0 ; i < m_nFrameCnt; i++)
		{
			CPoint ptPosition = GetControlPotision(i);

			CESMUtilRemoteFrame* pRemote = &(m_pRemoteFrameDlg[i]);

			int nFrameWidth  = ptPosition.x + m_nFrameWidth;
			int nFrameHeight = ptPosition.y + m_nFrameHeight;

			CRect rcDiv(ptPosition.x,ptPosition.y,nFrameWidth,nFrameHeight);
			pRemote->SetFullScreenSize(rc);
			pRemote->MoveWindow(rcDiv);
			/*CRect rcFull;

			GetClientRect(&rcFull);

			pRemote->SetReposition(ptPosition,rcDiv,rcFull);
			pRemote->SetSize(m_ptArrFrameStart[i].x,m_ptArrFrameStart[i].y,
				m_nFrameWidth,m_nFrameHeight);*/
		}
	}
#endif
}

void CESMUtilRemoteCtrl::SendEventMsg(int nID,WPARAM wParam)
{
	switch(wParam)
	{
	case '1':		case '2':		case '9':
	case VK_F1:	case VK_F2:	case VK_F3:	case VK_F4:	case VK_F5:
	case VK_RETURN: case 'Z':
		{
			ESMEvent* pEvent = new ESMEvent;
			pEvent->message  = WM_ESM_ENV_REMOTE_RECORDSTATE;
			pEvent->nParam1  = wParam;
			::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pEvent);
		}
		break;
	case VK_NUMPAD1:case VK_NUMPAD2:case VK_NUMPAD3:
	case VK_NUMPAD4:case VK_NUMPAD5:case VK_NUMPAD6:
	case VK_NUMPAD7:case VK_NUMPAD8:case VK_NUMPAD9:
		{
			CESMUtilRemoteFrame* pRemote = &(m_pRemoteFrameDlg[nID]);
			int nTime = pRemote->GetMakingTime();
			if(nTime >= 0)
			{
				ESMEvent* pEvent = new ESMEvent;
				pEvent->message  = WM_ESM_ENV_REMOTE_MAKING;
				pEvent->nParam1  = wParam;
				pEvent->nParam2	 = nTime;
				::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pEvent);
			}
			else
			{
				ESMLog(0,_T("Load Error! Reload Select Frame!"));
			}
		}
		break;
	}
}
void CESMUtilRemoteCtrl::SetTimeIndex()
{
	if(movie_frame_per_second == 25)
		m_nTimeIndex = movie_frame_per_second - 2;
	else
		m_nTimeIndex = movie_frame_per_second -1;
}

//wgkim
void CESMUtilRemoteCtrl::SetTemplateMgr(CESMTemplateMgr* pTemplateMgr)
{
	m_pTemplateMgr = pTemplateMgr;
}

CPoint CESMUtilRemoteCtrl::Point2fToCPoint(cv::Point2f point)
{
	CPoint temp;
	temp.x = point.x;
	temp.y = point.y;

	return temp;
}

cv::Point2f CESMUtilRemoteCtrl::CPointToPoint2f(CPoint point)
{
	cv::Point2f temp;
	temp.x = point.x;
	temp.y = point.y;

	return temp;
}

void CESMUtilRemoteCtrl::SetSelectTime(int nTime)
{
	nTime /= 10;
	int nMovieIdx = 0, nFrameIdx = 0;
	//ESMGetMovieTime(nTime,nSec,nMilli);
	int nShiftFrame = ESMGetFrameIndex(nTime);
	ESMGetMovieIndex(nShiftFrame,nMovieIdx,nFrameIdx);

	ImageSaveIndex stSave;
	stSave.nTime	 = nTime;
	stSave.nMovieIdx = nMovieIdx;
	stSave.nFrameIdx = nFrameIdx;

	for(int i = 0 ; i < m_nFrameCnt; i++)
	{
		CESMUtilRemoteFrame* pFrame = &m_pRemoteFrameDlg[i];

		pFrame->m_stArrSaveIdx.push_back(stSave);
		//pFrame->m_nArrNonDeleteIdx.push_back(nTime);
	}

	ESMLog(5,_T("'Z' Time: %d (%d - %d)"),nTime,nMovieIdx,nFrameIdx);
}

BOOL CESMUtilRemoteCtrl::SetControlFrameSize(CRect rc,int nDSCNum)
{
	BOOL bVertical = TRUE;

	m_nFrameHeight;
	m_nFrameWidth;

	return bVertical;
}

void CESMUtilRemoteCtrl::AllFrameStop(int nIdx,int nCalcPoint)
{
	for(int i = 0 ; i < MAX_VIEW; i++)
	{
		CESMUtilRemoteFrame* pFrame = &m_pRemoteFrameDlg[i];

		pFrame->SetSelectorNum(nCalcPoint);
		pFrame->SetSelectLoad(TRUE);
	}
}
void CESMUtilRemoteCtrl::SetSyncInFrameView(int m_nID,int nMovIdx,BOOL bMode/* = FALSE*/)
{
	for(int i = 0 ; i < MAX_VIEW; i++)
	{
		//if(m_nID == i)
		//	continue;

		CESMUtilRemoteFrame* pFrame = &m_pRemoteFrameDlg[i];

		if(bMode == TRUE)
			pFrame->SetSelectLoad(TRUE);
		else
			pFrame->SetPlayState(TRUE);
	}
}
void CESMUtilRemoteCtrl::ChangeFocus(int nID,BOOL bFlag)
{
	if(bFlag==TRUE)
	{
		m_nFocusID++;
		if(m_nFocusID > 1)
			m_nFocusID = 1;
	}
	else
	{
		m_nFocusID--;
		if(m_nFocusID < 0)
			m_nFocusID = 0;
	}

	for(int i = 0 ; i < MAX_VIEW; i++)
	{
		CESMUtilRemoteFrame* pFrame = &m_pRemoteFrameDlg[i];

		if(m_nFocusID == i)
		{
			//ESMLog(5,_T("FOCUS - %d"),i);
			pFrame->SetMouseClicked(FALSE);
			pFrame->OnLButtonUp(0,CPoint(0,0));
			pFrame->ShowWindow(SW_SHOW);
		}
		else
		{
			//ESMLog(5,_T("NON FOCUS - %d"),i);
			pFrame->SetMouseClicked(TRUE);
			pFrame->OnLButtonUp(0,CPoint(0,0));
			pFrame->ShowWindow(SW_HIDE);
		}		
	}
	Invalidate();
}
void CESMUtilRemoteCtrl::ShowRedSpotPoint(WPARAM wParam,int nID,int nCount,int nTime)
{
	switch(wParam)
	{
	case 'X': case 'C': case 'V':
		{
			CESMUtilRemoteFrame* pFrame = &(m_pRemoteFrameDlg[nID]);

			//ESMLog(5,_T("Move Red Point"));

			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_ESM_VIEW_FRMESPOTVIEWOFF;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

			pMsg = new ESMEvent;
			pMsg->message = WM_ESM_REMOTE_SPOT_POINT;
			pMsg->nParam1 = ESMGetDSCIndex(m_strArrDSCID.at(nID));//CamID
			pMsg->nParam2 = nCount;
			pMsg->nParam3 = nTime;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		}
		break;
	}
}
void CESMUtilRemoteCtrl::ClearTemplatePoint()
{
	//cv::Point2f pt(0.0,0.0);
	//m_pTemplateMgr->SetCenterPoint(pt);
	//m_pTemplateMgr->SetViewPoint(pt);
}
void CESMUtilRemoteCtrl::SetFocusFromMainFrm()
{
	CESMUtilRemoteFrame* pFrame = &m_pRemoteFrameDlg[m_nFocusID];
	pFrame->SetFocus();
	pFrame->SetMouseClicked(FALSE);
	pFrame->OnLButtonUp(0,CPoint(0,0));
}
void CESMUtilRemoteCtrl::ShowScreenFromUser(int nID)
{
	for(int i = 0 ; i < MAX_VIEW; i++)
	{
		if(i == nID)
			continue;
		
		CESMUtilRemoteFrame* pFrame = &(m_pRemoteFrameDlg[i]);
		pFrame->SetShowFrame(TRUE);
	}
}