#pragma once


// CESMEditorEX

class CESMEditorEX : public CEdit
{
	DECLARE_DYNAMIC(CESMEditorEX)

public:
	CESMEditorEX();
	virtual ~CESMEditorEX();

protected:
	DECLARE_MESSAGE_MAP()
	CFont *m_pFont;
	int m_nHeight;
public:
	afx_msg void OnNcPaint();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	CBrush m_brush;
	virtual void ChangeFont(int nSize = 9, CString strFont = _T("Noto Sans CJK KR Regular"), int nFontType = FW_NORMAL);
	void SetHeight(int nHeight = 24) { m_nHeight = nHeight; }
};


