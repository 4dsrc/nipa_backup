#pragma once
#include "ESMCtrl.h"

// CESMButtonEx

class CESMButtonEx : public CESMBtn
{
	DECLARE_DYNAMIC(CESMButtonEx)

public:
	CESMButtonEx();
	virtual ~CESMButtonEx();

	void ChangeColors();
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);
	virtual void ChangeFont(int nSize = 12, CString strFont = _T("Noto Sans CJK KR Regular"), int nFontType = FW_NORMAL);
	CFont *m_pFont;
	int m_nFontSize;
	COLORREF m_clrDisable;
	COLORREF m_clrNormal;
	COLORREF m_clrOver;
	COLORREF m_clrPress;
	COLORREF m_clrTextWhite;
	COLORREF m_clrTextGray;

	Color m_colorOver;
	Color m_colorNormal;
	Color m_colorPress;
protected:
	DECLARE_MESSAGE_MAP()
};


