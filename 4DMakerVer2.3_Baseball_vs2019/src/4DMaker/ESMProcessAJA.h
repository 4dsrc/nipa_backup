#pragma once 


#include"stdafx.h"
//#include "ESMFileOperation.h"

#include "ajastuff/common/types.h"
#include "ajastuff/common/options_popt.h"

#include "ESMFunc.h"
#include "ESMDefine.h"
#include "ESMIndexStructure.h"
#include "ntv2outputtestpattern.h"

#include "shlwapi.h"
#include "ESMDirRead.h"

#define MAX_RECEIVE_CNT 50
struct AJAFinish
{
	AJAFinish()
	{
		nAddingCnt = 0;
		nTotalCnt     = 0;
		nIndex         = 0;
		bFinish        = FALSE;

		for(int i = 0 ; i <MAX_RECEIVE_CNT; i++)
		{
			strPath[i].Format(_T(""));
			bAddFinish[i] = FALSE;
		}
	}
	int nAddingCnt;
	int nTotalCnt;
	int nIndex;
	BOOL bFinish;
	BOOL bAddFinish[MAX_RECEIVE_CNT];
	CString strPath[MAX_RECEIVE_CNT];
};

class CESMProcessAJA
{
public:
	CESMProcessAJA();
	~CESMProcessAJA();
	/*Functions*/
	LRESULT ManageMsg(ESMEvent* pMsg);
	void _DoWaitThread();
	void _DoDecodingThread(vector<DisplayMovieData>*pArrDisplayData,int nIndex);
	int DecodingMovie(vector<DisplayMovieData>*pArrDisplayData,int nIndex);
	void AddString(CString str,CString strTime,int nIndex);
	int BGR2YUV422(cv::Mat BGR,cv::Mat YUV,int nHeight,int nWidth);
	int InsertLogo(cv::Mat *Frame);
	void OverLayImage(cv::Mat *frame,cv::Mat Logo,cv::Mat Output);
	int CuttingValue(int nValue);
	AJAStatus AJAProcess();
	BOOL AJAStart();
	void SelectFilePlay(CString strPath);
	void SetMovieSignal(int nS){m_nSignal = nS;};
	int GetMovieSignal(){return m_nSignal;};
	void StartCounter();
	double GetCounter();
	void SetThreadSignal(BOOL bS){m_bThread = bS;};
	BOOL GetThreadSignal(){return m_bThread;};
	void DoReplay();
	void DoSelReplay(vector<cv::Mat>ArrMovieData);
	BOOL GetMovieStatus();
	BOOL GetReplayStatus(){return m_bReplayState;};
	void SetReplayStatus(BOOL bS){m_bReplayState = bS;};	/*Thread*/
	static unsigned WINAPI WaitPath  (LPVOID param);
	static unsigned WINAPI DecodingFrame  (LPVOID param);
	static unsigned WINAPI SelectFilePlay(LPVOID param);
	static unsigned WINAPI ReplayThread(LPVOID param);

	/*Map*/
	map<CString, AJAFinish> map_AJAData;

	/*Vector*/
	vector<DisplayMovieData> *m_pArrDisplayData;
	vector<cv::Mat>m_ArrReplayData;	/*Variable*/
	BOOL m_bMaking;
	BOOL m_bThread;
	BOOL m_bSelPlay;
	NTV2OutputTestPattern *m_Process;
	AJAStatus m_aStart;
	CString m_strSelectFile;
	CString m_strCurTime;
	char m_strBackgroundPath[200];
	cv::Mat m_mYUV;
	cv::Mat m_mBGR;
	cv::Mat m_mYUVBlack;
	int m_nSignal;
	double PCFreq;
	__int64 CounterStart;
	BOOL bDoingProcessing;
	BOOL m_bReplayState;


	/*CriticalSection*/
	CRITICAL_SECTION m_Cri;};
struct ThreadDisplayAJA
{
	ThreadDisplayAJA()
	{
		pParent = NULL;
		pParent2 = NULL;
		pArrDisplayData = NULL;
		bComplete = 0;
		nIndex = -1;
	};
	CESMProcessAJA * pParent;
	NTV2OutputTestPattern* pParent2;
	int bComplete;
	vector<DisplayMovieData>*pArrDisplayData;
	int nIndex;
	//CESMFileOperation* pParent1;
};