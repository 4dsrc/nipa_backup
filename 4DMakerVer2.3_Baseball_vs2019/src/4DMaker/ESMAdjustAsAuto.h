//-- 2014-08-31 wgkim@esmlab.com
//Implementation AutoAdjust Using LKOpticalFlow

#pragma once

#include "ESMAdjustMgr.h"

class ESMAdjustAsAuto
{
public:
	ESMAdjustAsAuto(void);
	~ESMAdjustAsAuto(void);
	
public:	
	void InputAdjustFeaturesFromList(CListCtrl* pList, int nSelectedItem);
	void ApplyCalculatedAdjustFeatureToList(CListCtrl* pList, int nSelectedItem);
	void CalcAutoAdjustUsingOpticalFlow(Mat& srcImage);	

private:
	vector<Point2f> currentAdjustFeatures;
	vector<Point2f> calculatedAdjustFeatures;
	vector<uchar>	statusAdjustFeatures;	
	vector<float>	_err;	

private:
	Mat previousImageForAdjust;
	Mat nextImageForAdjust;	

};

