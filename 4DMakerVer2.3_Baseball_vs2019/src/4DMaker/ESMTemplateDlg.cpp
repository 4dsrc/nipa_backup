// ESMTemplateDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#ifdef _4DMODEL
#include "4DModeler.h"
#else
#include "4DMaker.h"
#endif
#include "ESMTemplateDlg.h"
#include "ESMTemplateImage.h"
#include "ESMTemplateMgr.h"
#include "afxdialogex.h"
#include "FFmpegManager.h"
#include "ESMIni.h"
#include "ESMCtrl.h"

#include "ESMFileOperation.h"
#include "ESMFunc.h"
#include "ESMMovieMgr.h"
#include "ESMImgMgr.h"

#include<highgui.h>
#include<cv.h>

#include <math.h>

#define PI 3.1415926535897932384

// ESMAdjustMgrDlg 대화 상자입니다.
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


enum{
	FIRST_ADJUST_VIEW = 0,
};

enum{
	ADJUST_IMAGE = 0,
	MAX_IDX,
};

enum{
	ADJUST_LEFT_UP = 0,
	ADJUST_LEFT_DOWN,
	ADJUST_RIGHT_UP,
	ADJUST_RIGHT_DOWN,
};
enum{
	T_LB_D = 0,
	T_RB_D,
	T_PEAK_D,
	B_PEAK_D,
};
IMPLEMENT_DYNAMIC(CESMTemplateDlg, CDialogEx)


CESMTemplateDlg::CESMTemplateDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMTemplateDlg::IDD, pParent)
{
	m_nSelectPos = 0;
	m_nSelectedNum = -1;
	m_nHeight = 0;
	m_nWidth = 0;
	m_frameIndex=0;
	m_nPosX = 0, m_nPosY = 0;	
	m_MarginX = 0;
	m_MarginY = 0;	
	m_hTheadHandle = NULL;

	m_bThreadCloseFlag = FALSE;
	m_nYPointModifyValue = 0;

	m_background.CreateSolidBrush(RGB(44,44,44));
	m_AdjustDscList.SetUseCamStatusColor(FALSE);
}

CESMTemplateDlg::~CESMTemplateDlg()
{		
	WaitForSingleObject(m_hTheadHandle, INFINITE);
	CloseHandle(m_hTheadHandle);
}

void CESMTemplateDlg::OnOK()
{
	m_bThreadCloseFlag = TRUE;
	applyHomographiesController();	

	CDialog::OnOK();
}

void CESMTemplateDlg::OnCancel()
{	
	m_bThreadCloseFlag = TRUE;
	CDialog::OnCancel();
}

void CESMTemplateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX,	IDC_TEMPLATE_DSCLIST,	m_AdjustDscList);
	DDX_Control(pDX,	 IDC_TEMPLATE_COMBO,	m_ctrlCheckPoint);
	DDX_Control(pDX,	IDC_ST_PRISMIMAGE,  m_StAdjustImage);
	DDX_Control(pDX, IDC_EDIT_TEMPLATE_CENTER, m_ctrlCenterLine);
	DDX_Check(pDX, IDC_CHECK_AUTOPOINT, m_bAutoPoint);	
	DDX_Check(pDX, IDC_CHECK_AUTOPOINT_MOVE, m_bAutoPointMove);	
	DDX_Text(pDX, IDC_EDIT_YPOINT_MODIFY, m_nYPointModifyValue);
	DDX_Control(pDX, IDC_BTN_LOAD, m_btnLoad);
	DDX_Control(pDX, IDC_BTN_SAVE, m_btnSave);
	DDX_Control(pDX, IDC_BTN_RESET, m_btnReset);
	DDX_Control(pDX, IDOK, m_btnOk);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_BUTTON_YPOINT_MODIFY, m_btnYPointModify);
}


BEGIN_MESSAGE_MAP(CESMTemplateDlg, CDialogEx)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_TEMPLATE_DSCLIST, &CESMTemplateDlg::OnCustomdrawTcpList)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_TEMPLATE_DSCLIST, &CESMTemplateDlg::OnLvnItemchangedAdjust)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEWHEEL()
	ON_WM_LBUTTONDBLCLK()
	ON_BN_CLICKED(IDC_BTN_RESET, &CESMTemplateDlg::OnBnClickedBtnAdjustPointReset)
	ON_BN_CLICKED(IDC_BTN_SAVE, &CESMTemplateDlg::OnBnClickedBtn2ndSavePoint)
	ON_BN_CLICKED(IDC_BTN_LOAD, &CESMTemplateDlg::OnBnClickedBtn2ndLoadPoint)
//	ON_BN_CLICKED(IDC_BTN_STRUCT, &CESMTemplateDlg::OnBnApplyHomography)
	ON_BN_CLICKED(IDC_CHECK_AUTOPOINT, &CESMTemplateDlg::OnBnClickedBtnAutoFind)
	ON_BN_CLICKED(IDC_CHECK_AUTOPOINT_MOVE, &CESMTemplateDlg::OnBnClickedBtnAutoMove)
	ON_BN_CLICKED(IDC_BUTTON_YPOINT_MODIFY, &CESMTemplateDlg::OnBnClickedBtnYPointModifyAll)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

void CESMTemplateDlg::OnCustomdrawTcpList(NMHDR* pNMHDR, LRESULT* pResult)
{	
	NMLVCUSTOMDRAW* lplvcd = (NMLVCUSTOMDRAW*)pNMHDR;
	if(lplvcd->nmcd.dwDrawStage == CDDS_PREPAINT)
	{
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if(lplvcd->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
	{
		*pResult = CDRF_NOTIFYSUBITEMDRAW;
	}
	else if(lplvcd->nmcd.dwDrawStage == (CDDS_ITEMPREPAINT | CDDS_SUBITEM))
	{
		if( lplvcd->nmcd.dwItemSpec == m_nSelectedNum)
		{
			if(m_nSelectPos * 2<=  lplvcd->iSubItem && m_nSelectPos * 2 + 1 >=  lplvcd->iSubItem)
			{
				lplvcd->clrTextBk = RGB(200, 200, 255);
				lplvcd->clrText = RGB(255, 0, 0);
			}
			else
			{
				lplvcd->clrTextBk = RGB(255, 255, 255);
				lplvcd->clrText = RGB(0, 0, 0);
			}
		}
		*pResult = CDRF_DODEFAULT;
	}
}

void CESMTemplateDlg::setDscListItem(CListCtrl* pList, int rowIndex, int colIndex, int data)
{
	CString strText;
	strText.Format(_T("%d"), data);
	pList->SetItemText(rowIndex, colIndex, strText);	
}

int CESMTemplateDlg::getDSCListItem(CListCtrl* pList, int rowIndex, int colIndex)
{
	CString strSelect = pList->GetItemText(rowIndex, colIndex);
	return _ttoi(strSelect);	
}

void CESMTemplateDlg::SetMovieState(int nMovieState )
{
	m_nMovieState = nMovieState;
}

BOOL CESMTemplateDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_nMovieState = ESM_ADJ_FILM_STATE_MOVIE;

	m_AdjustDscList.InsertColumn(0, _T("ID"), 0, 30);
	m_AdjustDscList.InsertColumn(1, _T("DscId"), 0, 44);
	m_AdjustDscList.InsertColumn(2, _T("1st X"), 0, 50);
	m_AdjustDscList.InsertColumn(3, _T("1st Y"), 0, 50); 
	m_AdjustDscList.InsertColumn(4, _T("2nd X"), 0, 50);
	m_AdjustDscList.InsertColumn(5, _T("2nd Y"), 0, 50);
	m_AdjustDscList.InsertColumn(6, _T("3rd X"), 0, 50);
	m_AdjustDscList.InsertColumn(7, _T("3rd Y"), 0, 50);
	m_AdjustDscList.InsertColumn(10, _T("4th X"), 0, 50);
	m_AdjustDscList.InsertColumn(11, _T("4th Y"), 0, 50);

	m_AdjustDscList.SetExtendedStyle(/*LVS_EX_GRIDLINES|*/LVS_EX_FULLROWSELECT);

	m_ctrlCheckPoint.AddString(_T("1st"));
	m_ctrlCheckPoint.AddString(_T("2nd"));
	m_ctrlCheckPoint.AddString(_T("3rd"));
	m_ctrlCheckPoint.AddString(_T("4th"));
	m_ctrlCheckPoint.AddString(_T("None"));
	m_ctrlCheckPoint.SetCurSel(4);

	LoadData();
	Showimage();
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	LoadPointData(strFileName);
	
	m_Move = 0;
	m_Scale = 0;
	m_Number = 0;
	UpdateData(FALSE);
	return TRUE;
}

void CESMTemplateDlg::AddTemplateMgr(CESMTemplateMgr *templeteMgr)
{
	m_pTemplateMgr = templeteMgr;
}

vector<Point2f> CESMTemplateDlg::getSquareFromListCtrl(CListCtrl* pList, int nSelectedNumber)
{
	vector<Point2f> square(4);
	
	if(ESMGetValue(ESM_VALUE_REVERSE_MOVIE))
	{
		for(int j = 1; j < 5; j++)
		{			
			square[j-1].x = m_nWidth - getDSCListItem(pList, nSelectedNumber, j*2);
			square[j-1].y = m_nHeight - getDSCListItem(pList, nSelectedNumber, j*2 + 1);

			if(square[j-1].x == m_nWidth)
				square[j-1].x = 0;
			if(square[j-1].y == m_nHeight)
				square[j-1].y = 0;
		}
	}
	else
	{
		for(int j = 1; j < 5; j++)
		{
			square[j-1].x = getDSCListItem(pList, nSelectedNumber, j*2);
			square[j-1].y = getDSCListItem(pList, nSelectedNumber, j*2 + 1);		
		}
	}

	return square;
}

void CESMTemplateDlg::SavePointData(CString strFileName)
{
	CESMIni ini;
	CFile file;
	file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite);
	file.Close();

	if(!ini.SetIniFilename (strFileName))
		return;

	ini.WriteInt(_T("MovieSize"), _T("HEIGHT"), m_nHeight);
	ini.WriteInt(_T("MovieSize"), _T("WIDTH"), m_nWidth);

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);
		ini.WriteString(strSelect, _T("T_LBX"), m_AdjustDscList.GetItemText(i, 2));
		ini.WriteString(strSelect, _T("T_LBY"), m_AdjustDscList.GetItemText(i, 3));
		ini.WriteString(strSelect, _T("T_RBX"), m_AdjustDscList.GetItemText(i, 4));
		ini.WriteString(strSelect, _T("T_RBY"), m_AdjustDscList.GetItemText(i, 5));
		ini.WriteString(strSelect, _T("T_PeakX"), m_AdjustDscList.GetItemText(i, 6));
		ini.WriteString(strSelect, _T("T_PeakY"), m_AdjustDscList.GetItemText(i, 7));
		ini.WriteString(strSelect, _T("B_PeakX"), m_AdjustDscList.GetItemText(i, 8));
		ini.WriteString(strSelect, _T("B_PeakY"), m_AdjustDscList.GetItemText(i, 9));
	}
}

void CESMTemplateDlg::LoadPointData(CString strFileName)
{
	CESMIni ini;
	if(!ini.SetIniFilename (strFileName))
		return;

	int nHeight = 0, nWidth = 0;
	nHeight = ini.GetInt(_T("MovieSize"), _T("HEIGHT"), 0);
	nWidth = ini.GetInt(_T("MovieSize"), _T("WIDTH"), 0);
	if(nHeight != 0)
		m_nHeight = nHeight;

	if(nWidth != 0)
		m_nWidth = nWidth;

	CString strData;
	//stTemplateInfo axis;
	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);
		strData = ini.GetString(strSelect, _T("T_LBX"));
		m_AdjustDscList.SetItemText(i, 2, strData);		

		strData = ini.GetString(strSelect, _T("T_LBY"));
		m_AdjustDscList.SetItemText(i, 3, strData);		

		strData = ini.GetString(strSelect, _T("T_RBX"));
		m_AdjustDscList.SetItemText(i, 4, strData);		

		strData = ini.GetString(strSelect, _T("T_RBY"));
		m_AdjustDscList.SetItemText(i, 5, strData);		

		strData = ini.GetString(strSelect, _T("T_PeakX"));
		m_AdjustDscList.SetItemText(i, 6, strData);		

		strData = ini.GetString(strSelect, _T("T_PeakY"));
		m_AdjustDscList.SetItemText(i, 7, strData);

		strData = ini.GetString(strSelect, _T("B_PeakX"));
		m_AdjustDscList.SetItemText(i, 8, strData);		

		strData = ini.GetString(strSelect, _T("B_PeakY"));
		m_AdjustDscList.SetItemText(i, 9, strData);				
	}
}

void CESMTemplateDlg::applyHomographiesController()
{
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);	

	vector<vector<Point2f>> squares;
	CListCtrl* pList = &m_AdjustDscList;	

	for(int i = 0; i < pList->GetItemCount(); i++)	
		squares.push_back(getSquareFromListCtrl(pList, i));	

	m_pTemplateMgr->ClearArrSquares();
	m_pTemplateMgr->ApplyHomographiesFromArrSquares(squares);

	SavePointData(strFileName);	
}

void CESMTemplateDlg::Showimage()
{
	CListCtrl* pList;
	CESMTemplateImage* pAdjustImg;
	int nCount, nMaxCount;
	
	pList = &m_AdjustDscList;
	pAdjustImg = &m_StAdjustImage;

	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );
	if( nSelectedItem < 0)
		return;	

	m_nSelectedNum = nSelectedItem;
	CString strSelect = pList->GetItemText(nSelectedItem, 1);
	DscAdjustInfo* pAdjustInfo = NULL;
	pAdjustInfo = m_pTemplateMgr->GetDscAt(nSelectedItem);

	if(pAdjustInfo->pBmpBits != NULL || pAdjustImg->m_memDC == NULL)
	{
		if(DrawImage() == -1)
			return;

		if(nSelectedItem == 0)
		{
			m_pTemplateAsAuto.CalcAutoAdjustUsingOpticalFlow(cvarrToMat(pAdjustImg->m_pImage));
			m_pTemplateAsAuto.InputAdjustFeaturesFromList(&m_AdjustDscList, m_nSelectedNum);
		}
		if(m_bAutoPoint == TRUE && nSelectedItem != 0)
		{
			m_pTemplateAsAuto.ApplyCalculatedAdjustFeatureToList(pList, nSelectedItem);
		}
		DrawCenterPointAll();			
	}
	else
	{
		if(m_bAutoPoint == TRUE)
		{
			m_AdjustDscList.SetItemText(m_nSelectedNum, 2, _T(""));
			m_AdjustDscList.SetItemText(m_nSelectedNum, 3, _T(""));
			m_AdjustDscList.SetItemText(m_nSelectedNum, 4, _T(""));
			m_AdjustDscList.SetItemText(m_nSelectedNum, 5, _T(""));
			m_AdjustDscList.SetItemText(m_nSelectedNum, 6, _T(""));
			m_AdjustDscList.SetItemText(m_nSelectedNum, 7, _T(""));
			m_AdjustDscList.SetItemText(m_nSelectedNum, 8, _T(""));
			m_AdjustDscList.SetItemText(m_nSelectedNum, 9, _T(""));
		}

		DeleteImage();
	}
}

int CESMTemplateDlg::DrawImage()
{
	CListCtrl* pList = &m_AdjustDscList;	
	CESMTemplateImage* pAdjustImg = &m_StAdjustImage;
	DscAdjustInfo* pAdjustInfo = NULL;	

	UpdateData();
	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );
	if( nSelectedItem < 0)
		return -1;

	m_nSelectedNum = nSelectedItem;
	CString strSelect = pList->GetItemText(nSelectedItem, 1);
	CString strLine3;
	GetDlgItem(IDC_EDIT_TEMPLATE_CENTER)->GetWindowText(strLine3);

	pAdjustInfo = m_pTemplateMgr->GetDscAt(nSelectedItem);

	if(pAdjustInfo->pBmpBits == NULL)
	{
		AfxMessageBox(_T("Wait Loading..."));
		return -1;
	}

	Mat src = m_pTemplateMgr->ApplyAdjustImageUsingCPU(nSelectedItem, m_MarginX, m_MarginY);

	int size = src.total() * src.elemSize();
	pAdjustImg->SetImageBuffer(src.data, src.cols, src.rows, 3, NULL);	

	if(m_bAutoPoint == TRUE)
		m_pTemplateAsAuto.CalcAutoAdjustUsingOpticalFlow(cvarrToMat(pAdjustImg->m_pImage));

	pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
	pAdjustImg->DrawLine(pAdjustInfo->nHeight/2, RGB(0,0,0));
	pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
	pAdjustImg->Redraw();	

	m_nSelectPos = 0;
	return 0;
}

void CESMTemplateDlg::DeleteImage()
{
	CESMTemplateImage* pAdjustImg = &m_StAdjustImage;
	pAdjustImg->m_memDC->FillSolidRect(0,0,m_StAdjustImage.m_nImageWidth,m_StAdjustImage.m_nImageHeight,GetSysColor(COLOR_BTNFACE));
}

void CESMTemplateDlg::LoadData()
{
	m_AdjustDscList.DeleteAllItems();
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();
	for(int i =0; i< m_pTemplateMgr->GetDscCount(); i++)
	{		
		CString strTp;
		strTp.Format(_T("%d"), i + 1);
		m_AdjustDscList.InsertItem(i, strTp);
		strTp = m_pTemplateMgr->GetDscAt(i)->strDscName;
		m_AdjustDscList.SetItemText(i, 1, strTp);
		CString strDSC = m_AdjustDscList.GetItemText(i,1);
				
		m_pTemplateMgr->AddAdjInfo(pMovieMgr->GetAdjustData(strDSC));
		stAdjustInfo adjInfo = m_pTemplateMgr->GetAdjInfoAt(i);

		CESMImgMgr* pImgMgr = new CESMImgMgr();
		pImgMgr->SetMargin(m_MarginX, m_MarginY, adjInfo);
	}

	CString strTemp;
	m_RdImageViewColor = 0;
	m_nRdDetectColor = 0;	
	m_pTemplateMgr->GetResolution(m_nWidth,m_nHeight);

	if( m_hTheadHandle ) 
	{
		CloseHandle(m_hTheadHandle);
		m_hTheadHandle = NULL;
	}

	m_hTheadHandle = (HANDLE) _beginthreadex(NULL, 0, GetMovieDataThread, (void *)this, 0, NULL);

	GetDlgItem(IDC_EDIT_TEMPLATE_SIZE)->SetWindowText(_T("5"));
}

unsigned WINAPI CESMTemplateDlg::GetMovieDataThread(LPVOID param)
{	
	CESMTemplateDlg* pAdjustMgrDlg = (CESMTemplateDlg*)param;
	DscAdjustInfo* pAdjustInfo = NULL;

	//wgkim 170728
	int nMovieIndex = 0;
	int nRealFrameIndex = 0;
	int nShiftFrame = ESMGetSelectedFrameNumber();
	ESMGetMovieIndex(nShiftFrame, nMovieIndex, nRealFrameIndex);

	for(int i =0; i< pAdjustMgrDlg->m_pTemplateMgr->GetDscCount(); i++)
	{
		pAdjustInfo = pAdjustMgrDlg->m_pTemplateMgr->GetDscAt(i);
		FFmpegManager FFmpegMgr(FALSE);
		CString strFolder, strDscId, strDscIp;
		strDscId = pAdjustInfo->strDscName;
		strDscIp = pAdjustInfo->strDscIp;
		strFolder = ESMGetMoviePath(strDscId, nShiftFrame);
		BOOL bReverse = pAdjustInfo->bReverse;
		if( strFolder == _T(""))
		{
			return 0;
		}

		BYTE* pBmpBits = NULL;
		int nWidht =0, nHeight = 0;
		FFmpegMgr.GetCaptureImage(strFolder, &pBmpBits, nRealFrameIndex/*pAdjustMgrDlg->m_frameIndex*/, &nWidht, &nHeight, ESMGetGopSize(),bReverse);
		
		if( pBmpBits != NULL)
		{
			pAdjustInfo->pBmpBits = pBmpBits;
			pAdjustInfo->nWidht = nWidht;
			pAdjustInfo->nHeight = nHeight;
		}

		if(pAdjustMgrDlg->m_bThreadCloseFlag == TRUE)
			return 0;
	}	
	return 0;
}

void CESMTemplateDlg::OnLvnItemchangedAdjust(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	Showimage();
	Invalidate();
	*pResult = 0;
}
void CESMTemplateDlg::DrawCenterPointAll()
{
	CString strTp;
	int nPosX, nPosY;
	COLORREF color = RGB(255, 0, 255);

	for ( int i = 1 ; i < 5 ; i++ )
	{
		nPosX = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2);
		nPosY = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2 + 1);

		if(i == 2)
		{
			int nPosX_2 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2);
			int nPosY_2 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2 + 1);
			int nPosX_1 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-1)*2);
			int nPosY_1 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-1)*2 + 1);

			m_StAdjustImage.DrawLine(nPosX_2, nPosY_2, nPosX_1, nPosY_1, color);
		}

		if(i == 3)
		{
			int nPosX_3 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2);
			int nPosY_3 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2 + 1);
			int nPosX_2 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-1)*2);
			int nPosY_2 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-1)*2 + 1);
			m_StAdjustImage.DrawLine(nPosX_3, nPosY_3, nPosX_2, nPosY_2, color);
		}

		if(i == 4)
		{
			int nPosX_4 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2);
			int nPosY_4 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2 + 1);
			int nPosX_3 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-1)*2);
			int nPosY_3 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-1)*2 + 1);
			int nPosX_2 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-2)*2);
			int nPosY_2 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-2)*2 + 1);
			int nPosX_1 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-3)*2);
			int nPosY_1 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-3)*2 + 1);

			m_StAdjustImage.DrawLine(nPosX_4, nPosY_4, nPosX_2, nPosY_2, color);
			m_StAdjustImage.DrawLine(nPosX_3, nPosY_3, nPosX_1, nPosY_1, color);
			m_StAdjustImage.DrawLine(nPosX_4, nPosY_4, nPosX_1, nPosY_1, color);
			m_StAdjustImage.DrawLine(nPosX_4, nPosY_4, nPosX_3, nPosY_3, color);
		}

		DrawCenterPoint(nPosX, nPosY, i);
	}
}
void CESMTemplateDlg::DrawCenterPoint(int nPosX, int nPosY)
{
	int nCircle = m_StAdjustImage.m_nCircleSize;

	if ( nPosX != 0 && nPosY != 0 )
	{
		CRect rtOutLine(nPosX-nCircle,nPosY-nCircle,nPosX+nCircle,nPosY+nCircle);
		m_StAdjustImage.DrawRect(rtOutLine, RGB(0,255,0));

		CRect rtCenter(nPosX-1,nPosY-1,nPosX+1,nPosY+1);
		m_StAdjustImage.DrawRect(rtCenter, RGB(255,0,0));
	}
}

void CESMTemplateDlg::DrawCenterPoint(int nPosX, int nPosY, int nIndex)
{
	int nCircle = m_StAdjustImage.m_nCircleSize;

	if ( nPosX != 0 && nPosY != 0 )
	{
		CRect rtOutLine(nPosX-nCircle,nPosY-nCircle,nPosX+nCircle,nPosY+nCircle);
		m_StAdjustImage.DrawRect(rtOutLine, Coloring(nIndex));

		CRect rtCenter(nPosX-1,nPosY-1,nPosX+1,nPosY+1);
		m_StAdjustImage.DrawRect(rtCenter, Coloring(nIndex));
	}
}

COLORREF CESMTemplateDlg::Coloring(int index)
{
	int red = 0; int green = 0; int blue = 0;

	if(index % 6 == 0)		red		= 255;
	if(index % 6 - 1 == 0)	green	= 255;
	if(index % 6 - 2 == 0)	blue	= 255;
	if(index % 6 - 3 == 0) {red		= 255; green	= 255;}
	if(index % 6 - 4 == 0) {green	= 255; blue		= 255;}
	if(index % 6 - 5 == 0) {blue	= 255; red		= 255;}

	return RGB(blue, green, red);
}

void CESMTemplateDlg::OnRButtonDown(UINT nFlags, CPoint point)
{	
	CListCtrl* pList;
	CESMTemplateImage* pAdjustImg;
		
	pList = &m_AdjustDscList;
	pAdjustImg = &m_StAdjustImage;

	DrawImage();

	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );

	CString LocStr;
	m_ctrlCheckPoint.GetLBText(m_ctrlCheckPoint.GetCurSel(),LocStr);

	if(LocStr == "4th") m_nSelectPos = 3;
	else if(LocStr == "3rd") m_nSelectPos = 2;
	else if(LocStr == "1st") m_nSelectPos = 0;
	else if(LocStr == "2nd") m_nSelectPos = 1; 	

	CString strSize;
	CString strText;
	GetDlgItem(IDC_EDIT_TEMPLATE_SIZE)->GetWindowText(strSize);
	pAdjustImg->m_nCircleSize =  _ttoi(strSize) * 4;

	FindExactPosition(m_nPosX,m_nPosY);	

	if(LocStr == "None")
	{	
		if(nSelectedItem != 0 && m_bAutoPoint == TRUE)
		{
			int selectedCol = 0;
			double minDistance = sqrt((m_pTemplateMgr->GetWidth()-0)*(m_pTemplateMgr->GetWidth()-0) 
									- (m_pTemplateMgr->GetHeight()-0)*(m_pTemplateMgr->GetHeight()-0));

			for(int i = 0; i < 4; i++)
			{
				int nullCheck = -1;
				double nPosX, nPosY;
				double currentDisance = 0.;

				while(1)
				{
					nPosX = getDSCListItem(&m_AdjustDscList, nSelectedItem + nullCheck, i*2 + 2);
					nPosY = getDSCListItem(&m_AdjustDscList, nSelectedItem + nullCheck, i*2 + 3);

					if(nPosX != 0 || nPosY != 0)
					{
						nullCheck = -1;
						break;
					}
					nullCheck--;
				}
				currentDisance = sqrt((double)((m_nPosX-nPosX)*(m_nPosX-nPosX) + (m_nPosY-nPosY)*(m_nPosY-nPosY)));

				if(currentDisance < minDistance)
				{
					selectedCol = i;
					minDistance = currentDisance;
				}
			}
			setDscListItem(pList, nSelectedItem, selectedCol * 2 + 2, (double)m_nPosX);
			setDscListItem(pList, nSelectedItem, selectedCol * 2 + 3, (double)m_nPosY);

			m_pTemplateAsAuto.InputAdjustFeaturesFromList(pList, nSelectedItem);
		}
		else
		{	
			static int pos = 0;

			setDscListItem(pList, nSelectedItem, pos%4 * 2 + 2, (double)m_nPosX);
			setDscListItem(pList, nSelectedItem, pos%4 * 2 + 3, (double)m_nPosY);

			pos++;

			if(m_bAutoPoint == TRUE)
				m_pTemplateAsAuto.InputAdjustFeaturesFromList(pList, nSelectedItem);
		}
	}

	else
	{
		strText.Format(_T("%d"), m_nPosX);
		setDscListItem(pList, nSelectedItem, m_nSelectPos * 2 + 2, (double)m_nPosX);		
		
		strText.Format(_T("%d"), m_nPosY);
		setDscListItem(pList, nSelectedItem, m_nSelectPos * 2 + 3, (double)m_nPosY);		
		//SetItemText(nSelectedItem, 1 + m_nSelectPos * 2 , strText);
		m_pTemplateAsAuto.InputAdjustFeaturesFromList(&m_AdjustDscList, m_nSelectedNum);
				
		if( m_bAutoPointMove == TRUE)
		{
			int nAllItem = m_AdjustDscList.GetItemCount();
			if(nAllItem > nSelectedItem+1)
			{ 
				ESMLog(5,_T("Change Next Item"));

				int nSelectedItem = m_AdjustDscList.GetNextItem( -1, LVNI_SELECTED );
				m_AdjustDscList.SetItemState(nSelectedItem+1,LVIS_SELECTED,  LVIS_SELECTED);
				m_AdjustDscList.SetItemState(nSelectedItem, 0 ,  LVIS_SELECTED);
				m_AdjustDscList.SetFocus();
				m_nSelectPos = 0;		
			}		
		}
		
		UpdateData();		
	}

	DrawCenterPointAll();

	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	SavePointData(strFileName);
	CDialogEx::OnRButtonDown(nFlags, point);
}

void CESMTemplateDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	CESMTemplateImage* pAdjustImg;
	pAdjustImg = &m_StAdjustImage;

	CString strPos;
	CRect rect;
	pAdjustImg->GetWindowRect(&rect);
	ScreenToClient(&rect);
	int nPos = 0;
	if( point.x - rect.left < 0 || point.x > rect.right || point.y - rect.top < 0 || point.y > rect.bottom)
	{
		CDialogEx::OnMouseMove(nFlags, point);
		return;
	}

	//-- 2014-08-31 hongsu@esmlab.com
	//-- Get Image Position from Mouse Point
	CPoint ptImage;
	ptImage = GetPointFromMouse(point);

	strPos.Format(_T("%d"), ptImage.x);
	m_nPosX = ptImage.x - 1;
	GetDlgItem(IDC_TEMPLATE_MOUSEPOSX)->SetWindowText(strPos);

	strPos.Format(_T("%d"), ptImage.y);
	m_nPosY = ptImage.y - 1;
	GetDlgItem(IDC_TEMPLATE_MOUSEPOSY)->SetWindowText(strPos);

	//ImageOverlay
	BOOL bLeftButton = nFlags & MK_LBUTTON;
	if(pAdjustImg == &m_StAdjustImage && bLeftButton)
	{
		if(pAdjustImg->GetUsingRuler())
		{
			DrawImage();
			pAdjustImg->DrawRuler(m_nPosX, m_nPosY);
			pAdjustImg->Redraw();
			return;
		}
	}

		pAdjustImg->OnMouseMove(nFlags, point);

	CDialogEx::OnMouseMove(nFlags, point);
}

BOOL CESMTemplateDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CESMTemplateImage* pAdjustImg;
	int nCount;
	nCount = ADJUST_IMAGE;

	pAdjustImg = &m_StAdjustImage;

	//ImageOverlay
	if(pAdjustImg->GetUsingRuler())
	{
		DrawImage();
		pAdjustImg->RulerColorChange();
		pAdjustImg->Redraw();
		return CDialogEx::OnMouseWheel(nFlags, zDelta, pt);
	}

	CRect rtWindow;
	GetWindowRect(rtWindow);

	pt.x -= rtWindow.left;
	pt.y -= rtWindow.top;

	CPoint ptImage;
	ptImage = GetPointFromMouse(pt);

	if( zDelta > 0)
	{
		if(  pAdjustImg->GetMultiple() <= 0.2)
			return TRUE;
		pAdjustImg->ImageZoom(ptImage, TRUE);
	}
	else
		pAdjustImg->ImageZoom(ptImage, FALSE);

	CString strTp;
	strTp.Format(_T("%.03lf"), pAdjustImg->GetMultiple());
	GetDlgItem(IDC_TEMPLATE_IMAGESIZE)->SetWindowText(strTp);

	return CDialogEx::OnMouseWheel(nFlags, zDelta, pt);
}

CPoint CESMTemplateDlg::GetPointFromMouse(CPoint pt)
{
	CESMTemplateImage* pAdjustImg;
	pAdjustImg = &m_StAdjustImage;

	CRect rect;
	pAdjustImg->GetWindowRect(&rect);
	ScreenToClient(&rect);

	CPoint ptRet;
	pt.x = pt.x - rect.left;
	pt.y = pt.y - rect.top;
	
	CPoint ptImage = pAdjustImg->GetImagePos();
	ptRet.x = (int)(ptImage.x * pAdjustImg->GetMultiple() + pt.x * pAdjustImg->GetMultiple());
	ptRet.y = (int)(ptImage.y * pAdjustImg->GetMultiple() + pt.y * pAdjustImg->GetMultiple());

	//ESMLog(5, _T("Moust Point x:%d y:%d"), ptRet.x, ptRet.y);
	return ptRet;
}

void CESMTemplateDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	CESMTemplateImage* pAdjustImg;
	pAdjustImg= &m_StAdjustImage;

	//ImageOverlay
	if(pAdjustImg == &m_StAdjustImage)
	{
		if(pAdjustImg->GetUsingRuler())
		{
			pAdjustImg->StartDrawRuler(m_nPosX, m_nPosY);
			return;
		}
	}

	int nCount;
	pAdjustImg->OnLButtonDown(nFlags, point);
	pAdjustImg->SetFocus();

	CDialogEx::OnLButtonDown(nFlags, point);
}

void CESMTemplateDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	CESMTemplateImage* pAdjustImg;
	pAdjustImg = &m_StAdjustImage;

	//ImageOverlay
	if(pAdjustImg == &m_StAdjustImage)
	{
		if(pAdjustImg->GetUsingRuler())
		{
			pAdjustImg->EndDrawRuler();
			return;
		}
	}

	pAdjustImg->OnLButtonUp(nFlags, point);

	CDialogEx::OnLButtonUp(nFlags, point);
}

void CESMTemplateDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	
	CDialogEx::OnLButtonDblClk(nFlags, point);
}

void CESMTemplateDlg::FindExactPosition(int& nX, int& nY)
{
	//-- 2014-08-30 hongsu@esmlab.com
	//-- Load Image 
	CESMTemplateImage* pAdjustImg;
	pAdjustImg = &m_StAdjustImage;

	UpdateData();
	pAdjustImg->FindExactPosition(nX, nY);
}

void CESMTemplateDlg::OnBnClickedBtnAdjustPointReset()
{
	CESMFileOperation fo;
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	fo.Delete(strFileName);
	CString strData = _T("");

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);
		m_AdjustDscList.SetItemText(i, 2, strData);
		m_AdjustDscList.SetItemText(i, 3, strData);
		m_AdjustDscList.SetItemText(i, 4, strData);
		m_AdjustDscList.SetItemText(i, 5, strData);
		m_AdjustDscList.SetItemText(i, 6, strData);
		m_AdjustDscList.SetItemText(i, 7, strData);
		m_AdjustDscList.SetItemText(i, 8, strData);
		m_AdjustDscList.SetItemText(i, 9, strData);
	}
}

void CESMTemplateDlg::OnBnClickedBtnAutoFind()
{
	if(((CButton*)GetDlgItem(IDC_CHECK_AUTOPOINT))->GetCheck())
	{
		m_pTemplateAsAuto.InputAdjustFeaturesFromList(&m_AdjustDscList, m_nSelectedNum);
	}
	else
	{

	}
}

void CESMTemplateDlg::OnBnClickedBtnAutoMove()
{
	if(((CButton*)GetDlgItem(IDC_CHECK_AUTOPOINT_MOVE))->GetCheck())
	{
		
	}
	else
	{
		
	}
}

void CESMTemplateDlg::OnBnClickedBtn2ndSavePoint()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));
	CString szFilter = _T("Point List (*.pta)|*.pta|");

#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  	
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = _T("Point Save");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();	
		if( strFileName.Right(4) != _T(".pta"))
			strFileName = strFileName + _T(".pta");
		SavePointData(strFileName);


		//Save Image Data
		CString strAdjustFolder;
		strAdjustFolder.Format(_T("%s\\img"),ESMGetPath(ESM_PATH_SETUP));
		CreateDirectory(strAdjustFolder, NULL);
	}
}

void CESMTemplateDlg::OnBnClickedBtn2ndLoadPoint()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));

	CString szFilter = _T("Point List (*.pta)|*.pta|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = _T("Point Load");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();
		LoadPointData(strFileName);	
	}
}

BOOL CESMTemplateDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
		if(pMsg->message == WM_KEYDOWN)
		{
			if (pMsg->wParam == 'o' || pMsg->wParam == 'O')
			{
				m_StAdjustImage.SetUsingRuler(!m_StAdjustImage.GetUsingRuler());

				if(!m_StAdjustImage.GetUsingRuler())
				{
					DrawImage();
					m_StAdjustImage.ClearDrawRuler();
				}
			}
			switch(pMsg->wParam)
			{		
				case VK_DELETE://170615 - added by hjcho
				{
					DeleteSelectedList();
				}
				break;
			}
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}
void CESMTemplateDlg::DeleteSelectedList()
{
	int nSelectedCount = m_AdjustDscList.GetSelectedCount();

	if(nSelectedCount == 1)
	{
		POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();
		int nDeleteItem = m_AdjustDscList.GetNextSelectedItem(pos);

		DeleteSelectItem(nDeleteItem);
	}
	else
	{
		POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();

		while(pos)
		{			
			int nDeleteItem = m_AdjustDscList.GetNextSelectedItem(pos);
			DeleteSelectItem(nDeleteItem);
		}
	}
}
void CESMTemplateDlg::DeleteSelectItem(int nIndex)
{
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);

	//선택 아이템 삭제 ( 2,3,4,5,6,7,8,9 )
	for(int i = 2; i < 10 ; i++)
	{
		m_AdjustDscList.SetItemText(nIndex,i,NULL);
	}
	SavePointData(strFileName);
}

void CESMTemplateDlg::OnBnClickedBtnYPointModifyAll()
{
	CESMFileOperation fo;
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	fo.Delete(strFileName);

	UpdateData(TRUE);
	CString strData = _T("");

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);

		int nPosY = 0;
		
		nPosY = getDSCListItem(&m_AdjustDscList, i, 3) + m_nYPointModifyValue;
		strData.Format(_T("%d"), nPosY);
		m_AdjustDscList.SetItemText(i, 3, strData);

		nPosY = getDSCListItem(&m_AdjustDscList, i, 5) + m_nYPointModifyValue;
		strData.Format(_T("%d"), nPosY);
		m_AdjustDscList.SetItemText(i, 5, strData);

		nPosY = getDSCListItem(&m_AdjustDscList, i, 7) + m_nYPointModifyValue;
		strData.Format(_T("%d"), nPosY);
		m_AdjustDscList.SetItemText(i, 7, strData);

		nPosY = getDSCListItem(&m_AdjustDscList, i, 9) + m_nYPointModifyValue;
		strData.Format(_T("%d"), nPosY);
		m_AdjustDscList.SetItemText(i, 9, strData);
	}

	SavePointData(strFileName);
	Showimage();
}

HBRUSH CESMTemplateDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	hbr = (HBRUSH)m_background;	

	switch(nCtlColor)
	{
	case CTLCOLOR_STATIC:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			//pDC->SetBkColor(RGB(56, 56, 56));  // 글자 배경색 변경
			pDC->SetBkMode(TRANSPARENT);
			//return (HBRUSH)m_brush;
		}
		break;
	case CTLCOLOR_EDIT:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			pDC->SetBkColor(RGB(0, 0 ,0));
			//hbr = (HBRUSH)(hbr.GetSafeHandle());  
		}
		break;
	}
	return hbr;
}