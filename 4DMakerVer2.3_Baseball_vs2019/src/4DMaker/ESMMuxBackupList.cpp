#include "stdafx.h"
#include "ESMMuxBackupList.h"

CESMMuxBackUpList::CESMMuxBackUpList()
{

}
CESMMuxBackUpList::~CESMMuxBackUpList()
{

}
void CESMMuxBackUpList::Init(CESMMuxBackUpDlg* pParent)
{
	m_pMuxDlg = pParent;

	SetExtendedStyle(LVS_EX_DOUBLEBUFFER | LVS_EX_FULLROWSELECT );

	InsertColumn(LIST_NUM		 ,_T("#")    ,LVCFMT_CENTER,10);
	InsertColumn(LIST_4DM		,_T("4DM")	 ,LVCFMT_CENTER,150);	
	InsertColumn(LIST_4DMINDEX	,_T("Idx")	 ,LVCFMT_CENTER,30);
	InsertColumn(LIST_SELTIME	,_T("TIME")	 ,LVCFMT_CENTER,30);
	InsertColumn(LIST_TEMPLATE	,_T("TEM")	 ,LVCFMT_CENTER,30);
	InsertColumn(LIST_DBINFO	,_T("INFO")	 ,LVCFMT_CENTER,30);

	DragAcceptFiles();
}

BEGIN_MESSAGE_MAP(CESMMuxBackUpList, CListCtrl)
	ON_WM_DROPFILES()
END_MESSAGE_MAP()

void CESMMuxBackUpList::OnDropFiles(HDROP hDropInfo)
{
	int NumDropFiles = 0;
	char FilePath[MAX_PATH]; 

	NumDropFiles = DragQueryFileA(hDropInfo,0xFFFFFFFF,FilePath,MAX_PATH);

	CString strFileName,strFilePath,strEx;

	for(int i = 0 ; i < NumDropFiles; i++)
	{
		DragQueryFileA(hDropInfo,i,FilePath,MAX_PATH);

		strFilePath = (LPSTR)FilePath;

		strFileName = strFilePath.Right(strFilePath.GetLength() - strFilePath.ReverseFind(_T('\\')) -1);
		strEx = PathFindExtension(strFilePath);//strFilePath.Right(5);

		if(strEx == _T(".mp4") || strEx == _T(".avi") || strEx == _T(".wmv"))
			AddFiles(strFileName);
		//strFileName.Format(_T("%S"), (LPSTR)FilePath);
	}

	CListCtrl::OnDropFiles(hDropInfo);
}
void CESMMuxBackUpList::AddFiles(CString strPath)
{
	int nTotal = GetItemCount();
	CString strNum;
	strNum.Format(_T("%d"),nTotal);

	InsertItem(nTotal, strNum);
	SetItem(nTotal,LIST_NUM		,LVIF_TEXT,strNum,0,0,0,NULL);
	SetItem(nTotal,LIST_4DM		,LVIF_TEXT,strPath,0,0,0,NULL);
	SetItem(nTotal,LIST_4DMINDEX,LVIF_TEXT,_T("0"),0,0,0,NULL);
}