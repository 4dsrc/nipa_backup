// ESMOptSyncDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMOptSyncDlg.h"
#include "afxdialogex.h"


// CESMOptSyncDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMOptSyncDlg, CDialog)

CESMOptSyncDlg::CESMOptSyncDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CESMOptSyncDlg::IDD, pParent)
{

}

CESMOptSyncDlg::~CESMOptSyncDlg()
{
}

void CESMOptSyncDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SA_SYNC_TIME, m_ctrlPCSyncTime);
	//DDX_Control(pDX, IDC_AC_SYNC_TIME, m_ctrlDSCSyncTime);
	//DDX_Control(pDX, IDC_AC_WAIT_TIME, m_ctrlDSCWaitTime);
	DDX_Control(pDX, IDC_RESYNC_REC_WAIT_TIME, m_ctrlResyncRecWaitTime);	
}


BEGIN_MESSAGE_MAP(CESMOptSyncDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_OK, &CESMOptSyncDlg::OnBnClickedBtnOk)
	ON_BN_CLICKED(IDC_BTN_CANCEL, &CESMOptSyncDlg::OnBnClickedBtnCancel)
	ON_BN_CLICKED(IDC_BTN_DEFAULT_SET, &CESMOptSyncDlg::OnBnClickedBtnDefaultSet)
END_MESSAGE_MAP()

void CESMOptSyncDlg::InitProp(ESM4DMaker* Opt)
{
	m_pOpt = Opt;
	
}


// CESMOptSyncDlg 메시지 처리기입니다.


void CESMOptSyncDlg::OnBnClickedBtnOk()
{
	CString strData;

	// sync set value 통합.
#if 0
	m_ctrlPCSyncTime.GetWindowText(strData);
	m_pOpt->nPCSyncTime = _ttoi(strData);

	m_ctrlDSCSyncTime.GetWindowText(strData);
	m_pOpt->nDSCSyncTime = _ttoi(strData);

	m_ctrlDSCWaitTime.GetWindowText(strData);
	m_pOpt->nDSCSyncWaitTime = _ttoi(strData);
#else
	m_ctrlPCSyncTime.GetWindowText(strData);
	m_pOpt->nPCSyncTime = _ttoi(strData);
	m_pOpt->nDSCSyncTime = _ttoi(strData);
	m_pOpt->nDSCSyncWaitTime = _ttoi(strData);
#endif

	m_ctrlResyncRecWaitTime.GetWindowText(strData);
	m_pOpt->nResyncRecWaitTime= _ttoi(strData);
	OnOK();
}


void CESMOptSyncDlg::OnBnClickedBtnCancel()
{
	OnOK();
}


BOOL CESMOptSyncDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString strData;

	strData.Format(_T("%d"),m_pOpt->nPCSyncTime);
	m_ctrlPCSyncTime.SetWindowText(strData);

#if 0
	strData.Format(_T("%d"),m_pOpt->nDSCSyncTime);
	m_ctrlDSCSyncTime.SetWindowText(strData);

	strData.Format(_T("%d"),m_pOpt->nDSCSyncWaitTime);
	m_ctrlDSCWaitTime.SetWindowText(strData);
#endif

	strData.Format(_T("%d"),m_pOpt->nResyncRecWaitTime);
	m_ctrlResyncRecWaitTime.SetWindowText(strData);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CESMOptSyncDlg::OnBnClickedBtnDefaultSet()
{
	CString strData;

	strData.Format(_T("30000"));
	m_ctrlPCSyncTime.SetWindowText(strData);

	strData.Format(_T("30000"));
	m_ctrlResyncRecWaitTime.SetWindowText(strData);
}
