#pragma once


// CESMGroupBox

class CESMGroupBox : public CStatic
{
	DECLARE_DYNAMIC(CESMGroupBox)

public:
	CESMGroupBox();
	virtual ~CESMGroupBox();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNcPaint();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	CBrush m_brush;
};


