#pragma once

#include <vector>
#include "opencv2/opencv_modules.hpp"
#include "opencv2/opencv.hpp"

#include "ESMFunc.h"
#include "ESMUtil.h"
#include "ESMIni.h"
#include "FFmpegManager.h"
#include "ESMObjectFrame.h"

#include "ESMTemplateImage.h"

class CESMTemplateMgr
{
public:
	CESMTemplateMgr(void);
	~CESMTemplateMgr(void);

public:
	enum VIEW_DIRECT
	{
		VIEW_DIRECT_SAME		= 0x0,
		VIEW_DIRECT_LOW_HIGH	= 0x1,
		VIEW_DIRECT_HIGH_LOW	= 0x2,
	}; vector<VIEW_DIRECT>		m_vecViewDirect;
		
	struct TemplatePoint
	{
		CString strDscName;
		CString strDscIp;
		Point2f centerPointOfRotation;
		Point2f viewPoint;
	};

protected:
	double	m_dWidth;
	double	m_dHeight;
public:
	void	SetMovieSIze(double dWidth, double dHeight);
	double	GetWidth()	{ return m_dWidth;};
	double	GetHeight()	{ return m_dHeight;};

protected:
	vector<Mat>				m_arrHomography;
	vector<vector<Point2f>> m_arrSquares;
	
	BOOL	isSquareValid(int index);
	BOOL	isArrSquareValid();
	Mat		calcHomography(int index);
	void	calcPoint(Mat& homography, vector<Point2f>& currentPoint);
	void	setArrHomography();
	void	pushArrSquares(vector<Point2f> inputPoints);
	void	setSquareForReverseMode(vector<Point2f>& square);

public:
	BOOL	IsTemplatePointValid();
	void	ClearArrSquares();
	void	ApplyHomographiesFromArrSquares(vector<vector<Point2f>> squares);

private:
	vector<TemplatePoint>	m_arrTemplatePoint;
	Point2f					m_nViewPoint;
	Point2f					m_nCenterPoint;

	void			setViewPointsInArrTemplatePoint(int dscIdIndex, Point2f viewPoint);

public:
	void			ClearArrTemplatePoint();
	void			SetArrTemplatePoint();		
	void			SetArrTemplatePoint(CString strDscId, Point2f centerPoint, Point2f viewPoint);	
	TemplatePoint	GetTemplatePoint(int index);
	void			SetViewPoint(Point2f viewPoint);
	void			SetCenterPoint(Point2f centerPoint);
	void			SetDscIndex(CString dscIndex);
	void			LoadSquarePointData(CString strFileName);
	BOOL			IsArrTemplatePointValid();	

private:
	vector<DscAdjustInfo*> m_ArrDscInfo;
public:
	void			AddDscInfo(CString strDscId, CString strDscIp,BOOL bReverse = FALSE);
	void			GetDscInfo(vector<DscAdjustInfo*>** pArrDscInfo);
	int				GetDscCount();
	void			DscClear();
	DscAdjustInfo*	GetDscAt(int nIndex);
	int				GetDscIndexFrom(CString strDscId);
	
public:
	BOOL	SearchDetectPoint(IplImage* pNewGray, CSize MinSize, CSize MaxSize, vector<CRect>* vecDetectAreas, COLORREF clrDetectColor);
	BOOL	SearchPoint(IplImage*	pNewGray, vector<CPoint>* arrBeginPoint, vector<CPoint>* arrPointRange, vector<CPoint>* pArrPoint);
	BOOL	CalcAdjustData(DscAdjustInfo* pAdjustInfo, int nTargetLenth, int nZoom);
	BOOL	GetSearchDetectPoint(DscAdjustInfo* pAdjustInfo, int nThreshold, CSize minDetectSize, CSize maxDetectSize, int nPointGapMin, COLORREF clrDetectColor);	
	void	GetResolution(int &nWidth, int &nHeight);
	double	GetDistanceData(CString strDSCId);
	int		GetZoomData(CString strDSCId);

protected:
	vector<stAdjustInfo> m_arrAdjInfo;
public:
	void			AddAdjInfo(stAdjustInfo adjustInfo);
	stAdjustInfo	GetAdjInfoAt(int index);
	void			ClearAdjInfo();
	Mat				ApplyAdjustImageUsingCPU(int nSelectedItem, int marginX, int marginY);

protected:
	int		Round(double dData);
	void	GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle);
	void	GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY);
	void	GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY);
	void	CpuMakeMargin(Mat* gMat, int nX, int nY);
	void	CpuMoveImage(Mat* gMat, int nX, int nY);
	void	CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle);	
	void	affinetransform(Mat img,cv::Point a,cv::Point b, cv::Point c, Point2f srcTri[3],Mat dst);

//Template Modity For Target Disappearing From the Screen.
public:
	void	TemplateModifyForTargetDisappearingFromScreen(vector<TEMPLATE_STRUCT>* vecVMCCTemplate);
	
//test
public:
	vector<CESMObjectFrame*> m_arrExistFrame;
	vector<CString> m_arrStrDscExistFrame;

private:
	double m_dMaxOverBoundary;

public:
	void CalcOverBoundary(int nXchange, int nYchange, int nChangeRatio, CString dscIndex);
	double GetRecalculatedZoomSize(int zoom);
	void RecalculatedTemplateZoom();

protected:
	int m_nViewPointX;
	int m_nViewPointY;
	int m_nZoom;

	BOOL m_bIsOverBoundary;

	Point2f m_nModifiedViewPoint;
	Point2f m_nModifiedCenterPoint;

public:
	CString m_strDscIndex;
	void CheckPointBoundary();
	void RecalculatedCenterPoint();
	
	void SetOverBoundaryFlag(BOOL flag);
	BOOL IsOverBoundary();

	Point2f GetModifiedViewPoint();
	Point2f GetModifiedCenterPoint();

	//wgkim 180920
public:
	void ConvertArrSquaresUsingAdjustData(vector<vector<Point2f>> &squares);
};
