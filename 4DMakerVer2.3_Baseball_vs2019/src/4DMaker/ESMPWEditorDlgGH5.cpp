// ESMPWEditorDlgGH5.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMPWEditorDlgGH5.h"
#include "afxdialogex.h"
#include "ESMPWEditorDlgGH5WBAD.h"


// CESMPWEditorDlgGH5 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMPWEditorDlgGH5, CDialogEx)

CESMPWEditorDlgGH5::CESMPWEditorDlgGH5(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMPWEditorDlgGH5::IDD, pParent)
{
	m_pItem = NULL;
	m_WBAdjustDlg = NULL;
	m_photoStyle = PHOTO_STYLE_Unknown;
}

CESMPWEditorDlgGH5::~CESMPWEditorDlgGH5()
{
	if (m_WBAdjustDlg)
	{
		delete m_WBAdjustDlg;
		m_WBAdjustDlg = NULL;
	}
}

void CESMPWEditorDlgGH5::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER_WB_AB, m_ctrlWBAB);
	DDX_Control(pDX, IDC_SLIDER_WB_MG, m_ctrlWBMG);
	DDX_Control(pDX, IDC_VALUE_WB_AB, m_crtlValueAB);
	DDX_Control(pDX, IDC_VALUE_WB_MG, m_crtlValueMG);
	DDX_Control(pDX, IDC_SLIDER_CONTRAST, m_ctrlContrast);
	DDX_Control(pDX, IDC_SLIDER_SHARPNESS, m_ctrlSharpness);
	DDX_Control(pDX, IDC_SLIDER_NOISE_REDUCTION, m_ctrlNoiseReduction);
	DDX_Control(pDX, IDC_SLIDER_SATURATION, m_ctrlSaturation);
	DDX_Control(pDX, IDC_SLIDER_HUE, m_ctrlHue);
	DDX_Control(pDX, IDC_SLIDER_FILTER_EFFECT, m_ctrlFilterEffect);
}


BEGIN_MESSAGE_MAP(CESMPWEditorDlgGH5, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_OK, &CESMPWEditorDlgGH5::OnBnClickedButtonOk)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_WB_AB, &CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderWbAb)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_WB_MG, &CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderWbMg)	
	ON_MESSAGE(WM_ESM_WB_ADJUST, &CESMPWEditorDlgGH5::OnEsmWbAdjust)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_CONTRAST, &CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderContrast)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_SHARPNESS, &CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderSharpness)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_NOISE_REDUCTION, &CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderNoiseReduction)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_SATURATION, &CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderSaturation)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_HUE, &CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderHue)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_FILTER_EFFECT, &CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderFilterEffect)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BTN_APPLY_ALL, &CESMPWEditorDlgGH5::OnBnClickedBtnApplyAll)
	ON_BN_CLICKED(IDC_BTN_RESET, &CESMPWEditorDlgGH5::OnBnClickedBtnReset)
END_MESSAGE_MAP()


// CESMPWEditorDlgGH5 메시지 처리기입니다.

void CESMPWEditorDlgGH5::OnBnClickedBtnApplyAll()
{	
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	CString strValue;

	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	//for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
		{
			SendSdiMsg(pItem, PTP_DPC_WB_ADJUST_AB, GetPosAB());
			SendSdiMsg(pItem, PTP_DPC_WB_ADJUST_MG, GetPosMG());

			if(m_ctrlContrast.IsWindowEnabled())
				SendSdiMsg(pItem, PTP_DPC_PS_CONTRAST, GetPosPSValue(PTP_DPC_PS_CONTRAST));

			if(m_ctrlSharpness.IsWindowEnabled())
				SendSdiMsg(pItem, PTP_DPC_PS_SHARPNESS, GetPosPSValue(PTP_DPC_PS_SHARPNESS));

			if(m_ctrlNoiseReduction.IsWindowEnabled())
				SendSdiMsg(pItem, PTP_DPC_PS_NOISE_REDUCTION, GetPosPSValue(PTP_DPC_PS_NOISE_REDUCTION));

			if(m_ctrlSaturation.IsWindowEnabled())
				SendSdiMsg(pItem, PTP_DPC_PS_SATURATION, GetPosPSValue(PTP_DPC_PS_SATURATION));

			if(m_ctrlHue.IsWindowEnabled())
				SendSdiMsg(pItem, PTP_DPC_PS_HUE, GetPosPSValue(PTP_DPC_PS_HUE));

			if(m_ctrlFilterEffect.IsWindowEnabled())
				SendSdiMsg(pItem, PTP_DPC_PS_FILTER_EFFECT, GetPosPSValue(PTP_DPC_PS_FILTER_EFFECT));

			ESMEvent* pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
			pSdiMsg->nParam1 = HIDDEN_COMMAND_BANK_SAVE;
			pSdiMsg->nParam2 = PTP_VALUE_NONE;
			pItem->SdiAddMsg(pSdiMsg);
		}
	}
}

void CESMPWEditorDlgGH5::OnBnClickedButtonOk()
{	
	if (!m_pItem)
		return;

	SendSdiMsg(m_pItem, PTP_DPC_WB_ADJUST_AB, GetPosAB());
	SendSdiMsg(m_pItem, PTP_DPC_WB_ADJUST_MG, GetPosMG());

	if (m_ctrlContrast.IsWindowEnabled())
		SendSdiMsg(m_pItem, PTP_DPC_PS_CONTRAST, GetPosPSValue(PTP_DPC_PS_CONTRAST));
	if (m_ctrlSharpness.IsWindowEnabled())
		SendSdiMsg(m_pItem, PTP_DPC_PS_SHARPNESS, GetPosPSValue(PTP_DPC_PS_SHARPNESS));
	if (m_ctrlNoiseReduction.IsWindowEnabled())
		SendSdiMsg(m_pItem, PTP_DPC_PS_NOISE_REDUCTION, GetPosPSValue(PTP_DPC_PS_NOISE_REDUCTION));
	if (m_ctrlSaturation.IsWindowEnabled())
		SendSdiMsg(m_pItem, PTP_DPC_PS_SATURATION, GetPosPSValue(PTP_DPC_PS_SATURATION));
	if (m_ctrlHue.IsWindowEnabled())
		SendSdiMsg(m_pItem, PTP_DPC_PS_HUE, GetPosPSValue(PTP_DPC_PS_HUE));
	if (m_ctrlFilterEffect.IsWindowEnabled())
		SendSdiMsg(m_pItem, PTP_DPC_PS_FILTER_EFFECT, GetPosPSValue(PTP_DPC_PS_FILTER_EFFECT));

	ESMEvent* pSdiMsg = new ESMEvent;
	pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
	pSdiMsg->nParam1 = HIDDEN_COMMAND_BANK_SAVE;
	pSdiMsg->nParam2 = PTP_VALUE_NONE;
	m_pItem->SdiAddMsg(pSdiMsg);
}

void CESMPWEditorDlgGH5::SetInfo(CDSCItem* pItem, CESMPWEditorDlgSetInfo info)
{
	m_pItem = pItem;

	m_setInfo = info;
	m_photoStyle = (PHOTO_STYLE)info.nPS;
}

int CESMPWEditorDlgGH5::GetPosPSValue( int nType )
{
	int nRtn = 0;

	int nPSType = 0;
	CSliderCtrl* ctrlTemp;

	switch (nType)
	{
	case PTP_DPC_PS_CONTRAST		:		ctrlTemp = &m_ctrlContrast;			break;
	case PTP_DPC_PS_SHARPNESS		:		ctrlTemp = &m_ctrlSharpness;		break;
	case PTP_DPC_PS_NOISE_REDUCTION	:		ctrlTemp = &m_ctrlNoiseReduction;	break;
	case PTP_DPC_PS_SATURATION		:		ctrlTemp = &m_ctrlSaturation;		break;
	case PTP_DPC_PS_HUE				:		ctrlTemp = &m_ctrlHue;				break;
	case PTP_DPC_PS_FILTER_EFFECT	:		ctrlTemp = &m_ctrlFilterEffect;		break;
	}

	if (nType != PTP_DPC_PS_FILTER_EFFECT )
	{
		switch (ctrlTemp->GetPos())
		{
		case -5:	nRtn = PS_VALUE_M_5;	break;
		case -4:	nRtn = PS_VALUE_M_4;	break;
		case -3:	nRtn = PS_VALUE_M_3;	break;
		case -2:	nRtn = PS_VALUE_M_2;	break;
		case -1:	nRtn = PS_VALUE_M_1;	break;
		case  0:	nRtn = PS_VALUE_0;		break;
		case  1:	nRtn = PS_VALUE_P_1;	break;
		case  2:	nRtn = PS_VALUE_P_2;	break;
		case  3:	nRtn = PS_VALUE_P_3;	break;
		case  4:	nRtn = PS_VALUE_P_4;	break;
		case  5:	nRtn = PS_VALUE_P_5;	break;
		}
	}
	else
	{
		nRtn = ctrlTemp->GetPos();
	}

	return nRtn;
}

int CESMPWEditorDlgGH5::GetPosAB()
{
	int nRtn = 0;
	switch (m_ctrlWBAB.GetPos())
	{
	case -9:	nRtn = WB_ADJUST_AB_A9;		break;
	case -8:	nRtn = WB_ADJUST_AB_A8;		break;
	case -7:	nRtn = WB_ADJUST_AB_A7;		break;
	case -6:	nRtn = WB_ADJUST_AB_A6;		break;
	case -5:	nRtn = WB_ADJUST_AB_A5;		break;
	case -4:	nRtn = WB_ADJUST_AB_A4;		break;
	case -3:	nRtn = WB_ADJUST_AB_A3;		break;
	case -2:	nRtn = WB_ADJUST_AB_A2;		break;
	case -1:	nRtn = WB_ADJUST_AB_A1;		break;
	case  0:	nRtn = WB_ADJUST_AB_A0B0;	break;
	case  1:	nRtn = WB_ADJUST_AB_B1;		break;
	case  2:	nRtn = WB_ADJUST_AB_B2;		break;
	case  3:	nRtn = WB_ADJUST_AB_B3;		break;
	case  4:	nRtn = WB_ADJUST_AB_B4;		break;
	case  5:	nRtn = WB_ADJUST_AB_B5;		break;
	case  6:	nRtn = WB_ADJUST_AB_B6;		break;
	case  7:	nRtn = WB_ADJUST_AB_B7;		break;
	case  8:	nRtn = WB_ADJUST_AB_B8;		break;
	case  9:	nRtn = WB_ADJUST_AB_B9;		break;
	}

	return nRtn;
}

int CESMPWEditorDlgGH5::GetPosMG()
{
	int nRtn = 0;
	switch (m_ctrlWBMG.GetPos())
	{
	case -9:	nRtn = WB_ADJUST_MG_M9;		break;
	case -8:	nRtn = WB_ADJUST_MG_M8;		break;
	case -7:	nRtn = WB_ADJUST_MG_M7;		break;
	case -6:	nRtn = WB_ADJUST_MG_M6;		break;
	case -5:	nRtn = WB_ADJUST_MG_M5;		break;
	case -4:	nRtn = WB_ADJUST_MG_M4;		break;
	case -3:	nRtn = WB_ADJUST_MG_M3;		break;
	case -2:	nRtn = WB_ADJUST_MG_M2;		break;
	case -1:	nRtn = WB_ADJUST_MG_M1;		break;
	case  0:	nRtn = WB_ADJUST_MG_M0G0;	break;
	case  1:	nRtn = WB_ADJUST_MG_G1;		break;
	case  2:	nRtn = WB_ADJUST_MG_G2;		break;
	case  3:	nRtn = WB_ADJUST_MG_G3;		break;
	case  4:	nRtn = WB_ADJUST_MG_G4;		break;
	case  5:	nRtn = WB_ADJUST_MG_G5;		break;
	case  6:	nRtn = WB_ADJUST_MG_G6;		break;
	case  7:	nRtn = WB_ADJUST_MG_G7;		break;
	case  8:	nRtn = WB_ADJUST_MG_G8;		break;
	case  9:	nRtn = WB_ADJUST_MG_G9;		break;
	}

	return nRtn;
}

void CESMPWEditorDlgGH5::SetPosInfo()
{
	switch (m_setInfo.nWBAB)
	{
	case WB_ADJUST_AB_A9:	m_ctrlWBAB.SetPos(-9);	break;
	case WB_ADJUST_AB_A8:	m_ctrlWBAB.SetPos(-8);	break;
	case WB_ADJUST_AB_A7:	m_ctrlWBAB.SetPos(-7);	break;
	case WB_ADJUST_AB_A6:	m_ctrlWBAB.SetPos(-6);	break;
	case WB_ADJUST_AB_A5:	m_ctrlWBAB.SetPos(-5);	break;
	case WB_ADJUST_AB_A4:	m_ctrlWBAB.SetPos(-4);	break;
	case WB_ADJUST_AB_A3:	m_ctrlWBAB.SetPos(-3);	break;
	case WB_ADJUST_AB_A2:	m_ctrlWBAB.SetPos(-2);	break;
	case WB_ADJUST_AB_A1:	m_ctrlWBAB.SetPos(-1);	break;
	case WB_ADJUST_AB_A0B0:	m_ctrlWBAB.SetPos(0);	break;
	case WB_ADJUST_AB_B1:	m_ctrlWBAB.SetPos(1);	break;
	case WB_ADJUST_AB_B2:	m_ctrlWBAB.SetPos(2);	break;
	case WB_ADJUST_AB_B3:	m_ctrlWBAB.SetPos(3);	break;
	case WB_ADJUST_AB_B4:	m_ctrlWBAB.SetPos(4);	break;
	case WB_ADJUST_AB_B5:	m_ctrlWBAB.SetPos(5);	break;
	case WB_ADJUST_AB_B6:	m_ctrlWBAB.SetPos(6);	break;
	case WB_ADJUST_AB_B7:	m_ctrlWBAB.SetPos(7);	break;
	case WB_ADJUST_AB_B8:	m_ctrlWBAB.SetPos(8);	break;
	case WB_ADJUST_AB_B9:	m_ctrlWBAB.SetPos(9);	break;
	}

	switch (m_setInfo.nWBMG)
	{
	case WB_ADJUST_MG_M9:	m_ctrlWBMG.SetPos(-9);	break;
	case WB_ADJUST_MG_M8:	m_ctrlWBMG.SetPos(-8);	break;
	case WB_ADJUST_MG_M7:	m_ctrlWBMG.SetPos(-7);	break;
	case WB_ADJUST_MG_M6:	m_ctrlWBMG.SetPos(-6);	break;
	case WB_ADJUST_MG_M5:	m_ctrlWBMG.SetPos(-5);	break;
	case WB_ADJUST_MG_M4:	m_ctrlWBMG.SetPos(-4);	break;
	case WB_ADJUST_MG_M3:	m_ctrlWBMG.SetPos(-3);	break;
	case WB_ADJUST_MG_M2:	m_ctrlWBMG.SetPos(-2);	break;
	case WB_ADJUST_MG_M1:	m_ctrlWBMG.SetPos(-1);	break;
	case WB_ADJUST_MG_M0G0:	m_ctrlWBMG.SetPos(0);	break;
	case WB_ADJUST_MG_G1:	m_ctrlWBMG.SetPos(1);	break;
	case WB_ADJUST_MG_G2:	m_ctrlWBMG.SetPos(2);	break;
	case WB_ADJUST_MG_G3:	m_ctrlWBMG.SetPos(3);	break;
	case WB_ADJUST_MG_G4:	m_ctrlWBMG.SetPos(4);	break;
	case WB_ADJUST_MG_G5:	m_ctrlWBMG.SetPos(5);	break;
	case WB_ADJUST_MG_G6:	m_ctrlWBMG.SetPos(6);	break;
	case WB_ADJUST_MG_G7:	m_ctrlWBMG.SetPos(7);	break;
	case WB_ADJUST_MG_G8:	m_ctrlWBMG.SetPos(8);	break;
	case WB_ADJUST_MG_G9:	m_ctrlWBMG.SetPos(9);	break;
	}

	m_ctrlFilterEffect.SetPos(m_setInfo.nFilter);		// value : off, yellow, orange, red, green

	int nPSType = 0;
 	CSliderCtrl* ctrlTemp;

	for (int i = 0; i < 5 ; i++)
	{
		if (i == 0)
		{
			nPSType = m_setInfo.nContrast;
			ctrlTemp = &m_ctrlContrast;
		}
		else if (i == 1)
		{
			nPSType = m_setInfo.nSharpness;
			ctrlTemp = &m_ctrlSharpness;
		}
		else if (i == 2)
		{
			nPSType = m_setInfo.nNoise;
			ctrlTemp = &m_ctrlNoiseReduction;
		}
		else if (i == 3)
		{
			nPSType = m_setInfo.nSaturation;
			ctrlTemp = &m_ctrlSaturation;
		}
		else if (i == 4)
		{
			nPSType = m_setInfo.nHue;
			ctrlTemp = &m_ctrlHue;
		}
		
		switch (nPSType)
		{
		case PS_VALUE_M_5:	ctrlTemp->SetPos(-5);	break;
		case PS_VALUE_M_4:	ctrlTemp->SetPos(-4);	break;
		case PS_VALUE_M_3:	ctrlTemp->SetPos(-3);	break;
		case PS_VALUE_M_2:	ctrlTemp->SetPos(-2);	break;
		case PS_VALUE_M_1:	ctrlTemp->SetPos(-1);	break;
		case PS_VALUE_0:	ctrlTemp->SetPos(0);	break;
		case PS_VALUE_P_1:	ctrlTemp->SetPos(1);	break;
		case PS_VALUE_P_2:	ctrlTemp->SetPos(2);	break;
		case PS_VALUE_P_3:	ctrlTemp->SetPos(3);	break;
		case PS_VALUE_P_4:	ctrlTemp->SetPos(4);	break;
		case PS_VALUE_P_5:	ctrlTemp->SetPos(5);	break;
		}
	}	
}

void CESMPWEditorDlgGH5::SetPosInfo( WB_ADJUST_TYPE type, int nValue )
{
	if (nValue < -9)
		return;

	if (nValue > 9)
		return;

	if (type == WB_ADJUST_TYPE_AB)
		m_ctrlWBAB.SetPos(nValue);
	else
		m_ctrlWBMG.SetPos(nValue);
}

void CESMPWEditorDlgGH5::LoadPosText()
{
	CString strTemp;
	strTemp.Format(_T("Photo Style: [%s]"), m_setInfo.strPhotoStyle);
	GetDlgItem(IDC_GROUPBOX_PS)->SetWindowText(strTemp);

	CString strAB;
	CString strMG;
	CString strPSValue;

	switch (m_ctrlWBAB.GetPos())
	{
	case -9:	strAB = _T("A9");	break;
	case -8:	strAB = _T("A8");	break;
	case -7:	strAB = _T("A7");	break;
	case -6:	strAB = _T("A6");	break;
	case -5:	strAB = _T("A5");	break;
	case -4:	strAB = _T("A4");	break;
	case -3:	strAB = _T("A3");	break;
	case -2:	strAB = _T("A2");	break;
	case -1:	strAB = _T("A1");	break;
	case  0:	strAB = _T("A0B0");	break;
	case  1:	strAB = _T("B1");	break;
	case  2:	strAB = _T("B2");	break;
	case  3:	strAB = _T("B3");	break;
	case  4:	strAB = _T("B4");	break;
	case  5:	strAB = _T("B5");	break;
	case  6:	strAB = _T("B6");	break;
	case  7:	strAB = _T("B7");	break;
	case  8:	strAB = _T("B8");	break;
	case  9:	strAB = _T("B9");	break;
	}

	switch (m_ctrlWBMG.GetPos())
	{
	case -9:	strMG = _T("M9");	break;
	case -8:	strMG = _T("M8");	break;
	case -7:	strMG = _T("M7");	break;
	case -6:	strMG = _T("M6");	break;
	case -5:	strMG = _T("M5");	break;
	case -4:	strMG = _T("M4");	break;
	case -3:	strMG = _T("M3");	break;
	case -2:	strMG = _T("M2");	break;
	case -1:	strMG = _T("M1");	break;
	case  0:	strMG = _T("M0G0"); break;
	case  1:	strMG = _T("G1");	break;
	case  2:	strMG = _T("G2");	break;
	case  3:	strMG = _T("G3");	break;
	case  4:	strMG = _T("G4");	break;
	case  5:	strMG = _T("G5");	break;
	case  6:	strMG = _T("G6");	break;
	case  7:	strMG = _T("G7");	break;
	case  8:	strMG = _T("G8");	break;
	case  9:	strMG = _T("G9");	break;
	}

	m_crtlValueAB.SetWindowText(strAB);
	m_crtlValueMG.SetWindowText(strMG);

	strPSValue.Format(_T("%d"), m_ctrlContrast.GetPos());
	GetDlgItem(IDC_VALUE_CONTRAST)->SetWindowText(strPSValue);

	strPSValue.Format(_T("%d"), m_ctrlSharpness.GetPos());
	GetDlgItem(IDC_VALUE_SHARPNESS)->SetWindowText(strPSValue);

	strPSValue.Format(_T("%d"), m_ctrlNoiseReduction.GetPos());
	GetDlgItem(IDC_VALUE_NOISE_REDUCTION)->SetWindowText(strPSValue);

	strPSValue.Format(_T("%d"), m_ctrlSaturation.GetPos());
	GetDlgItem(IDC_VALUE_SATURATION)->SetWindowText(strPSValue);

	strPSValue.Format(_T("%d"), m_ctrlHue.GetPos());
	GetDlgItem(IDC_VALUE_HUE)->SetWindowText(strPSValue);

	switch (m_ctrlFilterEffect.GetPos())
	{
	case  PS_VALUE_FILTER_EFFECT_OFF:		strPSValue = _T("Off");		break;
	case  PS_VALUE_FILTER_EFFECT_YELLOW:	strPSValue = _T("Yellow");	break;
	case  PS_VALUE_FILTER_EFFECT_ORANGE:	strPSValue = _T("Orange");	break;
	case  PS_VALUE_FILTER_EFFECT_RED:		strPSValue = _T("Red");		break;
	case  PS_VALUE_FILTER_EFFECT_GREEN:		strPSValue = _T("Green");	break;
	}
	GetDlgItem(IDC_VALUE_FILTER_EFFECT)->SetWindowText(strPSValue);
}

void CESMPWEditorDlgGH5::SetChangePhotoStyleList(PHOTO_STYLE nPhotoStyle)
{
	if (!m_pItem)
		return;

	if (nPhotoStyle <= PHOTO_STYLE_Unknown)
	{
		// 모두 disable
		m_ctrlContrast.EnableWindow(FALSE);
		m_ctrlSharpness.EnableWindow(FALSE);
		m_ctrlNoiseReduction.EnableWindow(FALSE);
		m_ctrlSaturation.EnableWindow(FALSE);
		m_ctrlHue.EnableWindow(FALSE);
		m_ctrlFilterEffect.EnableWindow(FALSE);

		GetDlgItem(IDC_STATIC_CONTRAST)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_SHARPNESS)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_NOISE)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_SATURATION)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_HUE)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_FILTER)->EnableWindow(FALSE);

		GetDlgItem(IDC_VALUE_CONTRAST)->EnableWindow(FALSE);
		GetDlgItem(IDC_VALUE_SHARPNESS)->EnableWindow(FALSE);
		GetDlgItem(IDC_VALUE_NOISE_REDUCTION)->EnableWindow(FALSE);
		GetDlgItem(IDC_VALUE_SATURATION)->EnableWindow(FALSE);
		GetDlgItem(IDC_VALUE_HUE)->EnableWindow(FALSE);
		GetDlgItem(IDC_VALUE_FILTER_EFFECT)->EnableWindow(FALSE);
		return;
	}

	if (nPhotoStyle == PHOTO_STYLE_Monochrome || nPhotoStyle == PHOTO_STYLE_L_Monochrome)
	{
		m_ctrlHue.EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_HUE)->EnableWindow(FALSE);
		GetDlgItem(IDC_VALUE_HUE)->EnableWindow(FALSE);
	}
	else
	{
		m_ctrlFilterEffect.EnableWindow(FALSE);		
		GetDlgItem(IDC_STATIC_FILTER)->EnableWindow(FALSE);
		GetDlgItem(IDC_VALUE_FILTER_EFFECT)->EnableWindow(FALSE);

		if (nPhotoStyle == PHOTO_STYLE_Like709)
		{
			m_ctrlContrast.EnableWindow(FALSE);		
			GetDlgItem(IDC_STATIC_CONTRAST)->EnableWindow(FALSE);
			GetDlgItem(IDC_VALUE_CONTRAST)->EnableWindow(FALSE);
		}
	}
}

void CESMPWEditorDlgGH5::SendSdiMsg( CDSCItem* pItem, int nCode, int nValue)
{
	if (!pItem)
		return;

	ESMEvent* pSdiMsg = new ESMEvent;
	pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	pSdiMsg->nParam1 = nCode;
	pSdiMsg->nParam2 = PTP_DPV_UINT32;	
	pSdiMsg->pParam = (LPARAM)nValue;
	pItem->SdiAddMsg(pSdiMsg);
}

BOOL CESMPWEditorDlgGH5::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_ctrlWBAB.SetRange(-9,9);
	m_ctrlWBMG.SetRange(-9,9);
	m_ctrlContrast.SetRange(-5,5);
	m_ctrlSharpness.SetRange(-5,5);
	m_ctrlNoiseReduction.SetRange(-5,5);
	m_ctrlSaturation.SetRange(-5,5);
	m_ctrlHue.SetRange(-5,5);
	m_ctrlFilterEffect.SetRange(0,4);

	m_ctrlWBAB.SetTicFreq(1); 
	m_ctrlWBMG.SetTicFreq(1); 
	m_ctrlContrast.SetTicFreq(1); 
	m_ctrlSharpness.SetTicFreq(1); 
	m_ctrlNoiseReduction.SetTicFreq(1); 
	m_ctrlSaturation.SetTicFreq(1); 
	m_ctrlHue.SetTicFreq(1); 
	m_ctrlFilterEffect.SetTicFreq(1); 

	SetPosInfo();
	LoadPosText();

	m_WBAdjustDlg = new CESMPWEditorDlgGH5WBAD(this);
	m_WBAdjustDlg->Create(IDD_ESMPWEDITORDLG_GH5_WB_AD, this);	
	m_WBAdjustDlg->MoveWindow(CRect(10,30,110,140));
	m_WBAdjustDlg->ShowWindow(SW_SHOW);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderWbAb(NMHDR *pNMHDR, LRESULT *pResult)
{
	LoadPosText();
	*pResult = 0;
}

void CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderWbMg(NMHDR *pNMHDR, LRESULT *pResult)
{
	LoadPosText();
	*pResult = 0;
}

void CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderContrast(NMHDR *pNMHDR, LRESULT *pResult)
{
	LoadPosText();
	*pResult = 0;
}

void CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderSharpness(NMHDR *pNMHDR, LRESULT *pResult)
{
	LoadPosText();
	*pResult = 0;
}

void CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderNoiseReduction(NMHDR *pNMHDR, LRESULT *pResult)
{
	LoadPosText();
	*pResult = 0;
}

void CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderSaturation(NMHDR *pNMHDR, LRESULT *pResult)
{
	LoadPosText();
	*pResult = 0;
}

void CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderHue(NMHDR *pNMHDR, LRESULT *pResult)
{
	LoadPosText();
	*pResult = 0;
}

void CESMPWEditorDlgGH5::OnNMReleasedcaptureSliderFilterEffect(NMHDR *pNMHDR, LRESULT *pResult)
{
	LoadPosText();
	*pResult = 0;
}

void CESMPWEditorDlgGH5::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	SetChangePhotoStyleList(m_photoStyle);
}

afx_msg LRESULT CESMPWEditorDlgGH5::OnEsmWbAdjust(WPARAM wParam, LPARAM lParam)
{
	int nType = (int)wParam;
	int nValue = (int)lParam;

	nValue = nValue - 9;

	SetPosInfo((WB_ADJUST_TYPE)nType, nValue);

	LoadPosText();

	return 0;
}

void CESMPWEditorDlgGH5::OnBnClickedBtnReset()
{
	SetPosInfo(WB_ADJUST_TYPE_AB, 0);
	SetPosInfo(WB_ADJUST_TYPE_MG, 0);
	if (m_ctrlContrast.IsWindowEnabled())
		m_ctrlContrast.SetPos(0);
	if (m_ctrlSharpness.IsWindowEnabled())
		m_ctrlSharpness.SetPos(0);
	if (m_ctrlNoiseReduction.IsWindowEnabled())
		m_ctrlNoiseReduction.SetPos(0);
	if (m_ctrlSaturation.IsWindowEnabled())
		m_ctrlSaturation.SetPos(0);
	if (m_ctrlHue.IsWindowEnabled())
		m_ctrlHue.SetPos(0);
	if (m_ctrlFilterEffect.IsWindowEnabled())
		m_ctrlFilterEffect.SetPos(0);

	LoadPosText();

	OnBnClickedButtonOk();
}
