#include "stdafx.h"
#include "ESMStillImage.h"
#include "ESMFunc.h"
#include "ESMFileOperation.h"
CESMStillImage::CESMStillImage()
{
	m_nIndex = 0;
	m_bMake	= FALSE;
	m_nMovieIdx = -1;
	m_nFrameIdx = -1;
}
CESMStillImage::~CESMStillImage()
{

}
void CESMStillImage::PushTime(int nTime)
{
	/*if(m_bArrTime.size() == 0)
	{
		ESMLog(5,_T("Thread start"));
		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, _EncodingThread, (void*)this, 0, NULL);
		CloseHandle(hSyncTime);
	}*/
	m_nArrTime.push_back(STATE_WAIT);
	m_wav.PlaySoundFile(_T("C:\\Program Files\\ESMLab\\4DMaker\\img\\shutter.wav"));

	int nDSCIndex = ESMGetDSCCount();

	StillImageThread* pStill = new StillImageThread;
	pStill->pParent = this;
	pStill->nTime	= nTime;
	pStill->nIdx	= m_nIndex++;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, _DecodingThread, (void*)pStill, 0, NULL);
	CloseHandle(hSyncTime);
}
void CESMStillImage::PushTime(int nMovieIdx,int nFrameIdx)
{
	m_nArrTime.push_back(STATE_WAIT);

	StillImageThread2* pStill = new StillImageThread2;
	pStill->pParent = this;
	pStill->nMovieIdx = nMovieIdx;
	pStill->nFrameIdx = nFrameIdx;
	pStill->nIndex = m_nIndex++;
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, _DecodingThread, (void*)pStill, 0, NULL);
	CloseHandle(hSyncTime);
}
Mat CESMStillImage::RectImage(Mat frame0,Mat frame1,int nIdx,BOOL bEB)
{
	int nOutputWidth  = _ttoi(ESMGetCeremony(ESM_VALUE_CEREMONY_WIDTH));
	int nOutputHeight = _ttoi(ESMGetCeremony(ESM_VALUE_CEREMONY_HEIGHT));

	int nStep = nOutputWidth / 2;
	Mat result(nOutputHeight,nOutputWidth,CV_8UC3);

	int nRectStartX = frame0.cols/2 - frame0.rows/4;
	int nRectEndX	= frame0.cols/2 + frame0.rows/4;
#if 1
	Mat roi1 = (frame0)(cv::Rect(420,0,nOutputWidth,nOutputHeight));
	roi1.copyTo(result);
#else
#if 1
	if(bEB == FALSE)
	{
		int nProcMethod = nIdx % 3;
		switch(nProcMethod)
		{
		case 0:case 2:
			{
				//AB ( 150 - 1230)
				Mat roi1 = (frame0)(cv::Rect(150,0,nOutputWidth,nOutputHeight));
				roi1.copyTo(result);
				//putText(result,"A",cv::Point(270,540),CV_FONT_BOLD,2,Scalar(0,0,0),1,8);
				//putText(result,"B",cv::Point(810,540),CV_FONT_BOLD,2,Scalar(0,0,0),1,8);
			}
			break;
		case 1:
			{
				//EF ( 690 - 1770)
				Mat roi1 = (frame1)(cv::Rect(690,0,nOutputWidth,nOutputHeight));
				roi1.copyTo(result);
				//putText(result,"E",cv::Point(270,540),CV_FONT_BOLD,2,Scalar(0,0,0),1,8);
				//putText(result,"F",cv::Point(810,540),CV_FONT_BOLD,2,Scalar(0,0,0),1,8);
			}
			break;
		}
	}
	else
	{
		//EF
		//(src)(Rect(nLeft, nTop, nWidth, nHeight));
		Mat temp0,temp1;
		
		//putText(frame1,"E",cv::Point(960,540),CV_FONT_BOLD,2,Scalar(0,0,0),1,8);
		Mat roi0 = (frame1)(cv::Rect(960,0,nStep,nOutputHeight));
		roi0.copyTo(temp0);
		
		//putText(frame0,"B",cv::Point(960,540),CV_FONT_BOLD,2,Scalar(0,0,0),1,8);
		Mat roi1 = (frame0)(cv::Rect(690,0,1080,nOutputHeight));
		roi1.copyTo(result);	

		//for(int i = 0 ; i < roi0.rows; i++)
		//{
		//	for(int j = 0 ; j < roi1.cols; j++)
		//	{
		//		for(int c = 0 ; c < 3; c++)
		//		{
		//			result.data[nOutputWidth*(i*3)+(j*3)+c] = temp0.data[nStep*(i*3)+(j*3)+c];
		//			result.data[nOutputWidth*(i*3)+((j+nStep)*3)+c] = temp1.data[nStep*(i*3)+(j*3)+c];
		//			//result.data[1080 * ((i+540)*3) + (j*3) + c] = roi0.data[roi1.cols* (i*3) + (j*3) + c];
		//		}
		//	}
		//}
	}
	
#else
	

	int nRectStartX = frame0.cols/2 - frame0.rows/4;
	int nRectEndX	= frame0.cols/2 + frame0.rows/4;

	//(src)(Rect(nLeft, nTop, nWidth, nHeight));
	Mat temp0,temp1;
	Mat roi0 = (frame0)(cv::Rect(nRectStartX,0,nStep,nOutputHeight));
	roi0.copyTo(temp0);
	Mat roi1 = (frame1)(cv::Rect(nRectStartX,0,nStep,nOutputHeight));
	roi1.copyTo(temp1);	
	
	for(int i = 0 ; i < roi0.rows; i++)
	{
		for(int j = 0 ; j < roi1.cols; j++)
		{
			for(int c = 0 ; c < 3; c++)
			{
				result.data[nOutputWidth*(i*3)+(j*3)+c] = temp0.data[nStep*(i*3)+(j*3)+c];
				result.data[nOutputWidth*(i*3)+((j+nStep)*3)+c] = temp1.data[nStep*(i*3)+(j*3)+c];
				//result.data[1080 * ((i+540)*3) + (j*3) + c] = roi0.data[roi1.cols* (i*3) + (j*3) + c];
			}
		}
	}
#endif
#endif
	return result;
}
void CESMStillImage::CreateStillMovie()
{
	if(m_nIndex <= 0)
		return;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, _EncodingThread, (void*)this, 0, NULL);
	CloseHandle(hSyncTime);
}
void CESMStillImage::CreateCeremonyMovie(CString strCmd,CString strResult)
{
#if 1
	CString strOpt,strTempPath,strSendPath;
	strTempPath.Format(_T("M:\\Movie\\result_.mp4"));
	CString strTel1 = ESMGetCeremonyInfoString(_T("InputTel"));
	strSendPath.Format(_T("%s\\%s.mp4"), ESMGetCeremony(ESM_VALUE_CEREMONYMOVIEPATH), strTel1);

	ESMLog(5, _T("[Movie]Re-Encoding For Android....."));
	CString strReEncodingOpt;
	CString strReEncodingMovieFile;
	strReEncodingOpt.Append(_T(" -i \""));
	strReEncodingOpt.Append(strResult);
	strReEncodingOpt.Append(_T("\""));
	strReEncodingOpt.Append(_T(" -c:v libx264 -g 15 -preset ultrafast "));		
	//strReEncodingMovieFile.Format(_T("%s\\2D\\%s\\%s_%s.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName,_T("Android"));
	strReEncodingOpt.Append(_T("\""));
	strReEncodingOpt.Append(strTempPath);
	strReEncodingOpt.Append(_T("\""));
	
	SHELLEXECUTEINFO lpExecInfoReEncoding;
	lpExecInfoReEncoding.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfoReEncoding.lpFile = strCmd;
	lpExecInfoReEncoding.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfoReEncoding.hwnd = NULL;  
	lpExecInfoReEncoding.lpVerb = L"open";
	lpExecInfoReEncoding.lpParameters = strReEncodingOpt;
	lpExecInfoReEncoding.lpDirectory = NULL;

	lpExecInfoReEncoding.nShow = SW_HIDE; // hide shell during execution
	lpExecInfoReEncoding.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfoReEncoding);

	// wait until the process is finished
	if (lpExecInfoReEncoding.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfoReEncoding.hProcess, INFINITE);
		::CloseHandle(lpExecInfoReEncoding.hProcess);
	}
#else
	CString strOpt,strTempPath,strSendPath;
	strTempPath.Format(_T("M:\\Movie\\result_.mp4"));
	CString strTel1 = ESMGetCeremonyInfoString(_T("InputTel"));
	strSendPath.Format(_T("%s\\%s.mp4"), ESMGetCeremony(ESM_VALUE_CEREMONYMOVIEPATH), strTel1);
	strOpt.Format(_T("-i \"%s\" -c:a copy \"%s\""),
		strResult, 
		strTempPath);

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
#endif
	CopyFile(strTempPath,strSendPath,FALSE);
}
void CESMStillImage::EncodingImage(Mat frame0,Mat frame1,int nIdx)
{
	//char strPath[200];
	//sprintf(strPath,"M:\\Movie\\%d.mp4",nIdx);
	//int fourcc = cv::VideoWriter::fourcc('m','p','e','g');
	int nOutputWidth  = _ttoi(ESMGetCeremony(ESM_VALUE_CEREMONY_WIDTH));
	int nOutputHeight = _ttoi(ESMGetCeremony(ESM_VALUE_CEREMONY_HEIGHT));

	Mat white(nOutputHeight,nOutputWidth,CV_8UC3);
	white = Scalar(255,255,255);

	FFmpegManager ffmpeg(TRUE);
	CString strPath;
	strPath.Format(_T("M:\\Movie\\%d.mp4"),nIdx);
	//VideoWriter writer;
	//writer.open(strPath,fourcc,30,cv::Size(1080,1080),true);
	BOOL bEncode = ffmpeg.InitEncode(strPath,30,AV_CODEC_ID_MPEG2VIDEO,1,nOutputWidth,nOutputHeight);
	if(bEncode)
	{
		for(int c = 0 ; c < 2; c++)
		{
			Mat temp = RectImage(frame0,frame1,nIdx,c).clone();
			for(int i = 0 ;i  < 30; i++)
			{
				Sleep(10);
				ffmpeg.EncodePushImage(temp.data);
			}

			ffmpeg.EncodePushImage(white.data);
		}
	}
	if(bEncode)
	{
		ffmpeg.CloseEncode();
		m_nArrTime.at(nIdx) = STATE_FINISH;
		ESMLog(5,_T("[%d] Encode finish(%d)"),nIdx,bEncode);
	}
	else
	{
		m_nArrTime.at(nIdx) = STATE_FAIL;
		ESMLog(5,_T("[%d] Encode fail(%d)"),nIdx,bEncode);
	}
}
void CESMStillImage::EncodingImage(EncodeList stLoad)
{
	Mat frame0 = stLoad.frame0.clone();
	Mat frame1 = stLoad.frame1.clone();
	int nIdx   = stLoad.nIdx;

	int nOutputWidth  = _ttoi(ESMGetCeremony(ESM_VALUE_CEREMONY_WIDTH));
	int nOutputHeight = _ttoi(ESMGetCeremony(ESM_VALUE_CEREMONY_HEIGHT));

	Mat white(nOutputHeight,nOutputWidth,CV_8UC3);
	white = Scalar(255,255,255);

	FFmpegManager ffmpeg(TRUE);
	CString strPath;
	strPath.Format(_T("M:\\Movie\\%d.mp4"),nIdx);
	//VideoWriter writer;
	//writer.open(strPath,fourcc,30,cv::Size(1080,1080),true);
	BOOL bEncode = ffmpeg.InitEncode(strPath,30,AV_CODEC_ID_MPEG2VIDEO,1,nOutputWidth,nOutputHeight);
	if(bEncode)
	{
		Mat temp = RectImage(frame0,frame1,nIdx,0).clone();
		for(int i = 0 ;i  < 30; i++)
		{
			Sleep(10);
			ffmpeg.EncodePushImage(temp.data);
		}
		ffmpeg.EncodePushImage(temp.data);
		ffmpeg.EncodePushImage(white.data);
		//ffmpeg.EncodePushImage(white.data);
		/*for(int c = 0 ; c < 2; c++)
		{
		Mat temp = RectImage(frame0,frame1,nIdx,c).clone();
		for(int i = 0 ;i  < 30; i++)
		{
		Sleep(10);
		ffmpeg.EncodePushImage(temp.data);
		}

		ffmpeg.EncodePushImage(white.data);
		}*/
	}
	
	if(bEncode)
	{
		ffmpeg.EncodePushImage(white.data);
		ffmpeg.CloseEncode();
		m_nArrTime.at(nIdx) = STATE_FINISH;
		ESMLog(5,_T("[%d] Encode finish(%d)"),nIdx,bEncode);
	}
	else
	{
		m_nArrTime.at(nIdx) = STATE_FAIL;
		ESMLog(5,_T("[%d] Encode fail(%d)"),nIdx,bEncode);
	}
}
void CESMStillImage::ClearRamDisk()
{
	CESMFileOperation fo;
	CString strPath;
	strPath.Format(_T("M:\\Movie\\"));
	fo.Delete(strPath);
}
unsigned WINAPI CESMStillImage::_DecodingThread(LPVOID param)
{
#if 1
	StillImageThread2* pThread = (StillImageThread2*)param;
	CESMStillImage* pStill = pThread->pParent;
	int nMovieIndex = pThread->nMovieIdx;
	int nFrameIdx   = pThread->nFrameIdx;
	int nIndex = pThread->nIndex;
	delete pThread;
	
	CString strDSCID0 = ESMGetDSCIDFromIndex(0);
	int nTime = (nMovieIndex * 1000) + nFrameIdx*30;
	int nFIdx = ESMGetFrameIndex(nTime);
	//int nFIdx = //nMovieIndex * 30;//ESMGetFrameIndex(nTime);
	CString strPath0 = ESMGetMoviePath(strDSCID0,nFIdx);
	Mat frame0;
	int nWaitCnt = 0;
	BOOL bWait = TRUE;
	while(1)
	{
		BOOL bLoad = FALSE;

		CT2CA pszConvertedAnsiString (strPath0);
		std::string path0(pszConvertedAnsiString);

		for(int i = 0 ; i < 100; i++)
		{
			Sleep(20);

			VideoCapture vc0(path0);

			if(vc0.isOpened() == FALSE)
				continue;
			else if(vc0.isOpened() == TRUE)
			{
				bLoad = TRUE;
				int nCnt = 0;
				while(1)
				{
					vc0>> frame0;

					if(frame0.empty())
						break;

					if(nCnt == nFrameIdx)
						break;

					nCnt++;
				}
				break;
			}
		}
		if(bLoad == TRUE)
			break;

		nWaitCnt++;

		if(nWaitCnt > 1)
		{
			bWait = FALSE;
			break;
		}
	}
#else
	StillImageThread* pThread = (StillImageThread*)param;
	CESMStillImage* pStill = pThread->pParent;
	int nTime = pThread->nTime;
	int nIndex = pThread->nIdx;
	delete pThread;

	ESMLog(5,_T("[%d] %d decoding start"),nIndex,nTime);
	CString strDSCID0 = ESMGetDSCIDFromIndex(0);
	CString strDSCID1 = ESMGetDSCIDFromIndex(ESMGetDSCCount() - 1);

	int nFIdx = ESMGetFrameIndex(nTime);
	CString strPath0 = ESMGetMoviePath(strDSCID0,nFIdx);
	CString strPath1 = ESMGetMoviePath(strDSCID1,nFIdx);

	int nShiftFrame = 0;
	nShiftFrame = ESMGetShiftFrame(strDSCID0);

	nShiftFrame = nShiftFrame + ESMGetFrameIndex(nTime);
	int nMovieIndex = 0, nRealFrameIndex = 0; 
	ESMGetMovieIndex(nShiftFrame, nMovieIndex, nRealFrameIndex);
	
	Mat frame0,frame1;
	int nWaitCnt = 0;
	BOOL bWait = TRUE;
	while(1)
	{
		BOOL bLoad = FALSE;

		CT2CA pszConvertedAnsiString (strPath0);
		std::string path0(pszConvertedAnsiString);

		CT2CA pszConvertedAnsiString1 (strPath1);
		std::string path1(pszConvertedAnsiString1);

		for(int i = 0 ; i < 100; i++)
		{
			Sleep(20);

			VideoCapture vc0(path0);
			VideoCapture vc1(path1);

			if(vc0.isOpened() == FALSE || vc1.isOpened() == FALSE)
				continue;
			else if(vc0.isOpened() == TRUE && vc1.isOpened() == TRUE)
			{
				bLoad = TRUE;
				int nCnt = 0;
				while(1)
				{
					vc0>> frame0;
					vc1>> frame1;

					if(frame0.empty() || frame1.empty())
						break;

					if(nCnt == nRealFrameIndex)
						break;

					nCnt++;
				}
				break;
			}
		}
		if(bLoad == TRUE)
			break;

		nWaitCnt++;

		if(nWaitCnt > 1)
		{
			bWait = FALSE;
			break;
		}
	}
#endif
	if(bWait == TRUE)
	{
		ESMLog(5,_T("[%d] %d decoding finish"),nIndex,nFIdx);
		EncodeList stLoad;
		stLoad.frame0 = frame0.clone();
		stLoad.frame1 = frame0.clone();
		stLoad.nIdx   = nIndex;
		//pStill->m_stArrImage.push_back(stLoad);
		pStill->EncodingImage(stLoad);
	}
	else
	{
		ESMLog(5,_T("[%d] %d decoding fail"),nIndex,nTime);
		pStill->m_nArrTime.at(nIndex) = STATE_FAIL;
		//EncodeList stLoad;
		//stLoad.frame0 = frame0.clone();
		//stLoad.frame1 = frame1.clone();
		//stLoad.nIdx   = nIndex;
		//pStill->m_stArrImage.push_back(stLoad);
		//pStill->EncodingImage(stLoad);
	}

	//pStill->RectImage(frame0,frame1);
	//pStill->EncodingImage(frame0,frame1,nIndex);

	return FALSE;
}
unsigned WINAPI CESMStillImage::_EncodingThread(LPVOID param)
{
	CESMStillImage* pStill = (CESMStillImage*) param;

#if 1
	SYSTEMTIME st;
	GetLocalTime(&st);

	CString strDate = _T(""),strMovieFile;
	strDate.Format(_T("%04d%02d%02d"),st.wYear,st.wMonth,st.wDay); 
	CString str4DMName = ESMGetFrameRecord();

	strMovieFile.Format(_T("%s\\2D\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName);

	CString strForder = _T("");
	strForder.Format(_T("%s\\2D\\%s"),ESMGetPath(ESM_PATH_OUTPUT), strDate);

	CESMFileOperation fo;
	if(!fo.IsFileFolder(strForder))
		fo.CreateFolder(strForder);

	int nCount = 0;
	while(FileExists(strMovieFile))
	{
		strMovieFile.Format(_T("%s\\2D\\%s\\%s_%d.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName, nCount);
		//strTempPath.Format(_T("%s\\2D\\%s\\%s_%d_event.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName, nCount);
		nCount++;
	}

	CString strCmd,strOpt;
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));

	strOpt.Format(_T("-i \"concat:"));

	if(ESMGetValue(ESM_VALUE_INSERTLOGO))
	{
		CString LogoMovie;
		LogoMovie.Format(_T("%s\\img\\LogoMovie_intro.mp4"),ESMGetPath(ESM_PATH_HOME));	
		if(FileExists(LogoMovie))
		{
			strOpt.Append(LogoMovie);
			strOpt.Append(_T("|"));
		}
		else
			ESMLog(5, _T("Not Exist File[%s]"), LogoMovie);
	}

	for(int i = 0 ; i < pStill->m_nIndex; i++)
	{
		if(pStill->m_nArrTime.at(i)==STATE_WAIT)
		{
			int nCnt = 0;
			while(1)
			{
				if(pStill->m_nArrTime.at(i) == STATE_FINISH)
					break;

				if(pStill->m_nArrTime.at(i) == STATE_FAIL)
					break;
				Sleep(10);
			}
		}

		if(pStill->m_nArrTime.at(i) == STATE_FAIL)
			continue;

		CString strPath;
		strPath.Format(_T("M:\\Movie\\%d.mp4"),i);
		strOpt.Append(strPath);
		if( i != pStill->m_nIndex -1)
			strOpt.Append(_T("|"));
	}

	if(ESMGetValue(ESM_VALUE_INSERTLOGO))
	{
		CString LogoMovie;
		LogoMovie.Format(_T("%s\\img\\LogoMovie_end.mp4"),ESMGetPath(ESM_PATH_HOME));	
		if(FileExists(LogoMovie))
		{
			strOpt.Append(_T("|"));
			strOpt.Append(LogoMovie);
		}
		else
			ESMLog(5, _T("Not Exist File[%s]"), LogoMovie);
	}

	strOpt.Append(_T("\" -c copy -bsf:a aac_adtstoasc "));
	strOpt.Append(_T("\""));
	strOpt.Append(strMovieFile);
	strOpt.Append(_T("\""));

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	pStill->CreateCeremonyMovie(strCmd,strMovieFile);

	ESMLog(5,_T("Create Still Image Finish!"));
#else
	while(1)
	{
		Sleep(10);
		if(pStll->m_bMake == TRUE)
			break;

		if(pStll->m_stArrImage.size() > 0)
		{
			EncodeList stLoad = pStll->m_stArrImage.at(0);
			ESMLog(5,_T("[%d] Encoding Start"),stLoad.nIdx);
			pStll->EncodingImage(stLoad);
			pStll->m_stArrImage.erase(pStll->m_stArrImage.begin());
		}
		else
			continue;
	}
	ESMLog(5,_T("Encoding Thread killed"));
#endif
	return TRUE;
}
void CESMStillImage::RectMovie()
{
	int nWidth  = _ttoi(ESMGetCeremony(ESM_VALUE_CEREMONY_WIDTH));
	int nHeight = _ttoi(ESMGetCeremony(ESM_VALUE_CEREMONY_HEIGHT));
	FFmpegManager ffmpeg(FALSE);


	VideoCapture vc("C:\\Program Files\\ESMLab\\4DMaker\\img\\LogoMovie_intro_.mp4");

	if(ffmpeg.InitEncode(_T("C:\\Program Files\\ESMLab\\4DMaker\\img\\LogoMovie_end.mp4"),30,AV_CODEC_ID_MPEG2VIDEO,1,nWidth,nHeight));
	{
		while(1)
		{
			Mat frame;
			vc>>frame;
			
			if(frame.empty())
				break;
			
			//Mat rect1080 = (frame)(cv::Rect(420,0,1080,1080));
			//Mat te;
			//rect1080.copyTo(te);

			ffmpeg.EncodePushImage(frame.data);
		}
		ffmpeg.CloseEncode();
	}

}