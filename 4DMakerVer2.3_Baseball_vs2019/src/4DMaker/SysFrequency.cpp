// SysFrequency.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "SysFrequency.h"
#include "afxdialogex.h"


// CSysFrequency 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSysFrequency, CDialogEx)

CSysFrequency::CSysFrequency(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSysFrequency::IDD, pParent)
{

}

CSysFrequency::~CSysFrequency()
{
}

void CSysFrequency::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSysFrequency, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_NTSC, &CSysFrequency::OnBnClickedBtnNtsc)
	ON_BN_CLICKED(IDC_BTN_PAL, &CSysFrequency::OnBnClickedBtnPal)
END_MESSAGE_MAP()


// CSysFrequency 메시지 처리기입니다.


void CSysFrequency::OnBnClickedBtnNtsc()
{
	EndDialog(0);
}


void CSysFrequency::OnBnClickedBtnPal()
{
	EndDialog(1);
}
