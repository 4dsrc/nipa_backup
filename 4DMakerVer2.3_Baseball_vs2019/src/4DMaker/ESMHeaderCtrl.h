#pragma once
class CESMHeaderCtrl : public CHeaderCtrl
{
public:
	CESMHeaderCtrl();
	~CESMHeaderCtrl();
	
public:

	COLORREF m_clrBk;

	COLORREF m_clrBorder;

	COLORREF m_clrText;

	CFont *m_pFont;

protected:

	virtual void PreSubclassWindow();

	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	afx_msg void OnCustomDraw(NMHDR* pNMHDR, LRESULT* pResult);

	DECLARE_MESSAGE_MAP()
};

