#pragma once


// COoverlapFrameCount 대화 상자입니다.

class COoverlapFrameCount : public CDialog
{
	DECLARE_DYNAMIC(COoverlapFrameCount)

public:
	COoverlapFrameCount(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COoverlapFrameCount();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TIMELINEOBJECTEDITOR_FRAME_DIALOG };

	int m_nFrameCount;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
};
