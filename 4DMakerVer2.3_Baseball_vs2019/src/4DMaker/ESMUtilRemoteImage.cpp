#include "stdafx.h"
#include "ESMUtilRemoteImage.h"
#include "ESMFunc.h"
#include "highgui.h"
#include "ESMCtrl.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CESMRemoteImage, CStatic)

CESMRemoteImage::CESMRemoteImage()
{
	m_dMultiple = 1.0;
	m_nImageWidth = 1;
	m_nImageHeight = 1;

	m_pImage = NULL;
	m_bIsFull = FALSE;
	m_pTpImage = NULL;

	m_memFullDC = NULL;
	m_memDivDC = NULL;

	m_bRect	 = FALSE;

	m_nMovieIdx = -1;
	m_nFrameIdx = -1;
	m_nCamID	= -1;
	InitializeCriticalSection(&m_crImageLoad);
}

CESMRemoteImage::~CESMRemoteImage()
{
	if(m_pTpImage)
	{
		delete m_pTpImage;
		m_pTpImage = NULL;
	}

	if(m_memDivDC)
	{
		m_memDivDC->DeleteDC();
		delete m_memDivDC;
	}
	if(m_memFullDC)
	{
		m_memFullDC->DeleteDC();
		delete m_memFullDC;
	}

	DeleteCriticalSection(&m_crImageLoad);
}

BEGIN_MESSAGE_MAP(CESMRemoteImage, CStatic)
	ON_WM_PAINT()
//	ON_WM_SIZING()
ON_WM_SIZE()
END_MESSAGE_MAP()

void CESMRemoteImage::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	int nTp = 0;

	CDC* memDC;
	if(m_bIsFull)
		memDC = m_memFullDC;
	else
		memDC = m_memDivDC;

	CRect rect;
	GetClientRect(rect);
	if(m_rcTemp.IsRectEmpty())
		m_rcTemp = rect;

	BOOL bChange = FALSE;
	if(m_rcTemp.bottom != rect.bottom || m_rcTemp.right != rect.right)
	{
		bChange = TRUE;
		m_rcTemp = rect;
	}

	CBrush b(RGB(255,0,0));
	/*CBrush brush;
	brush.CreateSolidBrush(COLOR_YELLOW);*/
	//memDC->FillRect(&rct,&brush);

	int nTime = ESMGetTimeFromIndex(m_nMovieIdx,m_nFrameIdx);
	CString strTime;
	strTime.Format(_T("[%d]%d.%03d"),m_nCamID,nTime/1000,nTime%1000);

	CRect rct = CRect(m_rcTemp.Width()/4,m_rcTemp.Height()*9/10,
		m_rcTemp.Width()*3/4,m_rcTemp.Height());
	//sprintf(strID,"[%d]%d.%03d",GetCamID(),nTime/1000,nTime%1000);

	if(memDC)
	{
		memDC->SetTextColor(COLOR_WHITE);
		memDC->SetBkColor(COLOR_BLACK);
		memDC->DrawText(strTime,rct,DT_CENTER | DT_VCENTER | DT_SINGLELINE);

		dc.SetStretchBltMode(COLORONCOLOR);
		if(bChange)
			dc.Rectangle(0, 0, m_rcTemp.right, m_rcTemp.bottom);

		if(GetRedRect())
		{
			CRect tmp = m_rcTemp;
			tmp.top += 1;
			tmp.left += 1;
			tmp.bottom -= 1;
			tmp.right -= 1;
			tmp.OffsetRect(-tmp.left,-tmp.top);
			memDC->FrameRect(&tmp,&b);
		}

		dc.StretchBlt( 0, 0, m_rcTemp.Width(), m_rcTemp.Height(), memDC, 
			int((double)m_ptImageLeftTop.x * m_dMultiple), 
			int((double)m_ptImageLeftTop.y * m_dMultiple), 
			int((double)m_rcTemp.Width() * m_dMultiple), 
			int((double)m_rcTemp.Height() * m_dMultiple),  SRCCOPY);
	}
	else
		dc.Rectangle(m_rcTemp);
}

BOOL CESMRemoteImage::SetImageBuffer(BYTE* pImage, int nWidth, int nHeight, int nChannel)
{
	if( pImage == NULL)
		return FALSE;

	if(!m_memDivDC)
	{
		CDC *pDC = GetDC();
		CBitmap bitmap;
		m_memDivDC = new CDC;
		if(!m_memDivDC->CreateCompatibleDC(pDC))
			return FALSE;

		bitmap.CreateCompatibleBitmap(pDC, 3840, 2160);

		m_memDivDC->SelectObject(&bitmap);
		ReleaseDC(pDC);
	}
	CDC* memDC = m_memDivDC;
	/*if(nWidth == m_nImageWidth)
	{
		m_bIsFull = FALSE;

		if(!m_memDivDC)
		{
			CDC *pDC = GetDC();
			CBitmap bitmap;
			m_memDivDC = new CDC;
			if(!m_memDivDC->CreateCompatibleDC(pDC))
				return FALSE;

			bitmap.CreateCompatibleBitmap(pDC, nWidth, nHeight);

			m_memDivDC->SelectObject(&bitmap);
			ReleaseDC(pDC);
		}

		memDC = m_memDivDC;
	}
	else
	{
		memDC = m_memFullDC;

		if(!m_memFullDC)
		{
			CDC *pDC = GetDC();
			CBitmap bitmap;
			m_memFullDC = new CDC;
			if(!m_memFullDC->CreateCompatibleDC(pDC))
				return FALSE;

			bitmap.CreateCompatibleBitmap(pDC, nWidth, nHeight);

			m_memFullDC->SelectObject(&bitmap);
			ReleaseDC(pDC);
		}
		m_bIsFull = TRUE;
	}*/

	//m_nImageWidth = nWidth;
	//m_nImageHeight = nHeight;

	int nDepth = 8;

	EnterCriticalSection(&m_crImageLoad);
	if(m_pTpImage)
	{
		delete[] m_pTpImage;
		m_pTpImage = NULL;
	}

	
	if(m_pTpImage == NULL)
		m_pTpImage = new BYTE[nHeight * nWidth * nChannel];
	else
	{
		LeaveCriticalSection(&m_crImageLoad);
		return FALSE;
	}
	LeaveCriticalSection(&m_crImageLoad);

	if( nChannel == 1)
	{
		for( int j= 0 ;j < nHeight; j++)
		{
			for( int i = 0 ;i < nWidth; i++)
			{
				m_pTpImage[ j *  (nWidth * 3) + (i* 3)]		= pImage[ j *  nWidth + i];
				m_pTpImage[ j *  (nWidth * 3) + (i* 3) + 1] = pImage[ j *  nWidth + i];
				m_pTpImage[ j *  (nWidth * 3) + (i* 3) + 2] = pImage[ j *  nWidth + i];
			}
		}
	}
	else
	{
		EnterCriticalSection(&m_crImageLoad);
		memset(m_pTpImage,0,nHeight * nWidth * nChannel);
		memcpy(m_pTpImage, pImage,  nHeight * nWidth * nChannel);
		LeaveCriticalSection(&m_crImageLoad);
	}

	EnterCriticalSection(&m_crImageLoad);

	BITMAPINFOHEADER bih;
	memset(&bih, 0, sizeof(BITMAPINFOHEADER));    
	bih.biSize			= sizeof(BITMAPINFOHEADER);
	bih.biWidth			= nWidth;  
	bih.biHeight		= -nHeight; 
	bih.biPlanes		= 1;
	bih.biBitCount		= 3 * nDepth;
	bih.biSizeImage		= nWidth * nHeight * 3;
	bih.biCompression	= BI_RGB;

	memDC->SetStretchBltMode(COLORONCOLOR);

	::StretchDIBits(*memDC,		// hDC
		0,						// XDest
		0,						// YDest
		nWidth,					// nDestWidth
		nHeight,				// nDestHeight
		0,						// XSrc
		0,						// YSrc
		nWidth,					// nSrcWidth
		nHeight ,				// nSrcHeight
		(BYTE*)m_pTpImage,		// lpBits
		(BITMAPINFO*)&bih,		// lpBitsInfo
		DIB_RGB_COLORS,			// wUsage
		SRCCOPY);				// dwROP
	LeaveCriticalSection(&m_crImageLoad);

	//-- 2014-08-30 hongsu@esmlab.com
	//-- remember Image
	//-- delete when get new image or delete this
	// delete[] pTpImage;

	//-- 2014-08-31 hongsu@esmlab.com
	//-- Create Image 
	if(m_pImage)
	{
		//cvReleaseImage(&m_pImage);
		m_pImage = NULL;
	}

	if(!m_pImage)
	{
		cvReleaseImage(&m_pImage);
		return FALSE;
	}

	return TRUE;
}

void CESMRemoteImage::Redraw()
{
	RedrawWindow();
}

IplImage* CESMRemoteImage::BYTE2IplImage(BYTE* pByte, int nWidth, int nHeight)
{
	if(nWidth <= 0 || nHeight <= 0) 
		return NULL;

	IplImage* pImg = NULL;	
	pImg = cvCreateImageHeader(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 3); 
	if(!pImg)		
		return NULL;

	cvSetData( pImg, pByte, pImg->widthStep );
	return pImg;
}
void CESMRemoteImage::SetSize(int nOriWidth,int nOriHeight,int nFullWidth,int nFullHeight)
{
	m_nFullWidth  = nFullWidth;
	m_nFullHeight = nFullHeight;

	m_nImageHeight = nOriHeight;
	m_nImageWidth  = nOriWidth;

	if(!m_memDivDC)
	{
		CDC *pDC = GetDC();
		CBitmap bitmap;
		m_memDivDC = new CDC;
		if(!m_memDivDC->CreateCompatibleDC(pDC))
			return;

		bitmap.CreateCompatibleBitmap(pDC, m_nImageWidth, m_nImageHeight);

		m_memDivDC->SelectObject(&bitmap);
		ReleaseDC(pDC);
	}
	if(!m_memFullDC)
	{
		CDC *pDC = GetDC();
		CBitmap bitmap;
		m_memFullDC = new CDC;
		if(!m_memFullDC->CreateCompatibleDC(pDC))
			return;
		bitmap.CreateCompatibleBitmap(pDC, nFullWidth, nFullHeight);

		m_memFullDC->SelectObject(&bitmap);
		ReleaseDC(pDC);		
	}
}
void CESMRemoteImage::DrawRect(BOOL bShow)
{
	CRect rc;
	GetClientRect(&rc);

	if(bShow)
	{

	}
	else
	{

	}

	

	Redraw();
}
void CESMRemoteImage::SetDC(int nWidth,int nHeight,BOOL bFull)
{
	if(bFull)//Full screen
	{
		if(nWidth != m_nFullWidth || nHeight != m_nFullHeight)
		{
			m_nFullWidth	= nWidth;	
			m_nFullHeight	= nHeight;

			MoveWindow(0,0,m_nFullWidth,m_nFullHeight);
			delete m_memFullDC;
			m_memFullDC = NULL;
		}
	}
}

void CESMRemoteImage::OnSize(UINT nType, int cx, int cy)
{
	CStatic::OnSize(nType, cx, cy);

}
