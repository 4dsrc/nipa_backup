#pragma once

#include <afxsock.h> 
#include <afxinet.h> 
#include "afxcmn.h"
#include "ESMUtilMultiplexerOptDlg.h"

#include "ESMUtilMultiplexerMuxer.h"
#include "ESMButtonEx.h"
#define DB_ADDRESS "13.57.98.197"
#define DB_ID "admin"
#define DB_PASS "5esmlab!"
#define DB_NAME "4dlive"

#define DB_PORT 3306
#define MAX_REFEREE_CNT 9
extern MYSQL mysql;
enum
{
	HTTP_CREATE_DIR,
	HTTP_SEND_FILE,
	HTTP_DELETE_DIR,
	HTTP_DELETE_FILE,
	HTTP_GET_FILE,
};
enum
{
	REFEREE_ERROR_NOERROR = 0,
	REFEREE_ERROR_MAKINGLIST,
	REFEREE_ERROR_DSC,
	REFEREE_ERROR_FILE,
	REFEREE_ERROR_4DPCONNECT,
};
// CESMUtilMultiplexerDlg 대화 상자입니다.
struct MuxDPInfo{	
	MuxDPInfo()
	{
		nStartTime = -1;
		nEndTime   = -1;
	}
	CString strCamID;
	CString strDstIP;
	int nStartTime;
	int nEndTime;
};
class CESMUtilMultiplexerDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CESMUtilMultiplexerDlg)

public:
	CESMUtilMultiplexerDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMUtilMultiplexerDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UTIL_MULTIPLEXER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

	int m_nFinish;

public:
	afx_msg void OnBnClickedMuxStart();
	afx_msg void OnEnChangeMuxSelectTime();
	afx_msg void OnEnChangeMuxTotalTime();
	afx_msg void OnBnClickedMuxSave();

	static unsigned WINAPI MuxThread(LPVOID param);
	static unsigned WINAPI MuxThreadDP(LPVOID param);
	static unsigned WINAPI FTPThread(LPVOID param);
	static unsigned WINAPI MjpgEncodingThread(LPVOID param);
	

	BOOL FileListUpdata(CString strCamID, int nCol ,CString strLog);
	BOOL CheckFile(CString strPath);
	void SetFinish(int nFinish);
	int	 GetFinish();

	void SetFinish(int nFinish, CString strCamID, int nRemotIP);

	void MuxInfoLoad();
	void SetMUXInfoint(CString strKeyName, int nValue);
	void SetMUXInfoString(CString strKeyName, CString strValue);
	CString GetMUXInfoString(CString strKeyName, CString strDefault);
	int GetMUXInfoint(CString strKeyName, int nDefault);
	
	int GetMakingInfoInt(int nDefault);

	void FTPListLoad();
	BOOL CheckFTPList(CString strPath);
	//170829 hjcho
	void SetMuxInfo();
	static unsigned WINAPI MuxDPTemplate(LPVOID param);
	void GetTimePath(CString str4dm,CString &strDay,CString &strTime);
	BOOL m_bCurMuxState;
	vector<MuxDPInfo>m_ArrMuxDPInfo;
	CString m_strFileName;
	int m_nTotalMakingCnt;

	//180723 hjcho
	int m_nGroupIndex;
	void SetRefereeInfo(int nGroupIdx,int nMovieIndex,int nFrameIdx);
	BOOL RefereeCreateMakingList(int nGroupIdx);
	BOOL RefereeCheckConnect4DP();
	BOOL RefereeSendDataTo4DP(int nStartMov,int nEndMov,CString strSavePath);
	int LaunchRefereeReading(int nGroupIdx);
	static unsigned WINAPI LaunchRefereeReading(LPVOID param);
	void ReceiveMuxData(char*pData,CString rSavePath,int nBodySize);
	static unsigned WINAPI LaunchReceiveData(LPVOID param);
	int m_nFirstMovieIdx;
	int m_nFirstFrameIdx;
public:
	CMainFrame *m_pMainWnd;

	//FTP
	CInternetSession m_session; 
	CFtpConnection * m_pConnection;
	CFtpFileFind *m_pFileFind;
	CString m_strRemoteDir;
	CESMNetworkListCtrl m_LcFTP;

	//FTP Info
	CString m_strFtpIP;
	CString m_strFtpID;
	CString m_strFtpPW;
	int m_nFtpPort;
	BOOL m_bFtpSend;
	
	//Mux
	CString m_strMUXSavePath;
	CString m_strMUXSavePathProfile;
	CString m_str4DLSavePath;
	CString m_str4DLSavePathProfile;

	CString m_strMakeingServerPath;
	int m_nTotalTime;
	int m_nOutWidth;
	int m_nOutHeight;
	BOOL m_bInsertCamID;
	BOOL m_bMakingWait;
	BOOL m_bMUXing;
	BOOL m_bMakeDP;
	BOOL m_bHalf;
	CComboBox m_combo;
	CProgressCtrl m_ProgressMux;

	//170622 hjcho
	BOOL m_bColorTrans;
	double m_dbArrColorInfo[3];
	CComboBox m_cbColorDSC;
	CString m_strBaseDSC;
	void InitColorDSCList();
	BOOL DecodingFrame(CString strPath,int nStartFrame = 0);

	//Mux Info
	int m_nStartFrame;
	int m_nEndFrame;
	int m_nSelectTime;
	int m_nStartTime;
	int m_nEndTime;

	//DP
	CObArray m_arRCServerList;
	int m_nMuxDataSendCount;
	int m_nMuxDataFinishCount;
	int m_nMjpgEncodingCount;

	CRITICAL_SECTION _Finish;
	afx_msg void OnNMCustomdrawListMux(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedMuxSetResolution();
	afx_msg void OnLvnItemchangedListMux(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedMuxBtnSetcolor();
	afx_msg void OnBnClickedMuxCheckColortrans();
	
	afx_msg void OnBnClickedMuxBtnAdd();
	afx_msg void OnBnClickedMuxBtnSub();
	afx_msg void OnBnClickedMuxDbNaming();

	/*DB Naming*/ // 180214 hjcho
	//CESMUtilMultiplexerOptDlg* m_pMuxOptDlg;
	CString m_strSUBL;
	CString m_strSUBM;
	CString m_strSUBS;

	CString m_strOBJ;
	CString m_strDES;
	CString m_strOwner;
	
	CString m_strTempOBJ;
	CString m_strPlayerInfo;

	CString m_strLogTime;
	void WriteMovieInfo(CFile* pFile);

	BOOL m_bTCP;
	afx_msg void OnBnClickedMuxMakedp();

	//180312
	int m_nMuxSendCnt;
	void WriteFTPList(CFile *file);
	BOOL m_bBothProc;
	double m_dbZoomRatio;

	//180412 hjcho
	BOOL m_bPSCP;
	BOOL m_bFTPListLoad;
	void RunHttp(CString strPath,CString strFilePath,CString strFileName,int nCase);
	CString m_strDay;
	CString m_strTime;
	int m_nEncodingMethod;
	afx_msg void OnBnClickedMuxRadioAndroid(UINT value);
	afx_msg void OnBnClickedMuxBothproc();

	BOOL m_bThird;
	CString m_strFileSave;
	CString m_str4DLSaveFile;
	BOOL Create4DLPath(CString strDay,CString strTime);
	void CreateFTPList();
	void CheckMakingDSC();
	int CheckConnect4DP();
	void SendMuxingData(int nStartMov,int nEndMov,CString str4DLPath);
	void SendMuxingDataLocal(int nStartMov,int nEndMov,CString str4DLPath);
	void WriteFinishFile(CString strStartTime);
	
	//DB Check
	BOOL m_dbConnect;
	CComboBox m_cbSubL;
	CComboBox m_cbSubM;
	CComboBox m_cbSubS;
	CComboBox m_cbOwner;
	void CreateDBInfo();
	void CreateDBOwnerInfo();
	afx_msg void OnBnClickedMuxDbBtnDbconntect();
	afx_msg void OnBnClickedMuxCheckPscp();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnCbnSelchangeMuxDbComboSubl();
	afx_msg void OnCbnSelchangeMuxDbComboSubm();
	afx_msg void OnCbnSelchangeMuxDbComboSubs();
	afx_msg void OnCbnSelchangeMuxDbComboOwner();

	//Opt 정리
	void SetOptControl();
	CComboBox m_cbPlayer;
	afx_msg void OnCbnSelchangeMuxDbComboPlayer();
	CString CreateSFGName();
	afx_msg void OnCbnEditchangeMuxDbComboPlayer();

	//UI
	CBrush m_background;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	//180611 hjcho
	BOOL SetDBInfo();

	//180628 hjcho
	CString m_strDBIPInfo;
	
	//180621
	BOOL m_bCheckInputWindowsData;
	CString m_strMp4SavePath;
	void ClearDBInformation();

	afx_msg void OnBnClickedCheckWindowsRecord();

	CESMButtonEx m_btnSetResolution;
	CESMButtonEx m_btnSetColorRevision;
	CESMButtonEx m_btnSetThridOne;
	CESMButtonEx m_btnDBConnect;
	CESMButtonEx m_btnRemove;
	CESMButtonEx m_btnAdd;
	CESMButtonEx m_btnMuxStart;
	CESMButtonEx m_btnInfoSave;
	CESMButtonEx m_btnCancel;
	BOOL m_bLowBitrate;
	afx_msg void OnCbnSelchangeMuxSelResolution();

	//wgkim 190212
	int m_nRadioButtonForContainer;
	afx_msg void OnBnClickedRadioButtonForContainer(UINT msg);

	int m_nBitrateInMbps;
	afx_msg void OnEnChangeEditBitrateValue();
	virtual BOOL PreTranslateMessage(MSG* pMsg);	

	//hjcho 190225
	void ShowOrHideWindows(BOOL bState);
	static unsigned WINAPI _LaunchRTSPVODThread(LPVOID param);
	CString CheckRTSPVODListAndMerge();
};

struct FTPListData
{
	FTPListData()
	{
		memset(Index, 0, 20);
		memset(CamID, 0, 20);
		memset(MovieMakeCheck, 0, 20);
		memset(FTPSendCheck, 0, 20);
	}
	char Index[20];
	char CamID[20];
	char MovieMakeCheck[20];
	char FTPSendCheck[20];
};
	

struct MuxThreadData
{
	CESMUtilMultiplexerDlg* pMgr;
	CString strCamID;
};
struct MuxRefereeData
{
	MuxRefereeData()
	{
		nGroupIdx = -1;
	}
	CESMUtilMultiplexerDlg* pMgr;
	int nGroupIdx;
};
struct MUX_RECEIVE_DATA
{
	MUX_RECEIVE_DATA()
	{
		pData = NULL;
		nBodySize = 0;
	}
	char* pData;
	CString strSavePath;
	int nBodySize;
};