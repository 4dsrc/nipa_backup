#include "stdafx.h"
#include "ESMAdjustAsAuto.h"


ESMAdjustAsAuto::ESMAdjustAsAuto(void)
{
	currentAdjustFeatures.push_back(Point2f(0, 0));
	currentAdjustFeatures.push_back(Point2f(0, 0));
	currentAdjustFeatures.push_back(Point2f(0, 0));
	calculatedAdjustFeatures.push_back(Point2f(0, 0));
	calculatedAdjustFeatures.push_back(Point2f(0, 0));
	calculatedAdjustFeatures.push_back(Point2f(0, 0));
}

ESMAdjustAsAuto::~ESMAdjustAsAuto(void)
{
}


void ESMAdjustAsAuto::InputAdjustFeaturesFromList(CListCtrl* pList, int nSelectedItem)
{	
	for (int i = 0 ; i < 3 ; i++)
	{
		CString strPosX = pList->GetItemText(nSelectedItem, i*2 + 3 );
		CString strPosY = pList->GetItemText(nSelectedItem, i*2 + 4);

		int nPosX = _ttoi(strPosX);
		int nPosY = _ttoi(strPosY);

		currentAdjustFeatures[i].x = nPosX;
		currentAdjustFeatures[i].y = nPosY;				
	}
}

void ESMAdjustAsAuto::ApplyCalculatedAdjustFeatureToList(CListCtrl* pList, int nSelectedItem)
{
	for(int i = 0; i < 3; i++)
	{
		CString strText;
		strText.Format(_T("%d"), (int)currentAdjustFeatures[i].x);
		pList->SetItemText(nSelectedItem, i * 2 + 3, strText);
		strText.Format(_T("%d"), (int)currentAdjustFeatures[i].y);
		pList->SetItemText(nSelectedItem, i * 2 + 4, strText);
	}
}

void ESMAdjustAsAuto::CalcAutoAdjustUsingOpticalFlow(Mat& srcImage)
{		
	if(previousImageForAdjust.empty()) 
	{					
		cvtColor(srcImage, previousImageForAdjust, cv::COLOR_RGB2GRAY, 1);		
	}

	cvtColor(srcImage, nextImageForAdjust, cv::COLOR_RGB2GRAY, 1);		

	if(currentAdjustFeatures.size() == 3)		
	{
		calcOpticalFlowPyrLK(previousImageForAdjust, nextImageForAdjust, 
			currentAdjustFeatures, calculatedAdjustFeatures, statusAdjustFeatures, 
			_err, cv::Size(21, 21), 3,
			TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 30, 0.001));
	}

	previousImageForAdjust = nextImageForAdjust.clone();	
	swap(currentAdjustFeatures, calculatedAdjustFeatures);
}

