////////////////////////////////////////////////////////////////////////////////
//
//	MainFrmDSC.cpp : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-23
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "4DMaker.h"
#include "MainFrm.h"
#include "ESMUtil.h"

#include "SdiSingleMgr.h"
#include "DSCMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2010-11-29 
//! @owner    Lee JungTaek
//! @note
//! @return        
//------------------------------------------------------------------------------
LRESULT CMainFrame::OnChangeConnect(WPARAM wParam, LPARAM lParam)
{	
	//-- 2014/09/08 hongsu.jung
	//-- Debug
	if(!m_wndDSCViewer)
	{
		ESMLog(0,_T("OnChangeConnect"));
		return 0L;
	}

	m_DevChangeCount++;

	if(m_DevChangeCount > 20)
	{
		CESMRCManager* pRCMgr = NULL;
		pRCMgr = (CESMRCManager *)m_wndNetwork.m_pRCClient;
		if(pRCMgr)
		{
			ESMEvent* pMsg = new ESMEvent();
			pMsg->message	= WM_RS_RC_SEND_USBDEVICE_CHECK;
			pRCMgr->AddMsg(pMsg);
		}

		m_DevChangeCount = 0;
	}

	return m_wndDSCViewer.m_pDSCMgr->CheckConnect();
}

BOOL CMainFrame::IsAnyConnection()
{
	//-- 2013-09-04 hongsu@esmlab.com
	/*
	int nAll = m_wndDSCListView.m_pListCtrl->GetItemCount();
	if(nAll)
		return TRUE;
	*/
	return FALSE;
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-04-27
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @revision		
//------------------------------------------------------------------------------
void CMainFrame::OnFocusModeCAF()
{
	CString strModel = GetCamModel();	
	
	if (strModel.CompareNoCase(_T("GH5")) == 0)
	{
		// GH5
	}
	else
	{
		// NX1
		ESMLog(5,_T("Change Auto Focus Mode"));	
		//-- 2013-09-04 hongsu@esmlab.com
		m_wndDSCViewer.SetFocusMode(TRUE);
	}
}

void CMainFrame::OnFocusModeMF()
{	
	CString strModel = GetCamModel();	

	if (strModel.CompareNoCase(_T("GH5")) == 0)
	{
		// GH5		
	}
	else
	{
		// NX1
		m_wndDSCViewer.m_pDSCMgr->DoS1Release();

		ESMLog(5,_T("Change Manual Focus Mode"));
		//-- 2013-09-04 hongsu@esmlab.com
		m_wndDSCViewer.SetFocusMode(FALSE);
		m_wndDSCViewer.GetFocusAll();
	}
	
}