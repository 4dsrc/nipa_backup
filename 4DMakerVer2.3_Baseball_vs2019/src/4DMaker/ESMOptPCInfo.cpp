// ESMOptPCInfo.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMOptPCInfo.h"
#include "afxdialogex.h"
#include "ESMFunc.h"

// ESMOptPCInfo 대화 상자입니다.

IMPLEMENT_DYNAMIC(ESMOptPCInfo, CDialogEx)

ESMOptPCInfo::ESMOptPCInfo()
	: CESMOptBase(ESMOptPCInfo::IDD)
{
	m_pcList.SetUseCamStatusColor(FALSE);
}

ESMOptPCInfo::~ESMOptPCInfo()
{
}

void ESMOptPCInfo::DoDataExchange(CDataExchange* pDX)
{
	CESMOptBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OPT_PC_LIST, m_pcList);
	DDX_Control(pDX, IDC_OPT_PC_BTN_DELETE, m_btnDelete);
	DDX_Control(pDX, IDC_OPT_PC_BTN_ADDALL, m_btnAddAll);
	DDX_Control(pDX, IDC_OPT_PC_BTN_RESET, m_btnReset);
}
BOOL ESMOptPCInfo::OnInitDialog()
{
	CESMOptBase::OnInitDialog();

	InitData();

	return TRUE;
}

BEGIN_MESSAGE_MAP(ESMOptPCInfo, CESMOptBase)
	ON_BN_CLICKED(IDC_OPT_PC_BTN_DELETE, &ESMOptPCInfo::OnBnClickedOptPcBtnDelete)
	ON_BN_CLICKED(IDC_OPT_PC_BTN_RESET, &ESMOptPCInfo::OnBnClickedOptPcBtnReset)
	ON_BN_CLICKED(IDC_OPT_PC_BTN_ADDALL, &ESMOptPCInfo::OnBnClickedOptPcBtnAddall)
	ON_BN_CLICKED(IDC_PCMAKINGUSE, &ESMOptPCInfo::OnBnClickedPcmakinguse)
END_MESSAGE_MAP()


// ESMOptPCInfo 메시지 처리기입니다.
BOOL ESMOptPCInfo::InitData()
{
	// 이곳은 Dialog가 초기 화해야는 모든 데이터를 여기서한다.
	m_pcList.InsertColumn(0, _T("NUM"), LVCFMT_LEFT, 47);
	m_pcList.InsertColumn(1, _T("IP"), LVCFMT_LEFT, 100);
	m_pcList.ModifyStyle(LVS_TYPEMASK, LVS_REPORT);
	m_pcList.SetExtendedStyle( LVS_EX_FULLROWSELECT );

	CheckDlgButton(IDC_PCMAKINGUSE, ESMGetMakingUse());

	check_makinguse = IsDlgButtonChecked(IDC_PCMAKINGUSE); 
	ESMSetMakingUse(check_makinguse);
	return TRUE;

}

void ESMOptPCInfo::InitProp(ESMPCInfo& path)
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\PCList.csv"), ESMGetPath(ESM_PATH_SETUP));

	if(!ReadFile.Open(strInputData, CFile::modeRead))
		return;

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());

	if(iLength == 0)
		return;

	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0, i = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;

	CString strNum, strDSCIP;

	while(1)
	{
		i++;
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;

		AfxExtractSubString(strDSCIP, sLine, 0, ',');
		strDSCIP.Trim();


		if(strDSCIP != _T(""))
		{
			strNum.Format(_T("%d"),i);
			strDSCIP.Replace( _T(" "), NULL );
			m_pcList.InsertItem(i-1, strNum);
			m_pcList.SetItemText(i-1, 1, strDSCIP);		
		}
		else
			break;
	}

	GetDlgItem(IDC_STATIC_PCLIST)->		SetWindowPos(NULL,0,0,200,24,NULL);
	GetDlgItem(IDC_OPT_PC_LIST)->		SetWindowPos(NULL,0,25,306,299,NULL);

	GetDlgItem(IDC_PCMAKINGUSE)->		SetWindowPos(NULL,337,140,148,14,NULL);
	GetDlgItem(IDC_OPT_PC_BTN_DELETE)->	SetWindowPos(NULL,337,182,148,34,NULL);
	GetDlgItem(IDC_OPT_PC_BTN_ADDALL)->	SetWindowPos(NULL,337,236,148,34,NULL);
	GetDlgItem(IDC_OPT_PC_BTN_RESET)->	SetWindowPos(NULL,337,290,148,34,NULL);

	UpdateData(FALSE);
}
BOOL ESMOptPCInfo::SaveProp(ESMPCInfo& path)
{
	if(!UpdateData(TRUE))
		return FALSE;

	path.nPCount = m_pcList.GetItemCount();
	CString strTp;
	for(int i=0; i<path.nPCount; i++)
	{
		strTp =  m_pcList.GetItemText(i, 2);
		path.strPCIP[i] = m_pcList.GetItemText(i, 1);	
	}
	return TRUE;
}	

void ESMOptPCInfo::GetIPList()
{
	CString strFile;
	m_IPList.clear();
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONNECT);	
	CESMIni ini;	
	if(ini.SetIniFilename (strFile))
	{
		CString strIPSection;
		CString strIP;
		int nIndex = 0;
		while(1)
		{
			strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);			
			strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);	
			if(!strIP.GetLength())
				break;		
			nIndex++;	
			m_IPList.push_back(strIP);
		}		
	}
	
	//if(!ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
	{
		strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_EX);
		if(ini.SetIniFilename (strFile))
		{
			CString strIPSection;
			CString strIP;
			CString strRemote;
			int nIndex = 0;
			while(1)
			{
				strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);			
				strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);	
				if(!strIP.GetLength())
					break;		
				nIndex++;

				strRemote = ini.GetString(strIP, _T("Remote"), _T("N"));
				if(strRemote.CompareNoCase(_T("N"))== 0)
					m_IPList.push_back(strIP);
			}		
		}
	}

}



void ESMOptPCInfo::OnBnClickedOptPcBtnDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.


	UpdateData();

	POSITION pos = NULL; 
	pos  = m_pcList.GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		while(pos)	//jhhan 16-11-01 복수선택
		{
			int nItemIdx = m_pcList.GetNextSelectedItem( pos );
			m_pcList.DeleteItem(nItemIdx);
			pos = m_pcList.GetFirstSelectedItemPosition();
		}
		
	}
	
	//m_bModify = TRUE;
}


void ESMOptPCInfo::OnBnClickedOptPcBtnReset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	UpdateData();

	m_pcList.DeleteAllItems();
}


void ESMOptPCInfo::OnBnClickedOptPcBtnAddall()
{
	m_pcList.DeleteAllItems();

	GetIPList();
	int nAll = m_IPList.size();	
	CString strNum;
	CString strIP;
	int nRowCount = 0;


	for(int i = 0 ; i < nAll ; i++)
	{
		strIP = m_IPList[i];
		if(strIP != "")
		{
			strNum.Format(_T("%d"), nRowCount+1);
			m_pcList.InsertItem(nRowCount, strNum);
			m_pcList.SetItemText(nRowCount, 1, strIP);
			nRowCount++;
		}
	}

	UpdateData(FALSE);
	
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void ESMOptPCInfo::OnBnClickedPcmakinguse()
{

	//CheckDlgButton(IDC_PCMAKINGUSE, ESMGetMakingUse());

	check_makinguse = IsDlgButtonChecked(IDC_PCMAKINGUSE); 
	ESMSetMakingUse(check_makinguse);
	CheckDlgButton(IDC_PCMAKINGUSE, ESMGetMakingUse());
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
