////////////////////////////////////////////////////////////////////////////////
//
//	4DMakerMainView.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "4DMakerMainView.h"
#include "MainFrm.h"

#include "ESMSplashWnd.h"

#include "ExtGridCellPropertyHeader.h"
#include "ExtGridCellDynamicStringWithJoinSupport.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static LPCTSTR g_arrHdrNames[] = {_T("Order ID"),_T("Customer"),_T("Employee"),_T("Order Date"),_T("Required Date"),_T("Shipped Date"),_T("Ship Via"),_T("Freight"),_T("Ship Name"),_T("Ship Address")};
static INT g_arrHdrExtents[] = { 70, 160, 110, 90, 100, 100, 110, 70, 160, 160 };

//C4DMakerMainView::data_row_t C4DMakerMainView::g_arrDataRowsInit[] = {};

static void stat_TextToDate(
	CExtGridCellVariant * pCellVariant,
	LPCTSTR strDate
	)
{
	ASSERT_VALID( pCellVariant );
	ASSERT_KINDOF( CExtGridCellVariant, pCellVariant );
	ASSERT( strDate != NULL );
	if( pCellVariant->vt != VT_EMPTY )
		pCellVariant->_VariantClear();
	ASSERT( pCellVariant->vt == VT_EMPTY );
int nLen = int( _tcslen( strDate ) );
	if( nLen == 0 )
		return;
	ASSERT( nLen >= 9 );
TCHAR strBufferDate[32], strBufferMonths[32], strBufferYear[32];
	::memset( strBufferDate, 0, sizeof(strBufferDate) );
	::memset( strBufferMonths, 0, sizeof(strBufferMonths) );
	::memset( strBufferYear, 0, sizeof(strBufferYear) );
int i;
	for( i = 0; (*strDate) != _T('/'); ++strDate, ++i )
	{
		if( i >= (sizeof(strBufferDate)/sizeof(strBufferDate[0])) )
		{
			ASSERT( FALSE );
			return;
		}
		TCHAR tchr = (*strDate);
		if( tchr == 0 )
		{
			ASSERT( FALSE );
			return;
		}
		if( ! _istdigit(tchr) )
		{
			ASSERT( FALSE );
			return;
		}
		strBufferDate[i] = tchr;
	} // for( int i = 0; (*strDate) != _T('/'); ++strDate, ++i )
	for( ++strDate, i = 0; (*strDate) != _T('/'); ++strDate, ++i )
	{
		if( i >= (sizeof(strBufferMonths)/sizeof(strBufferMonths[0])) )
		{
			ASSERT( FALSE );
			return;
		}
		TCHAR tchr = (*strDate);
		if( tchr == 0 )
		{
			ASSERT( FALSE );
			return;
		}
		if( ! _istalpha(tchr) )
		{
			ASSERT( FALSE );
			return;
		}
		strBufferMonths[i] = tchr;
	} // for( ++strDate, i = 0; (*strDate) != _T('/'); ++strDate, ++i )
	for( ++strDate, i = 0; (*strDate) != _T('/'); ++strDate, ++i )
	{
		if( i >= (sizeof(strBufferYear)/sizeof(strBufferYear[0])) )
		{
			ASSERT( FALSE );
			return;
		}
		TCHAR tchr = (*strDate);
		if( tchr == 0 )
			break;
		if( ! _istdigit(tchr) )
		{
			ASSERT( FALSE );
			return;
		}
		strBufferYear[i] = tchr;
	} // for( ++strDate, i = 0; (*strDate) != _T('/'); ++strDate, ++i )
SYSTEMTIME st;
	::memset( &st, 0, sizeof(SYSTEMTIME) );
	st.wDay = (WORD)_ttol( strBufferDate );
	st.wYear = (WORD)_ttol( strBufferYear );
	__EXT_MFC_STRLWR( strBufferMonths, 32 );
static LPCTSTR g_ArrMonthNames[12] =
{
	_T("january"), _T("february"), _T("march"),
	_T("april"), _T("may"), _T("june"),
	_T("july"), _T("august"), _T("september"),
	_T("october"), _T("november"), _T("december"),
};
	st.wMonth = 0xFFFF;
	for( i = 0; i < 12 && st.wMonth == 0xFFFF; i++ )
	{
		if( _tcscmp(strBufferMonths,g_ArrMonthNames[i]) == 0 )
			st.wMonth = WORD(i + 1);
	} // for( i = 0; i < 12 && st.wMonth == 0xFFFF; i++ )
	if( st.wMonth == 0xFFFF )
	{
		ASSERT( FALSE );
		return;
	}
COleDateTime _odt( st );
	if( _odt.m_status != COleDateTime::valid )
	{
		ASSERT( FALSE );
		return;
	}
	pCellVariant->date = _odt.m_dt;
	pCellVariant->vt = VT_DATE;
	pCellVariant->ModifyStyleEx( 0, __EGCS_EX_EMPTY );
}

/////////////////////////////////////////////////////////////////////////////
// C4DMakerMainView

C4DMakerMainView::C4DMakerMainView()
	: m_nPictureColNo( 0L )
	, m_nProductNameColNo( 0L )
	, m_nProductModelNameColNo( 0L )
	, m_nCultureNameColNo( 0L )
	, m_nProductSubcategoryName( 0L )
	, m_bEnableCellJoins( true )
{
}

C4DMakerMainView::~C4DMakerMainView()
{
POSITION pos = m_mapLoadedPictures.GetStartPosition();
	for( ; pos != NULL; )
	{
		WORD nResourceID = 0;
		CExtBitmap * pBmp = NULL;
		m_mapLoadedPictures.GetNextAssoc( pos, nResourceID, (void*&)pBmp );
		ASSERT( nResourceID != 0 );
		ASSERT( pBmp != NULL );
		delete pBmp;
	}
	m_mapLoadedPictures.RemoveAll();
}

CExtBitmap * C4DMakerMainView::_GetAndLoadBitmap( UINT nResourceID )
{
	ASSERT( nResourceID != 0 );
WORD w = (WORD)nResourceID;
CExtBitmap * pBmp = NULL;
	if( m_mapLoadedPictures.Lookup( w, (void*&)pBmp ) )
	{
		ASSERT( pBmp != NULL );
		return pBmp;
	}
	pBmp = new CExtBitmap;
	VERIFY( pBmp->LoadBMP_Resource( MAKEINTRESOURCE(nResourceID) ) );
//pBmp->Make32();
//pBmp->AlphaColor( RGB(255,255,255), RGB(128,128,128), 0 );
	m_mapLoadedPictures.SetAt( (WORD)nResourceID, (void*)pBmp );
	return pBmp;
}


BEGIN_MESSAGE_MAP( C4DMakerMainView, CExtGridWnd )
	//{{AFX_MSG_MAP(C4DMakerMainView)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

int C4DMakerMainView::OnCreate( LPCREATESTRUCT lpCreateStruct ) 
{
	if( CExtGridWnd::OnCreate( lpCreateStruct ) == -1 )
		return -1;
	Init();
	return 0;
}

void C4DMakerMainView::OnDestroy() 
{
	CExtGridWnd::OnDestroy();
}

void C4DMakerMainView::OnBeginPrintPreview(
	bool bPreviewMode
	)
{
	ASSERT_VALID( this );
	CExtNSB < CExtPPVW < CExtGridWnd > > :: OnBeginPrintPreview( bPreviewMode );
}

void C4DMakerMainView::OnEndPrintPreview(
	CDC * pDC,
	CPrintInfo * pInfo,
	POINT point,
	CExtPPVW_HostWnd * pWndPrintPreviewHost
	)
{
	ASSERT_VALID( this );
	CExtNSB < CExtPPVW < CExtGridWnd > > :: OnEndPrintPreview( pDC, pInfo, point, pWndPrintPreviewHost );
}

CExtGridCellHeader * C4DMakerMainView::ColumnRegister(
	LPCTSTR strColumnName,
	LPCTSTR strCategoryName,
	INT nColumnExtent,
	INT nColumnExtentMin, // = 5
	INT nColumnExtentMax // = 32000
	)
{
	ASSERT_VALID( this );
	ASSERT( strColumnName != NULL && _tcslen(strColumnName) > 0 );
	ASSERT( strCategoryName != NULL && _tcslen(strCategoryName) > 0 );
	ASSERT( nColumnExtent >= 0 );
LONG nColNo = ColumnCountGet();
	VERIFY( ColumnAdd( 1L, false ) );
CExtGridCellHeader * pHdr0 = STATIC_DOWNCAST( CExtGridCellHeader, GridCellGetOuterAtTop( nColNo, 0L, RUNTIME_CLASS(CExtGridCellHeader) ) );
	ASSERT_VALID( pHdr0 );
	pHdr0->TextSet( strCategoryName );
	pHdr0->ExtentSet( nColumnExtent, 0 );
	pHdr0->ExtentSet( nColumnExtentMin, -1 );
	pHdr0->ExtentSet( nColumnExtentMax, 1 );
CExtGridCellHeader * pHdr1 = STATIC_DOWNCAST( CExtGridCellHeader, GridCellGetOuterAtTop( nColNo, 1L, RUNTIME_CLASS(CExtGridCellHeader) ) );
	ASSERT_VALID( pHdr1 );
	pHdr1->TextSet( strColumnName );
	if( _tcscmp( strColumnName, strCategoryName ) == 0 )
	{
		VERIFY( GridCellJoinSet( CSize( 1, 2 ), nColNo, 0L, 0, -1, true, true, true ) );
	}
	else if( nColNo > 0L )
	{
		LONG nColJoin = nColNo - 1L;
		CExtGridCellHeader * pHdrPrev = STATIC_DOWNCAST( CExtGridCellHeader, GridCellGetOuterAtTop( nColJoin, 0L ) );
		ASSERT_VALID( pHdrPrev );
		if( (*pHdrPrev) == (*pHdr0) )
		{
			CSize sizeJoinPrev = pHdrPrev->JoinGet();
			if( sizeJoinPrev.cy < 2 )
			{
				if( sizeJoinPrev.cx < 0 )
				{
					nColJoin = nColNo - 1L + sizeJoinPrev.cx;
					pHdrPrev = STATIC_DOWNCAST( CExtGridCellHeader, GridCellGetOuterAtTop( nColJoin, 0L ) );
					ASSERT_VALID( pHdrPrev );
					sizeJoinPrev = pHdrPrev->JoinGet();
					ASSERT( sizeJoinPrev.cy == 1 && sizeJoinPrev.cx > 1 );
				}
				VERIFY( GridCellJoinSet( CSize( nColNo - nColJoin + 1, 1 ), nColJoin, 0L, 0, -1, true, true, true ) );
			}
		}
	}
	return pHdr1;
}


void C4DMakerMainView::Init()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	if( m_pSplash->GetSafeHwnd() != NULL )
		m_pSplash->AddTextLine( _T("Loading data...") );

	SiwModifyStyle(
		__ESIS_STH_PIXEL|__ESIS_STV_ITEM| __EGBS_SFM_CELLS_HV
			|__EGBS_RESIZING_CELLS_OUTER|__EGBS_DYNAMIC_RESIZING
			|__EGBS_MULTI_AREA_SELECTION|__EGBS_NO_HIDE_SELECTION
			|__EGBS_GRIDLINES
		,
		0,
		false
		);
	SiwModifyStyleEx(
		__EGBS_EX_CELL_TOOLTIPS_OUTER|__EGBS_EX_CELL_EXPANDING_INNER
			|__EGWS_EX_PM_COLORS,
		0,
		false
		);
// 	BseModifyStyle(
// 		0,
// 		__EGWS_BSE_DEFAULT,
// 		false
// 		);

	CExtPaintManager * pPM = PmBridge_GetPM();
		ASSERT_VALID( pPM );
		RowRemoveAll( false );
		ColumnRemoveAll( false );
		OuterRowCountTopSet( 2L, false );
		OuterColumnCountLeftSet( 1L, false );
		OuterColumnCountRightSet( 1L, false );
	INT nExt = pPM->UiScalingDo( 14, CExtPaintManager::__EUIST_X );
		OuterColumnWidthSet( true, 0L, nExt );
		OuterColumnWidthSet( false, 0L, nExt );
	INT nRowHeight = pPM->UiScalingDo( 19, CExtPaintManager::__EUIST_Y );
		OuterRowHeightSet( true, 0L, nRowHeight );
		OuterRowHeightSet( true, 1L, nRowHeight );
		nRowHeight = pPM->UiScalingDo( 30, CExtPaintManager::__EUIST_Y );

	if( m_pSplash->GetSafeHwnd() != NULL )
		m_pSplash->AddTextLine( _T("Registering columns ...") );
	CExtGridCellHeader * pHeaderCell = NULL;
	INT nInitialColumnExtentSmall = 40;
	INT nInitialColumnExtentNormal = 100;
	INT nInitialColumnExtentBig = 250;
	//INT nInitialColumnExtentCheck = 22;
	//INT nInitialColumnExtentColor = 60;
	INT nInitialColumnExtentNumber = 60;
	//INT nInitialColumnExtentCurrency = 60;
	INT nInitialColumnExtentDateTime = 80;
	INT nIconIndex;
	CExtCmdIcon _icon;

	// LargePhoto_AdditionalInformation
	pHeaderCell =
		ColumnRegister(
			_T("Photo"),
			_T("Photo"),
			250 //, 250, 250
			);
	ASSERT_VALID( pHeaderCell );
	// ProductName_ProductInformation
	pHeaderCell =
		ColumnRegister(
			_T("Product Name"),
			_T("Model Information"),
			nInitialColumnExtentNormal + 60
			);
	ASSERT_VALID( pHeaderCell );

	// ProductModelName_AdditionalInformation
	pHeaderCell =
		ColumnRegister(
			_T("Model Name"),
			_T("Model Information"),
			nInitialColumnExtentNormal
			);
	ASSERT_VALID( pHeaderCell );

	// Description_ProductInformation
	pHeaderCell =
		ColumnRegister(
			_T("Description"),
			_T("Model Information"),
			nInitialColumnExtentNormal // nInitialColumnExtentBig
			);
	ASSERT_VALID( pHeaderCell );
	VERIFY(
		_icon.m_bmpNormal.LoadBMP_Resource(
			MAKEINTRESOURCE(IDB_BITMAP_FIELD_DESCRIPTION)
			)
		);
	nIconIndex = GridIconInsert( &_icon );
	ASSERT( nIconIndex >= 0 );
	pHeaderCell->IconIndexSet( nIconIndex );

	// ProductNumber_ProductInformation
	pHeaderCell =
		ColumnRegister(
			_T("Number"),
			_T("Model Information"),
			nInitialColumnExtentNormal
			);
	ASSERT_VALID( pHeaderCell );

// 	// MakeFlag_ProductInformation
// 	pHeaderCell =
// 		ColumnRegister(
// 			_T("Make Flag"),
// 			_T("Main Information"),
// 			nInitialColumnExtentCheck,
// 			nInitialColumnExtentCheck,
// 			nInitialColumnExtentCheck
// 			);
// 	ASSERT_VALID( pHeaderCell );
// 	VERIFY(
// 		_icon.m_bmpNormal.LoadBMP_Resource(
// 			MAKEINTRESOURCE(IDB_BITMAP_FIELD_MAKE_FLAG),
// 			RT_BITMAP,
// 			NULL,
// 			true
// 			)
// 		);
// 	_icon.m_bmpNormal.AlphaColor( RGB(255,0,255), RGB(0,0,0), 0 );
// 	nIconIndex = GridIconInsert( &_icon );
// 	ASSERT( nIconIndex >= 0 );
// 	pHeaderCell->IconIndexSet( nIconIndex );
// 	pHeaderCell->ModifyStyle( __EGCS_TA_HORZ_CENTER|__EGCS_ICA_HORZ_CENTER|__EGCS_ICA_VERT_MIDDLE );

// 	// FinishedGoodsFlag_ProductInformation
// 	pHeaderCell =
// 		ColumnRegister(
// 			_T("Finished Goods Flag"),
// 			_T("Main Information"),
// 			nInitialColumnExtentCheck,
// 			nInitialColumnExtentCheck,
// 			nInitialColumnExtentCheck
// 			);
// 	ASSERT_VALID( pHeaderCell );
// 	VERIFY(
// 		_icon.m_bmpNormal.LoadBMP_Resource(
// 			MAKEINTRESOURCE(IDB_BITMAP_FIELD_FINISHED_GOODS_FLAG),
// 			RT_BITMAP,
// 			NULL,
// 			true
// 			)
// 		);
// 	_icon.m_bmpNormal.AlphaColor( RGB(255,0,255), RGB(0,0,0), 0 );
// 	nIconIndex = GridIconInsert( &_icon );
// 	ASSERT( nIconIndex >= 0 );
// 	pHeaderCell->IconIndexSet( nIconIndex );
// 	pHeaderCell->ModifyStyle( __EGCS_TA_HORZ_CENTER|__EGCS_ICA_HORZ_CENTER|__EGCS_ICA_VERT_MIDDLE );

// 	// Color_ProductInformation
// 	pHeaderCell =
// 		ColumnRegister(
// 			_T("Color"),
// 			_T("Main Information"),
// 			nInitialColumnExtentColor
// 			);
// 	ASSERT_VALID( pHeaderCell );

// 	// SafetyStockLevel_AdditionalInformation
// 	pHeaderCell =
// 		ColumnRegister(
// 			_T("Safety Stock Level"),
// 			_T("Secondary Information"),
// 			nInitialColumnExtentNumber 
// 			);
// 	ASSERT_VALID( pHeaderCell );

// 	// ReorderPoint_AdditionalInformation
// 	pHeaderCell =
// 		ColumnRegister(
// 			_T("Reorder Point"),
// 			_T("Secondary Information"),
// 			nInitialColumnExtentNumber
// 			);
// 	ASSERT_VALID( pHeaderCell );

	// StandardCost_AdditionalInformation
	pHeaderCell =
		ColumnRegister(
			_T("Standard Cost"),
			_T("Price Information"),
			nInitialColumnExtentNormal // nInitialColumnExtentCurrency
			);
	ASSERT_VALID( pHeaderCell );

	// ListPrice_AdditionalInformation
	pHeaderCell =
		ColumnRegister(
			_T("List Price"),
			_T("Price Information"),
			nInitialColumnExtentNormal // nInitialColumnExtentCurrency
			);
	ASSERT_VALID( pHeaderCell );

	// Size_ProductInformation
	pHeaderCell =
		ColumnRegister(
			_T("Size"),
			_T("Details"),
			nInitialColumnExtentSmall
			);
	ASSERT_VALID( pHeaderCell );

	// Weight_ProductInformation
	pHeaderCell =
		ColumnRegister(
			_T("Weight"),
			_T("Details"),
			nInitialColumnExtentNumber
			);
	ASSERT_VALID( pHeaderCell );

	// WeightUnitMeasureCode_Production
	pHeaderCell =
		ColumnRegister(
			_T("Weight Unit Measure Code"),
			_T("Details"),
			nInitialColumnExtentSmall
			);
	ASSERT_VALID( pHeaderCell );

	// SizeUnitMeasureCode_Production
	pHeaderCell =
		ColumnRegister(
			_T("Size Unit Measure Code"),
			_T("Details"),
			nInitialColumnExtentSmall // 22, 22, 22
			);
	ASSERT_VALID( pHeaderCell );
	pHeaderCell->IconIndexSet( nIconIndex );

	// DaysToManufacture_AdditionalInformation
	pHeaderCell =
		ColumnRegister(
			_T("Days To Manufacture"),
			_T("Details"),
			nInitialColumnExtentNumber
			);
	ASSERT_VALID( pHeaderCell );

	// SellStartDate_AdditionalInformation
	pHeaderCell =
		ColumnRegister(
			_T("Start Date"),
			_T("Sell Information"),
			nInitialColumnExtentDateTime
			);
	ASSERT_VALID( pHeaderCell );

	// SellEndDate_AdditionalInformation
	pHeaderCell =
		ColumnRegister(
			_T("End Date"),
			_T("Sell Information"),
			nInitialColumnExtentDateTime
			);
	ASSERT_VALID( pHeaderCell );

// 	// CatalogDescription_AdditionalInformation
// 	pHeaderCell =
// 		ColumnRegister(
// 			_T("Catalog Description"),
// 			_T("Secondary Information"),
// 			nInitialColumnExtentNormal
// 			);
// 	ASSERT_VALID( pHeaderCell );

// 	// ThumbnailPhoto_AdditionalInformation
// 	pHeaderCell =
// 		ColumnRegister(
// 			_T("Photo"), // _T("Thumbnail Photo"),
// 			_T("Secondary Information"),
// 			24, 24, 24
// 			);
// 	ASSERT_VALID( pHeaderCell );
// 	VERIFY(
// 		_icon.m_bmpNormal.LoadBMP_Resource(
// 			MAKEINTRESOURCE(IDB_BITMAP_FIELD_PHOTO)
// 			)
// 		);
// 	nIconIndex = GridIconInsert( &_icon );
// 	ASSERT( nIconIndex >= 0 );
// 	pHeaderCell->IconIndexSet( nIconIndex );

	// CultureName_AdditionalInformation
	
	pHeaderCell;

	CExtGridCell * pCell = NULL;
	pCell = GridCellGet( 0, 0, -1, -1, RUNTIME_CLASS(CExtGridCellHeader) );
	pCell = GridCellGet( 0, 1, -1, -1, RUNTIME_CLASS(CExtGridCellHeader) );
	GridCellJoinSet( CSize(1,2), 0, 0, -1, -1 );
	pCell = GridCellGet( 0, 0, +1, -1, RUNTIME_CLASS(CExtGridCellHeader) );
	pCell = GridCellGet( 0, 1, +1, -1, RUNTIME_CLASS(CExtGridCellHeader) );
	GridCellJoinSet( CSize(1,2), 0, 0, +1, -1 );

	CExtBitmap * pBmp = NULL;
	CString strSplashStatus;
	/*
	LONG nRowNo, nRowCount = LONG( sizeof(g_arrDataRowsInit) / sizeof(g_arrDataRowsInit[0]) );
	VERIFY( RowAdd( nRowCount, false ) );
	for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
	{
		if( (nRowNo%50) == 0 )
		{
			strSplashStatus.Format(
				_T("%d rows inserted, %d %% done"),
				int(nRowNo),
				int( ::MulDiv( nRowNo, 100, nRowCount ) )
				);
			if( m_pSplash->GetSafeHwnd() != NULL )
				m_pSplash->AddTextLine( strSplashStatus, ( nRowNo == 0 ) ? false : true );
		}

		CExtGridCellPropertyHeader * pCellPropHeader =
			STATIC_DOWNCAST(
				CExtGridCellPropertyHeader,
				GridCellGetOuterAtLeft( 0L, nRowNo, RUNTIME_CLASS(CExtGridCellPropertyHeader) )
			);
		ASSERT_VALID( pCellPropHeader );
		pCellPropHeader->m_psRow.m_nRowNo = nRowNo;
		pCellPropHeader->ExtentSet( nRowHeight );
		pCellPropHeader->ModifyStyle( __EGCS_HDR_FOCUS_ARROW_RESERVE_SPACE|__EGCS_HDR_FOCUS_ARROW_DISPLAY );

		pCell = GridCellGetOuterAtRight( 0L, nRowNo, RUNTIME_CLASS(CExtGridCellHeader) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_HDR_FOCUS_ARROW_RESERVE_SPACE|__EGCS_HDR_FOCUS_ARROW_DISPLAY );

		data_row_t & _dr = g_arrDataRowsInit[ nRowNo ];
		USES_CONVERSION;
		LONG nColNo = 0L;

		// LargePhoto_AdditionalInformation;
		pBmp = _GetAndLoadBitmap( _dr.LargePhoto_AdditionalInformation );
		ASSERT( pBmp != NULL );
		m_nPictureColNo = nColNo;
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellPictureRef) );
		ASSERT_VALID( pCell );
		((CExtGridCellPictureRef*)pCell)->BitmapSetBuffer( pBmp );
		//((CExtGridCellPictureBase*)pCell)->ImageModeSet( CExtGridCellPictureBase::eStretch );
		pCell->ModifyStyle( __EGCS_ICA_HORZ_CENTER|__EGCS_ICA_VERT_TOP );
		// ProductName_ProductInformation
		m_nProductNameColNo = nColNo;
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(_dr.ProductName_ProductInformation) );
		// ProductModelName_AdditionalInformation
		m_nProductModelNameColNo = nColNo;
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS | __EGCS_TA_VERT_TOP );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(_dr.ProductModelName_AdditionalInformation) );
		// Description_ProductInformation
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER | __EGCS_EX_MULTILINE_TEXT );
		pCell->TextSet( OLE2CT(_dr.Description_ProductInformation) );
		// ProductNumber_ProductInformation
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(_dr.ProductNumber_ProductInformation) );
// 		// MakeFlag_ProductInformation
// 		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellCheckBox) );
// 		ASSERT_VALID( pCell );
// 		pCell->ModifyStyle( __EGCS_NO_INPLACE_CONTROL|__EGCS_ICA_HORZ_CENTER|__EGCS_ICA_VERT_MIDDLE );
// 		((CExtGridCellCheckBox*)pCell)->SetCheck( _dr.MakeFlag_ProductInformation ? true : false );
// 		// FinishedGoodsFlag_ProductInformation
// 		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellCheckBox) );
// 		ASSERT_VALID( pCell );
// 		pCell->ModifyStyle( __EGCS_NO_INPLACE_CONTROL|__EGCS_ICA_HORZ_CENTER|__EGCS_ICA_VERT_MIDDLE );
// 		((CExtGridCellCheckBox*)pCell)->SetCheck( _dr.FinishedGoodsFlag_ProductInformation ? true : false );
// 		// Color_ProductInformation
// 		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellColor) );
// 		ASSERT_VALID( pCell );
// 		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS|__EGCS_BUTTON_ELLIPSIS );
// 		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
// 		pCell->TextSet( OLE2CT(_dr.Color_ProductInformation) );
// 		//((CExtGridCellColor*)pCell)->m_bEnableButtonDefaultColor = true;
// 		// SafetyStockLevel_AdditionalInformation
// 		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellUpDown) );
// 		ASSERT_VALID( pCell );
// 		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
// 		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
// 		((CExtGridCellUpDown*)pCell)->_VariantAssign( _dr.SafetyStockLevel_AdditionalInformation, VT_I4 );
// 		// ReorderPoint_AdditionalInformation
// 		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellUpDown) );
// 		ASSERT_VALID( pCell );
// 		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
// 		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
// 		((CExtGridCellUpDown*)pCell)->_VariantAssign( _dr.ReorderPoint_AdditionalInformation, VT_I4 );
		// StandardCost_AdditionalInformation
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellCurrency) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		((CExtGridCellCurrency*)pCell)->_VariantAssign( _dr.StandardCost_AdditionalInformation, VT_R8 );
		// ListPrice_AdditionalInformation
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellCurrency) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		((CExtGridCellCurrency*)pCell)->_VariantAssign( _dr.ListPrice_AdditionalInformation, VT_R8 );
		// Size_ProductInformation
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TA_HORZ_CENTER|__EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(_dr.Size_ProductInformation) );
		// Weight_ProductInformation
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellNumber) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		((CExtGridCellNumber*)pCell)->_VariantAssign( _dr.Weight_ProductInformation, VT_R8 );
		// WeightUnitMeasureCode_Production
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(_dr.WeightUnitMeasureCode_Production) );
		// SizeUnitMeasureCode_Production
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TA_HORZ_CENTER|__EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(_dr.SizeUnitMeasureCode_Production) );
// 		// Style_ProductInformation
// 		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
// 		ASSERT_VALID( pCell );
// 		pCell->ModifyStyle( __EGCS_TA_HORZ_CENTER|__EGCS_TEXT_ELLIPSIS );
// 		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
// 		pCell->TextSet( OLE2CT(_dr.Style_ProductInformation) );
// 		// Class_ProductInformation
// 		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
// 		ASSERT_VALID( pCell );
// 		pCell->ModifyStyle( __EGCS_TA_HORZ_CENTER|__EGCS_TEXT_ELLIPSIS );
// 		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
// 		pCell->TextSet( OLE2CT(_dr.Class_ProductInformation) );
// 		// ProductLine_ProductInformation
// 		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
// 		ASSERT_VALID( pCell );
// 		pCell->ModifyStyle( __EGCS_TA_HORZ_CENTER|__EGCS_TEXT_ELLIPSIS );
// 		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
// 		pCell->TextSet( OLE2CT(_dr.ProductLine_ProductInformation) );
		// DaysToManufacture_AdditionalInformation
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellUpDown) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		((CExtGridCellUpDown*)pCell)->_VariantAssign( _dr.DaysToManufacture_AdditionalInformation, VT_I4 );
		// SellStartDate_AdditionalInformation
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDateTime) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS|__EGCS_BUTTON_DROPDOWN );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		((CExtGridCellDateTime*)pCell)->SetMode( CExtGridCellDateTime::date );
		((CExtGridCellDateTime*)pCell)->SetDateTime( DATE(_dr.SellStartDate_AdditionalInformation) );
		// SellEndDate_AdditionalInformation
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDateTime) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS|__EGCS_BUTTON_DROPDOWN );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		((CExtGridCellDateTime*)pCell)->SetMode( CExtGridCellDateTime::date );
		((CExtGridCellDateTime*)pCell)->SetDateTime( DATE(_dr.SellEndDate_AdditionalInformation) );
// 		// CatalogDescription_AdditionalInformation
// 		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
// 		ASSERT_VALID( pCell );
// 		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
// 		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
// 		pCell->TextSet( OLE2CT(_dr.CatalogDescription_AdditionalInformation) );
// 		// ThumbnailPhoto_AdditionalInformation;
// 		pBmp = _GetAndLoadBitmap( _dr.ThumbnailPhoto_AdditionalInformation );
// 		ASSERT( pBmp != NULL );
// 		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellPictureRef) );
// 		ASSERT_VALID( pCell );
// 		//((CExtGridCellPictureRef*)pCell)->ImageModeSet( CExtGridCellPicture::eStretch );
// 		((CExtGridCellPictureRef*)pCell)->BitmapSetBuffer( pBmp );
		// CultureName_AdditionalInformation
		m_nCultureNameColNo = nColNo;
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(_dr.CultureName_AdditionalInformation) );
		// CultureDescritpion_AdditionalInformation
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(_dr.CultureDescritpion_AdditionalInformation) );
		// ProductSubcategoryName_AdditionalInformation
		m_nProductSubcategoryName = nColNo;
		pCell = GridCellGet( nColNo++, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellDynamicStringWithJoinSupport) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS | __EGCS_TA_VERT_TOP );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(_dr.ProductSubcategoryName_AdditionalInformation) );

		if( nRowNo > 0L )
			JoinDataForRow( nRowNo );

		pCellPropHeader->ScanProperties( *this, nRowNo );
	} // for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
	strSplashStatus.Format(
		_T("%d rows inserted, 100 %% done"),
		int(nRowCount)
		);
	*/

	if( m_pSplash->GetSafeHwnd() != NULL )
		m_pSplash->AddTextLine( strSplashStatus, true );

	Sleep( 200 );
	OnSwUpdateScrollBars();
	OnSwDoRedraw();
}

void C4DMakerMainView::JoinDataForRow( LONG nRowNo )
{
	ASSERT_VALID( this );
LONG nRowJoin = nRowNo - 1L;
CExtGridCell * pCellCurr = GridCellGet( m_nPictureColNo, nRowNo );
	ASSERT_VALID( pCellCurr );
CExtGridCell * pCellPrev = GridCellGet( m_nPictureColNo, nRowJoin );
	ASSERT_VALID( pCellPrev );
	if( LPVOID(((CExtGridCellPictureBase*)pCellCurr)->BitmapGetBuffer()) == LPVOID(((CExtGridCellPictureBase*)pCellPrev)->BitmapGetBuffer()) )
	{
		CExtGridCell * pCellPrev = GridCellGet( m_nPictureColNo, nRowJoin );
		ASSERT_VALID( pCellPrev );
		CSize sizeJoinPrev = pCellPrev->JoinGet();
		if( sizeJoinPrev.cy < 0 )
		{
			nRowJoin = nRowNo - 1L + sizeJoinPrev.cy;
			pCellPrev = GridCellGet( m_nPictureColNo, nRowJoin );
			ASSERT_VALID( pCellPrev );
			sizeJoinPrev = pCellPrev->JoinGet();
			ASSERT( sizeJoinPrev.cy > 1 && sizeJoinPrev.cx == 1 );
		}
		CSize sizeReJoin( 1, nRowNo - nRowJoin + 1 );
		VERIFY( GridCellJoinSet( sizeReJoin, m_nPictureColNo,           nRowJoin, 0, 0, true, true, true ) );
		VERIFY( GridCellJoinSet( sizeReJoin, m_nProductModelNameColNo,  nRowJoin, 0, 0, true, true, true ) );
		VERIFY( GridCellJoinSet( sizeReJoin, m_nProductSubcategoryName, nRowJoin, 0, 0, true, true, true ) );
	}

	nRowJoin = nRowNo - 1L;
	pCellCurr = GridCellGet( m_nProductNameColNo, nRowNo );
	ASSERT_VALID( pCellCurr );
	pCellPrev = GridCellGet( m_nProductNameColNo, nRowJoin );
	ASSERT_VALID( pCellPrev );
	if( (*pCellCurr) == (*pCellPrev) )
	{
		CSize sizeJoinPrev = pCellPrev->JoinGet();
		if( sizeJoinPrev.cy < 0 )
		{
			nRowJoin = nRowNo - 1L + sizeJoinPrev.cy;
			pCellPrev = GridCellGet( m_nProductNameColNo, nRowJoin );
			ASSERT_VALID( pCellPrev );
			sizeJoinPrev = pCellPrev->JoinGet();
			ASSERT( sizeJoinPrev.cy > 1 && sizeJoinPrev.cx == 1 );
			pCellPrev->ModifyStyle( __EGCS_TA_VERT_TOP );
		}
		CSize sizeReJoin( 1, nRowNo - nRowJoin + 1 );
		VERIFY( GridCellJoinSet( sizeReJoin, m_nProductNameColNo, nRowJoin, 0, 0, true, true, true ) );
	}

	nRowJoin = nRowNo - 1L;
	pCellCurr = GridCellGet( m_nCultureNameColNo, nRowNo );
	ASSERT_VALID( pCellCurr );
	pCellPrev = GridCellGet( m_nCultureNameColNo, nRowJoin );
	ASSERT_VALID( pCellPrev );
	if( (*pCellCurr) == (*pCellPrev) )
	{
		CSize sizeJoinPrev = pCellPrev->JoinGet();
		if( sizeJoinPrev.cy < 0 )
		{
			nRowJoin = nRowNo - 1L + sizeJoinPrev.cy;
			pCellPrev = GridCellGet( m_nCultureNameColNo, nRowJoin );
			ASSERT_VALID( pCellPrev );
			sizeJoinPrev = pCellPrev->JoinGet();
			ASSERT( sizeJoinPrev.cy > 1 && sizeJoinPrev.cx == 1 );
			pCellPrev->ModifyStyle( __EGCS_TA_VERT_TOP );
		}
		CSize sizeReJoin( 1, nRowNo - nRowJoin + 1 );
		VERIFY( GridCellJoinSet( sizeReJoin, m_nCultureNameColNo, nRowJoin, 0, 0, true, true, true ) );
	}

}

void C4DMakerMainView::OnGridCellInplaceControlTextInputComplete(
	CExtGridCell & _cell,
	HWND hWndInplaceControl,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	__EXT_MFC_SAFE_LPCTSTR sTextNew,
	bool bSaveChanges
	)
{
	ASSERT_VALID( this );
	CExtGridWnd::OnGridCellInplaceControlTextInputComplete(
		_cell,
		hWndInplaceControl,
		nVisibleColNo,
		nVisibleRowNo,
		nColNo,
		nRowNo,
		nColType,
		nRowType,
		sTextNew,
		bSaveChanges
		);
	if( bSaveChanges && nColType == 0 && nRowType == 0 )
		HandleValueEditingComplete( nColNo, nRowNo, NULL, NULL );
}

void C4DMakerMainView::HandleValueEditingComplete(
	LONG nColNo,
	LONG nRowNo,
	CExtPropertyGridWnd * pPGW,
	CLocalPropertyValue * pPropertyValueL
	)
{
	ASSERT_VALID( this );
	ASSERT( 0 <= nColNo && nColNo < ColumnCountGet() );
	ASSERT( 0 <= nRowNo && nRowNo < RowCountGet() );
	ASSERT(
			( pPGW == NULL && pPropertyValueL == NULL )
		||	( pPGW != NULL && pPropertyValueL != NULL )
		);
	pPGW;
	if( pPropertyValueL == NULL )
	{
		CMainFrame * pMainFrame = STATIC_DOWNCAST( CMainFrame, ::AfxGetMainWnd() );
		ASSERT_VALID( pMainFrame );
		//-- 2013-04-22 hongsu.jung
		//-- Add Code to Property
		// pMainFrame->m_wndPGC.PropertyStoreSynchronize();
	}
	CRect rcInvalidate;
	if( GridCellRectsGet( nColNo, nRowNo, 0, 0, NULL, &rcInvalidate ) )
		InvalidateRect( &rcInvalidate );
}

CRect & C4DMakerMainView::_SelectionAreaConvert( CRect & rcArea ) const
{
	ASSERT_VALID( this );
	//CExtGridWnd::_SelectionAreaConvert( rcArea );
	rcArea.left = 0;
	rcArea.right = ColumnCountGet() - 1;
	return rcArea;
}

bool C4DMakerMainView::SelectionGetForCellPainting(
	LONG nColNo,
	LONG nRowNo
	) const
{
	ASSERT_VALID( this );
	if( nColNo == m_nPictureColNo )
		return false;
	if( ! CExtGridWnd::SelectionGetForCellPainting( nColNo, nRowNo ) )
		return false;
CPoint ptFocus = FocusGet();
	if( ptFocus.x == nColNo && ptFocus.y == nRowNo )
		return false;
	return true;
}

CSize C4DMakerMainView::OnGbwCellJoinQueryInfo(
	LONG nColNo,
	LONG nRowNo,
	INT nColType, // = 0
	INT nRowType // = 0
	) const
{
	ASSERT_VALID( this );
	if( m_bEnableCellJoins )
		return CExtGridWnd::OnGbwCellJoinQueryInfo( nColNo, nRowNo, nColType, nRowType );
	else
		return CSize( 1, 1 );
}

void C4DMakerMainView::OnGbwSelectionChanged()
{
	ASSERT_VALID( this );
	CMainFrame * pMainFrame = STATIC_DOWNCAST( CMainFrame, ::AfxGetMainWnd() );
	ASSERT_VALID( pMainFrame );
	//-- 2013-04-22 hongsu.jung
	//-- Add Code to Property
	/*
	CExtPropertyStore * pPS = pMainFrame->m_wndPGC.PropertyStoreGet();
	ASSERT_VALID( pPS );
	if( LPVOID(pPS) == LPVOID(&(pMainFrame->m_wndPGC.m_psCombinedSelection)) )
	{
		pPS->ItemRemove();
		LONG nRowNow = SelectionGetFirstRowInColumn( 0L );
		for( ; nRowNow >= 0; nRowNow = SelectionGetNextRowInColumn( 0L, nRowNow ) )
		{
			CExtGridCellPropertyHeader * pHdr =
				STATIC_DOWNCAST(
					CExtGridCellPropertyHeader,
					GridCellGetOuterAtLeft( 0L, nRowNow )
					);
			pPS->Combine( &(pHdr->m_psRow) );
		}
		pMainFrame->m_wndPGC.PropertyStoreSynchronize();
	}
	*/
	CExtGridWnd::OnGbwSelectionChanged();
}

void C4DMakerMainView::OnGbwFocusChanged(
	const POINT & ptOldFocus,
	const POINT & ptNewFocus
	)
{
	ASSERT_VALID( this );
	CMainFrame * pMainFrame = STATIC_DOWNCAST( CMainFrame, ::AfxGetMainWnd() );
	ASSERT_VALID( pMainFrame );
	//-- 2013-04-22 hongsu.jung
	//-- Change To Property
	/*
	CExtPropertyStore * pPS = pMainFrame->m_wndPGC.PropertyStoreGet();
	ASSERT_VALID( pPS );
	if( LPVOID(pPS) == LPVOID(&(pMainFrame->m_wndPGC.m_psFocusedRow)) )
	{
		pPS->ItemRemove();
		if( ptNewFocus.y >= 0 )
		{
			CExtGridCellPropertyHeader * pHdr =
				STATIC_DOWNCAST(
					CExtGridCellPropertyHeader,
					GridCellGetOuterAtLeft( 0L, ptNewFocus.y )
					);
			pPS->Combine( &(pHdr->m_psRow) );
		}
		pMainFrame->m_wndPGC.PropertyStoreSynchronize();
	}
	*/
	CExtGridWnd::OnGbwFocusChanged( ptOldFocus, ptNewFocus );
}
