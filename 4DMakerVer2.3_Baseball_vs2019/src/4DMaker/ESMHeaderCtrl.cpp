#include "stdafx.h"
#include "ESMHeaderCtrl.h"


CESMHeaderCtrl::CESMHeaderCtrl()
{
	m_clrBk = RGB(22, 22, 22);
	m_clrBorder = RGB(22,22,22);
	m_clrText = RGB(255,255,255);
}


CESMHeaderCtrl::~CESMHeaderCtrl()
{
}

BEGIN_MESSAGE_MAP(CESMHeaderCtrl, CHeaderCtrl)

	ON_WM_ERASEBKGND()
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnCustomDraw)

END_MESSAGE_MAP()

BOOL CESMHeaderCtrl::OnEraseBkgnd(CDC* pDC)
{
	return FALSE;
}



void CESMHeaderCtrl::OnCustomDraw(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMCUSTOMDRAW lplvcd = (LPNMCUSTOMDRAW)pNMHDR;

	switch(lplvcd->dwDrawStage)
	{
	case CDDS_PREPAINT:
		{
			CRect rcItem(lplvcd->rc);
			CDC *pDC= CDC::FromHandle(lplvcd->hdc);
			pDC->FillSolidRect(&rcItem, m_clrBk);
		}
		*pResult = CDRF_NOTIFYITEMDRAW;

		break;

	case CDDS_ITEMPREPAINT:
		{
			CRect rcItem(lplvcd->rc);
			CDC *pDC= CDC::FromHandle(lplvcd->hdc);
			int nItemID = lplvcd->dwItemSpec;
			TCHAR buf[256];
			HD_ITEM hdi;
			CPen penBorder;
			hdi.mask = HDI_TEXT | HDI_FORMAT;
			hdi.pszText = buf;
			hdi.cchTextMax = 255;
			GetItem( nItemID, &hdi );

			UINT uFormat = DT_SINGLELINE | DT_VCENTER |
				((hdi.fmt & HDF_LEFT) ? DT_LEFT :
				(hdi.fmt & HDF_RIGHT) ? DT_RIGHT : DT_CENTER);

			pDC->FillSolidRect(rcItem, m_clrBk);

			// 테두리 그리기
 			penBorder.CreatePen(PS_SOLID, 1, m_clrBorder);
 			CPen *pOldPen = pDC->SelectObject(&penBorder);
// 			pDC->MoveTo(rcItem.left, rcItem.bottom);
// 			pDC->LineTo(rcItem.left, rcItem.top);
// 			pDC->LineTo(rcItem.right, rcItem.top);

			// 텍스트 그리기
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(m_clrText);
			pDC->SelectObject(m_pFont);
			pDC->DrawText(hdi.pszText, &rcItem, uFormat);
			pDC->SelectObject(pOldPen);

			penBorder.DeleteObject();

			*pResult= CDRF_SKIPDEFAULT;

		}

		break;

	default:// it wasn't a notification that was interesting to us.
		*pResult = CDRF_DODEFAULT;
		break;
	}
}


void CESMHeaderCtrl::PreSubclassWindow()
{
	CHeaderCtrl::PreSubclassWindow();

	// Font set
	m_pFont = GetFont();
	//CFont NewFont;
	//NewFont.CreatePointFont(120,_T("Noto Sans CJK KR"));

	//SetFont(&NewFont);
}
