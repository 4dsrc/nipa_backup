// ESMEditorEX.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMEditorEX.h"


// CESMEditorEX

IMPLEMENT_DYNAMIC(CESMEditorEX, CEdit)

CESMEditorEX::CESMEditorEX()
{
	m_brush.CreateSolidBrush(RGB(44,44,44));
	m_pFont = NULL;

	
}

CESMEditorEX::~CESMEditorEX()
{
	if( m_pFont)
	{
		delete m_pFont;
		m_pFont = NULL;
	}
}


BEGIN_MESSAGE_MAP(CESMEditorEX, CEdit)
	ON_WM_NCPAINT()
	ON_WM_CTLCOLOR()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()



// CESMEditorEX 메시지 처리기입니다.




void CESMEditorEX::OnNcPaint()
{
	CDC* pDC = GetWindowDC();

	CRect rect;
	GetWindowRect( &rect );
	rect.OffsetRect( -rect.left, -rect.top);

	CBrush brush( RGB(0,0,0));
	//rect.bottom -= 5;
	pDC->FrameRect( &rect, &brush);

	SetWindowPos(NULL, 0,0,rect.Width(), 24, SWP_NOMOVE);

	SetTextColor(pDC->m_hDC,RGB(255, 255, 255));// 글자색 변경
	SetBkColor(pDC->m_hDC,RGB(0, 0 ,0));

	ReleaseDC(pDC);

	Invalidate();
}


HBRUSH CESMEditorEX::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CEdit::OnCtlColor(pDC, pWnd, nCtlColor);

	switch(nCtlColor)
	{
	case CTLCOLOR_EDIT:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			pDC->SetBkColor(RGB(0, 0 ,0));
			hbr = (HBRUSH)(m_brush.GetSafeHandle());  
		}
		break;
	} 
	return hbr;
}


BOOL CESMEditorEX::OnEraseBkgnd(CDC* pDC)
{
	//pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
	//pDC->SetBkColor(RGB(0, 0 ,0));

	return CEdit::OnEraseBkgnd(pDC);
}

void CESMEditorEX::ChangeFont(int nSize, CString strFont, int nFontType)
{
	if(!m_pFont)
	{
		m_pFont = new CFont();
		m_pFont->CreateFont( nSize, 0, 0, 0, nFontType, false, false, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			FIXED_PITCH|FF_MODERN, strFont);
	}
	SetFont(m_pFont);	
}