#pragma once
#include <vector>
#include "ESMUtilRemoteFrame.h"
#include "ESMIndex.h"
#include "ESMTemplateMgr.h"

#define MAX_VIEW 2
#define MARGIN 4
#define TEST
#define REVISION

// CESMUtilRemoteCtrl 대화 상자입니다.

class CESMUtilRemoteCtrl : public CDialog
{
	DECLARE_DYNAMIC(CESMUtilRemoteCtrl)

public:
	CESMUtilRemoteCtrl(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMUtilRemoteCtrl();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UTIL_REMOTECTRL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedRemoteBtnFrameset();

public:
	int m_nFrameCnt;//Connected Frame Count
	int m_nFrameWidth;
	int m_nFrameHeight;
	int m_nControlWidth;
	int m_nControlHeight;
	int m_nCurFullIdx;
	CSize m_szResolution;
	CPoint m_ptArrFrameStart[4];
	BOOL m_bRecordState;
	BOOL m_bThreadState;

	CESMUtilRemoteFrame* m_pRemoteFrameDlg;
public:
	void SetPlayIndex(int nID,int  nIndex);
	void SetPlayFinish(int nID,BOOL bFinish);
	void HideAllControl(BOOL bShow);
	BOOL GetRecordInit(){return m_bRecordState;}
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	
	
	/*Create Event*/
	BOOL m_bCreate;
	vector<CString>m_strArrDSCID;
	void AddDSCFromNetworkEX(CString strDSCID){m_strArrDSCID.push_back(strDSCID);}	
	void CreateRemoteFrameDlg();

	/*UI*/
	BOOL m_bTimeThreadRun;
	void SetControlRepotision();
	//Frame 위치 결정
	void SetControlPosition(int nIdx,CPoint ptPosition){m_ptArrFrameStart[nIdx] = ptPosition;}
	CPoint GetControlPotision(int nIdx)	{return m_ptArrFrameStart[nIdx];}
	BOOL SetControlFrameSize(CRect rc,int nDSCNum);


	/*Record Event*/
	int m_nRunningTime;
	int m_nFinishTimeMovieIdx;
	void SetRecordInit(BOOL bState);
	void SetCurRunningTime(int nTime){m_nRunningTime = nTime;}
	int	 GetCurRunningTime(){return m_nRunningTime;}
	void SetCurFinishTimeIdx(int nTime);//{m_nFinishTimeMovieIdx = nTime;}
	int	 GetCurFinishTimeIdx(){return m_nFinishTimeMovieIdx;}
	BOOL m_bRecordProcess;
	BOOL GetRecordProcess(){return m_bRecordProcess;}
	void SetRecordProcess(BOOL bSet){m_bRecordProcess = bSet;}
	static unsigned WINAPI _DeleteFrameThread(LPVOID param);
	static unsigned WINAPI _GetCurPlayTimeThread(LPVOID param);
	

	/*Add Path*/
	map<CString,int>m_mapDSCID;
	void AddDSCPath(CString strPath);

	/*Check State*/
	void SetPlayState(int m_nID,BOOL bState);//Pause & Play

	/*Event Manager*/
	void SetWindowState(int nID,BOOL bShow);//Full Image
	void SetFocusState(int nID,BOOL bFocus);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedOk();
	void SendEventMsg(int nID,WPARAM wParam);

	/*Frame Sync Controller*/
	BOOL m_bArrPlayState[MAX_VIEW];
	int m_nArrSyncMovieIdx[MAX_VIEW];
	int m_nArrSyncFrameIdx[MAX_VIEW];
	int m_nSyncMovieIdx;
	int m_nSyncFrameIdx;
	int GetSyncFrameIdx(int nIdx);//{return m_nSyncCount;}
	void SetSyncFrameIdx(int nID,int nMovieIdx,int nIdx);//{m_nSyncCount = nIdx;}
	afx_msg void OnSize(UINT nType, int cx, int cy);
	void SetSyncInFrameView(int m_nID,int nMovIdx,BOOL bMode = FALSE);

	/*Time Index Calculator*/
	int m_nTimeIndex;
	void SetTimeIndex();
	int  GetTimeIndex(){return m_nTimeIndex;}
	
	/*FrameSelector*/
	void SetSelectTime(int nTime);


//wgkim 171124
	CESMTemplateMgr* m_pTemplateMgr;
	void SetTemplateMgr(CESMTemplateMgr* pTemplateMgr);
		
	CPoint Point2fToCPoint(Point2f point);
	Point2f CPointToPoint2f(CPoint point);

	//ZoomValue
	int m_nZoomGuide;
	void SetZoomGuide(int nZoom){m_nZoomGuide = nZoom;}
	int  GetZoomGuide(){return m_nZoomGuide;}

	//Keyboard Event
	void AllFrameStop(int nIdx,int nCalcPoint);

	//Focus Change
	void SetFocusFromMainFrm();
	void ChangeFocus(int nID,BOOL bFlag);
	int m_nFocusID;

	//Show Red Spot Point
	void ShowRedSpotPoint(WPARAM wParam,int nID,int nCount,int nTime);
	
	//Init Point Axis
	void ClearTemplatePoint();

	//Show Screen
	void ShowScreenFromUser(int nID);
};
struct ThreadDeleteFrame{
	ThreadDeleteFrame()
	{
		pRemoteFrame = NULL;
		nIndex		= -1;
		bFinish		= FALSE;
	}
	CESMUtilRemoteFrame* pRemoteFrame;
	int nIndex;
	BOOL bFinish;
};
