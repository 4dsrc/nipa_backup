#pragma once

#include "ESMUtilCalculator.h"

// CESMUtilActualMeasurementDlg 대화 상자입니다.

class CESMUtilActualMeasurementDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CESMUtilActualMeasurementDlg)

public:
	CESMUtilActualMeasurementDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMUtilActualMeasurementDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UTIL_ACTUAL_MEASUREMENT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnChangeEditInputFocalLength();
	afx_msg void OnChangeEditInputActualLength();

public:
	CESMUtilCalculator *m_pUtilCalculator;

public:
	CString m_strSensorHeight;
	CString m_strSensorWidth;
	CString m_strFocalLength;
	CString m_strActualWidth169;
	CString m_strActualLength;
	CString m_strActualHeight169;
	CString m_strActualHeight;
	CString m_strActualWidth;
	CString m_strAngleOfView;
	CString m_strSensorDiagonalLength;

	double m_dSensorHeight;
	double m_dSensorWidth;
	double m_dFocalLength;
	double m_dActualWidth169;
	double m_dActualLength;
	double m_dActualHeight169;
	double m_dActualHeight;
	double m_dActualWidth;
	double m_dAngleOfView;
	double m_dSensorDiagonalLength;
	double m_dActualDiagonalLength;

};
