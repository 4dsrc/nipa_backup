#include "stdafx.h"
#include "ESMUtilCalculator.h"


CESMUtilCalculator::CESMUtilCalculator(void)
{
}


CESMUtilCalculator::~CESMUtilCalculator(void)
{
}

double CESMUtilCalculator::ArcLength(double radius, double degree)
{
	double arcLength = (2*3.141592*radius) * (degree/360);
	return arcLength;
}

double CESMUtilCalculator::DiagonalLength(double width, double height)
{
	return sqrt(width*width + height*height);
}

double CESMUtilCalculator::RadianToDegree(double radian)
{
	return radian * (180/3.141592);
}

double CESMUtilCalculator::ActualDiagonalLength(double actualLength, double angleOfView)
{
	return actualLength*tan(angleOfView)*2;	
}

double CESMUtilCalculator::ActualWidth(double sensorWidth, double sensorHeight, double actualDiagonalLength)
{
	double widthRatio = 0;
	widthRatio = sensorHeight/sensorWidth;	

	return sqrt((actualDiagonalLength*actualDiagonalLength)/(1+(widthRatio*widthRatio)));
}

double CESMUtilCalculator::ActualHeight(double sensorWidth, double sensorHeight, double actualDiagonalLength)
{
	double heightRatio = 0;	
	heightRatio = sensorWidth/sensorHeight;

	return sqrt((actualDiagonalLength*actualDiagonalLength)/(1+(heightRatio*heightRatio)));
}

double CESMUtilCalculator::ActualHeight169(double actualHeight)
{
	return actualHeight*(27./32);
}
