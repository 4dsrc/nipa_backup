

#include "stdafx.h"
#include "ESMTemplatePoint.h"


CESMTemplatePoint::CESMTemplatePoint()
{ 	
	
}

CESMTemplatePoint::~CESMTemplatePoint(void)
{
}

void CESMTemplatePoint::initCreateSubWindowAndTargetRect(Mat& image, Point2f targetPoint, int subWindowWidth, int subWindowHeight, int targetWidth, int targetHeight) 
{
	_targetRegion = Rect2d(
		targetPoint.x - (targetWidth/2), 
		targetPoint.y - (targetHeight*(5./6)), 
		targetWidth, targetHeight);	
	_subWindowRegion = Rect2d(
		targetPoint.x - (subWindowWidth/2), 
		targetPoint.y - (subWindowHeight/2), 
		subWindowWidth, 
		subWindowHeight);

	_paddingRegion = _subWindowRegion;
	_paddingRegion.x = 0;
	_paddingRegion.y = 0;

	ModifyRegionToBeValid(image);
}

void CESMTemplatePoint::ModifyRegionToBeValid(Mat& image)
{
	int width = _subWindowRegion.width;
	int height = _subWindowRegion.height;
	if(_subWindowRegion.x + _subWindowRegion.width > image.cols)
	{
		_subWindowRegion.width -= _subWindowRegion.x + _subWindowRegion.width - image.cols;
		_paddingRegion.width = _subWindowRegion.width;					
	}
	if(_subWindowRegion.y + _subWindowRegion.height > image.rows)
	{
		_subWindowRegion.height -= _subWindowRegion.y + _subWindowRegion.height- image.rows;
		_paddingRegion.height = _subWindowRegion.height;					
	}
	if(_subWindowRegion.x < 0)
	{
		_paddingRegion.width += _subWindowRegion.x;
		_subWindowRegion.width = _paddingRegion.width;
		_paddingRegion.x = abs(width - _paddingRegion.width);
		_subWindowRegion.x = 0;								
	}
	if(_subWindowRegion.y < 0)
	{
		_paddingRegion.height += _subWindowRegion.y;
		_subWindowRegion.height = _paddingRegion.height;
		_paddingRegion.y = abs(height - _paddingRegion.height);
		_subWindowRegion.y = 0;					
	}
}

void CESMTemplatePoint::calcPoint(Mat& homography, vector<Point2f>& currentPoint)
{
	for(int i = 0; i < currentPoint.size(); i++)
	{
		double tempMat_[3] = {(double)currentPoint[i].x, (double)currentPoint[i].y, 1.};
		Mat tempMat(3, 1, CV_64F, tempMat_);
		Mat resultMat = homography * tempMat;

		currentPoint[i] = Point2f(resultMat.at<double>(0)/resultMat.at<double>(2), 
								  resultMat.at<double>(1)/resultMat.at<double>(2));	
	}	
}

Mat CESMTemplatePoint::getHomography(int index)
{
	int nullCheck = 1;
	while(1)
	{
		if((_squarePoints[index-nullCheck].at(0).x == 0 && _squarePoints[index-nullCheck].at(0).y == 0)
			&& (_squarePoints[index-nullCheck].at(1).x == 0 && _squarePoints[index-nullCheck].at(1).y == 0)
			&& (_squarePoints[index-nullCheck].at(2).x == 0 && _squarePoints[index-nullCheck].at(2).y == 0)
			&& (_squarePoints[index-nullCheck].at(3).x == 0 && _squarePoints[index-nullCheck].at(3).y == 0))
		{
			nullCheck++;		
		}
		else
			return findHomography(_squarePoints[index-nullCheck], _squarePoints[index]);//RANSAC, 0.001);
	}	
}

void CESMTemplatePoint::setSquares(vector<Point2f> inputPoints)
{
	if(inputPoints.size() != 4)
		return;
	_squarePoints.push_back(inputPoints);
}

void CESMTemplatePoint::squareImport(string path)
{
	ifstream fileIn(path);	
	string test[10000];
	char ch[10];
	int cameraCount = 0;	

	if(fileIn.is_open())
	{		
		int count = 0;
		while(1)
		{
			fileIn >> test[count]; 							
			count++;

			if(fileIn.eof())
				break;
		}
	}
}
void CESMTemplatePoint::squareExport(string path, string cameraName, vector<Point2f> points, int cameraCount)
{
	ofstream fileOut(path, ios::app);	
	
	char ch[10];
	static bool init = true;

	if(fileOut.is_open())
	{
		if(init == true)
		{
			init = false;
			fileOut << "cameraCount" << '\t' << cameraCount << endl;
		}		
		
		fileOut << "camera" << '\t'<< cameraName << endl;

		for(int i = 0; i < 4; i++)
		{
			string x = "Pos" + string(itoa(i, ch, 10 ));
			string y = "Pos" + string(itoa(i, ch, 10 ));
			fileOut << x << '\t' << points[i].x << endl;
			fileOut << y << '\t' << points[i].y << endl;
		}
		fileOut << endl;		
	}
	fileOut.close();
}

void CESMTemplatePoint::vectorExport(string path)
{
	ofstream fileOut(path);	

	if(fileOut.is_open())
	{
		for(int i = 0; i < _squarePoints.size(); i++)
		{
			for(int j = 0; j < _squarePoints[i].size(); j++)
			{
				fileOut << _squarePoints[i][j].x << '\t';
				fileOut << _squarePoints[i][j].y << '\t';
			}
			fileOut << endl;
		}
	}
}

void CESMTemplatePoint::vectorImport(string path, int frameCount)
{
	ifstream fileIn(path);
	char ch[10];

	if(fileIn.is_open())
	{		
		for(int i = 0; i < frameCount; i++)
		{
			_squarePoints.push_back(vector<Point2f>());
			for(int j = 0; j < 4; j++)
			{
				_squarePoints[i].push_back(Point2f());
				
				fileIn >> ch; _squarePoints[i][j].x = strtod(ch, NULL);
				fileIn >> ch; _squarePoints[i][j].y = strtod(ch, NULL);
			}
		}		
	}
}


void CESMTemplatePoint::DrawSquare(Mat& frame, vector<Point2f> templatePoint)
{

	if(templatePoint.size() == 4)
	{
		line(frame, templatePoint[1], templatePoint[2], Scalar(255, 0, 255), 2);	
		line(frame, templatePoint[0], templatePoint[1], Scalar(255, 0, 255), 2);	
		line(frame, templatePoint[2], templatePoint[3], Scalar(255, 0, 255), 2);
		line(frame, templatePoint[0], templatePoint[3], Scalar(255, 0, 255), 2);		
	}	
}

Scalar CESMTemplatePoint::Coloring(int index)
{
	int red = 0; int green = 0; int blue = 0;

	if(index % 6 == 0)		red = 255;
	if(index % 6 - 1 == 0)	green = 255;
	if(index % 6 - 2 == 0)	blue = 255;
	if(index % 6  - 4== 0)	{red = 255; green = 255;}
	if(index % 6 - 5 == 0)	{green = 255; blue = 255;}
	if(index % 6 - 6 == 0)	{blue = 255; red = 255;};

	return Scalar(red, green, blue);
}
