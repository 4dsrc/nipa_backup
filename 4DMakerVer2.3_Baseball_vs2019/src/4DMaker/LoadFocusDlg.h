#pragma once
#include "afxwin.h"


// CLoadFocusDlg 대화 상자입니다.

class CLoadFocusDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CLoadFocusDlg)

private:
	CWnd* m_pParent;
	CString m_strLoadFilePath;
	BOOL m_bIsVisible;
	BOOL m_bIsFocusMove;

	CButton m_btnMove;
	CButton m_btnCompare;
	CButton m_btnClose;
	CStatic m_ctrlStatus;

public:
	CLoadFocusDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLoadFocusDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_FOCUS_LOAD };

	void SetLoadFilePath(CString strPath);
	void SetEndFocusMove();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	BOOL IsVisible();
	afx_msg void OnBnClickedBtnCompare();
	afx_msg void OnBnClickedBtnMove();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
};
