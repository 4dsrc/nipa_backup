#pragma once

#include <vector>
#include "cv.h"
#include "highgui.h"

#include "ESMFunc.h"
#include "ESMUtil.h"
#include "ESMIni.h"
#include "FFmpegManager.h"
#include "ESMObjectFrame.h"
#include "ESMObjectTracking.h"

#include "ESMTemplateImage.h"
#include "ESMRecalibrationImage.h"

class CESMRecalibrationMgr
{
public:
	CESMRecalibrationMgr(void);
	~CESMRecalibrationMgr(void);


private:
	vector<DscAdjustInfo*> m_ArrDscInfo;
public:
	void			AddDscInfo(CString strDscId, CString strDscIp,BOOL bReverse = FALSE);
	void			GetDscInfo(vector<DscAdjustInfo*>** pArrDscInfo);
	int				GetDscCount();
	void			DscClear();	
	DscAdjustInfo*	GetDscAt(int nIndex);		
	int				GetDscIndexFrom(CString strDscId);
	
public:
	BOOL	SearchDetectPoint(IplImage* pNewGray, CSize MinSize, CSize MaxSize, vector<CRect>* vecDetectAreas, COLORREF clrDetectColor);
	BOOL	SearchPoint(IplImage*	pNewGray, vector<CPoint>* arrBeginPoint, vector<CPoint>* arrPointRange, vector<CPoint>* pArrPoint);
	BOOL	CalcAdjustData(DscAdjustInfo* pAdjustInfo, int nTargetLenth, int nZoom);
	BOOL	GetSearchDetectPoint(DscAdjustInfo* pAdjustInfo, int nThreshold, CSize minDetectSize, CSize maxDetectSize, int nPointGapMin, COLORREF clrDetectColor);	
	void	GetResolution(int &nWidth, int &nHeight);
	double	GetDistanceData(CString strDSCId);
	int		GetZoomData(CString strDSCId);

private:
	vector<stAdjustInfo> m_arrAdjInfo;	
	vector<stAdjustInfo> m_arrModifiedAdjInfo;
public:
	void			AddAdjInfo(stAdjustInfo adjustInfo);
	stAdjustInfo	GetAdjInfoAt(int index);		
	
	Mat				ApplyAdjustImageUsingCPU(int nSelectedItem, int marginX, int marginY);

private:
	int		Round(double dData);
	void	GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle);
	void	GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY);
	void	GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY);
	void	CpuMakeMargin(Mat* gMat, int nX, int nY);
	void	CpuMoveImage(Mat* gMat, int nX, int nY);
	void	CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle);	
	void	affinetransform(Mat img,cv::Point a,cv::Point b, cv::Point c, Point2f srcTri[3],Mat dst);
	
public:
	//CESMObjectTracking *m_pObjectTracking;

public:
	void	tracking();
	void	moveImage(Mat* gMat, int nX, int nY);
};
