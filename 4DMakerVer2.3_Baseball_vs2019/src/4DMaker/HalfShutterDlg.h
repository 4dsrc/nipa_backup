#pragma once


// CHalfShutterDlg 대화 상자입니다.

class CHalfShutterDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CHalfShutterDlg)

public:
	CHalfShutterDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHalfShutterDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_HALF_SHUTTER };

	int GetReleaseSec();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();

private:
	int m_nReleaseSec;
};
