// ESMTrackingViewer.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMTrackingViewer.h"
#include "afxdialogex.h"


// CESMTrackingViewer 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMTrackingViewer, CDialogEx)

CESMTrackingViewer::CESMTrackingViewer(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMTrackingViewer::IDD, pParent)
{
	m_hMuxMovie = NULL;

	m_nZoomInValue = 200;
	m_dwIP = 3232235520;

	m_bUsingTrackingMode = FALSE;
	m_strTrackingMode = _T("");
}

CESMTrackingViewer::~CESMTrackingViewer()
{
	::TerminateThread( m_hMuxMovie, 0 );
	CloseHandle(m_hMuxMovie);
}

void CESMTrackingViewer::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);	
	DDX_Text(pDX, IDC_EDIT_TRACKING_ZOOMIN, m_nZoomInValue);
	DDV_MinMaxInt(pDX, m_nZoomInValue, 100, 1000);
	DDX_IPAddress(pDX, IDC_IPADDRESS_TRACKING_EDIT, m_dwIP);
	DDX_Text(pDX, IDC_STATIC_TRACKINGMODE_STATE, m_strTrackingMode);
}


BEGIN_MESSAGE_MAP(CESMTrackingViewer, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_TRACKINGMODE_APPLY, &CESMTrackingViewer::OnBnClickedButtonTrackingmodeApply)
END_MESSAGE_MAP()


// CESMTrackingViewer 메시지 처리기입니다.

BOOL CESMTrackingViewer::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	return TRUE;
}

unsigned WINAPI CESMTrackingViewer::ThreadDetectFolder(LPVOID param)
{
	CESMTrackingViewer* pData = (CESMTrackingViewer*)param;

	HANDLE hDir = CreateFileW(pData->m_strDetectedPath, 
		GENERIC_READ,
		FILE_SHARE_READ|FILE_SHARE_WRITE,
		0, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, 0);

	CONST DWORD cbBuffer = 1024;
	BYTE* pBuffer = (PBYTE)malloc(cbBuffer);
	DWORD byteReturned;
	WCHAR temp[MAX_PATH] = { 0 };

	while(1)
	{
		FILE_NOTIFY_INFORMATION* pfni;
		BOOL fOk = ReadDirectoryChangesW(
			hDir, 
			pBuffer, 
			cbBuffer, 
			FALSE, 
			FILE_NOTIFY_CHANGE_FILE_NAME, 
			&byteReturned, 0, 0);

		if(!fOk)
		{
			break;
		}
		pfni = (FILE_NOTIFY_INFORMATION*)pBuffer;

		do {
			switch(pfni->Action)
			{
			case FILE_ACTION_ADDED:
				{
					StringCbCopyNW(temp, sizeof(temp), pfni->FileName, pfni->FileNameLength);
					CString strTemp(temp);

					CString strFilePath = pData->GetStrFilePath(strTemp);
					
					if(!pData->StrFilter(strFilePath))
						break;

					//ObjectTracking
					pData->GetDlgItem(IDC_STATIC_TRACKINGMODE_STATE)->SetWindowTextW(strTemp);

					CString strCmd, strOpt = _T("");

					strCmd.Format(_T("%s\\bin\\ObjectTracking.exe"),ESMGetPath(ESM_PATH_HOME));	
					strOpt.Format(_T(" %s %d"), strFilePath, pData->m_nZoomInValue);
					
					SHELLEXECUTEINFO lpExecInfo;
					lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
					lpExecInfo.lpFile = strCmd;
					lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
					lpExecInfo.hwnd = NULL;  
					lpExecInfo.lpVerb = L"open";
					lpExecInfo.lpParameters = strOpt;
					lpExecInfo.lpDirectory = NULL;

					lpExecInfo.nShow = SW_SHOW; // hide shell during execution
					lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
					ShellExecuteEx(&lpExecInfo);

					// wait until the process is finished
					if (lpExecInfo.hProcess != NULL)
					{
						::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
						::CloseHandle(lpExecInfo.hProcess);
					}

					pfni = (FILE_NOTIFY_INFORMATION*)((PBYTE)pfni + pfni->NextEntryOffset);
				}
				break;
			default:
				break;
			}				
		} while(pfni->NextEntryOffset > 0);
	}
	return 0;
}

CString CESMTrackingViewer::GetStrFilePath(CString strFileName)
{
	CString strFilePath;

	CString strDate = GetStrDate();	
	
	strFilePath.Format(_T("\\\\%s\\RecordSave\\Output\\2D\\%s\\%s"), 
		GetStrIPFromDwIP(m_dwIP), strDate, strFileName);

	return strFilePath;
}

CString CESMTrackingViewer::GetStrDate()
{
	SYSTEMTIME st;
	GetLocalTime(&st);

	CString strDate = _T("");
	strDate.Format(_T("%04d%02d%02d"),st.wYear,st.wMonth,st.wDay);

	return strDate;
}

CString CESMTrackingViewer::GetStrIPFromDwIP(DWORD dwIP)
{	
	CString strIP;	
	strIP.Format(_T("%d.%d.%d.%d"),
		FIRST_IPADDRESS(dwIP),
		SECOND_IPADDRESS(dwIP),
		THIRD_IPADDRESS(dwIP),
		FOURTH_IPADDRESS(dwIP));

	return strIP;
}

void CESMTrackingViewer::OnBnClickedButtonTrackingmodeApply()
{
	ToggleTrackingMode();
	UpdateData(TRUE);
	
	if(GetTrackingModeState())
	{		
		if(m_dwIP == 0)
		{
			m_strTrackingMode = _T("IP를 입력하세요");
			UpdateData(FALSE);
			return;
		}
		m_strTrackingMode = _T("대기 중...");

		CString strDate = GetStrDate();
		CString strIP = GetStrIPFromDwIP(m_dwIP);	

		m_strDetectedPath.Format(_T("\\\\%s\\RecordSave\\Output\\2D\\%s\\"), 
			strIP, strDate);
		m_hMuxMovie = NULL;
		m_hMuxMovie = (HANDLE) _beginthreadex(NULL, 0, ThreadDetectFolder, (void *)this, 0, NULL);	
	}
	else
	{
		if(m_hMuxMovie != NULL)
		{
			::TerminateThread( m_hMuxMovie, 0 );
			CloseHandle(m_hMuxMovie);
		}
		m_strTrackingMode = _T("일시 정지");
	}

	UpdateData(FALSE);
}

BOOL CESMTrackingViewer::StrFilter(CString strFilePath)
{
	CString strFilter_1 = _T("_stab");
	CString strFilter_2 = _T("Info");
	CString strFilter_3 = _T("tracking");

	BOOL test1 = strFilePath.Find(strFilter_1);
	BOOL test2 = strFilePath.Find(strFilter_2);
	BOOL test3 = strFilePath.Find(strFilter_3);

	if( strFilePath.Find(strFilter_1) == -1 &&
		strFilePath.Find(strFilter_2) == -1 &&
		strFilePath.Find(strFilter_3) == -1)
		return TRUE;

	return FALSE;
}

void CESMTrackingViewer::ToggleTrackingMode()
{
	if(m_bUsingTrackingMode)
	{
		GetDlgItem(IDC_EDIT_TRACKING_ZOOMIN)->EnableWindow(m_bUsingTrackingMode);
		GetDlgItem(IDC_IPADDRESS_TRACKING_EDIT)->EnableWindow(m_bUsingTrackingMode);
		GetDlgItem(IDC_BUTTON_TRACKINGMODE_APPLY)->SetWindowTextW(_T("Apply"));

		m_bUsingTrackingMode = FALSE;		
	}
	else
	{
		GetDlgItem(IDC_EDIT_TRACKING_ZOOMIN)->EnableWindow(m_bUsingTrackingMode);
		GetDlgItem(IDC_IPADDRESS_TRACKING_EDIT)->EnableWindow(m_bUsingTrackingMode);
		GetDlgItem(IDC_BUTTON_TRACKINGMODE_APPLY)->SetWindowTextW(_T("Pause"));

		m_bUsingTrackingMode = TRUE;		
	}
}

BOOL CESMTrackingViewer::GetTrackingModeState()
{
	return m_bUsingTrackingMode;
}