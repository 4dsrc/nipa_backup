#define	ESM_4DMAKER_VERSION_10001 -10.0000		//2014-01-07  KCD 버전 관리 시작
#define	ESM_4DMAKER_VERSION_10002 -9.0000
#define	ESM_4DMAKER_VERSION_10003 -8.9000		//2014-01-21 kcd data 저장상태 정보 추가.
#define	ESM_4DMAKER_VERSION_10004 -8.7000		//2014-01-21 kcd data Picture 모드 추가.
#define	ESM_4DMAKER_VERSION_10005 -8.6000		//2014-01-21 kcd Item Picture Size 정보 추가.
#define	ESM_4DMAKER_VERSION_10006 -8.5000		//2014-01-21 Margin 정보 추가.
#define	ESM_4DMAKER_VERSION_10007 -8.4000		//2014-01-21 Margin 정보 추가.
#define	ESM_4DMAKER_VERSION_10008 -8.3000		//2014-01-21 Margin 정보 추가.
#define	ESM_4DMAKER_VERSION_10009 -8.2000		//2014-01-21 Margin 정보 추가.
#define	ESM_4DMAKER_VERSION_10010 -8.1000		//2014-01-21 Margin 정보 추가.
#define	ESM_4DMAKER_VERSION_10011 -8.0000		//2015-11-16 Running Time, Movie Size 추가
#define	ESM_4DMAKER_VERSION_10012 -7.9000		//2017       GH5 FRAME 정보 추가
#define	ESM_4DMAKER_VERSION_10013 -7.8000		//2017-12-23 PictureWizard 추가
#define	ESM_4DMAKER_VERSION_10014 -7.7000		//2018-03-08 카메라 정보 추가
#define	ESM_4DMAKER_VERSION_10015 -7.6000		//2018-03-08 카메라 정보 추가
#define	ESM_4DMAKER_VERSION_10016 -7.5000		//2018-04-06 Capture Image Record Exception 추가
#define	ESM_4DMAKER_VERSION_10017 -7.4000		//2019-04-17 DSC ListView ->F값 추가, 4dm에 DSC value 포함 안되도록 수정
#define	ESM_4DMAKER_VERSION_10018 -7.3000		//2019-05-20 Rect 기반 Margin 추가

// Version 정보 추가.


#define	ESM_4DMAKER_VERSION_CURRENTVER  ESM_4DMAKER_VERSION_10018	//현재 버전 