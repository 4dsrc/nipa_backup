#pragma once
#include <opencv2/highgui.hpp>

#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/videostab.hpp>

#include <opencv2/cudawarping.hpp>

#include "ESMIndex.h"
#include "ESMFunc.h"

using namespace std;
using namespace cv;

class CESMStabilizationMgr
{
public:
	CESMStabilizationMgr(bool isUHDtoFHD);
	~CESMStabilizationMgr(void);

public:
	Ptr<videostab::IFrameSource>					_stabilizedFrames; 
	Ptr<videostab::ImageSetForStabilization>		_imageSetForStabilization;		
	Ptr<videostab::MotionEstimatorRansacL2>			_motionEstimation; 		
	Ptr<videostab::KeypointBasedMotionEstimatorGpu> _motionEstBuilder;
	Ptr<videostab::IOutlierRejector>				_outlierRejector;	

	videostab::RansacParams _ransac; 
	videostab::TwoPassStabilizer *_twoPassStabilizer;

	double _minInlierRatio;

	int _width;
	int _height;

public:
	//void VideoDecoding(string strMovieFile, vector<TEMPLATE_STRUCT> templateStruct);
	bool VideoDecoding(string strMovieFile, int nStabilizationStartFrameNumber, int nStabilizationEndFrameNumber);
	void CreateStabilizatedMovie(string outputPath, double outputFps);
	void IncodingWithStabilizedImage(Ptr<videostab::IFrameSource> stabilizedFrames, string outputPath, double outputFps);
	
public:
	vector<Mat> frontFrame_;
	vector<Mat> rearFrame_;
	vector<Mat> stabilizedFrame_; 	

	int _stabilizerStartFrameNumber;
	int _stabilizerEndFrameNumber;
	double _dFrameRate;

public:	
	void TranslationAndWarpPerspective(Mat& srcFrame, Mat& dstFrame, Mat& motion, cv::Size size);
	void ResizeFrameToFHD(Mat& srcFrame, Mat& dstFrame);
//	void CalcFrontAndRearFrameCount(vector<TEMPLATE_STRUCT> templateStruct);	
};

