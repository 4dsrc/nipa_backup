#pragma once


#include "cv.h"
// ESMAdjustImage

class CESMRemoteImage : public CStatic
{
	DECLARE_DYNAMIC(CESMRemoteImage)

public:
	CESMRemoteImage();
	virtual ~CESMRemoteImage();

	//CDC* m_CDC;
	//CDC* m_memFullDC;
	double m_dMultiple;
	BOOL m_bIsFull;
	//wgkim 17-06-20
	double m_dPrevMultiple;

	int m_nImageWidth, m_nImageHeight;
	int m_nFullWidth,m_nFullHeight;

	CPoint m_ptImageMoveStart, m_ptImageMoving, m_ptImageLeftTop;
	CRect m_rcTemp;
	
	CDC* m_memFullDC;
	CDC* m_memDivDC;
	
	CRect m_rcOri;
	CRect m_rcFull;
	void SetSize(int nOriWidth,int nOriHeight,int nFullWidth,int nFullHeight);
	void SetDC(int nWidth,int nHeight,BOOL bFull);
	BOOL SetImageBuffer(BYTE* pImage, int nWidth, int nHeight, int nChannel);
	void Redraw();
	void DrawRect(BOOL bShow);
	BYTE* m_pTpImage;
	IplImage* m_pImage;
	IplImage* BYTE2IplImage(BYTE* pByte, int nWidth, int nHeight);

	BOOL m_bRect;
	void SetRedRect(BOOL b){m_bRect = b;}
	BOOL GetRedRect(){return m_bRect;}

	CRITICAL_SECTION m_crImageLoad;

	int m_nZoomRatio;
	int GetZoomRatio(){return m_nZoomRatio;}
	void SetZoomRatio(int n){m_nZoomRatio = n;}

	int m_nMovieIdx;
	int m_nFrameIdx;
	int m_nCamID;
	void SetTimeIndex(int nMovieIdx,int nFrameIdx){
		m_nMovieIdx = nMovieIdx;
		m_nFrameIdx = nFrameIdx;
	}
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
//	afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


