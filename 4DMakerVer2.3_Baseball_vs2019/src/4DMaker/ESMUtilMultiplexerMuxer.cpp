#include "stdafx.h"
#include "ESMUtilMultiplexerMuxer.h"

#include "ESMFunc.h"
#include "ESMFileOperation.h"

CESMUtilMultiplexerMuxer::CESMUtilMultiplexerMuxer(
	int nContainerValue, 
	CString strMuxPath, 
	CString strMp4FilePath)
{
	m_nContainerValue = nContainerValue;

	CString strHFilePath= strMuxPath + _T("\\") + ESMGetFrameRecord();
	CT2CA pszConvertedAnsiString(strHFilePath);
	string strPath(pszConvertedAnsiString);
	m_strHFilePath = strPath + "\\";
	
	ESMCreateAllDirectories(strMp4FilePath);

	CT2CA pszConvertedAnsiOutputString(strMp4FilePath);
	string strMp4OutputPath(pszConvertedAnsiOutputString);
	m_str4DLiveSavePath = strMp4OutputPath;
}

CESMUtilMultiplexerMuxer::~CESMUtilMultiplexerMuxer(void)
{
	m_strNumber.clear();
	m_strCameraNumber.clear();
	m_strVideoValid.clear();
}

BOOL CESMUtilMultiplexerMuxer::CheckIfFTPDataReceived()
{
	ifstream finishFile(m_strHFilePath + "finish.txt");

	if(finishFile.is_open())
		return TRUE;
	else
		return FALSE;
}

BOOL CESMUtilMultiplexerMuxer::OpenFTPList()
{
	ifstream dataFile(m_strHFilePath + "FTPList.txt");

	char number[20];
	char cameraNumber[20];
	char videoValid[20];
	char etc[20];

	int cnt = 0;

	if(dataFile.is_open())
	{
		while(1)
		{
			int lineNumber = cnt * 80;

			dataFile.get(number, 10, ' ');
			dataFile.seekg(20L + lineNumber, ios::beg);

			dataFile.get(cameraNumber, 10, ' ');
			dataFile.seekg(40L + lineNumber, ios::beg);

			dataFile.get(videoValid, 10, ' ');
			dataFile.seekg(80L + lineNumber, ios::beg);

			if(string(number).empty())
				break;

			m_strNumber.push_back(string(number));
			m_strCameraNumber.push_back(string(cameraNumber));
			m_strVideoValid.push_back(string(videoValid));

			cnt++;
		}
		return TRUE;
	}
	else
		return FALSE;
}

void CESMUtilMultiplexerMuxer::MuxingAllMJPEGFiles(
	CString strSubL, 
	CString strSubM,
	CString strSubS,
	CString strDate,
	CString strTime,
	CString strObject,
	CString strDescription,
	CString strOwner)
{
	string strOpt_;
	CString strCmd;
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";

	int cancelFileCount = 0;

	for(int i = 0; i < m_strNumber.size(); i++)
	{
		if( m_strVideoValid[i].compare("complete"))
		{
			cancelFileCount++;
			continue;
		}
		strOpt_ += "-i \"" + m_strHFilePath + m_strCameraNumber[i] + ".mp4\" ";
	}
	strOpt_ += "-c copy ";

	for(int i = 0; i < m_strNumber.size() - cancelFileCount; i++)
	{
		char tmp[10];
		strOpt_ += "-map " + string(itoa(i, tmp, 10)) + " ";
	}
	//subtitle
//	char tmp[10];
//	strOpt_ += "-map " + string(itoa(m_strNumber.size() - cancelFileCount, tmp, 10)) + " ";
//	strOpt_ += "-c:s mov_text -metadata:s:s:0 language=kor ";

	//Input Metadata 
	strOpt_ += "-metadata description=\"";

	strOpt_ += "SubL:";
	CT2CA pszConvertedAnsiStrSubL(strSubL);
	string strAnsiSubL(pszConvertedAnsiStrSubL);
	strOpt_ += strAnsiSubL + "\n";

	strOpt_ += "SubM:";
	CT2CA pszConvertedAnsiStrSubM(strSubM);
	string strAnsiSubM(pszConvertedAnsiStrSubM);
	strOpt_ += strAnsiSubM + "\n";

	strOpt_ += "SubS:";
	CT2CA pszConvertedAnsiStrSubS(strSubS);
	string strAnsiSubS(pszConvertedAnsiStrSubS);
	strOpt_ += strAnsiSubS + "\n";

	strOpt_ += "Object:";
	CT2CA pszConvertedAnsiStrObject(strObject);
	string strAnsiObject(pszConvertedAnsiStrObject);
	strOpt_ += strAnsiObject + "\n";

	strOpt_ += "Description:";
	CT2CA pszConvertedAnsiStrDescription(strDescription);
	string strAnsiDescription(pszConvertedAnsiStrDescription);
	strOpt_ += strAnsiDescription + "\n";

	strOpt_ += "Owner:";
	CT2CA pszConvertedAnsiStrOwner(strOwner);
	string strAnsiOwner(pszConvertedAnsiStrOwner);
	strOpt_ += strAnsiOwner + "\" ";
	//////////

	CString strOpt(strOpt_.c_str());
	CString strFilePath;
	

	if(m_nContainerValue == 0)
	{
		strFilePath.Format(_T("\"%S\\%s.mp4\""), 
			m_str4DLiveSavePath.c_str(), 
			ESMGetFrameRecord());

		int nCount = 0;
		CString strCheckFilePath = strFilePath;
		strCheckFilePath.Remove('\"');

		while(FileExists(strCheckFilePath))
		{
			strFilePath.Format(_T("\"%S\\%s_%d.mp4\""),
				m_str4DLiveSavePath.c_str(), 
				ESMGetFrameRecord(), 
				nCount);
			strCheckFilePath = strFilePath;
			strCheckFilePath.Remove('\"');
			nCount++;
		}
	}
	else if(m_nContainerValue == 1)
	{
		strFilePath.Format(_T("\"%S\\%s.ts\""), 
			m_str4DLiveSavePath.c_str(), 
			ESMGetFrameRecord());

		int nCount = 0;
		CString strCheckFilePath = strFilePath;
		strCheckFilePath.Remove('\"');

		while(FileExists(strCheckFilePath))
		{
			strFilePath.Format(_T("\"%S\\%s_%d.ts\""), 
				m_str4DLiveSavePath.c_str(), 
				ESMGetFrameRecord(), 
				nCount);
			strCheckFilePath = strFilePath;
			strCheckFilePath.Remove('\"');
			nCount++;
		}
	}

	strOpt += strFilePath;

	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_SHOW; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
}

void CESMUtilMultiplexerMuxer::WriteSubtitleTest(
	CString strSubL, 
	CString strSubM,
	CString strSubS,
	CString strDate,
	CString strTime,
	CString strObject,
	CString strDescription,
	CString strOwner)
{
	CT2CA pszConvertedAnsiString(ESMGetFrameRecord());
	string str4DLiveFileName(pszConvertedAnsiString);

	std::locale::global(std::locale("Korean"));

	ofstream fileOut;
	//fileOut.imbue(locale("ko_KR.UTF-8"));
	string str4DLiveSubtitleName = m_strHFilePath + str4DLiveFileName + ".srt";
	fileOut.open(str4DLiveSubtitleName, ios::app);

	char ch[10];
	static bool init = true;

	if(fileOut.is_open())
	{
		fileOut << "0\n";
		fileOut << "00:00:00,000 --> 00:00:00,000\n";

		CT2CA pszConvertedAnsiStrDate(strDate);
		string strAnsiDate(pszConvertedAnsiStrDate);
		fileOut << "Time:" << strAnsiDate << endl;

		CT2CA pszConvertedAnsiStrSubL(strSubL);
		string strAnsiSubL(pszConvertedAnsiStrSubL);
		fileOut << "SubL:" << strAnsiSubL << endl;

		CT2CA pszConvertedAnsiStrSubM(strSubM);
		string strAnsiSubM(pszConvertedAnsiStrSubM);
		fileOut << "SubM:" << strAnsiSubM << endl;

		CT2CA pszConvertedAnsiStrSubS(strSubS);
		string strAnsiSubS(pszConvertedAnsiStrSubS);
		fileOut << "SubS:" << strAnsiSubS << endl;

		CT2CA pszConvertedAnsiStrObject(strObject);
		string strAnsiObject(pszConvertedAnsiStrObject);
		fileOut << "Object:" << strAnsiObject << endl;

		CT2CA pszConvertedAnsiStrDescription(strDescription);
		string strAnsiDescription(pszConvertedAnsiStrDescription);
		fileOut << "Description:" << strAnsiDescription << endl;

		CT2CA pszConvertedAnsiStrOwner(strOwner);
		string strAnsiOwner(pszConvertedAnsiStrOwner);
		fileOut << "Owner:" << strAnsiOwner << endl;
	}
	fileOut.close();

	//CStdioFile fileOut;
	//CString strHFilePath(m_strHFilePath.c_str());
	//CString str4DLiveSubtitleName = strHFilePath + ESMGetFrameRecord() + _T(".srt");	

	//char ch[10];
	//static bool init = true;

	//if(fileOut.Open(str4DLiveSubtitleName, CFile::modeCreate | CFile::modeWrite))
	//{
	//	fileOut.WriteString(_T("0\n"));
	//	fileOut.WriteString(_T("00:00:00,000 --> 00:00:00,000"));

	//	fileOut.WriteString(_T("\nTime:"));
	//	fileOut.WriteString(strDate);

	//	fileOut.WriteString(_T("\nSubL:"));
	//	fileOut.WriteString(strSubL);

	//	fileOut.WriteString(_T("\nSubM:"));
	//	fileOut.WriteString(strSubM);

	//	fileOut.WriteString(_T("\nSubS:"));
	//	fileOut.WriteString(strSubS);

	//	fileOut.WriteString(_T("\nObject:"));
	//	fileOut.WriteString(strObject);

	//	fileOut.WriteString(_T("\nDescription:"));
	//	fileOut.WriteString(strDescription);

	//	fileOut.WriteString(_T("\nOwner:"));
	//	fileOut.WriteString(strOwner);
	//}
	//fileOut.Close();
}
