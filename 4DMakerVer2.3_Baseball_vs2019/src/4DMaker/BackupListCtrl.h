#pragma once
class CBackupListCtrl:public CListCtrl
{
public:
	CBackupListCtrl(void);
	~CBackupListCtrl(void);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP()
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnDestroy();	

public:
	afx_msg void DeleteAllItems();
	void Init();
	void AddFolder(CString strFolderName);
	void CheckAll(BOOL f);

private:
	bool CheckExist(CString str);		
};

