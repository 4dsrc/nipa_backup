#pragma once

#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

struct AdjustSquareInfo
{
	AdjustSquareInfo()
	{
		nIndex = 0;
		strDscId = "";

		dNorm = 0.f;
		dDistance = 0.f;
		dAngle = 0.f;
	}
	int nIndex;
	string strDscId;

	vector<Point3f> vecPtInit3DSquarePoint;
	vector<Point3f> vecPtInit3DSquarePointWithoutNormalization;

	vector<Point2f> vecPtSquarePoints;

	vector<Point2f> vecPtProjectedNormalVector;
	vector<Point2f> vecPtKZoneProjectedPoints;
	bool			bUseKZone;

	Point2f ptCenterPoint;

	double dNorm;
	double dDistance;
	double dAngle;

	Mat mRotMatrix;
};

class CESMAutoAdjustSquareCalibration
{
public:
	CESMAutoAdjustSquareCalibration(void);
	//CESMAutoAdjustSquareCalibration(IMAGE_SIZE eImageSize);
	~CESMAutoAdjustSquareCalibration(void);

public:
//	CDrawController pDrawController;
	cv::Size _sSize;

private:
	double _dFirstNorm;

	vector<Point3f>_vecPtInit3DSquarePoint;

public:
	Mat GetHomographyCameraToCamera	(vector<AdjustSquareInfo*> vecAdjustSquareInfo, int nIndex);
	Point2f CalcPointLocation(Mat homography,Point2f currentPoint);

private:
	vector<AdjustSquareInfo*> _vecAdjustSquareInfo;

public:
	bool	IsValidAdjustSquareInfo();
	void	AddSquarePoint(AdjustSquareInfo* vecPtSquarePoint);
	AdjustSquareInfo* GetSquareInfo(string strDscId);
	vector<AdjustSquareInfo*> GetSquarePoints(){return _vecAdjustSquareInfo;};

	void	SetSquarePointAt(string strDscId, Point2f pt1, Point2f pt2, Point2f pt3, Point2f pt4);
	Point2f	GetCenterPointAt(string strDscId);

	double	GetNormValueAt(string strDscId);
	void	SetNormValueAt(string stdDscId, double dValue);
	double	GetDistanceValueAt(string strDscId);
	void	SetDistanceValueAt(string stdDscId, double dValue);
	double	GetAngleValueAt(string strDscId);
	void	Set3DInitSquarePointAt(string strDscId, vector<Point3f> ptPoint);
	vector<Point3f> Get3DInitSquarePointAt(string strDscId);

private:
	vector<Point3f> _vecPtNormalVector;
	vector<Point2f> _vecPtProjectedNormalVector;

public:
	void SetInitSquarePoint(vector<Point3f> vecPtInitSquarePoint);
	vector<Point3f> GetInitSquarePoint(){return _vecPtInit3DSquarePoint;};
	void Set3DSquarePoint();
	void Normalization3DSquarePoint(vector<Point3f>& ptSquarePoint, int nMaxRange);

	void SetCenterPointAt(string strDscId, Point2f ptCenterPoint);
	void SetInitCenterPoint(string strDscId, Point2f ptCenterPoint);
	void SetInitCenterPointRange(string strStartDscId, string strEndDscId, Point2f ptCenterPoint);
	void ClearVectors();

	int GetAdjustIndexAt(string strDscId);

private:
	Mat _mCameraParameter;
	Mat _mDistCoeffs;
	Mat _mRotationMatrix;
	Mat _mTranslationMatrix;

public:
	void CalibrationUsingSquarePoints(string strDscId);
	//void RigidTransformationUsingSquarePoints(int nFrameIndex);
	void RigidTransformationUsingSquarePoints(string strDscId);
	vector<Point2f> ChangeCoordinateFromWorldToCamera(string strDscId, vector<Point3f> vecPtObjectPoints);
	vector<Point2f> ChangeCoordinateFromWorldToCameraAboutReverse(string strDscId, vector<Point3f> vecPtObjectPoints);

private:
	string	_strFirstStandardDscId;
	int		_nFirstStandardDscIndex;
public:
	
	void SetFirstStandardCameraId(string strDscId);
	void PreviewCalibration(Mat& mFrame, string strDscId);

public:
	Point2f GetSquareCentroidPoint(vector<vector<Point2f>> vVecPtSquarePoints, int nFrameIndex);

	void	RotateImage(int nFrameIndex);
	void	RotateImage(string strDscId);
	void	CpuMoveImage(Mat* gMat, int nX, int nY);
	void	CpuMakeMargin(Mat* gMat, int nX, int nY);

private:
	vector<Point3f> _vecPtKZone3DPoints;
	//vector<Point2f> _vecPtKZoneProjectedPoints;

	vector<Point3f> _vecPtCalibrationBar3DPoints;
	vector<Point2f> _vecPtCalibrationBarProjectedPoints;

private:
	int _nLinetickness;

public:
	Scalar Coloring(int index);
	void DrawSquare(Mat& mFrame, vector<Point2f> vecPtTemplatePoint);
	void DrawObject(Mat& mFrame, string strDscId);
	
	void DrawObjectForCalibrationBar(Mat& mFrame);
	vector<Point3f> GetCalibrationBar3DPoints(){return _vecPtCalibrationBar3DPoints;};
	void SetCalibrationBarPoints(vector<Point2f> vecPtCalibrationBarProjectedPoints)
	{_vecPtCalibrationBarProjectedPoints = vecPtCalibrationBarProjectedPoints;};
	void SetCalibrationBarObjectPoints();

	void DrawObjectForKZone(string strDscId, Mat& mFrame);
	void SetInitKZone3DPoints();

	void SetKZonePointAt(string strDscId);
	vector<Point2f> GetKZonePointAt(string strDscId);

	void SetUseKZoneAt(string strDscId, bool bUseKZone);
	bool GetUseKZoneAt(string strDscId);

	//wgkim 191030
	ofstream pFileOut;

};

