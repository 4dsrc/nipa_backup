#include "stdafx.h"
#include "4DMaker.h"
#include "DSCItem.h"
#include "ESMCtrl.h"
#include "ESMMovieMgr.h"

#include "ESMUtilRemoteFrame.h"
#include "ESMFileOperation.h"
#include "afxdialogex.h"
#include "ESMUtilRemoteCtrl.h"

#include "ESMIndex.h"
#include "ESMFunc.h"

IMPLEMENT_DYNAMIC(CESMUtilRemoteFrame, CDialog)

	CESMUtilRemoteFrame::CESMUtilRemoteFrame(CWnd* pParent /*=NULL*/)
	: CDialog(CESMUtilRemoteFrame::IDD, pParent)
{
	m_bFullScreen		= FALSE;
	m_nID				= -1;
	//m_pImage			= new CESMRemoteImage();
	m_bIsPause			= TRUE;
	m_bRunning			= FALSE;
	m_pArrImageMgr		= NULL;
	m_bFileExist		= FALSE;
	m_nFinishFrameIdx	= -1;
	m_nFinishMovieIdx	= -1;
	m_nCurPlayIdx		= -1;
	m_nSearchIdx		= 0;
	m_nCAMID			= -1;
	m_nCurFrameIdx		= -1;
	m_nFinishTime		= 999999999;
	m_bFinishState		= FALSE;
	m_bThreadFinish		= FALSE;
	m_nSyncFrameIdx		= -1;
	m_pRemoteCtrl		= NULL;
	m_nLastDelete		= -1;
	InitializeCriticalSection(&m_criImageLoad);
//	InitializeCriticalSection(&m_criOnSize);

	//m_criImageLoad 

	m_nRecordFrameCount	 = 30;
	m_nImageArrayCount	 = 20;

	m_nMarginX			 = 0;
	m_nMarginY			 = 0;
	m_nSrcWidth			 = 3840;
	m_nSrcHeight		 = 2160;
	m_bFocus			 = FALSE;

	m_nImageWidth		 = 160;
	m_nImageHeight		 = 90;
	//m_pstcMovieInfo = new CStatic;

	m_bIsMouseOver		 = FALSE;
	for(int i = 0; i < THREAD_TOTAL; i++)
	{
		m_bArrThreadState[i] = FALSE;
	}
	m_nSelectPoint		 = -1;
	m_bSelectLoad		 = FALSE;
	m_nZoomRatio		 = 100;
	m_bIsPressedRButton	 = FALSE;
	m_bAdjustLoad		 = FALSE;
	m_bRMouseActive		 = FALSE;
	m_bShowFrame		 = FALSE;
	m_bActiveFrame		 = FALSE;
}

CESMUtilRemoteFrame::~CESMUtilRemoteFrame()
{
	for(int i = 0 ; i < THREAD_TOTAL; i++)
	{
		if(m_bArrThreadState[i] == TRUE)
		{
			SetThreadFinish(TRUE);
			SetRecordState(TRUE);

			while(1)
			{
				if(m_bArrThreadState[i] == FALSE)
					break;

				Sleep(1);
			}
		}
	}

	//delete m_pstcMovieInfo;

	if(m_pArrImageMgr)
	{
		delete m_pArrImageMgr;
		m_pArrImageMgr = NULL;
	}
//	DeleteCriticalSection(&m_criOnSize);
	DeleteCriticalSection(&m_criImageLoad);
	/*if(m_pImage)
	{
	delete m_pImage;
	m_pImage = NULL;
	}*/
}

void CESMUtilRemoteFrame::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX,	IDC_REMOTE_FRAME_IMAGE, m_Image/**m_pImage*/);
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CESMUtilRemoteFrame, CDialog)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSELEAVE()
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
	ON_WM_MBUTTONUP()
END_MESSAGE_MAP()

BOOL CESMUtilRemoteFrame::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_matImage.create(720,1080,CV_8UC3);
	m_matImage = Scalar(0,0,0);

	CRect rc;
	GetClientRect(&rc);
	CalcMovieRatio(rc);

	return TRUE;  
}

void CESMUtilRemoteFrame::Run()
{
	/*for(int i = 0 ; i < 100 ; i++)
	{
	char strInput[40],strOutput[40];
	sprintf(strInput,"F:\\Movie\\%d_%d.mp4", GetCamID(),i);
	sprintf(strOutput,"F:\\Movie\\HD\\%d_%d.mp4", GetCamID(),i);

	VideoCapture vc(strInput);
	if(!vc.isOpened())
	break;

	VideoWriter writer(strOutput,cv::VideoWriter::fourcc('m','p','e','g'),30,cv::Size(1280,720),true);

	while(1)
	{
	Mat frame;
	vc>>frame;
	if(frame.empty())
	break;

	Mat re;
	resize(frame,re,cv::Size(1280,720),0,0,cv::INTER_LANCZOS4);
	writer<<re;
	}
	writer.release();
	}

	return;*/
	m_nCurFrameIdx = -1;
	if(GetIsFocus() == FALSE)
	{
		SetMouseClicked(FALSE);
		m_Image.SetRedRect(FALSE);
	}
	SetRMouseActive(FALSE);
	SetLastDeleteIdx(-1);
	m_Image.SetTimeIndex(-1,-1);
	m_Image.m_nCamID = GetCamID();
	ShowScreen(m_matImage);
	SetShowFrame(FALSE);
	SetAdjustInfo(m_strDSCID);
	//SetShowFrame(TRUE);
	//m_nZoomRatio		 = 100;
	
	
#if 0
	SetPlayState(FALSE);
	img = imread("M:\\10103.jpg",1);
	ShowScreen(img);
#else
	int nFrameCountCount;
	if(movie_frame_per_second == 25 || movie_frame_per_second == 50)
		nFrameCountCount = movie_frame_per_second - 1;
	else
		nFrameCountCount = movie_frame_per_second;

	m_nRecordFrameCount = nFrameCountCount;//ESMGetFrameRate();
	m_nImageArrayCount = 15;
	m_pArrImageMgr = new vector<ImageMgr>(m_nRecordFrameCount * m_nImageArrayCount);

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ThreadImageSelect2, this, 0, NULL);
	CloseHandle(hSyncTime);
	/*HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ThreadImageSelect, this, 0, NULL);
	CloseHandle(hSyncTime);*/

	HANDLE hSyncTime1 = NULL;
	hSyncTime1 = (HANDLE) _beginthreadex(NULL, 0, ThreadPathMgr, this, 0, NULL);
	CloseHandle(hSyncTime1);
#endif
}
void CESMUtilRemoteFrame::SetImageUsingZoomRatio(Mat &img)
{
	int nZoomValue = m_nZoomRatio;
	int nOriWidth  = img.cols;
	int nOriHeight = img.rows; 

	if(nZoomValue <= 100)
		return;

	double dbZoomRatio = (double)nZoomValue/100.0;

	if(dbZoomRatio <= 1.0)
		return;

	int nLeft = 0,nTop = 0,nWidth = 0,nHeight = 0;

	GetImageRect(dbZoomRatio,nOriWidth,nOriHeight,nLeft,nTop,nWidth,nHeight);

	Mat tmp = (img)(cv::Rect(nLeft,nTop,nWidth,nHeight));

	Mat re;
	//imshow("tmp",tmp);
	resize(tmp,re,cv::Size(nOriWidth,nOriHeight),0,0,CV_INTER_AREA);

	re.copyTo(img);
	//imshow("iii",img);
}
void CESMUtilRemoteFrame::GetImageRect(double dbZoomRatio,int nOriWidth,int nOriHeight,int &nLeft,int &nTop,int &nWidth,int &nHeight)
{
	//영상 센터
	int nCenterX = nOriWidth  / 2;
	int nCenterY = nOriHeight / 2;

	//비율에 따른 줌 계산
	nLeft = nCenterX - nOriWidth/dbZoomRatio/2;
	nTop  = nCenterY - nOriHeight/dbZoomRatio/2;

	nWidth = (nOriWidth/dbZoomRatio - 1);
	if((nLeft + nWidth) > nOriWidth)
		nLeft = nLeft - ((nLeft + nWidth) - nOriWidth);
	else if(nLeft < 0)
		nLeft = 0;

	nHeight = (nOriHeight/dbZoomRatio - 1);
	if((nTop + nHeight) > nOriHeight)
		nTop = nTop - ((nTop + nHeight) - nOriHeight);
	else if (nTop < 0)
		nTop = 0;
}
void CESMUtilRemoteFrame::ShowScreen(Mat frame,BOOL bColor/* = TRUE */)
{
	Mat re;
	//EnterCriticalSection(&m_criOnSize);

	if(bColor == FALSE)
		resize(m_matImage,re,cv::Size(m_nImageWidth,m_nImageHeight),0,1);
	else	
		resize(frame,re,cv::Size(m_nImageWidth,m_nImageHeight),0,0,1);

	if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT) == TRUE && GetIsFocus() ==TRUE
		&& GetMouseClicked() == TRUE && GetRMouseActive() == TRUE)
	{
		/*if(m_bIsPressedRButton)
			imshow("a",frame);*/
		DrawViewWindowAndCenterLine(&re);
		//destroyAllWindows();
	}
	SetImageUsingZoomRatio(re);
	if(GetShowFrame() == TRUE)
	{
		m_bActiveFrame = TRUE;
		m_Image.SetTimeIndex(GetCurPlayIdx(),GetCurFrameIdx());
		m_Image.SetDC(m_nImageWidth,m_nImageHeight,FALSE);
		m_Image.SetImageBuffer(re.data,m_nImageWidth,m_nImageHeight,3);
		m_Image.Redraw();
	}
//	LeaveCriticalSection(&m_criOnSize);
	
	Sleep((movie_next_frame_time - 15));

	re.release();
}

unsigned WINAPI CESMUtilRemoteFrame::ThreadImageSelect(LPVOID param)
{
	CESMUtilRemoteFrame* pRemoteFrame = (CESMUtilRemoteFrame*) param;
	pRemoteFrame->m_bArrThreadState[THREAD_SELIMAGE] = TRUE;

	pRemoteFrame->SetPlayRunning(TRUE);

	int nArrayCount = pRemoteFrame->m_nImageArrayCount;
	int nFrameCount = pRemoteFrame->m_nRecordFrameCount;

	int nFrameIdx = 0;
	int nMovIdx=0;
	int nIdx = 0;
	int nDeleteIdx = -1;
	int nDSCID = pRemoteFrame->m_nID;
	ESMLog(5,_T("[REMOTECTRL - %d] Image Select Create"),pRemoteFrame->m_nCAMID);
	int nSelectFrameIdx = 0,nSelectMovieIdx = 0;
#if 1
	while(1)
	{
#if 1
		Sleep(1);
		if(pRemoteFrame->GetThreadFinish() == TRUE)
			break;

		if(pRemoteFrame->GetLiveMgrSize() == 0 || pRemoteFrame->GetLiveMgrSize() <= nMovIdx)
			continue;

		EnterCriticalSection(&pRemoteFrame->m_criImageLoad);
		ImageLiveMgr stLiveMgr = pRemoteFrame->GetLiveMgr(nMovIdx);
		LeaveCriticalSection(&pRemoteFrame->m_criImageLoad);

		int nSize = stLiveMgr.pImageLiveArr->size();
		
		/*if(pRemoteFrame->GetSelectFrameSize() > 0)
		{
			pRemoteFrame->GetSelectTimeInfo(pRemoteFrame->m_nSelectFrameIndex - 1,
				nSelectMovieIdx,nSelectFrameIdx);
		}*/

		for(int i = 0 ; i < nSize; i++)
		{
			/*if(pRemoteFrame->GetSelectFrameSize() > 0)
			{
				if(pRemoteFrame->GetMouseClicked() == TRUE && pRemoteFrame->GetSelectFrameSize() != 0)
				{
					if(nMovIdx == nSelectMovieIdx && i == nSelectFrameIdx)
						pRemoteFrame->SetPlayState(FALSE);
				}
			}*/

			if(pRemoteFrame->GetPlayState() == FALSE)
			{
				while(1)
				{
					if(pRemoteFrame->GetPlayState() == TRUE)
						break;

					int nAddIdx = pRemoteFrame->GetPlayStatus();

					i += nAddIdx;

					if(i >= nSize)
					{
						i = 0;
						nMovIdx++;
					}
					else if(i < 0)
					{
						i = nSize - 1;
						nMovIdx--;
					}

					if(nMovIdx == nDeleteIdx && i <= 23)
					{
						i = 23;
						continue;
					}

					if(nMovIdx >= pRemoteFrame->GetLiveMgrSize())
						continue;

					if(nMovIdx < 0)
						nMovIdx = 0;

					if(pRemoteFrame->GetLastDeleteIdx() == nMovIdx)
					{
						nMovIdx = nMovIdx+1;
						i = 0;
					}

					Mat frame = pRemoteFrame->LoadImageFrame(nMovIdx,i);

					pRemoteFrame->ShowScreen(frame);

					pRemoteFrame->SetMakingTime(nMovIdx,i);

					if(pRemoteFrame->GetThreadFinish() == TRUE)
						break;

					Sleep(1);
				}

				//영상 저장 TEST
				/*pRemoteFrame->SaveImageArr(0,nMovIdx);*/

				//싱크 맞추기 여기서
				int nSyncIdx = pRemoteFrame->m_pRemoteCtrl->GetSyncFrameIdx(nDSCID);
				if(nSyncIdx != -100)
				{
					if(nSyncIdx >= pRemoteFrame->GetLiveMgrSize())
					{
						ESMLog(5,_T("Still doing Decoding [%d,%d]"),nSyncIdx,nMovIdx);
						nMovIdx = pRemoteFrame->GetLiveMgrSize() -1;
					}
					else if(nSyncIdx - 2 < nMovIdx)
					{
						nMovIdx = pRemoteFrame->GetLiveMgrSize() -1;
					}
					else
						nMovIdx = nSyncIdx;
				}
				break;
			}

			if(pRemoteFrame->GetThreadFinish() == TRUE)
				break;

			Mat frame = pRemoteFrame->LoadImageFrame(nMovIdx,i);

			pRemoteFrame->ShowScreen(frame);
			pRemoteFrame->SetMakingTime(nMovIdx,i);
		}

		nMovIdx++;
		if(nMovIdx - 2 >= 0)
		{
			//ESMLog(5,_T("[REMOTE] DELETE - %d"),nMovIdx-2);
			pRemoteFrame->DeleteLiveMgr(nMovIdx - 2);
			nDeleteIdx = nMovIdx - 2;
		}

		//pRemoteFrame->ShowScreen(pRemoteFrame->m_matImage,FALSE);
#else		
		nIdx = pRemoteFrame->GetPlayStatus();
		//nFrameIdx += nIdx;

		nFrameIdx = pRemoteFrame->GetCurFrameIdx() + nIdx;

		if(nFrameIdx >= nArrayCount * nFrameCount)
			nFrameIdx = 0;
		else if(nFrameIdx < 0)
			nFrameIdx = nArrayCount * nFrameCount - 1;

		if( pRemoteFrame->GetPlayState() == FALSE)
		{
			pRemoteFrame->SetCurFrameIdx(nFrameIdx);
			nFrameIdx = pRemoteFrame->GetScrollFrameIdx(nFrameIdx);
		}



		Mat frame = pRemoteFrame->LoadImageFrame(nFrameIdx++);

		//pRemoteFrame->SetCurFrameIdx(nFrameIdx);

		if(frame.empty() && pRemoteFrame->GetPlayState() == TRUE)
		{
			pRemoteFrame->ShowScreen(frame,FALSE);
			break;
		}

		if(pRemoteFrame->GetRecordState() == TRUE 
			&& pRemoteFrame->GetRecordFinishState() == TRUE)
		{
			pRemoteFrame->ShowScreen(frame,FALSE);
			break;
		}

		if(pRemoteFrame->GetThreadFinish() == TRUE)
		{
			pRemoteFrame->ShowScreen(frame,FALSE);
			break;
		}

		pRemoteFrame->ShowScreen(frame);
#endif	
	}
#else
	while(1)
	{
		nIdx = pRemoteFrame->GetPlayStatus();
		//Next Frame
		nFrameIdx += nIdx;

		if(nFrameIdx >= IMAGEARRAY * FRAMETOTAL)
			nFrameIdx = 0;
		else if(nFrameIdx < 0)
			nFrameIdx = IMAGEARRAY * FRAMETOTAL - 1;

		if( pRemoteFrame->GetPlayState() == FALSE)
		{
			nFrameIdx = pRemoteFrame->GetScrollFrameIdx(nFrameIdx);
			/*if(nFrameIdx == 0)
			nFrameIdx = IMAGEARRAY * FRAMETOTAL - 1;
			else
			nFrameIdx = 0;*/
		}
		/*else
		{
		if(nFrameIdx % 30 == 0)
		{
		pRemoteFrame->SetCurPlayIdx(nMovIdx++);
		}

		if(nFrameIdx == FRAMETOTAL * IMAGEARRAY)
		nFrameIdx = 0;
		}*/

		pRemoteFrame->SetCurFrameIdx(nFrameIdx);

		Mat frame = pRemoteFrame->LoadImageFrame(nFrameIdx++);

		if(frame.empty() && pRemoteFrame->GetPlayState() == TRUE)
		{
			pRemoteFrame->ShowScreen(frame,FALSE);
			break;
		}

		pRemoteFrame->ShowScreen(frame);

		/*if((nFrameIdx-1) % 30 == 0 && pRemoteFrame->GetPlayState() == TRUE)
		{
		pRemoteFrame->SetCurPlayIdx(nMovIdx++);
		}*/

		/*if(nFrameIdx == FRAMETOTAL * IMAGEARRAY && pRemoteFrame->GetPlayState() == TRUE)
		nFrameIdx = 0;*/
	}

#endif
	pRemoteFrame->SetFinishFrameIdx(-1);
	pRemoteFrame->SetFinishMovieIdx(-1);
	pRemoteFrame->SetPlayRunning(FALSE);
	pRemoteFrame->SetCurFrameIdx(-1);
	pRemoteFrame->SetRecordFinishState(FALSE);
	pRemoteFrame->SetPlayState(TRUE);
	pRemoteFrame->m_bFinishState = FALSE;
	pRemoteFrame->SetMakingTime(-1,-1);
	pRemoteFrame->DeleteAllLiveMgr();

	/*if(pRemoteFrame->m_pArrImageMgr)
	{
	delete pRemoteFrame->m_pArrImageMgr;
	pRemoteFrame->m_pArrImageMgr = NULL;
	}*/

	pRemoteFrame->m_bArrThreadState[THREAD_SELIMAGE] = FALSE;
	pRemoteFrame->m_pRemoteCtrl->SetPlayFinish(pRemoteFrame->m_nID,TRUE);
	ESMLog(5,_T("[REMOTECTRL - %d] Image Select Finish"),pRemoteFrame->m_nCAMID);
	return FALSE;	
}
int CESMUtilRemoteFrame::DoSyncCorrector(int nDSCID,int nCurIdx,BOOL bMode/* = FALSE*/)
{
	int nMovieIdx = 0;
#ifndef FRAMEVIEW
	int nSyncIdx = m_pRemoteCtrl->GetSyncFrameIdx(nDSCID);
	if(nSyncIdx != -100)
	{
		if(nSyncIdx >= GetLiveMgrSize())
		{
			ESMLog(5,_T("Still doing Decoding [%d,%d]"),nSyncIdx,nCurIdx);
			nMovieIdx = GetLiveMgrSize() -1;
		}
		else if(nSyncIdx - 2 < nCurIdx)
		{
			nMovieIdx = GetLiveMgrSize() -1;
		}
		else
			nMovieIdx = nSyncIdx;
	}
#else
	nMovieIdx = GetLiveMgrSize() -1;
	m_pRemoteCtrl->SetSyncInFrameView(m_nID,nMovieIdx,bMode);
	
#endif

	return nMovieIdx;
}
unsigned WINAPI CESMUtilRemoteFrame::ThreadImageSelect2(LPVOID param)
{
	CESMUtilRemoteFrame* pRemoteFrame = (CESMUtilRemoteFrame*) param;
	pRemoteFrame->m_bArrThreadState[THREAD_SELIMAGE] = TRUE;

	pRemoteFrame->SetPlayRunning(TRUE);

	int nArrayCount = pRemoteFrame->m_nImageArrayCount;
	int nFrameCount = pRemoteFrame->m_nRecordFrameCount;

	int nMovIdx=0;
	int nIdx = 0;
	int nDeleteIdx = -1;
	int nDSCID = pRemoteFrame->m_nID;
	ESMLog(5,_T("[REMOTECTRL - %d] Image Select Create"),pRemoteFrame->m_nCAMID);
	int nSelectFrameIdx = 0,nSelectMovieIdx = 0;
	BOOL bMode = FALSE;
	BOOL bSelector = FALSE;
	CString strDSCID = pRemoteFrame->m_strDSCID;
	while(1)
	{
		//TRACE(_T("SELECT MGR....."));
		Sleep(15);
		if(pRemoteFrame->GetThreadFinish() == TRUE)
			break;

		if(pRemoteFrame->GetSelectLoad() == TRUE)
			bMode = TRUE;

		if((pRemoteFrame->GetLiveMgrSize() == 0 || pRemoteFrame->GetLiveMgrSize() <= nMovIdx)
			&& bMode == FALSE)
			continue;

		if(bSelector == FALSE && bMode == FALSE)//Normal Mode
		{
			pRemoteFrame->SetAdjustInfo(strDSCID);
			EnterCriticalSection(&pRemoteFrame->m_criImageLoad);
			ImageLiveMgr stLiveMgr = pRemoteFrame->GetLiveMgr(nMovIdx);
			LeaveCriticalSection(&pRemoteFrame->m_criImageLoad);

			int nSize = stLiveMgr.pImageLiveArr->size();

			for(int i = 0 ; i < nSize; i++)
			{
				if(pRemoteFrame->GetPlayState() == FALSE)//일시 정지
				{
					nSelectMovieIdx = nMovIdx;
					nSelectFrameIdx = i;
					bSelector = TRUE;

					pRemoteFrame->SetCurPlayIdx(nSelectMovieIdx);
					pRemoteFrame->SetCurFrameIdx(nSelectFrameIdx);

					break;
				}

				if(pRemoteFrame->GetSelectLoad() == TRUE)
				{
					int nSelFrameIdx = pRemoteFrame->GetFrameIndexFromArr(pRemoteFrame->GetSelectorNum());
					int nSelMovieIdx = pRemoteFrame->GetMovieIndexFromArr(pRemoteFrame->GetSelectorNum());

					if(nSelMovieIdx == nMovIdx && nSelectFrameIdx == i)//재생 중 영상 선택 포인트 이동
					{
						nSelectMovieIdx = nMovIdx;
						nSelectFrameIdx = i;

						pRemoteFrame->SetCurPlayIdx(nSelectMovieIdx);
						pRemoteFrame->SetCurFrameIdx(nSelectFrameIdx);

						break;
					}
					if(nMovIdx > nSelMovieIdx)//이미 재생 된 영상 중 선택 포인트 이동 시
					{
						nSelectMovieIdx = nSelMovieIdx;
						nSelectFrameIdx = nSelFrameIdx;

						bMode = TRUE;
						pRemoteFrame->SetCurPlayIdx(nSelectMovieIdx);
						pRemoteFrame->SetCurFrameIdx(nSelectFrameIdx);
						
						break;
					}
				}
				Mat frame = pRemoteFrame->LoadImageFrame(nMovIdx,i);
				pRemoteFrame->ShowScreen(frame);
			}

			if(nMovIdx - 2 >= 0)
			{
				pRemoteFrame->DeleteLiveMgr(nMovIdx - 2);
				nDeleteIdx = nMovIdx - 2;
			}
			nMovIdx++;
		}
		else if(bSelector == TRUE && bMode == FALSE)
		{
			TRACE1("[%d] - bSelector == TRUE && bMode == FALSE\n",pRemoteFrame->m_nID);
			pRemoteFrame->SetMakingTime(nSelectMovieIdx,nSelectFrameIdx);
			
			pRemoteFrame->SetSelectLoad(FALSE);
			bSelector = FALSE;
			
			while(1)
			{
				Mat frame = pRemoteFrame->LoadImageFrame(nSelectMovieIdx,nSelectFrameIdx);

				pRemoteFrame->ShowScreen(frame);

				if(pRemoteFrame->GetPlayState() == TRUE)
				{
					bMode = FALSE;
					break;
				}

				if(pRemoteFrame->GetSelectLoad() == TRUE)
				{
					bMode = TRUE;
					break;
				}
				nSelectFrameIdx += pRemoteFrame->GetPlayStatus();

				pRemoteFrame->GetNextFrameIdx(nSelectMovieIdx,nSelectFrameIdx);

				pRemoteFrame->SetMakingTime(nSelectMovieIdx,nSelectFrameIdx);

				if(pRemoteFrame->GetThreadFinish() == TRUE)
					break;

				Sleep(1);
			}
			nMovIdx = pRemoteFrame->DoSyncCorrector(nDSCID,nSelectMovieIdx,bMode);
			
			if(nMovIdx != 0)
			{
				for(int i = nSelectMovieIdx ; i < nMovIdx; i++)
				{
					pRemoteFrame->DeleteLiveMgr(i);
				}
			}
			
			nMovIdx++;
		}
		else if(bSelector == FALSE && bMode == TRUE)
		{
			try
			{
				TRACE1("[%d] - bSelector == FALSE && bMode == TRUE\n",pRemoteFrame->m_nID);
				pRemoteFrame->SetMakingTime(nSelectMovieIdx,nSelectFrameIdx);

				pRemoteFrame->SetPlayState(FALSE);

				nSelectFrameIdx = pRemoteFrame->GetFrameIndexFromArr(pRemoteFrame->GetSelectorNum());
				nSelectMovieIdx = pRemoteFrame->GetMovieIndexFromArr(pRemoteFrame->GetSelectorNum());

				if(nSelectMovieIdx > pRemoteFrame->GetLiveMgrSize() - 1)
					continue;

				pRemoteFrame->SetSelectLoad(FALSE);

				EnterCriticalSection(&pRemoteFrame->m_criImageLoad);
				ImageLiveMgr LiveMgr = pRemoteFrame->GetLiveMgr(nSelectMovieIdx);
				LeaveCriticalSection(&pRemoteFrame->m_criImageLoad);

				int nSize = LiveMgr.pImageLiveArr->size();
				int nBaseMovieIdx = nSelectMovieIdx;
				while(1)
				{
					Mat frame = pRemoteFrame->LoadImageFrame(nSelectMovieIdx,nSelectFrameIdx);

					pRemoteFrame->ShowScreen(frame);

					if(pRemoteFrame->GetPlayState() == TRUE) 
					{
						bMode = FALSE;
						pRemoteFrame->SetSelectLoad(FALSE);
						break;
					}

					if(pRemoteFrame->GetSelectLoad() == TRUE)
					{
						bMode = TRUE;
						break;
					}

					nSelectFrameIdx += pRemoteFrame->GetPlayStatus();

					pRemoteFrame->GetNextOrPrevFrame(nSelectMovieIdx,nSelectFrameIdx,nBaseMovieIdx);

					pRemoteFrame->SetMakingTime(nSelectMovieIdx,nSelectFrameIdx);

					if(pRemoteFrame->GetThreadFinish() == TRUE)
						break;

					Sleep(1);
				}
				nMovIdx = pRemoteFrame->DoSyncCorrector(nDSCID,nSelectMovieIdx,bMode);

				if(nMovIdx != 0)
				{
					for(int i = nSelectMovieIdx ; i < nMovIdx; i++)
					{
						pRemoteFrame->DeleteLiveMgr(i);
					}
				}
				nMovIdx++;
			}
			catch(const exception &e)
			{
				AfxMessageBox(_T("?????????????????????????????"));
				continue;
			}
		}
	}

	pRemoteFrame->SetFinishFrameIdx(-1);
	pRemoteFrame->SetFinishMovieIdx(-1);
	pRemoteFrame->SetPlayRunning(FALSE);
	pRemoteFrame->SetCurFrameIdx(-1);
	pRemoteFrame->SetRecordFinishState(FALSE);
	pRemoteFrame->SetPlayState(TRUE);
	pRemoteFrame->m_bFinishState = FALSE;
	pRemoteFrame->SetMakingTime(-1,-1);
	pRemoteFrame->DeleteAllLiveMgr();
	pRemoteFrame->ClearSaveIndex();
	pRemoteFrame->SetSelectLoad(FALSE);

	/*if(pRemoteFrame->m_pArrImageMgr)
	{
	delete pRemoteFrame->m_pArrImageMgr;
	pRemoteFrame->m_pArrImageMgr = NULL;
	}*/

	pRemoteFrame->m_bArrThreadState[THREAD_SELIMAGE] = FALSE;
	pRemoteFrame->m_pRemoteCtrl->SetPlayFinish(pRemoteFrame->m_nID,TRUE);
	ESMLog(5,_T("[REMOTECTRL - %d] Image Select Finish"),pRemoteFrame->m_nCAMID);
	return FALSE;	
}
void CESMUtilRemoteFrame::GetNextOrPrevFrame(int &nMovIdx,int &nFrmIdx,int nBaseFrame)
{
	if(nMovIdx > GetLiveMgrSize() -1)
		return;

	EnterCriticalSection(&m_criImageLoad);
	ImageLiveMgr stLiveMgr = GetLiveMgr(nMovIdx);
	LeaveCriticalSection(&m_criImageLoad);

	int nSize = stLiveMgr.pImageLiveArr->size();

	if(nFrmIdx >= nSize)
	{
		nFrmIdx = 0;
		nMovIdx++;
	}
	else if (nFrmIdx < 0)
	{
		nFrmIdx = nSize-1;
		nMovIdx--;
	}

	if(nMovIdx > nBaseFrame + 2)
	{
		nMovIdx = nBaseFrame+2;
		nFrmIdx = nSize - 1;
		//return;
	}

	if(nMovIdx < nBaseFrame - 1)
	{
		nMovIdx = nBaseFrame -1;
		nFrmIdx = 0;
	}
}
void CESMUtilRemoteFrame::GetNextFrameIdx(int &nMovieIdx,int &nIdx)
{
	if(nMovieIdx > GetLiveMgrSize() -1)
		return;

	EnterCriticalSection(&m_criImageLoad);
	ImageLiveMgr stLiveMgr = GetLiveMgr(nMovieIdx);
	LeaveCriticalSection(&m_criImageLoad);

	int nSize = stLiveMgr.pImageLiveArr->size();

	if(nIdx >= nSize)
	{
		nIdx = 0;
		nMovieIdx++;
	}
	else if(nIdx < 0)
	{
		nIdx = nSize - 1;
		nMovieIdx--;
	}

	if(nMovieIdx >= GetLiveMgrSize())
		return;

	if(nMovieIdx < 0)
	{
		nMovieIdx = 0;
		nIdx = 0;
	}
}

BOOL CESMUtilRemoteFrame::FinishCheckPathMgr(int nMovieIdx)
{
	int nFinishIdx = GetFinishRecordTime();

	if(nFinishIdx == -1)
		return FALSE;

	if(nFinishIdx <= nMovieIdx + 1 )
	{
		if(GetMovieCount() > 0)
			return FALSE;
		else
			return TRUE;
	}
	else
		return FALSE;
}

unsigned WINAPI CESMUtilRemoteFrame::ThreadPathMgr(LPVOID param)
{
	CESMUtilRemoteFrame* pRemoteFrame = (CESMUtilRemoteFrame*) param;
	pRemoteFrame->m_bArrThreadState[THREAD_PATHMGR] = TRUE;

	CString strFile;
	int nIndex = 0;
	int nCAMID = pRemoteFrame-> GetCamID();
	int nMovieIdx = 0;
	int nFrameIdx = 0;
	ESMLog(5,_T("[REMOTECTRL - %d] PathMgr Create"),pRemoteFrame->m_nCAMID);
#ifndef TEST
	//CStringArray strArr;
	for(int i = 0 ; i < 30; i++)
	{
#ifndef FHD
		strFile.Format(_T("F:\\Test\\%d_%d.mp4"),nCAMID,i);
#else
		strFile.Format(_T("F:\\Movie\\HD\\%d_%d.mp4"),nCAMID,i);
#endif
		//strArr.Add(strFile);
		pRemoteFrame->m_pRemoteCtrl->AddDSCPath(strFile);
	}
#endif
	int nArrayCount = pRemoteFrame->m_nImageArrayCount;//7
	int nCheckCnt = nArrayCount/3;
#if 1
	while(1)
	{
		//TRACE(_T("PATH MGR..."));
		//Path Load Finish
		if(pRemoteFrame->GetRecordState() == FALSE 
			&& pRemoteFrame->GetMovieCount() == 0
			&& pRemoteFrame->FinishCheckPathMgr(nMovieIdx)
			/*(pRemoteFrame->GetFinishRecordTime()+1 <= nMovieIdx*/ 
			/*|| pRemoteFrame->GetFinishRecordTime()    == nMovieIdx + 1*/)

			break;

		if(pRemoteFrame->GetThreadFinish() == TRUE)
			break;

		if(pRemoteFrame->GetMovieCount() == 0)
		{
			Sleep(10);
			continue;
		}

		strFile = pRemoteFrame->GetMoviePath();

		/*if(nMovieIdx - nCheckCnt > pRemoteFrame->GetCurPlayIdx())
		{
		while(1)
		{
		if(nMovieIdx - nCheckCnt <= pRemoteFrame->GetCurPlayIdx() &&
		pRemoteFrame->GetPlayState() == TRUE)
		break;

		if(pRemoteFrame->GetThreadFinish() == TRUE)
		break;

		Sleep(10);
		}
		}*/

		if(pRemoteFrame->GetThreadFinish() == TRUE)
			break;

		CT2CA pszConvertedAnsiString (strFile);
		string strDst(pszConvertedAnsiString);

		VideoCapture vc(strDst);
		if(vc.isOpened())
		{
			nMovieIdx = _ttoi(ESMGetSecIdxFromPath(strFile));//nIndex;
			nFrameIdx = pRemoteFrame->ReadFrameCount(strFile);//(int)vc.get(cv::CAP_PROP_FRAME_COUNT);

			HANDLE hSyncTime = NULL;

			ThreadFrameData* pFrameData = new ThreadFrameData;
			pFrameData->pParent			= pRemoteFrame;
			pFrameData->nMovIdx			= nMovieIdx;
			pFrameData->strPath			= strFile;
			pFrameData->nFrameIdx		= nFrameIdx;
			pFrameData->vc				= vc;
			hSyncTime = (HANDLE)_beginthreadex(NULL,0,ThreadDecode,(void*)pFrameData,0,NULL);
			CloseHandle(hSyncTime);
		}

		pRemoteFrame->RemoveMoviePath();
	}
#else
	while(1)
	{
		if(pRemoteFrame->GetMovieCount() == 0)
		{
			Sleep(10);
			continue;
		}
		else
			break;
	}

	while(pRemoteFrame->GetMovieCount())
	{

		strFile = pRemoteFrame->GetMoviePath();

		if(nIndex - nCheckCnt > pRemoteFrame->GetCurPlayIdx())
		{
			while(1)
			{
				if(nIndex - nCheckCnt <= pRemoteFrame->GetCurPlayIdx() &&
					pRemoteFrame->GetPlayState() == TRUE)
					break;

				Sleep(10);
			}
		}

		CT2CA pszConvertedAnsiString (strFile);
		string strDst(pszConvertedAnsiString);

		VideoCapture vc(strDst);
		nMovieIdx = _ttoi(ESMGetSecIdxFromPath(strFile));//nIndex;
		nFrameIdx = (int)vc.get(cv::CAP_PROP_FRAME_COUNT);

		HANDLE hSyncTime = NULL;

		ThreadFrameData* pFrameData = new ThreadFrameData;
		pFrameData->pParent			= pRemoteFrame;
		pFrameData->nMovIdx			= nMovieIdx;
		pFrameData->strPath			= strFile;
		pFrameData->nFrameIdx		= nFrameIdx;
		pFrameData->vc				= vc;
		hSyncTime = (HANDLE)_beginthreadex(NULL,0,ThreadDecode,(void*)pFrameData,0,NULL);
		CloseHandle(hSyncTime);
#ifdef TEST
		Sleep(1000);
#endif

		nIndex++;

		//strArr.RemoveAt(0);
		pRemoteFrame->RemoveMoviePath();
	}
#endif
	pRemoteFrame->SetFinishRecordTime(-1);
	pRemoteFrame->SetFinishFrameIdx(nFrameIdx);
	pRemoteFrame->SetFinishMovieIdx(nMovieIdx);
	pRemoteFrame->RemoveAllPath();
	ESMLog(5,_T("[REMOTECTRL - %d] PathMgr Finish : %d %d"),pRemoteFrame->m_nCAMID,nMovieIdx,nFrameIdx);

	pRemoteFrame->m_bArrThreadState[THREAD_PATHMGR] = FALSE;

	return TRUE;
}

unsigned WINAPI CESMUtilRemoteFrame::ThreadDecode(LPVOID param)
{
	ThreadFrameData*pFrameData			= (ThreadFrameData*) param;
	CESMUtilRemoteFrame* pRemoteFrame	= pFrameData->pParent;
	int nMovIdx							= pFrameData->nMovIdx;
	CString strPath						= pFrameData->strPath;
	int nFrameIdx						= pFrameData->nFrameIdx;
	VideoCapture vc						= pFrameData->vc;
	delete pFrameData;

	int nCount = 0;

	vector<ImageArr>* imgArr = new vector<ImageArr>(nFrameIdx);
	ImageArr stImage;

	ImageLiveMgr stLiveMgr;
	stLiveMgr.pImageLiveArr	= imgArr;
	stLiveMgr.nMovidIdx		= nMovIdx;

	char strID[100];
	int nCamID = pRemoteFrame->GetCamID();
	TRACE2("[%d] - Enter Decode Frame(%d)\n",pRemoteFrame->m_nID,nMovIdx);

	while(1)
	{
		Mat frame;
		vc>>frame;
		if(frame.empty())
			break;
#if 1
		stImage.img			= frame.clone();
		stImage.nFrameIdx	= nCount;
		stImage.bFinish		= TRUE;

		imgArr->at(nCount) = stImage;
#else
		pRemoteFrame->StackImage(frame,nMovIdx,nFrameIdx);
#endif

		nCount++;
	}
	TRACE2("[%d] - Leave Decode Frame(%d)\n",pRemoteFrame->m_nID,nMovIdx);
	stLiveMgr.bDecodeFinish	= TRUE;

	EnterCriticalSection(&pRemoteFrame->m_criImageLoad);
	pRemoteFrame->AddLiveMgr(stLiveMgr);
	//ESMLog(5,_T("[RemoteFrame] - AddLiveMgr : %d"),pRemoteFrame->GetLiveMgrSize());
	LeaveCriticalSection(&pRemoteFrame->m_criImageLoad);
	
	TRACE2("[%d] - Add Decode Frame(%d)\n",pRemoteFrame->m_nID,nMovIdx);
	
	CESMFileOperation fo;
	if(fo.IsFileExist(strPath))
		fo.Delete(strPath);

	TRACE2("[%d] - Delete File Decode Frame(%d)\n",pRemoteFrame->m_nID,nMovIdx);

	vc.release();

	return TRUE;
}

void CESMUtilRemoteFrame::StackImage(Mat img,int nMovIdx,int nFrameIdx)
{
	int nCurIdx = (nMovIdx % m_nImageArrayCount) * m_nRecordFrameCount + nFrameIdx;

	char strID[100];
	sprintf(strID,"[%d]%d-%d",GetCamID(),nMovIdx,nFrameIdx);

	putText(img,strID,cv::Size(0,50),
		CV_FONT_HERSHEY_COMPLEX,2.0,cv::Scalar(255,255,255),2,8);

	EnterCriticalSection(&m_criImageLoad);
	ImageMgr* pImg  = &m_pArrImageMgr->at(nCurIdx);
	pImg->nFrameIdx = nFrameIdx;
	pImg->nMovIdx	= nMovIdx;
	pImg->Image		= img.clone();
	pImg->bFinish	= TRUE;
	LeaveCriticalSection(&m_criImageLoad);
}

Mat	 CESMUtilRemoteFrame::LoadImageFrame(int nIdx)
{
	BOOL bFinish = FALSE;
	int nTmp = nIdx;
	if(GetFinishMovieIdx() != -1 && GetFinishFrameIdx() != -1)
	{
		int nFinishCnt = (nIdx % m_nRecordFrameCount);
		if(nFinishCnt == 0)
			nFinishCnt = 30;

		if(GetFinishFrameIdx() ==  nFinishCnt
			&& GetFinishMovieIdx() == (GetCurPlayIdx())
			&& GetRecordState() == FALSE)
		{
			SetRecordFinishState(TRUE);
			//m_bFinishState = TRUE;
		}
	}

	if(m_bFinishState == TRUE)
	{
		if(GetRecordState() == FALSE)
		{
			nIdx--;
			if(nIdx == -1)
				nIdx = m_nImageArrayCount * m_nRecordFrameCount - 1;
		}
		else
			bFinish = TRUE;
	}
	if(GetPlayState() == FALSE && GetPlayState() == FALSE)
		nIdx = nTmp;

	EnterCriticalSection(&m_criImageLoad);
	ImageMgr* pImg = &m_pArrImageMgr->at(nIdx);
	LeaveCriticalSection(&m_criImageLoad);
	BOOL bRecordFinish = FALSE;
	BOOL bRecordStart = FALSE;

	if(pImg->bFinish == FALSE && GetPlayState() == TRUE)
	{
		TRACE1("[%d] - Wait LoadFrame2",m_nID);

		while(1)
		{
			if(pImg->bFinish == TRUE)
				break;

			if(GetFinishFrameIdx() != -1 && GetFinishMovieIdx() != -1)
				break;

			Sleep(1);
		}
	}

	SetCurFrameIdx(nIdx);
	SetCurPlayIdx(pImg->nMovIdx);

	if(bFinish)
	{
		Mat tmp;
		return tmp;
	}
	else
	{
		//if(GetPlayState() == TRUE)
		//pImg->bFinish = FALSE;

		return pImg->Image;
	}
}

Mat CESMUtilRemoteFrame::LoadImageFrame(int nMovieIdx,int nFrameIdx)
{
	BOOL bFinish = FALSE;

	Mat a(100,100,CV_8UC3,Scalar(0,0,0));

	if(nMovieIdx > GetLiveMgrSize() - 1)
		return a;

	EnterCriticalSection(&m_criImageLoad);
	ImageLiveMgr stLiveMgr = m_ArrLiveMgr.at(nMovieIdx);
	LeaveCriticalSection(&m_criImageLoad);

	//if(m_ArrLiveMgr.at(nMovieIdx).bDecodeFinish == FALSE)
	if(stLiveMgr.bDelete == FALSE)
	{
		if(stLiveMgr.pImageLiveArr->at(nFrameIdx).bFinish == FALSE)
		{
			TRACE1("[%d] - Wait LoadFrame",m_nID);
			while(1)
			{
				if(stLiveMgr.pImageLiveArr->at(nFrameIdx).bFinish == TRUE)
					break;

				Sleep(1);
			}
		}
	}
	else
	{
		return a;
	}
	if(m_bAdjustLoad == FALSE)
		SetAdjustInfo(m_strDSCID);

	if(GetPlayState() == TRUE)
	{
		m_pRemoteCtrl->SetSyncFrameIdx(m_nID,nMovieIdx,nFrameIdx);
	}

	SetCurFrameIdx(nFrameIdx);
	SetCurPlayIdx(nMovieIdx);

	Mat image = stLiveMgr.pImageLiveArr->at(nFrameIdx).img.clone();

	if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT) == TRUE 
		&& GetIsFocus() && GetMouseClicked()/* && GetMouseOver()*/
		/*&& GetPlayState() == FALSE*/)
	{
		//Adapt Adjust
		int nWidth = image.cols;
		int nHeight = image.rows;

		Mat tmp;
		tmp = CreateAdjustImage(image,nWidth,nHeight,
			m_nImageWidth,m_nImageHeight,ESMGetReverseMovie()).clone();

		int nTime = ESMGetTimeFromIndex(nMovieIdx,nFrameIdx);

		/*char strID[1024];
		sprintf(strID,"[%d]%d.%03d",GetCamID(),nTime/1000,nTime%1000);
		putText(tmp,strID,cv::Size(0,50),
			CV_FONT_HERSHEY_COMPLEX,2.0,cv::Scalar(255,255,255),2,8);*/

		return tmp;

	}
	else
	{
		int nTime = ESMGetTimeFromIndex(nMovieIdx,nFrameIdx);

		/*char strID[1024];
		sprintf(strID,"[%d]%d.%03d",GetCamID(),nTime/1000,nTime%1000);

		putText(image,strID,cv::Size(0,50),
			CV_FONT_HERSHEY_COMPLEX,2.0,cv::Scalar(255,255,255),2,8);*/

		return image;
	}
}

int CESMUtilRemoteFrame::GetPlayStatus()
{
	CString strLog;
	int nIdx = 1;
	if(GetPlayState() == FALSE)
	{
		strLog.Format(_T("[%d] - Enter GetPlayStatus\n"),m_nID);
		TRACE(strLog);
		while(1)
		{
			if(GetPlayState() == TRUE)
			{
				strLog.Format(_T("[%d]Leave GetPlayStatus"),m_nID);
				TRACE(strLog);
				nIdx = 0;
				break;
			}

			if(m_nSearchIdx == 2)
			{
				nIdx = 1;
				m_nSearchIdx = 0;
				break;
			}

			if(m_nSearchIdx == -2)
			{
				nIdx = -1;
				m_nSearchIdx = 0;
				break;
			}
			if(GetRecordFinishState() == TRUE)
			{
				nIdx = 0;
				break;
			}

			if(GetThreadFinish() == TRUE)
				break;

			if(GetSelectLoad() == TRUE)
				break;

			Sleep(10);
		}
		strLog.Format(_T("[%d] - Leave GetPlayStatus\n"),m_nID);
		TRACE(strLog);
	}

	return nIdx;
}

int	CESMUtilRemoteFrame::GetScrollFrameIdx(int nFrameIdx)
{
	//if i == 0, find max frame index
	//else i == max, find min frame index

	int nMax = 0;
	int nMin = 999999999;
	int nMaxMovieIdx = -1;
	int nMinMovieIdx = 999999999;

	if(GetFinishFrameIdx() == (nFrameIdx % 30) && GetFinishMovieIdx() == GetCurPlayIdx())
	{
		//마지막 프레임에서 Array 첫 프레임 이동 시
		for(int i = 0 ; i < m_pArrImageMgr->size(); i+= m_nRecordFrameCount)
		{
			int nMovieIdx = m_pArrImageMgr->at(i).nMovIdx;

			if(nMovieIdx < nMinMovieIdx)
			{
				nMin = i;
				nMinMovieIdx = nMovieIdx;
			}
		}

		return nMin;
		//for(int i = 0 ; i)
	}
	else if(nFrameIdx == 0)
	{
		int nInputMovieIdx = m_pArrImageMgr->at(nFrameIdx).nMovIdx;

		if(nInputMovieIdx == m_nCurPlayIdx+1)
			return 0;

		vector<int> nArrMovieIdx;

		//첫번째 프레임에서 Array 디코딩 된 마지막 프레임 이동시
		for(int i = 0 ; i < m_pArrImageMgr->size(); i+= m_nRecordFrameCount)
		{
			int nMovieIdx = m_pArrImageMgr->at(i).nMovIdx;
			//nArrMovieIdx.push_back(nMovieIdx);

			if(nMovieIdx > nMaxMovieIdx)
			{
				nMax = i;
				nMaxMovieIdx = nMovieIdx;
			}
			if(nMovieIdx == nInputMovieIdx-1)
			{
				//nMax = i;
				//nMaxMovieIdx = nMovieIdx;
				return 0;
				break;
			}
		}
		//BOOL bCheck = FALSE;


		int nFrame = nMax + m_nRecordFrameCount - 1;

		for(int i = nMax; i < nMax + m_nRecordFrameCount; i++)
		{
			int nMovieIdx = m_pArrImageMgr->at(i).nMovIdx;

			if(nMaxMovieIdx != nMovieIdx)
			{
				nFrame = i - 1;
				break;
			}
		}

		return nFrame;
		//return 0;//nMax + FRAMETOTAL - 1;

	}
	else
	{
		return nFrameIdx;
	}

	/*if(nFrameIdx == 0)
	{
	int nStartIdx = nMax * FRAMETOTAL;
	int nRerutnIdx;

	for(int i = nStartIdx ; i < nStartIdx+FRAMETOTAL ; i++)
	{
	int nMovieIdx = m_pArrImageMgr->at(i).nMovIdx;

	if(nMovieIdx == nMaxMovieIdx)
	nRerutnIdx = i;
	else
	continue;
	}

	return nRerutnIdx;
	}
	else
	return nMin * 30;*/
}

void CESMUtilRemoteFrame::OnRButtonUp(UINT nFlags, CPoint point)
{
	if(GetIsFocus() == FALSE)
		return;

	SetRMouseActive(TRUE);
	m_bIsPressedRButton = FALSE;
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	
	if(!(GetAsyncKeyState(VK_LSHIFT) & 0x8000))
	{
		point.x = fixXValueWhenShiftKeyIsPressed;
	}
	
	if(GetMouseOver())
	{
		m_ptImage.x = point.x - m_rcImage.left;
		m_ptImage.y = point.y - m_rcImage.top;
	}

	if(m_pRemoteCtrl->m_pTemplateMgr == NULL)
		return;

	//ESMLog(5,_T("%d Enter OnRButtonUp",GetCamID()));
//	EnterCriticalSection(&m_criOnSize);
	CPoint pImageSize = GetImageFrameSize();
	track_info pTrackInfo = ESMGetTrackInfo();

	cv::Point2f ptImage;
	ptImage.x = m_ptImage.x *= (double)pTrackInfo.nWidth / pImageSize.x;
	ptImage.y = m_ptImage.y *= (double)pTrackInfo.nHeight / pImageSize.y;

	double dbZoomRatio = (double)m_nZoomRatio / 100.0;
	if(dbZoomRatio < 1.0)
		dbZoomRatio = 1.0;

	int nLeft = 0,nTop = 0,nReWidth = 0,nReHeight = 0;

	GetImageRect(dbZoomRatio,pTrackInfo.nWidth,pTrackInfo.nHeight,nLeft,nTop,nReWidth,nReHeight);

	ptImage.x = (ptImage.x / dbZoomRatio) + nLeft;
	ptImage.y = (ptImage.y / dbZoomRatio) + nTop;

	ESMLog(5, _T("Template Point : (%.3f, %.3f)"), ptImage.x, ptImage.y);

	m_pRemoteCtrl->m_pTemplateMgr->ClearArrTemplatePoint();	
	m_pRemoteCtrl->m_pTemplateMgr->SetCenterPoint(ptImage);

	//17-02-09	
	m_pRemoteCtrl->m_pTemplateMgr->SetDscIndex(m_pRemoteCtrl->m_strArrDSCID[m_nID]);
	m_pRemoteCtrl->m_pTemplateMgr->SetArrTemplatePoint();

	/*int nCurMovieIdx = GetCurPlayIdx();
	int nCurFrameIdx = GetCurFrameIdx();

	ImageLiveMgr stLiveMgr = m_ArrLiveMgr.at(nCurMovieIdx);

	Mat pImage = stLiveMgr.pImageLiveArr->at(nCurFrameIdx).img.clone();
	Mat pAdjImage = CreateAdjustImage(pImage, pImage.cols, pImage .rows,
		m_nImageWidth,m_nImageHeight,ESMGetReverseMovie()).clone();*/
//	LeaveCriticalSection(&m_criOnSize);
	//ESMLog(5,_T("%d Leave OnRButtonUp",GetCamID()));

	//ShowScreen(pAdjImage);

	CDialog::OnRButtonUp(nFlags, point);
}

void CESMUtilRemoteFrame::OnRButtonDown(UINT nFlags, CPoint point)
{
	if(GetIsFocus() == FALSE)
		return;
	
	SetRMouseActive(TRUE);
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
//	EnterCriticalSection(&m_criOnSize);	
	m_bIsPressedRButton = TRUE;
	fixXValueWhenShiftKeyIsPressed = point.x;

	if(GetMouseOver())
	{
		m_ptImageCenter.x = point.x - m_rcImage.left;
		m_ptImageCenter.y = point.y - m_rcImage.top;
	}

	if(m_pRemoteCtrl->m_pTemplateMgr == NULL)
		return;

	track_info pTrackInfo = ESMGetTrackInfo();
	CPoint pImageSize = GetImageFrameSize();

	cv::Point2f ptImageCenter;
	ptImageCenter.x = m_ptImageCenter.x *= (double)pTrackInfo.nWidth / pImageSize.x;
	ptImageCenter.y = m_ptImageCenter.y *= (double)pTrackInfo.nHeight / pImageSize.y;

	double dbZoomRatio = (double)m_nZoomRatio / 100.0;
	if(dbZoomRatio < 1.0)
		dbZoomRatio = 1.0;

	int nLeft = 0,nTop = 0,nReWidth = 0,nReHeight = 0;

	GetImageRect(dbZoomRatio,pTrackInfo.nWidth,pTrackInfo.nHeight,nLeft,nTop,nReWidth,nReHeight);

	ptImageCenter.x = (ptImageCenter.x / dbZoomRatio) + nLeft;
	ptImageCenter.y = (ptImageCenter.y / dbZoomRatio) + nTop;
	
	ESMLog(5, _T("Template Center Point : (%.3f, %.3f)"), ptImageCenter.x, ptImageCenter.y);

	m_pRemoteCtrl->m_pTemplateMgr->SetOverBoundaryFlag(FALSE);
	m_pRemoteCtrl->m_pTemplateMgr->SetViewPoint(ptImageCenter);
//	LeaveCriticalSection(&m_criOnSize);
	CDialog::OnRButtonDown(nFlags, point);
}

void CESMUtilRemoteFrame::OnLButtonDown(UINT nFlags, CPoint point)
{
	CDialog::OnLButtonDown(nFlags, point);
}

void CESMUtilRemoteFrame::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//m_Image.DrawRect(TRUE);
	if(GetMouseClicked())
	{
		m_Image.SetRedRect(FALSE);
		SetMouseClicked(FALSE);
		SetIsFocus(FALSE);
	}
	else
	{
		SetItAsFocus();

		m_Image.SetRedRect(TRUE);
		SetMouseClicked(TRUE);
		SetIsFocus(TRUE);
	}

	if(GetPlayState() == FALSE)
		ShowScreenIfPauseState();

	m_pRemoteCtrl->SetFocusState(m_nID, GetMouseClicked());	

	CDialog::OnLButtonUp(nFlags, point);
}

void CESMUtilRemoteFrame::OnLButtonDblClk(UINT nFlags, CPoint point)
{
#ifndef REVISION
	if(GetFullScreen() == TRUE)
	{
		m_pRemoteCtrl->SetWindowState(m_nID,FALSE);
		SetFullScreen(FALSE);
	}
	else
	{
		m_pRemoteCtrl->SetWindowState(m_nID,TRUE);

		/*CRect rc;
		GetDlgItem(IDD_UTIL_REMOTEFRAME)->GetClientRect(rc);*/
		SetFullScreen(TRUE);
	}
	/*if(GetFullScreen())
	{
	m_pRemoteCtrl->SetWindowState(m_nID,TRUE);
	SetFullScreen(FALSE);
	}
	else
	{
	m_pRemoteCtrl->SetWindowState(m_nID,FALSE);
	SetFullScreen(TRUE);
	}*/

	//if(m_bFullScreen)
	//{
	//	GetDlgItem(IDC_REMOTE_FRAME_IMAGE)->MoveWindow(0,0,m_szOriScreen.width,m_szOriScreen.height);

	//	if(GetPlayState() == FALSE)
	//	{
	//		Mat Img = m_pArrImageMgr->at(GetCurFrameIdx()).Image;
	//		Mat re;
	//		resize(Img,re,m_szOriScreen,0,0,1);

	//		m_Image.SetDC(m_szOriScreen.width,m_szOriScreen.height,FALSE);
	//		m_Image.SetImageBuffer(re.data,m_szOriScreen.width,m_szOriScreen.height,3);
	//		m_Image.Redraw();
	//	}

	//	m_pRemoteCtrl->SetWindowState(m_nID,FALSE);
	//	//m_pRemoteCtrl->
	//	
	//	SetFullScreen(FALSE);
	//}
	//else
	//{
	//	MoveWindow(0,0,m_szFullScreen.width,m_szFullScreen.height);
	//	GetDlgItem(IDC_REMOTE_FRAME_IMAGE)->MoveWindow(0,0,m_szFullScreen.width,m_szFullScreen.height);
	//	
	//	if(GetPlayState() == FALSE)
	//	{
	//		Mat Img = m_pArrImageMgr->at(GetCurFrameIdx()).Image;
	//		Mat re;
	//		resize(Img,re,m_szFullScreen,0,0,1);

	//		m_Image.SetDC(m_szFullScreen.width,m_szFullScreen.height,TRUE);
	//		m_Image.SetImageBuffer(re.data,m_szFullScreen.width,m_szFullScreen.height,3);
	//		m_Image.Redraw();
	//	}

	//	m_pRemoteCtrl->SetWindowState(m_nID,TRUE);
	//	
	//	SetFullScreen(TRUE);
	//}
#endif
	CDialog::OnLButtonDblClk(nFlags, point);
}

BOOL CESMUtilRemoteFrame::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}

		switch(pMsg->wParam)
		{
		case '1':		case '2':		case '9':
		case VK_NUMPAD1:case VK_NUMPAD2:case VK_NUMPAD3:
		case VK_NUMPAD4:case VK_NUMPAD5:case VK_NUMPAD6:
		case VK_NUMPAD7:case VK_NUMPAD8:case VK_NUMPAD9:
		case VK_F1:	case VK_F2:	case VK_F3:	case VK_F4:	case VK_F5:
		case VK_F6: case VK_F7: case VK_F8: case VK_F9: case VK_F10:
		case VK_RETURN: case 'Z':/* case VK_DIVIDE:*/
			{
				if(pMsg->wParam == VK_DIVIDE)
					pMsg->wParam = 'Z';

				m_pRemoteCtrl->SendEventMsg(m_nID,pMsg->wParam);
				return TRUE;
			}
			break;
		case VK_VOLUME_MUTE:
			{
				//Pause
				if(GetPlayRunning() == FALSE)
					break;

				if(GetPlayState() == TRUE)
				{
					m_pRemoteCtrl->SetPlayState(m_nID,FALSE);
					SetPlayState(FALSE);
				}
				else
				{
					m_pRemoteCtrl->SetPlayState(m_nID,TRUE);
					SetPlayState(TRUE);
				}
			}
			break;
		case VK_VOLUME_DOWN:
			{
				//Prev frame
				SetPlayState(FALSE);
				m_nSearchIdx = -2;
			}
			break;
		case VK_VOLUME_UP:
			{
				//Next frame
				SetPlayState(FALSE);
				m_nSearchIdx = 2;
			}
			break;
		case 'C':
			{
				if(GetSaveIndexSize() == 0)
					break;

				m_nCalcPoint--;
				if(m_nCalcPoint < 0)
					m_nCalcPoint = 0;

				if(GetShowFrame() == FALSE)
				{
					SetShowFrame(TRUE);
					m_pRemoteCtrl->ShowScreenFromUser(m_nID);
				}
#ifndef FRAMEVIEW
				SetSelectorNum(m_nCalcPoint);
				SetSelectLoad(TRUE);
#else
				int nTime = m_stArrSaveIdx.at(m_nCalcPoint).nTime;
				
				m_pRemoteCtrl->AllFrameStop(m_nID,m_nCalcPoint);
				m_pRemoteCtrl->ShowRedSpotPoint(pMsg->wParam,m_nID,m_nCalcPoint,nTime);
#endif
			}
			break;
		case 'V':
			{
				if(GetSaveIndexSize() == 0)
					break;

				m_nCalcPoint++;
				if(m_nCalcPoint > GetSaveIndexSize() -1)
					m_nCalcPoint = GetSaveIndexSize() - 1;

				if(GetShowFrame() == FALSE)
				{
					SetShowFrame(TRUE);
					m_pRemoteCtrl->ShowScreenFromUser(m_nID);
				}
#ifndef FRAMEVIEW
				SetSelectorNum(m_nCalcPoint);
				SetSelectLoad(TRUE);
#else
				int nTime = m_stArrSaveIdx.at(m_nCalcPoint).nTime;

				m_pRemoteCtrl->AllFrameStop(m_nID,m_nCalcPoint);
				m_pRemoteCtrl->ShowRedSpotPoint(pMsg->wParam,m_nID,m_nCalcPoint,nTime);
#endif
			}
			break;
		case 'X':
			{
				if(GetSaveIndexSize() == 0)
					break;

				m_nCalcPoint = GetSaveIndexSize() - 1;
				
				if(GetShowFrame() == FALSE)
				{
					SetShowFrame(TRUE);
					m_pRemoteCtrl->ShowScreenFromUser(m_nID);
				}
#ifndef FRAMEVIEW
				SetSelectorNum(m_nCalcPoint);
				SetSelectLoad(TRUE);
#else
				int nTime = m_stArrSaveIdx.at(m_nCalcPoint).nTime;

				m_pRemoteCtrl->AllFrameStop(m_nID,m_nCalcPoint);
				m_pRemoteCtrl->ShowRedSpotPoint(pMsg->wParam,m_nID,m_nCalcPoint,nTime);
#endif
			}
			break;
#ifdef FRAMEVIEW
		case VK_LEFT:
			{
				if(GetMouseClicked())
				{
					SetPlayState(FALSE);
					m_nSearchIdx = -2;
				}
			}
			break;
		case VK_RIGHT:
			{
				if(GetMouseClicked())
				{
					SetPlayState(FALSE);
					m_nSearchIdx = +2;
				}
			}
			break;
		case VK_NUMPAD0:
			{
				if(GetPlayRunning() == FALSE)
					return FALSE;


				if(GetPlayState() == TRUE)
				{
					m_pRemoteCtrl->SetPlayState(m_nID,FALSE);
					SetPlayState(FALSE);
				}
				else
				{
					m_pRemoteCtrl->SetPlayState(m_nID,TRUE);
					SetPlayState(TRUE);
				}
			}
			break;
		case VK_NEXT:case VK_DOWN:
			{
				//ESMLog(5,_T("[%d] - Change Next Item"),m_nID);
				m_pRemoteCtrl->ChangeFocus(m_nID,TRUE);
				//Invalidate();
			}
			break;
		case VK_PRIOR:case VK_UP:
			{
				//ESMLog(5,_T("[%d] - Change Prior Item"),m_nID);
				m_pRemoteCtrl->ChangeFocus(m_nID,FALSE);
				//Invalidate();
			}
			break;
#endif
		case VK_ADD:
			{
				if(GetMouseClicked())
				{
					//줌 비율 증가, +5, 250%까지
					m_nZoomRatio += 5;
					if(m_nZoomRatio >= 400)
						m_nZoomRatio = 400;

					if(GetPlayState() == FALSE)
						ShowScreenIfPauseState();
				}		
			}
			break;
		case VK_SUBTRACT:
			{
				if(GetMouseClicked())
				{
					//줌 비율 감소, -5, 100%까지
					m_nZoomRatio -= 5;
					if(m_nZoomRatio <= 100)
						m_nZoomRatio = 100;

					if(GetPlayState() == FALSE)
						ShowScreenIfPauseState();
				}
			}
			break;
		case VK_MULTIPLY:
			{
				if(GetMouseClicked())
					m_nZoomRatio = 100;

				if(GetPlayState() == FALSE)
					ShowScreenIfPauseState();
			}
			break;
		}
	}


	return CDialog::PreTranslateMessage(pMsg);
}

void CESMUtilRemoteFrame::OnMouseLeave()
{
	CDialog::OnMouseLeave();
}

void CESMUtilRemoteFrame::OnMouseMove(UINT nFlags, CPoint point)
{
	if(point.x > m_rcImage.left && point.x < m_rcImage.right
		&& point.y > m_rcImage.top && point.y < m_rcImage.bottom && GetIsFocus())
	{
		SetMouseOver(TRUE);
		if(GetPlayState() == FALSE && m_bIsPressedRButton == TRUE)
		{
			if(!(GetAsyncKeyState(VK_LSHIFT) & 0x8000))
			{
				point.x = fixXValueWhenShiftKeyIsPressed;
			}

			if(GetMouseOver())
			{
				m_ptImage.x = point.x - m_rcImage.left;
				m_ptImage.y = point.y - m_rcImage.top;
			}

			if(m_pRemoteCtrl->m_pTemplateMgr == NULL)
				return;

//			EnterCriticalSection(&m_criOnSize);
			CPoint pImageSize = GetImageFrameSize();
			track_info pTrackInfo = ESMGetTrackInfo();

			cv::Point2f ptImage;
			ptImage.x = m_ptImage.x *= (double)pTrackInfo.nWidth / pImageSize.x;
			ptImage.y = m_ptImage.y *= (double)pTrackInfo.nHeight / pImageSize.y;

			double dbZoomRatio = (double)m_nZoomRatio / 100.0;
			if(dbZoomRatio < 1.0)
				dbZoomRatio = 1.0;

			int nLeft = 0,nTop = 0,nReWidth = 0,nReHeight = 0;

			GetImageRect(dbZoomRatio,pTrackInfo.nWidth,pTrackInfo.nHeight,nLeft,nTop,nReWidth,nReHeight);

			ptImage.x = (ptImage.x / dbZoomRatio) + nLeft;
			ptImage.y = (ptImage.y / dbZoomRatio) + nTop;

			//ESMLog(5, _T("Template Point : (%.3f, %.3f)"), ptImage.x, ptImage.y);
//			LeaveCriticalSection(&m_criOnSize);
			m_pRemoteCtrl->m_pTemplateMgr->ClearArrTemplatePoint();	
			m_pRemoteCtrl->m_pTemplateMgr->SetCenterPoint(ptImage);
			
			ShowScreenIfPauseState();
		}
	}
	else
	{
		SetMouseOver(FALSE);
	}
	CDialog::OnMouseMove(nFlags, point);
}

int CESMUtilRemoteFrame::ReadFrameCount(CString strPath)
{
	FILE *fp;	

	char filename[2047];
	//WideCharToMultiByte(CP_ACP, 0, strOutput.GetBuffer(), -1, filename, 255, NULL, NULL);
	sprintf(filename,"%S",strPath);

	fp = fopen(filename, "r");

	fseek( fp,  -2047, SEEK_END);
	long length = ftell(fp);

	char fileData[2047];
	fgets(fileData, 2047, fp);

	int nFrameCnt = atoi(fileData);

	fclose(fp);

	return nFrameCnt;
}


void CESMUtilRemoteFrame::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	BOOL bRecordProcess = TRUE;

	if(m_pRemoteCtrl != NULL)
		bRecordProcess = m_pRemoteCtrl->GetRecordProcess();

	if(m_Image)
	{
		if(GetPlayState() == FALSE || bRecordProcess == FALSE)
		{
			CRect rc;
			GetClientRect(&rc);
			CalcMovieRatio(rc);

			int nCurMovieIdx = GetCurPlayIdx();
			int nCurFrameIdx = GetCurFrameIdx();

			if(nCurMovieIdx < 0)
				return;

			EnterCriticalSection(&m_criImageLoad);
			ImageLiveMgr stLiveMgr = m_ArrLiveMgr.at(nCurMovieIdx);
			LeaveCriticalSection(&m_criImageLoad);

			Mat Img = stLiveMgr.pImageLiveArr->at(nCurFrameIdx).img;

			ShowScreen(Img);
			/*int nCurMovieIdx = GetCurPlayIdx();
			int nCurFrameIdx = GetCurFrameIdx();

			ImageLiveMgr stLiveMgr = m_ArrLiveMgr.at(nCurMovieIdx);

			Mat Img = stLiveMgr.pImageLiveArr->at(nCurFrameIdx).img;
			Mat re;
			resize(Img,re,cv::Size(cx,cy),0,0,1);
			ShowScreen(Img);

			GetDlgItem(IDC_REMOTE_FRAME_IMAGE)->MoveWindow(0,0,cx,cy);*/

			//m_Image.SetDC(m_szFullScreen.width,m_szFullScreen.height,FALSE);
			//m_Image.SetImageBuffer(re.data,m_szFullScreen.width,m_szFullScreen.height,3);
			//m_Image.Redraw();
		}
		if(GetPlayState() == TRUE)
		{
			if(GetFullScreen() == TRUE)
			{
				CRect rc;
				GetClientRect(&rc);
				CalcMovieRatio(rc);
			}
			else
			{
				CRect rc;
				GetClientRect(&rc);
				CalcMovieRatio(rc);
			}
		}
	}
}
void CESMUtilRemoteFrame::DeleteAllLiveMgr()
{
	int nCount = m_ArrLiveMgr.size();
	if(nCount)
	{
		for(int i = 0 ; i < nCount; i++)
		{
			DeleteLiveMgr(i,TRUE);
		}
	}
	m_ArrLiveMgr.clear();
}
void CESMUtilRemoteFrame::DeleteLiveMgr(int nIdx,BOOL bAll/* = FALSE*/)
{
	if(nIdx >= GetLiveMgrSize())
		return;

	BOOL bNonDelete = FALSE;

	if(bAll == FALSE)
	{
		if(GetSaveIndexSize() > 0)
		{
			for(int i = 0 ; i < GetSaveIndexSize(); i++)
			{
				ImageSaveIndex stSaveIdx = m_stArrSaveIdx.at(i);
				int nMovIdx = stSaveIdx.nMovieIdx;

				for(int j = nIdx - 2; j <= nIdx + 2; j++)
				{			
					if(j == nMovIdx)
					{
						bNonDelete = TRUE;
						break;
					}
				}
				if(bNonDelete == TRUE)
					break;
			}
			if(bNonDelete == TRUE)
			{
				//ESMLog(5,_T("[RemoteFrame] - Non Delete - %d "),nIdx);
				return;
			}
		}
	}

	EnterCriticalSection(&m_criImageLoad);
	ImageLiveMgr* imgArr = &(m_ArrLiveMgr.at(nIdx));
	LeaveCriticalSection(&m_criImageLoad);

	BOOL bFinish = imgArr->bDecodeFinish;
	BOOL bDelete = imgArr->bDelete;


	if(bFinish == TRUE && bDelete == FALSE)
	{
		if(imgArr->pImageLiveArr != NULL)
		{
			//수정해야됨
			imgArr->pImageLiveArr->clear();
			//delete imgArr->pImageLiveArr;
			//imgArr->pImageLiveArr = NULL;
			//imgArr.pImageLiveArr->clear();
		}
		imgArr->bDelete = TRUE;
	}
	SetLastDeleteIdx(nIdx);
	//m_nLastDelete = nIdx;
}

BOOL CESMUtilRemoteFrame::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
#ifndef FRAMEVIEW
	if(zDelta > 0)
	{
		m_nZoomRatio += 5;
		if(m_nZoomRatio >= 400)
			m_nZoomRatio = 400;
	}
	else
	{
		m_nZoomRatio -= 5;
		if(m_nZoomRatio <= 100)
			m_nZoomRatio = 100;
	}
	if(GetPlayState() == FALSE)
	{
		ShowScreenIfPauseState();
	}
#else
	if( zDelta < 0 )
	{
		SetPlayState(FALSE);
		m_nSearchIdx = -2;
	}
	else if(zDelta > 0)
	{
		SetPlayState(FALSE);
		m_nSearchIdx = 2;
	}
#endif
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}


void CESMUtilRemoteFrame::OnMButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
#ifndef FRAMEVIEW
	if(GetPlayRunning() == FALSE)
		return;


	if(GetPlayState() == TRUE)
	{
		m_pRemoteCtrl->SetPlayState(m_nID,FALSE);
		SetPlayState(FALSE);
	}
	else
	{
		m_pRemoteCtrl->SetPlayState(m_nID,TRUE);
		SetPlayState(TRUE);
	}
#endif
	CDialog::OnMButtonUp(nFlags, point);
}

void CESMUtilRemoteFrame::SetMakingTime(int nMovieIdx,int nFrameIdx)
{
	m_nMakingTime = ESMGetTimeFromIndex(nMovieIdx,nFrameIdx);
	//ESMLog(5,_T("[%d]Setted Making Time: %d"),m_nID,m_nMakingTime);
	//m_nMakingTime = ((movie_frame_per_second-1) * nMovieIdx * nFrameIdx) * movie_next_frame_time;
}
int CESMUtilRemoteFrame::GetMakingTime()
{
	if(m_nMakingTime < 0)
		return -1;
	else
		return m_nMakingTime;
}
int CESMUtilRemoteFrame::GetCurFrameTime(int nMovieIdx,int nFrameIdx)
{

	return 0;
}
void CESMUtilRemoteFrame::SetAdjustInfo(CString strDSC)
{
	m_strDSCID = strDSC;

	int nSrcWidth = 640;
	int nSrcHeight = 360;
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();

	m_stAdjust = pMovieMgr->GetAdjustData(strDSC);

	m_nSrcWidth = m_stAdjust.nWidth;
	m_nSrcHeight = m_stAdjust.nHeight;

	//m_dbRatioW = (double)nSrcWidth / (double)m_nSrcWidth;
	//m_dbRatioH = (double)nSrcHeight  / (double)m_nSrcHeight;

	m_dbRatio = (double)nSrcWidth / (double)m_nSrcWidth;

	m_nMarginX = (double)pMovieMgr->GetMarginX() * m_dbRatio;
	m_nMarginY = (double)pMovieMgr->GetMarginY() * m_dbRatio;

	//m_dbMarScaleW = 1 / ( (double)(nSrcWidth - m_nMarginX * 2) /(double) nSrcWidth);
	//m_dbMarScaleH = 1 / ( (double)(nSrcHeight - m_nMarginY * 2) /(double) nSrcHeight);
	m_dbMarginScale = 1 / ( (double)(nSrcWidth - m_nMarginX * 2) /(double) nSrcWidth);

	m_nLeft = (int)((nSrcWidth *m_dbMarginScale - nSrcWidth )/2);
	m_nTop  = (int)((nSrcHeight*m_dbMarginScale - nSrcHeight)/2);
	
	if(m_stAdjust.AdjAngle != 0.0)
		m_bAdjustLoad = TRUE;

	//ESMLog(m_bAdjustLoad,_T("[%s][%d] SetAdjustData Finish! - %.2f(%.2f,%.2f)"),strDSC,m_nID,
	//	m_stAdjust.AdjAngle,m_stAdjust.AdjptRotate.x,m_stAdjust.AdjptRotate.y);
}
Mat CESMUtilRemoteFrame::CreateAdjustImage(Mat img,int nSrcWidth,int nSrcHeight,int nDlgWidth,int nDlgHeight,BOOL bReverse)
{
	//Mat tmp;
	//resize(img,tmp,cv::Size(nDlgWidth,nDlgHeight),0,0,cv::INTER_LINEAR);

	//double dRatioW = nDlgWidth  / nSrcWidth;
	//double dRatioH = nDlgHeight / nSrcHeight;

	//if(nSrcWidth != nDlgWidth)
	//	dRatioW = (double)nDlgWidth / (double)nSrcWidth;

	//if(nSrcHeight != nDlgHeight)
	//	dRatioH = (double)nDlgHeight / (double)nSrcHeight;

	//double degree = m_stAdjust.AdjAngle;
	//double dbAngleAdjust = -1*(degree+90);
	//double dbScale = m_stAdjust.AdjSize;

	//Mat tmp;
	//resize(img,tmp,cv::Size(nDlgWidth,nDlgHeight),0,0,cv::INTER_LINEAR);

	//double dbRotX = Round(m_stAdjust.AdjptRotate.x * dRatioW);
	//double dbRotY = Round(m_stAdjust.AdjptRotate.y * dRatioH);
	//double dbMovX = Round(m_stAdjust.AdjMove.x * dRatioW);
	//double dbMovY = Round(m_stAdjust.AdjMove.y * dRatioH);

	//int nMarginX = m_nMarginX * dRatioW;
	//int nMarginY = m_nMarginY * dRatioH;

	//Mat RotMat(2,3,CV_64FC1);
	//RotMat = getRotationMatrix2D(Point2f(dbRotX,dbRotY),dbAngleAdjust,dbScale);

	//RotMat.at<double>(0,2) += (dbMovX / dbScale);
	//RotMat.at<double>(1,2) += (dbMovY / dbScale);

	//double dbMarginScaleW = 1 / ((double)(nDlgWidth - nMarginX * 2)/ nDlgWidth);
	//double dbMarginScaleH = 1 / ((double)(nDlgHeight - nMarginY * 2)/ nDlgHeight);


	///*int nNewW = ((nDlgWidth  * dbMarginScale - nDlgWidth)/2);
	//int nNewH = ((nDlgHeight * dbMarginScale - nDlgHeight)/2);*/

	//int nRcX = ((nDlgWidth  * dbMarginScaleW - nDlgWidth)/2);
	//int nRcY = ((nDlgHeight * dbMarginScaleH - nDlgHeight)/2);
	//
	//Mat rot,re;
	//imshow("ori",tmp);
	//warpAffine(tmp,rot,RotMat,tmp.size(),1,0,0);
	//imshow("rot",rot);
	//resize(rot,re,cv::Size(nDlgWidth * dbMarginScaleW,nDlgHeight * dbMarginScaleH)
	//	,0,0,cv::INTER_LINEAR);
	//circle(re,cv::Point(nRcX,nRcY),5,cv::Scalar(0,0,255),3,8);
	//imshow("re",re);
	//img = (re)(cv::Rect(nRcX,nRcY,nDlgWidth,nDlgHeight));
	//imshow("img",img);

	//waitKey(10);
	int nRotateX = 0, nRotateY = 0, nSize = 0;
	double dSize = 0.0;
	int nMoveX = 0, nMoveY = 0;

	//Rotation
	nRotateX = Round(m_stAdjust.AdjptRotate.x  * m_dbRatio );
	nRotateY = Round(m_stAdjust.AdjptRotate.y  * m_dbRatio );
	CpuRotateImage(img, nRotateX ,nRotateY, m_stAdjust.AdjSize, m_stAdjust.AdjAngle,0);

	//Move
	nMoveX = Round(m_stAdjust.AdjMove.x  * m_dbRatio);
	nMoveY = Round(m_stAdjust.AdjMove.y  * m_dbRatio);
	CpuMoveImage(&img, nMoveX,  nMoveY);

	Mat pMatResize;
	resize(img, pMatResize, cv::Size(nSrcWidth *m_dbMarginScale ,nSrcHeight * m_dbMarginScale),
		0.0, 0.0 ,INTER_LINEAR );

	Mat pMatCut =  (pMatResize)(cv::Rect(m_nLeft, m_nTop, nSrcWidth, nSrcHeight));

	if(bReverse)
		flip(pMatCut,pMatCut,-1);

	pMatCut.copyTo(img);

	return pMatCut;
}
int CESMUtilRemoteFrame::Round(double dData)
{
	int nResult = 0;
	if( dData == 0)
		nResult = 0;
	else if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);

	return nResult;
}
void CESMUtilRemoteFrame::CpuMoveImage(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void CESMUtilRemoteFrame::CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle, BOOL bReverse)
{
	Mat Iimage2;// = cvCreateImage(cvGetSize(Iimage), IPL_DEPTH_8U, 3);

	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;
	//cvShowImage("2",Iimage);
	CvMat *rot_mat = cvCreateMat( 2, 3, CV_32FC1);
	CvPoint2D32f rot_center = cvPoint2D32f( nCenterX, nCenterY);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust;// = -1 * (dAngle + 90);
	if(bReverse) //Reverse Movie
		dbAngleAdjust = -1 * (180);   
	else
		dbAngleAdjust = -1 * (dAngle + 90);   //Normal Movie

	//CString strTemp;
	//strTemp.Format(_T("%f"), dbAngleAdjust);
	//m_AdjustDsc2ndList.SetItemText(m_nSelectedNum, 17, strTemp);

	cv2DRotationMatrix( rot_center, dbAngleAdjust, dScale, rot_mat);

	//cuda::warpAffine(*gMat,d_rotate, rot_mat, Size(2500, 1500), cv::INTER_CUBIC+cv::WARP_FILL_OUTLIERS);
	//Mat d_rotate;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- ? CvPoint2D32f srcTri[3], dstTri[3];
	cvWarpAffine(&IplImage(Iimage), &IplImage(Iimage), rot_mat,cv::INTER_LINEAR+cv::WARP_FILL_OUTLIERS);	//d_rotate.copyTo(*gMat);
	//imshow("Iimage",Iimage);
	//cvWaitKey(0);
	//gMat =&(cvarrToMat(Iimage2));//
	//Iimage = cvCreateImage(cvGetSize(&Iimage), IPL_DEPTH_8U, 3);
	cvReleaseMat(&rot_mat);
}
void CESMUtilRemoteFrame::CalcMovieRatio(CRect rc)
{
	//	EnterCriticalSection(&m_criOnSize);

	int nWidth = rc.right - (rc.right%4);
	int nHeight = rc.bottom;

	if(nHeight > nWidth)
	{
		double dbNewWidth = ((double) nHeight / 9.0) * 16.0;
		int nMargin = (nWidth - dbNewWidth)/2;

		if(nMargin < 0)
		{
			double dbNewHeight = ((double) nWidth / 16.0) * 9.0;
			nMargin = (nHeight - dbNewHeight)/2;

			m_nImageWidth = nWidth;
			m_nImageHeight = (int)dbNewHeight;

			m_rcImage.left = 0;
			m_rcImage.top  = nMargin;
			m_rcImage.right = m_nImageWidth;
			m_rcImage.bottom = nHeight - nMargin;

			//ESMLog(5,_T("RESIZE %d - %d"),m_rcImage.Width(),m_rcImage.Height());
		}
		else
		{
			m_nImageWidth = (int)dbNewWidth - ((int)dbNewWidth%4);//(int)dRatio-nMargin;
			m_nImageHeight = nHeight;

			m_rcImage.left = nMargin;
			m_rcImage.top  = 0;
			m_rcImage.right = (int)dbNewWidth - ((int)dbNewWidth%4) + nMargin;
			m_rcImage.bottom = m_nImageHeight;

			GetDlgItem(IDC_REMOTE_FRAME_IMAGE)->MoveWindow(m_rcImage);
		}
	}
	else
	{
		double dbNewHeight = ((double) nWidth / 16.0) * 9.0;
		int nMargin = (nHeight - dbNewHeight)/2;
		if(nMargin < 0)
		{
			double dbNewWidth = ((double) nHeight / 9.0) * 16.0;
			int nMargin = (nWidth - dbNewWidth)/2;

			m_nImageWidth = (int)dbNewWidth  - ((int)dbNewWidth%4);//(int)dRatio-nMargin;
			m_nImageHeight = nHeight;

			m_rcImage.left = nMargin;
			m_rcImage.top  = 0;
			m_rcImage.right = (int)dbNewWidth - ((int)dbNewWidth%4) + nMargin;
			m_rcImage.bottom = m_nImageHeight;

			GetDlgItem(IDC_REMOTE_FRAME_IMAGE)->MoveWindow(m_rcImage);
		}
		else
		{
			m_nImageWidth = nWidth;
			m_nImageHeight = (int)dbNewHeight;

			m_rcImage.left = 0;
			m_rcImage.top  = nMargin;
			m_rcImage.right = m_nImageWidth;
			m_rcImage.bottom = nHeight - nMargin;

			GetDlgItem(IDC_REMOTE_FRAME_IMAGE)->MoveWindow(m_rcImage);
		}
	}

	//ESMLog(5,_T("Width = %d, Height = %d"),m_rcImage.Width(),m_rcImage.Height());
//	LeaveCriticalSection(&m_criOnSize);
}

//wgkim
CPoint CESMUtilRemoteFrame::GetImageFrameSize()
{
	CPoint pPoint;
	CRect rc;
	GetDlgItem(IDC_REMOTE_FRAME_IMAGE)->GetClientRect(&rc);

	pPoint.x = rc.right - rc.left;
	pPoint.y = rc.bottom - rc.top;

	return pPoint;
}

void CESMUtilRemoteFrame::DrawViewWindowAndCenterLine(Mat *img)
{
	//ostringstream oss;
	//oss << "Set Template Point";
	//string text = oss.str();// = "Set Template Point";

	//cv::Point textLocation = cv::Point(10, 200);
	//int fontFace= 4; double fontScale = 1.0;
	//putText(img, text, textLocation, fontFace, fontScale, Scalar::all(255));
	
	int nZoomGuide = m_pRemoteCtrl->GetZoomGuide();

	int lineThickness = 2;
	int centerPointLineLength = 250;
	cv::Point viewPoint;
	cv::Point centerPointOfRotation; 

	CPoint viewImageSize = GetImageFrameSize();
	int width = viewImageSize.x;
	int height = viewImageSize.y;

	if(m_pRemoteCtrl->m_pTemplateMgr == NULL)
	{
		ESMLog(0,_T("Template Point Load Error"));
		return;
	}

	if(m_pRemoteCtrl->m_pTemplateMgr->IsOverBoundary())
	{	
		viewPoint = m_ptImage;
		centerPointOfRotation= m_ptImageCenter;
	}
	else
	{
		if(!m_pRemoteCtrl->m_pTemplateMgr->IsArrTemplatePointValid()/* || m_bIsPressedRButton*/)
		{
			viewPoint = m_ptImageCenter;
			centerPointOfRotation= m_ptImage;

			if(m_bIsPressedRButton == TRUE)
			{				
				double dbZoomRatio = m_nZoomRatio/100.;
				if(dbZoomRatio < 1.0)
					dbZoomRatio = 1.0;

				viewPoint.x = (viewPoint.x ) * (width/m_pRemoteCtrl->m_pTemplateMgr->m_dWidth);
				viewPoint.y = (viewPoint.y ) * (height/m_pRemoteCtrl->m_pTemplateMgr->m_dHeight);

				centerPointOfRotation.x = (centerPointOfRotation.x ) * (width/m_pRemoteCtrl->m_pTemplateMgr->m_dWidth);
				centerPointOfRotation.y = (centerPointOfRotation.y ) * (height/m_pRemoteCtrl->m_pTemplateMgr->m_dHeight);

				int nLeft = 0,nTop = 0,nReWidth = 0,nReHeight = 0;

				GetImageRect(dbZoomRatio,width,height,nLeft,nTop,nReWidth,nReHeight);

				viewPoint.x = (viewPoint.x / dbZoomRatio) + nLeft;
				viewPoint.y = (viewPoint.y / dbZoomRatio) + nTop;

				/*centerPointOfRotation.x = (centerPointOfRotation.x / dbZoomRatio) + nLeft;
				centerPointOfRotation.y = (centerPointOfRotation.y / dbZoomRatio) + nTop;*/
			}
		}
		else
		{
			CString strDsc = m_pRemoteCtrl->m_strArrDSCID[m_nID];
			int dscIndex = m_pRemoteCtrl->m_pTemplateMgr->GetDscIndexFrom(strDsc);			

			viewPoint = m_pRemoteCtrl->m_pTemplateMgr->GetTemplatePoint(dscIndex).viewPoint ;					
			centerPointOfRotation= m_pRemoteCtrl->m_pTemplateMgr->GetTemplatePoint(dscIndex).centerPointOfRotation;

			viewPoint.x = (viewPoint.x ) * (width/m_pRemoteCtrl->m_pTemplateMgr->m_dWidth);
			viewPoint.y = (viewPoint.y ) * (height/m_pRemoteCtrl->m_pTemplateMgr->m_dHeight);			

			centerPointOfRotation.x = (centerPointOfRotation.x ) * (width/m_pRemoteCtrl->m_pTemplateMgr->m_dWidth);
			centerPointOfRotation.y = (centerPointOfRotation.y ) * (height/m_pRemoteCtrl->m_pTemplateMgr->m_dHeight);
			
			if(GetAsyncKeyState(VK_RBUTTON))
				viewPoint = m_ptImageCenter;

			if(m_bIsPressedRButton == TRUE)
			{
				double dbZoomRatio = m_nZoomRatio/100.;
				if(dbZoomRatio < 1.0)
					dbZoomRatio = 1.0;

				int nLeft = 0,nTop = 0,nReWidth = 0,nReHeight = 0;
				GetImageRect(dbZoomRatio,width,height,nLeft,nTop,nReWidth,nReHeight);

				viewPoint.x = (viewPoint.x / dbZoomRatio) + nLeft;
				viewPoint.y = (viewPoint.y / dbZoomRatio) + nTop;

				//centerPointOfRotation.x = (centerPointOfRotation.x / dbZoomRatio) + nLeft;
				//centerPointOfRotation.y = (centerPointOfRotation.y / dbZoomRatio) + nTop;
			}
		}		
	}

	//if(clickFlag == true)
	//	DrawAreaWhereCoordinateMove(img);

	double zoomRatio = (double)nZoomGuide/100.;//3.0;//m_dZoomValue/100.;

	line(*img, cv::Point(viewPoint.x-10, viewPoint.y), cv::Point(viewPoint.x+10, viewPoint.y), Scalar(128, 128, 255), lineThickness, 8, 0);
	line(*img, cv::Point(viewPoint.x, viewPoint.y-10), cv::Point(viewPoint.x, viewPoint.y+10), Scalar(128, 128, 255), lineThickness, 8, 0);	

	line(*img, cv::Point(viewPoint.x - ((width/zoomRatio)/2), viewPoint.y - ((height/zoomRatio)/2)), 
		cv::Point(viewPoint.x + ((width/zoomRatio)/2), viewPoint.y - ((height/zoomRatio)/2)), Scalar(128, 128, 255), lineThickness, 8, 0);
	line(*img, cv::Point(viewPoint.x - ((width/zoomRatio)/2), viewPoint.y + ((height/zoomRatio)/2)), 
		cv::Point(viewPoint.x + ((width/zoomRatio)/2), viewPoint.y + ((height/zoomRatio)/2)), Scalar(128, 128, 255), lineThickness, 8, 0);
	line(*img, cv::Point(viewPoint.x - ((width/zoomRatio)/2), viewPoint.y - ((height/zoomRatio)/2)), 
		cv::Point(viewPoint.x - ((width/zoomRatio)/2), viewPoint.y + ((height/zoomRatio)/2)), Scalar(128, 128, 255), lineThickness, 8, 0);
	line(*img, cv::Point(viewPoint.x + ((width/zoomRatio)/2), viewPoint.y - ((height/zoomRatio)/2)), 
		cv::Point(viewPoint.x + ((width/zoomRatio)/2), viewPoint.y + ((height/zoomRatio)/2)), Scalar(128, 128, 255), lineThickness, 8, 0);

	line(*img, cv::Point(centerPointOfRotation.x, centerPointOfRotation.y-(centerPointLineLength/2)), centerPointOfRotation, Scalar(255, 128, 128), lineThickness, 8, 0);
	LineIterator it(*img, centerPointOfRotation, cv::Point(centerPointOfRotation.x, centerPointOfRotation.y+(centerPointLineLength/2)), 8);		
	for(int i = 0; i < it.count; i++,it++)
		if ( i%5!=0 && (i+1)%5!=0) {(*it)[0]/*B*/ = 255;(*it)[1]/*G*/ = 128;(*it)[2]/*R*/ = 128;} 
		circle(*img, centerPointOfRotation, 1, Scalar(255, 0, 0), 2, 8);
		ellipse(*img, centerPointOfRotation, cvSize(20, 10), 0, 0, 360, Scalar(255, 128, 128), lineThickness);
		/*if(m_bIsPressedRButton)
			imshow("*img",*img);

		waitKey(10);*/
}

void CESMUtilRemoteFrame::ShowScreenIfPauseState()
{
//	EnterCriticalSection(&m_criOnSize);
	//GetCurPlayIdx();
	int nCurMovieIdx = GetCurPlayIdx();
	int nCurFrameIdx = GetCurFrameIdx();
	TRACE(_T("ShowScreenIfPauseState - 1\n"));
	if(nCurFrameIdx == -1 || nCurMovieIdx == -1)
		return;
	TRACE(_T("ShowScreenIfPauseState - 2\n"));
	ImageLiveMgr stLiveMgr = m_ArrLiveMgr.at(nCurMovieIdx);

	if(stLiveMgr.bDelete == TRUE)
	{
		ShowScreen(m_matImage);
//		LeaveCriticalSection(&m_criOnSize);
		return;
	}
	TRACE(_T("ShowScreenIfPauseState - 3\n"));
	Mat pImage = LoadImageFrame(nCurMovieIdx,nCurFrameIdx);
	
//	LeaveCriticalSection(&m_criOnSize);
	ShowScreen(pImage);
}
void CESMUtilRemoteFrame::SetItAsFocus()
{
	HWND hWND = GetSafeHwnd();
	::SetFocus(hWND);
}