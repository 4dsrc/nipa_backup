////////////////////////////////////////////////////////////////////////////////
//
//	MainFrmMsg.cpp : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-06-21
// @ver.1.0	4DMaker
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "4DMaker.h"
#include "MainFrm.h"
#include "DSCMgr.h"
#include "ESMUtil.h"
#include "SdiSingleMgr.h"
#include "ESMObjectFrame.h"
#include "ESMFileOperation.h"
#include "ESMAdjustMgrDlg.h"
#include "DSCFrameSelector.h"
#include "ESMMuxBackupDlg.h"
#include <random>
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

typedef struct TRANSCODE_INFO
{
	void *pMain;
	CString strPath;
}_TRANSCODE_INFO;

//------------------------------------------------------------------------------
//--
//--
//-- TOTAL MESSAGE CENTER
//-- 
//--
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------ 
//! @brief		OnSDIMsg
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------
LRESULT CMainFrame::OnSDIMsg(WPARAM wParam, LPARAM lParam)
{
	if(m_bClose)
		return ESM_ERR;

	ESMEvent* pMsg = NULL;
	pMsg = (ESMEvent*)lParam;

	
	switch(pMsg->message)
	{
	case WM_SDI_RESULT_GET_PROPERTY_DESC:
		//-- 2013-05-04 hongsu@esmlab.com
		//-- Send Message To Property 
		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
		{
			m_wndDSCViewer.SetPropertyListView((ESMEvent*)pMsg);
			m_wndProperty.m_pPropertyCtrl->LoadPropertyInfo((ESMEvent*)pMsg);
			CSdiSingleMgr* pSingMgr = (CSdiSingleMgr* )pMsg->pDest;
			CDSCItem* pDscItem = (CDSCItem*)pSingMgr->GetParent();

			IDeviceInfo *pDeviceInfo = (IDeviceInfo *)pMsg->pParam;
			if( pDeviceInfo != NULL)
			{
				int nType = pDeviceInfo->GetPropCode();
				CString strValue = m_wndProperty.m_pPropertyCtrl->GetPropertyValueStr(nType, pDeviceInfo->GetCurrentEnum());
				pDscItem->SetDSCCaptureInfo(nType, strValue);

				if (nType == PTP_DPC_SAMSUNG_FW_VERSION)
				{
					int nVer = pDeviceInfo->GetCurrentEnum();
					pDscItem->SetCamVersion(nVer);
				}
				
				// To Print Log
				TRACE(_T("[NET] Send Property to Server = [Code:%x] [Value:%x]\n"),pDeviceInfo->GetPropCode(), pDeviceInfo->GetCurrentEnum());		

				ESMEvent* pMsgNet = new ESMEvent();
				memcpy(pMsgNet, pMsg, sizeof(ESMEvent));
				pMsgNet->message	= WM_ESM_LIST_PROPERTY_RESULT;
				pMsgNet->pDest		= pMsgNet->pParent;
				OnESMMsg((WPARAM)WM_ESM_NET, (LPARAM)pMsgNet);
			}

			if(pMsg)
			{
				delete pMsg;
				pMsg = NULL;
			}
		}else
		{
			m_wndDSCViewer.SetPropertyListView((ESMEvent*)pMsg);
			m_wndProperty.m_pPropertyCtrl->LoadPropertyInfo((ESMEvent*)pMsg);
			CSdiSingleMgr* pSingMgr = (CSdiSingleMgr* )pMsg->pDest;
			CDSCItem* pDscItem = (CDSCItem*)pSingMgr->GetParent();
			
			IDeviceInfo *pDeviceInfo = (IDeviceInfo *)pMsg->pParam;
			if( pDeviceInfo != NULL)
			{
				int nType = pDeviceInfo->GetPropCode();
				CString strValue = m_wndProperty.m_pPropertyCtrl->GetPropertyValueStr(nType, pDeviceInfo->GetCurrentEnum());
				pDscItem->SetDSCCaptureInfo(nType, strValue);

				if (nType == PTP_DPC_SAMSUNG_FW_VERSION)
				{
					int nVer = pDeviceInfo->GetCurrentEnum();
					pDscItem->SetCamVersion(nVer);
				}
			}

			if(pMsg)
			{
				delete pMsg;
				pMsg = NULL;
			}
		}
		break;
 	case WM_SDI_RESULT_SET_PROPERTY_VALUE:
		{
			int nType = (int)pMsg->pParam;;
			if (nType == PTP_OC_SAMSUNG_SetFocusPosition)	//GH5
			{
				m_wndProperty.ResultFocusMoveCount();
			}

			if(pMsg)
			{
				delete pMsg;
				pMsg = NULL;
			}
		}
		break;

	case WM_SDI_RESULT_FOCUS:
		//-- 2013-05-04 hongsu@esmlab.com
		//-- Send Message To Property 
		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
		{
			pMsg->message	= WM_ESM_LIST_GETFOCUS_TOSERVER;
			pMsg->pDest		= pMsg->pParent;
			//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
			pMsg->nParam3   = pMsg->nParam3;
			OnESMMsg((WPARAM)WM_ESM_NET, (LPARAM)pMsg);
			TRACE(_T("WM_SDI_RESULT_GET_FOCUS"));
		}
		else
		{
			CSdiSingleMgr* pSingMgr = (CSdiSingleMgr* )pMsg->pDest;
			CDSCItem* pDscItem = (CDSCItem*)pSingMgr->GetParent();
			ESMEvent* pSendMsg = NULL;
			pSendMsg = new ESMEvent;
			pSendMsg->message = WM_RS_RC_GET_FOCUS;
			CString* strId;
			strId = new CString;
			*strId = pDscItem->GetDeviceDSCID();
			pSendMsg->pDest = (LPARAM)strId;
			FocusPosition* pData = new FocusPosition;
			memcpy(pData, (char* )pMsg->pParam, sizeof(FocusPosition));
			pSendMsg->pParam = (LPARAM)pData;
 			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_DSC, (LPARAM)pSendMsg);

// 			m_wndDSCViewer.SetPropertyListView((ESMEvent*)pMsg);
// 			m_wndProperty.m_pPropertyCtrl->LoadPropertyInfo((ESMEvent*)pMsg);
// 			CSdiSingleMgr* pSingMgr = (CSdiSingleMgr* )pMsg->pDest;
// 			CDSCItem* pDscItem = (CDSCItem*)pSingMgr->GetParent();
// 
// 			IDeviceInfo *pDeviceInfo = (IDeviceInfo *)pMsg->pParam;
// 			int nType = pDeviceInfo->GetPropCode();
// 			CString strValue = m_wndProperty.m_pPropertyCtrl->GetPropertyValueStr(nType, pDeviceInfo->GetCurrentEnum());
// 			pDscItem->SetDSCCaptureInfo(nType, strValue);

			if(pMsg)
			{
				delete pMsg;
				pMsg = NULL;
			}
		}
		break;
	case WM_SDI_RESULT_GET_FOCUS_FRAME:
		{
			if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
			{
				CSdiSingleMgr* pSingMgr = (CSdiSingleMgr* )pMsg->pDest;
				CDSCItem* pDscItem = (CDSCItem*)pSingMgr->GetParent();

				int nResult = (int)pMsg->nParam1;

				SdiUIntArray* pArList = (SdiUIntArray*)pMsg->pParam;

				UINT32 nX = pArList->GetAt(0);
				UINT32 nY = pArList->GetAt(1);
				UINT32 nW = pArList->GetAt(2);
				UINT32 nH = pArList->GetAt(3);
				UINT32 nMag = pArList->GetAt(4);

#if 0
				if (nResult == SDI_ERR_OK)
					nResult = 0x2001; //PTP_RC_OK

				ESMLog(5, _T("[%s] [__RESULT GET FOCUS FRAME] PTP_OC_CUSTOM_GetFocusFrame result=0x%X,X:%d,Y:%d,W:%d,H:%d,Mag:%d"), pDscItem->GetDeviceDSCID(), nResult, nX, nY, nW, nH, nMag);
#endif

				if (nResult == SDI_ERR_OK || nResult == 0x2001)
				{
					int nValue1 = (nX * 10000) + nY;
					int nValue2 = (nW * 10000) + nH;
					int nValue3 = nMag;

					ESMEvent* _pMsg = new ESMEvent();
					_pMsg->message	= WM_ESM_LIST_GET_FOCUS_FRAME_TOSERVER;
					_pMsg->nParam1 = nValue1;
					_pMsg->nParam2 = nValue2;
					_pMsg->nParam3 = nValue3;
					OnESMMsg((WPARAM)WM_ESM_NET, (LPARAM)_pMsg);
				}
				else
				{
					ESMEvent* _pMsg = new ESMEvent();
					_pMsg->message	= WM_ESM_LIST_GET_FOCUS_FRAME_TOSERVER;
					_pMsg->nParam1 = -1;
					OnESMMsg((WPARAM)WM_ESM_NET, (LPARAM)_pMsg);

					ESMLog(5, _T("[%s] [ERROR__ RESULT GET FOCUS FRAME] PTP_OC_CUSTOM_GetFocusFrame"), pDscItem->GetDeviceDSCID());
				}

				if (pArList != NULL)
				{
					pArList->RemoveAll();
					delete pArList;
				}

				if(pMsg)
				{
					delete pMsg;
					pMsg = NULL;
				}
			}
		}
		break;
	case WM_SDI_RESULT_SET_FOCUS_FRAME:
		{
			if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
			{
				CSdiSingleMgr* pSingMgr = (CSdiSingleMgr* )pMsg->pDest;
				CDSCItem* pDscItem = (CDSCItem*)pSingMgr->GetParent();

				int nResult = (int)pMsg->nParam1;

#if 0
				if (nResult == SDI_ERR_OK)
					nResult = 0x2001; //PTP_RC_OK

				ESMLog(5, _T("[%s] [__RESULT SET FOCUS FRAME] ver =0x%X, model = %s"), pDscItem->GetDeviceDSCID(), pDscItem->GetCamVersion(), pDscItem->GetDeviceModel());
#endif

				if(pDscItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0 && pDscItem->GetCamVersion() >= 0x100)
				{
					Sleep(500);
					ESMEvent* _pMsg = new ESMEvent();
					_pMsg->message	= WM_SDI_OP_GET_FOCUS_FRAME;
					pDscItem->SdiAddMsg(_pMsg);
				}

			}

			if(pMsg)
			{
				delete pMsg;
				pMsg = NULL;
			}
		}
		break;
	default:
		//-- 2013-09-04 hongsu@esmlab.com
		m_wndDSCViewer.m_pDSCMgr->AddMsg((ESMEvent*)pMsg);
		break;
	}		
	return ESM_ERR_NON;
}

//------------------------------------------------------------------------------ 
//! @brief    Message handler for WM_ESM
//! @date     2008-06-20
//! @owner    
//! @note
//! @return        
//! @revision 2009-04-30 hongsu.jung
//!           2010-02-15 hongsu.jung
//!           2010-03-05 hongsu.jung
//!			  2011-10-20 hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT CMainFrame::OnESMMsg(WPARAM wParam, LPARAM lParam)
{
	ESMEvent* pMsg = (ESMEvent*)lParam;
	if(!pMsg)
		return ESM_ERR_MSG;

	if(m_bClose)
		return ESM_ERR_STOP;
	//--------------------------------------------------------//
	//-- 2012-05-24											--//
	//-- Delete Message In Processing or Thread Position	--//
	//-- Check AddMessage (Thread)							--//
	//--------------------------------------------------------//

	switch((int)wParam)
	{
	case WM_ESM_DSC		: return MsgDSC(pMsg);
	case WM_ESM_LIST	: return MsgList(pMsg);
	case WM_ESM_VIEW	: return MsgView(pMsg);	
	case WM_ESM_FRAME	: return MsgFrame(pMsg);	
	case WM_ESM_NET		: return MsgNet(pMsg);		
	case WM_ESM_OPT		: return MsgOpt(pMsg);
	case WM_ESM_MOVIE	: return MsgMovie(pMsg);
	case WM_ESM_EFFECT	: return MsgEffect(pMsg);	//-- 2014-07-15 hongsu@esmlab.com
	case WM_ESM_REMOTE  : return MsgRemote(pMsg);
	case WM_ESM_REFEREE : return MsgReferee(pMsg);
	default:
		{
			ESMLog(0, _T("[ESM] Unknown ESM Message[0x%X]"), (int)wParam);
			//-- 2012-05-24 hongsu
			if(pMsg)
			{
				delete pMsg;
				pMsg = NULL;
			}
			break;
		}
	}
	return ESM_ERR_NON;
}

//------------------------------------------------------------------------------ 
//! @brief    Message handler for WM_ESM_LOG
//! @date     2016-07-28
//! @owner    
//! @note
//! @return
//------------------------------------------------------------------------------ 

LRESULT CMainFrame::OnESMLogMsg(WPARAM wParam, LPARAM lParam)
{
	if(m_bClose)
		return ESM_ERR_STOP;

	esm_msg_info* pEmi = (esm_msg_info*)lParam;
	if(!pEmi)
		return ESM_ERR_MSG;
	//--------------------------------------------------------//
	//-- 2012-05-24											--//
	//-- Delete Message In Processing or Thread Position	--//
	//-- Check AddMessage (Thread)							--//
	//--------------------------------------------------------//

	switch((int)wParam)
	{
	case WM_ESM_LOG_MSG		: ESMLog(pEmi->verbosity, pEmi->lpszFormat);	break;
	default:
		{
			ESMLog(0, _T("[ESM] Unknown ESM Message[0x%X]"), (int)wParam);
			//-- 2012-05-24 hongsu
			if(pEmi)
			{
				delete pEmi;
				pEmi = NULL;
			}
			break;
		}
	}
	return ESM_ERR_NON;
}

//------------------------------------------------------------------------------ 
//! @brief		MsgDSC
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
LRESULT CMainFrame::MsgDSC(ESMEvent* pMsg)
{
	//-- CIC Check
	if(!pMsg)
		return ESM_ERR_MSG;
	//-- Add to Thread Message 
	//-- 2013-09-04 hongsu@esmlab.com
	m_wndDSCViewer.m_pDSCMgr->AddMsg(pMsg);
	return ESM_ERR_NON;
}

LRESULT CMainFrame::MsgView(ESMEvent* pMsg)
{
	int nSize = 0;
	int nTpData1 = 0, nTpData2 = 0;
	char* pTpData = NULL;
	char* pData = NULL;
	FrameData* pFrameData = NULL;
	if(!pMsg)
		return ESM_ERR_MSG;
	switch(pMsg->message)
	{
	case WM_ESM_VIEW_CONTROL_MAIN_ONLY:
		{
			if(pMsg->nParam1 == VK_F10)
			{
				ESMEvent* pMsg = new ESMEvent;
				pMsg->message = WM_ESM_ENV_TEMPLATE;
				::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
				break;
			}
			else
				MakeShortCut(pMsg->nParam1);
		}
		break;
	case WM_ESM_VIEW_CONTROL_MAIN :
		{
			MSG* _pMsg = (MSG*)pMsg->pParam;
			KeyDownMsg(_pMsg);			
		}
		break;
	case WM_ESM_VIEW_GET_FOCUS_FRAME:
		{
			int nValue1 = pMsg->nParam1;

			if (nValue1 == -1)		//error
			{
				m_wndLiveView.GetLiveView()->SetFocusFrame(0,0,0,0,0);
				ESMLog(0, _T("[Focus Frame Load Fail] Press the Zoom IN / OUT button and select again from the list."));
				break;
			}

			int nValue2 = pMsg->nParam2;
			int nValue3 = pMsg->nParam3;
			int nX = nValue1/10000;
			int nY = nValue1%10000;
			int nW = nValue2/10000;
			int nH = nValue2%10000;
			int nMag = pMsg->nParam3;

// 			ESMLog(0, _T("focus frame p1: %d, p2: %d, p3: %d"), pMsg->nParam1, pMsg->nParam2, pMsg->nParam3);
// 			ESMLog(0, _T("focus frame x: %d, y: %d, w: %d, h: %d"), nX, nY, nW, nH);
			m_wndLiveView.GetLiveView()->SetFocusFrame(nX, nY, nW, nH, nMag);
		}
		break;
	case WM_ESM_VIEW_LIVEVIEW_BUFFER:
		if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
		{
			ESMEvent* pMsgBuffer	= new ESMEvent();
			pMsgBuffer->message	= WM_ESM_NET_LIVEVIEW_RESULT;
			pMsgBuffer->nParam2 = pMsg->nParam2;
			nTpData1 = sizeof(UINT) * 2;		// Image Size Infor
			nTpData2 = ((FrameData*)pMsg->pParam)->nWidth * ((FrameData*)pMsg->pParam)->nHeight * 3;		// Image Size Infor

			nSize = nTpData1 +  nTpData2;
			pData = new char[ nSize ];
			pFrameData = (FrameData*)pMsg->pParam;
			memcpy(pData, pFrameData, nTpData1);
			memcpy(pData + nTpData1, (char*)pFrameData->nFrameData, nTpData2);
			pMsgBuffer->pParam	= (LPARAM)pData;
			OnESMMsg((WPARAM)WM_ESM_NET, (LPARAM)pMsgBuffer);

		}
		else
		{
			if (!m_bLiveViewLogFlag)
			{
				ESMLog(5, _T("----WM_ESM_VIEW_LIVEVIEW_BUFFER Liveview get buffer"));
			}
			m_bLiveViewLogFlag = TRUE;

			m_wndLiveView.SetBuffer((PVOID)pMsg->pParam);
		}

		break;
	case WM_ESM_VIEW_SAVE_INFO:
		if(pMsg->nParam1 == TRUE)
			m_wndDashBoard.SetMakeMovieStatus(TRUE);

		m_wndDashBoard.LoadInfo();
		break;
	case WM_ESM_VIEW_DSC_RELOAD:
		{
			m_wndDSCViewer.CameraListReLoad();
			m_wndDSCViewer.m_pListView->RedrawInfo();

			int nAll = m_wndDSCViewer.m_pListView->GetItemCount();
			CDSCItem* pDSCItem = NULL;
			for(int i = 0 ; i < nAll ; i ++)
			{
				pDSCItem = m_wndDSCViewer.m_pListView->GetItemData(i);
				if(pDSCItem)
					m_wndDSCViewer.m_pListView->SetSelectItem(i, pDSCItem);
			}
		}
		break;
	case WM_ESM_VIEW_REDRAW:
		m_wndDSCViewer.ReDraw();
		break;
	case WM_ESM_VIEW_INSERTID:
		m_wndDSCViewer.m_bInsertID = !m_wndDSCViewer.m_bInsertID;
		GetESMOption()->m_4DOpt.bInsertCameraID = m_wndDSCViewer.m_bInsertID;
		m_wndDSCViewer.ReDraw();
		m_wndDashBoard.LoadInfo();
		break;
	case WM_ESM_VIEW_SCALING:
		m_wndDSCViewer.m_bScaleToWindow = !m_wndDSCViewer.m_bScaleToWindow;
		m_wndDSCViewer.ReDraw();
		break;
	case WM_ESM_VIEW_TIMELINE_RESET:
		//-- 2013-10-20 hongsu@esmlab.com
		//-- Clear Viewer
		//m_wndTimeLineEditor.Reset();
		//m_wndTimeLineEditor.UpdateFrames();

		//-- Clear Frame
		m_wndDSCViewer.m_pFrameSelector->ResetTimeLineObject();
		m_wndDSCViewer.ReDraw();
		break;
	case WM_ESM_VIEW_SORT:
		m_wndDSCViewer.SortDSC();
		break;
	case WM_ESM_VIEW_ORGANIZE_INDEX:
		m_wndDSCViewer.OrganizeIndex();
		break;
	case WM_ESM_VIEW_OBJ_PROGRESS:
		{
			float nProgress = (float)pMsg->pParam;
			int nObj		= pMsg->nParam1;
			int nKind		= pMsg->nParam2;			
			m_wndTimeLineEditor.SetProgress(nProgress, nObj, nKind);
		}		
		break;
	case WM_ESM_VIEW_OBJ_UPDATE:
		{
			//  2013-10-18 Ryumin
			//	Select TimeLine Object Editor
			if(!ESMGetSyncObj())
				m_wndTimeLineEditor.m_pTimeLineView->CheckOverReset();
			m_wndTimeLineEditor.UpdateFrames();

			CESMTimeLineObjectEditor* pObj = (CESMTimeLineObjectEditor*)pMsg->nParam1;
			int nSelStart	= 0;
			int nSelEnd		= 0;
			if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
			{
				nSelStart	= DEFAULT_ESM_PICTURE_DRAGTIME;
				nSelEnd		= DEFAULT_ESM_PICTURE_DRAGTIME;
			}
			else
			{
				nSelStart	= pObj->GetStartTime();
				nSelEnd		= pObj->GetEndTime();
			}
			m_wndDSCViewer.m_pFrameSelector->UpdateObject(&pObj->m_arDSCinObj, nSelStart, nSelEnd);
			
			
		}
		break;
	case WM_ESM_VIEW_RECORDDONE:
		{
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, RecordDoneThread, (void *)this, 0, NULL);
			CloseHandle(hHandle);
		}
		break;
// 	case WM_ESM_LIST_DSCDISCONNECT:
// 		{
// // 			HANDLE hHandle = NULL;
// // 			hHandle = (HANDLE) _beginthreadex(NULL, 0, RecordDoneThread, (void *)this, 0, NULL);
// // 			CloseHandle(hHandle);
// 		}
// 		break;
	case WM_ESM_GET_PICTURE_WIZARD_DETAIL:
		{
			if(m_wndProperty.m_pPropertyCtrl)
			{
				m_wndProperty.m_pPropertyCtrl->GetPictureWizardDetail(pMsg->nParam1, pMsg->nParam2);
			}
		}
		break;
	case WM_ESM_GET_PICTURE_WIZARD_DETAIL_ETC:
		{
			if(m_wndProperty.m_pPropertyCtrl)
			{
				m_wndProperty.m_pPropertyCtrl->GetPictureWizardDetailETC(pMsg->nParam1, pMsg->nParam2);
			}
		}
		break;
	case WM_ESM_VIEW_SYNC_GROUP:
		{
			CDSCGroup* pGroup = (CDSCGroup*)pMsg->pParam;
			pGroup->SetRCMgr(m_wndNetwork.GetRCMgrID(pGroup->m_nIP));
		}
		break;
	case WM_ESM_VIEW_NET_UPDATE_CONNECTION:
		{
			CESMRCManager* pRCMgr = (CESMRCManager*)pMsg->pParam;
			m_wndNetwork.UpdateStatus(pRCMgr);
			//joonho.kim 19.01.26 상태체트 수정
			if(!ESMGetRecordStatus())
			{
				m_wndDSCViewer.GroupUpdateStatus(pMsg);
			}
			//jhhan 16-11-15
			m_wndNetworkEx.UpdateStatus(pRCMgr);
		}
		break;
	case WM_ESM_VIEW_NET_UPDATE_DISKSIZE:
		{
			CESMRCManager* pRCMgr = (CESMRCManager*)pMsg->pParam;
			m_wndNetwork.UpdateDiskSize(pRCMgr);
			//jhhan 16-11-15
			m_wndNetworkEx.UpdateDiskSize(pRCMgr);
		}
		break;
	case WM_ESM_VIEW_TIMELINE_RELOAD:
		{
			m_wndDSCViewer.RemoveListAll();
			m_wndDSCViewer.CameraListLoad();
			
			m_wndTimeLineEditor.Reset();
			m_wndTimeLineEditor.UpdateFrames();

			//-- Clear Frame
			m_wndDSCViewer.m_pFrameSelector->ResetTimeLineObject();
			m_wndDSCViewer.ReDraw();
		}
		break;
	case WM_ESM_VIEW_MAKEMOVIEPLAY:
		{
			//CMiLRe 20151119 영상 저장 경로 변경
			TCHAR* cValue = (TCHAR*)pMsg->pParam;
			CString strPlayPath= _T("");
			strPlayPath.Format(_T("%s"), cValue);
			if( cValue)
			{
				delete cValue;
				cValue = NULL;
			}

			if (FileExists(strPlayPath))
			{
				m_wndTimeLineEditor.DSCMoviePlay(strPlayPath);
			}
			else
				m_wndTimeLineEditor.OnDSCMoviePlay();
// 			Sleep(3000);
// 			m_wndDSCViewer.m_pFrameSelector->DrawEditor();	
//			m_wndDSCViewer.SetForegroundWindow();
		}
		break;
	case WM_ESM_VIEW_MERGEMOVIE_PUSHBACK:
	{
		CString *moviePath = (CString *)pMsg->pParam;

		m_wndFIleMergeViewer.SetMoviePathArr(moviePath[0],moviePath[1],true);
	}
	break;
	case WM_ESM_VIEW_MERGEMOVIE_CLEAR:
		{
			CString *moviePath = (CString *)pMsg->pParam;
			m_wndFIleMergeViewer.ClearMoviePath();
		}
		break;

	case WM_ESM_VIEW_SETNEWESTADJ:
		{
			CString strFileName;
			strFileName.Format(_T("%s\\NewestAdj.adj"), ESMGetPath(ESM_PATH_MOVIE_CONF));
			LoadAdjustProfile(strFileName);
		}
		break;

	case WM_ESM_VIEW_FRMESPOTVIEWON:
		{
			int nTime		= pMsg->nParam1;
			int nDscNum		= pMsg->nParam2;	
			m_wndDSCViewer.m_pFrameSelector->SpotViewState(TRUE, nDscNum, nTime);
			m_wndDSCViewer.m_pFrameSelector->DrawEditor();	
		}
		break;

	case WM_ESM_VIEW_FRMESPOTVIEWOFF:
		{
			m_wndDSCViewer.m_pFrameSelector->SpotViewState(FALSE);
		}
		break;
	case WM_ESM_VIEW_AUTODETECT:
		{
#if 0
			//AT 후 Detecting 종료

			track_info stTrack = ESMGetTrackInfo();
			stTrack.bStart = FALSE

			m_wndDSCViewer.m_pListVie;
			ESMSetTrackInfo(stTrack);w->m_AUTO = "DEAUTO";
			m_wndDSCViewer.m_pListView->Invalidate();

			ESMSetKeyValue(VK_F11);
#else
			//Param1 : Time Index
			//Param2 : DSCIndex
			//Param3 : Left or Right
			//ESMGetDSCIndex();
			int nCount = ESMGetViewPointNum();
			
			if(pMsg->nParam3)
				m_wndDSCViewer.m_pFrameSelector->SetSelectAUTOLeftDscIndex(pMsg->nParam1);
			else
				m_wndDSCViewer.m_pFrameSelector->SetSelectAUTORightDscIndex(pMsg->nParam1);

			m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(pMsg->nParam1, nCount);
			m_wndDSCViewer.m_pFrameSelector->SpotViewState(TRUE,pMsg->nParam2,pMsg->nParam1);
			//m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(nCount);
			m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(pMsg->nParam1,pMsg->nParam2);
			::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
			
			//AT 후 Detecting 종료		
			/*track_info stTrack = ESMGetTrackInfo();
			stTrack.bStart = FALSE;
			ESMSetTrackInfo(stTrack);

			m_wndDSCViewer.m_pListView->m_AUTO = "DEAUTO";
			m_wndDSCViewer.m_pListView->Invalidate();*/

			ESMSetKeyValue(VK_F11);
#endif
		}
		break;
	case WM_ESM_VIEW_AUTODETECT_FAIL:
		{
			track_info stTrack = ESMGetTrackInfo();
			stTrack.bStart = FALSE;
			ESMSetTrackInfo(stTrack);

			m_wndDSCViewer.m_pListView->m_AUTO = "DEAUTO";
			m_wndDSCViewer.m_pListView->Invalidate();
		}
		break;
	case WM_ESM_VIEW_AUTODETECT_INFO:
		{
			CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();

			if(pMsg->nParam1)
			{
				pMainWnd->m_wndDSCViewer.m_pListView->m_AUTO = "AUTO";
				ESMLog(1,_T("[AutoDetecting] Set AutoDetect"));	
			}
			else
			{
				pMainWnd->m_wndDSCViewer.m_pListView->m_AUTO = "DEAUTO";
				ESMLog(1,_T("[AutoDetecting] Release AutoDetect"));
			}
			pMainWnd->m_wndDSCViewer.m_pListView->Invalidate();
		}
		break;
	case WM_ESM_LIST_TIMELINEALLDELETE:
		{
			m_wndTimeLineEditor.ItemAllDelete();
		}
		break;
	case WM_MAKEMOVIE_GPU_FILE_ADD_MSG:
		{
			ESMEvent* pMovieMsg = new ESMEvent;
			CString* strFile = (CString*)pMsg->pParam;
			CString* pStrData = NULL;
			pStrData = new CString;
			pStrData->Format(_T("%s"), *strFile);

			pMovieMsg->pParam = (LPARAM)pStrData;
			pMovieMsg->pDest = (LPARAM)this;
			//ESMLog(1, _T("[MSG] WM_MAKEMOVIE_GPU_FILE_ADD_MSG"));
			m_pMovieMakeFileMgr->AddMsg(pMovieMsg);
		}
		break;
	case WM_MAKEMOVIE_GPU_FILE_END:
		{
			if(pMsg->nParam1)
			{
				CStringArray* pstrArray = (CStringArray*)pMsg->pParam;
				CString strSavePath = pstrArray->GetAt(2);

				CESMFileOperation fo;
				if(fo.IsFileExist(strSavePath))
				{
					ESMLog(5,_T("[RefereeMode] %s finish"),strSavePath);
					fo.Delete(strSavePath);
				}
				break;
			}
			switch(ESMGetValue(ESM_VALUE_4DPMETHOD))
			{
			case 0:
				{
					if(!m_wndNetworkEx.m_bThread)
						m_wndNetworkEx.m_pRCClient->SendFrameInfo((vector<MakeFrameInfo>*)pMsg->pParam);
					else
					{
						//Delete Memory
						vector<MakeFrameInfo>* pArrFrameInfo = (vector<MakeFrameInfo>*)pMsg->pParam;
						MakeFrameInfo pFrameInfo;

						for( int i = 0; i< pArrFrameInfo->size(); i++)
						{
							pFrameInfo = pArrFrameInfo->at(i);


							if(pFrameInfo.pYUVImg)
							{
								delete[] pFrameInfo.pYUVImg;
								pFrameInfo.pYUVImg = NULL;
							}

							if(pFrameInfo.Image)
							{
								delete []pFrameInfo.Image;
							}	
						}

						pArrFrameInfo->clear();
						delete pArrFrameInfo;
					}
				}
				break;
			case 1:
			case 2:
				{
					if(!m_wndNetworkEx.m_bThread)
						m_wndNetworkEx.m_pRCClient->SendFrameInfo((CStringArray*)pMsg->pParam);
					else
					{
						//File Delete
						CESMFileOperation fo;
						CStringArray* strArrPath;
						for(int i = 2; i < strArrPath->GetSize(); i++)
						{
							CString strPath = strArrPath->GetAt(i);
							if(fo.IsFileExist(strPath))
								fo.Delete(strPath);
						}
					}
				}
				break;
			}
		}
		break;
	case WM_MAKEMOVIE_GPU_FILE_OPEN_CLOSE:
		{
			if(pMsg->nParam1 == 1)
			{
				if(!m_wndNetworkEx.m_bThread)	//jhhan 16-12-01 구분 수정
					m_wndNetworkEx.m_pRCClient->SendFrameInfoOpenClose(1);
			}else
			{
				if(!m_wndNetworkEx.m_bThread)	//jhhan 16-12-01 구분 수정
					m_wndNetworkEx.m_pRCClient->SendFrameInfoOpenClose(2);
			}
			
		}
		break;
	case WM_MAKEMOVIE_GPU_FILE_CMD_CLOSE:
		{
			ESMUtil::KillProcess(_T("ESMGPUProcess.exe"));
		}
		break;
	case WM_MAKEMOVIE_GPU_FILE:
		{
			//break;

			ESMLog(1, _T("[MSG] WM_MAKEMOVIE_GPU_FILE"));
			vector<MakeFrameInfo>* pArrMovieInfo = new vector<MakeFrameInfo>;
			CString* strFile = (CString*)pMsg->pParam;
			CString strPath = *strFile;
			pMsg->pDest = pMsg->pParam;
			//delete (void*)pMsg->pParam;
			int SecIndex;
			for(int i =0; i < 30; i++)
			{
				MakeFrameInfo stMakeFrameInfo;
				_stprintf(stMakeFrameInfo.strFramePath, strPath);//유니코드
				stMakeFrameInfo.nFrameIndex = i;
				stMakeFrameInfo.nFrameSpeed = 33;
				stMakeFrameInfo.bGPUPlay = TRUE;
				CString strCamID = ESMGetDSCIDFromPath(strPath);
				CString strSecIdx = ESMGetSecIdxFromPath(strPath);
#if 0			
				_stprintf(stMakeFrameInfo.strDSC_ID, _T("%s"), ESMGetDeviceDSCID());//유니코드
				stMakeFrameInfo.nCameraID = _ttoi(ESMGetDeviceDSCID());
#else			//4DP Making - MP4 Parsing
				_stprintf(stMakeFrameInfo.strDSC_ID, _T("%s"), strCamID);//유니코드
				_stprintf(stMakeFrameInfo.strCameraID, _T("%s"), strCamID);
				stMakeFrameInfo.nSecIdx = _ttol(strSecIdx);			//jhhan 16-11-29 SecIdx
				SecIndex = _ttoi(strSecIdx);
#endif
				stMakeFrameInfo.pYUVImg = NULL;
				stMakeFrameInfo.Image = NULL;
				pArrMovieInfo->push_back(stMakeFrameInfo);
				
			}

			pMsg->pParam = (LPARAM)pArrMovieInfo;
			int nCount = ESMGetGPUMakeFileCount();
			pMsg->nParam2 = SecIndex; //hjcho 초 index 수정
			pMsg->nParam3 = TRUE;

			m_wndTimeLineEditor.m_pTimeLineView->MakeMovieFrameBase(pMsg);
			nCount++;
			ESMSetGPUMakeFileCount(nCount);
		}
		break;
	case WM_MAKEMOVIE_REQUES_DATA:
		{
			m_wndTimeLineEditor.m_pTimeLineView->RequestData(pMsg);
		}
		break;
	case WM_MAKEMOVIE_DP_FRAME:
		{
			m_wndTimeLineEditor.m_pTimeLineView->MakeMovieFrameBase(pMsg);
		}
		break;
	case WM_MAKEMOVIE_REQUEST_DATA_SEND_FINISH:
		{
			CString* strPath = (CString*)pMsg->pDest;
			CESMFileOperation fo;

			CString strFile;
			CString strToken;
			int nPos = 0;
			while((strToken = strPath->Tokenize(_T("\\"),nPos)) != "")
			{
				strFile.Format(_T("\\%s"), strToken);
			}
			
			
			strPath->Replace(strFile,_T(""));
			fo.CreateFolder(*strPath);
			strPath->Append(strFile);
			CFile file;

			int nStart = GetTickCount();
			int nOpenEnd = 0, nWriteEnd = 0;

			
			//File Write
			if(file.Open(*strPath, CFile::modeCreate | CFile::modeReadWrite))
			{
				nOpenEnd = GetTickCount();
				file.Write((void*)(pMsg->pParam + sizeof(RCP_FileSave_Info)), pMsg->nParam2-sizeof(RCP_FileSave_Info));
				nWriteEnd = GetTickCount();
				file.Close();
				//ESMLog(1, _T("[#NETWORK] FILE Write [%s]"), *strPath);
			}
			else
				ESMLog(0, _T("[#NETWORK] FILE Open Fail [%s]"), *strPath);

			if(strPath)
				delete strPath;
		}
		break;
	case WM_MAKEMOVIE_REQUEST_DATA_FINISH:
		{
			CString* strPath = (CString*)pMsg->pDest;
			CESMFileOperation fo;

			int nMovieFinish = pMsg->nParam3;

			BOOL bMerge = TRUE;

			CString strFile;
			CString strToken;
			int nPos = 0;
			while((strToken = strPath->Tokenize(_T("\\"),nPos)) != "")
			{
				strFile.Format(_T("\\%s"), strToken);
			}
			
			
			strPath->Replace(strFile,_T(""));
			fo.CreateFolder(*strPath);
			strPath->Append(strFile);
			CFile file;

			int nStart = GetTickCount();
			int nOpenEnd = 0, nWriteEnd = 0;

			int nNetMgrCount = m_wndNetwork.m_arRCServerList.GetCount();
			for(int i = 0; i < nNetMgrCount; i++)
			{
				CESMRCManager* pRCMgr = NULL;
				//CDSCGroup* pGroup = NULL;
				pRCMgr = (CESMRCManager *)m_wndNetwork.m_arRCServerList.GetAt(i);
				//pGroup = new CDSCGroup(pRCMgr->GetID());
				//pGroup->SetRCMgr(pRCMgr);


				if(pRCMgr->m_strLocalPath.CompareNoCase(*strPath) == 0)
				{
					int nLength = pMsg->nParam2-sizeof(RCP_FileSave_Info);
					char* buf = new char[nLength];
					CString* path = new CString;
					path->Format(_T("%s"), *strPath);
					memcpy(buf, (void*)(pMsg->pParam + sizeof(RCP_FileSave_Info)), nLength);
					ESMEvent* pMsg = new ESMEvent();
					//pMsg->message = WM_ESM_NET_REQUEST_DATA;
					pMsg->message = WM_ESM_NET_REQUEST_SEND_DATA;
					pMsg->pParam = (LPARAM)path;
					pMsg->pDest  = (LPARAM)buf;	
					pMsg->nParam1 = nLength;

					CDSCGroup* pGroup = NULL;
					pGroup = new CDSCGroup(pRCMgr->GetID());
					pGroup->SetRCMgr(pRCMgr);

					pGroup->GetRCMgr()->AddMsg(pMsg);
					
					bMerge = FALSE;
				}
				/*else
				{
					delete pGroup;
				}*/
			}

			nNetMgrCount = m_wndNetworkEx.m_arRCServerList.GetCount();
			for(int i = 0; i < nNetMgrCount; i++)
			{
				CESMRCManager* pRCMgr = NULL;
				//CDSCGroup* pGroup = NULL;
				pRCMgr = (CESMRCManager *)m_wndNetworkEx.m_arRCServerList.GetAt(i);
				//pGroup = new CDSCGroup(pRCMgr->GetID());
				//pGroup->SetRCMgr(pRCMgr);


				if(pRCMgr->m_strLocalPath.CompareNoCase(*strPath) == 0)
				{
					int nLength = pMsg->nParam2-sizeof(RCP_FileSave_Info);
					char* buf = new char[nLength];
					CString* path = new CString;
					path->Format(_T("%s"), *strPath);
					memcpy(buf, (void*)(pMsg->pParam + sizeof(RCP_FileSave_Info)), nLength);
					ESMEvent* pMsg = new ESMEvent();
					//pMsg->message = WM_ESM_NET_REQUEST_DATA;
					pMsg->message = WM_ESM_NET_REQUEST_SEND_DATA;
					pMsg->pParam = (LPARAM)path;
					pMsg->pDest  = (LPARAM)buf;	
					pMsg->nParam1 = nLength;
					
					CDSCGroup* pGroup = NULL;
					pGroup = new CDSCGroup(pRCMgr->GetID());
					pGroup->SetRCMgr(pRCMgr);

					pGroup->GetRCMgr()->AddMsg(pMsg);

					bMerge = FALSE;
				}
				/*else
				{
					delete pGroup;
				}*/
			}
#ifndef _TCP_SENDER
			//File Write
			if(bMerge == TRUE)
			{
				if(file.Open(*strPath, CFile::modeCreate | CFile::modeReadWrite))
				{
					nOpenEnd = GetTickCount();
					file.Write((void*)(pMsg->pParam + sizeof(RCP_FileSave_Info)), pMsg->nParam2-sizeof(RCP_FileSave_Info));
					nWriteEnd = GetTickCount();
					file.Close();
					////AJA
					//if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetConnectAJANetwork()	&& ESMGetAJAScreen() == FALSE)
					//{
					//	CString* pCsAJAPath = new CString;
					//	pCsAJAPath->Format(_T("%s"),csAJASave);
					//	CString strFileName = ESMGetFileName(*strPath);
					//	CString strTemp = strFileName.Left(strFileName.GetLength()-strFileName.ReverseFind(_T('.')));
					//	int nMovieIdx = _ttoi(strTemp);

					//	ESMEvent*pAJAMsg = new ESMEvent;
					//	memcpy(pAJAMsg,pMsg,sizeof(ESMEvent));
					//	pAJAMsg->message = F4DC_SEND_CPU_FILE;
					//	pAJAMsg->pDest	 = (LPARAM)pCsAJAPath;
					//	pAJAMsg->nParam1 = nMovieIdx;
					//	ESMSendAJANetwork(pAJAMsg);
					//}
					CString strMsg = _T("- Merge");
					//ESMLog(1, _T("[#NETWORK] FILE Write [%s]"), *strPath);
					if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
					{
						if(ESMGetGPUMakeFile() && ESMGetRemoteSkip() == TRUE)
						{
							////Adjust Load
							//if(m_bTrcAdjustLoad == FALSE)
							//{
							//	FILE* adjdata = fopen("M:\\AdjData.txt","w");
							//	CString strCAMID = ESMGetDSCIDFromPath(*strPath);
							//	ESMCalcAdjustDataAsText(strCAMID,adjdata,1);
							//	ESMCalcAdjustDataAsText(strCAMID,adjdata,0.5);
							//	fclose(adjdata);
							//	m_bTrcAdjustLoad = TRUE;
							//}
							//Run Transcode console

							HANDLE hSyncTime = NULL;
							TRANSCODE_INFO* info = new TRANSCODE_INFO;
							info->pMain = this;
							info->strPath = *strPath;
							hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, RunTranscode, info, 0, NULL);
							CloseHandle(hSyncTime);

							/*CString strOutputPath = ESMDoTranscodeAs720p(*strPath);
							
							CESMFileOperation fo;
							if(fo.IsFileExist(strOutputPath))
							{
								m_wndNetwork.m_pRCClient->SendMovieFinish(strOutputPath);
								fo.Delete(strOutputPath);
							}
							if(fo.IsFileExist(*strPath))
								fo.Delete(*strPath);

							strMsg = _T("- Transcode");*/
						}else
						{
							int nTick = GetTickCount();
							int nReceiveTime = nTick-m_nRTThreadTime;
							//ESMLog(5,_T("[RTThread] %s - %d"),*strPath,nReceiveTime);
							//
							m_nRTThreadTime = nTick;

							if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
							{
								//jhhan 190128 제외
								if(ESMGetValue(ESM_VALUE_REFEREEREAD) != TRUE)
								{
									switch(ESMGetValue(ESM_VALUE_4DPMETHOD))
									{
									case 0:
										//m_pMovieRTThread->SetRTSendPath(*strPath);
										break;
									case 1:
										{
											//m_pMovieRTMovie->AddReceiveTime(nReceiveTime);
											m_pMovieRTMovie->SetRTSendPath(*strPath);										}
										break;
									case 2:
										m_pMovieRTSend60p->SetRTSendPath(*strPath);
										break;
									}
								}
								else
								{
									//jhhan 190129 m3u8
									/*static CESMm3u8 _m3u8;
									CString strText;
									strText.Format(_T("%s\\%s.m3u8"), _T("M:\\Movie"), ESMGetDSCIDFromPath(*strPath));
									_m3u8.SetFile(strText);
									_m3u8.Write(*strPath);*/
								}
								
							}
							/*if(ESMGetValue(ESM_VALUE_4DPMETHOD) == FALSE)
								m_pMovieRTSend->SetRTSendPath(*strPath);
							else
								m_pMovieRTThread->SetRTSendPath(*strPath);*/

							strMsg = _T("- RTThread");
						}
					}else
					{
						if(nMovieFinish == 100)
						{
							//ESMLog(1, _T("[#NETWORK] FILE Add [%s]"), *strPath);
#if 1
							//CESMUtilRemoteCtrl* pRemote = GetFrameExWnd()->m_pRemoteCtrlDlg;
							//if(pRemote)
							//{
							//	pRemote->AddDSCPath(*strPath);
							//}

#else
							if(m_pRemoteCtrlDlg)
							{
								m_pRemoteCtrlDlg->AddDSCPath(*strPath);
							}
#endif
							//m_pRemoteCtrlDlg->AddDSCPath(*strPath);
							strMsg = _T("- FrameThread");
						}
					}
					
					TRACE(("[#NETWORK] FILE Write [%s] %s\r\n"), *strPath, strMsg);
					//ESMLog(1, _T("[#NETWORK] FILE Write [%s] %s"), *strPath, strMsg);
					
				}
				else
					ESMLog(0, _T("[#NETWORK] FILE Open Fail [%s]"), *strPath);
			}
#endif
			

			if(strPath)
				delete strPath;
		}
		break;
	case WM_MAKEMOVIE_DP_MOVIE_FINISH:
		{
			m_wndTimeLineEditor.m_pTimeLineView->MakeMovieFrameBaseMovieFinish(pMsg);
		}
		break;
	case WM_MAKEMOVIE_MUX_MOVIE_FINISH:
		{
			CString* pCamID = (CString*)pMsg->pDest;

			if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
			{
				m_wndNetwork.m_pRCClient->SendMuxFinish(pMsg->nParam1, pCamID->GetString());
			}else
			{
				if(m_pMultiplexerDlg)
					m_pMultiplexerDlg->SetFinish(pMsg->nParam1);
			}

			if (pCamID)
			{
				delete pCamID;
				pCamID = NULL;
			}
		}
		break;
	case WM_ESM_REMOTE_SPOT_POINT:
		{
			m_wndDSCViewer.m_pFrameSelector
				->ShowFrameSpotFromRemote(pMsg->nParam1,pMsg->nParam2,pMsg->nParam3);
		}
		break;
	case WM_MAKEMOVIE_REFEREE_LIVE_MERGE:
		{
			//pMsg->nParam1 : sec index
			//pMsg->nParam2 : Group index
			//pMsg->nParam3 : 용량
			//pMsg->pDest   : Data

			int nBodySize = pMsg->nParam3;

			char* pData = new char[nBodySize];//(char*)pMsg->pDest;
			memcpy(pData,(char*)pMsg->pDest,nBodySize);

			CESMFileOperation fo;
			CString strPath,strSavePath;
			strSavePath.Format(_T("M:\\Movie\\%s"),ESMGetFrameRecord());
			if(!fo.IsFileFolder(strSavePath))
				fo.CreateFolder(strSavePath);
			
			strPath.Format(_T("%s\\%d_%d.mp4"),strSavePath,pMsg->nParam2,pMsg->nParam1);

			m_pRefereeMgr->RunReceiveThread(pData,pMsg->nParam1,pMsg->nParam2,nBodySize,strPath);
		}
		break;
	case WM_MAKEMOVIE_REFEREE_MUX_FINISH:
		{
			int nBodySize = pMsg->nParam3;

			char* pData = new char[nBodySize];//(char*)pMsg->pDest;
			memcpy(pData,(char*)pMsg->pParam,nBodySize);

			CString* pstrPath = (CString*)pMsg->pDest;
			CString strPath;
			strPath.Format(_T("%s"),*pstrPath);
			m_pMultiplexerDlg->ReceiveMuxData(pData,strPath,nBodySize);
		}
		break;
	case WM_MAKEMOVIE_REFEREE_OPENCLOSE:
		{
			m_pRefereeMgr->SetOpenClose(pMsg->nParam1,pMsg->nParam2);
		}
		break;
	case WM_MAKEMOVIE_REFEREE_MUXSTART:
		{
			//pMsg->nParam2 : cameraindex
			//pMsg->nParam3 : time
			if(m_pRefereeMgr)
			{
				int nFrameIdx = 0;
				int nMovieIdx = 0;
				ESMGetMovieIndex(ESMGetFrameIndex(pMsg->nParam3),nMovieIdx,nFrameIdx);
				m_pRefereeMgr->AddEvent(pMsg->nParam2,nMovieIdx,nFrameIdx);
			}
		}
		break;
	case WM_MAKEMOVIE_REFEREE_4KMOVIE:
		{
			//pHeader->param2: Camera Index
			//pHeader->param3: Frame Index(Movie Index * 60 + Frame Index)

			if(m_pRefereeMgr)
			{
				int nGroupIdx = pMsg->nParam2 / 100;
				int nDSCIdx = pMsg->nParam2 % 100;
				m_pRefereeMgr->Launch4KMovieSender(nGroupIdx,nDSCIdx,pMsg->nParam3);
			}
		}
		break;
	default:
		break;
	}
	
	//-- delete pointer
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}	
	return ESM_ERR_NON;
}

unsigned WINAPI CMainFrame::RunTranscode(LPVOID param)
{
	TRANSCODE_INFO* pinfo = (TRANSCODE_INFO*) param;
	CMainFrame* pMain = (CMainFrame*)pinfo->pMain;
	CString strOutputPath = ESMDoTranscodeAs720p(pinfo->strPath);

	CESMFileOperation fo;
	//jhhan 180115 TransCode 전송 중지 파일 저장
	/*if(fo.IsFileExist(strOutputPath))
	{
		pMain->m_wndNetwork.m_pRCClient->SendMovieFinish(strOutputPath);
		fo.Delete(strOutputPath);
	}*/
	//if(fo.IsFileExist(pinfo->strPath))
	//	fo.Delete(pinfo->strPath);

	//strMsg = _T("- Transcode");

	return 0;
}

void CMainFrame::MakeFolder(CString strPath)
{
	CString csPrefix(_T("")), csToken(_T(""));
	int nStart = 0, nEnd;
	while( (nEnd = strPath.Find('/', nStart)) >= 0)
	{
		CString csToken = strPath.Mid(nStart, nEnd-nStart);
		CreateDirectory(csPrefix + csToken, NULL);

		csPrefix += csToken;
		csPrefix += _T("/");
		nStart = nEnd+1;
	} 
	csToken = strPath.Mid(nStart);
	CreateDirectory(csPrefix + csToken, NULL);
}
LRESULT CMainFrame::MsgList(ESMEvent* pMsg)
{
	//-- CIC Check
	if(!pMsg)
	{
		return ESM_ERR_MSG;
	}

	CDSCItem* pItem = NULL;
	int nType = 0, nValue = 0;
	CString strValue, strDSCID;
	IDeviceInfo *pDeviceInfo = NULL;
	CSdiSingleMgr* pSingMgr = NULL;

	switch(pMsg->message)
	{
	case WM_ESM_LIST_SAVESYNC:
		OnSaveSyncTestResult();
		break;
	case WM_ESM_LIST_SCROLL:
		m_wndDSCViewer.m_pFrameSelector->DrawEditor();	
		break;
	case WM_ESM_LIST_CHECK:
		{
			m_wndDSCViewer.m_pDSCMgr->RemoveAllConnectVendorId();
			m_wndDSCViewer.m_pDSCMgr->CheckConnect();			
		}
		break;
	case WM_ESM_LIST_ADD:
		//-- Add to from Remote
		m_wndDSCViewer.AddDSC((CDSCItem*)pMsg->pParam);
		break;
	case WM_ESM_LIST_SELECT:
		{
			//-- Change Property
			CDSCItem* pDsc = (CDSCItem*)pMsg->pDest;
			if( pDsc )
			{
				/*if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
				{
					int nRecFlag = ESMGetRecordingInfoInt(_T("RecFlag"), -1);
					if( SDI_STATUS_REC != nRecFlag)
					{
						m_wndProperty.m_pPropertyCtrl->LoadDSCInfo((CDSCItem*)pMsg->pDest);
						m_wndLiveView.Connection((CDSCItem*)pMsg->pDest);
					}
				}
				
				else*/ 
				//2018.06.06 joonho.kim
			    //if(!(pDsc->GetDSCStatus() == SDI_STATUS_REC)
				if(!(pDsc->GetDSCStatus() == SDI_STATUS_REC) /*&& m_wndDSCViewer.m_pListView->m_bShowProp*/)
				{
					m_wndProperty.m_pPropertyCtrl->LoadDSCInfo((CDSCItem*)pMsg->pDest);
					m_wndLiveView.Connection((CDSCItem*)pMsg->pDest);	
				}
				else if(!(pDsc->GetDSCStatus() == SDI_STATUS_REC) && !ESMGetFirstFlag())
				{
					ESMSetFirstFlag(TRUE);
					m_wndProperty.m_pPropertyCtrl->LoadDSCInfo((CDSCItem*)pMsg->pDest);
					m_wndLiveView.Connection((CDSCItem*)pMsg->pDest);	
				}
			}		
			break;
		}
	case WM_ESM_LIST_SET_GROUP_PROPERTY:
		//-- Add to Thread Message 
		//-- 2013-09-04 hongsu@esmlab.com
		m_wndDSCViewer.SetGroupProperty(pMsg);
		break;
	case WM_ESM_LIST_PROPERTY_RESULT:
		{
			CString* pDSCID = (CString*)pMsg->pDest;
			strDSCID = *pDSCID;
			m_wndDSCViewer.SetPropertyListView((ESMEvent*)pMsg);
			m_wndProperty.m_pPropertyCtrl->LoadPropertyNetInfo((ESMEvent*)pMsg);

			nType = (int)pMsg->nParam1;
			nValue = (int)pMsg->nParam2;
			strValue = m_wndProperty.m_pPropertyCtrl->GetPropertyValueStr(nType, nValue);
			pItem = m_wndDSCViewer.GetItemData(strDSCID);

			if (nType == PTP_OC_SAMSUNG_GetFocusPosition)
			{
				pItem->SetFocus(nValue);
				strValue.Format(_T("%d"), nValue);
			}
			else if (nType == PTP_DPC_SAMSUNG_FW_VERSION)
			{
				pItem->SetCamVersion(nValue);
			}

			if(pItem)
				pItem->SetDSCCaptureInfo(nType, strValue);

			//ESMLog(5, _T("id:%s, type:%x, strvalue:%s"), strDSCID, nType, strValue);

			break;
		}
	case WM_ESM_LIST_PROPERTY_ISO_INCREASE:
		m_wndProperty.m_pPropertyCtrl->GetPropertyIsoIncrease((CDSCItem*)pMsg->pDest);
			break;
	case WM_ESM_LIST_PROPERTY_ISO_DECREASE:
		m_wndProperty.m_pPropertyCtrl->GetPropertyIsoDecrease((CDSCItem*)pMsg->pDest);
			break;
	case WM_ESM_LIST_FORMAT_ALL: 
		m_wndDSCViewer.FormatDeviceAll();
		break;
	//-- 2014-9-4 hongsu@esmlab.com
	//-- F/W Update
	case WM_ESM_LIST_FW_UPDATE: 
		m_wndDSCViewer.FWUpdateAll();
		break;

	case WM_ESM_LIST_MOVIEMAKINGSTOP: 
		m_wndDSCViewer.MovieMakingStop();
		break;
	case WM_ESM_LIST_CAPTURERESUME: 
		m_wndDSCViewer.CaptureResume((int)pMsg->nParam1);
		break;

	case WM_ESM_LIST_CAMSHUTDOWN: 
		m_wndDSCViewer.CamShutDownAll();
		break;

	case WM_ESM_LIST_INITMOVIEFILE:
		m_wndDSCViewer.InitMovieFile();
		break;

	case WM_ESM_LIST_VIEWLARGE:
		m_wndDSCViewer.ViewLarge(pMsg->nParam1);
		break;
	case WM_ESM_LIST_GETFOCUS_TOCLIENT: 
		m_wndDSCViewer.GetFocusAll();
		break;
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	case WM_ESM_LIST_GETFOCUS_TOCLIENT_READ_ONLY:
	//CMiLRe 20160204 Focus All Setting 시 Focus 저장되는 버그 수정
		m_wndDSCViewer.GetFocusAll(FALSE);
		break;
	case WM_ESM_LIST_SETFOCUS_TOCLIENT:
		{
			CString*pDSCID = (CString*)pMsg->pDest;
			strDSCID = *pDSCID;

			m_wndDSCViewer.SetFocus(strDSCID, pMsg->nParam1, pMsg->nParam2);
			if(pDSCID)
			{
				delete pDSCID;
				pDSCID = NULL;
			}
			break;
		}

	case WM_ESM_LIST_SETFOCUS_LOADFILE:
		{
			CString strPath;
			CString *pPath = (CString*)pMsg->pDest;
			strPath = *pPath;

			if(pPath)
			{
				delete pPath;
				pPath = NULL;
			}

			m_wndDSCViewer.SetFocusLoadFile(strPath);
			break;
		}

	case WM_SDI_OP_HIDDEN_COMMAND:
		m_wndDSCViewer.SetHiddenCommandAll(pMsg);
		break;

	case WM_ESM_LIST_HIDDEN_COMMAND:
		m_wndDSCViewer.SetGroupHiddenCommand(pMsg);
		break;
	case WM_ESM_LIST_SET_ENLARGE_TOCLIENT:
		{
			CString* pDSCID = (CString*)pMsg->pDest;
			strDSCID = *pDSCID;
			pItem = m_wndDSCViewer.GetItemData(strDSCID);

			if(pItem)
			{
				int nValue = pMsg->nParam1;
				m_wndLiveView.SetZoom(pItem, nValue);

				if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
				{
					// enlarge 적용하면 Focus Frame 크기가 변경되므로 Get Focus Frame을 해줘야한다.					
					ESMEvent* _pMsg = new ESMEvent();
					_pMsg->message	= WM_SDI_OP_GET_FOCUS_FRAME;
					pItem->SdiAddMsg(_pMsg);
				}
			}

			if(pDSCID)
			{
				delete pDSCID;
				pDSCID = NULL;
			}
		}
		break;
	case WM_ESM_LIST_HALF_SHUTTER_TOCLIENT:
		{
			CString* pDSCID = (CString*)pMsg->pDest;
			strDSCID = *pDSCID;
			pItem = m_wndDSCViewer.GetItemData(strDSCID);

			if(pItem)
			{
				int nRelease = (int)pMsg->nParam1;
				int nReleaseSec = (int)pMsg->nParam2;
				m_wndLiveView.OnHalfShutter(pItem, nRelease, nReleaseSec);
			}

			if(pDSCID)
			{
				delete pDSCID;
				pDSCID = NULL;
			}
		}
		break;
	case WM_ESM_LIST_FOCUS_LOCATE_SAVE_TOCLIENT:
		{
			CString* pDSCID = (CString*)pMsg->pDest;
			strDSCID = *pDSCID;
			pItem = m_wndDSCViewer.GetItemData(strDSCID);

			if(pItem)
			{
				ESMEvent* pSdiMsg = NULL;
				pSdiMsg = new ESMEvent;
				pSdiMsg->message = WM_SDI_OP_FOCUS_LOCATE_SAVE;
				pItem->SdiAddMsg(pSdiMsg);
			}

			if(pDSCID)
			{
				delete pDSCID;
				pDSCID = NULL;
			}
		}
		break;
	case WM_ESM_LIST_SET_FOCUS_FRAME_TOCLIENT:
		{
			CString* pDSCID = (CString*)pMsg->pDest;
			strDSCID = *pDSCID;
			pItem = m_wndDSCViewer.GetItemData(strDSCID);

			if(pItem)
			{
				ESMEvent* pSdiMsg = NULL;
				pSdiMsg = new ESMEvent;
				pSdiMsg->message = WM_SDI_OP_SET_FOCUS_FRAME;
				pSdiMsg->nParam1 = pMsg->nParam1;
				pSdiMsg->nParam2 = pMsg->nParam2;
				pSdiMsg->nParam3 = pMsg->nParam3;
				pItem->SdiAddMsg(pSdiMsg);
			}

			if(pDSCID)
			{
				delete pDSCID;
				pDSCID = NULL;
			}
		}
		break;
	case WM_ESM_LIST_ONE_SHOT_AF:
		{
			int nModel = pMsg->nParam1;
			if (nModel == 0)
			{
				pItem = (CDSCItem*)pMsg->pParam;
				m_wndDSCViewer.m_pDSCMgr->DoS1Press(pItem);
			}
			else
			{
				// all // NX1
				m_wndDSCViewer.m_pDSCMgr->DoS1Press();
			}
		}
		break;
	case WM_RS_RC_RECORDFINISH:
		{									
			// finish timer 필요		
			if (m_bReSyncFlag == TRUE)
			{
				if (m_bTimerRecFinishRunning)
					break;

				int nWaitTime = ESMGetValue(ESM_VALUE_RESYNC_WAIT_TIME);
				ESMLog(5, _T("----- Resync Record wait time: %d"), nWaitTime);

				if (nWaitTime < 18000)
				{
					ESMLog(5, _T("----- Resync Record wait time value change =>  18000"));
					nWaitTime = 18000;
				}
				nWaitTime = nWaitTime/10;

				SetTimer(TIMER_RECORD_FINISH_STATUS, nWaitTime, NULL);
			}
			else
			{
				KillTimer(TIMER_RECORD_FINISH_STATUS);
				SetTimer(TIMER_RECORD_FINISH_STATUS, 2000, NULL);
			}  		  			

			//ESMLog(5, _T("########## WM_RS_RC_RECORDFINISH... ReSyncFlag: %d,  ReSyncRecStopFinishFlag: %d"), m_bReSyncFlag, m_bReSyncRecStopFinishFlag);

			m_bTimerRecFinishRunning = TRUE;
		}
		break;
	case WM_ESM_DSC_GET_TICK_RESULT_CMD:
		{
			//ESMLog(5, _T("########## WM_ESM_DSC_GET_TICK_RESULT_CMD..."));
			// gettick finish timer 필요			
			m_bGetTickFirstFlag = TRUE;
 			KillTimer(TIMER_RECORD_RESYNC_GET_TICK_FINISH);
 			SetTimer(TIMER_RECORD_RESYNC_GET_TICK_FINISH, 1000*6, NULL);
		}
		break;
	case WM_ESM_LIST_CAM_ERROR_MSG:
		{
			int nMsg = pMsg->nParam1;
			CString* pDSCID = (CString*)pMsg->pDest;
			strDSCID = *pDSCID;
			pItem = m_wndDSCViewer.GetItemData(strDSCID);			

			switch(nMsg)
			{
			case ERROR_TEMPERATURE_REC_STOP_ADVANCE_NOTICE:
				{
					if (pItem)
						pItem->SetTemperatureWarning(FALSE);				
				}
				break;
			case ERROR_TEMPERATURE_REC_STOP_NOTICE:
			case ERROR_TEMPERATURE_REC_STOP_DROP:
				{
					if (pItem)
						pItem->SetTemperatureWarning(TRUE);
				}
				break;
			default:
				break;
			}

			m_wndDSCViewer.m_pListView->RedrawInfo();
		}
		break;
	case WM_RS_RC_GET_TICK_CMD:
		{
			m_nResyncGetTickCount = 0;

			ESMLog(5, _T("########## WM_RS_RC_GET_TICK_CMD!!!!!!!!!!!!!!!!..."));
			ESMEvent* _pMsg = new ESMEvent();
			_pMsg->message	= WM_RS_RC_GET_TICK_CMD;
			_pMsg->nParam1	= m_nResyncGetTickCount;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_DSC, (LPARAM)_pMsg);

			// gettick count 1
			//SetTimer(TIMER_RECORD_RESYNC_GET_TICK_REQUEST, 2000, NULL);
		}
		break;
	default:
		break;
	}
	
	//-- delete pointer
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}	
	return ESM_ERR_NON;
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-05-07
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
LRESULT CMainFrame::MsgFrame(ESMEvent* pMsg)
{
	//-- CIC Check
	if(!pMsg)
		return ESM_ERR_MSG;
	switch(pMsg->message)
	{
	case WM_ESM_FRAME_CONVERT_FINISH:
		{
			//-- 2013-09-29 hongsu@esmlab.com
			//-- Message to Finish Convert Frame 
			if(!pMsg->pDest)
				break;

			CDSCItem* pItem = (CDSCItem*)pMsg->pDest;
			if(!pItem)
				break;
			int nIndex = pMsg->nParam1;
			if( nIndex == 0)
				m_wndDSCViewer.m_pFrameSelector->SetInitRecTime();

			//-- 2013-09-29 hongsu@esmlab.com
			//-- Change Status 
			pItem->SetDSCStatus(SDI_STATUS_REC_FINISH); //disconnect camera no change
			if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
			{
				ESMEvent* pNetMsg = new ESMEvent;
				pNetMsg->message	= WM_ESM_FRAME_CONVERT_FINISH;
				pNetMsg->nParam1	= pItem->GetSavedLastTime();
				pNetMsg->pDest	= pMsg->pDest;
				OnESMMsg((WPARAM)WM_ESM_NET, (LPARAM)pNetMsg);
			}

			//-- Update Draw Editor TimeLine
			int nLastTime = pItem->GetSavedLastTime();
			if(!m_wndDSCViewer.m_pFrameSelector->SetSavedLastTime(nLastTime))
				ESMLog(0,_T("Convert File Check [%s]"), pItem->GetDeviceDSCID());

			//-- Update Draw Editor Color
			m_wndDSCViewer.m_pFrameSelector->DrawEditor();			
		}
		break;
	case WM_ESM_FRAME_SHOW:
		{
			//ESMLog(5,_T("[Msg]WM_ESM_FRAME_SHOW"));
			int nSelectTime =0 , nLine = 0;
			nSelectTime = (int)pMsg->nParam1;
			nLine = (int)pMsg->nParam2;
			m_wndFrame.SetSelectedTime(nSelectTime, nLine);
			m_wndFrame.SetSelectedDSC(((CDSCItem*)pMsg->pDest)->GetDeviceDSCID());
			m_wndFrame.SetSelectedIP(((CDSCItem*)pMsg->pDest)->GetDSCInfo(DSC_INFO_LOCATION));
			//-- 2.Update Frames
			ESMSetFrameDecodingFlag(pMsg->nParam3);
			m_wndFrame.UpdateFrames(TRUE); 
		}		
		break;
	case WM_ESM_FRAME_RESET:
		{
			m_wndFrame.SetSelectedTime(0);
			m_wndFrame.SetSelectedDSC(_T(""));
			m_wndFrame.SetSelectedIP(_T("0"));
			m_wndFrame.UpdateFrames(TRUE); 
		}		
		break;
	case WM_ESM_FRAME_REMOVE_OBJ_SELECTOR:
		{
			m_wndDSCViewer.m_pFrameSelector->RemoveObject();
			m_wndDSCViewer.m_pFrameSelector->DrawEditor();
		}		
		break;
	case WM_ESM_FRAME_INSERT_OBJ_EDITOR_M:
		{
			CESMTimeLineObjectEditor* pObj = (CESMTimeLineObjectEditor*)pMsg->pDest;
			m_wndTimeLineEditor.m_pTimeLineView->InsertObj(pObj, FALSE);
			m_wndTimeLineEditor.m_pTimeLineView->DrawEditor();
		}
		break;
	case WM_ESM_FRAME_INSERT_OBJ_EDITOR_P:
		{
			CESMTimeLineObjectEditor* pObj = (CESMTimeLineObjectEditor*)pMsg->pDest;
			m_wndTimeLineEditor.m_pTimeLineView->InsertObj(pObj, TRUE);
			m_wndTimeLineEditor.m_pTimeLineView->DrawEditor();
		}
		break;
	case WM_ESM_FRAME_REMOVE_OBJ_EDITOR:
		{
			//-- Reset Effect Editor
			m_wndEffectEditor.LoadMovieObject(NULL);
			m_wndEffectFrameViewer.m_pFrameView->SetObjectFrame(NULL);
			m_wndEffectFrameViewer.m_pFrameView->ShowWindow(SW_HIDE);

			CESMTimeLineObjectEditor* pObj = (CESMTimeLineObjectEditor*)ESMGetCopyObj();
			m_wndTimeLineEditor.m_pTimeLineView->RemoveObject((CESMTimeLineObjectEditor*)pMsg->pDest);

			if((LPARAM)pObj == pMsg->pDest)
			{
				ESMSetCopyObj(NULL);
			}
			//-- Calculate Total Time
			m_wndTimeLineEditor.m_pTimeLineView->CalculateTime();
			//-- Redraw
			m_wndTimeLineEditor.m_pTimeLineView->DrawEditor();
		}
		break;
	//CMiLRe 20151020 TimeLine Object Delete Key 추가
	case WM_ESM_FRAME_REMOVE_OBJ_TAIL:
		{
			//-- Reset Effect Editor
			m_wndEffectEditor.LoadMovieObject(NULL);
			m_wndEffectFrameViewer.m_pFrameView->SetObjectFrame(NULL);
			m_wndEffectFrameViewer.m_pFrameView->ShowWindow(SW_HIDE);
			m_wndTimeLineEditor.m_pTimeLineView->RemoveObjectTail();
			//-- Calculate Total Time
			m_wndTimeLineEditor.m_pTimeLineView->CalculateTime();
			//-- Redraw
			m_wndTimeLineEditor.m_pTimeLineView->DrawEditor();
		}
		break;
	//CMiLRe 20151020 TimeLine Object Delete Key 추가
	case WM_ESM_FRAME_REMOVE_OBJ_HEADER:
		{
			//-- Reset Effect Editor
			m_wndEffectEditor.LoadMovieObject(NULL);
			m_wndEffectFrameViewer.m_pFrameView->SetObjectFrame(NULL);
			m_wndEffectFrameViewer.m_pFrameView->ShowWindow(SW_HIDE);
			m_wndTimeLineEditor.m_pTimeLineView->RemoveObjectHeader();
			//-- Calculate Total Time
			m_wndTimeLineEditor.m_pTimeLineView->CalculateTime();
			//-- Redraw
			m_wndTimeLineEditor.m_pTimeLineView->DrawEditor();
		}
		break;
	case WM_ESM_FRAME_DRAW_TIMELINE:
		{
			CPoint ptS, ptE;
			int nAll = m_wndTimeLineEditor.m_pTimeLineView->m_arObject.GetCount();
			CESMTimeLineObjectEditor* pObj = NULL;
			int nGap = 0;

			ESMDeleteAllDrawPos();
			for(int i = 0; i < nAll; i++)
			{
				pObj = m_wndTimeLineEditor.m_pTimeLineView->GetObj(i);
				if(pObj)
				{
					ptS = pObj->m_startPos;
					ptE = pObj->m_EndPos;

					ESMSetDrawPos(ptS,ptE);
					
				}
			}

			m_wndDSCViewer.m_pFrameSelector->DrawEditor();	
		}
		break;
	case WM_ESM_FRAME_REMOVEALL_OBJ_EDITOR:
		{
			//-- Reset Effect Editor
			m_wndEffectEditor.RemoveMovieObject();
			m_wndEffectFrameViewer.m_pFrameView->SetObjectFrame(NULL);
			m_wndEffectFrameViewer.m_pFrameView->ShowWindow(SW_HIDE);
			m_wndTimeLineEditor.m_pTimeLineView->RemoveAll();
			//-- Calculate Total Time

			m_wndTimeLineEditor.m_pTimeLineView->CalculateTime();
			//-- Redraw
			m_wndTimeLineEditor.m_pTimeLineView->DrawEditor();
		}
		break;
	//-- 2013-12-12 kjpark@esmlab.com
	//-- if converting, toolbar disenable
	case WM_ESM_FRAME_TOOLBAR_SHOW:
		{
			m_wndTimeLineEditor.SetToolbarShowNHide((BOOL)pMsg->pDest);
			m_wndMenuBar.EnableWindow((BOOL)pMsg->pDest);
			m_wndToolBarStandard.EnableWindow((BOOL)pMsg->pDest);
		}
		break;
	case WM_ESM_FRAME_UPDATE_OBJ_EDITOR:
		{
			
			
			m_wndDSCViewer.m_pFrameSelector->UpdateObject((CESMTimeLineObjectEditor*)pMsg->pDest);
			//-- Calculate Total Time
			m_wndTimeLineEditor.m_pTimeLineView->CalculateTime();
			//-- Redraw
			m_wndTimeLineEditor.m_pTimeLineView->DrawEditor();	

		}
		break;
	case WM_ESM_FRAME_CONNECTDIVECE:
		{
			//OnDSCConnection();

			ESMEvent* pMsg2 = NULL;
			pMsg2 = new ESMEvent();		
			pMsg2->message = WM_ESM_LIST_CHECK;	
			::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg2);

			if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
			{
				pMsg2 = new ESMEvent();
				pMsg2->message	= WM_ESM_LIST_CHECK;
				pMsg2->nParam1 = pMsg->nParam1;
				OnESMMsg((WPARAM)WM_ESM_NET, (LPARAM)pMsg2);
			}
		}		
		break;
	case WM_ESM_FRAME_ADJUST_EFFECT_INFO:
		{
			//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
			m_wndTimeLineEditor.m_pTimeLineView->SyncEffectInfo( m_wndEffectEditor.GetMovieObject(), (CESMTimeLineObjectEditor*)pMsg->pParam, pMsg->nParam1, pMsg->nParam2);
		}
		break;
	case WM_ESM_FRAME_PROCESSOR_PLAY:
		{
			m_wndTimeLineProcessor.DSCMoviePlay((CESMTimeLineObjectEditor*)pMsg->pDest);
		}
		break;
	case WM_ESM_FRAME_PROCESSOR_INIT:
		{
			m_wndTimeLineProcessor.DeleteProcessor();
		}
		break;
	case WM_ESM_FRAME_FOCUS:
		{
			/*if(ESMGetGPUMakeFile() == TRUE && ESMGetRemoteSkip() == TRUE)
				ESMSetViewFocusAsRemote();
			else*/
				m_wndDSCViewer.m_pFrameSelector->SetFocus();
		}
		break;
	case WM_ESM_FRAME_MUX_FINISH:
		{
			CString* strCamID = (CString*)pMsg->pDest;
			
			if(m_pMultiplexerDlg)
				m_pMultiplexerDlg->SetFinish(pMsg->nParam1, strCamID->GetString(), pMsg->nParam3);

			if (strCamID)
			{
				delete strCamID;
				strCamID = NULL;
			}
		}
		break;
	case WM_ESM_FRAME_REFEREE_MUX_FINISH:
		{
			//param1: First Movie Idx
			//param2: First Frame Idx
			//pParam: Movie Path

			CString* pstrFile = (CString*)pMsg->pParam;
			CString strPath;
			strPath.Format(_T("%s"),*pstrFile);
			ESMLog(5,strPath);
			delete pstrFile; pstrFile = NULL;

			//Send To RC
			m_pRefereeMgr->SetFinishMuxPath(strPath);
			m_pRefereeMgr->SetIndex(pMsg->nParam1,pMsg->nParam2);
			m_pRefereeMgr->SetMuxFinish(TRUE);
			m_pMultiplexerDlg->ShowWindow(SW_HIDE);
		}
		break;
	case WM_ESM_MOVIE_MULTIVIEW_FINISH:
		{
			m_wndTimeLineEditor.m_pTimeLineView->m_pViewMovie->GetRecv(pMsg->nParam1,pMsg->nParam3);
		}
		break;
	case WM_ESM_FRAME_SAVE_STILLIMAGE:
		{
			CString* pstrPath = (CString*)pMsg->pParam;
			CString str;
			str.Format(_T("%s"),*pstrPath);
			m_wndFrame.SaveCurShowImage(str);
		}
		break;
	default:
		break;
	}

	//-- delete pointer
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}	
	return ESM_ERR_NON;
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-30
//! @owner		yongmin.lee (yongmin@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
LRESULT CMainFrame::MsgNet(ESMEvent* pMsg)
{
	//-- CIC Check
	if(!pMsg)
		return ESM_ERR_MSG;
	int nStatus = 0;
	int nRemoteIp = 0;
	//-- 2013-12-13 kjpark@esmlab.com
	//-- Network Program connect Check

	switch(pMsg->message)
	{
	case WM_ESM_FRAME_TOOLBAR_SHOW:
		m_wndNetworkBar.EnableWindow((BOOL)pMsg->pDest);

		//jhhan 16-11-22 NetworkEx Status
		if(pMsg->nParam3 == ESM_NETWORK_4DP)
			m_wndNetworkExBar.EnableWindow((BOOL)pMsg->pDest);

		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
		break;

	case WM_ESM_NET_ALLDISCONNET:
		//m_wndNetwork.DisConnectAll();
		
		//jhhan 16-11-22 NetworkEx Status
		if(pMsg->nParam3 == ESM_NETWORK_4DP)
			m_wndNetworkEx.DisConnectAll();
		else
			m_wndNetwork.DisConnectAll();
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
		break;

	case WM_ESM_NET_DSCCONSTATUS:
		nRemoteIp = pMsg->nParam1;
		nStatus = m_wndDSCViewer.GetConnectStatus(nRemoteIp);
		m_wndNetwork.SetNetStatus(nRemoteIp, nStatus);

		//jhhan 16-11-22 NetworkEx Status
		m_wndNetworkEx.SetNetStatus(nRemoteIp, nStatus);

		//2014-09-11 kcd
		//화면깜빡임 발생 (의미없는 코드)
		//m_wndNetwork.m_ctrlList.LoadInfo();
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
		break;
	//-- 2014-09-08 hongsu@esmlab.com
	//-- Redraw Window 
	case WM_ESM_NET_REDRAW:
		m_wndNetwork.Invalidate(TRUE);

		//jhhan 16-11-22 NetworkEx Status
		if(pMsg->nParam3 == ESM_NETWORK_4DP)
			m_wndNetworkEx.Invalidate(TRUE);

		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
		break;
	case WM_ESM_NET_CONNECT_STATUS_CHECK_RESULT:
		{
			int ip = pMsg->nParam1;
			//ESMLog(1,_T("%d,come on~"),ip);
			//m_wndNetwork.CheckToAgentAlive(ip);

			for(int i = 0; i < m_wndNetwork.m_ConnectStatusIPList.size(); i++)
			{
				if(ip == m_wndNetwork.m_ConnectStatusIPList[i].m_Ip)
					m_wndNetwork.m_ConnectStatusIPList[i].m_count = 0;
				
				else
					m_wndNetwork.m_ConnectStatusIPList[i].m_count++;

				if(m_wndNetwork.m_ConnectStatusIPList[i].m_count>30)
				{
					ESMLog(5,_T("%d - 4DMaker DOWN"),m_wndNetwork.m_ConnectStatusIPList[i].m_Ip);
				}
			}

			//jhhan 16-11-22
			if(pMsg->nParam3 == ESM_NETWORK_4DP)
			{
				for(int i = 0; i < m_wndNetworkEx.m_ConnectStatusIPList.size(); i++)
				{
					if(ip == m_wndNetworkEx.m_ConnectStatusIPList[i].m_Ip)
						m_wndNetworkEx.m_ConnectStatusIPList[i].m_count = 0;

					else
						m_wndNetworkEx.m_ConnectStatusIPList[i].m_count++;

					if(m_wndNetworkEx.m_ConnectStatusIPList[i].m_count>30)
					{
						ESMLog(5,_T("%d - 4DMakerEx DOWN"),m_wndNetworkEx.m_ConnectStatusIPList[i].m_Ip);
					}
				}
			}
		}
		break;
	default:
		ESMEvent* pExMsg = NULL;
		pExMsg = new ESMEvent();
		memcpy(pExMsg, pMsg, sizeof(ESMEvent));

		m_wndNetwork.AddMsg(pMsg);

		//jhhan 16-11-09 Path 정보 추가 - Process
		m_wndNetworkEx.AddMsg(pExMsg);
		break;
	}

	return ESM_ERR_NON;
}


//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-05-07
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
LRESULT CMainFrame::MsgOpt(ESMEvent* pMsg)
{
	//-- CIC Check
	if(!pMsg)
		return ESM_ERR_MSG;
	switch(pMsg->message)
	{
	case WM_ESM_OPT_MAIN:
	case WM_ESM_ENV_PATH:
	case WM_ESM_ENV_4D_OPTION:
	case WM_ESM_ENV_PC_INFO:
	case WM_ESM_ENV_CAMERA_INFO:
	case WM_ESM_ENV_TEMPLATE:
	case WM_ESM_ENV_CEREMONY:
		{
			int nInitPage;
			if		( pMsg->message == WM_ESM_ENV_PATH)			nInitPage = IDD_OPTION_PATH;
			else if	( pMsg->message == WM_ESM_ENV_4D_OPTION )	nInitPage = IDD_OPTION_4DMAKER;
			else if	( pMsg->message == WM_ESM_ENV_PC_INFO )		nInitPage = IDD_OPTION_PCINFO;
			else if	( pMsg->message == WM_ESM_ENV_CAMERA_INFO)	nInitPage = IDD_OPTION_CAMINFO;
			else if	( pMsg->message == WM_ESM_ENV_TEMPLATE)		nInitPage = IDD_OPTION_TEMPLATE;
			else if	( pMsg->message == WM_ESM_ENV_CEREMONY)		nInitPage = IDD_OPTION_CEREMONY;
			else												nInitPage = IDD_OPTION_PATH;
				
			CESMOptionDlg propDlg(&m_ESMOption, nInitPage);
			//-- 2012-05-21 hongsu
			m_pOptionDlg = &propDlg;
			
			//wgkim 171222
			m_pOptionDlg->m_OptManagement.SetBackupMgr(m_pBackupMgr);
			if(propDlg.DoModal() == IDOK)
			{
				m_wndNetworkEx.SaveInfo();
			}
			m_pOptionDlg = NULL;
		}
		break;
	case WM_ESM_ENV_MUX:
		{
			if(m_pMultiplexerDlg != NULL)
			{
				m_pMultiplexerDlg->ShowWindow(SW_SHOW);
				m_pMultiplexerDlg->SetMuxInfo();
			}
			/*CESMUtilMultiplexerDlg MultiplexerDlg;
			m_pMultiplexerDlg = &MultiplexerDlg;
			MultiplexerDlg.DoModal();
			m_pMultiplexerDlg = NULL;*/
			//m_pMultiplexerDlg->ShowWindow(TRUE);
		}
		break;
	case WM_ESM_BACKUP_MUX:
		{
			CESMMuxBackUpDlg MuxBackUpDlg;
			MuxBackUpDlg.DoModal();
		}
		break;
	case WM_ESM_ENV_REMOTECTRL:
		{
			m_pRemoteCtrlDlg->ShowWindow(SW_MAXIMIZE);
		}
		break;
	case WM_ESM_ENV_REMOTE_RECORDSTATE:
		{
			MakeShortCut(pMsg->nParam1);
		}
		break;
	case WM_ESM_ENV_REMOTE_MAKING:
		{
			m_wndDSCViewer.m_pFrameSelector->SetSelectedTime(pMsg->nParam2);
			ESMLog(5,_T("Making Time: %d ms"),pMsg->nParam2);
			MakeShortCut(pMsg->nParam1);
		}
		break;
	case WM_OPTION_ADJ_SETPATH:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;
			CString* strPathData = (CString*)pMsg->pParam;
			m_pOptionDlg->SetAdjPathData(*strPathData);
			CString strConfig;
			strConfig = m_pOptionDlg->m_pOption->m_strConfig;	
			m_pOptionDlg->FileSave(strConfig);
			delete strPathData;
			m_pOptionDlg = NULL;
		}
		break;
	case WM_OPTION_MAN_SETCOPY:
		{		
			
			CFileFind file;
			vector<CString> m_arrPathList;
			CString strFolder = ESMGetPath(ESM_PATH_MOVIE_FILE);
			BOOL b = file.FindFile(strFolder + _T("\\*.*"));
			CString strFolderItem, strFileExt, strTempString;
			while(b)
			{
				b = file.FindNextFile();
				if(file.IsDirectory() && !file.IsDots())
				{
					strFolderItem = file.GetFileName();
					m_arrPathList.push_back(strFolderItem);
				}
			}
			sort(m_arrPathList.begin(), m_arrPathList.end());
		

			COPYINFO *coinfo = (COPYINFO *)pMsg->pParam;
			CString target_path;
			CString copy_path;
			int start_index;
			int end_index;
			CString folderText;
			CString copystr;
			CString targetstr;
			CESMFileOperation fo;
			
			target_path = ESMUtil::CharToCString(coinfo->targetText);
			copy_path = ESMUtil::CharToCString(coinfo->copyText);
			start_index = coinfo->start_index;
			end_index = coinfo->end_index;
			for(int i = start_index ; i<=end_index; i++)
			{
				folderText =m_arrPathList[i];	
				copystr.Format(_T("/c xcopy \\\\127.0.0.1\\"+copy_path+"\\"+folderText+" "+ target_path+"\\"+folderText +"\\*.* /e /j /c /y /k"));
				ShellExecute(NULL,_T("open"), _T("cmd.exe"), copystr,  NULL, SW_HIDE);

			}

		}
		break;
	case WM_OPTION_MAN_SETDEL:
		{
			CFileFind file;
			vector<CString> m_arrPathList;
			CString strFolder = ESMGetPath(ESM_PATH_MOVIE_FILE);
			BOOL b = file.FindFile(strFolder + _T("\\*.*"));
			CString strFolderItem, strFileExt, strTempString;
			while(b)
			{
				b = file.FindNextFile();
				if(file.IsDirectory() && !file.IsDots())
				{
					strFolderItem = file.GetFileName();
					m_arrPathList.push_back(strFolderItem);
				}
			}
			sort(m_arrPathList.begin(), m_arrPathList.end());
		

			COPYINFO *coinfo = (COPYINFO *)pMsg->pParam;
			CString target_path;
			CString copy_path;
			int start_index;
			int end_index;
			CString folderText;
			CString copystr;
			CString targetstr;
			CESMFileOperation fo;

			
			target_path = ESMUtil::CharToCString(coinfo->targetText);
			copy_path = ESMUtil::CharToCString(coinfo->copyText);
			start_index = coinfo->start_index;
			end_index = coinfo->end_index;

			if(start_index == -1 && end_index == -1)
			{
				copystr.Format(_T("/c rmdir /s /q \\\\127.0.0.1\\"+copy_path +"& md \\\\127.0.0.1\\"+copy_path));
				ShellExecute(NULL,_T("open"), _T("cmd.exe"), copystr,  NULL, SW_NORMAL);

				fo.CreateFolder(copystr);

				break;
			}
			for(int i = start_index ; i<=end_index; i++)
			{
				folderText =m_arrPathList[i];
				copystr.Format(_T("/c rmdir /s /q \\\\127.0.0.1\\"+copy_path+"\\"+folderText));
				ShellExecute(NULL,_T("open"), _T("cmd.exe"), copystr,  NULL, SW_NORMAL);
			}

		  }
		  break;
	case WM_OPTION_SETPATH:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;
			CString* strPathData = (CString*)pMsg->pParam;
			m_pOptionDlg->SetPathData(pMsg->nParam1, *strPathData);

			ESMLog(5, _T("WM_OPTION_SETPATH : [%d] - %s"), pMsg->nParam1, *strPathData);

			CString strConfig;
			strConfig = m_pOptionDlg->m_pOption->m_strConfig;	
			m_pOptionDlg->FileSave(strConfig);
			delete strPathData;
			m_pOptionDlg = NULL;
		}
		break;
	case WM_BACKUP_STRING:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;
			CString* strPathData = (CString*)pMsg->pParam;
			m_pOptionDlg->SetBackupString(pMsg->nParam1, *strPathData);			

			ESMLog(5, _T("WM_BACKUP_STRING : [%d] - %s"), pMsg->nParam1, *strPathData);

			CString strConfig;
			strConfig = m_pOptionDlg->m_pOption->m_strConfig;	
			m_pOptionDlg->FileSave(strConfig);
			delete strPathData;
			m_pOptionDlg = NULL;
		}
		break;
	case WM_OPTION_SETSYNC:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;
			m_pOptionDlg->m_pOption->m_4DOpt.nPCSyncTime = pMsg->nParam1;
			m_pOptionDlg->m_pOption->m_4DOpt.nDSCSyncTime = pMsg->nParam2;
			m_pOptionDlg->m_pOption->m_4DOpt.nDSCSyncWaitTime = pMsg->nParam3;
			m_pOptionDlg = NULL;
		}
		break;
	case WM_OPTION_SETETC:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;
			m_pOptionDlg->m_pOption->m_4DOpt.nDSCSyncCnt = pMsg->nParam1;
			m_pOptionDlg->m_pOption->m_4DOpt.nMovieSaveLocation = pMsg->nParam2;
			m_pOptionDlg->m_pOption->m_4DOpt.bRemoteSkip = pMsg->nParam3;
			m_pOptionDlg = NULL;
		}
		break;
	case WM_OPTION_SETBASE:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;
			CString* strPathData = (CString*)pMsg->pParam;
			m_pOptionDlg->m_pOption->m_4DOpt.nWaitSaveDSC = _ttoi(*strPathData);

			m_pOptionDlg->m_pOption->m_4DOpt.nMovieSaveLocation = pMsg->nParam1;
			m_pOptionDlg->m_pOption->m_4DOpt.nSensorOnTime = pMsg->nParam2;
			m_pOptionDlg->m_pOption->m_4DOpt.nDeleteCnt = pMsg->nParam3;

			CString strConfig;
			strConfig = m_pOptionDlg->m_pOption->m_strConfig;	
			m_pOptionDlg->FileSave(strConfig);
			delete strPathData;
			m_pOptionDlg = NULL;
		}
		break;
	case WM_OPTION_MAKE_TYPE:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;

			m_pOptionDlg->m_pOption->m_4DOpt.bGPUMakeFile = pMsg->nParam1;
			m_pOptionDlg->m_pOption->m_4DOpt.bAJAOnOff	  = pMsg->nParam2;
			m_pOptionDlg->m_pOption->m_4DOpt.bEnableCopy  = pMsg->nParam3;
			CString strConfig;
			strConfig = m_pOptionDlg->m_pOption->m_strConfig;	
			m_pOptionDlg->FileSave(strConfig);
		}
		break;
	case WM_OPTION_MOVIESIZE:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;

			m_pOptionDlg->m_pOption->m_4DOpt.bEnableVMCC = pMsg->nParam1;
			m_pOptionDlg->m_pOption->m_4DOpt.bUHDtoFHD = pMsg->nParam2;
			m_pOptionDlg->m_pOption->m_4DOpt.bReverseMovie = pMsg->nParam3;

			CString strConfig;
			strConfig = m_pOptionDlg->m_pOption->m_strConfig;	
			m_pOptionDlg->FileSave(strConfig);
		}
		break;
		//wgkim
	case WM_OPTION_LIGHT_WEIGHT:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;

			m_pOptionDlg->m_pOption->m_4DOpt.bLightWeight = pMsg->nParam1;			
			m_pOptionDlg->m_pOption->m_4DOpt.n4DPMethod   = pMsg->nParam2;
			m_pOptionDlg->m_pOption->m_4DOpt.bRefereeRead  = pMsg->nParam3;
			if(pMsg->nParam3)
				ESMLog(5,_T("[RefereeRead] Start"));

			CString strConfig;
			strConfig = m_pOptionDlg->m_pOption->m_strConfig;	
			m_pOptionDlg->FileSave(strConfig);			
		}
		break;
	case WM_OPTION_SET_FRAMERATE:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;

			m_pOptionDlg->m_pOption->m_4DOpt.nSetFrameRateIndex= pMsg->nParam1;
			m_pOptionDlg->m_pOption->m_4DOpt.bRTSP			   = pMsg->nParam2;//180927 hjcho
			CString strConfig;
			strConfig = m_pOptionDlg->m_pOption->m_strConfig;	
			m_pOptionDlg->FileSave(strConfig);
			ESMLog(5, _T("Encoding FrameRate Index: %d"), m_pOptionDlg->m_pOption->m_4DOpt.nSetFrameRateIndex);
		}
		break;
	case WM_OPTION_RECORD_FILE_SPLIT:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;

			m_pOptionDlg->m_pOption->m_4DOpt.bRecordFileSplit = pMsg->nParam1;

			CString strConfig;
			strConfig = m_pOptionDlg->m_pOption->m_strConfig;	
			m_pOptionDlg->FileSave(strConfig);

			ESMSetRecordFileSplit(pMsg->nParam1);
			ESMLog(5, _T("ESMGetRecordFileSplit : %d"), ESMGetRecordFileSplit());
		}
		break;
	case WM_OPTION_VIEW_INFO:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;

			m_pOptionDlg->m_pOption->m_4DOpt.bViewInfo		= pMsg->nParam1;			
			m_pOptionDlg->m_pOption->m_4DOpt.bHorizonAdj	= pMsg->nParam2;
			//jhhan 180828
			ESMSetValue(ESM_VALUE_STORED_TIME, pMsg->nParam3);
			ESMLog(5, _T("OPT_STORED_TIME : %d"), pMsg->nParam3);

			CString strConfig;
			strConfig = m_pOptionDlg->m_pOption->m_strConfig;	
			m_pOptionDlg->FileSave(strConfig);
		}
		break;
	case WM_OPTION_SET_AUTOADJUST_KZONE:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;

			m_pOptionDlg->m_pOption->m_4DOpt.bAutoAdjustKZone = pMsg->nParam1;

			CString strConfig;
			strConfig = m_pOptionDlg->m_pOption->m_strConfig;
			m_pOptionDlg->FileSave(strConfig);
		}
		break;

	//CMiLRe 20160106 record load 단축키
	case WM_OPTION_LOAD_RECORD:
		{			
			OnLoadRecordProfile();
		}
		break;
	case WM_ESM_REFEREE_MUX:
		{
			if(m_pMultiplexerDlg != NULL)
			{
				m_pMultiplexerDlg->ShowWindow(SW_SHOW);
				m_pMultiplexerDlg->SetRefereeInfo(pMsg->nParam1,pMsg->nParam2,pMsg->nParam3);
			}
		}
		break;
	case WM_ESM_ENV_RTSPSTATEVIEW:
		{
			if(m_pRTSPStateViewDlg != NULL)
			{
				if(ESMGetValue(ESM_VALUE_RTSP) == TRUE)
				{
					m_pRTSPStateViewDlg->SetProcessRun(TRUE);
					m_pRTSPStateViewDlg->ShowWindow(SW_SHOW);
				}
			}
		}
		break;
#ifdef _4DMODEL
		//-- 2015-02-15 cygil@esmlab.com
		//-- Add picture coutdown info 전송
	case WM_OPTION_SETCOUNTDOWN:
		{
			CESMOptionDlg propDlg(&m_ESMOption);
			m_pOptionDlg = &propDlg;
			m_pOptionDlg->m_pOption->m_4DOpt.nCountdownLastIP = pMsg->nParam1;
			CString strConfig;
			strConfig = m_pOptionDlg->m_pOption->m_strConfig;	
			m_pOptionDlg->FileSave(strConfig);
			m_pOptionDlg = NULL;
		}
		break;
#endif
	default:
		break;
	}

	//-- delete pointer
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}	
	return ESM_ERR_NON;
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-06-16
//! @owner		HyoJae Jang
//! @return			
//! @revision		
//------------------------------------------------------------------------------
LRESULT CMainFrame::MsgMovie(ESMEvent* pMsg)
{
	//-- CIC Check
	if(!pMsg)
		return ESM_ERR_MSG;
	switch(pMsg->message)
	{
	case WM_MAKEMOVIE_MSG:
		{
			switch(pMsg->nParam1)
			{
			case ESM_ERR_MSG_FFMPEG_INIT:
				ESMLog(5, _T("FFMPEG Init Error!!"));
				break;
			}
		}
		break;
	case WM_ESM_MOVIE_MAKINGSTOP:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->nOpt = pMsg->nParam1;
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_MAKINGSTOP;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;

	case WM_ESM_MOVIE_SET_MOVIECOUNT:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->nOpt = pMsg->nParam1;
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_SETMOIVECOUNT;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_MOVIE_MAKING_INFO:
		{
			making_info makingInfo;
			memcpy(&makingInfo, (void*)pMsg->pParam, sizeof(making_info));
			ESMSetMakingInfo(makingInfo);

			/*CString strLog,strFirstDSC,strEndDSC;
			strFirstDSC = (LPCSTR)makingInfo.strFirstDSC;
			strEndDSC = (LPCSTR)makingInfo.strEndDSC;
			strLog.Format(_T("#Start CAM %s End CAM %s"),strFirstDSC, strEndDSC);
			ESMLog(1, strLog);
			
			for(int i = 0; i < makingInfo.nProcessorCnt; i++)
			{
				strLog.Format(_T("#4DP IP %d"),makingInfo.nProcessorIP[i]);
				ESMLog(1, strLog);
			}*/
		}
		break;
	case WM_ESM_MOVIE_SET_MAKING_INFO:
		{

			making_info makingInfo;
			makingInfo.nProcessorCnt = 0;

			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);
			CDSCItem* pItem = (CDSCItem*)arDSCList.GetAt(0);
			memset(makingInfo.strFirstDSC, 0 ,MAX_PATH);
			strcpy(makingInfo.strFirstDSC,CT2A(pItem->GetDeviceDSCID())); //CString to char*

			pItem = (CDSCItem*)arDSCList.GetAt(arDSCList.GetCount()-1);
			memset(makingInfo.strEndDSC, 0 ,MAX_PATH);
			strcpy(makingInfo.strEndDSC,CT2A(pItem->GetDeviceDSCID()));


			for(int i = 0; i < m_wndNetwork.m_arRCServerList.GetCount(); i++)
			{
				CESMRCManager* pRCMgr = (CESMRCManager*)m_wndNetwork.m_arRCServerList.GetAt(i);
				if (pRCMgr->m_bConnected)
				{
					CDSCGroup* pGroup = new CDSCGroup(pRCMgr->GetID());
					//if(pGroup->GetRCMgr()->m_bGPUUse)
					if(pRCMgr->m_bGPUUse)
					{
						//jhhan 170322
						ESMLog(5, _T("[4DP] WM_ESM_MOVIE_SET_MAKING_INFO : %d"), pGroup->m_nIP);
						
						makingInfo.nProcessorIP[makingInfo.nProcessorCnt] = pGroup->m_nIP;
						makingInfo.nProcessorCnt++;
					}

					if(pGroup)
						delete pGroup;
				}
			}
			
			ESMSetMakingInfo(makingInfo);
		}
		break;
	case WM_ESM_MOVIE_DELETE_DSC:
		{

			//ESMGetDeleteDSC();//??삭제 ->jhhan 16-11-04
			CString *str = (CString *)pMsg->pParam;

			CString DSCStr = str->GetBuffer(0);
			CString strcount = DSCStr;
			str->ReleaseBuffer();

			int count = strcount.Replace(_T(","),_T(""));
			vector<CString> _DSCDeleteList;
			CString DSCID;
			ESMLog(5, _T("Sync Delete : Execute"));
			for(int i = 0 ; i < count+1; i++)
			{
				AfxExtractSubString( DSCID, DSCStr, i, ',');
				_DSCDeleteList.push_back(DSCID);
				ESMLog(5, _T("Sync Delete [%d] : %s"), i+1, DSCID);
			}

			ESMSetDeleteDSCList(_DSCDeleteList);
			_DSCDeleteList.clear();	

		}
		break;
	case WM_ESM_MOVIE_SET_FPS:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->nParam1 = pMsg->nParam1;
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_SET_FPS;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;

	case WM_ESM_MOVIE_SET_IMAGESIZE:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->nParam1 = pMsg->nParam1;
			pMovMsg->nParam2 = pMsg->nParam2;
			pMovMsg->nParam3 = pMsg->nParam3;
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_SET_IMAGESIZE;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;

		//wgkim 170727
	case WM_ESM_MOVIE_SET_LIGHT_WEIGHT:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->nParam1 = pMsg->nParam1;			
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_SET_LIGHT_WEIGHT;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
		//wgkim 180629
	case WM_ESM_MOVIE_SET_FRAME_RATE:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->nParam1 = pMsg->nParam1;			
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_SET_FRMAERATE;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_MOVIE_END_MOVIE_MAKE:
		{
			//std::mt19937_64 rng1( 3244 ); 
			//std::uniform_int_distribution<__int64> dist1(19000, 21000);

			//int nGap = dist1(rng1);


			int nGap = 0;
			nGap = (rand() % 1000)+13499;
			
			m_TimeVieDlg->SetEndTime(nGap);
			ESMSetStartMakeTime(m_TimeVieDlg->m_nStartTime, nGap);
			/*while(1)
			{
				if((GetTickCount() - m_TimeVieDlg->m_nStartTime) > nGap)
					break;
				Sleep(100);
			}*/

			
		}
		break;
	case WM_ESM_MOVIE_START_MOVIE_MAKE:
		{
			m_TimeVieDlg->InitTime();
			if(ESMGetValue(ESM_VALUE_TEST_MODE))
			{
				m_TimeVieDlg->SetSeq1(_T("Make Movie Time..."));
				m_TimeVieDlg->ShowWindow(TRUE);
			}
		}
		break;
	case WM_ESM_MOVIE_SET_ADJUSTINFO:
		{
			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);

			ESMEvent* pAdjMsg = new ESMEvent();
			pAdjMsg->message	= WM_ESM_MOVIE_SET_ADJUSTINFO;
			pAdjMsg->nParam1	= arDSCList.GetSize();
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pAdjMsg);

			ESMEvent* pMarginMsg = new ESMEvent();
			pMarginMsg->message	= WM_ESM_MOVIE_SET_MARGIN;
			pMarginMsg->nParam1	= pMsg->nParam1;
			pMarginMsg->nParam2	= pMsg->nParam2;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMarginMsg);

			ESMEvent* pAJAMsg = new ESMEvent();
			pAJAMsg->message	= F4DC_ADJUST_SEND;
			pAJAMsg->nParam1	= arDSCList.GetSize();
			ESMSendAJANetwork(pAJAMsg);

			//wgkim 181002
			if(m_pTemplateMgr != NULL)
			{
				m_pTemplateMgr->ClearAdjInfo();
				m_pTemplateMgr->DscClear();
			}
			if(m_pAutoAdjustMgr != NULL)
			{
				m_pAutoAdjustMgr->ClearAdjInfo();
				m_pAutoAdjustMgr->DscClear();
			}

			CDSCItem* pItem = NULL;
			for( int i =0 ;i < arDSCList.GetSize(); i++)
			{
				pItem = (CDSCItem*)arDSCList.GetAt(i);

				stAdjustInfo* adjInfo = new stAdjustInfo;
				adjInfo->AdjAngle = pItem->GetDSCAdj(DSC_ADJ_A);
				adjInfo->AdjSize = pItem->GetDSCAdj(DSC_ADJ_SCALE);
				adjInfo->AdjptRotate.x = pItem->GetDSCAdj(DSC_ADJ_RX);
				adjInfo->AdjptRotate.y = pItem->GetDSCAdj(DSC_ADJ_RY);
				adjInfo->AdjMove.x = pItem->GetDSCAdj(DSC_ADJ_X);
				adjInfo->AdjMove.y = pItem->GetDSCAdj(DSC_ADJ_Y);
				adjInfo->strDSC = pItem->GetDeviceDSCID();
				adjInfo->nHeight = pItem->GetHeight();
				adjInfo->nWidth = pItem->GetWidth();
				adjInfo->stMargin = pItem->GetMargin();

				//wgkim 190527
				adjInfo->rtMargin = pItem->GetMarginRect();

				TCHAR* strMovieID = new TCHAR[15];
				_tcscpy(strMovieID, pItem->GetDeviceDSCID());
				ESMMovieMsg* pMovMsg = new ESMMovieMsg;
				pMovMsg->nMsg = ESM_MOVIE_MANAGER_ADJ_INFO;
				pMovMsg->pValue1 = adjInfo;
				pMovMsg->pValue2 = strMovieID;
				m_pESMMovieMgr->AddEvent(pMovMsg);

				//wgkim 180920
				if(m_pTemplateMgr != NULL)
				{
					m_pTemplateMgr->AddDscInfo(pItem->GetDeviceDSCID(), pItem->m_strInfo[DSC_INFO_LOCATION]);
					m_pTemplateMgr->AddAdjInfo(*adjInfo);
				}

				//wgkim 181002
				if(m_pAutoAdjustMgr != NULL)
				{
					m_pAutoAdjustMgr->AddDscInfo(pItem->GetDeviceDSCID(), pItem->m_strInfo[DSC_INFO_LOCATION]);
					m_pAutoAdjustMgr->AddAdjInfo(*adjInfo);
				}
			}

			// Margin 정보 전송
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_SET_MARGIN;
			pMovMsg->nParam1 = pMsg->nParam1;
			pMovMsg->nParam2 = pMsg->nParam2;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_MOVIE_SET_ADJUSTINFO_FROM_SERVER:
		{
// 			CObArray arDSCList;
// 			ESMGetDSCList(&arDSCList);
// 			CDSCItem* pItem = NULL;
//			ESMLog(5,_T("Receive Adjust Data"));

			stAdjustInfo* adjInfo = (stAdjustInfo*)pMsg->pParam;
// 			int idx = pMsg->nParam1;
// 			pItem = (CDSCItem*)arDSCList.GetAt(idx);
// 			TCHAR* strMovieID = new TCHAR[15];
// 			_tcscpy(strMovieID, pItem->GetDeviceDSCID());
			
			if(ESMGetValue(ESM_VALUE_RTSP) == TRUE)
				m_pMovieRTMovie->SetAdjustData(adjInfo);

			switch(ESMGetValue(ESM_VALUE_4DPMETHOD))
			{
			case 0:
				//m_pMovieRTThread->SetAdjustData(adjInfo);
				break;
			case 1:
				m_pMovieRTSend->SetAdjustData(adjInfo);
				break;
			case 2:
				m_pMovieRTSend60p->SetAdjustData(adjInfo);
				break;
			}
			/*if(ESMGetValue(ESM_VALUE_4DPMETHOD) == FALSE)
				m_pMovieRTSend->SetAdjustData(adjInfo);
			else
				m_pMovieRTThread->SetAdjustData(adjInfo);*/

			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_ADJ_INFO;
			pMovMsg->pValue1 = adjInfo;
			//ESMLog(5,_T("Set Adjust Finish"));
			//pMovMsg->pValue2 = strMovieID;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_MOVIE_SET_MARGIN_FROM_SERVER:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_SET_MARGIN;
			pMovMsg->nParam1 = pMsg->nParam1;
			pMovMsg->nParam2 = pMsg->nParam2;

			switch(ESMGetValue(ESM_VALUE_4DPMETHOD))
			{
			case 0:
				//m_pMovieRTThread->SetMargin((int)pMsg->nParam1,(int)pMsg->nParam2);
				break;
			case 1:
#if 0
				m_pMovieRTSend->SetMargin((int)pMsg->nParam1,(int)pMsg->nParam2);
#else
				m_pMovieRTMovie->SetMargin((int)pMsg->nParam1,(int)pMsg->nParam2);
#endif
				break;
			case 2:
				m_pMovieRTSend60p->SetMargin((int)pMsg->nParam1,(int)pMsg->nParam2);
				break;
			}
			/*if(ESMGetValue(ESM_VALUE_4DPMETHOD) == FALSE)
				m_pMovieRTSend->SetMargin((int)pMsg->nParam1,(int)pMsg->nParam2);
			else
				m_pMovieRTThread->SetMargin((int)pMsg->nParam1,(int)pMsg->nParam2);*/

			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_MOVIE_SET_MOVIEDATA:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;			
			pMovMsg->pValue1 = (PVOID)pMsg->pParam;
			pMovMsg->pValue2 = (PVOID)pMsg->pDest;
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_MOVIELOAD;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_MOVIE_MAKEMOVIE:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->pValue1 = (PVOID)pMsg->pParam;
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_MOVIEMAKE;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_MOVIE_MAKEFRAME_CLIENT_ADJUST:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->pValue1 = (PVOID)pMsg->pParam;
			pMovMsg->nParam1 = pMsg->nParam1;
			pMovMsg->nParam2 = pMsg->nParam2;
			pMovMsg->nParam3 = pMsg->nParam3;
			pMovMsg->pValue2	= (PVOID)pMsg->pDest;
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_MAKEFRAME_ADJUST;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_MOVIE_REQUEST_DATA:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->pValue1 = (PVOID)pMsg->pParam;
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_REQUEST_DATA;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_MOVIE_MAKEFRAME_CLIENT:
		{
			//joonho.kim 180609 Making Stop FileTransfer
			StopFileTransfer();


			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->pValue1 = (PVOID)pMsg->pParam;
			pMovMsg->nParam1 = pMsg->nParam1;
			pMovMsg->nParam2 = pMsg->nParam2;
			pMovMsg->nParam3 = pMsg->nParam3;
			pMovMsg->pValue2	= (PVOID)pMsg->pDest;
			pMovMsg->pValue3	= (PVOID)pMsg->pParent;

			//if(ESMGetValue(ESM_VALUE_AJAONOFF))
			//{
			//	pMovMsg->nParam4 = TRUE;
			//	ESMLog(5,_T("Adapt AJA Option"));
			//}
			//else
			//	pMovMsg->nParam4 = FALSE;

			//pMovMsg->nParam4 = TRUE;//Always Time base movie save
			//pMovMsg->nParam4 = ESMGetValue(ESM_VALUE_ENABLE_4DPCOPY);
			pMovMsg->nParam4 = FALSE;

			pMovMsg->nMsg = ESM_MOVIE_MANAGER_MAKEFRAME;
			SetInfoToMovieMgr();
			m_pESMMovieMgr->AddEvent(pMovMsg);
						
		}
		break;
	case WM_ESM_MOVIE_MAKEFRAME_CLIENT_REVERSE:
		{
			//joonho.kim 180609 Making Stop FileTransfer
			StopFileTransfer();

			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->pValue1 = (PVOID)pMsg->pParam;
			pMovMsg->nParam1 = pMsg->nParam1;
			pMovMsg->nParam2 = pMsg->nParam2;
			pMovMsg->nParam3 = pMsg->nParam3;
			pMovMsg->pValue2	= (PVOID)pMsg->pDest;
			pMovMsg->pValue3	= (PVOID)pMsg->pParent;

			//if(ESMGetValue(ESM_VALUE_AJAONOFF))
			//{
			//	pMovMsg->nParam4 = TRUE;
			//	ESMLog(5,_T("Adapt AJA Option"));
			//}
			//else
			//	pMovMsg->nParam4 = FALSE;
			
			//pMovMsg->nParam4 = TRUE;//Always Time base movie save
			//pMovMsg->nParam4 = ESMGetValue(ESM_VALUE_ENABLE_4DPCOPY);
			pMovMsg->nParam4 = FALSE;

			pMovMsg->nMsg = ESM_MOVIE_MANAGER_MAKEFRAME_REVERSE;
			SetInfoToMovieMgr();
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_MOVIE_MAKEFRAME_CLIENT_LOCAL:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->pValue1 = (PVOID)pMsg->pParam;
			pMovMsg->nParam1 = pMsg->nParam1;
			pMovMsg->nParam2 = pMsg->nParam2;
			pMovMsg->nParam3 = pMsg->nParam3;
			pMovMsg->pValue2	= (PVOID)pMsg->pDest;
			pMovMsg->pValue3	= (PVOID)pMsg->pParent;

			//if(ESMGetValue(ESM_VALUE_AJAONOFF))
			//{
			//	pMovMsg->nParam4 = TRUE;
			//	ESMLog(5,_T("Adapt AJA Option"));
			//}
			//else
			//	pMovMsg->nParam4 = FALSE;

			//pMovMsg->nParam4 = TRUE;//Always Time base movie save
			//pMovMsg->nParam4 = ESMGetValue(ESM_VALUE_ENABLE_4DPCOPY);
			pMovMsg->nParam4 = FALSE;

			pMovMsg->nMsg = ESM_MOVIE_MANAGER_MAKEFRAME_LOCAL;
			SetInfoToMovieMgr();
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_MOVIE_MAKEFRAME_CLIENT_REVERSE_LOCAL:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->pValue1 = (PVOID)pMsg->pParam;
			pMovMsg->nParam1 = pMsg->nParam1;
			pMovMsg->nParam2 = pMsg->nParam2;
			pMovMsg->nParam3 = pMsg->nParam3;
			pMovMsg->pValue2	= (PVOID)pMsg->pDest;
			pMovMsg->pValue3	= (PVOID)pMsg->pParent;

			//if(ESMGetValue(ESM_VALUE_AJAONOFF))
			//{
			//	pMovMsg->nParam4 = TRUE;
			//	ESMLog(5,_T("Adapt AJA Option"));
			//}
			//else
			//	pMovMsg->nParam4 = FALSE;

			//pMovMsg->nParam4 = TRUE;//Always Time base movie save
			//pMovMsg->nParam4 = ESMGetValue(ESM_VALUE_ENABLE_4DPCOPY);
			pMovMsg->nParam4 = FALSE;

			pMovMsg->nMsg = ESM_MOVIE_MANAGER_MAKEFRAME_REVERSE_LOCAL;
			SetInfoToMovieMgr();
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_MOVIE_MAKE_ADJUST:
		{
			m_pOptionDlg->m_OptAdjustMovie.SaveProp(m_pOptionDlg->m_pOption->m_AdjustMovie);
			m_wndTimeLineEditor.OnDSCADJMakeMovie();
		}
		break;
	case WM_ESM_MOVIE_MAKE_ADJUST_COUNT:
		{
			if(m_pOptionDlg)
				m_pOptionDlg->m_OptAdjustMovie.SetCount(pMsg->nParam1, pMsg->nParam2);
		}
		break;
	case WM_ESM_MOVIE_MAKE_WAIT_END:
		if(m_pMovieThreadMgr)
			m_pMovieThreadMgr->m_bWait = FALSE;
		break;
	case WM_ESM_MOVIE_MAKE_THREAD_RUN:
		m_wndTimeLineEditor.VmccTamplate(pMsg->nParam2, pMsg->nParam3, pMsg->nParam1);
		m_wndTimeLineEditor.OnDSCMakeMovie();
		m_pImgLoader->AllClearImageList();
		break;
	case WM_ESM_MOVIE_MAKE_THREAD:
		{
			//Make Msg Add
			if(m_pMovieThreadMgr)
			{
				ESMEvent* pMovMsg = new ESMEvent;
				pMovMsg->message = WM_ESM_MOVIE_MAKE_ARRAY_ADD;
				pMovMsg->nParam1 = pMsg->nParam1;   //VMCC Tamplate
				pMovMsg->nParam2 = pMsg->nParam2;
				pMovMsg->nParam3 = pMsg->nParam3;
				pMovMsg->pParam = (LPARAM)this;
				m_pMovieThreadMgr->AddMsg(pMovMsg);
			}

			/*m_wndTimeLineEditor.VmccTamplate(pMsg->nParam1);
			m_wndTimeLineEditor.OnDSCMakeMovie();
			m_pImgLoader->AllClearImageList();*/
		}
		break;
	case WM_ESM_MOVIE_SAVE_ADJUST_RUN:
		{
			ESMAdjustMgrDlg* pAdjustMgrDlg;
			pAdjustMgrDlg = (ESMAdjustMgrDlg*)pMsg->pParam;
			vector<DscAdjustInfo*>* pArrDscInfo = NULL;
			pAdjustMgrDlg->GetDscInfo(&pArrDscInfo);

			m_wndDSCViewer.SetDscAdjustData(pArrDscInfo);

			// Margin 계산
			m_wndDSCViewer.SetMargin(pArrDscInfo);

			int nMarginX = m_wndDSCViewer.GetMarginX();
			int nMarginY = m_wndDSCViewer.GetMarginY();

			//-- 2014-07-15 hjjang
			//-- Set Adjust Info
			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_ESM_MOVIE_SET_ADJUSTINFO;
			pMsg->nParam1 = nMarginX;
			pMsg->nParam2 = nMarginY;
			::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);

			OnSaveAdjustProfile();
		}
		break;
	case WM_ESM_MOVIE_SAVE_ADJUST:
		{
			m_wndDSCViewer.SetMarginX(pMsg->nParam1);
			m_wndDSCViewer.SetMarginY(pMsg->nParam2);
			OnSaveAdjustProfile2nd((void*)pMsg->pParam);
		}
		break;
	case WM_ESM_MOVIE_INFORESET:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_INFORESET;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	//-- 2014-09-17 hongsu@esmlab.com
	//-- VALID FRAME INFO 
	case WM_ESM_MOVIE_VALIDFRAME:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->nMsg		= ESM_MOVIE_MANAGER_VALIDFRAME;
			pMovMsg->pValue1	= (PVOID)pMsg->pParam;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_MOVIE_VALIDRESET:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->nMsg		= ESM_MOVIE_MANAGER_VALIDRESET;
			m_pESMMovieMgr->AddEvent(pMovMsg);

			//-- 2014-9-18 hongsu@esmlab.com
			//-- Reset Valid Data on DSCMgr
			m_wndDSCViewer.m_pDSCMgr->ResetErrorFrame();
		}
		break;
	case WM_ESM_FRAME_LOAD_TIME:
		{
			ESMLog(1, _T("### Load Frame : %d ms , Copy Time %d ms [%d]"), pMsg->nParam3, pMsg->nParam2, pMsg->nParam1);
		}
		break;
	case WM_ESM_MOVIE_MAKE_FINISH:
		{ 
			CTimeLineView* pView = (CTimeLineView*)m_wndTimeLineEditor.m_pTimeLineView;

			int nIdx = pMsg->nParam1;
			CESMTimeLineObjectEditor* pObj = pView->GetObj(nIdx);
			TRACE(_T("Finish Movie [%d]: %d"), pMsg->nParam2, nIdx);
			ESMLog(1, _T("Finish Movie [%d]: %d"), pMsg->nParam2, nIdx);

			if( pObj == NULL)
				break;

			if ( --pObj->m_nMovieNotFinished == 0 )
				pObj->SetMakeMovie(TRUE);

			if ( pObj->m_nMovieNotFinished < 0 )
			{
				pObj->SetMakeMovie(TRUE);
				TRACE(_T("Make Movie Error"));
			}
		}
		break;
	case WM_ESM_MOVIE_MAKE_FINISH_FROM_MOVIEMGR:
		{
			//Parsing TimeLineObject Index
			TCHAR* strTp = (TCHAR*)pMsg->pParam;
			CString strPathData;
			strPathData.Format(_T("%s"), strTp);
			delete[] strTp;

			int sepCount = ESMUtil::GetFindCharCount(strPathData, '\\'); 
			CString strIdx;
			AfxExtractSubString(strIdx, strPathData, sepCount, '\\');
			AfxExtractSubString(strIdx, strIdx, 0, '_');
			int nIdx = _ttoi(strIdx);

			if (ESMGetExecuteMode())
			{
				// Send To Server
				ESMEvent* pMsg = new ESMEvent();
				pMsg->nParam1 = nIdx;				// TimeLineObject Index
				pMsg->nParam2 = ESMGetIDfromIP(ESMGetLocalIP());
				pMsg->message = WM_ESM_NET_MAKEMOVIE_FINISH;
				::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_NET, (LPARAM)pMsg);
			}
			else
			{
				ESMEvent* pMsg = new ESMEvent();
				pMsg->nParam1 = nIdx;				// TimeLineObject Index
				pMsg->message = WM_ESM_MOVIE_MAKE_FINISH;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
			}
		}
		break;
	case WM_ESM_MOVIE_START_FILETRANSFER:
		{
			StartFileTransfer();
		}
		break;
	case WM_ESM_MOVIE_VALIDSKIP_FROM_MOVIEMGR:
		{
			CString strId = (TCHAR*)pMsg->pParam;
			int nValidSkip = 0;
			TCHAR* strTp = (TCHAR*)pMsg->pParam;
			strId.Format(_T("%s"), strTp);
			delete[] strTp;
			nValidSkip = pMsg->nParam1;
			ESMLog(0, _T("[Valid Skip Frame] DscID [%s], Index[%d]"), strId, nValidSkip);

			if (ESMGetExecuteMode())
			{
				ESMEvent* pNewMsg	= new ESMEvent();
				CObArray pItemList;
				ESMGetDSCList(&pItemList);
				CDSCItem* pDSCItem = NULL;
				for( int i = 0 ;i < pItemList.GetCount(); i++)
				{
					pDSCItem = (CDSCItem*)pItemList.GetAt(i);
					if(pDSCItem->GetDeviceDSCID() == strId)
						break;
				}

				pNewMsg->pDest		= (LPARAM)pDSCItem;
				pNewMsg->nParam1	= nValidSkip;
				pNewMsg->message	= WM_ESM_NET_VALIDSKIP;
				::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);
			}

		}
		break;
	case WM_ESM_PROCESSOR_INFO:						//Processor UI
		{
#ifndef _SERVER_SAVE		//jhhan 17-01-16 Server Save
			m_wndTimeLineEditor.m_pTimeLineView->SetClientId(pMsg->nParam3, pMsg->nParam1);
#endif
			/*int nId = pMsg->nParam1;
			int nFrameCnt = pMsg->nParam2;
			int nMakeFile = pMsg->nParam3;
			ESMLog(5, _T("WM_ESM_PROCESSOR_INFO : IP(id) - [%d], FRAME - [%d], FILE - [%d]"), nId, nFrameCnt, nMakeFile);*/
			m_wndTimeLineProcessor.MakeProcessor(pMsg);
		}
		break;
	case WM_ESM_PROCESSOR_INFO_MAKE:
		{
			int nId = pMsg->nParam1;
			int nFrameCnt = pMsg->nParam2;
			int nMakeFile = pMsg->nParam3;
			ESMLog(5, _T("WM_ESM_PROCESSOR_INFO_MAKE : IP(id) - [%d], FRAME - [%d], FILE - [%d]"), nId, nFrameCnt, nMakeFile);
			m_wndTimeLineProcessor.UpdateProcessor(pMsg);
		}
		break;
	case WM_ESM_PROCESSOR_INFO_FAIL:
		{
			/*int nId = pMsg->nParam1;
			int nFrameCnt = pMsg->nParam2;
			int nMakeFile = pMsg->nParam3;
			ESMLog(5, _T("WM_ESM_PROCESSOR_INFO_FAIL : IP(id) - [%d], FRAME - [%d], FILE - [%d]"), nId, nFrameCnt, nMakeFile);*/
			m_wndTimeLineProcessor.UpdateFailProcessor(pMsg);
		}
		break;
	case WM_ESM_MOVIELIST_ADDFILE:
		{
			CString *pstrListUp = (CString*)pMsg->pParam;
			CString *pstrListPath = (CString*)pMsg->pDest;
			CString strListUp,strListPath;
			strListUp.Format(_T("%s"),*pstrListUp);
			strListPath.Format(_T("%s"),*pstrListPath);

			m_wndMovieList.OnAddFile(strListUp,strListPath,pMsg->nParam1);
		}
		break;
	case WM_ESM_MOVIELIST_LOADINFO:
		{
			m_wndMovieList.BoardStatus(pMsg->nParam1);
		}
		break;
	case WM_ESM_AJA_FINISH:
		{
			CString *pStrTime = (CString*) pMsg->pParam;
			CString strTime;
			strTime.Format(_T("%s"),*pStrTime);
			int nAJACount = pMsg->nParam1;
			m_wndTimeLineEditor.m_pTimeLineView->map_MakingData[strTime].nAJACount = nAJACount;
		}
		break;
	case WM_ESM_PROCESSOR_LOAD:
	case WM_ESM_PROCESSOR_LOAD_PLAY:
	case WM_ESM_PROCESSOR_LOAD_STOP:
	case WM_ESM_AJA_PLAY_OPT:
	case WM_ESM_AJA_SELECT_PLAY:
	case WM_ESM_PROCESSOR_ADDFILE:
	case WM_ESM_AJA_THREADINIT:
	case WM_ESM_AJA_RELOAD:
		{
			//m_AJA.ManageMsg(pMsg);
		}
		break;
	case WM_ESM_MOVIE_MUX:
		{
			SetInfoToMovieMgr();
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->pValue1 = (PVOID)pMsg->pParam;
			
#if 1
			pMovMsg->nParam1 = pMsg->nParam1;//Reverse
			//ESMLog(5,_T("REVERSE: %d - %d"),pMsg->nParam1,pMovMsg->nParam1);
#else
			if(ESMGetValue(ESM_VALUE_REVERSE_MOVIE))
				pMovMsg->nParam1 = TRUE;
			else
				pMovMsg->nParam1 = FALSE;
#endif
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_MUX;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_BACKUP_SET_PATH:
		{

		}
		break;
	case WM_ESM_DELETE_FILE:
		{
			DeleteRecordFile(NULL);
		}
		break;
	case WM_ESM_MOVIE_SET_KZONE_POINTS:
		{
			KZoneDataInfo* pKZoneDataInfo	= (KZoneDataInfo*)pMsg->pParam;
			ESMMovieMsg* pKZoneMsg			= new ESMMovieMsg;
			pKZoneMsg->nMsg					= ESM_MOVIE_MANAGER_SET_KZONE_POINTS;
			pKZoneMsg->nParam1				= (int)pMsg->nParam1;
			pKZoneMsg->pValue1				= pKZoneDataInfo;
			m_pESMMovieMgr->AddEvent(pKZoneMsg);
			
			//wgkim 191012
			m_pMovieRTMovie->SetKZoneDscCount(pKZoneMsg->nParam1);
			m_pMovieRTMovie->SetKZoneDataInfo(pKZoneDataInfo);

			ESMLog(5, _T("[K-Zone]K_Zone Data Send to ESMMovie."));
		}
		break;

	case WM_ESM_MOVIE_SET_KZONE_JUDGMENT:
	{
		m_pMovieRTMovie->SetKZoneValidTime();
		ESMLog(5, _T("[K-Zone]K-Zone Judgment."));
	}
	break;
		//wgkim 180629
	case WM_ESM_MOVIE_OPTION_AUTOADJUST_KZONE:
		{
			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->nParam1 = pMsg->nParam1;			
			pMovMsg->nMsg = ESM_MOVIE_MANAGER_OPTION_AUTOADJUST_KZONE;
			m_pESMMovieMgr->AddEvent(pMovMsg);
		}
		break;
	case WM_ESM_DETECT_CMD:
		{
			//jhhan 181126
			ESMLog(5, _T("WM_ESM_DETECT_CMD"));
			vector<DetectInfo> * pInfo = (vector<DetectInfo> *) pMsg->pParam;
			int nTime = pMsg->nParam1;
			for(int i=0; i<pInfo->size(); i++)
			{
				DetectInfo stData = pInfo->at(i);
				ESMLog(5, _T("DETECT_INFO[%d] [%s] - [%d]"),nTime, stData._strId, stData._nTime);
			}

			//jhhan 181126
			ESMEvent *pEvent = new ESMEvent();
			pEvent->message = WM_ESM_NET_DETECT_RESULT;
			pEvent->nParam1 = 1;
			pEvent->nParam2 = nTime + 100;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pEvent);
		}
		break;
	case WM_ESM_MOVIE_MULTIVIEW_MAKING:
		{
			SetInfoToMovieMgr();

			ESMMovieMsg* pMovMsg = new ESMMovieMsg;
			pMovMsg->pValue1 = (PVOID)pMsg->pParam;
			pMovMsg->nMsg	 = ESM_MOVIE_MANAGER_MULTIVIEW_MAKING;
			m_pESMMovieMgr->AddEvent(pMovMsg);
			
		}
		break;
	default:
		break;
	}

	//-- 2014-07-15 hongsu@esmlab.com
	//-- delete pointer
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	return ESM_ERR_NON;
}

void CMainFrame::StopFileTransfer()
{
	int nAll = m_wndDSCViewer.m_pListView->GetItemCount();
	CDSCItem* pDSCItem = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pDSCItem = m_wndDSCViewer.m_pListView->GetItemData(i);
		CSdiSingleMgr* pSdi = pDSCItem->GetSDIMgr();
		if(pSdi)
		{
			pSdi->StopFileTransfer();
		}
	}
}

void CMainFrame::StartFileTransfer()
{
	int nAll = m_wndDSCViewer.m_pListView->GetItemCount();
	CDSCItem* pDSCItem = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pDSCItem = m_wndDSCViewer.m_pListView->GetItemData(i);
		CSdiSingleMgr* pSdi = pDSCItem->GetSDIMgr();
		if(pSdi)
		{
			pSdi->StartFileTransfer();
		}
	}
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-07-15
//! @owner		Hongsu Jung
//! @return			
//! @revision		
//------------------------------------------------------------------------------
LRESULT CMainFrame::MsgEffect(ESMEvent* pMsg)
{
	//-- CIC Check
	if(!pMsg)
		return ESM_ERR_MSG;
	switch(pMsg->message)
	{
	case WM_ESM_EFFECT_UPDATE:
		{
			//-- 2014-07-15 hongsu@esmlab.com
			//-- Set MovieObject
			CESMTimeLineObjectEditor* pObj = (CESMTimeLineObjectEditor*)pMsg->pParam;
			if(pObj)
			{
				m_wndEffectEditor.LoadMovieObject(pObj);				
				m_wndEffectEditor.UpdateFrames();
			}
			
			m_wndDSCViewer.m_pFrameSelector->SetFocus();
		}
		break;
	//-- Update Frame View
	case WM_ESM_EFFECT_FRAME_VIEW:
		{
			CESMObjectFrame* pObjFrm = (CESMObjectFrame*)pMsg->pParam;
			//-- Select Spot Item
			m_wndEffectEditor.SelectListItem(pObjFrm, (BOOL)pMsg->nParam1);
		}
		break;
	//-- 2014-07-18 hongsu@esmlab.com
	//-- Add/Delete Spot
	case WM_ESM_EFFECT_LIST_CHANGE:
		{
			CESMObjectFrame* pObjFrm = (CESMObjectFrame*)pMsg->pParam;
			m_wndEffectEditor.UpdateList(pObjFrm, (BOOL)pMsg->nParam1);
		}
		break;
	case WM_ESM_EFFECT_SPOTLIST_UPDATE:
		{
			CESMTimeLineObjectEditor* pObj = (CESMTimeLineObjectEditor*)pMsg->pParam;
			if(pObj)
			{
				m_wndEffectEditor.m_dlgSpotInfo.ResetList();
				m_wndEffectEditor.m_pEffectView->LoadSpots();
			}
		}
		break;
	//CMiLRe 20151020 TimeLine 수정 후 Effect List Update
	case WM_ESM_EFFECT_REFLASH:
		{
			m_wndEffectEditor.RemoveMovieObject();
			//-- 2014-07-15 hongsu@esmlab.com
			//-- Set MovieObject
			CESMTimeLineObjectEditor* pObj = (CESMTimeLineObjectEditor*)pMsg->pParam;
			if(pObj)
			{
				pObj->RemoveAllObjectFrame();
				m_wndEffectEditor.LoadMovieObject(pObj);				
				m_wndEffectEditor.UpdateFrames();
			}

			m_wndDSCViewer.m_pFrameSelector->SetFocus();
		}
		break;
	default:
		break;
	}

	//-- 2014-07-15 hongsu@esmlab.com
	//-- delete pointer
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	return ESM_ERR_NON;
}

LRESULT CMainFrame::OnESMServerMsg(WPARAM wParam, LPARAM lParam)
{
	switch((int)wParam)
	{
	case WM_ESM_GET_INFO	: break;
	case WM_ESM_GET_STATUS	: 
		ExcelSaveStatus();
		break;
	case WM_ESM_GET_COMMAND	: 
		OnHttpCommand((int)lParam);
		break;
	default:
		{
			ESMLog(0, _T("[ESM] Unknown ESM Message[0x%X]"), (int)wParam);
			break;
		}
	}
	return S_OK;
}

LRESULT CMainFrame::MsgRemote(ESMEvent* pMsg)
{
	//-- CIC Check
	if(!pMsg)
		return ESM_ERR_MSG;

	

	switch(pMsg->message)
	{

	case WM_ESM_REMOTE_RECORD:
		m_pRemote->SendRemote(RCPCMD_REMOTE_START, pMsg->nParam1);
		break;
	case WM_ESM_REMOTE_RECORD_STOP:
		m_pRemote->SendRemote(RCPCMD_REMOTE_STOP);
		break;
	case WM_ESM_REMOTE_MAKE:
		m_pRemote->SendRemote(RCPCMD_REMOTE_MAKE, pMsg->nParam1);
		break;
	default:
		break;
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	return ESM_ERR_NON;
}

LRESULT CMainFrame::MsgReferee(ESMEvent* pMsg)
{
	if(!pMsg)
		return ESM_ERR_MSG;
	
	switch(pMsg->message)
	{

	case WM_ESM_NET_REFEREE_DATA_TO_CLIENT:
		{
			FILE *fp;
			CString strLog;

			char filename[255] = {0,};
			CString *strFile = (CString *)pMsg->pParam;
			
			sprintf(filename, "%S", *strFile);

			fp = fopen(filename, "rb");

			if(fp == NULL)
			{
				strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), *strFile);
				ESMLog(0,strLog);
				return ESM_ERR;
			}

			strLog.Format(_T("[#NETWORK] Send File [%s]"), *strFile);
			//ESMLog(1,strLog);

			fseek(fp, 0L, SEEK_END);
			int nLength = ftell(fp);
			char* buf = new char[nLength];
			fseek(fp, 0, SEEK_SET);
			fread(buf, sizeof(char), nLength, fp);

			CString *pPath = new CString;
			if(pMsg->nParam1 == ENUM_REFEREE_ORIGINAL)
			{
				CString *strSaveFile = (CString *)pMsg->pDest;
				pPath->Format(_T("%s"),*strSaveFile);
			}
			else
				pPath->Format(_T("%s"),*strFile);

			int* pLength = new int;
			*pLength = nLength;

			ESMEvent* pDest = new ESMEvent();
			pDest->message = WM_ESM_NET_REFEREE_DATA_TO_CLIENT;
			pDest->pParam = (LPARAM)pPath;
			pDest->pDest = (LPARAM)buf;
			pDest->pParent = (LPARAM)pLength;

			pDest->nParam1 = pMsg->nParam1;
			pDest->nParam2 = pMsg->nParam2;
			pDest->nParam3 = pMsg->nParam3;
			m_pRemote->AddMsg(pDest, TRUE);
			
			fclose(fp);

			CESMFileOperation fo;
			if(fo.IsFileExist(*pPath))
			{
				fo.Delete(*pPath);
			}
		}
		break;
	case WM_ESM_REFEREE_LIVE:
		{
			CString *str = (CString *)pMsg->pParam;

			CString strBuffer = str->GetBuffer(0);
			str->ReleaseBuffer();

			vector<CString> _Live;
			
			int i = 0;
			while (1)
			{
				CString strTxt;
				AfxExtractSubString(strTxt, strBuffer, i++, ',');
				strTxt.Trim();

				if(strTxt.IsEmpty())
					break;

				_Live.push_back(strTxt);
				ESMLog(5, _T("REFREREE_LIVE [%d] : %s"), i, strTxt);
			}

			ESMSetLiveList(_Live);
		}
		break;
	case WM_ESM_REFEREE_SELECT:
		{
			MakeShortCut('9');

			CString strTxt;
			strTxt.Format(_T("%c"), 0x41 + pMsg->nParam1); //Group
			ESMLog(5, _T("WM_ESM_REFEREE_SELECT : %s"), strTxt);

			CString strInfo;	//Group Info

			int nAll = m_wndDSCViewer.GetItemCount();
			for(int i = 0; i < nAll; i++)
			{
				CDSCItem * pItem = NULL;
				pItem = (CDSCItem*)m_wndDSCViewer.m_arDSCItem.GetAt(i);
				{
					CString strGrp = pItem->GetGroup();
					if(strGrp.Find(strTxt) != -1)
					{
						CString strId = pItem->GetDeviceDSCID();
						strInfo.Append(strId);
						strInfo.Append(_T(","));
					}
				}
			}

			ESMEvent* pTxMsg = NULL;
			pTxMsg = new ESMEvent();
			pTxMsg->message = WM_ESM_NET_REFEREE_SELECT;
			pTxMsg->nParam1 = pMsg->nParam2;				//Time

			CString * pData = NULL;
			pData = new CString;
			pData->Format(_T("%s"), strInfo);

			pTxMsg->pParam = (LPARAM)pData;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pTxMsg);
		}
		break;
	case WM_ESM_REFEREE_MAKE_SELECT:
		{
			int nTime = pMsg->nParam1;
			CString *str = (CString *)pMsg->pParam;

			CString strBuffer = str->GetBuffer(0);
			str->ReleaseBuffer();

			ResetAllM3u8();

			int i = 0;
			while (1)
			{
				CString strTxt;
				AfxExtractSubString(strTxt, strBuffer, i++, ',');
				strTxt.Trim();

				if(strTxt.IsEmpty())
					break;

				ESMLog(5, _T("REFREREE_MAKE : %s"), strTxt);

				//int nAll = m_wndDSCViewer.GetItemCount();

				for(int i = 0; i < m_wndDSCViewer.m_arDSCItem.GetCount(); i++)
				{
					CDSCItem *pItem = NULL;
					pItem = (CDSCItem*)m_wndDSCViewer.m_arDSCItem.GetAt(i);
					if(pItem)
					{
						if(strTxt.CompareNoCase(pItem->GetDeviceDSCID()) == 0)
						{
							MakeRefereeMovie(nTime - 1, strTxt);
						}
					}
				}
			}
			//ESMLog(5, _T("REFREREE_MAKE : %s"), _T("Done"));
		
		}
		break;
	case WM_ESM_REFEREE_INFO:
		{
			vector<ESMRefereeInfo> *pReferee = new vector<ESMRefereeInfo>;

			for(int i = 0; i < m_wndDSCViewer.m_arDSCItem.GetCount(); i++)
			{
				ESMRefereeInfo stReferee;
				CDSCItem * pItem = NULL;
				pItem = (CDSCItem*)m_wndDSCViewer.m_arDSCItem.GetAt(i);

				_stprintf(stReferee.strDSCId, _T("%s"), pItem->GetDeviceDSCID());
				_stprintf(stReferee.strIp, _T("%s"), pItem->GetDSCInfo(DSC_INFO_LOCATION));
				_stprintf(stReferee.strGroup, _T("%s"), pItem->GetGroup());
				
				pReferee->push_back(stReferee);
			}
			ESMEvent* pDest = new ESMEvent();
			pDest->message = WM_ESM_NET_REFEREE_INFO;
			pDest->pParam = (LPARAM)pReferee;

			m_pRemote->AddMsg(pDest, TRUE);
		}
		break;
	case WM_ESM_REFEREE_RECORD:
		{
			if(pMsg->nParam1 == 1)
			{
				MakeShortCut('1');
				//ESMLog(5, _T("Referee Record : %d"), pMsg->nParam1);
			}else if(pMsg->nParam1 == 2)
			{
				MakeShortCut('9');
				//ESMLog(5, _T("Referee Record : %d"), pMsg->nParam1);
			}
			else
			{
				ESMLog(0, _T("Not define command : WM_ESM_REFEREE_RECORD [%d]"), pMsg->nParam1);
			}
		}
		break;
	default:
		break;
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	return ESM_ERR_NON;
}