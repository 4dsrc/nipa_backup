#pragma once

#include "resource.h"
#include "cv.h"
#include "highgui.h"
#include "FFmpegManager.h"
#include "ESMImgMgrIndex.h"
#include "ESMDefine.h"
#include "WavePlayer.h"

#include <string>       // std::string
#include <vector>       // std::vector
using namespace std;
using namespace cv;

enum
{
	STATE_WAIT = 0,
	STATE_FINISH,
	STATE_FAIL,
};
struct EncodeList
{
	EncodeList()
	{
		nIdx = -1;
	}
	Mat frame0;
	Mat frame1;
	int nIdx;
};
class CESMStillImage
{
public:
	CESMStillImage();
	~CESMStillImage();

	int m_nIndex;
	void PushTime(int nTime);
	void PushTime(int nMovieIdx,int nFrameIdx);
	void ClaerArrTime(){m_nArrTime.clear();m_nIndex = 0;
	m_nMovieIdx = -1;m_nFrameIdx = -1;}
	void EncodingImage(Mat frame0,Mat frame1,int nIdx);
	void EncodingImage(EncodeList stLoad);
	Mat RectImage(Mat frame0,Mat frame1,int nIdx,BOOL bEB);
	void CreateStillMovie();
	void ClearRamDisk();
	void CreateCeremonyMovie(CString strCmd,CString strResult);
	
	
	void RectMovie();
	CWavePlayer m_wav;
	BOOL m_bMake;
	vector<int>m_nArrTime;
	vector<EncodeList>m_stArrImage;
	static unsigned WINAPI _DecodingThread(LPVOID param);
	static unsigned WINAPI _EncodingThread(LPVOID param);

	int m_nMovieIdx;
	int m_nFrameIdx;
};
struct StillImageThread
{
	CESMStillImage* pParent;
	int nTime;
	int nIdx;
};
struct StillImageThread2
{
	CESMStillImage* pParent;
	int nMovieIdx;
	int nFrameIdx;
	int nIndex;
};