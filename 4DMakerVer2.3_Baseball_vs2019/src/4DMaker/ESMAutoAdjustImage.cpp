#include "stdafx.h"
#include "ESMAutoAdjustImage.h"
#include "ESMFunc.h"
#include "ESMLabeling.h"
#include "highgui.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const int m_nFindRange		= 10;
const int m_nMinPixel		= 3;

// ESMAdjustImage

IMPLEMENT_DYNAMIC(CESMAutoAdjustImage, CStatic)

void OnEventhandle (int event, int x, int y, int flags, void* param);

CESMAutoAdjustImage::CESMAutoAdjustImage()
{
	m_dMultiple = 1.0;
	m_nImageWidth = 1;
	m_nImageHeight = 1;
	m_bImageMove = FALSE;
	m_memDC = NULL;
	//-- 2014-08-30 hongsu@esmlab.com
	//-- Remember Image 
	m_pTpImage = NULL;
	m_pImage = NULL;
	m_nCircleSize = 10;

	//wgkim 17-06-20
	m_dPrevMultiple = 1.0;

	//Image Overlay
	m_bRulerPoint	= FALSE;
	nRulerRGB		= 0;
	cRulerRGB		= RGB(0, 0, 0);
	m_pImageWithRulerLine = NULL;
}

CESMAutoAdjustImage::~CESMAutoAdjustImage()
{
	//-- 2014-08-30 hongsu@esmlab.com
	//-- delete Image buffer
	if(m_pTpImage)
		delete m_pTpImage;
	if(m_memDC)
	{
		m_memDC->DeleteDC();
		delete m_memDC;
	}

	//Image Overlay
	ClearDrawRuler();
}


BEGIN_MESSAGE_MAP(CESMAutoAdjustImage, CStatic)
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

void CESMAutoAdjustImage::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	int nTp = 0;
	CRect rect;
	GetClientRect(rect);

	if(m_memDC)
	{
		dc.SetStretchBltMode(COLORONCOLOR);
		//dc.Rectangle(0, 0, rect.right, rect.bottom);
		dc.StretchBlt( 0, 0, rect.Width(), rect.Height(), m_memDC, 
			int((double)m_ptImageLeftTop.x * m_dMultiple), 
			int((double)m_ptImageLeftTop.y * m_dMultiple), 
			int((double)rect.Width() * m_dMultiple), 
			int((double)rect.Height() * m_dMultiple),  SRCCOPY);
	}
	/*else
		dc.Rectangle(rect);*/
}

void CESMAutoAdjustImage::SetImagePos(int nPosX, int nPosY)
{
	m_ptImageLeftTop.x = nPosX;
	m_ptImageLeftTop.y = nPosY;
}

BOOL CESMAutoAdjustImage::SetImageBuffer(BYTE* pImage, int nWidth, int nHeight, int nChannel,stAutoAdjustInfo *priinfo)
{
	//if( pImage == NULL)
	//	return FALSE;

	m_nImageWidth = nWidth;
	m_nImageHeight = nHeight;

	int nDepth = 8;
	CRect rect;
	GetClientRect(&rect);


	if(!m_memDC)
	{
		CDC *pDC = GetDC();
		CBitmap bitmap;
		m_memDC = new CDC;
		if(!m_memDC->CreateCompatibleDC(pDC))
			return FALSE;

		bitmap.CreateCompatibleBitmap(pDC, nWidth, nHeight);

		m_memDC->SelectObject(&bitmap);
		ReleaseDC(pDC);		
	}


	if(m_pTpImage)
		delete[] m_pTpImage;

	m_pTpImage = new BYTE[nHeight * nWidth * nChannel];
	if( nChannel == 1)
	{
		for( int j= 0 ;j < nHeight; j++)
		{
			for( int i = 0 ;i < nWidth; i++)
			{
				m_pTpImage[ j *  (nWidth * 3) + (i* 3)]		= pImage[ j *  nWidth + i];
				m_pTpImage[ j *  (nWidth * 3) + (i* 3) + 1] = pImage[ j *  nWidth + i];
				m_pTpImage[ j *  (nWidth * 3) + (i* 3) + 2] = pImage[ j *  nWidth + i];
			}
		}
	}
	else
	{
		memcpy(m_pTpImage, pImage,  nHeight * nWidth * nChannel);
	}

	BITMAPINFOHEADER bih;
	memset(&bih, 0, sizeof(BITMAPINFOHEADER));    
	bih.biSize			= sizeof(BITMAPINFOHEADER);
	bih.biWidth			= nWidth;  
	bih.biHeight		= -nHeight; 
	bih.biPlanes		= 1;
	bih.biBitCount		= 3 * nDepth;
	bih.biSizeImage		= nWidth * nHeight * 3;
	bih.biCompression	= BI_RGB;

	m_memDC->SetStretchBltMode(COLORONCOLOR);


	::StretchDIBits(*m_memDC,	// hDC
		0,						// XDest
		0,						// YDest
		nWidth,					// nDestWidth
		nHeight,				// nDestHeight
		0,						// XSrc
		0,						// YSrc
		nWidth ,				// nSrcWidth
		nHeight ,				// nSrcHeight
		(BYTE*)m_pTpImage,		// lpBits
		(BITMAPINFO*)&bih,		// lpBitsInfo
		DIB_RGB_COLORS,			// wUsage
		SRCCOPY);				// dwROP

	
	//-- 2014-08-30 hongsu@esmlab.com
	//-- remember Image
	//-- delete when get new image or delete this
	// delete[] pTpImage;

	//-- 2014-08-31 hongsu@esmlab.com
	//-- Create Image 
	if(m_pImage)
	{
		//cvReleaseImage(&m_pImage);
		m_pImage = NULL;
	}

	m_pImage = BYTE2IplImage(m_pTpImage,m_nImageWidth, m_nImageHeight);
	if(!m_pImage)
	{
		cvReleaseImage(&m_pImage);
		return FALSE;
	}

	return TRUE;
}

void CESMAutoAdjustImage::ImageZoom(CPoint ptMouse, BOOL bZoom)
{
	if(bZoom)
	{
		m_dMultiple -= 0.1;
	}
	else
	{
		m_dMultiple += 0.1;
	}

	CRect rtWindow;
	GetClientRect(rtWindow);

	//wgkim 17-06-20
	float prevImageWidth = m_nImageWidth / m_dPrevMultiple;
	float prevImageHeight= m_nImageHeight/ m_dPrevMultiple;

	float currentImageWidth = m_nImageWidth / m_dMultiple;
	float currentImageHeight= m_nImageHeight/ m_dMultiple;

	float widthRatio = (m_ptImageLeftTop.x + rtWindow.Width()/2.) / prevImageWidth;
	float heightRatio= (m_ptImageLeftTop.y + rtWindow.Height()/2.)/ prevImageHeight;

	int deltaX = m_ptImageLeftTop.x - (prevImageWidth - currentImageWidth) *widthRatio;
	int deltaY = m_ptImageLeftTop.y - (prevImageHeight- currentImageHeight)*heightRatio;

	m_dPrevMultiple = m_dMultiple;

	SetImagePos(deltaX, deltaY);

	Invalidate();
}

BOOL CESMAutoAdjustImage::DrawDefectPoint(int PosX, int PosY, COLORREF color)
{
	CRect rect;
	GetClientRect(rect);
	CBrush NewBrush(color);
	CBrush* pOldBrush = m_memDC->SelectObject(&NewBrush);
	m_memDC->Rectangle(PosX - 3, PosY - 3, PosX + 3, PosY + 3);
	m_memDC->SelectObject(pOldBrush);
	return TRUE;
}

void CESMAutoAdjustImage::Redraw()
{
	RedrawWindow();
}

void CESMAutoAdjustImage::OnMouseMove(UINT nFlags, CPoint point)
{
	if( m_bImageMove == TRUE)
	{
		m_ptImageMoving = point;
		m_ptImageLeftTop.x = m_ptImageMoveStart.x - m_ptImageMoving.x;
		m_ptImageLeftTop.y = m_ptImageMoveStart.y - m_ptImageMoving.y;
		Invalidate();
	}

	CStatic::OnMouseMove(nFlags, point);
}

void CESMAutoAdjustImage::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_bImageMove = TRUE;
	m_ptImageMoveStart = m_ptImageLeftTop + point;

	CStatic::OnLButtonDown(nFlags, point);
}


void CESMAutoAdjustImage::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_bImageMove = FALSE;
	m_ptImageMoveStart.x = 0 ;
	m_ptImageMoveStart.y = 0;
	m_ptImageMoving.x = 0;
	m_ptImageMoving.y = 0;
	CStatic::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-08-30
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
CPoint CESMAutoAdjustImage::FindExactPosition(int& nX, int& nY)
{
	CPoint pos;

	if( m_pImage == NULL)
		return pos;

	//-- 2014-08-30 hongsu@esmlab.com
	//-- Clicked Position 
	ESMLog(5,_T("Find Exact Position Before [%d,%d]"),nX,nY);

	//-- 2014-08-31 hongsu@esmlab.com
	//-- Draw Find Position
	if ( nX - m_nCircleSize < 0 )
		nX = m_nCircleSize;
	if ( nX + m_nCircleSize > m_pImage->width )
		nX = m_pImage->width - m_nCircleSize;

	if ( nY - m_nCircleSize < 0 )
		nY = m_nCircleSize;
	if ( nY + m_nCircleSize > m_pImage->height )
		nY = m_pImage->height - m_nCircleSize;

	POINT ptm;
	GetCursorPos( &ptm );

	CRect rtDraw(nX-m_nCircleSize,nY-m_nCircleSize,nX+m_nCircleSize,nY+m_nCircleSize);
	DrawRect(rtDraw, RGB(0,255,0));

	CPoint pt;

	IplImage*	pImgArea = NULL;
	IplImage*	pImgResize = NULL;

	pImgArea = cvCreateImage (cvSize(rtDraw.Width(), rtDraw.Height()) , IPL_DEPTH_8U, 3);

	if(GetUsingRuler())
	{
		if(m_pImageWithRulerLine == NULL)
		{
			cvSetImageROI (m_pImage, cvRect(rtDraw.left, rtDraw.top, rtDraw.Width(), rtDraw.Height()));
			cvCopy(m_pImage, pImgArea);
			cvResetImageROI(m_pImage);
		}
		else
		{
			cvSetImageROI (m_pImageWithRulerLine, cvRect(rtDraw.left, rtDraw.top, rtDraw.Width(), rtDraw.Height()));
			cvCopy(m_pImageWithRulerLine, pImgArea);
			cvResetImageROI(m_pImageWithRulerLine);
		}
	}
	else
	{
		cvSetImageROI (m_pImage, cvRect(rtDraw.left, rtDraw.top, rtDraw.Width(), rtDraw.Height()));
		cvCopy(m_pImage, pImgArea);
		cvResetImageROI(m_pImage);
	}

	int nPixel = 20;
	// 1:1 비율로 변환
	pImgResize = cvCreateImage(cvSize(pImgArea->width*nPixel , pImgArea->height*nPixel), IPL_DEPTH_8U, 3);
	cvResize(pImgArea, pImgResize, cv::INTER_LANCZOS4);


	for ( int i=1 ; i < pImgArea->width ; i++ )
		cvLine(pImgResize, cvPoint(i*nPixel, 0), cvPoint(i*nPixel, pImgArea->width*nPixel), CV_RGB(207,237,197) , 2);

	for ( int i=1 ; i < pImgArea->height; i++ )
		cvLine(pImgResize, cvPoint(0, i*nPixel), cvPoint(pImgArea->height*nPixel, i*nPixel), CV_RGB(207,237,197), 2 );

	ESMEvent* pMsg = new ESMEvent;
	pMsg->pParam = (LPARAM)pImgResize;
	pMsg->nParam1 = 0;
	pMsg->nParam2 = 0;

	// 팝업
	cvNamedWindow( "Area Image", 0 );
	cvResizeWindow( "Area Image", pImgResize->width, pImgResize->height );
	cvShowImage( "Area Image", pImgResize );

	CRect rect;

	GetWindowRect(rect);
	cvMoveWindow("Area Image", ptm.x - pImgResize->width/2, ptm.y - pImgResize->height/2);
	cvSetMouseCallback("Area Image", OnEventhandle,  (void *)pMsg);
	cvWaitKey(0);

	pt.x = rtDraw.left + (pMsg->nParam1)/nPixel + 1;
	pt.y = rtDraw.top + (pMsg->nParam2)/nPixel + 1;

	cvReleaseImage(&pImgArea);
	cvReleaseImage(&pImgResize);
	delete pMsg;

	nX = pt.x;
	nY = pt.y;

	pos.x = pt.x;
	pos.y = pt.y;
	//-- 2014-08-31 hongsu@esmlab.com
	//-- Draw Find Position
	CRect rtFind(pt.x-1,pt.y-1,pt.x+1,pt.y+1);
	DrawRect(rtFind, RGB(255,0,0));
	Invalidate();

	return pos;
}

CPoint CESMAutoAdjustImage::FindCenter(int nX, int nY, CRect rtArea)
{
	CRect rect;
	GetClientRect(rect);

	CPoint ptRet;
	IplImage*	pImgArea;
	IplImage*	pImgGray;
	IplImage*	pImgLabeled;

	//cvSaveImage("C:\\test.bmp", m_pImage);
	//-- 2014-08-31 hongsu@esmlab.com
	//-- Cut Image
	pImgArea = cvCreateImage (cvSize(rtArea.Width(), rtArea.Height()) , IPL_DEPTH_8U, 3);

	//cvSetImageROI (m_pImage, cvRect(rtArea.left, rtArea.top, rtArea.Width(), rtArea.Height()));
	//cvCopy(m_pImage, pImgArea);
	//cvResetImageROI(m_pImage);
	//cvSaveImage("C:\\test1.bmp", pImgArea);
	int nRgbMin, nRgbMax, nRgbAvg;
	WriteRGB(pImgArea, m_nCircleSize*2 , m_nCircleSize*2, nRgbMin, nRgbMax, nRgbAvg);

	CPoint ptCenter;
	FindCenterRegion(pImgArea, rtArea.Width(), ptCenter, nRgbMin, nRgbMax, nRgbAvg);

	nX = rtArea.left + ptCenter.x + 1;
	nY = rtArea.top + ptCenter.y + 1;

	CPoint pt;
	pt.x = nX;
	pt.y = nY;

	//TRACE(_T("Find Rect   [%d:%d , %d:%d]\n"),rtFind.left, rtFind.top, rtFind.right, rtFind.bottom);
	TRACE(_T("Find Center [%d:%d -> %d:%d]\n"),nX, nY, pt.x, pt.y);
	return pt;
}

void CESMAutoAdjustImage::FindCenterRegion(IplImage*	pImgArea, int nLength,  CPoint& ptCenter, int nRgbMin, int nRgbMax, int nRgbAvg)
{
	int nWhite, nBlack, nDiff = 60;
	int nWhiteTop, nWhiteBottom;

	int nMaxPointWidth = m_nCircleSize * 0.3;
	int nMinPointWidth = m_nCircleSize * 0.2;
	int nCutRegion = nLength * 0.15;


	nWhite = nRgbMax - 30;
	nBlack = nRgbAvg - 20;

	if ( nWhite > 190 )
		nWhite = 190;

	nWhiteTop = nWhite;
	nWhiteBottom = nWhite;

	// 각 라인별로 RGB값의 평균값을 구하여 Black이 되는 기준 RGB값과 White가 되는 기준 RGB값을 계산한다.
	int nSumTop=0, nSumBottom=0;
	for ( int i=0 ; i<nLength ; i++ )
	{
		nSumTop = nSumTop + GetRGBAvg(pImgArea, i, nLength/2 - nMaxPointWidth*1.5);
		nSumBottom = nSumBottom + GetRGBAvg(pImgArea, i, nLength/2 + nMaxPointWidth*1.2);
	}

	int nAvgTop = nSumTop / nLength;
	int nAvgBottom = nSumBottom / nLength;

	nWhiteTop = nAvgTop + 30;
	nWhiteBottom = nAvgBottom + 30;
	if ( nWhiteBottom > 190 )
		nWhiteBottom = 190;
	if ( nWhiteTop > 190 )
		nWhiteTop = 190;

	nWhite = min(nWhiteBottom, nWhiteTop);
	if ( nBlack < 100 )
		nBlack = 100;
	if ( nBlack + 20 > nWhite )
		nBlack = nWhite - 20;
	int nRgb;

	CvPoint ptTop;
	CvPoint ptBottom;

	BOOL bFindX = FALSE;

	int nPrevRgbAvg=255;

	CvPoint ptTmpTop, ptTmpBottom;
	ptTmpTop.x = 0;
	ptTmpTop.y = 0;
	ptTmpBottom.x = 0;
	ptTmpBottom.y = 255;

	// 중심점 가로 길이, 위치 찾기
	for ( int i=nCutRegion; i<nLength - nCutRegion ; i++ )
	{
		nRgb = GetRGBAvg(pImgArea, i, 0);

		if ( nRgb > nWhite )
		{
			for ( int j = nCutRegion ; j < nLength - nCutRegion ; j++)
			{
				nRgb = GetRGBAvg(pImgArea, i, j);
				if ( nRgb < nBlack )
				{
					ptTmpTop.x = i;
					ptTmpBottom.x = i;
					ptTmpTop.y = j;
					ptTmpBottom.y = j;

					for ( int k = i+1 ; k < nLength - nCutRegion ; k++ )
					{
						nRgb = GetRGBAvg(pImgArea, k, j);
						if ( nRgb < nBlack )
						{
							int nRgbTop = GetRGBAvg(pImgArea, k, nLength/2 - nMaxPointWidth*1.5);
							int nRgbBottom = GetRGBAvg(pImgArea, k, nLength/2 + nMaxPointWidth*1.2);

							if ( nRgbTop > nWhiteTop && nRgbBottom > nWhiteBottom )
							{
								ptTmpBottom.x = k;
							}
						}
						else
							break;
					}

					int nSum=0, nWidthRgbAvg;
					for ( int k = ptTmpTop.x ; k < ptTmpBottom.x + 1 ; k++ )
						nSum = nSum + GetRGBAvg(pImgArea, k, j);

					nWidthRgbAvg = nSum / ( ptTmpBottom.x - ptTmpTop.x + 1 );
					if ( nWidthRgbAvg == 0 )
						nWidthRgbAvg = 255;
					if ( nWidthRgbAvg < nPrevRgbAvg && !(ptTmpBottom.x - ptTmpTop.x + 1 > nMaxPointWidth) &&  !(ptTmpBottom.x - ptTmpTop.x + 2 < nMinPointWidth)  )
					{
						ptTop.x = ptTmpTop.x;
						ptBottom.x = ptTmpBottom.x;
						ptTop.y = ptTmpTop.y;
						ptBottom.y = ptTmpBottom.y;

						nPrevRgbAvg = nWidthRgbAvg;

						bFindX = TRUE;
					}

				}


			}
		}
		//if ( bFindX == TRUE )
		//	break;
	}

	if ( bFindX == FALSE )
	{
		ptTop.x = ptTmpTop.x;
		ptBottom.x = ptTmpBottom.x;
		ptTop.y = ptTmpTop.y;
		ptBottom.y = ptTmpBottom.y;
	}

	// 중심점 세로 길이, 위치 찾기
	// 위 방향 검색
	int nStRgb;
	for ( int i = ptTop.y ; i > nCutRegion ; i-- )
	{
		BOOL bRect = TRUE;
		for ( int j = ptTop.x ; j < ptBottom.x + 1 ; j++ )
		{
			nStRgb = GetRGBAvg(pImgArea, j, i);

			nRgb = GetRGBAvg(pImgArea, j, i-1);
			bRect = ( abs(nStRgb - nRgb) < nDiff || ( nStRgb < nBlack && nRgb < nBlack) ) &&  (nBlack + nWhite)/2 > nRgb ;
			if ( bRect == FALSE )
				break;
		}
		if ( bRect == TRUE)
			ptTop.y = i-1;
		else
			break;
	}

	// 아래 방향 검색
	for ( int i = ptTop.y ; i < nLength - nCutRegion ; i++ )
	{
		BOOL bRect = TRUE;
		for ( int j = ptTop.x ; j < ptBottom.x + 1 ; j++ )
		{
			nStRgb = GetRGBAvg(pImgArea, j, i);

			nRgb = GetRGBAvg(pImgArea, j, i+1);
			bRect = (abs(nStRgb - nRgb) < nDiff || ( nStRgb < nBlack && nRgb < nBlack)) &&  (nBlack + nWhite)/2 > nRgb ;
			if ( bRect == FALSE )
				break;

			ptBottom.y = i+1;
		}
		if ( bRect == TRUE)
			ptBottom.y = i+1;
		else
			break;
	}

	if ( bFindX )
	{
		ptCenter.x = (ptTop.x + ptBottom.x) / 2;
		ptCenter.y = (ptTop.y + ptBottom.y) / 2;
	}

}

int CESMAutoAdjustImage::GetRGBAvg(IplImage*	pImgArea, int nX, int nY)
{
	CvScalar v = cvGet2D(pImgArea, nY, nX);
	int nRgbAvg = (v.val[0] + v.val[1] + v.val[2]) / 3; 
	return nRgbAvg;
}

void CESMAutoAdjustImage::WriteRGB(IplImage*	pImgArea, int nWidth, int nHeight, int &nMin, int &nMax, int &nAvg)
{
	CString strFileName = _T("c:\\ImgData.txt");

	CFile file;
	CArchive ar(&file, CArchive::store);
	if(!file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite |CFile::modeNoTruncate))
		return;

	nMin = 255, nMax = 0;

	long nSum = 0;
	int nCnt = 0;
	for ( int i=0; i<nHeight ; i++ )
	{
		CString strValue;
		for ( int j=0; j<nWidth ; j++)
		{
			CvScalar v = cvGet2D(pImgArea, i,j);
			int nAvg = (v.val[0] + v.val[1] + v.val[2]) / 3; 
			strValue.Format(_T("%3d"), nAvg);
			ar.WriteString(strValue);
			ar.WriteString(_T(" "));

			nMin = min(nMin, nAvg);
			nMax = max(nMax, nAvg);

			nSum = nSum + nAvg;
			nCnt++;
		}
		ar.WriteString(_T("\r\n\r\n"));
	}

	nAvg = nSum / nCnt;
	CString strAvg;
	strAvg.Format(_T("%3d"), nAvg);
	ar.WriteString(_T("\r\n\r\n"));
	ar.WriteString(strAvg);

	ar.Close();
	file.Close();
}

void CESMAutoAdjustImage::GetImageRGB(int nX, int nY, int& nColorR, int& nColorG, int& nColorB)
{
	// Blue, Green, Red
	nColorB = m_pTpImage[ m_nImageWidth * (nY* 3) + (nX* 3)];
	nColorG = m_pTpImage[ m_nImageWidth * (nY* 3) + (nX* 3)+1];
	nColorR = m_pTpImage[ m_nImageWidth * (nY* 3) + (nX* 3)+2];
}

void CESMAutoAdjustImage::DrawRect(CRect rtDraw, COLORREF color)
{
	CPen shapePen(PS_SOLID,3,color);
	if( m_memDC == NULL)
		return;

	CPen* oldPen = m_memDC->SelectObject(&shapePen);

	CBrush brush;
	brush.CreateStockObject(NULL_BRUSH);
	CBrush* oldBrush =  (CBrush*)m_memDC->SelectObject(&brush);

	m_memDC->Rectangle(&rtDraw);
	m_memDC->SelectObject(oldPen);
}

void CESMAutoAdjustImage::DrawZoom(float nZoom, CRect* rect, CPoint margin)
{
	if( m_memDC == NULL)
		return;

	CPen shapePen(PS_SOLID,3,RGB(255,255,255));
	CPen* oldPen = m_memDC->SelectObject(&shapePen);
	CBrush brush;
	brush.CreateStockObject(NULL_BRUSH);
	CBrush* oldBrush =  (CBrush*)m_memDC->SelectObject(&brush);


	CRect rt;
	rt.left		= (m_nImageWidth/2) - (m_nImageWidth/nZoom/2)		- (m_nImageWidth/2 - margin.x);
	rt.right	= (m_nImageWidth/2) + (m_nImageWidth/nZoom/2)		- (m_nImageWidth/2 - margin.x);
	rt.top		= (m_nImageHeight/2) - (m_nImageHeight/nZoom/2)		- (m_nImageHeight/2 - margin.y);
	rt.bottom	= (m_nImageHeight/2) + (m_nImageHeight/nZoom/2)		- (m_nImageHeight/2 - margin.y);

	rect->left = rt.left;
	rect->right = rt.right;
	rect->top = rt.top;
	rect->bottom = rt.bottom;

	//-- TopLeft
	m_memDC->MoveTo(rt.left + rt.Height()/10 ,rt.top);
	m_memDC->LineTo(rt.left, rt.top);
	m_memDC->LineTo(rt.left, rt.top + rt.Height()/10);


	//-- BottomLeft
	m_memDC->MoveTo(rt.left , rt.bottom - rt.Height()/10 );
	m_memDC->LineTo(rt.left, rt.bottom);
	m_memDC->LineTo(rt.left + rt.Height()/10, rt.bottom);

	//-- TopRight
	m_memDC->MoveTo(rt.right - rt.Height()/10 ,rt.top);
	m_memDC->LineTo(rt.right, rt.top);
	m_memDC->LineTo(rt.right, rt.top + rt.Height()/10);

	//-- BottomRight
	m_memDC->MoveTo(rt.right , rt.bottom - rt.Height()/10 );
	m_memDC->LineTo(rt.right, rt.bottom);
	m_memDC->LineTo(rt.right - rt.Height()/10, rt.bottom);

	//-- Center Point
	m_memDC->MoveTo(rt.CenterPoint().x , rt.CenterPoint().y - rt.Height()/10 );
	m_memDC->LineTo(rt.CenterPoint().x , rt.CenterPoint().y + rt.Height()/10 );
	m_memDC->MoveTo(rt.CenterPoint().x - rt.Height()/10 , rt.CenterPoint().y );
	m_memDC->LineTo(rt.CenterPoint().x + rt.Height()/10 , rt.CenterPoint().y );



	m_memDC->SelectObject(oldPen);	
}
void CESMAutoAdjustImage::DrawPoint(int nX, int nY, COLORREF color)
{
	if( m_memDC == NULL)
		return;

	CPen shapePen(PS_SOLID,3,color);
	CPen* oldPen = m_memDC->SelectObject(&shapePen);

	CBrush brush;
	brush.CreateStockObject(NULL_BRUSH);
	CBrush* oldBrush =  (CBrush*)m_memDC->SelectObject(&brush);

	m_memDC->MoveTo(nX-5, nY);
	m_memDC->LineTo(nX+5, nY);

	m_memDC->MoveTo(nX, nY-5);
	m_memDC->LineTo(nX, nY+5);

	m_memDC->SelectObject(oldPen);	
}

void CESMAutoAdjustImage::DrawLine(int nHeight, COLORREF color)
{
	if( m_memDC == NULL)
		return;

	if ( nHeight < 0 || nHeight == 0 || m_nImageWidth < 2 || m_nImageHeight < 2 )
		return;
	CPen shapePen(PS_SOLID,3,color);
	CPen* oldPen = m_memDC->SelectObject(&shapePen);

	CBrush brush;
	brush.CreateStockObject(NULL_BRUSH);
	CBrush* oldBrush =  (CBrush*)m_memDC->SelectObject(&brush);

	m_memDC->MoveTo(0 , nHeight);
	//CMiLRe 20151020 TimeLine 수정 후 Effect List Update
	m_memDC->LineTo(m_nImageWidth, nHeight);
	m_memDC->SelectObject(oldPen);	
}

void CESMAutoAdjustImage::DrawLine(int nX1, int nY1, int nX2, int nY2, COLORREF color)
{
	if( m_memDC == NULL)
		return;

	//if ( nHeight < 0 || nHeight == 0 || m_nImageWidth < 2 || m_nImageHeight < 2 )
	if (nX1 <= 0 || nY1 <= 0 || nX2 <= 0 || nY2 <= 0 ||
		nX1 >= m_nImageWidth || nY1 >= m_nImageHeight || 
		nX2 >= m_nImageWidth || nY2 >= m_nImageHeight)
		return;

	CPen shapePen(PS_SOLID,3,color);
	CPen* oldPen = m_memDC->SelectObject(&shapePen);

	CBrush brush;
	brush.CreateStockObject(NULL_BRUSH);
	CBrush* oldBrush =  (CBrush*)m_memDC->SelectObject(&brush);

	m_memDC->MoveTo(nX1 , nY1);
	//CMiLRe 20151020 TimeLine 수정 후 Effect List Update
	m_memDC->LineTo(nX2, nY2);
	m_memDC->SelectObject(oldPen);
}

void CESMAutoAdjustImage::DrawCenterLine(int nWidth, COLORREF color)
{
	int nHalf = nWidth/2;

	if( m_memDC == NULL)
		return;

	if ( nWidth < 0 || nWidth == 0 || m_nImageWidth < 2 || m_nImageHeight < 2 )
		return;
	CPen shapePen(PS_SOLID,3,color);
	CPen* oldPen = m_memDC->SelectObject(&shapePen);

	CBrush brush;
	brush.CreateStockObject(NULL_BRUSH);
	CBrush* oldBrush =  (CBrush*)m_memDC->SelectObject(&brush);

	m_memDC->MoveTo(nHalf , 0);
	//CMiLRe 20151020 TimeLine 수정 후 Effect List Update
	m_memDC->LineTo(nHalf, m_nImageHeight);
	m_memDC->SelectObject(oldPen);	
}

IplImage* CESMAutoAdjustImage::BYTE2IplImage(BYTE* pByte, int nWidth, int nHeight)
{
	if(nWidth <= 0 || nHeight <= 0) 
		return NULL;

	IplImage* pImg = NULL;	
	pImg = cvCreateImageHeader(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 3); 
	if(!pImg)		
		return NULL;

	cvSetData( pImg, pByte, pImg->widthStep );
	return pImg;
}

BOOL CESMAutoAdjustImage::OnEraseBkgnd(CDC* pDC)
{
	CBrush backBrush(RGB(44, 44, 44)); // <- 흰색칼러로. 
	CBrush* pOldBrush = pDC->SelectObject(&backBrush); 
	CRect rect; 
	//pDC->GetClipBox(&rect); 
	GetClientRect(rect);
	pDC->PatBlt(rect.left, rect.top, rect.Width(), rect.Height(), PATCOPY);
	pDC->SelectObject(pOldBrush); 

	return TRUE;
	return CStatic::OnEraseBkgnd(pDC);
}

CPoint CESMAutoAdjustImage::StartDrawRuler(int nX, int nY)
{
	m_ptStartRulerPoint.x = nX;
	m_ptStartRulerPoint.y = nY;

	if(m_pImageWithRulerLine != NULL)
	{
		cvReleaseImage(&m_pImageWithRulerLine);
		m_pImageWithRulerLine = NULL;
	}

	return CPoint();
}

CPoint CESMAutoAdjustImage::DrawRuler(int nX, int nY)
{
	CPoint pos;

	if( m_pImage == NULL)
		return pos;

	m_ptEndRulerPoint.x = nX;
	m_ptEndRulerPoint.y = nY;

	ESMLog(5,_T("Find Exact Position Before [%d,%d]"),nX,nY);

	//for(int i = 0; i < m_vecStartRulerPoint.size(); i++)
	//{
	//	DrawRulerLine(m_vecStartRulerPoint[i].x, m_vecStartRulerPoint[i].y, m_vecEndRulerPoint[i].x, m_vecEndRulerPoint[i].y, 1);
	//}
	DrawLegacyRuler();

	DrawRulerLine(m_ptStartRulerPoint.x, m_ptStartRulerPoint.y, m_ptEndRulerPoint.x, m_ptEndRulerPoint.y, 1);
	Invalidate();

	return pos;
}

void CESMAutoAdjustImage::EndDrawRuler()
{
	m_vecStartRulerPoint.push_back(m_ptStartRulerPoint);
	m_vecEndRulerPoint.push_back(m_ptEndRulerPoint);

	if(m_pImageWithRulerLine != NULL)
	{
		cvReleaseImage(&m_pImageWithRulerLine);
		m_pImageWithRulerLine = NULL;
	}
	if(m_pImage == NULL)
		return;

	m_pImageWithRulerLine = cvCreateImage (cvSize(m_pImage->width, m_pImage->height) , IPL_DEPTH_8U, 3);
	cvCopy(m_pImage, m_pImageWithRulerLine);

	for(int i = 0; i < m_vecStartRulerPoint.size(); i++)
	{
		cvLine(m_pImageWithRulerLine, 
			cvPoint(m_vecStartRulerPoint[i].x, m_vecStartRulerPoint[i].y), 
			cvPoint(m_vecEndRulerPoint[i].x, m_vecEndRulerPoint[i].y), 
			CV_RGB(cRulerRGB&0x80, (cRulerRGB>>8)&0x80, (cRulerRGB>>16)&0x80),
			1, 8, 0);
	}
}

void CESMAutoAdjustImage::ClearDrawRuler()
{
	m_vecStartRulerPoint.clear();
	m_vecEndRulerPoint.clear();
}

void CESMAutoAdjustImage::DrawLegacyRuler()
{
	for(int i = 0; i < m_vecStartRulerPoint.size(); i++)
	{
		DrawRulerLine(m_vecStartRulerPoint[i].x, m_vecStartRulerPoint[i].y, m_vecEndRulerPoint[i].x, m_vecEndRulerPoint[i].y, 1);
	}
}

void CESMAutoAdjustImage::DrawRulerLine(int nX1, int nY1, int nX2, int nY2, int nThickness)
{
	if( m_memDC == NULL)
		return;

	//if ( nHeight < 0 || nHeight == 0 || m_nImageWidth < 2 || m_nImageHeight < 2 )
	if (nX1 <= 0 || nY1 <= 0 || nX2 <= 0 || nY2 <= 0 ||
		nX1 >= m_nImageWidth || nY1 >= m_nImageHeight || 
		nX2 >= m_nImageWidth || nY2 >= m_nImageHeight)
		return;

	CPen shapePen(PS_SOLID, nThickness, cRulerRGB);
	CPen* oldPen = m_memDC->SelectObject(&shapePen);

	CBrush brush;
	brush.CreateStockObject(NULL_BRUSH);
	CBrush* oldBrush =  (CBrush*)m_memDC->SelectObject(&brush);

	m_memDC->MoveTo(nX1 , nY1);
	//CMiLRe 20151020 TimeLine 수정 후 Effect List Update
	m_memDC->LineTo(nX2, nY2);
	m_memDC->SelectObject(oldPen);
}

void CESMAutoAdjustImage::RulerColorChange()
{
	nRulerRGB++;

	if(nRulerRGB % 5 == 0)
	{
		cRulerRGB = RGB(0, 0, 0);
	}
	else if(nRulerRGB % 5 == 1)
	{
		cRulerRGB = RGB(255, 0, 0);
	}
	else if(nRulerRGB % 5 == 2)
	{
		cRulerRGB = RGB(0, 255, 0);
	}
	else if(nRulerRGB % 5 == 3)
	{
		cRulerRGB = RGB(0, 0, 255);
	}
	else if(nRulerRGB % 5 == 4)
	{
		cRulerRGB = RGB(255, 255, 255);
	}
	else
	{
		cRulerRGB = RGB(0, 0, 0);
	}
	DrawLegacyRuler();

	if(m_pImageWithRulerLine != NULL)
	{
		cvReleaseImage(&m_pImageWithRulerLine);
		m_pImageWithRulerLine = NULL;
	}
	m_pImageWithRulerLine = cvCreateImage (cvSize(m_pImage->width, m_pImage->height) , IPL_DEPTH_8U, 3);
	cvCopy(m_pImage, m_pImageWithRulerLine);

	for(int i = 0; i < m_vecStartRulerPoint.size(); i++)
	{
		cvLine(m_pImageWithRulerLine, 
			cvPoint(m_vecStartRulerPoint[i].x, m_vecStartRulerPoint[i].y), 
			cvPoint(m_vecEndRulerPoint[i].x, m_vecEndRulerPoint[i].y), 
			CV_RGB(cRulerRGB&0x80, (cRulerRGB>>8)&0x80, (cRulerRGB>>16)&0x80),
			1, 8, 0);
	}
}