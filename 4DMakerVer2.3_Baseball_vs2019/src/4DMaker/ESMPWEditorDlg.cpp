_commandEntries
// ESMPWEditorDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMPWEditorDlg.h"
#include "afxdialogex.h"
#include "ESMIndexStructure.h"


// CESMPWEditorDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMPWEditorDlg, CDialog)

CESMPWEditorDlg::CESMPWEditorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CESMPWEditorDlg::IDD, pParent)
{

}

CESMPWEditorDlg::~CESMPWEditorDlg()
{
}

void CESMPWEditorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER_SATURATION, m_ctrlSaturation);
	DDX_Control(pDX, IDC_SLIDER_SHARPNESS, m_ctrlSharness);
	DDX_Control(pDX, IDC_SLIDER_CONTRAST, m_ctrlContrast);
	DDX_Control(pDX, IDC_SLIDER_HUE, m_ctrlHue);
	DDX_Control(pDX, IDC_VALUE_SATURATION, m_ctrlValueSaturation);
	DDX_Control(pDX, IDC_VALUE_SHARPNESS, m_ctrlValueSharpness);
	DDX_Control(pDX, IDC_VALUE_CONTRAST, m_ctrlValueContrast);
	DDX_Control(pDX, IDC_VALUE_HUE, m_ctrlValueHue);
	DDX_Control(pDX, IDC_SLIDER_RED, m_ctrlRed);
	DDX_Control(pDX, IDC_SLIDER_GREEN, m_ctrlGreen);
	DDX_Control(pDX, IDC_SLIDER_BLUE, m_ctrlBlue);
	DDX_Control(pDX, IDC_VALUE_RED, m_ctrlValueRed);
	DDX_Control(pDX, IDC_VALUE_GREEN, m_ctrlValueGreen);
	DDX_Control(pDX, IDC_VALUE_BLUE, m_ctrlValueBlue);
}


BEGIN_MESSAGE_MAP(CESMPWEditorDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_CANCLE, &CESMPWEditorDlg::OnBnClickedButtonCancle)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_SATURATION, &CESMPWEditorDlg::OnCustomdrawSliderSaturation)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_SHARPNESS, &CESMPWEditorDlg::OnCustomdrawSliderSharpness)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_HUE, &CESMPWEditorDlg::OnCustomdrawSliderHue)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_CONTRAST, &CESMPWEditorDlg::OnCustomdrawSliderContrast)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_RED, &CESMPWEditorDlg::OnCustomdrawSliderRed)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_GREEN, &CESMPWEditorDlg::OnCustomdrawSliderGreen)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_BLUE, &CESMPWEditorDlg::OnCustomdrawSliderBlue)
	ON_BN_CLICKED(IDC_BUTTON_APPLY, &CESMPWEditorDlg::OnBnClickedButtonApply)
	ON_BN_CLICKED(IDC_BUTTON_APPLY_ALL, &CESMPWEditorDlg::OnBnClickedButtonApplyAll)
	ON_BN_CLICKED(IDC_BUTTON_RESET, &CESMPWEditorDlg::OnBnClickedButtonReset)
END_MESSAGE_MAP()


void CESMPWEditorDlg::OnBnClickedButtonCancle()
{
	OnCancel();
}


BOOL CESMPWEditorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	CString strValue;
	m_ctrlRed.SetRange(0,199);
	m_ctrlGreen.SetRange(0,199);
	m_ctrlBlue.SetRange(0,199);

	m_ctrlRed.SetTicFreq(1); 
	m_ctrlGreen.SetTicFreq(1); 
	m_ctrlBlue.SetTicFreq(1); 


	m_ctrlSaturation.SetRange(-10,10);
	m_ctrlSharness.SetRange(-10,10);
	m_ctrlContrast.SetRange(-10,10);
	m_ctrlHue.SetRange(-30,30);

	m_ctrlSaturation.SetTicFreq(1); 
	m_ctrlSharness.SetTicFreq(1); 
	m_ctrlContrast.SetTicFreq(1); 
	m_ctrlHue.SetTicFreq(3); 

	SetRGBValue(m_nRGB);
	SetDetailValue(m_nValue);
	
	Invalidate();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CESMPWEditorDlg::SetRGBValue(int nValue)
{
	CString strValue;
	BYTE nRed = LOBYTE(LOWORD(nValue));
	BYTE nGreen = HIBYTE(LOWORD(nValue));
	BYTE nBlue = LOBYTE(HIWORD(nValue));

	m_ctrlRed.SetPos(nRed);
	m_ctrlGreen.SetPos(nGreen);
	m_ctrlBlue.SetPos(nBlue);

	strValue.Format(_T("%d"), m_ctrlRed.GetPos());
	m_ctrlValueRed.SetWindowText(strValue);

	strValue.Format(_T("%d"), m_ctrlGreen.GetPos());
	m_ctrlValueGreen.SetWindowText(strValue);

	strValue.Format(_T("%d"), m_ctrlBlue.GetPos());
	m_ctrlValueBlue.SetWindowText(strValue);
	
}

void CESMPWEditorDlg::SetDetailValue(int nValue)
{
	CString strValue;
	BYTE nSaturation = LOBYTE(LOWORD(nValue));
	BYTE nSharpness = HIBYTE(LOWORD(nValue));
	BYTE nContrast = LOBYTE(HIWORD(nValue));
	BYTE nHue = HIBYTE(HIWORD(nValue));

	m_ctrlSaturation.SetPos(nSaturation - 10);
	m_ctrlSharness.SetPos(nSharpness - 10);
	m_ctrlContrast.SetPos(nContrast - 10);
	m_ctrlHue.SetPos(nHue * 3 - 30);

	strValue.Format(_T("%d"), m_ctrlSaturation.GetPos());
	m_ctrlValueSaturation.SetWindowText(strValue);

	strValue.Format(_T("%d"), m_ctrlSharness.GetPos());
	m_ctrlValueSharpness.SetWindowText(strValue);

	strValue.Format(_T("%d"), m_ctrlContrast.GetPos());
	m_ctrlValueContrast.SetWindowText(strValue);

	strValue.Format(_T("%d"), m_ctrlHue.GetPos());
	m_ctrlValueHue.SetWindowText(strValue);

}

void CESMPWEditorDlg::OnCustomdrawSliderSaturation(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	CString strValue;
	strValue.Format(_T("%d"), m_ctrlSaturation.GetPos());
	m_ctrlValueSaturation.SetWindowText(strValue);
	*pResult = 0;
}


void CESMPWEditorDlg::OnCustomdrawSliderSharpness(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	CString strValue;
	strValue.Format(_T("%d"), m_ctrlSharness.GetPos());
	m_ctrlValueSharpness.SetWindowText(strValue);
	*pResult = 0;
}


void CESMPWEditorDlg::OnCustomdrawSliderHue(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	CString strValue;
	strValue.Format(_T("%d"), m_ctrlHue.GetPos());
	m_ctrlValueHue.SetWindowText(strValue);
	*pResult = 0;
}


void CESMPWEditorDlg::OnCustomdrawSliderContrast(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	CString strValue;
	strValue.Format(_T("%d"), m_ctrlContrast.GetPos());
	m_ctrlValueContrast.SetWindowText(strValue);
	*pResult = 0;
}


void CESMPWEditorDlg::OnCustomdrawSliderRed(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	CString strValue;
	strValue.Format(_T("%d"), m_ctrlRed.GetPos());
	m_ctrlValueRed.SetWindowText(strValue);
	*pResult = 0;
}


void CESMPWEditorDlg::OnCustomdrawSliderGreen(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	CString strValue;
	strValue.Format(_T("%d"), m_ctrlGreen.GetPos());
	m_ctrlValueGreen.SetWindowText(strValue);
	*pResult = 0;
}


void CESMPWEditorDlg::OnCustomdrawSliderBlue(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	CString strValue;
	strValue.Format(_T("%d"), m_ctrlBlue.GetPos());
	m_ctrlValueBlue.SetWindowText(strValue);
	*pResult = 0;
}

void CESMPWEditorDlg::OnBnClickedButtonApply()
{
	SetApply(m_pItem);
}


void CESMPWEditorDlg::OnBnClickedButtonApplyAll()
{
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	CString strValue;

	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		if (!pItem)
			continue;

		SetApply(pItem);
	}
}

void CESMPWEditorDlg::SetApply( CDSCItem* pItem )
{
	if (!pItem)
		return;

	BYTE nRed = (m_ctrlRed.GetPos());
	BYTE nGreen = (m_ctrlGreen.GetPos());
	BYTE nBlue = (m_ctrlBlue.GetPos());

	BYTE* pos = (BYTE*)&m_nRGB;
	memcpy(pos+1, &nBlue, 1);
	memcpy(pos+2, &nGreen, 1);
	memcpy(pos+3, &nRed, 1);

	ESMEvent* pSdiMsg = new ESMEvent;
	pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	pSdiMsg->nParam1 = m_nPTPCode;
	pSdiMsg->nParam2 = PTP_VALUE_UINT_32;
	pSdiMsg->pParam = m_nRGB;
	m_pItem->SdiAddMsg(pSdiMsg);


	BYTE nSaturation = (m_ctrlSaturation.GetPos()+10);
	BYTE nSharpness = (m_ctrlSharness.GetPos()+10);
	BYTE nContrast = (m_ctrlContrast.GetPos()+10);
	BYTE nHue = (m_ctrlHue.GetPos()+30)/3;

	pos = (BYTE*)&m_nValue;
	memcpy(pos, &nHue, 1);
	memcpy(pos+1, &nContrast, 1);
	memcpy(pos+2, &nSharpness, 1);
	memcpy(pos+3, &nSaturation, 1);

	ESMEvent* pSdiMsg1 = new ESMEvent;
	pSdiMsg1->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	pSdiMsg1->nParam1 = m_nPTPCodeETC;
	pSdiMsg1->nParam2 = PTP_VALUE_UINT_32;
	pSdiMsg1->pParam = m_nValue;
	m_pItem->SdiAddMsg(pSdiMsg1);

	ESMEvent* pSdiMsg2 = new ESMEvent;
	pSdiMsg2->message = WM_SDI_OP_HIDDEN_COMMAND;
	pSdiMsg2->nParam1 = HIDDEN_COMMAND_BANK_SAVE;
	pSdiMsg2->nParam2 = PTP_VALUE_NONE;
	m_pItem->SdiAddMsg(pSdiMsg2);
}


void CESMPWEditorDlg::OnBnClickedButtonReset()
{
	SetRGBValue(0x00646464);
	SetDetailValue(0x0a0a0a0a);

	SetApply(m_pItem);
}
