#pragma once

// CESMTrackingViewer 대화 상자입니다.
#include <strsafe.h>
#include "ESMFunc.h"

class CESMTrackingViewer : public CDialogEx
{
	DECLARE_DYNAMIC(CESMTrackingViewer)

public:
	CESMTrackingViewer(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMTrackingViewer();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UTIL_TRACKING_VIEWER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	afx_msg BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonTrackingmodeApply();

public:
	HANDLE m_hMuxMovie;
	static unsigned WINAPI ThreadDetectFolder(LPVOID param);
	void RunMuxing();	
	
	BOOL m_bUsingTrackingMode;
	void ToggleTrackingMode();
	BOOL GetTrackingModeState();

	CString m_strDetectedPath;	
	
	int m_nZoomInValue;
	DWORD m_dwIP;

	CString GetStrFilePath(CString strFileName);
	CString GetStrDate();
	CString GetStrIPFromDwIP(DWORD dwIP);
	BOOL StrFilter(CString strFilePath);
	
	CString m_strTrackingMode;
};
