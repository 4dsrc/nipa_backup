// ESMUtilMultiplexerDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "MainFrm.h"
#include "ESMUtilMultiplexerDlg.h"
#include "ESMFileOperation.h"


// CESMUtilMultiplexerDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMUtilMultiplexerDlg, CDialogEx)

CESMUtilMultiplexerDlg::CESMUtilMultiplexerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMUtilMultiplexerDlg::IDD, pParent)
	, m_bInsertCamID(FALSE)
	, m_bFtpSend(FALSE)
	, m_nSelectTime(0)
	, m_nTotalTime(4000)
	, m_nStartTime(0)
	, m_nEndTime(0)
	, m_nOutWidth(1920)
	, m_nOutHeight(1080)
	, m_nFtpPort(21)
	, m_strFtpIP(_T("183.99.124.112"))
	, m_strFtpID(_T("CMiLRe"))
	, m_strFtpPW(_T("1111"))
	, m_nStartFrame(0)
	, m_nEndFrame(0)
	, m_nFinish(0)
	/*, m_strMUXSavePath(_T("\\\\192.168.0.100\\RecordSave\\Output\\Mux"))
	, m_str4DLSavePath(_T("\\\\192.168.0.100\\RecordSave\\Output\\4DL"))
	, m_strMakeingServerPath(_T("\\\\192.168.0.100\\4DMaker"))*/
	, m_bMakingWait(FALSE)
	, m_bMUXing(FALSE)
	, m_bMakeDP(FALSE)
	, m_bHalf(FALSE)
	, m_bCurMuxState(FALSE)
	, m_bTCP(FALSE)
	, m_bBothProc(FALSE)
	, m_dbZoomRatio(100)
	, m_nEncodingMethod(0)
	, m_nGroupIndex(-1)
	, m_bLowBitrate(FALSE)
	, m_nRadioButtonForContainer(0)
{
	m_pMainWnd = (CMainFrame*)AfxGetMainWnd();

	//m_strMUXSavePath.Format(_T("%s\\MUX\\%s"), ESMGetPath(ESM_PATH_OUTPUT), ESMGetFrameRecord());

	m_strMUXSavePathProfile.Format(_T("%s\\%s"),m_strMUXSavePath,ESMGetFrameRecord());

	//int nTotalRecTime = m_wndDSCViewer.m_pFrameSelector->GetRecTime();
	m_nSelectTime = m_pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();

	m_nStartTime = m_nSelectTime - (m_nTotalTime/2);
	if(m_nStartTime < 0)
		m_nStartTime = 0;

	m_nEndTime = m_nSelectTime + (m_nTotalTime/2);

	m_pConnection = NULL;
	m_pFileFind = NULL;

	m_nMuxDataSendCount = 0;
	m_nMuxDataFinishCount = 0;
	m_nMjpgEncodingCount = 0;

	//170622 hjcho
	memset(m_dbArrColorInfo,0,sizeof(double)*3);
	m_bColorTrans = FALSE;

	CString strFrontIP = ESMGetFrontIP();
	m_strMUXSavePath.Format(_T("\\\\%s100\\RecordSave\\Output\\Mux"), strFrontIP);
	m_str4DLSavePath.Format(_T("\\\\%s100\\RecordSave\\Output\\4DL"), strFrontIP);
	m_strMakeingServerPath.Format(_T("\\\\%s100\\4DMaker"), strFrontIP);

	//m_pMuxOptDlg = NULL;
	m_strSUBL = _T("NONAME");
	m_strSUBM = _T("NONAME");
	m_strSUBS = _T("NONAME");
	m_strOBJ  = _T("NONAME");
	m_strDES  = _T("NONAME");
	//UpdateData(TRUE);

	m_nMuxSendCnt	= 0;
	m_bPSCP			= FALSE;
	m_dbConnect		= FALSE;
	m_bFTPListLoad	= FALSE;
	m_bThird		= FALSE;

	m_LcFTP.SetUseCamStatusColor(FALSE);
	m_background.CreateSolidBrush(RGB(44,44,44));

	//wgkim 180615
	m_bCheckInputWindowsData = TRUE;
	m_strMp4SavePath.Format(_T("\\\\%s100\\RecordSave\\Output\\Mux\\4DLive_Video"), strFrontIP);

	m_strDBIPInfo = _T("13.57.98.197");

	m_nFirstMovieIdx = 0;
	m_nFirstFrameIdx = 0;
	m_nBitrateInMbps = 0;
}

CESMUtilMultiplexerDlg::~CESMUtilMultiplexerDlg()
{
	m_bFtpSend = FALSE;

	if(m_pFileFind)
	{
		delete m_pFileFind;
		m_pFileFind = NULL;
	}

	if(m_pConnection)
	{
		delete m_pConnection;
		m_pConnection = NULL;
	}

	DeleteCriticalSection (&_Finish);
}

void CESMUtilMultiplexerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_MUX_INSERT_ID, m_bInsertCamID);
	DDX_Check(pDX, IDC_MUX_FTP_SEND, m_bFtpSend);
	DDX_Text(pDX, IDC_MUX_SELECT_TIME, m_nSelectTime);
	DDX_Text(pDX, IDC_MUX_TOTAL_TIME, m_nTotalTime);
	DDX_Text(pDX, IDC_MUX_START_TIME, m_nStartTime);
	DDX_Text(pDX, IDC_MUX_END_TIME, m_nEndTime);
	DDX_Text(pDX, IDC_MUX_OUTWIDTH, m_nOutWidth);
	DDX_Text(pDX, IDC_MUX_OUTHEIGTH, m_nOutHeight);
	DDX_Text(pDX, IDC_MUX_FTP_IP, m_strFtpIP);
	DDX_Text(pDX, IDC_MUX_FTP_ID, m_strFtpID);
	DDX_Text(pDX, IDC_MUX_FTP_PW, m_strFtpPW);
	DDX_Text(pDX, IDC_MUX_FTP_PORT, m_nFtpPort);
	DDX_Text(pDX, IDC_MUX_FILESAVEPATH, m_strMUXSavePath);
	DDX_Text(pDX, IDC_MUX_MAKEINGPATH, m_strMakeingServerPath);
	DDX_Text(pDX, IDC_MUX_4DLSAVEPATH, m_str4DLSavePath);
	DDX_Control(pDX, IDC_LIST_MUX, m_LcFTP);
	DDX_Check(pDX, IDC_MUX_MAKEING_WAIT, m_bMakingWait);
	DDX_Check(pDX, IDC_MUX_MAKEDP, m_bMakeDP);
	DDX_Check(pDX, IDC_MUX_HALF, m_bHalf);
	DDX_Check(pDX, IDC_MUX_LOWBITRATE, m_bLowBitrate);
	DDX_Control(pDX, IDC_MUX_SEL_RESOLUTION, m_combo);
	//170622 hjcho
	DDX_Check(pDX,IDC_MUX_CHECK_COLORTRANS,m_bColorTrans);
	DDX_Check(pDX,IDC_MUX_CHECK_TCPSEND,m_bTCP);
	DDX_Control(pDX, IDC_MUX_PROGRESS, m_ProgressMux);
	DDX_Check(pDX,IDC_MUX_BOTHPROC,m_bBothProc);
	DDX_Text(pDX, IDC_MUX_ZOOMRATIO, m_dbZoomRatio);
	DDX_Check(pDX,IDC_MUX_CHECK_PSCP,m_bPSCP);
	DDX_Radio(pDX,IDC_MUX_RADIO_ANDROID,(int&)m_nEncodingMethod);
	DDX_Control(pDX, IDC_MUX_DB_COMBO_SUBL, m_cbSubL);
	DDX_Control(pDX, IDC_MUX_DB_COMBO_SUBM, m_cbSubM);
	DDX_Control(pDX, IDC_MUX_DB_COMBO_SUBS, m_cbSubS);
	DDX_Control(pDX, IDC_MUX_DB_COMBO_OWNER, m_cbOwner);
	DDX_Control(pDX, IDC_MUX_COLOR_SELDSC, m_cbColorDSC);
	DDX_Control(pDX, IDC_MUX_DB_COMBO_PLAYER, m_cbPlayer);
	DDX_Check(pDX, IDC_CHECK_WINDOWS_RECORD, m_bCheckInputWindowsData);
	DDX_Text(pDX, IDC_MUX_MP4SAVEPATH, m_strMp4SavePath);
	DDX_Control(pDX, IDC_MUX_SET_RESOLUTION, m_btnSetResolution);
	DDX_Control(pDX, IDC_MUX_BTN_SETCOLOR, m_btnSetColorRevision);
	DDX_Control(pDX, IDC_MUX_DB_NAMING, m_btnSetThridOne);
	DDX_Control(pDX, IDC_MUX_DB_BTN_DBCONNTECT, m_btnDBConnect);
	DDX_Control(pDX, IDC_MUX_BTN_ADD, m_btnRemove);
	DDX_Control(pDX, IDC_MUX_BTN_SUB, m_btnAdd);
	DDX_Control(pDX, IDC_MUX_START, m_btnMuxStart);
	DDX_Control(pDX, IDC_MUX_SAVE, m_btnInfoSave);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);

	//wgkim 190212
	DDX_Radio(pDX, IDC_RADIO_CONTAINER_MP4, m_nRadioButtonForContainer);
	DDX_Text(pDX, IDC_EDIT_BITRATE_VALUE, m_nBitrateInMbps);
}
BEGIN_MESSAGE_MAP(CESMUtilMultiplexerDlg, CDialogEx)
	ON_EN_CHANGE(IDC_MUX_SELECT_TIME, &CESMUtilMultiplexerDlg::OnEnChangeMuxSelectTime)
	ON_EN_CHANGE(IDC_MUX_TOTAL_TIME, &CESMUtilMultiplexerDlg::OnEnChangeMuxTotalTime)
	ON_BN_CLICKED(IDC_MUX_START, &CESMUtilMultiplexerDlg::OnBnClickedMuxStart)
	ON_BN_CLICKED(IDC_MUX_SAVE, &CESMUtilMultiplexerDlg::OnBnClickedMuxSave)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_MUX, &CESMUtilMultiplexerDlg::OnNMCustomdrawListMux)
	ON_BN_CLICKED(IDC_MUX_SET_RESOLUTION, &CESMUtilMultiplexerDlg::OnBnClickedMuxSetResolution)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_MUX, &CESMUtilMultiplexerDlg::OnLvnItemchangedListMux)
	ON_BN_CLICKED(IDC_MUX_BTN_SETCOLOR, &CESMUtilMultiplexerDlg::OnBnClickedMuxBtnSetcolor)
	ON_BN_CLICKED(IDC_MUX_CHECK_COLORTRANS, &CESMUtilMultiplexerDlg::OnBnClickedMuxCheckColortrans)
	ON_BN_CLICKED(IDC_MUX_BTN_ADD, &CESMUtilMultiplexerDlg::OnBnClickedMuxBtnAdd)
	ON_BN_CLICKED(IDC_MUX_BTN_SUB, &CESMUtilMultiplexerDlg::OnBnClickedMuxBtnSub)
	ON_BN_CLICKED(IDC_MUX_DB_NAMING, &CESMUtilMultiplexerDlg::OnBnClickedMuxDbNaming)
	ON_BN_CLICKED(IDC_MUX_MAKEDP, &CESMUtilMultiplexerDlg::OnBnClickedMuxMakedp)
	ON_CONTROL_RANGE(BN_CLICKED,IDC_MUX_RADIO_ANDROID,IDC_MUX_RADIO_WINDOW,CESMUtilMultiplexerDlg::OnBnClickedMuxRadioAndroid)
	ON_BN_CLICKED(IDC_MUX_BOTHPROC, &CESMUtilMultiplexerDlg::OnBnClickedMuxBothproc)
	ON_BN_CLICKED(IDC_MUX_DB_BTN_DBCONNTECT, &CESMUtilMultiplexerDlg::OnBnClickedMuxDbBtnDbconntect)
	ON_BN_CLICKED(IDC_MUX_CHECK_PSCP, &CESMUtilMultiplexerDlg::OnBnClickedMuxCheckPscp)
	ON_WM_SHOWWINDOW()
	ON_CBN_SELCHANGE(IDC_MUX_DB_COMBO_SUBL, &CESMUtilMultiplexerDlg::OnCbnSelchangeMuxDbComboSubl)
	ON_CBN_SELCHANGE(IDC_MUX_DB_COMBO_SUBM, &CESMUtilMultiplexerDlg::OnCbnSelchangeMuxDbComboSubm)
	ON_CBN_SELCHANGE(IDC_MUX_DB_COMBO_SUBS, &CESMUtilMultiplexerDlg::OnCbnSelchangeMuxDbComboSubs)
	ON_CBN_SELCHANGE(IDC_MUX_DB_COMBO_OWNER, &CESMUtilMultiplexerDlg::OnCbnSelchangeMuxDbComboOwner)
//	ON_CBN_SELCHANGE(IDC_MUX_COLOR_SELDSC, &CESMUtilMultiplexerDlg::OnCbnSelchangeMuxColorSeldsc)
ON_CBN_SELCHANGE(IDC_MUX_DB_COMBO_PLAYER, &CESMUtilMultiplexerDlg::OnCbnSelchangeMuxDbComboPlayer)
ON_CBN_EDITCHANGE(IDC_MUX_DB_COMBO_PLAYER, &CESMUtilMultiplexerDlg::OnCbnEditchangeMuxDbComboPlayer)
ON_WM_CTLCOLOR()
ON_BN_CLICKED(IDC_CHECK_WINDOWS_RECORD, &CESMUtilMultiplexerDlg::OnBnClickedCheckWindowsRecord)
ON_CBN_SELCHANGE(IDC_MUX_SEL_RESOLUTION, &CESMUtilMultiplexerDlg::OnCbnSelchangeMuxSelResolution)

	//wgkim 190212
	ON_CONTROL_RANGE(BN_CLICKED, IDC_RADIO_CONTAINER_MP4, IDC_RADIO_CONTAINER_TS, &CESMUtilMultiplexerDlg::OnBnClickedRadioButtonForContainer)
	ON_EN_CHANGE(IDC_EDIT_BITRATE_VALUE, &CESMUtilMultiplexerDlg::OnEnChangeEditBitrateValue)
END_MESSAGE_MAP()


BOOL CESMUtilMultiplexerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	InitializeCriticalSection (&_Finish);

	m_LcFTP.InsertColumn(0, _T("Index"), 0, 50);
	m_LcFTP.InsertColumn(1, _T("CAM ID"), 0, 100);
	m_LcFTP.InsertColumn(2, _T("Movie State"), 0, 100);
	m_LcFTP.InsertColumn(3, _T("FTP State"), 0, 100);
	m_LcFTP.InsertColumn(4, _T("S"), 0, 1);
	m_LcFTP.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_CHECKBOXES);

	//FTPListLoad();

	if (m_pConnection != NULL) {
		m_pConnection->Close();
		delete m_pConnection;
		m_pConnection = NULL;
	}

	//170622 hjcho
	GetDlgItem(IDC_MUX_EDIT_BLUERATIO)->EnableWindow(FALSE);
	GetDlgItem(IDC_MUX_EDIT_GREENRATIO)->EnableWindow(FALSE);
	GetDlgItem(IDC_MUX_EDIT_REDRATIO)->EnableWindow(FALSE);
	
	CString strDefault = _T("100.0");
	SetDlgItemText(IDC_MUX_EDIT_REDRATIO,strDefault);
	SetDlgItemText(IDC_MUX_EDIT_GREENRATIO,strDefault);
	SetDlgItemText(IDC_MUX_EDIT_BLUERATIO,strDefault);
	
	//m_pMuxOptDlg = new CESMUtilMultiplexerOptDlg;

	MuxInfoLoad();

	//ComboBox
	GetDlgItem(IDC_MUX_OUTWIDTH)->EnableWindow(FALSE);
	GetDlgItem(IDC_MUX_OUTHEIGTH)->EnableWindow(FALSE);
	GetDlgItem(IDC_MUX_SET_RESOLUTION)->EnableWindow(FALSE);


	m_combo.AddString(_T("FULL-HD"));
	m_combo.AddString(_T("Ultra-HD"));
	m_combo.AddString(_T("CUSTOM"));

	if(m_nOutHeight == 1080)
		m_combo.SetCurSel(0);
	else if(m_nOutHeight == 2160)
		m_combo.SetCurSel(1);
	else
	{
		m_combo.SetCurSel(2);
		GetDlgItem(IDC_MUX_SET_RESOLUTION)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_OUTWIDTH)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_OUTHEIGTH)->EnableWindow(TRUE);
	}

	m_LcFTP.SetExtendedStyle(LVS_EX_FULLROWSELECT| LVS_EX_CHECKBOXES);
	m_ProgressMux.SetBarColor(RGB(255,0,0));

	m_btnSetResolution.ChangeFont(15);
	m_btnSetColorRevision.ChangeFont(15);
	m_btnSetThridOne.ChangeFont(15);
	m_btnDBConnect.ChangeFont(15);
	m_btnRemove.ChangeFont(15);
	m_btnAdd.ChangeFont(15);
	m_btnMuxStart.ChangeFont(15);
	m_btnInfoSave.ChangeFont(15);
	m_btnCancel.ChangeFont(15);

	CWnd *p_btn = GetDlgItem(IDC_CHECK_WINDOWS_RECORD);
	CWnd *p_group = GetDlgItem(IDC_STATIC_MUX_GROUP);
	p_btn->ShowWindow(FALSE);
	p_btn->SetWindowPos(p_group, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	p_btn->ShowWindow(TRUE);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CESMUtilMultiplexerDlg::OnBnClickedMuxStart()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	if (m_bMUXing)
	{
 		AfxMessageBox(_T("The previous operation was not terminated."));
		return;
	}
	else
		m_bMUXing = TRUE;

	CString strDay,strTime;
	GetTimePath(ESMGetFrameRecord(),strDay,strTime);
	m_strDay = strDay,m_strTime = strTime;

	if(ESMGetValue(ESM_VALUE_RTSP) == TRUE)
	{
		GetDlgItemText(IDC_MUX_DB_EDIT_DESCRIPT,m_strDES);
		/*if(m_strDES.IsEmpty())
		{
			AfxMessageBox(_T("Descript: "));
			m_bMUXing = FALSE;
			
			return;
		}*/
		m_strMUXSavePathProfile.Format(_T("%s\\%s"),m_strMUXSavePath,ESMGetFrameRecord());
		
		CString strFTPListPath = m_strMUXSavePathProfile + _T("\\FTPList.txt");
		CESMFileOperation fo;
		if(fo.IsFileExist(strFTPListPath))
		{
			int nCount = 0;
			while(1)
			{
				strFTPListPath.Format(_T("%s_%d\\FTPList.txt"),m_strMUXSavePathProfile,nCount);
				if(fo.IsFileExist(strFTPListPath))
				{
					nCount++;
				}
				else
				{
					m_strMUXSavePathProfile.Format(_T("%s\\%s_%d"),m_strMUXSavePath,ESMGetFrameRecord(),nCount);
					break;
				}
			}
		}
	
		if(CheckFTPList(m_strMUXSavePathProfile))
		{
			m_bMUXing = FALSE;
			m_bFTPListLoad = TRUE;
			return;
		}

		ESMCreateAllDirectories(m_strMUXSavePathProfile);
		AfxMessageBox(m_strMUXSavePathProfile);

		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)_beginthreadex(NULL,0,_LaunchRTSPVODThread,(void*)this,0,NULL);
		CloseHandle(hSyncTime);
	}
	else
	{
#if 1
		if(m_bMakeDP == FALSE)//Single Making[Server]
		{
			if(ESMGetValue(ESM_VALUE_DIRECTMUX) == TRUE)
			{
				AfxMessageBox(_T("Unsupported feature.\nF7-> Direct Mux uncheck or Distributed Processing check"));
				m_bMUXing = FALSE;
				return;
			}

			if(m_nEncodingMethod == ENCODE_WINDOW)
			{
				m_strMUXSavePathProfile.Format(_T("%s\\%s"),m_strMUXSavePath,ESMGetFrameRecord());

				if(CheckFTPList(m_strMUXSavePathProfile))
				{
					m_bMUXing = FALSE;
					m_bFTPListLoad = TRUE;
					return;
				}
				else
				{
					ESMCreateAllDirectories(m_strMUXSavePathProfile);
					ESMCreateAllDirectories(m_strMp4SavePath);
					AfxMessageBox(m_strMUXSavePathProfile);
				}
				if(SetDBInfo() == FALSE)
				{
					m_bMUXing = FALSE;
					return;
				}

				m_strFileSave = m_strMUXSavePathProfile;
			}
			else if(m_nEncodingMethod == ENCODE_ANDROID)
			{
				int nFind = m_str4DLSavePath.Find(_T("http"));
				if(nFind != -1)
				{
					AfxMessageBox(_T("It does not support http."));
					m_bMUXing = FALSE;
					return;
				}

				m_str4DLSavePathProfile.Format(_T("%s\\%s\\%s"),m_str4DLSavePath,strDay,strTime);
				if(CheckFTPList(m_str4DLSavePathProfile))
				{
					m_bMUXing = FALSE;
					m_bFTPListLoad = TRUE;
					//return;
				}
				else
				{
					ESMCreateAllDirectories(m_str4DLSavePathProfile);
					AfxMessageBox(m_str4DLSavePathProfile);
				}
				m_strFileSave = m_str4DLSavePathProfile;
			}

			HANDLE hMuxMovie = NULL;
			hMuxMovie = (HANDLE) _beginthreadex(NULL, 0, MuxThread, (void *)this, 0, NULL);
			CloseHandle(hMuxMovie);
		}
		else//4DP Making
		{
			if(m_bBothProc)
			{
				if(Create4DLPath(strDay,strTime) == FALSE)
					return;

				m_strMUXSavePathProfile.Format(_T("%s\\%s"),m_strMUXSavePath,ESMGetFrameRecord());
				if(CheckFTPList(m_strMUXSavePathProfile))
				{
					m_bMUXing = FALSE;
					return;
				}
				ESMCreateAllDirectories(m_strMUXSavePathProfile);
			}
			else
			{
				if(m_nEncodingMethod == ENCODE_WINDOW)
				{
					m_strMUXSavePathProfile.Format(_T("%s\\%s"),m_strMUXSavePath,ESMGetFrameRecord());

					if(CheckFTPList(m_strMUXSavePathProfile))
					{
						m_bMUXing = FALSE;
						return;
					}
					if(SetDBInfo() == FALSE && m_bCheckInputWindowsData == TRUE)
					{
						m_bMUXing = FALSE;
						return;
					}
					ESMCreateAllDirectories(m_strMUXSavePathProfile);

					AfxMessageBox(m_strMUXSavePathProfile);
					m_strFileSave = m_strMUXSavePathProfile;
				}
				else if(m_nEncodingMethod == ENCODE_ANDROID)
				{
					if(Create4DLPath(strDay,strTime) == FALSE)
						return;
				}
			}

			HANDLE hMuxMovie = NULL;
			hMuxMovie = (HANDLE) _beginthreadex(NULL, 0, MuxThreadDP, (void *)this, 0, NULL);
			CloseHandle(hMuxMovie);
		}
		if(m_bFtpSend)
		{
			HANDLE hSendFtp = NULL;
			hSendFtp = (HANDLE) _beginthreadex(NULL, 0, FTPThread, (void *)this, 0, NULL);
			CloseHandle(hSendFtp);
		}
#else
		CString strDay,strTime;
		GetTimePath(ESMGetFrameRecord(),strDay,strTime);

		if(ESMGetValue(ESM_VALUE_DIRECTMUX) && ESMGetValue(ESM_VALUE_GPUMAKESKIP))
		{
			m_strMUXSavePathProfile.Format(_T("%s\\%s\\%s"),m_strMUXSavePath,strDay,strTime);
			m_strFileName.Format(_T("%s_%s"),strDay,strTime);

			if(m_bBothProc)
			{
				m_strMUXSavePathProfile.Format(_T("%s\\%s"),m_strMUXSavePath,ESMGetFrameRecord());
				m_str4DLSavePathProfile.Format(_T("%s\\%s\\%s"),m_str4DLSavePath,strDay,strTime);
			}
		}
		else
		{
			m_strMUXSavePathProfile.Format(_T("%s\\%s"),m_strMUXSavePath,ESMGetFrameRecord());
			m_strFileName.Format(_T("%s_%s"),strDay,strTime);

			if(m_bBothProc)
			{
				m_strMUXSavePathProfile.Format(_T("%s\\%s"),m_strMUXSavePath,ESMGetFrameRecord());
				if(m_bPSCP)
				{
					m_str4DLSavePathProfile = m_str4DLSavePath;
					m_strDay = _T("/")+strDay, m_strTime = _T("/")+strTime;
				}
				else
					m_str4DLSavePathProfile.Format(_T("%s\\%s\\%s"),m_str4DLSavePath,strDay,strTime);
			}
			/*else
			{
			if(m_nEncodingMethod == ENCODE_WINDOW)
			m_strMUXSavePathProfile.Format(_T("%s\\%s"),m_strMUXSavePath,ESMGetFrameRecord());
			else
			m_strMUXSavePathProfile.Format(_T("%s\\%s\\%s"),m_strMUXSavePath,strDay,strTime);
			}*/
		}

		if(!ESMGetValue(ESM_VALUE_DIRECTMUX))
			AfxMessageBox(m_strMUXSavePathProfile);

		if(m_bMakeDP)
		{
			HANDLE hMuxMovie = NULL;
			hMuxMovie = (HANDLE) _beginthreadex(NULL, 0, MuxThreadDP, (void *)this, 0, NULL);
			CloseHandle(hMuxMovie);
		}
		else
		{
			HANDLE hMuxMovie = NULL;
			hMuxMovie = (HANDLE) _beginthreadex(NULL, 0, MuxThread, (void *)this, 0, NULL);
			CloseHandle(hMuxMovie);
		}

		if(m_bFtpSend)
		{
			HANDLE hSendFtp = NULL;
			hSendFtp = (HANDLE) _beginthreadex(NULL, 0, FTPThread, (void *)this, 0, NULL);
			CloseHandle(hSendFtp);
		}
#endif
	}
}

void CESMUtilMultiplexerDlg::SetFinish(int nFinish)
{
	//EnterCriticalSection (&_Finish);
	m_nFinish = nFinish;
	//LeaveCriticalSection (&_Finish);
}

int CESMUtilMultiplexerDlg::GetFinish()
{
	//EnterCriticalSection (&_Finish);
	return m_nFinish;
	//LeaveCriticalSection (&_Finish);
}

void CESMUtilMultiplexerDlg::SetFinish(int nFinish, CString strCamID, int nRemotIP)
{
	m_nMuxDataFinishCount++;
	ESMLog(5,_T("[MUX] Making Count (%d - %s) [%d/%d]"),nRemotIP,strCamID,m_nMuxDataFinishCount,m_nTotalMakingCnt);

	if (1 == nFinish)
	{
		//ESMLog(5,_T("Finish Mux Movie [%s]"),nCamID);

#ifndef _IBC
		MuxThreadData *data = new MuxThreadData;

		data->pMgr = this;
		data->strCamID = strCamID;


		HANDLE hMuxMovie = NULL;
		hMuxMovie = (HANDLE) _beginthreadex(NULL, 0, MjpgEncodingThread, (void *)data, 0, NULL);
		CloseHandle(hMuxMovie);
#else
		CString strState;
		strState.Format(_T("complete"));
		for (int i = 0 ; i < m_LcFTP.GetItemCount() ; i++)
		{
			CString _strCamID = m_LcFTP.GetItemText(i,1);

			if (strCamID == _strCamID)
			{
				m_LcFTP.SetItemText(i, 2, strState);
				FileListUpdata(m_LcFTP.GetItemText(i,1), 2,strState);
			}
		}
#endif
	}
	else if (0 > nFinish)
	{
		CString strState;
		strState.Format(_T("File ERR"));
		for (int i = 0 ; i < m_LcFTP.GetItemCount() ; i++)
		{
			CString _strCamID = m_LcFTP.GetItemText(i,1);

			if (strCamID == _strCamID)
			{
				m_LcFTP.SetItemText(i, 2, strState);
				FileListUpdata(m_LcFTP.GetItemText(i,1), 2,strState);
			}
		}
		m_nMjpgEncodingCount++;
		//ESMLog(5,_T("Err Mux Movie [%s]"),nCamID);
	}
}

void CESMUtilMultiplexerDlg::OnEnChangeMuxSelectTime()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	m_nStartTime = m_nSelectTime - (m_nTotalTime/2);
	if(m_nStartTime < 0)
		m_nStartTime = 0;

	m_nEndTime = m_nSelectTime + (m_nTotalTime/2);

	UpdateData(FALSE);
}

void CESMUtilMultiplexerDlg::OnEnChangeMuxTotalTime()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	m_nStartTime = m_nSelectTime - (m_nTotalTime/2);
	if(m_nStartTime < 0)
		m_nStartTime = 0;

	m_nEndTime = m_nSelectTime + (m_nTotalTime/2);

	OnBnClickedMuxSave();
	UpdateData(FALSE);
}

void CESMUtilMultiplexerDlg::OnBnClickedMuxSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	
	SetMUXInfoint(_T("m_nTotalTime"), m_nTotalTime);
	SetMUXInfoint(_T("m_nOutWidth"), m_nOutWidth);
	SetMUXInfoint(_T("m_nOutHeight"), m_nOutHeight);
	SetMUXInfoint(_T("m_bMakeDP"), m_bMakeDP);
	SetMUXInfoint(_T("m_bHalf"), m_bHalf);

	SetMUXInfoint(_T("m_nFtpPort"), m_nFtpPort);
	SetMUXInfoint(_T("m_bFtpSend"), m_bFtpSend);
	SetMUXInfoString(_T("m_strFtpIP"), m_strFtpIP);
	SetMUXInfoString(_T("m_strFtpID"), m_strFtpID);
	SetMUXInfoString(_T("m_strFtpPW"), m_strFtpPW);

	SetMUXInfoString(_T("m_strMUXSavePath"), m_strMUXSavePath);
	SetMUXInfoString(_T("m_str4DLSavePath"), m_str4DLSavePath);
	SetMUXInfoString(_T("m_strMakeingServerPath"), m_strMakeingServerPath);
	SetMUXInfoint(_T("m_bTCP"),m_bTCP);
	SetMUXInfoint(_T("m_bBothProc"),m_bBothProc);
	SetMUXInfoint(_T("m_bPSCP"),m_bPSCP);

	CString strZoom;
	strZoom.Format(_T("%f"),m_dbZoomRatio);
	
	SetMUXInfoString(_T("ZoomRatio"),strZoom);
	SetMUXInfoint(_T("m_nEncodingMethod"),m_nEncodingMethod);
	SetMUXInfoString(_T("m_strMp4SavePath"), m_strMp4SavePath);
	SetMUXInfoString(_T("m_strMuxDBIPInfo"), m_strDBIPInfo);
	SetMUXInfoint(_T("m_bLowBitrate"), m_bLowBitrate);

	//ESMLog(5,_T("[MUX] DBConnect IP: %s"),m_strDBIPInfo);

	//wgkim 190213

	SetMUXInfoint(_T("m_nRadioButtonForContainer"), m_nRadioButtonForContainer);
	SetMUXInfoint(_T("m_nBitrateInMbps"), m_nBitrateInMbps);

	UpdateData(FALSE);
}

void CESMUtilMultiplexerDlg::MuxInfoLoad()
{
	UpdateData(TRUE);

	m_nTotalTime	= GetMUXInfoint(_T("m_nTotalTime"), m_nTotalTime);
	m_nOutWidth		= GetMUXInfoint(_T("m_nOutWidth"), m_nOutWidth);
	m_nOutHeight	= GetMUXInfoint(_T("m_nOutHeight"), m_nOutHeight);
	m_bMakeDP		= GetMUXInfoint(_T("m_bMakeDP"), m_bMakeDP);
	m_bHalf			= GetMUXInfoint(_T("m_bHalf"), m_bHalf);

	m_nFtpPort		= GetMUXInfoint(_T("m_nFtpPort"), m_nFtpPort);
	m_bFtpSend		= GetMUXInfoint(_T("m_bFtpSend"), m_bFtpSend);
	m_strFtpIP		= GetMUXInfoString(_T("m_strFtpIP"), m_strFtpIP);
	m_strFtpID		= GetMUXInfoString(_T("m_strFtpID"), m_strFtpID);
	m_strFtpPW		= GetMUXInfoString(_T("m_strFtpPW"), m_strFtpPW);

	m_strMUXSavePath = GetMUXInfoString(_T("m_strMUXSavePath"), m_strMUXSavePath);
	m_str4DLSavePath = GetMUXInfoString(_T("m_str4DLSavePath"), m_str4DLSavePath);
	m_strMakeingServerPath = GetMUXInfoString(_T("m_strMakeingServerPath"), m_strMakeingServerPath);
	m_bTCP			 = GetMUXInfoint(_T("m_bTCP"),m_bTCP);
	m_bBothProc		 = GetMUXInfoint(_T("m_bBothProc"),m_bBothProc);
	m_bPSCP			 = GetMUXInfoint(_T("m_bPSCP"),m_bPSCP);
	m_nEncodingMethod= GetMUXInfoint(_T("m_nEncodingMethod"),m_nEncodingMethod);

	m_strMp4SavePath = GetMUXInfoString(_T("m_strMp4SavePath "), m_strMp4SavePath );

	m_bLowBitrate	= GetMUXInfoint(_T("m_bLowBitrate"), m_bLowBitrate);
	
	//OnBnClickedMuxRadioAndroid(m_nEncodingMethod);
	CString strZoom;
	strZoom = GetMUXInfoString(_T("ZoomRatio"),strZoom);
	if(strZoom.GetLength() == FALSE)
		strZoom.Format(_T("100"));

	m_dbZoomRatio = _wtof(strZoom);

	m_strDBIPInfo = GetMUXInfoString(_T("m_strMuxDBIPInfo"),m_strDBIPInfo);
	//OnBnClickedMuxMakedp();m

	//wgkim 190213
	m_nRadioButtonForContainer = GetMUXInfoint(_T("m_nRadioButtonForContainer"), m_nRadioButtonForContainer);
	m_nBitrateInMbps = GetMUXInfoint(_T("m_nBitrateInMbps"), m_nBitrateInMbps);

	UpdateData(FALSE);
}

void CESMUtilMultiplexerDlg::SetMUXInfoString(CString strKeyName, CString strValue)
{
	CString strSection = _T("MUX");
	CString strFileServerPath, strFileServerFile;

	strFileServerPath.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));
	strFileServerFile = strFileServerPath + _T("\\MUX.ini");

	::WritePrivateProfileString( strSection, strKeyName, strValue, strFileServerFile );
}

void CESMUtilMultiplexerDlg::SetMUXInfoint(CString strKeyName, int nValue)
{
	CString strSection = _T("MUX");
	CString strFileServerPath, strFileServerFile, strValue;

	strValue.Format(_T("%d"), nValue);

	strFileServerPath.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));
	strFileServerFile = strFileServerPath + _T("\\MUX.ini");

	::WritePrivateProfileString( strSection, strKeyName, strValue, strFileServerFile );
}

CString CESMUtilMultiplexerDlg::GetMUXInfoString(CString strKeyName, CString strDefault)
{
	CString strSection = _T("MUX");
	CString strFileServerPath, strFileServerFile, strValue;

	strFileServerPath.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));
	strFileServerFile = strFileServerPath + _T("\\MUX.ini");

	TCHAR* pBuffer = new TCHAR[MAX_PATH];

	long ret = ::GetPrivateProfileString( strSection, strKeyName, strDefault, pBuffer, 2047, strFileServerFile );

	CString s = pBuffer;
	delete pBuffer;

	if( ret==0 ) return strDefault;
	return s;
}

int CESMUtilMultiplexerDlg::GetMUXInfoint(CString strKeyName, int nDefault)
{
	CString strSection = _T("MUX");
	CString strFileServerPath, strFileServerFile, strValue;

	strFileServerPath.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));
	strFileServerFile = strFileServerPath + _T("\\MUX.ini");
	TCHAR* pBuffer = new TCHAR[2048];

	CString strDefault;
	strDefault.Format(_T("%d"),nDefault);

	long ret = ::GetPrivateProfileString( strSection, strKeyName, strDefault, pBuffer, 2047, strFileServerFile );

	int n = _ttoi(pBuffer);
	delete pBuffer;

	if( ret==0 ) return nDefault;
	return n;
}

int CESMUtilMultiplexerDlg::GetMakingInfoInt(int nDefault)
{
	if(!m_bMakingWait)
		return nDefault;

	CString strSection = _T("Recording");
	CString strKeyName = _T("MakingFlag");
	CString strServerFile;

	//\\192.168.0.34\4DMaker
	strServerFile = m_strMakeingServerPath + _T("\\Recorder.ini");
	int nResult = GetPrivateProfileInt( strSection, strKeyName, nDefault, strServerFile );

	return nResult;
}

BOOL CESMUtilMultiplexerDlg::CheckFile(CString strPath)
{
	CESMFileOperation fo;
	BOOL bCheckState = FALSE;
	CString strErr;
	for(int i = 0 ; i < 50 ; i++)
	{
		Sleep(10);

		if(fo.IsFileExist(strPath))
		{
			CFile file;
			if (file.Open(strPath, CFile::modeRead))
			{
				if(file.GetLength() > 0)
				{
					file.Close();
					bCheckState = TRUE;
					break;
				}
				else
				{
					strErr.Format(_T("[MUX]%s - %d"),strPath,file.GetLength());
					ESMLog(0,strErr);

					file.Close();
					continue;
					//return FALSE;
				}
			}
			else
			{
				CString strErr;
				strErr.Format(_T("[MUX]%s"),strPath);
				continue;
				//return FALSE;
			}
		}
		else
			return FALSE;
	}

	if(!bCheckState)
		ESMLog(0,strErr);

	return bCheckState;
}

BOOL CESMUtilMultiplexerDlg::FileListUpdata(CString strCamID, int nCol ,CString strLog)
{
	CFile file;
	FTPListData stData;
	CString strFileName;
	CString strTemp;
	int nFind = 1;

	strFileName.Format(_T("%s\\FTPList.txt"), m_strFileSave);

	if(file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite |CFile::modeNoTruncate))
	{
		int iLength = (int)(file.GetLength());

		while(iLength >= sizeof(FTPListData))
		{
			file.Read(&stData,sizeof(FTPListData));
			strTemp = stData.CamID;
			if(!strTemp.CompareNoCase(strCamID))
			{
				int nLength = 0;
				nLength= strLog.GetLength();

				if(nCol == 3)
				{
					memset(stData.FTPSendCheck,NULL,20);
					WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strLog,nLength,stData.FTPSendCheck,nLength,NULL,NULL);
				}
				else
				{
					memset(stData.MovieMakeCheck,NULL,20);
					WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strLog,nLength,stData.MovieMakeCheck,nLength,NULL,NULL);
				}

				file.Seek(-1*sizeof(FTPListData), CFile::current);
				file.Write(&stData,sizeof(FTPListData));

				break;
			}

			iLength = iLength-sizeof(FTPListData);
		}

		file.Close();
	}else
		return FALSE;

	return TRUE;
}

unsigned WINAPI CESMUtilMultiplexerDlg::MuxThread(LPVOID param)
{	
	CESMUtilMultiplexerDlg* pESMUtilMultiplexerDlg = (CESMUtilMultiplexerDlg*)param;

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	if(!arDSCList.GetSize())
	{
		AfxMessageBox(_T("can not Load Camera List"));	
		pESMUtilMultiplexerDlg->m_bMUXing = FALSE;
		return 0;
	}
	ESMLog(5,_T("CESMUtilMultiplexerDlg::MuxThread() Mux Start"));
	pESMUtilMultiplexerDlg->m_ProgressMux.SetRange32(0,arDSCList.GetSize());	
	pESMUtilMultiplexerDlg->m_ProgressMux.SetPos(0);

	CDSCItem* pItem = NULL;
	int nMovieIndex=0, nRealFrameIndex=0;
	CString strInsertData;
	CString strMakeCheck;
	int nLength = 0;

	CString strStartTime;
	CString strEndtTime;

	strStartTime = ESMGetCurTime();

	pESMUtilMultiplexerDlg->m_nStartFrame = ESMGetFrameIndex(pESMUtilMultiplexerDlg->m_nStartTime);
	pESMUtilMultiplexerDlg->m_nEndFrame = ESMGetFrameIndex(pESMUtilMultiplexerDlg->m_nEndTime)-1;

	CFile file;
	FTPListData stData;
	CString strFileName;
	strFileName.Format(_T("%s\\FTPList.txt"), pESMUtilMultiplexerDlg->m_strFileSave);
	
	if(!file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite))
	{
		CString strLog;
		strLog.Format(_T("can not access FTPList : %s"),strFileName);
		AfxMessageBox(strLog);
		pESMUtilMultiplexerDlg->m_bMUXing = FALSE;
		return 0;
	}else
	{
		int nLength = 0;
		pESMUtilMultiplexerDlg->WriteFTPList(&file);
		file.Close();
	}

	int nStartIdx = 0,nEndIdx = 0;
	int nStartMov = 0,nEndMov = 0;

	ESMGetMovieIndex(pESMUtilMultiplexerDlg->m_nStartFrame,nStartMov,nStartIdx);
	ESMGetMovieIndex(pESMUtilMultiplexerDlg->m_nEndFrame  ,nEndMov	,nEndIdx);

	pESMUtilMultiplexerDlg->CheckMakingDSC();

	pESMUtilMultiplexerDlg->SendMuxingDataLocal(nStartMov,nEndMov,_T(""));

	strInsertData.Format(_T("99999"));
	pESMUtilMultiplexerDlg->m_LcFTP.InsertItem(pESMUtilMultiplexerDlg->m_LcFTP.GetItemCount(), strInsertData);
	strInsertData.Format(_T("FTPList.txt"));
	pESMUtilMultiplexerDlg->m_LcFTP.SetItemText(pESMUtilMultiplexerDlg->m_LcFTP.GetItemCount()-1, 1, strInsertData);
	strInsertData.Format(_T("complete"));
	pESMUtilMultiplexerDlg->m_LcFTP.SetItemText(pESMUtilMultiplexerDlg->m_LcFTP.GetItemCount()-1, 2, strInsertData);


	strFileName.Format(_T("%s\\finish.txt"), pESMUtilMultiplexerDlg->m_strFileSave);
	if(file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite))
	{
		char strTime[40];
		memset(strTime, 0, 40);
		strEndtTime = ESMGetCurTime();

		nLength= strStartTime.GetLength();
		WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strStartTime,nLength,strTime,nLength,NULL,NULL);

		file.Write(&strTime,sizeof(char)*40);

		nLength= strEndtTime.GetLength();
		WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strEndtTime,nLength,strTime,nLength,NULL,NULL);

		file.SeekToEnd();
		file.Write(&strTime,sizeof(char)*40);

		pESMUtilMultiplexerDlg->WriteMovieInfo(&file);

		file.Close();
	}

	AfxMessageBox(_T("MUX Finish!!!"));
	ESMLog(5,_T("CESMUtilMultiplexerDlg::MuxThread() Mux End"));

	pESMUtilMultiplexerDlg->m_bMUXing = FALSE;
	return TRUE;
}
void CESMUtilMultiplexerDlg::WriteFTPList(CFile *file)
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	FTPListData stData;
	CDSCItem* pItem = NULL;

	CString strInsertData;
	int nLength = 0;

	/*vector<BOOL>bArrExcept(arDSCList.GetSize());

	for (int i = 0 ; i < arDSCList.GetSize() ; i++)
	{
		CString strExcept = m_LcFTP.GetItemText(i,2);
		if(strExcept.GetLength())
			bArrExcept.at(i) = FALSE;
		else
			bArrExcept.at(i) = TRUE;
	}

	m_LcFTP.DeleteAllItems();*/

	for (int i = 0 ; i < arDSCList.GetSize() ; i++)
	{
		memset(&stData,NULL,sizeof(FTPListData));

		pItem = (CDSCItem*)arDSCList.GetAt(i);

		strInsertData.Format(_T("%d"), i);
		//m_LcFTP.SetItemText(i,0,strInsertData);
		//m_LcFTP.InsertItem(i, strInsertData);
		nLength= strInsertData.GetLength();
		WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strInsertData,nLength,stData.Index,nLength,NULL,NULL);

		strInsertData.Format(_T("%s"), pItem->GetDeviceDSCID());
		//m_LcFTP.SetItemText(i, 1, strInsertData);
		nLength= strInsertData.GetLength();
		WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strInsertData,nLength,stData.CamID,nLength,NULL,NULL);

		CString strExcept = m_LcFTP.GetItemText(i,2);
		if(strExcept.GetLength())
			strInsertData.Format(_T("Except"));
		else
			strInsertData.Format(_T(""));

		//m_LcFTP.SetItemText(i, 2, strInsertData);
		nLength= strInsertData.GetLength();
		WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strInsertData,nLength,stData.MovieMakeCheck,nLength,NULL,NULL);

		//if(bArrExcept.at(i) == FALSE)
		//{
		//	strInsertData.Format(_T("Except"));
		//	m_LcFTP.SetItemText(i, 2, strInsertData);
		//	//nLength= strInsertData.GetLength();
		//	//WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strInsertData,nLength,stData.CamID,nLength,NULL,NULL);
		//}
		file->SeekToEnd();
		file->Write(&stData,sizeof(FTPListData));
	}

}
unsigned WINAPI CESMUtilMultiplexerDlg::MuxThreadDP(LPVOID param)
{	
#if 1
	CESMUtilMultiplexerDlg* pESMUtilMultiplexerDlg = (CESMUtilMultiplexerDlg*)param;
	
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	pESMUtilMultiplexerDlg->m_nTotalMakingCnt = arDSCList.GetSize();
	if(!arDSCList.GetSize())
	{
		AfxMessageBox(_T("can not Load Camera List"));	
		pESMUtilMultiplexerDlg->m_bMUXing = FALSE;
		return 0;
	}
	BOOL bBoth = pESMUtilMultiplexerDlg->m_bBothProc;
	BOOL bPSCP = pESMUtilMultiplexerDlg->m_bPSCP;
	ESMLog(5,_T("CESMUtilMultiplexerDlg::MuxThread() Mux Start"));
	
	if(ESMGetValue(ESM_VALUE_DIRECTMUX))
		Sleep(1000);//ESMSetWait(1000);

	BOOL bCheckTCP = pESMUtilMultiplexerDlg->m_bTCP;
	CDSCItem* pItem = NULL;
	int nMovieIndex=0, nRealFrameIndex=0;
	CString strInsertData;

	CString strStartTime,strEndtTime;

	strStartTime = ESMGetCurTime();

	pESMUtilMultiplexerDlg->m_nStartFrame = ESMGetFrameIndex(pESMUtilMultiplexerDlg->m_nStartTime);
	pESMUtilMultiplexerDlg->m_nEndFrame = ESMGetFrameIndex(pESMUtilMultiplexerDlg->m_nEndTime)-1;

	CFile file;
	FTPListData stData;
	CString str4DLPath;
	CString strFileName = pESMUtilMultiplexerDlg->m_strFileSave;
	strFileName.Append(_T("\\FTPList.txt"));

	if(!file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite))
	{
		CString strLog;
		strLog.Format(_T("can not access FTPList : %s"),strFileName);
		AfxMessageBox(strLog);
		pESMUtilMultiplexerDlg->m_bMUXing = FALSE;
		return 0;
	}else
	{
		int nLength = 0;
		pESMUtilMultiplexerDlg->WriteFTPList(&file);
	}
	file.Close();

	int nStartIdx = 0,nEndIdx = 0;
	int nStartMov = 0,nEndMov = 0;

	ESMGetMovieIndex(pESMUtilMultiplexerDlg->m_nStartFrame,nStartMov,nStartIdx);
	ESMGetMovieIndex(pESMUtilMultiplexerDlg->m_nEndFrame  ,nEndMov	,nEndIdx);

	pESMUtilMultiplexerDlg->CheckMakingDSC();

	if(pESMUtilMultiplexerDlg->CheckConnect4DP() == 0)
	{
		pESMUtilMultiplexerDlg->m_bMUXing = FALSE;
		return 0;
	}

	pESMUtilMultiplexerDlg->SendMuxingData(nStartMov,nEndMov,str4DLPath);

	//FTPList 전송
	if(bBoth || (bPSCP && pESMUtilMultiplexerDlg->m_nEncodingMethod == ENCODE_ANDROID))
	{
		pESMUtilMultiplexerDlg->RunHttp(pESMUtilMultiplexerDlg->m_str4DLSavePathProfile,strFileName,
										_T("FTPList.txt"),HTTP_SEND_FILE);
		CESMFileOperation fo;
		if(bBoth)
			fo.Copy(strFileName,pESMUtilMultiplexerDlg->m_strMUXSavePathProfile);
		fo.Delete(strFileName);
	}

	pESMUtilMultiplexerDlg->WriteFinishFile(strStartTime);

	if(bBoth || (bPSCP && pESMUtilMultiplexerDlg->m_nEncodingMethod == ENCODE_ANDROID))
	{
		strFileName.Replace(_T("FTPList"),_T("finish"));//.Append(_T("\\FTPList.txt"));
		pESMUtilMultiplexerDlg->RunHttp(pESMUtilMultiplexerDlg->m_str4DLSavePathProfile,strFileName,
										_T("finish.txt"),HTTP_SEND_FILE);
		CString strTemp;
		//strTemp.Format(_T("%s_finish.txt"),pESMUtilMultiplexerDlg->m_strOwner);
		strTemp.Format(_T("finish.txt"));
		pESMUtilMultiplexerDlg->RunHttp(pESMUtilMultiplexerDlg->m_str4DLSaveFile,strFileName,
									strTemp,HTTP_SEND_FILE);
		CESMFileOperation fo;
		if(bBoth)
			fo.Copy(strFileName,pESMUtilMultiplexerDlg->m_strMUXSavePathProfile);
		fo.Delete(strFileName,1);
	}
#else
	CESMUtilMultiplexerDlg* pESMUtilMultiplexerDlg = (CESMUtilMultiplexerDlg*)param;
	
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	pESMUtilMultiplexerDlg->m_nTotalMakingCnt = arDSCList.GetSize();
	if(!arDSCList.GetSize())
	{
		AfxMessageBox(_T("can not Load Camera List"));	
		pESMUtilMultiplexerDlg->m_bMUXing = FALSE;
		return 0;
	}
	BOOL bBoth = pESMUtilMultiplexerDlg->m_bBothProc;
	BOOL bPSCP = pESMUtilMultiplexerDlg->m_bPSCP;
	ESMLog(5,_T("CESMUtilMultiplexerDlg::MuxThread() Mux Start"));
	
	if(ESMGetValue(ESM_VALUE_DIRECTMUX))
		Sleep(1000);//ESMSetWait(1000);

	BOOL bCheckTCP = pESMUtilMultiplexerDlg->m_bTCP;
	CDSCItem* pItem = NULL;
	int nMovieIndex=0, nRealFrameIndex=0;
	CString strInsertData;

	CString strStartTime;
	CString strEndtTime;

	strStartTime = ESMGetCurTime();

	pESMUtilMultiplexerDlg->m_nStartFrame = ESMGetFrameIndex(pESMUtilMultiplexerDlg->m_nStartTime);
	pESMUtilMultiplexerDlg->m_nEndFrame = ESMGetFrameIndex(pESMUtilMultiplexerDlg->m_nEndTime)-1;

	CFile file;
	FTPListData stData;
	CString str4DLPath;
	CString strFileName;
	strFileName.Format(_T("%s\\FTPList.txt"), pESMUtilMultiplexerDlg->m_strMUXSavePathProfile);

	ESMCreateAllDirectories(pESMUtilMultiplexerDlg->m_strMUXSavePathProfile);

	if(bBoth)
	{
		if(bPSCP)
		{
			str4DLPath = pESMUtilMultiplexerDlg->m_str4DLSavePathProfile;
			str4DLPath.Append(pESMUtilMultiplexerDlg->m_strDay);
			pESMUtilMultiplexerDlg->RunHttp(str4DLPath,_T(""),_T(""),HTTP_CREATE_DIR);
			str4DLPath.Append(pESMUtilMultiplexerDlg->m_strTime);
			pESMUtilMultiplexerDlg->RunHttp(str4DLPath,_T(""),_T(""),HTTP_CREATE_DIR);
		}
		else
			ESMCreateAllDirectories(pESMUtilMultiplexerDlg->m_str4DLSavePathProfile);
		//m_str4DLSavePathProfile.Format(_T("%s\\%s\\%s"),m_str4DLSavePath,strDay,strTime);
	}
	CESMFileOperation fo;
	fo.Delete(pESMUtilMultiplexerDlg->m_strMUXSavePathProfile);

	if(!file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite))
	{
		CString strLog;
		strLog.Format(_T("can not access FTPList : %s"),strFileName);
		AfxMessageBox(strLog);
		pESMUtilMultiplexerDlg->m_bMUXing = FALSE;
		return 0;
	}else
	{
		int nLength = 0;
		/*if(AfxMessageBox(_T("삭제 후 다시 만들겠습니까?"),MB_YESNO)==IDYES)
		{
			pESMUtilMultiplexerDlg->m_LcFTP.DeleteAllItems();
			pESMUtilMultiplexerDlg->WriteFTPList(&file);
		}
		else*/
		pESMUtilMultiplexerDlg->WriteFTPList(&file);

			/*for (int i = 0 ; i < arDSCList.GetSize() ; i++)
			{
				memset(&stData,NULL,sizeof(FTPListData));

				pItem = (CDSCItem*)arDSCList.GetAt(i);

				strInsertData.Format(_T("%d"), i);
				pESMUtilMultiplexerDlg->m_LcFTP.InsertItem(i, strInsertData);
				nLength= strInsertData.GetLength();
				WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strInsertData,nLength,stData.Index,nLength,NULL,NULL);

				strInsertData.Format(_T("%s"), pItem->GetDeviceDSCID());
				pESMUtilMultiplexerDlg->m_LcFTP.SetItemText(i, 1, strInsertData);
				nLength= strInsertData.GetLength();
				WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strInsertData,nLength,stData.CamID,nLength,NULL,NULL);

				file.SeekToEnd();
				file.Write(&stData,sizeof(FTPListData));
			}*/
	}
	file.Close();

	int nStartIdx = 0,nEndIdx = 0;
	int nStartMov = 0,nEndMov = 0;

	ESMGetMovieIndex(pESMUtilMultiplexerDlg->m_nStartFrame,nStartMov,nStartIdx);
	ESMGetMovieIndex(pESMUtilMultiplexerDlg->m_nEndFrame  ,nEndMov	,nEndIdx);

	pESMUtilMultiplexerDlg->CheckMakingDSC();

	if(pESMUtilMultiplexerDlg->CheckConnect4DP() == 0)
		return 0;

	pESMUtilMultiplexerDlg->SendMuxingData(nStartMov,nEndMov,str4DLPath);

#ifndef _IBC
	CString strMovieFilePath;
	CString strState;
	CString strCmd, strOpt, strTemp;

	while(1)
	{
		if (pESMUtilMultiplexerDlg->m_nMuxDataSendCount <= pESMUtilMultiplexerDlg->m_nMjpgEncodingCount)
		{
			for (int i = 0 ; i < pESMUtilMultiplexerDlg->m_LcFTP.GetItemCount() ; i++)
			{
				strState = pESMUtilMultiplexerDlg->m_LcFTP.GetItemText(i, 2);
				if (strState == _T("complete"))
				{
					strOpt.Format(_T("-i %s\\%s.mjpg ")	 ,pESMUtilMultiplexerDlg->m_strMUXSavePathProfile
						,pESMUtilMultiplexerDlg->m_LcFTP.GetItemText(i, 1));
					strTemp.Append(strOpt);
				}
			}

			if(!fo.IsFileExist(pESMUtilMultiplexerDlg->m_str4DLSavePath))
				fo.CreateFolder(pESMUtilMultiplexerDlg->m_str4DLSavePath);

			strMovieFilePath.Format(_T("%s\\%s.mp4"),pESMUtilMultiplexerDlg->m_str4DLSavePath,ESMGetFrameRecord().Right(8));

			if(fo.IsFileExist(strMovieFilePath))
				if(!fo.Delete(strMovieFilePath))
				{
					CString strLog;
					strLog.Format(_T("Already Exist File %s"),strMovieFilePath);
					AfxMessageBox(strLog);

					break;
				}

				strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));

				CString strCount,strCnt;
				for(int idx = 0 ; idx < pESMUtilMultiplexerDlg->m_nMjpgEncodingCount; idx++)
				{
					strCnt.Format(_T("-map %d "),idx);
					strCount.Append(strCnt);
				}
				strOpt.Format(_T("%s-c copy %s %s"),strTemp ,strCount, strMovieFilePath);
				SHELLEXECUTEINFO lpExecInfo;
				lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
				lpExecInfo.lpFile = strCmd;
				lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
				lpExecInfo.hwnd = NULL;  
				lpExecInfo.lpVerb = L"open";
				lpExecInfo.lpParameters = strOpt;
				lpExecInfo.lpDirectory = NULL;

				lpExecInfo.nShow = SW_HIDE; // hide shell during execution
				lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
				ShellExecuteEx(&lpExecInfo);

				// wait until the process is finished
				if (lpExecInfo.hProcess != NULL)
				{
					::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
					::CloseHandle(lpExecInfo.hProcess);
				}

				break;
		}
		Sleep(10);
	}
#endif

	if(bBoth && bPSCP)
	{
		//str4DLPath
		pESMUtilMultiplexerDlg->RunHttp(str4DLPath,strFileName,_T("FTPList.txt"),HTTP_SEND_FILE);
		//(strFileName,_T("FTPList.txt"),HTTP_SEND_FILE);
	}

	pESMUtilMultiplexerDlg->WriteFinishFile(strStartTime);
	
	if(bBoth)
	{
		if(bPSCP)
		{
			//str4DLPath
			//pESMUtilMultiplexerDlg->RunHttp(str4DLPath,_T("finish.txt"),HTTP_SEND_FILE);
			pESMUtilMultiplexerDlg->RunHttp(str4DLPath,strFileName,_T("finish.txt"),HTTP_SEND_FILE);
		}
		else
		{
			CESMFileOperation fo;
			fo.Copy(strFileName,pESMUtilMultiplexerDlg->m_str4DLSavePathProfile);
		}
	}
#endif
	ESMLog(5,_T("CESMUtilMultiplexerDlg::MuxThreadDP() Finish"));
	
	if(bBoth)
	{
		CString strTFSavePath;
		CString strDate = pESMUtilMultiplexerDlg->m_strLogTime.Mid(0,8);
		CString strTime = pESMUtilMultiplexerDlg->m_strLogTime.Mid(9,14);

		strTFSavePath.Format(_T("%s/TF/%s/%s/%s/%s/%s"),
			pESMUtilMultiplexerDlg->m_str4DLSavePath,
			pESMUtilMultiplexerDlg->m_strSUBL,
			pESMUtilMultiplexerDlg->m_strSUBM,
			pESMUtilMultiplexerDlg->m_strSUBS,
			strDate,
			strTime);

		ESMLog(5,_T("Mux End[%d/%d] : %s"),
			pESMUtilMultiplexerDlg->m_nMuxDataSendCount,pESMUtilMultiplexerDlg->m_nTotalMakingCnt);

		ESMLog(5,_T("Windows: %s"),pESMUtilMultiplexerDlg->m_strMUXSavePathProfile);
		ESMLog(5,_T("Android: %s"),strTFSavePath);
	}
	else
	{
		if(pESMUtilMultiplexerDlg->m_nEncodingMethod == ENCODE_WINDOW)
		{
			ESMLog(5,_T("Mux End[%d/%d] : %s"),
			pESMUtilMultiplexerDlg->m_nMuxDataSendCount,pESMUtilMultiplexerDlg->m_nTotalMakingCnt,
			pESMUtilMultiplexerDlg->m_strMUXSavePathProfile);

			if(pESMUtilMultiplexerDlg->m_bCheckInputWindowsData == TRUE)
			{
				//wgkim 180611 Muxing Logic 적용
				CESMUtilMultiplexerMuxer pUtilMultiplexerMuxer(
					pESMUtilMultiplexerDlg->m_nRadioButtonForContainer,
					pESMUtilMultiplexerDlg->m_strMUXSavePath,
					pESMUtilMultiplexerDlg->m_strMp4SavePath);
				pUtilMultiplexerMuxer.CheckIfFTPDataReceived();
				pUtilMultiplexerMuxer.OpenFTPList();
				//pUtilMultiplexerMuxer.WriteSubtitleTest(
				//	pESMUtilMultiplexerDlg->m_strSUBL,
				//	pESMUtilMultiplexerDlg->m_strSUBM,
				//	pESMUtilMultiplexerDlg->m_strSUBS,
				//	pESMUtilMultiplexerDlg->m_strLogTime.Mid(0,8),
				//	pESMUtilMultiplexerDlg->m_strLogTime.Mid(9,14),
				//	pESMUtilMultiplexerDlg->m_strOBJ,
				//	pESMUtilMultiplexerDlg->m_strDES,
				//	pESMUtilMultiplexerDlg->m_strOwner);
				pUtilMultiplexerMuxer.MuxingAllMJPEGFiles(
					pESMUtilMultiplexerDlg->m_strSUBL,
					pESMUtilMultiplexerDlg->m_strSUBM,
					pESMUtilMultiplexerDlg->m_strSUBS,
					pESMUtilMultiplexerDlg->m_strLogTime.Mid(0,8),
					pESMUtilMultiplexerDlg->m_strLogTime.Mid(9,14),
					pESMUtilMultiplexerDlg->m_strOBJ,
					pESMUtilMultiplexerDlg->m_strDES,
					pESMUtilMultiplexerDlg->m_strOwner);
			}
		}
		else if(pESMUtilMultiplexerDlg->m_nEncodingMethod == ENCODE_ANDROID)
		{
			//http://13.57.98.197/webdav/4dapp/4DLive/TF/Sport/Baseball/DOOSAN/20180530/165813/
			CString strTFSavePath;
			CString strDate = pESMUtilMultiplexerDlg->m_strLogTime.Mid(0,8);
			CString strTime = pESMUtilMultiplexerDlg->m_strLogTime.Mid(9,14);

			strTFSavePath.Format(_T("%s/TF/%s/%s/%s/%s/%s"),
				pESMUtilMultiplexerDlg->m_str4DLSavePath,
				pESMUtilMultiplexerDlg->m_strSUBL,
				pESMUtilMultiplexerDlg->m_strSUBM,
				pESMUtilMultiplexerDlg->m_strSUBS,
				strDate,
				strTime);

			ESMLog(5,_T("Mux End[%d/%d] : %s"),
				pESMUtilMultiplexerDlg->m_nMuxDataSendCount,pESMUtilMultiplexerDlg->m_nTotalMakingCnt,
				strTFSavePath);
		}
	}

	pESMUtilMultiplexerDlg->m_bMUXing = FALSE;
	
	if(ESMGetValue(ESM_VALUE_DIRECTMUX))
		pESMUtilMultiplexerDlg->ShowWindow(FALSE);
	else
		AfxMessageBox(_T("DP MUX Finish!!!"));

	return 0;
}
unsigned WINAPI CESMUtilMultiplexerDlg::MjpgEncodingThread(LPVOID param)
{	
	MuxThreadData *pData = (MuxThreadData*)param;

	if (!pData)
		return 0;

	CESMUtilMultiplexerDlg* pDlg = pData->pMgr;
	CString strCamID = pData->strCamID;

	delete pData;
	
	CString strMovieFilePath;
	CString strMJPGFilePath;
	CString strCmd, strOpt;

	strMovieFilePath.Format(_T("%s\\%s.mp4"),pDlg->m_strMUXSavePathProfile,strCamID);
	strMJPGFilePath.Format(_T("%s\\%s.mjpg"),pDlg->m_strMUXSavePathProfile,strCamID);

	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));	
	strOpt.Format(_T("-i %s -c:v mjpeg -q:v 2 %s"),strMovieFilePath,strMJPGFilePath);
	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;

	CString strInsertData;

	strInsertData.Format(_T("complete"));

	for (int i = 0 ; i < arDSCList.GetSize() ; i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if (pItem->GetDeviceDSCID() == strCamID)
		{
			pDlg->m_LcFTP.SetItemText(i, 2, strInsertData);
		}
	}
	
	pDlg->m_nMjpgEncodingCount ++;
}
unsigned WINAPI CESMUtilMultiplexerDlg::FTPThread(LPVOID param)
{	
	CESMUtilMultiplexerDlg* pDlg = (CESMUtilMultiplexerDlg*)param;

	ESMLog(5,_T("CESMUtilMultiplexerDlg::FTPThread() Send Start"));

	CString strSendCheck;
	CString strDir;
	int nIndex = 0;

	CString strPort;
	strPort.Format(_T("%d"),pDlg->m_nFtpPort);

	if(pDlg->m_pConnection)
	{
		delete pDlg->m_pConnection;
		pDlg->m_pConnection = NULL;

		pDlg->m_pConnection = pDlg->m_session.GetFtpConnection(pDlg->m_strFtpIP, pDlg->m_strFtpID, pDlg->m_strFtpPW, (INTERNET_PORT) _ttoi(strPort));
	}else
		pDlg->m_pConnection = pDlg->m_session.GetFtpConnection(pDlg->m_strFtpIP, pDlg->m_strFtpID, pDlg->m_strFtpPW, (INTERNET_PORT) _ttoi(strPort));

	if(pDlg->m_pConnection)
	{
		pDlg->m_pConnection->GetCurrentDirectory(pDlg->m_strRemoteDir);
		pDlg->m_pFileFind = new CFtpFileFind(pDlg->m_pConnection);

		//pDlg->m_pConnection->GetCurrentDirectory(strDir);
		//pDlg->m_pConnection->SetCurrentDirectory(strDir);

		pDlg->m_pConnection->GetCurrentDirectory(strDir);
		strDir.Format(_T("%smux"),strDir);//, ESMGetFrameRecord());
		pDlg->m_pConnection->CreateDirectory(strDir);
		pDlg->m_pConnection->SetCurrentDirectory(strDir);
		pDlg->m_pConnection->GetCurrentDirectory(strDir);
		strDir.Format(_T("%s/%s"),strDir, ESMGetFrameRecord());
		pDlg->m_pConnection->CreateDirectory(strDir);
		pDlg->m_pConnection->SetCurrentDirectory(strDir);
		pDlg->m_pConnection->GetCurrentDirectory(strDir);
	}
	else
	{
		pDlg->m_pConnection = NULL;
		throw CString(L"m_Session.GetFtpConnection() failed");

		AfxMessageBox(_T("FTP Connect Fail!!"));
	}

	while(1)
	{
		if(pDlg->m_bFtpSend && pDlg->m_LcFTP.GetItemCount() > nIndex)
		{
			CString strMakeCheck = pDlg->m_LcFTP.GetItemText(nIndex,2);
			if(strMakeCheck == _T("complete"))
			{
				strSendCheck = pDlg->m_LcFTP.GetItemText(nIndex,3);
				if(strSendCheck.GetLength())
					nIndex++;
				else
				{
					CString strFile1, strFile2, strLog, strIndex;

					strIndex = pDlg->m_LcFTP.GetItemText(nIndex,0);

					if(_ttoi(strIndex) == 99999)
					{
						strFile2.Format(_T("%s"),pDlg->m_LcFTP.GetItemText(nIndex,1));
						strFile1.Format(_T("%s\\%s"), pDlg->m_strMUXSavePathProfile, strFile2);
						Sleep(1000);
					}
					else
					{
						strFile2.Format(_T("%s.mp4"),pDlg->m_LcFTP.GetItemText(nIndex,1));
						strFile1.Format(_T("%s\\%s"), pDlg->m_strMUXSavePathProfile, strFile2);
					}

					Sleep(1000);

					if(!pDlg->m_pConnection->PutFile(strFile1,strFile2 ))
					{
						pDlg->m_bFtpSend = FALSE;
						pDlg->UpdateData(FALSE);
						strLog.Format(_T("FTP Upload Fail! : %s"), strFile2);
						AfxMessageBox(strLog);
					}else
					{
						strLog.Format(_T("complete"));
						pDlg->m_LcFTP.SetItemText(nIndex, 3, strLog);

						pDlg->FileListUpdata(pDlg->m_LcFTP.GetItemText(nIndex,1), 3,strLog);

						nIndex++;
					}
				}
				Sleep(10);

			}
			else if(!strMakeCheck.GetLength())
				continue;
			else
				nIndex++;

		}
		else
			Sleep(100);
	}

	ESMLog(5,_T("CESMUtilMultiplexerDlg::FTPThread() Send End"));

	return 0;
}
void CESMUtilMultiplexerDlg::FTPListLoad()
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem *pItem;
	CString strInsertData;
	int nLength;
	FTPListData stData;

	for (int i = 0 ; i < arDSCList.GetSize() ; i++)
	{
		memset(&stData,NULL,sizeof(FTPListData));

		pItem = (CDSCItem*)arDSCList.GetAt(i);

		strInsertData.Format(_T("%d"), i+1);
		m_LcFTP.InsertItem(i, strInsertData);

		strInsertData.Format(_T("%s"), pItem->GetDeviceDSCID());
		m_LcFTP.SetItemText(i, 1, strInsertData);
	}
	//if(ESMGetValue(ESM_VALUE_DIRECTMUX) && ESMGetValue(ESM_VALUE_GPUMAKESKIP))
	//{
	//	CString strDay,strTime;
	//	GetTimePath(ESMGetFrameRecord(),strDay,strTime);
	//	m_strMUXSavePathProfile.Format(_T("%s\\%s\\%s"),m_strMUXSavePath,strDay,strTime);
	//}
	//else
	//{
	//	m_strMUXSavePathProfile.Format(_T("%s\\%s"),m_strMUXSavePath,ESMGetFrameRecord());
	//}

	//CFile file;
	//FTPListData stData;
	//CString strFileName;

	////if(ESMGetValue(ESM_VALUE_GPUMAKESKIP) && ESMGetValue(ESM_VALUE_DIRECTMUX))
	//strFileName.Format(_T("%s\\FTPList.txt"), m_strMUXSavePathProfile);

	//if(!file.Open(strFileName, CFile::modeRead))
	//{
	//	CESMFileOperation fo;
	//	if(!fo.IsFileFolder(m_strMUXSavePathProfile))
	//		fo.CreateFolder(m_strMUXSavePathProfile);

	//	file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite);

	//	CObArray arDSCList;
	//	ESMGetDSCList(&arDSCList);
	//	CDSCItem *pItem;
	//	CString strInsertData;
	//	int nLength;

	//	for (int i = 0 ; i < arDSCList.GetSize() ; i++)
	//	{
	//		memset(&stData,NULL,sizeof(FTPListData));

	//		pItem = (CDSCItem*)arDSCList.GetAt(i);

	//		strInsertData.Format(_T("%d"), i);
	//		m_LcFTP.InsertItem(i, strInsertData);
	//		nLength= strInsertData.GetLength();
	//		WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strInsertData,nLength,stData.Index,nLength,NULL,NULL);

	//		strInsertData.Format(_T("%s"), pItem->GetDeviceDSCID());
	//		m_LcFTP.SetItemText(i, 1, strInsertData);
	//		nLength= strInsertData.GetLength();
	//		WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strInsertData,nLength,stData.CamID,nLength,NULL,NULL);

	//		file.SeekToEnd();
	//		file.Write(&stData,sizeof(FTPListData));
	//	}

	//	return;
	//}

	//int iLength = (int)(file.GetLength());
	//if( iLength == 0 )
	//{
	//	file.Close();
	//	return;
	//}

	//while(iLength >= sizeof(FTPListData))
	//{
	//	file.Read(&stData,sizeof(FTPListData));

	//	int nCount = m_LcFTP.GetItemCount();
	//	CString strTemp;

	//	strTemp = stData.Index;
	//	m_LcFTP.InsertItem(nCount, strTemp);

	//	strTemp = stData.CamID;
	//	m_LcFTP.SetItemText(nCount, 1, strTemp);

	//	strTemp = stData.MovieMakeCheck;
	//	m_LcFTP.SetItemText(nCount, 2, strTemp);

	//	strTemp = stData.FTPSendCheck;
	//	m_LcFTP.SetItemText(nCount, 3, strTemp);

	//	iLength = iLength-sizeof(FTPListData);
	//}

	//int nColCount = m_LcFTP.GetItemCount();
	//m_LcFTP.SetItemState( -1, 0, LVIS_SELECTED|LVIS_FOCUSED );
	//m_LcFTP.SetItemState(nColCount - 1, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
	//m_LcFTP.EnsureVisible(nColCount - 1, FALSE);

	//file.Close();
}
void CESMUtilMultiplexerDlg::OnNMCustomdrawListMux(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	//LPNMLVCUSTOMDRAW pLVCD = reinterpret_cast<LPNMLVCUSTOMDRAW>(pNMHDR);

	//if(pLVCD->nmcd.dwDrawStage == CDDS_PREPAINT)
	//	*pResult = CDRF_NOTIFYITEMDRAW;
	//else if(pLVCD->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
	//{
	//	int nItem = static_cast<int>(pLVCD->nmcd.dwItemSpec);
	//	CString strTemp = m_LcFTP.GetItemText(nItem, 4/*pLVCD->iSubItem*/);
	//	if(!strTemp.IsEmpty())
	//	{
	//		if(strTemp == _T("1"))	//조건 분기 -> 컬러 변경
	//		{
	//			pLVCD->clrText		= RGB(0,0,0);
	//			pLVCD->clrTextBk	= RGB(0,155,218);; //blue
	//		}else if(strTemp == _T("2"))
	//		{
	//			pLVCD->clrText		= RGB(0,0,0);
	//			pLVCD->clrTextBk	= RGB(242,112,96); //red
	//		}else if(strTemp == _T("3"))
	//		{
	//			pLVCD->clrText		= RGB(0,0,0);
	//			pLVCD->clrTextBk	= RGB(13,177,75); //green
	//		}else if(strTemp == _T("4"))
	//		{
	//			pLVCD->clrText		= RGB(0,0,0);
	//			pLVCD->clrTextBk	= RGB(247,147,29); //orange
	//		}else
	//		{
	//			pLVCD->clrText		= RGB(0,0,0);
	//			pLVCD->clrTextBk	= RGB(255,255,255);
	//		}
	//	}
	//	//else
	//	//{
	//	//	//기본색상 - 화이트 배경 / 검정 텍스트
	//	//	pLVCD->clrText = RGB(0,0,0);
	//	//	pLVCD->clrTextBk = RGB(255,255,255);
	//	//}
	//	*pResult = CDRF_DODEFAULT;
	//}
}
void CESMUtilMultiplexerDlg::OnBnClickedMuxSetResolution()
{
	CString strResolution;
	m_combo.GetLBText(m_combo.GetCurSel(),strResolution);


	/*m_combo.AddString(_T("FULL-HD"));
	m_combo.AddString(_T("Ultra-HD"));
	m_combo.AddString(_T("CUSTOM"));*/
	/*if(strResolution == _T("FULL-HD"))
	{
		m_nOutHeight = 1080;
		m_nOutWidth = 1920;

		CString str;
		str.Format(_T("%d"),m_nOutWidth);
		GetDlgItem(IDC_MUX_OUTWIDTH)->SetWindowText(str);
		str.Format(_T("%d"),m_nOutHeight);
		GetDlgItem(IDC_MUX_OUTHEIGTH)->SetWindowText(str);

		GetDlgItem(IDC_MUX_OUTWIDTH)->EnableWindow(FALSE);
		GetDlgItem(IDC_MUX_OUTHEIGTH)->EnableWindow(FALSE);
	}
	else if(strResolution == _T("Ultra-HD"))
	{
		m_nOutHeight = 2160;
		m_nOutWidth = 3840;

		CString str;
		str.Format(_T("%d"),m_nOutWidth);
		GetDlgItem(IDC_MUX_OUTWIDTH)->SetWindowText(str);
		str.Format(_T("%d"),m_nOutHeight);
		GetDlgItem(IDC_MUX_OUTHEIGTH)->SetWindowText(str);

		GetDlgItem(IDC_MUX_OUTWIDTH)->EnableWindow(FALSE);
		GetDlgItem(IDC_MUX_OUTHEIGTH)->EnableWindow(FALSE);
	}
	else*/
	{
		CString str;
		GetDlgItem(IDC_MUX_OUTHEIGTH)->GetWindowText(str);
		int nHeight = _ttoi(str);
		m_nOutHeight = nHeight;
		GetDlgItem(IDC_MUX_OUTWIDTH)->GetWindowText(str);
		int nWidth = _ttoi(str);
		m_nOutWidth = nWidth;

		GetDlgItem(IDC_MUX_OUTWIDTH)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_OUTHEIGTH)->EnableWindow(TRUE);

		OnBnClickedMuxSave();
	}
}
void CESMUtilMultiplexerDlg::OnLvnItemchangedListMux(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if(pNMLV->uChanged == LVIF_STATE)
	{
		UINT nOldCheck = pNMLV->uOldState & LVIS_STATEIMAGEMASK;
		UINT nNewCheck = pNMLV->uNewState & LVIS_STATEIMAGEMASK;
		if(nOldCheck && nNewCheck && nOldCheck != nNewCheck)
		{
			BOOL bCheck = m_LcFTP.GetCheck(pNMLV->iItem);
			CString strCamID = m_LcFTP.GetItemText(pNMLV->iItem,1);
			CString strState = m_LcFTP.GetItemText(pNMLV->iItem,2);

			if(strState == "" || strState == "except")
			{
				if(bCheck)
					strState.Format(_T("except"));
				else
					strState.Format(_T(""));

				m_LcFTP.SetItemText(pNMLV->iItem, 2, strState);
				FileListUpdata(strCamID, 2, strState);
			}else
			{
				if(bCheck)
					m_LcFTP.SetCheck(pNMLV->iItem,FALSE);
			}
		}
	}

	*pResult = 0;
}
void CESMUtilMultiplexerDlg::OnBnClickedMuxBtnSetcolor()
{
	UpdateData(TRUE);
	if(!m_bColorTrans)
		return;

	for(int i = 0 ; i < 3; i++)
		m_dbArrColorInfo[i] = 0;
	
	int nIndex;
	nIndex = m_cbColorDSC.GetCurSel();
	m_cbColorDSC.GetLBText(nIndex,m_strBaseDSC);

	/*CString strData;
	GetDlgItemText(IDC_MUX_EDIT_BLUERATIO,strData);
	m_dbArrColorInfo[0] = _wtof(strData);
	GetDlgItemText(IDC_MUX_EDIT_GREENRATIO,strData);
	m_dbArrColorInfo[1] = _wtof(strData);
	GetDlgItemText(IDC_MUX_EDIT_REDRATIO,strData);
	m_dbArrColorInfo[2] = _wtof(strData);*/
}
void CESMUtilMultiplexerDlg::OnBnClickedMuxCheckColortrans()
{
	UpdateData(TRUE);

	if(m_bColorTrans)
	{
		GetDlgItem(IDC_MUX_EDIT_BLUERATIO)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_EDIT_GREENRATIO)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_EDIT_REDRATIO)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_MUX_EDIT_BLUERATIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_MUX_EDIT_GREENRATIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_MUX_EDIT_REDRATIO)->EnableWindow(FALSE);
	}
}
void CESMUtilMultiplexerDlg::SetMuxInfo()
{
	UpdateData(TRUE);	
#if 0
	UpdateData(TRUE);
	MuxInfoLoad();
	SetOptControl();
	InitColorDSCList();

	m_LcFTP.DeleteAllItems();

	FTPListLoad();

	//int nTotalRecTime = m_wndDSCViewer.m_pFrameSelector->GetRecTime();
	m_nSelectTime = m_pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();

	m_nStartTime = m_nSelectTime - (m_nTotalTime/2);
	if(m_nStartTime < 0)
		m_nStartTime = 0;

	m_nEndTime = m_nSelectTime + (m_nTotalTime/2);

	m_pConnection = NULL;
	m_pFileFind = NULL;

	m_nMuxDataSendCount = 0;
	m_nMuxDataFinishCount = 0;
	m_nMjpgEncodingCount = 0;

	//170622 hjcho
	memset(m_dbArrColorInfo,0,sizeof(double)*3);
	m_bColorTrans = FALSE;

	UpdateData(FALSE);

	if(ESMGetValue(ESM_VALUE_DIRECTMUX) && ESMGetValue(ESM_VALUE_GPUMAKESKIP))
	{
		OnBnClickedMuxStart();
	}
	if(m_nEncodingMethod == ENCODE_ANDROID && m_bPSCP == TRUE)
		OnBnClickedMuxDbBtnDbconntect();

	OnCbnEditchangeMuxDbComboPlayer();
#else
	MuxInfoLoad();
	SetOptControl();
	InitColorDSCList();

	m_LcFTP.DeleteAllItems();

	FTPListLoad();

	//int nTotalRecTime = m_wndDSCViewer.m_pFrameSelector->GetRecTime();
	m_nSelectTime = m_pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();

	if(ESMGetValue(ESM_VALUE_RTSP) == TRUE)
	{
		double dbSelTime = ((double) m_nSelectTime / 1000.) - (m_nSelectTime/1000);
		if(dbSelTime > 0.500)
		{
			m_nStartTime = m_nSelectTime - 1000;
			m_nEndTime = m_nSelectTime + 3000;
		}
		else
		{
			m_nStartTime = m_nSelectTime - 2000;
			m_nEndTime = m_nSelectTime + 2000;
		}
		if(m_nStartTime < 0)
			m_nStartTime = 0;
	}
	else
	{
		m_nStartTime = m_nSelectTime - (m_nTotalTime/2);
		if(m_nStartTime < 0)
			m_nStartTime = 0;

		m_nEndTime = m_nSelectTime + (m_nTotalTime/2);
	}

	m_pConnection = NULL;
	m_pFileFind = NULL;

	m_nMuxDataSendCount = 0;
	m_nMuxDataFinishCount = 0;
	m_nMjpgEncodingCount = 0;

	//170622 hjcho
	memset(m_dbArrColorInfo,0,sizeof(double)*3);
	m_bColorTrans = FALSE;

	UpdateData(FALSE);

	if(ESMGetValue(ESM_VALUE_RTSP) == TRUE)
	{
		ShowOrHideWindows(FALSE);
	}
	else
	{
		ShowOrHideWindows(TRUE);

		InitColorDSCList();

		m_pConnection = NULL;
		m_pFileFind = NULL;

		m_nMuxDataSendCount = 0;
		m_nMuxDataFinishCount = 0;
		m_nMjpgEncodingCount = 0;

		//170622 hjcho
		memset(m_dbArrColorInfo,0,sizeof(double)*3);
		m_bColorTrans = FALSE;

		if(ESMGetValue(ESM_VALUE_DIRECTMUX) && ESMGetValue(ESM_VALUE_GPUMAKESKIP))
		{
			OnBnClickedMuxStart();
		}
		if(m_nEncodingMethod == ENCODE_ANDROID && m_bPSCP == TRUE)
			OnBnClickedMuxDbBtnDbconntect();

		OnCbnEditchangeMuxDbComboPlayer();
	}
#endif
}
void CESMUtilMultiplexerDlg::GetTimePath(CString str4dm,CString &strDay,CString &strTime)
{
	strDay = str4dm.Left(10);
	strDay.Remove('_');

	strTime = str4dm.Right(8);
	strTime.Remove('_');
}
void CESMUtilMultiplexerDlg::OnBnClickedMuxBtnAdd()
{
	/*BOOL bCheck = m_LcFTP.GetCheck(pNMLV->iItem);
	CString strCamID = m_LcFTP.GetItemText(pNMLV->iItem,1);
	CString strState = m_LcFTP.GetItemText(pNMLV->iItem,2);

	if(strState == "" || strState == "except")
	{
		if(bCheck)
			strState.Format(_T("except"));
		else
			strState.Format(_T(""));

		m_LcFTP.SetItemText(pNMLV->iItem, 2, strState);
		FileListUpdata(strCamID, 2, strState);
	}else
	{
		if(bCheck)
			m_LcFTP.SetCheck(pNMLV->iItem,FALSE);
	}*/

	int nSelectedCount = m_LcFTP.GetSelectedCount();
	
	if(nSelectedCount >= 1)
	{
		POSITION pos = m_LcFTP.GetFirstSelectedItemPosition();

		while(pos)
		{
			int nDelId = m_LcFTP.GetNextSelectedItem(pos);

			BOOL bCheck = m_LcFTP.GetCheck(nDelId);
			CString strState = m_LcFTP.GetItemText(nDelId,2);
			CString strCamID = m_LcFTP.GetItemText(nDelId,1);

			if(bCheck)
			{
				continue;
				/*m_LcFTP.SetCheck(nDelId,FALSE);
				strState.Format(_T(""));*/
			}
			else
			{
				m_LcFTP.SetCheck(nDelId,TRUE);
				strState.Format(_T("except"));			
			}
			m_LcFTP.SetItemText(nDelId, 2, strState);
			FileListUpdata(strCamID, 2, strState);
		}
		/*for(int i = nSelectedCount; i >= 0; i--)//while(pos != NULL)
		{
			POSITION pos = m_LcFTP.GetFirstSelectedItemPosition();

			
		}*/
	}

	UpdateData(FALSE);
}
void CESMUtilMultiplexerDlg::OnBnClickedMuxBtnSub()
{
	int nSelectedCount = m_LcFTP.GetSelectedCount();
	
	if(nSelectedCount >= 1)
	{
		POSITION pos = m_LcFTP.GetFirstSelectedItemPosition();

		while(pos)
		{
			int nDelId = m_LcFTP.GetNextSelectedItem(pos);

			BOOL bCheck = m_LcFTP.GetCheck(nDelId);
			CString strState = m_LcFTP.GetItemText(nDelId,2);
			CString strCamID = m_LcFTP.GetItemText(nDelId,1);

			if(bCheck)
			{
				m_LcFTP.SetCheck(nDelId,FALSE);
				strState.Format(_T(""));
			}
			else
			{
				continue;
				/*m_LcFTP.SetCheck(nDelId,TRUE);
				strState.Format(_T("except"));	*/		
			}
			m_LcFTP.SetItemText(nDelId, 2, strState);
			FileListUpdata(strCamID, 2, strState);
		}
		/*for(int i = nSelectedCount; i >= 0; i--)//while(pos != NULL)
		{
			POSITION pos = m_LcFTP.GetFirstSelectedItemPosition();

			
		}*/
	}

	UpdateData(FALSE);
}
void CESMUtilMultiplexerDlg::OnBnClickedMuxDbNaming()
{
	UpdateData(TRUE);
	if(m_bThird)
	{
		m_bThird = FALSE;
		ESMLog(5,_T("[MUX] 1/3 Close"));
	}
	else
	{
		m_bThird = TRUE;
		ESMLog(5,_T("[MUX] 1/3 Start"));
		if(m_bHalf)
		{
			CheckDlgButton(IDC_MUX_HALF,FALSE);
			m_bHalf = FALSE;
		}
		//CheckDlgButton(IDC_MUX_HALF,TRUE);
	}
	UpdateData(FALSE);
	/*if(m_strSUBL.GetLength())
	{
	AfxMessageBox(_T("대분류를 입력하세요"));
	return;
	}
	if(m_strSUBM.GetLength())
	{
	AfxMessageBox(_T("중분류 입력하세요"));
	return;
	}
	if(m_strSUBS.GetLength())
	{
	AfxMessageBox(_T("소분류 입력하세요"));
	return;
	}
	GetDlgItem(IDC_MUX_DB_EDIT_OBJ)->GetWindowText(m_strOBJ);
	if(m_strOBJ.GetLength())
	{
	AfxMessageBox(_T("상황 입력하세요"));
	return;
	}

	GetDlgItem(IDC_MUX_DB_EDIT_OBJ)->GetWindowText(m_strDES);
	if(m_strDES.GetLength())
	{
	AfxMessageBox(_T("설명 입력하세요"));
	return;
	}

	if(m_strOwner.GetLength())
	{
	AfxMessageBox(_T("소유자 입력하세요"));
	return;
	}*/
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//m_pMuxOptDlg->InitData();

	//if(m_pMuxOptDlg->DoModal() == IDOK)
	//{
	//	//AfxMessageBox(_T("Second"));		
	//	m_strSUBL = m_pMuxOptDlg->m_strSUBL;
	//	//m_strSUBL.Append(_T("\r\n"));
	//	m_strSUBM = m_pMuxOptDlg->m_strSUBM;
	//	//m_strSUBM.Append(_T("\r\n"));
	//	m_strSUBS = m_pMuxOptDlg->m_strSUBS;
	//	//m_strSUBS.Append(_T("\r\n"));
	//	m_strOBJ = m_pMuxOptDlg->m_strOBJ;
	//	//m_strOBJ.Append(_T("\r\n"));
	//	m_strDES = m_pMuxOptDlg->m_strDES;
	//	//m_strDES.Append(_T("\r\n"));
	//	m_strOwner = m_pMuxOptDlg->m_strOwn;
	//	//m_strOwner.Append(_T("\r\n"));
	//}
}
void CESMUtilMultiplexerDlg::WriteMovieInfo(CFile* pFile)
{
	char strDest[512];
	memset(strDest, 0, 512);
	CString strMake = _T("\r\n");
	int nLength;
	CString strRevision;

	//Type
	CString strTF = _T("Type: TF\r\n");
	//nLength = strTF.GetLength();
	nLength = WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strTF,-1,NULL,0,NULL,NULL);
	WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strTF,nLength,strDest,nLength,NULL,NULL);
	pFile->SeekToEnd();
	pFile->Write(&strDest,sizeof(char)*nLength-1);
	//nLength = strMake.GetLength();
	//WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strMake,nLength,strDest,nLength,NULL,NULL);
	//pFile->SeekToEnd();
	//pFile->Write(&strDest,sizeof(char)*nLength);

	//SUBL
	strRevision.Format(_T("SubL: %s\r\n"),m_strSUBL);
	//nLength = strRevision.GetLength();
	nLength = WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strRevision,-1,NULL,0,NULL,NULL);
	WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strRevision,nLength,strDest,nLength,NULL,NULL);
	pFile->SeekToEnd();
	pFile->Write(&strDest,sizeof(char)*nLength-1);
	//nLength = strMake.GetLength();
	//WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strMake,nLength,strDest,nLength,NULL,NULL);
	//pFile->SeekToEnd();
	//pFile->Write(&strDest,sizeof(char)*nLength);

	//SUBM
	strRevision.Format(_T("SubM: %s\r\n"),m_strSUBM);
	//nLength = strRevision.GetLength();
	nLength = WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strRevision,-1,NULL,0,NULL,NULL);
	WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strRevision,nLength,strDest,nLength,NULL,NULL);
	pFile->SeekToEnd();
	pFile->Write(&strDest,sizeof(char)*nLength-1);
	//nLength = strMake.GetLength();
	//WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strMake,nLength,strDest,nLength,NULL,NULL);
	//pFile->SeekToEnd();
	//pFile->Write(&strDest,sizeof(char)*nLength);

	//SUBS
	strRevision.Format(_T("SubS: %s\r\n"),m_strSUBS);
	//nLength = strRevision.GetLength();
	nLength = WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strRevision,-1,NULL,0,NULL,NULL);
	WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strRevision,nLength,strDest,nLength,NULL,NULL);
	pFile->SeekToEnd();
	pFile->Write(&strDest,sizeof(char)*nLength-1);

	//Object
	strRevision.Format(_T("Object: %s\r\n"),m_strOBJ);
	//nLength = strRevision.GetLength();
	nLength = WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strRevision,-1,NULL,0,NULL,NULL);
	WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strRevision,nLength,strDest,nLength,NULL,NULL);
	pFile->SeekToEnd();
	pFile->Write(&strDest,sizeof(char)*nLength-1);

	//Description
	strRevision.Format(_T("Description: %s\r\n"),m_strDES);
	//nLength = strRevision.GetLength();
	nLength = WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strRevision,-1,NULL,0,NULL,NULL);
	WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strRevision,nLength,strDest,nLength,NULL,NULL);
	pFile->SeekToEnd();
	pFile->Write(&strDest,sizeof(char)*nLength-1);

	//Owner
	strRevision.Format(_T("Owner: %s\r\n"),m_strOwner);
	//nLength = strRevision.GetLength();
	nLength = WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strRevision,-1,NULL,0,NULL,NULL);
	WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strRevision,nLength,strDest,nLength,NULL,NULL);
	pFile->SeekToEnd();
	pFile->Write(&strDest,sizeof(char)*nLength-1);
}
void CESMUtilMultiplexerDlg::OnBnClickedMuxMakedp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	if(m_bMakeDP)
	{
		GetDlgItem(IDC_MUX_CHECK_TCPSEND)->EnableWindow(FALSE);//임시 비활성화
		//if(m_nEncodingMethod != ENCODE_WINDOW)
		//GetDlgItem(IDC_MUX_CHECK_PSCP)->EnableWindow(TRUE);//임시비활성화
		//GetDlgItem(IDC_MUX_RADIO_ANDROID)->EnableWindow(TRUE);
		//GetDlgItem(IDC_MUX_RADIO_WINDOW)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_BOTHPROC)->EnableWindow(TRUE);
		if(m_nEncodingMethod != ENCODE_WINDOW)
			GetDlgItem(IDC_MUX_CHECK_PSCP)->EnableWindow(TRUE);//임시비활성화
		if(m_bBothProc)
		{
			GetDlgItem(IDC_MUX_RADIO_ANDROID)->EnableWindow(FALSE);
			GetDlgItem(IDC_MUX_RADIO_WINDOW)->EnableWindow(FALSE);
		}
	}
	else
	{
		GetDlgItem(IDC_MUX_RADIO_ANDROID)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_RADIO_WINDOW)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_CHECK_TCPSEND)->EnableWindow(FALSE);
		GetDlgItem(IDC_MUX_CHECK_PSCP)->EnableWindow(FALSE);
		//GetDlgItem(IDC_MUX_RADIO_ANDROID)->EnableWindow(FALSE);
		//GetDlgItem(IDC_MUX_RADIO_WINDOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_MUX_BOTHPROC)->EnableWindow(FALSE);
		CheckDlgButton(IDC_MUX_CHECK_PSCP,FALSE);
		m_bPSCP = FALSE;
		OnBnClickedMuxCheckPscp();
	}
}
void CESMUtilMultiplexerDlg::RunHttp(CString strPath,CString strFilePath,CString strFileName,int nCase)
{
	CString strOpt;
	BOOL bRun = TRUE;
	strOpt.Format(_T("-u esmlab:5dldptmdpa! "));
	switch(nCase)
	{
	case HTTP_SEND_FILE:
		{
			strOpt.Append(_T("-T "));
			strOpt.Append(strFilePath);
			strOpt.Append(_T(" "));
			strOpt.Append(strPath+_T("/")+strFileName);
		}
		break;
	case HTTP_CREATE_DIR:
		{
			//CString strID = _T("-u esmlab:5dldptmdpa! -X MKCOL ");
			strOpt.Append(_T("-X MKCOL "));
			strOpt.Append(strPath);
		}
		break;
	case HTTP_DELETE_DIR:
		{
			strOpt.Append(_T("-X DELETE "));
			strOpt.Append(strPath+_T("/"));
		}
		break;
	case HTTP_DELETE_FILE:
		{
			strOpt.Append(_T("-X DELETE "));
			strOpt.Append(strPath+_T("/"));
			strOpt.Append(strFileName);
		}
		break;
	case HTTP_GET_FILE:
		{
			strOpt.Append(_T("-o "));
			strOpt.Append(strFilePath);//저장경로
			strOpt.Append(_T(" "));
			strOpt.Append(strPath);
			//strOpt.Append(_T(" "));
		}
		break;
	default:
		{
			bRun = FALSE;
		}
		break;
	}

	if(bRun)
	{
		CString strHomePath,strTest;
		strHomePath.Format(_T("%s\\bin\\curl.exe"),ESMGetPath(ESM_PATH_HOME));
		strTest.Format(_T("%s\\bin"),ESMGetPath(ESM_PATH_HOME));
		SHELLEXECUTEINFO lpExecInfo;
		lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
		lpExecInfo.lpFile = strHomePath;//_T("C:\\Users\\Administrator\\Desktop\\curl\\curl.exe");
		lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
		lpExecInfo.hwnd = NULL;  
		lpExecInfo.lpVerb = L"open";
		lpExecInfo.lpParameters = strOpt;
		lpExecInfo.lpDirectory = strTest;
		//lpExecInfo.nShow	=SW_SHOW;
		lpExecInfo.nShow = SW_HIDE; // hide shell during execution
		lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
		ShellExecuteEx(&lpExecInfo);

		// wait until the process is finished
		if (lpExecInfo.hProcess != NULL)
		{
			::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
			::CloseHandle(lpExecInfo.hProcess);
		}
		lpExecInfo.lpFile = _T("exit");
		lpExecInfo.lpVerb = L"close";
		lpExecInfo.lpParameters = NULL;

		if(TerminateProcess(lpExecInfo.hProcess,0))
		{
			lpExecInfo.hProcess = 0;
			ESMLog(5,_T("End"));
		}
	}
}
void CESMUtilMultiplexerDlg::OnBnClickedMuxRadioAndroid(UINT value)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	switch(m_nEncodingMethod)
	{
	case ENCODE_WINDOW:
		GetDlgItem(IDC_MUX_FILESAVEPATH)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_4DLSAVEPATH)->EnableWindow(FALSE);
		GetDlgItem(IDC_MUX_CHECK_PSCP)->EnableWindow(FALSE);
		TRACE("m_bPSCP: %d\n",m_bPSCP);
		if(IsDlgButtonChecked(IDC_MUX_CHECK_PSCP))
			CheckDlgButton(IDC_MUX_CHECK_PSCP,FALSE);

		GetDlgItem(IDC_MUX_DB_BTN_DBCONNTECT)->EnableWindow(TRUE);

		TRACE("m_bPSCP: %d\n",m_bPSCP);

		m_dbConnect = FALSE;
		mysql_close(&mysql);

		CreateDBInfo();
		CreateDBOwnerInfo();

		//wgkim
		GetDlgItem(IDC_MUX_MP4SAVEPATH)->EnableWindow(TRUE);
		break;
	case ENCODE_ANDROID:
		//ESMLog(5,_T("ANDROID"));
		//ClearDBInformation();
		//if(m_nEncodingMethod == ENCODE_ANDROID && m_bPSCP == TRUE)
		OnBnClickedMuxDbBtnDbconntect();
		CreateDBInfo();
		CreateDBOwnerInfo();

		GetDlgItem(IDC_MUX_FILESAVEPATH)->EnableWindow(FALSE);
		GetDlgItem(IDC_MUX_4DLSAVEPATH)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_CHECK_PSCP)->EnableWindow(TRUE);
		if(m_bMakeDP == FALSE)
			GetDlgItem(IDC_MUX_CHECK_PSCP)->EnableWindow(FALSE);
		if(!IsDlgButtonChecked(IDC_MUX_CHECK_PSCP))
			CheckDlgButton(IDC_MUX_CHECK_PSCP,TRUE);
		//m_bPSCP = TRUE;
		//wgkim
		GetDlgItem(IDC_MUX_MP4SAVEPATH)->EnableWindow(FALSE);
		break;
	case ENCODE_IOS:
		//ESMLog(5,_T("IOS"));
		break;
	}
}
void CESMUtilMultiplexerDlg::OnBnClickedMuxBothproc()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	if(m_bBothProc)
	{
		GetDlgItem(IDC_MUX_RADIO_ANDROID)->EnableWindow(FALSE);
		GetDlgItem(IDC_MUX_RADIO_WINDOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_MUX_FILESAVEPATH)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_4DLSAVEPATH)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_CHECK_PSCP)->EnableWindow(TRUE);
		//GetDlgItem(IDC_MUX_CHECK_PSCP)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_MUX_RADIO_ANDROID)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_RADIO_WINDOW)->EnableWindow(TRUE);
		OnBnClickedMuxRadioAndroid(m_nEncodingMethod);
	}
	//OnBnClickedMuxRadioAndroid(m_nEncodingMethod);
}
void CESMUtilMultiplexerDlg::CreateFTPList()
{

}
void CESMUtilMultiplexerDlg::CheckMakingDSC()
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CDSCItem* pItem = NULL;
	CString strInsertData;
	vector<BOOL>ArrCheck(arDSCList.GetSize());
	vector<BOOL>ArrExcept(arDSCList.GetSize());
	for(int i = 0 ; i < arDSCList.GetSize(); i++)
	{
		ArrCheck.at(i)  = FALSE;
		ArrExcept.at(i) = TRUE;
	}
	int nCheckCnt = 0;
	
	m_strBaseDSC;

	for (int i = 0 ; i < arDSCList.GetSize() ; i++)
	{
		ArrCheck.at(i) = TRUE;
		pItem = (CDSCItem*)arDSCList.GetAt(i);

		CString strExcept = m_LcFTP.GetItemText(i,2);
		if(strExcept.GetLength())
		{
			strInsertData.Format(_T("Except"));
			m_LcFTP.SetItemText(i,2,strInsertData);
			FileListUpdata(m_LcFTP.GetItemText(i,1),2,strInsertData);

			ArrCheck.at(i) = FALSE;
			continue;
		}

		if(pItem)
		{
			if(pItem->GetSensorSync() == FALSE)
			{
				strInsertData.Format(_T("Sync ERR"));
				m_LcFTP.SetItemText(i,2,strInsertData);
				FileListUpdata(m_LcFTP.GetItemText(i,1),2,strInsertData);
				ESMLog(5,_T("Sync ERR Mux Movie [%s]"),pItem->GetDeviceDSCID());
				ArrCheck.at(i) = FALSE;
				continue;
			}
		}else
		{
			strInsertData.Format(_T("DSC Item ERR"));
			m_LcFTP.SetItemText(i, 2, strInsertData);
			ArrCheck.at(i) = FALSE;
			continue;
		}
		//file check
		CString strPrePath;
		CString strCurPath;
		int bFileCheckERR = FALSE;
		BOOL bColorFinish = FALSE;
		for(int j = m_nStartFrame ; j <= m_nEndFrame ; j++)
		{
			strCurPath.Format(_T("%s"), ESMGetMoviePath(pItem->GetDeviceDSCID(), j));
			
			if(m_bColorTrans && bColorFinish == FALSE)
			{
				if(m_strBaseDSC == pItem->GetDeviceDSCID())
				{
					int nRealFrameIdx = 0,nMovieIdx = 0;
					ESMGetMovieIndex(j,nMovieIdx,nRealFrameIdx);
					bColorFinish = DecodingFrame(strCurPath,nRealFrameIdx);
				}
			}

			if(bColorFinish == TRUE)
				continue;

			if(strCurPath.CompareNoCase(strPrePath) == 0 && i != 0)
				continue;

			else
			{
				strPrePath = strCurPath;
				if(!CheckFile(strCurPath))
				{
					strInsertData.Format(_T("File ERR"));
					m_LcFTP.SetItemText(i, 2, strInsertData);
					FileListUpdata(m_LcFTP.GetItemText(i,1), 2,strInsertData);
					ESMLog(5,_T("File ERR Mux Movie [%s]"),pItem->GetDeviceDSCID());
					bFileCheckERR = TRUE;
					break;
				}
			}
		}
		if(bFileCheckERR)
		{
			ArrCheck.at(i) = FALSE;
			continue;
		}
	}

	if(m_bHalf && ArrCheck.size() != 0)
	{
		CString strHalf = _T("Half");
		int nRevCnt = 0;

		for(int i = 0 ; i < arDSCList.GetCount(); i++)
		{
			if(i==0)
				continue;

			if(ArrCheck.at(i) == TRUE && ArrCheck.at(i-1) == TRUE)//메이킹 제외
			{
				if(nRevCnt > 1)//연속적으로 포함되지 않을 때 일시 메이킹 포함
				{
					nRevCnt = 0;
					continue;
				}

				if(i == ArrCheck.size() - 1 && i+1 < arDSCList.GetCount() - 1)
					if(ArrCheck.at(i+1) == FALSE)
						continue;
				m_LcFTP.SetItemText(i,2,strHalf);
				FileListUpdata(m_LcFTP.GetItemText(i,1), 2,strHalf);
				ArrCheck.at(i) = FALSE;
			}
			else if(ArrCheck.at(i)== TRUE && ArrCheck.at(i-1) == FALSE)//메이킹 포함
			{
				continue;
			}
			else if(ArrCheck.at(i)  == FALSE && ArrCheck.at(i-1) == TRUE)//메이킹 포함
			{
				continue;
			}
			else if(ArrCheck.at(i) == FALSE && ArrCheck.at(i-1) == FALSE)
			{
				nRevCnt++;
			}
		}
	}
	else if(m_bThird && ArrCheck.size() != 0)
	{
		CString strHalf = _T("1/3");
		int nRevCnt = 0;
		//m_LcFTP.SetItemText(i,2,strHalf);
		for(int i = 0 ; i < arDSCList.GetCount(); i++)
		{
			if(i==0 || i == 1)
				continue;
			
			if(ArrCheck.at(i-1) == TRUE && ArrCheck.at(i) == TRUE)
			{
				ArrCheck.at(i) = FALSE;
				CString strTemp;
				strTemp = m_LcFTP.GetItemText(i,2);
				if(strTemp.GetLength())
					continue;
				else
					strHalf = _T("1/3");

				m_LcFTP.SetItemText(i,2,strHalf);
			}
			else if(ArrCheck.at(i-1) == TRUE && ArrCheck.at(i) == FALSE)
			{
				CString strTemp;
				strTemp = m_LcFTP.GetItemText(i,2);
				if(strTemp.GetLength())
					continue;
				else
					strHalf = _T("1/3");

				m_LcFTP.SetItemText(i,2,strHalf);
			}
			else if(ArrCheck.at(i-2) == TRUE && ArrCheck.at(i-1) == FALSE)
			{
				ArrCheck.at(i) = FALSE;
				CString strTemp;
				strTemp = m_LcFTP.GetItemText(i,2);
				if(strTemp.GetLength())
					continue;
				else
					strHalf = _T("1/3");

				m_LcFTP.SetItemText(i,2,strHalf);
			}
			else if(ArrCheck.at(i-2) == FALSE && ArrCheck.at(i-1) == FALSE)
				continue;
		}
	}
	//m_bMUXing = FALSE;
	//ESMLog(5,_T("Finish"));
	//return;
}
int CESMUtilMultiplexerDlg::CheckConnect4DP()
{
	int nStartNetCheck = GetTickCount();
	int nNetCount = m_pMainWnd->m_wndNetworkEx.GetRCMgrCount();

	for (int i = 0 ; i < nNetCount ; i++)
	{
		CESMRCManager *pRCMgr = m_pMainWnd->m_wndNetworkEx.GetRCMgr(i);

		if (pRCMgr->m_bConnected && pRCMgr->m_bGPUUse)
		{
			//pRCMgr->ConnectCheck(ESMGetFrameRate());	//180308 카메라타입 및 해상도 타입 동시전송
			pRCMgr->ConnectCheck(ESMGetIdentify());
			m_arRCServerList.Add((CObject*)pRCMgr);
		}
	}

	nNetCount = m_pMainWnd->m_wndNetwork.GetRCMgrCount();

	for (int i = 0 ; i < nNetCount ; i++)
	{
		CESMRCManager *pRCMgr = m_pMainWnd->m_wndNetwork.GetRCMgr(i);

		if (pRCMgr->m_bConnected && pRCMgr->m_bGPUUse)
		{
			//pRCMgr->ConnectCheck(ESMGetFrameRate());	//180308 카메라타입 및 해상도 타입 동시전송
			pRCMgr->ConnectCheck(ESMGetIdentify());
			m_arRCServerList.Add((CObject*)pRCMgr);
		}
	}
	int nEndNetCheck = GetTickCount();

	CString strNetCheck;
	strNetCheck.Format(_T("NetCheck : %d"), nEndNetCheck - nStartNetCheck);
	ESMLog(5, strNetCheck);


	if (m_arRCServerList.GetCount() < 1)
	{
		AfxMessageBox(_T("can not Connect 4D Processor"));

		m_bMUXing = FALSE;
		return 0;
	}
	else
		return m_arRCServerList.GetCount();
}
void CESMUtilMultiplexerDlg::SendMuxingData(int nStartMov,int nEndMov,CString str4DLPath)
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	m_nMuxDataSendCount = 0;
	m_nMuxDataFinishCount = 0;
	m_nMjpgEncodingCount = 0;
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();

	CDSCItem* pItem = NULL;
	
	for (int i = 0 ; i < arDSCList.GetSize() ; i++)
	{
		CString strCheck = m_LcFTP.GetItemText(i,2);
		if (strCheck.GetLength())
			continue;

		pItem = (CDSCItem*)arDSCList.GetAt(i);

		MuxDataInfo pMuxDataInfo;

		pMuxDataInfo.nOutWidth		= m_nOutWidth;
		pMuxDataInfo.nOutHeight		= m_nOutHeight;

		pMuxDataInfo.nStartFrame	= m_nStartFrame;
		pMuxDataInfo.nEndFrame		= m_nEndFrame;

		pMuxDataInfo.bMovieReverse = pItem->m_bReverse;

		if(m_dbZoomRatio < 100)
			m_dbZoomRatio = 100;

		pMuxDataInfo.dbZoomRatio	= m_dbZoomRatio;

		//wgkim 190113
		CESMTemplateMgr* pTemplateMgr = m_pMainWnd->m_pTemplateMgr;

		CPoint ptCenterPoint = CPoint(0, 0);
		if(pTemplateMgr != NULL && pTemplateMgr->IsArrTemplatePointValid() == TRUE)
		{
			pItem->m_nIndex;
			Point2f ptTempPoint= pTemplateMgr->GetTemplatePoint(pItem->m_nIndex-1).viewPoint;

			ptCenterPoint.x = ptTempPoint.x;
			ptCenterPoint.y = ptTempPoint.y;

			pMuxDataInfo.ptCenterPoint = ptCenterPoint;
		}

		if(m_bInsertCamID)
			pMuxDataInfo.bInserID	= !pItem->GetDeviceDSCID().IsEmpty();
		
		_stprintf(pMuxDataInfo.strCamID,pItem->GetDeviceDSCID());

		if(m_bPSCP && m_nEncodingMethod == ENCODE_ANDROID)
			pMuxDataInfo.bPSCP		= TRUE;
		else
			if(m_bBothProc && m_bPSCP)
				pMuxDataInfo.bPSCP		= TRUE;
			else
				pMuxDataInfo.bPSCP		= FALSE;

		CString strPath = ESMGetMoviePath(pItem->GetDeviceDSCID(), m_nStartFrame);
		int nFind = strPath.ReverseFind(_T('\\'));
		_stprintf(pMuxDataInfo.strPath, _T("%s"), strPath.Left(nFind+1));

		pMuxDataInfo.bMuxBoth		 = m_bBothProc;
		pMuxDataInfo.nEncodingMethod = m_nEncodingMethod;

		CString strFile;
		if(m_bBothProc)
		{
			_stprintf(pMuxDataInfo.strSavePath[ENCODE_WINDOW]	,m_strMUXSavePathProfile);
			strFile.Format(_T("%s.mp4"),pItem->GetDeviceDSCID());
			_stprintf(pMuxDataInfo.strFileName[ENCODE_WINDOW],strFile);

			_stprintf(pMuxDataInfo.strSavePath[ENCODE_ANDROID]	,m_str4DLSavePathProfile);
			strFile.Format(_T("%s_%s_h_%d.ts"),m_strDay,m_strTime,i);
			_stprintf(pMuxDataInfo.strFileName[ENCODE_ANDROID],strFile);
		}
		else
		{
			if(m_nEncodingMethod == ENCODE_WINDOW)
			{
				_stprintf(pMuxDataInfo.strSavePath[ENCODE_WINDOW]	,m_strMUXSavePathProfile);
				strFile.Format(_T("%s.mp4"),pItem->GetDeviceDSCID());
				_stprintf(pMuxDataInfo.strFileName[ENCODE_WINDOW],strFile);
			}
			else if(m_nEncodingMethod == ENCODE_ANDROID)
			{
				_stprintf(pMuxDataInfo.strSavePath[ENCODE_ANDROID]	,m_str4DLSavePathProfile);
				strFile.Format(_T("%s_%s_h_%d.ts"),m_strDay,m_strTime,i);
				_stprintf(pMuxDataInfo.strFileName[ENCODE_ANDROID],strFile);
			}
		}

		//170622 hjcho
		if(m_bColorTrans)
		{
			memcpy(pMuxDataInfo.dbArrColorInfo,m_dbArrColorInfo,3*sizeof(double));
			pMuxDataInfo.bColorTrans = TRUE;
		}

		//180824 hjcho
		pMuxDataInfo.bLocal		 = FALSE;
		pMuxDataInfo.bLowBitrate = m_bLowBitrate;

		//190214 wgkim
		pMuxDataInfo.nBitrate	 = m_nBitrateInMbps * 1024 * 1024;
		pMuxDataInfo.nSelectedContainer = m_nRadioButtonForContainer;

		_stprintf(pMuxDataInfo.strHomePath,_T("%s\\bin\\"),ESMGetPath(ESM_PATH_HOME));

		while(1)
		{
			//메이킹 서버가 메이킹시 Mux 작업 일시 중지
			if (GetMakingInfoInt(0))
				Sleep(10);
			else
				break;
		}

		BOOL bFlag = TRUE;
		while (bFlag)
		{
			for (int i = 0 ; i < m_arRCServerList.GetCount() ; i++)
			{
				CESMRCManager *pRCMgr =  (CESMRCManager*)m_arRCServerList.GetAt(i);
				if(pRCMgr->m_bMUXRunning)
					continue;
				else
				{
				//if(m_bTCP)
				//	{
				//		CString strDSCID = pItem->GetDeviceDSCID();

				//		if(ESMGetMuxSendCnt() != 0)
				//			continue;

				//		Sleep(500);

				//		int nTotalCnt = nEndMov - nStartMov + 1;
				//		ESMSetMuxSendCnt(nTotalCnt);
				//		pRCMgr->m_bMuxTCP = TRUE;

				//		_stprintf(pMuxDataInfo.strPath, _T("M:\\Movie\\%s\\%s_"), strDSCID,strDSCID);
				//		pMuxDataInfo.bSendTo4DP = TRUE;

				//		for(int j = nStartMov ; j <= nEndMov ; j++)
				//		{
				//			CString strSendPath,strSavePath;

				//			strSendPath.Format(_T("%s%d.mp4"),strPath.Left(nFind+1),j);
				//			strSavePath.Format(_T("M:\\Movie\\%s\\%s_%d.mp4"),strDSCID,strDSCID,j);
				//			//ESMLog(5,_T("[%d] - %s"),j-nStartMov,strSendPath);

				//			//Send Movie Using TCP/IP
				//			pRCMgr->RequestData(strSendPath,strSavePath,nTotalCnt--);

				//			Sleep(10);
				//		}
				//	}
				//	else
					pRCMgr->m_bMuxTCP = FALSE;

					pRCMgr->m_bMUXRunning = TRUE;
					pRCMgr->SendMakeMuxData(&pMuxDataInfo);
					m_nMuxDataSendCount++;
					bFlag = FALSE;

					/*if(pMuxDataInfo.pAdjInfo)
					{
					delete pMuxDataInfo.pAdjInfo;
					pMuxDataInfo.pAdjInfo = NULL;
					}*/
 					break;
				}
			}
			Sleep(10);
		}
	}
	while(1)
	{
		if(m_nMuxDataSendCount <= m_nMuxDataFinishCount)
			break;

		Sleep(10);
	}
}
void CESMUtilMultiplexerDlg::SendMuxingDataLocal(int nStartMov,int nEndMov,CString str4DLPath)
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();

	m_nMuxDataSendCount = 0;
	m_nMuxDataFinishCount = 0;
	m_nMjpgEncodingCount = 0;

	CDSCItem* pItem = NULL;
	for (int i = 0 ; i < arDSCList.GetSize() ; i++)
	{
		CString strCheck = m_LcFTP.GetItemText(i,2);
		if (strCheck.GetLength())
			continue;
	
		pItem = (CDSCItem*)arDSCList.GetAt(i);

		MuxDataInfo* pMuxDataInfo = new MuxDataInfo;

		pMuxDataInfo->nOutWidth		= m_nOutWidth;
		pMuxDataInfo->nOutHeight	= m_nOutHeight;

		pMuxDataInfo->nStartFrame	= m_nStartFrame;
		pMuxDataInfo->nEndFrame		= m_nEndFrame;

		if(m_dbZoomRatio < 100)
			m_dbZoomRatio = 100;

		pMuxDataInfo->dbZoomRatio	= m_dbZoomRatio;

		//wgkim 190113
		CESMTemplateMgr* pTemplateMgr = m_pMainWnd->m_pTemplateMgr;

		CPoint ptCenterPoint = CPoint(0, 0);
		if(pTemplateMgr != NULL && pTemplateMgr->IsArrTemplatePointValid() == TRUE)
		{
			pItem->m_nIndex;
			Point2f ptTempPoint= pTemplateMgr->GetTemplatePoint(pItem->m_nIndex).viewPoint;

			ptCenterPoint.x = ptTempPoint.x;
			ptCenterPoint.y = ptTempPoint.y;

			pMuxDataInfo->ptCenterPoint = ptCenterPoint;
		}

		if(m_bInsertCamID)
			pMuxDataInfo->bInserID		= !pItem->GetDeviceDSCID().IsEmpty();

		pMuxDataInfo->bMovieReverse	= pItem->m_bReverse;

		_stprintf(pMuxDataInfo->strCamID, _T("%s"), pItem->GetDeviceDSCID());

		pMuxDataInfo->bMuxBoth = FALSE;

		pMuxDataInfo->nEncodingMethod = m_nEncodingMethod;
		if(pMuxDataInfo->nEncodingMethod == ENCODE_WINDOW)
		{
			_stprintf(pMuxDataInfo->strSavePath[ENCODE_WINDOW], m_strMUXSavePathProfile);

			CString strFile;
			strFile.Format(_T("%s.mp4"),pItem->GetDeviceDSCID());
			_stprintf(pMuxDataInfo->strFileName[ENCODE_WINDOW],strFile);
		}
		else
		{
			_stprintf(pMuxDataInfo->strSavePath[ENCODE_ANDROID], m_str4DLSavePathProfile);

			CString strFile;
			strFile.Format(_T("h_%d.ts"),i);
			_stprintf(pMuxDataInfo->strFileName[ENCODE_ANDROID],strFile);
		}

		CString strPath = ESMGetMoviePath(pItem->GetDeviceDSCID(), m_nStartFrame);
		int nFind = strPath.ReverseFind(_T('\\'));
		_stprintf(pMuxDataInfo->strPath, _T("%s"), strPath.Left(nFind+1));

		SetFinish(0);

		/*stAdjustInfo* pAdjInfo */
		pMuxDataInfo->pAdjInfo = new stAdjustInfo;
		memcpy(pMuxDataInfo->pAdjInfo,&pMovieMgr->GetAdjustData(pItem->GetDeviceDSCID()),sizeof(stAdjustInfo));

		//170622 hjcho
		if(m_bColorTrans)
		{
			memcpy(pMuxDataInfo->dbArrColorInfo,m_dbArrColorInfo,3*sizeof(double));
			pMuxDataInfo->bColorTrans = TRUE;
		}

		//180824 hjcho
		pMuxDataInfo->bLocal		 = TRUE;
		pMuxDataInfo->bLowBitrate = m_bLowBitrate;

		while(1)
		{
			//메이킹 서버가 메이킹시 Mux 작업 일시 중지
			if (GetMakingInfoInt(0))
			{
				m_LcFTP.SetItemText(i, 4, _T("2"));
				Sleep(10);
			}
			else
				break;
		}

		m_LcFTP.SetItemText(i, 4, _T("1"));

		ESMEvent* pMsg	= new ESMEvent;
		pMsg->pParam	= (LPARAM)pMuxDataInfo;
		pMsg->nParam1	= ESMGetReverseMovie();
		pMsg->message	= WM_ESM_MOVIE_MUX;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);

		CString strInsertData;
		while (1)
		{
			Sleep(10);

			int nfinish = GetFinish();

			if (1 == nfinish)
			{
				strInsertData.Format(_T("complete"));
				m_LcFTP.SetItemText(i, 2, strInsertData);
				m_LcFTP.SetItemText(i, 4, _T("0"));
				FileListUpdata(m_LcFTP.GetItemText(i,1), 2,strInsertData);
				ESMLog(5,_T("Finish Mux Movie [%s]"),pItem->GetDeviceDSCID());
				break;
			}
			else if (0 > nfinish)
			{
				strInsertData.Format(_T("Mux ERR"));
				m_LcFTP.SetItemText(i, 2, strInsertData);
				m_LcFTP.SetItemText(i, 4, _T("0"));
				FileListUpdata(m_LcFTP.GetItemText(i,1), 2,strInsertData);
				ESMLog(5,_T("Err Mux Movie [%s]"),pItem->GetDeviceDSCID());
				break;
			}
		}
	}
}
void CESMUtilMultiplexerDlg::WriteFinishFile(CString strStartTime)
{
	CString strFileName,strEndtTime;
	strFileName.Format(_T("%s\\finish.txt"), m_strFileSave);
	
	CFile file;

	if(file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite))
	{
		char strTime[40];
		memset(strTime, 0, 40);
		strEndtTime = ESMGetCurTime(); 

		int nLength= strStartTime.GetLength();
		WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strStartTime,nLength,strTime,nLength,NULL,NULL);

		file.Write(&strTime,sizeof(char)*40);

		file.SeekToEnd();

		nLength= strEndtTime.GetLength();
		WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strEndtTime,nLength,strTime,nLength,NULL,NULL);

		file.SeekToEnd();
		file.Write(&strTime,sizeof(char)*40);

		if(ESMGetValue(ESM_VALUE_CEREMONYUSE))
		{
			//Mail주소 입력
			nLength = ESMGetEventEmail().GetLength();
			WideCharToMultiByte(CP_ACP,0,(LPCWSTR)ESMGetEventEmail(),nLength,strTime,nLength,NULL,NULL);
			file.SeekToEnd();
			file.Write(&strTime,sizeof(char)*nLength);
		}
		else
		{
			CString strMake;
			CTime time = CTime::GetCurrentTime();
			strMake.Format(_T("\r\nTime: %04d%02d%02d_%02d%02d%02d\r\n"), time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());
			m_strLogTime.Format(_T("%04d%02d%02d_%02d%02d%02d"),time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());
			
			//nLength = strMake.GetLength();
			nLength = WideCharToMultiByte(CP_UTF8,0,(LPCWSTR)strMake,-1,NULL,0,NULL,NULL);

			WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strMake,nLength,strTime,nLength,NULL,NULL);
			file.SeekToEnd();
			file.Write(&strTime,sizeof(char)*nLength-1);
			
			WriteMovieInfo(&file);

			//hjcho 180418 - OutputPath
			if(m_bBothProc || (m_bPSCP && m_nEncodingMethod == ENCODE_ANDROID))
			{
				char strMax[MAX_PATH];
				CString strOutput;// = m_str4DLSavePathProfile;
				strOutput.Format(_T("Path: %s/%s/%s\r\n"),m_strOwner,m_strDay,m_strTime);
				nLength = strOutput.GetLength();
				WideCharToMultiByte(CP_ACP,0,(LPCWSTR)strOutput,nLength,strMax,nLength,NULL,NULL);
				file.Write(&strMax,sizeof(char)*nLength);
			}
			//m_strTempOBJ;
		}
		
		file.Close();
	}
}
BOOL CESMUtilMultiplexerDlg::Create4DLPath(CString strDay,CString strTime)
{
	if(m_bPSCP)
	{
		int nFind = m_str4DLSavePath.Find(_T("http"));
		if(nFind == -1)
		{
			AfxMessageBox(_T("4DL 경로를 확인하세요!"));
			m_bMUXing = FALSE;
			return FALSE;
		}
		if(m_strSUBL.IsEmpty()
			|| m_strSUBM.IsEmpty() || m_strSUBS.IsEmpty())
		{
			CString strBox;
			strBox.Format(_T("대분류: %s\n중분류: %s\n소분류: %s"),m_strSUBL,m_strSUBM,m_strSUBS);
			AfxMessageBox(strBox);
			m_bMUXing = FALSE;
			return FALSE;
		}
		GetDlgItem(IDC_MUX_DB_EDIT_DESCRIPT)->GetWindowText(m_strDES);
		GetDlgItem(IDC_MUX_DB_EDIT_OBJ)->GetWindowText(m_strOBJ);
		if(m_strOwner.IsEmpty() || m_strOBJ.IsEmpty() || m_strDES.IsEmpty())
		{
			CString strBox;
			strBox.Format(_T("소유자: %s\n상황: %s\n설명: %s"),m_strOwner,m_strOBJ,m_strDES);
			AfxMessageBox(strBox);
			m_bMUXing = FALSE;
			return FALSE;
		}
		m_str4DLSaveFile = m_str4DLSavePath + _T("/");

		m_str4DLSavePathProfile = m_str4DLSavePath + _T("/");//기본 폴더 생성
		RunHttp(m_str4DLSavePathProfile,_T(""),_T(""),HTTP_CREATE_DIR);

		m_str4DLSavePathProfile.Append(m_strOwner+_T('/'));//Owner 생성
		RunHttp(m_str4DLSavePathProfile,_T(""),_T(""),HTTP_CREATE_DIR);

		m_str4DLSavePathProfile.Append(strDay+_T('/'));//Date 생성
		RunHttp(m_str4DLSavePathProfile,_T(""),_T(""),HTTP_CREATE_DIR);

		m_str4DLSavePathProfile.Append(strTime+_T('/'));//Time 생성
		RunHttp(m_str4DLSavePathProfile,_T(""),_T(""),HTTP_CREATE_DIR);

		if(CheckFTPList(m_str4DLSavePathProfile))
		{
			m_bMUXing = FALSE;
			return FALSE;
		}
		m_strFileSave.Format(_T("M:\\Movie\\%s\\%s"),strDay,strTime);
		ESMCreateAllDirectories(m_strFileSave);
	}
	else
	{
		int nFind = m_str4DLSavePath.Find(_T("http"));
		if(nFind != -1)
		{
			AfxMessageBox(_T("It does not support http\nPlease check the path!"));
			m_bMUXing = FALSE;
			return FALSE;
		}
		
		m_str4DLSavePathProfile.Format(_T("%s\\%s\\%s"),m_str4DLSavePath,strDay,strTime);
		if(CheckFTPList(m_str4DLSavePathProfile))
		{
			m_bMUXing = FALSE;
			return FALSE;
		}

		ESMCreateAllDirectories(m_str4DLSavePathProfile);
		m_strFileSave = m_str4DLSavePathProfile;
	}

	return TRUE;
}
void CESMUtilMultiplexerDlg::OnBnClickedMuxDbBtnDbconntect()
{
	if(m_nEncodingMethod == ENCODE_ANDROID)
	{
		if(m_dbConnect)
			return;
		char* pstrIP = NULL;
		pstrIP = ESMUtil::CStringToChar(m_strDBIPInfo);
		//ESMLog(5,_T("[MUX] DBConnect IP: %s"),m_strDBIPInfo);

		mysql_init(&mysql);
		if(!mysql_real_connect(&mysql, pstrIP, DB_ID, DB_PASS, DB_NAME ,DB_PORT, 0, 0))
		{
			ESMLog(5, _T("[MUX]Mysql connect Fail"));
			m_dbConnect = FALSE;

			GetDlgItem(IDC_MUX_DB_BTN_DBCONNTECT)->EnableWindow(TRUE);
		}
		else
		{
			ESMLog(5, _T("[MUX]Mysql connect Success [%s]"),m_strDBIPInfo);
			m_dbConnect = TRUE;
			CreateDBInfo();
			CreateDBOwnerInfo();
			GetDlgItem(IDC_MUX_DB_BTN_DBCONNTECT)->EnableWindow(FALSE);
		}
	}
	else if(m_nEncodingMethod == ENCODE_WINDOW)
	{
		CreateDBInfo();
		CreateDBOwnerInfo();
	}
}
void CESMUtilMultiplexerDlg::CreateDBOwnerInfo()
{
	m_cbOwner.ResetContent();

	if(m_nEncodingMethod == ENCODE_ANDROID)
	{
		char* sQuery = new char[200];
		strcpy(sQuery, "select * from OWNER;");
		MYSQL_ROW sql_row;
		MYSQL_RES *sql_result = NULL;

		int nRet = mysql_query(&mysql, sQuery);

		sql_result = mysql_store_result(&mysql);

		if(sql_result == NULL)
		{
			AfxMessageBox(_T("DB Connect Error"));
			return;
		}

		while ( (sql_row = mysql_fetch_row(sql_result)) != NULL )
		{
			//printf("%+11s %-30s %-10s", sql_row[0], sql_row[1], sql_row[2]);
			CString str = ESMUtil::CharToCString(sql_row[0]);
			str.Trim();
			m_cbOwner.AddString(str);
		}
		mysql_free_result(sql_result);

		delete[] sQuery;
	}
	else if(m_nEncodingMethod == ENCODE_WINDOW)
	{
		m_cbOwner.AddString(_T("DOOSAN"));
		m_cbOwner.AddString(_T("HANWHA"));
		m_cbOwner.AddString(_T("KIA"));
		m_cbOwner.AddString(_T("KT"));
		m_cbOwner.AddString(_T("LG"));
		m_cbOwner.AddString(_T("LOTTE"));
		m_cbOwner.AddString(_T("NC"));
		m_cbOwner.AddString(_T("NEXEN"));
		m_cbOwner.AddString(_T("SAMSUNG"));
		m_cbOwner.AddString(_T("SK"));
		m_cbOwner.AddString(_T("4dreplay"));
		m_cbOwner.AddString(_T("super"));
	}
}
void CESMUtilMultiplexerDlg::CreateDBInfo()
{
	//콤보 삭제
	ClearDBInformation();

  	if(m_nEncodingMethod == ENCODE_ANDROID)
	{
		char* sQuery = new char[200];
		strcpy(sQuery, "select name from SUB_L_INFO;");
		MYSQL_ROW sql_row;
		MYSQL_RES *sql_result = NULL;
		CString strTelNum, strEmail;
		int nRet = mysql_query(&mysql, sQuery);

		sql_result = mysql_store_result(&mysql);

		if(sql_result == NULL)
		{
			AfxMessageBox(_T("DB Connect Error"));
			return;
		}

		while ( (sql_row = mysql_fetch_row(sql_result)) != NULL )
		{
			//printf("%+11s %-30s %-10s", sql_row[0], sql_row[1], sql_row[2]);
			CString str = ESMUtil::CharToCString(sql_row[0]);
			str.Trim();
			m_cbSubL.AddString(str);
		}
		mysql_free_result(sql_result);

		delete[] sQuery;
	}
	else if(m_nEncodingMethod == ENCODE_WINDOW)
	{
		m_cbSubL.AddString(_T("Event"));
		m_cbSubL.AddString(_T("Movie"));
		m_cbSubL.AddString(_T("Sport"));
	}
}
void CESMUtilMultiplexerDlg::ClearDBInformation()
{
	m_cbSubL.ResetContent();
	m_cbSubM.ResetContent();
	m_cbSubS.ResetContent();
	m_cbOwner.ResetContent();
	m_cbPlayer.ResetContent();

	m_strSUBL = _T("");
	m_strSUBM = _T("");
	m_strSUBS = _T("");
	m_strOwner = _T("");
	m_strPlayerInfo  = _T("");
	OnCbnEditchangeMuxDbComboPlayer();
}
void CESMUtilMultiplexerDlg::OnBnClickedMuxCheckPscp()
{
	UpdateData(TRUE);

	//if(m_bPSCP)
	//{
	//	GetDlgItem(IDC_MUX_DB_COMBO_SUBL)->EnableWindow(TRUE);
	//	GetDlgItem(IDC_MUX_DB_COMBO_SUBM)->EnableWindow(TRUE);
	//	GetDlgItem(IDC_MUX_DB_COMBO_SUBS)->EnableWindow(TRUE);
	//	GetDlgItem(IDC_MUX_DB_COMBO_OWNER)->EnableWindow(TRUE);
	//	GetDlgItem(IDC_MUX_DB_EDIT_OBJ)->EnableWindow(TRUE);
	//	GetDlgItem(IDC_MUX_DB_EDIT_DESCRIPT)->EnableWindow(TRUE);
	//	//GetDlgItem(IDC_MUX_DB_NAMING)->EnableWindow(TRUE);
	//}
	//else
	//{
	//	GetDlgItem(IDC_MUX_DB_COMBO_SUBL)->EnableWindow(FALSE);
	//	GetDlgItem(IDC_MUX_DB_COMBO_SUBM)->EnableWindow(FALSE);
	//	GetDlgItem(IDC_MUX_DB_COMBO_SUBS)->EnableWindow(FALSE);
	//	GetDlgItem(IDC_MUX_DB_COMBO_OWNER)->EnableWindow(FALSE);
	//	GetDlgItem(IDC_MUX_DB_EDIT_OBJ)->EnableWindow(FALSE);
	//	GetDlgItem(IDC_MUX_DB_EDIT_DESCRIPT)->EnableWindow(FALSE);
	//	//GetDlgItem(IDC_MUX_DB_NAMING)->EnableWindow(FALSE);
	//}
}
void CESMUtilMultiplexerDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	if(bShow == TRUE)
	{
		if(m_bPSCP)
			OnBnClickedMuxDbBtnDbconntect();

		GetDlgItem(IDC_MUX_DB_EDIT_OBJ)->SetWindowText(_T(""));
		GetDlgItem(IDC_MUX_DB_EDIT_DESCRIPT)->SetWindowText(_T(""));
	}
}
void CESMUtilMultiplexerDlg::OnCbnSelchangeMuxDbComboSubl()
{
	CString str;
	int nIndex;
	nIndex = m_cbSubL.GetCurSel();
	m_cbSubL.GetLBText(nIndex,str);

	if(str == m_strSUBL)
		return;

	m_cbSubM.ResetContent();
	m_strSUBL = str;

	if(m_nEncodingMethod == ENCODE_ANDROID)
	{
		CString strTemp;
		strTemp.Format(_T("select name from SUB_M_INFO where sub_l = '%s'"),m_strSUBL);
		char* sQuery = new char[200];
		sQuery = ESMUtil::CStringToChar(strTemp);

		MYSQL_ROW sql_row;
		MYSQL_RES *sql_result = NULL;
		int nRet = mysql_query(&mysql, sQuery);

		sql_result = mysql_store_result(&mysql);

		if(sql_result == NULL)
		{
			AfxMessageBox(_T("DB Connect Error"));
			return;
		}

		while ( (sql_row = mysql_fetch_row(sql_result)) != NULL )
		{
			//printf("%+11s %-30s %-10s", sql_row[0], sql_row[1], sql_row[2]);
			CString str = ESMUtil::CharToCString(sql_row[0]);
			m_cbSubM.AddString(str);
		}
		mysql_free_result(sql_result);

		delete[] sQuery;
	}
	else if(m_nEncodingMethod == ENCODE_WINDOW)
	{
		m_cbSubM.AddString(_T("Baseball"));
		m_cbSubM.AddString(_T("Golf"));
		m_cbSubM.AddString(_T("Basketball"));
	}
}
void CESMUtilMultiplexerDlg::OnCbnSelchangeMuxDbComboSubm()
{
	CString str;
	int nIndex;
	nIndex = m_cbSubM.GetCurSel();
	m_cbSubM.GetLBText(nIndex,str);

	if(str == m_strSUBM)
		return;

	m_cbSubS.ResetContent();
	m_strSUBM = str;

	if(m_nEncodingMethod == ENCODE_ANDROID)
	{
		CString strTemp;
		strTemp.Format(_T("select name from SUB_S_INFO where sub_m = '%s'"),m_strSUBM);
		char* sQuery = new char[200];
		sQuery = ESMUtil::CStringToChar(strTemp);

		MYSQL_ROW sql_row;
		MYSQL_RES *sql_result = NULL;
		int nRet = mysql_query(&mysql, sQuery);

		sql_result = mysql_store_result(&mysql);

		if(sql_result == NULL)
		{
			AfxMessageBox(_T("DB Connect Error"));
			return;
		}

		while ( (sql_row = mysql_fetch_row(sql_result)) != NULL )
		{
			//printf("%+11s %-30s %-10s", sql_row[0], sql_row[1], sql_row[2]);
			CString str = ESMUtil::CharToCString(sql_row[0]);
			str.Trim();
			m_cbSubS.AddString(str);
		}
		mysql_free_result(sql_result);

		delete[] sQuery;
	}
	if(m_nEncodingMethod == ENCODE_WINDOW)
	{
		if(m_strSUBM.CompareNoCase(_T("Baseball")) == 0)
		{
			m_cbSubS.AddString(_T("DOOSAN"));
			m_cbSubS.AddString(_T("HANWHA"));
			m_cbSubS.AddString(_T("KIA"));
			m_cbSubS.AddString(_T("KT"));
			m_cbSubS.AddString(_T("LG"));
			m_cbSubS.AddString(_T("LOTTE"));
			m_cbSubS.AddString(_T("NC"));
			m_cbSubS.AddString(_T("NEXEN"));
			m_cbSubS.AddString(_T("SAMSUNG"));
			m_cbSubS.AddString(_T("SK"));
		}
	}
}
void CESMUtilMultiplexerDlg::OnCbnSelchangeMuxDbComboSubs()
{
	int nIndex;
	nIndex = m_cbSubS.GetCurSel();
	m_cbSubS.GetLBText(nIndex,m_strSUBS);
}
void CESMUtilMultiplexerDlg::OnCbnSelchangeMuxDbComboOwner()
{
#if 1
	CString str;
	int nIndex;
	nIndex = m_cbOwner.GetCurSel();
	m_cbOwner.GetLBText(nIndex,str);

	if(str == m_strOwner)
		return;

	m_cbPlayer.ResetContent();
	m_strOwner = str;

	if(m_nEncodingMethod == ENCODE_ANDROID)
	{
		CString strTemp;
		//strTemp.Format(_T("select name from PLAYER_INFO where owner_name = '%s'"),m_strOwner);
		strTemp.Format(_T("select back_no,name from PLAYER_INFO where owner_name = '%s'"),m_strOwner);
		char* sQuery = new char[200];
		sQuery = ESMUtil::CStringToChar(strTemp);

		MYSQL_ROW sql_row;
		MYSQL_RES *sql_result = NULL;

		int nRet = mysql_query(&mysql, sQuery);

		sql_result = mysql_store_result(&mysql);

		if(sql_result == NULL)
		{
			AfxMessageBox(_T("DB Connect Error"));
			return;
		}

		while ( (sql_row = mysql_fetch_row(sql_result)) != NULL )
		{
			//printf("%+11s %-30s %-10s", sql_row[0], sql_row[1], sql_row[2]);
			CString strBackNum = ESMUtil::CharToCString(sql_row[0]);
			strBackNum.Trim();
			CString strPlayer  = ESMUtil::CharToCString(sql_row[1]);
			strPlayer.Trim();
			CString strPlayerInfo;
			strPlayerInfo.Format(_T("%s.%s"),strBackNum,strPlayer);
			m_cbPlayer.AddString(strPlayerInfo);
		}
		mysql_free_result(sql_result);

		delete[] sQuery;
	}
	else if(m_nEncodingMethod == ENCODE_WINDOW)
	{

	}
#else
	int nIndex;
	nIndex = m_cbOwner.GetCurSel();
	m_cbOwner.GetLBText(nIndex,m_strOwner);
#endif
}
void CESMUtilMultiplexerDlg::InitColorDSCList()
{
	m_cbColorDSC.ResetContent();

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;
	CString str;
	for(int i = 0; i <arDSCList.GetSize();i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		str = pItem->GetDeviceDSCID();
		str.Trim();
		m_cbColorDSC.AddString(str);
	}
}
BOOL CESMUtilMultiplexerDlg::DecodingFrame(CString strPath,int nStartFrame)
{
	Mat frame;
	BOOL bCheckColor = FALSE;

	CT2CA pszCvted(strPath);
	std::string strBasePath(pszCvted);

	cv::VideoCapture vc(strBasePath);
	if(!vc.isOpened())
	{
		vc.release();
		return FALSE;
	}
	else
	{
		int nCnt = 0;
		while(1)
		{
			vc>>frame;
			
			if(frame.empty() || nCnt == nStartFrame)
				break;
			
			nCnt++;
		}
		bCheckColor = TRUE;
	}

	if(!bCheckColor)
	{
		ESMLog(5,_T("Connot Load All Frames"));
		m_dbArrColorInfo[0] = 0;
		m_dbArrColorInfo[1] = 0;
		m_dbArrColorInfo[2] = 0;
		return FALSE;
	}
	
	int nIdx = 0;
	double dB = 0,dG=0,dR=0;
	for(int i = 0 ; i < frame.total();i++)
	{
		dB += frame.data[nIdx++]; //B
		dG += frame.data[nIdx++]; //G
		dR += frame.data[nIdx++]; //R
	}

	m_dbArrColorInfo[0]= dB / frame.total();
	m_dbArrColorInfo[1]= dG / frame.total();
	m_dbArrColorInfo[2]= dR / frame.total();

	return TRUE;
}
void CESMUtilMultiplexerDlg::SetOptControl()
{
	UpdateData(TRUE);
	GetDlgItem(IDC_MUX_CHECK_TCPSEND)->EnableWindow(FALSE);

	if(m_bMakeDP)
	{
		GetDlgItem(IDC_MUX_FILESAVEPATH)->EnableWindow(FALSE);
		
		
		if(m_nEncodingMethod != ENCODE_WINDOW)
		{
			GetDlgItem(IDC_MUX_CHECK_PSCP)->EnableWindow(TRUE);
			GetDlgItem(IDC_MUX_FILESAVEPATH)->EnableWindow(FALSE);
			GetDlgItem(IDC_MUX_MP4SAVEPATH)->EnableWindow(FALSE);
		}
		else
		{
			GetDlgItem(IDC_MUX_CHECK_PSCP)->EnableWindow(FALSE);
			GetDlgItem(IDC_MUX_FILESAVEPATH)->EnableWindow(TRUE);
			GetDlgItem(IDC_MUX_4DLSAVEPATH)->EnableWindow(FALSE);
		}

		if(m_bBothProc)
		{
			GetDlgItem(IDC_MUX_RADIO_ANDROID)->EnableWindow(FALSE);
			GetDlgItem(IDC_MUX_RADIO_WINDOW)->EnableWindow(FALSE);
		}
		else
		{
			GetDlgItem(IDC_MUX_RADIO_ANDROID)->EnableWindow(TRUE);
			GetDlgItem(IDC_MUX_RADIO_WINDOW)->EnableWindow(TRUE);
		}
		OnBnClickedMuxCheckPscp();
	}
	else
	{
		GetDlgItem(IDC_MUX_FILESAVEPATH)->EnableWindow(TRUE);

		GetDlgItem(IDC_MUX_4DLSAVEPATH)->EnableWindow(FALSE);
		GetDlgItem(IDC_MUX_CHECK_PSCP)->EnableWindow(FALSE);

		GetDlgItem(IDC_MUX_BOTHPROC)->EnableWindow(FALSE);

		GetDlgItem(IDC_MUX_RADIO_ANDROID)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_RADIO_WINDOW)->EnableWindow(TRUE);

		OnBnClickedMuxCheckPscp();
	}
}
BOOL CESMUtilMultiplexerDlg::CheckFTPList(CString strPath)
{
	CString strFTPPath;
	int nFind = strPath.Find(_T("http"));
	CString strTempPath;
	if(nFind != -1)//http가 있을 경우
	{
		strFTPPath = strPath+_T("/FTPList.txt");
		CString strTempPath;
		strTempPath.Format(_T("M:\\Movie\\temp.txt"));
		RunHttp(strFTPPath,strTempPath,_T(""),HTTP_GET_FILE);
		strFTPPath = strTempPath;
	}
	else
		strFTPPath = strPath+_T("\\FTPList.txt");
	

	
	CESMFileOperation fo;
	if(fo.IsFileExist(strFTPPath))
	{
		CString strMessage;
		strMessage.Format(_T("File exist: %s\nDo you want continue after deleting?"),strPath);
		if(IDYES == AfxMessageBox(strMessage,MB_YESNO))
		{
			if(nFind != -1)
			{
				RunHttp(strPath,_T(""),_T(""),HTTP_DELETE_DIR);
				RunHttp(strPath,_T(""),_T(""),HTTP_CREATE_DIR);
			}
			else
				fo.Delete(strPath);

			
			return FALSE;
		}
		else
		{
			if(nFind != -1)
				fo.Delete(strTempPath);
			return TRUE;
		}
	}
	else
		return FALSE;
}
void CESMUtilMultiplexerDlg::OnCbnSelchangeMuxDbComboPlayer()
{
	CString str;
	int nIndex;
	nIndex = m_cbPlayer.GetCurSel();
	m_cbPlayer.GetLBText(nIndex,str);

	m_strPlayerInfo = str;
	GetDlgItem(IDC_MUX_DB_EDIT_OBJ)->SetWindowText(m_strPlayerInfo);
	//m_strOBJ = m_strPlayerInfo;

	if(m_strPlayerInfo.CompareNoCase(_T("")) == 0)
		return;

	//2016_07_09_21_21_55
	CString strDate = ESMGetFrameRecord();
	CString strYear   = strDate.Mid(0,4);
	CString strMonth  = strDate.Mid(5,2);
	CString strDay    = strDate.Mid(8,2);
	CString strHour   = strDate.Mid(11,2);
	CString strMin    = strDate.Mid(14,2);

	int nFind = m_strPlayerInfo.Find('.') + 1;
	int nCount = m_strPlayerInfo.GetLength() - nFind;
	m_strPlayerInfo = m_strPlayerInfo.Mid(nFind,nCount);
	CString strResult;
	strResult.Format(_T("%s/%s/%s,%s"),strMonth,strDay,strYear,m_strPlayerInfo);

	//CString strData = ESMGetRec;
	GetDlgItem(IDC_MUX_DB_EDIT_DESCRIPT)->SetWindowTextW(strResult);
	UpdateData(FALSE);
}
CString CESMUtilMultiplexerDlg::CreateSFGName()
{
	CString strTemp = _T("");

	return strTemp;
}
void CESMUtilMultiplexerDlg::OnCbnEditchangeMuxDbComboPlayer()
{
	//m_strOBJ = m_strPlayerInfo;

	CString strTemp;
	GetDlgItem(IDC_MUX_DB_COMBO_PLAYER)->GetWindowText(strTemp);
	GetDlgItem(IDC_MUX_DB_EDIT_OBJ)->SetWindowText(strTemp);
	
	if(strTemp.CompareNoCase(_T("")) == 0)
	{
		GetDlgItem(IDC_MUX_DB_EDIT_DESCRIPT)->SetWindowTextW(_T(""));
		return;
	}

	//2016_07_09_21_21_55
	CString strDate = ESMGetFrameRecord();
	CString strYear   = strDate.Mid(0,4);
	CString strMonth  = strDate.Mid(5,2);
	CString strDay    = strDate.Mid(8,2);
	CString strHour   = strDate.Mid(11,2);
	CString strMin    = strDate.Mid(14,2);

	int nFind = m_strPlayerInfo.Find('.') + 1;
	int nCount = m_strPlayerInfo.GetLength() - nFind;
	m_strPlayerInfo = m_strPlayerInfo.Mid(nFind,nCount);
	CString strResult;
	strResult.Format(_T("%s/%s/%s,%s"),strMonth,strDay,strYear,strTemp);

	//CString strData = ESMGetRec;
	GetDlgItem(IDC_MUX_DB_EDIT_DESCRIPT)->SetWindowTextW(strResult);
	UpdateData(FALSE);
}
HBRUSH CESMUtilMultiplexerDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	hbr = (HBRUSH)m_background;
	switch(nCtlColor)
	{
	case CTLCOLOR_STATIC:
		{

			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			//pDC->SetBkColor(RGB(56, 56, 56));  // 글자 배경색 변경
			pDC->SetBkMode(TRANSPARENT);
			//return (HBRUSH)m_brush;
		}
		break;
	case CTLCOLOR_EDIT:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			pDC->SetBkColor(RGB(0, 0 ,0));
			//hbr = (HBRUSH)(hbr.GetSafeHandle());  
		}
		break;
	}    

	return hbr;
}
BOOL CESMUtilMultiplexerDlg::SetDBInfo()
{
	GetDlgItem(IDC_MUX_DB_COMBO_SUBL)->GetWindowText(m_strSUBL);
	GetDlgItem(IDC_MUX_DB_COMBO_SUBM)->GetWindowText(m_strSUBM);
	GetDlgItem(IDC_MUX_DB_COMBO_SUBS)->GetWindowText(m_strSUBS);
	if((m_strSUBL.IsEmpty() || m_strSUBM.IsEmpty() || m_strSUBS.IsEmpty()) && 
		m_nEncodingMethod != ENCODE_WINDOW)
	{
		CString strBox;
		strBox.Format(_T("no input value L: %s\nM: %s\nS: %s"),m_strSUBL,m_strSUBM,m_strSUBS);
		AfxMessageBox(strBox);
		m_bMUXing = FALSE;
		return FALSE;
	}
	GetDlgItem(IDC_MUX_DB_COMBO_OWNER)->GetWindowText(m_strOwner);
	GetDlgItem(IDC_MUX_DB_EDIT_DESCRIPT)->GetWindowText(m_strDES);
	GetDlgItem(IDC_MUX_DB_EDIT_OBJ)->GetWindowText(m_strOBJ);

	if((m_strOwner.IsEmpty() || m_strOBJ.IsEmpty() || m_strDES.IsEmpty()) &&
		m_nEncodingMethod != ENCODE_WINDOW)
	{
		CString strBox;
		strBox.Format(_T("Owner: %s\nObject: %s\nDescription: %s"),m_strOwner,m_strOBJ,m_strDES);
		AfxMessageBox(strBox);
		m_bMUXing = FALSE;
		return FALSE;
	}
	return TRUE;
}
void CESMUtilMultiplexerDlg::OnBnClickedCheckWindowsRecord()
{
	if(((CButton*)GetDlgItem(IDC_CHECK_WINDOWS_RECORD))->GetCheck())
	{

	}
	else
	{
		TRACE("TEST");
	}
}
void CESMUtilMultiplexerDlg::SetRefereeInfo(int nGroupIdx,int nMovieIndex,int nFrameIdx)
{
	UpdateData(TRUE);
	MuxInfoLoad();

	m_LcFTP.DeleteAllItems();

	int nBaseTime = nMovieIndex*1000 + nFrameIdx * ESMGetFrameRate();

	m_nSelectTime = nBaseTime;//m_pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();
	m_nStartTime = m_nSelectTime - (m_nTotalTime/2);
	if(m_nStartTime < 0)
		m_nStartTime = 0;

	m_nEndTime = m_nSelectTime + (m_nTotalTime/2);

	m_pConnection = NULL;
	m_pFileFind = NULL;

	m_nMuxDataSendCount = 0;
	m_nMuxDataFinishCount = 0;
	m_nMjpgEncodingCount = 0;

	UpdateData(FALSE);

	//ShowWindow(SW_SHOW);
	//if(LaunchRefereeReading(nGroupIdx) > 0)
	//{
		////ERROR
	//}
	//ShowWindow(SW_HIDE);

	m_nGroupIndex = nGroupIdx;

	MuxRefereeData* pRefereeData = new MuxRefereeData;
	pRefereeData->pMgr		= this;
	pRefereeData->nGroupIdx = nGroupIdx;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,LaunchRefereeReading,(void*)pRefereeData,0,NULL);
	CloseHandle(hSyncTime);
}
BOOL CESMUtilMultiplexerDlg::RefereeCreateMakingList(int nGroupIdx)
{
	int nCnt = 0;

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CString strGroupIdx,strInsertData;
	if(nGroupIdx == 0)
		strGroupIdx = _T('A');
	else if(nGroupIdx == 1)
		strGroupIdx = _T('B');
	else if(nGroupIdx == 2)
		strGroupIdx = _T('C');
	else if(nGroupIdx == 3)
		strGroupIdx = _T('D');
	else if(nGroupIdx == 4)
		strGroupIdx = _T('E');
	else
		strGroupIdx = _T("Err");

	if(strGroupIdx == _T("Err"))
	{
		AfxMessageBox(_T("Group Index Error"));
		return FALSE;
	}
	CDSCItem* pItem = NULL;
	FTPListData stData;
	for(int i = 0 ; i < arDSCList.GetSize(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);

		CString strData = pItem->GetGroup();
		if(pItem->GetGroup().Find(strGroupIdx) == 0)
		{
			memset(&stData,NULL,sizeof(FTPListData));

			strInsertData.Format(_T("%d"), nCnt+1);
			m_LcFTP.InsertItem(nCnt, strInsertData);

			strInsertData.Format(_T("%s"), pItem->GetDeviceDSCID());
			m_LcFTP.SetItemText(nCnt, 1, strInsertData);

			if(pItem->GetSensorSync() == FALSE)
				m_LcFTP.SetItemText(i, 2, _T("Sync Out"));

			ESMLog(5,_T("[RefereeMode] %s"),strInsertData);
			nCnt++;
		}
	}

	int nGroupCount = m_LcFTP.GetItemCount();

	/*CString strPrePath;
	CString strCurPath;
	int bFileCheckERR = FALSE;
	int nErrorCnt = 0;
	for(int i = 0 ; i < nGroupCount ; i ++)
	{
		CString strSourceDSC = m_LcFTP.GetItemText(i,1);

		for(int j = m_nStartFrame ; j <= m_nEndFrame ; j++)
		{
			strCurPath.Format(_T("%s"), ESMGetMoviePath(strSourceDSC, j));

			if(strCurPath.CompareNoCase(strPrePath) == 0 && i != 0)
				continue;
			else
			{
				strPrePath = strCurPath;
				if(!CheckFile(strCurPath))
				{
					strInsertData.Format(_T("File ERR"));
					m_LcFTP.SetItemText(i, 2, strInsertData);
					FileListUpdata(m_LcFTP.GetItemText(i,1), 2,strInsertData);
					ESMLog(5,_T("File ERR Mux Movie [%s]"),pItem->GetDeviceDSCID());
					bFileCheckERR = TRUE;
					break;
				}
			}
		}
	}*/

	return TRUE;
}
BOOL CESMUtilMultiplexerDlg::RefereeCheckConnect4DP()
{
	int nStartNetCheck = GetTickCount();
#if 1
	int nNetCount = m_pMainWnd->m_wndNetwork.GetRCMgrCount();
	for (int i = 0 ; i < nNetCount ; i++)
	{
		CESMRCManager *pRCMgr = m_pMainWnd->m_wndNetwork.GetRCMgr(i);

		if (pRCMgr->m_bConnected)
		{
			if(!pRCMgr->GetSourceDSC().GetLength())
			{
				pRCMgr->ConnectCheck(ESMGetIdentify());
				m_arRCServerList.Add((CObject*)pRCMgr);
				ESMLog(5,_T("[Connect] %s"),pRCMgr->GetSourceIP());
			}
		}
	}
#else
	int nNetCount = m_pMainWnd->m_wndNetworkEx.GetRCMgrCount();
	for (int i = 0 ; i < nNetCount ; i++)
	{
		CESMRCManager *pRCMgr = m_pMainWnd->m_wndNetworkEx.GetRCMgr(i);

		if (pRCMgr->m_bConnected && pRCMgr->m_bGPUUse)
		{
			if(!pRCMgr->GetSourceDSC().GetLength())
			{
				pRCMgr->ConnectCheck(ESMGetIdentify());
				m_arRCServerList.Add((CObject*)pRCMgr);
				ESMLog(5,_T("[Connect] %s"),pRCMgr->GetSourceIP());
			}
		}
	}
#endif

	int nEndNetCheck = GetTickCount();
	CString strNetCheck;
	strNetCheck.Format(_T("NetCheck : %d"), nEndNetCheck - nStartNetCheck);
	ESMLog(5, strNetCheck);


	if (m_arRCServerList.GetCount() < 1)
		return 0;
	else
		return m_arRCServerList.GetCount();
}
BOOL CESMUtilMultiplexerDlg::RefereeSendDataTo4DP(int nStartMov,int nEndMov,CString strSavePath)
{
	int nGroupCount = m_LcFTP.GetItemCount();

	for(int i = 0 ; i < nGroupCount; i++)
	{
		CString strCheck = m_LcFTP.GetItemText(i,2);
		CString strDSCID = m_LcFTP.GetItemText(i,1);
		 
		MuxDataInfo MuxInfo;
		MuxInfo.nOutWidth  = 1280;
		MuxInfo.nOutHeight = 720;

		MuxInfo.nStartFrame	= m_nStartFrame;
		MuxInfo.nEndFrame	= m_nEndFrame;

		if(m_dbZoomRatio < 100)
			m_dbZoomRatio = 100;

		MuxInfo.dbZoomRatio = m_dbZoomRatio;

		_stprintf(MuxInfo.strCamID,strDSCID);

		MuxInfo.bPSCP		= FALSE;
		MuxInfo.bMuxBoth	= m_bBothProc;
		MuxInfo.nEncodingMethod = ENCODE_WINDOW;
		MuxInfo.bRefereeMode = TRUE;
		_stprintf(MuxInfo.strHomePath,_T("%s\\bin\\"),ESMGetPath(ESM_PATH_HOME));
		
		CString strFile,strSavePath;
		strSavePath.Format(_T("\\\\%s\\Movie"),ESMGetLocalIP());
		_stprintf(MuxInfo.strSavePath[ENCODE_WINDOW],strSavePath);
		strFile.Format(_T("%s.mp4"),strDSCID);
		_stprintf(MuxInfo.strFileName[ENCODE_WINDOW],strFile);

#if 1//Local Processing
		//M:\\Movie\\4dmname\\;
		CString strPath = ESMGetMoviePath(strDSCID, m_nStartFrame);
		int nFind = strPath.Find(_T('M'));
		
		CString strIP;
		strIP = strPath.Left(nFind - 1);
		strPath.Replace(strIP,_T("M:"));

		nFind = strPath.ReverseFind(_T('\\'));
		_stprintf(MuxInfo.strPath, _T("%s"), strPath.Left(nFind+1));

		CESMRCManager *pRCMgr = NULL;
		int nFindIndex = -1;
		for (int i = 0 ; i < m_arRCServerList.GetCount() ; i++)
		{
			CESMRCManager *pRCMgr = (CESMRCManager*)m_arRCServerList.GetAt(i);

			CString strRealIP = pRCMgr->GetIP();
			if(strIP.Find(pRCMgr->GetIP()) != -1)
			{
				nFindIndex = i;
				break;
			}
		}

		pRCMgr =  (CESMRCManager*)m_arRCServerList.GetAt(nFindIndex);
		pRCMgr->m_bMuxTCP = FALSE;

		pRCMgr->m_bMUXRunning = TRUE;
		pRCMgr->SendMakeMuxData(&MuxInfo);
		m_nMuxDataSendCount++;

		/*BOOL bFlag = TRUE;
		while (bFlag)
		{
		for (int i = 0 ; i < m_arRCServerList.GetCount() ; i++)
		{
		CESMRCManager *pRCMgr =  (CESMRCManager*)m_arRCServerList.GetAt(i);

		if(pRCMgr->m_bMUXRunning)
		continue;
		else
		{
		pRCMgr->m_bMuxTCP = FALSE;

		pRCMgr->m_bMUXRunning = TRUE;
		pRCMgr->SendMakeMuxData(&MuxInfo);
		m_nMuxDataSendCount++;
		bFlag = FALSE;
		break;
		}
		}
		Sleep(10);
		}*/
#else
		CString strPath = ESMGetMoviePath(strDSCID, m_nStartFrame);
		int nFind = strPath.ReverseFind(_T('\\'));
		_stprintf(MuxInfo.strPath, _T("%s"), strPath.Left(nFind+1));

		BOOL bFlag = TRUE;
		while (bFlag)
		{
			for (int i = 0 ; i < m_arRCServerList.GetCount() ; i++)
			{
				CESMRCManager *pRCMgr =  (CESMRCManager*)m_arRCServerList.GetAt(i);
				CString strIP = pRCMgr->GetIP();

				if(pRCMgr->m_bMUXRunning)
					continue;
				else
				{
					pRCMgr->m_bMuxTCP = FALSE;

					pRCMgr->m_bMUXRunning = TRUE;
					pRCMgr->SendMakeMuxData(&MuxInfo);
					m_nMuxDataSendCount++;
					bFlag = FALSE;
					break;
				}
			}
			Sleep(10);
		}
#endif
	}
	while(1)
	{
		if(m_nMuxDataSendCount <= m_nMuxDataFinishCount)
		{
			//AfxMessageBox(_T("Mux Finish!"));
			break;
		}

		Sleep(10);
	}

	//Merge Movie
	CString strTemp,strOpt,strCmd;
	CString* pstrOutputFile = new CString;
	pstrOutputFile->Format(_T("M:\\Movie\\[%d]%s.mp4"),m_nGroupIndex,ESMGetFrameRecord());
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));
	int nCnt = 0;

	for(int i = 0 ; i < MAX_REFEREE_CNT ; i++)
	{
		CString strDSCID = m_LcFTP.GetItemText(i,1);
		CString strCheck = m_LcFTP.GetItemText(i,2);

		if(nGroupCount < i)
		{
			//Black 
			strTemp.Format(_T("-i \"%s\\720.mp4\" "),ESMGetPath(ESM_PATH_IMAGE));
			//strTemp.Format(_T("-i M:\\Movie\\720.mp4 "));
			strOpt.Append(strTemp);
		}
		else
		{
			if(strCheck == _T("complete"))
			{
				strTemp.Format(_T("-i M:\\Movie\\%s.mp4 "),strDSCID);
				nCnt++;

				strOpt.Append(strTemp);
			}
			else
			{
				//Black 
				strTemp.Format(_T("-i \"%s\\720.mp4\" "),ESMGetPath(ESM_PATH_IMAGE));
				//strTemp.Format(_T("-i M:\\Movie\\720.mp4 "));
				strOpt.Append(strTemp);
			}
		}
	}
	ESMLog(5,strOpt);
	strOpt.Append(_T("-y -vcodec mpeg4 -qscale 1 -qmin 2 -intra -an -c copy "));
	for(int i = 0 ; i < MAX_REFEREE_CNT ; i++)
	{
		strTemp.Format(_T("-map %d "),i);
		strOpt.Append(strTemp);
	}
	//strTemp.Format(_T("[%d]%s.mp4"),m_nGroupIndex,ESMGetFrameRecord());
	strOpt.Append(*pstrOutputFile);

	int nStart = GetTickCount();
	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;
	//lpExecInfo.nShow	=SW_SHOW;
	lpExecInfo.nShow = SW_SHOW; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	lpExecInfo.lpFile = _T("exit");
	lpExecInfo.lpVerb = L"close";
	lpExecInfo.lpParameters = NULL;

	if(TerminateProcess(lpExecInfo.hProcess,0))
	{
		lpExecInfo.hProcess = 0;
		ESMLog(5,_T("End"));
	}

	for(int i = 0 ; i < nGroupCount ; i++)
	{
		CString strDSCID = m_LcFTP.GetItemText(i,1);
		CString strCheck = m_LcFTP.GetItemText(i,2);

		if(strCheck == _T("complete"))
		{
			strTemp.Format(_T("M:\\Movie\\%s.mp4 "),strDSCID);
			
			CESMFileOperation fo;
			if(fo.IsFileExist(strTemp))
				fo.Delete(strTemp);
		}
	}
	
#if 0
	if(pstrOutputFile)
	{
		delete pstrOutputFile;
		pstrOutputFile = NULL;
	}
#else
	ESMEvent *pMsg = new ESMEvent;
	pMsg->message = WM_ESM_FRAME_REFEREE_MUX_FINISH;
	pMsg->pParam  = (LPARAM) pstrOutputFile;
	pMsg->nParam1 = m_nFirstMovieIdx;
	pMsg->nParam2 = m_nFirstFrameIdx;
	::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);
#endif
	return TRUE;
}
int CESMUtilMultiplexerDlg::LaunchRefereeReading(int nGroupIdx)
{
	int nStartIdx = 0,nEndIdx = 0;
	int nStartMov = 0,nEndMov = 0;

	m_nStartFrame = ESMGetFrameIndex(m_nStartTime);
	m_nEndFrame = ESMGetFrameIndex(m_nEndTime) - 1;

	ESMGetMovieIndex(m_nStartFrame,nStartMov,nStartIdx);
	ESMGetMovieIndex(m_nEndFrame  ,nEndMov	,nEndIdx);

	m_nFirstMovieIdx = nStartMov;
	m_nFirstFrameIdx = nStartIdx;
	ESMLog(5,_T("[REFEREE]Create making List start"));
	if(RefereeCreateMakingList(nGroupIdx) == FALSE)
		return REFEREE_ERROR_MAKINGLIST;

	ESMLog(5,_T("[REFEREE]Connection check start"));
	if(RefereeCheckConnect4DP() == FALSE)
		return REFEREE_ERROR_4DPCONNECT;

	ESMLog(5,_T("[REFEREE]Mux Start"));
	if(RefereeSendDataTo4DP(nStartMov,nEndMov,_T("M:\\Movie")) == FALSE)
		return FALSE;

	ESMLog(5,_T("[REFEREE]Mux Finish!"));

	return REFEREE_ERROR_NOERROR;
}
unsigned WINAPI CESMUtilMultiplexerDlg::LaunchRefereeReading(LPVOID param)
{
	MuxRefereeData* pReferee = (MuxRefereeData*)param;
	int nGroupIdx = pReferee->nGroupIdx;
	CESMUtilMultiplexerDlg* pESMUtilMultiplexerDlg = pReferee->pMgr;
	delete pReferee;pReferee = NULL;


	pESMUtilMultiplexerDlg->LaunchRefereeReading(nGroupIdx);
	//pESMUtilMultiplexerDlg->ShowWindow(SW_HIDE);

	return TRUE;
}
void CESMUtilMultiplexerDlg::ReceiveMuxData(char*pData,CString strSavePath,int nBodySize)
{
	MUX_RECEIVE_DATA* pReceive = new MUX_RECEIVE_DATA;
	pReceive->pData = pData;
	pReceive->strSavePath = strSavePath;
	pReceive->nBodySize = nBodySize;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,LaunchReceiveData,(void*)pReceive,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI CESMUtilMultiplexerDlg::LaunchReceiveData(LPVOID param)
{
	MUX_RECEIVE_DATA* pReceive = (MUX_RECEIVE_DATA*)param;
	char* pData = pReceive->pData;
	CString strPath = pReceive->strSavePath;
	int nBodySize = pReceive->nBodySize;
	delete pReceive; pReceive = NULL;

	CFile file;
	if(file.Open(strPath, CFile::modeCreate | CFile::modeReadWrite))
	{
		file.Write((void*)(pData + sizeof(RCP_FileSave_Info)), nBodySize - sizeof(RCP_FileSave_Info));
		file.Close();
	}

	if(pData)
	{
		delete pData;
		pData = NULL;
	}
	return 0;
}

void CESMUtilMultiplexerDlg::OnCbnSelchangeMuxSelResolution()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strResolution;
	m_combo.GetLBText(m_combo.GetCurSel(),strResolution);


	/*m_combo.AddString(_T("FULL-HD"));
	m_combo.AddString(_T("Ultra-HD"));
	m_combo.AddString(_T("CUSTOM"));*/
	if(strResolution == _T("FULL-HD"))
	{
		m_nOutHeight = 1080;
		m_nOutWidth = 1920;

		CString str;
		str.Format(_T("%d"),m_nOutWidth);
		GetDlgItem(IDC_MUX_OUTWIDTH)->SetWindowText(str);
		str.Format(_T("%d"),m_nOutHeight);
		GetDlgItem(IDC_MUX_OUTHEIGTH)->SetWindowText(str);

		GetDlgItem(IDC_MUX_OUTWIDTH)->EnableWindow(FALSE);
		GetDlgItem(IDC_MUX_OUTHEIGTH)->EnableWindow(FALSE);

		GetDlgItem(IDC_MUX_SET_RESOLUTION)->EnableWindow(FALSE);
	}
	else if(strResolution == _T("Ultra-HD"))
	{
		m_nOutHeight = 2160;
		m_nOutWidth = 3840;

		CString str;
		str.Format(_T("%d"),m_nOutWidth);
		GetDlgItem(IDC_MUX_OUTWIDTH)->SetWindowText(str);
		str.Format(_T("%d"),m_nOutHeight);
		GetDlgItem(IDC_MUX_OUTHEIGTH)->SetWindowText(str);

		GetDlgItem(IDC_MUX_OUTWIDTH)->EnableWindow(FALSE);
		GetDlgItem(IDC_MUX_OUTHEIGTH)->EnableWindow(FALSE);

		GetDlgItem(IDC_MUX_SET_RESOLUTION)->EnableWindow(FALSE);
	}
	else
	{
		CString str;
		GetDlgItem(IDC_MUX_OUTHEIGTH)->GetWindowText(str);
		int nHeight = _ttoi(str);
		m_nOutHeight = nHeight;
		GetDlgItem(IDC_MUX_OUTWIDTH)->GetWindowText(str);
		int nWidth = _ttoi(str);
		m_nOutWidth = nWidth;

		GetDlgItem(IDC_MUX_OUTWIDTH)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_OUTHEIGTH)->EnableWindow(TRUE);

		GetDlgItem(IDC_MUX_SET_RESOLUTION)->EnableWindow(TRUE);

		AfxMessageBox(_T("Set below Resolution(width x height) and clicked SET button.."));
	}

	OnBnClickedMuxSave();
}

//wgkim 190212
void CESMUtilMultiplexerDlg::OnBnClickedRadioButtonForContainer(UINT msg)
{
	UpdateData(TRUE);

	//ESMLog(5, _T("Container Number : %d"), m_nRadioButtonForContainer);

	UpdateData(FALSE);
}

void CESMUtilMultiplexerDlg::OnEnChangeEditBitrateValue()
{
	UpdateData(TRUE);
	m_nBitrateInMbps;
	UpdateData(FALSE);
}

BOOL CESMUtilMultiplexerDlg::PreTranslateMessage( MSG* pMsg )
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if(GetDlgItem(IDC_MUX_DB_EDIT_DESCRIPT) == GetFocus())
		{
			if(pMsg->wParam == VK_RETURN)
			{
				CEdit* edit = (CEdit*)GetDlgItem(IDC_MUX_DB_EDIT_DESCRIPT);
				int nLen = edit->GetWindowTextLength();
				edit->SetSel(nLen,nLen);
				edit->ReplaceSel(_T("\r\n"));
			}
		}
		else if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}

unsigned WINAPI CESMUtilMultiplexerDlg::_LaunchRTSPVODThread(LPVOID param)
{
	CESMUtilMultiplexerDlg* pMultiplexer = (CESMUtilMultiplexerDlg*)param;
	CString strDescript = pMultiplexer->m_strDES;

	CFile file;
	FTPListData stData;
	CString str4DLPath;
	CString strFileName = pMultiplexer->m_strMUXSavePathProfile;
	strFileName.Append(_T("\\FTPList.txt"));

	if(!file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite))
	{
		CString strLog;
		strLog.Format(_T("can not access FTPList : %s"),strFileName);
		AfxMessageBox(strLog);
		pMultiplexer->m_bMUXing = FALSE;
		return 0;
	}else
	{
		int nLength = 0;
		pMultiplexer->WriteFTPList(&file);
	}
	file.Close();

	//Check File List & Merge Movie
	CString strUploadFile = pMultiplexer->CheckRTSPVODListAndMerge();

	//Descript upload
	CString strDescriptMaking;
	strDescriptMaking.Format(_T("%s\\%s.txt"),pMultiplexer->m_strMUXSavePathProfile,ESMGetFrameRecord());
	BOOL bLoad = FALSE;

	if(file.Open(strDescriptMaking,CFile::modeCreate|CFile::modeReadWrite))
	{
		CArchive ar(&file,CArchive::store);
		WORD mode = 0xFEFF;
		ar.Write(&mode,sizeof(DWORD));

		ar.WriteString(strDescript);
		ar.Close();

		file.Close();
		bLoad = TRUE;
	}
	else
	{
		for(int i = 0 ; i < 10 ; i++)
			ESMLog(5,_T("[Multiplexer] Description Read ERROR!!!!"));
	}

	//FTP Upload
#if 0
	CString strFTPPort;
	strFTPPort.Format(_T("%d"),pMultiplexer->m_nFtpPort);
	CEMSFtp ftp;
	
	if(ftp.Connect(pMultiplexer->m_strFtpIP,
		pMultiplexer->m_strFtpID,
		pMultiplexer->m_strFtpPW,
		strFTPPort))
	{
		//Upload Description File...
		if(bLoad)
		{
			ftp.FileUpload(strDescriptMaking,strDescriptMaking);
		}
		//UpLoad VOD File...
		if(strUploadFile.IsEmpty() == FALSE)
		{
			ftp.FileUpload(strUploadFile,strUploadFile);
		}
		ftp.Disconnect();
	}
#endif

	AfxMessageBox(_T("RTSP VOD Making Finish!!!"));
	pMultiplexer->m_bMUXing = FALSE;
	return FALSE;
}
CString CESMUtilMultiplexerDlg::CheckRTSPVODListAndMerge()
{
	int nListSize = m_LcFTP.GetItemCount();
	int nFrameRate = movie_frame_per_second;//ESMGetFrameRate();

	int nStartTime = m_nStartTime / 1000;
	int nEndTime = m_nEndTime / 1000;

	CESMFileOperation fo;
	
	vector<CString>strArrMergePath;
	CString strTemp,strMergeOpt,strInsertData;

	//for(int i = 0 ; i < nListSize ; i++)
	for(int i = nListSize - 1; i >= 0 ; i--)
	{
		CString strDSCID = m_LcFTP.GetItemText(i,1);
		
		CString strExcept = m_LcFTP.GetItemText(i,2);
		
		if(strExcept.GetLength())
		{
			strInsertData.Format(_T("Except"));
			continue;
		}

		BOOL bFind = TRUE;
		
		CString strOpt;
		strOpt.Format(_T("-i \"concat:"));

		for(int j = nStartTime ; j <= nEndTime ; j++)
		{
			int nFrameIdx = j * nFrameRate;

			CString strPath = ESMGetMoviePath(strDSCID,nFrameIdx);

			if(fo.IsFileExist(strPath) == FALSE)
			{
				/*int nCnt = 0;
				BOOL bFind = TRUE;
				while(1)
				{
					Sleep(10);

					if(fo.IsFileExist(strPath))
						break;
					if(nCnt >= 10)
					{
						if(fo.IsFileExist(strPath))
							break;
						else
						{
							bFind = FALSE;
							break;
						}
					}
					nCnt++;
				}*/
				m_LcFTP.SetItemText(i,2,_T("except"));
				bFind = FALSE;
				break;
			}
			else
			{
				if(j != nStartTime)
					strOpt.Append(_T("|"));

				strTemp.Format(_T("%s"),strPath);
				strOpt.Append(strTemp);
			}
		}
		if(bFind)
		{
			m_LcFTP.SetItemText(i,2,_T("complete"));
			strOpt.Append(_T("\" -c copy -bsf:a aac_adtstoasc "));
			strTemp.Format(_T("%s\\%s.ts"),m_strMUXSavePathProfile,strDSCID);
			strOpt.Append(strTemp);

			ESMLaunchFFMPEGConsole(strOpt);

			strArrMergePath.push_back(strTemp);

			strMergeOpt.Append(_T("-i \""));
			strMergeOpt.Append(strTemp);
			strMergeOpt.Append(_T("\" "));
		}
	}

	strTemp.Format(_T("-vcodec h264 -intra -c copy "));
	strMergeOpt.Append(strTemp);

	for(int i = 0 ; i < strArrMergePath.size() ; i++)
	{
		strTemp.Format(_T("-map %d "),i);
		strMergeOpt.Append(strTemp);
	}

	if(m_nRadioButtonForContainer == 0)
		strTemp.Format(_T("%s\\%s.mp4"),m_strMUXSavePathProfile,ESMGetFrameRecord());
	else
		strTemp.Format(_T("%s\\%s.ts"),m_strMUXSavePathProfile,ESMGetFrameRecord());

	strMergeOpt.Append(strTemp);
	
	ESMLaunchFFMPEGConsole(strMergeOpt);

	return strTemp;
}

void CESMUtilMultiplexerDlg::ShowOrHideWindows(BOOL bState)
{
	//RTSP Option On
	GetDlgItem(IDC_STATIC_MUX_SERVER_IP)->EnableWindow(!bState);
	GetDlgItem(IDC_STATIC_MUX_ID)->EnableWindow(!bState);
	GetDlgItem(IDC_STATIC_MUX_PW)->EnableWindow(!bState);
	GetDlgItem(IDC_STATIC_MUX_PORT)->EnableWindow(!bState);
	GetDlgItem(IDC_MUX_FTP_IP)->EnableWindow(!bState);
	GetDlgItem(IDC_MUX_FTP_ID)->EnableWindow(!bState);
	GetDlgItem(IDC_MUX_FTP_PW)->EnableWindow(!bState);
	GetDlgItem(IDC_MUX_FTP_PORT)->EnableWindow(!bState);
	GetDlgItem(IDC_STATIC_MUX_FTP)->EnableWindow(!bState);

	//Hide Windows
	GetDlgItem(IDC_MUX_CHECK_COLORTRANS)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_COLOR_SELDSC)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_BTN_SETCOLOR)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_MAKEDP)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_HALF)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_CHECK_TCPSEND)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_CHECK_PSCP)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_BOTHPROC)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_RADIO_ANDROID)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_RADIO_WINDOW)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_LOWBITRATE)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_DB_NAMING)->EnableWindow(bState);

	//EnableWindows
	GetDlgItem(IDC_MUX_DB_COMBO_SUBL)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_DB_COMBO_SUBM)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_DB_COMBO_SUBS)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_DB_COMBO_OWNER)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_DB_COMBO_PLAYER)->EnableWindow(bState);
	//GetDlgItem(IDC_MUX_DB_EDIT_DESCRIPT)->EnableWindow(bState);
	GetDlgItem(IDC_CHECK_WINDOWS_RECORD)->EnableWindow(bState);
	GetDlgItem(IDC_EDIT_BITRATE_VALUE)->EnableWindow(bState);
	GetDlgItem(IDC_RADIO_CONTAINER_MP4)->EnableWindow(!bState);
	GetDlgItem(IDC_RADIO_CONTAINER_TS)->EnableWindow(!bState);
	GetDlgItem(IDC_MUX_MAKEING_WAIT)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_4DLSAVEPATH)->EnableWindow(bState);
	GetDlgItem(IDC_MUX_MAKEINGPATH)->EnableWindow(bState);

	if(bState == FALSE)//RTSP On
	{
		GetDlgItem(IDC_MUX_MP4SAVEPATH)->EnableWindow(TRUE);
		GetDlgItem(IDC_MUX_FILESAVEPATH)->EnableWindow(TRUE);
	}
}