#include "stdafx.h"
#include "ESMBackup.h"
#include "ESMFunc.h"
#include "ESMIni.h"
#include <memory>

typedef struct stFTPPARAMS {
	stFTPPARAMS(){
		nCount = 0;
	}
	std::shared_ptr<CEMSFtp> pFtp;
	CString strLocalFile;
	CString strFoldername;
	CString strFilename;
	int nCount;
} FTPPARAMS;

CESMBackup::CESMBackup()
{	
	m_dFolderSize = 0.f;
	m_nFileCount = 0;

	m_hHandle = NULL;
	m_dTerminateCheck = FALSE;

	m_pBackupFtpProcessState = BACKUP_FTP_PROCESS_STATE_READY;

	m_bUseFolderList = FALSE;
	m_bUseDeleteAfterTransfer = FALSE;
}

CESMBackup::~CESMBackup(void)
{
	CloseFtp();
}

int CESMBackup::GetFileCount()
{
	m_nFileCount = 0;
	return GetFileCount(m_strLocalRootPath);
}

int CESMBackup::GetFileCount(CString strPath)
{
	BOOL bExist;
	CFileFind cFileFind;

	CString strAllInPath = strPath + _T("\\*.*");
	bExist = cFileFind.FindFile(strAllInPath);

	while (bExist)
	{
		bExist = cFileFind.FindNextFileW();		

		if (cFileFind.IsDots())
			continue;		
		else
		{				
			if (cFileFind.IsDirectory())
			{
				CString strFileName = cFileFind.GetFileName();

				if (m_bUseFolderList)
				{
					if (CheckMatchFolder(strFileName) == FALSE)
					{
						continue;
					}
				}

				CString strNextDir = _T("");
				strNextDir = strPath + _T("\\") + cFileFind.GetFileName();

				GetFileCount(strNextDir);
			}
			else
				m_nFileCount++;
		}		
	}
	cFileFind.Close();

	return m_nFileCount;
}

void CESMBackup::FtpInit()
{
	m_arrStrLocalFilePath.clear();	
	m_arrStrFtpFilePath.clear();
	m_arrStrFileName.clear();
	m_arrBFileUploadCheck.clear();

	m_dFolderSize = 0.;	

	m_bUseFolderList = FALSE;
	m_bUseDeleteAfterTransfer = FALSE;
}

void CESMBackup::SetBackupFtpState(BACKUP_FTP_PROCESS_STATE backupFtpState)
{
	m_pBackupFtpProcessState = backupFtpState;
}

void CESMBackup::SetStrLocalRootPath(CString strLocalRootPath)
{
	m_strLocalRootPath = strLocalRootPath;
}

void CESMBackup::SetStrFtpRootPath(CString strFtpRootPath)
{
	m_strFtpRootPath = strFtpRootPath;
}

CESMBackup::BACKUP_FTP_PROCESS_STATE CESMBackup::GetBackupFtpState()
{
	return m_pBackupFtpProcessState;
}

void CESMBackup::BackupByFtp()
{	
	if( GetBackupFtpState() == BACKUP_FTP_PROCESS_STATE_READY || 
		GetBackupFtpState() == BACKUP_FTP_PROCESS_STATE_END )
	{
		if( CheckUploadStateAllFile() == BACKUP_FTP_FILE_STATE_NOTDONE ||
			CheckUploadStateAllFile() == BACKUP_FTP_FILE_STATE_INCOMPLETE )
		{			
			m_hHandle = (HANDLE)_beginthreadex(NULL, 0, BackupByFtpRe, (void*)this, 0, NULL);	
		}
		else if(CheckUploadStateAllFile() == BACKUP_FTP_FILE_STATE_READY ||
			CheckUploadStateAllFile() == BACKUP_FTP_FILE_STATE_OK)
		{			
			m_hHandle = (HANDLE)_beginthreadex(NULL, 0, BackupByFtp, (void*)this, 0, NULL);	
		}		
	}
	else if( GetBackupFtpState() == BACKUP_FTP_PROCESS_STATE_PROCESSING)
	{
		ESMLog(5, _T("Already FTP is Exist"));
		return;	
	}
}

unsigned WINAPI CESMBackup::BackupByFtp( LPVOID param )
{
	CESMBackup* pBackup = (CESMBackup*)param;

	pBackup->SetBackupFtpState(BACKUP_FTP_PROCESS_STATE_PROCESSING);
	pBackup->FtpInit();
	
	std::shared_ptr<CEMSFtp> pFtp(new CEMSFtp());
	pFtp->Connect(
		pBackup->m_strAddress,
		pBackup->m_strID,
		pBackup->m_strPWD, 
		pBackup->m_strPort);
	
	pBackup->GetAllFilePathAndFileName(pBackup->m_strLocalRootPath, pFtp);	

	for(int i =0 ;i < pBackup->m_arrStrLocalFilePath.size(); ++ i)
	{
		CString strPreDeleteDir;
		CString strCurDeleteDir;
		int nPreDeleteDirNum = i;
		if (nPreDeleteDirNum < 1)
		{
			nPreDeleteDirNum = 0;
		}
		else
		{
			nPreDeleteDirNum = nPreDeleteDirNum-1;
		}

		strPreDeleteDir = pBackup->m_arrStrLocalFilePath.at(nPreDeleteDirNum);		// 이전폴더
		strCurDeleteDir = pBackup->m_arrStrLocalFilePath.at(i);						// 현재폴더

		CString strLocalFile = 
			pBackup->m_arrStrLocalFilePath.at(i) + _T("\\") + 
			pBackup->m_arrStrFileName.at(i);
		CString strFilename = pBackup->m_arrStrFileName.at(i);

		//todo : change dir			
		CString strCurrentDir = 
			pBackup->m_strFtpRootPath + _T("/") +
			pBackup->m_arrStrFtpFilePath.at(i) + _T("/") + 
			strFilename;
		
		pBackup->m_arrBFileUploadCheck.push_back(
			pFtp->FileUpload(strLocalFile, strCurrentDir));

		CString strRet;
		strRet.Format(_T("%d - "), pBackup->m_arrBFileUploadCheck.at(i));
		strRet = strRet + strLocalFile;
		ESMLog(5, strRet);

		if (pBackup->m_arrBFileUploadCheck.at(i) == 1)
		{
			if (strPreDeleteDir != strCurDeleteDir)
			{
				pBackup->DeleteAfterTransfer(strPreDeleteDir);				
			}
			else
			{
				if (i == pBackup->m_arrStrLocalFilePath.size()-1)
				{
					pBackup->DeleteAfterTransfer(strCurDeleteDir);
				}
			}
		}
	}
	pFtp->Disconnect();
	pBackup->SetBackupFtpState(BACKUP_FTP_PROCESS_STATE_END);

	pBackup->FtpInit();

// 	if (pBackup->m_bUseDeleteAfterTransfer)
// 		pBackup->DeleteAfterTransfer(pBackup->m_strLocalRootPath);


	return 0;
}

unsigned WINAPI CESMBackup::BackupByFtpRe( LPVOID param )
{
	CESMBackup* pBackup = (CESMBackup*)param;	

	pBackup->SetBackupFtpState(BACKUP_FTP_PROCESS_STATE_PROCESSING);		

	std::shared_ptr<CEMSFtp> pFtp(new CEMSFtp());
	pFtp->Connect(
		pBackup->m_strAddress,
		pBackup->m_strID,
		pBackup->m_strPWD, 
		pBackup->m_strPort);	

	for(int i = pBackup->m_arrBFileUploadCheck.size(); i < pBackup->m_arrStrLocalFilePath.size(); ++ i)
	{			
		CString strPreDeleteDir;
		CString strCurDeleteDir;
		int nPreDeleteDirNum = i;
		if (nPreDeleteDirNum < 1)
		{
			nPreDeleteDirNum = 0;
		}
		else
		{
			nPreDeleteDirNum = nPreDeleteDirNum-1;
		}

		strPreDeleteDir = pBackup->m_arrStrLocalFilePath.at(nPreDeleteDirNum);		// 이전폴더
		strCurDeleteDir = pBackup->m_arrStrLocalFilePath.at(i);						// 현재폴더

		CString strLocalFile = 
			pBackup->m_arrStrLocalFilePath.at(i) + _T("\\") + 
			pBackup->m_arrStrFileName.at(i);
		CString strFilename = pBackup->m_arrStrFileName.at(i);

		//todo : change dir			
		CString strCurrentDir = 
			pBackup->m_strFtpRootPath + _T("/") +
			pBackup->m_arrStrFtpFilePath.at(i) + _T("/") + 
			strFilename;

		pBackup->FileUpload(pFtp, strLocalFile, strCurrentDir);
		
		CString strRet;
		strRet.Format(_T("%d - "), pBackup->m_arrBFileUploadCheck.at(i));
		strRet = strRet + strLocalFile;
		ESMLog(5, strRet);		

		if (pBackup->m_arrBFileUploadCheck.at(i) == 1)
		{
			if (strPreDeleteDir != strCurDeleteDir)
			{
				pBackup->DeleteAfterTransfer(strPreDeleteDir);				
			}
			else
			{
				if (i == pBackup->m_arrStrLocalFilePath.size()-1)
				{
					pBackup->DeleteAfterTransfer(strCurDeleteDir);
				}
			}
		}
	}
	pFtp->Disconnect();
	pBackup->SetBackupFtpState(BACKUP_FTP_PROCESS_STATE_END);

	pBackup->FtpInit();

// 	if (pBackup->m_bUseDeleteAfterTransfer)
// 		pBackup->DeleteAfterTransfer(pBackup->m_strLocalRootPath);

	return 0;
}

void CESMBackup::FileUpload(std::shared_ptr<CEMSFtp> pFtp, CString strLocalFile, CString strCurrentDir)
{
	m_arrBFileUploadCheck.push_back(pFtp->FileUpload(strLocalFile, strCurrentDir));

	//while(!m_arrBFileUploadCheck[m_arrBFileUploadCheck.size()-1])
	//{
	//	pFtp->Connect(
	//		m_strAddress,
	//		m_strID,
	//		m_strPWD, 
	//		m_strPort);
	//	m_arrBFileUploadCheck[m_arrBFileUploadCheck.size()-1] = pFtp->FileUpload(strLocalFile, strCurrentDir);		
	//}
}

void CESMBackup::CloseFtp()
{
	if(m_hHandle == NULL)
		return;

	SetBackupFtpState(BACKUP_FTP_PROCESS_STATE_END);
	::TerminateThread( m_hHandle, 0 );
	CloseHandle(m_hHandle);
	m_hHandle = NULL;
}

double CESMBackup::GetAllFilePathAndFileName(CString strPath, std::shared_ptr<CEMSFtp> pFtp)
{
	//std::shared_ptr<CEMSFtp> pFtp(new CEMSFtp());
	//pFtp->Connect(m_strAddress,m_strID,m_strPWD, m_strPort);	

	CString strAllInPath = strPath + _T("\\*.*");
	BOOL bExsit;
	CFileFind cFileFind;

	bExsit = cFileFind.FindFile(strAllInPath);
	while (bExsit)
	{
		bExsit = cFileFind.FindNextFileW();		

		if (cFileFind.IsDots())
			continue;		
		else
		{	
			CString strLocalFilePath	= strPath;
			CString strFtpFilePath		= strPath;

			if(m_strLocalRootPath.CompareNoCase(strFtpFilePath))
				strFtpFilePath.Replace(m_strLocalRootPath + _T("\\"), _T(""));
			else
				strFtpFilePath.Replace(m_strLocalRootPath, _T(""));
			strFtpFilePath.Replace(_T("\\"), _T("/"));

			if (cFileFind.IsDirectory())
			{
				CString strFileName = cFileFind.GetFileName();

				if (m_bUseFolderList)
				{
					if (CheckMatchFolder(strFileName) == FALSE)
					{
						continue;
					}
				}

				pFtp->CreateDir(
					m_strFtpRootPath + _T("/") + 
					strFtpFilePath + _T("/") + 
					strFileName);

				CString strNextDir = _T("");
				strNextDir = strPath + _T("\\") + strFileName;

				GetAllFilePathAndFileName(strNextDir, pFtp);
			}
			else
			{				
				m_arrStrLocalFilePath.push_back(strLocalFilePath);
				m_arrStrFtpFilePath.push_back(strFtpFilePath);
				m_arrStrFileName.push_back(cFileFind.GetFileName());				

				m_dFolderSize += cFileFind.GetLength() / 1048576.;
				m_nFileCount++;
			}	
		}		
	}
	cFileFind.Close();	
//	pFtp->Disconnect();	

	return m_dFolderSize;
}

CESMBackup::BACKUP_FTP_FILE_STATE CESMBackup::CheckUploadStateAllFile()
{
	if(m_arrBFileUploadCheck.size() == 0)
		return BACKUP_FTP_FILE_STATE_READY;

	m_nFileCount = 0;
	int nFileCount = GetFileCount(m_strLocalRootPath);
	if(m_arrBFileUploadCheck.size() < nFileCount)
		return BACKUP_FTP_FILE_STATE_NOTDONE;
	
	for(int i = 0; i < m_arrBFileUploadCheck.size(); i++)
	{
		if(!m_arrBFileUploadCheck.at(i))
			return BACKUP_FTP_FILE_STATE_INCOMPLETE;
	}
	return BACKUP_FTP_FILE_STATE_OK;
}

void CESMBackup::SetFTPInfo( CString host, CString port, CString id, CString pwd )
{
	m_strAddress = host;
	m_strID = id;
	m_strPWD = pwd;
}

void CESMBackup::SetUseFolderList( BOOL f )
{
	m_bUseFolderList = f;
}

int CESMBackup::GetFolderListCount()
{
	CString strFileServerPath;
	strFileServerPath.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));

	CString strPath;
	strPath.Format(_T("%s\\config\\%s"), strFileServerPath, ESM_4DMAKER_BACKUP_FOLDER_LIST);

	CESMIni ini;
	ini.SetIniFilename(strPath);
	int nCount = ini.GetSectionCount(_T("FolderList"));

	return nCount;
}

BOOL CESMBackup::CheckMatchFolder(CString strLocal, BOOL bDeleteAfter)
{
	CString strListStyle = ESM_4DMAKER_BACKUP_FOLDER_LIST;
	// only delete
	if (bDeleteAfter == FALSE)	
		strListStyle = ESM_4DMAKER_DELETE_FOLDER_LIST;

	// check folder list
	int nCount = GetFolderListCount();
	CString strFileServerPath;
	strFileServerPath.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));

	CString strPath;
	strPath.Format(_T("%s\\config\\%s"), strFileServerPath, strListStyle);

	CESMIni ini;
	ini.SetIniFilename(strPath);

	for (int i = 0; i < nCount ; i++)
	{
		CString strKey;
		strKey.Format(_T("%d"), i);
		CString strTemp = ini.GetString(_T("FolderList"), strKey);

		if (strTemp == strLocal)
		{
			return TRUE;
		}
	}

	return FALSE;
}

void CESMBackup::SetUseDeleteAfterTransfer( BOOL f )
{
	m_bUseDeleteAfterTransfer = f;
}

void CESMBackup::DeleteAfterTransfer(CString strPath, BOOL bDeleteAfter)
{
	CFileFind cFileFind;
	CString strFolderPath;

	BOOL b = cFileFind.FindFile(strPath + _T("\\*.*"));

	while(b)
	{
		b = cFileFind.FindNextFile();

		if (cFileFind.IsDots())
			continue;

		strFolderPath = cFileFind.GetFilePath();

		if (strFolderPath.IsEmpty())
			continue;

		if(cFileFind.IsDirectory())
		{
			CString strFileName = cFileFind.GetFileName();

			if (m_bUseFolderList || !bDeleteAfter)
			{
				if (CheckMatchFolder(strFileName, bDeleteAfter) == FALSE)
				{
					continue;
				}
			}

			strFolderPath = cFileFind.GetFilePath();

			if (strFolderPath.IsEmpty())
				continue;

			DeleteDir(strFolderPath);
		}
		else
		{
			DeleteDir(strFolderPath, FALSE);
		}
	}

	cFileFind.Close();
}

void CESMBackup::DeleteDir( CString strPath, BOOL bDir/*=TRUE*/ )
{
	BOOL bFlag = FALSE;
	CFileFind find;
	CString strRoot = _T("");

	if (bDir)
		strRoot.Format(_T("%s\\*.*"), strPath);
	else
		strRoot = strPath;

	bFlag = find.FindFile(strRoot);

	if( bFlag == FALSE )  
	{        
		return;  
	}

	while( bFlag )  
	{  
		bFlag = find.FindNextFile();

		if( find.IsDots() == FALSE )   
		{   
			if( find.IsDirectory() )   
			{   
				RemoveDirectory( find.GetFilePath() );   

				DeleteDir(find.GetFilePath());   
			}   
			else   
			{    
				bFlag = DeleteFile(find.GetFilePath()); 
			}  
		}
	} 
	find.Close();

	if (bDir) RemoveDirectory(strPath); 
}

void CESMBackup::DeleteSelectList()
{
	DeleteAfterTransfer(m_strLocalRootPath, FALSE);
}
