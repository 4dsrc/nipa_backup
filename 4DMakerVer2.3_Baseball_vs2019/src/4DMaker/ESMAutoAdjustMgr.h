#pragma once

#include <vector>
#include "cv.h"
#include "highgui.h"

#include "ESMFunc.h"
#include "ESMUtil.h"
#include "ESMIni.h"
#include "FFmpegManager.h"
#include "ESMObjectFrame.h"
#include "ESMObjectTracking.h"

#include "ESMAutoAdjustSquareCalibration.h"

class CESMAutoAdjustMgr
{
public:
	CESMAutoAdjustMgr(void);
	~CESMAutoAdjustMgr(void);

private:
	CESMAutoAdjustSquareCalibration* m_pSquareCalibration;
public:
	void SetSquareCalibration(CESMAutoAdjustSquareCalibration* pSquareCalibration);

private:
	vector<DscAdjustInfo*> m_ArrDscInfo;
public:
	void			AddDscInfo(CString strDscId, CString strDscIp,BOOL bReverse = FALSE);
	void			GetDscInfo(vector<DscAdjustInfo*>** pArrDscInfo);
	BOOL			IsEmptyAdjustInfo();
	int				GetDscCount();
	void			DscClear();	
	DscAdjustInfo*	GetDscAt(int nIndex);		
	int				GetDscIndexFrom(CString strDscId);

public:
	BOOL	SearchDetectPoint(IplImage* pNewGray, CSize MinSize, CSize MaxSize, vector<CRect>* vecDetectAreas, COLORREF clrDetectColor);
	BOOL	SearchPoint(IplImage*	pNewGray, vector<CPoint>* arrBeginPoint, vector<CPoint>* arrPointRange, vector<CPoint>* pArrPoint);
	BOOL	CalcAdjustData(DscAdjustInfo* pAdjustInfo, int nTargetLenth, int nZoom);
	BOOL	GetSearchDetectPoint(DscAdjustInfo* pAdjustInfo, int nThreshold, CSize minDetectSize, CSize maxDetectSize, int nPointGapMin, COLORREF clrDetectColor);	
	void	GetResolution(int &nWidth, int &nHeight);
	double	GetDistanceData(CString strDSCId);
	int		GetZoomData(CString strDSCId);

private:
	vector<stAdjustInfo> m_arrAdjInfo;	
	vector<stAdjustInfo> m_arrModifiedAdjInfo;
public:
	void			AddAdjInfo(stAdjustInfo adjustInfo);
	stAdjustInfo	GetAdjInfoAt(int index);
	void			ClearAdjInfo();

	//Mat				ShowImageFrom(int nSelectedItem, int marginX, int marginY);

private:
	int		Round(double dData);
	void	GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle);
	void	GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY);
	void	GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY);
	void	CpuMakeMargin(Mat* gMat, int nX, int nY);
	void	CpuMoveImage(Mat* gMat, int nX, int nY);
	void	CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle);	
	void	affinetransform(Mat img,cv::Point a,cv::Point b, cv::Point c, Point2f srcTri[3],Mat dst);

public:
	//CESMObjectTracking *m_pObjectTracking;

private:
	vector<AdjustSquareInfo*> m_vecAdjustSquareInfo;

public:
	void SetAdjustSquarePointAt(CString strDscId, CPoint pt1, CPoint pt2, CPoint pt3, CPoint pt4);
	void SetAdjustSquareInfo();
	void AddAdjustSquareInfo(
		int nIndex, CString strDscId, vector<CPoint> vecPtPoints,
		CPoint pt1, CPoint pt2, CPoint pt3, CPoint pt4);
	
	BOOL IsValidAdjustSquareInfo();

	vector<Point2f> GetAdjustSquareInfo(int nIndex);
	vector<Point2f> GetAdjustSquareInfo(CString strDscId);

	void SetCenterPointAt(CString strDscId, CPoint ptCenterPoint);
	void SetInitCenterPoint(CString strStartDscId, CString strEndDscId, CPoint ptCenterPoint);
	CPoint GetCenterPoint(CString strDscId);

	double GetNormValue(CString strDscId);
	void SetNormValue(CString strDscId, double dValue);

	double GetDistanceValue(CString strDscId);
	void SetDistanceValue(CString strDscId, double dValue);

	BOOL GetUseKZoneAt(CString strDscId);
	void SetUseKZoneAt(CString strDscId, BOOL bUseKZone);

	void ClearAdjustSquareInfo();


public:
	vector<Point3f> m_ptSquartPointInWorld;
	void SetSquarePointInWorld(CPoint point);
	void ClearSquarePointInWorld();
	vector<CPoint> GetSquarePointInWorld();

	void SetSquarePointInWorldAt(CString strDscId, vector<CPoint> vecCPoint);
	vector<CPoint> GetSquarePointInWorldAt(CString strDscId);

public:
	KZoneDataInfo* m_pKZoneDataInfo;
	BOOL m_bValidKZone;
	BOOL m_bToggleUsingKZone;

	KZoneDataInfo* GetKZonePoint();
	BOOL IsValidKZone();
	void SetKZonePoint();
	void SetToggleUsingKZone(bool bToggleUSingKZone);
	BOOL GetUsageKZone();

	int LoadSquarePointDataForKZone(CString strFileName);

public:
	void	tracking();
	void	moveImage(Mat* gMat, int nX, int nY);
};
