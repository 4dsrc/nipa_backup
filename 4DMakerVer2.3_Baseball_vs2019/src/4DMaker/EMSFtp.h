#pragma once

#include <afxsock.h> 
#include <afxinet.h> 

class CInternetSessionEx : public CInternetSession
{
public :
	CInternetSessionEx(
		LPCTSTR		pstrAgent = NULL,
		DWORD_PTR	dwContext = 1,
		DWORD		dwAccessType = PRE_CONFIG_INTERNET_ACCESS,
		LPCTSTR		pstrProxyName = NULL,
		LPCTSTR		pstrProxyBypass = NULL,
		DWORD		dwFlags = 0) 
		:CInternetSession(
		pstrAgent,
		dwContext,
		dwAccessType,	
		pstrProxyName ,	
		pstrProxyBypass ,
		dwFlags )
	{

	}
	 
	virtual ~CInternetSessionEx() 
	{

	}

	virtual void OnStatusCallback(DWORD_PTR dwContext, DWORD dwInternetStatus,
		LPVOID lpvStatusInformation, DWORD dwStatusInformationLength);

private:
	BOOL m_bConnect;
};

class CEMSFtp
{
public:
	CEMSFtp();	
	~CEMSFtp(void);	

private:
	int m_iPort;
	
	CInternetSessionEx m_session; 
	CFtpConnection * m_pConnection;
	CFtpFileFind *m_pFileFind;

public:	
	BOOL Connect(CString strAddress, CString strId, CString strPwd, CString strPort);
	void Disconnect();
	BOOL FileUpload(CString strLocalFile ,CString strRemoteFile);
	BOOL FileDownLoad(CString strRemoteFile,CString strLoaclFile);
	void CreateDir(CString dir);
	BOOL SetCurrentDir(CString strDir);
	CString GetCurrentDir();
	
};

