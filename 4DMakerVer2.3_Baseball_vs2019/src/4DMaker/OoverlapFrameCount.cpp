// OoverlapFrameCount.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "OoverlapFrameCount.h"
#include "afxdialogex.h"


// COoverlapFrameCount 대화 상자입니다.

IMPLEMENT_DYNAMIC(COoverlapFrameCount, CDialog)

COoverlapFrameCount::COoverlapFrameCount(CWnd* pParent /*=NULL*/)
	: CDialog(COoverlapFrameCount::IDD, pParent)
{
	m_nFrameCount = 0;
}

COoverlapFrameCount::~COoverlapFrameCount()
{
}

void COoverlapFrameCount::DoDataExchange(CDataExchange* pDX)
{	
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX,  IDC_EDIT_FRAME_COUNT		, m_nFrameCount);
}


BEGIN_MESSAGE_MAP(COoverlapFrameCount, CDialog)
END_MESSAGE_MAP()


// COoverlapFrameCount 메시지 처리기입니다.
