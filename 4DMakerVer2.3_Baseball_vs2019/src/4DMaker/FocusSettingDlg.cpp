// FocusSettingDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "FocusSettingDlg.h"
#include "afxdialogex.h"


// CFocusSettingDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFocusSettingDlg, CDialogEx)


CFocusSettingDlg::CFocusSettingDlg(int nMinFocus, int nMaxFocus, int nCurrentFocus, CWnd* pParent /*=NULL*/)
	: CDialogEx(CFocusSettingDlg::IDD, pParent)
{
	m_nMinFocus = nMinFocus;
	m_nMaxFocus = nMaxFocus;
	m_nCurrentFocus = nCurrentFocus;
}

CFocusSettingDlg::~CFocusSettingDlg()
{
}

void CFocusSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_FOCUS_MIN		, m_nMinFocus		);
	DDX_Text(pDX, IDC_FOCUS_MAX	, m_nMaxFocus	);
	DDX_Text(pDX, IDC_FOCUS_SET_VALUE		, m_nCurrentFocus		);
}




BEGIN_MESSAGE_MAP(CFocusSettingDlg, CDialogEx)
END_MESSAGE_MAP()


// CFocusSettingDlg 메시지 처리기입니다.

