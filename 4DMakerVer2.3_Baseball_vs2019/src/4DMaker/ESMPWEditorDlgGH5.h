#pragma once

#include "DSCItem.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "resource.h"
#include "ESMPWEditorDlgGH5WBAD.h"

class CESMPWEditorDlgSetInfo
{
public:
	CESMPWEditorDlgSetInfo()
	{
		strPhotoStyle = "";
		nPS = 0;
		nWBAB = 0;
		nWBMG = 0;
		nContrast = 0;
		nSharpness = 0;
		nNoise = 0;
		nSaturation = 0;
		nHue = 0;
		nFilter = 0;
	}

	CString strPhotoStyle;
	int nPS;
	int nWBAB;
	int nWBMG;
	int nContrast, nSharpness, nNoise, nSaturation, nHue, nFilter;	
	
};

// CESMPWEditorDlgGH5 대화 상자입니다.

class CESMPWEditorDlgGH5 : public CDialogEx
{
	DECLARE_DYNAMIC(CESMPWEditorDlgGH5)

public:
	CESMPWEditorDlgGH5(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMPWEditorDlgGH5();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ESMPWEDITORDLG_GH5 };

	enum WB_ADJUST_TYPE
	{
		WB_ADJUST_TYPE_AB=0,
		WB_ADJUST_TYPE_MG
	};

	enum WB_ADJUST_AB
	{
		WB_ADJUST_AB_A9 = 0x8009,
		WB_ADJUST_AB_A8 = 0x8008,
		WB_ADJUST_AB_A7 = 0x8007,
		WB_ADJUST_AB_A6 = 0x8006,
		WB_ADJUST_AB_A5 = 0x8005,
		WB_ADJUST_AB_A4 = 0x8004,
		WB_ADJUST_AB_A3 = 0x8003,
		WB_ADJUST_AB_A2 = 0x8002,
		WB_ADJUST_AB_A1 = 0x8001,
		WB_ADJUST_AB_A0B0 = 0,
		WB_ADJUST_AB_B1 = 0x1,
		WB_ADJUST_AB_B2 = 0x2,
		WB_ADJUST_AB_B3 = 0x3,
		WB_ADJUST_AB_B4 = 0x4,
		WB_ADJUST_AB_B5 = 0x5,
		WB_ADJUST_AB_B6 = 0x6,
		WB_ADJUST_AB_B7 = 0x7,
		WB_ADJUST_AB_B8 = 0x8,
		WB_ADJUST_AB_B9 = 0x9,
	};

	enum WB_ADJUST_MG
	{
		WB_ADJUST_MG_M9 = 0x8009,
		WB_ADJUST_MG_M8 = 0x8008,
		WB_ADJUST_MG_M7 = 0x8007,
		WB_ADJUST_MG_M6 = 0x8006,
		WB_ADJUST_MG_M5 = 0x8005,
		WB_ADJUST_MG_M4 = 0x8004,
		WB_ADJUST_MG_M3 = 0x8003,
		WB_ADJUST_MG_M2 = 0x8002,
		WB_ADJUST_MG_M1 = 0x8001,
		WB_ADJUST_MG_M0G0 = 0,
		WB_ADJUST_MG_G1 = 0x1,
		WB_ADJUST_MG_G2 = 0x2,
		WB_ADJUST_MG_G3 = 0x3,
		WB_ADJUST_MG_G4 = 0x4,
		WB_ADJUST_MG_G5 = 0x5,
		WB_ADJUST_MG_G6 = 0x6,
		WB_ADJUST_MG_G7 = 0x7,
		WB_ADJUST_MG_G8 = 0x8,
		WB_ADJUST_MG_G9 = 0x9,
	};

	enum PHOTO_STYLE
	{		
		PHOTO_STYLE_Unknown			= 0x0,
		PHOTO_STYLE_Standard		= 0x1,
		PHOTO_STYLE_Vivid			= 0x2,
		PHOTO_STYLE_Natural			= 0x3,
		PHOTO_STYLE_Monochrome		= 0x4,
		PHOTO_STYLE_L_Monochrome	= 0x5,	
		PHOTO_STYLE_Scenery			= 0x6,
		PHOTO_STYLE_Portrait		= 0x7,
		PHOTO_STYLE_Custom1			= 0x8,
		PHOTO_STYLE_Custom2			= 0x9,
		PHOTO_STYLE_Custom3			= 0xA,
		PHOTO_STYLE_Custom4			= 0xB,
		PHOTO_STYLE_Cinelike_D		= 0xC,
		PHOTO_STYLE_Cinelike_V		= 0xD,
		PHOTO_STYLE_Like709			= 0xE,		
	};

	enum PS_VALUE
	{
		PS_VALUE_M_5	= 0x8005,
		PS_VALUE_M_4	= 0x8004,
		PS_VALUE_M_3	= 0x8003,
		PS_VALUE_M_2	= 0x8002,
		PS_VALUE_M_1	= 0x8001,
		PS_VALUE_0		= 0x0,
		PS_VALUE_P_1	= 0x1,
		PS_VALUE_P_2	= 0x2,
		PS_VALUE_P_3	= 0x3,
		PS_VALUE_P_4	= 0x4,
		PS_VALUE_P_5	= 0x5,

		PS_VALUE_CNT	= 11,
	};

	enum PS_VALUE_FILTER_EFFECT
	{
		PS_VALUE_FILTER_EFFECT_OFF	 = 0,
		PS_VALUE_FILTER_EFFECT_YELLOW	,	
		PS_VALUE_FILTER_EFFECT_ORANGE	,	
		PS_VALUE_FILTER_EFFECT_RED		,
		PS_VALUE_FILTER_EFFECT_GREEN	,
	};

private:
	CDSCItem* m_pItem;
	CSliderCtrl m_ctrlWBAB;
	CSliderCtrl m_ctrlWBMG;
	CStatic m_crtlValueAB;
	CStatic m_crtlValueMG;

	CSliderCtrl m_ctrlContrast;
	CSliderCtrl m_ctrlSharpness;
	CSliderCtrl m_ctrlNoiseReduction;
	CSliderCtrl m_ctrlSaturation;
	CSliderCtrl m_ctrlHue;
	CSliderCtrl m_ctrlFilterEffect;

	int m_nWBAB;
	int m_nWBMG;

	CESMPWEditorDlgSetInfo m_setInfo;
	CESMPWEditorDlgGH5WBAD* m_WBAdjustDlg;

	PHOTO_STYLE m_photoStyle;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	void SetInfo(CDSCItem* pItem, CESMPWEditorDlgSetInfo info);
	afx_msg void OnBnClickedBtnApplyAll();
	afx_msg void OnBnClickedButtonOk();
	
private:
	int GetPosPSValue(int nType);
	int GetPosAB();
	int GetPosMG();
	void SetPosInfo();
	void SetPosInfo(WB_ADJUST_TYPE type, int nValue);
	void LoadPosText();
	void SetChangePhotoStyleList(PHOTO_STYLE nPhotoStyle);
	void SendSdiMsg(CDSCItem* pItem, int nCode, int nValue);

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnNMReleasedcaptureSliderWbAb(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMReleasedcaptureSliderWbMg(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMReleasedcaptureSliderContrast(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMReleasedcaptureSliderSharpness(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMReleasedcaptureSliderNoiseReduction(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMReleasedcaptureSliderSaturation(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMReleasedcaptureSliderHue(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMReleasedcaptureSliderFilterEffect(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);	
protected:
	afx_msg LRESULT OnEsmWbAdjust(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedBtnReset();
};
