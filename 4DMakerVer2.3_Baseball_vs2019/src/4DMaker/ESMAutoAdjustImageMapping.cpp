#include "stdafx.h"
#include "ESMAutoAdjustImageMapping.h"


CESMAutoAdjustImageMapping::CESMAutoAdjustImageMapping(void)
{
	_dScale = 1.;

	eSelectKeypoint = SELECT_KEYPOINT_FAST;

	_dKDistanceCoef = 10.0;
	_nKMaxMatchingSize = 10000;

	_nMinKeypointsCount = 10000;
	_nMaxKeypointsCount = 15000;

	_nWidth = 3840;
	_nHeight = 2160;

	//Rt0 = Mat::eye(3, 4, CV_64FC1);
}


CESMAutoAdjustImageMapping::~CESMAutoAdjustImageMapping(void)
{
}

#include <opencv2/cudabgsegm.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudafilters.hpp>
#include <opencv2/cudafeatures2d.hpp>

#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/xfeatures2d.hpp>

#include "ESMFunc.h"

#include <set>

bool CESMAutoAdjustImageMapping::RunSquareDetecting(Mat& image)
{
	AdjustFeatureInfo* pAdjustFeatureInfo = new AdjustFeatureInfo;

	Mat mCurrDescript;
	vector<KeyPoint> vecCurrKeypoints;

	if(_vecAdjustFeatureInfo.size() == 1)
	{
		Mat temp;
		_vecAdjustFeatureInfo[0]->mGrayImage(GetImageROI()).copyTo(temp);
		GetKeyPointsFAST(temp, vecCurrKeypoints, _nMinKeypointsCount, _nMaxKeypointsCount, mCurrDescript);

		if(vecCurrKeypoints.size() <= 0)
			return false;

		circle(image, Point2f(_vecSquarePoint[0].x, _vecSquarePoint[0].y), 5, cv::Scalar(255,0,0), 2, 8);
		circle(image, Point2f(_vecSquarePoint[1].x, _vecSquarePoint[1].y), 5, cv::Scalar(255,0,0), 2, 8);
		circle(image, Point2f(_vecSquarePoint[2].x, _vecSquarePoint[2].y), 5, cv::Scalar(255,0,0), 2, 8);
		circle(image, Point2f(_vecSquarePoint[3].x, _vecSquarePoint[3].y), 5, cv::Scalar(255,0,0), 2, 8);

		//_mReferenceImage = CreateWarpingImage(image);
		//imshow("ReferenceImage", _mReferenceImage);
		//waitKey(1);
		return true;
	}
	else
	{
		Mat mPrevGrayImageTemp, mCurrGrayImageTemp;
		_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->mGrayImage(GetImageROI()).copyTo(mCurrGrayImageTemp);
		_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-2]->mGrayImage(GetImageROI()).copyTo(mPrevGrayImageTemp);

		GetKeyPointsFAST(mCurrGrayImageTemp, vecCurrKeypoints, _nMinKeypointsCount, _nMaxKeypointsCount, mCurrDescript);
		GetKeyPointsFAST(mPrevGrayImageTemp, _vecPrevKeypoints, _nMinKeypointsCount, _nMaxKeypointsCount, _mPrevDescript);

		if(vecCurrKeypoints.size() <= 0 || _vecPrevKeypoints.size() <= 0)
			return false;

		vector<DMatch> vecDMMatches;
		MatchingWithOpticalFlow(
			vecDMMatches, 
			mPrevGrayImageTemp, mCurrGrayImageTemp, 
			_vecPrevKeypoints, vecCurrKeypoints);

		if(vecDMMatches.size() <= 0)
			return false;
		vector<char> vecSzMatchesMask(vecDMMatches.size(), 1);

		//ESMLog(5,_T("KeyPoint Count : %d / Matches Count : %d"), 
		//	vecCurrKeypoints.size(), vecDMMatches.size());

		//if((double)vecDMMatches.size() / vecCurrKeypoints.size() < 0.02)
		//{
		//	ESMLog(0, _T("Matching Fail : %lf %"), 
		//		vecDMMatches.size()/(double)vecCurrKeypoints.size() * 100);
		//	return false;
		//}

		if((double)vecDMMatches.size() / vecCurrKeypoints.size() < 0.1)
		{
			ESMLog(5,_T("[Warning]AutoAdjust estimation result of %S may not be accurate. "),
				_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->strDscId);
		}

		Mat mHomography;
		//if(vecDMMatches.size() < 100000)
		//{
		//	if(vecDMMatches.size() == 0)
		//		return;

		//	vector<DMatch> vecMatches;
		//	vecSzMatchesMask.clear();
		//	//Matching(vecMatches, _mPrevDescript, mCurrDescript);

		//	if(vecMatches.size() == 0)
		//		return;

		//	DrawKeypoints(image, 
		//		mPrevGrayImageTemp, mCurrGrayImageTemp,
		//		_vecPrevKeypoints, vecCurrKeypoints,
		//		vecMatches, 
		//		vecSzMatchesMask);
		//	mHomography = FindKeyPointsHomography(image,vecMatches, _vecPrevKeypoints, vecCurrKeypoints, vecSzMatchesMask);
		//	//mHomography = Find3DKeyPointsHomography(vecMatches, _vecPrevKeypoints, vecCurrKeypoints, vecSzMatchesMask);
		//} 
		//else
		{
			//DrawKeypoints(image, 
			//	mPrevGrayImageTemp, mCurrGrayImageTemp,
			//	_vecPrevKeypoints, vecCurrKeypoints,
			//	vecDMMatches,
			//	vecSzMatchesMask);
			mHomography = FindKeyPointsHomography(image,
				vecDMMatches, 
				_vecPrevKeypoints, 
				vecCurrKeypoints, 
				vecSzMatchesMask);
		}

		CalcSquarePoint(
			_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-2]->vecSquarePoint, 
			_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->vecSquarePoint, 
			mHomography);

		//Mat mWarpingImage = CreateWarpingImage(image);

		//float fTransformErr = CheckTransformError(_mReferenceImage, mWarpingImage);
		//float fOcclusiontErr = CheckOcclusion(_mReferenceImage, mWarpingImage);

		//ESMLog(5, _T("LineCheck (%d:%d) : %lf"), mWarpingImage.cols, mWarpingImage.rows, fTransformErr);	
		//ESMLog(5, _T("AutoAdjustTest SubTrack Image Value : %f"), fOcclusiontErr);

		//if(fTransformErr > 0.03)
		//{

		//}

		line(image, 
			_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->vecSquarePoint[0], 
			_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->vecSquarePoint[1], 
			Scalar(255, 0, 0), 3, 8, 0);
		line(image, 
			_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->vecSquarePoint[1], 
			_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->vecSquarePoint[2], 
			Scalar(255, 0, 0), 3, 8, 0);
		line(image, 
			_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->vecSquarePoint[2], 
			_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->vecSquarePoint[3], 
			Scalar(255, 0, 0), 3, 8, 0);
		line(image, 
			_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->vecSquarePoint[3], 
			_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->vecSquarePoint[0], 
			Scalar(255, 0, 0), 3, 8, 0);

		//imshow("Test", image);
		//waitKey(1);
	}

	//SetGroundTruth(Mat()/*mCurrGrayImage*/, vecCurrKeypoints, _vecSquarePoint, mCurrDescript);
	return true;
}

float CESMAutoAdjustImageMapping::CheckTransformError(Mat mReferenceImage, Mat mWarpingImage)
{
	vector<KeyPoint> vecReferenceKeyPoint, vecWarpingKeyPoint;
	Mat mReferenceDescript, mWarpingDescript;
	GetKeyPointsFAST(_mReferenceImage, vecReferenceKeyPoint, 8000, 10000, mReferenceDescript);
	GetKeyPointsFAST(mWarpingImage, vecWarpingKeyPoint, 8000, 10000, mWarpingDescript);

	vector<DMatch> vecReferenceMatches;
	MatchingWithOpticalFlow(
		vecReferenceMatches, 
		mReferenceImage, mWarpingImage, 
		vecReferenceKeyPoint, vecWarpingKeyPoint);
	vector<char> vecSzReferenceMatchesMask(vecReferenceMatches.size(), 1);

	if(vecReferenceMatches.size() < 10)
	{
		ESMLog(5, _T("Reference Matching Error"));
		return -1;
	}

	//Matching Check Line
	vector<Point2f> ptCheckLine;
	ptCheckLine.push_back(Point2f(0, 0));
	ptCheckLine.push_back(Point2f(_mReferenceImage.cols, 0));
	ptCheckLine.push_back(Point2f(_mReferenceImage.cols, _mReferenceImage.rows));
	ptCheckLine.push_back(Point2f(0, _mReferenceImage.rows));

	Mat mReferenceHomography = FindKeyPointsHomography(
		mWarpingImage,
		vecReferenceMatches, 
		vecReferenceKeyPoint, 
		vecWarpingKeyPoint, 
		vecSzReferenceMatchesMask);
	vector<Point2f> ptCheckLine2;

	for(int i = 0; i < ptCheckLine.size(); i++)
	{
		Mat_<double> mCurrentPointMat = (
			Mat_<double>(3,1) << 
			(double)ptCheckLine[i].x, 
			(double)ptCheckLine[i].y, 
			1.
			);

		Mat mResultPointMat = mReferenceHomography * mCurrentPointMat;

		ptCheckLine2.push_back(Point2f(
			mResultPointMat.at<double>(0) / mResultPointMat.at<double>(2),
			mResultPointMat.at<double>(1) / mResultPointMat.at<double>(2)
			));
	}

	line(mWarpingImage, ptCheckLine2[0], ptCheckLine2[1], Scalar(255, 0, 0), 3, 8, 0);
	line(mWarpingImage, ptCheckLine2[1], ptCheckLine2[2], Scalar(255, 0, 0), 3, 8, 0);
	line(mWarpingImage, ptCheckLine2[2], ptCheckLine2[3], Scalar(255, 0, 0), 3, 8, 0);
	line(mWarpingImage, ptCheckLine2[3], ptCheckLine2[0], Scalar(255, 0, 0), 3, 8, 0);

	float fCheckLineNorm = (
		sqrt(	(double)pow((double)(ptCheckLine[0].x - ptCheckLine2[0].x), 2.) + 
				(double)pow((double)(ptCheckLine[0].y - ptCheckLine2[0].y), 2.)) +
		sqrt(	(double)pow((double)(ptCheckLine[1].x - ptCheckLine2[1].x), 2.) + 
				(double)pow((double)(ptCheckLine[1].y - ptCheckLine2[1].y), 2.)) +
		sqrt(	(double)pow((double)(ptCheckLine[2].x - ptCheckLine2[2].x), 2.) + 
				(double)pow((double)(ptCheckLine[2].y - ptCheckLine2[2].y), 2.)) +
		sqrt(	(double)pow((double)(ptCheckLine[3].x - ptCheckLine2[3].x), 2.) + 
				(double)pow((double)(ptCheckLine[3].y - ptCheckLine2[3].y), 2.))
				) / 
		(_rtImageROI.width + _rtImageROI.height) * 2;

	imshow("WarpingImage", mWarpingImage );
	imshow("ReferenceImage", _mReferenceImage);
	waitKey(1);

	return fCheckLineNorm;
}

float CESMAutoAdjustImageMapping::CheckOcclusion(Mat mReferenceImage, Mat mWarpingImage)
{
	Mat mSubtrackImage;
	subtract(_mReferenceImage, mWarpingImage, mSubtrackImage);

	cvtColor(mSubtrackImage, mSubtrackImage, cv::COLOR_RGB2GRAY);
	float totalPixelValue = mSubtrackImage.cols*mSubtrackImage.rows*255*3;
	float sumPixelValue = 0;

	for (int j = 0; j < mSubtrackImage.rows - 1; j++)
		for (int i = 0; i < mSubtrackImage.cols - 1; i++)
			sumPixelValue += mSubtrackImage.data[j * mSubtrackImage.step + (i+0)];
	//for (int j = 0; j < mSubtrackImage.rows - 1; j++)
	//	for (int i = 0; i < mSubtrackImage.cols - 1; i++)
	//		sumPixelValue += mSubtrackImage.data[j * mSubtrackImage.step + (i+1)];
	//for (int j = 0; j < mSubtrackImage.rows - 1; j++)
	//	for (int i = 0; i < mSubtrackImage.cols - 1; i++)
	//		sumPixelValue += mSubtrackImage.data[j * mSubtrackImage.step + (i+2)];

	float result = sumPixelValue/totalPixelValue;

	imshow("SubtrackImage", mSubtrackImage );
	waitKey(1);
	
	return result;
}

vector<Point2f> CESMAutoAdjustImageMapping::GetModifiedSquarePoint()
{
	return _vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->vecSquarePoint;
}

Mat CESMAutoAdjustImageMapping::CreateWarpingImage(Mat image)
{
	static vector<Point2f> vecInitSquarePoint = _vecSquarePoint;
	//Reference warping
	double dReferenceSizeRatio = 
		(_vecReferenceSquarePoints[0].x - _vecReferenceSquarePoints[3].x) /
		(_vecReferenceSquarePoints[0].y - _vecReferenceSquarePoints[1].y);

	if(dReferenceSizeRatio > 1.)	
	{
		dReferenceSizeRatio=
		(_vecReferenceSquarePoints[0].y - _vecReferenceSquarePoints[3].y) /
		(_vecReferenceSquarePoints[0].x - _vecReferenceSquarePoints[1].x);
	}


	int nWarpWidth = sqrt(
		pow((double)(vecInitSquarePoint[2].x - vecInitSquarePoint[3].x), 2.) + 
		pow((double)(vecInitSquarePoint[2].y - vecInitSquarePoint[3].y), 2.));

	int nWarpHeight = nWarpWidth *abs(dReferenceSizeRatio);
	Mat mWarpImage(cv::Size(nWarpWidth , nWarpHeight), image.type());

	vector<Point2f> ptCorners(4);
	ptCorners[0]=Point2f(_vecSquarePoint[0].x, _vecSquarePoint[0].y);
	ptCorners[1]=Point2f(_vecSquarePoint[1].x, _vecSquarePoint[1].y);
	ptCorners[2]=Point2f(_vecSquarePoint[2].x, _vecSquarePoint[2].y);
	ptCorners[3]=Point2f(_vecSquarePoint[3].x, _vecSquarePoint[3].y);

	vector<Point2f> warpCorners(4);
	warpCorners[0]=Point2f(0, 0);
	warpCorners[3]=Point2f(0, mWarpImage.rows);
	warpCorners[2]=Point2f(mWarpImage.cols, mWarpImage.rows);
	warpCorners[1]=Point2f(mWarpImage.cols, 0);

	Mat trans = getPerspectiveTransform(ptCorners, warpCorners);

	//double* data = (double*)trans.data;
	//data[8] *= (double)1.1;

	//Warping
	warpPerspective(image, mWarpImage, trans, cv::Size(nWarpWidth , nWarpHeight));
	imshow("warpImg", mWarpImage);
	waitKey(1);

	return mWarpImage;
}

void CESMAutoAdjustImageMapping::MatchingWithOpticalFlow(
	vector<DMatch>& vecMatches,
	Mat& mPrevImage,
	Mat& mCurrImage,
	vector<KeyPoint>& vecPrevKeypoints,
	vector<KeyPoint>& vecCurrKeypoints)
{
	vector<Point2f> vecPtPrevKeyPoints;
	KeyPoint::convert(vecPrevKeypoints, vecPtPrevKeyPoints);

	vector<Point2f> vecPtLKTrackedPoints(vecPtPrevKeyPoints.size());

	vector<uchar> vecSzStatus; vector<float> vecFError;
	calcOpticalFlowPyrLK(mPrevImage, mCurrImage, vecPtPrevKeyPoints, vecPtLKTrackedPoints,
		vecSzStatus, vecFError, cv::Size(15, 15), 10);

	//for(int i = 0; i < vecPtLKTrackedPoints.size(); i++)
	//{
	//	if (vecSzStatus[i] &&vecFError[i] < 12.0 * 3) 
	//	{
	//		circle(_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->mImage(GetImageROI()), vecPtPrevKeyPoints[i], 3, cv::Scalar(255, 0, 0), 1, 8, 0);
	//		circle(_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-2]->mImage(GetImageROI()), vecPtPrevKeyPoints[i], 3, cv::Scalar(255, 0, 0), 1, 8, 0);
	//		line(_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->mImage(GetImageROI()), vecPtPrevKeyPoints[i], vecPtLKTrackedPoints[i], cv::Scalar(0, 0, 255), 1, 8, 0);
	//	}
	//}
	//imshow("Flow1", _vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-1]->mImage(GetImageROI()));
	//imshow("Flow2", _vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-2]->mImage(GetImageROI()));
	//waitKey(0);

	// First, filter out the points with high error
	vector<Point2f> vecPtFilteredCurrKeyPoints;
	vector<int> vecNFilteredCurrKeyPointsIndex;
	for (unsigned int i = 0; i < vecSzStatus.size(); i++) {
		if (vecSzStatus[i] &&vecFError[i] < 12.0 * 3) 
		{
			vecNFilteredCurrKeyPointsIndex.push_back(i);
			vecPtFilteredCurrKeyPoints.push_back(vecPtLKTrackedPoints[i]);
		} else {
			vecSzStatus[i] = 0;
		}
	}
	Mat mFilteredCurrKeyPoints = Mat(vecPtFilteredCurrKeyPoints).reshape(1, vecPtFilteredCurrKeyPoints.size()); //flatten array

	vector<Point2f>vecPtCurrKeyPoints; // detected features
	KeyPoint::convert(vecCurrKeypoints,vecPtCurrKeyPoints);
	Mat mCurrKeyPoints = Mat(vecPtCurrKeyPoints).reshape(1,vecPtCurrKeyPoints.size());

	cv::BFMatcher* matcher = new cv::BFMatcher(cv::/*NORM_L2*/NORM_L2SQR, false);
	vector<vector<DMatch>> vecDMNearestNeighbors;
	matcher->radiusMatch(
		mFilteredCurrKeyPoints,
		mCurrKeyPoints,
		vecDMNearestNeighbors,
		2.0f);

	//cuda::DescriptorMatcher *matcher = cuda::DescriptorMatcher::createBFMatcher(cv::NORM_HAMMING);
	////Ptr<cuda::DescriptorMatcher> matcher = cuda::DescriptorMatcher::createBFMatcher(cv::NORM_HAMMING);
	//vector<vector<DMatch>> vecDMNearestNeighbors;
	//matcher->radiusMatch(mFilteredCurrKeyPoints, mCurrKeyPoints, vecDMNearestNeighbors, 2.0f);

	vector<DMatch> vecDMMatches;
	std::set<int> setNLKTrackedPoints;
	for(int i = 0;i<vecDMNearestNeighbors.size();i++) {
		DMatch dmMatch;
		if(vecDMNearestNeighbors[i].size()==1) {
			dmMatch= vecDMNearestNeighbors[i][0];
		} else if(vecDMNearestNeighbors[i].size()>1) {
			double ratio = vecDMNearestNeighbors[i][0].distance /
				vecDMNearestNeighbors[i][1].distance;
			if(ratio < 0.7) {
				dmMatch= vecDMNearestNeighbors[i][0];
			} else {
				continue;
			}
		} else {
			continue;
		}
		if (setNLKTrackedPoints.find(dmMatch.trainIdx) ==
			setNLKTrackedPoints.end()) 
		{
			dmMatch.queryIdx = vecNFilteredCurrKeyPointsIndex[dmMatch.queryIdx];
			vecDMMatches.push_back(dmMatch); // add this match
			setNLKTrackedPoints.insert(dmMatch.trainIdx);
		}
	}
	vecMatches = vecDMMatches;
}

void CESMAutoAdjustImageMapping::SetSquarePoint(vector<Point2f> vecSquarePoint)
{
	//pt1.x = pt1.x - _rtImageROI.x;
	//pt1.y = pt1.y - _rtImageROI.y;
	//pt2.x = pt2.x - _rtImageROI.x;
	//pt2.y = pt2.y - _rtImageROI.y;
	//pt3.x = pt3.x - _rtImageROI.x;
	//pt3.y = pt3.y - _rtImageROI.y;
	//pt4.x = pt4.x - _rtImageROI.x;
	//pt4.y = pt4.y - _rtImageROI.y;

	_vecSquarePoint.clear();
	_vecSquarePoint = vecSquarePoint;
	//_vecSquarePoint.push_back(pt1);
	//_vecSquarePoint.push_back(pt2);
	//_vecSquarePoint.push_back(pt3);
	//_vecSquarePoint.push_back(pt4);
}

void CESMAutoAdjustImageMapping::SetReferenceSquarePoints(vector<Point2f> vecReferenceSquarePoints)
{
	if(vecReferenceSquarePoints.size() != 4)
	{
		vecReferenceSquarePoints.clear();
		return;
	}
	_vecReferenceSquarePoints = vecReferenceSquarePoints;


}

void CESMAutoAdjustImageMapping::CalcSquarePoint(vector<Point2f> vecSquarePointROI, vector<Point2f>& vecSquarePoint, Mat mHomography)
{
	if(vecSquarePointROI.size() != 4)
		return;

	vecSquarePoint.clear();
	//for(int i = 0; i < vecSquarePointROI.size(); i++)
	//{
	//	Mat_<double> mCurrentPointMat = (
	//		Mat_<double>(3,1) << (double)vecSquarePointROI[i].x, (double)vecSquarePointROI[i].y, 1.
	//		);

	//	Mat mResultPointMat = mHomography * mCurrentPointMat;

	//	vecSquarePoint.push_back(Point2f(
	//		mResultPointMat.at<double>(0) / mResultPointMat.at<double>(2),
	//		mResultPointMat.at<double>(1) / mResultPointMat.at<double>(2)
	//		));
	//}

	for(int i = 0; i < vecSquarePointROI.size(); i++)
	{
		Mat_<double> mCurrentPointMat = (
			Mat_<double>(3,1) << 
			(double)vecSquarePointROI[i].x - _rtImageROI.x, 
			(double)vecSquarePointROI[i].y - _rtImageROI.y, 
			1.
			);

		Mat mResultPointMat = mHomography * mCurrentPointMat;

		vecSquarePoint.push_back(Point2f(
			mResultPointMat.at<double>(0) / mResultPointMat.at<double>(2) + _rtImageROI.x,
			mResultPointMat.at<double>(1) / mResultPointMat.at<double>(2) + _rtImageROI.y
			));
	}
}

void CESMAutoAdjustImageMapping::SetImageROI(int nX, int nY, int nWidth, int nHeight)
{
	_rtImageROI.x = nX;
	_rtImageROI.y = nY;
	_rtImageROI.width = nWidth;
	_rtImageROI.height = nHeight;
}

Rect2d CESMAutoAdjustImageMapping::GetImageROI()
{
	if(_rtImageROI.x < 0)
		_rtImageROI.x = 0;
	if(_rtImageROI.y < 0)
		_rtImageROI.y = 0;
	if(_rtImageROI.x + _rtImageROI.width >= _nWidth)
		_rtImageROI.width = _nWidth - _rtImageROI.x;
	if(_rtImageROI.y + _rtImageROI.height >= _nHeight)
		_rtImageROI.height = _nHeight - _rtImageROI.y;

	if(_rtImageROI.x > _nWidth || _rtImageROI.y > _nHeight)
		_rtImageROI = Rect2d();

	return _rtImageROI;
}

void CESMAutoAdjustImageMapping::SetGroundTruth(Mat& mCurrGrayImage, vector<KeyPoint>& vecCurrKeypoints, vector<Point2f>& vecCurrSquarePoint, Mat& mCurrDescript)
{
	_mPrevGrayImage		= mCurrGrayImage.clone();
	//_mPrevDescript		= mCurrDescript;
	_vecPrevKeypoints.clear();
	//_vecPrevKeypoints	= vecCurrKeypoints;
	_vecPrevSquarePoint.clear();
	_vecPrevSquarePoint	= vecCurrSquarePoint;
}

void CESMAutoAdjustImageMapping::BilateralFilter(Mat mCurrGrayImage, Mat& filterImage)
{
	cuda::GpuMat gpuGrayImage(mCurrGrayImage);	
	cuda::GpuMat gpuResizeImage;
	cuda::GpuMat gpuFilterImage;

	cuda::resize(gpuGrayImage, gpuResizeImage, 
		cv::Size(gpuGrayImage.cols / _dScale, gpuGrayImage.rows / _dScale));
	cuda::bilateralFilter(gpuResizeImage, gpuFilterImage, 
		10/_dScale*(1.15), 
		50/_dScale*(1.5), 
		50/_dScale*(1.5));
	gpuFilterImage.download(filterImage);
}

void CESMAutoAdjustImageMapping::GetKeyPoints(SELECT_KEYPOINT eSelectKeypoint, Mat mCurrGrayImage, vector<KeyPoint>& vecCurrKeypoints, Mat& mCurrDescript)
{
	Mat filterImage;
	mCurrGrayImage.copyTo(filterImage);

	vecCurrKeypoints.clear();
	mCurrDescript.release();

	switch(eSelectKeypoint)
	{
	case SELECT_KEYPOINT_ORB:
		{
			Ptr<ORB> pDetector = ORB::create();
			pDetector->detectAndCompute(filterImage, Mat(), vecCurrKeypoints, mCurrDescript);
			break;
		}
	case SELECT_KEYPOINT_SIMPLEBLOB:
		{
			Ptr<SimpleBlobDetector> pDetector = SimpleBlobDetector::create();
			pDetector->detect(filterImage, vecCurrKeypoints);
			break;
		}
	case SELECT_KEYPOINT_FAST:
		{
			Ptr<FastFeatureDetector> pDetector = FastFeatureDetector::create(5/*40*/);
			
			pDetector->detect(filterImage, vecCurrKeypoints);
			break;
		}
	case SELECT_KEYPOINT_BRISK:
		{
			Ptr<BRISK> pDetector = BRISK::create();
			pDetector->detectAndCompute(filterImage, Mat(), vecCurrKeypoints, mCurrDescript);
			break;
		}
	case SELECT_KEYPOINT_KAZE:
		{
			Ptr<KAZE> pDetector = KAZE::create();
			pDetector->detectAndCompute(filterImage, Mat(), vecCurrKeypoints, mCurrDescript);
			break;
		}
	case SELECT_KEYPOINT_AKAZE:
		{
			Ptr<AKAZE> pDetector = AKAZE::create();
			pDetector->detectAndCompute(filterImage, Mat(), vecCurrKeypoints, mCurrDescript);
			break;
		}
	case SELECT_KEYPOINT_SURF:
		{
			Ptr<cv::xfeatures2d::SURF> pDetector = cv::xfeatures2d::SURF::create(800.);
			pDetector->detectAndCompute(filterImage, Mat(), vecCurrKeypoints, mCurrDescript);
			break;
		}
	case SELECT_KEYPOINT_SIFT:
		{
			Ptr<cv::xfeatures2d::SIFT> pDetector = cv::xfeatures2d::SIFT::create(800.);
			pDetector->detectAndCompute(filterImage, Mat(), vecCurrKeypoints, mCurrDescript);
			break;
		}
	case SELECT_KEYPOINT_FREAK:
		{
			Ptr<xfeatures2d::FREAK> pDetector = xfeatures2d::FREAK::create();
			pDetector->compute(filterImage, vecCurrKeypoints, mCurrDescript);
			break;
		}
	case SELECT_KEYPOINT_DAISY:
		{
			Ptr<cv::xfeatures2d::DAISY> pDetector = cv::xfeatures2d::DAISY::create();
			pDetector->compute(filterImage, vecCurrKeypoints, mCurrDescript);
			break;
		}
	case SELECT_KEYPOINT_BRIEF:
		{
			Ptr<cv::xfeatures2d::BriefDescriptorExtractor> pDetector = cv::xfeatures2d::BriefDescriptorExtractor::create();
			pDetector->compute(filterImage, vecCurrKeypoints, mCurrDescript);
			break;
		}
	}
}

bool CESMAutoAdjustImageMapping::IsInside(Point2f B, const vector<Point2f> p){
	int crosses = 0;
	for(int i = 0 ; i < p.size() ; i++){
		int j = (i+1)%p.size();
		if((p[i].y > B.y) != (p[j].y > B.y) )
		{
			double atX = (p[j].x- p[i].x)*(B.y-p[i].y)/(p[j].y-p[i].y)+p[i].x;		
			if(B.x < atX)
				crosses++;
		}
	}
	return crosses % 2 > 0;
}

void CESMAutoAdjustImageMapping::GetKeyPointsFAST(
	Mat mCurrGrayImage, 
	vector<KeyPoint>& vecCurrKeypoints,
	int nMinKeypointCount,
	int nMaxKeypointCount,
	Mat& mCurrDescript)
{
	Mat filterImage;
	mCurrGrayImage.copyTo(filterImage);
	//BilateralFilter(mCurrGrayImage, filterImage);

	mCurrDescript.release();

	int nDetectorMinValue = 1;
	int nDetectorMaxValue = 50;
	int nDetectorValue = (nDetectorMaxValue - nDetectorMinValue)/2 + nDetectorMinValue;

	cuda::GpuMat gmFilterImage;
	gmFilterImage.upload(filterImage);
	while(true)
	{
		vecCurrKeypoints.clear();

		Ptr<FastFeatureDetector> pDetector = FastFeatureDetector::create(nDetectorValue);
		pDetector->detect(filterImage, vecCurrKeypoints);

		//Ptr<cuda::FastFeatureDetector> gpDetector = cuda::FastFeatureDetector::create(nDetectorValue);
		//gpDetector ->detect(gmFilterImage, vecCurrKeypoints);
		//ESMLog(5,_T("KeyPoint Iterator : %d"), vecCurrKeypoints.size());

		for(int i = 0; i < vecCurrKeypoints.size(); i++)
		{
			Point2f ptPoint(
				vecCurrKeypoints[i].pt.x + _rtImageROI.x, 
				vecCurrKeypoints[i].pt.y + _rtImageROI.y);

			bool bIsInside = IsInside(ptPoint, _vecSquarePt);
			if(bIsInside == false)
			{
				std::swap(vecCurrKeypoints[i], vecCurrKeypoints.back());
				vecCurrKeypoints.pop_back();
				//vecCurrKeypoints.erase(vecCurrKeypoints.begin()+i);
				i--;
			}
		}

		if(abs(nDetectorMaxValue - nDetectorMinValue) <= 1)
		{
			break;
		}

		if(vecCurrKeypoints.size() < nMinKeypointCount)
		{
			nDetectorMaxValue = nDetectorValue;
			nDetectorValue = (nDetectorMaxValue - nDetectorMinValue)/2 + nDetectorMinValue;
			continue;
		}
		else if(vecCurrKeypoints.size() > nMaxKeypointCount)
		{
			nDetectorMinValue = nDetectorValue;
			nDetectorValue = (nDetectorMaxValue - nDetectorMinValue)/2 + nDetectorMinValue;
			continue;
		}
		else
		{
			break;
		}
	}
}

void CESMAutoAdjustImageMapping::Matching(vector<DMatch>& vecMatches, Mat& mPrevDescript, Mat& mCurrDescript)
{
	BFMatcher desc_matcher(cv::NORM_L2, true);
	desc_matcher.match(mPrevDescript, mCurrDescript, vecMatches, Mat());

	//desc_matcher.knnMatch(mPrevDescript, mCurrDescript, vecMatches);
	//BFMatcher desc_matcher(cv::NORM_L2, true);
	//vector< vector<DMatch> > vmatches;
	//desc_matcher.knnMatch(_mPrevDescript, mCurrDescript, vmatches, 1);
	//for (int i = 0; i < static_cast<int>(vmatches.size()); ++i) 
	//{
	//	if (!vmatches[i].size()) 
	//	{
	//		continue;
	//	}
	//	vecMatches.push_back(vmatches[i][0]);
	//}

	std::sort(vecMatches.begin(), vecMatches.end());
	while (vecMatches.front().distance * _dKDistanceCoef < vecMatches.back().distance) 
	{
		vecMatches.pop_back();
	}
	while (vecMatches.size() > _nKMaxMatchingSize) 
	{
		vecMatches.pop_back();
	}
}

Mat CESMAutoAdjustImageMapping::FindKeyPointsHomography(Mat& image, vector<DMatch>& vecMatches, vector<KeyPoint>& vecPrevKeypoints, vector<KeyPoint>& vecCurrKeypoints, vector<char>& vecSzMatchesMask)
{
	vector<Point2f> pts1;
	vector<Point2f> pts2;
	for (int i = 0; i < static_cast<int>(vecMatches.size()); ++i) 
	{
		pts1.push_back(vecPrevKeypoints[vecMatches[i].queryIdx].pt);
		pts2.push_back(vecCurrKeypoints[vecMatches[i].trainIdx].pt);
	}
	//매칭점의 최대 면적 계산
	/*{
		int nMaxX = 0;
		int nMaxY = 0;
		int nMinX = _rtImageROI.width;
		int nMinY = _rtImageROI.height;
		
		Point2f ptLeft;
		Point2f ptRight;
		Point2f ptTop;
		Point2f ptBottom;
		for(int i = 0; i < pts1.size(); i++)
		{
			if(nMinX > pts1[i].x)
			{
				nMinX = pts1[i].x;
				ptLeft = 
					Point2f(_rtImageROI.x + pts1[i].x, _rtImageROI.y + pts1[i].y);
			}
			if(nMaxX < pts1[i].x)
			{
				nMaxX = pts1[i].x;
				ptRight = 
					Point2f(_rtImageROI.x + pts1[i].x, _rtImageROI.y + pts1[i].y);
			}
			if(nMinY > pts1[i].y)
			{
				nMinY = pts1[i].y;
				ptTop = 
					Point2f(_rtImageROI.x + pts1[i].x, _rtImageROI.y + pts1[i].y);
			}
			if(nMaxY < pts1[i].y)
			{
				nMaxY = pts1[i].y;
				ptBottom= 
					Point2f(_rtImageROI.x + pts1[i].x, _rtImageROI.y + pts1[i].y);
			}
		}
		line(image, ptLeft, ptTop, Scalar(0, 0, 255), 3, 8, 0);
		line(image, ptTop, ptRight, Scalar(0, 0, 255), 3, 8, 0);
		line(image, ptRight, ptBottom, Scalar(0, 0, 255), 3, 8, 0);
		line(image, ptBottom, ptLeft, Scalar(0, 0, 255), 3, 8, 0);

		imshow("Metric Test", image);
		waitKey(0);
	}*/

	Mat mHomography = findHomography(pts1, pts2, cv::RANSAC, 4, vecSzMatchesMask);
	if(mHomography.empty())
		return Mat_<double>(3, 3) << (1,0,0,0,1,0,0,0,1);
	return mHomography;
}

void CESMAutoAdjustImageMapping::DrawKeypoints(
	Mat image,
	Mat mPrevGrayImage,
	Mat mCurrGrayImage,
	vector<KeyPoint> vecPrevKeypoints,
	vector<KeyPoint> vecCurrKeypoints,
	vector<DMatch> vecMatches, 
	vector<char> vecSzMatchesMask)
{
	Mat res;
	drawMatches(
		mPrevGrayImage, vecPrevKeypoints, 
		mCurrGrayImage, vecCurrKeypoints, 
		vecMatches, res, 
		Scalar::all(-1), Scalar::all(-1), 
		vecSzMatchesMask, 
		DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

	Mat mKeypointImage = image(_rtImageROI).clone();
	vector<KeyPoint> vecDrawKeypoint;
	for(int i = 0; i < vecDrawKeypoint.size(); i++)
	{
		vecDrawKeypoint.push_back(vecCurrKeypoints[i]);
		vecDrawKeypoint[i].pt = Point2f(_rtImageROI.x, _rtImageROI.y) + vecCurrKeypoints[i].pt;
	}
	drawKeypoints(mKeypointImage, vecCurrKeypoints, image, Scalar::all(-1), DrawMatchesFlags::DRAW_RICH_KEYPOINTS );

	circle(res, Point2f(mPrevGrayImage.cols+_vecSquarePoint[0].x, _vecSquarePoint[0].y), 5, cv::Scalar(255,0,0),2,8);
	circle(res, Point2f(mPrevGrayImage.cols+_vecSquarePoint[1].x, _vecSquarePoint[1].y), 5, cv::Scalar(255,0,0),2,8);
	circle(res, Point2f(mPrevGrayImage.cols+_vecSquarePoint[2].x, _vecSquarePoint[2].y), 5, cv::Scalar(255,0,0),2,8);
	circle(res, Point2f(mPrevGrayImage.cols+_vecSquarePoint[3].x, _vecSquarePoint[3].y), 5, cv::Scalar(255,0,0),2,8);

	circle(res, Point2f(_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-2]->vecSquarePoint[0].x, _vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-2]->vecSquarePoint[0].y), 5, cv::Scalar(255,0,0),2,8);
	circle(res, Point2f(_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-2]->vecSquarePoint[1].x, _vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-2]->vecSquarePoint[1].y), 5, cv::Scalar(255,0,0),2,8);
	circle(res, Point2f(_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-2]->vecSquarePoint[2].x, _vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-2]->vecSquarePoint[2].y), 5, cv::Scalar(255,0,0),2,8);
	circle(res, Point2f(_vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-2]->vecSquarePoint[3].x, _vecAdjustFeatureInfo[_vecAdjustFeatureInfo.size()-2]->vecSquarePoint[3].y), 5, cv::Scalar(255,0,0),2,8);

	int nResHeight = mCurrGrayImage.rows/2;
	resize(res, res, cv::Size((mPrevGrayImage.cols+mCurrGrayImage.cols)/2, nResHeight));
	imshow("Matching", res);

	waitKey(1);
}

void CESMAutoAdjustImageMapping::DrawSquarePoints(Mat& mImage)
{
	Mat mSquarePointImage;
	mImage.copyTo(mSquarePointImage);
	circle(mSquarePointImage, Point2f(_vecSquarePoint[0].x+_rtImageROI.x, _vecSquarePoint[0].y+_rtImageROI.y), 7, cv::Scalar(255,0,0), 2, 8);
	circle(mSquarePointImage, Point2f(_vecSquarePoint[1].x+_rtImageROI.x, _vecSquarePoint[1].y+_rtImageROI.y), 7, cv::Scalar(255,0,0), 2, 8);
	circle(mSquarePointImage, Point2f(_vecSquarePoint[2].x+_rtImageROI.x, _vecSquarePoint[2].y+_rtImageROI.y), 7, cv::Scalar(255,0,0), 2, 8);
	circle(mSquarePointImage, Point2f(_vecSquarePoint[3].x+_rtImageROI.x, _vecSquarePoint[3].y+_rtImageROI.y), 7, cv::Scalar(255,0,0), 2, 8);

	int nMarginX = 100;
	int nMarginY = 100;
	Mat mSquarePointImage1;
	mSquarePointImage(
		Rect2d(
		_vecSquarePoint[0].x+_rtImageROI.x - nMarginX/2, 
		_vecSquarePoint[0].y+_rtImageROI.y - nMarginY/2,
		nMarginX, nMarginY)).copyTo(mSquarePointImage1);
	resize(mSquarePointImage1, mSquarePointImage1, cv::Size(mSquarePointImage1.cols*4, mSquarePointImage1.rows*4));

	Mat mSquarePointImage2;
	mSquarePointImage(
		Rect2d(
		_vecSquarePoint[1].x+_rtImageROI.x - nMarginX/2, 
		_vecSquarePoint[1].y+_rtImageROI.y - nMarginY/2,
		nMarginX, nMarginY)).copyTo(mSquarePointImage2);
	resize(mSquarePointImage2, mSquarePointImage2, cv::Size(mSquarePointImage2.cols*4, mSquarePointImage2.rows*4));

	Mat mSquarePointImage3;
	mSquarePointImage(
		Rect2d(
		_vecSquarePoint[2].x+_rtImageROI.x - nMarginX/2, 
		_vecSquarePoint[2].y+_rtImageROI.y - nMarginY/2,
		nMarginX, nMarginY)).copyTo(mSquarePointImage3);
	resize(mSquarePointImage3, mSquarePointImage3, cv::Size(mSquarePointImage3.cols*4, mSquarePointImage3.rows*4));

	Mat mSquarePointImage4;
	mSquarePointImage(
		Rect2d(
		_vecSquarePoint[3].x+_rtImageROI.x - nMarginX/2, 
		_vecSquarePoint[3].y+_rtImageROI.y - nMarginY/2,
		nMarginX, nMarginY)).copyTo(mSquarePointImage4);
	resize(mSquarePointImage4, mSquarePointImage4, cv::Size(mSquarePointImage4.cols*4, mSquarePointImage4.rows*4));

	resize(mSquarePointImage, mSquarePointImage, cv::Size(mSquarePointImage.cols/2, mSquarePointImage.rows/2));
	imshow("SquarePoints", mSquarePointImage);
	waitKey(1);
	imshow("SquarePoints1", mSquarePointImage1);
	waitKey(1);
	imshow("SquarePoints2", mSquarePointImage2);
	waitKey(1);
	imshow("SquarePoints3", mSquarePointImage3);
	waitKey(1);
	imshow("SquarePoints4", mSquarePointImage4);
	waitKey(1);
}

void CESMAutoAdjustImageMapping::AddAdjustFeatureInfo(int nIndex, string strDscId, Mat mImage, vector<Point2f> vecSquarePoint)
{
	AdjustFeatureInfo* pAdjustFeatureInfo = new AdjustFeatureInfo;

	pAdjustFeatureInfo->nIndex = nIndex;
	pAdjustFeatureInfo->strDscId = strDscId;
	pAdjustFeatureInfo->mImage = mImage.clone();
	cvtColor(pAdjustFeatureInfo->mImage, pAdjustFeatureInfo->mGrayImage, cv::COLOR_BGR2GRAY);
	pAdjustFeatureInfo->vecSquarePoint = vecSquarePoint;

	_nWidth = mImage.cols;
	_nHeight = mImage.rows;

	_vecAdjustFeatureInfo.push_back(pAdjustFeatureInfo);
}

void CESMAutoAdjustImageMapping::ClearAdjustFeatureInfo()
{
	for(int i = 0; i < _vecAdjustFeatureInfo.size(); i++)
		delete _vecAdjustFeatureInfo[i];
	_vecAdjustFeatureInfo.clear();
}