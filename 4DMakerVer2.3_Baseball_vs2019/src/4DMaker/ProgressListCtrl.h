#pragma once

#include "ESMUtil.h"


// CListCtrlEx
#define IDC_PROGRESS_LIST WM_USER + 8888

class CProgressListCtrl : public CListCtrl
{
	DECLARE_DYNAMIC(CProgressListCtrl)

	// the array of the Progress Controls in the list control
public:
	CArray<CProgressCtrl*,CProgressCtrl*> m_ProgressList;
	// the column which should contain the progress bars
	int m_ProgressColumn;
public:
	CProgressListCtrl();
	virtual ~CProgressListCtrl();
	// initialize the column containing the bars
	void InitProgressColumn(int ColNum=0);
	void InitProgressList();

	CArray<CProgressCtrl*,CProgressCtrl*>& GetProgressList();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	
private:
	vector<int> m_nPercent;
public:
	void SetProgressPercent(int nCount, int nPercent);
};


