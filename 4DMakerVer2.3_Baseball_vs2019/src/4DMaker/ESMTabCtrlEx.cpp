// ESMTabCtrlEx.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMTabCtrlEx.h"


// CESMTabCtrlEx

IMPLEMENT_DYNAMIC(CESMTabCtrlEx, CTabCtrl)

CESMTabCtrlEx::CESMTabCtrlEx()
{
	m_brush.CreateSolidBrush(RGB(44,44,44));
}

CESMTabCtrlEx::~CESMTabCtrlEx()
{
}


BEGIN_MESSAGE_MAP(CESMTabCtrlEx, CTabCtrl)
	ON_WM_CTLCOLOR()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()



// CESMTabCtrlEx 메시지 처리기입니다.




HBRUSH CESMTabCtrlEx::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CTabCtrl::OnCtlColor(pDC, pWnd, nCtlColor);

	switch(nCtlColor)
	{
	case CTLCOLOR_STATIC:
		{

			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			//pDC->SetBkColor(RGB(56, 56, 56));  // 글자 배경색 변경
			pDC->SetBkMode(TRANSPARENT);
			return (HBRUSH)m_brush;
		}
		break;
	case CTLCOLOR_EDIT:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			pDC->SetBkColor(RGB(0, 0 ,0));
			hbr = (HBRUSH)(m_brush.GetSafeHandle());  
		}
		break;
	}    

	return m_brush;
}


BOOL CESMTabCtrlEx::OnEraseBkgnd(CDC* pDC)
{
	CRect rt;
	GetClientRect(rt);
	pDC->FillSolidRect(rt,RGB(14,14,14));
	return TRUE;

	return CTabCtrl::OnEraseBkgnd(pDC);
}
