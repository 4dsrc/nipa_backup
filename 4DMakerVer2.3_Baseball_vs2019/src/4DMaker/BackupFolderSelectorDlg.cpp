// BackupFolderSelectorDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "BackupFolderSelectorDlg.h"
#include "afxdialogex.h"
#include "ESMFunc.h"
#include "ESMIni.h"
#include "ESMRCManager.h"
#include "ESMOptManagement.h"

IMPLEMENT_DYNAMIC(CBackupFolderSelectorDlg, CDialogEx)

CBackupFolderSelectorDlg::CBackupFolderSelectorDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CBackupFolderSelectorDlg::IDD, pParent)
{
	m_pParent = (CESMOptManagement*)pParent;
	m_pBackupListCtrl = new CBackupListCtrl;	
	m_bIsAllCheck = FALSE;
}

CBackupFolderSelectorDlg::~CBackupFolderSelectorDlg()
{
}

void CBackupFolderSelectorDlg::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX, IDC_BUTTON_CHECK_ALL, m_btnCheckAll);
	DDX_Control(pDX, IDC_LIST_BACKUP_FOLDER, *m_pBackupListCtrl); 
	m_pBackupListCtrl->Init();

	GetIPList();
	LoadFolderList();

	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CBackupFolderSelectorDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_CHECK_ALL, &CBackupFolderSelectorDlg::OnBnClickedButtonCheckAll)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CBackupFolderSelectorDlg::OnBnClickedButtonSave)	
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_BACKUP_FOLDER, &CBackupFolderSelectorDlg::OnLvnColumnclickListBackupFolder)
	ON_NOTIFY(LVN_ITEMCHANGING, IDC_LIST_BACKUP_FOLDER, &CBackupFolderSelectorDlg::OnLvnItemchangingListBackupFolder)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, &CBackupFolderSelectorDlg::OnBnClickedButtonDelete)
END_MESSAGE_MAP()

void CBackupFolderSelectorDlg::LoadFolderList()
{
	CString strFolder = _T("\\RecordSave\\Record\\Movie\\files");
	CFileFind file;
	CString strFolderName;
	CString strPath;
	CString strIp;

	for(int i = 0; i < m_IPList.size(); i++)			
	{
		//strIp = _T("\\\\192.168.0.204");
		strIp = _T("\\\\") + m_IPList.at(i);
		strPath.Format(_T("%s\\%s"), strIp, strFolder);	
		BOOL b = file.FindFile(strPath + _T("\\*.*"));

		while(b)
		{
			b = file.FindNextFile();
			if(file.IsDirectory() && !file.IsDots())
			{
				strFolderName = file.GetFileName();
				if (strFolderName.IsEmpty())
					continue;

				m_pBackupListCtrl->AddFolder(strFolderName);
			}
		}
	}

	file.Close();

	// test
#if 0
	CString strFolder1 = _T("\\RecordSave\\Record\\Movie\\files_");
	CString strIp1 = _T("\\\\192.168.0.204");

	CFileFind file1;
	CString strPath1;
	strPath1.Format(_T("%s\%s"),strIp1,strFolder1);	
	BOOL b1 = file1.FindFile(strPath1 + _T("\\*.*"));
	CString strFolderName1;
	while(b1)
	{
		b1 = file1.FindNextFile();
		if(file1.IsDirectory() && !file1.IsDots())
		{
			strFolderName1 = file1.GetFileName();
			m_pBackupListCtrl->AddFolder(strFolderName1);
		}
	}
#endif
}

void CBackupFolderSelectorDlg::GetIPList()
{
	m_IPList.clear();

	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONNECT);	
	CESMIni ini;	
	if(ini.SetIniFilename (strFile))
	{
		CString strIPSection;
		CString strIP;
		int nIndex = 0;
		while(1)
		{
			strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);			
			strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);	
			if(!strIP.GetLength())
				break;		
			nIndex++;	
			m_IPList.push_back(strIP);
		}			
	}
}

void CBackupFolderSelectorDlg::SetBackupMgr(CESMBackup* pBackupMgr)
{
	m_pBackupMgr = pBackupMgr;
}

void CBackupFolderSelectorDlg::OnBnClickedButtonCheckAll()
{
	m_pBackupListCtrl->CheckAll(!m_bIsAllCheck);

	if (m_bIsAllCheck)	m_btnCheckAll.SetWindowText(_T("Check All"));
	else	m_btnCheckAll.SetWindowText(_T("Uncheck All"));

	m_bIsAllCheck = !m_bIsAllCheck;
}

void CBackupFolderSelectorDlg::OnBnClickedButtonSave()
{
	DeleteBackupFolderListFile();

	int nNum = 0;
	int nCnt = m_pBackupListCtrl->GetItemCount();
	for (int i = 0; i < nCnt ; i++)
	{
		CString strValue = m_pBackupListCtrl->GetItemText(i, 1);

		if (strValue.IsEmpty())
			continue;

		bool check = m_pBackupListCtrl->GetCheck(i);

		if (m_pBackupListCtrl->GetCheck(i) == FALSE)
			continue;

		WriteFolderList(nNum, strValue);

		nNum++;
	}

	CDialogEx::OnOK();
}

void CBackupFolderSelectorDlg::DeleteBackupFolderListFile(BOOL backup)
{
	CString strListStyle = ESM_4DMAKER_BACKUP_FOLDER_LIST;
	CString strFile;	

	// only delete
	if (backup == FALSE)	
		strListStyle = ESM_4DMAKER_DELETE_FOLDER_LIST;

	strFile.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_CONFIG), strListStyle);

	CFileFind file;
	BOOL bRet = file.FindFile(strFile);
	file.Close();

	if (bRet == FALSE)
		return;

	DeleteFile(strFile);
}

void CBackupFolderSelectorDlg::WriteFolderList(int num, CString name, BOOL backup)
{
	CString strListStyle = ESM_4DMAKER_BACKUP_FOLDER_LIST;
	CString strFile;

	// only delete
	if (backup == FALSE)	
		strListStyle = ESM_4DMAKER_DELETE_FOLDER_LIST;

	strFile.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_CONFIG), strListStyle);

	CString key;
	key.Format(_T("%d"), num);

	WritePrivateProfileString(_T("FolderList"), key, name, strFile);
}

void CBackupFolderSelectorDlg::OnLvnColumnclickListBackupFolder(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int column = pNMLV->iSubItem;

	if (column != 0)
		return;

	OnBnClickedButtonCheckAll();
}

void CBackupFolderSelectorDlg::OnLvnItemchangingListBackupFolder(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;

	if (nRow < 0)
		return;

	m_pBackupListCtrl->SetCheck(nRow, TRUE);
}

void CBackupFolderSelectorDlg::OnBnClickedButtonDelete()
{
	DeleteBackupFolderListFile(FALSE);

	int nNum = 0;
	int nCnt = m_pBackupListCtrl->GetItemCount();
	for (int i = 0; i < nCnt ; i++)
	{
		CString strValue = m_pBackupListCtrl->GetItemText(i, 1);

		if (strValue.IsEmpty())
			continue;

		bool check = m_pBackupListCtrl->GetCheck(i);

		if (m_pBackupListCtrl->GetCheck(i) == FALSE)
			continue;

		WriteFolderList(nNum, strValue, FALSE);

		nNum++;
	}

	::SendMessage(m_pParent->GetSafeHwnd(), WM_ESM_OPT_DELETE_FOLDER_LIST, 0, 0);
}
