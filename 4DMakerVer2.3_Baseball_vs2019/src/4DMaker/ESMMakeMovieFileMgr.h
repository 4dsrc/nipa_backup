#pragma once


#include "ESMFunc.h"
// CESMMakeMovieFileMgr

class CESMMakeMovieFileMgr : public CWinThread
{
	DECLARE_DYNCREATE(CESMMakeMovieFileMgr)

public:
	CESMMakeMovieFileMgr();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CESMMakeMovieFileMgr();

public:
	CESMArray	m_arMsg;
	BOOL	m_bThreadStop;
	BOOL	m_bWait;

public:
	void AddMsg(ESMEvent* pMsg);

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run(void);

protected:
	DECLARE_MESSAGE_MAP()
};


