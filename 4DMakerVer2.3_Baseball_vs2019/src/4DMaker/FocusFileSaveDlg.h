#pragma once
#include "afxwin.h"

// CFocusFileSaveDlg 대화 상자입니다.

class CFocusFileSaveDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CFocusFileSaveDlg)

private:
	CEdit m_ctrlSelectFile;
	CEdit m_ctrlCompareFolder;

public:
	CFocusFileSaveDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFocusFileSaveDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_FOCUS_FILE_SAVE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedAbort();
	afx_msg void OnBnClickedClose();
	afx_msg void OnBnClickedBtnSelectFile();
	afx_msg void OnBnClickedBtnCompareFolder();

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnCompare();
};
