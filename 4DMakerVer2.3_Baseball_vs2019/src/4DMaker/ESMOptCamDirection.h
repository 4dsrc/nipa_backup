#pragma once
#include "ESMNetworkListCtrl.h"
#include "ESMButtonEx.h"
#include "ESMEditorEX.h"

// CESMOptCamDirection 대화 상자입니다.

class CESMOptCamDirection : public CDialogEx
{
	DECLARE_DYNAMIC(CESMOptCamDirection)

public:
	CESMOptCamDirection(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMOptCamDirection();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_CAMINFO_DIRECTION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CComboBox m_cmbRecGrp;
	afx_msg void OnCbnSelendokCmbRecGroup();
	afx_msg void OnBnClickedBtnNone();
	afx_msg void OnBnClickedBtnForward();
	afx_msg void OnBnClickedBtnBackward();
	afx_msg void OnBnClickedBtnSave();
	CESMNetworkListCtrl m_lstData;

	BOOL m_bModify;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CBrush m_background;
	CESMButtonEx m_btnNone;
	CESMButtonEx m_btnForward;
	CESMButtonEx m_btnBackward;
	CString m_strDelay;
	CESMButtonEx m_btnDelayA;
	CESMButtonEx m_btnDelayB;
	afx_msg void OnBnClickedBtnDelayA();
	afx_msg void OnBnClickedBtnDelayB();
	CESMEditorEX m_editDelay;
};
