#pragma once
#include "afxcmn.h"
#include "DSCItem.h"
#include "afxcmn.h"
// CESMPWEditorDlg 대화 상자입니다.

class CESMPWEditorDlg : public CDialog
{
	DECLARE_DYNAMIC(CESMPWEditorDlg)

public:
	CESMPWEditorDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMPWEditorDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ESMPWEDITORDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	int m_nPTPCode;
	int m_nPTPCodeETC;
	int m_nValue;
	int m_nRGB;
	CDSCItem * m_pItem;

	afx_msg void OnBnClickedButtonCancle();
	

	CSliderCtrl m_ctrlSaturation;
	CSliderCtrl m_ctrlSharness;
	CSliderCtrl m_ctrlContrast;
	CSliderCtrl m_ctrlHue;
	virtual BOOL OnInitDialog();
	CStatic m_ctrlValueSaturation;
	CStatic m_ctrlValueSharpness;
	CStatic m_ctrlValueContrast;
	CStatic m_ctrlValueHue;
	
	afx_msg void OnCustomdrawSliderSaturation(NMHDR *pNMHDR, LRESULT *pResult);
	
	afx_msg void OnCustomdrawSliderSharpness(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCustomdrawSliderHue(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCustomdrawSliderContrast(NMHDR *pNMHDR, LRESULT *pResult);

	void SetRGBValue(int nValue);
	void SetDetailValue(int nValue);
	CSliderCtrl m_ctrlRed;
	CSliderCtrl m_ctrlGreen;
	CSliderCtrl m_ctrlBlue;
	CStatic m_ctrlValueRed;
	CStatic m_ctrlValueGreen;
	CStatic m_ctrlValueBlue;
	afx_msg void OnCustomdrawSliderRed(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCustomdrawSliderGreen(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCustomdrawSliderBlue(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonOk2();
	afx_msg void OnBnClickedButtonApply();
	afx_msg void OnBnClickedButtonApplyAll();
	void SetApply(CDSCItem* pItem);
	afx_msg void OnBnClickedButtonReset();
};
