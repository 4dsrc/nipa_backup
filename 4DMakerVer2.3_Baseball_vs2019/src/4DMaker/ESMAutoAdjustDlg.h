#pragma once

#include "ESMAdjustMgr.h"
#include "ESMAutoAdjustImage.h"
#include "ESMAutoAdjustMgr.h"
#include "ESMAutoAdjustSquareCalibration.h"
#include "ESMAutoAdjustImageMapping.h"
#include "ESMAutoAdjustListCtrl.h"
#include "ESMTemplateMgr.h"
#include "ESMObjectTracking.h"
#include "ESMFunc.h"

#include <vector>
#include "afxwin.h"
#include "afxres.h"
#include "afxcmn.h"

//#include "ESMNetworkListCtrl.h"
#include "ESMEditorEX.h"
#include "ESMGroupBox.h"
#include "esmbuttonex.h"

#define TEMPLATE_TEMP_FILE _T("TemplateTemp.pta")

class CESMAutoAdjustDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CESMAutoAdjustDlg)

public:
	CESMAutoAdjustDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMAutoAdjustDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = ID_IMAGE_AUTOADJUST};

private:
	enum ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE
	{
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_BASEBALL_HOME,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_BASEBALL_GROUND,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_BASKETBALL_HALF,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_BASKETBALL_GROUND,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_BOXING,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_ICE_LINK_HALF,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_ICE_LINK,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_SOCCER_HALF,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_SOCCER,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_TAEKWONDO,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_TENNIS_HALF,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_TENNIS,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_UFC,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_VOLLEYBALL_HALF,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_VOLLEYBALL_GROUND,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_FOOTBALL,
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_COUNT
	};
	CString getWorldCoordinageImageName(ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE eWorldCoordinateImage);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();

public:
	afx_msg void OnLvnItemchanged(NMHDR *pNMHDR, LRESULT *pResult);
	
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);

	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint point);

	afx_msg void OnBnClickedBtnAdjustPointReset();
	afx_msg void OnBnClickedBtn2ndSavePoint();
	afx_msg void OnBnClickedBtn2ndLoadPoint();
	afx_msg void OnBnClickedBtnModifyAdj();

	afx_msg void OnBnClickedBtnAutoMove();

	static unsigned WINAPI GetMovieDataThread(LPVOID param);

	void SetAutoAdjustMgr(CESMAutoAdjustMgr *pAutoAdjustMgr);
	void SetTemplateMgr(CESMTemplateMgr *pTemplateMgr);

	CPoint GetPointFromMouse(CPoint pt);

	void Showimage();
	void DrawImage();
	void DrawImage2();
	void DeleteImage();
	void LoadData();

	void SavePointData(CString strFileName);
	void LoadPointData(CString strFileName);
	void SavePointData(CString strFileName, int nRow);
	void SavePointDataColumn(CString strFileName);

	vector<Point2f> getSquareFromListCtrl(CListCtrl* pList, int nSelectedNumber);

	void SaveAdjust();

	void DrawCenterPointAll();
	void DrawCenterPoint(int nPosX, int nPosY, int nIndex);

	BOOL IsPointsValid(int nIndex);
	COLORREF Coloring(int index);
	//void FindExactPosition(int& nX, int& nY);

	CESMAutoAdjustListCtrl m_AdjustDscList;
	CComboBox m_ctrlCheckPoint;
	HANDLE	m_hTheadHandle;
	CEdit m_ctrlCenterLine;

	int m_nSelectedNum;
	int m_nSelectPos;
	int m_RdImageViewColor;
	int m_nView;
	int m_nRdDetectColor;
	int m_nWidth, m_nHeight;
	int m_frameIndex;
	int m_nPosX, m_nPosY;
	int m_MarginX;
	int m_MarginY;
	int m_Scale;
	int m_Move;
	int m_Number;
	BOOL m_bThreadCloseFlag;
	BOOL m_bStabilizationCloseFlag;
	BOOL m_bLoadAdjustImageAll;

	CESMAutoAdjustMgr *m_pAutoAdjustMgr;
	CESMTemplateMgr *m_pTemplateMgr;

	CESMAutoAdjustImage* m_pAdjustImg;
	CESMAutoAdjustImage m_StAdjustImage;
	CESMAutoAdjustImage m_StAdjustWorldCoordinateImage;

	CESMAutoAdjustImage* GetStAdjustImageControl(CPoint point);

	CESMObjectTracking *m_pObjectTracking;
	BOOL m_bInitializedTracking;
	BOOL m_bAutoPointMove;

	vector<stAdjustInfo> m_pAdjInfo;

public:
	void setDscListItem(CListCtrl* pList, int rowIndex, int colIndex, int data);	
	int getDSCListItem(CListCtrl* pList, int rowIndex, int colIndex);

	CPoint m_pDownPoint;
	CPoint m_pUpPoint;

	void CalcAdjustData();
	void ModifyAdjustData();
	void getAdjustMoveCoordination();
	void CalcSelectedDistance();

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	void DeleteSelectedList();
	void DeleteSelectItem(int nIndex);

	vector<Point2f> m_pMovePoint;
	int m_dFrameTime;

	//171018
	int m_nStartSelectedItem;
	int m_nEndSelectedItem;

public:
	CBrush m_background;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

//World Coordinate
public:
	CComboBox m_cbSetWCImage;
	afx_msg void OnCbnSelchangeCbnSetWCImage();

	CESMAutoAdjustSquareCalibration *m_pSquareCalibration;

	string m_strWorldCoordinationImagePath;
	void ShowWorldCoordnationImage();
	void SetStrWorldCoordinationImagePath(string strWorldCoordinationImagePath);

	CPoint m_pCenterPoint;
	afx_msg void OnBnClickedButtonDistanceRegister();
	afx_msg void OnBnClickedButtonDistanceUnregister();
	afx_msg void OnBnClickedButtonAdjustCalc();

	void SetMargin(int& nMarginX, int& nMarginY, stAdjustInfo AdjustData );
	afx_msg void OnBnClickedButtonDistanceRegisterAll();
	afx_msg void OnBnClickedButtonDistanceRegisterAll2();
	afx_msg void OnBnClickedButtonAdjustSave();

	afx_msg void OnBnClickedButtonAutoadjustPlus();
	afx_msg void OnBnClickedButtonAutoadjustMinus();
	
	afx_msg void OnBnClickedButtonAutoadjustAutoFindUpStart();
	afx_msg void OnBnClickedButtonAutoadjustAutoFindDownStart();
	afx_msg void OnBnClickedButtonAutoadjustAutoFindPause();

	CESMTemplateAsAuto m_pTemplateAsAuto;
	BOOL m_bAutoMove;

	//180917
	void InputAutoAdjustDataFromCtrlList();
	void SetPositionTrackingData();

public:
	afx_msg void OnBnClickedCheckAutoadjustAutomove();
	afx_msg void OnBnClickedCheckAutoadjustAutofind();

	BOOL m_bAutoFindPause;
	vector<int> m_vecIsImageLoad;

	void SetAutoTrackingRoiFromSquarePoint(
		CESMAutoAdjustImageMapping *pAutoAdjustImageMapping,
		vector<Point2f> vecPt);

	CESMButtonEx m_btnLoad;
	CESMButtonEx m_btnSave;
	CESMButtonEx m_btnReset;
	CESMButtonEx m_btnDown;
	CESMButtonEx m_btnUp;
	CESMButtonEx m_btnPause;
	CESMButtonEx m_btnPlus;
	CESMButtonEx m_btnMinus;
	CESMButtonEx m_btnAdjustPreview;
	CESMButtonEx m_btnRegister;
	CESMButtonEx m_btnUnregister;
	CESMButtonEx m_btnRegisterAll;
	CESMButtonEx m_btnUnregisterAll;
	CESMButtonEx m_btnCalcAdjust;
	CESMButtonEx m_btnSaveAdjust;
	CESMButtonEx m_btnOk;
	CESMButtonEx m_btnCancel;
};
