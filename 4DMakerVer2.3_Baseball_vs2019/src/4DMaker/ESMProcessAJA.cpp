#include"stdafx.h"
#include "ESMProcessAJA.h"
#include "ESMFileOperation.h"
#include "ESMIni.h"

#define REVISION
//#define TEST
enum{
	AJA_DUMMY,
	AJA_PAUSE,
	AJA_PLAY,
	AJA_STOP
};
CESMProcessAJA::CESMProcessAJA()
{
	m_bMaking = FALSE;
	m_bSelPlay = FALSE;
	m_aStart = AJA_STATUS_FAIL;
	m_pArrDisplayData = NULL;
	m_bThread = TRUE;
	m_strSelectFile = _T("");
	m_nSignal = 0;
	PCFreq = 0.0;
	CounterStart = 0;
	//m_strBackgroundPath = _T("C:\\Program Files\\ESMLab\\4DMaker\\img\\4DBack.png");
	sprintf(m_strBackgroundPath,"C:\\Program Files\\ESMLab\\4DMaker\\img\\4DBack.png");
	InitializeCriticalSection(&m_Cri);
	bDoingProcessing = FALSE;
	m_bReplayState = FALSE;
}
CESMProcessAJA::~CESMProcessAJA()
{
	TRACE(_T("AJA Destructor"));
	if(bDoingProcessing)
	{

		SetMovieSignal(AJA_STOP);
		//Sleep(1000);
		/*SetThreadSignal(FALSE);*/

		//SetMovieStatus(
		while(1)
		{
			Sleep(10);
			SetMovieSignal(AJA_STOP);

			if(!bDoingProcessing)
				break;
		}
	}
	m_Process->EmitPattern(m_mYUVBlack,33);
	DeleteCriticalSection(&m_Cri);
	m_Process->m_bFinish = FALSE;
	Sleep(100);
	delete m_Process;
}

LRESULT CESMProcessAJA::ManageMsg(ESMEvent* pMsg)
{
	if(!pMsg)
		return ESM_ERR_MSG;

	switch(pMsg->message)
	{
	case WM_ESM_AJA_THREADINIT:
		{
			//pParam   : Time 'Key' value
			//nParam1 : Total Making Count
			CString* strTime = (CString*)pMsg->pParam;
			int nTotalMakingCnt = pMsg->nParam1;
			map_AJAData[*strTime].nTotalCnt = nTotalMakingCnt;

			//Memory 할당
			if(m_pArrDisplayData == NULL)
			{
				//Assign
				m_pArrDisplayData = new vector<DisplayMovieData>(nTotalMakingCnt);
			}
			EnterCriticalSection(&m_Cri);
			BOOL bThread = GetThreadSignal();
			LeaveCriticalSection(&m_Cri);

			if(bThread)
			{
				EnterCriticalSection(&m_Cri);
				//m_bThread = FALSE;//다른 영상 생성 막아줍니다.
				SetThreadSignal(FALSE);
				LeaveCriticalSection(&m_Cri);

				SetMovieSignal(AJA_STOP);
				_DoWaitThread();
			}
		}
		break;
	case WM_ESM_PROCESSOR_ADDFILE:
		{
			//pParam  : Movie Path 
			//nParam1: Index  0
			//nParam2: Total Movie Count 0
			//pDest     : Time Info.

			int nIndex = pMsg->nParam1;
			CString *pstrTime = (CString*)  pMsg->pDest;
			CString str,strTime;
			strTime.Format(_T("%s"),*pstrTime);
			str.Format(_T("%s\\%s\\%d.mp4"),ESMGetClientRamPath(),strTime,nIndex);
			
			map_AJAData[strTime].nAddingCnt++;
			
			if(GetThreadSignal())//쓰레드가 실행 중일 때, 대기
			{
				ESMLog(5,_T("[AJA] Erase : %s,%d"),strTime,nIndex);
				map_AJAData.erase(strTime);
			}

			if(strTime == m_strCurTime && !GetThreadSignal())
			{
				//map_AJAData[strTime].nAddingCnt++;

				AddString(str,strTime,nIndex);
				m_bMaking = TRUE;
			}
		}
		break;
	case WM_ESM_AJA_SELECT_PLAY:
		{
			CString *pStrPath = (CString*) pMsg->pParam;
			CString strPath = *pStrPath;

			//쓰레드로 재생, 다른 파일 입력 시 대기 시간 100ms
			SetMovieSignal(AJA_STOP);
			Sleep(100);

			while(1)
			{
				if(!m_bSelPlay)
				{
					m_bSelPlay = TRUE;
					break;
				}
			}

			SelectFilePlay(strPath);
		}
		break;
	case WM_ESM_AJA_PLAY_OPT:
		{
			//1 Pause
			//2 Play
			//3 Stop
			int nOpt = pMsg->nParam1;
			SetMovieSignal(nOpt);
		}
		break;
	case WM_ESM_AJA_RELOAD:
		{
			if(AJA_FAILURE(m_aStart))
			{
				delete m_Process;
				AJAStart();
			}
			else
			{
				ESMLog(5,_T("Already Load AJA Board"));
				break;
			}
		}
		break;
	default:
		break;
	}
}
void CESMProcessAJA::_DoWaitThread()
{
	SetMovieSignal(AJA_PLAY);
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,WaitPath,(void*)this,0,NULL);
	BOOL bCheck = SetThreadPriority(hSyncTime, THREAD_PRIORITY_HIGHEST);
	if(!bCheck)
		ESMLog(5,_T("Not set Priority [DoWaitThread]"));
	CloseHandle(hSyncTime);
}
void CESMProcessAJA::_DoDecodingThread(vector<DisplayMovieData>*pArrDisplayData,int nIndex)
{
	HANDLE hSyncTime = NULL;
	ThreadDisplayAJA* pThreadData = new ThreadDisplayAJA;
	pThreadData->pParent = this;
	pThreadData->pArrDisplayData = this->m_pArrDisplayData;
	pThreadData->nIndex = nIndex;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,DecodingFrame,(void*)pThreadData,0,NULL);
	CloseHandle(hSyncTime);
}
int CESMProcessAJA::DecodingMovie(vector<DisplayMovieData>*pArrDisplayData,int nIndex)
{
	DisplayMovieData *pDisplayData =&(pArrDisplayData->at(nIndex));

	CString str = pDisplayData->strPath;

	CT2CA pszConvertedAnsiString (str);
	string strDst(pszConvertedAnsiString);

	cv::VideoCapture vc(strDst);
	if(!vc.isOpened())
	{
		//ESMLog(5,_T("Not File Exist[%s]"),str);
		return -100;
	}
	cv::Mat frame;
	cv::Mat Logo;

	while(1)
	{
		vc>>frame;
		if(frame.empty()) break;
		//cv::putText(frame,strMovie,cv::Point(50,100),cv::FONT_HERSHEY_PLAIN,5,cv::Scalar(255,255,255),3,8);
		int nWidth = frame.cols;
		int nHeight = frame.rows;

		if(ESMGetValue(ESM_VALUE_AJALOGO))
		{
			InsertLogo(&frame);
		}		
#ifndef TEST
		cv::Mat YUV422(nHeight,nWidth*2,CV_8UC1);
		BGR2YUV422(frame,YUV422,nHeight,nWidth);

		pDisplayData->pPlayData.push_back(YUV422);
#else
		pDisplayData->pPlayData.push_back(frame);
#endif
		//nTotalFrameCnt++;
	}
	return TRUE;
}
int CESMProcessAJA::BGR2YUV422(cv::Mat BGR,cv::Mat YUV,int nHeight,int nWidth)
{
	int i,j;
	int B,G,R;
	int Y,U,V;
	int rIdx,cIdx,yIdx,xIdx;

	int start = GetTickCount();
	for(i=0;i<nHeight;i++)
	{
		rIdx = nWidth*(i*3);
		yIdx = (nWidth*2)*i;
		for(j=0;j<nWidth;j++)
		{
			cIdx = rIdx + (j*3);
			xIdx = yIdx +j*2;

			B = BGR.data[cIdx];
			G = BGR.data[cIdx+1];
			R = BGR.data[cIdx+2];

			Y = (0.257*R) + (0.504*G) + (0.098*B) +16;  CuttingValue(Y);
			U = -(0.148*R) - (0.291*G) + (0.439*B) + 128; CuttingValue(U);
			V = (0.439*R ) - (0.368*G) - (0.071*B) + 128; CuttingValue(V);

			if(j%2 == 0)
			{
				YUV.data[xIdx] = U;
				YUV.data[xIdx+1] = Y;
			}
			else
			{
				YUV.data[xIdx] = V;
				YUV.data[xIdx+1] = Y;
			}
		}
	}

	int end = GetTickCount();

	//return Image Processing Time
	return (end-start);
}
int CESMProcessAJA::CuttingValue(int nValue)
{
	if(nValue<0)
		nValue = 0;

	if(nValue>255)
		nValue = 255;

	return nValue;
}
int CESMProcessAJA::InsertLogo(cv::Mat *Frame)
{
	CESMIni ini;
	CString strInfoConfig;
	strInfoConfig.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\config\\4DMaker.info"));
	ini.SetIniFilename(strInfoConfig);
	CString strImagePath = ini.GetString(_T("Path"), _T("FileServer"));

	CString strPath;
	strPath.Format(_T("%s\\img\\4DCAM.png"),strImagePath);

	char pBannerPath[MAX_PATH] = {0,};
	wcstombs(pBannerPath,strPath,MAX_PATH);

	if(::PathFileExists(strPath) == FALSE)
		return -1;

	cv::Mat Logo = cv::imread(pBannerPath);
	cv::Mat Output(Logo.rows,Logo.cols,CV_8UC3);

	OverLayImage(Frame,Logo,Output);

	*Frame = Output.clone();

	return TRUE;
}
void CESMProcessAJA::OverLayImage(cv::Mat *Frame,cv::Mat Logo,cv::Mat Output)
{
	int i,j;
	int nWidth = Frame->cols;
	int nHeight = Frame->rows;
	int nX,nY;

	for(i = 0 ; i < nHeight ; i++)
	{
		nY = nWidth*(i*3);
		for(j = 0 ; j < nWidth ; j++)
		{
			nX = nY + (j*3);

			if(Logo.data[nX] == 0 && Logo.data[nX+1] == 0 && Logo.data[nX+2] == 0)
			{
				Output.data[nX]   = Frame->data[nX];
				Output.data[nX+1] = Frame->data[nX+1];
				Output.data[nX+2] = Frame->data[nX+2];
			}
			else
			{
				Output.data[nX]   = Logo.data[nX];
				Output.data[nX+1] = Logo.data[nX+1];
				Output.data[nX+2] = Logo.data[nX+2];
			}
		}
	}
}
void CESMProcessAJA::AddString(CString str,CString strTime,int nIndex)
{
	m_pArrDisplayData->at(nIndex).strPath = str;
	m_pArrDisplayData->at(nIndex).nFinish  = FALSE;
	m_pArrDisplayData->at(nIndex).strTime = strTime;

	//Threading
	_DoDecodingThread(m_pArrDisplayData,nIndex);

}
BOOL CESMProcessAJA::AJAStart()
{
	if(AJA_FAILURE(m_aStart))
	{
		m_aStart = AJAProcess();

		if(AJA_SUCCESS(m_aStart))
			m_aStart = AJA_STATUS_SUCCESS;

		if(AJA_SUCCESS(m_aStart))
		{
			ESMLog(5,_T("Load AJA Success"));
			cv::Mat img = cv::imread(m_strBackgroundPath);
			m_mYUV.create(img.rows,img.cols*2,CV_8UC1);
			m_mBGR.create(img.rows,img.cols,CV_8UC3);
			m_mBGR = cv::Scalar(0);
			m_mYUVBlack.create(img.rows,img.cols*2,CV_8UC1);
			BGR2YUV422(m_mBGR,m_mYUVBlack,img.rows,img.cols);

			BGR2YUV422(img,m_mYUV,img.rows,img.cols);
			m_Process->EmitPattern(m_mYUV,33);
		}
		else
		{
			ESMLog(0,_T("Load AJA Fail"));
			if(!ESMGetValue(ESM_MODESTATE_CLIENT))
			{
				ESMSetValue(ESM_VALUE_AJAONOFF,FALSE);
			}
		}
	}
	if(AJA_FAILURE(m_aStart))
	{
		ESMEvent* pAJA = new ESMEvent;
		pAJA->message = WM_ESM_MOVIELIST_LOADINFO;
		pAJA->nParam1 = FALSE;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pAJA);

		return FALSE;
	}
	else
	{
		ESMEvent* pAJA = new ESMEvent;
		pAJA->message = WM_ESM_MOVIELIST_LOADINFO;
		pAJA->nParam1 = TRUE;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pAJA);

		return TRUE;
	}
}
AJAStatus CESMProcessAJA::AJAProcess()
{
	AJAStatus	status				(AJA_STATUS_SUCCESS);
	uint32_t	deviceIndex			(0);
	uint32_t	channelNumber		(1);
	uint32_t	testPatternIndex	(0);					//	Which test pattern to display
	poptContext	optionsContext;								//	Context for parsing command line arguments


	const NTV2Channel	channel	(static_cast <NTV2Channel> (channelNumber - 1));
	if (!IS_VALID_NTV2Channel (channel))
	{
		cerr << "## ERROR:  Invalid channel number " << channelNumber << " -- expected 1 thru 8" << endl;
		AfxMessageBox(_T("AAAAAAAAAAAAAA"));
	}
	//NTV2OutputTestPattern outputTestPattern(deviceIndex,channel);
	//m_Process(deviceIndex,channel);

	//m_Process.Init();
	//m_Process.SetOperation(nTotalCnt);

	m_Process = new NTV2OutputTestPattern(deviceIndex,channel);
	if(AJA_SUCCESS(status))
		status = m_Process->Init();

	if(status == AJA_STATUS_BUSY)
	{
		//m_Process->
	}

	if(AJA_SUCCESS(status))
		status = m_Process->SetOperation();

	return status;
}
unsigned WINAPI CESMProcessAJA::WaitPath(LPVOID param)
{
	CESMProcessAJA* pPath = (CESMProcessAJA*)param;
	//map<CString,AJAFinish>::iterator mitFind = NULL;
	map<CString,AJAFinish>::iterator iter;

	int nTotalMakingCnt = 0;
	CString strCurPlayTime = _T("");
	CString strTmp = _T("");
	int nStartIndex = 0, nEndIndex = 0;
	int nAJAFinishCnt = 0;
	AJAStatus status(AJA_STATUS_FAIL);
	CString *pstrTime = new CString;

	pPath->bDoingProcessing = TRUE;
	//cv::namedWindow("FRAME",CV_WINDOW_FREERATIO);
	while(1)
	{
		iter = pPath->map_AJAData.begin(); 
		strCurPlayTime = iter->first;
		nTotalMakingCnt = iter->second.nTotalCnt;

		if(strCurPlayTime == strTmp)
		{
			pPath->map_AJAData.erase(pPath->map_AJAData.find(strTmp));
			strCurPlayTime = iter->first;
			nTotalMakingCnt = iter->second.nTotalCnt;
		}
		pPath->m_strCurTime = strCurPlayTime;
		if(AJA_FAILURE(pPath->m_aStart))
		{
			ESMLog(1,_T("AJA Load Error"));
		}
		//Play - 메이킹시
#ifndef TEST
		if(AJA_SUCCESS(pPath->m_aStart))
#endif
		if(!pPath->GetThreadSignal())
		{
			for(int i = 0 ; i <nTotalMakingCnt; i++)
			{
				DisplayMovieData *pData = &(pPath->m_pArrDisplayData->at(i));
				//status = pPath->m_Process->EmitPattern(pPlayData->pArrDisplayData,nStartIndex,nEndIndex);
				int nCnt = 0;
				int nWaitTime;
				if(i==0) nWaitTime = 16000;
				else nWaitTime = 8000;
				while(1)
				{		
					Sleep(20);

					if(++nCnt * 20 > nWaitTime)//16초 이상 영상이 들어오지 않으면 break..
					{
						ESMLog(5,_T("[AJA]Skip : %d"),i);
						pData->nFinish = -100;
						break;
					}

					//true
					if(pData->nFinish == 1)
						break;

					//Fail
					if(pData->nFinish == -100)
						break;

				}

				if(pData->nFinish == -100)
				{
					nAJAFinishCnt++;
					continue;
				}
#ifndef TEST
				nEndIndex = i;

				if(nEndIndex == nTotalMakingCnt-1) nEndIndex++;

				if(nTotalMakingCnt == 1)
					i = 1;
				if( i>=nTotalMakingCnt/3 &&AJA_SUCCESS(pPath->m_aStart))
				{
					//기존 리플레이 데이터가 존재할 경우 종료 대기 후 다음 파일 재생
					if(pPath->GetReplayStatus())
					{
						pPath->SetReplayStatus(FALSE);
						while(1)
						{
							if(!pPath->GetReplayStatus())
								break;

							Sleep(10);
						}
					}
					if(nEndIndex - nStartIndex > 1)
						ESMLog(5,_T("AJA Play Start.."));					status = pPath->m_Process->EmitPattern(pPath->m_pArrDisplayData,nStartIndex,nEndIndex);
					nStartIndex = i;
				}
#else
				int nSize = pPath->m_pArrDisplayData->at(i).pPlayData.size();
				for(int j = 0 ; j < nSize; j++)
				{
					cv::imshow("FRAME",pPath->m_pArrDisplayData->at(i).pPlayData.at(j));
					cv::waitKey(1);
				}
#endif
				
				//재생 옵션
				if(!pPath->GetMovieStatus())
				{
					ESMLog(5,_T("AJA Play Stop!"));
					break;
				}
				//if(pPath->GetMovieSignal() == AJA_STOP)//Stop or 선택파일 재생
				//{
				//	pPath->SetThreadSignal(TRUE);// = TRUE;
				//	pPath->m_bSelPlay = FALSE;
				//	break;
				//}

				//if(pPath->GetMovieSignal() == AJA_PAUSE)//Pause
				//{
				//	while(1)
				//	{
				//		Sleep(10);

				//		if(pPath->GetMovieSignal()==AJA_PLAY)//Play
				//			break;

				//		if(pPath->GetMovieSignal()==AJA_STOP)
				//			break;
				//	}
				//}

				//if(pPath->GetMovieSignal() == AJA_STOP)//Stop or 선택파일 재생
				//{
				//	pPath->SetThreadSignal(TRUE);// = TRUE;
				//	pPath->m_bSelPlay = FALSE;
				//	break;
				//}

			}
		}

		int nCnt = 0;
		//int nTotalMakingCnt;
		DisplayMovieData *pData = NULL;
		
		while(1)//Merge 전 최종 확인 과정
		{
			Sleep(10);
			if(nCnt < pPath->m_pArrDisplayData->size())
				pData = &(pPath->m_pArrDisplayData->at(nCnt++));
			else
				break;

			if(pData)
			{
				if(pData->nFinish == -100)
					continue;
				else
				{
					//nTotalMakingCnt = pData->
					break;
				}
			}
		}

		//재생한 파일 Merge
		ESMEvent* pAJA = new ESMEvent;
		pstrTime->Format(_T("%s"),strCurPlayTime);
		pAJA->message = WM_ESM_AJA_FINISH;
		pAJA->nParam1 = pPath->map_AJAData[*pstrTime].nTotalCnt;
		pAJA->pParam   = (LPARAM) pstrTime;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pAJA);

		pPath->map_AJAData.erase(strCurPlayTime);


#ifdef REVISION
		//vector<DisplayMovieData> *pArrReplay = NULL;
		int nSizing = pPath->m_pArrDisplayData->size();
		if(ESMGetValue(ESM_VALUE_AJAREPLAY))
		{
			//pArrReplay = new vector<DisplayMovieData>(nSizing);

			for(int i = 0 ; i < nSizing; i++)
			{
				//memcpy(&pArrReplay[i],&pPath->m_pArrDisplayData[i],sizeof(DisplayMovieData)*nSizing);
				int nIndexSize = pPath->m_pArrDisplayData->at(i).pPlayData.size();
				for(int j = 0 ; j < nIndexSize; j++)
				{
					cv::Mat mFrameData = pPath->m_pArrDisplayData->at(i).pPlayData.at(j).clone();
					pPath->m_ArrReplayData.push_back(mFrameData);
					//pArrReplay->at(i).pPlayData.push_back(mFrameData);
					//memcpy(&pArrReplay->at(j).pPlayData,&pPath->m_pArrDisplayData->at(j).pPlayData,sizeof(cv::Mat)*pArrReplay->size());
				}
			}
		}		delete pPath->m_pArrDisplayData;
		pPath->m_pArrDisplayData = NULL;
#endif
		pPath->m_strCurTime = strTmp;

		//두번 메이킹 or 선택파일재생이 되었을 때 남아있는 파일들 Merge로 전송
		if(!pPath->map_AJAData.empty())
		{
			//for(iter = pPath->map_AJAData.begin();iter != pPath->map_AJAData.end();iter++)
			pPath->map_AJAData.erase(strTmp);//[strTmp]

			while(1)
			{
				Sleep(1500);

				iter = pPath->map_AJAData.begin(); 

				if(pPath->map_AJAData.empty())
					break;

				CString strTime = iter->first;
				if(pPath->map_AJAData[strTime].nAddingCnt == pPath->map_AJAData[strTime].nTotalCnt)
				{
					ESMEvent* pAJA = new ESMEvent;
					pstrTime->Format(_T("%s"),strTime);
					pAJA->message = WM_ESM_AJA_FINISH;
					pAJA->nParam1 = pPath->map_AJAData[strTime].nTotalCnt;
					pAJA->pParam   = (LPARAM) pstrTime;
					::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pAJA);
				}
				else
					continue;

				pPath->map_AJAData.erase(strTime);

			}
			//int nSize = pPath->map_AJAData.size();
			//for(int i = 0 ; i < nSize;i++)
			//{
			//	Sleep(1500);
			//	iter = pPath->map_AJAData.begin(); 

			//	CString strTime = iter->first;
			//	ESMEvent* pAJA = new ESMEvent;
			//	pstrTime->Format(_T("%s"),strTime);
			//	pAJA->message = WM_ESM_AJA_FINISH;
			//	pAJA->nParam1 = pPath->map_AJAData[strTime].nTotalCnt;
			//	pAJA->pParam   = (LPARAM) pstrTime;
			//	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pAJA);

			//	pPath->map_AJAData.erase(strTime);
			//}
		}

		pPath->SetThreadSignal(TRUE);// = TRUE;
		pPath->m_bMaking = FALSE;	

		if(ESMGetValue(ESM_VALUE_AJAREPLAY))
		{
			pPath->SetReplayStatus(TRUE);
			pPath->DoReplay();
			//ESMLog(5,_T("ReplayStop"));
		}
		else
		{
			if(pPath->m_ArrReplayData.size() > 0)
				pPath->m_ArrReplayData.clear();
		}		break;
	}
#ifndef REVISION
	cv::destroyAllWindows();
	if(AJA_SUCCESS(pPath->m_aStart))
		pPath->m_Process->EmitPattern(pPath->m_mYUV,0);


#endif	
	pPath->bDoingProcessing = FALSE;
	ESMLog(5,_T("Sequential Play Finish"));
	//delete pPath->m_Process;
	//pPath->m_Process = NULL;

	//while(1)
	//{
	//	Sleep(10);

	//	if(pPath->map_AJAData.empty())	//쓰레드 종료
	//		break;

	//	if(strTmp == strCurPlayTime && pPath->map_AJAData[strTmp].bFinish == FALSE)
	//	{
	//		pPath->map_AJAData.erase(pPath->map_AJAData.find(strTmp));
	//	}

	//	if(!pPath->map_AJAData[strCurPlayTime].bFinish)
	//	{
	//		if(strCurPlayTime == strTmp)
	//			pPath->map_AJAData.erase(pPath->map_AJAData.find(strCurPlayTime));

	//		iter = pPath->map_AJAData.begin();
	//		strCurPlayTime = (CString) iter->first;
	//		int nIndex = iter->second.nIndex;
	//		int nTotalMakingCnt = iter->second.nTotalCnt;
	//	}

	//	//Add된 파일 Decoding...!

	//}

	return TRUE;
}
unsigned WINAPI CESMProcessAJA::DecodingFrame(LPVOID param)
{
	ThreadDisplayAJA* pThreadData = (ThreadDisplayAJA*) param;
	CESMProcessAJA* pParent = pThreadData->pParent;
	vector<DisplayMovieData>*pArrDisplayData = pThreadData->pArrDisplayData;
	int nIndex = pThreadData->nIndex;
	int status = pParent->DecodingMovie(pArrDisplayData,nIndex);
	if(status == -100)
	{
		//ESMLog(5,_T("[%d] AJA Load Decoding Frame Fail(%d)"),nIndex,status);
	}
	pArrDisplayData->at(nIndex).nFinish = status;
	return 0;
}
void CESMProcessAJA::SelectFilePlay(CString strPath)
{
	m_strSelectFile = strPath;
	m_bSelPlay = TRUE;
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,SelectFilePlay,(void*)this,0,NULL);
	BOOL bCheck = SetThreadPriority(hSyncTime, THREAD_PRIORITY_HIGHEST);
	if(!bCheck)
		ESMLog(5,_T("Not set Priority [SelectFilePlay]"));
	CloseHandle(hSyncTime);	
}
unsigned WINAPI CESMProcessAJA::SelectFilePlay(LPVOID param)
{
	//CString *pstrPath = (CString*)param->
	CESMProcessAJA* pSel = (CESMProcessAJA*)param;

	CT2CA pszConvertedAnsiString(pSel->m_strSelectFile);
	std::string strLoadPath(pszConvertedAnsiString);

	cv::VideoCapture vc(strLoadPath);
	if(!vc.isOpened())
	{
		ESMLog(5,_T("Select File Play Fail"));
		pSel->m_bSelPlay = FALSE;
		return -1;
	}
	int nWidth = vc.get(cv::CAP_PROP_FRAME_WIDTH);
	int nHeight = vc.get(cv::CAP_PROP_FRAME_HEIGHT);

	cv::Mat frame;
	cv::Mat mYUV(nHeight,nWidth*2,CV_8UC1);

#ifdef TEST
	cv::namedWindow(strLoadPath,CV_WINDOW_FREERATIO);
#endif
	//if(AJA_FAILURE(pSel->m_aStart))
	//{
	//	ESMLog(5,_T("Load AJA Device"));
	//	return -1;
	//}

	pSel->SetMovieSignal(AJA_PLAY);
	pSel->bDoingProcessing = TRUE;
	vector<cv::Mat> pArrMovieData;
	
	while(1)
	{
		//Decoding
		pSel->StartCounter();
		vc>>frame;
		if(frame.empty())
		{
			pSel->m_bSelPlay = FALSE;
			cv::destroyAllWindows();
			break;
		}

		if(ESMGetValue(ESM_VALUE_AJALOGO))
			pSel->InsertLogo(&frame);

		//Upload
		pSel->BGR2YUV422(frame,mYUV,nHeight,nWidth);
		cv::Mat tmpYUV = mYUV.clone();

		pArrMovieData.push_back(tmpYUV);

#ifdef TEST
		//int nSampleCnt = ESMGetValue(ESM_VALUE_AJASAMPLECNT);
		while(1)
		{
			if(pSel->GetCounter() > ESMGetValue(ESM_VALUE_AJASAMPLECNT))
				break;
		}
		cv::imshow(strLoadPath,frame);
		if(cv::waitKey(1)==27)
		{
			pSel->m_bSelPlay = FALSE;
			break;
		}
#else
		pSel->m_Process->EmitPattern(mYUV,pSel->GetCounter());
#endif
		//재생 옵션
		if(!pSel->GetMovieStatus())
		{
			ESMLog(5,_T("AJA Play Stop!"));
			break;
		}
	}

	if(ESMGetValue(ESM_VALUE_AJAREPLAY))
	{
		pSel->DoSelReplay(pArrMovieData);
		ESMLog(5,_T("AJA Play Stop!"));
	}
	pArrMovieData.clear();
	cv::destroyAllWindows();
	pSel->m_bSelPlay = FALSE;
	ESMLog(5,_T("Select File Finish"));
#ifndef REVISION
	if(AJA_SUCCESS(pSel->m_aStart))
		pSel->m_Process->EmitPattern(pSel->m_mYUV,33);
#endif
	pSel->bDoingProcessing = FALSE;

	return TRUE;
}

void CESMProcessAJA::StartCounter()
{
	PCFreq = 0;
	CounterStart = 0;
	LARGE_INTEGER li;
	if(!QueryPerformanceFrequency(&li))
		cout << "QueryPerformanceFrequency failed!\n";

	PCFreq = double(li.QuadPart)/1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}
double CESMProcessAJA::GetCounter()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart-CounterStart)/PCFreq;
}
void CESMProcessAJA::DoReplay()
{
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,ReplayThread,(void*)this,0,NULL);
	BOOL bCheck = SetThreadPriority(hSyncTime, THREAD_PRIORITY_HIGHEST);
	if(!bCheck)
		ESMLog(5,_T("Not set Priority [SelectFilePlay]"));
	CloseHandle(hSyncTime);
}
BOOL CESMProcessAJA::GetMovieStatus()
{
	//BOOL bSignal = TRUE;
	if(GetMovieSignal() == AJA_STOP)//Stop or 선택파일 재생
	{
		SetThreadSignal(TRUE);// = TRUE;
		m_bSelPlay = FALSE;
		return FALSE;
	}

	if(GetMovieSignal() == AJA_PAUSE)//Pause
	{
		ESMLog(5,_T("AJA Play Pause!"));

		while(1)
		{
			Sleep(10);

			if(GetMovieSignal()==AJA_PLAY)//Play
			{
				ESMLog(5,_T("AJA Play Continue!"));
				break;
			}

			if(GetMovieSignal()==AJA_STOP)
				break;
		}
	}

	if(GetMovieSignal() == AJA_STOP)//Stop or 선택파일 재생
	{
		SetThreadSignal(TRUE);// = TRUE;
		m_bSelPlay = FALSE;
		return FALSE;
	}

	return TRUE;
}
void CESMProcessAJA::DoSelReplay(vector<cv::Mat>ArrMovieData)
{
	while(1)
	{
		EnterCriticalSection(&m_Cri);
		if(!m_Process->m_bFinish || !GetMovieStatus()|| !GetThreadSignal() || !ESMGetValue(ESM_VALUE_AJAREPLAY))
		{
			LeaveCriticalSection(&m_Cri);
			ESMLog(5,_T("#########Kill Thread\n"));
			bDoingProcessing = FALSE;
			break;
		}
		LeaveCriticalSection(&m_Cri);

		for(int i = 0 ; i < ArrMovieData.size(); i++)
		{
			StartCounter();
			cv::Mat mYUV = ArrMovieData.at(i).clone();
			m_Process->EmitPattern(mYUV,GetCounter());

			EnterCriticalSection(&m_Cri);
			if(!m_Process->m_bFinish || !GetMovieStatus()|| !GetThreadSignal() || !ESMGetValue(ESM_VALUE_AJAREPLAY))
			{
				LeaveCriticalSection(&m_Cri);
				ESMLog(5,_T("#########Kill Thread\n"));
				bDoingProcessing = FALSE;
				break;
			}
			LeaveCriticalSection(&m_Cri);

		}

		EnterCriticalSection(&m_Cri);
		if(!m_Process->m_bFinish || !GetMovieStatus()|| !GetThreadSignal() || !ESMGetValue(ESM_VALUE_AJAREPLAY))
		{
			LeaveCriticalSection(&m_Cri);
			ESMLog(5,_T("#########Kill Thread\n"));
			bDoingProcessing = FALSE;
			break;
		}
		LeaveCriticalSection(&m_Cri);
	}
}
unsigned WINAPI CESMProcessAJA::ReplayThread(LPVOID param)
{
	CESMProcessAJA* pSel = (CESMProcessAJA*)param;
	vector<cv::Mat>mArrReplay  = pSel->m_ArrReplayData;

	int nFrameSize = mArrReplay.size();
	pSel->SetReplayStatus(TRUE);
	while(1)
	{
		EnterCriticalSection(&pSel->m_Cri);
		if(!ESMGetValue(ESM_VALUE_AJAREPLAY) 
			|| !pSel->m_Process->m_bFinish || !pSel->GetReplayStatus())		
		{	
			LeaveCriticalSection(&pSel->m_Cri);
			ESMLog(5,_T("#########Kill Thread\n"));
			pSel->bDoingProcessing = FALSE;
			break;
		}
		LeaveCriticalSection(&pSel->m_Cri);

		for(int i = 0 ; i < nFrameSize; i++)
		{
			EnterCriticalSection(&pSel->m_Cri);
			if(!ESMGetValue(ESM_VALUE_AJAREPLAY) 
				|| !pSel->m_Process->m_bFinish || !pSel->GetReplayStatus())		
			{	
				LeaveCriticalSection(&pSel->m_Cri);
				ESMLog(5,_T("#########Kill Thread\n"));
				pSel->bDoingProcessing = FALSE;
				break;
			}
			LeaveCriticalSection(&pSel->m_Cri);

			pSel->StartCounter();
			cv::Mat frame = mArrReplay.at(i).clone();
			pSel->m_Process->EmitPattern(frame,pSel->GetCounter());

			EnterCriticalSection(&pSel->m_Cri);
			if(!ESMGetValue(ESM_VALUE_AJAREPLAY) 
				|| !pSel->m_Process->m_bFinish || !pSel->GetReplayStatus())		
			{	
				LeaveCriticalSection(&pSel->m_Cri);
				ESMLog(5,_T("#########Kill Thread\n"));
				pSel->bDoingProcessing = FALSE;
				break;
			}
			LeaveCriticalSection(&pSel->m_Cri);
		}
		EnterCriticalSection(&pSel->m_Cri);
		if(!ESMGetValue(ESM_VALUE_AJAREPLAY) 
			|| !pSel->m_Process->m_bFinish || !pSel->GetReplayStatus())		
		{	
			LeaveCriticalSection(&pSel->m_Cri);
			ESMLog(5,_T("#########Kill Thread\n"));
			pSel->bDoingProcessing = FALSE;
			break;
		}
		LeaveCriticalSection(&pSel->m_Cri);
	}
	
	pSel->m_ArrReplayData.clear();
	pSel->SetReplayStatus(FALSE);

	return FALSE;
}