#include "stdafx.h"
#include "ESMObjectTracking.h"

CESMObjectTracking::CESMObjectTracking(IMAGE_SIZE imageSize)
{
	//Tracking init
	//param.desc_pca = TrackerKCF::GRAY | TrackerKCF::CN;
	//param.desc_npca = 0;
	//param.compress_feature = true;
	//param.compressed_size = 2;	
	//
	//tracker = TrackerKCF::createTracker(param);	
	//tracker->setFeatureExtractor(SobelExtractor);

	tracker = TrackerKCF::create();
			
	//Stabilization init
	imageSetForStabilization = makePtr<videostab::ImageSetForStabilization>();
	motionEstimation = makePtr<videostab::MotionEstimatorRansacL2>(videostab::MM_TRANSLATION); 		
	ransac = motionEstimation->ransacParams(); 
	twoPassStabilizer = new videostab::TwoPassStabilizer(); 
		
	motionEstBuilder = makePtr<videostab::KeypointBasedMotionEstimatorGpu>(motionEstimation); 
	outlierRejector = makePtr<videostab::NullOutlierRejector>(); 

	_minInlierRatio = 0.1; 

	_imageSize = imageSize;

	if(_imageSize == FHD_SIZE)
	{
		_outputSize = cv::Size(1920, 1080);
		_samplingScale = 2.0;
		_templateScale = 2.0;
		_constraintTrackingDistance = 4000/_samplingScale;
	}
	else if(_imageSize == UHD_SIZE)
	{
		_outputSize = cv::Size(3840, 2160);
		_samplingScale = 4.0;
		_templateScale = 2.0;
		_constraintTrackingDistance = 8000/_samplingScale;
	}
	else
	{			
		_samplingScale = 4.0;
		_templateScale = 1.0;
		_constraintTrackingDistance = 8000/_samplingScale;
	}
}

CESMObjectTracking::~CESMObjectTracking(void)
{
	imageSetForStabilization->imagesForApplyingTransformationMatrix.clear();
	imageSetForStabilization->imagesForGeneratingTransformationMatrix.clear();
	testImage.clear();

	tracker.release();
	stabilizedFrames.release();
	imageSetForStabilization.release();
	motionEstimation.release();
	motionEstBuilder.release();
	outlierRejector.release();	
}

void CESMObjectTracking::Filtering(Mat& srcImage, Mat& dstImage)
{
	if(srcImage.empty())
		return;

	cuda::GpuMat gpuSrcImage(srcImage);
	cuda::GpuMat gpuDstImage;

	cuda::resize(gpuSrcImage, gpuDstImage, cv::Size(srcImage.cols/_samplingScale, srcImage.rows/_samplingScale), CV_INTER_NN);
	
	//BilateralFiltering(gpuDstImage, gpuDstImage);
	//Ptr<cuda::Filter> filter = 
	//	cuda::createGaussianFilter(CV_8UC3, CV_8UC3, cv::Size(31, 31), 0, 0);
	//filter->apply(gpuDstImage, gpuDstImage);
	
	gpuDstImage.download(dstImage);	
}

void CESMObjectTracking::InitTracking(Mat& image, Rect2d& trackingRegion)
{		
	tracker->init( image, trackingRegion);
	
	ModifyRegionToBeValid(image, trackingRegion, Rect2d(), false);
		
	//_baseLinePoint.x = trackingRegion.x;
	//_baseLinePoint.y = trackingRegion.y;

	_baseLinePoint.x = trackingRegion.width/2. + trackingRegion.x;
	_baseLinePoint.y = trackingRegion.height/2. + trackingRegion.y;
}

void CESMObjectTracking::UpdateTracking(Mat& image, Rect2d& trackingRegion)
{
//	float prevDistance = (sqrt(pow((float)trackingRegion.x, 2) + pow((float)trackingRegion.y, 2)));
	Rect2d temp = trackingRegion;

	tracker->update( image, trackingRegion);
	
	Point2f _subPointTest;
	if(temp.width != 0)				
		_subPointTest = Point2f(
		(trackingRegion.width/2. + trackingRegion.x - _baseLinePoint.x), 
		(trackingRegion.height/2. + trackingRegion.y - _baseLinePoint.y)
		);
	else
		_subPointTest = Point2f();
	_subPoint.push_back(_subPointTest);

	//if(ModifyRegionToBeValid(image, trackingRegion, Rect2d(), false))
	//{
	//	tracker = TrackerKCF::createTracker(param);	
	//	//tracker->setFeatureExtractor(SobelExtractor);
	//	tracker->init( image, trackingRegion);
	//}

	//float currDistance = (sqrt(pow((float)trackingRegion.x, 2) + pow((float)trackingRegion.y, 2)));	
	//float distance = abs(prevDistance - currDistance);
	//
	//if(distance > _constraintTrackingDistance)
	//{
	//	trackingRegion = temp;
	//	tracker = TrackerKCF::createTracker(param);			
	//	tracker->init( image, trackingRegion);
	//}
}

void CESMObjectTracking::CorrectCenterPointInRect(Rect2d& region)
{
	Point2f _subPointTest;
	if(region.width != 0)				
		_subPointTest = Point2f(
		(region.width/2. + region.x - _outputSize.width/_samplingScale/2), 
		(region.height/2. + region.y - _outputSize.height/_samplingScale/2)
		);
	else
		_subPointTest = Point2f();
	_subPoint.push_back(_subPointTest);
}

bool CESMObjectTracking::ModifyRegionToBeValid(Mat& image, Rect2d& inputRegion, Rect2d& paddingRegion, bool cutMargin)
{
	bool isModifiedRegion = false;
	paddingRegion = inputRegion; 
	paddingRegion.x = 0; 
	paddingRegion.y = 0;
	
	if( cutMargin == false) 
	{
		if(inputRegion.x + inputRegion.width > image.cols)
		{
			inputRegion.x = image.cols - inputRegion.width;		
			isModifiedRegion = true;
		}
		if(inputRegion.y + inputRegion.height > image.rows)
		{
			inputRegion.y = image.rows - inputRegion.height;		
			isModifiedRegion = true;
		}
		if(inputRegion.x < 0)
		{
			inputRegion.x = 0;
			isModifiedRegion = true;
		}
		if(inputRegion.y < 0)
		{
			inputRegion.y = 0;	
			isModifiedRegion = true;
		}
	}
	else
	{
		if(inputRegion.x + inputRegion.width > image.cols)
		{
			inputRegion.width -= inputRegion.x + inputRegion.width - image.cols;
			paddingRegion.width = inputRegion.width;
			isModifiedRegion = true;
		}
		if(inputRegion.y + inputRegion.height > image.rows)
		{
			inputRegion.height -= inputRegion.y + inputRegion.height- image.rows;
			paddingRegion.height = inputRegion.height;
			isModifiedRegion = true;
		}
		if(inputRegion.x < 0)
		{
			paddingRegion.width += inputRegion.x;
			inputRegion.width = paddingRegion.width;
			paddingRegion.x = abs(_outputSize.width/_templateScale - paddingRegion.width);
			inputRegion.x = 0;			
			isModifiedRegion = true;
		}
		if(inputRegion.y < 0)
		{
			paddingRegion.height += inputRegion.y;
			inputRegion.height = paddingRegion.height;
			paddingRegion.y = abs(_outputSize.height/_templateScale - paddingRegion.height);
			inputRegion.y = 0;
			isModifiedRegion = true;
		}		
	}	
	return isModifiedRegion;
}

void CESMObjectTracking::SobelExtractor(const Mat img, const cv::Rect roi, Mat& feat){
	Mat sobel[2];
	Mat patch;
	cv::Rect region=roi;

	// extract patch inside the image
	if(roi.x<0){region.x=0;region.width+=roi.x;}
	if(roi.y<0){region.y=0;region.height+=roi.y;}
	if(roi.x+roi.width>img.cols)region.width=img.cols-roi.x;
	if(roi.y+roi.height>img.rows)region.height=img.rows-roi.y;
	if(region.width>img.cols)region.width=img.cols;
	if(region.height>img.rows)region.height=img.rows;

	patch=img(region).clone();
	cvtColor(patch,patch, cv::COLOR_BGR2GRAY);

	// add some padding to compensate when the patch is outside image border
	int addTop,addBottom, addLeft, addRight;
	addTop=region.y-roi.y;
	addBottom=(roi.height+roi.y>img.rows?roi.height+roi.y-img.rows:0);
	addLeft=region.x-roi.x;
	addRight=(roi.width+roi.x>img.cols?roi.width+roi.x-img.cols:0);

	copyMakeBorder(patch,patch,addTop,addBottom,addLeft,addRight,BORDER_REPLICATE);

	Sobel(patch, sobel[0], CV_32F,1,0,1);
	Sobel(patch, sobel[1], CV_32F,0,1,1);

	merge(sobel,2,feat);	

	Mat showSobel;	
	addWeighted(sobel[0], 0.5, sobel[1], 0.5, 0, showSobel);
	double minVal, maxVal;
	minMaxLoc(showSobel, &minVal, &maxVal); //find minimum and maximum intensities

	showSobel.convertTo(showSobel, CV_8U, 255.0/(maxVal - minVal), -minVal * 255.0/(maxVal - minVal));
	imshow("sobel", showSobel);

	feat.convertTo(feat,CV_64F);

	feat=feat/255.0-0.5; // normalize to range -0.5 .. 0.5
}

void CESMObjectTracking::BilateralFiltering(cuda::GpuMat& srcImage, cuda::GpuMat& dstImage)
{
	if(srcImage.empty())
		return;

	if(_imageSize == UHD_SIZE)
		cuda::bilateralFilter(srcImage, dstImage, 
		50/(_samplingScale*1.15), 
		250/(_samplingScale*1.5), 
		250/(_samplingScale*1.5));

	else if(_imageSize == FHD_SIZE)
		cuda::bilateralFilter(srcImage, dstImage, 
		50/(_samplingScale*1.15), 
		250/(_samplingScale*1.5), 
		250/(_samplingScale*1.5));		
}

void CESMObjectTracking::RoiImageOfOriginalSize(Mat& srcImage, Mat& dstImage, Rect2d& trackingRegion)
{
	srcImage(Rect2d(
		trackingRegion.x * _samplingScale, 
		trackingRegion.y * _samplingScale, 
		trackingRegion.width * _samplingScale, 
		trackingRegion.height * _samplingScale)).copyTo(dstImage);
}

void CESMObjectTracking::RoiImageOfStabilizationSize(Mat& srcImage, Mat& dstImage, Rect2d& stabilizationRegion)
{	
	//트래킹 영역의 중심을 기준으로
	Point2d centerPoint = 
		Point2d((stabilizationRegion.x*_samplingScale + (stabilizationRegion.width*_samplingScale/2)), 
				(stabilizationRegion.y*_samplingScale + (stabilizationRegion.height*_samplingScale/2)));
		stabilizationRegion = Rect2d( 
				centerPoint.x - _outputSize.width / (_templateScale*2),
				centerPoint.y - _outputSize.height / (_templateScale*2),
				_outputSize.width / _templateScale,
				_outputSize.height / _templateScale);
	
	//원 영상의 중심점을 기준으로 스테빌라이져					
	//if(count == 0)
	//{
	//	count ++;
	//	return;
	//}
	//Point2d centerPoint = 
	//	Point2d(((-subX[count])*_samplingScale*2 + (stabilizationRegion.width*_samplingScale/2)), 
	//			((-subY[count])*_samplingScale*2 + (stabilizationRegion.height*_samplingScale/2)));
	//count++;	
	//
	//
	//stabilizationRegion = Rect2d( 
	//	centerPoint.x - _outputSize.width / (_templateScale*2),
	//	centerPoint.y - _outputSize.height / (_templateScale*2),
	//	_outputSize.width / _templateScale,
	//	_outputSize.height / _templateScale);

	

	Rect2d padding;
	bool isModifiedRegion = ModifyRegionToBeValid(srcImage, stabilizationRegion, padding, true);

	//Rect stabilizationRegion_ = stabilizationRegion;
	//Rect padding_ = padding;

	if(isModifiedRegion == true)		
	{	
		Mat temp(cv::Size(Round(_outputSize.width/_templateScale), 
					  Round(_outputSize.height/_templateScale)), CV_8UC3, cv::Scalar(0));
		srcImage(stabilizationRegion).copyTo(temp(padding));
		temp.copyTo(dstImage);		
	}	
	else
		srcImage(stabilizationRegion).copyTo(dstImage);		


}

void CESMObjectTracking::CreateStabilizatedMovie(string outputPath, double outputFps)
{
	imageSetForStabilization->inputImageForGeneratingTransformationMatrix(Mat());
	imageSetForStabilization->inputImageForApplyingTransformationMatrix(Mat());

	try {
		ransac.size = 3; 
		ransac.thresh = 5; 
		ransac.eps = 0.5; 

		motionEstimation->setRansacParams(ransac); 
		motionEstimation->setMinInlierRatio(_minInlierRatio); // second, create a feature detector 		
		
		motionEstBuilder->setOutlierRejector(outlierRejector); // 3-Prepare the stabilizer 
						
		int radius_pass = 15;		
		bool est_trim = true; 
		
		twoPassStabilizer->setEstimateTrimRatio(est_trim); 
		twoPassStabilizer->setMotionStabilizer(makePtr<videostab::GaussianMotionFilter>(radius_pass)); 		
		
		int radius = 15; 
		double trim_ratio = 0.1; 
		bool incl_constr = false; 						
				
		twoPassStabilizer->setFrameSource(imageSetForStabilization); 		
		twoPassStabilizer->setMotionEstimator(motionEstBuilder); 
		twoPassStabilizer->setRadius(radius); 
		twoPassStabilizer->setTrimRatio(trim_ratio); 
		twoPassStabilizer->setCorrectionForInclusion(incl_constr); 
		twoPassStabilizer->setBorderMode(BORDER_REPLICATE);		

		stabilizedFrames.reset(dynamic_cast<videostab::IFrameSource*>(twoPassStabilizer)); // 4-Processing the stabilized frames. The results are showed and saved. 
		IncodingWithStabilizedImage(stabilizedFrames, outputPath, outputFps); 	
	} 
	catch (const exception &e) 
	{ 
		cout << "error: " << e.what() << endl; 
		stabilizedFrames.release(); 		
	}
}

void CESMObjectTracking::IncodingWithStabilizedImage(Ptr<videostab::IFrameSource> stabilizedFrames, string outputPath, double outputFps)
{	
	//VideoWriter writer; 
	Mat stabilizedFrame; 		
	
	while (!(stabilizedFrame = stabilizedFrames->nextFrameForGeneratingTransformationMatrix()).empty()) 
	{ 			
		//if (!writer.isOpened()) 
		{				
			//writer.open(outputPath, VideoWriter::fourcc('X','V','I','D'), outputFps, stabilizedFrame.size());
			//writer.open(outputPath, VideoWriter::fourcc('m','p','e','g'), outputFps, stabilizedFrame.size());
		}				

		//cuda::GpuMat gpuSrcImage(stabilizedFrame);
		//cuda::GpuMat gpuDstImage;

		//cuda::resize(gpuSrcImage, gpuDstImage, _outputSize, cv::INTER_CUBIC);	

		//gpuDstImage.download(stabilizedFrame);
		//writer << stabilizedFrame; 
	}

}

double CESMObjectTracking::Round(double n)
{
	if (n >= 0)
	{
		//소수점이 0.5이상이면 올림
		if (n - (int)n >= 0.5)
			return ++n;
		//아니면 내림
		else
			return n;
	}

	//음수일 경우
	else
	{
		//소수점이 -0.5이상이면 올림
		if (n - (int)n >= -0.5)
			return n;
		//아니면 내림
		else
			return --n;
	}
}

void CESMObjectTracking::moveImage(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

