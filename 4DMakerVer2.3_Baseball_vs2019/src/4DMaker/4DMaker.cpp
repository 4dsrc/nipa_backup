///////////////////////////////////////////////////////////////////////////// 
//
//  4DMaker.cpp : Defines the class behaviors for the application.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  ESMLab, Inc., and may not be copied
//  nor disclosed except upon written agreement
//  by ESMLab, Inc..
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-18
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "4DMaker.h"
#include "MainFrm.h"
#include "tlhelp32.h"
#include "4DMakerAbout.h"

#include <setupapi.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#pragma comment (lib , "setupapi.lib" )
/////////////////////////////////////////////////////////////////////////////
// C4DMakerApp

BEGIN_MESSAGE_MAP(C4DMakerApp, CWinApp)
	//{{AFX_MSG_MAP(C4DMakerApp)
	ON_COMMAND(IDD_ABOUTBOX, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//ON_COMMAND(ID_FILE_NEW, OnFileNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// C4DMakerApp construction

C4DMakerApp::C4DMakerApp()
{
	// Place all significant initialization in InitInstance
	m_hDll = NULL;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only C4DMakerApp object

C4DMakerApp theApp;
#include <crtdbg.h>
/////////////////////////////////////////////////////////////////////////////
// C4DMakerApp initialization
#ifdef _4DMODEL
#include "4DModelDll.h"
MODELER_IMPORT_DLL int GetDllMacAddrCount();
MODELER_IMPORT_DLL void GetDllMacAddrFormList(int nIndex, TCHAR* strDllMacAddr);
#endif

BOOL C4DMakerApp::InitInstance()
{
#ifdef _4DMODEL
	TCHAR strDllMacAddr[18];
	CString strMacAdd;
	CStringArray arrMac;
	BOOL bMacEqual = FALSE;
	int nLoopCount = 0;
	int nMacListCnt = GetDllMacAddrCount();
	for (int nMacIndex = 0; nMacIndex <  nMacListCnt; nMacIndex++)
	{
		GetDllMacAddrFormList(nMacIndex, strDllMacAddr);
 		strMacAdd = strDllMacAddr;
		GetMacAddress(arrMac, _T(""));
		for( nLoopCount =0 ;nLoopCount< arrMac.GetCount(); nLoopCount++)
		{
			if( arrMac.GetAt(nLoopCount) == strMacAdd)
			{
				bMacEqual = TRUE;
				break;
			}
		}
		if(bMacEqual)
			break;
	}
	if(!bMacEqual)
	{
		AfxMessageBox(_T("지정된 PC가 아닙니다."));
		return TRUE;
	}
#endif
	/*
	if(!GetCheckUSB())
	{
		AfxMessageBox(_T("USB 있냐? 있으면 꼽아!"));
		return TRUE;
	}*/

	//_CrtSetBreakAlloc(33227);
	AfxOleInit();

	//-- COM 서버를 멀티쓰레드 환경에서 사용하기 위함.
	::CoUninitialize();
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
	//HRESULT hr = CoInitializeEx(NULL, COINIT_SPEED_OVER_MEMORY);
	//HRESULT hr = CoInitializeEx(NULL, COINIT_DISABLE_OLE1DDE);
	//HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);	

	//-- Avoiding multiple instances of "ESM.exe"
	//-- 2012-04-17 hongsu.jung
	//-- Message Box with 
	if(!TerminatePreviousInstance())
		return FALSE;

	//InitCommonControls();	
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);
	
	AfxEnableControlContainer();
	

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#if _MFC_VER < 0x700
	#ifdef _AFXDLL
		Enable3dControls();			// Call this when using MFC in a shared DLL
	#else
		Enable3dControlsStatic();	// Call this when linking to MFC statically
	#endif
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey( _T("ESMLab") );


	//-- 2013-04-24 hongsu.jung
	//-- Initialize GDI+ resource
	GdiplusStartupInput gdiplusStartupInput;
	GdiplusStartup(&m_gdiplusToken, &gdiplusStartupInput, NULL);


	m_nCmdShow = SW_SHOWMAXIMIZED;

	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object.
		
	//2010-01-20
	//Add SciLexer.dll to use editor
	m_hDll = CScintillaWnd::LoadScintillaDll();
	// try to get version from lexer dll
 	if (!m_hDll)
 		AfxMessageBox(_T("Fail Load SciLexer.dll"));

	/////////////////////////////////////////
	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;

	// create main SMILE frame window
	if (!pFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;

	//	add additional member variables and load calls for
	//	additional menu types your application may need. 

	HINSTANCE hInst = AfxGetResourceHandle();
	m_hSMILEMenu  = ::LoadMenu(hInst, MAKEINTRESOURCE(IDR_MAINFRAME));
	m_hSMILEAccel = ::LoadAccelerators(hInst, MAKEINTRESOURCE(IDR_MAINFRAME));

	// window placement persistence
	pFrame->ActivateFrame( m_nCmdShow );

	//------------------------------------------------------------------------------
	//-- Date	 : 2010-6-15
	//-- Owner	 : hongsu.jung
	//-- Comment : Get Argument 
	//------------------------------------------------------------------------------

	CString strCmd;
	strCmd = m_lpCmdLine;
	/*if(strCmd.GetLength())
	{
	ESMLog(1,_T("Start With This Test Project File [%s]"),strCmd);
	if(strCmd.CompareNoCase(_T("NORMAL"))== 0)
	{
	SetPriorityClass(GetCurrentProcess(), NORMAL_PRIORITY_CLASS);
	}else if(strCmd.CompareNoCase(_T("HIGH"))== 0)
	{
	SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS);
	}else if(strCmd.CompareNoCase(_T("REALTIME"))== 0)
	{
	SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
	}
	}else
	{

	}*/
	/*if(strCmd.GetLength())
	{
		CString strTitle;
		m_pMainWnd->GetWindowTextW(strTitle);

		strTitle.Replace(_T("4DMaker"), strCmd);

		m_pMainWnd->SetWindowTextW(strTitle);
	}*/
#if _ARGUMENT_RUN
	CStringArray strArgList;
	for(int i = 0; i < __argc; i++)
	{
		strArgList.Add(__targv[i]);
	}
	if(strArgList.GetCount() == 1)
	{
		CString strTitle;
		m_pMainWnd->GetWindowTextW(strTitle);

		strTitle.Replace(_T("4DMaker"), strArgList.GetAt(1));

		m_pMainWnd->SetWindowTextW(strTitle);

	}else if(strArgList.GetCount() > 1)
	{
		if(strArgList.GetAt(1).CompareNoCase(_T("/t")) == 0)
		{
			CString strTitle;
			m_pMainWnd->GetWindowTextW(strTitle);
			strTitle.Replace(_T("4DMaker"), strArgList.GetAt(2));
			m_pMainWnd->SetWindowTextW(strTitle);
		}
		else if(strArgList.GetAt(1).CompareNoCase(_T("/p")) == 0)
		{
			if(strArgList.GetAt(2).CompareNoCase(_T("NORMAL"))== 0)
			{
				SetPriorityClass(GetCurrentProcess(), NORMAL_PRIORITY_CLASS);
			}else if(strArgList.GetAt(2).CompareNoCase(_T("HIGH"))== 0)
			{
				SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS);
			}else if(strArgList.GetAt(3).CompareNoCase(_T("REALTIME"))== 0)
			{
				SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
			}
		}
	}
#endif

	return TRUE;
}

//-- If a previous instance of "SMILE" is already running, kill it.
//-- 2012-05-02 hongsu.jung
//-- MessageBox and Optional Kill Previous Process
BOOL C4DMakerApp::TerminatePreviousInstance()
{
	//Take a snapshot of all processes in the system
	HANDLE hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if(INVALID_HANDLE_VALUE == hProcessSnap)
	{
		//ERROR
		return FALSE;
	}

	PROCESSENTRY32 pe32;
	pe32.dwSize =  sizeof(PROCESSENTRY32);

	//Retrieve information about the first process, and exit if unsuccessful
	if(!Process32First(hProcessSnap, &pe32))
	{
		//ERROR
		CloseHandle(hProcessSnap); //clean the snapshot object
		return FALSE;
	}

	CString strExeFileName1 = _T("4DMaker.exe");
	CString strExeFileName2 = _T("4DMakerD.exe");

	//-- 2013-09-26 hongsu@esmlab.com
	//-- kill Process ffmepg 
	CString strExeFFMpeg	= _T("ffmpeg.exe");
	
	BOOL bExe1 = FALSE;
	BOOL bExe2 = FALSE;
	BOOL bExe3 = FALSE;
	do
	{
		bExe1 = strExeFileName1.CompareNoCase(pe32.szExeFile);
		bExe2 = strExeFileName2.CompareNoCase(pe32.szExeFile);
		bExe3 = strExeFFMpeg.CompareNoCase(pe32.szExeFile);

		//-- 2013-09-26 hongsu@esmlab.com
		//-- Kill Process ffmpeg 
		if(!bExe3)
		{
			if(GetCurrentProcessId() != pe32.th32ProcessID)
			{
				DWORD dwExitCode = 0;
				HANDLE hHandle = OpenProcess(PROCESS_ALL_ACCESS, 0,pe32.th32ProcessID);
				GetExitCodeProcess(hHandle, &dwExitCode);
				TerminateProcess(hHandle, dwExitCode);
			}
		}

		if(!bExe1 || !bExe2 )
		{

			if(GetCurrentProcessId() != pe32.th32ProcessID)
			{
				//-- Terminate the previous instance.
				//-- Message Box
				CString strMsg;
				if(!bExe1)	strMsg.Format(_T("Already %s is running\nDo you want to kill previous process?"),strExeFileName1);
				if(!bExe2)	strMsg.Format(_T("Already %s is running\nDo you want to kill previous process?"),strExeFileName2);

				int nReturn = AfxMessageBox(strMsg,MB_YESNO);
				switch(nReturn)
				{
				case IDYES:		
					if(GetCurrentProcessId() != pe32.th32ProcessID)
					{
						DWORD dwExitCode = 0;
						HANDLE hHandle = OpenProcess(PROCESS_ALL_ACCESS, 0,pe32.th32ProcessID);
						GetExitCodeProcess(hHandle, &dwExitCode);
						TerminateProcess(hHandle, dwExitCode);
					}
					break;
				default:
					//-- permit multi process
					return FALSE;
					break;
				}
			}			
		}
	} while(Process32Next(hProcessSnap,&pe32));

	CloseHandle(hProcessSnap);
	return TRUE;

}


/////////////////////////////////////////////////////////////////////////////
// C4DMakerApp message handlers

int C4DMakerApp::ExitInstance() 
{
	//-- Release GDI+ resource
	GdiplusShutdown(m_gdiplusToken);

	if (m_hSMILEMenu != NULL)
		FreeResource(m_hSMILEMenu);
	if (m_hSMILEAccel != NULL)
		FreeResource(m_hSMILEAccel);

	if(m_pMainWnd != NULL)
	{
		delete m_pMainWnd;
		m_pMainWnd = NULL;
	}
	
	//-- 2010-01-20
	//-- Remove SciLexer.dll
	if (m_hDll)
		AfxFreeLibrary(m_hDll);
	else
		AfxMessageBox(_T("Fail Free SciLexer.dll"));

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
	CoUninitialize();
	return CWinApp::ExitInstance();
}

// 2012-04-17 hongsu
void C4DMakerApp::OnAppAbout()
{
	C4DMakerAbout about;
	about.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// C4DMakerApp message handlers
#pragma comment(lib, "Iphlpapi.lib")
#include <Iphlpapi.h>

BOOL C4DMakerApp::GetMacAddress(CStringArray& strMacArray, CString strSpecifiedIP)
{
	PIP_ADAPTER_INFO pAdapterInfo;
	PIP_ADAPTER_INFO pAdapter = NULL;
	DWORD dwRetVal = 0;

	ULONG ulOutBufLen = sizeof (IP_ADAPTER_INFO);
	pAdapterInfo = new IP_ADAPTER_INFO[ulOutBufLen];

	if (pAdapterInfo == NULL)
		return FALSE;

	if (GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW)
	{
		delete pAdapterInfo;
		pAdapterInfo = new IP_ADAPTER_INFO[ulOutBufLen];
		if (pAdapterInfo == NULL)
			return FALSE;
	}

	if ((dwRetVal = GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) == NO_ERROR)
	{
		pAdapter = pAdapterInfo;
		while (pAdapter)
		{
			CStringA strMacAddress;
			strMacAddress.Format("%02X:%02X:%02X:%02X:%02X:%02X",
				pAdapter->Address[0],
				pAdapter->Address[1],
				pAdapter->Address[2],
				pAdapter->Address[3],
				pAdapter->Address[4],
				pAdapter->Address[5]);

			CStringA strIPAddress;
			strIPAddress.Format("%s", pAdapter->IpAddressList.IpAddress.String);

			if(strSpecifiedIP.GetLength() > 0)
			{
				if(strIPAddress.Find(CT2A(strSpecifiedIP)) >= 0)
				{
					strMacArray.Add(CA2T(strMacAddress));
					break;
				}
			}
			else
				strMacArray.Add(CA2T(strMacAddress));

			pAdapter = pAdapter->Next;
		}
	}

	delete pAdapterInfo;
	pAdapterInfo = NULL;
	return TRUE;
}

BOOL C4DMakerApp::GetCheckUSB ()
{
	GUID guidDeviceInterface = { 0xA5DCBF10, 0x6530, 0x11D2, { 0x90, 0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED } };
	if (guidDeviceInterface == GUID_NULL)
	{
		return FALSE;
	}

	BOOL bResult = TRUE;
	BOOL bResult2 = FALSE;
	HDEVINFO hDevInfo;
	SP_DEVINFO_DATA DevInfoData;

	SP_DEVICE_INTERFACE_DATA DevInterfaceData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA pInterfaceDetailData = NULL;

	ULONG requiredLength=0;

	LPTSTR lpDevPath = NULL;
	CString strTemp = _T("");

	DWORD index = 0;

	hDevInfo = SetupDiGetClassDevs(&guidDeviceInterface, NULL, NULL,DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);

	if (hDevInfo == INVALID_HANDLE_VALUE) 
	{ 
		printf("Error SetupDiGetClassDevs: %d.\n", GetLastError());
		goto done;
	}

	DevInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
	
	for (index = 0; SetupDiEnumDeviceInfo(hDevInfo, index, &DevInfoData); index++)
	{
		if (lpDevPath)
		{
			LocalFree(lpDevPath);
			lpDevPath = NULL;
		}
		if (pInterfaceDetailData)
		{
			LocalFree(pInterfaceDetailData);
			pInterfaceDetailData = NULL;
		}

 		DevInterfaceData.cbSize = sizeof(SP_INTERFACE_DEVICE_DATA);
 
 		bResult = SetupDiEnumDeviceInterfaces(hDevInfo, NULL, &guidDeviceInterface, index, &DevInterfaceData);
 
		DWORD dwError;

		dwError = GetLastError();

 		if (GetLastError () == ERROR_NO_MORE_ITEMS)
 		{
 			break;
 		}

		if (!bResult) 
		{
			printf("Error SetupDiEnumDeviceInterfaces: %d.\n", GetLastError());
			goto done;
		}

		bResult = SetupDiGetDeviceInterfaceDetail(hDevInfo,	&DevInterfaceData, NULL, 0,	&requiredLength, NULL);


		if (!bResult) 
		{
			if ((ERROR_INSUFFICIENT_BUFFER==GetLastError()) && (requiredLength>0))
			{
				pInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)LocalAlloc(LPTR, requiredLength);

				if (!pInterfaceDetailData) 
				{ 
					// ERROR 
					printf("Error allocating memory for the device detail buffer.\n");
					goto done;
				}
			}
			else
			{
				printf("Error SetupDiEnumDeviceInterfaces: %d.\n", GetLastError());
				goto done;
			}
		}

		if(pInterfaceDetailData)
			pInterfaceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

		bResult = SetupDiGetDeviceInterfaceDetail(hDevInfo, &DevInterfaceData, pInterfaceDetailData, requiredLength, NULL, &DevInfoData);

		if (!bResult) 
		{
			printf("Error SetupDiGetDeviceInterfaceDetail: %d.\n", GetLastError());
			goto done;
		}
		else
		{
			size_t nLength = 0;
			if(pInterfaceDetailData)
				nLength = wcslen (pInterfaceDetailData->DevicePath) + 1;

			lpDevPath = (TCHAR *) LocalAlloc (LPTR, nLength * sizeof(TCHAR));
			if(!lpDevPath)
			{
				printf("Error %d.", GetLastError());
				goto done;
			}

			StringCchCopy(lpDevPath, nLength, pInterfaceDetailData->DevicePath);
			lpDevPath[nLength-1] = 0;

			printf("Device path: %S\n", lpDevPath);
			CString temp;
			temp.Format(_T("%s"),lpDevPath);

			if(1<temp.Find(SDI_VID_KEYLOCK))
				bResult2 = true;

			if(!lpDevPath)
			{
				printf("Error %d.", GetLastError());
				goto done;
			}
		}
	}	

done:
	if(lpDevPath)
		LocalFree(lpDevPath);
	if(pInterfaceDetailData)
		LocalFree(pInterfaceDetailData);    
	
	bResult = SetupDiDestroyDeviceInfoList(hDevInfo);
	
	return bResult2;
}
