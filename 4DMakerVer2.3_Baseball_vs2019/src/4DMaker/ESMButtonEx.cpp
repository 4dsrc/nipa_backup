// ESMButtonEx.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMButtonEx.h"


// CESMButtonEx

IMPLEMENT_DYNAMIC(CESMButtonEx, CESMBtn)

CESMButtonEx::CESMButtonEx()
{
	m_pFont = NULL;
	m_clrDisable = 0x2c2c2c;//GetRValue(0x2c);
	m_clrNormal = 0x1c1c1c;//GetRValue(0x1c);
	m_clrOver = 0x252525;//GetRValue(0x25);
	m_clrPress = 0x060606;//GetRValue(0x06);
	m_clrTextWhite = 0xffffff;//GetRValue(0xff);
	m_clrTextGray = 0x515151;//GetRValue(0x51);

	m_colorOver = Color::MakeARGB(255,GetRValue(m_clrOver),GetGValue(m_clrOver),GetBValue(m_clrOver));
	m_colorNormal = Color::MakeARGB(255,GetRValue(m_clrNormal),GetGValue(m_clrNormal),GetBValue(m_clrNormal));
	m_colorPress = Color::MakeARGB(255,GetRValue(m_clrPress),GetGValue(m_clrPress),GetBValue(m_clrPress));

	/*ChangeFont(,_T("Noto Sans CJK KR Regular"),FW_NORMAL);

	m_nFontSize = nFontSize;*/
}

CESMButtonEx::~CESMButtonEx()
{
}

BEGIN_MESSAGE_MAP(CESMButtonEx, CESMBtn)
END_MESSAGE_MAP()

void CESMButtonEx::ChangeColors()
{
	//SetDisabledColors(RGB(90,90,90), 0);
	//SetColors(	TEST_R,
	//	TEST_G,
	//	//RGB(255,255,255),
	//	TEST_B,
	//	TEST_W);
	//Color m_clrLowerHot = Color::MakeARGB()
}
void CESMButtonEx::ChangeFont(int nSize, CString strFont, int nFontType)
{
	if(!m_pFont)
	{
		m_pFont = new CFont();
		m_pFont->CreateFont( nSize, 0, 0, 0, nFontType, false, false, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			FIXED_PITCH|FF_MODERN, strFont);
	}
	SetFont(m_pFont);	
}
void CESMButtonEx::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	UINT				state(lpDIS->itemState);
	CString				btnText;
	Graphics			graphics(lpDIS->hDC);
	RECT				rctBgContent;
	Rect				clientRect;
	CRect				txtRect(0,0,0,0);
	LinearGradientBrush *plinGrBrush = NULL;

	UINT partID;
	UINT stateID;

	Color	clrDisabled = Color::MakeARGB(255,	
		m_clrDisable,
		m_clrDisable,
		m_clrDisable);
	Color	*pclrLower = NULL;

	partID	= DFC_BUTTON;
	stateID	= (state & ODS_SELECTED) ? DFCS_BUTTONPUSH|DFCS_FLAT|DFCS_PUSHED: DFCS_BUTTONPUSH|DFCS_FLAT;
	if(state & ODS_DISABLED) stateID = DFCS_BUTTONPUSH|DFCS_FLAT|DFCS_INACTIVE;
	DrawFrameControl(lpDIS->hDC, &lpDIS->rcItem, partID,stateID);
	memcpy(&rctBgContent, &lpDIS->rcItem, sizeof(RECT));
	//-- 2013-09-06 hongsu@esmlab.com
	//-- Non Draw Round
	//InflateRect(&rctBgContent, -GetSystemMetrics(SM_CXEDGE), -GetSystemMetrics(SM_CYEDGE));

	//TRACE("%d\n",stateID);
	clientRect.X = rctBgContent.left;
	clientRect.Y = rctBgContent.top;
	clientRect.Width = rctBgContent.right - rctBgContent.left;
	clientRect.Height = rctBgContent.bottom - rctBgContent.top;

	if (state & ODS_SELECTED)
	{
		if(m_nStatus == GRID_STATUS_OVER ) 
		{
			pclrLower = &m_colorPress;//m_colorPress
		}
		else 
			pclrLower = &m_colorNormal;
		plinGrBrush = new LinearGradientBrush(clientRect, *pclrLower, *pclrLower, 90);
	}
	else if( state & ODS_DISABLED )
	{
		pclrLower	= &clrDisabled;
		plinGrBrush = new LinearGradientBrush(clientRect, *pclrLower, *pclrLower, 90);
	}
	else
	{
		if(m_nStatus == GRID_STATUS_OVER ) 
			pclrLower = &m_colorOver ;
		else
			pclrLower = &m_colorNormal;
		plinGrBrush = new LinearGradientBrush(clientRect, *pclrLower, *pclrLower, 90);
	}

	graphics.FillRectangle(plinGrBrush, clientRect );

	// Draw the text
	GetWindowText(btnText);
	if(!btnText.IsEmpty())
	{
		COLORREF	clrTxt, clrOrg;
		int			bkModeOld;
		// select fg color
		if (state & ODS_DISABLED)
			clrTxt = m_clrTextGray;
		else if (m_nStatus == GRID_STATUS_OVER)
			clrTxt = m_clrTextWhite;
		else
			clrTxt = m_clrTextWhite;

		// calculate bounding rect cause we obviously want to make
		// it multi lined
		DrawText(lpDIS->hDC, btnText, btnText.GetLength(), &txtRect, DT_CALCRECT | DT_CENTER);

		int txtWidth = txtRect.Width();
		int txtHight = txtRect.Height();

		txtRect.left	= clientRect.X + (clientRect.Width - txtWidth)/2;
		txtRect.right	= txtRect.left + txtWidth;
		txtRect.top		= clientRect.Y + (clientRect.Height - txtHight)/2;
		txtRect.bottom	= txtRect.top + txtHight;

		// discard parts not in clientRect
		txtRect.IntersectRect(&rctBgContent, &txtRect);
		// txt color
		clrOrg = SetTextColor(lpDIS->hDC, clrTxt);
		// transparent bg
		bkModeOld = SetBkMode(lpDIS->hDC, TRANSPARENT);

		// Draw text
		DrawText(lpDIS->hDC, btnText, btnText.GetLength(), &txtRect, DT_CENTER);
		SetBkMode(lpDIS->hDC, bkModeOld);
		SetTextColor(lpDIS->hDC, clrOrg);
	}
	delete plinGrBrush;
	plinGrBrush = NULL;
}

// CESMButtonEx 메시지 처리기입니다.


