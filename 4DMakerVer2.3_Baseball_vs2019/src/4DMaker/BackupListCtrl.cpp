#include "stdafx.h"
#include "BackupListCtrl.h"


CBackupListCtrl::CBackupListCtrl(void)
{
}


CBackupListCtrl::~CBackupListCtrl(void)
{
}

void CBackupListCtrl::DoDataExchange(CDataExchange* pDX)
{
}

BEGIN_MESSAGE_MAP(CBackupListCtrl, CListCtrl)
END_MESSAGE_MAP()


void CBackupListCtrl::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	CListCtrl::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CBackupListCtrl::OnDestroy()
{
	CListCtrl::OnDestroy();
}

void CBackupListCtrl::DeleteAllItems()
{
	CListCtrl::DeleteAllItems();
}

void CBackupListCtrl::Init()
{
	//SetExtendedStyle(LVS_EX_DOUBLEBUFFER | LVS_EX_FULLROWSELECT );
	SetExtendedStyle(LVS_EX_CHECKBOXES| LVS_EX_FULLROWSELECT);
	InsertColumn(0, _T("#")	,	LVCFMT_CENTER,50);	
	//InsertColumn(1, _T("Count"),LVCFMT_CENTER,50);
	InsertColumn(1, _T("FolderName"),LVCFMT_CENTER,300);
}

void CBackupListCtrl::AddFolder(CString strFolderName)
{
	if (CheckExist(strFolderName))
		return;

	int nTotal = GetItemCount();
	CString strNum;
	strNum.Format(_T("%d"),nTotal);
	InsertItem(nTotal,strNum);

	SetItem(nTotal, 0, LVIF_TEXT, strNum, 0,0,0,NULL);
	SetItem(nTotal, 1, LVIF_TEXT, strFolderName, 0,0,0,NULL);	
}

bool CBackupListCtrl::CheckExist(CString str)
{
	int nCnt = GetItemCount();

	for (int i = 0; i < nCnt ; i++)
	{
		CString strValue = GetItemText(i, 1);

		if (strValue == str)
			return true;
	}

	return false;
}

void CBackupListCtrl::CheckAll(BOOL f)
{
	int nCnt = GetItemCount();

	for (int i = 0; i < nCnt ; i++)
	{
		if (f)	SetCheck(i, TRUE);
		else	SetCheck(i, FALSE);
	}
}
