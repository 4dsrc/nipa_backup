// ESMOptCamDirection.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMOptCamDirection.h"
#include "afxdialogex.h"

#include "ESMFunc.h"
#include "ESMUtil.h"
#include "ESMFileOperation.h"
#include "ESMIni.h"


// CESMOptCamDirection 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMOptCamDirection, CDialogEx)

CESMOptCamDirection::CESMOptCamDirection(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMOptCamDirection::IDD, pParent)
	, m_strDelay(_T(""))
{
	m_bModify = FALSE;
}

CESMOptCamDirection::~CESMOptCamDirection()
{
}

void CESMOptCamDirection::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CMB_REC_GROUP, m_cmbRecGrp);
	DDX_Control(pDX, IDC_LIST_DIRECTION, m_lstData);
	DDX_Control(pDX, IDC_BTN_NONE, m_btnNone);
	DDX_Control(pDX, IDC_BTN_FORWARD, m_btnForward);
	DDX_Control(pDX, IDC_BTN_BACKWARD, m_btnBackward);
	DDX_Text(pDX, IDC_EDIT_DELAY, m_strDelay);
	DDX_Control(pDX, IDC_BTN_DELAY_A, m_btnDelayA);
	DDX_Control(pDX, IDC_BTN_DELAY_B, m_btnDelayB);
	DDX_Control(pDX, IDC_EDIT_DELAY, m_editDelay);
}


BEGIN_MESSAGE_MAP(CESMOptCamDirection, CDialogEx)
	ON_CBN_SELENDOK(IDC_CMB_REC_GROUP, &CESMOptCamDirection::OnCbnSelendokCmbRecGroup)
	ON_BN_CLICKED(IDC_BTN_NONE, &CESMOptCamDirection::OnBnClickedBtnNone)
	ON_BN_CLICKED(IDC_BTN_FORWARD, &CESMOptCamDirection::OnBnClickedBtnForward)
	ON_BN_CLICKED(IDC_BTN_BACKWARD, &CESMOptCamDirection::OnBnClickedBtnBackward)
	ON_BN_CLICKED(IDC_BTN_SAVE, &CESMOptCamDirection::OnBnClickedBtnSave)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_DELAY_A, &CESMOptCamDirection::OnBnClickedBtnDelayA)
	ON_BN_CLICKED(IDC_BTN_DELAY_B, &CESMOptCamDirection::OnBnClickedBtnDelayB)
END_MESSAGE_MAP()


// CESMOptCamDirection 메시지 처리기입니다.


BOOL CESMOptCamDirection::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_lstData.InsertColumn(0, _T(""), LVCFMT_CENTER, 10);
	m_lstData.InsertColumn(1, _T("Group"), LVCFMT_CENTER, 100);
	m_lstData.InsertColumn(2, _T("Direction"), LVCFMT_CENTER, 150);
	m_lstData.InsertColumn(3, _T("Delay (A)"), LVCFMT_CENTER, 85);
	m_lstData.InsertColumn(4, _T("Delay (B)"), LVCFMT_CENTER, 85);
	m_lstData.ModifyStyle(LVS_TYPEMASK, LVS_REPORT);
	m_lstData.SetExtendedStyle( LVS_EX_FULLROWSELECT );

	m_background.CreateSolidBrush(RGB(44,44,44));
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CESMOptCamDirection::OnCbnSelendokCmbRecGroup()
{
	m_lstData.DeleteAllItems();

	CString strRecord;
	m_cmbRecGrp.GetLBText(m_cmbRecGrp.GetCurSel(), strRecord);
	if(strRecord.IsEmpty())
	{
		ESMLog(0, _T("COMBOBOX_Group is Empty!!"));
		return;
	}

	CString strPath;
	strPath.Format(_T("%s\\%s.ini"), ESMGetPath(ESM_PATH_SETUP), _T("CameraGroup"));
	
	CStringArray strGroup;
	CameraGroupLoad(strGroup);

	CESMFileOperation fo;
	if(fo.IsFileExist(strPath) == FALSE)
	{
		CFile file;
		if(file.Open(strPath, CFile::modeCreate | CFile::modeReadWrite))
		{
			file.Close();
		}
	}

	CESMIni ini;
	if(ini.SetIniFilename(strPath))
	{
		CString _Group, _Direction, _Delay_A, _Delay_B;

		for(int i = 0; i < strGroup.GetCount(); i++)
		{
			_Direction = ini.GetString(strRecord, strGroup.GetAt(i), _T("None"));
			
			_Delay_A = ini.GetString(_T("DELAY_A"), strGroup.GetAt(i), _T("0"));
			_Delay_B = ini.GetString(_T("DELAY_B"), strGroup.GetAt(i), _T("0"));

			m_lstData.InsertItem(i, _T(""));
			m_lstData.SetItemText(i, 1, strGroup.GetAt(i));
			m_lstData.SetItemText(i, 2, _Direction);

			m_lstData.SetItemText(i, 3, _Delay_A);
			m_lstData.SetItemText(i, 4, _Delay_B);

			ini.WriteString(strRecord, strGroup.GetAt(i), _Direction);
			
			ini.WriteString(_T("DELAY_A"), strGroup.GetAt(i), _Delay_A);
			ini.WriteString(_T("DELAY_B"), strGroup.GetAt(i), _Delay_B);
		}
	}
}


void CESMOptCamDirection::OnBnClickedBtnNone()
{
	CString strRecord;
	m_cmbRecGrp.GetLBText(m_cmbRecGrp.GetCurSel(), strRecord);
	if(strRecord.IsEmpty())
	{
		ESMLog(0, _T("COMBOBOX_Group is Empty!!"));
		return;
	}

	CString strPath;
	strPath.Format(_T("%s\\%s.ini"), ESMGetPath(ESM_PATH_SETUP), _T("CameraGroup"));

	CESMIni ini;
	if(ini.SetIniFilename(strPath) != TRUE)
	{
		return;
	}

	int nSelect = 0;
	POSITION pos = NULL; 
	CListCtrl *pList = &m_lstData;
	pos  = pList->GetFirstSelectedItemPosition();
	nSelect = pList->GetSelectedCount();

	if( NULL != pos )
	{
		int nCnt = 1;
		while(pos)
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );

			pList->SetItemText(nItemIdx, 2, _T("None"));

			ini.WriteString(strRecord, pList->GetItemText(nItemIdx, 1), _T("None"));
		}
	}
}


void CESMOptCamDirection::OnBnClickedBtnForward()
{
	CString strRecord;
	m_cmbRecGrp.GetLBText(m_cmbRecGrp.GetCurSel(), strRecord);
	if(strRecord.IsEmpty())
	{
		ESMLog(0, _T("COMBOBOX_Group is Empty!!"));
		return;
	}

	CString strPath;
	strPath.Format(_T("%s\\%s.ini"), ESMGetPath(ESM_PATH_SETUP), _T("CameraGroup"));

	CESMIni ini;
	if(ini.SetIniFilename(strPath) != TRUE)
	{
		return;
	}

	int nSelect = 0;
	POSITION pos = NULL; 
	CListCtrl *pList = &m_lstData;
	pos  = pList->GetFirstSelectedItemPosition();
	nSelect = pList->GetSelectedCount();

	if( NULL != pos )
	{
		int nCnt = 1;
		while(pos)
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );

			pList->SetItemText(nItemIdx, 2, _T("Forward"));

			ini.WriteString(strRecord, pList->GetItemText(nItemIdx, 1), _T("Forward"));
		}
	}
}


void CESMOptCamDirection::OnBnClickedBtnBackward()
{
	CString strRecord;
	m_cmbRecGrp.GetLBText(m_cmbRecGrp.GetCurSel(), strRecord);
	if(strRecord.IsEmpty())
	{
		ESMLog(0, _T("COMBOBOX_Group is Empty!!"));
		return;
	}

	CString strPath;
	strPath.Format(_T("%s\\%s.ini"), ESMGetPath(ESM_PATH_SETUP), _T("CameraGroup"));

	CESMIni ini;
	if(ini.SetIniFilename(strPath) != TRUE)
	{
		return;
	}

	int nSelect = 0;
	POSITION pos = NULL; 
	CListCtrl *pList = &m_lstData;
	pos  = pList->GetFirstSelectedItemPosition();
	nSelect = pList->GetSelectedCount();

	if( NULL != pos )
	{
		int nCnt = 1;
		while(pos)
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );

			pList->SetItemText(nItemIdx, 2, _T("Backward"));

			ini.WriteString(strRecord, pList->GetItemText(nItemIdx, 1), _T("Backward"));
		}
	}
}


void CESMOptCamDirection::OnBnClickedBtnSave()
{
	
}


HBRUSH CESMOptCamDirection::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	hbr = (HBRUSH)m_background;
	switch(nCtlColor)
	{
	case CTLCOLOR_STATIC:
		{

			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			//pDC->SetBkColor(RGB(56, 56, 56));  // 글자 배경색 변경
			pDC->SetBkMode(TRANSPARENT);
			//return (HBRUSH)m_brush;
		}
		break;
	case CTLCOLOR_EDIT:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			pDC->SetBkColor(RGB(0,0,0));
			//hbr = (HBRUSH)(hbr.GetSafeHandle());  
		}
	case CTLCOLOR_LISTBOX:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			pDC->SetBkColor(RGB(0,0,0));
			pDC->SetBkMode(TRANSPARENT);
		}
		break;
	}
	return hbr;
}


void CESMOptCamDirection::OnBnClickedBtnDelayA()
{
	UpdateData();
	if(ESMGetNumValidate(m_strDelay) != TRUE || m_strDelay.IsEmpty())
	{
		AfxMessageBox(_T("Please enter only numbers"));
		return;
	}

	CString strPath;
	strPath.Format(_T("%s\\%s.ini"), ESMGetPath(ESM_PATH_SETUP), _T("CameraGroup"));

	CESMIni ini;
	if(ini.SetIniFilename(strPath) != TRUE)
	{
		return;
	}

	int nSelect = 0;
	POSITION pos = NULL; 
	CListCtrl *pList = &m_lstData;
	pos  = pList->GetFirstSelectedItemPosition();
	nSelect = pList->GetSelectedCount();

	if( NULL != pos )
	{
		int nCnt = 1;
		while(pos)
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );

			pList->SetItemText(nItemIdx, 3, m_strDelay);

			ini.WriteString(_T("DELAY_A"), pList->GetItemText(nItemIdx, 1), m_strDelay);
		}
	}
}


void CESMOptCamDirection::OnBnClickedBtnDelayB()
{
	UpdateData();
	if(ESMGetNumValidate(m_strDelay) != TRUE || m_strDelay.IsEmpty())
	{
		AfxMessageBox(_T("Please enter only numbers"));
		return;
	}

	CString strPath;
	strPath.Format(_T("%s\\%s.ini"), ESMGetPath(ESM_PATH_SETUP), _T("CameraGroup"));

	CESMIni ini;
	if(ini.SetIniFilename(strPath) != TRUE)
	{
		return;
	}

	int nSelect = 0;
	POSITION pos = NULL; 
	CListCtrl *pList = &m_lstData;
	pos  = pList->GetFirstSelectedItemPosition();
	nSelect = pList->GetSelectedCount();

	if( NULL != pos )
	{
		int nCnt = 1;
		while(pos)
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );

			pList->SetItemText(nItemIdx, 4, m_strDelay);

			ini.WriteString(_T("DELAY_B"), pList->GetItemText(nItemIdx, 1), m_strDelay);
		}
	}
}
