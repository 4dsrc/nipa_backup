///////////////////////////////////////////////////////////////////////////////
//
//	MainFrmMgr.cpp : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-06-21
// @ver.1.0	4DMaker
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "4DMaker.h"
#include "MainFrm.h"
#include "ESMAdjustMgrDlg.h"
#include "ESMKzonePrism.h"
#include "ESMTemplateDlg.h"
#include "ESMMuxBackupDlg.h"
//REGISTRY
#include "DSCViewer.h"
#include "DSCMgr.h"
#include "ESMRegistry.h"
#include "ESMIni.h"
#include "ESMFunc.h"

#include "ESMFileOperation.h"
#include "FocusFileSaveDlg.h"

#include "DSCListViewer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <MMSystem.h>
#pragma comment(lib, "winmm")

static const float temp_sync_percentage = 0.8;
#define SET_SYNC_TEST_COUNT 30

CMutex g_mutex4DP(FALSE, _T("4DPSender"));
//------------------------------------------------------------------------------ 
//! @brief
//! @date     2013-04-23
//! @owner    Hongsu Jung
//! @note
//! @return        
//------------------------------------------------------------------------------


void CMainFrame::OnUpdateDSCTool(CCmdUI *pCmdUI)
{ 
	//-- 2013-04-29 hongsu@esmlab.com
	//-- Test Open Code
	if( m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_PAUSE 
		|| m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_PAUSEREADY
		|| m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_RESUME)
	{
		pCmdUI->Enable(FALSE);
		return;
	}

	if(m_wndDSCViewer.GetExecuteMovieMode())
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);



}	

void  CMainFrame::OnDSCConnection(void)		
{
	ESMEvent* pMsg = NULL;
	//-- 2013-09-10 hongsu@esmlab.com
	//-- Check Connection
	pMsg = new ESMEvent();		
	pMsg->message = WM_ESM_LIST_CHECK;	
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);

	if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		pMsg = new ESMEvent();
		pMsg->message	= WM_ESM_LIST_CHECK;
		OnESMMsg((WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}
}

void  CMainFrame::OnDSCSetFocus(void)		
{
	ESMLog(5,_T("Set Focus"));	

	CString strModel = GetCamModel();
	
	if (strModel.CompareNoCase(_T("GH5")) == 0)
	{
		// GH5		
	}
	else
	{
		// NX1
		m_wndDSCViewer.SetFocusAll();
	}
}

void  CMainFrame::OnDSCFocussing(void)		
{
	ESMLog(5,_T("Set AutoFocus"));	
	//-- 2013-09-04 hongsu@esmlab.com
	//	m_wndDSCViewer.SetFocusMode(FALSE);
	//	m_wndDSCViewer.m_pDSCMgr->DoMinFocus();
	//	Sleep(500);

	//	m_wndDSCViewer.SetFocusMode(TRUE);
	//m_wndDSCViewer.m_pDSCMgr->DoS1Press();
	//	m_wndDSCViewer.m_pDSCMgr->DoS1Release();		
	
	CString strModel = GetCamModel();

	if (strModel.CompareNoCase(_T("GH5")) == 0)
	{
		// GH5
		m_wndLiveView.TouchAfAll(SDI_MODEL_GH5);
	}
	else
	{
		// NX1
		m_wndDSCViewer.m_pDSCMgr->DoS1Press();
	}

}

void  CMainFrame::OnDSCFocussRelease(void)		
{
	ESMLog(5,_T("Set AutoFocus Release"));	
	//-- 2013-09-04 hongsu@esmlab.com	
	m_wndDSCViewer.m_pDSCMgr->DoS1Release();	
}

void  CMainFrame::OnFocusSaveSeq(void)		
{
	ESMLog(5,_T("Focus Save Seq"));	

	CFocusFileSaveDlg dlg;
	int result = dlg.DoModal(); 
	if(result == IDOK)
		StartFocusValueToFile();
	else if(result == IDABORT)
		EndFocusValueToFile();
}

void  CMainFrame::OnDSCAdjust(void)		
{
	ESMSetMakeAdjust(TRUE);

	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
	{
		ESMLog(5,_T("DSC Adjust... [OnDSCAdjust ESM_ADJ_FILM_STATE_MOVIE]"));
		ESMAdjustMgrDlg AdjustMgrDlg;

		CObArray arDSCList;
		ESMGetDSCList(&arDSCList);
		CDSCItem* pItem = NULL;
		for( int i =0 ;i < arDSCList.GetSize(); i++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(i);
			AdjustMgrDlg.AddDscInfo(pItem->GetDeviceDSCID(), pItem->m_strInfo[DSC_INFO_LOCATION],
									pItem->m_bReverse);
		}
		if( AdjustMgrDlg.DoModal()== IDOK)
		{
			if(AdjustMgrDlg.m_bAdjSave == FALSE)
			{
				vector<DscAdjustInfo*>* pArrDscInfo = NULL;
				AdjustMgrDlg.GetDscInfo(&pArrDscInfo);

				m_wndDSCViewer.SetDscAdjustData(pArrDscInfo);

				// Margin 계산
				m_wndDSCViewer.SetMargin(pArrDscInfo);

				int nMarginX = m_wndDSCViewer.GetMarginX();
				int nMarginY = m_wndDSCViewer.GetMarginY();

				//-- 2014-07-15 hjjang
				//-- Set Adjust Info
				ESMEvent* pMsg = new ESMEvent;
				pMsg->message = WM_ESM_MOVIE_SET_ADJUSTINFO;
				pMsg->nParam1 = nMarginX;
				pMsg->nParam2 = nMarginY;
				::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
			}
		}
	}

	else if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
	{
		ESMLog(5,_T("DSC Adjust... [OnDSCAdjust ESM_ADJ_FILM_STATE_PICTURE]"));
		ESMAdjustMgrDlg AdjustMgrDlg;

		CObArray arDSCList;
		ESMGetDSCList(&arDSCList);
		CDSCItem* pItem = NULL;
		AdjustMgrDlg.SetMovieState(ESM_ADJ_FILM_STATE_PICTURE);
		for( int i =0 ;i < arDSCList.GetSize(); i++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(i);
			AdjustMgrDlg.AddDscInfo(pItem->GetDeviceDSCID(), _T(""));
		}
		if( AdjustMgrDlg.DoModal())
		{
			vector<DscAdjustInfo*>* pArrDscInfo = NULL;
			AdjustMgrDlg.GetDscInfo(&pArrDscInfo);

			m_wndDSCViewer.SetDscAdjustData(pArrDscInfo);

			// Margin 계산
			m_wndDSCViewer.SetMargin(pArrDscInfo);

			int nMarginX = m_wndDSCViewer.GetMarginX();
			int nMarginY = m_wndDSCViewer.GetMarginY();

			ESMEvent* pMsg = new ESMEvent;
			pMsg->nParam1 = nMarginX;
			pMsg->nParam2 = nMarginY;
			pMsg->message = WM_ESM_MOVIE_SET_ADJUSTINFO;
			::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}
		//m_wndDSCViewer.DoAdjust();	
	}
}

void  CMainFrame::OnDSCSyncTime(void)
{
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL, 0, DSCSyncTimeThread, (void *)this, 0, NULL);
	CloseHandle(hSyncTime);
}

unsigned WINAPI CMainFrame::DSCSyncTimeThread(LPVOID param)
{	
	CMainFrame* pMainFrm = (CMainFrame*)param;
	pMainFrm->DscSyncTime();
	return 0;
}
unsigned WINAPI CMainFrame::RecordDoneThread(LPVOID param)
{
	// 	CMainFrame* pMainFrm = (CMainFrame*)param;
	// 	pMainFrm->m_bMovieSaveDone = FALSE;
	// 	CDSCItem* pItem = NULL;
	// 	CObArray arDSCList;
	// 	ESMGetDSCList(&arDSCList);
	// 	int nDscCount =  arDSCList.GetSize();
	// 	int nCaptureTime = ESMGetRecordingInfoInt(_T("SelectPointTime1"), 0);
	// 	int nOrderDirection = ESMGetRecordingInfoInt(_T("OrderDirection"), DSC_REC_DELAY_FORWARDDIRECTION);
	// 	CString strSelectDSC = ESMGetMiddleDSCSearch(nOrderDirection); // 파일 유무까지 확인.
	// 	int nSelectDsc = ESMGetDSCIndex(strSelectDSC);
	// 	pMainFrm->m_wndDSCViewer.m_pFrameSelector->m_nSelectLineDSC = _T("");		// 초기화
	// 
	// 	// 2. Movie Save Confirm & ImageLoader
	// 	pMainFrm->m_pImgLoader->AllClearImageList();
	// 
	// 	int nReadyCount = 0, nRepeatCount = 400;
	// 	for( int nRepeatIndex =0 ;nRepeatIndex < nRepeatCount; nRepeatIndex++)
	// 	{
	// 		for( int i =0 ;i < nDscCount; i++)
	// 		{
	// 			pItem = (CDSCItem*)arDSCList.GetAt(i);
	// 			if( nSelectDsc == i )
	// 			{
	// 				//Image Load할 DSC 위치 조정
	// 				if( pItem->GetDSCStatus() == SDI_STATUS_DISCONNECT || pItem->GetCaptureExcept() || pItem->GetSensorSync() == FALSE)
	// 				{
	// 					nSelectDsc++;
	// 					continue;
	// 				}
	// 			}
	// 
	// 			if(pItem->GetRecordReady() || pItem->GetDSCStatus() == SDI_STATUS_DISCONNECT || pItem->GetCaptureExcept())
	// 			{
	// 				//Image loader 데이터 셋팅
	// 				if( nSelectDsc == i && pMainFrm->m_wndDSCViewer.m_pFrameSelector->m_nSelectLineDSC == _T(""))	
	// 				{
	// 					//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가 선택 포인터에서 위/아래 DSC의 10Frame 저장
	// 
	// 					if(nSelectDsc >= 0)
	// 					{
	// 						if( nSelectDsc >= nDscCount)
	// 							continue;
	// 
	// 						CDSCItem* pSelectItem = (CDSCItem*)arDSCList.GetAt(nSelectDsc);
	// 						if(pSelectItem)
	// 						{
	// 							int nSelectFIndex = ESMGetFrameIndex(nCaptureTime);
	// 							if(nSelectFIndex < 0)
	// 								nSelectFIndex = 0;
	// 							int nStartFIdx = nSelectFIndex - 10;
	// 							if(nStartFIdx < 0) nStartFIdx = 0;
	// 							int nEndFIdx = nSelectFIndex + 10;
	// 							pMainFrm->m_pImgLoader->SetImageFromMovie(pSelectItem->GetDeviceDSCID(), nStartFIdx, nEndFIdx);
	// 							ESMLog(5, _T("SetImageFromMovie[%s], nStartFIdx[%d], nStartFIdx[%d]"), pSelectItem->GetDeviceDSCID(), nStartFIdx, nEndFIdx);
	// 							pMainFrm->m_wndDSCViewer.m_pFrameSelector->m_nSelectLineDSC = pSelectItem->GetDeviceDSCID();
	// 						}
	// 					}
	// 				}
	// 				nReadyCount++;
	// 			}
	// 
	// 		}
	// 		Sleep(10);
	// 
	// 		if( nReadyCount == nDscCount)
	// 			break;
	// 		nReadyCount = 0;
	// 	}
	// 	pMainFrm->m_bMovieSaveDone = TRUE;
	// 	if( nReadyCount == nDscCount)	//Done
	// 	{
	// 		ESMLog(5, _T("Record Done"));
	// 
	// 		// 파일 복사 종료
	// 		if(pMainFrm->m_bReStartFalg == TRUE && ESMGetValue(ESM_VALUE_SERVERMODE) != ESM_SERVERMODE_MAKING)
	// 		{
	// 			ESMLog(5, _T("Pause Restart"));
	// 			pMainFrm->m_bReStartFalg = FALSE;
	// 			pMainFrm->OnDSCMoviePause();
	// 		}
	// 	}
	// 	else
	// 	{
	// 		for( int i =0 ;i < nDscCount; i++)
	// 		{
	// 			pItem = (CDSCItem*)arDSCList.GetAt(i);
	// 			if( pItem == NULL)
	// 				continue;
	// 
	// 			if(!pItem->GetRecordReady())
	// 			{
	// 			//	pItem->SetCaptureExcept(TRUE);
	// 				ESMLog(5, _T("Record Done Fail[%s][%s]"), pItem->GetDSCInfo(DSC_INFO_LOCATION), pItem->GetDeviceDSCID());
	// 			}
	// 
	// 			int nSelectFIndex = ESMGetFrameIndex(nCaptureTime);
	// 			// 파일 로드가 실패했을경우 파일 유무에 따른 Image Load 결정
	// 			if(pMainFrm->m_wndDSCViewer.m_pFrameSelector->m_nSelectLineDSC == _T(""))
	// 			{
	// 				if(i == nSelectDsc && pMainFrm->m_pImgLoader->IsMovieFile(nSelectDsc, nSelectFIndex) && pItem->GetRecordReady())
	// 				{
	// 					// 이미지 로드
	// 					if(nSelectFIndex < 0)
	// 						nSelectFIndex = 0;
	// 					int nStartFIdx = nSelectFIndex - 10;
	// 					if(nStartFIdx < 0) nStartFIdx = 0;
	// 					int nEndFIdx = nSelectFIndex + 10;
	// 
	// 					//ESMLog(5, _T("ImageLoader [ %s ] - StartIndex [%d], EndIndex[%d]"), pItem->GetDeviceDSCID(), nStartFIdx, nEndFIdx);
	// 					pMainFrm->m_pImgLoader->SetImageFromMovie(pItem->GetDeviceDSCID(), nStartFIdx, nEndFIdx);
	// 					ESMLog(5, _T("SetImageFromMovie[%s], nStartFIdx[%d], nStartFIdx[%d]"), pItem->GetDeviceDSCID(), nStartFIdx, nEndFIdx);
	// 
	// 					pMainFrm->m_wndDSCViewer.m_pFrameSelector->m_nSelectLineDSC = pItem->GetDeviceDSCID();
	// 					break;
	// 				}
	// 				else if(i == nSelectDsc)
	// 					nSelectDsc++;
	// 			}
	// 		}
	// 	}
	// 	// 1. Sensor Sync Setting
	// 	if (ESMGetValue(ESM_VALUE_SERVERMODE) != ESM_SERVERMODE_RECORDING)
	// 	{
	// 		//위치 지정
	// 		//pMainFrm->MakeShortCut(_T('4'));
	// 
	// 		//Movie Load 알림
	// 		nDscCount =  arDSCList.GetSize();
	// 		for( int i =0 ;i < nDscCount; i++)
	// 		{
	// 			pItem = (CDSCItem*)arDSCList.GetAt(i);
	// 			if(pItem)
	// 				pItem->SetDSCStatus(SDI_STATUS_REC_FINISH);
	// 		}
	// 	}


	CMainFrame* pMainFrm = (CMainFrame*)param;
	//pMainFrm->m_bMovieSaveDone = FALSE;
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	int nDscCount =  arDSCList.GetSize();
	try
	{
		pMainFrm->m_wndDSCViewer.m_pFrameSelector->m_nSelectLineDSC = _T("");		// 초기화
	}
	catch(...)
	{
		ESMLog(0,_T("WHAT THE FUCK"));
	}
	
	//(CAMREVISION)
	/*int nReadyCount = 0, nRepeatCount = 400;
	for( int nRepeatIndex =0 ;nRepeatIndex < nRepeatCount; nRepeatIndex++)
	{
		for( int i =0 ;i < nDscCount; i++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(i);
			if(pItem->GetRecordReady() || pItem->GetDSCStatus() == SDI_STATUS_DISCONNECT || pItem->GetCaptureExcept())
				nReadyCount++;
		}
		Sleep(5);

		if( nReadyCount == nDscCount)
			break;
		nReadyCount = 0;
	}
	//pMainFrm->m_bMovieSaveDone = TRUE;
	if( nReadyCount == nDscCount)	//Done
	{
		ESMLog(5, _T("Record Done"));
		// 파일 복사 종료
		if(pMainFrm->m_bReStartFalg == TRUE && ESMGetValue(ESM_VALUE_SERVERMODE) != ESM_SERVERMODE_MAKING)
		{
			ESMLog(5, _T("Pause Restart"));
			pMainFrm->m_bReStartFalg = FALSE;
			pMainFrm->OnDSCMoviePause();
		}
	}*/
	// 1. Sensor Sync Setting
	//disconnect camera no change
	if (ESMGetValue(ESM_VALUE_SERVERMODE) != ESM_SERVERMODE_RECORDING)
	{
		//Movie Load 알림
		nDscCount =  arDSCList.GetSize();
		for( int i =0 ;i < nDscCount; i++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(i);
			if(pItem)
				pItem->SetDSCStatus(SDI_STATUS_REC_FINISH);
		}
	}
	//pMainFrm->m_wndNetwork.StartCheckToAgentAlive();
	//(CAMREVISION)
	ESMLog(1,_T("CAMREVISION######### Record Done Thread End!!!!!!!!!! ##########"));
	return 0;
}

BOOL CMainFrame::DscGH5Initialize()
{
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_RS_RC_PRE_REC_INIT_GH5;
	::SendMessage(ESMGetMainWnd() , WM_ESM, WM_ESM_DSC, (LPARAM)pMsg);

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	if(arDSCList.GetCount())
	{
		for( int i = 0 ;i < arDSCList.GetCount(); i++)
		{
			int nWaitCount = 300;
			CDSCItem* pItem = NULL;
			pItem	= (CDSCItem*)arDSCList.GetAt(i);

			if (pItem == NULL)
				continue;

			// GH5
			if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
			{
				BOOL bPreRecInit = pItem->GetInitPreRec();
				while(nWaitCount--)
				{
					if (bPreRecInit)
						break;

					Sleep(5);
				}
			}				
		}
	}

	if(arDSCList.GetCount())
	{
		for( int i = 0 ;i < arDSCList.GetCount(); i++)
		{
			CDSCItem* pItem = NULL;
			pItem	= (CDSCItem*)arDSCList.GetAt(i);

			if (pItem == NULL)
				continue;

			// GH5
			if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
			{
				BOOL bPreRecInit = pItem->GetInitPreRec();

				if (bPreRecInit == FALSE)
					return FALSE;
			}				
		}
	}

	return TRUE;
}

int CMainFrame::DscSyncTime()
{
	//PC Sync Array
	m_arrErrPCSync.clear();

	m_FileMgr->SetCurTime();
	m_wndDSCViewer.m_pFrameSelector->SetInitRecTime();
	if( m_TimeVieDlg != NULL)
	{
		m_TimeVieDlg->InitTime();
		m_TimeVieDlg->ShowWindow(TRUE);
	}

	if(!ESMGetValue(ESM_VALUE_NET_MODE))
	{
		//-- 2013-10-28
		//-- Camera Synchronization before Movie Capture
		//-- Sync 1 Server <-> Agent
		int nSyncCount = 300;
		m_wndDSCViewer.TickSyncAgent();

		//KT 전송 동기화 4DP Sync Time
		if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
		{
			if(ESMGetValue(ESM_VALUE_RTSP) == TRUE)
			{
				CESMRCManager* pRCMgr = NULL;

				int nCount = m_wndNetwork.m_arRCServerList.GetCount();

				while(nCount--)
				{
					pRCMgr = (CESMRCManager *)m_wndNetwork.m_arRCServerList.GetAt(nCount);

					if(!pRCMgr)
						continue;

					pRCMgr->m_nSyncMin = -1;
					pRCMgr->SyncTickCount(TRUE);
				}
			}
			else
			{
				CESMRCManager* pRCMgr = NULL;

				int nCount = m_wndNetworkEx.m_arRCServerList.GetCount();

				while(nCount--)
				{
					pRCMgr = (CESMRCManager *)m_wndNetworkEx.m_arRCServerList.GetAt(nCount);

					if(!pRCMgr)
						continue;

					pRCMgr->m_nSyncMin = -1;
					pRCMgr->SyncTickCount(TRUE);
				}
			}
		}

		int nRCount =0;
		for( nRCount =0 ;nRCount < nSyncCount; nRCount++)
		{
			if(m_wndDSCViewer.GetPcSyncCheck())					
				break;
			Sleep(5);
		}

		if(ESMGetValue(ESM_VALUE_SYNCSKIP))
		{
			if( nRCount == nSyncCount)
			{
				CDSCGroup* pGroup = NULL;
				int nAll = m_wndDSCViewer.GetGroupCount();	
				for(int nRoop = 0; nRoop < nAll; nRoop++)
				{
					pGroup = m_wndDSCViewer.GetGroup(nRoop);
					if(pGroup->GetRCMgr()->m_bPCSyncTime == FALSE)
					{
						ESMLog(5,_T("PC Sync Fail Group[%d]"), pGroup->m_nIP);
						m_arrErrPCSync.push_back(nRoop);
					}
				}


				AfxMessageBox(_T("PC Sync Time Out!!"));
				m_TimeVieDlg->ShowWindow(FALSE);
				//return 0;
			}
		}
		else
		{
			if( nRCount == nSyncCount)
			{
				CDSCGroup* pGroup = NULL;
				int nAll = m_wndDSCViewer.GetGroupCount();	
				for(int nRoop = 0; nRoop < nAll; nRoop++)
				{
					pGroup = m_wndDSCViewer.GetGroup(nRoop);
					if(pGroup->GetRCMgr()->m_bPCSyncTime == FALSE)
					{
						ESMLog(5,_T("PC Sync Fail Group[%d]"), pGroup->m_nIP);
						m_arrErrPCSync.push_back(nRoop);
					}
				}


				AfxMessageBox(_T("PC Sync Time Out!!"));
				m_TimeVieDlg->ShowWindow(FALSE);
				return 0;
			}
		}
	}

	if(m_arrErrPCSync.size() == 0)
		ESMLog(5,_T("PC Sync Success"));

	CDSCGroup* pGroup = NULL;
	int nAll = m_wndDSCViewer.GetGroupCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pGroup =  m_wndDSCViewer.GetGroup(i);
		if(!pGroup)
			continue;
		pGroup->SetSync(FALSE);
	}

	//-- Sync 2 PC -> Camera
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_RS_RC_CAMERA_SYNC;
	::SendMessage(ESMGetMainWnd() , WM_ESM, WM_ESM_DSC, (LPARAM)pMsg);

	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection "));
		m_TimeVieDlg->ShowWindow(FALSE);
		return 0;
	}

	BOOL bFinish;
	int nCnt = 0;
	while(1)
	{
		bFinish = TRUE;
		Sleep(10);

		for(int i = 0 ; i < nAll ; i ++)
		{
			pGroup =  m_wndDSCViewer.GetGroup(i);
			if(!pGroup)
				continue;
			if(!pGroup->GetSync())
			{
				bFinish = FALSE;
				break;
			}
		}
		if(bFinish)
		{
			int nAllDSC = 0;
			int nGroupDSC = 0;
			int nSyncDSC = 0;
			CDSCItem* pItem = NULL;
			for(int i = 0 ; i < nAll ; i ++)
			{
				pGroup =  m_wndDSCViewer.GetGroup(i);
				if(!pGroup)
					continue;

				//-- Count All Dsc 
				nGroupDSC = pGroup->m_arDSC.GetSize();
				nAllDSC += nGroupDSC;
				//-- Count Synced DSC
				while(nGroupDSC--)
				{
					pItem = pGroup->GetDSC(nGroupDSC);
					if(pItem)
					{
						if(!pItem->GetTickStatus())
							ESMLog(0,_T("[DSC] DSC disconnect [%s]"), pItem->GetDeviceDSCID());
						else if(pItem->GetCaptureExcept())
							ESMLog(0,_T("[DSC] DSC Sync Error [%s]"), pItem->GetDeviceDSCID());
						else
							nSyncDSC++;
							
					}
				}
			}

			//-- Show Sync Infomation
			ESMLog(1,_T("+++++++++[DSC] DSC Sync [%d/%d]+++++++++++"), nSyncDSC, nAllDSC);
			ESMSetRTSPCommand(RTSP_COMMAND_RECORD_RESUME,nSyncDSC);
			break;
		}

		//-- 2014-07-22 hjjang
		//-- Wait Up to 10Sec until Receive SyncResult Message from Client
		//if (nCnt++>400)  //DSC Sync Option
		int nDSCSyncCnt = ESMGetValue(ESM_VALUE_DSC_SYNC_COUNT);
		//ESMLog(1, _T("Get DSC Sync Count = %d"), nDSCSyncCnt);
		if (nCnt++>nDSCSyncCnt)  //DSC Sync Option
		{
			//-- 2016-05-19 hongsu 
			int nAllDSC = 0;
			int nGroupDSC = 0;
			int nSyncDSC = 0;
			CDSCItem* pItem = NULL;

			/*
			ESMLog(1,_T("[DSC] DSC Sync Failed"));
			//AfxMessageBox(_T("DSC Sync Failed!"));
			m_TimeVieDlg->ShowWindow(FALSE);
			*/

			for(int i = 0 ; i < nAll ; i ++)
			{
				pGroup =  m_wndDSCViewer.GetGroup(i);
				if(!pGroup)
					continue;

				//-- Count All Dsc 
				nGroupDSC = pGroup->m_arDSC.GetSize();
				nAllDSC += nGroupDSC;
				//-- Count Synced DSC
				if(!pGroup->GetSync())
				{
					ESMLog(0, _T("[#1]Group Sync Failed : %d"), pGroup->m_nIP);						
					continue;
				}
				while(nGroupDSC--)
				{
					pItem = pGroup->GetDSC(nGroupDSC);
					if(pItem)
					{
						if(pItem->GetTickStatus())
							nSyncDSC++;
						else
							ESMLog(0,_T("[DSC] DSC Fail [%s]"), pItem->GetDeviceDSCID());
					}
				}
			}

			//-- Show Sync Infomation
			ESMLog(1,_T("+++++++++[DSC] DSC Sync [%d/%d]+++++++++++"), nSyncDSC, nAllDSC);
			ESMSetRTSPCommand(RTSP_COMMAND_RECORD_RESUME,nSyncDSC);

			//-- 2016-05-19 hongsu
			//-- Check Sync DSC
			float fSyncPercentage = 0;
			fSyncPercentage = nSyncDSC / nAllDSC;
			/*if(fSyncPercentage < temp_sync_percentage)
			{
				ESMLog(1,_T("[DSC] DSC Sync Failed"));
				AfxMessageBox(_T("DSC Sync Failed!"));
				m_TimeVieDlg->ShowWindow(FALSE);
				return 0;
			}*/

			
			if(!pGroup->GetSync())
			{
				ESMLog(0, _T("[#2]Group Sync Failed : %d"), pGroup->m_nIP);
			}			

			m_TimeVieDlg->ShowWindow(FALSE);
			return 1;
		}
	}

	ESMLog(5,_T("DSC Sync Success"));

	m_TimeVieDlg->ShowWindow(FALSE);
	return 1;
}

int CMainFrame::DscSyncTime2()
{
	int nStart,nEnd;
	//PC Sync Array
	m_arrErrPCSync.clear();

	//m_FileMgr->SetCurTime();
	m_wndDSCViewer.m_pFrameSelector->SetInitRecTime();
	if( m_TimeVieDlg != NULL)
	{
		m_TimeVieDlg->InitTime();
		m_TimeVieDlg->SetSeq1(_T("Sync Time Adjusting..."));
		m_TimeVieDlg->ShowWindow(TRUE);
		nStart = GetTickCount();
		if(ESMGetValue(ESM_VALUE_TEST_MODE) == TRUE)
			ESMLog(5,_T("[TC4] Start"));
	}

	CDSCGroup* pGroup = NULL;
	int nAll = m_wndDSCViewer.GetGroupCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pGroup =  m_wndDSCViewer.GetGroup(i);
		if(!pGroup)
			continue;
		pGroup->SetSync(FALSE);
	}

	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection "));
		m_TimeVieDlg->ShowWindow(FALSE);
		return 0;
	}

	//-- Sync 2 PC -> Camera
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_RS_RC_CAMERA_SYNC;
	::SendMessage(ESMGetMainWnd() , WM_ESM, WM_ESM_DSC, (LPARAM)pMsg);


	if(!ESMGetValue(ESM_VALUE_NET_MODE))
	{
		//-- 2013-10-28
		//-- Camera Synchronization before Movie Capture
		//-- Sync 1 Server <-> Agent
		//int nSyncCount = 300;
		

		m_wndDSCViewer.TickSyncAgent();

		int nStartTime = ESMGetTick();

		//KT 전송 동기화 4DP Sync Time
		if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
		{
			if(ESMGetValue(ESM_VALUE_RTSP) == TRUE)
			{
				/*CESMRCManager* pRCMgr = NULL;

				int nCount = m_wndNetwork.m_arRCServerList.GetCount();

				while(nCount--)
				{
					pRCMgr = (CESMRCManager *)m_wndNetwork.m_arRCServerList.GetAt(nCount);

					if(!pRCMgr)
						continue;

					pRCMgr->m_nSyncMin = -1;
					pRCMgr->SyncTickCount(TRUE);
				}*/
			}
			else
			{
				CESMRCManager* pRCMgr = NULL;

				int nCount = m_wndNetworkEx.m_arRCServerList.GetCount();

				while(nCount--)
				{
					pRCMgr = (CESMRCManager *)m_wndNetworkEx.m_arRCServerList.GetAt(nCount);

					if(!pRCMgr)
						continue;

					pRCMgr->m_nSyncMin = -1;
					pRCMgr->SyncTickCount(TRUE);
				}
			}
		}

		int nRCount =0;
		BOOL bPCSync = TRUE;
		//for( nRCount =0 ;nRCount < nSyncCount; nRCount++)
		while(1)
		{
			int nEndTime = ESMGetTick();
			int nGap = nEndTime - nStartTime;
			if(nGap > (ESMGetValue(ESM_VALUE_PC_SYNC_TIME) + 5000)) //500ms 추가
			{
				if(!m_wndDSCViewer.GetPcSyncCheck())
					bPCSync = FALSE;
				
				break;
			}

			/*if(m_wndDSCViewer.GetPcSyncCheck())					
				break;*/

			Sleep(5);
		}

		
		//if( nRCount == nSyncCount)
		if(!bPCSync)
		{
			CDSCGroup* pGroup = NULL;
			int nAll = m_wndDSCViewer.GetGroupCount();	
			for(int nRoop = 0; nRoop < nAll; nRoop++)
			{
				pGroup = m_wndDSCViewer.GetGroup(nRoop);
				if(pGroup->GetRCMgr()->m_bPCSyncTime == FALSE)
				{
					ESMLog(5,_T("PC Sync Fail Group[%d]"), pGroup->m_nIP);
					m_arrErrPCSync.push_back(nRoop);
				}
			}

			if(ESMGetValue(ESM_VALUE_SYNCSKIP) == FALSE)
			{
				AfxMessageBox(_T("PC Sync Time Out_!!"));
				m_TimeVieDlg->ShowWindow(FALSE);
				return 0;
			}
			else
			{
				ESMLog(5,  _T("### [SYNC SKIP] PC Sync Time Out!!"));
				m_TimeVieDlg->ShowWindow(FALSE);
			}
		}
		
	}

	if(m_arrErrPCSync.size() == 0)
		ESMLog(5,_T("PC Sync Success"));

	

	BOOL bFinish;
	int nStartTick = ESMGetTick();
	int nWaitTime = ESMGetValue(ESM_VALUE_DSC_WAIT_TIME);
	int nSyncTime = ESMGetValue(ESM_VALUE_DSC_SYNC_TIME);
	int nStopTime = 0;

	if(nWaitTime > nSyncTime/*+20000*/)
		nStopTime = nWaitTime;
	else
		nStopTime = nSyncTime/*+20000*/;

	
	while(1)
	{
		bFinish = TRUE;
		Sleep(10);

		for(int i = 0 ; i < nAll ; i ++)
		{
			pGroup =  m_wndDSCViewer.GetGroup(i);
			if(!pGroup)
				continue;
			if(!pGroup->GetSync())
			{
				bFinish = FALSE;
				break;
			}
		}
		if(bFinish)
		{
			int nAllDSC = 0;
			int nGroupDSC = 0;
			int nSyncDSC = 0;
			CDSCItem* pItem = NULL;
			for(int i = 0 ; i < nAll ; i ++)
			{
				pGroup =  m_wndDSCViewer.GetGroup(i);
				if(!pGroup)
					continue;

				//-- Count All Dsc 
				nGroupDSC = pGroup->m_arDSC.GetSize();
				nAllDSC += nGroupDSC;
				//-- Count Synced DSC
				while(nGroupDSC--)
				{
					pItem = pGroup->GetDSC(nGroupDSC);
					if(pItem)
					{
						if(!pItem->GetTickStatus())
							ESMLog(0,_T("[DSC] DSC disconnect [%s]"), pItem->GetDeviceDSCID());
						else if(pItem->GetCaptureExcept())
							ESMLog(0,_T("[DSC] DSC Sync Error [%s]"), pItem->GetDeviceDSCID());
						else if(pItem->GetCaptureImageRecExcept())
							ESMLog(0,_T("[DSC] DSC Image Rec Result Error [%s]"), pItem->GetDeviceDSCID());
						else
							nSyncDSC++;
							
					}
				}
			}

			//-- Show Sync Infomation
			ESMLog(1,_T("+++++++++[DSC] DSC Sync [%d/%d]+++++++++++"), nSyncDSC, nAllDSC);
			ESMSetRTSPCommand(RTSP_COMMAND_RECORD_RESUME,nSyncDSC);
			break;
		}

		int nTime = ESMGetTick();
		
		if (nStopTime < nTime-nStartTick)
		{
			//-- 2016-05-19 hongsu 
			int nAllDSC = 0;
			int nGroupDSC = 0;
			int nSyncDSC = 0;
			CDSCItem* pItem = NULL;

			/*
			ESMLog(1,_T("[DSC] DSC Sync Failed"));
			//AfxMessageBox(_T("DSC Sync Failed!"));
			m_TimeVieDlg->ShowWindow(FALSE);
			*/

			for(int i = 0 ; i < nAll ; i ++)
			{
				pGroup =  m_wndDSCViewer.GetGroup(i);
				if(!pGroup)
					continue;

				//-- Count All Dsc 
				nGroupDSC = pGroup->m_arDSC.GetSize();
				nAllDSC += nGroupDSC;
				//-- Count Synced DSC
				if(!pGroup->GetSync())
				{
					ESMLog(0, _T("[#3]Group Sync Failed : %d"), pGroup->m_nIP);						
					continue;
				}
				while(nGroupDSC--)
				{
					pItem = pGroup->GetDSC(nGroupDSC);
					if(pItem)
					{
						if(pItem->GetTickStatus())
							nSyncDSC++;
						else
							ESMLog(0,_T("[DSC] DSC Fail [%s]"), pItem->GetDeviceDSCID());
					}
				}
			}

			//-- Show Sync Infomation
			ESMLog(1,_T("+++++++++[DSC] DSC Sync [%d/%d]+++++++++++"), nSyncDSC, nAllDSC);
			ESMSetRTSPCommand(RTSP_COMMAND_RECORD_RESUME,nSyncDSC);
			//-- 2016-05-19 hongsu
			//-- Check Sync DSC
			float fSyncPercentage = 0;
			fSyncPercentage = nSyncDSC / nAllDSC;
			/*if(fSyncPercentage < temp_sync_percentage)
			{
				ESMLog(1,_T("[DSC] DSC Sync Failed"));
				AfxMessageBox(_T("DSC Sync Failed!"));
				m_TimeVieDlg->ShowWindow(FALSE);
				return 0;
			}*/

			
			if(!pGroup->GetSync())
			{
				ESMLog(0, _T("[#4]Group Sync Failed : %d"), pGroup->m_nIP);
			}			

			m_TimeVieDlg->ShowWindow(FALSE);
			return 1;
		}
	}

	ESMLog(5,_T("DSC Sync Success"));
	if(ESMGetValue(ESM_VALUE_TEST_MODE) == TRUE)
		ESMLog(5,_T("[TC4] End"));

	m_TimeVieDlg->ShowWindow(FALSE);
	nEnd = GetTickCount();

	ESMSetInitialTime(nEnd - nStart);

	return 1;
}

int CMainFrame::DscSyncTest()
{
	//PC Sync Array
	m_arrErrPCSync.clear();

	m_FileMgr->SetCurTime();
	m_wndDSCViewer.m_pFrameSelector->SetInitRecTime();
	if( m_TimeVieDlg != NULL)
	{
		m_TimeVieDlg->InitTime();
		m_TimeVieDlg->ShowWindow(TRUE);
	}

	if(!ESMGetValue(ESM_VALUE_NET_MODE))
	{
		//-- 2013-10-28
		//-- Camera Synchronization before Movie Capture
		//-- Sync 1 Server <-> Agent
		int nSyncCount = 300;
		m_wndDSCViewer.TickSyncAgent();
		int nRCount =0;
		for( nRCount =0 ;nRCount < nSyncCount; nRCount++)
		{
			if(m_wndDSCViewer.GetPcSyncCheck())					
				break;
			Sleep(5);
		}

		if(ESMGetValue(ESM_VALUE_SYNCSKIP))
		{
			if( nRCount == nSyncCount)
			{
				CDSCGroup* pGroup = NULL;
				int nAll = m_wndDSCViewer.GetGroupCount();	
				for(int nRoop = 0; nRoop < nAll; nRoop++)
				{
					pGroup = m_wndDSCViewer.GetGroup(nRoop);
					if(pGroup->GetRCMgr()->m_bPCSyncTime == FALSE)
					{
						ESMLog(5,_T("PC Sync Fail Group[%d]"), pGroup->m_nIP);
						m_arrErrPCSync.push_back(nRoop);
						pGroup->GetRCMgr()->m_nSyncFailCnt++;
					}
				}
				m_TimeVieDlg->ShowWindow(FALSE);
			}
		}
	}

	if(m_arrErrPCSync.size() == 0)
		ESMLog(5,_T("PC Sync Success"));

	CDSCGroup* pGroup = NULL;
	int nAll = m_wndDSCViewer.GetGroupCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pGroup =  m_wndDSCViewer.GetGroup(i);
		if(!pGroup)
			continue;
		pGroup->SetSync(FALSE);
	}

	//-- Sync 2 PC -> Camera
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_RS_RC_CAMERA_SYNC;
	::SendMessage(ESMGetMainWnd() , WM_ESM, WM_ESM_DSC, (LPARAM)pMsg);

	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection "));
		m_TimeVieDlg->ShowWindow(FALSE);
		return 0;
	}

	BOOL bFinish;
	int nCnt = 0;
	while(1)
	{
		bFinish = TRUE;
		Sleep(10);

		for(int i = 0 ; i < nAll ; i ++)
		{
			pGroup =  m_wndDSCViewer.GetGroup(i);
			if(!pGroup)
				continue;
			if(!pGroup->GetSync())
			{
				bFinish = FALSE;
				break;
			}
		}
		if(bFinish)
		{
			int nAllDSC = 0;
			int nGroupDSC = 0;
			int nSyncDSC = 0;
			CDSCItem* pItem = NULL;
			for(int i = 0 ; i < nAll ; i ++)
			{
				pGroup =  m_wndDSCViewer.GetGroup(i);
				if(!pGroup)
					continue;

				//-- Count All Dsc 
				nGroupDSC = pGroup->m_arDSC.GetSize();
				nAllDSC += nGroupDSC;
				//-- Count Synced DSC
				while(nGroupDSC--)
				{
					pItem = pGroup->GetDSC(nGroupDSC);
					if(pItem)
					{
						if(!pItem->GetTickStatus())
							ESMLog(0,_T("[DSC] DSC disconnect [%s]"), pItem->GetDeviceDSCID());
						else if(pItem->GetCaptureExcept())
							ESMLog(0,_T("[DSC] DSC Sync Error [%s]"), pItem->GetDeviceDSCID());
						else
							nSyncDSC++;
							
					}
				}
			}

			//-- Show Sync Infomation
			ESMLog(1,_T("+++++++++[DSC] DSC Sync [%d/%d]+++++++++++"), nSyncDSC, nAllDSC);
			ESMSetRTSPCommand(RTSP_COMMAND_RECORD_RESUME,nSyncDSC);
			break;
		}

		//-- 2014-07-22 hjjang
		//-- Wait Up to 10Sec until Receive SyncResult Message from Client
		//if (nCnt++>400)  //DSC Sync Option
		int nDSCSyncCnt = ESMGetValue(ESM_VALUE_DSC_SYNC_COUNT);
		//ESMLog(1, _T("Get DSC Sync Count = %d"), nDSCSyncCnt);
		if (nCnt++>nDSCSyncCnt)  //DSC Sync Option
		{
			//-- 2016-05-19 hongsu 
			int nAllDSC = 0;
			int nGroupDSC = 0;
			int nSyncDSC = 0;
			CDSCItem* pItem = NULL;

			/*
			ESMLog(1,_T("[DSC] DSC Sync Failed"));
			//AfxMessageBox(_T("DSC Sync Failed!"));
			m_TimeVieDlg->ShowWindow(FALSE);
			*/

			for(int i = 0 ; i < nAll ; i ++)
			{
				pGroup =  m_wndDSCViewer.GetGroup(i);
				if(!pGroup)
					continue;

				//-- Count All Dsc 
				nGroupDSC = pGroup->m_arDSC.GetSize();
				nAllDSC += nGroupDSC;
				//-- Count Synced DSC
				if(!pGroup->GetSync())
				{
					ESMLog(0, _T("[#5]Group Sync Failed : %d"), pGroup->m_nIP);						
					continue;
				}
				while(nGroupDSC--)
				{
					pItem = pGroup->GetDSC(nGroupDSC);
					if(pItem)
					{
						if(pItem->GetTickStatus())
							nSyncDSC++;
						else
							ESMLog(0,_T("[DSC] DSC Fail [%s]"), pItem->GetDeviceDSCID());
					}
				}
			}

			//-- Show Sync Infomation
			ESMLog(1,_T("+++++++++[DSC] DSC Sync [%d/%d]+++++++++++"), nSyncDSC, nAllDSC);
			ESMSetRTSPCommand(RTSP_COMMAND_RECORD_RESUME,nSyncDSC);

			//-- 2016-05-19 hongsu
			//-- Check Sync DSC
			float fSyncPercentage = 0;
			fSyncPercentage = nSyncDSC / nAllDSC;
			/*if(fSyncPercentage < temp_sync_percentage)
			{
				ESMLog(1,_T("[DSC] DSC Sync Failed"));
				AfxMessageBox(_T("DSC Sync Failed!"));
				m_TimeVieDlg->ShowWindow(FALSE);
				return 0;
			}*/

			
			if(!pGroup->GetSync())
			{
				ESMLog(0, _T("[#6]Group Sync Failed : %d"), pGroup->m_nIP);
			}			

			m_TimeVieDlg->ShowWindow(FALSE);
			return 1;
		}
	}

	ESMLog(5,_T("DSC Sync Success"));

	m_TimeVieDlg->ShowWindow(FALSE);
	return 1;
}

int CMainFrame::DscReSync()
{
	//ESMLog(5, _T("######## CMainFrame::DscReSync() .... "));

	int nStart,nEnd;
	//PC Sync Array
	m_arrErrPCSync.clear();

	//m_FileMgr->SetCurTime();
	//m_wndDSCViewer.m_pFrameSelector->SetInitRecTime();

	if(!ESMGetValue(ESM_VALUE_NET_MODE))
	{
		//-- 2013-10-28
		//-- Camera Synchronization before Movie Capture
		//-- Sync 1 Server <-> Agent
		//int nSyncCount = 300;
		

		m_wndDSCViewer.TickSyncAgent();

		int nStartTime = ESMGetTick();

		//KT 전송 동기화 4DP Sync Time
		if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
		{
			if(ESMGetValue(ESM_VALUE_RTSP) == TRUE)
			{
				CESMRCManager* pRCMgr = NULL;

				int nCount = m_wndNetwork.m_arRCServerList.GetCount();

				while(nCount--)
				{
					pRCMgr = (CESMRCManager *)m_wndNetwork.m_arRCServerList.GetAt(nCount);

					if(!pRCMgr)
						continue;

					pRCMgr->m_nSyncMin = -1;
					pRCMgr->SyncTickCount(TRUE);
				}
			}
			else
			{
				CESMRCManager* pRCMgr = NULL;

				int nCount = m_wndNetworkEx.m_arRCServerList.GetCount();

				while(nCount--)
				{
					pRCMgr = (CESMRCManager *)m_wndNetworkEx.m_arRCServerList.GetAt(nCount);

					if(!pRCMgr)
						continue;

					pRCMgr->m_nSyncMin = -1;
					pRCMgr->SyncTickCount(TRUE);
				}
			}
		}
	}
		return 1;
}

void CMainFrame::DscReSyncGetTick()
{
	int nAllItem = m_wndDSCViewer.GetItemCount();
	while(nAllItem--)
	{
		CDSCItem* pDscItem = m_wndDSCViewer.GetItemData(nAllItem);
		pDscItem->SetReSyncGetTickReady(FALSE);
	}

	CDSCGroup* pGroup = NULL;
	int nAll = m_wndDSCViewer.GetGroupCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pGroup =  m_wndDSCViewer.GetGroup(i);
		if(!pGroup)
			continue;
		pGroup->SetSync(FALSE);
	}

	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_RS_RC_GET_TICK_CMD;
	::SendMessage(ESMGetMainWnd() , WM_ESM, WM_ESM_DSC, (LPARAM)pMsg);
}
void  CMainFrame::OnDSCTakePicture(void)				
{
	HANDLE hMovieCapture = NULL;
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();	
	if (nDSCCnt < 1)
		return;

	pItem = (CDSCItem*)arDSCList.GetAt(0);

	if (!pItem)
		return;

	if (pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
	{
		hMovieCapture = (HANDLE) _beginthreadex(NULL, 0, DSCPictureCaptureThreadGH5, (void *)this, 0, NULL);
	}
	else
	{
		hMovieCapture = (HANDLE) _beginthreadex(NULL, 0, DSCPictureCaptureThread, (void *)this, 0, NULL);
	}

	CloseHandle(hMovieCapture);
}

unsigned WINAPI CMainFrame::DSCPictureCaptureThreadGH5(LPVOID param)
{
	ESMLog(5,_T("Take PIcture GH5"));

	CMainFrame* pMainFrm = (CMainFrame*)param;

	pMainFrm->m_wndTimeLineEditor.ItemAllDelete();

	// DSC Movie Mode Check
	ESMSetFilmState(ESM_ADJ_FILM_STATE_PICTURE);
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;

	pMainFrm->m_wndLiveView.OnLiveOff();

	if(!(pMainFrm->m_wndDSCViewer.m_pFrameSelector->GetDSCRecStatus() == DSC_REC_ON))
	{
		if( ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
		{
			//if( !pMainFrm->DscSyncTime() )
			//	return 0;			
			if(ESMGetValue(ESM_VALUE_PC_SYNC_TIME) > 100)
			{
				if( !pMainFrm->DscSyncTime2() )
					return 0;
			}
			else
			{
				if( !pMainFrm->DscSyncTime() )
					return 0;
			}
		}
		pMainFrm->m_FileMgr->SetCurTime();
		pMainFrm->m_wndDSCViewer.m_pFrameSelector->SetInitRecTime();
	}

	int nDelayMSec = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC);

	for( int i =0 ;i < arDSCList.GetSize(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);

		ESMEvent* pNewMsg	= new ESMEvent();
		pNewMsg->pDest		= (LPARAM)pItem;
		pNewMsg->nParam1	= nDelayMSec;	
		//pNewMsg->nParam2	= nDelayM2Sec;
		pNewMsg->message	= WM_ESM_NET_CAPTUREDELAY;
		::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);
	}

	pMainFrm->m_wndDSCViewer.m_pFrameSelector->SetInitRecTime();
	pMainFrm->m_FileMgr->SetCurTime();
	//-- 2013-09-04 hongsu@esmlab.com
	pMainFrm->m_wndDSCViewer.m_pDSCMgr->DoTakePicture();

	return 0;
}

unsigned WINAPI CMainFrame::DSCPictureCaptureThread(LPVOID param)
{
	CMainFrame* pMainFrm = (CMainFrame*)param;
	ESMLog(5,_T("Take PIcture ... (under construction)"));

	pMainFrm->m_wndTimeLineEditor.ItemAllDelete();
	// DSC Movie Mode Check
	ESMSetFilmState(ESM_ADJ_FILM_STATE_PICTURE);
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;
	/*for( int i =0 ;i < arDSCList.GetSize(); i++)
	{
	pItem = (CDSCItem*)arDSCList.GetAt(i);
	if( pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_MODE) != _T("M") && pItem->GetType() != DSC_LOCAL )
	{
	AfxMessageBox(_T("DSC Mode Check \nAll Dsc Change M Mode!"));
	return ;
	}
	}*/

	pMainFrm->m_wndLiveView.OnLiveOff();

	if(!(pMainFrm->m_wndDSCViewer.m_pFrameSelector->GetDSCRecStatus() == DSC_REC_ON))
	{
		if( ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
		{
			//if( !pMainFrm->DscSyncTime() )
			//	return 0;		

			if(ESMGetValue(ESM_VALUE_PC_SYNC_TIME) > 100)
			{
				if( !pMainFrm->DscSyncTime2() )
					return 0;
			}
			else
			{
				if( !pMainFrm->DscSyncTime() )
					return 0;
			}
		}
		pMainFrm->m_FileMgr->SetCurTime();
		pMainFrm->m_wndDSCViewer.m_pFrameSelector->SetInitRecTime();
	}
	//jhhan
#if _ORIGINAL
	int nDelayMSec = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC);
#else
	int nDelayMSec = 0;

	int nDelayMSel = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC_SEL);
	if(nDelayMSel == 0)      //ESM_VALUE_WAIT_SAVE_DSC
	{
		nDelayMSec = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC);
	}
	else if(nDelayMSel == 1) //ESM_VALUE_WAIT_SAVE_DSC_SEL
	{
		nDelayMSec = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC_SUB);
	}
	else					//Default
	{
		nDelayMSec = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC);
	}
	
#endif


	int nDelayM2Sec = 0;
	for( int i =0 ;i < arDSCList.GetSize(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);

		nDelayM2Sec = pMainFrm->m_wndDSCViewer.GetDelayTime(pItem->GetDeviceDSCID());
		ESMEvent* pNewMsg	= new ESMEvent();
		pNewMsg->pDest		= (LPARAM)pItem;
		pNewMsg->nParam1	= i * nDelayMSec;		//순차 Delay
		pNewMsg->nParam2	= nDelayM2Sec;			//File Load Delay Time
		pNewMsg->message	= WM_ESM_NET_CAPTUREDELAY;
		::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);
	}

	pMainFrm->m_wndDSCViewer.m_pFrameSelector->SetInitRecTime();
	pMainFrm->m_FileMgr->SetCurTime();
	//-- 2013-09-04 hongsu@esmlab.com
	pMainFrm->m_wndDSCViewer.m_pDSCMgr->DoTakePicture();
	pMainFrm->m_FileMgr->PictureDoneSaveProfile();
	return 0;
}

unsigned WINAPI CMainFrame::OnDSCReMovieCapture(LPVOID param)
{
	int nCount = 0;
	CMainFrame* pMain = (CMainFrame*)param;
	while(1)
	{
		Sleep(1000);
		nCount++;

		if(nCount == 10)
		{
			ESMLog(5, _T("First Movie Capture Stop."));
			pMain->DSCMovieCapture();
		}
		else if(nCount > 15)
		{
			ESMLog(5, _T("Re Movie Caputure...."));
			pMain->DSCMovieCapture();
			break;
		}
	}
	return 0;
}

//jhhan 16-09-06
unsigned WINAPI CMainFrame::OnDSCReConnect(LPVOID param)
{
	int nCount = 0;
	CMainFrame* pMain = (CMainFrame*)param;
	while(1)
	{
		Sleep(1000);
		nCount++;

		if(nCount == 2)
		{
			ESMLog(5, _T("Reconnect"));
			pMain->m_wndNetwork.DoConnectionAll();
		}
		else if(nCount > 5)
		{
			ESMLog(5, _T("Movie Caputure...."));
			pMain->DSCMovieCapture();
			break;
		}
	}
	return 0;
}

void CMainFrame::OnDSCTesting()
{
	HANDLE hMovieCapture = NULL;
	hMovieCapture = (HANDLE) _beginthreadex(NULL, 0, DSCTestingThread, (void *)this, 0, NULL);
	CloseHandle(hMovieCapture);
}

void CMainFrame::OnDSCMovieCapture(void)				
{
	if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING && ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		ESMSetRecordSync();
	}

	if(OnResync_RecStart() == FALSE)
		MakeShortCut('1');
}

void CMainFrame::DSCMovieCapture(void)				
{
	//joonho.kim Exception 초기화
	CDSCItem* pItem = NULL;
	CDSCGroup* pGroup = NULL;
	int nGroupDSC, nAllDSC = 0;
	int nAll = m_wndDSCViewer.GetGroupCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pGroup =  m_wndDSCViewer.GetGroup(i);
		if(!pGroup)
			continue;

		//-- Count All Dsc 
		nGroupDSC = pGroup->m_arDSC.GetSize();
		nAllDSC += nGroupDSC;
		//-- Count Synced DSC
		while(nGroupDSC--)
		{
			pItem = pGroup->GetDSC(nGroupDSC);
			if(pItem)
			{
				pItem->SetCaptureExcept(FALSE);
				
				// gh5 pre rec init, gh5 외에 영향 없으므로 따로 분기 필요 없음 //  [2018/5/28/ stim]
				pItem->SetInitPreRec(FALSE);
			}
		}
	}


	//jhhan 16-09-06
	if(ESMGetLoadRecord())
	{
#if _USE_AUTOCONNECT				//jhhan 초안 자동재접속 5초 로직 현재 제외
		ESMSetLoadRecord(FALSE);
		
		//AfxMessageBox(_T("Load Record 이후 촬영시 5초후 촬영진행됩니다."));
		m_wndNetwork.DisConnectAll();
		ESMSetExecuteMode(TRUE);
		
		/*HANDLE hHandle = NULL;
		hHandle = (HANDLE) _beginthreadex(NULL, 0, OnDSCReConnect, this, 0, NULL);
		CloseHandle(hHandle);	*/

		SetTimer(TM_DSC_DISCONNECT, TM_GAP_DSC_DISCONNECT, NULL);
		
		ESMLog(5, _T("Load Record.... Wait 5sec"));
#else //jhhan 16-09-08
		AfxMessageBox(_T("Reconnection is required after Load Record."));
		CString strText;
		strText.LoadString(IDR_MAINFRAME);
		SetWindowText(strText);
		ESMReloadCamList();
		//m_wndDSCViewer.CameraListLoad();
		return;
		//ESMSetLoadRecord(FALSE);	//jhhan 16-09-19
		ESMReloadCamList();
		ESMSetExecuteMode(TRUE); //jhhan 16-09-12
#endif
	}

	if (ESMGetDevice() == SDI_MODEL_NX1)
	{
		// nx1
		ESMSetMovieGOPMode();
	}

	if( m_wndDSCViewer.m_pFrameSelector->GetDSCRecStatus() != DSC_REC_ON )
	{
		if(!CheckConcert())
			return;
	}
#if CMiLRe0
	if( m_bMovieSaveDone == FALSE)
	{
		ESMLog(5, _T("Not yet finished saving Movie."));
		return ;
	}
#endif
#ifdef _4DMODEL
	return;
#endif

	//jhhan 16-11-18 //촬영시 Adjust 기본값 로드
	//if(ESMGetGPUMakeFile() && ESMGetAdjustLoad() == FALSE)
	//jhhan 190125 - 메모리해제 이슈 임시 제거
	/*{
	ESMLog(5,_T("Send AdjustData!"));
	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_VIEW_SETNEWESTADJ;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	}*/
	
	ESMLog(1, _T("Nomal Mode Capture"));
	m_wndDSCViewer.m_bPauseMode = FALSE;
	if( m_wndDSCViewer.m_bRecordStep == ESM_NOMAL_START)
	{
		m_wndDSCViewer.m_bRecordStep = ESM_RECORD_STOP;
		m_wndNetwork.GetClientDiskSize();
	}
	else if( m_wndDSCViewer.m_bRecordStep == ESM_RECORD_STOP)
	{
		ESMInitViewPointNum();
		ESMInitSelectPointNum();

		//jhhan 16-11-21 LastPointNum
		ESMInitLastSelectPointNum();

		m_wndDSCViewer.m_bRecordStep = ESM_NOMAL_START;
	}

	//jhhan 16-10-11
	//MakingDeleteList
	ESMGetDeleteDSC();	//m_arrDeleteDSC,m_arrMakeDeleteDSC

	//m_wndTimeLineProcessor.DeleteProcessor();

	if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
		AfxMessageBox(_T("Current server mode is video production mode. \n Available in Record or Both mode only."));
	else
	{
		HANDLE hMovieCapture = NULL;
		hMovieCapture = (HANDLE) _beginthreadex(NULL, 0, DSCMovieCaptureThread, (void *)this, 0, NULL);
		CloseHandle(hMovieCapture);
	}
}

void  CMainFrame::OnDSCMoviePause(void)				
{
	if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING && ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		ESMSetRecordSync(FALSE);
	}

	MakeShortCut('9');
}

void  CMainFrame::DSCMoviePause(void)				
{
#if CMiLRe0
	if( m_bMovieSaveDone == FALSE)
	{
		ESMLog(5, _T("Not yet finished saving Movie."));
		return ;
	}
#endif
#ifdef _4DMODEL
	return;
#endif

	ESMLog(0, _T("Pause Mode Capture"));

	m_wndDSCViewer.m_bPauseMode = TRUE;
	if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
		AfxMessageBox(_T("Current server mode is video production mode. \n Available in Record or Both mode only."));
	else
	{

		if(m_wndDSCViewer. m_bRecordStep == ESM_RECORD_STOP)
			m_wndDSCViewer.m_bRecordStep = ESM_PAUSESTEP_PAUSE;
		else if( m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_RESUME)
			m_wndDSCViewer.m_bRecordStep = ESM_RECORD_STOP;
		else
			m_wndDSCViewer.m_bRecordStep = ESM_RECORD_STOP;

		HANDLE hMovieCapture = NULL;
		hMovieCapture = (HANDLE) _beginthreadex(NULL, 0, DSCMovieCaptureThread, (void *)this, 0, NULL);
		CloseHandle(hMovieCapture);
	}
}

void  CMainFrame::OnDSCMovieResume(void)				
{
#if 1
	BOOL bUseResync = ESMGetValue(ESM_VALUE_USE_RESYNC);
	if (!bUseResync)
	{
		CString strLog;
		strLog.Format(_T("[Movie Resume] Use resync restart in option is not checked."));
		ESMLog(5, strLog);
		return;
	}

	if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING && ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		ESMSetRecordSync();
	}

	if (OnResync_RecStart() == FALSE)
	{
		MakeShortCut('1');
	}
#else
	if( m_wndDSCViewer.m_bRecordStep != ESM_PAUSESTEP_PAUSEREADY )
		return;

	int nCaptureTime = m_wndDSCViewer.m_pDSCMgr->GetCaptureTime();
	int nCurTime = ESMGetTick();
	if( nCurTime - nCaptureTime < ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN) + sync_frame_time_after_sensor_on + ESMGetValue(ESM_VALUE_SENSORONTIME) + 200)
	{
		ESMLog(5, _T("nResumeTime < %d"), ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN) + sync_frame_time_after_sensor_on + ESMGetValue(ESM_VALUE_SENSORONTIME) + 200);
		return ;
	}
	CString strLog;
	int nFrmaeCount = 0;
	int nCaptureDelayTime = 7000; // 100usec;
	ESMLog(5, _T("nCaptureDelayTime [%d]"), nCaptureDelayTime);
	int nCurTick = ESMGetTick();
	strLog.Format(_T("nCurTick[%d], nCaptureTime[%d], ESM_VALUE_SYNC_TIME_MARGIN[%d], sync_frame_time_after_sensor_on[%d], ESM_VALUE_SENSORONTIME[%d]"), nCurTick, nCaptureTime, ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN), sync_frame_time_after_sensor_on, ESMGetValue(ESM_VALUE_SENSORONTIME));
	ESMLog(5, strLog);
	nCaptureTime = nCaptureTime + ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN) + sync_frame_time_after_sensor_on + ESMGetValue(ESM_VALUE_SENSORONTIME);
	nCaptureTime = nCurTick - nCaptureTime;
	nCaptureTime = nCaptureTime + nCaptureDelayTime;
	int nMilliSecondTime = nCaptureTime / 10;
	if( movie_frame_per_second == 30)
	{
		int nQuotient = 0, nRemain = 0;
		nQuotient = nMilliSecondTime / 1000;
		nRemain = nMilliSecondTime % 1000;
		nRemain = nRemain / 33;
		nFrmaeCount = nQuotient * 30 + nRemain;
	}
	else// if ( ESMGetValue(ESM_VALUE_MOVIEINPUTFPS) == ESM_MOVIE_INPUT_25P)
		nFrmaeCount = nMilliSecondTime / 40;

	m_wndDSCViewer.m_pDSCMgr->SetRecordTime(nCurTick + nCaptureDelayTime);
	ESMEvent* pMsg;
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_LIST_CAPTURERESUME;
	pMsg->nParam1 = nFrmaeCount;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);

	m_wndDSCViewer.m_bRecordStep = ESM_PAUSESTEP_RESUME;
	m_wndDSCViewer.m_pDSCMgr->SetCaptureTime(nCurTick + nCaptureDelayTime);

	strLog.Format(_T("Resume Frame Count : %d, Frame Count : %d"), nMilliSecondTime, nFrmaeCount);
	ESMLog(5, strLog);
#endif
}

unsigned WINAPI CMainFrame::DSCTestingThread(LPVOID param)
{
	CMainFrame* pMainFrm = (CMainFrame*)param;

	if( !pMainFrm->DscSyncTest() )
	{
		return 0;
	}

	//Draw Data
	pMainFrm->DrawTestingResult(FALSE);

	ESMLog(1, _T("Test End"));
	pMainFrm->m_bResumeSyncTest = TRUE;
	ESMSetSyncFlag(TRUE);
}

void CMainFrame::DrawTestingResult(BOOL bAuto)
{
//	return;

	m_bSyncTestAuto = bAuto;
	int nCount = 0;
	CDSCGroup* pGroup = NULL;
	CString strIP;
	int nAll = m_wndDSCViewer.m_pListView->GetItemCount();
	CDSCItem* pDSCItem = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pDSCItem = m_wndDSCViewer.m_pListView->GetItemData(i);
		strIP = ESMGetIPFromDSCID(pDSCItem->GetDeviceDSCID());

		for(int n = 0; n < m_wndDSCViewer.GetGroupCount(); n++)
		{
			pGroup = m_wndDSCViewer.GetGroup(n);
			if(!pGroup->m_nIP)
				continue;
			if(strIP.CompareNoCase(pGroup->GetRCMgr()->GetIP()) == 0 )
			{
				pDSCItem->m_nTesting[TESTING_PROP_TOTAL_CNT]++;
				pDSCItem->m_nTesting[TESTING_PROP_FAIL_CNT] = pGroup->GetRCMgr()->m_nSyncFailCnt;
				pDSCItem->m_nTesting[TESTING_PROP_AS_CNT] = pGroup->GetRCMgr()->m_nSyncCount;
				pDSCItem->m_nTesting[TESTING_PROP_AS_AVG] = (float)pGroup->GetRCMgr()->m_nSyncTotal / (float)pGroup->GetRCMgr()->m_nSyncCount;
				pDSCItem->m_nTesting[TESTING_PROP_AS_MIN] = pGroup->GetRCMgr()->m_nSyncMin;
				pDSCItem->m_nTesting[TESTING_PROP_AS_MAX] = pGroup->GetRCMgr()->m_nSyncMax;

				nCount = pDSCItem->m_nTesting[TESTING_PROP_TOTAL_CNT];
				break;
			}
		}
	}	

	if(bAuto && nCount >= SET_SYNC_TEST_COUNT)
	{
		//Auto Save
		m_wndDSCViewer.m_pListView->ReDrawTesting();
		OnSaveSyncTestResult();
		ResetSyncData();
	}

	m_wndDSCViewer.m_pListView->ReDrawTesting();
}

unsigned WINAPI CMainFrame::DSCMovieCaptureThread(LPVOID param)
{	
	CMainFrame* pMainFrm = (CMainFrame*)param;

	//pMainFrm->m_wndNetwork.StopCheckToAgentAlive();
	/*if(ESMGetInvalidMovieSize())
	{
	pMainFrm->m_wndDSCViewer.m_bRecordStep = ESM_RECORD_STOP;
	AfxMessageBox(_T("Movie Size Different!!"));
	return 0;
	}*/

	//pMainFrm->
	// DSC Movie Mode Check
	ESMSetFilmState(ESM_ADJ_FILM_STATE_MOVIE);
	ESMLog(5,_T("DSC State Confirm"));

	pMainFrm->m_wndLiveView.OnLiveOff();
	
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;

	int nRTSPSyncCnt = 0;
	BOOL bResync = FALSE;
	if( pMainFrm->m_wndDSCViewer.m_bRecordStep == ESM_NOMAL_START || pMainFrm->m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_PAUSE )
	{
		//jhhan 16-10-26
		//pMainFrm->OnHttpSender(ESM_NET_START);

		ESMLog(5,_T("Movie Button Click Capture Count : %d"), pMainFrm->m_CaptureCount++);
	
		//jhhan
#if _ORIGINAL
		int nDelayMSec = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC);
#else
		int nDelayMSec = 0;

		int nDelayMSel = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC_SEL);
		if(nDelayMSel == 0)      //ESM_VALUE_WAIT_SAVE_DSC
		{
			nDelayMSec = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC);
		}
		else if(nDelayMSel == 1) //ESM_VALUE_WAIT_SAVE_DSC_SEL
		{
			nDelayMSec = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC_SUB);
		}
		else					//Default
		{
			nDelayMSec = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC);
		}

		//ESMLog(5, _T("Movie Delay : %d"), nDelayMSec);
		
#endif
		int nDelayM2Sec = 0;
		int nSensorOnDesignation = 0, nDelayIndex = 0;
		for( int i =0 ;i < arDSCList.GetSize(); i++)
		{
			//대전 투수반 타자반 인경우 nDelayIndex 방향 상관없이 한쪽방향만 사용
			if( pMainFrm->m_DelayDirection == DSC_REC_DELAY_FORWARDDIRECTION || pMainFrm->m_DelayDirection == DSC_REC_DELAY_DIRECTION_2)
				nDelayIndex = i;
			else if( pMainFrm->m_DelayDirection == DSC_REC_DELAY_REVERSEDIRECTION || pMainFrm->m_DelayDirection == DSC_REC_DELAY_DIRECTION_1)
				nDelayIndex = (arDSCList.GetSize() - 1) - i;

			pItem = (CDSCItem*)arDSCList.GetAt(i);

			//jhhan 180508 Group
			CString strGroup = pItem->GetGroup();
			if(strGroup.IsEmpty() != TRUE)
			{
				CString strName, strIdx, strTotal;
				AfxExtractSubString(strName, strGroup, 0, '/');
				AfxExtractSubString(strIdx, strGroup, 1, '/');
				AfxExtractSubString(strTotal, strGroup, 2, '/');
				strName.Trim();
				strIdx.Trim();
				strTotal.Trim();

				int nIdx = _ttoi(strIdx) - 1;		//0 base
				int nTotal = _ttoi(strTotal) - 1;	//0 base

				CString strDir;

				if( pMainFrm->m_DelayDirection == DSC_REC_DELAY_FORWARDDIRECTION || pMainFrm->m_DelayDirection == DSC_REC_DELAY_DIRECTION_2)
				{
					nDelayIndex = nIdx;
					//2
					strDir = ESMGetDirection(_T("2"), strName);
				}
				else if( pMainFrm->m_DelayDirection == DSC_REC_DELAY_REVERSEDIRECTION || pMainFrm->m_DelayDirection == DSC_REC_DELAY_DIRECTION_1)
				{
					nDelayIndex = nTotal - nIdx;
					//1
					strDir = ESMGetDirection(_T("1"), strName);
				}
				else if(pMainFrm->m_DelayDirection == DSC_REC_DELAY_DIRECTION_3)
				{
					nDelayIndex = nTotal - nIdx;
					//3
					strDir = ESMGetDirection(_T("3"), strName);
				}else if(pMainFrm->m_DelayDirection == DSC_REC_DELAY_DIRECTION_4)
				{
					nDelayIndex = nIdx;
					//4
					strDir = ESMGetDirection(_T("4"), strName);
				}
				else if(pMainFrm->m_DelayDirection == DSC_REC_DELAY_DIRECTION_5)
				{
					nDelayIndex = nTotal - nIdx;
					strDir = ESMGetDirection(_T("5"), strName);
				}
				else if(pMainFrm->m_DelayDirection == DSC_REC_DELAY_DIRECTION_6)
				{
					nDelayIndex = nIdx;
					strDir = ESMGetDirection(_T("6"), strName);
				}
				

				if(strDir.IsEmpty() != TRUE)
				{
					if(strDir.CompareNoCase(_T("Forward")) == 0)
					{
						nDelayIndex = nIdx;
					}else if(strDir.CompareNoCase(_T("Backward")) == 0)
					{
						nDelayIndex = nTotal - nIdx;
					}
				}/*else
				 {
				 nDelayIndex = i;
				 }*/
			}
			
			//ESMLog(5, _T("Movive Delay [%s] : %d"), pItem->GetDeviceDSCID(), nDelayIndex);
			
			pItem->SetRecordReady(FALSE);
			nDelayM2Sec = pMainFrm->m_wndDSCViewer.GetDelayTime(pItem->GetDeviceDSCID());
			nSensorOnDesignation = pMainFrm->m_wndDSCViewer.GetSensorOnDesignation(pItem->GetDeviceDSCID());
			ESMEvent* pNewMsg	= new ESMEvent();
			pNewMsg->pDest		= (LPARAM)pItem;
			pNewMsg->nParam1	= nDelayIndex * nDelayMSec + nDelayM2Sec;		//순차 Delay
			pNewMsg->nParam2	= nSensorOnDesignation;			//File Load Delay Time
			//pNewMsg->nParam3	= pMainFrm->m_wndDSCViewer.m_bPauseMode;
			pNewMsg->nParam3	= ESMGetRecordStatus();//pMainFrm->m_bStart;//(CAMREVISION)
			pNewMsg->message	= WM_ESM_NET_CAPTUREDELAY;
			if( nDelayM2Sec != 0 || nSensorOnDesignation != 0)
				ESMLog(5, _T("DSC[%s] DelaySec1[%d], DelaySec2[%d], SensorOnDesignation[%d]"), pItem->GetDeviceDSCID(), i * nDelayMSec, nDelayM2Sec, nSensorOnDesignation);
			::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);
		}

		
		//////////////////////////////////////////////////////////////////////////
		// GH5 레코딩 이전에 Initialize 수행이 먼저 이루어 져야함. (Ver F4 이상)
#if 0
		CObArray arDSCList1;
		ESMGetDSCList(&arDSCList1);
		if(arDSCList1.GetCount())
		{
			for( int i = 0 ;i < arDSCList1.GetCount(); i++)
			{
				CDSCItem* pItem = NULL;
				pItem	= (CDSCItem*)arDSCList1.GetAt(i);

				if (pItem == NULL)
					continue;

				if(pItem->GetDeviceModel().IsEmpty())
				{
					continue;
				}

				// GH5
				if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
				{
					// agent로 버전체크 요청
					SdiMessage* pMsg = new SdiMessage;
					pMsg->message = WM_SDI_OP_GET_PROPERTY_DESC;
					pMsg->nParam1 = PTP_DPC_SAMSUNG_FW_VERSION;
					pItem->SdiAddMsg((ESMEvent*)pMsg);

				}

			}
		}		
#endif

		ESMEvent* pMsg = new ESMEvent();
		pMsg->message = WM_ESM_MOVIE_VALIDRESET;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
// 
// 		//joonho.kim 17.06.29 Sync
 		//if(!ESMGetSyncFlag())
		if (pMainFrm->GetReSyncFlag() == FALSE)
		{			
			if(ESMGetValue(ESM_VALUE_PC_SYNC_TIME) > 100)
			{
				if( !pMainFrm->DscSyncTime2() )
				{
					pMainFrm->m_wndDSCViewer.m_bRecordStep = ESM_RECORD_STOP;
					return 0;
				}
			}
			else
			{
				if( !pMainFrm->DscSyncTime() )
				{
					pMainFrm->m_wndDSCViewer.m_bRecordStep = ESM_RECORD_STOP;
					return 0;
				}
			}
			ESMSetSyncFlag(TRUE);
		}					
		else
		{
			pMainFrm->m_wndDSCViewer.m_pFrameSelector->SetInitRecTime();
		}
		//joonho.kim 17.07.10 Sync Test
		//pMainFrm->DrawTestingResult(TRUE);
		
		//Delete Record Data 17.04.11 joonho.kim
		if(!ESMGetMakeMovie())
		{
			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);
			int nCount = arDSCList.GetSize();
			
			//180425 hjcho Do Not Delete using ceremony & still Image
			if(!ESMGetValue(ESM_VALUE_CEREMONYUSE) && !ESMGetValue(ESM_VALUE_CEREMONY_STILLIMAGE))
			{
				if(ESMGetValue(ESM_VALUE_AUTO_DELETE))
				{
					DeleteRecordFile(&arDSCList);
					
					//jhhan 180817
					ESMEvent* pSendMsg = new ESMEvent();
					pSendMsg->message	= WM_ESM_NET_DELETE_FILE;
					::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_NET, (LPARAM)pSendMsg);

					ESMLog(5, _T("WM_ESM_NET_DELETE_FILE : %s"), ESMGetDeleteFolder());
				}
			}
		}else
		{
			if(ESMGetMakeAdjust())
			{
				ESMSetMakeAdjust(FALSE);
			}else
			{
				if(ESMGetValue(ESM_VALUE_AUTO_DELETE))
				{
					ESMEvent* pSendMsg = new ESMEvent();
					pSendMsg->message	= WM_ESM_NET_DELETE_FILE;
					::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_NET, (LPARAM)pSendMsg);

					ESMLog(5, _T("WM_ESM_NET_DELETE_FILE : %s"), ESMGetDeleteFolder());
				}
			}
		}
		DeleteFrameViewerFiles(pMainFrm);
		
		//ResetFrame();
		/*HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ResetFrameThread, (void*)pMainFrm, 0, NULL);
		CloseHandle(hSyncTime);*/

		ESMSetMakeMovie(FALSE);
		/////////////////////////////////////////////////
		
		//pMainFrm->m_FileMgr->SetCurTime();
		pMainFrm->m_wndDSCViewer.m_pFrameSelector->SetInitRecTime();
		pMainFrm->m_IpList.clear();
		CDSCGroup* pGroup = NULL;
		int nAll = pMainFrm->m_wndDSCViewer.GetGroupCount();

		for(int i = 0 ; i < nAll ; i ++)
		{
			pGroup =  pMainFrm->m_wndDSCViewer.GetGroup(i);
			if(!pGroup)
				continue;

			if(!pGroup->GetSync())
				pMainFrm->m_IpList.push_back(pGroup);
		}

		ESMLog(5,_T("Movie Capture Start_111"));

		if (pMainFrm->GetReSyncFlag())
		{
			bResync = TRUE;
			for( int i =0 ;i < arDSCList.GetSize(); i++)
			{
				pItem = (CDSCItem*)arDSCList.GetAt(i);
				if (pItem->GetReSyncGetTickReady())
				{
					ESMLog(5, _T("Ready Resync gettick 'OK' : %s "), pItem->GetDeviceDSCID());
					nRTSPSyncCnt++;
				}
				else
				{
					ESMLog(5, _T("Ready Resync gettick 'NOK' : %s "), pItem->GetDeviceDSCID());
				}
			}
		}
		pMainFrm->SetReSyncFlag(FALSE);
	}
	else if( pMainFrm->m_wndDSCViewer.m_bRecordStep == ESM_RECORD_STOP )
	{
		//jhhan 16-10-26
		//pMainFrm->OnHttpSender(ESM_NET_FINISH);

		int nCaptureTime = pMainFrm->m_wndDSCViewer.m_pDSCMgr->GetRecordTime();
		nCaptureTime = ESMGetTick() - nCaptureTime;

		//if( nCaptureTime < 10000 || ESMGetRecordingInfoInt(_T("MakingFlag"), FALSE) == TRUE)
		if( nCaptureTime < 10000 )
		{
			//			if(nCaptureTime < 10000)
			ESMLog(5, _T("nCaptureTime < 10000"));
			// 			else
			// 				ESMLog(5, _T("MakerPC is Making Movie"));

			if( pMainFrm->m_wndDSCViewer.m_bPauseMode == TRUE)
				pMainFrm->m_wndDSCViewer.m_bRecordStep = ESM_PAUSESTEP_RESUME;
			else
				pMainFrm->m_wndDSCViewer.m_bRecordStep = ESM_NOMAL_START;

			return 0;
		}


		//(CAMREVISION)
		for( int i =0 ;i < arDSCList.GetSize(); i++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(i);
			pItem->SetRecordReady(FALSE);
			ESMEvent* pNewMsg	= new ESMEvent();
			pNewMsg->pDest		= (LPARAM)pItem;
			pNewMsg->nParam1	= 0;
			pNewMsg->nParam2	= 0;
			pNewMsg->nParam3	= ESMGetRecordStatus();//pMainFrm->m_bStart;
			pNewMsg->message	= WM_ESM_NET_CAPTUREDELAY;
			::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);
		}

		HANDLE hHandle = NULL;
		hHandle = (HANDLE) _beginthreadex(NULL, 0, RecordDoneThread, (void *)pMainFrm, 0, NULL);
		CloseHandle(hHandle);
		CDSCGroup* pGroup = NULL;

		for(int i = 0 ; i< pMainFrm->m_IpList.size();i++)
		{
			pGroup = pMainFrm->m_IpList[i];
			if(!pGroup)
				continue;

			pGroup->SetSync(FALSE);
		}
	}

	ESMLog(5,_T("Movie Capture Start"));
	
	//-- Movie Capture Event
	pMainFrm->m_wndDSCViewer.m_pDSCMgr->DoMovieCapture(ESMGetRecordStatus());
	if(!(pMainFrm->m_wndDSCViewer.m_pFrameSelector->GetDSCRecStatus() == DSC_REC_ON))
	{
		pMainFrm->m_FileMgr->m_bInit = FALSE;
		pMainFrm->m_FileMgr->MovieDoneSaveProfile();
	}

	//KT 전송 동기화 4DP Sync Time
	if( pMainFrm->m_wndDSCViewer.m_bRecordStep == ESM_NOMAL_START || pMainFrm->m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_PAUSE )
	{
		if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
		{
			if(ESMGetValue(ESM_VALUE_RTSP) == TRUE)
			{
				CESMRCManager* pRCMgr = NULL;
				int nCaptureTime = pMainFrm->m_wndDSCViewer.m_pDSCMgr->GetCaptureTime();

				int nCount = pMainFrm->m_wndNetwork.m_arRCServerList.GetCount();

				ESMLog(5,_T("[RTSP] Run Cnt: %d, Wait Time : %d"),nCount,pMainFrm->m_nRTSPWaitTime);
				while(nCount--)
				{
					pRCMgr = (CESMRCManager *)pMainFrm->m_wndNetwork.m_arRCServerList.GetAt(nCount);

					if(!pRCMgr)
						continue;

					//  Waiting until Sync Time
					double nAgentTime;
					double nSavedLocalTime;
					double nAgentExeTime;

					nAgentTime = (double)pRCMgr->GetAgentTime();	
					nSavedLocalTime = (double)pRCMgr->_GetLocalTime();

					nAgentExeTime = nAgentTime + ((double)nCaptureTime - nSavedLocalTime);
					nAgentExeTime += (ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN) + pMainFrm->m_nRTSPWaitTime);

					ESMEvent* pMsg	= new ESMEvent();
					pMsg->message	= WM_ESM_NET_SEND_KTSENDTIME;
					pMsg->nParam1	= (UINT)nAgentExeTime;
					pRCMgr->AddMsg(pMsg);
				}
				if(bResync)
					ESMSetRTSPCommand(RTSP_COMMAND_RECORD_RESUME,nRTSPSyncCnt);
			}
			else
			{
				CESMRCManager* pRCMgr = NULL;
				int nCaptureTime = pMainFrm->m_wndDSCViewer.m_pDSCMgr->GetCaptureTime();

				int nCount = pMainFrm->m_wndNetworkEx.m_arRCServerList.GetCount();

				while(nCount--)
				{
					pRCMgr = (CESMRCManager *)pMainFrm->m_wndNetworkEx.m_arRCServerList.GetAt(nCount);

					if(!pRCMgr)
						continue;

					//  Waiting until Sync Time
					double nAgentTime;
					double nSavedLocalTime;
					double nAgentExeTime;

					nAgentTime = (double)pRCMgr->GetAgentTime();	
					nSavedLocalTime = (double)pRCMgr->_GetLocalTime();

					nAgentExeTime = nAgentTime + ((double)nCaptureTime - nSavedLocalTime);
					nAgentExeTime += (ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN) + 80000);

					ESMEvent* pMsg	= new ESMEvent();
					pMsg->message	= WM_ESM_NET_SEND_KTSENDTIME;
					pMsg->nParam1	= (UINT)nAgentExeTime;
					pRCMgr->AddMsg(pMsg);
				}
			}
		}
	}
	else if( pMainFrm->m_wndDSCViewer.m_bRecordStep == ESM_RECORD_STOP )
	{
		if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
		{
			CESMRCManager* pRCMgr = NULL;
			int nCaptureTime = pMainFrm->m_wndDSCViewer.m_pDSCMgr->GetRecordTime();//pMainFrm->m_wndDSCViewer.m_pDSCMgr->GetCaptureTime();
			int nTime = ESMGetTick();

			int nAgentStopCount = ((nTime - nCaptureTime) / 10000) - 1;

			int nStopLength = nAgentStopCount/600;
			//nAgentStopCount -= nStopLength;
			nAgentStopCount = nAgentStopCount - nStopLength;

#if 1
			if(ESMGetValue(ESM_VALUE_RTSP))
			{
				int nCount = pMainFrm->m_wndNetwork.m_arRCServerList.GetCount();
				ESMLog(5,_T("[RTSP_STOP_CNT] : %d - %s"),nAgentStopCount,ESMGetFrameRecord());

				while(nCount--)
				{
					pRCMgr = (CESMRCManager*) pMainFrm->m_wndNetwork.m_arRCServerList.GetAt(nCount);

					if(!pRCMgr)
						continue;

					ESMEvent* pMsg	= new ESMEvent();
					pMsg->message	= WM_ESM_NET_SEND_KTSTOPTIME;
					pMsg->nParam1	= (UINT)nAgentStopCount/* - nStopLength*/;
					pRCMgr->AddMsg(pMsg);
				}
				ESMSetRTSPStopTime(ESMGetFrameRecord(),nAgentStopCount);
				//m_pRTSPStateViewDlg->SetRecordStop(ESMGetFrameRecord(),nAgentStopCount);
			}
			else
			{
				int nCount = pMainFrm->m_wndNetworkEx.m_arRCServerList.GetCount();
				ESMLog(5,_T("[nCount] : %d"),nAgentStopCount);
				while(nCount--)
				{
					pRCMgr = (CESMRCManager *)pMainFrm->m_wndNetworkEx.m_arRCServerList.GetAt(nCount);

					if(!pRCMgr)
						continue;

					ESMEvent* pMsg	= new ESMEvent();
					pMsg->message	= WM_ESM_NET_SEND_KTSTOPTIME;
					pMsg->nParam1	= (UINT)nAgentStopCount;
					pRCMgr->AddMsg(pMsg);
				}
			}
#else

			int nCount = pMainFrm->m_wndNetworkEx.m_arRCServerList.GetCount();
			ESMLog(5,_T("[nCount] : %d"),nAgentStopCount);
			while(nCount--)
			{
				pRCMgr = (CESMRCManager *)pMainFrm->m_wndNetworkEx.m_arRCServerList.GetAt(nCount);

				if(!pRCMgr)
					continue;

				ESMEvent* pMsg	= new ESMEvent();
				pMsg->message	= WM_ESM_NET_SEND_KTSTOPTIME;
				pMsg->nParam1	= (UINT)nAgentStopCount;
				pRCMgr->AddMsg(pMsg);
			}
#endif
		}
	}

	return 0;
}

//------------------------------------------------------------------------------ 
//! @brief		OnToolsOptions
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CMainFrame::OnToolsOptions()
{
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_ESM_OPT_MAIN;
	SendMessage(WM_ESM, WM_ESM_OPT, (LPARAM)pMsg);
}

void CMainFrame::OnExecuteMode()
{
	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent();
	pMsg->message = WM_ESM_VIEW_TIMELINE_RELOAD;
	::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

	CString strText;
	strText.LoadString(IDR_MAINFRAME);
	SetWindowText(strText);

	m_wndDSCViewer.SetExecuteMovieMode(TRUE);
}

void CMainFrame::OnUpdateControlExecuteMode(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_wndDSCViewer.GetExecuteMovieMode());
}

void CMainFrame::OnUpdateCapturePause(CCmdUI* pCmdUI)
{
#if 0
	if(m_wndDSCViewer.m_bRecordStep == ESM_NOMAL_START)
		pCmdUI->Enable(FALSE);
	else if( m_wndDSCViewer.m_bRecordStep == ESM_RECORD_STOP || m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_RESUME)
		pCmdUI->Enable(TRUE);
	else 
		pCmdUI->Enable(FALSE);
#endif
}

void CMainFrame::OnUpdateCaptureResume(CCmdUI* pCmdUI)
{
#if 0
	if(m_wndDSCViewer.m_bRecordStep == ESM_NOMAL_START)
		pCmdUI->Enable(FALSE);
	else if( m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_PAUSE
		|| m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_PAUSEREADY)
		pCmdUI->Enable(TRUE);
	else 
		pCmdUI->Enable(FALSE);
#else
	if(m_wndDSCViewer.m_bRecordStep == ESM_NOMAL_START)
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
#endif
}

void CMainFrame::OnUpdateCapture(CCmdUI* pCmdUI)
{
	if( m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_PAUSE 
		|| 	m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_PAUSEREADY
		|| 	m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_RESUME)
	{
		pCmdUI->Enable(FALSE);
		return;
	}

	if(m_wndDSCViewer.GetExecuteMovieMode())
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

//2016/07/12 Lee SangA Template Load INI File
void CMainFrame::SetSelectPage(CString strPage)
{
	m_ESMOption.m_TemplateOpt.strSelectPage = strPage;
	m_ESMOption.m_TemplateOpt.intSelectPage = _ttoi(strPage);
}

//2016/07/12 Lee SangA Template Load INI File
int CMainFrame::GetSelectPage()
{
	return m_ESMOption.m_TemplateOpt.intSelectPage;
}

/*typedef struct TEST_
{
	int nIndex;
	CString strPath;
	CESMGpuDecode* pDecode;
	
};*/

unsigned WINAPI CMainFrame::DecodeThread(LPVOID param)
{
	
	return 0;
}

unsigned WINAPI CMainFrame::DeleteFrameViewerFiles(LPVOID param)
{
	CMainFrame* pMainFrm = (CMainFrame*)param;
	int n4DPAllCount = pMainFrm->m_wndNetworkEx.GetRCMgrCount();

	for(int i = 0; i < n4DPAllCount; i++)
	{				
		CESMRCManager* pMRCManager = pMainFrm->m_wndNetworkEx.GetRCMgr(i);

		HANDLE hSyncTime = NULL;
		CString* pstrPath = NULL;
		CString strPrePath = _T("");

		if(pMRCManager->GetSourceDSC().CompareNoCase(_T("")))
		{
			CString str4DPIP, strPath;
			str4DPIP = pMRCManager->GetIP();
			strPath.Format(_T("\\\\%s\\Movie\\%s"), str4DPIP, ESMGetDeleteFolder());

			if(strPrePath.CompareNoCase(strPath) != 0)
			{
				//Delete Folder
				strPrePath = strPath;
				pstrPath = new CString;
				pstrPath->Format(_T("%s"), strPath);
				hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, DeleteFolder, pstrPath, 0, NULL);
				CloseHandle(hSyncTime);
			}
		}
	}	
	return 0;
}

unsigned WINAPI CMainFrame::DeleteRecordFile(LPVOID param)
{
	HANDLE hSyncTime = NULL;
	CString* pstrPath = NULL;
	CString strPrePath = _T("");
	CString strDscIp, strDscId;
	CString strPath;

	if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		if(ESMGetDeleteFolder().IsEmpty())
			return 0;
		/*
		CObArray* arDSCList = (CObArray*)param;
		int nCount = arDSCList->GetSize();
		for(int i = 0; i < arDSCList->GetSize(); i++)
		{
			CDSCItem* pItem = (CDSCItem*)arDSCList->GetAt(i);
			strDscId = pItem->GetDeviceDSCID();
			strDscIp = ESMGetIPFromDSCIDInFrameViewer(strDscId);
			//strPath.Format(_T("%s\\%s"),ESMGetClientRecordPath(strDscIp), ESMGetFrameRecord());
			strPath.Format(_T("%s\\%s"),ESMGetClientRecordPath(strDscIp), ESMGetDeleteFolder());

			if(strPrePath.CompareNoCase(strPath) != 0)
			{
				//Delete Folder
				strPrePath = strPath;
				pstrPath = new CString;
				pstrPath->Format(_T("%s"), strPath);
				hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, DeleteFolder, pstrPath, 0, NULL);
				CloseHandle(hSyncTime);
				//DeleteFolder(pstrPath);
			}
		}
		*/
		CString strDelete = ESMGetDeleteFolder();
		//jhhan 170630 - 4dm Delete
		CESMFileOperation fo;
		strPath.Format(_T("%s\\%s.4dm"), ESMGetPath(ESM_PATH_MOVIE), strDelete);
		fo.Delete(strPath);
		ESMLog(5, _T("[Delete] %s"), strPath);

		strPath.Format(_T("%s\\%s.sp"), ESMGetPath(ESM_PATH_MOVIE), strDelete);
		fo.Delete(strPath);
		ESMLog(5, _T("[Delete] %s"), strPath);

		strPath.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_MOVIE_FILE), strDelete);
		fo.Delete(strPath, TRUE);
		ESMLog(5, _T("[Delete] %s"), strPath);	
	}
	else
	{
		CString strDel = ESMGetDeleteFolder();
		if(strDel.IsEmpty() != TRUE)
		{
			//jhhan 180817 네트워크 버전 삭제 - 에이전트 모드시
			strPath.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_LOCALMOVIE), strDel);
			ESMLog(5, _T("[Delete] %s"), strPath);

#ifdef _ALL_DELETE
			pstrPath = new CString;
			pstrPath->Format(_T("%s"), strPath);
			hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, DeleteFolder, pstrPath, 0, NULL);
			CloseHandle(hSyncTime);
#else	/*_COMPACT_DELETE*/
			hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, DeleteCompact, ESMGetMainWnd(), 0, NULL);
			CloseHandle(hSyncTime);
#endif
		}
	}
	return 0;
}


unsigned WINAPI CMainFrame::DeleteFolder(LPVOID param)
{
	CString* pstrPath = (CString*)param;
	CString strOpt;
	
	CESMFileOperation fo;
	fo.Delete(*pstrPath, TRUE);
	//ESMLog(5,_T("Delete Folder %s"),*pstrPath);
	if(pstrPath)
		delete pstrPath;
	return 0;
}

unsigned WINAPI CMainFrame::SyncTestingThread(LPVOID param)
{
	CMainFrame *pMain = (CMainFrame*)param;

	while(pMain->m_bSyncThread)
	{
		if(pMain->m_bResumeSyncTest)
		{
			pMain->m_bResumeSyncTest = FALSE;
			pMain->OnDSCTesting();
		}

		Sleep(10);
	}
	return 0;
}

void CMainFrame::ResetSyncData()
{
	CDSCGroup* pGroup = NULL;
	int nAll = m_wndDSCViewer.GetGroupCount();	
	for(int nRoop = 0; nRoop < nAll; nRoop++)
	{
		pGroup = m_wndDSCViewer.GetGroup(nRoop);
		pGroup->GetRCMgr()->m_nSyncAvg = 0;
		pGroup->GetRCMgr()->m_nSyncMin = -1;
		pGroup->GetRCMgr()->m_nSyncMax = 0;
		pGroup->GetRCMgr()->m_nSyncCount = 0;
		pGroup->GetRCMgr()->m_nSyncTotal = 0;
		pGroup->GetRCMgr()->m_nSyncFailCnt = 0;
	}

	nAll = m_wndDSCViewer.m_pListView->GetItemCount();
	CDSCItem* pDSCItem = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pDSCItem = m_wndDSCViewer.m_pListView->GetItemData(i);
		pDSCItem->ResetSyncDataAll();
	}
}

void CMainFrame::MakeShortCutOEM(WPARAM w)
{
	TRACE(_T("CMainFrame::MakeShortCutOEM [%0xX] \r\n"), w);
	switch (w)
	{
	case VK_OEM_MINUS:	// -
		m_wndNetwork.AllCameraConnect();
		break;
	case VK_OEM_PLUS:	// +
		m_wndNetwork.AllCameraOff();
		break;
	case VK_OEM_1:		// ;:
		m_wndDSCViewer.m_pListView->SetMouseWheel(TRUE);		// camera listview mouse wheel up down
		break;
	case VK_OEM_7:		// '"
		m_wndDSCViewer.m_pListView->SetMouseWheel(FALSE);		// camera listview mouse wheel up down
		break;
	case VK_OEM_2:		// /?
		OnUtilityAJAConnect();
		break;
	case VK_OEM_COMMA:	// ,<		
		ESMSetProperty1Step(ESM_PROPERTY_1SETP_SS_DOWN);
		break;
	case VK_OEM_PERIOD:	// .>
		ESMSetProperty1Step(ESM_PROPERTY_1SETP_SS_UP);
		break;
	case VK_OEM_4:		// [{
		ESMSetProperty1Step(ESM_PROPERTY_1SETP_ISO_DOWN);
		break;
	case VK_OEM_6:		// ]}
		ESMSetProperty1Step(ESM_PROPERTY_1SETP_ISO_UP);
		break;

	default:
		break;
	}
}

void CMainFrame::MakeShortCut(char cData)
{	
	//if(cData == 'P')
	//{
	//	for(int i = 0 ; i < 5; i++)
	//	{
	//		FILE *fp;
	//		CString strLog;

	//		fp = fopen("M:\\1.mp4", "rb");

	//		if(fp == NULL)
	//		{
	//			ESMLog(5,_T("Err"));
	//			return;
	//		}
	//		//SendLog(1,strLog);

	//		fseek(fp, 0L, SEEK_END);
	//		int nLength = ftell(fp);
	//		char* buf = new char[nLength+sizeof(RCP_FileSave_Info)];
	//		fseek(fp, 0, SEEK_SET);
	//		fread(buf+sizeof(RCP_FileSave_Info), sizeof(char), nLength, fp);

	//		ESMEvent* pMsg = new ESMEvent;
	//		pMsg->message = F4DC_SEND_CPU_FILE;
	//		pMsg->pDest  = (LPARAM)buf;
	//		pMsg->nParam1 = i;
	//		pMsg->nParam2 = sizeof(RCP_FileSave_Info)+nLength;
	//		pMsg->nParam3 = nLength;
	//		ESMSendAJANetwork(pMsg);

	//		fclose(fp);
	//		//pMsg->nParam1 = 
	//	}
	//}

	if(ESMGetValue(ESM_VALUE_RTSP))
	{
		m_nRTSPWaitTime = ESMGetValue(ESM_VALUE_RTSP_WAITTIME);
		if(cData == VK_ADD || cData == VK_SUBTRACT)
		{
			if(cData == VK_ADD)
			{
				m_nRTSPWaitTime += 1000;
				if(m_nRTSPWaitTime > 350000)
					m_nRTSPWaitTime = 350000;
#if 1
				CString strFile;
				strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONFIG);

				CESMIni ini;
				if(ini.SetIniFilename (strFile))
				{
					ini.WriteInt(INFO_SECTION_RC,INFO_RC_RTSP_WAITTIME,m_nRTSPWaitTime);
				}
#endif
			}
			if(cData == VK_SUBTRACT)
			{
				m_nRTSPWaitTime -= 1000;
				if(m_nRTSPWaitTime < 50000)
					m_nRTSPWaitTime = 50000;
#if 1
				CString strFile;
				strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONFIG);

				CESMIni ini;
				if(ini.SetIniFilename (strFile))
				{
					ini.WriteInt(INFO_SECTION_RC,INFO_RC_RTSP_WAITTIME,m_nRTSPWaitTime);
				}
#endif
			}
			ESMSetValue(ESM_VALUE_RTSP_WAITTIME,m_nRTSPWaitTime);
			ESMLog(5,_T("[RTSP] Wait Time : %d"),m_nRTSPWaitTime);
		}
	}
		
	if(cData == 'n')//NUMPAD - dot
	{
		if(ESMGetValue(ESM_VALUE_CEREMONYUSE) && ESMGetValue(ESM_VALUE_CEREMONY_STILLIMAGE))
			ESMPushSelectTime();
	}
	if(cData == '1')	 // 역방향 Nomal촬영
	{
		//wgkim 191210
		if (ESMGetValue(ESM_VALUE_AUTOADJUST_KZONE))
		{
			if (m_pAutoAdjustMgr->IsValidKZone() == 1)
			{
				if (m_pAutoAdjustMgr->GetDscCount() > 0)
				{
					KZoneDataInfo* pKZoneDataInfo = m_pAutoAdjustMgr->GetKZonePoint();

					//wgkim 191213
					if (pKZoneDataInfo->bIsEnabled != 0)
					{
						ESMEvent* pSendNetKZoneMsg = new ESMEvent();
						pSendNetKZoneMsg->message = WM_ESM_NET_KZONE_POINTS;
						pSendNetKZoneMsg->nParam1 = m_pAutoAdjustMgr->GetDscCount();
						pSendNetKZoneMsg->pParam = (LPARAM)pKZoneDataInfo;
						::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pSendNetKZoneMsg);

						ESMEvent* pSendLibKZoneMsg = new ESMEvent();
						pSendLibKZoneMsg->message = WM_ESM_MOVIE_SET_KZONE_POINTS;
						pSendLibKZoneMsg->nParam1 = m_pAutoAdjustMgr->GetDscCount();
						pSendLibKZoneMsg->pParam = (LPARAM)pKZoneDataInfo;
						::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pSendLibKZoneMsg);
					}
				}
			}
			else if (m_pAutoAdjustMgr->IsValidKZone() == 0)
			{
				ESMLog(m_pAutoAdjustMgr->IsValidKZone(), _T("[K-Zone]Do not Find \"TemplateTemp.pta\""));
			}
			else
			{
				ESMLog(m_pAutoAdjustMgr->IsValidKZone(), _T("[K-Zone]Unknown Error"));
			}
		}

		if(!ESMGetAdjustLoad())
		{
			if(ESMGetValue(ESM_VALUE_REFEREEREAD) == TRUE)
			{
				//임시저장
				ESMLog(5,_T("Movie Make without calibration"));
			}
			else if(ESMGetValue(ESM_VALUE_RTSP) == TRUE)
			{
				if(AfxMessageBox(_T("[WARNING] Adjust file can not be loaded.\n Do you want load?"),MB_YESNO)==IDYES)		
				{
					CString adjFileName;
					adjFileName.Format(_T("%s\\NewestAdj.adj"), ESMGetPath(ESM_PATH_MOVIE_CONF));
					LoadAdjustProfile(adjFileName);
				}
				else
				{
					ESMLog(5, _T("Recoding Cancel"));
					return;
				}
			}
			else
			{
				if(AfxMessageBox(_T("[WARNING] Adjust file can not be loaded. Continue?"),MB_YESNO)!=IDYES)		
				{
					ESMLog(5, _T("Recoding Cancel"));
					return;
				}
			}
		}

		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)	
		{	
			ESMLog(5, _T("Does not work. [4DA]"));
			return;	
		}	
		
		ESMLog(5, _T("[Key press] %c, record status [%d]"), cData, ESMGetRecordStatus());		
		
		ESMSetDelayDirection(_T("1"));

		OnRecording(cData);

		//180515 wgkim
		if(m_bReSyncFlag == FALSE)
			DeleteFiles(ESMGetFrameViewerSplitVideoRootPath());
	}

	if(cData == '2')	// 정방향 Nomal촬영
	{
		if(!ESMGetAdjustLoad())
		{
			if(ESMGetValue(ESM_VALUE_REFEREEREAD) != TRUE)
			{
				if(AfxMessageBox(_T("[WARNING] Adjust file can not be loaded. Continue?"),MB_YESNO)!=IDYES)		
				{
					ESMLog(5, _T("Recoding Cancel"));
					return;
				}
			}
		}
		
		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)	
		{	
			ESMLog(5, _T("Does not work. [4DA]"));	
			return;	
		}	

		ESMLog(5, _T("[Key press] %c, record status [%d]"), cData, ESMGetRecordStatus());		

		ESMSetDelayDirection(_T("2"));

		OnRecording(cData);

		//180515 wgkim
		if(m_bReSyncFlag == FALSE)
			DeleteFiles(ESMGetFrameViewerSplitVideoRootPath());
	}

	if(cData == '3')
	{
		if(!ESMGetAdjustLoad())
		{
			if(ESMGetValue(ESM_VALUE_REFEREEREAD) != TRUE)
			{
				if(AfxMessageBox(_T("[WARNING] Adjust file can not be loaded. Continue?"),MB_YESNO)!=IDYES)		
				{
					ESMLog(5, _T("Recoding Cancel"));
					return;
				}
			}
		}

		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)	
		{	
			ESMLog(5, _T("Does not work. [4DA]"));	
			return;	
		}	

		ESMLog(5, _T("[Key press] %c, record status [%d]"), cData, ESMGetRecordStatus());		

		ESMSetDelayDirection(_T("3"));

		OnRecording(cData);

		//180515 wgkim
		if(m_bReSyncFlag == FALSE)
			DeleteFiles(ESMGetFrameViewerSplitVideoRootPath());

		//OnDSCTesting();

		//CDSCItem* pItem = NULL;
		//pItem->AddMovieSize(1);
		/*
		m_bTestFlag = TRUE;
		SetTimer(TIMER_START_RECORD, 1000, NULL);*/
		/*for(int i = 0; i < 1; i++)
		{
			TEST_* decode_info = new TEST_;
			decode_info->strPath.Format(_T("F:\\%d.MP4"), i+1);
			decode_info->pDecode = &gpudecode[i];
			decode_info->nIndex = i;
			decode_info->pDecode->m_strWindowName = decode_info->strPath;
			HANDLE hSyncTime = NULL;
			hSyncTime = (HANDLE) _beginthreadex(NULL, 0, DecodeThread, (void *)decode_info, 0, NULL);
			CloseHandle(hSyncTime);
		}*/
		return;
	}

	if(cData == '4')
	{
		if(!ESMGetAdjustLoad())
		{
			if(ESMGetValue(ESM_VALUE_REFEREEREAD) != TRUE)
			{
				if(AfxMessageBox(_T("[WARNING] Adjust file can not be loaded. Continue?"),MB_YESNO)!=IDYES)		
				{
					ESMLog(5, _T("Recoding Cancel"));
					return;
				}
			}
		}

		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)	
		{	
			ESMLog(5, _T("Does not work. [4DA]"));	
			return;	
		}	

		ESMLog(5, _T("[Key press] %c, record status [%d]"), cData, ESMGetRecordStatus());		

		ESMSetDelayDirection(_T("4"));

		OnRecording(cData);

		//180515 wgkim
		if(m_bReSyncFlag == FALSE)
			DeleteFiles(ESMGetFrameViewerSplitVideoRootPath());

		//FFmpegManager ffmpeg(FALSE);
		//ESMLog(5,_T("Not USE Num '4' "));

		//m_bTestFlag = FALSE;
		//ESMLog(1,_T("######Terminate Test"));
	}

	if(cData == '5')
	{
		if(!ESMGetAdjustLoad())
		{
			if(ESMGetValue(ESM_VALUE_REFEREEREAD) != TRUE)
			{
				if(AfxMessageBox(_T("[WARNING] Adjust file can not be loaded. Continue?"),MB_YESNO)!=IDYES)		
				{
					ESMLog(5, _T("Recoding Cancel"));
					return;
				}
			}
		}

		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)	
		{	
			ESMLog(5, _T("Does not work. [4DA]"));	
			return;	
		}	

#ifdef _SYNC_TEST_MODULE
		m_bResumeSyncTest = TRUE;
		m_bSyncThread = TRUE;
		ResetSyncData();
		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, SyncTestingThread, this, 0, NULL);
		CloseHandle(hSyncTime);
#else
		
		ESMLog(5, _T("[Key press] %c, record status [%d]"), cData, ESMGetRecordStatus());		

		ESMSetDelayDirection(_T("5"));

		OnRecording(cData);

		//180515 wgkim
		if(m_bReSyncFlag == FALSE)
			DeleteFiles(ESMGetFrameViewerSplitVideoRootPath());
#endif
		return;
	}

	if(cData == '6')
	{
		if(ESMGetValue(ESM_VALUE_REFEREEREAD) != TRUE)
		{
			if(AfxMessageBox(_T("[WARNING] Adjust file can not be loaded. Continue?"),MB_YESNO)!=IDYES)		
			{
				ESMLog(5, _T("Recoding Cancel"));
				return;
			}
		}
#ifdef _SYNC_TEST_MODULE
		m_bSyncThread = FALSE;
#endif

		ESMSetDelayDirection(_T("6"));

		OnRecording(cData);

		//180515 wgkim
		if(m_bReSyncFlag == FALSE)
			DeleteFiles(ESMGetFrameViewerSplitVideoRootPath());

		return;
	}

	if(cData == 'T')
	{
		//jhhan 180611
		ESMLog(5, _T("[Key Press] Auto Test : %c"), cData);

		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)	
		{	
			ESMLog(5, _T("Does not work. [4DA]"));	
			return;	
		}	

		m_bTestFlag = TRUE;
		SetTimer(TIMER_START_RECORD, 1000, NULL);
	}

	if(cData == 'Y')
	{
		//jhhan 180611
		ESMLog(5, _T("[Key Press] Auto Test : %c"), cData);

		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)	
		{	
			ESMLog(5, _T("Does not work. [4DA]"));	
			return;	
		}	

		m_bTestFlag = FALSE;
		ESMLog(1,_T("######Terminate Test"));


	}

	if(cData == 'G')	// GetTick
	{
#if 0
		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)	
		{	
			ESMLog(5, _T("Does not work. [4DA]"));
			return;	
		}	

		if (ESMGetRecordStatus() == FALSE)
		{
			ESMLog(5, _T("[Key Press] Resync : %c , Deos not work record status [%d]"), cData, ESMGetRecordStatus());		
			return;
		}

		int nCaptureTime = m_wndDSCViewer.m_pDSCMgr->GetRecordTime();
		nCaptureTime = ESMGetTick() - nCaptureTime;
		if( nCaptureTime < 10000 )
		{
			CString strLog;
			double dbTime = (double)(10000 - nCaptureTime) / 10000;
			strLog.Format(_T("Wait for ReSync Record %02fs"), dbTime);
			ESMLog(0,strLog);
			return;
		}

		int nAll = m_wndDSCViewer.GetItemCount();
		CDSCItem* pItem = NULL;
		while(nAll--)
		{
			pItem = m_wndDSCViewer.GetItemData(nAll);
			if (pItem->GetReSyncGetTickReady() == FALSE)
			{
				ESMLog(5, _T("############### Please wait GetTick finish.. "));
				return;
			}
		}
			

		ESMLog(5, _T("[Key Press] GetTick : %c"), cData);		

		if(AfxMessageBox(_T("[ReSync] Do you want to GetTick commnad?"),MB_YESNO)!=IDYES)		
			return;

		DscReSyncGetTick();
#endif
	}

	if(cData == 'R')	// Resync
	{	
#if 0
		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)	
		{	
			ESMLog(5, _T("Does not work. [4DA]"));
			return;	
		}	

		BOOL bUseResync = ESMGetValue(ESM_VALUE_USE_RESYNC);
		if (bUseResync == FALSE)
		{
			ESMLog(5, _T("Please check Use_Resync in options."));
			return;				
		}

		if (ESMGetRecordStatus() == FALSE)
		{
			ESMLog(5, _T("[Key Press] Resync : %c , Deos not work record status [%d]"), cData, ESMGetRecordStatus());		
			return;
		}

		int nCaptureTime = m_wndDSCViewer.m_pDSCMgr->GetRecordTime();
		nCaptureTime = ESMGetTick() - nCaptureTime;
		if( nCaptureTime < 10000 )
		{
			CString strLog;
			double dbTime = (double)(10000 - nCaptureTime) / 10000;
			strLog.Format(_T("Wait for ReSync Record %02fs"), dbTime);
			ESMLog(0,strLog);
			return;
		}

		if (m_bReSyncFlag)
			return;		

		if(AfxMessageBox(_T("[ReSync] Do you want to ReSync record?"),MB_YESNO)!=IDYES)		
			return;

		KillTimer(TIMER_RECORD_RESYNC_GET_TICK);

		m_nReSyncTimerCount = 0;
		m_bReSyncFlag = TRUE;
		m_nGetTickRecFinishCount = 0;

		RecStopGetTickFinish();
		SetTimer(TIMER_RECORD_REC_GET_TICK_FINISH, 1000*1, NULL);

		//jhhan 180611
		ESMLog(5, _T("[Key Press] Resync : %c"), cData);	

#endif
	}

	if(cData == 'Q')	// Puase
	{	
		/*ESMSetRecordingInfoint(_T("OrderDirection"), DSC_REC_DELAY_REVERSEDIRECTION);
		m_DelayDirection = DSC_REC_DELAY_REVERSEDIRECTION;
		if(m_wndDSCViewer.m_bRecordStep == ESM_RECORD_STOP)
			OnDSCMoviePause();*/

		ESMEvent* pSendNetKZoneMsg = new ESMEvent();
		pSendNetKZoneMsg->message = WM_ESM_NET_KZONE_JUDGMENT;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pSendNetKZoneMsg);
	}

	if(cData == 'W')	// 역방향 Resume 촬영
	{	
		/*ESMSetRecordingInfoint(_T("OrderDirection"), DSC_REC_DELAY_FORWARDDIRECTION);
		m_DelayDirection = DSC_REC_DELAY_FORWARDDIRECTION; 
		if(m_wndDSCViewer.m_bRecordStep == ESM_RECORD_STOP)
			OnDSCMoviePause();*/
	}
	if(cData == 'E')	// 정방향 Resume 촬영
	{	
		/*if(m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_PAUSEREADY)
			OnDSCMovieResume();*/
	}

	if(cData == 'M')	//Merge Movie
	{
		//ESMEvent* pMovMsg = new ESMEvent;
		//pMovMsg->message = WM_ESM_MOVIE_MAKE_ARRAY_MERGE;
		//pMovMsg->pParam = (LPARAM)this;
		//m_pMovieThreadMgr->AddMsg(pMovMsg);

		//m_wndTimeLineEditor.m_pTimeLineView->MergeArrayMovie();
	}

	if(cData == 'Z')
	{
		if(ESMGetLoadRecord())
			return;

		//jhhan 180611
		ESMLog(5, _T("[Key Press] FrameView : %c"), cData);
		
		int nNum = ESMGetSelectPointNum();
		
		if(nNum >= 50)
		{
			nNum -= 1;
			AfxMessageBox(_T("Select Point is over 50!"));
			ESMSetLastSelectPointNum(nNum);
			ESMSetSelectPointNum(nNum);
			return;
		}

		int nCaptureTime = m_wndDSCViewer.m_pDSCMgr->GetRecordTime();

		CObArray arDSCList;
		ESMGetDSCList(&arDSCList);
		if(arDSCList.GetCount())
		{
			for( int i = 0 ;i < arDSCList.GetCount(); i++)
			{
				CDSCItem* pItem = NULL;
				pItem	= (CDSCItem*)arDSCList.GetAt(i);

				if (pItem == NULL)
					continue;

				if (pItem->GetDeviceModel().IsEmpty())
					continue;

				if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
				{
					nCaptureTime = nCaptureTime - sync_frame_time_after_sensor_on;
				}

				break;
			}
		}		

		nCaptureTime = ESMGetTick() - nCaptureTime;
		ESMSetRemoteTime(nCaptureTime);

		int nMilliSecond = nCaptureTime / 10;
		m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nMilliSecond, nNum);
		m_wndFrame.SetSelectedTime(nMilliSecond, nNum);
		//Save
		CString strTime;
		strTime.Format(_T("SelectPointTime%d"), nNum);
		ESMSetRecordingInfoint(strTime, nMilliSecond);

		//jhhan 190103
		ESMSetRecordingPoint(strTime,nMilliSecond);

		strTime.Format(_T("SelectLastPointTime"));
		ESMSetRecordingInfoint(strTime, nMilliSecond);
		//hjcho 17-03-20 SelectPointSave
		ESMSetRecordingPoint(strTime,nMilliSecond);
		//jhhan 16-11-21 LastPointNum
		ESMSetLastSelectPointNum(nNum);

		nNum++;
		ESMSetSelectPointNum(nNum);

		//Event
		/*if(ESMGetValue(ESM_VALUE_CEREMONYUSE) && ESMGetValue(ESM_VALUE_CEREMONY_STILLIMAGE)
		&&ESMGetRecordStatus())
		ESMPushTime(nMilliSecond);*/
	}

	if(cData == 'X')	// 선택 마지막 포인트 이동
	{
		//jhhan  180611
		ESMLog(5, _T("[Key Press] FrameView : %c"), cData);

		if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
		{
			ESMInitViewPointNum();
			ESMInitSelectPointNum();
			ESMInitLastSelectPointNum();

			CString strPoint;
			int nCount = 0;
			int nMilliSecond;
			//while(1)
			{
				nCount = ESMGetSelectPointNum();
				strPoint.Format(_T("SelectLastPointTime"), nCount);
				nMilliSecond = ESMGetRecordingInfoInt(strPoint,0);
				m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nMilliSecond, nCount);
				ESMSetLastSelectPointNum(nCount);
				nCount++;
				ESMSetSelectPointNum(nCount);
			}
		}
		
		//jhhan 190103
		if(m_bLoadPoint == FALSE)
		{
			ESMLog(5, _T("[][][][][][][][][] Load All Select Point [][][][][][][][][]"));
			int nCount = ESMGetLastSelectPointNum();
			while(nCount >= 0)
			{
				CString strTime;
				strTime.Format(_T("SelectPointTime%d"), nCount);
				int nCaptureTime = ESMGetRecordingInfoInt(strTime, nCount);

				m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nCaptureTime, nCount);
				m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(nCount);
				::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
				nCount--;
			}

			m_bLoadPoint = TRUE;
		}
		//jhhan 190113 : Tennis 호주 요청 : X - 맨마지막 이전 포인트 이동
		int nNum = ESMGetLastSelectPointNum();
		if(nNum < 0)
			nNum = 0;
		ESMSetViewPointNum(nNum);

		OnFrameSelectView();
		
		
		
/*
#if 0
		int nNum = ESMGetSelectPointNum() - 1;
#else
		int nNum = ESMGetLastSelectPointNum();
#endif
		if(nNum < 0)
			nNum = 0;
		ESMSetViewPointNum(nNum);

		OnFrameSelectView();*/
	}

	if(cData == 'S')	// 우타자(Right)
	{
//		if(!ESMGetLoadingFrame())
//		{
//			ESMSetLoadingFrame(TRUE);
//
//			ESMLog(1,_T("Select Frame Load Start"));
//			if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
//				AfxMessageBox(_T("Current server mode is video production mode. \n Available in Record or Both mode only."));
//			else// if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
//			{
//				int nCount = ESMGetViewPointNum();
//				
//				CString strTime;
//				strTime.Format(_T("SelectPointTime%d"), nCount);
//				ESMLog(1, _T("Set Select Time %s"), strTime);
//
//				int nCaptureTime = ESMGetRecordingInfoInt(strTime, nCount);
//				int nDSCIdx = ESMGetDSCIndex(ESMGetTrackInfo().st_AxisInfo[0].strCamID);
//
//				int nSelDSC = m_wndDSCViewer.m_pFrameSelector->SetSelectAUTORightDscIndex(nCaptureTime);
//				
//#if 0
//				if(nSelDSC != nDSCIdx)
//				{
//					ESMLog(5,_T("[AutoDetecting] Left Cam File did not exist"));
//					
//					CObArray arDSCList;
//					int nDSCListSize;
//					ESMGetDSCList(&arDSCList);
//					nDSCListSize = arDSCList.GetSize();
//
//					CDSCItem* pItem = NULL;					
//					pItem = (CDSCItem*)arDSCList.GetAt(nSelDSC);
//					CString strNextDSCID = pItem->GetDeviceDSCID();
//
//
//					ESMLog(5,_T("[AutoDetecting] Change DSC %s to %s"),ESMGetTrackInfo().st_Left.strCamID,strNextDSCID);
//					track_info stTrack = ESMGetTrackInfo();
//					stTrack.st_Left.strCamID = strNextDSCID;
//					
//					ESMSetTrackInfo(stTrack);
//				}
//#endif
//				m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nCaptureTime, nCount);
//
//				//m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(nCount);
//				m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(nCaptureTime,nDSCIdx);
//				::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
//			}
//			ESMSetKeyValue(VK_F11);
//			ESMLog(1,_T("Select Frame Load End"));
//		}
//		else
//		{
//			ESMLog(1,_T("Select Frame Loading..."));
//		}
	}

	if(cData == 'D')	// 좌타자(Left)
	{
//		if(!ESMGetLoadingFrame())
//		{
//			ESMSetLoadingFrame(TRUE);
//
//			ESMLog(1,_T("Select Frame Load Start"));
//			if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
//				AfxMessageBox(_T("현재 서버모드가 녹화모드입니다. \n 영상제작 또는 Both모드에서만 가능합니다."));
//			else// if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
//			{
//				//Load
//				int nCount = ESMGetViewPointNum();
//
//				CString strTime;
//				strTime.Format(_T("SelectPointTime%d"), nCount);
//				ESMLog(1, _T("Set Select Time %s"), strTime);
//
//				int nCaptureTime = ESMGetRecordingInfoInt(strTime, nCount);
//				int nDSCIdx = ESMGetDSCIndex(ESMGetTrackInfo().st_AxisInfo[1].strCamID);
//
//				int nSelDSC = m_wndDSCViewer.m_pFrameSelector->SetSelectAUTOLeftDscIndex(nCaptureTime);
//				
//#if 0
//				if(nSelDSC != nDSCIdx)
//				{
//					ESMLog(5,_T("[AutoDetecting] Right Cam  File did not exist"));
//
//					CObArray arDSCList;
//					int nDSCListSize;
//					ESMGetDSCList(&arDSCList);
//					nDSCListSize = arDSCList.GetSize();
//
//					CDSCItem* pItem = NULL;					
//					pItem = (CDSCItem*)arDSCList.GetAt(nSelDSC);
//					CString strNextDSCID = pItem->GetDeviceDSCID();
//
//
//					ESMLog(5,_T("[AutoDetecting] Change DSC %s to %s"),ESMGetTrackInfo().st_Right.strCamID,strNextDSCID);
//					track_info stTrack = ESMGetTrackInfo();
//					stTrack.st_Right.strCamID = strNextDSCID;
//
//					ESMSetTrackInfo(stTrack);
//				}
//#endif
//				m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nCaptureTime, nCount);
//
//				//m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(nCount);
//				m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(nCaptureTime,nDSCIdx);
//				::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
//			}
//			ESMSetKeyValue(VK_F12);
//
//			ESMLog(1,_T("Select Frame Load End"));
//		}
//		else
//		{
//			ESMLog(1,_T("Select Frame Loading..."));
//		}
	}

	if(cData == 'C')
	{
		//jhhan 180611
		ESMLog(5, _T("[Key Press] FrameView : %c"), cData);

		int nNum = ESMGetViewPointNum();
		nNum--;

		if(nNum < 0)
			nNum = 0;
		ESMSetViewPointNum(nNum);

		OnFrameSelectView();
	}

	//jhhan 16-11-23 삭제 -> 변경 View
	if(cData == VK_SPACE)
	{
#if _POINT_MOVE
		int nNum = ESMGetViewPointNum();
		nNum++;

		//jhhan 16-11-21 LastPointNum 최대값 제한
		if(ESMGetLastSelectPointNum() < nNum)
			nNum = ESMGetLastSelectPointNum();

		ESMSetViewPointNum(nNum);
#else
		OnFrameSelectView();
#endif
	}

	//jhhan 16-11-23 Select ++ View
	if(cData == 'V')
	{
		//jhhan 180611
		ESMLog(5, _T("[Key Press] FrameView : %c"), cData);

		int nNum = ESMGetViewPointNum();
		nNum++;

		//jhhan 16-11-21 LastPointNum 최대값 제한
		if(ESMGetLastSelectPointNum() < nNum)
			nNum = ESMGetLastSelectPointNum();

		ESMSetViewPointNum(nNum);

		OnFrameSelectView();
	}

	if(cData == '7' || cData == VK_NUMPAD7 /*|| cData == VK_F1*/)	// 포인트 선택
	{
		/*
		int nCaptureTime = m_wndDSCViewer.m_pDSCMgr->GetRecordTime();
		nCaptureTime = ESMGetTick() - nCaptureTime;
		int nMilliSecond = nCaptureTime / 10;
		m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nMilliSecond, 0);
		//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가 포인터 선택시 선택시간 Set
		//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가 포인터 선택시 SelectDSC Set
		m_wndFrame.SetSelectedTime(nMilliSecond, 0);
		//Save
		ESMSetRecordingInfoint(_T("SelectPointTime1"), nMilliSecond);
		*/
	}
	if(cData == VK_F1)
	{
		m_nFKey = 10;

		CString strPagenum;
		strPagenum = "10";
		SetSelectPage(strPagenum);

		CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
		pMainWnd->m_wndDSCViewer.m_pListView->m_strPage = "F1";
		pMainWnd->m_wndDSCViewer.m_pListView->m_iPage = 0;
		pMainWnd->m_wndDSCViewer.m_pListView->Invalidate();
		pMainWnd->m_wndDashBoard.LoadInfo();

		ESMSetFuncKey(cData);

		//jhhan 180611
		ESMLog(5, _T("[Key Press] Template : %s"), pMainWnd->m_wndDSCViewer.m_pListView->m_strPage);

	}

	if(cData == VK_F2)	// 포인트 선택
	{
		//CESMOptTemplate m_ot;
		//m_ot.m_ctrlSelectPage.SetCurSel(1);
		//m_ot.curPageNum = 1;

		m_nFKey = 20;
		/*
		int nCaptureTime = m_wndDSCViewer.m_pDSCMgr->GetRecordTime();
		nCaptureTime = ESMGetTick() - nCaptureTime;
		int nMilliSecond = nCaptureTime / 10;
		m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nMilliSecond, 1);
		//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가 포인터 선택시 선택시간 Set
		//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가 포인터 선택시 SelectDSC Set
		m_wndFrame.SetSelectedTime(nMilliSecond, 1);
		//Save
		ESMSetRecordingInfoint(_T("SelectPointTime2"), nMilliSecond);
		*/
		CString strPagenum;
		strPagenum = "20";
		SetSelectPage(strPagenum);

		//Viewer 갱신
		//2016/07/14 Lee SangA Write current page(F1~F5) on windowm
		CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
		pMainWnd->m_wndDSCViewer.m_pListView->m_strPage = "F2";
		pMainWnd->m_wndDSCViewer.m_pListView->m_iPage = 1;
		pMainWnd->m_wndDSCViewer.m_pListView->Invalidate();
		pMainWnd->m_wndDashBoard.LoadInfo();

		ESMSetFuncKey(cData);

		//jhhan 180611
		ESMLog(5, _T("[Key Press] Template : %s"), pMainWnd->m_wndDSCViewer.m_pListView->m_strPage);
	}
	if(cData == VK_F3)	// 포인트 선택
	{
		//CESMOptTemplate m_ot;
		//m_ot.m_ctrlSelectPage.SetCurSel(2);
		//m_ot.curPageNum = 2;

		m_nFKey = 30;
		/*
		int nCaptureTime = m_wndDSCViewer.m_pDSCMgr->GetRecordTime();
		nCaptureTime = ESMGetTick() - nCaptureTime;
		int nMilliSecond = nCaptureTime / 10;
		m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nMilliSecond, 2);
		//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가 포인터 선택시 선택시간 Set
		//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가 포인터 선택시 SelectDSC Set
		m_wndFrame.SetSelectedTime(nMilliSecond, 2);
		//Save
		ESMSetRecordingInfoint(_T("SelectPointTime3"), nMilliSecond);
		*/
		CString strPagenum;
		strPagenum = "30";
		SetSelectPage(strPagenum);

		//Viewer 갱신
		//2016/07/14 Lee SangA Write current page(F1~F5) on window
		CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
		pMainWnd->m_wndDSCViewer.m_pListView->m_strPage = "F3";
		pMainWnd->m_wndDSCViewer.m_pListView->Invalidate();
		pMainWnd->m_wndDSCViewer.m_pListView->m_iPage = 2;
		pMainWnd->m_wndDashBoard.LoadInfo();
		
		//CESMOptTemplate *pOptTpt = (CESMOptTemplate*)GetESMOption();
		//pOptTpt->curPageNum = 2;

		ESMSetFuncKey(cData);

		//jhhan 180611
		ESMLog(5, _T("[Key Press] Template : %s"), pMainWnd->m_wndDSCViewer.m_pListView->m_strPage);

	}
	if(cData == VK_F4)	// 포인트 선택
	{
		/*
		int nCaptureTime = m_wndDSCViewer.m_pDSCMgr->GetRecordTime();
		nCaptureTime = ESMGetTick() - nCaptureTime;
		int nMilliSecond = nCaptureTime / 10;
		m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nMilliSecond, 3);
		//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가 포인터 선택시 선택시간 Set
		//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가 포인터 선택시 SelectDSC Set
		m_wndFrame.SetSelectedTime(nMilliSecond, 3);
		//Save
		ESMSetRecordingInfoint(_T("SelectPointTime4"), nMilliSecond);
		*/

		m_nFKey = 40; //F4

		CString strPagenum;
		strPagenum = "40";
		SetSelectPage(strPagenum);

		CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
		pMainWnd->m_wndDSCViewer.m_pListView->m_strPage = "F4";
		pMainWnd->m_wndDSCViewer.m_pListView->Invalidate();
		pMainWnd->m_wndDSCViewer.m_pListView->m_iPage = 3;
		pMainWnd->m_wndDashBoard.LoadInfo();

		ESMSetFuncKey(cData);

		//jhhan 180611
		ESMLog(5, _T("[Key Press] Template : %s"), pMainWnd->m_wndDSCViewer.m_pListView->m_strPage);
	}

	if(cData == '8')	// 촬영종료
	{
		/*if(m_wndDSCViewer.m_bRecordStep == ESM_NOMAL_START)
			OnDSCMovieCapture();
		if(m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_RESUME)
		{
			m_bReStartFalg = TRUE;
			OnDSCMoviePause();
		}*/
	}
	if(cData == '9')	// 촬영종료
	{
		//jhhan 180611
		ESMLog(5, _T("[Key Press] Recoding : %c"), cData);

		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)	
		{	
			ESMLog(5, _T("Does not work. [4DA]"));	
			return;	
		}	

		/*if(ESMGetValue(ESM_VALUE_RTSP))
		{
			if(ESMGetRTSPBirdRecordState() == FALSE)
			{
				for(int i = 0 ; i < 10 ; i++)
				{
					ESMLog(0,_T("CHECK BIRDVIEW RECORD STATE"));
				}
				return;
			}
		}*/

		int nCaptureTime = m_wndDSCViewer.m_pDSCMgr->GetRecordTime();
		nCaptureTime = ESMGetTick() - nCaptureTime;
		if( nCaptureTime < 10000 )
		{
			CString strLog;
			double dbTime = (double)(10000 - nCaptureTime) / 10000;
			strLog.Format(_T("Wait for Record Stop%02fs"), dbTime);
			ESMLog(0,strLog);
			return;
		}

		// resync 관련
		KillTimer(TIMER_RECORD_RESYNC_START);
		KillTimer(TIMER_RECORD_RESYNC_GET_TICK);

		//
		int nAllItem = m_wndDSCViewer.GetItemCount();
		//BOOL bFlag = FALSE;
		for (int i = 0; i < nAllItem ; i++)
		{
			CDSCItem* pDscItem = m_wndDSCViewer.GetItemData(i);						
			// checked gettick finish..
			if (pDscItem->GetReSyncGetTickReady() == FALSE/* && bFlag == FALSE*/)
			{
				ESMLog(5, _T("[Key Press]: 9 Please wait gettick finish..."));					
				//bFlag = TRUE;
				m_nGetTickRecFinishCount = 0;
				SetTimer(TIMER_RECORD_REC_GET_TICK_FINISH, 1000, NULL);
				return;
			}
		}

		for (int i = 0; i < nAllItem ; i++)
		{
			CDSCItem* pDscItem = m_wndDSCViewer.GetItemData(i);	
			pDscItem->SetReSyncGetTickReady(TRUE);
		}

		if(m_wndDSCViewer.m_bRecordStep == ESM_RECORD_STOP)
		{
			ESMSetRecordStatus(FALSE);			
		}

		if(m_wndDSCViewer.m_bRecordStep == ESM_NOMAL_START)
		{
			SetTimer(TIMER_RECORD_FINISH_STATUS, 6000, NULL);

			//(CAMREVISION)
			//m_bStart = FALSE;
			ESMSetRecordStatus(FALSE);			
			DSCMovieCapture();
		}
		if(m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_RESUME)
		{
			DSCMoviePause();	
		}

		int nAll = m_wndDSCViewer.GetItemCount();
		CDSCItem* pItem = NULL;
		while(nAll--)
		{
			pItem = m_wndDSCViewer.GetItemData(nAll);
			pItem->SetDSCStatus(SDI_STATUS_REC_FINISH);
		}
		//wgkim@esmlab.com
		if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT))
		{
			LoadTemplateSquaresProfile();

			m_wndTimeLineEditor.SetTemplateMgr(m_pTemplateMgr);
			m_wndFrame.m_pDlgFrameList->SetTemplateMgr(m_pTemplateMgr);
//			m_wndFrameEx.m_pRemoteCtrlDlg->SetTemplateMgr(m_pTemplateMgr);
		}

		/*CString adjFileName;
		adjFileName.Format(_T("%s\\NewestAdj.adj"), ESMGetPath(ESM_PATH_MOVIE_CONF));
		LoadAdjustProfile(adjFileName);*/
		//hjcho - 171020
		ESMSetRemoteState(FALSE);

		//190312 hjcho
		//ESMSetRTSPCommand(RTSP_COMMAND_RECORD_PAUSE);
	}
	if(cData == '0' || cData == VK_NUMPAD0)	// Puase 종료.
	{
		if(m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_PAUSEREADY)
			DSCMovieCapture();
	}

	if(cData == '4' /*|| cData == VK_F5*/)	// 선택 포인트 이동
	{
		if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
			AfxMessageBox(_T("Current server mode is video Record mode. \n Available in production or Both mode only."));
		else// if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
		{
			//Load
			int nCaptureTime = ESMGetRecordingInfoInt(_T("SelectPointTime1"), 0);
			m_wndDSCViewer.m_pFrameSelector->SetSelectDscIndex(nCaptureTime);
			m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nCaptureTime, 0);

			m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(0);
			::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
		}
		// 		else
		// 		{
		// 			m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(0);
		// 			::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
		// 		}

		ESMSetKeyValue(VK_F5);
	}

	if(cData == VK_F5) {
		m_nFKey = 50; //F5

		CString strPagenum;
		strPagenum = "50";
		SetSelectPage(strPagenum);

		CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
		pMainWnd->m_wndDSCViewer.m_pListView->m_strPage = "F5";
		pMainWnd->m_wndDSCViewer.m_pListView->Invalidate();
		pMainWnd->m_wndDSCViewer.m_pListView->m_iPage = 4;
		pMainWnd->m_wndDashBoard.LoadInfo();

		ESMSetKeyValue(VK_F5);

		//jhhan 180611
		ESMLog(5, _T("[Key Press] Template : %s"), pMainWnd->m_wndDSCViewer.m_pListView->m_strPage);
	}

	if(cData == VK_F6)
	{
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_ENV_PATH;
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
	}

	if(cData == VK_F7)
	{
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_ENV_4D_OPTION;
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
	}

	if(cData == VK_F8)
	{
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_ENV_CAMERA_INFO;
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
	}
	if(cData == VK_F9)
	{
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_ENV_PC_INFO;
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
	}
	if(cData == VK_F11)
	{
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_ENV_CEREMONY;
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
	}
// 	if(cData == VK_F10)
// 	{
// 		ESMEvent* pMsg = new ESMEvent;
// 		pMsg->message = WM_ESM_ENV_TEMPLATE;
// 		::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
// 	}

#ifdef NOT_USE
	if(cData == VK_F6)	// 선택 포인트 이동
	{
		if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
			AfxMessageBox(_T("현재 서버모드가 녹화모드입니다. \n 영상제작 또는 Both모드에서만 가능합니다."));
		else// if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
		{
			//Load
			int nCaptureTime = ESMGetRecordingInfoInt(_T("SelectPointTime2"), 0);
			m_wndDSCViewer.m_pFrameSelector->SetSelectDscIndex(nCaptureTime);
			m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nCaptureTime, 1);

			m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(1);
			::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
		}
		//		else
		// 		{
		// 			m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(1);
		// 			::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
		// 		}

		ESMSetKeyValue(VK_F6);
	}
	if(cData == VK_F7)	// 선택 포인트 이동
	{
		if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
			AfxMessageBox(_T("현재 서버모드가 녹화모드입니다. \n 영상제작 또는 Both모드에서만 가능합니다."));
		else// if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
		{
			//Load
			int nCaptureTime = ESMGetRecordingInfoInt(_T("SelectPointTime3"), 0);
			m_wndDSCViewer.m_pFrameSelector->SetSelectDscIndex(nCaptureTime);
			m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nCaptureTime, 2);

			m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(2);
			::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
		}
		// 		else
		// 		{
		// 			m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(2);
		// 			::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
		// 		}

		ESMSetKeyValue(VK_F7);
	}
	if(cData == VK_F8)	// 선택 포인트 이동
	{
		if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
			AfxMessageBox(_T("현재 서버모드가 녹화모드입니다. \n 영상제작 또는 Both모드에서만 가능합니다."));
		else// if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
		{
			//Load
			int nCaptureTime = ESMGetRecordingInfoInt(_T("SelectPointTime4"), 0);
			m_wndDSCViewer.m_pFrameSelector->SetSelectDscIndex(nCaptureTime);
			m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nCaptureTime, 3);

			m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(3);
			::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
		}
		// 		else
		// 		{
		// 			m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(3);
		// 			::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
		// 		}

		ESMSetKeyValue(VK_F8);
	}
#endif
	if(cData == '5')	// 영상 Template 생성
	{
#if CMiLRe0
		if( m_bMovieSaveDone == FALSE)
		{
			ESMLog(5, _T("Not yet finished saving Movie."));
			return ;
		}
#endif
		ESMLog(5,_T("Not USE Num '5' Auto Template Making "));
/*
		int nOrderDirection = ESMGetRecordingInfoInt(_T("OrderDirection"), 0);
		if(nOrderDirection == DSC_REC_DELAY_FORWARDDIRECTION)
			m_wndTimeLineEditor.MakeBasicTamplete();
		else
			m_wndTimeLineEditor.MakeBasicTampleteRe();


		m_wndTimeLineEditor.OnDSCMakeMovie();
		m_pImgLoader->AllClearImageList();*/

	}
	if(cData == '6')	// 영상 제작
	{
		m_wndTimeLineEditor.OnDSCMakeMovie();
		m_pImgLoader->AllClearImageList();
	}

	if(cData == VK_DELETE)	// 영상 제작 종료
	{
		if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
		{
			m_wndDSCViewer.m_pDSCMgr->SetEditerRecording(FALSE);
			m_wndDSCViewer.m_pDSCMgr->SetTimeLineReset();

			CString strRecProfile = ESMGetRecordingInfoString(_T("RecProfile"), _T(""));
			ESMSetFrameRecord(strRecProfile);


			ESMInitViewPointNum();
			ESMInitSelectPointNum();
			ESMInitLastSelectPointNum();
		}
	}
	//CMiLRe 20151020 TimeLine Object Delete Key 추가
	if(cData == VK_DECIMAL || cData == VK_DELETE)
	{
#ifdef CMiLRe0
		if(GetMakingMovieFlag())
		{
			ESMLog(5, _T("Not Make Movie, because make movie NOW!"));
			return ;
		}
#endif
		ESMEvent* pMsg	= new ESMEvent;
		//pMsg->message	= WM_ESM_FRAME_REMOVE_OBJ_TAIL;
		pMsg->message = WM_ESM_FRAME_REMOVEALL_OBJ_EDITOR;
		pMsg->pDest		= (LPARAM)this;

		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);	
	}
	if(cData == VK_BACK)
	{

#ifdef CMiLRe0
		if(GetMakingMovieFlag())
		{
			ESMLog(5, _T("Not Make Movie, because make movie NOW!"));
			return ;
		}
#endif

		ESMEvent* pMsg	= new ESMEvent;
		//pMsg->message	= WM_ESM_FRAME_REMOVE_OBJ_HEADER;
		pMsg->message = WM_ESM_FRAME_REMOVEALL_OBJ_EDITOR;
		pMsg->pDest		= (LPARAM)this;
		
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);	
	}

	if(cData == VK_NUMPAD0)
	{
		if(ESMGetValue(ESM_VALUE_CEREMONYUSE) && ESMGetValue(ESM_VALUE_CEREMONY_STILLIMAGE))
			ESMCreateStillMovie();
	}
	
	if( cData >=  VK_NUMPAD1 && cData <= VK_NUMPAD9)
	{
		//jhhan 180611
		ESMLog(5, _T("[Key Press] Template Make : NUMPAD%d"), cData-VK_NUMPAD0);
		
		if(ESMGetFrameRecord().GetLength() < 5)
		{
			ESMLog(5, _T("Not Recorded\n"));
			return ;
		}

		m_wndDashBoard.SetMakeMovieStatus(FALSE);
		m_wndDashBoard.LoadInfo();

		////wgkim 181002
		if(ESMGetValue(ESM_VALUE_AUTOADJUST_KZONE))
		{
			if(m_pAutoAdjustMgr->IsValidKZone() == 1)
			{
				if(m_pAutoAdjustMgr->GetDscCount() > 0)
				{
					KZoneDataInfo* pKZoneDataInfo = m_pAutoAdjustMgr->GetKZonePoint();

					//wgkim 191213
					if (pKZoneDataInfo->bIsEnabled == 0)
						return;

					ESMEvent* pSendNetKZoneMsg	= new ESMEvent();
					pSendNetKZoneMsg->message	= WM_ESM_NET_KZONE_POINTS;
					pSendNetKZoneMsg->nParam1	= m_pAutoAdjustMgr->GetDscCount();
					pSendNetKZoneMsg->pParam	= (LPARAM)pKZoneDataInfo;
					::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pSendNetKZoneMsg);

					ESMEvent* pSendLibKZoneMsg	= new ESMEvent();
					pSendLibKZoneMsg->message	= WM_ESM_MOVIE_SET_KZONE_POINTS;
					pSendLibKZoneMsg->nParam1	= m_pAutoAdjustMgr->GetDscCount();
					pSendLibKZoneMsg->pParam	= (LPARAM)pKZoneDataInfo;
					::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pSendLibKZoneMsg);
				}
			}
			else if(m_pAutoAdjustMgr->IsValidKZone() == 0)
			{
				ESMLog(m_pAutoAdjustMgr->IsValidKZone(), _T("[K-Zone]Do not Find \"TemplateTemp.pta\""));
			}
			else
			{
				ESMLog(m_pAutoAdjustMgr->IsValidKZone(), _T("[K-Zone]Unknown Error"));
			}
		}

		if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetConnectAJANetwork() == FALSE)
		{
			if(AfxMessageBox(_T("AJA Player is not connected.\nDo you want to make exist method?"),MB_YESNO)==IDNO)
				return;
		}

		if(ESMGetConnectAJANetwork() && ESMGetAJAScreen() == FALSE 
			&& ESMGetAJAMaking() == FALSE && ESMGetAJADivProcess() == TRUE)
		{
			if(ESMGetLoadRecord() == TRUE)
			{
				if(ESMGetValue(ESM_VALUE_LOADFILE_DIRECT) == FALSE)
				{
					for(int i = 0 ; i < 10; i++)
						ESMLog(0,_T("AJA Player cannot support \"Load Record!!!!\""));
					return;
				}
			}

			ESMSetStartMakeTime(GetTickCount(),0);

			CString *pstr4DMName = new CString;
			pstr4DMName->Format(_T("%s"),ESMGetFrameRecord());

			ESMEvent* pMsg = new ESMEvent;
			pMsg->message	= F4DC_MAKING_START;
			pMsg->nParam1	= ESMGetValue(ESM_VALUE_TEMPLATE_STABILIZATION);
			//pMsg->nParam2	= ESMGetValue(ESM_VALUE_TEMPLATE_STABILIZATION);
			pMsg->pDest		= (LPARAM) pstr4DMName;

			ESMSendAJANetwork(pMsg);
		}

		ESMLog(5, _T("[CHECK_C]Make Movie, KeyPress Start \n"));

		
#if CMiLRe0
		if( m_bMovieSaveDone == FALSE)
		{
			ESMLog(5, _T("Not yet finished saving Movie."));
			return ;

			CString strRecProfile = ESMGetRecordingInfoString(_T("RecProfile"), _T(""));
			ESMSetFrameRecord(strRecProfile);
		}
#endif
#ifdef CMiLRe0
		if(GetMakingMovieFlag())
		{
			ESMLog(5, _T("Not Make Movie, because make movie NOW!"));
			return ;
		}
#endif
		int nOrderDirection = ESMGetRecordingInfoInt(_T("OrderDirection"), 0);

		if(ESMGetBaseBallFlag()) //-- BaseBall 촬영
		{
			//-- Tamplate 정보입력
			ESMSetBaseBallTamplate(cData);

			int nTotalRecTime = m_wndDSCViewer.m_pFrameSelector->GetRecTime();
			int nSelectedTime = m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();

			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message = WM_ESM_MOVIE_MAKE_THREAD;
			//pMsg->nParam1 = cData - VK_NUMPAD0-1;	// Template
			pMsg->nParam1 = cData - VK_NUMPAD0 + m_nFKey;	// Template
			pMsg->nParam2 = nTotalRecTime;			// Total RecTime
			pMsg->nParam3 = nSelectedTime;			// Select Time

			::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);	

			HWND hSlectorWnd = m_wndDSCViewer.m_pFrameSelector->m_hWnd;
			::SetFocus(hSlectorWnd);
			::SetForegroundWindow(hSlectorWnd);
			::SetActiveWindow(hSlectorWnd);
		}
		else //-- Normal 촬영
		{
			m_wndTimeLineEditor.m_pTimeLineView->m_bRecal = FALSE;
			//CMiLRe 20151202 VMCC 템플리 호출 함수 추가
			//m_wndTimeLineEditor.VmccTamplate(cData - VK_NUMPAD0 + GetSelectPage()); //cData - VK_NUMPAD0-1
			m_wndDashBoard.SetTemplateNum(cData - VK_NUMPAD0);
			int nTick = GetTickCount();
			m_wndTimeLineEditor.VmccTamplate(cData - VK_NUMPAD0 + m_nFKey); //cData - VK_NUMPAD0-1

			//jhhan 181219
			VIEW_STRUCT _stView;
			_stView = m_wndTimeLineEditor.GetViewTempalte(cData - VK_NUMPAD0 + m_nFKey);
			if(_stView.nCount > 0)
			{
				if(ESMGetValue(ESM_VALUE_LOADFILE_DIRECT) == FALSE && ESMGetLoadRecord() == TRUE)
				{
					for(int i = 0 ; i < 10; i++)
						ESMLog(0,_T("MULTIVIEW cannot support \"Load Record!!!!\""));
					return;
				}
				else
				{
					_stView.nSelectTime = m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();
					//hjcho 181224
					m_wndTimeLineEditor.m_pTimeLineView->AddViewStruct(_stView);//m_pViewMovie->AddViewStruct(_stView);
				}
			}

			//wgkim@esmlab.com 자동템플릿 좌표밀림 경고
			//if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT))
			//{	
			//	if(m_pTemplateMgr->IsOverBoundary())
			//	{
			//		ESMLog(5, _T("Template Boundary Is Over!!!!!!!"));				
			//	}
			//	else
			//	{
			//		ESMLog(5, _T("Template Boundary Is not Over!!!!!!!"));					
			//	}
			//	
			//	m_wndTimeLineEditor.Reset();				
			//	m_pTemplateMgr->SetArrTemplatePoint();
			//	return;
			//}

			ESMLog(5, _T("VMCC Template Loading Total : %d ms"), GetTickCount() - nTick);
			m_wndTimeLineEditor.OnDSCMakeMovie();
			m_pImgLoader->AllClearImageList();

			::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
		}
	}

	//jhhan
	if(cData == 'A' || cData == 'B')
	{
		CString strDelay;
		int nDelayMSec = 0;
		CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
		
		if(cData == 'A')
		{
			ESMSetValue(ESM_VALUE_WAIT_SAVE_DSC_SEL, 0);

			nDelayMSec = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC);
			strDelay.Format(_T("A : %d"), nDelayMSec);

			pMainWnd->m_wndDSCViewer.m_pListView->m_strDelay = "A"/*strDelay*/;		
			ESMSetDelayMode(_T("A"));
		}
		else if(cData == 'B')
		{
			ESMSetValue(ESM_VALUE_WAIT_SAVE_DSC_SEL, 1);

			nDelayMSec = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC_SUB);
			strDelay.Format(_T("B : %d"), nDelayMSec);

			pMainWnd->m_wndDSCViewer.m_pListView->m_strDelay = "B"/*strDelay*/;
			ESMSetDelayMode(_T("B"));
		}
		else
		{
			ESMSetValue(ESM_VALUE_WAIT_SAVE_DSC_SEL, 0);

			pMainWnd->m_wndDSCViewer.m_pListView->m_strDelay = "-";	
		}
		pMainWnd->m_wndDSCViewer.m_pListView->Invalidate();

		m_wndDashBoard.LoadInfo();
	}
	//hjcho 160830
	if(cData == 'F')
	{
#if 1
		if(ESMGetValue(ESM_VALUE_AUTODETECT))
		{
			if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
			{
				ESMInitViewPointNum();
				ESMInitSelectPointNum();
				ESMInitLastSelectPointNum();

				CString strPoint;
				int nCount = 0;
				int nMilliSecond;
				//while(1)
				{
					nCount = ESMGetSelectPointNum();
					strPoint.Format(_T("SelectLastPointTime"), nCount);
					nMilliSecond = ESMGetRecordingInfoInt(strPoint,0);
					m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nMilliSecond, nCount);
					ESMSetLastSelectPointNum(nCount);
					nCount++;
					ESMSetSelectPointNum(nCount);
				}
			}

			int nNum = ESMGetLastSelectPointNum();
			if(nNum < 0)
				nNum = 0;
			ESMSetViewPointNum(nNum);
			OnFrameAutoSelect();
		}
#else
		track_info stTrack = ESMGetTrackInfo();
		BOOL bStart = stTrack.bStart;
		CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
		if(!bStart)
		{
			stTrack.bStart = TRUE;
			ESMSetTrackInfo(stTrack);
			pMainWnd->m_wndDSCViewer.m_pListView->m_AUTO = "AUTO";
			ESMLog(1,_T("[AutoDetecting] Set AutoDetect"));
		}
		else
		{
			stTrack.bStart = FALSE;
			ESMSetTrackInfo(stTrack);
			pMainWnd->m_wndDSCViewer.m_pListView->m_AUTO = "DEAUTO";
			ESMLog(1,_T("[AutoDetecting] Release AutoDetect"));
		}
		pMainWnd->m_wndDSCViewer.m_pListView->Invalidate();
#endif
	}
	//hjcho 161208 FrameViewer Adjust
	if(cData == VK_RETURN)
	{
		if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT))
		{
			ESMSetValue(ESM_VALUE_TEMPLATEPOINT, FALSE);
			m_wndDSCViewer.m_pListView->m_FrameAdj = "-";				
		}
		else
		{
			ESMSetValue(ESM_VALUE_TEMPLATEPOINT, TRUE);
			m_wndDSCViewer.m_pListView->m_FrameAdj = "TEM";	
			LoadTemplateSquaresProfile();
		}
		m_wndDSCViewer.m_pListView->Invalidate();	
	}

	if(cData == 'K')
	{
		if(ESMGetValue(ESM_VALUE_AUTOADJUST_KZONE))
		{
			m_pAutoAdjustMgr->SetToggleUsingKZone(!m_pAutoAdjustMgr->GetUsageKZone());
			ESMLog(5,_T("KZone Toggle : %d"), m_pAutoAdjustMgr->GetUsageKZone());
			m_wndDashBoard.LoadInfo();
		}
		else
		{
			ESMLog(5,_T("[Warning]K-zone is disabled. If you want to use K-zone function, please check \"Option -> 4D Maker Option -> K-Zone (AutoAdjust)\""));
		}
	}
	if(cData == VK_HOME)
	{
		m_wndDSCViewer.m_pListView->OnDSCGridClick(0, 0);
	}
	if(cData == VK_END)
	{		
		m_wndDSCViewer.m_pListView->OnDSCGridClick(0, m_wndDSCViewer.GetItemCount()-1);
	}	
}

void CMainFrame::MakeCShortCut(char cData)
{ 
	CString strFileName;

	if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
	{
		AfxMessageBox(_T("Current server mode is video Record mode. \n Available in production or Both mode only."));
		return;
	}
	/*if(cData == '5' || cData == '2')
		strFileName = GetESMOption()->m_TemplateOpt.strTemplate1;

	if(cData == '6' )
		strFileName = GetESMOption()->m_TemplateOpt.strTemplate2;

	if(cData == '7')
		strFileName = GetESMOption()->m_TemplateOpt.strTemplate3;

	if(cData == '8')
		strFileName = GetESMOption()->m_TemplateOpt.strTemplate4;*/

	//-- 2014-10-22 cygil@esmlab.com Load VMCC template 
	/*if ( cData == '5' || cData == '6' || cData == '7' || cData == '8' )
	{
		CFile file;
		if(!file.Open(strFileName, CFile::modeRead))
		{
			ESMLog(0,_T("Load TimeLine Template File [%s]") , strFileName);
			return;
		}

		CArchive ar(&file, CArchive::load);
		m_wndTimeLineEditor.LoadMovieTemplate(ar);
		ar.Close();
		file.Close();

		//-- 2014-10-22 cygil@esmlab.com Timeline Editor에 template 로드후 Focus를 DSCViewer 이동 (MakeMovie 단축키 실행위해)
		m_wndDSCViewer.m_pFrameSelector->SetFocus();
	}*/

	if(cData == '1')
		m_wndTimeLineEditor.MakeBasicTamplete();

	if(cData == '2' )
		m_wndTimeLineEditor.MakeBasicTampleteRe();

	if(cData == '3')
		m_wndTimeLineEditor.MakeBasicTampleteFreze();

	if(cData == '4')
		m_wndTimeLineEditor.MakeBasicTampleteFrezeRe();

	/*if(cData == '5')
	OnResync();*/

	if(cData == 'S')
		m_wndPointList.SetStartPoint();

	if(cData == 'E')
		m_wndPointList.SetEndPoint();

	if(cData == 'T')
	{
		int nStartDSc = m_wndPointList.GetStartDsc()-1;
		int nStartTime = m_wndPointList.GetStartTime();
		int nEndDSc = m_wndPointList.GetEndDSc()-1;
		int nEndTime = m_wndPointList.GetEndTime();
		if( nStartDSc == nEndDSc && nStartTime == nEndTime)
			return ;
		m_wndTimeLineEditor.MakeTimeLine(nStartDSc, nEndDSc, nStartTime, nEndTime);
		m_wndTimeLineEditor.m_pTimeLineView->DrawEditor();
		m_wndPointList.ReSetData();
	}

	if(cData == 'V')
		m_wndTimeLineEditor.OnDSCMakeMovie();

	//if(cData == 'B')
	//	m_wndTimeLineEditor.OnDSCMakeMovie3D();

	//원본영상 Adjust 적용 저장
	if(cData == 'J')
	{
		//Create Directory
		CString strSubFolder;
		strSubFolder.Format(_T("%s\\2D\\Adjust"), ESMGetPath(ESM_PATH_OUTPUT));
		CreateDirectory(strSubFolder, NULL);
		strSubFolder.Format(_T("%s\\2D\\Adjust\\%s"), ESMGetPath(ESM_PATH_OUTPUT), ESMGetAdjustPath());
		CreateDirectory(strSubFolder, NULL);


		m_wndTimeLineEditor.ItemAllDelete();
		m_wndTimeLineEditor.MakeAdjustTamplete();
		m_wndTimeLineEditor.OnDSCADJMakeMovie();
	}

	if(cData == 'M')
	{
		//CESMUtilMultiplexerDlg MultiplexerDlg;
		//MultiplexerDlg.DoModal();
		
		/*ESMEvent* pMsg1 = NULL;
		pMsg1 = new ESMEvent;
		pMsg1->message = WM_ESM_VIEW_SETNEWESTADJ;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg1);*/
		
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_ENV_MUX;
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
	}
	if(cData == 'B')
	{
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_BACKUP_MUX;
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
	}
	if(cData == VK_NUMPAD0)
	{
		if(ESMGetValue(ESM_VALUE_CEREMONYUSE) && ESMGetValue(ESM_VALUE_CEREMONY_STILLIMAGE))
			m_stillImage.RectMovie();
	}
	
	//CMiLRe 20160106 record load 단축키
	if(cData == 'O')
	{
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message = WM_OPTION_LOAD_RECORD;

		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
	}
	//CMiLRe 20160113 VMCC Template load, Not Make Movie
	if(cData >=  VK_NUMPAD1 && cData <= VK_NUMPAD9)	// 영상 Template 생성(1530)
	{		
		m_wndTimeLineEditor.m_pTimeLineView->m_bRecal = TRUE;
		ESMSetSelectedTime(0);

		if(ESMGetFrameRecord().GetLength() < 5)
		{
			ESMLog(5, _T("Not Recorded\n"));
			return ;
		}

		ESMLog(5, _T("[CHECK_C]Make Movie, KeyPress Start\n"));
#if CMiLRe0
		if( m_bMovieSaveDone == FALSE)
		{
			ESMLog(5, _T("Not yet finished saving Movie."));
			return ;

			CString strRecProfile = ESMGetRecordingInfoString(_T("RecProfile"), _T(""));
			ESMSetFrameRecord(strRecProfile);
		}
#endif
#ifdef CMiLRe0
		if(GetMakingMovieFlag())
		{
			ESMLog(5, _T("Not Make Movie, because make movie NOW!"));
			return ;
		}
#endif
		int nOrderDirection = ESMGetRecordingInfoInt(_T("OrderDirection"), 0);
		//CMiLRe 20151202 VMCC 템플리 호출 함수 추가
		//m_wndTimeLineEditor.VmccTamplate(cData - VK_NUMPAD0-1);
		int nTick = GetTickCount();
		m_wndTimeLineEditor.VmccTamplate(cData - VK_NUMPAD0 + m_nFKey);
		ESMLog(5, _T("VMCC Template Loading Total : %d ms"), GetTickCount()-nTick);
		m_pImgLoader->AllClearImageList();
	}
	
//	if(cData == 'F')
//	{
//#if 1
//		if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
//		{
//			ESMInitViewPointNum();
//			ESMInitSelectPointNum();
//			ESMInitLastSelectPointNum();
//
//			CString strPoint;
//			int nCount = 0;
//			int nMilliSecond;
//			//while(1)
//			{
//				nCount = ESMGetSelectPointNum();
//				strPoint.Format(_T("SelectLastPointTime"), nCount);
//				nMilliSecond = ESMGetRecordingInfoInt(strPoint,0);
//				m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nMilliSecond, nCount);
//				ESMSetLastSelectPointNum(nCount);
//				nCount++;
//				ESMSetSelectPointNum(nCount);
//			}
//		}
//
//		int nNum = ESMGetLastSelectPointNum();
//		if(nNum < 0)
//			nNum = 0;
//		ESMSetViewPointNum(nNum);
//		OnFrameSelectView(FALSE);
//#else
//		track_info stTrack = ESMGetTrackInfo();
//		BOOL bStart = stTrack.bStart;
//		CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
//		if(!bStart)
//		{
//			stTrack.bStart = TRUE;
//			ESMSetTrackInfo(stTrack);
//			pMainWnd->m_wndDSCViewer.m_pListView->m_AUTO = "AUTO";
//			ESMLog(1,_T("[AutoDetecting] Set AutoDetect"));
//		}
//		else
//		{
//			stTrack.bStart = FALSE;
//			ESMSetTrackInfo(stTrack);
//			pMainWnd->m_wndDSCViewer.m_pListView->m_AUTO = "DEAUTO";
//			ESMLog(1,_T("[AutoDetecting] Release AutoDetect"));
//		}
//		pMainWnd->m_wndDSCViewer.m_pListView->Invalidate();
//#endif
//	}

	//if(cData == 'Z')
	//{
	//	//170818 hjcho - kt과제 Local Test용 
	//	CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
	//	ESMLog(5,_T("Start MultiGPU Test"));

	//	CObArray arDSCList;
	//	ESMGetDSCList(&arDSCList);
	//	CDSCItem* pItem = NULL;
	//	int nWidth,nHeight;
	//	CString strCamID;
	//	for(int i = 0 ; i < arDSCList.GetSize(); i++)
	//	{
	//		pItem = (CDSCItem*)arDSCList.GetAt(i);

	//		stAdjustInfo* adjInfo = new stAdjustInfo;
	//		adjInfo->AdjAngle = pItem->GetDSCAdj(DSC_ADJ_A);
	//		adjInfo->AdjSize = pItem->GetDSCAdj(DSC_ADJ_SCALE);
	//		adjInfo->AdjptRotate.x = pItem->GetDSCAdj(DSC_ADJ_RX);
	//		adjInfo->AdjptRotate.y = pItem->GetDSCAdj(DSC_ADJ_RY);
	//		adjInfo->AdjMove.x = pItem->GetDSCAdj(DSC_ADJ_X);
	//		adjInfo->AdjMove.y = pItem->GetDSCAdj(DSC_ADJ_Y);
	//		adjInfo->strDSC = pItem->GetDeviceDSCID();
	//		adjInfo->nHeight = pItem->GetHeight();
	//		adjInfo->nWidth = pItem->GetWidth();
	//		if(i==0)
	//		{
	//			strCamID = pItem->GetDeviceDSCID();
	//			nWidth = pItem->GetWidth();
	//			nHeight = pItem->GetHeight();
	//		}
	//		m_pMovieRTThread->SetAdjustData(adjInfo);
	//	}
	//	int nMarginX = m_wndDSCViewer.GetMarginX();
	//	int nMarginY = m_wndDSCViewer.GetMarginY();
	//	
	//	m_pMovieRTThread->SetMargin(nMarginX,nMarginY);

	//	//170904 hjcho
	//	CString strHomePath;
	//	strHomePath.Format(_T("%s//bin//ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));

	//	//m_pMovieRTThread->SetWhetherAorB(ESMGetValue(ESM_VALUE_4DPMETHOD));
	//	//m_pMovieRTThread->Set4DMakerPath(strHomePath);

	//	//m_pMovieRTThread->SetFrameSize(nWidth,nHeight);

	//	ESMMovieRTThread(_T("11.11.1.1"),strCamID);
	//}
	if(cData == 'R')
	{
		//ESMEvent* pMsg = new ESMEvent();
		//pMsg->message = WM_ESM_ENV_REMOTECTRL;
		//::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
	}
	if(cData == 'A')
	{
		OnUtilityAJAConnect();
	}
	if(cData == 'L')
	{
		//CESMUtilMultiplexerDlg MultiplexerDlg;
		//MultiplexerDlg.DoModal();
		
		/*ESMEvent* pMsg1 = NULL;
		pMsg1 = new ESMEvent;
		pMsg1->message = WM_ESM_VIEW_SETNEWESTADJ;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg1);*/
		
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_ENV_RTSPSTATEVIEW;
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
	}
}

void CMainFrame::MakeCSShortCut(char cData)
{ 
	if(cData == 'S')
	{
		if(!(m_wndDSCViewer.m_pFrameSelector->GetDSCRecStatus() == DSC_REC_ON))
			DSCMovieCapture();
	}

	if(cData == 'D')
	{
		if(m_wndDSCViewer.m_pFrameSelector->GetDSCRecStatus() == DSC_REC_ON)
			DSCMovieCapture();
	}
}

void CMainFrame::OnESMTimeLineSave()
{
	if ( m_wndTimeLineEditor.CheckSaveValidation() == FALSE )
		return;
	//save
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TIMELINE));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TIMELINE));

	CString szFilter = _T("TimeLine File (*.tl)|*.tl");
	CString strTitle = _T("TimeLine Files Save");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;	

	//-- 2013-10-02 hongsu@esmlab.com
	//-- Default Save Name
	CTime time = CTime::GetCurrentTime();
	CString strFile;
	strFile.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());

	int length = 1024;
	LPWSTR pwsz = strFile.GetBuffer(length);
	dlg.m_ofn.lpstrFile = pwsz;
	strFile.ReleaseBuffer();

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		CString strFilePath;
		strFilePath.Format(_T("%s%s"), strFileName, _T(".tl"));

		CFile file;
		CArchive ar(&file, CArchive::store);
		if(!file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite |CFile::modeNoTruncate))
			return;
		m_wndTimeLineEditor.SaveTimeLineTemplate(ar);
		ar.Close();
		file.Close();
	}
}

void CMainFrame::OnESMTimeLineLoad()
{
	//load
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TIMELINE));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TIMELINE));

	CString szFilter = _T("TimeLine File (*.tl)|*.tl|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();

		CFile file;
		if(!file.Open(strFileName, CFile::modeRead))
		{
			ESMLog(0,_T("Load TimeLine Template File [%s]") , strFileName);
			return;
		}

		CArchive ar(&file, CArchive::load);
		m_wndTimeLineEditor.LoadTimeLineTemplate(ar);

		ar.Close();
		file.Close();
	}
}

void CMainFrame::OnESMMovieTemplateSave()
{
	if ( m_wndTimeLineEditor.CheckSaveValidation() == FALSE )
		return;

	//save
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TEMPLATE));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TEMPLATE));

	CString szFilter = _T("Movie Template File (*.mvtm)|*.mvtm");
	CString strTitle = _T("Movie Template Files Save");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;	

	//-- 2013-10-02 hongsu@esmlab.com
	//-- Default Save Name
	CTime time = CTime::GetCurrentTime();
	CString strFile;
	strFile.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());

	int length = 1024;
	LPWSTR pwsz = strFile.GetBuffer(length);
	dlg.m_ofn.lpstrFile = pwsz;
	strFile.ReleaseBuffer();

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		CString strFilePath;
		strFilePath.Format(_T("%s%s"), strFileName, _T(".mvtm"));

		CFile file;
		CArchive ar(&file, CArchive::store);
		if(!file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite |CFile::modeNoTruncate))
			return;
		m_wndTimeLineEditor.SaveMovieTemplate(ar);
		ar.Close();
		file.Close();
	}
}

void CMainFrame::OnESMMovieTemplateLoad()
{
	//load
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TEMPLATE));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TEMPLATE));

	CString szFilter = _T("Movie Template File (*.mvtm)|*.mvtm|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();

		CFile file;
		if(!file.Open(strFileName, CFile::modeRead))
		{
			ESMLog(0,_T("Load TimeLine Template File [%s]") , strFileName);
			return;
		}

		CArchive ar(&file, CArchive::load);
		m_wndTimeLineEditor.LoadMovieTemplate(ar);

		ar.Close();
		file.Close();
	}
}

void  CMainFrame::OnDSCPrism(void)		
{
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
	{
		ESMLog(5,_T("DSC Adjust... [OnDSCPrism ESM_ADJ_FILM_STATE_MOVIE]"));
		ESMKzonePrism kzone;

		CObArray arDSCList;
		ESMGetDSCList(&arDSCList);
		CDSCItem* pItem = NULL;
		for( int i =0 ;i < arDSCList.GetSize(); i++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(i);
			kzone.AddDscInfo(pItem->GetDeviceDSCID(), pItem->m_strInfo[DSC_INFO_LOCATION]);
		}
		if( kzone.DoModal()== IDOK)
		{
			int nMarginX = m_wndDSCViewer.GetMarginX();
			int nMarginY = m_wndDSCViewer.GetMarginY();

			//-- 2014-07-15 hjjang
			//-- Set Adjust Info
			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_ESM_MOVIE_SET_ADJUSTINFO;
			pMsg->nParam1 = nMarginX;
			pMsg->nParam2 = nMarginY;
			::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}
	}

	else if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
	{
		ESMLog(5,_T("DSC Adjust... [OnDSCPrism ESM_ADJ_FILM_STATE_PICTURE]"));
		ESMKzonePrism kzone;

		CObArray arDSCList;
		ESMGetDSCList(&arDSCList);
		CDSCItem* pItem = NULL;
		kzone.SetMovieState(ESM_ADJ_FILM_STATE_PICTURE);
		for( int i =0 ;i < arDSCList.GetSize(); i++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(i);
			kzone.AddDscInfo(pItem->GetDeviceDSCID(), _T(""));
		}
		if( kzone.DoModal() == IDOK)
		{
			//vector<DscAdjustInfo*>* pArrDscInfo = NULL;
			//kzone.GetDscInfo(&pArrDscInfo);

			//m_wndDSCViewer.SetDscAdjustData(pArrDscInfo);

			//// Margin 계산
			//m_wndDSCViewer.SetMargin(pArrDscInfo);

			//int nMarginX = m_wndDSCViewer.GetMarginX();
			//int nMarginY = m_wndDSCViewer.GetMarginY();

			//ESMEvent* pMsg = new ESMEvent;
			//pMsg->nParam1 = nMarginX;
			//pMsg->nParam2 = nMarginY;
			//pMsg->message = WM_ESM_MOVIE_SET_ADJUSTINFO;
			//::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}
		//m_wndDSCViewer.DoAdjust();	
	}
}

//16-12-09 wgkim@esmlab.com
void  CMainFrame::OnDSCTemplate(void)
{
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
	{
		ESMLog(5,_T("DSC Adjust... [OnDSCTemplate ESM_ADJ_FILM_STATE_MOVIE]"));
		CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
		
		CESMTemplateDlg templateDlg;

		CObArray arDSCList;
		ESMGetDSCList(&arDSCList);
		CDSCItem* pItem = NULL;

		templateDlg.AddTemplateMgr(m_pTemplateMgr);
		m_pTemplateMgr->DscClear();
		for( int i =0 ;i < arDSCList.GetSize(); i++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(i);
			m_pTemplateMgr->AddDscInfo(pItem->GetDeviceDSCID(), pItem->m_strInfo[DSC_INFO_LOCATION],pItem->m_bReverse);
		}

		if( templateDlg.DoModal() == IDOK || IDCANCEL)
		{
			pMainWnd->m_wndTimeLineEditor.SetTemplateMgr(m_pTemplateMgr);
			pMainWnd->m_wndFrame.m_pDlgFrameList->SetTemplateMgr(m_pTemplateMgr);
			//pMainWnd->m_wndFrameEx.m_pRemoteCtrlDlg->SetTemplateMgr(m_pTemplateMgr);
		}
	}
}

//180703 wgkim
void CMainFrame::OnRecalibration(void)
{
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
	{
		ESMLog(5,_T("DSC Adjust... [OnRecalibration ESM_ADJ_FILM_STATE_MOVIE]"));
		CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();

		CESMRecalibrationDlg recalibrationDlg;

		CObArray arDSCList;
		ESMGetDSCList(&arDSCList);
		CDSCItem* pItem = NULL;

		recalibrationDlg.AddRecalibrationMgr(m_pRecalibrationMgr);
		m_pRecalibrationMgr->DscClear();
		for( int i =0 ;i < arDSCList.GetSize(); i++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(i);
			m_pRecalibrationMgr->AddDscInfo(pItem->GetDeviceDSCID(), pItem->m_strInfo[DSC_INFO_LOCATION],
				pItem->m_bReverse);
		}

		if( recalibrationDlg.DoModal()== IDOK)
		{

		}
	}
}

void CMainFrame::OnAutoAdjust(void)
{
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
	{
		CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();

		CESMAutoAdjustDlg pAutoAdjustDlg;	

		CObArray arDSCList;
		ESMGetDSCList(&arDSCList);
		CDSCItem* pItem = NULL;

		pAutoAdjustDlg.SetAutoAdjustMgr(m_pAutoAdjustMgr);
		pAutoAdjustDlg.SetTemplateMgr(m_pTemplateMgr);

		m_pAutoAdjustMgr->DscClear();
		m_pTemplateMgr->DscClear();
		for( int i =0 ;i < arDSCList.GetSize(); i++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(i);
			m_pAutoAdjustMgr->
				AddDscInfo(pItem->GetDeviceDSCID(), pItem->m_strInfo[DSC_INFO_LOCATION],
				pItem->m_bReverse);
			m_pTemplateMgr->
				AddDscInfo(pItem->GetDeviceDSCID(), pItem->m_strInfo[DSC_INFO_LOCATION],
				pItem->m_bReverse);
		}

		if( pAutoAdjustDlg.DoModal()== IDOK)
		{
			//wgkim 180718
			vector<DscAdjustInfo*>* pArrDscInfo = NULL;
			m_pAutoAdjustMgr->GetDscInfo(&pArrDscInfo);

			if(m_pAutoAdjustMgr->IsEmptyAdjustInfo())
			{
				int nFlag = AfxMessageBox(
					_T("Adjust value is not set. Do you want to apply?\n \YES : Adjust values are applied as 0\n \NO : Only position, tracking, and K-Zone except Adjust"), MB_YESNO);
				if(nFlag ==  IDNO)
				{
					CString strFileName;
					strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
					if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT))
					{
						m_pTemplateMgr->LoadSquarePointData(strFileName);
					}

					if(ESMGetValue(ESM_VALUE_AUTOADJUST_KZONE))
					{
						m_pAutoAdjustMgr->LoadSquarePointDataForKZone(strFileName);
					}

					return;
				}
			}

			m_wndDSCViewer.SetDscAdjustData(pArrDscInfo);

			// Margin 계산
			m_wndDSCViewer.SetMargin(pArrDscInfo);

			int nMarginX = m_wndDSCViewer.GetMarginX();
			int nMarginY = m_wndDSCViewer.GetMarginY();

			//-- 2014-07-15 hjjang
			//-- Set Adjust Info
			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_ESM_MOVIE_SET_ADJUSTINFO;
			pMsg->nParam1 = nMarginX;
			pMsg->nParam2 = nMarginY;
			::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);

			CString strFileName;
			strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
			if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT))
			{
				m_pTemplateMgr->LoadSquarePointData(strFileName);
			}

			if(ESMGetValue(ESM_VALUE_AUTOADJUST_KZONE))
			{
				m_pAutoAdjustMgr->LoadSquarePointDataForKZone(strFileName);
			}
		}
	}
}

void CMainFrame::OnUtilityArcCalculator()
{
	CESMUtilArcCalculatorDlg utilArcCalculatorDlg;
	utilArcCalculatorDlg.DoModal();
}

void CMainFrame::OnUtilityActualMeasurement()
{
	CESMUtilActualMeasurementDlg utilActualMeasurementDlg;
	utilActualMeasurementDlg.DoModal();
}

void CMainFrame::OnFrameSelectView(BOOL bAuto)
{
	if(!ESMGetLoadingFrame())
	{
		/*ESMSetLoadingFrame(TRUE);*/

		//ESMLog(1,_T("Select Frame Load Start"));
		if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
			AfxMessageBox(_T("Current server mode is video Record mode. \n Available in production or Both mode only."));
		else if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
		{
			ESMLog(1,_T("Select Frame Load Start"));
			//Load
			int nCount = ESMGetViewPointNum();
			CString strTime;
			strTime.Format(_T("SelectLastPointTime%d"), nCount);

			ESMLog(1, _T("Set Select Time %s"), strTime);

			int nCaptureTime = ESMGetRecordingInfoInt(strTime, nCount);
			m_wndDSCViewer.m_pFrameSelector->SetSelectDscIndex(nCaptureTime);
			m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nCaptureTime, nCount);

			m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(nCount);
			::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
		}
		else //ESM_SERVERMODE_BOTH
		{
			//Load
			int nCount = ESMGetViewPointNum();
			CString strTime;
			strTime.Format(_T("SelectPointTime%d"), nCount);

			//ESMLog(1, _T("Set Select Time %s"), strTime);

			int nCaptureTime = ESMGetRecordingInfoInt(strTime, nCount);
//#ifndef _FILE_TEMP
			//jhhan File Temp
			//if(!ESMGetRecordFileSplit())
			{
				if(ESMGetRecordStatus())
				{
					int nGap = 0;
					CObArray arDSCList;
					ESMGetDSCList(&arDSCList);
					if(arDSCList.GetCount())
					{
						for( int i = 0 ;i < arDSCList.GetCount(); i++)
						{
							CDSCItem* pItem = NULL;
							pItem	= (CDSCItem*)arDSCList.GetAt(i);

							if (pItem == NULL)
								continue;

							if (pItem->GetDeviceModel().IsEmpty())
								continue;

							if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
							{								
								nGap = sync_frame_time_after_sensor_on;
							}	

							break;
						}
					}

					int nIngTime = m_wndDSCViewer.m_pDSCMgr->GetRecordTime();
					nIngTime = (ESMGetTick() + nGap - nIngTime)/10;

					ESMLog(5, _T("Point : %d, Current : %d --------[%d]"), nCaptureTime, nIngTime, nIngTime-nCaptureTime);

					int nTime = 2000;

					//GH5 Load Frame 5초
					/*CObArray arDSCList;
					ESMGetDSCList(&arDSCList);
					if(arDSCList.GetCount())
					{
						CDSCItem* pItem = NULL;
						pItem	= (CDSCItem*)arDSCList.GetAt(0);
						if(pItem)
						{
							if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
								nTime = 5000;
						}
					}*/

					if(nIngTime - nCaptureTime < nTime)
						return;

				}
 			}
//#endif
			if(ESMGetDetectInfo(&m_stDetectInfo, nCaptureTime))
			{
				//m_stDetectInfo -> 4DA 정보 전송
				ESMEvent * pEvent = new ESMEvent();
				pEvent->message = WM_ESM_NET_DETECT_INFO;
				pEvent->nParam1 = m_stDetectInfo.size();
				pEvent->nParam2 = nCaptureTime;
				pEvent->pParam = (LPARAM)&m_stDetectInfo;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pEvent);
			}

			m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nCaptureTime, nCount);

			//파일 유무 체크
			if(m_wndDSCViewer.m_pFrameSelector->SetSelectDscIndex(nCaptureTime) == -1)
				return;
			
			ESMLog(1,_T("Select Frame Load Start : [%s,%d]"), strTime,nCaptureTime);
			//ESMLog(1, _T("Set Select Time %s"), strTime);

			m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(nCount);
			::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
		}
		if(bAuto)
			ESMSetKeyValue(VK_F10);
		else
			ESMSetKeyValue(VK_F11);//AutoDetecting Key value

		ESMLog(1,_T("Select Frame Load End"));
		ESMSetLoadingFrame(TRUE);
	}
	else
	{
		ESMLog(1,_T("Select Frame Loading..."));
		ESMSetLoadingFrame(FALSE);
	}
}

BOOL CMainFrame::Add4DPMgr(CString strDscId)
{
	BOOL bFind = FALSE;

	CString strIp = Load4DAPath(strDscId);
	if(strDscId.IsEmpty())
	{
		return FALSE;
	}

	for(int i=0; i< m_ar4DPRcList.GetCount(); i++)
	{
		CESMRCManager * pRcMgr = NULL;
		pRcMgr = (CESMRCManager * )m_ar4DPRcList.GetAt(i);
		if(pRcMgr->GetIP() == strIp)
		{
			if(pRcMgr->GetSourceDSC() == strDscId)
			{
				ESMLog(5, _T("[%s] IP : %s"), strDscId, strIp);

				bFind = TRUE;
			}
			else
			{
				if(pRcMgr->m_bConnected)
					pRcMgr->DisconnectToProcessor();

				if(pRcMgr)
				{
					delete pRcMgr;
					pRcMgr = NULL;
				}
				m_ar4DPRcList.RemoveAt(i);
			}
			break;
		}

	}

	if(bFind != TRUE)
	{
		BOOL bConn = FALSE;
		CESMRCManager* pRcMgr = NULL;
		if(strIp.IsEmpty() == FALSE)
		{
			pRcMgr = new CESMRCManager(strIp, 0);
			pRcMgr->CreateThread();

			pRcMgr->SetSourceDSC(strDscId);

			bConn = pRcMgr->ConnectToProcessor();
		}
		ESMLog(5, _T("[Connect] 4DP (%s) - [%s]"), strIp, bConn?_T("Success"):_T("Fail"));

		if(bConn)
		{
			m_ar4DPRcList.Add((CObject*)pRcMgr);
		}
		else
		{
			if(pRcMgr)
			{
				delete pRcMgr;
				pRcMgr = NULL;
			}
		}
	}


	return TRUE;
}

void CMainFrame::DeleteAll4DPMgr(int nRemoteID/* = 0*/)
{
	for(int i=0; i < m_ar4DPRcList.GetCount(); i++)
	{
		CESMRCManager * pRcMgr = NULL;
		pRcMgr = (CESMRCManager * )m_ar4DPRcList.GetAt(i);
		if(nRemoteID == 0)
		{
			if(pRcMgr)
			{	
				if(pRcMgr->m_bConnected)
					pRcMgr->DisconnectToProcessor();
				if(pRcMgr)
				{
					delete pRcMgr;
					pRcMgr = NULL;
				}

				m_ar4DPRcList.RemoveAt(i);
			}
		}else
		{
			if(pRcMgr)
			{
				if(pRcMgr->GetID() == nRemoteID)
				{
					if(pRcMgr->m_bConnected)
						pRcMgr->DisconnectToProcessor();
					/*if(pRcMgr)
					{
						delete pRcMgr;
						pRcMgr = NULL;
					}*/

					m_ar4DPRcList.RemoveAt(i);

					break;
				}
			}
		}
		
	}
}


BOOL CMainFrame::RequestData(CString strPath)
{
	g_mutex4DP.Lock();

	CString strDscId;
	strDscId = ESMGetDSCIDFromPath(strPath);
	BOOL bFind = FALSE;
	
	for(int i=0; i< m_ar4DPRcList.GetCount(); i++)
	{
		CESMRCManager * pRcMgr = NULL;
		pRcMgr = (CESMRCManager * )m_ar4DPRcList.GetAt(i);

		if(pRcMgr->GetSourceDSC() == strDscId)
		{
			pRcMgr->RequestData(strPath);
			ESMLog(5, _T("[%s] IP : %s / %s"), strDscId, pRcMgr->GetIP(), strPath);

			bFind = TRUE;

			break;
		}

	}

	if(bFind != TRUE)
	{
		//ESMLog(0, _T("FAIL - [%s] %s"), strDscId, strPath);
		g_mutex4DP.Unlock();
		return FALSE;
	}

	g_mutex4DP.Unlock();
	
	return TRUE;
}
void CMainFrame::OnESMVerificationInfo()
{
	ESMVerifyInfo();
}

void CMainFrame::OnESMFixedxApply()
{
	CString strPathName;

//#if _DEBUG
//	CFileDialog _dlg(
//		TRUE,
//		_T("XML"),
//		NULL,
//		OFN_FILEMUSTEXIST
//		|OFN_PATHMUSTEXIST
//		|OFN_HIDEREADONLY
//		|OFN_LONGNAMES
//		|OFN_NOCHANGEDIR
//		,
//		_T("XML files (*.XML)|*.XML|")
//		_T("All files (*.*)|*.*|")
//		_T("|")
//		,
//		this
//		);
//	TCHAR strCurrDir[_MAX_PATH];
//	::GetCurrentDirectory( _MAX_PATH, strCurrDir );
//	_dlg.m_ofn.lpstrInitialDir = strCurrDir;
//	_dlg.m_ofn.lpstrTitle = _T("Load XML skin");
//	if( _dlg.DoModal() != IDOK )
//		return;
//	strPathName = _dlg.GetPathName();
//	ASSERT( ! strPathName.IsEmpty() );
//#endif
	CString strSkin;
	strSkin.Format(_T("%s\\UI\\%s"),ESMGetPath(ESM_PATH_IMAGE), _T("BlackDiamond.xml"));
	
	if(strPathName.IsEmpty() != TRUE)
		strSkin = strPathName;
	
	CESMFileOperation fo;
	if(fo.IsFileExist(strSkin))
	{
		CExtPaintManagerSkin * pPM = new CExtPaintManagerSkin;
		if( ! pPM->m_Skin.Load( LPCTSTR(strSkin) ) )
		{
			::AfxMessageBox( _T("Failed to load skin.") );
			delete pPM;
			return;
		}
		g_PaintManager.InstallPaintManager( pPM );
	}

	//Invalidate();
}

void CMainFrame::OnESMDockingApply()
{
	g_PaintManager.InstallPaintManager(RUNTIME_CLASS(CExtPaintManagerOffice2010_R2_Black));
	//Invalidate();
}
BOOL CMainFrame::OnResync_RecStop()
{	
	if(m_wndDSCViewer.m_bRecordStep == ESM_NOMAL_START)
	{
		//(CAMREVISION)
		//m_bStart = FALSE;
		ESMSetRecordStatus(FALSE);
		DSCMovieCapture();
	}
	if(m_wndDSCViewer.m_bRecordStep == ESM_PAUSESTEP_RESUME)
	{
		DSCMoviePause();	
	}

	int nAll = m_wndDSCViewer.GetItemCount();
	CDSCItem* pItem = NULL;
	while(nAll--)
	{
		pItem = m_wndDSCViewer.GetItemData(nAll);
		pItem->SetDSCStatus(SDI_STATUS_REC_FINISH);
	}	

	//ESMSetWait(1000);

#if 0
	ESMSetWait(3500);

	/*시작*/
	if(ESMGetDelayDirection() == _T("1"))
	{
		//m_DelayDirection == DSC_REC_DELAY_REVERSEDIRECTION;
		MakeShortCut('1');
	}else if(ESMGetDelayDirection() == _T("2"))
	{
		//m_DelayDirection == DSC_REC_DELAY_FORWARDDIRECTION;
		MakeShortCut('2');
	}
	else if(ESMGetDelayDirection() == _T("3"))
	{
		//m_DelayDirection == DSC_REC_DELAY_REVERSEDIRECTION;
		MakeShortCut('3');
	}else if(ESMGetDelayDirection() == _T("4"))
	{
		//m_DelayDirection == DSC_REC_DELAY_FORWARDDIRECTION;
		MakeShortCut('4');
	}
	else if(ESMGetDelayDirection() == _T("5"))
	{
		//m_DelayDirection == DSC_REC_DELAY_FORWARDDIRECTION;
		MakeShortCut('5');
	}
	else
	{
		return FALSE;
	}
#endif
	return TRUE;
}

BOOL CMainFrame::OnResync_RecStart()
{	
	/*시작*/
	if(ESMGetDelayDirection() == _T("1"))
	{
		//m_DelayDirection == DSC_REC_DELAY_REVERSEDIRECTION;
		MakeShortCut('1');
	}else if(ESMGetDelayDirection() == _T("2"))
	{
		//m_DelayDirection == DSC_REC_DELAY_FORWARDDIRECTION;
		MakeShortCut('2');
	}
	else if(ESMGetDelayDirection() == _T("3"))
	{
		//m_DelayDirection == DSC_REC_DELAY_REVERSEDIRECTION;
		MakeShortCut('3');
	}else if(ESMGetDelayDirection() == _T("4"))
	{
		//m_DelayDirection == DSC_REC_DELAY_FORWARDDIRECTION;
		MakeShortCut('4');
	}
	else if(ESMGetDelayDirection() == _T("5"))
	{
		//m_DelayDirection == DSC_REC_DELAY_FORWARDDIRECTION;
		MakeShortCut('5');
	}
	else if(ESMGetDelayDirection() == _T("6"))
	{
		//m_DelayDirection == DSC_REC_DELAY_FORWARDDIRECTION;
		MakeShortCut('6');
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}

void CMainFrame::OnRecording(char cDirection)
{		
	if(ESMGetFrameRate() == MOVIE_FPS_UNKNOWN)
	{
		ESMLog(5, _T("------------------------------------------"));
		ESMLog(5, _T("$$$$$$$$$$$ Unknown framerate."));
		int nAll = m_wndDSCViewer.GetItemCount();
		CDSCItem* pItem = NULL;		
		while(nAll--)
		{
			pItem = m_wndDSCViewer.GetItemData(nAll);
			int nStatus = pItem->GetDSCStatus();
			CString strID = pItem->GetDeviceDSCID();
			if (nStatus == SDI_STATUS_CONNECTED)
			{
				ESMSetDefaultCameraID(strID);
				ESMLog(5, _T("$$$$$$$$$$$ Movie size information is requested. [%s] model:[%s]"), strID, pItem->GetDeviceModel());
				if (pItem->GetDeviceModel().CompareNoCase(_T("NX1")) == 0)
					ESMSetDevice(SDI_MODEL_NX1);
				else if (pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
					ESMSetDevice(SDI_MODEL_GH5);
				else
					ESMSetDevice(SDI_MODEL_GH5);	// default gh5

				m_wndDSCViewer.m_pListView->m_pView->GetPropertyListView(strID);
				break;
			}
		}		
	}

	StartPropertyDiffCheck();
	StartPropertyMinMaxInfoCheck();

	BOOL bUseResync = ESMGetValue(ESM_VALUE_USE_RESYNC);

	if (ESMGetRecordStatus())	// 녹화중일때..
	{
		if (bUseResync)
		{
			StartReSync();
		}
		return;
	}

	//jhhan 190201 Live 채널 전송
	ESMSendToLiveList();

	//ResetFrame();
	/*HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ResetFrameThread, (void*)this, 0, NULL);
	CloseHandle(hSyncTime);*/

	int nAll = m_wndDSCViewer.GetItemCount();
	CDSCItem* pItem = NULL;

	if (m_bReSyncFlag == FALSE)
	{
		// check record finish
		while(nAll--)
		{
			pItem = m_wndDSCViewer.GetItemData(nAll);
			if (pItem->GetRecordReady() == FALSE)
			{
				ESMLog(5, _T("$$$$$$$$$$$ Not ready to record. "));
				return;
			}
		}
	}
		
	if (bUseResync)
	{
		ESMLog(5, _T("$$$$$$$$$$$ OnRecording. bUseResync: %d"), bUseResync);
		m_bGetTickFirstFlag = FALSE;
		m_bReSyncRecStopFinishFlag = FALSE;
		SetTimer(TIMER_RECORD_RESYNC_GET_TICK, 1000*30, NULL);
	}

	CString strDirection;
	strDirection.Format(_T("%c"), cDirection);
	//AfxMessageBox(strDirection);

	//jhhan 180611
	ESMLog(5, _T("[Key Press] Recoding : %s"), strDirection);

	ESMSetDelayDirection(strDirection);
// 	if(ESMGetFrameRate() == -1)
// 	{
// 		m_wndDSCViewer.m_pListView->m_pView->GetPropertyListView();
// 		ESMLog(5, _T("------------------------------------------"));
// 		Sleep(100);
// 	}

	CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
	pMainWnd->m_wndDSCViewer.m_pListView->m_strCapture = strDirection;
	pMainWnd->m_wndDSCViewer.m_pListView->Invalidate();

	//jhhan 180907
	ESMSetDeleteFolder(ESMGetFrameRecord());
	//m_strDeleteFolder = ESMGetFrameRecord();

	//Make 4DM 18.06.26 joonho.kim
	m_FileMgr->m_bInit = TRUE;
	m_FileMgr->SetCurTime();
	if(!m_FileMgr->MovieDoneSaveProfile())
	{
		ESMLog(0, _T("4DM File Save Error!!!"));
		AfxMessageBox(_T("4DM File Save Error!!!"));
		return;
	}
	/////////////////////////////

	//Set Start Time   17.04.11 joonho.kim
	int nTest = ESMGetValue(ESM_VALUE_SENSORONTIME);
	int nTickTime = ESMGetTick();
	int nRecTime = nTickTime+ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN);
	nTickTime = nTickTime + ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN) + sync_frame_time_after_sensor_on + ESMGetValue(ESM_VALUE_SENSORONTIME);
	m_wndDSCViewer.m_pDSCMgr->SetRecordTime(nTickTime);
	//////////////////////////////////////////////////

 	nAll = m_wndDSCViewer.GetItemCount();
	pItem = NULL;

	while(nAll--)
	{
		pItem = m_wndDSCViewer.GetItemData(nAll);
		pItem->SetDSCStatus(SDI_STATUS_REC_READY);
	}

	m_wndDSCViewer.m_pFrameSelector->InitLine();

	
	if(cDirection == '1')
	{
		ESMSetRecordingInfoint(_T("OrderDirection"), DSC_REC_DELAY_REVERSEDIRECTION);
		m_DelayDirection = DSC_REC_DELAY_REVERSEDIRECTION;
	}else if(cDirection == '2')
	{
		ESMSetRecordingInfoint(_T("OrderDirection"), DSC_REC_DELAY_FORWARDDIRECTION);
		m_DelayDirection = DSC_REC_DELAY_FORWARDDIRECTION;
	}else if(cDirection == '3')
	{
		ESMSetRecordingInfoint(_T("OrderDirection"), DSC_REC_DELAY_DIRECTION_3);
		m_DelayDirection = DSC_REC_DELAY_DIRECTION_3;
	}
	else if(cDirection == '4')
	{
		ESMSetRecordingInfoint(_T("OrderDirection"), DSC_REC_DELAY_DIRECTION_4);
		m_DelayDirection = DSC_REC_DELAY_DIRECTION_4;
	}
	else if(cDirection == '5')
	{
		ESMSetRecordingInfoint(_T("OrderDirection"), DSC_REC_DELAY_DIRECTION_5);
		m_DelayDirection = DSC_REC_DELAY_DIRECTION_5;
	}
	else if(cDirection == '6')
	{
		ESMSetRecordingInfoint(_T("OrderDirection"), DSC_REC_DELAY_DIRECTION_6);
		m_DelayDirection = DSC_REC_DELAY_DIRECTION_6;
	}
	else
	{
		ESMSetRecordingInfoint(_T("OrderDirection"), DSC_REC_DELAY_FORWARDDIRECTION);
		m_DelayDirection = DSC_REC_DELAY_FORWARDDIRECTION;
	}
	
	if(m_wndDSCViewer.m_bRecordStep == ESM_RECORD_STOP)
	{
		//(CAMREVISION)
		//m_bStart = TRUE;
		ESMSetRecordStatus(TRUE);
		DSCMovieCapture();

		if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetConnectAJANetwork())
		{
			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = F4DC_PROGRAM_STATE;
			pMsg->nParam1 = m_DelayDirection;
			ESMSendAJANetwork(pMsg);
		}
	}

	//OnHttpSender(ESM_NET_START);
	//ESMSetRecordState(1);

	//hjcho - 171020
	ESMSetRemoteState(TRUE);

	//wgkim - 171110
	ESMClearArrGapAvg();

	if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT))
	{
		LoadTemplateSquaresProfile();

		m_wndTimeLineEditor.SetTemplateMgr(m_pTemplateMgr);
		m_wndFrame.m_pDlgFrameList->SetTemplateMgr(m_pTemplateMgr);
		//			m_wndFrameEx.m_pRemoteCtrlDlg->SetTemplateMgr(m_pTemplateMgr);
	}

	m_wndDashBoard.LoadInfo();

	if(ESMGetValue(ESM_VALUE_CEREMONYUSE) && ESMGetValue(ESM_VALUE_CEREMONY_STILLIMAGE))
		ESMClearPushTime();	
}

unsigned WINAPI CMainFrame::DeleteCompact(LPVOID param)
{
	CMainFrame* pMain = (CMainFrame*)param;

	CESMFileOperation fo;
	CString strPath;
	CString strDel = ESMGetDeleteFolder();
	if(strDel.IsEmpty() != TRUE)
	{
		strPath.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_LOCALMOVIE), strDel);
		ESMLog(5, _T("[Delete] %s"), strPath);
	}
	else
	{
		return 0;
	}

	vector<int> _nStoredTime = ESMGetStoredTime();

#ifndef _FIND_RENAME_DELETE_RENAME
	int nTime = ESMGetValue(ESM_VALUE_STORED_TIME);
	//jhhan 180907 - NX1 (60p, 120p 파일 분산 저장에 따른 삭제 보관 값 적용)
	if(ESMGetDevice() == SDI_MODEL_NX1)
	{
		if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P)
		{
			nTime = nTime * 2;
		}else if(ESMGetFrameRate() == MOVIE_FPS_FHD_120P)
		{
			nTime = nTime * 4;
		}
	}
	

	BOOL bRename = FALSE;

	//find & rename
	for(int _i = 0; _i < _nStoredTime.size(); _i++)
	{
		int nExt = _nStoredTime[_i];
		ESMLog(5, _T("Stored Time : %d"), nExt);
		for(int _j = nExt - nTime/*5*/; _j <= nExt + nTime/*5*/; _j++)
		{
			if(_j < 0)
				continue;

			CString strExt;
			strExt.Format(_T("*_%d.mp4"), _j);

			CStringArray _lstMp4u;
			fo.GetFileCountList(_lstMp4u, strPath, strExt);
			for(int _k = 0; _k < _lstMp4u.GetCount(); _k++)
			{
				CString _ori = _lstMp4u.GetAt(_k);
				CString _re = _ori;
				_re.Replace(_T("mp4"), _T("mp"));
				//_re.Append(_T("u"));

				fo.Rename(_ori, _re);

				bRename = TRUE;
			}
		}
	}

	//Initialize
	ESMInitStoredTime();
	
	if(bRename == TRUE)
	{
		//find & delete
		CStringArray _lstDel;
		fo.GetFileCountList(_lstDel, strPath, _T("*.mp4"));
		ESMLog(5, _T("DEL : %d"), _lstDel.GetCount());
		for(int _d = 0; _d < _lstDel.GetCount(); _d++)
		{
			CString _strDel = _lstDel.GetAt(_d);
			fo.Delete(_strDel);
		}


		//find & rename
		CStringArray _lst;
		fo.GetFileCountList(_lst, strPath, _T("*.mp"));
		ESMLog(5, _T("STORE : %d"), _lst.GetCount());
		for(int _e = 0; _e < _lst.GetCount(); _e++)
		{
			CString _ori = _lst.GetAt(_e);
			CString _re = _ori;
			_re.Replace(_T("mp"), _T("mp4"));
			fo.Rename(_ori, _re);
		}
	}else
	{
		fo.Delete(strPath, TRUE);
	}
#endif

#ifdef _SEARCH_DELETE
	CStringArray _lst;
	fo.GetFileCountList(_lst, strPath, _T("*.mp4"));
	int nTotal = _lst.GetCount() - 1;
	while(nTotal >= 0)
	{
		CString _str = _lst.GetAt(nTotal);
		int _start = _str.ReverseFind('_') + 1;
		int _end = _str.ReverseFind('.');
		CString _val = _str.Mid(_start+1, _end-_start);
		int _num = -1;
		if(ESMGetNumValidate(_val))
		{
			_num = _ttoi(_val);
		}
		BOOL bDel = TRUE;
		if(_num >=0)
		{
			for(int i = 0; i < _nStoredTime.size(); i++)
			{
				int nNum = _nStoredTime[i];

				if(_num >= nNum - 5 && _num <= nNum + 5)
				{
					bDel = FALSE;
					break;
				}
			}
		}
		
		if(bDel)
		{
			fo.Delete(_str);
		}
		nTotal--;
	}

	ESMInitStoredTime();
#endif


	return 0;
}

void CMainFrame::OnFrameAutoSelect()
{
	if(!ESMGetLoadingFrame())
	{
		/*ESMSetLoadingFrame(TRUE);*/

		//ESMLog(1,_T("Select Frame Load Start"));
		if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
			AfxMessageBox(_T("Current server mode is video Record mode. \n Available in production or Both mode only."));
		else if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
		{
			ESMLog(1,_T("Select Frame Load Start"));
			//Load
			int nCount = ESMGetViewPointNum();
			CString strTime;
			strTime.Format(_T("SelectLastPointTime%d"), nCount);

			ESMLog(1, _T("Set Select Time %s"), strTime);

			int nCaptureTime = ESMGetRecordingInfoInt(strTime, nCount);
			m_wndDSCViewer.m_pFrameSelector->SetSelectDscIndex(nCaptureTime);
			m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nCaptureTime, nCount);

			m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(nCount);
			::SetFocus(m_wndDSCViewer.m_pFrameSelector->GetSafeHwnd());
		}
		else //ESM_SERVERMODE_BOTH
		{
			//Load
			int nCount = ESMGetViewPointNum();
			CString strTime;
			strTime.Format(_T("SelectPointTime%d"), nCount);

			int nCaptureTime = ESMGetRecordingInfoInt(strTime, nCount);
			{
				if(ESMGetRecordStatus())
				{
					int nGap = 0;
					CObArray arDSCList;
					ESMGetDSCList(&arDSCList);
					if(arDSCList.GetCount())
					{
						for( int i = 0 ;i < arDSCList.GetCount(); i++)
						{
							CDSCItem* pItem = NULL;
							pItem	= (CDSCItem*)arDSCList.GetAt(i);

							if (pItem == NULL)
								continue;

							if (pItem->GetDeviceModel().IsEmpty())
								continue;

							if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
							{
								nGap = sync_frame_time_after_sensor_on;
							}

							break;
						}
					}

					int nIngTime = m_wndDSCViewer.m_pDSCMgr->GetRecordTime();
					nIngTime = (ESMGetTick() + nGap - nIngTime)/10;

					ESMLog(5, _T("Point : %d, Current : %d --------[%d]"), nCaptureTime, nIngTime, nIngTime-nCaptureTime);

					int nTime = 2000;

					if(nIngTime - nCaptureTime < nTime)
						return;

				}
			}

			m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nCaptureTime, nCount);

			//파일 유무 체크
			if(m_wndDSCViewer.m_pFrameSelector->SetSelectDscIndex(nCaptureTime) == -1)
				return;

			if(m_bAutoDetect == FALSE) // AutoDetecting Start
			{
				m_bAutoDetect = TRUE;

				ESMLog(1,_T("Select Frame Load Start : [%s,%d]"), strTime,nCaptureTime);

				THREAD_AUTO_DETECT* pThreadDetect = new THREAD_AUTO_DETECT;
				pThreadDetect->pMainFrm = this;
				pThreadDetect->nCaptureTime = nCaptureTime;
				pThreadDetect->nCount		= nCount;

				HANDLE hSyncTime = NULL;
				hSyncTime = (HANDLE)_beginthreadex(NULL,0,_ThreadAutoDetect,(void*)pThreadDetect,0,NULL);
				CloseHandle(hSyncTime);
			}
			else
			{
				ESMLog(0,_T("AutoDetecting is in progress....!"));
			}
		}
		
	}
	else
	{
		ESMLog(1,_T("Select Frame Loading..."));
		ESMSetLoadingFrame(FALSE);
	}
}
#define STR_SECTION_FIND	_T("External")
#define STR_SECTION_START	_T("Start")

//4DMaker -> AT
#define STR_AT_OPTION		_T("OPT")//int
#define STR_AT_BASE_PATH	_T("BASE_PATH")//string
#define STR_AT_EXPAND_PATH	_T("EXPAND_PATH")//string
#define STR_AT_TOTAL_FRAME	_T("TOTAL_FRAME")//int
#define STR_AT_MOVIE_IDX	_T("MOVIE_IDX")//int
#define STR_AT_FRAME_IDX	_T("FRAME_IDX")//int
#define STR_AT_START_IDX	_T("START_IDX")//int
#define STR_AT_END_IDX		_T("END_IDX")//int
//AT -> 4DMaker
#define STR_AT_FIND			_T("Find")
#define STR_MOVIE_IDX		_T("Movie")
#define STR_FRAME_IDX		_T("Frame")

unsigned WINAPI CMainFrame::_ThreadAutoDetect(LPVOID param)
{
	THREAD_AUTO_DETECT* pDetect = (THREAD_AUTO_DETECT*)param;
	
	CMainFrame* pMainFrm = pDetect->pMainFrm;
	int nCaptureTime = pDetect->nCaptureTime;
	int nCount		 = pDetect->nCount;
	
	delete pDetect; pDetect = NULL;

	CDSCViewer* pDSCViewer = &(pMainFrm->m_wndDSCViewer);
	CDSCFrameSelector* pDSCSelector = pDSCViewer->m_pFrameSelector;

	//AutoDetecting Start...
	CString strSelectDSC = ESMGetSelectDSC();
	track_info stTrack = ESMGetTrackInfo();

	BOOL bAuto = FALSE;
	for(int i = 0 ; i < 4 ; i ++)
	{
		if(strSelectDSC == stTrack.st_AxisInfo[i].strCamID)
		{
			bAuto = TRUE;
			break;
		}
	}

	//BOOL bProcessRun = IsExistProcess(_T("darknet(2).exe"));
	
	if(bAuto && ESMGetValue(ESM_VALUE_AUTODETECT) /*&& bProcessRun */)
	{
		CESMFileOperation fo;
		CESMIni ini;
		CString strTemp;

		BOOL bFirst = FALSE;
		BOOL bExpand = FALSE;

		int nFrameTotal = movie_frame_per_second;
		int nSearchRange = 5;
		int nStartIdx = 0,nEndIdx = 0;

		int nMovieIdx = 0, nRealFrameIdx = 0;
		int nFrameIdx = ESMGetFrameIndex(nCaptureTime);
		ESMGetMovieIndex(nFrameIdx,nMovieIdx,nRealFrameIdx);

		CString strBasePath,strExpandPath = _T("NONE");

		strBasePath = ESMGetMoviePath(strSelectDSC,nFrameIdx);

		nStartIdx = nRealFrameIdx - nSearchRange;
		nEndIdx	  = nRealFrameIdx + nSearchRange;

		if(nRealFrameIdx + nSearchRange >= nFrameTotal) // Expand
		{
			//nStartIdx = nRealFrameIdx - nSearchRange;
			nEndIdx	  = (nRealFrameIdx+nSearchRange) - nFrameTotal;

			bExpand = TRUE;
			int nExpandIdx = ESMGetFrameIndex(nCaptureTime + 1000);
			strExpandPath = ESMGetMoviePath(strSelectDSC,nExpandIdx);
		}
		else if(nRealFrameIdx - nSearchRange < 0)
		{
			nStartIdx = (nRealFrameIdx-nSearchRange) + nFrameTotal;
			//nEndIdx	  = nRealFrameIdx + nSearchRange;

			bExpand = TRUE;
			bFirst = TRUE;
			int nExpandIdx = ESMGetFrameIndex(nCaptureTime - 1000);
			strExpandPath = ESMGetMoviePath(strSelectDSC,nExpandIdx);
		}

		CString strFileName;
		strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_AUTODETECTING);

		if(!fo.IsFileExist(strFileName))
		{
			CFile file;
			file.Open(strFileName,CFile::modeCreate);
			file.Close();
		}

		//Inifile 초기화
		if(ini.SetIniFilename(strFileName))
		{
			CString str;
			str.Format(_T("0"));
			ini.WriteString(STR_SECTION_FIND,STR_AT_FIND,str);
			ini.WriteString(STR_SECTION_FIND,STR_MOVIE_IDX,str);
			ini.WriteString(STR_SECTION_FIND,STR_FRAME_IDX,str);
		}

		CString strOpt;
		if(bExpand)//2개 
		{
			if(bFirst)
			{
				strOpt.Format(_T("0 %s %s %d %d %d %d %d"),
				strExpandPath,strBasePath,nFrameTotal,nMovieIdx-1,nRealFrameIdx,nStartIdx,nEndIdx);
				
				ini.WriteString(STR_SECTION_FIND,STR_AT_BASE_PATH,strExpandPath);
				ini.WriteString(STR_SECTION_FIND,STR_AT_EXPAND_PATH,strBasePath);
			}
			else
			{
				//파일 확인
				if(fo.IsFileExist(strExpandPath) == FALSE)
				{
					while(1)
					{
						if(fo.IsFileExist(strExpandPath))
							break;

						Sleep(1);
					}
				}
				strOpt.Format(_T("0 %s %s %d %d %d"),
					strBasePath,strExpandPath,nFrameTotal,nMovieIdx,nRealFrameIdx,nStartIdx,nEndIdx);

				ini.WriteString(STR_SECTION_FIND,STR_AT_BASE_PATH,strBasePath);
				ini.WriteString(STR_SECTION_FIND,STR_AT_EXPAND_PATH,strExpandPath);
			}
			strTemp.Format(_T("%d"),0);
			ini.WriteString(STR_SECTION_FIND,STR_AT_OPTION,strTemp);
		}
		else
		{
			strOpt.Format(_T("1 %s %d %d %d %d %d"),
			strBasePath,nFrameTotal,nMovieIdx,nRealFrameIdx,nStartIdx,nEndIdx);
			
			strTemp.Format(_T("%d"),1);
			ini.WriteString(STR_SECTION_FIND,STR_AT_OPTION,strTemp);

			ini.WriteString(STR_SECTION_FIND,STR_AT_BASE_PATH,strBasePath);
		}
		strTemp.Format(_T("%d"),nFrameTotal);
		ini.WriteString(STR_SECTION_FIND,STR_AT_TOTAL_FRAME,strTemp);
		strTemp.Format(_T("%d"),nMovieIdx);
		ini.WriteString(STR_SECTION_FIND,STR_AT_MOVIE_IDX,strTemp);
		strTemp.Format(_T("%d"),nRealFrameIdx);
		ini.WriteString(STR_SECTION_FIND,STR_AT_FRAME_IDX,strTemp);
		strTemp.Format(_T("%d"),nStartIdx);
		ini.WriteString(STR_SECTION_FIND,STR_AT_START_IDX,strTemp);
		strTemp.Format(_T("%d"),nEndIdx);
		ini.WriteString(STR_SECTION_FIND,STR_AT_END_IDX,strTemp);

		ESMLog(5,strOpt);

		int nFind = 0;
		strTemp.Format(_T("1"));
		ini.WriteString(STR_SECTION_FIND,STR_SECTION_START,strTemp);

		while(1)
		{
			CString strFinish = ini.GetString(STR_SECTION_FIND,STR_AT_FIND);
			nFind = _ttoi(strFinish);
			if(nFind)
				break;

			Sleep(1);
		}
		//Read INI File
		if(ini.GetInt(STR_SECTION_FIND,STR_AT_FIND) == 2)//Find
		{
			int nFindMovieIdx = ini.GetInt(STR_SECTION_FIND,STR_MOVIE_IDX);
			int nFindFrameIdx = ini.GetInt(STR_SECTION_FIND,STR_FRAME_IDX);

			int nFindTime
				= nFindMovieIdx * 1000 + movie_next_frame_time * nFindFrameIdx;

			pDSCSelector->SetSelectTimeLine(nFindTime,nCount);

			ESMLog(5,_T("[AutoDetecting] Frame Find - %d(%d,%d)"),nFindTime,nFindMovieIdx,nFindFrameIdx);
		}

		//CString strCmd;//Processor가 존재하면?
		//strCmd.Format(_T("%s\\bin\\PROCESSNAME.exe"),ESMGetPath(ESM_PATH_HOME));

		//if(fo.IsFileExist(strCmd))
		//{
		//	//Launch SHELLEXE
		//	SHELLEXECUTEINFO lpExecInfo;
		//	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
		//	lpExecInfo.lpFile = strCmd;
		//	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
		//	lpExecInfo.hwnd = NULL;  
		//	lpExecInfo.lpVerb = L"open";
		//	lpExecInfo.lpParameters = strOpt;
		//	lpExecInfo.lpDirectory = NULL;
		//	lpExecInfo.nShow = SW_HIDE;
		//	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
		//	ShellExecuteEx(&lpExecInfo);

		//	// wait until the process is finished
		//	if (lpExecInfo.hProcess != NULL)
		//	{
		//		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		//		::CloseHandle(lpExecInfo.hProcess);
		//	}

		//	lpExecInfo.lpFile = _T("exit");
		//	lpExecInfo.lpVerb = L"close";
		//	lpExecInfo.lpParameters = NULL;

		//	if(TerminateProcess(lpExecInfo.hProcess,0))
		//	{
		//		lpExecInfo.hProcess = 0;
		//		ESMLog(5,_T("End"));
		//	}
	}
	//Focus 이동
	for(int i = 0 ; i < 10 ; i++)
		::SetFocus(pDSCSelector->GetSafeHwnd());

	pDSCSelector->SelectFrameLine(nCount);

	ESMSetKeyValue(VK_F10);//AutoDetecting Key value

	ESMLog(1,_T("Select Frame Load End"));
	ESMSetLoadingFrame(TRUE);

	pMainFrm->m_bAutoDetect = FALSE;

	return TRUE;
}
unsigned WINAPI CMainFrame::_ThreadLaunchExternalDetect(LPVOID param)
{

	return FALSE;
}

//181221 hjcho
void CMainFrame::MakeCAShorCut(char cData)
{
	if(cData >=  VK_NUMPAD1 && cData <= VK_NUMPAD9)
	{
		if(ESMGetFrameRecord().GetLength() < 5)
		{
			ESMLog(5, _T("Not Recorded\n"));
			return ;
		}
		//jhhan 181219
		VIEW_STRUCT _stView;
		_stView = m_wndTimeLineEditor.GetViewTempalte(cData - VK_NUMPAD0 + m_nFKey);
		_stView.nSelectTime = m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();

		m_wndTimeLineEditor.m_pTimeLineView->m_pViewMovie->AddViewStruct(_stView);
	}
}