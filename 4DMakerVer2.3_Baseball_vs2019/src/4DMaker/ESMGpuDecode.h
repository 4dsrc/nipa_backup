#pragma once

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#define WINDOWS_LEAN_AND_MEAN
#include <windows.h>
#include <windowsx.h>
#endif

// CUDA Header includes
#include "dynlink_nvcuvid.h"  // <nvcuvid.h>
#include "dynlink_cuda.h"     // <cuda.h>
#include "dynlink_cudaD3D11.h" // <cudaD3D11.h>
#include "dynlink_builtin_types.h"	  // <builtin_types.h>

// CUDA utilities and system includes
#include "helper_functions.h"
#include "helper_cuda_drvapi.h"

// cudaDecodeD3D11 related helper functions
#include "FrameQueue.h"
#include "VideoSource.h"
#include "VideoParser.h"
#include "VideoDecoder.h"
#include "ImageDX.h"

#include "cudaProcessFrame.h"
#include "cudaModuleMgr.h"

// Include files
#include <math.h>
#include <memory>
#include <iostream>
#include <cassert>

class CESMGpuDecode
{
public:
	CESMGpuDecode(void);
	~CESMGpuDecode(void);


public:
	CString m_strWindowName;

public:
	void GPUDecoding(CString strPath, int nIndex);
	void parseCommand(CString strPath);
	bool loadVideoSource(const char *video_file,unsigned int &width    , unsigned int &height,	unsigned int &dispWidth, unsigned int &dispHeight);
	bool initD3D11(HWND hWnd,int *pbTCC);
	HRESULT initCudaResources(int bUseInterop, int bTCC);
	void initCudaVideo();
	HRESULT initD3D11Surface(unsigned int nWidth, unsigned int nHeight);
	void renderVideoFrame(HWND hWnd, bool bUseInterop);
	bool copyDecodedFrameToTexture(unsigned int &nRepeats, int bUseInterop, int *pbIsProgressive);
	void cudaPostProcessFrame(CUdeviceptr *ppDecodedFrame, size_t nDecodedPitch,	CUdeviceptr *ppTextureData,  size_t nTexturePitch,CUmodule cuModNV12toARGB,	CUfunction fpCudaKernel, CUstream streamID);
	void cudaPostProcessFrame(CUdeviceptr *ppDecodedFrame, size_t nDecodedPitch,	CUarray array,CUmodule cuModNV12toARGB,CUfunction fpCudaKernel, CUstream streamID);
	void SaveFrameAsYUV(unsigned char *pdst,const unsigned char *psrc,int width, int height, int pitch);
	HRESULT reinitCudaResources();
	HRESULT cleanup(bool bDestroyContext);
	void freeCudaResources(bool bDestroyContext);
	HRESULT CESMGpuDecode::drawScene(int field_num);
	void computeFPS(HWND hWnd, bool bUseInterop);
	void printStatistics();
	void yuv420_to_rgb( unsigned char *in, unsigned char *out, int w, int h );
	void YUV_lookup_table();
};

