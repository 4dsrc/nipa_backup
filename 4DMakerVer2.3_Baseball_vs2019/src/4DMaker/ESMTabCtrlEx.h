#pragma once


// CESMTabCtrlEx

class CESMTabCtrlEx : public CTabCtrl
{
	DECLARE_DYNAMIC(CESMTabCtrlEx)

public:
	CESMTabCtrlEx();
	virtual ~CESMTabCtrlEx();

protected:
	DECLARE_MESSAGE_MAP()
public:
	CBrush m_brush;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};


