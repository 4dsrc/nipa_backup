////////////////////////////////////////////////////////////////////////////////
//
//	ESMNetworkListCtrl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-10-08
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"

#include "SdiDefines.h"
#include "ESMReportCtrl.h"
#include "ESMTCPSocket.h"
#include "ESMHeaderCtrl.h"
#include <vector>

class CESMRCManager;
class CESMNetworkDlg;

class CESMAutoAdjustListCtrl : public CListCtrl
{
public:
	CESMAutoAdjustListCtrl(void);
public:
	~CESMAutoAdjustListCtrl(void);
	void Clear();
	void Init();
	void Save(CESMNetworkDlg* pParent);
	void SetStatus(int Row, CString strStatus);
//	void SetUseCamStatusColor(BOOL bUse);

private:
	HANDLE	m_hThreadHandle;
	CRITICAL_SECTION m_CrSockket;

	CESMHeaderCtrl m_HeaderCtrl;

protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	virtual void PreSubclassWindow();
};
