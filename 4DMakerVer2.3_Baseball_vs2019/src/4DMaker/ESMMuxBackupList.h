#pragma once
//#include "ESMUtilRemoteCtrl.h"
#include "opencv2/core.hpp"
#include "opencv2/opencv_modules.hpp"
#include "opencv2/opencv.hpp"
#include "ESMIndex.h"
#include "ESMDefine.h"
#include "resource.h"
#include "ESMMuxBackupDlg.h"

class CESMMuxBackUpDlg;

enum
{
	LIST_NUM = 0,
	LIST_4DM,
	LIST_4DMINDEX,
	LIST_SELTIME,
	LIST_TEMPLATE,
	LIST_DBINFO,
};
class CESMMuxBackUpList : public CListCtrl
{
public:
	CESMMuxBackUpList();
	~CESMMuxBackUpList();

	void Init(CESMMuxBackUpDlg* pParent);
	void AddFiles(CString strPath);
	CESMMuxBackUpDlg* m_pMuxDlg;
	DECLARE_MESSAGE_MAP()
	afx_msg void OnDropFiles(HDROP hDropInfo);
};