// ESMTemplateDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#ifdef _4DMODEL
#include "4DModeler.h"
#else
#include "4DMaker.h"
#endif
#include "ESMRecalibrationDlg.h"
#include "ESMRecalibrationImage.h"
#include "ESMRecalibrationMgr.h"
#include "afxdialogex.h"
#include "FFmpegManager.h"
#include "ESMIni.h"
#include "ESMCtrl.h"

#include "ESMFileOperation.h"
#include "ESMFunc.h"
#include "ESMMovieMgr.h"
#include "ESMImgMgr.h"

#include<highgui.h>
#include<cv.h>

#include <math.h>

#define PI 3.1415926535897932384
#define TIMER_CHANGE_NEXT_ITEM	0x8001
#define TIME_CHANGE_NEXT_ITEM	700

// ESMAdjustMgrDlg 대화 상자입니다.
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


enum{
	FIRST_ADJUST_VIEW = 0,
};

enum{
	ADJUST_IMAGE = 0,
	MAX_IDX,
};

enum{
	ADJUST_LEFT_UP = 0,
	ADJUST_LEFT_DOWN,
	ADJUST_RIGHT_UP,
	ADJUST_RIGHT_DOWN,
};
enum{
	T_LB_D = 0,
	T_RB_D,
	T_PEAK_D,
	B_PEAK_D,
};
IMPLEMENT_DYNAMIC(CESMRecalibrationDlg, CDialogEx)


CESMRecalibrationDlg::CESMRecalibrationDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMRecalibrationDlg::IDD, pParent)
{
	m_nSelectPos = 0;
	m_nSelectedNum = -1;
	m_nHeight = 0;
	m_nWidth = 0;
	m_frameIndex= 0;
	m_nPosX = 0, m_nPosY = 0;	
	m_MarginX = 0;
	m_MarginY = 0;	
	m_hTheadHandle = NULL;	
	m_bThreadCloseFlag = FALSE;
	m_bStabilizationCloseFlag = FALSE;

	m_dFrameTime = 0;
	m_frameIndex = 0;

	m_bLoadAdjustImageAll = FALSE;
	m_bInitializedTracking = FALSE;	
	//  m_strFrameTime = _T("");	
	m_nTrackingOption = 0;

	m_background.CreateSolidBrush(RGB(44,44,44));
	m_AdjustDscList.SetUseCamStatusColor(FALSE);
}

CESMRecalibrationDlg::~CESMRecalibrationDlg()
{		
	WaitForSingleObject(m_hTheadHandle, INFINITE);
	CloseHandle(m_hTheadHandle);
}

void CESMRecalibrationDlg::OnOK()
{
	m_bThreadCloseFlag = TRUE;
	m_bStabilizationCloseFlag = TRUE;
	CDialog::OnOK();
}

void CESMRecalibrationDlg::OnCancel()
{	
	m_bThreadCloseFlag = TRUE;
	m_bStabilizationCloseFlag = TRUE;
	CDialog::OnCancel();
}

void CESMRecalibrationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX,	IDC_TEMPLATE_DSCLIST,	m_AdjustDscList);
	DDX_Control(pDX,	 IDC_TEMPLATE_COMBO,	m_ctrlCheckPoint);
	DDX_Control(pDX,	IDC_ST_PRISMIMAGE,  m_StAdjustImage);
	DDX_Control(pDX, IDC_EDIT_TEMPLATE_CENTER, m_ctrlCenterLine);	
	//  DDX_Text(pDX, IDC_EDIT_FRAMETIME, m_strFrameTime);
	DDX_Radio(pDX, IDC_RADIO_STABILIZATION0, m_nTrackingOption);
	//DDX_Radio(pDX, IDC_RADIO_STABILIZATION1, m_nTrackingOption);
	DDX_Control(pDX, IDC_BTN_LOAD, m_btnLoad);
	DDX_Control(pDX, IDC_BTN_SAVE, m_btnSave);
	DDX_Control(pDX, IDC_BTN_RESET, m_btnReset);
	DDX_Control(pDX, IDC_BTN_TRACKING, m_btnRun);
	DDX_Control(pDX, IDC_BTN_MODIFYADJ, m_btnSaveCalibration);
	DDX_Control(pDX, IDOK, m_btnOk);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CESMRecalibrationDlg, CDialogEx)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_TEMPLATE_DSCLIST, &CESMRecalibrationDlg::OnCustomdrawTcpList)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_TEMPLATE_DSCLIST, &CESMRecalibrationDlg::OnLvnItemchangedAdjust)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEWHEEL()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONUP()
	ON_BN_CLICKED(IDC_BTN_RESET, &CESMRecalibrationDlg::OnBnClickedBtnAdjustPointReset)
	ON_BN_CLICKED(IDC_BTN_SAVE, &CESMRecalibrationDlg::OnBnClickedBtn2ndSavePoint)
	ON_BN_CLICKED(IDC_BTN_LOAD, &CESMRecalibrationDlg::OnBnClickedBtn2ndLoadPoint)
	ON_BN_CLICKED(IDC_BTN_TRACKING, &CESMRecalibrationDlg::OnBnClickedBtnTracking)
	ON_BN_CLICKED(IDC_BTN_MODIFYADJ, &CESMRecalibrationDlg::OnBnClickedBtnModifyAdj)
//	ON_BN_CLICKED(IDC_BTN_SETFRAMENUM, &CESMRecalibrationDlg::OnBnClicktdBtnSetFrameNum)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_RADIO_STABILIZATION0, IDC_RADIO_STABILIZATION1, &CESMRecalibrationDlg::OnBnClickedRadioUsingTracking)
	ON_WM_CTLCOLOR()
	
END_MESSAGE_MAP()


void CESMRecalibrationDlg::OnCustomdrawTcpList(NMHDR* pNMHDR, LRESULT* pResult)
{	
	NMLVCUSTOMDRAW* lplvcd = (NMLVCUSTOMDRAW*)pNMHDR;
	if(lplvcd->nmcd.dwDrawStage == CDDS_PREPAINT)
	{
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if(lplvcd->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
	{
		*pResult = CDRF_NOTIFYSUBITEMDRAW;
	}
	else if(lplvcd->nmcd.dwDrawStage == (CDDS_ITEMPREPAINT | CDDS_SUBITEM))
	{
		if( lplvcd->nmcd.dwItemSpec == m_nSelectedNum)
		{
			if(m_nSelectPos * 2<=  lplvcd->iSubItem && m_nSelectPos * 2 + 1 >=  lplvcd->iSubItem)
			{
				lplvcd->clrTextBk = RGB(200, 200, 255);
				lplvcd->clrText = RGB(255, 0, 0);
			}
			else
			{
				lplvcd->clrTextBk = RGB(255, 255, 255);
				lplvcd->clrText = RGB(0, 0, 0);
			}
		}
		*pResult = CDRF_DODEFAULT;
	}
}

void CESMRecalibrationDlg::setDscListItem(CListCtrl* pList, int rowIndex, int colIndex, int data)
{
	CString strText;
	strText.Format(_T("%d"), data);
	pList->SetItemText(rowIndex, colIndex, strText);	
}

int CESMRecalibrationDlg::getDSCListItem(CListCtrl* pList, int rowIndex, int colIndex)
{
	CString strSelect = pList->GetItemText(rowIndex, colIndex);
	return _ttoi(strSelect);	
}

void CESMRecalibrationDlg::SetMovieState(int nMovieState )
{
	m_nMovieState = nMovieState;
}

BOOL CESMRecalibrationDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_nMovieState = ESM_ADJ_FILM_STATE_MOVIE;

	m_AdjustDscList.InsertColumn(0, _T("ID"), 0, 30);
	m_AdjustDscList.InsertColumn(1, _T("DscId"), 0, 44);
	m_AdjustDscList.InsertColumn(2, _T("Point1 X"), 0, 45);
	m_AdjustDscList.InsertColumn(3, _T("Point1 Y"), 0, 45); 
	m_AdjustDscList.InsertColumn(4, _T("Point2 X"), 0, 45);
	m_AdjustDscList.InsertColumn(5, _T("Point2 Y"), 0, 45);	
	
	m_AdjustDscList.InsertColumn(10, _T("AdjustX"), 0, 65);
	m_AdjustDscList.InsertColumn(11, _T("AdjustY"), 0, 65);
	m_AdjustDscList.InsertColumn(12, _T("Angle"), 0, 65);
	m_AdjustDscList.InsertColumn(13, _T("ModifiedX"), 0, 65);
	m_AdjustDscList.InsertColumn(14, _T("ModifiedY"), 0, 65);	

	m_AdjustDscList.SetExtendedStyle(/*LVS_EX_GRIDLINES|*/LVS_EX_FULLROWSELECT);

	m_ctrlCheckPoint.AddString(_T("T LB"));
	m_ctrlCheckPoint.AddString(_T("T RB"));
	m_ctrlCheckPoint.AddString(_T("T Peak"));
	m_ctrlCheckPoint.AddString(_T("B Peak"));
	m_ctrlCheckPoint.SetCurSel(0);

	LoadData();
	Showimage();
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	LoadPointData(strFileName);
	
	m_Move = 0;
	m_Scale = 0;
	m_Number = 0;

	CalcAdjustData();

	// hide
	GetDlgItem(IDC_EDIT_TEMPLATE_SIZE)->ShowWindow(FALSE);
	GetDlgItem(IDC_STATIC_BOXSIZE)->ShowWindow(FALSE);
	GetDlgItem(IDC_STATIC_SELECT_POS)->ShowWindow(FALSE);
	GetDlgItem(IDC_TEMPLATE_COMBO)->ShowWindow(FALSE);

	UpdateData(FALSE);

	return TRUE;	
}

void CESMRecalibrationDlg::AddRecalibrationMgr(CESMRecalibrationMgr *recalibrationMgr)
{
	m_pRecalibrationMgr = recalibrationMgr;
}

void CESMRecalibrationDlg::SavePointData(CString strFileName)
{
	CESMIni ini;
	CFile file;
	file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite);
	file.Close();

	if(!ini.SetIniFilename (strFileName))
		return;

	ini.WriteInt(_T("MovieSize"), _T("HEIGHT"), m_nHeight);
	ini.WriteInt(_T("MovieSize"), _T("WIDTH"), m_nWidth);

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);
		ini.WriteString(strSelect, _T("POINT1 X"), m_AdjustDscList.GetItemText(i, 2));
		ini.WriteString(strSelect, _T("POINT1 Y"), m_AdjustDscList.GetItemText(i, 3));
		ini.WriteString(strSelect, _T("POINT2 X"), m_AdjustDscList.GetItemText(i, 4));
		ini.WriteString(strSelect, _T("POINT2 Y"), m_AdjustDscList.GetItemText(i, 5));		
	}
}

void CESMRecalibrationDlg::LoadPointData(CString strFileName)
{
	CESMIni ini;
	if(!ini.SetIniFilename (strFileName))
		return;

	int nHeight = 0, nWidth = 0;
	nHeight = ini.GetInt(_T("MovieSize"), _T("HEIGHT"), 0);
	nWidth = ini.GetInt(_T("MovieSize"), _T("WIDTH"), 0);
	if(nHeight != 0)
		m_nHeight = nHeight;

	if(nWidth != 0)
		m_nWidth = nWidth;

	CString strData;
	//stRecalibrationInfo axis;
	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);
		strData = ini.GetString(strSelect, _T("POINT1 X"));
		m_AdjustDscList.SetItemText(i, 2, strData);		

		strData = ini.GetString(strSelect, _T("POINT1 Y"));
		m_AdjustDscList.SetItemText(i, 3, strData);		

		strData = ini.GetString(strSelect, _T("POINT2 X"));
		m_AdjustDscList.SetItemText(i, 4, strData);		

		strData = ini.GetString(strSelect, _T("POINT2 Y"));
		m_AdjustDscList.SetItemText(i, 5, strData);				
	}
}

void CESMRecalibrationDlg::Showimage()
{
	CListCtrl* pList;
	CESMRecalibrationImage* pAdjustImg;
	int nCount, nMaxCount;
	
	pList = &m_AdjustDscList;
	pAdjustImg = &m_StAdjustImage;

	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );
	if( nSelectedItem < 0)
		return;	

	m_nSelectedNum = nSelectedItem;
	CString strSelect = pList->GetItemText(nSelectedItem, 1);
	DscAdjustInfo* pAdjustInfo = NULL;
	pAdjustInfo = m_pRecalibrationMgr->GetDscAt(nSelectedItem);	

	if(pAdjustInfo->pBmpBits != NULL || pAdjustImg->m_memDC == NULL)
	{
		DrawImage();	
	}
	else
	{
		DeleteImage();
	}
}

void CESMRecalibrationDlg::DrawImage()
{
	CListCtrl* pList = &m_AdjustDscList;	
	CESMRecalibrationImage* pAdjustImg = &m_StAdjustImage;
	DscAdjustInfo* pAdjustInfo = NULL;	

	UpdateData();
	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );
	if( nSelectedItem < 0)
		return;

	m_nSelectedNum = nSelectedItem;
	CString strSelect = pList->GetItemText(nSelectedItem, 1);
	CString strLine3;
	GetDlgItem(IDC_EDIT_TEMPLATE_CENTER)->GetWindowText(strLine3);

	pAdjustInfo = m_pRecalibrationMgr->GetDscAt(nSelectedItem);
	
	if(pAdjustInfo->pBmpBits == NULL)
	{
		AfxMessageBox(_T("Wait Loading..."));
		return;
	}

	Mat src = m_pRecalibrationMgr->ApplyAdjustImageUsingCPU(nSelectedItem, m_MarginX, m_MarginY);

	int size = src.total() * src.elemSize();
	pAdjustImg->SetImageBuffer(src.data, src.cols, src.rows, 3, NULL);	

	pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
	pAdjustImg->DrawLine(pAdjustInfo->nHeight/2, RGB(0,0,0));
	pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));

	//	
	CString strPos1_X = pList->GetItemText(nSelectedItem, 2);
	CString strPos1_Y = pList->GetItemText(nSelectedItem, 3);
	CString strPos2_X = pList->GetItemText(nSelectedItem, 4);
	CString strPos2_Y = pList->GetItemText(nSelectedItem, 5);

	int nPos1_X, nPos1_Y, nPos2_X, nPos2_Y;
	nPos1_X = _ttoi(strPos1_X);
	nPos1_Y = _ttoi(strPos1_Y);
	nPos2_X = _ttoi(strPos2_X);
	nPos2_Y = _ttoi(strPos2_Y);

	if(nPos1_X != 0)
	{
		CPoint x_1 = CPoint(nPos1_X, nPos1_Y);			
		CPoint x_2 = CPoint(nPos2_X, nPos2_Y);

		CRect rtOutLine = CRect(x_1, x_2);
		m_StAdjustImage.DrawRect(rtOutLine, RGB(255,0,0));
		m_StAdjustImage.DrawPoint(x_1.x + (x_2.x-x_1.x)/2, x_1.y + (x_2.y-x_1.y)/2, RGB(0,255,0));	
	}
	//

	
	pAdjustImg->Redraw();	

	m_nSelectPos = 0;
}

void CESMRecalibrationDlg::DeleteImage()
{
	CESMRecalibrationImage* pAdjustImg = &m_StAdjustImage;	
	pAdjustImg->m_memDC->FillSolidRect(0,0,m_StAdjustImage.m_nImageWidth,m_StAdjustImage.m_nImageHeight,GetSysColor(COLOR_BTNFACE));
}

void CESMRecalibrationDlg::LoadData()
{
	m_AdjustDscList.DeleteAllItems();
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();
	for(int i =0; i< m_pRecalibrationMgr->GetDscCount(); i++)
	{		
		CString strTp;
		strTp.Format(_T("%d"), i + 1);
		m_AdjustDscList.InsertItem(i, strTp);
		strTp = m_pRecalibrationMgr->GetDscAt(i)->strDscName;
		m_AdjustDscList.SetItemText(i, 1, strTp);
		CString strDSC = m_AdjustDscList.GetItemText(i,1);

		m_pRecalibrationMgr->AddAdjInfo(pMovieMgr->GetAdjustData(strDSC));		
		stAdjustInfo adjInfo = m_pRecalibrationMgr->GetAdjInfoAt(i);

		CESMImgMgr* pImgMgr = new CESMImgMgr();
		pImgMgr->SetMargin(m_MarginX, m_MarginY, adjInfo);
	}	

	CString strTemp;
	m_RdImageViewColor = 0;
	m_nRdDetectColor = 0;	
	m_pRecalibrationMgr->GetResolution(m_nWidth,m_nHeight);

	if( m_hTheadHandle ) 
	{
		CloseHandle(m_hTheadHandle);
		m_hTheadHandle = NULL;
	}

	m_hTheadHandle = (HANDLE) _beginthreadex(NULL, 0, GetMovieDataThread, (void *)this, 0, NULL);

	GetDlgItem(IDC_EDIT_TEMPLATE_SIZE)->SetWindowText(_T("5"));
}

unsigned WINAPI CESMRecalibrationDlg::GetMovieDataThread(LPVOID param)
{	
	CESMRecalibrationDlg* pAdjustMgrDlg = (CESMRecalibrationDlg*)param;
	DscAdjustInfo* pAdjustInfo = NULL;

	//wgkim 170728
	int nMovieIndex = 0;
	int nRealFrameIndex = 0;
	int nShiftFrame = ESMGetSelectedFrameNumber();
	ESMGetMovieIndex(nShiftFrame, nMovieIndex, nRealFrameIndex);

	for(int i = 0; i < pAdjustMgrDlg->m_pRecalibrationMgr->GetDscCount(); i++)
		pAdjustMgrDlg->m_pImageLoadFlag.push_back(FALSE);

	for(int i =0; i< pAdjustMgrDlg->m_pRecalibrationMgr->GetDscCount(); i++)
	{
		pAdjustInfo = pAdjustMgrDlg->m_pRecalibrationMgr->GetDscAt(i);
		FFmpegManager FFmpegMgr(FALSE);
		CString strFolder, strDscId, strDscIp;
		strDscId = pAdjustInfo->strDscName;
		strDscIp = pAdjustInfo->strDscIp;
		strFolder = ESMGetMoviePath(strDscId, nShiftFrame);	
		BOOL bReverse = pAdjustInfo->bReverse;
		if( strFolder == _T(""))
		{	
			return 0;
		}	
		
		BYTE* pBmpBits = NULL;
		int nWidht =0, nHeight = 0;
		FFmpegMgr.GetCaptureImage(strFolder, &pBmpBits, nRealFrameIndex/*pAdjustMgrDlg->m_frameIndex*/, &nWidht, &nHeight, ESMGetGopSize(),bReverse);
		if( pBmpBits != NULL)
		{
			pAdjustInfo->pBmpBits = pBmpBits;
			pAdjustInfo->nWidht = nWidht;
			pAdjustInfo->nHeight = nHeight;
		}		
		pAdjustMgrDlg->m_pImageLoadFlag[i] = TRUE;

		if(pAdjustMgrDlg->m_bThreadCloseFlag == TRUE)
		{
			return 0;
		}
		
	}	
	pAdjustMgrDlg->m_bLoadAdjustImageAll = TRUE;
	return 0;
}

void CESMRecalibrationDlg::OnLvnItemchangedAdjust(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	Showimage();
	Invalidate();
	*pResult = 0;
}

void CESMRecalibrationDlg::OnRButtonDown(UINT nFlags, CPoint point)
{	
	CListCtrl* pList;
	CESMRecalibrationImage* pAdjustImg;
		
	pList = &m_AdjustDscList;
	pAdjustImg = &m_StAdjustImage;

	DrawImage();
	pAdjustImg->Redraw();

	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );

	CString LocStr;
	m_ctrlCheckPoint.GetLBText(m_ctrlCheckPoint.GetCurSel(),LocStr);

	CString strSize;
	CString strText;
	GetDlgItem(IDC_EDIT_TEMPLATE_SIZE)->GetWindowText(strSize);
	pAdjustImg->m_nCircleSize =  _ttoi(strSize) * 4;
		
	m_pDownPoint = GetPointFromMouse(point);	
	
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	SavePointData(strFileName);
	CDialogEx::OnRButtonDown(nFlags, point);
}

void CESMRecalibrationDlg::OnRButtonUp(UINT nFlags, CPoint point)
{	
	CListCtrl* pList;
	CESMRecalibrationImage* pAdjustImg;
		
	pList = &m_AdjustDscList;
	pAdjustImg = &m_StAdjustImage;

	DrawImage();

	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );

	CString LocStr;
	m_ctrlCheckPoint.GetLBText(m_ctrlCheckPoint.GetCurSel(),LocStr);

	CString strSize;
	CString strText;
	GetDlgItem(IDC_EDIT_TEMPLATE_SIZE)->GetWindowText(strSize);
	pAdjustImg->m_nCircleSize =  _ttoi(strSize) * 4;

	CRect rtOutLine(m_pDownPoint, GetPointFromMouse(point));
	m_StAdjustImage.DrawRect(rtOutLine, RGB(255,0,0));	

	m_pUpPoint = GetPointFromMouse(point);
	setSquareFromMouse();
	pAdjustImg->Redraw();
	
	//SetSquarePointListItem(nSelectedItem);
	setDscListItem(pList, nSelectedItem, 2, (double)m_pRect.x);
	setDscListItem(pList, nSelectedItem, 3, (double)m_pRect.y);		
	setDscListItem(pList, nSelectedItem, 4, (double)m_pRect.width + m_pRect.x);
	setDscListItem(pList, nSelectedItem, 5, (double)m_pRect.height + m_pRect.y);
	
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	SavePointData(strFileName);
	CDialogEx::OnRButtonUp(nFlags, point);
}

void CESMRecalibrationDlg::setSquareFromMouse()
{
	m_pRect.x = m_pDownPoint.x;
	m_pRect.y = m_pDownPoint.y;
	m_pRect.width = m_pUpPoint.x - m_pDownPoint.x;
	m_pRect.height = m_pUpPoint.y - m_pDownPoint.y;
}

void CESMRecalibrationDlg::SetSquarePointListItem(int nSelectedItem)
{
	CListCtrl *pList = &m_AdjustDscList;

	CPoint x_2 = CPoint(m_pRect.width*m_pObjectTracking->_samplingScale + m_pRect.x*m_pObjectTracking->_samplingScale, m_pRect.height*m_pObjectTracking->_samplingScale + m_pRect.y*m_pObjectTracking->_samplingScale);
	CPoint x_1 = CPoint(m_pRect.x*m_pObjectTracking->_samplingScale, m_pRect.y*m_pObjectTracking->_samplingScale);			

	setDscListItem(pList, nSelectedItem, 2, (double)x_1.x);
	setDscListItem(pList, nSelectedItem, 3, (double)x_1.y);		
	setDscListItem(pList, nSelectedItem, 4, (double)x_2.x);
	setDscListItem(pList, nSelectedItem, 5, (double)x_2.y);
}

void CESMRecalibrationDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	CESMRecalibrationImage* pAdjustImg;
	pAdjustImg = &m_StAdjustImage;

	CString strPos;
	CRect rect;
	pAdjustImg->GetWindowRect(&rect);
	ScreenToClient(&rect);
	int nPos = 0;
	if( point.x - rect.left < 0 || point.x > rect.right || point.y - rect.top < 0 || point.y > rect.bottom)
	{		
		CDialogEx::OnMouseMove(nFlags, point);
		return;
	}

	switch(nFlags)
	{		
	case MK_RBUTTON:		
		DrawImage();
		CRect rtOutLine(m_pDownPoint, GetPointFromMouse(point));
		m_StAdjustImage.DrawRect(rtOutLine, RGB(255,0,0));
		m_StAdjustImage.DrawPoint(m_pDownPoint.x + (GetPointFromMouse(point).x-m_pDownPoint.x)/2, m_pDownPoint.y + (GetPointFromMouse(point).y-m_pDownPoint.y)/2, RGB(0,255,0));
		pAdjustImg->Redraw();		
		break;
	}

	//-- 2014-08-31 hongsu@esmlab.com
	//-- Get Image Position from Mouse Point
	CPoint ptImage;
	ptImage = GetPointFromMouse(point);

	strPos.Format(_T("%d"), ptImage.x);
	m_nPosX = ptImage.x - 1;
	GetDlgItem(IDC_TEMPLATE_MOUSEPOSX)->SetWindowText(strPos);

	strPos.Format(_T("%d"), ptImage.y);
	m_nPosY = ptImage.y - 1;
	GetDlgItem(IDC_TEMPLATE_MOUSEPOSY)->SetWindowText(strPos);

	pAdjustImg->OnMouseMove(nFlags, point);

	CDialogEx::OnMouseMove(nFlags, point);
}

BOOL CESMRecalibrationDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CESMRecalibrationImage* pAdjustImg;
	int nCount;
	nCount = ADJUST_IMAGE;

	pAdjustImg = &m_StAdjustImage;

	CRect rtWindow;
	GetWindowRect(rtWindow);

	pt.x -= rtWindow.left;
	pt.y -= rtWindow.top;

	CPoint ptImage;
	ptImage = GetPointFromMouse(pt);

	if( zDelta > 0)
	{
		if(  pAdjustImg->GetMultiple() <= 0.2)
			return TRUE;
		pAdjustImg->ImageZoom(ptImage, TRUE);
	}
	else
		pAdjustImg->ImageZoom(ptImage, FALSE);

	CString strTp;
	strTp.Format(_T("%.03lf"), pAdjustImg->GetMultiple());
	GetDlgItem(IDC_TEMPLATE_IMAGESIZE)->SetWindowText(strTp);

	return CDialogEx::OnMouseWheel(nFlags, zDelta, pt);
}

CPoint CESMRecalibrationDlg::GetPointFromMouse(CPoint pt)
{
	CESMRecalibrationImage* pAdjustImg;
	pAdjustImg = &m_StAdjustImage;

	CRect rect;
	pAdjustImg->GetWindowRect(&rect);
	ScreenToClient(&rect);

	CPoint ptRet;
	pt.x = pt.x - rect.left;
	pt.y = pt.y - rect.top;
	
	CPoint ptImage = pAdjustImg->GetImagePos();
	ptRet.x = (int)(ptImage.x * pAdjustImg->GetMultiple() + pt.x * pAdjustImg->GetMultiple());
	ptRet.y = (int)(ptImage.y * pAdjustImg->GetMultiple() + pt.y * pAdjustImg->GetMultiple());

	//ESMLog(5, _T("Moust Point x:%d y:%d"), ptRet.x, ptRet.y);
	return ptRet;
}

void CESMRecalibrationDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	CESMRecalibrationImage* pAdjustImg;
	pAdjustImg= &m_StAdjustImage;

	int nCount;
	pAdjustImg->OnLButtonDown(nFlags, point);
	pAdjustImg->SetFocus();

	CDialogEx::OnLButtonDown(nFlags, point);
}

void CESMRecalibrationDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	CESMRecalibrationImage* pAdjustImg;
	pAdjustImg = &m_StAdjustImage;

	pAdjustImg->OnLButtonUp(nFlags, point);

	CDialogEx::OnLButtonUp(nFlags, point);
}

void CESMRecalibrationDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	
	CDialogEx::OnLButtonDblClk(nFlags, point);
}

void CESMRecalibrationDlg::OnBnClickedBtnAdjustPointReset()
{
	CESMFileOperation fo;
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	fo.Delete(strFileName);
	CString strData = _T("");

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);
		m_AdjustDscList.SetItemText(i, 2, strData);
		m_AdjustDscList.SetItemText(i, 3, strData);
		m_AdjustDscList.SetItemText(i, 4, strData);
		m_AdjustDscList.SetItemText(i, 5, strData);
	}
}


void CESMRecalibrationDlg::OnBnClickedBtn2ndSavePoint()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));
	CString szFilter = _T("Point List (*.ptl)|*.ptl|");

#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  	
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = _T("Point Save");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();	
		if( strFileName.Right(4) != _T(".ptl"))
			strFileName = strFileName + _T(".ptl");
		SavePointData(strFileName);

		//Save Image Data
		CString strAdjustFolder;
		strAdjustFolder.Format(_T("%s\\img"),ESMGetPath(ESM_PATH_SETUP));
		CreateDirectory(strAdjustFolder, NULL);
	}
}

void CESMRecalibrationDlg::OnBnClickedBtn2ndLoadPoint()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));

	CString szFilter = _T("Point List (*.ptl)|*.ptl|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = _T("Point Load");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();
		LoadPointData(strFileName);	
	}
}

void ProcessWindowMessage()
{
	MSG msg;
	while(::PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
	{
		::SendMessage(msg.hwnd, msg.message, msg.wParam, msg.lParam);
	}
}

void CESMRecalibrationDlg::OnBnClickedBtnTracking()
{
	m_pObjectTracking = new CESMObjectTracking(CESMObjectTracking::ETC_SIZE);

	CListCtrl* pList = &m_AdjustDscList;
	CESMRecalibrationImage* pAdjustImg = &m_StAdjustImage;
	int nCount, nMaxCount;	
	
	m_nStartSelectedItem = m_nSelectedNum;
	if(pList->GetSelectedCount() == 1)
		m_nEndSelectedItem = pList->GetItemCount();
	else
		m_nEndSelectedItem = m_nStartSelectedItem + pList->GetSelectedCount();

	for(int i = m_nStartSelectedItem; i < /*pList->GetItemCount()*/m_nEndSelectedItem; i++)
	{			
		if( i < 0 || m_nStartSelectedItem  < 0)
			return;	

		while(!m_pImageLoadFlag[i])
		{
			Sleep(100);
		}			

		UpdateData();
		ProcessWindowMessage();

		//m_nSelectedNum = i;
		
		CString strSelect = pList->GetItemText(i, 1);
		DscAdjustInfo* pAdjustInfo = NULL;
		pAdjustInfo = m_pRecalibrationMgr->GetDscAt(i);	

		if(pAdjustInfo->pBmpBits != NULL || pAdjustImg->m_memDC == NULL)
		{			
			if( i < 0)
				continue;
			
			CString strSelect = pList->GetItemText(i, 1);
			CString strLine3;
			GetDlgItem(IDC_EDIT_TEMPLATE_CENTER)->GetWindowText(strLine3);

			pAdjustInfo = m_pRecalibrationMgr->GetDscAt(i);
			Mat src = m_pRecalibrationMgr->ApplyAdjustImageUsingCPU(i, m_MarginX, m_MarginY);

			int size = src.total() * src.elemSize();
			pAdjustImg->SetImageBuffer(src.data, src.cols, src.rows, 3, NULL);
			
			//
			m_pObjectTracking->_samplingScale = pAdjustImg->GetMultiple();
			m_pObjectTracking->_outputSize.width = pAdjustImg->m_nImageWidth;
			m_pObjectTracking->_outputSize.height = pAdjustImg->m_nImageHeight;

			Mat image;			
			m_pObjectTracking->Filtering(src, image);
						
			CPoint x_2;
			CPoint x_1;

			if(m_bInitializedTracking != TRUE)	// -1
			{
				x_2 = CPoint((m_pRect.width/m_pObjectTracking->_samplingScale + m_pRect.x/m_pObjectTracking->_samplingScale), 
							 (m_pRect.height/m_pObjectTracking->_samplingScale + m_pRect.y/m_pObjectTracking->_samplingScale));
				x_1 = CPoint(m_pRect.x/m_pObjectTracking->_samplingScale, 
							 m_pRect.y/m_pObjectTracking->_samplingScale);			

				m_pRect= Rect2d(Point2f(x_1.x, x_1.y), Point2f(x_2.x, x_2.y));

				if(m_nTrackingOption)
					m_pObjectTracking->InitTracking(image, m_pRect);

				m_bInitializedTracking = TRUE;
			}
			else
			{	
				if(m_nTrackingOption)
					m_pObjectTracking->UpdateTracking(image, m_pRect);	
								
				x_2 = CPoint(m_pRect.width*m_pObjectTracking->_samplingScale + m_pRect.x*m_pObjectTracking->_samplingScale, 
							 m_pRect.height*m_pObjectTracking->_samplingScale + m_pRect.y*m_pObjectTracking->_samplingScale);
				x_1 = CPoint(m_pRect.x*m_pObjectTracking->_samplingScale, 
							 m_pRect.y*m_pObjectTracking->_samplingScale);			

				CRect rtOutLine = CRect(x_1, x_2);
				m_StAdjustImage.DrawRect(rtOutLine, RGB(255,0,0));
				m_StAdjustImage.DrawPoint(x_1.x + (x_2.x-x_1.x)/2, x_1.y + (x_2.y-x_1.y)/2, RGB(0,255,0));	

				Mat focusImage;
				m_pObjectTracking->RoiImageOfOriginalSize(src, focusImage, m_pRect);
				
				blur(focusImage, focusImage, cv::Size(21, 21));

				m_pObjectTracking->imageSetForStabilization->inputImageForGeneratingTransformationMatrix(focusImage);						
				m_pObjectTracking->testImage.push_back(src.clone());

				Rect2d tempBox = m_pRect;

				m_pObjectTracking->RoiImageOfStabilizationSize(src, src, tempBox);							
				rectangle(image, 
					Rect2d( 
					tempBox.x/m_pObjectTracking->_samplingScale,
					tempBox.y/m_pObjectTracking->_samplingScale,
					tempBox.width/m_pObjectTracking->_samplingScale,
					tempBox.height/m_pObjectTracking->_samplingScale), 
					Scalar(255, 0, 0), 2, 1);	

				CRect rtOutLine2 = CRect(
					CPoint(tempBox.x, tempBox.y),
					CPoint(tempBox.width + tempBox.x, tempBox.height + tempBox.y));
				m_StAdjustImage.DrawRect(rtOutLine2, RGB(0,0,255));				
			}

			pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
			pAdjustImg->DrawLine(pAdjustInfo->nHeight/2, RGB(0,0,0));
			pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
			pAdjustImg->Redraw();

			if(m_bStabilizationCloseFlag == TRUE)
			{
				m_pMovePoint.clear();
				m_pMovePoint = m_pObjectTracking->_movePoint;
				delete m_pObjectTracking;

				return;
			}
		}

		else
		{

		}

		SetSquarePointListItem(i);
		Invalidate();
	}
	string outputPath = "C:\\test.mp4";
	m_pObjectTracking->CreateStabilizatedMovie(outputPath, 30.);

	getAdjustMoveCoordination();
		
	for(int i = 0; i < m_pObjectTracking->_movePoint.size(); i++)
	{	
		if( i < 0 || m_nStartSelectedItem  < 0)
			break;	

		DscAdjustInfo* pAdjustInfo = m_pRecalibrationMgr->GetDscAt(i);	
		{
			CString strSelect;
			strSelect.Format(_T("%.3lf"),m_pObjectTracking->_movePoint[i].x);
			m_AdjustDscList.SetItemText(i+m_nStartSelectedItem, 9, strSelect);	

			strSelect.Format(_T("%.3lf"),m_pObjectTracking->_movePoint[i].y);
			m_AdjustDscList.SetItemText(i+m_nStartSelectedItem, 10, strSelect);
		}
	}
	m_bInitializedTracking = FALSE;	

	m_pMovePoint.clear();
	m_pMovePoint = m_pObjectTracking->_movePoint;
	delete m_pObjectTracking;
}

void CESMRecalibrationDlg::CalcAdjustData()
{
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();
	DscAdjustInfo* pAdjustInfo;

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strDSC = m_AdjustDscList.GetItemText(i,1);
		m_pAdjInfo.push_back(pMovieMgr->GetAdjustData(strDSC));		

		CString strSelect;
		strSelect.Format(_T("%.3lf"),m_pAdjInfo[i].AdjMove.x);
		m_AdjustDscList.SetItemText(i, 6, strSelect);	

		strSelect.Format(_T("%.3lf"),m_pAdjInfo[i].AdjMove.y);
		m_AdjustDscList.SetItemText(i, 7, strSelect);	

		strSelect.Format(_T("%.3lf"),m_pAdjInfo[i].AdjAngle);
		m_AdjustDscList.SetItemText(i, 8, strSelect);
	}
}

void CESMRecalibrationDlg::ModifyAdjustData()
{
	if (m_pMovePoint.size() == 0)
		return;

	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();
	DscAdjustInfo* pAdjustInfo;

	CESMRecalibrationImage* pAdjustImg = &m_StAdjustImage;

	vector<stAdjustInfo> test;

	int marginX = 0;
	int marginY = 0;

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		stAdjustInfo adjInfo = m_pAdjInfo.at(i);
		adjInfo.AdjAngle = m_pAdjInfo.at(i).AdjAngle;
		adjInfo.AdjMove.x = m_pAdjInfo.at(i).AdjMove.x+_ttoi(m_AdjustDscList.GetItemText(i,9));//m_pMovePoint[i].x/2;//m_pObjectTracking->_movePoint[i].x/2;
		adjInfo.AdjMove.y = m_pAdjInfo.at(i).AdjMove.y+_ttoi(m_AdjustDscList.GetItemText(i,10));//m_pMovePoint[i].y/2;//m_pObjectTracking->_movePoint[i].y/2;	

		test.push_back(adjInfo);

		CESMImgMgr* pImgMgr = new CESMImgMgr();
		pImgMgr->SetMargin(marginX,marginY,adjInfo);		
	}

	ESMEvent* pMsg = new ESMEvent();  
	pMsg->message = WM_ESM_MOVIE_SAVE_ADJUST;
	pMsg->pParam = (LPARAM)& test;
	pMsg->nParam1 = marginX;
	pMsg->nParam2 = marginY;

	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	//MessageBox(_T("ReCalibration\nAdjust Save Complete!"),_T("ReCalibration"),NULL);
}


void CESMRecalibrationDlg::OnBnClickedBtnModifyAdj()
{
	ModifyAdjustData();
}


void CESMRecalibrationDlg::getAdjustMoveCoordination()
{
	Mat image;			

	vector<Mat> testMotion = m_pObjectTracking->twoPassStabilizer->getMotions();
	float nX = 0, nY = 0;

	if(!m_nTrackingOption)	
	{
		nX = testMotion[0].at<float>(0, 2);
		nY = testMotion[0].at<float>(1, 2);
		m_pObjectTracking->_movePoint.push_back(cv::Point2f(nX, nY));	
	}
	else
		m_pObjectTracking->_movePoint.push_back(Point2f());	

	int cnt = 1;	
	for(int i = 1; i < m_nEndSelectedItem - m_nStartSelectedItem; i++)
	{
		CESMRecalibrationImage* pAdjustImg = &m_StAdjustImage;
		DscAdjustInfo* pAdjustInfo = m_pRecalibrationMgr->GetDscAt(i+m_nStartSelectedItem);	
		if(pAdjustInfo->pBmpBits != NULL || pAdjustImg->m_memDC == NULL)
		{
			if(i == 83)
				int test = 0;

			image = m_pObjectTracking->testImage[i-cnt].clone();

			float nX = 0, nY = 0;
		
			if(m_nTrackingOption)
			{
				nX = testMotion[i-cnt].at<float>(0, 2) - m_pObjectTracking->_subPoint[i-cnt].x*m_pObjectTracking->_samplingScale;
				nY = testMotion[i-cnt].at<float>(1, 2) - m_pObjectTracking->_subPoint[i-cnt].y*m_pObjectTracking->_samplingScale;
			}
			else
			{
				nX = testMotion[i-cnt].at<float>(0, 2);
				nY = testMotion[i-cnt].at<float>(1, 2);
			}

			m_pObjectTracking->_movePoint.push_back(cv::Point2f(nX, nY));
		}
		else
		{
			m_pObjectTracking->_movePoint.push_back(cv::Point());
			cnt++;
		}
	}	
	
	m_pObjectTracking->_subPoint.clear();
}

void CESMRecalibrationDlg::OnBnClickedRadioUsingTracking(UINT msg)
{
	UpdateData(TRUE);
	if(m_nTrackingOption == 0)
	{	
		
	}
	else if(m_nTrackingOption == 1)
	{
		
	}
}

HBRUSH CESMRecalibrationDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	hbr = (HBRUSH)m_background;	

	switch(nCtlColor)
	{
	case CTLCOLOR_STATIC:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			//pDC->SetBkColor(RGB(56, 56, 56));  // 글자 배경색 변경
			pDC->SetBkMode(TRANSPARENT);
			//return (HBRUSH)m_brush;
		}
		break;
	case CTLCOLOR_EDIT:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			pDC->SetBkColor(RGB(0, 0 ,0));
			//hbr = (HBRUSH)(hbr.GetSafeHandle());  
		}
		break;
	}
	return hbr;
}

BOOL CESMRecalibrationDlg::PreTranslateMessage( MSG* pMsg )
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}
