// LensPowerDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "LensPowerDlg.h"
#include "afxdialogex.h"


// CLensPowerDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLensPowerDlg, CDialogEx)

CLensPowerDlg::CLensPowerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLensPowerDlg::IDD, pParent)
{

}

CLensPowerDlg::~CLensPowerDlg()
{
}

void CLensPowerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CLensPowerDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_ON, &CLensPowerDlg::OnBnClickedBtnOn)
	ON_BN_CLICKED(IDC_BTN_OFF, &CLensPowerDlg::OnBnClickedBtnOff)
END_MESSAGE_MAP()


// CLensPowerDlg 메시지 처리기입니다.


void CLensPowerDlg::OnBnClickedBtnOn()
{
	EndDialog(1);
}


void CLensPowerDlg::OnBnClickedBtnOff()
{
	EndDialog(0);
}
