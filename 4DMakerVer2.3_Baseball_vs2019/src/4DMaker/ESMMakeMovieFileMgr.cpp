// ESMMakeMovieFileMgr.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMMakeMovieFileMgr.h"
#include "MainFrm.h"

// CESMMakeMovieFileMgr

IMPLEMENT_DYNCREATE(CESMMakeMovieFileMgr, CWinThread)

CESMMakeMovieFileMgr::CESMMakeMovieFileMgr()
{
	m_bThreadStop = FALSE;
	m_bWait = FALSE;
}

CESMMakeMovieFileMgr::~CESMMakeMovieFileMgr()
{
}

void CESMMakeMovieFileMgr::AddMsg(ESMEvent* pMsg)
{
	if(!m_bThreadStop)
	{
		m_arMsg.Add((CObject*)pMsg);
	}
}

BOOL CESMMakeMovieFileMgr::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CESMMakeMovieFileMgr::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	return CWinThread::ExitInstance();
}

int CESMMakeMovieFileMgr::Run()
{
	ESMEvent* pMsg	= NULL;
	while(!m_bThreadStop)
	{
		Sleep(1);
		int nAll = m_arMsg.GetSize();

		while(nAll--)
		{
			Sleep(1);
			if(m_bWait)
			{
				nAll++;
				continue;
			}

			pMsg = (ESMEvent*)m_arMsg.GetAt(0);

			if(!pMsg)
			{
				m_arMsg.RemoveAt(0);
				continue;
			}

			m_arMsg.RemoveAt(0);

			ESMEvent* pMovieMsg	= new ESMEvent;
			pMovieMsg->message = WM_MAKEMOVIE_GPU_FILE;

			/*CString* strFile = (CString*)pMsg->pParam;*/
			/*CString* pStrData = NULL;
			pStrData = new CString;
			pStrData->Format(_T("%s"), *strFile);*/

			pMovieMsg->pParam = pMsg->pParam;
			CMainFrame* pMain = (CMainFrame*)pMsg->pDest;
			HWND pMain2 = AfxGetMainWnd()->GetSafeHwnd();
			
			::SendMessage(pMain->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMovieMsg);	
			//SendMessage

			m_bWait = TRUE;
		}
	}

	return 0;
}

BEGIN_MESSAGE_MAP(CESMMakeMovieFileMgr, CWinThread)
END_MESSAGE_MAP()


// CESMMakeMovieFileMgr 메시지 처리기입니다.
