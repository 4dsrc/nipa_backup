// ESMPWEditorDlgGH5WBAD.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "afxdialogex.h"
#include "ESMPWEditorDlgGH5WBAD.h"
#include "ESMIndex.h"


// CESMPWEditorDlgGH5WBAD 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMPWEditorDlgGH5WBAD, CDialogEx)

CESMPWEditorDlgGH5WBAD::CESMPWEditorDlgGH5WBAD(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMPWEditorDlgGH5WBAD::IDD, pParent)
{
	m_bMouseLeftClickFlag = false;
	m_nWidth = 100;
	m_nHeight = 100;
}

CESMPWEditorDlgGH5WBAD::~CESMPWEditorDlgGH5WBAD()
{
}

void CESMPWEditorDlgGH5WBAD::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_PCTRL, m_ctrlPic);
}


BEGIN_MESSAGE_MAP(CESMPWEditorDlgGH5WBAD, CDialogEx)	
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()


// CESMPWEditorDlgGH5WBAD 메시지 처리기입니다.

BOOL CESMPWEditorDlgGH5WBAD::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	MoveWindow(CRect(0,0,m_nWidth,m_nHeight));

	HBITMAP hbit;
	hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_WB_AD));

	m_ctrlPic.SetBitmap(hbit);

	m_ctrlPic.MoveWindow(CRect(0,0,m_nWidth,m_nHeight));
	m_ctrlPic.ShowWindow(SW_MAXIMIZE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CESMPWEditorDlgGH5WBAD::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_bMouseLeftClickFlag = true;

	CDialogEx::OnLButtonDown(nFlags, point);
}


void CESMPWEditorDlgGH5WBAD::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (m_bMouseLeftClickFlag != true)
		return;

	m_bMouseLeftClickFlag = false;

	int nWidthValue = m_nWidth/19;
	int nHeightValue = m_nHeight/19;
	int nX = point.x;
	int nY = point.y;

	// i -> 0: AB,  1: MG
	for (int i = 0; i < 2 ; i++)
	{
		int nPos = nX;
		int nValue = nWidthValue;
		if (i == 1)
		{
			nValue = nHeightValue;
			nPos = nY;
		}

		// j -> -9 ~ 9 까지 선택 19
		for (int j = 0; j < 20 ; j++)
		{
			if (j == 0)
			{
				if (nPos <= nValue)
				{
					::SendMessage(GetParent()->GetSafeHwnd(), WM_ESM_WB_ADJUST, i, 0);
				}
				continue;
			}

			if (nPos > nValue*j && nPos <= nValue*(j+1))
			{
				::SendMessage(GetParent()->GetSafeHwnd(), WM_ESM_WB_ADJUST, i, j);
			}
		}
	}
	


	CDialogEx::OnLButtonUp(nFlags, point);
}
