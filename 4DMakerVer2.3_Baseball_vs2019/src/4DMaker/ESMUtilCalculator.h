#pragma once
class CESMUtilCalculator
{
public:
	CESMUtilCalculator(void);
	~CESMUtilCalculator(void);

public:
	double ArcLength(double radius, double degree);
	
	double DiagonalLength(double width, double height);
	double RadianToDegree(double radian);
	double ActualDiagonalLength(double actualLength, double angleOfView);
	double ActualWidth(double sensorWidth, double sensorHeight, double actualDiagonalLength);
	double ActualHeight(double sensorWidth, double sensorHeight, double actualDiagonalLength);
	double ActualHeight169(double actualHeight);

};

