#pragma once
#include <vector>
#include "cv.h"
#include "highgui.h"
#include "ESMFunc.h"
#include "ESMUtil.h"
#include "FFmpegManager.h"

class ESMKzoneMgr
{
public:
	ESMKzoneMgr(void);
	~ESMKzoneMgr(void);

	//����
	vector<DscAdjustInfo*> m_ArrDscInfo;

	void AddDscInfo(CString strDscId, CString strDscIp,BOOL bReverse = FALSE);
	int GetDscCount();
	void DscClear();
	void GetResolution(int &nWidth, int &nHeight);
	DscAdjustInfo* GetDscAt(int nIndex);
	BOOL CalcAdjustData(DscAdjustInfo* pAdjustInfo, int nTargetLenth, int nZoom);
	BOOL GetSearchDetectPoint(DscAdjustInfo* pAdjustInfo, int nThreshold, CSize minDetectSize, CSize maxDetectSize, int nPointGapMin, COLORREF clrDetectColor);
	//	BOOL GetSearchPoint(DscAdjustInfo* pAdjustInfo, int nThreshold, int nChannel);
	BOOL SearchDetectPoint(IplImage* pNewGray, CSize MinSize, CSize MaxSize, vector<CRect>* vecDetectAreas, COLORREF clrDetectColor);
	BOOL SearchPoint(IplImage*	pNewGray, vector<CPoint>* arrBeginPoint, vector<CPoint>* arrPointRange, vector<CPoint>* pArrPoint);
	void GetDscInfo(vector<DscAdjustInfo*>** pArrDscInfo);
	double GetDistanceData(CString strDSCId);
	int GetZoomData(CString strDSCId);
};

