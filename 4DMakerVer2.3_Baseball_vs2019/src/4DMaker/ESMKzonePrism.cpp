// ESMAdjustMgrDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#ifdef _4DMODEL
#include "4DModeler.h"
#else
#include "4DMaker.h"
#endif
#include "ESMKzonePrism.h"
#include "ESMKzoneImage.h"
#include "ESMKzoneMgr.h"
#include "afxdialogex.h"
#include "FFmpegManager.h"
#include "ESMIni.h"
#include "ESMCtrl.h"

#include "ESMFileOperation.h"
#include "ESMFunc.h"
#include "ESMMovieMgr.h"
#include "ESMImgMgr.h"
#include<highgui.h>
#include<cv.h>
//#define PRISMTEMPFILE _T("Prism_temp.ptl")

#include <math.h>

#define PI 3.1415926535897932384
#define TIMER_CHANGE_NEXT_ITEM	0x8001
#define TIME_CHANGE_NEXT_ITEM	700

// ESMAdjustMgrDlg 대화 상자입니다.
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


enum{
	FIRST_ADJUST_VIEW = 0,
};

enum{
	ADJUST_IMAGE = 0,
	MAX_IDX,
};

enum{
	ADJUST_LEFT_UP = 0,
	ADJUST_LEFT_DOWN,
	ADJUST_RIGHT_UP,
	ADJUST_RIGHT_DOWN,
};
enum{
	T_LB_D = 0,
	T_RB_D,
	T_PEAK_D,
	B_PEAK_D,
};
IMPLEMENT_DYNAMIC(ESMKzonePrism, CDialogEx)


ESMKzonePrism::ESMKzonePrism(CWnd* pParent /*=NULL*/)
	: CDialogEx(ESMKzonePrism::IDD, pParent)
{
	m_nSelectPos = 0;
	m_nSelectedNum = -1;
	m_nHeight = 0;
	m_nWidth = 0;
	m_frameIndex=0;
	m_nPosX = 0, m_nPosY = 0;
	m_nThreshold = 0;
	m_MarginX = 0;
	m_MarginY = 0;
	load_prism = 0;
	m_hTheadHandle = NULL;
	m_bAutoFind = FALSE;
	m_bChangeFlag = FALSE;

	//wgkim@esmlab.com 17-06-21
	m_bThreadCloseFlag = FALSE;

	m_nTickness = 2;
		
}

ESMKzonePrism::~ESMKzonePrism()
{
	m_bThreadCloseFlag = TRUE;

	WaitForSingleObject(m_hTheadHandle, INFINITE);
	CloseHandle(m_hTheadHandle);
}

void ESMKzonePrism::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX,	IDC_PRISMDSCLIST,	m_AdjustDscList);
	DDX_Control(pDX,	 IDC_PRISM_COMBO,	m_ctrlCheckPoint);
	DDX_Control(pDX,	IDC_ST_PRISMIMAGE,  m_StAdjustImage);
	DDX_Control(pDX, IDC_EDIT_PRISM_CENTER, m_ctrlCenterLine);
}


BEGIN_MESSAGE_MAP(ESMKzonePrism, CDialogEx)
ON_NOTIFY(NM_CUSTOMDRAW, IDC_PRISMDSCLIST, &ESMKzonePrism::OnCustomdrawTcpList)
ON_NOTIFY(LVN_ITEMCHANGED, IDC_PRISMDSCLIST, &ESMKzonePrism::OnLvnItemchangedAdjust)
ON_WM_MOUSEMOVE()
ON_WM_LBUTTONDOWN()
ON_WM_RBUTTONDOWN()
ON_WM_LBUTTONUP()
ON_WM_MOUSEWHEEL()
ON_WM_LBUTTONDBLCLK()
ON_BN_CLICKED(IDC_BTN_RESET, &ESMKzonePrism::OnBnClickedBtnAdjustPointReset)
ON_BN_CLICKED(IDC_BTN_SAVE, &ESMKzonePrism::OnBnClickedBtn2ndSavePoint)
ON_BN_CLICKED(IDC_BTN_LOAD, &ESMKzonePrism::OnBnClickedBtn2ndLoadPoint)
ON_BN_CLICKED(IDC_BTN_AXISMAKE, &ESMKzonePrism::OnBnAxisMaking)
ON_BN_CLICKED(IDC_BTN_INTERPOLATION, &ESMKzonePrism::OnBnAxisInterpolation)
ON_BN_CLICKED(IDC_BTN_STRUCT, &ESMKzonePrism::OnBnDrawPrismBone)
ON_BN_CLICKED(IDC_BTN_SAVEPRISM, &ESMKzonePrism::OnBnPrismImageMaking)
ON_BN_CLICKED(IDC_KZONE_SCALE_UP, &ESMKzonePrism::ScalingUp)
ON_BN_CLICKED(IDC_KZONE_SCALE_DOWN, &ESMKzonePrism::ScalingDown)
ON_BN_CLICKED(IDC_KZONE_MOVE_UP, &ESMKzonePrism::MovingUp)
ON_BN_CLICKED(IDC_KZONE_MOVE_DOWN, &ESMKzonePrism::MovingDown)
ON_BN_CLICKED(IDC_KZONE_AXIS_CLR, &ESMKzonePrism::AxisClear)
ON_BN_CLICKED(IDC_KZONE_SETAXIS, &ESMKzonePrism::Axis_Update)

END_MESSAGE_MAP()
void ESMKzonePrism::AxisClear()
{
	m_prismaxis.clear();
	m_Scale = 0;
	m_Move = 0;
	OnBnAxisMaking();
}
void ESMKzonePrism::ScalingUp()
{
	m_Scale -= 2;
	Showimage();
	Invalidate();
}
void ESMKzonePrism::ScalingDown()
{
	m_Scale += 2;
	Showimage();
	Invalidate();
}
void ESMKzonePrism::MovingUp()
{
	m_Move -= 2;
	Showimage();
	Invalidate();
}
void ESMKzonePrism::MovingDown()
{
	m_Move += 2;
	Showimage();
	Invalidate();
}
void ESMKzonePrism::OnCustomdrawTcpList(NMHDR* pNMHDR, LRESULT* pResult)
{
	//LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
	NMLVCUSTOMDRAW* lplvcd = (NMLVCUSTOMDRAW*)pNMHDR;
	if(lplvcd->nmcd.dwDrawStage == CDDS_PREPAINT)
	{
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if(lplvcd->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
	{
		*pResult = CDRF_NOTIFYSUBITEMDRAW;
	}
	else if(lplvcd->nmcd.dwDrawStage == (CDDS_ITEMPREPAINT | CDDS_SUBITEM))
	{
		if( lplvcd->nmcd.dwItemSpec == m_nSelectedNum)
		{
			if(m_nSelectPos * 2<=  lplvcd->iSubItem && m_nSelectPos * 2 + 1 >=  lplvcd->iSubItem)
			{
				lplvcd->clrTextBk = RGB(200, 200, 255);
				lplvcd->clrText = RGB(255, 0, 0);
			}
			else
			{
				lplvcd->clrTextBk = RGB(255, 255, 255);
				lplvcd->clrText = RGB(0, 0, 0);
			}
		}
		*pResult = CDRF_DODEFAULT;
	}
}
void ESMKzonePrism::AddDscInfo(CString strDscId, CString strDscIp)
{
	m_pAdjustMgr.AddDscInfo(strDscId, strDscIp);
}
void ESMKzonePrism::GetDscInfo(vector<DscAdjustInfo*>** pArrDscInfo)
{
	m_pAdjustMgr.GetDscInfo(pArrDscInfo);
}
void ESMKzonePrism::SetMovieState(int nMovieState )
{
	m_nMovieState = nMovieState;
}
BOOL ESMKzonePrism::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_nMovieState = ESM_ADJ_FILM_STATE_MOVIE;

	//m_AdjustDscList.ModifyStyle(0, LVS_SHOWSELALWAYS);
	m_AdjustDscList.InsertColumn(0, _T("ID"), 0, 30);
	m_AdjustDscList.InsertColumn(1, _T("DscId"), 0, 44);
	m_AdjustDscList.InsertColumn(2, _T("TLB X"), 0, 45);
	m_AdjustDscList.InsertColumn(3, _T("TLB Y"), 0, 45); 
	m_AdjustDscList.InsertColumn(4, _T("TRB X"), 0, 45);
	m_AdjustDscList.InsertColumn(5, _T("TRB Y"), 0, 45);
	m_AdjustDscList.InsertColumn(6, _T("TPeak X"), 0, 45);
	m_AdjustDscList.InsertColumn(7, _T("TPeak Y"), 0, 45);
	m_AdjustDscList.InsertColumn(10, _T("BPeak X"), 0, 65);
	m_AdjustDscList.InsertColumn(11, _T("BPeak Y"), 0, 65);

	m_AdjustDscList.SetExtendedStyle(LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT);

	m_ctrlCheckPoint.AddString(_T("None"));
	m_ctrlCheckPoint.AddString(_T("T LB"));
	m_ctrlCheckPoint.AddString(_T("T RB"));
	m_ctrlCheckPoint.AddString(_T("T Peak"));
	m_ctrlCheckPoint.AddString(_T("B Peak"));
	m_ctrlCheckPoint.SetCurSel(0);

	LoadData();
	Showimage();
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), KZONEPRISM);
	LoadPointData(strFileName);
	bone_color = Scalar(0,0,255);
	m_Move = 0;
	m_Scale = 0;
	m_Number = 0;
	UpdateData(FALSE);
	return TRUE;
}
void ESMKzonePrism::Showimage()
{
	CListCtrl* pList;
	ESMKzoneImage* pAdjustImg;
	int nCount, nMaxCount;
	pList = &m_AdjustDscList;

	pAdjustImg = &m_StAdjustImage;

	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );
	if( nSelectedItem < 0)
		return;

	m_nSelectedNum = nSelectedItem;
	CString strSelect = pList->GetItemText(nSelectedItem, 1);
	DscAdjustInfo* pAdjustInfo = NULL;
	pAdjustInfo = m_pAdjustMgr.GetDscAt(nSelectedItem);
	if(pAdjustInfo->pBmpBits != NULL || pAdjustImg->m_memDC == NULL)
	{
		DrawImage();
		DrawCenterPointAll();
	}
	else
	{
		DeleteImage();
	}
}
void ESMKzonePrism::DrawImage()
{
	CListCtrl* pList;
	ESMKzoneImage* pAdjustImg;

	DscAdjustInfo* pAdjustInfo = NULL;
	pList = &m_AdjustDscList;
	pAdjustImg = &m_StAdjustImage;

	UpdateData();
	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );
	if( nSelectedItem < 0)
		return;
	bool s;
	m_nSelectedNum = nSelectedItem;
	CString strSelect = pList->GetItemText(nSelectedItem, 1);
	CString strLine3;
	GetDlgItem(IDC_EDIT_PRISM_CENTER)->GetWindowText(strLine3);
	
	if(ESMGetCudaSupport()) //GPU 지원
	{
		pAdjustInfo = m_pAdjustMgr.GetDscAt(nSelectedItem);
		if( pAdjustInfo != NULL)
		{
			if(pAdjustInfo->bAdjust )
			{
				pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3, NULL , &load_prism);
			}
			else if(pAdjustInfo->nHeight == 0 || pAdjustInfo->nWidht == 0)
				return;

			Mat src(pAdjustInfo->nHeight, pAdjustInfo->nWidht, CV_8UC3);
			memcpy(src.data, pAdjustInfo->pBmpBits, pAdjustInfo->nHeight * pAdjustInfo->nWidht * 3);
			cuda::GpuMat* pImageMat = new cuda::GpuMat;
			pImageMat->upload(src);

			if( pImageMat == NULL)
			{
				TRACE(_T("Image Error \n"));
				return ;
			}


			int nRotateX = 0, nRotateY = 0, nSize = 0;
			double dSize = 0.0;
			double dRatio = 1;
			stAdjustInfo AdjustData = m_adjinfo.at(m_nSelectedNum);
			if(!pAdjustInfo->bRotate)
			{
				// Rotate 구현					
				nRotateX = Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
				nRotateY = Round(AdjustData.AdjptRotate.y  * dRatio );
				GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
			}

			if(!pAdjustInfo->bMove)
			{
				//-- MOVE EVENT
				int nMoveX = 0, nMoveY = 0;
				nMoveX = Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
				nMoveY = Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
				GpuMoveImage(pImageMat, nMoveX,  nMoveY);
			}

			if(!pAdjustInfo->bImgCut)
			{
				//-- IMAGE CUT EVENT
				int nMarginX = m_MarginX * dRatio;
				int nMarginY = m_MarginY * dRatio;
				if ( nMarginX < 0 || nMarginX >= pAdjustInfo->nWidht/2 || nMarginY < 0 || nMarginY >= pAdjustInfo->nHeight/2  )
				{
					TRACE(_T("Image Adjust Error 2\n"));
					return ;
				}
				//GpuMakeMargin(pImageMat, nMarginX, nMarginY);
				double dbMarginScale = 1 / ( (double)( pAdjustInfo->nWidht - nMarginX * 2) /(double) pAdjustInfo->nWidht  );
				cuda::GpuMat* pMatResize = new cuda::GpuMat;
				cuda::resize(*pImageMat, *pMatResize, cv::Size(pAdjustInfo->nWidht*dbMarginScale ,pAdjustInfo->nHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
				int nLeft = (int)((pAdjustInfo->nWidht*dbMarginScale - pAdjustInfo->nWidht)/2);
				int nTop = (int)((pAdjustInfo->nHeight*dbMarginScale - pAdjustInfo->nHeight)/2);
				cuda::GpuMat pMatCut =  (*pMatResize)(cv::Rect(nLeft, nTop, pAdjustInfo->nWidht, pAdjustInfo->nHeight));
				pMatCut.copyTo(*pImageMat);
				delete pMatResize;
			}


			Mat srcImage(pImageMat->rows, pImageMat->cols, CV_8UC3);
			pImageMat->download(srcImage);
			Mat tmp_srcImage = srcImage.clone();
			if(load_prism == 1)
			{
				stPrismInfo prism_axis_info = m_prismaxis.at(nSelectedItem);

				if(prism_axis_info.t_peak.x != 0 || prism_axis_info.t_peak.y != 0)
				{
					line(srcImage,prism_axis_info.t_peak,prism_axis_info.t_LT,bone_color,1,8);			
					line(srcImage,prism_axis_info.t_LT,prism_axis_info.t_LB,bone_color,1,8);
					line(srcImage,prism_axis_info.t_LB,prism_axis_info.t_RB,bone_color,1,8);
					line(srcImage,prism_axis_info.t_RB,prism_axis_info.t_RT,bone_color,1,8);
					line(srcImage,prism_axis_info.t_RT,prism_axis_info.t_peak,bone_color,1,8);

					line(srcImage,prism_axis_info.t_peak,prism_axis_info.b_peak,bone_color,1,8);
					line(srcImage,prism_axis_info.t_LT,prism_axis_info.b_LT,bone_color,1,8);
					line(srcImage,prism_axis_info.t_LB,prism_axis_info.b_LB,bone_color,1,8);
					line(srcImage,prism_axis_info.t_RB,prism_axis_info.b_RB,bone_color,1,8);
					line(srcImage,prism_axis_info.t_RT,prism_axis_info.b_RT,bone_color,1,8);

					line(srcImage,prism_axis_info.b_peak,prism_axis_info.b_LT,bone_color,1,8);
					line(srcImage,prism_axis_info.b_LT,prism_axis_info.b_LB,bone_color,1,8);
					line(srcImage,prism_axis_info.b_LB,prism_axis_info.b_RB,bone_color,1,8);
					line(srcImage,prism_axis_info.b_RB,prism_axis_info.b_RT,bone_color,1,8);
					line(srcImage,prism_axis_info.b_RT,prism_axis_info.b_peak,bone_color,1,8);			
				}
			}
			else
				srcImage = tmp_srcImage;
			int size = srcImage.total() * srcImage.elemSize();
			memcpy(pAdjustInfo->pBmpBits, srcImage.data, size);

			s = pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3, NULL , &load_prism);	
			pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
			pAdjustImg->DrawLine(pAdjustInfo->nHeight/2, RGB(0,0,0));
			pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
			pAdjustImg->Redraw();
			pAdjustInfo->bAdjust = TRUE;
			pAdjustInfo->bRotate = TRUE;
			pAdjustInfo->bMove = TRUE;
			pAdjustInfo->bImgCut = TRUE;

			delete pImageMat;
		}
	}
	else //Cpu 사용
	{		
		pAdjustInfo = m_pAdjustMgr.GetDscAt(nSelectedItem);
		if( pAdjustInfo != NULL)
		{
			if(pAdjustInfo->bAdjust )
			{
				s = pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3, NULL , &load_prism);	
			}
			else if(pAdjustInfo->nHeight == 0 || pAdjustInfo->nWidht == 0)
				return;

			Mat src(pAdjustInfo->nHeight, pAdjustInfo->nWidht, CV_8UC3);
			memcpy(src.data, pAdjustInfo->pBmpBits, pAdjustInfo->nHeight * pAdjustInfo->nWidht * 3);

			int nRotateX = 0, nRotateY = 0, nSize = 0;
			double dSize = 0.0;
			double dRatio = 1;
			stAdjustInfo AdjustData = m_adjinfo.at(m_nSelectedNum);

			if(!pAdjustInfo->bRotate)
			{
				// Rotate 구현					
				nRotateX = Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
				nRotateY = Round(AdjustData.AdjptRotate.y  * dRatio );

				//IplImage *Iimage = new IplImage(src);
				Mat Iimage(src);
				CpuRotateImage(Iimage, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
				//delete Iimage;
			}


			if(!pAdjustInfo->bMove)
			{
				//-- MOVE EVENT
				int nMoveX = 0, nMoveY = 0;
				nMoveX = Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
				nMoveY = Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
				CpuMoveImage(&src, nMoveX,  nMoveY);
			}

			if(!pAdjustInfo->bImgCut)
			{
				//-- IMAGE CUT EVENT
				int nMarginX = m_MarginX * dRatio;
				int nMarginY = m_MarginY * dRatio;
				if ( nMarginX < 0 || nMarginX >= pAdjustInfo->nWidht/2 || nMarginY < 0 || nMarginY >= pAdjustInfo->nHeight/2  )
				{
					TRACE(_T("Image Adjust Error 2\n"));
					return ;
				}
				//CpuMakeMargin(&src, nMarginX, nMarginY);
				double dbMarginScale = 1 / ( (double)( pAdjustInfo->nWidht - nMarginX * 2) /(double) pAdjustInfo->nWidht  );
				Mat  pMatResize;

				resize(src, pMatResize, cv::Size(pAdjustInfo->nWidht*dbMarginScale ,pAdjustInfo->nHeight * dbMarginScale ), 0.0, 0.0 ,CV_INTER_NN);
				int nLeft = (int)((pAdjustInfo->nWidht*dbMarginScale - pAdjustInfo->nWidht)/2);
				int nTop = (int)((pAdjustInfo->nHeight*dbMarginScale - pAdjustInfo->nHeight)/2);
				Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, pAdjustInfo->nWidht, pAdjustInfo->nHeight));
				pMatCut.copyTo(src);
				pMatResize = NULL;
			}
			Mat tmp_src = src.clone();

			if(load_prism == 1)
			{
				stPrismInfo prism_axis_info = m_prismaxis.at(nSelectedItem);
				pAdjustImg->GetAxisInfo(prism_axis_info,m_Scale,m_Move);
				//if(prism_axis_info.t_peak.x != 0 || prism_axis_info.t_peak.y != 0)
				//{
				//	line(src,prism_axis_info.t_peak,prism_axis_info.t_LT,bone_color,1,8);
				//	line(src,prism_axis_info.t_LT,prism_axis_info.t_LB,bone_color,1,8);
				//	line(src,prism_axis_info.t_LB,prism_axis_info.t_RB,bone_color,1,8);
				//	line(src,prism_axis_info.t_RB,prism_axis_info.t_RT,bone_color,1,8);
				//	line(src,prism_axis_info.t_RT,prism_axis_info.t_peak,bone_color,1,8);

				//	line(src,prism_axis_info.t_peak,prism_axis_info.b_peak,bone_color,1,8);
				//	line(src,prism_axis_info.t_LT,prism_axis_info.b_LT,bone_color,1,8);
				//	line(src,prism_axis_info.t_LB,prism_axis_info.b_LB,bone_color,1,8);
				//	line(src,prism_axis_info.t_RB,prism_axis_info.b_RB,bone_color,1,8);
				//	line(src,prism_axis_info.t_RT,prism_axis_info.b_RT,bone_color,1,8);

				//	line(src,prism_axis_info.b_peak,prism_axis_info.b_LT,bone_color,1,8);
				//	line(src,prism_axis_info.b_LT,prism_axis_info.b_LB,bone_color,1,8);
				//	line(src,prism_axis_info.b_LB,prism_axis_info.b_RB,bone_color,1,8);
				//	line(src,prism_axis_info.b_RB,prism_axis_info.b_RT,bone_color,1,8);
				//	line(src,prism_axis_info.b_RT,prism_axis_info.b_peak,bone_color,1,8);			
				//}
			}
			else
				src = tmp_src;

			int size = src.total() * src.elemSize();
			memcpy(pAdjustInfo->pBmpBits, src.data, size);
			s = pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3, NULL , &load_prism);	

			pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
			pAdjustImg->DrawLine(pAdjustInfo->nHeight/2, RGB(0,0,0));
			pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
			pAdjustImg->Redraw();

			pAdjustInfo->bAdjust = TRUE;
			pAdjustInfo->bRotate = TRUE;
			pAdjustInfo->bMove = TRUE;
			pAdjustInfo->bImgCut = TRUE;

		}
	}
	m_nSelectPos = 0;
}
void ESMKzonePrism::DeleteImage()
{
	ESMKzoneImage* pAdjustImg;
	pAdjustImg = &m_StAdjustImage;
	pAdjustImg->m_memDC->FillSolidRect(0,0,m_StAdjustImage.m_nImageWidth,m_StAdjustImage.m_nImageHeight,GetSysColor(COLOR_BTNFACE));
}
void ESMKzonePrism::LoadData()
{
	m_AdjustDscList.DeleteAllItems();
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();
	for(int i =0; i< m_pAdjustMgr.GetDscCount(); i++)
	{
		//m_pAdjustMgr
		CString strTp;
		strTp.Format(_T("%d"), i + 1);
		m_AdjustDscList.InsertItem(i, strTp);
		strTp = m_pAdjustMgr.GetDscAt(i)->strDscName;
		m_AdjustDscList.SetItemText(i, 1, strTp);
		CString strDSC = m_AdjustDscList.GetItemText(i,1);
		m_adjinfo.push_back(pMovieMgr->GetAdjustData(strDSC));

		stAdjustInfo adjInfo = m_adjinfo.at(i);
		CESMImgMgr* pImgMgr = new CESMImgMgr();
		pImgMgr->SetMargin(m_MarginX, m_MarginY, adjInfo);
	}

	CString strTemp;
	m_RdImageViewColor = 0;

	m_nRdDetectColor = 0;
	m_clrDetectColor = COLOR_BLACK;
	m_pAdjustMgr.GetResolution(m_nWidth,m_nHeight);

	if( m_nMovieState == ESM_ADJ_FILM_STATE_MOVIE)
	{
		if( m_hTheadHandle ) 
		{
			CloseHandle(m_hTheadHandle);
			m_hTheadHandle = NULL;
		}

		m_hTheadHandle = (HANDLE) _beginthreadex(NULL, 0, GetMovieDataThread, (void *)this, 0, NULL);
	}
	else
	{
		DscAdjustInfo* pAdjustInfo = NULL;
		for(int i =0; i< m_pAdjustMgr.GetDscCount(); i++)
		{
			pAdjustInfo = m_pAdjustMgr.GetDscAt(i);
			if( pAdjustInfo != NULL)
			{
				if( m_hTheadHandle ) 
				{
					CloseHandle(m_hTheadHandle);
					m_hTheadHandle = NULL;
				}

				m_hTheadHandle = (HANDLE) _beginthreadex(NULL, 0, GetImageDataThread, (void *)pAdjustInfo, 0, NULL);
			}
		}
	}

	GetDlgItem(IDC_EDIT_PRISM_SIZE)->SetWindowText(_T("5"));
}
unsigned WINAPI ESMKzonePrism::GetMovieDataThread(LPVOID param)
{	
	ESMKzonePrism* pAdjustMgrDlg = (ESMKzonePrism*)param;
	DscAdjustInfo* pAdjustInfo = NULL;

	int nMovieIndex = 0;
	int nRealFrameIndex = 0;
	int nShiftFrame = ESMGetSelectedFrameNumber();
	ESMGetMovieIndex(nShiftFrame, nMovieIndex, nRealFrameIndex);

	for(int i =0; i< pAdjustMgrDlg->m_pAdjustMgr.GetDscCount(); i++)
	{
		pAdjustInfo = pAdjustMgrDlg->m_pAdjustMgr.GetDscAt(i);
		FFmpegManager FFmpegMgr(FALSE);
		CString strFolder, strDscId, strDscIp;
		strDscId = pAdjustInfo->strDscName;
		strDscIp = pAdjustInfo->strDscIp;
		//strFolder = ESMGetMoviePath(strDscId, 0);
		strFolder = ESMGetMoviePath(strDscId, nShiftFrame);	
		BOOL bReverse = pAdjustInfo->bReverse;
		if( strFolder == _T(""))
			return 0;

		BYTE* pBmpBits = NULL;
		int nWidht =0, nHeight = 0;
		FFmpegMgr.GetCaptureImage(strFolder, &pBmpBits,nRealFrameIndex, &nWidht, &nHeight, ESMGetGopSize(),bReverse);
		if( pBmpBits != NULL)
		{
			pAdjustInfo->pBmpBits = pBmpBits;
			pAdjustInfo->nWidht = nWidht;
			pAdjustInfo->nHeight = nHeight;
		}

		//wgkim@esmlab.com 17-06-21
		if(pAdjustMgrDlg->m_bThreadCloseFlag == TRUE)
			return 0;
	}
	return 0;
}
unsigned WINAPI ESMKzonePrism::GetImageDataThread(LPVOID param)
{	
	DscAdjustInfo* pAdjustInfo= (DscAdjustInfo*)param;

	CString strFolder, strDscId;
	strDscId = pAdjustInfo->strDscName;
	//if(ESMGetFrameRecord().GetLength())
	//	strFolder.Format(_T("%s\\%s\\%s.jpg"), ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), strDscId);

	if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
		strFolder = ESMGetPicturePath(strDscId);
	else
		strFolder.Format(_T("%s\\%s\\%s.jpg"), ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), strDscId);

	if( strFolder == _T(""))
		return 0;

	IplImage *pImage, *pTpImage;
	int nTarHeight = 1238, nTarWidth = 2200;

	char pchPath[MAX_PATH] = {0};
	int nRequiredSize = (int)wcstombs(pchPath, strFolder, MAX_PATH); 
	//pTpImage = cvLoadImage(pchPath);
	//pImage = cvLoadImage(pchPath);
	// 	pTpImage = cvCreateImage(cvSize(nTarWidth, nTarHeight), pImage->depth, pImage->nChannels); 
	// 	cvResize(pImage, pTpImage);

	pTpImage->width;
	pTpImage->height;
	BYTE* pBmpBits = NULL;
	int nWidht =0, nHeight = 0;
 	pBmpBits = new BYTE[pTpImage->height * pTpImage->widthStep];
	memcpy(pBmpBits, pTpImage->imageData, pTpImage->height * pTpImage->widthStep);
	if( pBmpBits != NULL)
	{
		pAdjustInfo->pBmpBits = pBmpBits;
		pAdjustInfo->nWidht = pTpImage->width;
		pAdjustInfo->nHeight = pTpImage->height;
	}
	//cvReleaseImage( &pImage );
	cvReleaseImage( &pTpImage );

	return 0;
}
void ESMKzonePrism::OnLvnItemchangedAdjust(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	Showimage();
	Invalidate();
	*pResult = 0;
}
void ESMKzonePrism::DrawCenterPointAll()
{

	CString strTp;
	int nPosX, nPosY;

	for ( int i = 1 ; i < 5 ; i++ )
	{
		strTp = m_AdjustDscList.GetItemText(m_nSelectedNum, i*2);
		nPosX = _ttoi(strTp);
		strTp = m_AdjustDscList.GetItemText(m_nSelectedNum, i*2 + 1);
		nPosY = _ttoi(strTp);

		DrawCenterPoint(nPosX, nPosY);
	}

}
void ESMKzonePrism::DrawCenterPoint(int nPosX, int nPosY)
{
	int nCircle = m_StAdjustImage.m_nCircleSize;

	if ( nPosX != 0 && nPosY != 0 )
	{
		CRect rtOutLine(nPosX-nCircle,nPosY-nCircle,nPosX+nCircle,nPosY+nCircle);
		m_StAdjustImage.DrawRect(rtOutLine, RGB(0,255,0));

		CRect rtCenter(nPosX-1,nPosY-1,nPosX+1,nPosY+1);
		m_StAdjustImage.DrawRect(rtCenter, RGB(255,0,0));
	}
}
void ESMKzonePrism::OnRButtonDown(UINT nFlags, CPoint point)
{
	int nStep;
	CListCtrl* pList;
	ESMKzoneImage* pAdjustImg;

	nStep = 2;
	pList = &m_AdjustDscList;
	pAdjustImg = &m_StAdjustImage;

	DrawImage();

	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );

	CString LocStr;
	m_ctrlCheckPoint.GetLBText(m_ctrlCheckPoint.GetCurSel(),LocStr);
	if(LocStr == "B Peak") m_nSelectPos = 3;
	else if(LocStr == "T Peak") m_nSelectPos = 2;
	else if(LocStr == "T LB") m_nSelectPos = 0;
	else if(LocStr == "T RB") m_nSelectPos = 1; 

	CString strSize;
	CString strText;
	GetDlgItem(IDC_EDIT_PRISM_SIZE)->GetWindowText(strSize);
	pAdjustImg->m_nCircleSize =  _ttoi(strSize) * 4;

	FindExactPosition(m_nPosX,m_nPosY);	
	
	int nCount = 0;
	if(LocStr == _T("None"))
	{
		if(nSelectedItem >= 0)
		{
			for(int i = 0 ; i < 4 ; i ++)
			{
				CString strPointX = pList->GetItemText(nSelectedItem,i * 2 + nStep);
				CString strPointY = pList->GetItemText(nSelectedItem,i * 2 + nStep + 1);

				int nXLength = strPointX.GetLength();
				int nYLength = strPointX.GetLength();
				TRACE("%d,%d\n",strPointX.GetLength(),strPointY.GetLength());
				if(nXLength > 0)
				{
					nCount++;
					continue;
				}
				if(nYLength > 0)
				{
					nCount++;
					continue;
				}

				strText.Format(_T("%d"), m_nPosX);
				pList->SetItemText(nSelectedItem, i * 2 + nStep, strText);
				strText.Format(_T("%d"), m_nPosY);
				pList->SetItemText(nSelectedItem, 1 + i * 2 + nStep, strText);
				break;
			}
		}
	}
	else
	{
		strText.Format(_T("%d"), m_nPosX);
		pList->SetItemText(nSelectedItem, m_nSelectPos * 2 + nStep, strText);
		strText.Format(_T("%d"), m_nPosY);
		pList->SetItemText(nSelectedItem, m_nSelectPos * 2 + 1 + nStep, strText);
	}
	if(nCount+1 == 4)
	{
		int nAllItem = m_AdjustDscList.GetItemCount();
		if(nAllItem > nSelectedItem+1)
		{ 
			SetTimer(TIMER_CHANGE_NEXT_ITEM, TIME_CHANGE_NEXT_ITEM, NULL);			
		}
	}
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), KZONEPRISM);
	SavePointData(strFileName);
	CDialogEx::OnRButtonDown(nFlags, point);
}
void ESMKzonePrism::OnMouseMove(UINT nFlags, CPoint point)
{
	ESMKzoneImage* pAdjustImg;
	pAdjustImg = &m_StAdjustImage;

	//for(int i = nCount; i < MAX_IMAGE_VIEW; i++)
	{
		CString strPos;
		CRect rect;
		pAdjustImg->GetWindowRect(&rect);
		ScreenToClient(&rect);
		int nPos = 0;
		if( point.x - rect.left < 0 || point.x > rect.right || point.y - rect.top < 0 || point.y > rect.bottom)
		{
			CDialogEx::OnMouseMove(nFlags, point);
			return;
		}

		//-- 2014-08-31 hongsu@esmlab.com
		//-- Get Image Position from Mouse Point
		CPoint ptImage;
		ptImage = GetPointFromMouse(point);

		strPos.Format(_T("%d"), ptImage.x);
		m_nPosX = ptImage.x - 1;
		GetDlgItem(IDC_PRISM_MOUSEPOSX)->SetWindowText(strPos);

		strPos.Format(_T("%d"), ptImage.y);
		m_nPosY = ptImage.y - 1;
		GetDlgItem(IDC_PRISM_MOUSEPOSY)->SetWindowText(strPos);


		pAdjustImg->OnMouseMove(nFlags, point);
	}
	CDialogEx::OnMouseMove(nFlags, point);
}
BOOL ESMKzonePrism::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	ESMKzoneImage* pAdjustImg;
	int nCount;
	nCount = ADJUST_IMAGE;

	pAdjustImg = &m_StAdjustImage;


	CRect rtWindow;
	GetWindowRect(rtWindow);

	pt.x -= rtWindow.left;
	pt.y -= rtWindow.top;


	CPoint ptImage;
	ptImage = GetPointFromMouse(pt);

	if( zDelta > 0)
	{
		if(  pAdjustImg->GetMultiple() <= 0.2)
			return TRUE;
		pAdjustImg->ImageZoom(ptImage, TRUE);
	}
	else
		pAdjustImg->ImageZoom(ptImage, FALSE);

	CString strTp;
	strTp.Format(_T("%.03lf"), pAdjustImg->GetMultiple());
	GetDlgItem(IDC_PRISM_IMAGESIZE)->SetWindowText(strTp);

	return CDialogEx::OnMouseWheel(nFlags, zDelta, pt);
}
CPoint ESMKzonePrism::GetPointFromMouse(CPoint pt)
{
	ESMKzoneImage* pAdjustImg;
	pAdjustImg = &m_StAdjustImage;

	CRect rect;
	pAdjustImg->GetWindowRect(&rect);
	ScreenToClient(&rect);

	CPoint ptRet;
	pt.x = pt.x - rect.left;
	pt.y = pt.y - rect.top;
	CPoint ptImage = pAdjustImg->GetImagePos();
	ptRet.x = (int)(ptImage.x * pAdjustImg->GetMultiple() + pt.x * pAdjustImg->GetMultiple());
	ptRet.y = (int)(ptImage.y * pAdjustImg->GetMultiple() + pt.y * pAdjustImg->GetMultiple());

	//ESMLog(5, _T("Moust Point x:%d y:%d"), ptRet.x, ptRet.y);
	return ptRet;
}
void ESMKzonePrism::OnLButtonDown(UINT nFlags, CPoint point)
{
	ESMKzoneImage* pAdjustImg;
	pAdjustImg= &m_StAdjustImage;

	int nCount;
	pAdjustImg->OnLButtonDown(nFlags, point);
	pAdjustImg->SetFocus();

	CDialogEx::OnLButtonDown(nFlags, point);
}
void ESMKzonePrism::OnLButtonUp(UINT nFlags, CPoint point)
{
	ESMKzoneImage* pAdjustImg;
	pAdjustImg = &m_StAdjustImage;

	pAdjustImg->OnLButtonUp(nFlags, point);

	CDialogEx::OnLButtonUp(nFlags, point);
}
void ESMKzonePrism::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// 	if( m_nSelectPos > 2)
	// 		m_nSelectPos = 0;
	// 
	// 	m_nSelectPos++;
	// 	CString strText;
	// 	int nSelectedItem = m_AdjustDscList.GetNextItem( -1, LVNI_SELECTED );
	// 	strText.Format(_T("%d"), m_nPosX);
	// 	m_AdjustDscList.SetItemText(nSelectedItem, m_nSelectPos * 2, strText);
	// 	strText.Format(_T("%d"), m_nPosY);
	// 	m_AdjustDscList.SetItemText(nSelectedItem, 1 + m_nSelectPos * 2, strText);
	CDialogEx::OnLButtonDblClk(nFlags, point);
}
void ESMKzonePrism::FindExactPosition(int& nX, int& nY)
{
	//-- 2014-08-30 hongsu@esmlab.com
	//-- Load Image 
	ESMKzoneImage* pAdjustImg;
	pAdjustImg = &m_StAdjustImage;

	UpdateData();
	pAdjustImg->FindExactPosition(nX, nY, m_nThreshold, m_bAutoFind);
}
void ESMKzonePrism::SavePointData(CString strFileName)
{
	CESMIni ini;
	CFile file;
	file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite);
	file.Close();

	if(!ini.SetIniFilename (strFileName))
		return;

	ini.WriteInt(_T("MovieSize"), _T("HEIGHT"), m_nHeight);
	ini.WriteInt(_T("MovieSize"), _T("WIDTH"), m_nWidth);

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);
		ini.WriteString(strSelect, _T("T_LBX"), m_AdjustDscList.GetItemText(i, 2));
		ini.WriteString(strSelect, _T("T_LBY"), m_AdjustDscList.GetItemText(i, 3));
		ini.WriteString(strSelect, _T("T_RBX"), m_AdjustDscList.GetItemText(i, 4));
		ini.WriteString(strSelect, _T("T_RBY"), m_AdjustDscList.GetItemText(i, 5));
		ini.WriteString(strSelect, _T("T_PeakX"), m_AdjustDscList.GetItemText(i, 6));
		ini.WriteString(strSelect, _T("T_PeakY"), m_AdjustDscList.GetItemText(i, 7));
		ini.WriteString(strSelect, _T("B_PeakX"), m_AdjustDscList.GetItemText(i, 8));
		ini.WriteString(strSelect, _T("B_PeakY"), m_AdjustDscList.GetItemText(i, 9));
	}

}
void ESMKzonePrism::LoadPointData(CString strFileName)
{
	CESMIni ini;
	if(!ini.SetIniFilename (strFileName))
		return;

	int nHeight = 0, nWidth = 0;
	nHeight = ini.GetInt(_T("MovieSize"), _T("HEIGHT"), 0);
	nWidth = ini.GetInt(_T("MovieSize"), _T("WIDTH"), 0);
	if(nHeight != 0)
		m_nHeight = nHeight;

	if(nWidth != 0)
		m_nWidth = nWidth;

	CString strData;
	//stPrismInfo axis;
	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);
		strData = ini.GetString(strSelect, _T("T_LBX"));
		m_AdjustDscList.SetItemText(i, 2, strData);
		//axis.t_LB.x = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("T_LBY"));
		m_AdjustDscList.SetItemText(i, 3, strData);
		//axis.t_LB.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("T_RBX"));
		m_AdjustDscList.SetItemText(i, 4, strData);
		//axis.t_RB.x = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("T_RBY"));
		m_AdjustDscList.SetItemText(i, 5, strData);
	//	axis.t_RB.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("T_PeakX"));
		m_AdjustDscList.SetItemText(i, 6, strData);
	//	axis.t_peak.x = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("T_PeakY"));
		m_AdjustDscList.SetItemText(i, 7, strData);
	//	axis.t_peak.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("B_PeakX"));
		m_AdjustDscList.SetItemText(i, 8, strData);
//		axis.b_peak.x = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("B_PeakY"));
		m_AdjustDscList.SetItemText(i, 9, strData);
	//	axis.b_peak.y = _ttoi(strData);

	//	m_prismaxis.push_back(axis);
	}
}
void ESMKzonePrism::OnBnClickedBtnAdjustPointReset()
{
	CESMFileOperation fo;
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), KZONEPRISM);
	fo.Delete(strFileName);
	CString strData = _T("");

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);
		m_AdjustDscList.SetItemText(i, 2, strData);
		m_AdjustDscList.SetItemText(i, 3, strData);
		m_AdjustDscList.SetItemText(i, 4, strData);
		m_AdjustDscList.SetItemText(i, 5, strData);
		m_AdjustDscList.SetItemText(i, 6, strData);
		m_AdjustDscList.SetItemText(i, 7, strData);
		m_AdjustDscList.SetItemText(i, 8, strData);
		m_AdjustDscList.SetItemText(i, 9, strData);
	}
	m_prismaxis.clear();
}
void ESMKzonePrism::OnBnClickedBtn2ndSavePoint()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));
	CString szFilter = _T("Point List (*.ptl)|*.ptl|");

#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  	
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = _T("Point Save");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();	
		if( strFileName.Right(4) != _T(".ptl"))
			strFileName = strFileName + _T(".ptl");
		SavePointData(strFileName);


		//Save Image Data
		CString strAdjustFolder;
		strAdjustFolder.Format(_T("%s\\img"),ESMGetPath(ESM_PATH_SETUP));
		CreateDirectory(strAdjustFolder, NULL);
	}
}
void ESMKzonePrism::OnBnClickedBtn2ndLoadPoint()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));
	// 	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
	// 		strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));
	// 	else
	// 		strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_CONF));

	CString szFilter = _T("Point List (*.ptl)|*.ptl|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = _T("Point Load");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();

		LoadPointData(strFileName);
		m_bChangeFlag = TRUE;
	}
}
int ESMKzonePrism::Round(double dData)
{
	int nResult = 0;
	if( dData == 0)
		nResult = 0;
	else if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);

	return nResult;
}
void ESMKzonePrism::GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle)
{
	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}


	if( dAngle == 0.0 && dScale == 0.0)
		return ;

	Mat rot_mat(cv::Size(2, 3), CV_32FC1);
	cv::Point rot_center( nCenterX, nCenterY);


	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust = -1 * (dAngle + 90);

	//CString strTemp;
	//strTemp.Format(_T("%f"), dbAngleAdjust);
	//m_AdjustDsc2ndList.SetItemText(m_nSelectedNum, 17, strTemp);

	rot_mat = getRotationMatrix2D(rot_center, dbAngleAdjust, dScale);

	//cuda::warpAffine(*gMat,d_rotate, rot_mat, Size(2500, 1500), cv::INTER_CUBIC+cv::WARP_FILL_OUTLIERS);
	cuda::GpuMat d_rotate;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- ? CvPoint2D32f srcTri[3], dstTri[3];
	cuda::warpAffine(*gMat, d_rotate, rot_mat, cv::Size(gMat->cols, gMat->rows), cv::INTER_CUBIC);
	d_rotate.copyTo(*gMat);

	//cvReleaseMat(&rot_mat);

}
void ESMKzonePrism::GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;


	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void ESMKzonePrism::GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void ESMKzonePrism::CpuMakeMargin(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;


	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);

}
void ESMKzonePrism::CpuMoveImage(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void ESMKzonePrism::CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle)
{
	Mat Iimage2;// = cvCreateImage(cvGetSize(Iimage), IPL_DEPTH_8U, 3);

	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;
	//cvShowImage("2",Iimage);
	CvMat *rot_mat = cvCreateMat( 2, 3, CV_32FC1);
	CvPoint2D32f rot_center = cvPoint2D32f( nCenterX, nCenterY);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust = -1 * (dAngle + 90);

	//CString strTemp;
	//strTemp.Format(_T("%f"), dbAngleAdjust);
	//m_AdjustDsc2ndList.SetItemText(m_nSelectedNum, 17, strTemp);

	cv2DRotationMatrix( rot_center, dbAngleAdjust, dScale, rot_mat);

	//cuda::warpAffine(*gMat,d_rotate, rot_mat, Size(2500, 1500), cv::INTER_CUBIC+cv::WARP_FILL_OUTLIERS);
	//Mat d_rotate;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- ? CvPoint2D32f srcTri[3], dstTri[3];
	cvWarpAffine(&IplImage(Iimage), &IplImage(Iimage), rot_mat,cv::INTER_LINEAR+cv::WARP_FILL_OUTLIERS);	//d_rotate.copyTo(*gMat);
	//imshow("Iimage",Iimage);
	//cvWaitKey(0);
	//gMat =&(cvarrToMat(Iimage2));//
	//Iimage = cvCreateImage(cvGetSize(&Iimage), IPL_DEPTH_8U, 3);
	cvReleaseMat(&rot_mat);
}
void ESMKzonePrism::OnBnAxisMaking()
{
	CListCtrl *plist;
	plist = &m_AdjustDscList;

	CString str_x,str_y;
	int t_peak_x,t_peak_y,t_LB_x,t_LB_y;
	int b_peak_x,b_peak_y,t_RB_x,t_RB_y;
	int t_LT_x,t_LT_y;
	int t_RT_x,t_RT_y;
	int height;
	stPrismInfo prism_axis_info;

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		str_x = plist->GetItemText(i,2);
		str_y = plist->GetItemText(i,3);
		t_LB_x = _ttoi(str_x);
		t_LB_y = _ttoi(str_y);

		str_x = plist->GetItemText(i,4);
		str_y = plist->GetItemText(i,5);
		t_RB_x = _ttoi(str_x);
		t_RB_y = _ttoi(str_y);

		str_x = plist->GetItemText(i,6);
		str_y = plist->GetItemText(i,7);
		t_peak_x = _ttoi(str_x);
		t_peak_y = _ttoi(str_y);

#if 1
		str_x = plist->GetItemText(i,8);
		str_y = plist->GetItemText(i,9);
		b_peak_x = _ttoi(str_x);
		b_peak_y = _ttoi(str_y);
		height = b_peak_y - t_peak_y;
#else
		b_peak_x = t_peak_x;
		b_peak_y = t_peak_y - 50;
		height = b_peak_y - t_peak_y;
#endif

		FindPrismPixel(t_peak_x,t_peak_y,t_LB_x,t_LB_y,t_RB_x,t_RB_y,&t_LT_x,&t_LT_y,&t_RT_x,&t_RT_y);

		prism_axis_info.t_peak = cv::Point(t_peak_x,t_peak_y);
		prism_axis_info.t_LB = cv::Point(t_LB_x,t_LB_y);
		prism_axis_info.t_RB = cv::Point(t_RB_x,t_RB_y);
		prism_axis_info.t_LT = cv::Point(t_LT_x,t_LT_y);
		prism_axis_info.t_RT = cv::Point(t_RT_x,t_RT_y);

		prism_axis_info.b_peak = cv::Point(b_peak_x,b_peak_y);
		prism_axis_info.b_LB = cv::Point(t_LB_x,t_LB_y+height);
		prism_axis_info.b_RB = cv::Point(t_RB_x,t_RB_y+height);
		prism_axis_info.b_LT = cv::Point(t_LT_x,t_LT_y+height);
		prism_axis_info.b_RT = cv::Point(t_RT_x,t_RT_y+height);
		prism_axis_info.height = height;

		m_prismaxis.push_back(prism_axis_info);
	}
	MessageBox(_T("KZone Prism\nPrism Making Complete!\nClick only one time!"),_T("KZone Prism"),NULL);
}
void ESMKzonePrism::FindPrismPixel(int t_Peak_x,int t_Peak_y,int t_LB_x,int t_LB_y,int t_RB_x, int t_RB_y, int *t_LT_x, int *t_LT_y,int *t_RT_x, int *t_RT_y)
{
	double aslope,acen,aline;
	aslope = ((double(t_LB_y-t_RB_y)/double(t_LB_x-t_RB_x)));
	acen = t_Peak_y - (aslope*t_Peak_x);//center점을 지나는 원점 방정식

	double cen_slope,left_cen,right_cen,l_line,r_line;
	cv::Point n_cen = cv::Point( (t_LB_x + t_RB_x)/2, (t_LB_y + t_RB_y)/2);
	cen_slope =  ((double(t_Peak_y-n_cen.y)/double(t_Peak_x-n_cen.x)));
	right_cen = t_RB_y - (cen_slope * t_RB_x);
	left_cen = t_LB_y - (cen_slope * t_LB_x);

	//Left top
	int L_Px,L_Py,n_LPx,n_LPy;
	L_Px = (left_cen - acen)/(aslope-cen_slope);
	L_Py = ((left_cen - acen)/(aslope-cen_slope))*aslope + acen;
	n_LPx = (L_Px + t_LB_x)/2;
	n_LPy = (L_Py + t_LB_y)/2;

	//Right top
	int R_Px,R_Py,n_RPx,n_RPy;
	R_Px = (right_cen - acen)/(aslope-cen_slope);
	R_Py = ((right_cen - acen)/(aslope-cen_slope))*aslope + acen;
	n_RPx = (R_Px + t_RB_x)/2;
	n_RPy = (R_Py + t_RB_y)/2;

	*t_LT_x = n_LPx;
	*t_LT_y = n_LPy;

	*t_RT_x = n_RPx;
	*t_RT_y = n_RPy;

	if(t_Peak_x == 0 && t_Peak_y == 0 && t_RB_x == 0 && t_RB_y == 0 && t_LB_x == 0 && t_LB_y == 0)
	{
		*t_RT_x = 0;
		*t_RT_y = 0;
		*t_LT_x = 0;
		*t_LT_y = 0;
	}
}
void ESMKzonePrism::OnBnAxisInterpolation()
{
	int i,j;
	int max_dsc = m_AdjustDscList.GetItemCount();
	CString strdsc;
	CString enddsc;

	GetDlgItem(IDC_EDIT_STRDSC)->GetWindowText(strdsc);
	GetDlgItem(IDC_EDIT_ENDDSC)->GetWindowText(enddsc);
	int start,end;
	start = _ttoi64(strdsc) -1;
	end = _ttoi64(enddsc) -1;
	CListCtrl* pList;
	CString strText;
	stPrismInfo axis;

	if(max_dsc-1 == end && start == 0)
	{
		for(i=start;i<end;i++)
		{
			int t_p_x = 0, t_p_y = 0,t_lt_x = 0,t_lt_y = 0,t_lb_x = 0;
			int t_lb_y = 0, t_rb_x = 0, t_rb_y = 0, t_rt_x = 0, t_rt_y = 0;
			int b_p_x = 0,b_p_y = 0,b_lt_x = 0,b_lt_y = 0,b_lb_x = 0,b_lb_y = 0;
			int b_rb_x = 0, b_rb_y = 0, b_rt_x = 0, b_rt_y = 0;
			int cnt = 0;
			for(j=-2;j<=2;j++)
			{
				if(i+j <0) continue;
				else if(i+j > end-1) continue;
				else
				{
					axis = m_prismaxis.at(i+j);
					if(axis.t_peak.x == 0 && axis.t_peak.y == 0) continue;
					t_p_x += axis.t_peak.x;	t_p_y += axis.t_peak.y;
					t_lb_x += axis.t_LB.x;t_lb_y += axis.t_LB.y;
					t_lt_x += axis.t_LT.x;t_lt_y += axis.t_LT.y;
					t_rb_x += axis.t_RB.x;t_rb_y += axis.t_RB.y;
					t_rt_x += axis.t_RT.x;t_rt_y += axis.t_RT.y;

					b_p_x += axis.b_peak.x;	b_p_y += axis.b_peak.y;
					b_lb_x += axis.b_LB.x; b_lb_y += axis.b_LB.y;
					b_lt_x += axis.b_LT.x;b_lt_y += axis.b_LT.y;
					b_rb_x += axis.b_RB.x;b_rb_y += axis.b_RB.y;
					b_rt_x += axis.b_RT.x;b_rt_y += axis.b_RT.y;
					cnt ++;
				}
			}
			axis.t_peak.x = t_p_x / cnt;axis.t_peak.y = t_p_y / cnt;
			axis.t_LB.x = t_lb_x / cnt;axis.t_LB.y = t_lb_y / cnt;
			axis.t_LT.x = t_lt_x / cnt;axis.t_LT.y = t_lt_y / cnt;
			axis.t_RB.x = t_rb_x / cnt;axis.t_RB.y = t_rb_y / cnt;
			axis.t_RT.x = t_rt_x / cnt;axis.t_RT.y = t_rt_y / cnt;

			axis.b_peak.x = b_p_x / cnt;axis.b_peak.y = b_p_y / cnt;
			axis.b_LB.x = b_lb_x / cnt;axis.b_LB.y = b_lb_y / cnt;
			axis.b_LT.x = b_lt_x / cnt;axis.b_LT.y = b_lt_y / cnt;
			axis.b_RB.x = b_rb_x / cnt;axis.b_RB.y = b_rb_y / cnt;
			axis.b_RT.x = b_rt_x / cnt;axis.b_RT.y = b_rt_y / cnt;

			m_prismaxis.at(i).t_peak = cv::Point(axis.t_peak.x,axis.t_peak.y);
			m_prismaxis.at(i).t_LB = cv::Point(axis.t_LB.x,axis.t_LB.y);
			m_prismaxis.at(i).t_LT = cv::Point(axis.t_LT.x,axis.t_LT.y);
			m_prismaxis.at(i).t_RB = cv::Point(axis.t_RB.x,axis.t_RB.y);
			m_prismaxis.at(i).t_RT = cv::Point(axis.t_RT.x,axis.t_RT.y);

			m_prismaxis.at(i).b_peak = cv::Point(axis.b_peak.x,axis.b_peak.y);
			m_prismaxis.at(i).b_LB = cv::Point(axis.b_LB.x,axis.b_LB.y);
			m_prismaxis.at(i).b_LT = cv::Point(axis.b_LT.x,axis.b_LT.y);
			m_prismaxis.at(i).b_RB = cv::Point(axis.b_RB.x,axis.b_RB.y);
			m_prismaxis.at(i).b_RT = cv::Point(axis.b_RT.x,axis.b_RT.y);
			m_prismaxis.at(i).height = axis.b_peak.y - axis.t_peak.y;
		}
	}
	else
	{
		for(i=start;i<end+1;i++)
		{
			int t_p_x = 0, t_p_y = 0,t_lt_x = 0,t_lt_y = 0,t_lb_x = 0;
			int t_lb_y = 0, t_rb_x = 0, t_rb_y = 0, t_rt_x = 0, t_rt_y = 0;
			int b_p_x = 0,b_p_y = 0,b_lt_x = 0,b_lt_y = 0,b_lb_x = 0,b_lb_y = 0;
			int b_rb_x = 0, b_rb_y = 0, b_rt_x = 0, b_rt_y = 0;
			int cnt = 0;
			for(j=-2;j<=2;j++)
			{
				if(i+j <0) continue;
				else if(i+j > end) continue;
				else
				{
					axis = m_prismaxis.at(i+j);
					if(axis.t_peak.x == 0 && axis.t_peak.y == 0) continue;
					t_p_x += axis.t_peak.x;	t_p_y += axis.t_peak.y;
					t_lb_x += axis.t_LB.x;t_lb_y += axis.t_LB.y;
					t_lt_x += axis.t_LT.x;t_lt_y += axis.t_LT.y;
					t_rb_x += axis.t_RB.x;t_rb_y += axis.t_RB.y;
					t_rt_x += axis.t_RT.x;t_rt_y += axis.t_RT.y;

					b_p_x += axis.b_peak.x;	b_p_y += axis.b_peak.y;
					b_lb_x += axis.b_LB.x; b_lb_y += axis.b_LB.y;
					b_lt_x += axis.b_LT.x;b_lt_y += axis.b_LT.y;
					b_rb_x += axis.b_RB.x;b_rb_y += axis.b_RB.y;
					b_rt_x += axis.b_RT.x;b_rt_y += axis.b_RT.y;
					cnt ++;
				}
			}
			axis.t_peak.x = t_p_x / cnt;axis.t_peak.y = t_p_y / cnt;
			axis.t_LB.x = t_lb_x / cnt;axis.t_LB.y = t_lb_y / cnt;
			axis.t_LT.x = t_lt_x / cnt;axis.t_LT.y = t_lt_y / cnt;
			axis.t_RB.x = t_rb_x / cnt;axis.t_RB.y = t_rb_y / cnt;
			axis.t_RT.x = t_rt_x / cnt;axis.t_RT.y = t_rt_y / cnt;

			axis.b_peak.x = b_p_x / cnt;axis.b_peak.y = b_p_y / cnt;
			axis.b_LB.x = b_lb_x / cnt;axis.b_LB.y = b_lb_y / cnt;
			axis.b_LT.x = b_lt_x / cnt;axis.b_LT.y = b_lt_y / cnt;
			axis.b_RB.x = b_rb_x / cnt;axis.b_RB.y = b_rb_y / cnt;
			axis.b_RT.x = b_rt_x / cnt;axis.b_RT.y = b_rt_y / cnt;

			m_prismaxis.at(i).t_peak = cv::Point(axis.t_peak.x,axis.t_peak.y);
			m_prismaxis.at(i).t_LB = cv::Point(axis.t_LB.x,axis.t_LB.y);
			m_prismaxis.at(i).t_LT = cv::Point(axis.t_LT.x,axis.t_LT.y);
			m_prismaxis.at(i).t_RB = cv::Point(axis.t_RB.x,axis.t_RB.y);
			m_prismaxis.at(i).t_RT = cv::Point(axis.t_RT.x,axis.t_RT.y);

			m_prismaxis.at(i).b_peak = cv::Point(axis.b_peak.x,axis.b_peak.y);
			m_prismaxis.at(i).b_LB = cv::Point(axis.b_LB.x,axis.b_LB.y);
			m_prismaxis.at(i).b_LT = cv::Point(axis.b_LT.x,axis.b_LT.y);
			m_prismaxis.at(i).b_RB = cv::Point(axis.b_RB.x,axis.b_RB.y);
			m_prismaxis.at(i).b_RT = cv::Point(axis.b_RT.x,axis.b_RT.y);
			m_prismaxis.at(i).height = axis.b_peak.y - axis.t_peak.y;
		}
		for(i=end+1;i<max_dsc;i++)
		{
			int t_p_x = 0, t_p_y = 0,t_lt_x = 0,t_lt_y = 0,t_lb_x = 0;
			int t_lb_y = 0, t_rb_x = 0, t_rb_y = 0, t_rt_x = 0, t_rt_y = 0;
			int b_p_x = 0,b_p_y = 0,b_lt_x = 0,b_lt_y = 0,b_lb_x = 0,b_lb_y = 0;
			int b_rb_x = 0, b_rb_y = 0, b_rt_x = 0, b_rt_y = 0;
			int cnt = 0;
			for(j=-2;j<=2;j++)
			{
				if(i+j <0) continue;
				else if(i+j > max_dsc-1) continue;
				else
				{
					axis = m_prismaxis.at(i+j);
					if(axis.t_peak.x == 0 && axis.t_peak.y == 0) continue;
					t_p_x += axis.t_peak.x;	t_p_y += axis.t_peak.y;
					t_lb_x += axis.t_LB.x;t_lb_y += axis.t_LB.y;
					t_lt_x += axis.t_LT.x;t_lt_y += axis.t_LT.y;
					t_rb_x += axis.t_RB.x;t_rb_y += axis.t_RB.y;
					t_rt_x += axis.t_RT.x;t_rt_y += axis.t_RT.y;

					b_p_x += axis.b_peak.x;	b_p_y += axis.b_peak.y;
					b_lb_x += axis.b_LB.x; b_lb_y += axis.b_LB.y;
					b_lt_x += axis.b_LT.x;b_lt_y += axis.b_LT.y;
					b_rb_x += axis.b_RB.x;b_rb_y += axis.b_RB.y;
					b_rt_x += axis.b_RT.x;b_rt_y += axis.b_RT.y;
					cnt ++;
				}
			}
			axis.t_peak.x = t_p_x / cnt;axis.t_peak.y = t_p_y / cnt;
			axis.t_LB.x = t_lb_x / cnt;axis.t_LB.y = t_lb_y / cnt;
			axis.t_LT.x = t_lt_x / cnt;axis.t_LT.y = t_lt_y / cnt;
			axis.t_RB.x = t_rb_x / cnt;axis.t_RB.y = t_rb_y / cnt;
			axis.t_RT.x = t_rt_x / cnt;axis.t_RT.y = t_rt_y / cnt;

			axis.b_peak.x = b_p_x / cnt;axis.b_peak.y = b_p_y / cnt;
			axis.b_LB.x = b_lb_x / cnt;axis.b_LB.y = b_lb_y / cnt;
			axis.b_LT.x = b_lt_x / cnt;axis.b_LT.y = b_lt_y / cnt;
			axis.b_RB.x = b_rb_x / cnt;axis.b_RB.y = b_rb_y / cnt;
			axis.b_RT.x = b_rt_x / cnt;axis.b_RT.y = b_rt_y / cnt;

			m_prismaxis.at(i).t_peak = cv::Point(axis.t_peak.x,axis.t_peak.y);
			m_prismaxis.at(i).t_LB = cv::Point(axis.t_LB.x,axis.t_LB.y);
			m_prismaxis.at(i).t_LT = cv::Point(axis.t_LT.x,axis.t_LT.y);
			m_prismaxis.at(i).t_RB = cv::Point(axis.t_RB.x,axis.t_RB.y);
			m_prismaxis.at(i).t_RT = cv::Point(axis.t_RT.x,axis.t_RT.y);

			m_prismaxis.at(i).b_peak = cv::Point(axis.b_peak.x,axis.b_peak.y);
			m_prismaxis.at(i).b_LB = cv::Point(axis.b_LB.x,axis.b_LB.y);
			m_prismaxis.at(i).b_LT = cv::Point(axis.b_LT.x,axis.b_LT.y);
			m_prismaxis.at(i).b_RB = cv::Point(axis.b_RB.x,axis.b_RB.y);
			m_prismaxis.at(i).b_RT = cv::Point(axis.b_RT.x,axis.b_RT.y);
			m_prismaxis.at(i).height = axis.b_peak.y - axis.t_peak.y;
		}
	}
	MessageBox(_T("KZone Prism\nInterpolation Complete!"),_T("KZone Prism"),NULL);
}
void ESMKzonePrism::OnBnDrawPrismBone()
{
	if(!load_prism)
	{
		load_prism = 1;
		ESMLog(1,_T("Loading Prism architect"));
	}
	else
	{
		load_prism = 0;
		ESMLog(1,_T("Loading Prism false"));

	}
}
void ESMKzonePrism::OnBnPrismImageMaking()
{
	Mat pentagon = imread("C:\\Program Files\\ESMLab\\4DMaker\\img\\06.Prism\\pentagon.png",1);
	Mat square0 = imread("C:\\Program Files\\ESMLab\\4DMaker\\img\\06.Prism\\square0.png",1);
	Mat square1 = imread("C:\\Program Files\\ESMLab\\4DMaker\\img\\06.Prism\\square1.png",1);
	
	CString Iindex;
	GetDlgItem(IDC_KZONE_INDEX)->GetWindowText(Iindex);

	if(pentagon.empty() || square0.empty() || square1.empty())
	{
		MessageBox(_T("Check your Image Path"),_T("KZone Prism"),NULL);
		return;
	}
	int nWidth = 3840;
	int nHeight = 2160;

	Mat t_penta		 = Mat::zeros(nHeight,nWidth,CV_8UC3);
	Mat b_penta	     = Mat::zeros(nHeight,nWidth,CV_8UC3);
	Mat P_LT_square	 = Mat::zeros(nHeight,nWidth,CV_8UC3);
	Mat LT_LB_square = Mat::zeros(nHeight,nWidth,CV_8UC3);
	Mat LB_RB_square = Mat::zeros(nHeight,nWidth,CV_8UC3);
	Mat RB_RT_square = Mat::zeros(nHeight,nWidth,CV_8UC3);
	Mat RT_P_square  = Mat::zeros(nHeight,nWidth,CV_8UC3);
	Mat Image_prism  = Mat::zeros(nHeight,nWidth,CV_8UC3);
	Mat result0(nHeight,nWidth,CV_8UC3);
	Mat result1(nHeight,nWidth,CV_8UC3);


	Point2f axis_penta[3];
	Point2f axis_square0[3];
	Point2f axis_square1[3];

	axis_penta[0] = Point2f(0,pentagon.rows/2);//tLB
	axis_penta[1] = Point2f(pentagon.cols-1,0);//tRB
	axis_penta[2] = Point2f(pentagon.cols-1,pentagon.rows-1);	//bLB

	axis_square0[0] = Point2f(0,0);//tLB
	axis_square0[1] = Point2f(square0.cols-1,0);//tRB
	axis_square0[2] = Point2f(0,square0.rows-1);	//bLB

	axis_square1[0] = Point2f(0,0);//tLB
	axis_square1[1] = Point2f(square1.cols-1,0);//tRB
	axis_square1[2] = Point2f(0,square1.rows-1);//bLB

	stPrismInfo axis;
	Scalar b_color(0,0,0);
	char top_path[200],side_path[200];
	int nLineTickness = m_nTickness;
	ESMLog(5,_T("[OnBnPrismImageMaking] Tickness %d"),nLineTickness);

	for(int i =0;i<m_AdjustDscList.GetItemCount();i++)
	{
		if(_ttoi(m_AdjustDscList.GetItemText(i,2)) == 0)
			continue;

		result0 = Scalar(0,0,0);
		result1 = Scalar(0,0,0);

		axis = m_prismaxis.at(i);
		sprintf(top_path,"C:\\Program Files\\ESMLab\\4DMaker\\img\\06.Prism\\[%s]%d_0.png",Iindex,i);
		sprintf(side_path,"C:\\Program Files\\ESMLab\\4DMaker\\img\\06.Prism\\[%s]%d_1.png",Iindex,i);

		affinetransform(square1,axis.t_LB,axis.t_RB,axis.b_LB,axis_square1,LB_RB_square);
		//affinetransform(square0,axis.t_peak,axis.t_LT,axis.b_peak,axis_square0,P_LT_square);
		//affinetransform(square0,axis.t_LT,axis.t_LB,axis.b_LT,axis_square0,LT_LB_square);
		//affinetransform(square0,axis.t_RT,axis.t_RB,axis.b_RT,axis_square0,RB_RT_square);
		//affinetransform(square0,axis.t_peak,axis.t_RT,axis.b_peak,axis_square0,RT_P_square);
		//affinetransform(pentagon,axis.b_peak,axis.b_LB,axis.b_RB,axis_penta,b_penta);
		//affinetransform(pentagon,axis.t_peak,axis.t_LB,axis.t_RB,axis_penta,t_penta);

		add(result0,LB_RB_square,result0,noArray());
		//add(result0,b_penta,result0,noArray());
		//add(result0,t_penta,result0,noArray());
		//add(result1,P_LT_square,result1,noArray());
		//add(result1,LT_LB_square,result1,noArray());
		//add(result1,RB_RT_square,result1,noArray());
		//add(result1,RT_P_square,result1,noArray());

		line(result0,axis.t_peak,axis.t_LT,b_color,nLineTickness,8);
		line(result0,axis.t_LT,axis.t_LB,b_color,nLineTickness,8);
		line(result0,axis.t_LB,axis.t_RB,b_color,nLineTickness,8);
		line(result0,axis.t_RB,axis.t_RT,b_color,nLineTickness,8);
		line(result0,axis.t_RT,axis.t_peak,b_color,nLineTickness,8);

		line(result0,axis.t_peak,axis.b_peak,b_color,nLineTickness,8);
		line(result0,axis.t_LT,axis.b_LT,b_color,nLineTickness,8);
		line(result0,axis.t_LB,axis.b_LB,b_color,nLineTickness,8);
		line(result0,axis.t_RB,axis.b_RB,b_color,nLineTickness,8);
		line(result0,axis.t_RT,axis.b_RT,b_color,nLineTickness,8);

		line(result0,axis.b_peak,axis.b_LT,b_color,nLineTickness,8);
		line(result0,axis.b_LT,axis.b_LB,b_color,nLineTickness,8);
		line(result0,axis.b_LB,axis.b_RB,b_color,nLineTickness,8);
		line(result0,axis.b_RB,axis.b_RT,b_color,nLineTickness,8);
		line(result0,axis.b_RT,axis.b_peak,b_color,nLineTickness,8);	

		imwrite(top_path,result0);
		imwrite(side_path,result1);
	}
	MessageBox(_T("KZone Prism\nPrism Image Saving Complete!"),_T("KZone Prism"),NULL);
}
void ESMKzonePrism::affinetransform(Mat img,cv::Point a,cv::Point b, cv::Point c, Point2f srcTri[3],Mat dst)
{
	Mat warp_mat(2,3,CV_32FC1);
	Point2f dstTri[3];
	dstTri[0] = a;	dstTri[1] = b;	dstTri[2] = c;

	warp_mat = getAffineTransform(srcTri,dstTri);
	warpAffine(img,dst,warp_mat,dst.size());
}
void ESMKzonePrism::Axis_Update()
{
	int i;

	for(i=0;i<m_prismaxis.size();i++)
	{
		m_prismaxis.at(i).t_LT.y = m_prismaxis.at(i).t_LT.y+m_Scale+m_Move;
		m_prismaxis.at(i).t_LB.y = m_prismaxis.at(i).t_LB.y+m_Scale+m_Move; 
		m_prismaxis.at(i).t_RB.y = m_prismaxis.at(i).t_RB.y+m_Scale+m_Move; 
		m_prismaxis.at(i).t_RT.y = m_prismaxis.at(i).t_RT.y+m_Scale+m_Move; 
		m_prismaxis.at(i).t_peak.y = m_prismaxis.at(i).t_peak.y+m_Scale+m_Move; 

		m_prismaxis.at(i).b_LT.y = m_prismaxis.at(i).b_LT.y+m_Move;
		m_prismaxis.at(i).b_LB.y = m_prismaxis.at(i).b_LB.y+m_Move; 
		m_prismaxis.at(i).b_RB.y = m_prismaxis.at(i).b_RB.y+m_Move; 
		m_prismaxis.at(i).b_RT.y = m_prismaxis.at(i).b_RT.y+m_Move; 
		m_prismaxis.at(i).b_peak.y = m_prismaxis.at(i).b_peak.y+m_Move; 

		m_prismaxis.at(i).height =m_prismaxis.at(i).t_peak.y - m_prismaxis.at(i).b_peak.y;
	}
}
BOOL ESMKzonePrism::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
		if(pMsg->message == WM_KEYDOWN)
		{
			switch(pMsg->wParam)
			{		
			case VK_DELETE://170615 - added by hjcho
				{
					DeleteSelectedList();
				}
				break;
			case VK_ADD:
				{
					if(++m_nTickness > 5)
						m_nTickness = 5;
					ESMLog(5,_T("Tick: %d"),m_nTickness);
				}
				break;
			case VK_SUBTRACT:
				{
					if(--m_nTickness <= 1)
						m_nTickness = 1;
					ESMLog(5,_T("Tick: %d"),m_nTickness);
				}
				break;
			}
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}
void ESMKzonePrism::DeleteSelectedList()
{
	int nSelectedCount = m_AdjustDscList.GetSelectedCount();

	if(nSelectedCount == 1)
	{
		POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();
		int nDeleteItem = m_AdjustDscList.GetNextSelectedItem(pos);

		DeleteSelectItem(nDeleteItem);
	}
	else
	{
		POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();

		while(pos)
		{			
			int nDeleteItem = m_AdjustDscList.GetNextSelectedItem(pos);
			DeleteSelectItem(nDeleteItem);
		}
	}
}
void ESMKzonePrism::DeleteSelectItem(int nIndex)
{
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), KZONEPRISM);

	//선택 아이템 삭제 ( 2,3,4,5,6,7,8,9 )
	for(int i = 2; i < 10 ; i++)
	{
		m_AdjustDscList.SetItemText(nIndex,i,NULL);
	}
	SavePointData(strFileName);
}