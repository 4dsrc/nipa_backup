

========================================================================
       ESMLab : SMILE / Version 2
========================================================================

ver 2.1.0.0
12.11.12 - dongho.shin
- Acceptance Test

ver 2.0.3.2
12.08.22 - hongsu.jung
- Add test PTZ NCAM
- Add test PTZ DVR	(alpha version)

ver 2.0.3.1
12.08.22 - hongsu.jung
- Add Viewer:Test Area
	. Color Panel
- Add Graph:Latency Analysis
	. Show Color Detect Percentage	
	
ver 2.0.2.3
12.08.01 - hongsu.jung
- Add Viewer:Frame Deviation
	. Add Graph in Frame Analisys View
	. Calculate Frame Standard Deviation
	
ver 2.0.2.2
12.07.31 - hongsu.jung
- Add Viewer:GOP Analysis View
	. Calculate I,P,B Frame
	. Check Base GOP Size

ver 2.0.2.1
12.07.27 - yongmin.lee
- Add test PTZ ACAM (alpha version)

ver 2.0.2.0
12.07.24 - hongsu.jung
- Change Skin ( Office2007_R1 Style)
- Add Serial View ( 4 -> 8 : SERIAL_PORT_SIZE)
- Add Engine:Log Report
	. Get Log From CGI
	. Check Contents
- Add Step:CGI
	. Informal CGI Step (command with cgi string)
	
ver 2.0.1.9
12.07.17 - hongsu.jung
- Image Verification Module (alpha version)

ver 2.0.1.8
12.06.27 - yongmin.lee
- DST Change Time real calculation
- NextCase Recursive Function Modify for While

ver 2.0.1.7
12.05.31 - yongmin.lee
- Add Test LiveStreaming 
- Network Thread-based change

ver 2.0.1.6
12.05.21 - hongsu.jung
- Database Locking
- ESMLog member variable Change

ver 2.0.1.5
12.05.11 - 
- Modified abnormal start/stop in Manual Recording. 
- Modified run-time error in ODBC logic.

ver.2.0.1.4
12.05.03 - hongsu
-   Add DST List Viewer

ver.2.0.1.3
12.05.03 - 
-   Modified empty macaddress in HTTP.

ver.2.0.1.2
12.05.02 - 
- Modified OnNewMedia()run-time error in release mode(Codec/Resolution, Bandwidth).
 . Changes ESMConvertTimeToSystemTime() into TickToSystemTime()
- Adds XnsDevice.IsPlaying() fn exception handling.
- Adds DST DB extra field.

ver.2.0.1.0
12.04.30 - hongsu.jung
- NVR/DVR : XNS 1.37 version
	. Codec/Resolution
	. Bandwidth

ver.2.0.0.11
12.04.24 - 
- Resolved blocking issue in [Stop] teststep.

ver.2.0.0.10
12.04.17 - hongsu.jung
- Remove Authority (in XNS)


ver.2.0.0.9
12.04.17 - hongsu.jung
- NVR/DVR : XNS 1.37 version


ver.2.0.0.7
12.04.17 - 
- Remove first 1-second data and last 1-second data of Codec/Resolution testing data.  

ver.2.0.0.6
12.04.17 - hongsu.jung
- Post Fail System
  . Retry/Stop/Continue

ver.2.0.0.5
12.04.17 - hongsu.jung
- OSD Test
  . Check Compare String Data
  . Add Fail Policy

ver.2.0.0.4
12.04.12 - yongmin.lee
- ACAM RS_485 serial
  . CheckReturnValue Func in case all

ver.2.0.0.3
12.04.12 - hongsu.jung
- OCR Compare
  . Change Logic for compare number
  
ver.2.0.0.1
12.04.06 - hongsu.jung
- Change ODBC Setting
  . Connection Constancy
  . Change Auto Connect Option
- NVR/Bandwidth DB Insert ����
  . ProfileName.
    
  
ver.2.0.0.0
12.04.04
- Refactoring Project
  . Rename File / Folder
  . Remove TesESMuarantee Name, Samsung SDS
  

========================================================================
       ESMLab : TesESMuarantee
========================================================================

ver.4.0.0.1
11.10.06
- Refactoring Project
  . Remove DSC Steps
  . Move File to Folder
	
========================================================================
       TesESMuarantee  :  Version 4.0
========================================================================

ver.3.2.3.2(����)
11.08.08 - 
- Increases the stack buffer size of the serial event string log and adds "\r\n" to the end of line.
- Modifies "End of Each TestSuite" logic in Excel Report.

ver.3.2.3.1
- Creates a new directory with the name of the date("2011-07-26") for logging when the date changes.

ver.3.2.3.0
11.07.23 - hongsu.jung
- Increases the maximum size of the displaying buffer in Output View. 
  
ver.3.2.2.9
11.07.22 - hongsu.jung
- If there is no "&&&&"/"####" seperator in MLC packet payload, return ERROR.

ver.3.2.2.8
11.07.20 - hongsu.jung
- In NX200, calls to the upper functions failed with "FT_IO_ERROR" sometimes after 
  about four hours or so. If power-status is false, open the ESMSwitchControl device again.
- Display the thread-count of Zoran Thread Monitoring View("Thread Count: 000/000").
	
ver.3.2.2.7
11.07.18 - hongsu.jung
- Modifies the touch dragging logic for "MV800(coach12)".
- Modifies the handling logic of the key event status for "WB700(D3)"

ver.3.2.2.0
11.07.11 - hongsu.jung
- In MLC, Test Guarantee Tool waits for the specified interval even if the item count is zero(failed).
- Modifies the touch logic for "MV800(coach12)".

ver.3.2.1.9
11.07.05 - hongsu.jung
- Adds the touch logic for "MV800(coach12)".

ver.3.2.1.8
11.06.30 - hongsu.jung
- The MLC operation test of "F70" has been completed succefully.

ver.3.2.1.5
11.06.27 - hongsu.jung
- Adds the boolean flag not to insert data into DB anymore, if DB is full.

ver.3.2.1.4
11.06.23 - hongsu.jung
- Adds temperature test chamber logic.
- Adds the function that cpu task polling and switching data can be printed out to excel file.
- Fixes the bug that the task information of the same id, name but the different baseaddr fails
  to insert into DB.
  
ver.3.2.1.0
11.06.01 - hongsu.jung
- The asynchronous operation "TestStep" has been changed into the synchonous(Request-Response) operation.
- The title name has changed. ("TesESMuarantee" -> "TMS Tool")

ver.3.2.0.3
11.04.12 - Keunbae.Song
- The DB table name has changed ("SQE_CPU_RESULT_COACH12" -> "SQE_CPU_RESULT")
- The DB table name has changed ("SQE_CPU_RESULT_COACH13" -> "SQE_CPU_RESULT")
- The Db field name has changed ("Thread_ID" -> "Task_ID")

ver.3.2.0.1
11.04.08 - Keunbae Song
- commented GetRandomTestText(touch) for ST700 MLC Test

ver.3.2.0.0
11.04.07 - Keunbae Song
- Bug Fix The logic for comparing Widget-Size with String-Size.

ver.3.0.0.7
10.08.11 - Hongsu Jung
- Bug Fix CreateGraph
	. Fix Sometimes Non Create Graph	

ver.3.0.0.6
10.08.03 - Hongsu Jung
- Fucntion Monitoring
	. Capture Time 
	. Capture Time Viewer Add
		

ver.3.0.0.5
10.07.29 - Hongsu Jung
- Upgrade Graph Module
	: Update Time
	
	
ver.3.0.0.4
10.07.23 - Hongsu Jung
- Check F/W Version
- Buffer Name Format Change
	: Name_XX
- Capture Time Reset in rebooting


ver.3.0.0.3
10.07.22 - Hongsu Jung
- Set Reboot Interval Time
- Unknown Message Exception


ver.3.0.0.2
10.07.20
- Support Get Capture File (Index)
10.07.21
- Exception For Getting F/W Version
- Exception Send Database on non Uniqui ID


ver.3.0.0.1
10.07.15
- Craete New Version [3.0]
- Support Windows 7
	. Support F/W D2P
	. Support F/W Zoran

========================================================================
       TesESMuarantee  :  Version 3.0
========================================================================

ver.2.1
10.02.14 - Hongsu Jung
- Refactoring
- Add Multi Connection


ver.2,0,1,9
09.09.25 - Hongsu Jung
- T/C Creator Modification
    . Separate Key Operation and getting OSD
    . Seperate from Push/Press/Release


ver.2,0,1,8
09.09.22 - Hongsu Jung
- Change Capture Path (with Time Tag as distingushed from other testcase)
    . add $(TIME) in Capture file name
- Delete Previous Images
    
ver.2,0,1,7
09.09.15 - Hongsu Jung
- Separate Error Reason in Capturing
    "Focus Error or Non Capturing"
	"Can't Take Picture"
	"Can't Get File ID After Capture"
	"Can't Download File From SD Card"	    
- Optional Fail Rule
    . missing focussing.
    . Fail/Pass selection

ver.2,0,1,6
09.09.09 - Hongsu Jung
- Optional Remove Captured File in SD Card
    . => Menu => Configuration => Test Option => Option
    . Check Box "Remove file in SD Card after download"
    . Button "Remove captured files in file server" : Remove all downloaded files in File Server


ver.2,0,1,5
09.09.02 - Hongsu Jung
- Add Naming String
    . $(DATE) : Get Test Date based T/C
    . $(TIME) : Get Test Time based T/C
    . $(FILENAME) : Get real file name in SD Card


ver.2,0,1,4
09.08.24 - Hongsu Jung
- Additional Function
    . Basic Fucntion Test Module
    . Each Change Mode Capture Test System


ver.2,0,1,3
09.08.23 - Hongsu Jung
- Additional Function
    . Reference Image Auto Gathering for Taguchi


ver.2,0,1,2
09.08.21 - Hongsu Jung
- Debugging RGB Analysis Fucntion  


ver.2,0,1,1 
09.08.20 - Hongsu Jung
- Change how to transfer step from Taguchi Table
    KEY_WHEEL1,n ==> KEY_WHEEL1,1 repeat n times


