#include "stdafx.h"
#include "EMSFtp.h"
#include "ESMFunc.h"

CEMSFtp::CEMSFtp() : 
	m_iPort(INTERNET_INVALID_PORT_NUMBER),
	m_pConnection(NULL),
	m_pFileFind(NULL)	
{
	m_session.EnableStatusCallback(TRUE);
	
}

CEMSFtp::~CEMSFtp(void)
{
	Disconnect();
}

//connect to ftp server
BOOL CEMSFtp::Connect(CString strAddress, CString strId, CString strPwd, CString strPort)
{
	if(m_pConnection != NULL)
	{
		m_pConnection->Close();
		delete m_pConnection;
		m_pConnection = NULL;
	}
	m_pConnection = m_session.GetFtpConnection(strAddress,strId,strPwd, _ttoi(strPort));	
	
	if(m_pConnection == NULL)
		return FALSE;
	
	return TRUE;
}

void CEMSFtp::Disconnect()
{
	if( m_pConnection != NULL)
	{
		m_pConnection->Close();	
		delete m_pConnection;
	}	
	m_pConnection = NULL;	
}

BOOL CEMSFtp::FileUpload( CString strLocalFile ,CString strRemoteFile )
{
	return m_pConnection->PutFile(strLocalFile,strRemoteFile,2UL,10);
}

BOOL CEMSFtp::FileDownLoad( CString strRemoteFile,CString strLoaclFile )
{
	return m_pConnection->GetFile(strRemoteFile,strLoaclFile);
}

BOOL CEMSFtp::SetCurrentDir( CString strDir )
{
	return m_pConnection->SetCurrentDirectory(strDir);
}

CString CEMSFtp::GetCurrentDir()
{
	CString strReturn;
	m_pConnection->GetCurrentDirectory(strReturn);
	return strReturn;
}

void CEMSFtp::CreateDir( CString dir )
{
	BOOL bCheck = m_pConnection->CreateDirectory(dir);
}

void CInternetSessionEx::OnStatusCallback( DWORD_PTR dwContext, DWORD dwInternetStatus, LPVOID lpvStatusInformation, DWORD dwStatusInformationLength )
{
	static int nCount = 0;
	switch(dwInternetStatus)
	{
	case INTERNET_STATUS_CONNECTED_TO_SERVER:
		ESMLog(1,_T("INTERNET_STATUS_CONNECTED_TO_SERVER"));		
		break;
	case INTERNET_STATUS_CONNECTION_CLOSED:
		//ESMLog(1,_T("INTERNET_STATUS_CONNECTION_CLOSED"));
		nCount++;
		ESMLog(5, _T("File Transfer End : %d"), nCount);
		break;
	default:
		break;
	}
}
