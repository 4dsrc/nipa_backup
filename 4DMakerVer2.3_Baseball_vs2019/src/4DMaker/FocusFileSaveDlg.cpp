// FocusFileSaveDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "FocusFileSaveDlg.h"
#include "afxdialogex.h"
#include "ESMFunc.h"
#include "ESMIni.h"

// CFocusFileSaveDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFocusFileSaveDlg, CDialogEx)

CFocusFileSaveDlg::CFocusFileSaveDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CFocusFileSaveDlg::IDD, pParent)
{

}

CFocusFileSaveDlg::~CFocusFileSaveDlg()
{
}

void CFocusFileSaveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDT_SELECT_FILE, m_ctrlSelectFile);
	DDX_Control(pDX, IDC_EDT_COMPARE_FOLDER, m_ctrlCompareFolder);
}


BEGIN_MESSAGE_MAP(CFocusFileSaveDlg, CDialogEx)
	ON_BN_CLICKED(IDABORT, &CFocusFileSaveDlg::OnBnClickedAbort)
	ON_BN_CLICKED(IDCLOSE, &CFocusFileSaveDlg::OnBnClickedClose)
	ON_BN_CLICKED(IDC_BTN_SELECT_FILE, &CFocusFileSaveDlg::OnBnClickedBtnSelectFile)
	ON_BN_CLICKED(IDC_BTN_COMPARE_FOLDER, &CFocusFileSaveDlg::OnBnClickedBtnCompareFolder)
	ON_BN_CLICKED(IDC_BTN_COMPARE, &CFocusFileSaveDlg::OnBnClickedBtnCompare)
END_MESSAGE_MAP()


// CFocusFileSaveDlg 메시지 처리기입니다.	

void CFocusFileSaveDlg::OnBnClickedAbort()
{
	EndDialog(IDABORT);
}


void CFocusFileSaveDlg::OnBnClickedClose()
{
	EndDialog(IDCLOSE);
}


void CFocusFileSaveDlg::OnBnClickedBtnSelectFile()
{
	CString strEffectInfoFolder;

	strEffectInfoFolder.Format(_T("%s\\"), ESMGetPath(ESM_PATH_SETUP));

	CString szFilter = _T("Focus file (*.foc)|*.foc|");

	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, FALSE );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;

	if( dlg.DoModal() == IDOK ) //file 선택했을 때
	{
		CString strFileName;
		strFileName = dlg.GetPathName();

		m_ctrlSelectFile.SetWindowText(strFileName);
	}
}


void CFocusFileSaveDlg::OnBnClickedBtnCompareFolder()
{
	ITEMIDLIST*  pildBrowse;
	TCHAR   pszPathname[MAX_PATH];
	//strPath.GetBuffer();
	BROWSEINFO  bInfo;
	memset(&bInfo, 0, sizeof(bInfo));
	bInfo.hwndOwner   = GetSafeHwnd();
	bInfo.pidlRoot   = NULL;
	bInfo.pszDisplayName = pszPathname;
	bInfo.lpszTitle   = _T("Select Dir");
	bInfo.ulFlags   = BIF_RETURNONLYFSDIRS; 
	bInfo.lpfn    = NULL;
	bInfo.lParam  = (LPARAM)(LPCTSTR)"C:\\";
	bInfo.lParam  = (LPARAM)NULL;
	pildBrowse    = SHBrowseForFolder(&bInfo);
	if(pildBrowse)
	{
		SHGetPathFromIDList(pildBrowse, pszPathname);
		m_ctrlCompareFolder.SetWindowText(pszPathname);
	}
}


BOOL CFocusFileSaveDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_ctrlCompareFolder.SetWindowText(_T("C:\\Program Files\\ESMLab\\4DMaker\\Setup\\foc"));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CFocusFileSaveDlg::OnBnClickedBtnCompare()
{
	CString strSelectFile;
	CString strCompareFolder;

	m_ctrlSelectFile.GetWindowText(strSelectFile);
	m_ctrlCompareFolder.GetWindowText(strCompareFolder);

	if (strSelectFile.IsEmpty() || strCompareFolder.IsEmpty())
		return;

	if (!PathFileExists(strSelectFile))
		return;

	if (GetFileAttributes(strCompareFolder) == -1)
		return;


	CESMIni selectIni;
	if(!selectIni.SetIniFilename (strSelectFile))
		return;

	CStringArray arListSelectFile;
	selectIni.GetSectionList(INFO_SECTION_FOCUS,arListSelectFile);
	int nSelectFileCnt = arListSelectFile.GetCount();

	CFileFind file;
	CString strFileName;
	CString strFilePath;

	BOOL b = file.FindFile(strCompareFolder + _T("\\*.foc"));

	while(b)
	{
		b = file.FindNextFile();

		if (file.IsDots())
			continue;

		if(file.IsDirectory())
			continue;

		strFilePath = file.GetFilePath();
		if (strSelectFile == strFilePath)
			continue;

		strFileName = file.GetFileName();
		if (strFileName.IsEmpty())
			continue;

		CESMIni ini;
		if(!ini.SetIniFilename (strFilePath))
			return;

		CStringArray arList;
		ini.GetSectionList(INFO_SECTION_FOCUS,arList);
		int nCnt = arList.GetCount();

		for (int i = 0; i < nSelectFileCnt; i++)
		{
			CString strKey = arListSelectFile.GetAt(i);
			int nSelectValue;
			nSelectValue = selectIni.GetInt(INFO_SECTION_FOCUS, strKey, 0);

			int nValue;
			nValue = ini.GetInt(INFO_SECTION_FOCUS, strKey, -1);
			if (nValue < 0 || nSelectValue != nValue)
			{
				ESMLog(5,_T("The compared focus values are different.   File: [%s],  CamNo:[%s],  Select-Compare[%d - %d]"), strFileName, strKey, nSelectValue,nValue);			
			}
		}
	}

	
	file.Close();
}
