#pragma once


#include "ESMOptBase.h"
#include "resource.h"
#include "afxcmn.h"
#include "afxwin.h"
#include "ESMDefine.h"
#include "ESMIni.h"
#include "ESMNetworkListCtrl.h"
#include "ESMButtonEx.h"
// ESMOptPCInfo 대화 상자입니다.

class ESMOptPCInfo : public CESMOptBase
{
	DECLARE_DYNAMIC(ESMOptPCInfo)

public:
	ESMOptPCInfo();   // 표준 생성자입니다.
	virtual ~ESMOptPCInfo();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_PCINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	
	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	void InitProp(ESMPCInfo& path);
	BOOL SaveProp(ESMPCInfo& path);
	BOOL InitData();

	void GetIPList();

	vector<CString> m_IPList;
	//CListCtrl m_pcList;
	CESMNetworkListCtrl m_pcList;
	bool check_makinguse;
	afx_msg void OnBnClickedOptPcBtnDelete();
	afx_msg void OnBnClickedOptPcBtnReset();
	afx_msg void OnBnClickedOptPcBtnAddall();
	afx_msg void OnBnClickedPcmakinguse();
	CESMButtonEx m_btnDelete;
	CESMButtonEx m_btnAddAll;
	CESMButtonEx m_btnReset;
};
