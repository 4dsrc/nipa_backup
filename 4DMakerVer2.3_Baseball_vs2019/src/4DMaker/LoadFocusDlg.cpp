// LoadFocusDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "LoadFocusDlg.h"
#include "afxdialogex.h"
#include "ESMIndex.h"


// CLoadFocusDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLoadFocusDlg, CDialogEx)

CLoadFocusDlg::CLoadFocusDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLoadFocusDlg::IDD, pParent)
{
	m_pParent = pParent;
	m_bIsVisible = FALSE;
	m_bIsFocusMove = FALSE;
}

CLoadFocusDlg::~CLoadFocusDlg()
{
}

void CLoadFocusDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_MOVE, m_btnMove);
	DDX_Control(pDX, IDC_BTN_COMPARE, m_btnCompare);
	DDX_Control(pDX, IDCANCEL, m_btnClose);
	DDX_Control(pDX, IDC_STATIC_STATUS, m_ctrlStatus);
}


BEGIN_MESSAGE_MAP(CLoadFocusDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_COMPARE, &CLoadFocusDlg::OnBnClickedBtnCompare)
	ON_BN_CLICKED(IDC_BTN_MOVE, &CLoadFocusDlg::OnBnClickedBtnMove)
	ON_WM_SHOWWINDOW()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CLoadFocusDlg 메시지 처리기입니다.



void CLoadFocusDlg::SetLoadFilePath( CString strPath )
{
	m_strLoadFilePath = strPath;
}

void CLoadFocusDlg::SetEndFocusMove()
{
	m_btnMove.EnableWindow(TRUE);
	m_btnCompare.EnableWindow(TRUE);
	m_btnClose.EnableWindow(TRUE);

	m_bIsFocusMove = FALSE;

	m_ctrlStatus.SetWindowTextW(_T("Completed"));
}

void CLoadFocusDlg::OnBnClickedBtnCompare()
{
	if (m_strLoadFilePath.IsEmpty())
		return;

	::SendMessage(m_pParent->GetSafeHwnd(), WM_ESM_LOAD_FOCUS, FALSE, LPARAM(&m_strLoadFilePath));
}


void CLoadFocusDlg::OnBnClickedBtnMove()
{
	if (m_strLoadFilePath.IsEmpty())
		return;

	m_btnMove.EnableWindow(FALSE);
	m_btnCompare.EnableWindow(FALSE);
	m_btnClose.EnableWindow(FALSE);

	m_bIsFocusMove = TRUE;

	::SendMessage(m_pParent->GetSafeHwnd(), WM_ESM_LOAD_FOCUS, TRUE, LPARAM(&m_strLoadFilePath));

	m_ctrlStatus.SetWindowTextW(_T("Focus moving..."));
}


void CLoadFocusDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	m_bIsVisible = TRUE;
	m_bIsFocusMove = FALSE;

	m_btnMove.EnableWindow(TRUE);
	m_btnCompare.EnableWindow(TRUE);
	m_btnClose.EnableWindow(TRUE);

	m_ctrlStatus.SetWindowTextW(_T("-"));
}


void CLoadFocusDlg::OnClose()
{
	if (m_bIsFocusMove)
		return;
	
	m_bIsVisible = FALSE;

	CDialogEx::OnClose();
}


void CLoadFocusDlg::OnDestroy()
{
	if (m_bIsFocusMove)
		return;

	CDialogEx::OnDestroy();

	m_bIsVisible = FALSE;
}

BOOL CLoadFocusDlg::IsVisible()
{
	return m_bIsVisible;
}
