#pragma once

#include <fstream>
#include <vector>
#include <locale>
#include <codecvt>

class CESMUtilMultiplexerMuxer
{
public:
	CESMUtilMultiplexerMuxer(
		int nContainerValue,
		CString strHFilePath, 
		CString strMp4FilePath);
	~CESMUtilMultiplexerMuxer(void);

public:
	BOOL CheckIfFTPDataReceived();
	BOOL OpenFTPList();
	void MuxingAllMJPEGFiles(
		CString strSubL, 
		CString strSubM,
		CString strSubS,
		CString strDate,
		CString strTime,
		CString strObject,
		CString strDescription,
		CString strOwner);
	void WriteSubtitleTest(
		CString strSubL, 
		CString strSubM,
		CString strSubS,
		CString strDate,
		CString strTime,
		CString strObject,
		CString strDescription,
		CString strOwner);


private:
	string m_strHFilePath;
	string m_str4DLiveSavePath;

	vector<string> m_strNumber;
	vector<string> m_strCameraNumber;
	vector<string> m_strVideoValid;
	vector<string> m_strEtc;

	//wgkim 190212
	int m_nContainerValue;

public:
	int UnicodeToUtf8(TCHAR* pUnicode, char** pUtf8);
};

