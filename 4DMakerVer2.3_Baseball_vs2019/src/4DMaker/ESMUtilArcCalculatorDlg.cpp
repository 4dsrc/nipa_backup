// ESMUtilArcCalculator.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMUtilArcCalculatorDlg.h"
#include "afxdialogex.h"


// CESMUtilArcCalculatorDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMUtilArcCalculatorDlg, CDialogEx)

CESMUtilArcCalculatorDlg::CESMUtilArcCalculatorDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMUtilArcCalculatorDlg::IDD, pParent)
{
	m_strDegree = _T("");
	m_strRadius = _T("");
	m_strArcLength = _T("0.0");

	m_dDegree = 0.;
	m_dRadius = 0.;
	m_dArcLength = 0.;

	m_pUtilCaltulator = new CESMUtilCalculator();
}

CESMUtilArcCalculatorDlg::~CESMUtilArcCalculatorDlg()
{

}

void CESMUtilArcCalculatorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_EDIT_INPUT_DEGREE, m_strDegree);
	DDX_Text(pDX, IDC_EDIT_INPUT_RADIUS, m_strRadius);
	DDX_Text(pDX, IDC_EDIT_OUTPUT_ARC_LENGTH, m_strArcLength);
}


BEGIN_MESSAGE_MAP(CESMUtilArcCalculatorDlg, CDialogEx)
	ON_COMMAND(ID_UTILITY_ARCCALCULATOR, &CESMUtilArcCalculatorDlg::OnUtilityArccalculator)
	ON_EN_CHANGE(IDC_EDIT_INPUT_RADIUS, &CESMUtilArcCalculatorDlg::OnChangeEditInputRadius)
	ON_EN_CHANGE(IDC_EDIT_INPUT_DEGREE, &CESMUtilArcCalculatorDlg::OnChangeEditInputDegree)
END_MESSAGE_MAP()


// CESMUtilArcCalculatorDlg 메시지 처리기입니다.


void CESMUtilArcCalculatorDlg::OnUtilityArccalculator()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
}

void CESMUtilArcCalculatorDlg::OnCancel()
{

	CDialog::OnCancel();
}

void CESMUtilArcCalculatorDlg::OnChangeEditInputRadius()
{
	UpdateData(TRUE);
	m_dRadius = _ttof(m_strRadius);

	m_dArcLength = m_pUtilCaltulator->ArcLength(m_dRadius, m_dDegree);

	m_strArcLength.Format(_T("%lf"), m_dArcLength);
	UpdateData(FALSE);
}


void CESMUtilArcCalculatorDlg::OnChangeEditInputDegree()
{
	UpdateData(TRUE);
	m_dDegree = _ttof(m_strDegree);
	
	m_dArcLength = m_pUtilCaltulator->ArcLength(m_dRadius, m_dDegree);

	m_strArcLength.Format(_T("%lf"), m_dArcLength);
	UpdateData(FALSE);
}

