/**	@file processimpl.h
 *	Interface to system dependent process implementation.
 *
 *	Copyright (C) 2009 AJA Video Systems, Inc.  Proprietary and Confidential information.  All rights reserved.
 */

#ifndef AJA_PROCESS_IMPL_H
#define AJA_PROCESS_IMPL_H

#include "ajastuff/system/system.h"
#include "ajastuff/common/common.h"
#include "ajastuff/system/process.h"


class AJAProcessImpl
{
public:

	AJAProcessImpl();
	virtual ~AJAProcessImpl();

static		uint64_t	GetPid();    
static      bool        IsValid(uint64_t pid);
	
static		bool		Activate(const char* pWindow);
static		bool		Activate(uint64_t handle);	
};


#endif
