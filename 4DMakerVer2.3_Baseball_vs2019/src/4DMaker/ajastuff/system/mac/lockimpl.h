/**	@file lockimpl.h
 *	Interface to system dependent lock implementation.
 *
 *	Copyright (C) 2009 AJA Video Systems, Inc.  Proprietary and Confidential information.  All rights reserved.
 */

#ifndef AJA_LOCK_IMPL_H
#define AJA_LOCK_IMPL_H

#include <pthread.h>
#include "ajastuff/system/system.h"
#include "ajastuff/common/common.h"
#include "ajastuff/system/lock.h"

class AJALockImpl
{
public:
					
					AJALockImpl(const char* pName);
	virtual			~AJALockImpl();

	AJAStatus		Lock(uint32_t uTimeout = 0xffffffff);
	AJAStatus		Unlock();
	
private:
	const char*		mName;
	pthread_t		mOwner;
	int				mRefCount;
	pthread_mutex_t mMutex;		// For protecting the member variables
};


#endif
