/**	@file udp_socket.h
 *	Implement the UDP transport
 *
 *	Copyright (C) 2011 AJA Video Systems, Inc.
 *	Proprietary and Confidential information.  All rights reserved.
 */

#ifndef AJA_UDP_SOCKET_H
#define AJA_UDP_SOCKET_H

/////////////////////////////
// Includes
/////////////////////////////
#include "ajastuff/system/ip_socket.h"


/////////////////////////////
// Declarations
/////////////////////////////
class AJA_EXPORT AJAUDPSocket : public AJAIPSocket
{
	public:
		AJAUDPSocket(void);
		virtual ~AJAUDPSocket(void);

		AJAStatus Open(const std::string& ipAddress, uint16_t port);

		uint32_t Poll(
					uint8_t*            pData,
					uint32_t            dataLength,
					struct sockaddr_in& client,
					int                 timeout);
		uint32_t Read(uint8_t* pData, uint32_t dataLength, struct sockaddr_in& client);
		uint32_t Write(
					const uint8_t*      pData,
					uint32_t            dataLength,
					struct sockaddr_in& targetAddress);


	private:
};


#endif // AJA_UDP_SOCKET_H

