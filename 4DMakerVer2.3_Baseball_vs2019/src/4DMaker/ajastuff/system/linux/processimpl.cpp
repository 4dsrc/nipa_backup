//---------------------------------------------------------------------------------------------------------------------
//	processimpl.cpp
//
//	Copyright (C) 2011 AJA Video Systems, Inc.  Proprietary and Confidential information.  All rights reserved.
//---------------------------------------------------------------------------------------------------------------------

#include <sys/types.h>
#include <signal.h>
#include <unistd.h>

#include "ajastuff/system/linux/processimpl.h"
#include "ajastuff/common/timer.h"


// class AJAProcessImpl

AJAProcessImpl::AJAProcessImpl()
{
}


AJAProcessImpl::~AJAProcessImpl()
{
}

uint64_t
AJAProcessImpl::GetPid()
{
	return getpid();
}

bool
AJAProcessImpl::IsValid(uint64_t pid)
{
	if( kill(pid,0) == 0 )
		return true;
	else
		return false;
}

bool
AJAProcessImpl::Activate(uint64_t handle)
{
	// Dummy placeholder
	return false;
}

bool
AJAProcessImpl::Activate(const char* pWindow)
{
	// Dummy placeholder
	return false;
}
