/**	@file lockimpl.h
 *	Interface to system dependent lock implementation.
 *
 *	Copyright (C) 2009 AJA Video Systems, Inc.  Proprietary and Confidential information.  All rights reserved.
 */

#ifndef AJA_LOCK_IMPL_H
#define AJA_LOCK_IMPL_H

#include "ajastuff/system/system.h"
#include "ajastuff/common/common.h"
#include "ajastuff/system/lock.h"


class AJALockImpl
{
public:

	AJALockImpl(const char* pName);
	virtual ~AJALockImpl();

	AJAStatus Lock(uint32_t uTimeout = 0xffffffff);
	AJAStatus Unlock();

	HANDLE mMutex;
};


#endif
