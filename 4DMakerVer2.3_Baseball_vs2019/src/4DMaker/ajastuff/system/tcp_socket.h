/**	@file tcp_socket.h
 *	Implement the TCP transport
 *
 *	Copyright (C) 2011 AJA Video Systems, Inc.
 *	Proprietary and Confidential information.  All rights reserved.
 */

#ifndef AJA_TCP_SOCKET_H
#define AJA_TCP_SOCKET_H

/////////////////////////////
// Includes
/////////////////////////////
#include "ajastuff/system/ip_socket.h"


/////////////////////////////
// Declarations
/////////////////////////////
class AJA_EXPORT AJATCPSocket : public AJAIPSocket
{
	public:
		AJATCPSocket(void);
		virtual ~AJATCPSocket(void);

		AJAStatus Open(const std::string& ipAddress, uint16_t port);

		AJAStatus Connect(const std::string& ipAddress, uint16_t port);
		AJAStatus Listen(void);
		int       Accept(void);

		uint32_t Read(uint8_t* pData, uint32_t dataLength);
		uint32_t Write(const uint8_t* pData, uint32_t dataLength);

	private:
		// None
};


#endif // AJA_TCP_SOCKET_H
///////////////////////// End of tcp_socket.h ///////////////////////

