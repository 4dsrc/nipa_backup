/**	@file timer.h
 *	Interface to system independent stopwatch class.
 *
 *	Copyright (C) 2009 AJA Video Systems, Inc.  Proprietary and Confidential information.  All rights reserved.
 */

#ifndef AJA_TIMER_H
#define AJA_TIMER_H

#include "ajastuff/common/public.h"

/**
 *	Class to support timing of events
 *	@ingroup AJAGroupSystem
 */
class AJA_EXPORT AJATimer
{
public:

	AJATimer();
	virtual ~AJATimer();

	/**
	 *	Start the timer.
	 */
	void Start();

	/**
	 *	Stop the timer.
	 */
	void Stop();

	/**
	 *	Reset the timer.
	 */
	void Reset();

	/**
	 *	Get the elapsed time.
	 *
	 *	If the timer is running, return the elapsed time since Start() was called.  If Stop() 
	 *	has been called, return the time between Start() and Stop().
	 *
	 *	@return		The elapsed time in milliseconds.
	 */
	uint32_t ElapsedTime();

	/**
	 *	Check for timeout.
	 *
	 *	Timeout checks the ElapsedTime() and returns true if it is greater than interval.
	 *
	 *	@param	interval	Timeout interval in milliseconds.
	 *	@return				true if elapsed time greater than interval.
	 */
	bool Timeout(uint32_t interval);

	/**
	 *	Is the timer running.
	 *
	 *	@return				true if timer is running.
	 */
	bool IsRunning(void);

private:

	uint64_t	mStartTime;
	uint64_t	mStopTime;
	bool		mRun;
};


#endif

