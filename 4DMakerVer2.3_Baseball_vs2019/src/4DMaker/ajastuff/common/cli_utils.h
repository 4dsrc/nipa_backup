/////////////////////////////////////////////////////////////////////
//                            cli_utils.h
//
// Description: Helper functions for any given CLI
//
// Copyright (C) 2011 AJA Video Systems, Inc.
// Proprietary and Confidential information.  All rights reserved.
//
/////////////////////////////////////////////////////////////////////
#ifndef __CLI_UTILS_H
#define __CLI_UTILS_H

/////////////////////////////
// Includes
/////////////////////////////
#include "ajastuff/common/types.h"
#include <iostream>
#include <string>


/////////////////////////////
// Defines
/////////////////////////////
#define INVALID_CHOICE(c) \
	cout << "The choice '" << char(c) << "' is not valid" << endl


/////////////////////////////
// Declarations
/////////////////////////////
bool GetNumber(const std::string& prompt, uint32_t& value);
bool GetString(const std::string& prompt, std::string& value);
bool GetCharAsInt(const std::string& prompt, int& value);


#endif // __CLI_UTILS_H
///////////////////////// End of cli_utils.h ////////////////////////

