/////////////////////////////////////////////////////////////////////
//                           performance.h
//
// Description: This module will monitor the operational performance,
// the timing, and the statistics of an arbitrary module.
//
// Copyright (C) 2011 AJA Video Systems, Inc.
// Proprietary and Confidential information.  All rights reserved.
//
/////////////////////////////////////////////////////////////////////
#ifndef __PERFORMANCE_H
#define __PERFORMANCE_H

/////////////////////////////
// Includes
/////////////////////////////
#include "ajastuff/common/timer.h"
#include <string>

using std::string;


/////////////////////////////
// Typedef
/////////////////////////////
typedef struct
{
	string   name;
	uint32_t totalTime;
	uint32_t entries;
	uint32_t minTime;
	uint32_t maxTime;
} TimerStatisticsType;


/////////////////////////////
// Declarations
/////////////////////////////
class AJAPerformance
{
	public:
		AJAPerformance(void);
		~AJAPerformance(void);

		void Initialize(const string& name);
		void Start(void);
		void Stop(void);

		const TimerStatisticsType& GetTimerStatistics(void);


	private:
		AJATimer            mTimer;
		TimerStatisticsType mTimerStatistics;
};


#endif // __PERFORMANCE_H
//////////////////////// End of performance.h ///////////////////////

