/**	@file public.h
 *	Public include file for all ajastuff api header files.
 *
 *	Copyright (C) 2009 AJA Video Systems, Inc.  Proprietary and Confidential information.  All rights reserved.
 */

#ifndef AJA_PUBLIC_H
#define AJA_PUBLIC_H

#include <string>
#include <list>
#include <vector>
#include <map>

#include "types.h"
#include "export.h"

/**
 *	@mainpage 
 *
 *	The AJA ajastuff library defines the programming interface for many AJA utilities.
 */

#endif

