#pragma once

#include "afxwin.h"

// CESMPWEditorDlgGH5WBAD 대화 상자입니다.

class CESMPWEditorDlgGH5WBAD : public CDialogEx
{
	DECLARE_DYNAMIC(CESMPWEditorDlgGH5WBAD)

public:
	CESMPWEditorDlgGH5WBAD(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMPWEditorDlgGH5WBAD();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ESMPWEDITORDLG_GH5_WB_AD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

private:	
	CStatic m_ctrlPic;
	bool m_bMouseLeftClickFlag;
	int m_nWidth;
	int m_nHeight;

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
};
