#include "ESMIndexStructure.h"

#pragma once


// CESMOptSyncDlg 대화 상자입니다.

class CESMOptSyncDlg : public CDialog
{
	DECLARE_DYNAMIC(CESMOptSyncDlg)

public:
	CESMOptSyncDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMOptSyncDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_SYNC_SETTING };

public:
	void InitProp(ESM4DMaker* Opt);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	ESM4DMaker* m_pOpt;
	CEdit m_ctrlPCSyncTime;
	CEdit m_ctrlResyncRecWaitTime;

	afx_msg void OnBnClickedBtnOk();
	afx_msg void OnBnClickedBtnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnDefaultSet();
};
