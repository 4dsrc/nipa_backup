//-- 2014-08-31 wgkim@esmlab.com
//Implementation AutoAdjust Using LKOpticalFlow

#pragma once

#include "ESMTemplateMgr.h"

class CESMTemplateAsAuto
{
public:
	CESMTemplateAsAuto(void);
	~CESMTemplateAsAuto(void);
	
public:	
	void InputAdjustFeaturesFromList(CListCtrl* pList, int nSelectedItem);
	void ApplyCalculatedAdjustFeatureToList(CListCtrl* pList, int nSelectedItem);
	void CalcAutoAdjustUsingOpticalFlow(Mat& srcImage);	

private:
	vector<Point2f> currentAdjustFeatures;
	vector<Point2f> calculatedAdjustFeatures;
	vector<uchar>	statusAdjustFeatures;	
	vector<float>	_err;	

private:
	Mat previousImageForAdjust;
	Mat nextImageForAdjust;	

	int _imageScale;

};

