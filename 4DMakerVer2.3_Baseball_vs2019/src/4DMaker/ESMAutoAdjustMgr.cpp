#include "stdafx.h"
#include "ESMAutoAdjustMgr.h"
#include "ESMCtrl.h"
#include "ESMFileOperation.h"

CESMAutoAdjustMgr::CESMAutoAdjustMgr(void)
{
	m_pSquareCalibration	= NULL;
	m_pKZoneDataInfo		= NULL;
	m_bValidKZone			= FALSE;
	m_bToggleUsingKZone		= FALSE;
}

CESMAutoAdjustMgr::~CESMAutoAdjustMgr(void)
{
}

void CESMAutoAdjustMgr::SetSquareCalibration(CESMAutoAdjustSquareCalibration* pSquareCalibration)
{
	m_pSquareCalibration= pSquareCalibration;
}

void CESMAutoAdjustMgr::AddDscInfo(CString strDscId, CString strDscIp,BOOL bReverse /*= FALSE*/)
{
	DscAdjustInfo* pDscAdjustData;
	pDscAdjustData = new DscAdjustInfo;
	pDscAdjustData->strDscName = strDscId;
	pDscAdjustData->strDscIp = strDscIp;
	pDscAdjustData->bAdjust = FALSE;
	pDscAdjustData->bRotate = FALSE;
	pDscAdjustData->bMove = FALSE;
	pDscAdjustData->bImgCut = FALSE;
	pDscAdjustData->bReverse = bReverse;
	m_ArrDscInfo.push_back(pDscAdjustData);
}

void CESMAutoAdjustMgr::GetDscInfo(vector<DscAdjustInfo*>** pArrDscInfo)
{
	*pArrDscInfo = &m_ArrDscInfo;
}

BOOL CESMAutoAdjustMgr::IsEmptyAdjustInfo()
{
	BOOL IsEmpty = TRUE;
	for(int i = 0; i < m_ArrDscInfo.size(); i++)
	{
		if(!(m_ArrDscInfo[i]->dAdjustX	== 0 &&
			m_ArrDscInfo[i]->dAdjustY	== 0 &&
			m_ArrDscInfo[i]->dAngle		== 0 &&
			m_ArrDscInfo[i]->dRotateX	== 0 &&
			m_ArrDscInfo[i]->dRotateY	== 0 &&
			m_ArrDscInfo[i]->dScale		== 0 &&
			m_ArrDscInfo[i]->dDistance	== 0 ))
		{
			IsEmpty = FALSE;
		}
	}

	return IsEmpty;
}

int CESMAutoAdjustMgr::GetDscCount()
{
	return m_ArrDscInfo.size();
}

void CESMAutoAdjustMgr::DscClear()
{
	DscAdjustInfo* pDscAdjustData;
	for( int i =0 ;i < m_ArrDscInfo.size(); i++)
	{
		pDscAdjustData = m_ArrDscInfo.at(i);
		if( pDscAdjustData->pBmpBits != NULL)
		{
			delete[] pDscAdjustData->pBmpBits;
			pDscAdjustData->pBmpBits = NULL;
		}
		if( pDscAdjustData->pGrayBmpBits != NULL)
		{
			delete[] pDscAdjustData->pGrayBmpBits;
			pDscAdjustData->pGrayBmpBits = NULL;
		}

		delete pDscAdjustData;
		pDscAdjustData = NULL;
	}
	m_ArrDscInfo.clear();
}

DscAdjustInfo* CESMAutoAdjustMgr::GetDscAt(int nIndex)
{
	if(m_ArrDscInfo.size() > nIndex)
	{
		return m_ArrDscInfo.at(nIndex);
	}
	return NULL;
}

int CESMAutoAdjustMgr::GetDscIndexFrom(CString strDscId)
{
	int DscID = -1;
	for(int i = 0; i < GetDscCount(); i++)
	{	
		if(!strDscId.Compare(m_ArrDscInfo[i]->strDscName))
		{
			DscID = i;
			break;
		}
	}
	return DscID;
}

// 2014-01-07 kcd 
// Distance 임시 함수
double CESMAutoAdjustMgr::GetDistanceData(CString strDSCId)
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\DSCDistance.csv"), ESMGetPath(ESM_PATH_SETUP));
	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		return FALSE;
	}

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());
	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;
	CString strTpValue1, strTpValue2;
	double nDistance = 0;
	while (iPos  < iLength )
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;
		AfxExtractSubString(strTpValue1, sLine, 0, ',');
		AfxExtractSubString(strTpValue2, sLine, 1, ',');
		if( strTpValue1 == strDSCId)
		{
			nDistance = _ttof(strTpValue2);
			break;
		}
	}
	return nDistance;
}

int CESMAutoAdjustMgr::GetZoomData(CString strDSCId)
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\DSCZoom.csv"), ESMGetPath(ESM_PATH_SETUP));
	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		return FALSE;
	}

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());
	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;
	CString strTpValue1, strTpValue2;
	double nDistance = 0;
	while (iPos  < iLength )
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;
		AfxExtractSubString(strTpValue1, sLine, 0, ',');
		AfxExtractSubString(strTpValue2, sLine, 1, ',');
		if( strTpValue1 == strDSCId)
		{
			nDistance = _ttoi(strTpValue2);
			break;
		}
	}
	return nDistance;
}

BOOL CESMAutoAdjustMgr::CalcAdjustData(DscAdjustInfo* pAdjustInfo, int nTargetLenth, int nZoom)
{
	//회전
	//pAdjustInfo->dAngle = atan2((double)(pAdjustInfo->LowPos.y - pAdjustInfo->HighPos.y), 
	//	(double)(pAdjustInfo->LowPos.x - pAdjustInfo->HighPos.x)) * (180/3.141592);

	//if( pAdjustInfo->nHeight != 0 || pAdjustInfo->nWidht != 0 )
	{
		CString strDscId = pAdjustInfo->strDscName;
		CT2CA pszConvertedAnsiDscId(strDscId);
		string strAnsiDscId(pszConvertedAnsiDscId);
		pAdjustInfo->dAngle = -m_pSquareCalibration->GetAngleValueAt(strAnsiDscId);
		if(pAdjustInfo->dAngle >= -180)
			pAdjustInfo->dAngle += -90;
		else
			pAdjustInfo->dAngle += 270;

		//pAdjustInfo->dScale = m_pSquareCalibration->GetNormValueAt(strAnsiDscId)
		//	/ m_pSquareCalibration->GetDistanceValueAt(strAnsiDscId);
		pAdjustInfo->dScale = m_pSquareCalibration->GetDistanceValueAt(strAnsiDscId)
			/ m_pSquareCalibration->GetNormValueAt(strAnsiDscId);

		//CenterPoint
		Point2f ptCenterPoint = m_pSquareCalibration->GetCenterPointAt(strAnsiDscId);

		pAdjustInfo->dRotateX = pAdjustInfo->dAdjustX = ptCenterPoint.x;
		pAdjustInfo->dRotateY = pAdjustInfo->dAdjustY = ptCenterPoint.y;

		pAdjustInfo->dDistance = m_pSquareCalibration->GetDistanceValueAt(strAnsiDscId);;
		return TRUE;
	}

	return FALSE;
}

BOOL CESMAutoAdjustMgr::SearchDetectPoint(IplImage* pNewGray, CSize MinSize, CSize MaxSize, vector<CRect>* vecDetectAreas, COLORREF clrDetectColor)
{
	CPoint pointCenter = CPoint(pNewGray->width / 2, pNewGray->height / 2);
	vector<CRect> vecCenters;
	BOOL bStart = FALSE, bEnd = FALSE;
	int nStartPos = 0, nEndPos = 0;
	int nStartCenterArea = (pNewGray->height / 2) - ((pNewGray->height / 5) / 2);
	int nEndCenterArea = (pNewGray->height / 2) + ((pNewGray->height / 5) / 2);
	int nPosYGap = (MaxSize.cy / 2) + 10;

	int nFindColor;
	if(clrDetectColor == COLOR_WHITE)
		nFindColor = 0;
	else
		nFindColor = 255;

	for(int nFindY = 0; nFindY < pNewGray->height; nFindY++)
	{
		if((unsigned char)pNewGray->imageData[(nFindY * pNewGray->widthStep) + pointCenter.x] == nFindColor)
		{
			if(!bStart)	bStart = TRUE;
			else if(bEnd)
			{
				nEndPos = nFindY - 1;
				bEnd = FALSE;
				bStart = FALSE;
				if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
				{
					CRect recDetect(pointCenter.x, nStartPos, pointCenter.x, nEndPos);
					vecCenters.push_back(recDetect);
				}
			}
		}
		else
		{
			if(bStart && !bEnd)
			{
				nStartPos = nFindY;
				bEnd = TRUE;
			}
		}
	}
	if(vecCenters.size() < 3)
	{
		vecCenters.clear();
		bStart = FALSE;
		bEnd = FALSE;
		nStartPos = 0;
		nEndPos = 0;
		for(int nFindX = pointCenter.x - 1; nFindX > (pointCenter.x - MaxSize.cx); nFindX--)
		{
			vecCenters.clear();
			for(int nFindY = 0; nFindY < pNewGray->height; nFindY++)
			{
				if((unsigned char)pNewGray->imageData[(nFindY * pNewGray->widthStep) + nFindX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nFindY - 1;
						bEnd = FALSE;
						bStart = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							CRect recDetect(nFindX, nStartPos, nFindX, nEndPos);
							vecCenters.push_back(recDetect);
						}
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nFindY;
						bEnd = TRUE;
					}
				}
			}
			if(vecCenters.size() >= 3)
				break;
		}
	}
	if(vecCenters.size() < 3)
	{
		vecCenters.clear();
		bStart = FALSE;
		bEnd = FALSE;
		nStartPos = 0;
		nEndPos = 0;
		for(int nFindX = pointCenter.x + 1; nFindX < (pointCenter.x + MaxSize.cx); nFindX++)
		{
			vecCenters.clear();
			for(int nFindY = 0; nFindY < pNewGray->height; nFindY++)
			{
				if((unsigned char)pNewGray->imageData[(nFindY * pNewGray->widthStep) + nFindX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nFindY - 1;
						bEnd = FALSE;
						bStart = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							CRect recDetect(nFindX, nStartPos, nFindX, nEndPos);
							vecCenters.push_back(recDetect);
						}
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nFindY;
						bEnd = TRUE;
					}
				}
			}
			if(vecCenters.size() >= 3)
				break;
		}
	}
	if(vecCenters.size() < 3)
		return FALSE;

	//Center에 가장 가까운 영역을 찾는다. 
	CRect recCenter = vecCenters[0];
	int nCenterMargin = pointCenter.y - recCenter.CenterPoint().y;
	if(nCenterMargin < 0) nCenterMargin *= -1;
	for(int nIdx = 1; nIdx < vecCenters.size(); nIdx++)
	{
		int nMargin = pointCenter.y - vecCenters[nIdx].CenterPoint().y;
		if(nMargin < 0) nMargin *= -1;
		if(nMargin < nCenterMargin)
		{
			nCenterMargin = nMargin;
			recCenter =  vecCenters[nIdx];
		}
	}

	if(recCenter.CenterPoint().y < nStartCenterArea || recCenter.CenterPoint().y > nEndCenterArea)
		return FALSE;

	//First와 Third 영역을 찾는다. 
	CRect recFirst = recCenter;
	CRect recThird = recCenter;
	int nFirstMargin = 0, nThirdMargin = 0;
	for(int nIdx = 0; nIdx < vecCenters.size(); nIdx++)
	{
		if(recCenter == vecCenters[nIdx])
			continue;
		int nMargin = recCenter.CenterPoint().y - vecCenters[nIdx].CenterPoint().y;
		if(nMargin < 0)
		{
			nMargin *= -1;
			if(nThirdMargin == 0)
			{
				nThirdMargin = nMargin;
				recThird = vecCenters[nIdx];
			}
			else if(nMargin < nThirdMargin)
			{
				nThirdMargin = nMargin;
				recThird = vecCenters[nIdx];
			}
		}	
		else
		{
			if(nFirstMargin == 0)
			{
				nFirstMargin = nMargin;
				recFirst = vecCenters[nIdx];
			}
			else if(nMargin < nFirstMargin)
			{
				nFirstMargin = nMargin;
				recFirst = vecCenters[nIdx];
			}
		}
	}

	vecCenters.clear();
	vecCenters.push_back(recFirst);
	vecCenters.push_back(recCenter);
	vecCenters.push_back(recThird);


	for(int nAreaIdx = 0; nAreaIdx < vecCenters.size(); nAreaIdx++)
	{
		//Left 시작 지점 찾는다.
		int nLeft = vecCenters[nAreaIdx].left;
		int nTop = vecCenters[nAreaIdx].top;
		int nRight = vecCenters[nAreaIdx].right;
		int nBottom = vecCenters[nAreaIdx].bottom;
		BOOL bDetectStop = FALSE;
		CRect recDetectArea = vecCenters[nAreaIdx];
		vector<int> vecHeightAvg;
		vector<CRect> vecAreaList;
		vecAreaList.push_back(vecCenters[nAreaIdx]);
		vecHeightAvg.push_back(nBottom - nTop);
		for(int nPosX = vecCenters[nAreaIdx].CenterPoint().x - 1; nPosX > vecCenters[nAreaIdx].CenterPoint().x - MaxSize.cx; nPosX--)
		{
			if(nPosX > pNewGray->width) break;
			int nPosY = vecCenters[nAreaIdx].CenterPoint().y - nPosYGap;
			if(nPosY < 0) nPosY = 0;
			if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] != nFindColor)
				break;

			bStart = FALSE;
			bEnd = FALSE;
			nStartPos = 0;
			nEndPos = 0;

			int nHeightAvg = 0, nHeightSum = 0, nAvgIdxCnt = vecHeightAvg.size();
			for(int nAvgIdx = 0; nAvgIdx < nAvgIdxCnt; nAvgIdx++)
				nHeightSum += vecHeightAvg[nAvgIdx];
			nHeightAvg = nHeightSum / nAvgIdxCnt;

			for(; nPosY < vecCenters[nAreaIdx].CenterPoint().y + nPosYGap; nPosY++)
			{
				if(nPosY > pNewGray->height) break;
				if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nPosY - 1;
						bStart = FALSE;
						bEnd = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							int nHeight = nEndPos - nStartPos;
							if(nHeight >= nHeightAvg - 10 &&  nHeight <= nHeightAvg + 10)
							{
								nLeft = nPosX;
								if(nStartPos > nTop)	nTop = nStartPos;
								if(nEndPos < nBottom)	nBottom = nEndPos;
								recDetectArea.SetRect(nLeft, nTop, nRight, nBottom);
								vecHeightAvg.push_back(nHeight);
								vecAreaList.push_back(CRect(nPosX, nStartPos, nPosX, nEndPos));
							}
						}
						else
							bDetectStop = TRUE;
						break;
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nPosY;
						bEnd = TRUE;
					}
				}
			}
			if(bDetectStop)
				break;
		}
		//Right 마지막 지점 찾는다.
		bDetectStop = FALSE;
		for(int nPosX = vecCenters[nAreaIdx].CenterPoint().x + 1; nPosX < vecCenters[nAreaIdx].CenterPoint().x + MaxSize.cx; nPosX++)
		{
			if(nPosX > pNewGray->width) break;
			int nPosY = vecCenters[nAreaIdx].CenterPoint().y - nPosYGap;
			if(nPosY < 0) nPosY = 0;
			if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] != nFindColor)
				break;
			bStart = FALSE;
			bEnd = FALSE;
			nStartPos = 0;
			nEndPos = 0;

			int nHeightAvg = 0, nHeightSum = 0, nAvgIdxCnt = vecHeightAvg.size();
			for(int nAvgIdx = 0; nAvgIdx < nAvgIdxCnt; nAvgIdx++)
				nHeightSum += vecHeightAvg[nAvgIdx];
			nHeightAvg = nHeightSum / nAvgIdxCnt;

			for(; nPosY < vecCenters[nAreaIdx].CenterPoint().y + nPosYGap; nPosY++)
			{
				if(nPosY > pNewGray->height) break;
				if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nPosY - 1;
						bStart = FALSE;
						bEnd = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							int nHeight = nEndPos - nStartPos;
							if(nHeight >= nHeightAvg - 10 &&  nHeight <= nHeightAvg + 10)
							{
								nRight = nPosX;
								if(nStartPos > nTop)	nTop = nStartPos;
								if(nEndPos < nBottom)	nBottom = nEndPos;
								recDetectArea.SetRect(nLeft, nTop, nRight, nBottom);
								vecHeightAvg.push_back(nHeight);
								vecAreaList.push_back(CRect(nPosX, nStartPos, nPosX, nEndPos));
							}
						}
						else
							bDetectStop = TRUE;
						break;
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nPosY;
						bEnd = TRUE;
					}
				}
			}
			if(bDetectStop)
				break;
		}
		if(recDetectArea.Width() < 5)
		{
			recDetectArea.SetRect(0, 0, 0, 0);
		}
		else
		{
			int nCenterX = recDetectArea.CenterPoint().x;
			for(int nXIdx = 0; nXIdx < vecAreaList.size(); nXIdx++)
			{
				if(vecAreaList[nXIdx].left == nCenterX)
				{
					nLeft = recDetectArea.left;
					nTop = vecAreaList[nXIdx].top;
					nRight = recDetectArea.right;
					nBottom = vecAreaList[nXIdx].bottom;
					recDetectArea.SetRect(nLeft, nTop, nRight, nBottom);
					break;
				}
			}
		}
		vecDetectAreas->push_back(recDetectArea);
	}
	return TRUE;
}

BOOL CESMAutoAdjustMgr::SearchPoint(IplImage*	pNewGray, vector<CPoint>* arrBeginPoint, vector<CPoint>* arrPointRange, vector<CPoint>* pArrPoint)
{
	CPoint TpBeginP;
	CPoint TpPoints;
	if( pNewGray->width < arrBeginPoint->at(2).x + arrPointRange->at(2).x)
		return FALSE;
	if( pNewGray->height < arrBeginPoint->at(2).y + arrPointRange->at(2).y)
		return FALSE;

	for( int i = 0; i< arrBeginPoint->size(); i++)
	{
		CPoint CenterPoint;
		CPoint tpPoint;
		vector<CPoint> arrPoint;
		int x1 = 0;
		int nImageWidth	= pNewGray->width;
		int nImageHeight = pNewGray->height;
		TpPoints = arrPointRange->at(i);

		for(int y1 = arrBeginPoint->at(i).y ; y1 < arrBeginPoint->at(i).y + TpPoints.y ; y1++)
		{
			for(x1 = arrBeginPoint->at(i).x ; x1 < arrBeginPoint->at(i).x + TpPoints.x ; x1++)
			{
				if((unsigned char)pNewGray->imageData[y1*pNewGray->widthStep + x1] < 10)// 흑을 만난다면 Continue;
				{
					tpPoint.x = x1;
					tpPoint.y = y1;
					arrPoint.push_back(tpPoint);
					break;
				}
			}
			if( x1 == arrBeginPoint->at(i).x + TpPoints.x)
				continue;

			//Detect Right Line
			for(x1 = arrBeginPoint->at(i).x + TpPoints.x ; x1 > arrBeginPoint->at(i).x ; x1--)
			{
				if((unsigned char)pNewGray->imageData[y1*pNewGray->widthStep + x1] < 10)// 흑색을 만난다면 Continue;
				{
					tpPoint.x = x1;
					tpPoint.y = y1;
					arrPoint.push_back(tpPoint);
					break;
				}
			}
		}
		if( arrPoint.size() < 1)
			return FALSE;
		int dMinX = arrPoint.at(0).x, dMinY = arrPoint.at(0).y, dMaxX = arrPoint.at(0).x, dMaxY = arrPoint.at(0).y;
		for(int nIndex =0 ;nIndex < arrPoint.size() ; nIndex++)
		{
			if( arrPoint.at(nIndex).x < dMinX)
				dMinX = arrPoint.at(nIndex).x;
			else if( arrPoint.at(nIndex).y < dMinY)
				dMinY = arrPoint.at(nIndex).y;
			else if( arrPoint.at(nIndex).x > dMaxX)
				dMaxX = arrPoint.at(nIndex).x;
			else if( arrPoint.at(nIndex).x > dMaxY)
				dMaxY = arrPoint.at(nIndex).y;
		}
		TpBeginP.x = (dMinX + dMaxX) / 2;
		TpBeginP.y = (dMinY + dMaxY) / 2;
		pArrPoint->push_back(TpBeginP);

	}
	return TRUE;
}


void CESMAutoAdjustMgr::GetResolution(int &nWidth, int &nHeight)
{
	DscAdjustInfo* pAdjustInfo = NULL;
	CESMFileOperation fo;
	for(int i =0; i< GetDscCount(); i++)
	{
		pAdjustInfo = GetDscAt(i);

		FFmpegManager FFmpegMgr(FALSE);
		CString strFolder, strDscId, strDscIp;
		strDscId = pAdjustInfo->strDscName;
		strDscIp = pAdjustInfo->strDscIp;
		int nFIdx = ESMGetFrameIndex(0);
		strFolder = ESMGetMoviePath(strDscId, nFIdx);


		if( strFolder == _T(""))
			continue;

		if(!fo.IsFileExist(strFolder))
			continue;

		int nRunningTime = 0, nFrameCount = 0, nChannel = 3, nFrameRate = 0;
		FFmpegMgr.GetMovieInfo(strFolder, &nRunningTime, &nFrameCount, &nWidth, &nHeight, &nChannel, &nFrameRate);
		if( nWidth != 0)
			break;
	}
}

void CESMAutoAdjustMgr::AddAdjInfo(stAdjustInfo adjustInfo)
{
	m_arrAdjInfo.push_back(adjustInfo);
	m_arrModifiedAdjInfo.push_back(adjustInfo);
}

stAdjustInfo CESMAutoAdjustMgr::GetAdjInfoAt(int index)
{
	return m_arrAdjInfo.at(index);
}

void CESMAutoAdjustMgr::ClearAdjInfo()
{
	m_arrAdjInfo.clear();
}

//Mat CESMAutoAdjustMgr::ShowImageFrom(int nSelectedItem, int marginX, int marginY)
//{	
//	Mat src;
//	DscAdjustInfo *pAdjustInfo = GetDscAt(nSelectedItem);
//	if( pAdjustInfo != NULL)
//	{
//		if(pAdjustInfo->nHeight == 0 || pAdjustInfo->nWidht == 0)
//		{
//			return Mat();
//		}
//		src.create(pAdjustInfo->nHeight, pAdjustInfo->nWidht, CV_8UC3);
//		memcpy(src.data, pAdjustInfo->pBmpBits, pAdjustInfo->nHeight * pAdjustInfo->nWidht * 3);
//	}
//	return src;
//}

int CESMAutoAdjustMgr::Round(double dData)
{
	int nResult = 0;
	if( dData == 0)
		nResult = 0;
	else if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);

	return nResult;
}
void CESMAutoAdjustMgr::GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle)
{
	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;

	Mat rot_mat(cv::Size(2, 3), CV_64FC1);
	cv::Point rot_center( nCenterX, nCenterY);

	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust = -1 * (dAngle + 90);

	rot_mat = getRotationMatrix2D(rot_center, dbAngleAdjust, dScale);

	cuda::GpuMat d_rotate;	
	cuda::warpAffine(*gMat, d_rotate, rot_mat, cv::Size(gMat->cols, gMat->rows), cv::INTER_CUBIC);
	d_rotate.copyTo(*gMat);
}
void CESMAutoAdjustMgr::GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;

	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void CESMAutoAdjustMgr::GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void CESMAutoAdjustMgr::CpuMakeMargin(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;

	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);

}
void CESMAutoAdjustMgr::CpuMoveImage(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void CESMAutoAdjustMgr::CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle)
{
	Mat Iimage2;// = cvCreateImage(cvGetSize(Iimage), IPL_DEPTH_8U, 3);

	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;

	CvMat *rot_mat = cvCreateMat( 2, 3, CV_64FC1);
	CvPoint2D32f rot_center = cvPoint2D32f( nCenterX, nCenterY);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust = -1 * (dAngle + 90);

	cv2DRotationMatrix( rot_center, dbAngleAdjust, dScale, rot_mat);
	cvWarpAffine(&IplImage(Iimage), &IplImage(Iimage), rot_mat,cv::INTER_LINEAR+cv::WARP_FILL_OUTLIERS);	//d_rotate.copyTo(*gMat);
	cvReleaseMat(&rot_mat);
}

void CESMAutoAdjustMgr::affinetransform(Mat img,cv::Point a,cv::Point b, cv::Point c, Point2f srcTri[3],Mat dst)
{
	Mat warp_mat(2,3,CV_64FC1);
	Point2f dstTri[3];
	dstTri[0] = a;	dstTri[1] = b;	dstTri[2] = c;

	warp_mat = getAffineTransform(srcTri,dstTri);
	warpAffine(img,dst,warp_mat,dst.size());
}


void CESMAutoAdjustMgr::moveImage(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

BOOL CESMAutoAdjustMgr::IsValidAdjustSquareInfo()
{
	if(m_pSquareCalibration->IsValidAdjustSquareInfo() == true)
		return TRUE;
	else
		return FALSE;
}

void CESMAutoAdjustMgr::SetAdjustSquareInfo()
{
	m_pSquareCalibration->ClearVectors();
	DscAdjustInfo* pAdjustInfo = NULL;

	for(int i = 0; i < GetDscCount(); i++)
	{
		AdjustSquareInfo* pAdjustSquareInfo = new AdjustSquareInfo;
		pAdjustInfo = GetDscAt(i);

		CT2CA pszConvertedAnsiDscId(pAdjustInfo->strDscName);
		string strAnsiDscId(pszConvertedAnsiDscId);

		pAdjustSquareInfo->nIndex = i;
		pAdjustSquareInfo->strDscId = strAnsiDscId;

		vector<Point3f> ptPoints;
		int nMaxRange = 100;

		ptPoints.push_back(Point3f(0,		 0,			0));
		ptPoints.push_back(Point3f(nMaxRange, 0,			0));
		ptPoints.push_back(Point3f(nMaxRange, nMaxRange,	0));
		ptPoints.push_back(Point3f(0,		 nMaxRange,	0));
		ptPoints.push_back(Point3f(0,		 0,			0));
		ptPoints.push_back(Point3f(nMaxRange, 0,			0));
		ptPoints.push_back(Point3f(nMaxRange, 0,			0));

		pAdjustSquareInfo->vecPtInit3DSquarePoint = ptPoints;

		//180723 wgkim
		pAdjustSquareInfo->vecPtSquarePoints.push_back(Point2f(0.f, 0.f));
		pAdjustSquareInfo->vecPtSquarePoints.push_back(Point2f(0.f, 0.f));
		pAdjustSquareInfo->vecPtSquarePoints.push_back(Point2f(0.f, 0.f));
		pAdjustSquareInfo->vecPtSquarePoints.push_back(Point2f(0.f, 0.f));
		//pAdjustSquareInfo->vecPtSquarePoints.push_back(Point2f(0.f, 0.f));
		//pAdjustSquareInfo->vecPtSquarePoints.push_back(Point2f(0.f, 0.f));
		//pAdjustSquareInfo->vecPtSquarePoints.push_back(Point2f(0.f, 0.f));
		//pAdjustSquareInfo->vecPtSquarePoints.push_back(Point2f(0.f, 0.f));
		//pAdjustSquareInfo->vecPtSquarePoints.push_back(Point2f(0.f, 0.f));
		//pAdjustSquareInfo->vecPtSquarePoints.push_back(Point2f(0.f, 0.f));

		m_pSquareCalibration->AddSquarePoint(pAdjustSquareInfo);
	}
}

void CESMAutoAdjustMgr::SetAdjustSquarePointAt(CString strDscId, CPoint pt1, CPoint pt2, CPoint pt3, CPoint pt4)
{
	CT2CA pszConvertedAnsiDscId(strDscId);
	string strAnsiDscId(pszConvertedAnsiDscId);

	Point2f pt1_; pt1_.x = pt1.x; pt1_.y = pt1.y;
	Point2f pt2_; pt2_.x = pt2.x; pt2_.y = pt2.y;
	Point2f pt3_; pt3_.x = pt3.x; pt3_.y = pt3.y;
	Point2f pt4_; pt4_.x = pt4.x; pt4_.y = pt4.y;

	m_pSquareCalibration->SetSquarePointAt(strAnsiDscId, pt1_, pt2_, pt3_, pt4_);
}

void CESMAutoAdjustMgr::AddAdjustSquareInfo(int nIndex, CString strDscId, vector<CPoint> vecPtPoints,
	CPoint pt1, CPoint pt2, CPoint pt3, CPoint pt4)
{
	CT2CA pszConvertedAnsiDscId(strDscId);
	string strAnsiDscId(pszConvertedAnsiDscId);

	AdjustSquareInfo* pAdjustSquareInfo = m_pSquareCalibration->GetSquareInfo(strAnsiDscId);

	if(pAdjustSquareInfo == NULL)
		return;

	for(int i = 0; i < vecPtPoints.size(); i++)
	{
		Point3f ptPoints = Point3f(vecPtPoints[i].x, vecPtPoints[i].y, 0.f);
		pAdjustSquareInfo->vecPtInit3DSquarePoint[i] = ptPoints;
	}
	m_pSquareCalibration->Normalization3DSquarePoint(
		pAdjustSquareInfo->vecPtInit3DSquarePoint, 100);

	//180723 wgkim
	if(pAdjustSquareInfo->vecPtSquarePoints.size() == 4)
	{
		Point2f pt2f1 = Point2f(pt1.x, pt1.y);
		Point2f pt2f2 = Point2f(pt2.x, pt2.y);
		Point2f pt2f3 = Point2f(pt3.x, pt3.y);
		Point2f pt2f4 = Point2f(pt4.x, pt4.y);

		//180723 wgkim
		if( pt1.x == 0 || pt1.y == 0 ||
			pt2.x == 0 || pt2.y == 0 ||
			pt3.x == 0 || pt3.y == 0 ||
			pt4.x == 0 || pt4.y == 0)
		{
			vector<AdjustSquareInfo*> vecAdjustSquareInfo = m_pSquareCalibration->GetSquarePoints();
			if(nIndex == 0)
			{
				pAdjustSquareInfo->vecPtSquarePoints[0] = vecAdjustSquareInfo[GetDscCount()-1]->vecPtSquarePoints.at(0);
				pAdjustSquareInfo->vecPtSquarePoints[1] = vecAdjustSquareInfo[GetDscCount()-1]->vecPtSquarePoints.at(1);
				pAdjustSquareInfo->vecPtSquarePoints[2] = vecAdjustSquareInfo[GetDscCount()-1]->vecPtSquarePoints.at(2);
				pAdjustSquareInfo->vecPtSquarePoints[3] = vecAdjustSquareInfo[GetDscCount()-1]->vecPtSquarePoints.at(3);
				//pAdjustSquareInfo->vecPtSquarePoints[4] = vecAdjustSquareInfo[GetDscCount()-1]->vecPtSquarePoints.at(0) + Point2f(1.f, 1.f);
				//pAdjustSquareInfo->vecPtSquarePoints[5] = vecAdjustSquareInfo[GetDscCount()-1]->vecPtSquarePoints.at(1) - Point2f(1.f, 1.f);
				//pAdjustSquareInfo->vecPtSquarePoints[6] = vecAdjustSquareInfo[GetDscCount()-1]->vecPtSquarePoints.at(1) + Point2f(1.f, 1.f);
				//pAdjustSquareInfo->vecPtSquarePoints[7] = vecAdjustSquareInfo[GetDscCount()-1]->vecPtSquarePoints.at(2) - Point2f(1.f, 1.f);
				//pAdjustSquareInfo->vecPtSquarePoints[8] = vecAdjustSquareInfo[GetDscCount()-1]->vecPtSquarePoints.at(2) + Point2f(1.f, 1.f);
				//pAdjustSquareInfo->vecPtSquarePoints[9] = vecAdjustSquareInfo[GetDscCount()-1]->vecPtSquarePoints.at(2) - Point2f(2.f, 2.f);
			}
			else
			{
				pAdjustSquareInfo->vecPtSquarePoints[0] = vecAdjustSquareInfo[nIndex-1]->vecPtSquarePoints.at(0);
				pAdjustSquareInfo->vecPtSquarePoints[1] = vecAdjustSquareInfo[nIndex-1]->vecPtSquarePoints.at(1);
				pAdjustSquareInfo->vecPtSquarePoints[2] = vecAdjustSquareInfo[nIndex-1]->vecPtSquarePoints.at(2);
				pAdjustSquareInfo->vecPtSquarePoints[3] = vecAdjustSquareInfo[nIndex-1]->vecPtSquarePoints.at(3);
				//pAdjustSquareInfo->vecPtSquarePoints[4] = vecAdjustSquareInfo[nIndex-1]->vecPtSquarePoints.at(0) + Point2f(1.f, 1.f);
				//pAdjustSquareInfo->vecPtSquarePoints[5] = vecAdjustSquareInfo[nIndex-1]->vecPtSquarePoints.at(1) - Point2f(1.f, 1.f);
				//pAdjustSquareInfo->vecPtSquarePoints[6] = vecAdjustSquareInfo[nIndex-1]->vecPtSquarePoints.at(1) + Point2f(1.f, 1.f);
				//pAdjustSquareInfo->vecPtSquarePoints[7] = vecAdjustSquareInfo[nIndex-1]->vecPtSquarePoints.at(2) - Point2f(1.f, 1.f);
				//pAdjustSquareInfo->vecPtSquarePoints[8] = vecAdjustSquareInfo[nIndex-1]->vecPtSquarePoints.at(2) + Point2f(1.f, 1.f);
				//pAdjustSquareInfo->vecPtSquarePoints[9] = vecAdjustSquareInfo[nIndex-1]->vecPtSquarePoints.at(2) - Point2f(2.f, 2.f);
			}
		}
		else
		{
			pAdjustSquareInfo->vecPtSquarePoints[0] = pt2f1;
			pAdjustSquareInfo->vecPtSquarePoints[1] = pt2f2;
			pAdjustSquareInfo->vecPtSquarePoints[2] = pt2f3;
			pAdjustSquareInfo->vecPtSquarePoints[3] = pt2f4;
			//pAdjustSquareInfo->vecPtSquarePoints[4] = pt2f1 + Point2f(1.f, 1.f);
			//pAdjustSquareInfo->vecPtSquarePoints[5] = pt2f2 - Point2f(1.f, 1.f);
			//pAdjustSquareInfo->vecPtSquarePoints[6] = pt2f2 + Point2f(1.f, 1.f);
			//pAdjustSquareInfo->vecPtSquarePoints[7] = pt2f3 - Point2f(1.f, 1.f);
			//pAdjustSquareInfo->vecPtSquarePoints[8] = pt2f3 + Point2f(1.f, 1.f);
			//pAdjustSquareInfo->vecPtSquarePoints[9] = pt2f3 - Point2f(2.f, 2.f);
		}
	}
}

void CESMAutoAdjustMgr::ClearAdjustSquareInfo()
{
	for(int i = 0; i < m_vecAdjustSquareInfo.size(); i++)
	{
		AdjustSquareInfo* pAdjustSquareInfo = m_vecAdjustSquareInfo[i];
		delete pAdjustSquareInfo;
	}
	m_vecAdjustSquareInfo.clear();
}

void CESMAutoAdjustMgr::SetSquarePointInWorld(CPoint point)
{
	if(m_ptSquartPointInWorld.size() >= 4)
		return;

	Point3f ptPoint;
	ptPoint.x = point.x;
	ptPoint.y = point.y;
	ptPoint.z = 0;

	TRACE("Input: %d, %d\n", ptPoint.x, ptPoint.y);
	m_ptSquartPointInWorld.push_back(ptPoint);

	if(m_ptSquartPointInWorld.size() == 4)
	{
		for(int i = 0; i < GetDscCount(); i++)
		{
			DscAdjustInfo* pAdjustInfo = NULL;
			pAdjustInfo = GetDscAt(i);

			CString strDscId = pAdjustInfo->strDscName;
			CT2CA pszConvertedAnsiDscId(strDscId);
			string strAnsiDscId(pszConvertedAnsiDscId);

			m_pSquareCalibration->Set3DInitSquarePointAt(strAnsiDscId, m_ptSquartPointInWorld);
		}
		TRACE("Input WorldCoordination\n");
	}
}

void CESMAutoAdjustMgr::SetSquarePointInWorldAt(CString strDscId, vector<CPoint> vecCPoint)
{
	CT2CA pszConvertedAnsiDscId(strDscId);
	string strAnsiDscId(pszConvertedAnsiDscId);

	vector<Point3f> vecPtPoint;
	for(int i = 0; i < vecCPoint.size(); i++)
	{
		vecPtPoint.push_back(Point3f());
		vecPtPoint[i].x = vecCPoint[i].x;
		vecPtPoint[i].y = vecCPoint[i].y;
		vecPtPoint[i].z = 0;
	}

	m_pSquareCalibration->Set3DInitSquarePointAt(strAnsiDscId, vecPtPoint);
}

void CESMAutoAdjustMgr::ClearSquarePointInWorld()
{
	m_ptSquartPointInWorld.clear();
}

vector<CPoint> CESMAutoAdjustMgr::GetSquarePointInWorld()
{
	vector<CPoint> vecPtSquarePoint;
	for(int i = 0; i < m_ptSquartPointInWorld.size(); i++)
	{
		CPoint ptPoint;
		ptPoint.x = m_ptSquartPointInWorld[i].x;
		ptPoint.y = m_ptSquartPointInWorld[i].y;

		vecPtSquarePoint.push_back(ptPoint);
	}
	return vecPtSquarePoint;
}

vector<CPoint> CESMAutoAdjustMgr::GetSquarePointInWorldAt(CString strDscId)
{
	CT2CA pszConvertedAnsiDscId(strDscId);
	string strAnsiDscId(pszConvertedAnsiDscId);

	vector<Point3f> vecPt3fSquarePoint = m_pSquareCalibration->Get3DInitSquarePointAt(strAnsiDscId);
	vector<CPoint> vecPtSquarePoint;
	for(int i = 0; i < vecPt3fSquarePoint.size(); i++)
	{
		CPoint ptPoint;
		ptPoint.x = vecPt3fSquarePoint[i].x;
		ptPoint.y = vecPt3fSquarePoint[i].y;

		vecPtSquarePoint.push_back(ptPoint);
	}
	return vecPtSquarePoint;
}

void CESMAutoAdjustMgr::SetCenterPointAt(CString strDscId, CPoint ptCenterPoint)
{
	CT2CA pszConvertedAnsiDscId(strDscId);
	string strAnsiDscId(pszConvertedAnsiDscId);

	Point2f ptTempPoint = Point2f(ptCenterPoint.x, ptCenterPoint.y);
	m_pSquareCalibration->SetCenterPointAt(strAnsiDscId, ptTempPoint);
}

void CESMAutoAdjustMgr::SetInitCenterPoint(CString strStartDscId, CString strEndDscId, CPoint ptCenterPoint)
{
	CT2CA pszConvertedAnsiStartDscId(strStartDscId);
	string strAnsiStartDscId(pszConvertedAnsiStartDscId);

	CT2CA pszConvertedAnsiEndDscId(strEndDscId);
	string strAnsiEndDscId(pszConvertedAnsiEndDscId);

	Point2f ptTempPoint = Point2f(ptCenterPoint.x, ptCenterPoint.y);

	if(strAnsiEndDscId.compare(strAnsiStartDscId) == 0)
		m_pSquareCalibration->SetInitCenterPoint(strAnsiStartDscId, ptTempPoint);
	else
		m_pSquareCalibration->SetInitCenterPointRange(strAnsiStartDscId, strAnsiEndDscId, ptTempPoint);
}

CPoint CESMAutoAdjustMgr::GetCenterPoint(CString strDscId)
{
	CT2CA pszConvertedAnsiDscId(strDscId);
	string strAnsiDscId(pszConvertedAnsiDscId);

	Point2f ptTempPoint = m_pSquareCalibration->GetCenterPointAt(strAnsiDscId);

	CPoint ptResultPoint = CPoint(ptTempPoint.x, ptTempPoint.y);
	return ptResultPoint;
}

double CESMAutoAdjustMgr::GetNormValue(CString strDscId)
{
	CT2CA pszConvertedAnsiDscId(strDscId);
	string strAnsiDscId(pszConvertedAnsiDscId);

	return m_pSquareCalibration->GetNormValueAt(strAnsiDscId);
}

void CESMAutoAdjustMgr::SetNormValue(CString strDscId, double dValue)
{
	CT2CA pszConvertedAnsiDscId(strDscId);
	string strAnsiDscId(pszConvertedAnsiDscId);

	m_pSquareCalibration->SetNormValueAt(strAnsiDscId, dValue);
}

double CESMAutoAdjustMgr::GetDistanceValue(CString strDscId)
{
	CT2CA pszConvertedAnsiDscId(strDscId);
	string strAnsiDscId(pszConvertedAnsiDscId);

	return m_pSquareCalibration->GetDistanceValueAt(strAnsiDscId);
}

void CESMAutoAdjustMgr::SetDistanceValue(CString strDscId, double dValue)
{
	CT2CA pszConvertedAnsiDscId(strDscId);
	string strAnsiDscId(pszConvertedAnsiDscId);

	m_pSquareCalibration->SetDistanceValueAt(strAnsiDscId, dValue);
}

void CESMAutoAdjustMgr::SetKZonePoint()
{
	for(int i = 0; i < GetDscCount(); i++)
	{
		DscAdjustInfo* pAdjustInfo = NULL;
		pAdjustInfo = GetDscAt(i);
		CString strDscId = pAdjustInfo->strDscName;

		CT2CA pszConvertedAnsiDscId(strDscId);
		string strAnsiDscId(pszConvertedAnsiDscId);

		m_pSquareCalibration->SetKZonePointAt(strAnsiDscId);
	}
}

#include "DSCItem.h"
int CESMAutoAdjustMgr::LoadSquarePointDataForKZone(CString strFileName)
{
	CESMIni ini;
	if(!ini.SetIniFilename (strFileName))
	{
		m_bValidKZone = FALSE;
		return 0;
	}

	if (GetDscCount() <= 0)
	{
		CObArray arDSCList;
		ESMGetDSCList(&arDSCList);
		CDSCItem* pItem = NULL;
		DscClear();

		for (int i = 0; i < arDSCList.GetSize(); i++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(i);
			AddDscInfo(pItem->GetDeviceDSCID(), pItem->m_strInfo[DSC_INFO_LOCATION]);
		}
	}

	int nHeight = 0, nWidth = 0;
	nHeight = ini.GetInt(_T("MovieSize"), _T("HEIGHT"), 0);
	nWidth	= ini.GetInt(_T("MovieSize"), _T("WIDTH"), 0);

	//if(m_pSquareCalibration == NULL)
	//{
	//	CESMAutoAdjustSquareCalibration* pSquareCalibration = NULL;
	//	pSquareCalibration = new CESMAutoAdjustSquareCalibration();
	//	m_pSquareCalibration = pSquareCalibration;
	//}

	if(m_pSquareCalibration == NULL)
	{
		delete m_pSquareCalibration;
	}
	m_pSquareCalibration = new CESMAutoAdjustSquareCalibration;

	SetAdjustSquareInfo();

	CString strData;	
	for(int i = 0; i < GetDscCount(); i++)
	{
		CString strSelect = GetDscAt(i)->strDscName;

		vector<CPoint> square;
		CPoint pt1, pt2, pt3, pt4;
		CPoint ptw1, ptw2, ptw3, ptw4;
		CPoint ptCenter;

		strData = ini.GetString(strSelect, _T("T_LBX"));
		pt1.x = _ttoi(strData);
		strData = ini.GetString(strSelect, _T("T_LBY"));
		pt1.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("T_RBX"));
		pt2.x = _ttoi(strData);
		strData = ini.GetString(strSelect, _T("T_RBY"));
		pt2.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("T_PeakX"));
		pt3.x = _ttoi(strData);
		strData = ini.GetString(strSelect, _T("T_PeakY"));
		pt3.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("B_PeakX"));
		pt4.x = _ttoi(strData);
		strData = ini.GetString(strSelect, _T("B_PeakY"));
		pt4.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("1st_World_X")); 
		ptw1.x = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("1st_World_Y")); 
		ptw1.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("2nd_World_X")); 
		ptw2.x = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("2nd_World_Y")); 
		ptw2.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("3rd_World_X")); 
		ptw3.x = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("3rd_World_Y")); 
		ptw3.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("4th_World_X")); 
		ptw4.x = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("4th_World_Y")); 
		ptw4.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("CenterX")); 
		ptCenter.x = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("CenterY")); 
		ptCenter.x = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("K-Zone"));
		if(strData.Compare(_T("TRUE")) == 0)
		{
			SetUseKZoneAt(strSelect, TRUE);
		}
		else
		{
			SetUseKZoneAt(strSelect, FALSE);
		}

		vector<CPoint> ptPoint;
		ptPoint.push_back(ptw1);
		ptPoint.push_back(ptw2);
		ptPoint.push_back(ptw3);
		ptPoint.push_back(ptw4);

		SetSquarePointInWorldAt(strSelect, ptPoint);
		SetCenterPointAt(strSelect, ptCenter);
		SetAdjustSquarePointAt(strSelect, pt1, pt2, pt3, pt4);
	}
	SetKZonePoint();

	if(m_pKZoneDataInfo != NULL)
	{
		delete[] m_pKZoneDataInfo;
		m_pKZoneDataInfo == NULL;
	}

	m_pKZoneDataInfo = new KZoneDataInfo[GetDscCount()];

	for(int i = 0; i < GetDscCount(); i++)
	{
		DscAdjustInfo* pAdjustInfo = NULL;
		pAdjustInfo = GetDscAt(i);
		CString strDscId = pAdjustInfo->strDscName;

		CT2CA pszConvertedAnsiDscId(strDscId);
		string strAnsiDscId(pszConvertedAnsiDscId);

		vector<Point2f> pt2fKZonePoint = m_pSquareCalibration->GetKZonePointAt(strAnsiDscId);

		//wgkim 191024
		if (pt2fKZonePoint.empty())
			continue;

		if(GetUsageKZone())
		{
			if(m_pSquareCalibration->GetUseKZoneAt(strAnsiDscId))
				m_pKZoneDataInfo[i].bIsEnabled = TRUE;
			else
				m_pKZoneDataInfo[i].bIsEnabled = FALSE;
		}
		else
		{
			m_pKZoneDataInfo[i].bIsEnabled = FALSE;
		}

		//m_pKZoneDataInfo[i].nDscId = _ttoi(strDscId);
		strcpy(m_pKZoneDataInfo[i].szDscID, strAnsiDscId.c_str());
		for(int j = 0; j < 10; j++)
		{
			CPoint ptKZonePoint(pt2fKZonePoint[j].x, pt2fKZonePoint[j].y);
			m_pKZoneDataInfo[i].ptKZonePoint[j] = ptKZonePoint;
		}
	}
	m_bValidKZone = TRUE;
	return 1;
}

KZoneDataInfo* CESMAutoAdjustMgr::GetKZonePoint()
{
	return m_pKZoneDataInfo;
}

BOOL CESMAutoAdjustMgr::IsValidKZone()
{
	return m_bValidKZone;
}

BOOL CESMAutoAdjustMgr::GetUseKZoneAt(CString strDscId)
{
	CT2CA pszConvertedAnsiDscId(strDscId);
	string strAnsiDscId(pszConvertedAnsiDscId);

	return m_pSquareCalibration->GetUseKZoneAt(strAnsiDscId);
}

void CESMAutoAdjustMgr::SetUseKZoneAt(CString strDscId, BOOL bUseKZone)
{
	CT2CA pszConvertedAnsiDscId(strDscId);
	string strAnsiDscId(pszConvertedAnsiDscId);

	m_pSquareCalibration->SetUseKZoneAt(strAnsiDscId, bUseKZone);
}

void CESMAutoAdjustMgr::SetToggleUsingKZone(bool bToggleUSingKZone)
{
	m_bToggleUsingKZone = bToggleUSingKZone;

	if(m_pKZoneDataInfo == NULL)
	{
		CString strFileName;
		strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), _T("TemplateTemp.pta"));
		LoadSquarePointDataForKZone(strFileName);
	}

	for(int i = 0; i < GetDscCount(); i++)
	{
		DscAdjustInfo* pAdjustInfo = NULL;
		pAdjustInfo = GetDscAt(i);
		CString strDscId = pAdjustInfo->strDscName;

		CT2CA pszConvertedAnsiDscId(strDscId);
		string strAnsiDscId(pszConvertedAnsiDscId);

		if(bToggleUSingKZone)
		{
			if(m_pSquareCalibration->GetUseKZoneAt(strAnsiDscId))
				m_pKZoneDataInfo[i].bIsEnabled = TRUE;
			else
				m_pKZoneDataInfo[i].bIsEnabled = FALSE;
		}
		else
		{
			m_pKZoneDataInfo[i].bIsEnabled = FALSE;
		}
	}
}

BOOL CESMAutoAdjustMgr::GetUsageKZone()
{
	return m_bToggleUsingKZone;
}