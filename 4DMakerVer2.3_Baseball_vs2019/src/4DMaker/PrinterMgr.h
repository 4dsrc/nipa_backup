#pragma once
class CPrinterMgr
{
public:
	enum{
		IMAGE_ZOOM_NORMAL = 0,
		IMAGE_ZOOM_3_2,
		IMAGE_ZOOM_2_1,
		IMAGE_ZOOM_5_4,
	};

public:
	CPrinterMgr(void);
	~CPrinterMgr(void);

public:
	BOOL PrintStart(CString strPrinter, CString strFile, int nZoom = 0);
	BOOL GetPrinterDevice(LPTSTR pszPrinterName, HGLOBAL* phDevNames, HGLOBAL* phDevMode);
	BOOL SetDIBToDevice(HDC hdc,HBITMAP hbmp,int xDest,int yDest,int DestWidth,int DestHeight,int xSrc,int ySrc, int nZoom = 0, bool bStretch=false,int nStretchMode=COLORONCOLOR,DWORD dwRop=SRCCOPY);
	HBITMAP LoadGraphicsFile(LPCTSTR szFileName);

	bool IsPrinterOnline(wstring strPrinterFriendlyName);
};

