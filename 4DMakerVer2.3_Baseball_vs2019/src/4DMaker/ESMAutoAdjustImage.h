#pragma once

#include "cv.h"


struct stAutoAdjustInfo
{
	stAutoAdjustInfo()
	{
		t_peak = cv::Point(0,0);
		t_LT = cv::Point(0,0);
		t_LB = cv::Point(0,0);
		t_RT = cv::Point(0,0);
		t_RB = cv::Point(0,0);
		b_peak = cv::Point(0,0);
		b_LT = cv::Point(0,0);
		b_LB = cv::Point(0,0);
		b_RT = cv::Point(0,0);
		b_RB = cv::Point(0,0);
		height = 0;
	}
	cv::Point t_peak;
	cv::Point t_LT;
	cv::Point t_LB;
	cv::Point t_RB;
	cv::Point t_RT;
	cv::Point b_peak;
	cv::Point b_LT;
	cv::Point b_LB;
	cv::Point b_RB;
	cv::Point b_RT;
	int height;
};
class CESMAutoAdjustImage : public CStatic
{
	DECLARE_DYNAMIC(CESMAutoAdjustImage)

public:
	CESMAutoAdjustImage();
	virtual ~CESMAutoAdjustImage();

	CDC* m_memDC;
	BITMAP m_bmpinfo;	
	double m_dMultiple;

	//wgkim 17-06-20
	double m_dPrevMultiple;

	int m_nImageWidth, m_nImageHeight;
	int m_nCircleSize;
	BOOL m_bImageMove;
	CPoint m_ptImageMoveStart, m_ptImageMoving, m_ptImageLeftTop;
	//	BOOL InitSetup();

	BOOL SetImageBuffer(BYTE* pImage, int nWidth, int nHeight, int nChannel,stAutoAdjustInfo *priinfo);
	void SetImagePos(int nPosX, int nPosY);
	void Redraw();
	void ImageZoom(CPoint ptMouse, BOOL bZoom);
	double GetMultiple() { return m_dMultiple;}
	CPoint GetImagePos() { return m_ptImageLeftTop; }
	stAutoAdjustInfo IPriinfo;
	int mScale;
	int mMove;
	//-- 2014-08-30 hongsu@esmlab.com
	//-- Find Exact Position
	BYTE* m_pTpImage;
	IplImage* m_pImage;
	IplImage* BYTE2IplImage(BYTE* pByte, int nWidth, int nHeight);
	CPoint FindExactPosition(int& nX, int& nY);
	void GetImageRGB(int nX, int nY, int& nColorR, int& nColorG, int& nColorB);	
	CPoint FindCenter(int nX, int nY, CRect rtArea);

	void DrawRect(CRect rtDraw, COLORREF color);
	void DrawLine(int nHeight, COLORREF color);
	void DrawPoint(int nX, int nY, COLORREF color);
	void DrawCenterLine(int nWidth, COLORREF color);
	void DrawZoom(float nZoom, CRect* rt, CPoint margin);
	void DrawLine(int nX1, int nY1, int nX2, int nY2, COLORREF color);

	void WriteRGB(IplImage*	pImgArea, int nWidth, int nHeight, int &nMin, int &nMax, int &nRgbAvg);
	void FindCenterRegion(IplImage*	pImgArea, int nLength,  CPoint& ptCenter, int nRgbMin, int nRgbMax, int nRgbAvg);
	int GetRGBAvg(IplImage*	pImgArea, int nX, int nY);
	void GetAxisInfo(stAutoAdjustInfo priInfo,int scale,int move)
	{
		IPriinfo = priInfo;
		mScale = scale;
		mMove = move;
	};

protected:
	BOOL DrawDefectPoint(int PosX, int PosY, COLORREF color);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	//Draw Ruler
private:
	BOOL m_bRulerPoint;
	CPoint m_ptStartRulerPoint;
	CPoint m_ptEndRulerPoint;

	vector<CPoint> m_vecStartRulerPoint;
	vector<CPoint> m_vecEndRulerPoint;

	int nRulerRGB;
	COLORREF cRulerRGB;

public:
	void SetUsingRuler(BOOL bRulerPoint){m_bRulerPoint = bRulerPoint;};
	BOOL GetUsingRuler() {return m_bRulerPoint;};

	CPoint StartDrawRuler(int nX, int nY);
	CPoint DrawRuler(int nX, int nY);
	void EndDrawRuler();
	void ClearDrawRuler();

	void RulerColorChange();
	void DrawLegacyRuler();
	void DrawRulerLine(int nX1, int nY1, int nX2, int nY2, int nThickness);

public:
	IplImage* m_pImageWithRulerLine;

};
