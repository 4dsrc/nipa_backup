////////////////////////////////////////////////////////////////////////////////
//
//	ESMNetworkListCtrl.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMAutoAdjustListCtrl.h"
#include "ESMNetworkDlg.h"
#include "NetworkAgentAddDlg.h"
#include "ESMUtil.h"
#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CESMAutoAdjustListCtrl::CESMAutoAdjustListCtrl(void)
{
	InitializeCriticalSection (&m_CrSockket);

//	m_UseCamStatusColor = TRUE;
}

CESMAutoAdjustListCtrl::~CESMAutoAdjustListCtrl(void)
{
	DeleteCriticalSection (&m_CrSockket);
}

BEGIN_MESSAGE_MAP(CESMAutoAdjustListCtrl, CListCtrl)
	ON_WM_MEASUREITEM()
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CESMAutoAdjustListCtrl::OnNMCustomdraw)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CESMAutoAdjustListCtrl::OnNMDblclk)
END_MESSAGE_MAP()

void CESMAutoAdjustListCtrl::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	CListCtrl::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CESMAutoAdjustListCtrl::OnDestroy()
{
	CListCtrl::OnDestroy();
}

void CESMAutoAdjustListCtrl::Clear()
{
	int nItemCount = GetItemCount();
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-10-08
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMAutoAdjustListCtrl::Init()
{
	InsertColumn(0, _T("[AR]ID"), 0, 48);
	InsertColumn(1, _T("DscId"), 0, 44);
	InsertColumn(2, _T("1st X"), 0, 50);
	InsertColumn(3, _T("1st Y"), 0, 50); 
	InsertColumn(4, _T("2nd X"), 0, 50);
	InsertColumn(5, _T("2nd Y"), 0, 50);
	InsertColumn(6, _T("3rd X"), 0, 50);
	InsertColumn(7, _T("3rd Y"), 0, 50);
	InsertColumn(10, _T("4th X"), 0, 50);
	InsertColumn(11, _T("4th Y"), 0, 50);
	InsertColumn(12, _T("CenterX"), 0, 50);
	InsertColumn(13, _T("CenterY"), 0, 50);
	InsertColumn(14, _T("Select"), 0, 50);
	InsertColumn(15, _T("distance"), 0, 50);
	InsertColumn(16, _T("AdjustX"), 0, 50);
	InsertColumn(17, _T("AdjustY"), 0, 50);
	InsertColumn(18, _T("Angle"), 0, 50);
	InsertColumn(19, _T("RotateX"), 0, 50);
	InsertColumn(20, _T("RotateY"), 0, 50);
	InsertColumn(21, _T("Scale"), 0, 50);

	InsertColumn(22, _T("1st W_X"), 0, 50);
	InsertColumn(23, _T("1st W_Y"), 0, 50);
	InsertColumn(24, _T("2nd W_X"), 0, 50);
	InsertColumn(25, _T("2nd W_Y"), 0, 50);
	InsertColumn(26, _T("3rd W_X"), 0, 50);
	InsertColumn(27, _T("3rd W_Y"), 0, 50);
	InsertColumn(28, _T("4th W_X"), 0, 50);
	InsertColumn(29, _T("4th W_Y"), 0, 50);

	SetExtendedStyle(LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
}

void CESMAutoAdjustListCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	//	blablabla....
	CListCtrl::OnLButtonDown(nFlags, point);
}

void CESMAutoAdjustListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	LPNMLVCUSTOMDRAW pLVCD = reinterpret_cast<LPNMLVCUSTOMDRAW>(pNMHDR);

	if(pLVCD->nmcd.dwDrawStage == CDDS_PREPAINT)
	{
		CDC* pDC = CDC::FromHandle(pNMCD->hdc);
		CRect rect(0, 0, 0, 0);
		GetClientRect(&rect);
		pDC->FillSolidRect(&rect, RGB(44, 44, 44));

		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if(pLVCD->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
	{
		int nItem = static_cast<int>(pLVCD->nmcd.dwItemSpec);
		CString strTemp = GetItemText(nItem, LIST_CODE/*pLVCD->iSubItem*/);
		if(!strTemp.IsEmpty())
		{
			if (nItem%2 == 0)
				pLVCD->clrTextBk	= RGB(35,35,35);
			else
				pLVCD->clrTextBk	= RGB(40,40,40);

			pLVCD->clrText		= RGB(255,255,255);
			*pResult = (LRESULT)CDRF_NOTIFYSUBITEMDRAW;//sub-item 을 변경하기 위해서.
			return;
		}
		else
		{
			//기본색상 - 검정 배경 / 화이트 텍스트
			pLVCD->clrTextBk = RGB(44,44,44);
			pLVCD->clrText	 = RGB(255,255,255);
		}
		*pResult = CDRF_DODEFAULT;
	}
	 else if ( ( CDDS_ITEMPREPAINT | CDDS_SUBITEM) == pLVCD->nmcd.dwDrawStage ) 
	 {
		 if(pLVCD->iSubItem == 0 || pLVCD->iSubItem == 1)
			 pLVCD->clrText		= RGB(255,255,255);
		 else if(pLVCD->iSubItem == 2 || pLVCD->iSubItem == 3 ||
				 pLVCD->iSubItem == 20 || pLVCD->iSubItem == 21)
			 pLVCD->clrText		= RGB(0,255,0);
		 else if(pLVCD->iSubItem == 4 || pLVCD->iSubItem == 5 ||
				 pLVCD->iSubItem == 22 || pLVCD->iSubItem == 23)
			 pLVCD->clrText		= RGB(255,0,0);
		 else if(pLVCD->iSubItem == 6 || pLVCD->iSubItem == 7 ||
				 pLVCD->iSubItem == 24 || pLVCD->iSubItem == 25)
			 pLVCD->clrText		= RGB(0,255,255);
		 else if(pLVCD->iSubItem == 8 || pLVCD->iSubItem == 9 ||
				 pLVCD->iSubItem == 26 || pLVCD->iSubItem == 27)
			 pLVCD->clrText		= RGB(255,255,0);
		 else if(pLVCD->iSubItem == 10 || pLVCD->iSubItem == 11)
			 pLVCD->clrText		= RGB(255,0,255);
		 else
			 pLVCD->clrText	 = RGB(255,255,255);
		 //pLVCD->clrTextBk = RGB(88,88,88);
		 //pLVCD->clrText	 = RGB(23,64,255);
		 *pResult = CDRF_DODEFAULT;
	}
}

void CESMAutoAdjustListCtrl::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if(pNMItemActivate->iItem != -1)
	{
		CString strIP = GetItemText(pNMItemActivate->iItem, 2);
		if(strIP.IsEmpty() != TRUE)
		{
			CString strParam;
			strParam.Format(_T("/v %s"),strIP);
			ESMUtil::ExecuteProcess(_T("NULL"), _T("mstsc"), strParam);
		}		
	}
	*pResult = 0;
}

void CESMAutoAdjustListCtrl::PreSubclassWindow()
{	
	CListCtrl::PreSubclassWindow();
	m_HeaderCtrl.SubclassWindow(::GetDlgItem(m_hWnd,0));
}
