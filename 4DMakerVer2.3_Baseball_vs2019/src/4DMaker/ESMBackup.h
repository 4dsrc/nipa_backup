#pragma once
#include <vector>
#include <memory>  // Siseong Ahn 20190612
#include "EMSFtp.h"

using namespace std;  // Siseong Ahn 20190612

class CESMBackup
{
public:
	enum BACKUP_FTP_PROCESS_STATE
	{
		BACKUP_FTP_PROCESS_STATE_READY		= 0x0,
		BACKUP_FTP_PROCESS_STATE_PROCESSING	= 0x1,
		BACKUP_FTP_PROCESS_STATE_FORCEKILL	= 0x2,
		BACKUP_FTP_PROCESS_STATE_END		= 0x4,
	}; BACKUP_FTP_PROCESS_STATE m_pBackupFtpProcessState;

	void SetBackupFtpState(BACKUP_FTP_PROCESS_STATE backupFtpState);
	BACKUP_FTP_PROCESS_STATE GetBackupFtpState();

	enum BACKUP_FTP_FILE_STATE
	{
		BACKUP_FTP_FILE_STATE_READY			= 0x0,
		BACKUP_FTP_FILE_STATE_OK			= 0x1,
		BACKUP_FTP_FILE_STATE_NOTDONE		= 0x2,
		BACKUP_FTP_FILE_STATE_INCOMPLETE	= 0x4,
	};

public:
	//CESMBackup(void);
	CESMBackup();
	~CESMBackup(void);

	void DeleteSelectList();
	void BackupByFtp();	
	static unsigned WINAPI BackupByFtp(LPVOID param);
	static unsigned WINAPI BackupByFtpRe(LPVOID param);
	HANDLE m_hHandle;

	void FtpInit();
	void SetFTPInfo(CString host, CString port, CString id, CString pwd);
	void CloseFtp();

	void SetUseFolderList(BOOL f);
	void SetUseDeleteAfterTransfer(BOOL f);
	void SetStrLocalRootPath(CString strLocalRootPath);
	void SetStrFtpRootPath(CString strFtpRootPath);
	int GetFileCount();
	int GetFileCount(CString strPath);
	BACKUP_FTP_FILE_STATE CheckUploadStateAllFile();
	void FileUpload(std::shared_ptr<CEMSFtp> pFtp, CString strLocalFile, CString strCurrentDir);

	vector<BOOL>	m_arrBFileUploadCheck;

private:	
	double GetAllFilePathAndFileName(CString strPath, std::shared_ptr<CEMSFtp> pFtp);
	int GetFolderListCount();
	BOOL CheckMatchFolder(CString strLocal, BOOL bDeleteAfter=TRUE);
	void DeleteAfterTransfer(CString strPath, BOOL bDeleteAfter=TRUE);
	void DeleteDir(CString strPath, BOOL bDir=TRUE);
	
private:
	CString m_strAddress;
	CString m_strPort;
	CString m_strID;
	CString m_strPWD;

	CString			m_strLocalRootPath;
	CString			m_strFtpRootPath;

	vector<CString> m_arrStrLocalFilePath;	
	vector<CString> m_arrStrFtpFilePath;
	vector<CString> m_arrStrFileName;
	//vector<BOOL>	m_arrBFileUploadCheck;
	double			m_dFolderSize;
	int				m_nFileCount;

	BOOL			m_dTerminateCheck;	
	BOOL			m_bUseFolderList;
	BOOL			m_bUseDeleteAfterTransfer;
};