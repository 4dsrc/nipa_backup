#pragma once

#include <iostream>
#include <fstream>
#include <string>

#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"

using namespace cv;
using namespace std;

class CESMTemplatePoint
{
public:
	CESMTemplatePoint();
	~CESMTemplatePoint(void);

public:
	void initCreateSubWindowAndTargetRect(
		Mat& image,
		Point2f targetPoint, 
		int subWindowWidth,
		int subWindowHeight,
		int targetWidth,
		int targetHeight);	

	Rect2d getSubWindow;
	Rect2d getTargetRegion;
	Rect2d getPaddingRegion;

private:
	void ModifyRegionToBeValid(Mat& image);
	
public:	
	void setSquares(vector<Point2f> inputPoints);
	Mat getHomography(int index);	

	void calcPoint(Mat& homography, vector<Point2f>& currentPoint);

	void squareImport(string path);
	void squareExport(string path, string cameraName, vector<Point2f> points, int cameraCount);
	
	void vectorExport(string path);
	void vectorImport(string path, int frameCount);	

public:
	vector<vector<Point2f>> _squarePoints;
	vector<Mat> _homography;
	vector<cv::Point> _transPoints;

	Rect2d _targetRegion;
	Rect2d _subWindowRegion;
	Rect2d _paddingRegion;

	int subWindowWidth;
	int subWindowHeight;
	int targetWidth;
	int targetHeight;	

public:
	Scalar Coloring(int index);
	void DrawSquare(Mat& frame, vector<Point2f> templatePoint);	
};
