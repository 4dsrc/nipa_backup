////////////////////////////////////////////////////////////////////////////////
//
//	MainFrmMgr.cpp : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-06-21
// @ver.1.0	4DMaker
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "4DMaker.h"
#include "MainFrm.h"
#include "ESMUtil.h"
#include "ESMFunc.h"
//TimeTrace
#include <sys/timeb.h>
#include <time.h>

//REGISTRY
#include "ESMRegistry.h"
#include "ESMIni.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//------------------------------
//! @brief		GetRoot
//! @date		2011-05-19
//! @author	hongsu.jung
//! @note	 	Get Root Path
//------------------------------------------------------------------------------ 
CString CMainFrame::GetRoot(CString str)
{
	CString strFull = _T("");
	strFull.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_HOME), str);
	return strFull;
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2010-11-29 
//! @owner    Lee JungTaek
//! @note
//! @return        
//------------------------------------------------------------------------------
void CMainFrame::PrintLog(int nVerbosity, CString strLog)
{
	if(!strLog.GetLength())
		return;
	if(!m_pLogManager)
		return;
	
	m_pLogManager->OutputLog(nVerbosity, strLog);
	
	if(!nVerbosity)
	{
		PrintError(strLog);
		return;
	}

	if(m_wndOutput.GetSafeHwnd())
	{

		int nFind = 0;
		nFind = strLog.Find(_T("]"));
		if(nFind > 0)
			strLog = strLog.Mid(nFind+1, strLog.GetLength());

		//copy the time to a buffer.
		SYSTEMTIME cur_time; 
		GetLocalTime(&cur_time); 

		CString strLine;
		strLine.Format(_T("[%02d:%02d:%02d:%03ld] "), cur_time.wHour, cur_time.wMinute, cur_time.wSecond, cur_time.wMilliseconds);
		strLine.Append(strLog);
		strLine.Append(_T("\r\n"));
		m_wndOutput.ShowESMOutput(strLine);
		
		//-- 2014/09/04 hongsu
		TRACE(_T("%s"),strLine);
	}
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2010-11-29
//! @owner    Lee JungTaek
//! @note
//! @return        
//------------------------------------------------------------------------------ 
void CMainFrame::PrintError(CString strError)
{
	if(m_pLogManager) 
		m_pLogManager->OutputErrLog(strError);
	else
		return;

	CString strLine = strError;
	if(m_wndOutput.GetSafeHwnd())
	{
		strLine = _T("ERROR: ")+strLine + _T("\r\n");

		// 2012-05-16 eunkyu.park
		struct _timeb timebuffer;
		_ftime(&timebuffer);
		tstring timeline = _tstrtime(_tctime(& (timebuffer.time)));

		CString strTemp = _T("");
		CTime time = CTime::GetCurrentTime();
		time.Format("%H:%M:%S");
		CString strDate = time.Format("%H:%M:%S");
		strTemp.Format(_T("[%s.%03hu] "), strDate, timebuffer.millitm);
		strLine = strTemp + strLine;

		m_wndOutput.ShowESMOutput(strLine); // 해당 라인 Serial View 에 표시
	}
}

//------------------------------------------------------------------------------ 
//! @brief    Creates test case and serial log file.
//! @date     2011-1-4
//! @owner    Lee JungTaek
//! @note     strLogFile : "ST700_TMS_PV_Common_BeautyShot_CaptureSingleOnce_0001"
//! @return        
//------------------------------------------------------------------------------ 
void CMainFrame::CreateNewLogFile(CString strLogFile)
{
	CString strLogFilePath = _T("");

	if(m_pLogManager)
	{
		delete m_pLogManager;
		m_pLogManager= NULL;
	}

	strLogFilePath = strLogFile + _T("_ESM") + _T(".log");
	m_pLogManager = new LogMgr(ESMGetPath(ESM_PATH_LOG_DATE), strLogFilePath, m_ESMOption.m_Log.nLimitday, m_ESMOption.m_Log.nVerbosity);	
	if(m_pLogManager)
	{
		m_pLogManager->Start();
		ESMLog(1, _T("[TC] Create TestCase Log \"%s\""), strLogFilePath);			
	}
}

//------------------------------------------------------------------------------ 
//! @brief		RecenltyFileWriteReg()
//------------------------------------------------------------------------------ 
void CMainFrame::RecenltyFileWriteReg()
{
	int nTotalCnt;
	int nCurrentCnt;
	int nFinalCnt;

	CWinApp* pApp = AfxGetApp();
	int nOpenCnt = pApp->GetProfileInt(REG_ROOT_ADD_RECENTLY, _T("Count"), -1);

	//-- Delete Previous Registry
	for(int i = 0; i < nOpenCnt; i++)
	{
		CString strEntry;
		strEntry.Format(_T("%s%d"), REG_ROOT_RECENTLY, i+1);
		RegDelValue(HKEY_CURRENT_USER, REG_ROOT_FULL_RECENTLY, strEntry);		
	}

	//-- Set Condition (Max : ESM_RECENTLY_REG_SIZE)
	if(m_listRecentlyPath.GetCount() > ESM_RECENTLY_REG_SIZE)
	{	
		nTotalCnt = (int)m_listRecentlyPath.GetCount() - 1;
		nCurrentCnt = nTotalCnt - ESM_RECENTLY_REG_SIZE;
		nFinalCnt = ESM_RECENTLY_REG_SIZE;
	}
	else
	{
		nTotalCnt = (int)m_listRecentlyPath.GetCount();
		nCurrentCnt = 0;
		nFinalCnt = (int)m_listRecentlyPath.GetCount();
	}

	//-- Write Recently File Path Count
	pApp->WriteProfileInt(REG_ROOT_ADD_RECENTLY, _T("Count"), nFinalCnt);

	POSITION pos = m_listRecentlyPath.GetHeadPosition();
	CString strNewItem = _T("");
	for(int i = nCurrentCnt; i < nTotalCnt; i++)
	{
		//-- Write Recently File Path
		CString strEntry;
		strEntry.Format(_T("%s%d"), REG_ROOT_RECENTLY, i + 1);

		CString strRecently = m_listRecentlyPath.GetNext(pos);
		pApp->WriteProfileString(REG_ROOT_ADD_RECENTLY, strEntry, strRecently);	

		//-- 2012-06-13 hongsu@esmlab.com
		//-- Log 
		ESMLog(5,_T("Write Recently Path [%s|%s]"),strEntry,strRecently);
	}

	m_listRecentlyPath.RemoveAll();
}
//------------------------------------------------------------------------------ 
//! @brief		RecenltyFileRemove()
//------------------------------------------------------------------------------ 
void CMainFrame::RecenltyFileRemove()
{
	//-- Get Menu
	CMenu *pMainMenu;
	pMainMenu = m_wndMenuBar.GetMenu();
	if(!pMainMenu)		
		return;

	//-- Init Menu
	CMenu *pSubMenu;
	pSubMenu = pMainMenu->GetSubMenu(ESM_MENUBAR_LOC);
	if(!pSubMenu)	
		return;

	CMenu *pRecentlyMenu;
	pRecentlyMenu = pSubMenu->GetSubMenu(ESM_RECENTLY_MENUBAR_LOC);
	if(!pRecentlyMenu)	
	{
		return;
	}

	
	//-- Remove Recently Items
	for(int i = pRecentlyMenu->GetMenuItemCount(); i > 0; i--)
		pRecentlyMenu->RemoveMenu(i-1, MF_DELETE|MF_REMOVE|MF_BYPOSITION);
	//-- Remove SubMenu
	pSubMenu->DeleteMenu(ESM_RECENTLY_MENUBAR_LOC, MF_DELETE|MF_BYPOSITION);	
}
//------------------------------------------------------------------------------ 
//! @brief		RecentlyFileAdd
//------------------------------------------------------------------------------ 
void CMainFrame::RecentlyFileAdd(CString strFilePath)
{
	/*
	CString strExt = strFilePath;
	strExt.MakeLower();

	//-- Check File Type
	if( strExt.Find(DEF_TP_EXT)	< 0 &&
		strExt.Find(DEF_TS_EXT)	< 0 &&
		strExt.Find(DEF_TC_EXT)	< 0)
	{
		ESMLog(4, _T("Non Supported File Type : %s"), strFilePath);
		return;
	}

	//-- Check Exist Path
	POSITION pos = m_listRecentlyPath.Find(strFilePath);

	//-- Change Position to Head
	if(pos)
	{
		m_listRecentlyPath.RemoveAt(pos);
		m_listRecentlyPath.AddHead(strFilePath);
	}
	else
		m_listRecentlyPath.AddHead(strFilePath);

	//-- Update List
	RecenltyFileWriteReg();
	RecenltyFileRemove();
	RecentlyFileLoad();
	*/
	return;
}
//------------------------------------------------------------------------------ 
//! @brief	   Add MRU(Most Recently Used) files to menu.
//------------------------------------------------------------------------------ 
void CMainFrame::RecentlyFileLoad()
{
	//-- Get "Main" Menu
	CMenu *pMainMenu;
	pMainMenu = m_wndMenuBar.GetMenu();
	if(!pMainMenu)		
		return;

	//-- Get "Window" Menu
	CMenu *pSubMenu;
	pSubMenu = pMainMenu->GetSubMenu(ESM_MENUBAR_LOC);
	if(!pSubMenu)		
		return;	
}
//------------------------------------------------------------------------------
//! @function	RecentlyFilelOpen
//------------------------------------------------------------------------------
//int nRegID = ID_OPEN_RECENTLY_REG1;
void CMainFrame::RecentlyFilelOpen(CMenu * pWindowMenu, CMenu * pViewMenu)
{
	//-- commented out
	if(!pViewMenu)		
		return;
}
//------------------------------------------------------------------------------ 
//! @brief	  Check TesESMuarantee Version
//! @date	  2010-12-6
//! @note
//! @revision	
//------------------------------------------------------------------------------ 
BOOL CMainFrame::CheckVersions()
{
	/*
	BOOL bResult;
	WORD wServerVer[4];
	WORD wLocalVer[4];

	CString strLocalFile;
	CString strServerFile;

	GetAppPath(strLocalFile);
	strServerFile = GetFileServerPath();


	for(int i=0; i<ARRAY_SIZE(szUpdateFileLists); i++)
	{
		CString strFileName = szUpdateFileLists[i]; 
		bResult = GetFileVersion(strLocalFile + strFileName, wLocalVer[0], wLocalVer[1], wLocalVer[2], wLocalVer[3]);
		bResult = GetFileVersion(strServerFile + strFileName, wServerVer[0], wServerVer[1], wServerVer[2], wServerVer[3]);

		if(i == 0) // ESM.exe
		{
			for(int j = 0 ; j < 4; j ++)
				m_wLocalver[j] = wLocalVer[j];
		}

		//-- 2012-3-6 hongsu
		//-- Non Setting File
		if (!wLocalVer[0])
		{
			CString strMsg;
			strMsg.Format(_T("Not Installed SMILE on your PC.\nCheck Install and Config files\n\nPath:%s\nFile:%s"),strLocalFile,strFileName);
			AfxMessageBox(strMsg);
			return FALSE;
		}

		ULONGLONG uLocalVer		= ((ULONGLONG)wLocalVer[0]<<48) | ((ULONGLONG)wLocalVer[1]<<32) | ((ULONGLONG)wLocalVer[2]<<16) | (ULONGLONG)wLocalVer[3];
		ULONGLONG uServerVer	= ((ULONGLONG)wServerVer[0]<<48) | ((ULONGLONG)wServerVer[1]<<32) | ((ULONGLONG)wServerVer[2]<<16) | (ULONGLONG)wServerVer[3];

		if(uLocalVer < uServerVer)
			return TRUE;
	}
	*/

	return FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief		  Get file version of a specified file
//! @date		    2010-8-6
//! @attention	none
//! @revision		2010-8-6. if a specified file does not exist, return the version number "0.0.0.0"
//------------------------------------------------------------------------------
CString CMainFrame::GetVersion()
{
	CString strVer;
	strVer.Format(_T("%d.%d.%d.%d"),m_wLocalver[0],
		m_wLocalver[1],
		m_wLocalver[2],
		m_wLocalver[3]);
	return strVer;
}

void CMainFrame::ConnectMySql()
{
	if(ESMGetValue(ESM_VALUE_CEREMONYUSE))
	{
		char* pDbAddrIp = ESMUtil::CStringToChar(ESMGetCeremony(ESM_VALUE_CEREMONYDBADDRIP));
		char* pDbID = ESMUtil::CStringToChar(ESMGetCeremony(ESM_VALUE_CEREMONYDBID));
		char* pDbPW = ESMUtil::CStringToChar(ESMGetCeremony(ESM_VALUE_CEREMONYDBPW));
		char* pDbName = ESMUtil::CStringToChar(ESMGetCeremony(ESM_VALUE_CEREMONYDBNAME));

		my_bool	bReconnect = 1;
		unsigned int time_out = 5;

		mysql_init(&mysql);
		//jhhan 180716 mysql 자동 재접속
		mysql_options(&mysql, MYSQL_OPT_CONNECT_TIMEOUT, (const char*)&time_out);
		mysql_options(&mysql, MYSQL_OPT_RECONNECT, &bReconnect);
		
		if(!mysql_real_connect(&mysql, pDbAddrIp, pDbID, pDbPW, pDbName ,3306, 0, 0))
		{
			CString strErr;
			strErr = ESMUtil::CharToCString((char*)mysql_error(&mysql));
			ESMLog(5, _T("Failed to connect to databases: Error: %s"), strErr);

			return;
		}
		else
		{
			ESMLog(5, _T("Mysql connect Success\n"));
		}
		

		mysql_query(&mysql,"use project");
		delete[] pDbAddrIp;
		delete[] pDbID;
		delete[] pDbPW;
		delete[] pDbName;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		  Get file version of a specified file
//! @date		    2010-8-6
//! @attention	none
//! @revision		2010-8-6. if a specified file does not exist, return the version number "0.0.0.0"
//------------------------------------------------------------------------------
BOOL CMainFrame::GetFileVersion(CString strFullPath, WORD& wMajor1, WORD& wMajor2, WORD& wMinor1, WORD& wMinor2)
{
	BOOL bResult    = FALSE;
	LPBYTE lpBuffer = NULL;
	DWORD dwHandle;
	DWORD dwSize = GetFileVersionInfoSize(strFullPath, &dwHandle);
	if(dwSize <= 0)
		goto ESMUPDATERDLG_ERROR;

	lpBuffer = new BYTE[dwSize];

	if(FALSE == GetFileVersionInfo(strFullPath, 0, dwSize, lpBuffer))
		goto ESMUPDATERDLG_ERROR;

	UINT cbTranslate = 0;
	VS_FIXEDFILEINFO* pFixedInfo;
	if(!VerQueryValue(lpBuffer, TEXT("\\"), (LPVOID*)&pFixedInfo,	&cbTranslate))
		goto ESMUPDATERDLG_ERROR;

	wMajor1 = HIWORD(pFixedInfo->dwFileVersionMS);
	wMajor2 = LOWORD(pFixedInfo->dwFileVersionMS);
	wMinor1 = HIWORD(pFixedInfo->dwFileVersionLS);
	wMinor2 = LOWORD(pFixedInfo->dwFileVersionLS);

	bResult = TRUE;

ESMUPDATERDLG_ERROR:
	if(lpBuffer)
	{
		delete[] lpBuffer; 
		lpBuffer = NULL;
	}

	if(!bResult)
		wMajor1 = wMajor2 = wMinor1 = wMinor2 = 0;

	return bResult;
}

//-- Get App Path
BOOL CMainFrame::GetAppPath(OUT CString& strAppPath)
{
	CESMRegistry registry;
	CString strPath = REG_ROOT_PATH;

	if(registry.Open(HKEY_LOCAL_MACHINE, strPath))
		registry.Read(_T("Path"), strAppPath);
	//-- 2010-7-30 hongsu.jung
	else
		strAppPath = BASE_APP_PATH;
	return TRUE;
}


//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-10-14
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CMainFrame::LoadRecordProfile(CString strFileName)
{
	//-- 2013-10-02 hongsu@esmlab.com
	//-- Load Record Profile File
	m_FileMgr->LoadRecordProfile(strFileName);

	//-- 2013-12-17 kcd
	//-- Network DisConnet 변경
	//m_wndNetwork.DisConnectAll();

	//-- 2014-06-25 hjjang
	//-- Set Dsc Count
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_MOVIE_SET_MOVIECOUNT;
	pMsg->nParam1	= m_wndDSCViewer.m_arDSCItem.GetSize();
	::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_MOVIE, (LPARAM)pMsg);
	
	//-- 2014-07-15 hongsu@esmlab.com
	//-- Get Movie Path 
	CString strMoviePath, strFile;
	strMoviePath.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord());
	
	//jhhan 16-09-23
	/*START*/
	CESMRCManager* pRCMgr = NULL;
	
	int nNetMgrCount	= 0;
	int nIsRcMgrCount	= 0;
	
	nNetMgrCount = m_wndNetwork.m_arRCServerList.GetCount();

	for ( int i = 0 ;i < nNetMgrCount; i++)
	{
		pRCMgr = (CESMRCManager *)m_wndNetwork.m_arRCServerList.GetAt(i);
		pRCMgr->m_bCheckConnect = FALSE;
		if (pRCMgr->m_bConnected)
		{
			if( pRCMgr->GetServerSocket())
			{
				//pRCMgr->ConnectCheck(ESMGetFrameRate());	//180308 카메라타입 및 해상도 타입 동시전송
				pRCMgr->ConnectCheck(ESMGetIdentify());
				ESMLog(5,_T("Send Agent Check [IP:%d][Time:%d]"), pRCMgr->GetID(), ESMGetTick());

				nIsRcMgrCount++;
			}
			ESMEvent* pMsg = new ESMEvent();
			pMsg->message	= WM_RS_RC_SET_FRAME_RECORD;
			pRCMgr->AddMsg(pMsg);
		}
	}
	/*END*/
	
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;
	for( int i =0 ;i < arDSCList.GetSize(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		strFile.Format(_T("%s\\%s.mp4"), strMoviePath, pItem->GetDeviceDSCID());

		TCHAR* cPath = new TCHAR[1+strFile.GetLength()];
		_tcscpy(cPath, strFile);
		TCHAR* strId = new TCHAR[pItem->GetDeviceDSCID().GetLength() + 1];
		_tcscpy(strId, pItem->GetDeviceDSCID());
		ESMEvent* pMsgBuffer	= new ESMEvent();
		pMsgBuffer->message	= WM_ESM_MOVIE_SET_MOVIEDATA;
		pMsgBuffer->pParam	= (LPARAM)cPath;
		pMsgBuffer->pDest	= (LPARAM)strId;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsgBuffer);
	}

	//-- 2014-07-15 hjjang
	//-- Set Adjust Info
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_MOVIE_SET_ADJUSTINFO;
	pMsg->nParam1 = m_wndDSCViewer.GetMarginX();
	pMsg->nParam2 = m_wndDSCViewer.GetMarginY();
	::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);

}

void CMainFrame::SaveRecordProfile(CString strFileName)
{
	//-- 2013-10-02 hongsu@esmlab.com
	//-- Save Record Profile File 
	m_FileMgr->SaveRecordProfile(strFileName);
}

void CMainFrame::LoadAdjustProfile(CString strFileName)
{
	//-- 2013-10-02 hongsu@esmlab.com
	//-- Load Record Profile File
	m_wndDSCViewer.LoadAdjustProfile(strFileName);
}

void CMainFrame::SaveAdjustProfile(CString strFileName, void* pAdj)
{
	//-- 2013-10-02 hongsu@esmlab.com
	//-- Save Record Profile File
	m_wndDSCViewer.SaveAdjustProfile(strFileName, pAdj);
}

void CMainFrame::SaveSyncTestFile(CString strFileName)
{
	m_wndDSCViewer.SaveSyncTestFile(strFileName);
}

void CMainFrame::SaveFocusValue()
{
	CESMIni ini;

	if(!m_strConfig.GetLength())
		return;

	if(!ini.SetIniFilename (m_strConfig))
		return;
	
	CDSCItem* pItem = NULL;
	int nAll = m_wndDSCViewer.GetItemCount();
	
	while(nAll--)
	{
		pItem = (CDSCItem*)m_wndDSCViewer.GetItemData(nAll);

		ini.WriteInt	(INFO_SECTION_FOCUS, pItem->GetDeviceDSCID()	, pItem->m_nFocus);
	}
}

BOOL CMainFrame::CheckConcert()
{
	if(ESMGetValue(ESM_VALUE_CEREMONYUSE))
	{
		//jhhan 180716 mysql 자동 재접속 - 연결확인
		if(mysql_ping(&mysql) == -1)
		{
			unsigned int errCode = 0;
			errCode = mysql_errno(&mysql);
			ESMLog(0, _T("MySQL err [%d] - %s"), errCode, mysql_error(&mysql));
			if(errCode == 2006)
			{
				ConnectMySql();
			}
		}

		char* sQuery = new char[200];
		strcpy(sQuery, "select Tel1, Email1 from user where CurMovie = \"1\";");
		MYSQL_ROW row;
		MYSQL_RES *m_res = NULL;
		CString strTelNum, strEmail;
		int nRet = mysql_query(&mysql, sQuery);

		m_res = mysql_store_result(&mysql);

		if(m_res == NULL)
		{
			AfxMessageBox(_T("DB Connect Error"));
			ESMSetCeremonyDBConnect(FALSE);
			return TRUE;
		}

		row = mysql_fetch_row(m_res);
		if(row)
		{
			if(((row[0]==NULL)||(!strlen(row[0]))))
			{
				strTelNum = _T("");
				strEmail = _T("");
			}
			else
			{
				strTelNum = ESMUtil::CharToCString(row[0]);
				strEmail = ESMUtil::CharToCString(row[1]);
			}
		}

		CString strTelNum1  = ESMGetCeremonyInfoString(_T("InputTel"));
		if(strTelNum == strTelNum1)
		{
			int nReturn = AfxMessageBox(_T("The same as the previous record. \n Are you sure you want to produce a video?"), MB_YESNO);
			switch(nReturn)
			{
			case IDYES:		
				break;
			default:
				//-- permit multi process
				AfxMessageBox(_T("The video production was canceled."));
				return FALSE;
				break;
			}
		}
		ESMSetCeremonyInfoString(_T("InputTel"), strTelNum);
		ESMSetEventEmail(strEmail);
	}

	ESMSetCeremonyDBConnect(TRUE);
	return TRUE;
}