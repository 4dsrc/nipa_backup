#pragma once

#include "ESMAdjustMgr.h"
#include "ESMRecalibrationImage.h"
#include "ESMFunc.h"
#include "ESMRecalibrationMgr.h"
#include "ESMObjectTracking.h"
#include <vector>
#include "afxwin.h"
#include "afxres.h"
#include "afxcmn.h"

#include "ESMNetworkListCtrl.h"
#include "ESMEditorEX.h"
#include "ESMGroupBox.h"
#include "esmbuttonex.h"

#define TEMPLATE_TEMP_FILE _T("TemplateTemp.pta")

class CESMRecalibrationDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CESMRecalibrationDlg)

public:
	CESMRecalibrationDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMRecalibrationDlg();

// 대화 상자 데이터입니다.
	enum { IDD = ID_IMAGE_RECALIBRATION};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnCustomdrawTcpList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLvnItemchangedAdjust(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);

	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);

	afx_msg void OnBnClickedBtnAdjustPointReset();
	afx_msg void OnBnClickedBtn2ndSavePoint();
	afx_msg void OnBnClickedBtn2ndLoadPoint();		
	afx_msg void OnBnClickedBtnTracking();	
	afx_msg void OnBnClickedBtnModifyAdj();
//	afx_msg void OnBnClicktdBtnSetFrameNum();
		
	virtual BOOL OnInitDialog();	
	virtual void OnOK();
	virtual void OnCancel();	

	static unsigned WINAPI GetMovieDataThread(LPVOID param);	

	void AddRecalibrationMgr(CESMRecalibrationMgr *recalibrationMgr);

	CPoint GetPointFromMouse(CPoint pt);
	void SetMovieState(int nMovieState = ESM_ADJ_FILM_STATE_MOVIE);
	
	void Showimage();

	void DrawImage();  
	void DeleteImage(); 
	void LoadData();
	
	void SavePointData(CString strFileName);
	void LoadPointData(CString strFileName);	

	void SetSquarePointListItem(int nSelectedItem);
	
	CESMNetworkListCtrl m_AdjustDscList;
	CComboBox m_ctrlCheckPoint;
	HANDLE	m_hTheadHandle;
	CEdit m_ctrlCenterLine;	

	int m_nMovieState;
	int m_nSelectedNum;
	int m_nSelectPos;
	int m_RdImageViewColor;
	int m_nView;
	int m_nRdDetectColor;
	int m_nWidth, m_nHeight;
	int m_frameIndex;
	int m_nPosX, m_nPosY;	
	int m_MarginX;
	int m_MarginY;	
	int m_Scale;
	int m_Move;
	int m_Number;		
	BOOL m_bThreadCloseFlag;	
	BOOL m_bStabilizationCloseFlag;
	BOOL m_bLoadAdjustImageAll;
		
	CESMRecalibrationMgr *m_pRecalibrationMgr;	
	CESMRecalibrationImage m_StAdjustImage;	

	CESMObjectTracking *m_pObjectTracking;	
	cv::Rect2d m_pRect;
	BOOL m_bInitializedTracking;

	vector<stAdjustInfo> m_pAdjInfo;
	
public:
	void setDscListItem(CListCtrl* pList, int rowIndex, int colIndex, int data);	
	int getDSCListItem(CListCtrl* pList, int rowIndex, int colIndex);

	CPoint m_pDownPoint;
	CPoint m_pUpPoint;

	void setSquareFromMouse();
	void CalcAdjustData();
	void ModifyAdjustData();
	void getAdjustMoveCoordination();

	vector<Point2f> m_pMovePoint;
	vector<BOOL> m_pImageLoadFlag;
//	CString m_strFrameTime;
	int m_dFrameTime;

	//171018
	int m_nStartSelectedItem;
	int m_nEndSelectedItem;

	afx_msg void OnBnClickedRadioUsingTracking(UINT msg);
	int m_nTrackingOption;	

public:
	CBrush m_background;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CESMButtonEx m_btnLoad;
	CESMButtonEx m_btnSave;
	CESMButtonEx m_btnReset;
	CESMButtonEx m_btnRun;
	CESMButtonEx m_btnSaveCalibration;
	CESMButtonEx m_btnOk;
	CESMButtonEx m_btnCancel;
	virtual BOOL PreTranslateMessage(MSG* pMsg);	
};
