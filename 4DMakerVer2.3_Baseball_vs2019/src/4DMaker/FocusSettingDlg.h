#pragma once


// CFocusSettingDlg 대화 상자입니다.

class CFocusSettingDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CFocusSettingDlg)

public:
	CFocusSettingDlg(int nMinFocus = 0, int nMaxFocus = 0, int nCurrentFocus = 0 , CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFocusSettingDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_FOCUS_SET };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:


	int m_nMinFocus;
	int m_nMaxFocus;
	int m_nCurrentFocus;
};
