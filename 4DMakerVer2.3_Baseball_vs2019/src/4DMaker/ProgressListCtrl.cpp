// ListCtrlEx.cpp : implementation file
//

#include "stdafx.h"
#include "ProgressListCtrl.h"


// CListCtrlEx

IMPLEMENT_DYNAMIC(CProgressListCtrl, CListCtrl)
CProgressListCtrl::CProgressListCtrl() : m_ProgressColumn(0)
{
	
}

CProgressListCtrl::~CProgressListCtrl()
{
	InitProgressList();
}


BEGIN_MESSAGE_MAP(CProgressListCtrl, CListCtrl)
	ON_WM_PAINT()
END_MESSAGE_MAP()



// CListCtrlEx message handlers


void CProgressListCtrl::OnPaint()
{		
	int Top=GetTopIndex();
	int Total=GetItemCount();
	int PerPage=GetCountPerPage();
	int LastItem=((Top+PerPage)>Total)?Total:Top+PerPage;

	if(m_nPercent.size() != LastItem)
	{
		m_nPercent.clear();
		for(int i = 0; i < LastItem; i++)
			m_nPercent.push_back(0);
	}

	// if the count in the list os nut zero delete all the progress controls and them procede
	{
		InitProgressList();	
	}

	CHeaderCtrl* pHeader=GetHeaderCtrl();
	for(int i=Top;i<LastItem;i++)
	{
		CRect ColRt;
		pHeader->GetItemRect(m_ProgressColumn,&ColRt);
		// get the rect
		CRect rt;
		GetItemRect(i,&rt,LVIR_LABEL);
		rt.top+=1;
		rt.bottom-=1;
		rt.left+=ColRt.left;
		int Width=ColRt.Width();
		rt.right=rt.left+Width-4;

		// create the progress control and set their position
		CProgressCtrl* pControl=new CProgressCtrl();
		pControl->Create(NULL,rt,this,IDC_PROGRESS_LIST+i);
		pControl->SetRange(0,100);

		//todo : change value
		CString Data=GetItemText(i,0);
		//int Percent=_ttoi(m_strPercent);

		// set the position on the control
		pControl->SetPos(m_nPercent[i]);
		pControl->ShowWindow(SW_SHOWNORMAL);
		// add them to the list
		m_ProgressList.Add(pControl);
	}
	CListCtrl::OnPaint();
}

void CProgressListCtrl::InitProgressColumn(int ColNum/*=0*/)
{
	m_ProgressColumn=ColNum;
}

void CProgressListCtrl::InitProgressList()
{
	int Count=(int)m_ProgressList.GetCount();
	for(int i=0;i<Count;i++)
	{
		CProgressCtrl* pControl=m_ProgressList.GetAt(0);
		pControl->DestroyWindow();
		delete pControl;
		m_ProgressList.RemoveAt(0);
	}
}

CArray<CProgressCtrl*,CProgressCtrl*>& CProgressListCtrl::GetProgressList()
{
	return m_ProgressList;
}

void CProgressListCtrl::SetProgressPercent(int nIndex, int nPercent)
{
	if(m_nPercent.size() <= nIndex)
		return;
	m_nPercent[nIndex] = nPercent;	
}
