#pragma once
#include "ESMDefine.h"
#include "ESMFunc.h"
#include "ESMIni.h"

#define DB_ADDRESS "192.168.0.25"
#define DB_ID "root"
#define DB_PASS "5esmlab!"
#define DB_NAME "4dlive"

#define DB_PORT 3306

extern MYSQL mysql;

// CESMUtilMultiplexerOptDlg 대화 상자입니다.
#define STRING_MODE _T("mode")
#define STRING_OWN	_T("Owner")

class CESMUtilMultiplexerOptDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CESMUtilMultiplexerOptDlg)

public:
	CESMUtilMultiplexerOptDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMUtilMultiplexerOptDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UTIL_MULTIPLEXER_OPTION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();

	void InitData();
	void CreateNewIni();
	void CreateComboBox();
	CString GetInIString(CString strName);
	CString CheckNamePath(CString strName);
	/*UI*/
	CComboBox m_cbSUBL;
	CString	  m_strSUBL;
	int		  m_nSubL;
	CComboBox m_cbSUBM;
	CString	  m_strSUBM;
	int		  m_nSubM;
	CComboBox m_cbSUBS;
	CString	  m_strSUBS;
	int		  m_nSubS;
	CComboBox m_cbOwn;
	CString	  m_strOwn;
	int		  m_nOwn;

	CString m_strOBJ;

	CString m_strDES;

	CString m_strHomePath;
	CESMIni m_ini;

	afx_msg void OnCbnSelchangeMuxoptComboSubL();
	afx_msg void OnCbnSelchangeMuxoptComboSubM();
	afx_msg void OnBnClickedOk();

	BOOL m_bInILoad;
	afx_msg void OnBnClickedMuxoptBtnSubL();
	afx_msg void OnBnClickedMuxoptBtnSubM();
	afx_msg void OnBnClickedMuxoptBtnSubS();

	void ConnectDB();
	BOOL m_bDBConnect;
	void LoadExistData();
	afx_msg void OnCbnSelchangeMuxoptComboOwner();
};
