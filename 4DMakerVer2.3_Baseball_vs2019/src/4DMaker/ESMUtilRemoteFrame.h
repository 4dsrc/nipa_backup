#pragma once
//#include "ESMUtilRemoteCtrl.h"
#include "ESMUtilRemoteImage.h"
#include "opencv2/core.hpp"
#include "opencv2/opencv_modules.hpp"
#include "opencv2/opencv.hpp"
#include "ESMIndex.h"
#include "ESMDefine.h"

#define FRAMEVIEW

using namespace cv;

#define IMAGEARRAY 20
#define FRAMETOTAL 30
#define TEST
#define FHD
enum{
	RE_PAUSE,
	RE_PREV,
	RE_NEXT
};
enum{
	MODE_PLAY,
	MODE_PAUSE,
	MODE_SELECTOR
};

struct ImageMgr{
	ImageMgr()
	{
		bFinish	  = FALSE;
		nMovIdx   = 0;
		nFrameIdx = 0;
	}
	BOOL bFinish;
	int nMovIdx;
	int nFrameIdx;
	Mat Image;
};
struct ImageArr{
	ImageArr()
	{
		nFrameIdx = -1;
		bFinish	  = FALSE;
	}
	Mat img;
	int nFrameIdx;
	BOOL bFinish;
};
struct ImageSaveIndex{
	ImageSaveIndex()
	{
		nTime	  = -1;
		nMovieIdx = -1;
		nFrameIdx = -1;
	}
	int nTime;
	int nMovieIdx;
	int nFrameIdx;
};
struct ImageLiveMgr{
	ImageLiveMgr()
	{
		nMovidIdx = -1;
		bDecodeFinish = FALSE;
		pImageLiveArr = NULL;
		bDelete		  = FALSE;
	}
	//CString strPath;
	int nMovidIdx;
	BOOL bDecodeFinish;
	BOOL bDelete;
	vector<ImageArr>*pImageLiveArr;
};
class CESMUtilRemoteCtrl;

class CESMUtilRemoteFrame : public CDialog
{
	DECLARE_DYNAMIC(CESMUtilRemoteFrame)

public:
	CESMUtilRemoteFrame(CWnd* pParent = NULL); 
	virtual ~CESMUtilRemoteFrame();

	enum { IDD = IDD_UTIL_REMOTEFRAME };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP()

public: 
	cv::Mat img;
	int m_nRecordFrameCount;
	int m_nImageArrayCount;
	BOOL m_bThreadFinish;
	void SetThreadFinish(BOOL bSet){m_bThreadFinish = bSet;}
	BOOL GetThreadFinish(){return m_bThreadFinish;}

	/*Path Manager*/
	CString				m_strDSCID;
	int					m_DSC;
	int					m_nID;
	void				SetCamID(int nCamID){m_DSC = nCamID;}
	int					GetCamID(){return m_DSC;}
	CString		GetMoviePath(){return m_strArrMoviePath.GetAt(0);}
	void		AddMoviePath(CString strPath){m_strArrMoviePath.Add(strPath);}
	void		RemoveMoviePath(){m_strArrMoviePath.RemoveAt(0);}
	void		RemoveAllPath(){m_strArrMoviePath.RemoveAll();}
	int			GetMovieCount(){return m_strArrMoviePath.GetCount();}
	int			ReadFrameCount(CString strPath);
	
	/*RecordState Manager*/
	BOOL				m_bRecordState;
	BOOL				m_bArrThreadState[THREAD_TOTAL];
	int					m_nFinishTime;
	BOOL				m_bFinishState;
	void SetFinishRecordTime(int nSet){m_nFinishTime = nSet;}
	int	 GetFinishRecordTime(){return m_nFinishTime;}
	void SetRecordState(BOOL bState){m_bRecordState = bState;}
	BOOL GetRecordState(){return m_bRecordState;}

	//Image Select
	void SetRecordFinishState(BOOL bSet){m_bFinishState = bSet;}
	BOOL GetRecordFinishState(){return m_bFinishState;}
	BOOL  FinishCheckPathMgr(int nMovieIdx);

	/*Class*/
	CESMUtilRemoteCtrl* m_pRemoteCtrl;
	CESMRemoteImage		m_Image;
	void Init(CESMUtilRemoteCtrl* pParent){m_pRemoteCtrl = pParent;}

	/*Image */
	Mat					m_matImage;	
	cv::Point2f			m_ptImageCenter;
	cv::Point2f			m_ptImage;
	CRect				m_rcImage;
	BOOL				m_bIsMouseOver;
	void				SetMouseOver(BOOL b){m_bIsMouseOver = b;}
	BOOL				GetMouseOver(){return m_bIsMouseOver;}

	/*UI*/
	CStringArray		m_strArrMoviePath;
	int					m_nCAMID;
	BOOL				m_bMouseOn;
	CRect				m_rcFullSize;
	void				SetFullScreenSize(CRect rc){m_rcFullSize = rc;}
	CRect				GetFullScreenSize(){return m_rcFullSize;}
	void				CalcMovieRatio(CRect rc);
	int					m_nImageWidth;
	int					m_nImageHeight;
	/*Play State*/
	BOOL				m_bIsPause;//Default TRUE, play = TRUE, pause = FALSE;
	BOOL				m_bRunning;
	BOOL				m_bFileExist;
	int					m_nFinishMovieIdx;
	int					m_nFinishFrameIdx;
	int					m_nCurPlayIdx;
	int					m_nSearchIdx;
	int					m_nCurFrameIdx;
	
	CRITICAL_SECTION	m_criImageLoad;

	void SetPlayState(BOOL b){m_bIsPause = b;}
	BOOL GetPlayState(){return m_bIsPause;}
	void SetPlayRunning(BOOL b){m_bRunning = b;}
	BOOL GetPlayRunning(){return m_bRunning;}
	int  GetPlayStatus();
	void  GetNextFrameIdx(int &nMovieIdx,int &nIdx);
	void StackImage(Mat img,int nMovIdx,int nFrameIdx);
	Mat	 LoadImageFrame(int nIdx);

	int  GetFinishFrameIdx(){return m_nFinishFrameIdx;}
	void SetFinishFrameIdx(int nIdx){m_nFinishFrameIdx = nIdx;}
	int  GetFinishMovieIdx(){return m_nFinishMovieIdx;};
	void SetFinishMovieIdx(int nIdx){m_nFinishMovieIdx = nIdx;}	
	int  GetCurPlayIdx(){return m_nCurPlayIdx;}
	void SetCurPlayIdx(int nIdx){m_nCurPlayIdx = nIdx;}
	int	 GetCurFrameIdx(){return m_nCurFrameIdx;}
	void SetCurFrameIdx(int nIdx){m_nCurFrameIdx = nIdx;}
	int GetScrollFrameIdx(int nFrameIdx);
	

	/*Sync Control*/
	int DoSyncCorrector(int nDSCID,int nCurIdx,BOOL bMode=FALSE);
	int m_nSyncFrameIdx;
	void SetSyncFrameIdx(int nIdx){m_nSyncFrameIdx = nIdx;}
	int	 GetSyncFrameIdx(){return m_nSyncFrameIdx;}

	/*Image UI - Screen Show Func & Zoom*/
	BOOL				m_bFullScreen;
	void SetFullScreen(BOOL b){m_bFullScreen = b;}
	BOOL GetFullScreen(){return m_bFullScreen;}
	void ShowScreen(Mat frame,BOOL bColor= TRUE);

	void	SetImageUsingZoomRatio(Mat &img);
	void	GetImageRect(double dbZoomRatio,int nOriWidth,int nOriHeight,int &nLeft,int &nTop,int &nWidth,int &nHeight);

	/*Execute Func*/
	void Run();
	static unsigned WINAPI ThreadImageSelect(LPVOID param);
	static unsigned WINAPI ThreadImageSelect2(LPVOID param);
	static unsigned WINAPI ThreadDecode(LPVOID param);
	static unsigned WINAPI ThreadPathMgr(LPVOID param);

	/*UI Event*/
	//CStatic *m_pstcMovieInfo;
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	/*Memory Mgr*/
	vector<ImageMgr>*	m_pArrImageMgr;
	vector<ImageLiveMgr> m_ArrLiveMgr;
	int GetLiveMgrSize(){return m_ArrLiveMgr.size();}
	int GetLiveMgrFrameCnt(int nIdx)
	{
		return m_ArrLiveMgr.at(nIdx).pImageLiveArr->size();
	}
	ImageLiveMgr GetLiveMgr(int nIdx){return m_ArrLiveMgr.at(nIdx);}
	void AddLiveMgr(ImageLiveMgr LiveMgr)
	{
		m_ArrLiveMgr.push_back(LiveMgr);
	}
	void DeleteLiveMgr(int nIdx,BOOL bAll = FALSE);
	void DeleteAllLiveMgr();
	int m_nLastDelete;
	void SetLastDeleteIdx(int nIdx){m_nLastDelete = nIdx;}
	int  GetLastDeleteIdx(){return m_nLastDelete;}
	Mat LoadImageFrame(int nMovieIdx,int nFrameIdx);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);

	/*Template Manager*/
	int m_nMakingTime;
	void SetMakingTime(int nMovieIdx,int nFrameIdx);
	int  GetMakingTime();
	int  GetCurFrameTime(int nMovieIdx,int nFrameIdx);

	/*Focus State*/
	BOOL m_bFocus;
	void SetIsFocus(BOOL b){m_bFocus = b;}
	BOOL GetIsFocus(){return m_bFocus;}
	
	/*Mouse Click*/
	BOOL m_bMouseClicled;
	void SetMouseClicked(BOOL b){m_bMouseClicled = b;}
	BOOL GetMouseClicked(){return m_bMouseClicled;}
	BOOL m_bRMouseActive;
	void SetRMouseActive(BOOL b){m_bRMouseActive = b;}
	BOOL GetRMouseActive(){return m_bRMouseActive;}

	/*Adjust & TemplatePoint*/
	stAdjustInfo m_stAdjust;
	int m_nMarginX;
	int m_nMarginY;
	int m_nSrcWidth;
	int m_nSrcHeight;
	//double m_dbRatioW;
	//double m_dbRatioH;
	//double m_dbMarScaleW;
	//double m_dbMarScaleH;
	double m_dbRatio;
	double m_dbMarginScale;
	int m_nLeft;
	int m_nTop;
	BOOL m_bAdjustLoad;
	int Round(double dData);
	void SetAdjustInfo(CString strDSC);
	Mat CreateAdjustImage(Mat img,int nSrcWidth,int nSrcHeight,int nDlgWidth,int nDlgHeight,BOOL bReverse=FALSE);
	void CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle, BOOL bReverse);
	void CpuMoveImage(Mat* gMat, int nX, int nY);
	
	/*Image Select Point*/
	vector<ImageSaveIndex>m_stArrSaveIdx;
	int GetSaveIndexSize(){return m_stArrSaveIdx.size();}
	void ClearSaveIndex(){m_stArrSaveIdx.clear();m_nSelectPoint = -1;}
	int GetMovieIndexFromArr(int n){return m_stArrSaveIdx.at(n).nMovieIdx;}
	int GetFrameIndexFromArr(int n){return m_stArrSaveIdx.at(n).nFrameIdx;}

	int m_nCalcPoint;
	int m_nSelectPoint;
	int GetSelectorNum(){return m_nCalcPoint;}
	void SetSelectorNum(int n){m_nCalcPoint = n;}
	BOOL m_bSelectLoad;
	void SetSelectLoad(BOOL b){m_bSelectLoad = b;}
	BOOL GetSelectLoad(){return m_bSelectLoad;}

	void GetNextOrPrevFrame(int &nMovIdx,int &nFrmIdx,int nBaseFrame);
	/*Size Load Critical Section*/
	//CRITICAL_SECTION m_criOnSize;
	
	//wgkim 171126
	CPoint GetImageFrameSize();
	void DrawViewWindowAndCenterLine(Mat *img);
	int fixXValueWhenShiftKeyIsPressed;
	BOOL m_bIsPressedRButton;
	/*Zoom In & out*/
	int m_nZoomRatio;

	/**/
	void ShowScreenIfPauseState();

	BOOL m_bShowFrame;
	void SetShowFrame(BOOL b){m_bShowFrame = b;}
	BOOL GetShowFrame(){return m_bShowFrame;}
	BOOL m_bActiveFrame;
	void SetItAsFocus();
};
struct ThreadSaveImage
{
	ThreadSaveImage()
	{
		pParent = NULL;
		nSaveIdx = -1;
		nTime  = -1;
	}
	CESMUtilRemoteFrame* pParent;
	int nSaveIdx;
	int nTime;
};
struct ThreadFrameData
{
	ThreadFrameData()
	{
		pParent		= NULL;
		nFrameIdx	= -1;
		nMovIdx		= -1;
		bFinish		= FALSE;
	}
	CESMUtilRemoteFrame* pParent;
	int					 nFrameIdx;
	int					 nMovIdx;
	VideoCapture		 vc;
	CString				 strPath;
	BOOL				 bFinish;
};