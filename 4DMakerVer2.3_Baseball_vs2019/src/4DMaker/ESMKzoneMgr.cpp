#include "stdafx.h"
#include "ESMCtrl.h"
#include "ESMKzoneMgr.h"
#include "ESMFileOperation.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

ESMKzoneMgr::ESMKzoneMgr(void)
{
}


ESMKzoneMgr::~ESMKzoneMgr(void)
{
	DscClear();
}


void ESMKzoneMgr::AddDscInfo(CString strDscId, CString strDscIp,BOOL bReverse/* = FALSE*/)
{
	DscAdjustInfo* pDscAdjustData;
	pDscAdjustData = new DscAdjustInfo;
	pDscAdjustData->strDscName = strDscId;
	pDscAdjustData->strDscIp = strDscIp;
	pDscAdjustData->bAdjust = FALSE;
	pDscAdjustData->bRotate = FALSE;
	pDscAdjustData->bMove = FALSE;
	pDscAdjustData->bImgCut = FALSE;
	pDscAdjustData->bReverse = bReverse;
	m_ArrDscInfo.push_back(pDscAdjustData);
}

int ESMKzoneMgr::GetDscCount()
{
	return m_ArrDscInfo.size();
}

DscAdjustInfo* ESMKzoneMgr::GetDscAt(int nIndex)
{
	if(m_ArrDscInfo.size() > nIndex)
	{
		return m_ArrDscInfo.at(nIndex);
	}
	return NULL;
}

void ESMKzoneMgr::DscClear()
{
	DscAdjustInfo* pDscAdjustData;
	for( int i =0 ;i < m_ArrDscInfo.size(); i++)
	{
		pDscAdjustData = m_ArrDscInfo.at(i);
		if( pDscAdjustData->pBmpBits != NULL)
		{
			delete[] pDscAdjustData->pBmpBits;
			pDscAdjustData->pBmpBits = NULL;
		}
		if( pDscAdjustData->pGrayBmpBits != NULL)
		{
			delete[] pDscAdjustData->pGrayBmpBits;
			pDscAdjustData->pGrayBmpBits = NULL;
		}

		delete pDscAdjustData;
		pDscAdjustData = NULL;
	}
	m_ArrDscInfo.clear();
}


BOOL ESMKzoneMgr::GetSearchDetectPoint(DscAdjustInfo* pAdjustInfo, int nThreshold, CSize minDetectSize, CSize maxDetectSize, int nPointGapMin, COLORREF clrDetectColor)
{
	if( pAdjustInfo == NULL || pAdjustInfo->nWidht == 0 ||  pAdjustInfo->nHeight == 0)
		return FALSE;

	int nChannel = 3;
	IplImage *pImage, *pGray;
	pImage = cvCreateImage(cvSize(pAdjustInfo->nWidht, pAdjustInfo->nHeight), IPL_DEPTH_8U, nChannel);
	memcpy(pImage->imageData, pAdjustInfo->pBmpBits, pAdjustInfo->nWidht * pAdjustInfo->nHeight * nChannel);
	if(!pImage)
		return FALSE;

	pGray = cvCreateImage(cvGetSize(pImage), IPL_DEPTH_8U, 1);
	if(!pGray)
	{
		cvReleaseImage(&pImage);
		cvReleaseImage(&pGray);
		return FALSE;
	}

	cvCvtColor(pImage, pGray, cv::COLOR_RGB2GRAY);
	cvThreshold(pGray, pGray, nThreshold, 255 , CV_THRESH_BINARY);

	vector<CRect> vecDetectAreas;
	
	SearchDetectPoint(pGray, minDetectSize, maxDetectSize, &vecDetectAreas, clrDetectColor);

	IplImage* pGrayImg = cvCreateImage(cvSize(pGray->width, pGray->height), IPL_DEPTH_8U, nChannel);
	cvCvtColor(pGray, pGrayImg, CV_GRAY2RGB);
	pAdjustInfo->pGrayBmpBits = new BYTE[pGrayImg->height * pGrayImg->widthStep];

	if( vecDetectAreas.size() < 3)
	{
		memcpy(pAdjustInfo->pGrayBmpBits, pGrayImg->imageData, pGrayImg->height * pGrayImg->widthStep);
		cvReleaseImage(&pImage);
		cvReleaseImage(&pGray);
		cvReleaseImage(&pGrayImg);
		return FALSE;
	}

	if(vecDetectAreas[1].Width() > 1 && vecDetectAreas[1].Height() > 1)
	{
		pAdjustInfo->recMiddle = vecDetectAreas[1];
		pAdjustInfo->MiddlePos = vecDetectAreas[1].CenterPoint();
	}
	if(vecDetectAreas[0].Width() > 1 && vecDetectAreas[0].Height() > 1)
	{
		if(pAdjustInfo->MiddlePos.y - vecDetectAreas[0].CenterPoint().y > nPointGapMin)
		{
			pAdjustInfo->recHigh = vecDetectAreas[0];
			pAdjustInfo->HighPos = vecDetectAreas[0].CenterPoint();
		}
		else
			vecDetectAreas[0].SetRect(0,0,0,0);

	}
	if(vecDetectAreas[2].Width() > 1 && vecDetectAreas[2].Height() > 1)
	{
		if(vecDetectAreas[2].CenterPoint().y - pAdjustInfo->MiddlePos.y > nPointGapMin)
		{
			pAdjustInfo->recLow = vecDetectAreas[2];
			pAdjustInfo->LowPos = vecDetectAreas[2].CenterPoint();
		}
		else
			vecDetectAreas[2].SetRect(0,0,0,0);
	}

	for(int i =0 ;i < vecDetectAreas.size(); i++)
	{
		if(vecDetectAreas[i].CenterPoint().x != 0)
			cvRectangle(pGrayImg, cvPoint(vecDetectAreas[i].left, vecDetectAreas[i].top), cvPoint(vecDetectAreas[i].right, vecDetectAreas[i].bottom), cvScalar(0, 0, 255), 2, 8, 0);
	}
	if( vecDetectAreas.size() >= 3)
	{
		if((vecDetectAreas[0].Width() > 1 && vecDetectAreas[0].Height() > 1) && (vecDetectAreas[2].Width() > 1 && vecDetectAreas[2].Height() > 1))
			cvLine(pGrayImg, cvPoint(vecDetectAreas[0].CenterPoint().x, vecDetectAreas[0].CenterPoint().y), cvPoint(vecDetectAreas[2].CenterPoint().x, vecDetectAreas[2].CenterPoint().y), cvScalar(0, 0, 255), 2, 8, 0);
	}
	memcpy(pAdjustInfo->pGrayBmpBits, pGrayImg->imageData, pGrayImg->height * pGrayImg->widthStep);

	cvReleaseImage(&pImage);
	cvReleaseImage(&pGray);
	cvReleaseImage(&pGrayImg);
	

	return TRUE;
}

/*
BOOL ESMAdjustMgr::GetSearchPoint(DscAdjustInfo* pAdjustInfo, int nThreshold, int nChannel)
{
	if( pAdjustInfo == NULL)
		return FALSE;

	IplImage *pImage, *pGray;
	pImage = cvCreateImage(cvSize(pAdjustInfo->nWidht, pAdjustInfo->nHeight), IPL_DEPTH_8U, nChannel);
	memcpy(pImage->imageData, pAdjustInfo->pBmpBits, pAdjustInfo->nWidht * pAdjustInfo->nHeight * nChannel);

	pAdjustInfo->pGrayBmpBits = new BYTE[pAdjustInfo->nWidht * pAdjustInfo->nHeight];
	if(!pImage)
		return FALSE;

	pGray = cvCreateImage(cvGetSize(pImage), IPL_DEPTH_8U, 1);
	if(!pGray)
	{
		cvReleaseImage(&pImage);
		cvReleaseImage(&pGray);
		return FALSE;
	}

	cvCvtColor(pImage, pGray, cv::COLOR_RGB2GRAY);
	cvThreshold(pGray, pGray, nThreshold, 255 , CV_THRESH_BINARY);

	vector<CPoint> arrBeginPoint;
	vector<CPoint> arrCenterPoint;
	vector<CPoint> arrPointRange;
	CPoint TpBeginP, PointRange;
	TpBeginP.x = ESMGetValue(ESM_VALUE_ADJ_POINTX);
	TpBeginP.y = ESMGetValue(ESM_VALUE_ADJ_FIRSTPOINTY);
	arrBeginPoint.push_back(TpBeginP);

	TpBeginP.y = ESMGetValue(ESM_VALUE_ADJ_FIRSTPOINTY) + ESMGetValue(ESM_VALUE_ADJ_FIRSTYRANGE);
	arrBeginPoint.push_back(TpBeginP);

	TpBeginP.y = ESMGetValue(ESM_VALUE_ADJ_FIRSTPOINTY) + ESMGetValue(ESM_VALUE_ADJ_FIRSTYRANGE) + ESMGetValue(ESM_VALUE_ADJ_SECONDYRANGE);
	arrBeginPoint.push_back(TpBeginP);

	PointRange.x = ESMGetValue(ESM_VALUE_ADJ_XRANGE);
	PointRange.y = ESMGetValue(ESM_VALUE_ADJ_FIRSTYRANGE);
	arrPointRange.push_back(PointRange);
	PointRange.y = ESMGetValue(ESM_VALUE_ADJ_SECONDYRANGE);
	arrPointRange.push_back(PointRange);
	PointRange.y = ESMGetValue(ESM_VALUE_ADJ_THIRDYRANGE);
	arrPointRange.push_back(PointRange);

	SearchPoint(pGray, &arrBeginPoint, &arrPointRange, &arrCenterPoint);


	for(int i =0 ;i < arrBeginPoint.size(); i++)
		cvRectangle(pGray, cvPoint(arrBeginPoint.at(i).x, arrBeginPoint.at(i).y), cvPoint(arrBeginPoint.at(i).x + arrPointRange.at(i).x, arrBeginPoint.at(i).y + arrPointRange.at(i).y), cvScalar(0, 0, 0, 0), 1, 8, 0);

	if( arrCenterPoint.size() < 3)
	{
		memcpy(pAdjustInfo->pGrayBmpBits, pGray->imageData, pAdjustInfo->nWidht * pAdjustInfo->nHeight);
		return FALSE;
	}

	pAdjustInfo->HighPos = arrCenterPoint.at(0);
	pAdjustInfo->MiddlePos = arrCenterPoint.at(1);
	pAdjustInfo->LowPos = arrCenterPoint.at(2);

	if( arrCenterPoint.size() > 2)
		cvLine(pGray, cvPoint(arrCenterPoint.at(0).x, arrCenterPoint.at(0).y), cvPoint(arrCenterPoint.at(2).x, arrCenterPoint.at(2).y), cvScalar(0, 0, 0, 0), 1, 8, 0);

	memcpy(pAdjustInfo->pGrayBmpBits, pGray->imageData, pAdjustInfo->nWidht * pAdjustInfo->nHeight);

	cvReleaseImage(&pImage);
	cvReleaseImage(&pGray);

	return TRUE;
}
*/

// 2014-01-07 kcd 
// Distance 임시 함수
double ESMKzoneMgr::GetDistanceData(CString strDSCId)
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\DSCDistance.csv"), ESMGetPath(ESM_PATH_SETUP));
	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		return FALSE;
	}

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());
	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;
	CString strTpValue1, strTpValue2;
	double nDistance = 0;
	while (iPos  < iLength )
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;
		AfxExtractSubString(strTpValue1, sLine, 0, ',');
		AfxExtractSubString(strTpValue2, sLine, 1, ',');
		if( strTpValue1 == strDSCId)
		{
			nDistance = _ttof(strTpValue2);
			break;
		}
	}
	return nDistance;
}

int ESMKzoneMgr::GetZoomData(CString strDSCId)
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\DSCZoom.csv"), ESMGetPath(ESM_PATH_SETUP));
	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		return FALSE;
	}

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());
	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;
	CString strTpValue1, strTpValue2;
	double nDistance = 0;
	while (iPos  < iLength )
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;
		AfxExtractSubString(strTpValue1, sLine, 0, ',');
		AfxExtractSubString(strTpValue2, sLine, 1, ',');
		if( strTpValue1 == strDSCId)
		{
			nDistance = _ttoi(strTpValue2);
			break;
		}
	}
	return nDistance;
}

BOOL ESMKzoneMgr::CalcAdjustData(DscAdjustInfo* pAdjustInfo, int nTargetLenth, int nZoom)
{
	//회전
	pAdjustInfo->dAngle = atan2((double)(pAdjustInfo->LowPos.y - pAdjustInfo->HighPos.y), 
		(double)(pAdjustInfo->LowPos.x - pAdjustInfo->HighPos.x)) * (180/3.141592);
	
	//Size
	double dTargetDistance = 0;
	double dTargetLength = 0;

	if( pAdjustInfo->dDistance == 0.0)
		dTargetDistance = GetDistanceData(pAdjustInfo->strDscName);
	else
		dTargetDistance = pAdjustInfo->dDistance;
	
	int nPixelDefaultSize = 0;
	if( pAdjustInfo->nWidht == 1920)
		nPixelDefaultSize = DEFAULT_PIXEL1920_DISTANCE;
	else if( pAdjustInfo->nWidht == 5472)
		nPixelDefaultSize = DEFAULT_PIXEL5472_DISTANCE;
	else if( pAdjustInfo->nWidht == 2200 && pAdjustInfo->nHeight == 1238)
		nPixelDefaultSize = DEFAULT_PIXEL5472_3080_DISTANCE;
	else if( pAdjustInfo->nWidht == 3840 && pAdjustInfo->nHeight == 2160)
		nPixelDefaultSize = DEFAULT_PIXEL3840_2160_DISTANCE;
	else
		nPixelDefaultSize = DEFAULT_PIXEL1920_DISTANCE;

	dTargetLength = (double)nTargetLenth * nPixelDefaultSize  / DEFAULT_TARGETLENGTH;
	if( dTargetDistance != 0.0)
		dTargetLength = dTargetLength / dTargetDistance;
	else
		dTargetLength =1.0;

	if(nZoom == 0)
		nZoom = GetZoomData(pAdjustInfo->strDscName);

	dTargetLength = dTargetLength * nZoom /DEFAULT_CAMERAZOOM;

	int nHeight = pAdjustInfo->LowPos.y - pAdjustInfo->HighPos.y;
	int nWidth = abs(pAdjustInfo->LowPos.x - pAdjustInfo->HighPos.x);
	if( nHeight != 0 || nWidth != 0 )
	{
		double dTpResize = 0.0;
		if( nWidth != 0)
			dTpResize = sqrt(double(nHeight * nHeight + nWidth * nWidth));
		else
			dTpResize = nHeight;

// 		if ( ESMGetValue(ESM_VALUE_ADJ_RESIZE_USE) )
// 			pAdjustInfo->dScale = dTargetLength / dTpResize ;
// 		else
// 			pAdjustInfo->dScale = 1;
		pAdjustInfo->dScale = dTargetLength / dTpResize ;

		//CenterPoint
		pAdjustInfo->dRotateX = pAdjustInfo->dAdjustX = pAdjustInfo->MiddlePos.x;
		pAdjustInfo->dRotateY = pAdjustInfo->dAdjustY = pAdjustInfo->MiddlePos.y;
		pAdjustInfo->dAngle = pAdjustInfo->dAngle * -1;
		pAdjustInfo->dDistance = dTargetDistance;
		return TRUE;
	}
	return FALSE;
}

BOOL ESMKzoneMgr::SearchDetectPoint(IplImage* pNewGray, CSize MinSize, CSize MaxSize, vector<CRect>* vecDetectAreas, COLORREF clrDetectColor)
{
	CPoint pointCenter = CPoint(pNewGray->width / 2, pNewGray->height / 2);
	vector<CRect> vecCenters;
	BOOL bStart = FALSE, bEnd = FALSE;
	int nStartPos = 0, nEndPos = 0;
	int nStartCenterArea = (pNewGray->height / 2) - ((pNewGray->height / 5) / 2);
	int nEndCenterArea = (pNewGray->height / 2) + ((pNewGray->height / 5) / 2);
	int nPosYGap = (MaxSize.cy / 2) + 10;

	int nFindColor;
	if(clrDetectColor == COLOR_WHITE)
		nFindColor = 0;
	else
		nFindColor = 255;

	for(int nFindY = 0; nFindY < pNewGray->height; nFindY++)
	{
		if((unsigned char)pNewGray->imageData[(nFindY * pNewGray->widthStep) + pointCenter.x] == nFindColor)
		{
			if(!bStart)	bStart = TRUE;
			else if(bEnd)
			{
				nEndPos = nFindY - 1;
				bEnd = FALSE;
				bStart = FALSE;
				if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
				{
					CRect recDetect(pointCenter.x, nStartPos, pointCenter.x, nEndPos);
					vecCenters.push_back(recDetect);
				}
			}
		}
		else
		{
			if(bStart && !bEnd)
			{
				nStartPos = nFindY;
				bEnd = TRUE;
			}
		}
	}
	if(vecCenters.size() < 3)
	{
		vecCenters.clear();
		bStart = FALSE;
		bEnd = FALSE;
		nStartPos = 0;
		nEndPos = 0;
		for(int nFindX = pointCenter.x - 1; nFindX > (pointCenter.x - MaxSize.cx); nFindX--)
		{
			vecCenters.clear();
			for(int nFindY = 0; nFindY < pNewGray->height; nFindY++)
			{
				if((unsigned char)pNewGray->imageData[(nFindY * pNewGray->widthStep) + nFindX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nFindY - 1;
						bEnd = FALSE;
						bStart = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							CRect recDetect(nFindX, nStartPos, nFindX, nEndPos);
							vecCenters.push_back(recDetect);
						}
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nFindY;
						bEnd = TRUE;
					}
				}
			}
			if(vecCenters.size() >= 3)
				break;
		}
	}
	if(vecCenters.size() < 3)
	{
		vecCenters.clear();
		bStart = FALSE;
		bEnd = FALSE;
		nStartPos = 0;
		nEndPos = 0;
		for(int nFindX = pointCenter.x + 1; nFindX < (pointCenter.x + MaxSize.cx); nFindX++)
		{
			vecCenters.clear();
			for(int nFindY = 0; nFindY < pNewGray->height; nFindY++)
			{
				if((unsigned char)pNewGray->imageData[(nFindY * pNewGray->widthStep) + nFindX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nFindY - 1;
						bEnd = FALSE;
						bStart = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							CRect recDetect(nFindX, nStartPos, nFindX, nEndPos);
							vecCenters.push_back(recDetect);
						}
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nFindY;
						bEnd = TRUE;
					}
				}
			}
			if(vecCenters.size() >= 3)
				break;
		}
	}
	if(vecCenters.size() < 3)
		return FALSE;

	//Center에 가장 가까운 영역을 찾는다. 
	CRect recCenter = vecCenters[0];
	int nCenterMargin = pointCenter.y - recCenter.CenterPoint().y;
	if(nCenterMargin < 0) nCenterMargin *= -1;
	for(int nIdx = 1; nIdx < vecCenters.size(); nIdx++)
	{
		int nMargin = pointCenter.y - vecCenters[nIdx].CenterPoint().y;
		if(nMargin < 0) nMargin *= -1;
		if(nMargin < nCenterMargin)
		{
			nCenterMargin = nMargin;
			recCenter =  vecCenters[nIdx];
		}
	}

	if(recCenter.CenterPoint().y < nStartCenterArea || recCenter.CenterPoint().y > nEndCenterArea)
		return FALSE;

	//First와 Third 영역을 찾는다. 
	CRect recFirst = recCenter;
	CRect recThird = recCenter;
	int nFirstMargin = 0, nThirdMargin = 0;
	for(int nIdx = 0; nIdx < vecCenters.size(); nIdx++)
	{
		if(recCenter == vecCenters[nIdx])
			continue;
		int nMargin = recCenter.CenterPoint().y - vecCenters[nIdx].CenterPoint().y;
		if(nMargin < 0)
		{
			nMargin *= -1;
			if(nThirdMargin == 0)
			{
				nThirdMargin = nMargin;
				recThird = vecCenters[nIdx];
			}
			else if(nMargin < nThirdMargin)
			{
				nThirdMargin = nMargin;
				recThird = vecCenters[nIdx];
			}
		}	
		else
		{
			if(nFirstMargin == 0)
			{
				nFirstMargin = nMargin;
				recFirst = vecCenters[nIdx];
			}
			else if(nMargin < nFirstMargin)
			{
				nFirstMargin = nMargin;
				recFirst = vecCenters[nIdx];
			}
		}
	}

	vecCenters.clear();
	vecCenters.push_back(recFirst);
	vecCenters.push_back(recCenter);
	vecCenters.push_back(recThird);

	
	for(int nAreaIdx = 0; nAreaIdx < vecCenters.size(); nAreaIdx++)
	{
		//Left 시작 지점 찾는다.
		int nLeft = vecCenters[nAreaIdx].left;
		int nTop = vecCenters[nAreaIdx].top;
		int nRight = vecCenters[nAreaIdx].right;
		int nBottom = vecCenters[nAreaIdx].bottom;
		BOOL bDetectStop = FALSE;
		CRect recDetectArea = vecCenters[nAreaIdx];
		vector<int> vecHeightAvg;
		vector<CRect> vecAreaList;
		vecAreaList.push_back(vecCenters[nAreaIdx]);
		vecHeightAvg.push_back(nBottom - nTop);
		for(int nPosX = vecCenters[nAreaIdx].CenterPoint().x - 1; nPosX > vecCenters[nAreaIdx].CenterPoint().x - MaxSize.cx; nPosX--)
		{
			if(nPosX > pNewGray->width) break;
			int nPosY = vecCenters[nAreaIdx].CenterPoint().y - nPosYGap;
			if(nPosY < 0) nPosY = 0;
			if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] != nFindColor)
				break;

			bStart = FALSE;
			bEnd = FALSE;
			nStartPos = 0;
			nEndPos = 0;

			int nHeightAvg = 0, nHeightSum = 0, nAvgIdxCnt = vecHeightAvg.size();
			for(int nAvgIdx = 0; nAvgIdx < nAvgIdxCnt; nAvgIdx++)
				nHeightSum += vecHeightAvg[nAvgIdx];
			nHeightAvg = nHeightSum / nAvgIdxCnt;

			for(; nPosY < vecCenters[nAreaIdx].CenterPoint().y + nPosYGap; nPosY++)
			{
				if(nPosY > pNewGray->height) break;
				if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nPosY - 1;
						bStart = FALSE;
						bEnd = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							int nHeight = nEndPos - nStartPos;
							if(nHeight >= nHeightAvg - 10 &&  nHeight <= nHeightAvg + 10)
							{
								nLeft = nPosX;
								if(nStartPos > nTop)	nTop = nStartPos;
								if(nEndPos < nBottom)	nBottom = nEndPos;
								recDetectArea.SetRect(nLeft, nTop, nRight, nBottom);
								vecHeightAvg.push_back(nHeight);
								vecAreaList.push_back(CRect(nPosX, nStartPos, nPosX, nEndPos));
							}
						}
						else
							bDetectStop = TRUE;
						break;
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nPosY;
						bEnd = TRUE;
					}
				}
			}
			if(bDetectStop)
				break;
		}
		//Right 마지막 지점 찾는다.
		bDetectStop = FALSE;
		for(int nPosX = vecCenters[nAreaIdx].CenterPoint().x + 1; nPosX < vecCenters[nAreaIdx].CenterPoint().x + MaxSize.cx; nPosX++)
		{
			if(nPosX > pNewGray->width) break;
			int nPosY = vecCenters[nAreaIdx].CenterPoint().y - nPosYGap;
			if(nPosY < 0) nPosY = 0;
			if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] != nFindColor)
				break;
			bStart = FALSE;
			bEnd = FALSE;
			nStartPos = 0;
			nEndPos = 0;

			int nHeightAvg = 0, nHeightSum = 0, nAvgIdxCnt = vecHeightAvg.size();
			for(int nAvgIdx = 0; nAvgIdx < nAvgIdxCnt; nAvgIdx++)
				nHeightSum += vecHeightAvg[nAvgIdx];
			nHeightAvg = nHeightSum / nAvgIdxCnt;

			for(; nPosY < vecCenters[nAreaIdx].CenterPoint().y + nPosYGap; nPosY++)
			{
				if(nPosY > pNewGray->height) break;
				if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nPosY - 1;
						bStart = FALSE;
						bEnd = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							int nHeight = nEndPos - nStartPos;
							if(nHeight >= nHeightAvg - 10 &&  nHeight <= nHeightAvg + 10)
							{
								nRight = nPosX;
								if(nStartPos > nTop)	nTop = nStartPos;
								if(nEndPos < nBottom)	nBottom = nEndPos;
								recDetectArea.SetRect(nLeft, nTop, nRight, nBottom);
								vecHeightAvg.push_back(nHeight);
								vecAreaList.push_back(CRect(nPosX, nStartPos, nPosX, nEndPos));
							}
						}
						else
							bDetectStop = TRUE;
						break;
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nPosY;
						bEnd = TRUE;
					}
				}
			}
			if(bDetectStop)
				break;
		}
		if(recDetectArea.Width() < 5)
		{
			recDetectArea.SetRect(0, 0, 0, 0);
		}
		else
		{
			int nCenterX = recDetectArea.CenterPoint().x;
			for(int nXIdx = 0; nXIdx < vecAreaList.size(); nXIdx++)
			{
				if(vecAreaList[nXIdx].left == nCenterX)
				{
					nLeft = recDetectArea.left;
					nTop = vecAreaList[nXIdx].top;
					nRight = recDetectArea.right;
					nBottom = vecAreaList[nXIdx].bottom;
					recDetectArea.SetRect(nLeft, nTop, nRight, nBottom);
					break;
				}
			}
		}
		vecDetectAreas->push_back(recDetectArea);
	}
	return TRUE;
}

BOOL ESMKzoneMgr::SearchPoint(IplImage*	pNewGray, vector<CPoint>* arrBeginPoint, vector<CPoint>* arrPointRange, vector<CPoint>* pArrPoint)
{
	CPoint TpBeginP;
	CPoint TpPoints;
	if( pNewGray->width < arrBeginPoint->at(2).x + arrPointRange->at(2).x)
		return FALSE;
	if( pNewGray->height < arrBeginPoint->at(2).y + arrPointRange->at(2).y)
		return FALSE;

	for( int i = 0; i< arrBeginPoint->size(); i++)
	{
		CPoint CenterPoint;
		CPoint tpPoint;
		vector<CPoint> arrPoint;
		int x1 = 0;
		int nImageWidth	= pNewGray->width;
		int nImageHeight = pNewGray->height;
		TpPoints = arrPointRange->at(i);

		for(int y1 = arrBeginPoint->at(i).y ; y1 < arrBeginPoint->at(i).y + TpPoints.y ; y1++)
		{
			for(x1 = arrBeginPoint->at(i).x ; x1 < arrBeginPoint->at(i).x + TpPoints.x ; x1++)
			{
				if((unsigned char)pNewGray->imageData[y1*pNewGray->widthStep + x1] < 10)// 흑을 만난다면 Continue;
				{
					tpPoint.x = x1;
					tpPoint.y = y1;
					arrPoint.push_back(tpPoint);
					break;
				}
			}
			if( x1 == arrBeginPoint->at(i).x + TpPoints.x)
				continue;

			//Detect Right Line
			for(x1 = arrBeginPoint->at(i).x + TpPoints.x ; x1 > arrBeginPoint->at(i).x ; x1--)
			{
				if((unsigned char)pNewGray->imageData[y1*pNewGray->widthStep + x1] < 10)// 흑색을 만난다면 Continue;
				{
					tpPoint.x = x1;
					tpPoint.y = y1;
					arrPoint.push_back(tpPoint);
					break;
				}
			}
		}
		if( arrPoint.size() < 1)
			return FALSE;
		int dMinX = arrPoint.at(0).x, dMinY = arrPoint.at(0).y, dMaxX = arrPoint.at(0).x, dMaxY = arrPoint.at(0).y;
		for(int nIndex =0 ;nIndex < arrPoint.size() ; nIndex++)
		{
			if( arrPoint.at(nIndex).x < dMinX)
				dMinX = arrPoint.at(nIndex).x;
			else if( arrPoint.at(nIndex).y < dMinY)
				dMinY = arrPoint.at(nIndex).y;
			else if( arrPoint.at(nIndex).x > dMaxX)
				dMaxX = arrPoint.at(nIndex).x;
			else if( arrPoint.at(nIndex).x > dMaxY)
				dMaxY = arrPoint.at(nIndex).y;
		}
		TpBeginP.x = (dMinX + dMaxX) / 2;
		TpBeginP.y = (dMinY + dMaxY) / 2;
		pArrPoint->push_back(TpBeginP);

	}
	return TRUE;
}

void ESMKzoneMgr::GetDscInfo(vector<DscAdjustInfo*>** pArrDscInfo)
{
	*pArrDscInfo = &m_ArrDscInfo;
}

void ESMKzoneMgr::GetResolution(int &nWidth, int &nHeight)
{
	DscAdjustInfo* pAdjustInfo = NULL;
	CESMFileOperation fo;
	for(int i =0; i< GetDscCount(); i++)
	{
		pAdjustInfo = GetDscAt(i);

		FFmpegManager FFmpegMgr(FALSE);
		CString strFolder, strDscId, strDscIp;
		strDscId = pAdjustInfo->strDscName;
		strDscIp = pAdjustInfo->strDscIp;
		int nFIdx = ESMGetFrameIndex(0);
		strFolder = ESMGetMoviePath(strDscId, nFIdx);


		if( strFolder == _T(""))
			continue;

		if(!fo.IsFileExist(strFolder))
			continue;
// 		BYTE* pBmpBits = NULL;
// 		FFmpegMgr.GetCaptureImage(strFolder, &pBmpBits, 0, &nWidth, &nHeight);
		int nRunningTime = 0, nFrameCount = 0, nChannel = 3, nFrameRate = 0;
		FFmpegMgr.GetMovieInfo(strFolder, &nRunningTime, &nFrameCount, &nWidth, &nHeight, &nChannel, &nFrameRate);
		if( nWidth != 0)
			break;
	}
}