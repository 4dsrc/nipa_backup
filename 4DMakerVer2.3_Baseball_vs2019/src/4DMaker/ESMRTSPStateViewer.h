#pragma once
#include <afxsock.h> 
#include <afxinet.h> 
#include "afxcmn.h"
#include "ESMUtilMultiplexerOptDlg.h"
#include "ESMUtilMultiplexerMuxer.h"
#include "ESMNetworkListCtrl.h"

#include "ESMButtonEx.h"

enum RTSPLIST
{
	RTSP_LIST_INDEX = 0,
	RTSP_LIST_IP,
	RTSP_LIST_DSC,
	RTSP_LIST_RECORDNAME,
	RTSP_LIST_MAX
};
// CESMRTSPStateViewer 대화 상자입니다.
struct stRecordState 
{
	stRecordState()
	{
		nRecordStopCnt  = 999999;
		nSyncCount		= 0;
	}
	int nRecordStopCnt;
	int nSyncCount;
	vector<int>nArrDSC;
};
class CESMRTSPStateViewer : public CDialogEx
{
	DECLARE_DYNAMIC(CESMRTSPStateViewer)

public:
	CESMRTSPStateViewer(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMRTSPStateViewer();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_RTSPSTATE_VIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

public:
	CESMNetworkListCtrl m_LcFTP;
	void UpdateDSCList();
	void Update4DAPState(CString str,int nIndex,int nType,int nValue);

	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();

	afx_msg void OnBnClickedBtnRtspviewStart();
	afx_msg void OnBnClickedBtnServiceFinish();

private:
	int m_nSyncCount;
	CString m_strBirdViewPath;
	BOOL m_bProcessRun;
public:
	//RTSP RESTFul
	void SetRTSPCommand(RTSP_RESTFul_COMMAND command,CString str4DM,int nCount = 0);
	void LaunchSyncFinishThread(CString str4DM,int nSyncCnt);
	static unsigned WINAPI _SyncFinishThread(LPVOID param);
	int GetListRemainSize(int nIdx);

	void SendResumeMessage();
	void SendPauseMessage();
	BOOL GetRecordState();
	afx_msg void OnBnClickedBtnRtspviewRecin();
	afx_msg void OnBnClickedBtnRtspviewRecout();

	void SetProcessRun(BOOL b){m_bProcessRun = b;}
	BOOL GetProcessRun(){return m_bProcessRun;}
	afx_msg void OnBnClickedCancel();

	map<CString,stRecordState*> m_mapRecordState;

	void SetRTSPPlay(CString str4DM,int nIdx,int nData);
	void SetRTSPFrameCount(CString str4DM,int nIdx,int nData);
	void SetRecordStop(CString str4DM,int nStopCnt)
	{
		EnterCriticalSection(&m_criInsertData);
		if(m_mapRecordState[str4DM] != NULL)
			m_mapRecordState[str4DM]->nRecordStopCnt = nStopCnt;
		LeaveCriticalSection(&m_criInsertData);
	}
	int GetRTSPPlayIdx(CString str4DM,int nIdx)
	{
		EnterCriticalSection(&m_criInsertData);
		//nIdx -= 1;
		int nIndex = 0;
		if(m_mapRecordState[str4DM] != NULL)
			nIndex = m_mapRecordState[str4DM]->nArrDSC.at(nIdx);
		LeaveCriticalSection(&m_criInsertData);
		return nIndex;
	}
	void DeleteRTSP4DM(CString str4DM)
	{
		EnterCriticalSection(&m_criInsertData);
		if(m_mapRecordState[str4DM] != NULL)
		{
			m_mapRecordState[str4DM]->nArrDSC.clear();
			delete m_mapRecordState[str4DM];
			m_mapRecordState[str4DM] = NULL;

			/*map<CString,stRecordState*>::iterator iter;
			while(iter != m_mapRecordState.end())
			{
				if(iter->first == str4DM)
				{
					m_mapRecordState.erase(iter->first);
					ESMLog(5,_T("Delete - %s"),str4DM);
					break;
				}

				iter++;
			}*/
			m_mapRecordState.erase(str4DM);
		}
		LeaveCriticalSection(&m_criInsertData);
	}
	stRecordState* GetRecordState(CString str4DM);
	/*{
		stRecordState* pRecordState = NULL;
		EnterCriticalSection(&m_criInsertData);
		pRecordState = m_mapRecordState[str4DM];
		LeaveCriticalSection(&m_criInsertData);

		return pRecordState;
	}*/
	void SetThreadStop(BOOL b){m_bThreadStop = b;}
	BOOL GetThreadStop(){return m_bThreadStop;}

	CRITICAL_SECTION m_criInsertData;
private:
	int m_nRecordStopCnt;
	BOOL m_bThreadStop;
	int m_nLastFrameCnt;
	int m_nLastRepeatCnt;
	/*void LaunchSyncFinishThread(CString str4DM);
	static unsigned WINAPI _SyncFinishThread(LPVOID param);*/
};
struct THREAD_RTSP_VIEWER
{
	THREAD_RTSP_VIEWER()
	{
		pViewer = NULL;
	}
	CString str4DM;
	CESMRTSPStateViewer* pViewer;
};
