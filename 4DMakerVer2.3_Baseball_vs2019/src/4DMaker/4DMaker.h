/////////////////////////////////////////////////////////////////////////////
//
//  4DMaker.h : main header file for the SMILE application
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  ESMLab, Inc., and may not be copied
//  nor disclosed except upon written agreement
//  by ESMLab, Inc..
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-18
//
/////////////////////////////////////////////////////////////////////////////


#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// C4DMakerApp:
// See ESM.cpp for the implementation of this class
//

class C4DMakerApp : public CWinApp
{
public:
	C4DMakerApp();
	//-- 2013-04-24 hongsu.jung
	//-- For GDI+
	ULONG_PTR	m_gdiplusToken;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(C4DMakerApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	BOOL GetMacAddress(CStringArray& strMacArray, CString strSpecifiedIP);
	BOOL GetCheckUSB ();
	//}}AFX_VIRTUAL

// Implementation
protected:
	HMENU m_hSMILEMenu;
	HACCEL m_hSMILEAccel;

public:
	//{{AFX_MSG(C4DMakerApp)
	afx_msg void OnAppAbout();
	//afx_msg void OnFileNew();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


public:
	HINSTANCE   m_hDll;	//SciLexer.dll 등록을 위한 함수
	BOOL TerminatePreviousInstance();
};



