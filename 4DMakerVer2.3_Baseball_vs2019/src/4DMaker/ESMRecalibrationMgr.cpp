#include "stdafx.h"
#include "ESMRecalibrationMgr.h"
#include "ESMCtrl.h"
#include "ESMFileOperation.h"


CESMRecalibrationMgr::CESMRecalibrationMgr(void)
{
//	m_bIsOverBoundary = FALSE;
}

CESMRecalibrationMgr::~CESMRecalibrationMgr(void)
{

}


void CESMRecalibrationMgr::AddDscInfo(CString strDscId, CString strDscIp,BOOL bReverse /*= FALSE*/)
{
	DscAdjustInfo* pDscAdjustData;
	pDscAdjustData = new DscAdjustInfo;
	pDscAdjustData->strDscName = strDscId;
	pDscAdjustData->strDscIp = strDscIp;
	pDscAdjustData->bAdjust = FALSE;
	pDscAdjustData->bRotate = FALSE;
	pDscAdjustData->bMove = FALSE;
	pDscAdjustData->bImgCut = FALSE;
	pDscAdjustData->bReverse = bReverse;
	m_ArrDscInfo.push_back(pDscAdjustData);
}

void CESMRecalibrationMgr::GetDscInfo(vector<DscAdjustInfo*>** pArrDscInfo)
{
	*pArrDscInfo = &m_ArrDscInfo;
}

int CESMRecalibrationMgr::GetDscCount()
{
	return m_ArrDscInfo.size();
}

void CESMRecalibrationMgr::DscClear()
{
	DscAdjustInfo* pDscAdjustData;
	for( int i =0 ;i < m_ArrDscInfo.size(); i++)
	{
		pDscAdjustData = m_ArrDscInfo.at(i);
		if( pDscAdjustData->pBmpBits != NULL)
		{
			delete[] pDscAdjustData->pBmpBits;
			pDscAdjustData->pBmpBits = NULL;
		}
		if( pDscAdjustData->pGrayBmpBits != NULL)
		{
			delete[] pDscAdjustData->pGrayBmpBits;
			pDscAdjustData->pGrayBmpBits = NULL;
		}

		delete pDscAdjustData;
		pDscAdjustData = NULL;
	}
	m_ArrDscInfo.clear();
}

DscAdjustInfo* CESMRecalibrationMgr::GetDscAt(int nIndex)
{
	if(m_ArrDscInfo.size() > nIndex)
	{
		return m_ArrDscInfo.at(nIndex);
	}
	return NULL;
}

int CESMRecalibrationMgr::GetDscIndexFrom(CString strDscId)
{
	int DscID = -1;
	for(int i = 0; i < GetDscCount(); i++)
	{	
		if(!strDscId.Compare(m_ArrDscInfo[i]->strDscName))
		{
			DscID = i;
			break;
		}
	}
	return DscID;
}

BOOL CESMRecalibrationMgr::GetSearchDetectPoint(DscAdjustInfo* pAdjustInfo, int nThreshold, CSize minDetectSize, CSize maxDetectSize, int nPointGapMin, COLORREF clrDetectColor)
{
	if( pAdjustInfo == NULL || pAdjustInfo->nWidht == 0 ||  pAdjustInfo->nHeight == 0)
		return FALSE;

	int nChannel = 3;
	IplImage *pImage, *pGray;
	pImage = cvCreateImage(cvSize(pAdjustInfo->nWidht, pAdjustInfo->nHeight), IPL_DEPTH_8U, nChannel);
	memcpy(pImage->imageData, pAdjustInfo->pBmpBits, pAdjustInfo->nWidht * pAdjustInfo->nHeight * nChannel);
	if(!pImage)
		return FALSE;

	pGray = cvCreateImage(cvGetSize(pImage), IPL_DEPTH_8U, 1);
	if(!pGray)
	{
		cvReleaseImage(&pImage);
		cvReleaseImage(&pGray);
		return FALSE;
	}

	cvCvtColor(pImage, pGray, cv::COLOR_RGB2GRAY);
	cvThreshold(pGray, pGray, nThreshold, 255 , CV_THRESH_BINARY);

	vector<CRect> vecDetectAreas;
	
	SearchDetectPoint(pGray, minDetectSize, maxDetectSize, &vecDetectAreas, clrDetectColor);

	IplImage* pGrayImg = cvCreateImage(cvSize(pGray->width, pGray->height), IPL_DEPTH_8U, nChannel);
	cvCvtColor(pGray, pGrayImg, CV_GRAY2RGB);
	pAdjustInfo->pGrayBmpBits = new BYTE[pGrayImg->height * pGrayImg->widthStep];

	if( vecDetectAreas.size() < 3)
	{
		memcpy(pAdjustInfo->pGrayBmpBits, pGrayImg->imageData, pGrayImg->height * pGrayImg->widthStep);
		cvReleaseImage(&pImage);
		cvReleaseImage(&pGray);
		cvReleaseImage(&pGrayImg);
		return FALSE;
	}

	if(vecDetectAreas[1].Width() > 1 && vecDetectAreas[1].Height() > 1)
	{
		pAdjustInfo->recMiddle = vecDetectAreas[1];
		pAdjustInfo->MiddlePos = vecDetectAreas[1].CenterPoint();
	}
	if(vecDetectAreas[0].Width() > 1 && vecDetectAreas[0].Height() > 1)
	{
		if(pAdjustInfo->MiddlePos.y - vecDetectAreas[0].CenterPoint().y > nPointGapMin)
		{
			pAdjustInfo->recHigh = vecDetectAreas[0];
			pAdjustInfo->HighPos = vecDetectAreas[0].CenterPoint();
		}
		else
			vecDetectAreas[0].SetRect(0,0,0,0);

	}
	if(vecDetectAreas[2].Width() > 1 && vecDetectAreas[2].Height() > 1)
	{
		if(vecDetectAreas[2].CenterPoint().y - pAdjustInfo->MiddlePos.y > nPointGapMin)
		{
			pAdjustInfo->recLow = vecDetectAreas[2];
			pAdjustInfo->LowPos = vecDetectAreas[2].CenterPoint();
		}
		else
			vecDetectAreas[2].SetRect(0,0,0,0);
	}

	for(int i =0 ;i < vecDetectAreas.size(); i++)
	{
		if(vecDetectAreas[i].CenterPoint().x != 0)
			cvRectangle(pGrayImg, cvPoint(vecDetectAreas[i].left, vecDetectAreas[i].top), cvPoint(vecDetectAreas[i].right, vecDetectAreas[i].bottom), cvScalar(0, 0, 255), 2, 8, 0);
	}
	if( vecDetectAreas.size() >= 3)
	{
		if((vecDetectAreas[0].Width() > 1 && vecDetectAreas[0].Height() > 1) && (vecDetectAreas[2].Width() > 1 && vecDetectAreas[2].Height() > 1))
			cvLine(pGrayImg, cvPoint(vecDetectAreas[0].CenterPoint().x, vecDetectAreas[0].CenterPoint().y), cvPoint(vecDetectAreas[2].CenterPoint().x, vecDetectAreas[2].CenterPoint().y), cvScalar(0, 0, 255), 2, 8, 0);
	}
	memcpy(pAdjustInfo->pGrayBmpBits, pGrayImg->imageData, pGrayImg->height * pGrayImg->widthStep);

	cvReleaseImage(&pImage);
	cvReleaseImage(&pGray);
	cvReleaseImage(&pGrayImg);
	

	return TRUE;
}

// 2014-01-07 kcd 
// Distance 임시 함수
double CESMRecalibrationMgr::GetDistanceData(CString strDSCId)
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\DSCDistance.csv"), ESMGetPath(ESM_PATH_SETUP));
	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		return FALSE;
	}

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());
	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;
	CString strTpValue1, strTpValue2;
	double nDistance = 0;
	while (iPos  < iLength )
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;
		AfxExtractSubString(strTpValue1, sLine, 0, ',');
		AfxExtractSubString(strTpValue2, sLine, 1, ',');
		if( strTpValue1 == strDSCId)
		{
			nDistance = _ttof(strTpValue2);
			break;
		}
	}
	return nDistance;
}

int CESMRecalibrationMgr::GetZoomData(CString strDSCId)
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\DSCZoom.csv"), ESMGetPath(ESM_PATH_SETUP));
	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		return FALSE;
	}

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());
	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;
	CString strTpValue1, strTpValue2;
	double nDistance = 0;
	while (iPos  < iLength )
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;
		AfxExtractSubString(strTpValue1, sLine, 0, ',');
		AfxExtractSubString(strTpValue2, sLine, 1, ',');
		if( strTpValue1 == strDSCId)
		{
			nDistance = _ttoi(strTpValue2);
			break;
		}
	}
	return nDistance;
}

BOOL CESMRecalibrationMgr::CalcAdjustData(DscAdjustInfo* pAdjustInfo, int nTargetLenth, int nZoom)
{
	//회전
	pAdjustInfo->dAngle = atan2((double)(pAdjustInfo->LowPos.y - pAdjustInfo->HighPos.y), 
		(double)(pAdjustInfo->LowPos.x - pAdjustInfo->HighPos.x)) * (180/3.141592);
	
	//Size
	double dTargetDistance = 0;
	double dTargetLength = 0;

	if( pAdjustInfo->dDistance == 0.0)
		dTargetDistance = GetDistanceData(pAdjustInfo->strDscName);
	else
		dTargetDistance = pAdjustInfo->dDistance;
	
	int nPixelDefaultSize = 0;
	if( pAdjustInfo->nWidht == 1920)
		nPixelDefaultSize = DEFAULT_PIXEL1920_DISTANCE;
	else if( pAdjustInfo->nWidht == 5472)
		nPixelDefaultSize = DEFAULT_PIXEL5472_DISTANCE;
	else if( pAdjustInfo->nWidht == 2200 && pAdjustInfo->nHeight == 1238)
		nPixelDefaultSize = DEFAULT_PIXEL5472_3080_DISTANCE;
	else if( pAdjustInfo->nWidht == 3840 && pAdjustInfo->nHeight == 2160)
		nPixelDefaultSize = DEFAULT_PIXEL3840_2160_DISTANCE;
	else
		nPixelDefaultSize = DEFAULT_PIXEL1920_DISTANCE;

	dTargetLength = (double)nTargetLenth * nPixelDefaultSize  / DEFAULT_TARGETLENGTH;
	if( dTargetDistance != 0.0)
		dTargetLength = dTargetLength / dTargetDistance;
	else
		dTargetLength =1.0;

	if(nZoom == 0)
		nZoom = GetZoomData(pAdjustInfo->strDscName);

	dTargetLength = dTargetLength * nZoom /DEFAULT_CAMERAZOOM;

	int nHeight = pAdjustInfo->LowPos.y - pAdjustInfo->HighPos.y;
	int nWidth = abs(pAdjustInfo->LowPos.x - pAdjustInfo->HighPos.x);
	if( nHeight != 0 || nWidth != 0 )
	{
		double dTpResize = 0.0;
		if( nWidth != 0)
			dTpResize = sqrt(double(nHeight * nHeight + nWidth * nWidth));
		else
			dTpResize = nHeight;

		pAdjustInfo->dScale = dTargetLength / dTpResize ;

		//CenterPoint
		pAdjustInfo->dRotateX = pAdjustInfo->dAdjustX = pAdjustInfo->MiddlePos.x;
		pAdjustInfo->dRotateY = pAdjustInfo->dAdjustY = pAdjustInfo->MiddlePos.y;
		pAdjustInfo->dAngle = pAdjustInfo->dAngle * -1;
		pAdjustInfo->dDistance = dTargetDistance;
		return TRUE;
	}
	return FALSE;
}

BOOL CESMRecalibrationMgr::SearchDetectPoint(IplImage* pNewGray, CSize MinSize, CSize MaxSize, vector<CRect>* vecDetectAreas, COLORREF clrDetectColor)
{
	CPoint pointCenter = CPoint(pNewGray->width / 2, pNewGray->height / 2);
	vector<CRect> vecCenters;
	BOOL bStart = FALSE, bEnd = FALSE;
	int nStartPos = 0, nEndPos = 0;
	int nStartCenterArea = (pNewGray->height / 2) - ((pNewGray->height / 5) / 2);
	int nEndCenterArea = (pNewGray->height / 2) + ((pNewGray->height / 5) / 2);
	int nPosYGap = (MaxSize.cy / 2) + 10;

	int nFindColor;
	if(clrDetectColor == COLOR_WHITE)
		nFindColor = 0;
	else
		nFindColor = 255;

	for(int nFindY = 0; nFindY < pNewGray->height; nFindY++)
	{
		if((unsigned char)pNewGray->imageData[(nFindY * pNewGray->widthStep) + pointCenter.x] == nFindColor)
		{
			if(!bStart)	bStart = TRUE;
			else if(bEnd)
			{
				nEndPos = nFindY - 1;
				bEnd = FALSE;
				bStart = FALSE;
				if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
				{
					CRect recDetect(pointCenter.x, nStartPos, pointCenter.x, nEndPos);
					vecCenters.push_back(recDetect);
				}
			}
		}
		else
		{
			if(bStart && !bEnd)
			{
				nStartPos = nFindY;
				bEnd = TRUE;
			}
		}
	}
	if(vecCenters.size() < 3)
	{
		vecCenters.clear();
		bStart = FALSE;
		bEnd = FALSE;
		nStartPos = 0;
		nEndPos = 0;
		for(int nFindX = pointCenter.x - 1; nFindX > (pointCenter.x - MaxSize.cx); nFindX--)
		{
			vecCenters.clear();
			for(int nFindY = 0; nFindY < pNewGray->height; nFindY++)
			{
				if((unsigned char)pNewGray->imageData[(nFindY * pNewGray->widthStep) + nFindX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nFindY - 1;
						bEnd = FALSE;
						bStart = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							CRect recDetect(nFindX, nStartPos, nFindX, nEndPos);
							vecCenters.push_back(recDetect);
						}
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nFindY;
						bEnd = TRUE;
					}
				}
			}
			if(vecCenters.size() >= 3)
				break;
		}
	}
	if(vecCenters.size() < 3)
	{
		vecCenters.clear();
		bStart = FALSE;
		bEnd = FALSE;
		nStartPos = 0;
		nEndPos = 0;
		for(int nFindX = pointCenter.x + 1; nFindX < (pointCenter.x + MaxSize.cx); nFindX++)
		{
			vecCenters.clear();
			for(int nFindY = 0; nFindY < pNewGray->height; nFindY++)
			{
				if((unsigned char)pNewGray->imageData[(nFindY * pNewGray->widthStep) + nFindX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nFindY - 1;
						bEnd = FALSE;
						bStart = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							CRect recDetect(nFindX, nStartPos, nFindX, nEndPos);
							vecCenters.push_back(recDetect);
						}
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nFindY;
						bEnd = TRUE;
					}
				}
			}
			if(vecCenters.size() >= 3)
				break;
		}
	}
	if(vecCenters.size() < 3)
		return FALSE;

	//Center에 가장 가까운 영역을 찾는다. 
	CRect recCenter = vecCenters[0];
	int nCenterMargin = pointCenter.y - recCenter.CenterPoint().y;
	if(nCenterMargin < 0) nCenterMargin *= -1;
	for(int nIdx = 1; nIdx < vecCenters.size(); nIdx++)
	{
		int nMargin = pointCenter.y - vecCenters[nIdx].CenterPoint().y;
		if(nMargin < 0) nMargin *= -1;
		if(nMargin < nCenterMargin)
		{
			nCenterMargin = nMargin;
			recCenter =  vecCenters[nIdx];
		}
	}

	if(recCenter.CenterPoint().y < nStartCenterArea || recCenter.CenterPoint().y > nEndCenterArea)
		return FALSE;

	//First와 Third 영역을 찾는다. 
	CRect recFirst = recCenter;
	CRect recThird = recCenter;
	int nFirstMargin = 0, nThirdMargin = 0;
	for(int nIdx = 0; nIdx < vecCenters.size(); nIdx++)
	{
		if(recCenter == vecCenters[nIdx])
			continue;
		int nMargin = recCenter.CenterPoint().y - vecCenters[nIdx].CenterPoint().y;
		if(nMargin < 0)
		{
			nMargin *= -1;
			if(nThirdMargin == 0)
			{
				nThirdMargin = nMargin;
				recThird = vecCenters[nIdx];
			}
			else if(nMargin < nThirdMargin)
			{
				nThirdMargin = nMargin;
				recThird = vecCenters[nIdx];
			}
		}	
		else
		{
			if(nFirstMargin == 0)
			{
				nFirstMargin = nMargin;
				recFirst = vecCenters[nIdx];
			}
			else if(nMargin < nFirstMargin)
			{
				nFirstMargin = nMargin;
				recFirst = vecCenters[nIdx];
			}
		}
	}

	vecCenters.clear();
	vecCenters.push_back(recFirst);
	vecCenters.push_back(recCenter);
	vecCenters.push_back(recThird);

	
	for(int nAreaIdx = 0; nAreaIdx < vecCenters.size(); nAreaIdx++)
	{
		//Left 시작 지점 찾는다.
		int nLeft = vecCenters[nAreaIdx].left;
		int nTop = vecCenters[nAreaIdx].top;
		int nRight = vecCenters[nAreaIdx].right;
		int nBottom = vecCenters[nAreaIdx].bottom;
		BOOL bDetectStop = FALSE;
		CRect recDetectArea = vecCenters[nAreaIdx];
		vector<int> vecHeightAvg;
		vector<CRect> vecAreaList;
		vecAreaList.push_back(vecCenters[nAreaIdx]);
		vecHeightAvg.push_back(nBottom - nTop);
		for(int nPosX = vecCenters[nAreaIdx].CenterPoint().x - 1; nPosX > vecCenters[nAreaIdx].CenterPoint().x - MaxSize.cx; nPosX--)
		{
			if(nPosX > pNewGray->width) break;
			int nPosY = vecCenters[nAreaIdx].CenterPoint().y - nPosYGap;
			if(nPosY < 0) nPosY = 0;
			if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] != nFindColor)
				break;

			bStart = FALSE;
			bEnd = FALSE;
			nStartPos = 0;
			nEndPos = 0;

			int nHeightAvg = 0, nHeightSum = 0, nAvgIdxCnt = vecHeightAvg.size();
			for(int nAvgIdx = 0; nAvgIdx < nAvgIdxCnt; nAvgIdx++)
				nHeightSum += vecHeightAvg[nAvgIdx];
			nHeightAvg = nHeightSum / nAvgIdxCnt;

			for(; nPosY < vecCenters[nAreaIdx].CenterPoint().y + nPosYGap; nPosY++)
			{
				if(nPosY > pNewGray->height) break;
				if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nPosY - 1;
						bStart = FALSE;
						bEnd = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							int nHeight = nEndPos - nStartPos;
							if(nHeight >= nHeightAvg - 10 &&  nHeight <= nHeightAvg + 10)
							{
								nLeft = nPosX;
								if(nStartPos > nTop)	nTop = nStartPos;
								if(nEndPos < nBottom)	nBottom = nEndPos;
								recDetectArea.SetRect(nLeft, nTop, nRight, nBottom);
								vecHeightAvg.push_back(nHeight);
								vecAreaList.push_back(CRect(nPosX, nStartPos, nPosX, nEndPos));
							}
						}
						else
							bDetectStop = TRUE;
						break;
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nPosY;
						bEnd = TRUE;
					}
				}
			}
			if(bDetectStop)
				break;
		}
		//Right 마지막 지점 찾는다.
		bDetectStop = FALSE;
		for(int nPosX = vecCenters[nAreaIdx].CenterPoint().x + 1; nPosX < vecCenters[nAreaIdx].CenterPoint().x + MaxSize.cx; nPosX++)
		{
			if(nPosX > pNewGray->width) break;
			int nPosY = vecCenters[nAreaIdx].CenterPoint().y - nPosYGap;
			if(nPosY < 0) nPosY = 0;
			if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] != nFindColor)
				break;
			bStart = FALSE;
			bEnd = FALSE;
			nStartPos = 0;
			nEndPos = 0;

			int nHeightAvg = 0, nHeightSum = 0, nAvgIdxCnt = vecHeightAvg.size();
			for(int nAvgIdx = 0; nAvgIdx < nAvgIdxCnt; nAvgIdx++)
				nHeightSum += vecHeightAvg[nAvgIdx];
			nHeightAvg = nHeightSum / nAvgIdxCnt;

			for(; nPosY < vecCenters[nAreaIdx].CenterPoint().y + nPosYGap; nPosY++)
			{
				if(nPosY > pNewGray->height) break;
				if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nPosY - 1;
						bStart = FALSE;
						bEnd = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							int nHeight = nEndPos - nStartPos;
							if(nHeight >= nHeightAvg - 10 &&  nHeight <= nHeightAvg + 10)
							{
								nRight = nPosX;
								if(nStartPos > nTop)	nTop = nStartPos;
								if(nEndPos < nBottom)	nBottom = nEndPos;
								recDetectArea.SetRect(nLeft, nTop, nRight, nBottom);
								vecHeightAvg.push_back(nHeight);
								vecAreaList.push_back(CRect(nPosX, nStartPos, nPosX, nEndPos));
							}
						}
						else
							bDetectStop = TRUE;
						break;
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nPosY;
						bEnd = TRUE;
					}
				}
			}
			if(bDetectStop)
				break;
		}
		if(recDetectArea.Width() < 5)
		{
			recDetectArea.SetRect(0, 0, 0, 0);
		}
		else
		{
			int nCenterX = recDetectArea.CenterPoint().x;
			for(int nXIdx = 0; nXIdx < vecAreaList.size(); nXIdx++)
			{
				if(vecAreaList[nXIdx].left == nCenterX)
				{
					nLeft = recDetectArea.left;
					nTop = vecAreaList[nXIdx].top;
					nRight = recDetectArea.right;
					nBottom = vecAreaList[nXIdx].bottom;
					recDetectArea.SetRect(nLeft, nTop, nRight, nBottom);
					break;
				}
			}
		}
		vecDetectAreas->push_back(recDetectArea);
	}
	return TRUE;
}

BOOL CESMRecalibrationMgr::SearchPoint(IplImage*	pNewGray, vector<CPoint>* arrBeginPoint, vector<CPoint>* arrPointRange, vector<CPoint>* pArrPoint)
{
	CPoint TpBeginP;
	CPoint TpPoints;
	if( pNewGray->width < arrBeginPoint->at(2).x + arrPointRange->at(2).x)
		return FALSE;
	if( pNewGray->height < arrBeginPoint->at(2).y + arrPointRange->at(2).y)
		return FALSE;

	for( int i = 0; i< arrBeginPoint->size(); i++)
	{
		CPoint CenterPoint;
		CPoint tpPoint;
		vector<CPoint> arrPoint;
		int x1 = 0;
		int nImageWidth	= pNewGray->width;
		int nImageHeight = pNewGray->height;
		TpPoints = arrPointRange->at(i);

		for(int y1 = arrBeginPoint->at(i).y ; y1 < arrBeginPoint->at(i).y + TpPoints.y ; y1++)
		{
			for(x1 = arrBeginPoint->at(i).x ; x1 < arrBeginPoint->at(i).x + TpPoints.x ; x1++)
			{
				if((unsigned char)pNewGray->imageData[y1*pNewGray->widthStep + x1] < 10)// 흑을 만난다면 Continue;
				{
					tpPoint.x = x1;
					tpPoint.y = y1;
					arrPoint.push_back(tpPoint);
					break;
				}
			}
			if( x1 == arrBeginPoint->at(i).x + TpPoints.x)
				continue;

			//Detect Right Line
			for(x1 = arrBeginPoint->at(i).x + TpPoints.x ; x1 > arrBeginPoint->at(i).x ; x1--)
			{
				if((unsigned char)pNewGray->imageData[y1*pNewGray->widthStep + x1] < 10)// 흑색을 만난다면 Continue;
				{
					tpPoint.x = x1;
					tpPoint.y = y1;
					arrPoint.push_back(tpPoint);
					break;
				}
			}
		}
		if( arrPoint.size() < 1)
			return FALSE;
		int dMinX = arrPoint.at(0).x, dMinY = arrPoint.at(0).y, dMaxX = arrPoint.at(0).x, dMaxY = arrPoint.at(0).y;
		for(int nIndex =0 ;nIndex < arrPoint.size() ; nIndex++)
		{
			if( arrPoint.at(nIndex).x < dMinX)
				dMinX = arrPoint.at(nIndex).x;
			else if( arrPoint.at(nIndex).y < dMinY)
				dMinY = arrPoint.at(nIndex).y;
			else if( arrPoint.at(nIndex).x > dMaxX)
				dMaxX = arrPoint.at(nIndex).x;
			else if( arrPoint.at(nIndex).x > dMaxY)
				dMaxY = arrPoint.at(nIndex).y;
		}
		TpBeginP.x = (dMinX + dMaxX) / 2;
		TpBeginP.y = (dMinY + dMaxY) / 2;
		pArrPoint->push_back(TpBeginP);

	}
	return TRUE;
}


void CESMRecalibrationMgr::GetResolution(int &nWidth, int &nHeight)
{
	DscAdjustInfo* pAdjustInfo = NULL;
	CESMFileOperation fo;
	for(int i =0; i< GetDscCount(); i++)
	{
		pAdjustInfo = GetDscAt(i);

		FFmpegManager FFmpegMgr(FALSE);
		CString strFolder, strDscId, strDscIp;
		strDscId = pAdjustInfo->strDscName;
		strDscIp = pAdjustInfo->strDscIp;
		int nFIdx = ESMGetFrameIndex(0);
		strFolder = ESMGetMoviePath(strDscId, nFIdx);


		if( strFolder == _T(""))
			continue;

		if(!fo.IsFileExist(strFolder))
			continue;

		int nRunningTime = 0, nFrameCount = 0, nChannel = 3, nFrameRate = 0;
		FFmpegMgr.GetMovieInfo(strFolder, &nRunningTime, &nFrameCount, &nWidth, &nHeight, &nChannel, &nFrameRate);
		if( nWidth != 0)
			break;
	}
}

void CESMRecalibrationMgr::AddAdjInfo(stAdjustInfo adjustInfo)
{
	m_arrAdjInfo.push_back(adjustInfo);
	m_arrModifiedAdjInfo.push_back(adjustInfo);
}

stAdjustInfo CESMRecalibrationMgr::GetAdjInfoAt(int index)
{
	return m_arrAdjInfo.at(index);
}

Mat CESMRecalibrationMgr::ApplyAdjustImageUsingCPU(int nSelectedItem, int marginX, int marginY)
{	
	Mat src;
	DscAdjustInfo *pAdjustInfo = GetDscAt(nSelectedItem);
	if( pAdjustInfo != NULL)
	{
		if(pAdjustInfo->bAdjust )
		{
			//pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3, NULL , 0);	
		}
		else if(pAdjustInfo->nHeight == 0 || pAdjustInfo->nWidht == 0)
		{
			return Mat();
		}

		src.create(pAdjustInfo->nHeight, pAdjustInfo->nWidht, CV_8UC3);
		memcpy(src.data, pAdjustInfo->pBmpBits, pAdjustInfo->nHeight * pAdjustInfo->nWidht * 3);

		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = 1;
		stAdjustInfo AdjustData = m_arrAdjInfo.at(nSelectedItem);

		if(!pAdjustInfo->bRotate)
		{
			// Rotate 구현					
			nRotateX = Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
			nRotateY = Round(AdjustData.AdjptRotate.y  * dRatio );

			//IplImage *Iimage = new IplImage(src);
			Mat Iimage(src);
			CpuRotateImage(Iimage, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
			//delete Iimage;
		}

		if(!pAdjustInfo->bMove)
		{
			//-- MOVE EVENT
			int nMoveX = 0, nMoveY = 0;
			nMoveX = Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
			nMoveY = Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
			CpuMoveImage(&src, nMoveX,  nMoveY);
		}

		if(!pAdjustInfo->bImgCut)
		{
			//-- IMAGE CUT EVENT
			int nMarginX = marginX* dRatio;
			int nMarginY = marginY * dRatio;
			if ( nMarginX < 0 || nMarginX >= pAdjustInfo->nWidht/2 || nMarginY < 0 || nMarginY >= pAdjustInfo->nHeight/2  )
			{
				TRACE(_T("Image Adjust Error 2\n"));
				return Mat();
			}
			//CpuMakeMargin(&src, nMarginX, nMarginY);
			double dbMarginScale = 1 / ( (double)( pAdjustInfo->nWidht - nMarginX * 2) /(double) pAdjustInfo->nWidht  );
			Mat  pMatResize;

			resize(src, pMatResize, cv::Size(pAdjustInfo->nWidht*dbMarginScale ,pAdjustInfo->nHeight * dbMarginScale ), 0.0, 0.0 ,CV_INTER_NN);
			int nLeft = (int)((pAdjustInfo->nWidht*dbMarginScale - pAdjustInfo->nWidht)/2);
			int nTop = (int)((pAdjustInfo->nHeight*dbMarginScale - pAdjustInfo->nHeight)/2);
			Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, pAdjustInfo->nWidht, pAdjustInfo->nHeight));
			pMatCut.copyTo(src);
			pMatResize = NULL;
		}						
		int size = src.total() * src.elemSize();
		memcpy(pAdjustInfo->pBmpBits, src.data, size);

		pAdjustInfo->bAdjust = TRUE;
		pAdjustInfo->bRotate = TRUE;
		pAdjustInfo->bMove = TRUE;
		pAdjustInfo->bImgCut = TRUE;	
	}
	return src;
}

int CESMRecalibrationMgr::Round(double dData)
{
	int nResult = 0;
	if( dData == 0)
		nResult = 0;
	else if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);

	return nResult;
}
void CESMRecalibrationMgr::GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle)
{
	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;

	Mat rot_mat(cv::Size(2, 3), CV_32FC1);
	cv::Point rot_center( nCenterX, nCenterY);

	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust = -1 * (dAngle + 90);

	rot_mat = getRotationMatrix2D(rot_center, dbAngleAdjust, dScale);

	cuda::GpuMat d_rotate;	
	cuda::warpAffine(*gMat, d_rotate, rot_mat, cv::Size(gMat->cols, gMat->rows), cv::INTER_CUBIC);
	d_rotate.copyTo(*gMat);
}
void CESMRecalibrationMgr::GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;

	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void CESMRecalibrationMgr::GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void CESMRecalibrationMgr::CpuMakeMargin(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;

	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);

}
void CESMRecalibrationMgr::CpuMoveImage(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void CESMRecalibrationMgr::CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle)
{
	Mat Iimage2;// = cvCreateImage(cvGetSize(Iimage), IPL_DEPTH_8U, 3);

	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;

	CvMat *rot_mat = cvCreateMat( 2, 3, CV_32FC1);
	CvPoint2D32f rot_center = cvPoint2D32f( nCenterX, nCenterY);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust = -1 * (dAngle + 90);

	cv2DRotationMatrix( rot_center, dbAngleAdjust, dScale, rot_mat);
	cvWarpAffine(&IplImage(Iimage), &IplImage(Iimage), rot_mat,cv::INTER_LINEAR+cv::WARP_FILL_OUTLIERS);	//d_rotate.copyTo(*gMat);
	cvReleaseMat(&rot_mat);
}

void CESMRecalibrationMgr::affinetransform(Mat img,cv::Point a,cv::Point b, cv::Point c, Point2f srcTri[3],Mat dst)
{
	Mat warp_mat(2,3,CV_32FC1);
	Point2f dstTri[3];
	dstTri[0] = a;	dstTri[1] = b;	dstTri[2] = c;

	warp_mat = getAffineTransform(srcTri,dstTri);
	warpAffine(img,dst,warp_mat,dst.size());
}


void CESMRecalibrationMgr::moveImage(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
