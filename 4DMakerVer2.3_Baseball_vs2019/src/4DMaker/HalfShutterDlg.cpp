// HalfShutterDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "HalfShutterDlg.h"
#include "afxdialogex.h"


// CHalfShutterDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CHalfShutterDlg, CDialogEx)

CHalfShutterDlg::CHalfShutterDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CHalfShutterDlg::IDD, pParent)
{
	m_nReleaseSec = 0;
}

CHalfShutterDlg::~CHalfShutterDlg()
{
}

void CHalfShutterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CHalfShutterDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CHalfShutterDlg::OnBnClickedOk)
END_MESSAGE_MAP()

// CHalfShutterDlg 메시지 처리기입니다.

int CHalfShutterDlg::GetReleaseSec()
{
	return m_nReleaseSec;
}


BOOL CHalfShutterDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	GetDlgItem(IDC_EDIT_SEC)->SetWindowText(_T("3"));

	GetDlgItem(IDC_EDIT_SEC)->ModifyStyle(0, ES_NUMBER);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CHalfShutterDlg::OnBnClickedOk()
{
	CString strTemp;

	GetDlgItem(IDC_EDIT_SEC)->GetWindowText(strTemp);

	if (strTemp.IsEmpty())
		return;

	if (strTemp.SpanIncluding(_T("0123456789")) == strTemp)
	{
		int nValue = _ttoi(strTemp);
		m_nReleaseSec = nValue;
	}
	else
	{
		return;
	}

	if (m_nReleaseSec < 1)
		return;	

	CDialogEx::OnOK();
}
