/////////////////////////////////////////////////////////////////////////////
//
//	MainFrm.h: implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  ESMLab, Inc., and may not be copied
//  nor disclosed except upon written agreement
//  by ESMLab, Inc..
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2009-02-01
// @ver.4.0	Change New Mainframe
// @Date	2011-10-17
// @ver.1.0	4DMaker
// @Date	2013-04-22
//
/////////////////////////////////////////////////////////////////////////////
#pragma once

#if !defined(__EXT_TEMPL_H)
#include <ExtTempl.h>
#endif

//#include "ESMGpuDecode.h"
//-- TEST GUARANTEE FUCNTIONS
#include "ESMFunc.h"
//-- Management Information
#include "ESMIndex.h"
#include "ESMOption.h"
#include "ESMOptionDlg.h"
#include "ESMFileMgr.h"

//-- Management Log
#include "LogMgr.h"
#include "ESMEdit.h"

//-- Splash Window
#include "ESMSplashWnd.h"

//-- New Output View
#include "ESMOutputViewDlg.h"
#include "ESMFilteringWindowDlg.h"

//-- ESM MessageBox
#include "ESMMessageBox.h"

//-- Main View
#include "DSCViewer.h"
//-- Dash Board View
#include "ESMDashBoardDlg.h"
//-- Liveview
#include "ESMLiveviewDlg.h"
//-- Frame
#include "ESMFrameViewer.h"
#include "ESMFrameViewerEx.h"
#include "ESMResizeControlBar.h"
//-- TimeLine
#include "TimeLineEditor.h"
//-- Property
#include "ESMPropertyView.h"
#include "ESMPointListDlg.h"
//-- Effect EDITOR
#include "ESMEffectEditor.h"
#include "ESMEffectFrame.h"
#include "ESMEffectPreview.h"
//-- Property
#include "ESMNetworkDlg.h"
// [10/25/2013 Administrator]
#include "StopWatch.h"
#include "TimeViewDlg.h"
//-- 2014-05-26 hongsu@esmlab.com
//-- ESMImgMgr
#include "ESMMovieMgr.h"

#include "ESMMakeMovieMgr.h"

#include "ImageLoader.h"

#include "ESMFileMergeDlg.h"

#include "ESMMakeMovieFileMgr.h"

#include "ESMNetworkExDlg.h"

#include "ESMTemplateDlg.h"
#include "ESMTemplateMgr.h"

#include "TimeLineProcessor.h"
#include "ESMProcessAJA.h"

//KT Transfering method
#include "ESMMovieRTThread.h"
#include "ESMMovieRTSend.h"
#include "ESMMovieRTSend60p.h"

//LGU+ RTSP SendMethod
#include "ESMMovieRTMovie.h"

//wgkim@esmlab.com
#include "ESMUtilArcCalculatorDlg.h"
#include "ESMUtilActualMeasurementDlg.h"
#include "ESMUtilMultiplexerDlg.h"
#include "ESMRecalibrationDlg.h"
#include "ESMBackup.h"
#include "ESMTrackingViewer.h"

#include "ESMMovieListDlg.h"

#include "ESMUtilRemoteCtrl.h"

//180423 hjcho still image
#include "ESMStillImage.h"
//180508 hjcho AJA network
#include "ESMAJANetwork.h"

//180703 wgkim Auto Adjust
#include "ESMAutoAdjustDlg.h"
#include "ESMAutoAdjustMgr.h"

//180719 hjcho RefereeReading
#include "ESMRefereeReading.h"

#include "ESMm3u8.h"

//190227 hjcho RTSP StateView
#include "ESMRTSPStateViewer.h"
//-- Owner	 : hongsu.jung
//-- Comment : CMainFrame
//------------------------------------------------------------------------------
class CMainFrame : public CExtNCW < CFrameWnd >
{
public:
	BOOL m_bFirst;
	CString m_strDelayDirection;
	BOOL m_bPropertyCheck;
	int m_nInitialTime;
	int m_nMakeEndTime;
	int m_nMakeStartTime;
	int m_nMakeGapTime;

	BOOL m_bSyncTestAuto;
	BOOL m_bResumeSyncTest;
	BOOL m_bSyncThread;
	//CString m_strDeleteFolder[RECORD_STORED_COUNT];
	CString m_strDeleteFolder;
	BOOL m_bNextStatus;
	BOOL m_bMakeMovie;
	BOOL m_bMakeAdjust;
	BOOL m_bSyncFlag;
	BOOL m_bStart;//(CAMREVISION)
	int m_CaptureCount;
	int m_DelayDirection;
	BOOL m_cudasupport;
	BOOL m_bGPUDecodeSupport;
	BOOL m_MakingUse;
	BOOL m_bReSyncFlag;				// resync 중인지..
	int m_nReSyncTimerCount;
	int m_nGetTickRecFinishCount;
	int m_nResyncGetTickCount;		// resync하기위해 타이머 돌며 gettick 3번요청..
	BOOL m_bGetTickFirstFlag;		// 한번이라도 gettick이 이루어졌는지 확인하기 위해..
	BOOL m_bReSyncRecStopFinishFlag;			// camera record stop finish wait time
	BOOL m_bTimerRecFinishRunning;

	BOOL m_bStartFocusSaveToFileFlag;

	void* m_pCurrentObj;
	BOOL m_bSyncObj;
	BOOL m_bEditObj;
	vector<CPoint> m_arrptS;
	vector<CPoint> m_arrptE;

	//wgkim 180202
	//BOOL m_bRecordFileSplit;
	vector<CDSCGroup*> m_IpList;
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();
	virtual ~CMainFrame();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

private:	
	//==================================================================
	//== Basic	
	//==================================================================
	WINDOWPLACEMENT m_dataFrameWP;
	CESMOption		m_ESMOption;
	

	//==================================================================
	//== File Load Save
	//==================================================================
public:
	//CESMGpuDecode gpudecode[10];
	CESMFileMgr*	m_FileMgr;
	CESMOptionDlg*  m_pOptionDlg;
	CESMUtilMultiplexerDlg*  m_pMultiplexerDlg;
	CESMUtilRemoteCtrl*		 m_pRemoteCtrlDlg;
	CESMRTSPStateViewer*	 m_pRTSPStateViewDlg;
	CImageLoader*	m_pImgLoader;	//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가
	BOOL		m_bMovieSaveDone;
	BOOL     m_bCheckGPU;
	BOOL		m_bIsMakeMovie;
	//==================================================================
	//== Tick Count	
	//==================================================================
public:
	CStopWatch m_Clock;
	int m_nRecState;
	BOOL m_bReStartFalg;
	vector<int> m_arrErrPCSync;
	vector<CString> m_arrDeleteDSC;
	vector<CString> m_DeleteDSCList;
	//==================================================================
	//== Invalid	
	//==================================================================
public:
	//CString m_strDSCID;
	int m_nGPUMakeFileCount;
	int m_nScrollY;
	BOOL m_bInvalidMovieSize;
	int m_nMovieSize;
	char	m_baseballkey;
	char m_key;
	CString m_strAdjustMovieFolder;
	CString m_strSelectDSC;
	int m_nSelectPointNum;
	int m_nViewPointNum;
	BOOL m_bFirstConnect;

	making_info m_makingInfo;
	BOOL m_bMakingInfo;

	//jhhan 16-11-21 LastPointNum
	int m_nLastSelectPointNum;
	BOOL m_bFlag;

	BOOL GetCudaSupport();
	void SetCudaSupport(bool cudasupport);

	BOOL GetGPUDecodeSupport();
	void SetGPUDecodeSupport(BOOL bSupport);

	BOOL GetMakingUse();
	void SetMakingUse(bool makingUse);


	vector<CString> GetDeleteDSC();

	void SetDeleteDSCList(vector<CString> list);
	vector<CString> GetDeleteDSCList();

	//jhhan
	CString m_str4DMPath;
private:		
	void LoadInfo();

	//==================================================================
	//== Private
	//==================================================================
private:
	BOOL m_bClose;
	CString m_strConfig;
	CString m_strESMLogFile;

	//==================================================================
	//== Control UI 
	//==================================================================
private:
	void ClearWindowViewState();
	void LoadBarState(CESMSplashWnd& _splash);
	BOOL FrameEnableDocking();
	//==================================================================
	//== Menu, Tool Bar 
	//==================================================================
private:	
	CExtMenuControlBar m_wndMenuBar;
	CExtStatusControlBar m_wndStatusBar;
	CExtToolControlBar m_wndToolBarStandard;	
	BOOL CreateProFile(CESMSplashWnd &splash, CWinApp* pApp);
	BOOL CreateToolBar(CESMSplashWnd &splash);	

	//==================================================================
	//== Main View
	//== Date	 : 2013-09-04
	//== Owner	 : hongsu.jung
	//== Comment : DSC Control
	//==================================================================
public:
	CDSCViewer		m_wndDSCViewer;	
	CDSCViewer		m_wndDSCViewerEX;	

	//==================================================================
	//== ESM Image/Movie Manager
	//== Date	 : 2014-05-26
	//== Owner	 : hongsu.jung
	//== Comment : Image/Movie Manager
	//==================================================================
	CESMMovieMgr* m_pESMMovieMgr;
private:
	BOOL CreateMainView(CESMSplashWnd &splash);
	LRESULT OnChangeConnect(WPARAM wParam, LPARAM lParam);
	BOOL IsAnyConnection();

	int m_DevChangeCount;

	//==================================================================
	//== Output Viewer 
	//==================================================================
private:
	CExtControlBar				m_wndOutputBar;
	CExtWFF<CESMOutputViewDlg>	m_wndOutput;
	BOOL CreateOutputViewer(CESMSplashWnd &splash);
	inline void ShowOutputViewer(){m_wndOutputBar.ShowWindow(SW_SHOW);}
	inline void HideOutputViewer(){m_wndOutputBar.ShowWindow(SW_HIDE);}
	//==================================================================
	//== LOG
	//== Date	 : 2009-09-18
	//== Owner	 : hongsu.jung	
	//==================================================================
private:
	LogMgr* m_pLogManager;	
	void PrintError(CString strError);
	int CreateLog(BOOL bNewLog = FALSE);	
	BOOL SaveAsNewLog(CString strNewName);	
	//-- To Save Each Log
	void ClosePreviousLogFile(CString strLogFile = _T(""));
	void CreateNewLogFile(CString strLogFile);
	void SetESMLogFile(CString s){m_strESMLogFile = s;}
	//-- Save Profile
	void SaveWindowStatus();
	void MakeShortCutOEM(WPARAM w);
	void MakeShortCut(char cData);
	void MakeCShortCut(char cData);
	void MakeCSShortCut(char cData);
	//181221 hjcho
	void MakeCAShorCut(char cData);

	static unsigned WINAPI DeleteFolder(LPVOID param);
	static unsigned WINAPI DeleteRecordFile(LPVOID param);
	static unsigned WINAPI DecodeThread(LPVOID param);
	static unsigned WINAPI DeleteFrameViewerFiles(LPVOID param);
public:
	CString	GetRoot(CString str);
	CESMOption* GetESMOption() {return &m_ESMOption;}
	LogMgr* GetLogManager(){return m_pLogManager;}
	void PrintLog(int nVerbosity, CString strLog);
	CString GetESMLogFile(){return m_strESMLogFile;}
	void SetReSyncFlag(BOOL b){m_bReSyncFlag = b;}
	BOOL GetReSyncFlag(){return m_bReSyncFlag;}
	//==================================================================
	//== Live Viewer 
	//==================================================================
	BOOL m_bLiveViewLogFlag;		// liveview buffer log 남기기위해...

private:	
	CExtControlBar					m_wndLiveViewBar;
	CExtWFF<CESMLiveviewDlg>		m_wndLiveView;
	BOOL CreateLiveViewer(CESMSplashWnd &splash);
	inline void ShowLiveViewer(){m_wndLiveViewBar.ShowWindow(SW_SHOW);}
	inline void HideLiveViewer(){m_wndLiveViewBar.ShowWindow(SW_HIDE);}
public:
	CESMLiveviewDlg* GetLiveviewWnd()	{ return &m_wndLiveView; }
	//==================================================================
	//== Frame View
	//==================================================================	
private:
	CESMResizeControlBar		m_wndFrameBar;
	CExtWFF<CESMFrameViewer>	m_wndFrame;
	BOOL CreateFrameViewer(CESMSplashWnd &splash);
	inline void ShowFrameViewer(){m_wndFrameBar.ShowWindow(SW_SHOW);}
	inline void HideFrameViewer(){m_wndFrameBar.ShowWindow(SW_HIDE);}	
public:
	CESMFrameViewer* GetFrameWnd()	{ return &m_wndFrame; }

	//==================================================================
	//== Frame View
	//==================================================================	
private:
	//CESMResizeControlBar		m_wndFrameExBar;
	//CExtWFF<CESMFrameViewerEx>	m_wndFrameEx;
	//BOOL CreateFrameViewerEx(CESMSplashWnd &splash);
	//inline void ShowFrameViewerEx(){m_wndFrameExBar.ShowWindow(SW_SHOW);}
	//inline void HideFrameViewerEx(){m_wndFrameExBar.ShowWindow(SW_HIDE);}	
public:
	//CESMFrameViewerEx* GetFrameExWnd()	{ return &m_wndFrameEx; }


	//==================================================================
	//== TimeLine Editor
	//==================================================================	
public:
	CESMResizeControlBar		m_wndTimeLineEditorBar;
	CExtWFF<CTimeLineEditor>	m_wndTimeLineEditor;
	BOOL CreateTimeLineEditor(CESMSplashWnd &splash);
	inline void ShowTimeLineEditor(){m_wndTimeLineEditorBar.ShowWindow(SW_SHOW);}
	inline void HideTimeLineEditor(){m_wndTimeLineEditorBar.ShowWindow(SW_HIDE);}		

	//==================================================================
	//== Property Viewer 
	//==================================================================
public:
	CExtControlBar				m_wndPropertyBar;
	CESMPropertyView			m_wndProperty;
	BOOL CreatePropertyViewer(CESMSplashWnd &splash);
	inline void ShowPropertyViewer(){m_wndPropertyBar.ShowWindow(SW_SHOW);}
	inline void HidePropertyViewer(){m_wndPropertyBar.ShowWindow(SW_HIDE);}	
	//==================================================================
	//== Network Viewer 
	//==================================================================
public:
	CExtControlBar				m_wndNetworkBar;
	CESMNetworkDlg				m_wndNetwork;
	BOOL CreateNetworkViewer(CESMSplashWnd &splash);
	inline void ShowNetworkViewer(){m_wndNetworkBar.ShowWindow(SW_SHOW);}
	inline void HideNetworkViewer(){m_wndNetworkBar.ShowWindow(SW_HIDE);}	

	//==================================================================
	//== Point List Viewer
	//==================================================================
private:
	CExtControlBar				m_wndPointListBar;
	CESMPointListDlg			m_wndPointList;
	BOOL CreatePointListViewer(CESMSplashWnd &splash);
	inline void ShowPointListViewer(){m_wndPointListBar.ShowWindow(SW_SHOW);}
	inline void HidePointListViewer(){m_wndPointListBar.ShowWindow(SW_HIDE);}	

	//==================================================================
	//== Effect Editor
	//== 2014-07-03 hongsu@esmlab.com
	//==================================================================
public:
	CESMEffectEditor			m_wndEffectEditor;
private:
	CESMResizeControlBar		m_wndEffectEditorBar;
	BOOL CreateEffectEditor(CESMSplashWnd &splash);
	inline void ShowEffectEditor(){m_wndEffectEditorBar.ShowWindow(SW_SHOW);}
	inline void HideEffectEditor(){m_wndEffectEditorBar.ShowWindow(SW_HIDE);}	
	//==================================================================
	//== Effect Frame Viewer
	//==================================================================	
private:
	CESMResizeControlBar			m_wndEffectFrameViewerBar;
	CESMFileMergeDlg					m_wndFIleMergeViewer;
	BOOL CreateEffectFrameViewer(CESMSplashWnd &splash);
	inline void ShowEffectFrameViewer(){m_wndEffectFrameViewerBar.ShowWindow(SW_SHOW);}
	inline void HideEffectFrameViewer(){m_wndEffectFrameViewerBar.ShowWindow(SW_HIDE);}		

	//==================================================================
	//== Merge File Viewer
	//==================================================================
public:
	CESMResizeControlBar			m_wndFileMergeViewerBar;
	CESMEffectFrame					m_wndEffectFrameViewer;
	BOOL CreateFileMergeViewer(CESMSplashWnd &splash);
	inline void ShowFileMergeViewer(){m_wndFileMergeViewerBar.ShowWindow(SW_SHOW);}
	inline void HideFileMergeViewer(){m_wndFileMergeViewerBar.ShowWindow(SW_HIDE);}	

	//==================================================================
	//== Effect Previewer
	//==================================================================	
private:
	CESMResizeControlBar			m_wndEffectPreviewBar;
	CESMEffectPreview				m_wndEffectPreview;
	BOOL CreateEffectPreview(CESMSplashWnd &splash);
	inline void ShowEffectPreview(){m_wndEffectPreviewBar.ShowWindow(SW_SHOW);}
	inline void HideEffectPreview(){m_wndEffectPreviewBar.ShowWindow(SW_HIDE);}	

	//==================================================================
	//== ESM MessageBox 
	//==================================================================	
public:
	BOOL m_bLoadData;
	CTimeViewDlg* m_TimeVieDlg;

public:
	CESMMakeMovieMgr* m_pMovieThreadMgr;
	CESMMakeMovieFileMgr* m_pMovieMakeFileMgr;

	//CESMMovieRTThread* m_pMovieRTThread;
	CESMMovieRTSend*   m_pMovieRTSend;
	CESMMovieRTSend60p*   m_pMovieRTSend60p;

	//180913 hjcho
	CESMMovieRTMovie*	m_pMovieRTMovie;
	int m_nRTThreadTime;
private:

	//==================================================================
	//== Open Recently 
	//==================================================================
private:
	CStringList m_listRecentlyPath;
	afx_msg void OnOpenSelectedFile(UINT nID);
	afx_msg void OnUpdateOpenSelectedFile(CCmdUI *pCmdUI);
	void RecentlyFileAdd(CString strFilePath);
	void RecenltyFileWriteReg();
	void RecenltyFileRemove();
	void RecentlyFileLoad();	
	void RecentlyFilelOpen(CMenu * pWindowMenu, CMenu * pViewMenu);	
	void UpdateRecentlyFile();
	//==================================================================
	//== Version Check  
	//==================================================================
private:
	WORD m_wLocalver[4];
	BOOL CheckVersions();
	BOOL GetAppPath(OUT CString& strAppPath);
	CString GetFileServerPath();
	BOOL GetFileVersion(CString strFullPath, WORD& wMajor1, WORD& wMajor2, WORD& wMinor1, WORD& wMinor2);
	CString GetVersion();
	void ConnectMySql();


	//==================================================================
	//==
	//== Message Center
	//-- Date	 : 2009-03-30
	//-- Owner	 : hongsu.jung
	//-- Comment : MESSAGE CENTER
	//==
	//==================================================================
private:
	LRESULT OnSDIMsg(WPARAM w, LPARAM l);		//-- 2013-04-24 | Transfer to DSCMgr
	LRESULT OnESMLogMsg(WPARAM w, LPARAM l);
private:
	LRESULT OnESMMsg(WPARAM w, LPARAM l);
	LRESULT MsgDSC	(ESMEvent* pMsg);			//-- 2013-04-22
	LRESULT MsgList	(ESMEvent* pMsg);			//-- 2013-04-22
	LRESULT MsgView	(ESMEvent* pMsg);			//-- 2013-04-24
	LRESULT MsgOpt	(ESMEvent* pMsg);			//-- 2013-05-07
	LRESULT MsgFrame(ESMEvent* pMsg);			//-- 2013-09-25
	LRESULT MsgNet	(ESMEvent* pMsg);			//-- 2013-09-30
	LRESULT MsgMovie(ESMEvent* pMsg);			//-- 2014-06-16
	LRESULT MsgEffect(ESMEvent* pMsg);			//-- 2014-07-15

	LRESULT OnESMServerMsg(WPARAM w, LPARAM l);	//jhhan 16-10-19 

	LRESULT MsgRemote(ESMEvent* pMsg);			//-- 2018-02
	LRESULT MsgReferee	(ESMEvent* pMsg);			//-- 2018-07-19

	//------------------------------------------------------------------------------
	// Overrides
	// ClassWizard generated virtual function overrides
	//------------------------------------------------------------------------------
	//{{AFX_VIRTUAL(CMainFrame)
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);	
	virtual void ActivateFrame(int nCmdShow = -1);

	static unsigned WINAPI DSCPictureCaptureThreadGH5(LPVOID param);
	static unsigned WINAPI DSCPictureCaptureThread(LPVOID param);
	static unsigned WINAPI DSCMovieCaptureThread(LPVOID param);
	static unsigned WINAPI DSCSyncTimeThread(LPVOID param);
	static unsigned WINAPI RecordDoneThread(LPVOID param);
	static unsigned WINAPI DSCTestingThread(LPVOID param);

	void DrawTestingResult(BOOL bAuto);
	//}}AFX_VIRTUAL
	// Implementation	
	// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	//}}AFX_MSG
	afx_msg LRESULT OnPreparePopupMenu( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnConstructPopupMenuCB( WPARAM wParam, LPARAM lParam );
	afx_msg void OnUpdateControlBarMenu(CCmdUI* pCmdUI);
	afx_msg BOOL OnBarCheck(UINT nID);	

	afx_msg void OnMove(int x, int y);

public:
	//--------------------------------------------
	// BASIC
	//--------------------------------------------
	afx_msg void OnToolsOptions();	
	afx_msg void OnToolsHelp();
	afx_msg void OnToolsThemeSave();	//2012-10-12
	//-----------------------------------------------------------
	//-- PROJECT MANAGEMENT
	//-----------------------------------------------------------	
	afx_msg void OnUpdateSetRandomInterval(CCmdUI *pCmdUI);
	afx_msg void OnUpdateToolBasic(CCmdUI *pCmdUI);		
	//==================================================================
	//== Event (DSC) | MainFrmEvent
	//-- Date	 : 2013-04-23
	//-- Owner	 : hongsu.jung	
	//==================================================================
	afx_msg void OnDSCConnection		();
	afx_msg void OnDSCFocussing			();
	afx_msg void OnDSCSetFocus			();
	afx_msg void OnDSCFocussRelease		();
	afx_msg void OnFocusSaveSeq			();
	afx_msg void OnDSCAdjust			();
	afx_msg void OnDSCSyncTime			();
	afx_msg void OnDSCTakePicture		();
	afx_msg void OnDSCMovieCapture		();
	void DSCMovieCapture				();
	afx_msg void OnDSCMoviePause		();
	void DSCMoviePause					();
	afx_msg void OnDSCMovieResume		();
	afx_msg void OnDSCTesting			();
	afx_msg void OnUpdateDSCTool(CCmdUI *pCmdUI);
	afx_msg void OnExecuteMode			();
	afx_msg void OnUpdateControlExecuteMode	(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCapturePause(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCaptureResume(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCapture(CCmdUI* pCmdUI);
	//==================================================================
	//== Event (DSC) | MainFrmEvent
	//-- Date	 : 2013-04-23
	//-- Owner	 : hongsu.jung	
	//==================================================================	
	afx_msg void OnFocusModeCAF			();
	afx_msg void OnFocusModeMF			();	
	//==================================================================
	//== Control (DSC) | MainFrmCtrl
	//-- Date	 : 2013-10-02
	//-- Owner	 : hongsu.jung	
	//==================================================================	
	afx_msg void OnLoadRecordProfile			();
	afx_msg void OnLoadRecordProfileDirect		();
	afx_msg void OnLoadRecordProfilenBackup		();
	afx_msg void OnSaveRecordProfile			();	
	afx_msg void OnLoadAdjustProfile			();
	afx_msg void OnSaveAdjustProfile			();	
	afx_msg void OnSaveSyncTestResult			();	

	void OnSaveAdjustProfile2nd(void* pAdj);
	void LoadRecordProfileInit(BOOL bBackup = FALSE);
	void LoadRecordProfile(CString strFileName);
	void SaveRecordProfile(CString strFileName);
	BOOL DscGH5Initialize();
	int DscSyncTime();
	int DscSyncTime2();
	int DscSyncTest();
	int DscReSync();
	void DscReSyncGetTick();
	void LoadAdjustProfile(CString strFileName);
	void SaveAdjustProfile(CString strFileName, void* pAdj = NULL);
	void SaveSyncTestFile(CString strFileName);
	void SaveFocusValue();
	BOOL CheckConcert();
	void BackupRecordFile();

	//==================================================================
	//== Test
	//==================================================================
	void OnESMTimeLineSave();
	void OnESMTimeLineLoad();
	void OnESMMovieTemplateLoad();
	void OnESMMovieTemplateSave();
	void OnESMVerificationInfo();
	void OnESMFixedxApply();
	void OnESMDockingApply();
	//==================================================================
	static unsigned WINAPI CUDASupport(LPVOID param);
	static unsigned WINAPI SyncTestingThread(LPVOID param);
	//==================================================================
	static unsigned WINAPI OnDSCReMovieCapture(LPVOID param);
	DECLARE_MESSAGE_MAP()

public:
	//CMiLRe 20160128 카메라 연결 count
	int m_CarmerTotalCount;

	BOOL m_bDBConnect;

public:
	void ResetSyncData();
	void MakeFrameMemory();
	int m_nFrameMargin;
	//vector<void*> m_pImgArray;
	vector<ESMFrameArray*> m_pImgArray;
	BOOL m_bFrameDecoding;
	//2016-06-17 hjcho
	track_info t_info;
	track_info GetTrackinfo();
	void SetTrackinfo(track_info tinfo);

	//2016-12-08 hjcho FrameViewer Adjust
	BOOL mFrameAdjSet;
	void SetFrameAdjust(BOOL mSet);
	BOOL GetFrameAdjust();

	void SetSelectPage(CString strPage);
	int  GetSelectPage();

	//16-08-09 hjcho
	afx_msg void OnDSCPrism();
	//16-12-09 wgkim
	afx_msg void OnDSCTemplate();
	afx_msg void OnUtilityArcCalculator();
	afx_msg void OnUtilityActualMeasurement();
	afx_msg void OnRecalibration();

	//

public:
	BOOL m_bLoadFrame;

	//==================================================================
	//-- Date	 : 2016/07/13
	//-- Owner	 : Lee SangA	
	//-- Work    : Context of option
	//==================================================================
public:
	int m_nFKey;

	//jhhan 16-09-06
public:
	BOOL m_bLoadRecord;
	static unsigned WINAPI OnDSCReConnect(LPVOID param);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	//jhhan 16-10-11 Making Delete
public:
	vector<CString> m_arrMakeDeleteDSC;
	vector<CString> m_MakeDeleteDSCList;
	vector<CString> GetMakeDeleteDSC();
	vector<CString> GetMakeDeleteDSCList();
	void SetMakeDeleteDSCList(vector<CString> list);
	BOOL MakeDeleteDSCList(CString str);

	//jhhan 16-10-18
	void ExcelSaveStatus();
	void OnHttpSender(int nMode);
	void OnHttpCommand(int nMode);
	void OnHttpSync(int nIdx);
	//jhhan 16-11-07
	/*DWORD GetProcessPID(CString strProcess);
	HWND GetWndHandle(DWORD dwPID);*/

	//==================================================================
	//== NetworkEx Viewer 
	//==================================================================
public:
	CExtControlBar				m_wndDashBoardBar;
	CESMDashBoardDlg			m_wndDashBoard;
	BOOL CreateDashBoardViewer(CESMSplashWnd &splash);
	inline void ShowDashBoardViewer(){m_wndDashBoardBar.ShowWindow(SW_SHOW);}
	inline void HideDashBoardViewer(){m_wndDashBoardBar.ShowWindow(SW_HIDE);}
	//==================================================================
	//== NetworkEx Viewer 
	//==================================================================
public:
	CExtControlBar				m_wndNetworkExBar;
	CESMNetworkExDlg			m_wndNetworkEx;
	BOOL CreateNetworkExViewer(CESMSplashWnd &splash);
	inline void ShowNetworkExViewer(){m_wndNetworkExBar.ShowWindow(SW_SHOW);}
	inline void HideNetworkExViewer(){m_wndNetworkExBar.ShowWindow(SW_HIDE);}

	//4DA GPU메이킹시 파일FLAG 기록 초기 파일 생성
	void InitFileini(CString strSourceDSC = _T(""));

	//Adjust 초기 로드 관련
	BOOL m_bLoadAdj;

	/*
	//SensorOut 파일 기록 초기파일 생성
	void InitSensorOut(CString strDate);
	BOOL SetSensorOut(CString strDate, CString strDSCID, int nStartTime, int nTimeStamp);
	void GetSensorOut(CString strDate);
	*/

	//jhhan 16-11-23
	void OnFrameSelectView(BOOL bAuto = TRUE);

	//hjcho 18-09-19
	void OnFrameAutoSelect();
	static unsigned WINAPI _ThreadAutoDetect(LPVOID param);
	static unsigned WINAPI _ThreadLaunchExternalDetect(LPVOID param);

	BOOL m_bAutoDetect;

	//jhhan 16-12-05
	void InitTitleBar();

	//wgkim@esmlab.com	
	CESMTemplateMgr *m_pTemplateMgr;
	CESMRecalibrationMgr *m_pRecalibrationMgr;
	cv::Point templatePoint;
	void LoadTemplateSquaresProfile();
	int m_nSelectedFrameNumber;

	//hjcho 17-01-10
	//CESMProcessAJA m_AJA;
	CString Load4DAPath(CString strSourceDSC);

	//hjcho 17-09-01
	CString m_strEventEmail;

	//wgkim 17-12-22
	CESMBackup* m_pBackupMgr;

	//wgkim 18-07-03
	CESMAutoAdjustMgr *m_pAutoAdjustMgr;
	afx_msg void OnAutoAdjust();

	//hjcho 18-07-19
	CESMRefereeReading* m_pRefereeMgr;
	//==================================================================
	//== Movie List
	//==================================================================
public:
	CExtControlBar				m_wndMovieListBar;
	CESMMovieListDlg			m_wndMovieList;
	BOOL CreateMovieListViewer(CESMSplashWnd &splash);
	inline void ShowMovieListViewer(){m_wndMovieListBar.ShowWindow(SW_SHOW);}
	inline void HideMovieListViewer(){m_wndMovieListBar.ShowWindow(SW_HIDE);}

	//==================================================================
	//== TimeLine Processor
	//==================================================================	
public:
	CESMResizeControlBar		m_wndTimeLineProcessorBar;
	CExtWFF<CTimeLineProcessor>	m_wndTimeLineProcessor;

	BOOL CreateTimeLineProcessor(CESMSplashWnd &splash);
	inline void ShowTimeLineProcessor(){m_wndTimeLineProcessorBar.ShowWindow(SW_SHOW);}
	inline void HideTimeLineProcessorr(){m_wndTimeLineProcessorBar.ShowWindow(SW_HIDE);}	

	void RunESMMovieRTTThread(CString strIP, CString strDSC);
	void RunESMMovieRTSend(CString strIP,CString strDSC,BOOL bRefereeMode = FALSE);
	void RunESMMovieRTSend60p(CString strIP,CString strDSC);
	void RunESMMovieRTMovie(CString strIP, CString strDSC,int nIdx,int nFrameRate = 30);
public:
	BOOL m_bTestFlag;	
	int m_nGridYPoint;
	
	//jhhan 170324
	CRITICAL_SECTION m_criRecord;
	CMap<CString, LPCTSTR, UINT, UINT&> m_mapRecordFrame;
	//void SetRecordFrameInfo(CString strKey, UINT nValue);
	//BOOL GetRecordFrameInfo(CString strKey);

	void RunESMDump(BOOL bRun = TRUE);
	int m_nSenderGap;
	int m_nFPS;

	void SetMakeDelete(CString strDSC,BOOL bDelete = FALSE);
	void SetFrameRate(int nMovieSize);

	//hjcho 170728
	int m_nRecordState;
	//hjcho 171107
	BOOL m_bTrcAdjustLoad;

	void MakeFolder(CString strPath);
	static unsigned WINAPI RunTranscode(LPVOID param);
public:
	CObArray	m_ar4DPRcList;
	BOOL Add4DPMgr(CString strDscId);
	void DeleteAll4DPMgr(int nRemoteID = 0);
	BOOL RequestData(CString strPath);

	CESMRCManager * m_pRemote;

	//wgkim 171110
	map<CString, int> m_nArrAvgGap;

	CString GetESMOptionFile() {return m_strConfig;}

	void SetInfoToMovieMgr();
	afx_msg void OnUtilityTrackingViewer();

	void GetFocusValueAll();
	void SetFocusSaveAllToFile(CString str="");

	void StartFocusValueToFile();
	void EndFocusValueToFile();

	void ReStartAgent();

	int m_nDevice;
	float m_flVerInfo;
	BOOL OnResync_RecStop();
	BOOL OnResync_RecStart();
	//180423 hjcho
	CESMStillImage m_stillImage;

	//180508 hjcho
	afx_msg void  OnUtilityAJAConnect();
	CESMAJANetwork *m_pAJANetwork;

	afx_msg void  OnUtilityCheckedPropertyInfo();

	CMapStringToString m_mapDirection;

	void OnRecording(char cDirection);
	void StopFileTransfer();
	void StartFileTransfer();

	CString GetCamModel();
	BOOL RecStopGetTickFinish();
	void StartReSync();
	
	static unsigned WINAPI DeleteCompact(LPVOID param);
	vector<int> m_nStoredTime;
	vector<int> m_nViewJump;
	//hjcho 181016
	int m_nRTSPWaitTime;
	BOOL m_bRecordSync;

	vector<DetectInfo> m_stDetectInfo;	

	PropertyDiffCount m_stPropertyDiffCount;
	void SetPropertyDiffCount(PropertyDiffCount values) { m_stPropertyDiffCount = values; }
	PropertyDiffCount GetPropertyDiffCount() { return m_stPropertyDiffCount; }
	void StartPropertyDiffCheck();

	PropertyMinMaxInfo m_stPropertyMinMaxInfo;
	void SetPropertyMinMaxInfo(PropertyMinMaxInfo values) { m_stPropertyMinMaxInfo = values; }
	PropertyMinMaxInfo GetPropertyMinMaxInfo() { return m_stPropertyMinMaxInfo; }
	void StartPropertyMinMaxInfoCheck();

	void OnSelectViewBar(int nTime, int nIdx);

	//181224 jhhan
private:
	map<CString, vector<ESMFrameArray*>*> m_ImageBuf;
	//map<CString, CESMm3u8 *> m_M3u8;
public:
	vector<ESMFrameArray*> * MakeFrame();
	void InsertFrame(CString strKey, vector<ESMFrameArray*> * pVetor = NULL);
	vector<ESMFrameArray*> * GetFrame(CString strKey);
	void ResetFrame();
	static unsigned WINAPI ResetFrameThread(LPVOID param);
	static unsigned WINAPI DeleteFrameThread(LPVOID param);
	BOOL m_bLoadPoint;
	void DeleteFrame();
	vector<CString> m_vFrame;

	/*void MakeM3u8(CString strKey);
	CESMm3u8 * GetM3u8(CString strKey);
	void DeleteM3u8();*/
	vector<CString> m_arrLive;
	void SetLiveList(vector<CString> list);
	vector<CString> GetLiveList();
	void InitLiveListParser();
	void SendToLiveList();

private:
	map<CString, CESMm3u8 *> m_FHDM3u8;
	map<CString, CESMm3u8 *> m_UHDM3u8;
public:
	void MakeFhdM3u8(CString strKey);
	CESMm3u8 * GetFhdM3u8(CString strKey);
	void MakeUhdM3u8(CString strKey);
	CESMm3u8 * GetUhdM3u8(CString strKey);
	void DeleteAllM3u8();
	void ResetAllM3u8();

	void MakeRefereeMovie(int nTime, CString vtMake, int nMax = 3);
	void OnEncoder(CString strFile, int nMode, void * pM3u8);
	static unsigned WINAPI MovieEncoderThread(LPVOID param);
public:
	CString m_strDefaultCameraID;		// 레코딩시 프로퍼티 기준 카메라.
	afx_msg void OnCommand4dliveconverter();
	afx_msg void OnHelpShortcutlist();

	BOOL KeyDownMsg(MSG* pMsg);
	int m_nResyncSucessCnt;
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};

struct THREAD_AUTO_DETECT
{
	THREAD_AUTO_DETECT()
	{
		pMainFrm = NULL;
		nCaptureTime = -1;
		nCount = -1;
	}
	CMainFrame* pMainFrm;
	int nCaptureTime;
	int nCount;
};

struct THREAD_ENCODER
{
	THREAD_ENCODER()
	{
		pParent = NULL;
		nMode = 0;
		pM3u8 = NULL;
	}
	CMainFrame* pParent;
	CString strFile;
	int nMode;
	CESMm3u8 * pM3u8;
};

#define TM_DSC_CONNECT		WM_USER+0x00fe
#define TM_DSC_DISCONNECT	WM_USER+0x00ff
#define TM_GAP_DSC_CONNECT		3000
#define TM_GAP_DSC_DISCONNECT	1000

//Test Code
#define TIMER_START_RECORD	WM_USER+0x0010
#define TIMER_SELECT_FRAME	WM_USER+0x0011
#define TIMER_VIEW_FRAME	WM_USER+0x0012
#define TIMER_MAKE_MOVIE	WM_USER+0x0013
#define TIMER_END_RECORD	WM_USER+0x0014

// auto focus value save seq
#define	SEQ_FOCUS_SAVE_1	WM_USER+0x8283
#define	SEQ_FOCUS_SAVE_2	WM_USER+0x8284
#define	SEQ_FOCUS_SAVE_3	WM_USER+0x8285
#define	SEQ_FOCUS_SAVE_4	WM_USER+0x8286
#define	SEQ_FOCUS_SAVE_5	WM_USER+0x8287
#define	SEQ_FOCUS_SAVE_6	WM_USER+0x8288

// Restart Agent
#define TIMER_RESTART_AGENT_START		WM_USER+0x8289
#define TIMER_RESTART_AGENT_DISCONNECT	WM_USER+0x8290
#define TIMER_RESTART_AGENT_END			WM_USER+0x8291

// record finish status
#define TIMER_RECORD_FINISH_STATUS		WM_USER+0x8292

// resync 
#define TIMER_RECORD_RESYNC_GET_TICK	WM_USER+0x8293
#define TIMER_RECORD_RESYNC_START		WM_USER+0x8294
#define TIMER_RECORD_RESYNC_GET_TICK_FINISH		WM_USER+0x8295
#define TIMER_RECORD_REC_GET_TICK_FINISH		WM_USER+0x8296	// gettick이 끝나지 않으면 기다려야한다.
#define TIMER_RECORD_RESYNC_GET_TICK_REQUEST	WM_USER+0x8297	// gettick요청 3번..

