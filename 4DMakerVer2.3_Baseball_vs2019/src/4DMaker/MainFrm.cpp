/////////////////////////////////////////////////////////////////////////////
//
//	MainFrm.cpp: implementation of the TesESMuarantee Fucntion Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  ESMLab, Inc., and may not be copied
//  nor disclosed except upon written agreement
//  by ESMLab, Inc..
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2009-02-01
// @Date	2011-10-07
// @Note	ver 4.0
// @ver.1.0	4DMaker
// @Date	2013-04-22
// 
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "4DMaker.h"
#include "MainFrm.h"
#include <..\Src\ExtDockBar.h>
#include <..\Src\ExtControlBarTabbedFeatures.h>
#include "WndThinFrame.h"

//-- 2009-07-07	//-- REGISTRY
#include "ESMRegistry.h"
#include "ESMIni.h"
//-- 2013-04-24 | SDI Manager
#include "SdiDefines.h"

//-- service treatment
#include <winsvc.h.>
//-- 2009-11-19 //-- ProcessControl
#include "ESMProcessControl.h"
#include "ESMUtil.h"
//-- TimeTrace
#include <sys/timeb.h>
#include <time.h>

#include "DSCMgr.h"

#define GRAPIC_BOARD_NAME  _T("NVIDIA GeForce GTX 1060 3GB")
#define GRAPIC_BOARD_NAME2 _T("NVIDIA GeForce GTX 760")
#define GRAPIC_BOARD_NAME3 _T("NVIDIA GeForce GTX 1050 Ti")
#define GRAPIC_BOARD_NAME4 _T("NVIDIA GeForce GTX 1060")
#define GRAPIC_BOARD_NAME5  _T("NVIDIA GeForce GTX 1060 6GB")

#include "ESMFileOperation.h"

#include <TlHelp32.h>

CMutex g_mtxFv(FALSE, _T("Frame"));

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame
IMPLEMENT_DYNAMIC( CMainFrame, CFrameWnd )
BEGIN_MESSAGE_MAP( CMainFrame, CFrameWnd )
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP	
	ON_REGISTERED_MESSAGE( CExtPopupMenuWnd::g_nMsgPrepareMenu, OnPreparePopupMenu )
	ON_REGISTERED_MESSAGE( CExtControlBar::g_nMsgConstructPopupMenu, OnConstructPopupMenuCB )
	//--------------------------------------------
	// CONTROLS
	//--------------------------------------------
	//-- MENU
	ON_COMMAND_EX(ID_VIEW_MENUBAR,					OnBarCheck)
	ON_UPDATE_COMMAND_UI(ID_VIEW_MENUBAR,			OnUpdateControlBarMenu)		
	ON_COMMAND_EX(ID_VIEW_UI_LOOK_BAR,				OnBarCheck)
	ON_UPDATE_COMMAND_UI(ID_VIEW_UI_LOOK_BAR,		OnUpdateControlBarMenu)		
	//-- TOOLBAR
	ON_COMMAND_EX(ID_TOOLBARS_CAPTURE,				OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_TOOLBARS_CAPTURE,		OnUpdateControlBarMenu)	
	ON_COMMAND_EX(ID_TOOLBARS_CONTROL,				OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_TOOLBARS_CONTROL,		OnUpdateControlBarMenu)		
	//-- OUTPUT
	ON_COMMAND_EX(ID_VIEW_BAR_OUTPUT,				OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_OUTPUT,		OnUpdateControlBarMenu)	
	//-- Liveview
	ON_COMMAND_EX(ID_VIEW_BAR_LIVEVIEW,				OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_LIVEVIEW,		OnUpdateControlBarMenu)	
	//-- FRAME VIEW
	ON_COMMAND_EX(ID_VIEW_BAR_FRAME,				OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_FRAME,			OnUpdateControlBarMenu)	
	//-- FRAME VIEW
	ON_COMMAND_EX(ID_VIEW_BAR_FRAME_EX,				OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_FRAME_EX,			OnUpdateControlBarMenu)	
	//-- TIMELINE EDITOR
	ON_COMMAND_EX(ID_VIEW_BAR_TIMELINE,				OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_TIMELINE,		OnUpdateControlBarMenu)		
	//-- PROPERTY VIEW
	ON_COMMAND_EX(ID_VIEW_BAR_PROPERTY,				OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_PROPERTY,		OnUpdateControlBarMenu)	
	//-- NETWORK VIEW
	ON_COMMAND_EX(ID_VIEW_BAR_NETWORK,				OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_NETWORK,		OnUpdateControlBarMenu)	
	//-- POINTLIST VIEW
	ON_COMMAND_EX(ID_VIEW_BAR_POINTLIST,			OnBarCheck)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_POINTLIST,		OnUpdateControlBarMenu)
	//-- EFFECT EDITOR
	ON_COMMAND_EX(ID_VIEW_BAR_EFFECTEDITOR,			OnBarCheck)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_EFFECTEDITOR,		OnUpdateControlBarMenu)
	//-- EFFECT FRAME VIEW
	ON_COMMAND_EX(ID_VIEW_BAR_EFFECT_FRAME,			OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_EFFECT_FRAME,	OnUpdateControlBarMenu)	
	//-- EFFECT FRAME VIEW
	ON_COMMAND_EX(ID_VIEW_BAR_EFFECT_PREVIEW,			OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_EFFECT_PREVIEW,	OnUpdateControlBarMenu)	
	//-- FILE MERGE VIEW
	ON_COMMAND_EX(ID_VIEW_BAR_FILEMERGE,			OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_FILEMERGE,	OnUpdateControlBarMenu)	

	//-- NETWORK VIEW
	ON_COMMAND_EX(ID_VIEW_BAR_NETWORKEX,				OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_NETWORKEX,		OnUpdateControlBarMenu)	
	
	//-- MOVIE LIST VIEW
	ON_COMMAND_EX(ID_VIEW_BAR_MOVIELIST,				OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_MOVIELIST,		OnUpdateControlBarMenu)	

	//-- PROCESSOR VIEW
	ON_COMMAND_EX(ID_VIEW_BAR_TIMELINE_PROCESSOR,		OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_TIMELINE_PROCESSOR,		OnUpdateControlBarMenu)	

	//-- DASHBOARD VIEW
	ON_COMMAND_EX(ID_VIEW_BAR_DASHBOARD,				OnBarCheck)	
	ON_UPDATE_COMMAND_UI(ID_VIEW_BAR_DASHBOARD,		OnUpdateControlBarMenu)	
	
	ON_COMMAND(ID_BTN_OPTION,						OnToolsOptions		) 
	ON_COMMAND(ID_BTN_2D_CALIB,						OnDSCAdjust		) 

	//--------------------------------------------
	// COMMAND MENU
	//--------------------------------------------
	ON_UPDATE_COMMAND_UI(ID_IMAGE_FM_CAF		,	OnUpdateDSCTool)	
	ON_UPDATE_COMMAND_UI(ID_IMAGE_FM_MF			,	OnUpdateDSCTool)
	ON_UPDATE_COMMAND_UI(ID_IMAGE_CONNECT		,	OnUpdateDSCTool)	
	ON_UPDATE_COMMAND_UI(ID_IMAGE_SETFOCUS		,	OnUpdateDSCTool)	
	ON_UPDATE_COMMAND_UI(ID_IMAGE_FOCUSSING		,	OnUpdateDSCTool)	
	//ON_UPDATE_COMMAND_UI(ID_IMAGE_ADJUST		,	OnUpdateDSCTool)		
	ON_UPDATE_COMMAND_UI(ID_IMAGE_TAKEPICTURE	,	OnUpdateDSCTool)		
	ON_UPDATE_COMMAND_UI(ID_IMAGE_SYNCTIME		,	OnUpdateDSCTool)		
	ON_UPDATE_COMMAND_UI(ID_IMAGE_EXCUTEFILMMODE,	OnUpdateControlExecuteMode)	
	ON_UPDATE_COMMAND_UI(ID_IMAGE_MOVIEPAUSE	,	OnUpdateCapturePause)	
	ON_UPDATE_COMMAND_UI(ID_IMAGE_MOVIERESUME	,	OnUpdateCaptureResume)	
	ON_UPDATE_COMMAND_UI(ID_IMAGE_MOVIECAPTURE	,	OnUpdateCapture)	
	//--------------------------------------------
	// COMMAND MENU
	//--------------------------------------------
	ON_COMMAND(ID_IMAGE_OPTION				,OnToolsOptions		) 
	ON_COMMAND(ID_IMAGE_CONNECT				,OnDSCConnection	) 
	ON_COMMAND(ID_IMAGE_SETFOCUS			,OnDSCSetFocus		) 
	ON_COMMAND(ID_IMAGE_FOCUSSING			,OnDSCFocussing		) 
	ON_COMMAND(ID_IMAGE_FOCUS_RELEASE		,OnDSCFocussRelease	) 	
	ON_COMMAND(ID_IMAGE_FOCUS_SAVE_SEQ		,OnFocusSaveSeq	) 	
	ON_COMMAND(ID_IMAGE_ADJUST				,OnDSCAdjust		) 
	ON_COMMAND(ID_IMAGE_KZONE				,OnDSCPrism		) 
	ON_COMMAND(ID_IMAGE_TEMPLATE			,OnDSCTemplate	) 
	ON_COMMAND(ID_IMAGE_SYNCTIME			,OnDSCSyncTime	) 	
	ON_COMMAND(ID_IMAGE_TAKEPICTURE			,OnDSCTakePicture	) 	
	ON_COMMAND(ID_IMAGE_MOVIECAPTURE		,OnDSCMovieCapture	) 	
	ON_COMMAND(ID_IMAGE_MOVIEPAUSE			,OnDSCMoviePause	) 	
	ON_COMMAND(ID_IMAGE_MOVIERESUME			,OnDSCMovieResume	) 
	ON_COMMAND(ID_IMAGE_FM_CAF				,OnFocusModeCAF		)
	ON_COMMAND(ID_IMAGE_FM_MF				,OnFocusModeMF		)	
	ON_COMMAND(ID_IMAGE_RECORD_LOAD			,OnLoadRecordProfile)
	ON_COMMAND(ID_IMAGE_RECORD_LOAD_DIRECT	,OnLoadRecordProfileDirect)
	ON_COMMAND(ID_IMAGE_RECORD_LOAD_BACKUP	,OnLoadRecordProfilenBackup)
	ON_COMMAND(ID_IMAGE_RECORD_SAVE			,OnSaveRecordProfile)
	ON_COMMAND(ID_IMAGE_ADJUST_LOAD			,OnLoadAdjustProfile)
	ON_COMMAND(ID_IMAGE_ADJUST_SAVE			,OnSaveAdjustProfile)
	ON_COMMAND(ID_IMAGE_EXCUTEFILMMODE		,OnExecuteMode		)
	ON_COMMAND(ID_IMAGE_SYNC_TEST		,OnSaveSyncTestResult		)
	
	//wgkim@esmlab.com
	//--------------------------------------------
	// UTIL_MENU
	//--------------------------------------------
	ON_COMMAND(ID_UTILITY_ARCCALCULATOR		,OnUtilityArcCalculator	)
	ON_COMMAND(ID_UTILITY_ACTUALMEASUREMENT, OnUtilityActualMeasurement)
	ON_COMMAND(ID_IMAGE_RECALIBRATION		,OnRecalibration)
	ON_COMMAND(ID_IMAGE_AUTOADJUST			,OnAutoAdjust)
	
	//--------------------------------------------
	// ESMMOVIE_TEST
	//--------------------------------------------
	ON_COMMAND(ID_HELP_ESMTIMELINESAVE				,OnESMTimeLineSave	) 	
	ON_COMMAND(ID_HELP_ESMTIMELINELOAD				,OnESMTimeLineLoad	) 	
	ON_COMMAND(ID_HELP_MOVIETEMPLATELOAD	,OnESMMovieTemplateLoad		) 	
	ON_COMMAND(ID_HELP_MOVIETEMPLATESAVE	,OnESMMovieTemplateSave) 	
	ON_COMMAND(ID_HELP_VERIFY4DM			,OnESMVerificationInfo) 	
	ON_COMMAND(ID_COLORLOAD_FIXEDMODE			,OnESMFixedxApply) 	
	ON_COMMAND(ID_COLORLOAD_DOCKINGMODE			,OnESMDockingApply) 	
	

	//-- Detect USB Connection
	ON_MESSAGE(WM_DEVICECHANGE				,OnChangeConnect)
	//-------------------------------------------------------------
	//--
	//-- 2008-06-20
	//-- MAIN MESSAGE	
	//-- All TesESMuarantee Message Main
	//--
	//-------------------------------------------------------------
	ON_MESSAGE(WM_ESM,	OnESMMsg)
	ON_MESSAGE(WM_SDI,	OnSDIMsg)
	ON_MESSAGE( WM_ESM_LOG, OnESMLogMsg)
	ON_WM_TIMER()
	ON_MESSAGE(WM_ESM_SERVER, OnESMServerMsg)
	ON_COMMAND(ID_UTILITY_TRACKINGVIEWER, &CMainFrame::OnUtilityTrackingViewer)
	ON_COMMAND(ID_UTILITY_AJACONNECT,&CMainFrame::OnUtilityAJAConnect)
	ON_COMMAND(ID_UTILITY_CHECKEDPROPERTYINFO,&CMainFrame::OnUtilityCheckedPropertyInfo)
	ON_WM_MOVE()
	ON_COMMAND(ID_COMMAND_4DLIVECONVERTER, &CMainFrame::OnCommand4dliveconverter)
	ON_COMMAND(ID_HELP_SHORTCUTLIST, &CMainFrame::OnHelpShortcutlist)
	ON_WM_ERASEBKGND()
	END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

#define MAX_FRAME_THUMBNAIL 150


/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction
//------------------------------------------------------------------------------ 
//! @brief		CMainFrame construction
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
CMainFrame::CMainFrame()
:
m_pLogManager	(NULL),
m_pOptionDlg	(NULL),
m_bClose		(FALSE),
m_FileMgr		(NULL),
m_cudasupport	(FALSE),
m_bGPUDecodeSupport (FALSE),
m_pMultiplexerDlg(NULL),
m_bSyncThread(FALSE),
m_bResumeSyncTest(FALSE),
m_bSyncTestAuto(FALSE),
m_nFPS(MOVIE_FPS_UNKNOWN),
m_pRemoteCtrlDlg(NULL),
m_nInitialTime(0),
m_nMakeEndTime(0),
m_nMakeStartTime(0),
m_nMakeGapTime(0),
m_bPropertyCheck(TRUE),
m_nDevice(0),
m_pCurrentObj(NULL),
m_bSyncObj(FALSE),
m_bEditObj(TRUE),
m_bFirst(FALSE),
m_pRTSPStateViewDlg(NULL)
{
	//2016/07/13 Lee SangA Load page number from file
// 	CESMIni ini;
// 	CString strFile = _T("C:\\Program Files\\ESMLab\\4DMaker\\config\\4DMaker.info");
// 	if(!ini.SetIniFilename(strFile));

	g_PaintManager.InstallPaintManager(RUNTIME_CLASS(CExtPaintManagerOffice2010_R2_Black)); //CExtPaintManagerOffice2007_R1)); //CExtPaintManagerOffice2007_Silver));	// CExtPaintManagerOffice2007_R1
	//g_PaintManager.InstallPaintManager(RUNTIME_CLASS(CExtPaintManagerOffice2007_Black)); //CExtPaintManagerOffice2007_R1)); //CExtPaintManagerOffice2007_Silver));	// CExtPaintManagerOffice2007_R1
	// window placement persistence
	::memset( &m_dataFrameWP, 0, sizeof(WINDOWPLACEMENT) );
	m_dataFrameWP.length = sizeof(WINDOWPLACEMENT);
	m_dataFrameWP.showCmd = SW_HIDE;	
	m_TimeVieDlg = NULL;
	m_FileMgr = new CESMFileMgr(this);
	m_pImgLoader = new CImageLoader();	//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가

	for(int i= 0; i < 4 ; i++)
		m_wLocalver[i] = 0;	
	m_CaptureCount = 0;
	m_DelayDirection = DSC_REC_DELAY_FORWARDDIRECTION; 
	m_nRecState = DSC_REC_STOP;
	m_bReStartFalg = FALSE;
	m_bMovieSaveDone = TRUE;
	m_bCheckGPU  = FALSE;
	m_bIsMakeMovie = FALSE;
	m_nScrollY = 0;
	m_nSelectPointNum = 0;
	m_nGPUMakeFileCount = 0;
	m_nViewPointNum = 0;
	m_strSelectDSC = _T("");
	m_bFirstConnect = TRUE;
	m_bMakingInfo = FALSE;

	//CMiLRe 20160128 카메라 연결 count
	m_CarmerTotalCount = 1;
	m_bDBConnect = FALSE;

	m_pMovieThreadMgr = NULL;
	m_pMovieThreadMgr = new CESMMakeMovieMgr();
	m_pMovieThreadMgr->CreateThread();

	m_pMovieMakeFileMgr = NULL;
	m_pMovieMakeFileMgr = new CESMMakeMovieFileMgr();
	m_pMovieMakeFileMgr->CreateThread();

	/*m_pMovieRTThread = NULL;
	m_pMovieRTThread = new CESMMovieRTThread();
	m_pMovieRTThread->SetParent(this);*/

	m_pMovieRTSend = NULL;
	m_pMovieRTSend = new CESMMovieRTSend();
	m_pMovieRTSend->SetParent(this);
	
	m_pMovieRTSend60p = NULL;
	m_pMovieRTSend60p = new CESMMovieRTSend60p();
	m_pMovieRTSend60p->SetParent(this);

	m_pMovieRTMovie = NULL;
	m_pMovieRTMovie = new CESMMovieRTMovie();
	m_pMovieRTMovie->SetParent(this);
	m_pMovieRTMovie->SetClock(&m_Clock);
	//TRACE("%x\r\n",&m_Clock);
	/*m_pMovieRTThread->Set4DAInfo(_T(""), _T(""));
	m_pMovieRTThread->CreateThread();*/
	
	m_bFrameDecoding = FALSE;
	m_nFrameMargin = 11;

	m_DeleteDSCList.clear();

	m_bLoadFrame = FALSE;
	//Auto_start = 0;
	//tmp_start = 0;
	//2016/07/13 Lee SangA Load page number from file
	//m_nFKey = (_ttoi(ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE)) + 1) * 10;

	//Image Memory
	MakeFrameMemory();

	//jhhan 16-09-06
	m_bLoadRecord = FALSE;

	//jhhan 16-11-18
	m_bLoadAdj = FALSE;

	//hjcho 16-12-08
	mFrameAdjSet = FALSE;

	//wgkim 17-01-02
	m_pTemplateMgr = NULL;
	m_pTemplateMgr = new CESMTemplateMgr();		
	m_pRecalibrationMgr = new CESMRecalibrationMgr();

	m_bTestFlag = TRUE;
	//jhhan 170315
	m_nGridYPoint = 0;
	m_nSenderGap = 0;

	m_bStart = 0;//(CAMREVISION)
	m_bNextStatus = FALSE;
	m_bMakeMovie = FALSE;
	m_bSyncFlag = FALSE;
	m_bMakeAdjust = FALSE;
	InitializeCriticalSection(&m_criRecord);

	//hjcho 170728
	m_nRecordState = -1;

	//wgkim 170728
	m_nSelectedFrameNumber = 0;
	m_DevChangeCount	= 0;

	//jhhan 171107
	m_pRemote = NULL;

	//hjcho 171107
	m_bTrcAdjustLoad = FALSE;

	m_nRTThreadTime = GetTickCount();

	ESMUtil::KillProcess(_T("ESMGPUProcess.exe"));

	//wgkim 171222
	m_pBackupMgr = new CESMBackup;
	//wgkim 180202
	//m_bRecordFileSplit = 0;

	m_bStartFocusSaveToFileFlag = FALSE;
	m_flVerInfo = 0.0;

	//wgkim 180703
	m_pAutoAdjustMgr = new CESMAutoAdjustMgr();

	// resync	
	m_bReSyncFlag = FALSE;
	m_nReSyncTimerCount = 0;

	// gettick finish wait
	m_nGetTickRecFinishCount = 0;

	m_nResyncGetTickCount = 0;
	m_bGetTickFirstFlag = FALSE;
	m_bReSyncRecStopFinishFlag = FALSE;
	m_bTimerRecFinishRunning = FALSE;

	//180919 hjcho
	m_bAutoDetect = FALSE;
	m_nRTSPWaitTime = 60000;
	
	//jhhan 181023
	m_bRecordSync = FALSE;

	m_bLiveViewLogFlag = FALSE;

	m_bLoadPoint = TRUE;

	m_nResyncSucessCnt = 0;
}

void CMainFrame::MakeFrameMemory()
{
	for(int i = 0; i < MAX_FRAME_THUMBNAIL; i++)
	{
		ESMFrameArray* pdata = new ESMFrameArray;
		//IplImage* img = cvCreateImage(cvSize(1920 , 1080), IPL_DEPTH_8U, 3);

		pdata->pImg = NULL;
		pdata->bLoad = FALSE;
		m_pImgArray.push_back(pdata);
	}
}

void CMainFrame::OnClose() 
{
	m_wndNetworkEx.OnKillTimer();

	DeleteAll4DPMgr();

	if(m_TimeVieDlg)
	{
		m_TimeVieDlg->DestroyWindow();
		delete m_TimeVieDlg;
		m_TimeVieDlg = NULL;
	}

	//-- Stop Thread, Working
	m_bClose = TRUE;	
	SaveWindowStatus();
	//-- Waiting Close Thread	
	CWnd::OnClose();
}

//------------------------------------------------------------------------------ 
//! @brief		CMainFrame destruction
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
CMainFrame::~CMainFrame()
{	
	//Dump Program End
	//ESMUtil::KillProcess(_T("procdump64.exe"));
	ESMSetDump(FALSE);



	delete m_TimeVieDlg;
	//-- 2013-05-07 hongsu@esmlab.com
	//-- Delete Option Dlg
	/*if(m_pOptionDlg)
	{
		delete m_pOptionDlg;
		m_pOptionDlg = NULL;
	}*/
	//-- 2010-3-2 hongsu.jung
	//-- Delete Log
	if(m_pLogManager)
	{
		delete m_pLogManager;
		m_pLogManager = NULL;
	}		
	if( m_FileMgr )
	{
		delete m_FileMgr;
		m_FileMgr = NULL;
	}
	//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가
	if(m_pImgLoader)
	{
		delete m_pImgLoader;
		m_pImgLoader = NULL;
	}
	//-- 2014-05-26 hongsu@esmlab.com
	//-- Delete ESM Movie Manager
	if(m_pESMMovieMgr)
	{
		delete m_pESMMovieMgr;
		m_pESMMovieMgr = NULL;
	}
	//m_AJA._ThreadDestroy();
	if(m_pMultiplexerDlg)
	{
		delete m_pMultiplexerDlg;
		m_pMultiplexerDlg = NULL;
	}
	if(m_pRemoteCtrlDlg)
	{
		delete m_pRemoteCtrlDlg;
		m_pRemoteCtrlDlg = NULL;
	}

	//hjcho 171106
	if(m_pMovieRTSend)
	{
		delete m_pMovieRTSend;
		m_pMovieRTSend = NULL;
	}
	if(m_pMovieRTSend60p)
	{
		delete m_pMovieRTSend60p;
		m_pMovieRTSend60p = NULL;
	}
	/*if(m_pMovieRTThread)
	{
		delete m_pMovieRTThread;
		m_pMovieRTThread = NULL;
	}*/
	DeleteCriticalSection(&m_criRecord);

	if(m_pAJANetwork)
	{
		delete m_pAJANetwork;
		m_pAJANetwork = NULL;
	}
	if(m_pRefereeMgr)
	{
		delete m_pRefereeMgr;
		m_pRefereeMgr = NULL;
	}
	if(m_pRTSPStateViewDlg)
	{
		delete m_pRTSPStateViewDlg;
		m_pRTSPStateViewDlg = NULL;
	}

	ESMUtil::KillProcess(_T("ESMGPUProcess.exe"));
}

//------------------------------------------------------------------------------ 
//! @brief		  The framework calls this member function when an application requests that the Windows window 
//!             be created by calling the Create or CreateEx member function.
//! @date		
//! @attention	
//! @note	 	 
//! @return     must return 0 to continue. if returns -1, the window will be destroyed.
//------------------------------------------------------------------------------ 
int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	//-- turn off menu expanding
	CExtPopupMenuWnd::g_bMenuExpanding = false;
	CExtPopupMenuWnd::g_bMenuHighlightRarely = false;
	if( CExtNCW < CFrameWnd > :: OnCreate( lpCreateStruct ) == -1 )
		return -1;
	
	//-- 2009-05-14 hongsu.jung
	//-- Set Windows Handle
	ESMRegist();

	//-- 2013-04-22 hongsu.jung
	//-- Load Info
	LoadInfo();
		
	//Create SPLASH 2008-05-23
	CESMSplashWnd _splash(this, IDB_BITMAP_SPLASH);
	_splash.AddTextLine(_T("Load Infomations ..."));	
	
	//Create log 2009-03-30
	_splash.AddTextLine(_T("Start ESMLog ..."));
	if(!CreateLog())
	{
		_splash.AddTextLine(_T("Error ESMLog ..."));	
		return -1;
	}
	
	ESMLog(1,_T("\r\n"));
	_splash.AddTextLine(_T("Initializing ESM"));

	CString strText;
	strText.LoadString(IDR_MAINFRAME);
	_splash.AddTextLine(strText);

	_splash.AddTextLine(_T("Regist Application"));
	CWinApp * pApp = ::AfxGetApp();
	ASSERT( pApp != NULL );
	ASSERT( pApp->m_pszRegistryKey != NULL );
	ASSERT( pApp->m_pszRegistryKey[0] != _T('\0') );
	ASSERT( pApp->m_pszProfileName != NULL );
	ASSERT( pApp->m_pszProfileName[0] != _T('\0') );
	
	_splash.AddTextLine(_T("Update Profile"));
	g_CmdManager->ProfileSetup(pApp->m_pszProfileName,GetSafeHwnd());
	_splash.AddTextLine(_T("Update Menu"));
	VERIFY(g_CmdManager->UpdateFromMenu(pApp->m_pszProfileName, IDR_MAINFRAME));	
	VERIFY(g_CmdManager->UpdateFromToolBar(pApp->m_pszProfileName,IDR_TOOLBAR_DOC));	
	VERIFY(g_CmdManager->UpdateFromToolBar(pApp->m_pszProfileName,IDR_TOOLBAR_CTRL1));	
	VERIFY(g_CmdManager->UpdateFromToolBar(pApp->m_pszProfileName,IDR_TOOLBAR_CTRL2));
	VERIFY(g_CmdManager->UpdateFromToolBar(pApp->m_pszProfileName,IDR_TOOLBAR_CTRL3));
	VERIFY(g_CmdManager->UpdateFromToolBar(pApp->m_pszProfileName,IDR_TOOLBAR_PROP_CTRL));	
	VERIFY(g_CmdManager->UpdateFromToolBar(pApp->m_pszProfileName,IDR_TOOLBAR_NET_CTRL));	
	VERIFY(g_CmdManager->UpdateFromToolBar(pApp->m_pszProfileName,IDR_TOOLBAR_NETEX_CTRL));
	VERIFY(g_CmdManager->UpdateFromToolBar(pApp->m_pszProfileName,IDR_TOOLBAR_MOVIE_LIST));	
	

	_splash.AddTextLine(_T("Create Sub Viewers"));
	if(!CreateOutputViewer(_splash)
	|| !CreateToolBar(_splash)
	)
		return -1;
	
	//-- 2014-05-26 hongsu@esmlab.com
	//-- Create Movie Manager
	m_pESMMovieMgr = new CESMMovieMgr(AfxGetMainWnd()->GetSafeHwnd());

	//-- Set Save Path each timeline object movie
	m_pESMMovieMgr->SetSaveFolderPath(ESMGetServerRamPath());
	//-- Create Thread
	m_pESMMovieMgr->CreateThread();

	//----------------------------------------
	//-- Create Thread Engine
	//-- 2013-04-22 hongsu.jung	
	_splash.AddTextLine(_T("Create Main Viewers"));
	if(	!CreateNetworkViewer(_splash)
	|| !CreateMainView(_splash)
	|| !CreateLiveViewer(_splash)
	|| !CreateFrameViewer(_splash)	
	//|| !CreateFrameViewerEx(_splash)	
	|| !CreatePropertyViewer(_splash)	
	|| !CreateTimeLineEditor(_splash)
	|| !CreatePointListViewer(_splash)	
	|| !CreateEffectEditor(_splash)	
	|| !CreateEffectFrameViewer(_splash)	
	|| !CreateEffectPreview(_splash)
	|| !CreateFileMergeViewer(_splash)
	|| !CreateNetworkExViewer(_splash)
	|| !CreateTimeLineProcessor(_splash)
	|| !CreateMovieListViewer(_splash)
	|| !CreateDashBoardViewer(_splash)
	 
	) 
	return -1;

	//-- Create Status bar.
	if(!m_wndStatusBar.Create(this) || !m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT)))
	{
		ESMLog(0, _T("[ESM] Failed to create Status Bar"));
		return -1;      
	}
	m_wndStatusBar.m_bOuterRectInFirstBand = true;

	//-- Load last state. 2010-4-30 hongsu.jung
	LoadBarState(_splash);
		
	//hjcho 17-01-10
	//m_AJA.AJAStart();

	DISPLAY_DEVICE Display;
	WCHAR GraphicCard[128];
	 
	ZeroMemory( GraphicCard, sizeof( GraphicCard ) );
	memset( &Display, 0, sizeof( DISPLAY_DEVICE ) );
	Display.cb = sizeof( Display );

	int nCnt = 0;
	CString strGraphic;// = Display.DeviceString;
	
	while(1)
	{
		EnumDisplayDevices( NULL, nCnt, &Display, 0 );
		strGraphic = Display.DeviceString;
		
		if(strGraphic.GetLength() == FALSE)
			break;

		//ESMLog(5,strGraphic);
		if(strGraphic.Find(_T("NVIDIA GeForce")) != -1)
			break;

		nCnt++;
	}

	//CString strGraphic = Display.DeviceString;
#if 0
	if(strGraphic.CompareNoCase(GRAPIC_BOARD_NAME) == 0 || strGraphic.CompareNoCase(GRAPIC_BOARD_NAME2)== 0 
		|| strGraphic.CompareNoCase(GRAPIC_BOARD_NAME3) == 0 || strGraphic.CompareNoCase(GRAPIC_BOARD_NAME4) == 0
		|| strGraphic.CompareNoCase(GRAPIC_BOARD_NAME5) == 0)
#else
	if(strGraphic.Find(_T("NVIDIA GeForce")) != -1)
#endif
	{
		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, CUDASupport, this, 0, NULL);
		CloseHandle(hSyncTime);

		CString strLog;
		strLog.Format(_T("GPU Use [%s]"), strGraphic);
		_splash.AddTextLine(strLog);
		SetGPUDecodeSupport(TRUE);
	}

	/*if(strGraphic[0] == 'I' || strGraphic[0] == 'R' )
	{
		ESMLog(0,_T("Graphics device does not support Cuda."));
		SetCudaSupport(FALSE);
	}
	else
	{
		try
		{
			Mat src(1, 1, CV_8UC3);
			cuda::GpuMat* pImageMat = new cuda::GpuMat;
			pImageMat->upload(src);
			delete pImageMat;
			SetCudaSupport(TRUE);
			ESMLog(1,_T("Graphics device does support Cuda."));
		}
		catch (cv::Exception& e)
		{
			ESMLog(0,_T("Graphics device does not support Cuda."));
			SetCudaSupport(FALSE);
		}	

	}*/
	
	SetMakingUse(FALSE);

	//2016/07/29
	CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
	pMainWnd->m_wndDSCViewer.m_pListView->m_strPage = "F1";
	pMainWnd->m_wndDSCViewer.m_pListView->m_iPage = 0;

	//jhhan
	/*CString strDelay;
	int nDelayMSec = 0;
	nDelayMSec = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC);
	strDelay.Format(_T("A : %d"), nDelayMSec);*/
	ESMSetValue(ESM_VALUE_WAIT_SAVE_DSC_SEL, 0);
	pMainWnd->m_wndDSCViewer.m_pListView->m_strDelay = "A"/*strDelay*/;

	//hjcho 060830
	pMainWnd->m_wndFrame.m_pATView;
	if(ESMGetValue(ESM_VALUE_AUTODETECT))
		pMainWnd->m_wndDSCViewer.m_pListView->m_AUTO = "AUTO";
	else
		pMainWnd->m_wndDSCViewer.m_pListView->m_AUTO = "DEAUTO";

	//hjcho 161208 
	//pMainWnd->m_wndDSCViewer.m_pListView->m_FrameAdj = "-";
	pMainWnd->m_wndDSCViewer.m_pListView->Invalidate();

	//wgkim 170615
	if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT))
		m_wndDSCViewer.m_pListView->m_FrameAdj = "TEM";			
	else	
		m_wndDSCViewer.m_pListView->m_FrameAdj = "-";							

	m_nFKey = 10;

	CString strPagenum;
	strPagenum = "10";
	SetSelectPage(strPagenum);


	//-- Destroy Splash Window.
	_splash.AddTextLine(_T("Finish Initialize"));
	_splash.AddTextLine(_T("Close Splash Window"));	
	_splash.DestroyWindow();
	ESMEvent* pMsg = NULL;
	//-- 2013-09-10 hongsu@esmlab.com
	//-- Check Connection
	/*
	pMsg = new ESMEvent();
	pMsg->message = WM_ESM_LIST_CHECK;
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
	*/

	//-- 2013-09-10 hongsu@esmlab.com
	//-- Redraw Main View 
	pMsg = new ESMEvent();
	pMsg->message = WM_ESM_VIEW_REDRAW;	
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

	//-- Set Start Tick Count
	m_Clock.Start();

	if( m_TimeVieDlg == NULL)
	{
		m_TimeVieDlg = new CTimeViewDlg;
		m_TimeVieDlg->Create( this);
		m_TimeVieDlg->InitTime();
		m_TimeVieDlg->SetSeq1(_T("Sync Time Adjusting..."));
		m_TimeVieDlg->ShowWindow(FALSE);
		m_TimeVieDlg->OnPaint();

	}

	//-- 2014-07-18 hongsu@esmlab.com
	//-- Set Interactive View Pointer
	if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		m_wndEffectEditor.SetFrameView(&m_wndEffectFrameViewer);
		m_wndEffectFrameViewer.SetEffectEditor(&m_wndEffectEditor);
		m_wndEffectFrameViewer.SetPreview(&m_wndEffectPreview);
		ESMSetRecordingInfoint(_T("MakingFlag"), FALSE);

		ESMInitDirection();
	}
	else
	{
		//180119
		CESMFileOperation fo;
		CString strPath;
		strPath.Format(_T("%s"),ESMGetClientRamPath());
		fo.Delete(strPath, FALSE);
	}

	ConnectMySql();

	//Dump Program Start
	/*TCHAR strPath[MAX_PATH];
	GetModuleFileName( NULL, strPath, _MAX_PATH);
	PathRemoveFileSpec(strPath);
	ESMUtil::ExecuteProcess(strPath,_T("procdump64.exe"), _T("4DMaker.exe -t -e -h"));*/

	//jhhan 170413 KT Dump 해제(Agent or Processor) TEST CODE
	ESMSetDump(TRUE);

	//hjcho 일시 주석
	/*if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		ESMSetDump(TRUE);
	}
	else
	{
		if(ESMGetGPUMakeFile() != TRUE)
			ESMSetDump(TRUE);
		
	}*/
	//procdump -r -ma reportingservicesservice.exe D:\temp\procdump.dmp
	//ESMUtil::ExecuteProcess(strPath,_T("procdump64.exe"), _T("-r -ma 4DMaker.exe F:\\Project\\procdump.dmp"));
	
	ESMLog(5, _T("Div Frame : %d"), ESMGetValue(ESM_VALUE_DIV_FRAME));
	
	m_pMultiplexerDlg = new CESMUtilMultiplexerDlg;
	m_pMultiplexerDlg->Create(IDD_UTIL_MULTIPLEXER,this);
	m_pMultiplexerDlg->ShowWindow(SW_HIDE);

	/*if(m_pRemoteCtrlDlg == NULL)
	{
		m_pRemoteCtrlDlg = new CESMUtilRemoteCtrl;
		m_pRemoteCtrlDlg->Create(IDD_UTIL_REMOTECTRL,this);
		m_pRemoteCtrlDlg->ShowWindow(SW_HIDE);
	}*/

	//jhhan 171107
	if(m_pRemote == NULL)
	{
		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
		{
			if(ESMGetRemoteUse() != TRUE)
			{
				m_pRemote = new CESMRCManager();
				m_pRemote->StartServer(TRUE);
				m_pRemote->CreateThread();
			}
			else
			{
				CString strIP = ESMGetRemoteIP();
				if(strIP.CompareNoCase(_T("127.0.0.1")) != 0)
				{
					m_pRemote = new CESMRCManager(strIP, 0);
					m_pRemote->CreateThread();

					m_pRemote->ConnectToRemote();
				}
				
			}
		}
	}

	m_wndDashBoard.LoadInfo();

	int nKey = ESMGetFuncKey();

	MakeShortCut(nKey);

#ifndef _DEBUG
	OnESMFixedxApply();
#endif
	

	//180607 hjcho
	m_pAJANetwork = new CESMAJANetwork;

	//180719
	m_pRefereeMgr = new CESMRefereeReading;

	
	if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
	{
		CString strTitle;
		GetWindowText(strTitle);
		strTitle.Append(_T(" - Making"));
		SetWindowText(strTitle);
	}else if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
	{
		CString strTitle;
		GetWindowText(strTitle);
		strTitle.Append(_T(" - Recording"));
		SetWindowText(strTitle);
	}

	//190227 hjcho
	m_pRTSPStateViewDlg = new CESMRTSPStateViewer;
	m_pRTSPStateViewDlg->Create(IDD_DIALOG_RTSPSTATE_VIEW,this);
	m_pRTSPStateViewDlg->ShowWindow(SW_HIDE);

	return 0;
}
unsigned WINAPI CMainFrame::CUDASupport(LPVOID param)
{
	CMainFrame *pMain = (CMainFrame*)param;
	
	int start = GetTickCount();
	Mat tmp(1,1,CV_8UC3);
	cuda::GpuMat *pImageMat = new cuda::GpuMat;
	pImageMat->upload(tmp);
	//cuda::copyMakeBorder(*pImageMat,*pImageMat,0,0,0,0,BORDER_CONSTANT,0);
	delete pImageMat;
	int end = GetTickCount();
	ESMLog(5,_T("Loading GPU Time %d"),end-start);

	ESMSetGPUCheck(TRUE);
	return 0;
}
//------------------------------------------------------------------------------
//-- Date	 : 2010-4-30
//-- Owner	 : hongsu.jung
//-- Comment :  
//------------------------------------------------------------------------------
BOOL CMainFrame::FrameEnableDocking()
{
	if( !CExtControlBar::FrameEnableDocking(this) )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	if( !CExtControlBar::FrameInjectAutoHideAreas(this) )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	RecalcLayout();
	return TRUE;
}
BOOL CMainFrame::GetCudaSupport()
{
	return m_cudasupport;
}

BOOL CMainFrame::GetGPUDecodeSupport()
{
	return m_bGPUDecodeSupport;
}
vector<CString> CMainFrame::GetDeleteDSC()
{
	m_arrDeleteDSC.clear();
	m_arrMakeDeleteDSC.clear();
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\Setup\\CameraList.csv"), ESMGetPath(ESM_PATH_FILESERVER));

	if(!ReadFile.Open(strInputData, CFile::modeRead))
		return m_arrDeleteDSC;

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());

	if(iLength == 0)
		return m_arrDeleteDSC;

	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0, i = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;

	CString strNum, strDSCIP, strDSCID, strUsedFlag, strMakeFlag;

	while(1)
	{
		i++;
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;

		AfxExtractSubString(strDSCID, sLine, 0, ',');
		AfxExtractSubString(strDSCIP, sLine, 1, ',');
		AfxExtractSubString(strUsedFlag, sLine, 2, ',');
		AfxExtractSubString(strMakeFlag, sLine, 3, ',');
		strDSCID.Trim();
		strDSCIP.Trim();
		strUsedFlag.Trim();
		strMakeFlag.Trim();


		if(strDSCID != _T(""))
		{
			strNum.Format(_T("%d"),i);
			strDSCIP.Replace( _T(" "), NULL );
			strDSCID.Replace( _T(" "), NULL );		
			if(strUsedFlag =="TRUE")
			{
				//return TRUE;
			}
			else
			{
				m_arrDeleteDSC.push_back(strDSCID);//return FALSE;
			}

			if(strMakeFlag != _T("TRUE"))
			{
				m_arrMakeDeleteDSC.push_back(strDSCID);
			}
		}
		else
			break;
	}

	//jhhan 16-10-11
	SetMakeDeleteDSCList(m_arrMakeDeleteDSC);

	return m_arrDeleteDSC;
}
void CMainFrame::SetGPUDecodeSupport(BOOL bSupport)
{
	m_bGPUDecodeSupport = bSupport;
}
void CMainFrame::SetCudaSupport(bool cudasupport)
{
	m_cudasupport =  cudasupport;
}

void CMainFrame::SetDeleteDSCList(vector<CString> list)
{
	m_DeleteDSCList = list;
	ESMLog(5, _T("SetDeleteDSCList : %d, %d"), m_DeleteDSCList.size(), list.size());
	
	//180119
	//ESMDeleteMovie();
}
vector<CString> CMainFrame::GetDeleteDSCList()
{
	return m_DeleteDSCList;
}

BOOL CMainFrame::GetMakingUse()
{
	return m_MakingUse;
}
void CMainFrame::SetMakingUse(bool makingUse)
{
	m_MakingUse =  makingUse;
}

//------------------------------------------------------------------------------
//-- Date	 : 2010-4-30
//-- Owner	 : hongsu.jung
//-- Comment : Load Bar State , Position
//------------------------------------------------------------------------------
void CMainFrame::LoadBarState(CESMSplashWnd& _splash)
{
	_splash.AddTextLine(_T("Loading last state ..."));	
	CWinApp * pApp = ::AfxGetApp();
	try
	{
		if(!CExtControlBar::ProfileBarStateLoad(this, pApp->m_pszRegistryKey, pApp->m_pszProfileName, pApp->m_pszProfileName, &m_dataFrameWP, 
			true,true,HKEY_CURRENT_USER,false))
		{
			DockControlBar(&m_wndMenuBar);
			DockControlBar(&m_wndToolBarStandard);			

			DockControlBar(&m_wndOutputBar, AFX_IDW_DOCKBAR_BOTTOM);
			ShowControlBar(&m_wndOutputBar,TRUE, TRUE);

			DockControlBar(&m_wndLiveViewBar, AFX_IDW_DOCKBAR_RIGHT);			
			ShowControlBar(&m_wndLiveViewBar,TRUE, TRUE);

			DockControlBar(&m_wndFrameBar);
			ShowControlBar(&m_wndFrameBar,TRUE, TRUE);

			//DockControlBar(&m_wndFrameExBar);
			//ShowControlBar(&m_wndFrameExBar,TRUE, TRUE);

			DockControlBar(&m_wndTimeLineEditorBar);
			ShowControlBar(&m_wndTimeLineEditorBar,TRUE, TRUE);

			DockControlBar(&m_wndPropertyBar);
			ShowControlBar(&m_wndPropertyBar,TRUE, TRUE);			

			DockControlBar(&m_wndNetworkBar);
			ShowControlBar(&m_wndNetworkBar,TRUE, TRUE);			

			DockControlBar(&m_wndPointListBar);
			ShowControlBar(&m_wndPointListBar,TRUE, TRUE);

			DockControlBar(&m_wndFileMergeViewerBar);
			ShowControlBar(&m_wndFileMergeViewerBar,TRUE, TRUE);

			DockControlBar(&m_wndEffectEditorBar);
			ShowControlBar(&m_wndEffectEditorBar,TRUE, TRUE);

			DockControlBar(&m_wndEffectFrameViewerBar);
			ShowControlBar(&m_wndEffectFrameViewerBar,TRUE, TRUE);

			DockControlBar(&m_wndEffectPreviewBar);
			ShowControlBar(&m_wndEffectPreviewBar,TRUE, TRUE);

			DockControlBar(&m_wndTimeLineProcessorBar);
			ShowControlBar(&m_wndTimeLineProcessorBar,TRUE, TRUE);



			RecalcLayout();
		}
	}
	catch(...)
	{
		TRACE("Warning! The Prof-UIS control bar state being loaded does not match the control bars created in the frame window! \n");
	}
	_splash.AddTextLine(_T("OK."));
}


BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( ! CExtNCW < CFrameWnd > :: PreCreateWindow( cs ) )
		return FALSE;
	cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE
		| WS_THICKFRAME | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE;
	return TRUE;
}



//-----------------------------------------------------------
//-- CONTROL
//-----------------------------------------------------------
void CMainFrame::OnUpdateControlBarMenu(CCmdUI* pCmdUI) 
{
	if (pCmdUI->m_nID == ID_VIEW_BAR_NETWORK)
	{
		pCmdUI->SetCheck(m_wndNetworkBar.IsWindowVisible());
	}
	else if (pCmdUI->m_nID == ID_VIEW_BAR_NETWORKEX)
	{
		pCmdUI->SetCheck(m_wndNetworkExBar.IsWindowVisible());
	}
	else if (pCmdUI->m_nID == ID_VIEW_BAR_FRAME)
	{
		pCmdUI->SetCheck(m_wndFrameBar.IsWindowVisible());
	}
	else if (pCmdUI->m_nID == ID_VIEW_BAR_LIVEVIEW)
	{
		pCmdUI->SetCheck(m_wndLiveViewBar.IsWindowVisible());
	}
	else if (pCmdUI->m_nID == ID_VIEW_BAR_PROPERTY)
	{
		pCmdUI->SetCheck(m_wndPropertyBar.IsWindowVisible());
	}
	else if (pCmdUI->m_nID == ID_VIEW_BAR_OUTPUT)
	{
		pCmdUI->SetCheck(m_wndOutputBar.IsWindowVisible());
	}
	else if (pCmdUI->m_nID == ID_VIEW_BAR_DASHBOARD)
	{
		pCmdUI->SetCheck(m_wndDashBoardBar.IsWindowVisible());
	}
	else if (pCmdUI->m_nID == ID_VIEW_BAR_TIMELINE_PROCESSOR)
	{
		pCmdUI->SetCheck(m_wndTimeLineProcessorBar.IsWindowVisible());
	}
	else if (pCmdUI->m_nID == ID_VIEW_BAR_TIMELINE)
	{
		pCmdUI->SetCheck(m_wndTimeLineEditorBar.IsWindowVisible());
	}
	else if (pCmdUI->m_nID == ID_TOOLBARS_CAPTURE)
	{
		pCmdUI->SetCheck(m_wndToolBarStandard.IsWindowVisible());
	}	
	
	CExtControlBar::DoFrameBarCheckUpdate(this,pCmdUI,false);		
}

BOOL CMainFrame::OnBarCheck(UINT nID)					
{
	return  CExtControlBar::DoFrameBarCheckCmd( this, nID, false );	
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CExtNCW < CFrameWnd > :: AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CExtNCW < CFrameWnd > :: Dump( dc );
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
BOOL CMainFrame::PreTranslateMessage(MSG* pMsg) 
{
	if((	pMsg->message == WM_SYSKEYDOWN
		||	pMsg->message == WM_SYSKEYUP
		||	pMsg->message == WM_KEYDOWN
		||	pMsg->message == WM_KEYUP
		)
		&&	( HIWORD(pMsg->lParam) & KF_ALTDOWN ) != 0
		&&	(	((TCHAR)pMsg->wParam) == VK_SUBTRACT
		// disable warning C4310: cast truncates constant value
#pragma warning( push )
#pragma warning( disable : 4310 )
#pragma warning( disable : 4995 )
		||	((TCHAR)pMsg->wParam) == ((TCHAR)0xbd) // '-' near '0'
		// rollback warning C4310: cast truncates constant value
#pragma warning( pop )
		)
		)
		return TRUE; // (+ v.2.23)

#if 0
	if(pMsg->message == WM_KEYDOWN)
	{
		

		TRACE(_T("KEY DOWN\n"));
		BOOL bShift = ((GetKeyState(VK_SHIFT) & 0x8000) != 0); // Shift 키가 눌렸는지의 여부 저장
		BOOL bControl = ((GetKeyState(VK_CONTROL) & 0x8000) != 0); // Control 키가 눌렸는지의 여부 저장
		BOOL bAlt = ((GetKeyState(VK_MENU) & 0x8000) != 0);

/*
#ifdef DEBUG
		int nShift = ::GetKeyState(VK_SHIFT);
		int nControl = ::GetKeyState(VK_CONTROL);
		int nAlt	= ::GetKeyState(VK_MENU);

		if(nShift < 0) bShift = TRUE; else bShift = FALSE;
		if(nControl < 0) bControl = TRUE; else bControl = FALSE;
		if(nAlt < 0) bAlt = TRUE; else bAlt = FALSE;
		ESMLog(5,_T("shift: %d - control: %d - alt: %d"),bShift,bControl,bAlt);
		ESMLog(5,_T("VKNUMPAD1: %d"),::GetKeyState(VK_NUMPAD1));
#endif*/
		ESMSetSyncObj(bShift);


		if(bControl && !bShift && !bAlt)
			MakeCShortCut(pMsg->wParam);

		if(!bControl && !bShift && !bAlt)
		{			
			MakeShortCut(pMsg->wParam);
			
			if(pMsg->wParam >= VK_OEM_1 && pMsg->wParam <= VK_OEM_8) // [{, ]}, -, +, :, ' ....
			{
				MakeShortCutOEM(pMsg->wParam);
			}
		}

		if(bControl && bShift && !bAlt)
			MakeCSShortCut(pMsg->wParam);

		if(bControl && !bShift && bAlt)
			MakeCAShorCut(pMsg->wParam);
	}

	if(pMsg->message == WM_KEYUP)
	{
		ESMSetSyncObj(FALSE);
	}

	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F10)
		{
			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_ESM_ENV_TEMPLATE;
			::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
			return TRUE;
		}
	}
#else
	if (KeyDownMsg(pMsg) == FALSE)
		return TRUE;
#endif

	if( m_wndMenuBar.TranslateMainFrameMessage(pMsg) )
		return TRUE;

	return CExtNCW < CFrameWnd > :: PreTranslateMessage( pMsg );
}

void CMainFrame::SaveWindowStatus() 
{
	CWinApp * pApp = ::AfxGetApp();
	ASSERT( pApp != NULL );
	ASSERT( pApp->m_pszRegistryKey != NULL );
	ASSERT( pApp->m_pszRegistryKey[0] != _T('\0') );
	ASSERT( pApp->m_pszProfileName != NULL );
	ASSERT( pApp->m_pszProfileName[0] != _T('\0') );

	VERIFY(
		CExtControlBar::ProfileBarStateSave(
		this,
		pApp->m_pszRegistryKey,
		pApp->m_pszProfileName,
		pApp->m_pszProfileName
		)
		);
	g_CmdManager->ProfileWndRemove( GetSafeHwnd() );
}


void CMainFrame::ActivateFrame(int nCmdShow) 
{
	// window placement persistence
	if( m_dataFrameWP.showCmd != SW_HIDE )
	{
		SetWindowPlacement( &m_dataFrameWP );
		CExtNCW < CFrameWnd > :: ActivateFrame( m_dataFrameWP.showCmd );
		CExtPaintManager::stat_PassPaintMessages();
		m_dataFrameWP.showCmd = SW_HIDE;
		return;
	}
	CExtNCW < CFrameWnd >:: ActivateFrame( nCmdShow );
}

LRESULT CMainFrame::OnPreparePopupMenu( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	lParam;

	CExtPopupMenuWnd::MsgPrepareMenuData_t * pData =
		reinterpret_cast
		< CExtPopupMenuWnd::MsgPrepareMenuData_t * >
		( wParam );
	ASSERT( pData != NULL );

	CExtPopupMenuWnd * pPopup = pData->m_pPopup;
	ASSERT( pPopup != NULL );
	pPopup;

#if (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)

	//////////////////////////////////////////////////////////////////////////
	// Add "Windows..." command
	//////////////////////////////////////////////////////////////////////////

	INT nItemPos;
	INT nNewPos = -1;

	nItemPos = pPopup->ItemFindPosForCmdID( __ID_MDIWND_DLGWINDOWS );
	if( nItemPos > 0 )
	{
		// "More Windows..." command found
		pPopup->ItemRemove( nItemPos );
		nNewPos = nItemPos;
	}
	else
	{
		int nMaxCmdID = 0;
		for( int nCmdID = __ID_MDIWNDLIST_FIRST; nCmdID <= __ID_MDIWNDLIST_LAST; nCmdID++ ){
			nItemPos = pPopup->ItemFindPosForCmdID( nCmdID );
			if( nItemPos > 0 ){
				if( nCmdID > nMaxCmdID ){
					nMaxCmdID = nCmdID;
					nNewPos = nItemPos;
				}
			}
		}
		if( nNewPos > 0 ){
			pPopup->ItemInsert(
				(UINT)CExtPopupMenuWnd::TYPE_SEPARATOR,
				++nNewPos
				);
			nNewPos++;
		}
	}
	if( nNewPos > 0 )
	{
		UINT nCmdID = ID_WINDOWS_LIST;
		CExtCmdItem * pCmdItem =
			g_CmdManager->CmdGetPtr(
			g_CmdManager->ProfileNameFromWnd( this->GetSafeHwnd() ),
			nCmdID
			);
		if( pCmdItem == NULL ){
			pCmdItem = 
				g_CmdManager->CmdAllocPtr( 
				g_CmdManager->ProfileNameFromWnd( this->GetSafeHwnd() ), 
				nCmdID 
				);
		}
		ASSERT( pCmdItem != NULL );
		if( pCmdItem != NULL )
		{
			CExtSafeString sMoreWindows(_T("Windows..."));
			CExtSafeString sManageWindows(_T("Manages the currently open windows"));
			pCmdItem->m_sMenuText = sMoreWindows;
			pCmdItem->m_sToolbarText = pCmdItem->m_sMenuText;
			pCmdItem->m_sTipTool = sManageWindows;
			pCmdItem->m_sTipStatus = pCmdItem->m_sTipTool;
			pCmdItem->StateSetBasic( true );

			pPopup->ItemInsert(
				nCmdID,
				nNewPos,
				sMoreWindows,
				NULL,
				m_hWnd
				);
		}
	}

#endif // #if (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)

	return (LRESULT)(!0);
}

static bool stat_init_folder_cmd(
								 CExtPopupMenuWnd * pPopup,
								 LPCTSTR sFolderName,
								 UINT nCmdID,
								 LPUINT arrCmdIDs,
								 UINT nCount
								 )
{
	ASSERT_VALID( pPopup );
	ASSERT( sFolderName != NULL );
	ASSERT( _tcslen(sFolderName) > 0 );
	ASSERT( nCmdID > 0 );
	ASSERT( arrCmdIDs != 0 );
	ASSERT( nCount > 0 );

	INT nFolderIdx = pPopup->ItemFindByText(sFolderName, FALSE);

	CExtPopupMenuWnd * pFolder = NULL;
	if( nFolderIdx >= 0 )
	{
		pFolder = pPopup->ItemGetPopup(nFolderIdx);
		ASSERT_VALID( pFolder );
	}
	else
	{
		VERIFY( pPopup->ItemInsert( (UINT)CExtPopupMenuWnd::TYPE_POPUP, -1, sFolderName ) );
		pFolder = pPopup->ItemGetPopup(pPopup->ItemGetCount()-1);
		ASSERT_VALID( pFolder );
	}

	UINT i;
	for( i = 0; i < nCount; i++ )
	{
		if( arrCmdIDs[i] == nCmdID )
			break;
	}

	if( i == nCount )
		return false;

	//-- 2012-09-11 hongsu@esmlab.com
	//-- Exception 
	if(!pFolder)
		return false;

	VERIFY( pFolder->ItemInsert(nCmdID) );
	return true;
}

static void stat_insert_panel_to_menu(
									  CExtPopupMenuWnd * pPopup,
									  CExtControlBar * pBar
									  )
{
	ASSERT_VALID( pPopup );
	ASSERT_VALID( pBar );
	ASSERT_KINDOF( CExtControlBar, pBar );

	if( pBar->AutoHideModeGet() )
		return;

	CExtDynControlBar * pDynBar = DYNAMIC_DOWNCAST(CExtDynControlBar, pBar);
	if( pDynBar == NULL )
	{
		//VERIFY( pPopup->ItemInsert(pBar->GetDlgCtrlID()) );
		return;
	}
	ASSERT( !pBar->IsFixedMode() );
	ASSERT_VALID( pDynBar->m_pWndDynDocker );
	CString sFolderName = _T("Dynamic docker");
	if( pDynBar->IsKindOf(RUNTIME_CLASS(CExtDynTabControlBar)) )
		sFolderName = _T("Tab docker");

	VERIFY( pPopup->ItemInsert( (UINT)CExtPopupMenuWnd::TYPE_POPUP, -1, (LPCTSTR)sFolderName ) );
	CExtPopupMenuWnd * pFolderInner = pPopup->ItemGetPopup(pPopup->ItemGetCount()-1);
	ASSERT_VALID( pPopup );

	for( int nBarNo = 0; nBarNo < pDynBar->m_pWndDynDocker->m_arrBars.GetSize(); nBarNo++ )
	{
		CExtControlBar * pBar = (CExtControlBar *)pDynBar->m_pWndDynDocker->m_arrBars[nBarNo];
		if( pBar == NULL || __PLACEHODLER_BAR_PTR(pBar) )
			continue;
		ASSERT_VALID( pBar );
		ASSERT_KINDOF( CExtControlBar, pBar );
		ASSERT( !pBar->IsFixedMode() );
		stat_insert_panel_to_menu(pFolderInner,	pBar);
	}

	if( pFolderInner->ItemGetCount() == 0 )
	{
		VERIFY( pFolderInner->ItemInsert(ID_EMPTY_MENU) );
	}
}

static void stat_GetRowBars(
							CControlBar * pBar,
							MfcControlBarVector_t & vBars
							)
{
	ASSERT_VALID( pBar );
	ASSERT_VALID( pBar->m_pDockBar );
	vBars.RemoveAll();
	int nPos = pBar->m_pDockBar->FindBar( pBar );
	ASSERT( nPos >= 0 && nPos < pBar->m_pDockBar->m_arrBars.GetSize() );
	for( nPos--; nPos >= 0 && pBar->m_pDockBar->m_arrBars[nPos] != NULL; nPos-- );
	nPos++;
	ASSERT( nPos >= 0 && nPos < pBar->m_pDockBar->m_arrBars.GetSize() );
	for( ; pBar->m_pDockBar->m_arrBars[nPos] != NULL && nPos < pBar->m_pDockBar->m_arrBars.GetSize(); nPos++ )
	{
		CControlBar * pBarInRow = (CControlBar *)pBar->m_pDockBar->m_arrBars[nPos];
		if( pBarInRow == NULL || __PLACEHODLER_BAR_PTR(pBarInRow) )
			continue;
		ASSERT_VALID( pBarInRow );
		ASSERT_KINDOF( CControlBar, pBarInRow );
		vBars.Add( pBarInRow );
	}
}

//-- Revision : To display the popup menu
LRESULT CMainFrame::OnConstructPopupMenuCB( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	lParam;

	CExtControlBar::POPUP_MENU_EVENT_DATA * p_pmed =
		CExtControlBar::POPUP_MENU_EVENT_DATA::FromWParam( wParam );
	ASSERT( p_pmed != NULL );
	ASSERT_VALID( p_pmed->m_pPopupMenuWnd );
	ASSERT_VALID( p_pmed->m_pWndEventSrc );
	ASSERT( p_pmed->m_pWndEventSrc->GetSafeHwnd() != NULL );
	ASSERT( ::IsWindow(p_pmed->m_pWndEventSrc->GetSafeHwnd()) );

	if(p_pmed->m_bPostNotification)
		return 0;

	// insert popup menu with list of toolbars in the same row/column
	if(	p_pmed->m_nHelperNotificationType == CExtControlBar::POPUP_MENU_EVENT_DATA::__PMED_CTXEXPBTN_APPEND)
	{
		CExtToolControlBar * pExtBar = STATIC_DOWNCAST(CExtToolControlBar, p_pmed->m_pWndEventSrc);
		ASSERT_VALID( pExtBar );
		ASSERT( pExtBar->IsFixedMode() );
		LPCTSTR sRowFolderText =
			pExtBar->IsDockedHorizontally()
			? _T("Toolbars in the same row")
			: _T("Toolbars in the same column")
			;
		VERIFY( p_pmed->m_pPopupMenuWnd->ItemInsert( (UINT)CExtPopupMenuWnd::TYPE_POPUP, -1, sRowFolderText ) );
		CExtPopupMenuWnd::MENUITEMDATA & mi = p_pmed->m_pPopupMenuWnd->ItemGetInfo(p_pmed->m_pPopupMenuWnd->ItemGetCount()-1);
		mi.SetChildCombine();
		mi.SetToolButton();
		CExtPopupMenuWnd * pFolder = mi.GetPopup();
		ASSERT_VALID( pFolder );
		VERIFY( p_pmed->m_pPopupMenuWnd->ItemInsert(ID_SEPARATOR) );
		MfcControlBarVector_t vBars;
		stat_GetRowBars( pExtBar, vBars );
		for( int nBarNo = 0; nBarNo < vBars.GetSize(); nBarNo++ )
		{
			CControlBar * pBar = vBars[nBarNo];
			ASSERT_VALID( pBar );
			if(pFolder)
				if(pFolder->GetSafeHwnd())
					VERIFY( pFolder->ItemInsert(pBar->GetDlgCtrlID()) );
		}
		if( pFolder->ItemGetCount() == 0 )
		{
			if(pFolder->GetSafeHwnd())
				VERIFY( pFolder->ItemInsert(ID_EMPTY_MENU) );
		}
	}

	// insert popup menu with list of controlbars in the same floating palette
	bool bInsertPaletteContent = false;
	if(	p_pmed->m_nHelperNotificationType == CExtControlBar::POPUP_MENU_EVENT_DATA::__PMED_CONTROLBAR_NCBTNMENU_TOP)
		bInsertPaletteContent = true;
	else if(	p_pmed->m_nHelperNotificationType ==
		CExtControlBar::POPUP_MENU_EVENT_DATA::__PMED_CONTROLBAR_CTX
		||	p_pmed->m_nHelperNotificationType ==
		CExtControlBar::POPUP_MENU_EVENT_DATA::__PMED_CONTROLBAR_NC_CTX
		||	p_pmed->m_nHelperNotificationType ==
		CExtControlBar::POPUP_MENU_EVENT_DATA::__PMED_DOCKBAR_CTX
		||	p_pmed->m_nHelperNotificationType ==
		CExtControlBar::POPUP_MENU_EVENT_DATA::__PMED_MINIFRAME_NC_CTX
		||	p_pmed->m_nHelperNotificationType ==
		CExtControlBar::POPUP_MENU_EVENT_DATA::__PMED_DYNCBCTABS_CTX
		)
	{
		CMiniDockFrameWnd * pMiniFrame =
			DYNAMIC_DOWNCAST( CMiniDockFrameWnd, p_pmed->m_pWndEventSrc->GetParentFrame() );
		if( pMiniFrame != NULL )
			bInsertPaletteContent = true;
	}

	if( bInsertPaletteContent )
	{
		VERIFY( p_pmed->m_pPopupMenuWnd->ItemInsert( (UINT)CExtPopupMenuWnd::TYPE_POPUP, 0, _T("In this floating palette") ) );
		CExtPopupMenuWnd::MENUITEMDATA & mi = p_pmed->m_pPopupMenuWnd->ItemGetInfo(p_pmed->m_pPopupMenuWnd->ItemGetCount()-1);
		mi.SetChildCombine();
		mi.SetToolButton();
		CExtPopupMenuWnd * pFolder = mi.GetPopup();
		ASSERT_VALID( pFolder );
		VERIFY( p_pmed->m_pPopupMenuWnd->ItemInsert( ID_SEPARATOR ) );

		CMiniDockFrameWnd * pMiniFrame = STATIC_DOWNCAST( CMiniDockFrameWnd, p_pmed->m_pWndEventSrc->GetParentFrame() );
		ASSERT_VALID( pMiniFrame );
		ASSERT( pMiniFrame->m_wndDockBar.GetSafeHwnd() != NULL );
		ASSERT( ::IsWindow(pMiniFrame->m_wndDockBar.GetSafeHwnd()) );
		ASSERT( pMiniFrame->m_wndDockBar.GetDlgCtrlID() == AFX_IDW_DOCKBAR_FLOAT );

		CExtControlBar * pExtBar =	STATIC_DOWNCAST( CExtControlBar, pMiniFrame->m_wndDockBar.GetWindow(GW_CHILD));
		ASSERT_VALID( pExtBar );
		stat_insert_panel_to_menu(pFolder, pExtBar);
		//if( pFolder->ItemGetCount() == 0 )
		//	VERIFY( pFolder->ItemInsert(ID_EMPTY_MENU) );
	}

	if( ! p_pmed->IsControlBarMenuListNotification() )
		return 0;

	static UINT arrBarIDs_Main[]=
	{
		ID_VIEW_BAR_OUTPUT,	
		ID_VIEW_BAR_LIVEVIEW,
		ID_VIEW_BAR_FRAME,
		ID_VIEW_BAR_TIMELINE,
		ID_VIEW_BAR_PROPERTY,
		ID_VIEW_BAR_NETWORK,
		ID_VIEW_BAR_POINTLIST,
		ID_VIEW_BAR_EFFECTEDITOR,
		ID_VIEW_BAR_EFFECT_FRAME,
		ID_VIEW_BAR_EFFECT_PREVIEW,
		ID_VIEW_BAR_FILEMERGE,
		ID_VIEW_BAR_NETWORKEX,
		ID_VIEW_BAR_TIMELINE_PROCESSOR,
		ID_VIEW_BAR_DASHBOARD,
	};

	POSITION pos;
	pos = m_listControlBars.GetHeadPosition();
	while(pos)
	{
		CControlBar * pBar = (CControlBar *)m_listControlBars.GetNext( pos );
		ASSERT_VALID( pBar );
		ASSERT_KINDOF( CControlBar, pBar );
		if( pBar->IsDockBar() )
		{
			ASSERT_KINDOF( CDockBar, pBar );
			continue;
		}

		CExtControlBar * pExtBar = DYNAMIC_DOWNCAST( CExtControlBar, pBar );
		if( pExtBar == NULL )
			continue;

		if( pExtBar->IsFixedMode() )
		{
			ASSERT_KINDOF( CExtToolControlBar, pExtBar );
			if( pExtBar->IsKindOf(RUNTIME_CLASS(CExtMenuControlBar)) )
			{
				//VERIFY( p_pmed->m_pPopupMenuWnd->ItemInsert( pExtBar->GetDlgCtrlID(), bInsertPaletteContent ? -1 : 0 ) );
				continue;
			}

			INT nFolderIdx = p_pmed->m_pPopupMenuWnd->ItemFindByText(_T("Toolbars"), FALSE);
			//-- 2013-04-19 hongsu.jung
			//-- exception			
			if(nFolderIdx < 0)
				continue;

			CExtPopupMenuWnd * pFolder = NULL;
			if( nFolderIdx >= 0 )
			{
				pFolder = p_pmed->m_pPopupMenuWnd->ItemGetPopup(nFolderIdx);
				ASSERT_VALID( pFolder );
			}
			else
			{
				VERIFY( p_pmed->m_pPopupMenuWnd->ItemInsert(AFX_IDW_STATUS_BAR) );
				VERIFY( p_pmed->m_pPopupMenuWnd->ItemInsert( (UINT)CExtPopupMenuWnd::TYPE_POPUP, -1, _T("Toolbars") ) );
				pFolder = p_pmed->m_pPopupMenuWnd->ItemGetPopup(p_pmed->m_pPopupMenuWnd->ItemGetCount()-1);
				ASSERT_VALID( pFolder );
				VERIFY( p_pmed->m_pPopupMenuWnd->ItemInsert(ID_SEPARATOR) );
			}
			//-- 2013-04-19 hongsu.jung
			//-- exception
			if(!pFolder)
				continue;
			VERIFY( pFolder->ItemInsert( pExtBar->GetDlgCtrlID() ) );
			continue;
		}

		ASSERT( ! pExtBar->IsKindOf(RUNTIME_CLASS(CExtToolControlBar)) );
		if( pExtBar->IsKindOf(RUNTIME_CLASS(CExtDynControlBar))) 
			continue;
		UINT nCmdID = (UINT)pExtBar->GetDlgCtrlID();

		
		//VERIFY( p_pmed->m_pPopupMenuWnd->ItemInsert(ID_SEPARATOR) );
		/*
		if(stat_init_folder_cmd( p_pmed->m_pPopupMenuWnd, _T("Status View"), nCmdID, arrBarIDs_Status, sizeof(arrBarIDs_Status)/sizeof(arrBarIDs_Status[0])))	continue;		
		if(stat_init_folder_cmd( p_pmed->m_pPopupMenuWnd, _T("Image View"), nCmdID, arrBarIDs_Image, sizeof(arrBarIDs_Image)/sizeof(arrBarIDs_Image[0])))		continue;				
		if(stat_init_folder_cmd( p_pmed->m_pPopupMenuWnd, _T("Graph View"), nCmdID, arrBarIDs_Graph, sizeof(arrBarIDs_Graph)/sizeof(arrBarIDs_Graph[0])))		continue;		
		if(stat_init_folder_cmd( p_pmed->m_pPopupMenuWnd, _T("Result View"), nCmdID, arrBarIDs_Result, sizeof(arrBarIDs_Result)/sizeof(arrBarIDs_Result[0])))	continue;				

		VERIFY( p_pmed->m_pPopupMenuWnd->ItemInsert(ID_SEPARATOR) );

		if(stat_init_folder_cmd( p_pmed->m_pPopupMenuWnd, _T("SRM View"), nCmdID, arrBarIDs_SRM, sizeof(arrBarIDs_SRM)/sizeof(arrBarIDs_SRM[0])))				continue;				
		
		VERIFY( p_pmed->m_pPopupMenuWnd->ItemInsert(ID_SEPARATOR) );

		if(stat_init_folder_cmd( p_pmed->m_pPopupMenuWnd, _T("Serial View"), nCmdID, arrBarIDs_Serial, sizeof(arrBarIDs_Serial)/sizeof(arrBarIDs_Serial[0])))				continue;		
		if(stat_init_folder_cmd( p_pmed->m_pPopupMenuWnd, _T("Serial Filtering View"), nCmdID, arrBarIDs_Filter, sizeof(arrBarIDs_Filter)/sizeof(arrBarIDs_Filter[0])))		continue;
		*/
	}	
	return (!0);
}

//-- To Close Previous Log File
void CMainFrame::ClosePreviousLogFile(CString strLogFile)
{
	CString strCommnet = _T("");
	CString strLogFilePath = _T("");

	//-- ESM Log Stop
	if(m_pLogManager)
	{
		if(strLogFile.GetLength())
		{
			//-- Write Information
			strCommnet.Format(_T("===== Start [%s] TC ====="), strLogFile);
			ESMLog(1,strCommnet);

			strLogFilePath = ESMGetTime() + _T("_") + strLogFile + _T(".log");
			strCommnet.Format(_T("===== Check [%s] Log File ====="), strLogFilePath);
			ESMLog(1,strCommnet);
		}		
		m_pLogManager->Stop();
	}
}
track_info CMainFrame::GetTrackinfo()
{
	return t_info;
}

void CMainFrame::SetTrackinfo(track_info tinfo)
{
	t_info = tinfo;
}
void CMainFrame::SetFrameAdjust(BOOL mSet)
{
	mFrameAdjSet = mSet;
}
BOOL CMainFrame::GetFrameAdjust()
{
	return mFrameAdjSet;
}
void CMainFrame::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case TIMER_START_RECORD:
		{
			if(!m_bTestFlag)
			{
				ESMLog(1, _T("#### End Test"));
				KillTimer(TIMER_START_RECORD);
				break;
			}

			ESMLog(1, _T("####[1] Start Record"));
			MakeShortCut('1');
			KillTimer(TIMER_START_RECORD);
			//SetTimer(TIMER_SELECT_FRAME, 10000, NULL);
			SetTimer(TIMER_END_RECORD, 1000 * 360, NULL);
		}
		break;
	case TIMER_SELECT_FRAME:
		{
			if(!m_bTestFlag)
			{
				ESMLog(1, _T("#### End Test"));
				KillTimer(TIMER_SELECT_FRAME);
				break;
			}
			
			if(ESMGetSelectPointNum() >= 50)
			{
				m_bTestFlag = FALSE;
				KillTimer(TIMER_SELECT_FRAME);
				ESMLog(1, _T("####[5] Stop Record"));
				MakeShortCut('9');
				SetTimer(TIMER_START_RECORD, 10000, NULL);
				break;
			}

			ESMLog(1, _T("####[2] Select Frame"));
#if 1
			MakeShortCut('Z');
#else
			int nNum = ESMGetSelectPointNum();

			if(nNum >= 50)
			{
				nNum -= 1;
				AfxMessageBox(_T("Select Point is over 50!"));
				ESMSetLastSelectPointNum(nNum);
				ESMSetSelectPointNum(nNum);
				return;
			}
			int nCaptureTime = m_wndDSCViewer.m_pDSCMgr->GetRecordTime() - sync_frame_time_after_sensor_on;
			int nMilliSecond = nCaptureTime / 10 / 2;
			m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nMilliSecond, nNum);
			m_wndFrame.SetSelectedTime(nMilliSecond, nNum);
			//Save
			CString strTime;
			strTime.Format(_T("SelectPointTime%d"), nNum);
			ESMSetRecordingInfoint(strTime, nMilliSecond);
			strTime.Format(_T("SelectLastPointTime"));
			ESMSetRecordingInfoint(strTime, nMilliSecond);
			//hjcho 17-03-20 SelectPointSave
			ESMSetRecordingPoint(strTime,nMilliSecond);
			//jhhan 16-11-21 LastPointNum
			ESMSetLastSelectPointNum(nNum);

			nNum++;
			ESMSetSelectPointNum(nNum);
#endif

			KillTimer(TIMER_SELECT_FRAME);
			SetTimer(TIMER_VIEW_FRAME, 3500, NULL);
		}
		break;
	case TIMER_VIEW_FRAME:
		{
			if(!m_bTestFlag)
			{
				ESMLog(1, _T("#### End Test"));
				KillTimer(TIMER_VIEW_FRAME);
				break;
			}

			ESMLog(1, _T("####[3] View Frame"));
			MakeShortCut('X');
			KillTimer(TIMER_VIEW_FRAME);
			//SetTimer(TIMER_MAKE_MOVIE, 5000, NULL);
			//SetTimer(TIMER_END_RECORD, 5000, NULL);
			SetTimer(TIMER_SELECT_FRAME, 5000, NULL);
		}
		break;
	case TIMER_MAKE_MOVIE:
		{
			if(!m_bTestFlag)
			{
				ESMLog(1, _T("#### End Test"));
				KillTimer(TIMER_MAKE_MOVIE);
				break;
			}

			ESMLog(1, _T("####[4] Make Movie"));
			MakeShortCut(VK_NUMPAD1);
			KillTimer(TIMER_MAKE_MOVIE);
			SetTimer(TIMER_END_RECORD, 3000, NULL);
		}
		break;
	case TIMER_END_RECORD:
		{
			if(!m_bTestFlag)
			{
				ESMLog(1, _T("#### End Test"));
				KillTimer(TIMER_END_RECORD);
				break;
			}

			ESMLog(1, _T("####[5] Stop Record"));
			MakeShortCut('9');
			KillTimer(TIMER_END_RECORD);

			if(m_bTestFlag)
				SetTimer(TIMER_START_RECORD, 1000 * 20, NULL);
		}
		break;
	case TM_DSC_CONNECT:
		KillTimer(TM_DSC_CONNECT);
		DSCMovieCapture();
		break;
	case TM_DSC_DISCONNECT:
		KillTimer(TM_DSC_DISCONNECT);
		m_wndDSCViewer.RemoveListAll();
		m_wndNetwork.DoConnectionAll();
		m_wndNetwork.FileExistCheck();
		SetTimer(TM_DSC_CONNECT, TM_GAP_DSC_CONNECT, NULL);
		break;
	case SEQ_FOCUS_SAVE_1:
		{
			if (!m_bStartFocusSaveToFileFlag)
				break;

			ESMLog(1, _T("#### SEQ_FOCUS_SAVE_1 : Start Focus Save All to file"));
			m_wndNetwork.AllCameraConnect();
			SetTimer(SEQ_FOCUS_SAVE_2, 1000*18, NULL);
			KillTimer(SEQ_FOCUS_SAVE_1);
		}
		break;
	case SEQ_FOCUS_SAVE_2:
		{
			if (!m_bStartFocusSaveToFileFlag)
				break;

			ESMLog(1, _T("#### SEQ_FOCUS_SAVE_2: Get Focus Value all"));
			GetFocusValueAll();
			SetTimer(SEQ_FOCUS_SAVE_3, 1000*3, NULL);
			KillTimer(SEQ_FOCUS_SAVE_2);
		}
		break;
	case SEQ_FOCUS_SAVE_3:
		{
			if (!m_bStartFocusSaveToFileFlag)
				break;

			ESMLog(1, _T("#### SEQ_FOCUS_SAVE_3: Focus Save All to file"));

			SetFocusSaveAllToFile("");

			SetTimer(SEQ_FOCUS_SAVE_4, 1000, NULL);
			KillTimer(SEQ_FOCUS_SAVE_3);
		}
		break;
	case SEQ_FOCUS_SAVE_4:
		{
			if (!m_bStartFocusSaveToFileFlag)
				break;

			ESMLog(1, _T("#### SEQ_FOCUS_SAVE_4: Start rec"));

			ESMSetMakeMovie(TRUE);		// 원본파일 지워지지않도록..
			MakeShortCut('1');

			SetTimer(SEQ_FOCUS_SAVE_5, 1000*12, NULL);
			KillTimer(SEQ_FOCUS_SAVE_4);
		}
		break;
	case SEQ_FOCUS_SAVE_5:
		{
			if (!m_bStartFocusSaveToFileFlag)
				break;
			
			ESMLog(1, _T("#### SEQ_FOCUS_SAVE_5: Stop rec"));

			MakeShortCut('9');

			SetTimer(SEQ_FOCUS_SAVE_6, 1000*8, NULL);
			KillTimer(SEQ_FOCUS_SAVE_5);
		}
		break;
	case SEQ_FOCUS_SAVE_6:
		{
			if (!m_bStartFocusSaveToFileFlag)
				break;			

			ESMLog(1, _T("#### SEQ_FOCUS_SAVE_6: Cam Off"));

			m_wndNetwork.AllCameraOff();
			SetTimer(SEQ_FOCUS_SAVE_1, 1000*12, NULL);
			KillTimer(SEQ_FOCUS_SAVE_6);
		}
		break;
	case TIMER_RESTART_AGENT_START:
		{
			ESMLog(1, _T("### Restart 4DA program"));
			m_wndProperty.OnImageSetBanksave();

			SetTimer(TIMER_RESTART_AGENT_DISCONNECT, 1500, NULL);
			KillTimer(TIMER_RESTART_AGENT_START);
		}
		break;
	case TIMER_RESTART_AGENT_DISCONNECT:
		{
			m_wndNetwork.AllCameraOff();

			SetTimer(TIMER_RESTART_AGENT_END, 1500, NULL);
			KillTimer(TIMER_RESTART_AGENT_DISCONNECT);
		}
		break;
	case TIMER_RESTART_AGENT_END:
		{
			m_wndNetwork.AllCameraConnect();

			KillTimer(TIMER_RESTART_AGENT_END);
		}
		break;
	case TIMER_RECORD_FINISH_STATUS:
		{			
			if (m_bReSyncFlag == TRUE)
			{
				ESMLog(5, _T("@@@@@@@ [TIMER_RECORD_FINISH_STATUS] Wait time Record finish..."));

				m_bReSyncRecStopFinishFlag = TRUE;
#if 1
				int nAll = m_wndDSCViewer.m_pListView->GetItemCount();
				CDSCItem* pDSCItem = NULL;
				for(int i = 0 ; i < nAll ; i ++)
				{
					pDSCItem = m_wndDSCViewer.m_pListView->GetItemData(i);

					if (pDSCItem->GetRecordReady() == FALSE)
						ESMLog(5, _T("@@@@@@@ [TIMER_RECORD_FINISH_STATUS] not record ready ID: %s"), pDSCItem->GetDeviceDSCID());

					//pDSCItem->SetRecordReady(TRUE);
				}
#endif
			}
			else
			{
				int nAll = m_wndDSCViewer.m_pListView->GetItemCount();
				CDSCItem* pDSCItem = NULL;
				for(int i = 0 ; i < nAll ; i ++)
				{
					pDSCItem = m_wndDSCViewer.m_pListView->GetItemData(i);
					pDSCItem->SetRecordReady(TRUE);
				}
			}  	

			m_bTimerRecFinishRunning = FALSE;
			KillTimer(TIMER_RECORD_FINISH_STATUS);			
		}
		break;
	case TIMER_RECORD_RESYNC_GET_TICK_FINISH:
		{
			ESMLog(5, _T("[TIMER_RECORD_RESYNC_GET_TICK_FINISH] Get Tick finish.."));		

			int nAll = m_wndDSCViewer.GetItemCount();
			while(nAll--)
			{
				CDSCItem* pDscItem = m_wndDSCViewer.GetItemData(nAll);
				pDscItem->SetReSyncGetTickReady(TRUE);
			}

			KillTimer(TIMER_RECORD_RESYNC_GET_TICK_FINISH);
		}
		break;
	case TIMER_RECORD_RESYNC_GET_TICK:
		{			
			if (ESMGetRecordStatus() == FALSE)
			{
				ESMLog(5, _T("[TIMER_RECORD_RESYNC_GET_TICK] Deos not work record status [%d]"), ESMGetRecordStatus());		
				return;
			}

			int nCaptureTime = m_wndDSCViewer.m_pDSCMgr->GetRecordTime();
			nCaptureTime = ESMGetTick() - nCaptureTime;
			if( nCaptureTime < 10000 )
			{
				CString strLog;
				double dbTime = (double)(10000 - nCaptureTime) / 10000;
				strLog.Format(_T("[TIMER_RECORD_RESYNC_GET_TICK] Wait for Record %02fs"), dbTime);
				ESMLog(0,strLog);
				return;
			}

			/*
			int nAll = m_wndDSCViewer.m_pDSCMgr->m_pDSCViewer->GetItemCount();
			CDSCItem* pItem = NULL;
			while(nAll--)
			{
				pItem = m_wndDSCViewer.m_pDSCMgr->m_pDSCViewer->GetItemData(nAll);
				if (pItem->GetReSyncGetTickReady() == FALSE)
				{
					ESMLog(5, _T("[TIMER_RECORD_RESYNC_GET_TICK] Please wait GetTick finish.. "));
					return;
				}
			}
			*/

			ESMLog(5, _T("@@@@@@@ TIMER_RECORD_RESYNC_GET_TICK ..  RECORD STATUS[%d], ReSyncFlag[%d]"), ESMGetRecordStatus(), m_bReSyncFlag);

			if (m_bReSyncFlag)
				break;
			
			DscReSyncGetTick();
		}
		break;
	case TIMER_RECORD_RESYNC_START:
		{
			//ESMLog(5, _T("@@@@@@@@@@@@@@@ TIMER_RECORD_RESYNC_START ..  RECORD STATUS [%d], ReSyncFlag[%d]"), ESMGetRecordStatus(), m_bReSyncFlag);

			m_nReSyncTimerCount++;

			BOOL bResyncStart = TRUE;

			CDSCGroup* pGroup = NULL;
			int nGroupAll = m_wndDSCViewer.GetGroupCount();	
			for(int nRoop = 0; nRoop < nGroupAll; nRoop++)
			{
				pGroup = m_wndDSCViewer.GetGroup(nRoop);
				if (pGroup->GetSync() == FALSE)
				{
					ESMLog(5, _T("$$$$$$ TIMER_RECORD_RESYNC_START Not ready to GetSync. "));
					bResyncStart = FALSE;
					break;
				}
			}

			if (m_bReSyncRecStopFinishFlag == FALSE)
			{
				//ESMLog(5, _T("$$$$$$$$$$$$$$$ TIMER_RECORD_RESYNC_START Not ready to record. m_bReSyncRecStopFinishFlag: %d"), m_bReSyncRecStopFinishFlag);
				bResyncStart = FALSE;
			}

			// check record finish
			int nAll = m_wndDSCViewer.m_pListView->GetItemCount();
			CDSCItem* pDSCItem = NULL;
			m_nResyncSucessCnt = 0;

			for(int i = 0 ; i < nAll ; i ++)
			{
				pDSCItem = m_wndDSCViewer.m_pListView->GetItemData(i);
				// record finish....
// 				if (pDSCItem->GetRecordReady() == FALSE)
// 				{
// 					ESMLog(5, _T("$$$$$$$$$$$$$$$ TIMER_RECORD_RESYNC_START Not ready to record. %s"), pDSCItem->GetDeviceDSCID());
// 					bResyncStart = FALSE;
// 				}

				// gettick finish..
				if (pDSCItem->GetReSyncGetTickReady() == FALSE)
				{
					//ESMLog(5, _T("$$$$$$$$$$$$ TIMER_RECORD_RESYNC_START Not ready to GetTick Cmd. %s , ReSyncReady: %d"), pDSCItem->GetDeviceDSCID(), pDSCItem->GetReSyncGetTickReady());	
					bResyncStart = FALSE;
				}
				else
				{
					m_nResyncSucessCnt++;
				}

				if (bResyncStart == FALSE)
					continue;
			}

			if (m_nReSyncTimerCount < 100)
			{
				if (bResyncStart == FALSE)
					break;
			}

			ESMLog(5, _T("################# TIMER_RECORD_RESYNC_START !!!! ..  RECORD STATUS [%d], ReSyncFlag[%d], Model[%d], ReSyncTimerCount[%d]"),
				ESMGetRecordStatus(), m_bReSyncFlag, ESMGetDevice(), m_nReSyncTimerCount);

			KillTimer(TIMER_RECORD_RESYNC_START);

			OnResync_RecStart();
		}
		break;
	case TIMER_RECORD_REC_GET_TICK_FINISH:
		{
			RecStopGetTickFinish();			
		}
		break;
	case TIMER_RECORD_RESYNC_GET_TICK_REQUEST:
		{
			m_nResyncGetTickCount++;

			ESMLog(5, _T("[TIMER_RECORD_RESYNC_GET_TICK_REQUEST] count: %d"), m_nResyncGetTickCount);
			ESMEvent* _pMsg = new ESMEvent();
			_pMsg->message	= WM_RS_RC_GET_TICK_CMD;
			_pMsg->nParam1	= m_nResyncGetTickCount;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_DSC, (LPARAM)_pMsg);

			if (m_nResyncGetTickCount > 1)
				KillTimer(TIMER_RECORD_RESYNC_GET_TICK_REQUEST);
		}
		break;
	default:
		break;
	}

	CExtNCW::OnTimer(nIDEvent);
}

vector<CString> CMainFrame::GetMakeDeleteDSC()
{
	/*m_arrMakeDeleteDSC.clear();
	
	GetDeleteDSC();*/

	return m_arrMakeDeleteDSC;
}

vector<CString> CMainFrame::GetMakeDeleteDSCList()
{
	return m_MakeDeleteDSCList;
}

void CMainFrame::SetMakeDeleteDSCList(vector<CString> list)
{
	m_MakeDeleteDSCList = list;

	////jhhan 16-11-23
	//if(m_MakeDeleteDSCList.size() > 0)
	//{
	//	ESMLog(5, _T("SetMakeDeleteDSCList : %d, %d"), m_MakeDeleteDSCList.size(), list.size());
	//}
}

BOOL CMainFrame::MakeDeleteDSCList(CString str)
{
	vector<CString> _DSCMakeDeleteList;
	_DSCMakeDeleteList = ESMGetMakeDeleteDSCList();

	for(int i = 0; i< _DSCMakeDeleteList.size();i++)
	{
		if(str == _DSCMakeDeleteList[i])
		{
			//ESMLog(5, _T("MakeDeleteDSCList : %s"), _DSCMakeDeleteList[i]);
			return FALSE;
		}
	}
	return TRUE;
}

void CMainFrame::ExcelSaveStatus()		//ESMhttp CameraState
{
	CFile file;
	CString strFilePath, WriteData;
	strFilePath.Format(_T("%s\\CameraState.csv"), ESMGetPath(ESM_PATH_SETUP));
	CESMFileOperation fo;
	CObArray pItemList;
	ESMGetDSCList(&pItemList);
	char* pstrData = NULL;

	if(file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite))
	{
		for( int i = 0 ;i < pItemList.GetSize(); i++)
		{
			CDSCItem * pItem = NULL;
			pItem = (CDSCItem*)pItemList.GetAt(i);
			WriteData.Format(_T("%s, %s\r\n"), pItem->GetDSCInfo(DSC_INFO_ID), pItem->GetStatusString());
			pstrData = ESMUtil::CStringToChar(WriteData);
			file.Write(pstrData, strlen(pstrData));
			delete[] pstrData;
			pstrData = NULL;
		}
		file.Close();
	}
}

void CMainFrame::OnHttpSender(int nMode)	//4DMaker -> ESMhttp
{
	if(ESMGetGPUMakeFile() != TRUE)
		return;

	HWND _hWnd = NULL;
	DWORD dwPid = 0;
	/*_hWnd = ::FindWindow(NULL, _T("ESMhttp"));
	if(_hWnd == NULL)
	return;*/

#if _DEBUG
	dwPid = ESMUtil::GetProcessPID(_T("ESMhttpD.exe"));
	if(dwPid == 0)
	{
		dwPid = ESMUtil::GetProcessPID(_T("ESMhttp.exe"));
	}
#else
	dwPid = ESMUtil::GetProcessPID(_T("ESMhttp.exe"));
#endif
	if (dwPid != 0)
	{
		_hWnd = ESMUtil::GetWndHandle(dwPid);
	}

	if (_hWnd == NULL)
	{
		//AfxMessageBox(_T("Not found - 4DMaker"));
		ESMLog(0, _T("Not found - 4DMaker"));
		return;
	}

	//TCHAR buffer[256];
	//::GetWindowText(_hWnd, buffer, sizeof(buffer));
	//ESMLog(5, _T("FindWindow : %s"), buffer);
	
	::PostMessage(_hWnd, WM_ESM_SERVER, (WPARAM)WM_ESM_NOTIFY, (LPARAM)nMode);
	TRACE(_T("SendMessage : WM_ESM_SERVER - %d\r\n"), nMode);
}
void CMainFrame::OnHttpSync(int nIdx)   //4DMaker -> ESMhttp
{
   if(ESMGetGPUMakeFile() != TRUE)
      return;

   HWND _hWnd = NULL;
   DWORD dwPid = 0;
   /*_hWnd = ::FindWindow(NULL, _T("ESMhttp"));
   if(_hWnd == NULL)
   return;*/

#if _DEBUG
   dwPid = ESMUtil::GetProcessPID(_T("ESMhttpD.exe"));
   if(dwPid == 0)
   {
      dwPid = ESMUtil::GetProcessPID(_T("ESMhttp.exe"));
   }
#else
   dwPid = ESMUtil::GetProcessPID(_T("ESMhttp.exe"));
#endif
   if (dwPid != 0)
   {
      _hWnd = ESMUtil::GetWndHandle(dwPid);
   }

   if (_hWnd == NULL)
   {
      //AfxMessageBox(_T("Not found - 4DMaker"));
      ESMLog(0, _T("Not found - 4DMaker"));
      return;
   }

   //TCHAR buffer[256];
   //::GetWindowText(_hWnd, buffer, sizeof(buffer));
   //ESMLog(5, _T("FindWindow : %s"), buffer);
   
   ::SendMessage(_hWnd, WM_ESM_SERVER, (WPARAM)WM_ESM_NOTIFY_SYNC, (LPARAM)nIdx);
   TRACE(_T("SendMessage : WM_ESM_SERVER - %d\r\n"), nIdx);
}
void CMainFrame::OnHttpCommand(int nMode)	//ESMhttp -> 4DMaker
{
	ESMLog(5, _T("OnHttpCommand : %d"), nMode);

	switch(nMode)
	{
	case 0:	//준비
		m_wndNetwork.OnConnectCmd();
		m_wndNetworkEx.OnConnectCmd();
		break;
	case 1:	//시작
		MakeShortCut('1');
		break;
	case 2:	//중지
		MakeShortCut('9');
		break;
	case 3:	//종료
		m_wndNetwork.OnAllCameraOffCmd();
		m_wndNetworkEx.OnAllCameraOffCmd();
		break;
	default:
		break;
	}
}

//DWORD CMainFrame::GetProcessPID(CString strProcess)
//{
//	HANDLE hndl = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
//	DWORD dwsma = GetLastError();
//
//	DWORD dwExitCode = 0;
//
//	PROCESSENTRY32  procEntry = { 0 };
//	procEntry.dwSize = sizeof(PROCESSENTRY32);
//	Process32First(hndl, &procEntry);
//	do
//	{
//		if (!_tcsicmp(procEntry.szExeFile, strProcess))
//		{
//			return procEntry.th32ProcessID;
//		}
//	} while (Process32Next(hndl, &procEntry));
//
//	if (hndl != INVALID_HANDLE_VALUE)
//		CloseHandle(hndl);
//	hndl = INVALID_HANDLE_VALUE;
//
//	return 0;
//}
//
//HWND CMainFrame::GetWndHandle(DWORD dwPID)
//{
//	HWND hWnd = ::FindWindow(NULL, NULL);
//	while (hWnd != NULL)
//	{
//		if (::GetParent(hWnd) == NULL) {
//			DWORD dwProcId;
//			GetWindowThreadProcessId(hWnd, &dwProcId);
//
//			if (dwPID == dwProcId) {
//				return hWnd;
//			}
//		}
//		hWnd = ::GetWindow(hWnd, GW_HWNDNEXT);
//	}
//	return NULL;
//}

void CMainFrame::InitFileini(CString strSourceDSC/* = _T("")*/)
{
	if(ESMGetValue(ESM_VALUE_NET_MODE) != ESM_MODESTATE_CLIENT)
		return;

	CFile file;
	CString strFilePath, WriteData;
	
#ifdef _INI_4DA_LOCAL
	//네트워크 접근 경로 - 4DA ini 저장
	strFilePath.Format(_T("%s\\%s.ini"), _T("M:\\movie"), strSourceDSC);
#endif
#ifdef _INI_4DP_NET_RAM
	//로컬 경로 - 4DP ini 저장
	strFilePath.Format(_T("\\\\%s\\movie\\%s.ini"), ESMGet4DAPath(strSourceDSC), strSourceDSC);
#endif
#ifdef _INI_4DP_NET_RT
	CESMFileOperation fo;
	strFilePath.Format(_T("\\\\%s\\4DMaker\\RT"), ESMGet4DAPath(strSourceDSC));
	fo.CreateFolder(strFilePath);
	ESMLog(5, strFilePath);
	//로컬 경로 - 4DP RT
	strFilePath.Format(_T("\\\\%s\\4DMaker\\RT\\RT.ini"), ESMGet4DAPath(strSourceDSC) );
#endif

	
	
	char* pstrData = NULL;
	if (file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite))
	{
		ESMLog(5, strFilePath);
		file.Close();
	}
}

/*
void CMainFrame::InitSensorOut(CString strDate)
{
	if(ESMGetValue(ESM_VALUE_NET_MODE) != ESM_MODESTATE_SERVER)
		return;

	CFile file;
	CString strFilePath;
	strFilePath.Format(_T("%s\\%s.out"), ESMGetPath(ESM_PATH_MOVIE_CONF), strDate);

	char* pstrData = NULL;
	if (file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite))
	{
		file.Close();
	}
}

BOOL CMainFrame::SetSensorOut(CString strDate, CString strDSCID, int nStartTime, int nTimeStamp)
{
	CString strFilePath;
	strFilePath.Format(_T("%s\\%s.out"), ESMGetPath(ESM_PATH_MOVIE), strDate);
	CESMIni ini;

	CString strSection = _T("SensorOut");

	if(ini.SetIniFilename (strFilePath))
	{
		int nCnt = ini.GetInt(strSection, _T("COUNT"));

		CString strDSC, strStartTime, strTimeStamp;
		strDSC.Format(_T("DSCID_%d"), nCnt);
		strStartTime.Format(_T("StartTime_%d"), nCnt);
		strTimeStamp.Format(_T("TimeStamp_%d"), nCnt);

		{ 
			ini.WriteString(strSection, strDSC, strDSCID);
			ini.WriteInt(strSection, strStartTime, nStartTime);
			ini.WriteInt(strSection, strTimeStamp, nTimeStamp);

			nCnt++;
			ini.WriteInt(strSection, _T("COUNT"), nCnt);
		}

		return TRUE;
	}


	return FALSE;
}
void CMainFrame::GetSensorOut(CString strDate)
{
	CString strFilePath;
	strFilePath.Format(_T("%s\\%s.out"), ESMGetPath(ESM_PATH_MOVIE), strDate);
	CESMIni ini;

	CString strSection = _T("SensorOut");

	if(ini.SetIniFilename (strFilePath))
	{
		int nCnt = ini.GetInt(strSection, _T("COUNT"));
		for(int i = 0; i < nCnt; i++)
		{
			CString strDSC, strStartTime, strTimeStamp;
			strDSC.Format(_T("DSCID_%d"), i);
			strStartTime.Format(_T("StartTime_%d"), i);
			strTimeStamp.Format(_T("TimeStamp_%d"), i);

			CString strDscId;
			int nStartTime, nTimeStamp;

			strDscId = ini.GetString(strSection, strDSC);
			nStartTime = ini.GetInt(strSection, strStartTime);
			nTimeStamp = ini.GetInt(strSection, strTimeStamp);

			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_RS_RC_SETSENSORTIME;
			CString *pStr = new CString;
			pStr->Format(_T("%s"), strDscId);
			pMsg->pDest = (LPARAM)pStr;
			pMsg->nParam1 = (LPARAM)nStartTime;
			pMsg->nParam2 = (LPARAM)nTimeStamp;

			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_DSC, (LPARAM)pMsg);
		}
	}
}
*/

CString CMainFrame::Load4DAPath(CString strSourceDSC)
{
	CString strPath;

	if(ESMGetGPUMakeFile())
	{
		CString strFile;
		strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_EX);
		CESMIni ini;	
		if(ini.SetIniFilename (strFile))
		{
			CString strIp;
			strIp = ini.GetString(_T("4DA"), strSourceDSC);
			strPath = strIp;
		}
	}
	return strPath;
}

void CMainFrame::RunESMMovieRTTThread(CString strIP, CString strDSC)
{
	//m_pMovieRTThread->Set4DAInfo(strIP, strDSC);

	//SetWhetherAorB
	//TRUE  - PLAN A : 4DReplay Plan - MULTIGPU
	//FALSE - PLAN B : KT Plan		 - ENCODING

	//m_pMovieRTThread->Set4DMakerPath(ESMGetPath(ESM_PATH_HOME));

	//m_pMovieRTThread->CreateThread();
	//m_pMovieRTThread->SetThreadPriority(THREAD_PRIORITY_HIGHEST);
}

void CMainFrame::RunESMDump(BOOL bRun/* = TRUE*/)
{
	if(bRun)
	{
		TCHAR strPath[MAX_PATH];
		GetModuleFileName( NULL, strPath, _MAX_PATH);
		PathRemoveFileSpec(strPath);
		ESMUtil::ExecuteProcess(strPath,_T("procdump64.exe"), _T("4DMaker.exe -t -e -h"), FALSE);
	}
	else
	{
		ESMUtil::KillProcess(_T("procdump64.exe"));
	}

}

void CMainFrame::SetFrameRate(int nMovieSize)
{
	int nAll = m_wndDSCViewer.m_pListView->GetItemCount();
	CDSCItem* pDSCItem = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pDSCItem = m_wndDSCViewer.m_pListView->GetItemData(i);
		CSdiSingleMgr* pSdi = pDSCItem->GetSDIMgr();
		if(pSdi)
		{
			pSdi->m_nMovieSize = nMovieSize;
			//ESMLog(1, _T("########## Set Movie Size %d"), nMovieSize );
		}
	}
}

void CMainFrame::SetMakeDelete(CString strDSC,BOOL bDelete/* = FALSE*/)
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\CameraList.csv"), ESMGetPath(ESM_PATH_SETUP));

	if(!ReadFile.Open(strInputData, CFile::modeRead))
		return;

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());

	if(iLength == 0)
		return;

	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0, i = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;

	CString strNum, strDSCIP, strDSCID,strUsedFlag, strMakeFlag, strName, strReverse, strGroup, strVertical, strDetect;

	//Write
	CString WriteData = _T("");
	CFile file;
	if(file.Open(strInputData, CFile::modeCreate | CFile::modeReadWrite))
	{
#if 1
		CObArray pArr;
		ESMGetDSCList(&pArr);

		for(int i = 0 ; i < pArr.GetCount(); i++)
		{
			char* pstrData = NULL;
			CDSCItem* pItem = (CDSCItem*)pArr.GetAt(i);

			sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

			if( sLine == _T(""))
				break;

			AfxExtractSubString(strDSCID, sLine, 0, ',');
			AfxExtractSubString(strDSCIP, sLine, 1, ',');
			AfxExtractSubString(strUsedFlag, sLine, 2, ',');
			AfxExtractSubString(strMakeFlag, sLine, 3, ',');
			AfxExtractSubString(strName, sLine, 4, ',');

			//AfxExtractSubString(strReverse, sLine, 5, ',');
			
			AfxExtractSubString(strGroup, sLine, 6, ',');
			AfxExtractSubString(strVertical, sLine, 7, ',');

			AfxExtractSubString(strDetect, sLine, 8, ',');

			strDSCID.Trim();
			strDSCIP.Trim();
			strUsedFlag.Trim();
			strMakeFlag.Trim();
			strName.Trim();

			strGroup.Trim();
			strVertical.Trim();

			strDetect.Trim();

			if(strDSCID.CompareNoCase(strDSC) == 0)
			{
				if(bDelete)
					strMakeFlag = _T("FALSE");
				else
					strMakeFlag = _T("TRUE");		
			}
			if(pItem->m_bReverse)
				strReverse = _T("REVERSE");
			else
				strReverse = _T("NORMAL");

			WriteData.Format(_T("%s, %s, %s, %s, %s, %s, %s, %s, %s\r\n"), strDSCID, strDSCIP,strUsedFlag, strMakeFlag, strName,strReverse,strGroup,strVertical, strDetect);
			pstrData = ESMUtil::CStringToChar(WriteData);
			file.Write(pstrData, strlen(pstrData));
		}
#else
		while(1)
		{
			char* pstrData = NULL;
			CDSCItem* pItem = (CDSCItem*)pArr.GetAt(i);
			i++;
			sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

			if( sLine == _T(""))
				break;

			AfxExtractSubString(strDSCID, sLine, 0, ',');
			AfxExtractSubString(strDSCIP, sLine, 1, ',');
			AfxExtractSubString(strUsedFlag, sLine, 2, ',');
			AfxExtractSubString(strMakeFlag, sLine, 3, ',');
			AfxExtractSubString(strName, sLine, 4, ',');
			strDSCID.Trim();
			strDSCIP.Trim();
			strUsedFlag.Trim();
			strMakeFlag.Trim();
			strName.Trim();

			if(strDSCID.CompareNoCase(strDSC) == 0)
			{
				if(bDelete)
					strMakeFlag = _T("TRUE");
				else
					strMakeFlag = _T("FALSE");		
			}
			if(pItem->m_bReverse)
				strReverse = _T("REVERSE");
			else
				strReverse = _T("NORMAL");

			WriteData.Format(_T("%s, %s, %s, %s, %s, %s\r\n"), strDSCID, strDSCIP,strUsedFlag, strMakeFlag, strName,strReverse);
			pstrData = ESMUtil::CStringToChar(WriteData);
			file.Write(pstrData, strlen(pstrData));
		}
#endif
		file.Close();
	}
	
}
void CMainFrame::RunESMMovieRTMovie(CString strIP, CString strDSC,int nIdx,int nFrameRate/* = 30*/)
{
	if(m_pMovieRTMovie->GetMovieStart() == FALSE)
	{
		ESMLog(5,_T("[%d] RunESMMovieRTMovie"),nIdx);
		m_pMovieRTMovie->Set4DAInfo(strIP,strDSC,nIdx,nFrameRate);
		//m_pMovieRTMovie->Set4DMakerPath(ESMGetPath(ESM_PATH_HOME));
		m_pMovieRTMovie->Run();
		m_pMovieRTMovie->SetMovieStart(TRUE);
	}
}
void CMainFrame::RunESMMovieRTSend(CString strIP,CString strDSC,BOOL bRefereeMode /*= FALSE*/)
{
	m_pMovieRTSend->SetRefereeMode(bRefereeMode);
	m_pMovieRTSend->Set4DAInfo(strIP, strDSC);
	m_pMovieRTSend->Set4DMakerPath(ESMGetPath(ESM_PATH_HOME));
	m_pMovieRTSend->Run();
}
void CMainFrame::RunESMMovieRTSend60p(CString strIP,CString strDSC)
{
	m_pMovieRTSend60p->Set4DAInfo(strIP, strDSC);
	m_pMovieRTSend60p->Set4DMakerPath(ESMGetPath(ESM_PATH_HOME));
	m_pMovieRTSend60p->Run();
}

void CMainFrame::SetInfoToMovieMgr()
{
#ifdef DSC_INFO_FIND
	// SetModel
	CObArray pItemList;
	ESMGetDSCList(&pItemList);
	CDSCItem* pItem = NULL;
	for( int i = 0 ;i < pItemList.GetCount(); i++)
	{
		pItem = (CDSCItem*)pItemList.GetAt(i);
		if (pItem == NULL)
			continue;

		if (pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		{
			m_pESMMovieMgr->SetModel((SDI_MODEL)SDI_MODEL_GH5);
		}

		break;
	}
#else
	m_pESMMovieMgr->SetDeviceModel((SDI_MODEL)ESMGetDevice()); //180309
	
	m_pESMMovieMgr->SetFrameRate(ESMGetFrameRate(TRUE));//180202_C
#endif
}


void CMainFrame::OnUtilityTrackingViewer()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	CESMTrackingViewer pTrackingViewer;
	pTrackingViewer.DoModal();
}

void CMainFrame::GetFocusValueAll()
{
	m_wndDSCViewer.GetPropertyAll(PTP_OC_SAMSUNG_GetFocusPosition);
}

void CMainFrame::SetFocusSaveAllToFile(CString str)
{	
	m_wndProperty.SetFocusSave(str);
}

void CMainFrame::StartFocusValueToFile()
{
	m_bStartFocusSaveToFileFlag = TRUE;
	SetTimer(SEQ_FOCUS_SAVE_1, 1000, NULL);
}

void CMainFrame::EndFocusValueToFile()
{
	m_bStartFocusSaveToFileFlag = FALSE;
	KillTimer(SEQ_FOCUS_SAVE_1);
	KillTimer(SEQ_FOCUS_SAVE_2);
	KillTimer(SEQ_FOCUS_SAVE_3);
	KillTimer(SEQ_FOCUS_SAVE_4);
	KillTimer(SEQ_FOCUS_SAVE_5);
	KillTimer(SEQ_FOCUS_SAVE_6);	


	ESMLog(1, _T("#### SEQ END"));
	//ESMLog(1, _T("#### SEQ: End Focus Save All to file"));
}
void CMainFrame::OnUtilityAJAConnect()
{
	//if(ESMGetValue(ESM_VALUE_AJANETWORK))
	ESMLaunchAJANetwork();
}

void CMainFrame::ReStartAgent()
{	
	SetTimer(TIMER_RESTART_AGENT_START, 3000, NULL);
}

CString CMainFrame::GetCamModel()
{
	CString strModel;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;
	for( int i =0 ;i < arDSCList.GetSize(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if (pItem == NULL)
			continue;

		strModel = pItem->GetDeviceModel();
		if (strModel.IsEmpty())
			continue;

		break;
	}

	return strModel;
}

BOOL CMainFrame::RecStopGetTickFinish()
{
	m_nGetTickRecFinishCount++;

	int nAllItem = m_wndDSCViewer.GetItemCount();
	BOOL bGetTickFinishFlag = TRUE;
	for (int i = 0; i < nAllItem ; i++)
	{
		CDSCItem* pDscItem = m_wndDSCViewer.GetItemData(i);						
		// checked gettick finish..
		if (pDscItem->GetReSyncGetTickReady() == FALSE)
		{
			ESMLog(5, _T("[TIMER_RECORD_REC_FINISH] Please wait gettick finish..."));
			bGetTickFinishFlag = FALSE;
			break;
		}				
	}

	ESMLog(5, _T(" ----- bGetTickFinishFlag[%d], count[%d]"), bGetTickFinishFlag, m_nGetTickRecFinishCount);

	if (bGetTickFinishFlag || (m_nGetTickRecFinishCount > 10))
	{
		KillTimer(TIMER_RECORD_REC_GET_TICK_FINISH);		

		for (int i = 0; i < nAllItem ; i++)
		{
			CDSCItem* pDscItem = m_wndDSCViewer.GetItemData(i);		
			pDscItem->SetReSyncGetTickReady(TRUE);
		}

		if (m_bReSyncFlag)
		{
			//Sleep(1000);
			ESMSetWait(1000);
			ESMLog(5, _T(" ---ReSync--- bGetTickFinishFlag[%d], count[%d]"), bGetTickFinishFlag, m_nGetTickRecFinishCount);
			// resync stop -> start
			OnResync_RecStop();		
			DscReSync();

			SetTimer(TIMER_RECORD_RESYNC_START, 100, NULL);

			return TRUE;
		}
		else
		{
			MakeShortCut('9');
		}				
	}

	return FALSE;
}

void CMainFrame::StartReSync()
{		
#if 0		// 30초 전에 (get tick 0회) resync restart 안들어가지도록 하는부분..
	if (m_bGetTickFirstFlag == FALSE)
	{		
		ESMLog(5, _T("[StartReSync] GetTick is not ready."));		
		return;
	}
#endif

	BOOL bUseResync = ESMGetValue(ESM_VALUE_USE_RESYNC);
	if (bUseResync == FALSE)
	{
		ESMLog(5, _T("Please check Use_Resync in options."));
		return;				
	}

	if (ESMGetRecordStatus() == FALSE)
	{
		ESMLog(5, _T("Deos not work record status [%d]"), ESMGetRecordStatus());		
		return;
	}

	int nCaptureTime = m_wndDSCViewer.m_pDSCMgr->GetRecordTime();
	nCaptureTime = ESMGetTick() - nCaptureTime;
	if( nCaptureTime < 10000 )
	{
		CString strLog;
		double dbTime = (double)(10000 - nCaptureTime) / 10000;
		strLog.Format(_T("Wait for Record %02fs -.."), dbTime);
		ESMLog(0,strLog);
		return;
	}

	if (m_bReSyncFlag)
		return;		

	if(AfxMessageBox(_T("[ReSync] Do you want to ReSync record?"),MB_YESNO)!=IDYES)		
		return;

	ESMLog(5, _T("Start resync."));

	KillTimer(TIMER_RECORD_RESYNC_GET_TICK);

	m_nReSyncTimerCount = 0;
	m_bReSyncFlag = TRUE;
	m_nGetTickRecFinishCount = 0;
	m_bReSyncRecStopFinishFlag = FALSE;

	if (RecStopGetTickFinish() == FALSE)
	{
		SetTimer(TIMER_RECORD_REC_GET_TICK_FINISH, 1000*1, NULL);
	}
}

void CMainFrame::OnMove( int x, int y )
{
	CExtNCW::OnMove(x, y);

	UpdateWindow();
	//Invalidate();
}

void CMainFrame::StartPropertyDiffCheck()
{
	//ESMLog(5, _T("StartPropertyDiffCheck init~~"));
	m_stPropertyDiffCount.nShutterSpeed = 0;
	m_stPropertyDiffCount.nPictureWizard = 0;
	m_stPropertyDiffCount.nIso = 0;
	m_stPropertyDiffCount.nMovieSize = 0;
	m_stPropertyDiffCount.nWhiteBalance = 0;
	m_stPropertyDiffCount.nColorTemp = 0;
	m_stPropertyDiffCount.nPhotoStyle = 0;

	if (m_stPropertyDiffCount.mapCheckedID == NULL)
		m_stPropertyDiffCount.mapCheckedID = new CMap<CString, LPCTSTR, PropertyDiffCheckedID, PropertyDiffCheckedID>;
	else
		m_stPropertyDiffCount.mapCheckedID->RemoveAll();
}

void CMainFrame::StartPropertyMinMaxInfoCheck()
{
	//ESMLog(5, _T("StartPropertyMinMaxInfoCheck init~"));
	m_stPropertyMinMaxInfo.nFMin = 0;
	m_stPropertyMinMaxInfo.nFMax = 0;
	m_stPropertyMinMaxInfo.nUnknown = 0;
	m_stPropertyMinMaxInfo.nAuto = 0;
	if (m_stPropertyMinMaxInfo.strIDList == NULL)
		m_stPropertyMinMaxInfo.strIDList = new CStringList();
	else
		m_stPropertyMinMaxInfo.strIDList->RemoveAll();
}

void CMainFrame::OnSelectViewBar(int nTime, int nIdx)
{
	int nMilliSecond = nTime * 10;
	m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nMilliSecond, nIdx);
	m_wndFrame.SetSelectedTime(nMilliSecond, nIdx);
	m_wndDSCViewer.m_pFrameSelector->SelectFrameLine(nIdx);
}

vector<ESMFrameArray*> * CMainFrame::MakeFrame()
{
	vector<ESMFrameArray *> *pData = NULL;
	pData = new vector<ESMFrameArray *>;
	if(pData)
	{
		for(int i = 0; i < MAX_FRAME_THUMBNAIL; i++)
		{
			ESMFrameArray* pFrame = new ESMFrameArray;

			pFrame->pImg = NULL;
			pFrame->bLoad = FALSE;
			pData->push_back(pFrame);
		}
	}
	return pData;
}

void CMainFrame::InsertFrame(CString strKey, vector<ESMFrameArray*> * pVetor /*= NULL*/)
{
	g_mtxFv.Lock();
	if(pVetor == NULL)
	{
		pVetor = MakeFrame();
	}
	m_ImageBuf.insert(make_pair(strKey, pVetor));

	m_vFrame.push_back(strKey);

	g_mtxFv.Unlock();
}

vector<ESMFrameArray*> * CMainFrame::GetFrame(CString strKey)
{
	g_mtxFv.Lock();
	vector<ESMFrameArray*> *pData = NULL;

	map<CString, vector<ESMFrameArray*>*>::iterator itmapList = m_ImageBuf.find(strKey);
	if(itmapList != m_ImageBuf.end())
	{
		pData = itmapList->second;
	}
	g_mtxFv.Unlock();

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL, 0, DeleteFrameThread, (void*)this, 0, NULL);
	CloseHandle(hSyncTime);

	return pData;
}
void CMainFrame::ResetFrame()
{
	g_mtxFv.Lock();
	
	ESMLog(5, _T("ResetFrame"));

	for(auto it = m_ImageBuf.begin(); it != m_ImageBuf.end(); it++)
	{
		vector<ESMFrameArray*> *pData = it->second;

		for(int i=0; i< pData->size(); i++)
		{
			if((pData->at(i))->pImg != NULL)
			{
				delete[] (pData->at(i))->pImg;
				(pData->at(i))->pImg = NULL;
			}
		}
		pData->clear();
	}
	m_ImageBuf.clear();

	m_vFrame.clear();

	m_wndFrame.m_pDlgFrameList->m_vSplit.clear();
	
	g_mtxFv.Unlock();
}

unsigned WINAPI CMainFrame::ResetFrameThread(LPVOID param)
{
	CMainFrame * pParent = (CMainFrame*)param;
	
	pParent->ResetFrame();
	
	return 0;
}

void CMainFrame::DeleteFrame()
{
	vector<CString>::iterator it;

	CString strKey;
	if(m_vFrame.size() > 15)
	{
		it = m_vFrame.begin();
		strKey = *it;
		m_vFrame.erase(it);
	}else
	{
		return;
	}

	g_mtxFv.Lock();
	vector<ESMFrameArray*> *pData = NULL;
	pData = GetFrame(strKey);
	if(pData)
	{
		for(int i=0; i< pData->size(); i++)
		{
			if((pData->at(i))->pImg != NULL)
			{
				delete[] (pData->at(i))->pImg;
				(pData->at(i))->pImg = NULL;
			}
		}
		pData->clear();
		/*CESMFileOperation fo;
		fo.Delete(strKey);
		ESMLog(5, _T("DeleteFrame : %s"), strKey);*/
		m_ImageBuf.erase(strKey);
	}
	
	g_mtxFv.Unlock();
}

unsigned WINAPI CMainFrame::DeleteFrameThread(LPVOID param)
{
	CMainFrame * pParent = (CMainFrame*)param;

	pParent->DeleteFrame();

	return 0;
}

void CMainFrame::OnUtilityCheckedPropertyInfo()
{
	CString strTemp;

	if (m_stPropertyDiffCount.mapCheckedID)
	{
		strTemp.Format(_T("Checked Property difference Count =>\n ShuterSpeed: %d\n PictureWizard: %d\n ISO: %d \n MovieSize: %d\n WhiteBalance: %d\n ColorTemperature: %d\n PhotoStyle: %d\n Checked Camera count: %d/%d\n"),
			m_stPropertyDiffCount.nShutterSpeed, 
			m_stPropertyDiffCount.nPictureWizard,
			m_stPropertyDiffCount.nIso,
			m_stPropertyDiffCount.nMovieSize,
			m_stPropertyDiffCount.nWhiteBalance,
			m_stPropertyDiffCount.nColorTemp,
			m_stPropertyDiffCount.nPhotoStyle,
			m_stPropertyDiffCount.mapCheckedID->GetCount(),
			m_wndDSCViewer.m_pListView->GetItemCount());

		ESMLog(5, strTemp);
	}
	else
	{
		strTemp.Format(_T("Camera property information not loaded."));
		ESMLog(5, strTemp);
		return;
	}

	if (m_stPropertyMinMaxInfo.strIDList)
	{
		CString strFmin;
		strFmin.Format(_T("%d"), m_stPropertyMinMaxInfo.nFMin);
		if (strFmin.GetLength() > 0)
			strFmin.Insert(strFmin.GetLength()-1, _T("."));

		CString strFmax;
		strFmax.Format(_T("%d"), m_stPropertyMinMaxInfo.nFMax);
		if (strFmax.GetLength() > 0)
			strFmax.Insert(strFmax.GetLength()-1, _T("."));

		strTemp.Format(_T("Checked Property min max info =>\n F min: %s\n F Max: %s\n F Unknown count: %d\n F Auto count: %d\n Checked Camera count: %d/%d\n"), 
			strFmin, 
			strFmax, 
			m_stPropertyMinMaxInfo.nUnknown, 
			m_stPropertyMinMaxInfo.nAuto,
			m_stPropertyMinMaxInfo.strIDList->GetCount(),
			m_wndDSCViewer.m_pListView->GetItemCount());

		ESMLog(5, strTemp);
	}	
}


void CMainFrame::OnCommand4dliveconverter()
{
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_ESM_ENV_MUX;
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
}


void CMainFrame::OnHelpShortcutlist()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
}

//void CMainFrame::MakeM3u8(CString strKey)
//{
//	CESMm3u8 *pData = NULL;
//
//	map<CString, CESMm3u8 *>::iterator itList = m_M3u8.find(strKey);
//	if(itList != m_M3u8.end())
//	{
//		pData = itList->second;
//	}
//
//	if(pData == NULL)
//	{
//		pData = new CESMm3u8();
//		CString strTxt;
//		strTxt.Format(_T("%s\\%s.m3u8"), _T("M:\\Movie"), strKey);
//		pData->SetFile(strTxt);
//		pData->SetInfo(MOVIE_FPS_UHD_60P);
//
//		m_M3u8.insert(make_pair(strKey, pData));
//	}
//}
//
//CESMm3u8 * CMainFrame::GetM3u8(CString strKey)
//{
//	CESMm3u8 *pData = NULL;
//	map<CString, CESMm3u8 *>::iterator itList = m_M3u8.find(strKey);
//	if(itList != m_M3u8.end())
//	{
//		pData = itList->second;
//	}
//	return pData;
//}
//
//void CMainFrame::DeleteM3u8()
//{
//	for(auto it = m_M3u8.begin(); it != m_M3u8.end(); it++)
//	{
//		CESMm3u8 *pData = it->second;
//		if(pData)
//		{
//			delete pData;
//			pData = NULL;
//		}
//	}
//
//	m_M3u8.clear();
//}

void CMainFrame::SetLiveList(vector<CString> list)
{
	m_arrLive = list;
}
vector<CString> CMainFrame::GetLiveList()
{
	return m_arrLive;
}

void CMainFrame::InitLiveListParser()
{
	CFile ReadFile;
	CString _InputData;
	_InputData.Format(_T("%s\\Setup\\CameraList.csv"), ESMGetPath(ESM_PATH_FILESERVER));

	if(!ReadFile.Open(_InputData, CFile::modeRead))
		return;

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());

	if(iLength == 0)
		return;

	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0;
			
	vector<CString> _List;
	
	while(1)
	{
		CString sLine;
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;
		CString strCamera, strGroup;
		AfxExtractSubString(strCamera, sLine, 0, ',');
		AfxExtractSubString(strGroup, sLine, 6, ',');
		strCamera.Trim();
		strGroup.Trim();
		
		if(strGroup.IsEmpty())
			break;
				
		CString strIdx;
		AfxExtractSubString(strIdx, strGroup, 1, '/');
		strIdx.Trim();
		if(strIdx.IsEmpty() != TRUE && strIdx == _T("1"))
		{
			_List.push_back(strCamera);
		}
	}

	SetLiveList(_List);

	SendToLiveList();

}

void CMainFrame::SendToLiveList()
{
	vector<CString> _List;
	_List = GetLiveList();

	CString _arr;

	for(int i = 0 ; i < _List.size();i++)
	{
		/*if(i ==_List.size() -1 )
		{
			_arr.Format(_T(""+_arr+_List[i]));
		}
		else
		{
			_arr.Format(_T(""+_arr+_List[i]+","));
		}*/
		_arr.Append(_List[i]);
		_arr.Append(_T(","));
	}
	
	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_REFEREE_LIVE;

	CString * pData = NULL;
	pData = new CString;
	pData->Format(_T("%s"), _arr);

	pMsg->pParam = (LPARAM)pData;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

}

void CMainFrame::MakeFhdM3u8(CString strKey)
{
	CESMm3u8 *pData = NULL;

	/*map<CString, CESMm3u8 *>::iterator itList = m_FHDM3u8.find(strKey);
	if(itList != m_FHDM3u8.end())
	{
		pData = itList->second;
	}*/

	pData = GetFhdM3u8(strKey);

	if(pData == NULL)
	{
		CString strTxt;
		strTxt.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("fhd"));
		ESMCreateAllDirectories(strTxt);
		ESMLog(5, _T("m3u8 : %s"), strTxt);

		pData = new CESMm3u8();
		strTxt.Format(_T("%s\\%s\\%s.m3u8"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("fhd"), strKey);
		pData->SetFile(strTxt);
		//pData->SetInfo(MOVIE_FPS_UHD_60P);
		//ESMLog(5, _T("MakeM3u8 : %s"),strKey);

		m_FHDM3u8.insert(make_pair(strKey, pData));
	}
}

CESMm3u8 * CMainFrame::GetFhdM3u8(CString strKey)
{
	//ESMLog(5, _T("GetM3u8 : %s"), strKey);
	CESMm3u8 *pData = NULL;
	map<CString, CESMm3u8 *>::iterator itList = m_FHDM3u8.find(strKey);
	if(itList != m_FHDM3u8.end())
	{
		pData = itList->second;
	}
	return pData;
}

void CMainFrame::MakeUhdM3u8(CString strKey)
{
	CESMm3u8 *pData = NULL;

	/*map<CString, CESMm3u8 *>::iterator itList = m_UHDM3u8.find(strKey);
	if(itList != m_UHDM3u8.end())
	{
		pData = itList->second;
	}*/

	pData = GetUhdM3u8(strKey);

	if(pData == NULL)
	{
		CString strTxt;
		strTxt.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("uhd"));
		ESMCreateAllDirectories(strTxt);
		ESMLog(5, _T("m3u8 : %s"), strTxt);

		pData = new CESMm3u8();
		strTxt.Format(_T("%s\\%s\\%s.m3u8"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("uhd"), strKey);
		pData->SetFile(strTxt);
		//pData->SetInfo(MOVIE_FPS_UHD_60P);
		//ESMLog(5, _T("MakeM3u8 : %s"),strKey);

		m_UHDM3u8.insert(make_pair(strKey, pData));
	}
}

CESMm3u8 * CMainFrame::GetUhdM3u8(CString strKey)
{
	//ESMLog(5, _T("GetM3u8 : %s"), strKey);
	CESMm3u8 *pData = NULL;
	map<CString, CESMm3u8 *>::iterator itList = m_UHDM3u8.find(strKey);
	if(itList != m_UHDM3u8.end())
	{
		pData = itList->second;
	}
	return pData;
}

void CMainFrame::DeleteAllM3u8()
{
	for(auto it = m_FHDM3u8.begin(); it != m_FHDM3u8.end(); it++)
	{
		CESMm3u8 *pData = it->second;
		if(pData)
		{
			pData->ResetList();

			delete pData;
			pData = NULL;
		}
	}

	m_FHDM3u8.clear();

	for(auto it = m_UHDM3u8.begin(); it != m_UHDM3u8.end(); it++)
	{
		CESMm3u8 *pData = it->second;
		if(pData)
		{
			pData->ResetList();

			delete pData;
			pData = NULL;
		}
	}

	m_UHDM3u8.clear();
}

void CMainFrame::ResetAllM3u8()
{
	ESMLog(5, _T("ResetM3u8"));

	for(auto it = m_FHDM3u8.begin(); it != m_FHDM3u8.end(); it++)
	{
		CESMm3u8 *pData = it->second;
		if(pData)
		{
			pData->ResetList();
		}
	}
	
	for(auto it = m_UHDM3u8.begin(); it != m_UHDM3u8.end(); it++)
	{
		CESMm3u8 *pData = it->second;
		if(pData)
		{
			pData->ResetList();
		}
	}
	
	CESMFileOperation fo;

	CString strTxt;
	strTxt.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("fhd"));
	fo.DeleteEx(strTxt, FALSE, _T("*.ts"));
	fo.DeleteEx(strTxt, FALSE, _T("*.jpg"));

	strTxt.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("uhd"));
	fo.DeleteEx(strTxt, FALSE, _T("*.ts"));
	fo.DeleteEx(strTxt, FALSE, _T("*.jpg"));
}

void CMainFrame::MakeRefereeMovie(int nTime, CString strMake, int nMax/* = 3*/)
{
	CESMm3u8 * _fhd = NULL;
	_fhd = GetFhdM3u8(strMake);

	for(int i = 0; i < nMax; i++)
	{
		CString strFile;
		strFile.Format(_T("%s\\%s\\%s_%d.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), strMake, nTime + i);
		//ESMLog(5, _T("MakeReferee FHD : %s"), strFile);
		
		//ESMTranscoding(strFile, 1);
		ESMTranscoding(strFile, 1, _fhd);
		ESMSetWait(500);
		//_fhd->Write(strFile);
	}

	CESMm3u8 * _uhd = NULL;
	_uhd = GetUhdM3u8(strMake);

	for(int i = 0; i < nMax; i++)
	{
		CString strFile;
		strFile.Format(_T("%s\\%s\\%s_%d.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), strMake, nTime + i);
		//ESMLog(5, _T("MakeReferee UHD : %s"), strFile);

		//ESMTranscoding(strFile, 2);
		ESMTranscoding(strFile, 2, _uhd);
		ESMSetWait(500);
		//_uhd->Write(strFile);
	}

}

void CMainFrame::OnEncoder(CString strFile, int nMode, void * pM3u8)
{
	THREAD_ENCODER * pData = new THREAD_ENCODER;
	pData->pParent = this;
	pData->nMode = nMode;
	pData->pM3u8 = (CESMm3u8*)pM3u8;
	pData->strFile = strFile;

	HANDLE hEncoder = NULL;
	hEncoder = (HANDLE) _beginthreadex(NULL,0, MovieEncoderThread, (void*)pData,0,NULL);
	CloseHandle(hEncoder);

	
}

unsigned WINAPI CMainFrame::MovieEncoderThread(LPVOID param)
{
	THREAD_ENCODER * pData = (THREAD_ENCODER*) param;
	CMainFrame * pParent = (CMainFrame *)pData->pParent;
	int nMode = pData->nMode;
	CESMm3u8 * pM3u8 = pData->pM3u8;
	CString strInput = pData->strFile;

	CString strPath;
	if(nMode == 1)
	{
		strPath.Format(_T("%s\\%s\\"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("fhd"));
	}else if(nMode == 2)
	{
		strPath.Format(_T("%s\\%s\\"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("uhd"));
	}else
	{
		strPath.Format(_T("%s\\%s\\"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("hd"));
	}

	CESMEncoderForReferee *pEncoder = new CESMEncoderForReferee(
		strInput, strPath, (CESMEncoderForReferee::ENCODING_SIZE)nMode);
	pEncoder->SetM3u8(pM3u8);

	if(nMode == 0)
		pEncoder->Run();
	else
		pEncoder->Run_I();

	return 0;
}

BOOL CMainFrame::KeyDownMsg( MSG* pMsg )
{
	if (pMsg == NULL)
		return FALSE;

	if(pMsg->message == WM_KEYDOWN)
	{
		TRACE(_T("KEY DOWN\n"));
		BOOL bShift = ((GetKeyState(VK_SHIFT) & 0x8000) != 0); // Shift 키가 눌렸는지의 여부 저장
		BOOL bControl = ((GetKeyState(VK_CONTROL) & 0x8000) != 0); // Control 키가 눌렸는지의 여부 저장
		BOOL bAlt = ((GetKeyState(VK_MENU) & 0x8000) != 0);

		ESMSetSyncObj(bShift);

		if(bControl && !bShift && !bAlt)
			MakeCShortCut(pMsg->wParam);

		if(!bControl && !bShift && !bAlt)
		{			
			MakeShortCut(pMsg->wParam);
			
			if(pMsg->wParam >= VK_OEM_1 && pMsg->wParam <= VK_OEM_8) // [{, ]}, -, +, :, ' ....
			{
				MakeShortCutOEM(pMsg->wParam);
			}
		}

		if(bControl && bShift && !bAlt)
			MakeCSShortCut(pMsg->wParam);

		if(bControl && !bShift && bAlt)
			MakeCAShorCut(pMsg->wParam);
	}

	if(pMsg->message == WM_KEYUP)
	{
		ESMSetSyncObj(FALSE);
	}

	if(pMsg->message == WM_SYSKEYDOWN)
	{
		if(pMsg->wParam == VK_F10)
		{
			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_ESM_ENV_TEMPLATE;
			::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
			return FALSE;
		}
	}

	return TRUE;
}

BOOL CMainFrame::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	return false;
	//return CExtNCW::OnEraseBkgnd(pDC);
}
