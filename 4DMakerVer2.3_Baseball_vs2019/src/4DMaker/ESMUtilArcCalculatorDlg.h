#pragma once

#include "ESMUtilCalculator.h"

// CESMUtilArcCalculatorDlg 대화 상자입니다.

class CESMUtilArcCalculatorDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CESMUtilArcCalculatorDlg)

public:
	CESMUtilArcCalculatorDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMUtilArcCalculatorDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UTIL_ARC_CALCULATOR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnUtilityArccalculator();

public:	
	virtual void OnCancel();	

public:
	afx_msg void OnChangeEditInputRadius();	
	afx_msg void OnChangeEditInputDegree();

public:
	CESMUtilCalculator *m_pUtilCaltulator;

public:
	CString m_strDegree;
	CString m_strRadius;
	CString m_strArcLength;
	
	double m_dDegree;
	double m_dRadius;
	double m_dArcLength;
};
