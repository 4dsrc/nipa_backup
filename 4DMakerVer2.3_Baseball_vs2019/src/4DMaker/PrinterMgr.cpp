#include "StdAfx.h"
#include "PrinterMgr.h"
#include <Winspool.h>

#define PRINT_MAX_SIZE_X 1920
#define PRINT_MAX_SIZE_Y 1140



CPrinterMgr::CPrinterMgr(void)
{
}


CPrinterMgr::~CPrinterMgr(void)
{

}

BOOL CPrinterMgr::PrintStart(CString strPrinter, CString strFile, int nZoom)
{
	std::wstring strTemp(strPrinter);
	bool isConnect = IsPrinterOnline(strTemp);

	if (isConnect == false)
		return FALSE;

	HGLOBAL hDevMode;
	HGLOBAL hDevNames;
	if (GetPrinterDevice((LPTSTR)(LPCTSTR)strPrinter, &hDevNames, &hDevMode))
	{
		AfxGetApp()->SelectPrinter(hDevNames, hDevMode);
	}
	else
	{
		AfxMessageBox(_T("GetPrinterDevice called failed\n"));
		return FALSE;
	}
	CPrintDialog PrintDlg(FALSE);
	PrintDlg.m_pd.hDevMode = hDevMode;
	PrintDlg.m_pd.hDevNames = hDevNames;


	PrintDlg.CreatePrinterDC();

	HDC hPrintDC = PrintDlg.GetPrinterDC();
	CDC* pDC = CDC::FromHandle(hPrintDC);
	pDC->m_bPrinting = TRUE;
	DOCINFO di;
	::ZeroMemory (&di, sizeof (DOCINFO));
	di.cbSize = sizeof (DOCINFO);
	di.lpszDocName = strFile;
	BOOL bPrintingOK = pDC->StartDoc(&di);

	CFileFind pFind;
	if(!pFind.FindFile(strFile))
	{
		CString strMsg;
		strMsg.Format(_T("[Print] Fail [%s] not find!"), strFile);
		AfxMessageBox(strMsg);
		return FALSE;
	}

	//Image Load
	HBITMAP hBmp = NULL;
	hBmp = LoadGraphicsFile(strFile);
	CBitmap bitmap;
	if(!bitmap.Attach(hBmp))
		return FALSE;
	BITMAP bm;
	bitmap.GetBitmap(&bm);
	SetDIBToDevice(pDC->m_hDC,hBmp,0,60,PRINT_MAX_SIZE_X,PRINT_MAX_SIZE_Y,0,0, nZoom,true);


	CPrintInfo Info;
	Info.SetMaxPage(1); // just one page
	int maxw = pDC->GetDeviceCaps(HORZRES);
	int maxh = pDC->GetDeviceCaps(VERTRES);
	Info.m_rectDraw.SetRect(0, 0, maxw, maxh);



	for (UINT page = Info.GetMinPage(); page <=
		Info.GetMaxPage() && bPrintingOK; page++)
	{
		pDC->StartPage();    // 문서시작
		Info.m_nCurPage = page;
		bPrintingOK = (pDC->EndPage ()> 0); // end page 
	} 

	if (bPrintingOK) 
	{ 
		pDC->EndDoc (); // end a print job 
		return TRUE; 
	} 
	else  
	{ 
		pDC->AbortDoc (); // abort job. 
		return FALSE; 
	} 
}


BOOL CPrinterMgr::SetDIBToDevice(HDC hdc,HBITMAP hbmp,int xDest,int yDest,int DestWidth,int DestHeight,	int xSrc,int ySrc,int nZoom, bool bStretch,int nStretchMode,DWORD dwRop)
{
	BITMAP bmp;
	HANDLE hDibBits;    // array for bitmap bits = bitmap width * hight
	LPSTR pDibBits;    // point of hDibBits
	HANDLE hBitInfo;    // Handle of BITMAPINFO
	LPBITMAPINFO pBitInfo;    // point of hBitInfo
	int err;
	WORD    cClrBits;

	GetObject(hbmp,sizeof(bmp),&bmp);//get bitmap structure from DIB handle
	// Convert the color format to a count of bits. 
	cClrBits = (WORD)(bmp.bmPlanes * bmp.bmBitsPixel); 
	if (cClrBits == 1) 
		cClrBits = 1; 
	else if (cClrBits <= 4) 
		cClrBits = 4; 
	else if (cClrBits <= 8) 
		cClrBits = 8; 
	else if (cClrBits <= 16) 
		cClrBits = 16; 
	else if (cClrBits <= 24) 
		cClrBits = 24; 
	else cClrBits = 32; 

	if (cClrBits != 24) 
		hBitInfo = GlobalAlloc(GHND, sizeof(BITMAPINFOHEADER) + 		sizeof(RGBQUAD) * (1<< cClrBits)); 
	// There is no RGBQUAD array for the 24-bit-per-pixel format. 
	else 
		hBitInfo =GlobalAlloc(GHND, sizeof(BITMAPINFOHEADER)); 

	if(!hBitInfo)
	{
		AfxMessageBox(_T("Global Alloc Error"));return false;
	}

	pBitInfo = (LPBITMAPINFO)GlobalLock(hBitInfo);
	pBitInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	pBitInfo->bmiHeader.biWidth = bmp.bmWidth;
	pBitInfo->bmiHeader.biHeight = bmp.bmHeight;
	pBitInfo->bmiHeader.biPlanes = bmp.bmPlanes;
	pBitInfo->bmiHeader.biBitCount =bmp.bmBitsPixel;    // 2^8 = 256, 2^4 = 16, 2^1 = mono
	pBitInfo->bmiHeader.biCompression = BI_RGB;
	pBitInfo->bmiHeader.biSizeImage = ((pBitInfo->bmiHeader.biWidth * cClrBits +31) & ~31) /8 * pBitInfo->bmiHeader.biHeight; 
	pBitInfo->bmiHeader.biXPelsPerMeter = 0;
	pBitInfo->bmiHeader.biYPelsPerMeter = 0;
	if (cClrBits < 24) 
		pBitInfo->bmiHeader.biClrUsed = (1<<cClrBits); 

	pBitInfo->bmiHeader.biClrImportant = 0;
	hDibBits = GlobalAlloc(GHND,pBitInfo->bmiHeader.biSizeImage);
	pDibBits = (LPSTR)GlobalLock(hDibBits);
	// GetDIBits()로 HBITMAP을 bit정보를 이용하여 DIB로 만든다.
	if(!GetDIBits(hdc, hbmp, 0, bmp.bmHeight, pDibBits, pBitInfo, DIB_RGB_COLORS))
	{
		AfxMessageBox(_T("Get DIB Information Failure"));return false; 
	}

	int srcX, srcY;
	int srcW, srcH;

	if(bStretch)
	{
		SetStretchBltMode(hdc,nStretchMode);

		switch(nZoom)
		{
		case IMAGE_ZOOM_NORMAL:
			err=StretchDIBits(hdc,xDest,yDest,DestWidth,DestHeight, xSrc,ySrc,bmp.bmWidth,bmp.bmHeight, pDibBits, pBitInfo, DIB_RGB_COLORS,dwRop);
			break;
		case IMAGE_ZOOM_3_2:
			srcW = (bmp.bmWidth/3) * 2;
			srcH = (bmp.bmHeight/3) * 2;
			srcX = bmp.bmWidth - (srcW + ((bmp.bmWidth-srcW)/2));
			srcY = bmp.bmHeight - (srcH + ((bmp.bmHeight-srcH)/2));
			err=StretchDIBits(hdc,xDest,yDest,DestWidth,DestHeight, srcX,srcY,srcW,srcH, pDibBits, pBitInfo, DIB_RGB_COLORS,dwRop);
			break;
		case IMAGE_ZOOM_2_1:
			srcW = (bmp.bmWidth/2) * 1;
			srcH = (bmp.bmHeight/2) * 1;
			srcX = bmp.bmWidth - (bmp.bmWidth/2 + srcW/2);
			srcY = bmp.bmHeight - (bmp.bmHeight/2 + srcH/2);
			err=StretchDIBits(hdc,xDest,yDest,DestWidth,DestHeight, srcX,srcY,srcW,srcH, pDibBits, pBitInfo, DIB_RGB_COLORS,dwRop);
			break;
		case IMAGE_ZOOM_5_4:
			srcW = (bmp.bmWidth/5) * 4;
			srcH = (bmp.bmHeight/5) * 4;
			srcX = bmp.bmWidth - (srcW + ((bmp.bmWidth-srcW)/2));
			srcY = bmp.bmHeight - (srcH + ((bmp.bmHeight-srcH)/2));
			err=StretchDIBits(hdc,xDest,yDest,DestWidth,DestHeight, srcX,srcY,srcW,srcH, pDibBits, pBitInfo, DIB_RGB_COLORS,dwRop);
			break;
		default:
			err=StretchDIBits(hdc,xDest,yDest,DestWidth,DestHeight, xSrc,ySrc,bmp.bmWidth,bmp.bmHeight, pDibBits, pBitInfo, DIB_RGB_COLORS,dwRop);
			break;
		}

	}
	else
		err=SetDIBitsToDevice(hdc,xDest,yDest,DestWidth,DestHeight,xSrc,ySrc,0,bmp.bmHeight,pDibBits,pBitInfo,DIB_RGB_COLORS);


	GlobalUnlock(hDibBits);
	GlobalFree(hDibBits);
	GlobalUnlock(hBitInfo);
	GlobalFree(hBitInfo);
	if(err==GDI_ERROR)
	{
		AfxMessageBox(_T("Printing Job Fail"));return false;
	}

	return TRUE;
} 



HBITMAP CPrinterMgr::LoadGraphicsFile(LPCTSTR szFileName)
{
	{ 
		HANDLE  hFile   = INVALID_HANDLE_VALUE; 
		LPVOID  pvData  = NULL; 
		HGLOBAL hGlobal = NULL; 
		LPPICTURE pPicture; 
		try 
		{ 
			HRESULT hr; 
			DWORD   dwFileSize; 
			// 파일 열기 
			hFile = CreateFile(szFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL); 
			// 파일이 없을 경우 
			if(hFile == INVALID_HANDLE_VALUE) 
			{ 
				TRACE("GetBitmapFromFile() File Open Error"); 
				throw -1; 
			} 

			// 파일 크기 얻기 
			dwFileSize = GetFileSize(hFile, NULL); 
			if(dwFileSize == -1) 
			{ 
				TRACE("GetBitmapFromFile() File Read Error"); 
				throw -1;        
			} 

			// 파일 크기만큼 동적할당 
			hGlobal = GlobalAlloc(GMEM_MOVEABLE, dwFileSize); 
			if(hGlobal == NULL) 
			{ 
				TRACE("GetBitmapFromFile() GlobalAlloc Error"); 
				throw -1; 
			} 
			pvData = GlobalLock(hGlobal); 
			if(pvData == NULL) 
			{ 
				TRACE("GetBitmapFromFile() GlobalLock Error"); 
				throw -1; 
			} 
			DWORD dwBytesRead = 0; 
			BOOL bRead = ReadFile(hFile, pvData, dwFileSize, &dwBytesRead, NULL); 
			if(!bRead) 
			{ 
				TRACE("GetBitmapFromFile() ReadFile Error"); 
				throw -1; 
			} 
			GlobalUnlock(hGlobal); 
			CloseHandle(hFile); 
			LPSTREAM pstm = NULL; 
			hr = CreateStreamOnHGlobal(hGlobal, TRUE, &pstm); 
			if(S_OK != hr || pstm == NULL) 
				throw -1; 
			// Stream 에서 IPicture 를 생성 
			hr = ::OleLoadPicture(pstm, dwFileSize, FALSE, IID_IPicture, (LPVOID *)&pPicture); 
			if(S_OK != hr || pPicture == NULL) 
			{ 
				TRACE("Graphic File Load Error"); 
				return false; 
			} 
			pstm->Release(); 
			GlobalFree(hGlobal); 
			HBITMAP PicHandle; 
			pPicture->get_Handle((unsigned int *)&PicHandle); 
			HBITMAP NewHandle = (HBITMAP)CopyImage(PicHandle, IMAGE_BITMAP, 0, 0, LR_COPYRETURNORG); 
			pPicture->Release(); 
			return NewHandle; 

		} 
		catch (int e) 
		{ 
			UNREFERENCED_PARAMETER (e); 
			if (hFile != INVALID_HANDLE_VALUE) 
				CloseHandle(hFile); 
			if (hGlobal) 
			{ 
				GlobalUnlock(hGlobal); 
				GlobalFree(hGlobal); 
			} 
			if (pPicture) 
				pPicture->Release(); 
		} 
	} 
}

BOOL CPrinterMgr::GetPrinterDevice(LPTSTR pszPrinterName, HGLOBAL* phDevNames, HGLOBAL* phDevMode)
{
	// if NULL is passed, then assume we are setting app object's
	// devmode and devnames
	if (phDevMode == NULL || phDevNames == NULL)
		return FALSE;

	// Open printer
	HANDLE hPrinter;
	if (OpenPrinter(pszPrinterName, &hPrinter, NULL) == FALSE)
		return FALSE;

	// obtain PRINTER_INFO_2 structure and close printer
	DWORD dwBytesReturned, dwBytesNeeded;
	GetPrinter(hPrinter, 2, NULL, 0, &dwBytesNeeded);
	PRINTER_INFO_2* p2 = (PRINTER_INFO_2*)GlobalAlloc(GPTR,
		dwBytesNeeded);
	if (GetPrinter(hPrinter, 2, (LPBYTE)p2, dwBytesNeeded,
		&dwBytesReturned) == 0) {
			GlobalFree(p2);
			ClosePrinter(hPrinter);
			return FALSE;
	}
	ClosePrinter(hPrinter);

	// Allocate a global handle for DEVMODE
	HGLOBAL  hDevMode = GlobalAlloc(GHND, sizeof(*p2->pDevMode) +
		p2->pDevMode->dmDriverExtra);
	ASSERT(hDevMode);
	DEVMODE* pDevMode = (DEVMODE*)GlobalLock(hDevMode);
	ASSERT(pDevMode);

	// copy DEVMODE data from PRINTER_INFO_2::pDevMode
	memcpy(pDevMode, p2->pDevMode, sizeof(*p2->pDevMode) +
		p2->pDevMode->dmDriverExtra);
	GlobalUnlock(hDevMode);

	// Compute size of DEVNAMES structure from PRINTER_INFO_2's data
	DWORD drvNameLen = lstrlen(p2->pDriverName)+1;  // driver name
	DWORD ptrNameLen = lstrlen(p2->pPrinterName)+1; // printer name
	DWORD porNameLen = lstrlen(p2->pPortName)+1;    // port name

	// Allocate a global handle big enough to hold DEVNAMES.
	HGLOBAL hDevNames = GlobalAlloc(GHND,
		sizeof(DEVNAMES) +
		(drvNameLen + ptrNameLen + porNameLen)*sizeof(TCHAR));
	ASSERT(hDevNames);
	DEVNAMES* pDevNames = (DEVNAMES*)GlobalLock(hDevNames);
	ASSERT(pDevNames);

	// Copy the DEVNAMES information from PRINTER_INFO_2
	// tcOffset = TCHAR Offset into structure
	int tcOffset = sizeof(DEVNAMES)/sizeof(TCHAR);
	ASSERT(sizeof(DEVNAMES) == tcOffset*sizeof(TCHAR));

	pDevNames->wDriverOffset = tcOffset;
	memcpy((LPTSTR)pDevNames + tcOffset, p2->pDriverName,
		drvNameLen*sizeof(TCHAR));
	tcOffset += drvNameLen;

	pDevNames->wDeviceOffset = tcOffset;
	memcpy((LPTSTR)pDevNames + tcOffset, p2->pPrinterName,
		ptrNameLen*sizeof(TCHAR));
	tcOffset += ptrNameLen;

	pDevNames->wOutputOffset = tcOffset;
	memcpy((LPTSTR)pDevNames + tcOffset, p2->pPortName,
		porNameLen*sizeof(TCHAR));
	pDevNames->wDefault = 0;

	GlobalUnlock(hDevNames);
	GlobalFree(p2);   // free PRINTER_INFO_2

	// set the new hDevMode and hDevNames
	*phDevMode = hDevMode;
	*phDevNames = hDevNames;
	return TRUE;
} 


bool CPrinterMgr::IsPrinterOnline(wstring strPrinterFriendlyName)
{
	HANDLE hPrinter ;
	if ( OpenPrinter(const_cast<LPWSTR>(strPrinterFriendlyName.c_str()), &hPrinter, NULL) == 0 )
	{    
		/*OpenPrinter call failed*/
		return false;
	}

	DWORD dwBufsize = 0;
	PRINTER_INFO_2* pinfo = 0;
	int nRet = 0;
	nRet = GetPrinter(hPrinter, 2,(LPBYTE)pinfo, dwBufsize, &dwBufsize); //Get dwBufsize
	DWORD dwGetPrinter = 0;
	if (nRet == 0)
	{
		dwGetPrinter = GetLastError(); 
	}

	PRINTER_INFO_2* pinfo2 = (PRINTER_INFO_2*)malloc(dwBufsize); //Allocate with dwBufsize
	nRet = GetPrinter(hPrinter, 2,reinterpret_cast<LPBYTE>(pinfo2), dwBufsize, &dwBufsize);
	if (nRet == 0)
	{
		dwGetPrinter = GetLastError(); 
		return false;
	}

	if (pinfo2->Attributes & PRINTER_ATTRIBUTE_WORK_OFFLINE )
	{
		free(pinfo2); 
		ClosePrinter( hPrinter );
		return false;
	}

	free(pinfo2); 
	ClosePrinter( hPrinter );
	return true;
}