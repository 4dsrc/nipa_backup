// ZoomAllDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ZoomAllDlg.h"
#include "afxdialogex.h"
#include "DSCItem.h"

// CZoomAllDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CZoomAllDlg, CDialogEx)

CZoomAllDlg::CZoomAllDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CZoomAllDlg::IDD, pParent)
{

}

CZoomAllDlg::~CZoomAllDlg()
{
}

void CZoomAllDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_ENLARGE, m_ctrlEnLarge);
}


BEGIN_MESSAGE_MAP(CZoomAllDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_MOVE, &CZoomAllDlg::OnBnClickedBtnMove)
END_MESSAGE_MAP()


// CZoomAllDlg 메시지 처리기입니다.

BOOL CZoomAllDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_ctrlEnLarge.AddString(_T("x1"));
	m_ctrlEnLarge.AddString(_T("x3"));
	m_ctrlEnLarge.AddString(_T("x4"));
	m_ctrlEnLarge.AddString(_T("x5"));
	m_ctrlEnLarge.AddString(_T("x6"));
	m_ctrlEnLarge.AddString(_T("x7"));
	m_ctrlEnLarge.AddString(_T("x8"));
	m_ctrlEnLarge.AddString(_T("x9"));
	m_ctrlEnLarge.AddString(_T("x10"));
	m_ctrlEnLarge.SetCurSel(0);

	CWnd* pCombo = GetDlgItem(IDC_COMBO_ENLARGE);
	HWND hWnd = ::FindWindowEx(pCombo->m_hWnd, NULL, _T("Edit"), NULL);
	if(NULL != hWnd) ((CEdit *)CWnd::FromHandle(hWnd))->SetReadOnly(TRUE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CZoomAllDlg::OnBnClickedBtnMove()
{
	int nIndex = m_ctrlEnLarge.GetCurSel();

	switch(nIndex)
	{
	case 0:
		nIndex = 0;
		break;
	default:
		nIndex = nIndex + 2;
		break;
	}

	SetZoom(nIndex);
}

void CZoomAllDlg::SetZoom( int nValue )
{
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	CString strValue;

	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);

		if (!pItem)
			return;

		if (pItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
			continue;

		ESMEvent* pSendMsg = NULL;
		pSendMsg = new ESMEvent;
		pSendMsg->message = WM_SDI_OP_SET_ENLARGE;
		pSendMsg->nParam1 = nValue;
		pItem->SdiAddMsg(pSendMsg);
	}
}
