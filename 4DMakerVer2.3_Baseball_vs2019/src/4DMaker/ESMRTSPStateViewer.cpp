// ESMRTSPStateViewer.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "MainFrm.h"
#include "ESMRTSPStateViewer.h"
#include "afxdialogex.h"


// CESMRTSPStateViewer 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMRTSPStateViewer, CDialogEx)

CESMRTSPStateViewer::CESMRTSPStateViewer(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMRTSPStateViewer::IDD, pParent)
{
	m_LcFTP.SetUseCamStatusColor(FALSE);
	m_nSyncCount = 0;
	m_strBirdViewPath = _T("\\\\172.21.187.95\\4DMaker");
	m_bProcessRun = FALSE;

	m_bThreadStop = TRUE;

	InitializeCriticalSection(&m_criInsertData);
	m_nLastFrameCnt = 0;
	m_nLastRepeatCnt = 0;
}

CESMRTSPStateViewer::~CESMRTSPStateViewer()
{
	SetThreadStop(FALSE);

	map<CString,stRecordState*>::iterator iter;
	
	for (iter = m_mapRecordState.begin(); iter != m_mapRecordState.end(); ++iter)
	{
		CString str4DM = iter->first;
		stRecordState* stRecord = iter->second;

		if(stRecord)
		{
			stRecord->nArrDSC.clear();
			delete stRecord;
			stRecord = NULL;
		}
	}
	m_mapRecordState.clear();

	DeleteCriticalSection(&m_criInsertData);
}

void CESMRTSPStateViewer::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX,IDC_LIST_RTSPVIEWER_DSCLIST,m_LcFTP);
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CESMRTSPStateViewer, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_RTSPVIEW_START, &CESMRTSPStateViewer::OnBnClickedBtnRtspviewStart)
	ON_BN_CLICKED(IDC_BTN_SERVICE_FINISH, &CESMRTSPStateViewer::OnBnClickedBtnServiceFinish)
	ON_BN_CLICKED(IDC_BTN_RTSPVIEW_RECIN, &CESMRTSPStateViewer::OnBnClickedBtnRtspviewRecin)
	ON_BN_CLICKED(IDC_BTN_RTSPVIEW_RECOUT, &CESMRTSPStateViewer::OnBnClickedBtnRtspviewRecout)
	ON_BN_CLICKED(IDCANCEL, &CESMRTSPStateViewer::OnBnClickedCancel)
END_MESSAGE_MAP()


// CESMRTSPStateViewer 메시지 처리기입니다.


BOOL CESMRTSPStateViewer::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_LcFTP.InsertColumn(RTSP_LIST_INDEX,_T("#"),0,20);
	m_LcFTP.InsertColumn(RTSP_LIST_IP,_T("IP"),0,1);
	m_LcFTP.InsertColumn(RTSP_LIST_DSC,_T("DSC"),0,50);
	m_LcFTP.InsertColumn(RTSP_LIST_RECORDNAME,_T("4DM"),0,1);
	
	m_LcFTP.InsertColumn(RTSP_LIST_MAX + RTSP_MESSAGE_STATE_RECORD_IN,_T("REC IN"),0,50);
	m_LcFTP.InsertColumn(RTSP_LIST_MAX + RTSP_MESSAGE_STATE_DECODE_IN,_T("DEC IN"),0,1);
	m_LcFTP.InsertColumn(RTSP_LIST_MAX + RTSP_MESSAGE_STATE_PROCESS_IN,_T("PROC IN"),0,1);
	m_LcFTP.InsertColumn(RTSP_LIST_MAX + RTSP_MESSAGE_STATE_REMAIN,_T("REMAIN"),0,50);
	m_LcFTP.InsertColumn(RTSP_LIST_MAX + RTSP_MESSAGE_STATE_PLAY_IN,_T("PLAY IN"),0,50);
	m_LcFTP.InsertColumn(RTSP_LIST_MAX + RTSP_MESSAGE_STATE_FRAME_COUNT, _T("FrameCount"),0,70);
	GetDlgItem(IDC_EDIT_RTSP_BIRDVIEW)->SetWindowText(m_strBirdViewPath);
	//m_LcFTP
	UpdateDSCList();
	return TRUE;
}
void CESMRTSPStateViewer::UpdateDSCList()
{
	CObArray DSCList;
	ESMGetDSCList(&DSCList);

	CString strInsertData;

	for(int i = 0 ; i < DSCList.GetCount() ; i++)
	{
		CDSCItem* pItem = (CDSCItem*) DSCList.GetAt(i);

		if(pItem)
		{
			strInsertData.Format(_T("%d"),i+1);
			m_LcFTP.InsertItem(i,strInsertData);

			m_LcFTP.SetItemText(i,RTSP_LIST_IP,pItem->GetDSCInfo(DSC_INFO_LOCATION));

			m_LcFTP.SetItemText(i,RTSP_LIST_DSC,pItem->GetDeviceDSCID());
		}
	}
}
void CESMRTSPStateViewer::Update4DAPState(CString str,int nIndex,int nType,int nValue)
{
	if(nIndex == 0)
		return;

	//UpdateData(TRUE);

	CString strInsertData;

	m_LcFTP.SetItemText(nIndex - 1,RTSP_LIST_RECORDNAME,str);
	
	strInsertData.Format(_T("%d"),nValue);
	m_LcFTP.SetItemText(nIndex - 1,RTSP_LIST_MAX+nType,strInsertData);

	//UpdateData(FALSE);
}
void CESMRTSPStateViewer::SetRTSPCommand(RTSP_RESTFul_COMMAND command,CString str4DM,int nCount)
{
	switch(command)
	{
	case RTSP_COMMAND_RECORD_START:
		{

		}
		break;
	case RTSP_COMMAND_RECORD_RESUME:
		{
			m_nSyncCount = nCount;
			LaunchSyncFinishThread(str4DM,nCount);
		}
		break;
	case RTSP_COMMAND_RECORD_PAUSE:
		{
			SendPauseMessage();
		}
		break;
	case RTSP_COMMAND_RECORD_END:
		{

		}
		break;
	}
}
void CESMRTSPStateViewer::LaunchSyncFinishThread(CString str4DM,int nSyncCnt)
{
	for(int i = 0 ; i < m_LcFTP.GetItemCount() ; i++)
	{
		CString strInsertData;

		strInsertData.Format(_T("%d"),0);
		for(int nType = 0 ; nType < 4 ; nType++)
			m_LcFTP.SetItemText(i,RTSP_LIST_MAX+nType,strInsertData);
	}

	/*if(GetRecordState() != NULL)
		return;*/

	if(m_mapRecordState[str4DM] == NULL)
	{
		CObArray dsclist;
		ESMGetDSCList(&dsclist);
		int nSize = dsclist.GetCount();

		stRecordState* pstRecordState = new stRecordState;
		m_mapRecordState[str4DM] = pstRecordState;

		for(int i = 0 ; i < nSize ; i++)
			m_mapRecordState[str4DM]->nArrDSC.push_back(0);

		m_mapRecordState[str4DM]->nSyncCount = nSyncCnt;
		//m_mapRecordState[str4DM].nArrDSC.assign(m_LcFTP.GetItemCount());

		THREAD_RTSP_VIEWER* pViewer = new THREAD_RTSP_VIEWER;
		pViewer->str4DM.Format(_T("%s"),str4DM);
		pViewer->pViewer = this;

		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)_beginthreadex(NULL,0,_SyncFinishThread,(void*)pViewer,0,NULL);
		CloseHandle(hSyncTime);
	}
}
unsigned WINAPI CESMRTSPStateViewer::_SyncFinishThread(LPVOID param)
{
	//CESMRTSPStateViewer* pViewer = (CESMRTSPStateViewer*)param;
	THREAD_RTSP_VIEWER* pTHREAD = (THREAD_RTSP_VIEWER*)param;
	CESMRTSPStateViewer* pViewer = pTHREAD->pViewer;
	CString str4DM = pTHREAD->str4DM;
	delete pTHREAD;pTHREAD = NULL;

	int nCount = pViewer->m_LcFTP.GetItemCount();

	int nWaitCnt = 0;
	int nWaitTime=100;
	BOOL bFinish = FALSE;

	stRecordState* pRecordState = NULL;
	pRecordState = pViewer->m_mapRecordState[str4DM];
	if(pRecordState == NULL)
		return FALSE;

	int nSyncCount = pRecordState->nSyncCount;
	ESMLog(5,_T("[RTSPViewer][%s] - SyncCount: %d"),str4DM,nSyncCount);

	while(pViewer->GetThreadStop())
	{
		int nFinish = 0;
#if 1
		int nTotalCnt = pRecordState->nArrDSC.size();
		for(int i = 0 ; i < nTotalCnt ; i++)
		{
			int nCount = pViewer->GetRTSPPlayIdx(str4DM,i);
			if(nCount)
				nFinish++;
		}
#else
		for(int i = 0 ; i < nCount ; i++)
		{
			int nCount = pViewer->GetListRemainSize(i);
			if(nCount)
				nFinish++;
		}
#endif
		if(nFinish >= nSyncCount/2)
		{
			bFinish = TRUE;
			break;
		}

		/*if(++nWaitCnt >= 5000)
			break;*/

		Sleep(1);
	}

	if(bFinish)
		pViewer->SendResumeMessage();

	while(pViewer->GetThreadStop())
	{
		int nTotalCnt = pRecordState->nArrDSC.size();
		int nRecordStopCnt = pRecordState->nRecordStopCnt;
		int nMatched = 0;

		for(int i = 0 ; i < nTotalCnt ; i++)
		{
			if(pRecordState->nRecordStopCnt - 1 == pViewer->GetRTSPPlayIdx(str4DM,i))
				nMatched++;
		}

		if(nMatched >=nSyncCount/2)
		{
			pViewer->SendPauseMessage();
			break;
		}

		Sleep(1);
	}

	if(pViewer->GetThreadStop() == TRUE)
		pViewer->DeleteRTSP4DM(str4DM);

	ESMLog(5,_T("[RTSPViewer][%s] - Delete"),str4DM);

	return FALSE;
}
int CESMRTSPStateViewer::GetListRemainSize(int nIdx)
{
	CString str;
	str = m_LcFTP.GetItemText(nIdx,RTSP_LIST_MAX+RTSP_MESSAGE_STATE_REMAIN);

	int nCount = _ttoi(str);

	return nCount;
}
void CESMRTSPStateViewer::OnBnClickedBtnRtspviewStart()
{
	//Service Start & Pause
	ESMSendRTSPHttpCmd(ESM_NET_START);
	ESMLog(5,_T("START MESSAGE"));

	SendPauseMessage();
}
void CESMRTSPStateViewer::SendResumeMessage()
{
	ESMSendRTSPHttpCmd(ESM_NET_RESUME);
	
	for(int i = 0 ; i < 10 ; i++)
		ESMLog(5,_T("RESUME MESSAGE"));
}
void CESMRTSPStateViewer::SendPauseMessage()
{
	ESMSendRTSPHttpCmd(ESM_NET_PAUSE);
	
	for(int i = 0 ; i < 10 ; i++)
		ESMLog(5,_T("PAUSE MESSAGE"));
}
void CESMRTSPStateViewer::OnBnClickedBtnServiceFinish()
{
	//Service Finish
	ESMSendRTSPHttpCmd(ESM_NET_FINISH);
	ESMLog(5,_T("FINISH MESSAGE"));
}
BOOL CESMRTSPStateViewer::GetRecordState()
{
	if(GetProcessRun() == TRUE)
	{
		int nDefault = 0;
		CString strSection = _T("Recording");
		CString strKeyName = _T("RecFlag");
		CString strServerFile;

		GetDlgItem(IDC_EDIT_RTSP_BIRDVIEW)->GetWindowText(m_strBirdViewPath);

		//\\192.168.0.34\4DMaker
		strServerFile = m_strBirdViewPath + _T("\\Recorder.ini");
		int nResult = GetPrivateProfileInt( strSection, strKeyName, nDefault, strServerFile );

		if(nResult == SDI_STATUS_REC)
		{
			return TRUE;
		}
		else //if(nResult == )
		{
			return FALSE;
		}
	}
	else
		return TRUE;
	//return FALSE;
}
void CESMRTSPStateViewer::OnBnClickedBtnRtspviewRecin()
{
	if(AfxMessageBox(_T("[WARNING] Clicked Resume button. Continue?"),MB_YESNO)==IDYES)
	{
		ESMSendRTSPHttpCmd(ESM_NET_RESUME);

		for(int i = 0 ; i < 10 ; i++)
			ESMLog(5,_T("RESUME MESSAGE"));
	}
}
void CESMRTSPStateViewer::OnBnClickedBtnRtspviewRecout()
{
	if(AfxMessageBox(_T("[WARNING] Clicked Pause button. Continue?"),MB_YESNO)==IDYES)
	{
		ESMSendRTSPHttpCmd(ESM_NET_PAUSE);

		for(int i = 0 ; i < 10 ; i++)
			ESMLog(5,_T("PAUSE MESSAGE"));
	}
}
void CESMRTSPStateViewer::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SetProcessRun(FALSE);
	CDialogEx::OnCancel();
}
void CESMRTSPStateViewer::SetRTSPPlay(CString str4DM,int nIdx,int nData)
{
	nIdx -= 1;

	EnterCriticalSection(&m_criInsertData);
	if(m_mapRecordState[str4DM] != NULL)
		m_mapRecordState[str4DM]->nArrDSC.at(nIdx) = nData;
	LeaveCriticalSection(&m_criInsertData);

	CString strInsertData;

	//m_LcFTP.SetItemText(nIdx,RTSP_LIST_RECORDNAME,str4DM);

	strInsertData.Format(_T("%d"),nData);
	m_LcFTP.SetItemText(nIdx,RTSP_LIST_MAX+RTSP_MESSAGE_STATE_PLAY_IN,strInsertData);
}
stRecordState* CESMRTSPStateViewer::GetRecordState(CString str4DM)
{
	stRecordState* pRecordState = NULL;
	EnterCriticalSection(&m_criInsertData);
	if(m_mapRecordState[str4DM] == NULL)
		pRecordState = NULL;
	else
		pRecordState = m_mapRecordState[str4DM];
	LeaveCriticalSection(&m_criInsertData);

	return pRecordState;
}
void CESMRTSPStateViewer::SetRTSPFrameCount(CString str4DM,int nIdx,int nData)
{
	nIdx -= 1;

	EnterCriticalSection(&m_criInsertData);
	if(m_mapRecordState[str4DM] != NULL)
		m_mapRecordState[str4DM]->nArrDSC.at(nIdx) = nData;
	LeaveCriticalSection(&m_criInsertData);

	CString strInsertData;

	//m_LcFTP.SetItemText(nIdx,RTSP_LIST_RECORDNAME,str4DM);

	strInsertData.Format(_T("%d"),nData);
	m_LcFTP.SetItemText(nIdx,RTSP_LIST_MAX+RTSP_MESSAGE_STATE_FRAME_COUNT,strInsertData);

	m_nLastFrameCnt = nData;
	if(m_nLastFrameCnt >= 65536)
		m_nLastRepeatCnt++;

	if(m_nLastRepeatCnt == 256)
		m_nLastRepeatCnt = 0;
}