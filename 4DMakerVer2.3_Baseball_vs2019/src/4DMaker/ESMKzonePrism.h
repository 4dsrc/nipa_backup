#pragma once

// ESMAdjustMgrDlg 대화 상자입니다.
#include "ESMKzoneImage.h"
#include "ESMFunc.h"
#include "ESMKzoneMgr.h"
#include <vector>
#include "afxwin.h"
#include "afxres.h"
#include "afxcmn.h"

#define KZONEPRISM _T("KZONEPrism.ptl")

class ESMKzonePrism : public CDialogEx
{
	DECLARE_DYNAMIC(ESMKzonePrism)

public:
	ESMKzonePrism(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~ESMKzonePrism();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_KZONE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnCustomdrawTcpList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedBtnCommit();
	afx_msg void OnLvnItemchangedAdjust(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedBtnAdjustPointReset();
	afx_msg void OnBnClickedBtn2ndSavePoint();
	afx_msg void OnBnClickedBtn2ndLoadPoint();

	afx_msg void OnBnAxisInterpolation();
	afx_msg void OnBnAxisMaking();
	afx_msg void OnBnDrawPrismBone();
	afx_msg void OnBnPrismImageMaking();
	afx_msg void Axis_Update();


	CPoint GetPointFromMouse(CPoint pt);

	static unsigned WINAPI GetMovieDataThread(LPVOID param);
	static unsigned WINAPI GetImageDataThread(LPVOID param);
	void SetMovieState(int nMovieState = ESM_ADJ_FILM_STATE_MOVIE);
	void AddDscInfo(CString strDscId, CString strDscIp);
	void GetDscInfo(vector<DscAdjustInfo*>** pArrDscInfo);
	virtual BOOL OnInitDialog();
	void Showimage();
	void DrawImage();  
	void DeleteImage(); 
	void LoadData();
	void DrawCenterPointAll();
	void DrawCenterPoint(int nPosX, int nPosY);
	void FindExactPosition(int& nX, int& nY);
	void SavePointData(CString strFileName);
	void LoadPointData(CString strFileName);
	int Round(double dData);
	void GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle);
	void GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY);
	void GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY);
	void CpuMakeMargin(Mat* gMat, int nX, int nY);
	void CpuMoveImage(Mat* gMat, int nX, int nY);
	void CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle);
	void FindPrismPixel(int t_Peak_x,int t_Peak_y,int t_LB_x,int t_LB_y,int t_RB_x, int t_RB_y, int *t_LT_x, int *t_LT_y,int *t_RT_x, int *t_RT_y);
	void affinetransform(Mat img,cv::Point a,cv::Point b, cv::Point c, Point2f srcTri[3],Mat dst);
	void ScalingUp();
	void ScalingDown();
	void MovingUp();
	void MovingDown();
	void AxisClear();

	CListCtrl m_AdjustDscList;
	CComboBox m_ctrlCheckPoint;
	HANDLE	m_hTheadHandle;
	CEdit m_ctrlCenterLine;

	vector<stAdjustInfo> m_adjinfo;
	vector<stPrismInfo> m_prismaxis;

	int m_nMovieState;
	int m_nSelectedNum;
	int m_nSelectPos;
	int m_RdImageViewColor;
	int m_nView;
	int m_nRdDetectColor;
	int m_nWidth, m_nHeight;
	int m_frameIndex;
	int m_nPosX, m_nPosY;
	int m_nThreshold;
	int m_MarginX;
	int m_MarginY;
	int load_prism;
	int m_Scale;
	int m_Move;
	int m_Number;

	Scalar bone_color;
	COLORREF m_clrDetectColor;

	BOOL m_bAutoFind;
	BOOL m_bChangeFlag;
	ESMKzoneMgr m_pAdjustMgr;
	ESMKzoneMgr m_pAdjustMgrRe;
	ESMKzoneImage m_StAdjustImage;

	//170621 wgkim@esmlab.com
	BOOL m_bThreadCloseFlag;

	//170615 hjcho - 선택 영역 지우기
	virtual BOOL PreTranslateMessage(MSG* pMsg);	
	void DeleteSelectedList();
	void DeleteSelectItem(int nIndex);

	int m_nTickness;
};


