#include "stdafx.h"
#include "ESMTemplateMgr.h"
#include "ESMCtrl.h"
#include "ESMFileOperation.h"


CESMTemplateMgr::CESMTemplateMgr(void)
{
	m_bIsOverBoundary		= FALSE;
	m_arrHomography.clear();

	m_dWidth = 3840.;
	m_dHeight = 2160.;
}

CESMTemplateMgr::~CESMTemplateMgr(void)
{

}

void CESMTemplateMgr::SetMovieSIze(double dWidth, double dHeight)
{
	m_dWidth = dWidth;
	m_dHeight = dHeight;
}

BOOL CESMTemplateMgr::isSquareValid(int index)
{
	if (m_arrSquares.size() < 1)
		return FALSE;

	if((m_arrSquares[index].at(0).x == 0 && m_arrSquares[index].at(0).y == 0)
	&& (m_arrSquares[index].at(1).x == 0 && m_arrSquares[index].at(1).y == 0)
	&& (m_arrSquares[index].at(2).x == 0 && m_arrSquares[index].at(2).y == 0)
	&& (m_arrSquares[index].at(3).x == 0 && m_arrSquares[index].at(3).y == 0))
		return FALSE;
	else
		return TRUE;
}

BOOL CESMTemplateMgr::isArrSquareValid()
{
	int count = 0;
	for(int i = 0; i < GetDscCount(); i++)	
	{
		if(!isSquareValid(i))
			count ++;
	}

	if(count >= GetDscCount() - 3)
		return FALSE;
	else
		return TRUE;
}


Mat CESMTemplateMgr::calcHomography(int index)
{
	int nullCheck = 1;

	while(1)
	{
		int circularArrayIndex = index - nullCheck;

		if(circularArrayIndex < 0)
			circularArrayIndex += GetDscCount();

		if(!isSquareValid(circularArrayIndex))
		{
			nullCheck++;		
		}
		else
		{	
			index %= GetDscCount();
			return findHomography(m_arrSquares[circularArrayIndex], m_arrSquares[index]);//RANSAC, 0.001);
		}
	}		
}

void CESMTemplateMgr::calcPoint(Mat& homography, vector<Point2f>& currentPoint)
{
	for(int i = 0; i < currentPoint.size(); i++)
	{
		double tempMat_[3] = {(double)currentPoint[i].x, (double)currentPoint[i].y, 1.};
		Mat tempMat(3, 1, CV_64F, tempMat_);
		Mat resultMat = homography * tempMat;

		currentPoint[i] = Point2f(resultMat.at<double>(0)/resultMat.at<double>(2), 
			resultMat.at<double>(1)/resultMat.at<double>(2));	
	}	
}

void CESMTemplateMgr::setArrHomography()
{
	m_arrHomography.clear();
	m_arrHomography.resize(GetDscCount());

	for(int i = 0; i <= GetDscCount(); i++)
	{
		if(i != GetDscCount())
			m_arrHomography[i] = calcHomography(i);		
		else
			m_arrHomography[0] = calcHomography(i);
	}
}

void CESMTemplateMgr::pushArrSquares(vector<Point2f> inputPoints)
{
	if(inputPoints.size() != 4)
		return;

	m_arrSquares.push_back(inputPoints);
}

BOOL CESMTemplateMgr::IsTemplatePointValid()
{
	if(!isArrSquareValid())
		return FALSE;
	if(m_nViewPoint.x == 0 && m_nViewPoint.y == 0)
		return FALSE;
	else return TRUE;
}

void CESMTemplateMgr::setSquareForReverseMode(vector<Point2f>& square)
{
	float nWidth = m_dWidth;
	float nHeight= m_dHeight;

	square[0] = Point2f(nWidth - square[0].x, nHeight - square[0].y);
	square[1] = Point2f(nWidth - square[1].x, nHeight - square[1].y);
	square[2] = Point2f(nWidth - square[2].x, nHeight - square[2].y);
	square[3] = Point2f(nWidth - square[3].x, nHeight - square[3].y);

	if (square[0].x == nWidth && square[0].y == nHeight)
	{
		square[0].x = 0; 
		square[0].y = 0;
	}
	if (square[1].x == nWidth && square[1].y == nHeight)
	{
		square[1].x = 0; 
		square[1].y = 0;
	}
	if (square[2].x == nWidth && square[2].y == nHeight)
	{	
		square[2].x = 0; 
		square[2].y = 0;
	}
	if (square[3].x == nWidth && square[3].y == nHeight)
	{
		square[3].x = 0; 
		square[3].y = 0;
	}
}

void CESMTemplateMgr::ClearArrSquares()
{
	m_arrSquares.clear();
}

void CESMTemplateMgr::ApplyHomographiesFromArrSquares(vector<vector<Point2f>> squares)
{
	//wgkim 180921
	if(ESMGetValue(ESM_VALUE_AUTOADJUST_POSITION_TRACKING))
		ConvertArrSquaresUsingAdjustData(squares);

	for(int i = 0; i < GetDscCount(); i++)
	{
		if(ESMGetValue(ESM_VALUE_REVERSE_MOVIE))
		{
			setSquareForReverseMode(squares[i]);
		}
		pushArrSquares(squares.at(i));	
	}
	if(isArrSquareValid())
		setArrHomography();
}

void CESMTemplateMgr::setViewPointsInArrTemplatePoint(int dscIdIndex, Point2f viewPoint)
{
	Point2f subPoint = m_arrTemplatePoint[dscIdIndex].centerPointOfRotation - viewPoint;

	for(int i = 0; i < GetDscCount(); i++)
	{
		if(isSquareValid(i))		
			m_arrTemplatePoint[i].viewPoint = m_arrTemplatePoint[i].centerPointOfRotation - subPoint;
	}
}

void CESMTemplateMgr::ClearArrTemplatePoint()
{
	m_arrTemplatePoint.clear();
}

void CESMTemplateMgr::SetArrTemplatePoint()
{
	if(m_arrHomography.empty())
		return;

	TemplatePoint templetePoint;
	int dscIdIndex = GetDscIndexFrom(m_strDscIndex);	

	if(dscIdIndex == -1) return;	

	templetePoint.strDscName = GetDscAt(dscIdIndex)->strDscName;
	templetePoint.strDscIp = GetDscAt(dscIdIndex)->strDscIp;
	templetePoint.centerPointOfRotation = m_nCenterPoint;	

	m_arrTemplatePoint.resize(GetDscCount());	
	m_arrTemplatePoint[dscIdIndex] = templetePoint;	

	for(int i = dscIdIndex+1; i != dscIdIndex; i++)			
	{ 		
		i %= GetDscCount();
		if(isSquareValid(i))
		{	
			vector<Point2f> vectorPoint;
			vectorPoint.push_back(templetePoint.centerPointOfRotation);
			calcPoint(m_arrHomography[i],vectorPoint);

			templetePoint.strDscName = m_ArrDscInfo[i]->strDscName;
			templetePoint.strDscIp = m_ArrDscInfo[i]->strDscIp;
			templetePoint.centerPointOfRotation = vectorPoint[0];

			m_arrTemplatePoint[i] = templetePoint;
			templetePoint = m_arrTemplatePoint[i];
		}
		else
		{			
			m_arrTemplatePoint[i] = templetePoint;
			templetePoint.strDscName = m_ArrDscInfo[i]->strDscName;
			templetePoint.strDscIp = m_ArrDscInfo[i]->strDscIp;
		}		

		if(dscIdIndex == 0 && i == GetDscCount()-1)
			i = -1;
	}
	setViewPointsInArrTemplatePoint(dscIdIndex, m_nViewPoint);
}

void CESMTemplateMgr::SetArrTemplatePoint(CString strDscId, Point2f centerPoint, Point2f viewPoint)
{
	TemplatePoint templetePoint;
	int dscIdIndex = GetDscIndexFrom(strDscId);	

	if(dscIdIndex == -1) return;

	templetePoint.strDscName = GetDscAt(dscIdIndex)->strDscName;
	templetePoint.strDscIp = GetDscAt(dscIdIndex)->strDscIp;
	templetePoint.centerPointOfRotation = centerPoint;	

	m_arrTemplatePoint.resize(GetDscCount());	
	m_arrTemplatePoint[dscIdIndex] = templetePoint;	

	for(int i = dscIdIndex+1; i != dscIdIndex; i++)
	{
		i %= GetDscCount();
		if(isSquareValid(i))
		{
			vector<Point2f> vectorPoint;
			vectorPoint.push_back(templetePoint.centerPointOfRotation);
			calcPoint(m_arrHomography[i],vectorPoint);

			templetePoint.strDscName = m_ArrDscInfo[i]->strDscName;
			templetePoint.strDscIp = m_ArrDscInfo[i]->strDscIp;
			templetePoint.centerPointOfRotation = vectorPoint[0];

			m_arrTemplatePoint[i] = templetePoint;
			templetePoint = m_arrTemplatePoint[i];
		}
		else
		{
			m_arrTemplatePoint[i] = templetePoint;
			templetePoint.strDscName = m_ArrDscInfo[i]->strDscName;
			templetePoint.strDscIp = m_ArrDscInfo[i]->strDscIp;
		}

		if(dscIdIndex == 0 && i == GetDscCount()-1)
			i = -1;
	}
	setViewPointsInArrTemplatePoint(dscIdIndex, viewPoint);	
}

CESMTemplateMgr::TemplatePoint CESMTemplateMgr::GetTemplatePoint(int index)
{
	return m_arrTemplatePoint[index];
}

void CESMTemplateMgr::SetViewPoint(Point2f viewPoint)
{
	m_nViewPoint = viewPoint;	
}

void CESMTemplateMgr::SetCenterPoint(Point2f centerPoint)
{
	m_nCenterPoint = centerPoint;	
}

void CESMTemplateMgr::SetDscIndex(CString dscIndex)
{
	m_strDscIndex = dscIndex;
}


void CESMTemplateMgr::LoadSquarePointData(CString strFileName)
{
	CESMIni ini;
	if(!ini.SetIniFilename (strFileName))
		return;

	int nHeight = 0, nWidth = 0;
	nHeight = ini.GetInt(_T("MovieSize"), _T("HEIGHT"), 0);
	nWidth	= ini.GetInt(_T("MovieSize"), _T("WIDTH"), 0);

	ClearArrSquares();

	vector<vector<Point2f>> squares;
	CString strData;	
	for(int i = 0; i < GetDscCount(); i++)
	{
		CString strSelect = GetDscAt(i)->strDscName;

		vector<Point2f> square;
		Point2f pt1, pt2, pt3, pt4;

		strData = ini.GetString(strSelect, _T("T_LBX"));
		pt1.x = _ttoi(strData);
		strData = ini.GetString(strSelect, _T("T_LBY"));
		pt1.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("T_RBX"));
		pt2.x = _ttoi(strData);
		strData = ini.GetString(strSelect, _T("T_RBY"));
		pt2.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("T_PeakX"));
		pt3.x = _ttoi(strData);
		strData = ini.GetString(strSelect, _T("T_PeakY"));
		pt3.y = _ttoi(strData);

		strData = ini.GetString(strSelect, _T("B_PeakX"));
		pt4.x = _ttoi(strData);
		strData = ini.GetString(strSelect, _T("B_PeakY"));
		pt4.y = _ttoi(strData);

		square.push_back(pt1);
		square.push_back(pt2);
		square.push_back(pt3);
		square.push_back(pt4);

		squares.push_back(square);
	}

	ApplyHomographiesFromArrSquares(squares);
}

BOOL CESMTemplateMgr::IsArrTemplatePointValid()
{
	if(m_arrTemplatePoint.empty()) 
		return FALSE;
	else 
		return TRUE;
}

void CESMTemplateMgr::AddDscInfo(CString strDscId, CString strDscIp,BOOL bReverse/* = FALSE*/)
{
	DscAdjustInfo* pDscAdjustData;
	pDscAdjustData = new DscAdjustInfo;
	pDscAdjustData->strDscName = strDscId;
	pDscAdjustData->strDscIp = strDscIp;
	pDscAdjustData->bAdjust = FALSE;
	pDscAdjustData->bRotate = FALSE;
	pDscAdjustData->bMove = FALSE;
	pDscAdjustData->bImgCut = FALSE;
	pDscAdjustData->bReverse = bReverse;
	m_ArrDscInfo.push_back(pDscAdjustData);
}

void CESMTemplateMgr::GetDscInfo(vector<DscAdjustInfo*>** pArrDscInfo)
{
	*pArrDscInfo = &m_ArrDscInfo;
}

int CESMTemplateMgr::GetDscCount()
{
	return m_ArrDscInfo.size();
}

void CESMTemplateMgr::DscClear()
{
	DscAdjustInfo* pDscAdjustData;
	for( int i =0 ;i < m_ArrDscInfo.size(); i++)
	{
		pDscAdjustData = m_ArrDscInfo.at(i);
		if( pDscAdjustData->pBmpBits != NULL)
		{
			delete[] pDscAdjustData->pBmpBits;
			pDscAdjustData->pBmpBits = NULL;
		}
		if( pDscAdjustData->pGrayBmpBits != NULL)
		{
			delete[] pDscAdjustData->pGrayBmpBits;
			pDscAdjustData->pGrayBmpBits = NULL;
		}

		delete pDscAdjustData;
		pDscAdjustData = NULL;
	}
	m_ArrDscInfo.clear();
}

DscAdjustInfo* CESMTemplateMgr::GetDscAt(int nIndex)
{
	if(m_ArrDscInfo.size() > nIndex)
	{
		return m_ArrDscInfo.at(nIndex);
	}
	return NULL;
}

int CESMTemplateMgr::GetDscIndexFrom(CString strDscId)
{
	int DscID = -1;
	for(int i = 0; i < GetDscCount(); i++)
	{	
		if(!strDscId.Compare(m_ArrDscInfo[i]->strDscName))
		{
			DscID = i;
			break;
		}
	}
	return DscID;
}


BOOL CESMTemplateMgr::GetSearchDetectPoint(DscAdjustInfo* pAdjustInfo, int nThreshold, CSize minDetectSize, CSize maxDetectSize, int nPointGapMin, COLORREF clrDetectColor)
{
	if( pAdjustInfo == NULL || pAdjustInfo->nWidht == 0 ||  pAdjustInfo->nHeight == 0)
		return FALSE;

	int nChannel = 3;
	IplImage *pImage, *pGray;
	pImage = cvCreateImage(cvSize(pAdjustInfo->nWidht, pAdjustInfo->nHeight), IPL_DEPTH_8U, nChannel);
	memcpy(pImage->imageData, pAdjustInfo->pBmpBits, pAdjustInfo->nWidht * pAdjustInfo->nHeight * nChannel);
	if(!pImage)
		return FALSE;

	pGray = cvCreateImage(cvGetSize(pImage), IPL_DEPTH_8U, 1);
	if(!pGray)
	{
		cvReleaseImage(&pImage);
		cvReleaseImage(&pGray);
		return FALSE;
	}

	cvCvtColor(pImage, pGray, cv::COLOR_RGB2GRAY);
	cvThreshold(pGray, pGray, nThreshold, 255 , CV_THRESH_BINARY);

	vector<CRect> vecDetectAreas;
	
	SearchDetectPoint(pGray, minDetectSize, maxDetectSize, &vecDetectAreas, clrDetectColor);

	IplImage* pGrayImg = cvCreateImage(cvSize(pGray->width, pGray->height), IPL_DEPTH_8U, nChannel);
	cvCvtColor(pGray, pGrayImg, CV_GRAY2RGB);
	pAdjustInfo->pGrayBmpBits = new BYTE[pGrayImg->height * pGrayImg->widthStep];

	if( vecDetectAreas.size() < 3)
	{
		memcpy(pAdjustInfo->pGrayBmpBits, pGrayImg->imageData, pGrayImg->height * pGrayImg->widthStep);
		cvReleaseImage(&pImage);
		cvReleaseImage(&pGray);
		cvReleaseImage(&pGrayImg);
		return FALSE;
	}

	if(vecDetectAreas[1].Width() > 1 && vecDetectAreas[1].Height() > 1)
	{
		pAdjustInfo->recMiddle = vecDetectAreas[1];
		pAdjustInfo->MiddlePos = vecDetectAreas[1].CenterPoint();
	}

	if(vecDetectAreas[0].Width() > 1 && vecDetectAreas[0].Height() > 1)
	{
		if(pAdjustInfo->MiddlePos.y - vecDetectAreas[0].CenterPoint().y > nPointGapMin)
		{
			pAdjustInfo->recHigh = vecDetectAreas[0];
			pAdjustInfo->HighPos = vecDetectAreas[0].CenterPoint();
		}
		else
			vecDetectAreas[0].SetRect(0,0,0,0);
	}

	if(vecDetectAreas[2].Width() > 1 && vecDetectAreas[2].Height() > 1)
	{
		if(vecDetectAreas[2].CenterPoint().y - pAdjustInfo->MiddlePos.y > nPointGapMin)
		{
			pAdjustInfo->recLow = vecDetectAreas[2];
			pAdjustInfo->LowPos = vecDetectAreas[2].CenterPoint();
		}
		else
			vecDetectAreas[2].SetRect(0,0,0,0);
	}

	for(int i =0 ;i < vecDetectAreas.size(); i++)
	{
		if(vecDetectAreas[i].CenterPoint().x != 0)
			cvRectangle(pGrayImg, cvPoint(vecDetectAreas[i].left, vecDetectAreas[i].top), cvPoint(vecDetectAreas[i].right, vecDetectAreas[i].bottom), cvScalar(0, 0, 255), 2, 8, 0);
	}

	if( vecDetectAreas.size() >= 3)
	{
		if((vecDetectAreas[0].Width() > 1 && vecDetectAreas[0].Height() > 1) && (vecDetectAreas[2].Width() > 1 && vecDetectAreas[2].Height() > 1))
			cvLine(pGrayImg, cvPoint(vecDetectAreas[0].CenterPoint().x, vecDetectAreas[0].CenterPoint().y), cvPoint(vecDetectAreas[2].CenterPoint().x, vecDetectAreas[2].CenterPoint().y), cvScalar(0, 0, 255), 2, 8, 0);
	}
	memcpy(pAdjustInfo->pGrayBmpBits, pGrayImg->imageData, pGrayImg->height * pGrayImg->widthStep);

	cvReleaseImage(&pImage);
	cvReleaseImage(&pGray);
	cvReleaseImage(&pGrayImg);
	

	return TRUE;
}

// 2014-01-07 kcd 
// Distance 임시 함수
double CESMTemplateMgr::GetDistanceData(CString strDSCId)
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\DSCDistance.csv"), ESMGetPath(ESM_PATH_SETUP));
	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		return FALSE;
	}

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());
	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;
	CString strTpValue1, strTpValue2;
	double nDistance = 0;

	while (iPos  < iLength )
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;
		AfxExtractSubString(strTpValue1, sLine, 0, ',');
		AfxExtractSubString(strTpValue2, sLine, 1, ',');
		if( strTpValue1 == strDSCId)
		{
			nDistance = _ttof(strTpValue2);
			break;
		}
	}
	return nDistance;
}

int CESMTemplateMgr::GetZoomData(CString strDSCId)
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\DSCZoom.csv"), ESMGetPath(ESM_PATH_SETUP));
	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		return FALSE;
	}

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());
	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;
	CString strTpValue1, strTpValue2;
	double nDistance = 0;
	while (iPos  < iLength )
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;
		AfxExtractSubString(strTpValue1, sLine, 0, ',');
		AfxExtractSubString(strTpValue2, sLine, 1, ',');
		if( strTpValue1 == strDSCId)
		{
			nDistance = _ttoi(strTpValue2);
			break;
		}
	}
	return nDistance;
}

BOOL CESMTemplateMgr::CalcAdjustData(DscAdjustInfo* pAdjustInfo, int nTargetLenth, int nZoom)
{
	//회전
	pAdjustInfo->dAngle = atan2((double)(pAdjustInfo->LowPos.y - pAdjustInfo->HighPos.y), 
		(double)(pAdjustInfo->LowPos.x - pAdjustInfo->HighPos.x)) * (180/3.141592);
	
	//Size
	double dTargetDistance = 0;
	double dTargetLength = 0;

	if( pAdjustInfo->dDistance == 0.0)
		dTargetDistance = GetDistanceData(pAdjustInfo->strDscName);
	else
		dTargetDistance = pAdjustInfo->dDistance;
	
	int nPixelDefaultSize = 0;
	if( pAdjustInfo->nWidht == 1920)
		nPixelDefaultSize = DEFAULT_PIXEL1920_DISTANCE;
	else if( pAdjustInfo->nWidht == 5472)
		nPixelDefaultSize = DEFAULT_PIXEL5472_DISTANCE;
	else if( pAdjustInfo->nWidht == 2200 && pAdjustInfo->nHeight == 1238)
		nPixelDefaultSize = DEFAULT_PIXEL5472_3080_DISTANCE;
	else if( pAdjustInfo->nWidht == 3840 && pAdjustInfo->nHeight == 2160)
		nPixelDefaultSize = DEFAULT_PIXEL3840_2160_DISTANCE;
	else
		nPixelDefaultSize = DEFAULT_PIXEL1920_DISTANCE;

	dTargetLength = (double)nTargetLenth * nPixelDefaultSize  / DEFAULT_TARGETLENGTH;
	if( dTargetDistance != 0.0)
		dTargetLength = dTargetLength / dTargetDistance;
	else
		dTargetLength =1.0;

	if(nZoom == 0)
		nZoom = GetZoomData(pAdjustInfo->strDscName);

	dTargetLength = dTargetLength * nZoom /DEFAULT_CAMERAZOOM;

	int nHeight = pAdjustInfo->LowPos.y - pAdjustInfo->HighPos.y;
	int nWidth = abs(pAdjustInfo->LowPos.x - pAdjustInfo->HighPos.x);
	if( nHeight != 0 || nWidth != 0 )
	{
		double dTpResize = 0.0;
		if( nWidth != 0)
			dTpResize = sqrt(double(nHeight * nHeight + nWidth * nWidth));
		else
			dTpResize = nHeight;

		pAdjustInfo->dScale = dTargetLength / dTpResize ;

		//CenterPoint
		pAdjustInfo->dRotateX = pAdjustInfo->dAdjustX = pAdjustInfo->MiddlePos.x;
		pAdjustInfo->dRotateY = pAdjustInfo->dAdjustY = pAdjustInfo->MiddlePos.y;
		pAdjustInfo->dAngle = pAdjustInfo->dAngle * -1;
		pAdjustInfo->dDistance = dTargetDistance;
		return TRUE;
	}
	return FALSE;
}

BOOL CESMTemplateMgr::SearchDetectPoint(IplImage* pNewGray, CSize MinSize, CSize MaxSize, vector<CRect>* vecDetectAreas, COLORREF clrDetectColor)
{
	CPoint pointCenter = CPoint(pNewGray->width / 2, pNewGray->height / 2);
	vector<CRect> vecCenters;
	BOOL bStart = FALSE, bEnd = FALSE;
	int nStartPos = 0, nEndPos = 0;
	int nStartCenterArea = (pNewGray->height / 2) - ((pNewGray->height / 5) / 2);
	int nEndCenterArea = (pNewGray->height / 2) + ((pNewGray->height / 5) / 2);
	int nPosYGap = (MaxSize.cy / 2) + 10;

	int nFindColor;
	if(clrDetectColor == COLOR_WHITE)
		nFindColor = 0;
	else
		nFindColor = 255;

	for(int nFindY = 0; nFindY < pNewGray->height; nFindY++)
	{
		if((unsigned char)pNewGray->imageData[(nFindY * pNewGray->widthStep) + pointCenter.x] == nFindColor)
		{
			if(!bStart)	bStart = TRUE;
			else if(bEnd)
			{
				nEndPos = nFindY - 1;
				bEnd = FALSE;
				bStart = FALSE;
				if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
				{
					CRect recDetect(pointCenter.x, nStartPos, pointCenter.x, nEndPos);
					vecCenters.push_back(recDetect);
				}
			}
		}
		else
		{
			if(bStart && !bEnd)
			{
				nStartPos = nFindY;
				bEnd = TRUE;
			}
		}
	}
	if(vecCenters.size() < 3)
	{
		vecCenters.clear();
		bStart = FALSE;
		bEnd = FALSE;
		nStartPos = 0;
		nEndPos = 0;
		for(int nFindX = pointCenter.x - 1; nFindX > (pointCenter.x - MaxSize.cx); nFindX--)
		{
			vecCenters.clear();
			for(int nFindY = 0; nFindY < pNewGray->height; nFindY++)
			{
				if((unsigned char)pNewGray->imageData[(nFindY * pNewGray->widthStep) + nFindX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nFindY - 1;
						bEnd = FALSE;
						bStart = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							CRect recDetect(nFindX, nStartPos, nFindX, nEndPos);
							vecCenters.push_back(recDetect);
						}
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nFindY;
						bEnd = TRUE;
					}
				}
			}
			if(vecCenters.size() >= 3)
				break;
		}
	}
	if(vecCenters.size() < 3)
	{
		vecCenters.clear();
		bStart = FALSE;
		bEnd = FALSE;
		nStartPos = 0;
		nEndPos = 0;
		for(int nFindX = pointCenter.x + 1; nFindX < (pointCenter.x + MaxSize.cx); nFindX++)
		{
			vecCenters.clear();
			for(int nFindY = 0; nFindY < pNewGray->height; nFindY++)
			{
				if((unsigned char)pNewGray->imageData[(nFindY * pNewGray->widthStep) + nFindX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nFindY - 1;
						bEnd = FALSE;
						bStart = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							CRect recDetect(nFindX, nStartPos, nFindX, nEndPos);
							vecCenters.push_back(recDetect);
						}
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nFindY;
						bEnd = TRUE;
					}
				}
			}
			if(vecCenters.size() >= 3)
				break;
		}
	}
	if(vecCenters.size() < 3)
		return FALSE;

	//Center에 가장 가까운 영역을 찾는다. 
	CRect recCenter = vecCenters[0];
	int nCenterMargin = pointCenter.y - recCenter.CenterPoint().y;
	if(nCenterMargin < 0) nCenterMargin *= -1;
	for(int nIdx = 1; nIdx < vecCenters.size(); nIdx++)
	{
		int nMargin = pointCenter.y - vecCenters[nIdx].CenterPoint().y;
		if(nMargin < 0) nMargin *= -1;
		if(nMargin < nCenterMargin)
		{
			nCenterMargin = nMargin;
			recCenter =  vecCenters[nIdx];
		}
	}

	if(recCenter.CenterPoint().y < nStartCenterArea || recCenter.CenterPoint().y > nEndCenterArea)
		return FALSE;

	//First와 Third 영역을 찾는다. 
	CRect recFirst = recCenter;
	CRect recThird = recCenter;
	int nFirstMargin = 0, nThirdMargin = 0;
	for(int nIdx = 0; nIdx < vecCenters.size(); nIdx++)
	{
		if(recCenter == vecCenters[nIdx])
			continue;
		int nMargin = recCenter.CenterPoint().y - vecCenters[nIdx].CenterPoint().y;
		if(nMargin < 0)
		{
			nMargin *= -1;
			if(nThirdMargin == 0)
			{
				nThirdMargin = nMargin;
				recThird = vecCenters[nIdx];
			}
			else if(nMargin < nThirdMargin)
			{
				nThirdMargin = nMargin;
				recThird = vecCenters[nIdx];
			}
		}	
		else
		{
			if(nFirstMargin == 0)
			{
				nFirstMargin = nMargin;
				recFirst = vecCenters[nIdx];
			}
			else if(nMargin < nFirstMargin)
			{
				nFirstMargin = nMargin;
				recFirst = vecCenters[nIdx];
			}
		}
	}

	vecCenters.clear();
	vecCenters.push_back(recFirst);
	vecCenters.push_back(recCenter);
	vecCenters.push_back(recThird);

	
	for(int nAreaIdx = 0; nAreaIdx < vecCenters.size(); nAreaIdx++)
	{
		//Left 시작 지점 찾는다.
		int nLeft = vecCenters[nAreaIdx].left;
		int nTop = vecCenters[nAreaIdx].top;
		int nRight = vecCenters[nAreaIdx].right;
		int nBottom = vecCenters[nAreaIdx].bottom;
		BOOL bDetectStop = FALSE;
		CRect recDetectArea = vecCenters[nAreaIdx];
		vector<int> vecHeightAvg;
		vector<CRect> vecAreaList;
		vecAreaList.push_back(vecCenters[nAreaIdx]);
		vecHeightAvg.push_back(nBottom - nTop);
		for(int nPosX = vecCenters[nAreaIdx].CenterPoint().x - 1; nPosX > vecCenters[nAreaIdx].CenterPoint().x - MaxSize.cx; nPosX--)
		{
			if(nPosX > pNewGray->width) break;
			int nPosY = vecCenters[nAreaIdx].CenterPoint().y - nPosYGap;
			if(nPosY < 0) nPosY = 0;
			if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] != nFindColor)
				break;

			bStart = FALSE;
			bEnd = FALSE;
			nStartPos = 0;
			nEndPos = 0;

			int nHeightAvg = 0, nHeightSum = 0, nAvgIdxCnt = vecHeightAvg.size();
			for(int nAvgIdx = 0; nAvgIdx < nAvgIdxCnt; nAvgIdx++)
				nHeightSum += vecHeightAvg[nAvgIdx];
			nHeightAvg = nHeightSum / nAvgIdxCnt;

			for(; nPosY < vecCenters[nAreaIdx].CenterPoint().y + nPosYGap; nPosY++)
			{
				if(nPosY > pNewGray->height) break;
				if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nPosY - 1;
						bStart = FALSE;
						bEnd = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							int nHeight = nEndPos - nStartPos;
							if(nHeight >= nHeightAvg - 10 &&  nHeight <= nHeightAvg + 10)
							{
								nLeft = nPosX;
								if(nStartPos > nTop)	nTop = nStartPos;
								if(nEndPos < nBottom)	nBottom = nEndPos;
								recDetectArea.SetRect(nLeft, nTop, nRight, nBottom);
								vecHeightAvg.push_back(nHeight);
								vecAreaList.push_back(CRect(nPosX, nStartPos, nPosX, nEndPos));
							}
						}
						else
							bDetectStop = TRUE;
						break;
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nPosY;
						bEnd = TRUE;
					}
				}
			}
			if(bDetectStop)
				break;
		}
		//Right 마지막 지점 찾는다.
		bDetectStop = FALSE;
		for(int nPosX = vecCenters[nAreaIdx].CenterPoint().x + 1; nPosX < vecCenters[nAreaIdx].CenterPoint().x + MaxSize.cx; nPosX++)
		{
			if(nPosX > pNewGray->width) break;
			int nPosY = vecCenters[nAreaIdx].CenterPoint().y - nPosYGap;
			if(nPosY < 0) nPosY = 0;
			if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] != nFindColor)
				break;
			bStart = FALSE;
			bEnd = FALSE;
			nStartPos = 0;
			nEndPos = 0;

			int nHeightAvg = 0, nHeightSum = 0, nAvgIdxCnt = vecHeightAvg.size();
			for(int nAvgIdx = 0; nAvgIdx < nAvgIdxCnt; nAvgIdx++)
				nHeightSum += vecHeightAvg[nAvgIdx];
			nHeightAvg = nHeightSum / nAvgIdxCnt;

			for(; nPosY < vecCenters[nAreaIdx].CenterPoint().y + nPosYGap; nPosY++)
			{
				if(nPosY > pNewGray->height) break;
				if((unsigned char)pNewGray->imageData[(nPosY * pNewGray->widthStep) + nPosX] == nFindColor)
				{
					if(!bStart)	bStart = TRUE;
					else if(bEnd)
					{
						nEndPos = nPosY - 1;
						bStart = FALSE;
						bEnd = FALSE;
						if(nEndPos-nStartPos >= MinSize.cy && nEndPos-nStartPos <= MaxSize.cy)
						{
							int nHeight = nEndPos - nStartPos;
							if(nHeight >= nHeightAvg - 10 &&  nHeight <= nHeightAvg + 10)
							{
								nRight = nPosX;
								if(nStartPos > nTop)	nTop = nStartPos;
								if(nEndPos < nBottom)	nBottom = nEndPos;
								recDetectArea.SetRect(nLeft, nTop, nRight, nBottom);
								vecHeightAvg.push_back(nHeight);
								vecAreaList.push_back(CRect(nPosX, nStartPos, nPosX, nEndPos));
							}
						}
						else
							bDetectStop = TRUE;
						break;
					}
				}
				else
				{
					if(bStart && !bEnd)
					{
						nStartPos = nPosY;
						bEnd = TRUE;
					}
				}
			}
			if(bDetectStop)
				break;
		}
		if(recDetectArea.Width() < 5)
		{
			recDetectArea.SetRect(0, 0, 0, 0);
		}
		else
		{
			int nCenterX = recDetectArea.CenterPoint().x;
			for(int nXIdx = 0; nXIdx < vecAreaList.size(); nXIdx++)
			{
				if(vecAreaList[nXIdx].left == nCenterX)
				{
					nLeft = recDetectArea.left;
					nTop = vecAreaList[nXIdx].top;
					nRight = recDetectArea.right;
					nBottom = vecAreaList[nXIdx].bottom;
					recDetectArea.SetRect(nLeft, nTop, nRight, nBottom);
					break;
				}
			}
		}
		vecDetectAreas->push_back(recDetectArea);
	}
	return TRUE;
}

BOOL CESMTemplateMgr::SearchPoint(IplImage*	pNewGray, vector<CPoint>* arrBeginPoint, vector<CPoint>* arrPointRange, vector<CPoint>* pArrPoint)
{
	CPoint TpBeginP;
	CPoint TpPoints;
	if( pNewGray->width < arrBeginPoint->at(2).x + arrPointRange->at(2).x)
		return FALSE;
	if( pNewGray->height < arrBeginPoint->at(2).y + arrPointRange->at(2).y)
		return FALSE;

	for( int i = 0; i< arrBeginPoint->size(); i++)
	{
		CPoint CenterPoint;
		CPoint tpPoint;
		vector<CPoint> arrPoint;
		int x1 = 0;
		int nImageWidth	= pNewGray->width;
		int nImageHeight = pNewGray->height;
		TpPoints = arrPointRange->at(i);

		for(int y1 = arrBeginPoint->at(i).y ; y1 < arrBeginPoint->at(i).y + TpPoints.y ; y1++)
		{
			for(x1 = arrBeginPoint->at(i).x ; x1 < arrBeginPoint->at(i).x + TpPoints.x ; x1++)
			{
				if((unsigned char)pNewGray->imageData[y1*pNewGray->widthStep + x1] < 10)// 흑을 만난다면 Continue;
				{
					tpPoint.x = x1;
					tpPoint.y = y1;
					arrPoint.push_back(tpPoint);
					break;
				}
			}
			if( x1 == arrBeginPoint->at(i).x + TpPoints.x)
				continue;

			//Detect Right Line
			for(x1 = arrBeginPoint->at(i).x + TpPoints.x ; x1 > arrBeginPoint->at(i).x ; x1--)
			{
				if((unsigned char)pNewGray->imageData[y1*pNewGray->widthStep + x1] < 10)// 흑색을 만난다면 Continue;
				{
					tpPoint.x = x1;
					tpPoint.y = y1;
					arrPoint.push_back(tpPoint);
					break;
				}
			}
		}
		if( arrPoint.size() < 1)
			return FALSE;
		int dMinX = arrPoint.at(0).x, dMinY = arrPoint.at(0).y, dMaxX = arrPoint.at(0).x, dMaxY = arrPoint.at(0).y;
		for(int nIndex =0 ;nIndex < arrPoint.size() ; nIndex++)
		{
			if( arrPoint.at(nIndex).x < dMinX)
				dMinX = arrPoint.at(nIndex).x;
			else if( arrPoint.at(nIndex).y < dMinY)
				dMinY = arrPoint.at(nIndex).y;
			else if( arrPoint.at(nIndex).x > dMaxX)
				dMaxX = arrPoint.at(nIndex).x;
			else if( arrPoint.at(nIndex).x > dMaxY)
				dMaxY = arrPoint.at(nIndex).y;
		}
		TpBeginP.x = (dMinX + dMaxX) / 2;
		TpBeginP.y = (dMinY + dMaxY) / 2;
		pArrPoint->push_back(TpBeginP);

	}
	return TRUE;
}

void CESMTemplateMgr::GetResolution(int &nWidth, int &nHeight)
{
	DscAdjustInfo* pAdjustInfo = NULL;
	CESMFileOperation fo;
	for(int i =0; i< GetDscCount(); i++)
	{
		pAdjustInfo = GetDscAt(i);

		FFmpegManager FFmpegMgr(FALSE);
		CString strFolder, strDscId, strDscIp;
		strDscId = pAdjustInfo->strDscName;
		strDscIp = pAdjustInfo->strDscIp;
		int nFIdx = ESMGetFrameIndex(0);
		strFolder = ESMGetMoviePath(strDscId, nFIdx);


		if( strFolder == _T(""))
			continue;

		if(!fo.IsFileExist(strFolder))
			continue;

		int nRunningTime = 0, nFrameCount = 0, nChannel = 3, nFrameRate = 0;
		FFmpegMgr.GetMovieInfo(strFolder, &nRunningTime, &nFrameCount, &nWidth, &nHeight, &nChannel, &nFrameRate);
		if( nWidth != 0)
			break;
	}
}

void CESMTemplateMgr::AddAdjInfo(stAdjustInfo adjustInfo)
{
	m_arrAdjInfo.push_back(adjustInfo);
}

stAdjustInfo CESMTemplateMgr::GetAdjInfoAt(int index)
{
	return m_arrAdjInfo.at(index);
}

void CESMTemplateMgr::ClearAdjInfo()
{
	m_arrAdjInfo.clear();
}

Mat CESMTemplateMgr::ApplyAdjustImageUsingCPU(int nSelectedItem, int marginX, int marginY)
{

	Mat src;
	DscAdjustInfo *pAdjustInfo = GetDscAt(nSelectedItem);
	if( pAdjustInfo != NULL)
	{
		if(pAdjustInfo->bAdjust )
		{
			//pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3, NULL , 0);	
		}
		else if(pAdjustInfo->nHeight == 0 || pAdjustInfo->nWidht == 0)
		{
			return Mat();
		}

		src.create(pAdjustInfo->nHeight, pAdjustInfo->nWidht, CV_8UC3);
		memcpy(src.data, pAdjustInfo->pBmpBits, pAdjustInfo->nHeight * pAdjustInfo->nWidht * 3);

		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = 1;
		stAdjustInfo AdjustData = m_arrAdjInfo.at(nSelectedItem);

		if(!pAdjustInfo->bRotate)
		{
			// Rotate 구현					
			nRotateX = Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
			nRotateY = Round(AdjustData.AdjptRotate.y  * dRatio );

			//IplImage *Iimage = new IplImage(src);
			Mat Iimage(src);
			CpuRotateImage(Iimage, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
			//delete Iimage;
		}

		if(!pAdjustInfo->bMove)
		{
			//-- MOVE EVENT
			int nMoveX = 0, nMoveY = 0;
			nMoveX = Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
			nMoveY = Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
			CpuMoveImage(&src, nMoveX,  nMoveY);
		}

		if(!pAdjustInfo->bImgCut)
		{
			//-- IMAGE CUT EVENT
			int nMarginX = marginX* dRatio;
			int nMarginY = marginY * dRatio;
			if ( nMarginX < 0 || nMarginX >= pAdjustInfo->nWidht/2 || nMarginY < 0 || nMarginY >= pAdjustInfo->nHeight/2  )
			{
				TRACE(_T("Image Adjust Error 2\n"));
				return Mat();
			}
			//CpuMakeMargin(&src, nMarginX, nMarginY);
			double dbMarginScale = 1 / ( (double)( pAdjustInfo->nWidht - nMarginX * 2) /(double) pAdjustInfo->nWidht  );
			Mat  pMatResize;

			resize(src, pMatResize, cv::Size(pAdjustInfo->nWidht*dbMarginScale ,pAdjustInfo->nHeight * dbMarginScale ), 0.0, 0.0 ,CV_INTER_NN);
			int nLeft = (int)((pAdjustInfo->nWidht*dbMarginScale - pAdjustInfo->nWidht)/2);
			int nTop = (int)((pAdjustInfo->nHeight*dbMarginScale - pAdjustInfo->nHeight)/2);
			Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, pAdjustInfo->nWidht, pAdjustInfo->nHeight));
			pMatCut.copyTo(src);
			pMatResize = NULL;
		}						
		int size = src.total() * src.elemSize();
		memcpy(pAdjustInfo->pBmpBits, src.data, size);

		pAdjustInfo->bAdjust = TRUE;
		pAdjustInfo->bRotate = TRUE;
		pAdjustInfo->bMove = TRUE;
		pAdjustInfo->bImgCut = TRUE;	
	}
	return src;
}

int CESMTemplateMgr::Round(double dData)
{
	int nResult = 0;
	if( dData == 0)
		nResult = 0;
	else if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);

	return nResult;
}

void CESMTemplateMgr::GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle)
{
	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;

	Mat rot_mat(cv::Size(2, 3), CV_32FC1);
	cv::Point rot_center( nCenterX, nCenterY);

	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust = -1 * (dAngle + 90);

	rot_mat = getRotationMatrix2D(rot_center, dbAngleAdjust, dScale);

	cuda::GpuMat d_rotate;	
	cuda::warpAffine(*gMat, d_rotate, rot_mat, cv::Size(gMat->cols, gMat->rows), cv::INTER_CUBIC);
	d_rotate.copyTo(*gMat);
}

void CESMTemplateMgr::GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;

	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

void CESMTemplateMgr::GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

void CESMTemplateMgr::CpuMakeMargin(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;

	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

void CESMTemplateMgr::CpuMoveImage(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

void CESMTemplateMgr::CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle)
{
	Mat Iimage2;// = cvCreateImage(cvGetSize(Iimage), IPL_DEPTH_8U, 3);

	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;

	CvMat *rot_mat = cvCreateMat( 2, 3, CV_32FC1);
	CvPoint2D32f rot_center = cvPoint2D32f( nCenterX, nCenterY);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust = -1 * (dAngle + 90);

	cv2DRotationMatrix( rot_center, dbAngleAdjust, dScale, rot_mat);
	cvWarpAffine(&IplImage(Iimage), &IplImage(Iimage), rot_mat,cv::INTER_LINEAR+cv::WARP_FILL_OUTLIERS);	//d_rotate.copyTo(*gMat);
	cvReleaseMat(&rot_mat);
}

void CESMTemplateMgr::affinetransform(Mat img,cv::Point a,cv::Point b, cv::Point c, Point2f srcTri[3],Mat dst)
{
	Mat warp_mat(2,3,CV_32FC1);
	Point2f dstTri[3];
	dstTri[0] = a;	dstTri[1] = b;	dstTri[2] = c;

	warp_mat = getAffineTransform(srcTri,dstTri);
	warpAffine(img,dst,warp_mat,dst.size());
}

void CESMTemplateMgr::TemplateModifyForTargetDisappearingFromScreen(vector<TEMPLATE_STRUCT>* vecVMCCTemplate)
{	
	m_vecViewDirect.clear();

	int nInputZoomValue = ESMGetValue(ESM_VALUE_TEMPLATE_TARGETOUT_PERCENT);
	double dAddZoomRatioPercent = (100+nInputZoomValue)/100.0;

	int nWidth	= m_dWidth;
	int nHeight	= m_dHeight;
	int nZoomRatio	= 100;

	int nLowDSC		= GetDscCount();
	int nHighDSC	= 0;

	for(int i = 0; i < vecVMCCTemplate->size(); i++)
	{			
		for(int k = 0; k < vecVMCCTemplate->at(i).nVMCC_Count; k++)
		{
			if( nZoomRatio < vecVMCCTemplate->at(i).nVMCC_Data[k].nZoom )
				nZoomRatio = vecVMCCTemplate->at(i).nVMCC_Data[k].nZoom;
		}		
	}
	nZoomRatio *= dAddZoomRatioPercent;

	for(int i = 0; i < GetDscCount(); i++)
	{
		TemplatePoint tpTemplatePoint = GetTemplatePoint(i);

		Point2f pPoint = tpTemplatePoint.viewPoint;
		if(pPoint .x == 0 && pPoint .y == 0)
			continue;

		Rect2d rcAppliedZoomRect;					
		rcAppliedZoomRect.width  = nWidth /(nZoomRatio/100.);
		rcAppliedZoomRect.height = nHeight/(nZoomRatio/100.);
		rcAppliedZoomRect.x = pPoint.x - (rcAppliedZoomRect.width  / 2);
		rcAppliedZoomRect.y = pPoint.y - (rcAppliedZoomRect.height / 2);

		if(!(rcAppliedZoomRect.x < 0 ||
			rcAppliedZoomRect.y < 0 ||
			rcAppliedZoomRect.x + rcAppliedZoomRect.width > 3840 ||
			rcAppliedZoomRect.y + rcAppliedZoomRect.height > 2160))
		{
			nLowDSC = i;
			break;
		}
	}

	for(int i = GetDscCount()-1; i >= 0; i--)
	{
		TemplatePoint tpTemplatePoint = GetTemplatePoint(i);

		Point2f pPoint = tpTemplatePoint.viewPoint;
		if(pPoint .x == 0 && pPoint .y == 0)
			continue;

		Rect2d rcAppliedZoomRect;					
		rcAppliedZoomRect.width  = nWidth /(nZoomRatio/100.);
		rcAppliedZoomRect.height = nHeight/(nZoomRatio/100.);
		rcAppliedZoomRect.x = pPoint.x - (rcAppliedZoomRect.width  / 2);
		rcAppliedZoomRect.y = pPoint.y - (rcAppliedZoomRect.height / 2);

		if(!(rcAppliedZoomRect.x < 0 ||
			rcAppliedZoomRect.y < 0 ||
			rcAppliedZoomRect.x + rcAppliedZoomRect.width > 3840 ||
			rcAppliedZoomRect.y + rcAppliedZoomRect.height > 2160))
		{	
			nHighDSC = i;		
			break;
		}
	}

	if(nLowDSC == GetDscCount() && nHighDSC == 0)
	{
		ESMLog(5, _T("Error ESM_VALUE_TEMPLATE_TARGETOUT_PERCENT(All Target out!!!"));
		return;
	}
	if(nLowDSC == 0 && nHighDSC == GetDscCount()-1)
	{
		ESMLog(5, _T("Do not need to ESM_VALUE_TEMPLATE_TARGETOUT_PERCENT"));
		return;
	}

	for(int i = 0; i < vecVMCCTemplate->size(); i++)
	{
		int nStartDSC	= vecVMCCTemplate->at(i).nStrartDSC;
		int nEndDSC		= vecVMCCTemplate->at(i).nEndDSC;

		if(nStartDSC == nEndDSC)
			m_vecViewDirect.push_back(VIEW_DIRECT_SAME);
		if(nStartDSC < nEndDSC)
			m_vecViewDirect.push_back(VIEW_DIRECT_LOW_HIGH);
		if(nStartDSC > nEndDSC)
			m_vecViewDirect.push_back(VIEW_DIRECT_HIGH_LOW);
	}

	for(int i = 0; i < vecVMCCTemplate->size(); i++)
	{		
		if(m_vecViewDirect[i] == VIEW_DIRECT_SAME)
		{
			if(i > 0)
			{
				if(vecVMCCTemplate->at(i).nVMCC_Data[0].nZoom != vecVMCCTemplate->at(i-1).nVMCC_Data[1].nZoom)
				{
					vecVMCCTemplate->at(i).nVMCC_Data[0].nZoom = vecVMCCTemplate->at(i-1).nVMCC_Data[1].nZoom;
					vecVMCCTemplate->at(i).nVMCC_Data[1].nZoom = vecVMCCTemplate->at(i-1).nVMCC_Data[1].nZoom;
				}
			}
			if(vecVMCCTemplate->at(i).nStrartDSC < nLowDSC)
			{
				vecVMCCTemplate->at(i).nStrartDSC	= nLowDSC;
				vecVMCCTemplate->at(i).nEndDSC		= nLowDSC;
			}
			if(vecVMCCTemplate->at(i).nStrartDSC > nHighDSC)
			{
				vecVMCCTemplate->at(i).nStrartDSC	= nHighDSC;
				vecVMCCTemplate->at(i).nEndDSC		= nHighDSC;
			}
		}

		if(m_vecViewDirect[i] == VIEW_DIRECT_LOW_HIGH)
		{
			if(i > 0)
			{
				if(vecVMCCTemplate->at(i).nVMCC_Data[0].nZoom != vecVMCCTemplate->at(i-1).nVMCC_Data[1].nZoom)
				{
					vecVMCCTemplate->at(i).nVMCC_Data[0].nZoom = vecVMCCTemplate->at(i-1).nVMCC_Data[1].nZoom;
				}
			}

			if(vecVMCCTemplate->at(i).nStrartDSC < nLowDSC)
			{
				if(vecVMCCTemplate->at(i).nEndDSC < nLowDSC)
				{
					vecVMCCTemplate->erase(vecVMCCTemplate->begin()+i);
					m_vecViewDirect.erase(m_vecViewDirect.begin()+i);
					i--;
					continue;
				}
				vecVMCCTemplate->at(i).nStrartDSC	= nLowDSC;
			}
			if(vecVMCCTemplate->at(i).nEndDSC > nHighDSC)
			{
				if(vecVMCCTemplate->at(i).nStrartDSC > nHighDSC)
				{
					vecVMCCTemplate->erase(vecVMCCTemplate->begin()+i);
					m_vecViewDirect.erase(m_vecViewDirect.begin()+i);
					i--;
					continue;
				}
				vecVMCCTemplate->at(i).nEndDSC		= nHighDSC;
			}
		}

		if(m_vecViewDirect[i] == VIEW_DIRECT_HIGH_LOW)
		{
			if(i > 0)
			{
				if(vecVMCCTemplate->at(i).nVMCC_Data[0].nZoom != vecVMCCTemplate->at(i-1).nVMCC_Data[1].nZoom)
				{
					vecVMCCTemplate->at(i).nVMCC_Data[0].nZoom = vecVMCCTemplate->at(i-1).nVMCC_Data[1].nZoom;
				}
			}

			if(vecVMCCTemplate->at(i).nStrartDSC > nHighDSC)
			{
				if(vecVMCCTemplate->at(i).nEndDSC > nHighDSC)
				{
					vecVMCCTemplate->erase(vecVMCCTemplate->begin()+i);
					m_vecViewDirect.erase(m_vecViewDirect.begin()+i);
					i--;
					continue;
				}
				vecVMCCTemplate->at(i).nStrartDSC	= nHighDSC;
			}
			if(vecVMCCTemplate->at(i).nEndDSC < nLowDSC)
			{
				if(vecVMCCTemplate->at(i).nStrartDSC < nLowDSC)
				{
					vecVMCCTemplate->erase(vecVMCCTemplate->begin()+i);
					m_vecViewDirect.erase(m_vecViewDirect.begin()+i);
					i--;
					continue;
				}
				vecVMCCTemplate->at(i).nEndDSC		= nLowDSC;
			}
		}
	}
}

void CESMTemplateMgr::CalcOverBoundary(int nXchange, int nYchange, int nChangeRatio, CString dscIndex)
{
	Rect2d appliedZoomRect;

	double width = m_dWidth;
	double height = m_dHeight;

	appliedZoomRect.width  = width/(nChangeRatio/100.);
	appliedZoomRect.height = height/(nChangeRatio/100.);
	appliedZoomRect.x = nXchange - (appliedZoomRect.width / 2);
	appliedZoomRect.y = nYchange - (appliedZoomRect.height / 2);

	double leftOverBoundaryX = -appliedZoomRect.x;
	double rightOverBoundaryX = appliedZoomRect.x + appliedZoomRect.width - width;
	double topOverBoundaryY = -appliedZoomRect.y;
	double bottomOverBoundaryY = appliedZoomRect.y + appliedZoomRect.height - height;

	double overBoundaryX;
	double overBoundaryY;
	double overBoundary;
	
	if(leftOverBoundaryX > 0 || rightOverBoundaryX > 0)
	{
		if(leftOverBoundaryX > rightOverBoundaryX)
			overBoundaryX = leftOverBoundaryX;
		else
			overBoundaryX = rightOverBoundaryX;
	} else overBoundaryX = 0.;

	if(topOverBoundaryY > 0 || bottomOverBoundaryY > 0)
	{
		if(topOverBoundaryY > bottomOverBoundaryY)
			overBoundaryY = topOverBoundaryY;
		else
			overBoundaryY = bottomOverBoundaryY;
	} else overBoundaryY = 0.;

	if(overBoundaryX > overBoundaryY*1.777778)	
	{
		overBoundary = appliedZoomRect.width / (appliedZoomRect.width - overBoundaryX*2) ;
	}
	else
	{
		overBoundary = appliedZoomRect.width/ (appliedZoomRect.width - (overBoundaryY*2*1.777778));
	}	

	if(m_dMaxOverBoundary < overBoundary)
	{
		m_dMaxOverBoundary = overBoundary;

		m_nViewPointX = nXchange;
		m_nViewPointY = nYchange;
		m_nZoom = nChangeRatio;		
	}	
}

double CESMTemplateMgr::GetRecalculatedZoomSize(int zoom)
{	
	double appliedZoom = 0.;

	if(m_dMaxOverBoundary == 0)
		return 0.;

	appliedZoom = zoom * m_dMaxOverBoundary; 

	if(appliedZoom < 100 || appliedZoom > 3000)
		appliedZoom = 3000;

	return appliedZoom;
}

void CESMTemplateMgr::RecalculatedTemplateZoom()
{
	int nMaxZoom = 0;
	for(int i = 0; i < m_arrExistFrame.size(); i++)
	{
		int nChangeRatio = m_arrExistFrame[i]->GetZoomRatio();
		if(nChangeRatio > nMaxZoom)
		{
			nMaxZoom = nChangeRatio;
		}
	}

	for(int i = 0; i < m_arrExistFrame.size(); i++)
	{
		int nChangeRatio = m_arrExistFrame[i]->GetZoomRatio();
		int nXchange = m_arrExistFrame[i]->GetPosX();
		int nYchange = m_arrExistFrame[i]->GetPosY();

		Rect2d appliedZoomRect;

		double width = m_dWidth;
		double height = m_dHeight;

		appliedZoomRect.width  = width/(nChangeRatio/100.);
		appliedZoomRect.height = height/(nChangeRatio/100.);
		appliedZoomRect.x = nXchange - (appliedZoomRect.width / 2);
		appliedZoomRect.y = nYchange - (appliedZoomRect.height / 2);

		double leftOverBoundaryX = -appliedZoomRect.x;
		double rightOverBoundaryX = appliedZoomRect.x + appliedZoomRect.width - width;
		double topOverBoundaryY = -appliedZoomRect.y;
		double bottomOverBoundaryY = appliedZoomRect.y + appliedZoomRect.height - height;

		double overBoundaryX;
		double overBoundaryY;
		double overBoundary;

		if(leftOverBoundaryX > 0 || rightOverBoundaryX > 0)
		{
			if(leftOverBoundaryX > rightOverBoundaryX)
				overBoundaryX = leftOverBoundaryX;
			else
				overBoundaryX = rightOverBoundaryX;
		} else overBoundaryX = 0.;

		if(topOverBoundaryY > 0 || bottomOverBoundaryY > 0)
		{
			if(topOverBoundaryY > bottomOverBoundaryY)
				overBoundaryY = topOverBoundaryY;
			else
				overBoundaryY = bottomOverBoundaryY;
		} else overBoundaryY = 0.;

		if(overBoundaryX > overBoundaryY*1.777778)
		{
			overBoundary = appliedZoomRect.width / (appliedZoomRect.width - overBoundaryX*2) ;
		}
		else
		{
			overBoundary = appliedZoomRect.width/ (appliedZoomRect.width - (overBoundaryY*2*1.777778));
		}	

		double test = GetRecalculatedZoomSize(m_arrExistFrame[i]->GetZoomRatio());
		if(nChangeRatio == nMaxZoom)
			m_arrExistFrame[i]->SetZoomRatio(nChangeRatio * overBoundary);

		/*double test = GetRecalculatedZoomSize(m_arrExistFrame[i]->GetZoomRatio());
		m_arrExistFrame[i]->SetZoomRatio(test);*/
	}

	m_arrExistFrame.clear();
	m_dMaxOverBoundary = 0.;
}

void CESMTemplateMgr::CheckPointBoundary()
{	
	double maxZoom = 100;
	for(int i = 0; i < m_arrExistFrame.size(); i++)
	{	
		if(m_arrExistFrame[i]->GetZoomRatio() > maxZoom)
			maxZoom = m_arrExistFrame[i]->GetZoomRatio();
	}

	double width = m_dWidth;
	double height = m_dHeight;

	int dscNumber = GetDscIndexFrom(m_strDscIndex);
	Point2f modifiedCenterPoint = m_nCenterPoint;//m_arrTemplatePoint[dscNumber].centerPointOfRotation;
	Point2f modifiedViewPoint = m_nViewPoint;
	
	int divideInterval = 50;
	double modifyIntervalX = ((width/2 - modifiedCenterPoint.x)/divideInterval);
	double modifyIntervalY = ((height/2 - modifiedCenterPoint.y)/divideInterval);

	int count = 0;
	while(1)
	{
		BOOL isRectOverBoundary = false;
		Rect2d zoomRect;	

		SetArrTemplatePoint(m_strDscIndex, modifiedCenterPoint, modifiedViewPoint);
	
		for(int i = 0; i < m_arrExistFrame.size(); i++)
		{	
			if(maxZoom ==  m_arrExistFrame[i]->GetZoomRatio())
			{
				double zoomSize = m_arrExistFrame[i]->GetZoomRatio();				

				Point2f rectCenter = GetTemplatePoint(GetDscIndexFrom(m_arrExistFrame[i]->GetDSC())).viewPoint;

				//2017-05-16 wgkim@esmlab.com
				if(rectCenter.x == 0 || rectCenter.y == 0)				
					continue;		
				//

				zoomRect.width  = width/(zoomSize/100.);
				zoomRect.height = height/(zoomSize/100.);
				zoomRect.x = rectCenter.x - (zoomRect.width / 2);
				zoomRect.y = rectCenter.y - (zoomRect.height / 2);			

				if( zoomRect.x < 0 || 
					zoomRect.y < 0 ||
					zoomRect.x + zoomRect.width > width ||
					zoomRect.y + zoomRect.height > height)
				{
					m_bIsOverBoundary = true;
					isRectOverBoundary = true;				
				}					
			}
		}

		if(isRectOverBoundary == false || count == divideInterval )
		{	
			m_nModifiedViewPoint = modifiedViewPoint;
			m_nModifiedCenterPoint = modifiedCenterPoint;
			break;
		}
		else
		{
			count++;
			modifiedViewPoint.x += modifyIntervalX;
			modifiedViewPoint.y += modifyIntervalY;
			modifiedCenterPoint.x += modifyIntervalX;
			modifiedCenterPoint.y += modifyIntervalY;			
		}		
	}	
}

void CESMTemplateMgr::RecalculatedCenterPoint()
{
	for(int i = 0; i < m_arrExistFrame.size(); i++)
	{	
		int dscIdIndex = GetDscIndexFrom(m_arrExistFrame[i]->GetDSC());
		int nXchange = GetTemplatePoint(dscIdIndex).viewPoint.x;
		int nYchange = GetTemplatePoint(dscIdIndex).viewPoint.y;
		m_arrExistFrame[i]->SetPosX(nXchange);				
		m_arrExistFrame[i]->SetPosY(nYchange);
	}

	m_arrExistFrame.clear();	
	//m_arrTemplftePoint.clear();
	m_dMaxOverBoundary = 0.;
}

void CESMTemplateMgr::SetOverBoundaryFlag(BOOL flag)
{
	m_bIsOverBoundary = flag;
}

BOOL CESMTemplateMgr::IsOverBoundary()
{
	return m_bIsOverBoundary;
}

Point2f CESMTemplateMgr::GetModifiedViewPoint()
{
	return m_nModifiedViewPoint;	
}

Point2f CESMTemplateMgr::GetModifiedCenterPoint()
{
	return m_nModifiedCenterPoint;	
}

void CESMTemplateMgr::ConvertArrSquaresUsingAdjustData(vector<vector<Point2f>> &squares)
{
	//wgkim 180919
	if(!m_arrAdjInfo.empty())
	{
		for(int i = 0; i < GetDscCount(); i++)
		{
			if (squares.size() < 1)
				return;

			if( (squares[i].at(0).x == 0 && squares[i].at(0).y == 0) && 
				(squares[i].at(1).x == 0 && squares[i].at(1).y == 0) && 
				(squares[i].at(2).x == 0 && squares[i].at(2).y == 0) &&
				(squares[i].at(3).x == 0 && squares[i].at(3).y == 0))
				continue;

			double dScale = m_arrAdjInfo.at(i).AdjSize;
			double dAngle = m_arrAdjInfo.at(i).AdjAngle;
			Point2f ptRotateCenter = Point2f( m_arrAdjInfo.at(i).AdjptRotate.x, m_arrAdjInfo.at(i).AdjptRotate.y);

			if(dScale == 0)
			{
				dScale = 1.;
				dAngle += -90.;
			}
			if( dAngle == 0.0 && dScale == 0.0)
			{
				//return ;
				//continue;
			}
			double dbAngleAdjust = -1 * (dAngle + 90.);
			Mat rotationMat(cv::Size(2, 3), CV_32FC1);
			rotationMat = getRotationMatrix2D(ptRotateCenter, dbAngleAdjust, dScale);

			for(int j = 0; j < squares[i].size(); j++)
			{
				double tempMat_[3] = {(double)squares.at(i)[j].x, (double)squares.at(i)[j].y, 1.};
				Mat tempMat(3, 1, CV_64F, tempMat_);
				Mat resultMat = rotationMat * tempMat;

				squares.at(i)[j] = Point2f(resultMat.at<double>(0), resultMat.at<double>(1));
			}

			double dMoveX = m_arrAdjInfo.at(i).AdjMove.x;	 // 반올림
			double dMoveY = m_arrAdjInfo.at(i).AdjMove.y;	 // 반올림

			for(int j = 0; j < squares[i].size(); j++)
			{
				squares.at(i)[j].x += dMoveX; 
				squares.at(i)[j].y += dMoveY;
			}

			int nMarginX = m_arrAdjInfo.at(i).stMargin.nMarginX;
			int nMarginY = m_arrAdjInfo.at(i).stMargin.nMarginY;
			if ( nMarginX < 0 || nMarginX >= m_arrAdjInfo.at(i).nWidth/2. || nMarginY < 0 || nMarginY >= m_arrAdjInfo.at(i).nHeight/2. )
			{
				//TRACE(_T("Image Adjust Error 2\n"));
				//return;
				continue;
			}
			double dbMarginScale = 1. / ( (double)( m_arrAdjInfo.at(i).nWidth- nMarginX * 2) /(double) m_arrAdjInfo.at(i).nWidth);
			Mat  pMatResize;

			for(int j = 0; j < squares[i].size(); j++)
			{
				double tempSquareX = squares.at(i)[j].x * dbMarginScale;
				double tempSquarey = squares.at(i)[j].y * dbMarginScale;

				squares.at(i)[j].x = tempSquareX - (m_arrAdjInfo.at(i).nWidth*dbMarginScale - m_arrAdjInfo.at(i).nWidth)/2.;
				squares.at(i)[j].y = tempSquarey - (m_arrAdjInfo.at(i).nHeight*dbMarginScale - m_arrAdjInfo.at(i).nHeight)/2.;
			}
		}
		ESMLog(5, _T("Set AutoAdjust Position Tracking"));
	}
	else
	{
		ESMLog(5, _T("Set Normal Position Tracking"));
	}
}