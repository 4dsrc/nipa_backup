#pragma once

#include <list>
#include <set>
#include <algorithm>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

class CESMAutoAdjustImageMapping
{
public:
	CESMAutoAdjustImageMapping(void);
	~CESMAutoAdjustImageMapping(void);

public:
	enum SELECT_KEYPOINT {
		SELECT_KEYPOINT_ORB,
		SELECT_KEYPOINT_SIMPLEBLOB,
		SELECT_KEYPOINT_FAST,
		SELECT_KEYPOINT_BRISK,//good
		SELECT_KEYPOINT_KAZE,
		SELECT_KEYPOINT_AKAZE,
		SELECT_KEYPOINT_SURF,
		SELECT_KEYPOINT_SIFT,
		SELECT_KEYPOINT_FREAK,
		SELECT_KEYPOINT_DAISY,
		SELECT_KEYPOINT_BRIEF,
	};
	SELECT_KEYPOINT eSelectKeypoint;

	struct AdjustFeatureInfo
	{
		AdjustFeatureInfo()
		{
			nIndex = 0;
			strDscId = "";
		}
		int nIndex;
		string strDscId;

		Mat mImage;
		Mat mGrayImage;

		vector<Point2f> vecSquarePoint;

		//Mat mDescript;
		//vector<KeyPoint> vecKeypoints;
	};
	vector<AdjustFeatureInfo*> _vecAdjustFeatureInfo;

	void AddAdjustFeatureInfo(
		int nIndex, 
		string strDscId, 
		Mat mImage, 
		vector<Point2f> vecSquarePoint);
	void ClearAdjustFeatureInfo();

private:
	int _nMinKeypointsCount;
	int _nMaxKeypointsCount;

	int _nWidth;
	int _nHeight;

	vector<Point2f> _vecSquarePoint;
	Mat _mHomography;
	vector<Point2f> _vecPrevSquarePoint;

	Rect2d _rtImageROI;
	vector<Point2f> _vecReferenceSquarePoints;

public:
	bool RunSquareDetecting(Mat& image);

	void SetSquarePoint(vector<Point2f> vecSquarePoint);
	void CalcSquarePoint(vector<Point2f> vecSquarePointROI, vector<Point2f>& vecSquarePoint, Mat mHomography);

	void SetImageROI(int nX, int nY, int nWidth, int nHeight);
	Rect2d GetImageROI();

	void SetGroundTruth(
		Mat& mCurrGrayImage,
		vector<KeyPoint>& vecCurrKeypoints,
		vector<Point2f>& vecCurrSquarePoint,
		Mat& mCurrDescript
		);

	void SetReferenceSquarePoints(vector<Point2f> vecReferenceSquarePoints);
	Mat CreateWarpingImage(Mat image);

	float CheckTransformError(Mat mReferenceImage, Mat mWarpingImage);
	float CheckOcclusion(Mat mReferenceImage, Mat mWarpingImage);
	Mat _mReferenceImage;

	//Filter
public:
	void BilateralFilter(Mat mCurrGrayImage, Mat& filterImage);

	//KeyPoint Detecting
public:
	void GetKeyPoints(
		SELECT_KEYPOINT eSelectKeypoint,
		Mat mCurrGrayImage, 
		vector<KeyPoint>& vecCurrKeypoints, 
		Mat& mCurrDescript);
	void GetKeyPointsFAST(
		Mat mCurrGrayImage,
		vector<KeyPoint>& vecCurrKeypoints, 
		int nMinKeypointCount,
		int nMaxKeypointCount,
		Mat& mCurrDescript);
	bool IsInside(Point2f B, const vector<Point2f> p);
	vector<Point2f> _vecSquarePt;

	//KeyPoint Matching
private:
	double	_dKDistanceCoef;
	int		_nKMaxMatchingSize;

public:
	void Matching(
		vector<DMatch>& vecMatches, 
		Mat& mPrevDescript,
		Mat& mCurrDescript);
	void MatchingWithOpticalFlow(
		vector<DMatch>& vecMatches,
		Mat& mPrevImage,
		Mat& mCurrImage,
		vector<KeyPoint>& vecPrevKeypoints,
		vector<KeyPoint>& vecCurrKeypoints);

	//KeyPoint Filtering
public:
	Mat FindKeyPointsHomography(
		Mat& image,
		vector<DMatch>& vecMatches, 
		vector<KeyPoint>& vecPrevKeypoints, 
		vector<KeyPoint>& vecCurrKeypoints, 
		vector<char>& vecSzMatchesMask);

	Mat Find3DKeyPointsHomography(
		Mat& image,
		vector<DMatch>& vecMatches, 
		vector<KeyPoint>& vecPrevKeypoints, 
		vector<KeyPoint>& vecCurrKeypoints, 
		vector<char>& vecSzMatchesMask);

	//Draw
public:
	void DrawKeypoints(
		Mat image,
		Mat mPrevGrayImage,
		Mat mCurrGrayImage,
		vector<KeyPoint> vecPrevKeypoints,
		vector<KeyPoint> vecCurrKeypoints,
		vector<DMatch> vecMatches, 
		vector<char> vecSzMatchesMask
		);
	void DrawSquarePoints(Mat& mImage);

	vector<Point2f> GetModifiedSquarePoint();


private:
	double _dScale;

	Mat _currImage;
	Mat _mPrevGrayImage;
	Mat _mPrevDescript;
	vector<KeyPoint> _vecPrevKeypoints;
};
