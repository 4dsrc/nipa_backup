// ESMUtilMultiplexerOptDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMUtilMultiplexerOptDlg.h"
#include "afxdialogex.h"

#include "ESMIni.h"
#include "ESMFileOperation.h"
#include "MainFrm.h"

// CESMUtilMultiplexerOptDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMUtilMultiplexerOptDlg, CDialogEx)

CESMUtilMultiplexerOptDlg::CESMUtilMultiplexerOptDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMUtilMultiplexerOptDlg::IDD, pParent)
{
	m_bInILoad = FALSE;
	m_bDBConnect = FALSE;
	m_nSubL		= -1;
	m_nSubM		= -1;
	m_nSubS		= -1;
	m_nOwn		= -1;
}

CESMUtilMultiplexerOptDlg::~CESMUtilMultiplexerOptDlg()
{
}

void CESMUtilMultiplexerOptDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MUXOPT_COMBO_SUB_L, m_cbSUBL);
	DDX_CBString(pDX, IDC_MUXOPT_COMBO_SUB_L,m_strSUBL);
	DDX_Control(pDX, IDC_MUXOPT_COMBO_SUB_M, m_cbSUBM);
	DDX_CBString(pDX, IDC_MUXOPT_COMBO_SUB_M,m_strSUBM);
	DDX_Control(pDX, IDC_MUXOPT_COMBO_SUB_S, m_cbSUBS);
	DDX_CBString(pDX, IDC_MUXOPT_COMBO_SUB_S,m_strSUBS);
	DDX_Control(pDX, IDC_MUXOPT_COMBO_OWNER, m_cbOwn);
}


BEGIN_MESSAGE_MAP(CESMUtilMultiplexerOptDlg, CDialogEx)
	ON_CBN_SELCHANGE(IDC_MUXOPT_COMBO_SUB_L, &CESMUtilMultiplexerOptDlg::OnCbnSelchangeMuxoptComboSubL)
	//ON_CBN_SELCHANGE(IDC_MUXOPT_COMBO_SUB_L, &CESMUtilMultiplexerOptDlg::OnCbnSelchangeMuxoptComboSubL)
	ON_CBN_SELCHANGE(IDC_MUXOPT_COMBO_SUB_M, &CESMUtilMultiplexerOptDlg::OnCbnSelchangeMuxoptComboSubM)
	ON_BN_CLICKED(IDOK, &CESMUtilMultiplexerOptDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_MUXOPT_BTN_SUB_L, &CESMUtilMultiplexerOptDlg::OnBnClickedMuxoptBtnSubL)
	ON_BN_CLICKED(IDC_MUXOPT_BTN_SUB_M, &CESMUtilMultiplexerOptDlg::OnBnClickedMuxoptBtnSubM)
	ON_BN_CLICKED(IDC_MUXOPT_BTN_SUB_S, &CESMUtilMultiplexerOptDlg::OnBnClickedMuxoptBtnSubS)
	ON_CBN_SELCHANGE(IDC_MUXOPT_COMBO_OWNER, &CESMUtilMultiplexerOptDlg::OnCbnSelchangeMuxoptComboOwner)
END_MESSAGE_MAP()


// CESMUtilMultiplexerOptDlg 메시지 처리기입니다.


BOOL CESMUtilMultiplexerOptDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	//ConnectDB();
	InitData();
	LoadExistData();
	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void CESMUtilMultiplexerOptDlg::InitData()
{
#if 1
	CESMFileOperation fo;

	CString strHomePath = ESMGetPath(ESM_PATH_HOME);
	strHomePath.Append(_T("\\config\\cblist.ini"));
	m_strHomePath = strHomePath;

	if(!fo.IsFileExist(strHomePath))
	{
		CFile file;
		if(file.Open(m_strHomePath,CFile::modeCreate))
		{
			file.Close();
			m_bInILoad = m_ini.SetIniFilename(m_strHomePath);
			if(m_ini.SetIniFilename(m_strHomePath) == TRUE)
			{
				m_bInILoad = TRUE;
				CreateNewIni();
			}
		}
	}
	if(m_bInILoad == FALSE)
		m_ini.SetIniFilename(m_strHomePath);

	//Setting UI using readed InI
	CreateComboBox();
#else
	if(m_bDBConnect == FALSE)
	{
		CESMFileOperation fo;

		CString strHomePath = ESMGetPath(ESM_PATH_HOME);
		strHomePath.Append(_T("\\config\\cblist.ini"));
		m_strHomePath = strHomePath;

		if(!fo.IsFileExist(strHomePath))
		{
			CFile file;
			if(file.Open(m_strHomePath,CFile::modeCreate))
			{
				file.Close();
				m_bInILoad = m_ini.SetIniFilename(m_strHomePath);
				if(m_ini.SetIniFilename(m_strHomePath) == TRUE)
				{
					m_bInILoad = TRUE;
					CreateNewIni();
				}
			}
		}
		if(m_bInILoad == FALSE)
			m_ini.SetIniFilename(m_strHomePath);

		//Setting UI using readed InI
		CreateComboBox();
	}
	else
	{
		char* sQuery = new char[200];
		//strcpy(sQuery, "select Tel1, Email1 from user where CurMovie = \"1\";");
		strcpy(sQuery, "select * from SUB_L_INFO;");
		MYSQL_ROW row;
		MYSQL_RES *m_res = NULL;
		CString strTelNum, strEmail;
		int nRet = mysql_query(&mysql, sQuery);

		m_res = mysql_store_result(&mysql);

		if(m_res == NULL)
		{
			AfxMessageBox(_T("DB Connect Error"));
			ESMSetCeremonyDBConnect(FALSE);
			return;
		}

		row = mysql_fetch_row(m_res);
		int nCnt = m_res->row_count;
		for(int i = 0 ; i < nCnt; i++)
		{
			strTelNum = ESMUtil::CharToCString(row[nCnt]);
			ESMLog(5,strTelNum);
		}
		/*while(nCnt--)
		{
		strTelNum = ESMUtil::CharToCString(row[nCnt]);
		m_cbSUBL.AddString(strTelNum);
		}*/
		if(row)
		{
			if(((row[0]==NULL)||(!strlen(row[0]))))
			{
				strTelNum = _T("");
				strEmail = _T("");
			}
			else
			{
				strTelNum = ESMUtil::CharToCString(row[0]);
				//m_cbSUBL.AddString(strTelNum);
				//strEmail = ESMUtil::CharToCString(row[1]);
			}
		}
	}
#endif
}

void CESMUtilMultiplexerOptDlg::CreateComboBox()
{
	CStringArray strArrMode;

	m_ini.GetSectionLineList(STRING_MODE,strArrMode);

	for(int i = 0 ; i < strArrMode.GetCount(); i++)
	{
		CString str = GetInIString(strArrMode.GetAt(i));
		m_cbSUBL.AddString(str);
	}

	strArrMode.RemoveAll();
	m_ini.GetSectionLineList(STRING_OWN,strArrMode);

	for(int i = 0 ; i < strArrMode.GetCount(); i++)
	{
		CString str = GetInIString(strArrMode.GetAt(i));
		m_cbOwn.AddString(str);
	}
}
void CESMUtilMultiplexerOptDlg::CreateNewIni()
{
	CString strValue = _T("0");

	m_ini.WriteString(STRING_MODE,_T("Sport"),strValue);
	m_ini.WriteString(STRING_MODE,_T("Movie"),strValue);
	m_ini.WriteString(STRING_MODE,_T("Event"),strValue);

	m_ini.WriteString(_T("Sport"),_T("Baseball"),strValue);
	m_ini.WriteString(_T("Sport"),_T("Soccer"),strValue);
	m_ini.WriteString(_T("Sport"),_T("Valleyball"),strValue);

	m_ini.WriteString(_T("Movie"),_T("Movie"),strValue);
	m_ini.WriteString(_T("Movie"),_T("Drama"),strValue);
	
	m_ini.WriteString(_T("Event"),_T("IBC"),strValue);
	m_ini.WriteString(_T("Event"),_T("Audi"),strValue);
	m_ini.WriteString(_T("Event"),_T("Dior"),strValue);
	
	m_ini.WriteString(_T("Baseball"),_T("ORIX"),strValue);
	m_ini.WriteString(_T("Baseball"),_T("NIPPONHAM"),strValue);
	m_ini.WriteString(_T("Baseball"),_T("KIA"),strValue);
	m_ini.WriteString(_T("Baseball"),_T("SAMSUNG"),strValue);
	m_ini.WriteString(_T("Baseball"),_T("HANHWA"),strValue);
	m_ini.WriteString(_T("Baseball"),_T("NEXEN"),strValue);
	m_ini.WriteString(_T("Baseball"),_T("LOTTE"),strValue);
	m_ini.WriteString(_T("Baseball"),_T("DOOSAN"),strValue);
	m_ini.WriteString(_T("Baseball"),_T("NC"),strValue);
	m_ini.WriteString(_T("Baseball"),_T("SK"),strValue);
	m_ini.WriteString(_T("Baseball"),_T("KT"),strValue);
	m_ini.WriteString(_T("Baseball"),_T("LG"),strValue);
	m_ini.WriteString(_T("Baseball"),_T("WBC"),strValue);
	m_ini.WriteString(_T("Baseball"),_T("MLB"),strValue);

	m_ini.WriteString(_T("Valleyball"),_T("WorldLeague"),strValue);
	m_ini.WriteString(_T("Valleyball"),_T("KOVO"),strValue);

	m_ini.WriteString(_T("IBC"),_T("IBC2017"),strValue);
}

void CESMUtilMultiplexerOptDlg::OnCbnSelchangeMuxoptComboSubL()
{
#if 0
	char* sQuery = new char[200];
	//strcpy(sQuery, "select Tel1, Email1 from user where CurMovie = \"1\";");
	strcpy(sQuery, "select * from SUB_L_INFO;");
	MYSQL_ROW row;
	MYSQL_RES *m_res = NULL;
	CString strTelNum, strEmail;
	int nRet = mysql_query(&mysql, sQuery);

	m_res = mysql_store_result(&mysql);

	if(m_res == NULL)
	{
		AfxMessageBox(_T("DB Connect Error"));
		ESMSetCeremonyDBConnect(FALSE);
		return;
	}

	row = mysql_fetch_row(m_res);
	if(row)
	{
		if(((row[0]==NULL)||(!strlen(row[0]))))
		{
			strTelNum = _T("");
			strEmail = _T("");
		}
		else
		{
			strTelNum = ESMUtil::CharToCString(row[0]);
			strEmail = ESMUtil::CharToCString(row[1]);
		}
	}
#else
	int nIndex;
	CString strTemp;

	nIndex = m_cbSUBL.GetCurSel();
	m_cbSUBL.GetLBText(nIndex,strTemp);
	m_strSUBL = strTemp;

	CStringArray arList;
	m_ini.GetSectionLineList(strTemp,arList);

	m_cbSUBM.ResetContent();
	m_cbSUBS.ResetContent();

	int nCount = arList.GetCount();
	for(int i = 0 ; i < nCount ; i++) 
	{
		CString str = GetInIString(arList.GetAt(i));

		m_cbSUBM.AddString(str);
	}
#endif
}

void CESMUtilMultiplexerOptDlg::OnCbnSelchangeMuxoptComboSubM()
{
	int nIndex;
	CString strTemp;
	
	nIndex = m_cbSUBM.GetCurSel();
	m_cbSUBM.GetLBText(nIndex,strTemp);
	
	CStringArray arList;
	m_ini.GetSectionLineList(strTemp,arList);

	m_cbSUBS.ResetContent();

	int nCount = arList.GetCount();
	for(int i = 0 ; i < nCount ; i++)
	{
		CString str = GetInIString(arList.GetAt(i));

		m_cbSUBS.AddString(str);
		/*mysql_init(&mysql);
		if(!mysql_real_connect(&mysql, DB_ADDRESS, DB_ID, DB_PASS, DB_NAME ,DB_PORT, 0, 0))
		{
		ESMLog(5, _T("Mysql connect Fail"));
		return ;
		}
		else
		ESMLog(5, _T("Mysql connect Success\n"));

		mysql_query(&mysql,"use project");*/
	}
}

CString CESMUtilMultiplexerOptDlg::GetInIString(CString strName)
{
	if(!strName.GetLength())
		return _T("");

	int nPos = 0;
	CString strToken = strName.Tokenize(_T("="),nPos);
	strToken.Replace(_T(" "), _T(""));

	return strToken;
}

void CESMUtilMultiplexerOptDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//AfxMessageBox(_T("First"));
	
	CString temp;

	int nSel = m_cbSUBL.GetCurSel();	
	m_cbSUBL.GetLBText(nSel,temp);
	m_strSUBL = CheckNamePath(temp);
	m_nSubL = nSel;

	nSel = m_cbSUBM.GetCurSel();
	m_cbSUBM.GetLBText(nSel,temp);
	m_strSUBM = CheckNamePath(temp);
	m_nSubM = nSel;

	nSel = m_cbSUBS.GetCurSel();
	m_cbSUBS.GetLBText(nSel,temp);
	m_strSUBS = CheckNamePath(temp);
	m_nSubS = nSel;

	GetDlgItemText(IDC_MUXOPT_EDIT_OBJECT,temp);
	m_strOBJ = CheckNamePath(temp);

	GetDlgItemText(IDC_MUXOPT_EDIT_DESCRIPTION,temp);
	m_strDES = CheckNamePath(temp);

	nSel = m_cbOwn.GetCurSel();
	m_cbOwn.GetLBText(nSel,temp);
	m_strOwn = CheckNamePath(temp);
	m_nOwn = nSel;
	CDialogEx::OnOK();
}

void CESMUtilMultiplexerOptDlg::OnBnClickedMuxoptBtnSubL()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nCount = m_cbSUBL.GetCount();
	if(nCount == 0)
		return;

	CString str;
	GetDlgItemText(IDC_MUXOPT_COMBO_SUB_L,str);
	
	if(!str.GetLength())
		return;

	CStringArray arr;
	m_ini.GetSectionList(STRING_MODE,arr);

	BOOL bIsSame = TRUE;
	for(int i = 0 ; i < arr.GetCount(); i++)
	{
		CString strList = arr.GetAt(i);
		if(strList == str)
		{
			bIsSame = FALSE;
			break;
		}
	}
	if(bIsSame == TRUE)
	{
		m_cbSUBL.AddString(str);
		m_ini.WriteString(STRING_MODE,str,_T("0"));
		nCount = m_cbSUBL.GetCount() - 1;
		m_cbSUBL.SetCurSel(nCount);
		m_strSUBL = str;
	}
	/*int nIndex = m_cbSUBL.GetCurSel();
	if(nIndex == -1)
		return;

	CString s;
	s.Format(_T("0x%X"),(UINT)m_cbSUBL.GetItemData(nIndex));
	SetDlgItemText(IDC_MUXOPT_COMBO_SUB_L,s);*/
}

void CESMUtilMultiplexerOptDlg::OnBnClickedMuxoptBtnSubM()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nCount = m_cbSUBM.GetCount();
	if(nCount == 0)
		return;

	CString str;
	GetDlgItemText(IDC_MUXOPT_COMBO_SUB_M,str);

	if(!str.GetLength())
		return;

	CStringArray arr;
	m_ini.GetSectionList(m_strSUBL,arr);

	BOOL bIsSame = TRUE;
	for(int i = 0 ; i < arr.GetCount(); i++)
	{
		CString strList = arr.GetAt(i);
		if(strList == str)
		{
			bIsSame = FALSE;
			break;
		}
	}
	if(bIsSame == TRUE)
	{
		m_cbSUBM.AddString(str);
		m_ini.WriteString(STRING_MODE,str,_T("0"));
		nCount = m_cbSUBM.GetCount() - 1;
		m_cbSUBM.SetCurSel(nCount);
		m_strSUBM = str;
	}
}

void CESMUtilMultiplexerOptDlg::OnBnClickedMuxoptBtnSubS()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nCount = m_cbSUBS.GetCount();
	if(nCount == 0)
		return;

	CString str;
	GetDlgItemText(IDC_MUXOPT_COMBO_SUB_S,str);

	if(!str.GetLength())
		return;

	CStringArray arr;
	m_ini.GetSectionList(m_strSUBM,arr);

	BOOL bIsSame = TRUE;
	for(int i = 0 ; i < arr.GetCount(); i++)
	{
		CString strList = arr.GetAt(i);
		if(strList == str)
		{
			bIsSame = FALSE;
			break;
		}
	}
	if(bIsSame)
	{
		m_cbSUBS.AddString(str);
		m_ini.WriteString(STRING_MODE,str,_T("0"));
		nCount = m_cbSUBS.GetCount() - 1;
		m_cbSUBS.SetCurSel(nCount);
		m_strSUBS = str;
	}
}

CString CESMUtilMultiplexerOptDlg::CheckNamePath(CString strName)
{
	int nCount = 0;
	CString strtmp = strName;

	if(!strName.GetLength())
		return _T("");

	char *temp = new char[strName.GetLength()];
	temp = ESMUtil::CStringToChar(strName);
	for(int i = 0 ; i < strName.GetLength(); i++)
	{
		if(temp[i] == ' ')
			nCount++;

		if(nCount > 10)
			break;
	}
	if(temp)
	{
		delete temp;
		temp = NULL;
	}
	if(nCount > 10)
		return _T("NoName");
	else
		return strName;
}

void CESMUtilMultiplexerOptDlg::ConnectDB()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//if(m_bDBConnect == FALSE)
	//{
	//	mysql_init(&mysql);
	//	if(!mysql_real_connect(&mysql, DB_ADDRESS, DB_ID, DB_PASS, DB_NAME , DB_PORT, 0, 0))
	//	{
	//		m_bDBConnect = FALSE;
	//		AfxMessageBox(_T("Mysql connect Fail"));
	//		return ;
	//	}
	//	else
	//	{
	//		m_bDBConnect = TRUE;
	//		AfxMessageBox(_T("Mysql connect Success"));
	//	}
	//	//mysql_close(&mysql);
	//}
}

void CESMUtilMultiplexerOptDlg::OnCbnSelchangeMuxoptComboOwner()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str;
	GetDlgItemText(IDC_MUXOPT_COMBO_OWNER,str);

	if(!str.GetLength())
		return;

	m_strOwn = str;
}
void CESMUtilMultiplexerOptDlg::LoadExistData()
{
	if(m_strSUBL.GetLength() && m_nSubL != -1)
		m_cbSUBL.SetCurSel(m_nSubL);

	if(m_strSUBM.GetLength() && m_nSubM != -1)
		m_cbSUBM.SetCurSel(m_nSubM);

	if(m_strSUBS.GetLength() && m_nSubS != -1)
		m_cbSUBS.SetCurSel(m_nSubS);

	if(m_strOwn.GetLength() && m_nOwn != -1)
		m_cbSUBL.SetCurSel(m_nOwn);

	if(m_strOBJ.GetLength())
		GetDlgItem(IDC_MUXOPT_EDIT_OBJECT)->SetWindowTextW(m_strOBJ);

	if(m_strDES.GetLength())
		GetDlgItem(IDC_MUXOPT_EDIT_DESCRIPTION)->SetWindowTextW(m_strDES);
}