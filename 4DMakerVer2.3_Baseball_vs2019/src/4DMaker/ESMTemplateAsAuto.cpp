#include "stdafx.h"
#include "ESMTemplateAsAuto.h"


CESMTemplateAsAuto::CESMTemplateAsAuto(void)
{
	calculatedAdjustFeatures.resize(4);
	currentAdjustFeatures.resize(4);

	_imageScale = 1;
}

CESMTemplateAsAuto::~CESMTemplateAsAuto(void)
{
}


void CESMTemplateAsAuto::InputAdjustFeaturesFromList(CListCtrl* pList, int nSelectedItem)
{	
	for (int i = 0 ; i < 4 ; i++)
	{
		CString strPosX = pList->GetItemText(nSelectedItem, 2 + i*2);
		CString strPosY = pList->GetItemText(nSelectedItem, 2 + i*2 + 1);

		int nPosX = _ttoi(strPosX);
		int nPosY = _ttoi(strPosY);

		currentAdjustFeatures[i].x = nPosX/_imageScale;
		currentAdjustFeatures[i].y = nPosY/_imageScale;				
	}
}

void CESMTemplateAsAuto::ApplyCalculatedAdjustFeatureToList(CListCtrl* pList, int nSelectedItem)
{
	for(int i = 0; i < 4; i++)
	{
		CString strText;
		strText.Format(_T("%d"), (int)currentAdjustFeatures[i].x*_imageScale);
		pList->SetItemText(nSelectedItem, 2 + i*2, strText);
		strText.Format(_T("%d"), (int)currentAdjustFeatures[i].y*_imageScale);
		pList->SetItemText(nSelectedItem, 2 + i*2 + 1, strText);
	}
}

void CESMTemplateAsAuto::CalcAutoAdjustUsingOpticalFlow(Mat& srcImage)
{		
	Mat tempImage;
	resize(srcImage, tempImage, cv::Size(srcImage.cols/_imageScale, srcImage.rows/_imageScale));	

	if(previousImageForAdjust.empty()) 
	{					
		cvtColor(tempImage, previousImageForAdjust, cv::COLOR_RGB2GRAY, 1);	
		equalizeHist(previousImageForAdjust, previousImageForAdjust);
	}

	cvtColor(tempImage, nextImageForAdjust, cv::COLOR_RGB2GRAY, 1);		
	equalizeHist(nextImageForAdjust, nextImageForAdjust);

	if(currentAdjustFeatures.size() == 4)		
	{
		calcOpticalFlowPyrLK(previousImageForAdjust, nextImageForAdjust, 
			currentAdjustFeatures, calculatedAdjustFeatures, statusAdjustFeatures, 
			_err, cv::Size(21, 21), 5,
			TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 30, 0.001));
	}

	previousImageForAdjust = nextImageForAdjust.clone();	
	swap(currentAdjustFeatures, calculatedAdjustFeatures);
}

