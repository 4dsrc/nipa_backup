#pragma once

#include <opencv2/tracking.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <opencv2/cudabgsegm.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudafilters.hpp>

#include <opencv2/videostab.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/video.hpp>

#include <iostream>
#include <cstring>
#include <functional>

using namespace cv;
using namespace std;

class CESMObjectTracking
{

public:
	enum IMAGE_SIZE
	{
		FHD_SIZE = 0x1,
		UHD_SIZE = 0x2,
		ETC_SIZE = 0x4
	};

public:
	CESMObjectTracking(IMAGE_SIZE imageSize);
	~CESMObjectTracking(void);

public:
	void Filtering(Mat& srcImage, Mat& dstImage);
	void InitTracking(Mat& image, Rect2d& trackingRegion);
	void UpdateTracking(Mat& image, Rect2d& trackingRegion);
	void CorrectCenterPointInRect(Rect2d& region);

	void RoiImageOfOriginalSize(Mat& srcImage, Mat& dstImage, Rect2d& trackingRegion);
	void RoiImageOfStabilizationSize(Mat& srcImage, Mat& dstImage, Rect2d& trackingRegion);	

private:
	static void SobelExtractor(const Mat img, const cv::Rect roi, Mat& feat);
	void MeanShiftSegmentation(cuda::GpuMat& srcImage, cuda::GpuMat& dstImage);
	void BilateralFiltering(cuda::GpuMat& srcImage, cuda::GpuMat& dstImage);
	bool ModifyRegionToBeValid(Mat& image, Rect2d& inputRegion, Rect2d& paddingRegion, bool cutMargin);

public:
	double _samplingScale;
	double _templateScale;

	//Ptr<TrackerKCF> tracker;		
	Ptr<Tracker> tracker;
	TrackerKCF::Params param;	

public:
	void CreateStabilizatedMovie(string outputPath, double outputFps);	

private:
	void IncodingWithStabilizedImage(Ptr<videostab::IFrameSource> stabilizedFrames, string outputPath, double outputFps);

public:
	Ptr<videostab::IFrameSource> stabilizedFrames; 
	Ptr<videostab::ImageSetForStabilization> imageSetForStabilization;		
	Ptr<videostab::MotionEstimatorRansacL2> motionEstimation; 		
	Ptr<videostab::KeypointBasedMotionEstimatorGpu> motionEstBuilder;
	Ptr<videostab::IOutlierRejector> outlierRejector;	

	videostab::RansacParams ransac; 
	videostab::TwoPassStabilizer *twoPassStabilizer;

	IMAGE_SIZE _imageSize;	
	cv::Size _outputSize;
	double _minInlierRatio;

	int _constraintTrackingDistance;

public:
	double Round(double n);

public:	
	vector<Point2f> _movePoint;

	void moveImage(Mat* gMat, int nX, int nY);

	Point2f _baseLinePoint;
		
	vector<cv::Point2f> _subPoint;
	vector<Mat> testImage;		

	struct RecalibrationData
	{
		CString strDscName;
		//CString strDscIp;
		Point2f movePoint;
		Point2f subPoint;
		Point2f baseLinePoint;
		Mat testImage;
	}; vector<RecalibrationData> m_arrRecalibrationData;	
};
