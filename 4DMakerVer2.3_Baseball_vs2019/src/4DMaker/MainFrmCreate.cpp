////////////////////////////////////////////////////////////////////////////////
//
//	MainFrmCreate.cpp : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-06-21
// @ver.1.0	4DMaker
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "4DMaker.h"
#include "MainFrm.h"

#include "ESMRegistry.h"
#include "ESMFileOperation.h"
#include "WndThinFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//------------------------------------------------------------------------------ 
//! @brief		LoadInfo
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CMainFrame::LoadInfo()
{
	//-- GET LOAD ESM Config File (4dmaker.info) from Registry
	CESMRegistry reg;
	LPCTSTR lpszSection;

	//---------------------------------------------
	//-- COMMUNICATION
	//---------------------------------------------
	//lpszSection = REG_ROOT_CONF;
	lpszSection = REG_ROOT_CONF_4DMAKER;
	if(reg.VerifyKey(HKEY_CURRENT_USER, lpszSection))
		if(reg.Open(HKEY_CURRENT_USER, lpszSection))
			if(reg.Read (REG_ROOT_CONFIG, m_strConfig))
			{
				ESMLog(1,_T("Load Config File From Registry : \"%s\""),m_strConfig);
				//-- 2013-05-07 hongsu@esmlab.com
				//-- Check Exist File
				CESMFileOperation fo;
				if(!fo.IsFileExist(m_strConfig))
					m_strConfig = _T("");
			}

	//-- Set From Load Function
	if(!m_strConfig.GetLength())
	{
		/*
		
		//-- 2013-05-07 hongsu@esmlab.com
		//-- Just 1 Time Set on First Running PC
		//TCHAR currentDir[MAX_PATH];
		//GetCurrentDirectory( MAX_PATH, currentDir );
		CString strSourceDir,strSourceDir1;
		//strSourceDir1.Format(_T("%s"),currentDir);
		//CESMFileOperation fo;
		//strSourceDir = fo.GetPreviousFolder(strSourceDir1);
		//strSourceDir1 = fo.GetPreviousFolder(strSourceDir);
		//-- 2013-05-07 hongsu@esmlab.com
		//-- Execute Reg
		//strSourceDir.Format(_T("/s %s\\%s"),strSourceDir1, ESM_4DMAKER_REG);
		ShellExecute(NULL, NULL, _T("regedit"),  strSourceDir, NULL, NULL);
		
				
		//-- 2013-05-07 hongsu@esmlab.com
		//-- Create Working Folder
		CString strWorkingDir, strWorkingDir1,strWorkingDir2;
		strWorkingDir = fo.GetPreviousFolder(BASE_APP_PATH);
		strWorkingDir1 = fo.GetPreviousFolder(strWorkingDir);
		strWorkingDir2 = fo.GetPreviousFolder(strWorkingDir1);
		CreateDirectory(strWorkingDir2, NULL);
		CreateDirectory(strWorkingDir1, NULL);
		strWorkingDir.Format(_T("%s\\config"),strWorkingDir1);
		CreateDirectory(strWorkingDir, NULL);

		//-- 2013-05-07 hongsu@esmlab.com
		//-- Get Dest File 
		strWorkingDir.Format(_T("%s\\%s"),strWorkingDir,ESM_4DMAKER_CONFIG);
		//-- Get Source File
		strSourceDir.Format(_T("%s\\config\\%s"),strSourceDir1,ESM_4DMAKER_CONFIG);
		fo.Copy(strSourceDir, strWorkingDir);

		//-- 2013-05-07 hongsu@esmlab.com
		//-- Create Registry


		//-- 2013-05-07 hongsu@esmlab.com
		//-- Copy File 
		*/
		m_strConfig = _T("C:\\Program Files\\ESMLab\\4DMaker\\config\\4DMaker.info");
		ESMLog(1,_T("Load Default Config File (Non Registry) \"%s\""),m_strConfig);
	}	

	m_ESMOption.Load(m_strConfig);
}
//------------------------------------------------------------------------------ 
//! @brief		CreateLog
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
int CMainFrame::CreateLog(BOOL bNewLog)
{
	if(m_pLogManager)
	{
		delete m_pLogManager;
		m_pLogManager= NULL;
	}

	m_pLogManager = new LogMgr(	ESMGetPath(ESM_PATH_LOG_DATE), 
		m_ESMOption.m_Log.strLogName, 
		m_ESMOption.m_Log.nLimitday, 
		m_ESMOption.m_Log.nVerbosity);

	if(m_pLogManager)
	{
		m_pLogManager->Start();
		ESMLog(1, _T("[ESM] Create Log \"%s\""), m_ESMOption.m_Log.strLogName);	
		SetESMLogFile(m_pLogManager->GetLogFile());
		return true;
	}
	return false;
}

BOOL CMainFrame::CreateProFile(CESMSplashWnd &splash, CWinApp* pApp)
{
	//--------------------------------------------------------------------------------------
	//-- 2009-03-30
	//-- CREATE PROFILE
	//--------------------------------------------------------------------------------------		
	splash.AddTextLine(_T("Creating Profile"));
	CExtCmdProfile * pProfile = g_CmdManager->ProfileGetPtr( pApp->m_pszProfileName );
	ASSERT( pProfile != NULL );
	POSITION posCmd = pProfile->m_cmds.GetStartPosition();
	for( ; posCmd != NULL; )
	{
		UINT nID;
		CExtCmdItem * pCmdItem = NULL;
		pProfile->m_cmds.GetNextAssoc( posCmd, nID, pCmdItem );
		ASSERT( pCmdItem != NULL );
		if(pCmdItem->m_sTipTool.IsEmpty()	|| pCmdItem->m_sTipStatus.IsEmpty())
		{
			CString sTip;
			if(!pCmdItem->m_sMenuText.IsEmpty())
				sTip = (LPCTSTR)pCmdItem->m_sMenuText;
			else if(!pCmdItem->m_sToolbarText.IsEmpty())
				sTip = (LPCTSTR)pCmdItem->m_sToolbarText;
			if(!sTip.IsEmpty())
			{
				if(pCmdItem->m_sTipTool.IsEmpty())
					pCmdItem->m_sTipTool = sTip;
				if(pCmdItem->m_sTipStatus.IsEmpty())
					pCmdItem->m_sTipStatus = sTip;
			}
		}
	}
	splash.AddTextLine( _T("OK.") );
	return TRUE;
}

BOOL CMainFrame::CreateToolBar(CESMSplashWnd &splash)
{
	//-- Create Menu. 2009-03-30
	splash.AddTextLine(_T("Creating MenuBar"));
	m_wndMenuBar.SetMdiWindowPopupName( _T("Window") );
	if(!m_wndMenuBar.Create(NULL, this, ID_VIEW_MENUBAR,	
		WS_CHILD|WS_VISIBLE|CBRS_TOP|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_SIZE_DYNAMIC|CBRS_HIDE_INPLACE))
	{
		ESMLog(0, _T("[ESM] Failed to create menubar"));
		return FALSE;      
	}
	m_wndMenuBar.EnableDocking(CBRS_ALIGN_ANY);
	splash.AddTextLine( _T("OK.") );

	//-- 2012-06-13 hongsu@esmlab.com
	RecentlyFileLoad();

	//Create Standard Toolbar
	splash.AddTextLine(_T("Creating DSC Control Toolbar"));

#ifdef _4DMODEL
	if(!m_wndToolBarStandard.Create(_T("DSC Control"), this, ID_TOOLBARS_CAPTURE) || 
		!m_wndToolBarStandard.LoadToolBar(IDR_TOOLBAR_DSC_MODEL))
	{
		ESMLog(0, _T("[ESM] Failed to create Standard Toolbar"));
		return FALSE;      
	}
#else
	if(!m_wndToolBarStandard.Create(_T("DSC Control"), this, ID_TOOLBARS_CAPTURE) || 
		!m_wndToolBarStandard.LoadToolBar(IDR_TOOLBAR_DSC))
	{
		ESMLog(0, _T("[ESM] Failed to create Standard Toolbar"));
		return FALSE;      
	}
#endif


	m_wndToolBarStandard.EnableDocking(CBRS_ALIGN_ANY);
	splash.AddTextLine( _T("OK.") );

	m_wndMenuBar.ShowWindow(SW_SHOW);
	m_wndToolBarStandard.ShowWindow(SW_SHOW);
	return TRUE;
}

BOOL CMainFrame::CreateMainView(CESMSplashWnd& splash)
{
	splash.AddTextLine( _T("Initializing Main DSC View") );

	//-- 2013-09-04 hongsu@esmlab.com
	//-- Set Splash
	m_wndDSCViewer.m_pSplash = &splash;
	//-- Create Viewer
	if( ! m_wndDSCViewer.Create(NULL,
		NULL,
		AFX_WS_DEFAULT_VIEW,
		CRect( 0, 0, 0, 0 ),
		this,
		AFX_IDW_PANE_FIRST,
		NULL
		)
		)
	{
		ASSERT( FALSE );
		return -1;
	}	

	//-- Init Viewer
	m_wndDSCViewer.InitDSCViewer(m_hWnd);
	
	//-- Close Splash
	m_wndDSCViewer.m_pSplash = NULL;

	//-- 2013-10-08 hongsu@esmlab.com
	//-- Add Remote Control Info 
	m_wndDSCViewer.m_bRCMode = ESMGetValue(ESM_VALUE_NET_MODE);
	//-- Add Init Camera List
	m_wndDSCViewer.CameraListLoad();

	splash.AddTextLine( _T("OK.") );
	return TRUE;
}

BOOL CMainFrame::CreateOutputViewer(CESMSplashWnd &splash)
{
	splash.AddTextLine(_T("Initializing Log Viewer"));
	if(!m_wndOutputBar.Create(_T("Log"), this, ID_VIEW_BAR_OUTPUT) ||
		!m_wndOutput.Create(CESMOutputViewDlg::IDD, &m_wndOutputBar))
	{
		ESMLog(0, _T("Failed to create Output Viewer"));
		return FALSE;      
	}
	m_wndOutput.InitImageFrameWnd();
	HideOutputViewer();
	m_wndOutput.ShowWindow(SW_SHOW);
	m_wndOutputBar.EnableDocking(CBRS_ALIGN_ANY);
	splash.AddTextLine( _T("Creating Log Viewer OK.") );
	return TRUE;
}

BOOL CMainFrame::CreateLiveViewer(CESMSplashWnd &splash)
{
	splash.AddTextLine(_T("Initializing LiveViewer..."));
	if(!m_wndLiveViewBar.Create(_T("Live Viewer"), this, ID_VIEW_BAR_LIVEVIEW) || 		
		!m_wndLiveView.Create(CESMLiveviewDlg::IDD, &m_wndLiveViewBar))
	{
		ESMLog(0, _T("Failed to create LiveViewer"));
		return FALSE;
	}
	m_wndLiveView.InitImageFrameWnd();
	HideLiveViewer();
	m_wndLiveView.ShowWindow(SW_SHOW);
	m_wndLiveViewBar.EnableDocking(CBRS_ALIGN_ANY);
	splash.AddTextLine( _T("Creating LiveViewer OK.") );
	return TRUE;
}

BOOL CMainFrame::CreateFrameViewer(CESMSplashWnd& splash)
{
	splash.AddTextLine(_T("Initializing Frame Viewer..."));
	if(	!m_wndFrameBar.Create(_T("Frame Viewer"),this,ID_VIEW_BAR_FRAME)
		|| !m_wndFrame.Create(CESMEffectFrame::IDD,&m_wndFrameBar))
	{
		ESMLog(1, _T("Failed to create Frame Viewer"));
		return FALSE;
	}
	m_wndFrame.InitImageFrameWnd();
	HideFrameViewer();
	m_wndFrame.ShowWindow(SW_SHOW);
	m_wndFrameBar.EnableDocking(CBRS_ALIGN_ANY);		

	//-- 2013-04-30 hongsu@esmlab.com
	m_wndFrameBar.SetMinSize(CSize(170, 170));	// width	
	splash.AddTextLine( _T("Creating Frame Viewer OK.") );
	return TRUE;
}

//BOOL CMainFrame::CreateFrameViewerEx(CESMSplashWnd& splash)
//{
//	splash.AddTextLine(_T("Initializing Frame ViewerEx..."));
//	if(	!m_wndFrameExBar.Create(_T("Frame ViewerEx"),this,ID_VIEW_BAR_FRAME_EX)
//		|| !m_wndFrameEx.Create(CESMEffectFrame::IDD,&m_wndFrameExBar))
//	{
//		ESMLog(1, _T("Failed to create Frame ViewerEx"));
//		return FALSE;
//	}
//	m_wndFrameEx.InitImageFrameWnd();
//	HideFrameViewerEx();
//	m_wndFrameEx.ShowWindow(SW_SHOW);
//	m_wndFrameExBar.EnableDocking(CBRS_ALIGN_ANY);		
//
//	//-- 2013-04-30 hongsu@esmlab.com
//	m_wndFrameExBar.SetMinSize(CSize(170, 170));	// width	
//	splash.AddTextLine( _T("Creating Frame ViewerEx OK.") );
//	return TRUE;
//}

BOOL CMainFrame::CreateTimeLineEditor(CESMSplashWnd& splash)
{
	splash.AddTextLine(_T("Initializing Timeline Editor ..."));
	if(	!m_wndTimeLineEditorBar.Create(_T("Timeline Editor"),this,ID_VIEW_BAR_TIMELINE)
		|| !m_wndTimeLineEditor.Create(CTimeLineEditor::IDD,&m_wndTimeLineEditorBar))
	{
		ESMLog(1, _T("Failed to create Timeline Editor"));
		return FALSE;
	}
	m_wndTimeLineEditor.InitImageFrameWnd();
	HideTimeLineEditor();
	m_wndTimeLineEditor.ShowWindow(SW_SHOW);
	m_wndTimeLineEditorBar.EnableDocking(CBRS_ALIGN_ANY);		

	//-- 2013-04-30 hongsu@esmlab.com
	m_wndTimeLineEditorBar.SetMinSize(CSize(450, 185));
	splash.AddTextLine( _T("Creating Timeline Editor OK.") );

	return TRUE;
}


BOOL CMainFrame::CreatePropertyViewer(CESMSplashWnd& splash)
{
	splash.AddTextLine(_T("Initializing Property Viewer..."));
	if(	!m_wndPropertyBar.Create(_T("Property"),this,ID_VIEW_BAR_PROPERTY) ||
		!m_wndProperty.Create(CESMPropertyView::IDD, &m_wndPropertyBar))
	{
		ESMLog(1, _T("Failed to create Property Viewer"));
		return FALSE;
	}
	m_wndProperty.InitImageFrameWnd();
	HidePropertyViewer();
	m_wndPropertyBar.EnableDocking(CBRS_ALIGN_ANY);			
	
	//-- 2012-06-18 hongsu@esmlab.com	
	CExtPropertyGridTipBar * pWndTipBar = STATIC_DOWNCAST( CExtPropertyGridTipBar, m_wndProperty.m_pPropertyCtrl->GetChildByRTC(RUNTIME_CLASS(CExtPropertyGridTipBar)));
	if(!pWndTipBar)	return FALSE;
	pWndTipBar->ShowWindow(SW_HIDE);

	CExtPropertyGridComboBoxBar * pWndCmbBar =	STATIC_DOWNCAST(CExtPropertyGridComboBoxBar,m_wndProperty.m_pPropertyCtrl->GetChildByRTC(RUNTIME_CLASS(CExtPropertyGridComboBoxBar)));
	if(!pWndCmbBar)	return FALSE;
	pWndCmbBar->ShowWindow(SW_HIDE);

	splash.AddTextLine( _T("Creating Property Viewer OK.") );
	return TRUE;
}

BOOL CMainFrame::CreateNetworkViewer(CESMSplashWnd& splash)
{
	splash.AddTextLine(_T("Initializing Main Network Viewer..."));
	if(	!m_wndNetworkBar.Create(_T("Main Network"),this,ID_VIEW_BAR_NETWORK) ||
		!m_wndNetwork.Create(CESMNetworkDlg::IDD, &m_wndNetworkBar))
	{
		ESMLog(1, _T("Failed to create Main Network Viewer"));
		return FALSE;
	}
	m_wndNetwork.InitImageFrameWnd();
	HideNetworkViewer();
	m_wndNetworkBar.EnableDocking(CBRS_ALIGN_ANY);			
	
	//-- Load Ini file ( \config\4d_connection.info )
	m_wndNetwork.LoadInfo();

	splash.AddTextLine( _T("Creating Main Network Viewer OK.") );
	return TRUE;
}

 BOOL CMainFrame::CreatePointListViewer(CESMSplashWnd &splash)
{
	splash.AddTextLine(_T("Initializing PointLive Viewer..."));
	if(	!m_wndPointListBar.Create(_T("PointList Viewer"),this,ID_VIEW_BAR_POINTLIST) ||
		!m_wndPointList.Create(CESMPointListDlg::IDD, &m_wndPointListBar))
	{
		ESMLog(1, _T("Failed to create PointLive Viewer"));
		return FALSE;
	}
	m_wndPointList.InitImageFrameWnd();
	HidePointListViewer();
	m_wndPointListBar.EnableDocking(CBRS_ALIGN_ANY);

	splash.AddTextLine( _T("Creating PointList Viewer OK.") );
	return TRUE;
}

 BOOL CMainFrame::CreateFileMergeViewer(CESMSplashWnd &splash)
 {
	 splash.AddTextLine(_T("Initializing FileMerge Viewer..."));
	 if(	!m_wndFileMergeViewerBar.Create(_T("FileMerge Viewer"),this,ID_VIEW_BAR_FILEMERGE) ||
		 !m_wndFIleMergeViewer.Create(CESMFileMergeDlg::IDD, &m_wndFileMergeViewerBar))
	 {
		 ESMLog(1, _T("Failed to create FileMerge Viewer"));
		 return FALSE;
	 }
	 m_wndEffectFrameViewer.InitImageFrameWnd();
	 HideFileMergeViewer();
	 m_wndFileMergeViewerBar.EnableDocking(CBRS_ALIGN_ANY);

	 splash.AddTextLine( _T("Creating FileMerge Viewer OK.") );
	 return TRUE;
 }

 BOOL CMainFrame::CreateEffectEditor(CESMSplashWnd &splash)
 {
	 splash.AddTextLine(_T("Initializing Effect Editor..."));
	 if( !m_wndEffectEditorBar.Create(_T("Effect Editor"),this,ID_VIEW_BAR_EFFECTEDITOR) ||
		 !m_wndEffectEditor.Create(CESMEffectEditor::IDD, &m_wndEffectEditorBar))
	 {
		 ESMLog(1, _T("Failed to Create EffectEditor"));
		 return FALSE;
	 }
	 m_wndEffectEditor.InitImageFrameWnd();
	 HideEffectEditor();
	 m_wndEffectEditorBar.EnableDocking(CBRS_ALIGN_ANY);

	 //-- 2014-07-16 hongsu@esmlab.com
	 //-- Minimum Size
	 m_wndEffectEditorBar.SetMinSize(CSize(700, 470));
	 splash.AddTextLine( _T("Creating Create Effect Editor OK.") );
	 return TRUE;
 }


 BOOL CMainFrame::CreateEffectFrameViewer(CESMSplashWnd& splash)
 {
	 splash.AddTextLine(_T("Initializing Effect Frame Viewer..."));
	 if(	!m_wndEffectFrameViewerBar.Create(_T("Effect Frame Viewer"),this,ID_VIEW_BAR_EFFECT_FRAME)
		 || !m_wndEffectFrameViewer.Create(CESMEffectFrame::IDD,&m_wndEffectFrameViewerBar))
	 {
		 ESMLog(1, _T("Failed to create Effect Frame Viewer"));
		 return FALSE;
	 }
	 m_wndEffectFrameViewer.InitImageFrameWnd();
	 HideEffectFrameViewer();
	 m_wndEffectFrameViewer.ShowWindow(SW_SHOW);
	 m_wndEffectFrameViewerBar.EnableDocking(CBRS_ALIGN_ANY);		

	 //-- 2013-04-30 hongsu@esmlab.com
	 m_wndEffectFrameViewerBar.SetMinSize(CSize(170, 170));	// width	
	 splash.AddTextLine( _T("Creating Effect Frame Viewer OK.") );
	 return TRUE;
 }


 BOOL CMainFrame::CreateEffectPreview(CESMSplashWnd& splash)
 {
	 splash.AddTextLine(_T("Initializing Effect Preview..."));
	 if(	!m_wndEffectPreviewBar.Create(_T("Effect Preview"),this,ID_VIEW_BAR_EFFECT_PREVIEW)
		 || !m_wndEffectPreview.Create(CESMEffectPreview::IDD,&m_wndEffectPreviewBar))
	 {
		 ESMLog(1, _T("Failed to create Effect Preview"));
		 return FALSE;
	 }
	 m_wndEffectFrameViewer.InitImageFrameWnd();
	 HideEffectPreview();
	 m_wndEffectPreview.ShowWindow(SW_SHOW);
	 m_wndEffectPreviewBar.EnableDocking(CBRS_ALIGN_ANY);		

	 //-- 2013-04-30 hongsu@esmlab.com
	 m_wndEffectPreviewBar.SetMinSize(CSize(170, 170));	// width	
	 splash.AddTextLine( _T("Creating Effect Preview.") );
	 return TRUE;
 }

 BOOL CMainFrame::CreateNetworkExViewer(CESMSplashWnd& splash)
 {
	 splash.AddTextLine(_T("Initializing Sub Network Viewer..."));
	 if(	!m_wndNetworkExBar.Create(_T("Sub Network"),this,ID_VIEW_BAR_NETWORKEX) ||
		 !m_wndNetworkEx.Create(CESMNetworkExDlg::IDD, &m_wndNetworkExBar))
	 {
		 ESMLog(1, _T("Failed to create Sub Network Viewer"));
		 return FALSE;
	 }
	 m_wndNetworkEx.InitImageFrameWnd();
	 HideNetworkExViewer();
	 m_wndNetworkExBar.EnableDocking(CBRS_ALIGN_ANY);			

	 //-- Load Ini file ( \config\4d_connection.info )
	 m_wndNetworkEx.LoadInfo();

	 splash.AddTextLine( _T("Creating Sub Network Viewer OK.") );
	 return TRUE;
 }

 BOOL CMainFrame::CreateDashBoardViewer(CESMSplashWnd& splash)
 {
	 splash.AddTextLine(_T("Initializing Dashboard Viewer..."));
	 if(	!m_wndDashBoardBar.Create(_T("Dashboard"),this,ID_VIEW_BAR_DASHBOARD) ||
		 !m_wndDashBoard.Create(CESMDashBoardDlg::IDD, &m_wndDashBoardBar))
	 {
		 ESMLog(1, _T("Failed to create Dashboard Viewer"));
		 return FALSE;
	 }
	 m_wndDashBoard.InitImageFrameWnd();
	 HideDashBoardViewer();
	 m_wndDashBoardBar.EnableDocking(CBRS_ALIGN_ANY);			

	 //-- Load Ini file ( \config\4d_connection.info )
	 //m_wndDashBoard.LoadInfo();

	 splash.AddTextLine( _T("Creating Dashboard Viewer OK.") );
	 return TRUE;
 }

 BOOL CMainFrame::CreateMovieListViewer(CESMSplashWnd& splash)
 {
	 splash.AddTextLine(_T("Initializing MovieList Viewer..."));
	 if(	!m_wndMovieListBar.Create(_T("MovieList Viewer"),this,ID_VIEW_BAR_MOVIELIST) ||
		 !m_wndMovieList.Create(CESMMovieListDlg::IDD, &m_wndMovieListBar))
	 {
		 ESMLog(1, _T("Failed to create MovieList Viewer"));
		 return FALSE;
	 }
	 m_wndMovieList.InitImageFrameWnd();
	 HideMovieListViewer();
	 m_wndMovieListBar.EnableDocking(CBRS_ALIGN_ANY);			

	 //-- Load Ini file ( \config\4d_connection.info )
	 m_wndMovieList.LoadInfo();

	 splash.AddTextLine( _T("Creating MovieList Viewer OK.") );
	 return TRUE;
 }

 BOOL CMainFrame::CreateTimeLineProcessor(CESMSplashWnd& splash)
 {
	 splash.AddTextLine(_T("Initializing TimeLine Processor ..."));
	 if(	!m_wndTimeLineProcessorBar.Create(_T("TimeLine Processor"),this,ID_VIEW_BAR_TIMELINE_PROCESSOR)
		 || !m_wndTimeLineProcessor.Create(CTimeLineProcessor::IDD,&m_wndTimeLineProcessorBar))
	 {
		 ESMLog(1, _T("Failed to create TimeLine Editor"));
		 return FALSE;
	 }
	 m_wndTimeLineProcessor.InitImageFrameWnd();
	 HideTimeLineProcessorr();
	 m_wndTimeLineProcessor.ShowWindow(SW_SHOW);
	 m_wndTimeLineProcessorBar.EnableDocking(CBRS_ALIGN_ANY);		

	 //-- 2013-04-30 hongsu@esmlab.com
	 m_wndTimeLineProcessorBar.SetMinSize(CSize(450, 150));
	 splash.AddTextLine( _T("Creating TimeLine Processor OK.") );

	 return TRUE;
 }