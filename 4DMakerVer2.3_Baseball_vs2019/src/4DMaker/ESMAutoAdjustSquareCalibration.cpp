#include "StdAfx.h"
#include "ESMAutoAdjustSquareCalibration.h"

CESMAutoAdjustSquareCalibration::CESMAutoAdjustSquareCalibration(void)
{
	Set3DSquarePoint();
	//wgkim 191030
	_sSize = cv::Size(1920, 1080);

	_nLinetickness = 3;
	SetInitKZone3DPoints();
	SetCalibrationBarObjectPoints();

	_nFirstStandardDscIndex = 0;

	//wgkim 191030
	pFileOut = ofstream("F:\\CameraParameter.txt");
}

//CESMAutoAdjustSquareCalibration::CESMAutoAdjustSquareCalibration(IMAGE_SIZE eImageSize)
//{
//	Set3DSquarePoint();
//
//	if(eImageSize == FHD_SIZE)
//		_sSize = Size(1920, 1080);
//	else if(eImageSize == UHD_SIZE)
//		_sSize = Size(3840, 2160);
//	else
//		_sSize = Size(1920, 1080);
//}


CESMAutoAdjustSquareCalibration::~CESMAutoAdjustSquareCalibration(void)
{
	ClearVectors();
}


Mat CESMAutoAdjustSquareCalibration::GetHomographyCameraToCamera(vector<AdjustSquareInfo*> vecAdjustSquareInfo, int index)
{
	if(index == 0)
		return findHomography(vecAdjustSquareInfo[vecAdjustSquareInfo.size()-1]->vecPtSquarePoints, vecAdjustSquareInfo[index]->vecPtSquarePoints);//RANSAC, 0.001);
	else
		return findHomography(vecAdjustSquareInfo[index-1]->vecPtSquarePoints, vecAdjustSquareInfo[index]->vecPtSquarePoints);//RANSAC, 0.001);
}

Point2f CESMAutoAdjustSquareCalibration::CalcPointLocation(Mat mHomography,Point2f ptCurrentPoint)
{
	Mat_<double> mCurrentPointMat = (
		Mat_<double>(3,1) << 
		(double)ptCurrentPoint.x, (double)ptCurrentPoint.y, 1.
		);
	
	Mat mResultPointMat = mHomography * mCurrentPointMat;

	ptCurrentPoint = Point2f(
		mResultPointMat.at<double>(0)/mResultPointMat.at<double>(2),
		mResultPointMat.at<double>(1)/mResultPointMat.at<double>(2)
		);

	return ptCurrentPoint;
}

//void CESMAutoAdjustSquareCalibration::PointInterpolationAsLine(int nPointIndex)
//{
//	Point2f ptStart;
//	Point2f ptEnd;
//	Point2f ptWeight;
//	int nVecSize = _vVecPtSquarePoints.size();
//
//	int nDivLine = 12;
//
//	for(int j = 0; j < nDivLine; j++)
//	{
//		ptStart = _vVecPtSquarePoints[(nVecSize/nDivLine)*j].at(nPointIndex);
//		ptEnd	= _vVecPtSquarePoints[(nVecSize/nDivLine)*(j+1)-1].at(nPointIndex);
//		ptWeight= Point2f();
//
//		for(int i = (nVecSize/nDivLine)*j; i < (nVecSize/nDivLine)*(j+1); i++)
//		{
//			ptWeight.x += (ptEnd.x - ptStart.x)/(float)(nVecSize/nDivLine);
//			ptWeight.y += (ptEnd.y - ptStart.y)/(float)(nVecSize/nDivLine);
//
//			_vVecPtAfterSquarePoints.push_back(_vVecPtSquarePoints[i]);
//
//			_vVecPtAfterSquarePoints[i].at(nPointIndex).x = ptStart.x + ptWeight.x;
//			_vVecPtAfterSquarePoints[i].at(nPointIndex).y = ptStart.y + ptWeight.y;
//		}
//	}
//}
//
//void CESMAutoAdjustSquareCalibration::SquareCalibrationAsLine()
//{
//	if(_vVecPtSquarePoints.empty())
//	{
//		printf("SquarePoint is not exist\n");
//		return;
//	}
//
//	PointInterpolationAsLine(0);
//	PointInterpolationAsLine(1);
//	PointInterpolationAsLine(2);
//	PointInterpolationAsLine(3);
//}
//
//void CESMAutoAdjustSquareCalibration::PointApproximationAsBezier(int nPointIndex)
//{
//	Point2f ptStart;
//	Point2f ptEnd;
//	Point2f ptWeight;
//	int nVecSize = _vVecPtSquarePoints.size();
//
//	int nDivLine = 1;
//
//	for(int j = 0; j < nDivLine; j++)
//	{
//		ptStart = _vVecPtSquarePoints[(nVecSize/nDivLine)*j].at(nPointIndex);
//		ptEnd	= _vVecPtSquarePoints[(nVecSize/nDivLine)*(j+1)-1].at(nPointIndex);
//		ptWeight= Point2f();
//
//		for(int i = (nVecSize/nDivLine)*j; i < (nVecSize/nDivLine)*(j+1); i++)
//		{
//			ptWeight.x += (ptEnd.x - ptStart.x)/(float)(nVecSize/nDivLine);
//			ptWeight.y += (ptEnd.y - ptStart.y)/(float)(nVecSize/nDivLine);
//
//			_vVecPtAfterSquarePoints.push_back(_vVecPtSquarePoints[i]);
//
//			_vVecPtAfterSquarePoints[i].at(nPointIndex).x = ptStart.x + ptWeight.x;
//			_vVecPtAfterSquarePoints[i].at(nPointIndex).y = ptStart.y + ptWeight.y;
//		}
//	}
//}
//
//void CESMAutoAdjustSquareCalibration::SquareCalibrationAsBezier()
//{
//	if(_vVecPtSquarePoints.empty())
//	{
//		printf("SquarePoint is not exist\n");
//		return;
//	}
//
//	PointInterpolationAsLine(0);
//	PointInterpolationAsLine(1);
//	PointInterpolationAsLine(2);
//	PointInterpolationAsLine(3);
//}

void CESMAutoAdjustSquareCalibration::CalibrationUsingSquarePoints(string strDscId)
{
	int nIndex = GetAdjustIndexAt(strDscId);
	if(nIndex == -1)
		return;

	//wgkim 191023
	vector<Point3f> vec3DSquarePoints = _vecAdjustSquareInfo[nIndex]->vecPtInit3DSquarePoint;
	if (vec3DSquarePoints.at(0).x == 0 && vec3DSquarePoints.at(0).y == 0 &&
		vec3DSquarePoints.at(1).x == 0 && vec3DSquarePoints.at(1).y == 0 &&
		vec3DSquarePoints.at(2).x == 0 && vec3DSquarePoints.at(2).y == 0 &&
		vec3DSquarePoints.at(3).x == 0 && vec3DSquarePoints.at(3).y == 0)
		return;

	_vecAdjustSquareInfo[nIndex]->vecPtProjectedNormalVector 
		= ChangeCoordinateFromWorldToCamera(strDscId, _vecPtNormalVector);

	//if( _vecAdjustSquareInfo[nIndex]->vecPtProjectedNormalVector.at(1).y > 
	//	_vecAdjustSquareInfo[nIndex]->vecPtProjectedNormalVector.at(0).y)
	//{
	//	_vecPtNormalVector[1].z = -_vecPtNormalVector[1].z;
	//	_vecAdjustSquareInfo[nIndex]->vecPtProjectedNormalVector 
	//		= ChangeCoordinateFromWorldToCamera(strDscId, _vecPtNormalVector);
	//}

	RigidTransformationUsingSquarePoints(strDscId);
}

void CESMAutoAdjustSquareCalibration::SetFirstStandardCameraId(string strDscId)
{
	_strFirstStandardDscId = strDscId;

	int nIndex = GetAdjustIndexAt(strDscId);
	if(nIndex == -1)
		return;
	_nFirstStandardDscIndex = nIndex;
}

void CESMAutoAdjustSquareCalibration::PreviewCalibration(Mat& mFrame, string strDscId)
{
	int nIndex = GetAdjustIndexAt(strDscId);
	if(nIndex == -1)
		return;

	DrawObject(mFrame, strDscId);
	
	warpAffine(mFrame, mFrame, _vecAdjustSquareInfo[nIndex]->mRotMatrix, mFrame.size());

	Point2f ptStandardMovePoint = _vecAdjustSquareInfo[_nFirstStandardDscIndex]->ptCenterPoint;
	Point2f ptCurrentMovePoint = _vecAdjustSquareInfo[nIndex]->ptCenterPoint;

	Point2f ptMovePoint = ptStandardMovePoint - ptCurrentMovePoint;
	CpuMoveImage(&mFrame, ptMovePoint.x, ptMovePoint.y);
}

vector<Point2f> CESMAutoAdjustSquareCalibration::ChangeCoordinateFromWorldToCamera
	(string strDscId, vector<Point3f> vecPtObjectPoints)
{
	int nIndex = GetAdjustIndexAt(strDscId);
	if(nIndex == -1)
		return vector<Point2f>();

	vector<Point2f> vecPtResultPoints;

	float fFocalLength = (55. / 17.30) * _sSize.width;

	_mCameraParameter = (Mat_<float>(3, 3) <<
		fFocalLength, 0, _sSize.width / 2,
		0, fFocalLength, _sSize.height / 2,
		0, 0, 1);	

	//_mCameraParameter = (Mat_<float>(3,3) <<
	//	_sSize.width,	0,				_sSize.width/2,
	//	0,				_sSize.width,	_sSize.height/2,
	//	0,				0,				1);
	_mDistCoeffs = Mat::zeros(4,1, cv::DataType <double>::type);

	solvePnP(
		_vecAdjustSquareInfo[nIndex]->vecPtInit3DSquarePoint,
		_vecAdjustSquareInfo[nIndex]->vecPtSquarePoints,
		_mCameraParameter,
		_mDistCoeffs,
		_mRotationMatrix,
		_mTranslationMatrix,
		false, SOLVEPNP_ITERATIVE);

	projectPoints(
		vecPtObjectPoints,
		_mRotationMatrix,
		_mTranslationMatrix,
		_mCameraParameter,
		_mDistCoeffs,
		vecPtResultPoints);

	Mat mRodrigues;
	cv::Rodrigues(_mRotationMatrix, mRodrigues);	

	transpose(_mRotationMatrix, _mRotationMatrix);
	transpose(_mTranslationMatrix, _mTranslationMatrix);

	cout << "NormalVector : " << _vecPtProjectedNormalVector << endl;
	cout << "RotatMatrix : " << _mRotationMatrix << endl;
	cout << "TransMatrix : " << _mTranslationMatrix << endl;

	//wgkim 191030
	Mat mExtParmeter = (Mat_<double>(4, 4) <<
		mRodrigues.at<double>(0, 0), mRodrigues.at<double>(0, 1), mRodrigues.at<double>(0, 2), _mTranslationMatrix.at<double>(0, 0),
		mRodrigues.at<double>(1, 0), mRodrigues.at<double>(1, 1), mRodrigues.at<double>(1, 2), _mTranslationMatrix.at<double>(0, 1),
		mRodrigues.at<double>(2, 0), mRodrigues.at<double>(2, 1), mRodrigues.at<double>(2, 2), _mTranslationMatrix.at<double>(0, 2),
		0, 0, 0, 1);
	Mat mIntParameter= (Mat_<double>(4, 4) <<
		(double)fFocalLength, 0, _sSize.width / 2, 0,
		0, (double)fFocalLength, _sSize.height / 2, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);
	
	Mat mExtInv = mExtParmeter.inv();
	Mat mIntInv = mIntParameter.inv();
	
	Mat mExtInv_Mul_IntInv = mExtInv * mIntInv;

	if (pFileOut.is_open())
	{
		pFileOut << nIndex + 1 << '\t' << "\'" << strDscId << "\'" << '\n';
		pFileOut << mExtInv_Mul_IntInv.at<double>(0, 0) << '\t' << mExtInv_Mul_IntInv.at<double>(0, 1) << '\t' << mExtInv_Mul_IntInv.at<double>(0, 2) << '\t' << mExtInv_Mul_IntInv.at<double>(0, 3) << '\n';
		pFileOut << mExtInv_Mul_IntInv.at<double>(1, 0) << '\t' << mExtInv_Mul_IntInv.at<double>(1, 1) << '\t' << mExtInv_Mul_IntInv.at<double>(1, 2) << '\t' << mExtInv_Mul_IntInv.at<double>(1, 3) << '\n';
		pFileOut << mExtInv_Mul_IntInv.at<double>(2, 0) << '\t' << mExtInv_Mul_IntInv.at<double>(2, 1) << '\t' << mExtInv_Mul_IntInv.at<double>(2, 2) << '\t' << mExtInv_Mul_IntInv.at<double>(2, 3) << '\n';
		pFileOut << mExtInv_Mul_IntInv.at<double>(3, 0) << '\t' << mExtInv_Mul_IntInv.at<double>(3, 1) << '\t' << mExtInv_Mul_IntInv.at<double>(3, 2) << '\t' << mExtInv_Mul_IntInv.at<double>(3, 3) << '\n';
		pFileOut << '\n';
	}

	return vecPtResultPoints;
}

vector<Point2f> CESMAutoAdjustSquareCalibration::ChangeCoordinateFromWorldToCameraAboutReverse
	(string strDscId, vector<Point3f> vecPtObjectPoints)
{
	int nIndex = GetAdjustIndexAt(strDscId);
	if(nIndex == -1)
		return vector<Point2f>();

	vector<Point2f> vecPtResultPoints;

	_mCameraParameter = (Mat_<float>(3,3) <<
		_sSize.width,	0,				_sSize.width/2,
		0,				_sSize.width,	_sSize.height/2,
		0,				0,				1);
	_mDistCoeffs = Mat::zeros(4,1, cv::DataType <float>::type);

	solvePnP(
		_vecAdjustSquareInfo[nIndex]->vecPtInit3DSquarePoint,
		_vecAdjustSquareInfo[nIndex]->vecPtSquarePoints,
		_mCameraParameter,
		_mDistCoeffs,
		_mRotationMatrix,
		_mTranslationMatrix,
		false, SOLVEPNP_ITERATIVE);

	//Mat R;
	//Rodrigues(_mRotationMatrix, R);

	//double SwapAxes [9] = { 1, 0, 0, 0, 0, 1, 0, -1, 0 };
	//Mat mSwapAxes = Mat(3, 3, R.type(), SwapAxes );
	//R *= mSwapAxes ;

	//double mult[9] = { 1, -1, 1, 1, -1, 1,-1, 1, -1 };
	//Mat FlipAxes = Mat(3, 3, R.type(), mult);
	//R = R.mul(FlipAxes);

	//transpose(_mTranslat
	//Mat tNormTrans;ionMatrix, tNormTrans);
	//Vec3d forward(0, 0, 1);
	//Vec3d T(tNormTrans);
	//Vec3d tnorm = T / norm(_mTranslationMatrix);	
	//Vec3d axis = tnorm.cross(forward);
	//double angle = -2 * acos(forward.dot(tnorm));

	//Mat R_(angle*axis);
	//Rodrigues(R_, R_);
	//R = R_*R;

	//Rodrigues(R, _mRotationMatrix);

	projectPoints(
		vecPtObjectPoints,
		_mRotationMatrix,
		_mTranslationMatrix,
		_mCameraParameter,
		_mDistCoeffs,
		vecPtResultPoints);

	transpose(_mRotationMatrix, _mRotationMatrix);
	transpose(_mTranslationMatrix, _mTranslationMatrix);

	cout << "NormalVector : " << _vecPtProjectedNormalVector << endl;
	cout << "RotatMatrix : " << _mRotationMatrix << endl;
	cout << "TransMatrix : " << _mTranslationMatrix << endl;

	return vecPtResultPoints;
}

void CESMAutoAdjustSquareCalibration::RigidTransformationUsingSquarePoints(string strDscId)
{
	int nIndex = GetAdjustIndexAt(strDscId);
	if(nIndex == -1)
		return;

	RotateImage(nIndex);

	//Point2f ptStandardMovePoint = _vecAdjustSquareInfo[0]->ptCenterPoint;
	//Point2f ptCurrentMovePoint = _vecAdjustSquareInfo[nFrameIndex]->ptCenterPoint;

	//Point2f ptMovePoint = ptStandardMovePoint - ptCurrentMovePoint;

	//CpuMoveImage(&mFrame, ptMovePoint.x, ptMovePoint.y);
	//CpuMakeMargin	(&mFrame, ptMovePoint.x, ptMovePoint.y);
}

void CESMAutoAdjustSquareCalibration::ClearVectors()
{
	_vecPtProjectedNormalVector.clear();

	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		AdjustSquareInfo* pAdjustSquareInfo = _vecAdjustSquareInfo[i];
		delete pAdjustSquareInfo;
	}
	_vecAdjustSquareInfo.clear();
}

Point2f CESMAutoAdjustSquareCalibration::GetSquareCentroidPoint(vector<vector<Point2f>> vecPtSquarePoints, int nFrameIndex)
{
	Point2f ptCenterPoint;

	float nArea = 0;
	float nCenterX = 0;
	float nCenterY = 0;
	for (int i = 0; i < vecPtSquarePoints.at(0).size(); i++)
	{
		float nX1, nX2, nY1, nY2;

		if(i == vecPtSquarePoints.at(0).size() - 1)
		{
			nX1 = vecPtSquarePoints[nFrameIndex].at(i).x;
			nX2 = vecPtSquarePoints[nFrameIndex].at(0).x;
			nY1 = vecPtSquarePoints[nFrameIndex].at(i).y;
			nY2 = vecPtSquarePoints[nFrameIndex].at(0).y;
		}
		else
		{
			nX1 = vecPtSquarePoints[nFrameIndex].at(i).x;
			nX2 = vecPtSquarePoints[nFrameIndex].at(i+1).x;
			nY1 = vecPtSquarePoints[nFrameIndex].at(i).y;
			nY2 = vecPtSquarePoints[nFrameIndex].at(i+1).y;
		}
		nArea += nX1 * nY2;
		nArea -= nY1 * nX2;

		nCenterX += ((nX1+nX2) * ((nX1*nY2) - (nX2*nY1)));
		nCenterY += ((nY1+nY2) * ((nX1*nY2) - (nX2*nY1)));
	}

	nArea /= 2.0;
	nArea = abs(nArea);

	ptCenterPoint.x = nCenterX / (6.0 * nArea);
	ptCenterPoint.y = nCenterY / (6.0 * nArea);

	return ptCenterPoint;
}

void CESMAutoAdjustSquareCalibration::RotateImage(int nFrameIndex)
{
	//if( _vecAdjustSquareInfo[nFrameIndex]->vecPtProjectedNormalVector.at(1).y > 
	//	_vecAdjustSquareInfo[nFrameIndex]->vecPtProjectedNormalVector.at(0).y)
	//{
	//	vector<Point3f> vecPtNormalVector;

	//	int nMaxRange = 100;
	//	int nLocation	= nMaxRange / 2;
	//	vecPtNormalVector.push_back( Point3f(nLocation,	 nLocation,	0));
	//	vecPtNormalVector.push_back( Point3f(nLocation,	 nLocation,	-nMaxRange));

	//	//Point2f ptSwapPoint;
	//	//ptSwapPoint = _vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[0];
	//	//_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[0] = _vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[1];
	//	//_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[1] = ptSwapPoint;

	//	//ptSwapPoint = _vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[1];
	//	//_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[1] = _vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[2];
	//	//_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[2] = ptSwapPoint;

	//	//ptSwapPoint = _vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[2];
	//	//_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[2] = _vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[3];
	//	//_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[3] = ptSwapPoint;

	//	//ptSwapPoint = _vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[3];
	//	//_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[3] = _vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[0];
	//	//_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[0] = ptSwapPoint;

	//	vector<Point2f> ptTempPoint;
	//	ptTempPoint.push_back(_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[2]);
	//	ptTempPoint.push_back(_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[3]);
	//	ptTempPoint.push_back(_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[0]);
	//	ptTempPoint.push_back(_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[1]);
	//	ptTempPoint.push_back(_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[2]);
	//	ptTempPoint.push_back(_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[3]);
	//	ptTempPoint.push_back(_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[3]);
	//	ptTempPoint.push_back(_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[0]);
	//	ptTempPoint.push_back(_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[0]);
	//	ptTempPoint.push_back(_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints[0]);

	//	_vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints = ptTempPoint;

	//	_vecAdjustSquareInfo[nFrameIndex]->vecPtProjectedNormalVector 
	//		= ChangeCoordinateFromWorldToCamera(nFrameIndex, _vecPtNormalVector);
	//}

	Point2f ptAngleVector = _vecAdjustSquareInfo[nFrameIndex]->vecPtProjectedNormalVector.at(1)
						  - _vecAdjustSquareInfo[nFrameIndex]->vecPtProjectedNormalVector.at(0);

	double dNorm	= norm(ptAngleVector);
	double dRad		= atan2(ptAngleVector.x, ptAngleVector.y);
	double dDegree;
	if( _vecAdjustSquareInfo[nFrameIndex]->vecPtProjectedNormalVector.at(1).y > 
		_vecAdjustSquareInfo[nFrameIndex]->vecPtProjectedNormalVector.at(0).y)
	{
		dDegree = 360. - dRad*180. / CV_PI;
		if(dDegree >= 360)
		{
			dDegree -= 360;
		}
	}
	else
	{
		dDegree = 180. - dRad*180. / CV_PI;
	}

	if(nFrameIndex == _nFirstStandardDscIndex)
		_dFirstNorm = dNorm;

	_vecAdjustSquareInfo[nFrameIndex]->dNorm = /*_dFirstNorm/*/dNorm;
	_vecAdjustSquareInfo[nFrameIndex]->dAngle = dDegree;

	_vecAdjustSquareInfo[nFrameIndex]->mRotMatrix = getRotationMatrix2D(
		_vecAdjustSquareInfo[nFrameIndex]->ptCenterPoint,
		dDegree, _dFirstNorm/_vecAdjustSquareInfo[nFrameIndex]->dNorm );

	cout << "Norm : " << _vecAdjustSquareInfo[nFrameIndex]->dNorm << endl;
	cout << "degree : " << dDegree << endl;
	cout << "scale : " << norm(ptAngleVector) << endl;
}

void CESMAutoAdjustSquareCalibration::RotateImage(string strDscId)
{
	int nIndex = GetAdjustIndexAt(strDscId);
	if(nIndex == -1)
		return;

	Point2f ptAngleVector = _vecAdjustSquareInfo[nIndex]->vecPtProjectedNormalVector.at(1)
						  - _vecAdjustSquareInfo[nIndex]->vecPtProjectedNormalVector.at(0);

	double dNorm	= norm(ptAngleVector);
	double dRad		= atan2(ptAngleVector.x, ptAngleVector.y);
	double dDegree;
	if( _vecAdjustSquareInfo[nIndex]->vecPtProjectedNormalVector.at(1).y > 
		_vecAdjustSquareInfo[nIndex]->vecPtProjectedNormalVector.at(0).y)
	{
		dDegree = 360. - dRad*180. / CV_PI;
		if(dDegree >= 360)
		{
			dDegree -= 360;
		}
	}
	else
	{
		dDegree = 180. - dRad*180. / CV_PI;
	}

	if(nIndex == 0)
		_dFirstNorm = dNorm;

	_vecAdjustSquareInfo[nIndex]->dNorm = /*_dFirstNorm/*/dNorm;
	_vecAdjustSquareInfo[nIndex]->dAngle = dDegree;

	_vecAdjustSquareInfo[nIndex]->mRotMatrix = getRotationMatrix2D(
		_vecAdjustSquareInfo[nIndex]->ptCenterPoint,
		dDegree, _dFirstNorm/_vecAdjustSquareInfo[nIndex]->dNorm );

	cout << "Norm : " << _vecAdjustSquareInfo[nIndex]->dNorm << endl;
	cout << "degree : " << dDegree << endl;
	cout << "scale : " << norm(ptAngleVector) << endl;
}

void CESMAutoAdjustSquareCalibration::CpuMoveImage(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

void CESMAutoAdjustSquareCalibration::CpuMakeMargin(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;

	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

void CESMAutoAdjustSquareCalibration::Normalization3DSquarePoint(vector<Point3f>& ptSquarePoint, int nMaxRange)
{
	float fMin_x = (float)_sSize.width;
	float fMin_y = (float)_sSize.height;
	float fMax_x = 0.f;
	float fMax_y = 0.f;

	for(int i = 0; i < ptSquarePoint.size(); i++)
	{
		if (fMax_x < ptSquarePoint[i].x)
			fMax_x = ptSquarePoint[i].x;
		if (fMax_y < ptSquarePoint[i].y)
			fMax_y = ptSquarePoint[i].y;
		if (fMin_x > ptSquarePoint[i].x)
			fMin_x = ptSquarePoint[i].x;
		if (fMin_y > ptSquarePoint[i].y)
			fMin_y = ptSquarePoint[i].y;
	}

	//wgkim 191023
	if ((fMax_x == 0 && fMin_x == 0) ||
		(fMax_y == 0 && fMin_y == 0))
	{
		ptSquarePoint = vector<Point3f>(4);
		return;
	}

	float fRange;
	float fMargin_X = 0.f;
	float fMargin_Y = 0.f;

	if( (fMax_x - fMin_x) > (fMax_y - fMin_y))
	{
		fRange = nMaxRange/(fMax_x - fMin_x);
		fMargin_Y = (nMaxRange-((fMax_y - fMin_y)*(fRange)))/2;
	}
	else
	{
		fRange = nMaxRange/(fMax_y - fMin_y);
		fMargin_X = (nMaxRange-((fMax_x - fMin_x)*(fRange)))/2;
	}

	for(int i = 0; i < ptSquarePoint.size(); i++)
	{
		ptSquarePoint[i].x -= fMin_x;
		ptSquarePoint[i].y -= fMin_y;

		ptSquarePoint[i].x *= fRange;
		ptSquarePoint[i].y *= fRange;

		ptSquarePoint[i].x += fMargin_X;
		ptSquarePoint[i].y += fMargin_Y;
	}
}

void CESMAutoAdjustSquareCalibration::SetInitSquarePoint(vector<Point3f> vecPtInitSquarePoint) 
{
	_vecPtInit3DSquarePoint.clear();
	_vecPtInit3DSquarePoint = vecPtInitSquarePoint;

	Normalization3DSquarePoint(_vecPtInit3DSquarePoint, 100);
}

void CESMAutoAdjustSquareCalibration::SetCenterPointAt(string strDscId, Point2f ptCenterPoint)
{
	int nIndex = GetAdjustIndexAt(strDscId);
	if(nIndex == -1)
		return;

	_vecAdjustSquareInfo[nIndex]->ptCenterPoint = ptCenterPoint;
}

void CESMAutoAdjustSquareCalibration::SetInitCenterPoint(string strDscId, Point2f ptCenterPoint)
{
	int nIndex = GetAdjustIndexAt(strDscId);
	if(nIndex == -1)
		return;

	_vecAdjustSquareInfo[nIndex]->ptCenterPoint = ptCenterPoint;

	for(int i = nIndex+1; i != nIndex; i++)
	{
		i %= _vecAdjustSquareInfo.size();
		Mat mHomography = GetHomographyCameraToCamera(_vecAdjustSquareInfo, i);

		if(mHomography.empty())
			continue;

		if(i == 0)
		{
			_vecAdjustSquareInfo[i]->ptCenterPoint = 
				CalcPointLocation(
				mHomography, 
				_vecAdjustSquareInfo[_vecAdjustSquareInfo.size()-1]->ptCenterPoint);
		}
		else
		{
			_vecAdjustSquareInfo[i]->ptCenterPoint = 
				CalcPointLocation(
				mHomography, 
				_vecAdjustSquareInfo[i-1]->ptCenterPoint);
		}

		if(nIndex == 0 && i == _vecAdjustSquareInfo.size()-1)
			i = -1;
	}
}

void CESMAutoAdjustSquareCalibration::SetInitCenterPointRange(string strStartDscId, string strEndDscId, Point2f ptCenterPoint)
{
	int nStartIndex = -1;
	int nEndIndex = -1;
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strStartDscId) == 0)
			nStartIndex = i;
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strEndDscId) == 0)
			nEndIndex = i;
	}
	if(nStartIndex == -1 || nEndIndex == -1)
		return;

	_vecAdjustSquareInfo[nStartIndex]->ptCenterPoint = ptCenterPoint;

	for(int i = nStartIndex+1; i <= nEndIndex; i++)
	{
		Mat mHomography = GetHomographyCameraToCamera(_vecAdjustSquareInfo, i);

		if(mHomography.empty())
			continue;

		_vecAdjustSquareInfo[i]->ptCenterPoint = 
			CalcPointLocation(
			mHomography, 
			_vecAdjustSquareInfo[i-1]->ptCenterPoint);
	}
}

void CESMAutoAdjustSquareCalibration::Set3DSquarePoint()
{
	int nMaxRange = 100;
	int nLocation	= nMaxRange / 2;

	_vecPtInit3DSquarePoint.push_back(Point3f(0,		 0,			0));
	_vecPtInit3DSquarePoint.push_back(Point3f(nMaxRange, 0,			0));
	_vecPtInit3DSquarePoint.push_back(Point3f(nMaxRange, nMaxRange,	0));
	_vecPtInit3DSquarePoint.push_back(Point3f(0,		 nMaxRange,	0));

	_vecPtNormalVector.push_back( Point3f(nLocation,	 nLocation,	0));
	_vecPtNormalVector.push_back( Point3f(nLocation,	 nLocation,	-nMaxRange));
}

Scalar CESMAutoAdjustSquareCalibration::Coloring(int index)
{
	int red = 0; int green = 0; int blue = 0;

	if(index % 6 == 0)		red		= 255;
	if(index % 6 - 1 == 0)	green	= 255;
	if(index % 6 - 2 == 0)	blue	= 255;
	if(index % 6 - 3 == 0) {red		= 255;
	green	= 255;}
	if(index % 6 - 4 == 0) {green	= 255;
	blue	= 255;}
	if(index % 6 - 5 == 0) {blue	= 255;
	red		= 255;};

	return Scalar(blue, green, red);
}


void CESMAutoAdjustSquareCalibration::DrawSquare(Mat& mFrame, vector<Point2f> vecPtTemplatePoint)
{
	for(int i = 0; i < vecPtTemplatePoint.size(); i++)
	{
		circle(mFrame, vecPtTemplatePoint[i], 20, Coloring(i), cv::FILLED);

		if(i == vecPtTemplatePoint.size()-1)
			line(mFrame, vecPtTemplatePoint[i], vecPtTemplatePoint[0], Scalar(255, 0, 255), _nLinetickness, cv::LINE_AA);
		else
			line(mFrame, vecPtTemplatePoint[i], vecPtTemplatePoint[i+1], Scalar(255, 0, 255), _nLinetickness, cv::LINE_AA);
	}

	if(vecPtTemplatePoint.size() == 4)
	{
		line(mFrame, vecPtTemplatePoint[0], vecPtTemplatePoint[2], Scalar(255, 0, 255), _nLinetickness, cv::LINE_AA);
		line(mFrame, vecPtTemplatePoint[1], vecPtTemplatePoint[3], Scalar(255, 0, 255), _nLinetickness, cv::LINE_AA);
	}
}

void CESMAutoAdjustSquareCalibration::DrawObject(Mat& mFrame, string strDscId)
{
	//DrawSquare(mFrame, _vecAdjustSquareInfo[nFrameIndex]->vecPtSquarePoints);
	
	//wgkim 191023
	int nIndex = GetAdjustIndexAt(strDscId);
	if (nIndex == -1)
		return;

	vector<Point3f> vec3DSquarePoints = _vecAdjustSquareInfo[nIndex]->vecPtInit3DSquarePoint;
	if (vec3DSquarePoints.at(0).x == 0 && vec3DSquarePoints.at(0).y == 0 &&
		vec3DSquarePoints.at(1).x == 0 && vec3DSquarePoints.at(1).y == 0 &&
		vec3DSquarePoints.at(2).x == 0 && vec3DSquarePoints.at(2).y == 0 &&
		vec3DSquarePoints.at(3).x == 0 && vec3DSquarePoints.at(3).y == 0)
		return;

	SetCalibrationBarPoints(
		ChangeCoordinateFromWorldToCamera(
		strDscId, 
		GetCalibrationBar3DPoints())
		);
	DrawObjectForCalibrationBar(mFrame);

	bool bFlag = GetUseKZoneAt(strDscId);
	if(bFlag == TRUE)
	{
		SetKZonePointAt(strDscId);
		DrawObjectForKZone(strDscId, mFrame);
	}
}

bool CESMAutoAdjustSquareCalibration::IsValidAdjustSquareInfo()
{
	if(_vecAdjustSquareInfo.empty())
		return false;
	else
	{
		for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
		{
			if(_vecAdjustSquareInfo[i]->ptCenterPoint == Point2f(0, 0))
				return false;
		}
		return true;
	}
}

void CESMAutoAdjustSquareCalibration::AddSquarePoint(AdjustSquareInfo* vecPtSquarePoint)
{
	_vecAdjustSquareInfo.push_back(vecPtSquarePoint);
}

void CESMAutoAdjustSquareCalibration::SetSquarePointAt(string strDscId, Point2f pt1, Point2f pt2, Point2f pt3, Point2f pt4)
{
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
		{
			_vecAdjustSquareInfo[i]->vecPtSquarePoints[0] = pt1;
			_vecAdjustSquareInfo[i]->vecPtSquarePoints[1] = pt2;
			_vecAdjustSquareInfo[i]->vecPtSquarePoints[2] = pt3;
			_vecAdjustSquareInfo[i]->vecPtSquarePoints[3] = pt4;
		}
	}
}

AdjustSquareInfo* CESMAutoAdjustSquareCalibration::GetSquareInfo(string strDscId)
{
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
			return _vecAdjustSquareInfo[i];
	}
	return NULL;
}

void CESMAutoAdjustSquareCalibration::DrawObjectForCalibrationBar(Mat& mFrame)
{
	//���غ�
	line(mFrame, _vecPtCalibrationBarProjectedPoints.at(0), _vecPtCalibrationBarProjectedPoints.at(1), Scalar(0, 0, 255), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecPtCalibrationBarProjectedPoints.at(0), _vecPtCalibrationBarProjectedPoints.at(2), Scalar(0, 255, 0), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecPtCalibrationBarProjectedPoints.at(0), _vecPtCalibrationBarProjectedPoints.at(3), Scalar(255, 0, 0), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecPtCalibrationBarProjectedPoints.at(0), _vecPtCalibrationBarProjectedPoints.at(4), Scalar(255, 255, 0), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecPtCalibrationBarProjectedPoints.at(0), _vecPtCalibrationBarProjectedPoints.at(5), Scalar(255, 0, 255), _nLinetickness, cv::LINE_AA, 0);
}

void CESMAutoAdjustSquareCalibration::DrawObjectForKZone(string strDscId, Mat& mFrame)
{
	int nIndex = GetAdjustIndexAt(strDscId);
	if(nIndex == -1)
		return;

	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(0), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(1), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(1), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(2), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(2), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(3), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(3), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(0), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);

	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(0), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(4), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(1), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(5), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(2), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(6), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(3), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(7), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);

	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(4), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(8), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(5), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(8), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(6), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(9), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(7), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(9), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);

	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(4), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(7), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(5), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(6), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);
	line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(8), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(9), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);

	//line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(10), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(11), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);
	//line(mFrame, _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(12), _vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints.at(13), Scalar(255, 255, 255), _nLinetickness, cv::LINE_AA, 0);
}

void CESMAutoAdjustSquareCalibration::SetInitKZone3DPoints()
{
	int nMaxRange = 100;

	int nLocation	= nMaxRange / 2;
	int nLength		= 6.5;//nMaxRange / 10;//6.5;
	int nYaxis		= 7.7;

	//kzone
	_vecPtKZone3DPoints.push_back( Point3f(nLocation-nLength, nLocation-nYaxis,	-nLength*2));
	_vecPtKZone3DPoints.push_back( Point3f(nLocation+nLength, nLocation-nYaxis,	-nLength*2));
	_vecPtKZone3DPoints.push_back( Point3f(nLocation+nLength, nLocation-nYaxis,	-nLength*5));
	_vecPtKZone3DPoints.push_back( Point3f(nLocation-nLength, nLocation-nYaxis,	-nLength*5));

	_vecPtKZone3DPoints.push_back( Point3f(nLocation-nLength, nLocation,		-nLength*2));
	_vecPtKZone3DPoints.push_back( Point3f(nLocation+nLength, nLocation,		-nLength*2));
	_vecPtKZone3DPoints.push_back( Point3f(nLocation+nLength, nLocation,		-nLength*5));
	_vecPtKZone3DPoints.push_back( Point3f(nLocation-nLength, nLocation,		-nLength*5));

	_vecPtKZone3DPoints.push_back( Point3f(nLocation,		 nLocation+nYaxis,	-nLength*2));
	_vecPtKZone3DPoints.push_back( Point3f(nLocation,		 nLocation+nYaxis,	-nLength*5));

	_vecPtKZone3DPoints.push_back( Point3f(nLocation,		 nLocation-nYaxis,	0));
	_vecPtKZone3DPoints.push_back( Point3f(nLocation,		 nLocation-nYaxis,	-nLength*2));
	_vecPtKZone3DPoints.push_back( Point3f(nLocation,		 nLocation+nYaxis,	0));
	_vecPtKZone3DPoints.push_back( Point3f(nLocation,		 nLocation+nYaxis,	-nLength*2));
}

void CESMAutoAdjustSquareCalibration::SetCalibrationBarObjectPoints()
{
	int nMaxRange = 100;

	int nLocation	= nMaxRange / 2;
	int nLength		= 6.5;//nMaxRange / 10;//6.5;
	int nYaxis		= 7.7;

	//���غ�
	_vecPtCalibrationBar3DPoints.push_back( Point3f(nLocation,			nLocation,			0));
	_vecPtCalibrationBar3DPoints.push_back( Point3f(nLocation+nLength,	nLocation,			0));
	_vecPtCalibrationBar3DPoints.push_back( Point3f(nLocation,			nLocation+nLength,	0));
	_vecPtCalibrationBar3DPoints.push_back( Point3f(nLocation,			nLocation,			-nLength*10));
	_vecPtCalibrationBar3DPoints.push_back( Point3f(nLocation-nLength,	nLocation,			0));
	_vecPtCalibrationBar3DPoints.push_back( Point3f(nLocation,			nLocation-nLength,	0));
}

Point2f CESMAutoAdjustSquareCalibration::GetCenterPointAt(string strDscId)
{
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
			return _vecAdjustSquareInfo[i]->ptCenterPoint;
	}
	return Point2f(0, 0);
}

double CESMAutoAdjustSquareCalibration::GetNormValueAt(string strDscId)
{
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
			return _vecAdjustSquareInfo[i]->dNorm;
	}
	return 1.0;
}

void CESMAutoAdjustSquareCalibration::SetNormValueAt(string strDscId, double dValue)
{
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
			_vecAdjustSquareInfo[i]->dNorm = dValue;
	}
}

double CESMAutoAdjustSquareCalibration::GetAngleValueAt(string strDscId)
{
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
			return _vecAdjustSquareInfo[i]->dAngle;
	}
	return 0.f;
}

double CESMAutoAdjustSquareCalibration::GetDistanceValueAt(string strDscId)
{
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
			return _vecAdjustSquareInfo[i]->dDistance;
	}
	return 1.0;
}

void CESMAutoAdjustSquareCalibration::SetDistanceValueAt(string strDscId, double dValue)
{
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
			_vecAdjustSquareInfo[i]->dDistance = dValue;
	}
}

void CESMAutoAdjustSquareCalibration::Set3DInitSquarePointAt(string strDscId, vector<Point3f> ptPoint)
{
	vector<Point3f> ptPointWithoutNormalization = ptPoint;
	Normalization3DSquarePoint(ptPoint, 100);

	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
		{
			_vecAdjustSquareInfo[i]->vecPtInit3DSquarePoint = ptPoint;
			_vecAdjustSquareInfo[i]->vecPtInit3DSquarePointWithoutNormalization = ptPointWithoutNormalization;
		}
	}
}

vector<Point3f> CESMAutoAdjustSquareCalibration::Get3DInitSquarePointAt(string strDscId)
{
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
			return _vecAdjustSquareInfo[i]->vecPtInit3DSquarePointWithoutNormalization;
	}
	return vector<Point3f>();
}

void CESMAutoAdjustSquareCalibration::SetKZonePointAt(string strDscId)
{
	int nIndex = GetAdjustIndexAt(strDscId);
	if(nIndex == -1)
		return;

	if(_vecPtKZone3DPoints.size() < 10)
		SetInitKZone3DPoints();

	//wgkim 191023
	vector<Point3f> vec3DSquarePoints = _vecAdjustSquareInfo[nIndex]->vecPtInit3DSquarePoint;
	if (vec3DSquarePoints.at(0).x == 0 && vec3DSquarePoints.at(0).y == 0 &&
		vec3DSquarePoints.at(1).x == 0 && vec3DSquarePoints.at(1).y == 0 &&
		vec3DSquarePoints.at(2).x == 0 && vec3DSquarePoints.at(2).y == 0 &&
		vec3DSquarePoints.at(3).x == 0 && vec3DSquarePoints.at(3).y == 0)
		return;

	vector<Point2f> vecPtKZoneProjectedPoints 
		= ChangeCoordinateFromWorldToCamera(strDscId, _vecPtKZone3DPoints);
	_vecAdjustSquareInfo[nIndex]->vecPtKZoneProjectedPoints = vecPtKZoneProjectedPoints;
}

vector<Point2f> CESMAutoAdjustSquareCalibration::GetKZonePointAt(string strDscId)
{
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
			return _vecAdjustSquareInfo[i]->vecPtKZoneProjectedPoints;
	}
	return vector<Point2f>();
}

int CESMAutoAdjustSquareCalibration::GetAdjustIndexAt(string strDscId)
{
	int nIndex = -1;
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
			nIndex = i;
	}

	return nIndex;
}

bool CESMAutoAdjustSquareCalibration::GetUseKZoneAt(string strDscId)
{
	if(_vecAdjustSquareInfo.size() == 0)
	{
		return false;
	}
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
			return _vecAdjustSquareInfo[i]->bUseKZone;
	}
	return false;
}

void CESMAutoAdjustSquareCalibration::SetUseKZoneAt(string strDscId, bool bUseKZone)
{
	for(int i = 0; i < _vecAdjustSquareInfo.size(); i++)
	{
		if(_vecAdjustSquareInfo[i]->strDscId.compare(strDscId) == 0)
			_vecAdjustSquareInfo[i]->bUseKZone = bUseKZone;
	}
}