#pragma once


// CSysFrequency 대화 상자입니다.

class CSysFrequency : public CDialogEx
{
	DECLARE_DYNAMIC(CSysFrequency)

public:
	CSysFrequency(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSysFrequency();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_SYS_FREQUENCY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnNtsc();
	afx_msg void OnBnClickedBtnPal();
};
