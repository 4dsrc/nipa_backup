#pragma once

#include "ESMOptManagement.h"
#include "BackupListCtrl.h"
#include "ESMBackup.h"
#include "afxwin.h"
#include <vector>

// CBackupFolderSelectorDlg 대화 상자입니다.

class CBackupFolderSelectorDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CBackupFolderSelectorDlg)

public:
	CBackupFolderSelectorDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CBackupFolderSelectorDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_MANAGEMENT_BACKUP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

private:
	CBackupListCtrl *m_pBackupListCtrl;
	CESMBackup *m_pBackupMgr;
	BOOL m_bIsAllCheck;
	CButton m_btnCheckAll;
	vector<CString> m_IPList;
	CESMOptManagement* m_pParent;

public:
	void SetBackupMgr(CESMBackup* pBackupMgr);

	afx_msg void OnBnClickedButtonCheckAll();
	afx_msg void OnBnClickedButtonSave();
	
private:
	void LoadFolderList();
	void GetIPList();
	void DeleteBackupFolderListFile(BOOL backup=TRUE);
	void WriteFolderList(int num, CString name, BOOL backup=TRUE);
public:
	afx_msg void OnLvnColumnclickListBackupFolder(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchangingListBackupFolder(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonDelete();
};
