#pragma once

#include "ESMTemplateImage.h"
#include "ESMFunc.h"
#include "ESMTemplateMgr.h"
#include "ESMTemplateAsAuto.h"
#include <vector>
#include "afxwin.h"
#include "afxres.h"
#include "afxcmn.h"

#include "ESMNetworkListCtrl.h"
#include "ESMEditorEX.h"
#include "ESMGroupBox.h"
#include "esmbuttonex.h"

#define TEMPLATE_TEMP_FILE _T("TemplateTemp.pta")

class CESMTemplateDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CESMTemplateDlg)

public:
	CESMTemplateDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMTemplateDlg();

// 대화 상자 데이터입니다.
	enum { IDD = ID_IMAGE_TEMPLATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnCustomdrawTcpList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLvnItemchangedAdjust(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);

	afx_msg void OnBnClickedBtnAdjustPointReset();
	afx_msg void OnBnClickedBtn2ndSavePoint();
	afx_msg void OnBnClickedBtn2ndLoadPoint();	
	afx_msg void OnBnClickedBtnAutoFind();
	afx_msg void OnBnClickedBtnAutoMove();
	afx_msg void OnBnClickedBtnYPointModifyAll();
		
	virtual BOOL OnInitDialog();	
	virtual void OnOK();
	virtual void OnCancel();	

	static unsigned WINAPI GetMovieDataThread(LPVOID param);	

	void AddTemplateMgr(CESMTemplateMgr *templateMgr);

	CPoint GetPointFromMouse(CPoint pt);
	void SetMovieState(int nMovieState = ESM_ADJ_FILM_STATE_MOVIE);
	
	void Showimage();
	int DrawImage();  
	void DeleteImage(); 
	void LoadData();
	void DrawCenterPointAll();
	void DrawCenterPoint(int nPosX, int nPosY);
	void DrawCenterPoint(int nPosX, int nPosY, int nIndex);
	COLORREF Coloring(int index);
	void FindExactPosition(int& nX, int& nY);

	void SavePointData(CString strFileName);
	void LoadPointData(CString strFileName);	
	
	CESMNetworkListCtrl m_AdjustDscList;
	CComboBox m_ctrlCheckPoint;
	HANDLE	m_hTheadHandle;
	CEdit m_ctrlCenterLine;

	vector<stAdjustInfo> m_adjinfo;	

	int m_nMovieState;
	int m_nSelectedNum;
	int m_nSelectPos;
	int m_RdImageViewColor;
	int m_nView;
	int m_nRdDetectColor;
	int m_nWidth, m_nHeight;
	int m_frameIndex;
	int m_nPosX, m_nPosY;	
	int m_MarginX;
	int m_MarginY;	
	int m_Scale;
	int m_Move;
	int m_Number;	
	BOOL m_bAutoPoint;
	BOOL m_bAutoPointMove;
	BOOL m_bThreadCloseFlag;	
		
	CESMTemplateMgr *m_pTemplateMgr;	
	CESMTemplateImage m_StAdjustImage;
	CESMTemplateAsAuto m_pTemplateAsAuto;
	
public:
	void setDscListItem(CListCtrl* pList, int rowIndex, int colIndex, int data);	
	int getDSCListItem(CListCtrl* pList, int rowIndex, int colIndex);
	vector<Point2f> getSquareFromListCtrl(CListCtrl* pList, int nSelectedNumber);	

public:
	void applyHomographiesController();

	//170615 hjcho - 선택 영역 지우기
	virtual BOOL PreTranslateMessage(MSG* pMsg);	
	void DeleteSelectedList();
	void DeleteSelectItem(int nIndex);

	int m_nYPointModifyValue;

public:
	CBrush m_background;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CESMButtonEx m_btnLoad;
	CESMButtonEx m_btnSave;
	CESMButtonEx m_btnReset;
	CESMButtonEx m_btnOk;
	CESMButtonEx m_btnCancel;
	CESMButtonEx m_btnYPointModify;
};
