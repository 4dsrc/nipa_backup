// ESMTemplateDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#ifdef _4DMODEL
#include "4DModeler.h"
#else
#include "4DMaker.h"
#endif
#include "MainFrm.h"
#include "ESMAutoAdjustDlg.h"
#include "ESMAutoAdjustImage.h"
#include "ESMAutoAdjustMgr.h"
#include "afxdialogex.h"
#include "FFmpegManager.h"
#include "ESMIni.h"
#include "ESMCtrl.h"

#include "ESMFileOperation.h"
#include "ESMFunc.h"
#include "ESMMovieMgr.h"
#include "ESMImgMgr.h"

#include<highgui.h>
#include<cv.h>

#include <math.h>

#define PI 3.1415926535897932384
#define TIMER_CHANGE_NEXT_ITEM	0x8001
#define TIME_CHANGE_NEXT_ITEM	700

// ESMAdjustMgrDlg 대화 상자입니다.
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


enum{
	FIRST_ADJUST_VIEW = 0,
};

enum{
	ADJUST_IMAGE = 0,
	MAX_IDX,
};

enum{
	ADJUST_LEFT_UP = 0,
	ADJUST_LEFT_DOWN,
	ADJUST_RIGHT_UP,
	ADJUST_RIGHT_DOWN,
};
enum{
	T_LB_D = 0,
	T_RB_D,
	T_PEAK_D,
	B_PEAK_D,
};
IMPLEMENT_DYNAMIC(CESMAutoAdjustDlg, CDialogEx)


	CESMAutoAdjustDlg::CESMAutoAdjustDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMAutoAdjustDlg::IDD, pParent)
{
	m_nSelectPos = 0;
	m_nSelectedNum = -1;
	m_nHeight = 0;
	m_nWidth = 0;
	m_frameIndex= 0;
	m_nPosX = 0, m_nPosY = 0;	
	m_MarginX = 0;
	m_MarginY = 0;	
	m_hTheadHandle = NULL;	
	m_bThreadCloseFlag = FALSE;
	m_bStabilizationCloseFlag = FALSE;

	m_dFrameTime = 0;
	m_frameIndex = 0;

	m_bLoadAdjustImageAll = FALSE;
	m_bInitializedTracking = FALSE;	

	m_background.CreateSolidBrush(RGB(44,44,44));

	m_pSquareCalibration = NULL;
	m_pSquareCalibration = new CESMAutoAdjustSquareCalibration();

	m_pAdjustImg = NULL;

	m_strWorldCoordinationImagePath = "";
	//RunWorldCoordinateDlg();

	m_pCenterPoint = CPoint(0, 0);

	m_bAutoFindPause = FALSE;
}

CESMAutoAdjustDlg::~CESMAutoAdjustDlg()
{
	WaitForSingleObject(m_hTheadHandle, INFINITE);
	CloseHandle(m_hTheadHandle);

//	DestroyWorldCoordinateDlg();
	if(m_pSquareCalibration != NULL)
	{
		delete m_pSquareCalibration;
	}

	destroyWindow("Adjust Preview");
}

void CESMAutoAdjustDlg::OnOK()
{
	m_bThreadCloseFlag = TRUE;
	m_bStabilizationCloseFlag = TRUE;

	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	SavePointData(strFileName);

	CDialog::OnOK();
}

void CESMAutoAdjustDlg::OnCancel()
{	
	m_bThreadCloseFlag = TRUE;
	m_bStabilizationCloseFlag = TRUE;

	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	SavePointData(strFileName);

	CDialog::OnCancel();
}

void CESMAutoAdjustDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX,	IDC_TEMPLATE_DSCLIST,	m_AdjustDscList);
	DDX_Control(pDX,	 IDC_TEMPLATE_COMBO,	m_ctrlCheckPoint);
	DDX_Control(pDX,	IDC_ST_PRISMIMAGE,  m_StAdjustImage);
	DDX_Control(pDX,	IDC_ST_PRISMIMAGE2,  m_StAdjustWorldCoordinateImage);
	DDX_Control(pDX, IDC_EDIT_TEMPLATE_CENTER, m_ctrlCenterLine);
	DDX_Control(pDX, IDC_TEMPLATE_COMBO_WCIMAGE, m_cbSetWCImage);
	//DDX_Check(pDX, IDC_CHECK_AUTOADJUST_AUTOFIND, m_bAutoFind);
	DDX_Check(pDX, IDC_CHECK_AUTOADJUST_AUTOMOVE, m_bAutoMove);
	DDX_Control(pDX, IDC_BTN_LOAD, m_btnLoad);
	DDX_Control(pDX, IDC_BTN_SAVE, m_btnSave);
	DDX_Control(pDX, IDC_BTN_RESET, m_btnReset);
	DDX_Control(pDX, IDC_BUTTON_AUTOADJUST_AUTOFIND_DOWNSTART, m_btnDown);
	DDX_Control(pDX, IDC_BUTTON_AUTOADJUST_AUTOFIND_UPSTART, m_btnUp);
	DDX_Control(pDX, IDC_BUTTON_AUTOADJUST_AUTOFIND_PAUSE, m_btnPause);
	DDX_Control(pDX, IDC_BUTTON_AUTOADJUST_PLUS, m_btnPlus);
	DDX_Control(pDX, IDC_BUTTON_AUTOADJUST_MINUS, m_btnMinus);
	DDX_Control(pDX, IDC_BTN_MODIFYADJ, m_btnAdjustPreview);
	DDX_Control(pDX, IDC_BUTTON_DISTANCE_REGISTER, m_btnRegister);
	DDX_Control(pDX, IDC_BUTTON_DISTANCE_UNREGISTER, m_btnUnregister);
	DDX_Control(pDX, IDC_BUTTON_DISTANCE_REGISTER_ALL, m_btnRegisterAll);
	DDX_Control(pDX, IDC_BUTTON_DISTANCE_UNREGISTER_ALL, m_btnUnregisterAll);
	DDX_Control(pDX, IDC_BUTTON_ADJUST_CALC, m_btnCalcAdjust);
	DDX_Control(pDX, IDC_BUTTON_ADJUST_SAVE, m_btnSaveAdjust);
	DDX_Control(pDX, IDOK, m_btnOk);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}

BEGIN_MESSAGE_MAP(CESMAutoAdjustDlg, CDialogEx)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_TEMPLATE_DSCLIST, &CESMAutoAdjustDlg::OnLvnItemchanged)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEWHEEL()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONUP()
	ON_WM_MBUTTONUP()
	ON_BN_CLICKED(IDC_BTN_RESET, &CESMAutoAdjustDlg::OnBnClickedBtnAdjustPointReset)
	ON_BN_CLICKED(IDC_BTN_SAVE, &CESMAutoAdjustDlg::OnBnClickedBtn2ndSavePoint)
	ON_BN_CLICKED(IDC_BTN_LOAD, &CESMAutoAdjustDlg::OnBnClickedBtn2ndLoadPoint)
	ON_BN_CLICKED(IDC_BTN_MODIFYADJ, &CESMAutoAdjustDlg::OnBnClickedBtnModifyAdj)
	ON_BN_CLICKED(IDC_CHECK_AUTOPOINT_MOVE, &CESMAutoAdjustDlg::OnBnClickedBtnAutoMove)
	ON_CBN_SELCHANGE(IDC_TEMPLATE_COMBO_WCIMAGE, &CESMAutoAdjustDlg::OnCbnSelchangeCbnSetWCImage)
	ON_WM_CTLCOLOR()

	ON_BN_CLICKED(IDC_BUTTON_DISTANCE_REGISTER, &CESMAutoAdjustDlg::OnBnClickedButtonDistanceRegister)
	ON_BN_CLICKED(IDC_BUTTON_DISTANCE_UNREGISTER, &CESMAutoAdjustDlg::OnBnClickedButtonDistanceUnregister)
	ON_BN_CLICKED(IDC_BUTTON_ADJUST_CALC, &CESMAutoAdjustDlg::OnBnClickedButtonAdjustCalc)
	ON_BN_CLICKED(IDC_BUTTON_DISTANCE_REGISTER_ALL, &CESMAutoAdjustDlg::OnBnClickedButtonDistanceRegisterAll)
	ON_BN_CLICKED(IDC_BUTTON_DISTANCE_UNREGISTER_ALL, &CESMAutoAdjustDlg::OnBnClickedButtonDistanceRegisterAll2)
	ON_BN_CLICKED(IDC_BUTTON_ADJUST_SAVE, &CESMAutoAdjustDlg::OnBnClickedButtonAdjustSave)
//	ON_BN_CLICKED(IDC_CHECK1, &CESMAutoAdjustDlg::OnBnClickedCheck1)
//	ON_BN_CLICKED(IDC_CHECK9, &CESMAutoAdjustDlg::OnBnClickedCheck9)
	ON_BN_CLICKED(IDC_CHECK_AUTOADJUST_AUTOMOVE, &CESMAutoAdjustDlg::OnBnClickedCheckAutoadjustAutomove)
	ON_BN_CLICKED(IDC_CHECK_AUTOADJUST_AUTOFIND, &CESMAutoAdjustDlg::OnBnClickedCheckAutoadjustAutofind)
	ON_BN_CLICKED(IDC_BUTTON_AUTOADJUST_PLUS, &CESMAutoAdjustDlg::OnBnClickedButtonAutoadjustPlus)
	ON_BN_CLICKED(IDC_BUTTON_AUTOADJUST_MINUS, &CESMAutoAdjustDlg::OnBnClickedButtonAutoadjustMinus)
	ON_BN_CLICKED(IDC_BUTTON_AUTOADJUST_AUTOFIND_UPSTART, &CESMAutoAdjustDlg::OnBnClickedButtonAutoadjustAutoFindUpStart)
	ON_BN_CLICKED(IDC_BUTTON_AUTOADJUST_AUTOFIND_DOWNSTART, &CESMAutoAdjustDlg::OnBnClickedButtonAutoadjustAutoFindDownStart)
	ON_BN_CLICKED(IDC_BUTTON_AUTOADJUST_AUTOFIND_PAUSE, &CESMAutoAdjustDlg::OnBnClickedButtonAutoadjustAutoFindPause)
END_MESSAGE_MAP()

void CESMAutoAdjustDlg::setDscListItem(CListCtrl* pList, int rowIndex, int colIndex, int data)
{
	CString strText;
	strText.Format(_T("%d"), data);
	pList->SetItemText(rowIndex, colIndex, strText);	
}

int CESMAutoAdjustDlg::getDSCListItem(CListCtrl* pList, int rowIndex, int colIndex)
{
	CString strSelect = pList->GetItemText(rowIndex, colIndex);
	return _ttoi(strSelect);	
}

BOOL CESMAutoAdjustDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_AdjustDscList.Init();

	m_ctrlCheckPoint.AddString(_T("1st"));
	m_ctrlCheckPoint.AddString(_T("2nd"));
	m_ctrlCheckPoint.AddString(_T("3rd"));
	m_ctrlCheckPoint.AddString(_T("4th"));
	m_ctrlCheckPoint.AddString(_T("None"));
	m_ctrlCheckPoint.SetCurSel(4);

	for(int i = 0; i < ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_COUNT; i++)
	{
		ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE eIndex = (ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE)i;
		m_cbSetWCImage.AddString(getWorldCoordinageImageName(eIndex));
	}

	m_cbSetWCImage.SetCurSel(0);

	LoadData();
	Showimage();
	CString strFileName;

	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	LoadPointData(strFileName);

	m_Move = 0;
	m_Scale = 0;
	m_Number = 0;

	OnCbnSelchangeCbnSetWCImage();
	ShowWorldCoordnationImage();

	m_pAutoAdjustMgr->SetAdjustSquareInfo();
	InputAutoAdjustDataFromCtrlList();

	UpdateData(FALSE);
	return TRUE;
}

CString CESMAutoAdjustDlg::getWorldCoordinageImageName(ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE eWorldCoordinateImage)
{
	CString strWorldCoordinageImageName;
	switch(eWorldCoordinateImage)
	{
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_BASEBALL_HOME:
		strWorldCoordinageImageName = _T("Baseball_Home.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_BASEBALL_GROUND:
		strWorldCoordinageImageName = _T("Baseball_Ground.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_BASKETBALL_HALF:
		strWorldCoordinageImageName = _T("Basketball_Half.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_BASKETBALL_GROUND:
		strWorldCoordinageImageName = _T("Basketball_Ground.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_BOXING:
		strWorldCoordinageImageName = _T("Boxing.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_ICE_LINK_HALF:
		strWorldCoordinageImageName = _T("Ice-Link_Half.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_ICE_LINK:
		strWorldCoordinageImageName = _T("Ice-Link.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_SOCCER_HALF:
		strWorldCoordinageImageName = _T("Soccer_Half.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_SOCCER:
		strWorldCoordinageImageName = _T("Soccer.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_TAEKWONDO:
		strWorldCoordinageImageName = _T("Taekwondo.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_TENNIS_HALF:
		strWorldCoordinageImageName = _T("Tennis_Half.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_TENNIS:
		strWorldCoordinageImageName = _T("Tennis.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_UFC:
		strWorldCoordinageImageName = _T("UFC.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_VOLLEYBALL_HALF:
		strWorldCoordinageImageName = _T("Volleyball_Half.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_VOLLEYBALL_GROUND:
		strWorldCoordinageImageName = _T("Volleyball.png");
		break;
	case ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE_FOOTBALL:
		strWorldCoordinageImageName = _T("Football.png");
		break;
	default:
		ESMLog(0, _T("not Exist World Coordinate Image!"));
		break;
	}
	return strWorldCoordinageImageName;
}

void CESMAutoAdjustDlg::SetAutoAdjustMgr(CESMAutoAdjustMgr *pAutoAdjustMgr)
{
	m_pAutoAdjustMgr = pAutoAdjustMgr;
	m_pAutoAdjustMgr->SetSquareCalibration(m_pSquareCalibration);
}

void CESMAutoAdjustDlg::SetTemplateMgr(CESMTemplateMgr *pTemplateMgr)
{
	m_pTemplateMgr = pTemplateMgr;
}

void CESMAutoAdjustDlg::SavePointData(CString strFileName)
{
	CESMIni ini;
	CFile file;
	file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite);
	file.Close();

	if(!ini.SetIniFilename (strFileName))
		return;

	ini.WriteInt(_T("MovieSize"), _T("HEIGHT"), m_nHeight);
	ini.WriteInt(_T("MovieSize"), _T("WIDTH"), m_nWidth);

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);
		ini.WriteString(strSelect, _T("T_LBX"), m_AdjustDscList.GetItemText(i, 2));
		ini.WriteString(strSelect, _T("T_LBY"), m_AdjustDscList.GetItemText(i, 3));
		ini.WriteString(strSelect, _T("T_RBX"), m_AdjustDscList.GetItemText(i, 4));
		ini.WriteString(strSelect, _T("T_RBY"), m_AdjustDscList.GetItemText(i, 5));
		ini.WriteString(strSelect, _T("T_PeakX"), m_AdjustDscList.GetItemText(i, 6));
		ini.WriteString(strSelect, _T("T_PeakY"), m_AdjustDscList.GetItemText(i, 7));
		ini.WriteString(strSelect, _T("B_PeakX"), m_AdjustDscList.GetItemText(i, 8));
		ini.WriteString(strSelect, _T("B_PeakY"), m_AdjustDscList.GetItemText(i, 9));

		ini.WriteString(strSelect, _T("1st_World_X"), m_AdjustDscList.GetItemText(i, 20));
		ini.WriteString(strSelect, _T("1st_World_Y"), m_AdjustDscList.GetItemText(i, 21));
		ini.WriteString(strSelect, _T("2nd_World_X"), m_AdjustDscList.GetItemText(i, 22));
		ini.WriteString(strSelect, _T("2nd_World_Y"), m_AdjustDscList.GetItemText(i, 23));
		ini.WriteString(strSelect, _T("3rd_World_X"), m_AdjustDscList.GetItemText(i, 24));
		ini.WriteString(strSelect, _T("3rd_World_Y"), m_AdjustDscList.GetItemText(i, 25));
		ini.WriteString(strSelect, _T("4th_World_X"), m_AdjustDscList.GetItemText(i, 26));
		ini.WriteString(strSelect, _T("4th_World_Y"), m_AdjustDscList.GetItemText(i, 27));

		ini.WriteString(strSelect, _T("CenterX"), m_AdjustDscList.GetItemText(i, 10));
		ini.WriteString(strSelect, _T("CenterY"), m_AdjustDscList.GetItemText(i, 11));

		CString strData = m_AdjustDscList.GetItemText(i, 12);
		if(strData.Compare(_T("0")) == 0)
		{
			ini.WriteString(strSelect, _T("Select"), _T("0"));
		}
		else
		{
			ini.WriteString(strSelect, _T("Select"), _T(""));
		}

		if(m_AdjustDscList.GetCheck(i))
		{
			ini.WriteString(strSelect, _T("K-Zone"), _T("TRUE"));
		}
		else
		{
			ini.WriteString(strSelect, _T("K-Zone"), _T("FALSE"));
		}
	}
}

void CESMAutoAdjustDlg::SavePointData(CString strFileName, int nRow)
{
	CESMIni ini;
	CFile file;
	file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite);
	file.Close();

	if(!ini.SetIniFilename (strFileName))
		return;

	CString strSelect = m_AdjustDscList.GetItemText(nRow, 1);
	ini.WriteString(strSelect, _T("T_LBX"), m_AdjustDscList.GetItemText(nRow, 2));
	ini.WriteString(strSelect, _T("T_LBY"), m_AdjustDscList.GetItemText(nRow, 3));
	ini.WriteString(strSelect, _T("T_RBX"), m_AdjustDscList.GetItemText(nRow, 4));
	ini.WriteString(strSelect, _T("T_RBY"), m_AdjustDscList.GetItemText(nRow, 5));
	ini.WriteString(strSelect, _T("T_PeakX"), m_AdjustDscList.GetItemText(nRow, 6));
	ini.WriteString(strSelect, _T("T_PeakY"), m_AdjustDscList.GetItemText(nRow, 7));
	ini.WriteString(strSelect, _T("B_PeakX"), m_AdjustDscList.GetItemText(nRow, 8));
	ini.WriteString(strSelect, _T("B_PeakY"), m_AdjustDscList.GetItemText(nRow, 9));

	ini.WriteString(strSelect, _T("1st_World_X"), m_AdjustDscList.GetItemText(nRow, 20));
	ini.WriteString(strSelect, _T("1st_World_Y"), m_AdjustDscList.GetItemText(nRow, 21));
	ini.WriteString(strSelect, _T("2nd_World_X"), m_AdjustDscList.GetItemText(nRow, 22));
	ini.WriteString(strSelect, _T("2nd_World_Y"), m_AdjustDscList.GetItemText(nRow, 23));
	ini.WriteString(strSelect, _T("3rd_World_X"), m_AdjustDscList.GetItemText(nRow, 24));
	ini.WriteString(strSelect, _T("3rd_World_Y"), m_AdjustDscList.GetItemText(nRow, 25));
	ini.WriteString(strSelect, _T("4th_World_X"), m_AdjustDscList.GetItemText(nRow, 26));
	ini.WriteString(strSelect, _T("4th_World_Y"), m_AdjustDscList.GetItemText(nRow, 27));

	ini.WriteString(strSelect, _T("CenterX"), m_AdjustDscList.GetItemText(nRow, 10));
	ini.WriteString(strSelect, _T("CenterY"), m_AdjustDscList.GetItemText(nRow, 11));

	CString strData = m_AdjustDscList.GetItemText(nRow, 12);
	if(strData.Compare(_T("0")) == 0)
	{
		ini.WriteString(strSelect, _T("Select"), _T("0"));
	}
	else
	{
		ini.WriteString(strSelect, _T("Select"), _T(""));
	}

	if(m_AdjustDscList.GetCheck(nRow))
	{
		ini.WriteString(strSelect, _T("K-Zone"), _T("TRUE"));
	}
	else
	{
		ini.WriteString(strSelect, _T("K-Zone"), _T("FALSE"));
	}

}

void CESMAutoAdjustDlg::SavePointDataColumn(CString strFileName)
{
	CESMIni ini;
	CFile file;
	file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite);
	file.Close();

	if(!ini.SetIniFilename (strFileName))
		return;

	ini.WriteInt(_T("MovieSize"), _T("HEIGHT"), m_nHeight);
	ini.WriteInt(_T("MovieSize"), _T("WIDTH"), m_nWidth);

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);

		ini.WriteString(strSelect, _T("1st_World_X"), m_AdjustDscList.GetItemText(i, 20));
		ini.WriteString(strSelect, _T("1st_World_Y"), m_AdjustDscList.GetItemText(i, 21));
		ini.WriteString(strSelect, _T("2nd_World_X"), m_AdjustDscList.GetItemText(i, 22));
		ini.WriteString(strSelect, _T("2nd_World_Y"), m_AdjustDscList.GetItemText(i, 23));
		ini.WriteString(strSelect, _T("3rd_World_X"), m_AdjustDscList.GetItemText(i, 24));
		ini.WriteString(strSelect, _T("3rd_World_Y"), m_AdjustDscList.GetItemText(i, 25));
		ini.WriteString(strSelect, _T("4th_World_X"), m_AdjustDscList.GetItemText(i, 26));
		ini.WriteString(strSelect, _T("4th_World_Y"), m_AdjustDscList.GetItemText(i, 27));

		ini.WriteString(strSelect, _T("CenterX"), m_AdjustDscList.GetItemText(i, 10));
		ini.WriteString(strSelect, _T("CenterY"), m_AdjustDscList.GetItemText(i, 11));

		CString strData = m_AdjustDscList.GetItemText(i, 12);
		if(strData.Compare(_T("0")) == 0)
		{
			ini.WriteString(strSelect, _T("Select"), _T("0"));
		}
		else
		{
			ini.WriteString(strSelect, _T("Select"), _T(""));
		}

		if(m_AdjustDscList.GetCheck(i))
		{
			ini.WriteString(strSelect, _T("K-Zone"), _T("TRUE"));
		}
		else
		{
			ini.WriteString(strSelect, _T("K-Zone"), _T("FALSE"));
		}
	}
}

void CESMAutoAdjustDlg::LoadPointData(CString strFileName)
{
	CESMIni ini;
	if(!ini.SetIniFilename (strFileName))
		return;

	int nHeight = 0, nWidth = 0;
	nHeight = ini.GetInt(_T("MovieSize"), _T("HEIGHT"), 0);
	nWidth = ini.GetInt(_T("MovieSize"), _T("WIDTH"), 0);
	if(nHeight != 0)
		m_nHeight = nHeight;

	if(nWidth != 0)
		m_nWidth = nWidth;

	CString strData;
	//stTemplateInfo axis;
	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);
		strData = ini.GetString(strSelect, _T("T_LBX"));
		m_AdjustDscList.SetItemText(i, 2, strData);

		strData = ini.GetString(strSelect, _T("T_LBY"));
		m_AdjustDscList.SetItemText(i, 3, strData);

		strData = ini.GetString(strSelect, _T("T_RBX"));
		m_AdjustDscList.SetItemText(i, 4, strData);

		strData = ini.GetString(strSelect, _T("T_RBY"));
		m_AdjustDscList.SetItemText(i, 5, strData);

		strData = ini.GetString(strSelect, _T("T_PeakX"));
		m_AdjustDscList.SetItemText(i, 6, strData);

		strData = ini.GetString(strSelect, _T("T_PeakY"));
		m_AdjustDscList.SetItemText(i, 7, strData);

		strData = ini.GetString(strSelect, _T("B_PeakX"));
		m_AdjustDscList.SetItemText(i, 8, strData);

		strData = ini.GetString(strSelect, _T("B_PeakY"));
		m_AdjustDscList.SetItemText(i, 9, strData);

		strData = ini.GetString(strSelect, _T("1st_World_X")); 
		m_AdjustDscList.SetItemText(i, 20, strData);

		strData = ini.GetString(strSelect, _T("1st_World_Y")); 
		m_AdjustDscList.SetItemText(i, 21, strData);

		strData = ini.GetString(strSelect, _T("2nd_World_X")); 
		m_AdjustDscList.SetItemText(i, 22, strData);

		strData = ini.GetString(strSelect, _T("2nd_World_Y")); 
		m_AdjustDscList.SetItemText(i, 23, strData);

		strData = ini.GetString(strSelect, _T("3rd_World_X")); 
		m_AdjustDscList.SetItemText(i, 24, strData);

		strData = ini.GetString(strSelect, _T("3rd_World_Y")); 
		m_AdjustDscList.SetItemText(i, 25, strData);

		strData = ini.GetString(strSelect, _T("4th_World_X")); 
		m_AdjustDscList.SetItemText(i, 26, strData);

		strData = ini.GetString(strSelect, _T("4th_World_Y")); 
		m_AdjustDscList.SetItemText(i, 27, strData);

		strData = ini.GetString(strSelect, _T("CenterX")); 
		m_AdjustDscList.SetItemText(i, 10, strData);

		strData = ini.GetString(strSelect, _T("CenterY")); 
		m_AdjustDscList.SetItemText(i, 11, strData);

		strData = ini.GetString(strSelect, _T("K-Zone"));
		if(strData.Compare(_T("TRUE")) == 0)
		{
			m_AdjustDscList.SetCheck(i, TRUE);
		}
		else
		{
			m_AdjustDscList.SetCheck(i, FALSE);
		}

		strData = ini.GetString(strSelect, _T("Select"));
		if(strData.Compare(_T("0")) == 0)
		{
			m_AdjustDscList.SetItemText(i, 12, strData);
		}
		else
		{
			m_AdjustDscList.SetItemText(i, 12, strData);
		}
	}	
	InputAutoAdjustDataFromCtrlList();
}

void CESMAutoAdjustDlg::Showimage()
{
	CListCtrl* pList;
	int nCount, nMaxCount;

	pList = &m_AdjustDscList;
	m_pAdjustImg = &m_StAdjustImage;

	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );
	if( nSelectedItem < 0)
		return;	

	m_nSelectedNum = nSelectedItem;
	CString strSelect = pList->GetItemText(nSelectedItem, 1);
	DscAdjustInfo* pAdjustInfo = NULL;
	pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(nSelectedItem);	

	if(pAdjustInfo->pBmpBits != NULL || m_pAdjustImg->m_memDC == NULL)
	{
		DrawImage();
		DrawImage2();
	}
	else
	{
		DeleteImage();
	}
}

void CESMAutoAdjustDlg::DrawImage()
{
	CListCtrl* pList = &m_AdjustDscList;
	m_pAdjustImg = &m_StAdjustImage;
	DscAdjustInfo* pAdjustInfo = NULL;

	UpdateData();
	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );
	if( nSelectedItem < 0)
		return;

	m_nSelectedNum = nSelectedItem;
	CString strSelect = pList->GetItemText(nSelectedItem, 1);
	CString strLine3;
	GetDlgItem(IDC_EDIT_TEMPLATE_CENTER)->GetWindowText(strLine3);

	pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(nSelectedItem);

	if(pAdjustInfo->pBmpBits == NULL)
	{
		AfxMessageBox(_T("Wait Loading..."));
		return;
	}

	m_pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3, NULL);

	DrawCenterPointAll();

	m_pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
	m_pAdjustImg->DrawLine(pAdjustInfo->nHeight/2, RGB(0,0,0));
	m_pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
	m_pAdjustImg->Redraw();

	m_nSelectPos = 0;
}

void CESMAutoAdjustDlg::DrawImage2()
{
	CListCtrl* pList = &m_AdjustDscList;

	m_pAdjustImg = &m_StAdjustWorldCoordinateImage;
	DscAdjustInfo* pAdjustInfo = NULL;

	UpdateData();
	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );
	if( nSelectedItem < 0)
		return;

	m_nSelectedNum = nSelectedItem;
	pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(nSelectedItem);

	ShowWorldCoordnationImage();

	vector<CPoint> ptPoint;
	for(int j = 0; j < 4; j++)
	{
		ptPoint.push_back(CPoint());
		ptPoint[j].x = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, 20 + j*2);
		ptPoint[j].y = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, 20 + j*2 + 1);
	}

	COLORREF color = RGB(255, 0, 255);
	for ( int i = 0; i < ptPoint.size(); i++ )
	{
		m_nPosX = ptPoint[i].x;
		m_nPosY = ptPoint[i].y;

		if(i < 4)
		{
			int nCircle = m_StAdjustWorldCoordinateImage.m_nCircleSize;
			CRect rtOutLine(
				ptPoint[i].x-nCircle,
				ptPoint[i].y-nCircle,
				ptPoint[i].x+nCircle,
				ptPoint[i].y+nCircle);
			m_StAdjustWorldCoordinateImage.DrawRect(rtOutLine, Coloring(i+1));
		}

		if(i == 1)
		{
			int nPosX_2 = ptPoint[i].x;
			int nPosY_2 = ptPoint[i].y;
			int nPosX_1 = ptPoint[i-1].x;
			int nPosY_1 = ptPoint[i-1].y;
			m_StAdjustWorldCoordinateImage.DrawLine(nPosX_2, nPosY_2, nPosX_1, nPosY_1, color);
		}

		if(i == 2)
		{
			int nPosX_3 = ptPoint[i].x;
			int nPosY_3 = ptPoint[i].y;
			int nPosX_2 = ptPoint[i-1].x;
			int nPosY_2 = ptPoint[i-1].y;
			m_StAdjustWorldCoordinateImage.DrawLine(nPosX_3, nPosY_3, nPosX_2, nPosY_2, color);
		}

		if(i == 3)
		{
			int nPosX_4 = ptPoint[i].x;
			int nPosY_4 = ptPoint[i].y;
			int nPosX_3 = ptPoint[i-1].x;
			int nPosY_3 = ptPoint[i-1].y;
			int nPosX_2 = ptPoint[i-2].x;
			int nPosY_2 = ptPoint[i-2].y;
			int nPosX_1 = ptPoint[i-3].x;
			int nPosY_1 = ptPoint[i-3].y;

			m_StAdjustWorldCoordinateImage.DrawLine(nPosX_4, nPosY_4, nPosX_2, nPosY_2, color);
			m_StAdjustWorldCoordinateImage.DrawLine(nPosX_3, nPosY_3, nPosX_1, nPosY_1, color);
			m_StAdjustWorldCoordinateImage.DrawLine(nPosX_4, nPosY_4, nPosX_1, nPosY_1, color);
			m_StAdjustWorldCoordinateImage.DrawLine(nPosX_4, nPosY_4, nPosX_3, nPosY_3, color);
		}

		m_StAdjustWorldCoordinateImage.Redraw();
	}

	m_pAdjustImg->Redraw();

	m_nSelectPos = 0;
}

void CESMAutoAdjustDlg::DeleteImage()
{
	CESMAutoAdjustImage* pAdjustImg = &m_StAdjustImage;	
	pAdjustImg->m_memDC->FillSolidRect(0,0,m_StAdjustImage.m_nImageWidth,m_StAdjustImage.m_nImageHeight,GetSysColor(COLOR_BTNFACE));
}

void CESMAutoAdjustDlg::LoadData()
{
	m_pTemplateMgr->ClearAdjInfo();
	m_pAutoAdjustMgr->ClearAdjInfo();

	m_AdjustDscList.DeleteAllItems();
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();
	for(int i =0; i< m_pAutoAdjustMgr->GetDscCount(); i++)
	{		
		CString strTp;
		strTp.Format(_T("%d"), i + 1);

		m_AdjustDscList.InsertItem(i, strTp);
		m_AdjustDscList.SetItemText(i, 1, m_pAutoAdjustMgr->GetDscAt(i)->strDscName);

		CString strDSC = m_AdjustDscList.GetItemText(i,1);
		m_pAutoAdjustMgr->AddAdjInfo(pMovieMgr->GetAdjustData(strDSC));
		m_pTemplateMgr->AddAdjInfo(pMovieMgr->GetAdjustData(strDSC));
	}

	CString strTemp;
	m_RdImageViewColor = 0;
	m_nRdDetectColor = 0;
	m_pAutoAdjustMgr->GetResolution(m_nWidth,m_nHeight);

	if( m_hTheadHandle )
	{
		CloseHandle(m_hTheadHandle);
		m_hTheadHandle = NULL;
	}

	m_hTheadHandle = (HANDLE) _beginthreadex(NULL, 0, GetMovieDataThread, (void *)this, 0, NULL);

	CString strSize = _T("5");
	GetDlgItem(IDC_EDIT_TEMPLATE_SIZE)->SetWindowText(strSize);
	m_StAdjustImage.m_nCircleSize =  _ttoi(strSize) * 4;
}

unsigned WINAPI CESMAutoAdjustDlg::GetMovieDataThread(LPVOID param)
{	
	CESMAutoAdjustDlg* pAdjustMgrDlg = (CESMAutoAdjustDlg*)param;
	DscAdjustInfo* pAdjustInfo = NULL;
	
	//wgkim 170728
	int nMovieIndex = 0;
	int nRealFrameIndex = 0;
	int nShiftFrame = ESMGetSelectedFrameNumber();
	ESMGetMovieIndex(nShiftFrame, nMovieIndex, nRealFrameIndex);

	for(int i = 0; i < pAdjustMgrDlg->m_pAutoAdjustMgr->GetDscCount(); i++)
		pAdjustMgrDlg->m_vecIsImageLoad.push_back(FALSE);

	for(int i =0; i< pAdjustMgrDlg->m_pAutoAdjustMgr->GetDscCount(); i++)
	{
		pAdjustInfo = pAdjustMgrDlg->m_pAutoAdjustMgr->GetDscAt(i);
		FFmpegManager FFmpegMgr(FALSE);
		CString strFolder, strDscId, strDscIp;
		strDscId = pAdjustInfo->strDscName;
		strDscIp = pAdjustInfo->strDscIp;
		strFolder = ESMGetMoviePath(strDscId, nShiftFrame);
		BOOL bReverse = pAdjustInfo->bReverse;
		if( strFolder == _T(""))
		{
			return 0;
		}

		BYTE* pBmpBits = NULL;
		int nWidht =0, nHeight = 0;
		FFmpegMgr.GetCaptureImage(
			strFolder, 
			&pBmpBits, 
			nRealFrameIndex/*pAdjustMgrDlg->m_frameIndex*/, 
			&nWidht, 
			&nHeight, 
			ESMGetGopSize(),
			bReverse);

		if( pBmpBits != NULL)
		{
			pAdjustInfo->pBmpBits = pBmpBits;
			pAdjustInfo->nWidht = nWidht;
			pAdjustInfo->nHeight = nHeight;
		}
		pAdjustMgrDlg->m_vecIsImageLoad[i] = TRUE;

		if(pAdjustMgrDlg->m_bThreadCloseFlag == TRUE)
		{
			return 0;
		}

	}
	pAdjustMgrDlg->m_bLoadAdjustImageAll = TRUE;
	return 0;
}

void CESMAutoAdjustDlg::OnLvnItemchanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	//K-Zone Check Event As Mouse Click
	if(GetKeyState(VK_LBUTTON) & 0x8000)
	{
		int ns = pNMLV->uNewState & LVIS_STATEIMAGEMASK;
		if ( ( ns & 0x2000 ) != 0 )
		{
			TRACE("CHECKING\n");

			for(int i = 0; i < m_pAutoAdjustMgr->GetDscCount(); i++)
			{
				DscAdjustInfo* pAdjustInfo = NULL;
				pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);
				BOOL bUseKZone = m_AdjustDscList.GetCheck(i);
				m_pAutoAdjustMgr->SetUseKZoneAt(pAdjustInfo->strDscName, bUseKZone);
			}

			CString strFileName;
			strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
			SavePointData(strFileName);
		}
		else if ( ( ns & 0x1000 ) != 0 )
		{
			TRACE("UNCHECKING\n");

			for(int i = 0; i < m_pAutoAdjustMgr->GetDscCount(); i++)
			{
				DscAdjustInfo* pAdjustInfo = NULL;
				pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);
				m_pAutoAdjustMgr->SetUseKZoneAt(pAdjustInfo->strDscName, m_AdjustDscList.GetCheck(i));
			}

			CString strFileName;
			strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
			SavePointData(strFileName);
		}
		else
		{
			TRACE("NOTHING CHK\n");
		}
	}

	if(m_AdjustDscList.GetSelectedCount() == 1)
	{
		int nSelectedNum = m_AdjustDscList.GetNextItem( -1, LVNI_SELECTED) + 1;
		CString strSelectedNum;
		strSelectedNum.Format(_T("%d ~ %d"), 1, m_pAutoAdjustMgr->GetDscCount());
		GetDlgItem(IDC_AUTOADJUST_CAMERA_RANGE_VALUE)->SetWindowText(strSelectedNum);

		Showimage();
		Invalidate();
	}
	else
	{
		int nStartSelectedNum = m_AdjustDscList.GetNextItem( -1, LVNI_SELECTED) + 1;
		int nEndSelectedNum = nStartSelectedNum + m_AdjustDscList.GetSelectedCount() - 1;
		CString strSelectedNum;
		strSelectedNum.Format(_T("%d ~ %d"), nStartSelectedNum, nEndSelectedNum);
		GetDlgItem(IDC_AUTOADJUST_CAMERA_RANGE_VALUE)->SetWindowText(strSelectedNum);

		Invalidate();
	}
	*pResult = 0;
}

void CESMAutoAdjustDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	CListCtrl* pList;
	//CESMAutoAdjustImage* pAdjustImg;

	pList = &m_AdjustDscList;
	m_pAdjustImg = GetStAdjustImageControl(point);
	
	if(m_pAdjustImg == NULL)
	{
		return;
	}

	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );

	CString strSize;
	CString strText;
	GetDlgItem(IDC_EDIT_TEMPLATE_SIZE)->GetWindowText(strSize);
	m_pAdjustImg->m_nCircleSize =  _ttoi(strSize) * 4;

	if(m_pAdjustImg == &m_StAdjustImage)
	{
		destroyWindow("Adjust Preview");
		m_StAdjustImage.FindExactPosition(m_nPosX,m_nPosY);

		CString LocStr;
		m_ctrlCheckPoint.GetLBText(m_ctrlCheckPoint.GetCurSel(),LocStr);

		if(LocStr == "4th") m_nSelectPos = 3;
		else if(LocStr == "3rd") m_nSelectPos = 2;
		else if(LocStr == "1st") m_nSelectPos = 0;
		else if(LocStr == "2nd") m_nSelectPos = 1;

		if(LocStr == "None")
		{
			static int pos = 0;

			setDscListItem(pList, nSelectedItem, pos%4 * 2 + 2, (double)m_nPosX);
			setDscListItem(pList, nSelectedItem, pos%4 * 2 + 3, (double)m_nPosY);

			pos++;
		}
		else
		{
			strText.Format(_T("%d"), m_nPosX);
			setDscListItem(pList, nSelectedItem, m_nSelectPos * 2 + 2, (double)m_nPosX);

			strText.Format(_T("%d"), m_nPosY);
			setDscListItem(pList, nSelectedItem, m_nSelectPos * 2 + 3, (double)m_nPosY);

			if( m_bAutoMove == TRUE)
			{
				int nAllItem = m_AdjustDscList.GetItemCount();
				if(nAllItem > nSelectedItem+1)
				{
					//ImageOverlay
					if(m_StAdjustImage.GetUsingRuler() == TRUE)
					{
						m_StAdjustImage.ClearDrawRuler();
						m_StAdjustImage.SetUsingRuler(FALSE);
					}

					ESMLog(5,_T("Change Next Item"));

					int nSelectedItem = m_AdjustDscList.GetNextItem( -1, LVNI_SELECTED );
					m_AdjustDscList.SetItemState(nSelectedItem+1,LVIS_SELECTED,  LVIS_SELECTED);
					m_AdjustDscList.SetItemState(nSelectedItem, 0 ,  LVIS_SELECTED);
					m_AdjustDscList.SetFocus();
					m_nSelectPos = 0;
				}
			}
			UpdateData();
		}
		DrawImage();

		CString strFileName;
		strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
		SavePointData(strFileName, nSelectedItem);
	}

	if(m_pAdjustImg == &m_StAdjustWorldCoordinateImage)
	{
		POSITION posItem = m_AdjustDscList.GetFirstSelectedItemPosition();

		int nSelectedDscCount = m_AdjustDscList.GetSelectedCount();

		if(nSelectedDscCount == 1)
		{
			m_nStartSelectedItem = 0;
			m_nEndSelectedItem = 0;
			nSelectedDscCount = m_pAutoAdjustMgr->GetDscCount();
		}
		else
		{
			for (int i = 0; i < nSelectedDscCount; i++)
			{
				int nIndex = m_AdjustDscList.GetNextSelectedItem(posItem);

				if(i == 0)
					m_nStartSelectedItem = nIndex;
				if(i == nSelectedDscCount - 1)
					m_nEndSelectedItem = nIndex;
			}
		}

		CPoint ptPoint = CPoint(m_nPosX, m_nPosY);
		m_pAutoAdjustMgr->SetSquarePointInWorld(ptPoint);

		vector<CPoint> vecPtPoints = m_pAutoAdjustMgr->GetSquarePointInWorld();

		COLORREF color = RGB(255, 0, 255);
		for ( int i = 0; i < vecPtPoints.size(); i++ )
		{
			m_nPosX = vecPtPoints[i].x;
			m_nPosY = vecPtPoints[i].y;

			for(int j = m_nStartSelectedItem; j < m_nStartSelectedItem + nSelectedDscCount; j++)
			{
				if(i == vecPtPoints.size()-1)
				{
					CString strData;		
					strData.Format(_T("%d"), vecPtPoints[vecPtPoints.size()-1].x);
					m_AdjustDscList.SetItemText(j, 20 + (vecPtPoints.size()-1)*2, strData);
					strData.Format(_T("%d"), vecPtPoints[vecPtPoints.size()-1].y);
					m_AdjustDscList.SetItemText(j, 21 + (vecPtPoints.size()-1)*2, strData);
				}
			}

			if(i < 4)
			{
				int nCircle = m_StAdjustWorldCoordinateImage.m_nCircleSize;
				CRect rtOutLine(
					vecPtPoints[i].x-nCircle,
					vecPtPoints[i].y-nCircle,
					vecPtPoints[i].x+nCircle,
					vecPtPoints[i].y+nCircle);
				m_StAdjustWorldCoordinateImage.DrawRect(rtOutLine, Coloring(i+1));
			}

			if(i == 1)
			{
				int nPosX_2 = vecPtPoints[i].x;
				int nPosY_2 = vecPtPoints[i].y;
				int nPosX_1 = vecPtPoints[i-1].x;
				int nPosY_1 = vecPtPoints[i-1].y;
				m_StAdjustWorldCoordinateImage.DrawLine(nPosX_2, nPosY_2, nPosX_1, nPosY_1, color);
			}

			if(i == 2)
			{
				int nPosX_3 = vecPtPoints[i].x;
				int nPosY_3 = vecPtPoints[i].y;
				int nPosX_2 = vecPtPoints[i-1].x;
				int nPosY_2 = vecPtPoints[i-1].y;
				m_StAdjustWorldCoordinateImage.DrawLine(nPosX_3, nPosY_3, nPosX_2, nPosY_2, color);
			}

			if(i == 3)
			{
				int nPosX_4 = vecPtPoints[i].x;
				int nPosY_4 = vecPtPoints[i].y;
				int nPosX_3 = vecPtPoints[i-1].x;
				int nPosY_3 = vecPtPoints[i-1].y;
				int nPosX_2 = vecPtPoints[i-2].x;
				int nPosY_2 = vecPtPoints[i-2].y;
				int nPosX_1 = vecPtPoints[i-3].x;
				int nPosY_1 = vecPtPoints[i-3].y;

				m_StAdjustWorldCoordinateImage.DrawLine(nPosX_4, nPosY_4, nPosX_2, nPosY_2, color);
				m_StAdjustWorldCoordinateImage.DrawLine(nPosX_3, nPosY_3, nPosX_1, nPosY_1, color);
				m_StAdjustWorldCoordinateImage.DrawLine(nPosX_4, nPosY_4, nPosX_1, nPosY_1, color);
				m_StAdjustWorldCoordinateImage.DrawLine(nPosX_4, nPosY_4, nPosX_3, nPosY_3, color);
			}

			m_StAdjustWorldCoordinateImage.Redraw();
		}

		CString strFileName;
		strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
		SavePointDataColumn(strFileName);
	}

	CDialogEx::OnRButtonDown(nFlags, point);
}

void CESMAutoAdjustDlg::OnRButtonUp(UINT nFlags, CPoint point)
{
	CDialogEx::OnRButtonUp(nFlags, point);
}

void CESMAutoAdjustDlg::OnMButtonUp(UINT nFlags, CPoint point)
{
	//ImageOverlay
	if(m_pAdjustImg->GetUsingRuler())
	{
		DrawImage();
		m_pAdjustImg->RulerColorChange();
		m_pAdjustImg->Redraw();
		return CDialogEx::OnMButtonUp(nFlags, point);
	}

	CDialogEx::OnMButtonUp(nFlags, point);
}

void CESMAutoAdjustDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//CESMAutoAdjustImage* pAdjustImg;
	m_pAdjustImg = GetStAdjustImageControl(point);//&m_StAdjustImage;
	if(m_pAdjustImg == NULL)
		return;

	CString strPos;

	//-- 2014-08-31 hongsu@esmlab.com
	//-- Get Image Position from Mouse Point
	CPoint ptImage;
	ptImage = GetPointFromMouse(point);

	strPos.Format(_T("%d"), ptImage.x);
	m_nPosX = ptImage.x - 1;
	GetDlgItem(IDC_TEMPLATE_MOUSEPOSX)->SetWindowText(strPos);

	strPos.Format(_T("%d"), ptImage.y);
	m_nPosY = ptImage.y - 1;
	GetDlgItem(IDC_TEMPLATE_MOUSEPOSY)->SetWindowText(strPos);

	//ImageOverlay
	BOOL bLeftButton = nFlags & MK_LBUTTON;
	if(m_pAdjustImg == &m_StAdjustImage && bLeftButton)
	{
		if(m_pAdjustImg->GetUsingRuler())
		{
			DrawImage();
			m_pAdjustImg->DrawRuler(m_nPosX, m_nPosY);
			m_pAdjustImg->Redraw();
			return;
		}
	}

	m_pAdjustImg->OnMouseMove(nFlags, point);

	CDialogEx::OnMouseMove(nFlags, point);
}

BOOL CESMAutoAdjustDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint point)
{
	ScreenToClient(&point);

	//CESMAutoAdjustImage* pAdjustImg;
	m_pAdjustImg = GetStAdjustImageControl(point);

	if(m_pAdjustImg == NULL)
		return FALSE;

	CPoint ptImage;
	ptImage = GetPointFromMouse(point);

	if( zDelta > 0)
	{
		if(  m_pAdjustImg->GetMultiple() <= 0.2)
			return TRUE;
		m_pAdjustImg->ImageZoom(ptImage, TRUE);
	}
	else
		m_pAdjustImg->ImageZoom(ptImage, FALSE);

	CString strTp;
	strTp.Format(_T("%.03lf"), m_pAdjustImg->GetMultiple());
	GetDlgItem(IDC_TEMPLATE_IMAGESIZE)->SetWindowText(strTp);

	return CDialogEx::OnMouseWheel(nFlags, zDelta, point);
}

CESMAutoAdjustImage* CESMAutoAdjustDlg::GetStAdjustImageControl(CPoint point)
{
	CString strPos;
	CRect rect;

	m_StAdjustImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	if(!(point.x - rect.left < 0 || 
		point.x > rect.right || 
		point.y - rect.top < 0 || 
		point.y > rect.bottom))
	{
		return &m_StAdjustImage;
	}

	m_StAdjustWorldCoordinateImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	if(!(point.x - rect.left < 0 || 
		point.x > rect.right || 
		point.y - rect.top < 0 || 
		point.y > rect.bottom))
	{
		return &m_StAdjustWorldCoordinateImage;
	}

	return NULL;
}

CPoint CESMAutoAdjustDlg::GetPointFromMouse(CPoint pt)
{
	CESMAutoAdjustImage* pAdjustImg;
	pAdjustImg = GetStAdjustImageControl(pt);

	CRect rect;
	pAdjustImg->GetWindowRect(&rect);
	ScreenToClient(&rect);

	CPoint ptRet;
	pt.x = pt.x - rect.left;
	pt.y = pt.y - rect.top;

	CPoint ptImage = pAdjustImg->GetImagePos();
	ptRet.x = (int)(ptImage.x * pAdjustImg->GetMultiple() + pt.x * pAdjustImg->GetMultiple());
	ptRet.y = (int)(ptImage.y * pAdjustImg->GetMultiple() + pt.y * pAdjustImg->GetMultiple());

	//ESMLog(5, _T("Moust Point x:%d y:%d"), ptRet.x, ptRet.y);
	return ptRet;
}

void CESMAutoAdjustDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	//CESMAutoAdjustImage* pAdjustImg;
	m_pAdjustImg= GetStAdjustImageControl(point);

	if(m_pAdjustImg == NULL)
		return;

	//ImageOverlay
	if(m_pAdjustImg == &m_StAdjustImage)
	{
		if(m_pAdjustImg->GetUsingRuler())
		{
			m_pAdjustImg->StartDrawRuler(m_nPosX, m_nPosY);
			return;
		}
	}
	
	m_pAdjustImg->OnLButtonDown(nFlags, point);
	m_pAdjustImg->SetFocus();

	CDialogEx::OnLButtonDown(nFlags, point);
}

void CESMAutoAdjustDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	//CESMAutoAdjustImage* pAdjustImg;
	m_pAdjustImg = GetStAdjustImageControl(point);

	if(m_pAdjustImg == NULL)
		return;

	//ImageOverlay
	if(m_pAdjustImg == &m_StAdjustImage)
	{
		if(m_pAdjustImg->GetUsingRuler())
		{
			m_pAdjustImg->EndDrawRuler();
			return;
		}
	}

	m_pAdjustImg->OnLButtonUp(nFlags, point);

	CDialogEx::OnLButtonUp(nFlags, point);
}

void CESMAutoAdjustDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	POSITION posItem = m_AdjustDscList.GetFirstSelectedItemPosition();

	int nSelectedDscCount = m_AdjustDscList.GetSelectedCount();

	if(nSelectedDscCount == 1)
	{
		m_nStartSelectedItem = 0;
		m_nEndSelectedItem = 0;
		nSelectedDscCount = m_pAutoAdjustMgr->GetDscCount();
	}
	else
	{
		for (int i = 0; i < nSelectedDscCount; i++)
		{
			int nIndex = m_AdjustDscList.GetNextSelectedItem(posItem);

			if(i == 0)
				m_nStartSelectedItem = nIndex;
			if(i == nSelectedDscCount - 1)
				m_nEndSelectedItem = nIndex;
		}
	}

	m_pAdjustImg = GetStAdjustImageControl(point);
	if( m_pAdjustImg == NULL )
		return;

	vector<CPoint> pAutoAdjustWorldPoint = m_pAutoAdjustMgr->GetSquarePointInWorld();

	if(m_pAdjustImg == &m_StAdjustWorldCoordinateImage)
	{
		m_pAutoAdjustMgr->ClearSquarePointInWorld();
		ShowWorldCoordnationImage();
	}
	if(m_pAdjustImg == &m_StAdjustImage)
	{
		destroyWindow("Adjust Preview");
		m_pCenterPoint = GetPointFromMouse(point);

		CString strData;		
		strData.Format(_T("%d"), m_pCenterPoint.x);
		m_AdjustDscList.SetItemText(m_nSelectedNum, 10, strData);
		strData.Format(_T("%d"), m_pCenterPoint.y);
		m_AdjustDscList.SetItemText(m_nSelectedNum, 11, strData);

		int nCircle = m_StAdjustImage.m_nCircleSize;
		CRect rtOutLine(
			m_pCenterPoint.x-nCircle,
			m_pCenterPoint.y-nCircle,
			m_pCenterPoint.x+nCircle,
			m_pCenterPoint.y+nCircle);
		m_StAdjustImage.DrawRect(rtOutLine, Coloring(5));
		m_StAdjustImage.Redraw();

		for(int i = m_nStartSelectedItem; i < m_nStartSelectedItem + nSelectedDscCount; i++)
		{
			DscAdjustInfo* pAdjustInfo = NULL;
			pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);

			CString strDscId = pAdjustInfo->strDscName;
			CT2CA pszConvertedAnsiDscId(strDscId);
			string strAnsiDscId(pszConvertedAnsiDscId);

			CPoint pt1, pt2, pt3, pt4;
			pt1.x = _ttoi(m_AdjustDscList.GetItemText(i, 2)); pt1.y = _ttoi(m_AdjustDscList.GetItemText(i, 3));
			pt2.x = _ttoi(m_AdjustDscList.GetItemText(i, 4)); pt2.y = _ttoi(m_AdjustDscList.GetItemText(i, 5));
			pt3.x = _ttoi(m_AdjustDscList.GetItemText(i, 6)); pt3.y = _ttoi(m_AdjustDscList.GetItemText(i, 7));
			pt4.x = _ttoi(m_AdjustDscList.GetItemText(i, 8)); pt4.y = _ttoi(m_AdjustDscList.GetItemText(i, 9));

			vector<CPoint> ptPoint;
			for(int j = 0; j < 4; j++)
			{
				ptPoint.push_back(CPoint());
				ptPoint[j].x = getDSCListItem(&m_AdjustDscList, i, 20 + j*2);
				ptPoint[j].y = getDSCListItem(&m_AdjustDscList, i, 20 + j*2 + 1);
			}
			m_pAutoAdjustMgr->SetSquarePointInWorldAt(strDscId, ptPoint);

			vector<CPoint> vecPtPoints = m_pAutoAdjustMgr->GetSquarePointInWorldAt(strDscId);

			for(int j = 0; j < 4; j++)
			{
				strData.Format(_T("%d"), vecPtPoints.at(j).x);
				m_AdjustDscList.SetItemText(i, 20 + 2*j, strData);
				strData.Format(_T("%d"), vecPtPoints.at(j).y);
				m_AdjustDscList.SetItemText(i, 21 + 2*j, strData);
			}

			//180723 wgkim
			if(vecPtPoints.size() != 4)
			{
				ESMLog(5, _T("not Enough 3D Points (do need 4 points)"));
				return;
			}

			m_pAutoAdjustMgr->AddAdjustSquareInfo(i, strDscId, vecPtPoints, pt1, pt2, pt3, pt4);
		}

		int nSelectedItem = m_AdjustDscList.GetNextItem( -1, LVNI_SELECTED );
		CString strStartDscId = m_AdjustDscList.GetItemText(m_nStartSelectedItem, 1);
		CString strEndDscId = m_AdjustDscList.GetItemText(m_nEndSelectedItem, 1);
		m_pAutoAdjustMgr->SetInitCenterPoint(strStartDscId, strEndDscId, m_pCenterPoint);

		for(int i = 0; i < m_pAutoAdjustMgr->GetDscCount(); i++)
		//for(int i  = 0; i < m_pAutoAdjustMgr->GetDscCount(); i++)
		{
			DscAdjustInfo* pAdjustInfo = NULL;
			pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);

			CString strData;
			CPoint ptCenterPoint = m_pAutoAdjustMgr->GetCenterPoint(pAdjustInfo->strDscName);
			
			strData.Format(_T("%d"), ptCenterPoint.x);
			m_AdjustDscList.SetItemText(i, 10, strData);
			strData.Format(_T("%d"), ptCenterPoint.y);
			m_AdjustDscList.SetItemText(i, 11, strData);
		}

		for(int i = m_nStartSelectedItem; i < m_nStartSelectedItem + nSelectedDscCount; i++)
		//for(int i = 0; i < m_pAutoAdjustMgr->GetDscCount(); i++)
		{
			DscAdjustInfo* pAdjustInfo = NULL;
			pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);

			CString strDscId = pAdjustInfo->strDscName;
			CT2CA pszConvertedAnsiDscId(strDscId);
			string strAnsiDscId(pszConvertedAnsiDscId);

			m_pSquareCalibration->CalibrationUsingSquarePoints(strAnsiDscId);
		}
	}

	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	SavePointDataColumn(strFileName);

	CDialogEx::OnLButtonDblClk(nFlags, point);
}

void CESMAutoAdjustDlg::OnBnClickedBtnAdjustPointReset()
{
	if(AfxMessageBox(_T("Are you want to Reset?"),MB_YESNO)==IDNO)
	{
		return;
	}

	//181019
	CESMFileOperation fo;
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	fo.Delete(strFileName);
	CString strData = _T("");

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strSelect = m_AdjustDscList.GetItemText(i, 1);

		for(int j = 2; j < 28; j++)
			m_AdjustDscList.SetItemText(i, j, strData);
	}
}

void CESMAutoAdjustDlg::OnBnClickedBtn2ndSavePoint()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));
	CString szFilter = _T("Point List (*.pta)|*.pta|");

#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  	
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = _T("Point Save");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();	
		if( strFileName.Right(4) != _T(".pta"))
			strFileName = strFileName + _T(".pta");
		SavePointData(strFileName);

		//Save Image Data
		CString strAdjustFolder;
		strAdjustFolder.Format(_T("%s\\img"),ESMGetPath(ESM_PATH_SETUP));
		CreateDirectory(strAdjustFolder, NULL);
	}
}

void CESMAutoAdjustDlg::OnBnClickedBtn2ndLoadPoint()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));

	CString szFilter = _T("Point List (*.pta)|*.pta|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = _T("Point Load");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();
		LoadPointData(strFileName);	
	}
}
//
//void ProcessWindowMessage()
//{
//	MSG msg;
//	while(::PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
//	{
//		::SendMessage(msg.hwnd, msg.message, msg.wParam, msg.lParam);
//	}
//}

void CESMAutoAdjustDlg::CalcAdjustData()
{
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();
	DscAdjustInfo* pAdjustInfo;

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strDSC = m_AdjustDscList.GetItemText(i,1);
		m_pAdjInfo.push_back(pMovieMgr->GetAdjustData(strDSC));		

		CString strSelect;
		strSelect.Format(_T("%.3lf"),m_pAdjInfo[i].AdjMove.x);
		m_AdjustDscList.SetItemText(i, 6, strSelect);	

		strSelect.Format(_T("%.3lf"),m_pAdjInfo[i].AdjMove.y);
		m_AdjustDscList.SetItemText(i, 7, strSelect);	

		strSelect.Format(_T("%.3lf"),m_pAdjInfo[i].AdjAngle);
		m_AdjustDscList.SetItemText(i, 8, strSelect);
	}
}

void CESMAutoAdjustDlg::ModifyAdjustData()
{
	if (m_pMovePoint.size() == 0)
		return;

	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();
	DscAdjustInfo* pAdjustInfo;

	CESMAutoAdjustImage* pAdjustImg = &m_StAdjustImage;

	vector<stAdjustInfo> test;

	int marginX = 0;
	int marginY = 0;

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		stAdjustInfo adjInfo = m_pAdjInfo.at(i);
		adjInfo.AdjAngle = m_pAdjInfo.at(i).AdjAngle;
		adjInfo.AdjMove.x = m_pAdjInfo.at(i).AdjMove.x+_ttoi(m_AdjustDscList.GetItemText(i,9));//m_pMovePoint[i].x/2;//m_pObjectTracking->_movePoint[i].x/2;
		adjInfo.AdjMove.y = m_pAdjInfo.at(i).AdjMove.y+_ttoi(m_AdjustDscList.GetItemText(i,10));//m_pMovePoint[i].y/2;//m_pObjectTracking->_movePoint[i].y/2;	

		test.push_back(adjInfo);

		CESMImgMgr* pImgMgr = new CESMImgMgr();
		pImgMgr->SetMargin(marginX,marginY,adjInfo);		
	}

	ESMEvent* pMsg = new ESMEvent();  
	pMsg->message = WM_ESM_MOVIE_SAVE_ADJUST;
	pMsg->pParam = (LPARAM)& test;
	pMsg->nParam1 = marginX;
	pMsg->nParam2 = marginY;

	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	MessageBox(_T("ReCalibration\nAdjust Save Complete!"),_T("ReCalibration"),NULL);
}


void CESMAutoAdjustDlg::OnBnClickedBtnModifyAdj()
{
	//ModifyAdjustData();

	//if(!m_pAutoAdjustMgr->IsValidAdjustSquareInfo())
	//{
	//	AfxMessageBox(_T("Invalid Square Info"));
	//	return;
	//}

	InputAutoAdjustDataFromCtrlList();

	BOOL bIsFirstCamera = TRUE;
	for(int i = 0; i < m_pAutoAdjustMgr->GetDscCount(); i++)
	{
		DscAdjustInfo* pAdjustInfo = NULL;
		pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);

		CString strDscId = pAdjustInfo->strDscName;
		CT2CA pszConvertedAnsiDscId(strDscId);
		string strAnsiDscId(pszConvertedAnsiDscId);

		if(pAdjustInfo == NULL || pAdjustInfo->pBmpBits == NULL || pAdjustInfo->nHeight == 0 || pAdjustInfo->nWidht == 0 ||
			_ttoi(m_AdjustDscList.GetItemText(i, 2)) == 0)
			continue;

		Mat mFrame;
		mFrame.create(pAdjustInfo->nHeight, pAdjustInfo->nWidht, CV_8UC3);
		memcpy(mFrame.data, pAdjustInfo->pBmpBits, pAdjustInfo->nHeight * pAdjustInfo->nWidht * 3);		

		line(mFrame, cv::Point(0,0), cv::Point(pAdjustInfo->nWidht,0), Scalar(0, 0, 255), 10 );
		line(mFrame, cv::Point(pAdjustInfo->nWidht, 0), cv::Point(pAdjustInfo->nWidht, pAdjustInfo->nHeight), Scalar(0, 0, 255), 10);
		line(mFrame, cv::Point(pAdjustInfo->nWidht, pAdjustInfo->nHeight), cv::Point(0,pAdjustInfo->nHeight), Scalar(0, 0, 255), 10);
		line(mFrame, cv::Point(0,pAdjustInfo->nHeight), cv::Point(0,0), Scalar(0, 0, 255), 10);

		if(bIsFirstCamera == TRUE)
		{
			m_pSquareCalibration->SetFirstStandardCameraId(strAnsiDscId);
			bIsFirstCamera = FALSE;
		}
		m_pSquareCalibration->PreviewCalibration(mFrame, strAnsiDscId);

		rectangle(mFrame, cv::Point(200,10), cv::Point(1000,250), Scalar(0, 0, 0), cv::FILLED );
		putText(mFrame,strAnsiDscId,cv::Point(200,200),CV_FONT_HERSHEY_COMPLEX,8,cv::Scalar(255,255,255),3,8);

		resize(mFrame, mFrame, cv::Size(720, 405));

		imshow("Adjust Preview", mFrame);
		HWND hWnd = ::FindWindowW(NULL, _T("Adjust Preview"));
		::SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
		waitKey(1);
	}

	m_pAutoAdjustMgr->ClearAdjustSquareInfo();
}

void CESMAutoAdjustDlg::getAdjustMoveCoordination()
{

}

HBRUSH CESMAutoAdjustDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	hbr = (HBRUSH)m_background;	

	switch(nCtlColor)
{
	case CTLCOLOR_STATIC:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			//pDC->SetBkColor(RGB(56, 56, 56));  // 글자 배경색 변경
			pDC->SetBkMode(TRANSPARENT);
			//return (HBRUSH)m_brush;
		}
		break;
	case CTLCOLOR_EDIT:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			pDC->SetBkColor(RGB(0, 0 ,0));
			//hbr = (HBRUSH)(hbr.GetSafeHandle());  
		}
		break;
	}
	return hbr;
}

void CESMAutoAdjustDlg::OnBnClickedBtnAutoMove()
{
	if(((CButton*)GetDlgItem(IDC_CHECK_AUTOPOINT_MOVE))->GetCheck())
	{

	}
	else
	{

	}
}

BOOL CESMAutoAdjustDlg::IsPointsValid(int nIndex)
{
	CPoint pt1, pt2, pt3, pt4;
	pt1.x = _ttoi(m_AdjustDscList.GetItemText(nIndex, 2)); 
	pt1.y = _ttoi(m_AdjustDscList.GetItemText(nIndex, 3));
	pt2.x = _ttoi(m_AdjustDscList.GetItemText(nIndex, 4)); 
	pt2.y = _ttoi(m_AdjustDscList.GetItemText(nIndex, 5));
	pt3.x = _ttoi(m_AdjustDscList.GetItemText(nIndex, 6)); 
	pt3.y = _ttoi(m_AdjustDscList.GetItemText(nIndex, 7));
	pt4.x = _ttoi(m_AdjustDscList.GetItemText(nIndex, 8)); 
	pt4.y = _ttoi(m_AdjustDscList.GetItemText(nIndex, 9));

	if( pt1.x == 0 || pt1.y == 0 ||
		pt2.x == 0 || pt2.y == 0 ||
		pt3.x == 0 || pt3.y == 0 ||
		pt4.x == 0 || pt4.y == 0)
		return false;

	return true;
}

void CESMAutoAdjustDlg::DrawCenterPointAll()
{
	CString strTp;
	int nPosX, nPosY;
	COLORREF color = RGB(255, 0, 255);

	for ( int i = 1 ; i < 5 ; i++ )
	{
		nPosX = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2);
		nPosY = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2 + 1);

		if(i == 2)
		{
			int nPosX_2 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2);
			int nPosY_2 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2 + 1);
			int nPosX_1 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-1)*2);
			int nPosY_1 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-1)*2 + 1);
			
			m_StAdjustImage.DrawLine(nPosX_2, nPosY_2, nPosX_1, nPosY_1, color);
		}

		if(i == 3)
		{
			int nPosX_3 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2);
			int nPosY_3 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2 + 1);
			int nPosX_2 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-1)*2);
			int nPosY_2 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-1)*2 + 1);
			m_StAdjustImage.DrawLine(nPosX_3, nPosY_3, nPosX_2, nPosY_2, color);
		}

		if(i == 4)
		{
			int nPosX_4 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2);
			int nPosY_4 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, i*2 + 1);
			int nPosX_3 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-1)*2);
			int nPosY_3 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-1)*2 + 1);
			int nPosX_2 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-2)*2);
			int nPosY_2 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-2)*2 + 1);
			int nPosX_1 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-3)*2);
			int nPosY_1 = getDSCListItem(&m_AdjustDscList, m_nSelectedNum, (i-3)*2 + 1);

			m_StAdjustImage.DrawLine(nPosX_4, nPosY_4, nPosX_2, nPosY_2, color);
			m_StAdjustImage.DrawLine(nPosX_3, nPosY_3, nPosX_1, nPosY_1, color);
			m_StAdjustImage.DrawLine(nPosX_4, nPosY_4, nPosX_1, nPosY_1, color);
			m_StAdjustImage.DrawLine(nPosX_4, nPosY_4, nPosX_3, nPosY_3, color);
		}

		DrawCenterPoint(nPosX, nPosY, i);
	}
}

void CESMAutoAdjustDlg::DrawCenterPoint(int nPosX, int nPosY, int nIndex)
{
	int nCircle = m_StAdjustImage.m_nCircleSize;

	if ( nPosX != 0 && nPosY != 0 )
	{
		CRect rtOutLine(nPosX-nCircle,nPosY-nCircle,nPosX+nCircle,nPosY+nCircle);
		m_StAdjustImage.DrawRect(rtOutLine, Coloring(nIndex));

		CRect rtCenter(nPosX-1,nPosY-1,nPosX+1,nPosY+1);
		m_StAdjustImage.DrawRect(rtCenter, Coloring(nIndex));
	}
}

COLORREF CESMAutoAdjustDlg::Coloring(int index)
{
	int red = 0; int green = 0; int blue = 0;

	if(index % 6 == 0)		red		= 255;
	if(index % 6 - 1 == 0)	green	= 255;
	if(index % 6 - 2 == 0)	blue	= 255;
	if(index % 6 - 3 == 0) {red		= 255; green	= 255;}
	if(index % 6 - 4 == 0) {green	= 255; blue		= 255;}
	if(index % 6 - 5 == 0) {blue	= 255; red		= 255;}

	return RGB(blue, green, red);
}

//void CESMAutoAdjustDlg::FindExactPosition(int& nX, int& nY)
//{
//	//-- 2014-08-30 hongsu@esmlab.com
//	//-- Load Image 
//	CESMAutoAdjustImage* pAdjustImg;
//	pAdjustImg = &m_StAdjustImage;
//
//	UpdateData();
//	pAdjustImg->FindExactPosition(nX, nY);
//}

BOOL CESMAutoAdjustDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
		if(pMsg->message == WM_KEYDOWN)
		{
			if (pMsg->wParam == 'o' || pMsg->wParam == 'O')
			{
				m_StAdjustImage.SetUsingRuler(!m_StAdjustImage.GetUsingRuler());

				if(!m_StAdjustImage.GetUsingRuler())
				{
					CString strStatus;
					strStatus.Format(_T("Ruler Off"));
					GetDlgItem(IDC_STATIC_AUTOADJUST_STATUS)->SetWindowText(strStatus);

					DrawImage();
					m_StAdjustImage.ClearDrawRuler();
				}
				else
				{
					CString strStatus;
					strStatus.Format(_T("Ruler On"));
					GetDlgItem(IDC_STATIC_AUTOADJUST_STATUS)->SetWindowText(strStatus);
				}
			}

			switch(pMsg->wParam)
			{
			case VK_DELETE://170615 - added by hjcho
				{
					DeleteSelectedList();
				}
				break;
			}
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CESMAutoAdjustDlg::DeleteSelectedList()
{
	int nSelectedCount = m_AdjustDscList.GetSelectedCount();

	if(nSelectedCount == 1)
	{
		POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();
		int nDeleteItem = m_AdjustDscList.GetNextSelectedItem(pos);

		DeleteSelectItem(nDeleteItem);
	}
	else
	{
		POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();

		while(pos)
		{
			int nDeleteItem = m_AdjustDscList.GetNextSelectedItem(pos);
			DeleteSelectItem(nDeleteItem);
		}
	}
}
void CESMAutoAdjustDlg::DeleteSelectItem(int nIndex)
{
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);

	for(int i = 2; i < 12; i++)
	{
		m_AdjustDscList.SetItemText(nIndex,i,NULL);
	}
	for(int i = 13; i < 20; i++)
	{
		m_AdjustDscList.SetItemText(nIndex,i,NULL);
	}
	for(int i = 20; i < 28; i++)
	{
		m_AdjustDscList.SetItemText(nIndex,i,NULL);
	}
	SavePointData(strFileName, nIndex);
}

void CESMAutoAdjustDlg::ShowWorldCoordnationImage()
{
	//Image Load Test 180710
	Mat image = imread(m_strWorldCoordinationImagePath);

	m_StAdjustWorldCoordinateImage.SetImageBuffer(image.data, image.cols, image.rows, 3, NULL);
	m_StAdjustWorldCoordinateImage.Redraw();
}

void CESMAutoAdjustDlg::SetStrWorldCoordinationImagePath(string strWorldCoordinationImagePath)
{
	m_strWorldCoordinationImagePath = strWorldCoordinationImagePath;
}

void CESMAutoAdjustDlg::OnCbnSelchangeCbnSetWCImage()
{
	int nIndex = m_cbSetWCImage.GetCurSel();

	CString strConfigPath = ESMGetPath(ESM_PATH_CONFIG);
	CString strImageName = getWorldCoordinageImageName((ESM_AUTOADJUST_WORLD_COORDINATE_IMAGE)nIndex);

	strConfigPath.Append(_T("\\CalibrationImage\\"));
	strConfigPath.Append(strImageName);

	CT2CA pszConvertedAnsiConfigPath(strConfigPath);
	string strAnsiConfigPath(pszConvertedAnsiConfigPath);

	m_strWorldCoordinationImagePath = strAnsiConfigPath;

	m_pAutoAdjustMgr->ClearSquarePointInWorld();
	ShowWorldCoordnationImage();
}

void CESMAutoAdjustDlg::OnBnClickedButtonDistanceRegister()
{
	POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();
	int nItem = m_AdjustDscList.GetNextSelectedItem(pos);

	m_AdjustDscList.SetItemText(nItem, 12, _T("0"));

	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	SavePointData(strFileName, nItem);
}


void CESMAutoAdjustDlg::OnBnClickedButtonDistanceUnregister()
{
	POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();
	int nItem = m_AdjustDscList.GetNextSelectedItem(pos);

	m_AdjustDscList.SetItemText(nItem, 12, _T(""));

	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	SavePointData(strFileName, nItem);
}


void CESMAutoAdjustDlg::OnBnClickedButtonAdjustCalc()
{
	for( int i =0 ;i < m_AdjustDscList.GetItemCount(); i++ )
	{
		m_AdjustDscList.SetItemText(i, 13, _T(""));
		m_pAutoAdjustMgr->SetDistanceValue(m_pAutoAdjustMgr->GetDscAt(i)->strDscName, 0);
	}

	UpdateData(TRUE);
	// 선택 Point 의 Distance 계산.
	for(int nDscIndex =1; nDscIndex < m_pAutoAdjustMgr->GetDscCount(); nDscIndex++)
		m_pAutoAdjustMgr->GetDscAt(nDscIndex)->dDistance = 0.0;

	CalcSelectedDistance();

	CString strDsc;
	double dTargetDistance = 0.0, dFirstDist = 0.0, dSecondDist = 0.0, nGabDist = 0.0;
	int nAvgCount = 0, nfirstIndex = 0, nSecondIndex = 0;
	//if( m_pAutoAdjustMgr->GetDscCount() > 0)
	//{
	for(int i = 0; i < m_pAutoAdjustMgr->GetDscCount(); i++)
	{
		if(m_pAutoAdjustMgr->GetDscAt(i)->dDistance != 0.)
		{
			dFirstDist = m_pAutoAdjustMgr->GetDscAt(i)->dDistance;
			break;
		}
	}
		
	//}

	for(int nDscIndex =1; nDscIndex< m_pAutoAdjustMgr->GetDscCount(); nDscIndex++)
	{
		strDsc = m_pAutoAdjustMgr->GetDscAt(nDscIndex)->strDscName;
		dTargetDistance = m_pAutoAdjustMgr->GetDscAt(nDscIndex)->dDistance;

		if( dTargetDistance != 0.0)
		{
			dSecondDist = dTargetDistance;
			nSecondIndex = nDscIndex;
			nGabDist = dSecondDist - dFirstDist;
			nGabDist = nGabDist / ( nSecondIndex - nfirstIndex);
			dTargetDistance = dFirstDist + nGabDist;
			for( int nInsertIndex = nfirstIndex + 1; nInsertIndex < nSecondIndex; nInsertIndex++)
			{
				m_pAutoAdjustMgr->GetDscAt(nInsertIndex)->dDistance = dTargetDistance;
				dTargetDistance += nGabDist;
			}
			m_pAutoAdjustMgr->GetDscAt(nfirstIndex)->dDistance = dFirstDist;
			m_pAutoAdjustMgr->GetDscAt(nSecondIndex)->dDistance = dSecondDist;
			nfirstIndex = nDscIndex;
			dFirstDist = dTargetDistance;
		}
	}
}

void CESMAutoAdjustDlg::CalcSelectedDistance()
{
	UpdateData(TRUE);
	double dTpPosX = 0.0, dTpPosY = 0.0;
	DscAdjustInfo* pAdjustInfo;
	CString strCtrlName, strTp, strTp1;

	//등록점 Distance
	for( int j =0 ;j < m_AdjustDscList.GetItemCount(); j++ )
	{
		strTp = m_AdjustDscList.GetItemText(j, 12);
		strCtrlName = m_AdjustDscList.GetItemText(j, 1);
		if( strTp == _T("0"))
		{
			pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(j);
			double dNorm = m_pAutoAdjustMgr->GetNormValue(strCtrlName);
			m_pAutoAdjustMgr->SetDistanceValue(strCtrlName, dNorm);

			if( strCtrlName == pAdjustInfo->strDscName)
			{
				//Size
				double dTargetDistance = 0;
				double dTargetLength = 0;

				if( pAdjustInfo->nHeight != 0 || pAdjustInfo->nWidht != 0 )
				{
					double dTpResize = 0.0;
					if( pAdjustInfo->nWidht != 0)
						dTpResize = dNorm;
					else
						dTpResize = pAdjustInfo->nHeight;

					dTargetDistance = dTpResize;
				}
				strTp1.Format(_T("%.4lf"), dTargetDistance);
				pAdjustInfo->dDistance = dTargetDistance;
				m_AdjustDscList.SetItemText(j, 13, strTp1);
			}
		}
	}

	//등록점 기준 선형 Distance 보정
	CString strDsc;
	double dTargetDistance = 0.0, dFirstDist = 0.0, dSecondDist = 0.0, nGabDist = 0.0;
	int nAvgCount = 0, nfirstIndex = 0, nSecondIndex = 0;
	if( m_pAutoAdjustMgr->GetDscCount() > 0)
	{
		for(int i = 0; i < m_pAutoAdjustMgr->GetDscCount(); i++)
		{
			if(m_pAutoAdjustMgr->GetDscAt(i)->dDistance != 0.)
			{
				pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);
				dFirstDist = m_pAutoAdjustMgr->GetDistanceValue(pAdjustInfo->strDscName);
				nfirstIndex = i;
				break;
			}
		}
	}

	for(int nDscIndex =nfirstIndex+1; nDscIndex< m_pAutoAdjustMgr->GetDscCount(); nDscIndex++)
	{
		strDsc = m_pAutoAdjustMgr->GetDscAt(nDscIndex)->strDscName;
		
		dTargetDistance = m_pAutoAdjustMgr->GetDistanceValue(strDsc);//m_pAutoAdjustMgr->GetDistanceData(strDsc);

		if( dTargetDistance != 0.0)
		{
			dSecondDist = dTargetDistance;
			nSecondIndex = nDscIndex;
			nGabDist = dSecondDist - dFirstDist;
			nGabDist = nGabDist / ( nSecondIndex - nfirstIndex);
			dTargetDistance = dFirstDist + nGabDist;
			for( int nInsertIndex = nfirstIndex + 1; nInsertIndex < nSecondIndex; nInsertIndex++)
			{
				CString strDsc_= m_pAutoAdjustMgr->GetDscAt(nInsertIndex)->strDscName;
				m_pAutoAdjustMgr->SetDistanceValue(strDsc_, dTargetDistance);
				dTargetDistance += nGabDist;
			}
			m_pAutoAdjustMgr->SetDistanceValue(strDsc, dFirstDist);
			m_pAutoAdjustMgr->SetDistanceValue(strDsc, dSecondDist);
			nfirstIndex = nDscIndex;
			dFirstDist = dTargetDistance;
		}
	}

	//Distance 출력
	BOOL bIsFirstDistance = FALSE;
	CString strText;
	for(int i =0; i< m_pAutoAdjustMgr->GetDscCount(); i++)
	{
		pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);
		double dDist = m_pAutoAdjustMgr->GetDistanceValue(pAdjustInfo->strDscName);

		if(bIsFirstDistance == FALSE)
		{
			if(dDist == 0.)
			{
				continue;
			}
			else
			{
				bIsFirstDistance = TRUE;
			}
		}

		//Distance
		strText.Format(_T("%.4lf"), dDist);
		m_AdjustDscList.SetItemText(i, 13, strText);
	}

	//Adjust 계산
	for(int i =0; i< m_pAutoAdjustMgr->GetDscCount(); i++)
	{
		pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);
		if( m_pAutoAdjustMgr->CalcAdjustData(pAdjustInfo, 500/*m_nTargetLenth*/, 20/*nZoom*/))
		{
			if(!IsPointsValid(i))
			{
				pAdjustInfo->dAdjustX = 0;
				pAdjustInfo->dAdjustY = 0;
				continue;
			}

			dTpPosX += pAdjustInfo->dAdjustX;
			dTpPosY += pAdjustInfo->dAdjustY;
			//pAdjustInfo->nHeight = m_nAdjViewHeight;
			//pAdjustInfo->nWidht = m_nAdjViewWidth;
			nAvgCount++;
		}
	}

	//wgkim 190527
	CESMImgMgr* pImgMgr = new CESMImgMgr;

	//중점 좌표의 평균
	dTpPosX = dTpPosX / nAvgCount;
	dTpPosY = dTpPosY / nAvgCount;
	for(int i =0; i< m_pAutoAdjustMgr->GetDscCount(); i++)
	{
		pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);
		if( pAdjustInfo == NULL || pAdjustInfo->nHeight == 0 || pAdjustInfo->nWidht == 0 ||
			!IsPointsValid(i))
		{
			pAdjustInfo->dAdjustX = 0.; 
			pAdjustInfo->dAdjustY = 0.;
			pAdjustInfo->dAngle = 0.;
			pAdjustInfo->dRotateX = 0.;
			pAdjustInfo->dRotateY = 0.;
			pAdjustInfo->dScale = 0.;
			pAdjustInfo->dDistance= 0.;

			continue;
		}

		pAdjustInfo->dAdjustX = dTpPosX - pAdjustInfo->dAdjustX;
		pAdjustInfo->dAdjustY = dTpPosY - pAdjustInfo->dAdjustY;

		//Adjust X,Y 
		strText.Format(_T("%.4lf"), pAdjustInfo->dAdjustX);
		m_AdjustDscList.SetItemText(i, 14, strText);
		strText.Format(_T("%.4lf"), pAdjustInfo->dAdjustY);
		m_AdjustDscList.SetItemText(i, 15, strText);
		//Angle
		strText.Format(_T("%.4lf"), pAdjustInfo->dAngle);
		m_AdjustDscList.SetItemText(i, 16, strText);

		//Rotate X,Y 
		strText.Format(_T("%.4lf"), pAdjustInfo->dRotateX);
		m_AdjustDscList.SetItemText(i, 17, strText);
		strText.Format(_T("%.4lf"), pAdjustInfo->dRotateY);
		m_AdjustDscList.SetItemText(i, 18, strText);

		//Scale
		strText.Format(_T("%.4lf"), pAdjustInfo->dScale);
		m_AdjustDscList.SetItemText(i, 19, strText);

		//wgkim 190527
		stAdjustInfo adjInfo;
		adjInfo.AdjAngle = pAdjustInfo->dAngle;
		adjInfo.AdjSize = pAdjustInfo->dScale;
		adjInfo.AdjptRotate.x = pAdjustInfo->dRotateX;
		adjInfo.AdjptRotate.y = pAdjustInfo->dRotateY;
		adjInfo.AdjMove.x = pAdjustInfo->dAdjustX;
		adjInfo.AdjMove.y = pAdjustInfo->dAdjustY;
		adjInfo.strDSC = pAdjustInfo->strDscName;
		adjInfo.nHeight = pAdjustInfo->nHeight;
		adjInfo.nWidth = pAdjustInfo->nWidht;

		//wgkim 190527
		pImgMgr->SetRectMargin(adjInfo);
	}

	for(int i =0; i< m_pAutoAdjustMgr->GetDscCount(); i++)
	{
		pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);
		pAdjustInfo->rtMargin = pImgMgr->GetMinimumMargin();
	}

	delete pImgMgr;
}


void CESMAutoAdjustDlg::OnBnClickedButtonDistanceRegisterAll()
{
	int nTotalItem = m_AdjustDscList.GetItemCount();

	for(int i = 0 ; i < nTotalItem; i++)
	{
		m_AdjustDscList.SetItemText(i, 12, _T("0"));
	}

	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	SavePointData(strFileName);
}


void CESMAutoAdjustDlg::OnBnClickedButtonDistanceRegisterAll2()
{
	for( int i =0 ;i < m_AdjustDscList.GetItemCount(); i++ )
	{
		m_AdjustDscList.SetItemText(i, 12, _T(""));
	}

	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	SavePointData(strFileName);
}


void CESMAutoAdjustDlg::OnBnClickedButtonAdjustSave()
{
	destroyWindow("Adjust Preview");

	int marginX = 0;
	int marginY = 0;

	vector<stAdjustInfo> test;
	DscAdjustInfo* pAdjustInfo;

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);
		stAdjustInfo adjInfo;
		adjInfo.AdjAngle = pAdjustInfo->dAngle;
		adjInfo.AdjSize = pAdjustInfo->dScale;
		adjInfo.AdjMove.x = pAdjustInfo->dAdjustX;
		adjInfo.AdjMove.y = pAdjustInfo->dAdjustY;
		adjInfo.nWidth = m_nWidth;
		adjInfo.nHeight= m_nHeight;
		adjInfo.AdjptRotate.x = pAdjustInfo->dRotateX;
		adjInfo.AdjptRotate.y = pAdjustInfo->dRotateY;

		test.push_back(adjInfo);

		CESMImgMgr* pImgMgr = new CESMImgMgr();
		pImgMgr->SetMargin(marginX,marginY,adjInfo);
	}

	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	vector<DscAdjustInfo*>* pArrDscInfo = NULL;
	m_pAutoAdjustMgr->GetDscInfo(&pArrDscInfo);
	pMainWnd->m_wndDSCViewer.SetDscAdjustData(pArrDscInfo);

	// Margin 계산
	pMainWnd->m_wndDSCViewer.SetMargin(pArrDscInfo);

	ESMEvent* pMsg = new ESMEvent();  
	pMsg->message = WM_ESM_MOVIE_SAVE_ADJUST;
	pMsg->pParam = (LPARAM)& test;
	pMsg->nParam1 = marginX;
	pMsg->nParam2 = marginY;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
}


void CESMAutoAdjustDlg::OnBnClickedCheckAutoadjustAutomove()
{
	if(((CButton*)GetDlgItem(IDC_CHECK_AUTOADJUST_AUTOMOVE))->GetCheck())
	{
		m_pTemplateAsAuto.InputAdjustFeaturesFromList(&m_AdjustDscList, m_nSelectedNum);
	}
	else
	{

	}
}


void CESMAutoAdjustDlg::OnBnClickedCheckAutoadjustAutofind()
{
	if(((CButton*)GetDlgItem(IDC_CHECK_AUTOADJUST_AUTOFIND))->GetCheck())
	{

	}
	else
	{

	}
}

vector<Point2f> CESMAutoAdjustDlg::getSquareFromListCtrl(CListCtrl* pList, int nSelectedNumber)
{
	vector<Point2f> square(4);

	if(ESMGetValue(ESM_VALUE_REVERSE_MOVIE))
	{
		for(int j = 1; j < 5; j++)
		{			
			square[j-1].x = m_nWidth - getDSCListItem(pList, nSelectedNumber, j*2);
			square[j-1].y = m_nHeight - getDSCListItem(pList, nSelectedNumber, j*2 + 1);

			if(square[j-1].x == m_nWidth)
				square[j-1].x = 0;
			if(square[j-1].y == m_nHeight)
				square[j-1].y = 0;
		}
	}
	else
	{
		for(int j = 1; j < 5; j++)
		{
			square[j-1].x = getDSCListItem(pList, nSelectedNumber, j*2);
			square[j-1].y = getDSCListItem(pList, nSelectedNumber, j*2 + 1);		
		}
	}

	return square;
}

void CESMAutoAdjustDlg::InputAutoAdjustDataFromCtrlList()
{
	vector<CPoint> pAutoAdjustWorldPoint = m_pAutoAdjustMgr->GetSquarePointInWorld();

	int nCircle = m_StAdjustImage.m_nCircleSize;
	CRect rtOutLine(
		m_pCenterPoint.x-nCircle,
		m_pCenterPoint.y-nCircle,
		m_pCenterPoint.x+nCircle,
		m_pCenterPoint.y+nCircle);
	m_StAdjustImage.DrawRect(rtOutLine, Coloring(5));
	m_StAdjustImage.Redraw();

	for(int i = 0; i < m_pAutoAdjustMgr->GetDscCount(); i++)
	{
		DscAdjustInfo* pAdjustInfo = NULL;
		pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);

		CString strDscId = pAdjustInfo->strDscName;
		CT2CA pszConvertedAnsiDscId(strDscId);
		string strAnsiDscId(pszConvertedAnsiDscId);

		vector<CPoint> ptPoint;
		for(int j = 0; j < 4; j++)
		{
			ptPoint.push_back(CPoint());
			ptPoint[j].x = getDSCListItem(&m_AdjustDscList, i, 20 + j*2);
			ptPoint[j].y = getDSCListItem(&m_AdjustDscList, i, 20 + j*2 + 1);
		}
		m_pAutoAdjustMgr->SetSquarePointInWorldAt(strDscId, ptPoint);

		CPoint pCenter;
		pCenter.x = _ttoi(m_AdjustDscList.GetItemText(i, 10));
		pCenter.y = _ttoi(m_AdjustDscList.GetItemText(i, 11));
		m_pAutoAdjustMgr->SetCenterPointAt(strDscId, pCenter);

		CPoint pt1, pt2, pt3, pt4;
		pt1.x = _ttoi(m_AdjustDscList.GetItemText(i, 2)); pt1.y = _ttoi(m_AdjustDscList.GetItemText(i, 3));
		pt2.x = _ttoi(m_AdjustDscList.GetItemText(i, 4)); pt2.y = _ttoi(m_AdjustDscList.GetItemText(i, 5));
		pt3.x = _ttoi(m_AdjustDscList.GetItemText(i, 6)); pt3.y = _ttoi(m_AdjustDscList.GetItemText(i, 7));
		pt4.x = _ttoi(m_AdjustDscList.GetItemText(i, 8)); pt4.y = _ttoi(m_AdjustDscList.GetItemText(i, 9));

		m_pAutoAdjustMgr->SetAdjustSquarePointAt(strDscId, pt1, pt2, pt3, pt4);
		m_pAutoAdjustMgr->SetUseKZoneAt(strDscId, m_AdjustDscList.GetCheck(i));
	}

	int nSelectedItem = m_AdjustDscList.GetNextItem( -1, LVNI_SELECTED );
	CString strStartDscId = m_AdjustDscList.GetItemText(0, 1);
	CString strEndDscId = m_AdjustDscList.GetItemText(m_pAutoAdjustMgr->GetDscCount()-1, 1);

	for(int i = 0; i < m_pAutoAdjustMgr->GetDscCount(); i++)
	{
		DscAdjustInfo* pAdjustInfo = NULL;
		pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);

		CString strDscId = pAdjustInfo->strDscName;
		CT2CA pszConvertedAnsiDscId(strDscId);
		string strAnsiDscId(pszConvertedAnsiDscId);

		m_pSquareCalibration->CalibrationUsingSquarePoints(strAnsiDscId);
	}
}

void CESMAutoAdjustDlg::SetPositionTrackingData()
{
	//PositionTracking
	vector<vector<Point2f>> vecPtSquares;
	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
		vecPtSquares.push_back(getSquareFromListCtrl(&m_AdjustDscList, i));

	m_pTemplateMgr->ClearArrSquares();
	m_pTemplateMgr->ApplyHomographiesFromArrSquares(vecPtSquares);
}

void CESMAutoAdjustDlg::OnBnClickedButtonAutoadjustPlus()
{
	int nSelectedCount = m_AdjustDscList.GetSelectedCount();
	if(nSelectedCount >= 1)
	{
		POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();
		while(pos)
		{
			int nIndex = m_AdjustDscList.GetNextSelectedItem(pos);
			BOOL bCheck = m_AdjustDscList.GetCheck(nIndex);

			if(!bCheck)
			{
				m_AdjustDscList.SetCheck(nIndex, TRUE);
			}
		}
	}
	UpdateData(FALSE);

	for(int i = 0; i < m_pAutoAdjustMgr->GetDscCount(); i++)
	{
		DscAdjustInfo* pAdjustInfo = NULL;
		pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);
		BOOL bUseKZone = m_AdjustDscList.GetCheck(i);
		m_pAutoAdjustMgr->SetUseKZoneAt(pAdjustInfo->strDscName, bUseKZone);
	}

	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	SavePointData(strFileName);
}

void CESMAutoAdjustDlg::OnBnClickedButtonAutoadjustMinus()
{
	int nSelectedCount = m_AdjustDscList.GetSelectedCount();
	if(nSelectedCount >= 1)
	{
		POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();
		while(pos)
		{
			int nIndex = m_AdjustDscList.GetNextSelectedItem(pos);
			BOOL bCheck = m_AdjustDscList.GetCheck(nIndex);
			if(bCheck)
			{
				m_AdjustDscList.SetCheck(nIndex, FALSE);
			}
		}
	}
	UpdateData(FALSE);

	for(int i = 0; i < m_pAutoAdjustMgr->GetDscCount(); i++)
	{
		DscAdjustInfo* pAdjustInfo = NULL;
		pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);
		BOOL bUseKZone = m_AdjustDscList.GetCheck(i);
		m_pAutoAdjustMgr->SetUseKZoneAt(pAdjustInfo->strDscName, bUseKZone);
	}

	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	SavePointData(strFileName);
}

void CESMAutoAdjustDlg::SetAutoTrackingRoiFromSquarePoint(CESMAutoAdjustImageMapping *pAutoAdjustImageMapping, vector<Point2f> vecPt)
{
	pAutoAdjustImageMapping->_vecSquarePt = vecPt;

	int nLeft = m_nWidth;
	int nRight = 0;
	int nTop = m_nHeight;
	int nButtom = 0;

	for(int i = 0; i < 4; i++)
	{
		if(vecPt[i].x < nLeft)
			nLeft = vecPt[i].x;
		if(vecPt[i].x > nRight)
			nRight = vecPt[i].x;
		if(vecPt[i].y < nTop)
			nTop= vecPt[i].y;
		if(vecPt[i].y > nButtom)
			nButtom= vecPt[i].y;
	}

	pAutoAdjustImageMapping->SetImageROI(
		(nLeft-50),
		(nTop-50), 
		(nRight - nLeft)+50*2, 
		(nButtom - nTop)+50*2);
}

void ProcessWindowMessageAutoAdjust()
{
	MSG msg;
	while(::PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
	{
		::SendMessage(msg.hwnd, msg.message, msg.wParam, msg.lParam);
	}
}

void CESMAutoAdjustDlg::OnBnClickedButtonAutoadjustAutoFindDownStart()
{
	CESMAutoAdjustImageMapping *pAutoAdjustImageMapping = new CESMAutoAdjustImageMapping;

	POSITION posItem = m_AdjustDscList.GetFirstSelectedItemPosition();
	int nSelectedDscCount = m_AdjustDscList.GetSelectedCount();

	int nStartSelectedItem;
	if(nSelectedDscCount == 1)
	{
		nStartSelectedItem = m_nSelectedNum;
		nSelectedDscCount = m_pAutoAdjustMgr->GetDscCount() - m_nSelectedNum;
	}
	else
	{
		for (int i = 0; i < nSelectedDscCount; i++)
		{
			int nIndex = m_AdjustDscList.GetNextSelectedItem(posItem);

			if(i == 0)
				nStartSelectedItem = nIndex;
		}
		ESMLog(5, _T("Start : %d, Selected : %d"), nStartSelectedItem, nSelectedDscCount);
	}

	vector<Point2f> vecPt;
	vector<Point2f> vecRpt;
	for(int i = 0; i < 4; i++)
	{
		Point2f pt;
		pt.x = _ttoi(m_AdjustDscList.GetItemText(nStartSelectedItem, 2*i + 2));
		pt.y = _ttoi(m_AdjustDscList.GetItemText(nStartSelectedItem, 2*i + 3));
		vecPt.push_back(pt);

		Point2f rpt;
		rpt.x = _ttoi(m_AdjustDscList.GetItemText(nStartSelectedItem, 2*i + 20));
		rpt.y = _ttoi(m_AdjustDscList.GetItemText(nStartSelectedItem, 2*i + 21));
		vecRpt.push_back(rpt);
	}
	pAutoAdjustImageMapping->SetSquarePoint(vecPt);
	pAutoAdjustImageMapping->SetReferenceSquarePoints(vecRpt);
	SetAutoTrackingRoiFromSquarePoint(pAutoAdjustImageMapping, vecPt);


	for(int i = nStartSelectedItem; i < nStartSelectedItem + nSelectedDscCount; i++)
	{
		if(m_bAutoFindPause == TRUE)
		{
			m_bAutoFindPause = FALSE;
			break;
		}
		while(!m_vecIsImageLoad[i])
		{
			if(m_bAutoFindPause == TRUE)
			{
				m_bAutoFindPause = FALSE;
				break;
			}
			Sleep(10);
		}

		UpdateData();
		ProcessWindowMessageAutoAdjust();

		DscAdjustInfo* pAdjustInfo = NULL;
		pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);

		CString strDscId = pAdjustInfo->strDscName;
		CT2CA pszConvertedAnsiDscId(strDscId);
		string strAnsiDscId(pszConvertedAnsiDscId);

		CString strStatus;
		strStatus.Format(_T("Auto Find : %s(%dth)"), strDscId, i+1);
		GetDlgItem(IDC_STATIC_AUTOADJUST_STATUS)->SetWindowText(strStatus);

		if(	pAdjustInfo == NULL || 
			pAdjustInfo->pBmpBits == NULL || 
			pAdjustInfo->nHeight == 0 || 
			pAdjustInfo->nWidht == 0)
		{
			DeleteSelectItem(i);
			continue;
		}

		Mat mFrame;
		mFrame.create(pAdjustInfo->nHeight, pAdjustInfo->nWidht, CV_8UC3);
		memcpy(mFrame.data, pAdjustInfo->pBmpBits, pAdjustInfo->nHeight * pAdjustInfo->nWidht * 3);

		pAutoAdjustImageMapping->AddAdjustFeatureInfo(i, strAnsiDscId, mFrame, vecPt);
		bool bCheck = pAutoAdjustImageMapping->RunSquareDetecting(mFrame);

		if(bCheck == false)
		{
			ESMLog(5, _T("Auto Line Detecting Error"));
			break;
		}

		if(i != m_nSelectedNum)
		{
			vector<Point2f> vecPtNextSquarePoint;
			vecPtNextSquarePoint = pAutoAdjustImageMapping->GetModifiedSquarePoint();

			vecPt.clear();
			for(int j = 0; j < 4; j++)
			{
				setDscListItem(&m_AdjustDscList, i, j*2+2, (double)vecPtNextSquarePoint[j].x);
				setDscListItem(&m_AdjustDscList, i, j*2+3, (double)vecPtNextSquarePoint[j].y);

				Point2f pt;
				pt.x = _ttoi(m_AdjustDscList.GetItemText(i, 2*j + 2));
				pt.y = _ttoi(m_AdjustDscList.GetItemText(i, 2*j + 3));
				vecPt.push_back(pt);
			}
			SetAutoTrackingRoiFromSquarePoint(pAutoAdjustImageMapping, vecPt);

			if(nSelectedDscCount != 1)
			{
				for(int j = 0; j < m_pAutoAdjustMgr->GetDscCount(); j++)
					m_AdjustDscList.SetItemState(j, NULL, LVIS_SELECTED|LVIS_FOCUSED); 
			}
			m_AdjustDscList.SetItemState(i,LVIS_SELECTED,  LVIS_SELECTED);
			m_AdjustDscList.SetItemState(i-1, 0 ,  LVIS_SELECTED);
			m_AdjustDscList.EnsureVisible(i, FALSE); 
			m_AdjustDscList.SetFocus();
		}
	}

	CString strStatus;
	strStatus.Format(_T("Auto Find : Finish"));
	GetDlgItem(IDC_STATIC_AUTOADJUST_STATUS)->SetWindowText(strStatus);
	
	pAutoAdjustImageMapping->ClearAdjustFeatureInfo();
	delete pAutoAdjustImageMapping;

}

void CESMAutoAdjustDlg::OnBnClickedButtonAutoadjustAutoFindUpStart()
{
	CESMAutoAdjustImageMapping *pAutoAdjustImageMapping = new CESMAutoAdjustImageMapping;

	POSITION posItem = m_AdjustDscList.GetFirstSelectedItemPosition();
	int nSelectedDscCount = m_AdjustDscList.GetSelectedCount();

	int nStartSelectedItem = -1;
	int nEndSelectedItem = -1;
	if(nSelectedDscCount == 1)
	{
		nStartSelectedItem = m_AdjustDscList.GetNextItem( -1, LVNI_SELECTED );//m_nSelectedNum;
		nEndSelectedItem = 0;
		nSelectedDscCount = m_pAutoAdjustMgr->GetDscCount() - nStartSelectedItem;
	}
	else
	{
		int nIndex = m_AdjustDscList.GetNextSelectedItem(posItem);
		nStartSelectedItem = nIndex + nSelectedDscCount-1;
		nEndSelectedItem = nIndex;
		ESMLog(5, _T("Start : %d, End : %d, Selected : %d"), nStartSelectedItem, nEndSelectedItem, nSelectedDscCount);
	}

	vector<Point2f> vecPt;
	vector<Point2f> vecRpt;
	for(int i = 0; i < 4; i++)
	{
		Point2f pt;
		pt.x = _ttoi(m_AdjustDscList.GetItemText(nStartSelectedItem, 2*i + 2));
		pt.y = _ttoi(m_AdjustDscList.GetItemText(nStartSelectedItem, 2*i + 3));
		vecPt.push_back(pt);

		Point2f rpt;
		rpt.x = _ttoi(m_AdjustDscList.GetItemText(nStartSelectedItem, 2*i + 20));
		rpt.y = _ttoi(m_AdjustDscList.GetItemText(nStartSelectedItem, 2*i + 21));
		vecRpt.push_back(rpt);
	}
	pAutoAdjustImageMapping->SetSquarePoint(vecPt);
	pAutoAdjustImageMapping->SetReferenceSquarePoints(vecRpt);
	SetAutoTrackingRoiFromSquarePoint(pAutoAdjustImageMapping, vecPt);

	BOOL bFirstFlag = TRUE;
	for(int i = nStartSelectedItem; i >= nEndSelectedItem; i--)
	{
		if(m_bAutoFindPause == TRUE)
		{
			m_bAutoFindPause = FALSE;
			break;
		}
		while(!m_vecIsImageLoad[i])
		{
			if(m_bAutoFindPause == TRUE)
			{
				m_bAutoFindPause = FALSE;
				break;
			}
			Sleep(10);
		}
		UpdateData();
		ProcessWindowMessageAutoAdjust();

		DscAdjustInfo* pAdjustInfo = NULL;
		pAdjustInfo = m_pAutoAdjustMgr->GetDscAt(i);

		CString strDscId = pAdjustInfo->strDscName;
		CT2CA pszConvertedAnsiDscId(strDscId);
		string strAnsiDscId(pszConvertedAnsiDscId);

		CString strStatus;
		strStatus.Format(_T("Auto Find : %s(%dth)"), strDscId, i+1);
		GetDlgItem(IDC_STATIC_AUTOADJUST_STATUS)->SetWindowText(strStatus);

		if(	pAdjustInfo == NULL || 
			pAdjustInfo->pBmpBits == NULL || 
			pAdjustInfo->nHeight == 0 || 
			pAdjustInfo->nWidht == 0)
		{
			DeleteSelectItem(i);
			continue;
		}

		Mat mFrame;
		mFrame.create(pAdjustInfo->nHeight, pAdjustInfo->nWidht, CV_8UC3);
		memcpy(mFrame.data, pAdjustInfo->pBmpBits, pAdjustInfo->nHeight * pAdjustInfo->nWidht * 3);

		pAutoAdjustImageMapping->AddAdjustFeatureInfo(i, strAnsiDscId, mFrame, vecPt);
		bool bCheck = pAutoAdjustImageMapping->RunSquareDetecting(mFrame);
		ESMLog(5, _T("Selected DSC: %s"), strDscId);

		if(bCheck == false)
		{
			ESMLog(5, _T("Auto Line Detecting Error"));
			break;
		}

		if(bFirstFlag)
		{
			bFirstFlag = FALSE;
		}
		else
		{
			vector<Point2f> vecPtNextSquarePoint;
			vecPtNextSquarePoint = pAutoAdjustImageMapping->GetModifiedSquarePoint();

			vecPt.clear();
			for(int j = 0; j < 4; j++)
			{
				setDscListItem(&m_AdjustDscList, i, j*2+2, (double)vecPtNextSquarePoint[j].x);
				setDscListItem(&m_AdjustDscList, i, j*2+3, (double)vecPtNextSquarePoint[j].y);

				Point2f pt;
				pt.x = _ttoi(m_AdjustDscList.GetItemText(i, 2*j + 2));
				pt.y = _ttoi(m_AdjustDscList.GetItemText(i, 2*j + 3));
				vecPt.push_back(pt);
			}
			SetAutoTrackingRoiFromSquarePoint(pAutoAdjustImageMapping, vecPt);

			if(nSelectedDscCount != 1)
			{
				for(int j = 0; j < m_pAutoAdjustMgr->GetDscCount(); j++)
					m_AdjustDscList.SetItemState(j, NULL, LVIS_SELECTED|LVIS_FOCUSED); 
			}
			m_AdjustDscList.SetItemState(i+1, 0 ,  LVIS_SELECTED);
			m_AdjustDscList.SetItemState(i,LVIS_SELECTED,  LVIS_SELECTED);
			m_AdjustDscList.EnsureVisible(i, FALSE); 
			m_AdjustDscList.SetFocus();
		}
	}

	CString strStatus;
	strStatus.Format(_T("Auto Find : Finish"));
	GetDlgItem(IDC_STATIC_AUTOADJUST_STATUS)->SetWindowText(strStatus);

	pAutoAdjustImageMapping->ClearAdjustFeatureInfo();
	delete pAutoAdjustImageMapping;
}

void CESMAutoAdjustDlg::OnBnClickedButtonAutoadjustAutoFindPause()
{
	m_bAutoFindPause = TRUE;
}
