// ESMUtilActualMeasurementDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMUtilActualMeasurementDlg.h"
#include "afxdialogex.h"


// CESMUtilActualMeasurementDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMUtilActualMeasurementDlg, CDialogEx)

CESMUtilActualMeasurementDlg::CESMUtilActualMeasurementDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMUtilActualMeasurementDlg::IDD, pParent)
{
	m_strSensorWidth = _T("23.5");
	m_strSensorHeight = _T("15.7");

	m_strFocalLength = _T("");
	m_strActualWidth169 = _T("0.0");
	m_strActualLength = _T("");
	m_strActualHeight169 = _T("0.0");
	m_strActualHeight = _T("0.0");
	m_strActualWidth = _T("0.0");
	m_strAngleOfView = _T("0.0");
	m_strSensorDiagonalLength = _T("0.0");

	m_pUtilCalculator = new CESMUtilCalculator();
}

CESMUtilActualMeasurementDlg::~CESMUtilActualMeasurementDlg()
{
}

void CESMUtilActualMeasurementDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_INPUT_SENSOR_HEIGHT, m_strSensorHeight);
	DDX_Text(pDX, IDC_EDIT_INPUT_SENSOR_WIDTH, m_strSensorWidth);
	DDX_Text(pDX, IDC_EDIT_INPUT_FOCAL_LENGTH, m_strFocalLength);
	DDX_Text(pDX, IDC_EDIT_INPUT_ACTUAL_WIDTH_169, m_strActualWidth169);
	DDX_Text(pDX, IDC_EDIT_INPUT_ACTUAL_LENGTH, m_strActualLength);
	DDX_Text(pDX, IDC_EDIT_INPUT_ACTUAL_HEIGHT_169, m_strActualHeight169);
	DDX_Text(pDX, IDC_EDIT_OUTPUT_ACTUAL_HEIGHT, m_strActualHeight);
	DDX_Text(pDX, IDC_EDIT_OUTPUT_ACTUAL_WIDTH, m_strActualWidth);
	DDX_Text(pDX, IDC_EDIT_OUTPUT_ANGLE_OF_VIEW, m_strAngleOfView);
	DDX_Text(pDX, IDC_EDIT_OUTPUT_SENSOR_DIAGONAL_LENGTH, m_strSensorDiagonalLength);
}


BEGIN_MESSAGE_MAP(CESMUtilActualMeasurementDlg, CDialogEx)
	ON_EN_CHANGE(IDC_EDIT_INPUT_FOCAL_LENGTH, &CESMUtilActualMeasurementDlg::OnChangeEditInputFocalLength)
	ON_EN_CHANGE(IDC_EDIT_INPUT_ACTUAL_LENGTH, &CESMUtilActualMeasurementDlg::OnChangeEditInputActualLength)
END_MESSAGE_MAP()


// CESMUtilActualMeasurementDlg 메시지 처리기입니다.

void CESMUtilActualMeasurementDlg::OnChangeEditInputFocalLength()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.

	UpdateData(TRUE);

	m_dSensorHeight = _ttof(m_strSensorHeight);
	m_dSensorWidth = _ttof(m_strSensorWidth);

	m_dSensorDiagonalLength = m_pUtilCalculator->DiagonalLength(m_dSensorWidth, m_dSensorHeight);		
	m_strSensorDiagonalLength.Format(_T("%lf"), m_dSensorDiagonalLength);

	m_dFocalLength = _ttof(m_strFocalLength);
	m_dAngleOfView = atan2(m_dSensorDiagonalLength/2, m_dFocalLength);
	m_strAngleOfView.Format(_T("%lf"), m_pUtilCalculator->RadianToDegree(m_dAngleOfView));
	
	UpdateData(FALSE);
}


void CESMUtilActualMeasurementDlg::OnChangeEditInputActualLength()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.

	UpdateData(TRUE);

	m_dActualLength = _ttof(m_strActualLength);
	m_dActualDiagonalLength = m_pUtilCalculator->ActualDiagonalLength(m_dActualLength, m_dAngleOfView);

	m_dActualWidth = m_pUtilCalculator->ActualWidth(m_dSensorWidth, m_dSensorHeight, m_dActualDiagonalLength);
	m_dActualHeight= m_pUtilCalculator->ActualHeight(m_dSensorWidth, m_dSensorHeight, m_dActualDiagonalLength);

	m_strActualWidth.Format(_T("%lf"), m_dActualWidth);
	m_strActualHeight.Format(_T("%lf"), m_dActualHeight);
	m_strActualWidth169.Format(_T("%lf"), m_dActualWidth);
	m_strActualHeight169.Format(_T("%lf"), m_pUtilCalculator->ActualHeight169(m_dActualHeight));
	
	UpdateData(FALSE);
}
