#pragma once
//#include "ESMUtilRemoteCtrl.h"
#include "opencv2/core.hpp"
#include "opencv2/opencv_modules.hpp"
#include "opencv2/opencv.hpp"
#include "ESMIndex.h"
#include "ESMDefine.h"
#include "resource.h"
#include "ESMMuxBackupList.h"

class CESMMuxBackUpList;

class CESMMuxBackUpDlg : public CDialog
{
	DECLARE_DYNAMIC(CESMMuxBackUpDlg)

public:
	CESMMuxBackUpDlg(CWnd* pParent = NULL); 
	virtual ~CESMMuxBackUpDlg();

	enum { IDD = IDD_UTIL_MUX_BACKUP };
	CESMMuxBackUpList* m_pBackupList;
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);
};