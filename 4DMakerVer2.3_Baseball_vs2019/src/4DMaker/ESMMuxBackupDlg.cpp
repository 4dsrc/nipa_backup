#include "StdAfx.h"
#include "ESMMuxBackupDlg.h"

IMPLEMENT_DYNAMIC(CESMMuxBackUpDlg, CDialog)

CESMMuxBackUpDlg::CESMMuxBackUpDlg(CWnd* pParent /*=NULL*/)
: CDialog(CESMMuxBackUpDlg::IDD, pParent)
{
	m_pBackupList = new CESMMuxBackUpList;
}
CESMMuxBackUpDlg::~CESMMuxBackUpDlg()
{
	if(m_pBackupList)
	{
		delete m_pBackupList;
		m_pBackupList = NULL;
	}
}

BOOL CESMMuxBackUpDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_pBackupList->Init(this);


	return TRUE;  
}

void CESMMuxBackUpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_UTIL_MUXBACKUP_LIST, *m_pBackupList);
}
