// ESMGroupBox.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMGroupBox.h"


// CESMGroupBox

IMPLEMENT_DYNAMIC(CESMGroupBox, CStatic)

CESMGroupBox::CESMGroupBox()
{
	m_brush.CreateSolidBrush(RGB(44,44,44));
}

CESMGroupBox::~CESMGroupBox()
{
}


BEGIN_MESSAGE_MAP(CESMGroupBox, CStatic)
	ON_WM_NCPAINT()
	ON_WM_CTLCOLOR()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()



// CESMGroupBox 메시지 처리기입니다.




void CESMGroupBox::OnNcPaint()
{
	CDC* pDC = GetWindowDC();

	CRect rect;
	GetWindowRect( &rect );
	rect.OffsetRect( -rect.left, -rect.top);

	CBrush brush( RGB(145,145,145));
	pDC->FrameRect( &rect, &brush);
	SetTextColor(pDC->m_hDC,RGB(255, 255, 255));// 글자색 변경
	SetBkColor(pDC->m_hDC,RGB(0, 0 ,0));
	ReleaseDC(pDC);
}


HBRUSH CESMGroupBox::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CStatic::OnCtlColor(pDC, pWnd, nCtlColor);

	switch(nCtlColor)
	{
	case CTLCOLOR_STATIC:
		{

			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경

			//pDC->SetBkColor(RGB(56, 56, 56));  // 글자 배경색 변경
			pDC->SetBkMode(TRANSPARENT);
			return (HBRUSH)m_brush;
		}
		break;
	}

	return hbr;
}


BOOL CESMGroupBox::OnEraseBkgnd(CDC* pDC)
{
	CRect rt;
	GetClientRect(rt);
	//pDC->FillSolidRect(rt,RGB(44,44,44));
	pDC->SetBkMode(TRANSPARENT);
	return TRUE;

	return CStatic::OnEraseBkgnd(pDC);
}
