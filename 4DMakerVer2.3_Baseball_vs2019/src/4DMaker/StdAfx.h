////////////////////////////////////////////////////////////////////////////////
//
//	StdAfx.h : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-06-13
//
////////////////////////////////////////////////////////////////////////////////


#pragma once


#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#define _SECURE_ATL 1

/*
// 아래 지정된 플랫폼에 우선하는 플랫폼을 대상으로 하는 경우 다음 정의를 수정하십시오.
// 다른 플랫폼에 사용되는 해당 값의 최신 정보는 MSDN을 참조하십시오.
#ifndef WINVER				// Windows XP 이상에서만 기능을 사용할 수 있습니다.
#define WINVER 0x0501		// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif						

#ifndef _WIN32_WINDOWS			// Windows 98 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_WINDOWS 0x0410	// Windows Me 이상에 맞도록 적합한 값으로 변경해 주십시오.
#endif
*/

#ifndef _WIN32_IE			// IE 6.0 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_IE 0x0600	// 다른 버전의 IE에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls

//-- 2012-05-22 hongsu
#define _CRTDGB_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#endif // _AFX_NO_AFXCMN_SUPPORT

#ifdef __EXT_MFC_NO_TAB_CONTROLBARS
	#error "Critical error: SMILE sample requires support for the tabbed control bars"
#endif

#include <afx.h>
//------------------------------------------------------------------------------
//-- Date	 : 2010-3-4 
//-- Owner	 : hongsu.jung
//-- Comment : FOR USING DATABASE
//------------------------------------------------------------------------------
#if (!defined _INC_COMDEF)
#include <comdef.h>
#endif // (!defined _INC_COMDEF)

#if (!defined __PROF_UIS_H)
#include <Prof-UIS.h>
#endif // (!defined __PROF_UIS_H)

#if (!defined __PROF_SKIN_H)
#include <ProfSkin.h>
#endif // (!defined __PROF_SKIN_H)

// disable warnings C4192, C4146
#pragma warning( push )
//-- Version Check Option
#pragma comment(lib, "version.lib")
#pragma warning ( disable : 4192 )
#pragma warning ( disable : 4146 )
#pragma warning ( disable : 4244 )
#pragma warning ( disable : 4819 )
#pragma warning ( disable : 4995 )
#pragma warning ( disable : 4996 )



#include <objbase.h>
CComModule _Module;
#include <atlcom.h>


#pragma warning( pop )
#include <atlbase.h>

#include <objbase.h>
#include <gdiplus.h>
#pragma comment(lib, "gdiplus")
using namespace Gdiplus;

// rollback warnings C4192, C4146
#define USEXPTHEMES

#define SAFE_DELETE(p) { if (p) delete p; p=NULL;};
#define SafeDelete(ptr) { if(ptr) delete ptr; ptr=NULL;}

#include <string>
using namespace std; 

#define __EXT_MFC_ENABLE_TEMPLATED_CHARS

#ifdef UNICODE
typedef wstring tstring;
typedef wostringstream tostringstream;
typedef wofstream tofstream; 
typedef wifstream tifstream;
#else
typedef string tstring;
typedef ostringstream tostringstream;
typedef ofstream tofstream; 
typedef ifstream tifstream;
#endif

//-- 2013-04-24 hongsu.jung
//-- CDialogEx
#include <afxcontrolbars.h>     // MFC support for ribbons and control bars
#define CONCERT


#ifdef CONCERT
#include "mysql.h"
#include <WinSock2.h>
#pragma comment(lib,"libmysql.lib")
#pragma comment(lib,"ws2_32.lib")

#define DB_ADDRESS "192.168.0.36"
#define DB_ID "root"
#define DB_PASS "qwer1234!"
#define DB_NAME "project"

extern MYSQL mysql; // mysql의 전체적인 연동을 담당한다. 

#endif


#define _INI_4DA_LOCAL
//#define _INI_4DP_NET_RAM
//#define _INI_4DP_NET_RT