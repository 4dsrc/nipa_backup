/////////////////////////////////////////////////////////////////////////////
//
//  DeviceInfoMgr.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-09
//
/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "DeviceInfoMgr.h"
#include "ptpindex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CDeviceInfoMgr::CDeviceInfoMgr(void)
{
}

CDeviceInfoMgr::~CDeviceInfoMgr(void)
{
}

//------------------------------------------------------------------------------ 
//! @brief		GetDeviceInfo
//! @date		2011-06-09
//! @author	hongsu.jung
//------------------------------------------------------------------------------
CDeviceInfoBase* CDeviceInfoMgr::GetDeviceInfo(int nCode)
{
	switch(nCode)
	{
		case PTP_DPC_SAMSUNG_FW_VERSION						:	return	&m_nInfo[PTP_TYPE_FW_VERSION						] ; break;
		case PTP_CODE_BATTERYLEVEL							:	return	&m_nInfo[PTP_TYPE_BATTERYLEVEL						] ; break;
		case PTP_CODE_FUNCTIONALMODE						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_3D_SHOT					] ; break;
		//dh0.seo 2014-10-29
		case PTP_DPC_SAMSUNG_3D_SHOT						:	return	&m_nInfo[PTP_TYPE_FUNCTIONALMODE					] ; break;
		case PTP_CODE_IMAGESIZE								:	return	&m_nInfo[PTP_TYPE_IMAGESIZE							] ; break;
		case PTP_CODE_COMPRESSIONSETTING					:	return	&m_nInfo[PTP_TYPE_COMPRESSIONSETTING				] ; break;
		case PTP_CODE_WHITEBALANCE							:	return	&m_nInfo[PTP_TYPE_WHITEBALANCE						] ; break;
		case PTP_CODE_F_NUMBER								:	return	&m_nInfo[PTP_TYPE_F_NUMBER							] ; break;
		case PTP_CODE_FOCUSMODE								:	return	&m_nInfo[PTP_TYPE_FOCUSMODE							] ; break;
		case PTP_CODE_EXPOSUREMETERINGMODE					:	return	&m_nInfo[PTP_TYPE_EXPOSUREMETERINGMODE				] ; break;
		case PTP_CODE_FLASHMODE								:	return	&m_nInfo[PTP_TYPE_FLASHMODE							] ; break;
		case PTP_CODE_EXPOSUREINDEX							:	return	&m_nInfo[PTP_TYPE_EXPOSUREINDEX						] ; break;
		//CMiLRe 20141001 Date, Time 추가
		case PTP_OC_SAMSUNG_DATE_TIME						:	return	&m_nInfo[PTP_TYPE_DATETIME							] ; break;
		//CMiLRe 20140902 DRIVER GET 설정
		case PTP_DPC_STILL_CAPTURE_MODE_H_FRAME:
		case PTP_DPC_STILL_CAPTURE_MODE_L_FRAME:
		case PTP_CODE_STILLCAPTUREMODE						:	return	&m_nInfo[PTP_TYPE_STILLCAPTUREMODE					] ; break;
		case PTP_CODE_FOCUSMETERINGMODE						:	return	&m_nInfo[PTP_TYPE_FOCUSMETERINGMODE					] ; break;
		case PTP_CODE_SAMSUNG_EV							:	return	&m_nInfo[PTP_TYPE_SAMSUNG_EV						] ; break;
		case PTP_CODE_SAMSUNG_ISOSTEP						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_ISOSTEP					] ; break;		
		case PTP_CODE_SAMSUNG_DATETIMETYPE					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_DATETIMETYPE				] ; break;
		case PTP_CODE_SAMSUNG_DATETIMEHOUR					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_DATETIMEHOUR				] ; break;
		case PTP_CODE_SAMSUNG_DATETIMETIMEZONE				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_DATETIMETIMEZONE			] ; break;
		case PTP_CODE_SAMSUNG_DATETIMEDST					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_DATETIMEDST				] ; break;
		case PTP_CODE_SAMSUNG_DATETIMEIMPRINT				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_DATETIMEIMPRINT			] ; break;		
		case PTP_CODE_SAMSUNG_AUTOISORANGE					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_AUTOISORANGE				] ; break;
		case PTP_CODE_SAMSUNG_AFPRIORITY					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_AFPRIORITY				] ; break;
		//CMiLRe 20140902 ISO Expansion 추가
		case PTP_CODE_SAMSUNG_ISOEXPANSION					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_ISOEXPANSION				] ; break;
		case PTP_CODE_SAMSUNG_MFASSIST						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_MFASSIST					] ; break;
		case PTP_CODE_SAMSUNG_LINKAETOAF					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_LINKAETOAF				] ; break;
		case PTP_CODE_SAMSUNG_OIS							:	return	&m_nInfo[PTP_TYPE_SAMSUNG_OIS						] ; break;
		case PTP_CODE_SAMSUNG_SMARTFILTER					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_SMARTFILTER				] ; break;
		case PTP_CODE_SAMSUNG_PICTUREWIZARD					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PICTUREWIZARD				] ; break;
		//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
		case PTP_CODE_SAMSUNG_GetFocusPosition				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_GetFocusPosition				] ; break;
		case PTP_CODE_SAMSUNG_AEBRKORDER					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_AEBRKORDER				] ; break;
		case PTP_CODE_SAMSUNG_AEBRKAREA						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_AEBRKAREA					] ; break;
		case PTP_CODE_SAMSUNG_WBBRK							:	return	&m_nInfo[PTP_TYPE_SAMSUNG_WBBRK						] ; break;
		case PTP_CODE_SAMSUNG_PWBRK							:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PWBRK						] ; break;
		case PTP_CODE_SAMSUNG_FLASHEXPOSUREVALUE			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_FLASHEXPOSUREVALUE		] ; break;
		case PTP_CODE_SAMSUNG_SHUTTERSPEED					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_SHUTTERSPEED				] ; break;		
		case PTP_CODE_SAMSUNG_SMARTRANGE					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_SMARTRANGE				] ; break;
		case PTP_CODE_SAMSUNG_CSPACE						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_CSPACE					] ; break;
		case PTP_CODE_SAMSUNG_AELAFL						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_AELAFL					] ; break;
		case PTP_CODE_SAMSUNG_FILENAME						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_FILENAME					] ; break;
		case PTP_CODE_SAMSUNG_FILENUMBER					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_FILENUMBER				] ; break;
		case PTP_CODE_SAMSUNG_FOLDERTYPE					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_FOLDERTYPE				] ; break;
		case PTP_CODE_SAMSUNG_UNIQUEID						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_UNIQUEID					] ; break;
		case PTP_CODE_SAMSUNG_SYSTEMVOLUME					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_SYSTEMVOLUME				] ; break;
		case PTP_CODE_SAMSUNG_AFSOUND						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_AFSOUND					] ; break;
		case PTP_CODE_SAMSUNG_BUTTONSOUND					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_BUTTONSOUND				] ; break;
		//CMiLRe 20140901 E Shutter Sound 추가
		case PTP_CODE_SAMSUNG_E_SHUTTER_SOUND:		 return &m_nInfo[PTP_TYPE_SAMSUNG_E_SHUTTER_SOUND];		break;
		//CMiLRe 20140910 Movie Voice&Size 추가
		case PTP_CODE_SAMSUNG_VOICE_VOLUME:		 return &m_nInfo[PTP_TYPE_SAMSUNG_VOICE_VOLUME];		break;
		case PTP_CODE_SAMSUNG_MOV_SIZE:		 return &m_nInfo[PTP_TYPE_SAMSUNG_MOV_SIZE];		break;
		//CMiLRe 20140918 Picture Setting 추가
		case PTP_DPC_SAMSUNG_OLED_COLOR:									 return &m_nInfo[PTP_TYPE_SAMSUNG_OLED_COLOR];		break;
		case PTP_DPC_SAMSUNG_MFASSIST:									 return &m_nInfo[PTP_TYPE_SAMSUNG_MF_ASSIST];		break;
		case PTP_DPC_SAMSUNG_FOCUS_PEAKING_LEVEL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_FOCUS_PEAKING_LEVEL];		break;
		case PTP_DPC_SAMSUNG_FOCUS_PEAKING_COLOR:									 return &m_nInfo[PTP_TYPE_SAMSUNG_FOCUS_PEAKING_COLOR];		break;
		case PTP_DPC_SAMSUNG_BRIGHTNESS_ADJUST_GUIDE:									 return &m_nInfo[PTP_TYPE_SAMSUNG_BRIGHTNESS_ADJUST_GUIDE];		break;
		case PTP_DPC_SAMSUNG_FRAMING_MODE:									 return &m_nInfo[PTP_TYPE_SAMSUNG_FRAMING_MODE];		break;
		case PTP_DPC_SAMSUNG_DYNAMIC_RANGE:									 return &m_nInfo[PTP_TYPE_SAMSUNG_DYNAMIC_RANGE];		break;
		case PTP_DPC_SAMSUNG_LINK_AE_2_AF:									 return &m_nInfo[PTP_TYPE_SAMSUNG_LINK_AE_2_AF];		break;
		case PTP_DPC_SAMSUNG_OVER_EXPOSURE:									 return &m_nInfo[PTP_TYPE_SAMSUNG_OVER_EXPOSURE];		break;
		//CMiLRe 20140918 Picture Setting 추가
		case PTP_DPC_SAMSUNG_FLASH_USE_WIRELESS:									 return &m_nInfo[PTP_TYPE_SAMSUNG_FLASH_USE_WIRELESS];		break;
		case PTP_DPC_SAMSUNG_FLASH_CHANNEL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_FLASH_CHANNEL];		break;
		case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_A_ATTL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_GROUP_FLASH_GROUP_A_ATTL];		break;
		case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_A_MANUAL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_GROUP_FLASH_GROUP_A_MANUAL];		break;
		case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_A:									 return &m_nInfo[PTP_TYPE_SAMSUNG_GROUP_FLASH_GROUP_A];		break;
		case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_B_ATTL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_GROUP_FLASH_GROUP_B_ATTL];		break;
		case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_B_MANUAL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_GROUP_FLASH_GROUP_B_MANUAL];		break;
		case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_B:									 return &m_nInfo[PTP_TYPE_SAMSUNG_GROUP_FLASH_GROUP_B];		break;
		case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_C_ATTL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_GROUP_FLASH_GROUP_C_ATTL];		break;
		case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_C_MANUAL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_GROUP_FLASH_GROUP_C_MANUAL];		break;
		case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_C:									 return &m_nInfo[PTP_TYPE_SAMSUNG_GROUP_FLASH_GROUP_C];		break;

		case PTP_DPC_SAMSUNG_EXT_FLASH_USE_WIRELESS:									 return &m_nInfo[PTP_TYPE_SAMSUNG_EXT_FLASH_USE_WIRELESS];		break;
		case PTP_DPC_SAMSUNG_EXT_FLASH_CHANNEL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_EXT_FLASH_CHANNEL];		break;
		case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_A_ATTL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_EXT_GROUP_FLASH_GROUP_A_ATTL];		break;
		case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_A_MANUAL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_EXT_GROUP_FLASH_GROUP_A_MANUAL];		break;
		case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_A:									 return &m_nInfo[PTP_TYPE_SAMSUNG_EXT_GROUP_FLASH_GROUP_A];		break;
		case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_B_ATTL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_EXT_GROUP_FLASH_GROUP_B_ATTL];		break;
		case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_B_MANUAL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_EXT_GROUP_FLASH_GROUP_B_MANUAL];		break;
		case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_B:									 return &m_nInfo[PTP_TYPE_SAMSUNG_EXT_GROUP_FLASH_GROUP_B];		break;
		case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_C_ATTL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_EXT_GROUP_FLASH_GROUP_C_ATTL];		break;
		case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_C_MANUAL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_EXT_GROUP_FLASH_GROUP_C_MANUAL];		break;
		case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_C:									 return &m_nInfo[PTP_TYPE_SAMSUNG_EXT_GROUP_FLASH_GROUP_C];		break;

		//CMiLRe 20140918 Movie Setting 추가
		case PTP_DPC_SAMSUNG_MOV_QUALITY:									 return &m_nInfo[PTP_TYPE_SAMSUNG_MOV_QUALITY];		break;
		case PTP_DPC_SAMSUNG_MOV_MULTI_MOTION:									 return &m_nInfo[PTP_TYPE_SAMSUNG_MOV_MULTI_MOTION];		break;
		case PTP_DPC_SAMSUNG_MOV_FADER:									 return &m_nInfo[PTP_TYPE_SAMSUNG_MOV_FADER];		break;
		case PTP_DPC_SAMSUNG_WIND_CUT:									 return &m_nInfo[PTP_TYPE_SAMSUNG_WIND_CUT];		break;
		case PTP_DPC_SAMSUNG_MIC_MANUAL_VALUE:									 return &m_nInfo[PTP_TYPE_SAMSUNG_MIC_MANUAL_VALUE];		break;
		case PTP_DPC_SAMSUNG_MOVIE_SMART_RANGE:									 return &m_nInfo[PTP_TYPE_SAMSUNG_MOVIE_SMART_RANGE];		break;
		case PTP_DPC_SAMSUNG_INTERVAL_CYCLE_TIME_HOUR:									 return &m_nInfo[PTP_TYPE_SAMSUNG_INTERVAL_CYCLE_TIME_HOUR];		break;
		case PTP_DPC_SAMSUNG_INTERVAL_NUM:									 return &m_nInfo[PTP_TYPE_SAMSUNG_INTERVAL_NUM];		break;
		case PTP_DPC_SAMSUNG_INTERVAL_BEGINE_TIME_USE:									 return &m_nInfo[PTP_TYPE_SAMSUNG_INTERVAL_BEGINE_TIME_USE];		break;
		case PTP_DPC_SAMSUNG_TIMELABPS:									 return &m_nInfo[PTP_TYPE_SAMSUNG_TIMELABPS];		break;
		case PTP_DPC_SAMSUNG_INTERVAL_BEGINE_TIME_HOUR:									 return &m_nInfo[PTP_TYPE_SAMSUNG_INTERVAL_BEGINE_TIME_HOUR];		break;
		case PTP_DPC_SAMSUNG_INTERVAL_BEGINE_TIME_MIN:									 return &m_nInfo[PTP_TYPE_SAMSUNG_INTERVAL_BEGINE_TIME_MIN];		break;
		//CMiLRe 20140922 Picture Interval Capture 추가
		case PTP_DPC_SAMSUNG_INTERVAL_SET:									 return &m_nInfo[PTP_TYPE_SAMSUNG_INTERVAL_SET];		break;
		//CMiLRe 20140929 Custom 추가
		case PTP_DPC_SAMSUNG_DISP_BRIGHT_LEVEL:									 return &m_nInfo[PTP_TYPE_SAMSUNG_DISP_BRIGHT_LEVEL];		break;
		case PTP_DPC_SAMSUNG_DISP_AUTO_BRIGHT:									 return &m_nInfo[PTP_TYPE_SAMSUNG_DISP_AUTO_BRIGHT];		break;
		case PTP_DPC_SAMSUNG_DISP_COLOR_DETAIL_XY:									 return &m_nInfo[PTP_TYPE_SAMSUNG_DISP_COLOR_DETAIL_XY];		break;
		case PTP_DPC_SAMSUNG_COLOR_SPACE:									 return &m_nInfo[PTP_TYPE_SAMSUNG_COLOR_SPACE];		break;
		case PTP_DPC_SAMSUNG_DISTORTION_CORRECT:									 return &m_nInfo[PTP_TYPE_SAMSUNG_DISTORTION_CORRECT];		break;
		case PTP_DPC_SAMSUNG_WIFI_SENDNG_SIZE:									 return &m_nInfo[PTP_TYPE_SAMSUNG_WIFI_SENDNG_SIZE];		break;
		case PTP_DPC_SAMSUNG_EFS:									 return &m_nInfo[PTP_TYPE_SAMSUNG_EFS];		break;
		//CMiLRe 20141001 Setting Battery Selection 추가
		case PTP_DPC_SAMSUNG_BATTERY_SELECTION:									 return &m_nInfo[PTP_TYPE_SAMSUNG_BATTERY_SELECTION];		break;
		//CMiLRe 20141021 Setting Battery Selection 추가 - Internal, External Battry Level 추가
		case PTP_DPC_BATTERY_LEVEL_EXT:									 return &m_nInfo[PTP_TYPE_SAMSUNG_BATTERY_EXT];		break;
		//CMiLRe 20141008 Bracket Settings (AE,WB)
		case PTP_DPC_SAMSUNG_USER_DISP_ICON:									 return &m_nInfo[PTP_TYPE_SAMSUNG_USER_DISP_ICON];		break;
		case PTP_DPC_SAMSUNG_USER_DISP_DATE_TIME:									 return &m_nInfo[PTP_TYPE_SAMSUNG_USER_DISP_DATE_TIME];		break;
		case PTP_DPC_SAMSUNG_USER_DISP_BUTTONS:									 return &m_nInfo[PTP_TYPE_SAMSUNG_USER_DISP_BUTTONS];		break;
		case PTP_DPC_SAMSUNG_I_FN_FNO:									 return &m_nInfo[PTP_TYPE_SAMSUNG_I_FN_FNO];		break;
		case PTP_DPC_SAMSUNG_I_FN_SHUTTER_SPEED:									 return &m_nInfo[PTP_TYPE_SAMSUNG_I_FN_SHUTTER_SPEED];		break;
		case PTP_DPC_SAMSUNG_I_FN_EV:									 return &m_nInfo[PTP_TYPE_SAMSUNG_I_FN_EV];		break;
		case PTP_DPC_SAMSUNG_I_FN_ISO:									 return &m_nInfo[PTP_TYPE_SAMSUNG_I_FN_ISO];		break;
		case PTP_DPC_SAMSUNG_I_FN_WB:									 return &m_nInfo[PTP_TYPE_SAMSUNG_I_FN_WB];		break;
		case PTP_DPC_SAMSUNG_I_FN_I_ZOOM:									 return &m_nInfo[PTP_TYPE_SAMSUNG_I_FN_I_ZOOM];		break;
		case PTP_DPC_SAMSUNG_BRK_AE_STEP:									 return &m_nInfo[PTP_TYPE_SAMSUNG_BRK_AE_STEP];		break;
		case PTP_DPC_SAMSUNG_BRK_WB_SET:									 return &m_nInfo[PTP_TYPE_SAMSUNG_BRK_WB_SET];		break;
		case PTP_DPC_SAMSUNG_BRK_PW_TYPE:									 return &m_nInfo[PTP_TYPE_SAMSUNG_BRK_PW_TYPE];		break;
		//dh0.seo 2014-10-02 Key Mapping
		case PTP_DPC_SAMSUNG_KEY_MPPING_PREVIEW						:	return &m_nInfo[PTP_TYPE_SAMSUNG_KEY_PREVIEW];		break;
		case PTP_DPC_SAMSUNG_KEY_MPPING_AEL							:	return &m_nInfo[PTP_TYPE_SAMSUNG_KEY_AEL];		break;
		case PTP_DPC_SAMSUNG_KEY_MPPING_AF_ON						:	return &m_nInfo[PTP_TYPE_SAMSUNG_KEY_AF_ON];		break;
		case PTP_DPC_SAMSUNG_KEY_MPPING_CUSTOM_KEY1					:	return &m_nInfo[PTP_TYPE_SAMSUNG_KEY_CUSTOM1];		break;
		case PTP_DPC_SAMSUNG_KEY_MPPING_CUSTOM_KEY2					:	return &m_nInfo[PTP_TYPE_SAMSUNG_KEY_CUSTOM2];		break;
		case PTP_DPC_SAMSUNG_KEY_MPPING_CUSTOM_KEY3					:	return &m_nInfo[PTP_TYPE_SAMSUNG_KEY_CUSTOM3];		break;
		case PTP_DPC_SAMSUNG_KEY_MPPING_CUSTOM_WHEEL				:	return &m_nInfo[PTP_TYPE_SAMSUNG_KEY_WHEEL];		break;
		case PTP_DPC_SAMSUNG_KEY_MPPING_COMMAND_DIAL				:	return &m_nInfo[PTP_TYPE_SAMSUNG_KEY_DIAL];		break;

		case PTP_CODE_SAMSUNG_LDC							:	return	&m_nInfo[PTP_TYPE_SAMSUNG_LDC						] ; break;
		case PTP_CODE_SAMSUNG_AFLAMP						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_AFLAMP					] ; break;
		//CMiLRe 20141029 White Balnace Detail 종류별로 R/W
		case PTP_DPC_SAMSUNG_WB_DETAIL_X:			return	&m_nInfo[PTP_TYPE_SAMSUNG_WB_DETAIL_X			] ; break;		
		case PTP_CODE_SAMSUNG_WB_DETAIL_AUTO				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_WB_DETAIL_AUTO			] ; break;		
		case PTP_CODE_SAMSUNG_WB_DETAIL_DAYLIGHT			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_WB_DETAIL_DAYLIGHT		] ; break;
		case PTP_CODE_SAMSUNG_WB_DETAIL_CLOUDY				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_WB_DETAIL_CLOUDY			] ; break;
		case PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_W		:	return	&m_nInfo[PTP_TYPE_SAMSUNG_WB_DETAIL_FLURESCENT_W	] ; break;
		case PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_NW		:	return	&m_nInfo[PTP_TYPE_SAMSUNG_WB_DETAIL_FLURESCENT_NW	] ; break;
		case PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_D		:	return	&m_nInfo[PTP_TYPE_SAMSUNG_WB_DETAIL_FLURESCENT_D	] ; break;
		case PTP_CODE_SAMSUNG_WB_DETAIL_TUNGSTEN			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_WB_DETAIL_AUTO_TUNGSTEN		] ; break;
		case PTP_CODE_SAMSUNG_WB_DETAIL_AUTO_TUNGSTEN			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_WB_DETAIL_TUNGSTEN		] ; break;
		case PTP_CODE_SAMSUNG_WB_DETAIL_FLASH				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_WB_DETAIL_FLASH			] ; break;
		case PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_WB_DETAIL_KELVIN			] ; break;
		case PTP_CODE_SAMSUNG_SCENE_MODE					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_SCENE_MODE				] ; break;		
		case PTP_CODE_SAMSUNG_FACE_TONE						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_FACE_TONE					] ; break;
		case PTP_CODE_SAMSUNG_FACE_RETOUCH					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_FACE_RETOUCH				] ; break;
		case PTP_CODE_SAMSUNG_MAGIC_MODE					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_MAGIC_MODE				] ; break;
		case PTP_CODE_SAMSUNG_MAGIC_FRAME					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_MAGIC_FRAME				] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_STANDARD			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_STANDARD		] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_VIVID				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_VIVID			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_PORTRAIT			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_PORTRAIT		] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_LANDSCAPE			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_LANDSCAPE		] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_FOREST				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_FOREST			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_RETRO				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_RETRO			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_COOL				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_COOL			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_CALM				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_CALM			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_CLASSIC				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_CLASSIC			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM1				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_CUSTOM1			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM2				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_CUSTOM2			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM3				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_CUSTOM3			] ; break;
		//CMiLRe 20140912 NX1 SmartWizard PW Detail Set/Get(Saturation, Sharpness, Contrast, Hue)
		case PTP_CODE_SAMSUNG_PW_DETAIL_STANDARD_ETC			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_STANDARD_ETC		] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_VIVID_ETC				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_VIVID_ETC			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_PORTRAIT_ETC			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_PORTRAIT_ETC		] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_LANDSCAPE_ETC			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_LANDSCAPE_ETC		] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_FOREST_ETC				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_FOREST_ETC			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_RETRO_ETC				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_RETRO_ETC			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_COOL_ETC				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_COOL_ETC			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_CALM_ETC				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_CALM_ETC			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_CLASSIC_ETC				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_CLASSIC_ETC			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM1_ETC				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_CUSTOM1_ETC			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM2_ETC				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_CUSTOM2_ETC			] ; break;
		case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM3_ETC				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PW_DETAIL_CUSTOM3_ETC			] ; break;
		case PTP_CODE_SAMSUNG_DRIVE_DETAIL_BURST				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_DRIVE_DETAIL_BURST			] ; break;
		case PTP_CODE_SAMSUNG_DRIVE_DETAIL_TIMER				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_DRIVE_DETAIL_TIMER			] ; break;
		case PTP_CODE_SAMSUNG_PHYSICAL_MODE						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_PHYSICAL_MODE					] ; break;
		case PTP_CODE_SAMSUNG_I_SMART_FILTER					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_I_SMART_FILTER				] ; break;
		//CMiLRe 2015129 줌 컨트롤
		case PTP_CODE_SAMSUNG_ZOOM_CTRL_SET:
		case PTP_CODE_SAMSUNG_ZOOM_CTRL_GET						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_ZOOM_CTRL			] ; break;
		//dh0.seo 2014-09-12
		case PTP_CODE_SAMSUNG_POWERSAVE							:	return	&m_nInfo[PTP_TYPE_SAMSUNG_POWERSAVE						] ; break;
		//dh0.seo 2014-09-15
		case PTP_CODE_SAMSUNG_AUTODISPLAY						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_AUTODISPLAY					] ; break;
		//dh0.seo 2014-09-15
		case PTP_CODE_SAMSUNG_QUICKVIEW							:	return	&m_nInfo[PTP_TYPE_SAMSUNG_QUICKVIEW						] ; break;
		//SDK Test
		//CMiLRe 20140929 Custom 추가
		case PTP_DPC_SAMSUNG_VIDEO_OUT							:	return	&m_nInfo[PTP_TYPE_SAMSUNG_AUTODISPLAY					] ; break;
		case PTP_DPC_SAMSUNG_ANYNET								:	return	&m_nInfo[PTP_TYPE_SAMSUNG_ANYNET						] ; break;
		case PTP_DPC_SAMSUNG_HDMI								:	return	&m_nInfo[PTP_TYPE_SAMSUNG_HDMI							] ; break;
		case PTP_DPC_SAMSUNG_3D_HDMI							:	return	&m_nInfo[PTP_TYPE_SAMSUNG_3D_HDMI						] ; break;
		case PTP_DPC_SAMSUNG_LANGUAGE							:	return	&m_nInfo[PTP_TYPE_SAMSUNG_LANGUAGE						] ; break;
		case PTP_DPC_SAMSUNG_MODE_HELP_GUIDE					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_MODE_HELP_GUIDE				] ; break;
		case PTP_DPC_SAMSUNG_FUNCTION_HELP_GUIDE				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_FUNCTION_HELP_GUIDE			] ; break;
		case PTP_DPC_SAMSUNG_TOUCH_OPERATION					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_TOUCH_OPERATION				] ; break;
		case PTP_DPC_SAMSUNG_AF_RELEASE_PRIORITY				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_AF_RELEASE_PRIORITY			] ; break;
		case PTP_DPC_SAMSUNG_EVF_BUTTON_INTERACTION				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_EVF_BUTTON_INTERACTION		] ; break;
		case PTP_DPC_SAMSUNG_GRID_LINE							:	return	&m_nInfo[PTP_TYPE_SAMSUNG_GRID_LINE						] ; break;
		case PTP_DPC_SAMSUNG_NOISE_HIGH_ISO						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_NOISE_HIGH_ISO				] ; break;
		case PTP_DPC_SAMSUNG_NOISE_LONG_TERM					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_NOISE_LONG_TERM				] ; break;
		case PTP_DPC_SAMSUNG_BLUETOOTH							:	return	&m_nInfo[PTP_TYPE_SAMSUNG_BLUETOOTH						] ; break;
		case PTP_DPC_SAMSUNG_BLUETOOTH_AUTO_TIME				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_BLUETOOTH_AUTO_TIME			] ; break;
		case PTP_DPC_SAMSUNG_MY_SMARTPHONE						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_MY_SMARTPHONE					] ; break;
		case PTP_DPC_SAMSUNG_WIFI_PRIVACY_LOCK					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_WIFI_PRIVACY_LOCK				] ; break;
		case PTP_DPC_SAMSUNG_DUAL_BAND_MOBILE_AP				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_DUAL_BAND_MOBILE_AP			] ; break;
		case PTP_DPC_SAMSUNG_SENSOR_CLEANING_START_UP			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_SENSOR_CLEANING_START_UP		] ; break;
		case PTP_DPC_SAMSUNG_SENSOR_CLEANING_SHUT_DOWN			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_SENSOR_CLEANING_SHUT_DOWN		] ; break;
		//CMiLRe 20141030 DMF 추가
		case PTP_DPC_SAMSUNG_DMF								:	return	&m_nInfo[PTP_TYPE_SAMSUNG_DMF							] ; break;
		//CMiLRe 20141119 Sleep Mode 에서 캡쳐
		case PTP_DPC_SAMSUNG_DP_SLEEP_MODE						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_DP_SLEEP_MODE					] ; break;
// 		case PTP_DPC_SAMSUNG_SELECTION_AF_POS					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_SELECT_AF_POSITION			] ; break;
// 		case PTP_DPC_SAMSUNG_TOUCH_AF							:	return	&m_nInfo[PTP_TYPE_SAMSUNG_TOUCH_AF						] ; break;
		case PTP_DPC_SAMSUNG_SELECTION_AF_BOX_TYPE				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_SELECT_AF_BOX_SIZE			] ; break;
		case PTP_DPC_SAMSUNG_MIN_SHUTTER_SPEED					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_MIN_SHUTTER					] ; break;
		case PTP_DPC_SAMSUNG_SELECTION_AF_BOX_STATUS			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_SELECTION_AF_BOX_STATUS		] ; break;
		case PTP_DPC_SAMSUNG_BRK_DEPTH_SET						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_BRK_DEPTH_SET					] ; break;
		case PTP_DPC_SAMSUNG_CENTER_MARKER_DISPLAY				:	return	&m_nInfo[PTP_TYPE_SAMSUNG_CENTER_MARKER_DISPLAY			] ; break;
		case PTP_DPC_SAMSUNG_CENTER_MARKER_SIZE					:	return	&m_nInfo[PTP_TYPE_SAMSUNG_CENTER_MARKER_SIZE			] ; break;
		case PTP_DPC_SAMSUNG_CENTER_MARKER_TRANSPARENCY			:	return	&m_nInfo[PTP_TYPE_SAMSUNG_CENTER_MARKER_TRANSPARENCY	] ; break;
		case PTP_DPC_SAMSUNG_DISPLAY_SET						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_DISPLAY_SET					] ; break;
		case PTP_DPC_SAMSUNG_ROTATE_EVENT						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_ROTATE_EVENT					] ; break;
		case PTP_DPC_SAMSUNG_CURRENT_STATE						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_CURRENT_STATE					] ; break;
		case PTP_DPC_SAMSUNG_AUTO_ISO_VALUE						:	return	&m_nInfo[PTP_TYPE_SAMSUNG_AUTO_ISO_VALUE				] ; break;

		//GH5
		case PTP_DPC_LENS_POWER_CTRL							:	return	&m_nInfo[PTP_TYPE_LENS_POWERT_CTRL						] ; break;
		case PTP_DPC_CONTRAST_GET								:	return	&m_nInfo[PTP_TYPE_CONTRAST_GET							] ; break;
		case PTP_DPC_WB_DETAIL_CUSTOM							:	return	&m_nInfo[PTP_TYPE_WB_DETAIL_CUSTOM						] ; break;
		case PTP_DPC_WB_DETAIL_KELVIN							:	return	&m_nInfo[PTP_TYPE_WB_DETAIL_KELVIN						] ; break;
		case PTP_DPC_CAP_IMAGE_ASPECT_RATIO						:	return	&m_nInfo[PTP_TYPE_CAP_IMAGE_ASPECT_RATIO				] ; break;
		case PTP_DPC_MOV_REC_FORMAT								:	return	&m_nInfo[PTP_TYPE_MOV_REC_FORMAT						] ; break;
		case PTP_DPC_MOV_LUMINANCE_LEVEL						:	return	&m_nInfo[PTP_TYPE_MOV_LUMINANCE_LEVEL					] ; break;
		case PTP_DPC_MOV_I_DYNAMIC								:	return	&m_nInfo[PTP_TYPE_MOV_I_RESOLUTION						] ; break;
		case PTP_DPC_MOV_I_RESOLUTION							:	return	&m_nInfo[PTP_TYPE_MOV_I_RESOLUTION						] ; break;
		case PTP_DPC_MOV_STA_E_STABILIZATION					:	return	&m_nInfo[PTP_TYPE_MOV_STA_E_STABILIZATION				] ; break;
		case PTP_DPC_MOV_STA_FOCAL_LENGTH_SET					:	return	&m_nInfo[PTP_TYPE_MOV_STA_FOCAL_LENGTH_SET				] ; break;
		case PTP_DPC_MOV_MIC_LEVEL_ADJ							:	return	&m_nInfo[PTP_TYPE_MOV_MIC_LEVEL_ADJ						] ; break;
		case PTP_DPC_MOV_MIC_LEVEL_LIMITER						:	return	&m_nInfo[PTP_TYPE_MOV_MIC_LEVEL_LIMITER					] ; break;
		case PTP_DPC_MOV_WIND_NOISE_CANCELLER					:	return	&m_nInfo[PTP_TYPE_MOV_WIND_NOISE_CANCELLER				] ; break;
		case PTP_DPC_MOV_DIGITAL_ZOOM							:	return	&m_nInfo[PTP_TYPE_MOV_DIGITAL_ZOOM						] ; break;
		case PTP_DPC_SET_REC_AREA								:	return	&m_nInfo[PTP_TYPE_SET_REC_AREA							] ; break;
		case PTP_DPC_SET_LENS_POSITION_RESUME					:	return	&m_nInfo[PTP_TYPE_SET_LENS_POSITION_RESUME				] ; break;
		case PTP_DPC_SET_SYSTEM_FREQUENCY						:	return	&m_nInfo[PTP_TYPE_SET_SYSTEM_FREQUENCY					] ; break;

		case PTP_DPC_LCD										:	return	&m_nInfo[PTP_TYPE_LCD									] ; break;
		case PTP_DPC_PEAKING									:	return	&m_nInfo[PTP_TYPE_PEAKING								] ; break;
		case PTP_DPC_PHOTO_STYLE								:	return	&m_nInfo[PTP_TYPE_PHOTO_STYLE							] ; break;
		case PTP_DPC_PS_CONTRAST								:	return	&m_nInfo[PTP_TYPE_PS_CONTRAST							] ; break;
		case PTP_DPC_PS_SHARPNESS								:	return	&m_nInfo[PTP_TYPE_PS_SHARPNESS							] ; break;
		case PTP_DPC_PS_NOISE_REDUCTION							:	return	&m_nInfo[PTP_TYPE_PS_NOISE_REDUCTION					] ; break;
		case PTP_DPC_PS_SATURATION								:	return	&m_nInfo[PTP_TYPE_PS_SATURATION							] ; break;
		case PTP_DPC_PS_HUE										:	return	&m_nInfo[PTP_TYPE_PS_HUE								] ; break;
		case PTP_DPC_PS_FILTER_EFFECT							:	return	&m_nInfo[PTP_TYPE_PS_FILTER_EFFECT						] ; break;
		case PTP_DPC_WB_ADJUST_AB								:	return	&m_nInfo[PTP_TYPE_WB_ADJUST_AB							] ; break;
		case PTP_DPC_WB_ADJUST_MG								:	return	&m_nInfo[PTP_TYPE_WB_ADJUST_MG							] ; break;
		case PTP_DPC_ALART_PRE_STOP_TEMP						:	return	&m_nInfo[PTP_TYPE_ALART_PRE_STOP_TEMP					] ; break;
	
		//dh0.seo 2014-11-25 Flash Detail
/*		case PTP_DPC_SAMSUNG_FLASH_DETAIL					:		return	&m_nInfo[PTP_TYPE_SAMSUNG_FLASH_DETAIL						] ; break;
		case PTP_DPC_SAMSUNG_SMART_FLASH_DETAIL				:		return	&m_nInfo[PTP_TYPE_SAMSUNG_SMART_FLASH_DETAIL				] ; break;
		case PTP_DPC_SAMSUNG_FILL_IN_FLASH_DETAIL			:		return	&m_nInfo[PTP_TYPE_SAMSUNG_FILL_IN_FLASH_DETAIL				] ; break;
		case PTP_DPC_SAMSUNG_FILL_IN_RED_FLASH_DETAIL		:		return	&m_nInfo[PTP_TYPE_SAMSUNG_FILL_IN_RED_FLASH_DETAIL			] ; break;
		case PTP_DPC_SAMSUNG_1ST_CURTAIN_FLASH_DETAIL		:		return	&m_nInfo[PTP_TYPE_SAMSUNG_1ST_CURTAIN_FLASH_DETAIL			] ; break;
		case PTP_DPC_SAMSUNG_2nd_CURTAIN_FLASH_DETAIL		:		return	&m_nInfo[PTP_TYPE_SAMSUNG_2nd_CURTAIN_FLASH_DETAIL			] ; break;
		case PTP_DPC_SAMSUNG_AUTO_FLASH_DETAIL				:		return	&m_nInfo[PTP_TYPE_SAMSUNG_AUTO_FLASH_DETAIL					] ; break;
		case PTP_DPC_SAMSUNG_AUTO_RED_EYE_FLASH_DETAIL		:		return	&m_nInfo[PTP_TYPE_SAMSUNG_AUTO_RED_EYE_FLASH_DETAIL			] ; break;*/
	}
	return NULL;
}

//------------------------------------------------------------------------------ 
//! @brief		SetType
//! @date		2011-06-09
//! @author	hongsu.jung
//------------------------------------------------------------------------------
void CDeviceInfoMgr::SetType(int nCode, int nType)
{
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		pInfo->SetType(nType);
}

//------------------------------------------------------------------------------ 
//! @brief		GetType
//! @date		2011-06-09
//! @author	hongsu.jung
//------------------------------------------------------------------------------
int CDeviceInfoMgr::GetType(int nCode)
{
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		return pInfo->GetType();
	return PTP_VALUE_NONE;
}

//CMiLRe 20141029 White Balnace Detail 종류별로 R/W
int CDeviceInfoMgr::GetPropCode_Sub(int nCode)
{
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		return pInfo->GetPropCode_Sub();
	return PTP_VALUE_NONE;
}

//------------------------------------------------------------------------------ 
//! @brief		Add
//! @date		2011-06-09
//! @author	hongsu.jung
//------------------------------------------------------------------------------
void CDeviceInfoMgr::AddEnum(int nCode, int nValue)
{
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		pInfo->AddEnum(nValue);
}

void CDeviceInfoMgr::AddStr(int nCode, CString str)
{
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		pInfo->AddString(str);	
}

//------------------------------------------------------------------------------ 
//! @brief		CleanAll()
//! @date		2011-06-09
//! @author	hongsu.jung
//------------------------------------------------------------------------------
void CDeviceInfoMgr::CleanAll(int nCode)
{
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		pInfo->CleanAll();
}

//------------------------------------------------------------------------------ 
//! @brief		SetCurrent
//! @date		2011-06-09
//! @author	hongsu.jung
//------------------------------------------------------------------------------
void CDeviceInfoMgr::SetCurrentStr(int nCode, CString str)
{
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		pInfo->SetCurrentStr(str);	
}

void CDeviceInfoMgr::SetCurrentEnum(int nCode, int nValue)
{
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		pInfo->SetCurrentEnum(nValue);
}

//------------------------------------------------------------------------------ 
//! @brief		GetCurrent
//! @date		2011-06-09
//! @author	hongsu.jung
//------------------------------------------------------------------------------
CString CDeviceInfoMgr::GetCurrentStr(int nCode)
{
	CString strValue;
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		strValue = pInfo->GetCurrentStr();		
	return strValue;
}

int CDeviceInfoMgr::GetCurrentEnum(int nCode)
{
	int nRet = 0;
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		nRet = pInfo->GetCurrentEnum();
	return nRet;
}

//------------------------------------------------------------------------------ 
//! @brief		SetDefault
//! @date		2011-06-09
//! @author	hongsu.jung
//------------------------------------------------------------------------------
void CDeviceInfoMgr::SetDefaultStr(int nCode, CString str)
{
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		pInfo->SetDefaultStr(str);
}

void CDeviceInfoMgr::SetDefaultEnum(int nCode, int nValue)
{
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		pInfo->SetDefaultEnum(nValue);	
}

//------------------------------------------------------------------------------ 
//! @brief		GetDefault
//! @date		2011-06-09
//! @author	hongsu.jung
//------------------------------------------------------------------------------
CString CDeviceInfoMgr::GetDefaultStr(int nCode)
{
	CString strValue;
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		strValue = pInfo->GetDefaultStr();	
	return strValue;
}

int CDeviceInfoMgr::GetDefaultEnum(int nCode)
{
	int nRet = 0;
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		nRet = pInfo->GetDefaultEnum();
	return nRet;
}

//------------------------------------------------------------------------------ 
//! @brief		GetPorpValueStatusEnum
//! @date		2011-6-15
//! @author		Lee JungTaek
//! @note	 	Check Exist Enum Value
//------------------------------------------------------------------------------ 
int CDeviceInfoMgr::GetPropValueCntEnum(int nCode)
{
	int nRet = 0;

	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		nRet = pInfo->GetPropValueCntEnum();
	return nRet;
}

//------------------------------------------------------------------------------ 
//! @brief		GetPorpValueStatusStr
//! @date		2011-6-15
//! @author		Lee JungTaek
//! @note	 	Check Exist String Value
//------------------------------------------------------------------------------ 
int CDeviceInfoMgr::GetPropValueCntStr(int nCode)
{
	int nRet = 0;

	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		nRet = pInfo->GetPropValueCntStr();
	return nRet;
}

//------------------------------------------------------------------------------ 
//! @brief		GetPorpValueStatusEnum
//! @date		2011-6-15
//! @author		Lee JungTaek
//! @note	 	Check Exist Enum Value
//------------------------------------------------------------------------------ 
int CDeviceInfoMgr::GetPorpValueStatusEnum(int nCode, int nPropValue)
{
	int nStatus = PROPERTY_STATUS_DISABLE;
	
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		nStatus = pInfo->GetPropValueStatusEnum(nPropValue);
	return nStatus;
}

//------------------------------------------------------------------------------ 
//! @brief		GetPorpValueStatusStr
//! @date		2011-6-15
//! @author		Lee JungTaek
//! @note	 	Check Exist String Value
//------------------------------------------------------------------------------ 
int CDeviceInfoMgr::GetPorpValueStatusStr(int nCode, CString strPropValue)
{
	int nStatus = PROPERTY_STATUS_DISABLE;

	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		nStatus = pInfo->GetPropValueStatusStr(strPropValue);
	return nStatus;
}

//------------------------------------------------------------------------------ 
//! @brief		GetPorpValueStatusEnum
//! @date		2011-6-15
//! @author		Lee JungTaek
//! @note	 	Check Exist Enum Value
//------------------------------------------------------------------------------ 
CUIntArray* CDeviceInfoMgr::GetPropValueListEnum(int nCode)
{
	CUIntArray * parEnum = NULL;

	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		parEnum = pInfo->GetPropValueListEnum();
	return parEnum;
}

//------------------------------------------------------------------------------ 
//! @brief		SaveBank
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
BOOL CDeviceInfoMgr::SaveBank(CString strFile)
{
	CFile f;
    BOOL bret = f.Open(strFile, CFile::modeCreate | CFile::modeWrite);
    if(!bret)
        return FALSE;

    CArchive ar( &f, CArchive::store);
    try
    {
		for(int i=0; i<PTP_TYPE_SIZE; i++)
			m_nInfo[i].Serialize(ar);
    }
    catch(...)
    {
        bret = FALSE;
    }
	
	ar.Close();
	f.Close();
	if(!bret)
        return FALSE;
	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief		LoadBank
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
BOOL CDeviceInfoMgr::LoadBank(CString strFile)
{
	BOOL bret;
    CFile f;
    bret = f.Open(strFile, CFile::modeRead);
    if(!bret)
        return FALSE;

    CArchive ar( &f, CArchive::load);
    try
    {
		for(int i=0; i<PTP_TYPE_SIZE; i++)
			m_nInfo[i].Serialize(ar);
    }
    catch(...)
    {
         bret = FALSE;
    }

	ar.Close();
	f.Close();
	if(!bret)
        return FALSE;
	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropCode
//! @date		2011-07-15
//! @author	jeansu
//------------------------------------------------------------------------------
void CDeviceInfoMgr::SetPropCode(int nCode){
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		pInfo->SetPropCode(nCode);
}

//CMiLRe 20141029 White Balnace Detail 종류별로 R/W
void CDeviceInfoMgr::SetPropCode_Sub(int nCode, int nSubCode){
	CDeviceInfoBase* pInfo = NULL;
	pInfo = GetDeviceInfo(nCode);
	if(pInfo)
		pInfo->SetPropCode_Sub(nSubCode);
}
