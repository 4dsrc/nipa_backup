/////////////////////////////////////////////////////////////////////////////
//
//  DeviceInfoMgr.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-09
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "DeviceInfoBase.h"
#include "ptpIndex.h"

class AFX_EXT_CLASS CDeviceInfoMgr
{
public:
	CDeviceInfoMgr(void);
	~CDeviceInfoMgr(void);

private:
	CDeviceInfoBase	 m_nInfo[PTP_TYPE_SIZE];

public:	
	CDeviceInfoBase* GetDeviceInfo(int nCode);
	void SetPropCode(int nCode);
	//CMiLRe 20141029 White Balnace Detail �������� R/W
	void SetPropCode_Sub(int nCode, int nSubCode);
	int GetPropCode_Sub(int nCode);
	//-- 2011-7-22 Lee JungTaek
	CDeviceInfoBase* GetDeviceInfoByIndex(int nIndex) {return &m_nInfo[nIndex];}

	void SetType(int nCode, int nType);
	int GetType(int nCode);
	//-- 2011-06-09 hongsu.jung
	void AddEnum(int nCode, int nValue);
	void AddStr(int nCode, CString str);
	
	void CleanAll(int nCode);

	//-- 2011-06-09 hongsu.jung	
	void SetCurrentStr(int nCode, CString str);
	void SetCurrentEnum(int nCode, int nValue);	
	CString GetCurrentStr(int nCode);
	int GetCurrentEnum(int nCode);

	//-- 2011-06-09 hongsu.jung	
	void SetDefaultStr(int nCode, CString str);
	void SetDefaultEnum(int nCode, int nValue);	
	CString GetDefaultStr(int nCode);
	int GetDefaultEnum(int nCode);	

	//-- 2011-6-15 Lee JungTaek
	int	GetPropValueCntEnum(int nCode);
	int	GetPropValueCntStr(int nCode); 

	int GetPorpValueStatusEnum(int nCode, int nPropValue);
	int GetPorpValueStatusStr(int nCode, CString strPropValue);

	//-- 2011-6-21 Lee JungTaek
	CUIntArray* GetPropValueListEnum(int nCode);

	//-- 2011-07-14 Load/Save
	BOOL LoadBank(CString strFile);
	BOOL SaveBank(CString strFile);
};

