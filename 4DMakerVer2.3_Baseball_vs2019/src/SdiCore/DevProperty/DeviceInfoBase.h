/////////////////////////////////////////////////////////////////////////////
//
//  DeviceInfoBase.h
//	--> not necessary DeviceInfoBase.cpp 
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date		2011-06-09
//
/////////////////////////////////////////////////////////////////////////////
#pragma once
#include "ptpindex.h"
#include "SdiDefines.h"

class CDeviceInfoBase : public IDeviceInfo
{
	DECLARE_SERIAL( CDeviceInfoBase)    // 직렬화 선언
public:
	CDeviceInfoBase();


protected:
	SdiUInt16 m_nPropCode;
	//CMiLRe 20141029 White Balnace Detail 종류별로 R/W
	SdiUInt16 m_nPropCode_Sub;
	SdiUInt16 m_nType;
	CUIntArray m_arEnum;
	CStringArray m_arStr;

	SdiUInt32 m_nCurr;
	CString m_strCurr;
	SdiUInt32 m_nDefault;
	CString m_strDefault;

public:	
	virtual SdiUInt16 GetPropCode(){ return m_nPropCode; }
	void SetPropCode(SdiUInt16 nPropCode) { m_nPropCode = nPropCode; }

	//CMiLRe 20141029 White Balnace Detail 종류별로 R/W
	virtual SdiUInt16 GetPropCode_Sub(){ return m_nPropCode_Sub; }
	void SetPropCode_Sub(SdiUInt16 nPropCodeSub) { m_nPropCode_Sub = nPropCodeSub; }

	void SetType(int nType) { m_nType = (SdiUInt16)nType;}
	virtual SdiUInt16	 GetType() { return m_nType;}

	//-- 2011-06-09 hongsu.jung	
	void AddEnum(int nValue)		{	m_arEnum.Add(nValue);	}
	void AddString(CString str)	{	m_arStr.Add(str);				}
	void CleanAll()					{	m_arEnum.RemoveAll();		m_arStr.RemoveAll();	}
	
	void SetCurrentEnum(int nValue)	{ m_nCurr = nValue; }
	void SetCurrentStr(CString str)	{ m_strCurr = str;	}
	virtual SdiUInt32	  GetCurrentEnum()	{ return m_nCurr; }
	virtual CString GetCurrentStr()	{ return m_strCurr;}
	
	void SetDefaultEnum(int nValue)	{ m_nDefault = nValue; }
	void SetDefaultStr(CString str)	{ m_strDefault = str;	}
	virtual SdiUInt32	  GetDefaultEnum()	{ return m_nDefault; }
	virtual CString GetDefaultStr()	{ return m_strDefault;}

	//-- 2011-6-15 Lee JungTaek
	SdiUInt16	GetPropValueCntEnum() {return (SdiUInt16)m_arEnum.GetCount();}
	SdiUInt16	GetPropValueCntStr() {return (SdiUInt16)m_arStr.GetCount();}

	int GetPropValueStatusEnum(int nPropValue);	
	int GetPropValueStatusStr(CString strPropValue);

	//-- 2011-6-21 Lee JungTaek	
	virtual CUIntArray* GetPropValueListEnum() {return &m_arEnum;}
	//-- 2011-7-12 jeansu
	virtual CStringArray* GetPropValueListStr() {return &m_arStr;}

	//-- 2011-07-14
	virtual void Serialize( CArchive& archive );

	//-- 2017-09-08 yongmin.lee 1Step next&prev Value
	virtual bool GetNextEnum(int& nValue);
	virtual bool GetPrevEnum(int& nValue);
	virtual bool GetNextStr(CString& str);
	virtual bool GetPrevStr(CString& str);
};