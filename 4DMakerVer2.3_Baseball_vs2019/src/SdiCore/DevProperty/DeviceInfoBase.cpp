/////////////////////////////////////////////////////////////////////////////
//
//  DeviceInfoBase.cpp
//	--> not necessary DeviceInfoBase.cpp 
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date		2011-06-09
//
/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "DeviceInfoBase.h"
#include "ptpindex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_SERIAL( CDeviceInfoBase, CObject, 1)
////////////////////////////////////////////////////////
// Construction/Destruction
////////////////////////////////////////////////////////

CDeviceInfoBase::CDeviceInfoBase() 
{
	m_nPropCode = 0x00;
	//CMiLRe 20141029 White Balnace Detail 종류별로 R/W
	m_nPropCode_Sub = 0x00;
	m_nCurr		= 0x00;
	m_nDefault	 = 0x00;
	m_nType = PTP_VALUE_NONE;
}

//------------------------------------------------------------------------------ 
//! @brief		Serialize
//! @date		2011-6-15
//! @author		Lee JungTaek
//! @note	 	Check Exist Enum Value
//------------------------------------------------------------------------------ 
void CDeviceInfoBase::Serialize( CArchive& ar)
{
    // 부모클래스의 함수를 호출
    CObject::Serialize( ar);
    // 이클래스의 데이터를 보존
    if( ar.IsLoading() == TRUE)
    {
		if(GetType() == PTP_VALUE_STRING)
			ar >> m_nPropCode >> m_nType >> m_strCurr;
		else
			ar >> m_nPropCode >> m_nType >> m_nCurr;
	}
    else
    {
		if(GetType() == PTP_VALUE_STRING)
			ar << m_nPropCode << m_nType << m_strCurr;
		else
			ar << m_nPropCode << m_nType << m_nCurr;
    }
}




int CDeviceInfoBase::GetPropValueStatusEnum(int nPropValue)
{
	int nStatus = PROPERTY_STATUS_DISABLE;
	for(int  i = 0; i < m_arEnum.GetCount(); i++)
	{
		if(nPropValue == m_arEnum.GetAt(i))
		{
			if(nPropValue == GetCurrentEnum())
				nStatus = PROPERTY_STATUS_SELECTED;
			else
				nStatus = PROPERTY_STATUS_NORMAL;

			break;
		}
	}
	return nStatus;			
}
	
int CDeviceInfoBase::GetPropValueStatusStr(CString strPropValue)
{
	int nStatus = PROPERTY_STATUS_DISABLE;
	for(int  i = 0; i < m_arStr.GetCount(); i++)
	{
		if(strPropValue == m_arStr.GetAt(i))
		{
			if(strPropValue == GetCurrentStr())
				nStatus = PROPERTY_STATUS_SELECTED;
			else
				nStatus = PROPERTY_STATUS_NORMAL;

			break;
		}
	}
	return nStatus;
}

bool CDeviceInfoBase::GetNextEnum(int& nValue)
{
	int nCount = m_arEnum.GetCount();

	for(int  i = 0; i < nCount; i++)
	{
		if(GetCurrentEnum() == m_arEnum.GetAt(i))
		{
			if((nCount-1) >= (i+1))
				nValue = m_arEnum.GetAt(i+1);
			else
				nValue = m_arEnum.GetAt(i);

			return TRUE;
		}
	}

	return FALSE;
}

bool CDeviceInfoBase::GetPrevEnum(int& nValue)
{ 
	int nCount = m_arEnum.GetCount();

	for(int  i = 0; i < nCount; i++)
	{
		if(GetCurrentEnum() == m_arEnum.GetAt(i))
		{
			if(i > 0)
				nValue = m_arEnum.GetAt(i-1);
			else
				nValue = m_arEnum.GetAt(i);

			return TRUE;
		}
	}

	return FALSE;
}

bool CDeviceInfoBase::GetNextStr(CString& str)	
{ 
	int nCount = m_arEnum.GetCount();

	for(int  i = 0; i < nCount; i++)
	{
		if(GetCurrentEnum() == m_arEnum.GetAt(i))
		{
			if((nCount-1) >= i+1)
				str = m_arStr.GetAt(i+1);
			else
				str = m_arStr.GetAt(i);

			return TRUE;
		}
	}

	return FALSE;
}

bool CDeviceInfoBase::GetPrevStr(CString& str)
{ 
	int nCount = m_arEnum.GetCount();

	for(int  i = 0; i < nCount; i++)
	{
		if(GetCurrentEnum() == m_arEnum.GetAt(i))
		{
			if(i > 0)
				str = m_arStr.GetAt(i-1);
			else
				str = m_arStr.GetAt(i);

			return TRUE;
		}
	}

	return FALSE;
}