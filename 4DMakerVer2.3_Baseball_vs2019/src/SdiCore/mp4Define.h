#define MAX_TRACK_NUM 4

typedef tInt64 tMmfClockTime;

typedef enum {
	MMF_MEDIA_TYPE_NONE,
	MMF_MEDIA_TYPE_AUDIO,
	MMF_MEDIA_TYPE_VIDEO,
	MMF_MEDIA_TYPE_SUBTITLE,
	MMF_MEDIA_TYPE_TIMECODE,
	MMF_MEDIA_TYPE_UNKNOWN
} eMmfMediaType;

typedef enum {
	MMF_AUDIO_FORMAT_NONE,
	MMF_AUDIO_FORMAT_AAC_RAW,
	MMF_AUDIO_FORMAT_AAC,
	MMF_AUDIO_FORMAT_MP3,
	MMF_AUDIO_FORMAT_MP2,
	MMF_AUDIO_FORMAT_PCM,
	MMF_AUDIO_FORMAT_ALAW,
	MMF_AUDIO_FORMAT_uLAW,
	MMF_AUDIO_FORMAT_MAX
} eMmfAudioFormat;

typedef enum {
	MMF_VIDEO_FORMAT_NONE,
	MMF_VIDEO_FORMAT_YUV420,
	MMF_VIDEO_FORMAT_YUV422,
	MMF_VIDEO_FORMAT_H264,
	MMF_VIDEO_FORMAT_AVC,
	MMF_VIDEO_FORMAT_MPEG1,
	MMF_VIDEO_FORMAT_MPEG2,
	MMF_VIDEO_FORMAT_MPEG4,
	MMF_VIDEO_FORMAT_MJPEG,
	MMF_VIDEO_FORMAT_YUV420_NLC,
	MMF_VIDEO_FORMAT_YUV422_NLC,
	MMF_VIDEO_FORMAT_HEVC,
	MMF_VIDEO_FORMAT_MAX
} eMmfVideoFormat;

typedef enum {
	MMF_STEREO_VIDEO_NONE,
	MMF_STEREO_VIDEO_TOP_BOTTOM_LEFT_FIRST,
	MMF_STEREO_VIDEO_TOP_BOTTOM_RIGHT_FIRST,
	MMF_STEREO_VIDEO_SIDE_BY_SIDE_LEFT_FIRST,
	MMF_STEREO_VIDEO_SIDE_BY_SIDE_RIGHT_FIRST,
	MMF_STEREO_VIDEO_FRAME_SEQUENCE_LEFT_FIRST,
	MMF_STEREO_VIDEO_FRAME_SEQUENCE_RIGHT_FIRST,
	MMF_STEREO_VIDEO_VERTICAL_LINE_INTERLEAVE_LEFT_FIRST,
	MMF_STEREO_VIDEO_VERTICAL_LINE_INTERLEAVE_RIGHT_FIRST,
	MMF_STEREO_VIDEO_VIEW_SEQUENCE_LEFT_FIRST,
	MMF_STEREO_VIDEO_VIEW_SEQUENCE_RIGHT_FIRST,
	MMF_STEREO_VIDEO_MAX
} eMmfStereoVideo;

typedef enum {
	MMF_VID_COLR_RANGE_FULL,
	MMF_VID_COLR_RANGE_16_255,
	MMF_VID_COLR_RANGE_16_235,
	MMF_VID_COLR_RANGE_MAX
} eMmfVideoColorRange;

typedef struct {
	unsigned int fNumberNum;
	unsigned int fNumberDen;
	unsigned int ISOValue;
	unsigned int exposureTimeNum;
	unsigned int exposureTimeDen;
	unsigned short focalLengthNum;
	unsigned short focalLengthDen;
	int exposureBiasValueNum;
	int exposureBiasValueDen;
	unsigned int meteringMode;
	unsigned int whiteBalance;
	int colorSpace;
	unsigned int lightSource;
} tMmfExifFrameInfo;

struct sFrameEntry {
	long size;
	tUint64 offset;
	int framesInChunk;
	bool key_frame;
	tMmfClockTime cts;
	tMmfClockTime dts;
	tMmfExifFrameInfo vfexif;
};

typedef struct {
	eMmfVideoFormat format;         /*!< Video Stream Format */
	int             nWidth;         /*!< width */
	int             nHeight;        /*!< height */
	float           frameRate;      /*!< frame rate */
	eMmfStereoVideo stereoVideo;    /*!< Stereo Video Information */
	int             orientation;    /*!< Video Orientation in degrees (0~360) */
	unsigned char   gop_type;       /*!< 0: IPP, 1: IBP, 2: III */
	eMmfVideoColorRange videoColorRange;
} tMmfVideoInfo;

typedef struct {
	eMmfAudioFormat format;             /*!< Audio Stream Format */
	int             nChannel;           /*!< channel */
	int             nBitPerSample;      /*!< bits per sample */
	int             nSampleRate;        /*!< sampling rate */
	int             nSamplesPerFrame;   /*!< samples per frame */
} tMmfAudioInfo;

