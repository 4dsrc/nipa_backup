// 0513PTP.cpp : Defines the entry point for the console application.

#include <stdafx.h>	// for windows api
#include <PTPApi.h>
#include <PTP_HOST.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void start_ptphost(PTPHOST *host);
void ptphosttask(PTPHOST *host);

UINT PTPTask( void* pParam )
{
	
	TRACE("START PTP HOST TASK #####\n");

	//PTPHOST *m_pPTPHost = NULL;
	PTPHOST *m_pPTPHost= new PTPHOST;
	
	start_ptphost(m_pPTPHost);

	m_pPTPHost->setStatusForPTPHOST(PTPHOST_STATUS_CONNECTED);
	ptphosttask(m_pPTPHost);
	
	AfxMessageBox(_T("PTPTask"));

	return 0;
}


void
start_ptphost(PTPHOST *host)
{
	int i =0;
	//host = new PTPHOST; // create

	i = host->create(0);

	/* 아래와 같은 Code가 존재하지 않아 host->initialize()의
	if (mpEnvCfg->isConfigCompleted() == FALSE) 부분에서 에러 발생됨
	*/
	
	/* 
	ipb_instance = CPictBridgeInstance::createPictBridge();
	...
	// configuration 하는 code필요	
	int ret = ipb_instance->setDeviceConfigForPTP("SAMSUNG MiniKet", "Samsung Electronics", "01.2006", 0);
	*/
	// not sure whether we need to set the all 
	host->setDeviceConfig("PTP HOST","Samsung","0.9", 0); // little endian(0),big endian(1)
	host->setNumStorage(1);
//	host->setStorageConfig(int number, char * name, char * path, int type, int fsType)
//	host->setStorageActive(int number, unsigned char bActive)

	//rujin setTransportConfig
	host->setTransportConfig("", 1,1);// little endian(0),big endian(1)

	host->setNumThumbList(9);		
	i = host->initialize();
	host->setDebugPrintLevel(3);
	host->setOptions(1,1,0,0,0);
	
	return;
}

void ptphosttask(PTPHOST *host)
{
	int ret= PTP_ERROR_NONE;
	ret = host->transaction();	

	host->setStatusForPTPHOST(PTPHOST_STATUS_NEXT);
	ret = host->transaction();	
	host->setStatusForPTPHOST(PTPHOST_STATUS_REQUEST_COPY);
	ret = host->transaction();		

#if 0
	while(1)
	{
		ret = host->transaction();		
	}
	return;
#endif

}

