#include "stdafx.h"
#include <USBDevice.h>
#include <setupapi.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//unicode  warning disable
#pragma warning(disable : 4996)
// device 처리를 위한 lib linking
#pragma comment(lib, "Setupapi.lib")

// Copy from HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\DeviceClasses
// USB의 Device Class GUID:
// Device Class GUID: 각각의 장치마다 부여되는 GUID
// Device Interface GUID: 장비에 따른 I/O Interface에 부여되는 GUID
static const GUID GUID_DEVINTERFACE_USB = 
{ 0xA5DCBF10, 0x6530, 0x11D2, { 0x90, 0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED } };

CUSBDevice::CUSBDevice()
{
	m_pNotifyDeviceChange = NULL;
	memset(m_szDevicePath,0,sizeof(TCHAR)*MAX_PATH);	
}

CUSBDevice::~CUSBDevice()
{
	
}

CUSBDevice* 
CUSBDevice::GetInstance()
{
	static CUSBDevice USBDevice_Instance;

	return &USBDevice_Instance;
}


BOOL CUSBDevice::RegisterHandleForNotify( HWND hNotification )
{
	HDEVNOTIFY hDevNotify;
	DEV_BROADCAST_DEVICEINTERFACE NotificationFilter;

	ZeroMemory( &NotificationFilter, sizeof(DEV_BROADCAST_DEVICEINTERFACE) );

	// WM_DEVICECHANGE USB Device의 변경이 있을 경우 통보를 받을수 있도록 설정한다.
	NotificationFilter.dbcc_size		= sizeof(DEV_BROADCAST_DEVICEINTERFACE);
	NotificationFilter.dbcc_devicetype	= DBT_DEVTYP_DEVICEINTERFACE;
	NotificationFilter.dbcc_classguid	= GUID_DEVINTERFACE_USB;

	hDevNotify = RegisterDeviceNotification(hNotification, &NotificationFilter, 
		DEVICE_NOTIFY_WINDOW_HANDLE);

	if( !hDevNotify )
	{
		return FALSE;
	}

	return TRUE;
}


BOOL CUSBDevice::SetNotifyCallback( NOTIFYDEVICECHANGE pNotifyDeviceChange )
{

	//AfxMessageBox(L"SetNotifyCallback");

	if(pNotifyDeviceChange == NULL)
	{
		return FALSE;	
	}

	m_pNotifyDeviceChange = pNotifyDeviceChange;

	return TRUE;
}


BOOL CUSBDevice::NotifyDeviceChange( WPARAM wChangeType, LPARAM lDeviceHeader )
{
	PDEV_BROADCAST_HDR pDeviceHeader;
	PDEV_BROADCAST_DEVICEINTERFACE pDeviceInterface;

	// Device의 생성과 소멸시에만 체크한다.
	if(	DBT_DEVICEARRIVAL == wChangeType 
		|| DBT_DEVICEREMOVECOMPLETE == wChangeType )
	{
		pDeviceHeader = (PDEV_BROADCAST_HDR)lDeviceHeader;

		// 생성된 Device의 형태가 Interface class 인지 검사한다.
		if( (pDeviceHeader->dbch_devicetype) == DBT_DEVTYP_DEVICEINTERFACE )
		{
			pDeviceInterface = (PDEV_BROADCAST_DEVICEINTERFACE)pDeviceHeader;
			UpdateUSBDeviceInformation(pDeviceInterface, wChangeType);
		}
	}

	return TRUE;
}


void CUSBDevice::GetUSBDeviceInformation(TCHAR* szDevicePath)
{
	_tcscpy(szDevicePath,m_szDevicePath);
}

void CUSBDevice::UpdateUSBDeviceInformation( PDEV_BROADCAST_DEVICEINTERFACE pDeviceInfomation, WPARAM wChangeType )
{
	CString szDevId = pDeviceInfomation->dbcc_name+4;
	int idx = szDevId.ReverseFind(_T('#'));
	szDevId.Truncate(idx);
	szDevId.Replace(_T('#'), _T('\\'));
	szDevId.MakeUpper();

	DWORD dwFlag = DBT_DEVICEARRIVAL != wChangeType ? DIGCF_DEVICEINTERFACE : (DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);
	HDEVINFO hDevInfo = SetupDiGetClassDevs(&GUID_DEVINTERFACE_USB,NULL,NULL,dwFlag);

	SP_DEVINFO_DATA spDevInfoData;
	spDevInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
	for(int i=0; SetupDiEnumDeviceInfo(hDevInfo, i, &spDevInfoData); i++) 
	{
		DWORD nSize=0 ;
		TCHAR buf[MAX_PATH];

		if ( !SetupDiGetDeviceInstanceId(hDevInfo, &spDevInfoData, buf, sizeof(buf), &nSize) ) 
		{
			return;
		} 

		if ( szDevId == buf ) 
		{
			TCHAR buf[MAX_PATH];
			DWORD DataT ;
			DWORD nSize = 0;

			CString strPathPrefix = _T("\\\\?\\");
			CString strPath = szDevId;
			strPath.Replace(_T('\\'),_T('#'));
			strPath = strPathPrefix + strPath + _T("#");			

			// GUID값
			memset(buf,0,sizeof(TCHAR)*MAX_PATH);
			SetupDiGetDeviceRegistryProperty(hDevInfo, &spDevInfoData, 
				SPDRP_CLASSGUID, &DataT, (PBYTE)buf, sizeof(buf), &nSize);

			CString strGUID = buf;
			strPath = strPath + strGUID;

			_stprintf(m_szDevicePath,_T("%s"),strPath.GetBuffer(0));
			strPath.ReleaseBuffer();


	//rujin UpdateUSBDeviceInformation

			if(m_pNotifyDeviceChange != NULL)
			{
				if(wChangeType == DBT_DEVICEARRIVAL)
				{
					(*m_pNotifyDeviceChange)(DEVICE_NOTIFY_DEVICEARRIVAL,(void*)NULL,(void*)NULL);

					memset(buf,0,sizeof(TCHAR)*MAX_PATH);
					SetupDiGetDeviceRegistryProperty(hDevInfo, &spDevInfoData, 
						SPDRP_LOCATION_INFORMATION, &DataT, (PBYTE)buf, sizeof(buf), &nSize);

					(*m_pNotifyDeviceChange)(DEVICE_NOTIFY_DEVICEDESC,(void*)buf,(void*)NULL);

					memset(buf,0,sizeof(TCHAR)*MAX_PATH);

					SetupDiGetDeviceRegistryProperty(hDevInfo, &spDevInfoData, 
						SPDRP_DEVICEDESC, &DataT, (PBYTE)buf, sizeof(buf), &nSize);

					(*m_pNotifyDeviceChange)(DEVICE_NOTIFY_LOCATION_INFORMATION,(void*)buf,(void*)NULL);
				}
				else if(wChangeType == DBT_DEVICEARRIVAL)
				{
					(*m_pNotifyDeviceChange)(DEVICE_NOTIFY_DEVICEREMOVECOMPLETE,(void*)NULL,(void*)NULL);
				}
				else if(wChangeType == DBT_DEVICEREMOVECOMPLETE)
				{
					(*m_pNotifyDeviceChange)(DEVICE_NOTIFY_DEVICEREMOVECOMPLETE,(void*)NULL,(void*)NULL);
				}
				else
				{
					AfxMessageBox(_T("XXX"));
				}
			}
			return;
		}
	}

}

