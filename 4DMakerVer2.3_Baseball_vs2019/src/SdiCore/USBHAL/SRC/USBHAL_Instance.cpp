/*
 * Project Name: Gene
 *  
 * Copyright 2006 by Samsung Electronics, Inc.
 * All rights reserved.
 * 
 * Project Description:
 * 'Gene' is the name of common software platform for CE  product such as
 * optical storage product (BD player, BD recorder, HD DVD player, etc.),
 * personal product (MiniKet, PMP, etc.), and TV product (digital TV, STB, etc.).
 * The Gene has at least one software component. And implementation target of
 * said software component will be CE product.
 * The Gene project's main outputs are as following.
 *     1. Reference architecture for CE product
 *     2. Software component which can be reused with at least one product line
 *     3. Software development kit (SDK), and adaptation development kit (ADK)
 *     4. Tool and infra to help easily reuse said 1~3 outputs.
 */
/*!
 \file CUSBHAL_Instance.cpp
 \brief Implement CUSBHALInstance Class.
 \author <a class="el" href="mailto:yuseong74@samsung.com">JEON, YU-SEONG</a>
		 <br><b>Dept:</b> DM R&D Center, SE Lab
 \date 2006-11-03
 \version 0.50 
*/
#include "stdafx.h"
#include <USBHAL_Instance.h>
#include <USBHAL.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

int CUSBHALInstance::msUSBHALCreated = 0;

IUSBHAL *
CUSBHALInstance::createUSBHAL(void)
{
	CUSBHAL* usbHal = NULL;

	if (msUSBHALCreated == 0)
	{
		usbHal = new CUSBHAL;

		if (usbHal == NULL)
		{
			return NULL;
		}
		else
		{
			msUSBHALCreated = 0;                // chlee 멀티에서는 몇번이고 생성해야 돼.ㅋ msUSBHALCreated = 1;  
			return (IUSBHAL *)usbHal;
		}
	}
	else
		return NULL;

	return 0;
}

void
CUSBHALInstance::destroyUSBHAL(IUSBHAL* ifUSBHAL)
{

	CUSBHAL* usbHal = NULL;

	if (ifUSBHAL != NULL)
	{
		usbHal = (CUSBHAL *)ifUSBHAL;
		delete usbHal;
	}

	msUSBHALCreated = 0;
	
	return;

}
