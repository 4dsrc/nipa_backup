/*
 * Project Name: Gene
 *  
 * Copyright 2006 by Samsung Electronics, Inc.
 * All rights reserved.
 * 
 * Project Description:
 * 'Gene' is the name of common software platform for CE  product such as
 * optical storage product (BD player, BD recorder, HD DVD player, etc.),
 * personal product (MiniKet, PMP, etc.), and TV product (digital TV, STB, etc.).
 * The Gene has at least one software component. And implementation target of
 * said software component will be CE product.
 * The Gene project's main outputs are as following.
 *     1. Reference architecture for CE product
 *     2. Software component which can be reused with at least one product line
 *     3. Software development kit (SDK), and adaptation development kit (ADK)
 *     4. Tool and infra to help easily reuse said 1~3 outputs.
 */
/*!
 \file CUSBHAL.cpp
 \brief Implement CUSBHAL Class.
 \author <a class="el" href="mailto:yuseong74@samsung.com">JEON, YU-SEONG</a>
		 <br><b>Dept:</b> DM R&D Center, SE Lab
 \date 2006-11-03
 \version 0.50 
*/
#include "stdafx.h"
#include <USBHAL.h>
#include <USBDevice.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#if 1   // duvallee 07-29-2010 : for LibUSB
#include "usb.h"
#endif
#ifdef _DEBUG
#define PRINTD(x)					TRACE(x)
#define PRINTD0(x)					TRACE0(x)
#define PRINTD1(x, a1)				TRACE1(x,a1)
#define PRINTD2(x, a1, a2)			TRACE2(x,a1,a2)
#define PRINTD3(x, a1, a2, a3)		TRACE3(x,a1,a2,a3)
#define PRINTD4(x, a1, a2, a3, a4)	
#else
#define PRINTD(x)
#define PRINTD0(x)
#define PRINTD1(x, a1)
#define PRINTD2(x, a1, a2)
#define PRINTD3(x, a1, a2, a3)
#define PRINTD4(x, a1, a2, a3, a4)
#endif

#if 1   // duvallee 07-29-2010 : Windows7 OS 정보를 갖고 있다.
BOOL CUSBHAL::m_bOsVersionWin7   = FALSE;
#endif

/**
 * Endpoint Max Parellel bit Size.
 * For detail, see usb_char.h
 */
#define EP0_MAXP_SIZE				8
#define EP1_MAXP_SIZE				8
#define EP2_MAXP_SIZE				64
#define EP3_MAXP_SIZE				64

#define MAX_READ_TIMEOUT		5
#define MAX_WRITE_TIMEOUT		10

CUSBHAL::CUSBHAL()
{
	memset(&mDevInfo,0,sizeof(USBHALDeviceInfoType));
	mDevInfo.FdNum = NULL;

    mUsbTimeOut = 10000;
    m_pUSBDeviceHandle = NULL;

}

CUSBHAL::~CUSBHAL()
{

}
#if 1   // duvallee 07-29-2010 : Hexa String to Hexa Value
DWORD CUSBHAL::wtohexa(CString szValue)
{
    DWORD dwHexa        = 0;
    DWORD dwShift       = 0;
    int nIndex          = 0;
    for (nIndex = szValue.GetLength(), dwShift = 0; nIndex > 0; nIndex--, dwShift++)
    {
        DWORD dwOneDigit    = 0;
        switch (szValue.GetAt(nIndex - 1))
        {
            case '0' :
                dwOneDigit  = 0;
                break;
            case '1' :
                dwOneDigit  = 1;
                break;
            case '2' :
                dwOneDigit  = 2;
                break;
            case '3' :
                dwOneDigit  = 3;
                break;
            case '4' :
                dwOneDigit  = 4;
                break;
            case '5' :
                dwOneDigit  = 5;
                break;
            case '6' :
                dwOneDigit  = 6;
                break;
            case '7' :
                dwOneDigit  = 7;
                break;
            case '8' :
                dwOneDigit  = 8;
                break;
            case '9' :
                dwOneDigit  = 9;
                break;
            case 'a' :
            case 'A' :
                dwOneDigit  = 10;
                break;
            case 'b' :
            case 'B' :
                dwOneDigit  = 11;
                break;
            case 'c' :
            case 'C' :
                dwOneDigit  = 12;
                break;
            case 'd' :
            case 'D' :
                dwOneDigit  = 13;
                break;
            case 'e' :
            case 'E' :
                dwOneDigit  = 14;
                break;
            case 'f' :
            case 'F' :
                dwOneDigit  = 15;
                break;
        }
        dwHexa  |= dwOneDigit << (dwShift * 4);
    }
    return dwHexa;
}
#endif

int
CUSBHAL::openUSB(char* name, int mode)
{
    //향후에 Device를 선택하는 부분이 추가되어야 함
    CUSBDevice *pUsbDevice;
    pUsbDevice = CUSBDevice::GetInstance();

    //여기서 에러처리 부분을 추가해야 함
    pUsbDevice->GetUSBDeviceInformation(mDevInfo.Name);

	//-- Get USB List
	int length = MultiByteToWideChar(CP_ACP,0,name,-1,NULL,NULL);
	WCHAR *ustr = new WCHAR[length];
	MultiByteToWideChar(CP_ACP,0,name,strlen(name)+1,ustr,length);

    wsprintf(mDevInfo.Name,_T("%s"),ustr);
//	AfxMessageBox(ustr);
	delete [] ustr;
    mDevInfo.FdNum = CreateFile ( mDevInfo.Name ,						 // mDevInfo.Name		chlee
        GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL, 
        OPEN_EXISTING, 
        0, 
        NULL );

    if(mDevInfo.FdNum == INVALID_HANDLE_VALUE)
    {
        return USBHAL_ERR_OPEN;
    }
    else
    {
        //rujin 0102 openUSB
        mDevInfo.ConnectStatus = 1;
    }

    return USBHAL_ERR_NONE;
}

void
CUSBHAL::closeUSB(void)
{
	if(mDevInfo.FdNum != NULL)
	{
		CloseHandle(mDevInfo.FdNum);
		memset(&mDevInfo,0,sizeof(USBHALDeviceInfoType));
		mDevInfo.FdNum = NULL;
	}
}


int
CUSBHAL::readBulk(char* buffer, unsigned long size)
{
	BOOL ret = FALSE;
	DWORD dwReadSize = 0;
	DWORD dwTemp = -1;
	int nTimeout = 0;

	if( mDevInfo.FdNum == NULL )
	{
		return USBHAL_ERR_OPEN;
	}

	if( (buffer == NULL) || (size < 0) )
	{
		return USBHAL_ERR_PARAM;
	}

	//size 0일 경우에는 아무일도 하지 않음
	if(size == 0)
	{
		return USBHAL_ERR_NONE;
	}

	while(size > 0)
	{
		ret = ReadFile(mDevInfo.FdNum,
			(buffer + dwReadSize),
			size*sizeof(char),
			&dwTemp,
			NULL);
#if 0
		if(ret == FALSE)
		{
			// 에러 출력
			return USBHAL_ERR_WRITE;
		}
#endif
		if(dwTemp > 0)
		{
			dwReadSize += dwTemp;
			size -= dwTemp;

			if(nTimeout != 0)
				nTimeout = 0;
		}
		else if(dwTemp == 0)
		{
			nTimeout++;

			if(nTimeout > MAX_READ_TIMEOUT)
			{
				//에러 출력
				return USBHAL_ERR_READDATA_NONE;
			}

		}
		else
		{
			return USBHAL_ERR_READ;
		}
	}

	return USBHAL_ERR_NONE;

}


int
CUSBHAL::writeBulk(char* data, unsigned long size)
{
	BOOL ret = FALSE;
	DWORD dwWriteSize = 0;
	DWORD dwTemp = -1;
	int nTimeout = 0;

	if( mDevInfo.FdNum == NULL )
	{
		return USBHAL_ERR_OPEN;
	}

	if( (data == NULL) || (size < 0) )
	{
		return USBHAL_ERR_PARAM;
	}
	
	//size 0일 경우에는 아무일도 하지 않음
	if(size == 0)
	{
		return USBHAL_ERR_NONE;
	}

	while(size > 0)
	{
		ret = WriteFile(mDevInfo.FdNum,
					(data + dwWriteSize),
					size*sizeof(char),
					&dwTemp,
					NULL);

		//dwTemp와 dwWriteSize가 같아야한다.

#if 0
		if(ret == FALSE)
		{
			// 에러 출력
			return USBHAL_ERR_WRITE;
		}
#endif
		Sleep(5);

		if(dwTemp > 0)
		{
			dwWriteSize += dwTemp;
			size -= dwTemp;

			if(nTimeout != 0)
				nTimeout = 0;
		}
		else if(dwTemp == 0)
		{
			nTimeout++;

			if(nTimeout > MAX_WRITE_TIMEOUT)
			{
				//에러 출력
				return USBHAL_ERR_WRITEDATA_NONE;
			}

		}
		else
		{
			return USBHAL_ERR_WRITE;
		}
	}

	return USBHAL_ERR_NONE;
}

int
CUSBHAL::writeIntr(char* data, unsigned long size)
{
	//현재 미구현

	return USBHAL_ERR_NONE;
}

int
CUSBHAL::flushAllBulk(void)
{
	BOOL ret;

	if( mDevInfo.FdNum == NULL )
	{
		return USBHAL_ERR_OPEN;
	}

	ret = FlushFileBuffers(mDevInfo.FdNum);

	if(ret == FALSE)
	{
		// 에러 출력
		return USBHAL_ERR_FLUSH;
	}

	return USBHAL_ERR_NONE;
}

int
CUSBHAL::flushTxBulk(void)
{
	BOOL ret;

	if( mDevInfo.FdNum == NULL )
	{
		return USBHAL_ERR_OPEN;
	}

	ret = FlushFileBuffers(mDevInfo.FdNum);

	if(ret == FALSE)
	{
		// 에러 출력
		return USBHAL_ERR_FLUSH;
	}

	return USBHAL_ERR_NONE;
}

int
CUSBHAL::flushRxBulk(void)
{
	BOOL ret;

	if( mDevInfo.FdNum == NULL )
	{
		return USBHAL_ERR_OPEN;
	}

	ret = FlushFileBuffers(mDevInfo.FdNum);

	if(ret == FALSE)
	{
		// 에러 출력
		return USBHAL_ERR_FLUSH;
	}

	return USBHAL_ERR_NONE;
}

int 
CUSBHAL::checkConnection(void)
{
	// 1 : connected
	// 0 : disconnected

	int status = 0;
	
	status = mDevInfo.ConnectStatus ;

	return status;



}

int
CUSBHAL::isNullPktNeed(int ep, unsigned long size)
{
	return 0;
}

int
CUSBHAL::writeBulkNullPkt(void)
{
	//일단 지금 사용하지 않음
	/*
	BOOL ret = FALSE;
	DWORD dwTemp = -1;

	if( mDevInfo.FdNum == NULL )
	{
		return USBHAL_ERR_OPEN;
	}

	ret = WriteFile(mDevInfo.FdNum,
				NULL,
				0,
				&dwTemp,
				NULL);

	if(ret == FALSE)
	{
		// 에러 출력
		return USBHAL_ERR_NULL_PACKET;
	}
	*/
	return USBHAL_ERR_NONE;
}

int
CUSBHAL::writeIntrNullPkt(void)
{
	return 0;
}

void
CUSBHAL::SetUsbEndPointInfo(struct usb_device *dev)
{  
    typedef unsigned char SdiUInt8;

    int i;
    SdiUInt8 j;
    SdiUInt8 k;
    SdiUInt8 nEndPoint;
    struct usb_endpoint_descriptor *endPoint;
	SdiUInt8 interface;

    for (i = 0; i < dev->descriptor.bNumConfigurations; i++)
    {
        for (j = 0; j < dev->config[i].bNumInterfaces; j++)
        {
            interface = dev->config[i].interface[j].altsetting->bInterfaceNumber;
            endPoint = dev->config[i].interface[j].altsetting->endpoint;
            nEndPoint = dev->config[i].interface[j].altsetting->bNumEndpoints;

            for (k = 0; k < nEndPoint; k++)
            {
                if (endPoint[k].bmAttributes==USB_ENDPOINT_TYPE_BULK)
				{
                    if ((endPoint[k].bEndpointAddress & USB_ENDPOINT_DIR_MASK)==USB_ENDPOINT_DIR_MASK)
                    {
                        mEndPointInfo.epIn=endPoint[k].bEndpointAddress;
                        mEndPointInfo.epIn_maxpacket=endPoint[k].wMaxPacketSize;
                    }
                    if ((endPoint[k].bEndpointAddress & USB_ENDPOINT_DIR_MASK)==0)
                    {
                        mEndPointInfo.epOut=endPoint[k].bEndpointAddress;
                        mEndPointInfo.epOut_maxpacket=endPoint[k].wMaxPacketSize;
                    }
                }
				else if (endPoint[k].bmAttributes==USB_ENDPOINT_TYPE_INTERRUPT)
				{
                    if ((endPoint[k].bEndpointAddress & USB_ENDPOINT_DIR_MASK)==USB_ENDPOINT_DIR_MASK)
                    {
                        mEndPointInfo.epInt=endPoint[k].bEndpointAddress;
                    }
                }
            }
            return;
        }
    }    
}

