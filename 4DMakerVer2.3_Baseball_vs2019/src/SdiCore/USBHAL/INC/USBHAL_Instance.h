/*
 * Project Name: Gene
 *  
 * Copyright 2006 by Samsung Electronics, Inc.
 * All rights reserved.
 * 
 * Project Description:
 * 'Gene' is the name of common software platform for CE  product such as
 * optical storage product (BD player, BD recorder, HD DVD player, etc.),
 * personal product (MiniKet, PMP, etc.), and TV product (digital TV, STB, etc.).
 * The Gene has at least one software component. And implementation target of
 * said software component will be CE product.
 * The Gene project's main outputs are as following.
 *     1. Reference architecture for CE product
 *     2. Software component which can be reused with at least one product line
 *     3. Software development kit (SDK), and adaptation development kit (ADK)
 *     4. Tool and infra to help easily reuse said 1~3 outputs.
 */
/*!
 \file CUSBHAL_Instance.h
 \brief Define CUSBHALInstance Class.
 \author <a class="el" href="mailto:yuseong74@samsung.com">JEON, YU-SEONG</a>
		 <br><b>Dept:</b> DM R&D Center, SE Lab
 \date 2006-11-03
 \version 0.50 
*/


#ifndef __CUSBHAL_INSTANCE_H__
#define __CUSBHAL_INSTANCE_H__

#include "IUSBHAL.h"

/*!
\ingroup USBHAL_Label
\brief USB HAL 사용을 위한 Interface의 생성/파괴를 담당하는 Class.
\par Features :
  -# CUSBHAL Class Instance를 생성/파괴.
  -# CUSBHAL Class의 Interface Class인 IUSBHAL Class의 포인터를 반환.
  -# 하나의 CUSBHAL Class Instance 생성만 가능.
\par Requirements :
  <UL>
    <LI>Custom Implementation : No</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
  </UL>
\sa USBHAL_Label, CUSBHAL, IUSBHAL, PictBridge_Label
\author <a class="el" href="mailto:yuseong74@samsung.com">전유성</a>
\date 2006-11-29
*/

class CUSBHALInstance
{
private:
	//! CUSBHAL Class Instance를 하나만 만들기 위한 Flag 변수.
	static int msUSBHALCreated;
	
private:
	CUSBHALInstance();
	~CUSBHALInstance();

public:
	//! CUSBHAL Class Instance 생성.
	static IUSBHAL* createUSBHAL(void);

	//! CUSBHAL Class Instance 파괴.
	static void destroyUSBHAL(IUSBHAL* ifUSBHAL);
};

#endif  /* __CUSBHAL_INSTANCE_H__ */

