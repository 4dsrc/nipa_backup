#ifndef _CUSBDEVICE_H_
#define _CUSBDEVICE_H_

//c
#include <Dbt.h>

#define DEVICE_NOTIFY_DEVICEARRIVAL			0x0001
#define DEVICE_NOTIFY_DEVICEREMOVECOMPLETE	0x0002
#define DEVICE_NOTIFY_DEVICEDESC			0x0003
#define DEVICE_NOTIFY_LOCATION_INFORMATION	0x0004
#define DEVICE_NOTIFY_CLASSGUID				0x0005

typedef int (*NOTIFYDEVICECHANGE)(int ,void* ,void* );

class CUSBDevice
{
public:
	static CUSBDevice* GetInstance();
	~CUSBDevice();

private:
	CUSBDevice();

public:
	//! Device 상태 변화를 감지할 HWND를 등록한다.
	BOOL RegisterHandleForNotify(HWND hNotification);
	//! Device의 상태 변화를 알려줄 Callback function을 등록한다.
	BOOL SetNotifyCallback(NOTIFYDEVICECHANGE pNotifyDeviceChange);
	//! Device의 상태 변화를 분석한다.
	BOOL NotifyDeviceChange(WPARAM wChangeType, LPARAM lDeviceHeader);
	//! 현재 접속된 Device의 PATH정보를 반환한다.
	void GetUSBDeviceInformation(TCHAR* szDevicePath);

private:
	//! 상태가 변경된 Device의 정보
	void UpdateUSBDeviceInformation(PDEV_BROADCAST_DEVICEINTERFACE pDeviceInfomation, WPARAM wChangeType);

private:
	TCHAR m_szDevicePath[MAX_PATH];
	NOTIFYDEVICECHANGE m_pNotifyDeviceChange;

public:
	
};

#endif