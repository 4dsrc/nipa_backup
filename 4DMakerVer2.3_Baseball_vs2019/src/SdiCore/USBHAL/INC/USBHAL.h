/*
 * Project Name: Gene
 *  
 * Copyright 2006 by Samsung Electronics, Inc.
 * All rights reserved.
 * 
 * Project Description:
 * 'Gene' is the name of common software platform for CE  product such as
 * optical storage product (BD player, BD recorder, HD DVD player, etc.),
 * personal product (MiniKet, PMP, etc.), and TV product (digital TV, STB, etc.).
 * The Gene has at least one software component. And implementation target of
 * said software component will be CE product.
 * The Gene project's main outputs are as following.
 *     1. Reference architecture for CE product
 *     2. Software component which can be reused with at least one product line
 *     3. Software development kit (SDK), and adaptation development kit (ADK)
 *     4. Tool and infra to help easily reuse said 1~3 outputs.
 */
/*!
 \file CUSBHAL.h
 \brief Define CUSBHAL Class.
 \author <a class="el" href="mailto:yuseong74@samsung.com">JEON, YU-SEONG</a>
		 <br><b>Dept:</b> DM R&D Center, SE Lab
 \date 2006-11-03
 \version 0.50 
*/

#ifndef __CUSBHAL_H__
#define __CUSBHAL_H__

//#include "stdafx.h"
#include <IUSBHAL.h>
//#include <stdafx.h>

// CUSBHAL Class에서 Device 정보를 저장하기 위한 구조체.
typedef struct _usbhal_device_info
{
	TCHAR Name[USBHAL_DEV_NAME_LEN];	/* USB Device Driver Path */
	int Mode;							/* USB Device Access Mode */
	HANDLE FdNum;							/* USB Device의 HANDLE */
#if 1       // duvallee 07-22-2010 : LibUSB Porting
            // duvallee 07-29-2010 : Windows7에서는 LibUSB를 사용
    DWORD   dwVendorID;
    DWORD   dwProductID;
#endif
	
	int ConnectStatus;					/* Status of Connection */

	int MaxSizeBulk;					/* Maximun Bulk Pipe Data Size */
	int MaxSizeIntr;					/* Maximun Interrupt Pipe Data Size */

} USBHALDeviceInfoType;


typedef struct _usbhal_device_endpoint
{
    unsigned char epIn;
    unsigned short epIn_maxpacket;
    unsigned char epOut;
    unsigned short epOut_maxpacket;
    unsigned char epInt;
    unsigned short epInt_maxpacket;
} USBHALDeviceEndPoint;



/*!
\ingroup USBHAL_Label
\brief USB HAL의 Interface를 실제 구현한 Class.
\par Features :
  -# Open/Close.
  -# Read/Write.
  -# 버퍼 Flush.
  -# Check connection status.
  -# Check Null Packet need or not.
  -# Write Null Packet on Bulk and Interrupt.
\par Needed Sequence :
  CUSBHALInstance Class의 createUSBHAL() Operation을 통하여 생성하여야 한다.
\par Extension :
  -# 실제 멤버 함수들은 Target Device에 맞게 구현되어야 한다.
  -# Mass Storage와 A/V Stream을 위한 USB HAL과 통합된 HAL을 추후 구현 예정.
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
  </UL>
\sa USBHAL_Label, IUSBHAL, CUSBHALInstance
\author <a class="el" href="mailto:yuseong74@samsung.com">전유성</a>
\date 2006-11-29
*/

#if 1   // duvallee 07-29-2010 : LibUSB에서 사용한다.
struct usb_dev_handle;
#endif

class CUSBHAL : public IUSBHAL
{

	
#if 1   // duvallee 07-29-2010 : Windows7의 OS Version 및 LibUSB Handle을 위한...
	public :
	static BOOL 				m_bOsVersionWin7;
		usb_dev_handle* 		m_pUSBDeviceHandle;
	
	static BOOL IsWindows7()	{ return CUSBHAL::m_bOsVersionWin7; };
#endif	
private:
	USBHALDeviceInfoType mDevInfo;
    USBHALDeviceEndPoint mEndPointInfo;
    int mUsbTimeOut;
	
public:
	CUSBHAL();
	~CUSBHAL();

private:
#if 1   // duvallee 07-29-2010 : CString to Hexa
    DWORD wtohexa(CString szValue);
#endif

	int openUSB(char* name, int mode);
	void closeUSB(void);
	
	int readBulk(char* buffer, unsigned long size);
/*	int readBulk_remain(char* buffer, unsigned long size); */

#if 0   // duvallee 07-29-2010 : for LibUSB & Windows7
    int readBulk_Ex(char* buffer, unsigned long size);
#endif

	int writeBulk(char* data, unsigned long size);
	int writeIntr(char* data, unsigned long size);

	int flushAllBulk(void);
	int flushTxBulk(void);
	int flushRxBulk(void);

	int checkConnection(void);

	int isNullPktNeed(int ep, unsigned long size);
	int writeBulkNullPkt(void);
	int writeIntrNullPkt(void);

public:
    void SetTransTimeout(int timeout) { mUsbTimeOut = timeout;};
    int  GetTransTimeout() { return mUsbTimeOut;};

private:
    void SetUsbEndPointInfo(struct usb_device *dev);
};

#endif /* __CUSBHAL_H__ */

/*!
\fn int CUSBHAL::openUSB(char* name, int mode)
\sa IUSBHAL::openUSB
*/


/*!
\fn void CUSBHAL::closeUSB(void)
\sa IUSBHAL::closeUSB
*/

/*!
\fn int CUSBHAL::readBulk(char* buffer, unsigned long size)
\sa IUSBHAL::readBulk
*/

/*!
\fn int CUSBHAL::writeBulk(char* data, unsigned long size)
\sa IUSBHAL::writeBulk
*/

/*!
\fn int CUSBHAL::flushAllBulk(void)
\sa IUSBHAL::flushAllBulk
*/


/*!
\fn int CUSBHAL::flushTxBulk(void)
\sa IUSBHAL::flushTxBulk
*/


/*!
\fn int CUSBHAL::flushRxBulk(void)
\sa IUSBHAL::flushRxBulk
*/


/*!
\fn int CUSBHAL::checkConnection(void)
\sa IUSBHAL::checkConnection
*/

/*!
\fn int CUSBHAL::isNullPktNeed(int ep, unsigned long size)
\sa IUSBHAL::isNullPktNeed
*/

/*!
\fn int CUSBHAL::writeBulkNullPkt(void)
\sa IUSBHAL::writeBulkNullPkt
*/

/*!
\fn int CUSBHAL::writeIntrNullPkt(void)
\sa IUSBHAL::writeIntrNullPkt
*/
