/*
 * Project Name: Gene
 *  
 * Copyright 2006 by Samsung Electronics, Inc.
 * All rights reserved.
 * 
 * Project Description:
 * 'Gene' is the name of common software platform for CE  product such as
 * optical storage product (BD player, BD recorder, HD DVD player, etc.),
 * personal product (MiniKet, PMP, etc.), and TV product (digital TV, STB, etc.).
 * The Gene has at least one software component. And implementation target of
 * said software component will be CE product.
 * The Gene project's main outputs are as following.
 *     1. Reference architecture for CE product
 *     2. Software component which can be reused with at least one product line
 *     3. Software development kit (SDK), and adaptation development kit (ADK)
 *     4. Tool and infra to help easily reuse said 1~3 outputs.
 */
/*!
 \file IUSBHAL.h
 \brief Define IUSBHAL Class that is Interface Class of USB HAL.
 \author <a class="el" href="mailto:yuseong74@samsung.com">JEON, YU-SEONG</a>
		 <br><b>Dept:</b> DM R&D Center, SE Lab
 \date 2006-11-03
 \version 0.50 
*/

/*!
\ingroup PictBridge_Label
\defgroup USBHAL_Label USBHAL Component
\brief USBHAL Component는 USB Device를 제어하기 위한 Component이다.
\par Features :
  -# USB Device Open.
  -# USB Device Close.
  -# USB Device Read.
  -# USB Device Write.
  -# Buffer Flush.
  -# Check connection status.
  -# Check Null Packet need or not.
  -# Write Null Packet on Bulk and Interrupt.
\par Needed Sequnence :
  CUSBHALInstance Class의 createUSBHAL() Operation을 통하여 USB HAL 사용을 위한 Interface를
  생성하여야 한다.
\par Extension :
  -# USB Device의 새로운 기능을 제어하기 위하여 새로운 Interface 함수가 추가 될 수 있다.
\par Constraint :
  -# 실제 Interface 함수의 구현은 Target Device에 맞게 구현되어야 한다.
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
  </UL>
\sa PTP_Label
\author <a class="el" href="mailto:yuseong74@samsung.com">전유성</a>
\date 2006-11-29
\version 0.50
*/

#ifndef __IUSBHAL_H__
#define __IUSBHAL_H__

//#include "OSLibConfig.h"
#include "USBHAL_Errors.h"

/*!
 \ingroup USBHAL_Label
 \note USB Device Name의 최대 길이
*/
#define USBHAL_DEV_NAME_LEN				255

/*!
 \ingroup USBHAL_Label
 \note USB Interrupt Endpoint Number
*/
#define USBHAL_EP_INTR					1

/*!
 \ingroup USBHAL_Label
 \note USB Bulk IN Endpoint Number
*/
#define USBHAL_EP_BULK_IN				2

/*!
 \ingroup USBHAL_Label
 \note USB Bulk OUT Endpoint Number
*/
#define USBHAL_EP_BULK_OUT				3

/*!
\interface IUSBHAL
\ingroup USBHAL_Label
\brief USB HAL의 기능을 사용하기 위한 Interface Class.
\par Features :
  -# Open/Close.
  -# Read/Write.
  -# 버퍼 Flush.
  -# Check connection status.
  -# Check Null Packet need or not.
  -# Write Null Packet on Bulk and Interrupt.
\par Extension :
  -# USB Device의 새로운 기능을 제어하기 위하여 새로운 Interface 함수가 추가 될 수 있다.
\par Constraint :
  -# 실제 Interface 함수의 구현은 Target Device에 맞게 구현되어야 한다.
\par Requirements :
  <UL>
    <LI>Custom Implementation : No</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
  </UL>
\sa USBHAL_Label, CUSBHAL, CUSBHALInstance
\author <a class="el" href="mailto:yuseong74@samsung.com">전유성</a>
\date 2006-11-29
*/

class IUSBHAL
{
public:
	virtual ~IUSBHAL(){};

	//! Open USB Device.
	virtual int openUSB(char* name, int mode) = 0;

	//! Close USB Device.
	virtual void closeUSB(void) = 0;

	//! Read USB Device.
	virtual int readBulk(char* buffer, unsigned long size) = 0;


	//! Write USB Device.
	virtual int writeBulk(char* data, unsigned long size) = 0;
	
	//! Write in Interrupt Endpoint.
	virtual int writeIntr(char* data, unsigned long size) = 0;

	//! Flush Tx/Rx Buffer.
	virtual int flushAllBulk(void) = 0;

	//! Flush Tx Buffer.
	virtual int flushTxBulk(void) = 0;

	//! Flush Rx Buffer.
	virtual int flushRxBulk(void) = 0;

	//! Check status of Connection and return current status of Connection.
	virtual int checkConnection(void) = 0;

	//! Check Null Packet need or not.
	virtual int isNullPktNeed(int ep, unsigned long size) = 0;

	//! Write Null Packet in Bulk endpoint.
	virtual int writeBulkNullPkt(void) = 0;

	//! Write Null Packet in Interrupt endpoint.
	virtual int writeIntrNullPkt(void) = 0;

    virtual void SetTransTimeout(int timeout) = 0;
    virtual int  GetTransTimeout() = 0;
};

#endif /* __IUSBHAL_H__ */

/*!
\fn virtual int IUSBHAL::openUSB(char* name, int mode) = 0
\par Basic Flow :
  -# Open 할 USB Device의 Name, Access Mode 저장.
  -# USB Device Open.
\param [in] name USB Device Name
     <BR><b>length:</b> 1 .. 255 Byte
\param [in] mode Access Mode
     <BR><b>value:</b> 0(Read/Write), 1(Read Only), 2(Write Only)
\retval USBHAL_ERR_NONE if no error.
\retval USBHAL_ERR_PARAM if name is NULL,
						 or if name length > 255 Byte.
\retval USBHAL_ERR_OPEN if can't open USB Device.
\par Example :
\code
	IUSBHAL* mpIfUSBHAL;

	...
	mpIfUSBHAL = CUSBHALInstance::createUSBHAL();
	...
	if (mpIfUSBHAL->openUSB(mUSBDevConfig.Name, 0) != USBHAL_ERR_NONE) //O_RDWR
	{
		return MassStorErrOpenUSB;
	}
\endcode
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
 </UL>
\sa CUSBHAL
*/


/*!
\fn virtual void IUSBHAL::closeUSB(void) = 0
\par Basic Flow :
  -# USB Device Close.
  -# 멤버 변수 초기화.
\par Example :
\code
	IUSBHAL* mpIfUSBHAL;

	...
	mpIfUSBHAL = CUSBHALInstance::createUSBHAL();
	...
	if (mpIfUSBHAL->openUSB(mUSBDevConfig.Name, 0) != USBHAL_ERR_NONE) //O_RDWR
	{
		return MassStorErrOpenUSB;
	}
	...
	mpIfUSBHAL->closeUSB();
\endcode
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
 </UL>
\sa CUSBHAL
*/

/*!
\fn virtual int IUSBHAL::readBulk(char* buffer, unsigned long size) = 0
\par Basic Flow :
  -# size만큼 읽어서 buffer에 저장.
\param [out] buffer 읽은 Data를 저장할 buffer Pointer
\param [in] size Data의 크기
\retval USBHAL_ERR_NONE if no error or size == 0.
\retval USBHAL_ERR_PARAM if buffer == NULL or size < 0.
\retval USBHAL_ERR_READ if 읽은 Data 크기 < 0.
\retval USBHAL_ERR_READDATA_NONE if 읽은 Data 크기가 0인 경우가 Timeout 개수 초과.
\par Example :
\code
	IUSBHAL* mpIfUSBHAL;

	...
	mpIfUSBHAL = CUSBHALInstance::createUSBHAL();
	...
	if (mpIfUSBHAL->openUSB(mUSBDevConfig.Name, 0) != USBHAL_ERR_NONE) //O_RDWR
	{
		return MassStorErrOpenUSB;
	}
	...
	ret = mpIfUSBHAL->readBulk((char *)pRdBuffer, tempLen);
	..
	mpIfUSBHAL->closeUSB();
\endcode
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
 </UL>
\sa CUSBHAL
*/


/*!
\fn virtual int IUSBHAL::writeBulk(char* data, unsigned long size) = 0
\par Basic Flow :
  -# size만큼 Data를 bulk endpoint에 쓴다.
\param [in] data Data를 저장하고 있는 buffer Pointer
\param [in] size Data의 크기
\retval USBHAL_ERR_NONE if no error or size == 0.
\retval USBHAL_ERR_PARAM if data == NULL or size < 0.
\retval USBHAL_ERR_WRITE if 쓴 Data 크기 < 0 and != -1.
\retval USBHAL_ERR_WRITEDATA_NONE if 쓴 Data 크기가 0인 경우가 Timeout 개수 초과.
\retval USBHAL_ERR_WRITE_BUSY if 쓴 Data 크기 == -1.
\par Example :
\code
	IUSBHAL* mpIfUSBHAL;

	...
	mpIfUSBHAL = CUSBHALInstance::createUSBHAL();
	...
	if (mpIfUSBHAL->openUSB(mUSBDevConfig.Name, 0) != USBHAL_ERR_NONE) //O_RDWR
	{
		return MassStorErrOpenUSB;
	}
	...
	ret = mpIfUSBHAL->readBulk((char *)pRdBuffer, tempLen);
	..
	ret = mpIfUSBHAL->writeBulk((char *)pWrBuffer, tempLen);
	...
	mpIfUSBHAL->closeUSB();
\endcode
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
 </UL>
\sa CUSBHAL
*/


/*!
\fn virtual int IUSBHAL::writeIntr(char* data, unsigned long size) = 0
\par Basic Flow :
  -# size만큼 Data를 interruppt endpoint에 쓴다.
\param [in] data Data를 저장하고 있는 buffer Pointer
\param [in] size Data의 크기
\retval USBHAL_ERR_NONE if no error or size == 0.
\retval USBHAL_ERR_PARAM if data == NULL or size < 0.
\retval USBHAL_ERR_WRITE if 쓴 Data 크기 < 0 or write error.
\par Example :
\code
	IUSBHAL* mpIfUSBHAL;

	...
	mpIfUSBHAL = CUSBHALInstance::createUSBHAL();
	...
	if (mpIfUSBHAL->openUSB(mUSBDevConfig.Name, 0) != USBHAL_ERR_NONE) //O_RDWR
	{
		return MassStorErrOpenUSB;
	}
	...
	ret = mpIfUSBHAL->readBulk((char *)pRdBuffer, tempLen);
	..
	ret = mpIfUSBHAL->writeBulk((char *)pWrBuffer, tempLen);
	...
	mpIfUSBHAL->closeUSB();
\endcode
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
 </UL>
\sa CUSBHAL
*/


/*!
\fn virtual int IUSBHAL::flushAllBulk(void) = 0
\par Basic Flow :
  -# 모든 Bulk endpoint의 buffer를 Flush 시킨다.
\retval USBHAL_ERR_NONE if no error.
\retval USBHAL_ERR_FLUSH if error.
\par Example :
\code
	IUSBHAL* mpIfUSBHAL;

	...
	mpIfUSBHAL = CUSBHALInstance::createUSBHAL();
	...
	if (mpIfUSBHAL->openUSB(mUSBDevConfig.Name, 0) != USBHAL_ERR_NONE) //O_RDWR
	{
		return MassStorErrOpenUSB;
	}
	...
	ret = mpIfUSBHAL->readBulk((char *)pRdBuffer, tempLen);
	..
	ret = mpIfUSBHAL->writeBulk((char *)pWrBuffer, tempLen);
	...
	ret = mpIfUSBHAL->flushAllBulk();
	...
	mpIfUSBHAL->closeUSB();
\endcode
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
 </UL>
\sa CUSBHAL
*/


/*!
\fn virtual int IUSBHAL::flushTxBulk(void) = 0
\par Basic Flow :
  -# Transmit용 Bulk endpoint의 buffer를 Flush 시킨다.
\retval USBHAL_ERR_NONE if no error.
\retval USBHAL_ERR_FLUSH if error.
\par Example :
\code
	IUSBHAL* mpIfUSBHAL;

	...
	mpIfUSBHAL = CUSBHALInstance::createUSBHAL();
	...
	if (mpIfUSBHAL->openUSB(mUSBDevConfig.Name, 0) != USBHAL_ERR_NONE) //O_RDWR
	{
		return MassStorErrOpenUSB;
	}
	...
	ret = mpIfUSBHAL->readBulk((char *)pRdBuffer, tempLen);
	..
	ret = mpIfUSBHAL->writeBulk((char *)pWrBuffer, tempLen);
	...
	ret = mpIfUSBHAL->flushAllBulk();
	...
	ret = mpIfUSBHAL->flushTxBulk();
	...
	mpIfUSBHAL->closeUSB();
\endcode
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
 </UL>
\sa CUSBHAL
*/


/*!
\fn virtual int IUSBHAL::flushRxBulk(void) = 0
\par Basic Flow :
  -# Receive용 Bulk endpoint의 buffer를 Flush 시킨다.
\retval USBHAL_ERR_NONE if no error.
\retval USBHAL_ERR_FLUSH if error.
\par Example :
\code
	IUSBHAL* mpIfUSBHAL;

	...
	mpIfUSBHAL = CUSBHALInstance::createUSBHAL();
	...
	if (mpIfUSBHAL->openUSB(mUSBDevConfig.Name, 0) != USBHAL_ERR_NONE) //O_RDWR
	{
		return MassStorErrOpenUSB;
	}
	...
	ret = mpIfUSBHAL->readBulk((char *)pRdBuffer, tempLen);
	..
	ret = mpIfUSBHAL->writeBulk((char *)pWrBuffer, tempLen);
	...
	ret = mpIfUSBHAL->flushAllBulk();
	...
	ret = mpIfUSBHAL->flushTxBulk();
	...
	ret = mpIfUSBHAL->flushRxBulk();
	...
	mpIfUSBHAL->closeUSB();
\endcode
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
 </UL>
\sa CUSBHAL
*/


/*!
\fn virtual int IUSBHAL::checkConnection(void) = 0
\par Basic Flow :
  -# USB Device의 Connection 여부를 체크하고 한다.
\retval 1 if USB is connected.
\retval 0 if USB is disconnected.
\par Example :
\code
	IUSBHAL* mpIfUSBHAL;
	int bIsConnected = 0;

	...
	mpIfUSBHAL = CUSBHALInstance::createUSBHAL();
	...
	if (mpIfUSBHAL->openUSB(mUSBDevConfig.Name, 0) != USBHAL_ERR_NONE) //O_RDWR
	{
		return MassStorErrOpenUSB;
	}
	...
	ret = mpIfUSBHAL->readBulk((char *)pRdBuffer, tempLen);
	..
	ret = mpIfUSBHAL->writeBulk((char *)pWrBuffer, tempLen);
	...
	bIsConnected = mpIfUSBHAL->checkConnection();
	...
	mpIfUSBHAL->closeUSB();
\endcode
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
 </UL>
\sa CUSBHAL
*/


/*!
\fn virtual int IUSBHAL::isNullPktNeed(int ep, unsigned long size) = 0
\par Basic Flow :
  -# size가 ep 번호를 가진 USB endpoint의 buffer size의 배수인지를 체크한다.
  -# 배수이면 NULL Packet이 필요하다.
\param [in] ep USB Endpoint Number
\param [in] size Endpoint buffer size와 비교할 size.
\retval 1 if need NULL Packet.
\retval 0 if don't need NULL Packet.
\par Example :
\code
	IUSBHAL* mpIfUSBHAL;
	int bIsConnected = 0;

	...
	mpIfUSBHAL = CUSBHALInstance::createUSBHAL();
	...
	if (mpIfUSBHAL->openUSB(mUSBDevConfig.Name, 0) != USBHAL_ERR_NONE) //O_RDWR
	{
		return MassStorErrOpenUSB;
	}
	..
	ret = mpIfUSBHAL->writeBulk((char *)pWrBuffer, tempLen);
	...
	if (mpIfUSBHAL->isNullPktNeed())
	{
		...
		mpIfUSBHAL->writeBulkNullPkt();
		...
	}
	...
	
	mpIfUSBHAL->closeUSB();
\endcode
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
 </UL>
\sa CUSBHAL
*/


/*!
\fn virtual int IUSBHAL::writeBulkNullPkt(void) = 0
\par Basic Flow :
  -# size가 ep 번호를 가진 Bulk endpoint의 buffer size의 배수인지를 체크한다.
  -# 배수이면 NULL Packet이 필요하다.
\param [in] ep Bulk Endpoint Number
\param [in] size Endpoint buffer size와 비교할 size.
\retval 1 if need NULL Packet.
\retval 0 if don't need NULL Packet.
\par Example :
\code
	IUSBHAL* mpIfUSBHAL;
	int bIsConnected = 0;

	...
	mpIfUSBHAL = CUSBHALInstance::createUSBHAL();
	...
	if (mpIfUSBHAL->openUSB(mUSBDevConfig.Name, 0) != USBHAL_ERR_NONE) //O_RDWR
	{
		return MassStorErrOpenUSB;
	}
	..
	ret = mpIfUSBHAL->writeBulk((char *)pWrBuffer, tempLen);
	...
	if (mpIfUSBHAL->isNullPktNeed(1, 512))
	{
		...
		mpIfUSBHAL->writeBulkNullPkt();
		...
	}
	...
	
	mpIfUSBHAL->closeUSB();
\endcode
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
 </UL>
\sa CUSBHAL
*/


/*!
\fn virtual int IUSBHAL::writeIntrNullPkt(void) = 0
\par Basic Flow :
  -# size가 ep 번호를 가진 Interrupt endpoint의 buffer size의 배수인지를 체크한다.
  -# 배수이면 NULL Packet이 필요하다.
\param [in] ep Interrupt Endpoint Number
\param [in] size Endpoint buffer size와 비교할 size.
\retval 1 if need NULL Packet.
\retval 0 if don't need NULL Packet.
\par Example :
\code
	IUSBHAL* mpIfUSBHAL;
	int bIsConnected = 0;

	...
	mpIfUSBHAL = CUSBHALInstance::createUSBHAL();
	...
	if (mpIfUSBHAL->openUSB(mUSBDevConfig.Name, 0) != USBHAL_ERR_NONE) //O_RDWR
	{
		return MassStorErrOpenUSB;
	}
	..
	ret = mpIfUSBHAL->writeBulk((char *)pWrBuffer, tempLen);
	...
	if (mpIfUSBHAL->isNullPktNeed(3, 36))
	{
		...
		mpIfUSBHAL->writeIntrNullPkt();
		...
	}
	...
	
	mpIfUSBHAL->closeUSB();
\endcode
\par Requirements :
  <UL>
    <LI>Custom Implementation : Yes</LI>
    <LI>Minimum operating systems : uCLinux 2.4.x, Linux 2.4.x</LI>
 </UL>
\sa CUSBHAL
*/