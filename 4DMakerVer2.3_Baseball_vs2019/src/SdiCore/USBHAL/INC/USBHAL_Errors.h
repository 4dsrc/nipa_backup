
/*
 * Project Name: Gene
 *  
 * Copyright 2006 by Samsung Electronics, Inc.
 * All rights reserved.
 * 
 * Project Description:
 * 'Gene' is the name of common software platform for CE  product such as
 * optical storage product (BD player, BD recorder, HD DVD player, etc.),
 * personal product (MiniKet, PMP, etc.), and TV product (digital TV, STB, etc.).
 * The Gene has at least one software component. And implementation target of
 * said software component will be CE product.
 * The Gene project's main outputs are as following.
 *     1. Reference architecture for CE product
 *     2. Software component which can be reused with at least one product line
 *     3. Software development kit (SDK), and adaptation development kit (ADK)
 *     4. Tool and infra to help easily reuse said 1~3 outputs.
 */

/*!
 \file USBHAL_Errors.h
 \brief USBHAL Class의 Error Code를 선언한 헤더 파일.
 \author <a class="el" href="mailto:yuseong74@samsung.com">전유성</a>
         <br><b>Dept:</b> DM 연구소, SE Lab
 \date 2006-09-07
 \version 
*/

#ifndef __USBHAL_ERRORS_H__
#define __USBHAL_ERRORS_H__

#define USBHAL_ERR_NONE				0x0000

#define USBHAL_ERR_PARAM			0xFFFF
#define USBHAL_ERR_OPEN				0xFFFE
#define USBHAL_ERR_READ				0xFFFD
#define USBHAL_ERR_READDATA_NONE	0xFFFC
#define USBHAL_ERR_WRITE			0xFFFB
#define USBHAL_ERR_WRITEDATA_NONE	0xFFFA
#define USBHAL_ERR_WRITE_BUSY		0xFFF9
#define USBHAL_ERR_STALL			0xFFF8
#define USBHAL_ERR_FLUSH			0xFFF7
#define USBHAL_ERR_CHK_RESET		0xFFF6
#define USBHAL_ERR_SELECT_DESC		0xFFF5
#define USBHAL_ERR_SET_CONNECT		0xFFF4
#define USBHAL_ERR_SET_DISCONNECT	0xFFF3
#define USBHAL_ERR_NULL_PACKET		0xFFF2
#define USBHAL_ERR_CHK_CONNECT		0xFFF1

#endif /* __USBHAL_ERRORS_H__ */


/*! 
\page USBHAL_Errors

<h1>USB HAL의 Error Codes</h1>

\section a SUCCESS
- 0x0000 : USBHAL_ERR_NONE - No Error.
\section b FAIL
- 0xFFFF : USBHAL_ERR_PARAM - Paramter is invalid.
- 0xFFFE : USBHAL_ERR_OPEN - The USB HAL can't open USB Device.
- 0xFFFD : USBHAL_ERR_READ - The USB HAL can't read.
- 0xFFFC : USBHAL_ERR_READDATA_NONE - The USB HAL read none of data.
- 0xFFFB : USBHAL_ERR_WRITE - The USB HAL can't write.
- 0xFFFA : USBHAL_ERR_WRITEDATA_NONE - The USB HAL write none of data.
- 0xFFF9 : USBHAL_ERR_WRITE_BUSY - The USB HAL busy in writing data.
- 0xFFF8 : USBHAL_ERR_STALL - The USB HAL can't stall endpoint.
- 0xFFF7 : USBHAL_ERR_FLUSH - The USB HAL can't flush endpoint.
- 0xFFF6 : USBHAL_ERR_CHK_RESET - The USB HAL can't check whether USB Device is reseted or not.
- 0xFFF5 : USBHAL_ERR_SELECT_DESC - The USB HAL can't select USB descriptor.
- 0xFFF4 : USBHAL_ERR_SET_CONNECT - The USB HAL can't set connect.
- 0xFFF3 : USBHAL_ERR_SET_DISCONNECT - The USB HAL can't set disconnect.
- 0xFFF2 : USBHAL_ERR_NULL_PACKET - The USB HAL can't write null packet.
- 0xFFF1 : USBHAL_ERR_CHK_CONNECT - The USB HAL can't check status of connection.
*/


