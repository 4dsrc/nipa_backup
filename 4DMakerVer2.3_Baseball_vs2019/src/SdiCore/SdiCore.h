/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        SdiCoreWin7.h
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */

#pragma once

#include "stdafx.h"
#include "SdiDefines.h"
#include "DeviceInfoMgr.h"
#define	PTPErrNone		0

class AFX_EXT_CLASS CSdiCore
{

public:
	CSdiCore(void)	{};	
	~CSdiCore(void)	{};
	CDeviceInfoMgr* GetDeviceInfo() { return &m_DeviceInfoMgr; }

	//-- 2011-7-12 jeansu : SDK 용. 용도는 GetDevPropDesc() 이후에 실행해서 사용자가 DeviceInfo 값을 직접 핸들하도록 제공한다.
	int SdiGetDeviceInfo(WORD wPropCode, IDeviceInfo **pDeviceInfo)
	{
		*pDeviceInfo = (IDeviceInfo *) m_DeviceInfoMgr.GetDeviceInfo(wPropCode);
		if(*pDeviceInfo == NULL)
			return SDI_ERR_INVALID_PARAMETER;
		
		return SDI_ERR_OK;
	}
public:
	CDeviceInfoMgr		m_DeviceInfoMgr;
	int					m_nModel;

public:
	BOOL				m_bStop;
	
	virtual void RemoveThread() {};
	virtual SdiResult SdiInitSession(SdiChar* strDevName)=0;
	virtual	SdiResult SdiExOpenSession()=0;
	virtual	SdiResult SdiOpenSession()=0;
	virtual	SdiResult SdiCloseSession()=0;

	virtual	SdiResult SdiSendAdjCapture(SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size)=0;
	virtual SdiResult SdiSendAdjCapture(CString strSaveName)=0;      
	virtual	SdiResult SdiSendAdjLiveview(SdiUChar* pYUV)=0;
	virtual	SdiResult SdiSendAdjLiveviewInfo(RSLiveviewInfo* pLiveviewInfo)=0;
	virtual SdiResult SdiSendShutterCapture(SdiInt8 nShutter)=0;
	virtual SdiResult SdiSendAdjCompleteCapture(CString, bool& , int nFileNum, int nClose)=0;
	//NX3000 20150119 ImageTransfer
	virtual SdiResult SdiSendAdjCompleteCapture(CString, bool&)=0;
	//virtual SdiResult SdiSendAdjCompleteMovie(CString strSaveName, int nWaitSaveDSC)=0;
	virtual SdiResult SdiSendAdjCompleteMovie(CString, int nWaitSaveDSC, int nFilenNum, int nClose)=0;
	virtual SdiResult SdiSendAdjMovieTransfer(CString strSaveName, int nfilenum, int nClose) = 0;
	virtual	SdiResult SdiGetDevPropDesc(WORD wPropCode, WORD wAllFlag = 1)=0;		
	virtual	SdiResult SdiSetDevPropValue(SdiInt nPTPCode, SdiInt nType, LPARAM lpValue)=0;

	virtual SdiResult SdiSetDevUniqueID(SdiInt nPTPCode,  CString strUUID)=0;
	virtual SdiResult SdiGetDevUniqueID(SdiInt nPTPCode,  SdiChar **chUniqueID)=0;
	virtual SdiResult SdiGetDevModeIndex(SdiInt nPTPCode, SdiInt &nModeIndex)=0;
	virtual SdiResult SdiSetFocusPosition(FocusPosition *ptFocusPos, SdiInt8 nType)=0;
	virtual SdiResult SdiSetFocusPosition(SdiInt32 nValue)=0;
	virtual SdiResult SdiGetFocusPosition(FocusPosition* ptFocusPos)=0;
	
	

	//-- 2011-8-23 Lee JungTaek
	virtual SdiResult SdiGetPTPDeviceInfo(PTPDeviceInfo * ptDevInfoType)=0;
	virtual SdiResult SdiCheckEvent(SdiInt32& nEventExist, SdiUIntArray* pArList=NULL)=0; 
	virtual SdiResult SdiGetEvent(BYTE **pData,SdiInt32 *size)=0;

	//-- 2011-7-18 Lee JungTaek
	virtual SdiResult SdiSendAdjRequireCaptureOnce(int nModel = 0)=0;
	//-- Send Command
	virtual	SdiResult SdiFormatDevice()=0;
	virtual	SdiResult SdiResetDevice(int nPram = 5)=0;
	//dh0.seo 2014-11-04
	virtual	SdiResult SdiSensorCleaningDevice()=0;
	//CMiLRe 20141113 IntervalCapture Stop 추가
	virtual	SdiResult SdiIntervalCaptureStop()=0;
	//CMiLRe 20141127 Display Save Mode 에서 Wake up
	virtual	SdiResult SdiDisplaySaveModeWakeUp()=0;	
	//CMiLRe NX2000 FirmWare Update CMD 추가
	virtual	SdiResult SdiFWUpdate()=0;
	//20150128 CMiLRe NX3000
	//CMiLRe 2015129 펌웨어 다운로드 경로 추가
	virtual	SdiResult SdiFWDownload(CString strPath)=0;
	
	virtual SdiResult SdiInitGH5()=0;

	//-- 2013-10-22 yongmin
	virtual SdiResult SdiGetTick(SdiInt& pcTime, SdiDouble& deviceTime, SdiInt& nCheckCnt, SdiInt& nCheckTime, SdiInt nDSCSynTime, HWND handle)=0; 
	virtual	SdiResult SdiSetTick(SdiDouble nTick, SdiInt nIntervalSensorOn, SdiUIntArray* pArList=NULL)=0;
	virtual	SdiResult SdiHiddenCommand(SdiInt32 nCommand, SdiInt32 nValue)=0;

	virtual SdiResult SdiFocusSave()=0;

	//-- 2014-07-14 joonho.kim
	virtual SdiResult SdiSetPause(int nPause) =0;
	virtual SdiResult SdiSetResume(int nResume) =0;
	virtual SdiResult SdiTrackingAFStop()=0;
	virtual SdiResult SdiControlTouchAF(int nX, int nY)=0;
	virtual	SdiResult SdiMovieCancle()=0;
	virtual SdiResult SdiGetRecordStatus()=0;
	virtual SdiResult SdiGetCaptureCount()=0;
	virtual SdiResult SdiGetImagePath(CString& strPath)=0;
	virtual SdiResult SdiFileTransfer(char* pPath, int nfilenum, int nmdat, int nOffset,int* nFileSize, int nMovieSize)=0;
	virtual SdiResult SdiImageTransfer(CString strName,int nfilenum, int nClose)=0;
	virtual SdiResult SdiImageTransferEX(CString strName,int nfilenum, int nClose)=0;
	virtual SdiResult SdiSetEnLarge(int nLarge)=0;
	virtual SdiResult SdiSetRecordPause(int nLarge)=0;
	virtual SdiResult SdiSetRecordResume(int nLarge)=0;
	virtual SdiResult SdiSetLiveView(int nCommand)=0;
	virtual SdiResult SdiFileReceiveComplete()=0;
	virtual SdiResult SdiImageReceiveComplete()=0;
	virtual SdiResult SdiLiveReceiveComplete()=0;
	virtual	SdiBool SdiFileMgrSetStartTime(int nStartTime, int nIntervalSensorOn)=0;
	virtual SdiResult SdiFileMgrGetEndTime()=0;
	virtual	SdiBool SdiSetResumeFrame(int nResumeFrame)=0;
	virtual SdiResult SdiGetResumeFrame()=0;
	virtual	SdiBool SdiSetResumeMode(BOOL bResumeMode)=0;
	virtual	SdiBool SdiGetResumeMode()=0;
	virtual	void SdiSyncStop() = 0;
	virtual	void SetFps(int nFps) = 0;
	virtual SdiResult SdiHalfShutter(int nRelease)=0;
	virtual SdiResult SdiSetFocusFrame(int nX, int nY, int nMagnification)=0;
	virtual SdiResult SdiGetFocusFrame(BYTE **pData,SdiInt32 *size)=0;

	//-- 2013-12-01 hongsu@esmlab.com
	//-- Check Connection
	virtual	SdiBool SdiCheckGetFile() {return FALSE;}

	//NX3000
	virtual	SdiResult SdiDeviceReboot()=0;
	virtual	SdiResult SdiDevicePowerOff()=0;

	virtual SdiResult SdiDeviceExportLog()=0;

};
