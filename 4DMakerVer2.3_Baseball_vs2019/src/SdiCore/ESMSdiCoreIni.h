////////////////////////////////////////////////////////////////////////////////
//
//	ESMIni.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

typedef BOOL (CALLBACK *SUBSTRPROC)(LPCTSTR, LPVOID);

class CESMSdiCoreIni : public CObject  
{
public:
   CESMSdiCoreIni();
   CESMSdiCoreIni( LPCTSTR ESMIniFilename );
   virtual ~CESMSdiCoreIni();

// Methods
public:
	// Sets the current ESMIni-file to use.
	BOOL SetIniFilename(LPCTSTR strFile, BOOL bReset = FALSE);
	//
	// Reads an integer from the ini-file.
	UINT GetInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault=0);
	// Reads a boolean value from the ini-file.
	BOOL GetBoolean(LPCTSTR lpszSection, LPCTSTR lpszEntry, BOOL bDefault=FALSE);
	// Reads a string from the ini-file.
	CString GetString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszDefault=_T(""));
	//

	//BY_0625
	UINT GetInt3(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault=0);
	BOOL WriteInt3(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nValue);

	// Writes an integer to the ini-file.
	BOOL WriteInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nValue);
	BOOL WriteIntAdd(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nValue);

	// Writes a boolean value to the ini-file.
	BOOL WriteBoolean(LPCTSTR lpszSection, LPCTSTR lpszEntry, BOOL bValue);

	//-- 2009-03-31
	//-- For NX Project
	BOOL WriteTrueFalse(LPCTSTR lpszSection, LPCTSTR lpszEntry, BOOL bValue);

	// Writes a string to the ini-file.
	BOOL WriteString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszValue);
   
	// Removes an item from the current ini-file.
	BOOL DeleteKey(LPCTSTR lpszSection, LPCTSTR lpszEntry);
	// Removes a complete section from the ini-file.
	BOOL DeleteSection(LPCTSTR lpszSection);

	//-- 12.04.26 yongmin 
	CString GetStringSection(LPCTSTR lpszSection);
	CString GetStringValuetoKey(LPCTSTR lpszSection, LPCTSTR lpszEntry);
	//CMiLRe 20150202 NX3000 Shutter Speed Enum No. ��������
	int GetStringValuetoKeyIndex(LPCTSTR lpszSection, LPCTSTR lpszEntry);
	CString GetNextStringValue(LPCTSTR lpszSection,LPCTSTR lpszEntry);
	CString GetBeforeStringValue(LPCTSTR lpszSection,LPCTSTR lpszEntry);
	//-- 2012-04-27 hongsu
	int GetSectionCount(LPCTSTR lpszSection);
	void GetSectionList(LPCTSTR lpszSection, CStringArray& arList);
	//-- 12.07.13 cy.gil 
	void GetSectionLineList(LPCTSTR lpszSection, CStringArray& arList);

	//-- 2013-04-27 hongsu@esmlab.com
	//-- Get all section names from an ini file
	void GetSectionNames(CStringArray* pArray) const;
	DWORD GetSectionNames(LPTSTR lpBuffer, DWORD dwBufSize) const;
	static BOOL CALLBACK __SubStrAdd(LPCTSTR lpString, LPVOID lpParam);
	static BOOL ParseDNTString(LPCTSTR lpString, SUBSTRPROC lpFnStrProc, LPVOID lpParam = NULL);
// Variables
protected:
   CString m_strIniFilename; // The current ini-file used.
};
