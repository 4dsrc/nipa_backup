﻿/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        SdiCoreWin7.cpp
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */


#include "StdAfx.h"
#include "SdiCoreWin7.h"
#include <wpdmtpextensions.h>
#include <process.h>
#include "PTP_Base.h"

//  [10/14/2013 Administrator]
//  For Tracing
#include <MMSystem.h>
#pragma comment(lib, "winmm")

#define MAX_LOOP 500
#define MAX_LOOP_TEST 5000

#define CLIENT_NAME         L"WPD Sample Application"
#define CLIENT_MAJOR_VER    1
#define CLIENT_MINOR_VER    0
#define CLIENT_REVISION     2

//-- 2011-06-27
#define FOCUS_DETAIL_UNIT		100
#define FOCUS_ROUGH_UNIT		10

#define DEFAULT_WAIT_SAVEDSC		2500
#define DEFAULT_WAIT_SAVEPC			2500

//-- 2013-02-04 hongsu.jung
const WORD PTP_PROP_BASE = 0x00; 
static int g_MDAT = 0;
CRITICAL_SECTION* CSdiCoreWin7::m_CriSectionLog = NULL;
CRITICAL_SECTION* CSdiCoreWin7::m_CriSection = NULL;
//-- 2011-06-14
//-- time measure
#include <sys/timeb.h>
#include <time.h>

CAtlStringW g_strEventRegistrationCookie;

// IPortableDeviceEventCallback implementation for use with
// device events.

void DisplayStringProperty(
    IPortableDeviceValues*  pProperties,
    REFPROPERTYKEY          key,
    PCWSTR                  pszKey)
{
    PWSTR   pszValue = NULL;
    HRESULT hr = pProperties->GetStringValue(key,&pszValue);
    if (SUCCEEDED(hr))
    {
        // Get the length of the string value so we
        // can output <empty string value> if one
        // is encountered.
        CAtlStringW strValue;
        strValue = pszValue;
        if (strValue.GetLength() > 0)
        {
            TRACE("%ws: %ws\n",pszKey, pszValue);
        }
        else
        {
            TRACE("%ws: <empty string value>\n", pszKey);
        }
    }
    else
    {
        TRACE("%ws: <Not Found>\n", pszKey);
    }

    // Free the allocated string returned from the
    // GetStringValue method
    CoTaskMemFree(pszValue);
    pszValue = NULL;
}

// Displays a property assumed to be in GUID form.
void DisplayGuidProperty(
    IPortableDeviceValues*  pProperties,
    REFPROPERTYKEY          key,
    PCWSTR                  pszKey)
{
    GUID    guidValue = GUID_NULL;
    HRESULT hr = pProperties->GetGuidValue(key,&guidValue);
    if (SUCCEEDED(hr))
    {
        TRACE("%ws: %ws\n",pszKey, (PWSTR)CGuidToString(guidValue));
    }
    else
    {
        TRACE("%ws: <Not Found>\n", pszKey);
    }
}


class CPortableDeviceEventsCallback : public IPortableDeviceEventCallback
{
public:
    CPortableDeviceEventsCallback() : m_cRef(1)
    {
    }

    ~CPortableDeviceEventsCallback()
    {
    }

	HWND m_hRecvWnd;
	int  m_Index;

    HRESULT __stdcall QueryInterface(
        REFIID  riid,
        LPVOID* ppvObj)
    {
        HRESULT hr = S_OK;
        if (ppvObj == NULL)
        {
            hr = E_INVALIDARG;
            return hr;
        }

        if ((riid == IID_IUnknown) ||
            (riid == IID_IPortableDeviceEventCallback))
        {
            AddRef();
            *ppvObj = this;
        }
        else
        {
            *ppvObj = NULL;
            hr = E_NOINTERFACE;
        }
        return hr;
    }

    ULONG __stdcall AddRef()
    {
        InterlockedIncrement((long*) &m_cRef);
        return m_cRef;
    }

    ULONG __stdcall Release()
    {
        ULONG ulRefCount = m_cRef - 1;

        if (InterlockedDecrement((long*) &m_cRef) == 0)
        {
            delete this;
            return 0;
        }
        return ulRefCount;
    }

    HRESULT __stdcall OnEvent(
        IPortableDeviceValues* pEventParameters)
    {
		HRESULT hr = S_OK;
		return hr;
    }

    ULONG m_cRef;
};


CSdiCoreWin7::CSdiCoreWin7(void)
{
	Init();	
	//-- 2013-11-27 hongsu.jung
	m_nModel = SDI_MODEL_UNKNOWN;
}

CSdiCoreWin7::CSdiCoreWin7(int nModel, LARGE_INTEGER swFreq, LARGE_INTEGER swStart)
{
	Init();
	//-- 2013-11-27 hongsu.jung
	m_swFreq = swFreq;
	m_swStart= swStart;	
	m_nModel = nModel;
}

void CSdiCoreWin7::Init()
{
	(void)HeapSetInformation(NULL, HeapEnableTerminationOnCorruption, NULL, 0);
	// Initialize COM for COINIT_MULTITHREADED
	HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
	//-- 2013-03-08 hongsu.jung
	m_hRecvWnd = NULL;
	m_pIPortableDevice = NULL;
	//-- 2013-12-01 hongsu@esmlab.com
	m_bStop = FALSE;
	m_bSyncStop = FALSE;
	//-- CreateFileMgr
	CreateFileManager();

	/*if (hr == S_OK)
    {
        // Place code to connect to the device here
        // IPortableDevice::Open(...)

        // Create an instance of our callback using the new operator
        CPortableDeviceEventsCallback* pCallback = new CPortableDeviceEventsCallback;

        if (pCallback == NULL)
        {
            hr = E_OUTOFMEMORY;
        }

        // Setup our callback class to register for events
        if (hr == S_OK)
        {
            hr = pCallback->Register(spDevice);
        }

        // Block so that our event callback will get a chance to execute
        if (hr == S_OK)
        {
            printf("Press any key to stop listening to events\n");
            _getch();
        }

        // Unregister for event notifications
        if (hr == S_OK)
        {
            hr = pCallback->UnRegister(spDevice);
        }

        // Call Release on our callback object, and /not/ delete
        if (pCallback != NULL)
        {
            pCallback->Release();
        }    

        // Close device connection
        if (bDeviceOpened)
        {
            hr = spDevice->Close();
        }   
    }*/


	if(m_CriSection == NULL)
	{
		m_CriSection = new CRITICAL_SECTION;
		InitializeCriticalSection (m_CriSection);
	}
	if(m_CriSectionLog == NULL)
	{
		m_CriSectionLog = new CRITICAL_SECTION;
		InitializeCriticalSection (m_CriSectionLog);
	}

	CString strDate = _T("");
	CString strTime = _T("");
	CTime time = CTime::GetCurrentTime();
	//time = CTime::GetCurrentTime();
	int nYear = time.GetYear();
	int nMonth = time.GetMonth();
	int nDay = time.GetDay();

	strDate.Format(_T("%04d-%02d-%02d"), nYear, nMonth, nDay);
	strTime.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), nYear, nMonth, nDay, time.GetHour(), time.GetMinute(), time.GetSecond());
		
	m_strLog.Format(_T("%s\\%s\\%s_SdiCore.log"), _T("C:\\Program Files\\ESMLab\\4DMaker\\log"), strDate, strTime);
}

CSdiCoreWin7::~CSdiCoreWin7(void)
{
	if(m_CriSection != NULL)
	{
		DeleteCriticalSection (m_CriSection);	
		delete m_CriSection;
	}
	if(m_CriSectionLog != NULL)
	{
		DeleteCriticalSection (m_CriSectionLog);	
		delete m_CriSectionLog;
	}
	RemoveThread();
}

void CSdiCoreWin7::RemoveThread()
{
	if(m_pSdiFileMgr)
	{
		delete m_pSdiFileMgr;
		m_pSdiFileMgr = NULL;
	}	
}	

void CSdiCoreWin7::RegisterForEventNotifications(IPortableDevice* pDevice)
{
    HRESULT                        hr = S_OK;
    PWSTR                          pszEventCookie = NULL;
    CPortableDeviceEventsCallback* pCallback = NULL;

    if (pDevice == NULL)
    {
        return;
    }

    // Check to see if we already have an event registration cookie.  If so,
    // then avoid registering again.
    // NOTE: An application can register for events as many times as they want.
    //       This sample only keeps a single registration cookie around for
    //       simplicity.
    if (g_strEventRegistrationCookie.GetLength() > 0)
    {
        TRACE("This application has already registered to receive device events.\n");
        return;
    }

    // Create an instance of the callback object.  This will be called when events
    // are received.
    if (hr == S_OK)
    {
        pCallback = new (std::nothrow) CPortableDeviceEventsCallback();
        if (pCallback == NULL)
        {
            hr = E_OUTOFMEMORY;
            TRACE("Failed to allocate memory for IPortableDeviceEventsCallback object, hr = 0x%lx\n",hr);
        }
		//-- 2011-7-19 Lee JungTaek
		//-- CodeSonar
		else
			pCallback->m_hRecvWnd = m_hRecvWnd;
    }

    // Call Advise to register the callback and receive events.
    if (hr == S_OK)
    {
        hr = pDevice->Advise(0, pCallback, NULL, &pszEventCookie);
        if (FAILED(hr))
        {
            TRACE("! Failed to register for device events, hr = 0x%lx\n",hr);
        }
    }

    // Save the event registration cookie if event registration was successful.
    if (hr == S_OK)
    {
        g_strEventRegistrationCookie = pszEventCookie;
    }

    // Free the event registration cookie, if one was returned.
    if (pszEventCookie != NULL)
    {
        CoTaskMemFree(pszEventCookie);
        pszEventCookie = NULL;
    }

    if (hr == S_OK)
    {
        TRACE("This application has registered for device event notifications and was returned the registration cookie '%ws'", g_strEventRegistrationCookie.GetString());
    }

    // If a failure occurs, remember to delete the allocated callback object, if one exists.
    if (pCallback != NULL)
    {
        pCallback->Release();
    }
}

void CSdiCoreWin7::UnregisterForEventNotifications(IPortableDevice* pDevice)
{
    HRESULT hr = S_OK;

    if (pDevice == NULL)
    {
        return;
    }

    hr = pDevice->Unadvise(g_strEventRegistrationCookie);
    if (FAILED(hr))
    {
        TRACE("! Failed to unregister for device events using registration cookie '%ws', hr = 0x%lx\n",g_strEventRegistrationCookie.GetString(), hr);
    }

    if (hr == S_OK)
    {
        TRACE("This application used the registration cookie '%ws' to unregister from receiving device event notifications", g_strEventRegistrationCookie.GetString());
    }

    g_strEventRegistrationCookie = L"";
}

DWORD CSdiCoreWin7::EnumerateAllDevices()
{
	DWORD                           cPnPDeviceIDs = 0;
	PWSTR*                          pPnpDeviceIDs = NULL;
	CComPtr<IPortableDeviceManager> pPortableDeviceManager;

	// CoCreate the IPortableDeviceManager interface to enumerate
	// portable devices and to get information about them.
	//<SnippetDeviceEnum1>
	HRESULT hr = CoCreateInstance(CLSID_PortableDeviceManager,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_PPV_ARGS(&pPortableDeviceManager));
	if (FAILED(hr))
	{
		TRACE("! Failed to CoCreateInstance CLSID_PortableDeviceManager, hr = 0x%lx\n",hr);
	}
	//</SnippetDeviceEnum1>

	// First, pass NULL as the PWSTR array pointer to get the total number
	// of devices found on the system.
	//<SnippetDeviceEnum2>
	if (SUCCEEDED(hr))
	{
		hr = pPortableDeviceManager->GetDevices(NULL, &cPnPDeviceIDs);
		if (FAILED(hr))
		{
			TRACE("! Failed to get number of devices on the system, hr = 0x%lx\n",hr);
		}
	}

	// Report the number of devices found.  NOTE: we will report 0, if an error
	// occured.

	TRACE("\n%d Windows Portable Device(s) found on the system\n\n", cPnPDeviceIDs);
	//</SnippetDeviceEnum2>
	// Second, allocate an array to hold the PnPDeviceID strings returned from
	// the IPortableDeviceManager::GetDevices method
	if (SUCCEEDED(hr) && (cPnPDeviceIDs > 0))
	{
		pPnpDeviceIDs = new (std::nothrow) PWSTR[cPnPDeviceIDs];
		if (pPnpDeviceIDs != NULL)
		{
			DWORD dwIndex = 0;

			hr = pPortableDeviceManager->GetDevices(pPnpDeviceIDs, &cPnPDeviceIDs);

			
			// Free all returned PnPDeviceID strings by using CoTaskMemFree.
			// NOTE: CoTaskMemFree can handle NULL pointers, so no NULL
			//       check is needed.
			//<SnippetDeviceEnum3>
			for (dwIndex = 0; dwIndex < cPnPDeviceIDs; dwIndex++)
			{
				CoTaskMemFree(pPnpDeviceIDs[dwIndex]);
				pPnpDeviceIDs[dwIndex] = NULL;
			}

			// Delete the array of PWSTR pointers
			delete [] pPnpDeviceIDs;
			pPnpDeviceIDs = NULL;
			//</SnippetDeviceEnum3>
		}
		else
		{
			TRACE("! Failed to allocate memory for PWSTR array\n");
		}
	}
	return cPnPDeviceIDs;
}

void CSdiCoreWin7::GetClientInformation(IPortableDeviceValues** ppClientInformation)
{
	// Client information is optional.  The client can choose to identify itself, or
	// to remain unknown to the driver.  It is beneficial to identify yourself because
	// drivers may be able to optimize their behavior for known clients. (e.g. An
	// IHV may want their bundled driver to perform differently when connected to their
	// bundled software.)

	// CoCreate an IPortableDeviceValues interface to hold the client information.
	//<SnippetDeviceEnum7>
	HRESULT hr = CoCreateInstance(CLSID_PortableDeviceValues,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_PPV_ARGS(ppClientInformation));
	//</SnippetDeviceEnum7>
	//<SnippetDeviceEnum8>
	if (SUCCEEDED(hr))
	{
		// Attempt to set all bits of client information
		hr = (*ppClientInformation)->SetStringValue(WPD_CLIENT_NAME, CLIENT_NAME);
		if (FAILED(hr))
		{
			TRACE("! Failed to set WPD_CLIENT_NAME, hr = 0x%lx\n",hr);
		}

		hr = (*ppClientInformation)->SetUnsignedIntegerValue(WPD_CLIENT_MAJOR_VERSION, CLIENT_MAJOR_VER);
		if (FAILED(hr))
		{
			TRACE("! Failed to set WPD_CLIENT_MAJOR_VERSION, hr = 0x%lx\n",hr);
		}

		hr = (*ppClientInformation)->SetUnsignedIntegerValue(WPD_CLIENT_MINOR_VERSION, CLIENT_MINOR_VER);
		if (FAILED(hr))
		{
			TRACE("! Failed to set WPD_CLIENT_MINOR_VERSION, hr = 0x%lx\n",hr);
		}

		hr = (*ppClientInformation)->SetUnsignedIntegerValue(WPD_CLIENT_REVISION, CLIENT_REVISION);
		if (FAILED(hr))
		{
			TRACE("! Failed to set WPD_CLIENT_REVISION, hr = 0x%lx\n",hr);
		}

		//  Some device drivers need to impersonate the caller in order to function correctly.  Since our application does not
		//  need to restrict its identity, specify SECURITY_IMPERSONATION so that we work with all devices.
		hr = (*ppClientInformation)->SetUnsignedIntegerValue(WPD_CLIENT_SECURITY_QUALITY_OF_SERVICE, SECURITY_IMPERSONATION);
		if (FAILED(hr))
		{
			TRACE("! Failed to set WPD_CLIENT_SECURITY_QUALITY_OF_SERVICE, hr = 0x%lx\n",hr);
		}
	}
	else
	{
		TRACE("! Failed to CoCreateInstance CLSID_PortableDeviceValues, hr = 0x%lx\n",hr);
	}
}

HRESULT  CSdiCoreWin7::StartRemoteStudio(IPortableDevice* pDevice)
{
    HRESULT hr = S_OK;
	const WORD PTP_OPCODE_START_REMOTESTUDIO = PTP_OC_SAMSUNG_SetRemoteStudioMode;
	const WORD PTP_RESPONSECODE_OK = 0x2001;     // 0x2001 indicates command success

    // Build basic WPD parameters for the command
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDeviceValues,
                              NULL,
                              CLSCTX_INPROC_SERVER,
                              IID_IPortableDeviceValues,
                              (VOID**)&spParameters);
    }

    // Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
    // Similar commands exist for reading and writing data phases
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);	
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    // Specify the actual MTP opcode that we want to execute here
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_START_REMOTESTUDIO);
    // GetNumObject requires three params - storage ID, object format, and parent object handle   
    // Parameters need to be first put into a PropVariantCollection
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
                                  NULL,
                                  CLSCTX_INPROC_SERVER,
                                  IID_IPortableDevicePropVariantCollection,
                                  (VOID**)&spMtpParams);
    }

    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;

    // Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
    // should be changed to use the device's real storage ID (which can be obtained by
    // removing the prefix for the WPD object ID for the storage)
    if (hr == S_OK)
    {
		pvParam.ulVal = 0x0;
        hr = spMtpParams->Add(&pvParam);
    }

    // Add MTP parameters collection to our main parameter list
    if (hr == S_OK) hr = spParameters->SetIPortableDevicePropVariantCollectionValue( WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	// Send the command to the MTP device
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
    HRESULT hrCmd = S_OK;
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hrCmd != S_OK)	
	{
		TRACE("Driver return code: 0x%08X\n", hrCmd);
		return hrCmd;
	}
	
	// If the command was executed successfully, check the MTP response code to see if the
    // device can handle the command. Be aware that there is a distinction between the command
    // being successfully sent to the device and the command being handled successfully by the device.
    DWORD dwResponseCode = 0;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
    // If the command was executed successfully, the MTP response parameters are returned in 
    // the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS property, which is a PropVariantCollection
    CComPtr<IPortableDevicePropVariantCollection> spRespParams;
    if (hr == S_OK)	hr = spResults->GetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS, &spRespParams);

	if (hr != S_OK) return dwResponseCode;
	return SDI_ERR_OK;
}

SdiResult CSdiCoreWin7::SdiInitSession(char* strDevName)
{
	CComPtr<IPortableDevice> pIPortableDevice;
    pIPortableDevice = NULL;

	CString temp,temp1;
	int usize = MultiByteToWideChar(CP_ACP,0,strDevName,-1,NULL,NULL);

	if(usize <= 0) return SDI_ERR_INVALID_PARAMETER;

	WCHAR *ustr = new WCHAR[usize];
	MultiByteToWideChar(CP_ACP,0,strDevName,strlen(strDevName)+1,ustr,usize);

	temp.Format(_T("%s"),ustr);
	int cut_point;
	cut_point = temp.Find(_T("{"));
	temp1 = temp.Left(cut_point)+_T("{6ac27878-a6fa-4155-ba85-f98f491d4f33}");
	delete [] ustr;
	ustr = NULL;
		
    //return ChooseDevice(&pIPortableDevice,temp1);
	return ChooseDevice(&pIPortableDevice,temp1);
}
	
SdiResult CSdiCoreWin7::SdiOpenSession()
{
	HRESULT hr = S_OK;
	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;

	TRACE(_T("[SdiCore|Win7] Open Session\n"));
	hr = OpenSession(m_pIPortableDevice);
	if(hr==S_OK || (HRESULT)hr==0x800700aa)
	{
		//-- 0x800700aa : ERROR_BUSY
		if(m_nModel == SDI_MODEL_GH5)
			hr = SDI_ERR_OK;
		else
			hr = StartRemoteStudio(m_pIPortableDevice);	//-- for NX200 with no "Info.tgw", MF Focus

		if(hr == SDI_ERR_OK || hr == SDI_ERR_PTP_GENERAL_ERROR)
			return SDI_ERR_OK;
		else
			return hr;
	}
	return hr;
}

SdiResult CSdiCoreWin7::SdiExOpenSession()
{
	HRESULT hr = S_OK;
	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;

	TRACE(_T("[SdiCore|Win7] Open Session\n"));
	hr = OpenSession(m_pIPortableDevice);
	
	return hr;
}

SdiResult CSdiCoreWin7::SdiCloseSession()
{
	HRESULT hr = S_OK;
	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;

	TRACE(_T("[SdiCore|Win7] Terminate Session\n"));
	if(m_pIPortableDevice->Close() == S_OK)
	{
		hr = CloseSession(m_pIPortableDevice);
		m_pIPortableDevice = NULL;
		return hr;
	}
	else 
		return SDI_ERR_DEVICE_CLOSE_FAILED;
}
	
SdiResult CSdiCoreWin7::SdiSendAdjCapture(CString strSaveName)
{
	//CaptureExec(m_pIPortableDevice,strSaveName);
	return SDI_ERR_OK;
}

SdiResult CSdiCoreWin7::SdiSendAdjRequireCaptureOnce(int nModel)
{
	SdiResult result = SDI_ERR_OK;
	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;

	if(nModel == SDI_MODEL_GH5)
	{
		result = RequireCaptureExec(m_pIPortableDevice,CAPTURE_ONCE);
		if(result!=SDI_ERR_OK) 
			return result;
	}
	else
	{
		result = RequireCaptureExec(m_pIPortableDevice,S1_PRESS);
		if(result!=SDI_ERR_OK) 
			return result;
		Sleep(200);

		result = RequireCaptureExec(m_pIPortableDevice,S2_PRESS);
		if(result!=SDI_ERR_OK) 
			return result;
		Sleep(200);

		result = RequireCaptureExec(m_pIPortableDevice,S2_RELEASE);
		if(result!=SDI_ERR_OK) 
			return result;
		Sleep(200);

		result = RequireCaptureExec(m_pIPortableDevice,S1_RELEASE);
	}
	

	return result;
}

SdiResult CSdiCoreWin7::SdiSendShutterCapture(SdiInt8 nShutter)
{
    if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;

    switch(nShutter)
    {
    case S1_PRESS:
    case S2_PRESS:
    case S2_RELEASE:
    case S1_RELEASE:
	case S3_PRESS:
    case S3_RELEASE:
	case WAIT_MODE:
    case WAIT_EXIT:
	case CAPTURE_ONCE:
	case RECORD_START:
	case RECORD_STOP:
		return RequireCaptureExec(m_pIPortableDevice, nShutter);
    default:
        return SDI_ERR_INVALID_PARAMETER;
    }
}

SdiResult CSdiCoreWin7::SdiSendAdjCompleteCapture(CString strFileName, bool &bIsLast, int nFileNum, int nClose)
{
	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(strFileName.GetLength() <= 0) 
		return SDI_ERR_INVALID_PARAMETER;
	return CaptureExec(m_pIPortableDevice, strFileName, nFileNum, nClose);
}

//NX3000 20150119 ImageTransfer
SdiResult CSdiCoreWin7::SdiSendAdjCompleteCapture(CString strFileName, bool &bIsLast)
{
	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(strFileName.GetLength() <= 0) 
		return SDI_ERR_INVALID_PARAMETER;
	return CaptureExec(m_pIPortableDevice, strFileName);
}

SdiResult CSdiCoreWin7::SdiSendAdjMovieTransfer(CString strSaveName, int nfilenum, int nClose)
{
	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(strSaveName.GetLength() <= 0) 
		return SDI_ERR_INVALID_PARAMETER;
	return SdiMovieTransfer(m_pIPortableDevice, strSaveName,  nfilenum, nClose);
}

SdiResult CSdiCoreWin7::SdiSendAdjCompleteMovie(CString strFileName, int nWaitSaveDSC, int nFileNum, int nClose)  //-- 2014-09-19 joonho.kim
{
	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(strFileName.GetLength() <= 0) 
		return SDI_ERR_INVALID_PARAMETER;
	return MovieCompleteExec(m_pIPortableDevice, strFileName, nWaitSaveDSC, nFileNum, nClose); //-- 2014-09-19 joonho.kim
}

SdiResult CSdiCoreWin7::SdiSendAdjLiveview(unsigned char* pYUV)
{
	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;
	else if(pYUV==NULL) 
		return SDI_ERR_INVALID_PARAMETER;
	
	//-- 2011-06-14 hongsu.jung
	return LiveviewExec(m_pIPortableDevice, pYUV);
}

SdiResult CSdiCoreWin7::SdiSendAdjLiveviewInfo(RSLiveviewInfo* pLiveviewInfo)
{
	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;
	else if(pLiveviewInfo==NULL) 
		return SDI_ERR_INVALID_PARAMETER;

	return LiveviewInfo(m_pIPortableDevice, pLiveviewInfo);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author		hongsu.jung
//------------------------------------------------------------------------------ 
HRESULT  CSdiCoreWin7::OpenSession(IPortableDevice* pDevice)
{
	TRACE(_T("[SdiCore|Win7] OpenSession \n"));
    HRESULT hr = S_OK;
	const WORD PTP_OPCODE_OPEN							= 0x1002;	// GetNumObject opcode is 0x1002
	const WORD PTP_RESPONSECODE_OK						= 0x2001;   // 0x2001 indicates command success
	const WORD PTP_RESPONSECODE_INVALID_PARAMETER		= 0x201d;
	const WORD PTP_RESPONSECODE_SESSION_ALREADY_OPEN	= 0x201e;
    // Build basic WPD parameters for the command
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)
    {
		TRACE(_T("[SdiCore|Win7] OpenSession :: CoCreateInstance | CLSCTX_INPROC_SERVER\n"));
        hr = CoCreateInstance(CLSID_PortableDeviceValues,
                              NULL,
                              CLSCTX_INPROC_SERVER,
                              IID_IPortableDeviceValues,
                              (VOID**)&spParameters);
    }

    // Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
    // Similar commands exist for reading and writing data phases
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,	WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);	 
	if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID,	WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    // Specify the actual MTP opcode that we want to execute here
    if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE,		(ULONG) PTP_OPCODE_OPEN);

    // GetNumObject requires three params - storage ID, object format, and parent object handle   
    // Parameters need to be first put into a PropVariantCollection	
	TRACE(_T("[SdiCore|Win7] OpenSession :: CoCreateInstance | CLSID_PortableDevicePropVariantCollection\n"));
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)
        hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
                                  NULL,
                                  CLSCTX_INPROC_SERVER,
                                  IID_IPortableDevicePropVariantCollection,
                                  (VOID**)&spMtpParams);

	{
		TRACE(_T("[SdiCore|Win7] OpenSession :: SetIPortableDevicePropVariantCollectionValue\n"));
		PROPVARIANT pvParam = {0};
		pvParam.vt = VT_UI4;
		if (hr == S_OK)
		{
			pvParam.ulVal = PTP_PROP_BASE;
			hr = spMtpParams->Add(&pvParam);
		}	
		if (hr == S_OK) hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}
	
	

	TRACE(_T("[SdiCore|Win7] OpenSession :: Send the command to the MTP device\n"));
	// Send the command to the MTP device
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);

	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
    HRESULT hrCmd = S_OK;
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);	
    if (hrCmd != S_OK)
	{
		TRACE(_T("[SdiCore|Win7] [Error] OpenSession :: Driver return code: 0x%08X\n"),hrCmd);
		return hrCmd;
	}

    // If the command was executed successfully, check the MTP response code to see if the
    // device can handle the command. Be aware that there is a distinction between the command
    // being successfully sent to the device and the command being handled successfully by the device.
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK || dwResponseCode == (DWORD) PTP_RESPONSECODE_SESSION_ALREADY_OPEN || dwResponseCode == (DWORD)PTP_RESPONSECODE_INVALID_PARAMETER) ? S_OK : E_FAIL;
    return hr;
}

HRESULT  CSdiCoreWin7::CloseSession(IPortableDevice* pDevice)
{
    HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETNUMOBJECT = 0x1003; // GetNumObject opcode is 0x1003
	const WORD PTP_RESPONSECODE_OK = 0x2001;     // 0x2001 indicates command success

    // Build basic WPD parameters for the command
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDeviceValues,
                              NULL,
                              CLSCTX_INPROC_SERVER,
                              IID_IPortableDeviceValues,
                              (VOID**)&spParameters);
    }

    // Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
    // Similar commands exist for reading and writing data phases
    if (hr == S_OK)
    {
        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);	
 //                                        WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
    }

    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
//                                                      WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    }

    // Specify the actual MTP opcode that we want to execute here
    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
                                                      PTP_OPCODE_GETNUMOBJECT);
    }

    // GetNumObject requires three params - storage ID, object format, and parent object handle   
    // Parameters need to be first put into a PropVariantCollection
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
                                  NULL,
                                  CLSCTX_INPROC_SERVER,
                                  IID_IPortableDevicePropVariantCollection,
                                  (VOID**)&spMtpParams);
    }

    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;

    // Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
    // should be changed to use the device's real storage ID (which can be obtained by
    // removing the prefix for the WPD object ID for the storage)
    if (hr == S_OK)
    {
 //     pvParam.ulVal = 0x10001;
		pvParam.ulVal = 0x0001;
        hr = spMtpParams->Add(&pvParam);
    }
/*
    // Specify object format code parameter. 0x0 can be specified to indicate this is unused 
    if (hr == S_OK)
    {
        pvParam.ulVal = 0x0;
        hr = spMtpParams->Add(&pvParam);
    }

    // Specify parent object handle parameter. 0x0 can be specified to indicate this is unused
    if (hr == S_OK)
    {
        pvParam.ulVal = 0x0;
        hr = spMtpParams->Add(&pvParam);
    }
    
	if (hr == S_OK)
    {
        pvParam.ulVal = 0x0;
        hr = spMtpParams->Add(&pvParam);
    }

    if (hr == S_OK)
    {
        pvParam.ulVal = 0x0;
        hr = spMtpParams->Add(&pvParam);
    }
*/
    // Add MTP parameters collection to our main parameter list
    if (hr == S_OK)
    {
        hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
                                          WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
//		  hr = hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS,0x0001);
    }  

// Send the command to the MTP device
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK)
    {
        hr = pDevice->SendCommand(0, spParameters, &spResults);
    } 

// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
    HRESULT hrCmd = S_OK;
    if (hr == S_OK)
    {
         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    }

    if (hr == S_OK)
    {
        //TRACE("Driver return code: 0x%08X\n", hrCmd);
        hr = hrCmd;
    }

    // If the command was executed successfully, check the MTP response code to see if the
    // device can handle the command. Be aware that there is a distinction between the command
    // being successfully sent to the device and the command being handled successfully by the device.
    DWORD dwResponseCode = 0;

    if (hr == S_OK)
    {
       hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
//		hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &pbBufferOut, &cbBytesRead);
//		hr =spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE,&dwResponseCode);
    }

    if (hr == S_OK)
    {
		//TRACE("MTP Response code: 0x%X\n", dwResponseCode);
        hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
    }
  
    // If the command was executed successfully, the MTP response parameters are returned in 
    // the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS property, which is a PropVariantCollection

    CComPtr<IPortableDevicePropVariantCollection> spRespParams;

	 PWSTR pszStringValue = NULL;
    if (hr == S_OK)
    {

		hr = spResults->Clear(); // GetStringValue(WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS,&pszStringValue);
		
    }
	if(hr != S_OK) return dwResponseCode;
	return SDI_ERR_OK;
}

HRESULT CSdiCoreWin7::LiveviewExec(IPortableDevice* pDevice, unsigned char* pYUV)
{
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_CAPTURE	= PTP_OC_SAMSUNG_GetLiveviewImg;		
    const WORD PTP_RESPONSECODE_OK	= PTP_RC_OK;

    // Build basic WPD parameters for the command
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (VOID**)&spParameters);
    // Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
    if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID,  WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
    // Specify the actual MTP opcode to execute here
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_CAPTURE);
    // GetDevicePropValue requires the property code as an MTP parameter
    // MTP parameters need to be first put into a PropVariantCollection
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK) hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDevicePropVariantCollection, (VOID**)&spMtpParams);
    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;
    if (hr == S_OK)
    {
        pvParam.ulVal = PTP_PROP_BASE;
        hr = spMtpParams->Add(&pvParam);
    }
    // Add MTP parameters collection to our main parameter list
    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue( WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    // Send the command to initiate the transfer
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);
	if(!spParameters)
	{
		TRACE("ERROR spParameters\n");
		return SDI_ERR_LIVEVIEW_CREATE_COMMAND;;
	}

    // Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
    HRESULT hrCmd = S_OK;
    if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK)
    {
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		/*
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [LiveviewExec]: 0x%08X\n", hrCmd);
			return hr;
		} 
		*/
    }

    // If the transfer was initiated successfully, the driver returns a context cookie
    LPWSTR pwszContext = NULL;
    if (hr == S_OK)	hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);

    // The driver indicates how many bytes will be transferred. This is important to
    // retrieve because we have to read all the data that the device sends us (even if it
    // is not the size we were expecting); otherwise, we run the risk of the device going out of sync
    // with the driver.
    ULONG cbReportedDataSize = 0;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, &cbReportedDataSize);

    // Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
    // which suggests the chunk size that the date should be retrieved in. If your application will be 
    // transferring a large amount of data (>256K), use this property to break down the
    // transfer into small chunks so that your app is more responsive.
    // We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
    BOOL bSkipDataPhase = FALSE;
    if (hr == S_OK && cbReportedDataSize == 0)
    { 
        hr = S_FALSE;
        bSkipDataPhase = TRUE;
    }

    // Use the WPD_COMMAND_MTP_EXT_READ_DATA command to read in the data
    (void) spParameters->Clear();
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_READ_DATA.pid);
    // Specify the same context that we received earlier
    if (hr == S_OK) hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
    // Allocate a buffer for the command to read data into - this should 
    // be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
    BYTE* pbBufferIn = NULL;
    if (hr == S_OK)
    {
        pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
        if (pbBufferIn == NULL) hr = E_OUTOFMEMORY;
    }

    // Pass the allocated buffer as a parameter
    if (hr == S_OK) hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBufferIn, cbReportedDataSize);
    // Specify the number of bytes to transfer as a parameter
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, cbReportedDataSize);
    // Send the command to transfer the data
    spResults = NULL;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);
    // Check if the driver was able to transfer the data
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			//-- hrCmd = 0x80070079 | ������ ���� �ð��� ����Ǿ����ϴ�.
			//-- hrCmd = 0x8007001f | �ý��ۿ� ������ ��ġ�� �۵����� �ʽ��ϴ�.
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			return hr;
		}        
    }

    // IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
    // Instead, it is available in the results collection.
    BYTE* pbBufferOut = NULL;
    ULONG cbBytesRead = 0;
    if (hr == S_OK) hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);
    // Reset hr to S_OK because we skipped the data phase
    if (hr == S_FALSE && bSkipDataPhase == TRUE) hr = S_OK;	
	// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
    (void) spParameters->Clear();
    if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
    // Specify the same context that we received earlier
    if (hr == S_OK) hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
    // Send the completion command
    spResults = NULL;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);
    // Check if the driver successfully ended the data transfer
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK)
    {
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			return hr;
		}              
    }

    // If the command was executed successfully, check the MTP response code to see if the device
    // can handle the command. If the device cannot handle the command, the data phase would 
    // have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
    // error. 
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK) hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK)
    {
        hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
		if(hr == E_FAIL)
			TRACE("MTP Response code: 0x%X\n", dwResponseCode);
	}

    // If the command was handled by the device, return the property value in the BYREF property
    if (hr == S_OK)
    {
        if (pbBufferOut)
        {
			int nLen = cbReportedDataSize;
			memcpy(pYUV, pbBufferOut, nLen);
        }
        else
        {
            hr = E_UNEXPECTED;
        }
    }

    // If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
    // property. GetDevicePropValue does not return additional response parameters, so skip this code 
    // If required, you might find that code in the post that covered sending MTP commands without data

    // Free up any allocated memory
    CoTaskMemFree(pbBufferIn);
    CoTaskMemFree(pbBufferOut);
    CoTaskMemFree(pwszContext);

	switch(hr){
	case S_OK:
		return SDI_ERR_OK;
	case E_OUTOFMEMORY:
		return SDI_ERR_OUT_OF_MEMORY;
	case E_UNEXPECTED:
		return SDI_ERR_NO_DATA_PHASE_OCCURED;
	case 0x8007001fL:
		return SDI_ERR_DEVICE_DISCONNECTED;
	default:
		return dwResponseCode;
	}
}

HRESULT CSdiCoreWin7::LiveviewInfo(IPortableDevice* pDevice, RSLiveviewInfo* pLiveviewInfo)
{
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_CAPTURE	= PTP_OC_SAMSUNG_GetLiveviewInfo;		
    const WORD PTP_RESPONSECODE_OK	= PTP_RC_OK;

    // Build basic WPD parameters for the command
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDeviceValues,
                              NULL,
                              CLSCTX_INPROC_SERVER,
                              IID_IPortableDeviceValues,
                              (VOID**)&spParameters);
    }

    // Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
    if (hr == S_OK)
    {
        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
                                           WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
    }

    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
                                              WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
    }

    // Specify the actual MTP opcode to execute here
    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
                                                      (ULONG) PTP_OPCODE_CAPTURE);
    }

    // GetDevicePropValue requires the property code as an MTP parameter
    // MTP parameters need to be first put into a PropVariantCollection
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
                                  NULL,
                                  CLSCTX_INPROC_SERVER,
                                  IID_IPortableDevicePropVariantCollection,
                                  (VOID**)&spMtpParams);
    }

    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;

    if (hr == S_OK)
    {
        pvParam.ulVal = PTP_PROP_BASE;
        hr = spMtpParams->Add(&pvParam);
    }

    // Add MTP parameters collection to our main parameter list
    if (hr == S_OK)
    {
        hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
                                          WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    }  

    // Send the command to initiate the transfer
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK)
    {
        hr = pDevice->SendCommand(0, spParameters, &spResults);
    }

    // Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
    HRESULT hrCmd = S_OK;
    if (hr == S_OK)
    {
         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    }

    if (hr == S_OK)
    {
        hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [LiveviewInfo]: 0x%08X\n", hrCmd);
			return hr;
		}        
    }

    // If the transfer was initiated successfully, the driver returns a context cookie
    LPWSTR pwszContext = NULL;
    if (hr == S_OK)
    {
         hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
    }

    // The driver indicates how many bytes will be transferred. This is important to
    // retrieve because we have to read all the data that the device sends us (even if it
    // is not the size we were expecting); otherwise, we run the risk of the device going out of sync
    // with the driver.
    ULONG cbReportedDataSize = 0;
    if (hr == S_OK)
    {
        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, 
                                               &cbReportedDataSize);
    }

    // Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
    // which suggests the chunk size that the date should be retrieved in. If your application will be 
    // transferring a large amount of data (>256K), use this property to break down the
    // transfer into small chunks so that your app is more responsive.
    // We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
    BOOL bSkipDataPhase = FALSE;
    if (hr == S_OK && cbReportedDataSize == 0)
    { 
        hr = S_FALSE;
        bSkipDataPhase = TRUE;
    }

    if(spParameters)	
    (void) spParameters->Clear();
    if (hr == S_OK)
    {
        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
                                           WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
    }

    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
                                                      WPD_COMMAND_MTP_EXT_READ_DATA.pid);
    }

    // Specify the same context that we received earlier
    if (hr == S_OK)
    {
        hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
    }

    // Allocate a buffer for the command to read data into - this should 
    // be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
    BYTE* pbBufferIn = NULL;
    if (hr == S_OK)
    {
        pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
        if (pbBufferIn == NULL)
        {
            hr = E_OUTOFMEMORY;
        }
    }

    // Pass the allocated buffer as a parameter
    if (hr == S_OK)
    {
        hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, 
                                                        pbBufferIn, cbReportedDataSize);
    }

    // Specify the number of bytes to transfer as a parameter
    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, 
                                                        cbReportedDataSize);
    }

    // Send the command to transfer the data
    spResults = NULL;
    if (hr == S_OK)
    {
        hr = pDevice->SendCommand(0, spParameters, &spResults);
    }

    // Check if the driver was able to transfer the data

    if (hr == S_OK)
    {
         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    }

    if (hr == S_OK)
    {
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			return hr;
		}    
    }

    // IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
    // Instead, it is available in the results collection.
    BYTE* pbBufferOut = NULL;
    ULONG cbBytesRead = 0;
    if (hr == S_OK)
    {
        hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);
    }

    // Reset hr to S_OK because we skipped the data phase
    if (hr == S_FALSE && bSkipDataPhase == TRUE)
    {
        hr = S_OK;
    }
// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
    if(spParameters)	
    (void) spParameters->Clear();
    if (hr == S_OK)
    {
        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
                                           WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
    }

    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
                                                      WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
    }

    // Specify the same context that we received earlier
    if (hr == S_OK)
    {
        hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
    }

    // Send the completion command
    spResults = NULL;
    if (hr == S_OK)
    {
        hr = pDevice->SendCommand(0, spParameters, &spResults);
    }

    // Check if the driver successfully ended the data transfer
    if (hr == S_OK)
    {
         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    }

    if (hr == S_OK)
    {
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			return hr;
		}              
    }

    // If the command was executed successfully, check the MTP response code to see if the device
    // can handle the command. If the device cannot handle the command, the data phase would 
    // have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
    // error. 
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)
    {
        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    }

    if (hr == S_OK)
    {
        //TRACE("MTP Response code: 0x%X\n", dwResponseCode);
        hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
    }

    // If the command was handled by the device, return the property value in the BYREF property
    if (hr == S_OK)
    {
        if (pbBufferOut != NULL)
        {
			//-- 2011-06-11 hongsu.jung
			int nLen = cbReportedDataSize;

			//-- 2017 08 25 yongmin	NX1 buffer size over ( RSLiveviewInfo size 20 cbReportedDataSize size 32 )
			//memcpy(pLiveviewInfo, pbBufferOut, nLen);
			memcpy(pLiveviewInfo, pbBufferOut, sizeof(RSLiveviewInfo));

			//TRACE(_T("w: %d, h: %d, off w: %d, off y: %d\n"), pLiveviewInfo->imageSizeWidth, pLiveviewInfo->imageSizeHeight, pLiveviewInfo->offsetSizeWidth, pLiveviewInfo->offsetSizeHeight);
		}
        else
        {
            // MTP response code was OK, but no data phase occurred
            hr = E_UNEXPECTED;
        }
    }

    // If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
    // property. GetDevicePropValue does not return additional response parameters, so skip this code 
    // If required, you might find that code in the post that covered sending MTP commands without data

    // Free up any allocated memory
    CoTaskMemFree(pbBufferIn);
    CoTaskMemFree(pbBufferOut);
    CoTaskMemFree(pwszContext);
	
	switch(hr){
	case S_OK:
		return SDI_ERR_OK;
	case E_OUTOFMEMORY:
		return SDI_ERR_OUT_OF_MEMORY;
	case E_UNEXPECTED:
		return SDI_ERR_NO_DATA_PHASE_OCCURED;
	case 0x8007001fL:
		return SDI_ERR_DEVICE_DISCONNECTED;
	default:
		return dwResponseCode;
	}
}
//NX3000 20150119 ImageTransfer
HRESULT CSdiCoreWin7::CaptureExec(IPortableDevice* pDevice,CString strSaveName)
{
	CString strLogs;		
	TRACE(_T("[Capture] CaptureExec || File Save [%s]\n"),strSaveName);
	//WriteLog(_T("SaveStart"));
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_CAPTURE = PTP_OC_SAMSUNG_GetObjectInMemory;
    const WORD PTP_RESPONSECODE_OK = PTP_RC_OK;

	CComPtr<IPortableDeviceValues> spParameters;

	if (hr == S_OK)
		hr = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (VOID**)&spParameters);

	if (hr == S_OK)
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);

	if (hr == S_OK)
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID,  WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);

	if (hr == S_OK) 
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE,  (ULONG) PTP_OPCODE_CAPTURE);

	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;

	if (hr == S_OK)
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDevicePropVariantCollection, (VOID**)&spMtpParams);

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;
	if (hr == S_OK)
	{
		pvParam.ulVal = PTP_PROP_BASE;
		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK) 
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue( WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	
	CComPtr<IPortableDeviceValues> spResults;

	if (hr == S_OK) 
		hr = pDevice->SendCommand(0, spParameters, &spResults);

	HRESULT hrCmd = S_OK;

	if (hr == S_OK)
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	
	if (hr == S_OK)
	{
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			strLogs.Format(_T("1774 Driver return code (initiating): 0x%08X\n", hrCmd));
			//WriteLog(strLogs);
			TRACE("Driver return code (initiating): 0x%08X\n", hrCmd);
			return hr;
		} 
	}

	LPWSTR pwszContext = NULL;
	if (hr == S_OK)
		hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);

	ULONG cbReportedDataSize = 0;
	
	if (hr == S_OK) 
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE,  &cbReportedDataSize);

	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)
	{ 
		hr = S_FALSE;
		bSkipDataPhase = TRUE;
	}

	if(spParameters)	
		(void) spParameters->Clear();

	if (hr == S_OK)
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
	
	if (hr == S_OK) 
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID,  WPD_COMMAND_MTP_EXT_READ_DATA.pid);

	if (hr == S_OK) 
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   

	BYTE* pbBufferIn = NULL;
	if (hr == S_OK)
	{
		float nMBSize;		
		nMBSize = (float)cbReportedDataSize;
		nMBSize /=1024;
		nMBSize /=1024;
		TRACE(_T("[Capture] Memory Allocation [%0.2f]MB\n"),nMBSize);
		pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
		if (pbBufferIn == NULL)
		{
			hr = E_OUTOFMEMORY;
		}
	}

	if (hr == S_OK)
		hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA,  pbBufferIn, cbReportedDataSize);
	
	if (hr == S_OK) 
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, cbReportedDataSize);

	spResults = NULL;
	
	if (hr == S_OK)
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	
	if (hr == S_OK)
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);

	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			strLogs.Format(_T("1843 Driver return code (initiating): 0x%08X\n", hrCmd));
			//WriteLog(strLogs);
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			return hr;
		}    
	}

	BYTE* pbBufferOut = NULL;
	ULONG cbBytesRead = 0;
	if (hr == S_OK)
		hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);

	if (hr == S_FALSE && bSkipDataPhase == TRUE) 
		hr = S_OK;

	if(spParameters)	
		(void) spParameters->Clear();

	if (hr == S_OK)
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);

	if (hr == S_OK)
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID,  WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);

	if (hr == S_OK)
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   

	spResults = NULL;
	TRACE(_T("[Capture] >>> Start Transfer Data\n"));

	if (hr == S_OK) 
		hr = pDevice->SendCommand(0, spParameters, &spResults);

	TRACE(_T("[Capture] >>> End Transfer Data\n"));

	if (hr == S_OK) 
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);

	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			strLogs.Format(_T("1886 Driver return code (ending transfer): 0x%08X\n", hrCmd));
			//WriteLog(strLogs);
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			return hr;
		}              
	}

	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);

	if (hr == S_OK) 
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if (hr == S_OK)
	{
		if (pbBufferOut != NULL)
		{
#ifdef _THREAD_FILE_SAVE
			SdiFileInfo* pFileInfo = new SdiFileInfo;

			pFileInfo->pImageBuffer = NULL;
			//pFileInfo->strImageFileName = strSaveName;
			_tcscpy_s(pFileInfo->strImageFileName, strSaveName);
			

			if(pbBufferOut[0] == 0x1)
			{
				//pFileInfo->nImageSize		= cbReportedDataSize-1-12;
				pFileInfo->nImageSize		= cbReportedDataSize-1;
				pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize-1];
				memcpy(pFileInfo->pImageBuffer,pbBufferOut+1,cbReportedDataSize-1);			
			}
			else
			{
				//pFileInfo->nImageSize		= cbReportedDataSize-12;
				pFileInfo->nImageSize		= cbReportedDataSize;
				pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize];
				memset(pFileInfo->pImageBuffer, 0xFF, cbReportedDataSize);
				memcpy(pFileInfo->pImageBuffer,pbBufferOut,cbReportedDataSize);
			}

			strLogs.Format(_T("[Capture] File Save [%s] on Thread (File Manager)\n"),strSaveName);
			//WriteLog(strLogs);
			TRACE(_T("[Capture] File Save [%s] on Thread (File Manager)\n"),strSaveName);
			m_pSdiFileMgr->AddFile(pFileInfo);
#else
			CFile file;
			file.Open(strSaveName, CFile::modeWrite | CFile::modeCreate ,NULL);
			//-- 2013-6-20 yongmin.lee
			TRACE(_T("[Capture] >>> Start File Save [%s]\n"),strSaveName);
			file.Write(pbBufferOut,cbReportedDataSize);
			TRACE(_T("[Capture] <<< End File Save\n"));
			file.Flush();
			file.Close();
#endif

		}
		else
		{
			hr = E_UNEXPECTED;
		}
	}

	CoTaskMemFree(pbBufferIn);
	CoTaskMemFree(pbBufferOut);
	CoTaskMemFree(pwszContext);

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return hr;
}

HRESULT CSdiCoreWin7::RequireCaptureExec(IPortableDevice* pDevice,WORD code)
{
	TRACE(_T("Start [%d] RequireCaptureExec \r\n"), code);
	HRESULT hr = S_OK;
	WORD PTP_OPCODE_GETNUMOBJECT;
	if(code == CAPTURE_ONCE)
		PTP_OPCODE_GETNUMOBJECT = PTP_OC_CUSTOM_Capture; // opcode is 0x902a
	else if(code == RECORD_START)
		PTP_OPCODE_GETNUMOBJECT = PTP_OC_CUSTOM_Record; // opcode is 0x902a
	else if(code == RECORD_STOP)
		PTP_OPCODE_GETNUMOBJECT = PTP_OC_CUSTOM_RecordStop; // opcode is 0x902a
	else
		PTP_OPCODE_GETNUMOBJECT = PTP_OC_SAMSUNG_Capture; // GetNumObject opcode is 0x1006
	
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;     // PTP_RC_OK indicates command success

	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
		hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
	// Similar commands exist for reading and writing data phases
	if (hr == S_OK)
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	// Specify the actual MTP opcode that we want to execute here
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
	// GetNumObject requires three params - storage ID, object format, and parent object handle   
	// Parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;
	// Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
	// should be changed to use the device's real storage ID (which can be obtained by
	// removing the prefix for the WPD object ID for the storage)
	if (hr == S_OK)
	{
		pvParam.ulVal = code;
		hr = spMtpParams->Add(&pvParam);
	}

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	// Send the command to the MTP device
	CComPtr<IPortableDeviceValues> spResults;
	CString strLogs;
	strLogs.Format(_T("RequireCaptureExec SendCommand 000 %d", code));
	//WriteLog(strLogs);
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);   
	//WriteLog(_T("RequireCaptureExec SendCommand 222"));
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{	
		hr = hrCmd;
		if(hr != S_OK)
		{
			//WriteLog(_T("RequireCaptureExec SendCommand error"));
			CString strLog;
			strLog.Format(_T("RequireCaptureExec Error Message : 0x%08X\n"), hrCmd);
			//WriteLog(strLog);
			TRACE("###################################\n", hrCmd);
			TRACE("RequireCaptureExec #####################\n", hrCmd);
			TRACE("###################################\n", hrCmd);
		} 
	}
	
	// If the command was executed successfully, check the MTP response code to see if the
	// device can handle the command. Be aware that there is a distinction between the command
	// being successfully sent to the device and the command being handled successfully by the device.
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
	TRACE(_T("End [%d] RequireCaptureExec  \r\n"), code);
}

HRESULT CSdiCoreWin7::CaptureCompleteExec(IPortableDevice* pDevice,CString strSaveName, bool &bIsLast)
{
	int capture_start;
	//int capture_end;
	capture_start = GetTickCount();
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_CAPTURE	= PTP_OC_SAMSUNG_GetObjectInMemory;    
    const WORD PTP_RESPONSECODE_OK	= PTP_RC_OK;     // 0x2001 indicates command success

    // Build basic WPD parameters for the command
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (VOID**)&spParameters);
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
	// Specify the actual MTP opcode to execute here
    if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_CAPTURE);
	// GetDevicePropValue requires the property code as an MTP parameter
    // MTP parameters need to be first put into a PropVariantCollection
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;
    if (hr == S_OK)    { pvParam.ulVal = PTP_PROP_BASE; hr = spMtpParams->Add(&pvParam); }
	// Add MTP parameters collection to our main parameter list
    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	// Send the command to initiate the transfer
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
    HRESULT hrCmd = S_OK;
    if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	else			TRACE(_T(" CaptureCompleteExec() : SendCommand ERROR : 0x%08x\n"), hr);
	if (hr == S_OK)
	{	
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [CaptureCompleteExec]: 0x%08X\n", hrCmd);
			return hr;
		} 
	}
    // If the transfer was initiated successfully, the driver returns a context cookie
    LPWSTR pwszContext = NULL;
    if (hr == S_OK)	hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	// The driver indicates how many bytes will be transferred. This is important to
    // retrieve because we have to read all the data that the device sends us (even if it
    // is not the size we were expecting); otherwise, we run the risk of the device going out of sync
    // with the driver.
    ULONG cbReportedDataSize = 0;
	ULONG cbReportedChunkDataSize = 0;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE ,   &cbReportedDataSize);
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE ,  &cbReportedChunkDataSize);
    // Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
    // which suggests the chunk size that the date should be retrieved in. If your application will be 
    // transferring a large amount of data (>256K), use this property to break down the
    // transfer into small chunks so that your app is more responsive.
    // We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
    BOOL bSkipDataPhase = FALSE;
    if (hr == S_OK && cbReportedDataSize == 0)  {  hr = S_FALSE; bSkipDataPhase = TRUE; }
	BYTE* pbBufferIn = NULL;
	BYTE* pbBufferOut = NULL;
	BYTE* pbBufferTotal = NULL;	
	pbBufferTotal = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
	if(cbReportedDataSize==0 || cbReportedChunkDataSize==0)
	{
		TRACE(_T(" CaptureCompleteExec() : Data/Chunk Size : %d / %d\n"), cbReportedDataSize, cbReportedChunkDataSize);
		CoTaskMemFree(pbBufferTotal);
		return SDI_ERR_DEVICE_DISCONNECTED;
	}

	// Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
	// which suggests the chunk size that the date should be retrieved in. If your application will be 
	// transferring a large amount of data (>256K), use this property to break down the
	// transfer into small chunks so that your app is more responsive.
	// We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data

	int nDivideCnt = cbReportedDataSize/cbReportedChunkDataSize;
	for(int i=0 ; i<= nDivideCnt ;i++)
	{
		if(spParameters)
		(void) spParameters->Clear();
		if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
		if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_READ_DATA.pid);
		// Specify the same context that we received earlier
		if (hr == S_OK)	hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
		// Allocate a buffer for the command to read data into - this should 
		// be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
		if (hr == S_OK)
		{
			pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedChunkDataSize);
			if (pbBufferIn == NULL)
				hr = E_OUTOFMEMORY;
		}

		// Pass the allocated buffer as a parameter
		if (hr == S_OK)	hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBufferIn, cbReportedChunkDataSize);
		// Specify the number of bytes to transfer as a parameter
		if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, cbReportedChunkDataSize);
		// Send the command to transfer the data
		spResults = NULL;
		if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
		// Check if the driver was able to transfer the data
		if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
		if (hr == S_OK) hr = hrCmd;
		// IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
		// Instead, it is available in the results collection.

		ULONG cbBytesRead = 0;
		if (hr == S_OK)	hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);				
		if (pbBufferOut != NULL)
		{
			if( i != nDivideCnt)
				memcpy(pbBufferTotal+(i*cbReportedChunkDataSize),pbBufferOut,cbReportedChunkDataSize);
			else
				memcpy(pbBufferTotal+(i*cbReportedChunkDataSize),pbBufferOut,cbReportedDataSize%cbReportedChunkDataSize);
		}
		if(pbBufferIn)
		{
			CoTaskMemFree(pbBufferIn);
			pbBufferIn = NULL;
		}
		if(pbBufferOut)
		{
			CoTaskMemFree(pbBufferOut);
			pbBufferOut = NULL;
		}

    // Reset hr to S_OK because we skipped the data phase
    if (hr == S_FALSE && bSkipDataPhase == TRUE)	hr = S_OK;
	// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
	if(spParameters)
	(void) spParameters->Clear();
	if (hr != S_OK) TRACE(_T(" spParameters Clear() Error\n"));
	if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	// Specify the same context that we received earlier
	if (hr == S_OK) hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	// Send the completion command
	spResults = NULL;
	if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver successfully ended the data transfer
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;


	if(cbReportedDataSize!=0)
	{

		SdiFileInfo* pFileInfo = new SdiFileInfo;

		pFileInfo->pImageBuffer = NULL;
		//pFileInfo->strImageFileName = strSaveName;
		_tcscpy_s(pFileInfo->strImageFileName, strSaveName);

		if(pbBufferTotal[0] == 0x1)
		{
			pFileInfo->nImageSize		= cbReportedDataSize-1;
			pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize-1];
			memcpy(pFileInfo->pImageBuffer,pbBufferTotal+1,cbReportedDataSize-1);			
		}
		else
		{
			pFileInfo->nImageSize		= cbReportedDataSize;
			pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize];
			memcpy(pFileInfo->pImageBuffer,pbBufferTotal,cbReportedDataSize);
		}

		m_pSdiFileMgr->AddFile(pFileInfo);
// 		//- kcd 2013-11-25
// 		//- ���� ��� SdiDataMgr ���� ���� 
// 		CFile file;
// 		file.Open(strSaveName, CFile::modeWrite | CFile::modeCreate ,NULL);
// 		//-- 2013-6-20 yongmin.lee
// 		if(pbBufferTotal[0] == 0x1)
// 			file.Write(pbBufferTotal+1,cbReportedDataSize-1);
// 		else
// 			file.Write(pbBufferTotal,cbReportedDataSize);  
// 		file.Flush();
// 		file.Close();

		//--2011-09-07 jeansu : Return IsLastCaptureDone
// 		if(pbBufferTotal[0] == 0x1) 
// 			bIsLast = true;
		//-- 2013-11-22 hongsu@esmlab.com
		//-- Memory Leak
		CoTaskMemFree(pbBufferTotal);
	}

	TRACE(_T(" File Write Done!! %s\n\t\t[%d Bytes]\n"), strSaveName, cbReportedDataSize);


    // If the command was executed successfully, check the MTP response code to see if the device
    // can handle the command. If the device cannot handle the command, the data phase would 
    // have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
    // error. 
    DWORD dwResponseCode = 0x0;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK)	hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;// Free up any allocated memory
    CoTaskMemFree(pwszContext);	

	if (hr == S_OK) return SDI_ERR_OK;
	else if(hr==0x80070079){
		TRACE(_T(" GetCaptureComplete() : ERROR_SEM_TIMEOUT !!!\n"));
		return SDI_ERR_SEMAPHORE_TIMEOUT;
	}
	else return dwResponseCode;
	}
}

HRESULT CSdiCoreWin7::MovieCompleteExec(IPortableDevice* pDevice,CString strSaveName, int nWaitSaveDSC, int nFileNum, int nClose) //-- 2014-09-19 joonho.kim
{
	//-- 2014-09-20 joonho.kim
	//-- SaveFileName
	if(m_pSdiFileMgr->m_bFileClose)
	{
		m_strSaveName = strSaveName;
		m_pSdiFileMgr->m_bFileClose = FALSE;
	}

	TRACE(_T("[Movie] File Save [%s]\n"),strSaveName);
	//-- 2013-10-23 hongsu
	//-- Wait Until File Save on DSC from Buffer
	if (nWaitSaveDSC == 0)
		nWaitSaveDSC = DEFAULT_WAIT_SAVEDSC;

	TRACE(_T("[Movie] Waiting File Transfer [%d]ms for finish saving on SD Card\n"),DEFAULT_WAIT_SAVEDSC);
	Sleep(nWaitSaveDSC);

	HRESULT hr = S_OK;
	//const WORD PTP_OPCODE_CAPTURE	= PTP_OC_SAMSUNG_GetObjectInSDCard;    //-- 2014-09-19 joonho.kim
	const WORD PTP_OPCODE_CAPTURE	= PTP_OC_SAMSUNG_MovieTransfer;    
	const WORD PTP_RESPONSECODE_OK	= PTP_RC_OK;     // 0x2001 indicates command success


	//-----------------------------------------------------------------------------
	//--
	//-- 1. initiating
	//--
	//-----------------------------------------------------------------------------
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK) hr = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (VOID**)&spParameters);
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
	if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID,  WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);

	// Specify the actual MTP opcode to execute here
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE,  (ULONG) PTP_OPCODE_CAPTURE);
	// GetDevicePropValue requires the property code as an MTP parameter
	// MTP parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK) hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDevicePropVariantCollection, (VOID**)&spMtpParams);

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;
	
	//-- 2014-09-19 joonho.kim
	/*if (hr == S_OK)
	{
		pvParam.ulVal = PTP_PROP_BASE;
		hr = spMtpParams->Add(&pvParam);
	}*/

	if (hr == S_OK)
	{
		pvParam.ulVal = nFileNum;
		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
	{
		pvParam.ulVal = nClose;
		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue( WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating): 0x%08X\n", hrCmd);
			return hr;
		} 
	}

	LPWSTR pwszContext = NULL;
	if (hr == S_OK)
		hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	
	ULONG cbReportedDataSize = 0;
	if (hr == S_OK) 
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE,  &cbReportedDataSize);

	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)
	{ 
		hr = S_FALSE;
		bSkipDataPhase = TRUE;
	}

	//-- 2014-09-20 joonho.kim
	//-- file size check
	if(cbReportedDataSize < (20*1024*1024))
		nClose = 1;
	else
		m_pSdiFileMgr->m_bFileClose = FALSE;

	if(nClose == 1)
		m_pSdiFileMgr->m_bFileClose = TRUE;

	if(spParameters)	
		(void) spParameters->Clear();
	if (hr == S_OK) 
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
	if (hr == S_OK) 
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID,  WPD_COMMAND_MTP_EXT_READ_DATA.pid);
	if (hr == S_OK) 
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   

	BYTE* pbBufferIn = NULL;
	if (hr == S_OK)
	{
		float nMBSize;
		nMBSize = (float)cbReportedDataSize;
		nMBSize /=1024;
		nMBSize /=1024;
		TRACE(_T("[Movie] Memory Allocation [%0.2f]MB\n"),nMBSize);
		pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
		if (pbBufferIn == NULL)
		{
			hr = E_OUTOFMEMORY;
		}
	}

	if (hr == S_OK) 
		hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA,  pbBufferIn, cbReportedDataSize);

	if (hr == S_OK) 
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, cbReportedDataSize);


	spResults = NULL;
	if (hr == S_OK) 
		hr = pDevice->SendCommand(0, spParameters, &spResults);

	if (hr == S_OK)
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);

	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			return hr;
		}    
	}

	BYTE* pbBufferOut = NULL;
	ULONG cbBytesRead = 0;
	if (hr == S_OK)
		hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);

	if (hr == S_FALSE && bSkipDataPhase == TRUE) 
		hr = S_OK;
	
	if(spParameters)	
		(void) spParameters->Clear();

	if (hr == S_OK) 
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);

	if (hr == S_OK)
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID,  WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);

	if (hr == S_OK)
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	
	spResults = NULL;
	TRACE(_T("[Movie] >>> Start Transfer Data\n"));
	if (hr == S_OK)
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	TRACE(_T("[Movie] >>> End Transfer Data\n"));

	
	if (hr == S_OK) 

		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			return hr;
		}              
	}

	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);

	if (hr == S_OK)
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if (hr == S_OK)
	{
		if (pbBufferOut != NULL)
		{
#ifdef _THREAD_FILE_SAVE
			SdiFileInfo* pFileInfo = new SdiFileInfo;

			pFileInfo->pImageBuffer = NULL;
			//pFileInfo->strImageFileName = strSaveName; //-- 2014-09-20 joonho.kim
			//pFileInfo->strImageFileName = m_strSaveName; //-- 2014-09-20 joonho.kim
			_tcscpy_s(pFileInfo->strImageFileName, m_strSaveName);


			if(pbBufferOut[0] == 0x1)
			{
				pFileInfo->nImageSize		= cbReportedDataSize-1;
				pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize-1];
				memcpy(pFileInfo->pImageBuffer,pbBufferOut+1,cbReportedDataSize-1);			
			}
			else
			{
				pFileInfo->nImageSize		= cbReportedDataSize;
				pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize];
				memcpy(pFileInfo->pImageBuffer,pbBufferOut,cbReportedDataSize);
			}

			TRACE(_T("[Movie] File Save [%s] on Thread (File Manager)\n"),strSaveName);
			pFileInfo->nAppendMsg = WM_SDI_OP_MOVIE_TRANSFER;
			pFileInfo->nClose = nClose;
			m_pSdiFileMgr->AddFile(pFileInfo);
#else
			CFile file;
			file.Open(strSaveName, CFile::modeWrite | CFile::modeCreate ,NULL);
			//-- 2013-6-20 yongmin.lee
			TRACE(_T("[Movie] >>> Start File Save [%s]\n"),strSaveName);
			file.Write(pbBufferOut,cbReportedDataSize);
			TRACE(_T("[Movie] <<< End File Save \n"));
			file.Flush();
			file.Close();
#endif

		}
		else
		{
			hr = E_UNEXPECTED;
		}
	}
	CoTaskMemFree(pbBufferIn);
	CoTaskMemFree(pbBufferOut);
	CoTaskMemFree(pwszContext);

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return hr;

}

HRESULT CSdiCoreWin7::SdiMovieTransfer(IPortableDevice* pDevice,CString strSaveName,int nfilenum, int nClose)
{
	int capture_start;
	//int capture_end;
	capture_start = GetTickCount();
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_CAPTURE	= PTP_OC_SAMSUNG_MovieTransfer;   
	const WORD PTP_RESPONSECODE_OK	= PTP_RC_OK;     // 0x2001 indicates command success
	
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (VOID**)&spParameters);
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
	if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
	// Specify the actual MTP opcode to execute here
	if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_CAPTURE);
	// GetDevicePropValue requires the property code as an MTP parameter
	// MTP parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		pvParam.ulVal = nfilenum;
		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
	{
		pvParam.ulVal = nClose;
		hr = spMtpParams->Add(&pvParam);
	}
	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	// Send the command to initiate the transfer
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	else			TRACE(_T(" CaptureCompleteExec() : SendCommand ERROR : 0x%08x\n"), hr);
	if (hr == S_OK)
	{	
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [SdiMovieTransfer]: 0x%08X\n", hrCmd);
			return hr;
		} 
	}
	// If the transfer was initiated successfully, the driver returns a context cookie
	LPWSTR pwszContext = NULL;
	if (hr == S_OK)	hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	// The driver indicates how many bytes will be transferred. This is important to
	// retrieve because we have to read all the data that the device sends us (even if it
	// is not the size we were expecting); otherwise, we run the risk of the device going out of sync
	// with the driver.
	ULONG cbReportedDataSize = 0;
	ULONG cbReportedChunkDataSize = 0;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE ,   &cbReportedDataSize);
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE ,  &cbReportedChunkDataSize);
	// Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
	// which suggests the chunk size that the date should be retrieved in. If your application will be 
	// transferring a large amount of data (>256K), use this property to break down the
	// transfer into small chunks so that your app is more responsive.
	// We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)  {  hr = S_FALSE; bSkipDataPhase = TRUE; }
	BYTE* pbBufferIn = NULL;
	BYTE* pbBufferOut = NULL;
	BYTE* pbBufferTotal = NULL;	
	pbBufferTotal = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
	if(cbReportedDataSize==0 || cbReportedChunkDataSize==0)
	{
		TRACE(_T(" CaptureCompleteExec() : Data/Chunk Size : %d / %d\n"), cbReportedDataSize, cbReportedChunkDataSize);
		CoTaskMemFree(pbBufferTotal);
		return SDI_ERR_DEVICE_DISCONNECTED;
	}

	// Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
	// which suggests the chunk size that the date should be retrieved in. If your application will be 
	// transferring a large amount of data (>256K), use this property to break down the
	// transfer into small chunks so that your app is more responsive.
	// We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data

	int nDivideCnt = cbReportedDataSize/cbReportedChunkDataSize;
	for(int i=0 ; i<= nDivideCnt ;i++)
	{
		// Use the WPD_COMMAND_MTP_EXT_READ_DATA command to read in the data
		(void) spParameters->Clear();
		if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
		if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_READ_DATA.pid);
		// Specify the same context that we received earlier
		if (hr == S_OK)	hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
		// Allocate a buffer for the command to read data into - this should 
		// be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
		if (hr == S_OK)
		{
			pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedChunkDataSize);
			if (pbBufferIn == NULL)
				hr = E_OUTOFMEMORY;
		}

		// Pass the allocated buffer as a parameter
		if (hr == S_OK)	hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBufferIn, cbReportedChunkDataSize);
		// Specify the number of bytes to transfer as a parameter
		if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, cbReportedChunkDataSize);
		// Send the command to transfer the data
		spResults = NULL;
		if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
		// Check if the driver was able to transfer the data
		if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
		if (hr == S_OK) hr = hrCmd;
		// IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
		// Instead, it is available in the results collection.

		ULONG cbBytesRead = 0;
		if (hr == S_OK)	hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);				
		if (pbBufferOut != NULL)
		{
			if( i != nDivideCnt)
				memcpy(pbBufferTotal+(i*cbReportedChunkDataSize),pbBufferOut,cbReportedChunkDataSize);
			else
				memcpy(pbBufferTotal+(i*cbReportedChunkDataSize),pbBufferOut,cbReportedDataSize%cbReportedChunkDataSize);
		}
		CoTaskMemFree(pbBufferIn);
		CoTaskMemFree(pbBufferOut);
	}

	// Reset hr to S_OK because we skipped the data phase
	if (hr == S_FALSE && bSkipDataPhase == TRUE)	hr = S_OK;
	// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
	(void) spParameters->Clear();
	if (hr != S_OK) TRACE(_T(" spParameters Clear() Error\n"));
	if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	// Specify the same context that we received earlier
	if (hr == S_OK) hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	// Send the completion command
	spResults = NULL;
	if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver successfully ended the data transfer
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK) hr = hrCmd;


	if(cbReportedDataSize!=0)
	{

		SdiFileInfo* pFileInfo = new SdiFileInfo;

		pFileInfo->pImageBuffer = NULL;
		//pFileInfo->strImageFileName = strSaveName;
		_tcscpy_s(pFileInfo->strImageFileName, strSaveName);

		if(pbBufferTotal[0] == 0x1)
		{
			pFileInfo->nImageSize		= cbReportedDataSize-1;
			pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize-1];
			memcpy(pFileInfo->pImageBuffer,pbBufferTotal+1,cbReportedDataSize-1);			
		}
		else
		{
			pFileInfo->nImageSize		= cbReportedDataSize;
			pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize];
			memcpy(pFileInfo->pImageBuffer,pbBufferTotal,cbReportedDataSize);
		}

		m_pSdiFileMgr->AddFile(pFileInfo);
		// 		//- kcd 2013-11-25
		// 		//- ���� ��� SdiDataMgr ���� ���� 
		// 		CFile file;
		// 		file.Open(strSaveName, CFile::modeWrite | CFile::modeCreate ,NULL);
		// 		//-- 2013-6-20 yongmin.lee
		// 		if(pbBufferTotal[0] == 0x1)
		// 			file.Write(pbBufferTotal+1,cbReportedDataSize-1);
		// 		else
		// 			file.Write(pbBufferTotal,cbReportedDataSize);  
		// 		file.Flush();
		// 		file.Close();

		//--2011-09-07 jeansu : Return IsLastCaptureDone
		// 		if(pbBufferTotal[0] == 0x1) 
		// 			bIsLast = true;
		//-- 2013-11-22 hongsu@esmlab.com
		//-- Memory Leak
		CoTaskMemFree(pbBufferTotal);
	}

	TRACE(_T(" File Write Done!! %s\n\t\t[%d Bytes]\n"), "C:\\picture\\test.txt", cbReportedDataSize);


	// If the command was executed successfully, check the MTP response code to see if the device
	// can handle the command. If the device cannot handle the command, the data phase would 
	// have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
	// error. 
	DWORD dwResponseCode = 0x0;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK)	hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;// Free up any allocated memory
	CoTaskMemFree(pwszContext);	
	if (hr == S_OK) return SDI_ERR_OK;
	else if(hr==0x80070079){
		TRACE(_T(" GetCaptureComplete() : ERROR_SEM_TIMEOUT !!!\n"));
		return SDI_ERR_SEMAPHORE_TIMEOUT;
	}
	else return dwResponseCode;

}

void CSdiCoreWin7::ChooseDevice(IPortableDevice** ppDevice)
{
    HRESULT                         hr              = S_OK;
    UINT                            uiCurrentDevice = 0;
    CHAR                            szSelection[81] = {0};
    DWORD                           cPnPDeviceIDs   = 0;
    PWSTR*                          pPnpDeviceIDs   = NULL;

    CComPtr<IPortableDeviceManager> pPortableDeviceManager;
    CComPtr<IPortableDeviceValues>  pClientInformation;

    if (ppDevice == NULL)
    {
        TRACE("! A NULL IPortableDevice interface pointer was received\n");
        return;
    }

    if (*ppDevice != NULL)
    {
        // To avoid operating on potiential bad pointers, reject any non-null
        // IPortableDevice interface pointers.  This will force the caller
        // to properly release the interface before obtaining a new one.
        TRACE("! A non-NULL IPortableDevice interface pointer was received, please release this interface before obtaining a new one.\n");
        return;
    }

    // Fill out information about your application, so the device knows
    // who they are speaking to.
	// ������
    GetClientInformation(&pClientInformation);

    // Enumerate and display all devices.
    cPnPDeviceIDs = EnumerateAllDevices();

    if (cPnPDeviceIDs > 0)
    {
        // Prompt user to enter an index for the device they want to choose.
        TRACE("Enter the index of the device you wish to use.\n>");
        hr = StringCbGetsA(szSelection,sizeof(szSelection));
        if (SUCCEEDED(hr))
        {
            uiCurrentDevice = (UINT) atoi(szSelection);
            if (uiCurrentDevice >= cPnPDeviceIDs)
            {
                TRACE("An invalid device index was specified, defaulting to the first device in the list.\n");
                uiCurrentDevice = 0;
            }
        }
        else
        {
            TRACE("An invalid device index was specified, defaulting to the first device in the list.\n");
            uiCurrentDevice = 0;
        }

        // CoCreate the IPortableDeviceManager interface to enumerate
        // portable devices and to get information about them.
		// Open_Session - ������
        hr = CoCreateInstance(CLSID_PortableDeviceManager,
                              NULL,
                              CLSCTX_INPROC_SERVER,
                              IID_PPV_ARGS(&pPortableDeviceManager));
        if (FAILED(hr))
        {
            TRACE("! Failed to CoCreateInstance CLSID_PortableDeviceManager, hr = 0x%lx\n",hr);
        }

        // Allocate an array to hold the PnPDeviceID strings returned from
        // the IPortableDeviceManager::GetDevices method
        if (SUCCEEDED(hr) && (cPnPDeviceIDs > 0))
        {
            pPnpDeviceIDs = new (std::nothrow)PWSTR[cPnPDeviceIDs];
            if (pPnpDeviceIDs != NULL)
            {
                DWORD dwIndex = 0;

                hr = pPortableDeviceManager->GetDevices(pPnpDeviceIDs, &cPnPDeviceIDs);
                if (SUCCEEDED(hr))
                {
					//<SnippetDeviceEnum5>
                    // CoCreate the IPortableDevice interface and call Open() with
                    // the chosen PnPDeviceID string.
                    hr = CoCreateInstance(CLSID_PortableDeviceFTM,
                                          NULL,
                                          CLSCTX_INPROC_SERVER,
                                          IID_PPV_ARGS(ppDevice));
                    if (SUCCEEDED(hr))
                    {
                        hr = (*ppDevice)->Open(pPnpDeviceIDs[uiCurrentDevice], pClientInformation);
						
						//LPWSTR pPnpDeviceIDsTemp;
						//pPnpDeviceIDsTemp = pPnpDeviceIDs[uiCurrentDevice];

						//hr = (*ppDevice)->Open(pPnpDeviceIDsTemp, pClientInformation);
                        if (FAILED(hr))
                        {
                            if (hr == E_ACCESSDENIED)
                            {
                                TRACE("Failed to Open the device for Read Write access, will open it for Read-only access instead\n");
                                pClientInformation->SetUnsignedIntegerValue(WPD_CLIENT_DESIRED_ACCESS, GENERIC_READ);
                                hr = (*ppDevice)->Open(pPnpDeviceIDs[uiCurrentDevice], pClientInformation);
                                if (FAILED(hr))
                                {
                                    TRACE("! Failed to Open the device, hr = 0x%lx\n",hr);
                                    // Release the IPortableDevice interface, because we cannot proceed
                                    // with an unopen device.
                                    (*ppDevice)->Release();
                                    *ppDevice = NULL;
                                }
                            }
                            else
                            {
                                TRACE("! Failed to Open the device, hr = 0x%lx\n",hr);
                                // Release the IPortableDevice interface, because we cannot proceed
                                // with an unopen device.
                                (*ppDevice)->Release();
                                *ppDevice = NULL;
                            }
                        }
						else
						{
							//-- ������
							RegisterForEventNotifications(*ppDevice);
						}
                    }
                    else
                    {
                        TRACE("! Failed to CoCreateInstance CLSID_PortableDeviceFTM, hr = 0x%lx\n",hr);
                    }
					//</SnippetDeviceEnum5>
                }
                else
                {
                    TRACE("! Failed to get the device list from the system, hr = 0x%lx\n",hr);
                }

                // Free all returned PnPDeviceID strings by using CoTaskMemFree.
                // NOTE: CoTaskMemFree can handle NULL pointers, so no NULL
                //       check is needed.
				//<SnippetDeviceEnum4>
                for (dwIndex = 0; dwIndex < cPnPDeviceIDs; dwIndex++)
                {
                    CoTaskMemFree(pPnpDeviceIDs[dwIndex]);
                    pPnpDeviceIDs[dwIndex] = NULL;
                }

                // Delete the array of PWSTR pointers
                delete [] pPnpDeviceIDs;
                pPnpDeviceIDs = NULL;
				//</SnippetDeviceEnum4>
            }
            else
            {
                TRACE("! Failed to allocate memory for PWSTR array\n");
            }
        }

    }
}

SdiResult CSdiCoreWin7::ChooseDevice(IPortableDevice** ppDevice, CString strDeviceName)
{
	HRESULT  hr = S_OK;
	CComPtr<IPortableDeviceValues>  pClientInformation;

	if (ppDevice == NULL)
	{
		TRACE("! A NULL IPortableDevice interface pointer was received\n");
		return SDI_ERR_INVALID_PARAMETER;
	}

	if (*ppDevice != NULL)
	{
		// To avoid operating on potiential bad pointers, reject any non-null
		// IPortableDevice interface pointers.  This will force the caller
		// to properly release the interface before obtaining a new one.
		TRACE("! A non-NULL IPortableDevice interface pointer was received, please release this interface before obtaining a new one.\n");
		return SDI_ERR_INVALID_PARAMETER;
	}

	// Fill out information about your application, so the device knows
	// who they are speaking to.
	// ������
	GetClientInformation(&pClientInformation);


	if (SUCCEEDED(hr))
	{
		hr = CoCreateInstance(CLSID_PortableDeviceFTM,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_PPV_ARGS(ppDevice));

		if (SUCCEEDED(hr))
		{
			m_pIPortableDevice = *ppDevice;

//			wsprintf(temp,_T("%s"),strDeviceName);
			hr = (*ppDevice)->Open(strDeviceName, pClientInformation);
			
			if (FAILED(hr))
			{
				if (hr == E_ACCESSDENIED)
				{
					TRACE("Failed to Open the device for Read Write access, will open it for Read-only access instead\n");
					pClientInformation->SetUnsignedIntegerValue(WPD_CLIENT_DESIRED_ACCESS, GENERIC_READ);
					hr = (*ppDevice)->Open(strDeviceName, pClientInformation);
					if (FAILED(hr))
					{
						TRACE("! Failed to Open the device, hr = 0x%lx\n",hr);
						// Release the IPortableDevice interface, because we cannot proceed
						// with an unopen device.
						(*ppDevice)->Release();
						*ppDevice = NULL;
						return SDI_ERR_ACCESS_DENIED;
					}
					return SDI_ERR_OK;	// Read-Only ���� Ȯ�� �ʿ�.
				}
				else
				{
					TRACE("! Failed to Open the device, hr = 0x%lx\n",hr);
					// Release the IPortableDevice interface, because we cannot proceed
					// with an unopen device.
					(*ppDevice)->Release();
					*ppDevice = NULL;
					return SDI_ERR_DEVICE_OPEN_FAILED;
				}
			}
			else
			{
				RegisterForEventNotifications(*ppDevice);
//				AfxMessageBox(_T("success"));
				return SDI_ERR_OK;
			}
		}
		else
		{
			TRACE("! Failed to get the device list from the system, hr = 0x%lx\n",hr);
			return SDI_ERR_INVALID_PARAMETER;
		}
	}
	return SDI_ERR_OK;
}

SdiResult CSdiCoreWin7::SdiSendAdjCapture(SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size)
{
	return 0;
}

SdiResult CSdiCoreWin7::SdiGetDevUniqueID(SdiInt  nPTPCode,  SdiChar **chUniqueID)
{
	return GetDevUniqueID(m_pIPortableDevice, nPTPCode, chUniqueID);
}

SdiResult CSdiCoreWin7::SdiSetDevUniqueID(SdiInt  nPTPCode,  CString strUUID)
{
	return SetDevPropValue(m_pIPortableDevice, nPTPCode, strUUID);
}

SdiResult CSdiCoreWin7::SdiGetDevModeIndex(SdiInt  nPTPCode, SdiInt  &nModeIndex)
{
	GetDevModeIndex(m_pIPortableDevice, nPTPCode, nModeIndex);
	return SDI_ERR_OK;
}

SdiResult CSdiCoreWin7::SdiGetDevPropDesc(WORD wPropCode, WORD wAllFlag)
{
	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;	
	
	return GetDevPropDesc(m_pIPortableDevice,wPropCode, wAllFlag);
}

SdiResult CSdiCoreWin7::SdiSetDevPropValue(SdiInt nPTPCode, SdiInt nType, LPARAM lpValue)
{
	int nPropValue = 0;
	CString strPropValue = _T("");
	
	
	switch(nType)
	{
	case PTP_VALUE_INT_8:
	case PTP_VALUE_UINT_8:
	case PTP_VALUE_UINT_16:
	case PTP_VALUE_UINT_32:
	case PTP_DPV_UINT32:
		return SetDevPropValue(m_pIPortableDevice, nPTPCode, nType, (unsigned int)lpValue);
	case PTP_VALUE_STRING:
	case PTP_DPV_STR:
		{
		TCHAR* cValue = (TCHAR*)lpValue;
		CString strValue= _T("");
		strValue.Format(_T("%s"), cValue);
		//delete cValue;
		return SetDevPropValue(m_pIPortableDevice,nPTPCode, strValue);
		}
	 
	default:
		return SDI_ERR_INVALID_PARAMETER;
	}
}

SdiResult CSdiCoreWin7::SdiCheckEvent(SdiInt32& nEventExist, SdiUIntArray* pArList)
{
	//SdiInt32 nEventExist = 0;
	SdiResult result = SDI_ERR_OK;

	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;
	result = SendCheckEvent(m_pIPortableDevice,&nEventExist, pArList);

#if 0
	if(result==SDI_ERR_OK) 
		return nEventExist;
	else return result;
#else
	return result;
#endif
}

SdiResult CSdiCoreWin7::SdiGetEvent(BYTE **pData,SdiInt32 *size)
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(pData==NULL || size==NULL) return SDI_ERR_INVALID_PARAMETER;

	return SendGetEvent(m_pIPortableDevice,pData,size);
}

SdiResult CSdiCoreWin7::SdiSetFocusPosition(FocusPosition * ptFocusPos, SdiInt8 nType)
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(ptFocusPos==NULL) return SDI_ERR_INVALID_PARAMETER;

	return GetFocusPosition(m_pIPortableDevice, ptFocusPos, nType);
}

SdiResult CSdiCoreWin7::SdiSetFocusPosition(SdiInt32 nValue)
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	return SetFocusPosition(m_pIPortableDevice, nValue);
}

HRESULT CSdiCoreWin7::SetFocusPosition(IPortableDevice*  pDevice, SdiInt32 nCurPos)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_SETFOCUSPOSITION = PTP_OC_SAMSUNG_SetFocusPosition;
	const WORD PTP_RESPONSECODE_OK = PTP_RC_OK;     // 0x2001 indicates command success
	LPWSTR pwszContext = NULL;

    // Build basic WPD parameters for the command
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDeviceValues,
                              NULL,
                              CLSCTX_INPROC_SERVER,
                              IID_IPortableDeviceValues,
                              (VOID**)&spParameters);
    }

    // Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
    // Similar commands exist for reading and writing data phases
    if (hr == S_OK)
    {
        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
                                           WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
    }

    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
                                                      WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    }

    // Specify the actual MTP opcode that we want to execute here
    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
                                                      (ULONG) PTP_OPCODE_SETFOCUSPOSITION);
    }

    // GetNumObject requires three params - storage ID, object format, and parent object handle   
    // Parameters need to be first put into a PropVariantCollection
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
                                  NULL,
                                  CLSCTX_INPROC_SERVER,
                                  IID_IPortableDevicePropVariantCollection,
                                  (VOID**)&spMtpParams);
    }

    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;

    // Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
    // should be changed to use the device's real storage ID (which can be obtained by
    // removing the prefix for the WPD object ID for the storage)
    if (hr == S_OK)
    {
        pvParam.ulVal = nCurPos;
        hr = spMtpParams->Add(&pvParam);
    }

    // Add MTP parameters collection to our main parameter list
    if (hr == S_OK)
    {
        hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
                                          WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    }  

	// Send the command to the MTP device
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK)
    {
        hr = pDevice->SendCommand(0, spParameters, &spResults);
    } 
    
    // Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
    HRESULT hrCmd = S_OK;
    if (hr == S_OK)
    {
         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    }

    if (hr == S_OK)
    {
        //TRACE("Driver return code: 0x%08X\n", hrCmd);
        hr = hrCmd;
    }

    // If the command was executed successfully, check the MTP response code to see if the
    // device can handle the command. Be aware that there is a distinction between the command
    // being successfully sent to the device and the command being handled successfully by the device.
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)
    {
        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    }

    if (hr == S_OK)
    {
        //TRACE("MTP Response code: 0x%X\n", dwResponseCode);
        hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
    }
    return hr;
}

HRESULT CSdiCoreWin7::GetFocusPosition(IPortableDevice*  pDevice, FocusPosition* ptFocusPos, SdiInt8 nType)
{
	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: nType = %d\n"),nType);	
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GET_FOCUSPOS = PTP_OC_SAMSUNG_GetFocusPosition;    
	const WORD PTP_RESPONSECODE_OK = 0x2001;     // 0x2001 indicates command success

	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: CoCreateInstance / CLSID_PortableDeviceValues\n"));	
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
	if (hr == S_OK)        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
	// Specify the actual MTP opcode to execute here
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GET_FOCUSPOS);


	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: CoCreateInstance / CLSID_PortableDevicePropVariantCollection\n"));	
	// GetDevicePropValue requires the property code as an MTP parameter
	// MTP parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		pvParam.ulVal = PTP_PROP_BASE;
		hr = spMtpParams->Add(&pvParam);
	}

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)        hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);

	// Send the command to initiate the transfer
	CComPtr<IPortableDeviceValues> spResults;
	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: SendCommand\n"));	
	if (hr == S_OK)        hr = pDevice->SendCommand(0, spParameters, &spResults);  

	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [GetFocusPosition]: 0x%08X\n", hrCmd);\
			return hr;
		} 
	}

	// If the transfer was initiated successfully, the driver returns a context cookie
	LPWSTR pwszContext = NULL;
	if (hr == S_OK)         hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	// The driver indicates how many bytes will be transferred. This is important to
	// retrieve because we have to read all the data that the device sends us (even if it
	// is not the size we were expecting); otherwise, we run the risk of the device going out of sync
	// with the driver.
	ULONG cbReportedDataSize = 0;
	if (hr == S_OK)        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, &cbReportedDataSize);

	// Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
	// which suggests the chunk size that the date should be retrieved in. If your application will be 
	// transferring a large amount of data (>256K), use this property to break down the
	// transfer into small chunks so that your app is more responsive.
	// We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)
	{ 
		hr = S_FALSE;
		bSkipDataPhase = TRUE;
	}

	// Use the WPD_COMMAND_MTP_EXT_READ_DATA command to read in the data
	(void) spParameters->Clear();
	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: SetGuidValue\n"));
	if (hr == S_OK)        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_READ_DATA.pid);
	// Specify the same context that we received earlier
	if (hr == S_OK)        hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   

	// Allocate a buffer for the command to read data into - this should 
	// be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
	BYTE* pbBufferIn = NULL;
	if (hr == S_OK)
	{
		pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
		if (pbBufferIn == NULL)
		{
			hr = E_OUTOFMEMORY;
		}
	}

	// Pass the allocated buffer as a parameter
	if (hr == S_OK)        hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBufferIn, cbReportedDataSize);
	// Specify the number of bytes to transfer as a parameter
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, cbReportedDataSize);
	// Send the command to transfer the data
	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: Send the command to transfer the data\n"));
	spResults = NULL;
	if (hr == S_OK)	        hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver was able to transfer the data
	if (hr == S_OK)         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			return hr;
		}    
	}

	// IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
	// Instead, it is available in the results collection.
	BYTE* pbBufferOut = NULL;
	ULONG cbBytesRead = 0;
	if (hr == S_OK)        hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);
	// Reset hr to S_OK because we skipped the data phase
	if (hr == S_FALSE && bSkipDataPhase == TRUE)
	{
		hr = S_OK;
	}
	// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
	(void) spParameters->Clear();
	if (hr == S_OK)        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	// Specify the same context that we received earlier
	if (hr == S_OK)        hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	// Send the completion command
	spResults = NULL;
	if (hr == S_OK)        hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver successfully ended the data transfer
	if (hr == S_OK)         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			return hr;
		}    
	}

	// If the command was executed successfully, check the MTP response code to see if the device
	// can handle the command. If the device cannot handle the command, the data phase would 
	// have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
	// error. 
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK)
	{
		//TRACE(_T("[SdiCore|Win7] GetFocusPosition :: MTP Response code: 0x%08X\n"), dwResponseCode);
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}

	// If the command was handled by the device, return the property value in the BYREF property
	if (hr == S_OK)
	{
		if (pbBufferOut != NULL)
		{
			int nLen = cbReportedDataSize;
			memcpy(ptFocusPos, pbBufferOut, nLen);

			int nCurPos;
			int nPosDetailUnit	= (ptFocusPos->max - ptFocusPos->min) / FOCUS_DETAIL_UNIT;
			int nPosRoughUnit	= (ptFocusPos->max - ptFocusPos->min) / FOCUS_ROUGH_UNIT;
			TRACE(_T("Focus Max : %d, Focus Min : %d\n"), ptFocusPos->max, ptFocusPos->min);

			switch(nType)
			{
			case SDI_FOCUS_NEARER	:	nCurPos = ptFocusPos->current - nPosRoughUnit;	break;
			case SDI_FOCUS_NEAR		:	nCurPos = ptFocusPos->current - nPosDetailUnit;		break;
			case SDI_FOCUS_FAR		:	nCurPos = ptFocusPos->current + nPosDetailUnit;	break;
			case SDI_FOCUS_FARTHER	:	nCurPos = ptFocusPos->current +  nPosRoughUnit;	break;
			case SDI_FOCUS_NULL		:
				nCurPos = ptFocusPos->current;	break;
			default:
				{
					return SDI_ERR_INVALID_PARAMETER;
				}
			}

			if(nCurPos < ptFocusPos->min)	nCurPos = ptFocusPos->min;
			if(nCurPos > ptFocusPos->max)	nCurPos = ptFocusPos->max;

			TRACE(_T("[SdiCore|Win7] Focus :: [%04d < %04d < %04d] ==> Move To %04d\n"),ptFocusPos->min, ptFocusPos->current, ptFocusPos->max, nCurPos);
			SetFocusPosition(m_pIPortableDevice, ptFocusPos->min);
			Sleep(500);
			SetFocusPosition(m_pIPortableDevice, nCurPos);
			//-- 2011-07-06 hongsu.jung
			//-- Change CurPos
			ptFocusPos->current = nCurPos;
		}
		else
		{
			// MTP response code was OK, but no data phase occurred
			hr = E_UNEXPECTED;
		}
	}

	// If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
	// property. GetDevicePropValue does not return additional response parameters, so skip this code 
	// If required, you might find that code in the post that covered sending MTP commands without data

	// Free up any allocated memory
	CoTaskMemFree(pbBufferIn);
	CoTaskMemFree(pbBufferOut);
	CoTaskMemFree(pwszContext);

	switch(hr){
	case S_OK:
		return SDI_ERR_OK;
	case E_OUTOFMEMORY:
		return SDI_ERR_OUT_OF_MEMORY;
	case E_UNEXPECTED:
		return SDI_ERR_NO_DATA_PHASE_OCCURED;
	case 0x8007001fL:
		return SDI_ERR_DEVICE_DISCONNECTED;
	default:
		return dwResponseCode;
	}
}

SdiResult CSdiCoreWin7::SdiGetFocusPosition(FocusPosition* ptFocusPos)
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	return GetFocusPosition(m_pIPortableDevice, ptFocusPos);
}

HRESULT CSdiCoreWin7::GetFocusPosition(IPortableDevice*  pDevice, FocusPosition* ptFocusPos)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GET_FOCUSPOS = PTP_OC_SAMSUNG_GetFocusPosition;    
	const WORD PTP_RESPONSECODE_OK = 0x2001;     // 0x2001 indicates command success

	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: CoCreateInstance / CLSID_PortableDeviceValues\n"));	

	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
	if (hr == S_OK)        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
	// Specify the actual MTP opcode to execute here
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GET_FOCUSPOS);


	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: CoCreateInstance / CLSID_PortableDevicePropVariantCollection\n"));	
	// GetDevicePropValue requires the property code as an MTP parameter
	// MTP parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		pvParam.ulVal = PTP_PROP_BASE;
		hr = spMtpParams->Add(&pvParam);
	}

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)        hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);

	// Send the command to initiate the transfer
	CComPtr<IPortableDeviceValues> spResults;
	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: SendCommand\n"));	
	if (hr == S_OK)        hr = pDevice->SendCommand(0, spParameters, &spResults);  

	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [GetFocusPosition]: 0x%08X\n", hrCmd);
			
			return hr;
		} 
	}

	// If the transfer was initiated successfully, the driver returns a context cookie
	LPWSTR pwszContext = NULL;
	if (hr == S_OK)         hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	// The driver indicates how many bytes will be transferred. This is important to
	// retrieve because we have to read all the data that the device sends us (even if it
	// is not the size we were expecting); otherwise, we run the risk of the device going out of sync
	// with the driver.
	ULONG cbReportedDataSize = 0;
	if (hr == S_OK)        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, &cbReportedDataSize);

	// Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
	// which suggests the chunk size that the date should be retrieved in. If your application will be 
	// transferring a large amount of data (>256K), use this property to break down the
	// transfer into small chunks so that your app is more responsive.
	// We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)
	{ 
		hr = S_FALSE;
		bSkipDataPhase = TRUE;
	}

	// Use the WPD_COMMAND_MTP_EXT_READ_DATA command to read in the data
	(void) spParameters->Clear();
	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: SetGuidValue\n"));
	if (hr == S_OK)        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_READ_DATA.pid);
	// Specify the same context that we received earlier
	if (hr == S_OK)        hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   

	// Allocate a buffer for the command to read data into - this should 
	// be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
	BYTE* pbBufferIn = NULL;
	if (hr == S_OK)
	{
		pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
		if (pbBufferIn == NULL)
		{
			hr = E_OUTOFMEMORY;
		}
	}

	// Pass the allocated buffer as a parameter
	if (hr == S_OK)        hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBufferIn, cbReportedDataSize);
	// Specify the number of bytes to transfer as a parameter
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, cbReportedDataSize);
	// Send the command to transfer the data
	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: Send the command to transfer the data\n"));
	spResults = NULL;
	if (hr == S_OK)	        hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver was able to transfer the data
	if (hr == S_OK)         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			
			return hr;
		}    
	}

	// IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
	// Instead, it is available in the results collection.
	BYTE* pbBufferOut = NULL;
	ULONG cbBytesRead = 0;
	if (hr == S_OK)        hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);
	// Reset hr to S_OK because we skipped the data phase
	if (hr == S_FALSE && bSkipDataPhase == TRUE)
	{
		hr = S_OK;
	}
	// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
	(void) spParameters->Clear();
	if (hr == S_OK)        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	// Specify the same context that we received earlier
	if (hr == S_OK)        hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	// Send the completion command
	spResults = NULL;
	if (hr == S_OK)        hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver successfully ended the data transfer
	if (hr == S_OK)         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			
			return hr;
		}    
	}

	// If the command was executed successfully, check the MTP response code to see if the device
	// can handle the command. If the device cannot handle the command, the data phase would 
	// have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
	// error. 
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK)
	{
		//TRACE(_T("[SdiCore|Win7] GetFocusPosition :: MTP Response code: 0x%08X\n"), dwResponseCode);
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}

	// If the command was handled by the device, return the property value in the BYREF property
	if (hr == S_OK)
	{
		if (pbBufferOut)
		{
			if(cbBytesRead > 12)
				memcpy(ptFocusPos, pbBufferOut, 12);
			else
				memcpy(ptFocusPos, pbBufferOut, cbBytesRead);
		}
		else
			hr = E_UNEXPECTED;
	}

	// If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
	// property. GetDevicePropValue does not return additional response parameters, so skip this code 
	// If required, you might find that code in the post that covered sending MTP commands without data

	// Free up any allocated memory
	CoTaskMemFree(pbBufferIn);
	CoTaskMemFree(pbBufferOut);
	CoTaskMemFree(pwszContext);

	
	switch(hr){
	case S_OK:
		return SDI_ERR_OK;
	case E_OUTOFMEMORY:
		return SDI_ERR_OUT_OF_MEMORY;
	case E_UNEXPECTED:
		return SDI_ERR_NO_DATA_PHASE_OCCURED;
	case 0x8007001fL:
		return SDI_ERR_DEVICE_DISCONNECTED;
	default:
		return dwResponseCode;
	}
}

HRESULT CSdiCoreWin7::GetDevUniqueID(IPortableDevice*  pDevice, WORD wPropCode, char **chUniqueID)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETDEVICEPROPDESC = PTP_OC_GetDevicePropDesc;
	const WORD PTP_RESPONSECODE_OK = 0x2001;     // 0x2001 indicates command success
	
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
	if (hr == S_OK)		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	if (hr == S_OK)		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
	// Specify the actual MTP opcode to execute here
	if (hr == S_OK)		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETDEVICEPROPDESC);
	// GetDevicePropDesc requires the property code as an MTP parameter
	// MTP parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	// Specify the device property code as the MTP parameter
	if (hr == S_OK)
	{
		pvParam.ulVal = wPropCode;
		hr = spMtpParams->Add(&pvParam);
	}

	// Get All Flag
	if (hr == S_OK)
	{
		if(m_nModel >= SDI_MODEL_GH5)
		{
			pvParam.ulVal = 0;
			hr = spMtpParams->Add(&pvParam);
		}
	}

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	// Send the command to initiate the transfer
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)		hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)		hr = hrCmd;
	// If the transfer was initiated successfully, the driver returns a context cookie
	LPWSTR pwszContext = NULL;
	if (hr == S_OK)		hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	// The driver indicates how many bytes will be transferred. This is important to
	// retrieve because we have to read all the data that the device sends us (even if it
	// is not the size we were expecting); otherwise, we run the risk of the device going out of sync
	// with the driver.
	ULONG cbReportedDataSize = 0;
	if (hr == S_OK)		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, &cbReportedDataSize);
	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)
	{ 
		hr = S_FALSE;
		bSkipDataPhase = TRUE;
	}

	// Use the WPD_COMMAND_MTP_EXT_READ_DATA command to read in the data
	(void) spParameters->Clear();
	if (hr == S_OK)		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
	if (hr == S_OK)		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_READ_DATA.pid);
	// Specify the same context that we received earlier
	if (hr == S_OK)		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	// Allocate a buffer for the command to read data into - this should 
	// be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
	BYTE* pbBufferIn = NULL;
	if (hr == S_OK)
	{
		pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
		if (pbBufferIn == NULL)
			hr = E_OUTOFMEMORY;
	}

	// Pass the allocated buffer as a parameter
	if (hr == S_OK)		hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBufferIn, cbReportedDataSize);
	// Specify the number of bytes to transfer as a parameter
	if (hr == S_OK)		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, cbReportedDataSize);
	// Send the command to transfer the data
	spResults = NULL;
	if (hr == S_OK)		hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver was able to transfer the data
	if (hr == S_OK)		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)		hr = hrCmd;
	// IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
	// Instead, it is available in the results collection.
	BYTE* pbBufferOut = NULL;
	ULONG cbBytesRead = 0;
	if (hr == S_OK)		hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);
	// Reset hr to S_OK because we skipped the data phase
	if (hr == S_FALSE && bSkipDataPhase == TRUE)		hr = S_OK;
	// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
	(void) spParameters->Clear();
	if (hr == S_OK)		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	if (hr == S_OK)		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
// Specify the same context that we received earlier
	if (hr == S_OK)		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	// Send the completion command
	spResults = NULL;
	if (hr == S_OK)		hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver successfully ended the data transfer
	if (hr == S_OK)		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)		hr = hrCmd;
	// If the command was executed successfully, check the MTP response code to see if the device
	// can handle the command. If the device cannot handle the command, the data phase would 
	// have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
	// error. 
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK)		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	// If the command was handled by the device, return the property value in the BYREF property
	if (hr == S_OK)
	{
		if(m_nModel >= SDI_MODEL_GH5)
		{//pana
			if (pbBufferOut != NULL)
			{
				UINT offSet = 0;
				ULONG propCode, all_flag, dataType, cur_val, data_size;
				
				memcpy(&propCode, &pbBufferOut[offSet], 4); offSet+=4;
				memcpy(&all_flag, &pbBufferOut[offSet], 4); offSet+=4;
				memcpy(&dataType, &pbBufferOut[offSet], 4); offSet+=4;
				memcpy(&cur_val,  &pbBufferOut[offSet], 4); offSet+=4;
				memcpy(&data_size,&pbBufferOut[offSet], 4); offSet+=4;
				//TRACE("propCode : 0x%x\n", propCode);
				//TRACE("dataType : 0x%x\n", dataType);
				//TRACE("get/set  : 0x%x\n", getSet);

				switch(dataType){					
				case PTP_DPV_STR:	// STR
					{
						int current;
						char *str;

						str = new char[data_size + 1];
						current=offSet;
							
						for(int i=0;i<data_size;i++) 
						{
							offSet=current+(i); // default
							str[i]=pbBufferOut[offSet]; 
						}

						str[data_size]=NULL;
							
						//-- Set UniqueID
						*chUniqueID = (char*)CoTaskMemAlloc(data_size);
						memset(*chUniqueID, NULL, data_size);
						strcpy(*chUniqueID,str);

						delete[] str;
					}
					break;
				default:
					break;
				}

				if(hr == S_OK)
					hr = SDI_ERR_OK;
			}
			else
			{
				// MTP response code was OK, but no data phase occurred
				hr = E_UNEXPECTED;
			}
		}
		else
		{
			if (pbBufferOut != NULL)
			{
				// Plan [5/24/11] jeansu.kim
				// Desc�� ġȯ, unpack �ϸ� �ǰڴ�.
				UINT offSet = 0;
				USHORT propCode, dataType;
				UCHAR getSet;
				memcpy(&propCode, &pbBufferOut[offSet], 2); offSet+=2;
				memcpy(&dataType, &pbBufferOut[offSet], 2); offSet+=2;
				memcpy(&getSet, &pbBufferOut[offSet], 1); offSet+=1;
				//TRACE("propCode : 0x%x\n", propCode);
				//TRACE("dataType : 0x%x\n", dataType);
				//TRACE("get/set  : 0x%x\n", getSet);

				//ULONG cur_val, def_val, form_val;
				ULONG cur_val, def_val, form_val;

				UCHAR str_len;
				char *str;
				cur_val = def_val = form_val = 0x0;
				CString temp;
				switch(dataType){
				case PTP_DPV_STR:	// STR
					{
						int current;
						memcpy(&str_len, &pbBufferOut[offSet], 1); offSet+=1;
						str = new char[str_len + 1];
						current=offSet;
						for(int i=0;i<str_len;i++) {
							offSet=current+(i*2); // default
							str[i]=pbBufferOut[offSet]; 
						}
						offSet+=2;
						memcpy(&str_len, &pbBufferOut[offSet], 1); offSet+=1;
						delete[] str;
						str = new char[str_len + 1];
						current=offSet;
						for(int i=0;i<str_len;i++)
						{
							offSet=current+(i*2); // current
							str[i]=pbBufferOut[offSet]; 
						}
						str[str_len]=NULL;
						offSet+=2;
						if(str_len == 0x00)
							hr = S_FALSE;
						//-- Set UniqueID
						*chUniqueID = (char*)CoTaskMemAlloc(str_len);
						memset(*chUniqueID, NULL, str_len);
						strcpy(*chUniqueID,str);
						delete[] str;
					}
					break;
				default:					
					break;
				}
				if(hr == S_OK)
					hr = SDI_ERR_OK;
			}
			else
			{
				// MTP response code was OK, but no data phase occurred
				hr = E_UNEXPECTED;
				//-- *chUniqueID
			}
		}
	}

	// If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
	// property. GetDevicePropValue does not return additional response parameters, so skip this code 
	// If required, you might find that code in the post that covered sending MTP commands without data

	// Free up any allocated memory
	CoTaskMemFree(pbBufferIn);
	CoTaskMemFree(pbBufferOut);
	CoTaskMemFree(pwszContext);
	if( *chUniqueID != NULL)
	{
		CString strUniqueID;
		int usize = MultiByteToWideChar(CP_ACP,0,*chUniqueID,-1,NULL,NULL);
		WCHAR *ustr = new WCHAR[usize];
		MultiByteToWideChar(CP_ACP,0,*chUniqueID,strlen(*chUniqueID)+1,ustr,usize);
		strUniqueID.Format(_T("%s"),ustr);
		if( ustr )
			delete[] ustr;
		m_strUniqueID = strUniqueID.Right(5);
		m_strFullID = strUniqueID;
	}

	return hr;
}

HRESULT CSdiCoreWin7::GetDevModeIndex(IPortableDevice*  pDevice, WORD wPropCode, SdiInt  &nModeIndex)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETDEVICEPROPDESC = PTP_OC_GetDevicePropDesc;
	const WORD PTP_RESPONSECODE_OK = 0x2001;     // 0x2001 indicates command success

	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
	}

	// Specify the actual MTP opcode to execute here
	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_GETDEVICEPROPDESC);
	}

	// GetDevicePropDesc requires the property code as an MTP parameter
	// MTP parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	// Specify the device property code as the MTP parameter
	if (hr == S_OK)
	{
		pvParam.ulVal = wPropCode;
		hr = spMtpParams->Add(&pvParam);
	}

	// Get All Flag
	if (hr == S_OK)
	{
		if(m_nModel >= SDI_MODEL_GH5)
		{
			pvParam.ulVal = 0;
			hr = spMtpParams->Add(&pvParam);
		}
	}

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	// Send the command to initiate the transfer
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	}

	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [GetDevModeIndex]: 0x%08X\n", hrCmd);
			return hr;
		} 
	}

	// If the transfer was initiated successfully, the driver returns a context cookie
	LPWSTR pwszContext = NULL;
	if (hr == S_OK)
	{
		hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	}

	// The driver indicates how many bytes will be transferred. This is important to
	// retrieve because we have to read all the data that the device sends us (even if it
	// is not the size we were expecting); otherwise, we run the risk of the device going out of sync
	// with the driver.
	ULONG cbReportedDataSize = 0;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, 
			&cbReportedDataSize);
	}

	// Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
	// which suggests the chunk size that the date should be retrieved in. If your application will be 
	// transferring a large amount of data (>256K), use this property to break down the
	// transfer into small chunks so that your app is more responsive.
	// We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)
	{ 
		hr = S_FALSE;
		bSkipDataPhase = TRUE;
	}

	// Use the WPD_COMMAND_MTP_EXT_READ_DATA command to read in the data
	(void) spParameters->Clear();
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_READ_DATA.pid);
	}

	// Specify the same context that we received earlier
	if (hr == S_OK)
	{
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	}

	// Allocate a buffer for the command to read data into - this should 
	// be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
	BYTE* pbBufferIn = NULL;
	if (hr == S_OK)
	{
		pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
		if (pbBufferIn == NULL)
		{
			hr = E_OUTOFMEMORY;
		}
	}

	// Pass the allocated buffer as a parameter
	if (hr == S_OK)
	{
		hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, 
			pbBufferIn, cbReportedDataSize);
	}

	// Specify the number of bytes to transfer as a parameter
	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, 
			cbReportedDataSize);
	}

	// Send the command to transfer the data
	spResults = NULL;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	}

	// Check if the driver was able to transfer the data

	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			return hr;
		}    
	}

	// IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
	// Instead, it is available in the results collection.
	BYTE* pbBufferOut = NULL;
	ULONG cbBytesRead = 0;
	if (hr == S_OK)
	{
		hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);
	}

	// Reset hr to S_OK because we skipped the data phase
	if (hr == S_FALSE && bSkipDataPhase == TRUE)
	{
		hr = S_OK;
	}
	// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
	(void) spParameters->Clear();
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	}

	// Specify the same context that we received earlier
	if (hr == S_OK)
	{
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	}

	// Send the completion command
	spResults = NULL;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	}

	// Check if the driver successfully ended the data transfer
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			return hr;
		}              
	}

	// If the command was executed successfully, check the MTP response code to see if the device
	// can handle the command. If the device cannot handle the command, the data phase would 
	// have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
	// error. 
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	}

	if (hr == S_OK)
	{
		//TRACE("MTP Response code: 0x%X\n", dwResponseCode);
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}

	// If the command was handled by the device, return the property value in the BYREF property
	if(m_nModel >= SDI_MODEL_GH5)
	{//pana
		if (hr == S_OK)
		{
			if (pbBufferOut != NULL)
			{
				UINT offSet = 0;
				ULONG propCode, dataType, cur_val, data_size, all_flag;

				memcpy(&propCode, &pbBufferOut[offSet], 4); offSet+=4;
				memcpy(&all_flag, &pbBufferOut[offSet], 4); offSet+=4;
				memcpy(&dataType, &pbBufferOut[offSet], 4); offSet+=4;
				memcpy(&cur_val,  &pbBufferOut[offSet], 4); offSet+=4;
				memcpy(&data_size,&pbBufferOut[offSet], 4); offSet+=4;
				//TRACE("propCode : 0x%x\n", propCode);
				//TRACE("dataType : 0x%x\n", dataType);
				//TRACE("get/set  : 0x%x\n", getSet);

				if(all_flag)
				{
					m_DeviceInfoMgr.CleanAll(wPropCode);					
					m_DeviceInfoMgr.SetPropCode(wPropCode);			

					switch(dataType){
					case PTP_DPV_UINT32	:		// UINT32
						{
							m_DeviceInfoMgr.SetType(wPropCode, SDI_TYPE_UINT_32);
							m_DeviceInfoMgr.SetCurrentEnum(wPropCode,cur_val);	

							int nCount = 0;
							if(data_size)
							{
								nCount = data_size/4;
								for(int i=0 ;i<nCount ; i++)
								{
									memcpy(&cur_val, &pbBufferOut[offSet], 4); offSet+=4;
									//TRACE("0x%x\n", form_val);
									//-- 2011-06-09 hongsu.jung
									m_DeviceInfoMgr.AddEnum(wPropCode,cur_val);		
								}
							}
						}
						break;						
					default:
						break;
					}
				}
				else
				{
					if(dataType == PTP_DPV_UINT32)
					{
						m_DeviceInfoMgr.SetCurrentEnum(wPropCode,cur_val);		
					}
				}
			}
			else
			{
				// MTP response code was OK, but no data phase occurred
				hr = E_UNEXPECTED;
			}
		}
	}
	else
	{
		if (hr == S_OK)
		{
			if (pbBufferOut != NULL)
			{
				//-- 2011-5-30 Lee JungTaek
				//-- Clean All
				m_DeviceInfoMgr.CleanAll(wPropCode);

				// Plan [5/24/11] jeansu.kim
				// Desc�� ġȯ, unpack �ϸ� �ǰڴ�.
				UINT offSet = 0;
				USHORT propCode, dataType;
				UCHAR getSet;
				memcpy(&propCode, &pbBufferOut[offSet], 2); offSet+=2;
				memcpy(&dataType, &pbBufferOut[offSet], 2); offSet+=2;
				memcpy(&getSet, &pbBufferOut[offSet], 1); offSet+=1;
				//TRACE("propCode : 0x%x\n", propCode);
				//TRACE("dataType : 0x%x\n", dataType);
				//TRACE("get/set  : 0x%x\n", getSet);

				//ULONG cur_val, def_val, form_val;
				ULONG cur_val, def_val, form_val;
				cur_val = def_val = form_val = 0x0;
				CString temp;
				switch(dataType)
				{			
				case PTP_DPV_UINT16 :		// UINT16
					{	
						//-- 2011-06-09 hongsu.jung
						m_DeviceInfoMgr.SetType(wPropCode, PTP_VALUE_UINT_16);

						//-- Default Value
						//-- 2011-06-09 hongsu.jung
						memcpy(&def_val, &pbBufferOut[offSet], 2); offSet+=2;					
						memcpy(&cur_val, &pbBufferOut[offSet], 2); offSet+=2;

						m_DeviceInfoMgr.SetDefaultEnum(wPropCode,def_val);
						m_DeviceInfoMgr.SetCurrentEnum(wPropCode,cur_val);	

						nModeIndex = cur_val;
					}
					break;
				default:					break;
				}
			}
			else
			{
				// MTP response code was OK, but no data phase occurred
				hr = E_UNEXPECTED;
			}
		}
	}

	// If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
	// property. GetDevicePropValue does not return additional response parameters, so skip this code 
	// If required, you might find that code in the post that covered sending MTP commands without data

	// Free up any allocated memory
	CoTaskMemFree(pbBufferIn);
	CoTaskMemFree(pbBufferOut);
	CoTaskMemFree(pwszContext);

	return hr;
}

HRESULT CSdiCoreWin7::GetDevPropDesc(IPortableDevice*  pDevice, WORD wPropCode, WORD wAllFlag)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETDEVICEPROPDESC = PTP_OC_GetDevicePropDesc;
	const WORD PTP_RESPONSECODE_OK = 0x2001;     // 0x2001 indicates command success

	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
	}

	// Specify the actual MTP opcode to execute here
	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_GETDEVICEPROPDESC);
	}

	// GetDevicePropDesc requires the property code as an MTP parameter
	// MTP parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	// Specify the device property code as the MTP parameter
	if (hr == S_OK)
	{
		pvParam.ulVal = wPropCode;
		hr = spMtpParams->Add(&pvParam);
	}

	// Get All Flag
	if (hr == S_OK)
	{
		if(m_nModel >= SDI_MODEL_GH5)
		{
			pvParam.ulVal = wAllFlag;
			hr = spMtpParams->Add(&pvParam);
		}
	}

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	// Send the command to initiate the transfer
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	}

	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [GetDevPropDesc]: 0x%08X\n", hrCmd);
			return hr;
		} 
	}

	// If the transfer was initiated successfully, the driver returns a context cookie
	LPWSTR pwszContext = NULL;
	if (hr == S_OK)
	{
		hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	}

	// The driver indicates how many bytes will be transferred. This is important to
	// retrieve because we have to read all the data that the device sends us (even if it
	// is not the size we were expecting); otherwise, we run the risk of the device going out of sync
	// with the driver.
	ULONG cbReportedDataSize = 0;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, 
			&cbReportedDataSize);
	}

	// Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
	// which suggests the chunk size that the date should be retrieved in. If your application will be 
	// transferring a large amount of data (>256K), use this property to break down the
	// transfer into small chunks so that your app is more responsive.
	// We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)
	{ 
		hr = S_FALSE;
		bSkipDataPhase = TRUE;
	}

	// Use the WPD_COMMAND_MTP_EXT_READ_DATA command to read in the data
	(void) spParameters->Clear();
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_READ_DATA.pid);
	}

	// Specify the same context that we received earlier
	if (hr == S_OK)
	{
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	}

	// Allocate a buffer for the command to read data into - this should 
	// be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
	BYTE* pbBufferIn = NULL;
	if (hr == S_OK)
	{
		pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
		if (pbBufferIn == NULL)
		{
			hr = E_OUTOFMEMORY;
		}
	}

	// Pass the allocated buffer as a parameter
	if (hr == S_OK)
	{
		hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, 
			pbBufferIn, cbReportedDataSize);
	}

	// Specify the number of bytes to transfer as a parameter
	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, 
			cbReportedDataSize);
	}

	// Send the command to transfer the data
	spResults = NULL;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	}

	// Check if the driver was able to transfer the data

	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			return hr;
		}    
	}

	// IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
	// Instead, it is available in the results collection.
	BYTE* pbBufferOut = NULL;
	ULONG cbBytesRead = 0;
	if (hr == S_OK)
	{
		hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);
	}

	// Reset hr to S_OK because we skipped the data phase
	if (hr == S_FALSE && bSkipDataPhase == TRUE)
	{
		hr = S_OK;
	}
	// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
	(void) spParameters->Clear();
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	}

	// Specify the same context that we received earlier
	if (hr == S_OK)
	{
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	}

	// Send the completion command
	spResults = NULL;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	}

	// Check if the driver successfully ended the data transfer
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			return hr;
		}              
	}

	// If the command was executed successfully, check the MTP response code to see if the device
	// can handle the command. If the device cannot handle the command, the data phase would 
	// have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
	// error. 
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	}

	if (hr == S_OK)
	{
		//TRACE("MTP Response code: 0x%X\n", dwResponseCode);
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}
	
	// If the command was handled by the device, return the property value in the BYREF property
	
	if(m_nModel >= SDI_MODEL_GH5)
	{//pana
		if (hr == S_OK)
		{
			if (pbBufferOut != NULL)
			{
				UINT offSet = 0;
				ULONG propCode, dataType, cur_val, data_size, all_flag;

				memcpy(&propCode, &pbBufferOut[offSet], 4); offSet+=4;
				memcpy(&all_flag, &pbBufferOut[offSet], 4); offSet+=4;
				memcpy(&dataType, &pbBufferOut[offSet], 4); offSet+=4;
				memcpy(&cur_val,  &pbBufferOut[offSet], 4); offSet+=4;
				memcpy(&data_size,&pbBufferOut[offSet], 4); offSet+=4;
				//TRACE("propCode : 0x%x\n", propCode);
				//TRACE("dataType : 0x%x\n", dataType);
				//TRACE("get/set  : 0x%x\n", getSet);

				if(all_flag)
				{
					m_DeviceInfoMgr.CleanAll(wPropCode);					
					m_DeviceInfoMgr.SetPropCode(wPropCode);			

					switch(dataType){
					case PTP_DPV_UINT32	:		// UINT32
						{
							m_DeviceInfoMgr.SetType(wPropCode, SDI_TYPE_UINT_32);
							m_DeviceInfoMgr.SetCurrentEnum(wPropCode,cur_val);	

							int nCount = 0;
							if(data_size)
							{
								nCount = data_size/4;
								for(int i=0 ;i<nCount ; i++)
								{
									memcpy(&cur_val, &pbBufferOut[offSet], 4); offSet+=4;
									//TRACE("0x%x\n", form_val);
									//-- 2011-06-09 hongsu.jung
									m_DeviceInfoMgr.AddEnum(wPropCode,cur_val);		
								}
							}
						}
						break;						
					case PTP_DPV_STR:	// STR
						{
							//-- 2011-06-09 hongsu.jung
							m_DeviceInfoMgr.SetType(wPropCode, SDI_TYPE_STRING);

							int current;
							TCHAR *str;

							str = new TCHAR[data_size + 1];
							current=offSet;
							
							for(int i=0 ; i<data_size ; i++) 
							{
								offSet=current+(i*1); // default
								str[i]=pbBufferOut[offSet]; 
							}

							str[data_size]=NULL;
				
							CString temp;
							temp.Format(_T("%s"),str);
							//-- Set Default
							m_DeviceInfoMgr.SetCurrentStr(wPropCode, temp);
							m_DeviceInfoMgr.AddStr(wPropCode, temp);
							//TRACE(_T("Add : %s\n"), temp);
							delete[] str;
						}
						break;
					default:
						break;
					}
				}
				else
				{
					if(dataType == PTP_DPV_UINT32)
					{
						m_DeviceInfoMgr.SetCurrentEnum(wPropCode,cur_val);		
					}
					else
					{
						int current;
						TCHAR *str;

						str = new TCHAR[data_size + 1];
						current=offSet;
							
						for(int i=0;i<data_size;i++) 
						{
							offSet=current+(i*2); // default
							str[i]=pbBufferOut[offSet]; 
						}

						str[data_size]=NULL;
				
						CString temp;
						temp.Format(_T("%s"),str);
						//-- Set Default
						m_DeviceInfoMgr.SetCurrentStr(wPropCode, temp);
						m_DeviceInfoMgr.AddStr(wPropCode, temp);
						//TRACE(_T("Add : %s\n"), temp);
						delete[] str;
					}
				}
			}
			else
			{
				// MTP response code was OK, but no data phase occurred
				hr = E_UNEXPECTED;
			}
		}
	}
	else 
	{ // nX
		if (hr == S_OK)
		{
			if (pbBufferOut != NULL)
			{
				//-- 2011-5-30 Lee JungTaek
				//-- Clean All
				m_DeviceInfoMgr.CleanAll(wPropCode);

				// Plan [5/24/11] jeansu.kim
				// Desc�� ġȯ, unpack �ϸ� �ǰڴ�.
				UINT offSet = 0;
				USHORT propCode, dataType;
				UCHAR getSet;
				memcpy(&propCode, &pbBufferOut[offSet], 2); offSet+=2;
				memcpy(&dataType, &pbBufferOut[offSet], 2); offSet+=2;
				memcpy(&getSet, &pbBufferOut[offSet], 1); offSet+=1;
				//TRACE("propCode : 0x%x\n", propCode);
				//TRACE("dataType : 0x%x\n", dataType);
				//TRACE("get/set  : 0x%x\n", getSet);

				//ULONG cur_val, def_val, form_val;
				ULONG cur_val, def_val, form_val;
				ULONG min_val, max_val, step_val;
				UCHAR formFlag;
				USHORT enum_num;

				UCHAR str_len;
				TCHAR *str;
				cur_val = def_val = form_val = 0x0;
				min_val = max_val = step_val = 0x0;
				CString temp;
				//--2011-7-15 jeansu
				m_DeviceInfoMgr.SetPropCode(wPropCode);			
				//CMiLRe 20141029 White Balnace Detail �������� R/W
				if(PTP_DPC_SAMSUNG_WB_DETAIL_X == wPropCode)
				{
					m_DeviceInfoMgr.SetPropCode_Sub(wPropCode, propCode);
					m_DeviceInfoMgr.CleanAll(propCode);
					m_DeviceInfoMgr.SetPropCode(propCode);
				}

				switch(dataType){
				case PTP_DPV_INT8 :
				case PTP_DPV_UINT8:		// UINT8
					{
						//-- 2011-06-09 hongsu.jung
						switch(dataType)
						{
						case PTP_DPV_INT8:
							m_DeviceInfoMgr.SetType(wPropCode, SDI_TYPE_INT_8);
							break;
						case PTP_DPV_UINT8:
							m_DeviceInfoMgr.SetType(wPropCode, SDI_TYPE_UINT_8);
							break;
						}
					
						memcpy(&def_val, &pbBufferOut[offSet], 1); offSet+=1;
						memcpy(&cur_val, &pbBufferOut[offSet], 1); offSet+=1;
						//-- Set Current Value
						m_DeviceInfoMgr.SetDefaultEnum(wPropCode,def_val);
						m_DeviceInfoMgr.SetCurrentEnum(wPropCode,cur_val);										

						memcpy(&formFlag, &pbBufferOut[offSet], 1); offSet+=1;					
						switch(formFlag){
						case PTP_DPF_NONE:
							break;
						case PTP_DPF_RANGE:
							memcpy(&min_val, &pbBufferOut[offSet], 1); offSet+=1;	//min
							memcpy(&max_val, &pbBufferOut[offSet], 1); offSet+=1;	//max
							memcpy(&step_val, &pbBufferOut[offSet], 1); offSet+=1;	//step
							
							for(int i=min_val ; i<=max_val ; i+= step_val)
								m_DeviceInfoMgr.AddEnum(wPropCode,i);
							break;
						case PTP_DPF_ENUM: 
							memcpy(&enum_num, &pbBufferOut[offSet], 2); offSet+=2;
							for(int i=0;i<enum_num;i++)
							{
								memcpy(&form_val, &pbBufferOut[offSet], 1); offSet+=1;
								//-- 2011-06-09 hongsu.jung
								m_DeviceInfoMgr.AddEnum(wPropCode,form_val);
							}
							break;
						default:
							break;
						}
						break;
					}
				case PTP_DPV_UINT16 :		// UINT16
					{
						//-- 2011-06-09 hongsu.jung
						m_DeviceInfoMgr.SetType(wPropCode, SDI_TYPE_UINT_16);
						//CMiLRe 20141029 White Balnace Detail �������� R/W
						if(PTP_DPC_SAMSUNG_WB_DETAIL_X == wPropCode)
							m_DeviceInfoMgr.SetType(propCode, SDI_TYPE_UINT_16);
						//-- Default Value
						//-- 2011-06-09 hongsu.jung
						memcpy(&def_val, &pbBufferOut[offSet], 2); offSet+=2;					
						memcpy(&cur_val, &pbBufferOut[offSet], 2); offSet+=2;
					
						m_DeviceInfoMgr.SetDefaultEnum(wPropCode,def_val);
						m_DeviceInfoMgr.SetCurrentEnum(wPropCode,cur_val);		

						//CMiLRe 20141029 White Balnace Detail �������� R/W
						if(PTP_DPC_SAMSUNG_WB_DETAIL_X == wPropCode)
						{
							m_DeviceInfoMgr.SetDefaultEnum(propCode,def_val);
							m_DeviceInfoMgr.SetCurrentEnum(propCode,cur_val);		
						}
						//-- Flag
						memcpy(&formFlag, &pbBufferOut[offSet], 1); offSet+=1;					
						switch(formFlag){
						case PTP_DPF_NONE:
							break;
						case PTP_DPF_RANGE:
							memcpy(&min_val, &pbBufferOut[offSet], 2); offSet+=2;	//min
							memcpy(&max_val, &pbBufferOut[offSet], 2); offSet+=2;	//max
							memcpy(&step_val, &pbBufferOut[offSet], 2); offSet+=2;	//step
							
							for(int i=min_val ; i<=max_val ; i+= step_val)
								m_DeviceInfoMgr.AddEnum(wPropCode,i);
							break;
						case PTP_DPF_ENUM:
							//TRACE("EnumForm\n");
							memcpy(&enum_num, &pbBufferOut[offSet], 2); offSet+=2;
							for(int i=0;i<enum_num;i++){
								memcpy(&form_val, &pbBufferOut[offSet], 2); offSet+=2;
								//TRACE("0x%x\n", form_val);
								//-- 2011-06-09 hongsu.jung
								if(wPropCode == PTP_CODE_STILLCAPTUREMODE  && (form_val >= 0x200 && form_val <= 0x1E93))
								{
									m_DeviceInfoMgr.AddEnum(wPropCode,eUCS_CAP_DRIVE_TIMER);
									m_DeviceInfoMgr.AddEnum(wPropCode,eUCS_CAP_DRIVE_TIMER); //�ϳ��� ��� smart panel�� ��ư�� disable �Ǿ �ӽ� ó�� 									
								}
								else
								{
									m_DeviceInfoMgr.AddEnum(wPropCode,form_val);
									//CMiLRe 20141029 White Balnace Detail �������� R/W
									if(PTP_DPC_SAMSUNG_WB_DETAIL_X == wPropCode)
										m_DeviceInfoMgr.AddEnum(propCode,form_val);
								}
							}							
							break;
						default:
							break;
						}
						break;
					}
				case PTP_DPV_UINT32	:		// UINT32
					{
						//-- 2011-06-09 hongsu.jung
						m_DeviceInfoMgr.SetType(wPropCode, SDI_TYPE_UINT_32);

						//-- Default Value
						//-- 2011-06-09 hongsu.jung
						memcpy(&def_val, &pbBufferOut[offSet], 4); offSet+=4;					
						memcpy(&cur_val, &pbBufferOut[offSet], 4); offSet+=4;

						m_DeviceInfoMgr.SetDefaultEnum(wPropCode,def_val);
						m_DeviceInfoMgr.SetCurrentEnum(wPropCode,cur_val);					
						//-- Flag
						memcpy(&formFlag, &pbBufferOut[offSet], 1); offSet+=1;					
						switch(formFlag){
						case PTP_DPF_NONE:
							break;
						case PTP_DPF_RANGE:
							memcpy(&min_val, &pbBufferOut[offSet], 4); offSet+=4;	//min
							memcpy(&max_val, &pbBufferOut[offSet], 4); offSet+=4;	//max
							memcpy(&step_val, &pbBufferOut[offSet], 4); offSet+=4;	//step
							
							for(int i=min_val ; i<=max_val ; i+= step_val)
								m_DeviceInfoMgr.AddEnum(wPropCode,i);
							break;
						case PTP_DPF_ENUM:
							//TRACE("EnumForm\n");
							memcpy(&enum_num, &pbBufferOut[offSet], 2); offSet+=2;
							for(int i=0;i<enum_num;i++){
								memcpy(&form_val, &pbBufferOut[offSet], 4); offSet+=4;
								//TRACE("0x%x\n", form_val);
								//-- 2011-06-09 hongsu.jung
								m_DeviceInfoMgr.AddEnum(wPropCode,form_val);							
							}							
							break;
						default:
							break;
						}
						break;
					}
				case PTP_DPV_STR:	// STR
					{
						//-- 2011-06-09 hongsu.jung
						m_DeviceInfoMgr.SetType(wPropCode, SDI_TYPE_STRING);

						int current;
						memcpy(&str_len, &pbBufferOut[offSet], 1); offSet+=1;
						str = new TCHAR[str_len + 1];
						current=offSet;
						for(int i=0;i<str_len;i++) {
							offSet=current+(i*2); // default
							str[i]=pbBufferOut[offSet]; 
						}
						str[str_len]=NULL;
						temp.Format(_T("%s"),str);
						//-- Set Default
						m_DeviceInfoMgr.SetDefaultStr(wPropCode, temp);
						delete[] str;

						offSet+=2;
						memcpy(&str_len, &pbBufferOut[offSet], 1); offSet+=1;
						str = new TCHAR[str_len + 1];
						current=offSet;
						for(int i=0;i<str_len;i++)
						{
							offSet=current+(i*2); // current
							str[i]=pbBufferOut[offSet]; 
						}
						str[str_len]=NULL;
						offSet+=2; // 
						temp.Format(_T("%s"),str);
						//-- Set Current
						m_DeviceInfoMgr.SetCurrentStr(wPropCode,temp);						
						delete[] str;

						memcpy(&formFlag, &pbBufferOut[offSet], 1); offSet+=1;
						CString temp;
						switch(formFlag){
						case PTP_DPF_ENUM:
							//TRACE("EnumForm Start\n");
							memcpy(&enum_num, &pbBufferOut[offSet], 2); offSet+=2;
							for(int i=0;i<enum_num;i++)
							{
								/*
								memcpy(&str_len, &pbBufferOut[offSet], 1); offSet+=1;
								current=offSet;
								str = new TCHAR[str_len*2 + 1];
								for(int j=0;j<str_len;j++)
								{
									offSet=current+(j*2); // default
									str[j]=pbBufferOut[offSet]; 
								}
								str[str_len]=NULL;
								offSet+=2;
								temp.Format(_T("%s"),str);
								//-- 2011-06-09 hongsu.jung
								m_DeviceInfoMgr.AddStr(wPropCode,temp);			
								delete[] str;
								*/
	
								memcpy(&str_len, &pbBufferOut[offSet], 1); offSet+=1;
								str = new TCHAR[str_len + 1];
								current=offSet;
								for(int i=0;i<str_len;i++) {
									offSet=current+(i*2); // default
									str[i]=pbBufferOut[offSet]; 
								}
								str[str_len]=NULL;
								offSet+=2;
								temp.Format(_T("%s"),str);
								//-- Set Default
								m_DeviceInfoMgr.AddStr(wPropCode, temp);
								//TRACE(_T("Add : %s\n"), temp);
								delete[] str;
							}
							//TRACE("EnumForm End\n");
							break;
						default:
							break;
						}
						break;

					}
				}
			}
			else
			{
				// MTP response code was OK, but no data phase occurred
				hr = E_UNEXPECTED;
			}
		}
	}
	

	// If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
	// property. GetDevicePropValue does not return additional response parameters, so skip this code 
	// If required, you might find that code in the post that covered sending MTP commands without data

	// Free up any allocated memory
	CoTaskMemFree(pbBufferIn);
	CoTaskMemFree(pbBufferOut);
	CoTaskMemFree(pwszContext);

	switch(hr){
	case S_OK:
		return SDI_ERR_OK;
	case E_OUTOFMEMORY:
		return SDI_ERR_OUT_OF_MEMORY;
	case E_UNEXPECTED:
		return SDI_ERR_NO_DATA_PHASE_OCCURED;
	case 0x8007001fL:
		return SDI_ERR_DEVICE_DISCONNECTED;
	default:
		return dwResponseCode;
	}
}

HRESULT CSdiCoreWin7::SetDevPropValue(IPortableDevice* pDevice, WORD wPropCode, CString strValue)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_SETDEVICEPROPVALUE = PTP_OC_SetDevicePropValue;
	const WORD PTP_RESPONSECODE_OK = 0x2001; 
	LPWSTR pwszContext = (LPWSTR)(LPCSTR)(LPCTSTR)strValue;

	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_WRITE.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_WRITE.pid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_SETDEVICEPROPVALUE);
	}

	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		pvParam.ulVal = wPropCode;
		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	BYTE* pbBuffer = NULL;
	ULONGLONG cbBufferSize = 0;

	if (hr == S_OK)
	{
		int nlen = _tcslen(pwszContext);
		if(m_nModel >= SDI_MODEL_GH5)
		{//pana
			if(nlen > 13)
				nlen = 13;
			pbBuffer =  (BYTE*) CoTaskMemAlloc(nlen + 1+sizeof(int));
			//pbBuffer = new BYTE(nlen + 1+sizeof(int));

			wchar_t* str;
			int nlength;
			str = strValue.GetBuffer( strValue.GetLength() );
			nlength = WideCharToMultiByte( CP_ACP, 0, str, -1, NULL, 0, NULL, NULL );

			if(pbBuffer)
			{
				memcpy(pbBuffer, &nlength, sizeof(int));
				
				for(int i=1 ; i<=nlen ; i++)
				{
					memcpy(&pbBuffer[i+3], strValue.GetBuffer(1), 1);
					strValue = strValue.Right(nlen-i);
				}

				//WideCharToMultiByte( CP_ACP, 0, str, -1, (char*)pbBuffer+sizeof(int), nlength, 0, 0 );
				pbBuffer[nlen + sizeof(SdiInt) ] = NULL;
				cbBufferSize = sizeof(SdiInt)+nlen+1;
			}
		}
		else
		{
			pbBuffer =  (BYTE*) CoTaskMemAlloc(nlen * 2);
			if(pbBuffer)
			{
				pbBuffer[0] = nlen;
				memcpy(&pbBuffer[1], pwszContext, nlen * 2 - 1);
				cbBufferSize = nlen * 2;
			}

		}
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedLargeIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, 
			cbBufferSize);
	}

	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	}

	HRESULT hrCmd = S_OK;
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating): 0x%08X\n", hrCmd);
			/*if(pbBuffer)
			{
				delete pbBuffer;
				pbBuffer = NULL;
			}*/
			CoTaskMemFree(pbBuffer);
			return hr;
		} 
	}

	// The driver returns a context cookie that we need to use during our data transfer
	if (hr == S_OK)
	{
		hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	}

	// Use the WPD_COMMAND_MTP_EXT_WRITE_DATA command to send the data
	(void) spParameters->Clear();
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_WRITE_DATA.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_WRITE_DATA.pid);
	}

	// Specify the same context that we received earlier
	if (hr == S_OK)
	{
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	}

	// Specify the number of bytes that arrive with this command. This allows us to 
	// send the data in chunks if required (multiple WRITE_DATA commands). In this case,    //  send the data in a single chunk
	if (hr == S_OK)		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_WRITE, cbBufferSize);
	// Provide the data that needs to be transferred
	if (hr == S_OK)		hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBuffer, cbBufferSize);

	// Send the data to the device
	spResults = NULL;
	if (hr == S_OK)		hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the data was sent successfully by interrogating COMMON_HRESULT
	if (hr == S_OK)		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		TRACE("Driver return code (sending data): 0x%08X\n", hrCmd);
		hr = hrCmd;
	}

	// The driver informs us about the number of bytes that were actually transferred. Normally this
	// should be the same as the number that we provided.
	DWORD cbBytesWritten = 0;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_WRITTEN, 
			&cbBytesWritten);
	}

	// Use the WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER command to signal transfer completion
	(void) spParameters->Clear();
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	}

	// Specify the same context that we received earlier
	if (hr == S_OK)
	{
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	}

	// Send the completion command
	spResults = NULL;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	}

	// Check if the driver successfully ended the data transfer
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			return hr;
		}              
	}

	// If the command was executed successfully, check the MTP response code to see if the
	// device can handle the command and the data. Be aware that there is a distinction between the command
	// and the data being successfully sent to the device and the command and data being handled successfully 
	// by the device
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	}

	if (hr == S_OK)
	{
		//TRACE("MTP Response code: 0x%X\n", dwResponseCode);
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}

	// If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
	// property. SetDevicePropValue does not return additional response parameters, so skip this code 


	// Free up any allocated memory
	CoTaskMemFree(pbBuffer);
	//delete pbBuffer;
	CoTaskMemFree((LPVOID)pwszContext);

	return hr;
}

HRESULT CSdiCoreWin7::SetDevPropValue(IPortableDevice* pDevice, WORD wPropCode, SdiInt nType, SdiInt nValue)
{
	/*if(m_nModel == SDI_MODEL_GH5)
		nType = SDI_TYPE_UINT_32;*/

	TRACE(_T("Code = %x, Type = %x, Value = %x \n"), wPropCode, nType, nValue);
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_SETDEVICEPROPVALUE = PTP_OC_SetDevicePropValue;
    const WORD PTP_RESPONSECODE_OK = PTP_RC_OK;     // PTP_RC_OK indicates command success
	LPWSTR pwszContext = NULL;

	// Build basic WPD parameters for the command
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDeviceValues,
                              NULL,
                              CLSCTX_INPROC_SERVER,
                              IID_IPortableDeviceValues,
                              (VOID**)&spParameters);
    }

    // Use WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_WRITE
    if (hr == S_OK)
    {
        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
                                           WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_WRITE.fmtid);
    }

    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
                                              WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_WRITE.pid);
    }

    // Specify the actual MTP op-code to execute here
    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
                                                      (ULONG) PTP_OPCODE_SETDEVICEPROPVALUE);
    }

    // SetDevicePropValue requires the property code as an MTP parameter
    // MTP parameters need to be first put into a PropVariantCollection
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
                                  NULL,
                                  CLSCTX_INPROC_SERVER,
                                  IID_IPortableDevicePropVariantCollection,
                                  (VOID**)&spMtpParams);
    }

    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;

    // Specify the DateTime property as the MTP parameter
    if (hr == S_OK)
    {
        pvParam.ulVal = wPropCode;
        hr = spMtpParams->Add(&pvParam);
    }

    // Add MTP parameters collection to our main parameter list
    if (hr == S_OK)
    {
        hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
                                          WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    }  

    // Figure out the data to send - in this case it will be an MTP string
    BYTE* pbBuffer = NULL;
    ULONGLONG cbBufferSize = 0;

	char *PropValue;

    if (hr == S_OK)
    {
		switch(nType)
		{
		case SDI_TYPE_INT_8:
			{
				PropValue = new char;
				*((SdiUInt8 *)PropValue) = nValue;
				pbBuffer = (BYTE *)CoTaskMemAlloc(1);
				memcpy(pbBuffer, PropValue, 1);
				cbBufferSize = 1;	
				delete PropValue;
			}
			break;
		case SDI_TYPE_UINT_8:
			{
				PropValue = new char;
				*((SdiUInt8 *)PropValue) = nValue;
				pbBuffer = (BYTE *)CoTaskMemAlloc(1);
				memcpy(pbBuffer, PropValue, 1);
				cbBufferSize = 1;
				delete PropValue;
			}			
			break;
		case SDI_TYPE_UINT_16:
			{
				PropValue = new char[2];
				*((SdiUInt16 *)PropValue) = nValue;
				pbBuffer = (BYTE *)CoTaskMemAlloc(2);
				memcpy(pbBuffer, PropValue, 2);
				cbBufferSize = 2;	
				delete PropValue;
			}
			break;
		case SDI_TYPE_UINT_32:
		case SDI_TYPE_UINT_32_EX:
			{
				PropValue = new char[4];
				*((SdiUInt32 *)PropValue) = nValue;
				pbBuffer = (BYTE *)CoTaskMemAlloc(4);
				memcpy(pbBuffer, PropValue, 4);
				cbBufferSize = 4;	
				delete PropValue;
			}
			break;
		}		
    }

    // Inform the device how much data will arrive - this is a required parameter
    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedLargeIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, 
                                                        cbBufferSize);
    }

    // Send the command to initiate the transfer
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK)
    {
        hr = pDevice->SendCommand(0, spParameters, &spResults);
    }

    // Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
    HRESULT hrCmd = S_OK;
    if (hr == S_OK)
    {
         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    }

    if (hr == S_OK)
    {
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [SetDevPropValue]: 0x%08X\n", hrCmd);
			if(pbBuffer)
			{
				CoTaskMemFree(pbBuffer);
				pbBuffer = NULL;
			}
			return hr;
		} 
    }

    // The driver returns a context cookie that we need to use during our data transfer
    if (hr == S_OK)
    {
         hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
    }

	// Use the WPD_COMMAND_MTP_EXT_WRITE_DATA command to send the data
    (void) spParameters->Clear();
    if (hr == S_OK)
    {
        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
                                           WPD_COMMAND_MTP_EXT_WRITE_DATA.fmtid);
    }

    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
                                                      WPD_COMMAND_MTP_EXT_WRITE_DATA.pid);
    }

    // Specify the same context that we received earlier
    if (hr == S_OK)
    {
        hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
		//hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, dTemp);
    }

    // Specify the number of bytes that arrive with this command. This allows us to 
    // send the data in chunks if required (multiple WRITE_DATA commands). In this case,    //  send the data in a single chunk
    if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_WRITE, cbBufferSize);
	// Provide the data that needs to be transferred
    if (hr == S_OK)        hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBuffer, cbBufferSize);
    // Send the data to the device
    spResults = NULL;
    if (hr == S_OK)        hr = pDevice->SendCommand(0, spParameters, &spResults);
    // Check if the data was sent successfully by interrogating COMMON_HRESULT
    if (hr == S_OK)         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK)
    {
        TRACE("Driver return code (sending data): 0x%08X\n", hrCmd);
        hr = hrCmd;
    }

    // The driver informs us about the number of bytes that were actually transferred. Normally this
    // should be the same as the number that we provided.
    DWORD cbBytesWritten = 0;
    if (hr == S_OK)
    {
        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_WRITTEN, 
                                                &cbBytesWritten);
    }

	// Use the WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER command to signal transfer completion
    (void) spParameters->Clear();
    if (hr == S_OK)
    {
        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
                                           WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
    }

    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
                                                      WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
    }

    // Specify the same context that we received earlier
    if (hr == S_OK)
    {
        hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
		//hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, dTemp);
    }

    // Send the completion command
    spResults = NULL;
    if (hr == S_OK)
    {
        hr = pDevice->SendCommand(0, spParameters, &spResults);
    }

    // Check if the driver successfully ended the data transfer
    if (hr == S_OK)
    {
         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    }

    if (hr == S_OK)
    {
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			return hr;
		}              
    }

    // If the command was executed successfully, check the MTP response code to see if the
    // device can handle the command and the data. Be aware that there is a distinction between the command
    // and the data being successfully sent to the device and the command and data being handled successfully 
    // by the device
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)
    {
        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    }

    if (hr == S_OK)
    {
        //TRACE("MTP Response code: 0x%X\n", dwResponseCode);
        hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
    }
	
	return hr;

}

HRESULT CSdiCoreWin7::SendCheckEvent(IPortableDevice* pDevice, SdiInt32* result, SdiUIntArray* pArList)
{
    HRESULT hr = S_OK;
    const WORD PTP_OPCODE_GETNUMOBJECT = PTP_OC_SAMSUNG_CheckEvent; // GetNumObject opcode is 0x1006
    const WORD PTP_RESPONSECODE_OK = 0x2001;     // 0x2001 indicates command success

	
    // Build basic WPD parameters for the command
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDeviceValues,
                              NULL,
                              CLSCTX_INPROC_SERVER,
                              IID_IPortableDeviceValues,
                              (VOID**)&spParameters);
    }

    // Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
    // Similar commands exist for reading and writing data phases
    if (hr == S_OK)
    {
        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
                                           WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
    }

    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
                                                      WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    }

    // Specify the actual MTP opcode that we want to execute here
    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
                                                      (ULONG) PTP_OPCODE_GETNUMOBJECT);
    }

    // GetNumObject requires three params - storage ID, object format, and parent object handle   
    // Parameters need to be first put into a PropVariantCollection
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
                                  NULL,
                                  CLSCTX_INPROC_SERVER,
                                  IID_IPortableDevicePropVariantCollection,
                                  (VOID**)&spMtpParams);
    }

    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;

    // Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
    // should be changed to use the device's real storage ID (which can be obtained by
    // removing the prefix for the WPD object ID for the storage)
    if (hr == S_OK)
    {
        pvParam.ulVal = 0x10001;
        hr = spMtpParams->Add(&pvParam);
    }

    // Specify object format code parameter. 0x0 can be specified to indicate this is unused 
/*    if (hr == S_OK)
    {
        pvParam.ulVal = 0x0;
        hr = spMtpParams->Add(&pvParam);
    }

    // Specify parent object handle parameter. 0x0 can be specified to indicate this is unused
    if (hr == S_OK)
    {
        pvParam.ulVal = 0x0;
        hr = spMtpParams->Add(&pvParam);
    }
*/
    // Add MTP parameters collection to our main parameter list
    if (hr == S_OK)
    {
        hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
                                          WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    }  


    // Send the command to the MTP device
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK)
    {
		CString strLogs;
		strLogs.Format(_T("PTP_OC_SAMSUNG_CheckEvent\n"));
		//WriteLog(strLogs);
        hr = pDevice->SendCommand(0, spParameters, &spResults);
    } 
    
    // Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
    HRESULT hrCmd = S_OK;
    if (hr == S_OK)
    {
         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    }

    if (hr == S_OK)
    {
        //TRACE("Driver return code: 0x%08X\n", hrCmd);
        hr = hrCmd;
    }

    // If the command was executed successfully, check the MTP response code to see if the
    // device can handle the command. Be aware that there is a distinction between the command
    // being successfully sent to the device and the command being handled successfully by the device.
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)
    {
        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    }

    if (hr == S_OK)
    {
        //TRACE("MTP Response code: 0x%X\n", dwResponseCode);
        hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
    }

	// If the command was executed successfully, the MTP response parameters are returned in 
	// the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS property, which is a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spRespParams;
	if (hr == S_OK)
	{
		hr = spResults->GetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS, 
			&spRespParams);
	}

	// The first response parameter contains the number of objects result
	PROPVARIANT pvResult = {0};
	if (hr == S_OK)
	{
		hr = spRespParams->GetAt(0, &pvResult);
	}

	if (hr == S_OK)
	{
		TRACE("Reported number of objects: %d\n", pvResult.ulVal);
		*result = pvResult.ulVal;
		PropVariantClear(&pvResult); 	// Not really required, but use it for completeness
	}

	if(m_nModel == SDI_MODEL_GH5)
	{
		// The first response parameter contains the number of objects result
		PROPVARIANT pvResult2 = {0};
		if (hr == S_OK)
		{
			hr = spRespParams->GetAt(1, &pvResult2);
		}

		if (hr == S_OK)
		{
			TRACE("Reported number of objects: %d\n", pvResult2.ulVal);
			pArList->SetAt(0, pvResult2.ulVal);
			PropVariantClear(&pvResult2); 	// Not really required, but use it for completeness
		}

		// The first response parameter contains the number of objects result
		PROPVARIANT pvResult3 = {0};
		if (hr == S_OK)
		{
			hr = spRespParams->GetAt(2, &pvResult3);
		}

		if (hr == S_OK)
		{
			TRACE("Reported number of objects: %d\n", pvResult3.ulVal);
			pArList->SetAt(1, pvResult3.ulVal);
			PropVariantClear(&pvResult3); 	// Not really required, but use it for completeness
		}
	}

	if(hr==S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

HRESULT CSdiCoreWin7::SendGetEvent(IPortableDevice* pDevice, BYTE** pData, SdiInt32* size)
{
    HRESULT hr = S_OK;
    const WORD PTP_OPCODE_GETNUMOBJECT = PTP_OC_SAMSUNG_GetEvent; // GetNumObject opcode is 0x1006
    const WORD PTP_RESPONSECODE_OK = 0x2001;     // 0x2001 indicates command success

    // Build basic WPD parameters for the command
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDeviceValues,
                              NULL,
                              CLSCTX_INPROC_SERVER,
                              IID_IPortableDeviceValues,
                              (VOID**)&spParameters);
    }

    // Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
    if (hr == S_OK)
    {
        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
                                           WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
    }

    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
                                              WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
    }

    // Specify the actual MTP opcode to execute here
    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
                                                      (ULONG) PTP_OPCODE_GETNUMOBJECT);
    }

    // GetDevicePropValue requires the property code as an MTP parameter
    // MTP parameters need to be first put into a PropVariantCollection
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)
    {
        hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
                                  NULL,
                                  CLSCTX_INPROC_SERVER,
                                  IID_IPortableDevicePropVariantCollection,
                                  (VOID**)&spMtpParams);
    }


    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;

    // Add MTP parameters collection to our main parameter list
    if (hr == S_OK)
    {
        hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
                                          WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    }  

    // Send the command to initiate the transfer
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK)
    {
        hr = pDevice->SendCommand(0, spParameters, &spResults);
    }

    // Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
    HRESULT hrCmd = S_OK;
    if (hr == S_OK)
    {
         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    }

    if (hr == S_OK)
    {
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [PTP_OC_SAMSUNG_GetEvent]: 0x%08X\n", hrCmd);
			return hr;
		} 
    }

    // If the transfer was initiated successfully, the driver returns a context cookie
    LPWSTR pwszContext = NULL;
    if (hr == S_OK)
    {
         hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
    }

    // The driver indicates how many bytes will be transferred. This is important to
    // retrieve because we have to read all the data that the device sends us (even if it
    // is not the size we were expecting); otherwise, we run the risk of the device going out of sync
    // with the driver.
    ULONG cbReportedDataSize = 0;
    if (hr == S_OK)
    {
        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, 
                                               &cbReportedDataSize);
    }

    // Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
    // which suggests the chunk size that the date should be retrieved in. If your application will be 
    // transferring a large amount of data (>256K), use this property to break down the
    // transfer into small chunks so that your app is more responsive.
    // We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
    BOOL bSkipDataPhase = FALSE;
    if (hr == S_OK && cbReportedDataSize == 0)
    { 
        hr = S_FALSE;
        bSkipDataPhase = TRUE;
    }

    // Use the WPD_COMMAND_MTP_EXT_READ_DATA command to read in the data
    (void) spParameters->Clear();
    if (hr == S_OK)
    {
        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
                                           WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
    }

    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
                                                      WPD_COMMAND_MTP_EXT_READ_DATA.pid);
    }

    // Specify the same context that we received earlier
    if (hr == S_OK)
    {
        hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
    }

    // Allocate a buffer for the command to read data into - this should 
    // be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
    BYTE* pbBufferIn = NULL;
    if (hr == S_OK)
    {
        pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
        if (pbBufferIn == NULL)
        {
            hr = E_OUTOFMEMORY;
        }
    }

    // Pass the allocated buffer as a parameter
    if (hr == S_OK)
    {
        hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, 
                                                        pbBufferIn, cbReportedDataSize);
    }

    // Specify the number of bytes to transfer as a parameter
    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, 
                                                        cbReportedDataSize);
    }

    // Send the command to transfer the data
    spResults = NULL;
    if (hr == S_OK)
    {
        hr = pDevice->SendCommand(0, spParameters, &spResults);
    }

    // Check if the driver was able to transfer the data

    if (hr == S_OK)
    {
         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    }

    if (hr == S_OK)
    {
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			return hr;
		}    
    }

    // IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
    // Instead, it is available in the results collection.
    BYTE* pbBufferOut = NULL;
    ULONG cbBytesRead = 0;
    if (hr == S_OK)
    {
        hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);
    }

    // Reset hr to S_OK because we skipped the data phase
    if (hr == S_FALSE && bSkipDataPhase == TRUE)
    {
        hr = S_OK;
    }
// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
    (void) spParameters->Clear();
    if (hr == S_OK)
    {
        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
                                           WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
    }

    if (hr == S_OK)
    {
        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
                                                      WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
    }

    // Specify the same context that we received earlier
    if (hr == S_OK)
    {
        hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
    }

    // Send the completion command
    spResults = NULL;
    if (hr == S_OK)
    {
        hr = pDevice->SendCommand(0, spParameters, &spResults);
    }

    // Check if the driver successfully ended the data transfer
    if (hr == S_OK)
    {
         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    }

    if (hr == S_OK)
    {
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			return hr;
		}              
    }

    // If the command was executed successfully, check the MTP response code to see if the device
    // can handle the command. If the device cannot handle the command, the data phase would 
    // have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
    // error. 
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)
    {
        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    }

    if (hr == S_OK)
    {
        //TRACE("MTP Response code: 0x%X\n", dwResponseCode);
        hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
    }

    // If the command was handled by the device, return the property value in the BYREF property
    if (hr == S_OK)
    {
        if (pbBufferOut != NULL)
        {
			*pData =(BYTE*) CoTaskMemAlloc(cbReportedDataSize);
			memcpy((BYTE*)*pData,(BYTE*)pbBufferOut,cbReportedDataSize);
			*size = cbReportedDataSize;
        }
        else
        {
            // MTP response code was OK, but no data phase occurred
            hr = E_UNEXPECTED;
        }
    }

    // If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
    // property. GetDevicePropValue does not return additional response parameters, so skip this code 
    // If required, you might find that code in the post that covered sending MTP commands without data

    // Free up any allocated memory
    CoTaskMemFree(pbBufferIn);
    CoTaskMemFree(pbBufferOut);
    CoTaskMemFree(pwszContext);
	
	switch(hr){
	case S_OK:
		return SDI_ERR_OK;
	case E_OUTOFMEMORY:
		return SDI_ERR_OUT_OF_MEMORY;
	case E_UNEXPECTED:
		return SDI_ERR_NO_DATA_PHASE_OCCURED;
	case 0x8007001fL:
		return SDI_ERR_DEVICE_DISCONNECTED;
	default:
		return dwResponseCode;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		SdiFormatDevice / SdiResetDevice / SdiFWUpdate
//! @date		2011-7-18
//! @author		Lee JungTaek
//! @note	 	Control Camera
//------------------------------------------------------------------------------ 
SdiResult CSdiCoreWin7::SdiFormatDevice()
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(FormatDevice(m_pIPortableDevice) == S_OK) return SDI_ERR_OK;
	else return SDI_ERR_ACCESS_DENIED;
}

SdiResult CSdiCoreWin7::SdiResetDevice(int nParam)
{
	//-- 2011-07-23 jeansu : error code change : reset, format�� fw���� Response_OK �� �ѱ�Ƿ� error�� Access Denied �θ� ó��.
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(ResetDevice(m_pIPortableDevice, nParam) == S_OK) return SDI_ERR_OK;
	else return SDI_ERR_ACCESS_DENIED;
}

//dh0.seo 2014-11-04
SdiResult CSdiCoreWin7::SdiSensorCleaningDevice()
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(SensorCleaningDevice(m_pIPortableDevice) == S_OK) return SDI_ERR_OK;
	else return SDI_ERR_ACCESS_DENIED;
}

//CMiLRe 20141113 IntervalCapture Stop �߰�
SdiResult CSdiCoreWin7::SdiIntervalCaptureStop()
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(IntervalCaptureStop(m_pIPortableDevice) == S_OK) return SDI_ERR_OK;
	else return SDI_ERR_ACCESS_DENIED;
}

//CMiLRe 20141127 Display Save Mode ���� Wake up
SdiResult CSdiCoreWin7::SdiDisplaySaveModeWakeUp()
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(DisplaySaveModeWakeUp(m_pIPortableDevice) == S_OK) return SDI_ERR_OK;
	else return SDI_ERR_ACCESS_DENIED;
}

SdiResult CSdiCoreWin7::SdiFWUpdate()
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(FWUpdate(m_pIPortableDevice) == S_OK) return SDI_ERR_OK;
	else return SDI_ERR_ACCESS_DENIED;
}

//20150128 CMiLRe NX3000
//CMiLRe 2015129 �߿��� �ٿ�ε� ��� �߰�
SdiResult CSdiCoreWin7::SdiFWDownload(CString strPath)
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(FWDownload(m_pIPortableDevice, strPath) == S_OK) return SDI_ERR_OK;
	else return SDI_ERR_ACCESS_DENIED;
}

//NX3000
SdiResult CSdiCoreWin7::SdiDeviceReboot()
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(DeviceReboot(m_pIPortableDevice) == S_OK) return SDI_ERR_OK;
	else return SDI_ERR_ACCESS_DENIED;
}

//NX3000
SdiResult CSdiCoreWin7::SdiDevicePowerOff()
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(DevicePowerOff(m_pIPortableDevice) == S_OK) return SDI_ERR_OK;
	else return SDI_ERR_ACCESS_DENIED;
}

SdiResult CSdiCoreWin7::SdiDeviceExportLog()
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(DeviceExportLog(m_pIPortableDevice) == S_OK) return SDI_ERR_OK;
	else return SDI_ERR_ACCESS_DENIED;
}

HRESULT CSdiCoreWin7::FormatDevice(IPortableDevice*  pDevice)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_FORMAT_DEVICE = PTP_OC_SAMSUNG_Format;
	const WORD PTP_RESPONSECODE_OK = PTP_RC_OK;     // 0x2001 indicates command success
	LPWSTR pwszContext = NULL;
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
	// Similar commands exist for reading and writing data phases
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	}

	// Specify the actual MTP opcode that we want to execute here
	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_FORMAT_DEVICE);
	}

	// GetNumObject requires three params - storage ID, object format, and parent object handle   
	// Parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	// Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
	// should be changed to use the device's real storage ID (which can be obtained by
	// removing the prefix for the WPD object ID for the storage)
	if (hr == S_OK)
	{
// 		pvParam.ulVal = nCurPos;
// 		hr = spMtpParams->Add(&pvParam);
	}

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	// Send the command to the MTP device
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	} 

	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		//TRACE("Driver return code: 0x%08X\n", hrCmd);
		hr = hrCmd;
	}

	// If the command was executed successfully, check the MTP response code to see if the
	// device can handle the command. Be aware that there is a distinction between the command
	// being successfully sent to the device and the command being handled successfully by the device.
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	}

	if (hr == S_OK)
	{
		//TRACE("MTP Response code: 0x%X\n", dwResponseCode);
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}
	return hr;
}

HRESULT CSdiCoreWin7::ResetDevice(IPortableDevice*  pDevice, int nParam)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_RESET_DEVICE = PTP_OC_SAMSUNG_Reset;
	const WORD PTP_RESPONSECODE_OK = PTP_RC_OK; 
	LPWSTR pwszContext = NULL;
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_RESET_DEVICE);
	}

	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		 		pvParam.ulVal = nParam;
		 		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	} 

	HRESULT hrCmd = S_OK;
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
	}

	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	}

	if (hr == S_OK)
	{
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}
	return hr;
}

HRESULT CSdiCoreWin7::SensorCleaningDevice(IPortableDevice*  pDevice)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_SENSEORCLEANING_DEVICE = PTP_OC_SAMSUNG_SensorCleaning;
	const WORD PTP_RESPONSECODE_OK = PTP_RC_OK;
	LPWSTR pwszContext = NULL;
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_SENSEORCLEANING_DEVICE);
	}

	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		// 		pvParam.ulVal = nCurPos;
		// 		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	} 

	HRESULT hrCmd = S_OK;
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
	}

	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	}

	if (hr == S_OK)
	{
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}
	return hr;
}

//CMiLRe 20141113 IntervalCapture Stop �߰�
HRESULT CSdiCoreWin7::IntervalCaptureStop(IPortableDevice*  pDevice)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_SENSEORCLEANING_DEVICE = PTP_OC_SAMSUNG_IntervalCaptureStop;
	const WORD PTP_RESPONSECODE_OK = PTP_RC_OK; 
	LPWSTR pwszContext = NULL;
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_SENSEORCLEANING_DEVICE);
	}

	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		// 		pvParam.ulVal = nCurPos;
		// 		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	} 

	HRESULT hrCmd = S_OK;
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
	}

	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	}

	if (hr == S_OK)
	{
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}
	return hr;
}

//CMiLRe 20141127 Display Save Mode ���� Wake up
HRESULT CSdiCoreWin7::DisplaySaveModeWakeUp(IPortableDevice*  pDevice)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_SENSEORCLEANING_DEVICE = PTP_OC_SAMSUNG_DISPLAY_SAVE_MODE_WAKE_UP;
	const WORD PTP_RESPONSECODE_OK = PTP_RC_OK;
	LPWSTR pwszContext = NULL;
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_SENSEORCLEANING_DEVICE);
	}

	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		// 		pvParam.ulVal = nCurPos;
		// 		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	} 

	HRESULT hrCmd = S_OK;
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
	}

	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	}

	if (hr == S_OK)
	{
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}
	return hr;
}


//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-8-23
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
SdiResult CSdiCoreWin7::SdiGetPTPDeviceInfo(PTPDeviceInfo * ptDevInfoType)
{
	return GetPTPDeviceInfo(m_pIPortableDevice, ptDevInfoType);
}

HRESULT CSdiCoreWin7::GetPTPDeviceInfo(IPortableDevice*  pDevice, PTPDeviceInfo * ptDevInfoType)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GET_DEVICEINFO = PTP_OC_GetDeviceInfo;	
	const WORD PTP_RESPONSECODE_OK = 0x2001;     // 0x2001 indicates command success

	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: CoCreateInstance / CLSID_PortableDeviceValues\n"));	
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
	if (hr == S_OK)        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
	// Specify the actual MTP opcode to execute here
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GET_DEVICEINFO);


	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: CoCreateInstance / CLSID_PortableDevicePropVariantCollection\n"));	
	// GetDevicePropValue requires the property code as an MTP parameter
	// MTP parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		pvParam.ulVal = PTP_PROP_BASE;
		hr = spMtpParams->Add(&pvParam);
	}

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)        hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);

	// Send the command to initiate the transfer
	CComPtr<IPortableDeviceValues> spResults;
	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: SendCommand\n"));	
	if (hr == S_OK)        hr = pDevice->SendCommand(0, spParameters, &spResults);  

	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [GetPTPDeviceInfo]: 0x%08X\n", hrCmd);
			return hr;
		} 
	}

	// If the transfer was initiated successfully, the driver returns a context cookie
	LPWSTR pwszContext = NULL;
	if (hr == S_OK)         hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	// The driver indicates how many bytes will be transferred. This is important to
	// retrieve because we have to read all the data that the device sends us (even if it
	// is not the size we were expecting); otherwise, we run the risk of the device going out of sync
	// with the driver.
	ULONG cbReportedDataSize = 0;
	if (hr == S_OK)        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, &cbReportedDataSize);

	// Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
	// which suggests the chunk size that the date should be retrieved in. If your application will be 
	// transferring a large amount of data (>256K), use this property to break down the
	// transfer into small chunks so that your app is more responsive.
	// We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)
	{ 
		hr = S_FALSE;
		bSkipDataPhase = TRUE;
	}

	// Use the WPD_COMMAND_MTP_EXT_READ_DATA command to read in the data
	(void) spParameters->Clear();
	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: SetGuidValue\n"));
	if (hr == S_OK)        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_READ_DATA.pid);
	// Specify the same context that we received earlier
	if (hr == S_OK)        hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   

	// Allocate a buffer for the command to read data into - this should 
	// be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
	BYTE* pbBufferIn = NULL;
	if (hr == S_OK)
	{
		pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
		if (pbBufferIn == NULL)
		{
			hr = E_OUTOFMEMORY;
		}
	}

	// Pass the allocated buffer as a parameter
	if (hr == S_OK)        hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBufferIn, cbReportedDataSize);
	// Specify the number of bytes to transfer as a parameter
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, cbReportedDataSize);
	// Send the command to transfer the data
	TRACE(_T("[SdiCore|Win7] GetFocusPosition :: Send the command to transfer the data\n"));
	spResults = NULL;
	if (hr == S_OK)	        hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver was able to transfer the data
	if (hr == S_OK)         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);

	// IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
	// Instead, it is available in the results collection.
	BYTE* pbBufferOut = NULL;
	ULONG cbBytesRead = 0;
	if (hr == S_OK)        hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);
	// Reset hr to S_OK because we skipped the data phase
	if (hr == S_FALSE && bSkipDataPhase == TRUE)
	{
		hr = S_OK;
	}
	// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
	(void) spParameters->Clear();
	if (hr == S_OK)        hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	if (hr == S_OK)        hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	// Specify the same context that we received earlier
	if (hr == S_OK)        hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	// Send the completion command
	spResults = NULL;
	if (hr == S_OK)        hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver successfully ended the data transfer
	if (hr == S_OK)         hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			return hr;
		}    
	}

	// If the command was executed successfully, check the MTP response code to see if the device
	// can handle the command. If the device cannot handle the command, the data phase would 
	// have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
	// error. 
	DWORD dwResponseCode;
	if (hr == S_OK)        hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK)
	{
		TRACE(_T("[SdiCore|Win7] MTP Response code: 0x%08X\n"), dwResponseCode);
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}

	// If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
	// property. GetDevicePropValue does not return additional response parameters, so skip this code 
	// If required, you might find that code in the post that covered sending MTP commands without data

	// Free up any allocated memory
	CoTaskMemFree(pbBufferIn);
	CoTaskMemFree(pbBufferOut);
	CoTaskMemFree(pwszContext);

	return hr;
}

SdiResult CSdiCoreWin7::SdiInitGH5()
{
	SdiResult result = SDI_ERR_OK;

	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;
	return InitGH5(m_pIPortableDevice);
}

HRESULT CSdiCoreWin7::InitGH5(IPortableDevice* pDevice)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETNUMOBJECT	= 0x90FB;		 // PTP_OC_CUSTOM_LumixInitialize		// 녹화 전 GH5 초기화
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;     // PTP_RC_OK indicates command success


	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
	// Similar commands exist for reading and writing data phases
	if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	// Specify the actual MTP opcode that we want to execute here
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
	// GetNumObject requires three params - storage ID, object format, and parent object handle   
	// Parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;
	// Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
	// should be changed to use the device's real storage ID (which can be obtained by
	// removing the prefix for the WPD object ID for the storage)

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);

	// Send the command to the MTP device
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK) hr = hrCmd;
	// If the command was executed successfully, check the MTP response code to see if the
	// device can handle the command. Be aware that there is a distinction between the command
	// being successfully sent to the device and the command being handled successfully by the device.
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;


	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2013-10-22
//! @author		yongmin
//! @note	 	
//------------------------------------------------------------------------------ 
SdiResult CSdiCoreWin7::SdiGetTick(SdiInt& pcTime, SdiDouble& deviceTime, SdiInt& nCheckCnt, SdiInt& nCheckTime, SdiInt nDSCSyncTime, HWND handle)
{
	CString strLog;
	strLog.Format(_T("SdiGetTick : %s"), m_strUniqueID);
	WriteLog(strLog);

	SdiResult result = SDI_ERR_OK;

	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;
	
	//joonho.kim 170622
	//return GetTick(m_pIPortableDevice, pcTime, deviceTime);
	return GetTick(m_pIPortableDevice, pcTime, deviceTime, nCheckCnt, nCheckTime, nDSCSyncTime, handle);
}

SdiResult CSdiCoreWin7::SdiSetTick(SdiDouble nTick, SdiInt nIntervalSensorOn, SdiUIntArray* pArList)
{
	CString strLog;
	strLog.Format(_T("SdiSetTick : %s"), m_strUniqueID);
	WriteLog(strLog);

	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;	

	SdiFileMgrSetStartTime(nTick, nIntervalSensorOn);
	return SetTick(m_pIPortableDevice,nTick, nIntervalSensorOn, pArList);
}


SdiResult CSdiCoreWin7::SdiMovieCancle()
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_MovieTransferCancle;
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;
    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);

    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
    HRESULT hrCmd = S_OK;
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	CComPtr<IPortableDevicePropVariantCollection> spRespParams;
	if (hr == S_OK)		hr = spResults->GetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS, &spRespParams);
	PROPVARIANT pvResult = {0};
	if (hr == S_OK)		hr = spRespParams->GetAt(0, &pvResult);   
	
	if(hr == S_OK) 
		return (SdiResult)pvResult.ulVal;
	else 
		return dwResponseCode;
}

typedef struct tagLISTEVENTMSG {
	UINT	message;
	UINT	nParam1;
	UINT	nParam2;
	UINT	nParam3;
	LPARAM	pParam;	
	LPARAM	pDest;
	LPARAM	pParent;
} RSEvent;


HRESULT CSdiCoreWin7::GetTick(IPortableDevice* pDevice, SdiInt& pcTime, SdiDouble& deviceTime, SdiInt& nCheckCnt, SdiInt& nCheckTime,  SdiInt nDSCSyncTime, HWND handle)
{
	SdiDouble deviceMinTime = 0;
	SdiInt pcMinTime = 0;
	SdiInt nCheckMinTime = -1;

	WriteLog(_T("GetTick : 1"));

	int nSyncStartTime = GetTickSync();

	WriteLog(_T("GetTick : 2"));
	//-- 2013-10-23 hongsu@esmlab.com
	//-- Get Tick Count on PC  
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETNUMOBJECT = PTP_OC_SAMSUNG_GetTick; // GetNumObject opcode is 0x1006
	const WORD PTP_RESPONSECODE_OK = 0x2001;     // 0x2001 indicates command success
	m_bSyncStop =FALSE;
	
	WriteLog(_T("GetTick : 3"));
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}
	WriteLog(_T("GetTick : 4"));
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
	// Similar commands exist for reading and writing data phases
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	}
	WriteLog(_T("GetTick : 5"));
	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	}
	WriteLog(_T("GetTick : 6"));
	// Specify the actual MTP opcode that we want to execute here
	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_GETNUMOBJECT);
	}
	WriteLog(_T("GetTick : 7"));
	// GetNumObject requires three params - storage ID, object format, and parent object handle   
	// Parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}
	WriteLog(_T("GetTick : 8"));
	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	// Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
	// should be changed to use the device's real storage ID (which can be obtained by
	// removing the prefix for the WPD object ID for the storage)
	if (hr == S_OK)
	{
		pvParam.ulVal = 0x10001;
		hr = spMtpParams->Add(&pvParam);
	}
	WriteLog(_T("GetTick : 9"));
	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  
	WriteLog(_T("GetTick : 10"));
	DWORD dwResponseCode;

#if 0 //joonho.kim 17.06.27 Sync
	int nTry = 2000;
	int nCheckNum = nDSCSyncTime;
#else
	int nTry = MAX_LOOP;
	int nCheckNum = nDSCSyncTime;
#endif

	int nStart = GetTickCount();
	if(nDSCSyncTime > 100)
	{
		SdiDouble dDeviceTime = 0;
		SdiInt nPCTime = 0;
		SdiInt nPCTime2 = 0;
		
		nCheckTime = -1;
		nCheckCnt = 0;
		WriteLog(_T("GetTick : 11"));
		while(TRUE)
		{
			nCheckCnt++;
			// Send the command to the MTP device
			CComPtr<IPortableDeviceValues> spResults;


			//-- 2013-10-23 hongsu@esmlab.com
			//-- Get Tick Count on PC  
			nPCTime = GetTickSync();

			if (hr == S_OK)		hr = pDevice->SendCommand(0, spParameters, &spResults);
			// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
			HRESULT hrCmd = S_OK;
			if (hr == S_OK)		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
			if (hr == S_OK)		hr = hrCmd;
			dwResponseCode = 0x2001;
			if (hr == S_OK)		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
			
			//-- Get Tick
			nPCTime2 = GetTickSync();

			if (hr == S_OK)		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
			CComPtr<IPortableDevicePropVariantCollection> spRespParams;
			if (hr == S_OK)		hr = spResults->GetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS, &spRespParams);
			// The first response parameter contains the number of objects result
			PROPVARIANT pvResult = {0};
			if (hr == S_OK)		hr = spRespParams->GetAt(0, &pvResult);   
			if (hr == S_OK)		
			{
				if(!pvResult.ulVal)
					dDeviceTime = 0 ;
				else
					dDeviceTime = (SdiDouble)pvResult.ulVal;
			}
			else
			{
				dDeviceTime = -1 ;
				hr = S_OK;
				TRACE(_T("[Fail] PC Time[%d][%d] => DSC Time[%lf]\n"),nPCTime, nPCTime2, dDeviceTime);
				int nEnd = GetTickCount();
				if(nEnd-nStart > (nDSCSyncTime/10))
					goto FINISH_SYNC;

				continue;
			}

			//-- 2013-10-23 hongsu@esmlab.com
			//-- Get Tick Count on PC  	
			TRACE(_T("[Try:%d] PC Time[%d][%d]: Gap[%d]  => DSC Time[%lf]\n"),nCheckCnt, nPCTime, nPCTime2, nPCTime2- nPCTime, dDeviceTime);
			
			if((nCheckTime > (nPCTime2-nPCTime) || nCheckTime == -1) && dDeviceTime > 0)
			{
				pcTime = nPCTime;
				deviceTime = dDeviceTime;
				nCheckTime = nPCTime2-nPCTime;
			}

			if( nCheckTime == 1 || ((nPCTime - nSyncStartTime) > nDSCSyncTime) || m_bSyncStop == TRUE)
			{
				pcMinTime = pcTime;
				nCheckMinTime = nCheckTime;
				deviceMinTime = deviceTime;
				goto FINISH_SYNC;
			}

		}
	}
	else
	{
		int nCount = 0;
		WriteLog(_T("GetTick : 12"));
		while(nTry--)
		{
			//-- 2013-10-23 hongsu@esmlab.com
			//-- Get Tick Count on PC  
			pcTime = GetTickSync();
			// Send the command to the MTP device
			CComPtr<IPortableDeviceValues> spResults;
			if (hr == S_OK)		hr = pDevice->SendCommand(0, spParameters, &spResults);
			// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
			HRESULT hrCmd = S_OK;
			if (hr == S_OK)		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
			if (hr == S_OK)		hr = hrCmd;
			dwResponseCode = 0x2001;
			if (hr == S_OK)		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
			//-- Get Tick
			int pcTime2 = GetTickSync();
			if (hr == S_OK)		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
			CComPtr<IPortableDevicePropVariantCollection> spRespParams;
			if (hr == S_OK)		hr = spResults->GetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS, &spRespParams);
			// The first response parameter contains the number of objects result
			PROPVARIANT pvResult = {0};
			if (hr == S_OK)		hr = spRespParams->GetAt(0, &pvResult);   
			if (hr == S_OK)		
			{
				if(!pvResult.ulVal)
					deviceTime = 0 ;
				else
					deviceTime = (SdiDouble)pvResult.ulVal;
			}
			else
			{
				deviceTime = -1 ;
				hr = S_OK;
				TRACE(_T("[Fail] PC Time[%d][%d] => DSC Time[%lf]\n"),pcTime, pcTime2, deviceTime);
				continue;
			}

			if(handle == NULL)// joonho.kim 17.06.27 Sync
			{
				if( nTry <= 480)
					nCheckNum = 15;

				//-- 2013-10-23 hongsu@esmlab.com
				//-- Get Tick Count on PC  	
				TRACE(_T("[Try:%d] PC Time[%d][%d]: Gap[%d]  => DSC Time[%lf]\n"),nTry, pcTime, pcTime2, pcTime2- pcTime, deviceTime);
				switch(m_nModel)
				{
				case SDI_MODEL_NX1000:	
					if (pcTime2-pcTime <= 2) 
						goto FINISH_SYNC;
					break;
				case SDI_MODEL_NX2000:	
					if (pcTime2-pcTime <=  1&& deviceTime > 0) 
						goto FINISH_SYNC;
					break;
				case SDI_MODEL_NX3000:
					if (pcTime2-pcTime <=  1&& deviceTime > 0) 
						goto FINISH_SYNC;
					break;
				case SDI_MODEL_NX1:
				case SDI_MODEL_GH5:
					if (pcTime2-pcTime <=  nCheckNum&& deviceTime > 0) 
					{
						WriteLog(_T("GetTick : 13"));
						TRACE(_T("[Try:%d][nCheckNum:%d] Seccess Tick : %d\r\n"), nTry, nCheckNum, pcTime2-pcTime);

						//joonho.kim 17.06.22 Sync Test
						nCheckCnt = MAX_LOOP - nTry;
						nCheckTime = pcTime2-pcTime;

						//joonho.kim 17.06.29 Sync
						//deviceTime += ((pcTime2-pcTime)/2);
						goto FINISH_SYNC;
					}

					break;
				case SDI_MODEL_NX500:
					if (pcTime2-pcTime <=  nCheckNum&& deviceTime > 0) 
					{
						TRACE(_T("[Try:%d][nCheckNum:%d] Seccess Tick : %d\r\n"), nTry, nCheckNum, pcTime2-pcTime);
						goto FINISH_SYNC;
					}

					break;
				default:
					break;
				}
				if( m_bSyncStop == TRUE)
					break;
			}
			else //Sample Program
			{
				switch(m_nModel)
				{
				case SDI_MODEL_NX1:
				case SDI_MODEL_GH5:
					//if (pcTime2-pcTime <=  nCheckNum&& deviceTime > 0) 
					{
						WriteLog(_T("GetTick : 14"));
						TRACE(_T("[Try:%d][nCheckNum:%d] Seccess Tick : %d\r\n"), nTry, nCheckNum, pcTime2-pcTime);

						//joonho.kim 17.06.22 Sync Test
						nCheckCnt = MAX_LOOP - nTry;
						nCheckTime = pcTime2-pcTime;

						if(nCount == 0)
						{
							pcMinTime = pcTime;
							nCheckMinTime = nCheckTime;
							deviceMinTime = deviceTime;
						}
						else
						{
							if(nCheckTime < nCheckMinTime)
							{
								pcMinTime = pcTime;
								nCheckMinTime = nCheckTime;
								deviceMinTime = deviceTime;
							}
						}

						if (pcTime2-pcTime <=  nCheckNum&& deviceTime > 0) 
							goto FINISH_SYNC;

						nCount++;
						//joonho.kim 17.06.29 Sync
						//deviceTime += ((pcTime2-pcTime)/2);
					}

					break;
				}
			}

			Sleep(5);
			//Sleep(10);

			/*if(handle!=NULL)
			{
				nCheckTime = pcTime2-pcTime;
				RSEvent* pMsgToMain = new RSEvent();
				//-- 2013-04-24 hongsu.jung
				//-- Indicator (message)
				pMsgToMain->message	= WM_SDI_RESULT_GET_TICK;
				pMsgToMain->nParam3 = pcTime2-pcTime;
				::SendMessage(handle, WM_SDI, (WPARAM)WM_SDI_RESULT_GET_TICK, (LPARAM)pMsgToMain);	
			}*/
		}
	}

FINISH_SYNC:
	TRACE(_T("[Finish] PC Time[%d] DSC Time[%lf] gap[%u]\n"),pcTime, deviceTime, nCheckTime);
	//-- Check Try Timing

	
	
	if(handle!=NULL) //Sample Program
	{
		WriteLog(_T("GetTick : 15"));
		RSEvent* pMsgToMain = new RSEvent();
		//-- 2013-04-24 hongsu.jung
		//-- Indicator (message)
		pMsgToMain->message	= WM_SDI_RESULT_GET_TICK;
		pMsgToMain->nParam1 = pcMinTime;
		pMsgToMain->nParam2 = deviceMinTime;
		pMsgToMain->nParam3 = nCheckMinTime;
		pMsgToMain->pParam = (int)MAX_LOOP - (nTry+1);
		CString *pStr = new CString;
		pStr->Format(_T("%s"), m_strFullID);
		pMsgToMain->pDest = (LPARAM)pStr;
		::SendMessage(handle, WM_SDI, (WPARAM)WM_SDI_RESULT_GET_TICK, (LPARAM)pMsgToMain);	

		TRACE(_T("Time %d\m"), deviceMinTime);
	}

	WriteLog(_T("GetTick : 16"));
	if (!nTry)
		return SDI_ERR_SEMAPHORE_TIMEOUT;
	if(hr==S_OK)
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

int CSdiCoreWin7::GetTickSync()
{
	LARGE_INTEGER swEnd;
	QueryPerformanceCounter(&swEnd);
	double m_fTimeforDuration = ((swEnd.QuadPart - m_swStart.QuadPart)/(double)m_swFreq.QuadPart)*1000.f;
	return (int)(m_fTimeforDuration * 10);
}

HRESULT CSdiCoreWin7::SetTick(IPortableDevice* pDevice, SdiDouble nTick, SdiInt nIntervalSensorOn, SdiUIntArray* pArList)
{
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_SetTick; // GetNumObject opcode is 0x1006
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;     // PTP_RC_OK indicates command success

	WriteLog(_T("SetTick : 1"));

    // Build basic WPD parameters for the command
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
    // Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
    // Similar commands exist for reading and writing data phases
	WriteLog(_T("SetTick : 2"));
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    // Specify the actual MTP opcode that we want to execute here
	WriteLog(_T("SetTick : 3"));
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
    // GetNumObject requires three params - storage ID, object format, and parent object handle   
    // Parameters need to be first put into a PropVariantCollection
	WriteLog(_T("SetTick : 4"));
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;
    // Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
    // should be changed to use the device's real storage ID (which can be obtained by
    // removing the prefix for the WPD object ID for the storage)
	if (hr == S_OK)
    {
        pvParam.ulVal = nTick;
		hr = spMtpParams->Add(&pvParam);
    }
	WriteLog(_T("SetTick : 5"));
	if (hr == S_OK)
	{
		if(m_nModel == SDI_MODEL_GH5)
			pvParam.ulVal = sync_frame_time_after_sensor_on_ex;	//9.5us  
		else
			pvParam.ulVal = sync_frame_time_after_sensor_on;	//Timing 
		hr = spMtpParams->Add(&pvParam);
	}
	WriteLog(_T("SetTick : 6"));
	//-- 2014-09-21 hongsu@esmlab.com
	//-- Check Timing between Tick count and Timestamp
	if (hr == S_OK)
	{
		if(m_nModel == SDI_MODEL_GH5)
			pvParam.ulVal = 0;  
		else
			pvParam.ulVal = nIntervalSensorOn * 1000 ;	//Timing (micro second)
		hr = spMtpParams->Add(&pvParam);
	}
	WriteLog(_T("SetTick : 7"));
    // Add MTP parameters collection to our main parameter list
    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    // Send the command to the MTP device
	WriteLog(_T("SetTick : 8"));
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
    // Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	WriteLog(_T("SetTick : 9"));
    HRESULT hrCmd = S_OK;
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;
	WriteLog(_T("SetTick : 10"));
    // If the command was executed successfully, check the MTP response code to see if the
    // device can handle the command. Be aware that there is a distinction between the command
    // being successfully sent to the device and the command being handled successfully by the device.
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	WriteLog(_T("SetTick : 11"));
	switch(m_nModel)
	{
	case SDI_MODEL_GH5:
		{
			DWORD   dwCount = 0;

			CComPtr<IPortableDevicePropVariantCollection> spRespParams;
			if (hr == S_OK)		hr = spResults->GetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS, &spRespParams);
			PROPVARIANT pvResultP1 = {0};	
			PROPVARIANT pvResultP2 = {0};	
			WriteLog(_T("SetTick : 12"));
			if (hr == S_OK)	hr = spRespParams->GetCount(&dwCount);
			WriteLog(_T("SetTick : 13"));
			if (dwCount > 0)
			{
				if (hr == S_OK)		hr = spRespParams->GetAt(0, &pvResultP1);   
				WriteLog(_T("SetTick : 14"));
				if (pvResultP1.ulVal != nTick-1)
				{
					if (hr == S_OK)		hr = spResults->GetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS, &spRespParams);
					if (hr == S_OK)		hr = spRespParams->GetAt(0, &pvResultP1);   
				}
				WriteLog(_T("SetTick : 15"));
			}
			
			if (dwCount > 1)
			{
				if (hr == S_OK)		hr = spRespParams->GetAt(1, &pvResultP2);   
			}
			WriteLog(_T("SetTick : 16"));
			if (pArList != NULL)
			{
				if (pArList->GetSize() > 1)
				{
					pArList->SetAt(0, ((SdiResult)pvResultP1.ulVal));			// response param1 > nTick-1
					pArList->SetAt(1, ((SdiResult)pvResultP2.ulVal));			// respones param2 > settick code
				}
			}
			WriteLog(_T("SetTick : 17"));
		}
	default:
		{
			WriteLog(_T("SetTick : -1"));
		}
		break;
	}

	if (hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

void CSdiCoreWin7::CreateFileManager()
{
	m_pSdiFileMgr = new CSdiCoreFileMgr();
	m_pSdiFileMgr->CreateThread(CREATE_SUSPENDED);	
	m_pSdiFileMgr->ResumeThread() ;
}


HRESULT CSdiCoreWin7::CheckGetFile()
{	
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE			= PTP_OC_SAMSUNG_GetLiveviewImg;		
	const WORD PTP_RESPONSECODE_OK	= PTP_RC_OK;

	
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (VOID**)&spParameters);

	//- WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
	if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID,  WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
	
	// Specify the actual MTP opcode to execute here
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE);
	
	// GetDevicePropValue requires the property code as an MTP parameter
	// MTP parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK) hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDevicePropVariantCollection, (VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;
	if (hr == S_OK)
	{
		pvParam.ulVal = PTP_PROP_BASE;
		hr = spMtpParams->Add(&pvParam);
	}
	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue( WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);




	//------------------------------------------------------------------------------
	//! @brief		1. Driver return code (initiating)
	//------------------------------------------------------------------------------	
	// Send the command to initiate the transfer
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK) TRACE("Driver return code (initiating) [CheckGetFile]: 0x%08X\n", hrCmd);


	// If the transfer was initiated successfully, the driver returns a context cookie
	LPWSTR pwszContext = NULL;
	if (hr == S_OK)	hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);

	// The driver indicates how many bytes will be transferred. This is important to
	// retrieve because we have to read all the data that the device sends us (even if it
	// is not the size we were expecting); otherwise, we run the risk of the device going out of sync
	// with the driver.
	DWORD cbReportedDataSize = 0;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, &cbReportedDataSize);

	// Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
	// which suggests the chunk size that the date should be retrieved in. If your application will be 
	// transferring a large amount of data (>256K), use this property to break down the
	// transfer into small chunks so that your app is more responsive.
	// We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)
	{
		
		return SDI_ERR_NO_DATA_PHASE_OCCURED;
	}

	// Use the WPD_COMMAND_MTP_EXT_READ_DATA command to read in the data
	(void) spParameters->Clear();
	if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_READ_DATA.pid);
	// Specify the same context that we received earlier
	if (hr == S_OK) hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	// Allocate a buffer for the command to read data into - this should 
	// be the same size as the number of bytes we are expecting to read (per chunk, if applicable).

	//-- 2013-12-02 hongsu@esmlab.com
	//-- Check Read Data Size
	TRACE(_T("Driver return code (reading reported data size): %dX\n"), cbReportedDataSize);
	if(!cbReportedDataSize)
	{		
		
		return SDI_ERR_NO_DATA_PHASE_OCCURED;
	}

	BYTE* pbBufferIn = NULL;
	if (hr == S_OK)
	{
		pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
		if (pbBufferIn == NULL) 
		{
			TRACE(_T("[Fail] pbBufferIn:%dX\n"), cbReportedDataSize);
			hr = E_OUTOFMEMORY;
		}
	}

	// Pass the allocated buffer as a parameter
	if (hr == S_OK) hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBufferIn, cbReportedDataSize);
	// Specify the number of bytes to transfer as a parameter
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, cbReportedDataSize);
	// Send the command to transfer the data

	//------------------------------------------------------------------------------
	//! @brief		2. Driver return code (reading data)
	//------------------------------------------------------------------------------	
	spResults = NULL;
	TRACE(_T("[Start] 2. Driver return code (reading data)\n"));
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);
	TRACE(_T("[End] 2. Driver return code (reading data)\n"));
	// Check if the driver was able to transfer the data
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
		if(hrCmd != S_OK)
		{
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			
			return hr;
		}
			




	// IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
	// Instead, it is available in the results collection.
	BYTE* pbBufferOut = NULL;
	ULONG cbBytesRead = 0;
	if (hr == S_OK) hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);
	TRACE(_T("Driver return code (reading data size): %dX\n"), cbBytesRead);
	
	// Reset hr to S_OK because we skipped the data phase
	if (hr == S_FALSE && bSkipDataPhase == TRUE) hr = S_OK;	

	// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
	(void) spParameters->Clear();
	if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);	
	if (hr == S_OK) hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   


	//------------------------------------------------------------------------------
	//! @brief		3. Driver return code (ending transfer)
	//------------------------------------------------------------------------------	
	// Send the completion command
	spResults = NULL;
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver successfully ended the data transfer
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
		if(hrCmd != S_OK)
		{
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			
			return hr;
		}

	// If the command was executed successfully, check the MTP response code to see if the device
	// can handle the command. If the device cannot handle the command, the data phase would 
	// have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
	// error. 
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK) hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK)
	{
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
		if(hr == E_FAIL)
			TRACE("MTP Response code: 0x%X\n", dwResponseCode);
	}

	// If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
	// property. GetDevicePropValue does not return additional response parameters, so skip this code 
	// If required, you might find that code in the post that covered sending MTP commands without data
	// Free up any allocated memory
	CoTaskMemFree(pbBufferIn);
	CoTaskMemFree(pbBufferOut);
	CoTaskMemFree(pwszContext);

	
	if(hr == S_OK)
		return SDI_ERR_OK;

	return hr;
}

void ThreadCheckConnection(void *param)
{
	TRACE(_T(">>> [Thread] Check Connection\n"));
	CSdiCoreWin7* pThis = (CSdiCoreWin7*)param;	
	//-- Set FALSE
	pThis->m_bCheckSemaphore = FALSE;

	HRESULT result = pThis->CheckGetFile();

	if(result != SDI_ERR_OK)
		pThis->m_bCheckSemaphore = FALSE;
	else
		pThis->m_bCheckSemaphore = TRUE;

	_endthread();
}

SdiBool CSdiCoreWin7::SdiCheckGetFile()
{
	BOOL bRet = FALSE;
	if(m_pIPortableDevice==NULL) 
		return bRet;

	//-- 2013-12-01 hongsu@esmlab.com
	//-- Check Semaphore (Get Liveview Once)
	_beginthread( ThreadCheckConnection, 0, this );

	//-- 2013-12-01 hongsu@esmlab.com
	//-- Wait TimeOut 
	Sleep(1000);

	if (!m_bCheckSemaphore)
	{
		TRACE("\n\n\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
		TRACE(">>> [Check] Get Liveview Image File Once : FAIL\n");
		TRACE(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n\n\n\n\n");
		//-- Close Command
		//AfxMessageBox(_T("File Transfer Fail\nPlease Reboot Camera"));
		m_pIPortableDevice->Cancel();
	}
	else
		TRACE(">>>\n>>> [Check] Get Liveview Image File Once : TRUE\n>>>\n");

	return m_bCheckSemaphore;
}

SdiBool CSdiCoreWin7::SdiFileMgrSetStartTime(int nStartTime, int nIntervalSensorOn)
{
	if(m_pSdiFileMgr)
	{
		m_pSdiFileMgr->m_nStartTime = nStartTime;
		m_pSdiFileMgr->m_nIntervalSensorOn = nIntervalSensorOn;
		return TRUE;
	}

	return FALSE;
}

SdiResult CSdiCoreWin7::SdiFileMgrGetEndTime()
{
	return m_pSdiFileMgr->m_nPrevTimeStamp;
}

SdiBool CSdiCoreWin7::SdiSetResumeFrame(int nResumeFrame)
{
	if(m_pSdiFileMgr)
	{
		m_pSdiFileMgr->m_nResumeFrame = nResumeFrame;
		return TRUE;
	}

	return FALSE;
}

SdiResult CSdiCoreWin7::SdiGetResumeFrame()
{
	return m_pSdiFileMgr->m_nResumeFrame;
}

SdiBool CSdiCoreWin7::SdiSetResumeMode(BOOL bResumeMode)
{
	if(m_pSdiFileMgr)
	{
		m_pSdiFileMgr->m_nResumeMode = bResumeMode;
		return TRUE;
	}

	return FALSE;
}

SdiBool CSdiCoreWin7::SdiGetResumeMode()
{
	return m_pSdiFileMgr->m_nResumeMode;
}

void CSdiCoreWin7::SdiSyncStop()
{
	m_bSyncStop = TRUE;
}


void CSdiCoreWin7::SetFps(int nFps)
{
	m_pSdiFileMgr->SetFps(nFps);
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2013-10-22
//! @author		yongmin
//! @note	 	
//------------------------------------------------------------------------------ 
SdiResult CSdiCoreWin7::SdiHiddenCommand(SdiInt32 nCommand, SdiInt32 nValue)
{
	SdiResult result = SDI_ERR_OK;

	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;
	return SetHiddenCommand(m_pIPortableDevice, nCommand, nValue);
}

SdiResult CSdiCoreWin7::SdiFocusSave()
{
	SdiResult result = SDI_ERR_OK;

	if(m_pIPortableDevice==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;
	return SetFocusSave(m_pIPortableDevice);
}

HRESULT CSdiCoreWin7::SetFocusSave(IPortableDevice* pDevice)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETNUMOBJECT	= 0x9013; // GetNumObject opcode is 0x1006
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;     // PTP_RC_OK indicates command success


	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
	// Similar commands exist for reading and writing data phases
	if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	// Specify the actual MTP opcode that we want to execute here
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
	// GetNumObject requires three params - storage ID, object format, and parent object handle   
	// Parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;
	// Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
	// should be changed to use the device's real storage ID (which can be obtained by
	// removing the prefix for the WPD object ID for the storage)
	
	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);

	// Send the command to the MTP device
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK) hr = hrCmd;
	// If the command was executed successfully, check the MTP response code to see if the
	// device can handle the command. Be aware that there is a distinction between the command
	// being successfully sent to the device and the command being handled successfully by the device.
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;


	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

HRESULT CSdiCoreWin7::SetHiddenCommand(IPortableDevice* pDevice, SdiInt32 nCommand, SdiInt32 nValue)
{
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_HiddenCommand; // GetNumObject opcode is 0x1006
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;     // PTP_RC_OK indicates command success

	
    // Build basic WPD parameters for the command
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
    // Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
    // Similar commands exist for reading and writing data phases
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    // Specify the actual MTP opcode that we want to execute here
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
    // GetNumObject requires three params - storage ID, object format, and parent object handle   
    // Parameters need to be first put into a PropVariantCollection
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;
    // Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
    // should be changed to use the device's real storage ID (which can be obtained by
    // removing the prefix for the WPD object ID for the storage)
    if (hr == S_OK)
    {
        pvParam.ulVal = nCommand;
		hr = spMtpParams->Add(&pvParam);
    }

	if (hr == S_OK)
    {
        pvParam.ulVal = nValue;
        hr = spMtpParams->Add(&pvParam);
    }

    // Add MTP parameters collection to our main parameter list
    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);

    // Send the command to the MTP device
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
    // Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
    HRESULT hrCmd = S_OK;
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;
    // If the command was executed successfully, check the MTP response code to see if the
    // device can handle the command. Be aware that there is a distinction between the command
    // being successfully sent to the device and the command being handled successfully by the device.
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	
	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

SdiResult CSdiCoreWin7::SdiGetRecordStatus()
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_GetRecordStatus; // GetNumObject opcode is 0x1006
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;     // PTP_RC_OK indicates command success

	
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
	// Similar commands exist for reading and writing data phases
	if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	// Specify the actual MTP opcode that we want to execute here
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
	// GetNumObject requires three params - storage ID, object format, and parent object handle   
	// Parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;
	// Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
	// should be changed to use the device's real storage ID (which can be obtained by
	// removing the prefix for the WPD object ID for the storage)

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);

	// Send the command to the MTP device
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK) hr = hrCmd;
	// If the command was executed successfully, check the MTP response code to see if the
	// device can handle the command. Be aware that there is a distinction between the command
	// being successfully sent to the device and the command being handled successfully by the device.
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	CComPtr<IPortableDevicePropVariantCollection> spRespParams;
	if (hr == S_OK)		hr = spResults->GetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS, &spRespParams);
	// The first response parameter contains the number of objects result
	PROPVARIANT pvResult = {0};
	if (hr == S_OK)		hr = spRespParams->GetAt(0, &pvResult);   

	
	if(hr == S_OK) 
		return (SdiResult)pvResult.ulVal;
	else 
		return dwResponseCode;
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-09-08
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
SdiResult CSdiCoreWin7::SdiFileTransfer(char* pPath, int nfilenum, int nmdat, int nOffset,int* nFileSize, int nMovieSize)
{
	// [9/9/2014 ESMLab]
	// Debug
	CString strFile;
	strFile = (TCHAR*)pPath;
	delete[] pPath;
// 	CString strLog;
// 	strLog.Format(_T("SdiFileTransfer : [%s][%d][%d][%d]\n"), strFile, nfilenum, nmdat, nOffset);
// 	TRACE(strLog);
	//TRACE(_T("##[%s][%d] mdat[%d] SdiFileTransfer [Start]\n"), strFile, nfilenum, nmdat);


	IPortableDevice* pDevice = m_pIPortableDevice;	
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_CAPTURE	= PTP_OC_SAMSUNG_FileTransfer;    
	const WORD PTP_RESPONSECODE_OK	= PTP_RC_OK;     // 0x2001 indicates command success
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (VOID**)&spParameters);
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
	if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
	// Specify the actual MTP opcode to execute here
	if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_CAPTURE);
	// GetDevicePropValue requires the property code as an MTP parameter
	// MTP parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		pvParam.ulVal = nfilenum;
		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
	{
		pvParam.ulVal = nmdat;
		hr = spMtpParams->Add(&pvParam);
	}
	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	// Send the command to initiate the transfer
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	else			TRACE(_T(" CaptureCompleteExec() : SendCommand ERROR : 0x%08x\n"), hr);
	if (hr == S_OK)
	{	
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("****************************************************?\n");
			TRACE("**[%s][%d] mdat[%d] [ERROR] Fail Transfer [Retry]\n", strFile, nfilenum, nmdat);
			TRACE("****************************************************?\n");
			return hr;			
			//return SdiFileTransfer(pPath, nfilenum, nmdat, nOffset,nFileSize);
		} 
	}
	// If the transfer was initiated successfully, the driver returns a context cookie
	LPWSTR pwszContext = NULL;
	if (hr == S_OK)	hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	// The driver indicates how many bytes will be transferred. This is important to
	// retrieve because we have to read all the data that the device sends us (even if it
	// is not the size we were expecting); otherwise, we run the risk of the device going out of sync
	// with the driver.
	ULONG cbReportedDataSize = 0;
	ULONG cbReportedChunkDataSize = 0;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE ,   &cbReportedDataSize);
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE ,  &cbReportedChunkDataSize);
	// Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
	// which suggests the chunk size that the date should be retrieved in. If your application will be 
	// transferring a large amount of data (>256K), use this property to break down the
	// transfer into small chunks so that your app is more responsive.
	// We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)  {  hr = S_FALSE; bSkipDataPhase = TRUE; }
	BYTE* pbBufferIn = NULL;
	BYTE* pbBufferOut = NULL;
	BYTE* pbBufferTotal = NULL;	
	pbBufferTotal = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
	if(cbReportedDataSize==0 || cbReportedChunkDataSize==0)
	{
		TRACE("**[%s][%d] mdat[%d] [ERROR] [ERROR] CaptureCompleteExec() : Data/Chunk Size : %d/%d\n", strFile, nfilenum, nmdat, cbReportedDataSize, cbReportedChunkDataSize);
		CoTaskMemFree(pbBufferTotal);			
		//return SDI_ERR_DEVICE_DISCONNECTED;
	}


	//-- 2014-09-08 hongsu@esmlab.com
	//-- Buffer In Size 
	ULONG lDivBufSize;
	ULONG lDivBufTotSize = 0;
	int nDivideCnt = cbReportedDataSize/cbReportedChunkDataSize;
	TRACE(_T("nDivideCnt = %d, cbReportedDataSize = %u, cbReportedChunkDataSize = %u\n"), nDivideCnt, cbReportedDataSize, cbReportedChunkDataSize);

	for(int i=0 ; i<= nDivideCnt ;i++)
	{
		if( i != nDivideCnt)	lDivBufSize = cbReportedChunkDataSize;
		else					lDivBufSize = cbReportedDataSize%cbReportedChunkDataSize;			

		// Use the WPD_COMMAND_MTP_EXT_READ_DATA command to read in the data
		(void) spParameters->Clear();
		if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
		if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_READ_DATA.pid);		
		if (hr == S_OK)	hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
		// Allocate a buffer for the command to read data into - this should 
		// be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
		if (hr == S_OK)
		{
			pbBufferIn = NULL;
			pbBufferIn = (BYTE*) CoTaskMemAlloc(lDivBufSize);
			if (pbBufferIn == NULL)
				hr = E_OUTOFMEMORY;
		}

		// Pass the allocated buffer as a parameter
		if (hr == S_OK)	hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBufferIn, lDivBufSize);
		// Specify the number of bytes to transfer as a parameter
		if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, lDivBufSize);
		// Send the command to transfer the data
		spResults = NULL;
		if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
		// Check if the driver was able to transfer the data
		if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
		if (hr == S_OK) hr = hrCmd;
		
		ULONG cbBytesRead = 0;
		pbBufferOut = NULL;
		if (hr == S_OK)	hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);				
		//-- 2014-09-08 hongsu@esmlab.com
		//-- Check Read/Buf Size
		if(cbBytesRead != lDivBufSize)
		{
			TRACE("**[%s][%d] mdat[%d] [ERROR] Get File Transfer ExpectedByte[%d]=>ReadByte[%d]\n", strFile, nfilenum, nmdat, lDivBufSize,cbBytesRead);
		}

		if (pbBufferOut != NULL)
		{
			//TRACE(_T("##[%s][%d] File Get [%d/%d] [%d] [%d][%d]\n"), strFile, nfilenum, i,nDivideCnt,pbBufferTotal+(i*cbReportedChunkDataSize),lDivBufSize,cbBytesRead);
			memcpy(pbBufferTotal+(i*cbReportedChunkDataSize),pbBufferOut,lDivBufSize);
			lDivBufTotSize += lDivBufSize;
		}
		else
		{
			TRACE("[Error]!! Get Buffer Fail strFile[%s], nfilenum[%d] ExpectedByte[%d]=>ReadByte[%d] \n", strFile, nfilenum, nmdat, lDivBufSize,cbBytesRead);
			CoTaskMemFree(pbBufferIn);
			break;
		}

		CoTaskMemFree(pbBufferIn);
		CoTaskMemFree(pbBufferOut);
	}


	//-- 2014-09-09 hongsu@esmlab.com
	//-- Check Size
	if(lDivBufTotSize != cbReportedDataSize)
	{
		TRACE("**[%s][%d] [ERROR] cbReportedDataSize[%d] lDivBufTotSize[%d]#\n#\n#\n#\n",strFile, nfilenum, cbReportedDataSize,lDivBufTotSize);
		//2019.08.24 joonho.kim size error return
		return hr;
	}

	//-- 2014/09/08 hongsu
	//-- Save File
	/*if(cbReportedDataSize < 2*1024*1024)
	{
		CString strError;
		strError.Format(_T("%d Size error"),  cbReportedDataSize);
		AfxMessageBox(strError);
	}*/
	if(cbReportedDataSize)
	{
		*nFileSize = cbReportedDataSize;
		SdiFileInfo* pFileInfo = new SdiFileInfo;
		pFileInfo->pImageBuffer = NULL;
		_tcscpy_s(pFileInfo->strImageFileName, strFile);

		pFileInfo->nImageSize		= cbReportedDataSize;
		pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize];
		memcpy(pFileInfo->pImageBuffer,pbBufferTotal,cbReportedDataSize);
		// TRACE(_T("##[%s][%d] File Save cbReportedDataSize[%d] -> ImageSize[%d] mdat[%d]\n"), strFile, nfilenum, cbReportedDataSize,pFileInfo->nImageSize,nmdat);			

		if(m_nModel == SDI_MODEL_GH5)
		{
			pFileInfo->nAppendMsg = WM_SDI_OP_FILE_TRANSFER;
			pFileInfo->nMovieSize = nMovieSize;
			pFileInfo->nFileNum = nfilenum;
			pFileInfo->nmdat = nmdat;
			pFileInfo->nOffset = nOffset;
			pFileInfo->nModel = m_nModel;

			m_pSdiFileMgr->AddFile(pFileInfo);
		}
		else //NX Series
		{
			if(*(char*)(pFileInfo->pImageBuffer+4) == 'f')
			{
				if(*(char*)(pFileInfo->pImageBuffer+5) == 't')
				{
					if(*(char*)(pFileInfo->pImageBuffer+6) == 'y')
					{
						if(*(char*)(pFileInfo->pImageBuffer+7) == 'p')
						{
							pFileInfo->nAppendMsg = WM_SDI_OP_FILE_TRANSFER;
							pFileInfo->nMovieSize = nMovieSize;
							pFileInfo->nFileNum = nfilenum;
							pFileInfo->nmdat = nmdat;
							pFileInfo->nOffset = nOffset;
							pFileInfo->nModel = m_nModel;

							m_pSdiFileMgr->AddFile(pFileInfo);
						}
						else
						{
							TRACE(_T("File Data Error"));
						}
					}
				}
			}
		}
		

		
		
		//-- 2013-11-22 hongsu@esmlab.com
		//-- Memory Leak
		CoTaskMemFree(pbBufferTotal);
	}

	// Reset hr to S_OK because we skipped the data phase
	if (hr == S_FALSE && bSkipDataPhase == TRUE)	hr = S_OK;
	// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
	(void) spParameters->Clear();
	if (hr != S_OK) TRACE(_T(" spParameters Clear() Error\n"));
	if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	// Specify the same context that we received earlier
	if (hr == S_OK) hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	// Send the completion command
	spResults = NULL;
	if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver successfully ended the data transfer
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK) hr = hrCmd;

	// If the command was executed successfully, check the MTP response code to see if the device
	// can handle the command. If the device cannot handle the command, the data phase would 
	// have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
	// error. 
	DWORD dwResponseCode = 0x0;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK)	hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;// Free up any allocated memory
	CoTaskMemFree(pwszContext);	


	//TRACE(_T("##[%s][%d] mdat[%d] SdiFileTransfer [End]\n"), strFile, nfilenum, nmdat);
		
	if (hr == S_OK) 
		return SDI_ERR_OK;
	else if(hr==0x80070079){
		TRACE(_T("[ERROR] SdiFileTransfer() : ERROR_SEM_TIMEOUT !!!\n"));
		return SDI_ERR_SEMAPHORE_TIMEOUT;
	}
	else 
		return dwResponseCode;
}


SdiResult CSdiCoreWin7::SdiImageReceiveComplete()
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_FileReceiveComplete; // GetNumObject opcode is 0x1006
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;     // PTP_RC_OK indicates command success
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
	// Similar commands exist for reading and writing data phases
	if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	// Specify the actual MTP opcode that we want to execute here
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
	// GetNumObject requires three params - storage ID, object format, and parent object handle   
	// Parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;
	// Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
	// should be changed to use the device's real storage ID (which can be obtained by
	// removing the prefix for the WPD object ID for the storage)

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	// Send the command to the MTP device
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK) hr = hrCmd;
	// If the command was executed successfully, check the MTP response code to see if the
	// device can handle the command. Be aware that there is a distinction between the command
	// being successfully sent to the device and the command being handled successfully by the device.
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}


HRESULT CSdiCoreWin7::DeviceReboot(IPortableDevice*  pDevice)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_FORMAT_DEVICE = PTP_OC_SAMSUNG_POWER_REBOOT;
	const WORD PTP_RESPONSECODE_OK = PTP_RC_OK; 
	LPWSTR pwszContext = NULL;
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_FORMAT_DEVICE);
	}

	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	} 

	return hr;
}

HRESULT CSdiCoreWin7::HalfShutter(IPortableDevice*  pDevice, int nRelease)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_FORMAT_DEVICE = PTP_OC_CUSTOM_HalfShutter;
	const WORD PTP_RESPONSECODE_OK = PTP_RC_OK; 
	LPWSTR pwszContext = NULL;
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_FORMAT_DEVICE);
	}

	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		pvParam.ulVal = nRelease;
		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	} 

	return hr;
}

HRESULT CSdiCoreWin7::DeviceExportLog(IPortableDevice*  pDevice)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_FORMAT_DEVICE = PTP_OC_CUSTOM_ExportLog;
	const WORD PTP_RESPONSECODE_OK = PTP_RC_OK; 
	LPWSTR pwszContext = NULL;
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_FORMAT_DEVICE);
	}

	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	} 

	return hr;
}

HRESULT CSdiCoreWin7::DevicePowerOff(IPortableDevice*  pDevice)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_FORMAT_DEVICE = PTP_OC_SAMSUNG_POWER_OFF;
	const WORD PTP_RESPONSECODE_OK = PTP_RC_OK; 
	LPWSTR pwszContext = NULL;
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_FORMAT_DEVICE);
	}

	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	} 

	return hr;
}
HRESULT CSdiCoreWin7::FWUpdate(IPortableDevice*  pDevice)
{
		HRESULT hr = S_OK;
		const WORD PTP_OPCODE_FORMAT_DEVICE = PTP_OC_SAMSUNG_FWUpdate;
		const WORD PTP_RESPONSECODE_OK = PTP_RC_OK;
		LPWSTR pwszContext = NULL;
		CComPtr<IPortableDeviceValues> spParameters;
		if (hr == S_OK)
		{
			hr = CoCreateInstance(CLSID_PortableDeviceValues,
				NULL,
				CLSCTX_INPROC_SERVER,
				IID_IPortableDeviceValues,
				(VOID**)&spParameters);
		}

		// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
		// Similar commands exist for reading and writing data phases
		if (hr == S_OK)
		{
			hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
				WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
		}

		if (hr == S_OK)
		{
			hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
				WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
		}

		// Specify the actual MTP opcode that we want to execute here
		if (hr == S_OK)
		{
			hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
				(ULONG) PTP_OPCODE_FORMAT_DEVICE);
		}

		// GetNumObject requires three params - storage ID, object format, and parent object handle   
		// Parameters need to be first put into a PropVariantCollection
		CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
		if (hr == S_OK)
		{
			hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
				NULL,
				CLSCTX_INPROC_SERVER,
				IID_IPortableDevicePropVariantCollection,
				(VOID**)&spMtpParams);
		}

		PROPVARIANT pvParam = {0};
		pvParam.vt = VT_UI4;

		// Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
		// should be changed to use the device's real storage ID (which can be obtained by
		// removing the prefix for the WPD object ID for the storage)
		if (hr == S_OK)
		{
			// 		pvParam.ulVal = nCurPos;
			// 		hr = spMtpParams->Add(&pvParam);
		}

		// Add MTP parameters collection to our main parameter list
		if (hr == S_OK)
		{
			hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
				WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
		}  

		// Send the command to the MTP device
		CComPtr<IPortableDeviceValues> spResults;
		if (hr == S_OK)
		{
			hr = pDevice->SendCommand(0, spParameters, &spResults);
		} 

		// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
		HRESULT hrCmd = S_OK;
		if (hr == S_OK)
		{
			hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
		}

		if (hr == S_OK)
		{
			//TRACE("Driver return code: 0x%08X\n", hrCmd);
			hr = hrCmd;
		}

		// If the command was executed successfully, check the MTP response code to see if the
		// device can handle the command. Be aware that there is a distinction between the command
		// being successfully sent to the device and the command being handled successfully by the device.
		DWORD dwResponseCode = 0x2001;
		if (hr == S_OK)
		{
			hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
		}

		if (hr == S_OK)
		{
			//TRACE("MTP Response code: 0x%X\n", dwResponseCode);
			hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
		}
		return hr;
}
	
//20150128 CMiLRe NX3000
//CMiLRe 2015129 �߿��� �ٿ�ε� ��� �߰�
HRESULT CSdiCoreWin7::FWDownload(IPortableDevice*  pDevice, CString strPath)
{	
	HRESULT hr = S_OK;
	EnterCriticalSection (m_CriSection);
	CFile file;
    if (!file.Open(strPath, CFile::modeRead))
	{
		return hr;
	}
	INT iLength = (INT)(file.GetLength());
	BYTE* pBuffer = new BYTE[iLength];
	file.Read(pBuffer, iLength);
	file.Close();
	LeaveCriticalSection (m_CriSection);

	const WORD PTP_OPCODE_CAPTURE = PTP_OC_SAMSUNG_FW_DOWNLOAD;
    const WORD PTP_RESPONSECODE_OK = PTP_RC_OK;

	LPWSTR pwszContext = (LPWSTR)pBuffer;

	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	// Use WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_WRITE
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_WRITE.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_WRITE.pid);
	}

	// Specify the actual MTP op-code to execute here
	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OC_SAMSUNG_FW_DOWNLOAD);
	}

	// SetDevicePropValue requires the property code as an MTP parameter
	// MTP parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	// Specify the DateTime property as the MTP parameter
	if (hr == S_OK)
	{
		pvParam.ulVal = PTP_OC_SAMSUNG_FW_DOWNLOAD;
		hr = spMtpParams->Add(&pvParam);
	}

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	// Figure out the data to send - in this case it will be an MTP string
	BYTE* pbBuffer = NULL;
	ULONGLONG cbBufferSize = 0;

	if (hr == S_OK)
	{
		int nlen = iLength;//_tcslen(pwszContext);
		pbBuffer =  (BYTE*) CoTaskMemAlloc(nlen);
		pbBuffer[0] = nlen;
		memcpy(&pbBuffer[0], pwszContext, nlen);
		cbBufferSize = nlen;
	}

	// Inform the device how much data will arrive - this is a required parameter
	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedLargeIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, 
			cbBufferSize);
	}

	// Send the command to initiate the transfer
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	}

	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [SetDevPropValue]: 0x%08X\n", hrCmd);
			return hr;
		} 
	}

	// The driver returns a context cookie that we need to use during our data transfer
	if (hr == S_OK)
	{
		hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
		//hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT,(LPWSTR*)pbBuffer);
		//hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, *pBuffer, 
		
	}

	// Use the WPD_COMMAND_MTP_EXT_WRITE_DATA command to send the data
	(void) spParameters->Clear();
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_WRITE_DATA.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_WRITE_DATA.pid);
	}

	// Specify the same context that we received earlier
	if (hr == S_OK)
	{
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
		//hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, (LPCWSTR)pbBuffer);   
	}

	// Specify the number of bytes that arrive with this command. This allows us to 
	// send the data in chunks if required (multiple WRITE_DATA commands). In this case,    //  send the data in a single chunk
	if (hr == S_OK)		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_WRITE, cbBufferSize);
	// Provide the data that needs to be transferred
	if (hr == S_OK)		hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBuffer, cbBufferSize);

	// Send the data to the device
	spResults = NULL;
	if (hr == S_OK)		hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the data was sent successfully by interrogating COMMON_HRESULT
	if (hr == S_OK)		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		TRACE("Driver return code (sending data): 0x%08X\n", hrCmd);
		hr = hrCmd;
	}

	// The driver informs us about the number of bytes that were actually transferred. Normally this
	// should be the same as the number that we provided.
	DWORD cbBytesWritten = 0;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_WRITTEN, 
			&cbBytesWritten);
	}

	// Use the WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER command to signal transfer completion
	(void) spParameters->Clear();
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	}

	// Specify the same context that we received earlier
	if (hr == S_OK)
	{
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
		//hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, (LPCWSTR)pbBuffer);   
	}

	// Send the completion command
	spResults = NULL;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	}

	// Check if the driver successfully ended the data transfer
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			return hr;
		}              
	}

	// If the command was executed successfully, check the MTP response code to see if the
	// device can handle the command and the data. Be aware that there is a distinction between the command
	// and the data being successfully sent to the device and the command and data being handled successfully 
	// by the device
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	}

	if (hr == S_OK)
	{
		//TRACE("MTP Response code: 0x%X\n", dwResponseCode);
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}

	// If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
	// property. SetDevicePropValue does not return additional response parameters, so skip this code 


	// Free up any allocated memory
	CoTaskMemFree(pbBuffer);
	CoTaskMemFree((LPVOID)pwszContext);
	if(pBuffer)
	{
		delete pBuffer;
		pBuffer = NULL;
	}
	//WriteLog(_T("SetDevPropValue 000"));
	hr = SetDevPropValue(pDevice,PTP_OC_SAMSUNG_FWUpdate, SDI_TYPE_UINT_8, 0xFF);
	//WriteLog(_T("SetDevPropValue 111"));
	return hr;
}
void CSdiCoreWin7::WriteLog(CString strLog)
{
// 	EnterCriticalSection (m_CriSectionLog);
// 
// 	CString strFolderName = _T("C:\\4DLog"), strFileName;
// 
// 	CTime cTime = CTime::GetCurrentTime();
// 	strFileName.Format(_T("%04d-%02d-%02d_%s.log"), cTime.GetYear(), cTime.GetMonth(), cTime.GetDay(), m_strUniqueID);
// 
// 	CreateDirectory(strFolderName, NULL);
// 	CFile logFile;
// 	if(!logFile.Open(strFolderName + _T("\\") + strFileName, CFile::modeCreate | CFile::modeReadWrite |CFile::modeNoTruncate))
// 	{
// 		return ;
// 	}
// 	logFile.SeekToEnd();
// 	CString strLogData;
// 	strLogData.Format(_T("[%02d_%02d_%02d] %s \r\n"), cTime.GetHour(), cTime.GetMinute(), cTime.GetSecond(), strLog);
// 	logFile.Write(strLogData, strLogData.GetLength() * 2);
// 	logFile.Close();
// 	LeaveCriticalSection (m_CriSectionLog);
	
	
	SYSTEMTIME cur_time; 
	GetLocalTime(&cur_time); 

	CString strLine;
	strLine.Format(_T("[%02d:%02d:%02d:%03ld] "), cur_time.wHour, cur_time.wMinute, cur_time.wSecond, cur_time.wMilliseconds);
	
	
	EnterCriticalSection (m_CriSectionLog);
	CFile logFile;
	if(!logFile.Open(m_strLog, CFile::modeCreate | CFile::modeReadWrite |CFile::modeNoTruncate))
	{
		return ;
	}
	logFile.SeekToEnd();
	strLine.Append(strLog);
	strLine.Append(_T("\r\n"));
	logFile.Write(strLine, strLine.GetLength() * 2);
	logFile.Close();

	LeaveCriticalSection (m_CriSectionLog);
}


HRESULT CSdiCoreWin7::CaptureExec(IPortableDevice* pDevice,CString strSaveName, int nFileNum, int nClose)
{
	TRACE(_T("[Capture] CaptureExec || File Save [%s]\n"),strSaveName);

	HRESULT hr = S_OK;
	//-- 2014-09-19 kcd
	//-- Image �������� ����.
	//const WORD PTP_OPCODE_CAPTURE = PTP_OC_SAMSUNG_GetObjectInMemory;
	const WORD PTP_OPCODE_CAPTURE = PTP_OC_SAMSUNG_ImageTransfer;
    const WORD PTP_RESPONSECODE_OK = PTP_RC_OK;     // PTP_RC_OK indicates command success

	//-----------------------------------------------------------------------------
	//--
	//-- 1. initiating
	//--
	//-----------------------------------------------------------------------------
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK) hr = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (VOID**)&spParameters);
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
	if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID,  WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);

	// Specify the actual MTP opcode to execute here
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE,  (ULONG) PTP_OPCODE_CAPTURE);
	// GetDevicePropValue requires the property code as an MTP parameter
	// MTP parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK) hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDevicePropVariantCollection, (VOID**)&spMtpParams);

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;
// 	if (hr == S_OK)
// 	{
// 		pvParam.ulVal = PTP_PROP_BASE;
// 		hr = spMtpParams->Add(&pvParam);
// 	}
	/*
	if (hr == S_OK)
	{
		pvParam.ulVal = nFileNum;
		hr = spMtpParams->Add(&pvParam);
	}
	if (hr == S_OK)
	{
		pvParam.ulVal = nClose;
		hr = spMtpParams->Add(&pvParam);
	}
	*/
	//TRACE(_T("nFileNum 22: %d, nClose : %d\r\n"), nFileNum, nClose);
	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK) hr = spParameters->SetIPortableDevicePropVariantCollectionValue( WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	// Send the command to initiate the transfer
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [CaptureExec]: 0x%08X\n", hrCmd);
			return hr;
		} 
	}


	//-----------------------------------------------------------------------------
	//--
	//-- 2. Reading Data
	//--
	//-----------------------------------------------------------------------------
	// If the transfer was initiated successfully, the driver returns a context cookie
	LPWSTR pwszContext = NULL;
	if (hr == S_OK) hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	// The driver indicates how many bytes will be transferred. This is important to
	// retrieve because we have to read all the data that the device sends us (even if it
	// is not the size we were expecting); otherwise, we run the risk of the device going out of sync
	// with the driver.
	ULONG cbReportedDataSize = 0;
	if (hr == S_OK) hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE,  &cbReportedDataSize);

	// Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
	// which suggests the chunk size that the date should be retrieved in. If your application will be 
	// transferring a large amount of data (>256K), use this property to break down the
	// transfer into small chunks so that your app is more responsive.
	// We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)
	{ 
		hr = S_FALSE;
		bSkipDataPhase = TRUE;
	}

	// Use the WPD_COMMAND_MTP_EXT_READ_DATA command to read in the data
	(void) spParameters->Clear();
	if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID,  WPD_COMMAND_MTP_EXT_READ_DATA.pid);
	// Specify the same context that we received earlier
	if (hr == S_OK) hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	// Allocate a buffer for the command to read data into - this should 
	// be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
	BYTE* pbBufferIn = NULL;
	if (hr == S_OK)
	{
		float nMBSize;
		nMBSize = (float)cbReportedDataSize;
		nMBSize /=1024;
		nMBSize /=1024;
		TRACE(_T("[Capture] Memory Allocation [%0.2f]MB\n"),nMBSize);
		pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
		if (pbBufferIn == NULL)
		{
			hr = E_OUTOFMEMORY;
		}
	}

	// Pass the allocated buffer as a parameter
	if (hr == S_OK) hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA,  pbBufferIn, cbReportedDataSize);
	// Specify the number of bytes to transfer as a parameter
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, cbReportedDataSize);

	// Send the command to transfer the data
	spResults = NULL;
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);
	// Check if the driver was able to transfer the data
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			return hr;
		}    
	}

	//-----------------------------------------------------------------------------
	//--
	//-- 3. Ending Transfer
	//--
	//-----------------------------------------------------------------------------
	// IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
	// Instead, it is available in the results collection.
	BYTE* pbBufferOut = NULL;
	ULONG cbBytesRead = 0;
	if (hr == S_OK) hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);
	// Reset hr to S_OK because we skipped the data phase
	if (hr == S_FALSE && bSkipDataPhase == TRUE) hr = S_OK;
	// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
	(void) spParameters->Clear();
	if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID,  WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	// Specify the same context that we received earlier
	if (hr == S_OK) hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	// Send the completion command
	spResults = NULL;
	TRACE(_T("[Capture] >>> Start Transfer Data\n"));
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);
	TRACE(_T("[Capture] >>> End Transfer Data\n"));
	// Check if the driver successfully ended the data transfer
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			return hr;
		}              
	}

	// If the command was executed successfully, check the MTP response code to see if the device
	// can handle the command. If the device cannot handle the command, the data phase would 
	// have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
	// error. 
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK) hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	// If the command was handled by the device, return the property value in the BYREF property


	//-----------------------------------------------------------------------------
	//--
	//-- 4. Save File (Thread/On Time)
	//--
	//-----------------------------------------------------------------------------
	if (hr == S_OK)
	{
		if (pbBufferOut != NULL)
		{
			SdiFileInfo* pFileInfo = new SdiFileInfo;

			pFileInfo->pImageBuffer = NULL;
			//pFileInfo->strImageFileName = strSaveName;
			_tcscpy_s(pFileInfo->strImageFileName, strSaveName);

			if(pbBufferOut[0] == 0x1)
			{
				TRACE(_T("pbBufferOut[0] *************** \n"),strSaveName);
				pFileInfo->nImageSize		= cbReportedDataSize-1;
				pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize-1];
				memcpy(pFileInfo->pImageBuffer,pbBufferOut+1,cbReportedDataSize-1);			
			}
			else
			{
				TRACE(_T("pbBufferOut[0] ################ \n"),strSaveName);
				pFileInfo->nImageSize		= cbReportedDataSize;
				pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize];
				memcpy(pFileInfo->pImageBuffer,pbBufferOut,cbReportedDataSize);
			}
			pFileInfo->nmdat = nFileNum;
			pFileInfo->nOffset = nClose;

			TRACE(_T("[Capture] File Save [%s] on Thread (File Manager)\n"),strSaveName);
			pFileInfo->nAppendMsg = WM_SDI_OP_IMAGEFILE_TRANSFER;
			m_pSdiFileMgr->AddFile(pFileInfo);
// 			CFile file;
// 			file.Open(strSaveName, CFile::modeWrite | CFile::modeCreate ,NULL);
// 			//-- 2013-6-20 yongmin.lee
// 			TRACE(_T("[Capture] >>> Start File Save [%s]\n"),strSaveName);
// 			file.Write(pbBufferOut,cbReportedDataSize);
// 			TRACE(_T("[Capture] <<< End File Save\n"));
// 			file.Flush();
// 			file.Close();

		}
		else
		{
			// MTP response code was OK, but no data phase occurred
			hr = E_UNEXPECTED;
		}
	}

	// If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
	// property. GetDevicePropValue does not return additional response parameters, so skip this code 
	// If required, you might find that code in the post that covered sending MTP commands without data

	// Free up any allocated memory
	CoTaskMemFree(pbBufferIn);
	CoTaskMemFree(pbBufferOut);
	CoTaskMemFree(pwszContext);

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return hr;
}
HRESULT CSdiCoreWin7::ResetDevice(IPortableDevice*  pDevice)
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_RESET_DEVICE = PTP_OC_SAMSUNG_Reset;
	const WORD PTP_RESPONSECODE_OK = PTP_RC_OK;     // 0x2001 indicates command success
	LPWSTR pwszContext = NULL;
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
	// Similar commands exist for reading and writing data phases
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	}

	// Specify the actual MTP opcode that we want to execute here
	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_RESET_DEVICE);
	}

	// GetNumObject requires three params - storage ID, object format, and parent object handle   
	// Parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	// Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
	// should be changed to use the device's real storage ID (which can be obtained by
	// removing the prefix for the WPD object ID for the storage)
	if (hr == S_OK)
	{
		// 		pvParam.ulVal = nCurPos;
		// 		hr = spMtpParams->Add(&pvParam);
	}

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	// Send the command to the MTP device
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	} 

	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		//TRACE("Driver return code: 0x%08X\n", hrCmd);
		hr = hrCmd;
	}

	// If the command was executed successfully, check the MTP response code to see if the
	// device can handle the command. Be aware that there is a distinction between the command
	// being successfully sent to the device and the command being handled successfully by the device.
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	}

	if (hr == S_OK)
	{
		//TRACE("MTP Response code: 0x%X\n", dwResponseCode);
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}
	return hr;
}
SdiResult CSdiCoreWin7::SdiSetEnLarge(int nLarge)
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_SetEnLarge; // GetNumObject opcode is 0x1006
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;     // PTP_RC_OK indicates command success
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
	// Similar commands exist for reading and writing data phases
	if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	// Specify the actual MTP opcode that we want to execute here
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
	// GetNumObject requires three params - storage ID, object format, and parent object handle   
	// Parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;
	// Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
	// should be changed to use the device's real storage ID (which can be obtained by
	// removing the prefix for the WPD object ID for the storage)
	if (hr == S_OK)
	{
		pvParam.ulVal = nLarge;
		hr = spMtpParams->Add(&pvParam);
	}

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	// Send the command to the MTP device
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK) hr = hrCmd;
	// If the command was executed successfully, check the MTP response code to see if the
	// device can handle the command. Be aware that there is a distinction between the command
	// being successfully sent to the device and the command being handled successfully by the device.
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

SdiResult CSdiCoreWin7::SdiSetPause(int nPause)
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_SetRecordPause; // GetNumObject opcode is 0x1006
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;     // PTP_RC_OK indicates command success
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
	// Similar commands exist for reading and writing data phases
	if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	// Specify the actual MTP opcode that we want to execute here
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
	// GetNumObject requires three params - storage ID, object format, and parent object handle   
	// Parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;
	// Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
	// should be changed to use the device's real storage ID (which can be obtained by
	// removing the prefix for the WPD object ID for the storage)
	if (hr == S_OK)
	{
		pvParam.ulVal = nPause;
		hr = spMtpParams->Add(&pvParam);
	}

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	// Send the command to the MTP device
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK) hr = hrCmd;
	// If the command was executed successfully, check the MTP response code to see if the
	// device can handle the command. Be aware that there is a distinction between the command
	// being successfully sent to the device and the command being handled successfully by the device.
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

SdiResult CSdiCoreWin7::SdiSetResume(int nResume)
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_SetRecordResume; // GetNumObject opcode is 0x1006
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;     // PTP_RC_OK indicates command success
	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE command
	// Similar commands exist for reading and writing data phases
	if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	// Specify the actual MTP opcode that we want to execute here
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
	// GetNumObject requires three params - storage ID, object format, and parent object handle   
	// Parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;
	// Specify storage ID parameter. Most devices that have 0x10001 have the storage ID. This
	// should be changed to use the device's real storage ID (which can be obtained by
	// removing the prefix for the WPD object ID for the storage)
	if (hr == S_OK)
	{
		pvParam.ulVal = nResume;
		hr = spMtpParams->Add(&pvParam);
	}

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	// Send the command to the MTP device
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK) hr = hrCmd;
	// If the command was executed successfully, check the MTP response code to see if the
	// device can handle the command. Be aware that there is a distinction between the command
	// being successfully sent to the device and the command being handled successfully by the device.
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}
SdiResult CSdiCoreWin7::SdiTrackingAFStop()
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_TrackingAFStop;
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK; 
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;
 

    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
    HRESULT hrCmd = S_OK;
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

SdiResult CSdiCoreWin7::SdiControlTouchAF(int nX, int nY)
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_ControlTouchAf;
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK; 
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;
    if (hr == S_OK)
    {
        pvParam.ulVal = nX;
		hr = spMtpParams->Add(&pvParam);
    }

	if (hr == S_OK)
    {
        pvParam.ulVal = nY;
		hr = spMtpParams->Add(&pvParam);
    }

    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
    HRESULT hrCmd = S_OK;
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

SdiResult CSdiCoreWin7::SdiGetCaptureCount()
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_GetCaptureCount;
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK; 
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;
    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
    HRESULT hrCmd = S_OK;
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	CComPtr<IPortableDevicePropVariantCollection> spRespParams;
	if (hr == S_OK)		hr = spResults->GetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS, &spRespParams);
	PROPVARIANT pvResult = {0};
	if (hr == S_OK)		hr = spRespParams->GetAt(0, &pvResult);   
	
	if(hr == S_OK) 
		return (SdiResult)pvResult.ulVal;
	else 
		return dwResponseCode;
}

SdiResult CSdiCoreWin7::SdiGetImagePath(CString& strPath)
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	TRACE(_T("[Capture] SdiGetImagePath \n"));

	int capture_start;
	capture_start = GetTickCount();
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_CAPTURE	= PTP_OC_SAMSUNG_GetImagePath;    
    const WORD PTP_RESPONSECODE_OK	= PTP_RC_OK;
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (VOID**)&spParameters);
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
    if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_CAPTURE);
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;

    
    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
    HRESULT hrCmd = S_OK;
    if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	else			TRACE(_T(" SdiGetImagePath() : SendCommand ERROR : 0x%08x\n"), hr);
    if (hr == S_OK)
	{	
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating): 0x%08X\n", hrCmd);
			return hr;
		} 
	}
    LPWSTR pwszContext = NULL;
    if (hr == S_OK)	hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
    ULONG cbReportedDataSize = 0;
	ULONG cbReportedChunkDataSize = 0;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE ,   &cbReportedDataSize);
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE ,  &cbReportedChunkDataSize);
    BOOL bSkipDataPhase = FALSE;
    if (hr == S_OK && cbReportedDataSize == 0)  {  hr = S_FALSE; bSkipDataPhase = TRUE; }
	BYTE* pbBufferIn = NULL;
	BYTE* pbBufferOut = NULL;
	BYTE* pbBufferTotal = NULL;	
	pbBufferTotal = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
	if(cbReportedDataSize==0 || cbReportedChunkDataSize==0)
	{
		TRACE(_T(" SdiGetImagePath() : Data/Chunk Size : %d / %d\n"), cbReportedDataSize, cbReportedChunkDataSize);
		CoTaskMemFree(pbBufferTotal);
		return SDI_ERR_DEVICE_DISCONNECTED;
	}

	int nDivideCnt = cbReportedDataSize/cbReportedChunkDataSize;
	int nMemAllocSize;

	TRACE(_T("[Capture] SdiGetImagePath || Div Cnt[%d] => DataSize[%d] ChunkSize[%d]\n"),nDivideCnt ,cbReportedDataSize , cbReportedChunkDataSize);

	for(int i=0 ; i<= nDivideCnt ;i++)
	{
		if(i != nDivideCnt)	nMemAllocSize = cbReportedChunkDataSize;
		else				nMemAllocSize = cbReportedDataSize%cbReportedChunkDataSize;
		TRACE(_T("Get File [Mem Size][%d] Chunk Size[%d] Total Size[%d]\n"),nMemAllocSize, cbReportedChunkDataSize, cbReportedDataSize);

		if(spParameters)	(void) spParameters->Clear();
		if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
		if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_READ_DATA.pid);
		if (hr == S_OK)	hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
		if (hr == S_OK)
		{
			pbBufferIn = (BYTE*) CoTaskMemAlloc(nMemAllocSize);

			if (pbBufferIn == NULL)
			{
				TRACE(_T("[Capture] SdiGetImagePath || Out of Memory\n"));
				hr = E_OUTOFMEMORY;
			}
			else
				TRACE(_T("[Capture] SdiGetImagePath || pbBufferIn = (BYTE*) CoTaskMemAlloc(%d);\n"),nMemAllocSize);
		}

		if (hr == S_OK)	hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBufferIn, nMemAllocSize);
		if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, nMemAllocSize);
		spResults = NULL;
		if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
		if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
		if (hr == S_OK) hr = hrCmd;

		ULONG cbBytesRead = 0;
		if (hr == S_OK)	hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);				
		if (pbBufferOut)
		{
			TRACE(_T("[Capture] MemCpy [%d][%d]\n"),i*cbReportedChunkDataSize, nMemAllocSize);
			memcpy(pbBufferTotal+(i*cbReportedChunkDataSize),pbBufferOut,nMemAllocSize);
		}
		if(pbBufferIn)
		{
			CoTaskMemFree(pbBufferIn);
			pbBufferIn = NULL;
		}

		if(pbBufferOut)
		{
			CoTaskMemFree(pbBufferOut);
			pbBufferOut = NULL;
		}
	}

    if (hr == S_FALSE && bSkipDataPhase == TRUE)	hr = S_OK;
	if(spParameters)	(void) spParameters->Clear();
	if (hr != S_OK) TRACE(_T(" spParameters Clear() Error\n"));
	if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	if (hr == S_OK) hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	spResults = NULL;
	TRACE(_T("[SendCommand] Result\n"));

	if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;


	TRACE(_T("[Capture] Save File Size [%d] >>>>> \n"),cbReportedDataSize);

	char pData[50];
	memcpy(pData, pbBufferTotal, cbReportedDataSize);
	if(cbReportedDataSize!=0)
	{
		CString cstring;
		cstring = CString( pData );

		TCHAR drive[MAX_PATH];
		TCHAR dir  [MAX_PATH];
		TCHAR name [MAX_PATH];
		TCHAR ext  [MAX_PATH];

		_tsplitpath((LPCTSTR)cstring, drive, dir, name, ext);
		strPath = name;
		CoTaskMemFree(pbBufferTotal);

		TRACE(_T("[Capture] Save File Size [%d] <<<< \n"),cbReportedDataSize);
	}

	TRACE(_T(" File Write Done!! %s\n\t\t[%d Bytes]\n"), "C:\\picture\\test.txt", cbReportedDataSize);

    DWORD dwResponseCode = 0x0;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK)	hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
    CoTaskMemFree(pwszContext);	
	if (hr == S_OK) return SDI_ERR_OK;
	else if(hr==0x80070079){
		TRACE(_T(" GetCaptureComplete() : ERROR_SEM_TIMEOUT !!!\n"));
		return SDI_ERR_SEMAPHORE_TIMEOUT;
	}
	else return dwResponseCode;

}

SdiResult CSdiCoreWin7::SdiImageTransfer(CString strName,int nfilenum, int nClose)
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	TRACE(_T("[Capture] CaptureCompleteExec || File Save\n"));

	int capture_start;
	capture_start = GetTickCount();
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_CAPTURE	= PTP_OC_SAMSUNG_ImageTransfer;    
    const WORD PTP_RESPONSECODE_OK	= PTP_RC_OK; 
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (VOID**)&spParameters);
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
    if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_CAPTURE);
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;

    if (hr == S_OK)
    {
        pvParam.ulVal = nfilenum;
		hr = spMtpParams->Add(&pvParam);
    }

	if (hr == S_OK)
    {
        pvParam.ulVal = nClose;
        hr = spMtpParams->Add(&pvParam);
    }
    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
    HRESULT hrCmd = S_OK;
    if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	else			TRACE(_T(" CaptureCompleteExec() : SendCommand ERROR : 0x%08x\n"), hr);
    if (hr == S_OK)
	{	
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating): 0x%08X\n", hrCmd);
			return hr;
		} 
	}
    LPWSTR pwszContext = NULL;
    if (hr == S_OK)	hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
    ULONG cbReportedDataSize = 0;
	ULONG cbReportedChunkDataSize = 0;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE ,   &cbReportedDataSize);
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE ,  &cbReportedChunkDataSize);
    BOOL bSkipDataPhase = FALSE;
    if (hr == S_OK && cbReportedDataSize == 0)  {  hr = S_FALSE; bSkipDataPhase = TRUE; }
	BYTE* pbBufferIn = NULL;
	BYTE* pbBufferOut = NULL;
	BYTE* pbBufferTotal = NULL;	
	pbBufferTotal = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
	if(cbReportedDataSize==0 || cbReportedChunkDataSize==0)
	{
		TRACE(_T(" CaptureCompleteExec() : Data/Chunk Size : %d / %d\n"), cbReportedDataSize, cbReportedChunkDataSize);
		CoTaskMemFree(pbBufferTotal);
		return SDI_ERR_DEVICE_DISCONNECTED;
	}

	//-- 2014-09-20 joonho.kim
	//-- file size check
	if(cbReportedDataSize < (20*1024*1024))
		nClose = 1;
	else
		m_pSdiFileMgr->m_bFileClose = FALSE;

	if(nClose == 1)
		m_pSdiFileMgr->m_bFileClose = TRUE;


	int nDivideCnt = cbReportedDataSize/cbReportedChunkDataSize;
	for(int i=0 ; i<= nDivideCnt ;i++)
	{
		if(spParameters)	(void) spParameters->Clear();
		if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
		if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_READ_DATA.pid);
		if (hr == S_OK)	hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
		if (hr == S_OK)
		{
			pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedChunkDataSize);
			if (pbBufferIn == NULL)
				hr = E_OUTOFMEMORY;
		}

		if (hr == S_OK)	hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBufferIn, cbReportedChunkDataSize);
		if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, cbReportedChunkDataSize);
		spResults = NULL;
		if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
		if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
		if (hr == S_OK) hr = hrCmd;

		ULONG cbBytesRead = 0;
		if (hr == S_OK)	hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);				
		if (pbBufferOut != NULL)
		{
			if( i != nDivideCnt)
				memcpy(pbBufferTotal+(i*cbReportedChunkDataSize),pbBufferOut,cbReportedChunkDataSize);
			else
				memcpy(pbBufferTotal+(i*cbReportedChunkDataSize),pbBufferOut,cbReportedDataSize%cbReportedChunkDataSize);
		}
		if(pbBufferIn)
		{
			CoTaskMemFree(pbBufferIn);
			pbBufferIn = NULL;
		}

		if(pbBufferOut)
		{
			CoTaskMemFree(pbBufferOut);
			pbBufferOut = NULL;
		}
	}

    if (hr == S_FALSE && bSkipDataPhase == TRUE)	hr = S_OK;
	if(spParameters)	(void) spParameters->Clear();
	if (hr != S_OK) TRACE(_T(" spParameters Clear() Error\n"));
	if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	if (hr == S_OK) hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	spResults = NULL;
	if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;


	if(cbReportedDataSize!=0)
	{

#ifdef _THREAD_FILE_SAVE
		SdiFileInfo* pFileInfo = new SdiFileInfo;

		pFileInfo->pImageBuffer = NULL;

		//-- 2014-11-10 joonho.kim
		if(nfilenum == 0)
		{
			//pFileInfo->strImageFileName = strName;
			_tcscpy_s(pFileInfo->strImageFileName, strName);
			m_strSaveName = strName;
		}
		else
		{
			//pFileInfo->strImageFileName = m_strSaveName;
			_tcscpy_s(pFileInfo->strImageFileName, m_strSaveName);
		}
		
		if(pbBufferTotal[0] == 0x1)
		{
			pFileInfo->nImageSize		= cbReportedDataSize-1;
			pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize-1];
			memcpy(pFileInfo->pImageBuffer,pbBufferTotal+1,cbReportedDataSize-1);			
		}
		else
		{
			pFileInfo->nImageSize		= cbReportedDataSize;
			pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize];
			memcpy(pFileInfo->pImageBuffer,pbBufferTotal,cbReportedDataSize);
		}
		TRACE(_T("[Capture] File Save [%s] on Thread (File Manager)\n"),strName);

		pFileInfo->nAppendMsg = WM_SDI_OP_IMAGE_TRANSFER;
		pFileInfo->nClose = nClose;
		if(nClose == 1)
			m_pSdiFileMgr->AddFile(pFileInfo);
		else
		m_pSdiFileMgr->AddFile(pFileInfo);
#else
		//- kcd 2013-11-25
		//- ���� ��� SdiDataMgr ���� ���� 
		CFile file;
		file.Open(strSaveName, CFile::modeWrite | CFile::modeCreate ,NULL);
		//-- 2013-6-20 yongmin.lee
		if(pbBufferTotal[0] == 0x1)
			file.Write(pbBufferTotal+1,cbReportedDataSize-1);
		else
			file.Write(pbBufferTotal,cbReportedDataSize);  
		file.Flush();
		file.Close();
#endif
		//--2011-09-07 jeansu : Return IsLastCaptureDone
		//if(pbBufferTotal[0] == 0x1) 
			//bIsLast = true;
		//-- 2013-11-22 hongsu@esmlab.com
		//-- Memory Leak
		CoTaskMemFree(pbBufferTotal);
	}

	TRACE(_T(" File Write Done!! %s\n\t\t[%d Bytes]\n"), "C:\\picture\\test.txt", cbReportedDataSize);

    DWORD dwResponseCode = 0x0;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK)	hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
    CoTaskMemFree(pwszContext);	
	if (hr == S_OK) return SDI_ERR_OK;
	else if(hr==0x80070079){
		TRACE(_T(" GetCaptureComplete() : ERROR_SEM_TIMEOUT !!!\n"));
		return SDI_ERR_SEMAPHORE_TIMEOUT;
	}
	else return dwResponseCode;
}

SdiResult CSdiCoreWin7::SdiImageTransferEX(CString strName,int nfilenum, int nClose)
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	TRACE(_T("[Capture] CaptureCompleteExec || File Save\n"));

	int capture_start;
	capture_start = GetTickCount();
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_CAPTURE	= PTP_OC_SAMSUNG_ImageTransfer;    
	const WORD PTP_RESPONSECODE_OK	= PTP_RC_OK; 
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (VOID**)&spParameters);
	if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,  WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
	if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_CAPTURE);
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		pvParam.ulVal = nfilenum;
		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
	{
		pvParam.ulVal = nClose;
		hr = spMtpParams->Add(&pvParam);
	}
	if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	else			TRACE(_T(" CaptureCompleteExec() : SendCommand ERROR : 0x%08x\n"), hr);
	if (hr == S_OK)
	{	
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating): 0x%08X\n", hrCmd);
			return hr;
		} 
	}
	LPWSTR pwszContext = NULL;
	if (hr == S_OK)	hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	ULONG cbReportedDataSize = 0;
	ULONG cbReportedChunkDataSize = 0;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE ,   &cbReportedDataSize);
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE ,  &cbReportedChunkDataSize);
	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)  {  hr = S_FALSE; bSkipDataPhase = TRUE; }
	BYTE* pbBufferIn = NULL;
	BYTE* pbBufferOut = NULL;
	BYTE* pbBufferTotal = NULL;	
	pbBufferTotal = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
	if(cbReportedDataSize==0 || cbReportedChunkDataSize==0)
	{
		TRACE(_T(" CaptureCompleteExec() : Data/Chunk Size : %d / %d\n"), cbReportedDataSize, cbReportedChunkDataSize);
		CoTaskMemFree(pbBufferTotal);
		return SDI_ERR_DEVICE_DISCONNECTED;
	}

	
	int nDivideCnt = cbReportedDataSize/cbReportedChunkDataSize;
	for(int i=0 ; i<= nDivideCnt ;i++)
	{
		if(spParameters)	(void) spParameters->Clear();
		if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY,WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
		if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_READ_DATA.pid);
		if (hr == S_OK)	hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
		if (hr == S_OK)
		{
			pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedChunkDataSize);
			if (pbBufferIn == NULL)
				hr = E_OUTOFMEMORY;
		}

		if (hr == S_OK)	hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, pbBufferIn, cbReportedChunkDataSize);
		if (hr == S_OK)	hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, cbReportedChunkDataSize);
		spResults = NULL;
		if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
		if (hr == S_OK)	hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
		if (hr == S_OK) hr = hrCmd;

		ULONG cbBytesRead = 0;
		if (hr == S_OK)	hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);				
		if (pbBufferOut != NULL)
		{
			if( i != nDivideCnt)
				memcpy(pbBufferTotal+(i*cbReportedChunkDataSize),pbBufferOut,cbReportedChunkDataSize);
			else
				memcpy(pbBufferTotal+(i*cbReportedChunkDataSize),pbBufferOut,cbReportedDataSize%cbReportedChunkDataSize);
		}
		if(pbBufferIn)
		{
			CoTaskMemFree(pbBufferIn);
			pbBufferIn = NULL;
		}

		if(pbBufferOut)
		{
			CoTaskMemFree(pbBufferOut);
			pbBufferOut = NULL;
		}
	}

	if (hr == S_FALSE && bSkipDataPhase == TRUE)	hr = S_OK;
	if(spParameters)	(void) spParameters->Clear();
	if (hr != S_OK) TRACE(_T(" spParameters Clear() Error\n"));
	if (hr == S_OK) hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	if (hr == S_OK) hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	spResults = NULL;
	if (hr == S_OK)	hr = pDevice->SendCommand(0, spParameters, &spResults);
	if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	if (hr == S_OK) hr = hrCmd;


	if(cbReportedDataSize!=0)
	{
		SdiFileInfo* pFileInfo = new SdiFileInfo;
		pFileInfo->pImageBuffer = NULL;
		_tcscpy_s(pFileInfo->strImageFileName, strName);
		m_strSaveName = strName;
		pFileInfo->nImageSize		= cbReportedDataSize;
		pFileInfo->pImageBuffer		= new BYTE[cbReportedDataSize];
		memcpy(pFileInfo->pImageBuffer,pbBufferTotal,cbReportedDataSize);
		TRACE(_T("[Capture] File Save [%s] on Thread (File Manager)\n"),strName);

		pFileInfo->nAppendMsg = WM_SDI_OP_IMAGE_TRANSFER;
		pFileInfo->nClose = 1;
		m_pSdiFileMgr->AddFile(pFileInfo);
		
		CoTaskMemFree(pbBufferTotal);
	}

	TRACE(_T(" File Write Done!! %s\n\t\t[%d Bytes]\n"), "C:\\picture\\test.txt", cbReportedDataSize);

	DWORD dwResponseCode = 0x0;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK)	hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	CoTaskMemFree(pwszContext);	
	if (hr == S_OK) return SDI_ERR_OK;
	else if(hr==0x80070079){
		TRACE(_T(" GetCaptureComplete() : ERROR_SEM_TIMEOUT !!!\n"));
		return SDI_ERR_SEMAPHORE_TIMEOUT;
	}
	else return dwResponseCode;
}

SdiResult CSdiCoreWin7::SdiSetRecordPause(int nPause)
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_SetRecordPause;
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;
    if (hr == S_OK)
    {
        pvParam.ulVal = nPause;
		hr = spMtpParams->Add(&pvParam);
    }
    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);
    HRESULT hrCmd = S_OK;
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

SdiResult CSdiCoreWin7::SdiSetRecordResume(int nSkip)
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_SetRecordResume;
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;
    if (hr == S_OK)
    {
        pvParam.ulVal = nSkip;
		hr = spMtpParams->Add(&pvParam);
    }
    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);
    HRESULT hrCmd = S_OK;
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

SdiResult CSdiCoreWin7::SdiSetLiveView(int nCommand)
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_SetLiveView;
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;
    if (hr == S_OK)
    {
        pvParam.ulVal = nCommand;
		hr = spMtpParams->Add(&pvParam);
    }

    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);
    HRESULT hrCmd = S_OK;
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

SdiResult CSdiCoreWin7::SdiFileReceiveComplete()
{
	//if(m_pSdiFileMgr->m_bFileClose == TRUE)
	//	return SDI_ERR_OK;

	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_FileReceiveComplete;
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;
 
    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);    
    HRESULT hrCmd = S_OK;
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

SdiResult CSdiCoreWin7::SdiLiveReceiveComplete()
{
	IPortableDevice* pDevice = m_pIPortableDevice;
	HRESULT hr = S_OK;
    const WORD PTP_OPCODE_GETNUMOBJECT	= PTP_OC_SAMSUNG_LiveReceiveComplete;
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;
    CComPtr<IPortableDeviceValues> spParameters;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDeviceValues,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDeviceValues,(VOID**)&spParameters);
    if (hr == S_OK)	hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
    if (hr == S_OK) hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, (ULONG) PTP_OPCODE_GETNUMOBJECT);
    CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
    if (hr == S_OK)	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,NULL,CLSCTX_INPROC_SERVER,IID_IPortableDevicePropVariantCollection,(VOID**)&spMtpParams);
    PROPVARIANT pvParam = {0};
    pvParam.vt = VT_UI4;
    if (hr == S_OK)	hr = spParameters->SetIPortableDevicePropVariantCollectionValue(WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
    CComPtr<IPortableDeviceValues> spResults;
    if (hr == S_OK) hr = pDevice->SendCommand(0, spParameters, &spResults);
    HRESULT hrCmd = S_OK;
    if (hr == S_OK) hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
    if (hr == S_OK) hr = hrCmd;
    DWORD dwResponseCode = 0x2001;
    if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
    if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

SdiResult CSdiCoreWin7::SdiHalfShutter( int nRelease )
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	if(HalfShutter(m_pIPortableDevice, nRelease) == S_OK) return SDI_ERR_OK;
	else return SDI_ERR_ACCESS_DENIED;
}

SdiResult CSdiCoreWin7::SdiSetFocusFrame(int nX, int nY, int nMagnification)
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	return SetFocusFrame(m_pIPortableDevice, nX, nY, nMagnification);
}

SdiResult CSdiCoreWin7::SdiGetFocusFrame(BYTE **pData, SdiInt32* size)
{
	if(m_pIPortableDevice==NULL) return SDI_ERR_DEVICE_NOT_INITIALIZED;
	return GetFocusFrame(m_pIPortableDevice, pData, size);
}

HRESULT CSdiCoreWin7::SetFocusFrame( IPortableDevice* pDevice, int nX, int nY, int nMagnification )
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_FORMAT_DEVICE = 0x9014;	// GH5 set focus frame
	const WORD PTP_RESPONSECODE_OK = PTP_RC_OK; 
	LPWSTR pwszContext = NULL;
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITHOUT_DATA_PHASE.pid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_FORMAT_DEVICE);
	}

	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}

	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	if (hr == S_OK)
	{
		pvParam.ulVal = nX;
		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
	{
		pvParam.ulVal = nY;
		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
	{
		pvParam.ulVal = nMagnification;
		hr = spMtpParams->Add(&pvParam);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	} 

	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)	hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	if (hr == S_OK) hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}

HRESULT CSdiCoreWin7::GetFocusFrame( IPortableDevice* pDevice, BYTE **pData, SdiInt32* size )
{
	HRESULT hr = S_OK;
	const WORD PTP_OPCODE_GETNUMOBJECT	= 0x9015;		 // GH5 get focus frame
	const WORD PTP_RESPONSECODE_OK		= PTP_RC_OK;     // PTP_RC_OK indicates command success

	// Build basic WPD parameters for the command
	CComPtr<IPortableDeviceValues> spParameters;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDeviceValues,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDeviceValues,
			(VOID**)&spParameters);
	}

	// Use the WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ command here
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_EXECUTE_COMMAND_WITH_DATA_TO_READ.pid);
	}

	// Specify the actual MTP opcode to execute here
	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_OPERATION_CODE, 
			(ULONG) PTP_OPCODE_GETNUMOBJECT);
	}

	// GetDevicePropValue requires the property code as an MTP parameter
	// MTP parameters need to be first put into a PropVariantCollection
	CComPtr<IPortableDevicePropVariantCollection> spMtpParams;
	if (hr == S_OK)
	{
		hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IPortableDevicePropVariantCollection,
			(VOID**)&spMtpParams);
	}


	PROPVARIANT pvParam = {0};
	pvParam.vt = VT_UI4;

	// Add MTP parameters collection to our main parameter list
	if (hr == S_OK)
	{
		hr = spParameters->SetIPortableDevicePropVariantCollectionValue(
			WPD_PROPERTY_MTP_EXT_OPERATION_PARAMS, spMtpParams);
	}  

	// Send the command to initiate the transfer
	CComPtr<IPortableDeviceValues> spResults;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	}

	// Check if the driver was able to send the command by interrogating WPD_PROPERTY_COMMON_HRESULT
	HRESULT hrCmd = S_OK;
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
		// 2013-12-01 hongsu.jung
		//-- hrCmd = 0x800700a | ��û�� ���ҽ��� ��� ���Դϴ�.
		//-- Return Error
		if(hr != S_OK)
		{
			TRACE("Driver return code (initiating) [PTP_OC_SAMSUNG_GetEvent]: 0x%08X\n", hrCmd);
			return hr;
		} 
	}

	// If the transfer was initiated successfully, the driver returns a context cookie
	LPWSTR pwszContext = NULL;
	if (hr == S_OK)
	{
		hr = spResults->GetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, &pwszContext);
	}

	// The driver indicates how many bytes will be transferred. This is important to
	// retrieve because we have to read all the data that the device sends us (even if it
	// is not the size we were expecting); otherwise, we run the risk of the device going out of sync
	// with the driver.
	ULONG cbReportedDataSize = 0;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_TOTAL_DATA_SIZE, 
			&cbReportedDataSize);
	}

	// Note: The driver provides an additional property, WPD_PROPERTY_MTP_EXT_OPTIMAL_TRANSFER_BUFFER_SIZE,
	// which suggests the chunk size that the date should be retrieved in. If your application will be 
	// transferring a large amount of data (>256K), use this property to break down the
	// transfer into small chunks so that your app is more responsive.
	// We'll skip this here because device properties are never that big (especially BatteryLevel).
	// If no data will be transferred, skip reading in the data
	BOOL bSkipDataPhase = FALSE;
	if (hr == S_OK && cbReportedDataSize == 0)
	{ 
		hr = S_FALSE;
		bSkipDataPhase = TRUE;
	}

	// Use the WPD_COMMAND_MTP_EXT_READ_DATA command to read in the data
	(void) spParameters->Clear();
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_READ_DATA.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_READ_DATA.pid);
	}

	// Specify the same context that we received earlier
	if (hr == S_OK)
	{
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	}

	// Allocate a buffer for the command to read data into - this should 
	// be the same size as the number of bytes we are expecting to read (per chunk, if applicable).
	BYTE* pbBufferIn = NULL;
	if (hr == S_OK)
	{
		pbBufferIn = (BYTE*) CoTaskMemAlloc(cbReportedDataSize);
		if (pbBufferIn == NULL)
		{
			hr = E_OUTOFMEMORY;
		}
	}

	// Pass the allocated buffer as a parameter
	if (hr == S_OK)
	{
		hr = spParameters->SetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, 
			pbBufferIn, cbReportedDataSize);
	}

	// Specify the number of bytes to transfer as a parameter
	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_TRANSFER_NUM_BYTES_TO_READ, 
			cbReportedDataSize);
	}

	// Send the command to transfer the data
	spResults = NULL;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	}

	// Check if the driver was able to transfer the data

	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (reading data): 0x%08X\n", hrCmd);
			return hr;
		}    
	}

	// IMPORTANT: The API does not actually transfer the data into the buffer we provided earlier.
	// Instead, it is available in the results collection.
	BYTE* pbBufferOut = NULL;
	ULONG cbBytesRead = 0;
	if (hr == S_OK)
	{
		hr = spResults->GetBufferValue(WPD_PROPERTY_MTP_EXT_TRANSFER_DATA, &pbBufferOut, &cbBytesRead);
	}

	// Reset hr to S_OK because we skipped the data phase
	if (hr == S_FALSE && bSkipDataPhase == TRUE)
	{
		hr = S_OK;
	}
	// WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER is the command to signal transfer completion
	(void) spParameters->Clear();
	if (hr == S_OK)
	{
		hr = spParameters->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, 
			WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.fmtid);
	}

	if (hr == S_OK)
	{
		hr = spParameters->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, 
			WPD_COMMAND_MTP_EXT_END_DATA_TRANSFER.pid);
	}

	// Specify the same context that we received earlier
	if (hr == S_OK)
	{
		hr = spParameters->SetStringValue(WPD_PROPERTY_MTP_EXT_TRANSFER_CONTEXT, pwszContext);   
	}

	// Send the completion command
	spResults = NULL;
	if (hr == S_OK)
	{
		hr = pDevice->SendCommand(0, spParameters, &spResults);
	}

	// Check if the driver successfully ended the data transfer
	if (hr == S_OK)
	{
		hr = spResults->GetErrorValue(WPD_PROPERTY_COMMON_HRESULT, &hrCmd);
	}

	if (hr == S_OK)
	{
		hr = hrCmd;
		if(hr != S_OK)
		{
			TRACE("Driver return code (ending transfer): 0x%08X\n", hrCmd);
			return hr;
		}              
	}

	// If the command was executed successfully, check the MTP response code to see if the device
	// can handle the command. If the device cannot handle the command, the data phase would 
	// have been skipped (detected by cbReportedDataSize==0) and the MTP response will indicate the
	// error. 
	DWORD dwResponseCode = 0x2001;
	if (hr == S_OK)
	{
		hr = spResults->GetUnsignedIntegerValue(WPD_PROPERTY_MTP_EXT_RESPONSE_CODE, &dwResponseCode);
	}

	if (hr == S_OK)
	{
		//TRACE("MTP Response code: 0x%X\n", dwResponseCode);
		hr = (dwResponseCode == (DWORD) PTP_RESPONSECODE_OK) ? S_OK : E_FAIL;
	}

	// If the command was handled by the device, return the property value in the BYREF property
	if (hr == S_OK)
	{
		if (pbBufferOut != NULL)
		{
			*pData =(BYTE*) CoTaskMemAlloc(cbReportedDataSize);
			memcpy((BYTE*)*pData,(BYTE*)pbBufferOut,cbReportedDataSize);
			*size = cbReportedDataSize;			
		}
		else
		{
			// MTP response code was OK, but no data phase occurred
			hr = E_UNEXPECTED;
		}
	}

	// If response parameters are present, they will be contained in the WPD_PROPERTY_MTP_EXT_RESPONSE_PARAMS 
	// property. GetDevicePropValue does not return additional response parameters, so skip this code 
	// If required, you might find that code in the post that covered sending MTP commands without data

	// Free up any allocated memory
	CoTaskMemFree(pbBufferIn);
	CoTaskMemFree(pbBufferOut);
	CoTaskMemFree(pwszContext);

	if(hr == S_OK) 
		return SDI_ERR_OK;
	else 
		return dwResponseCode;
}
