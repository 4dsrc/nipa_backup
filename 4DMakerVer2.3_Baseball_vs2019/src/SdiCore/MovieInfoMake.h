#pragma once
typedef long long tInt64;
typedef unsigned long long tUint64;
#include "mp4Define.h"
#include "SdiDefines.h"
#include <vector>
#define MAX_FRAME_COUNT 30

typedef struct
{
	BYTE* pFrame;
	int nFrameSize;
}FRAME_ARRAY;


class CMovieInfoMake
{
public:
	int m_nMaxFrame;
	int m_nRecordType;
	int m_nFileIndex;
	CString m_strFilePath;

public:
	struct sSttsTable {
		int count;
		int duration;
	};

	struct sMP4Track {
		eMmfMediaType type; /* Audio or video */

		union {
			tMmfAudioInfo audioInfo;
			tMmfVideoInfo videoInfo;
		};

		int nMaxBitRate;
		int nAvgBitRate;

		int entry;
		long timescale;
		tMmfClockTime trackDuration;
		long sampleCount;
		long sampleSize;
		long sampleDuration;
		int hasKeyframes;
		int hasBframes;
		int language;
		int trackID;

		int vosLen;
		unsigned char *vosData;

		struct sFrameEntry *cluster;
		unsigned int max_cluster_index;

		int audio_vbr;

		void *pThmbData;
		int nThmbSize;
	} m_Tracks[MAX_TRACK_NUM];
public:
	std::vector<FRAME_ARRAY> m_arrCurrentFrame;
	std::vector<FRAME_ARRAY> m_arrPreFrame;
	
	int m_nRemainFrame;
	int m_nFrameRate;

	int m_nArray[MAX_FRAME_COUNT];
	CFile* m_pFile;
	FILE* m_pFile_;
	int g_nFileSize;
	int m_mdatOffset;
	int g_strm_pos;
	int g_nmoov_pos;
	int m_mp4BufferSize;
	int m_format;
	unsigned char* mp4_buffer;

	int m_refTrackId;
	int m_nbTracks;
	unsigned int time, timescale;

	int IsCheckFileFPS(SdiFileInfo* pFileInfo);
	CString MakeMovieFile(SdiFileInfo* pFileInfo, CString &strBackup, BOOL bRetry);
	void ParseFrame(SdiFileInfo* pFileInfo);

	BOOL Write_ftyp();
	void WriteByte(unsigned int val);
	void Write16be(unsigned int val);
	void Write24be(unsigned int val);
	void Write32be(unsigned int val);
	void Write64be(tUint64 val);
	void WriteTag(const char *pTag);

	void Write_moov();
	void Write_mvhd();
	void Write_trak(sMP4Track *track);
	void Write_tkhd(sMP4Track *track);
	void Write_mdia(sMP4Track *track);
	void Write_mdhd(sMP4Track *track);
	void Write_hdlr(sMP4Track *track);
	void Write_minf(sMP4Track *track);
	void Write_vmhd();
	void Write_dinf();
	void Write_stbl(sMP4Track *track);
	void Write_dref();
	void Write_stsd(sMP4Track *track);
	void Write_stts(sMP4Track *track);
	void Write_stss();
	void Write_stsc();
	void Write_stsz();
	void Write_stco(sMP4Track *track);
	void Write_video(sMP4Track *track);
	void Write_hvcC(sMP4Track *track);
	void Write_colr(sMP4Track *track);

	void WriteTrakData();

	void UpdateSize(unsigned int size, unsigned int offset);
public:
	CMovieInfoMake(void);
	~CMovieInfoMake(void);
};

