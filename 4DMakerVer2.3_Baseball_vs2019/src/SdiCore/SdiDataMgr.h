////
#pragma once
#include "SdiDefines.h"
#include <vector>
using namespace std;

class SdiDataMgr
{
public:
	SdiDataMgr(void);
	~SdiDataMgr(void);

	void AddData(SdiFileInfo* pPicData);

protected:
	vector<SdiFileInfo> m_arrPicData;
	BOOL m_bRunThread;
	HANDLE		m_hThradHandle;

	static unsigned WINAPI ProtectPictureThread(LPVOID param);
};

