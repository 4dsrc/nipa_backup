﻿////////////////////////////////////////////////////////////////////////////////
//
//	CSdiCoreFileMgr.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-11-28
//
////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "SdiCoreFileMgr.h"
#include "SRSIndex.h"
#include <stdint.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
//#include "DllFunc.h"
#include "MovieInfo.h"

#ifdef _WIN64
extern "C"{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/opt.h>
#include <libswscale/swscale.h>
#include <libavutil/timestamp.h>
#include <libavutil/adler32.h>
};
#endif

#include "winsock2.h"
#define DESIRED_WINSOCK_VERSION        0x0101
#define MINIMUM_WINSOCK_VERSION        0x0001

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAKE_25P


IMPLEMENT_DYNCREATE(CSdiCoreFileMgr, CWinThread)
	//------------------------------------------------------------------------------ 
	//! @brief		CSdiCoreFileMgr()
	//! @date		2013-11-28
	//! @author		hongsu.jung
	//! @note	 	constructor
	//------------------------------------------------------------------------------
	CSdiCoreFileMgr::CSdiCoreFileMgr(void)	
{
	m_bFileSaveThreadFlag = TRUE;
	m_bFileTransfer = TRUE;
	m_nRecordType = -1;
	m_bRecordEnd = FALSE;
	m_bThreadFlag = TRUE;	
	m_nCount = 0;
	m_bFileClose = TRUE;
	m_nFrameCnt = 0;
	m_nPrevTimeStamp = 0;
	m_nStartTime = 0;
	m_nIntervalSensorOn = 0;
	m_nResumeFrame = 0;
	m_nResumeMode = FALSE;
	m_nFrameRate = FRAMERATE30;
	InitializeCriticalSection (&m_csFileMgr);
	InitializeCriticalSection (&m_csBackup);
	m_bCloseBackupThread = FALSE;
	m_hBuackupThread = (HANDLE) _beginthreadex(NULL, 0, BackupThread, (void *)this, 0, NULL);


	/*m_pMovieMakeFileMgr = NULL;
	m_pMovieMakeFileMgr = new CESMParseMovieFileMgr();
	m_pMovieMakeFileMgr->CreateThread();*/

	//InitializeCriticalSection(&m_criRecord);
}

CSdiCoreFileMgr::~CSdiCoreFileMgr(void)
{
	//DeleteCriticalSection(&m_criRecord);
	StopThread();
	//-- Exit Thread (Confirm)
	ExitInstance();
}

BOOL CSdiCoreFileMgr::InitInstance() 
{ 
	// 	m_bCloseBackupThread = FALSE;
	// 	m_hBuackupThread = (HANDLE) _beginthreadex(NULL, 0, BackupThread, (void *)this, 0, NULL);
	return TRUE; 
}

int CSdiCoreFileMgr::ExitInstance()				
{
	DeleteCriticalSection (&m_csFileMgr);
	m_bCloseBackupThread = TRUE;
	WaitForSingleObject(m_hBuackupThread, 1000);
	DeleteCriticalSection (&m_csBackup);
	return CWinThread::ExitInstance();
}

//------------------------------------------------------------------------------ 
//! @brief		StopThread
//! @date		2010-06-29
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CSdiCoreFileMgr::StopThread()
{
	if(!m_bThreadRun)		
		return;

	m_bThreadFlag = FALSE;	
	while(m_bThreadRun)
		Sleep(1);

	RemoveAll();
	//-- 2013-12-01 hongsu@esmlab.com
	//-- Confirm Stop Thread
	WaitForSingleObject(this->m_hThread, INFINITE) ;
}

//------------------------------------------------------------------------------ 
//! @brief		DivJPEGSave
//! @date		2015-06-03
//! @author		joonho.kim
//! @note	 	Save jpeg data
//------------------------------------------------------------------------------ 
BOOL CSdiCoreFileMgr::DivJPEGSave(CString strPath, BYTE* pBuffer, int nSize)
{
	CFile m_file;
	DWORD offest = 0;

	ReceiveFrameData* imageData = new ReceiveFrameData;
	int nCurFrameCount  = 0;
	if(m_strPrevPath != strPath) // First Frame 
	{
		m_nFrameCnt = 0;
		//		m_nResumeFrame = 0;
		m_nPrevTimeStamp = 0;
	}

	int nSaveCount = 0;
	while(1)
	{
		if(offest >= nSize )
			break;

		memset(imageData, 0x00, sizeof(ReceiveFrameData));
		memcpy(&imageData->nTimeStamp, pBuffer+offest, 0x04);    
		memcpy(&imageData->nSize, pBuffer+offest+0x04, 0x04);    
		imageData->nImageData = (char*)(pBuffer+offest)+0x04+0x04;

		offest = offest + imageData->nSize+0x08;

		if(imageData->nSize == 8)
			continue;

		//CMiLRe 20150605 JPEG FileTrans
		if(imageData->nSize < 0 || imageData->nTimeStamp < 0)
		{
			TRACE(_T(" ### imageData->nSize[%d] < 0 || imageData->nTimeStamp[%d] < 0 ### \n"), imageData->nSize, imageData->nTimeStamp);
			continue;
		}

		// 		if( imageData->nSize > 150000)
		// 			nFrameRate = FRAMERATE12;
		// 		else if( imageData->nSize < 150000)
		//  			nFrameRate = FRAMERATE30;


		//m_nFrameRate = FRAMERATE12;
		//TRACE(_T("FrameRate [%d]   image size[%d]\n"), nFrameRate, imageData->nSize);

		VerifyNume( m_nPrevTimeStamp, imageData->nTimeStamp, m_nFrameCnt, &nCurFrameCount, m_nFrameRate);
		if( nCurFrameCount == 0)
		{
			VerifySensorTime(imageData->nTimeStamp, strPath);
		}
		//TRACE(_T("m_nFrameCnt [%d], m_nResumeFrame [%d]   inCurFrameCount[%d]\n"), m_nFrameCnt, m_nResumeFrame, nCurFrameCount);

		//m_nFrameCnt = nCurFrameCount;
		m_nPrevTimeStamp = imageData->nTimeStamp;
		if (m_nFrameCnt == nCurFrameCount)
			continue;			//1 OK
		else
			m_nFrameCnt = nCurFrameCount;


		if(m_nResumeMode) //Puase Mode 확인
		{
			if(m_nResumeFrame == 0 || m_nResumeFrame >= nCurFrameCount)
			{
				//TRACE(_T("continue m_nResumeFrame [%d], nCurFrameCount [%d], nCurFrameCount-m_nResumeFrame [%d]\n"), m_nResumeFrame, nCurFrameCount, nCurFrameCount-m_nResumeFrame);
				continue;		//2
			}
		}
		else
			m_nResumeFrame = 0;

		//TRACE(_T("Save m_nResumeFrame [%d], nCurFrameCount [%d], nCurFrameCount-m_nResumeFrame [%d]\n"), m_nResumeFrame, nCurFrameCount, nCurFrameCount-m_nResumeFrame);
		if (nCurFrameCount-m_nResumeFrame > 1800)
			continue;			//3
		else
			nSaveCount++;

		CString strImage;
		strImage.Format(_T("%s_%d.jpg"), strPath, nCurFrameCount-m_nResumeFrame);
		//strImage.Format(_T("%s_%d.jpg"), strPath, m_nFrameCnt);
		int fp;
		if((fp = _topen(strImage, _O_CREAT | _O_BINARY | _O_RDWR, 0777)) != -1)
		{
			_write(fp, imageData->nImageData, imageData->nSize);
			_commit(fp);
			_close(fp);
		}
	}
	m_strPrevPath = strPath;
	delete[] imageData;
	imageData = NULL;

	if( nSaveCount >0)
		return TRUE;
	else
		return FALSE;
}

BOOL CSdiCoreFileMgr::VerifySensorTime(int nImageTimeStamp, CString strPath)
{
	m_nStartTime *= 0.1;
	m_nIntervalSensorOn *= 0.1;
	int nFrameTimeAferSensorOn = sync_frame_time_after_sensor_on * 0.1;

	TCHAR* cPath = {0,};
	cPath = new TCHAR[_tcslen(strPath) + 1];
	_tcscpy( cPath, strPath );

	HWND hHwnd = AfxGetMainWnd()->GetSafeHwnd();

	RSEvent* pNewMsg	= new RSEvent();
	pNewMsg->pParam		= (LPARAM)cPath; 
	pNewMsg->nParam1	= m_nStartTime;
	pNewMsg->nParam2	= nImageTimeStamp;
	pNewMsg->message	= WM_RS_RC_INFORMSENSOROK;
	::SendMessage(hHwnd, WM_SDI, (WPARAM)WM_RS_RC_INFORMSENSOROK, (LPARAM)pNewMsg);

	return TRUE;
}

BOOL CSdiCoreFileMgr::VerifyNume(int nPrevTimeStamp, int nCurTimeStamp, int nPrevFrame, int* nCurFrame, int nFrameRate)
{
	int nVeryfyData = nCurTimeStamp - nPrevTimeStamp;
	double dFrameNum = 0.0;
	if( nVeryfyData != 0.0 && nPrevTimeStamp != 0)
	{
		if( nFrameRate == FRAMERATE30)
			dFrameNum = (double)nVeryfyData / 33.33;
		else if( nFrameRate == FRAMERATE12)
			dFrameNum = (double)nVeryfyData / 83.33;
		//dFrameNum = (double)nVeryfyData / 41.66;
		dFrameNum = dFrameNum + 0.5;
		// 		if( dFrameNum > 0)
		// 			dFrameNum = dFrameNum + 0.5;
		// 		else
		// 			dFrameNum = dFrameNum - 0.5;
	}

	int nFrameGap = (int)dFrameNum;
	*nCurFrame =  nPrevFrame + nFrameGap;

	//	TRACE(_T("Time Gap [%d], nFrameGap[%d], nPrevFrame[%d], nCurFrame[%d]\n"), nVeryfyData, nFrameGap, nPrevFrame, *nCurFrame);
	if( nVeryfyData == 33 && nVeryfyData == 34)
		return TRUE;

	return FALSE;
}

int CSdiCoreFileMgr::CheckFirstFile(CString strPath)
{
	int nPos = 0;
	int nIndex = 0;
	CString strToken;
	CString strFileNum;

	strPath = strPath.Left(strPath.GetLength()-4);
	while((strToken = strPath.Tokenize(_T("_"),nPos)) != "")
	{
		strFileNum = strToken;
	}

	

	return _ttoi(strFileNum);
}

typedef struct {
	CString strPath;
	CString strBackupPath;
	CSdiCoreFileMgr* pMovie;
	int nStartTime;
	int nIndex;
}MOVIE_SAVE_INFO;

int g_nArray[8] = {204,205,201,202,206,209,210,211};
int g_nStartTime[8];
CString g_strPath;
int g_nIndex = 0;
BOOL g_bStatus[8];

CString GetFrontIP()
{
	CString strData = _T("");
	CString strIP;
	WSADATA wsadata;

	if( !WSAStartup( DESIRED_WINSOCK_VERSION, &wsadata ) )
	{
		if( wsadata.wVersion >= MINIMUM_WINSOCK_VERSION )
		{
			HOSTENT *p_host_info;
			IN_ADDR in;
			char host_name[128]={0, };

			gethostname(host_name, 128);
			p_host_info = gethostbyname( host_name );

			if( p_host_info != NULL )
			{
				for( int i = 0; p_host_info->h_addr_list[i]; i++ )
				{
					memcpy( &in, p_host_info->h_addr_list[i], 4 );
					strIP = inet_ntoa( in );


					CString strToken, strTemp;
					int nPos = 0, nCount = 0;
					while((strToken = strIP.Tokenize(_T("."),nPos)) != "")
					{
						if(nCount > 2)
							break;

						strTemp.Format(_T("%s."), strToken);
						strData.Append(strTemp);
						nCount++;
					}
				}
			}
		} 
		WSACleanup();
	}

	return strData;
}
unsigned WINAPI CSdiCoreFileMgr::MovieSaveThread(LPVOID param)
{
	MOVIE_SAVE_INFO* pData = (MOVIE_SAVE_INFO*)param;
	CString strBackupPath;

	CString strFrontIP = GetFrontIP();
	while(1)
	{
		Sleep(1);
		if(!g_bStatus[pData->nIndex])
		{
			//strBackupPath.Format(_T("\\\\192.168.0.%d\\movie\\%d.mp4"), g_nArray[pData->nIndex], g_nIndex);
			strBackupPath.Format(_T("\\\\%s%d\\movie\\%d.mp4"),strFrontIP, g_nArray[pData->nIndex], g_nIndex);
			CopyFile(g_strPath, strBackupPath, FALSE);
			TRACE(_T("SAVE FINISH %s [%d]\n"), strBackupPath, GetTickCount()-g_nStartTime[pData->nIndex]);
			g_bStatus[pData->nIndex] = TRUE;
			
		}
	}
	return 0;
}

//#ifdef FILE_SPLIT_TEST
int GetFindCharCount(CString parm_string, char parm_find_char)
{
	int length = parm_string.GetLength(), find_count = 0;

	for(int i = 0; i < length; i++)
	{ 
		if(parm_string[i] == parm_find_char)
		{
			find_count++;
		}
	}
	return find_count; 
}

unsigned WINAPI CSdiCoreFileMgr::CopyBackup(LPVOID param)
{
#ifdef _WIN64
	CSdiCoreFileMgr *pMain = (CSdiCoreFileMgr*)param;

	CString strTok, strTokWithExtension;

	int nStrCount = GetFindCharCount(pMain->m_strPath, '_');
	AfxExtractSubString(strTokWithExtension, pMain->m_strPath, nStrCount, '_');
	AfxExtractSubString(strTok, strTokWithExtension, 0, '.');

	int nStrLength = strTok.GetLength();

	float dGop = 10.f;
	float dFrameRate = 30.f;
	float dDurationTime = 2.f;
	float dSplitCount = 2;//(dDurationTime * dFrameRate) / dGop;

	if(pMain->m_strBackupPath.Find(strTokWithExtension))
	{
		for(int i = 0; i < dSplitCount; i++)
		{
			CString strNumber;
			int nNumber = _ttoi(strTok);
			nNumber = nNumber * dSplitCount + i;
			float dStartTime = dDurationTime / dSplitCount * i;
			float dEndTime = dDurationTime / dSplitCount * (i+1);
			strNumber.Format(_T("%d"), nNumber);

			CString strPath, strBackupPath;
			strPath = pMain->m_strPath;
			strBackupPath = pMain->m_strBackupPath;			

			CString strReplace = strNumber+_T(".mp4");
			strBackupPath.Replace(strTokWithExtension, strReplace);
			strPath.Replace(strTokWithExtension, strReplace);
			strPath.Replace(_T("\\Temp"), _T(""));

			pMain->SplitVideoOnGop(dStartTime, dEndTime, pMain->m_strPath, strPath);			
			CopyFile(strPath, strBackupPath, FALSE);
		}
	}
#endif
	return 0;
}

int CSdiCoreFileMgr::SplitVideoOnGop(double dStartSeconds, double dEndSeconds, CString strInputFilePath, CString strOutputFilePath)
{	
#ifdef _WIN64
	char* szInputFileName;
	char* szOutputFileName;

	wchar_t* wCharInputStr;
	wchar_t* wCharOutputStr;
	int      szLength;

	wCharInputStr = strInputFilePath.GetBuffer(strInputFilePath.GetLength());
	szLength = WideCharToMultiByte(CP_ACP, 0, wCharInputStr, -1, NULL, 0, NULL, NULL); 
	szInputFileName = new char[szLength];
	WideCharToMultiByte(CP_ACP, 0, wCharInputStr, -1, szInputFileName, szLength, 0,0);

	wCharOutputStr = strOutputFilePath.GetBuffer(strOutputFilePath.GetLength());
	szLength = WideCharToMultiByte(CP_ACP, 0, wCharOutputStr, -1, NULL, 0, NULL, NULL); 
	szOutputFileName = new char[szLength];
	WideCharToMultiByte(CP_ACP, 0, wCharOutputStr, -1, szOutputFileName, szLength, 0,0);

	AVOutputFormat *ofmt = NULL;
	AVFormatContext *ifmt_ctx = NULL, *ofmt_ctx = NULL;

	AVPacket pkt;
	int ret, i;

	av_register_all();

	if ((ret = avformat_open_input(&ifmt_ctx, szInputFileName, 0, 0)) < 0) {	
		fprintf(stderr, "Could not open input file '%s'", szInputFileName);
	}

	avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, szOutputFileName);	
	if (!ofmt_ctx) {		
		fprintf(stderr, "Could not create output context\n");
		ret = AVERROR_UNKNOWN;
	}		
	ofmt = ofmt_ctx->oformat;

	for (i = 0; i < ifmt_ctx->nb_streams; i++) {
		if(i == AVMEDIA_TYPE_VIDEO)
		{
			AVStream *in_stream = ifmt_ctx->streams[i];
			AVStream *out_stream = avformat_new_stream(ofmt_ctx, in_stream->codec->codec);
			if (!out_stream) {
				fprintf(stderr, "Failed allocating output stream\n");
				ret = AVERROR_UNKNOWN;
			}

			ret = avcodec_copy_context(out_stream->codec, in_stream->codec);		

			if (ret < 0) {
				fprintf(stderr, "Failed to copy context from input to output stream codec context\n");
			}
			out_stream->codec->codec_tag = 0;
			if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
				out_stream->codec->flags |= CODEC_FLAG_GLOBAL_HEADER;
		}
	}

	av_dump_format(ofmt_ctx, 0, szOutputFileName, 1);

	if (!(ofmt->flags & AVFMT_NOFILE)) {
		ret = avio_open(&ofmt_ctx->pb, szOutputFileName, AVIO_FLAG_WRITE);
		if (ret < 0) {
			fprintf(stderr, "Could not open output file '%s'", szOutputFileName);
		}
	}

	ret = avformat_write_header(ofmt_ctx, NULL);

	if (ret < 0) {
		fprintf(stderr, "Error occurred when opening output file\n");
	}

	ret = av_seek_frame(ifmt_ctx, -1, dStartSeconds*AV_TIME_BASE, AVSEEK_FLAG_ANY);
	if (ret < 0) {
		fprintf(stderr, "Error seek\n");		
	}	

	int64_t *dts_start_from = (int64_t*)malloc(sizeof(int64_t) * ifmt_ctx->nb_streams);
	memset(dts_start_from, 0, sizeof(int64_t) * ifmt_ctx->nb_streams);
	int64_t *pts_start_from = (int64_t*)malloc(sizeof(int64_t) * ifmt_ctx->nb_streams);
	memset(pts_start_from, 0, sizeof(int64_t) * ifmt_ctx->nb_streams);		

	int cnt = 0;
	int cnt_aud = 0;
	while (1) {
		AVStream *in_stream, *out_stream;

		ret = av_read_frame(ifmt_ctx, &pkt);
		if (ret < 0)
			break;

		if(pkt.stream_index == AVMEDIA_TYPE_VIDEO)
		{
			in_stream  = ifmt_ctx->streams[pkt.stream_index];
			out_stream = ofmt_ctx->streams[pkt.stream_index];

			if (av_q2d(in_stream->time_base) * pkt.pts > dEndSeconds) {
				av_free_packet(&pkt);
				break;
			}

			if(cnt == 0)
			{
				if (dts_start_from[pkt.stream_index] == 0)
					dts_start_from[pkt.stream_index] = pkt.dts;	
				if (pts_start_from[pkt.stream_index] == 0) 
					pts_start_from[pkt.stream_index] = pkt.pts;
			}

			/* copy packet */
			pkt.pts = av_rescale_q(pkt.pts - pts_start_from[pkt.stream_index], in_stream->time_base, out_stream->time_base);
			pkt.dts = av_rescale_q(pkt.dts - dts_start_from[pkt.stream_index], in_stream->time_base, out_stream->time_base);
			pkt.pts = pkt.dts;

			pkt.duration = (int)av_rescale_q((int64_t)pkt.duration, in_stream->time_base, out_stream->time_base);			
			pkt.pos = -1;

			/* copy packet */						
			printf("Packet : %d\n", cnt);
			cnt++;

			ret = av_write_frame(ofmt_ctx, &pkt);
			if (ret < 0) {
				fprintf(stderr, "Error muxing packet\n");
				break;
			}
			av_free_packet(&pkt);
		}
	}
	free(dts_start_from);
	free(pts_start_from);

	av_write_trailer(ofmt_ctx);
	avformat_close_input(&ifmt_ctx);

	/* close output */
	if (ofmt_ctx && !(ofmt->flags & AVFMT_NOFILE))
		avio_closep(&ofmt_ctx->pb);
	avformat_free_context(ofmt_ctx);

	if (ret < 0 && ret != AVERROR_EOF) {
		return 1;
	}
#endif
	return 0;
}
//#endif


unsigned WINAPI CSdiCoreFileMgr::FileSaveThread(LPVOID param)
{
	CSdiCoreFileMgr *pMain = (CSdiCoreFileMgr*)param;

	for(int i = 0; i < pMain->m_arFileData.size(); i++)
	{
		THREAD_FILEDATA* pFileData = pMain->m_arFileData.at(i);
		if(pFileData)
		{
			CFile file;

			if(file.Open( pFileData->m_strBackupPath, CFile::modeNoTruncate | CFile::modeWrite | CFile::modeCreate ,NULL))
			{

				file.Write(pFileData->m_pFileData, pFileData->m_nFileSize );
				file.Close();

				if(pFileData->m_pFileData)
				{
					delete pFileData->m_pFileData;
					pFileData->m_pFileData = NULL;
				}
			}
			
			delete pFileData;
			pFileData = NULL;
		}
	}

	pMain->m_arFileData.clear();
	pMain->m_bFileSaveThreadFlag = TRUE;
	return 0;
}

unsigned WINAPI CSdiCoreFileMgr::FileSaveThread2(LPVOID param)
{
	THREAD_FILEDATA* pFileData = (THREAD_FILEDATA*)param;
	if(pFileData)
	{
		CFile file;
		if(file.Open( pFileData->m_strSavePath, CFile::modeNoTruncate | CFile::modeWrite | CFile::modeCreate ,NULL))
		{
			file.Write(pFileData->m_pFileData, pFileData->m_nFileSize );
			file.Close();

			//Write Done
			CString* pStrData = NULL;
			pStrData = new CString;
			pStrData->Format(_T("%s"), pFileData->m_strSavePath);
			RSEvent* pMsg	= new RSEvent;
			pMsg->message	= WM_CAPTURE_FILE_WRITE_DONE;
			pMsg->pParam	= (LPARAM)pStrData;
			::SendMessage(pFileData->_hwnd, WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);

			if(pFileData->m_pFileData)
			{
				delete pFileData->m_pFileData;
				pFileData->m_pFileData = NULL;
			}

			if(pFileData->bRTSP == FALSE)
			{
				if(!pFileData->m_strBackupPath.IsEmpty())
					CopyFile(pFileData->m_strSavePath, pFileData->m_strBackupPath, FALSE);
			}
		}

		delete pFileData;
		pFileData = NULL;
	}

	


	return 0;
}

//------------------------------------------------------------------------------ 
//! @brief		Run
//! @date		2010-05-30
//! @author		hongsu.jung
//! @note	 	get message
//------------------------------------------------------------------------------ 

int CSdiCoreFileMgr::Run(void)
{
	BOOL bRelease = TRUE;
	SdiFileInfo* pFileInfo = NULL;

	m_bThreadRun = TRUE;
	m_bAutoDelete = FALSE;

	int nAll;
	int nSaveCount = 0;

	for(int i = 0; i < 8; i++)
		g_bStatus[i] = FALSE;

	while(m_bThreadFlag)
	{
		//-- 2013-03-01 honsgu.jung
		//-- For CPU
		Sleep(1);

		//-- 2014-09-10 hongsu@esmlab.com
		//-- Lock
		EnterCriticalSection (&m_csFileMgr);

		nAll = (int)m_arMsg.GetSize();
		while(nAll--)
		{
			pFileInfo = (SdiFileInfo*)m_arMsg.GetAt(0);
			if(!pFileInfo)
				continue;

			CFile file;
			if(pFileInfo->nAppendMsg == WM_SDI_OP_FILE_TRANSFER) // Movie Append
			{
#ifdef JPEG_RECORD
				//-- 2015.06.03 joonho.kim
				//-- jpeg div save
				CString strData, strPath, strBackupPath;
				strData = pFileInfo->strImageFileName;
				AfxExtractSubString(strPath, strData, 0, '@');
				AfxExtractSubString(strBackupPath, strData, 1, '@');


				if(DivJPEGSave(strPath, pFileInfo->pImageBuffer, pFileInfo->nImageSize))
				{
					//20150625 yongmin strPrevPath != strPath
					// 				if(pFileInfo->nmdat > 0)
					// 					m_nFrameCnt = 0;

					// 이부분을 Server Backup Path에 저장.
					// 				if(!file.Open(strBackupPath, CFile::modeNoTruncate | CFile::modeWrite | CFile::modeCreate ,NULL))
					// 				{
					// 					continue;
					// 				}
					// 				file.SeekToEnd();
					// 				file.Write(pFileInfo->pImageBuffer, pFileInfo->nImageSize);
					// 				file.Flush();
					// 				file.Close();
					ImageData stImageData;
					stImageData.pstPath = new TCHAR[strBackupPath.GetLength() + 1];
					stImageData.pBuffer = new  BYTE[pFileInfo->nImageSize];
					memcpy(stImageData.pBuffer, pFileInfo->pImageBuffer, pFileInfo->nImageSize);
					_tcscpy(stImageData.pstPath, strBackupPath);
					stImageData.nImageSize = pFileInfo->nImageSize;

					EnterCriticalSection (&m_csBackup);
					m_arrImageData.push(stImageData);
					LeaveCriticalSection (&m_csBackup);

					TCHAR* cPath = {0,};
					cPath = new TCHAR[_tcslen(pFileInfo->strImageFileName) + 1];
					_tcscpy( cPath, pFileInfo->strImageFileName );

					HWND hHwnd = AfxGetMainWnd()->GetSafeHwnd();
					RSEvent* pMsg	= new RSEvent;
					pMsg->message	= WM_RS_RC_SETFRAMECOUNT;
					pMsg->pParam	= (LPARAM)cPath;   
					::SendMessage(hHwnd, WM_SDI, (WPARAM)WM_RS_RC_SETFRAMECOUNT, (LPARAM)pMsg);	
				}
				// 				else
				// 				{
				// 					AfxMessageBox(_T("AAAAAAAAAA"));
				// 				}

#else

				CString strData, strPath, strBackupPath, str4DA, strMovie, strRTSP;
				BOOL bRTSP;
				strData = pFileInfo->strImageFileName;
				AfxExtractSubString(strPath, strData, 0, '@');
				AfxExtractSubString(strBackupPath, strData, 1, '@');
				AfxExtractSubString(strRTSP, strData, 2, '@');
				bRTSP = _ttoi(strRTSP);
				//AfxExtractSubString(str4DA, strData, 2, '@');
				//AfxExtractSubString(strMovie, strData, 3, '@');

				//25P Check
				pFileInfo->nFileNum = CheckFirstFile(strPath);
				if(pFileInfo->nFileNum == 0)
				{
					TRACE(_T("First File %s\n"), strPath);
					m_MovieMaker.m_arrCurrentFrame.clear();
					m_bRecordEnd = FALSE;
					//m_nRecordType = m_MovieMaker.IsCheckFileFPS(pFileInfo);
					m_nRecordType = pFileInfo->nMovieSize;
				}
				
				//joonho.kim
				if(pFileInfo->nModel == SDI_MODEL_GH5)
				{
					BOOL bCheckFile = TRUE;
					//M:\\ Save

					try
					{
						//Drive Check
						int nPos = 0;
						CString strToken, strDrive;
						while((strToken = strPath.Tokenize(_T(":"),nPos)) != "")
						{
							strDrive.Format(_T("%s"), strToken);
							break;
						}

						ULARGE_INTEGER FreeByteAvailable;
						ULARGE_INTEGER unTotalSize;
						ULARGE_INTEGER unFreeSize;
						strDrive.Append(_T(":\\"));
						GetDiskFreeSpaceEx(strDrive ,&FreeByteAvailable, &unTotalSize, &unFreeSize);


						uint64_t nFreeSize = ((uint64_t)((unFreeSize.QuadPart)));

						if(pFileInfo->nImageSize < nFreeSize)
						{

#if 0
							//joonho.kim 180609 StopFileTransfer
							//file.Write(pFileInfo->pImageBuffer, pFileInfo->nImageSize);
							if(m_bFileTransfer)
							{
								if(m_arFileData.size())
								{
									if(m_bFileSaveThreadFlag)
									{
										m_bFileSaveThreadFlag = FALSE;
										HANDLE hSyncTime = NULL;
										hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, FileSaveThread, this, 0, NULL);
										CloseHandle(hSyncTime);
									}	
								}

								file.Write(pFileInfo->pImageBuffer, pFileInfo->nImageSize);
							}
							else
							{
								THREAD_FILEDATA* pFileData = new THREAD_FILEDATA;
								pFileData->m_pFileData = new BYTE[pFileInfo->nImageSize];
								pFileData->m_strSavePath = strPath;
								pFileData->m_strBackupPath = strBackupPath;
								pFileData->m_nFileSize = pFileInfo->nImageSize;

								memcpy(pFileData->m_pFileData, pFileInfo->pImageBuffer, pFileInfo->nImageSize);
								m_arFileData.push_back(pFileData);
							}
							///////////////////////////////////////////
#else
							if(m_bFileTransfer)
							{
								THREAD_FILEDATA* pFileData = new THREAD_FILEDATA;
								pFileData->m_pFileData = new BYTE[pFileInfo->nImageSize];
								pFileData->m_strSavePath = strPath;
								pFileData->m_strBackupPath = strBackupPath;
								pFileData->m_nFileSize = pFileInfo->nImageSize;
								pFileData->_hwnd = AfxGetMainWnd()->GetSafeHwnd();
								pFileData->bRTSP = bRTSP;

								memcpy(pFileData->m_pFileData, pFileInfo->pImageBuffer, pFileInfo->nImageSize);
								HANDLE hSyncTime = NULL;
								hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, FileSaveThread2, pFileData, 0, NULL);
								CloseHandle(hSyncTime);

								if(m_arFileData.size())
								{
									if(m_bFileSaveThreadFlag)
									{
										m_bFileSaveThreadFlag = FALSE;
										HANDLE hSyncTime = NULL;
										hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, FileSaveThread, this, 0, NULL);
										CloseHandle(hSyncTime);
									}	
								}
							}
							else
							{								
								THREAD_FILEDATA* pFileData = new THREAD_FILEDATA;
								pFileData->m_pFileData = new BYTE[pFileInfo->nImageSize];
								pFileData->m_strSavePath = strPath;
								pFileData->m_strBackupPath = strBackupPath;
								pFileData->m_nFileSize = pFileInfo->nImageSize;

								memcpy(pFileData->m_pFileData, pFileInfo->pImageBuffer, pFileInfo->nImageSize);
								m_arFileData.push_back(pFileData);
							}
							
#endif
						}
						else
						{
							bCheckFile = FALSE;
							CString* pStrData = NULL;
							pStrData = new CString;
							pStrData->Format(_T("%s"), strBackupPath);

							BYTE* pBuf = new BYTE[pFileInfo->nImageSize];
							memcpy(pBuf, pFileInfo->pImageBuffer, pFileInfo->nImageSize);

							RSEvent* pMsg	= new RSEvent;
							pMsg->message	= WM_CAPTURE_FILE_WRITE_ERROR;
							pMsg->nParam1	= pFileInfo->nImageSize;
							pMsg->nParam2	= pFileInfo->nmdat;
							pMsg->nParam3	= pFileInfo->nOffset;
							pMsg->pParam	= (LPARAM)pStrData;
							pMsg->pDest		= (LPARAM)pBuf;
							::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
						}
					}
					catch(...)
					{
						bCheckFile = FALSE;
						CString* pStrData = NULL;
						pStrData = new CString;
						pStrData->Format(_T("%s"), strBackupPath);

						BYTE* pBuf = new BYTE[pFileInfo->nImageSize];
						memcpy(pBuf, pFileInfo->pImageBuffer, pFileInfo->nImageSize);

						RSEvent* pMsg	= new RSEvent;
						pMsg->message	= WM_CAPTURE_FILE_WRITE_ERROR;
						pMsg->nParam1	= pFileInfo->nImageSize;
						pMsg->nParam2	= pFileInfo->nmdat;
						pMsg->nParam3	= pFileInfo->nOffset;
						pMsg->pParam	= (LPARAM)pStrData;
						pMsg->pDest		= (LPARAM)pBuf;
						::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
					}


					BOOL bOptionRecordFileSplit = FALSE;					
					if(bCheckFile)
					{
						//jhhan File Temp
						if(!strBackupPath.IsEmpty())
						{
#ifdef FILE_SPLIT_TEST
							CESMSdiCoreIni pIni;							
							if(pIni.SetIniFilename(_T("C:\\Program Files\\ESMLab\\4DMaker\\config\\4DMaker.info")))
							{							
								bOptionRecordFileSplit = pIni.GetBoolean(_T("RemoteControl"), _T("RecordFileSplit"), FALSE);								

								if(bOptionRecordFileSplit)
								{
									m_strPath = strPath;
									m_strBackupPath = strBackupPath;
									HANDLE hSyncTime = NULL;
									hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, CopyBackup, this, 0, NULL);
									SetThreadAffinityMask(hSyncTime,1<<4);
									CloseHandle(hSyncTime);
								}
								else
									CopyFile(strPath, strBackupPath, FALSE);
							}
							else
								CopyFile(strPath, strBackupPath, FALSE);

#else
							//CopyFile(strPath, strBackupPath, FALSE);
#endif
						}
					}

					CString strLog;
					strLog.Format(_T("##########FILE Write Done %s\n"), strPath);
					TRACE(strLog);
					
					if(!bOptionRecordFileSplit)
					{
						/*CString* pStrData = NULL;
						pStrData = new CString;

						pStrData->Format(_T("%s"), strPath);
						RSEvent* pMsg	= new RSEvent;
						pMsg->message	= WM_CAPTURE_FILE_WRITE_DONE;
						pMsg->pParam	= (LPARAM)pStrData;
						::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);*/
					}
					else
					{
						CString strTempPath = strPath;
						CString strTok, strTokWithExtension;

						int nStrCount = GetFindCharCount(strTempPath, '_');
						AfxExtractSubString(strTokWithExtension, strTempPath, nStrCount, '_');
						AfxExtractSubString(strTok, strTokWithExtension, 0, '.');

						int nStrLength = strTok.GetLength();
						
						for(int i = 0; i < 2; i++)
						{	
							Sleep(500);
							CString strNumber;
							int nNumber = _ttoi(strTok);
							nNumber = nNumber * 2 + i;
							strNumber.Format(_T("%d"), nNumber);
												
							strTempPath = strPath;
							CString strReplace = strNumber+_T(".mp4");
							strTempPath.Replace(strTokWithExtension, strReplace);							
							strTempPath.Replace(_T("\\Temp"), _T(""));

							CString* pStrData = NULL;
							pStrData = new CString;

							pStrData->Format(_T("%s"), strTempPath);
							RSEvent* pMsg	= new RSEvent;
							pMsg->message	= WM_CAPTURE_FILE_WRITE_DONE;
							pMsg->pParam	= (LPARAM)pStrData;
							::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
							Sleep(500);
						}
					}
				}
				else //NX Series
				{
					int nMaxFrame;
					if( m_nRecordType == MOVIE_FPS_UHD_25P || 
						m_nRecordType == MOVIE_FPS_FHD_50P ||
						m_nRecordType == MOVIE_FPS_FHD_25P)
					{
#if 1
						

						if(m_nRecordType == MOVIE_FPS_UHD_25P)
							nMaxFrame = GOP_SIZE_UHD_25P;
						else if(m_nRecordType == MOVIE_FPS_FHD_25P)
							nMaxFrame = GOP_SIZE_FHD_25P;
						else if(m_nRecordType == MOVIE_FPS_FHD_50P)
							nMaxFrame = GOP_SIZE_FHD_50P;

						if(m_bRecordEnd)
						{
							m_MovieMaker.m_nFileIndex = 0;
							TRACE(_T("RECORD END!!!!\n"));
						}
						else
						{
							/*bRelease = FALSE;
							pFileInfo->strBackupPath = strBackupPath;
							m_pMovieMakeFileMgr->m_nFrameRate = m_nRecordType;
							m_pMovieMakeFileMgr->m_nMaxFrame = nMaxFrame;
							m_pMovieMakeFileMgr->AddMsg(pFileInfo);*/
							BOOL bRetry = FALSE;
							while(1)
							{
								m_MovieMaker.m_nFrameRate = m_nRecordType;
								m_MovieMaker.m_nMaxFrame = nMaxFrame;
								int nStart  = GetTickCount();
								strPath = m_MovieMaker.MakeMovieFile(pFileInfo, strBackupPath, bRetry);
								if(!strPath.IsEmpty())
								{
									/*if( strBackupPath != _T(""))
										m_pMovieMakeFileMgr->AddMsg(strPath, strBackupPath);*/
									if( strBackupPath != _T(""))
									{
										//EnterCriticalSection(&m_criRecord);
										CopyFile(strPath, strBackupPath, FALSE);
										//LeaveCriticalSection(&m_criRecord);
									}

									CString* pStrData = NULL;
									pStrData = new CString;

									pStrData->Format(_T("%s"), strPath);
									RSEvent* pMsg	= new RSEvent;
									pMsg->message	= WM_CAPTURE_FILE_WRITE_DONE;
									pMsg->pParam	= (LPARAM)pStrData;
									pMsg->nParam1 = GetTickCount() - nStart;

									::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
								}

								if(m_MovieMaker.m_arrCurrentFrame.size() < nMaxFrame)
									break;
								/*else
									bRetry = TRUE;*/

							}
						}
#else
						BOOL bCheckFile = TRUE;
						//M:\\ Save
						strPath.Append(_T("_Temp"));
						if(!file.Open(strPath, CFile::modeNoTruncate | CFile::modeWrite | CFile::modeCreate ,NULL))
						{
							// 					CString strMsg;
							// 					strMsg.Format(_T("%s"), pFileInfo->strImageFileName);
							// 					AfxMessageBox(strMsg);
							continue;
						}
						//file.SeekToEnd();

						try
						{
							//Drive Check
							int nPos = 0;
							CString strToken, strDrive;
							while((strToken = strPath.Tokenize(_T(":"),nPos)) != "")
							{
								strDrive.Format(_T("%s"), strToken);
								break;
							}

							ULARGE_INTEGER FreeByteAvailable;
							ULARGE_INTEGER unTotalSize;
							ULARGE_INTEGER unFreeSize;
							strDrive.Append(_T(":\\"));
							GetDiskFreeSpaceEx(strDrive ,&FreeByteAvailable, &unTotalSize, &unFreeSize);


							uint64_t nFreeSize = ((uint64_t)((unFreeSize.QuadPart)));

							if(pFileInfo->nImageSize < nFreeSize)
								file.Write(pFileInfo->pImageBuffer, pFileInfo->nImageSize);
							else
							{
								bCheckFile = FALSE;
								CString* pStrData = NULL;
								pStrData = new CString;
								pStrData->Format(_T("%s"), strBackupPath);

								BYTE* pBuf = new BYTE[pFileInfo->nImageSize];
								memcpy(pBuf, pFileInfo->pImageBuffer, pFileInfo->nImageSize);

								RSEvent* pMsg	= new RSEvent;
								pMsg->message	= WM_CAPTURE_FILE_WRITE_ERROR;
								pMsg->nParam1	= pFileInfo->nImageSize;
								pMsg->nParam2	= pFileInfo->nmdat;
								pMsg->nParam3	= pFileInfo->nOffset;
								pMsg->pParam	= (LPARAM)pStrData;
								pMsg->pDest		= (LPARAM)pBuf;
								::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
							}
						}
						catch(...)
						{
							bCheckFile = FALSE;
							CString* pStrData = NULL;
							pStrData = new CString;
							pStrData->Format(_T("%s"), strBackupPath);

							BYTE* pBuf = new BYTE[pFileInfo->nImageSize];
							memcpy(pBuf, pFileInfo->pImageBuffer, pFileInfo->nImageSize);

							RSEvent* pMsg	= new RSEvent;
							pMsg->message	= WM_CAPTURE_FILE_WRITE_ERROR;
							pMsg->nParam1	= pFileInfo->nImageSize;
							pMsg->nParam2	= pFileInfo->nmdat;
							pMsg->nParam3	= pFileInfo->nOffset;
							pMsg->pParam	= (LPARAM)pStrData;
							pMsg->pDest		= (LPARAM)pBuf;
							::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
						}

						char* pmdat;
						if(pFileInfo->nmdat > 0)
						{
							file.Seek(pFileInfo->nOffset , CFile::begin); // mdat Write
							pmdat = (char*)&pFileInfo->nmdat;

							file.Write(&pmdat[3], 1);
							file.Write(&pmdat[2], 1);
							file.Write(&pmdat[1], 1);
							file.Write(&pmdat[0], 1);

							m_nCount++;
						}

						file.Flush();
						file.Close();

						if(m_nRecordType == MOVIE_FPS_UHD_25P)
							nMaxFrame = GOP_SIZE_UHD_25P;
						else if(m_nRecordType == MOVIE_FPS_FHD_25P)
							nMaxFrame = GOP_SIZE_FHD_25P;
						else if(m_nRecordType == MOVIE_FPS_FHD_50P)
							nMaxFrame = GOP_SIZE_FHD_50P;

						m_pMovieMakeFileMgr->m_nFrameRate = m_nRecordType;
						m_pMovieMakeFileMgr->m_nMaxFrame = nMaxFrame;
						//m_pMovieMakeFileMgr->AddMsg(strPath);
						SdiFileInfo* pTemp = new SdiFileInfo;
						pTemp->strBackupPath = strBackupPath;
						pTemp->strTempPath = strPath;
						pTemp->nFileNum = pFileInfo->nFileNum;
						memcpy(pTemp->strImageFileName, pFileInfo->strImageFileName, MAX_PATH);
						m_pMovieMakeFileMgr->AddMsg(pTemp);

#endif
					}
					else if( m_nRecordType == -1)
					{
						TRACE(_T("m_nRecordType Error!!!!\n"));
					}
					else //30P
					{

						BOOL bCheckFile = TRUE;
						//M:\\ Save
						if(!file.Open(strPath, CFile::modeNoTruncate | CFile::modeWrite | CFile::modeCreate ,NULL))
						{
							// 					CString strMsg;
							// 					strMsg.Format(_T("%s"), pFileInfo->strImageFileName);
							// 					AfxMessageBox(strMsg);
							continue;
						}
						//file.SeekToEnd();

						try
						{
							//Drive Check
							int nPos = 0;
							CString strToken, strDrive;
							while((strToken = strPath.Tokenize(_T(":"),nPos)) != "")
							{
								strDrive.Format(_T("%s"), strToken);
								break;
							}

							ULARGE_INTEGER FreeByteAvailable;
							ULARGE_INTEGER unTotalSize;
							ULARGE_INTEGER unFreeSize;
							strDrive.Append(_T(":\\"));
							GetDiskFreeSpaceEx(strDrive ,&FreeByteAvailable, &unTotalSize, &unFreeSize);


							uint64_t nFreeSize = ((uint64_t)((unFreeSize.QuadPart)));

							if(pFileInfo->nImageSize < nFreeSize)
								file.Write(pFileInfo->pImageBuffer, pFileInfo->nImageSize);
							else
							{
								bCheckFile = FALSE;
								CString* pStrData = NULL;
								pStrData = new CString;
								pStrData->Format(_T("%s"), strBackupPath);

								BYTE* pBuf = new BYTE[pFileInfo->nImageSize];
								memcpy(pBuf, pFileInfo->pImageBuffer, pFileInfo->nImageSize);

								RSEvent* pMsg	= new RSEvent;
								pMsg->message	= WM_CAPTURE_FILE_WRITE_ERROR;
								pMsg->nParam1	= pFileInfo->nImageSize;
								pMsg->nParam2	= pFileInfo->nmdat;
								pMsg->nParam3	= pFileInfo->nOffset;
								pMsg->pParam	= (LPARAM)pStrData;
								pMsg->pDest		= (LPARAM)pBuf;
								::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
							}
						}
						catch(...)
						{
							bCheckFile = FALSE;
							CString* pStrData = NULL;
							pStrData = new CString;
							pStrData->Format(_T("%s"), strBackupPath);

							BYTE* pBuf = new BYTE[pFileInfo->nImageSize];
							memcpy(pBuf, pFileInfo->pImageBuffer, pFileInfo->nImageSize);

							RSEvent* pMsg	= new RSEvent;
							pMsg->message	= WM_CAPTURE_FILE_WRITE_ERROR;
							pMsg->nParam1	= pFileInfo->nImageSize;
							pMsg->nParam2	= pFileInfo->nmdat;
							pMsg->nParam3	= pFileInfo->nOffset;
							pMsg->pParam	= (LPARAM)pStrData;
							pMsg->pDest		= (LPARAM)pBuf;
							::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
						}

						char* pmdat;
						if(pFileInfo->nmdat > 0)
						{
							file.Seek(pFileInfo->nOffset , CFile::begin); // mdat Write
							pmdat = (char*)&pFileInfo->nmdat;

							file.Write(&pmdat[3], 1);
							file.Write(&pmdat[2], 1);
							file.Write(&pmdat[1], 1);
							file.Write(&pmdat[0], 1);

							m_nCount++;
						}

						file.Flush();
						file.Close();

						if(bCheckFile)
						{
							//jhhan File Temp
							if(!strBackupPath.IsEmpty())
								CopyFile(strPath, strBackupPath, FALSE);
						}

						CString strLog;
						strLog.Format(_T("##########FILE Write Done %s\n"), strPath);
						TRACE(strLog);

						CString* pStrData = NULL;
						pStrData = new CString;

						pStrData->Format(_T("%s"), strPath);
						RSEvent* pMsg	= new RSEvent;
						pMsg->message	= WM_CAPTURE_FILE_WRITE_DONE;
						pMsg->pParam	= (LPARAM)pStrData;
						::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
					}
				}
#endif
			}
			else if(pFileInfo->nAppendMsg == WM_SDI_OP_IMAGE_TRANSFER) // Image Append
			{
				CString strData, strPath, strBackupPath;
				strData = pFileInfo->strImageFileName;
				AfxExtractSubString(strPath, strData, 0, '@');
				AfxExtractSubString(strBackupPath, strData, 1, '@');

				if(!file.Open(strPath, CFile::modeNoTruncate | CFile::modeWrite | CFile::modeCreate ,NULL))
					continue;
				file.SeekToEnd();
				file.Write(pFileInfo->pImageBuffer, pFileInfo->nImageSize);

				file.Flush();
				file.Close();

				if(pFileInfo->nClose > 0)
				{
					RSEvent* pMsg	= new RSEvent;
					pMsg->message	= WM_CAPTURE_FILE_WRITE_DONE;
					pMsg->nParam1	= TRUE;
					::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
				}
				else
				{
					RSEvent* pMsg	= new RSEvent;
					pMsg->message	= WM_CAPTURE_FILE_WRITE_DONE;
					pMsg->nParam1	= FALSE;
					::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
				}

				if( strBackupPath != _T(""))
					CopyFile(strPath, strBackupPath, FALSE);
			}
			else if(pFileInfo->nAppendMsg == WM_SDI_OP_MOVIE_TRANSFER ) // Movie Append
			{
				if(!file.Open(pFileInfo->strImageFileName, CFile::modeNoTruncate | CFile::modeWrite | CFile::modeCreate ,NULL))
					continue;
				file.SeekToEnd();
				file.Write(pFileInfo->pImageBuffer, pFileInfo->nImageSize);

				file.Flush();
				file.Close();

				if(pFileInfo->nClose > 0)
				{
					m_nCount++; //다음파일로 변경
					RSEvent* pMsg	= new RSEvent;
					pMsg->message	= WM_RECORD_POPUP_DLG;
					pMsg->nParam1	= TRUE;
					pMsg->nParam2	= TRUE;
					pMsg->pParam	= (LPARAM)&pFileInfo->strImageFileName;
					::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
				}
			}
			else
			{
				if(!file.Open(pFileInfo->strImageFileName, CFile::modeWrite | CFile::modeCreate ,NULL))
					continue;
				file.Write(pFileInfo->pImageBuffer, pFileInfo->nImageSize);  // movie write

				file.Flush();
				file.Close();
			}

			if(bRelease)
			{
				delete[] pFileInfo->pImageBuffer;
				delete pFileInfo;
			}
			m_arMsg.RemoveAt(0);
		}

		//-- 2014-09-10 hongsu@esmlab.com
		//-- Unlock
		LeaveCriticalSection (&m_csFileMgr);
	}

	// [11/27/2013 Administrator]
	// Remove Array
	RemoveAll();

	m_bThreadRun = FALSE;	
	return 0;
}

void CSdiCoreFileMgr::AddFile(SdiFileInfo* pFileInfo)
{
	//-- 2014-09-10 hongsu@esmlab.com
	//-- Lock
	EnterCriticalSection (&m_csFileMgr);
	m_arMsg.Add((CObject*)pFileInfo);
	//-- 2014-09-10 hongsu@esmlab.com
	//-- Unlock
	LeaveCriticalSection (&m_csFileMgr);
}

void CSdiCoreFileMgr::RemoveAll()
{
	SdiFileInfo* pFileInfo = NULL;

	//-- 2014-09-10 hongsu@esmlab.com
	//-- Lock
	EnterCriticalSection (&m_csFileMgr);

	int nAll = m_arMsg.GetSize();
	while(nAll--)
	{
		pFileInfo = (SdiFileInfo*)m_arMsg.GetAt(nAll);
		if(pFileInfo)
			delete pFileInfo;
		m_arMsg.RemoveAt(nAll);
	}
	m_arMsg.RemoveAll();

	//-- 2014-09-10 hongsu@esmlab.com
	//-- Unlock
	LeaveCriticalSection (&m_csFileMgr);
}

unsigned WINAPI CSdiCoreFileMgr::BackupThread(LPVOID param)
{	
	CSdiCoreFileMgr* pCoreFileMgr= (CSdiCoreFileMgr*)param;

	while(1)
	{
		EnterCriticalSection (&pCoreFileMgr->m_csBackup);
		if( !pCoreFileMgr->m_arrImageData.empty())
		{
			ImageData stImageData = pCoreFileMgr->m_arrImageData.front();
			pCoreFileMgr->m_arrImageData.pop();
			LeaveCriticalSection (&pCoreFileMgr->m_csBackup);

			CFile file;
			// 이부분을 Server Backup Path에 저장.
			if(!file.Open(stImageData.pstPath, CFile::modeNoTruncate | CFile::modeWrite | CFile::modeCreate ,NULL))
			{
				delete[] stImageData.pstPath;
				delete[] stImageData.pBuffer;
				continue;
			}
			file.SeekToEnd();
			file.Write(stImageData.pBuffer, stImageData.nImageSize);
			file.Flush();
			file.Close();

			delete[] stImageData.pstPath;
			delete[] stImageData.pBuffer;
		}
		else
			LeaveCriticalSection (&pCoreFileMgr->m_csBackup);

		if( pCoreFileMgr->m_bCloseBackupThread )
			break;
		Sleep(10);

	}

	return 0;
}
