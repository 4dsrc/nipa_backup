////////////////////////////////////////////////////////////////////////////////
//
//	SdiCoreFileMgr.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-11-28
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

//#define _FILE_TEMP
//#define FILE_SPLIT_TEST

#include <Afxtempl.h>
//-- 2011-06-14
//-- time measure
#include <sys/timeb.h>
#include <time.h>

//#include "ESMParseMovieFileMgr.h"
#include "MovieInfoMake.h"
#include "SdiDefines.h"
#include "SetupAPI.h"
#include "ESMDefine.h"
#include "ESMSdiCoreIni.h"
#include <queue>
using namespace std;
struct ImageData
{
	TCHAR* pstPath;
	BYTE* pBuffer;
	int nImageSize;
};

struct THREAD_FILEDATA
{
	BYTE* m_pFileData;
	int m_nFileSize;
	CString m_strSavePath;
	CString m_strBackupPath;
	HWND _hwnd;
	BOOL bRTSP;
};

class AFX_EXT_CLASS CSdiCoreFileMgr: public CWinThread
{
	DECLARE_DYNCREATE(CSdiCoreFileMgr)
public:
	//CRITICAL_SECTION m_criRecord;
	//CESMParseMovieFileMgr* m_pMovieMakeFileMgr;
	int m_nRecordType;
	BOOL m_bFileTransfer;
	BOOL m_bFileSaveThreadFlag;
	BOOL m_bRecordEnd;
	int m_nCount;
	BOOL m_bFileClose;
	BOOL m_bGPUMakeFile;
	CString m_strPath;
	CString m_strBackupPath;

	CMovieInfoMake m_MovieMaker;

	//joonho.kim 180609 FileMemory
	vector<THREAD_FILEDATA*> m_arFileData;
private:	
	BOOL m_bThreadFlag;
	BOOL m_bThreadRun;

	//-- 2014-09-10 hongsu@esmlab.com
	//-- Lock
	CRITICAL_SECTION m_csFileMgr;
	CRITICAL_SECTION m_csBackup;

public:
	CSdiCoreFileMgr(void);	
	~CSdiCoreFileMgr(void);

protected:
	CObArray	m_arMsg;
	int m_nFrameCnt;
	CString m_strPrevPath;
	queue<ImageData> m_arrImageData;
	HANDLE m_hBuackupThread;
	BOOL m_bCloseBackupThread;
	static unsigned WINAPI BackupThread(LPVOID param);

	virtual BOOL InitInstance();	
	virtual int Run(void);	
	virtual int ExitInstance();	
	void RemoveAll();
	BOOL DivJPEGSave(CString strPath, BYTE* pData, int nSize);
	BOOL VerifyNume(int nTimeStamp1, int nTimeStamp2, int nPrevFrame, int* nCurFramem, int nFrameRate);
	BOOL VerifySensorTime(int nImageTimeStamp, CString strPath);

public:
	int m_nStartTime;
	int m_nIntervalSensorOn;
	int m_nPrevTimeStamp;
	int m_nResumeFrame;
	BOOL m_nResumeMode;
	int m_nFrameRate;
	void StopThread();
	void AddFile(SdiFileInfo* pFileInfo);
	void SetFps(int nFrameRate) {m_nFrameRate = nFrameRate;}
	int CheckFirstFile(CString strPath);
	static unsigned WINAPI MovieSaveThread(LPVOID param);
	static unsigned WINAPI CopyBackup(LPVOID param);
		
	//wgkim 171214
	int SplitVideoOnGop(double dStartSeconds, double dEndSeconds, CString strInputFilePath, CString strOutputFilePath);	

	static unsigned WINAPI FileSaveThread(LPVOID param);
	static unsigned WINAPI FileSaveThread2(LPVOID param);
};

