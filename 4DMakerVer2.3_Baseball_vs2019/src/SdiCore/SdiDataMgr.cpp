////
#include "StdAfx.h"
#include "SdiDataMgr.h"


SdiDataMgr::SdiDataMgr(void)
{
	m_bRunThread = TRUE;
}


SdiDataMgr::~SdiDataMgr(void)
{
	m_bRunThread = FALSE;
}

void SdiDataMgr::AddData(SdiFileInfo* pPicData)
{
	SdiFileInfo SdiData;
	SdiData.pImageBuffer = new BYTE[pPicData->nImageSize];
	memcpy(SdiData.pImageBuffer , pPicData->pImageBuffer, pPicData->nImageSize);
	SdiData.nImageSize = pPicData->nImageSize;
	//SdiData.strImageFileName = pPicData->strImageFileName;
	_tcscpy_s(SdiData.strImageFileName, pPicData->strImageFileName);
	m_arrPicData.push_back(SdiData);
}

unsigned WINAPI SdiDataMgr::ProtectPictureThread(LPVOID param)
{
	SdiDataMgr* pSdiDataMgr = (SdiDataMgr*)param;
	SdiFileInfo SdiData;
	while(1)
	{
		if(pSdiDataMgr->m_arrPicData.size() >= 1)
		{
			SdiData = pSdiDataMgr->m_arrPicData.at(pSdiDataMgr->m_arrPicData.size() - 1 );


	 		CFile file;
	 		file.Open(SdiData.strImageFileName, CFile::modeWrite | CFile::modeCreate ,NULL);
	 		//-- 2013-6-20 yongmin.lee
	 		file.Write(SdiData.pImageBuffer ,SdiData.nImageSize);
	 
	 		file.Flush();
	 		file.Close();

			delete SdiData.pImageBuffer;
			pSdiDataMgr->m_arrPicData.pop_back();
		}
		if( pSdiDataMgr->m_bRunThread == FALSE)
			break;

		Sleep(10);
	}
	return 0;
}