/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file SdiTypes.h
 \brief Define types used in PTP Component.
 \date 2009-06-11
 \version 0.70 
*/

#ifndef __PTP_TYPES_H__
#define __PTP_TYPES_H__

/***********************************************/ 
/* NULL                                        */
/***********************************************/
#ifndef NULL
#define NULL					((void *)0)
#endif

/***********************************************/ 
/* TRUE                                        */
/***********************************************/
#ifndef TRUE
#define TRUE					1
#else
#undef  TRUE
#define TRUE					1
#endif

/***********************************************/ 
/* FALSE                                       */
/***********************************************/
#ifndef FALSE
#define FALSE					0
#else
#undef  FALSE
#define FALSE					0
#endif

/***********************************************/ 
/* YES                                        */
/***********************************************/
#ifndef YES
#define YES						TRUE
#else
#undef  YES
#define YES						TRUE
#endif

/***********************************************/ 
/* NO                                        */
/***********************************************/
#ifndef NO
#define NO						FALSE
#else
#undef  NO
#define NO						FALSE
#endif

/***********************************************/ 
/* VOID                                        */
/***********************************************/
#ifndef SdiVoid
typedef void                     SdiVoid;
#endif


/***********************************************/ 
/* BOOL                                        */
/***********************************************/
#ifndef SdiBool
typedef int                     SdiBool;
#endif

/***********************************************/ 
/* CHARACTER                                   */
/***********************************************/
#ifndef SdiChar
typedef char                    SdiChar;
#endif

#ifndef SdiUChar
typedef unsigned char			SdiUChar;
#endif

/***********************************************/ 
/* INTEGER                                     */
/***********************************************/
#ifndef SdiInt
typedef int						SdiInt;
#endif

#ifndef SdiUInt
typedef unsigned int			SdiUInt;
#endif

#ifndef SdiInt8
typedef signed char				SdiInt8;
#endif


#ifndef SdiUInt8
typedef unsigned char			SdiUInt8;
#endif

#ifndef SdiInt16
typedef short					SdiInt16;
#endif

#ifndef SdiUInt16
typedef unsigned short			SdiUInt16;
#endif

#ifndef SdiInt32
typedef long int				SdiInt32;
#endif

#ifndef SdiUInt32
typedef unsigned long			SdiUInt32;
#endif

#ifndef SdiInt64
typedef long long				SdiInt64;
#endif

#ifndef SdiUInt64
typedef unsigned long long		SdiUInt64;
#endif

/***********************************************/ 
/* SHORT INTEGER                               */
/***********************************************/
#ifndef SdiShort
typedef signed short			SdiShort;
#endif

#ifndef SdiUShort
typedef unsigned short			SdiUShort;
#endif

/***********************************************/ 
/* LONG INTEGER                                */
/***********************************************/
#ifndef SdiLong
typedef signed long				SdiLong;
#endif

#ifndef SdiULong
typedef unsigned long			SdiULong;
#endif

/***********************************************/ 
/* DOUBLE                                */
/***********************************************/
#ifndef SdiDouble
typedef double				SdiDouble;
#endif

/***********************************************/ 
/* FLOAT                                       */
/***********************************************/
#ifndef SdiFloat32
typedef float                   SdiFloat32;
#endif

#ifndef SdiFloat64
typedef double					SdiFloat64;
#endif

/***********************************************/ 
/* DATE & TIME                                 */
/***********************************************/
#ifndef SdiDateTime
typedef char					SdiDateTime;
#endif

/***********************************************/ 
/* ERROR INTEGER                               */
/***********************************************/
#ifndef SdiError
typedef unsigned long			SdiError;
#endif

#endif  /* __PTP_TYPES_H__ */

