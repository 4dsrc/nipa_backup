#pragma once


#include "ESMFunc.h"
#include "SdiDefines.h"
#include "MovieInfoMake.h"
// CESMMakeMovieFileMgr

class CESMParseMovieFileMgr : public CWinThread
{
	DECLARE_DYNCREATE(CESMParseMovieFileMgr)

public:
	CESMParseMovieFileMgr();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CESMParseMovieFileMgr();

public:
	int m_nMaxFrame;
	int m_nFrameRate;
	CMovieInfoMake m_MovieMaker;
	CESMArray	m_arMsg;
	BOOL	m_bThreadStop;
	BOOL	m_bWait;
	CStringArray m_arrPath;
	CStringArray m_arrBackupPath;
	CRITICAL_SECTION m_criRecord;

public:
	void AddMsg(SdiFileInfo* pMsg);
	void AddMsg(CString strPath, CString strBackup);

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run(void);

protected:
	DECLARE_MESSAGE_MAP()
};


