/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        SdiCoreWin7.h
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */

#pragma once

#include "SdiCore.h"

#include <WindowsPTP.h>
#include <USBDevice.h>
#include <USBHAL_Instance.h>
#include <PTPApi.h>
#include <PTP_HOST.h>
#include "iphlpapi.h" 

#include <vector>
using namespace std;

//-----------Lib File

#pragma comment(lib, "IPHLPAPI.lib")

//-----------

class AFX_EXT_CLASS CSdiCoreWinXp :
	public CSdiCore
{
public:
	CSdiCoreWinXp(void);
	~CSdiCoreWinXp(void);

private:
	PTPHOST *m_pPTPHost;
 	PTPInfoType m_PTPInfo;

public:
	void RemoveThread() {};

	//-----------Function
	int InitPtpHost();

	int setStatusForPTPHOST(int sel,int value);
	int getStatusForPTPHOST(int sel);
	BOOL RegisterHandleForNotify(HWND hwnd);
	int ReceiveDeviceEvent(int sel,int nType,void* pParam1, void* pParam2);
	BOOL RegisterCallbackForEvent();
	void OnSystemDeviceChanged( WPARAM wParam, LPARAM lParam );
	void Init_ptphost();

	int RequireCaptureExec(WORD code);
	int CaptureCompleteExec(CString strSaveName);
	int MovieCompleteExec(CString strSaveName);	

	virtual SdiResult SdiInitSession(char* strDevName);
	virtual SdiResult SdiOpenSession();
	virtual SdiResult SdiExOpenSession();
	virtual SdiResult SdiCloseSession();

	virtual SdiResult SdiSendAdjCapture(SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size);
	virtual	SdiResult SdiSendAdjCapture(CString strSaveName);
	virtual	SdiResult SdiSendAdjLiveview(unsigned char* pYUV);
	virtual	SdiResult SdiSendAdjLiveviewInfo(RSLiveviewInfo* pLiveviewInfo);

	virtual SdiResult SdiSendShutterCapture(SdiInt8);
	virtual SdiResult SdiSendAdjCompleteCapture(CString strSaveName, bool &bIsLast, int nFileNum, int nClose);
	virtual SdiResult SdiSendAdjCompleteCapture(CString strSaveName, bool &bIsLast);
	//virtual SdiResult SdiSendAdjCompleteMovie(CString strSaveName, int nWaitSaveDSC);
	virtual SdiResult SdiSendAdjCompleteMovie(CString strSaveName, int nWaitSaveDSC, int nFilenNum, int nClose);
	virtual SdiResult SdiSendAdjMovieTransfer(CString strSaveName, int nfilenum, int nClose);
	virtual SdiResult SdiGetDevPropDesc(WORD wPropCode, WORD wAllFlag = 1);
	virtual SdiResult SdiSetDevPropValue(int nPTPCode, int nType, LPARAM lpValue);

	virtual SdiResult SdiGetDevUniqueID(int nPTPCode,  char **chUniqueID);
	virtual SdiResult SdiSetDevUniqueID(int nPTPCode,  CString strUUID);
	virtual SdiResult SdiGetDevModeIndex(int nPTPCode, int &nModeIndex);
	
	virtual SdiResult SetFocusPosition(int nCurPos);
	virtual SdiResult SdiSetFocusPosition(FocusPosition *ptFocusPos, SdiInt8 nType);
	virtual SdiResult SdiSetFocusPosition(SdiInt32 nValue);
	virtual SdiResult SdiGetFocusPosition(FocusPosition* ptFocusPos) {return 0;}

	virtual SdiResult SdiGetEvent(BYTE **pData,SdiInt32 *size);
	virtual SdiResult SdiCheckEvent(SdiInt32& nEventExist, SdiUIntArray* pArList=NULL);

	virtual SdiResult SdiSendAdjRequireCaptureOnce(int nModel = 0);

	virtual	SdiResult SdiFormatDevice();
	virtual	SdiResult SdiResetDevice(int nParam = 5);
	virtual	SdiResult SdiFWUpdate();
	//20150128 CMiLRe NX3000
	//CMiLRe 2015129 펌웨어 다운로드 경로 추가
	virtual	SdiResult SdiFWDownload(CString strPath);
	virtual	SdiResult SdiSensorCleaningDevice();
	//CMiLRe 20141113 IntervalCapture Stop 추가
	virtual	SdiResult SdiIntervalCaptureStop();
	//CMiLRe 20141127 Display Save Mode 에서 Wake up
	virtual	SdiResult SdiDisplaySaveModeWakeUp();

	virtual SdiResult SdiGetPTPDeviceInfo(PTPDeviceInfo * ptDevInfoType);
	virtual SdiResult SdiGetTick(SdiInt& pcTime, SdiDouble& deviceTime, SdiInt& nCheckCnt, SdiInt& nCheckTime, SdiInt nDSCSyncTime, HWND handle) {return 0;}
	virtual SdiResult SdiSetTick(SdiDouble nTick, SdiInt nIntervalSensorOn, SdiUIntArray* pArList=NULL) {return 0;}
	virtual SdiResult SdiHiddenCommand(SdiInt32 nCommand, SdiInt32 nValue) {return 0;}

	virtual SdiResult SdiInitGH5() {return 0;}
	virtual SdiResult SdiFocusSave() {return 0;}
	//-- 2014-07-14 joonho.kim
	virtual SdiResult SdiSetPause(int nPause) {return 0;}
	virtual SdiResult SdiSetResume(int nResume) {return 0;}
	virtual SdiResult SdiTrackingAFStop() {return 0;}
	virtual SdiResult SdiControlTouchAF(int nX, int nY) {return 0;}
	virtual	SdiResult SdiMovieCancle() {return 0;}
	virtual SdiResult SdiGetRecordStatus() {return 0;}
	virtual SdiResult SdiGetCaptureCount() {return 0;}
	virtual SdiResult SdiGetImagePath(CString& strPath) {return 0;}
	virtual SdiResult SdiFileTransfer(char* pPath, int nfilenum, int nmdat, int nOffset,int* nFileSize, int nMovieSize) {return 0;}
	virtual SdiResult SdiImageTransfer(CString strName, int nfilenum, int nClose) {return 0;}
	virtual SdiResult SdiImageTransferEX(CString strName, int nfilenum, int nClose) {return 0;}
	virtual SdiResult SdiSetEnLarge(int nLarge) {return 0;}
	virtual SdiResult SdiSetRecordPause(int nPause) {return 0;}
	virtual SdiResult SdiSetRecordResume(int nSkip) {return 0;}
	virtual SdiResult SdiSetLiveView(int nCommand) {return 0;}
	virtual SdiResult SdiFileReceiveComplete() {return 0;}
	virtual SdiResult SdiImageReceiveComplete() {return 0;}
	//-- 2013-12-01 hongsu@esmlab.com
	virtual	SdiBool SdiCheckGetFile() {return FALSE;}
	virtual SdiResult SdiLiveReceiveComplete() {return 0;}
	virtual	SdiBool SdiFileMgrSetStartTime(int nStartTime, int nIntervalSensorOn) {return 0;}
	virtual SdiResult SdiFileMgrGetEndTime() {return 0;}
	virtual	SdiBool SdiSetResumeFrame(int nResumeFrame) {return 0;}
	virtual SdiResult SdiGetResumeFrame() {return 0;}
	virtual	SdiBool SdiSetResumeMode(BOOL bResumeMode){return 0;}
	virtual	SdiBool SdiGetResumeMode() {return 0;}
	virtual	void SdiSyncStop() {return ;}
	virtual void SetFps(int nFps) {return ;};

	virtual	SdiResult SdiHalfShutter(int nRelease);

	virtual SdiResult SdiSetFocusFrame(int nX, int nY, int nMagnification);
	virtual SdiResult SdiGetFocusFrame(BYTE **pData,SdiInt32 *size);

	//NX3000
	virtual	SdiResult SdiDeviceReboot();
	virtual	SdiResult SdiDevicePowerOff();

	virtual	SdiResult SdiDeviceExportLog();
};
