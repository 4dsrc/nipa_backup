#include "StdAfx.h"
#include "MovieInfoMake.h"
#include "MovieInfo.h"
#include <vector>
//#define MAKE_TEMP_FILE



enum{
	INDEX_30P = 0,
	INDEX_FHD_25P,
	INDEX_UHD_25P,
	INDEX_FHD_50P,
};


CMovieInfoMake::CMovieInfoMake(void)
{
	m_nFileIndex = 0;
	g_nFileSize = 0;
	m_mdatOffset = 0;
	g_strm_pos = 0;
	mp4_buffer = 0;
	m_mp4BufferSize = 0;
	m_pFile = new CFile;
	mp4_buffer = new unsigned char[10000000];

	for(int i = 0; i < m_arrCurrentFrame.size(); i++)
	{
		m_arrCurrentFrame.at(i).pFrame = NULL;
		m_arrCurrentFrame.at(i).nFrameSize = 0;
		m_arrPreFrame.at(i).pFrame = NULL;
		m_arrPreFrame.at(i).nFrameSize = 0;
	}
	
	m_nRemainFrame = 0;
}


CMovieInfoMake::~CMovieInfoMake(void)
{
	if(mp4_buffer)
	{
		delete mp4_buffer;
		mp4_buffer = NULL;
	}
}

void CMovieInfoMake::ParseFrame(SdiFileInfo* pFileInfo)
{
	int nMaxCount;
	switch(m_nFrameRate)
	{
	case MOVIE_FPS_UHD_25P:
		nMaxCount = 22;
		break;
	case MOVIE_FPS_FHD_25P:
		nMaxCount = 30;
		break;
	case MOVIE_FPS_FHD_50P:
		nMaxCount = 30;
		break;
	}

	BYTE* nPos;
	int nFrame = 0;
	BYTE* pFileCount = NULL;
	BYTE FrameSize[4];
	int nCount;

	while(1)
	{
		if(nFrame == 0)
		{
			nPos = pFileInfo->pImageBuffer+64;
		}
		
		if(nFrame < nMaxCount)
		{
			//Frame 사이즈 얻기
			pFileCount = (BYTE*)nPos;
			FrameSize[0] = *(pFileCount+3);
			FrameSize[1] = *(pFileCount+2);
			FrameSize[2] = *(pFileCount+1);
			FrameSize[3] = *(pFileCount);
			memcpy(&nCount, FrameSize, 4);
			
			FRAME_ARRAY frameData;
			frameData.pFrame = new BYTE[nCount+4];
			frameData.nFrameSize = nCount;
			memcpy(frameData.pFrame, (BYTE*)nPos, nCount+4);
			int nSize = sizeof(frameData.pFrame);
			m_arrCurrentFrame.push_back(frameData);
			
			nPos += (nCount+4);
			nFrame++;
		}
		else
		{
			break;
		}		
	}
}
//CString g_strPath1;
CString CMovieInfoMake::MakeMovieFile(SdiFileInfo* pFileInfo, CString& strBackup, BOOL bRetry)
{
	if(!bRetry)
		ParseFrame(pFileInfo);

	if(m_arrCurrentFrame.size() < m_nMaxFrame)
	{
		return _T("");
	}


	ULONG t1,t2,t3,t4;
	ULONG t1_,t2_,t3_,t4_;


	t1 = GetTickCount();

	CString strData, strPath, strBackupPath, str4DA;
	strData = pFileInfo->strImageFileName;
	AfxExtractSubString(strPath, strData, 0, '@');
	AfxExtractSubString(strBackupPath, strData, 1, '@');
	AfxExtractSubString(str4DA, strData, 2, '@');

	CString strTemp;
	CString strFile;
	strTemp.Format(_T("%d.mp4"), pFileInfo->nFileNum);
	strFile.Format(_T("%d.mp4"), m_nFileIndex);
	strPath.Replace(strTemp,strFile);
	strBackupPath.Replace(strTemp,strFile);
	strBackup = strBackupPath;

	BYTE FrameSize[4];
	int nPos = 0;
	int nTotalCount = 0;
		
	LPCSTR lpszA = (LPSTR)(LPCTSTR)strPath;
	m_pFile->Open((LPCTSTR)lpszA, CFile::modeCreate|CFile::modeReadWrite);
	//g_strPath1 = strPath;
	int nFrame = 0;
	CFile readFile;
	BYTE* buf = NULL;
	FRAME_ARRAY frame_data;

	while(1)
	{
		frame_data = m_arrCurrentFrame.at(0);
		if(nFrame == 0)
		{
			nPos = (int)buf+64;
			nTotalCount += 64;
			g_nFileSize += 64;
			Write_ftyp();
		}

		m_nArray[nFrame] = frame_data.nFrameSize+4;
		memcpy(mp4_buffer+g_strm_pos, frame_data.pFrame, frame_data.nFrameSize+4);
		g_strm_pos += (frame_data.nFrameSize+4);
		

		delete frame_data.pFrame;
		frame_data.pFrame = NULL;
		frame_data.nFrameSize = 0;
		m_arrCurrentFrame.erase(m_arrCurrentFrame.begin());

		nFrame++;

		if(nFrame >= m_nMaxFrame)
		{
			t2 = GetTickCount();
			WriteTrakData();

			CString strVosPath;
			TCHAR strPath[MAX_PATH];
			GetModuleFileName(NULL, strPath, MAX_PATH);
			strVosPath = strPath;
			int nLastIndex = strVosPath.ReverseFind('\\');
			if (nLastIndex!=-1) {
				strFile =  strVosPath.Left(nLastIndex);
			} else {
				strFile = _T("\\");
			}

			switch(m_nFrameRate)
			{
			case MOVIE_FPS_UHD_25P:
				strFile.Append(_T("\\vosdatauhd25p"));
				break;
			case MOVIE_FPS_FHD_25P:
				strFile.Append(_T("\\vosdatafhd25p"));
				break;
			case MOVIE_FPS_FHD_50P:
				strFile.Append(_T("\\vosdatafhd50p"));
				break;
			}

			
			LPCSTR path = (LPSTR)(LPCTSTR)strFile;
			if(readFile.Open((LPCTSTR)path, CFile::modeRead))
			{
				buf = new BYTE[readFile.GetLength()];
				readFile.Read(buf,readFile.GetLength());
				memcpy(m_Tracks[0].vosData, buf, readFile.GetLength());
				readFile.Close();

				delete buf;
				buf = NULL;
			}

			Write_moov();
			t2_ = GetTickCount();
			break;
		}
	}

	int nOffset = g_strm_pos;
	int nValue = g_strm_pos - (g_strm_pos-g_nmoov_pos+48);
	g_strm_pos = m_mdatOffset;
	Write32be(nValue);
	m_pFile->Write(mp4_buffer, nOffset);
	m_pFile->Close();

	m_nFileIndex++;

	t1_ = GetTickCount();


	CString strLog;
	strLog.Format(_T(" t1[%d] t1_[%d] t2[%d] t2[%d] Total Time = %u , Movie Header Time = %u"),t1,t1_,t2,t2_, t1_-t1, t2_-t2);
	//AfxMessageBox(strLog);

	return strPath;
}

int CMovieInfoMake::IsCheckFileFPS(SdiFileInfo* pFileInfo)
{
	int nPos;
	int nFrame = 0;
	BYTE* pFileCount = NULL;
	BYTE FrameSize[4];
	int nCount;

	int frame_size[100];

	while(nFrame < 25 )
	{
		if(nFrame == 0)
		{
			nPos = (int)pFileInfo->pImageBuffer+64;
		}

		pFileCount = (BYTE*)nPos;
		FrameSize[0] = *(pFileCount+3);
		FrameSize[1] = *(pFileCount+2);
		FrameSize[2] = *(pFileCount+1);
		FrameSize[3] = *(pFileCount);
		memcpy(&nCount, FrameSize, 4);
		frame_size[nFrame] = nCount;

		if(nFrame == 22)
		{
			FrameSize[0] = *(pFileCount+4);
			FrameSize[1] = *(pFileCount+5);
			FrameSize[2] = *(pFileCount+6);
			FrameSize[3] = *(pFileCount+7);

			if(FrameSize[0] == 'm' &&
				FrameSize[1] == 'o' &&
				FrameSize[2] == 'o' &&
				FrameSize[3] == 'v')
				return INDEX_UHD_25P;
		}
		else if(nFrame > 22)
			break; 
		
		
		
		nPos += (nCount+4);
		nFrame++;
	}

	return INDEX_30P;
}

BOOL CMovieInfoMake::Write_ftyp()
{
	long writeSize;

	g_strm_pos = 0;
	
	Write32be( 48 );
	WriteTag( "ftyp" );
	WriteTag( "mp42" );
	Write32be( 0 );
	WriteTag( "mp42" );
	WriteTag( "isom" );
	WriteTag( "hvc1" );

	Write32be(0);
	Write32be(0);
	Write32be(0);
	Write32be(0);
	Write32be(0);

	m_mdatOffset = g_strm_pos;

	Write32be(0 );	// size
	WriteTag( "mdat" );
	Write64be(0 );

	return TRUE;
}
void CMovieInfoMake::WriteTrakData()
{
	switch(m_nFrameRate)
	{
	case MOVIE_FPS_UHD_25P:
		//25P
		timescale = 120000;
		m_refTrackId = 1;
		m_nbTracks = 1;
		m_Tracks[0].trackID = 1;
		m_Tracks[0].entry = GOP_SIZE_UHD_25P;
		m_Tracks[0].trackDuration = 129600;
		m_Tracks[0].hasBframes = 0;
		m_Tracks[0].type = MMF_MEDIA_TYPE_VIDEO;
		m_Tracks[0].videoInfo.orientation = 0;
		m_Tracks[0].videoInfo.nWidth = 3840;
		m_Tracks[0].videoInfo.nHeight = 2160;
		m_Tracks[0].timescale = 120000;
		m_Tracks[0].language = 0;
		m_Tracks[0].hasBframes = 0;
		m_Tracks[0].hasKeyframes = 1;
		m_Tracks[0].videoInfo.format = MMF_VIDEO_FORMAT_HEVC;
		m_Tracks[0].vosLen = 108;
		m_Tracks[0].vosData = new unsigned char[500];
		m_Tracks[0].videoInfo.videoColorRange = MMF_VID_COLR_RANGE_FULL;
		m_refTrackId = m_Tracks[0].trackID;
		break;
	case MOVIE_FPS_FHD_25P:
		//25P
		timescale = 120000;
		m_refTrackId = 1;
		m_nbTracks = 1;
		m_Tracks[0].trackID = 1;
		m_Tracks[0].entry = GOP_SIZE_UHD_25P;
		m_Tracks[0].trackDuration = 129600;
		m_Tracks[0].hasBframes = 0;
		m_Tracks[0].type = MMF_MEDIA_TYPE_VIDEO;
		m_Tracks[0].videoInfo.orientation = 0;
		m_Tracks[0].videoInfo.nWidth = 1920;
		m_Tracks[0].videoInfo.nHeight = 1080;
		m_Tracks[0].timescale = 120000;
		m_Tracks[0].language = 0;
		m_Tracks[0].hasBframes = 0;
		m_Tracks[0].hasKeyframes = 1;
		m_Tracks[0].videoInfo.format = MMF_VIDEO_FORMAT_HEVC;
		m_Tracks[0].vosLen = 108;
		m_Tracks[0].vosData = new unsigned char[500];
		m_Tracks[0].videoInfo.videoColorRange = MMF_VID_COLR_RANGE_FULL;
		m_refTrackId = m_Tracks[0].trackID;
		break;
	case MOVIE_FPS_FHD_50P:
		timescale = 120000;
		m_refTrackId = 1;
		m_nbTracks = 1;
		m_Tracks[0].trackID = 1;
		m_Tracks[0].entry = GOP_SIZE_FHD_50P;
		m_Tracks[0].trackDuration = 129600;
		m_Tracks[0].hasBframes = 0;
		m_Tracks[0].type = MMF_MEDIA_TYPE_VIDEO;
		m_Tracks[0].videoInfo.orientation = 0;
		m_Tracks[0].videoInfo.nWidth = 1920;
		m_Tracks[0].videoInfo.nHeight = 1080;
		m_Tracks[0].timescale = 120000;
		m_Tracks[0].language = 0;
		m_Tracks[0].hasBframes = 0;
		m_Tracks[0].hasKeyframes = 1;
		m_Tracks[0].videoInfo.format = MMF_VIDEO_FORMAT_HEVC;
		m_Tracks[0].vosLen = 108;
		m_Tracks[0].vosData = new unsigned char[500];
		m_Tracks[0].videoInfo.videoColorRange = MMF_VID_COLR_RANGE_FULL;
		m_refTrackId = m_Tracks[0].trackID;
		break;
	default:
		timescale = 120000;
		m_refTrackId = 1;
		m_nbTracks = 1;
		m_Tracks[0].trackID = 1;
		m_Tracks[0].entry = 30;
		m_Tracks[0].trackDuration = 120120;
		m_Tracks[0].hasBframes = 0;
		m_Tracks[0].type = MMF_MEDIA_TYPE_VIDEO;
		m_Tracks[0].videoInfo.orientation = 0;
		m_Tracks[0].videoInfo.nWidth = 1920;
		m_Tracks[0].videoInfo.nHeight = 1080;
		m_Tracks[0].timescale = 120000;
		m_Tracks[0].language = 0;
		m_Tracks[0].hasBframes = 0;
		m_Tracks[0].hasKeyframes = 2;
		m_Tracks[0].videoInfo.format = MMF_VIDEO_FORMAT_HEVC;
		m_Tracks[0].vosLen = 108;
		m_Tracks[0].vosData = new unsigned char[500];
		m_Tracks[0].videoInfo.videoColorRange = MMF_VID_COLR_RANGE_FULL;
		m_refTrackId = m_Tracks[0].trackID;
		break;
	}

}
void CMovieInfoMake::Write_moov()
{
	g_nmoov_pos = g_strm_pos;

	int nSize;
	int i, moov_pos = g_strm_pos;
	
	Write32be(0);
	WriteTag("moov");
	Write_mvhd();

	for ( i = 0; i < m_nbTracks; i++ ) {
		if ( m_Tracks[ i ].entry > 0 ) {
			Write_trak( &m_Tracks[ i ] );
		}
	}

	nSize = g_strm_pos - moov_pos;
	UpdateSize(nSize, moov_pos);
}
void CMovieInfoMake::Write_mvhd()
{
	int i, maxTrackID = 1;
	unsigned int maxTrackLenTemp, maxTrackLen = 0;

	// FIXME Calculate MAX Track Length Here
	for ( i = 0; i < m_nbTracks; i++ ) {
		maxTrackLenTemp = m_Tracks[ i ].trackDuration;
		if ( m_Tracks[ i ].entry > 0 ) {
			if ( maxTrackLen < maxTrackLenTemp ) {
				maxTrackLen = maxTrackLenTemp;
			}
			if ( maxTrackID < m_Tracks[ i ].trackID ) {
				maxTrackID = m_Tracks[ i ].trackID;
			}
		}
	}

	Write32be( 108 ); /* size */
	WriteTag( "mvhd" );
	Write32be( 0 ); /* version & flags */

	Write32be( 0 ); // Creation Time  //Set 0   									x
	Write32be( 0 ); // Modification Time        									x
	Write32be( timescale);    // Timescale   //need #  00 01 D4 C0						o
	//Write32be( maxTrackLen); // Duration of longest track							x

	switch(m_nFrameRate)
	{
	case MOVIE_FPS_UHD_25P:
		Write32be( 105600);
		break;
	case MOVIE_FPS_FHD_25P:
		Write32be( 144000);
		break;
	case MOVIE_FPS_FHD_50P:
		Write32be( 76800);
		break;
	}

	Write32be( 0x00010000 ); /* reserved (preferred rate) 1.0 = normal */		//	o
	Write16be( 0x0100 ); /* reserved (preferred volume) 1.0 = normal */		//	o	
	Write16be( 0 ); /* reserved */											//	o
	Write32be( 0 ); /* reserved */											//	o
	Write32be( 0 ); /* reserved */											//	o
	/* Matrix structure */
	Write32be( 0x00010000 ); /* reserved */									//	o
	Write32be( 0x0 ); /* reserved */											//	o
	Write32be( 0x0 ); /* reserved */											// 	o
	Write32be( 0x0 ); /* reserved */											//	o
	Write32be( 0x00010000 ); /* reserved */									//	o
	Write32be( 0x0 ); /* reserved */											//	o
	Write32be( 0x0 ); /* reserved */											//	o
	Write32be( 0x0 ); /* reserved */											//	o
	Write32be( 0x40000000 ); /* reserved */									//	o
	Write32be( 0 ); /* reserved (preview time) */								//	o
	Write32be( 0 ); /* reserved (preview duration) */							//	o
	Write32be( 0 ); /* reserved (poster time) */								//	o
	Write32be( 0 ); /* reserved (selection time) */							//	o
	Write32be( 0 ); /* reserved (selection duration) */						//	o
	Write32be( 0 ); /* reserved (current time) */								//	o
	Write32be( maxTrackID + 1 ); /* Next track id */							//	o
}
void CMovieInfoMake::Write_mdia(sMP4Track *track)
{
	unsigned int size;
	unsigned int mdia_pos = g_strm_pos;

	Write32be(0 );								//o
	WriteTag( "mdia" );							//o
	
	Write_mdhd(track);
	Write_hdlr(track);
	Write_minf(track);

	size = g_strm_pos - mdia_pos;
	UpdateSize( size, mdia_pos );
}
void CMovieInfoMake::Write_mdhd(sMP4Track *track)
{
	unsigned int trackDuration = (tUint64)track->trackDuration;
	Write32be( 32 ); /* size */					//	o
	WriteTag( "mdhd" );							//	o

	Write32be( 0 ); /* version & flags */			// 	o
	Write32be( 0 ); /* creation time */		//	x
	Write32be( 0 ); /* modification time */	//	x
	Write32be( track->timescale ); /* time scale : This should be the sampling rate */  // o 120000

	switch(m_nFrameRate)
	{
	case MOVIE_FPS_UHD_25P:
		Write32be( trackDuration );
		break;
	case MOVIE_FPS_FHD_25P:
		Write32be( 144000 );
		break;
	case MOVIE_FPS_FHD_50P:
		Write32be( 76800 );
		break;
	}

	Write16be( track->language ); /* language */	//	x
	Write16be( 0 ); /* reserved (quality) */
}
void CMovieInfoMake::Write_hdlr(sMP4Track *track)
{
	char *descr, *hdlr, *hdlr_type;
	unsigned int hldr_pos = g_strm_pos;
	unsigned int size;

	hdlr = (char*)"\0\0\0\0";
	hdlr_type = (char*)"vide";
	descr = (char*)"VideoHandler";

	Write32be( 0 ); /* size */						//	o -> 45
	WriteTag( "hdlr" );								//	o
	Write32be( 0 );         /* Version & flags */		//	o
	WriteTag( hdlr );       /* Component Type */		//	x	
	WriteTag( hdlr_type );  /* Component Subtype */	//	o	-> vide
	Write32be( 0 );         /* Component Manufacturer */ //	o
	Write32be( 0 );         /* Component flags */			//	o
	Write32be( 0 );         /* Component flags mask */	//	o
	WriteByte( strlen( descr ) ); /* string counter */	//	-> 0x0c videhandler
	memcpy( &mp4_buffer[g_strm_pos], descr, strlen( descr ) ); g_strm_pos += strlen(descr);
	size = g_strm_pos - hldr_pos;
	UpdateSize( size, hldr_pos ); 						// x 4byte 0

}
void CMovieInfoMake::Write_minf(sMP4Track *track)
{
	unsigned int size;
	unsigned int minf_pos = g_strm_pos;

	Write32be(0 );					//	o
	WriteTag( "minf" );				//	o

	Write_vmhd();
	Write_dinf();
	Write_stbl(track);

	size = g_strm_pos - minf_pos;
	UpdateSize( size, minf_pos );
}
void CMovieInfoMake::Write_vmhd()
{
	Write32be( 0x14 ); /* size (always 0x14) */			//	o
	WriteTag( "vmhd" );									//	o
	Write32be( 0x01 ); /* version & flags */				//	o
	Write32be( 0 ); /* reserved (graphics mode = copy) */	//	o
	Write32be( 0 ); /* reserved (graphics mode = copy) */	//	o
}
void CMovieInfoMake::Write_dinf()
{
	unsigned int size;
	unsigned int dinf_pos = g_strm_pos;

	Write32be( 0 );			//	o -> 24
	WriteTag( "dinf" );		//	o

	Write_dref();

	size = g_strm_pos - dinf_pos;
	UpdateSize( size, dinf_pos );
}
void CMovieInfoMake::Write_dref()
{
	Write32be( 28 );     /* size */				//	o
	WriteTag( "dref" );							//	o
	Write32be( 0 );      /* version & flag */		//	o
	Write32be( 1 );      /* entry count */		//	o

	Write32be( 0xc );    /* size */				//	o
	WriteTag( "url " );							//	o
	Write32be( 1 );      /* version & flag */		//	o
}
void CMovieInfoMake::Write_stbl(sMP4Track *track)
{
	unsigned int size;
	unsigned int stbl_pos = g_strm_pos;

	Write32be(0 );			//	o
	WriteTag( "stbl" );		//	o

	Write_stsd(track);
	Write_stts(track);
	Write_stss();
	Write_stsc();
	Write_stsz();
	Write_stco(track);

	size = g_strm_pos - stbl_pos;
	UpdateSize( size, stbl_pos );
}
void CMovieInfoMake::Write_stsd(sMP4Track *track)
{
	unsigned int size;
	unsigned int stsd_pos = g_strm_pos;

	Write32be( 0 );					//	o -> ED (237)
	WriteTag( "stsd" );				//	o
	Write32be( 0 );  /* version & flag */	//	o
	Write32be( 1 );  /* entry count */	//	o

	Write_video(track);

	size = g_strm_pos - stsd_pos;
	UpdateSize( size, stsd_pos );
}
void CMovieInfoMake::Write_video(sMP4Track *track)
{
	unsigned int size, video_pos = g_strm_pos;
	char compressor_name[ 32 ] = "DRIMeV HEVC Encoder";
	Write32be( 0 ); /* size */		
	WriteTag( "hvc1" );	
	Write32be( 0 ); /* Reserved */						//	o
	Write16be( 0 ); /* Reserved */						//	o
	Write16be( 1 ); /* Data-reference index */			//	o

	Write16be( 0 ); /* Codec stream version */			//	o
	Write16be( 0 ); /* Codec stream revision (=0) */		//	o

	Write32be( 0 ); /* Reserved */						//	o
	Write32be( 0 ); /* Reserved */						//	o
	Write32be( 0 ); /* Reserved */						//	o

	Write16be( track->videoInfo.nWidth ); /* Video width */		//	o 1920 or 3840
	Write16be( track->videoInfo.nHeight ); /* Video height */		//	o 1080 or 2160
	Write32be( 0x00480000 ); /* Horizontal resolution 72dpi */
	Write32be( 0x00480000 ); /* Vertical resolution 72dpi */
	Write32be( 0 ); /* Data size (= 0) */
	Write16be( 1 ); /* Frame count (= 1) */
	WriteByte( 31 );										//	o	32개  DRIMeV HEVC Encoder
	memcpy( &mp4_buffer[g_strm_pos], compressor_name, 31); g_strm_pos += 31;
	Write16be( 0x18 ); /* Reserved */					//	o
	Write16be( 0xffff ); /* Reserved */				//	o

	Write_hvcC(track);
	Write_colr(track);

	size = g_strm_pos - video_pos;
	UpdateSize( size, video_pos );
}
void CMovieInfoMake::Write_hvcC(sMP4Track *track)
{
	Write32be( track->vosLen + 8 );         // 	o 116(0x74)
	WriteTag( "hvcC" );						//	o
	
#ifdef MAKE_TEMP_FILE
	//g_strPath1.Replace(_T("10102_0.mp4"), _T("10102_0_.mp4"));
	//LPCSTR lpszA = (LPSTR)(LPCTSTR)g_strPath1;
	CFile readFile;
	readFile.Open((LPCTSTR)lpszA, CFile::modeRead);
	BYTE* buf = new BYTE[readFile.GetLength()];
	readFile.Read(buf,readFile.GetLength());
	//g_strm_pos

	CString strFile;
	strFile.Format(_T("M:\\Movie\\vosdatafhd25p"));
	CFile writefile;
	LPCSTR path = (LPSTR)(LPCTSTR)strFile;
	if(writefile.Open((LPCTSTR)path, CFile::modeCreate|CFile::modeReadWrite))
	{
		//writefile.Write(track->vosData, track->vosLen);
		writefile.Write(buf+g_strm_pos, track->vosLen);
		writefile.Close();
	}
#endif
	memcpy( &mp4_buffer[g_strm_pos], track->vosData, track->vosLen); 
	g_strm_pos += track->vosLen;    // ETC !!!!! 
}
void CMovieInfoMake::Write_colr(sMP4Track *track)
{
	unsigned int matVal = 1;
	unsigned int startPos = g_strm_pos;

	Write32be(0);	// size					// o
	WriteTag("colr");	// tag				//	o
	WriteTag("nclc");	// tag				//	o
	Write16be(1);	// primarie index			//	o
	Write16be(1);	// transfer function index	//	o
	Write16be(matVal);	// matrix index		//	x -> 0
	WriteByte(0);							// 	o
	UpdateSize(g_strm_pos-startPos, startPos);    // o 18 or 20
}
void CMovieInfoMake::Write_stts(sMP4Track *track)
{
	sSttsTable *stts_entries;
	int entries = -1;
	unsigned int atom_size;
	int i;

	atom_size = 16 + ( 1 * 8 );     
	Write32be(atom_size ); /* size */                   	//	o   (0x18)
	WriteTag( "stts" );									//	o
	Write32be(0 ); /* version & flags */					//	0

	switch(m_nFrameRate)
	{
	case MOVIE_FPS_UHD_25P:
		Write32be(1 ); /* entry count */				//	
		Write32be(GOP_SIZE_UHD_25P );				//	o	Frame Count (ox1e) 30
		Write32be(4800 );				//	o	4800 ( 0x12c0)
		break;
	case MOVIE_FPS_FHD_25P:
		Write32be(1 ); /* entry count */				//	
		Write32be(GOP_SIZE_FHD_25P );				//	o	Frame Count (ox1e) 30
		Write32be(4800 );				//	o	4800 ( 0x12c0)
		break;
	case MOVIE_FPS_FHD_50P:
		Write32be(1 ); /* entry count */				//	
		Write32be(GOP_SIZE_FHD_50P );				//	o	Frame Count (ox1e) 30
		Write32be(4800 );	
		break;
	default:
		Write32be(1 ); /* entry count */				//	
		Write32be(30 );				//	o	Frame Count (ox1e) 30
		Write32be(4004 );				//	o	4800 ( 0x12c0)
		break;
	}
}
void CMovieInfoMake::Write_stss()
{
	int i, index = 0;
	unsigned int size, entryPos, stss_pos = g_strm_pos;

	Write32be(0 );
	WriteTag( "stss" );

	Write32be(0 );  /* version & flag */

	entryPos = g_strm_pos;

	switch(m_nFrameRate)
	{
	case MOVIE_FPS_UHD_25P:
		Write32be(2 );    /* Number of entries */
		Write32be(1 );		// I Frame
		Write32be(13 );		// I Frame
		break;
	case MOVIE_FPS_FHD_25P:
		Write32be(2 );    /* Number of entries */
		Write32be(1 );		// I Frame
		Write32be(13 );		// I Frame
		break;
	case MOVIE_FPS_FHD_50P:
		Write32be(2 );    /* Number of entries */
		Write32be(1 );		// I Frame
		Write32be(26 );		// I Frame
		break;
	default:
		Write32be(2 );    /* Number of entries */
		Write32be(1 );		// I Frame
		Write32be(16 );		// I Frame
		break;
	}

	index = 2;

	UpdateSize( index, entryPos );
	size = g_strm_pos - stss_pos;

	UpdateSize( size, stss_pos );

}
void CMovieInfoMake::Write_stsc()
{
	int index = 0;
	unsigned int entryPos;
	unsigned int size;
	unsigned int stsc_pos = g_strm_pos;

	Write32be( 0 );
	WriteTag( "stsc" );
	Write32be( 0 );      // version & flag
	entryPos = g_strm_pos;
	Write32be(1 ); // entry count

	index = 1;
	Write32be( 1 ); // first chunk
	Write32be( 1 );
	Write32be( 1 ); // sample description index

	UpdateSize( index, entryPos );

	size = g_strm_pos - stsc_pos;
	UpdateSize( size, stsc_pos );
}
void CMovieInfoMake::Write_stsz()
{
	unsigned int size;
	unsigned int stsz_pos = g_strm_pos;

	Write32be( 0 );
	WriteTag( "stsz" );
	Write32be( 0 );      // version & flag
	Write32be(0 ); // sample size
	

	Write32be(m_nMaxFrame ); // sample count
	for(int i = 0; i < m_nMaxFrame; i++)
	{
		Write32be(m_nArray[i] );
	}

	size = g_strm_pos - stsz_pos;
	UpdateSize( size, stsz_pos );

}
void CMovieInfoMake::Write_stco(sMP4Track *track)
{
	int nSum;
	unsigned int size = 16 + ( 4 * track->entry );

	Write32be( size );
	WriteTag( "stco" );

	Write32be( 0 );  /* version & flag */
	Write32be(track->entry);


	for(int i = 0; i < track->entry; i++)
	{
		if(i == 0)
		{
			nSum = 64;	
		}
		else
		{
			nSum += m_nArray[i-1];
		}
		Write32be(nSum);
		TRACE("offset %d 0x%X \n",i+1, nSum );
	}
}
void CMovieInfoMake::Write_trak(sMP4Track *track)
{
	unsigned int size;
	unsigned int trak_pos = g_strm_pos;

	Write32be(0 );                        										//o
	WriteTag( "trak" );		
	Write_tkhd(track);

	Write_mdia(track);

	size = g_strm_pos - trak_pos;
	UpdateSize( size, trak_pos );
}
void CMovieInfoMake::Write_tkhd(sMP4Track *track)
{
	tMmfClockTime DurationBaseMvhd = (tUint64)track->trackDuration*timescale/track->timescale;
	Write32be( 92 ); /* size */												//	o
	WriteTag( "tkhd" );														//	o

	Write32be(0xf ); /* version & flags (track enabled) */					//	o
	Write32be(0 ); /* creation time */										//	x
	Write32be(0 ); /* modification time */									//	x

	Write32be(track->trackID ); /* track-id */								//	x
	Write32be(0 ); /* reserved */												//	o
	//Write32be(track->trackDuration );												//	x
	switch(m_nFrameRate)
	{
	case MOVIE_FPS_UHD_25P:
		Write32be(105600 );			
		break;
	case MOVIE_FPS_FHD_25P:
		Write32be(144000 );			
		break;
	case MOVIE_FPS_FHD_50P:
		Write32be(76800 );			
		break;
	}

	Write32be(0 ); /* reserved */												//	o
	Write32be(0 ); /* reserved */												//	o
	Write32be(0x0 ); /* reserved (Layer & Alternate group) */					//	o
	Write16be( 0 );
	Write16be( 0 );

	/* Matrix structure */
	int matrix[3][3] = {{0x00010000, 0, 0}, {0, 0x00010000, 0}, {0, 0, 0x40000000}};
	
	Write32be(matrix[0][0]);													//	x
	Write32be(matrix[0][1]);													//	x
	Write32be(matrix[0][2]);													//	x
	Write32be(matrix[1][0]);													//	x
	Write32be(matrix[1][1]);													//	x
	Write32be(matrix[1][2]);													//	x
	Write32be(matrix[2][0]);													//	x
	Write32be(matrix[2][1]);													//	x
	Write32be(matrix[2][2]);													//	x

	Write16be( track->videoInfo.nWidth );									//	o		1920 or 3840
	Write16be( 0 );															//	o
	Write16be( track->videoInfo.nHeight );									//	o		1080 or 2160
	Write16be( 0 );	
}
void CMovieInfoMake::UpdateSize(unsigned int size, unsigned int offset)
{
	unsigned int cur_pos = g_strm_pos;
	g_strm_pos = offset;
	Write32be(size);
	g_strm_pos = cur_pos;
}

void CMovieInfoMake::WriteByte(unsigned int val)
{
	mp4_buffer[g_strm_pos++] = val;
}
void CMovieInfoMake::Write16be(unsigned int val)
{
	WriteByte(val >> 8 );
	WriteByte(val );
}
void CMovieInfoMake::Write24be(unsigned int val)
{
	WriteByte(val >> 16 );
	WriteByte(val >> 8 );
	WriteByte(val );
}
void CMovieInfoMake::Write32be(unsigned int val)
{
	WriteByte(val >> 24 );
	WriteByte(val >> 16 );
	WriteByte(val >> 8 );
	WriteByte(val );
}
void CMovieInfoMake::Write64be(tUint64 val)
{
	WriteByte(val >> 56 );
	WriteByte(val >> 48 );
	WriteByte(val >> 40 );
	WriteByte(val >> 32 );
	WriteByte(val >> 24 );
	WriteByte(val >> 16 );
	WriteByte(val >> 8 );
	WriteByte(val );
}
void CMovieInfoMake::WriteTag(const char *pTag)
{
	for ( int i = 0; i < 4; i++ ) {
		WriteByte(*pTag++ );
	}
}