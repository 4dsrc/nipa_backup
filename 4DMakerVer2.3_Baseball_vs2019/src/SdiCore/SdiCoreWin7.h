/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        SdiCoreWin7.h
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */

//-- 2014-09-15 hongsu@esmlab.com
//-- Sync Time on DSC after Sensor On
// static const int sync_frame_time_after_sensor_on = 5150; // * 100um
// //static const int sync_interval_sensor_on		 = 23;	// 13: 20-50mm	21:30mm
// static const int sync_decision_sensor_on		 = 13;	// with 3 ms


#pragma once
#include "SdiCore.h"
#include "ptpIndex.h"

#include <atlbase.h>
#include <atlstr.h>
#include <atlcoll.h>
#include <specstrings.h>
#include <commdlg.h>
#include <new>
#include <PortableDeviceApi.h>  // Include this header for Windows Portable Device API interfaces
#include <PortableDevice.h>     // Include this header for Windows Portable Device definitions

#include "CommonFunctions.h"    // Includes common prototypes for functions used across source files

//#ifdef _THREAD_FILE_SAVE
	#include "SdiCoreFileMgr.h"
//#endif

#include <strsafe.h>
#include <vector>
using namespace std;
class AFX_EXT_CLASS CSdiCoreWin7 :
	public CSdiCore
{
public:
	CString m_strSaveName;
	CSdiCoreWin7(void);
	CSdiCoreWin7(int, LARGE_INTEGER , LARGE_INTEGER );
	virtual ~CSdiCoreWin7(void);

public:
	CComPtr<IPortableDevice> m_pIPortableDevice;
	HWND m_hRecvWnd;
	//-- 2013-10-25 hongsu.jung
	//-- For Sync Time with MainFrame
	LARGE_INTEGER m_swFreq, m_swStart;
	CSdiCoreFileMgr* m_pSdiFileMgr;
	static CRITICAL_SECTION* m_CriSection;
	static CRITICAL_SECTION* m_CriSectionLog;
	BOOL m_bSyncStop;
	CString m_strUniqueID;
	CString m_strFullID;
	void CreateFileManager();
	void RemoveThread();

public:
	void Init();
	// Windows Portable Device 관련 샘플
	void ChooseDevice(IPortableDevice** ppDevice);
	SdiResult ChooseDevice(IPortableDevice** ppDevice, CString strDeviceName);
	void GetClientInformation(IPortableDeviceValues** ppClientInformation);
	void RegisterForEventNotifications(IPortableDevice* pDevice);
	void UnregisterForEventNotifications(IPortableDevice* pDevice);
	DWORD EnumerateAllDevices();
	//-----------Function
	HRESULT CaptureExec(IPortableDevice* pDevice,CString strSaveName, int nFileNum, int nClose);
	//NX3000 20150119 ImageTransfer
	HRESULT CaptureExec(IPortableDevice* pDevice,CString strSaveName);
	HRESULT CaptureCompleteExec(IPortableDevice* pDevice,CString strSaveName,bool &bIsLast);
	//HRESULT MovieCompleteExec(IPortableDevice* pDevice,CString strSaveName, int WaitSaveDSC); //-- 2014-09-19 joonho.kim
	HRESULT MovieCompleteExec(IPortableDevice* pDevice,CString strSaveName, int WaitSaveDSC, int nFileNum = 0, int nClose = 0); 
	HRESULT RequireCaptureExec(IPortableDevice* pDevice,WORD code);
	HRESULT LiveviewExec(IPortableDevice* pDevice,unsigned char* pYUV);	
	HRESULT LiveviewInfo(IPortableDevice* pDevice, RSLiveviewInfo* pLiveviewInfo);

	HRESULT SdiMovieTransfer(IPortableDevice* pDevice,CString strFileName, int nfilenum, int nClose);
	//-- 2011-5-31 chlee
	virtual SdiResult SdiSendShutterCapture(SdiInt8 nShutter);
	virtual SdiResult SdiSendAdjRequireCaptureOnce(int nModel = 0);
	virtual SdiResult SdiSendAdjCompleteCapture(CString, bool&, int nFileNum, int nClose);
	virtual SdiResult SdiSendAdjCompleteCapture(CString, bool&);
	//virtual SdiResult SdiSendAdjCompleteMovie(CString strSaveName, int nWaitSaveDSC);
	virtual SdiResult SdiSendAdjCompleteMovie(CString, int nWaitSaveDSC, int nFilenNum, int nClose);

	virtual SdiResult SdiSendAdjMovieTransfer(CString strSaveName, int nfilenum, int nClose);
	
			
	virtual SdiResult SdiSetFocusPosition(FocusPosition *ptFocusPos, SdiInt8 nType);
	virtual SdiResult SdiSetFocusPosition(SdiInt32 nValue);
	virtual SdiResult SdiGetFocusPosition(FocusPosition* ptFocusPos);
	virtual SdiResult SdiCheckEvent(SdiInt32& nEventExist, SdiUIntArray* pArList=NULL); //-- 2011-6-29		chlee
	virtual SdiResult SdiGetEvent(BYTE **pData, SdiInt32 *size); //-- 2011-6-29		chlee

	//-- 2011-8-23 Lee JungTaek
	virtual SdiResult SdiGetPTPDeviceInfo(PTPDeviceInfo * ptDevInfoType);
	HRESULT GetPTPDeviceInfo(IPortableDevice*  pDevice, PTPDeviceInfo * ptDevInfoType);

	HRESULT StartRemoteStudio(IPortableDevice* pDevice);
	HRESULT GetFocusPosition(IPortableDevice*  pDevice, FocusPosition* ptFocusPos, SdiInt8 nType);
	HRESULT GetFocusPosition(IPortableDevice*  pDevice, FocusPosition* ptFocusPos);
	HRESULT SetFocusPosition(IPortableDevice*  pDevice, SdiInt32 nCurPos);

	//-- 2011-5-28 Lee JungTaek
	//-- Property
	HRESULT GetDevPropDesc(IPortableDevice*  pDevice, WORD wPropCode, WORD wAllFlag = 1);
	HRESULT GetDevUniqueID(IPortableDevice*  pDevice, WORD wPropCode,  SdiChar **chUniqueID);
	HRESULT GetDevModeIndex(IPortableDevice*  pDevice, WORD wPropCode, SdiInt &nModeIndex);
	HRESULT SetDevPropValue(IPortableDevice* pDevice, WORD wPropCode, CString strValue);
	HRESULT SetDevPropValue(IPortableDevice* pDevice, WORD wPropCode, SdiInt nType, SdiInt nValue);
	
	//-- 2011-6-29 chlee
	HRESULT SendCheckEvent(IPortableDevice* pDevice,SdiInt32* result, SdiUIntArray* pArList=NULL);
	HRESULT SendGetEvent(IPortableDevice* pDevice, BYTE **pData, SdiInt32* size);

	HRESULT OpenSession(IPortableDevice* pDevice);
	HRESULT CloseSession(IPortableDevice* pDevice);

	//-- 2011-7-18 Lee JungTaek
	HRESULT FormatDevice(IPortableDevice*  pDevice);
	HRESULT ResetDevice(IPortableDevice*  pDevice);
	HRESULT ResetDevice(IPortableDevice*  pDevice, int nParam = 5);
	HRESULT FWUpdate(IPortableDevice*  pDevice);
	//20150128 CMiLRe NX3000
	//CMiLRe 2015129 펌웨어 다운로드 경로 추가
	HRESULT FWDownload(IPortableDevice*  pDevice, CString strPath);
	HRESULT SensorCleaningDevice(IPortableDevice*  pDevice);
	//CMiLRe 20141113 IntervalCapture Stop 추가
	HRESULT IntervalCaptureStop(IPortableDevice*  pDevice);
	//CMiLRe 20141127 Display Save Mode 에서 Wake up
	HRESULT DisplaySaveModeWakeUp(IPortableDevice*  pDevice);	
	
	//NX3000
	HRESULT DevicePowerOff(IPortableDevice*  pDevice);
	HRESULT DeviceReboot(IPortableDevice*  pDevice);

	// GH5
	HRESULT HalfShutter(IPortableDevice*  pDevice, int nRelease);

	HRESULT SetFocusFrame(IPortableDevice*  pDevice, int nX, int nY, int nMagnification);
	HRESULT GetFocusFrame(IPortableDevice*  pDevice, BYTE **pData, SdiInt32* size);

	HRESULT DeviceExportLog(IPortableDevice*  pDevice);
	
	void ResetCounter();
	void IncreaseCounter();
	void OnSystemDeviceChanged( WPARAM wParam, LPARAM lParam );

	//-- 2013-10-25 hongsu.jung
	//-- For Get Tick Count with sync MainFrame
	int GetTickSync();

	virtual SdiResult SdiInitSession(SdiChar* strDevName);
	virtual SdiResult SdiOpenSession();
	virtual SdiResult SdiExOpenSession();
	virtual SdiResult SdiCloseSession();
	virtual SdiResult SdiSendAdjCapture(SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size);
	virtual	SdiResult SdiSendAdjLiveview(SdiUChar* pYUV);
	virtual	SdiResult SdiSendAdjLiveviewInfo(RSLiveviewInfo* pLiveviewInfo);
	virtual SdiResult SdiSendAdjCapture(CString strSaveName);

	//-- Camera Property
	virtual	SdiResult SdiGetDevPropDesc(WORD wPropCode, WORD wAllFlag = 1);
	virtual SdiResult SdiGetDevUniqueID(SdiInt nPTPCode,  SdiChar **chUniqueID);
	virtual SdiResult SdiSetDevUniqueID(SdiInt nPTPCode,  CString strUUID);
	virtual SdiResult SdiGetDevModeIndex(SdiInt nPTPCode, SdiInt &nModeIndex);
	virtual	SdiResult SdiSetDevPropValue(SdiInt nPTPCode, SdiInt nType, LPARAM lpValue);

	//-- Send Command
	virtual	SdiResult SdiFormatDevice();
	virtual	SdiResult SdiResetDevice(int nParam = 5);
	virtual	SdiResult SdiFWUpdate();
	//20150128 CMiLRe NX3000
	//CMiLRe 2015129 펌웨어 다운로드 경로 추가
	virtual	SdiResult SdiFWDownload(CString strPath);
	virtual SdiResult SdiSensorCleaningDevice();
	//CMiLRe 20141113 IntervalCapture Stop 추가
	virtual SdiResult SdiIntervalCaptureStop();
	//CMiLRe 20141127 Display Save Mode 에서 Wake up
	virtual SdiResult SdiDisplaySaveModeWakeUp();
		
	virtual SdiResult SdiInitGH5();
	HRESULT InitGH5(IPortableDevice* pDevice);

	//-- 2013-10-22 yongmin
	virtual SdiResult SdiGetTick(SdiInt& pcTime, SdiDouble& deviceTime, SdiInt& nCheckCnt, SdiInt& nCheckTime, SdiInt nDSCSynTime, HWND handle);
	HRESULT GetTick(IPortableDevice* pDevice, SdiInt& pcTime, SdiDouble& deviceTime, SdiInt& nCheckCnt, SdiInt& nCheckTime, SdiInt nDSCSyncTime, HWND handle);
	virtual SdiResult SdiSetTick(SdiDouble nTick, SdiInt nIntervalSensorOn, SdiUIntArray* pArList=NULL);
	HRESULT SetTick(IPortableDevice* pDevice,SdiDouble nTick, SdiInt nIntervalSensorOn, SdiUIntArray* pArList=NULL);

	//-- 2014-01-06 yongmin
	virtual SdiResult SdiHiddenCommand(SdiInt32 nCommand, SdiInt32 nValue);
	HRESULT SetHiddenCommand(IPortableDevice* pDevice, SdiInt32 nCommand, SdiInt32 nValue);

	virtual SdiResult SdiFocusSave();
	HRESULT SetFocusSave(IPortableDevice* pDevice);
	//-- 2014-07-21 joonho.kim
	virtual SdiResult SdiSetPause(int nPause);
	virtual SdiResult SdiSetResume(int nResume);
	virtual SdiResult SdiTrackingAFStop();
	virtual	SdiResult SdiControlTouchAF(int nX, int nY);
	virtual	SdiResult SdiMovieCancle();
	virtual SdiResult SdiGetRecordStatus();
	virtual SdiResult SdiGetCaptureCount();
	virtual SdiResult SdiGetImagePath(CString& strPath);
	virtual SdiResult SdiFileTransfer(char* pPath, int nfilenum, int nmdat, int nOffset,int* nFileSize, int nMovieSize);
	virtual SdiResult SdiImageTransfer(CString strName, int nfilenum, int nClose);
	virtual SdiResult SdiImageTransferEX(CString strName, int nfilenum, int nClose);
	virtual SdiResult SdiSetEnLarge(int nLarge);
	virtual SdiResult SdiSetRecordPause(int nPause);
	virtual SdiResult SdiSetRecordResume(int nSkip);
	virtual SdiResult SdiSetLiveView(int nCommand);
	virtual SdiResult SdiFileReceiveComplete();
	virtual SdiResult SdiImageReceiveComplete();
	virtual SdiResult SdiLiveReceiveComplete();
	virtual	SdiBool SdiCheckGetFile();
	virtual	SdiBool SdiFileMgrSetStartTime(int nStartTime, int nIntervalSensorOn);
	virtual	SdiResult SdiFileMgrGetEndTime();
	virtual	SdiBool SdiSetResumeFrame(int nResumeFrame);
	virtual	SdiResult SdiGetResumeFrame();
	virtual	SdiBool SdiSetResumeMode(BOOL bResumeMode);
	virtual	SdiBool SdiGetResumeMode();
	virtual	void SdiSyncStop();
	virtual void SetFps(int nFps);

	BOOL m_bCheckSemaphore;	
	HRESULT CheckGetFile();	

	virtual	SdiResult SdiHalfShutter(int nRelease);

	virtual SdiResult SdiSetFocusFrame(int nX, int nY, int nMagnification);
	virtual SdiResult SdiGetFocusFrame(BYTE **pData, SdiInt32* size);
	//NX3000
	virtual	SdiResult SdiDeviceReboot();
	virtual	SdiResult SdiDevicePowerOff();

	virtual	SdiResult SdiDeviceExportLog();
	//kcd Test Log
	void WriteLog(CString strLog);
	CString m_strLog;
};
