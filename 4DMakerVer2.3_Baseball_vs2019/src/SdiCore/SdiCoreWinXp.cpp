
/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        SdiCoreWin7.h
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */

#include "StdAfx.h"
#include "SdiCoreWinXp.h"

//-- 2011-06-27
#define FOCUS_DETAIL_UNIT		100
#define FOCUS_ROUGH_UNIT		10

CSdiCoreWinXp::CSdiCoreWinXp(void)
{
	m_pPTPHost = NULL;
}

CSdiCoreWinXp::~CSdiCoreWinXp(void)
{
	if(m_pPTPHost)
		delete m_pPTPHost;
}

void CSdiCoreWinXp::Init_ptphost()
{
	InitPtpHost();
}

int CSdiCoreWinXp::InitPtpHost()
{
	int i = 0;
	char buff[0x50];
	i = m_pPTPHost->create(0);

	if(i != PTP_ERROR_NONE)
		return PTP_ERROR_CREATE_INSTANCE;

	// not sure whether we need to set the all 
	m_pPTPHost->setDeviceConfig("PTP HOST","Samsung","0.9", 0); // little endian(0),big endian(1)
	m_pPTPHost->setNumStorage(1);
	sprintf(buff,"%S",m_pPTPHost->getDevName());

	m_pPTPHost->setTransportConfig(buff, 1,0);// little endian(0),big endian(1)

	m_pPTPHost->setNumThumbList(9);
	int ret  = m_pPTPHost->initialize();
	m_pPTPHost->setDebugPrintLevel(3);
	m_pPTPHost->setOptions(1,1,0,0,0);

	return ret;
}

int CSdiCoreWinXp::setStatusForPTPHOST(int sel,int value)
{
	BOOL retValue = TRUE;

	if(m_pPTPHost != NULL)
		m_pPTPHost->setStatusForPTPHOST(value);
	else
		retValue = FALSE;

	return retValue;
}
	
int CSdiCoreWinXp::getStatusForPTPHOST(int sel)
{
	int retValue = 0;

	if(m_pPTPHost != NULL)
		retValue = m_pPTPHost->getStatusForPTPHOST();

	return retValue;
}
	
BOOL CSdiCoreWinXp::RegisterHandleForNotify(HWND hwnd)
{
	BOOL retValue = FALSE;
	CUSBDevice *usbDevice = CUSBDevice::GetInstance();

	// 현재 HWND가 Device 상태 변경시에 Message를 받을 수 있도록 등록함
	retValue = usbDevice->RegisterHandleForNotify(hwnd);

	return retValue;
}
	
int CSdiCoreWinXp::ReceiveDeviceEvent(int sel,int nType,void* pParam1, void* pParam2)
{
	CString strOutputMsg;
	int ret=PTPErrNone;

	if(nType == DEVICE_NOTIFY_DEVICEARRIVAL)
	{
		strOutputMsg = L"<USB Connected>\r\n";
		if(m_pPTPHost != NULL)
		{
			delete m_pPTPHost;
			m_pPTPHost = NULL;
		}

		m_pPTPHost = new PTPHOST; // create

		InitPtpHost();         // sel,""

		m_pPTPHost->mIsCreated = 1;
		m_pPTPHost->setStatusForPTPHOST(PTPHOST_STATUS_CONNECTED);

		m_PTPInfo.Request.Code = PTP_OC_GetDeviceInfo;
		
		ret = m_pPTPHost->mpHostOpHandler->sendSQEorder(&m_PTPInfo, m_pPTPHost->mpTransport);
		if (ret != PTPErrNone)
			return ret;

		//-- For Timing
		Sleep(10);

		ret = m_pPTPHost->mpHostOpHandler->getSETpayload(&m_PTPInfo, m_pPTPHost->mpTransport);
		if (ret != PTPErrNone)
			return ret;
	
		//-- For Timing
		Sleep(10);

		ret = m_pPTPHost->mpHostOpHandler->getSETresponse(&m_PTPInfo, m_pPTPHost->mpTransport);
		if (ret != PTPErrNone)
			return ret;

		//-- For Timing
		Sleep(10);
		
		ret = m_pPTPHost->mpHostOpHandler->openSession(&m_PTPInfo, m_pPTPHost->mpTransport);
		if (ret != PTPErrNone)
			return ret;

	}
	else if(nType == DEVICE_NOTIFY_DEVICEREMOVECOMPLETE)
	{
		strOutputMsg = L"<USB Disconnected>\r\n";
	
		if(m_pPTPHost)
		{
			m_pPTPHost->setStatusForPTPHOST(PTPHOST_STATUS_DISCONNECTED);
			m_pPTPHost->terminate();
			m_pPTPHost->destroy();		
			m_pPTPHost->mIsCreated = 0;
					
			delete m_pPTPHost;
			m_pPTPHost = NULL;		
		}
	}
	else if(nType == DEVICE_NOTIFY_LOCATION_INFORMATION)
	{
		strOutputMsg.Format(_T("device:[%s]\r\n"),(char*)pParam1);
	}
	else if(nType == DEVICE_NOTIFY_DEVICEDESC)
	{
		strOutputMsg.Format(_T("type:[%s]\r\n"),(char*)pParam1);
	}
	else
	{
		strOutputMsg.Format(_T("etc:[%s]\r\n"),(char*)pParam1);
	}
	return 0;
}
	
BOOL CSdiCoreWinXp::RegisterCallbackForEvent()
{
	BOOL retValue = FALSE;
	CUSBDevice *usbDevice = CUSBDevice::GetInstance();

	TRACE(L"RegisterCallbackForEvent\n");
	//retValue = usbDevice->SetNotifyCallback(ReceiveDeviceEvent);        // chlee  에러나서 주석처리

	return retValue; 
}
	
void CSdiCoreWinXp::OnSystemDeviceChanged( WPARAM wParam, LPARAM lParam )
{
	CUSBDevice *usbDevice = CUSBDevice::GetInstance();

	TRACE(L"OnSystemDeviceChanged\n");
	usbDevice->NotifyDeviceChange(wParam,lParam);

}
	
//------------------------------------------------------------------------------
//! @function	SDI FUNCTIONS
//! @brief				
//! @date		2013-12-02
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//------------------------------------------------------------------------------
SdiResult CSdiCoreWinXp::SdiInitSession(char* strDevName)
{
    int cnt = 0;	

	if(strDevName==NULL) 
		return SDI_ERR_INVALID_PARAMETER;

	CString temp;
	int length = MultiByteToWideChar(CP_ACP,0,strDevName,-1,NULL,NULL);
	WCHAR *ustr = new WCHAR[length];

	MultiByteToWideChar(CP_ACP,0,strDevName,strlen(strDevName)+1,ustr,length);
	temp.Format(_T("%s"),ustr);
	delete [] ustr;

	if(m_pPTPHost != NULL)
    {
        delete m_pPTPHost;
        m_pPTPHost = NULL;
    }

	m_pPTPHost = new PTPHOST;
	m_pPTPHost->setDevName(temp);

    int ret = InitPtpHost();

	if(ret == PTP_ERROR_CREATE_INSTANCE) 
		return SDI_ERR_DEVICE_OPEN_FAILED;
    else 
		return SDI_ERR_OK;
}
	
SdiResult CSdiCoreWinXp::SdiOpenSession()
{
	int nResult = SDI_ERR_OK;
	if(m_pPTPHost==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;

	nResult = m_pPTPHost->mpHostOpHandler->openSession(&m_PTPInfo, m_pPTPHost->mpTransport);

	if(nResult == PTPErrCannotSendCommand)
		return SDI_ERR_DEVICE_DISCONNECTED;
	else
	{
		nResult = m_pPTPHost->mpHostOpHandler->StartRemoteStudio(&m_PTPInfo, m_pPTPHost->mpTransport);				// chlee
		if(nResult==PTP_RC_OK) 
			return SDI_ERR_OK;
		else if(nResult==PTPErrCannoESMetResponse) 
			return SDI_ERR_DEVICE_DISCONNECTED;
		else 
			return nResult;
	}
}

SdiResult CSdiCoreWinXp::SdiExOpenSession()
{
	int nResult = SDI_ERR_OK;
	if(m_pPTPHost==NULL) 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;

	nResult = m_pPTPHost->mpHostOpHandler->openSession(&m_PTPInfo, m_pPTPHost->mpTransport);
	return nResult;
}

SdiResult CSdiCoreWinXp::SdiCloseSession()
{
	int cnt = 0;
	int sel=cnt;

	//-- Terminate Session
	if(m_pPTPHost!=NULL)
	{
		m_pPTPHost->setStatusForPTPHOST(PTPHOST_STATUS_DISCONNECTED);
		m_pPTPHost->terminate();
		m_pPTPHost->destroy();
		m_pPTPHost->mIsCreated = 0;
		delete m_pPTPHost;
		m_pPTPHost = NULL;
	}
	else 
		return SDI_ERR_DEVICE_NOT_INITIALIZED;
	
	//-- Close Session
	return m_pPTPHost->mpHostOpHandler->closeSession(&m_PTPInfo, m_pPTPHost->mpTransport);
}
	
SdiResult CSdiCoreWinXp::SdiSendAdjCapture(SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size)
{
    return m_pPTPHost->mpHostOpHandler->SdisendCapture(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes, data,size);        
}
	
SdiResult CSdiCoreWinXp::SdiSendAdjCapture(CString strSaveName)
{
	 return 0;
}

SdiResult CSdiCoreWinXp::SdiGetDevPropDesc(WORD wPropCode, WORD wAllFlag) 
{
	int nResult;
	int dataSize = 1;                
	int tempSize = 20;
	unsigned char *pData = '\0';

	SdiUInt32 adjReq[5] ={1, };
	SdiUInt32 adjRes[3];
  
	unsigned short msgId = wPropCode;
    
	adjReq[0] = msgId ;
	adjReq[4] =(SdiUInt32)dataSize;

	nResult = m_pPTPHost->mpHostOpHandler->SdiGetDevPropDesc(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes, (SdiUInt8**)&pData,(SdiUInt32&)dataSize);
	
	if(nResult!=SDI_ERR_OK)
	{
		if(pData!='\0') 
			delete[] pData;
		return nResult;
	}

	if(pData=='\0') 
		return SDI_ERR_INTERNAL;

	m_DeviceInfoMgr.CleanAll(wPropCode);
	UINT offSet = 0;
	USHORT propCode, dataType;
	UCHAR getSet;
	propCode = (pData)[offSet]+(pData)[offSet+1]*0x100; offSet+=2;
	dataType = (pData)[offSet]+(pData)[offSet+1]*0x100; offSet+=2;
	getSet= (pData)[offSet]; offSet+=1;

	TRACE("propCode : 0x%x\n", propCode);
	TRACE("dataType : 0x%x\n", dataType);
	TRACE("get/set  : 0x%x\n", getSet);

	//ULONG cur_val, def_val, form_val;
	ULONG cur_val, def_val, form_val;
	UCHAR formFlag;
	USHORT enum_num;

	UCHAR str_len;
	TCHAR *str;
	cur_val = def_val = form_val = 0x0;
	CString temp;
	
	m_DeviceInfoMgr.SetPropCode(propCode);
	switch(dataType)
	{
	case PTP_DPV_INT8 :
	case PTP_DPV_UINT8:		// UINT8
		{
			switch(dataType)
			{
			case PTP_DPV_INT8:
				m_DeviceInfoMgr.SetType(wPropCode, PTP_VALUE_INT_8);
				break;
			case PTP_DPV_UINT8:
				m_DeviceInfoMgr.SetType(wPropCode, PTP_VALUE_UINT_8);
				break;
			}
					
			def_val = (pData)[offSet]; offSet+=1;
			cur_val = (pData)[offSet]; offSet+=1;
			//-- Set Current Value
			m_DeviceInfoMgr.SetDefaultEnum(wPropCode,def_val);
			m_DeviceInfoMgr.SetCurrentEnum(wPropCode,cur_val);										

			formFlag = (pData)[offSet]; offSet+=1;					
			switch(formFlag){
			case PTP_DPF_NONE:
				break;
			case PTP_DPF_RANGE:
				form_val = (pData)[offSet]; offSet+=1;	//min
				form_val = (pData)[offSet]; offSet+=1;	//max
				form_val = (pData)[offSet]; offSet+=1;	//step
				break;
			case PTP_DPF_ENUM: 
				enum_num = (pData)[offSet]+(pData)[offSet+1]*0x100; offSet+=2;
				for(int i=0;i<enum_num;i++)
				{
					form_val = (pData)[offSet]; offSet+=1;
					//-- 2011-06-09 hongsu.jung
					m_DeviceInfoMgr.AddEnum(wPropCode,form_val);
				}
				break;
			default:
				break;
			}
			break;
		}
	case PTP_DPV_UINT16 :		// UINT16
		{
			//-- 2011-06-09 hongsu.jung
			m_DeviceInfoMgr.SetType(wPropCode, PTP_VALUE_UINT_16);

			//-- Default Value
			//-- 2011-06-09 hongsu.jung
			def_val =  (pData)[offSet]+(pData)[offSet+1]*0x100; offSet+=2;					
			cur_val = (pData)[offSet]+(pData)[offSet+1]*0x100; offSet+=2;
					
			m_DeviceInfoMgr.SetDefaultEnum(wPropCode,def_val);
			m_DeviceInfoMgr.SetCurrentEnum(wPropCode,cur_val);		

			//-- Flag
			formFlag =(pData)[offSet]; offSet+=1;		
			switch(formFlag){
			case PTP_DPF_NONE:
				break;
			case PTP_DPF_RANGE:
				form_val = (pData)[offSet]+(pData)[offSet+1]*0x100; offSet+=2;	//min
				form_val = (pData)[offSet]+(pData)[offSet+1]*0x100; offSet+=2;	//max
				form_val = (pData)[offSet]+(pData)[offSet+1]*0x100; offSet+=2;	//step
				break;
			case PTP_DPF_ENUM:
				//TRACE("EnumForm\n");
				enum_num = (pData)[offSet]+(pData)[offSet+1]*0x100;; offSet+=2;
				for(int i=0;i<enum_num;i++){
					form_val= (pData)[offSet]+(pData)[offSet+1]*0x100;; offSet+=2;
					//TRACE("0x%x\n", form_val);
					//-- 2011-06-09 hongsu.jung
					m_DeviceInfoMgr.AddEnum(wPropCode,form_val);							
				}							
				break;
			default:
				break;
			}
			break;
		}
	case PTP_DPV_UINT32	:
		{
			//-- 2011-06-09 hongsu.jung
			m_DeviceInfoMgr.SetType(wPropCode, PTP_VALUE_UINT_32);

			//-- Default Value
			//-- 2011-06-09 hongsu.jung
			def_val = (pData)[offSet] + (pData)[offSet+1]*0x100 + (pData)[offSet+2]*0x10000 + (pData)[offSet+3]*0x1000000; offSet+=4;					
			cur_val = (pData)[offSet] + (pData)[offSet+1]*0x100 + (pData)[offSet+2]*0x10000 + (pData)[offSet+3]*0x1000000; offSet+=4;

			m_DeviceInfoMgr.SetDefaultEnum(wPropCode,def_val);
			m_DeviceInfoMgr.SetCurrentEnum(wPropCode,cur_val);					
			//-- Flag
			formFlag = (pData)[offSet] ; offSet+=1;					
			switch(formFlag){
			case PTP_DPF_NONE:
				break;
			case PTP_DPF_RANGE:
				form_val = (pData)[offSet] + (pData)[offSet+1]*0x100 + (pData)[offSet+2]*0x10000 + (pData)[offSet+3]*0x1000000; offSet+=4;	//min
				form_val = (pData)[offSet] + (pData)[offSet+1]*0x100 + (pData)[offSet+2]*0x10000 + (pData)[offSet+3]*0x1000000; offSet+=4;	//max
				form_val = (pData)[offSet] + (pData)[offSet+1]*0x100 + (pData)[offSet+2]*0x10000 + (pData)[offSet+3]*0x1000000; offSet+=4;	//step
				break;
			case PTP_DPF_ENUM:
				//TRACE("EnumForm\n");
				enum_num = (pData)[offSet] + (pData)[offSet+1]*0x100; offSet+=2;
				for(int i=0;i<enum_num;i++){
					form_val = (pData)[offSet] + (pData)[offSet+1]*0x100 + (pData)[offSet+2]*0x10000 + (pData)[offSet+3]*0x1000000; offSet+=4;
					//TRACE("0x%x\n", form_val);
					//-- 2011-06-09 hongsu.jung
					m_DeviceInfoMgr.AddEnum(wPropCode,form_val);							
				}							
				break;
			default:
				break;
			}
			break;
		}
	case PTP_DPV_STR:	// STR
		{
			//-- 2011-06-09 hongsu.jung
			m_DeviceInfoMgr.SetType(wPropCode, PTP_VALUE_STRING);

			int current;
			str_len =  (pData)[offSet]; offSet+=1;
			str = new TCHAR[str_len + 1];
			current=offSet;
			for(int i=0;i<str_len;i++) {
				offSet=current+(i*2); // default
				str[i]=(pData)[offSet]; 
			}
			str[str_len]=NULL;
			temp.Format(_T("%s"),str);
			//-- Set Default
			m_DeviceInfoMgr.SetDefaultStr(wPropCode, temp);
			delete[] str;

			offSet+=2;
			str_len = (pData)[offSet]; offSet+=1;
			str = new TCHAR[str_len + 1];
			current=offSet;
			for(int i=0;i<str_len;i++)
			{
				offSet=current+(i*2); // current
				str[i]=(pData)[offSet]; 
			}
			str[str_len]=NULL;
			offSet+=2; // 
			temp.Format(_T("%s"),str);
			//-- Set Current
			m_DeviceInfoMgr.SetCurrentStr(wPropCode,temp);						
			delete[] str;

			formFlag = (pData)[offSet]; offSet+=1;
			CString temp;
			switch(formFlag){
			case PTP_DPF_ENUM:
				//TRACE("EnumForm Start\n");
				enum_num = (pData)[offSet]+(pData)[offSet+1]*0x100; offSet+=2;
				for(int i=0;i<enum_num;i++)
				{
					str_len =  (pData)[offSet]; offSet+=1;
					current=offSet;
					str = new TCHAR[str_len*2 + 1];
					for(int j=0;j<str_len;j++)
					{
						offSet=current+(j*2); // default
						str[j] = (pData)[offSet]; 
					}
					str[str_len]=NULL;
					offSet+=2;
					temp.Format(_T("%s"),str);
					//-- 2011-06-09 hongsu.jung
					m_DeviceInfoMgr.AddStr(wPropCode,temp);			
					delete[] str;
				}
				//TRACE("EnumForm End\n");
				break;
			default:
				break;
			}
			break;
		}
	}
	if(pData!='\0') 
		delete[] pData;

	return nResult;
}

SdiResult CSdiCoreWinXp::SdiSetDevPropValue(int nPTPCode, int nType, LPARAM lpValue)
{
	int nResult;
	int tempSize = 20;

	SdiUInt32 adjReq[5] ={1, };
	SdiUInt32 adjRes[3];
	unsigned short msgId = nPTPCode;
	adjReq[0] = msgId ;

	switch(nType)
	{
	case PTP_VALUE_INT_8:
	case PTP_VALUE_UINT_8:
	case PTP_VALUE_UINT_16:
	case PTP_VALUE_UINT_32:
		nResult = m_pPTPHost->mpHostOpHandler->SdiSetDevPropValue(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes,nType, (int)lpValue);
		break;
	case PTP_VALUE_STRING:
		nResult = m_pPTPHost->mpHostOpHandler->SdiSetDevPropValue(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes, (CString)(LPCTSTR)lpValue);
		break;
	}	

	UINT offSet = 0;
	nResult = m_PTPInfo.Response.Param1;	
	nResult = (nResult == 0x2001) ? SDI_ERR_OK : nResult;

	return nResult;
}

SdiResult CSdiCoreWinXp::SdiSendShutterCapture(SdiInt8 nShutter)
{
	//-- 2011-07-28 jeansu : shutter speed order change.
	switch(nShutter)
	{
	case S1_PRESS:
	case S2_PRESS:
	case S2_RELEASE:
	case S1_RELEASE:
	//-- add movie rec button
	case S3_PRESS:
    case S3_RELEASE:
		return RequireCaptureExec(nShutter);
	default:
		break;
	}
	return SDI_ERR_INVALID_PARAMETER;
}

int CSdiCoreWinXp::RequireCaptureExec(WORD code)
{
	int dataSize = 1;                
	int tempSize = 20;

	SdiUInt32 adjReq[5] ={1, };
	SdiUInt32 adjRes[3];
	unsigned short msgId = code;
    
	adjReq[0] = msgId ;
	adjReq[4] =(SdiUInt32)dataSize;

	return m_pPTPHost->mpHostOpHandler->SdiRequireCaptureExec(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes);
}

SdiResult CSdiCoreWinXp::SdiSendAdjCompleteCapture(CString strSaveName, bool&, int nFileNum, int nClose)
{
	return CaptureCompleteExec(strSaveName);
}

//NX3000 20150119
SdiResult CSdiCoreWinXp::SdiSendAdjCompleteCapture(CString strSaveName, bool&)
{
	return CaptureCompleteExec(strSaveName);
}

SdiResult CSdiCoreWinXp::SdiSendAdjCompleteMovie(CString strSaveName, int nWaitSaveDSC, int nFileNum, int nClose) //-- 2014-09-19 joonho.kim
{
	return MovieCompleteExec(strSaveName);
}


SdiResult CSdiCoreWinXp::SdiSendAdjMovieTransfer(CString strSaveName, int nfilenum, int nClose)
{
	return 0;
}
int CSdiCoreWinXp::CaptureCompleteExec(CString strSaveName)
{
	int nResult;
	int dataSize = 1;                
	int tempSize = 20;

	unsigned char *pData = '\0';
	SdiUInt32 adjReq[5] ={1, };
	SdiUInt32 adjRes[3];
	unsigned short msgId = 0;
    
	adjReq[0] = msgId ;
	adjReq[4] =(SdiUInt32)dataSize;

	nResult = m_pPTPHost->mpHostOpHandler->SdiCaptureCompleteExec(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes, (SdiUInt8**)&pData,(SdiUInt32&)dataSize);

	if(dataSize!=0 && pData!='\0')
	{
		CFile file;
		file.Open(strSaveName, CFile::modeWrite |CFile::shareExclusive | CFile::modeCreate ,NULL);
		file.Write(pData+1,dataSize-1);
		file.Close();
	}
	if(pData!='\0') delete[] pData;

	if(nResult==PTP_RC_OK || nResult==PTPErrNone) return SDI_ERR_OK;	// -- 2011-7-26 chlee
	else return nResult;
}

int CSdiCoreWinXp::MovieCompleteExec(CString strSaveName)
{
	int nResult;
	int dataSize = 1;                
	int tempSize = 20;

	unsigned char *pData = '\0';
	SdiUInt32 adjReq[5] ={1, };
	SdiUInt32 adjRes[3];
	unsigned short msgId = 0;
    
	adjReq[0] = msgId ;
	adjReq[4] =(SdiUInt32)dataSize;

	nResult = m_pPTPHost->mpHostOpHandler->SdiMovieCompleteExec(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes, (SdiUInt8**)&pData,(SdiUInt32&)dataSize);

	if(dataSize!=0 && pData!='\0')
	{
		CFile file;
		file.Open(strSaveName, CFile::modeWrite |CFile::shareExclusive | CFile::modeCreate ,NULL);
		file.Write(pData,dataSize);
		file.Close();
	}
	if(pData!='\0') delete[] pData;

	if(nResult==PTP_RC_OK || nResult==PTPErrNone) return SDI_ERR_OK;	// -- 2011-7-26 chlee
	else return nResult;
}
SdiResult CSdiCoreWinXp::SdiSetDevUniqueID(int nPTPCode,  CString strUUID)
{
	return TRUE;
}
SdiResult CSdiCoreWinXp::SdiGetDevUniqueID(int nPTPCode,  char **chUniqueID)
{
	//-- 2011-06-23 chlee
	int nResult;
	int dataSize = 1;                
	int tempSize = 20;

	SdiUInt32 adjReq[5] ={1, };
	SdiUInt32 adjRes[3];
    
	unsigned short msgId = nPTPCode;
    
	adjReq[0] = msgId ;
	adjReq[4] =(SdiUInt32)dataSize;

	nResult = m_pPTPHost->mpHostOpHandler->SdiGetDevUniqueID(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes, (SdiUInt8**)chUniqueID,(SdiUInt32&)dataSize);

	if(chUniqueID=='\0' || *chUniqueID == NULL)
		return PTPErrUSBIOWrite;

	int len =(int) (*chUniqueID)[5];
	int i;
	for( i=0; i<len; i++)
	{
		(*chUniqueID)[i] = (*chUniqueID)[i*2+6];
	}
	(*chUniqueID)[i]=NULL;

	nResult = (nResult == 0x2001) ? SDI_ERR_OK : E_FAIL;			//-- 2011.7.26 chlee  SDI_ERR_OK ADD

	return nResult;
}

SdiResult CSdiCoreWinXp::SdiGetDevModeIndex(int nPTPCode, int &nModeIndex)
{
	int nResult;
	int dataSize = 1;                
	int tempSize = 20;
	unsigned char *pData = '\0';

	SdiUInt32 adjReq[5] ={1, };
	SdiUInt32 adjRes[3];
	unsigned short msgId = nPTPCode;
    
	adjReq[0] = msgId ;
	adjReq[4] =(SdiUInt32)dataSize;

	nResult = m_pPTPHost->mpHostOpHandler->SdiGetDevModeIndex(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes, (SdiUInt8**) &pData,(SdiUInt32&)dataSize);

	if(pData=='\0')		// error code 대체 해야 하나.
		return nResult;

	m_DeviceInfoMgr.CleanAll(nPTPCode);
	UINT offSet = 0;
	USHORT propCode, dataType;
	UCHAR getSet;
	propCode = (pData)[offSet]+(pData)[offSet+1]*0x100; offSet+=2;
	dataType = (pData)[offSet]+(pData)[offSet+1]*0x100; offSet+=2;
	getSet= (pData)[offSet]; offSet+=1;
	ULONG cur_val, def_val, form_val;
	cur_val = def_val = form_val = 0x0;
	CString temp;
	switch(dataType)
	{			
	case PTP_DPV_UINT16 :		// UINT16
		{	
			//-- 2011-06-09 hongsu.jung
			m_DeviceInfoMgr.SetType(propCode, PTP_VALUE_UINT_16);

			//-- Default Value
			//-- 2011-06-09 hongsu.jung
			def_val = (pData)[offSet]+(pData)[offSet+1]*0x100; offSet+=2;					
			cur_val = (pData)[offSet]+(pData)[offSet+1]*0x100; offSet+=2;

			m_DeviceInfoMgr.SetDefaultEnum(propCode,def_val);
			m_DeviceInfoMgr.SetCurrentEnum(propCode,cur_val);	

			nModeIndex = cur_val;
		}
		break;
	default:
		break;
	}

	if(pData!='\0')
		delete[] pData;

	return SDI_ERR_OK;
}

SdiResult CSdiCoreWinXp::SdiSendAdjLiveviewInfo(RSLiveviewInfo* pLiveviewInfo)
{
	int nResult;
	int dataSize = 1;                
	int tempSize = 20;
	unsigned char *pData = '\0';

	SdiUInt32 adjReq[5] ={1, };
	SdiUInt32 adjRes[3];
	unsigned short msgId = 0x1234;
    
	adjReq[0] = msgId ;
	adjReq[4] =(SdiUInt32)dataSize;

	nResult = m_pPTPHost->mpHostOpHandler->SdiSendAdjLiveviewInfo(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes, (SdiUInt8**)&pData,(SdiUInt32&)dataSize);
	
	UINT offSet = 0;
	if(pData!='\0')
	{
		memcpy(pLiveviewInfo, pData, dataSize);
		TRACE(_T("w: %d, h: %d, off w: %d, off y: %d\n"), pLiveviewInfo->imageSizeWidth, pLiveviewInfo->imageSizeHeight, pLiveviewInfo->offsetSizeWidth, pLiveviewInfo->offsetSizeWidth);
		delete[] pData;
	}

	return nResult;
}

SdiResult CSdiCoreWinXp::SdiSendAdjLiveview(unsigned char* pYUV)
{
	//-- 2011-06-23 chlee
	int nResult;
	const int dataSize = 1;                
	int tempSize = 20;
	unsigned char *pData = '\0';	

	SdiUInt32 adjReq[5] ={1, };
	SdiUInt32 adjRes[3];
	SdiUInt32 size;
	
	unsigned short eventId = 0x1234;    
	unsigned short msgId = 0x1001;
    
	adjReq[0] = (msgId << 16) | eventId;
	adjReq[4] =(SdiUInt32)dataSize;

	TRACE(_T(" Start SdiSendLiveview\n"));
	nResult = m_pPTPHost->mpHostOpHandler->SdiSendLiveview(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes, &pData, &size); 				// Windows XP
	TRACE(_T(" End   SdiSendLiveview\n"));

	if(pData!='\0')
	{
		memcpy(pYUV,pData,size);
		delete[] pData;
	}
	else 
		TRACE(_T(" Error!! SdiSendLiveview\n"));

	return SDI_ERR_OK;
}

SdiResult CSdiCoreWinXp::SetFocusPosition(int nCurPos)
{
	int dataSize = 1;                
	int tempSize = 20;

	SdiUInt32 adjReq[5] ={1, };
	SdiUInt32 adjRes[3];
	unsigned short msgId = nCurPos;
    
	adjReq[0] = msgId ;
	adjReq[4] =(SdiUInt32)dataSize;

	return m_pPTPHost->mpHostOpHandler->SetFocusPosition(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes);
}

SdiResult CSdiCoreWinXp::SdiSetFocusPosition(FocusPosition * ptFocusPos, SdiInt8 nType)
{
	int nResult;
	int dataSize = 1;                
	int tempSize = 20;
	unsigned char *pData = '\0';

	SdiUInt32 adjReq[5] ={1, };
	SdiUInt32 adjRes[3];
	unsigned short msgId = 0x0;
    
	adjReq[0] = msgId ;
	adjReq[4] =(SdiUInt32)dataSize;

	nResult = m_pPTPHost->mpHostOpHandler->SdiSetFocusPosition(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes, (SdiUInt8**)&pData,(SdiUInt32&)dataSize);

	if(pData!='\0') memcpy(ptFocusPos, pData, dataSize);

	int nCurPos;
	int nPosDetailUnit	= (ptFocusPos->max - ptFocusPos->min) / FOCUS_DETAIL_UNIT;
	int nPosRoughUnit	= (ptFocusPos->max - ptFocusPos->min) / FOCUS_ROUGH_UNIT;

	switch(nType)
	{
	case FOCUS_REW	:	nCurPos = ptFocusPos->current - nPosRoughUnit;	break;
	case FOCUS_PREV	:	nCurPos = ptFocusPos->current - nPosDetailUnit;		break;
	case FOCUS_NEXT	:	nCurPos = ptFocusPos->current + nPosDetailUnit;	break;
	case FOCUS_FF		:	nCurPos = ptFocusPos->current +  nPosRoughUnit;	break;
	case FOCUS_GET	:
	default:
		nCurPos = ptFocusPos->current;	break;
	}

	if(nCurPos < (int)ptFocusPos->min)
		nCurPos = ptFocusPos->min;
	if(nCurPos > (int)ptFocusPos->max)
		nCurPos = ptFocusPos->max;

	TRACE(_T("[WIN Xp] Focus :: [%04d < %04d < %04d] ==> Move To %04d\n"),ptFocusPos->min, ptFocusPos->current, ptFocusPos->max, nCurPos);			
	
	SetFocusPosition( nCurPos);  
	
	//-- 2011-07-06 hongsu.jung
	//-- Change CurPos
	ptFocusPos->current = nCurPos;	

	if(pData!='\0') 
		delete[] pData;
	return nResult;
}

SdiResult CSdiCoreWinXp::SdiSetFocusPosition(SdiInt32 nValue)
{	
	return SetFocusPosition(nValue);
}

SdiResult CSdiCoreWinXp::SdiCheckEvent(SdiInt32& _nEventExist, SdiUIntArray* pArList)
{
	int nResult;
	int dataSize = 1;
	int tempSize = 20;
	unsigned char *pData='\0';
	
	SdiUInt32 adjReq[5] ={1, };
	SdiUInt32 adjRes[3];
	unsigned short msgId = 0x1234;
    
	adjReq[0] = msgId ;
	adjReq[4] =(SdiUInt32)dataSize;

	nResult = m_pPTPHost->mpHostOpHandler->SdiSendCheckEvent(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes, (SdiUInt8**) &pData,(SdiUInt32&)dataSize);

	UINT offSet = 0;

	nResult = m_PTPInfo.Response.Param1;

	if(pData!='\0') 
		free(pData);

	return nResult;
}

SdiResult CSdiCoreWinXp::SdiGetEvent(BYTE **pData,SdiInt32 *size)
{
	int nResult;
	int dataSize = 1;                
	int tempSize = 20;

	SdiUInt32 adjReq[5] ={1, };
	SdiUInt32 adjRes[3];
    
	unsigned short msgId = 0x0001;
    
	adjReq[0] = msgId ;
	adjReq[4] =(SdiUInt32)dataSize;

	nResult = m_pPTPHost->mpHostOpHandler->SdiGetEvent(&m_PTPInfo, m_pPTPHost->mpTransport, adjReq, adjRes, (SdiUInt8**)pData,(SdiUInt32&)dataSize);

	*size = dataSize;

	return nResult;
}

//-- 2013-03-05 hongsu.jung
//-- Capture
SdiResult CSdiCoreWinXp::SdiSendAdjRequireCaptureOnce(int nModel)
{
	SdiResult result = SDI_ERR_OK;
	
	result = SdiSendShutterCapture(S1_PRESS);
	if(result!=SDI_ERR_OK) 
		return result;
	Sleep(1000);

	result = SdiSendShutterCapture(S2_PRESS);
	if(result!=SDI_ERR_OK) 
		return result;
	Sleep(1000);

	result = SdiSendShutterCapture(S2_RELEASE);
	if(result!=SDI_ERR_OK) 
		return result;
	Sleep(1000);

	result = SdiSendShutterCapture(S1_RELEASE);
	return result;
}

SdiResult CSdiCoreWinXp::SdiFormatDevice()
{
	return m_pPTPHost->mpHostOpHandler->SdiFormat(&m_PTPInfo, m_pPTPHost->mpTransport);
}

SdiResult CSdiCoreWinXp::SdiResetDevice(int nParam)
{
	return m_pPTPHost->mpHostOpHandler->SdiReset(&m_PTPInfo, m_pPTPHost->mpTransport);
}

//dh0.seo 2014-11-04
SdiResult CSdiCoreWinXp::SdiSensorCleaningDevice()
{
	return m_pPTPHost->mpHostOpHandler->SdiSensorCleaning(&m_PTPInfo, m_pPTPHost->mpTransport);
}

//CMiLRe 20141113 IntervalCapture Stop 추가
SdiResult CSdiCoreWinXp::SdiIntervalCaptureStop()
{
	return SDI_ERR_OK;
}

//CMiLRe 20141127 Display Save Mode 에서 Wake up
SdiResult CSdiCoreWinXp::SdiDisplaySaveModeWakeUp()
{
	return SDI_ERR_OK;
}

SdiResult CSdiCoreWinXp::SdiFWUpdate()
{
	return SDI_ERR_OK;//m_pPTPHost->mpHostOpHandler->SdiReset(&m_PTPInfo, m_pPTPHost->mpTransport);
}

SdiResult CSdiCoreWinXp::SdiHalfShutter( int nRelease ) {return SDI_ERR_OK;}

SdiResult CSdiCoreWinXp::SdiSetFocusFrame(int nX, int nY, int nMagnification)	{return SDI_ERR_OK;}

SdiResult CSdiCoreWinXp::SdiGetFocusFrame(BYTE **pData,SdiInt32 *size)	{return SDI_ERR_OK;}

//CMiLRe 2015129 펌웨어 다운로드 경로 추가
SdiResult CSdiCoreWinXp::SdiFWDownload(CString strPath){return SDI_ERR_OK;}
//NX3000
SdiResult CSdiCoreWinXp::SdiDeviceReboot(){return SDI_ERR_OK;}
SdiResult CSdiCoreWinXp::SdiDevicePowerOff(){return SDI_ERR_OK;}

SdiResult CSdiCoreWinXp::SdiDeviceExportLog(){return SDI_ERR_OK;}
//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-8-23
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
SdiResult CSdiCoreWinXp::SdiGetPTPDeviceInfo(PTPDeviceInfo * ptDevInfoType)
{
	return SDI_ERR_OK;
}