// ESMMakeMovieFileMgr.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ESMParseMovieFileMgr.h"

// CESMMakeMovieFileMgr

IMPLEMENT_DYNCREATE(CESMParseMovieFileMgr, CWinThread)

CESMParseMovieFileMgr::CESMParseMovieFileMgr()
{
	m_bThreadStop = FALSE;
	m_bWait = FALSE;
	m_nFrameRate = FRAMERATE30;
	InitializeCriticalSection(&m_criRecord);
}

CESMParseMovieFileMgr::~CESMParseMovieFileMgr()
{
	DeleteCriticalSection(&m_criRecord);
}

void CESMParseMovieFileMgr::AddMsg(SdiFileInfo* pMsg)
{
	if(!m_bThreadStop)
	{
		m_arMsg.Add((CObject*)pMsg);
	}
}

void CESMParseMovieFileMgr::AddMsg(CString strPath, CString strBackup)
{
	if(!m_bThreadStop)
	{
		//m_arMsg.Add((CObject*)pMsg);
		m_arrPath.Add(strPath);
		m_arrBackupPath.Add(strBackup);
	}
}

BOOL CESMParseMovieFileMgr::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CESMParseMovieFileMgr::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	return CWinThread::ExitInstance();
}

int CESMParseMovieFileMgr::Run()
{
#if 0
	SdiFileInfo* pFileInfo	= NULL;
	while(!m_bThreadStop)
	{
		Sleep(1);
		int nAll = m_arMsg.GetSize();

		while(nAll--)
		{
			Sleep(1);
			/*if(m_bWait)
			{
				nAll++;
				continue;
			}*/

			pFileInfo = (SdiFileInfo*)m_arMsg.GetAt(0);

			if(!pFileInfo)
			{
				m_arMsg.RemoveAt(0);
				continue;
			}

			m_arMsg.RemoveAt(0);

			////////////////////////////////
			BOOL bRetry = FALSE;
			CString strPath;

			while(1)
			{
				m_MovieMaker.m_nFrameRate = m_nFrameRate;
				m_MovieMaker.m_nMaxFrame = m_nMaxFrame;
				strPath = m_MovieMaker.MakeMovieFile(pFileInfo, pFileInfo->strBackupPath, bRetry);
				if(!strPath.IsEmpty())
				{
					if( pFileInfo->strBackupPath != _T(""))
						CopyFile(strPath, pFileInfo->strBackupPath, FALSE);

					/*CString* pStrData = NULL;
					pStrData = new CString;

					pStrData->Format(_T("%s"), strPath);
					RSEvent* pMsg	= new RSEvent;
					pMsg->message	= WM_CAPTURE_FILE_WRITE_DONE;
					pMsg->pParam	= (LPARAM)pStrData;
					::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);*/
				}

				if(m_MovieMaker.m_arrCurrentFrame.size() < m_nMaxFrame)
					break;
				else
					bRetry = TRUE;

			}
			////////////////////////////////

			delete[] pFileInfo->pImageBuffer;
			delete pFileInfo;

			m_bWait = TRUE;
		}
	}
#else
	CString strPath, strBackupPath;
	while(!m_bThreadStop)
	{
		Sleep(1);
		int nAll = m_arrPath.GetCount();

		while(nAll--)
		{
			Sleep(1);
			

			strPath = (CString)m_arrPath.GetAt(0);
			strBackupPath = (CString)m_arrBackupPath.GetAt(0);

			/*if(!strPath.IsEmpty())
			{
				m_arrPath.RemoveAt(0);
				m_arrBackupPath.RemoveAt(0);
				continue;
			}*/

			m_arrPath.RemoveAt(0);
			m_arrBackupPath.RemoveAt(0);

			////////////////////////////////
			EnterCriticalSection(&m_criRecord);
			CopyFile(strPath, strBackupPath, FALSE);
			LeaveCriticalSection(&m_criRecord);
			////////////////////////////////

			
			m_bWait = TRUE;
		}
	}
#endif
	return 0;
}

BEGIN_MESSAGE_MAP(CESMParseMovieFileMgr, CWinThread)
END_MESSAGE_MAP()


// CESMMakeMovieFileMgr 메시지 처리기입니다.
