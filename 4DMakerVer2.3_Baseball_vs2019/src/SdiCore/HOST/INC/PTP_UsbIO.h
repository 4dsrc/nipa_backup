/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_UsbIO.h
 \brief Define PTP USB I/O Class inherited from PTP Transport I/O Class.
 \date 2009-06-11
 \version 0.70 
*/

#ifndef __PTP_USB_IO_H__
#define __PTP_USB_IO_H__


#include "USBHAL_Instance.h"
#include "PTP_Common.h"

#include "PTP_Endian.h"
#include "PTP_TransportIO.h"


class PTPUSBIO : public PTPTransportIO 
{
private:
	void printUSBBulkContainer(PTPUSBBulkContainerType* usbBulk);
	void printUSBEventContainer(PTPUSBEventContainerType* usbEvent);
	
	SdiBool mbNeedSendingNullPacket;
	IUSBHAL* mpIfUSBHAL;

public:
	PTPUSBIO();
	~PTPUSBIO();

private:	
	int open(char* name);
	void close(void);

#ifdef __PTP_HOST__
	int sendCommand(PTPContainerType* command);
	int getResponse(PTPContainerType* res);

#else
	int getRequest(PTPContainerType* req);
	int sendResponse(PTPContainerType* resp);

	int sendEvent(PTPContainerType* event);
	int sendData(PTPContainerType* ptpCntr, char* txData, SdiUInt32 size, SdiUInt32 totalSize);
	int sendDataOnly(char* txData, SdiUInt32 size, SdiBool bIsLastData);
#endif
	int getData(PTPContainerType* req, char* rxData, SdiUInt32 size, int opt, void* optParam);

	void reset(int targetIO);
	int getConnectionStatus(void);

public:
    int SdisetRequeset(PTPInfoType *ptpInfo);
    int SdigetResponse(PTPInfoType *ptpInfo);

    int SdisetData(PTPInfoType *ptpInfo, char **txData, SdiUInt32 size);
    int SdigetData(PTPInfoType *ptpInfo, char **rxData, SdiUInt32& size);

    void SetTransTimeout(int timeout);
    int  GetTransTimeout();

};

#endif /* __PTP_USB_IO_H__ */

