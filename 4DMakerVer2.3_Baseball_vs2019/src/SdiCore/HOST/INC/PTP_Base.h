/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_Base.h
 \brief Define specific structure and value defined PTP Spec.
 \date 2009-06-11
 \version 0.70 
*/
#ifndef __PTP_BASE_H__
#define __PTP_BASE_H__

#include "SdiDefines.h"

/** Whether PTP supports PC or not */
#define PTP_PC_SUPPORT					1
#define PTP_EXPORT_ONLY_IN_DICM		1
#define PTP_SUPPORT_OBJHANDLES_FOR_XP	1

/** Number of PTP operation code */
// [3/4/2011 JanuS] Add Vendor Operation
#define PTP_NUM_STANDARD_OPERATION_CODE			28
#define PTP_NUM_VENDOR_OPERATION_CODE			4
#define PTP_NUM_OPERATION_CODE			PTP_NUM_STANDARD_OPERATION_CODE + PTP_NUM_VENDOR_OPERATION_CODE

/** Special value of parameter in operation request */
#define PTP_DESIRE_ALL_STORAGES			0xFFFFFFFFUL
#define PTP_DESIRE_ALL_IMAGE_FORMAT		0xFFFFFFFFUL
#define PTP_DESIRE_ROOT_PARENT			0xFFFFFFFFUL
#define PTP_DESIRE_END_OF_FILE			0xFFFFFFFFUL
#define PTP_DESIRE_ALL_OBJECTS			0xFFFFFFFFUL

/** SELF TEST - Production Mode **/
#define PTP_SELF_TEST_TYPE              0xCCAACCAAUL

/** Not used value of member in SI */
#define PTP_NOT_USED_SI				0xFFFFFFFFFFFFFFFFULL

/** Not used value of member in OI */
#define PTP_NOT_USED_OI				0x00000000UL

/** Special defined object */
#define PTP_ROOT_OBJECT				0x00000000UL

/** PTP maxium string length */
#define PTP_MAX_STRING_LEN			255

/** PTP maxium date string length */
#define PTP_MAX_DATE_LEN			20//18

/** PTP maxium keyword string length */
#define PTP_MAX_KEYWORD_LEN			32


/**
 * \brief PTP mDeviceInfo Dataset Structure
 *
 * PTP mDeviceInfo Dataset Structure (returned by GetDevInfo Operation)
 */
typedef struct _ptp_device_info_type
{
	SdiUInt16	StandardVersion;
	SdiUInt32	VendorExtensionID;
	SdiUInt16	VendorExtensionVersion;
	char*		VendorExtensionDesc;
	SdiUInt16	FunctionalMode;
	SdiUInt32	OperationsSupported_len;
	SdiUInt16*	OperationsSupported;
	SdiUInt32	EventsSupported_len;
	SdiUInt16*	EventsSupported;
	SdiUInt32	DevicePropertiesSupported_len;
	SdiUInt16*	DevicePropertiesSupported;
	SdiUInt32	CaptureFormats_len;
	SdiUInt16*	CaptureFormats;
	SdiUInt32	ImageFormats_len;
	SdiUInt16*	ImageFormats;
	char*		Manufacturer;
	char*		Model;
	char*		DeviceVersion;
	char*		SerialNumber;

} PTPDeviceInfoType;

/**
 * \brief PTP ObjectInfo Dataset Structure
 *
 * PTP ObjectInfo Dataset Structure (returned by GetObjectInfo Operation)
 */
typedef struct _ptp_object_info_type
{
	SdiUInt32	StorageID;
	SdiUInt16	ObjectFormat;
	SdiUInt16	ProtectionStatus;
	SdiUInt32	ObjectCompressedSize;
	SdiUInt16	ThumbFormat;
	SdiUInt32	ThumbCompressedSize;
	SdiUInt32	ThumbPixWidth;
	SdiUInt32	ThumbPixHeight;
	SdiUInt32	ImagePixWidth;
	SdiUInt32	ImagePixHeight;
	SdiUInt32	ImageBitDepth;
	SdiUInt32	ParentObject;
	SdiUInt16	AssociationType;
	SdiUInt32	AssociationDesc;
	SdiUInt32	SequenceNumber;
	char		Filename[PTP_MAX_STRING_LEN];
	SdiDateTime	CaptureDate[PTP_MAX_DATE_LEN];
#if 0
	SdiDateTime	ModificationDate[PTP_MAX_DATE_LEN];
#endif
	SdiDateTime*	ModificationDate;
	char*		Keywords;

} PTPObjectInfoType;

/**
 * \brief PTP StorageInfo Dataset Structure
 *
 * PTP StorageInfo Dataset Structure (returned by GetStorageInfo Operations)
 */
typedef struct _ptp_storage_info_type
{
	SdiUInt16	StorageType;
	SdiUInt16	FilesystemType;
	SdiUInt16	AccessCapability;
	SdiUInt64	MaxCapability;
	SdiUInt64	FreeSpaceInBytes;
	SdiUInt32	FreeSpaceInImages;
	char*		StorageDescription;
	char*		VolumeLabel;

} PTPStorageInfoType;

/**
 * \brief General PTP Container Structure
 *
 * PTP mRequest/Response/Event general PTP Container Structure
 */
typedef struct _ptp_container_type
{
	SdiUInt16	Code;
	SdiUInt32	SessionID;
	SdiUInt32	TransactionID;

	/* params  may be of any type of size less or equal to SdiUInt32 */
	SdiUInt32	Param1;
	SdiUInt32	Param2;
	SdiUInt32	Param3;

	/* events can only have three parameters */
	SdiUInt32	Param4;
	SdiUInt32	Param5;

	/* the number of meaningfull parameters */
	char		Nparam;

	char Data[255];
} PTPContainerType;

/** PTP USB Bulk max packet length definitions for high speed endpoints */
#define PTP_USB_BULK_HS_MAX_PACKET_LEN			512
#define PTP_USB_BULK_HDR_LEN					(sizeof(SdiUInt32)*2 + sizeof(SdiUInt16)*2)
#define PTP_USB_BULK_HDR_DATA_LEN				(sizeof(SdiUInt32)*2 + sizeof(SdiUInt32)*2)				 //-- 2011-7-9 chlee
#define PTP_USB_BULK_PAYLOAD_LEN				(PTP_USB_BULK_HS_MAX_PACKET_LEN - PTP_USB_BULK_HDR_LEN)
#define PTP_USB_BULK_REQ_LEN	       			(PTP_USB_BULK_HDR_LEN + sizeof(SdiUInt32)*5)
#define PTP_USB_BULK_RESP_LEN	     		  	(PTP_USB_BULK_HDR_LEN + sizeof(SdiUInt32)*5)

/**
 * \brief USB Bulk-Pipe Container Structure
 *
 * USB Bulk-Pipe Container Structure for PTP
 */
typedef struct _ptp_usb_bulk_container_type
{
	SdiUInt32	Length;
	SdiUInt16 	Type;
	SdiUInt16	Code;
	SdiUInt32	TransID;

	union
	{
		struct
		{
			SdiUInt32	Param1;
			SdiUInt32	Param2;
			SdiUInt32	Param3;
			SdiUInt32	Param4;
			SdiUInt32	Param5;
		} Params;

		char Data[PTP_USB_BULK_PAYLOAD_LEN];

	} Payload;

} PTPUSBBulkContainerType;

/** PTP USB event(use interrupt EP) container length definitions */
#define PTP_USB_EVENT_HDR_LEN				(sizeof(SdiUInt32)*2 + sizeof(SdiUInt16)*2)
#define PTP_USB_EVENT_REQ_LEN				(PTP_USB_EVENT_HDR_LEN + sizeof(SdiUInt32)*3)

/**
 * \brief USB Asynchronous Event Interrupt Data Format Structure
 *
 * USB Asynchronous Event Interrupt Data Format Structure for PTP
 */
typedef struct _ptp_usb_event_container_type
{
	SdiUInt32	Length;
	SdiUInt16	Type;
	SdiUInt16	Code;
	SdiUInt32	TransID;
	SdiUInt32	Param1;
	SdiUInt32	Param2;
	SdiUInt32	Param3;

} PTPUSBEventContainerType;

//-- 2011-08-29 jeansu : Event Container 정의

typedef struct _ptp_event_container
{
	SdiUInt16    Code;
	SdiUInt32    TransactionID;
	SdiUInt32    Param1;
	SdiUInt32    Param2;
	SdiUInt32    Param3;
} PTPEventContainer;

#pragma pack(1)
typedef struct _ptp_event_container_gh5
{
	SdiUInt16    Code;
	SdiUInt32    TransactionID;
	SdiUInt32    Param1;
	SdiUInt32    Param2;
	SdiUInt32    Param3;
} PTPEventContainer_GH5;
#pragma pack()

/** USB Container types */
#define PTP_USB_CONTAINER_UNDEFINED		0x0000
#define PTP_USB_CONTAINER_COMMAND		0x0001
#define PTP_USB_CONTAINER_DATA			0x0002
#define PTP_USB_CONTAINER_RESPONSE		0x0003
#define PTP_USB_CONTAINER_EVENT			0x0004

#if 0
/**
 * Vendor IDs
 */
#define PTP_VENDOR_EASTMAN_KODAK		0x00000001
#define PTP_VENDOR_SEIKO_EPSON			0x00000002
#define PTP_VENDOR_AGILENT				0x00000003
#define PTP_VENDOR_POLAROID				0x00000004
#define PTP_VENDOR_AGFA_GEVAERT			0x00000005
#define PTP_VENDOR_MICROSOFT			0x00000006
#define PTP_VENDOR_EQUINOX				0x00000007
#define PTP_VENDOR_VIEWQUEST			0x00000008
#define PTP_VENDOR_STMICROELECTRONICS	0x00000009
#define PTP_VENDOR_NIKON				0x0000000A
#define PTP_VENDOR_CANON				0x0000000B
#endif

/** PTP Operation Codes */
#define PTP_OC_Undefined					0x1000
#define PTP_OC_GetDeviceInfo				0x1001
#define PTP_OC_OpenSession					0x1002
#define PTP_OC_CloseSession					0x1003
#define PTP_OC_GetStorageIDs				0x1004
#define PTP_OC_GetStorageInfo				0x1005
#define PTP_OC_GetNumObjects				0x1006
#define PTP_OC_GetObjectHandles				0x1007
#define PTP_OC_GetObjectInfo				0x1008
#define PTP_OC_GetObject					0x1009
#define PTP_OC_GetThumb						0x100A
#define PTP_OC_DeleteObject					0x100B
#define PTP_OC_SendObjectInfo				0x100C
#define PTP_OC_SendObject					0x100D
#define PTP_OC_InitiateCapture				0x100E
#define PTP_OC_FormatStore					0x100F
#define PTP_OC_ResetDevice					0x1010
#define PTP_OC_SelfTest						0x1011
#define PTP_OC_SetObjectProtection			0x1012
#define PTP_OC_PowerDown					0x1013
#define PTP_OC_GetDevicePropDesc			0x1014
#define PTP_OC_GetDevicePropValue			0x1015
#define PTP_OC_SetDevicePropValue			0x1016
#define PTP_OC_ResetDevicePropValue			0x1017
#define PTP_OC_TerminateOpenCapture			0x1018
#define PTP_OC_MoveObject					0x1019
#define PTP_OC_CopyObject					0x101A
#define PTP_OC_GetPartialObject				0x101B
#define PTP_OC_InitiateOpenCapture			0x101C


//- Samsung Vendor Operation
//------------------------------------------------------------------------------
//Test Automation & TMS#define PTP_OC_SAMSUNG_ESM                  	0x9000

enum EPTP_OC_SPE
{
    PTP_OC_SPE_MIN                      = 0x9000,
	PTP_OC_SAMSUNG_SetRemoteStudioMode  = 0x9001,
	PTP_OC_SAMSUNG_CheckEvent           = 0x9002,
	PTP_OC_SAMSUNG_GetEvent             = 0x9003,
	PTP_OC_SAMSUNG_Capture              = 0x9004,    
	PTP_OC_SAMSUNG_GetObjectInMemory    = 0x9005,
	PTP_OC_SAMSUNG_GetLiveviewInfo      = 0x9006,
	PTP_OC_SAMSUNG_GetLiveviewImg       = 0x9007,
//	PTP_OC_SAMSUNG_SetFocusPosition     = 0x9008,
//	PTP_OC_SAMSUNG_GetFocusPosition     = 0x9009,
	// Added [06/28/11] jeansu.kim
	PTP_OC_SAMSUNG_Reset				= 0x900A,
	PTP_OC_SAMSUNG_Format				= 0x900B,
	PTP_OC_SAMSUNG_GetObjectInSDCard	= 0x900C,
	PTP_OC_SAMSUNG_GetTick				= 0x900D,
	PTP_OC_SAMSUNG_SetTick				= 0x900E,
	PTP_OC_SAMSUNG_HiddenCommand		= 0x900F,
	PTP_OC_SAMSUNG_GetRecordStatus		= 0x9010,
	PTP_OC_SAMSUNG_FileTransfer			= 0x9011,
	PTP_OC_SAMSUNG_SetEnLarge			= 0x9012,
	PTP_OC_SAMSUNG_MovieTransfer		= 0x9013,
	PTP_OC_SAMSUNG_SetRecordPause		= 0x9014,
	PTP_OC_SAMSUNG_SetRecordResume		= 0x9015,
	PTP_OC_SAMSUNG_SetLiveView			= 0x9017,
	PTP_OC_SAMSUNG_ImageTransfer		= 0x9018,
	PTP_OC_SAMSUNG_FileReceiveComplete	= 0x9019,
	//-- Set/Get
	PTP_OC_SAMSUNG_SETDEVICEPROPVALUE	= /*0x901A*/0x1016,
	PTP_OC_SAMSUNG_GETDEVICEPROPDESC	= /*0x901B*/0x1014,
	PTP_OC_SAMSUNG_LiveReceiveComplete	= 0x9020,
	PTP_OC_SAMSUNG_SensorCleaning		= 0x9021,
	//CMiLRe 20141113 IntervalCapture Stop 추가
	PTP_OC_SAMSUNG_IntervalCaptureStop		= 0x9022,
	//CMiLRe 20141127 Display Save Mode 에서 Wake up
	PTP_OC_SAMSUNG_DISPLAY_SAVE_MODE_WAKE_UP		= 0x9023,
	PTP_OC_SAMSUNG_MovieTransferCancle	= 0x9024,
	PTP_OC_SAMSUNG_GetCaptureCount		= 0x9025,
	PTP_OC_SAMSUNG_ControlTouchAf		= 0x9026,
	PTP_OC_SAMSUNG_GetImagePath			= 0x9027,
	PTP_OC_SAMSUNG_TrackingAFStop		= 0x9028,
	PTP_OC_CUSTOM_ExportLog				= 0x902D,
	//- Add here

	PTP_OC_CUSTOM_HalfShutter			= 0x9030,

	//CMiLRe 2015129 줌 컨트롤
	PTP_OO_SAMSUNG_ZOOM_CTRL	 = 0xD967,
	//NX3000
	PTP_OC_SAMSUNG_FW_DOWNLOAD				= 0x90FB,
	PTP_OC_SAMSUNG_POWER_REBOOT		= 0x90FC,
	PTP_OC_SAMSUNG_POWER_OFF		= 0x90FD,
	PTP_OC_SAMSUNG_FWUpdate				= 0x90FE,
    PTP_OC_SAMSUNG_SelfTest             = 0x90FF,	
	//- Adjustment
	PTP_OC_SAMSUNG_AdjustShutter	   	= 0x9102,
	PTP_OC_SAMSUNG_AdjustScript		    = 0x9103,
	// [4/5/2011] Added chlee
	PTP_OC_SAMSUNG_BCR_Capture          = 0x921D,	
	
	PTP_OC_SPE_MAX,
};

//- Panasonic Vendor Operation
//------------------------------------------------------------------------------

enum EPTP_OC_PPE
{
	PTP_OC_CUSTOM_Capture	= 0x902a,
	PTP_OC_CUSTOM_Record    = 0x902b,
	PTP_OC_CUSTOM_RecordStop = 0x902c,
};


enum EIndexFunc
{   
    eIndexFunc_ESM = PTP_NUM_STANDARD_OPERATION_CODE,
    eIndexFunc_ExtensionAPI,
    eIndexFunc_AdjustShutter,
    eIndexFunc_AdjustScript,
};

#define PTP_TD_NONE  0x00000000
#define PTP_TD_SET   0x00000001
#define PTP_TD_GET   0x00000002
#define PTP_TD_MASK  0x0000000F
//------------------------------------------------------------------------------

/** PTP Response Codes */
#define PTP_RC_Undefined					0x2000
#define PTP_RC_OK							0x2001
#define PTP_RC_GeneralError					0x2002
#define PTP_RC_SessionNotOpen				0x2003
#define PTP_RC_InvalidTransactionID			0x2004
#define PTP_RC_OperationNotSupported		0x2005
#define PTP_RC_ParameterNotSupported		0x2006
#define PTP_RC_IncompleteTransfer			0x2007
#define PTP_RC_InvalidStorageId				0x2008
#define PTP_RC_InvalidObjectHandle			0x2009
#define PTP_RC_DevicePropNotSupported		0x200A
#define PTP_RC_InvalidObjectFormatCode		0x200B
#define PTP_RC_StoreFull					0x200C
#define PTP_RC_ObjectWriteProtected			0x200D
#define PTP_RC_StoreReadOnly				0x200E
#define PTP_RC_AccessDenied					0x200F
#define PTP_RC_NoThumbnailPresent			0x2010
#define PTP_RC_SelfTestFailed				0x2011
#define PTP_RC_PartialDeletion 				0x2012
#define PTP_RC_StoreNotAvailable			0x2013
#define PTP_RC_SpecByFormatUnsupported		0x2014
#define PTP_RC_NoValidObjectInfo			0x2015
#define PTP_RC_InvalidCodeFormat			0x2016
#define PTP_RC_UnknownVendorCode			0x2017
#define PTP_RC_CaptureAlreadyTerminated		0x2018
#define PTP_RC_DeviceBusy					0x2019
#define PTP_RC_InvalidParentObject			0x201A
#define PTP_RC_InvalidDevicePropFormat 		0x201B
#define PTP_RC_InvalidDevicePropValue		0x201C
#define PTP_RC_InvalidParameter				0x201D
#define PTP_RC_SessionAlreadyOpened			0x201E
#define PTP_RC_TransactionCanceled			0x201F
#define PTP_RC_SpecOfDestUnsupported		0x2020


/** PTP Event Codes */
#define PTP_EC_Undefined					0x4000
#define PTP_EC_CancelTransaction			0x4001
#define PTP_EC_ObjectAdded					0x4002
#define PTP_EC_ObjectRemoved				0x4003
#define PTP_EC_StoreAdded					0x4004
#define PTP_EC_StoreRemoved					0x4005
#define PTP_EC_DevicePropChanged			0x4006
#define PTP_EC_ObjectInfoChanged			0x4007
#define PTP_EC_DeviceInfoChanged			0x4008
#define PTP_EC_RequestObjectTransfer		0x4009
#define PTP_EC_StoreFull					0x400A
#define PTP_EC_DeviceReset					0x400B
#define PTP_EC_StorageInfoChanged			0x400C
#define PTP_EC_CaptureComplete				0x400D
#define PTP_EC_UnreportedStatus				0x400E

//- Samsung PTP Extension Event Code
#define PTP_EC_SAMSUNG_Undefined                0xC000
#define PTP_EC_SAMSUNG_RequestObjectTransfer    0xC001
#define PTP_EC_SAMSUNG_DevicePropChanged		0xC002
#define PTP_EC_SAMSUNG_AFDetect         		0xC003
#define PTP_EC_SAMSUNG_EventBufferFull			0xC004
#define PTP_EC_SAMSUNG_KeyEvent					0xC005
#define PTP_EC_SAMSUNG_MovieTransfer			0xC006
#define PTP_EC_SAMSUNG_FileTransfer				0xC007
#define PTP_EC_SAMSUNG_AddFrame					0xC008
#define PTP_EC_SAMSUNG_StartTime				0xC009
#define PTP_EC_SAMSUNG_SkipFrame				0xC010
#define PTP_EC_SAMSUNG_ErrorFrame				0xC011
#define PTP_EC_SAMSUNG_ErrorMsg					0xC012
#define PTP_EC_SAMSUNG_IntervalKeyStop			0xC013
#define PTP_EC_SAMSUNG_EVF_LCD_Display			0xC014
#define PTP_EC_SAMSUNG_IntervalCapture			0xC015
#define PTP_EC_SAMSUNG_IntervalCaptureValue		0xC016
#define PTP_EC_SAMSUNG_ImagePath				0xC017
#define PTP_EC_SAMSUNG_FDBoxInfo1			  	0xC018
#define PTP_EC_SAMSUNG_FDBoxInfo2			  	0xC019
#define PTP_EC_SAMSUNG_TrackingBoxInfo1		  	0xC020
#define PTP_EC_SAMSUNG_TrackingBoxInfo2		  	0xC021
#define PTP_EC_SAMSUNG_RecodingStop			  	0xC022


/** PTP DevicePropertyCode definition */
// [Added 5/9/11] jeansu.kim@samsung.com
#define PTP_DPC_UNDEFINED					0x5000
#define PTP_DPC_BATTERY_LEVEL				0x5001
#define PTP_DPC_FUNCTIONAL_MODE				0x5002
#define PTP_DPC_IMAGE_SIZE					0x5003
#define PTP_DPC_COMPRESSION_SETTING			0x5004
#define PTP_DPC_WHITE_BALANCE				0x5005
#define PTP_DPC_RGB_GAIN					0x5006
#define PTP_DPC_F_NUMBER					0x5007
#define PTP_DPC_FOCAOL_LENGTH				0x5008
#define PTP_DPC_FOCUS_DISTANCE				0x5009
#define PTP_DPC_FOCUS_MODE					0x500A
#define PTP_DPC_EXPOSURE_METERING_MODE		0x500B
#define PTP_DPC_FLASH_MODE					0x500C
#define PTP_DPC_EXPOSURE_TIME 				0x500D
#define PTP_DPC_EXPOSURE_PROGRAM_MODE 		0x500E
#define PTP_DPC_EXPOSURE_INDEX				0x500F
#define PTP_DPC_EXPOSURE_BIAS_COMPENSATIO	0x5010
#define PTP_DPC_DATETIME 					0x5011
#define PTP_DPC_CAPTURE_DELAY				0x5012
#define PTP_DPC_STILL_CAPTURE_MODE			0x5013
#define PTP_DPC_CONTRAST					0x5014
#define PTP_DPC_SHARPNESS					0x5015
#define PTP_DPC_DIGITAL_ROOM				0x5016
#define PTP_DPC_EFFECT_MODE 				0x5017
#define PTP_DPC_BURST_NUMBER 				0x5018
#define PTP_DPC_BURST_INTERVAL 				0x5019
#define PTP_DPC_TIMELAPSE_NUMBER 			0x501A
#define PTP_DPC_TIMELAPSE_INTERVAL 			0x501B
#define PTP_DPC_FOCUS_METERING_MODE 		0x501C
#define PTP_DPC_UPLOAD_URL 					0x501D
#define PTP_DPC_ARTIST 						0x501E
#define PTP_DPC_COPYRIGHT_INFO 				0x501F
#define PTP_DPC_SYNCHRONIZATION_PARTNER 	0xD401
#define PTP_DPC_DEVICE_FRIENDLY_NAME 		0xD402
#define PTP_DPC_VOLUME 						0xD403
#define PTP_DPC_SUPPORTEDFORMATSORDERED 	0xD404
#define PTP_DPC_DEVICE_ICON 				0xD405
#define PTP_DPC_SESSION_INITIATOR_VER_INFO 	0xD406
#define PTP_DPC_PERCEIVED_DEVICE_TYPE 		0xD407
#define PTP_DPC_PLAYBACK_RATE 				0xD410
#define PTP_DPC_PLAYBACK_OBJECT 			0xD411
#define PTP_DPC_PLAYBACK_CONTAINER 			0xD412

// Added [5/19/11] jeansu.kim for test
#define PTP_DPC_SAMSUNG_UNDEFINED			0xD800
#define PTP_DPC_SAMSUNG_EV					0xD801
#define PTP_DPC_SAMSUNG_ISO_STEP			0xD802
#define PTP_DPC_SAMSUNG_DATETIME_TYPE		0xD803
#define PTP_DPC_SAMSUNG_DATETIME_HOUR		0xD804
#define PTP_DPC_SAMSUNG_DATETIME_TIMEZONE	0xD805
#define PTP_DPC_SAMSUNG_DATETIME_DST		0xD806
#define PTP_DPC_SAMSUNG_DATETIME_IMPRINT	0xD807
#define PTP_DPC_SAMSUNG_ISO_EXPANSION       0xD808
#define PTP_DPC_SAMSUNG_AUTO_ISO_RANGE      0xD809
#define PTP_DPC_SAMSUNG_AF_PRIORITY			0xD80A
#define PTP_DPC_SAMSUNG_MF_ASSIST			0xD80B
#define PTP_DPC_SAMSUNG_LINK_AE_TO_AF		0xD80C
#define PTP_DPC_SAMSUNG_OIS					0xD80D
#define PTP_DPC_SAMSUNG_SMART_FILTER		0xD80E
#define PTP_DPC_SAMSUNG_PICTURE_WIZARD		0xD80F
#define PTP_DPC_SAMSUNG_AE_BRK_ORDER		0xD810
#define PTP_DPC_SAMSUNG_AE_BRK_AREA			0xD811
#define PTP_DPC_SAMSUNG_WB_BRK				0xD812
#define PTP_DPC_SAMSUNG_PW_BRK				0xD813
#define PTP_DPC_SAMSUNG_FLASH_EV			0xD814
#define PTP_DPC_SAMSUNG_SHUTTER_SPEED		0xD815
#define PTP_DPC_SAMSUNG_SMART_RANGE			0xD816
#define PTP_DPC_SAMSUNG_COLOR_SPACE			0xD817
#define PTP_DPC_SAMSUNG_AEL_AFL				0xD818
#define PTP_DPC_SAMSUNG_FILE_NAME			0xD819
#define PTP_DPC_SAMSUNG_FILE_NUMBER			0xD81A
#define PTP_DPC_SAMSUNG_FOLDER_TYPE			0xD81B
#define PTP_DPC_SAMSUNG_UNIQUE_ID           0xD81C
#define PTP_DPC_SAMSUNG_SYSTEM_VOLUME       0xD81D
#define PTP_DPC_SAMSUNG_AF_SOUND            0xD81E
#define PTP_DPC_SAMSUNG_BUTTON_SOUND        0xD81F
#define PTP_DPC_SAMSUNG_LDC                 0xD820
#define PTP_DPC_SAMSUNG_AF_LAMP             0xD821
#define	PTP_DPC_SAMSUNG_WB_DETAIL_AUTO		0xD822
#define	PTP_DPC_SAMSUNG_WB_DETAIL_DAYLIGHT	0xD823
#define	PTP_DPC_SAMSUNG_WB_DETAIL_CLOUDY	0xD824
#define	PTP_DPC_SAMSUNG_WB_DETAIL_FLURESCENT_W	0xD825
#define	PTP_DPC_SAMSUNG_WB_DETAIL_FLURESCENT_NW	0xD826
#define	PTP_DPC_SAMSUNG_WB_DETAIL_FLURESCENT_D	0xD827
#define	PTP_DPC_SAMSUNG_WB_DETAIL_TUNGSTEN	0xD828
#define	PTP_DPC_SAMSUNG_WB_DETAIL_FLASH		0xD829
#define PTP_DPC_SAMSUNG_WB_DETAIL_KELVIN	0xD82A
#define PTP_DPC_SAMSUNG_FW_VERSION							0xD991
//CMiLRe 2015129 줌 컨트롤
#define PTP_DPC_SAMSUNG_ZOOM_CTRL_GET	0xD967
#define PTP_DPC_SAMSUNG_ZOOM_CTRL_SET	0xD968
#define PTP_DPC_SAMSUNG_WHITE_BALANCE_DETAIL_COUSTOM_RGB	0xD969

/** PTP DeviceProperty FormFlag definition */
// [Added 5/30/11] jeansu.kim@samsung.com
#define PTP_DPF_NONE	0X0
#define PTP_DPF_RANGE	0x1
#define PTP_DPF_ENUM	0x2

/** PTP DeviceProperty GetSet definition */
// [Added 5/30/11] jeansu.kim@samsung.com
#define PTP_DP_GET		0x0
#define PTP_DP_GETSET	0x1

/** PTP DevicePropertyValue type definition */
// [Added 5/9/11] jeansu.kim@samsung.com
#define PTP_DPV_UNDEF		0x0000
#define PTP_DPV_INT8		0x0001
#define PTP_DPV_UINT8		0x0002
#define	PTP_DPV_INT16		0x0003
#define PTP_DPV_UINT16		0x0004
#define PTP_DPV_INT32		0x0005
#define PTP_DPV_UINT32		0x0006
#define PTP_DPV_INT64		0x0007
#define PTP_DPV_UINT64		0x0008
#define PTP_DPV_INT128		0x0009
#define PTP_DPV_UINT128		0x000A
#define PTP_DPV_AINT8		0x4001
#define	PTP_DPV_AUINT8		0x4002
#define PTP_DPV_AINT16		0x4003
#define PTP_DPV_AUINT16		0x4004
#define PTP_DPV_AINT32		0x4005
#define PTP_DPV_AUINT32		0x4006
#define PTP_DPV_AINT64		0x4007
#define PTP_DPV_AUINT64		0x4008
#define PTP_DPV_AINT128		0x4009
#define PTP_DPV_AUINT128	0x400A
#define PTP_DPV_STR			0xFFFF
#define PTP_DPV_STR_EX		0x0000FFFF

#define PTP_DPV_ARRAY_MASK	0x4000

/** DATATYPE, PTPPropertyValue, PTPPropDescRangeForm, PTPPropDescEnumForm for DevPropDesc */
// [Added 5/9/11] jeansu.kim@samsung.com
typedef union _PTPDevicePropValue {
	char *str;			// for STR
	SdiUInt8 pInt[16];		// for INT8, INT16, INT32, INT64, INT128, also Unsigned.
	struct _array {
		SdiUInt32 count;
		union _PTPDevicePropValue *value;
	} array;
}PTPDevicePropValue;

typedef struct _PTPRangeForm {
	PTPDevicePropValue Min;
	PTPDevicePropValue Max;
	PTPDevicePropValue Step;
}PTPRangeForm;

typedef struct _PTPEnumForm {
	SdiUInt16 NumberOfValues;
	PTPDevicePropValue *SupportedValue;
}PTPEnumForm;

/** PTP DevicePropertyDescription structure */
// [Added 5/9/11] jeansu.kim@samsung.com
typedef struct _PTPDevicePropDesc{
    SdiUInt16	PropCode;
    SdiUInt16	DataType;
    SdiUInt8		GetSet;
    // 0x00 Get, 0x01 Get/Set
    
    PTPDevicePropValue Default;
    PTPDevicePropValue Current;
    	
    SdiUInt8		FormFlag;
    // 0x00 None, 0x01 RangeForm, 0x02 EnumerationForm
    	
    PTPRangeForm RangeForm;
    PTPEnumForm EnumForm;	// currently it's array. need to change to list?
} PTPDevicePropDesc;


/** PTP Object Format Codes */
/* ancillary formats */
#define PTP_OFC_Undefined					0x3000
#define PTP_OFC_Association					0x3001
#define PTP_OFC_Script						0x3002
#define PTP_OFC_Executable					0x3003
#define PTP_OFC_Text						0x3004
#define PTP_OFC_HTML						0x3005
#define PTP_OFC_DPOF						0x3006
#define PTP_OFC_AIFF	 					0x3007
#define PTP_OFC_WAV							0x3008
#define PTP_OFC_MP3							0x3009
#define PTP_OFC_AVI							0x300A
#define PTP_OFC_MPEG						0x300B
#define PTP_OFC_ASF							0x300C
/* ? */
#define PTP_OFC_QT							0x300D
/* image formats */
#define PTP_OFC_EXIF_JPEG					0x3801
#define PTP_OFC_TIFF_EP						0x3802
#define PTP_OFC_FlashPix					0x3803
#define PTP_OFC_BMP							0x3804
#define PTP_OFC_CIFF						0x3805
#define PTP_OFC_Undefined_0x3806			0x3806
#define PTP_OFC_GIF							0x3807
#define PTP_OFC_JFIF						0x3808
#define PTP_OFC_PCD							0x3809
#define PTP_OFC_PICT						0x380A
#define PTP_OFC_PNG							0x380B
#define PTP_OFC_Undefined_0x380C			0x380C
#define PTP_OFC_TIFF						0x380D
#define PTP_OFC_TIFF_IT						0x380E
#define PTP_OFC_JP2							0x380F
#define PTP_OFC_JPX							0x3810


/** PTP Association Types */
#define PTP_AT_Undefined					0x0000
#define PTP_AT_GenericFolder				0x0001
#define PTP_AT_Album						0x0002
#define PTP_AT_TimeSequence					0x0003
#define PTP_AT_HorizontalPanoramic			0x0004
#define PTP_AT_VerticalPanoramic			0x0005
#define PTP_AT_2DPanoramic					0x0006
#define PTP_AT_AncillaryData				0x0007

/** PTP Protection Status */
#define PTP_PS_NoProtection					0x0000
#define PTP_PS_ReadOnly						0x0001

/** PTP Storage Types */
#define PTP_ST_Undefined					0x0000
#define PTP_ST_FixedROM						0x0001
#define PTP_ST_RemovableROM					0x0002
#define PTP_ST_FixedRAM						0x0003
#define PTP_ST_RemovableRAM					0x0004

/** PTP File System Types */
#define PTP_FST_Undefined					0x0000
#define PTP_FST_GenericFlat					0x0001	
#define PTP_FST_GenericHierarchical			0x0002
#define PTP_FST_DCF							0x0003

/** PTP StorageInfo Access Capability Values */
#define PTP_AC_ReadWrite					0x0000
#define PTP_AC_ReadOnly						0x0001
#define PTP_AC_ReadOnly_WithObjectDeletion	0x0002


/** PTP Trnasport Types */
#define PTP_TRANS_TYPE_USB							0x0001

//- Adjustment for Transaction direction
//------------------------------------------------------------------------------
#define ADT_NX_NODATA				0x0000
#define ADT_NX_SENDDATA				0x1000
#define ADT_NX_GETDATA				0x2000
#define ADT_NX_DATA_MASK			0xF000
//------------------------------------------------------------------------------

//- PTP Transaction direction
//------------------------------------------------------------------------------
#define PTP_TD_NONE  0x00000000
#define PTP_TD_SET   0x00000001
#define PTP_TD_GET   0x00000002
#define PTP_TD_MASK  0x0000000F
//------------------------------------------------------------------------------


/* SAMSUNG NX Adjustment Message ID */
#define ADM_COMMON					0x1000
#define ADM_SHUTTER_SPEED			0x1001
#define ADM_CIS_DATA_WRITE			0x1002
#define ADM_RTC_CHECK				0x1003

/* SAMSUNG NX Adjustment Event ID */
#define ADE_GET_CAMERA_CMD			0x0000
#define ADE_SET_ADJ_READY			0x0001
#define ADE_SET_ADJ_FINISHED		0x0002
#define ADE_SET_ADJ_CONTINUE		0x0003

#define ADE_SET_CAPTURE_READY		0x0000
#define ADE_SET_MEASURE_RESULT		0x0001
#define ADE_SEND_SHUTTER_DATA		0x1000			// send data MASK 0x1000

#define ADE_SEND_CIS_DOT_DATA		0x1000	
#define ADE_SET_DOT_DATA_RESULT		0x0000	

#define ADE_SET_CMD_IDLE			0x0000
#define ADE_SET_COMM_CHK			0x0001
#define ADE_SET_ADJ_EVENT			0x0002

#define ADE_SET_ADJ_PARAM			0x0000
#define ADE_SET_CAPTURE_START		0x0001
#define ADE_REQ_MEASURE_DATA		0x0002
#define ADE_RESULT_DATA				0x0003


#define ADE_REQ_CIS_DOT_DATA		0x0000
#define ADE_REQ_PC_TIME_DATA		0x0000
#define ADE_REQ_DOT_DATA_READY      0x0001


#define ADE_SET_PC_TIME_DATA		0x0000

/* SAMSUNG NX Adjustment Parameter ID */
#define ADP_NONE					0x0000
#define ADP_OK						0x0000
#define ADP_NG						0x0001

#define ADP_DATA_POSITION_A			0x0000
#define ADP_DATA_POSITION_B			0x0001
#define ADP_DATA_POSITION_C			0x0002

#define ADP_DATA_TYPE_UCHAR			0x0000
#define ADP_DATA_TYPE_UINT16		0x0001
#define ADP_DATA_TYPE_UINT32		0x0002

#define ADP_SET_CMD_RCV_IDLE		0x0000
#define ADP_SET_CMD_RCV_OK			0x0001
#define ADP_SET_CMD_RCV_NG			0x0002
#define ADP_REQ_CMD_AGAIN			0x0003

#define ADP_ADJ_RESULT				0x0000
#define ADP_ADJ_PROGRESS			0x0001

#define ADP_START					0x0000
#define ADP_ING						0x0001
#define ADP_END						0x0002
#define ADP_CONTINUE				0x0003

#define ADP_SHUTTER_SPD_1			0x0000
#define ADP_SHUTTER_SPD_2			0x0001

#define ADP_ADJ_MEASURE_NUM			0x0000

#define  ADJ_SHUTTER_SPEED			0x1001

#endif /* __PTP_BASE_H__ */

