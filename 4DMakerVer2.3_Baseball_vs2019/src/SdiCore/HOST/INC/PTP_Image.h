/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_Image.h
 \brief Define PTP Image Class handling image file information.
 \date 2009-06-11
 \version 0.70 
*/

#ifndef __PTP_IMAGE_H__
#define __PTP_IMAGE_H__


#include "PTP_Common.h"
#include "PTP_EnvironmentConfig.h"

/** Tag No. of Thumbnail Pointer */
#define JPEG_TAG_NO_INTERCHANGE_FORMAT1(en)		((en == PTP_LITTLE_ENDIAN) ? 0x01 : 0x02)
#define JPEG_TAG_NO_INTERCHANGE_FORMAT2(en)		((en == PTP_LITTLE_ENDIAN) ? 0x02 : 0x01)

/** Tag No. of Thumbnail Size */
#define JPEG_TAG_NO_INTERCHANGE_FORMAT_LEN1(en)	((en == PTP_LITTLE_ENDIAN) ? 0x02 : 0x02)
#define JPEG_TAG_NO_INTERCHANGE_FORMAT_LEN2(en)	((en == PTP_LITTLE_ENDIAN) ? 0x02 : 0x02)

#if 0
/** Tag No. of Date and Time */
#define JPEG_TAG_NO_DATE_TIME1(en)				((en == PTP_LITTLE_ENDIAN) ? 0x32 : 0x01)
#define JPEG_TAG_NO_DATE_TIME2(en)				((en == PTP_LITTLE_ENDIAN) ? 0x01 : 0x32)
#endif

/** Tag No. of Exif IFD Pointer */
#define JPEG_TAG_NO_EXIF_IFD_POINTER1(en)		((en == PTP_LITTLE_ENDIAN) ? 0x69 : 0x87)
#define JPEG_TAG_NO_EXIF_IFD_POINTER2(en)		((en == PTP_LITTLE_ENDIAN) ? 0x87 : 0x69)

/** Tag No. of Pixel X Dimension */
#define JPEG_TAG_NO_PIXEL_X_DIMENSION1(en)		((en == PTP_LITTLE_ENDIAN) ? 0x02 : 0xa0)
#define TAG_NO_PIXEL_X_DIMENSION2(en)			((en == PTP_LITTLE_ENDIAN) ? 0xa0 : 0x02)

/** Tag No. of Pixel Y Dimension */
#define JPEG_TAG_NO_PIXEL_Y_DIMENSION1(en)		((en == PTP_LITTLE_ENDIAN) ? 0x03 : 0xa0)
#define JPEG_TAG_NO_PIXEL_Y_DIMENSION2(en)		((en == PTP_LITTLE_ENDIAN) ? 0xa0 : 0x03)

/** Offset for Start Of Image */
#define JPEG_OFFSET_SOI_SIZE						2

/** Offset for TIFF Header in Exif */
#define JPEG_OFFSET_APP1_MARKERS_SIZE				10

/** Offset for APP1 Header in Exif */
#define JPEG_OFFSET_APP1_TIFF_HEADER_SIZE			8

/** Size of each Interoperability Array */
#define EXIF_INTEROP_ARRAY_SIZE						12




class PTPImage
{
private:	
	int mDevEndian;
	int mExifEndian;
	
	/* Use to handle file data (ex. jpeg...) */
	char mFileBuffer[PTP_FILE_BUFFER_LEN];

public:
	PTPImage();
	~PTPImage();

	void Init(PTPEnvironmentConfig* env);
	void term(void);
	
	void getJpegInfo(char* path, SdiUInt32 filesize, PTPObjectInfoType* oi, PTPObjectType* obj);

private:
	SdiBool isExif(SdiUInt8* ptr);
	int getByteOrder(SdiUInt8* ptr);
	SdiUInt16 extod16a(SdiUInt8* a);
	SdiUInt32 extod32a(SdiUInt8* a);
};

#endif  /* __PTP_IMAGE_H__ */

