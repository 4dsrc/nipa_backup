/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_HostOperationHandler.h
 \brief Define PTP Host OperationHandler Class.
 \date 2009-06-11
 \version 0.70 
*/

#ifndef __PTPHOST_OPERATION_HANDLER_H__
#define __PTPHOST_OPERATION_HANDLER_H__

#include "PTP_Common.h"

#include "PTP_EnvironmentConfig.h"
#include "PTP_Endian.h"
//#include "PTP_Image.h"
//#include "PTP_Device.h"
#include "PTP_Storage.h"
#include "PTP_Transport.h"

class PTPHOSTOperationHandler;

typedef struct _ptp_thumb_data
{
	SdiUInt32	handle;
	//prevent
	//char ThumbData[1024*15];
	char ThumbData[1024*16];
	
} PTPThumbData;


typedef struct _ptp_thumb_data_list
{
	SdiUInt32	numthumbs;
	PTPThumbData*	thumbs;	
} PTPThumbDataList;

class PTPHOSTOperationHandler
{
private:
	SdiUInt32 ulTransactionID;
	SdiUInt32 ulSessionID;


public:
	PTPHOSTOperationHandler();
	~PTPHOSTOperationHandler();
	
	int sendSQEorder(PTPInfoType* ptpInfo, PTPTransport* trans);
	int getSETpayload(PTPInfoType* ptpInfo, PTPTransport* trans);
	int getSETresponse(PTPInfoType* ptpInfo, PTPTransport* trans);



	int getDeviceInfo(PTPInfoType* ptpInfo, PTPTransport* trans);
	int openSession(PTPInfoType* ptpInfo, PTPTransport* trans);
	int closeSession(PTPInfoType* ptpInfo, PTPTransport* trans);
	int getStorageIDs(PTPInfoType* ptpInfo, PTPStorage* sESMAndObj, PTPTransport* trans,   PTPEnvironmentConfig* mpEnvCfg );
	int getStorageInfo(PTPInfoType* ptpInfo, PTPStorage* sESMAndObj, PTPTransport* trans);
	int getObjectHandles(PTPInfoType* ptpInfo, PTPStorage* sESMAndObj, PTPTransport* trans);
	int getObjectInfo(PTPInfoType* ptpInfo, PTPStorage* sESMAndObj, PTPTransport* trans);
	int getObject(PTPInfoType* ptpInfo, PTPTransport* trans, SdiUInt32	handle, char * path);
	int getThumb(PTPInfoType* ptpInfo, PTPTransport* trans, SdiUInt32	Handle, PTPThumbData* mthumbdata);
private:
	void printObjectHandles(SdiUInt32* handles, SdiUInt32 len);
	SdiUInt32 nextTransactionID();

public:    
    int SdiopenSession(PTPInfoType* ptpInfo, PTPTransport* trans);
	int StartRemoteStudio(PTPInfoType* ptpInfo, PTPTransport* trans);

    int SdicloseSession(PTPInfoType* ptpInfo, PTPTransport* trans);

    int SdisendCapture(PTPInfoType* ptpInfo, PTPTransport* trans,
                            SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size);

	int	SdiGetDevUniqueID(PTPInfoType* ptpInfo, PTPTransport* trans,
                            SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size);

	int	SdiGetDevModeIndex(PTPInfoType* ptpInfo, PTPTransport* trans,
                            SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size);

	int	SdiSendAdjLiveviewInfo(PTPInfoType* ptpInfo, PTPTransport* trans,
                            SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size);

	int SdiSendLiveview(PTPInfoType* ptpInfo, PTPTransport* trans,									// chlee 2011-06-22
                           SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32* size);

	int	SdiGetDevPropDesc(PTPInfoType* ptpInfo, PTPTransport* trans,
                            SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size);

	int SdiSetDevPropValue(PTPInfoType* ptpInfo, PTPTransport* trans,								
                           SdiUInt32* adjReq, SdiUInt32* adjRes,int nType, SdiUInt nValue);

	int SdiSetDevPropValue(PTPInfoType* ptpInfo, PTPTransport* trans,								
                           SdiUInt32* adjReq, SdiUInt32* adjRes, CString strValue);
	
	int SetFocusPosition(PTPInfoType* ptpInfo, PTPTransport* trans,								
                           SdiUInt32* adjReq, SdiUInt32* adjRes);

	int SdiSetFocusPosition(PTPInfoType* ptpInfo, PTPTransport* trans,								
                           SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size);

	int	SdiSendCheckEvent(PTPInfoType* ptpInfo, PTPTransport* trans,
                            SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size);

	int	SdiGetEvent(PTPInfoType* ptpInfo, PTPTransport* trans,
                            SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size);

	int SdiRequireCaptureExec(PTPInfoType* ptpInfo, PTPTransport* trans,
                           SdiUInt32* adjReq, SdiUInt32* adjRes);

	int SdiCaptureCompleteExec(PTPInfoType* ptpInfo, PTPTransport* trans,
                            SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size);

	int SdiMovieCompleteExec(PTPInfoType* ptpInfo, PTPTransport* trans,
                            SdiUInt32* adjReq, SdiUInt32* adjRes, SdiUInt8 **data, SdiUInt32& size);
	//--2011-07-28 jeansu
	int SdiReset(PTPInfoType* ptpInfo, PTPTransport* trans);
	int SdiFormat(PTPInfoType* ptpInfo, PTPTransport* trans);
	//dh0.seo 2014-11-04
	int SdiSensorCleaning(PTPInfoType* ptpInfo, PTPTransport* trans);
	//CMiLRe 20141113 IntervalCapture Stop �߰�
	int SdiIntervalCaptureStop(PTPInfoType* ptpInfo, PTPTransport* trans);
	//CMiLRe 20141127 Display Save Mode ���� Wake up
	int SdiDisplaySaveModeWakeUp(PTPInfoType* ptpInfo, PTPTransport* trans);	
	
};

#endif /* __PTPHOST_OPERATION_HANDLER_H__ */

