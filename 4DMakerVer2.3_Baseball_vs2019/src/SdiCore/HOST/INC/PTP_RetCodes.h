/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_RetCodes.h
 \brief Define internal return codes used in PTP Component.
 \date 2009-06-11
 \version 0.70 
*/

#ifndef __PTP_RETCODES_H__
#define __PTP_RETCODES_H__

#include "PTP_Errors.h"


#define PTPErrNone							0

/** Internal ErrorR Code */
#define PTPErrCannotCreateInstance			0x02DF
#define PTPErrBadParam						0x02DE

#define PTPErrNotReady						0x02DD
#define PTPErrNotEnoughMem					0x02DC
#define PTPErrTransactionID					0x02DB
#define PTPErrNotSupportOpCode				0x02DA
#define PTPErrNotSessionOpen				0x02D9

#define PTPErrInvalidStorageID				0x02D8
#define PTPErrInvalidHandle 				0x02D7
#define PTPErrInvalidFormat 				0x02D6
#define PTPErrInvalidOpCode					0x02D5

#define PTPErrReadOnly						0x02D4
#define PTPErrPartialDeleted				0x02D3
#define PTPErrStorageFull					0x02D2
#define PTPErrStorageNone					0x02D1

#define PTPErrCannotOpenTransport			0x02D0
#define PTPErrCannoESMetRequest				0x02CF
#define PTPErrCannoESMetResponse			0x02CF
#define PTPErrCannotSendResponse			0x02CE
#define PTPErrCannotSendEvent				0x02CD
#define PTPErrCannotSendData				0x02CC
#define PTPErrCannotSendCommand				0x02BD
#define PTPErrCannotSendDataOnly			0x02CB
#define PTPErrCannoESMetData				0x02CA

#define PTPErrUSBIOOpen						0x02C9
#define PTPErrUSBIORead						0x02C8
#define PTPErrUSBIOWrite					0x02C7

#define PTPErrNotContainerType				0x02C6
#define PTPErrBufferSize					0x02C5

#define PTPErrReadFile						0x02C4
#define PTPErrReadFileNone					0x02C4
#define PTPErrWriteFile						0x02C4
#define PTPErrWriteFileNone					0x02C4
#define PTPErrSeekFile						0x02C4

#define PTPErrFileRead						0x02C3
#define PTPErrFileWrite						0x02C2

#define PTPErrNotMount						0x02C1
#define PTPErrUnknownByteOrder				0x02C0

#define PTPErrCannotRemoveDir				0x02BF
#define PTPErrCannotRemoveFile				0x02BE

#define PTPNoData							0x02BD




#endif  /* __PTP_RETCODES_H__ */


