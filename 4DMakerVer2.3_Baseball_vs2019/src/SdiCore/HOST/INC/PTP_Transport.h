/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_Transport.h
 \brief Define PTP Transport Class.
 \date 2009-06-11
 \version 0.70 
*/

#ifndef __PTP_TRANSPORT_H__
#define __PTP_TRANSPORT_H__

#include "PTP_Common.h"

#include "PTP_EnvironmentConfig.h"
#include "PTP_Endian.h"

#include "PTP_TransportIO.h"
#include "PTP_UsbIO.h"

class PTPTransport
{
public:
	int m_Type;
	char m_Name[PTP_OBJECT_NAME_LEN];

	PTPTransportIO* m_pTransIO;
	SdiUInt32 m_TransContainerHdrLen;
	
public:
	PTPTransport();
	~PTPTransport();
	
	int Init(PTPEnvironmentConfig* env);
	void term(void);	

#ifdef __PTP_HOST__
	int sendCommand(PTPInfoType* ptpInfo,
						 SdiUInt16      commandCode,
						 SdiUInt32      param1,
						 SdiUInt32      param2,
						 SdiUInt32      param3,
						 char          nparam);	
	int getResponse(PTPInfoType* ptpInfo);
#else
	int getRequest(PTPInfoType* ptpInfo);

	int sendResponse(PTPInfoType* ptpInfo,
						SdiUInt16 respCode,
						SdiUInt32 param1,
						SdiUInt32 param2,
						SdiUInt32 param3,
						char nparam);
						
	int sendEvent(PTPInfoType* ptpInfo,
					SdiUInt16 eventCode,
					SdiUInt32 param1,
					SdiUInt32 param2,
					SdiUInt32 param3,
					char nparam);
				
	int sendData(PTPInfoType* ptpInfo, char* data, SdiUInt32 size, SdiUInt32 totalSize);
	int sendDataOnly(char* data, SdiUInt32 size, SdiBool bIsLastData);
#endif
	int getData(PTPInfoType* ptpInfo, char* data, SdiUInt32 size, int opt, void* optParam);
/*
#if 1   // duvallee 07-29-2010 : for LibUSB & Windows7
		int getData_Ex(PTPInfoType* ptpInfo, char* data, SdiUInt32 size, int opt, void* optParam);
#endif
*/

	int getConnectionStatus(void);

private:	
	void printPTPContainer(PTPContainerType* ptpCntr);

    
public:

    int Sditransaction_set(PTPInfoType* ptpInfo, char **pBuff, SdiUInt32 size);
    int Sditransaction_get(PTPInfoType* ptpInfo, char **pBuff, SdiUInt32& size);
    int Sditransaction_none(PTPInfoType* ptpInfo);

    void SetTransTimeout(int timeout);
    int  GetTransTimeout();

};

#endif /* __PTP_TRANSPORT_H__ */

