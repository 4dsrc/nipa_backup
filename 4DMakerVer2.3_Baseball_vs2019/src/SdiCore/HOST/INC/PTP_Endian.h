/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_Endian.h
 \brief Define PTP Endian Class used for endian converting.
 \date 2009-06-11
 \version 0.70 

 In this file, define endian converting function using PTP Component.\n
 Summary of Converting Endian Function :
 
 <b>dtotXX(), dtotXXa()</b>
 
 See \a dtotXX() function, 'XX' is data size.\n
 It convert '<b>XX</b>' bit Hexadecimal data determined by byte order of PTP <b>D</b>evice <b>to</b>
 same bit data determined by byte order of PTP <b>T</b>ransport.\n
 Also, here is \a dtotXXa() function.\n
 It return a converted value with string in array 'a'. '<b>a</b>' means that this function put converted
 value in array 'a'.\n

 <b>ttodXX(), ttodXXa()</b>\n
 
 And, see \a ttodXX() function, '<b>XX</b>' is data size.\n
 It convert reversly, aginst <b>dtotXX()</b> function.\n
*/

#ifndef __PTP_ENDIAN_H__
#define __PTP_ENDIAN_H__

#include "PTP_Common.h"
#include "PTP_EnvironmentConfig.h"

/** Mask for High 8bit value in 16bit value */
#define PTP_MASK_HIGH_16			0xFF00

/** Mask for Low 8bit value in 16bit value */
#define PTP_MASK_LOW_16				0x00FF

/** Mask for High 16bit value in 32bit value */
#define PTP_MASK_HIGH_32			0xFFFF0000UL

/** Mask for Low 16bit value in 32bit value */
#define PTP_MASK_LOW_32				0x0000FFFFUL

/** Mask for High 32bit value in 64bit value */
#define PTP_MASK64_H				0xFFFFFFFF00000000ULL

/** Mask for Low 32bit value in 64bit value */
#define PTP_MASK64_L				0x00000000FFFFFFFFULL


class PTPEndian
{
private:
	static int msDevEndian;
	static int msTransEndian;
	
public:
	PTPEndian();
	~PTPEndian();

	static void Init(PTPEnvironmentConfig *env);
	static void term(void);
	
	static SdiUInt16 changeEndian16(SdiUInt16 val);
	static SdiUInt32 changeEndian32(SdiUInt32 val);
	static SdiUInt64 changeEndian64(SdiUInt64 val);
	
	static SdiUInt16 dtot16(SdiUInt16 val);
	static SdiUInt32 dtot32(SdiUInt32 val);
	static SdiUInt64 dtot64(SdiUInt64 val);
	
	static void dtot8a(SdiUInt8 *a, SdiUInt8 val);
	static void dtot16a(SdiUInt8 *a, SdiUInt16 val);
	static void dtot32a(SdiUInt8 *a, SdiUInt32 val);
	static void dtot64a(SdiUInt8 *a, SdiUInt64 val);

	static SdiUInt16 ttod16(SdiUInt16 val);
	static SdiUInt32 ttod32(SdiUInt32 val);
	static SdiUInt64 ttod64(SdiUInt64 val);

	static SdiUInt8 ttod8a(SdiUInt8 *a);
	static SdiUInt16 ttod16a(SdiUInt8 *a);
	static SdiUInt32 ttod32a(SdiUInt8 *a);
	static SdiUInt64 ttod64a(SdiUInt8 *a);

	static void packString(char *toBuf, char *fromStr, SdiUInt16 offset, SdiUInt8 *len);
	static void packUINT16Array(char *toBuf, SdiUInt16 *fromArray, SdiUInt16 offset, SdiUInt32 len);
	static void packUINT32Array(char *toBuf, SdiUInt32 *fromArray, SdiUInt16 offset, SdiUInt32 len, SdiBool dataOnly);

	static void unpackString(char *toStr, char *fromBuf, SdiUInt16 offset, SdiUInt8 *len);
	static void unpackUINT16Array(char *toBuf, char *fromArray, SdiUInt16 offset, SdiUInt32 len);
	static void unpackUINT32Array(char *toBuf, char *fromArray, SdiUInt16 offset, SdiUInt32 len);
};


#endif /* __PTP_ENDIAN_H__ */

