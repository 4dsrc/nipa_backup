/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_Common.h
 \brief Define common used header files and defines for PTP Component.
 \date 2009-06-11
 \version 0.70 
*/

#pragma once

#define __WIN_SIMUL__

#ifndef __WIN_SIMUL__
#include "OSLibConfig.h"
#include "OSAbstraction.h"
#include "GeneDebug.h"
#else
#include "memory.h"   // for memset
#endif

#include "SdiDefines.h"
#include "PTP_RetCodes.h"
#include "PTP_Base.h"

#define __PTP_HOST__
// [3/8/2011 JanuS] 
#define __SDI_NX__

/**
 * Debug message print function.\n
 * if \c _PTP_NO_DEBUG is not defined, this Macro is same as \a TRACE().\n
 * But, if  \c _PTP_NO_DEBUG is defined, this Macro will be ignore.\n
 *
 * \param x string indicating format
 */

//#define __PTP_NO_DEBUG__

#ifndef __PTP_NO_DEBUG__
#ifdef __WIN_SIMUL__
#define PRINTD0(x)						TRACE((x))
#define PRINTD1(x, a1)					TRACE((x), (a1))
#define PRINTD2(x, a1, a2)				TRACE((x), (a1), (a2))
#define PRINTD3(x, a1, a2, a3)			TRACE((x), (a1), (a2), (a3))
#define PRINTD4(x, a1, a2, a3, a4)		TRACE((x), (a1), (a2), (a3), (a4))
#else
#define PRINTD0(x)						GENEDEBUG_DEBUG0((x))
#define PRINTD1(x, a1)					GENEDEBUG_DEBUG1((x), (a1))
#define PRINTD2(x, a1, a2)				GENEDEBUG_DEBUG2((x), (a1), (a2))
#define PRINTD3(x, a1, a2, a3)			GENEDEBUG_DEBUG3((x), (a1), (a2), (a3))
#define PRINTD4(x, a1, a2, a3, a4)		GENEDEBUG_DEBUG4((x), (a1), (a2), (a3), (a4))
#endif
#else
#define PRINTD0(x)
#define PRINTD1(x, a1)
#define PRINTD2(x, a1, a2)
#define PRINTD3(x, a1, a2, a3)
#define PRINTD4(x, a1, a2, a3, a4)
#endif

#ifndef PTP_VIRTUAL_FILE_SIZE
#define PTP_VIRTUAL_FILE_SIZE		 (1024 * 16) //16K
#endif

/**
 * Initialize structure
 *
 * \param cnt structure to be initialized
 */
#define PTP_STRUCT_INIT(st)	{memset(&st, 0, sizeof(st));}


/** Maxium data & file buffer length */
#define PTP_DATA_BUFFER_LEN			4096//512

#define PTP_FILE_BUFFER_LEN			(PTP_DATA_BUFFER_LEN * 8)

/** Max number of object to be restored in Data_Buf */
#define PTP_NUM_HANDLE_IN_DATA_BUFFER	(PTP_DATA_BUFFER_LEN / sizeof(SdiUInt32))


/** Maxium path length */
#define PTP_MAX_PATH_LEN			255

/** Maxium base directory length */
#define PTP_BASEPATH_LEN			64

/** Maxium object(dir, file...) name length */
#define PTP_OBJECT_NAME_LEN			200         // 64 chlee 

#define PTP_VERSION_LEN				16


/** Byte order Big or Little Endian */
#define PTP_BIG_ENDIAN						0xFF00
#define PTP_LITTLE_ENDIAN					0x00FF

/** Session Status */
#define PTP_SESSION_CLOSED			0x00
#define PTP_SESSION_OPENED			0x01

/** Mode used find object handle by name */
#define PTP_FIND_OBJECT_UNKNOWN		0x0000
#define PTP_FIND_OBJECT_ASSOC		0x0001
#define PTP_FIND_OBJECT_DPOF		0x0002
#define PTP_FIND_OBJECT_JPEG		0x0003

//meteor
#define PTP_IO_ALL					0
#define PTP_IO_RX					1
#define PTP_IO_TX					2

#define PTP_GET_DATA_NONE				0
#define PTP_GET_DATA_VIRTUAL_FILE		1
#define PTP_GET_DATA_OBJECT_INFO		2
#define PTP_GET_DATA_OBJECT				3


/** Object attribute */
#define PTP_OBJ_ATTR_NONE		0x0000
#define PTP_OBJ_ATTR_DIR		0x0001
#define PTP_OBJ_ATTR_FILE		0x0002

/** Object mode */
#define PTP_OBJ_MODE_RDWR		0x0000
#define PTP_OBJ_MODE_NONE		0x0001
#define PTP_OBJ_MODE_RDONLY		0x0002



typedef struct _options_config_type
{
	SdiBool IsOnlyImageFilesSupported;
	SdiBool IsOnlyDCIMFolderSupported;
	SdiBool option3;
	SdiBool option4;
	SdiBool option5;
  
} OptionsConfigType;

typedef struct _device_config_type
{
	char Manufacturer[PTP_BASEPATH_LEN];
	char Model[PTP_OBJECT_NAME_LEN];
	char Version[PTP_VERSION_LEN];
	
	int ByteOrder;
  
} DeviceConfigType;

typedef struct _storage_config_type
{
	char Name[PTP_OBJECT_NAME_LEN];
	char Path[PTP_BASEPATH_LEN];
	
	SdiUInt16 Type;
	SdiUInt16 FilesystemType;
	
	SdiBool bIsActive;
  
} StorageConfigType;

typedef struct _transport_config_type
{
	char Name[PTP_OBJECT_NAME_LEN];
	
	int Type;
	int ByteOrder;
	 
} TransportConfigType;


/** 
 * \brief Date and Time Structure
 *
 * This structure consists of year, month, day and hour, min, sec...used in PTP
 */
typedef struct _ptp_date_time_type
{

	SdiUInt16	Year;		/* 2004 */
	SdiUInt16	Month;		/* 3 */
	SdiUInt16	Day;		/* 19 */

	SdiUInt16	Hour;		/* 18 */
	SdiUInt16	Min;		/* 30 */
	SdiUInt16	Sec;		/* 50 */

} PTPDateTimeType;


/** 
 * \brief Object State Structure
 *
 * This struture consists of file or directory information entry in current file system
 */
typedef struct _ptp_object_stat_type
{

	SdiUInt16		Attr;	/**< File or Folder */
	SdiUInt16		Mode;	/**< Read Only or Read/Write */
	SdiUInt32		Size;	/**< File Size */
	PTPDateTimeType 	Ctime;	/**< Create Time */
	PTPDateTimeType		Mtime;	/**< Last Modify Time */
	PTPDateTimeType		Atime;	/**< Last Access Time */

} PTPObjectStatType;

/** 
 * \brief Object Structure
 * 
 * This structure consists of important features of Object.\n
 * - name, handle, format, ...
 */
typedef struct _ptp_object_type
{
	
	//char		Name[PTP_OBJECT_NAME_LEN];		/**< Object Name */
	char		Name[PTP_MAX_STRING_LEN];		/**< Object Name */	
	SdiUInt32	Handle;						/**< Object Handle */

	SdiUInt32	ParentHandle;				/**< Parent Object Handle */
	SdiUInt16	Format;						/**< Object Format */

	SdiUInt8		AlreadySearchedThumb;		/**< Flag that whether thumbnail information examined or not */

	SdiUInt32	OffsetThumbPtr;					/**< Thumbnail Start Position in JPEG File */
	SdiUInt32	ThumbSize;					/**< Thumbnail Size */

	SdiUInt32	ImgPixW;					/**< Image Width by pixel */
	SdiUInt32	ImgPixH;					/**< Image Height by pixel */

} PTPObjectType;

/**
 * \brief Object Full Information Structure (include Object Handle)
 * 
 * This structure consists of object handle and object information stucture
 */
typedef struct _ptp_full_object_info_type
{

	SdiUInt32		Handle; 	/**< Object Handle */
	PTPObjectInfoType	ObjectInfo;		/**< Object Information structure */
	
} PTPFullObjectInfoType;

/**
 * \brief Storage Structure
 * 
 * This structure consists of important features of Storage.\n
 * - base pathname, ID, PTPStorageType Information structure, PTPObjectType structure list, ...
 */
typedef struct _ptp_storage_type
{

	char			Name[PTP_OBJECT_NAME_LEN];		/**< Base Pathname */
	char			BasePath[PTP_BASEPATH_LEN];		/**< Base Pathname */
	SdiUInt32		ID;								/**< Storage ID */
	SdiBool			bIsActive;						/**< Storage ID */
		
	PTPStorageInfoType 	StorageInfo;						/**< Storage Information structure */
	
	SdiUInt32		NumObject;							/**< Number of Object in Storage */
	PTPObjectType*	Object;							/**< Pointer of PTPObjectType structure list */
 
} PTPStorageType;

/**
 * \brief Storage List Structure 
 * 
 * This structure consists of Number of storage, PTPSorage structure list
 */
typedef struct _ptp_storage_list_type
{

	int				Num;			/**< Number of Storage */
	PTPStorageType*	Storage;			/**< Point of PTPStorageType structure list */
  
} PTPStorageListType;

typedef struct _ptp_info_type
{
	int IsConnected;
	int StatusForDPS;				/**< It will be used for PictBridge */
	int StatusForHOST;			/**< It will be used for PTPHOST*/

	SdiUInt8			SessionStatus;	/**< whether the session is opened or close */
	SdiUInt32		SessionID;			
	PTPContainerType	Request;
	PTPContainerType  Response;

	char VirtualFile[PTP_VIRTUAL_FILE_SIZE];
	
} PTPInfoType;


