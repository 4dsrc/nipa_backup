/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_Host.h
 \brief Define PTP Host Class
 \date 2009-06-11
 \version 0.70 
*/

#pragma once

#include "PTP_Common.h"
#include "PTP_EnvironmentConfig.h"
#include "PTP_Endian.h"
//#include "PTP_Image.h"
//#include "PTP_Device.h"
#include "PTP_Storage.h"
#include "PTP_Transport.h"
#include "PTP_HOSTOperationHandler.h"


/*!
 \ingroup PTPHOST_Label
 \note ptp host status aren't be changed.
*/
#define PTPHOST_STATUS_NO_ACTION		0x0000

/*!
 \ingroup PTPHOST_Label
 \note Initial connection status : the first 9 thumbnail images would be displayed
*/
#define PTPHOST_STATUS_CONNECTED		0x0001

#define PTPHOST_STATUS_DISCONNECTED		0xFFFF

/*!
 \ingroup PTPHOST_Label
 \note request the next thumbnail images
*/
#define PTPHOST_STATUS_NEXT			0x0002

/*!
 \ingroup PTPHOST_Label
 \note request the previous thumbnail images
*/
#define PTPHOST_STATUS_PREVIOUS		0x0003

/*!
 \ingroup PTPHOST_Label
 \note request image copy
*/
#define PTPHOST_STATUS_REQUEST_COPY		0x0004

class PTPHOST //: public IPTP
{
public:

	//! PTPHOST Class의 create()/destroy()가 한번만 이루어지게 하기 위한 Flag.
	SdiBool mIsCreated;

	//! PTPHOST Class의 initialize()/terminate()가 한번만 이루어지게 하기 위한 Flag.
	SdiBool mIsInited;
public:

	//! indicate which thumbnail images displayed.
	int curmax;
	int curmin;
	int processing;
	//! In PTPHOST Class, thumbData array for displaying
	PTPThumbDataList mThumbDataList;

	//! this array will store the objecthandles at the beginning of firstConnection.
	SdiUInt32*	 array; 		//! initial  size = 1000
	


	//! PTPHOST Class가 동작하기 위해서 필요한 Inforamtion 저장.
	PTPInfoType m_PTPInfo;

	//! PTP Component 동작을 위한 Configuration 처리 Class Pointer.
	PTPEnvironmentConfig* mpEnvCfg;

	//! Image inforamtion 처리 Class Pointer.
	//PTPImage* mpImage;

	//! Device inforamtion 처리 Class Pointer.
	//PTPDevice* mpDevice;

	//! Storage inforamtion 처리 Class Pointer.
	PTPStorage* mpStorage;

	//! Transport inforamtion 처리 Class Pointer.
	PTPTransport* mpTransport;

	//! PTP Operation 처리 Class Pointer.
	PTPHOSTOperationHandler* mpHostOpHandler;
public:
	PTPHOST();
	
	~PTPHOST();
	
	//! processing PTP operation received from host.
	int transaction(void);
	int getSetObject(int count,char *filename);
	int getImgProcessing(void);
	//! create member class instance.
	int create(int bStandalone);

	//! destory member class instance.
	void destroy(void);

	//! set a level to print debug message.
	void setDebugPrintLevel(int level);

	//! set options. currently, handling only image or not, handling only objects in DCIM folder or not.
	void setOptions(unsigned char bIsOnlyImage,
					  unsigned char bIsOnlyDCIM,
					  unsigned char opt3,
					  unsigned char opt4,
					  unsigned char opt5);

	//! set device configuration. Device Model Name, Manufactuer Name, Version, Byte Order.
	int setDeviceConfig(char* model, char* manufacturer, char* version, int byteOrder);

	//! set total number of storage in device.
	int setNumStorage(int numStorage);

	//! set each storage configuration. Storage Name, Mount Path, Type, File System Type.
	int setStorageConfig(int number, char* name, char* path, int type, int fsType);

	//! set active storage in all storages.
	int setStorageActive(int number, unsigned char bActive);

	//! set transport configuration. Transoprt Name, Type, Byte Order.
	int setTransportConfig(char* name, int type, int byteOrder);


	//! set the number of the thumb data
	int setNumThumbList(int num);	

	//! initialize member variables and classes.
	int initialize(void);

	//!PTPHOST Status is next 
	int next(void);

	//!PTPHOST Status is previous 
	int previous(void);
	//! PTPHOST Status is connection
	int firstConnection(void);


	//! return resource used member class and reset member variables.
	void terminate(void);
	//! set the status of PTPHOST.
	void setStatusForPTPHOST(int value);
	//! get status for PictBridge(DPS).
	int getStatusForPTPHOST(void);
	//! get the Thumbnail list for displaying.
	int getThumbList(PTPThumbData* buffer, int thumbs);

	//!PTPHOST Status is requestcopy 
	int requestcopy(char * selected);

	CString getDevName();										// chlee
	void    setDevName(CString DevName);						// chlee


private:
	CString m_strDevName;										// chlee

	//! reset storage and object in it.
	int resetStorage(void);

	//! After receive the StorageID from Device, reinitialize the Storage.
	int reInitializeStorage(void);

	//! PTPHOST Status is connection
//	int firstConnection(void);

	//!PTPHOST Status is next 
//	int next(void);

	//!PTPHOST Status is previous 
//	int previous(void);

	//!to get the directory path where images will be copied.
	void getPath(char * path);
		
	//! get object handle number by its name.
	unsigned long getHandleByName(char* name);

	//! get object handle number by its path.
	unsigned long getHandleByPath(char* path);

	//! get object name by its handle number.
	int getNameByHandle(SdiUInt32 handle, char* name, int size);

	//! get object data by its handle number. 
	int getDateByHandle(SdiUInt32 handle, char* date, int size);



	//! check disconnected to host or not.
	SdiBool isDisconnected(void);

	//! close current session by force.
	void closeSessionByForce(void);

};

