/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_Errors.h
 \brief Define Error Codes used in PTP Component.
 \date 2009-06-11
 \version 0.70 
*/


#ifndef __PTP_ERRORS_H__
#define __PTP_ERRORS_H__


#define PTP_ERROR_NONE							0x0000

/** ERROR CODE - used in PTP Component Interface */
#define PTP_ERROR_CREATE_INSTANCE				0x02FF
#define PTP_ERROR_CANNOT_INIT					0x02FE

#define PTP_ERROR_BAD_PARAM						0x02FD
#define PTP_ERROR_NOT_READY						0x02FC

#define PTP_ERROR_VIRTUAL_FILE_SIZE				0x02FB
#define PTP_ERROR_BUFFER_SIZE					0x02FA

#define PTP_ERROR_SET_DEVICE_CONFIG				0x02F9
#define PTP_ERROR_SET_NUM_STORAGE				0x02F8
#define PTP_ERROR_SET_STORAGE_CONFIG			0x02F7
#define PTP_ERROR_SET_STORAGE_ACTIVE			0x02F6
#define PTP_ERROR_SET_TRANSPORT_CONFIG			0x02F5

#define PTP_ERROR_SEND_EVENT					0x02F4
#define PTP_ERROR_CANNOT_RESET					0x02F3

//#define PTP_ERROR_GET_NAME						0x02F1
//#define PTP_ERROR_GET_DATE						0x02F0

#endif  /* __PTP_ERRORS_H__ */

/*! 
\page PTP_Errors

<h1>PTP�� Error Codes</h1>

\section a SUCCESS
- 0x0000 : PTP_ERROR_NONE - No Error.
\section b FAIL
- 0x02FF : PTP_ERROR_CREATE_INSTANCE - The PTP can't create Sub Class Instance.
- 0x02FE : PTP_ERROR_CANNOT_INIT - The PTP can't initialize Sub Class Instatnce.
- 0x02FD : PTP_ERROR_BAD_PARAM - Paramter is invalid.
- 0x02FC : PTP_ERROR_NOT_READY - The PTP can't initialize, because Sub Class Instance is not created or
                                 Environment Configuration is not completed.
- 0x02FB : PTP_ERROR_VIRTUAL_FILE_SIZE - Virtual File Size is invalid.
- 0x02FA : PTP_ERROR_BUFFER_SIZE - Buffer Size is invalid.
- 0x02F9 : PTP_ERROR_SET_DEVICE_CONFIG - The PTP can't set device configuration.
- 0x02F8 : PTP_ERROR_SET_NUM_STORAGE - The PTP can't set total number of storage.
- 0x02F7 : PTP_ERROR_SET_STORAGE_CONFIG - The PTP can't set storage configuration.
- 0x02F6 : PTP_ERROR_SET_STORAGE_ACTIVE - The PTP can't set active storage.
- 0x02F5 : PTP_ERROR_SET_TRANSPORT_CONFIG - The PTP can't set transport configuration.
- 0x02F4 : PTP_ERROR_SEND_EVENT - The PTP can't send event through PTP Transport.
- 0x02F3 : PTP_ERROR_CANNOT_RESET - The PTP can't reset storage and object.
*/


