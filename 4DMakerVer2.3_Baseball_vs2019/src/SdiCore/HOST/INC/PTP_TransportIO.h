/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_TransportIO.h
 \brief Define PTP Transport I/O Interface Class.
 \date 2009-06-11
 \version 0.70 
*/


#ifndef __PTP_TRANSPORT_IO_H__
#define __PTP_TRANSPORT_IO_H__

#include "PTP_Common.h"

class PTPTransportIO
{
public:
	virtual ~PTPTransportIO(){};
	
	virtual int open(char* name) = 0;
	virtual void close(void) = 0;

#ifdef __PTP_HOST__
	virtual int sendCommand(PTPContainerType* command)= 0;	
	virtual int getResponse(PTPContainerType* res) = 0;

#else
	virtual int getRequest(PTPContainerType* req) = 0;
	virtual int sendResponse(PTPContainerType* resp) = 0;

	virtual int sendEvent(PTPContainerType* event) = 0;
	virtual int sendData(PTPContainerType* ptpCntr, char* txData, SdiUInt32 size, SdiUInt32 totalSize) = 0;
	virtual int sendDataOnly(char* txData, SdiUInt32 size, SdiBool bIsLastData) = 0;
#endif
	virtual int getData(PTPContainerType* req, char* rxData, SdiUInt32 size, int opt, void* optParam) = 0;
	virtual void reset(int targetIO) = 0;
	virtual int getConnectionStatus(void) = 0;

public:
    virtual void SetTransTimeout(int timeout) = 0;
    virtual int  GetTransTimeout() = 0;

    virtual int SdisetRequeset(PTPInfoType *ptpInfo) = 0;
    virtual int SdigetResponse(PTPInfoType *ptpInfo) = 0;
    virtual int SdisetData(PTPInfoType *ptpInfo, char **txData, SdiUInt32 size) = 0;
    virtual int SdigetData(PTPInfoType *ptpInfo, char **rxData, SdiUInt32& size) = 0;



};

#endif /* __PTP_TRANSPORT_IO_H__ */

