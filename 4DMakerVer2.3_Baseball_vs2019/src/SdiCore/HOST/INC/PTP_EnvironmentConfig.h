/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_EnvironmentConfig.h
 \brief Define PTP Environment Configuration Class.
 \date 2009-06-11
 \version 0.70 
*/
#ifndef __PTP_ENVIRONMENT_CONFIG_H__
#define __PTP_ENVIRONMENT_CONFIG_H__

#include "PTP_Common.h"

#define PTP_NUM_MANDATORY_OPT		6



class PTPEnvironmentConfig
{

private:
	int mCompletedCfg[PTP_NUM_MANDATORY_OPT];
	int mNumOfStorage;
	int mNumOfThumbs;
	
	//! In PTPHOST Class, thumbData array for displaying
//	PTPThumbDataList mThumbDataList;
	
	OptionsConfigType mOptionsCfg;
	DeviceConfigType mDeviceCfg;
	StorageConfigType *mpStorageCfg;
	TransportConfigType mTransportCfg;

public:
	static int msPTPDebugLevel;
	
public:
	PTPEnvironmentConfig();
	~PTPEnvironmentConfig();
	
	void setDebugPrintLevel(int level);
	void setOptions(SdiBool bIsOnlyImage,
					  SdiBool bIsOnlyDCIM,
					  SdiBool opt3,
					  SdiBool opt4,
					  SdiBool opt5);

	int setDeviceConfig(char* model, char* manufacturer, char* version, int byteOrder);
	int setNumStorage(int num);
	int setStorageConfig(int index, char* name, char* pathname, int type, int fsType);
	int setStorageActive(int index, SdiBool bActive);
	int setTransportConfig(char* name, int type, int byteOrder);
#ifdef __PTP_HOST__
	int setNumThumbList(int num);
#else
	char* getDeviceManufacturer(void);
	char* getDeviceModel(void);
	char* getDeviceVersion(void);

#endif

	
	int getDeviceByteOrder(void);

	int getNumStorage(void);
	int getNumThumbs(void);
	char* getStorageName(int number);
	char* getStoragePath(int number);
	SdiUInt16 getStorageType(int number);
	SdiUInt16 getStorageFilesystemType(int number);
	SdiBool getStorageActive(int number);
	
	char* getTransportName(void);
	int getTransportType(void);
	int getTransportByteOrder(void);

private:
	SdiBool isConfigCompleted(void);
	
	int getDebugPrintLevel(void);

	SdiBool getOnlyImageOption(void);
	SdiBool getOnlyDCIMOption(void);

	
};


#endif  /* __PTP_ENVIRONMENT_CONFIG_H__ */

