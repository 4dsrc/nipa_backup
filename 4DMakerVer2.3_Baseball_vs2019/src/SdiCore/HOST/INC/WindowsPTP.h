/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        WindowsPTP.h
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */

#ifndef _CWINDOWSPTP_H_
#define _CWINDOWSPTP_H_


class CWindowsPTP
{
public:
	static CWindowsPTP* GetInstance();
	~CWindowsPTP();

private:
	CWindowsPTP();

	// for method
public:
	BOOL GetPTPStatus(void);
	BOOL SetPTPStatus( BOOL bStatus );

	// for method
private:
	void InitilizePTP_Path();
	BOOL SafeMoveFile(LPCTSTR lpExistingFileName, LPCTSTR lpNewFileName);

	// for variable
private:
	TCHAR m_szPtpusb[MAX_PATH];
	TCHAR m_szPtpusd[MAX_PATH];
	TCHAR m_szConvertPtpusb[MAX_PATH];
	TCHAR m_szConvertPtpusd[MAX_PATH];
};

#endif