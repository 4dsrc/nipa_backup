/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_Storage.h
 \brief Define PTP Storage Class.
 \date 2009-06-11
 \version 0.70 
*/

#ifndef __PTP_STORAGE_H__
#define __PTP_STORAGE_H__

#include "PTP_Common.h"

#include "PTP_EnvironmentConfig.h"
#include "PTP_Endian.h"
#include "PTP_Image.h"


/** Pre-assigned Storage ID */
#define PTP_PHYSICAL_SESMID					0x0001
#define PTP_LOGICAL_SESMID					0x0001
#define PTP_LOGICAL_SESMID_REMOVED			0x0000

/** Virtual persistent storage ID */
#define PTP_PERSISTENT_VIRTUAL_SESMID		0x80000001

/** Mask for checking invalid Storage ID */
#define PTP_MASK_INVAILD_SESMID				0x0000FFFF

/**
 * Check whether storage is available by Storage ID, or not
 *
 * \param sESMID a Storage ID
 */
#define PTP_IS_SESM_AVAILABLE(sESMID)		(((sESMID & PTP_MASK_INVAILD_SESMID) == 0) ? 0 : 1)

/**
 * Make Storage ID
 *
 * \param phy a ID of physical storage
 * \param logic a ID of logical storage
 */
#define PTP_MAKE_SESMID(phy, logic)		(((SdiUInt32)(phy) << 16) + (SdiUInt32)(logic))


/** Byte position of StorageInfo dataset member */
#define PTP_SI_POS_SESMType				0
#define PTP_SI_POS_FSType				2
#define PTP_SI_POS_AccessCap			4
#define PTP_SI_POS_MaxCap				6
#define PTP_SI_POS_FreeSpcInBytes		14
#define PTP_SI_POS_FreeSpcInImg			22
#define PTP_SI_POS_SESMDesc				26

#define PTP_SID_POS_StorIDTot			0
#define PTP_SID_POS_StorageID			4

/** Deleted object handle */
#define PTP_DELETED_HANDLE		0xFFFFFFFFUL

/** Deleted object foramt */
#define PTP_DELETED_FORMAT		0xFFFF

/** Mask Value of lower object handle */
#define PTP_MASK_LOWER_HANDLE	0x0000FFFFUL

/** Upper Mask Value of object handle */
#define PTP_MASK_UPPER_HANDLE	0xFFFF0000UL


/**
 * Make object handle value
 *
 * \param sESMindex a index of storage indicating order in storage list
 * \param num a number of object
 */
#define PTP_MAKE_HANDLE(sESMIndex, num)	(((SdiUInt32)(sESMIndex) << 16) + (SdiUInt32)(num))

/**
 * Check whether object handle is a handle of ROOT object
 *
 * \param objhandle a handle of object
 */
#define PTP_IS_ROOT_HANDLE(handle)		(((handle & PTP_MASK_LOWER_HANDLE) == 0) ? 1 : 0)

/**
 * Make a index of object indicating restoring order in object list
 *
 * \param objhandle a handle of object
 */
#define PTP_MAKE_OBJINDEX_BYHANDLE(handle)			(int)(((SdiUInt32)handle & PTP_MASK_LOWER_HANDLE) - 1)

/**
 * Make a index of storage indicating restoring order in storage list
 *
 * \param objhandle a handle of object
 */
#define PTP_MAKE_SESMINDEX_BY_HANDLE(handle)			(int)(((SdiUInt32)handle & PTP_MASK_UPPER_HANDLE) >> 16)



/** Byte position of ObjectInfo dataset member */
#define PTP_OI_POS_SESMID				0
#define PTP_OI_POS_ObjFormat			4
#define PTP_OI_POS_ProtStat				6
#define PTP_OI_POS_ObjCompSize			8
#define PTP_OI_POS_ThumbFormat			12
#define PTP_OI_POS_ThumbCompSize		14
#define PTP_OI_POS_ThumbPixW			18
#define PTP_OI_POS_ThumbPixH			22
#define PTP_OI_POS_ImgPixW				26
#define PTP_OI_POS_ImgPixH				30
#define PTP_OI_POS_ImgBitD				34
#define PTP_OI_POS_ParentObj			38
#define PTP_OI_POS_AssType				42
#define PTP_OI_POS_AssDesc				44
#define PTP_OI_POS_SeqNum				48
#define PTP_OI_POS_FileName				52




class PTPStorage
{
private:
	PTPStorageListType mStorageList;

	PTPImage* mpPTPImage;
	
public:

	/* For PTP BOTH */
	
	PTPStorage();
	~PTPStorage();
	
	//! For BOTH : Initialize StorageList (Device-public, Host-private)
	int initStorage(PTPEnvironmentConfig* env, PTPImage* image);

	//! For BOTH : terminate StorageList
	void termStorage(void);

	//! For BOTH : return the number of Storage
	int getNumStorage(void);

	//! For BOTH : return the specific storage IDs
	SdiUInt32 getStorageIDs(SdiUInt32* IDs, int size);

	//! For BOTH : return the Storage given by ID
	PTPStorageType* findStorageByID(SdiUInt32 id);

	//! For BOTH : return the Storage given by Index
	PTPStorageType* findStorageByIndex(int index);

	//!For BOTH : initialize objects in the storages
	void initObject(void);

	//! For BOTH : terminate objects
	void termObject(void);

	//! For BOTH : unpack objectinfotype
	void unpackObjectInfo(PTPObjectInfoType* oi, char* buf);

	//! For BOTH : return the object Path
	int getObjectPath(PTPStorageType* sESM, PTPObjectType* obj, char* path);

	SdiUInt16 getObjectFormatByName(char* name);

	SdiUInt32 findObjectHandleByName(char* name, int mode);

	SdiUInt32 findObjectHandleByPath(char* path);

	PTPObjectType* findObjectByHandleInStorages(SdiUInt32 handle);

	SdiUInt32 getNumObjectHandlesInStorages(PTPStorageType* sESMHaveParent,
                                                  PTPObjectType* parentObj,
												  SdiUInt32 objFormat,
												  SdiUInt32* array);
	
	SdiUInt32 getNumObjectHandlesInSESM(PTPStorageType* sESM,
											PTPObjectType* parentObj,
											SdiUInt32 objFormat,
											SdiUInt32* array);


#ifdef __PTP_HOST__
	/* For PTP HOST */
	
	//! For PTP HOST : set the Storage IDs and Storage Nums
	void unpackStorageIDs(char* buf, PTPEnvironmentConfig* mpEnvCfg );

	//! 	For PTP HOST : return the number of object in the specific storage.
	SdiUInt32 getNumObject(int index);

	//! For PTP HOST : return the objecthandles in the specific storage.
	SdiUInt32 getObjectHandle(int index, int objectindex);

	//! For PTP HOST : set the StorageInfoType
	void unpackStorageInfo(PTPStorageInfoType* si, char* buf);	

	//! For PTP HOST : set the StorageInfo
	void setStorageInfo(int index, PTPStorageInfoType* si);

	//! For PTP HOST : set objecthandles
	void unpackObjectHandles(char* buf, SdiUInt32 sid);

	//! For PTP HOST : set object in the storageLIST
	void setPTPObjectType(SdiUInt32 id, SdiUInt32 i, PTPObjectInfoType* sentOi);

#else
	/* For PTP Device */	
	
	//! For Device : initialize virtual StorageInfo
	void initVirtualStorageInfo(PTPStorageInfoType* si);

	//! For Device : pack StorafeInfoType
	void packStorageInfo(char *buf, PTPStorageInfoType* si);

	//! For Device : return the size of StorageInfo
	SdiUInt32 sizeofStorageInfo(PTPStorageInfoType* si);

	//! For Device : return whether the Storage is removed or not.
	SdiBool isStorageRemoved(PTPStorageType* sESM);

	//! For Device : set objectinfoType
	int setObjectInfo(SdiUInt32 handle, PTPFullObjectInfoType* foi);

	void packObjectInfo(char* buf, PTPObjectInfoType* oi);

	SdiUInt32 sizeofObjectInfo(PTPObjectInfoType* oi);

 	int deleteObjectInStorages(SdiUInt32 objFormat);

	int deleteObject(PTPStorageType* sESM, PTPObjectType* obj);

	SdiBool isObjectFormatValid(SdiUInt32 format);

	int makeReservedFullObjectInfo(PTPFullObjectInfoType* foi,
										SdiUInt32 sESMID,
										SdiUInt32 parentObjHandle,
										PTPObjectInfoType* sentOi);

	void printObjectInfo(PTPObjectInfoType* oi);

#endif

private:	
	//! initialize the StorageInfoType
	void initStorageInfo(PTPEnvironmentConfig* env, int index);

	//! return the Storage index given by ID	
	int findStorageIndexByID(SdiUInt32 id);

	//! print the storageinfo
	void printStorageInfo(int index, PTPStorageInfoType* si);

	//! return the number of objects.
	SdiUInt32 countObject(char* basePath);

	//! set the obejct
	SdiUInt32 setObject(char* basePath, PTPObjectType* objs, SdiUInt32 offset, SdiUInt32 totalCount, int sIndex);

	void getObjectAssociationData(char* name, SdiUInt16 format, PTPObjectInfoType* oi);

	void getObjectDateTime(PTPDateTimeType* fromDate, SdiDateTime* toDate);

	PTPObjectType* findObjectByHandleInSESM(PTPStorageType* sESM, SdiUInt32 handle);

	SdiUInt32 getNumObjectHandlesInAssoc(PTPStorageType* sESM,
											  SdiUInt32 parentHandle,
											  SdiUInt32 objFormat,
											  SdiUInt32* array);
	
	int deleteAssociation(PTPStorageType* sESM, PTPObjectType* assocObj, int* isException);

	void markDeletedObject(PTPObjectType* obj);

	SdiBool isImageFormat(SdiUInt16 format);
	

	/* may not be necessary. */
	SdiUInt32 setStorageIDs(SdiUInt32 *IDs, int size);

	int getFileNameAndLastDirFromPath(char* toFileName, char* toDirName, char* fromPath);

	void getObjectName(PTPObjectType* obj, char* name);
	SdiUInt32 getTotalNumObjectHandles(SdiUInt32* array);
	

};

#endif /* __PTP_STORAGE_H__ */

