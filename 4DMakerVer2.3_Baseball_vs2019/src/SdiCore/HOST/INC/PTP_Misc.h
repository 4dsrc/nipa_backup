/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 */
/*!
 \file PTP_Misc.h
 \brief Define PTP Misc Class handling directory, file, and so on.
 \date 2009-06-11
 \version 0.70 
*/

#ifndef __PTP_MISC_H__
#define __PTP_MISC_H__

#include "PTP_Common.h"



#ifndef tm_t
typedef struct tm				tm_t;
#endif

/***********************************************/ 
/* Directory & FILE                            */
/***********************************************/
#ifndef dir_t
//typedef DIR						dir_t;
typedef char*						dir_t;
#endif

#ifndef dirent_t
//typedef struct dirent			dirent_t;
typedef struct DIRENT			dirent_t;
#endif

#ifndef file_t
typedef FILE					file_t;
#endif

#ifndef stat_t
//typedef	struct stat				stat_t;
typedef	struct _stat				stat_t;
#endif

#ifdef __WIN_SIMUL__
struct PTTime
{
	int year;
	int month;
	int day;
	int hour;
	int minute;
	int second;
};
struct tm_struct {
	int tm_sec;     //!< seconds after the minute - [0,59]
	int tm_min;     //!< minutes after the hour - [0,59]
	int tm_hour;    //!< hours since midnight - [0,23]
	int tm_mday;    //!< day of the month - [1,31]
	int tm_mon;     //!< months since January - [0,11]
	int tm_year;    //!< years since 1900
	int tm_wday;    //!< days since Sunday - [0,6]
	int tm_yday;    //!< days since January 1 - [0,365]
	int tm_isdst;   //!< daylight savings time flag  // NOT USED
};

#endif

class PTPMisc
{
public:
	static int checkMount(char* devname, char* dirname);

	static dir_t* openDir(char* dirname);
	static void closeDir(dir_t* dp);
	static int removeDir(char* dirname);
	static dirent_t* readDir(dir_t* dp);
	
	static file_t* openFile(char* path, char* opentype);
	static void closeFile(file_t* fp);
	static int removeFile(char* path);
	
	static int readFile(file_t* fp, char* readbuf, int size);
	static int writeFile(file_t* fp, char* writebuf, int size);
	static int seekFile(file_t* fp, SdiUInt32 offset);

	static void getStat(char* path, PTPObjectStatType* objStat);

	static void ConvertEpochToLocalTime(PTTime* localtime, unsigned long sec, int timezone, bool daylight);
	static void DS1371_BinaryToDate(unsigned long binary,tm_struct *datetime);
	static void changeToPTPDateTime(PTPDateTimeType *toDt, PTTime *fromDt);
	static void strncpyToUpper(char* toStr, char* fromStr, int len);
};


#if 0


/* Function prototype */



/**
 * \brief Check whether a storage(MC) is mounted, or not
 *
 * \param devname a pointer to storage  device name
 * \param dirname a pointer to directory name for checking
 *
 * \return PTPErrNone(= 1) if be mounted, otherwise PTP_NOTOK(= 0)
 *
 * This function check whether a storage(MC) is mounted on file system, or not.
 *
 * \remarks
 *
 * \note
 */
extern int
PTPMisc::checkMount(const char* devname, const char* dirname);


/**
 * \brief Remove directory in file system
 *
 * \param dirname a pointer to directory name for removing
 *
 * \return PTPErrNone(= 1) if success, otherwise PTP_NOTOK(= 0)
 *
 * This function remove directory in file system.
 *
 * \remarks
 * The directory to be removed should be empty, before remove.\n
 * If not so, this function can't remove directory. 
 *
 * \note
 */
extern int
PTPMisc::removeDir(char* dirname);


/**
 * \brief Open directory in file system
 *
 * \param dirname a pointer to directory name for opening
 *
 * \return a pointer to dir_t* , directory handle
 *
 * This function open directory in file system.
 *
 * \remarks
 *
 * \note
 */
extern dir_t* 
PTPMisc::openDir(char* dirname);


/**
 * \brief Close directory in file system
 *
 * \param dp a pointer to directory handle for closing
 *
 * \return none
 *
 * This function close directory in file system.
 *
 * \remarks
 *
 * \note
 */
extern void
PTPMisc::closeDir(dir_t*  dp);


/**
 * \brief Read directory in file system
 *
 * \param dp a pointer to directory handle for reading
 *
 * \return PTPErrNone(= 1) if success, otherwise PTP_NOTOK(= 0)
 *
 * This function read directory in file system.
 *
 * \remarks
 *
 * \note
 */
extern dirent_t *
PTPMisc::readDir(dir_t*  dp);


/**
 * \brief Remove file in file system
 *
 * \param path a pointe to file path for removing
 *
 * \return a pointer to dirent_t *, directory entryinfo structure
 *
 * This function remove file in file system.
 *
 * \remarks
 *
 * \note
 */
extern int
PTPMisc::removeFile(char* path);


/**
 * \brief Open file in file system
 *
 * \param path a pointer to file path for opening
 * \param opentype a pointer to open type
 *
 * \return PTPErrNone(= 1) if success, otherwise PTP_NOTOK(= 0)
 *
 * This function open file in file system as using opentype mode.
 *
 * \remarks
 *
 * \note
 */
extern file_t*
PTPMisc::openFile(char* path, char* opentype);


/**
 * \brief Close file in file system
 *
 * \param fp a pointer to file handle for closing
 *
 * \return PTPErrNone(= 1) if success, otherwise PTP_NOTOK(= 0)
 *
 * This function close file in file system.
 *
 * \remarks
 *
 * \note
 */
extern void
PTPMisc::closeFile(file_t* fp);


/**
 * \brief Read file in file system
 *
 * \param readbuf a pointer to buffer to be restored file data
 * \param size a size of data to read
 * \param fp a pointer to file handle for reading
 *
 * \return PTPErrNone(= 1) if success, otherwise PTP_NOTOK(= 0)
 *
 * This function read file in file system.
 *
 * \remarks
 *
 * \note
 */
extern int
PTPMisc::readFile(file_t* fp, char* readbuf, int size);


/**
 * \brief Read file in file system
 *
 * \param writebuf a pointer to buffer to be written file data
 * \param size a size of data to write
 * \param fp a pointer to file handle for writing
 *
 * \return PTPErrNone(= 1) if success, otherwise PTP_NOTOK(= 0)
 *
 * This function write data in file.
 *
 * \remarks
 *
 * \note
 */
extern int
PTPMisc::writeFile(file_t* fp, char* writebuf, int size);


/**
 * \brief Seek a position in file
 *
 * \param fp a pointer to file handle for seeking
 * \param offset a position in file
 *
 * \return PTPErrNone(= 1) if success, otherwise PTP_NOTOK(= 0)
 *
 * This function seek position in file.
 *
 * \remarks
 *
 * \note
 */
extern int
PTPMisc::seekFile(file_t* fp, uint32_t offset);


/**
 * \brief Get state data of file
 *
 * \param path a pointer to file path for getting state
 * \param objStat a pointer to \a PTPObjectStatType structure
 *
 * \return none
 *
 * This function get state data of file.
 * Namely, this state in formation...
 * \code
 *     SdiUInt16        attr;
 *     SdiUInt16        mode;
 *     uint32_t        size;
 *     PTPDateTimeType    ctime;
 *     PTPDateTimeType    mtime;
 *     PTPDateTimeType    atime;
 * \endcode
 *
 * \remarks
 * The State data of file is restored in \a objStat, \a PTPObjectStatType structure.
 *
 * \note
 */
extern void
PTPMisc::getStat(char* path, PTPObjectStatType* objStat);

/**
 * \brief Set \a PTPDateTimeType structure from \a tm_t structure
 *
 * \param todt a pointer to \a PTPDateTimeType structure
 * \param fromdt a pointer to \a tm_t structrue
 *
 * \return none
 *
 * This function set the \a todt - \a PTPDateTimeType structure from member data of \a fromdt - \a tm_t structure.
 * This function re-organize the date & time information in the \a fromdt - \a tm_t structure.
 *
 * \remarks
 *
 * \note
 */
extern void
PTPMisc::changeToPTPDateTime(PTPDateTimeType *toDt, PTTime *fromDt);

extern void
PTPMisc::strncpyToUpper(char* toStr, char* fromStr, int len);
#endif


#endif /* __PTP_MISC_H__ */

