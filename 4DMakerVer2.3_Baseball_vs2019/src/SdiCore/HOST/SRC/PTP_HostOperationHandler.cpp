/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 * 
 */

/*!
 \file PTP_HOSTOperationHandler.cpp
 \brief Implement PTP Host Operation Class.
 \date 2009-06-11
 \version 0.70 
*/

#include "stdafx.h"
#include "ptpIndex.h"
#include <PTP_HOSTOperationHandler.h>
#include <PTP_Misc.h>
#include <windows.h>
#if 1   // duvallee 07-29-2010 : for Windows7
#include "USBHAL.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

typedef enum
{
	PTP_NO_DATA_PHASE = 0,
	PTP_DATA_PRN_TO_DSC,
	PTP_DATA_DSC_TO_PRN,
	PTP_DATA_DSC_TO_PRN_M, //stores readed directly in caller buffer
} TPTP_DataPhaseType;



PTPHOSTOperationHandler::PTPHOSTOperationHandler()
{
	ulTransactionID = 0;
	ulSessionID = 1;

}

PTPHOSTOperationHandler::~PTPHOSTOperationHandler()
{

}

/**
* @ fn  int getDeviceInfo(PTPInfoType* ptpInfo,  PTPDevice* dev,
* 					 PTPStorage* sESMAndObj,  PTPTransport* trans)
* @ brief 	this operation is the only operation that may be issued inside or outside of a session. 
* 		when used outside a session, both the sessionID and the transactionID in the OperationRequest
* 		dataset shall be set to 0x00000000
* 		send Command[0x1001] and get Data and response
* @ param ptpInfo [inout] 
* @ param dev [in] 
* @ param sESMAndObj [in] 
* @ param trans [in] 
* @ return int result
*/

int
PTPHOSTOperationHandler::sendSQEorder(PTPInfoType* ptpInfo,
									  PTPTransport* trans)
{
	int ret;

	
	ptpInfo->SessionID = ulSessionID;

	ulTransactionID = nextTransactionID();

	ptpInfo->Request.TransactionID = ulTransactionID;

	PRINTD0("---< Command : getDeviceInfo >---\n");

//PTP_OC_GetDeviceInfo

	ret = trans->sendCommand(ptpInfo,ptpInfo->Request.Code , ptpInfo->Request.Param1,ptpInfo->Request.Param2,ptpInfo->Request.Param3,ptpInfo->Request.Nparam);

	if(ret != PTPErrNone)
	{
		return ret;
	}

	return ret; 	
}

int
PTPHOSTOperationHandler::getSETpayload(PTPInfoType* ptpInfo,
									  PTPTransport* trans)
{
	int ret;

//	ulSessionID =0;
//	ulTransactionID = 0;
	
	ptpInfo->SessionID = ulSessionID;
	ptpInfo->Request.TransactionID = ulTransactionID;

	PRINTD0("---< Command : getDeviceInfo >---\n");


	ret = trans->getData(ptpInfo, ptpInfo->VirtualFile,PTP_VIRTUAL_FILE_SIZE, PTP_GET_DATA_NONE, NULL);

	
	if(ret != PTPErrNone)
	{
		return ret;
	}

		return ret;
}


int
PTPHOSTOperationHandler::getSETresponse(PTPInfoType* ptpInfo,
									  PTPTransport* trans)
{
	int ret;

//	ulSessionID =0;
//	ulTransactionID = 0;
	
	ptpInfo->SessionID = ulSessionID;
	ptpInfo->Request.TransactionID = ulTransactionID;

	PRINTD0("---< Command : getDeviceInfo >---\n");

	ret = trans->getResponse(ptpInfo);
	if(ret != PTPErrNone)
	{
		return ret;
	}

//	ulSessionID++;

		return ret;

}


int
PTPHOSTOperationHandler::getDeviceInfo(PTPInfoType* ptpInfo,
									  PTPTransport* trans)
{
	int ret;

	//ulSessionID =0;
	//ulTransactionID = 0;
	
	ptpInfo->SessionID = ulSessionID;

	ulTransactionID = nextTransactionID();

	ptpInfo->Request.TransactionID = ulTransactionID;

	PRINTD0("---< Command : getDeviceInfo >---\n");
	
	ret = trans->sendCommand(ptpInfo, PTP_OC_GetDeviceInfo, 0,0,0,0);

	if(ret != PTPErrNone)
	{
		return ret;
	}
	
	ret = trans->getData(ptpInfo, ptpInfo->VirtualFile,PTP_VIRTUAL_FILE_SIZE, PTP_GET_DATA_NONE, NULL);
	if(ret != PTPErrNone)
	{
		return ret;
	}

	ret = trans->getResponse(ptpInfo);
	if(ret != PTPErrNone)
	{
		return ret;
	}



	return ret; 
}

/**
* @ fn  int openSession(PTPInfoType* ptpInfo, PTPTransport* trans)
* @ brief 	this operation 	send Command[0x1002] and get response from Device
* @ param ptpInfo [inout] 
* @ param dev [in] 
* @ param sESMAndObj [in] 
* @ param trans [in] 
* @ return int result
*/

int
PTPHOSTOperationHandler::openSession(PTPInfoType* ptpInfo, PTPTransport* trans)
{
	int ret;
	SdiUInt32 param1 = 0;
	char nparam = 0;

	SdiUInt16 respCode = PTP_RC_OK;

	PTPContainerType* req = &(ptpInfo->Request);
	
	PRINTD0("---< Command openSession >---\n");
	
	if (ptpInfo->SessionStatus == PTP_SESSION_OPENED)
	{
		/* support only single session */
		PRINTD0("-?>sesseion is already opened.\n");

		respCode = PTP_OC_CloseSession;
		param1 = ptpInfo->SessionID;
		ptpInfo->Request.TransactionID = nextTransactionID();
		nparam = 1;
		ret = trans->sendCommand(ptpInfo, PTP_OC_CloseSession, param1, 0,0, 1);
		if(ret != PTPErrNone)
		{
			return ret;
		}
		ret = trans->getResponse(ptpInfo);	
        
		if(ret != PTPErrNone)
		{
			return ret;
		}
	}

	/* save session ID in context */	
	ptpInfo->SessionStatus = PTP_SESSION_OPENED;
	ptpInfo->SessionID = ulSessionID;
	ptpInfo->Request.TransactionID = 0;
	param1 = ptpInfo->SessionID;
	req->SessionID = ulSessionID;
	
	/*
	 * allocate resources, assigns handles to data objects 
	 * if necessary, and performs any connection-specific initialization
	 */

	ret = trans->sendCommand(ptpInfo, PTP_OC_OpenSession,  param1, 0,0,1);
	if(ret != PTPErrNone)
	{
		return ret;
	}

	ret = trans->getResponse(ptpInfo);	
	if(ret != PTPErrNone)
	{
		return ret;
	}

	//--2011-07-28 jeansu
	//return PTPErrNone;
	return ptpInfo->Response.Code;
}

/**
* @ fn  int getStorageIDs(PTPInfoType* ptpInfo, PTPDevice* dev, PTPStorage* sESMAndObj, PTPTransport* trans, PTPEnvironmentConfig* mpEnvCfg )
* @ brief 	this operation 	send Command[0x1004] and get Data and response from Device
* 		After that, StorageID and StorageNum will be set
* @ param ptpInfo [inout] 
* @ param dev [in] 
* @ param sESMAndObj [in] 
* @ param trans [in] 
* @ return int result
*/

int
PTPHOSTOperationHandler::getStorageIDs(PTPInfoType* ptpInfo,
									  PTPStorage* sESMAndObj,
									  PTPTransport* trans,
									  PTPEnvironmentConfig* mpEnvCfg )
{
	SdiUInt32 param = 0;
	int ret=0;

	PRINTD0("---< ptp_opfunc_getstorageIDs >---\n");

	ptpInfo->Request.TransactionID = nextTransactionID();
	param = ptpInfo->Request.TransactionID;
	ret = trans->sendCommand(ptpInfo, PTP_OC_GetStorageIDs, param, 0,0,1);
	if(ret != PTPErrNone)
		return ret;
 
	ret = trans->getData(ptpInfo, ptpInfo->VirtualFile,PTP_VIRTUAL_FILE_SIZE, PTP_GET_DATA_OBJECT_INFO, NULL);

	if(ret != PTPErrNone)
		return ret;

	sESMAndObj->unpackStorageIDs(ptpInfo->VirtualFile, mpEnvCfg);
	if(ret != PTPErrNone)
		return ret;

	ret = trans->getResponse( ptpInfo);
	if(ret != PTPErrNone)
		return ret;
	
	return PTPErrNone;
}


/**
* @ fn  int getStorageInfo(PTPInfoType* ptpInfo, PTPDevice* dev,PTPStorage* sESMAndObj,PTPTransport* trans)
* @ brief 	this operation 	send Command[0x1005] and get Data and response from Device, 
*		After that Storage Information will be set
* @ param ptpInfo [inout] 
* @ param dev [in] 
* @ param sESMAndObj [in] 
* @ param trans [in] 
* @ return int result
*/

int
PTPHOSTOperationHandler::getStorageInfo(PTPInfoType* ptpInfo,
									  PTPStorage* sESMAndObj,
									  PTPTransport* trans)
{
	int ret, i=0, num=0, count = 0;
	PTPStorageInfoType si;
	SdiUInt32* sESMIDs = NULL;
	SdiUInt32 param = 0;
	
	PRINTD0("---< getStorageInfo >---\n");
	ptpInfo->Request.TransactionID = nextTransactionID();

	/* Data Phase : send device info D->I */
	num = sESMAndObj->getNumStorage();
	sESMIDs = new SdiUInt32[num + 1];
	if (sESMIDs == NULL)
		return PTPErrNotEnoughMem;
	count = sESMAndObj->getStorageIDs(sESMIDs, num + 1);
	if(num>0)
	{
		for (i = 0; i < num; i++)
		{
			param = sESMIDs[i];
			ret = trans->sendCommand(ptpInfo, PTP_OC_GetStorageInfo, param, 0,0,1);
			if(ret != PTPErrNone)
				return ret;

			ret = trans->getData(ptpInfo, ptpInfo->VirtualFile,PTP_VIRTUAL_FILE_SIZE, PTP_GET_DATA_OBJECT_INFO, NULL);
			if(ret != PTPErrNone)
				return ret;
			sESMAndObj->unpackStorageInfo(&si, ptpInfo->VirtualFile);
			sESMAndObj->setStorageInfo(i, &si);
			ret = trans->getResponse( ptpInfo);
			if(ret != PTPErrNone)
				return ret;			
		}
	}else {
		param = PTP_PERSISTENT_VIRTUAL_SESMID;
		ret = trans->sendCommand(ptpInfo, PTP_OC_GetStorageInfo, param, 0,0,1);

		if(ret != PTPErrNone)
			return ret;

		ret = trans->getData(ptpInfo, ptpInfo->VirtualFile,PTP_VIRTUAL_FILE_SIZE, PTP_GET_DATA_OBJECT_INFO, NULL);
		sESMAndObj->unpackStorageInfo(&si, ptpInfo->VirtualFile);
		if(ret != PTPErrNone)
			return ret;
		sESMAndObj->setStorageInfo(0, &si);
		ret = trans->getResponse( ptpInfo);
		if(ret != PTPErrNone)
			return ret;

	}
//euns 0303
	if(sESMIDs!=NULL)
	{
		delete[] sESMIDs;
		sESMIDs =NULL;
	}
	return PTPErrNone;
}

/**
* @ fn  int getObjectHandles(PTPInfoType* ptpInfo, PTPDevice* dev, PTPStorage* sESMAndObj, PTPTransport* trans)
* @ brief 	this operation 	send Command[0x1007] and get Data and response from Device, 
*		After that Object Handles will be set
* @ param ptpInfo [inout] 
* @ param dev [in] 
* @ param sESMAndObj [in] 
* @ param trans [in] 
* @ return int result
*/

int
PTPHOSTOperationHandler::getObjectHandles(PTPInfoType* ptpInfo,
									  PTPStorage* sESMAndObj,
									  PTPTransport* trans)
{
	int ret, num=0, count = 0;
	SdiUInt32 param1 = 0, param2 = 0, param3 = 0;
	SdiUInt32* sESMIDs = NULL;


	PRINTD0("---< getObjectHandles >---\n");
	ptpInfo->Request.TransactionID = nextTransactionID();

	/* Need to consider whether we gonna indicate the storageID of selected store. */
	param1 = PTP_DESIRE_ALL_STORAGES;

	num = sESMAndObj->getNumStorage();
#if 0	
	sESMIDs = new SdiUInt32[num + 1];
	if (sESMIDs == NULL)
		return PTPErrNotEnoughMem;
	count = sESMAndObj->getStorageIDs(sESMIDs, num + 1);

	param1 = sESMIDs[0];
#endif
	
	param2 = 0;
	param3 = 0; //PTP_DESIRE_ROOT_PARENT;
	
	ret = trans->sendCommand(ptpInfo, PTP_OC_GetObjectHandles, param1, param2,param3,3);
	if(ret != PTPErrNone)
		return ret;

	ret = trans->getData(ptpInfo, ptpInfo->VirtualFile,
		PTP_VIRTUAL_FILE_SIZE, PTP_GET_DATA_OBJECT_INFO, NULL);
	sESMAndObj->unpackObjectHandles(ptpInfo->VirtualFile, param1);
	    
	if(ret != PTPErrNone)
		return ret;

	ret = trans->getResponse( ptpInfo);
	if(ret != PTPErrNone)
		return ret;
	
	return PTPErrNone;
}


/**
* @ fn  int getObjectInfo(PTPInfoType* ptpInfo, PTPDevice* dev, PTPStorage* sESMAndObj, PTPTransport* trans)
* @ brief 	this operation 	send Command[0x1008] and get Data and response from Device, 
*		After that Object Type will be set
* @ param ptpInfo [inout] 
* @ param dev [in] 
* @ param sESMAndObj [in] 
* @ param trans [in] 
* @ return int result
*/
int
PTPHOSTOperationHandler::getObjectInfo(PTPInfoType* ptpInfo,
									PTPStorage* sESMAndObj,
									PTPTransport* trans)
{

	SdiUInt32 i, ret;
	SdiUInt32 param1 = 0, param2 = 0, param3 = 0, totalCount = 0;
	PTPObjectInfoType sentOi;
	PTPStorageType* foundSESM = NULL;
	SdiUInt32* objHandles = NULL;

	PRINTD0("---< getObjectInfo >---\n");

	foundSESM = sESMAndObj->findStorageByID(0);
	totalCount = sESMAndObj->getNumObject(0);
	objHandles = new SdiUInt32[totalCount];
	if (objHandles == NULL)
	{
		//-- 2011-8-4 Lee JungTaek
		//-- Code Sonar
		PRINTD0("-?>not enough memory for object handles.\n");
		return TRUE;
	}
	else
		/* get object handle list */
		memset(objHandles, 0, sizeof(SdiUInt32)*totalCount);
	
	/* Need to consider whether we gonna indicate the storageID of selected store. */
	for(i=0; i < totalCount; i++)
	{	
		objHandles[i] = sESMAndObj->getObjectHandle(0,i);
		ptpInfo->Request.TransactionID = nextTransactionID();
	
		ret = trans->sendCommand(ptpInfo, PTP_OC_GetObjectInfo, objHandles[i], param2,param3,1);
		if(ret != PTPErrNone)
			return ret;

		ret = trans->getData(ptpInfo, ptpInfo->VirtualFile,
			PTP_VIRTUAL_FILE_SIZE, PTP_GET_DATA_OBJECT_INFO, NULL);
		sESMAndObj->unpackObjectInfo(&sentOi, ptpInfo->VirtualFile);    
		sESMAndObj->setPTPObjectType(0,i, &sentOi);
		if(ret != PTPErrNone)
			return ret;
		ret = trans->getResponse( ptpInfo);
		if(ret != PTPErrNone)
			return ret;
	}
	//euns 0303
	if(objHandles!=NULL)
	{
		//prevent
		//delete objHandles;
		delete[] objHandles;
		objHandles =NULL;
	}
	return PTPErrNone;
	
}


/**
* @ fn  int getThumb(PTPInfoType* ptpInfo, PTPDevice* dev,PTPStorage* sESMAndObj,
*				PTPTransport* trans, SdiUInt32	handle, PTPThumbData* mthumbdata)
* @ brief 	this operation 	send Command[0x100A] and get Data and response from Device, 
*		After that ThumbData will be set in the thumbdata list.
* @ param ptpInfo [inout] 
* @ param dev [in] 
* @ param sESMAndObj [in] 
* @ param trans [in] 
* @ return int result
*/
int
PTPHOSTOperationHandler::getThumb(PTPInfoType* ptpInfo,
									PTPTransport* trans,
									SdiUInt32	handle,
									PTPThumbData* mthumbdata)
{

	int  ret;
	SdiUInt32 param1 = 0, param2 = 0, param3 = 0;

	PRINTD0("---< getThumb >---\n");
	ptpInfo->Request.TransactionID = nextTransactionID();
	param1 = handle;
	ret = trans->sendCommand(ptpInfo, PTP_OC_GetThumb, param1, param2,param3,1);
	if(ret != PTPErrNone)
		return ret;

	ret = trans->getData(ptpInfo, ptpInfo->VirtualFile,
		PTP_VIRTUAL_FILE_SIZE, PTP_GET_DATA_OBJECT_INFO, NULL);
	mthumbdata->handle = handle;
	memcpy(mthumbdata->ThumbData, ptpInfo->VirtualFile, sizeof(ptpInfo->VirtualFile));
	if(ret != PTPErrNone)
		return ret;

	ret = trans->getResponse( ptpInfo);
	if(ret != PTPErrNone)
		return ret;

	return PTPErrNone;

}

/**
* @ fn  int closeSession(PTPInfoType	* ptpInfo, PTPTransport* trans)
* @ brief 	this operation 	send Command[0x1003] and get response from Device, 
* @ param ptpInfo [inout] 
* @ param dev [in] 
* @ param sESMAndObj [in] 
* @ param trans [in] 
* @ return int result
*/

int
PTPHOSTOperationHandler::closeSession(PTPInfoType	* ptpInfo,
								PTPTransport* trans)
{
	int ret;
	SdiUInt32 param1 = 0, param2 = 0, param3 = 0;

	PRINTD0("---< closeSession >---\n");
	ptpInfo->Request.TransactionID = nextTransactionID();

	/*
	 * free resources, assigns handles to data objects 
	 * if necessary, and performs any connection-specific termination
	 */
//	sESMAndObj->termObject();  /* Seems not necessary... Maybe later when the cable is disconnected.*/
	ret = trans->sendCommand(ptpInfo, PTP_OC_CloseSession, param1, param2,param3,0);
	if(ret != PTPErrNone)
		return SDI_ERR_DEVICE_DISCONNECTED;
	/* Response Phase */
	ret = trans->getResponse( ptpInfo);
	if(ret != PTPErrNone)
		return SDI_ERR_DEVICE_DISCONNECTED;

	/* reset sesseion ID in context */
	ptpInfo->SessionID = 0;
	ptpInfo->SessionStatus = PTP_SESSION_CLOSED;
	ptpInfo->Request.SessionID = 0;
	
	//--2011-07-28 jeansu
	if(ptpInfo->Response.Code == PTP_RC_OK) return SDI_ERR_OK;
	return ptpInfo->Response.Code; // -- 2011-7-26 chlee
}

/**
* @ fn  int getObject(PTPInfoType* ptpInfo, PTPDevice* dev,PTPStorage* sESMAndObj,
*				PTPTransport* trans,SdiUInt32	handle, char * path)
* @ brief 	this operation 	send Command[0x1009] and get Data and response from Device, 
*		After that Object Data will be get and saved in a file.
* @ param ptpInfo [inout] 
* @ param dev [in] 
* @ param sESMAndObj [in] 
* @ param trans [in] 
* @ return int result
*/


#define CHECK_TIME_START QueryPerformanceFrequency((_LARGE_INTEGER*)&freq);QueryPerformanceCounter((_LARGE_INTEGER*)&start)

#define CHECK_TIME_END(a) QueryPerformanceFrequency((_LARGE_INTEGER*)&freq);QueryPerformanceCounter((_LARGE_INTEGER*)&end); a=(float)((float)(end-start)/freq)

int
PTPHOSTOperationHandler::getObject(PTPInfoType* ptpInfo,
									PTPTransport* trans,
									SdiUInt32	handle,
									char * path)


{
	int  ret, respCode;
	SdiUInt32 param1 = 0, param2 = 0, param3 = 0;

	__int64 start,freq,end;
	float resultTime = 0;
	CString down ;

	file_t* fp = NULL;
	
	PRINTD0("---< getObject >---\n");
	
	ptpInfo->Request.TransactionID = nextTransactionID();
	param1 = handle;
	ret = trans->sendCommand(ptpInfo, PTP_OC_GetObject, param1, param2,param3,1);

	if(ret != PTPErrNone)
		return ret;
	fp = PTPMisc::openFile(path, "wb+");




	CHECK_TIME_START;

	if (fp)
	{
		ret = trans->getData(ptpInfo, ptpInfo->VirtualFile, PTP_VIRTUAL_FILE_SIZE, PTP_GET_DATA_OBJECT, fp);
		if (ret != PTPErrNone)
			respCode = PTP_RC_GeneralError;
		else
			respCode = PTP_RC_OK;
		PTPMisc::closeFile(fp);
	}
	else
		respCode = PTP_RC_StoreFull;

	CHECK_TIME_END(resultTime);

	down.Format(_T("%f(s)"),resultTime);


	//rujin 11.24 getObject

	//AfxMessageBox(down);


	ret = trans->getResponse( ptpInfo);

	if(ret != PTPErrNone)
		return ret;

	return PTPErrNone;
}

/**
* @ fn  int printObjectHandles(SdiUInt32* handles, SdiUInt32 len)
* @ brief 	this operation prints out the list of handles given
* @ param handles [in] 
* @ param len [in] 
* @ return void
*/

void
PTPHOSTOperationHandler::printObjectHandles(SdiUInt32* handles, SdiUInt32 len)
{
	if (PTPEnvironmentConfig::msPTPDebugLevel <= 1)
	{
		SdiUInt32 i;

		PRINTD0("\n->Object Handle Array");
		
		for (i = 0; i < len; i++)
		{
			if (!(i % 4))
				PRINTD1("\n [%08lX] ", handles[i]);
			else
				PRINTD1(" [%08lX] ", handles[i]);
		}
	}
	
	return;
}

/**
* @ fn  SdiUInt32 nextTransactionID()
* @ brief 	this operation offers the next transactionID
* @ return transactionID
*/
SdiUInt32 PTPHOSTOperationHandler::nextTransactionID()
{

	++ulTransactionID;


	if (ulTransactionID == 0xffffffff)
		ulTransactionID = 0x00000000;

	return ulTransactionID;
}

int
PTPHOSTOperationHandler::SdiopenSession(PTPInfoType* ptpInfo,
                                     PTPTransport* trans)
{
    int ret;
    SdiUInt32 param1 = 0;
    char nparam = 0;
    
    SdiUInt16 respCode = PTP_RC_OK;

    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(100);
    ret = SdicloseSession(ptpInfo, trans);
    trans->SetTransTimeout(Timeout);
    

    PTPContainerType* req = &(ptpInfo->Request);

    PRINTD0("---< Command openSession >---\n");
    
    if (ptpInfo->SessionStatus == PTP_SESSION_OPENED)
    {
        /* support only single session */
        PRINTD0("-?>session is already opened.\n");

        ret = SdicloseSession(ptpInfo, trans);
        
        if(ret != PTPErrNone)
        {
            return ret;
        }
        return ret;
    }

    /* save sesseion ID in context */
    req->Code = PTP_OC_OpenSession;    
    req->SessionID = 0;
    req->TransactionID = 0;    
    req->Param1 = ulSessionID;
    req->Nparam = 1;

    ret = trans->Sditransaction_none(ptpInfo);

    if (ret != PTPErrNone)
    {
        return ret;    
    }

    ptpInfo->SessionStatus = PTP_SESSION_OPENED;
    req->SessionID = ulSessionID;

    return PTPErrNone;
}

int
PTPHOSTOperationHandler::StartRemoteStudio(PTPInfoType* ptpInfo,
                                     PTPTransport* trans)
{
    int ret;
    SdiUInt32 param1 = 0;
    char nparam = 0;
    
    SdiUInt16 respCode = PTP_RC_OK;

/*    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(100);
    ret = SdicloseSession(ptpInfo, trans);
    trans->SetTransTimeout(Timeout);
  */  

    PTPContainerType* req = &(ptpInfo->Request);
/*
    PRINTD0("---< Command openSession >---\n");
    
    if (ptpInfo->SessionStatus == PTP_SESSION_OPENED)
    {
        /* support only single session 
        PRINTD0("-?>session is already opened.\n");

        ret = SdicloseSession(ptpInfo, trans);
        
        if(ret != PTPErrNone)
        {
            return ret;
        }
        return ret;
    }
*/

    /* save sesseion ID in context */
    req->Code = PTP_OC_SAMSUNG_SetRemoteStudioMode;    
    req->SessionID = 0;
    req->TransactionID = nextTransactionID();    
    req->Param1 = ulSessionID;
    req->Nparam = 1;

    ret = trans->Sditransaction_none(ptpInfo);

    if (ret != PTPErrNone)
    {
        return ret;    
    }

    ptpInfo->SessionStatus = PTP_SESSION_OPENED;
    req->SessionID = ulSessionID;

    return SDI_ERR_OK; // -- 2011-7-26 chlee
}


int
PTPHOSTOperationHandler::SdicloseSession(PTPInfoType* ptpInfo,
                                         PTPTransport* trans)
{
    PRINTD0("---< closeSession >---\n");

    PTPContainerType* req = &(ptpInfo->Request);

    req->Code = PTP_OC_CloseSession;
    req->TransactionID = nextTransactionID();    
    req->Nparam = 0;
    
    int ret = trans->Sditransaction_none(ptpInfo);

    /* reset sesseion ID in context */
    ptpInfo->SessionID = 0;
    ptpInfo->SessionStatus = PTP_SESSION_CLOSED;
    ptpInfo->Request.SessionID = 0;

    return ret;
}

int
PTPHOSTOperationHandler::SdisendCapture(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes,
                                            SdiUInt8 **data, SdiUInt32& size)
{
    
    PTPContainerType* req = &(ptpInfo->Request);

    req->Code = PTP_OC_SAMSUNG_BCR_Capture;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;
	req->Param1 = 0;						//  AF_TIME;  chlee

    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_GETDATA;

    int ret = SDI_ERR_OK; // -- 2011-7-26 chlee
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(100000);               // timeout
	
	

    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    case ADT_NX_GETDATA:			
        ret = trans->Sditransaction_get(ptpInfo, (char **)data, dataSize);
        TRACE ("Get Data from DSC : size [%d]\n", dataSize);
		size = dataSize;
        break;

    case ADT_NX_SENDDATA:		
        ret = trans->Sditransaction_set(ptpInfo, (char **)data, dataSize);
        TRACE ("Send Data to DSC : size [%d]\n", adjReq[4]);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
        break;
    }

    trans->SetTransTimeout(Timeout);
    

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
    return ret;
}

                                        

//-- 2011-7-7		 chlee
int
PTPHOSTOperationHandler::SdiGetDevUniqueID(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes,
                                            SdiUInt8** data,
											SdiUInt32& size)
{
    
    PTPContainerType* req = &(ptpInfo->Request);

    //req->Code = PTP_OC_GetDevicePropDesc;
	req->Code = PTP_OC_SAMSUNG_GETDEVICEPROPDESC;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;

    memcpy (&req->Param1, adjReq, sizeof(SdiUInt32) * 5);
    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_GETDATA;;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(30000);

    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    case ADT_NX_GETDATA:			
        ret = trans->Sditransaction_get(ptpInfo, (char **)data, dataSize);
        TRACE ("Get Data from DSC : size [%d]\n", dataSize);
		size = dataSize;
        break;

    case ADT_NX_SENDDATA:		
        ret = trans->Sditransaction_set(ptpInfo, (char **)data, dataSize);
        TRACE ("Send Data to DSC : size [%d]\n", adjReq[4]);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
        break;
    }

    trans->SetTransTimeout(Timeout);
    

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
    ret = ptpInfo->Response.Code;

    return ret;
}

int
PTPHOSTOperationHandler::SdiGetDevModeIndex(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes,
                                            SdiUInt8** data,
											SdiUInt32& size)
{
    
    PTPContainerType* req = &(ptpInfo->Request);

    //req->Code = PTP_OC_GetDevicePropDesc;
	req->Code = PTP_OC_SAMSUNG_GETDEVICEPROPDESC;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;

//	req->Param1 = PTP_CODE_SAMSUNG_UNIQUEID ;
//	req->Param1 = 0x0000d81c;

    memcpy (&req->Param1, adjReq, sizeof(SdiUInt32) * 5);
    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_GETDATA;;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(30000);

    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    case ADT_NX_GETDATA:			
        ret = trans->Sditransaction_get(ptpInfo, (char **)data, dataSize);
        TRACE ("Get Data from DSC : size [%d]\n", dataSize);
		size = dataSize;
        break;

    case ADT_NX_SENDDATA:		
        ret = trans->Sditransaction_set(ptpInfo, (char **)data, dataSize);
        TRACE ("Send Data to DSC : size [%d]\n", adjReq[4]);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
    }

    trans->SetTransTimeout(Timeout);
    

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
	ret = ptpInfo->Response.Code;

	ret = (ret == 0x2001) ? SDI_ERR_OK : ret;			//-- 2011.7.26 chlee  SDI_ERR_OK ADD

    return ret;
}

int
PTPHOSTOperationHandler::SdiSendAdjLiveviewInfo(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes,
                                            SdiUInt8** data,
											SdiUInt32& size)
{
    
    PTPContainerType* req = &(ptpInfo->Request);

    req->Code = PTP_OC_SAMSUNG_GetLiveviewInfo;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;

//	req->Param1 = PTP_CODE_SAMSUNG_UNIQUEID ;
//	req->Param1 = 0x0000d81c;

    memcpy (&req->Param1, adjReq, sizeof(SdiUInt32) * 5);
    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_GETDATA;;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(30000);

    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    case ADT_NX_GETDATA:			
        ret = trans->Sditransaction_get(ptpInfo, (char **)data, dataSize);
        TRACE ("Get Data from DSC : size [%d]\n", dataSize);
		size = dataSize;
        break;

    case ADT_NX_SENDDATA:		
        ret = trans->Sditransaction_set(ptpInfo, (char **)data, dataSize);
        TRACE ("Send Data to DSC : size [%d]\n", adjReq[4]);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
        break;
    }

    trans->SetTransTimeout(Timeout);
    

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
	ret = ptpInfo->Response.Code;

	ret = (ret == 0x2001) ? SDI_ERR_OK : ret;			//-- 2011.7.26 chlee  SDI_ERR_OK ADD

    return ret;
}

int
PTPHOSTOperationHandler::SdiGetDevPropDesc(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes,
                                            SdiUInt8** data,
											SdiUInt32& size)
{
    
    PTPContainerType* req = &(ptpInfo->Request);

    //req->Code = PTP_OC_GetDevicePropDesc;
	req->Code = PTP_OC_SAMSUNG_GETDEVICEPROPDESC;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;

//	req->Param1 = PTP_CODE_SAMSUNG_UNIQUEID ;
//	req->Param1 = 0x0000d81c;

    memcpy (&req->Param1, adjReq, sizeof(SdiUInt32) * 5);
    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_GETDATA;;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(30000);
	
    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    case ADT_NX_GETDATA:			
        ret = trans->Sditransaction_get(ptpInfo, (char **)data, dataSize);
        TRACE ("Get Data from DSC : size [%d]\n", dataSize);
		size = dataSize;
        break;

    case ADT_NX_SENDDATA:		
        ret = trans->Sditransaction_set(ptpInfo, (char **)data, dataSize);
        TRACE ("Send Data to DSC : size [%d]\n", adjReq[4]);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
        break;
    }

    trans->SetTransTimeout(Timeout);
    

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
	ret = ptpInfo->Response.Code;
	ret = (ret == 0x2001) ? SDI_ERR_OK : ret;

    return ret;
}

int PTPHOSTOperationHandler::SdiSetDevPropValue(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes, 
											int nType,
											SdiUInt nValue)
{
    PTPContainerType* req = &(ptpInfo->Request);

    //req->Code = PTP_OC_SetDevicePropValue;
	req->Code = PTP_OC_SAMSUNG_SETDEVICEPROPVALUE;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;

    memcpy (&req->Param1, adjReq, sizeof(SdiUInt32) * 5);
    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_SENDDATA;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(30000);

	char data[1][1][50];
	int nSize = 0;

	switch(nType)
	{
	case PTP_VALUE_INT_8:
	case PTP_VALUE_UINT_8:
		data[0][0][0]= nValue;
		data[0][0][1]= NULL;
		nSize = 1;
		break;

	case PTP_VALUE_UINT_16:
		data[0][0][0]= nValue%0x100; 
		data[0][0][1] = nValue/0x100; 
		data[0][0][2] = NULL;
		nSize = 2;
		break;

	case PTP_VALUE_UINT_32:
		data[0][0][0]= nValue%0x100; 
		data[0][0][1] = nValue/0x100; 
		data[0][0][2] = nValue/0x10000; 
		data[0][0][3] = nValue/0x1000000; 	
		data[0][0][4] = NULL;
		nSize = 4;
		break;
	}

    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    case ADT_NX_SENDDATA:		
        ret = trans->Sditransaction_set(ptpInfo, (char **)data, nSize);//strlen(data[0][0]));
        TRACE ("Send Data to DSC : size [%d]\n", adjReq[4]);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
        break;
    }

    trans->SetTransTimeout(Timeout);

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
	ret = ptpInfo->Response.Code;

	return 0;
}

int PTPHOSTOperationHandler::SdiSetDevPropValue(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes,
											CString strValue)
{
//	LPWSTR pwszContext = (LPWSTR)(LPCSTR)(LPCTSTR)strValue;

    PTPContainerType* req = &(ptpInfo->Request);

    //req->Code = PTP_OC_SetDevicePropValue;
	req->Code = PTP_OC_SAMSUNG_SETDEVICEPROPVALUE;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;

    memcpy (&req->Param1, adjReq, sizeof(SdiUInt32) * 5);
    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_SENDDATA;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(30000);
	char temp[1][1][50]={0x00,};
	char data[1][1][50]; // = {"4896x3264"};
	sprintf(data[0][0],"%S",strValue);
	for(int i=0;i<strlen(data[0][0]);i++)
	{
		temp[0][0][i*2+1]=data[0][0][i];
	}
	temp[0][0][0] = strlen(data[0][0]);
	dataSize = (strlen(data[0][0])+1)*2+1;


    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    case ADT_NX_SENDDATA:		
        ret = trans->Sditransaction_set(ptpInfo, (char **)temp, dataSize);
        TRACE ("Send Data to DSC : size [%d]\n", adjReq[4]);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
    }

    trans->SetTransTimeout(Timeout);

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
	ret = ptpInfo->Response.Code;

	return 0;
}

int
PTPHOSTOperationHandler::SetFocusPosition(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes)
{
    PTPContainerType* req = &(ptpInfo->Request);

    req->Code = PTP_OC_SAMSUNG_SetFocusPosition;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;

    memcpy (&req->Param1, adjReq, sizeof(SdiUInt32) * 5);
    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_NODATA;;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(30000);

    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
        break;
    }

    trans->SetTransTimeout(Timeout);
    

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
	ret = ptpInfo->Response.Code;

    return ret;
}

// 2011-07-9 	chlee 
int
PTPHOSTOperationHandler::SdiSendLiveview(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes,
                                            SdiUInt8 **data, SdiUInt32* size)
{
	//-- 2013-03-07 hongsu.jung
	if(!trans)
		return SDI_ERR_ACCESS_DENIED;

    PTPContainerType* req = &(ptpInfo->Request);

    req->Code = PTP_OC_SAMSUNG_GetLiveviewImg;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;
	req->Param1 = 0;						//  AF_TIME;  chlee

    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_GETDATA;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(3000);               // timeout
	
    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    case ADT_NX_GETDATA:			
        ret = trans->Sditransaction_get(ptpInfo, (char **)data, dataSize);
        TRACE ("Get Data from DSC : size [%d]\n", dataSize);
		*size = dataSize;
        break;

    case ADT_NX_SENDDATA:		
        ret = trans->Sditransaction_set(ptpInfo, (char **)data, dataSize);
        TRACE ("Send Data to DSC : size [%d]\n", adjReq[4]);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
        break;
    }

    trans->SetTransTimeout(Timeout);
    

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
	ret = ptpInfo->Response.Code;

	if(ret == PTP_RC_OK) return SDI_ERR_OK;
    else return ret;
}

int
PTPHOSTOperationHandler::SdiSetFocusPosition(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes,
                                            SdiUInt8** data,
											SdiUInt32& size)
{
    
    PTPContainerType* req = &(ptpInfo->Request);

    req->Code = PTP_OC_SAMSUNG_GetFocusPosition;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;

//	req->Param1 = PTP_CODE_SAMSUNG_UNIQUEID ;
//	req->Param1 = 0x0000d81c;

    memcpy (&req->Param1, adjReq, sizeof(SdiUInt32) * 5);
    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_GETDATA;;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(30000);

    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    case ADT_NX_GETDATA:			
        ret = trans->Sditransaction_get(ptpInfo, (char **)data, dataSize);
        TRACE ("Get Data from DSC : size [%d]\n", dataSize);
		size = dataSize;
        break;

    case ADT_NX_SENDDATA:		
        ret = trans->Sditransaction_set(ptpInfo, (char **)data, dataSize);
        TRACE ("Send Data to DSC : size [%d]\n", adjReq[4]);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
        break;
    }

    trans->SetTransTimeout(Timeout);
    

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
	ret = ptpInfo->Response.Code;

	if(ret==PTP_RC_OK) return SDI_ERR_OK;
    else return ret;
}

int
PTPHOSTOperationHandler::SdiSendCheckEvent(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes,
                                            SdiUInt8** data,
											SdiUInt32& size)
{
    
    PTPContainerType* req = &(ptpInfo->Request);

    req->Code = PTP_OC_SAMSUNG_CheckEvent;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;


    memcpy (&req->Param1, adjReq, sizeof(SdiUInt32) * 5);
    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_NODATA;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(30000);

    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    case ADT_NX_GETDATA:			
        ret = trans->Sditransaction_get(ptpInfo, (char **)data, dataSize);
        TRACE ("Get Data from DSC : size [%d]\n", dataSize);
		size = dataSize;
        break;

    case ADT_NX_SENDDATA:		
        ret = trans->Sditransaction_set(ptpInfo, (char **)data, dataSize);
        TRACE ("Send Data to DSC : size [%d]\n", adjReq[4]);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
        break;
    }

    trans->SetTransTimeout(Timeout);
    

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
	ret = ptpInfo->Response.Code;

	if(ret == PTP_RC_OK) return SDI_ERR_OK;
    else return ret;
}

int
PTPHOSTOperationHandler::SdiGetEvent(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes,
                                            SdiUInt8** data,
											SdiUInt32& size)
{
    
    PTPContainerType* req = &(ptpInfo->Request);

    req->Code = PTP_OC_SAMSUNG_GetEvent;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;

    memcpy (&req->Param1, adjReq, sizeof(SdiUInt32) * 5);
    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_GETDATA;;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(30000);

    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    case ADT_NX_GETDATA:			
        ret = trans->Sditransaction_get(ptpInfo, (char **)data, dataSize);
        TRACE ("Get Data from DSC : size [%d]\n", dataSize);
		size = dataSize;
        break;

    case ADT_NX_SENDDATA:		
        ret = trans->Sditransaction_set(ptpInfo, (char **)data, dataSize);
        TRACE ("Send Data to DSC : size [%d]\n", adjReq[4]);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
        break;
    }

    trans->SetTransTimeout(Timeout);
    

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
	ret = ptpInfo->Response.Code;

	if(ret == PTP_RC_OK) return SDI_ERR_OK;
    else return ret;
}

int
PTPHOSTOperationHandler::SdiRequireCaptureExec(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes)
{
    PTPContainerType* req = &(ptpInfo->Request);

    req->Code = PTP_OC_SAMSUNG_Capture;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;

    memcpy (&req->Param1, adjReq, sizeof(SdiUInt32) * 5);
    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_NODATA;;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(30000);

    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
        break;
    }

    trans->SetTransTimeout(Timeout);
    

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
	ret = ptpInfo->Response.Code;
	
	ret = (ret == PTP_RC_OK) ? SDI_ERR_OK : ret;			//-- 2011.7.27 chlee  SDI_ERR_OK ADD

    return ret;
}

int
PTPHOSTOperationHandler::SdiCaptureCompleteExec(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes,
                                            SdiUInt8** data,
											SdiUInt32& size)
{
    
    PTPContainerType* req = &(ptpInfo->Request);

    req->Code = PTP_OC_SAMSUNG_GetObjectInMemory;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;

//	req->Param1 = PTP_CODE_SAMSUNG_UNIQUEID ;
//	req->Param1 = 0x0000d81c;

    memcpy (&req->Param1, adjReq, sizeof(SdiUInt32) * 5);
    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_GETDATA;;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(30000);

    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    case ADT_NX_GETDATA:			
        ret = trans->Sditransaction_get(ptpInfo, (char **)data, dataSize);
        TRACE ("Get Data from DSC : size [%d]\n", dataSize);
		size = dataSize;
        break;

    case ADT_NX_SENDDATA:		
        ret = trans->Sditransaction_set(ptpInfo, (char **)data, dataSize);
        TRACE ("Send Data to DSC : size [%d]\n", adjReq[4]);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
        break;
    }

    trans->SetTransTimeout(Timeout);    

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
	ret = ptpInfo->Response.Code;

    return ret;
}

int
PTPHOSTOperationHandler::SdiMovieCompleteExec(PTPInfoType* ptpInfo,
                                            PTPTransport* trans,
                                            SdiUInt32* adjReq,
                                            SdiUInt32* adjRes,
                                            SdiUInt8** data,
											SdiUInt32& size)
{
    
    PTPContainerType* req = &(ptpInfo->Request);

    req->Code = PTP_OC_SAMSUNG_GetObjectInSDCard;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;

//	req->Param1 = PTP_CODE_SAMSUNG_UNIQUEID ;
//	req->Param1 = 0x0000d81c;

    memcpy (&req->Param1, adjReq, sizeof(SdiUInt32) * 5);
    req->Nparam = 1;

    SdiUInt32 dataSize = req->Param5;

    SdiUInt16 nEventID = ADT_NX_GETDATA;;

    int ret = PTPErrNone;
    
    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(30000);

    switch(nEventID & ADT_NX_DATA_MASK)
    {
    case ADT_NX_NODATA:
        ret = trans->Sditransaction_none(ptpInfo);
        break;

    case ADT_NX_GETDATA:			
        ret = trans->Sditransaction_get(ptpInfo, (char **)data, dataSize);
        TRACE ("Get Data from DSC : size [%d]\n", dataSize);
		size = dataSize;
        break;

    case ADT_NX_SENDDATA:		
        ret = trans->Sditransaction_set(ptpInfo, (char **)data, dataSize);
        TRACE ("Send Data to DSC : size [%d]\n", adjReq[4]);
        break;

    default:
        TRACE ("ERROR : Invalid Data Transaction Parameter \n");
        return PTP_RC_InvalidParameter;
        break;
    }

    trans->SetTransTimeout(Timeout);
    

    PTPContainerType  *ptpRes = &ptpInfo->Response;

    memcpy(adjRes, ptpRes, sizeof(SdiUInt32) * 3);
    
	ret = ptpInfo->Response.Code;

    return ret;
}

int
PTPHOSTOperationHandler::SdiReset(PTPInfoType* ptpInfo, PTPTransport* trans)
{
	int ret;
    
    SdiUInt16 respCode = PTP_RC_OK;

    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(100);
    ret = SdicloseSession(ptpInfo, trans);
    trans->SetTransTimeout(Timeout);
    

    PTPContainerType* req = &(ptpInfo->Request);

    PRINTD0("---< Command reset >---\n");

    /* save sesseion ID in context */
    req->Code = PTP_OC_SAMSUNG_Reset;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;
    req->Nparam = 0;

    ret = trans->Sditransaction_none(ptpInfo);

	if(ptpInfo->Response.Code==PTP_RC_OK) return SDI_ERR_OK;
    return ptpInfo->Response.Code;
}

//dh0.seo 2014-11-04
int
	PTPHOSTOperationHandler::SdiSensorCleaning(PTPInfoType* ptpInfo, PTPTransport* trans)
{
	int ret;

	SdiUInt16 respCode = PTP_RC_OK;

	int Timeout = trans->GetTransTimeout();
	trans->SetTransTimeout(100);
	ret = SdicloseSession(ptpInfo, trans);
	trans->SetTransTimeout(Timeout);


	PTPContainerType* req = &(ptpInfo->Request);

	PRINTD0("---< Command reset >---\n");

	/* save sesseion ID in context */
	req->Code = PTP_OC_SAMSUNG_SensorCleaning;
	req->TransactionID = nextTransactionID();    
	req->SessionID = ulSessionID;
	req->Nparam = 0;

	ret = trans->Sditransaction_none(ptpInfo);

	if(ptpInfo->Response.Code==PTP_RC_OK) return SDI_ERR_OK;
	return ptpInfo->Response.Code;
}

//CMiLRe 20141113 IntervalCapture Stop �߰�
int	PTPHOSTOperationHandler::SdiIntervalCaptureStop(PTPInfoType* ptpInfo, PTPTransport* trans)
{
	int ret;

	SdiUInt16 respCode = PTP_RC_OK;

	int Timeout = trans->GetTransTimeout();
	trans->SetTransTimeout(100);
	ret = SdicloseSession(ptpInfo, trans);
	trans->SetTransTimeout(Timeout);


	PTPContainerType* req = &(ptpInfo->Request);

	PRINTD0("---< Command reset >---\n");

	/* save sesseion ID in context */
	req->Code = PTP_OC_SAMSUNG_IntervalCaptureStop;
	req->TransactionID = nextTransactionID();    
	req->SessionID = ulSessionID;
	req->Nparam = 0;

	ret = trans->Sditransaction_none(ptpInfo);

	if(ptpInfo->Response.Code==PTP_RC_OK) return SDI_ERR_OK;
	return ptpInfo->Response.Code;
}

//CMiLRe 20141127 Display Save Mode ���� Wake up
int	PTPHOSTOperationHandler::SdiDisplaySaveModeWakeUp(PTPInfoType* ptpInfo, PTPTransport* trans)
{
	int ret;

	SdiUInt16 respCode = PTP_RC_OK;

	int Timeout = trans->GetTransTimeout();
	trans->SetTransTimeout(100);
	ret = SdicloseSession(ptpInfo, trans);
	trans->SetTransTimeout(Timeout);


	PTPContainerType* req = &(ptpInfo->Request);

	PRINTD0("---< Command reset >---\n");

	/* save sesseion ID in context */
	req->Code = PTP_OC_SAMSUNG_DISPLAY_SAVE_MODE_WAKE_UP;
	req->TransactionID = nextTransactionID();    
	req->SessionID = ulSessionID;
	req->Nparam = 0;

	ret = trans->Sditransaction_none(ptpInfo);

	if(ptpInfo->Response.Code==PTP_RC_OK) return SDI_ERR_OK;
	return ptpInfo->Response.Code;
}

int 
PTPHOSTOperationHandler::SdiFormat(PTPInfoType* ptpInfo, PTPTransport* trans){
	
	int ret;
    
    SdiUInt16 respCode = PTP_RC_OK;

    int Timeout = trans->GetTransTimeout();
    trans->SetTransTimeout(100);
    ret = SdicloseSession(ptpInfo, trans);
    trans->SetTransTimeout(Timeout);
    

    PTPContainerType* req = &(ptpInfo->Request);

    PRINTD0("---< Command reset >---\n");

    /* save sesseion ID in context */
    req->Code = PTP_OC_SAMSUNG_Format;
    req->TransactionID = nextTransactionID();    
    req->SessionID = ulSessionID;
    req->Nparam = 0;

    ret = trans->Sditransaction_none(ptpInfo);

	if(ptpInfo->Response.Code==PTP_RC_OK) return SDI_ERR_OK;
    return ptpInfo->Response.Code;
}