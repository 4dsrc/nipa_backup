/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 * 
 */
/*!
 \file PTP_Misc.cpp
 \brief Implement PTP Misc Class handling directory, file, and so on.
 \date 2009-06-11
 \version 0.70 
*/

#include "stdafx.h"


/* For FileManagement */
#include <windows.h>
#include <io.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>	//_stat sturcture
#include <errno.h>
#include <direct.h>
#include <stdlib.h>
#include "PTP_Misc.h"


//#using <mscorlib.dll>

//using namespace System;
//using namespace System::IO;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/* System Options */
//#define SUPPORT_PROC_FS

int
PTPMisc::checkMount(char* devName, char* dirName)
{
#ifdef SUPPORT_PROC_FS
	#include <mntent.h>
	
	file_t* mounttb;

	if (devName == NULL)
		return PTPErrBadParam;
	
	mounttb = setmntent("/proc/mounts", "r");
	
	if(mounttb) 
	{ 
		struct mntent *mountEnt;
		
		while((mountEnt = getmntent(mounttb)) != NULL)
		{ 
			if(strcmp((char* )mountEnt->mnt_fsname, devName) == 0)
				return PTPErrNone;
		}
	}

	return PTPErrNotMount;
	
#else

	dir_t* dp = NULL;
	dirent_t* denp = NULL;

	int count;

	if (dirName == NULL)
		return PTPErrBadParam;

	dp = PTPMisc::openDir(dirName);

	if (dp == NULL)
		return PTPErrNotMount;

	count = 0;
	
	while (count <= 3)
	{
		denp = PTPMisc::readDir(dp);

		if (denp != NULL)
			count++;
		else
			break;
	}

	PTPMisc::closeDir(dp);

	if (count < 3)
		return PTPErrNotMount;

	return PTPErrNone;
#endif
}

dir_t *
PTPMisc::openDir(char* dirName)
{
	WIN32_FIND_DATA FindFileData;

	if (dirName == NULL)
		return NULL;


	//return (dir_t *)opendir(dirName);
	//return (dir_t *) CreateDirectory((LPCWSTR) dirName,(LPSECURITY_ATTRIBUTES)"r+");
	//rujin 11.17
	//return (dir_t *) FindFirstFile((LPCWSTR) dirName, &FindFileData);	
	return (dir_t *) FindFirstFile((LPCWSTR) dirName, &FindFileData);
}

void
PTPMisc::closeDir(dir_t* dp)
{
	if (dp != NULL)
		//closedir(dp);
		FindClose(dp);

	return;
}

int
PTPMisc::removeDir(char* dirName)
{
	if (rmdir(dirName) != 0)
		return PTPErrCannotRemoveDir;
		
	return PTPErrNone;
}

dirent_t *
PTPMisc::readDir(dir_t* dp)
{
#ifdef __WIN_SIMUL__
	return NULL;
#else
	if (dp == NULL)
		return NULL;
	
	return (dirent_t *)readdir(dp);
#endif	
}

file_t *
PTPMisc::openFile(char* path, char* mode)
{
/* 
 * mode string
 *
 * r or rb : Open file for reading. 
 * w or wb : Truncate to zero length or create file for writing. 
 * a or ab : Append; open or create file for writing at end-of-file. 
 * r+ or rb+ or r+b : Open file for update (reading and writing). 
 * w+ or wb+ or w+b : Truncate to zero length or create file for update. 
 * a+ or ab+ or a+b : Append; open or create file for update, writing at end-of-file. 
 */
	if (path == NULL)
		return NULL;

	if (mode == NULL)
		return (file_t *)fopen(path, "r+");

	return (file_t *)fopen(path, mode);
}

void
PTPMisc::closeFile(file_t* fp)
{
	if(fp != NULL)
		fclose(fp);

	return;
}

int
PTPMisc::removeFile(char* path)
{
	if (remove(path) != 0)
		return PTPErrCannotRemoveFile;

	return PTPErrNone;
	
}

int
PTPMisc::readFile(file_t* fp, char* buffer, int size)
{
	int	n = 0;

	n = fread(buffer, size, 1, fp);

	if (n == 0)
	{
		return PTPErrReadFileNone;
	}
	else if (n < (-1))
	{
		return PTPErrReadFile;
	}
	
	return PTPErrNone;
}

int
PTPMisc::writeFile(file_t* fp, char* data, int size)
{
	int	n = 0;

	n = fwrite(data, size, 1, fp);
	
	if (n == 0)
	{
		return PTPErrWriteFileNone;
	}
	else if (n < (-1))
	{
		return PTPErrWriteFile;
	}
	
	return PTPErrNone;
}


int
PTPMisc::seekFile(file_t* fp, SdiUInt32 offset)
{
	if (fp == NULL)
		return PTPErrBadParam;
	
	if (fseek(fp, offset, SEEK_SET) != 0)
		return PTPErrSeekFile;

	return PTPErrNone;
}

void
PTPMisc::getStat(char* path, PTPObjectStatType* objStat)
{
	stat_t statBuf = {0,};
	PTTime localtm;

	if ((path == NULL) || (objStat == NULL))
	{
		PRINTD0("-?>path or objstat are NULL.\n");
		return;
	}
	
	//if (stat(path, &statBuf) < 0)
	if (_stat (path, &statBuf) < 0)

	{
		PRINTD0("-?>Cannot get state information of the file.\n");
		
		objStat->Attr = PTP_OBJ_ATTR_NONE;
		objStat->Mode = PTP_OBJ_MODE_NONE;
		objStat->Size = 0;

		changeToPTPDateTime(&(objStat->Ctime), NULL);
		changeToPTPDateTime(&(objStat->Mtime), NULL);
		changeToPTPDateTime(&(objStat->Atime), NULL);
		return;
	}

	objStat->Size = (SdiUInt32)statBuf.st_size;

	/* Attribute */
	//if (S_ISDIR(statBuf.st_mode) != 0)
	if ( statBuf.st_mode & _S_IFDIR )	
	{
		objStat->Attr = PTP_OBJ_ATTR_DIR;
	}
	//else if (S_ISREG(statBuf.st_mode) != 0)
	else if ( statBuf.st_mode & _S_IFREG )
	{
		objStat->Attr = PTP_OBJ_ATTR_FILE;
	}
	else
	{
		objStat->Attr = PTP_OBJ_ATTR_NONE;
	}

	/* Protection Mode */
	//if ((statBuf.st_mode & S_IWUSR) != 0)
	if (statBuf.st_mode & _S_IWRITE)
	{
		objStat->Mode = PTP_OBJ_MODE_RDWR;	
	}
	//else if ((statBuf.st_mode & S_IRUSR) != 0)
	else if (statBuf.st_mode & _S_IREAD)
	{
		objStat->Mode = PTP_OBJ_MODE_RDONLY;
	}
	else
	{
		objStat->Mode = PTP_OBJ_MODE_NONE;
	}

	/* Date & Time */
	//localtm = localtime(&(statBuf.st_ctime));
	ConvertEpochToLocalTime(&localtm, statBuf.st_ctime, 0, false);
	changeToPTPDateTime(&(objStat->Ctime), &localtm);

#if 0
	localtm = localtime(&(statBuf.st_mtime));
	changeToPTPDateTime(&(objStat->Mtime), localtm);

	localtm = localtime(&(statBuf.st_atime));
	changeToPTPDateTime(&(objStat->Atime), localtm);
#else
	changeToPTPDateTime(&(objStat->Mtime), NULL);
	changeToPTPDateTime(&(objStat->Atime), NULL);
#endif

	return;	
}

#ifdef __WIN_SIMUL__
void PTPMisc::ConvertEpochToLocalTime(PTTime* localtime, unsigned long sec, int timezone, bool daylight)
{
	//ASSERT(localtime != NULL);

	sec += timezone * 60 + (daylight ? 3600 : 0);

	tm_struct date;
	DS1371_BinaryToDate(sec, &date);

	localtime->year   = date.tm_year + 1900;
	localtime->month  = date.tm_mon + 1;
	localtime->day    = date.tm_mday;
	localtime->hour   = date.tm_hour;
	localtime->minute = date.tm_min;
	localtime->second = date.tm_sec;

	return;
}

unsigned long DaysToMonth[13] = {
	0,31,59,90,120,151,181,212,243,273,304,334,365
};
void PTPMisc::DS1371_BinaryToDate(unsigned long binary,tm_struct *datetime)
{
	unsigned long hour;
	unsigned long day;
	unsigned long minute;
	unsigned long second;
	unsigned long month;
	unsigned long year;

	unsigned long whole_minutes;
	unsigned long whole_hours;
	unsigned long whole_days;
	unsigned long whole_days_since_1968;
	unsigned long leap_year_periods;
	unsigned long days_since_current_lyear;
	unsigned long whole_years;
	unsigned long days_since_first_of_year;
	unsigned long days_to_month;
	unsigned long day_of_week;

	whole_minutes = binary / 60;
	second = binary - (60 * whole_minutes);				// leftover seconds

	whole_hours  = whole_minutes / 60;
	minute = whole_minutes - (60 * whole_hours);		// leftover minutes

	whole_days   = whole_hours / 24;
	hour         = whole_hours - (24 * whole_days);		// leftover hours

	whole_days_since_1968 = whole_days + 365 + 366;
	leap_year_periods = whole_days_since_1968 / ((4 * 365) + 1);

	days_since_current_lyear = whole_days_since_1968 % ((4 * 365) + 1);

	// if days are after a current leap year then add a leap year period
	if ((days_since_current_lyear >= (31 + 29))) {
		leap_year_periods++;
	}
	whole_years = (whole_days_since_1968 - leap_year_periods) / 365;
	days_since_first_of_year = whole_days_since_1968 - (whole_years * 365) - leap_year_periods;

	if ((days_since_current_lyear <= 365) && (days_since_current_lyear >= 60)) {
		days_since_first_of_year++;
	}
	year = whole_years + 68;

	// setup for a search for what month it is based on how many days have past
	//   within the current year
	month = 13;
	days_to_month = 366;
	while (days_since_first_of_year < days_to_month) {
		month--;
		days_to_month = DaysToMonth[month];
		if ((month >= 2) && ((year % 4) == 0)) {
			days_to_month++;
		}
	}
	day = days_since_first_of_year - days_to_month + 1;

	day_of_week = (whole_days  + 4) % 7;

	datetime->tm_yday =
		days_since_first_of_year;		/* days since January 1 - [0,365]		*/
	datetime->tm_sec  = second;			/* seconds after the minute - [0,59]	*/
	datetime->tm_min  = minute;			/* minutes after the hour - [0,59]		*/
	datetime->tm_hour = hour;			/* hours since midnight - [0,23]		*/
	datetime->tm_mday = day;			/* day of the month - [1,31]			*/
	datetime->tm_wday = day_of_week;	/* days since Sunday - [0,6]			*/
	datetime->tm_mon  = month;			/* months since January - [0,11]		*/
	datetime->tm_year = year;			/* years since 1900						*/

	return;
}
#endif


void
PTPMisc::changeToPTPDateTime(PTPDateTimeType* toDt, PTTime* fromDt)
{
	if (toDt == NULL)
	{
		PRINTD0("-?>toDt are NULL.\n");
		return;
	}

	if (fromDt == NULL)
	{
		toDt->Year  = 0;
		toDt->Month = 0;
		toDt->Day   = 0;

		toDt->Hour = 0;
		toDt->Min  = 0;
		toDt->Sec  = 0;
	}
	else
	{
		toDt->Year  = (SdiUInt16)(fromDt->year);
		toDt->Month = (SdiUInt16)(fromDt->month);
		toDt->Day   = (SdiUInt16)(fromDt->day);

		toDt->Hour = (SdiUInt16)(fromDt->hour);
		toDt->Min  = (SdiUInt16)(fromDt->minute);
		toDt->Sec  = (SdiUInt16)(fromDt->second);
	}

	return;

}

void
PTPMisc::strncpyToUpper(char* toStr, char* fromStr, int len)
{
	int i;

	if ((toStr == NULL) || (fromStr == NULL) || (len == 0))
	{
		PRINTD0("-?>toStr or fromStr is NULL, or  len is 0.\n");
		return;
	}
	
	for (i = 0; i < len; i++)
	{
		*(toStr + i) = toupper(*(fromStr + i));
	}

	return;
}

