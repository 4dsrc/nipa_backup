/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 * 
 */
/*!
 \file PTP_EnvironmentConfig.cpp
 \brief Implement PTP Environment Configuration Class.
 \date 2009-06-11
 \version 0.70 
*/

#include "stdafx.h"

#include "PTP_EnvironmentConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

int PTPEnvironmentConfig::msPTPDebugLevel = 3;

PTPEnvironmentConfig::PTPEnvironmentConfig()
{
	memset(mCompletedCfg, 0, sizeof(int)*PTP_NUM_MANDATORY_OPT);

	mNumOfStorage = 0;
	
	PTP_STRUCT_INIT(mOptionsCfg);
	PTP_STRUCT_INIT(mDeviceCfg);

	mpStorageCfg = NULL;

	PTP_STRUCT_INIT(mTransportCfg);
}

PTPEnvironmentConfig::~PTPEnvironmentConfig()
{
	if (mpStorageCfg != NULL)
	{
		delete [] mpStorageCfg;
		//free(mpStorageCfg);
	}

	PTP_STRUCT_INIT(mOptionsCfg);
	PTP_STRUCT_INIT(mDeviceCfg);

	mpStorageCfg = NULL;

	PTP_STRUCT_INIT(mTransportCfg);
}

void
PTPEnvironmentConfig::setDebugPrintLevel(int level)
{
	msPTPDebugLevel = level;
	return;
}

void
PTPEnvironmentConfig::setOptions(SdiBool bIsOnlyImage,
								   SdiBool bIsOnlyDCIM,
								   SdiBool opt3,
								   SdiBool opt4,
								   SdiBool opt5)
{
	mOptionsCfg.IsOnlyImageFilesSupported = bIsOnlyImage;
	mOptionsCfg.IsOnlyDCIMFolderSupported = bIsOnlyDCIM;
	
	return;
}

int
PTPEnvironmentConfig::setDeviceConfig(char* model, char* manufacturer, char* version, int byteOrder)
{
	int len;

	len = strlen(manufacturer);
	len = ((PTP_BASEPATH_LEN - 1) < len) ? (PTP_BASEPATH_LEN - 1) : len;
	memset(mDeviceCfg.Manufacturer, 0, PTP_BASEPATH_LEN);
	memcpy(mDeviceCfg.Manufacturer, manufacturer, len);
	
	len = strlen(model);
	len = ((PTP_OBJECT_NAME_LEN - 1) < len) ? (PTP_OBJECT_NAME_LEN - 1) : len;
	memset(mDeviceCfg.Model, 0, PTP_OBJECT_NAME_LEN);
	memcpy(mDeviceCfg.Model, model, len);

	len = strlen(version);
	len = ((PTP_VERSION_LEN - 1) < len) ? (PTP_VERSION_LEN - 1) : len;
	memset(mDeviceCfg.Version, 0, PTP_VERSION_LEN);
	memcpy(mDeviceCfg.Version, version, len);

	if (byteOrder == 1)
		mDeviceCfg.ByteOrder = PTP_BIG_ENDIAN;
	else
		mDeviceCfg.ByteOrder = PTP_LITTLE_ENDIAN;

	mCompletedCfg[0] = 1;

	return PTPErrNone;
}

int
PTPEnvironmentConfig::setNumStorage(int num)
{
	if (num < 0)
	{
		return PTPErrBadParam;
	}
	else
	{
		mNumOfStorage = num;

//euns 0303
		if(mpStorageCfg==NULL)		
		mpStorageCfg = new StorageConfigType[mNumOfStorage];

		if (mpStorageCfg == NULL)
			return PTPErrNotEnoughMem;
		else
			memset(mpStorageCfg, 0, sizeof(StorageConfigType)*mNumOfStorage);
	}
	
	mCompletedCfg[1] = 1;
		
	return PTPErrNone;	
}

int
PTPEnvironmentConfig::setStorageConfig(int number, char* name, char* path, int type, int fsType)
{
	int len;
	int i;

	if (mNumOfStorage <= 0)
		return PTPErrNotReady;

	if ((number <= 0) || (number > mNumOfStorage))
		return PTPErrBadParam;

	i = number - 1;
	
	len = strlen(name);
	len = ((PTP_OBJECT_NAME_LEN - 1) < len) ? (PTP_OBJECT_NAME_LEN - 1) : len;
	memset(mpStorageCfg[i].Name, 0, PTP_OBJECT_NAME_LEN);
	memcpy(mpStorageCfg[i].Name, name, len);

	len = strlen(path);
	len = ((PTP_BASEPATH_LEN - 1) < len) ? (PTP_BASEPATH_LEN - 1) : len;
	memset(mpStorageCfg[i].Path, 0, PTP_BASEPATH_LEN);
	memcpy(mpStorageCfg[i].Path, path, len);

	switch (type)
	{
		case 1:
			mpStorageCfg[i].Type = PTP_ST_FixedROM;
			break;

		case 2:
			mpStorageCfg[i].Type = PTP_ST_RemovableROM;
			break;

		case 3:
			mpStorageCfg[i].Type = PTP_ST_FixedRAM;
			break;
			
		case 4:
			mpStorageCfg[i].Type = PTP_ST_RemovableRAM;
			break;

		case 0:
		default:
			mpStorageCfg[i].Type = PTP_ST_Undefined;
			break;
	}

	switch (fsType)
	{
		case 1:
			mpStorageCfg[i].FilesystemType = PTP_FST_GenericFlat;
			break;

		case 2:
			mpStorageCfg[i].FilesystemType = PTP_FST_GenericHierarchical;
			break;

		case 3:
			mpStorageCfg[i].FilesystemType = PTP_FST_DCF;
			break;

		case 0:
		default:
			mpStorageCfg[i].FilesystemType = PTP_FST_Undefined;
			break;
	}

	mCompletedCfg[2] = 1;

	return PTPErrNone;
}

int
PTPEnvironmentConfig::setStorageActive(int number, SdiBool bActive)
{
	if (mNumOfStorage <= 0)
		return PTPErrNotReady;

	if ((number <= 0) || (number > mNumOfStorage))
		return PTPErrBadParam;

	mpStorageCfg[number - 1].bIsActive = bActive;

	mCompletedCfg[3] = 1;

	return PTPErrNone;
}

int
PTPEnvironmentConfig::setTransportConfig(char* name, int type, int byteOrder)
{
	int len;
	
	len = strlen(name);
	len = ((PTP_OBJECT_NAME_LEN - 1) < len) ? (PTP_OBJECT_NAME_LEN - 1) : len;
	memset(mTransportCfg.Name, 0, PTP_OBJECT_NAME_LEN);
	memcpy(mTransportCfg.Name, name, len);

	switch (type)
	{
		case 1:
			/* FALL THROUGH */
		default:
			mTransportCfg.Type = PTP_TRANS_TYPE_USB;
			break;
	}

	if (byteOrder == 1)
		mTransportCfg.ByteOrder = PTP_BIG_ENDIAN;
	else
		mTransportCfg.ByteOrder = PTP_LITTLE_ENDIAN;

	mCompletedCfg[4] = 1;

	return PTPErrNone;
}

#ifdef __PTP_HOST__
int
PTPEnvironmentConfig::setNumThumbList(int num)
{
	if (num < 0)
	{
		return PTPErrBadParam;
	}
	else
	{
#if 1
		mNumOfThumbs = num;
#else		
		mThumbDataList.numthumbs = 0;
		
		mNumOfThumbs = num;

		mThumbDataList.thumbs = new PTPThumbData[mNumOfThumbs];

		if (mThumbDataList.thumbs == NULL)
			return PTPErrNotEnoughMem;
		else
			memset(mThumbDataList.thumbs, 0, sizeof(mThumbDataList.thumbs)*mNumOfThumbs);
#endif		
	}
	
	mCompletedCfg[5] = 1;
		
	return PTPErrNone;	
}

#else

char *
PTPEnvironmentConfig::getDeviceManufacturer(void)
{
	return (mDeviceCfg.Manufacturer);
}

char *
PTPEnvironmentConfig::getDeviceModel(void)
{
	return (mDeviceCfg.Model);
}

char *
PTPEnvironmentConfig::getDeviceVersion(void)
{
	return (mDeviceCfg.Version);
}



#endif



SdiBool
PTPEnvironmentConfig::isConfigCompleted(void)
{
	int i;

	for (i = 0; i < PTP_NUM_MANDATORY_OPT; i++)
	{
		if (mCompletedCfg[i] != 1)
			return FALSE;
	}

	return TRUE;
}

int
PTPEnvironmentConfig::getDebugPrintLevel(void)
{
	return PTPEnvironmentConfig::msPTPDebugLevel;
}

SdiBool
PTPEnvironmentConfig::getOnlyImageOption(void)
{
	return (mOptionsCfg.IsOnlyImageFilesSupported);
}

SdiBool
PTPEnvironmentConfig::getOnlyDCIMOption(void)
{
	return (mOptionsCfg.IsOnlyDCIMFolderSupported);
}



int
PTPEnvironmentConfig::getDeviceByteOrder(void)
{
	return (mDeviceCfg.ByteOrder);
}

int
PTPEnvironmentConfig::getNumStorage(void)
{
	return mNumOfStorage;
}

int
PTPEnvironmentConfig::getNumThumbs(void)
{
	return mNumOfThumbs;
}

char *
PTPEnvironmentConfig::getStorageName(int number)
{
	return (mpStorageCfg[number - 1].Name);
}

char *
PTPEnvironmentConfig::getStoragePath(int number)
{
	return (mpStorageCfg[number - 1].Path);
}

SdiUInt16
PTPEnvironmentConfig::getStorageType(int number)
{
	return (mpStorageCfg[number - 1].Type);
}

SdiUInt16
PTPEnvironmentConfig::getStorageFilesystemType(int number)
{
	return (mpStorageCfg[number - 1].FilesystemType);
}

SdiBool
PTPEnvironmentConfig::getStorageActive(int number)
{
	return (mpStorageCfg[number - 1].bIsActive);
}

char *
PTPEnvironmentConfig::getTransportName(void)
{
	return (mTransportCfg.Name);
}

int
PTPEnvironmentConfig::getTransportType(void)
{
	return (mTransportCfg.Type);
}

int
PTPEnvironmentConfig::getTransportByteOrder(void)
{
	return (mTransportCfg.ByteOrder);
}

