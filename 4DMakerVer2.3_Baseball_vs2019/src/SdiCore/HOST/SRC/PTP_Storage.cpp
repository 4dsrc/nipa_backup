/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 * 
 */
/*!
 \file PTP_Storage.cpp
 \brief Implement PTP Storage Class.
 \date 2009-06-11
 \version 0.70 
*/

#include "stdafx.h"

#include "PTP_Storage.h"
#include "PTP_Misc.h"

/* For FileManagement */
#include <windows.h>
#include <io.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>	//_stat sturcture
#include <errno.h>
#include <direct.h>
#include <stdlib.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//meteor
//#include "media.h"

PTPStorage::PTPStorage()
{
	PTP_STRUCT_INIT(mStorageList);
}

PTPStorage::~PTPStorage()
{
}

int
PTPStorage::initStorage(PTPEnvironmentConfig* env, PTPImage* image)
{
	//int ret;
	int i, len = 0;
	
	PTPStorageType* sESMs;

	if ((env == NULL) || (image == NULL))
		return PTPErrBadParam;

	mpPTPImage = image;

	mStorageList.Num = env->getNumStorage();

	if (mStorageList.Num <= 0)
		return PTPErrStorageNone;

	mStorageList.Storage = new PTPStorageType[mStorageList.Num];
	//mStorageList.Storage = (PTPStorageType *)malloc(sizeof(PTPStorageType)*mStorageList.Num);

	if (mStorageList.Storage == NULL)
		return PTPErrNotEnoughMem;
	else
		memset(mStorageList.Storage, 0, (sizeof(PTPStorageType) * mStorageList.Num));

	sESMs = mStorageList.Storage;
		
	for (i = 0; i < mStorageList.Num; i++)
	{
		len = strlen(env->getStorageName(i + 1));
		//strncpy(sESMs[i].Name, env->getStorageName(i + 1), len);
		strncpy_s(sESMs[i].Name, env->getStorageName(i + 1), len);
		
		len = strlen(env->getStoragePath(i + 1));
		//strncpy(sESMs[i].BasePath, env->getStoragePath(i + 1), len);
		strncpy_s(sESMs[i].BasePath, env->getStoragePath(i + 1), len);

		if (env->getStorageActive(i + 1))
		{
			sESMs[i].ID = PTP_MAKE_SESMID((PTP_PHYSICAL_SESMID + i), PTP_LOGICAL_SESMID);
			sESMs[i].bIsActive = TRUE;
		}
		else
		{
			PRINTD1("-?>(%d)storage is not active.\n", i+1);
			
			sESMs[i].ID = PTP_MAKE_SESMID((PTP_PHYSICAL_SESMID + i), PTP_LOGICAL_SESMID_REMOVED);
			sESMs[i].bIsActive = FALSE;
		}

		initStorageInfo(env, i);
	
		sESMs[i].NumObject = 0;
		sESMs[i].Object = NULL;
	}

	return PTPErrNone;
}

void
PTPStorage::termStorage(void)
{
	if (mStorageList.Storage)
	{	
		
		mStorageList.Storage = NULL;
		
		delete [] (mStorageList.Storage);


		//free(mStorageList.Storage);



	}

	mStorageList.Num = 0;
	
	return;
}


int
PTPStorage::getNumStorage(void)
{
	return mStorageList.Num;
}



SdiUInt32
PTPStorage::getStorageIDs(SdiUInt32 *IDs, int size)
{
	int i;
	SdiUInt32 count = 0;

	if (size < mStorageList.Num + 1)
		return PTPErrBadParam;

	for (i = 0; i < mStorageList.Num; i++)
	{
		if (PTP_IS_SESM_AVAILABLE(mStorageList.Storage[i].ID))
		{
			IDs[i] = mStorageList.Storage[i].ID;
			count++;
		}
	}

	if (count == 0)
	{
		/* virtual persistent storage */
		IDs[0] = PTP_PERSISTENT_VIRTUAL_SESMID;
		count = 1;
	}

	return count;
}



SdiUInt32
PTPStorage::setStorageIDs(SdiUInt32 *IDs, int size)
{
	int i;
	SdiUInt32 count = 0;

	mStorageList.Num = size;
	
	for (i = 0; i < mStorageList.Num; i++)
	{
		mStorageList.Storage[i].ID = IDs[i];
		count++;
	}

	if (count == 0)
	{
		/* virtual persistent storage */
		IDs[0] = PTP_PERSISTENT_VIRTUAL_SESMID;
		count = 1;
	}

	return count;
}


void
PTPStorage::initStorageInfo(PTPEnvironmentConfig* env, int index)
{
	PTPStorageInfoType *si;
		
	si = &(mStorageList.Storage[index].StorageInfo);
	
	si->StorageType = env->getStorageType(index + 1);
	si->FilesystemType = env->getStorageFilesystemType(index + 1);

	
#if 0
	si->AccessCapability	= PTP_AC_ReadWrite;
	/* 실제로 읽어서 값을 채워야 함. */
	si->MaxCapability		= (SdiUInt64)PTP_NOT_USED_SI;
	si->FreeSpaceInBytes	= (SdiUInt64)PTP_NOT_USED_SI;
	si->FreeSpaceInImages	= (SdiUInt32)PTP_NOT_USED_SI;
	
#else
	#if 1
		si->AccessCapability = PTP_AC_ReadOnly_WithObjectDeletion;
	#else
		si->AccessCapability = PTP_AC_ReadOnly;
	#endif
		
	si->MaxCapability		= (SdiUInt64)PTP_NOT_USED_SI;
	si->FreeSpaceInBytes	= (SdiUInt64)PTP_NOT_USED_SI;
	si->FreeSpaceInImages	= (SdiUInt32)PTP_NOT_USED_SI;
	
#endif

	memset((si->StorageDescription), 0, sizeof(char) * PTP_MAX_STRING_LEN);
	memset(si->VolumeLabel, 0, sizeof(char ) * PTP_MAX_STRING_LEN);
	//si->StorageDescription	= 0;
	//si->VolumeLabel			= 0;

#ifndef __PTP_NO_DEBUG__
	printStorageInfo(index, si);
#endif

	return;
}


PTPStorageType *
PTPStorage::findStorageByID(SdiUInt32 id)
{
	int i;
	PTPStorageType *foundSESM = NULL;

	for (i = 0; i < mStorageList.Num; i++)
	{
		if (mStorageList.Storage[i].ID == id)
		{
			foundSESM = &(mStorageList.Storage[i]);
			break;
		}
	}

	/*This is for PTP HOST */
	if(id == 0)
	{
		foundSESM = &(mStorageList.Storage[0]);
			
	}
	
	return foundSESM;
}

PTPStorageType *
PTPStorage::findStorageByIndex(int index)
{
	return &(mStorageList.Storage[index]);
}

int
PTPStorage::findStorageIndexByID(SdiUInt32 id)
{
	int i;
	
	for (i = 0; i < mStorageList.Num; i++)
	{
		if (mStorageList.Storage[i].ID == id)
			return i;
	}

	return -1;
}



void
PTPStorage::printStorageInfo(int index, PTPStorageInfoType* si)
{
	if (PTPEnvironmentConfig::msPTPDebugLevel <= 2)
	{
		PRINTD0("->Storage Info\n");
		PRINTD1("->Storage Name       = [%s]\n", mStorageList.Storage[index].Name);
		PRINTD1("->Storage BasePath   = [%s]\n", mStorageList.Storage[index].BasePath);
		PRINTD1("->Storage ID         = [%08lX]\n", mStorageList.Storage[index].ID);
		PRINTD1("->Is Active?         = [%04d]\n", mStorageList.Storage[index].bIsActive);
		PRINTD1("->StorageType        = [%04X]\n", si->StorageType);
		PRINTD1("->FilesystemType     = [%04X]\n", si->FilesystemType);
		PRINTD1("->AccessCapability   = [%04X]\n", si->AccessCapability);
		
		PRINTD1("->MaxCapability      = [%08lX]\n", (SdiUInt32)si->MaxCapability);
		PRINTD1("->FreeSpaceInBytes   = [%08lX]\n", (SdiUInt32)si->FreeSpaceInBytes);
		PRINTD1("->FreeSpaceInImages  = [%08lX]\n", si->FreeSpaceInImages);

		PRINTD0("->StorageDescription = [0]\n");
		PRINTD0("->VolumeLabel        = [0]\n");
	}
	return;
} 


void
PTPStorage::initObject(void)
{
	int i;
	SdiUInt32 totalCount = 0, count = 0;
	
	PTPStorageType *sESMs = mStorageList.Storage;
		
	for (i = 0; i < mStorageList.Num; i++)
	{
		if (!PTP_IS_SESM_AVAILABLE(sESMs[i].ID))
			continue;
	
		totalCount = countObject(sESMs[i].BasePath);

		PRINTD1("->total number of objects = [%ld]\n", totalCount);

		if (totalCount > 0)
		{
			sESMs[i].Object = new PTPObjectType[totalCount];
			//sESMs[i].Object = (PTPObjectType *)malloc(sizeof(PTPObjectType)*totalCount);

			if (sESMs[i].Object == NULL)
			{
				PRINTD0("-?>not enough memory for Init object.\n");
				
				sESMs[i].NumObject = 0;
				
				continue;
			}
			else
				memset(sESMs[i].Object, 0, sizeof(PTPObjectType)*totalCount);

			count = setObject(sESMs[i].BasePath, sESMs[i].Object, 0, totalCount, i);
			
			sESMs[i].NumObject = count;
		}
	}

	return;
}

void
PTPStorage::termObject(void)
{
	int i;
	PTPStorageType *sESMs = mStorageList.Storage;

	//rujin 12/2
	//if(sESMs->Name
	if (sESMs == NULL)
		return;
	
	for (i = 0; i < mStorageList.Num; i++)
	{
		if (sESMs[i].Object != NULL)
		{
			if(sESMs[i].Object->Handle == NULL)
				break;


			PRINTD1("->term objects : sESMs[%d].Object\n", i);
			
			delete [] sESMs[i].Object;
			//free(sESMs[i].Object);
			
			sESMs[i].Object = NULL;
		}

		sESMs[i].NumObject = 0;
	}

	return;
}


SdiUInt32
PTPStorage::countObject(char* basePath)
{

#ifdef __WIN_SIMUL__
	return 0;
#else
#if 1
	SdiUInt32 count = 0;
	
	dir_t * dp;
	dirent_t * denp;

	char dirName[PTP_MAX_PATH_LEN] = {0,};
	const char delimiter[] = "/";
	
	dp = PTPMisc::openDir(basePath);

	if (dp == NULL)
		return 0;

	while ((denp = PTPMisc::readDir(dp)) != NULL)
	{
		if (!strcmp((char*)denp->d_name, ".") || !strcmp((char*)denp->d_name, ".."))
			continue;

		count++;
		
		memset(dirName, 0, PTP_MAX_PATH_LEN);
		
		strcat(dirName, basePath);
		strcat(dirName, delimiter);
		strcat(dirName, (char*)denp->d_name);
		
		count += countObject(dirName);
	}

	PTPMisc::closeDir(dp);

	return count;
#else
	SdiUInt32 i, count = 0, totalCount = 0;
	char filePath[PTP_MAX_PATH_LEN] = {0,};
	char dirName[PTP_OBJECT_NAME_LEN] = {0,};
	char oldDirName[PTP_OBJECT_NAME_LEN] = {0,};

	count = pDCF->DCF_get_last_number(TOTAL_FILES);

	if (count == 0)
		return 0;
	else
		totalCount = count;

	totalCount++; // DCIM Folder

	for (i = 0; i < count; i++)
	{
		memset(filePath, 0, PTP_MAX_PATH_LEN);
		pDCF->DCF_get_file_name(i, filePath);

		memset(dirName, 0, PTP_OBJECT_NAME_LEN);
		getFileNameAndLastDirFromPath(NULL, dirName, filePath);

		if (strcmp(oldDirName, dirName) != 0)
		{
			memset(oldDirName, 0, PTP_OBJECT_NAME_LEN);
			strncpy(oldDirName, dirName, PTP_OBJECT_NAME_LEN);
			
			totalCount++;		
		}	
	}

	return totalCount;
#endif
#endif

}

SdiUInt32
PTPStorage::setObject(char* basePath,
								PTPObjectType* objs,
								SdiUInt32	offset,
								SdiUInt32	totalCount,
								int			sIndex)
{
#ifdef __WIN_SIMUL__
	return 0;
#else

#if 1
	SdiUInt32 count = 0, i = 0;
	int len = 0;
	
	dir_t * dp;
	dirent_t * denp;
	
	char dirName[PTP_MAX_PATH_LEN] = {0,};
	const char delimiter[] = "/";
	
	dp = PTPMisc::openDir(basePath);

	if (dp == NULL)
		return 0;

	while ((denp = PTPMisc::readDir(dp)) != NULL)
	{
		if (!strcmp((char *)denp->d_name, ".") || !strcmp((char *)denp->d_name, "..")) 
			continue;

		// increase count
 		count++;

 		// caculate index
		i = offset + count;

		if (i > totalCount)
		{
			PTPMisc::closeDir(dp);
			return 0;
		}
	
		/* object handle : 0x00000000 is "root directory", start - 0x00000001 */
		len = strlen((char *)denp->d_name);
		len = (len < PTP_OBJECT_NAME_LEN) ? len : PTP_OBJECT_NAME_LEN;
		strncpy(objs[i - 1].Name, (char *)denp->d_name, len);
	
		objs[i - 1].Handle = PTP_MAKE_HANDLE(sIndex, i);
		objs[i - 1].ParentHandle = PTP_MAKE_HANDLE(sIndex, offset);
		
		objs[i - 1].Format = getObjectFormatByName(objs[i - 1].Name);

		objs[i - 1].AlreadySearchedThumb = 0;

		objs[i - 1].OffsetThumbPtr = 0;
		objs[i - 1].ThumbSize = 0;
		
		objs[i - 1].ImgPixW = 0;
		objs[i - 1].ImgPixH = 0;

		if (PTPEnvironmentConfig::msPTPDebugLevel <= 3)
		{
			PRINTD1("->object[%04lx]\n", i - 1);
			PRINTD1("->name         = [%s]\n", objs[i - 1].Name);
			PRINTD1("->handle       = [%08lx]\n", objs[i - 1].Handle);
			PRINTD1("->parentHandle = [%08lx]\n", objs[i - 1].ParentHandle);
			PRINTD1("->format       = [%04x]\n", objs[i - 1].Format);
		}

 		memset(dirName, 0, PTP_MAX_PATH_LEN);

		strcat(dirName, basePath);
		strcat(dirName, delimiter);
		strcat(dirName, (char*)denp->d_name);
		 
		count += setObject(dirName, objs, i, totalCount, sIndex);

	} /* end of while */

	PTPMisc::closeDir(dp);

	return count;
#else
	SdiUInt32 i, n = 0, count = 0;
	
	char filePath[PTP_MAX_PATH_LEN] = {0,};
	
	char fileName[PTP_OBJECT_NAME_LEN] = {0,};
	char dirName[PTP_OBJECT_NAME_LEN] = {0,};
	char oldDirName[PTP_OBJECT_NAME_LEN] = {0,};

	/* object handle : 0x00000000 is "root directory", start - 0x00000001 */
	
	/* 1 : DCIM Folder */
	memset(objs[0].Name, 0, PTP_OBJECT_NAME_LEN);
	strcpy(objs[0].Name, "DCIM");
	
	objs[0].Handle = PTP_MAKE_HANDLE(sIndex, 1);
	objs[0].ParentHandle = PTP_MAKE_HANDLE(sIndex, 0);
	
	objs[0].Format = PTP_OFC_Association;

	objs[0].AlreadySearchedThumb = 0;

	objs[0].OffsetThumbPtr = 0;
	objs[0].ThumbSize = 0;
	
	objs[0].ImgPixW = 0;
	objs[0].ImgPixH = 0;

	offset = 1; //DCIM - Main Dir for Image
	
	totalCount = 1; //DCIM
	n = 1; //DCIM

	if (PTPEnvironmentConfig::msPTPDebugLevel <= 3)
	{
		PRINTD1("\n->object[%04d]\n", 0);
		PRINTD1("->name         = [%s]\n", objs[0].Name);
		PRINTD1("->handle       = [%08lX]\n", objs[0].Handle);
		PRINTD1("->parentHandle = [%08lX]\n", objs[0].ParentHandle);
		PRINTD1("->format       = [%04X]\n", objs[0].Format);
	}
	
	count = pDCF->DCF_get_last_number(TOTAL_FILES);
	totalCount += count; //All JPEG File
	
	for (i = 0; i < count; i++)
	{
		memset(filePath, 0, PTP_MAX_PATH_LEN);
		pDCF->DCF_get_file_name(i, filePath);

		memset(fileName, 0, PTP_OBJECT_NAME_LEN);
		memset(dirName, 0, PTP_OBJECT_NAME_LEN);
		getFileNameAndLastDirFromPath(fileName, dirName, filePath);

		if (strcmp(oldDirName, dirName) != 0)
		{
			memset(oldDirName, 0, PTP_OBJECT_NAME_LEN);
			strncpy(oldDirName, dirName, PTP_OBJECT_NAME_LEN);
		
			memset(objs[n].Name, 0, PTP_OBJECT_NAME_LEN);
			strncpy(objs[n].Name, dirName, PTP_OBJECT_NAME_LEN);

			objs[n].Handle = PTP_MAKE_HANDLE(sIndex, n + 1);
			objs[n].ParentHandle = PTP_MAKE_HANDLE(sIndex, 1);

			objs[n].Format = PTP_OFC_Association;

			objs[n].AlreadySearchedThumb = 0;

			objs[n].OffsetThumbPtr = 0;
			objs[n].ThumbSize = 0;
			
			objs[n].ImgPixW = 0;
			objs[n].ImgPixH = 0;

			if (PTPEnvironmentConfig::msPTPDebugLevel <= 3)
			{
				PRINTD1("\n->object[%04ld]\n", n);
				PRINTD1("->name         = [%s]\n", objs[n].Name);
				PRINTD1("->handle       = [%08lX]\n", objs[n].Handle);
				PRINTD1("->parentHandle = [%08lX]\n", objs[n].ParentHandle);
				PRINTD1("->format       = [%04X]\n", objs[n].Format);
			}
			
			offset = n + 1; //Sub Dir in DCIM
			
			totalCount++; //Sub Dir
			n++; //Sub Dir

		}

		//Normal JPEG File
		memset(objs[n].Name, 0, PTP_OBJECT_NAME_LEN);
		strcpy(objs[n].Name, fileName);

		objs[n].Handle = PTP_MAKE_HANDLE(sIndex, n + 1);
		objs[n].ParentHandle = PTP_MAKE_HANDLE(sIndex, offset);
		
		objs[n].Format = getObjectFormatByName(objs[n].Name);

		objs[n].AlreadySearchedThumb = 0;

		objs[n].OffsetThumbPtr = 0;
		objs[n].ThumbSize = 0;
		
		objs[n].ImgPixW = 0;
		objs[n].ImgPixH = 0;

		if (PTPEnvironmentConfig::msPTPDebugLevel <= 3)
		{
			PRINTD1("\n->object[%04ld]\n", n);
			PRINTD1("->name         = [%s]\n", objs[n].Name);
			PRINTD1("->handle       = [%08lX]\n", objs[n].Handle);
			PRINTD1("->parentHandle = [%08lX]\n", objs[n].ParentHandle);
			PRINTD1("->format       = [%04X]\n", objs[n].Format);
		}
		
		n++; //JPEG File
	}

	return totalCount;
#endif
#endif
}




int
PTPStorage::getFileNameAndLastDirFromPath(char* toFileName, char* toDirName, char *fromPath)
{
	char tempPath[PTP_MAX_PATH_LEN] = {0,};

	char* ptr = NULL;
	char* ptrParentName = NULL;
	char* ptrName = NULL;

	int len = 0;
	
	const char delimiter[] = "/\\";

	if ((toFileName == NULL) && (toDirName == NULL))
		return PTPErrBadParam;

	if (fromPath == NULL)
		return PTPErrBadParam;

	len = strlen(fromPath);
//	strncpy(tempPath, fromPath, len);		
	strncpy_s(tempPath, fromPath, len);		


	/* extract final name */
//	ptr = strtok(tempPath, delimiter);
	ptr = strtok(tempPath, delimiter);

	ptrName = ptr;

	while (ptr != NULL)
	{
		ptr = strtok(NULL, delimiter);

		if (ptr != NULL)
		{
			ptrParentName = ptrName;
			ptrName = ptr;
		}
	}

	if ((toFileName != NULL) && (ptrName != NULL))
	{
		len = strlen(ptrName);
		len = (len < PTP_OBJECT_NAME_LEN) ? len : PTP_OBJECT_NAME_LEN;
		
		//strncpy(toFileName, ptrName, len);
		strncpy(toFileName, ptrName, len);
	}

	if ((toDirName != NULL) && (ptrParentName != NULL))
	{
		len = strlen(ptrParentName);
		len = (len < PTP_OBJECT_NAME_LEN) ? len : PTP_OBJECT_NAME_LEN;
		
		//strncpy(toDirName, ptrParentName, len);		
		strncpy(toDirName, ptrParentName, len);		
	}

	return PTPErrNone;
}


void
PTPStorage::unpackObjectInfo(PTPObjectInfoType* oi, char* buf)
{
	SdiUInt8 len = 0, offset = 0;

	oi->StorageID			= PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_OI_POS_SESMID]);
	oi->ObjectFormat		= PTPEndian::ttod16a((SdiUInt8 *)&buf[PTP_OI_POS_ObjFormat]);
	oi->ProtectionStatus	= PTPEndian::ttod16a((SdiUInt8 *)&buf[PTP_OI_POS_ProtStat]);
	oi->ObjectCompressedSize = PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_OI_POS_ObjCompSize]);
	
	oi->ThumbFormat			= PTPEndian::ttod16a((SdiUInt8 *)&buf[PTP_OI_POS_ThumbFormat]);
	oi->ThumbCompressedSize	= PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_OI_POS_ThumbCompSize]);
	oi->ThumbPixWidth		= PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_OI_POS_ThumbPixW]);
	oi->ThumbPixHeight		= PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_OI_POS_ThumbPixH]);
	
	oi->ImagePixWidth	= PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_OI_POS_ImgPixW]);
	oi->ImagePixHeight	= PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_OI_POS_ImgPixH]);
	oi->ImageBitDepth	= PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_OI_POS_ImgBitD]);
	
	oi->ParentObject	= PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_OI_POS_ParentObj]);
	
	oi->AssociationType	= PTPEndian::ttod16a((SdiUInt8 *)&buf[PTP_OI_POS_AssType]);
	oi->AssociationDesc	= PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_OI_POS_AssDesc]);
	oi->SequenceNumber	= PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_OI_POS_SeqNum]);
	
	PTPEndian::unpackString(oi->Filename,
							buf,
							PTP_OI_POS_FileName,
							&len);
	offset = len*sizeof(SdiUInt16) + 1;

	PTPEndian::unpackString(oi->CaptureDate,
							buf,
							(SdiUInt16)(PTP_OI_POS_FileName + offset),
							&len);
	offset += len*sizeof(SdiUInt16) + 1;

#if 0
	PTPEndian::unpackString( oi->ModificationDate,
							 buf,
							 (SdiUInt16)(PTP_OI_POS_FileName + offset),
							 &len );
	offset += len*sizeof(SdiUInt16) + 1;
#endif

	oi->ModificationDate = 0;
	oi->Keywords = 0;

	return;
}


void
PTPStorage::getObjectName(PTPObjectType* obj, char* name)
{
	int len = 0;

	len = strlen(obj->Name);
	strncpy(name, obj->Name, len);
	
	return;		
}

int
PTPStorage::getObjectPath(PTPStorageType* sESM, PTPObjectType* obj, char* path)
{
	SdiUInt32 i, index = 0, parentObjIndex = 0;

	PTPObjectType* objs = NULL;
	PTPObjectType* parentObj = NULL;

	const char delimiter[] = "/";
	
	if ((sESM == NULL) || (obj == NULL) || (path == NULL))
	{
		PRINTD0("-?>bad parameter for makeing object path.\n");
		return PTPErrBadParam;
	}

	if (PTP_IS_ROOT_HANDLE(obj->ParentHandle))
	{
		strcat(path, sESM->BasePath);
		strcat(path, delimiter);
		strcat(path, obj->Name);
		return PTPErrNone;
	}

	index = PTP_MAKE_OBJINDEX_BYHANDLE(obj->Handle);
	if (index >= sESM->NumObject)
		index = sESM->NumObject - 1;
		
	parentObjIndex = PTP_MAKE_OBJINDEX_BYHANDLE(obj->ParentHandle);
	if (parentObjIndex >= sESM->NumObject)
		parentObjIndex = 0;
	
	objs = sESM->Object;

	for (i = parentObjIndex; i < index; i++)
	{
		if (objs[i].Handle == obj->ParentHandle)
		{
			parentObj = &(objs[i]);

			if (getObjectPath(sESM, parentObj, path) == PTPErrNone)
				break;
			else
				return PTPErrBadParam;			
		}
	}

	if (parentObj == NULL)
	{
		/* search from first object again */
		PRINTD0("->search parent object from first object again.\n");
		
		for (i = 0; i < index; i++)
		{
			if (objs[i].Handle == obj->ParentHandle)
			{
				parentObj = &(objs[i]);
				
				if (getObjectPath(sESM, parentObj, path) == PTPErrNone)
					break;
				else
					return PTPErrBadParam;
			}
		} /* end of for (i) */
	}

	strcat(path, delimiter);
	strcat(path, obj->Name);

	return PTPErrNone;		
}

void
PTPStorage::getObjectAssociationData(char* name, SdiUInt16 format, PTPObjectInfoType* oi)
{
	switch (format)
	{
	case PTP_OFC_Association:
		/* Have to divide AssociationType with name */
		oi->AssociationType = (SdiUInt16)PTP_AT_GenericFolder;
		break;

	/* other format */
	default:
		oi->AssociationType = 0;
		break;
	}

	/* Have to determine AssociationDesc with AssociationType or name */
	oi->AssociationDesc = 0;

	/* Have to determine SequenceNumber with AssociationType or AssociationDesc or name */
	oi->SequenceNumber = 0;

	return;
}

void
PTPStorage::getObjectDateTime(PTPDateTimeType* fromDate, SdiDateTime* toDate)
{
	sprintf(toDate, "%04d%02d%02dT%02d%02d%02d.0",
			fromDate->Year,
			fromDate->Month,
			fromDate->Day,
			fromDate->Hour,
			fromDate->Min,
			fromDate->Sec);
	return;
}

SdiUInt16
PTPStorage::getObjectFormatByName(char* name)
{
	int len = 0, delimiter = '.';
	SdiUInt16 format = 0;

	char tempName[PTP_OBJECT_NAME_LEN] = {0,};
	
	char extName[10] = {0,};
	char* extPtr = NULL;

	len = strlen(name);
	strncpy(tempName, name, len);	

	/* extract ext-name */
	extPtr = strchr(tempName, delimiter);

	if (extPtr == NULL)
	{
		/* not file, maybe folder or album */
		return (SdiUInt16)PTP_OFC_Association;
	}

	extPtr++;
	
	len = strlen(extPtr);
	PTPMisc::strncpyToUpper(extName, extPtr, len);
	
	/* determin object format */
	if (!strncmp(extName, "JPG", len) || !strncmp(extName, "JPEG", len))
	{
		format = PTP_OFC_EXIF_JPEG;
	}
	else if (!strncmp(extName, "MPEG", len) || !strncmp(extName, "MPG", len))
	{
		format = PTP_OFC_MPEG;
	}
	else if (!strncmp(extName, "DPS", len))
	{
		format = PTP_OFC_Script;
	}
	else if (!strncmp(extName, "MRK", len))
	{
		format = PTP_OFC_DPOF;
	}
	else
	{
		format = PTP_OFC_Undefined;
	}

	return format;	
}


SdiUInt32
PTPStorage::findObjectHandleByName(char* name, int mode)
{
	int i, len = 0, objNameLen = 0;
	SdiUInt32 j;
	SdiUInt16 format = 0;

	PTPStorageType* sESMs = mStorageList.Storage;
	PTPObjectType* objs = NULL;

	char uprName[PTP_OBJECT_NAME_LEN] = {0,};
	char uprObjName[PTP_OBJECT_NAME_LEN] = {0,};

	switch (mode)
	{
	case PTP_FIND_OBJECT_ASSOC:
		format = PTP_OFC_Association;
		break;

	case PTP_FIND_OBJECT_DPOF:
		format = PTP_OFC_DPOF;
		break;

	case PTP_FIND_OBJECT_JPEG:
		format = PTP_OFC_EXIF_JPEG;
		break;

	case PTP_FIND_OBJECT_UNKNOWN:
	default:
		format = 0;
		break;
	}

	len = strlen(name);
	PTPMisc::strncpyToUpper(uprName, name, len);

	for (i = 0; i < mStorageList.Num; i++)
	{
		if (!PTP_IS_SESM_AVAILABLE(sESMs[i].ID))
			continue;
	
		objs = sESMs[i].Object;
		for (j = 0; j < sESMs[i].NumObject; j++)
		{
			if ((format != 0) && (objs[j].Format != format))
				continue;

			objNameLen = strlen(objs[j].Name);
			PTPMisc::strncpyToUpper(uprObjName, objs[j].Name, objNameLen);
					
			if (!strncmp(uprName, uprObjName, len))
				return objs[j].Handle;
				
		} /* end of for (j) */
		
	} /* end of for (i) */

	/* can't find file */
	return 0;	
}

SdiUInt32
PTPStorage::findObjectHandleByPath(char* path)
{
	int i, isFoundBaseDir = 0;
	SdiUInt32 j, k;
	
	int pathLen = 0, baseDirLen = 0, len = 0, parentNameLen = 0;
	SdiUInt32 parentHandle = 0;
	
	PTPStorageType* sESMs = mStorageList.Storage;
	PTPObjectType* objs = NULL;

	char uprPath[PTP_MAX_PATH_LEN] = {0,};
	char uprBaseDir[PTP_BASEPATH_LEN] = {0,};

	char tempPath[PTP_MAX_PATH_LEN] = {0,};
	
	char uprObjName[PTP_OBJECT_NAME_LEN] = {0,};

	char* ptr = NULL;
	char* ptrParentName = NULL;
	char* ptrName = NULL;
	
	const char delimiter[] = "/\\";

	if (path == NULL)
		return 0;

	pathLen = strlen(path);
	PTPMisc::strncpyToUpper(uprPath, path, pathLen);

	for (i = 0; i < mStorageList.Num; i++)
	{
		if (!PTP_IS_SESM_AVAILABLE(sESMs[i].ID))
			continue;
	
		baseDirLen = strlen(sESMs[i].BasePath);
		PTPMisc::strncpyToUpper(uprBaseDir, sESMs[i].BasePath, baseDirLen);

		ptr = strstr(uprPath, uprBaseDir);
		
		if (ptr)
		{
			isFoundBaseDir = 1;
			strncpy(tempPath, uprPath + baseDirLen, pathLen - baseDirLen);
			break;
		}
	}

	if (isFoundBaseDir == 0)
		strncpy(tempPath, uprPath, pathLen);

	/* extract final name */
	ptr = NULL;
	
	ptr = strtok(tempPath, delimiter);
	ptrName = ptr;

	while (ptr != NULL)
	{
		ptr = strtok(NULL, delimiter);

		if (ptr != NULL)
		{
			ptrParentName = ptrName;
			ptrName = ptr;
		}
	}

	if (ptrName == NULL)
	{
		len = strlen(tempPath);

		if (len == 0)
			return 0;
		else
			ptrName = tempPath;
	}

	len = strlen(ptrName);

	if (ptrParentName != NULL)
	{
		if (!strcmp(ptrParentName, ".."))
		{
			ptrParentName = NULL;
		}
		else
		{
			parentNameLen = strlen(ptrParentName);
		}
	}
	
	for (i = 0; i < mStorageList.Num; i++)
	{
		if (!PTP_IS_SESM_AVAILABLE(sESMs[i].ID))
			continue;
	
		objs = sESMs[i].Object;
		
		if (ptrParentName != NULL)
		{
			/* find parent folder */
			for (j = 0; j < sESMs[i].NumObject; j++)
			{
				/* Object format이 폴더인지 확인 하고...*/
				
				memset(uprObjName, 0, PTP_OBJECT_NAME_LEN);
				PTPMisc::strncpyToUpper(uprObjName, objs[j].Name, parentNameLen);

				if (!strncmp(ptrParentName, uprObjName, parentNameLen))
				{
					parentHandle = objs[j].Handle;

					/* find object */
					for (k = j + 1; k < sESMs[i].NumObject; k++)
					{
						memset(uprObjName, 0, PTP_OBJECT_NAME_LEN);
						PTPMisc::strncpyToUpper(uprObjName, objs[k].Name, len);

						//if (!strncmp(name, uprObjName, len))
						if (!strncmp(ptrName, uprObjName, len))
						{
							if (objs[k].ParentHandle == parentHandle)
								return objs[k].Handle;
							else
								return 0;
						}
					} /* end of for(k) */
				}
			} /* end of for(j) */
		}
		else
		{
			/* find object directly */
			for (j = 0; j < sESMs[i].NumObject; j++)
			{
				if (PTP_IS_ROOT_HANDLE(objs[j].ParentHandle))
				{
					memset(uprObjName, 0, PTP_OBJECT_NAME_LEN);
					PTPMisc::strncpyToUpper(uprObjName, objs[j].Name, len);
					
					//if (!strncmp(name, uprObjName, len))
					if (!strncmp(ptrName, uprObjName, len))
							return objs[j].Handle;
				}
			} /* end of for(j) */			
		}
		
	} /* end of for (i) */

	return 0;
}

PTPObjectType *
PTPStorage::findObjectByHandleInSESM(PTPStorageType* sESM, SdiUInt32 handle)
{
	int i, index = 0; 
	
	PTPObjectType* objs = sESM->Object;
	PTPObjectType* foundObj = NULL;

	index = PTP_MAKE_OBJINDEX_BYHANDLE(handle);
	
	for (i = index; i < (int)sESM->NumObject; i++)
	{
		if (objs[i].Handle == handle)
		{
			foundObj = &(objs[i]);
			break;
		}
	}

	/* search from first object again */
	if (foundObj == NULL)
	{
		PRINTD0("->search from first object again.\n");
		
		for (i = 0; i < index; i++)
		{
			if (objs[i].Handle == handle)
			{
				foundObj = &(objs[i]);
				break;
			}
		}		
	}

	return foundObj;
}

PTPObjectType *
PTPStorage::findObjectByHandleInStorages(SdiUInt32 handle)
{
	int i, sIndex = 0;

	PTPStorageType* sESMs = mStorageList.Storage;
	PTPObjectType* foundObj = NULL;

	sIndex = PTP_MAKE_SESMINDEX_BY_HANDLE(handle);
	
	if (sIndex >= mStorageList.Num)
		sIndex = mStorageList.Num - 1;
		
	foundObj = findObjectByHandleInSESM(&(sESMs[sIndex]), handle);

	if (foundObj == NULL)
	{
		for (i = 0; i < mStorageList.Num; i++)
		{
			if (!PTP_IS_SESM_AVAILABLE(sESMs[i].ID))
				continue;
		
			foundObj = findObjectByHandleInSESM(&(sESMs[i]), handle);

			if (foundObj != NULL)
				break;
		} /* end of for (i) */
	}

	return foundObj;
}

SdiUInt32
PTPStorage::getTotalNumObjectHandles(SdiUInt32* array)
{
	int i;
	SdiUInt32 j, count = 0;

	PTPStorageType* sESMs = mStorageList.Storage;
	PTPObjectType* objs = NULL;

	/* return  total number of object and handle array of total object, if array isn't null */
	for (i = 0; i < mStorageList.Num; i++)
	{
		if (!PTP_IS_SESM_AVAILABLE(sESMs[i].ID))
			continue;
	
		objs = sESMs[i].Object;
	
		for (j = 0; j < sESMs[i].NumObject; j++)
		{
			/* 지워진 object 빼고... */
			if (objs[j].Handle != PTP_DELETED_HANDLE)
			{
				if (array != NULL)
					array[count] = objs[j].Handle;
				
				count++;
			}
		} /* end of for (j) */
	} /* end of for (i) */

	return count;
}

SdiUInt32
PTPStorage::getNumObjectHandlesInStorages(PTPStorageType* sESMHaveParent,
														  PTPObjectType* parentObj,
														  SdiUInt32		 objFormat,
														  SdiUInt32*      larray)
{
	int i;
	SdiUInt32 count = 0;

	PTPStorageType* sESMs = mStorageList.Storage;

	/* have to check parent object handle? */
	if (parentObj)
	{
		count = getNumObjectHandlesInSESM(sESMHaveParent, parentObj, objFormat, larray);
	}
	else
	{
		for (i = 0; i < mStorageList.Num; i++)
		{
			if (!PTP_IS_SESM_AVAILABLE(sESMs[i].ID))
				continue;
			
			count += getNumObjectHandlesInSESM(&(sESMs[i]), parentObj, objFormat, &(larray[count]));
		}
	}

	return count;
}


SdiUInt32
PTPStorage::getNumObjectHandlesInSESM(PTPStorageType* sESM,
												   PTPObjectType* parentObj,
												   SdiUInt32	  objFormat,
												   SdiUInt32* llarray)
{
	SdiUInt32 i, parentHandle = 0, count = 0;

	PTPObjectType* objs = sESM->Object;

	/* have to check parent object handle? */
	if (parentObj)
	{
		parentHandle = parentObj->Handle;
	
		if (llarray)
			llarray[count] = parentObj->Handle;
		
		count++;

		if (objFormat == 0)
		{
			/* Specific Parent Object & All Format */
			
			for (i = 0; i < sESM->NumObject; i++)
			{
				/* 지워진 object 는 parenthandle에서 걸러짐 */
				if (objs[i].ParentHandle == parentHandle)
				{
					/* if sub folder in parent object... */
					if (objs[i].Format == PTP_OFC_Association)
					{
						count += getNumObjectHandlesInAssoc(sESM, objs[i].Handle,
																		objFormat, &(llarray[count]));
						i += (count - 1);
					}
					else
					{
						if (llarray != NULL)
							llarray[count] = objs[i].Handle;

						count++;						
					}
				}
			}

			/* End : Specific Parent Object & All Format */			
		}
		else if (objFormat == PTP_DESIRE_ALL_IMAGE_FORMAT)
		{
			/* Specific Parent Object & All Image Format */
				
			for (i = 0; i < sESM->NumObject; i++)
			{
				/* 지워진 object 는 parenthandle에서 걸러짐 */
				if (objs[i].ParentHandle == parentHandle)
				{
					/* if sub folder in parent object... */
					if (objs[i].Format == PTP_OFC_Association)
					{
						count += getNumObjectHandlesInAssoc(sESM, objs[i].Handle,
																		objFormat, &(llarray[count]));
						i += (count - 1);
					}
					else if (isImageFormat(objs[i].Format) == TRUE)
					{
						if (llarray != NULL)
							llarray[count] = objs[i].Handle;

						count++;						
					}
				}
			}
			
			/* End : Specific Parent Object & All Image Format */
		}
		else
		{
			/* Specific Parent Object &  Specific Format */

			for (i = 0; i < sESM->NumObject; i++)
			{
				/* 지워진 object 는 parenthandle에서 걸러짐 */
				if (objs[i].ParentHandle == parentHandle)
				{
					/* if sub folder in parent object... */
					if (objs[i].Format == PTP_OFC_Association)
					{
						count += getNumObjectHandlesInAssoc(sESM, objs[i].Handle,
																		objFormat, &(llarray[count]));
						i += (count - 1);
					}
					else if (objs[i].Format == (SdiUInt16)objFormat)
					{
						if (llarray != NULL)
							llarray[count] = objs[i].Handle;
			
						count++;						
					}
				}
			}
			
			/* End : Specific Parent Object & Specific Format */			
		}

		return count;
	}
	else
	{
		if (objFormat == 0)
		{
			/* All Format */
			
			for (i = 0; i < sESM->NumObject; i++)
			{
				/* 지워진 object 빼고... */
				if (objs[i].Handle != PTP_DELETED_HANDLE)
				{
					if (llarray != NULL)
						llarray[count] = objs[i].Handle;
					
					count++;
				}
			}
			
			/* End : All Format */
		}
		else if (objFormat == PTP_DESIRE_ALL_IMAGE_FORMAT)
		{
			/* All Image Format */

			for (i = 0; i < sESM->NumObject; i++)
			{
				/* 지워진 object 는 image objformat에서 걸러짐 */
				if (isImageFormat(objs[i].Format) == TRUE)
				{
					if (llarray != NULL)
						llarray[count] = objs[i].Handle;
					
					count++;
				}
			}

			/* End : All Image Format */
		}
		else
		{
			/* Specific Format */

			for (i = 0; i < sESM->NumObject; i++)
			{
				/* 지워진 object 는 objformat에서 걸러짐 */
				if (objs[i].Format == (SdiUInt16)objFormat)
				{
					if (llarray != NULL)
						llarray[count] = objs[i].Handle;
					
					count++;
				}
			}

			/* End : Specific Format */
		}

		return count;	
	}

	return count;
}

SdiUInt32
PTPStorage::getNumObjectHandlesInAssoc(PTPStorageType* sESM,
													  SdiUInt32	 parentHandle,
													  SdiUInt32	 objFormat,
													  SdiUInt32*  array)
{
	SdiUInt32 i, count = 0;
	int	indexParentObj = 0;

	PTPObjectType* objs = sESM->Object;

	/* save & count parent object */
	if (array)
		array[count] = parentHandle;
		
	count++;

	indexParentObj = PTP_MAKE_OBJINDEX_BYHANDLE(parentHandle);

	if (objFormat == 0)
	{
		for (i = indexParentObj + 1; i < sESM->NumObject; i++)
		{
			/* 지워진 object 는 parenthandle에서 걸러짐 */	
			if (objs[i].ParentHandle == parentHandle)
			{
				if (objs[i].Format == PTP_OFC_Association)
				{
					count += getNumObjectHandlesInAssoc(sESM, objs[i].Handle,
														objFormat, &(array[count]));
					i += (count - 1);
				}
				else
				{
					if (array)
						array[count] = objs[i].Handle;
					
					count++;				
				}
			}
			else
			{
				if (objs[i].Handle != PTP_DELETED_HANDLE)
					break;
			}
		}
	}
	else if (objFormat == PTP_DESIRE_ALL_IMAGE_FORMAT)
	{
		for (i = indexParentObj + 1; i < sESM->NumObject; i++)
		{
			/* 지워진 object 는 parenthandle에서 걸러짐 */	
			if (objs[i].ParentHandle == parentHandle)
			{
				if (objs[i].Format == PTP_OFC_Association)
				{
					count += getNumObjectHandlesInAssoc(sESM, objs[i].Handle,
															objFormat, &(array[count]));
					i += (count - 1);
				}
				else if (isImageFormat(objs[i].Format) == TRUE)
				{
					if (array)
						array[count] = objs[i].Handle;
					
					count++;				
				}
			}
			else
			{
				if (objs[i].Handle != PTP_DELETED_HANDLE)
					break;
			}
		}
	}
	else
	{
		for (i = indexParentObj + 1; i < sESM->NumObject; i++)
		{
			/* 지워진 object 는 parenthandle에서 걸러짐 */	
			if (objs[i].ParentHandle == parentHandle)
			{
				if (objs[i].Format == PTP_OFC_Association)
				{
					count += getNumObjectHandlesInAssoc(sESM, objs[i].Handle,
															objFormat, &(array[count]));
					i += (count - 1);
				}
				else if (objs[i].Format == (SdiUInt16)objFormat)
				{
					if (array)
						array[count] = objs[i].Handle;
					
					count++;				
				}
			}
			else
			{
				if (objs[i].Handle != PTP_DELETED_HANDLE)
					break;
			}
		}	
	}
	
	return count;
}


int
PTPStorage::deleteAssociation(PTPStorageType* sESM, PTPObjectType* assocObj, int* isException)
{
	SdiUInt32 i;
	int ret, removedCount = 0;
	char path[PTP_MAX_PATH_LEN] = {0,};
	int assocObj_pos;

	PTPObjectType* objs = sESM->Object;

	assocObj_pos = PTP_MAKE_OBJINDEX_BYHANDLE(assocObj->Handle);

	for (i = assocObj_pos + 1; i < sESM->NumObject; i++)
	{
		if (objs[i].ParentHandle == assocObj->Handle)
		{
			if (objs[i].Format == PTP_OFC_Association)
			{
				removedCount += deleteAssociation(sESM, &(objs[i]), isException);

				i += (removedCount - 1);
			}
			else
			{
				memset(path, 0, PTP_MAX_PATH_LEN);
				getObjectPath(sESM, &(objs[i]), path);
				
				ret = PTPMisc::removeFile(path);

				if (ret == PTPErrNone)
				{
					markDeletedObject(&(objs[i]));
					removedCount++;
				}
				else
				{
					*isException = 1;
				}
			} /* end of else */
		}
		else
		{
			if (objs[i].Handle != PTP_DELETED_HANDLE)
			{
				memset(path, 0, PTP_MAX_PATH_LEN);
				getObjectPath(sESM, assocObj, path);
				
				ret = PTPMisc::removeDir(path);
				
				if (ret == PTPErrNone)
				{
					markDeletedObject(&(objs[i]));
					removedCount++;
				}
				else
				{
					*isException = 1;
				}
				
				return removedCount;
			}
		}
	} /* end of for (i) */

	return removedCount;
}

void
PTPStorage::markDeletedObject(PTPObjectType* obj)
{
	memset(obj->Name, 0, PTP_OBJECT_NAME_LEN);
	
	/* 0xffffffff */
	obj->Handle 	  = PTP_DELETED_HANDLE;
	obj->ParentHandle = PTP_DELETED_HANDLE;
	
	/* 0xffff */
	obj->Format = PTP_DELETED_FORMAT;

	obj->AlreadySearchedThumb = 0;

	obj->OffsetThumbPtr	= 0;
	obj->ThumbSize = 0;
	obj->ImgPixW	= 0;
	obj->ImgPixH	= 0;

	return;
}



SdiBool
PTPStorage::isImageFormat(SdiUInt16 format)
{
	if ((PTP_OFC_EXIF_JPEG <= format) && (format <= PTP_OFC_JPX))
		return TRUE;

	return FALSE;
}


#ifdef __PTP_HOST__

void
PTPStorage::unpackStorageIDs(char* buf, PTPEnvironmentConfig* mpEnvCfg )
{
	int i;
	SdiUInt8 len = 0, offset = 0;

	mStorageList.Num = PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_SID_POS_StorIDTot]);

//	mStorageList.Num = buf[PTP_SID_POS_StorIDTot];

	mpEnvCfg->setNumStorage(mStorageList.Num);

	initStorage(mpEnvCfg, mpPTPImage) ; // mpPTPImage is useless for now and  isn't initialized.
	
	for (i = 0; i < mStorageList.Num; i++)
	{
		mStorageList.Storage[i].ID = PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_SID_POS_StorageID]);
		mStorageList.Storage[i].bIsActive = TRUE;
	}
	return;
}

SdiUInt32
PTPStorage::getNumObject(int index)
{
	return mStorageList.Storage[index].NumObject;
}

SdiUInt32
PTPStorage::getObjectHandle(int index, int objectindex)
{
	return mStorageList.Storage[index].Object[objectindex].Handle;
}

void
PTPStorage::unpackStorageInfo(PTPStorageInfoType* si, char* buf)
{
	SdiUInt8 len = 0, offset = 0;

	si->StorageType			= PTPEndian::ttod16a((SdiUInt8 *)&buf[PTP_SI_POS_SESMType]);
	si->FilesystemType		= PTPEndian::ttod16a((SdiUInt8 *)&buf[PTP_SI_POS_FSType]);
	si->AccessCapability	= PTPEndian::ttod16a((SdiUInt8 *)&buf[PTP_SI_POS_AccessCap]);
//	si->MaxCapability = PTPEndian::dtot64a((SdiUInt8 *)&buf[PTP_SI_POS_MaxCap]);
	si->MaxCapability = PTPEndian::ttod64a((SdiUInt8 *)&buf[PTP_SI_POS_MaxCap]);
	
//	si->FreeSpaceInBytes			= PTPEndian::dtot64a((SdiUInt8 *)&buf[PTP_SI_POS_FreeSpcInBytes]);
	si->FreeSpaceInBytes			= PTPEndian::ttod64a((SdiUInt8 *)&buf[PTP_SI_POS_FreeSpcInBytes]);	
	si->FreeSpaceInImages	= PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_SI_POS_FreeSpcInImg]);

	//memset(&si->StorageDescription, 0, sizeof(SdiUInt8 ) * PTP_MAX_STRING_LEN);
	PTPEndian::unpackString(si->StorageDescription,buf,PTP_SI_POS_SESMDesc,&len);
	
	offset = len*sizeof(SdiUInt16) + 1;

	PTPEndian::unpackString(si->VolumeLabel,
							buf,
							(SdiUInt16)(PTP_SI_POS_SESMDesc + offset),
							&len);
	offset += len*sizeof(SdiUInt16) + 1;

	return;
}

void
PTPStorage::setStorageInfo(int index, PTPStorageInfoType* si)
{
	mStorageList.Storage[index].StorageInfo.AccessCapability = si->AccessCapability;
	mStorageList.Storage[index].StorageInfo.FilesystemType = si->FilesystemType;
	mStorageList.Storage[index].StorageInfo.FreeSpaceInBytes = si->FreeSpaceInBytes;
	mStorageList.Storage[index].StorageInfo.FreeSpaceInImages = si->FreeSpaceInImages;
	mStorageList.Storage[index].StorageInfo.MaxCapability = si->MaxCapability;
	strcpy(mStorageList.Storage[index].StorageInfo.StorageDescription, si->StorageDescription);
	//mStorageList.Storage[index].StorageInfo.StorageDescription = si->StorageDescription;
	mStorageList.Storage[index].StorageInfo.StorageType = si->StorageType;
	strcpy(mStorageList.Storage[index].StorageInfo.VolumeLabel, si->VolumeLabel);
//	mStorageList.Storage[index].StorageInfo.VolumeLabel = si->VolumeLabel;
	return;
}

/* param1(32bit)  = total number of objecthandle
	    param2(32bit)  = objecthandle.
	    param3(32bit)  = objecthandle....*/

void
PTPStorage::unpackObjectHandles(char* buf, SdiUInt32 sid)
{
	int i;
	SdiUInt32 j;
	SdiUInt8 len = 0, offset = 0;
	PTPStorageType *sESMs = mStorageList.Storage;


/* sid is PTP_DESIRE_ALL_STORAGES*/
	for(i = 0; i < mStorageList.Num; i++)	
	{
		if(mStorageList.Storage[i].ID == sid)
			break;
	}

	if((sid == PTP_DESIRE_ALL_STORAGES)&&(mStorageList.Num ==1 ))
	{
		i =0;
	}
	
	sESMs[i].NumObject = PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_SID_POS_StorIDTot]);
	if (sESMs[i].NumObject > 0)
	{
		sESMs[i].Object = new PTPObjectType[sESMs[i].NumObject];
		if (sESMs[i].Object == NULL)
		{
			PRINTD0("-?>not enough memory for Init object.\n");
			return;
		}
		else
			memset(sESMs[i].Object, 0, sizeof(PTPObjectType)*sESMs[i].NumObject);
	}

	for (j = 0; j < sESMs[i].NumObject; j++)
	{
		sESMs[i].Object[j].Handle=PTPEndian::ttod32a((SdiUInt8 *)&buf[PTP_SID_POS_StorageID*(j+1)]);
	}
	return;	
}

void
PTPStorage::setPTPObjectType(SdiUInt32 id, SdiUInt32 i, PTPObjectInfoType* sentOi)
{

	mStorageList.Storage[id].Object[i].AlreadySearchedThumb=1;//or 0 it is a flag
	mStorageList.Storage[id].Object[i].Format = sentOi->ObjectFormat;
	mStorageList.Storage[id].Object[i].ImgPixH = sentOi->ImagePixHeight;
	mStorageList.Storage[id].Object[i].ImgPixW = sentOi->ImagePixWidth;
	//mStorageList.Storage[id].Object[i].Name = sentOi->Filename;
	strcpy(mStorageList.Storage[id].Object[i].Name , sentOi->Filename);	
	mStorageList.Storage[id].Object[i].OffsetThumbPtr = 0;
	mStorageList.Storage[id].Object[i].ParentHandle = sentOi->ParentObject;
	mStorageList.Storage[id].Object[i].ThumbSize = sentOi->ThumbCompressedSize;

}

#else

void
PTPStorage::initVirtualStorageInfo(PTPStorageInfoType* si)
{
	si->StorageType			= PTP_ST_FixedRAM;
	si->FilesystemType		= PTP_FST_GenericHierarchical;
	si->AccessCapability	= PTP_AC_ReadOnly;

	si->MaxCapability		= (SdiUInt64)PTP_NOT_USED_SI;
	si->FreeSpaceInBytes	= (SdiUInt64)PTP_NOT_USED_SI;
	si->FreeSpaceInImages	= (SdiUInt32)PTP_NOT_USED_SI;

	memset((si->StorageDescription), 0, sizeof(char) * PTP_MAX_STRING_LEN);
	memset(si->VolumeLabel, 0, sizeof(char ) * PTP_MAX_STRING_LEN);

//	si->StorageDescription	= 0;
//	si->VolumeLabel			= 0;

	return;
}


void
PTPStorage::packStorageInfo(char* buf, PTPStorageInfoType* si)
{
	SdiUInt8 len = 0, offset = 0;

	PTPEndian::dtot16a((SdiUInt8 *)&buf[PTP_SI_POS_SESMType], si->StorageType);
	PTPEndian::dtot16a((SdiUInt8 *)&buf[PTP_SI_POS_FSType], si->FilesystemType);
	PTPEndian::dtot16a((SdiUInt8 *)&buf[PTP_SI_POS_AccessCap], si->AccessCapability);

	PTPEndian::dtot64a((SdiUInt8 *)&buf[PTP_SI_POS_MaxCap], si->MaxCapability);
	PTPEndian::dtot64a((SdiUInt8 *)&buf[PTP_SI_POS_FreeSpcInBytes], si->FreeSpaceInBytes);
	PTPEndian::dtot32a((SdiUInt8 *)&buf[PTP_SI_POS_FreeSpcInImg], si->FreeSpaceInImages);

	PTPEndian::packString(buf,
						  si->StorageDescription,
						  PTP_SI_POS_SESMDesc,
						  &len);
						  
	offset = len*sizeof(SdiUInt16) + 1;

	PTPEndian::packString(buf,
						  si->VolumeLabel,
						  (SdiUInt16)(PTP_SI_POS_SESMDesc + offset),
						  &len);
						  
	offset += len*sizeof(SdiUInt16) + 1;

	return;
}

SdiUInt32
PTPStorage::sizeofStorageInfo(PTPStorageInfoType* si)
{
	SdiUInt32 totalSize = 0;

	/* 
	 * Count of SdiUInt64 : 2
	 * FreeSpaceInBytes, FreeSpaceInBytes
	 */
	totalSize += sizeof(SdiUInt64)*2;

	/* 
	 * Count of SdiUInt32 : 1
	 * FreeSpaceInImages
	 */
	totalSize += sizeof(SdiUInt32)*1;
	
	/* 
	 * Count of SdiUInt16 : 3
	 * StorageType, FilesystemType, AccessCapability
	 */
	totalSize += sizeof(SdiUInt16)*3;

	/* StorageDescription : string */
	if (si->StorageDescription)
		totalSize += 1 + (strlen(si->StorageDescription) + 1)*sizeof(SdiUInt16);
	else
		totalSize += 1;
	
	/* VolumeLabel : string */
	if (si->VolumeLabel)
		totalSize += 1 + (strlen(si->VolumeLabel) + 1)*sizeof(SdiUInt16);
	else
		totalSize += 1;

	return totalSize;
}


SdiBool
PTPStorage::isStorageRemoved(PTPStorageType* sESM)
{
	int index = 0;
		
	index = findStorageIndexByID(sESM->ID);

	if (index < 0)
	{
		PRINTD1("invalid storage index:[%d].\n", index);
		return TRUE;
	}

	if (PTPMisc::checkMount(sESM->Name, sESM->BasePath) == PTPErrNotMount)
	{
		//removed
		return TRUE;
	}
	else
	{
		//not removed
		return FALSE;
	}
}

int
PTPStorage::setObjectInfo(SdiUInt32 handle, PTPFullObjectInfoType* foi)
{
	int len = 0;
	char path[PTP_MAX_PATH_LEN] = {0,};

	PTPObjectStatType objStat;
	
	PTPStorageType* foundSESM = NULL;
	PTPObjectType* foundObj = NULL;	
	PTPObjectInfoType* oi = NULL;

	foundObj = findObjectByHandleInStorages(handle);
		
	if (foundObj == NULL)
	{
		PRINTD0("-?>invalid object handle for set Object Info.\n");
		return PTPErrInvalidHandle;
	}

	foundSESM = &(mStorageList.Storage[PTP_MAKE_SESMINDEX_BY_HANDLE(foundObj->Handle)]);

	/* object handle */
	foi->Handle = handle;

	PTP_STRUCT_INIT(foi->ObjectInfo);
	oi = &(foi->ObjectInfo);

	/* object name */
	memset(oi->Filename, 0, PTP_MAX_PATH_LEN);
	len = strlen(foundObj->Name);
	
	PTPMisc::strncpyToUpper(oi->Filename, foundObj->Name, len);

	/* storage ID */
	oi->StorageID = foundSESM->ID;

	/* object format */
	oi->ObjectFormat = getObjectFormatByName(foundObj->Name);

	if (getObjectPath(foundSESM, foundObj, path) == PTPErrNone)
	{
		PTP_STRUCT_INIT(objStat);
		PTPMisc::getStat(path, &objStat);

#if 0
		/* 필요하면 나중에 푼다 */
		switch (objStat.Mode)
		{
		case PTP_OBJ_MODE_RDWR:
		case PTP_OBJ_MODE_NONE:
			oi->ProtectionStatus = PTP_PS_NoProtection;
			break;

		case PTP_OBJ_MODE_RDONLY:
		default:
			oi->ProtectionStatus = PTP_PS_ReadOnly;
			break;
		}
#endif

		/* object protection mode */
		oi->ProtectionStatus = PTP_PS_NoProtection;

		/* object size */
		oi->ObjectCompressedSize = objStat.Size;

		/* image & thumbnail info */
		if (oi->ObjectFormat == PTP_OFC_EXIF_JPEG)
		{
			if ((foundObj->AlreadySearchedThumb == 1) && (foundObj->OffsetThumbPtr != 0))
			{
				oi->ThumbFormat = PTP_OFC_EXIF_JPEG;
				oi->ThumbCompressedSize	= foundObj->ThumbSize;
				oi->ThumbPixWidth  = foundObj->ImgPixW;
				oi->ThumbPixHeight = foundObj->ImgPixH;
				oi->ImagePixWidth  = foundObj->ImgPixW;
				oi->ImagePixHeight = foundObj->ImgPixH;
				oi->ImageBitDepth  = 0;

			}
			else if ((foundObj->AlreadySearchedThumb == 1) && (foundObj->OffsetThumbPtr == 0))
			{
				oi->ThumbFormat = PTP_OFC_Undefined;
				oi->ThumbCompressedSize	= 0;
				oi->ThumbPixWidth  = 0;
				oi->ThumbPixHeight = 0;
				oi->ImagePixWidth  = 0;
				oi->ImagePixHeight = 0;
				oi->ImageBitDepth  = 0;
			}
			else
			{
				mpPTPImage->getJpegInfo(path, objStat.Size, oi, foundObj);
			}
		}
		else
		{
			oi->ThumbFormat = PTP_OFC_Undefined;
			oi->ThumbCompressedSize	= 0;
			oi->ThumbPixWidth  = 0;
			oi->ThumbPixHeight = 0;
			oi->ImagePixWidth  = 0;
			oi->ImagePixHeight = 0;
			oi->ImageBitDepth  = 0;
			
			foundObj->AlreadySearchedThumb = 1;

			foundObj->OffsetThumbPtr	 = 0;
			foundObj->ThumbSize = 0;
			foundObj->ImgPixW	 = 0;
			foundObj->ImgPixH	 = 0;	
		}
	}
	
	/* parent object handle */
	oi->ParentObject = foundObj->ParentHandle;

	/* AssociationType, AssociationDesc, SequenceNumber */
	getObjectAssociationData(oi->Filename, oi->ObjectFormat, oi);

	/* capture date */
	memset(oi->CaptureDate, 0, PTP_MAX_DATE_LEN);
	getObjectDateTime(&(objStat.Ctime), oi->CaptureDate);

#if 0
	memset(oi->ModificationDate, 0, PTP_MAX_DATE_LEN);
	getObjectDateTime(&(objStat.Mtime), oi->ModificationDate);
#endif

	oi->ModificationDate = 0;
	
	oi->Keywords = 0;	

#ifndef __PTP_NO_DEBUG__
	printObjectInfo(oi);
#endif
		
	return PTPErrNone;
}

void
PTPStorage::packObjectInfo(char* buf, PTPObjectInfoType* oi)
{
	SdiUInt8 len = 0, offset = 0;

	PTPEndian::dtot32a((SdiUInt8 *)&buf[PTP_OI_POS_SESMID],		  oi->StorageID);
	PTPEndian::dtot16a((SdiUInt8 *)&buf[PTP_OI_POS_ObjFormat],	  oi->ObjectFormat);
	PTPEndian::dtot16a((SdiUInt8 *)&buf[PTP_OI_POS_ProtStat],	  oi->ProtectionStatus);
	PTPEndian::dtot32a((SdiUInt8 *)&buf[PTP_OI_POS_ObjCompSize], oi->ObjectCompressedSize);
	
	PTPEndian::dtot16a((SdiUInt8 *)&buf[PTP_OI_POS_ThumbFormat],	oi->ThumbFormat);
	PTPEndian::dtot32a((SdiUInt8 *)&buf[PTP_OI_POS_ThumbCompSize],	oi->ThumbCompressedSize);
	PTPEndian::dtot32a((SdiUInt8 *)&buf[PTP_OI_POS_ThumbPixW],		oi->ThumbPixWidth);
	PTPEndian::dtot32a((SdiUInt8 *)&buf[PTP_OI_POS_ThumbPixH],		oi->ThumbPixHeight);
	
	PTPEndian::dtot32a((SdiUInt8 *)&buf[PTP_OI_POS_ImgPixW],	oi->ImagePixWidth);
	PTPEndian::dtot32a((SdiUInt8 *)&buf[PTP_OI_POS_ImgPixH],	oi->ImagePixHeight);
	PTPEndian::dtot32a((SdiUInt8 *)&buf[PTP_OI_POS_ImgBitD],	oi->ImageBitDepth);
	
	PTPEndian::dtot32a((SdiUInt8 *)&buf[PTP_OI_POS_ParentObj],	oi->ParentObject);
	
	PTPEndian::dtot16a((SdiUInt8 *)&buf[PTP_OI_POS_AssType],	oi->AssociationType);
	PTPEndian::dtot32a((SdiUInt8 *)&buf[PTP_OI_POS_AssDesc],	oi->AssociationDesc);
	PTPEndian::dtot32a((SdiUInt8 *)&buf[PTP_OI_POS_SeqNum],	oi->SequenceNumber);

	PTPEndian::packString(buf,
						  oi->Filename,
						  (SdiUInt16)PTP_OI_POS_FileName,
						  &len); 
	offset = len*sizeof(SdiUInt16) + 1;

	PTPEndian::packString(buf,
						  oi->CaptureDate,
						  (SdiUInt16)(PTP_OI_POS_FileName + offset),
						  &len); 
	offset += len*sizeof(SdiUInt16) + 1;
	
	PTPEndian::packString(buf,
						  oi->ModificationDate,
						  (SdiUInt16)(PTP_OI_POS_FileName + offset),
						  &len); 
	offset += len*sizeof(SdiUInt16) + 1;

	PTPEndian::packString(buf,
						  oi->Keywords,
						  (SdiUInt16)(PTP_OI_POS_FileName + offset),
						  &len); 
	offset += len*sizeof(SdiUInt16) + 1;

	return;
}

SdiUInt32
PTPStorage::sizeofObjectInfo(PTPObjectInfoType* oi)
{
	SdiUInt32 totalSize = 0;

	/* 
	 * Count of SdiUInt32 : 11
	 * StorageID, ObjectCompressedSize,
	 * ThumbCompressedSize, ThumbPixWidth, ThumbPixHeight,
	 * ImagePixWidth, ImagePixHeight, ImageBitDepth,
	 * ParentObject, AssociationDesc, SequenceNumber
	 */
	totalSize += sizeof(SdiUInt32)*11;
	
	/* 
	 * Count of SdiUInt16 : 4
	 * ObjectFormat, ProtectionStatus,
	 * ThumbFormat, AssociationType
	 */
	totalSize += sizeof(SdiUInt16)*4;

	/* Filename : string */
	if (oi->Filename)
		totalSize += 1 + (strlen(oi->Filename) + 1)*sizeof(SdiUInt16);
	else
		totalSize += 1;
	
	/* CaptureDate : string */
	if (oi->CaptureDate)
		totalSize += 1 + (strlen(oi->CaptureDate) + 1)*sizeof(SdiUInt16);
	else
		totalSize += 1;

	/* ModificationDate : string */
	if (oi->ModificationDate)
		totalSize += 1 + (strlen(oi->ModificationDate) + 1)*sizeof(SdiUInt16);
	else
		totalSize += 1;

	/* Keywords : string */
	if (oi->Keywords)
		totalSize += 1 + (strlen(oi->Keywords) + 1)*sizeof(SdiUInt16);
	else
		totalSize += 1;

	return totalSize;
}


int
PTPStorage::deleteObjectInStorages(SdiUInt32 objFormat)
{
	int i, ret;
	SdiUInt32 j;

	PTPStorageType* sESMs = mStorageList.Storage;
	PTPObjectType* objs = NULL;
	
	if (objFormat == 0)
	{
		for (i = 0; i < mStorageList.Num; i++)
		{
			if (!PTP_IS_SESM_AVAILABLE(sESMs[i].ID))
				continue;
		
			if (sESMs[i].StorageInfo.AccessCapability != PTP_AC_ReadOnly)
			{
				objs = sESMs[i].Object;

				for (j = 0; j < sESMs[i].NumObject; j++)
				{
					if (objs[j].Format != PTP_DELETED_FORMAT)
					{
						ret = deleteObject(&(sESMs[i]), &(objs[j]));

						if (ret != PTPErrNone)
							return ret;
					}
				} /* end of for (j) */
			}
		} /* end of for (i) */
	}
	else if (objFormat == PTP_DESIRE_ALL_IMAGE_FORMAT)
	{
		for (i = 0; i < mStorageList.Num; i++)
		{
			if (!PTP_IS_SESM_AVAILABLE(sESMs[i].ID))
				continue;
			
			if (sESMs[i].StorageInfo.AccessCapability != PTP_AC_ReadOnly)
			{
				objs = sESMs[i].Object;

				for (j = 0; j < sESMs[i].NumObject; j++)
				{
					if (isImageFormat(objs[j].Format) == TRUE)
					{
						ret = deleteObject(&(sESMs[i]), &(objs[j]));

						if (ret != PTPErrNone)
							return ret;
					}
				} /* end of for (j) */
			}
		} /* end of for (i) */
	}
	else
	{
		for (i = 0; i < mStorageList.Num; i++)
		{
			if (!PTP_IS_SESM_AVAILABLE(sESMs[i].ID))
				continue;
		
			if (sESMs[i].StorageInfo.AccessCapability != PTP_AC_ReadOnly)
			{
				objs = sESMs[i].Object;

				for (j = 0; j < sESMs[i].NumObject; j++)
				{
					if (objs[j].Format == (SdiUInt16)objFormat)
					{
						ret = deleteObject(&(sESMs[i]), &(objs[j]));

						if (ret != PTPErrNone)
							return ret;
					}
				} /* end of for (j) */
			}
		} /* end of for (i) */		
	}

	return PTPErrNone;
}


int
PTPStorage::deleteObject(PTPStorageType* sESM, PTPObjectType* obj)
{
	int ret, removedCount = 0, isException = 0;
	char path[PTP_MAX_PATH_LEN] = {0,};
	
	PTPObjectStatType objStat;

	getObjectPath(sESM, obj, path);

	PTP_STRUCT_INIT(objStat);
	PTPMisc::getStat(path, &objStat);

	if (objStat.Mode == PTP_OBJ_MODE_RDONLY)
	{
		PRINTD0("-?>readonly object.\n");
		return PTPErrReadOnly;		
	}

	PRINTD1("->delete object : handle[%08lx].\n", obj->Handle);

	/* req->Param1가 폴더이면, 폴더 안의 file을 먼저 지우고, 폴더를 지운다. */
	/* 폴더가 아니면, req->Param1를 지운다. */

	if (obj->Format == PTP_OFC_Association)
	{
		removedCount = deleteAssociation(sESM, obj, &isException);

		if (removedCount == 0)
		{
			PRINTD0("-?>readonly association object.\n");
			return PTPErrReadOnly;					
		}

		if (isException)
		{
			PRINTD0("-?>there is an exception in deleting associtaion.\n");
			return PTPErrPartialDeleted;				
		}
	}
	else
	{
		ret = PTPMisc::removeFile(path);
	
		if (ret == PTPErrNone)
		{
			markDeletedObject(obj);
		}
		else
		{
			PRINTD0("-?>there is error in deleting object.\n");
			return PTPErrReadOnly;	
		}
	} /* end of else */
		
	return PTPErrNone;
}

SdiBool
PTPStorage::isObjectFormatValid(SdiUInt32 format)
{
	if ( (format == PTP_OFC_Association)
		|| (format == PTP_OFC_EXIF_JPEG)
		|| (format == PTP_OFC_MPEG)
		|| (format == PTP_OFC_Script)
		|| (format == PTP_OFC_DPOF) )
	{
		return TRUE;
	}

	PRINTD0("-?>invaild object format.\n");
	
	return FALSE;
}



int
PTPStorage::makeReservedFullObjectInfo(PTPFullObjectInfoType* foi,
													SdiUInt32			sESMID,
													SdiUInt32			parentObjHandle,
													PTPObjectInfoType*  sentOi)
{
	int len = 0;
	PTPObjectInfoType* oi = &(foi->ObjectInfo);

	/* 사용가능한 스토리지를 검색 ? */
	oi->StorageID 	         = sESMID;
	
	oi->ObjectFormat         = sentOi->ObjectFormat;
	oi->ProtectionStatus     = sentOi->ProtectionStatus;
	oi->ObjectCompressedSize = sentOi->ObjectCompressedSize;

	oi->ThumbFormat          = sentOi->ThumbFormat;
	oi->ThumbCompressedSize  = sentOi->ThumbCompressedSize;
	oi->ThumbPixWidth        = sentOi->ThumbPixWidth;
	oi->ThumbPixHeight       = sentOi->ThumbPixHeight;
	
	oi->ImagePixWidth  = sentOi->ImagePixWidth;
	oi->ImagePixHeight = sentOi->ImagePixHeight;
	oi->ImageBitDepth  = sentOi->ImageBitDepth;

	/* 사용가능한 Parent Directory 검색 ? */
	oi->ParentObject    = parentObjHandle;
	
	oi->AssociationType = sentOi->AssociationType;
	oi->AssociationDesc = sentOi->AssociationDesc;
	oi->SequenceNumber  = sentOi->SequenceNumber;

	len = strlen(sentOi->Filename);
	PTPMisc::strncpyToUpper(oi->Filename, sentOi->Filename, len);

	len = strlen(sentOi->CaptureDate);
	strncpy(oi->CaptureDate, sentOi->CaptureDate, len);
	
#if 0
	len = strlen(sentOi->ModificationDate);
	strncpy(oi->ModificationDate, sentOi->ModificationDate, len);
#endif

	oi->ModificationDate = 0;
	oi->Keywords = 0;

#ifndef __WIN_SIMUL__
	
	/* Virtual File */
	if (!strcmp(oi->Filename, "HDISCVRY.DPS"))
	{
		foi->Handle = PTP_HANDLE_DPS_HDISCVRY;
	}
	else if (!strcmp(oi->Filename, "HREQUEST.DPS"))
	{
		foi->Handle = PTP_HANDLE_DPS_HREQUEST;
	}
	else if (!strcmp(oi->Filename, "HRSPONSE.DPS"))
	{
		foi->Handle = PTP_HANDLE_DPS_HRSPONSE;
	}
	else
#endif		
	{
	
#if 0	
		/* Normal Object : 사용가능한 Object handle 검색 */
		
		foi->ObjHandle = ptp_get_lastobjhandle();
		
#else		
		/* ERROR 로 처리 */
		foi->Handle = PTP_DESIRE_ALL_OBJECTS;
		return PTPErrStorageFull;   /*WJ: For Full PTP It is not necessary to treat it as error*/
#endif

	}		

	return PTPErrNone;
}


void
PTPStorage::printObjectInfo(PTPObjectInfoType* oi)
{
	if (PTPEnvironmentConfig::msPTPDebugLevel <= 3)
	{
		PRINTD0("->Object Info\n");
		PRINTD1("->StorageID            = [%08lX]\n", oi->StorageID);
		PRINTD1("->ObjectFormat         = [%04X]\n", oi->ObjectFormat);
		PRINTD1("->ProtectionStatus     = [%04X]\n", oi->ProtectionStatus);
		PRINTD1("->ObjectCompressedSize = [%08lX]\n", oi->ObjectCompressedSize);
		
		PRINTD1("->ThumbFormat          = [%04X]\n", oi->ThumbFormat);
		PRINTD1("->ThumbCompressedSize  = [%08lX]\n", oi->ThumbCompressedSize);
		PRINTD1("->ThumbPixWidth        = [%08lX]\n", oi->ThumbPixWidth);
		PRINTD1("->ThumbPixHeight       = [%08lX]\n", oi->ThumbPixHeight);
		PRINTD1("->ImagePixWidth        = [%08lX]\n", oi->ImagePixWidth);
		PRINTD1("->ImagePixHeight       = [%08lX]\n", oi->ImagePixHeight);
		PRINTD1("->ImageBitDepth        = [%08lX]\n", oi->ImageBitDepth);

		PRINTD1("->ParentObject         = [%08lX]\n", oi->ParentObject);
		PRINTD1("->AssociationType      = [%04X]\n", oi->AssociationType);
		PRINTD1("->AssociationDesc      = [%08lX]\n", oi->AssociationDesc);
		PRINTD1("->SequenceNumber       = [%08lX]\n", oi->SequenceNumber);

		PRINTD1("->Filename             = [%s]\n", oi->Filename);
		PRINTD1("->CaptureDate          = [%s]\n", oi->CaptureDate);
		PRINTD0("->ModificationDate     = [0]\n");
		PRINTD0("->Keywords             = [0]\n");
	}
	return;
}


#endif

#if 0
/* folder 안의 특정 format의 object를 삭제한다. */
int
PTPStorage::deleteObjectInAssoc(PTPStorageType* sESM,
										   PTPObjectType* assocObj,
										   SdiUInt32	      objFormat,
										   int*           isException)
{
	int i, ret, n = 0, removedCount = 0, index = 0;
	char path[PTP_MAX_PATH_LEN] = {0,};
	
	PTPObjectStatType objStat;
	PTPObjectType* objs = sESM->Object;

	if ((SdiUInt32)objFormat == PTP_OFC_Association)
	{
		PRINTD0("-?>bad object format.\n");
		return 0;
	}

	getObjectPath(sESM, assocObj, path);

	PTP_STRUCT_INIT(objStat);
	PTPMisc::getStat(path, &objStat);

	if (objStat.Mode == PTP_OBJ_MODE_RDONLY)
	{
		*isException = 1;
		
		PRINTD0("-?>read only object.\n");
		return 0;		
	}

	index = PTP_MAKE_OBJINDEX_BYHANDLE(assocObj->Handle);
	
	for (i = index + 1; i < sESM->NumObject; i++)
	{
		if (objs[i].parentHandle == assocObj->Handle)
		{
			if (objs[i].Format == (SdiUInt16)objFormat) 
			{
				/* delete object */
				memset(path, 0, PTP_MAX_PATH_LEN);
				getObjectPath(sESM, &(objs[i]), path);
				
				ret = PTPMisc::removeFile(path);

				if (ret == PTPErrNone)
				{
					markDeletedObject(&(objs[i]));
					removedCount++;
				}
				else
				{
					*isException = 1;
				}
			}
			else if (objs[i].Format == PTP_OFC_Association)
			{
				removedCount += deleteObjectInAssoc(sESM, &(objs[i]), objFormat, isException);
				
				i += (removedCount - 1);
			} /* end of else if */
		}
		else
		{
			if (objs[i].Handle != PTP_DELETED_HANDLE)
				return removedCount;
		}
	} /* end of for (i) */

	return removedCount;
}
#endif

