/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 * 
 */
/*!
 \file PTPHOST.cpp
 \brief Implement PTPHOST Class.
 \date 2009-06-11
 \version 0.70 
*/

#include "stdafx.h"

#include "PTP_HOST.h"
#include "PTP_Misc.h"

#include <windows.h>
#include <io.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>	
#include <errno.h>
#include <direct.h>
#include <stdlib.h>

/*
! PTP Host Class
*/

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CString Imagepath;


PTPHOST::PTPHOST()
{
	mIsCreated = FALSE;
	mIsInited = FALSE;

	m_PTPInfo.IsConnected = 0;
	m_PTPInfo.StatusForHOST = PTPHOST_STATUS_NO_ACTION;		
	m_PTPInfo.SessionStatus = PTP_SESSION_CLOSED;
	m_PTPInfo.SessionID = 0;

	PTP_STRUCT_INIT(m_PTPInfo.Request);
	PTP_STRUCT_INIT(m_PTPInfo.Response);

	memset(m_PTPInfo.VirtualFile, 0, PTP_VIRTUAL_FILE_SIZE);
	
	curmax = 0; curmin = 0;

	Imagepath="";

	PTP_STRUCT_INIT(mThumbDataList);
	array = NULL;
	
	mpEnvCfg = NULL;
//	mpImage = NULL;
	//mpDevice = NULL;
	mpStorage = NULL;
	mpTransport = NULL;
	mpHostOpHandler = NULL;
}

PTPHOST::~PTPHOST()
{
}


int
PTPHOST::create(int bStandalone)
{
	if (mIsCreated != FALSE)
	{
		PRINTD0("-?>Already created.\n");
		return PTP_ERROR_NONE;
	}

	/* create */
	mpEnvCfg = new PTPEnvironmentConfig;

	if (mpEnvCfg == NULL)
	{
		PRINTD0("-?>Cannot create PTPEnvironmentConfig class.\n");
		goto PTP_CREATE_ERROR;
	}

#if 0
	mpImage = new PTPImage;
	
	if (mpImage == NULL)
	{
		PRINTD0("-?>Cannot create PTPImageInfo class.\n");
		goto PTP_CREATE_ERROR;
	}

	/* Need to consider */
	mpDevice = new PTPDevice;
	
	if (mpDevice == NULL)
	{
		PRINTD0("-?>Cannot create PTPDevice class.\n");
		goto PTP_CREATE_ERROR;
	}
#endif
	mpStorage = new PTPStorage;

	if (mpStorage == NULL)
	{
		PRINTD0("-?>Cannot create PTPStorage class.\n");
		goto PTP_CREATE_ERROR;
	}

	mpTransport = new PTPTransport;

	if (mpTransport == NULL)
	{
		PRINTD0("-?>Cannot create PTPTransport class.\n");
		goto PTP_CREATE_ERROR;
	}

	mpHostOpHandler = new PTPHOSTOperationHandler;

	if (mpHostOpHandler == NULL)
	{
		PRINTD0("-?>Cannot create PTPOperationHandler class.\n");
		goto PTP_CREATE_ERROR;
	}

	mIsCreated = TRUE;

	return PTP_ERROR_NONE;

PTP_CREATE_ERROR:
	if (mpEnvCfg != NULL)
	{
		delete mpEnvCfg;
		mpEnvCfg = NULL;
	}
	if (mpStorage != NULL)
	{
		delete mpStorage;
		mpStorage = NULL;
	}
	
#if 0
	if (mpImage != NULL)
	{
		delete mpImage;
		mpImage = NULL;
	}


	if (mpDevice != NULL)
	{
		delete mpDevice;
		mpDevice = NULL;
	}
#endif	
	if (mpHostOpHandler != NULL)
	{
		delete mpHostOpHandler;
		mpHostOpHandler = NULL;
	}

	if (mpTransport != NULL)
	{
		delete mpTransport;
		mpTransport = NULL;
	}

	return PTP_ERROR_CREATE_INSTANCE;
}

void
PTPHOST::destroy(void)
{
	if (mIsCreated != TRUE)
	{
		PRINTD0("-?>Not created, yet.\n");
		return;
	}

	if (mpEnvCfg != NULL)
	{
		delete mpEnvCfg;
		mpEnvCfg = NULL;
	}
#if 0
	if (mpImage != NULL)
		delete mpImage;

	if (mpDevice != NULL)
		delete mpDevice;		

#endif
	if (mpStorage != NULL)
	{
		delete mpStorage;
		mpStorage = NULL;
	}

	
	if (mpHostOpHandler != NULL)
	{
		delete mpHostOpHandler;
		mpHostOpHandler = NULL;
	}

//euns 0303
	if (mThumbDataList.thumbs != NULL)
	{
		delete mThumbDataList.thumbs;
		mThumbDataList.thumbs = NULL;
	}
		
	if (mpTransport != NULL)
	{
		delete mpTransport;
		mpTransport = NULL;
	}

//euns 0303
	if(array !=NULL)
	{
		delete array;
		array = NULL;
	}

	mpEnvCfg = NULL;
#if 0
	
	mpImage = NULL;
	mpDevice = NULL;
#endif

	mpStorage = NULL;
	mpTransport = NULL;
	mpHostOpHandler = NULL;
	mIsCreated = FALSE;

	return;
}

void
PTPHOST::setDebugPrintLevel(int level)
{
	mpEnvCfg->setDebugPrintLevel(level);
	return;
}

void
PTPHOST::setOptions(unsigned char bIsOnlyImage,
				   unsigned char bIsOnlyDCIM,
				   unsigned char opt3,
				   unsigned char opt4,
				   unsigned char opt5)
{
	mpEnvCfg->setOptions(bIsOnlyImage, bIsOnlyDCIM, opt3, opt4, opt5);
	return;
}

int
PTPHOST::setDeviceConfig(char* model, char* manufacturer, char* version, int byteOrder)
{
	if (mpEnvCfg->setDeviceConfig(model, manufacturer, version, byteOrder) != PTPErrNone)
		return PTP_ERROR_SET_DEVICE_CONFIG;

	return PTP_ERROR_NONE;
}

int
PTPHOST::setNumStorage(int numStorage)
{
	if (mpEnvCfg->setNumStorage(numStorage) != PTPErrNone)
		return PTP_ERROR_SET_NUM_STORAGE;

	return PTP_ERROR_NONE;
}

int
PTPHOST::setStorageConfig(int number, char* name, char* path, int type, int fsType)
{
	if (mpEnvCfg->setStorageConfig(number, name, path, type, fsType) != PTPErrNone)
		return PTP_ERROR_SET_STORAGE_CONFIG;

	return PTP_ERROR_NONE;
}

int
PTPHOST::setStorageActive(int number, unsigned char bActive)
{
	if (mpEnvCfg->setStorageActive(number, bActive) != PTPErrNone)
		return PTP_ERROR_SET_STORAGE_ACTIVE;

	return PTP_ERROR_NONE;
}

int
PTPHOST::setTransportConfig(char* name, int type, int byteOrder)
{
	if (mpEnvCfg->setTransportConfig(name, type, byteOrder) != PTPErrNone)
		return PTP_ERROR_SET_TRANSPORT_CONFIG;

	return PTP_ERROR_NONE;
}

int
PTPHOST::setNumThumbList(int num)
{
	if (mpEnvCfg->setNumThumbList(num) != PTPErrNone)
		return PTP_ERROR_SET_TRANSPORT_CONFIG;

	return PTP_ERROR_NONE;
}


/* initialize member variables and classes. */
int
PTPHOST::initialize(void)
{
	int ret, numthumb;

/* Not sure whether host needs this or not */
#if 0
	if (mpEnvCfg->isConfigCompleted() == FALSE)
		return PTP_ERROR_NOT_READY;
#endif
	if (mIsCreated != TRUE)
	{
		PRINTD0("-?>Not created.\n");
		return PTP_ERROR_NOT_READY;
	}

	if (mIsInited != FALSE)
	{
		PRINTD0("-?>Already inited.\n");
		return PTP_ERROR_NONE;
	}


	/* Init */
	PTPEndian::Init(mpEnvCfg);

	//mpImage->Init(mpEnvCfg);

	ret = mpTransport->Init(mpEnvCfg);

	if (ret != PTPErrNone)
	{
		PRINTD0("-?>Cannot initialize Transport.\n");
		goto PTP_INITIALIZE_ERROR;
	}

	//mpHostOpHandler->Init(mpEnvCfg);

	//mpDevice->Init(mpEnvCfg);
#if 0
	if (mpStorage->initStorage(mpEnvCfg, mpImage) != PTPErrNone)
	{
		PRINTD0("-?>Cannot initialize Storage.\n");
		goto PTP_INITIALIZE_ERROR;
	}


	//move to openSession
	mpStorage->initObject();
#endif

	//rujin new
	m_PTPInfo.IsConnected = mpTransport->getConnectionStatus();
	
	if(!m_PTPInfo.IsConnected)
	{
		m_PTPInfo.IsConnected = mpTransport->getConnectionStatus();
	}

	mIsInited = TRUE;


//euns 0303	array = new SdiUInt32[10];
//euns 0303	memset(&array, 0, sizeof(SdiUInt32)* 10);


	mThumbDataList.numthumbs = 0;
	numthumb = mpEnvCfg->getNumThumbs();
	mThumbDataList.thumbs = new PTPThumbData[numthumb];

	if (mThumbDataList.thumbs == NULL)
		return PTPErrNotEnoughMem;
	else
		memset(mThumbDataList.thumbs, 0, sizeof(mThumbDataList.thumbs)*numthumb);

	return PTP_ERROR_NONE;

PTP_INITIALIZE_ERROR:
	PTPEndian::term();
#if 0
	if (mpImage != NULL)
		mpImage->term();
	
	if (mpHostOpHandler != NULL)
		;//mpHostOpHandler->term();

	if (mpDevice != NULL)
		mpDevice->term();


	if (mpTransport != NULL)
		mpTransport->term();


	if (mpStorage != NULL)
		mpStorage->termStorage();
#endif
	return PTP_ERROR_CANNOT_INIT;
}


int
PTPHOST::reInitializeStorage(void)
{
#if 0
	if (mpStorage->initStorage(mpEnvCfg, mpImage) != PTPErrNone)
	{
		PRINTD0("-?>Cannot initialize Storage.\n");
		goto PTP_INITIALIZE_ERROR;
	}

	return PTP_ERROR_NONE;

PTP_INITIALIZE_ERROR:
	if (mpStorage != NULL)
		mpStorage->termStorage();

	return PTP_ERROR_CANNOT_INIT;
#else

	return PTP_ERROR_NONE;
#endif
}


void
PTPHOST::terminate(void)
{
	if (mIsCreated != TRUE)
	{
		PRINTD0("-?>Not created.\n");
		return;
	}

	if (mIsInited != TRUE)
	{
		PRINTD0("-?>Not inited, yet.\n");
		return;
	}

	if (mpStorage != NULL)
	{
		mpStorage->termObject();

//rujin 11.18 terminate

		mpStorage->termStorage();
	}

	if (mpTransport != NULL)
		mpTransport->term();

#if 0
	if (mpDevice != NULL)
		mpDevice->term();
	
	if (mpHostOpHandler != NULL)
		;//mpHostOpHandler->term();


	if (mpImage != NULL)
		mpImage->term();
#endif
	PTPEndian::term();
	
	m_PTPInfo.IsConnected = 0;
//	m_PTPInfo.StatusForHOST = PTP_STATUS_DPS_NONACTION;
	
	m_PTPInfo.SessionStatus = PTP_SESSION_CLOSED;
	m_PTPInfo.SessionID = 0;

	PTP_STRUCT_INIT(m_PTPInfo.Request);

	memset(m_PTPInfo.VirtualFile, 0, PTP_VIRTUAL_FILE_SIZE);

	mIsInited = FALSE;
	
	return;
}

int
PTPHOST::resetStorage(void)
{
	if (mpStorage != NULL)
	{
//rujin 12.12
		//mpStorage->termObject();
		//mpStorage->termStorage();

#if 0		
		if (mpStorage->initStorage(mpEnvCfg, mpImage) != PTPErrNone)
		{
			PRINTD0("-?>Cannot initialize Storage.\n");
			return PTP_ERROR_CANNOT_RESET;
		}
#endif
//rujin 12.12

		//if (m_PTPInfo.SessionStatus == PTP_SESSION_OPENED)
		//	mpStorage->initObject();
	}

	return PTP_ERROR_NONE;
	
}

/* Depending on the PTP HOST Status, 
*/
int
PTPHOST::transaction(void)
{
	char selected[9]={0,};
	int ret = PTP_ERROR_NONE;

	/*
	1. initial connection -> display thumbnails at the first stage.
	2. user clicks the next page -> display the next thumbnails
	3. user clicks the previous page -> display the previous thumbnails.
	4. user selects several images and requests to copy the selected images.
	5. 
	*/

	switch(m_PTPInfo.StatusForHOST)
	{
		case PTPHOST_STATUS_NO_ACTION:
			break;
		case PTPHOST_STATUS_CONNECTED: // as soon as the enumeration is done.
			ret = firstConnection();
			break;
		case PTPHOST_STATUS_NEXT: // by UI
			ret = next();
			break;
		case PTPHOST_STATUS_PREVIOUS: // by UI
			ret = previous();
			break;
		case PTPHOST_STATUS_REQUEST_COPY:  // by UI
			selected[0] = 1;
			selected[1] = 1;
			selected[2] = 1;
			selected[3] = 1;
			selected[4] = 1;
			selected[5] = 1;
			selected[6] = 1;
			selected[7] = 1;
			selected[8] = 1;
			
			ret = requestcopy(selected);
			break;
		default:
			break;
								
	}
	
	return ret ;
}

int
PTPHOST::firstConnection(void)
{
	int ret=PTPErrNone, i, maxObj;
	SdiUInt32 handle;

	/* Inital Connection */
	//if (isDisconnected() == FALSE)
	{
			ret = mpHostOpHandler->getDeviceInfo(&m_PTPInfo, mpTransport);
			if (ret != PTPErrNone)
			{
				return ret;
			}

			ret = mpHostOpHandler->openSession(&m_PTPInfo, mpTransport);
			if (ret != PTPErrNone)
			{
				return ret;
			}

			ret = mpHostOpHandler->getStorageIDs(&m_PTPInfo, mpStorage, mpTransport, mpEnvCfg);

			if (ret != PTPErrNone)
			{
				return ret;
			}

#if 0
			ret = reInitializeStorage();
			if (ret != PTPErrNone)
			{
				return ret;
			}
#endif

			ret = mpHostOpHandler->getStorageInfo(&m_PTPInfo, mpStorage, mpTransport);		
			if (ret != PTPErrNone)
			{
				return ret;
			}
			
			ret = mpHostOpHandler->getObjectHandles(&m_PTPInfo, mpStorage, mpTransport);
			if (ret != PTPErrNone)
			{
				return ret;
			}
			
			ret = mpHostOpHandler->getObjectInfo(&m_PTPInfo, mpStorage, mpTransport);
			if (ret != PTPErrNone)
			{
				return ret;
			}

			array = new SdiUInt32[10];

			/* This is the initial thumbnail images display */		
			maxObj = mpStorage->getNumObjectHandlesInStorages(mpStorage->findStorageByIndex(0),
														  NULL,
														  PTP_DESIRE_ALL_IMAGE_FORMAT,
														  array);
			maxObj = mpEnvCfg->getNumThumbs();
			if(maxObj >9 )			maxObj = 9;

			mThumbDataList.numthumbs = 0;
			
			for(i=0; i< maxObj; i++)
			{
				handle = array[i];
				mThumbDataList.numthumbs++;
				ret = mpHostOpHandler->getThumb(&m_PTPInfo, mpTransport, handle, &mThumbDataList.thumbs[i]);		
				if (ret != PTPErrNone)
				{
					return ret;
				}			
			}
			curmax = maxObj;
			curmin = 0;
			ret = mpHostOpHandler->closeSession(&m_PTPInfo, mpTransport);
			if (ret != PTPErrNone)
			{
				return ret;
			}
			
		}

	return ret;

}


int
PTPHOST::next(void)
{
	int ret=PTPErrNone, i, maxObj;
	SdiUInt32 handle;

	/* Inital Connection */
	if (isDisconnected() == FALSE)
	{
		maxObj = mpStorage->getNumObject(0);
		if((maxObj - curmax)> 0)
		{
			/* Need to initialize the mThumbDataList...*/
			mThumbDataList.numthumbs=0;
			/* Need to initialize the  mThumbDataList->thumbs*/

			if((maxObj - curmax) > mpEnvCfg->getNumThumbs()) 
				maxObj = mpEnvCfg->getNumThumbs();
			else
				maxObj = (maxObj - curmax) ;
		}
		else
		{
			m_PTPInfo.StatusForHOST = PTPHOST_STATUS_NO_ACTION;
			return PTPNoData; // there is no more data
		}

		ret = mpHostOpHandler->openSession(&m_PTPInfo, mpTransport);
		if (ret != PTPErrNone)
		{
			return ret;
		}

		mThumbDataList.numthumbs =0;
		for(i=0; i<mpEnvCfg->getNumThumbs(); i++)
		{
			memset(mThumbDataList.thumbs[i].ThumbData, 0, (1024*15));					
		}
		
		for(i=curmax; i< maxObj+curmax; i++)
		{
			handle = array[i];
			ret = mpHostOpHandler->getThumb(&m_PTPInfo, mpTransport, handle, &mThumbDataList.thumbs[mThumbDataList.numthumbs]);		
			if (ret != PTPErrNone)
			{
				return ret;
			}
			mThumbDataList.numthumbs++;

		}
		curmin = curmax;
		curmax = maxObj+curmax;
		ret = mpHostOpHandler->closeSession(&m_PTPInfo, mpTransport);
		if (ret != PTPErrNone)
		{
			return ret;
		}
			
	}
	else
	{
		closeSessionByForce();	
	}
	return ret;
}


int
PTPHOST::previous(void)
{
	int ret=PTPErrNone, i, maxObj;
	SdiUInt32 handle;
	

	/* Inital Connection */
	if (isDisconnected() == FALSE)
	{
		if((curmin-mpEnvCfg->getNumThumbs())>=0)
		{
			/* Need to initialize the mThumbDataList...*/
			mThumbDataList.numthumbs=0;
			/* Need to initialize the  mThumbDataList->thumbs*/

			if(curmin >= mpEnvCfg->getNumThumbs()) 
				maxObj = mpEnvCfg->getNumThumbs();
			else
				maxObj = curmin;
		}
		else
		{
			m_PTPInfo.StatusForHOST = PTPHOST_STATUS_NO_ACTION;
			return PTPNoData; // there is no more data
		}

		/*
		ret = mpHostOpHandler->openSession(&m_PTPInfo, mpTransport);
		if (ret != PTPErrNone)
		{
			return ret;
		}
*/
		mThumbDataList.numthumbs = 0;
		for(i=0; i<mpEnvCfg->getNumThumbs(); i++)
		{
			memset(mThumbDataList.thumbs[i].ThumbData, 0, (1024*15));					
		}
		
		for(i=curmin-maxObj; i< curmin; i++)
		{
			handle = array[i];		
			ret = mpHostOpHandler->getThumb(&m_PTPInfo, mpTransport, handle, &mThumbDataList.thumbs[mThumbDataList.numthumbs]);		
			if (ret != PTPErrNone)
			{
				return ret;
			}			
			mThumbDataList.numthumbs++;
		}
		curmax = curmin;
		curmin = curmin-maxObj;

	}
	else
	{
		closeSessionByForce();	
	}
	return ret;
}


int
PTPHOST::requestcopy(char * selected)
{
	int ret=PTPErrNone, i;
	SdiUInt32 handle;
	char pathname[PTP_OBJECT_NAME_LEN]= {0,};	
	const char delimiter[] = "/";

	/* Inital Connection */
	if (isDisconnected() == FALSE)
	{

		ret = mpHostOpHandler->openSession(&m_PTPInfo, mpTransport);
		if (ret != PTPErrNone)
		{
			return ret;
		}

		for(i =0; i<9; i++)
		{
			if(selected[i] == 1) // selected
			{
				char path[PTP_OBJECT_NAME_LEN]= {0,};				
 				handle = array[curmin+i];				
				getPath(&path[0]);
				strcat(path, delimiter);
				ret = getNameByHandle (handle, pathname, PTP_OBJECT_NAME_LEN);
				strcat(path,pathname);
				/* Need to check whether there is enough space or not. */
				ret = mpHostOpHandler->getObject(&m_PTPInfo, mpTransport,  handle, path);
				if (ret != PTPErrNone)
				{
					return ret;
				}			
				
			}
			
		}
		ret = mpHostOpHandler->closeSession(&m_PTPInfo, mpTransport);
		if (ret != PTPErrNone)
		{
			return ret;
		}
			
	}
	else
	{
		closeSessionByForce();	
	}
	return ret;
}

CString PTPHOST::getDevName(void)						// chlee
{
	return m_strDevName;
}

void PTPHOST::setDevName(CString DevName)				// chlee
{
	m_strDevName = DevName;
}

int PTPHOST::getImgProcessing(void)
{


	return processing;

}


int PTPHOST::getSetObject(int count,char *filename)
{
	int ret=PTPErrNone, i;
	SdiUInt32 handle;
	char pathname[PTP_OBJECT_NAME_LEN]= {0,};	
	const char delimiter[] = "\\";
	char path[PTP_OBJECT_NAME_LEN]= {0,};
	char dirname[PTP_OBJECT_NAME_LEN];

	memcpy(dirname,filename,strlen(filename));

	dirname[strlen(filename)] = '\0';
 
	if (isDisconnected() == FALSE)
	{
		for(i =0; i<count; i++)
		{
				memset(&path,0,strlen(dirname));
				strcat(path,dirname);
 				handle = array[curmin+i];				
				strcat(path, delimiter);
		
				ret = getNameByHandle (handle, pathname, PTP_OBJECT_NAME_LEN);				
				strcat(path,pathname);
				/* Need to check whether there is enough space or not. */
				Sleep(30);
				ret = mpHostOpHandler->getObject(&m_PTPInfo, mpTransport,  handle, path);

				/*�ӽ� �ڵ� */
				ret = PTPErrNone;
				if (ret != PTPErrNone)
				{
					return 0;
				}
				
				processing++;
			
		}
	}
	else
	{
	}
	return 1;
}



int
PTPHOST::getNameByHandle(SdiUInt32 handle, char* name, int size)
{
	int len = 0;
	
	PTPObjectType *foundObj = NULL;

	foundObj = mpStorage->findObjectByHandleInStorages(handle);

	if (foundObj == NULL)
	{
		PRINTD0("-?>Cannot find object by handle.\n");
		return PTP_ERROR_BAD_PARAM;
	}

	len = strlen(foundObj->Name);

	if (size < len)
	{
		PRINTD0("-?>Buffer size for name is not enough.\n");
		return PTP_ERROR_BUFFER_SIZE;
	}

	memcpy(name, foundObj->Name, len);

	return PTP_ERROR_NONE;
}


void
PTPHOST::getPath(char * path)
{
	/* Need to calculate 
		DCIM/999SCOPY/COPY0001.JPG ~ DCIM/999SCOPY/COPY9999.JPG		
	*/
	
	dir_t * dp;

	char dirName[PTP_MAX_PATH_LEN] = {0,};
	//const char delimiter[] = "/";
	const char delimiter[] = "\\";

#if 0
	dp =  (dir_t *) CreateDirectory((LPCTSTR) "c:\\DSCPRO",NULL);	

	if (dp == NULL) // not exist...
	{
		//create the directory for copying command.
		
	}
	dp =  (dir_t *) CreateDirectory((LPCTSTR) "c:\\DSCPRO\\img",NULL);	

	if (dp == NULL) 
	{
		//create the directory for copying command.
		
	}
#endif
	CTime tCurrentTime;
	CString strYear;
	CString strMonth;
	CString strDay;
	CString strHr;
	CString strMin;
	CString strSec;
	CString strFoldername;

	Imagepath="C:\\DSCPRO\\img";

 	sprintf(path,"%S",Imagepath);


	//PTPMisc::closeDir(dp);
	return ;
	
}

unsigned long
PTPHOST::getHandleByName(char* name)
{
	SdiUInt16 format;
	
	format = mpStorage->getObjectFormatByName(name);

	if (format == PTP_OFC_Association)
	{
		return mpStorage->findObjectHandleByName(name, PTP_FIND_OBJECT_ASSOC);		
	}
	else if (format == PTP_OFC_DPOF)
	{
		return mpStorage->findObjectHandleByName(name, PTP_FIND_OBJECT_DPOF);
	}
	else if (format == PTP_OFC_EXIF_JPEG)
	{
		return mpStorage->findObjectHandleByName(name, PTP_FIND_OBJECT_JPEG);
	}
	else
	{
		return mpStorage->findObjectHandleByName(name, PTP_FIND_OBJECT_UNKNOWN);
	}
}

unsigned long
PTPHOST::getHandleByPath(char* path)
{
	return mpStorage->findObjectHandleByPath(path);
}

int
PTPHOST::getDateByHandle(SdiUInt32 handle, char* date, int size)
{
	char path[PTP_MAX_PATH_LEN] = {0,};
	
	PTPStorageType *foundSESM = NULL;
	PTPObjectType *foundObj = NULL;

	PTPObjectStatType stat;
	
	foundObj = mpStorage->findObjectByHandleInStorages(handle);

	if (foundObj == NULL)
	{
		PRINTD1("-?>Cannot find object, handle:[%08lx].\n", handle);
		return PTP_ERROR_BAD_PARAM;
	}

	foundSESM = mpStorage->findStorageByIndex(PTP_MAKE_SESMINDEX_BY_HANDLE(foundObj->Handle));

	mpStorage->getObjectPath(foundSESM, foundObj, path);

	PTP_STRUCT_INIT(stat);
	PTPMisc::getStat(path, &stat);

	//meteor
	if (size < 20)
	{
		PRINTD0("-?>Buffer size for name is not enough.\n");
		return PTP_ERROR_BUFFER_SIZE;
	}

	//"2004/01/01 24:00:00"
	sprintf(date, "%04d/%02d/%02d %02d:%02d:%02d",
					stat.Ctime.Year, stat.Ctime.Month, stat.Ctime.Day,
					stat.Ctime.Hour, stat.Ctime.Min, stat.Ctime.Sec);

	return PTP_ERROR_NONE;
}

int
PTPHOST::getStatusForPTPHOST(void)
{
	return m_PTPInfo.StatusForHOST;
}

void
PTPHOST::setStatusForPTPHOST(int value)
{
	m_PTPInfo.StatusForHOST = value;

	return;
}

int
PTPHOST::getThumbList(PTPThumbData* buffer, int thumbs)
{
	thumbs = mThumbDataList.numthumbs;	
	memcpy(buffer, mThumbDataList.thumbs, sizeof(PTPThumbData)*thumbs);
	return PTP_ERROR_NONE;	
}

SdiBool
PTPHOST::isDisconnected(void)
{
	int newStatus;
	
	newStatus = mpTransport->getConnectionStatus();

	if (newStatus == 1)
	{
		if (m_PTPInfo.IsConnected == 0)
		{
			PRINTD0("->Transport is connected.\n");
			m_PTPInfo.IsConnected = 1;
		}
		
		return FALSE;
	}
	else
	{
		//newStatus == 0
		if (m_PTPInfo.IsConnected == 1)
		{
			PRINTD0("->Transport is disconnected.\n");
			m_PTPInfo.IsConnected = 0;
		}
		
		return TRUE;
	}
}

void
PTPHOST::closeSessionByForce(void)
{
	if (m_PTPInfo.SessionStatus == PTP_SESSION_CLOSED)
		return;

	PRINTD0("->Close session by force.\n");

	/* reset sesseion ID in dc */
	m_PTPInfo.SessionID = 0;
	m_PTPInfo.SessionStatus = PTP_SESSION_CLOSED;
	m_PTPInfo.Request.SessionID = 0;
	
	//mpHostOpHandler->mbIsDPSDiscoveryCompleted = FALSE;

	//rujin 12.12
	mpStorage->termObject(); 

	return;
}
 
