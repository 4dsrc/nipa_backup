/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 * 
 */
/*!
 \file PTP_UsbIO.cpp
 \brief Implement PTP USB I/O Class inherited from PTP Transport I/O Class.
 \date 2009-06-11
 \version 0.70 
*/
#include "stdafx.h"


#include "PTP_UsbIO.h"
#include "PTP_Misc.h"
// #include <Objbase.h>			// 2011-7-9	 chlee

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#if 1   // duvallee 07-29-2010 : for Windows7
#include "USBHAL.h"
#endif
PTPUSBIO::PTPUSBIO()
{
	mbNeedSendingNullPacket = 0;
	
	mpIfUSBHAL = NULL;
	mpIfUSBHAL = CUSBHALInstance::createUSBHAL();

	if (mpIfUSBHAL == NULL)
		PRINTD0("-?>Cannot open usbio.\n");
}

PTPUSBIO::~PTPUSBIO()
{   
	if (mpIfUSBHAL != NULL)
	{
		CUSBHALInstance::destroyUSBHAL(mpIfUSBHAL);
	
		mpIfUSBHAL = NULL;
	}
}

int
PTPUSBIO::open(char* name)
{
	int ret;
	
	if (mpIfUSBHAL == NULL)
		return PTPErrCannotCreateInstance;


	ret = mpIfUSBHAL->openUSB(name, 0);


	if (ret != USBHAL_ERR_NONE)
	{
		PRINTD0("-?>Cannot open usbio.\n");
		return PTPErrUSBIOOpen;
	}

	return PTPErrNone;
}

void
PTPUSBIO::close(void)
{
	if (mpIfUSBHAL != NULL)
#ifdef __ADJ_NX__
		mpIfUSBHAL->closeUSB();
#else
        mpIfUSBHAL->closeUSB();
#endif
	
	mbNeedSendingNullPacket = 0;
	
	return;
}


#ifdef __PTP_HOST__
int
PTPUSBIO::sendCommand(PTPContainerType* command)
{
	int ret;
	SdiInt32 size = 0;
	int temp;
	PTPUSBBulkContainerType usbCommand;
	
	if (command == NULL)
	{
		PRINTD0("-?>there is no command.\n");
		return PTPErrBadParam;
	}

	PTP_STRUCT_INIT(usbCommand);

	size = PTP_USB_BULK_REQ_LEN - sizeof(SdiInt32)*(5 - command->Nparam);
	
	/* build appropriate USB ptpCntr */

	usbCommand.Length  = (size);
	usbCommand.Type    = (PTP_USB_CONTAINER_COMMAND);
	usbCommand.Code    = (command->Code);
	usbCommand.TransID = (command->TransactionID);
	
	switch (command->Nparam)
	{
	case 5:
		usbCommand.Payload.Params.Param1 = (command->Param1);
		usbCommand.Payload.Params.Param2 = (command->Param2);
		usbCommand.Payload.Params.Param3 = (command->Param3);
		usbCommand.Payload.Params.Param4 = (command->Param4);
		usbCommand.Payload.Params.Param5 = (command->Param5);
		break;
		
	case 4:
		usbCommand.Payload.Params.Param1 = (command->Param1);
		usbCommand.Payload.Params.Param2 = (command->Param2);
		usbCommand.Payload.Params.Param3 = (command->Param3);
		usbCommand.Payload.Params.Param4 = (command->Param4);
		break;
	
	case 3:
		usbCommand.Payload.Params.Param1 = (command->Param1);
		usbCommand.Payload.Params.Param2 = (command->Param2);
		usbCommand.Payload.Params.Param3 = (command->Param3);
		break;

	case 2:
		usbCommand.Payload.Params.Param1 = (command->Param1);
		usbCommand.Payload.Params.Param2 = (command->Param2);
		break;

	case 1:
		usbCommand.Payload.Params.Param1 = (command->Param1);
		break;

	case 0:
		//rujin.kim 11/01
		size = strlen(command->Data) + 12;
		usbCommand.Length  = (size);
		temp = strlen(command->Data);
		memcpy(usbCommand.Payload.Data,command->Data,strlen(command->Data));
	default:
		break;
	}

	/* send command to responder*/
	ret = mpIfUSBHAL->writeBulk((char *)&usbCommand, size);

	
	if (ret != USBHAL_ERR_NONE)
	{
		PRINTD0("-?>Cannot write command.\n");
		return PTPErrUSBIOWrite;
	}

	return PTPErrNone;	
}

int
PTPUSBIO::getResponse(PTPContainerType* res)
{
	int	ret;
	SdiInt32 resSize = 0;
	PTPUSBBulkContainerType usbRes;

	if (res == NULL)
	{
		PRINTD0("-?>there is no response ptpCntr.\n");
		return PTPErrBadParam;
	}

	PTP_STRUCT_INIT(usbRes);
	
	/* read operation response, it should never be longer than sizeof(usbReq) */
	ret = mpIfUSBHAL->readBulk((char *)&usbRes, PTP_USB_BULK_HDR_LEN);

	if (ret != USBHAL_ERR_NONE)
	{
		/*
		PRINTD0("-?>Cannot read response.\n");
		*/
		return PTPErrUSBIORead;
	}

	if (PTPEndian::ttod16(usbRes.Type) != PTP_USB_CONTAINER_RESPONSE)
	{
		PRINTD0("-?>request type is not response.\n");
		return PTPErrNotContainerType;
	}

	resSize = PTPEndian::ttod32(usbRes.Length) - PTP_USB_BULK_HDR_LEN;

	if (resSize > 0)
	{
		/*
		PRINTD0("->response size:[%04d].\n", resSize);
		*/

		ret = mpIfUSBHAL->readBulk((char *)usbRes.Payload.Data, resSize);

		if (ret != USBHAL_ERR_NONE)
		{
			PRINTD0("-?>Cannot read the rest of reponse.\n");
			return PTPErrUSBIORead;
		}
	}

	/* build an appropriate PTPContainerType */
	res->Code = PTPEndian::ttod16(usbRes.Code);
	res->TransactionID = PTPEndian::ttod32(usbRes.TransID);

	/*Need to check whether the response transactionID and command transactionID are matched */
	
	res->Param1 = PTPEndian::ttod32(usbRes.Payload.Params.Param1);
	res->Param2 = PTPEndian::ttod32(usbRes.Payload.Params.Param2);
	res->Param3 = PTPEndian::ttod32(usbRes.Payload.Params.Param3);
	res->Param4 = PTPEndian::ttod32(usbRes.Payload.Params.Param4);
	res->Param5 = PTPEndian::ttod32(usbRes.Payload.Params.Param5);
	
	return PTPErrNone;
}

#else

int
PTPUSBIO::getRequest(PTPContainerType* req)
{
	int	ret;
	uint32_t reqSize = 0;
	PTPUSBBulkContainerType usbReq;

	if (req == NULL)
	{
		PRINTD0("-?>there is no request ptpCntr.\n");
		return PTPErrBadParam;
	}

	PTP_STRUCT_INIT(usbReq);
	
	/* read operation request, it should never be longer than sizeof(usbReq) */
	ret = mpIfUSBHAL->readBulk((char *)&usbReq, PTP_USB_BULK_HDR_LEN);

	if (ret != USBHAL_ERR_NONE)
	{
		/*
		PRINTD0("-?>Cannot read request.\n");
		*/
		return PTPErrUSBIORead;
	}

	if (PTPEndian::ttod16(usbReq.Type) != PTP_USB_CONTAINER_COMMAND)
	{
		PRINTD0("-?>request type is not command.\n");
		return PTPErrNotContainerType;
	}

	reqSize = PTPEndian::ttod32(usbReq.Length) - PTP_USB_BULK_HDR_LEN;

	if (reqSize > 0)
	{
		/*
		PRINTD0("->request size:[%04d].\n", reqSize);
		*/

		ret = mpIfUSBHAL->readBulk((char *)usbReq.Payload.Data, reqSize);

		if (ret != USBHAL_ERR_NONE)
		{
			PRINTD0("-?>Cannot read the rest of requset.\n");
			return PTPErrUSBIORead;
		}
	}

	/* build an appropriate PTPContainerType */
	req->Code = PTPEndian::ttod16(usbReq.Code);
	req->TransactionID = PTPEndian::ttod32(usbReq.TransID);

	req->Param1 = PTPEndian::ttod32(usbReq.Payload.Params.Param1);
	req->Param2 = PTPEndian::ttod32(usbReq.Payload.Params.Param2);
	req->Param3 = PTPEndian::ttod32(usbReq.Payload.Params.Param3);
	req->Param4 = PTPEndian::ttod32(usbReq.Payload.Params.Param4);
	req->Param5 = PTPEndian::ttod32(usbReq.Payload.Params.Param5);
	
	return PTPErrNone;
}

int
PTPUSBIO::sendResponse(PTPContainerType* resp)
{
	int	ret;
	uint32_t size = 0;
	PTPUSBBulkContainerType	usbResp;

	if (resp == NULL)
	{
		PRINTD0("-?>there is no response ptpCntr.\n");
		return PTPErrBadParam;
	}

	PTP_STRUCT_INIT(usbResp);
	
	size = PTP_USB_BULK_RESP_LEN - sizeof(uint32_t)*(5 - resp->Nparam);

	/* build appropriate USB ptpCntr */
	usbResp.Length  = PTPEndian::dtot32(size);
	usbResp.Type    = PTPEndian::dtot16(PTP_USB_CONTAINER_RESPONSE);
	usbResp.Code    = PTPEndian::dtot16(resp->Code);
	usbResp.TransID = PTPEndian::dtot32(resp->TransactionID);

#if 0	
	usbResp.Payload.Params.Param1 = PTPEndian::dtot32(resp->Param1);
	usbResp.Payload.Params.Param2 = PTPEndian::dtot32(resp->Param2);
	usbResp.Payload.Params.Param3 = PTPEndian::dtot32(resp->Param3);
	usbResp.Payload.Params.Param4 = PTPEndian::dtot32(resp->Param4);
	usbResp.Payload.Params.Param5 = PTPEndian::dtot32(resp->Param5);
#endif	

	switch (resp->Nparam)
	{
	case 5:
		usbResp.Payload.Params.Param1 = PTPEndian::dtot32(resp->Param1);
		usbResp.Payload.Params.Param2 = PTPEndian::dtot32(resp->Param2);
		usbResp.Payload.Params.Param3 = PTPEndian::dtot32(resp->Param3);
		usbResp.Payload.Params.Param4 = PTPEndian::dtot32(resp->Param4);
		usbResp.Payload.Params.Param5 = PTPEndian::dtot32(resp->Param5);
		break;
		
	case 4:
		usbResp.Payload.Params.Param1 = PTPEndian::dtot32(resp->Param1);
		usbResp.Payload.Params.Param2 = PTPEndian::dtot32(resp->Param2);
		usbResp.Payload.Params.Param3 = PTPEndian::dtot32(resp->Param3);
		usbResp.Payload.Params.Param4 = PTPEndian::dtot32(resp->Param4);
		break;
		
	case 3:
		usbResp.Payload.Params.Param1 = PTPEndian::dtot32(resp->Param1);
		usbResp.Payload.Params.Param2 = PTPEndian::dtot32(resp->Param2);
		usbResp.Payload.Params.Param3 = PTPEndian::dtot32(resp->Param3);
		break;

	case 2:
		usbResp.Payload.Params.Param1 = PTPEndian::dtot32(resp->Param1);
		usbResp.Payload.Params.Param2 = PTPEndian::dtot32(resp->Param2);
		break;

	case 1:
		usbResp.Payload.Params.Param1 = PTPEndian::dtot32(resp->Param1);
		break;

	case 0:
	default:
		break;
	}

	/* send response to initiator */
	ret = mpIfUSBHAL->writeBulk((char *)&usbResp, size);
	
	if (ret != USBHAL_ERR_NONE)
	{
		PRINTD0("-?>Cannot write response.");
		return PTPErrUSBIOWrite;		
	}

	return PTPErrNone;
}

int
PTPUSBIO::sendEvent(PTPContainerType* event)
{
	int ret;
	uint32_t size = 0;
	PTPUSBEventContainerType usbEvent;
	
	if (event == NULL)
	{
		PRINTD0("-?>there is no event.\n");
		return PTPErrBadParam;
	}

	PTP_STRUCT_INIT(usbEvent);

	if (event->Nparam >= 3)
		event->Nparam = 3;

	size = PTP_USB_EVENT_REQ_LEN - sizeof(uint32_t)*(3 - event->Nparam);
	
	/* build appropriate USB ptpCntr */
	usbEvent.Length  = PTPEndian::dtot32(size);
	usbEvent.Type    = PTPEndian::dtot16(PTP_USB_CONTAINER_EVENT);
	usbEvent.Code    = PTPEndian::dtot16(event->Code);
	usbEvent.TransID = PTPEndian::dtot32(event->TransactionID);
	
#if 0
	usbEvent.Param1 = PTPEndian::dtot32(event->Param1);
	usbEvent.Param2 = PTPEndian::dtot32(event->Param2);
	usbEvent.Param3 = PTPEndian::dtot32(event->Param3);
#endif

	switch (event->Nparam)
	{
	case 3:
		usbEvent.Param1 = PTPEndian::dtot32(event->Param1);
		usbEvent.Param2 = PTPEndian::dtot32(event->Param2);
		usbEvent.Param3 = PTPEndian::dtot32(event->Param3);
		break;

	case 2:
		usbEvent.Param1 = PTPEndian::dtot32(event->Param1);
		usbEvent.Param2 = PTPEndian::dtot32(event->Param2);
		break;

	case 1:
		usbEvent.Param1 = PTPEndian::dtot32(event->Param1);
		break;

	case 0:
	default:
		break;
	}

	/* send response to initiator */
	ret = mpIfUSBHAL->writeIntr((char *)&usbEvent, size);
	
	if (ret != USBHAL_ERR_NONE)
	{
		PRINTD0("-?>Cannot write event.\n");
		return PTPErrUSBIOWrite;
	}

	return PTPErrNone;	
}

int
PTPUSBIO::sendData(PTPContainerType* ptpCntr,
					char*           txData,
					uint32_t		size,
					uint32_t		totalSize)
{
	int	ret;
	uint32_t ttSize = 0, tempSize = 0;
	PTPUSBBulkContainerType usbData;

	if ((ptpCntr == NULL) || (txData == NULL)) 
	{
		PRINTD0("-?>there is no ptpCntr or tx data.\n");
		return PTPErrBadParam;	
	}

	PTP_STRUCT_INIT(usbData);	

	/* build appropriate USB ptpCntr */
	ttSize = PTP_USB_BULK_HDR_LEN + totalSize;
	
	PRINTD2("->write data, size:[%ld], total size:[%ld]\n", size, totalSize);

	mbNeedSendingNullPacket = 0;
	
	if (mpIfUSBHAL->isNullPktNeed(USBHAL_EP_BULK_OUT, ttSize) == 1)
		mbNeedSendingNullPacket = 1;
#if 0
	if (isneed_nullpacket_usb(USBHAL_EP_BULK_OUT, ttSize) == PTPErrNone)
	{
		ttSize += 1;
		mbNeedSendingNullPacket = 1;
	}
#endif

	usbData.Length  = PTPEndian::dtot32(ttSize);
	usbData.Type    = PTPEndian::dtot16(PTP_USB_CONTAINER_DATA);
	usbData.Code    = PTPEndian::dtot16(ptpCntr->Code);
	usbData.TransID = PTPEndian::dtot32(ptpCntr->TransactionID);

#ifndef __PTP_NO_DEBUG__
	printUSBBulkContainer(&usbData);
#endif

	tempSize = (size < PTP_USB_BULK_PAYLOAD_LEN) ? size : PTP_USB_BULK_PAYLOAD_LEN;
	
	memcpy((char *)usbData.Payload.Data, (char *)txData, tempSize);

	/* send first part of data */
	ret = mpIfUSBHAL->writeBulk((char *)&usbData, (PTP_USB_BULK_HDR_LEN + tempSize));
	
	if (ret != USBHAL_ERR_NONE)
	{
		PRINTD0("-?>Cannot write data.\n");
		return PTPErrUSBIOWrite;
	}

	if ((size - tempSize) > 0)
	{
		/* if everything OK, send the rest */
		ret = mpIfUSBHAL->writeBulk((char *)(txData + tempSize), (size - tempSize));
		
		if (ret != USBHAL_ERR_NONE)
		{
			PRINTD0("-?>Cannot write the rest of data.\n");
			return PTPErrUSBIOWrite;
		}
	}

	if (mbNeedSendingNullPacket && (totalSize == size))
	{
		ret = mpIfUSBHAL->writeBulkNullPkt();
		mbNeedSendingNullPacket = 0;

		if (ret != USBHAL_ERR_NONE)
		{
			PRINTD0("-?>Cannot write null packet.\n");
			return PTPErrUSBIOWrite;
		}		
	}

	return PTPErrNone;
}

int
PTPUSBIO::sendDataOnly(char* txData, uint32_t size, bool_t bIsLastData)
{
	int	ret;

	if (txData == NULL)
	{
		PRINTD0("-?>there is no tx data.\n");
		return PTPErrBadParam;	
	}

	PRINTD2("->write data only, size:[%ld], bIsLastData[%d].\n", size, bIsLastData);

	if (size > 0)
	{
		ret = mpIfUSBHAL->writeBulk((char *)txData, size);

		if (ret != USBHAL_ERR_NONE)
		{
			PRINTD0("-?>Cannot write data.\n");
			return PTPErrUSBIOWrite;
		}
	}

	if (mbNeedSendingNullPacket && bIsLastData)
	{
		ret =  mpIfUSBHAL->writeBulkNullPkt();
		mbNeedSendingNullPacket = 0;

		if (ret != USBHAL_ERR_NONE)
		{
			PRINTD0("-?>Cannot write null packet.\n");
			return PTPErrUSBIOWrite;
		}		
	}

	return PTPErrNone;
}

#endif

#if 0   // duvallee 07-29-2010 : for LibUSB & Windows7
int PTPUSBIO::getData_Ex(PTPContainerType* req, char* rxData, uint32_t size, int opt, void* optParam)
{
    int	nReadBytes                          = 0;
    int nReponseReadBytes                   = 0;
    int nRemainReadBytes                    = 0;

    PTPUSBBulkContainerType* pUsbData		= 0;
    //euns test
    //PTPUSBBulkContainerType* usbData		= 0;

    char szBuffer[4096]                     = {0, };
    char* pRxData                           = rxData;
	uint32_t tempSize = 0;
	int	ret;

    if ((req == NULL) || (rxData == NULL))
    {
        PRINTD0("-?>there is no request or rx data.\n");
        return PTPErrBadParam;
    }

    nReadBytes  = mpIfUSBHAL->readBulk_Ex((char*) szBuffer, PTP_USB_BULK_HDR_LEN);
   
    if (nReadBytes <= 0)
    {
        PRINTD0("-?>Cannot read data.\n");
        return PTPErrUSBIORead;
    }

    pUsbData    = (PTPUSBBulkContainerType*) szBuffer;

    if (PTPEndian::ttod16(pUsbData->Type) != PTP_USB_CONTAINER_DATA)
    {
        PRINTD0("-?>usb bulk container type is not data.\n");
        return PTPErrNotContainerType;
    }

    if (PTPEndian::ttod16(pUsbData->Code) != req->Code)
    {
        PRINTD0("-?>bulk container code is not matched to request code.\n");
        return PTPErrInvalidOpCode;
    }

    /* evaluate data length */
    nReponseReadBytes   = PTPEndian::ttod32(pUsbData->Length);
    //PRINTD2("--->read data, size:[%ld], opt:[%d].\n", dataSize, opt);	

    if (opt == PTP_GET_DATA_NONE)
    {
        memcpy (pRxData, (szBuffer + PTP_USB_BULK_HDR_LEN), nReadBytes - PTP_USB_BULK_HDR_LEN);
        pRxData += nReadBytes - PTP_USB_BULK_HDR_LEN;
        nRemainReadBytes    = nReponseReadBytes - nReadBytes;
        while (nRemainReadBytes > 0)
        {
            nReadBytes  = mpIfUSBHAL->readBulk_Ex((char*) szBuffer, nRemainReadBytes);
            if (nReadBytes < 0)
            {
                return PTPErrUSBIORead;
            }
            memcpy (pRxData, szBuffer, nReadBytes);
            nRemainReadBytes    -= nReadBytes;
        }
    }
    else
    { 
		if ((opt == PTP_GET_DATA_VIRTUAL_FILE) || (opt == PTP_GET_DATA_OBJECT_INFO))
		{
		        nReadBytes = sizeof(pUsbData->Payload.Data);
		    	/* copy first part of data to 'rxData' */
		    	memcpy(pRxData, pUsbData->Payload.Data, nReadBytes);

		    	/* is that all of data? */
		    	if ((nReadBytes + PTP_USB_BULK_HDR_LEN) <= sizeof(*pUsbData))
		    		return PTPErrNone;

				/* is buffer size enough? */
				if (nReponseReadBytes > size)
				{
					PRINTD0("-?>data buffer size is not enough.\n");
					return PTPErrBufferSize;
				}
                nRemainReadBytes    = nReponseReadBytes - nReadBytes;
                int nWriteBytes = nReadBytes;
                while(nRemainReadBytes>0)
                {
                	/* if not finally, read the rest of it */
		    	    nReadBytes = mpIfUSBHAL->readBulk_Ex((char *)(pRxData + nWriteBytes),nRemainReadBytes);
                    
                    if(nReadBytes <0)
		    	    {
		    		    PRINTD0("-?>Cannot read the rest of data.\n");
		    		    return PTPErrUSBIORead;	
		    	    }
		    	    nWriteBytes+=nReadBytes;
		    	    nRemainReadBytes-=nReadBytes;
                }


		}
		else
		{
			/* opt : PTP_GET_DATA_OBJECT */
			
			file_t * rxFile;

//rujin 11/18 fast file trans


			//char rxBuf[PTP_USB_BULK_HS_MAX_PACKET_LEN*1000] = {0,};
			char rxBuf[PTP_USB_BULK_HS_MAX_PACKET_LEN] = {0,};
			rxFile = (file_t *)optParam;
			
			tempSize = sizeof(pUsbData->Payload.Data);
			/* write first part of data to 'rxFile' */
			ret = PTPMisc::writeFile(rxFile, pUsbData->Payload.Data, tempSize);
			
			if (ret != PTPErrNone)
			{
				PRINTD0("-?>Cannot write the payload in file.\n");
				return PTPErrFileWrite;
			}
			 
			/* is that all of data? */
			if ((nReponseReadBytes + PTP_USB_BULK_HDR_LEN) <= sizeof(*pUsbData))
				return PTPErrNone;

            nRemainReadBytes = (nReponseReadBytes - tempSize);
				
			
			while (nRemainReadBytes > 0)
			{


//rujin 11/18 fast file trans
				memset(rxBuf, 0, PTP_USB_BULK_HS_MAX_PACKET_LEN);
				char str[500*1000];
				memset(str,0x00,500*1000);
				tempSize = (PTP_USB_BULK_HS_MAX_PACKET_LEN < nReponseReadBytes) ? PTP_USB_BULK_HS_MAX_PACKET_LEN : nReponseReadBytes;
                tempSize = 500*1000;
				/* if not finally, read the rest of it */
				nReadBytes = mpIfUSBHAL->readBulk_Ex(rxBuf, tempSize);
				//nReadBytes = mpIfUSBHAL->readBulk_Ex(str, tempSize);
				
				if(nReadBytes < 0)
				{
				    return PTPErrUSBIORead;	
				}
				
				ret = PTPMisc::writeFile(rxFile, rxBuf, (int)nReadBytes);
				
				if (ret != PTPErrNone)
				{
					PRINTD0("-?>Cannot write the rest of data in file.\n");
					return PTPErrFileWrite;
				}

				nRemainReadBytes -= nReadBytes;
			}
		}
    }

#if 0

	if (opt == PTP_GET_DATA_NONE)
	{
		/* 루프를 돌면서 읽기만 한다. */
		while (dataSize > 0)
		{
			tempSize = (size < dataSize) ? size : dataSize;

			/* if not finally, read the rest of it */
			ret = mpIfUSBHAL->readBulk(rxData, tempSize);

			if (ret != USBHAL_ERR_NONE)
			{
				PRINTD0("-?>Cannot read the rest of data.\n");
				return PTPErrUSBIORead;
			}

			dataSize -= tempSize;
		}
    	}
	else
	{
	    	tempSize = (PTP_USB_BULK_PAYLOAD_LEN < dataSize) ? PTP_USB_BULK_PAYLOAD_LEN : dataSize;

	    	ret = mpIfUSBHAL->readBulk((char *)usbData->Payload.Data, tempSize);	

	    	if (ret != USBHAL_ERR_NONE)
	    	{
	    		PRINTD0("-?>Cannot read the payload data.\n");
	    		return PTPErrUSBIORead;
	    	}

		if ((opt == PTP_GET_DATA_VIRTUAL_FILE) || (opt == PTP_GET_DATA_OBJECT_INFO))
		{
		    	/* copy first part of data to 'rxData' */
		    	memcpy(rxData, usbData->Payload.Data, tempSize);

		    	/* is that all of data? */
		    	if ((dataSize + PTP_USB_BULK_HDR_LEN) <= sizeof(*usbData))
		    		return PTPErrNone;

				/* is buffer size enough? */
				if (dataSize > size)
				{
					PRINTD0("-?>data buffer size is not enough.\n");
					return PTPErrBufferSize;
				}

		    	/* if not finally, read the rest of it */
		    	ret = mpIfUSBHAL->readBulk((char *)(rxData + tempSize), dataSize - tempSize);

		    	if (ret != USBHAL_ERR_NONE)
		    	{
		    		PRINTD0("-?>Cannot read the rest of data.\n");
		    		return PTPErrUSBIORead;	
		    	}
		}
		else
		{
			/* opt : PTP_GET_DATA_OBJECT */
			
			file_t * rxFile;

//rujin 11/18 fast file trans


			//-- 2011-7-19 Lee JungTaek
			//-- Code Sonar
			//char rxBuf[PTP_USB_BULK_HS_MAX_PACKET_LEN*1000] = {0,};
			char rxBuf[PTP_USB_BULK_HS_MAX_PACKET_LEN] = {0,};

			rxFile = (file_t *)optParam;
			
			/* write first part of data to 'rxFile' */
			ret = PTPMisc::writeFile(rxFile, usbData->Payload.Data, (int)tempSize);
			
			if (ret != PTPErrNone)
			{
				PRINTD0("-?>Cannot write the payload in file.\n");
				return PTPErrFileWrite;
			}
			 
			/* is that all of data? */
			if ((dataSize + PTP_USB_BULK_HDR_LEN) <= sizeof(*usbData))
				return PTPErrNone;

			dataSize -= tempSize;	
			
			while (dataSize > 0)
			{


//rujin 11/18 fast file trans
				memset(rxBuf, 0, PTP_USB_BULK_HS_MAX_PACKET_LEN*1000);
				
				tempSize = (PTP_USB_BULK_HS_MAX_PACKET_LEN*1000 < dataSize) ? PTP_USB_BULK_HS_MAX_PACKET_LEN*1000 : dataSize;

				/*
				memset(rxBuf, 0, PTP_USB_BULK_HS_MAX_PACKET_LEN);
				
				tempSize = (PTP_USB_BULK_HS_MAX_PACKET_LEN < dataSize) ? PTP_USB_BULK_HS_MAX_PACKET_LEN : dataSize;
*/
				/* if not finally, read the rest of it */
				ret = mpIfUSBHAL->readBulk(rxBuf, tempSize);
				
				if (ret != USBHAL_ERR_NONE)
				{
					PRINTD0("-?>Cannot read the rest of data.\n");
					return PTPErrUSBIOWrite;
				}
				
				ret = PTPMisc::writeFile(rxFile, rxBuf, (int)tempSize);
				
				if (ret != PTPErrNone)
				{
					PRINTD0("-?>Cannot write the rest of data in file.\n");
					return PTPErrFileWrite;
				}

				dataSize -= tempSize;
			}
		}
	}
#endif
    return PTPErrNone;
}
#endif
/* Need to consider whether the getData can be shared with a host's function*/
int
PTPUSBIO::getData(PTPContainerType* req,
				   char*             rxData,
				   SdiUInt32          size,
				   int               opt,
				   void*             optParam)
{
	int	ret;
	SdiInt32 dataSize = 0, tempSize = 0;
	PTPUSBBulkContainerType *usbData= 0;

	if ((req == NULL) || (rxData == NULL))
	{
		PRINTD0("-?>there is no request or rx data.\n");
		return PTPErrBadParam;
	}
	usbData= new PTPUSBBulkContainerType;
	if(usbData)
	{
		PTP_STRUCT_INIT(*usbData);

		ret = mpIfUSBHAL->readBulk((char *)usbData, PTP_USB_BULK_HDR_LEN);

		if (ret != USBHAL_ERR_NONE)
		{
			PRINTD0("-?>Cannot read data.\n");
			if( usbData)
			{
				delete 	usbData;		
				usbData = NULL;
			}
			return PTPErrUSBIORead;
		}

		if (PTPEndian::ttod16(usbData->Type) != PTP_USB_CONTAINER_DATA)
		{
       		 PRINTD0("-?>usb bulk container type is not data.\n");
			 if( usbData)
			 {
				 delete 	usbData;		
				 usbData = NULL;
			 }	 
			return PTPErrNotContainerType;
		}
	
		if (PTPEndian::ttod16(usbData->Code) != req->Code)
		{
			PRINTD0("-?>bulk container code is not matched to request code.\n");
			if( usbData)
			{
				delete 	usbData;		
				usbData = NULL;
			}			
			return PTPErrInvalidOpCode;
		}
	
		/* evaluate data length */
		dataSize = PTPEndian::ttod32(usbData->Length) - PTP_USB_BULK_HDR_LEN;

		PRINTD2("--->read data, size:[%ld], opt:[%d].\n", dataSize, opt);	

		if (opt == PTP_GET_DATA_NONE)
		{
			/* 루프를 돌면서 읽기만 한다. */
			while (dataSize > 0)
			{
				tempSize = (size < dataSize) ? size : dataSize;

				/* if not finally, read the rest of it */
				ret = mpIfUSBHAL->readBulk(rxData, tempSize);

				if (ret != USBHAL_ERR_NONE)
				{
					PRINTD0("-?>Cannot read the rest of data.\n");
					//euns 0303
					if(usbData!=NULL)
					{
						delete 	usbData;
						usbData=NULL;
					
					}

					return PTPErrUSBIORead;
				}

				dataSize -= tempSize;
			}
		}
		else
		{
	    		tempSize = (PTP_USB_BULK_PAYLOAD_LEN < dataSize) ? PTP_USB_BULK_PAYLOAD_LEN : dataSize;

	    		ret = mpIfUSBHAL->readBulk((char *)usbData->Payload.Data, tempSize);	

	    		if (ret != USBHAL_ERR_NONE)
	    		{
	    			PRINTD0("-?>Cannot read the payload data.\n");
					//euns 0303
					if(usbData!=NULL)
					{
						delete 	usbData;
						usbData=NULL;
					
					}

	    			return PTPErrUSBIORead;
	    		}

			if ((opt == PTP_GET_DATA_VIRTUAL_FILE) || (opt == PTP_GET_DATA_OBJECT_INFO))
			{
		    		/* copy first part of data to 'rxData' */
		    		memcpy(rxData, usbData->Payload.Data, tempSize);

		    		/* is that all of data? */
		    		if ((dataSize + PTP_USB_BULK_HDR_LEN) <= sizeof(*usbData))
					{
						//euns 0303
						if(usbData!=NULL)
						{
							delete 	usbData;
							usbData=NULL;
						
						}

		    			return PTPErrNone;
					}
					/* is buffer size enough? */
					if (dataSize > size)
					{
						PRINTD0("-?>data buffer size is not enough.\n");
	//euns 0303
					if(usbData!=NULL)
					{
						delete 	usbData;
						usbData=NULL;
					
					}
						return PTPErrBufferSize;
					}

		    		/* if not finally, read the rest of it */
		    		ret = mpIfUSBHAL->readBulk((char *)(rxData + tempSize), dataSize - tempSize);

		    		if (ret != USBHAL_ERR_NONE)
		    		{
		    			PRINTD0("-?>Cannot read the rest of data.\n");
	//euns 0303
					if(usbData!=NULL)
					{
						delete 	usbData;
						usbData=NULL;
					
					}
		    			return PTPErrUSBIORead;	
		    		}
			}
			else
			{
				/* opt : PTP_GET_DATA_OBJECT */
			
				file_t * rxFile;

	//rujin 11/18 fast file trans


				char rxBuf[PTP_USB_BULK_HS_MAX_PACKET_LEN*1000] = {0,};
			
				rxFile = (file_t *)optParam;
			
				/* write first part of data to 'rxFile' */
				ret = PTPMisc::writeFile(rxFile, usbData->Payload.Data, (int)tempSize);
			
				if (ret != PTPErrNone)
				{
					PRINTD0("-?>Cannot write the payload in file.\n");
	//euns 0303
					if(usbData!=NULL)
					{
						delete 	usbData;
						usbData=NULL;
					
					}
					return PTPErrFileWrite;
				}
			 
				/* is that all of data? */
				if ((dataSize + PTP_USB_BULK_HDR_LEN) <= sizeof(*usbData))
				{
			
					//euns 0223
					if(usbData!=NULL)
					{
						delete	usbData;
						usbData=NULL;
					
					}
					return PTPErrNone;
				}

				dataSize -= tempSize;	
			
				while (dataSize > 0)
				{


	//rujin 11/18 fast file trans
					memset(rxBuf, 0, PTP_USB_BULK_HS_MAX_PACKET_LEN*1000);
				
					tempSize = (PTP_USB_BULK_HS_MAX_PACKET_LEN*1000 < dataSize) ? PTP_USB_BULK_HS_MAX_PACKET_LEN*1000 : dataSize;

					/*
					memset(rxBuf, 0, PTP_USB_BULK_HS_MAX_PACKET_LEN);
				
					tempSize = (PTP_USB_BULK_HS_MAX_PACKET_LEN < dataSize) ? PTP_USB_BULK_HS_MAX_PACKET_LEN : dataSize;
	*/
					/* if not finally, read the rest of it */
					ret = mpIfUSBHAL->readBulk(rxBuf, tempSize);
				
					if (ret != USBHAL_ERR_NONE)
					{
						PRINTD0("-?>Cannot read the rest of data.\n");
	//euns 0303
						if(usbData!=NULL)
						{
							delete 	usbData;
							usbData=NULL;
						
						}
						return PTPErrUSBIOWrite;
					}
				
					ret = PTPMisc::writeFile(rxFile, rxBuf, (int)tempSize);
				
					if (ret != PTPErrNone)
					{
						PRINTD0("-?>Cannot write the rest of data in file.\n");
	//euns 0303
						if(usbData!=NULL)
						{
							delete 	usbData;
							usbData=NULL;
						
						}
						return PTPErrFileWrite;
					}

					dataSize -= tempSize;
				}
			}
		}
	//euns 0303
		if(usbData!=NULL)
		{
			delete 	usbData;
			usbData=NULL;
		
		}
	}


	
	return PTPErrNone;
// }
}


/* Have to make Cancel Operation function */

void
PTPUSBIO::reset(int targetIO)
{
	switch (targetIO)
	{
		case PTP_IO_RX:
			mpIfUSBHAL->flushRxBulk();
			break;

		case PTP_IO_TX:
			mpIfUSBHAL->flushTxBulk();
			break;

		case PTP_IO_ALL:
		default:
			mpIfUSBHAL->flushAllBulk();
			break;
	}

	return;
}

int
PTPUSBIO::getConnectionStatus(void)
{
	return mpIfUSBHAL->checkConnection();
}


void
PTPUSBIO::printUSBBulkContainer(PTPUSBBulkContainerType* usbBulk)
{
	if (PTPEnvironmentConfig::msPTPDebugLevel <= 5)
	{
	    PRINTD0("->USB Bulk Container\n");
		PRINTD1("->length  = [%ld]\n", usbBulk->Length);
		PRINTD1("->type    = [%04X]\n", usbBulk->Type);
		PRINTD1("->code    = [%04X]\n", usbBulk->Code);
		PRINTD1("->transID = [%08lX]\n", usbBulk->TransID);

		PRINTD1("->param1  = [%08lX]\n", usbBulk->Payload.Params.Param1);
		PRINTD1("->param2  = [%08lX]\n", usbBulk->Payload.Params.Param2);
		PRINTD1("->param3  = [%08lX]\n", usbBulk->Payload.Params.Param3);
		PRINTD1("->param4  = [%08lX]\n", usbBulk->Payload.Params.Param4);
		PRINTD1("->param5  = [%08lX]\n", usbBulk->Payload.Params.Param5);
	}
	return;
}

void
PTPUSBIO::printUSBEventContainer(PTPUSBEventContainerType* usbEvent)
{
	if (PTPEnvironmentConfig::msPTPDebugLevel <= 5)
	{
	    PRINTD0("->USB Event Container\n");
		PRINTD1("->length  = [%ld]\n", usbEvent->Length);
		PRINTD1("->type    = [%04X]\n", usbEvent->Type);
		PRINTD1("->code    = [%04X]\n", usbEvent->Code);
		PRINTD1("->transID = [%08lX]\n", usbEvent->TransID);

		PRINTD1("->param1  = [%08lX]\n", usbEvent->Param1);
		PRINTD1("->param2  = [%08lX]\n", usbEvent->Param2);
		PRINTD1("->param3  = [%08lX]\n", usbEvent->Param3);
	}

	return;
}


void
PTPUSBIO::SetTransTimeout(int timeout)
{
    mpIfUSBHAL->SetTransTimeout(timeout);
}

int
PTPUSBIO::GetTransTimeout()
{
    return mpIfUSBHAL->GetTransTimeout();
}

int
PTPUSBIO::SdisetRequeset(PTPInfoType *ptpInfo)
{
    int ret;
    SdiInt32 size = 0;
    PTPUSBBulkContainerType usbCommand;
    PTPContainerType* ptpReq = &(ptpInfo->Request);

    if (ptpReq == NULL)
    {
        PRINTD0("-?>there is no command.\n");
        return PTPErrBadParam;
    }

    PTP_STRUCT_INIT(usbCommand);

    size = PTP_USB_BULK_REQ_LEN - sizeof(SdiInt32)*(5 - ptpReq->Nparam);

    /* build appropriate USB ptpCntr */

    usbCommand.Length  = (size);
    usbCommand.Type    = (PTP_USB_CONTAINER_COMMAND);
    usbCommand.Code    = (ptpReq->Code);
    usbCommand.TransID = (ptpReq->TransactionID);

    switch (ptpReq->Nparam)
    {
    
    case 5:
        usbCommand.Payload.Params.Param5 = (ptpReq->Param5);    
    case 4:
        usbCommand.Payload.Params.Param4 = (ptpReq->Param4);       
    case 3:
        usbCommand.Payload.Params.Param3 = (ptpReq->Param3);
    case 2:
        usbCommand.Payload.Params.Param2 = (ptpReq->Param2);
    case 1:
        usbCommand.Payload.Params.Param1 = (ptpReq->Param1);
    case 0:
        break;
    default:
        break;
    }

    /* send command to responder*/
	ret = mpIfUSBHAL->writeBulk((char *)&usbCommand, size);             // chlee


    if (ret != USBHAL_ERR_NONE)
    {
        PRINTD0("-?>Cannot write command.\n");
        return PTPErrUSBIOWrite;
    }

    return PTPErrNone;	
}

int
PTPUSBIO::SdigetResponse(PTPInfoType *ptpInfo)
{
    int	ret;
    SdiInt32 resSize = 0;
    PTPUSBBulkContainerType usbRes;
    PTPContainerType* ptpRes = &(ptpInfo->Response);
    PTPContainerType* ptpReq = &(ptpInfo->Request);

    if (ptpRes == NULL)
    {
        PRINTD0("-?>there is no response ptpCntr.\n");
        return PTPErrBadParam;
    }

    PTP_STRUCT_INIT(usbRes);

    /* read operation response, it should never be longer than sizeof(usbReq) */
	if(ptpReq->Code != 0x9002)
	{
		ret = mpIfUSBHAL->readBulk((char *)&usbRes, 	PTP_USB_BULK_HDR_LEN);          // chlee
	}
	else
	{
		ret = mpIfUSBHAL->readBulk((char *)&usbRes, 	PTP_USB_BULK_HDR_DATA_LEN);     // chlee
	}

    if (ret != USBHAL_ERR_NONE)
    {
        /*
        PRINTD0("-?>Cannot read response.\n");
        */
        return PTPErrUSBIORead;
    }

    if ((PTPEndian::ttod16(usbRes.Type) != PTP_USB_CONTAINER_DATA)&& (PTPEndian::ttod16(usbRes.Type) != PTP_USB_CONTAINER_RESPONSE))            // chlee 	
    {
        PRINTD0("-?>request type is not response.\n");
        return PTPErrNotContainerType;
    }

    /* build an appropriate PTPContainerType */
    ptpRes->Code = PTPEndian::ttod16(usbRes.Code);
    ptpRes->TransactionID = PTPEndian::ttod32(usbRes.TransID);

    /*Need to check whether the response transactionID and command transactionID are matched */
    if (ptpReq->TransactionID != ptpReq->TransactionID)
    {
        return PTPErrTransactionID;
    }
        
	
    ptpRes->Param1 = PTPEndian::ttod32(usbRes.Payload.Params.Param1);
	ptpRes->Param2 = PTPEndian::ttod32(usbRes.Payload.Params.Param2);
	ptpRes->Param3 = PTPEndian::ttod32(usbRes.Payload.Params.Param3);
	ptpRes->Param4 = PTPEndian::ttod32(usbRes.Payload.Params.Param4);
	ptpRes->Param5 = PTPEndian::ttod32(usbRes.Payload.Params.Param5);

    return PTPErrNone;
}



int
PTPUSBIO::SdisetData(PTPInfoType *ptpInfo, char **txData, SdiUInt32 size)
{
    int	ret;
    SdiInt32 ttSize = 0, tempSize = 0;
    PTPUSBBulkContainerType usbData;
    PTPContainerType* ptpCntr = &(ptpInfo->Request); 

    if ((ptpInfo == NULL) || (txData == NULL)) 
    {
        PRINTD0("-?>there is no ptpCntr or tx data.\n");
        return PTPErrBadParam;	
    }

    PTP_STRUCT_INIT(usbData);	

    /* build appropriate USB ptpCntr */
    ttSize = PTP_USB_BULK_HDR_LEN + size;

    PRINTD1("->write data, size:[%ld]" ,size);

    usbData.Length  = PTPEndian::dtot32(ttSize);
    usbData.Type    = PTPEndian::dtot16(PTP_USB_CONTAINER_DATA);
    usbData.Code    = PTPEndian::dtot16(ptpCntr->Code);
    usbData.TransID = PTPEndian::dtot32(ptpCntr->TransactionID);

#ifndef __PTP_NO_DEBUG__
    printUSBBulkContainer(&usbData);
#endif

    tempSize = (size < PTP_USB_BULK_PAYLOAD_LEN) ? size : PTP_USB_BULK_PAYLOAD_LEN;

    memcpy((char *)usbData.Payload.Data, (char *)txData, tempSize);

    /* send first part of data */
	ret = mpIfUSBHAL->writeBulk((char *)&usbData, (PTP_USB_BULK_HDR_LEN + tempSize));   // chlee

    if (ret != USBHAL_ERR_NONE)
    {
        PRINTD0("-?>Cannot write data.\n");
        return PTPErrUSBIOWrite;
    }

    if ((size - tempSize) > 0)
    {
        /* if everything OK, send the rest */
        ret = mpIfUSBHAL->writeBulk((char *)(txData + tempSize), (size - tempSize)); // chlee

        if (ret != USBHAL_ERR_NONE)
        {
            PRINTD0("-?>Cannot write the rest of data.\n");
            return PTPErrUSBIOWrite;
        }
    }

    return PTPErrNone;
}

int
PTPUSBIO::SdigetData(PTPInfoType *ptpInfo, char **rxdata, SdiUInt32 &size)
{
	char buf[513];						   // chlee 
    int nReadBytes							= 0;
    int nReponseReadBytes					= 0;
    
    PTPUSBBulkContainerType UsbData;
    
    if ((ptpInfo == NULL) || (rxdata == NULL))
    {
        PRINTD0("-?>there is no request or rx data.\n");
        return PTPErrBadParam;
    }

    PTPContainerType* req = &(ptpInfo->Request); 

	int ret = mpIfUSBHAL->readBulk((char*)&UsbData, PTP_USB_BULK_HDR_LEN);			// chlee   PTP_USB_BULK_HS_MAX_PACKET_LEN

    if (ret != USBHAL_ERR_NONE)
    {
        PRINTD0("-?>Cannot read data.\n");
        return PTPErrUSBIORead;
    }

    if (PTPEndian::ttod16(UsbData.Type) != PTP_USB_CONTAINER_DATA)
    {
        PRINTD0("-?>usb bulk container type is not data.\n");
        return PTPErrNotContainerType;
    }

    if (PTPEndian::ttod16(UsbData.Code) != req->Code)
    {
        PRINTD0("-?>bulk container code is not matched to request code.\n");
        return PTPErrInvalidOpCode;
    }

    /* evaluate data length */
    int ttSize = PTPEndian::ttod32(UsbData.Length);
	
	*rxdata = (char*)malloc(sizeof(char)*ttSize);

	ret = mpIfUSBHAL->readBulk(*rxdata, ttSize-12);
	size = ttSize-12;

/*
//	size = ttSize-12;

    uint32_t tempSize = (ttSize > PTP_USB_BULK_HDR_LEN) ? ttSize : PTP_USB_BULK_HS_MAX_PACKET_LEN;
    tempSize -= PTP_USB_BULK_HDR_LEN;    
    
    memcpy((char *)*rxdata, (char *)UsbData.Payload.Data,  tempSize);

    uint32_t remainSize = ttSize - tempSize - PTP_USB_BULK_HDR_LEN;

    if (remainSize > 0)
    {
        /* if everything OK, send the rest 

		int i;
		for(i=0;i<(remainSize/512);i++)
		{
			ret = mpIfUSBHAL->readBulk((char *)(buf), 512);
            
			memcpy((char *)*rxdata+tempSize+512*i, (char *)buf, 512);

			if (ret != USBHAL_ERR_NONE)
			{
				PRINTD0("-?>Cannot write the rest of data.\n");
				return PTPErrUSBIOWrite;
			}
		}
		if(remainSize%512!=0)
		{
			ret = mpIfUSBHAL->readBulk((char *)(buf), remainSize%512);
			memcpy((char *)*rxdata+tempSize+512*i, (char *)buf, remainSize%512);
			if (ret != USBHAL_ERR_NONE)
			{
				PRINTD0("-?>Cannot write the rest of data.\n");
				return PTPErrUSBIOWrite;
			}
		}
    }
  */  
    return PTPErrNone;

    
}
