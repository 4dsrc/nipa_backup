/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 * 
 */
/*!
 \file PTP_Endian.cpp
 \brief Implement PTP Endian Class used for endian converting.
 \date 2009-06-11
 \version 0.70 
*/

#include "stdafx.h"

#include "PTP_Endian.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//rujin 
#define LITTLE_ENDIAN 
 
int PTPEndian::msDevEndian = PTP_LITTLE_ENDIAN;

int PTPEndian::msTransEndian = PTP_LITTLE_ENDIAN;

/*
 * htodXX(a)  :  Hex-XX-data(LE or BE used in your machine) TO(->) Data(LE from ptp usb receive)
 *
 * ----XX  : 'XX' is data size
 * ------a : 'a' means that is array
 */
PTPEndian::PTPEndian()
{
}

PTPEndian::~PTPEndian()
{
}

void
PTPEndian::Init(PTPEnvironmentConfig* env)
{
	msDevEndian = env->getDeviceByteOrder();
	msTransEndian = env->getTransportByteOrder();

	return;
}

void
PTPEndian::term(void)
{
	msDevEndian = 0;
	msTransEndian = 0;
}

SdiUInt16
PTPEndian::changeEndian16(SdiUInt16 val)
{
	SdiUInt16 high16, low16;

	high16 = val & PTP_MASK_HIGH_16;
	low16 = val & PTP_MASK_LOW_16;
	
	return (SdiUInt16)((low16 << 8) | (high16 >> 8));
}

SdiUInt32
PTPEndian::changeEndian32(SdiUInt32 val)
{
	SdiUInt16 high16, low16;
	
	high16 = (SdiUInt16)((val & PTP_MASK_HIGH_32) >> 16);
	low16  = (SdiUInt16)(val & PTP_MASK_LOW_32);
	
	high16 = changeEndian16(high16);
	low16  = changeEndian16(low16);

	return (SdiUInt32)(((SdiUInt32)low16 << 16) | (SdiUInt32)high16);
}

SdiUInt64
PTPEndian::changeEndian64(SdiUInt64 val)
{
	SdiUInt32 high32, low32;

	high32 = (SdiUInt32)((val & PTP_MASK64_H) >> 32);
	low32  = (SdiUInt32)(val & PTP_MASK64_L);

	high32 = changeEndian32(high32);
	low32  = changeEndian32(low32);
	return (SdiUInt64)(((SdiUInt64)low32 << 32) | (SdiUInt64)high32);
}

SdiUInt16
PTPEndian::dtot16(SdiUInt16 val)
{
	if (msDevEndian == msTransEndian)
	{
		return val;
	}
	else
	{
		return changeEndian16(val);
	}
}

SdiUInt32
PTPEndian::dtot32(SdiUInt32 val)
{
	if (msDevEndian == msTransEndian)
	{
		return val;
	}
	else
	{
		return changeEndian32(val);
	}
}

SdiUInt64
PTPEndian::dtot64(SdiUInt64 val)
{
	if (msDevEndian == msTransEndian)
	{
		return val;
	}
	else
	{
		return changeEndian64(val);
	}
}

void
PTPEndian::dtot8a(SdiUInt8* a, SdiUInt8 val)
{
	*(SdiUInt8 *)a = val;
	return;
}

void
PTPEndian::dtot16a(SdiUInt8* a, SdiUInt16 val)
{
	int i, len;
	SdiUInt8 *ch_val = NULL;

	ch_val = (SdiUInt8 *)&val;
	len = sizeof(SdiUInt16);

	if (msDevEndian == msTransEndian)
	{
		for (i = 0; i < len; i++)
		{
			a[i] = ch_val[i];
		}
	}
	else
	{
		for (i = 0; i < len; i++)
		{
			a[i] = ch_val[(len - 1) - i];
		}
	}

	return;
}

void
PTPEndian::dtot32a(SdiUInt8* a, SdiUInt32 val)
{
	int i, len;
	SdiUInt8 *ch_val = NULL;

	ch_val = (SdiUInt8 *)&val;
	len = sizeof(SdiUInt32);

	if (msDevEndian == msTransEndian)
	{
		for (i = 0; i < len; i++)
		{
			a[i] = ch_val[i];
		}
	}
	else 
	{
		for (i = 0; i < len; i++)
		{
			a[i] = ch_val[(len - 1) - i];
		}
	}

	return;
}

void
PTPEndian::dtot64a(SdiUInt8* a, SdiUInt64 val)
{
	int i, len;
	SdiUInt8 *ch_val = NULL;

	ch_val = (SdiUInt8 *)&val;
	len = sizeof(SdiUInt64);

	if (msDevEndian == msTransEndian)
	{
		for (i = 0; i < len; i++)
		{
			a[i] = ch_val[i];
		}
	}
	else 
	{
		for (i = 0; i < len; i++)
		{
			a[i] = ch_val[(len - 1) - i];
		}
	}

	return;
}

/*
 * dtohXX  : Data(LE from ptp usb receive) TO(->) Hex-XX-data(LE or BE used in your machine)
 * ----XX  : 'XX' is data size
 * ------a : 'a' is array
 */
SdiUInt16
PTPEndian::ttod16(SdiUInt16 val)
{
#ifdef LITTLE_ENDIAN

	return val;
#else
	return dtot16(val);

#endif

}

SdiUInt32
PTPEndian::ttod32(SdiUInt32 val)
{
#ifdef LITTLE_ENDIAN

	return val;
#else


	return dtot32(val);

#endif
}

SdiUInt64
PTPEndian::ttod64(SdiUInt64 val)
{
#ifdef LITTLE_ENDIAN

	return val;
#else


	return dtot64(val);

#endif
}

SdiUInt8
PTPEndian::ttod8a(SdiUInt8* a)
{
	return *(SdiUInt8 *)a;
}

SdiUInt16
PTPEndian::ttod16a(SdiUInt8* a)
{
	int i, len;
	SdiUInt16 retval = 0;
	SdiUInt8 *ch_val = NULL;

	ch_val = (SdiUInt8 *)&retval;
	len = sizeof(SdiUInt16);
		
	if (msDevEndian == msTransEndian)
	{
		for (i = 0; i < len; i++)
		{
			ch_val[i] = a[i];
		}
	}
	else
	{
		for (i = 0; i < len; i++)
		{
			ch_val[i] = a[(len - 1) - i];
		}
	}
	
	return retval;
}

SdiUInt32
PTPEndian::ttod32a(SdiUInt8* a)
{
	int i, len;
	SdiUInt32 retval = 0;
	SdiUInt8 *ch_val = NULL;

	ch_val = (SdiUInt8 *)&retval;
	len = sizeof(SdiUInt32);
	
	//rujin

	//if(a==NULL)
	//	return retval;

	if (msDevEndian == msTransEndian)
	{
		for (i = 0; i < len; i++)
		{
			ch_val[i] = a[i];
		}
	}
	else
	{
		for (i = 0; i < len; i++)
		{
			ch_val[i] = a[(len - 1) - i];
		}
	}
	
	return retval;
}

SdiUInt64
PTPEndian::ttod64a(SdiUInt8* a)
{
	int i, len;
	SdiUInt64 retval = 0;
	SdiUInt8 *ch_val = NULL;

	ch_val = (SdiUInt8 *)&retval;
	len = sizeof(SdiUInt64);
		
	if (msDevEndian == msTransEndian)
	{
		for (i = 0; i < len; i++)
		{
			ch_val[i] = a[i];
		}
	}
	else
	{
		for (i = 0; i < len; i++)
		{
			ch_val[i] = a[(len - 1) - i];
		}
	}
	
	return retval;
}

void
PTPEndian::packString(char* toBuf, char* fromStr, SdiUInt16 offset, SdiUInt8* len)
{
	SdiUInt8 i, temp_len = 0;

	if (fromStr != NULL)
		temp_len = (SdiUInt8)strlen(fromStr);

	if ((fromStr == NULL) || (temp_len == 0))
	{
		*len = 0;
		dtot8a((SdiUInt8 *)&toBuf[offset], *len);
		return;
	}

	*len = temp_len + 1; /* 1 : include NULL */
	dtot8a((SdiUInt8 *)&toBuf[offset], *len);
	
	for (i = 0; (i < temp_len) && (i < PTP_MAX_STRING_LEN); i++)
	{
		dtot16a((SdiUInt8 *)&toBuf[offset + sizeof(SdiUInt16)*i + 1], (SdiUInt16)fromStr[i]);
	}
	return;
}

void
PTPEndian::packUINT16Array(char* toBuf, SdiUInt16* fromArray, SdiUInt16 offset, SdiUInt32 len)
{
	SdiUInt32 i = 0;

	dtot32a((SdiUInt8 *)&toBuf[offset], len);
	
	while (i < len)
	{
		dtot16a((SdiUInt8 *)&toBuf[offset + sizeof(SdiUInt16)*(i+2)], fromArray[i]);
		i++;
	}
	
	return;
}

void
PTPEndian::packUINT32Array(char* 	toBuf,
							  SdiUInt32* fromArray,
							  SdiUInt16	offset,
							  SdiUInt32	len,
							  SdiBool	dataOnly)
{
	SdiUInt32 i = 0;

	if (dataOnly == FALSE)
	{
		dtot32a((SdiUInt8 *)&toBuf[offset], len);

		while (i < len)
		{
			dtot32a((SdiUInt8 *)&toBuf[offset + sizeof(SdiUInt32)*(i+1)], fromArray[i]);
			i++;
		}
	}
	else
	{
		while (i < len)
		{
			dtot32a((SdiUInt8 *)&toBuf[offset + sizeof(SdiUInt32)*i], fromArray[i]);
			i++;
		}
	}

	return;
}

/* This for unpackObjectInfo*/
void
PTPEndian::unpackString(char* toStr, char* fromStr, SdiUInt16 offset, SdiUInt8* len)
{

	SdiUInt8 i, temp_len = 0;
#if 0
	if (fromStr == NULL)
	{
		*len = 0;
		dtot8a((SdiUInt8 *)&toStr[offset], *len);
		return;
	}

	if (fromStr != NULL)
	{
		SdiUInt16 index = offset; 
		while(fromStr[index] != NULL)
		{
			index++;
			temp_len++;			
		}
	}
	*len = temp_len + 1; /* 1 : include NULL */

	
	dtot8a((SdiUInt8 *)&toStr[offset], *len);
	//toStr[offset] = ttod8a((SdiUInt8 *)&fromStr[offset]);
	
	for (i = 0; (i < temp_len) && (i < PTP_MAX_STRING_LEN); i++)
	{
		dtot16a((SdiUInt8 *)&toStr[offset + sizeof(SdiUInt16)*i + 1], (SdiUInt16)fromStr[i]);
	}
	return;

#else
	*len = ttod8a((SdiUInt8 *)&fromStr[offset]);

	for (i = 0; (i < *len) && (i < PTP_MAX_STRING_LEN); i++)
	{
		toStr[i] = (char)ttod16a((SdiUInt8 *)&fromStr[offset + sizeof(SdiUInt16)*i + 1]);
	}
	
	return;
#endif

}
void
PTPEndian::unpackUINT16Array(char* toBuf, char* fromArray, SdiUInt16 offset, SdiUInt32 len)
{
	return;
}
void
PTPEndian::unpackUINT32Array(char* toBuf, char* fromArray, SdiUInt16 offset, SdiUInt32 len)
{
	return;
}

