/*
 * Project Name: SPID

 *  visual C++ express registration key = NBGMKDB4ZCGVDF 
 
 * Copyright 2009 by Samsung
 * All rights reserved.
 * 
 */
/*!
 \file PTP_Transport.cpp
 \brief Implement PTP Transport Class.
 \date 2009-06-11
 \version 0.70 
*/

#define __PTP_HOST__

#include "stdafx.h"
#include "PTP_Transport.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


PTPTransport::PTPTransport()
{
	m_TransContainerHdrLen = 0;
	
	m_Type = 0;
	memset(m_Name, 0, sizeof(char)*PTP_OBJECT_NAME_LEN);
		
	m_pTransIO = NULL;
}

PTPTransport::~PTPTransport()
{
}

int
PTPTransport::Init(PTPEnvironmentConfig* env)
{
	int ret, len = 0;

	m_Type = env->getTransportType();

	len = strlen(env->getTransportName());
	strncpy(m_Name, env->getTransportName(), len);

	switch (m_Type)
	{
		case PTP_TRANS_TYPE_USB:
		default:
		{
			PTPUSBIO* ptpUSB;
			
			ptpUSB = new PTPUSBIO;

			if (ptpUSB == NULL)
			{
				PRINTD0("-?>Cannot create PTPUSBIO class.\n");
				return PTPErrCannotCreateInstance;
			}

			m_pTransIO = (PTPTransportIO *)ptpUSB;

			m_TransContainerHdrLen = PTP_USB_BULK_HDR_LEN;
			break;
		}
	}

    // [3/9/2011 JanuS] Try Open Device
    int iRetryCount = 10;
    while (iRetryCount)
    {
	    ret = m_pTransIO->open(m_Name);
        if(ret == PTPErrNone)
        {
            break;
        }

        iRetryCount--;
        Sleep(1000);
    }
	
	if (ret != PTPErrNone)
	{
		PRINTD0("-?>Cannot open transport IO.\n");
		return PTPErrCannotOpenTransport;
	}
	
	return PTPErrNone;
}


void
PTPTransport::term(void)
{
	if (m_pTransIO != NULL)
	{
		m_pTransIO->close();
		
		switch (m_Type)
		{
			case PTP_TRANS_TYPE_USB:
			default:
			{
				PTPUSBIO *ptpUSB;
				
				ptpUSB = (PTPUSBIO *)m_pTransIO;
				delete(ptpUSB);

				break;
			}
		}
	}

	m_TransContainerHdrLen = 0;

	m_Type = 0;
	memset(m_Name, 0, sizeof(char)*PTP_OBJECT_NAME_LEN);
	
	m_pTransIO = NULL;
	
	return;
}



/* For HOST */
#ifdef __PTP_HOST__

int
PTPTransport::sendCommand(PTPInfoType* ptpInfo,
						 SdiUInt16      commandCode,
						 SdiUInt32      param1,
						 SdiUInt32      param2,
						 SdiUInt32      param3,
						 char          nparam)
{
	int ret;
	PTPContainerType command;

	PTP_STRUCT_INIT(command);
	
	command.Code = commandCode;
	command.SessionID = ptpInfo->SessionID;
	command.TransactionID = ptpInfo->Request.TransactionID;


	if(nparam==0) //rujin 11/01
	{
	
		command.Data[0]= 0xaa;
		command.Data[1]= 0xcc;
		command.Data[2]= 0xaa;
		command.Data[3]= 0xcc;
		
		memcpy(&command.Data[4],ptpInfo->Request.Data,strlen(ptpInfo->Request.Data));
	}
	else
	{
		command.Param1 = param1;
		command.Param2 = param2;
		command.Param3 = param3;
		command.Param4 = 0;
		command.Param5 = 0;
	}


	command.Nparam = nparam;
	memcpy(&ptpInfo->Request, &command, sizeof(PTPContainerType));

	PRINTD0("\n--->send command.\n");

	ret = m_pTransIO->sendCommand(&command);
	
	if (ret != PTPErrNone)
	{
		PRINTD0("-?>Cannot send command.\n");
		return PTPErrCannotSendCommand;
	}

#ifndef __PTP_NO_DEBUG__
	printPTPContainer(&command);
#endif
    
	return PTPErrNone;
}

int
PTPTransport::getResponse(PTPInfoType* ptpInfo)
{
	int ret;
	
	/* Reset PTP_Status_DPS */
//	ptpInfo->StatusForDPS = PTP_STATUS_DPS_NONACTION;
	ptpInfo->Request.SessionID = ptpInfo->SessionID;

	ret = m_pTransIO->getResponse(&(ptpInfo->Response));

	if(ptpInfo->Request.TransactionID != ptpInfo->Response.TransactionID)
	{
		return PTPErrCannoESMetResponse;
	}

	if (ret != PTPErrNone)
	{
		return PTPErrCannoESMetResponse;
	}
	else
		PRINTD0("\n--->get response.\n");
		
#ifndef __PTP_NO_DEBUG__
	printPTPContainer(&(ptpInfo->Response));
#endif

	return PTPErrNone;
}

#else

int
PTPTransport::getRequest(PTPInfoType* ptpInfo)
{
	int ret;
	
	/* Reset PTP_Status_DPS */
//	ptpInfo->StatusForDPS = PTP_STATUS_DPS_NONACTION;
	ptpInfo->Request.SessionID = ptpInfo->SessionID;

	ret = m_pTransIO->getRequest(&(ptpInfo->Request));

	if (ret != PTPErrNone)
	{
		//PRINTD1("-?>Cannot get request[%X].\n", ret);
		
		//m_pTransIO->reset(PTP_IO_RX);
		return PTPErrCannoESMetRequest;
	}
	else
		PRINTD0("\n--->get requst.\n");
		
#ifndef __PTP_NO_DEBUG__
	printPTPContainer(&(ptpInfo->Request));
#endif

	return PTPErrNone;
}

int
PTPTransport::sendResponse(PTPInfoType* ptpInfo,
	    					SdiUInt16      respCode,
	    					SdiUInt32      param1,
	    					SdiUInt32      param2,
	    					SdiUInt32      param3,
	    					char          nparam)
{
	int ret;
	PTPContainerType resp;

	PTP_STRUCT_INIT(resp);

	resp.Code = respCode;
	resp.SessionID = ptpInfo->SessionID;
	resp.TransactionID = ptpInfo->Request.TransactionID;
	resp.Param1 = param1;
	resp.Param2 = param2;
	resp.Param3 = param3;
	resp.Param4 = 0;
	resp.Param5 = 0;
	resp.Nparam = nparam;

	PRINTD0("--->send response.\n");
	
	ret = m_pTransIO->sendResponse(&resp);

	if (ret != PTPErrNone)
	{
		PRINTD0("-?>Cannot send response.\n");
		
		m_pTransIO->reset(PTP_IO_TX);
		return PTPErrCannotSendResponse;
	}

#ifndef __PTP_NO_DEBUG__
	printPTPContainer(&resp);
#endif

	return PTPErrNone;
}


int
PTPTransport::sendEvent(PTPInfoType* ptpInfo,
						 SdiUInt16      eventCode,
						 SdiUInt32      param1,
						 SdiUInt32      param2,
						 SdiUInt32      param3,
						 char          nparam)
{
	int ret;
	PTPContainerType event;

	PTP_STRUCT_INIT(event);
	
	event.Code = eventCode;
	event.SessionID = ptpInfo->SessionID;
	event.TransactionID = ptpInfo->Request.TransactionID;
	event.Param1 = param1;
	event.Param2 = param2;
	event.Param3 = param3;
	event.Param4 = 0;
	event.Param5 = 0;
	event.Nparam = nparam;

	PRINTD0("\n--->send event.\n");

	ret = m_pTransIO->sendEvent(&event);
	
	if (ret != PTPErrNone)
	{
		PRINTD0("-?>Cannot send event.\n");
		return PTPErrCannotSendEvent;
	}

#ifndef __PTP_NO_DEBUG__
	printPTPContainer(&event);
#endif

	return PTPErrNone;
}


int
PTPTransport::sendData(PTPInfoType	* ptpInfo,
						char*       data,
						SdiUInt32	size,
						SdiUInt32	totalSize)
{
	int ret;

	PRINTD0("--->send data.\n");

	ret = m_pTransIO->sendData(&(ptpInfo->Request), data, size, totalSize);

	if (ret != PTPErrNone)
	{
		PRINTD0("-?>Cannot send data.\n");
		
		m_pTransIO->reset(PTP_IO_TX);
		return PTPErrCannotSendData;
	}
	
	return PTPErrNone;	
}


int
PTPTransport::sendDataOnly(char* data, SdiUInt32 size, SdiBool bIsLastData)
{
	int ret;

	PRINTD0("--->send data only.\n");

	ret = m_pTransIO->sendDataOnly(data, size, bIsLastData);
	
	if (ret != PTPErrNone)
	{
		PRINTD0("-?>Cannot send data only.\n");
		
		m_pTransIO->reset(PTP_IO_TX);
		return PTPErrCannotSendDataOnly;
	}

	return PTPErrNone;	
}


#endif

#if 0   // duvallee 07-29-2010 : for LibUSB & Windows7
int PTPTransport::getData_Ex(PTPInfoType* ptpInfo, char* data, SdiUInt32	size, int opt, void* optParam)
{
    int ret;
    PRINTD0("--->get data.\n");

    ret = m_pTransIO->getData_Ex(&(ptpInfo->Request), data, size, opt, optParam);

    if (ret != PTPErrNone)
    {
        PRINTD0("-?>Cannot get data.\n");

        if(m_pTransIO==NULL)
        {
            return 0;
        }
        else
        {
            //	m_pTransIO->reset(PTP_IO_RX);
        }
        return PTPErrCannoESMetData;
    }
    return PTPErrNone;	
}
#endif


/* For BOTH */
int
PTPTransport::getData(PTPInfoType* ptpInfo,
						char*       data,
						SdiUInt32	size,
						int			opt,
						void*       optParam)
{
	int ret = PTPErrNone;

	PRINTD0("--->get data.\n");

	if(m_pTransIO)
		ret = m_pTransIO->getData(&(ptpInfo->Request), data, size, opt, optParam);
	else
		return 0;
	
	if (ret != PTPErrNone)
	{
		PRINTD0("-?>Cannot get data.\n");
		
		if(m_pTransIO==NULL)
		{
			return 0;
		}
		/*else
		{
		//	m_pTransIO->reset(PTP_IO_RX);
		}*/
		return PTPErrCannoESMetData;
	}

	return PTPErrNone;	
}

/* For BOTH */

int
PTPTransport::getConnectionStatus(void)
{
	//rujin new
	return 1;

	//return m_pTransIO->getConnectionStatus();
}


void
PTPTransport::printPTPContainer(PTPContainerType* ptpCntr)
{
	if (PTPEnvironmentConfig::msPTPDebugLevel <= 4)
	{
		PRINTD0("->PTP Container\n");
		PRINTD1("->Code          = [%04X]\n", ptpCntr->Code);
		PRINTD1("->SessionID     = [%08lX]\n", ptpCntr->SessionID);
		PRINTD1("->TransactionID = [%08lX]\n", ptpCntr->TransactionID);
		PRINTD1("->Param1        = [%08lX]\n", ptpCntr->Param1);
		PRINTD1("->Param2        = [%08lX]\n", ptpCntr->Param2);
		PRINTD1("->Param3        = [%08lX]\n", ptpCntr->Param3);
	}
	return;
}

#define CHECK_RC(result) { if(result != PTPErrNone)  return result; }

int
PTPTransport::Sditransaction_none(PTPInfoType* ptpInfo)
{
    int ret = m_pTransIO->SdisetRequeset(ptpInfo);
    CHECK_RC(ret);

    ret = m_pTransIO->SdigetResponse(ptpInfo);
    CHECK_RC(ret);
    
	//-- 2011-07-26 jeansu
	//return ret;
	return ptpInfo->Response.Code;
}

int
PTPTransport::Sditransaction_set(PTPInfoType* ptpInfo, char **pBuff, SdiUInt32 size)
{
    int ret = m_pTransIO->SdisetRequeset(ptpInfo);
    CHECK_RC(ret);

    ret = m_pTransIO->SdisetData(ptpInfo, pBuff, size);
    CHECK_RC(ret);

    ret = m_pTransIO->SdigetResponse(ptpInfo);
    CHECK_RC(ret);

    return ret;
}

int
PTPTransport::Sditransaction_get(PTPInfoType* ptpInfo, char **pBuff, SdiUInt32& size)
{
    int ret = m_pTransIO->SdisetRequeset(ptpInfo);
    CHECK_RC(ret);

    ret = m_pTransIO->SdigetData(ptpInfo, pBuff, size);         // chlee

    CHECK_RC(ret);

    ret = m_pTransIO->SdigetResponse(ptpInfo);
    CHECK_RC(ret);

    return ret;
}


void
PTPTransport::SetTransTimeout(int timeout)
{
    m_pTransIO->SetTransTimeout(timeout);
}

int
PTPTransport::GetTransTimeout()
{
    return m_pTransIO->GetTransTimeout();
}
