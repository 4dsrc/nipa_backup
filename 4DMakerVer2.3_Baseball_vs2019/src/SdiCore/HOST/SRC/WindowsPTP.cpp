/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        WindowsPTP.h
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */

#include "stdafx.h"
#include <WindowsPTP.h>

#define PTPUSB_DLLNAME _T("ptpusb.dll")
#define PTPUSD_DLLNAME _T("ptpusd.dll")

#define PTPUSB_CHANGENAME _T("ptpusb_.dll")
#define PTPUSD_CHANGENAME _T("ptpusd_.dll")


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
CWindowsPTP::CWindowsPTP()
{
	InitilizePTP_Path();
}

CWindowsPTP::~CWindowsPTP()
{

}

CWindowsPTP* 
CWindowsPTP::GetInstance()
{
	static CWindowsPTP WindowsPTP_Instance;

	return &WindowsPTP_Instance;
}

void 
CWindowsPTP::InitilizePTP_Path()
{
	TCHAR szSystemDir[MAX_PATH] = {0,};

	// system32 Directory의 경로획득
	GetSystemDirectory(szSystemDir,MAX_PATH);
	_tcscat(szSystemDir,_T("\\"));

	//ptpusb.dll ptpusd.dll의 fullpath
	_tcscpy(m_szPtpusb,szSystemDir);
	_tcscpy(m_szPtpusd,szSystemDir);
	_tcscat(m_szPtpusb,PTPUSB_DLLNAME);
	_tcscat(m_szPtpusd,PTPUSD_DLLNAME);

	//ptpusb_.dll ptpusd_.dll의 fullpath
	_tcscpy(m_szConvertPtpusb,szSystemDir);
	_tcscpy(m_szConvertPtpusd,szSystemDir);
	_tcscat(m_szConvertPtpusb,PTPUSB_CHANGENAME);
	_tcscat(m_szConvertPtpusd,PTPUSD_CHANGENAME);
}

BOOL 
CWindowsPTP::GetPTPStatus( void )
{
	HANDLE hPtpusb, hPtpusd;
	WIN32_FIND_DATA fd;

	// 두 개의 dll이 존재하는지 검사한다.
	hPtpusb = FindFirstFile(m_szPtpusb, &fd);
	hPtpusd = FindFirstFile(m_szPtpusd, &fd);

	if( hPtpusb != INVALID_HANDLE_VALUE )
	{
		//-- Code Sonar
		if(hPtpusb)		FindClose(hPtpusb);	
		if(hPtpusd)		FindClose(hPtpusd);	
		return TRUE;
	}

	if( hPtpusd != INVALID_HANDLE_VALUE )
	{
		//-- Code Sonar
		if(hPtpusb)		FindClose(hPtpusb);	
		if(hPtpusd)		FindClose(hPtpusd);	
		return TRUE;
	}

	return FALSE;
}

BOOL CWindowsPTP::SafeMoveFile( LPCTSTR lpExistingFileName, LPCTSTR lpNewFileName )
{
	if(!MoveFile(lpExistingFileName, lpNewFileName))
		return FALSE;

	DeleteFile(lpExistingFileName);

	return TRUE;
}

BOOL 
CWindowsPTP::SetPTPStatus( BOOL bStatus )
{
	HANDLE hPtpusb, hPtpusd;
	WIN32_FIND_DATA fd;

	// 두 개의 dll이 존재하는지 검사한다.
	hPtpusb = FindFirstFile(m_szPtpusb, &fd);
	hPtpusd = FindFirstFile(m_szPtpusd, &fd);

	// ptpusb_.dll -> ptpusb.dll 
	// ptpusd_.dll -> ptpusd.dll
	if(bStatus == TRUE)
	{
		if( hPtpusb == INVALID_HANDLE_VALUE )
		{
			if(!SafeMoveFile(m_szConvertPtpusb,m_szPtpusb))
			{
				if(hPtpusb)		FindClose(hPtpusb);	
				if(hPtpusd)		FindClose(hPtpusd);	
				return FALSE;
			}			
		}

		if( hPtpusd == INVALID_HANDLE_VALUE )
		{
			if(!SafeMoveFile(m_szConvertPtpusd,m_szPtpusd))
			{
				if(hPtpusb)		FindClose(hPtpusb);	
				if(hPtpusd)		FindClose(hPtpusd);	
				return FALSE;
			}		
		}
	}
	// ptpusb.dll -> ptpusb_.dll
	// ptpusd.dll -> ptpusd_.dll
	else
	{
		if( hPtpusb != INVALID_HANDLE_VALUE )
		{
			if(!SafeMoveFile(m_szPtpusb,m_szConvertPtpusb))
			{
				if(hPtpusb)		FindClose(hPtpusb);	
				if(hPtpusd)		FindClose(hPtpusd);	
				return FALSE;
			}		
		}

		if( hPtpusd != INVALID_HANDLE_VALUE )
		{
			if(!SafeMoveFile(m_szPtpusd,m_szConvertPtpusd))
			{
				if(hPtpusb)		FindClose(hPtpusb);	
				if(hPtpusd)		FindClose(hPtpusd);	
				return FALSE;
			}		
		}
	}

	// 파일 Path 변경시간
	Sleep(100);

	//-- Code Sonar
	if(hPtpusb)		FindClose(hPtpusb);	
	if(hPtpusd)		FindClose(hPtpusd);	

	return TRUE;
}
