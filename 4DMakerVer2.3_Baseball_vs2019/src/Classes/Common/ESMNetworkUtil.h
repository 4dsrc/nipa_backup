////////////////////////////////////////////////////////////////////////////////
//
//	TGNetworkUtil.h : implementation of the SMILE Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once



class CTGNetworkUtil
{
	CTGNetworkUtil();
	~CTGNetworkUtil();

public:
	static CString ConvertMACAddress(IN BYTE* macaddr);
	static CString ConvertMACAddress(IN CString macaddr);
	static void URLNavigate(CString url);
	static CString UriEncode(CString src);
	static CString UriDecode(CString src);
	static BOOL IsTCPListenPort(USHORT port);
};