////////////////////////////////////////////////////////////////////////////////
//
//	ESMTextProgressCtrl.h : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

class CESMTextProgressCtrl : public CProgressCtrl
{
// Construction
public:
	CESMTextProgressCtrl();

// Attributes
public:

// Operations
public:
    int			SetPos(int nPos);
    int			StepIt();
    void		SetRange(int nLower, int nUpper);
    int			OffsetPos(int nPos);
    int			SetStep(int nStep);
	void		SetForeColour(COLORREF col);
	void		SetBkColour(COLORREF col);
	void		SetTextForeColour(COLORREF col);
	void		SetTextBkColour(COLORREF col);
	COLORREF	GetForeColour();
	COLORREF	GetBkColour();
	COLORREF	GetTextForeColour();
	COLORREF	GetTextBkColour();

    void		SetShowText(BOOL bShow);

	int GetMax()   { return m_nMax; }
	int GetCurrentPos() { return m_nPos;}

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CESMTextProgressCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CESMTextProgressCtrl();

	// Generated message map functions
protected:
    int			m_nPos, 
				m_nStepSize, 
				m_nMax, 
				m_nMin;
    CString		m_strText;
    BOOL		m_bShowText;
    int			m_nBarWidth;
	COLORREF	m_colFore,
				m_colBk,
				m_colTextFore,
				m_colTextBk;

	//{{AFX_MSG(CESMTextProgressCtrl)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
    afx_msg LRESULT OnSetText(WPARAM, LPARAM szText);
    afx_msg LRESULT OnGetText(WPARAM cchTextMax, LPARAM szText);

	DECLARE_MESSAGE_MAP()
};
