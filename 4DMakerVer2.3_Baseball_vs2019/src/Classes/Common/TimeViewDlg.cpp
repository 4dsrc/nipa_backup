// TimeViewDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TimeViewDlg.h"

// CTimeViewDlg 대화 상자입니다.

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC(CTimeViewDlg, CDialog)

CTimeViewDlg::CTimeViewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTimeViewDlg::IDD, pParent)
	, m_strSeq1(_T(""))
	, m_strSeq2(_T(""))
	, m_strTime(_T(""))
{
	m_bThread = TRUE;
	m_pWThd = NULL;
}

CTimeViewDlg::~CTimeViewDlg()
{

}

void CTimeViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTimeViewDlg, CDialog)
	ON_WM_LBUTTONDOWN()
	ON_WM_NCHITTEST()
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CTimeViewDlg 메시지 처리기입니다.
void CTimeViewDlg::Create(CWnd *pParent)	
{ 
	CDialog::Create(CTimeViewDlg::IDD, pParent);
}
void CTimeViewDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	CPoint pt = point;
	ClientToScreen(&pt);
	PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(pt.x, pt.y));

	CDialog::OnLButtonDown(nFlags, point);
}

LRESULT CTimeViewDlg::OnNcHitTest(CPoint point)
{
	return HTCAPTION;
}

void CTimeViewDlg::SetSeq1(CString strSeq1)
{
	m_strSeq1 = strSeq1;
//	Invalidate(TRUE);
}

void CTimeViewDlg::SetSeq2(CString strSeq2)
{
	m_strSeq2 = strSeq2;
//	Invalidate(TRUE);
}

void CTimeViewDlg::InitTime()
{
// 	m_strSeq1 = _T("");
// 	m_strSeq2 = _T("");
	m_nStartTime = GetTickCount();

	m_StartTime = CTime::GetCurrentTime();
	m_StopWatch.Start();
	m_strStartTime.Format(_T("Start Time [ %02d : %02d : %02d ]"), m_StartTime.GetHour(), m_StartTime.GetMinute(), m_StartTime.GetSecond());
	m_bThread = TRUE;
	Invalidate(TRUE);
}

void CTimeViewDlg::EndTime()
{
	m_bThread = FALSE;
}

UINT CTimeViewDlg::TimeUpdate(LPVOID pParam)
{
	CTimeViewDlg* pUCAManager = (CTimeViewDlg*)pParam;

	while(1)
	{
		pUCAManager->UpdateTime();
		Sleep(20);
		if( pUCAManager->m_bThread == FALSE)
			return 0;
	}
	return 0;
}
BOOL CTimeViewDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_StartTime = CTime::GetCurrentTime();
	m_StopWatch.Start();
	m_strStartTime.Format(_T("Start Time [ %02d : %02d : %02d ]"), m_StartTime.GetHour(), m_StartTime.GetMinute(), m_StartTime.GetSecond());
	m_pWThd = AfxBeginThread(TimeUpdate, (LPVOID)(this));
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CTimeViewDlg::SetEndTime(int nTime)
{
	m_nEndTime = nTime;
	m_bTestMode = TRUE;
}

void CTimeViewDlg::UpdateTime()
{
	if(m_bTestMode)
	{
		if(m_StopWatch.GetDurationSecond()*1000 > m_nEndTime)
		{
			m_bTestMode = FALSE;
			ShowWindow(FALSE);
		}
	}

	CString strTimeGab;
	m_StopWatch.End();
	strTimeGab.Format(_T("Job Time   [    %03d : %02d ]"),(int)m_StopWatch.GetDurationSecond(), ((int)m_StopWatch.GetDurationMilliSecond()/10) % 100);
	m_strTime = strTimeGab;
	Invalidate(TRUE);
}
void CTimeViewDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CRect rect; 
	GetClientRect(&rect);

	CBrush brush1, brush2, *pOldBrush;
	brush1.CreateSolidBrush(RGB(20, 20, 20)); 
	brush2.CreateSolidBrush(RGB(70, 68, 65)); 

	CPen pen;
	dc.SetTextColor(RGB(230, 230, 230));
	pOldBrush = dc.SelectObject(&brush1);
	dc.Rectangle(rect);
	pOldBrush = dc.SelectObject(&brush2);
	dc.Rectangle(4,52,309,100);

	DeleteObject(brush1);
	DeleteObject(brush2);

	dc.SelectObject(pOldBrush); 

	dc.SetBkMode(TRANSPARENT);
	CFont *pOldFont, Font1, Font2;
	LOGFONT lf;
	::ZeroMemory(&lf, sizeof(lf));
	lf.lfHeight = 30;
	wsprintf(lf.lfFaceName, _T("%s"), _T("Arial"));
	Font1.CreateFontIndirect(&lf);
	pOldFont = dc.SelectObject(&Font1);
	dc.TextOut(30, 3, m_strSeq1);
	dc.SelectObject(pOldFont);
	lf.lfHeight = 20;
	wsprintf(lf.lfFaceName, _T("%s"), _T("Arial"));
	Font2.CreateFontIndirect(&lf);
	pOldFont = dc.SelectObject(&Font2);

	dc.TextOut(30, 30, m_strSeq2);
	dc.TextOut(75, 59, m_strStartTime);
	dc.TextOut(75, 79, m_strTime);
	dc.SelectObject(pOldFont);
}

BOOL CTimeViewDlg::DestroyWindow()
{
	m_bThread = FALSE;
	WaitForSingleObject(m_pWThd->m_hThread, 3000);
	return CDialog::DestroyWindow();
}
