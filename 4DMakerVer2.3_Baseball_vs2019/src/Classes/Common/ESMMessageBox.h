////////////////////////////////////////////////////////////////////////////////
//
//	ESMMessageBox.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-08-02
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
//TimeTrace
#include <sys/timeb.h>
#include <time.h>


#define DEFAULT_WAIT_TIME	1500

/////////////////////////////////////////////////////////////////////////////
// CESMMessageBox dialog

class CESMMessageBox : public CDialog
{
// Construction
public:
	CESMMessageBox(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CESMMessageBox)
	enum { IDD = IDD_ESM_MESSAGEBOX};		
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CESMMessageBox)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CESMMessageBox)

	//-- 2013-05-01 hongsu.jung
	//afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnTimer(UINT_PTR nIDEvent); //Fixed
	
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


/*---------------------------
 * Use this method & variable
 *---------------------------*/
private:
	BLENDFUNCTION	m_bf;			// AlphaBlend function parameter
	CBitmap			m_bitmap;		// Splash Bitmap
	
	
	int				m_nCount;	
	int				m_nWaitDraw;	// First Draw After this Milli second
	int				m_nAlphaDelay;	// Delay Alpha Blending
	int				m_nWidth;	
	int				m_nHeight;		// Splash Bitmap's width & height

	int				m_nDraw;
	int				m_nHide;

	CString			m_strComment;	

	//-- 2012-08-02 hongsu@esmlab.com
	//-- Start Time
	struct _timeb   m_tStart;

// Method
public:
	void Create(CWnd *pParent);		
	void OnShowMessage(CString strComment = _T(""), int nStartGap = 1000);		// Initialize variable.
	void OnHideMessage();

private:
	int GetTimeGap();
	void DrawString(CDC* pDC, CBitmap* pBmp, CRect* pRtBK);
	void DrawMessageBox();
	void HideMessageBox();
};
