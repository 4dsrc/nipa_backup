
#ifndef _SVNP_STRUCTS_H_
#define _SVNP_STRUCTS_H_

#include <time.h>

#include "svnp_define.h"

#ifdef WIN32
#pragma pack(push, old_pack_size, 1)
#endif

	const unsigned short g_cLenIPV4		= 4;
	const unsigned short g_cLenIPV6		= 8;
	const unsigned short g_cLenMacAddress		= 6;
	const unsigned short g_cLenObjectID = 10;
	const unsigned short g_cLenObectName1	= 20;
	const unsigned short g_cLenObectName2	= 41;
	const unsigned short g_cLenDeviceName	= 64;
	const unsigned short g_cLenDescription	= 64;
	const unsigned short g_cLenModelName	= 64;
	const unsigned short g_cLenGroup		= 64;
	const unsigned short g_cLenUserID		= 64;
	const unsigned short g_cLenData			= 512;
/**
 * @bf SVNP Header
 * @doc 3.1.1
 * @since 1.x
 */
struct SVNP_Header 
{
	enum { LEN_IDENTIFIER = 2, LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	identifier[LEN_IDENTIFIER];		//0 identifier[0] should be 0x01, identifier[1] should be 0X00
	UINT8	objectType;						//2 SVNPE_DEVICE_TYPE
	UINT16	modelID;						//3
	UINT8	objectID[LEN_OBJECT_ID];		//5
	UINT16	deviceSessionID;				//15
	UINT16	sequence;						//17
	UINT8	direction;						//19 SVNPE_DIRECTION
	UINT8	commandID;						//20
	UINT8	commandSub;						//21
	UINT8	commandType;					//22
	UINT32  length;							//23
	UINT8	isEncrypted	: 1;				//27
	UINT8	reserverd1	: 7;				//27
	UINT32	vSequence;						//28
	UINT32	vRequestor;						//32
	UINT8	connPairID;						//36
	UINT8	protocolRevisionID;				//37
	UINT16	rescode;						//38
	UINT16	headerCRC;						//40
};											//42

/**
 * @bf SVNP Media Header
 * @doc 3.1.2
 * @since 1.x
 */
struct SVNP_MediaHeader
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
#ifdef _BIG_ENDIAN_
	UINT32	reserved        : 1;
	UINT32	isPTZPosition   : 1;
	UINT32	isMetadata      : 1;
	UINT32	isSequenceReset : 1;
	UINT32	isMarker        : 1;
	UINT32	isExtension     : 1;
	UINT32	mediaType       : 6;  //SVNPE_MEDIA_TYPE
	UINT32	payloadLength   : 20;
	UINT8	minorVersion    : 4;
	UINT8	majorVersion    : 4;
#else
	UINT8	majorVersion    : 4;
	UINT8	minorVersion    : 4;
	UINT32	payloadLength   : 20;
	UINT32	mediaType       : 6;  //SVNPE_MEDIA_TYPE
	UINT32	isExtension     : 1;
	UINT32	isMarker        : 1;
	UINT32	isSequenceReset : 1;
	UINT32	isMetadata      : 1;
	UINT32	isPTZPosition   : 1;
	UINT32	reserved        : 1;
#endif
	UINT16	frameSequence;
	UINT16	packetSequence;
	UINT32	mediaSessionID;
	UINT8	objectID[LEN_OBJECT_ID];
};

/**
 * @bf SVNP Media Video Header
 * @doc 3.1.3
 * @since 1.x
 */
struct SVNP_MediaVideoHeader
{
	UINT8	  mediaType;        //SVNPE_MEDIA_TYPE
	UINT8	  codecType;        //SVNPE_MEDIA_CODEC_TYPE
	UINT16	  frameNumber;		//replay_sequence
	UINT32	  timeStamp;
#ifdef _BIG_ENDIAN_
	UINT32	  isMotionRegion  : 1;
	UINT32	  isMotion        : 1;
	UINT32	  isSensorIn      : 1;	//is_alarm
	UINT32	  isVideoloss     : 1;
	UINT32	  isAudiosrc      : 1;
	UINT32	  frameRate       : 5;
	UINT32	  frameType       : 2;  //SVNPE_FRAME_TYPE
	UINT32	  reserved        : 12;
	UINT32	  gopSize         : 8;
#else
	UINT32	  gopSize         : 8;
	UINT32	  reserved        : 12;
	UINT32	  frameType       : 2;  //SVNPE_FRAME_TYPE
	UINT32	  frameRate       : 5;
	UINT32	  isAudiosrc      : 1;
	UINT32	  isVideoloss     : 1;
	UINT32	  isSensorIn      : 1;	//is_alarm
	UINT32	  isMotion        : 1;
	UINT32	  isMotionRegion  : 1;
#endif
	UINT16	  frameWidth;
	UINT16	  frameHeight;
	struct tm frameTime;
	UINT32    frameTz;
	UINT32	  frameSize;
} ;

/**
 * @bf SVNP Media Audio Header
 * @doc 3.1.4
 * @since 1.x
 */
struct SVNP_MediaAudioHeader
{
	UINT8	mediaType;      //SVNPE_MEDIA_TYPE
	UINT8	codecType;      //SVNPE_MEDIA_CODEC_TYPE
	UINT32	timeStamp;
#ifdef _BIG_ENDIAN_
	UINT32	reserved   : 12;
	UINT32	frameSize  : 20;
#else
	UINT32	frameSize  : 20;
	UINT32	reserved   : 12;
#endif
} ;

/**
 * @bf SVNP Metadata Header
 * @doc 3.1.5
 * @since 1.x
 */
struct SVNP_MetadataHeader
{
	UINT32	metadataID;	   //SVNPE_METADATA_ID
	UINT8	reserved[12];
	UINT32	payloadLength; //PayloadLength
	UINT8*	payload;       //Payload[0]
} ;

/**
 * @bf Object ID
 * @doc 5.1.1
 * @since 1.x
 */
struct SCOMMON_ObjectID // SVNP_TARGet_OBJECT
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8 objectID[LEN_OBJECT_ID];	
};

/**
 * @bf Protocol version
 * @doc 5.1.2
 * @since 2.0
 */
struct SCOMMON_ProtocolVersion 
{
	UINT16	majorVersion;
	UINT16	minorVersion;	
};

/**
 * @bf Request login 1.x version
 * @doc 5.1.3
 * @since 1.x
 */
struct SCOMMON_RequestLogin // SVNPReqLogIN
{
	enum { LEN_ID = 21, LEN_PASSWORD = 21, LEN_MAC_ADDRESS = g_cLenMacAddress };
	UINT8  id[LEN_ID];
	UINT8  password[LEN_PASSWORD];
	UINT8  mode;
	UINT8  mac[LEN_MAC_ADDRESS];
	UINT16 dataCRC;
};

struct SCOMMON_RequestLogin2
{
	UINT8  mode;
	UINT8  mac[6];
	UINT8  reserved1;
	UINT8  id[64];
	UINT8  password[64];
	UINT8  reserved2[64];
};


/**
 * @bf Request login 2.x version
 * @doc 5.1.4
 * @since 2.0
 */
struct SCOMMON_LoginRequest
{
	enum { LEN_ID = g_cLenUserID, LEN_NONCE = 64, LEN_DATA = g_cLenData };
	UINT16	algorithmType;		// SVNPE_ALGORITHM_TYPE
	UINT32 	option;           //SVNPE_LOGIN_OPTION
	UINT8	userID[LEN_ID];
	UINT32	nonceLength;
	UINT8	nonce[LEN_NONCE];
	UINT32 	dataLength;
	UINT8   data[LEN_DATA];
	UINT8   reserved[64];
};

/**
 * @bf Audio Information
 * @doc 5.1.5
 * @since 1.x
 */
struct SCOMMON_AudioInfo
{
	UINT8	codecType;    //SVNPE_MEDIA_CODEC_TYPE
	UINT8	bitPerSample;
	UINT8	channelCount;
	UINT16	sampleRate;
	UINT16	packetSize;
};

/**
 * @bf Socket address information
 * @doc
 * @since 1.x
 */
struct SCOMMON_SockAddr
{
	enum {LEN_ADDR = 16};
	UINT8  len;
	UINT32 addrFamily;
	UINT16 port;
	UINT8  addr[LEN_ADDR];
};

/**
 * @bf Object Information
 * @doc
 * @since 1.x
 */
struct SCOMMON_ObjectAdvV200
{
	enum { LEN_OBJECT_ID = g_cLenObjectID, LEN_OBJECT_NAME = g_cLenObectName2};
	UINT8	      objectType;        // SVNPE_DEVICE_TYPE
	UINT8	      objectID[LEN_OBJECT_ID];
	UINT8	      objectParent[LEN_OBJECT_ID];
	UINT8	      objectName[LEN_OBJECT_NAME];
	UINT8	      possibilityStatus; // whether the video_profile is available or not
	UINT32	      objectCap;
	UINT32	      PTZCap;
	UINT32	      eventStatus;       // sensor-in, digital-out status
	UINT32	      playbackCap;
	SCOMMON_SockAddr multicastAddr;
};

/**
 * @bf Object List
 * @doc
 * @since 1.2
 */
struct SCOMMON_ObjectsAdvV200
{
	UINT16					count;
	SCOMMON_AudioInfo	    sListenInfo;
	SCOMMON_AudioInfo	    sTalkInfo;
	SCOMMON_ObjectAdvV200**	object;      // First pointer of Object
};

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
//                                 Capabilities                               //
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

/**
 * @bf Media request detail capability
 * @doc 5.1.6
 * @since 2.0
 */
struct SCOMMON_CapMediaRequestDetail
{
	UINT32 liveVideo        :1;
	UINT32 liveAudio        :1;
	UINT32 talk             :1;
	UINT32 upgrade          :1;
	UINT32 searchVideo      :1;
	UINT32 searchAudio      :1;
	UINT32 thumbnail        :1;
	UINT32 streamingTypeTCP :1;

	UINT32 streamingTypeUDPUnicast   :1;
	UINT32 streamingTypeUDPMulticast :1;
	UINT32 reserved1        :2;
	UINT32 reserved2        :4;

	UINT32 reserved3        :16;
};

/**
 * @bf Metadata Request detail capability
 * @doc 5.1.6
 * @since 2.0
 */
struct SCOMMON_CapMetadataRequestDetail
{
	UINT32 PTZ       :1;
	UINT32 pos       :1;
	UINT32 IV        :1;
	UINT32 reserved1 :1;
	UINT32 reserved2 :4;

	UINT32 reserved3 :24;
};

/**
 * @bf Camera OSD menu control detail Capability
 * @doc 5.1.6
 * @since 2.0
 */
struct SCOMMON_CapCameraOSDMenuDetail
{
	UINT32 reserved;
};

/**
 * @bf search detail Capability
 * @doc 5.1.6
 * @since 2.0
 */
struct SCOMMON_CapDataSearchDetail
{
	UINT32 subCmdCalendarSearch     :1;
	UINT32 subCmdDataSearch         :1;
	UINT32 subCmdDataSearchOverlap  :1;
	UINT32 recordTypeMD             :1;
	UINT32 recordTypeVLoss          :1;
	UINT32 recordTypeSensor         :1;
	UINT32 recordTypeSchedule       :1;
	UINT32 recordTypeManual         :1;

	UINT32 recordTypeIVPassing      :1;
	UINT32 recordTypeIVEntering     :1;
	UINT32 recordTypeIVExiting      :1;
	UINT32 recordTypeIVAppearing    :1;
	UINT32 recordTypeIVDisappearing :1;
	UINT32 recordTypeIVSceneChange  :1;
	UINT32 reserved1                :2;

	UINT32 reserved2                :16;
};

/**
 * @bf replay detail Capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapSearchControlDetail
{
	UINT32 pause          :1;
	UINT32 play           :1;
	UINT32 backPlay       :1;
	UINT32 seek           :1;
	UINT32 stepNextIFrame :1;
	UINT32 stepPrevIFrame :1;
	UINT32 stepNextFrame  :1;
	UINT32 getNextGOPData :1;

	UINT32 getPrevGOPData :1;
	UINT32 reserved1      :3;
	UINT32 reserved2      :4;

	UINT32 reserved3      :16;
};

/**
 * @bf Thumbnail detail Capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapThumbnailDetail
{
	UINT32 interval  :1;
	UINT32 imageSize :1;
	UINT32 reserved1 :2;
	UINT32 reserved2 :4;

	UINT32 reserved3 :24;
};

/**
 * @bf Search authority detail Capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapReplayAuthorityDetail
{
	UINT32 reserved;
};

/**
 * @bf metadata search detail Capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapQueryMetadataDetail
{
	UINT32 PTZ        :1;
	UINT32 IVRule     :1;
	UINT32 pos        :1;
	UINT32 motionArea :1;
	UINT32 reserved1  :4;

	UINT32 reserved2  :24;
};

/**
 * @bf PTZ control detail Capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapPTZDirDetail
{
	UINT32 pan       :1;
	UINT32 tilt      :1;
	UINT32 zoom      :1;
	UINT32 iris      :1;
	UINT32 focus     :1;
	UINT32 reserved1 :3;

	UINT32 reserved2 :24;
};

/**
 * @bf PTZ Preset detail Capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapPTZPresetDetail
{
	UINT32 subCmdMove       :1;
	UINT32 subCmdAdd        :1;
	UINT32 subCmdDelete     :1;
	UINT32 subCmdDeleteAll  :1;
	UINT32 subCmdGetList    :1;
	UINT32 subCmdGetListAdv :1;
	UINT32 reserved1		:1;
	UINT32 reserved2        :1;

	UINT32 reserved3		:24;
};


struct SCOMMON_CapPTZPresetDetail2
{
	UINT32 subCmdMove       :1;
	UINT32 reserved			:31;
};

/**
 * @bf autopan detail Capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapPTZAutoPanDetail
{
	UINT32 reserved;
};

/**
 * @bf swing detail Capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapPTZSwingDetail
{
	UINT32 reserved;
};

/**
 * @bf ptz scan detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapPTZScanDetail
{
	UINT32 reserved;
};

/**
 * @bf ptz tour detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapPTZTourDetail
{
	UINT32 reserved;
};

/**
 * @bf ptz pattern detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapPTZPatternDetail
{
	UINT32 reserved;
};

/**
 * @bf ptz position detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapPTZPosDetail
{
	UINT32 subCmdPTZGetPos          :1;
	UINT32 subCmdPTZSetPos          :1;
	UINT32 subCmdPTZGetPosPrecision :1;
	UINT32 subCmdPTZSetPosPrecision :1;
	UINT32 subCmdPTZAreaZoomIn      :1;
	UINT32 subCmdPTZMove            :1;
	UINT32 subCmdPTZSetHome         :1;
	UINT32 subCmdPTZGoHome          :1;

	UINT32 subCmdPTZGo1X            :1;
	UINT32 reserved1                :3;
	UINT32 reserved2                :4;

	UINT32 reserved3                :16;
};

/**
 * @bf clear all event detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapClearAllEventDetail
{
	UINT32 reserved;
};

/**
 * @bf digital out detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapDigitalOutDetail
{
	UINT32 reserved;
};

/**
 * @bf user defined event detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapUserDefinedEventDetail
{
	UINT32 reserved;
};

/**
 * @bf video info detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapVideoInfoDetail
{
	UINT32 getVideoInfo        :1;
	UINT32 getVideoSetting     :1;
	UINT32 setVideoSetting     :1;
	UINT32 getVideoCodecInfo   :1;
	UINT32 getVideoProfile     :1;
	UINT32 changeVideoProfile  :1;
	UINT32 addVideoProfile     :1;
	UINT32 deleteVideoProfile  :1;

	UINT32 reserved            :24;
};

/**
 * @bf audio info detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapAudioInfoDetail
{
	UINT32 getAudioCodecInfo  :1;
	UINT32 getAudioProfile    :1;
	UINT32 changeAudioProfile :1;
	UINT32 addAudioProfile    :1;
	UINT32 deleteAudioProfile :1;
	UINT32 reserved1          :3;

	UINT32 reserved2          :24;
};

/**
 * @bf layout into detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapLayoutInfoDetail
{
	UINT32 getProfile           :1;
	UINT32 setProfile           :1;
	UINT32 getProfileList       :1;
	UINT32 setProfileList       :1;
	UINT32 getDisplayLayout     :1;
	UINT32 setDisplayLayout     :1;
	UINT32 getDisplayLayoutList :1;
	UINT32 setDisplayLayoutList :1;

	UINT32 addCamera            :1;
	UINT32 deleteCamera         :1;
	UINT32 addDecoder           :1;
	UINT32 getSequenceList      :1;
	UINT32 setSequenceList      :1;
	UINT32 sequenceOn           :1;
	UINT32 sequenceOff          :1;
	UINT32 fullScreenOn         :1;
	UINT32 fullScreenOff        :1;

	UINT32 eventPopupOn         :1;
	UINT32 eventPopupOff        :1;
	UINT32 reserved1            :2;
	UINT32 reserved2            :4;

	UINT32 reserved3            :8;
};

/**
 * @bf system setting detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapSystemSettingDetail
{
	UINT32 subCmdDeviceSetting       :1;
	UINT32 subCmdTimeSetting         :1;
	UINT32 subCmdNETWORKSetting      :1;
	UINT32 subCmdUSERSSetting        :1;
	UINT32 subCmdChangedeviceSetting :1;
	UINT32 subCmdChangeTimeSetting   :1;
	UINT32 subCmdAddNetworkDevice    :1;
	UINT32 subCmdChangeNetworkConfig :1;

	UINT32 runningTime               :1;
	UINT32 changeLanguage            :1;
	UINT32 NTPSyncInterval           :1;
	UINT32 defaultGateway            :1;
	UINT32 deviceName                :1;
	UINT32 reserved1                 :3;

	UINT32 reserved2                 :16;
};

/**
 * @bf system log detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapSystemLogDetail
{
	UINT32 subCmdReqLogKeyword :1;
	UINT32 subCmdReqLogData    :1;
	UINT32 reserved1           :2;
	UINT32 reserved2           :4;

	UINT32 reserved3           :24;
};

/**
 * @bf login list detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapLoginListDetail
{
	UINT32 connectionDuration :1;
	UINT32 reserved1          :3;
	UINT32 reserved2          :4;

	UINT32 reserved3          :24;
};

/**
 * @bf ptz preset setup detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapPTZPresetSetupDetail
{
	UINT32 subCmdGet     :1;
	UINT32 subCmdSet     :1;
	UINT32 subCmdGetList :1;
	UINT32 subCmdSetList :1;
	UINT32 panDecimal    :1;
	UINT32 tiltDecimal   :1;
	UINT32 zoomDecimal   :1;
	UINT32 panSpeed      :1;

	UINT32 tiltSpeed     :1;
	UINT32 zoomSpeed     :1;
	UINT32 reserved1     :2;
	UINT32 reserved2     :4;

	UINT32 reserved3     :16;
};

/**
 * @bf ptz function setup detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapPTZFunctionSetupDetail
{
	UINT32 subCmdGetAutoPan     :1;
	UINT32 subCmdGetAutoPanList :1;
	UINT32 subCmdGetSwing       :1;
	UINT32 subCmdGetSwingList   :1;
	UINT32 subCmdGetGroup       :1;
	UINT32 subCmdGetGroupList   :1;
	UINT32 subCmdGetPattern     :1;
	UINT32 subCmdGetPatternList :1;

	UINT32 reserved             :24;
};

/**
 * @bf send serial data detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapSendSerialDataDetail
{
	UINT32 reserved;
};

/**
 * @bf camera setup detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapCameraSetupDetail
{
	UINT32 subCmdCameraSettingReq    :1;
	UINT32 subCmdCameraSettingChange :1;
	UINT32 dayAndNightMode           :1;
	UINT32 irisMode                  :1;
	UINT32 focusMode                 :1;
	UINT32 panTiltRelatedZoomMode    :1;
	UINT32 panTiltManualMode         :1;
	UINT32 digitalFlipMode           :1;

	UINT32 autoExecutionMode         :1;
	UINT32 operationTemperature      :1;
	UINT32 reserved1                 :2;
	UINT32 reserved2                 :4;

	UINT32 reserved3                 :16;
};

/**
 * @bf request auto scan detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapRequestAutoScanDetail
{
	UINT32 reserved;
};

/**
 * @bf get iv rule detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapGetIVRuleDetail
{
	UINT32 IVRuleLine       :1;
	UINT32 IVRuleArea       :1;
	UINT32 IVRuleFull       :1;
	UINT32 IVRuleSizeFilter :1;
	UINT32 reserved1        :4;

	UINT32 reserved2        :24;
};

/**
 * @bf device restart detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapDeviceRestartDetail
{
	UINT32 networkModule :1;
	UINT32 analogModule  :1;
	UINT32 zoomModule    :1;
	UINT32 reserved1     :1;
	UINT32 reserved2     :4;

	UINT32 reserved3     :24;
};

/**
 * @bf factory reset detail capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_CapDeviceFactoryResetDetail
{
	UINT32 excludeNetworkModule :1;
	UINT32 reserved1            :3;
	UINT32 reserved2            :4;

	UINT32 reserved3            :24;
};

/**
 * @bf Capability
 * @doc .1.6
 * @since 2.0
 */
struct SCOMMON_ObjectCapabilities
{
	UINT8 capMediaRequest;
	SCOMMON_CapMediaRequestDetail capMediaRequestDetail;

	UINT8 capMetadataRequest;
	SCOMMON_CapMetadataRequestDetail capMetadataRequestDetail;

	UINT8 capCameraOSDMenu;
	SCOMMON_CapCameraOSDMenuDetail capCameraOSDMenuDetail;

	UINT8 capDataSearch;
	SCOMMON_CapDataSearchDetail capDataSearchDetail;

	UINT8 capSearchControl;
	UINT8 capSearchControl2;
	SCOMMON_CapSearchControlDetail capSearchControlDetail;

	UINT8 capThumbnail;
	SCOMMON_CapThumbnailDetail capThumbnailDetail;

	UINT8 capReplayAuthority;
	SCOMMON_CapReplayAuthorityDetail capReplayAuthorityDetail;

	UINT8 capQueryMetadata;
	SCOMMON_CapQueryMetadataDetail capQueryMetadataDetail;

	UINT8 capPTZDir;
	SCOMMON_CapPTZDirDetail capPTZDirDetail;

	UINT8 capPTZPreset;
	SCOMMON_CapPTZPresetDetail capPTZPresetDetail;

	UINT8 capPTZPreset2;
	SCOMMON_CapPTZPresetDetail2 capPTZPresetDetail2;

	UINT8 capPTZAutoPan;
	SCOMMON_CapPTZAutoPanDetail capPTZAutoPanDetail;

	UINT8 capPTZSwing;
	SCOMMON_CapPTZSwingDetail capPTZSwingDetail;

	UINT8 capPTZScan;
	SCOMMON_CapPTZScanDetail capPTZScanDetail;

	UINT8 capPTZTour;
	SCOMMON_CapPTZTourDetail capPTZTourDetail;

	UINT8 capPTZPattern;
	SCOMMON_CapPTZPatternDetail capPTZPatternDetail;

	UINT8 capPTZPos;
	SCOMMON_CapPTZPosDetail capPTZPosDetail;

	UINT8 capPTZPos2;
	SCOMMON_CapPTZPosDetail capPTZPosDetail2;


	UINT8 capClearAllEvent;
	SCOMMON_CapClearAllEventDetail capClearAllEventDetail;

	UINT8 capDigitalOut;
	SCOMMON_CapDigitalOutDetail capDigitalOutDetail;

	UINT8 capUserDefinedEvent;
	SCOMMON_CapUserDefinedEventDetail capUserDefinedEventDetail;

	UINT8 capVideoInfo;
	SCOMMON_CapVideoInfoDetail capVideoInfoDetail;

	UINT8 capVideoInfo2;
	SCOMMON_CapVideoInfoDetail capVideoInfoDetail2;

	UINT8 capAudioInfo;
	SCOMMON_CapAudioInfoDetail capAudioInfoDetail;

	UINT8 capLayoutInfo;
	SCOMMON_CapLayoutInfoDetail capLayoutInfoDetail;

	UINT8 capSystemSetting;
	SCOMMON_CapSystemSettingDetail capSystemSettingDetail;

	UINT8 capSystemLog;
	SCOMMON_CapSystemLogDetail capSystemLogDetail;

	UINT8 capLoginList;
	SCOMMON_CapLoginListDetail capLoginListDetail;

	UINT8 capPTZPresetSetup;
	SCOMMON_CapPTZPresetSetupDetail capPTZPresetSetupDetail;

	UINT8 capPTZFunctionSetup;
	SCOMMON_CapPTZFunctionSetupDetail capPTZFunctionSetupDetail;

	UINT8 capSendSerialData;
	SCOMMON_CapSendSerialDataDetail capSendSerialDataDetail;

	UINT8 capCameraSetup;
	SCOMMON_CapCameraSetupDetail capCameraSetupDetail;

	UINT8 capRequestAutoScan;
	SCOMMON_CapRequestAutoScanDetail capRequestAutoScanDetail;

	UINT8 capGetIVRule;
	SCOMMON_CapGetIVRuleDetail capGetIVRuleDetail;

	UINT8 capDeviceRestart;
	SCOMMON_CapDeviceRestartDetail capDeviceRestartDetail;

	UINT8 capDeviceFactoryReset;
	SCOMMON_CapDeviceFactoryResetDetail capDeviceFactoryResetDetail;

};

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
//                                 Capabilities                               //
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//

/**
 * @bf Connection Information
 * @doc 5.1.7
 * @since 1.x
 */
union SCOMMON_ConnInfo
{
	UINT8 connPairID;                // for TCP
	struct SCOMMON_SockAddr clientAddr; // for UDP
};

/**
 * @bf Media request information
 * @doc 5.1.7
 * @since 1.x
 */
struct SCOMMON_MreqInfo
{
	UINT8 streamingType;
	union SCOMMON_ConnInfo connInfo;
};

/**
 * @bf Media request
 * @doc 5.1.7
 * @since 1.x
 */
struct SCOMMON_MediaRequest	//VNP_MEDIA_REQUEST_200
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8     cmd;
	UINT8     mode;
	UINT8     objectID[LEN_OBJECT_ID];
	struct tm startTime;   //time_start
	struct tm endTime;     //time_end
	UINT32    frameTz;
	UINT32    reserved;    //replay_frames
	UINT32    vTileID;
	UINT32    mediaSessionID;
	UINT32    eventType;
	SCOMMON_MreqInfo mediaRequestInfo;
};

/**
 * @bf Metadata request
 * @doc  5.1.8
 * @since 1.x
 */
struct SCOMMON_MetadataRequest
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8     cmd;
	UINT8     mode;
	UINT8     objectID[LEN_OBJECT_ID];
	struct tm startTime;
	struct tm endTime; 
	UINT32    frameTz;
	UINT32    reserved;
	UINT32    vTileID;
	UINT32    mediaSessionID;
	UINT32    metadataType;
	SCOMMON_MreqInfo info;
};

/**
 * @bf QOS information
 * @doc 5.1.9
 * @since 2.0
 */
struct SCOMMON_QoSInfo
{
	UINT8	  type;
	UINT8     connPairID;
	UINT16    qosInterval;
	UINT32     reserved1;
	UINT32    mediaSessionID;
	UINT32    skippedFrame;
	struct tm timeFrame;
	struct tm timeDecode;
};

/**
 * @bf IP address
 * @doc 5.1.10
 * @since 2.0
 */
struct SCOMMON_IPAddress
{
	enum { LEN_IPV4 = g_cLenIPV4, LEN_IPV6 = g_cLenIPV6 };
	UINT16 type;	//0x01 : IPV4, 0x02 : IPV6
	UINT16 reserved1;
	UINT8  ipv4Address[LEN_IPV4];
	UINT16 ipv6Address[LEN_IPV6];
	UINT16 reserved2;
};

/**
 * @bf multicast information
 * @doc 5.1.11
 * @since 2.0
 */
struct SCOMMON_MulticastInfo
{
	UINT32 TTL;
	SCOMMON_IPAddress ipAddress;
} ;

/**
 * @bf network host
 * @doc 5.1.12
 * @since 2.0
 */
struct SCOMMON_NetworkHost
{
	enum { LEN_IPV4 = g_cLenIPV4, LEN_IPV6 = g_cLenIPV6, LEN_DNS_NAME = 256 };
	UINT16 type;
	UINT16 reserved1;
	UINT8  ipv4Address[LEN_IPV4];
	UINT16  ipv6Address[LEN_IPV6];
	UINT16 reserved2;
	UINT8  DNSName[LEN_DNS_NAME];
} ;

/**
 * @bf Query Time
 * @doc 5.2.1
 * @since 1.2
 */
struct SREPLAY_QueryTime
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT32  recordType;    
	struct tm  startTime; 
	struct tm  endTime;   
	UINT32 frameTz;        
	UINT16  count;         
	UINT8*  objectID[LEN_OBJECT_ID]; 
};

/**
 * @bf Search Result Day
 * @doc 5.2.2
 * @since 1.2
 */
struct SREPLAY_ResultDay
{
	enum { LEN_DAYS = 31 };
	SREPLAY_QueryTime queryTag;
	UINT8 days[LEN_DAYS];
}  ;

/**
 * @bf Search Result Time List
 * @doc 5.2.3
 * @since 1.2
 */

struct SREPLAY_TimeLine
{
	struct tm startTime;
	struct tm endTime;
	UINT32	frameTz;
	UINT32	recordType;
};

struct SREPLAY_ResultTime
{
	enum { LEN_OBJECT_ID = g_cLenObjectID, LEN_OBJECT_NAME = g_cLenObectName1 };
	UINT8	devObjectID[LEN_OBJECT_ID];
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	objectName[LEN_OBJECT_NAME];
	UINT32	count;
	SREPLAY_TimeLine**	timeLine;	//count
};

struct SREPLAY_ResultTimeList
{
	UINT32	devCount; 
	SREPLAY_ResultTime** resultTime;//devCount
};

/**
 * @bf Time Line With Overlapped Res
 * @doc 5.2.4
 * @since 2.0
 */
struct SREPLAY_TimeLineResultWithOverlapRes
{
	enum { LEN_OBJECT_ID = g_cLenObjectID, LEN_OBJECT_NAME = g_cLenObectName1 };
	UINT16	overlappedDataIndex;
	SREPLAY_ResultTime timeLine;
};

struct SREPLAY_TimeLineWithOverlappedRes
{
	UINT32 overlappedDataMaxIndex;
	UINT32 count;
	SREPLAY_TimeLineResultWithOverlapRes*	resultTime;//count
};

/**
 * @bf Query Metadata Req
 * @doc 5.2.5
 * @since 2.0
 */
struct SREPLAY_QueryMetadataReq
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT16	reserved;
	struct tm startTime;
	struct tm endTime;
	UINT32	lengthOfData;
	UINT8*	pData;	//lengthOfData
};

/**
 * @bf Query Metadata Res
 * @doc 5.2.6
 * @since 2.0
 */
struct SREPLAY_QueryMetadataRes
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	dataIndex;
	UINT8	isLastData;
	UINT32	lengthOfData;
	UINT8*	pData;
} ;

/**
 * @bf Search Control
 * @doc 5.2.7
 * @since 1.2
 */

struct SREPLAY_SearchControl
{
	UINT8	cmd;
	UINT8	speed;
	struct tm timePos;	 //be valid only if (mtype  == search)//start_time
	UINT32	frameTz;
	UINT32	mode;
	UINT32	count;
	UINT32*	mediaSessionID;	//count
} ;

/**
 * @bf Search Control2
 * @doc 5.2.8
 * @since 2.0
 */
struct SREPLAY_SearchControl2
{
	UINT8	cmd;
	UINT8	speed;	
	struct tm timePos;	 //be valid only if (mtype  == search)
	UINT32	mode;
	UINT32	mediaSessionID;
	UINT16	overlappedDataIndex;
	UINT8	isAsyncMode;
	UINT16	asyncSessionCount;
	UINT32*	pAsyncMediaSessionIDList;	//asyncSessionCount
} ;

/**
 * @bf Query Thumbnail Req
 * @doc 5.2.9
 * @since 2.0
 */
struct SREPLAY_QueryThumbnailReq
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	struct tm startTime;
	struct tm endTime;
	UINT16 interval;
	UINT16 imageSize;
	UINT16 overlappedDataIndex;
	UINT32 numberOfObject;
	UINT8* objectID[LEN_OBJECT_ID]; //numberOfObject
} ;

/**
 * @bf Query Thumbnail Res
 * @doc 5.2.10
 * @since 2.0
 */
struct SREPLAY_QueryThumbnailRes
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT32	dataIndex;
	UINT8	isLastData;
	UINT8	objectID[LEN_OBJECT_ID];
	UINT16	overlappedDataIndex;
	struct tm dateTime;
	UINT32	lengOfData;
	UINT8*	pData; //lengOfData
} ;

/**
 * @bf Authority
 * @doc 5.2.11
 * @since 2.0
 */
struct SREPLAY_Authority //svnp_authority
{
	UINT8 authorityType;
	UINT8 reserved;
} ;

/**
 * @bf PTZ Value
 * @doc 5.3.1
 * @since 1.2
 */
struct SCONTROL_PTZValue // SVNP_PTZValue
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT32	value; // 0~100, -1
} ;

/**
 * @bf PTZ Unit
 * @doc 5.3.2
 * @since 1.2
 */
struct SCONTROL_PTZUnit // SVNP_PTZ_UNIT
{
	enum { LEN_OBJECT_ID = g_cLenObjectID, LEN_NAME = 16 };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	Index;
	UINT8	name[LEN_NAME];
};

/**
 * @bf PTZ List
 * @doc 5.3.3
 * @since 1.2
 */
struct SCONTROL_PTZList //SVNP_PTZ_LIST
{
	enum { LEN_OBJECT_ID = g_cLenObjectID, LEN_LIST = 255 };
	UINT8 objectID[LEN_OBJECT_ID];
	UINT8 count;
	SCONTROL_PTZUnit list[LEN_LIST];
};

/**
 * @bf PTZ List Adv
 * @doc 5.3.4
 * @since 1.2
 */
struct SCONTROL_PTZListAdv //SVNP_PTZ_LIST_ADV
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT16	totalPresetCount;
	UINT16	validPresetCount;
	SCONTROL_PTZUnit* list;	//validPresetCount
};

/**
 * @bf PTZ Pos
 * @doc 5.3.5
 * @since 1.2
 */
struct SCONTROL_PTZPos //SVNP_PTZ_POS
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8 objectID[LEN_OBJECT_ID];
	INT32 mode; 
	INT32 pan;
	INT32 tilt;
	INT32 zoom;
	INT32 flip;
	INT32 ceiling;
};

/**
 * @bf PTZ PosPrec
 * @doc 5.3.6
 * @since 1.2
 */
struct SCONTROL_PTZPosPrec //SVNP_PTZ_POS_PREC
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8 objectID[LEN_OBJECT_ID];
	INT32 panDec;
	INT32 panInt;
	INT32 tiltInt;
	INT32 tiltDec;
	INT32 zoomInt;
	INT32 zoomDec;
};

/**
 * @bf PTZ AreaZoomIn
 * @doc 5.3.7
 * @since 1.2
 */
struct SCONTROL_PTZAreaZoomIn //SVNP_PTZ_AREA_ZOOMIN
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8 objectID[LEN_OBJECT_ID];
	INT32 left; 
	INT32 top;
	INT32 right;
	INT32 bottom;
	INT32 screenWidth;
	INT32 screenHeight;
	INT32 srcWidth;
	INT32 srcHeight;
};

/**
 * @bf PTZ Move
 * @doc 5.3.8
 * @since 1.2
 */
struct SCONTROL_PTZMove  //SVNP_PTZ_MOVE
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8 objectID[LEN_OBJECT_ID];
	INT32 panSpeed;
	INT32 tiltSpeed;
	INT32 zoomSpeed;
	INT32 reserved;
};

/**
 * @bf PTZ Point
 * @doc 5.3.9
 * @since 2.0
 */

struct SCONTROL_PTZPoint
{
	UINT16 panIntger;
	UINT16 panDecimal;
	UINT16 tiltInteger;
	UINT16 tiltDecimal;
	UINT16 zoomInteger;
	UINT16 zoomDecimal;
};

struct SCONTROL_PTZSpeed
{
	UINT16 panIntger;
	UINT16 panDecimal;
	UINT16 tiltInteger;
	UINT16 tiltDecimal;
	UINT16 zoomInteger;
	UINT16 zoomDecimal;
};

struct SCONTROL_PTZPointWithSpeed
{
	SCONTROL_PTZPoint ptzPoint;
	SCONTROL_PTZSpeed ptzSpeed;
};

/**
 * @bf Preset Unit
 * @doc 5.3.10
 * @since 2.0
 */
struct SCONTROL_PresetUnit
{
	enum { LEN_NAME = 64 };
	UINT32 index;
	SCONTROL_PTZPointWithSpeed ptzPoint;
	UINT32	name[LEN_NAME];
};

struct SCONTROL_PresetList
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8 objectID[LEN_OBJECT_ID];
	UINT32	maxNumOfPresetUnits;
	UINT32	numOfPresetUnits;
	SCONTROL_PresetUnit* presetUnits;	//numOfPresetUnits
} ;

/**
 * @bf Function Unit
 * @doc 5.3.11
 * @since 2.0
 */
struct SCONTROL_FunctionUnit 
{
	enum { LEN_CHILD_PTZ_POINT = 2, LEN_CHILD_FUNCTION_INDEX = 2, LEN_NAME = 64 };
	UINT32	indexFunctionUnit;
	UINT32	childFunctionType;
	SCONTROL_PTZPoint childPTZPoint[LEN_CHILD_PTZ_POINT];   // Preset, AutoPan
	UINT32	childFunctionIndex[LEN_CHILD_FUNCTION_INDEX]; // If Swing, the available length 	// of the array is 2, otherwise 1
	UINT32	dwellTime;		// Swing, Group, Tour
	UINT32	speed;			// Swing, Group
	UINT32	mode;			// AutoPan: endless	// Swing: pan, tilt, pan&tilt
	UINT32	direction; 		// AutoPan: left, right
	UINT8	name[LEN_NAME];
};

struct SCONTROL_FuctionUnitList
{
	UINT32 functionIndex;
	UINT32 functionType;
	UINT32 maxNumOfChildFunctionUnits;
	UINT32 numOfChildFunctionUnits;
	SCONTROL_FunctionUnit* pChildFunctionUnits;
};

struct SCONTROL_ParentFuctionList
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT16	reserved;
	UINT32	functionType;
	UINT32	maxNumOfParentFunctionUnits;
	UINT32	numOfParentFunctionUnits;
	SCONTROL_FunctionUnit* pParentFunctionUnits;
};
	
/**
 * @bf Function Unit
 * @doc 5.3.12
 * @since 2.0
 */
struct SCONTROL_FunctionGetReq
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT16	reserved;
	UINT32	functionType;
	UINT32	functionIndex;
};

struct SCONTROL_FunctionSetReq
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT16	reserved;
	UINT32	parentFunctionType;
	SCONTROL_FuctionUnitList	childFunctionUnit;
};

/**
 * @bf Camera Control
 * @doc 5.3.13
 * @since 2.0
 */
struct SCONTROL_CameraControl
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT16	reserved;
	UINT32	type;
	UINT32	operation;
	struct tm timeOfPC;
};

/**
 * @bf Serial Data
 * @doc 5.3.14
 * @since 1.2
 */
struct SCONTROL_SerialData
{
	UINT32 sendDataLength;
	UINT32 recvDataLength;
	UINT8* message;
} ;

/**
 * @bf Digital Out Control
 * @doc 5.4.1
 * @since 1.2
 */
struct SEVENT_DigitalOutControl
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8 objectID[LEN_OBJECT_ID];
	UINT8 eventStatus;
};

/**
 * @bf User Defined Event Req
 * @doc 5.4.2
 * @since 2.0
 */
struct SEVENT_UserDefinedEventReq
{
	UINT32	dataLength;
	UINT8*	pData;
} ;

/**
 * @bf Video List
 * @doc 5.5.1
 * @since 1.2
 */
struct SCONFIG_Resolution
{
	UINT16 width;
	UINT16 height;
} ;

struct SCONFIG_CodecInfo
{
	UINT8 codecId;  // Index . start from 1
	UINT8 changedItem;  // fixed to 0x00
	UINT16 CodecType;   
	UINT8 qualityCount; 
	UINT8 qualityValue; 
	UINT8 frameRateCount;  
	UINT8 frameRateValue;  
	UINT8 resolutionCount; 
	UINT8 resolutionValue; 
	UINT16** frameRateInfo;//frameRateCount 
	SCONFIG_Resolution** resolutionInfo;//resolutionCount
} ;

struct SCONFIG_VideoInfo
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8 objectID[LEN_OBJECT_ID];  
	UINT8 codecCount;  
	UINT8 currentCodecID;
	SCONFIG_CodecInfo** sCodecInfo;//codecCount 
} ;

struct SCONFIG_VideoList
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8 objectID[LEN_OBJECT_ID];  
	UINT8 videoCount;  
	SCONFIG_VideoInfo** VideoInfo;//videoCount
} ;

/**
 * @bf Video Setting
 * @doc 5.5.2
 * @since 1.2
 */
struct SCONFIG_VideoSetting
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8 objectID[LEN_OBJECT_ID];
	UINT16 changedType; 
	UINT16 codecType;  
	UINT16 frameRate;  
	UINT16 quality;  
	SCONFIG_Resolution resolution;
} ;

/**
 * @bf Device FW Version
 * @doc 5.5.3
 * @since 1.2
 */
struct SCONFIG_DeviceFWVersion //SVNP_VERSION
{
	enum { LEN_VERSION = 21 };
	UINT8	version[LEN_VERSION];
} ;

/**
 * @bf IV Rule Info
 * @doc 5.5.4
 * @since 1.2
 */
struct SCONFIG_IVRuleObject
{
	enum { LEN_STATUS = 4, LEN_X = 11, LEN_Y = 11};
	INT32 ID;			// ObjectID, Priority
	INT32 type;		// refer to the SVNP_IV_RULE_TYPE
	INT32 status[LEN_STATUS];	// refer to the SVNP_IV_RULE_LINE/AREA_TYPE 
	INT32 X[LEN_X];		// x position of each vertex
	INT32 Y[LEN_Y];		// y position of each vertex
	INT32 pointCnt;		// number  of vertex of the object
	INT32 enable; 		//
};

// Full View Based Rules 
struct SCONFIG_IVRuleFull
{
	enum { LEN_STATUS = 3 };
	INT32 type;		// refer to SVNP_IV_RULE_TYPE 
	INT32 status[LEN_STATUS];    // refer to SVNP_IV_RULE_FULL_TYPE
	INT32 enable;       
};

// Sensitivity 
struct SCONFIG_IVRuleSensitivity
{
	INT32 step;	// Defalut number is 3. Range(1~5)		
}; 

// Size Filter ( controllable factor : minimum area, maximum area)
struct SCONFIG_IVRuleSizeObject
{
	INT32 type;                     // refer to SVNP_IV_RULE_TYPE
	INT32 sizeFilterType;	// MINs=1, MAXs
	INT32 blobWidth;	// width of minimum area or maximum area
	INT32 blobHeight;// height of minimum area or maximum area
	INT32 enable;	//09.02.05 by JY
};

struct SCONFIG_IVRuleInfo
{
	enum { LEN_SIZE_OBJECT = 2, LEN_LINE_OBJECT = 3, LEN_AREA_OBJECT = 3 };
	SCONFIG_IVRuleObject		IVLineObject[3];
	SCONFIG_IVRuleObject		IVAreaObject[3];
	SCONFIG_IVRuleFull			IVFullObject;
	SCONFIG_IVRuleSizeObject	IVSizeObject[2];
	SCONFIG_IVRuleSensitivity	IVSensitivity;
	INT32	lineObjectCnt;
	INT32	areaObjectCnt;
	INT32	isFullViewSet;
	INT32	isSizeFilterSet;
	UINT32	IVCommand; 
	INT32	activated;   // atcivation  of I/F detection function
	INT32	isCount;		/**< 06/10 사용하지 않음 */
};

/**
 * @bf Video Codec Profile Info
 * @doc 5.5.5
 * @since 2.0
 */

struct SCONFIG_VideoCodecInfo
{
	UINT16	codecIndex;  // Index . start from 1
	UINT16	codecType;   
	UINT16	qualityCount; // 
	UINT32	encodingMode;// CBR/VBR
	UINT32	targetMinBitrate;	// kbps단위
	UINT32	targetMaxBitrate;	// kbps단위
	UINT16	GOPMinSize;
	UINT16	GOPMaxSize;
	UINT8	reserved[64];
	UINT16	encodingPriority;	// Quality / Frame Rate
	UINT16	frameRateCount;  
	UINT16	resolutionCount; 
	UINT32*	frameRateInfo;//frameRateCount
	SCONFIG_Resolution*	resolutionInfo;//ResolutionCount
} ;

struct SCONFIG_VideoCodecInfoList
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT32 responseSequenceIndex; 
	UINT32 responseMode;

	UINT8 objectID[LEN_OBJECT_ID];  
	UINT8 reserved1[2];
	UINT8 reserved2[64];
	UINT32 codecCount;  
	SCONFIG_VideoCodecInfo* pCodecInfo; //  videoCount 만큼 존재
} ;

/**
 * @bf Video Info
 * @doc 5.5.6
 * @since 2.0
 */
struct SCONFIG_VideoProfileInfo
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	UINT8	sFixed;
	UINT16	fixedItems;
	UINT16	videoProfileIndex;
	UINT16	codecIndex;
	UINT16	codecType;  
	UINT16	frameRate;  
	UINT16	quality;  
	UINT16	encodingMode;// CBR/VBR
	UINT32	targetBitrate;	// kbps단위
	UINT32	GOPSize;
	UINT32	encodingPriority;	// Quality / Frame Rate
	UINT16	resolutionWidth;
	UINT16	resolutionHeight;
	SCOMMON_MulticastInfo multicastInfo;
	UINT8	reserved2[64];
} ;

struct SCONFIG_VideoInfo2
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];  
	UINT8	reserved1[2];
	UINT8	reserved2[64];
	UINT32	totalSupportProfileCount;
	UINT32	availableProfileCount;
	SCONFIG_VideoProfileInfo*	pAvailableProfileInfo;
} ;

struct SCONFIG_VideoInfoList
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT32	responseSequenceIndex; 
	UINT32	responseMode;
	UINT8	objectID [LEN_OBJECT_ID];  
	UINT8	reserved1[2];
	UINT8	reserved2[64];
	UINT32	videoCount;  
	SCONFIG_VideoInfo2*	pVideoInfo; //videoCount
} ;

/**
 * @bf Video Profile Setting
 * @doc 5.5.7
 * @since 2.0
 */

struct SCONFIG_VideoProfileSetting
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	UINT16	videoProfileIndex;
	UINT16	changedItem;
	UINT16	codecType;  
	UINT16	frameRate;  
	UINT16	quality;  
	UINT16	encodingMode;// CBR/VBR
	UINT32	targetBitrate;	// kbps단위
	UINT32	GOPSize;
	UINT32	encodingPriority;	// Quality / Frame Rate
	UINT16	resolutionWidth;
	UINT16	resolutionHeight;
	SCOMMON_MulticastInfo	multicastInfo;
} ;

/**
 * @bf Audio Codec Info
 * @doc 5.5.8
 * @since 2.0
 */
struct SCONFIG_AudioProfileInfo
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	UINT16	audioProfileIndex;
	UINT16	codecIndex;
	UINT16	audioCodecType;
	UINT16	bitPerSample;
	UINT16	channelCount;
	UINT16	sampleRate;
	UINT16	blockCount;
	UINT16	packetSize;
	SCOMMON_MulticastInfo multicastInfo;
};

struct SCONFIG_AudioCodec
{
	UINT16	codecIndex; // Index. Start from 1
	UINT16	audioCodecType;
	UINT16	bitPerSample;
	UINT16	channelCount;
	UINT16	sampleRate;
	UINT16	blockSize;
	UINT32	packetSize;
	UINT8	reserved1[64];
};

struct SCONFIG_AudioCodecList
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT32	responseSequenceIndex;
	UINT32	responseMode;
	UINT8	parentObjectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	UINT32	audioCodecCount; 
	SCONFIG_AudioProfileInfo* audioCodecInfo; //audioCodecCount
} ;

/**
 * @bf Audio Info
 * @doc 5.5.9
 * @since 2.0
 */
struct SCONFIG_AudioInfo
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];  
	UINT8	reserved1[2];
	UINT8	reserved2[64];
	UINT32	totalSupportProfileCount;
	UINT32	availableProfileCount;
	SCONFIG_AudioProfileInfo*	pAudioProfileInfo; //availableProfileCount
} ;

struct SCONFIG_AudioInfoList
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT32	responseSequenceIndex; 
	UINT32	responseMode;
	UINT8	objectID[LEN_OBJECT_ID];  
	UINT8	reserved1[2];
	UINT8	reserved2[64];
	UINT32	audioCount;  
	SCONFIG_AudioInfo*	pAudioInfo; // audioCount
} ;

/**
 * @bf Audio Setting Change
 * @doc 5.5.10
 * @since 2.0
 */
struct SCONFIG_AudioSettingChange
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	UINT8	reserved2[64];
	UINT32	audioProfileIndex;
	UINT32	audioCodecIndex;
} ;

/**
 * @bf Display Layout
 * @doc 5.5.11
 * @since 2.0
 */
struct SCONFIG_LayoutPoint
{
	UINT16	x;
	UINT16	y;
} ;

struct SCONFIG_LayoutTile
{
	enum { LEN_POINT = 2, LEN_OBJECT_ID = g_cLenObjectID };
	UINT16	tileIndex;
	UINT8	objectID[LEN_OBJECT_ID];
	UINT16	zOrder;
	SCONFIG_LayoutPoint points[LEN_POINT];
	UINT32	reserved[2];
} ;

struct SCONFIG_LayoutProfile
{
	enum { LEN_OBJECT_ID = g_cLenObjectID, LEN_DESCRIPTION = g_cLenDescription, LEN_TILES = 256 };
	UINT16	layoutProfileIndex;
	UINT16	width;
	UINT16	height;
	UINT16	objectID[LEN_OBJECT_ID];
	UINT8	description[LEN_DESCRIPTION];
	UINT16	numberOfTiles;
	SCONFIG_LayoutTile	tiles[LEN_TILES];
} ;

struct SCONFIG_LayoutProfileList
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT16	reserved;
	UINT16	numberOfLayoutProfiles;
	SCONFIG_LayoutProfile*	layoutProfiles;//numberOfLayoutProfiles
};

struct SCONFIG_DisplayLayout
{
	enum { LEN_OBJECT_ID = g_cLenObjectID, LEN_LAYOUT_INDEXES = 16 };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT16	reserved;
	UINT16	numberOfLayouts;
	UINT16	layoutIndexes[LEN_LAYOUT_INDEXES];
};

struct SCONFIG_DisplayLayoutList
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT16	reserved;
	UINT16	numberOfDisplayLayouts;
	SCONFIG_DisplayLayout*	displayLayouts;
};

struct SCONFIG_LayoutProfileIndex
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT16	ayoutProfileIndex;
};

/**
 * @bf Device Config
 * @doc 5.5.12
 * @since 2.0
 */
struct SCONFIG_DeviceConfig
{
	enum { LEN_OBJECT_ID = g_cLenObjectID, LEN_SERIAL_NUMBER = 64, LEN_FIRMWARE_VERSION = 64,
		LEN_MODEL_NAME = g_cLenModelName, LEN_DEVICE_NAME = g_cLenDeviceName, LEN_SUPPORT_LANGUAGE = 256 };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	UINT8	serialNumber[LEN_SERIAL_NUMBER];
	UINT8	firmwareVersion[LEN_FIRMWARE_VERSION]; 
	UINT8	modelName[LEN_MODEL_NAME];
	UINT16	protocolMajorVersion;
	UINT16	protocolMinorVersion;
	UINT8	deviceName[LEN_DEVICE_NAME];
	UINT32	reserved2;
	UINT32	languageID;
	UINT8	supportLanguages[LEN_SUPPORT_LANGUAGE];
	UINT32	extraDataLength;
	UINT8*	pExtraData;
} ;

/**
 * @bf Network Config
 * @doc 5.5.13
 * @since 2.0
 */
struct SCONFIG_NICConfig
{
	enum { LEN_MAC_ADDRESS = g_cLenMacAddress };
	UINT32	NICIndex;
	UINT32	purposeOfNIC;	
	UINT8	isDefaultGateway;
	UINT8	macAddress[LEN_MAC_ADDRESS];
	UINT8	reserved1;
	SCOMMON_IPAddress	ipAddress;
	SCOMMON_IPAddress	subnetMask;
	SCOMMON_IPAddress	gateway;
	UINT8	reserved2[32];
	SCOMMON_IPAddress	dns1Address;
	SCOMMON_IPAddress	dns2Address;
	UINT8	reserved3[32];
} ;

struct SCONFIG_NetworkConfig
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	UINT32	portWeb;
	UINT32	portRTSP;
	UINT32	portSVNP;
	UINT8	reserved2[32];
	UINT32	numberOfNIC;
	SCONFIG_NICConfig*	infoOfNIC;
	UINT32	extraDataLength;
	UINT8*	pExtraData;
} ;

/**
 * @bf Time Config
 * @doc 5.5.14
 * @since 2.0
 */
struct SCONFIG_TimeConfig
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	INT16	timeZoneIndex;
	UINT32	useDst;
	struct tm	dstStartTime;
	struct tm	dstEndTime;
	UINT32	useNTP;
	UINT32  NTPServerMaxCount;
	UINT32	NTPServerCount;
	SCOMMON_NetworkHost*	NTPServerList;//NTPServerCount
	UINT32	NTPSyncInteval;
	UINT32	extraDataLength;
	UINT8*	extraData;//extraDataLength
} ;

/**
 * @bf User ID Config
 * @doc 5.5.15
 * @since 2.0
 */
struct SCONFIG_UserIDConfig
{
	enum { LEN_USER_ID = g_cLenUserID, LEN_GROUP = g_cLenGroup, LEN_DESCRIPTION = g_cLenDescription };
	UINT8	ID[LEN_USER_ID];
	UINT8	group[LEN_GROUP];
	UINT8	description[LEN_DESCRIPTION];
	UINT8	reserved[128];
} ;

struct SCONFIG_UsersConfig
{
	UINT32	numberOfUserID;
	SCONFIG_UserIDConfig* userIDInfo;
	UINT32	extraDataLength;
	UINT8*	extraData;
} ;

/**
 * @bf Device Config Change
 * @doc 5.5.16
 * @since 2.0
 */
struct SCONFIG_DeviceConfigChange
{
	enum { LEN_OBJECT_ID = g_cLenObjectID , LEN_DEVICE_NAME = g_cLenDeviceName };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	UINT8	deviceName[LEN_DEVICE_NAME];
	UINT32	languageID;
	UINT8	reserved2[64];
} ;

/**
 * @bf Time Config Change
 * @doc 5.5.17
 * @since 2.0
 */
struct SCONFIG_TimeConfigChange
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	UINT32	timeZone;
	UINT32	useDst;
	struct tm	dstStartTime;
	struct tm	dstEndTime;
	UINT32	useNTP;
	UINT32	NTPServerCount;
	SCOMMON_NetworkHost*	NTPServerList;//NTPServerCount
	UINT8	reserved2[64];
	UINT32	NTPSyncInteval;
	UINT32	currentTimeSet;
	struct tm	currentTime;
	UINT8	reserved3[64];
} ;

struct SCONFIG_TimeZoneList
{
	UINT8	isDSTEnabled;
	UINT16	count;
	CHAR*	GMT[128];
	CHAR*	CITY[128];
	CHAR*	DST[128];
};


/**
 * @bf Network Device Add
 * @doc 5.5.18
 * @since 2.0
 */
struct SCONFIG_NetworkDeviceAdd
{
	enum { LEN_OBJECT_ID = g_cLenObjectID, LEN_MODEL_NAME = g_cLenModelName, LEN_DATA = g_cLenData };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	UINT8	modelName[LEN_MODEL_NAME];
	SCOMMON_IPAddress	ipAddress;
	UINT32	portSVNP;
	UINT32	portHTTP;
	UINT32	loginOption;
	SCOMMON_RequestLogin2 userInfo;
	UINT8	reserved2[64];
};

/**
 * @bf Network Device Add 수정 2012-06-29
 * @doc 5.5.18
 * @since 2.0
 */
struct SCONFIG_NetworkDeviceAdd2
{
	
	enum { LEN_DEVICE_NAME = g_cLenModelName, LEN_MODEL_NAME = g_cLenModelName, LEN_VERSION = 21 };
	UINT8	modelName[LEN_MODEL_NAME];
	UINT8	deviceName[LEN_DEVICE_NAME];
	UINT8	version[LEN_VERSION];
	UINT32	portHTTP;
	UINT32	portSVNP;
	UINT32	loginOption;
	UINT8	reserved1[2];
	SCOMMON_IPAddress	ipAddress;
	UINT8	reserved2[67];
};

/**
 * @bf Camera Config
 * @doc 5.5.19
 * @since 2.0
 */
struct SCONFIG_CameraConfig
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	UINT32	targetOperation;
	UINT16	dayAndNightMode;
	UINT16	irisMode;
	UINT16	focusMode;
	UINT16	panTiltSpeedRelatedZoomMode;
	UINT16	panTiltManualSpeed;
	UINT16	digitalFlipMode;
	UINT16	autoExecutionMode;
	UINT16	operationTemperatureMode;
	UINT8	reserved2[64];
};

/**
 * @bf System Log
 * @doc 5.5.20
 * @since 2.0
 */
struct SCONFIG_SystemLog
{
	enum { LEN_OBJECT_ID = g_cLenObjectID, LEN_KEYWORD = 64 };
	UINT8	ObjectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	struct tm	startTime;
	struct tm	endTime;
	UINT8	reserved2[64];
#if 0	// issue : is different with struct defined doc
	UINT32	keywordCount;
	UINT8*	keyword[LEN_KEYWORD];
#endif
	UINT32	typeLength;
	CHAR*	type;
} ;

/**
 * @bf Device Auto Scan Info
 * @doc 5.5.21
 * @since 2.0
 */
struct SCONFIG_DeviceAutoScanInfo
{
	enum { LEN_MAC_ADDRESS = g_cLenMacAddress, LEN_MODEL_NAME = g_cLenModelName };
	UINT8	modelName[LEN_MODEL_NAME];
	UINT8	mac[LEN_MAC_ADDRESS];
	UINT8	reserved1[2];
	SCOMMON_IPAddress	ipAddress;
	SCOMMON_IPAddress	subnetMask;
	SCOMMON_IPAddress	gateway;
	UINT8	reserved2[32];
	SCOMMON_IPAddress	dns1Address;
	SCOMMON_IPAddress	dns2Address;
	UINT16	devicePort;
	UINT16	httpPort;
	UINT8	reserved3[32];
} ;

struct SCONFIG_DeviceScanList
{
	UINT32	responseSequenceIndex;
	UINT32	responseMode;
	UINT8	reserved[64];
	UINT32	deviceInfoCount;
	SCONFIG_DeviceAutoScanInfo*	pDeviceAutoScanInfo;
} ;

/**
 * @bf System Log Data
 * @doc 5.5.22
 * @since 2.0
 */
struct SCONFIG_SystemLogData
{
	UINT32	responseSequencdIndex;
	UINT32	responseMode;
	UINT32	dataLength;
	UINT8*	pData;
} ;

/**
 * @bf Login Users
 * @doc 5.5.23
 * @since 2.0
 */
struct SCONFIG_LoginUserInfo
{
	enum { LEN_GROUP = g_cLenGroup, LEN_USER_ID = g_cLenUserID,LEN_DESCRIPTION = g_cLenDescription };
	UINT8	loginID[LEN_USER_ID];
	UINT8	userGroup[LEN_GROUP];
	struct tm	userLoginTime;
	SCOMMON_IPAddress	ipAddress;
	UINT8	description[LEN_DESCRIPTION];
	UINT8	reserved[64];
} ;

struct SCONFIG_LoginUsers
{
	UINT32	responseSequenceIndex;
	UINT32	responseMode;
	UINT8	reserved[64];
	UINT32	usersCount;
	SCONFIG_LoginUserInfo*	pLoginUserInfo;
} ;

/**
 * @bf Device Restart
 * @doc 5.5.24
 * @since 2.0
 */
struct SCONFIG_DeviceRestart
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	UINT32	restartMode;
	UINT8	reserved2[64];
} ;

/**
 * @bf Device Factory Reset
 * @doc 5.5.25
 * @since 2.0
 */
struct SCONFIG_DeviceFactoryReset
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT16	reserved1;
	UINT32	resetMode;
	UINT8	reserved2[4];
} ;

/**
 * @bf Device Model
 * @doc 5.5.26
 * @since 2.0
 */
struct SCONFIG_DeviceModel
{
	enum { LEN_OBJECT_ID = g_cLenObjectID, LEN_MODEL_NAME = g_cLenModelName };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	reserved1[2];
	UINT8	modelName[LEN_MODEL_NAME];
	UINT16	majorFWVersion;
	UINT16	minorFWVersion;
	UINT8	reserved2[4];
	CHAR	strFWReleaseDate[15];	//YYYYMMDDHHmmSS
	UINT8	reserved3;
	UINT16	majorProtocolVersion;
	UINT16	minorProtocolVersion;
	UINT8	reserved4[64];
} ;

/**
 * @bf Serial Data
 * @doc 5.5.27
 * @since 1.2
 */
struct SCONFIG_SerialData //SVNP_SERIAL_DATA
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT32	dataLength;
	UINT8*	pData; 
} ;

/**
 * @bf DEVICE EVENT
 * @doc 6.1
 * @since 1.2
 */
struct SEVENT_DeviceEvent
{
	enum { LEN_OBJECT_ID = g_cLenObjectID, LEN_EVENT_OBJECT_NAME = 21, LEN_OBJECT_NAME = g_cLenObectName2	};
	UINT16	event_duration;
	UINT16	event_type;
	UINT8	objectID[LEN_OBJECT_ID];	   		//camera object id 
	UINT8	objectName[LEN_OBJECT_NAME]; 		//camera object name 
	UINT8	eventObjectName[LEN_EVENT_OBJECT_NAME];     	//event object name
	UINT32	sensorPort;		        	//sensor or digital-out port
};

/**
 * @bf Proprietary
 * @doc 5.6.2
 * @since 1.2
 */
struct SEVENT_ProprietaryMsg
{
	UINT8	MessageID;
	UINT8	MessageLen;
	UINT8*	Message;
} ;

/**
 * @bf Force Logout
 * @doc 5.6.3
 * @since 2.0
 */
struct SEVENT_ForceLogout
{
	enum { LEN_USER_ID = g_cLenUserID, LEN_DESCRIPTION = g_cLenDescription }; 
	UINT8	userID[LEN_USER_ID];
	SCOMMON_IPAddress	ipAddress;
	UINT8	description[64];
} ;

/**
 * @bf User Login Information
 * @doc 5.6.4
 * @since 2.0
 */
struct SEVENT_UserLoginInformation
{
	enum { LEN_USER_ID = g_cLenUserID };
	UINT8	userID[64];
	SCOMMON_IPAddress	ipAddress;
	UINT8	isAdmin;
} ;

/**
 * @bf Password Changed
 * @doc 5.6.5
 * @since 2.0
 */
struct SEVENT_PasswordChanged
{
	enum { LEN_USER_ID = g_cLenUserID };
	UINT8 userID[LEN_USER_ID];
	UINT8 isAdmin;
} ;

/**
 * @bf Storage Information
 * @doc 5.6.6
 * @since 2.0
 */
struct SEVENT_StorageInformation
{
	UINT8	storageType;
	UINT8	isExternal;
	UINT16	number;
} ;

/**
 * @bf Channel Configuration Changed
 * @doc 5.6.7
 * @since 2.0
 */
struct SEVENT_ChannelConfigurationChanged
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];
	UINT8	ptzEnabled;
	UINT8	videoOnOff;
	UINT8	audioOnOff;
} ;

/**
 * @bf Video Profile Temp Info
 * @doc 5.6.8
 * @since 2.0
 */
struct SEVENT_VideoProfileInfo
{
	enum { LEN_OBJECT_ID = g_cLenObjectID };
	UINT8	objectID[LEN_OBJECT_ID];	// Channel의 ObjectID
	UINT16	videoProfileIndex;
	SCONFIG_VideoProfileInfo	videoProfile;
};


#ifdef WIN32
#pragma pack(pop, old_pack_size)
#endif

#endif //_SVNP_STRUCTS_H_