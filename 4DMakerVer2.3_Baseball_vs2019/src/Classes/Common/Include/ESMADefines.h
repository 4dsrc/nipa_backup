////////////////////////////////////////////////////////////////////////////////
//
//	TGADefines.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once

/////////////////////////////////////////////////
// types

typedef short              TGA_INT16;
typedef unsigned short     TGA_UINT16;
typedef int                TGA_INT32;
typedef unsigned long      TGA_UINT32;
//-- 2011-2-14 keunbae.song
typedef unsigned char      TGA_UINT8;



/////////////////////////////////////////////////
// defines

#define TGADEF_ET_BE       0xF0   // DEVICE ENDIAN TYPE BIG
#define TGADEF_ET_LE       0x0F   // DEVICE ENDIAN TYPE LITTLE

#define TGADEF_MAX_TASKCNT 128
#define TGADEF_MAX_BUFCNT  128
#define TGADEF_MAX_HEAPCNT 128
#define TGADEF_MAX_STKCNT  128
#define TGADEF_MAX_TMQCNT  128

#define TGAFILETYPE_DIR    0
#define TGAFILETYPE_FILE   1

// TGA Protocol Error Code
#define TGAE_SUCCESS         0
#define TGAE_ERROR_UNKNOWN  -1  

/////////////////////////////////////////////////
// TGA ITEM CODE 

#define TGAITEMCD_CPU           0x01
#define TGAITEMCD_HEAP          0x02
#define TGAITEMCD_STK           0x03
#define TGAITEMCD_BUFUSG        0x04
#define TGAITEMCD_KEY           0x05
#define TGAITEMCD_LOG           0x06
#define TGAITEMCD_TMSGQ         0x07
// D3 Task Info
#define TGAITEMCD_TASK			    0x08
//-- 2011-5-17 keunbae.song
#define TGAITEMCD_CPUSWITCH     0x10
//-- 2011-7-7 keunbae.song
#define TGAITEMCD_CPUHOOK       0x11
/* Bruce 2010-10-06: 추가되는 Heap, Stack(Thread) */
#define TGAITEMCD_ADD_HEAP		0x12
#define TGAITEMCD_ADD_STK		0x13
//-- Touch Command
#define TGAITEMCD_TOUCH			0x15
//-- TMS Analyzer
#define TGAITEMCD_ANALYZER				0x31
// [2/9/2011 JanuS] Memory Dump
#define TGAITEMCD_MEMORY_DUMP			0x32

/////////////////////////////////////////////////
// TGAITEM

struct TGAITEM
{
	TGA_UINT32  ItemSize;           // item size     
	TGA_UINT32  ItemCode;           // item code   
	TGA_UINT32  TickCount;          // system tick count 
};

/////////////////////////////////////////////////
// TGAITEM_CPU

struct TGAITEM_CPU 
	: public TGAITEM
{
	TGAITEM_CPU()
	{
		Initialize();
	}

	void Initialize()
	{
		ItemSize   = sizeof(TGAITEM_CPU);
		ItemCode   = TGAITEMCD_CPU;
	}    

	TGA_UINT32 TaskID;             // task id
	char		TaskName[64];
};

struct TGAITEM_CPU_D3 
	: public TGAITEM
{
	TGAITEM_CPU_D3()
	{
		Initialize();
	}

	void Initialize()
	{
		ItemSize   = sizeof(TGAITEM_CPU_D3);
		ItemCode   = TGAITEMCD_CPU;
	}    

	TGA_UINT32 TaskID;             // task id
};

/////////////////////////////////////////////////
// TGAITEM_HEAP

struct TGAITEM_HEAP
{
	TGA_INT32  ID;                 // Heap ID
	TGA_UINT32 Free;               // free size
	TGA_UINT32 MaxFreeBlkSize;     // max free block size        
};

/////////////////////////////////////////////////
// TGAITEM_HEAPLSTHDR

struct TGAITEM_HEAPLSTHDR
	: public TGAITEM
{
	TGAITEM_HEAPLSTHDR()
	{
		Initialize();
	}

	void Initialize()
	{
		ItemSize        = sizeof(TGAITEM_HEAPLSTHDR);
		ItemCode        = TGAITEMCD_HEAP;
	}

	TGA_UINT32     ItemCount;
};

/////////////////////////////////////////////////
// TGAITEM_HEAPLST   

struct TGAITEM_HEAPLST
	: public TGAITEM_HEAPLSTHDR
{
	TGAITEM_HEAP    Item[1];    
};        

/////////////////////////////////////////////////
// TGAITEM_STK   

struct TGAITEM_STK 
{
	TGA_UINT32 TaskID;              // Task ID
	TGA_UINT32 TskStat;             // Task State;    
	TGA_UINT32 SP;                  // stack pointer
	TGA_UINT32 MAXSP;               // max sp
	//TGA_UINT32 TaskPri;              // Task Priority
};

/////////////////////////////////////////////////
// TGAITEM_STKLSTHDR 

struct TGAITEM_STKLSTHDR     
	: public TGAITEM
{
	TGAITEM_STKLSTHDR()
	{
		Initialize();
	}

	void Initialize()
	{
		ItemSize  = sizeof(TGAITEM_STKLSTHDR);
		ItemCode  = TGAITEMCD_STK;          
	}

	TGA_UINT32   ItemCount;         // item count
};   

/////////////////////////////////////////////////
// TGAITEM_STKLST   

struct TGAITEM_STKLST
	: public TGAITEM_STKLSTHDR
{
	TGAITEM_STK Item[1];          
};      

/////////////////////////////////////////////////
// TGAITEM_BUFUSG
struct TGAITEM_BUFUSG
	: public TGAITEM
{
	TGAITEM_BUFUSG()
	{
		Initialize();
	}

	void Initialize()
	{
		ItemSize        = sizeof(TGAITEM_BUFUSG);
		ItemCode        = TGAITEMCD_BUFUSG;
		Index           = 0;
		Status          = 0;        
	}

	TGA_INT32  ID;                   // buffer id
	TGA_UINT32 Index;                // buffer index
	TGA_UINT32 Status;               // alloc status , 0 - free, 1 - alloc
};

/////////////////////////////////////////////////
// TGAITEM_KEY

struct TGAITEM_KEY
	: public TGAITEM
{
	TGAITEM_KEY()
	{
		Initialize();
	}

	TGAITEM_KEY( TGA_UINT32 TickCount, TGA_UINT32 Code, TGA_UINT32 Status )
	{

		Initialize();

		this->TickCount = TickCount;
		this->Code      = Code;
		this->Status    = Status;
	}

	void Initialize()
	{
		ItemSize        = sizeof(TGAITEM_KEY);
		ItemCode        = TGAITEMCD_KEY;
	}

	TGA_UINT32 Code;         // Key Code
	TGA_UINT32 Status;       // Key Status , 
};

/////////////////////////////////////////////////
// TGAITEM_TOUCH
// Bruce 2010.11.10 추가
struct TGAITEM_TOUCH
	: public TGAITEM
{
	TGAITEM_TOUCH()
	{
		Initialize();
	}

	void Initialize()
	{
		ItemSize        = sizeof(TGAITEM_TOUCH);
		ItemCode        = TGAITEMCD_TOUCH;
	}

	TGA_UINT16 uTouchX;
	TGA_UINT16 uTouchY;
	TGA_UINT16 Status;		// push, long, release
	BOOL bIsDrag;
};

/////////////////////////////////////////////////
// TGAITEM_LOGHDR

struct TGAITEM_LOGHDR
	: public TGAITEM
{
	TGAITEM_LOGHDR()
	{
		Initialize();
	}

	void Initialize()
	{
		ItemSize        = sizeof(TGAITEM_LOGHDR);
		ItemCode        = TGAITEMCD_LOG;
	}

	TGA_UINT32 LogLen;         // Log Message Length
};

/////////////////////////////////////////////////
// TGAITEM_LOG

struct TGAITEM_LOG
	: public TGAITEM_LOGHDR
{
	char   Log[1];              // Log Message    
}; 

/////////////////////////////////////////////////
// TGAITEM_TMSGQ

struct TGAITEM_TMSGQ 
	: public TGAITEM
{
	TGAITEM_TMSGQ()
	{
		Initialize();
	}

	void Initialize()
	{
		ItemSize = sizeof(TGAITEM_TMSGQ);
		ItemCode = TGAITEMCD_TMSGQ;
	}

	TGA_INT32  ID;            // queue id
	TGA_UINT32 Size;          // current size
};

/////////////////////////////////////////////////
// TGAITEM_ANALYZER
struct TGAITEM_ANALYZER
	: public TGAITEM
{
	TGAITEM_ANALYZER()
	{
		Initialize();
	}

	void Initialize()
	{
		ItemSize        = sizeof(TGAITEM_ANALYZER);
		ItemCode        = TGAITEMCD_ANALYZER;
	}

	TGA_UINT32  ulFunc;
	TGA_UINT32  ulProc;
	TGA_UINT32  ulData1;
	TGA_UINT32  ulData2;
};

/////////////////////////////////////////////////
// D3 Add Task Info
struct TGAITEM_TASK  : public TGAITEM
{
	TGA_UINT32  TaskID;                 // task id
	char        TaskName[64];          // task name 
	TGA_UINT32  Size;                   // stack size
	TGA_UINT32  BP;                     // base  pointer  
} ;

struct TGAITEM_D3_STK 
{
	TGA_UINT32 TaskID;              // Task ID
	TGA_UINT32 TskStat;             // Task State;    
	TGA_UINT32 SP;                  // stack pointer
	TGA_UINT32 MAXSP;               // max sp
	TGA_UINT32 BASESP;            // Task Priority
};

struct TGAITEM_D3_STKLST
	: public TGAITEM_STKLSTHDR
{
		TGAITEM_D3_STK Item[1]; 
};

//-- 2011-2-14 keunbae.song
struct TGAITEM_MEMORY_DUMP
    : public TGAITEM
{
    TGA_UINT32 memDumpID; 
		TGA_UINT32 Address;
		TGA_UINT32 Length; 
    //TGA_UINT32 data[1]; 
		TGA_UINT8 data[1]; 
};

/////////////////////////////////////////////////
// EImgFormat
enum EImgFormat
{
	eImgFormat_RAW,
	eImgFormat_YUV422,
	eImgFormat_YUV420,
	eImgFormat_RGB24,
	eImgFormat_RGB32,
	eImgFormat_MAX
};

/////////////////////////////////////////////////
// TGAImg_Header
struct TGAImg_Header
{
	TGA_UINT32 width;
	TGA_UINT32 height;
	TGA_UINT32 format;
}; 

/////////////////////////////////////////////////
// TGA CONFIG ITEM

#define TGACFGCD_CPU              ( 0x1 << 0 )
#define TGACFGCD_HEAP             ( 0x1 << 1 )
#define TGACFGCD_STK              ( 0x1 << 2 )
#define TGACFGCD_BUFUSG           ( 0x1 << 3 )
#define TGACFGCD_KEY              ( 0x1 << 4 ) 
#define TGACFGCD_RECKEY           ( 0x1 << 5 ) 
#define TGACFGCD_LOG              ( 0x1 << 6 ) 
#define TGACFGCD_TMSGQ            ( 0x1 << 7 )
#define TGACFGCD_ANAYZER_THREAD   ( 0x1 << 9 )
#define TGACFGCD_ANAYZER_FUNCTION ( 0x1 << 10 )
#define TGACFGCD_ANAYZER_MSGQUEUE ( 0x1 << 11 )
//-- 2011-5-17 keunbae.song
#define TGACFGCD_CPU_SWITCH       ( 0x1 << 12 ) // task switch
//-- 2011-7-8 keunbae.song
#define TGACFGCD_CPU_HOOK         ( 0x1 << 13 ) // task hook

#define TGACFGOPT_OFF             0
#define TGACFGOPT_ON              1


struct TGACFG_CPU
{
	TGACFG_CPU(){}
	TGACFG_CPU( int OnOff_, int nFallingTime_ ) 
		: OnOff( OnOff_ ), nFallingTime(nFallingTime_) { }

	int OnOff;        // 0 : Off , 1 : On
	int nFallingTime; // ms
};

struct TGACFG_HEAP
{
	TGACFG_HEAP() {}
	TGACFG_HEAP( int OnOff_, int nFallingTime_ ) 
		: OnOff( OnOff_ ), nFallingTime(nFallingTime_) { }

	int OnOff;        // 0 : Off , 1 : On
	int nFallingTime; // ms
};

struct TGACFG_STK
{
	TGACFG_STK() {}
	TGACFG_STK( int OnOff_, int nFallingTime_ ) 
		: OnOff( OnOff_ ), nFallingTime(nFallingTime_) { }

	int OnOff;        // 0 : Off , 1 : On
	int nFallingTime; // ms
};

struct TGACFG_BUFUSG
{
	TGACFG_BUFUSG() {}
	TGACFG_BUFUSG( int OnOff_ ) 
		: OnOff( OnOff_ ) { }

	int OnOff;
};

struct TGACFG_KEY
{
	TGACFG_KEY() {}
	TGACFG_KEY( int OnOff_ ) 
		: OnOff( OnOff_ ) { }

	int OnOff;
};

struct TGACFG_RECKEY
{
	TGACFG_RECKEY() {}
	TGACFG_RECKEY( int OnOff_ ) 
		: OnOff( OnOff_ ) { }

	int OnOff;
};

struct TGACFG_LOG
{
	TGACFG_LOG() {}
	TGACFG_LOG( int OnOff_ ) 
		: OnOff( OnOff_ ) { }

	int OnOff;
};

struct TGACFG_TMSGQ
{
	TGACFG_TMSGQ() {}
	TGACFG_TMSGQ( int OnOff_ ) 
		: OnOff( OnOff_ ) { }

	int OnOff;
};

/////////////////////////////////////////////////
// TGA_TARGETINFO

struct TGA_TARGETINFO
{
	char   Category[32];   // hybrid dsc , ... 
	char   Platform[16];
	char   Model[16];      // nx 10, nx 5, ...
	char   Step[16];       // PVR, PIV ,...
	char   FirmVer[64];    // Firmware Version   
	char   EndianType;     // endian type            
	char   BuildType[16];  // Debug or Release 
	unsigned int CisNo;
};

/////////////////////////////////////////////////
// TGA_TSKINFO

struct TGA_TSKINFO
{
	TGA_TSKINFO()
	{
		ID      = 0;
		Name[0] = '\0';
	}

	TGA_UINT32  ID;                     // task id
	char        Name[64];              // task name 
};

/////////////////////////////////////////////////
// TGA_TSKINFOLST

struct TGA_TSKINFOLST
{
	TGA_UINT32      ValidItemCnt;           // Valid Item Count
	TGA_TSKINFO     list[TGADEF_MAX_TASKCNT];     
}; 

/////////////////////////////////////////////////
// TGA_HEAPINFO

typedef struct _TGA_HEAPINFO
{
	TGA_UINT32  ID;                     // heap ID
	char        Name[64];               // heap name
	TGA_UINT32  Size;                   // heap size
}TGA_HEAPINFO;

typedef struct _TGA_ZORAN_HEAPINFO
{
	TGA_INT32 iId;					// heap ID
	TGA_UINT32 ulPoolTotalSize;		// Heap Size
	TGA_UINT32 ulAllocedSize;		// Heap AllocedSize
}TGA_ZORAN_HEAPINFO;

typedef struct _TGA_D3_HEAPINFO
{
	TGA_UINT32  ID;                     // heap ID
	TGA_UINT32  Size;                   // heap size
}TGA_D3_HEAPINFO;

typedef struct _TGA_CAMC12_HEAPINFO
{
	TGA_INT32 iId;					// heap ID
	TGA_UINT32 ulPoolTotalSize;		// Heap Size
	TGA_UINT32 ulAllocedSize;		// Heap AllocedSize
}TGA_CAMC12_HEAPINFO;
/////////////////////////////////////////////////
// TGA_HEAPINFOLST
struct TGA_HEAPINFOLST
{
	TGA_UINT32       ValidItemCnt;            // Valid Item Count
	union 
	{
		TGA_HEAPINFO		list[TGADEF_MAX_HEAPCNT];// array of item    
		TGA_ZORAN_HEAPINFO  list_zoran[TGADEF_MAX_HEAPCNT];// array of item    
		TGA_CAMC12_HEAPINFO list_camc12[TGADEF_MAX_HEAPCNT];
		TGA_D3_HEAPINFO		list_d3[TGADEF_MAX_HEAPCNT];
	}TGA_COMMON_HEAPINFO ;
};

/////////////////////////////////////////////////
//// Zoran
/////////////////////////////////////////////////
// TGA_HEAP_INFO
struct TGA_HEAP_INFO
{
	TGA_INT32  iId;                 // Heap ID
	TGA_UINT32 ulPoolTotalSize;
	TGA_UINT32 ulAllocedSize;
	TGA_UINT32 ulFreeMemory;
	TGA_UINT32 ulMaxFreeBlockSize;
};

///////////////// ###### ///////////////
// TGAITEM_ADD_HEAPLST
struct TGAITEM_ADD_HEAPLST
	: public TGAITEM_HEAPLSTHDR
{
	TGA_HEAP_INFO    Item[1];
};


/////////////////////////////////////////////////
// TGA_STKINFO

struct TGA_STKINFO
{
	TGA_STKINFO()
	{
		TaskID      = 0;
		TaskName[0] = '\0';
		Size        = 0;
	}

	TGA_UINT32  TaskID;                 // task id
	char        TaskName[64];          // task name 
	TGA_UINT32  Size;                   // stack size
	TGA_UINT32  BP;                     // base  pointer    
};

/////////////////////////////////////////////////
// TGA_STKINFOLST  

struct TGA_STKINFOLST
{
	TGA_UINT32      ValidItemCnt;             // Valid Item Count
	TGA_STKINFO     list[TGADEF_MAX_STKCNT];  // Item
};


///////////////// ###### ///////////////
// Zoran
// TGAITEM_ADD_STKLST
struct TGAITEM_ADD_STKLST
	: public TGAITEM_STKLSTHDR
{
	TGA_STKINFO Item[1];
};


/////////////////////////////////////////////////
// TGA_BUFINFO

struct TGA_BUFINFO
{
	TGA_INT32   ID;                     // Buffer ID    
	char        Name[64];               // Buffer Name
	TGA_UINT32  Address;                // Buffer Address
	TGA_UINT32  Count;                  // Buffer Count
};

/////////////////////////////////////////////////
// TGA_BUFINFOLST

struct TGA_BUFINFOLST
{
	TGA_UINT32          ValidItemCnt;             // Valid Item Count
	TGA_BUFINFO         list[TGADEF_MAX_BUFCNT];  // Item
};

/////////////////////////////////////////////////
// TGA_TASKMQINFO

struct TGA_TASKMQINFO
{
	TGA_TASKMQINFO()
	{
	}

	TGA_TASKMQINFO( TGA_INT32 ID, TGA_UINT32 MaxSize, TGA_UINT32 OwnerTaskID  )
	{
		this->ID          = ID;
		this->MaxSize     = MaxSize;
		this->OwnerTaskID = OwnerTaskID; 
	}

	TGA_INT32       ID;
	TGA_UINT32      MaxSize;
	TGA_UINT32      OwnerTaskID;
};

/////////////////////////////////////////////////
// TGA_TASKMQINFOLST

struct TGA_TASKMQINFOLST
{
	TGA_UINT32        ValidItemCnt;             // Valid Item Count
	TGA_TASKMQINFO    list[TGADEF_MAX_TMQCNT];  // Item
};

/////////////////////////////////////////////////
// TGA MDQ 

struct TGAMDQHDR
{
	int TotItemSize;
};

struct TGAMDQ
	: public TGAMDQHDR
{
	char Buffer[1];           // buffer
};

/////////////////////////////////////////////////
// MDQ File

struct TGAMDQFILEHDR
{
	TGA_TARGETINFO      TgtInf;               
	TGA_TSKINFOLST      TskInfLst;
	TGA_HEAPINFOLST     HeapInfLst;
	TGA_STKINFOLST      StkInfLst;
	TGA_BUFINFOLST      BufInfLst;
	TGA_TASKMQINFOLST   TskMQInfLst;
};

// [2/9/2011 JanuS] Memroy Dump Request data structure
struct TGAMEMORYDUMP
{
	TGA_UINT32 memDumpID;   //0-based(0, 1, 2, 3)
	TGA_UINT32 onOff;
	TGA_UINT32 address;
	TGA_UINT32 length;
	TGA_UINT32 interval;
};

/////////////////////////////////////////////////
// TGA FILE INFO
struct TGAFILEINFO
{
	int  Type;         // TGAFILETYPE_DIR, TGAFILETYPE_FILE
	char Name[256];
};

//-- D3 Etc. Task Name
enum NONTASKID
{
	ID_IDLE      = 10000, 
	ID_INTERRUPT = 10001, 
	ID_KERNEL    = 10002
};

struct TASK_NAME_ID_SET
{
	TCHAR Name[64];
	int nID;
};