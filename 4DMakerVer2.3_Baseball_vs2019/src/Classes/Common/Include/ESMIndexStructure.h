////////////////////////////////////////////////////////////////////////////////
//
//	ESMIndexStructure.h : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-18
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#define MAX_CAMCOUNT	200
//---------------------------------------------------------------------------
//-- MSG
//--------------------------------------------------------------------------
typedef struct _ESMEVENTMSG {

	_ESMEVENTMSG ()
	{
		message = 0;
		nParam1 = 0;
		nParam2 = 0;
		nParam3 = 0;
		pParam	= NULL;
		pDest	= NULL;
		pParent = NULL;
	}
	UINT	message;
	UINT	nParam1;
	UINT	nParam2;
	UINT	nParam3;
	LPARAM	pParam;
	LPARAM	pDest;	// Multi Managing
	LPARAM	pParent; // 4DMaker DSCItem Pointer
} ESMEvent;


typedef struct _ESMFAMEARRAY 
{
	_ESMFAMEARRAY()
	{
		bError = -100;
	}
	BYTE* pYUV;

	BYTE* pImg;
	BOOL bLoad;
	int bError;
	int nResizeX;
	int nResizeY;
	int nWidth;
	int nHeight;
}ESMFrameArray;

//-- 2012-03-26 hongsu@esmlab.com
//-- Get / Set
enum
{
	ESM_GET	= 0,
	ESM_SET,
	ESM_SETTING,
};

//-- 2009-05-25
enum 
{ 	
	IMG_TYPE_BMP = 1,
	IMG_TYPE_JPG,
	IMG_TYPE_PNG,
};

//-- filtering Info
struct ESMFilteringInfo {
	int nLIneNum;
	CString strLog;
};

//-- 2012-08-06 joonho.kim
//-- thread struct
struct LOAD_FILE
{
	LPVOID m_pMain;
	char* m_strModel;
};

enum {
	ESM_OPT_PATH_HOME	 = 0,
	ESM_OPT_PATH_FILESVR	,
	ESM_OPT_PATH_LOG		,
	ESM_OPT_PATH_CONF		,
	ESM_OPT_PATH_RECORD		,
	ESM_OPT_PATH_WORK		,
	ESM_OPT_PATH_OUTPUT		,
	ESM_OPT_PATH_SERVERRAM	,
	ESM_OPT_PATH_CLIENTRAM	,
	ESM_OPT_PATH_SAVE_IMG	,
	ESM_OPT_PATH_ALL		,
};

enum {
	ESM_BACKUP_STRING_SRCFOLDER  = 0,
	ESM_BACKUP_STRING_DSTFOLDER		,
	ESM_BACKUP_STRING_FTPHOST		,
	ESM_BACKUP_STRING_FTPID			,
	ESM_BACKUP_STRING_FTPPW			,
	ESM_BACKUP_STRING_ALL			,
};
enum
{
	ENCODE_ANDROID,
	ENCODE_WINDOW,
	ENCODE_IOS,
	ENCODE_MAX,
};

struct ESMCamInfo{
	ESMCamInfo()
	{
		nCamCount = 0;
	}
	int nCamCount;
	CString strCamID[MAX_CAMCOUNT];
	CString strCamIP[MAX_CAMCOUNT];
	BOOL usedFlag[MAX_CAMCOUNT];
	BOOL makeFlag[MAX_CAMCOUNT];		//jhhan 16-10-10 Making Flag
	CString strCamName[MAX_CAMCOUNT];	//jhhan 16-10-19 KT_NAME
	BOOL reverse[MAX_CAMCOUNT];			//hjcho 180312 reverse movie
	int	nArrVertical[MAX_CAMCOUNT];		//wgkim 180512 vertcal mode

	CString strGroup[MAX_CAMCOUNT];
	
	CString strView;					//jhhan 181014 VIEW
	BOOL detectFlag[MAX_CAMCOUNT];		//jhhan 181210 Detect
};

struct ESMPCInfo{
	ESMPCInfo()
	{
		nPCount = 0;
	}
	int nPCount;
	CString strPCIP[MAX_CAMCOUNT];
};

//jhhan - bDefault �߰�
struct ESMBasicPath {
	ESMBasicPath()
	{
		bDefault = FALSE;
	}
	CString strPath[ESM_OPT_PATH_ALL];
	BOOL bDefault;
};
struct ESMBackupString{
	ESMBackupString()
	{
		
	}
	CString strString[ESM_BACKUP_STRING_ALL];	
};
struct ESMCeremony{
	ESMCeremony()
	{
		bCeremonyUse =FALSE;
		bPutImage	= FALSE;
		bCeremonyRatio = FALSE;
		bStillImage	= FALSE;
		bUseCanonSelphy = FALSE;
	}
	BOOL bCeremonyUse;
	BOOL bPutImage;
	BOOL bCeremonyRatio;
	CString strDbAddrIP;
	CString strDbId;
	CString strDbPasswd;
	CString strDbName;
	CString strImagePath;
	CString strMoviePath;
	CString strWidth;
	CString strHeight;
	BOOL bStillImage;
	BOOL bUseCanonSelphy;
};
struct ESMManagement{
	ESMManagement()
	{		
	}
	CString strSelectFilePath;
	CString strSaveFilePath;	
};

struct ESM4DMaker{
	BOOL bDelayMode;
	BOOL bMakeServer;
	BOOL bRCMode; // FALSE : Local || TRUE : Remote (Agent)	
	int nServerMode;
	int nPort;
	int nDeleteCnt;
	int nRepeatMovieCnt;
	int nDSCSyncCnt;
	int nWaitSaveDSC;
	//jhhan
	int nWaitSaveDSCSub;
	int nWaitSaveDSCSel;

	int nSyncTimeMargin;
	int nTemplateStartMargin;
	int nTemplateEndMargin;
	int nTemplateStartSleep;
	int nTemplateEndSleep;
	int nSensorOnTime;
	int nMovieSaveLocation;
	//-- 2015-02-15 cygil@esmlab.com
	//-- Add picture coutdown
	int nCountdownLastIP;
	int nBaseBallInning;
	BOOL bViewInfo;
	BOOL bTestMode;
	BOOL bMakeAndAotuPlay;
	BOOL bInsertLogo;
	BOOL b3DLogo;
	BOOL bLogoZoom;
	BOOL bInsertCameraID;
	BOOL bTimeLinePreview;
	BOOL bEnableVMCC;
	BOOL bEnableCopy;
	BOOL bUHDtoFHD;
	BOOL bSaveImg;
	BOOL bEnableBaseBall;
	BOOL bReverseMovie;
	BOOL bGPUMakeFile;
	BOOL bRepeatMovie;
	BOOL bCheckValid;
	BOOL bInsertPrism;
	BOOL bInsertWhiteBalance;
	BOOL bColorRevision;
	BOOL bPrnColorInfo;
	BOOL bSyncSkip;
	int  nPrismValue;
	int  nPrismValue2;

	//16-12-12 wgkim@esmlab.com
	BOOL bTemplatePoint;

	//17-05-17
	BOOL bTemplateStabilization;
	BOOL bTemplateModify;

	//17-07-27 wgkim
	BOOL bLightWeight;

	//17-01-13 hjcho
	BOOL bAJAReplay;
	BOOL bAJALogo;

	//170207 hjcho
	BOOL bAJAOnOff;
	//170318 hjcho
	int nAJASapleCnt;

	BOOL bGetRamDiskSize;

	//jhhan 170327
	int nCheckVaildTime;
	int nDivFrame;

	//joonho.kim 170627
	int nPCSyncTime;
	int nDSCSyncTime;
	int nDSCSyncWaitTime;

	int nResyncRecWaitTime;

	BOOL bGPUSkip;

	BOOL bAutoDetect;

	//hjcho 170830
	BOOL bDirectMux;

	//hjcho 170904
	int n4DPMethod;

	//jhhan 171019
	BOOL bRemote;
	CString strRemote;
	BOOL bRemoteSkip;

	//hjcho 171113
	BOOL bHorizonAdj;

	//wgkim 171127
	BOOL bEncodingForSmartPhones;

	BOOL bProcessorShare;

	//hjcho 171210
	int nFrameZoomRatio;

	int nFuncKey;

	//wgkim 180119
	int nTemplateTargetOutPercent;
	BOOL bRecordFileSplit;

	BOOL bFile2Sec;

	//180508 hjcho
	BOOL bAJANetwork;
	CString strAJAIP;

	//180629 hjcho
	BOOL bGIFMaking;
	int nGIFQuality;
	int nGIFSize;

	//180629 wgkim
	int nSetFrameRateIndex;

	//180718 hjcho
	BOOL bRefereeRead;
	BOOL bUseReSync;
	//jhhan 180828
	BOOL bAutoDelete;
	int nStoredTime;
	BOOL bTemplateEditor;

	//wgkim 180921
	BOOL bAutoAdjustPositionTracking;
	BOOL bAutoAdjustKZone;

	//hjcho 180927
	BOOL bRTSP;

	BOOL b4DAP;

	//wgkim 181119
	int nSetTemplateModifyMode;

	//190103 hjcho
	int nSmartPhoneBitrate;

	//190319 hjcho
	int nRTSPWaitTime;
};

struct ESMTemplate{

	//2016/07/11 Lee SangA Template Load INI File
	CString strTemplate[5][9];
	/*
	CString strTemplate1;
	CString strTemplate2;
	CString strTemplate3;
	CString strTemplate4;
	//CMiLRe 20151013 Template Load INI File
	CString strTemplate5;
	CString strTemplate6;
	CString strTemplate7;
	CString strTemplate8;
	CString strTemplate9;*/

	//16/07/08 Lee SangA
	CString strSelectPage;
	int intSelectPage;

	BOOL bRecovery;
};

struct ESMFileCopy{
};

struct ESMAdjust{
	int	nThresdhold;
	int nPointGapMin;
	int nMinWidth;
	int nMinHeight;
	int nMaxWidth;
	int nMaxHeight;
	int nFlowmoTime;
	int nCamZoom;
	int	nAutoBrightness;
	BOOL bClockwise;
	BOOL bClockreverse;
	BOOL bMakeAndAotuPlay;
	int	 nTargetLenth;
};

struct ESMAdjustMovie{
	CStringArray arrFolderList;
	CStringArray arrFileList;
	CStringArray arrPathList;
	CString strSrcPath;
	CString strTarPath;
	CString strStartPath;
	CString strEndPath;
};

struct ESMAdjustInfo{
	CString strDSC;
	CString strTargetPath;
	int nCount;
};

struct DscAdjustInfo
{
	DscAdjustInfo()
	{
		bImgLoad = FALSE;
		nWidht = 0;
		nHeight = 0;
		pBmpBits = NULL;
		pGrayBmpBits = NULL;
		dAdjustX = 0.0;
		dAdjustY = 0.0;
		dAngle = 0.0;
		dRotateX = 0.0;
		dRotateY = 0.0;
		dScale = 0.0;
		dDistance = 0.0;
		nTargetLenth = 0;
		bReverse = FALSE;
	}
	CString strDscName;
	CString strDscIp;
	int nWidht;
	int nHeight;
	int nTargetLenth;
	BOOL bImgLoad;
	BYTE* pBmpBits;
	BYTE* pGrayBmpBits;
	CPoint HighPos;
	CPoint MiddlePos;
	CPoint LowPos;
	CRect recHigh;
	CRect recMiddle;
	CRect recLow;
	BOOL bAdjust;
	BOOL bRotate;
	BOOL bMove;
	BOOL bImgCut;

	double dAdjustX;
	double dAdjustY;
	double dAngle;
	double dRotateX;
	double dRotateY;
	double dScale;
	double dDistance;
	BOOL bReverse;

	//wgkim
	CRect rtMargin;
};

// struct MakeMovieInfo
// {
// 	MakeMovieInfo()
// 	{
// 		memset(strDscID, 0, sizeof(TCHAR)* 10);
// 		nStartFrame = 0;
// 		nEndFrame = 0;
// 	}
// 	TCHAR strDscID[10];
// 	TCHAR strMovieName[10];
// 	int	nStartFrame;
// 	int nEndFrame;
// };

struct ESMLogOpt {
	CString strLogName;
	int		nVerbosity;
	int		nLimitday;
	BOOL	bTraceTime;
};

#define INFO_LF_CHECK 4
struct ESMLogFilter {
	BOOL bInCheck	[INFO_LF_CHECK];
	BOOL bInPutCheck[INFO_LF_CHECK];
	BOOL bExCheck	[INFO_LF_CHECK];
	CString strInclude	[INFO_LF_CHECK];
	CString strInPutMsg	[INFO_LF_CHECK];
	CString strExclude	[INFO_LF_CHECK];
};

//-- 2013-10-08 hongsu@esmlab.com
//-- For Remote Control Infomation 
#define REMOTE_CONTROL_CNT	128
struct ESMRemoteControl
{
	int nBasePort;
	CString strIP[REMOTE_CONTROL_CNT];
	int nPort[REMOTE_CONTROL_CNT];
};