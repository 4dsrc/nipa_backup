
#ifndef _SVNP_DEFINE_H_
#define _SVNP_DEFINE_H_

//#include "stdafx.h"

#define LEN_OBJ_ID	10
#define LEN_STRING	21
#define SVNP_LEN_NAME 41
//#define SVNP_LEN_OBJNAME 21
#define SNT1010_MOTION_AREA_LENGTH	 72	// 72 bytes

const int MAXLEN_OBJECT = 10;
const int SVNP_LEN_OBJNAME = 21;
const int SVNP_LEN_ADDRESS = 16;
const int SVNP_LEN_USERID = 10;
const int SVNP_LEN_PWD = 10;
const int SVNP_LEN_DESC = 30;

const UINT MAXLEN_IP_ = 30;
const int LEN_FILE_PATH = 500;

// 이전 데이터 사이즈 - 호환성룸에서 테스트 결과 문제없는 최소Size

/*
Device Buffer Size Rearrange
*/
const int LEN_DEVICE_BUFFER = 640*1024;			
const int LEN_DEVICE_PACKET_SIZE = 64*1024;

/*
Media Buffer size 1.3M : 1280 * 1024 * 2(frame)
				  2.0M : 1280 * 1024 * 2 * 2(frame)
				  3.0M : 1280 * 1024 * 5 * 2(frame)
*/
const UINT LEN_MEDIA_PACKET_SIZE = 1280*1024;
const UINT LEN_MEDIA_PACKET_SIZE_2M = 1280 * 1024 * 2;
const UINT LEN_MEDIA_PACKET_SIZE_5M = 2048 * 1536;

const int LEN_MEDIA_AUDIO_BUFF = 64*1024;	
const UINT LEN_COMM_RECV_BUFFER = 64*1024;	// 320*1024 -> 1024*1024

const UINT D1_DEVICE_DEVICE_NUM = 2;			// M300 에 비해 D1 계열은 1/2 의 메모리를 할당한다.
const UINT RING_BUFFER_FRAME = 2;				// Ring Buffer 는 Frame Buffer Size 의 2배정도를 할당한다.


//namespace svnp
//{

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
//                            SVNP enum define                                //
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

/**
 * @bf 오브젝트 타입
 * @struct SVNP_Header, SCOMMON_ObjectAdvV200
 * @var    objectType
 */
typedef enum
{
	SVNP_OTYPE_NONE          = 0,
	SVNP_OTYPE_ENCODER       = 2,
	SVNP_OTYPE_CHANNEL		 = 4,
	SVNP_OTYPE_ALARM_IN      = 5,
	SVNP_OTYPE_VIDEO         = 6,
	SVNP_OTYPE_DVR           = 7,
	SVNP_OTYPE_NVR           = 8,
	SVNP_OTYPE_NET_CAMERA    = 9,
	SVNP_OTYPE_DECODER       = 10,
	SVNP_OTYPE_STORAGE       = 11,
	SVNP_OTYPE_DISPLAY       = 12,
	SVNP_OTYPE_AUDIO         = 13,
	SVNP_OTYPE_ALARM_OUT     = 20,
	SVNP_OTYPE_DATA_PORT     = 21,
} SVNPE_DEVICE_TYPE;

/**
 * @br 명령 전송 방향
 * @struct SVNP_Header
 * @var    direction
 */
typedef enum {
	DIR_REQUEST			= 0x01,
	DIR_RESPONSE,
	DIR_NOTIFY,
	DIR_NORIFY_RESPONSE,
} SVNPE_DIRECTION;

/**
 * @bf 미디어 타입
 * @struct SVNP_MediaHeader, SVNP_MediaVideoHeader, SVNP_MediaAudioHeader
 * @var    mediaType
 */
typedef enum
{
	MEDIA_TYPE_METADATA = 0,
	MEDIA_TYPE_VIDEO = 1,
	MEDIA_TYPE_AUDIO = 2,
} SVNPE_MEDIA_TYPE;

/**
 * @bf 코덱 타입
 * @struct SVNP_MediaVideoHeader, SVNP_MediaAudioHeader, SCOMMON_AudioInfo
 * @var    codecType
 */
typedef enum
{
	CODEC_UNKNOWN         = 0x00,
	MEDIA_CODEC_JPEG      = 0x01,
	MEDIA_CODEC_MJPEG,    //6475, encoder_box
	MEDIA_CODEC_MP4V,     //ingenient - 6475, dvr_4xxx
	MEDIA_CODEC_MP4V_ASP,
	MEDIA_CODEC_PENTA,    //penta - encoder_box, dvr_2xxx
	MEDIA_CODEC_H264      = 0x07,
	MEDIA_CODEC_G711      = 0x11, //penta - encoder_box, dvr_2xxx
	MEDIA_CODEC_G726,     //ingenient - dvr_4xxxx
	MEDIA_CODEC_META      = 0x31,// meta data
} SVNPE_MEDIA_CODEC_TYPE;

/**
 * @bf 프레임 타입
 * @struct SVNP_MediaVideoHeader
 * @var    frameType
 */
typedef enum
{
	FRAME_UNKNOWN = 0,
	FRAME_IVOP,
	FRAME_PVOP,
	FRAME_BVOP
} SVNPE_FRAME_TYPE;

/**
 * @bf 로그인 인증 암호 알고리즘 유형
 * @struct SCOMMON_LoginRequest
 * @var    algorithmType
 */
//namespace e_alogrithm_type
//{
enum SVNPE_ALGORITHM_TYPE
{
	TYPE_1 = 1
} ;
//}

/**
 * @bf 강제 접속 여부
 * @struct SCOMMON_LoginRequest
 * @var    option
 */
typedef enum
{
	LOGIN_OPTION_NORMAL = 0,
	LOGIN_OPTION_FORCE,
} SVNPE_LOGIN_OPTION;

/**
 * @bf 메타데이터 ID
 * @struct SVNP_MetadataHeader
 * @var    metadataID
 */
typedef enum
{
	SVNP_FW_UPGRADE = 2,
	SVNP_THUMBNAIL,
	//SVNP_METADATA,
} SVNPE_METADATA_ID;

/**
 * @bf 이벤트 타입
 * @struct SEVENT_DeviceEvent
 * @var    eventType
 */
typedef enum
{
	EVENT_MOTION_OCCUR		= 0x0000, //EVENT_MOTION
	EVENT_VIDEO_LOSS		= 0x0001,
	EVENT_SENSOR_OCCUR		= 0x0002,//EVENT_ALARM
	EVENT_CODEC_CHANGE		= 0x0003,
	EVENT_VIDEO_LOSS_END	= 0x0004,
	EVENT_PTZ_CHANGE		= 0x0005,
	EVENT_FAN_BROKEN		= 0x0007,
    EVENT_IV_OCCUR			= 0x0010,
	EVENT_CODEC_ADDED		= 0x0013,
	EVENT_CODEC_REMOVED		= 0x0014,
	EVENT_MOTION_START      = 0x0015,
	EVENT_MOTION_END        = 0x0016,

	EVENT_AUX_OCCUR         = 0x0017,
	EVENT_AUX_START         = 0x0018,
	EVENT_AUX_END           = 0x0019,

	EVENT_VLOSS_OCCUR       = 0x001C,
	EVENT_VLOSS_START       = 0x001D,

	EVENT_FAN_BROKEN_END	= 0x0107,

	EVENT_DIGITAL_OUT_START	= 0x000B,//EVENT_ALARM_OUT_START
	EVENT_DIGITAL_OUT_END   = 0x010B,//EVENT_ALARM_OUT_END
} SVNPE_EVENT_TYPE;

/**
 * @bf 녹화 타입 - 검색 요청 시 사용
 * @struct SREPLAY_QueryTime
 * @var    recordType
 */
typedef enum
{
	SVNP_QUERY_All                = 0x00,
	SVNP_QUERY_MD                 = 0x01,
	SVNP_QUERY_VLOSS              = 0x02,
	SVNP_QUERY_SENSOR             = 0x04,
	SVNP_QUERY_SCHEDULE_RECORDING = 0x08,
	SVNP_QUERY_MANUAL_RECORDING   = 0x10,
	SVNP_QUERY_IV_PASSING         = 0x20,
	SVNP_QUERY_IV_ENTERING        = 0x40,
	SVNP_QUERY_IV_EXTING          = 0x80,
	SVNP_QUERY_IV_APPEARING       = 0x100,
	SVNP_QUERY_IV_DISAPPEARING    = 0x200,
	SVNP_QUERY_IV_SCENE_CHANGE    = 0x400,
} SVNPE_QUERY_RECTYPE;

/**
 * @bf 녹화 타입
 * @struct SREPLAY_TimeLine
 * @var    recordType
 */
typedef enum {
	SVNP_RTYPE_NONE            = 0x0000,  // None or All
	SVNP_RTYPE_PANIC           = 0x0001,
	SVNP_RTYPE_MOTION          = 0x0002,
	SVNP_RTYPE_ALARM           = 0x0004,
	SVNP_RTYPE_VIDEOLOSS       = 0x0008,
	SVNP_RTYPE_MANUAL          = 0x0010,
	SVNP_RTYPE_SCHEDULE        = 0x0020,
	SVNP_RTYPE_EVENT           = 0x0040,  //no use, for internal use
	SVNP_RTYPE_BOOKMARK        = 0x0080,
	SVNP_RTYPE_IV_EVENT        = 0x0100,
	SVNP_RTYPE_IV_PASSING      = 0x0100,
	SVNP_RTYPE_IV_ENTERING     = 0x0200,
	SVNP_RTYPE_IV_EXITING      = 0x0400,
	SVNP_RTYPE_IV_APPEARING    = 0x0800,
	SVNP_RTYPE_IV_DISAPPEARING = 0x1000,
	SVNP_RTYPE_IV_SCENE_CHANGE = 0x2000,
	SVNP_RTYPE_LOCAL           = 0x4000,
	SVNP_RTYPE_REMOTE          = 0x8000,
	SVNP_RTYPE_ALL             = 0xFFFF,
} SVNPE_TIMELINE_RECTYPE;

/**
 * @bf 미디어 요청 타입
 * @struct SCOMMON_MediaRequest
 * @var mode
 */
typedef enum
{
	MREQ_NONE			= 0x00,
	MREQ_LIVE_VIDEO		= 0x01,
	MREQ_LIVE_AUDIO		= 0x02,
	MREQ_TALK			= 0x04,
	MREQ_UPGRADE		= 0x08,
	MREQ_SEARCH_VIDEO	= 0x10,
	MREQ_SEARCH_AUDIO	= 0x20,
	MREQ_THUMBNAIL      = 0x40,
} SVNPE_MREQ_MTYPE;

/**
 * @bf 미디어 요청 하위 명령
 * @struct SCOMMON_MediaRequest
 * @var    cmd(command)
 */
typedef enum
{
	MREQ_OPEN   = 0x00,
	MREQ_CLOSE,
} SVNPE_MREQ_COMMAND;

/**
 * @bf 스트리밍 타입
 * @struct SCOMMON_MediaRequestInfo
 * @var    streamingType
 *
 */
typedef enum {
	PROTOCOL_TCP	= 0x00,
	PROTOCOL_UDP,
	PROTOCOL_MULTICAST,
} SVNPE_PROTOCOL_TYPE;

/**
 * @bf 이벤트 상태
 * @struct SEVENT_DigitalOutControl
 * @var    eventStatus
 */
typedef enum
{
	OSTATUS_OFF = 0x0,
	OSTATUS_ON,
	OSTATUS_VLOSS,
} SVNPE_OSTATUS;

/**
 * @bf 서치 명령
 * @struct SCOMMON_SearchControl, SCOMMON_SearchControl2
 * @var    cmd(command)
 */
typedef enum {
	SEARCH_PAUSE = 0x01,//SEARCH_STOP
	SEARCH_PLAY,
	SEARCH_BACK_PLAY,
	SEARCH_SEEK,
	SEARCH_STEP_NEXT_IFRAME, //SEARCH_STEP_FORWARD,
	SEARCH_STEP_PREV_IFRAME, //SEARCH_STEP_BACKWARD,
	SEARCH_STEP_NEXTFRAME,
	GET_NEXT_GOP_DATA,
	GET_PREV_GOP_DATA
} SVNPE_SEARCH_CONTROL_TYPE;

/**
 * @bf object capability
 * @struct SCOMMON_ObjectAdvV200
 * @var    objectCap
 */
typedef enum
{
	CAP_UNIT_NONE					= 0x0000,
	CAP_UNIT_LIVE					= 0x0001,
	CAP_UNIT_SEARCH					= 0x0002,
	CAP_UNIT_LISTEN					= 0x0004,
	CAP_UNIT_TALK					= 0x0008,
	CAP_UNIT_PTZ					= 0x0010,
	CAP_UNIT_REPLAY					= 0x0020,
	CAP_UNIT_SEARCH_MOTION_AREAD	= 0x0040,
	CAP_UNIT_IV_RULE				= 0x0100,
	CAP_UPGRADE						= 0x0200,
	CAP_UNIT_ALARM_IN				= 0x00100000,
	CAP_UNIT_ALARM_OUT				= 0x00200000,
} SVNPE_OBJ_CAP_TYPE;

/**
 * @bf PTZ capability
 * @struct SCOMMON_ObjectAdvV200
 * @var    ptzCap
 */
typedef enum
{
	CAP_PTZ_NONE			= 0x00000000,
	CAP_PTZ_BASIC			= 0x00000001,//PAN & TILT & ZOOM & PRESET
	CAP_PTZ_POWER			= 0x00000002,
	CAP_PTZ_FUNCTION		= 0x00000004,//AUTOPAN, SCAN, PATTERN
	CAP_PTZ_IRIS			= 0x00000008,
	CAP_PTZ_FOCUS			= 0x00000010,
	CAP_PTZ_BRIGHTNESS		= 0x00000020,
	CAP_PTZ_PAN				= 0x00000040,
	CAP_PTZ_MENU			= 0x00000080,
	CAP_PTZ_TILT			= 0x00010000,
	CAP_PTZ_ZOOM			= 0x00020000,
	CAP_PTZ_PRESET			= 0x00040000,
	CAP_PTZ_AREA_ZOOM		= 0x00080000,
	CAP_PTZ_SCAN			= 0x00100000,
	CAP_PTZ_AUTOPAN			= 0x00200000,
	CAP_PTZ_PATTERN			= 0x00400000,
	CAP_PTZ_PRESET_VALIABLE	= 0x00800000,
} SVNPE_PTZ_CAP_TYPE;

/**
 * @bf PTZ capability
 * @struct SCOMMON_ObjectAdvV200
 * @var    playbackCap
 */
typedef enum _tagVnpPlaybackCapType
{
	CAP_PB_NONE			= 0x00000000,
	CAP_PB_PLAY			= 0x00000001,	// 1x play
	CAP_PB_STOP			= 0x00000002,	// stop
	CAP_PB_PB			= 0x00000004,	// 1x play backward
	CAP_PB_SEEK			= 0x00000008,
	CAP_PB_FF			= 0x00000010,	// fast forward
	CAP_PB_FB			= 0x00000020,	// fast backward
	CAP_PB_SF			= 0x00000040,	// slow forward
	CAP_PB_SB			= 0x00010000,	// slow backward
	CAP_PB_EXCLUSIVE	= 0x10000000,
} SVNPE_PLAYBACK_CAP_TYPE;

/**
 * @bf Search 권한
 * @struct SREPLAY_Authority
 * @var    authorityType
 */
typedef enum
{
	AUTH_TYPE_NONE = 0x00000000,
	AUTH_TYPE_SEARCH = 0x00000001,
} SVNPE_AUTH_TYPE;

/**
 * @bf PTZ 동작중 여부
 * @struct SCONTROL_PTZPos
 * @var    mode
 */
typedef enum
{
	PTZ_STATE_MOVING = 1,
	PTZ_STATE_MOVE_DONE,
} SVNPE_PTZ_MOVE_STATE;

/**
 * @bf video 설정 변경 type
 * @struct SCONFIG_VideoSetting
 * @var    changedType
 */
typedef enum
{
    CODEC_CHANGED       = 0x0001,
    FRAME_RATE_CHANGED  = 0x0002,
    QUALITY_CHANGED     = 0x0004,
    RESOLUTION_CHANGED  = 0x0008,
} SVNPE_VIDEO_CHANGED_TYPE;

/**
 * @bf 펌웨어 업그레이드 상태
 * @struct SEVENT_FWUpdateStatus
 * @var messageID
 */
typedef enum
{
	FWUPGRADE_DOWNLOAD_ACK = 1,
	FWUPGRADE_DOWNLOAD_OK,
	FWUPGRADE_DOWNLOAD_FAIL,
	FWUPGRADE_UPGRADE_START,
	FWUPGRADE_UPGRADE_END,
	FWUPGRADE_UPGRADE_OK,
	FWUPGRADE_UPGRADE_SKIP,
	FWUPGRADE_UPGRADE_FAIL,
	CAMERA_TEMPERATURE,
	CAMERA_OSD_TEXT,
	FWUPGRADE_UPDATING_ISP,
} SVNPE_PROPRIETARY_MSG;

/**
 * @bf IV 룰 타입
 * @struct SCONFIG_IVRuleObject
 * @var    m_nType
 */
typedef enum
{
	IV_RULE_LINE=0,
	IV_RULE_AREA,
	IV_RULE_FULL,
	IV_RULE_SIZEFILTER
} SVNPE_IV_RULE_TYPE;

/**
 * @bf 변경된 비디오 정보
 * @struct SCONFIG_VideoProfileSetting
 * @var    changedItem
 */
typedef enum
{
	PROFILE_CODEC_TYPE_CHANGED         = 0x0001,
	PROFILE_FRAMERATE_CHANGED          = 0x0002,
	PROFILE_QUALITY_CHANGED            = 0x0004,
	PROFILE_ENCODING_MODE_CHANGED      = 0x0008,
	PROFILE_TARGET_BITRATE_CHANGED     = 0x0010,
	PROFILE_GOP_SIZE_CHANGED           = 0x0020,
	PROFILE_ENCODING_PRIORITY_CHANGED  = 0x0040,
	PROFILE_RESOLUTION_CHANGED         = 0x0080,
	PROFILE_MULTICAST_CHANGED          = 0x0100,
} SVNPE_CHANGED_PROFILE_SETTING;

/**
 * @bf 언어
 * @struct SCONFIG_DeviceConfig
 * @var    languageID
 */

typedef enum
{
	LANGUAGE_ENGLISH = 1,//영어
	LANGUAGE_FRENCH,     //불어
	LANGUAGE_GERMAN,     //독일어
	LANGUAGE_SPANISH,    //스페인어
	LANGUAGE_ITALIAN,    //이탈리아어
	LANGUAGE_CHINESE,    //중국어
	LANGUAGE_RUSSIAN,    //러시아어
	LANGUAGE_KOREAN,     //한국어
	LANGUAGE_POLISH,     //폴란드어
	LANGUAGE_JAPANESE,   //일본어
	LANGUAGE_DUTCH,      //네덜란드어
	LANGUAGE_PORTUGUESE, //포르투칼어
	LANGUAGE_TURKISH,    //터키어
	LANGUAGE_CZECH,      //체코어
	LANGUAGE_DANISH,     //덴마크어
	LANGUAGE_SWEDISH,    //스웨덴어
	LANGUAGE_TAIWANESE,  //대만어
	LANGUAGE_GREEK,      //그리스
	LANGUAGE_CROATIAN,   //크로아티아
	LANGUAGE_SERBIAN,    //세르비아어
	LANGUAGE_HUNGARIAN,  //헝가리어
	LANGUAGE_RUMANIAN,   //루마니아어
} SVNPE_LANGUAGE_ID;

/**
 * @bf NIC 타입
 * @struct SCONFIG_NICConfig
 * @var    purposeOfNIC
 */
typedef enum
{
	NIC_TYPE_NOT_DEFINED = 0,
	NIC_TYPE_CMS_INTERFACE,
	NIC_TYPE_DEVICE_INTERFACE,
} SVNPE_NIC_TYPE;

/**
 * @bf 카메라 동작 모드
 * @struct SCONFIG_CameraConfig
 * @var    targetOperation
 */
typedef enum
{
	CAM_OP_DAY_AND_NIGHT         = 0x00000001,
	CAM_OP_IRIS                  = 0x00000002,
	CAM_OP_FOCUS                 = 0x00000004,
	CAM_OP_PAN_TILT_RELATED_ZOOM = 0x00000008,
	CAM_OP_PAN_TILT_MANUAL       = 0x00000010,
	CAM_OP_DIGITAL_FLIP          = 0x00000020,
	CAM_OP_AUTO_EXECUTION        = 0x00000040,
	CAM_OP_OPERATION_TEMPERATURE = 0x00000080,
} SVNPE_CAMERA_OPERATION_MODE;

/**
 * @bf Day and Night 모드
 * @struct SCONFIG_CameraConfig
 * @var    dayAndNightMode
 */
typedef enum
{
	DAY_N_NIGHT_MANUAL_DAY   = 0x0001,
	DAY_N_NIGHT_MANUAL_NIGHT = 0x0002,
	DAY_N_NIGHT_AUTOCHANGE   = 0x0003,
	DAY_N_NIGHT_EXTERNAL     = 0x0004,
} SVNPE_DAY_N_NIGHT_MODE;

/**
 * @bf IRIS 모드
 * @struct SCONFIG_CameraConfig
 * @var    irisMode
 */
typedef enum
{
	IRIS_MANUAL_MODE = 0x0001,
	IRIS_AUTO_MODE   = 0x0002,
} SVNPE_IRIS_MODE;

/**
 * @bf FOCUS 모드
 * @struct SCONFIG_CameraConfig
 * @var    focusMode
 */
typedef enum
{
	FOCUS_MANUAL_MODE         = 0x0001,
	FOCUS_AUTO_FOCUS_MODE     = 0x0002,
	FOCUS_ONE_AUTO_FOCUS_MODE = 0x0003,
} SVNPE_FOCUS_MODE;

/**
 * @bf Pan Tile 속도 zoom 배율에 따라 자동 변경 여부
 * @struct SCONFIG_CameraConfig
 * @var    panTiltSpeedRelatedZoomMode
 */
typedef enum
{
	PT_SPD_RELATED_ZOOM_ON  = 0x0001,
	PT_SPD_RELATED_ZOOM_OFF = 0x0001,
} SVNPE_PAN_TILT_SPEED_RELATED_ZOOM_MODE;

/**
 * @bf 자동 실행 모드
 * @struct SCONFIG_CameraConfig
 * @var    autoExecutionMode
 */
typedef enum
{
	AUTO_EXECUTION_MODE1 = 0x0001,
	AUTO_EXECUTION_MODE2 = 0x0002,
} SVNPE_AUTO_EXECUTION_MODE;

/**
 * @bf 열상 카메라 동작 모드
 * @struct SCONFIG_CameraConfig
 * @var    operationTemperatureMode
 */
typedef enum
{
	OPERATION_TEMPERATURE_LOW  = 0x0001,
	OPERATION_TEMPERATURE_HIGH = 0x0002,
} SVNPE_OPERATION_TEMPERATURE_MODE;

/**
 * @bf 장비 재시작 모드
 * @struct SCONFIG_DeviceRestart
 * @var    restartMode
 */
typedef enum
{
	DEVICE_RESTART_NETWORK = 0x0001,
	DEVICE_RESTART_ANALOG  = 0x0002,
	DEVICE_RESTART_ZOOM    = 0x0003,
} SVNPE_DEVICE_RESTART_MODE;

/**
 * @bf IV event
 * @struct SEVENT_DeviceEvent
 * @var    sensorPort
 */
typedef enum
{
	IV_DETAIL_PASSING      = 0x00000001,
	IV_DETAIL_ENTERING     = 0x00000010,
	IV_DETAIL_EXITING      = 0x00000020,
	IV_DETAIL_APPEARING    = 0x00000040,
	IV_DETAIL_DISAPPEARING = 0x00000080,
	IV_DETAIL_SCENE_CHANGE = 0x00000100,
} SVNPE_IV_EVENT_DETAIL;

/**
 * @bf 녹화 타입
 * @struct SEVENT_DeviceEvent
 * @var    sensorPort
 */
typedef enum
{
	RECORD_TYPE_EVENT    = 0x00000001,
	RECORD_TYPE_SCHEDULE = 0x00000002,
	RECORD_TYPE_MANUAL   = 0x00000003,
} SVNPE_RECORD_TYPE;

/**
 * @bf 펌웨어 모듈
 * @struct SVNP_PROPRIETARY_MSG
 * @var    message
 */
typedef enum
{
	FW_NONE = 0x00,
	FW_KERNEL,
	FW_APP,
	FW_WEB,
	FW_ISP,
} SVNPE_SORT_OF_FIRMWARE;

/**
 * @bf Storage 장치 종류
 * @struct SEVENT_StorageInformation
 * @var    storageType
 */
typedef enum
{
	STORAGE_TYPE_HDD     = 0x0001,
	STORAGE_TYPE_CD_DVD  = 0x0002,
	STORAGE_TYPE_SD_CARD = 0x0003,
	STORAGE_TYPE_USB     = 0x0004,
} SVNPE_STORAGE_TYPE;

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
//                            SVNP enum define                                //
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
//                                 COMMANDS                                   //
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

typedef enum
{
	CMD_LOGIN							= 0x01,	// 1
	CMD_LOGOUT							= 0x02,	// 2
	CMD_DATA_SEARCH						= 0x06,	// 6
	CMD_MEDIA_INFO						= 0x07,	// 7
	CMD_MEDIA_REQUEST					= 0x08,	// 8
	CMD_SEARCH_CONTROL					= 0x0A,	// 10
	NOTIFY_DEVICE_EVENT					= 0x0B,	// 11
	CMD_CAMERA_OSDMENU				    = 0x0C,	// 12
	CMD_PTZ_DIR							= 0x0D,	// 13
	CMD_PTZ_PRESET						= 0x0F,	// 15
	CMD_PTZ_AUTOPAN						= 0x10, // 16
	CMD_PTZ_SCAN						= 0x11,	// 17
	CMD_PTZ_GROUP						= 0x11,	// 17 - Duplicate
	CMD_PTZ_PATTERN						= 0x12,	// 18
	CMD_PTZ_TRACE						= 0x12,	// 18 - Duplicate
	CMD_HEARTBEAT						= 0x28,	// 40
	CMD_GET_OBJECTLIST_ADV				= 0x2B, // 43
	CMD_VIDEO_INFO						= 0x2C, // 44
	CMD_PAIR_CONNS						= 0x2D, // 45
	NOTIFY_FW_UPDATE_STATUS				= 0x2E, // 46

	//------- SVNP 2.0 NEW COMMAND START -------//
	CMD_GET_PROTOCOL_VERSION			= 0x30,	// 48
	CMD_LOGIN2							= 0x31,	// 49
	CMD_LOGOUT2							= 0x32,	// 50
	CMD_GET_OBJECT_CAPABILITIES			= 0x33,	// 51
	CMD_METADATA_REQUEST				= 0x34,	// 52
	CMD_SEARCH_CONTROL2					= 0x35,	// 53
	CMD_THUMBNAIL						= 0x36, // 54
	CMD_GET_EVENT_CAPABILITIES			= 0x37,	// 55	
	CMD_PTZ_POS2						= 0x38,	// 56
	CMD_QUERY_METADATA					= 0x39, // 57
	CMD_PTZ_PRESET2						= 0x3A,	// 58
	CMD_PTZ_SWING						= 0x3B, // 59
	CMD_PTZ_TOUR						= 0x3C, // 60
	CMD_CLEAR_ALL_EVENT					= 0x22,	// 61     Change 2011.05.24 0x3D -> 0x22
	CMD_USER_DEFINED_EVENT				= 0x3E, // 62
	CMD_AUDIO_INFO						= 0x3F, // 63
	CMD_LAYOUT_INFO						= 0x40, // 64
	CMD_SYSTEM_SETTING					= 0x41, // 65
	CMD_SYSTEM_LOG						= 0x42, // 66
	CMD_LOGIN_LIST						= 0x43, // 67
	CMD_CAMERA_SETUP					= 0x44, // 68
	CMD_PTZ_PRESET_SETUP				= 0x45, // 69
	CMD_PTZ_FUNCTION_SETUP				= 0x46, // 70
	CMD_VIDEO_INFO2						= 0x47,	// 71
	CMD_REQUEST_AUTO_SCAN				= 0x48,	// 72
	CMD_DEVICE_RESTART					= 0x49, // 73
	CMD_DEVICE_FACTORY_RESET			= 0x4A,	// 74
	CMD_GET_DEVICE_MODEL				= 0x4B, // 75
	NOTIFY_QOS_INFO						= 0x4C, // 76
	NOTIFY_USER_DEFINED_EVENT			= 0x4D, // 77
	NOTIFY_USER_EVENT					= 0x4E,	// 78
	NOTIFY_SYSTEM_EVENT					= 0x4F,	// 79
	NOTIFY_CONFIGURATION_CHANGED		= 0x50, // 80
	NOTIFY_VIDEO_TEMOPRARILY_CHANGED	= 0x51,	// 81
	NOTIFY_SERIAL_DATA					= 0x52,	// 82
	
	//------- SVNP 2.0 NEW COMMAND END -------//
	CMD_NETWORK_DEVICE					= 0x53, // 83

	CMD_FW_VERSION						= 0x64, // 100
	CMD_AUTHORITY						= 0x65, // 101
	CMD_DIGITAL_OUT						= 0x67, // 103
	CMD_GET_IV_RULE						= 0x6B, // 107
	NOTIFY_IV_RULE_EVENT				= 0x6C, // 108
	CMD_PTZ_POS							= 0x6D, // 109
	CMD_SEND_SERIAL_DATA				= 0xF0	// 240
} SVNP_COMMAND_ID;

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
//                                 COMMANDS                                   //
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
//                               SUB COMMANDS                                 //
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

/**
 * @bf CMD_LOGIN2의 sub command
 */
typedef enum
{
	SUBCMD_NONCE_S	 = 1,  
	SUBCMD_CONFIRM_HASH	= 2,   
} SVNP_SUBCMD_LOGIN2;

/**
 * @bf CMD_METADATA_REQUEST의 sub command
 */
typedef enum
{
	CMD_METADATA_STREAMING_START = 1,
	CMD_METADATA_STREAMING_STOP  = 2,
} SVNP_SUBCMD_METADATA_REQUEST;

/**
 * @bf CMD_CAMERA_OSDMENU(CMD_MENU)의 sub command
 */
typedef enum
{
	MENU_UP = 0x01,
	MENU_DOWN,
	MENU_LEFT,
	MENU_RIGHT,

	PTZ_MENU_ENTER,
	PTZ_MENU_ON,
	PTZ_MENU_OFF,

	//probe
	PTZ_MENU_CANCEL,
} SVNP_SUBCMD_MENU_DIRECTION;

/**
 * @bf CMD_DATA_SEARCH의 sub command
 */
typedef enum
{
    SUB_CMD_CALENDAR_SEARCH = 1,
    SUB_CMD_DAYTIME_SEARCH,
    SUB_CMD_DAYTIME_SEARCH_OVERLAP,
} SVNP_SUBCMD_SEARCH;

/**
 * @bf CMD_THUMBNAIL의 sub command
 */
typedef enum
{
	SUBCMD_GET_THUMBNAIL  = 1,
	SUBCMD_STOP_THUMBNAIL = 2,
} SVNP_SUBCMD_THUMBNAIL;

/**
 * @bf CMD_REPLAY_AUTHORITY의 sub command
 */
typedef enum
{
	SUBCMD_GET_REPLAY_AUTHORITY = 1,
	SUBCMD_RELEASE_REPLAY_AUTHORITY = 2,
} SVNP_SUBCMD_REPLAY_AUTHORITY;

/**
 * @bf CMD_PTZ_DIR(CMD_PTZ)의 sub command
 */
typedef enum
{
	PTZ_UP = 0x01,
	PTZ_DOWN,
	PTZ_LEFT,
	PTZ_RIGHT,

	PTZ_UP_LEFT,
	PTZ_UP_RIGHT,
	PTZ_DOWN_LEFT,
	PTZ_DOWN_RIGHT,

	PTZ_ZOOM_IN,
	PTZ_ZOOM_OUT,
	PTZ_STOP,

	PTZ_FOCUS_FAR,
	PTZ_FOCUS_NEAR,
	PTZ_FOCUS_STOP,
	PTZ_IRIS_OPEN,
	PTZ_IRIS_CLOSE = 0x10,
	PTZ_IRIS_STOP
} SVNP_SUBCMD_PTZ_DIRECTION;

/**
 * @bf CMD_PTZ_PRESET의 sub command
 */
typedef enum
{
	PTZ_ACTION_MOVE = 0x01,
	PTZ_ACTION_ADD,
	PTZ_ACTION_DELETE,
	PTZ_ACTION_DELETE_ALL,
	PTZ_ACTION_GETLIST,
	PTZ_ACTION_GETLIST_ADV,
} SVNP_SUBCMD_PTZ_ACTIONS;

/**
 * @bf CMD_PTZ_AUTOPAN의 sub command
 */
typedef enum
{
	SUBCMD_PTZ_AUTOPAN_START = 1,
	SUBCMD_PTZ_AUTOPAN_STOP  = 2,
	SUBCMD_PTZ_AUTOPAN_ON    = 3,
	SUBCMD_PTZ_AUTOPAN_OFF   = 4,
} SVNP_SUBCMD_PTZ_AUTOPAN;

/**
 * @bf CMD_PTZ_POS의 sub command
 */
typedef enum
{
	SUBCMD_PTZ_GET_POS           = 1,
	SUBCMD_PTZ_SET_POS           = 2,
	SUBCMD_PTZ_GET_POS_PRECISION = 3,
	SUBCMD_PTZ_SET_POS_PRECISION = 4,
	SUBCMD_PTZ_AREA_ZOOMIN       = 5,
	SUBCMD_PTZ_MOVE              = 6,
	SUBCMD_PTZ_SET_HOME          = 7,
	SUBCMD_PTZ_GO_HOME           = 8,
	SUBCMD_PTZ_GO_1X             = 9,
} SVNP_SUBCMD_PTZ_POS;

/**
 * @bf CMD_VIDEO_INFO의 sub command
 */
typedef enum
{
    SUBCMD_GET_VIDEO_INFO = 0x01,
    SUBCMD_GET_VIDEO_SETTING,
    SUBCMD_SET_VIDEO_SETTING,
    SUBCMD_GET_VIDEO_CODEC_INFO,
    SUBCMD_GET_VIDEO_PROFILE,
    SUBCMD_CHANGE_VIDEO_PROFILE,
    SUBCMD_ADD_VIDEO_PROFILE,
    SUBCMD_DEL_VIDEO_PROFILE,
} SVNP_SUBCMD_VIDEO_INFO;

/**
 * @bf CMD_AUDIO_INFO의 sub command
 */
typedef enum
{
	SUBCMD_GET_AUDIO_CODEC_INFO = 1,
	SUBCMD_GET_AUDIO_PROFILE    = 2,
	SUBCMD_CHANGE_AUDIO_PROFILE = 3,
	SUBCMD_ADD_AUDIO_PROFILE    = 4,
	SUBCMD_DEL_AUDIO_PROFILE    = 5,
} SVNP_SUBCMD_AUDIO_INFO;

/**
 * @bf CMD_LAYOUT_INFO의 sub command
 */
typedef enum
{
	SUBCMD_GET_LAYOUT_PROFILE = 1,
	SUBCMD_SET_LAYOUT_PROFILE,
	SUBCMD_GET_LAYOUT_PROFILE_LIST,
	SUBCMD_SET_LAYOUT_PROFILE_LIST,
	SUBCMD_GET_DISPLAY_LAYOUT,
	SUBCMD_SET_DISPLAY_LAYOUT,
	SUBCMD_GET_DISPLAY_LAYOUT_LIST,
	SUBCMD_SET_DISPLAY_LAYOUT_LIST,
	SUBCMD_ADD_CAMERA,
	SUBCMD_DEL_CAMERA,
	SUBCMD_ADD_DECODER,
	SUBCMD_GET_SET_SEQUENCE_LIST,
	SUBCMD_SEQUENCE_ON,
	SUBCMD_SEQUENCE_OFF,
	SUBCMD_FULLSCREEN_ON,
	SUBCMD_FULLSCREEN_OFF,
	SUBCMD_EVENT_POPUP_ON,
	SUBCMD_EVENT_POPUP_OFF,
} SVNP_SUBCMD_LAYOUT_INFO;

/**
 * @bf CMD_SYSTEM_SETTING의 sub command
 */
typedef enum
{
	SUBCMD_DEVICE_SETTING = 1,
	SUBCMD_TIME_SETTING,
	SUBCMD_NETWORK_SETTING,
	SUBCMD_USERS_SETTING,
	SUBCMD_CHANGE_DEVICE_SETTING,
	SUBCMD_CHANGE_TIME_SETTING,
	SUBCMD_ADD_NETWORK_DEVICE,
	SUBCMD_CHANGE_NETWORK_CONFIG,
} SVNP_SUBCMD_SYSTEM_SETTING;

/**
 * @bf CMD_SYSTEM_LOG의 sub command
 */
typedef enum
{
	SUBCMD_REQ_LOG_KEYWORD = 1,
	SUBCMD_REQ_LOG_DATA
} SVNP_SUBCMD_SYSTEM_LOG;

/**
 * @bf CMD_PTZ_PRESET_SETUP의 sub command
 */
typedef enum
{
	SUBCMD_GET_PRESET = 1,
	SUBCMD_SET_PRESET,
	SUBCMD_GET_PRESET_LIST,
	SUBCMD_SET_PRESET_LIST,
} SVNP_SUBCMD_PTZ_PRESET_SETUP;

/**
 * @bf CMD_PTZ_FUNCTION_SETUP의 sub command
 */
typedef enum
{
	SUBCMD_GET_AUTOPAN = 1,
	SUBCMD_GET_AUTOPAN_LIST,
	SUBCMD_GET_SWING,
	SUBCMD_GET_SWING_LIST,
	SUBCMD_GET_GROUP,
	SUBCMD_GET_GROUP_LIST,
	SUBCMD_GET_TOUR,
	SUBCMD_GET_TOUR_LIST,
	SUBCMD_GET_TRACE,		//=SUBCMD_GET_PATTERN
	SUBCMD_GET_TRACE_LIST,	//=SUBCMD_GET_PATTERN_LIST
} SVNP_SUBCMD_PTZ_FUNCTION_SETUP;

/**
 * @bf CMD_CAMERA_SETUP의 sub command
 */
typedef enum
{
	SUBCMD_CAMERA_SETTING_REQ = 1,
	SUBCMD_CAMERA_SETTING_CHANGE,
} SVNP_SUBCMD_CMD_CAMERA_SETUP;

/**
 * @bf NOTIFY_USER_EVENT의 sub command
 */
typedef enum
{
	SUBCMD_NOTIFY_LOGIN = 1,
	SUBCMD_NOTIFY_LOGOUT,
	SUBCMD_NOTIFY_FORCE_LOGOUT,
	SUBCMD_NOTIFY_PASSWORD_CHANGE,
} SVNP_SUBCMD_NOTIFY_USER_EVENT;

/**
 * @bf NOTIFY_SYSTEM_EVENT의 sub command
 */
typedef enum
{
	SUBCMD_NOTIFY_STORAGE_FAILURE = 1,
	SUBCMD_NOTIFY_STORAGE_DEFECT,
	SUBCMD_NOTIFY_STORAGE_FULL,
	SUBCMD_NOTIFY_FAN_FAILURE,
	SUBCMD_NOTIFY_RESTART,
	SUBCMD_NOTIFY_SHUTDOWN,
} SVNP_SUBCMD_NOTIFY_SYSTEM_EVENT;

/**
 * @bf NOTIFY_CONFIGURATION_CHANGED의 sub command
 */
typedef enum
{
	SUBCMD_NOTIFY_VIDEO_PROFILE_CHANGED = 1,
	SUBCMD_NOTIFY_AUDIO_PROFILE_CHANGED,
	SUBCMD_NOTIFY_CHANNEL_CONFIGURATION_CHANGED,
	SUBCMD_NOTIFY_LAYOUT_CONFIGURATION_CHANGED,
	SUBCMD_NOTIFY_DISPLAY_CONFIGURATION_CHANGED,
	SUBCMD_NOTIFY_HOSTNAME_CHANGED,
	SUBCMD_NOTIFY_DATETIME_CHANGED,
} SVNP_SUBCMD_NOTIFY_CONFIGURATION_CHANGED;

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
//                               SUB COMMANDS                                 //
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//

/**
 * @bf SVNP 오류 코드
 */
typedef enum {
	RES_NOERR					        = 0x0000,
	RES_NO_DATA							= 0x0001,
	RES_REQUEST_TIME_ERROR				= 0x0002,
	RES_INVALID_CHANNEL					= 0x0003,
	RES_INVALID_PACKET_LENGTH	        = 0x1001,
	RES_INVALID_CRC_HEADER				= 0x1002,					
	RES_INVALID_CRC_DATA				= 0x1003,
	RES_INVALID_FORMAT					= 0x1004,
	RES_INVALID_COMMAND					= 0x1005,
	RES_VERSION_NOT_SUPPORTED			= 0x1006,
	RES_CONTINUE						= 0x1007,
	RES_BAD_REQUEST						= 0x1008,
	RES_UNAUTHORIZED					= 0x1009,
	RES_NOT_ALLOWED						= 0x100A,
	RES_RESPONSE_TIMEOUT				= 0x100B,
	RES_INVALID_RANGE					= 0x100C,
	RES_INTERNAL_SERVER_ERROR			= 0x100D,
	RES_SERVICE_UNAVAILABLE				= 0x100E,

	RES_LOGIN_INVALID_ID		        = 0x1101,
	RES_LOGIN_INVALID_PASSWORD			= 0x1102,
	RES_LOGIN_USER_FULL					= 0x1103,
	RES_LOGIN_USER_CONFLICT				= 0x1104,
	RES_LOGIN_NEW_SESSION				= 0x1105,

	RES_UNSUPPORTED_MEDIA_TYPE			= 0x1151,
	RES_NOT_ENOUGH_BANDWIDTH			= 0x1152,

	RES_UNKNOWN							= 0x1201,
	RES_REQUEST_VLOSS					= 0x1202,

    RES_MEDIA_SESSION_ID_EMPTY			= 0x1211,

	RES_IV_NOT_SUPPORTED_FUNCTION		= 0x1301,
	RES_IV_NOT_ACTIVATED				= 0x1302,
	RES_IV_NOT_SUPPORTED_RESOLUTION		= 0x1303,
	
	//SVNP 2.0
	RES_SUBCMD_NOT_ALLOWED				= 0x1401,	

	//SVNP 2.0 COMMON 5/31 SPEC  
	//CMD_LOGIN2 
	RES_INVALID_LOGIN2_PROCEDURE =0x2101, 
	RES_INVALID_NONCE_S =0x2102,
	//CMD_REPLAY_AUTHORITY 
	RES_NO_REPLAY_AUTHORITY =0x2121, 
	RES_ALREADY_GET_REPLAY_AUTHORITY =0x2122, 
	//CMD_QUERY_METADATA 
	RES_QUERY_DATA_PARSE_ERROR =0x2131, 
	RES_TIME_ERROR =0x2132, 
	RES_UNKNOWN_OBJECT =0x2133,
	//NOTIFY_QOS_INFO 
	RES_INVALID_QOS_INFO = 0x2111
} SVNP_RESPONSE_CODE;

/**
 * @bf 장치의 모델 코드
 * @remark SVNP1.2 지원 장치가지만 모델코드를 관리함
 *         SVNP2.x 이후 부터는 장치의 모델코드를 관리하지 않음.
 */
typedef enum
{
	// VNP 1.0 or Other
	DTYPE_DVR_NONE		= 0x00,

	DTYPE_DVR_SHR2040	= 0x101,
	DTYPE_DVR_SHR2041,
	DTYPE_DVR_SHR2042,

	DTYPE_DVR_SHR2080	= 0x111,
	DTYPE_DVR_SHR2081,
	DTYPE_DVR_SHR2082,

	DTYPE_DVR_SHR2160	= 0x121,
	DTYPE_DVR_SHR2161,
	DTYPE_DVR_SHR2162,

	DTYPE_DVR_SHR4080	= 0x201,
	DTYPE_DVR_SHR4081,
	DTYPE_DVR_SHR4082,

	DTYPE_DVR_SHR4160	= 0x211,
	DTYPE_DVR_SHR4161,
	DTYPE_DVR_SHR4162,

	DTYPE_ENC_SNT1010	= 0x401,		// By SUN07 - 2009.02.26 : SNT-1010, IP-Camera(5395, 2315, M300)

	DTYPE_IPC_SNCC7478  = 0x501,		// By SUN07 - 2009.02.26 : SCC-C7478 Model Device ID 추가
	DTYPE_IPC_SNCC6225  = 0x502,        // By SUN07 - 2009.05.18 : SNC-C6225 Model Device ID 추가
	DTYPE_IPC_SNCC7225  = 0x503,        // By SUN07 - 2009.05.18 : SNC-C7225 Model Device ID 추가
	DTYPE_IPC_SNCC7479  = 0x504,        // By SUN07 - 2009.11.02 : SNC-C7479 Model Device ID 추가 (7478 + 43x Zoom Module)

	DTYPE_IPC_SNCB2331  = 0x601,        // By SUN07 - 2009.03.25 : SNC-B2331 Model Device ID 추가
	DTYPE_IPC_SNCB2335  = 0x602,        // By SUN07 - 2009.04.02 : SNC-B2335 Model Device ID 추가
	DTYPE_IPC_SNCB5368  = 0x611,		// By SUN07 - 2009.04.25 : SNC-B5368 Model Device ID 추가
	DTYPE_IPC_SNCB5399  = 0x612,        // By SUN07 - 2009.04.25 : SNC-B5399 Model Device ID 추가

	DTYPE_IPC_SNCM2110  = 0x621,		// By SUN07 - 2009.09.02 : SNC-M2110 Model Device ID 추가
	DTYPE_IPC_SNCM5130  = 0x622,		// By SUN07 - 2009.09.02 : SNC-M5130 Model Device ID 추가
	DTYPE_IPC_SNCM5131  = 0x623,		// By SUN07 - 2009.09.02 : SNC-M5131 Model Device ID 추가
	DTYPE_IPC_SNCM5140  = 0x624,		// By SUN07 - 2009.09.02 : SNC-M5140 Model Device ID 추가
	DTYPE_IPC_SNCM5150  = 0x625,		// By SUN07 - 2009.09.02 : SNC-M5150 Model Device ID 추가


	DTYPE_IPC_SNB2000   = 0x1601,        // By Kim KD - 2010.02.03 : SNB-2000 Model Device ID 추가
	DTYPE_IPC_SNB3000   = 0x1602,        // By Kim KD - 2010.02.03 : SNB-3000 Model Device ID 추가

	DTYPE_IPC_SND3080   = 0x1611,        // By Kim KD - 2010.02.03 : SND-3080 Model Device ID 추가
	DTYPE_IPC_SNV3080   = 0x1612,        // By Kim KD - 2010.02.03 : SNV-3080 Model Device ID 추가
	DTYPE_IPC_SND3080C	= 0x1613,		 //SND-3080CF - 장철진B
	DTYPE_IPC_SNV3080C	= 0x1614,		 //SNV-3080CF - 장철진B
	DTYPE_IPC_SND3081	= 0x1615,

	// VNP 2.0

	DTYPE_IPC_SNB5000   = 0x1631,		// By kyungduk.kim - 2010.02.07 : SNB-5000 Model Device ID 추가

	DTYPE_IPC_SND5080	= 0x1641,		// By kyungduk.kim - 2010.02.22 : SND-5080 Model Device ID 추가
	DTYPE_IPC_SND5080F  = 0x1641,		// By kyungduk.kim - 2010.02.22 : SND-5080F Model Device ID 추가
	DTYPE_IPC_SNV5080   = 0x1643,		// By kyungduk.kim - 2010.02.22 : SNV-5080 Model Device ID 추가
	DTYPE_IPC_SNO5080R  = 0x1644,		//SNO-5080R - 장철진B
	DTYPE_IPC_SNV5080R  = 0x1645,		//SNV-5080R - 장철진B

	DTYPE_IPC_SNV3120	= 0x1651,		//SNV-3120 - 장철진B
	DTYPE_IPC_SNP3120	= 0x1652,		//SNP-3120 - 장철진B
	DTYPE_IPC_SNP3120V	= 0x1653,		//SNP-3120V - 장철진B
	DTYPE_IPC_SNP3120VH	= 0x1654,		//SNP-3120VH - 장철진B

	DTYPE_IPC_SND5010   = 0x1661,		//SND-5010 - 장철진B
	DTYPE_IPC_SNV5010   = 0x1662,		//SNV-5010 - 장철진B

	DTYPE_IPC_SNZ3370	= 0x1671,		//SNZ-3370 - 장철진B

	DTYPE_IPC_SNP5200   = 0x1681,		//SNP-5200 - 장철진B
	DTYPE_IPC_SNP5200TH = 0x1682,		//SNP-5200TH - 장철진B
	DTYPE_IPC_SNP5200H  = 0x1683,		//SNP-5200H - 장철진B
	DTYPE_IPC_SNZ5200   = 0x1684,		//SNZ-5200 - 장철진B

	DTYPE_IPC_SNB1000	= 0x1691,		//SNB-1000 - 조성용B
	DTYPE_IPC_SND1010	= 0x1701,		//SND-1010 - 조성용B
	DTYPE_IPC_SNB6000	= 0x1711,		//SNB-6000 - 조성용B

	DTYPE_IPC_SNB6001	= 0x1721,		//SNB-6000 - 김영한B

	DTYPE_IPC_SNV6080	= 0x1731,		//SNV-6080 - 김영한B
	DTYPE_IPC_SND6080   = 0x1732,		//SND-6080

	DTYPE_IPC_SNB7000	= 0x1741,		//SNB-7000 - 김영한B

	DTYPE_IPC_SNV7080	= 0x1751,		//SNV-7080 - 김영한B
	DTYPE_IPC_SND7080	= 0x1752,		//SND-7080
	DTYPE_IPC_SND7080F  = 0x1753,		//SND-7080F	- 이영기K
	DTYPE_IPC_SNV7080R  = 0x1754,		//SND-7080R	- 이영기K

	DTYPE_ENC_SPE100	= 0x1761,		//SPE-100 - 명지태K
	DTYPE_ENC_SPE400	= 0x1762,		//SPE-400 - 명지태K
	DTYPE_ENC_SPE400B	= 0x1763,		//SPE-400B - 명지태K
	DTYPE_ENC_SPE1600R	= 0x1764,		//SPE-1600R - 명지태K

	DTYPE_IPC_SNO1080R	= 0x1771,		//SNO-1080R
	DTYPE_IPC_SNB1001	= 0x1772,		//SNB-1001
	DTYPE_IPC_SNV1010	= 0x1773,		//SNV-1010
	DTYPE_IPC_SNV1080	= 0x1774,		//SNV-1080
	DTYPE_IPC_SND1080	= 0x1775,		//SND-1080

	DTYPE_IPC_SNP3430H	= 0x1781,		//SNP-3430H - 조성용B

	DTYPE_IPC_SNP6200	= 0X1791,		//SNP-6200
	DTYPE_IPC_SNP6200H	= 0X1792,		//SNP-6200H
	DTYPE_IPC_SNZ6200	= 0X1793,		//SNZ-6200

	DTYPE_IPC_SNB9050	= 0x1801,		//SNB-9050 - 장철진B
	DTYPE_IPC_SNB9051	= 0x1802,		//SNB-9051 - 장철진B

	DTYPE_ENC_SPE10		= 0x1811,		//SPE-10
	DTYPE_ENC_SPE40		= 0X1812,		//SPE-40
	DTYPE_DEC_SPD400    = 0X1821,		//SPD-400
} SVNPE_DEVICE_MODEL;


/////////////////////////////////////////////
//		Inserted XNSE Defines
/////////////////////////////////////////////
typedef enum
{
//	MTYPE_NONE = 0x0,
	SVNP_MTYPE_NONE 		= 0X00, //SSO ADD
	SVNP_MTYPE_LIVE 		= 0x01,
	SVNP_MTYPE_SEARCH_NET	= 0x02, 
	SVNP_MTYPE_REPLAY		= 0x04,
	SVNP_MTYPE_SEARCH_LOCAL = 0x08,
	SVNP_MTYPE_SEARCH_CLIP	= 0x10,
	SVNP_MTYPE_BACKUP_NET	= 0x20,
	SVNP_MTYPE_BACKUP_LOCAL = 0x40,
	SVNP_MTYPE_COPY 		= 0x80, //hong
} SVNP_MEDIA_TYPE;

typedef enum
{
	VNP_MPEG_UNKNOWN = 0,
	VNP_MPEG_IVOP, 		//usen in mjpeg
	VNP_MPEG_PVOP,
	VNP_MPEG_BVOP,
	VNP_BACKUP_END,
	VNP_THUMBNAIL,
	VNP_THUMBNAIL_END

} MPEG_FRAME_TYPE;	

typedef enum
{
	SS_STATUS_NONE			= 0,
	SS_STATUS_INIT			= 1,
	SS_STATUS_CONNECTING	= 2,
	SS_STATUS_CONNECTED		= 3,
	SS_STATUS_LOGIN			= 4,
	SS_STATUS_CLOSING		= 5,
	SS_STATUS_DISCONNECTED  = 6,

} DME_SS_STATUS;

typedef enum
{
//	RES_NOERR					= 0x0000,

//	ERR_SUCCESS = 0x0001,
	ERR_SW_UNKNOWN = 0x0001,		// 리턴값 1 : 파일 업로드 실패	
	ERR_SW_NO_OBJECT = 0x0002,
	ERR_SW_INVALID_PARAMETER = 0x0003,
	ERR_SW_DISCONNECTED = 0x0004,
	ERR_SW_NOTCONNECT = 0x0005,			// 리턴값 5 : upload server에 연결할 수 없습니다.
	ERR_SW_FILEOPEN = 0x0007,
	ERR_SW_NETWORK = 0x0009,

	ERR_SW_UPLOAD_FILESIZE = 0x0006,		//// 리턴값 6 : 파일의 크기가 너무 큽니다.
	ERR_SW_UPLOAD_DEVICE_FAIL = 0x0008,
	ERR_SW_UPLOAD_ING		= 0x0010,
	ERR_CREATE_SOCK			= 0x0011,
	ERR_WRONG_IP			= 0x0012,
	ERR_WRONG_ID			= 0x0013,
	ERR_WRONG_PW			= 0x0014

	//0x0000			- success
	//0x0001 ~ 0x0999
	//0x1000 ~ 0x2000	- rescode from device
} DME_ERROR;

//}//namespace svnp
#endif //_VNP_DEFINE_H_