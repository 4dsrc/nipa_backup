////////////////////////////////////////////////////////////////////////////////
//
//	ESMIndex.h : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-18
//
////////////////////////////////////////////////////////////////////////////////


//---------------------------------------------------------------------------
//-- MOVIE
//---------------------------------------------------------------------------
// #define movie_frame_per_second			25	//30
// #define movie_next_frame_time			40	//33
// #define movie_frame_per_second			30
// #define movie_next_frame_time			33
// #define movie_next_frame_time_margin	2

#pragma once

#include "ESMIndexStructure.h"
#include <vector>
using namespace std;

//---------------------------------------------------------------------------
//-- STRING
//---------------------------------------------------------------------------

// Add Type
#define ADD_HEAD			1
#define ADD_TAIL			2
#define ADD_INSERT			3

#define	ESMLOG_LINE_SIZE		4096

#define IMG_SCALING_RATE			0.5
#define	ESM_DEFAULT_RETRY			5
#define	ESM_DEFAULT_RETRY_SLEEP		10000

#define	SERIAL_PORT_SIZE			8
#define INFO_LF_CHECK				4

//-- 2010-3-24 hongsu.jung
//-- Color
#define LIST_COLOR_CRITICAL			RGB(249, 32, 43)
#define LIST_COLOR_NORMAL			RGB(154,204, 38)
#define LIST_COLOR_TEXT				RGB(  0,  0,  0)

//-- 2012-06-13 hongsu@esmlab.com
//-- Add Registry
#define ESM_RECENTLY_REG_SIZE		9
#define ESM_RECENTLY_MENUBAR_LOC	10
#define ESM_MENUBAR_LOC				0

#define ARRAY_SIZE(A)	(sizeof(A) / sizeof(A[0]))

#define ESM_MAKE_SERVER_OFF			0
#define ESM_MAKE_SERVER_ON			1
//- 2014-02-19 kcd
//- Mode State
#define ESM_MODESTATE_SERVER		0		// Server 역할( Program 문법상 Client ) 
#define ESM_MODESTATE_CLIENT		1		// Client 역할( Program 문법상 Server ) 

#define ESM_SERVERMODE_BOTH			0		// Server 레코딩 & 영상제작
#define ESM_SERVERMODE_MAKING		1		// Server 영상제작만
#define ESM_SERVERMODE_RECORDING	2		// Server 레코딩만

//---------------------------------------------------------------------------
//-- SIZE 
//---------------------------------------------------------------------------

//#define EXIT						-1
#define MAX_4DP						50
#define NOT_FOUND					-1
#define ESM_UNKNOWN					-1
#define ESM_SELECT_ALL				-1

#define DSC_LOCAL_CNT				14

#define DEFAULT_HALFSHUTTER			2000
#define MAX_EDIT_TEXT_LENGTH		30000
#define DEFAULT_FILEINDEX			0
#define	WAIT_NEXT_TC_TIME			2000
#define	RETRY_COMMUNICATION			1000
#define COMPARE_PATTERN_SCORE		500
#define	DEFAULT_IMAGE_INTERVAL		100
#define	DEFAULT_MLC_INTERVAL		300
#define DEFAULT_CHECK_PTP_TIME		5000
#define DEFAULT_INTERVAL_TMS_BUFFER	250
#define DEFAULT_WAIT_MESSAGE		5000
#define MAX_IMAGECOUNT				20000
#define DEFAULT_WAIT_SAVEDSC		2500
#define DEFAULT_SYNG_TIME_MARGIN	1000
#define DEFAULT_SENSORON_TIME		23
#define	DEFAULT_TARGETLENGTH		100	
#define	DEFAULT_CAMERAZOOM			20	
#define	DEFAULT_PIXEL1920_DISTANCE		1699		//Image 상의 피사체 Pixel 길이 [피사체길이(100Cm) - 피사체와 Camera 거리(1m)]
#define	DEFAULT_PIXEL3840_2160_DISTANCE		3398		//Image 상의 피사체 Pixel 길이 [피사체길이(100Cm) - 피사체와 Camera 거리(1m)]
#define	DEFAULT_PIXEL5472_DISTANCE		4900		//Image 상의 피사체 Pixel 길이 [피사체길이(100Cm) - 피사체와 Camera 거리(1m)]
#define	DEFAULT_PIXEL5472_3080_DISTANCE		1970//1890		//Image 상의 피사체 Pixel 길이 [피사체길이(100Cm) - 피사체와 Camera 거리(1m)]
#define DEFAULT_ESM_PICTURE_DRAGTIME	132
#define DEFAULT_ESM_SDKDELAY_WAITTIME	10000

#define DEFAULT_ESM_DSC_SYNC_WAITTIME	2000

#define EFFECT_ZOOM_RATIO_MIN				100
#define EFFECT_ZOOM_RATIO_MAX				900
#define EFFECT_ZOOM_WEIGHT_MIN				0.1
#define EFFECT_ZOOM_WEIGHT_MAX				10
#define EFFECT_BLUR_SIZE_MIN				10
#define EFFECT_BLUR_SIZE_MAX				100
#define EFFECT_BLUR_WEIGHT_MIN				0
#define EFFECT_BLUR_WEIGHT_MAX				100


#define MAX_COMMAND_LENG			128
#define LIST_END_VALUE				0xFFFFFFFF
#define LIST_END_STRING				_T("NULL")
 
//-- 2013-04-29 hongsu@esmlab.com
//-- Movie Make
//CMiLRe 20151020 TimeLine 수정 후 Effect List Update
#define FULL_HD_WIDTH			1920
#define FULL_HD_HEIGHT			1080
#define FULL_UHD_WIDTH			3840
#define FULL_UHD_HEIGHT			2160

//-- Frame Size
#define ESM_TOOLBAR_HEIGHT		26	//30
#define ESM_SUB_BAR_HEIGHT		30
#define FRAME_BASIC_MARGIN		10
#define FRAME_MIN_HEIGHT		90
#define FRAME_MIN_WIDTH			150

#define START_SPOT				1
#define END_SPOT				2

//-- TIME SYNC
#define TIME_SYNC_MARGIN_NET	2000

//-- ADJUST BASE VALUE
#define ADJ_BASIC_VAL_THRESHOLD			150
#define ADJ_BASIC_VAL_FLOWMO_TIME		50
#define ADJ_BASIC_VAL_AUTOBRIGHTNESS	15
#define ADJ_BASIC_VAL_FIRSTPOINTY		180
#define ADJ_BASIC_VAL_POINTX			850
#define ADJ_BASIC_VAL_XRANGE			230
#define ADJ_BASIC_VAL_FIRSTYRANGE		160
#define ADJ_BASIC_VAL_SECONDYRANGE		160
#define ADJ_BASIC_VAL_THIRDYRANGE		160
#define ADJ_BASIC_VAL_TARGETLENGTH		101
#define	ADJ_BASIC_VAL_CAMERAZOOM		20

#define ADJ_BASIC_VAL_POINTGAPMIN		150
#define ADJ_BASIC_VAL_MIN_WIDTH			12
#define ADJ_BASIC_VAL_MIN_HEIGHT		8
#define ADJ_BASIC_VAL_MAX_WIDTH			80
#define ADJ_BASIC_VAL_MAX_HEIGHT		60
#define ADJ_BASIC_VAL_TARGETLENTH		100

//-- MAIN TESESMUARANTEE MESSAGE
#define	WM_ESM									0x1000

//jhhan 16-10-18 http
#define WM_ESM_SERVER							0x1010
#define WM_ESM_GET_INFO							0x1011
#define WM_ESM_GET_STATUS						0x1012
#define WM_ESM_GET_COMMAND						0x1013
#define WM_ESM_NOTIFY							0x1014
#define WM_ESM_NOTIFY_SYNC						0x1015
//--
//-- CREATE
#define WM_ESM_CREATE							0x1020
#define WM_ESM_DESTROY							0x1021

//-- 2011-10-20 hongsu.jung
//-- DSC
#define	WM_ESM_DSC								0x1030
#define	WM_ESM_DSC_OPEN_SESSION					0x1031
#define WM_ESM_DSC_SYNC_RESULT					0x1032
#define WM_ESM_DSC_SYNC_CHECK					0x1033
#define WM_ESM_DSC_GET_TICK_RESULT_CMD			0x1034
//--
//-- LIST (MainGrid)
#define WM_ESM_LIST_HIDDEN_COMMAND				0x1039
#define WM_ESM_LIST								0x1040
#define WM_ESM_LIST_CHECK						0x1041
#define WM_ESM_LIST_ADD							0x1042
#define WM_ESM_LIST_SELECT						0x1043
#define	WM_ESM_LIST_GET_PROPERTY				0x1044
#define	WM_ESM_LIST_SET_PROPERTY				0x1045
#define WM_ESM_LIST_SET_PROPERTY_EX				0x1046
#define	WM_ESM_LIST_SET_GROUP_PROPERTY			0x1047
#define	WM_ESM_LIST_PROPERTY_RESULT				0x1048
#define	WM_ESM_LIST_FORMAT_ALL					0x1049
#define	WM_ESM_LIST_GETFOCUS_TOCLIENT			0x104a
#define	WM_ESM_LIST_GETFOCUS_TOSERVER			0x1050
#define WM_ESM_LIST_SETFOCUS_TOCLIENT			0x1051
#define WM_ESM_LIST_PROPERTY_ISO_INCREASE		0x1052
#define WM_ESM_LIST_PROPERTY_ISO_DECREASE		0x1053
#define	WM_ESM_LIST_FW_UPDATE					0x1054
#define	WM_ESM_LIST_VIEWLARGE					0x1055
#define	WM_ESM_LIST_SET_FOCUS					0x1056
#define	WM_ESM_LIST_GETITEMFOCUS				0x1057
#define	WM_ESM_LIST_INITMOVIEFILE				0x1058
#define	WM_ESM_LIST_CAMSHUTDOWN					0x1059
#define	WM_ESM_LIST_CAPTURERESUME				0x105a
#define	WM_ESM_LIST_MOVIEMAKINGSTOP				0x105b
#define WM_ESM_LIST_SCROLL						0x105c
//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
#define WM_ESM_LIST_PROPERTY_FOCUS_ALL			0x105d
#define WM_ESM_LIST_GETFOCUS_TOCLIENT_READ_ONLY 0x105e
#define WM_ESM_LIST_SET_FOCUS_TOCLIENT			0x105f
#define WM_ESM_LIST_SET_FOCUS_VALUE				0x1060
#define	WM_ESM_LIST_SETFOCUS_LOADFILE			0x1061
#define	WM_ESM_LIST_SAVESYNC					0x1062
#define	WM_ESM_LIST_HALF_SHUTTER_TOCLIENT		0x1063
#define	WM_ESM_LIST_SET_ENLARGE_TOCLIENT		0x1064
#define WM_ESM_LIST_FOCUS_LOCATE_SAVE_TOCLIENT	0X1065
#define WM_ESM_LIST_GET_FOCUS_FRAME_TOSERVER	0X1066
#define WM_ESM_LIST_SET_FOCUS_FRAME_TOCLIENT	0X1067
#define WM_ESM_LIST_ONE_SHOT_AF					0X1068
#define WM_ESM_LIST_CAM_ERROR_MSG				0X1069



//--
//-- FRAME
#define WM_ESM_FRAME							0x1070
#define WM_ESM_FRAME_SHOW						0x1071
#define WM_ESM_FRAME_REMOVE_OBJ_SELECTOR		0x1072
#define WM_ESM_FRAME_REMOVE_OBJ_EDITOR			0x1073
#define WM_ESM_FRAME_REMOVEALL_OBJ_EDITOR		0x1074
#define WM_ESM_FRAME_INSERT_OBJ_EDITOR_M		0x1075
#define WM_ESM_FRAME_INSERT_OBJ_EDITOR_P		0x1076
#define WM_ESM_FRAME_COPY_OBJ_EDITOR			0x1077
#define WM_ESM_FRAME_UPDATE_OBJ_EDITOR			0x1078
#define WM_ESM_FRAME_CONVERT_FINISH				0x1079
//CMiLRe 20151020 TimeLine Object Delete Key 추가
#define WM_ESM_FRAME_REMOVE_OBJ_TAIL			0x107a
#define WM_ESM_FRAME_REMOVE_OBJ_HEADER			0x107b
//-- 2013-12-12 kjpark@esmlab.com
//-- if converting, toolbar disenable
#define WM_ESM_FRAME_TOOLBAR_SHOW				0x107c
//-- 2013-12-14 kcd
//- 
#define WM_ESM_FRAME_CONNECTDIVECE				0x107d
#define WM_ESM_FRAME_ADJUST_EFFECT_INFO			0x107e
#define WM_ESM_FRAME_RESET						0x107f
//jhhan 17-01-09
#define WM_ESM_FRAME_PROCESSOR_PLAY				0x1080
#define WM_ESM_FRAME_PROCESSOR_INIT				0x1081
//yongmin 17-04-21
#define WM_ESM_FRAME_MUX_FINISH					0x1082
#define WM_ESM_FRAME_FOCUS						0x1083

#define WM_ESM_FRAME_DRAW_TIMELINE				0x1084
//--
//-- PROPERTY
#define WM_ESM_PROPERTY							0x1085
#define WM_ESM_PROPERTY_REFRESH     			0x1086
#define WM_ESM_FRAME_REFEREE_MUX_FINISH			0x1087

//--
//-- OPTION
#define WM_ESM_OPT								0x1090
#define WM_ESM_OPT_MAIN							0x1091
#define WM_OPTION_SETPATH						0x1092
#define WM_OPTION_SETBASE						0x1093
#define WM_OPTION_SETETC						0x1094
#define WM_OPTION_SETCOUNTDOWN					0x1095
//CMiLRe 20160106 record load 단축키
#define WM_OPTION_LOAD_RECORD					0x1096
#define WM_OPTION_MOVIESIZE						0x1097
#define WM_OPTION_MAKE_TYPE						0x1098

#define WM_OPTION_ADJ_SETPATH					0x1099
#define WM_OPTION_MAN_SETCOPY					0x109a
#define WM_OPTION_MAN_SETDEL					0x109b
//jhhan
#define WM_OPTION_WAIT_SAVE_DSC_SEL				0x109c
#define WM_OPTION_SETSYNC						0x109d
//wgkim 170727
#define WM_OPTION_LIGHT_WEIGHT					0x109e
//wgkim 171108
#define WM_OPTION_VIEW_INFO						0x109f
#define WM_OPTION_RECORD_FILE_SPLIT				0x10a1
//wgkim 180629
#define WM_OPTION_SET_FRAMERATE					0x10a2
//wgkim 181005
#define WM_OPTION_SET_AUTOADJUST_KZONE			0x10a3

//--
//-- NETWORK
#define WM_ESM_NET								0x1100
#define WM_ESM_NET_LIVEVIEW_RESULT				0x1101
#define	WM_ESM_NET_ALLDISCONNET					0x1102
#define	WM_ESM_NET_DSCCONSTATUS					0X1103
#define WM_ESM_NET_REDRAW						0x1104
#define WM_ESM_NET_OPTION_PATH					0x1105
#define WM_ESM_NET_OPTION_BASE					0x1106
#define WM_ESM_NET_OPTION_COUNTDOWN				0x1107
#define WM_ESM_NET_OPTION_ETC					0x1108
#define WM_ESM_NET_MAKEMOVIETOCLIENT			0x1109
#define WM_ESM_NET_MAKEMOVIE_FINISH				0x110a
#define WM_ESM_NET_SENSORSYNCOUT				0x110b
#define WM_ESM_NET_ERRORFRAME					0x110c
#define WM_ESM_NET_EXCEPTIONDSC					0x110d
#define WM_ESM_NET_ADDFRAME						0x110e
#define WM_ESM_NET_SKIPFRAME					0x110f
#define WM_ESM_NET_CAPTUREDELAY					0x1110
#define WM_ESM_NET_VALIDSKIP					0x1111
#define WM_ESM_NET_RECORDFINISH					0x1112
#define WM_ESM_NET_COPYTOSERVER					0x1113
#define WM_ESM_NET_CONNECT_CHECK				0x1114
#define WM_ESM_NET_MAKEFRAMEMOVIETOCLIENT		0x1115
#define WM_ESM_NET_MAKEFRAMEMOVIEFINISHTOSERVER	0x1116
#define WM_ESM_NET_SENSORSYNCTIME				0x1117
#define WM_ESM_NET_OPTION_MOVIESIZE				0x1118
#define WM_ESM_NET_OPTION_ADJ_TARGET_PATH		0x1119
#define WM_ESM_NET_OPTION_MAKING_TYPE			0x111a
#define WM_ESM_NET_DELETE_DSC					0x111b
#define WM_ESM_NET_PROPERTY_1STEP				0x111c
#define WM_ESM_NET_SEND_KTSENDTIME				0x111d
#define WM_ESM_NET_SEND_KTSTOPTIME				0x111e
#define WM_ESM_NET_OPTION_MAN_COPY_PATH			0x1120
#define WM_ESM_NET_OPTION_MAN_DEL_PACH			0x1121
#define WM_ESM_NET_GET_DSIKSIZE					0x1122
#define WM_ESM_NET_ERROR_MSG					0x1123
#define WM_ESM_NET_ERROR_MSG_GH5				0x1124
#define WM_ESM_NET_CONNECT_STATUS_CHECK			0x1125
#define WM_ESM_NET_CONNECT_STATUS_CHECK_RESULT	0x1126
//jhhan 16-10-11
#define WM_ESM_NET_DELETE_MAKE					0x1127
#define WM_ESM_NET_RECORD_STATUS				0x1128//(CAMREVISION)
#define WM_ESM_NET_OPTION_SYNC					0x1129
//wgkim 170727
#define WM_ESM_NET_OPTION_LIGHT_WEIGHT			0x112a
#define WM_ESM_NET_FRAME_LOAD_TIMETOSERVER		0x112b
#define WM_ESM_NET_RESULT_IMAGE_RECORD_TOSERVER 0x112c
#define WM_ESM_NET_RESULT_GET_TICK_TOSERVER		0x112d
#define WM_ESM_NET_REFEREE_TRANSCODE_FINISH		0x112e
//wgkim 181005
#define WM_ESM_NET_OPTION_AUTOADJUST_KZONE		0x112f0
//--
//-- MOVIE

#define WM_ESM_MOVIE_DELETE_DSC					0x112c
#define WM_ESM_MOVIE							0x112d
#define WM_ESM_MOVIE_SET_MOVIECOUNT				0x112e
#define WM_ESM_MOVIE_SET_ADJUSTINFO				0x112f

#define WM_ESM_MOVIE_SET_ADJUSTINFO_FROM_SERVER	0x1130
#define WM_ESM_MOVIE_SET_MOVIEDATA				0x1131
#define WM_ESM_MOVIE_SET_FPS					0x1132
#define WM_ESM_MOVIE_MAKEMOVIE					0x1133
#define WM_ESM_MOVIE_MAKE_FINISH				0x1134
#define WM_ESM_MOVIE_MAKE_FINISH_FROM_MOVIEMGR	0x1135
#define WM_ESM_MOVIE_SET_MARGIN_FROM_SERVER		0x1136
#define WM_ESM_MOVIE_SET_MARGIN					0x1137
#define WM_ESM_MOVIE_INFORESET					0x1138
#define WM_ESM_MOVIE_VALIDFRAME					0x1139
#define WM_ESM_MOVIE_VALIDRESET					0x113a
#define WM_ESM_MOVIE_VALIDSKIP_FROM_MOVIEMGR	0x113b
#define WM_ESM_MOVIE_MAKINGSTOP					0x113c
#define WM_ESM_MOVIE_MAKEFRAME_CLIENT			0x113d
#define WM_ESM_MOVIE_MAKEFRAME_CLIENT_REVERSE	0x113e
#define WM_ESM_MOVIE_MAKEFRAME_CLIENT_LOCAL		0x113f

#define WM_ESM_MOVIE_MAKEFRAME_CLIENT_REVERSE_LOCAL	0x1140
#define WM_ESM_MOVIE_MAKEFRAME_CLIENT_ADJUST	0x1141
#define WM_ESM_MOVIE_SET_IMAGESIZE				0x1142
#define WM_ESM_MOVIE_MAKE_ADJUST				0x1143
#define WM_ESM_MOVIE_MAKE_ADJUST_COUNT			0x1144
#define WM_ESM_MOVIE_SAVE_ADJUST				0x1145
#define WM_ESM_MOVIE_MAKE_THREAD				0x1146
#define WM_ESM_MOVIE_MAKE_WAIT_END				0x1147
#define WM_ESM_MOVIE_MAKE_THREAD_RUN			0x1148
#define WM_ESM_MOVIE_START_MOVIE_MAKE			0x1149
#define WM_ESM_MOVIE_END_MOVIE_MAKE				0x114a
//jhhan 17-01-05
#define WM_ESM_PROCESSOR_INFO					0x114b	
#define WM_ESM_PROCESSOR_INFO_MAKE				0x114c
#define WM_ESM_PROCESSOR_INFO_FAIL				0x114d
#define WM_ESM_PROCESSOR_LOAD					0x114e
#define WM_ESM_PROCESSOR_LOAD_PLAY				0x114f

#define WM_ESM_PROCESSOR_LOAD_STOP				0x1150
#define WM_ESM_MOVIE_MAKING_INFO				0x1151
#define WM_ESM_MOVIE_SET_MAKING_INFO			0x1152
#define WM_ESM_MOVIE_SAVE_ADJUST_RUN			0x1153

#define WM_ESM_FRAME_LOAD_TIME					0x1154
#define WM_ESM_NET_REQUEST_DATA					0x1155
#define WM_ESM_MOVIE_REQUEST_DATA				0x1156
#define WM_ESM_NET_REQUEST_DATA_TOSERVER		0x1157
#define WM_ESM_NET_REQUEST_SEND_DATA			0x1158

//wgkim
#define WM_ESM_NET_OPTION_VIEW_INFO				0x1159
#define WM_ESM_NET_OPTION_RECORD_FILE_SPLIT		0x115a

//hjcho 180307
#define WM_ESM_MOVIE_REQUEST_MUX_DATA			0x115b

//joonho.kim 180606
#define WM_ESM_MOVIE_START_FILETRANSFER			0x115c
//hjcho 180725
#define WM_ESM_NET_REFEREE_OPENCLOSE			0x115d
#define WM_ESM_NET_REFEREE_MUX_FINISH			0x115e
#define WM_MAKEMOVIE_REFEREE_MUX_FINISH			0x115f

//wgkim 181001
#define WM_ESM_MOVIE_SET_KZONE_POINTS			0x1160
#define WM_ESM_MOVIE_OPTION_AUTOADJUST_KZONE	0x1161

//hjcho 181221
#define WM_ESM_MOVIE_MULTIVIEW_MAKING			0x1162
#define WM_ESM_MOVIE_MULTIVIEW_FINISH			0x1163

//hjcho 170211
#define WM_ESM_MOVIELIST_ADDFILE				0x1166
//hjcho 170315
#define WM_ESM_MOVIELIST_LOADINFO				0x1165

//hjcho 170211
#define WM_ESM_AJA_FINISH							0x1167
#define WM_ESM_AJA_SELECT_PLAY				0x1168
#define WM_ESM_AJA_PLAY_OPT					0x1169
#define WM_ESM_PROCESSOR_ADDFILE		0x1170
#define WM_ESM_AJA_THREADINIT					0x1171
#define WM_ESM_AJA_RELOAD						0x1172

//yongmin 170317
#define WM_ESM_MOVIE_MUX						0x1173

//wgkim 170727
#define WM_ESM_MOVIE_SET_LIGHT_WEIGHT			0x1174
#define WM_ESM_BACKUP_SET_PATH					0x1175
#define WM_ESM_NET_BACKUP_STRING				0x1176
#define WM_BACKUP_STRING						0x1177

//wgkim 170727
#define WM_ESM_MOVIE_SET_FRAME_RATE				0x1178

//wgkim 181001
#define WM_ESM_MOVIE_SET_KZONE_JUDGMENT			0x1179

//--
//-- EFFECT EDITOR
#define WM_ESM_EFFECT							0x114c
#define WM_ESM_EFFECT_UPDATE					0x114d
#define WM_ESM_EFFECT_FRAME_VIEW				0x114e
#define WM_ESM_EFFECT_LIST_CHANGE				0x114f
#define WM_ESM_EFFECT_SPOTLIST_UPDATE			0x1150
//CMiLRe 20151020 TimeLine 수정 후 Effect List Update
#define WM_ESM_EFFECT_REFLASH					0x1151


//-- Movie Make
#define WM_ESM_MOVIE_MAKE_ARRAY_ADD				0x1152
#define WM_ESM_MOVIE_MAKE_ARRAY_MERGE			0x1153

//--
//-- VIEW
#define WM_ESM_VIEW								0x1170
#define	WM_ESM_VIEW_LIVEVIEW_BUFFER				0x1171
#define	WM_ESM_VIEW_REDRAW						0x1172
#define WM_ESM_VIEW_SCALING						0x1173
#define WM_ESM_VIEW_TIMELINE_RESET				0x1174
#define WM_ESM_VIEW_SORT						0x1175
#define WM_ESM_VIEW_ORGANIZE_INDEX				0x1176
#define WM_ESM_VIEW_OBJ_PROGRESS				0x1177
#define WM_ESM_VIEW_SYNC_GROUP					0x1178
#define WM_ESM_VIEW_NET_UPDATE_CONNECTION		0x1179
#define WM_ESM_VIEW_OBJ_UPDATE					0x117a
#define WM_ESM_VIEW_TIMELINE_RELOAD				0x117b
#define WM_ESM_VIEW_MAKEMOVIEPLAY				0x117c
#define WM_ESM_VIEW_FRMESPOTVIEWON				0x117d
#define WM_ESM_VIEW_FRMESPOTVIEWOFF				0x117e
#define WM_ESM_VIEW_CONTROL_MAIN				0x117f
#define WM_ESM_VIEW_SETNEWESTADJ				0x1180
#define WM_ESM_LIST_TIMELINEALLDELETE			0x1181
#define WM_ESM_VIEW_RECORDDONE					0x1182
#define WM_ESM_VIEW_NET_UPDATE_DISKSIZE			0x1183
#define WM_ESM_VIEW_MERGEMOVIE_PUSHBACK			0x1184
#define WM_ESM_VIEW_MERGEMOVIE_CLEAR			0x1185
#define WM_ESM_VIEW_NET_GPUUSE					0x1186
#define WM_ESM_VIEW_AUTODETECT					0x1187
#define WM_ESM_VIEW_INSERTID					0x1188
#define WM_ESM_VIEW_AUTODETECT_FAIL				0x1189
#define WM_ESM_VIEW_DSC_RELOAD					0x118a
#define WM_ESM_VIEW_SAVE_INFO					0x118b
#define WM_ESM_VIEW_AUTODETECT_INFO				0x118c
#define WM_ESM_VIEW_GET_FOCUS_FRAME				0x118d
#define WM_ESM_VIEW_CONTROL_MAIN_ONLY			0x118e
//--
//-- MAKEMOVIE
#define WM_MAKEMOVIE_DP_FRAME					0x1190
#define WM_MAKEMOVIE_DP_MOVIE_FINISH			0x1191
#define WM_MAKEMOVIE_MSG						0x1192
#define WM_MAKEMOVIE_GPU_FILE_ADD_MSG			0x1193
#define WM_MAKEMOVIE_GPU_FILE					0x1194
#define WM_MAKEMOVIE_GPU_FILE_END				0x1195
#define WM_MAKEMOVIE_GPU_FILE_OPEN_CLOSE		0x1196
#define WM_MAKEMOVIE_MUX_MOVIE_FINISH			0x1197
#define WM_MAKEMOVIE_REQUES_DATA				0x1198
#define WM_MAKEMOVIE_REQUEST_DATA_FINISH		0x1199
#define WM_MAKEMOVIE_REQUEST_DATA_SEND_FINISH	0x119a
#define WM_MAKEMOVIE_GPU_FILE_CMD_CLOSE			0x119b
#define WM_MAKEMOVIE_REFEREE_LIVE_MERGE			0x119c
#define WM_MAKEMOVIE_REFEREE_OPENCLOSE			0x119d
#define WM_MAKEMOVIE_REFEREE_MUXSTART			0x119e
//--MainProgram to DLL ESMLOG
#define WM_ESM_LOG								0x11a0
#define WM_ESM_LOG_MSG							0x11a1
//180726 hjcho
#define WM_MAKEMOVIE_REFEREE_4KMOVIE			0x11a2

#define WM_ESM_OPT_MSG							0x11b0
#define WM_ESM_OPT_COPY_SUCCESS					0x11b1
#define WM_ESM_OPT_COPY_FAIL					0x11b2
#define WM_ESM_OPT_DELETE_END					0x11b3
#define WM_ESM_OPT_COPY_TIMER					0x11b4

//jhhan
#define WM_ESM_OPT_COPY_NOT_FOUND				0x11b5
#define WM_ESM_OPT_LOG_COPY_TIMER				0x11b6

//--Option Dialog Shortcut
#define WM_ESM_ENV_PATH							0x11c0
#define WM_ESM_ENV_4D_OPTION					0x11c1
#define WM_ESM_ENV_PC_INFO						0x11c2
#define WM_ESM_ENV_CAMERA_INFO					0x11c3
#define WM_ESM_ENV_TEMPLATE						0x11c4
#define WM_ESM_ENV_MUX							0x11c5
#define WM_ESM_ENV_REMOTECTRL					0x11c6
#define WM_ESM_ENV_REMOTE_RECORDSTATE			0x11c7
#define WM_ESM_ENV_REMOTE_MAKING				0x11c8
#define WM_ESM_REFEREE_MUX						0x11c9
#define WM_ESM_ENV_CEREMONY						0x11ca
#define WM_ESM_ENV_RTSPSTATEVIEW				0x11cd

#define WM_ESM_REMOTE							0x11d0
#define WM_ESM_REMOTE_RECORD					0x11d1
#define WM_ESM_REMOTE_RECORD_STOP				0x11d2
#define WM_ESM_REMOTE_MAKE						0x11d3
#define WM_ESM_REMOTE_SPOT_POINT				0x11d4

// esm property ctrl
#define WM_ESM_SET_HIDDEN_ALL_CMD_TIMER			0x11d5
#define WM_ESM_SET_FOCUS_LOCATE_ALL_CMD_TIMER	0x11d6
#define WM_ESM_GET_PICTURE_WIZARD_DETAIL		0x11d7
#define WM_ESM_GET_PICTURE_WIZARD_DETAIL_ETC	0x11d8

#define WM_ESM_OPT_DELETE_FOLDER_LIST			0x11d9
#define WM_ESM_WB_ADJUST						0x11da
#define WM_ESM_LOAD_FOCUS						0x11db
#define WM_ESM_LOAD_FOCUS_END					0x11dc

//wgkim 170727
//#define WM_ESM_NET_OPTION_LIGHT_WEIGHT			0x112a
#define WM_ESM_NET_OPTION_SET_FRAME_RATE			0x11dd

//ESMMuxBackUpDlg
#define WM_ESM_BACKUP_MUX						0x11e0

//jhhan 180709
#define WM_ESM_REFEREE							0X11f0
#define WM_ESM_NET_REFEREE_DATA_TO_CLIENT		0x11f1
//#define WM_ESM_NET_REFEREE_REQUEST_TO_SERVER	0x11f2
//#define WM_ESM_NET_REFEREE_FINISH_TO_SERVER	0x11f3
#define WM_ESM_REFEREE_INFO						0x11f2
#define WM_ESM_NET_REFEREE_INFO					0x11f3

#define WM_ESM_NET_REFEREE_LIVE					0x11f4
#define WM_ESM_REFEREE_LIVE						0x11f5
#define WM_ESM_REFEREE_SELECT					0x11f6
#define WM_ESM_NET_REFEREE_SELECT				0x11f7
#define WM_ESM_REFEREE_MAKE_SELECT				0x11f8
#define WM_ESM_REFEREE_RECORD					0x11f9


//jhhan 180817
#define WM_ESM_NET_DELETE_FILE					0x1200
#define WM_ESM_DELETE_FILE						0x1201

//wgkim 181001
#define WM_ESM_NET_KZONE_POINTS					0x1202

//jhhan 181126
#define WM_ESM_NET_DETECT_INFO					0x1203
#define WM_ESM_NET_DETECT_RESULT				0x1204
#define WM_ESM_DETECT_CMD						0x1205

//hjcho 181221
#define WM_ESM_NET_MULTIVIEW_FINISH				0x1206
//hjcho 190225
#define WM_ESM_NET_RTSP_STATE_CHECK				0x1207
#define WM_ESM_FRAME_SAVE_STILLIMAGE			0x1208

//wgkim 191217
#define WM_ESM_NET_KZONE_JUDGMENT				0x1209

//--
//-- FOR USING IMAGE PROCESSING
//--
#define WMU_DONEMATCHING						WM_APP+0x0100
#define WMU_GETIMAGE							WM_APP+0x0101
#define WMU_DOMATCHPATTERN						WM_APP+0x0102
#define WMU_GET_MATCHING_OPTION					WM_APP+0x0103
#define WMU_PROGRESS_STEPIT						WM_APP+0x0104
#define WMU_ERR_MESSAGEBOX						WM_APP+0x0105
#define WMU_GET_IMG_PROC_TYPE					WM_APP+0x0106
#define WMU_ADD_DSC_RECORDING					WM_APP+0x0107
#define WMU_UPDATE_STEP_LIST					WM_APP+0x0313		
#define WMU_NC_CLOSE_CONTROL_BAR				WM_APP+0x0314	
#define WMU_DESTROY_TRAINING_DLG				WM_APP+0x0316	
#define WMU_VIEW_ORACLE_IMAGE					WM_APP+0x0319
#define WMU_DESTROY_PATTERN_OPT_DLG				WM_APP+0x0320
#define WMU_NC_SHOW_CONTROL_BAR					WM_APP+0x0321
#define WMU_GETAVICAPTURE						WM_APP+0x0323
#define WMU_GET_IMAGE_SPNESS					WM_APP+0x0324
#define WMU_COPY_STEP_OBJECT					WM_APP+0x0325		
#define WMU_COPY_IMAGE_COLOR_INFO				WM_APP+0x0326
#define WMU_SHOW_IMAGE_RESULT					WM_APP+0x0327
#define WMU_SHOW_IMAGE_FILE_PROP				WM_APP+0x0328
#define WMU_GET_IMAGE_COLOR_AVG					WM_APP+0x0329
#define WMU_UPDATE_RGB_AVG						WM_APP+0x0330
#define WMU_SHOW_AVI_RECORDING_DLG				WM_APP+0x0331
#define WMU_AVI_CAPTURE							WM_APP+0x0332
#define WMU_GET_IMAGE_DISTORTION				WM_APP+0x0333
#define WMU_GRAPHNOTIFY							WM_APP+0x0334

//---------------------------------------------------------------------------
//-- STRING 
//---------------------------------------------------------------------------
#define	ESM_INF_INF						_T("INF")
#define	ESM_INF_LF_CR					_T("\r\n")

#define	ESM_INF_POSTFAIL					_T("POSTFAIL")

#define STR_HOME						_T("$(HOME)")
#define STR_FILESVR						_T("$(FILESVR)")
#define STR_IP							_T("$(IP)")
#define STR_DATE						_T("$(DATE)")
#define STR_TIME						_T("$(TIME)")
#define STR_TIME_SAVE					_T("$(TIME_SAVE)")
#define STR_SNAPSHOT_TIME				_T("$(SNAPSHOT_TIME)")
#define STR_REALTIME					_T("$(REALTIME)")
#define STR_SDFILENAME					_T("$(FILENAME)")
#define STR_INDEX						_T("$(INDEX)")
#define STR_SEARCH_TIME					_T("$(SEARCH_TIME)")
#define STR_SERIAL_LOG					_T("SerialLog")
#define STR_PTP_SERIAL_LOG				_T("PTPSerialLog")
#define STR_STR							_T("STR")
#define STR_KEYMAP						_T("KeyMap")
#define STR_KEYTYPE						_T("KEY_TYPE")
#define STR_OCR							_T("OCR")
#define STR_DEFAULT						_T("Default")
#define STR_PROJECT						_T("Project")
#define STR_CGI_IP						_T("<Device IP>")
#define STR_CGI_HTTP					_T("http://")

//-----------------------------------------------------------
//-- Initialize
//-----------------------------------------------------------
#define REG_ROOT_CONF					_T("Software\\ESMLab\\ESM")
#define REG_ROOT_CONF_4DMAKER			_T("Software\\ESMLab\\4DMaker")
#define REG_ROOT_CONFIG					_T("config")

#define REG_ROOT_PATH					_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\ESM.exe")
#define BASE_ROOT_PATH					_T("C:\\Program Files\\ESMLab\\4DMaker\\")
#define BASE_APP_PATH					_T("C:\\Program Files\\ESMLab\\4DMaker\\bin\\")

#define REG_ROOT_RECENTLY				_T("OpenRecently")
#define REG_ROOT_ADD_RECENTLY			_T("Recently")
#define REG_ROOT_FULL_RECENTLY			_T("Software\\ESMLab\\ESM\\Recently")

//-- 2009-06-21
#define WIA_SERVICE_NAME				_T("stisvc")

#define	DEF_STR_PATH_ROOT				_T("$(ESMDir)")
#define	DEF_STR_PATH_IMAGE				_T("$(ESMImagesDir)")
#define	DEF_STR_PATH_OUTPUT				_T("$(ESMOutputDir)")
#define	DEF_STR_PATH_CONFIG				_T("$(ESMConfigDir)")
#define	DEF_STR_PATH_SCENARIO			_T("$(ESMScenarioDir)")
#define	DEF_STR_PATH_LOG				_T("$(ESMLogDir)")

#define	DEF_STR_PATH_REMOTE				_T("$(ESMRemoteDir)")
#define	DEF_STR_PATH_REMOTE_IMAGE		_T("$(ESMRemoteImageDir)")
#define	DEF_STR_PATH_REMOTE_CONFIG		_T("$(ESMRemoteConfigDir)")
#define	DEF_STR_PATH_REMOTE_SCENARIO	_T("$(ESMRemoteScenarioDir)")
#define	DEF_STR_PATH_REMOTE_SERIAL		_T("$(ESMRemoteSerialDir)")
#define	DEF_STR_PATH_REMOTE_LOG			_T("$(ESMRemoteLogDir)")

#define	DEF_STR_PATH_TIME				_T("$(Time)")
//-- 2009-11-12 hongsu.jung
#define	DEF_STR_MULTILANG				_T("OSD")
#define	PATTERN_IMAGE_COMMAND			_T("IMG_CHECK")
#define	PATTERN_IMAGE_NONE				_T("IMG_NONE")

#define	EXT_BMP							_T(".bmp")
#define	EXT_JPG							_T(".jpg")

//-- 2012-03-09 hongsu@esmlab.com
#define STR_MOVIE						_T("movie")
#define STR_READY						_T("Ready")

//-- 2013-04-22 hongsu.jung
//-- 4DMaker
#define ESM_4DMAKER_CONFIG				_T("4DMaker.info")
#define ESM_4DMAKER_CONNECT				_T("4DMaker.net")
#define ESM_4DMAKER_REG					_T("4DMaker.reg")
#define ESM_4DMAKER_MAC				    _T("4DMaker.mac")
#define ESM_4DMAKER_EX					_T("4DMakerEx.net")
#define ESM_4DMAKER_MOVIE_LIST			_T("4DMakerMovie.ini")
#define ESM_4DMAKER_AUTODETECTING		_T("4DMakerAutoDetecting.ini")
#define ESM_4DMAKER_BACKUP_FOLDER_LIST	_T("4DMakerBackupFolderList.ini")
#define ESM_4DMAKER_DELETE_FOLDER_LIST	_T("4DMakerDeleteFolderList.ini")

//-- OTHERS
#define DEF_STR_ALL				_T("ALL")
#define STR_TOKEN_OPT			_T("||")
#define STR_TOKEN_BASIC			_T("~~")

#define INFO_SECTION_COMM		_T("Communication")
#define INFO_COMM_IP			_T("IP")

#define INFO_SECTION_PATH		_T("Path")
#define INFO_PATH_HOME			_T("Home")
#define INFO_PATH_FILESVR		_T("FileServer")
#define INFO_PATH_CONFIG		_T("ConfigPath")
#define INFO_PATH_LOG			_T("LogPath")
#define INFO_PATH_WORK			_T("WorkPath")
#define INFO_PATH_RECORD		_T("RecordPath")
#define INFO_PATH_OUTPUT		_T("OutputPath")
#define INFO_PATH_SERVERRAM		_T("ServerRamPath")
#define INFO_PATH_CLIENTRAM		_T("ServerClientPath")
#define INFO_PATH_SAVEIMG		_T("SaveImgPath")
//jhhan
#define INFO_PATH_DEFAULT		_T("Default")

#define INFO_ADJUST_STANDARD	_T("Standard")
#define INFO_ADJUST_X			_T("adj_x")
#define INFO_ADJUST_Y			_T("adj_y")
#define INFO_ADJUST_LEFT		_T("adj_left")
#define INFO_ADJUST_RIGHT		_T("adj_right")
#define INFO_ADJUST_TOP			_T("adj_top")
#define INFO_ADJUST_BOTTOM		_T("adj_bottom")

//wgkim
#define INFO_SECTION_BACKUP		_T("Backup")
#define INFO_BACKUP_STRING_SRCFOLDER	_T("SrcFolder")
#define INFO_BACKUP_STRING_DSTFOLDER	_T("DstFolder")
#define INFO_BACKUP_STRING_FTPHOST		_T("FTPHost")
#define INFO_BACKUP_STRING_FTPID		_T("FTPId")
#define INFO_BACKUP_STRING_FTPPW		_T("FTPPassword")


#define INFO_TEMPLATE			_T("Template")
/*#define INFO_TEMPLATE_CTRL1		_T("Template_Ctrl1")
#define INFO_TEMPLATE_CTRL2		_T("Template_Ctrl2")
#define INFO_TEMPLATE_CTRL3		_T("Template_Ctrl3")
#define INFO_TEMPLATE_CTRL4		_T("Template_Ctrl4")
//CMiLRe 20151013 Template Load INI File
#define INFO_TEMPLATE_CTRL5		_T("Template_Ctrl5")
#define INFO_TEMPLATE_CTRL6		_T("Template_Ctrl6")
#define INFO_TEMPLATE_CTRL7		_T("Template_Ctrl7")
#define INFO_TEMPLATE_CTRL8		_T("Template_Ctrl8")
#define INFO_TEMPLATE_CTRL9		_T("Template_Ctrl9")*/

//2016/07/11 Lee SangA Each page(5) of control(9)
#define INFO_TEMPLATE_PAGE1_CTRL1 _T("Template_1_Ctrl1")
#define INFO_TEMPLATE_PAGE1_CTRL2 _T("Template_1_Ctrl2")
#define INFO_TEMPLATE_PAGE1_CTRL3 _T("Template_1_Ctrl3")
#define INFO_TEMPLATE_PAGE1_CTRL4 _T("Template_1_Ctrl4")
#define INFO_TEMPLATE_PAGE1_CTRL5 _T("Template_1_Ctrl5")
#define INFO_TEMPLATE_PAGE1_CTRL6 _T("Template_1_Ctrl6")
#define INFO_TEMPLATE_PAGE1_CTRL7 _T("Template_1_Ctrl7")
#define INFO_TEMPLATE_PAGE1_CTRL8 _T("Template_1_Ctrl8")
#define INFO_TEMPLATE_PAGE1_CTRL9 _T("Template_1_Ctrl9")

#define INFO_TEMPLATE_PAGE2_CTRL1 _T("Template_2_Ctrl1")
#define INFO_TEMPLATE_PAGE2_CTRL2 _T("Template_2_Ctrl2")
#define INFO_TEMPLATE_PAGE2_CTRL3 _T("Template_2_Ctrl3")
#define INFO_TEMPLATE_PAGE2_CTRL4 _T("Template_2_Ctrl4")
#define INFO_TEMPLATE_PAGE2_CTRL5 _T("Template_2_Ctrl5")
#define INFO_TEMPLATE_PAGE2_CTRL6 _T("Template_2_Ctrl6")
#define INFO_TEMPLATE_PAGE2_CTRL7 _T("Template_2_Ctrl7")
#define INFO_TEMPLATE_PAGE2_CTRL8 _T("Template_2_Ctrl8")
#define INFO_TEMPLATE_PAGE2_CTRL9 _T("Template_2_Ctrl9")

//page3
#define INFO_TEMPLATE_PAGE3_CTRL1 _T("Template_3_Ctrl1")
#define INFO_TEMPLATE_PAGE3_CTRL2 _T("Template_3_Ctrl2")
#define INFO_TEMPLATE_PAGE3_CTRL3 _T("Template_3_Ctrl3")
#define INFO_TEMPLATE_PAGE3_CTRL4 _T("Template_3_Ctrl4")
#define INFO_TEMPLATE_PAGE3_CTRL5 _T("Template_3_Ctrl5")
#define INFO_TEMPLATE_PAGE3_CTRL6 _T("Template_3_Ctrl6")
#define INFO_TEMPLATE_PAGE3_CTRL7 _T("Template_3_Ctrl7")
#define INFO_TEMPLATE_PAGE3_CTRL8 _T("Template_3_Ctrl8")
#define INFO_TEMPLATE_PAGE3_CTRL9 _T("Template_3_Ctrl9")

//page4
#define INFO_TEMPLATE_PAGE4_CTRL1 _T("Template_4_Ctrl1")
#define INFO_TEMPLATE_PAGE4_CTRL2 _T("Template_4_Ctrl2")
#define INFO_TEMPLATE_PAGE4_CTRL3 _T("Template_4_Ctrl3")
#define INFO_TEMPLATE_PAGE4_CTRL4 _T("Template_4_Ctrl4")
#define INFO_TEMPLATE_PAGE4_CTRL5 _T("Template_4_Ctrl5")
#define INFO_TEMPLATE_PAGE4_CTRL6 _T("Template_4_Ctrl6")
#define INFO_TEMPLATE_PAGE4_CTRL7 _T("Template_4_Ctrl7")
#define INFO_TEMPLATE_PAGE4_CTRL8 _T("Template_4_Ctrl8")
#define INFO_TEMPLATE_PAGE4_CTRL9 _T("Template_4_Ctrl9")

//page 5
#define INFO_TEMPLATE_PAGE5_CTRL1 _T("Template_5_Ctrl1")
#define INFO_TEMPLATE_PAGE5_CTRL2 _T("Template_5_Ctrl2")
#define INFO_TEMPLATE_PAGE5_CTRL3 _T("Template_5_Ctrl3")
#define INFO_TEMPLATE_PAGE5_CTRL4 _T("Template_5_Ctrl4")
#define INFO_TEMPLATE_PAGE5_CTRL5 _T("Template_5_Ctrl5")
#define INFO_TEMPLATE_PAGE5_CTRL6 _T("Template_5_Ctrl6")
#define INFO_TEMPLATE_PAGE5_CTRL7 _T("Template_5_Ctrl7")
#define INFO_TEMPLATE_PAGE5_CTRL8 _T("Template_5_Ctrl8")
#define INFO_TEMPLATE_PAGE5_CTRL9 _T("Template_5_Ctrl9")

#define INFO_TEMPLATE_PAGE		_T("Template_Page")

//-- [2010-4-27 Lee JungTaek]
//-- Add TCG Path
#define INFO_SECTION_LOG			_T("Log")
#define INFO_LOG_LOGNAME			_T("LogName")
#define INFO_LOG_VERBOSITY			_T("LogVerbosity")
#define INFO_LOG_LIMITDAY			_T("LogLimitday")
#define INFO_LOG_TRACETIME_Enable	_T("TraceTime Enable")

#define INFO_PATH_KEY			_T("KEY")
#define INFO_PATH_REG			_T("RegistryPath")

#define INFO_MOVIE_LIST			_T("MovieList")
#define INFO_MOVIE_LISTNUM		_T("Movie")
//-- [2013-9-24 yongmin]
//-- Add RC Info 
#define INFO_SECTION_RC		 _T("RemoteControl")
#define INFO_SECTION_MACADDR _T("Network")
#define INFO_SECTION_MACADDR_EX _T("NetworkEx")
#define INFO_RC_MODE		 _T("ContolMode")
#define INFO_RC_SERVERMAKE	 _T("ServerMake")
#define INFO_RC_SERVERMODE	 _T("ServerMode")
#define INFO_RC_IP			 _T("IPAddress")
#define INFO_RC_PORT		 _T("Port")
#define INFO_RC_SENDIP		 _T("SendIP")
#define INFO_RC_SYNC_TIME_MARGIN	_T("Sync-Time-Margin")
#define INFO_RC_SENSOR_ON_TIME		_T("SensorOnTime")
#define INFO_RC_WAIT_TIME_DSC		_T("Wait-Time-DSC")
//jhhan
#define INFO_RC_WAIT_TIME_DSC_SUB	_T("Wait-Time-DSC-Sub")
#define INFO_RC_WAIT_TIME_DSC_SEL	_T("Wait-Time-DSC-Sel")
#define INFO_RC_SOURCE		 _T("Source")
#define INFO_RC_DSC			 _T("DSC")
//jhhan 170327
#define INFO_RC_CHECK_VALID_TIME	_T("CheckValidTime")
#define INFO_RC_DIV_FRAME			_T("DivFrame")
//joonho.kim 170628
#define INFO_RC_PC_SYNC_TIME		_T("PCSyncTime")
#define INFO_RC_DSC_SYNC_TIME		_T("DSCSyncTime")
#define INFO_RC_DSC_WAIT_TIME		_T("DSCWaitTime")
#define INFO_RC_TEST_MODE			_T("TestMode")
#define INFO_RC_VIEW_INFO			_T("ViewInfo")
#define INFO_RC_MAKEANDAUTOPLAY		_T("MakeAndAutoPlay")
#define INFO_RC_INSERTLOGO			_T("InsertLogo")
#define INFO_RC_3DLOGO				_T("3DLogo")
#define INFO_RC_LOGOZOOM			_T("LogoZoom")
#define INFO_RC_INSERTCAMERAID		_T("InsertCameraID")
#define INFO_RC_MOVIESAVELOCATION	_T("MovieSaveLocation")
#define INFO_RC_TEMPLATESTARTMARGIN	_T("TemplateStartMargin")
#define INFO_RC_TEMPLATEENDMARGIN	_T("TemplateEndMargin")
#define INFO_RC_TEMPLATESTARTSLEEP	_T("TemplateStartSleep")
#define INFO_RC_TEMPLATEENDSLEEP	_T("TemplateEndSleep")
#define INFO_RC_TIMELINEPREVIEW		_T("TimeLinePreview")
#define INFO_RC_ENABLE_BASEBALL		_T("Enable-BaseBall")
#define INFO_RC_ENABLE_VMCC			_T("Enable-VMCC")
#define INFO_RC_ENABLE_4DPCOPY		_T("Enable-4dpCopy")
#define INFO_RC_UHD_TO_FHD			_T("UHDtoFHD")
#define INFO_RC_DELETE_CNT			_T("DeleteCount")
#define INFO_RC_REPEAT_CNT			_T("RepeatCount")
#define INFO_RC_DSC_SYNC_CNT		_T("DSCSyncCount")
#define INFO_RC_ENABLE_SAVE_IMG		_T("SaveImg")
#define INFO_RC_ENABLE_CHECK_VALID	_T("CheckValid")
#define INFO_RC_REVERSE_MOVIE		_T("ReverseMovie")
#define INFO_RC_BASEBALL_INNING		_T("BaseBallInning")
#define INFO_RC_GPU_MAKE_FILE		_T("GPUMakeFile")
#define INFO_RC_REPEAT_MOVIE		_T("RepeatMovie")

//wgkim
#define INFO_RC_TEMPLATE_POINT		_T("TemplatePoint")
#define INFO_RC_TEMPLATE_STABILIZATION _T("TemplateStabilization")
#define INFO_RC_TEMPLATE_MODIFY		_T("TemplateModify")

//wgkim 170727
#define INFO_RC_LIGHT_WEIGHT		_T("LightWeight")

//hjcho 170830
#define INFO_RC_DIRECT_MUX			_T("DirectMux")
//For prism
#define INFO_RC_INSERT_KZONE		_T("InsertKzone")
//hjcho 160912
#define INFO_RC_SYNCSKIP			_T("SyncSkip")
#define INFO_RC_KZONEVALUE			_T("KzoneValue")
#define INFO_RC_KZONEVALUE1			_T("KzoneValue1")
#define INFO_RC_COLORREVISION		_T("ColorRevision")
#define INFO_RC_PRINTCOLORINFO      _T("PrintColorInfo")

//hjcho 160912
#define INFO_RC_AJALOGO				_T("AJA Logo")
#define INFO_RC_AJAREPLAY			_T("AJA Replay")
//hjcho 170207
#define INFO_RC_AJAONOFF			_T("AJA ONOFF")
//hjcho 170318
#define INFO_RC_AJASAMPLE			_T("AJA SamplingCnt")
//hjcho 170706
#define INFO_RC_GPUSKIP				_T("GPUSkip")
//hjcho 170728
#define INFO_RC_AUTODETECT			_T("AutoDetect")
#define INFO_RC_4DPMETHOD			_T("4DPMethod")
#define INFO_RC_GETRAMDISKSIZE		_T("GETRAMDISKSIZEONOFF")

//jhhan 171019
#define INFO_RC_REMOTE_USE			_T("RemoteUSE")
#define INFO_RC_REMOTE_IP			_T("RemoteIP")
#define INFO_RC_REMOTE_SKIP			_T("RemoteSKIP")

//hjcho 171113
#define INFO_RC_HORIZON_ADJUST		_T("Horizontal")

#define INFO_RC_PROCESSOR_SHARE		_T("Processor Share")

#define INFO_RC_FUNC_KEY			_T("FunctionKey")

//wgkim 171127
#define INFO_RC_ENCODING_FOR_SMARTPHONES	_T("EncodingForSmartPhones")
#define INFO_RC_RECORD_FILE_SPLIT			_T("RecordFileSplit")

//hjcho 171210
#define INFO_RC_FRAMEZOOMRATIO		_T("ViewZoomRatio")

//wgkim 180119
#define INFO_SECTION_RC_TEMPLATE_TARGETOUT_PERCENT	_T("TemplateTargetOutPercent")

//jhhan 180411
#define INFO_RC_FILE_2SEC		_T("File2Sec")

//hjcho 180508
#define INFO_RC_AJANETWORK			_T("AJANetwork")
#define INFO_RC_AJANETWORK_IP		_T("AJANetwork IP")
#define INFO_RC_GIF_MAKING			_T("GIFMaking")
#define INFO_RC_GIF_QUAILITY		_T("GIF Quality")
#define INFO_RC_GIF_SIZE			_T("GIF Size")

#define INFO_RC_SET_FRAMERATE		_T("SetFrameRate")
//hjcho 180718
#define INFO_RC_REFEREEREAD			_T("RefereeRead")

#define INFO_RC_USE_RESYNC			_T("UseReSync")

#define INFO_RC_RESYNC_REC_WAIT_TIME			_T("ResyncRecWaitTime")


//jhhan 180828
#define INFO_RC_AUTO_DELETE			_T("AutoDelete")
#define INFO_RC_STORED_TIME			_T("StoredTime")

//jhhan 180918
#define INFO_RC_TEMPLATE_EDITOR		_T("TemplateEditor")

//wgkim 180921
#define INFO_RC_AUTOADJUST_POSITION_TRACKING _T("AutoAdjustPositionTracking")
#define INFO_RC_AUTOADJUST_KZONE	_T("AutoAdjustKZone")

//hjcho 180927
#define INFO_RC_RTSP				_T("RTSP")
#define INFO_RC_RTSP_WAITTIME		_T("RTSPWaitTime")
//-- Camera Info
//jhhan 181004 VIEW
#define INFO_SECTION_CAM			_T("Camera")
#define INFO_CAM_VIEW				_T("CameraView")

#define INFO_CAM_INFO				_T("CameraInfo")
#define INFO_CAM_COUNT				_T("CameraNum")
#define INFO_CAM_NAME				_T("CameraName")
#define INFO_CAM_IP					_T("CameraIP")

//jhhan 181029
#define INFO_RC_4DAP				_T("4DAP")

//wgkim 181119
#define INFO_RC_SET_TEMPLATE_MODIFY _T("SetTemplateModifyMode")

//hjcho 190103
#define INFO_RC_BITRATE_FOR_ENCODING _T("EncodingBitrate")


//-- WhiteBalance
#define  INFO_RC_INSERT_WHITEBAL    _T("WhiteBalance")

//-- SrmView Option INFO_ADJ_RANGE_X
#define INFO_SECTION_MOVIE			_T("Movie")
#define INFO_SECTION_ADJUST			_T("Adjust")
#define INFO_SECTION_ADJUST_MOVIE	_T("Adjust_Movie")
#define INFO_ADJ_TH					_T("Threshold")
#define INFO_ADJ_FLOW_T				_T("FlowmoTime")
#define INFO_ADJ_ORDER_RTL			_T("R-to-L")
#define INFO_ADJ_ORDER_LTR			_T("L-to-R")
#define INFO_ADJ_AUTOBRIGHTNESS		_T("AUTO_BRIGHTNESS")

#define INFO_ADJ_TARGETPATH			_T("ADJ_TARGET_PATH");

#define INFO_ADJ_FIRSTPOINTY		_T("ADJ_FIRSTPOINTY")
#define INFO_ADJ_POINTX				_T("ADJ_POINTX")
#define INFO_ADJ_XRANGE				_T("ADJ_XRANGE")
#define INFO_ADJ_FIRSTYRANGE		_T("ADJ_FIRSTYRANGE")
#define INFO_ADJ_SECONDYRANGE		_T("ADJ_SECONDYRANGE")
#define INFO_ADJ_THIRDYRANGE		_T("ADJ_THIRDYRANGE")
#define INFO_ADJ_TARGETLENGTH		_T("ADJ_TARGETLENGTH")
#define INFO_ADJ_CAMERAZOOM			_T("ADJ_CAMERAZOOM")

#define INFO_ADJ_POINTGAPMIN		_T("ADJ_POINTGAPMIN")
#define INFO_ADJ_MIN_WIDTH			_T("ADJ_MIN_WIDTH")
#define INFO_ADJ_MIN_HEIGHT			_T("ADJ_MIN_HEIGHT")
#define INFO_ADJ_MAX_WIDTH			_T("ADJ_MAX_WIDTH")
#define INFO_ADJ_MAX_HEIGHT			_T("ADJ_MAX_HEIGHT")
#define INFO_ADJ_TARGETLENTH		_T("ADJ_TARGETLENTH")

#define INFO_SECTION_SER_PORT		_T("PORT")
#define ALPHABET_A					65

#define INFO_SER_BAUDRATE			_T("Baud rate")		//Baudrate
#define INFO_SER_DATABITS			_T("Data bits")
#define INFO_SER_STOPBITS			_T("Stop bits")
#define INFO_SER_HANDSHAKING		_T("HandShaking")
#define INFO_SER_PARITYSCHEME		_T("Parity scheme")
#define INFO_SER_PORT				_T("Port")
#define INFO_SER_PORT_CONNECT		_T("Auto Connect")
#define INFO_SECTION_LOGFILTER      _T("LogFilter")
#define INFO_LF_INCHECK1            _T("Include1 Enable")
#define INFO_LF_INCHECK2            _T("Include2 Enable")
#define INFO_LF_INCHECK3            _T("Include3 Enable")
#define INFO_LF_INCHECK4            _T("Include4 Enable")
#define INFO_LF_EXCHECK1            _T("Exclude1 Enable")
#define INFO_LF_EXCHECK2            _T("Exclude2 Enable")
#define INFO_LF_EXCHECK3            _T("Exclude3 Enable")
#define INFO_LF_EXCHECK4            _T("Exclude4 Enable")
#define INFO_LF_INPUTCHECK1         _T("InPutMsg1 Enable")
#define INFO_LF_INPUTCHECK2         _T("InPutMsg2 Enable")
#define INFO_LF_INPUTCHECK3         _T("InPutMsg3 Enable")
#define INFO_LF_INPUTCHECK4         _T("InPutMsg4 Enable")
#define INFO_LF_INCLUDE1            _T("Include 1")
#define INFO_LF_INCLUDE2            _T("Include 2")
#define INFO_LF_INCLUDE3            _T("Include 3")
#define INFO_LF_INCLUDE4            _T("Include 4")
#define INFO_LF_EXCLUDE1            _T("Exclude 1")
#define INFO_LF_EXCLUDE2            _T("Exclude 2")
#define INFO_LF_EXCLUDE3            _T("Exclude 3")
#define INFO_LF_EXCLUDE4            _T("Exclude 4")
#define INFO_LF_INPUTMSG1           _T("Put Message 1")
#define INFO_LF_INPUTMSG2           _T("Put Message 2")
#define INFO_LF_INPUTMSG3           _T("Put Message 3")
#define INFO_LF_INPUTMSG4           _T("Put Message 4")

#define INFO_SECTION_IMG_RGB		_T("RGB Analysis")
#define INFO_IMGCMP_RPIXELVALUE		_T("RPixelValue")
#define INFO_IMGCMP_GPIXELVALUE		_T("GPixelValue")
#define INFO_IMGCMP_BPIXELVALUE		_T("BPixelValue")
#define INFO_IMGCMP_FAILCOUNT		_T("FailCount")

#define INFO_SECTION_IMG_PATTERN	_T("Pattern Matching")
#define INFO_IMGCMP_TOLERABLESCORE	_T("TolerableScore")

#define INFO_SECTION_TEST			_T("Test")
#define INFO_TEST_MODE				_T("Mode")
#define INFO_TEST_INTERVAL_TC		_T("IntervalTC")
#define INFO_GRAPH_LINE_CNT			_T("ShowGraphLineCount")
#define INFO_TEST_POSTFAIL			_T("PostFail")
#define INFO_TEST_POWER_SAVE_OFF	_T("PowerSaveModeOff")
#define INFO_TEST_REMOVE_FILE		_T("RemoveFileAfterReceive")
#define INFO_TEST_REMOVE_FILES		_T("RemoveFileInFileServer")
#define INFO_TEST_CHECK_SRM			_T("SRM Use")


#define INFO_SECTION_TOLERANCE			_T("Tolerance")
#define INFO_TOLERANCE_OCR_TOLERANCE	_T("OCRTolerance")
#define INFO_TOLERANCE_FPS				_T("Framerate")
#define INFO_TOLERANCE_GOP				_T("GOP")
#define INFO_TOLERANCE_FPSGAP			_T("FPS_GAP")
#define INFO_TOLERANCE_FPSOPT			_T("FPS_OPT")
#define INFO_TOLERANCE_PCGAP			_T("PC_GAP")
#define INFO_TOLERANCE_PCOPT			_T("PC_OPT")

//-- 2012-08-20 joonho.kim
#define INFO_TOLERANCE_IMG_RED		_T("ImgToleranceRed")
#define INFO_TOLERANCE_IMG_GREEN	_T("ImgToleranceGreen")
#define INFO_TOLERANCE_IMG_BLUE		_T("ImgToleranceBlue")
#define INFO_TOLERANCE_LT_COLOR		_T("LatencyToleranceColor")

#define INFO_TOLERANCE_PAN			_T("PanTolerance")
#define INFO_TOLERANCE_TILT			_T("TiltTolerance")
#define INFO_TOLERANCE_ZOOM			_T("ZoomTolerance")
#define INFO_TOLERANCE_FOCUS		_T("FocusTolerance")

//-- 2010-3-4 hongsu.jung
//-- FOR USING ODBC
#define INFO_SECTION_TMS_ODBC			_T("ESM ODBC")
#define INFO_ODBC_TMS_DATASOURCE		_T("ESM Data Source")
#define INFO_ODBC_TMS_INITIALCATALOG	_T("ESM Initial Catalog")
#define INFO_ODBC_TMS_PASSWORD			_T("ESM Password")
#define INFO_ODBC_TMS_PROVIDER			_T("ESM Provider")
#define INFO_ODBC_TMS_USER				_T("ESM User")
//-- Add Check Box
#define INFO_ODBC_TMS_CHECK				_T("TMS ODBC Set")
#define INFO_REPORT_OPT					_T("TMS Report Option")

//-- 2010-3-18 hongsu.jung
//-- FOR FW UPDATE
#define INFO_SECTION_FWUPDATE		_T("FW Update")
#define INFO_FWUPDATE_PATH			_T("Path")
#define INFO_FWUPDATE_NAME			_T("Name")
#define INFO_FWUPDATE_CMD			_T("Command")


#define INFO_SECTION_PTP			_T("PTP")
#define INFO_PTP_USE_CHECK			_T("UsePTPCheck")
#define INFO_PTP_CHECK				_T("GuaranteeConnection")
#define INFO_PTP_CHECK_TIME			_T("CheckTime")
#define INFO_PTP_DISABLE_WIA		_T("DiableWIA")
#define INFO_PTP_RETRY_CONNECTION	_T("RetryConnection")

#define INFO_SECTION_FOCUS			_T("Focus")

//---------------------------------------------------------------------------
//-- INI READ ITEMS
//--------------------------------------------------------------------------

#define	INI_TESTPROPERTY_PROPERTY	_T("Property")
#define	INI_TESTPROPERTY_REVISION	_T("Revision")
#define	INI_TESTPROPERTY_TITLE		_T("Title")
#define	INI_TESTPROPERTY_VER		_T("Version")
#define	INI_TESTPROPERTY_CREATOR	_T("Creator")
#define	INI_TESTPROPERTY_MODIFIER	_T("Modifier")
#define	INI_TESTPROPERTY_INI_COND	_T("InitialCondition")
#define	INI_TESTPROPERTY_INPUTDATE	_T("InputData")
#define	INI_TESTPROPERTY_PRIORITY	_T("Priority")
#define	INI_TESTPROPERTY_DESC		_T("Description")
//-- 2008-06-21
#define	INI_TESTPROPERTY_CREATEDATE	_T("Create Date")
#define	INI_TESTPROPERTY_MODIDATE	_T("Modification Date")

#define	INI_TESTFILE_SUB_FILE		_T("File")
#define	INI_TESTFILE_SUB_REVTITLE	_T("RevTitle")
#define	INI_TESTFILE_SUB_EXEOPTION	_T("ExecuteOption")
#define	INI_TESTFILE_SUB_COUNT		_T("ExecuteCount")
#define	INI_TESTFile_SUB_PROGRESS	_T("FailProgress")

#define	INI_TESTFILE_SUB_ITEM		_T("Item")
#define	INI_TESTFILE_SUB_BP			_T("BreakPoint")
#define	INI_TESTFILE_SUB_INTERVAL	_T("Interval")
#define	INI_TESTFILE_SUB_COMMENT	_T("Comment")

//-- 2012-1-3 Jeongyong.kim
//-- OSD Step
#define STR_OSD_CHECK				_T("CHK_STR")

//-- 2015-02-15 cygil@esmlab.com
//-- Add picture coutdown
#define INFO_RC_COUNTDOWNLASTIP		_T("CountdownLastIP")
#define COUNTDOWNFILE				_T("countdown.wav")


//-- Ceremony

#define INFO_CEREMONY				_T("Ceremony")
#define INFO_CEREMONY_USE			_T("CeremonyUse")
#define INFO_CEREMONY_PUTIMAGE		_T("CeremonyPutImage")
#define INFO_CEREMONY_RATIO			_T("CeremonyRatio")
#define INFO_CEREMONY_DBADDRIP		_T("CeremonyAddrIP")
#define INFO_CEREMONY_DBID			_T("CeremonyDBID")
#define INFO_CEREMONY_DBPW			_T("CeremonyDBPW")
#define INFO_CEREMONY_DBNAME		_T("CeremonyDBName")
#define INFO_CEREMONY_DBIMAGEPATH	_T("CeremonyImagePath")
#define INFO_CEREMONY_DBMOVIEPATH	_T("CeremonyMoviePath")
#define INFO_CEREMONY_WIDTH			_T("CeremonyWidth")
#define INFO_CEREMONY_HEIGHT		_T("CeremonyHeight")
#define INFO_CEREMONY_STILLIMG		_T("StillImage")
#define INFO_CEREMONY_CANON_SELPHY	_T("CeremonyUseCanonSelphy")


//-- Management
#define INFO_MANAGEMENT							_T("Management")
#define INFO_MANAGEMENT_SELFILEPATH				_T("SelectFilePath")
#define INFO_MANAGEMENT_SAVEFILEPATH			_T("SaveFilePath")

//--AutoDetect 16-08-25
#define INFO_AUTODETECT							_T("AutoDetect")
#define INFO_AUTODETECT_CAM_LEFT				_T("LeftCam")
#define INFO_AUTODETECT_CAM_RIGHT				_T("RightCam")
#define INFO_AUTODETECT_REVERSE					_T("Reverse")
#define INFO_AUTODETECT_RESOLUTION_ROW			_T("Resolution_row")
#define INFO_AUTODETECT_RESOLUTION_COL			_T("Resolution_col")
#define INFO_AUTODETECT_CIRCLE_SIZE				_T("Circle_size")
#define INFO_AUTODETECT_LEFT_START_X			_T("(Left)start_x")
#define INFO_AUTODETECT_LEFT_START_Y			_T("(Left)start_y")
#define INFO_AUTODETECT_LEFT_END_X				_T("(Left)end_x")
#define INFO_AUTODETECT_LEFT_END_Y				_T("(Left)end_y")
#define INFO_AUTODETECT_LEFT_MOV_X				_T("(Left)mov_x")
#define INFO_AUTODETECT_LEFT_MOV_Y				_T("(Left)mov_y")
#define INFO_AUTODETECT_RIGHT_START_X			_T("(Right)start_x")
#define INFO_AUTODETECT_RIGHT_START_Y			_T("(Right)start_y")
#define INFO_AUTODETECT_RIGHT_END_X				_T("(Right)end_x")
#define INFO_AUTODETECT_RIGHT_END_Y				_T("(Right)end_y")
#define INFO_AUTODETECT_RIGHT_MOV_X				_T("(Right)mov_x")
#define INFO_AUTODETECT_RIGHT_MOV_Y				_T("(Right)mov_y")







//---------------------------------------------------------------------------
//-- TIME
//--------------------------------------------------------------------------
#define TC_OTHER_DEFAULT_INTERVAL	0

//---------------------------------------------------------------------------
//-- ERROR CODE
//--------------------------------------------------------------------------
//-- Condition Fail (Warning)
#define ESM_ERR_MSG				0xb0	//-- Wrong Formal Message (Message Queue)
#define ESM_ERR_NY				0xFF	//-- Not Available Error
#define ESM_ERR					0x00	//-- Something Error
#define ESM_ERR_NON				0x01	//-- TG_OK (PASS)
#define ESM_ERR_STOP			0x02	//-- Stop Testing
#define ESM_ERR_IMAGE			0x03	//-- ImageVerification Error
#define ESM_ERR_PTP				0x04	//-- ImageVerification Error
#define ESM_ERR_ANAL			0xa0	//-- Analysis Error
#define ESM_ERR_TIME			0xa1	//-- Time Get/Set Error
#define ESM_ERR_SER				0xa2	//-- Serial Error
#define ESM_ERR_DST				0xa3	//-- DST Error
#define ESM_ERR_OSD				0xa4	//-- OSD Error
#define ESM_ERR_OCR				0xa5	//-- OCR Error
#define ESM_ERR_PTZ				0xa6	//-- PTZ
#define ESM_ERR_POWER			0xa7	//-- POWER
#define ESM_ERR_RGB				0xa8	//-- RGB Image Analysis
#define ESM_ERR_IMG_VER_COLOR	0xab	//-- Image Verification
#define ESM_ERR_IMG_VER_POS		0xac	//-- Image Verification

#define esmround(x) ((x)>=0?(int)((x)+0.5):(int)((x)-0.5))   // Siseong Ahn

enum ESM_PATH
{
	ESM_PATH_HOME		= 0,
	ESM_PATH_CONFIG		,
	ESM_PATH_FILESERVER	,
	ESM_PATH_LOG		,
	ESM_PATH_IMAGE		,
	ESM_PATH_LOG_DATE	,
	ESM_PATH_RESULT		,
	ESM_PATH_OUTPUT		,	
	ESM_PATH_MOVIE		,
	ESM_PATH_MOVIE_FILE ,
	ESM_PATH_MOVIE_CONF ,
	ESM_PATH_PICTURE	,
	ESM_PATH_PICTURE_FILE,
	ESM_PATH_PICTURE_CONF,
	ESM_PATH_OBJECT		,
	ESM_PATH_ADJUST		,
	ESM_PATH_SETUP		,
	ESM_PATH_MOVIE_EFFECT,
	ESM_PATH_PICTURE_EFFECT,
	ESM_PATH_MOVIE_TIMELINE,
	ESM_PATH_PICTURE_TIMELINE,
	ESM_PATH_MOVIE_TEMPLATE,
	ESM_PATH_PICTURE_TEMPLATE,
	ESM_PATH_LOCALMOVIE ,
	ESM_PATH_MOVIE_WEB,
	ESM_PATH_ALL		,	
};

enum ESM_VALUE
{
	ESM_VALUE_NULL			 = 0,
	ESM_VALUE_ADJ_TH			,
	ESM_VALUE_ADJ_FLOW_T		,
	ESM_VALUE_ADJ_ORDER_RTL		,
	ESM_VALUE_ADJ_ORDER_LTR		,
	ESM_VALUE_MAKE_SERVER		,
	ESM_VALUE_NET_MODE			,
	ESM_VALUE_SERVERMODE		,
	ESM_VALUE_NET_PORT			,
	ESM_VALUE_VIEW_INFO			,
	ESM_VALUE_TEST_MODE			,
	ESM_VALUE_MAKEAUTOPLAY		,
	ESM_VALUE_INSERTLOGO		,
	ESM_VALUE_3DLOGO			,
	ESM_VALUE_LOGOZOOM			,
	ESM_VALUE_MOVIESAVELOCATION	,
	ESM_VALUE_WAIT_SAVE_DSC		,

	ESM_VALUE_WAIT_SAVE_DSC_SUB	,	//jhhan
	ESM_VALUE_WAIT_SAVE_DSC_SEL	,	//jhhan
	
	ESM_VALUE_SYNC_TIME_MARGIN	,
	ESM_VALUE_SENSORONTIME		,
	ESM_VALUE_TEMPLATE_STARTTIME,
	ESM_VALUE_TEMPLATE_ENDTIME	,
	ESM_VALUE_TEMPLATE_STARTSLEEP,
	ESM_VALUE_TEMPLATE_ENDSLEEP	,
	ESM_VALUE_ADJ_VIEW_LEFT		,
	ESM_VALUE_ADJ_VIEW_TOP		,
	ESM_VALUE_ADJ_VIEW_RIGHT	,
	ESM_VALUE_ADJ_VIEW_BOTTOM	,
	ESM_VALUE_ADJ_VIEW_MA		,
	ESM_VALUE_ADJ_CAMERAZOOM	,
	ESM_VALUE_ADJ_AUTOBRIGHTNESS,
	ESM_VALUE_TIMELINEPREVIEW	,
	ESM_VALUE_COUNTDOWNLASTIP	,
	ESM_VALUE_ADJ_POINTGAPMIN	,
	ESM_VALUE_ADJ_MIN_WIDTH		,
	ESM_VALUE_ADJ_MIN_HEIGHT	,
	ESM_VALUE_ADJ_MAX_WIDTH		,
	ESM_VALUE_ADJ_MAX_HEIGHT	,
	ESM_VALUE_ADJ_TARGETLENTH	, 
	ESM_VALUE_ENABLE_BASEBALL	,
	ESM_VALUE_REVERSE_MOVIE		,
	ESM_VALUE_GPU_MAKE_FILE		,
	ESM_VALUE_REPEAT_MOVIE		,
	ESM_VALUE_ENABLE_VMCC		,
	ESM_VALUE_ENABLE_4DPCOPY	,
	ESM_VALUE_BASEBALL_INNING	,
	ESM_VALUE_UHD_TO_FHD		,
	ESM_VALUE_DELETE_COUNT		,
	ESM_VALUE_MOVIE_REPEAT_COUNT,
	ESM_VALUE_DSC_SYNC_COUNT	,
	ESM_VALUE_KEY_TYPE			,
	ESM_VALUE_CEREMONYUSE		,
	ESM_VALUE_CEREMONYPUTIMAGE	,
	ESM_VALUE_CEREMONYRATIO		,
	ESM_VALUE_CEREMONYDBADDRIP	, 
	ESM_VALUE_CEREMONYDBID		, 
	ESM_VALUE_CEREMONYDBPW		, 
	ESM_VALUE_CEREMONYDBNAME	, 
	ESM_VALUE_CEREMONYIMAGEPATH	, 
	ESM_VALUE_CEREMONYMOVIEPATH	, 
	ESM_VALUE_CEREMONY_WIDTH	, 
	ESM_VALUE_CEREMONY_HEIGHT	, 
	ESM_VALUE_SAVE_IMAGE		,
	ESM_VALUE_INSERT_CAMERAID	,
	ESM_VALUE_LOADFILE_DIRECT	,
	ESM_VALUE_CHECK_VALID,
	ESM_VALUE_INSERTPRISM,
	ESM_VALUE_SYNCSKIP,
	ESM_VALUE_PRISMVALUE,
	ESM_VALUE_PRISMVALUE2,
	ESM_VALUE_COLORREVISION,
	ESM_VALUE_AUTO_WHITEBALANCE,
	ESM_VALUE_TEMPLATEPOINT,
	ESM_VALUE_TEMPLATE_STABILIZATION,
	ESM_VALUE_TEMPLATE_MODIFY,
	ESM_VALUE_AJALOGO,
	ESM_VALUE_AJAREPLAY,
	ESM_VALUE_AJAONOFF,
	ESM_VALUE_AJASAMPLECNT,
	ESM_VALUE_GETRAMDISKSIZE,
	//jhhan 170327
	ESM_VALUE_CHECK_VALID_TIME,
	ESM_VALUE_PRINT_COLORINFO,
	//jhhan 170613
	ESM_VALUE_DIV_FRAME,
	ESM_VALUE_PC_SYNC_TIME,
	ESM_VALUE_DSC_SYNC_TIME,
	ESM_VALUE_DSC_WAIT_TIME,
	ESM_VALUE_GPUMAKESKIP,
	//wgkim 170727
	ESM_VALUE_LIGHTWEIGHT,
	ESM_VALUE_AUTODETECT,
	//hjcho 170830
	ESM_VALUE_DIRECTMUX,
	ESM_VALUE_4DPMETHOD,
	ESM_VALUE_HORIZON,
	//wgkim 171127
	ESM_VALUE_ENCODING_FOR_SMARTPHONES,
	//hjcho 171210
	ESM_VALUE_VIEW_RATIO,
	//wgkim 180119
	ESM_VALUE_TEMPLATE_TARGETOUT_PERCENT,
	ESM_VALUE_RECORD_FILE_SPLIT,
	//hjcho 180423
	ESM_VALUE_CEREMONY_STILLIMAGE,
	//hjcho 180508
	ESM_VALUE_AJANETWORK,
	ESM_VALUE_AJANETWORK_IP,
	//hjcho 180629
	ESM_VALUE_GIF_MAKING,
	ESM_VALUE_GIF_QUALITY,
	ESM_VALUE_GIF_SIZE,
	//wgkim 180629
	ESM_VALUE_FRAMERATE,
	ESM_VALUE_REFEREEREAD,//180718 hjcho
	ESM_VALUE_USE_RESYNC,
	ESM_VALUE_RESYNC_WAIT_TIME,
	ESM_VALUE_AUTO_DELETE,//jhhan 180828
	ESM_VALUE_STORED_TIME,//jhhan 180828
	ESM_VALUE_TEMPLATE_EDITOR,//jhhan 180918
	ESM_VALUE_AUTOADJUST_POSITION_TRACKING,//wgkim 180921
	ESM_VALUE_AUTOADJUST_KZONE,
	ESM_VALUE_RTSP,//180927 hjcho
	ESM_VALUE_CEREMONY_USE_CANON_SELPHY, //  [2018/10/2/ stim]
	ESM_VALUE_4DAP,
	ESM_VALUE_TEMPLATE_MODIFY_MODE, //wgkim 181119
	ESM_VALUE_BITRATE_FOR_SMARTPHONE,
	ESM_VALUE_RTSP_WAITTIME,
};
//-- 2016.08.02
enum ESM_MANAGEMENT
{
	ESM_MANAGEMENT_SEL_PATH,
	ESM_MANAGEMENT_SAVE_PATH,
};
enum DSC_ADJ
{
	DSC_ADJ_X	= 0,
	DSC_ADJ_Y	,
	DSC_ADJ_A	,	
	DSC_ADJ_RX	,
	DSC_ADJ_RY	,
	DSC_ADJ_SCALE,
	DSC_ADJ_DISTANCE,
	//DSC_ADJ_REVERSE,
	DSC_ADJ_CNT	, 
};

enum ESM_ADJ_KIND
{
	ESM_ADJ_KIND_NULL		= 0,
	ESM_ADJ_KIND_POSITION	,
	ESM_ADJ_KIND_CREATE3D	,

};


struct ESM3DInfo
{
	CString strDSC[2];			// 0 : Left | 1: Right
	float nAdj[2][DSC_ADJ_CNT];	// 0 : Left | 1: Right
};

enum ESM_FILM_STATE
{
	ESM_ADJ_FILM_STATE_MOVIE		= 0,
	ESM_ADJ_FILM_STATE_PICTURE		,
};

enum ESM_MOVIE_PAUSEMODE_STEP
{
	ESM_NOMAL_START		, 
	ESM_PAUSESTEP_PAUSE	, 
	ESM_PAUSESTEP_PAUSEREADY	, 
	ESM_PAUSESTEP_RESUME, 
	ESM_RECORD_STOP	,
};

enum ESM_MOVIE_ERROR_MSG
{
	ESM_ERR_MSG_FFMPEG_INIT,
};

//20151015 Template VMCC Save/Load
#define TEMPLATE_ENTRY_FRAME_COUNT _T("frame_count")
#define TEMPLATE_ENTRY_KZONE _T("KzoneIndex")
#define TEMPLATE_ENTRY_STARTTIME _T("starttime")
#define TEMPLATE_ENTRY_START_DSC _T("start_dsc")
#define TEMPLATE_ENTRY_ENDTIME _T("endtime")
#define TEMPLATE_ENTRY_END_DSC _T("end_dsc")
#define TEMPLATE_ENTRY_TIMEFRAME _T("timeframe")
#define TEMPLATE_ENTRY_STEP_FRAME _T("stepframe")
#define TEMPLATE_ENTRY_VMCC_COUNT _T("VMCC_COUNT")
#define TEMPLATE_ENTRY_VMCC _T("VMCC")

#define TEMPLATE_COMMON_SECTION _T("COMMON")

#define TEMPLATE_VIEW_SECTION	_T("VIEW")
#define TEMPLATE_VIEW_ENTRY		_T("VIEW")
#define TEMPLATE_VIEW_COUNT		_T("VIEW_COUNT")
#define TEMPLATE_VIEW_TIME		_T("VIEW_TIME")

struct TEMPLATE_VMCC_STRUCT
{
	double	dRatioTime;
	double	dRatioX;
	double	dRatioY;
	int nZoom;
	float dWeight;
};

struct TEMPLATE_VIEW_STRUCT
{
	TEMPLATE_VIEW_STRUCT()
	{
		nCameraIdx = 1;
		nZoom = 100;
		bSend = FALSE;
	}

	int nCameraIdx;
	int nZoom;
	int nX;
	int nY;
	BOOL bSend;
};

struct TEMPLATE_STRUCT
{
	int KzoneIndex;
	int	nStartTime;
	int	nStrartDSC;
	int	nEndTime;
	int	nEndDSC;
	int	nFrameRate;
	int	nVMCC_Count;
	int nFrameCount;
	int nStepFrame;
	vector<TEMPLATE_VMCC_STRUCT> nVMCC_Data;
};

struct VIEW_STRUCT
{
	VIEW_STRUCT()
	{
		nCount = 0;
		nTime = 0;
		nSelectTime = 0;
	}
	int nCount;
	int nTime;
	int nSelectTime;
	vector<TEMPLATE_VIEW_STRUCT> nArrData;
};

enum ESM_MOVIE_SIZE
{
	ESM_MOVIESIZE_FHD	, 
	ESM_MOVIESIZE_UHD	, 
};
struct AxisInfo //170529 hjcho
{
	AxisInfo()
	{
		strCamID = _T("");
		nStartX = 0;
		nStartY = 0;
		nEndX   = 0;
		nEndY   = 0;
		nMovX   = 0;
		nMovY   = 0;
		nScaleX = 8;
		nScaleY = 6;
	}
	CString strCamID;
	int nStartX;
	int nStartY;
	int nEndX;
	int nEndY;
	int nMovX;
	int nMovY;
	int nScaleX;
	int nScaleY;
};
struct track_info //2016-06-17 hjcho
{
	track_info()
	{
		bReverse = FALSE;
		nWidth  = 3840;
		nHeight = 2160;
		bStart = FALSE;
		nTime = -1;
		nCircleSize = -1;
		bShowTracking = FALSE;
	}
	//RIGHT - LEFT - RIGHTH - LEFTH
	AxisInfo st_AxisInfo[4];


/*
	AxisInfo st_Right;
	AxisInfo st_LeftH;
	AxisInfo st_RightH;*/
	BOOL bStart;
	int nTime;
	BOOL bReverse;
	int nWidth;
	int nHeight;
	int nCircleSize;
	BOOL bShowTracking;
};

struct esm_msg_info
{
	int verbosity;
	LPCTSTR lpszFormat;
};

enum ESM_NET_STATUS
{
	ESM_NET_START = 1,
	ESM_NET_FINISH = 9,
	ESM_NET_RESUME = 11,
	ESM_NET_PAUSE = 19,
	ESM_NET_ERROR = 300
};

enum ESM_SETUP
{
	ESM_4DMAKER	= 0,

	ESM_SERVER_IP,
	ESM_SERVER_PORT,
	ESM_SERVER_URL,

	ESM_CLIENT_IP,
	ESM_CLIENT_PORT,
	ESM_CLIENT_URL,
	
	ESM_CLIENT_UPLUS_01,
	ESM_CLIENT_UPLUS_02,

	ESM_SETUP_COUNT
};

enum ESM_NETWORK
{
	ESM_NETWORK_4DA = 0,
	ESM_NETWORK_4DP
};


//joonho.kim 17-02-17
struct making_info
{
	making_info()
	{
		memset(strFirstDSC, 0, sizeof(strFirstDSC));
		memset(strEndDSC, 0, sizeof(strEndDSC));
		memset(nProcessorIP, 0, sizeof(nProcessorIP));
		nProcessorCnt = 0;
	}
	char strFirstDSC[MAX_PATH];
	char strEndDSC[MAX_PATH];
	int nProcessorIP[MAX_4DP];
	int nProcessorCnt;
};

enum ESM_PROPERTY_1SETP
{
	ESM_PROPERTY_1SETP_DEFAULT = 0	,
	ESM_PROPERTY_1SETP_ISO_UP		,
	ESM_PROPERTY_1SETP_ISO_DOWN		,
	ESM_PROPERTY_1SETP_SS_UP		,
	ESM_PROPERTY_1SETP_SS_DOWN		,
	ESM_PROPERTY_1SETP_CT_UP		,
	ESM_PROPERTY_1SETP_CT_DOWN 
};
//hjcho
enum{
	THREAD_PATHMGR,
	THREAD_SELIMAGE,
	THREAD_TOTAL,
};
enum ESM_REFEREEMODE
{
	ENUM_REFEREE_LIVE = 0,
	ENUM_REFEREE_MUX,
	ENUM_REFEREE_ORIGINAL,
	ENUM_REFEREE_TOTAL
};
//hjcho 190226
enum RTSP_STAT_MESSAGE
{
	RTSP_MESSAGE_STATE_RECORD_IN = 0,
	RTSP_MESSAGE_STATE_DECODE_IN,
	RTSP_MESSAGE_STATE_PROCESS_IN,
	RTSP_MESSAGE_STATE_REMAIN,
	RTSP_MESSAGE_STATE_PLAY_IN,
	RTSP_MESSAGE_STATE_FRAME_COUNT,
};
enum RTSP_RESTFul_COMMAND
{
	RTSP_COMMAND_RECORD_START = 0,
	RTSP_COMMAND_RECORD_PAUSE,
	RTSP_COMMAND_RECORD_RESUME,
	RTSP_COMMAND_RECORD_END,
	RTSP_COMMAND_MAX
};