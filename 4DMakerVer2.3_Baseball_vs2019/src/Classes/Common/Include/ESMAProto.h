////////////////////////////////////////////////////////////////////////////////
//
//	TGAProto.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "TGADefines.h"

/////////////////////////////////////////////////
// TG protol - commmand code 

//------------------------------------------------------------------------------

// REQUEST PROTOCOL 
#define TGAPROTO_REQ_TEST                           0x0001
#define TGAPROTO_REQ_AGENT_START                    0x0010
#define TGAPROTO_REQ_AGENT_STOP                     0x0011
#define TGAPROTO_REQ_CONNECTIONSTATE                0x0101
#define TGAPROTO_REQ_SETFILE                        0x0102
#define TGAPROTO_REQ_GETFILE                        0x0103
#define TGAPROTO_REQ_EXECCMD                        0x0104
#define TGAPROTO_REQ_CAPTURE                        0x0105
#define TGAPROTO_REQ_OSDIMG                         0x0106
#define TGAPROTO_REQ_GUISTRINGRES                   0x0107
#define TGAPROTO_REQ_SETCFG                         0x0108
#define TGAPROTO_REQ_MULTILANG_TEST                 0x0109
#define TGAPROTO_REQ_GET_CURRENT_LANG				0x010A


#define TGAPROTO_REQ_CFGINFO                        0x0201
#define TGAPROTO_REQ_TARGETINFO                     0x0208
#define TGAPROTO_REQ_STACKINFO                      0x020B
#define TGAPROTO_REQ_HEAPINFO                       0x020C
#define TGAPROTO_REQ_TASKINFO                       0x020D
#define TGAPROTO_REQ_BUFFERINFO                     0x020E
#define TGAPROTO_REQ_TASKMQINFO                     0x020F
// [2/9/2011 JanuS] Memory Analyzer view
#define TGAPROTO_REQ_MEMORYDUMP                     0x0210

#define TGAPROTO_REQ_STACKUSAGE                     0x0300
#define TGAPROTO_REQ_MDQ                            0x0350

#define TGAPROTO_REQ_DEVICEPROPERTY                 0x0400

#define TGAPROTO_REQ_GETDIRINFO                     0x0501
#define TGAPROTO_REQ_DELETEFILE                     0x0510

// EVENT PROTOCOL

#define TGA_PROTO_EVT_AGENT_STOPPED                 0xD000
#define TGA_PROTO_EVT_WRITE_FILE_DONE               0xA010
#define TGA_PROTO_EVT_CAPTURE                       0xA020
#define TGA_PROTO_EVT_BURST_CAPTURE                 0xA030

//------------------------------------------------------------------------------

/////////////////////////////////////////////////
// TGAPROTO_REQ_EXECCMD PARAM 2
#define TGAPROTO_REQ_EXECCMD_KEY                    0x00
#define TGAPROTO_REQ_EXECCMD_ADJ                    0x01
#define TGAPROTO_REQ_EXECCMD_DBG                    0x02

/////////////////////////////////////////////////
// TGAPROTO_REQ_GETFILE PARAM 2 
#define TGAPROTO_REQ_GETFILE_INFO                   0x01
#define TGAPROTO_REQ_GETFILE_GET                    0x02

/////////////////////////////////////////////////
// TGAPROTO_REQ_DEVICEPROPERTY PARAM 2
#define TGAPROTO_REQ_DEVICEPROPERTY_CONNECTION_ID   0x01

/////////////////////////////////////////////////
// TGAPROTO_REQ_DEVICEPROPERTY PARAM 3
#define TGAPROTO_REQ_DEVICEPROPERTY_SET             0x01
#define TGAPROTO_REQ_DEVICEPROPERTY_GET             0x02

/////////////////////////////////////////////////
// TGAPROTO_REQ_GETDIRINFO PARAM 2
#define TGAPROTO_REQ_GETDIRINFO_SET                 0x01
#define TGAPROTO_REQ_GETDIRINFO_GET                 0x02

/////////////////////////////////////////////////
// TG protol - struct


/////////////////////////////////////////////////
// TGARPT

struct TGARPT
{
	TGA_INT32 proto;
};

/////////////////////////////////////////////////
// TGARPT_TARGETINFO
struct TGARPT_TARGETINFO 
	: public TGARPT
{
	void Initialize()
	{
		proto = TGAPROTO_REQ_TARGETINFO;
	}

	TGA_TARGETINFO ti;     // target info
};

/////////////////////////////////////////////////
// TGARPT_TSKINFOLST

struct TGARPT_TSKINFOLST
	: public TGARPT
{
	void Initialize()
	{
		proto = TGAPROTO_REQ_TASKINFO;
	}

	TGA_TSKINFOLST  til;   // task info list
}; 


/////////////////////////////////////////////////
// TGARPT_HEAPINFOLST

struct TGARPT_HEAPINFOLST
	: public TGARPT
{
	void Initialize()
	{
		proto = TGAPROTO_REQ_HEAPINFO;
	}

	TGA_HEAPINFOLST  hil;  // heap info list
};


/////////////////////////////////////////////////
// TGARPT_STKINFOLST  

struct TGARPT_STKINFOLST
	: public TGARPT
{
	void Initialize()
	{
		proto = TGAPROTO_REQ_STACKINFO;
	}

	TGA_STKINFOLST sil;
}; 


/////////////////////////////////////////////////
// TGARPT_BUFINFOLST

struct TGARPT_BUFINFOLST
	: public TGARPT
{
	void Initialize()
	{
		proto = TGAPROTO_REQ_BUFFERINFO;
	}

	TGA_BUFINFOLST bil;
};


/////////////////////////////////////////////////
// TGARPT_TASKMQINFOLST

struct TGARPT_TASKMQINFOLST
	: public TGARPT
{
	void Initialize()
	{
		proto = TGAPROTO_REQ_TASKMQINFO;
	}

	TGA_TASKMQINFOLST   tmil;
};

/////////////////////////////////////////////////
// TGARPT_MDQ

struct TGARPT_MDQ
	: public TGARPT
{
	void Initialize()
	{
		proto = TGAPROTO_REQ_MDQ;
	}

	TGAMDQ mdq;
};

struct TGARPT_STKUSGLST
	: public TGARPT
{
	void Initialize()
	{
		proto = TGAPROTO_REQ_STACKUSAGE;
	}

	TGAITEM_STKLST stkl;
};

/////////////////////////////////////////////////
// TGARPT_CFGINFO
struct TGARPT_CFGINFO
	: public TGARPT
{
	void Initialize()
	{
		proto = TGAPROTO_REQ_CFGINFO;
	}

	TGACFG_CPU       CPU;
	TGACFG_HEAP      HEAP;
	TGACFG_STK       STK;
	TGACFG_BUFUSG    BUFUSG;
	TGACFG_KEY       KEY;
	TGACFG_RECKEY    RECKEY;
	TGACFG_LOG       LOG;
	TGACFG_TMSGQ     TMSGQ;
};

/////////////////////////////////////////////////
// TGAREQ_GETDIRINFO

struct TGAREQ_GETDIRINFO
{
	char DirName[256];
};

struct TGAREQ_DELETEFILE
{
	char FilePath[256];
};

struct TGARPT_GETDIRINFO_HDR
	: public TGARPT
{
	void Initialize()
	{
		proto = TGAPROTO_REQ_GETDIRINFO;
	}

	TGA_INT32   Count;
};


struct TGARPT_GETDIRINFO
	: public TGARPT_GETDIRINFO_HDR
{
	TGAFILEINFO file[1];    
};

//-- [2010-11-25] Lee JungTaek
//-- OSD PLATFORM(D2P/COACH12/COACH13/CAMC12)
#pragma pack(push,1)
typedef struct _OSD_LANGUAGE_TEST_HEADER
{
	UINT	GuideCode;
	BYTE	Direction;
	USHORT	Items;
	USHORT	PayLoadLen;
	BYTE	DataID;
}OSD_LANGUAGE_TEST_HEADER;
#pragma pack(pop)

//-- [2011-1-31] keunbae.song
//-- OSD PLATFORM(D3)
#pragma pack(push,1)
typedef struct _OSD_LANGUAGE_TEST_HEADEREX
{
	UINT	GuideCode;
	USHORT Direction;
	USHORT	Items;
	USHORT	PayLoadLen;
	USHORT DataID;
} OSD_LANGUAGE_TEST_HEADEREX;
#pragma pack(pop)



typedef struct _OSD_LANGUAGE_TEST_DATA
{
	UINT32	WidgetID;
	USHORT	WidgetWidth;
	USHORT	WidgetHeight;
	USHORT	StringWidth;
	USHORT 	StringHeight;
	USHORT	BufferSize;
	wchar_t *Buffer;
}OSD_LANGUAGE_TEST_DATA;

//-- 2010-11-25 keunbae.song
typedef struct _OSD_LANGUAGE_TEST_GETDATA
{
	wstring stringid;
	wstring stringdata;
	UINT32	widgetid;
	USHORT	widgetwidth;
	USHORT	widgetheight;
	USHORT	stringwidth;
	USHORT 	stringheight;
} OSD_LANGUAGE_TEST_GETDATA;
