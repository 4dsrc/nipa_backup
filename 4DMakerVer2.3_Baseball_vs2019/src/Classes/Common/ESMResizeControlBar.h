////////////////////////////////////////////////////////////////////////////////
//
//	ESMResizeControlBar.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-30
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

class CESMResizeControlBar : public CExtControlBar
{
public:
	CESMResizeControlBar();
	~CESMResizeControlBar(void);

	void SetMinSize(CSize sz);
	void SetMaxSize(CSize sz);
	
private:
	CSize m_szMin;
	CSize m_szMax;

	virtual INT _CalcDesiredMinHW() const
	{
		return m_szMin.cx;
	}

	virtual INT _CalcDesiredMinVH() const
	{
		return m_szMin.cy;
	}
	
	virtual CSize _CalcDesiredMinFloatedSize() const
	{
		//TRACE(_T("Set Min W(%d), H(%d)\n"),_CalcDesiredMinHW(),_CalcDesiredMinVH());
		return CSize( _CalcDesiredMinHW(), _CalcDesiredMinVH() );
	}

	virtual CSize _CalcDesiredMinOuterSize( BOOL bHorz ) const
	{
		bHorz;
		//TRACE(_T("_CalcDesiredMinOuterSize\n"));
		return _CalcDesiredMinFloatedSize();
	}

	void _RowResizingUpdateState();
};
