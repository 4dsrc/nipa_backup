/////////////////////////////////////////////////////////////////////////////
//
//  ESMProcessControl.cpp: implementation of the Registry class.
//
//
// Copyright (c) 2008 ESMLab, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of ESMLab, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2008-06-26
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMProcessControl.h"
#include "Tlhelp32.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

int g_nProcessCount = 0;
BOOL g_bExistBrowser = FALSE;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

 CESMProcessControl:: CESMProcessControl() {}
 CESMProcessControl::~ CESMProcessControl() {}

//----------------------------------------------------
//--
//-- GET WINDOWS HANDLE
//-- 2008-07-14
//--
//----------------------------------------------------

typedef struct tagWNDINFO { 
    DWORD   dwProcessID; 
    HWND    hWnd; 
} WNDINFO; 

BOOL CALLBACK EnumWindowsProc(HWND hWnd, LPARAM lParam)
{ 
	WNDINFO*    pWndInfo = (WNDINFO*)(lParam); 
	DWORD       dwProcessID; 

	::GetWindowThreadProcessId(hWnd, &dwProcessID);
	if (dwProcessID == pWndInfo->dwProcessID) 
	{ 
		pWndInfo->hWnd = hWnd;
		return(FALSE);
	} 
	return(TRUE);
} 

BOOL  CESMProcessControl::IsExist(CString strProcess, BOOL bSelf)
{
	HANDLE hndl = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
	DWORD dwsma = GetLastError();

	DWORD dwExitCode = 0;

	PROCESSENTRY32  procEntry={0};
	procEntry.dwSize = sizeof( PROCESSENTRY32 );
	Process32First(hndl,&procEntry);
	int nCount = 0;
	if(bSelf)
		nCount = 1;
	do
	{
		//if(!_strcmpi(procEntry.szExeFile,strProcess))
		if(!_tcsicmp(procEntry.szExeFile,strProcess))
		{
			if(!nCount)
				return TRUE;		
			nCount--;
		}
	}while(Process32Next(hndl,&procEntry));	
	return FALSE;
}

int CESMProcessControl::KillProcess(CString strProcess)
{
	HANDLE hndl = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
	DWORD dwsma = GetLastError();

	DWORD dwExitCode = 0;
	int nCnt = 0;

	PROCESSENTRY32  procEntry={0};
	procEntry.dwSize = sizeof( PROCESSENTRY32 );
	Process32First(hndl,&procEntry);
	do
	{
		if(!_tcsicmp(procEntry.szExeFile,strProcess))
		{
			HANDLE hHandle;
			hHandle = ::OpenProcess(PROCESS_ALL_ACCESS,0,procEntry.th32ProcessID);	
			::GetExitCodeProcess(hHandle,&dwExitCode);
			::TerminateProcess(hHandle,dwExitCode);

			CloseHandle(hHandle);
			TRACE("Kill Process  %s\n",procEntry.szExeFile);
			nCnt++;
		}
		TRACE("Running Process  %s\n",procEntry.szExeFile);
	}while(Process32Next(hndl,&procEntry));	

	return nCnt;
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2012-08-09
//! @note     [IN] strCmdLine    : "cmd.exe /c java -jar -Dhost="127.0.0.1" -Dport="28704" WebAgent-jar-with-dependencies.jar"
//!           [IN] strCurrentDir : "C:\Program Files\Samsung\SMILE\bin"
//!           [IN] bShowWindow   : TRUE / FALSE
//!           [OUT]dwProcessId   : Process Identifier
//!           [OUT]dwErrorCode   : error-code
//! @return        
//! @revision 
//------------------------------------------------------------------------------
BOOL CESMProcessControl::StartConsoleProcess(CString strCmdLine, CString strCurrentDir, BOOL bShowWindow, DWORD& dwProcessId, DWORD& dwErrorCode)
{
	dwProcessId = 0;
	dwErrorCode = 0;
	
	strCmdLine.Replace(_T("C:\\Program Files"), _T("C:\\Progra~1"));

  STARTUPINFO suInfo;
  memset(&suInfo, 0, sizeof(suInfo));
  suInfo.cb          = sizeof(suInfo);
	suInfo.dwFlags     = STARTF_USESHOWWINDOW;
	suInfo.wShowWindow = bShowWindow?SW_SHOWMINIMIZED:SW_HIDE;
	suInfo.lpTitle     = _T("Web-Agent");

	PROCESS_INFORMATION procInfo;
	memset(&procInfo, 0, sizeof(procInfo));

	BOOL bResult = CreateProcess(NULL, (LPTSTR)(LPCTSTR)strCmdLine, NULL, NULL, FALSE, 
		CREATE_NEW_CONSOLE, NULL, strCurrentDir, &suInfo, &procInfo);

	if(!bResult)
	{
		dwErrorCode = GetLastError();
		dwProcessId = 0;

		return FALSE;
	}

  dwProcessId = procInfo.dwProcessId;

	//Sleep(1000);

	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2012-09-03
//! @note     [IN] dwProcessId : 
//! @return        
//! @revision 
//------------------------------------------------------------------------------
BOOL CESMProcessControl::IsRunningProcess(DWORD dwProcessId)
{
	HANDLE hProcess = OpenProcess(SYNCHRONIZE|PROCESS_TERMINATE, FALSE, dwProcessId);
	if(NULL == hProcess)
	{
		return FALSE;
	}

	return TRUE;
}

