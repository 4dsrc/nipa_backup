#pragma once


#include "ESMFunc.h"
// CESMMovieMgr

class CESMMakeMovieMgr : public CWinThread
{
	DECLARE_DYNCREATE(CESMMakeMovieMgr)

public:
	CESMMakeMovieMgr();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CESMMakeMovieMgr();

public:
	CESMArray	m_arMsg;
	BOOL	m_bThreadStop;
	BOOL	m_bWait;

public:
	void AddMsg(ESMEvent* pMsg);

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run(void);

protected:
	DECLARE_MESSAGE_MAP()
};


