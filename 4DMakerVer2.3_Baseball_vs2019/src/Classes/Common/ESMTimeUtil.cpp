////////////////////////////////////////////////////////////////////////////////
//
//	TGTimeUtil.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TGTimeUtil.h"


#define FEBRUARY        2
#define STARTOFTIME     1970
#define SECDAY          86400L
#define SECYR           (SECDAY * 365)
#define leapyear(year)  ((year) % 4 == 0)
#define days_in_year(a)  (leapyear(a) ? 366 : 365)
#define days_in_month(a) (month_days[(a) - 1])
 
static int month_days[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

struct rtc_time 
{
 int tm_sec;   //seconds after the minute - [0,59] 
 int tm_min;   //minutes after the hour - [0,59] 
 int tm_hour;  //hours since midnight - [0,23] 
 int tm_mday;  //day of the month - [1,31] 
 int tm_mon;   //months since January - [0,11]
 int tm_year;  //years since 1900 
 int tm_wday;  //days since Sunday - [0,6] 
 int tm_yday;  //days since January 1 - [0,365] 
 int tm_isdst; //daylight savings time flag 
};

//------------------------------------------------------------------------------ 
//! @brief    Constructor
//! @date     2012-3-13
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------
CTGTimeUtil::CTGTimeUtil()
{
}

//------------------------------------------------------------------------------ 
//! @brief    Destructor
//! @date     2012-3-13
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------
CTGTimeUtil::~CTGTimeUtil()
{
}

//------------------------------------------------------------------------------ 
//! @brief    SYSTEMTIME --> tick
//! @date     2012-3-13
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------
DWORD CTGTimeUtil::SystemTimeToTick(UINT year, UINT mon, UINT day, UINT hour, UINT min, UINT sec)
{
 DWORD dwTime = 0;
 
 if(0 >= (int)(mon -= 2)) 
 {
	 //1..12 -> 11,12,1..10
  mon += 12;  //Puts Feb last since it has leap day
  year -= 1;
 }
 
 dwTime = ((((DWORD) (year/4 - year/100 + year/400 + 367*mon/12 + day) + year*365 - 719499)*24 + hour //now have hours
  )*60 + min  //now have minutes
  )*60 + sec; //finally seconds
 
 return dwTime;
}

//------------------------------------------------------------------------------ 
//! @brief    This only works for the Gregorian calendar - i.e. after 1752 (in the UK)
//! @date     2012-3-13
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------
void TGTimeUtil_GregorianDay(struct rtc_time * tm)
{
	int leapsToDate;
	int lastYear;
	int day;
	int MonthOffset[] = { 0,31,59,90,120,151,181,212,243,273,304,334 };
	
	lastYear = tm->tm_year-1;
	
	//Number of leap corrections to apply up to end of last year
	leapsToDate = lastYear/4 - lastYear/100 + lastYear/400;
	
	//This year is a leap year if it is divisible by 4 except when it is divisible by 100 unless it is divisible by 400
	//e.g. 1904 was a leap year, 1900 was not, 1996 is, and 2000 will be
	if((tm->tm_year%4==0) && ((tm->tm_year%100!=0) || (tm->tm_year%400==0)) && (tm->tm_mon>2)) 
	{
		//We are past Feb. 29 in a leap year
		day = 1;
	} 
	else 
	{
		day = 0;
	}

	day += lastYear*365 + leapsToDate + MonthOffset[tm->tm_mon-1] + tm->tm_mday;
	tm->tm_wday = day%7;
}

//------------------------------------------------------------------------------ 
//! @brief    TICK --> SYSTEMTIME
//! @date     2012-3-13
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------
SYSTEMTIME CTGTimeUtil::TickToSystemTime(DWORD tick)
{
	rtc_time tm;
	memset(&tm, 0, sizeof(rtc_time));

	register int i;
	register long hms, day;
	day = tick / SECDAY;
	hms = tick % SECDAY;
	
	//Hours, minutes, seconds are easy
	tm.tm_hour = hms / 3600;
	tm.tm_min = (hms % 3600) / 60;
	tm.tm_sec = (hms % 3600) % 60;

	//Number of years in days
	for(i = STARTOFTIME; day >= days_in_year(i); i++) 
	{
		day -= days_in_year(i);
	}
	
	tm.tm_year = i;

	//Number of months in days left
	if(leapyear(tm.tm_year)) 
	{
		days_in_month(FEBRUARY) = 29;
	}

	for(i=1; day >= days_in_month(i); i++) 
	{
		day -= days_in_month(i);
	}
	
	days_in_month(FEBRUARY) = 28;
	
	tm.tm_mon = i;
	//Days are what is left over (+1) from all that.
	tm.tm_mday = day + 1;

	//Determine the day of week
	TGTimeUtil_GregorianDay(&tm);

	SYSTEMTIME systime;
	memset(&systime, 0, sizeof(SYSTEMTIME));
	systime.wYear   = tm.tm_year;
	systime.wMonth  = tm.tm_mon;
	systime.wDay    = tm.tm_mday;
	systime.wHour   = tm.tm_hour;
	systime.wMinute = tm.tm_min;
	systime.wSecond = tm.tm_sec;
	systime.wDayOfWeek = tm.tm_wday;

	return systime;
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2012-3-19
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
CString CTGTimeUtil::TicktoSystemTime(IN DWORD tick)
{
	CString sTime = _T("");

	SYSTEMTIME st = CTGTimeUtil::TickToSystemTime(tick); 
	//sTime.Format(_T("%04d/%02d/%02d %02d:%02d:%02d.%03d"), st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
	sTime.Format(_T("%04d-%02d-%02d %02d:%02d:%02d.%03d"), st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);

	return sTime;
}
