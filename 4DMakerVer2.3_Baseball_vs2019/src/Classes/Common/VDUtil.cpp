////////////////////////////////////////////////////////////////////////////////
//
//	VDUtil.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "VDUtil.h"
#include <math.h>
#include <winsock2.h>

//-- 2012-06-13 hongsu@esmlab.com
//-- For Get IP, Mac Address 
#pragma comment(lib, "Iphlpapi.lib")
#include <Iptypes.h>
#include <Iphlpapi.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

VDUtil::VDUtil(void)
{
}

VDUtil::~VDUtil(void)
{
}

BOOL VDUtil::GetLineInFile(CFile *fp, CStringArray* pArStr)
{
	if(!pArStr->GetCount())
	{
		TRACE("String Array empty.\n");
	}
	ULONGLONG ulFileSize = fp->GetLength();
	if(!ulFileSize)
	{
		TRACE("File Size is zero\n");
		return FALSE;
	}

	fp->SeekToBegin();

	//
	//- GET LINE, getString
	//	
	char cReadChar;
	int  iReadBufIndex = 0;
	int iSizeCount = 0;
	BOOL isNotes = FALSE;

	char readBuff[1000];	// command GetLength is limited 1000 char.
	while( fp->Read(&cReadChar, 1) )
	{
		iSizeCount++;

		// if notes, ignore cmd line.
		if(cReadChar == '#' && iReadBufIndex == 0)
		{
			isNotes =TRUE;
		}
		else if( cReadChar == '/' && iReadBufIndex == 0)
		{	
			readBuff[iReadBufIndex] = cReadChar;	
			iReadBufIndex++;

			fp->Read(&cReadChar, 1);
			if( cReadChar == '/')
				isNotes = TRUE;
		}
		else if(cReadChar == '\r')
		{
			//do nothing. skip parse.			
		}
		//-- Load 1 Line
		else if(cReadChar == '\n')		// if encount '\n'
		{				
			readBuff[iReadBufIndex++] = '\0';
			iReadBufIndex = 0;

			if(!isNotes)	// if notes. copy string. and return.
			{	
#ifdef UNICODE
				TCHAR pchCmd[1000] = {0};
				mbstowcs(pchCmd, readBuff, strlen(readBuff));
				pArStr->Add(pchCmd);
#else
				char* strCmd = new char[strlen(readBuff)+1]; 
				strcpy(strCmd, readBuff);  
				pArStr->Add(strCmd);
				delete strCmd;
#endif		
			}
			else
				isNotes = FALSE;
		}
		//-- Load the end of Line
		else if(iSizeCount == ulFileSize)	// if encount EOF
		{
			readBuff[iReadBufIndex++] = cReadChar;				
			readBuff[iReadBufIndex] = '\0';

			if(!isNotes)	// if notes. do not CreatTestStep.
			{	

#ifdef UNICODE
				TCHAR pchCmd[1000] = {0};
				mbstowcs(pchCmd, readBuff, strlen(readBuff));
				pArStr->Add(pchCmd);
#else
				char* strCmd = new char[strlen(readBuff)+1]; 
				strcpy(strCmd, readBuff);  
				pArStr->Add(strCmd);
				delete strCmd;
#endif		
			}
			else
				isNotes = FALSE;
		}
		else
		{
			readBuff[iReadBufIndex] = cReadChar;	
			iReadBufIndex++;
		}	
	}

	return TRUE;
}

void VDUtil::ReplaceFileDelimit(CString strOrgin, CString  strConvertPath)
{	
	int nIndex = 0;
	for(int i = 0; i < strOrgin.GetLength(); i++)
	{
		if( strOrgin.GetAt(i) == '/')		
		{
			strConvertPath.SetAt(nIndex++,'\\');	
			strConvertPath.SetAt(nIndex++,'\\');
		}
		else	
			strConvertPath.SetAt(nIndex++,strOrgin.GetAt(i)); 
	}
	strConvertPath.SetAt(nIndex,'\0');
}

CString  VDUtil::GetFileName(CString strFile)
{	
	CString  strFileName;	
	strFile.Replace(_T("/"), _T("\\"));

	int nLastDelimeter = strFile.ReverseFind ('\\');		
	strFile.Delete(0, nLastDelimeter+1);
	return strFile;
}

CString	VDUtil::GetFolder(CString strFolderName)
{
	CString  strFileName;	
	strFolderName.Replace(_T("/"), _T("\\"));
	//-- SET FILE NAME
	int nLastDelimeter;

	nLastDelimeter = strFolderName.ReverseFind('\\');	
	strFileName = strFolderName.Left(nLastDelimeter);

	return strFileName ;
}

CString  VDUtil::GetFileID(CString strFileName)
{	
	strFileName = strFileName.Left(strFileName.GetLength()-4);
	//-- SET FILE NAME
	return strFileName;
}

CString  VDUtil::GetFolderID(CString strFolderName)
{	
	CString  strFileName;	
	strFolderName.Replace(_T("/"), _T("\\"));
	//-- SET FILE NAME
	int nLastDelimeter;
	
	nLastDelimeter = strFolderName.ReverseFind('\\');	
	strFileName = strFolderName.Left(nLastDelimeter);

	nLastDelimeter = strFileName.ReverseFind ('\\');		
	strFileName.Delete(0, nLastDelimeter+1);
	
	return strFileName;
}

CString VDUtil::ReplacePathSymbol(CString strSource, CString strSymbol, CString strPath)
{	
	CString stringSymbol = _T("$(") + (CString)strSymbol +_T(")");	
	
	strSource.Replace(_T("/"), _T("\\"));	// replace '/' '\'
	strSource.Replace(strSymbol, strPath);	// replace symbol to path

	return strSource;
}

void VDUtil::StringToken(const CString& strSource, CStringArray* pArStr, const CString& delimiters)
{
	CString strToken, strDes;	
	int nPos = 0;

	//-- Set Destination
	strDes = strSource;
	while(strDes != _T(""))
	{
		nPos = strDes.Find(delimiters);
		if(nPos == -1)
		{
			if(strDes.GetLength())
				pArStr->Add(strDes);
			break;
		}

		//-- Send the left string
		strToken = strDes.Left(nPos);
		strDes = strDes.Mid(nPos+1);
		pArStr->Add(strToken);
	}
}

CString VDUtil::Trim(CString strSource)
{
	UINT nStartPos = 0;
	UINT nEndPos = (UINT)strSource.GetLength() - 1;

	//-- Trim Left
	for(UINT i = 0; i < nEndPos; i++)
	{
		if(strSource.GetAt(i) != ' ' && strSource.GetAt(i) != '\t')
		{
			nStartPos = i;
			break;
		}
	}

	//-- Trim Right
	for(UINT i = nEndPos; i > nStartPos; i--)
	{
		if(strSource.GetAt(i) != ' ' && strSource.GetAt(i) != '\t')
		{
			nEndPos = i;
			break;
		}
	}

	const int STRLEN = nEndPos - nStartPos + 1;
	strSource.Delete(nStartPos, STRLEN);	
	return strSource;
}


//--------------------------------------------------------------------------
//!@fn			itoa
//!@brief		translate integer to string
//
//!@param[in]	int value, unsigned int base	:  0 < base <= 16 
//!@param[out]	none
//!@return		string
//!@note				
//--------------------------------------------------------------------------
CString VDUtil::itoa(int value, unsigned int base)
{
	const TCHAR digitMap[] = _T("0123456789abcdef");
	CString strBuf;

	// Guard: base 
	if (base == 0 || base > 16) 		
		return strBuf;

	// negative int
	CString strSign;
	int _value = value;

	// if 0
	if (_value == 0) return _T("0");

	if (value < 0) {
		_value = -value;
		strSign = _T("-");
	}

	// Translating number to string with base:
	for (int i = 30; _value && i ; --i) {
		strBuf = digitMap[ _value % base ] + strBuf;
		_value /= base;
	}

	strSign.Append(strBuf);
	return strSign;
} 


//--------------------------------------------------------------------------
//!@brief	Convert int to CSting	
//
//!@param	value : integer value
//!@param	base  : 2, 4, 8, 10, 16
//!@return	on success, any other value means failure.
//!@note				
//--------------------------------------------------------------------------
CString VDUtil::itocs(const int value, const unsigned int base)
{
	CString strValue = itoa(value, base);
	return strValue;
}

//--------------------------------------------------------------------------
//!@brief	16/Str -> Int
//--------------------------------------------------------------------------
int VDUtil::wtoi(CString str16Value)
{
	CString strChar;
	
	double nValue = 0;
	int nAll = str16Value.GetLength();
	str16Value = str16Value.MakeLower();
	
	for(int i = 0 ; i < nAll ; i ++)
	{
		strChar = str16Value.GetAt(i);
 		  if(strChar == _T("0"))	{ nValue += ( 0 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("1"))	{ nValue += ( 1 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("2"))	{ nValue += ( 2 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("3"))	{ nValue += ( 3 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("4"))	{ nValue += ( 4 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("5"))	{ nValue += ( 5 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("6"))	{ nValue += ( 6 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("7"))	{ nValue += ( 7 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("8"))	{ nValue += ( 8 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("9"))	{ nValue += ( 9 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("a"))	{ nValue += (10 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("b"))	{ nValue += (11 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("c"))	{ nValue += (12 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("d"))	{ nValue += (13 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("e"))	{ nValue += (14 * (pow(16.0, (nAll-1-i)))); }
	 else if(strChar == _T("f"))	{ nValue += (15 * (pow(16.0, (nAll-1-i)))); }
	}

	return (int)nValue;
}
//--------------------------------------------------------------------------
//!@brief	16/Str -> Int
//--------------------------------------------------------------------------
CString	VDUtil::itow(int nVal16, BOOL bTag)
{
	int nAll = 2;	
	if(nVal16 > 256)
		nAll = 4;	

	CString str16;
	int nDigit[4];

	int nIndex = nAll;
	while(nIndex--)
	{
		nDigit[nIndex] = nVal16/pow(16.0,nIndex);
		if(nDigit[nIndex] > 0)
			nVal16 -= nDigit[nIndex]*pow(16.0,nIndex);
	}

	nIndex = nAll;
	while(nIndex --)
	{
			 if(nDigit[nIndex] == 0 )	{ str16.Append(_T("0")); }
		else if(nDigit[nIndex] == 1 )	{ str16.Append(_T("1")); }
		else if(nDigit[nIndex] == 2 )	{ str16.Append(_T("2")); }
		else if(nDigit[nIndex] == 3 )	{ str16.Append(_T("3")); }
		else if(nDigit[nIndex] == 4 )	{ str16.Append(_T("4")); }
		else if(nDigit[nIndex] == 5 )	{ str16.Append(_T("5")); }
		else if(nDigit[nIndex] == 6 )	{ str16.Append(_T("6")); }
		else if(nDigit[nIndex] == 7 )	{ str16.Append(_T("7")); }
		else if(nDigit[nIndex] == 8 )	{ str16.Append(_T("8")); }
		else if(nDigit[nIndex] == 9 )	{ str16.Append(_T("9")); }
		else if(nDigit[nIndex] == 10)	{ str16.Append(_T("a")); }
		else if(nDigit[nIndex] == 11)	{ str16.Append(_T("b")); }
		else if(nDigit[nIndex] == 12)	{ str16.Append(_T("c")); }
		else if(nDigit[nIndex] == 13)	{ str16.Append(_T("d")); }
		else if(nDigit[nIndex] == 14)	{ str16.Append(_T("e")); }
		else if(nDigit[nIndex] == 15)	{ str16.Append(_T("f")); }		
	}

	if(bTag)
	{
		CString strTag;
		strTag.Format(_T("0x%s"),str16);
		return strTag;
	}
	
	return str16;
}

CString VDUtil::I2S(int value)
{
	CString strValue;
	strValue.Format(_T("%d"),value);
	return strValue;
} 

CString VDUtil::F2S(float value)
{
	CString strValue;
	strValue.Format(_T("%f"),value);
	return strValue;
}

CString VDUtil::F2S(float value, int nPointCnt)
{
	CString strValue;
	TCHAR str[5]={0};
	str[0] = _T('%');
	_stprintf(&str[1], _T(".%df"), nPointCnt);

	strValue.Format(str, value);
	return strValue;

}
//------------------------------------------------------------------------------ 
//! @brief
//! @date     2011-6-22
//! @owner    keunbae.song
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
void VDUtil::PrintToFile(const TCHAR* strPath, const TCHAR* format, ...)
{
	FILE* fp = _tfopen(strPath, _T("a+"));

	va_list args;
	va_start(args, format);

	_vftprintf(fp, format, args);
	_ftprintf(fp, _T("\r\n"));
	fflush(fp);
	va_end(args);

	fclose(fp);
}

//------------------------------------------------------------------------------ 
//! @brief   Reposition video control.
//! @date     2012-4-16
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------
DWORD VDUtil::ReadFileEx(HANDLE hFile, BYTE* pBuffer, DWORD nNumberOfBytesToRead)
{
	DWORD dwIndex = 0;
	while(TRUE)
	{
		DWORD dwRead = 0;
		if(!ReadFile(hFile, pBuffer+dwIndex, nNumberOfBytesToRead, &dwRead, NULL))
			return GetLastError();
		
		if((dwRead == nNumberOfBytesToRead) || (nNumberOfBytesToRead == 0)) 
			break;
		
		if(dwRead == 0) 
			return ERROR_HANDLE_EOF;

		dwIndex += dwRead;
		nNumberOfBytesToRead -= dwRead;
	}

	return ERROR_SUCCESS;
}

CString VDUtil::GetLocalMacAddress()
{
	CString strMacAddress = _T("00:00:00:00:00:00");
	IP_ADAPTER_INFO AdapterInfo[5];

	DWORD dwBufLen = sizeof(AdapterInfo);
	DWORD dwStatus = GetAdaptersInfo(AdapterInfo, &dwBufLen);                  

	if(dwStatus != ERROR_SUCCESS)
	{
		strMacAddress.Format(_T("Error : %d "),dwStatus) ;
		return strMacAddress ;
	}
	PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo;

	strMacAddress.Format(_T("%02X:%02X:%02X:%02X:%02X:%02X"),
		pAdapterInfo->Address[0],
		pAdapterInfo->Address[1],
		pAdapterInfo->Address[2],
		pAdapterInfo->Address[3],
		pAdapterInfo->Address[4],
		pAdapterInfo->Address[5]);

	return strMacAddress;
}


CString VDUtil::GetLocalIPAddress()
{
	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	CString strIP;
	PHOSTENT hostinfo;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 )
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				strIP = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}
		}      
		WSACleanup( );
	} 
	return strIP;
}

CString VDUtil::GetLocalHostname()
{
	CString strHostname;
	// Add 'ws2_32.lib' to your linker options
	WSADATA WSAData;
	// Initialize winsock dll
	if(::WSAStartup(MAKEWORD(1, 0), &WSAData))
		return strHostname;

	// Get local host name
	char szHostName[128] = "";
	if(::gethostname(szHostName, sizeof(szHostName)))
		return strHostname;


#ifdef UNICODE
	TCHAR pchHostName[128] = {0};
	mbstowcs(pchHostName, szHostName, strlen(szHostName));
	strHostname.Format(_T("%s"), pchHostName);
#else
	strHostname.Format (_T("%s"),szHostName);
#endif
	return strHostname;
}

CString VDUtil::GetDate()
{	
	CString strDate;
	CTime time  = CTime::GetCurrentTime();
	int	nYear = time.GetYear();		//년도
	int	nMon = time.GetMonth();		//월
	int	nDay = time.GetDay();		//날짜.	
	strDate.Format(_T("%d/%d/%d"), nYear, nMon, nDay);
	return strDate;
}

CString VDUtil::GetTime()
{	
	CString strTime;
	CTime time  = CTime::GetCurrentTime();
	int	nH = time.GetHour();	//시
	int	nM = time.GetMinute();	//분
	int	nS = time.GetSecond();		//초
	strTime.Format(_T("%d:%d:%d"), nH, nM, nS);
	return strTime;
}

CString VDUtil::GetGraphTime()
{	
	CString strTime;
	SYSTEMTIME tTime = {0,};
	::GetLocalTime(&tTime);                 
	strTime.Format(_T("%04d%02d%02d%02d%02d%02d"), tTime.wYear, tTime.wMonth, tTime.wDay, tTime.wHour, tTime.wMinute, tTime.wSecond);
	return strTime;
}


void VDUtil::GetMac(CString strMac, DWORD* dMac)
{
	CStringArray arMac;
	StringToken(strMac, &arMac, _T("-"));
	if(arMac.GetCount() < MAC_ADDR_CNT)
		return;
	for(int i=0; i < MAC_ADDR_CNT; i ++)
		dMac[i] = wtoi(arMac.GetAt(i));
	arMac.RemoveAll();
}


void VDUtil::GetMac(CString strMac, BYTE* btMac)
{
	CStringArray arMac;
	StringToken(strMac, &arMac, _T("-"));
	if(arMac.GetCount() < MAC_ADDR_CNT)
		return;
	for(int i=0; i < MAC_ADDR_CNT; i ++)
		btMac[i] = wtoi(arMac.GetAt(i));
	arMac.RemoveAll();
}


void VDUtil::MultiByteToWideChar(char *cBuffer, wchar_t *strBuffer)
{
	int iBufLen = (int)strlen(cBuffer);
	if(iBufLen <= 0)
		return;

	int Unicodelen = ::MultiByteToWideChar(CP_UTF8, NULL, cBuffer,iBufLen, NULL, 0);
	wchar_t* szUnicode = new wchar_t[Unicodelen+1];
	::MultiByteToWideChar(CP_UTF8, NULL , cBuffer, iBufLen, szUnicode, Unicodelen);
	szUnicode[Unicodelen] = _T('\0');
	wcscpy(strBuffer, szUnicode);
	delete[] szUnicode;
}

//-- 2012-06-26 cy.gil@esmlab.com use by Agent
void VDUtil::WideCharToMultiByte(wchar_t *strBuffer, char *cBuffer)
{

	DWORD dwLength = wcslen(strBuffer);
	if(dwLength <= 0)
		return;

	::WideCharToMultiByte(CP_ACP, 0, strBuffer, dwLength, cBuffer, dwLength, NULL, NULL);
	cBuffer[dwLength] = '\0';
}

CString VDUtil::GetBoolToStr(BOOL b)
{
	if(b)
		return VD_STR_YES;
	return VD_STR_NO;
}

BOOL VDUtil::GetBoolFromStr(CString str)
{
	if(str == VD_STR_YES)
		return TRUE;
	return FALSE;
}


//------------------------------------------------------------------------------
//! @brief				
//! @date		2012-07-16
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
char* VDUtil::GetCharString(CString str)
{
	int nLength = str.GetLength()+1;
	char* pChar = new char[nLength];
	if(!pChar)
		return NULL;

	memset(pChar, 0, str.GetLength()+1);
	size_t CharCinverted = 0;
	//wcstombs(pfullPath, fullPath, _tcslen(fullPath));  
	wcstombs_s(&CharCinverted, pChar, nLength, str, _tcslen(str));
	return pChar; 

	//char* pcBuffer = new char[strBuffer.GetLength()+1];
	//WideCharToMultiByte(strBuffer, pcBuffer);
}

//------------------------------------------------------------------------------
//! @brief		
//! @date		2012-07-16
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
CString VDUtil::GetStrChar(char* ch, BOOL bDelete)
{
	CString str;

	TCHAR pch[128] = {0};
	mbstowcs(pch, ch, strlen(ch));
	str.Format(_T("%s"), pch);

	//-- 2012-07-16 hongsu@esmlab.com
	//-- Remove Char
	if(bDelete)
		delete ch;
	return str; 	
}