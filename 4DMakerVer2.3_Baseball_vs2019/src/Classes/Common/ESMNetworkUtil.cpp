////////////////////////////////////////////////////////////////////////////////
//
//	TGNetworkUtil.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TGNetworkUtil.h"
#include "iphlpapi.h"

#define XNS_LEN_MAC_ADDRESS	6


//------------------------------------------------------------------------------------------------------ 
//! @brief
//! @date     2012-03-23
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------------------------------ 
CTGNetworkUtil::CTGNetworkUtil()
{
}

//------------------------------------------------------------------------------------------------------ 
//! @brief
//! @date     2012-03-23
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------------------------------
CTGNetworkUtil::~CTGNetworkUtil()
{
}

//------------------------------------------------------------------------------------------------------ 
//! @brief
//! @date     2012-03-23
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------------------------------ 
CString CTGNetworkUtil::ConvertMACAddress(BYTE* macaddr)
{
	CString strMACAddr = _T("");

	if(NULL == macaddr)
		return strMACAddr;

	for(int i=0;i<XNS_LEN_MAC_ADDRESS;i++)
	{
		CString strTemp;
		strTemp.Format(_T("%02X"), macaddr[i]);
		strMACAddr += strTemp;

		if(i != XNS_LEN_MAC_ADDRESS-1) 
			strMACAddr += _T(":");
	}

	return strMACAddr;
}

//------------------------------------------------------------------------------------------------------
//! @brief    Converts "00091871A748" to "00:09:18:71:A7:48"
//! @date     2012-05-03
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note     
//! @return        
//! @revision 
//------------------------------------------------------------------------------------------------------
CString CTGNetworkUtil::ConvertMACAddress(CString macaddr)
{
	CString newmacaddr = _T("");

	if(macaddr.GetLength() < 12)
		return macaddr;

	macaddr.Replace(_T(":"), _T(""));

	newmacaddr += macaddr.Mid(0, 2);
	newmacaddr += _T(":");
	newmacaddr += macaddr.Mid(2, 2);
	newmacaddr += _T(":");
	newmacaddr += macaddr.Mid(4, 2);
	newmacaddr += _T(":");
	newmacaddr += macaddr.Mid(6, 2);
	newmacaddr += _T(":");
	newmacaddr += macaddr.Mid(8, 2);
	newmacaddr += _T(":");
	newmacaddr += macaddr.Mid(10, 2);

	return newmacaddr;
}

//------------------------------------------------------------------------------------------------------ 
//! @brief    Navigates the browser to a URL.
//! @date     2012-05-09
//! @owner    keunbae.song
//! @note     url : "http://www.google.com"
//! @return        
//! @revision 
//------------------------------------------------------------------------------------------------------
void CTGNetworkUtil::URLNavigate(CString url)
{
	::CoInitialize(NULL);

	HRESULT hr;
	IWebBrowser2* pWebBrowser = NULL;
	hr = CoCreateInstance(CLSID_InternetExplorer, NULL, CLSCTX_SERVER, IID_IWebBrowser2, (LPVOID*)&pWebBrowser);

	if(SUCCEEDED(hr) && (pWebBrowser != NULL))
	{
		VARIANT vDummy = {0};
		VARIANT vUrl;

		vUrl.vt = VT_BSTR;
		vUrl.bstrVal = SysAllocString(url); 
		pWebBrowser->Navigate2(&vUrl, &vDummy, &vDummy,&vDummy, &vDummy);
		VariantClear(&vUrl);

		HWND hWnd;
    pWebBrowser->get_HWND((long*)&hWnd);
		ShowWindow(hWnd, SW_SHOWMAXIMIZED);

		// If you want to display the IE window...
		pWebBrowser->put_Visible(VARIANT_TRUE);
	}
	else
	{
		if(pWebBrowser)
			pWebBrowser->Release();
	}

	//-- Remove Database OLE
	::CoUninitialize();	
}

//------------------------------------------------------------------------------------------------------ 
//! @brief    If characters such as blanks and punctuation are passed in an HTTP stream, they might be 
//!           misinterpreted at the receiving end. URL encoding replaces characters that are not allowed 
//!           in a URL with character-entity equivalents consisting of hexadecimal escape sequences. 
//!           The converted string is expected to conform to the UTF-8 format. URL encoding replaces all
//!           character codes except for letters, numbers, and the following punctuation characters: 
//!           -(minus sign) / _(underscore) / .(period) / !(exclamation point) / *(asterisk) / 
//!           '(single quoatation mark) / ((opening parentheses / )(closing parentheses)
//!           For example, when embedded in a block of text to be transmitted in a URL, 
//!           the characters < and > are encoded as %3c and %3e. 
//! @date     2012-05-10
//! @owner    keunbae.song
//! @note     
//! @return        
//! @revision 
//--------------------------------------------------------------------------------------------------------
CString CTGNetworkUtil::UriEncode(CString src)
{  
	return _T("");
}

//-------------------------------------------------------------------------------------------------------- 
//! @brief    reverses the encoding.
//! @date     2012-05-10
//! @owner    keunbae.song
//! @note     
//! @return        
//! @revision 
//--------------------------------------------------------------------------------------------------------
CString CTGNetworkUtil::UriDecode(CString src)
{
	return _T("");
}

//-------------------------------------------------------------------------------------------------------- 
//! @brief    Check whether the specified TCP port is in the LISTEN state waiting for a connection request.
//! @date     2012-07-03
//! @owner    keunbae.song
//! @note     
//! @return        
//! @revision 
//--------------------------------------------------------------------------------------------------------
BOOL CTGNetworkUtil::IsTCPListenPort(USHORT port)
{
	BOOL bResult = FALSE;

	MIB_TCPTABLE* pTcpTable = (MIB_TCPTABLE*)malloc(sizeof(MIB_TCPTABLE));
	
	DWORD dwSize = sizeof(MIB_TCPTABLE);

  //Make an initial call to GetTcpTable to get the necessary size into the dwSize variable
	DWORD dwRetVal = GetTcpTable(pTcpTable, &dwSize, TRUE);
	if(ERROR_INSUFFICIENT_BUFFER == dwRetVal)
	{
		free(pTcpTable);
		pTcpTable = (MIB_TCPTABLE*)malloc(dwSize);
	}

	//Make a second call to GetTcpTable to get the actual data we require
  dwRetVal =  GetTcpTable(pTcpTable, &dwSize, TRUE);
	if(NO_ERROR != dwRetVal)
	{
		TGLog(1, _T("[WEB] ERROR: Failed to get TCP-Table")); 
		return FALSE;
	}

	//Retrieves the IPv4 TCP connection table.
	//GetTcpTable(tcptable, &tablesize, TRUE);
	int num = pTcpTable->dwNumEntries;

	for(int i=0; i<num; i++)
	{
		MIB_TCPROW row = pTcpTable->table[i];
		//if(row.dwState != MIB_TCP_STATE_LISTEN)
		//	continue;

		USHORT listenport = ntohs((USHORT)(row.dwLocalPort & 0xFFFF));
		if(listenport == port)
		{
			bResult = TRUE;
			break;
		}
	}

	free(pTcpTable);
	pTcpTable = NULL;

	return bResult;
}

//BOOL CTGNetworkUtil::IsTCPListenPort(USHORT port)
//{
//	BOOL bResult = FALSE;
//
//	DWORD tablesize = sizeof(MIB_TCPTABLE) * 128;
//	MIB_TCPTABLE* tcptable = (MIB_TCPTABLE*)malloc(tablesize);
//
//	//Retrieves the IPv4 TCP connection table.
//	GetTcpTable(tcptable, &tablesize, TRUE);
//	int num = tcptable->dwNumEntries;
//
//	for(int i=0; i<num; i++)
//	{
//		MIB_TCPROW row = tcptable->table[i];
//		if(row.dwState != MIB_TCP_STATE_LISTEN)
//			continue;
//
//		USHORT listenport = ntohs((USHORT)(row.dwLocalPort & 0xFFFF));
//		if(listenport == port)
//		{
//			bResult = TRUE;
//			break;
//		}
//	}
//
//	free(tcptable);
//	tcptable = NULL;
//
//	return bResult;
//}
