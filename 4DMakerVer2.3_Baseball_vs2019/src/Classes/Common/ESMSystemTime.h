////////////////////////////////////////////////////////////////////////////////
//
//	TGSystemTime.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
using namespace std;

typedef struct
{
	CString strName;
	TIME_ZONE_INFORMATION tInfo;
}TIMEZONE_INFO;

class CTGSystemTime
{
private:
	vector<TIMEZONE_INFO> m_TimezoneList;
	SYSTEMTIME m_tCurrentUTCTime;
	TIME_ZONE_INFORMATION m_tCurrentTimezoneInfo;
	//-- 2012-03-11 hongsu@esmlab.com
	//-- DSTType
	UINT m_nDSTType;
	BOOL m_bDST;
	BOOL m_bSetTime;

	BOOL SetTimezoneList();
	BOOL StringCompare(CString strReference, CString strTarget);
	BOOL StringFind(CString strReference, CString strTarget);
	time_t CTGSystemTime::ConvertTGSystemTimeToTime(SYSTEMTIME &tTime);
	SYSTEMTIME CTGSystemTime::ConvertTGTimeToSystemTime(time_t &tTime);

	//-- 2012-05-10
	//-- SmileTime
	time_t m_tSmileStartTime;
	DWORD m_dwSmileStartTick;
	
public:
	CTGSystemTime();
	~CTGSystemTime();

	BOOL Init();
	BOOL Backup();
	BOOL Restore();
	BOOL GetSystemTime(SYSTEMTIME &tTime);
	BOOL SetSystemTime(SYSTEMTIME tTime);
	BOOL GetLocalTime(SYSTEMTIME &tTime);
	BOOL SetLocalTime(SYSTEMTIME tTime);
	BOOL GetTimezoneList(vector<CString> &TimezoneList);
	BOOL GetTimezoneList(vector<TIMEZONE_INFO> &TimezoneList);
	DWORD GetTimezoneIndex(CString strTimezoneInfo);
	TIME_ZONE_INFORMATION GetTimezoneInfo(CString strTimezoneInfo);
	BOOL GetTimezone(DWORD &dwTimezoneListIndex);
	BOOL SetTimezone(DWORD dwTimezoneListIndex);
	BOOL GetTimezone(TIME_ZONE_INFORMATION &tTimezoneInfo);
	BOOL GetTimezone(DWORD dwTimezoneListIndex, TIME_ZONE_INFORMATION &tTimezoneInfo);
	BOOL SetTimezone(TIME_ZONE_INFORMATION tTimezoneInfo);
	BOOL GetDaylightSavingTime();
	BOOL SetDaylightSavingTime(BOOL bEnable, DWORD dwTimezoneListIndex);
	BOOL SyncTimeServer();
	//-- 2012-03-11 hongsu@esmlab.com
	//-- Set DST Type
	void SetDSTType(UINT nType, BOOL bDST) { m_nDSTType = nType; m_bDST =bDST; }
	UINT GetDSTType() {return m_nDSTType ;}
	BOOL GetDSTOpt() {return m_bDST ;}
	void CreateTimeZoneFile();

	//-- 2012-05-10
	//-- SmileTime
	BOOL GetSmileTime(SYSTEMTIME &tTime);
	time_t GetSmileTime();
};

