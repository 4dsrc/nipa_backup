////////////////////////////////////////////////////////////////////////////////
//
//	TGSystemTime.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TGSystemTime.h"
#include "TGSntp.h"
#include <Winsock2.h>


#define RETRY_COUNT 3
#define DIFFERENCE_TIME	1000

CTGSystemTime::CTGSystemTime()
{
	m_TimezoneList.clear();
	memset(&m_tCurrentUTCTime, 0, sizeof(SYSTEMTIME));
	memset(&m_tCurrentTimezoneInfo, 0, sizeof(TIME_ZONE_INFORMATION));

	SYSTEMTIME tTime;
	::GetLocalTime(&tTime);
	m_tSmileStartTime = ConvertTGSystemTimeToTime(tTime);
	m_dwSmileStartTick = GetTickCount();
	m_bSetTime = FALSE;
}

CTGSystemTime::~CTGSystemTime()
{
	if(m_bSetTime)
		Restore();
	//-- 2012-06-12 hongsu@esmlab.com
	m_TimezoneList.clear();
}

BOOL CTGSystemTime::SetTimezoneList()
{
	typedef struct _REG_TZI_FORMAT
	{
		LONG Bias;
		LONG StandardBias;
		LONG DaylightBias;
		SYSTEMTIME StandardDate;
		SYSTEMTIME DaylightDate;
	}REG_TZI_FORMAT;

	BOOL bResult = FALSE;
	CString strSubKey = _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Time Zones");
	HKEY hKey = HKEY_LOCAL_MACHINE;
	HKEY hKeyResult = NULL;
	DWORD dwIndex = 0;
	TCHAR wcKeyName[MAX_PATH] = {0,};
	TCHAR wcKeyStandard[MAX_PATH] = {0,};
	TCHAR wcKeyDaylight[MAX_PATH] = {0,};

	HKEY hChildKeyResult = NULL;
	CString strChildSubKey;
	REG_TZI_FORMAT tRegTZI;

	if(RegOpenKey(hKey, strSubKey, &hKeyResult) != ERROR_SUCCESS) goto RESULT_ERROR;

	m_TimezoneList.clear();
	while(TRUE)
	{
		memset(wcKeyName, 0, MAX_PATH*sizeof(TCHAR));
		if(RegEnumKey(hKeyResult, dwIndex++, wcKeyName, MAX_PATH) != ERROR_SUCCESS)	break;

		hChildKeyResult = NULL;
		strChildSubKey = strSubKey+_T("\\")+wcKeyName;
		if(RegOpenKey(hKey, strChildSubKey, &hChildKeyResult) != ERROR_SUCCESS) 
			goto RESULT_ERROR;

		memset(&tRegTZI, 0, sizeof(REG_TZI_FORMAT));
		memset(wcKeyName, 0, MAX_PATH*sizeof(TCHAR));
		memset(wcKeyStandard, 0, MAX_PATH*sizeof(TCHAR));
		memset(wcKeyDaylight, 0, MAX_PATH*sizeof(TCHAR));		
		DWORD dwSize = sizeof(REG_TZI_FORMAT);
		if(RegQueryValueEx(hChildKeyResult, _T("TZI"), 0, NULL, (LPBYTE)&tRegTZI, &dwSize) != ERROR_SUCCESS) 
			goto RESULT_ERROR;
		dwSize = MAX_PATH;
		if(RegQueryValueEx(hChildKeyResult, _T("Display"), 0, NULL, (LPBYTE)&wcKeyName, &dwSize) != ERROR_SUCCESS) 
			goto RESULT_ERROR;
		dwSize = MAX_PATH;
		if(RegQueryValueEx(hChildKeyResult, _T("Dlt"), 0, NULL, (LPBYTE)&wcKeyDaylight, &dwSize) != ERROR_SUCCESS) 
			goto RESULT_ERROR;
		dwSize = MAX_PATH;
		if(RegQueryValueEx(hChildKeyResult, _T("Std"), 0, NULL, (LPBYTE)&wcKeyStandard, &dwSize) != ERROR_SUCCESS) 
			goto RESULT_ERROR;

		TIMEZONE_INFO tTimezoneInfo;
		tTimezoneInfo.strName = wcKeyName;
		tTimezoneInfo.tInfo.Bias = tRegTZI.Bias;
		tTimezoneInfo.tInfo.DaylightBias = tRegTZI.DaylightBias;
		tTimezoneInfo.tInfo.DaylightDate = tRegTZI.DaylightDate;
		tTimezoneInfo.tInfo.StandardBias = tRegTZI.StandardBias;
		tTimezoneInfo.tInfo.StandardDate = tRegTZI.StandardDate;
		wcscpy(tTimezoneInfo.tInfo.DaylightName, wcKeyDaylight);
		wcscpy(tTimezoneInfo.tInfo.StandardName, wcKeyStandard);
		m_TimezoneList.push_back(tTimezoneInfo);
	}

	//-- 2012-04-01 hongsu@esmlab.com
	//-- Create Timezone File
	// CreateTimeZoneFile();

	bResult = TRUE;

RESULT_ERROR:
	if(hChildKeyResult != NULL) RegCloseKey(hChildKeyResult);
	if(hKeyResult != NULL) RegCloseKey(hKeyResult);

	return bResult;
}

//------------------------------------------------------------------------------
//! @class		
//! @brief		
//! @date		2012-04-01
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return		
//! @revision	
//------------------------------------------------------------------------------
void CTGSystemTime::CreateTimeZoneFile()
{
	//-- 2012-04-01 hongsu@esmlab.com
	//-- Test
	
	CString strFilePath;
	strFilePath.Format(_T("C:\\TimeZone.xls"));
	
	char chChange[512] = {0.};
	int nLength;
	int nCntNoKorean = 0;
	int nDeviceUTCCnt = 0;		

	CFile file;
	if(!file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite ))
		return;

	int nAll = m_TimezoneList.size();
	TIMEZONE_INFO *pTimezoneInfo;

	//-- 2012-04-01 hongsu@esmlab.com
	//-- Title 
	CString desc;
	CString strName;
	CString strGMT;
	int nToken[2];

	desc.Format(_T("LOCAL(English)\tLOCAL(Korean)\tUTC\tDST_START(Month_INDEX_DAY_TIME)\tDST_END(Month_INDEX_DAY_TIME\r\n"));	
	memset(chChange,0,sizeof(char)*512);
	nLength = desc.GetLength();
	WideCharToMultiByte( CP_ACP, 0, desc, desc.GetLength(), chChange, nLength, NULL, NULL );

	file.Write(chChange,nLength);
	file.Flush();

	
	//-- Contents
	for(int i = 0 ; i < nAll ; i ++)
	{
		nDeviceUTCCnt	= 0;
		nCntNoKorean	= 0;
		pTimezoneInfo	= &m_TimezoneList[i];

		nToken[0]	= pTimezoneInfo->strName.Find(_T("UTC"));
		nToken[1]	= pTimezoneInfo->strName.Find(_T(')'));
		strGMT		= pTimezoneInfo->strName.Mid(nToken[0]+3, nToken[1] - (nToken[0]+3));
		strName		= pTimezoneInfo->strName.Mid(nToken[1]+2);
		
		//-- 2012-03-14 hongsu@esmlab.com
		//-- Korean Language 		
		desc.Format(_T("%s\t%s\t%s\t%s\t%s\r\n"),
							_T("name"),
							strName,
							strGMT,
							pTimezoneInfo->tInfo.StandardName,
							pTimezoneInfo->tInfo.DaylightName);		
		//-- 2012-03-14 hongsu@esmlab.com
		//-- Check Korean Language Size		
		for(int j=0 ; j < desc.GetLength() ; j++)
		{
			if(desc.GetAt(j) != ' ')	nDeviceUTCCnt++;
			else						break;
		}
		for(int j = nDeviceUTCCnt ; j < desc.GetLength() ; j++)	
			if(	desc.GetAt(j) == ' ' || 
				desc.GetAt(j) == ',' || 
				desc.GetAt(j) == '(' || 
				desc.GetAt(j) == ')' ||	
				desc.GetAt(j) == ':' || 
				desc.GetAt(j) == '+' || 
				desc.GetAt(j) == '-' 		 				
				)	nCntNoKorean++;
		
		//-- 2012-03-14 hongsu@esmlab.com
		//-- Get Korean String Length 
		nLength = 2*desc.GetLength();// - nDeviceUTCCnt - nCntNoKorean + 2;
		// File Change To Ansi
		memset(chChange,0,sizeof(char)*512);
		WideCharToMultiByte( CP_ACP, 0, desc, desc.GetLength(), chChange, nLength, NULL, NULL );

		file.Write(chChange,nLength);
		file.Flush();
	}
	file.Close();
}

BOOL CTGSystemTime::StringCompare(CString strReference, CString strTarget)
{
	TCHAR* wcReference = strReference.GetBuffer();
	TCHAR* wcTarget = strTarget.GetBuffer();

	BOOL bResult = FALSE;
	if(wcsncmp(wcReference, wcTarget, strReference.GetLength()) == 0) bResult = TRUE;

	strReference.ReleaseBuffer();
	strTarget.ReleaseBuffer();

	return bResult;
}

//------------------------------------------------------------------------------
//! @function	StringFind		
//! @brief				
//! @date		2012-10-07
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
BOOL CTGSystemTime::StringFind(CString strReference, CString strTarget)
{
	/*
	TCHAR* wcReference = strReference.GetBuffer();
	TCHAR* wcTarget = strTarget.GetBuffer();

	BOOL bResult = FALSE;
	if(wcsncmp(wcReference, wcTarget, strReference.GetLength()) == 0) bResult = TRUE;

	strReference.ReleaseBuffer();
	strTarget.ReleaseBuffer();
	*/
	int nFind = strReference.Find(strTarget);
	if(nFind > -1)
		return TRUE;
	return FALSE;
	//return bResult;
}



time_t CTGSystemTime::ConvertTGSystemTimeToTime(SYSTEMTIME &tTime)
{
	tm time = {0,};

	time.tm_year = tTime.wYear-1900;
	time.tm_mon	 = tTime.wMonth-1;
	time.tm_mday = tTime.wDay;
	time.tm_hour = tTime.wHour;
	time.tm_min  = tTime.wMinute;
	time.tm_sec  = tTime.wSecond;

	return mktime(&time);
}

SYSTEMTIME CTGSystemTime::ConvertTGTimeToSystemTime(time_t &tTime)
{
	SYSTEMTIME tSYSTime;
	memset(&tSYSTime,0,sizeof(SYSTEMTIME));

	struct tm* time = localtime(&tTime);

	tSYSTime.wYear	= time->tm_year+1900;
	tSYSTime.wMonth = time->tm_mon+1;
	tSYSTime.wDay	= time->tm_mday;
	tSYSTime.wHour	= time->tm_hour;
	tSYSTime.wMinute= time->tm_min;
	tSYSTime.wSecond= time->tm_sec;

	return tSYSTime;
}

BOOL CTGSystemTime::Init()
{
	if(!SetTimezoneList())
		return FALSE;

	return Backup();
}

BOOL CTGSystemTime::Backup()
{
	if(!GetTimezone(m_tCurrentTimezoneInfo)) 
		return FALSE;

	return GetSystemTime(m_tCurrentUTCTime);
}

BOOL CTGSystemTime::Restore()
{
	if(m_TimezoneList.size() == 0) 
		return FALSE;
	if(!SetTimezone(m_tCurrentTimezoneInfo)) 
		return FALSE;
	if(!SetSystemTime(m_tCurrentUTCTime)) 
		return FALSE;

	return SyncTimeServer();
}

BOOL CTGSystemTime::GetSystemTime(SYSTEMTIME &tTime)
{
	memset(&tTime, 0, sizeof(SYSTEMTIME));
	::GetSystemTime(&tTime);
	return TRUE;
}

BOOL CTGSystemTime::SetSystemTime(SYSTEMTIME tTime)
{
	BOOL bResult = FALSE;

	if(!m_bSetTime)	m_bSetTime = TRUE;

	HANDLE hAccessToken = NULL;
	if(!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES|TOKEN_QUERY, &hAccessToken)) 
		return bResult;

	TOKEN_PRIVILEGES tTokenPrivileges;
	TCHAR tcTimeZoneName[] = _T("SeTimeZonePrivilege");
	if(!LookupPrivilegeValue(NULL, tcTimeZoneName, &tTokenPrivileges.Privileges[0].Luid)) 
	{
		CloseHandle(hAccessToken);
		hAccessToken = NULL;
		return bResult;
	}

	tTokenPrivileges.PrivilegeCount = 1;
	tTokenPrivileges.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	if(!AdjustTokenPrivileges(hAccessToken, FALSE, &tTokenPrivileges, 0, (PTOKEN_PRIVILEGES)NULL, 0)) 
	{
		CloseHandle(hAccessToken);
		hAccessToken = NULL;
		return bResult;
	}

	for(DWORD i=0;i<RETRY_COUNT;i++)
	{
		bResult = ::SetSystemTime(&tTime);
		if(!bResult)
		{
			//-- 2012-09-30 hongsu@esmlab.com
			//-- Retry 
			Sleep(1000);
			continue;
		}

		SYSTEMTIME tTemp = {0,};
		::GetSystemTime(&tTemp);
		DWORD dwDifference = abs((int)(ConvertTGSystemTimeToTime(tTime) - ConvertTGSystemTimeToTime(tTemp)));
		TGLog(3, _T("[SYSTEM TIME] Retry : %d"), i);
		TGLog(3, _T("[SYSTEM TIME] Set : %04d-%02d-%02d %02d:%02d:%02d"), tTime.wYear, tTime.wMonth, tTime.wDay, tTime.wHour, tTime.wMinute, tTime.wSecond);
		if(dwDifference <= DIFFERENCE_TIME) 
			break;
	}

	tTokenPrivileges.Privileges[0].Attributes = 0; 
	AdjustTokenPrivileges(hAccessToken, FALSE, &tTokenPrivileges, 0, (PTOKEN_PRIVILEGES)NULL, 0);

	PostMessage(NULL, WM_TIMECHANGE, 0, 0);
	
	CloseHandle(hAccessToken);

	return bResult;
}

BOOL CTGSystemTime::GetLocalTime(SYSTEMTIME &tTime)
{
	memset(&tTime, 0, sizeof(SYSTEMTIME));
	::GetLocalTime(&tTime);
	return TRUE;
}


#include <Windows.h>

BOOL CTGSystemTime::SetLocalTime(SYSTEMTIME tTime)
{
	BOOL bResult = FALSE;

	if(!m_bSetTime)	m_bSetTime = TRUE;

	HANDLE hAccessToken = NULL;
	if(!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES|TOKEN_QUERY, &hAccessToken)) 
		return bResult;

	TOKEN_PRIVILEGES tTokenPrivileges;
	TCHAR tcTimeZoneName[] = _T("SeTimeZonePrivilege");
	if(!LookupPrivilegeValue(NULL, tcTimeZoneName, &tTokenPrivileges.Privileges[0].Luid)) 
	{
		CloseHandle(hAccessToken);
		hAccessToken = NULL;
		return bResult;
	}

	tTokenPrivileges.PrivilegeCount = 1;
	tTokenPrivileges.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	if(!AdjustTokenPrivileges(hAccessToken, FALSE, &tTokenPrivileges, 0, (PTOKEN_PRIVILEGES)NULL, 0)) 
	{
		CloseHandle(hAccessToken);
		hAccessToken = NULL;
		return bResult;
	}

	for(DWORD i=0;i<RETRY_COUNT;i++)
	{
		bResult = ::SetLocalTime(&tTime);
		if(!bResult)
		{
			//-- 2012-09-30 hongsu@esmlab.com
			//-- Retry 
			Sleep(1000);
			continue;
		}

		SYSTEMTIME tTemp = {0,};
		::GetLocalTime(&tTemp);
		DWORD dwDifference = abs((int)(ConvertTGSystemTimeToTime(tTime) - ConvertTGSystemTimeToTime(tTemp)));
		TGLog(3, _T("[SYSTEM TIME] Retry : %d"), i);
		TGLog(3, _T("[SYSTEM TIME] Set : %04d-%02d-%02d %02d:%02d:%02d"), tTime.wYear, tTime.wMonth, tTime.wDay, tTime.wHour, tTime.wMinute, tTime.wSecond);
		if(dwDifference <= DIFFERENCE_TIME) 
			break;
	}

	tTokenPrivileges.Privileges[0].Attributes = 0; 
	AdjustTokenPrivileges(hAccessToken, FALSE, &tTokenPrivileges, 0, (PTOKEN_PRIVILEGES)NULL, 0);

	PostMessage(NULL, WM_TIMECHANGE, 0, 0);
	
	CloseHandle(hAccessToken);

	return bResult;
}
BOOL CTGSystemTime::GetTimezoneList(vector<CString> &TimezoneList)
{
	DWORD dwSize = m_TimezoneList.size();

	for(DWORD i=0;i<dwSize;i++)
	{
		TimezoneList.push_back(m_TimezoneList[i].strName);
	}

	return TRUE;
}

BOOL CTGSystemTime::GetTimezoneList(vector<TIMEZONE_INFO> &TimezoneList)
{
	DWORD dwSize = m_TimezoneList.size();

	for(DWORD i=0;i<dwSize;i++)
	{
		TimezoneList.push_back(m_TimezoneList[i]);
	}

	return TRUE;
}

DWORD CTGSystemTime::GetTimezoneIndex(CString strTimezoneInfo)
{
	DWORD dwSize = m_TimezoneList.size();
	//-- 2012-03-11 hongsu@esmlab.com
	//-- Remove 00:00
	int nFine = strTimezoneInfo.Find(')');
	CString strName;
	strName = strTimezoneInfo.Mid(nFine+1);

	for(DWORD i=0;i<dwSize;i++)
	{
		//if(StringCompare(m_TimezoneList[i].strName, strTimezoneInfo)) return i;
		if(StringFind(m_TimezoneList[i].strName, strName)) 
			return i;
		if(StringFind(m_TimezoneList[i].strName, strName)) 
			return i;
	}

	return -1;
}

TIME_ZONE_INFORMATION CTGSystemTime::GetTimezoneInfo(CString strTimezoneName)
{
	DWORD dwSize = m_TimezoneList.size();
	DWORD i=0;

	for(i=0;i<dwSize;i++)
	{
		if(StringFind(m_TimezoneList[i].strName, strTimezoneName)) 
			return m_TimezoneList[i].tInfo;
		if(StringFind(m_TimezoneList[i].strName, strTimezoneName)) 
			return m_TimezoneList[i].tInfo;
	}

	TIME_ZONE_INFORMATION tInfo = {0,};
	return tInfo;
}

BOOL CTGSystemTime::GetTimezone(DWORD &dwTimezoneListIndex)
{
	TIME_ZONE_INFORMATION tTimezoneInfo;
	if(!GetTimezone(tTimezoneInfo)) return FALSE;

	DWORD dwSize = m_TimezoneList.size();
	for(DWORD i=0;i<dwSize;i++)
	{
		if((wcscmp(tTimezoneInfo.DaylightName, m_TimezoneList[i].tInfo.DaylightName) == 0) &&
		   (wcscmp(tTimezoneInfo.StandardName, m_TimezoneList[i].tInfo.StandardName) == 0))
		{
			dwTimezoneListIndex = i;
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CTGSystemTime::SetTimezone(DWORD dwTimezoneListIndex)
{
	return SetTimezone(m_TimezoneList[dwTimezoneListIndex].tInfo);
}

BOOL CTGSystemTime::GetTimezone(TIME_ZONE_INFORMATION &tTimezoneInfo)
{
	DWORD dwResult = 0xFFFFFFFF;
	dwResult = GetTimeZoneInformation(&tTimezoneInfo);
	if((dwResult != TIME_ZONE_ID_UNKNOWN) && 
	   (dwResult != TIME_ZONE_ID_STANDARD) && 
	   (dwResult != TIME_ZONE_ID_DAYLIGHT)) return FALSE;

	return TRUE;
}

BOOL CTGSystemTime::GetTimezone(DWORD dwTimezoneListIndex, TIME_ZONE_INFORMATION &tTimezoneInfo)
{
	memcpy(&tTimezoneInfo, &m_TimezoneList[dwTimezoneListIndex].tInfo, sizeof(TIME_ZONE_INFORMATION));
	return TRUE;
}

BOOL CTGSystemTime::SetTimezone(TIME_ZONE_INFORMATION tTimezoneInfo)
{
	BOOL bResult = FALSE;

	if(!m_bSetTime)	m_bSetTime = TRUE;

	HANDLE hAccessToken = NULL;
	if(!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES|TOKEN_QUERY, &hAccessToken)) return bResult;

	TOKEN_PRIVILEGES tTokenPrivileges;
	TCHAR tcTimeZoneName[] = _T("SeTimeZonePrivilege");
	//-- 2012-1-26 hongsu.jung
	//-- TCHAR SE_TIME_ZONE_NAME[] = _T("SeTimeZonePrivilege");
	if(!LookupPrivilegeValue(NULL, tcTimeZoneName, &tTokenPrivileges.Privileges[0].Luid)) return bResult;

	tTokenPrivileges.PrivilegeCount = 1;
	tTokenPrivileges.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	if(!AdjustTokenPrivileges(hAccessToken, FALSE, &tTokenPrivileges, 0, (PTOKEN_PRIVILEGES)NULL, 0)) return bResult;

	if(!SetTimeZoneInformation(&tTimezoneInfo)) return bResult;

	tTokenPrivileges.Privileges[0].Attributes = 0; 
	bResult = AdjustTokenPrivileges(hAccessToken, FALSE, &tTokenPrivileges, 0, (PTOKEN_PRIVILEGES)NULL, 0);

	CloseHandle(hAccessToken);

	return bResult;
}

BOOL CTGSystemTime::GetDaylightSavingTime()
{
	BOOL bResult = FALSE;

	TIME_ZONE_INFORMATION tTimezoneInfo;
	if(!GetTimezone(tTimezoneInfo)) return bResult;

	if(((tTimezoneInfo.DaylightDate.wYear != 0) || (tTimezoneInfo.DaylightDate.wMonth != 0) || (tTimezoneInfo.DaylightDate.wDay != 0) ||
		(tTimezoneInfo.DaylightDate.wHour != 0) || (tTimezoneInfo.DaylightDate.wMinute != 0) || (tTimezoneInfo.DaylightDate.wSecond != 0)) ||
	   ((tTimezoneInfo.StandardDate.wYear != 0) || (tTimezoneInfo.StandardDate.wMonth != 0) || (tTimezoneInfo.StandardDate.wDay != 0) ||
		(tTimezoneInfo.StandardDate.wHour != 0) || (tTimezoneInfo.StandardDate.wMinute != 0) || (tTimezoneInfo.StandardDate.wSecond != 0))) bResult = TRUE;

	return bResult;
}

BOOL CTGSystemTime::SetDaylightSavingTime(BOOL bEnable, DWORD dwTimezoneListIndex)
{
	BOOL bResult = FALSE;
	//-- 2012-10-07 hongsu@esmlab.com
	//-- Exception
	if(dwTimezoneListIndex < 0 || dwTimezoneListIndex == 0xffffffff)
		return bResult;

	TIME_ZONE_INFORMATION tTimezoneInfo;
	memcpy(&tTimezoneInfo, &m_TimezoneList[dwTimezoneListIndex].tInfo, sizeof(TIME_ZONE_INFORMATION));
	
	if(!bEnable)
	{
		tTimezoneInfo.DaylightDate.wYear = 0;
		tTimezoneInfo.DaylightDate.wMonth = 0;
		tTimezoneInfo.DaylightDate.wDay = 0;
		tTimezoneInfo.DaylightDate.wHour = 0;
		tTimezoneInfo.DaylightDate.wMinute = 0;
		tTimezoneInfo.DaylightDate.wSecond = 0;
		tTimezoneInfo.StandardDate.wYear = 0;
		tTimezoneInfo.StandardDate.wMinute = 0;
		tTimezoneInfo.StandardDate.wDay = 0;
		tTimezoneInfo.StandardDate.wHour = 0;
		tTimezoneInfo.StandardDate.wMinute = 0;
		tTimezoneInfo.StandardDate.wSecond = 0;
	}

	return SetTimezone(tTimezoneInfo);
}

BOOL CTGSystemTime::SyncTimeServer()
{
	BOOL bResult = FALSE;
	WSADATA wsaData;
	if(WSAStartup(MAKEWORD(1,1), &wsaData) != 0)
		return bResult;

	CTGSNTPClient Sntp;
	NtpServerResponse tServerResponse;

	CString strUrl = _T("time.kriss.re.kr");
	CString strUrl2 = _T("time2.kriss.re.kr");
	if((!Sntp.GetServerTime(strUrl, tServerResponse)) && (!Sntp.GetServerTime(strUrl2, tServerResponse)))
	{
		WSACleanup();
		return bResult;
	}

	CNtpTime newTime(CNtpTime::GetCurrentTime() + tServerResponse.m_LocalClockOffset); 
	bResult = Sntp.SetClientTime(newTime);

	WSACleanup();

	return bResult;
}

BOOL CTGSystemTime::GetSmileTime(SYSTEMTIME &tTime)
{
	DWORD dwTickDiff;
	DWORD dwTick;
	time_t tSMileTime;

	memset(&tTime, 0, sizeof(SYSTEMTIME));
	
	dwTick = GetTickCount();

	dwTickDiff = dwTick-m_dwSmileStartTick;

	dwTickDiff = dwTickDiff/1000;

	tSMileTime = m_tSmileStartTime + dwTickDiff;


	tTime = ConvertTGTimeToSystemTime(tSMileTime);
	return TRUE;
}

time_t CTGSystemTime::GetSmileTime()
{
	DWORD dwTickDiff;
	DWORD dwTick;
	time_t tSMileTime;
	
	dwTick = GetTickCount();

	dwTickDiff = dwTick-m_dwSmileStartTick;

	dwTickDiff = dwTickDiff/1000;

	tSMileTime = m_tSmileStartTime + dwTickDiff;

	return tSMileTime;
}
