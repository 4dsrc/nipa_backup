// ESMMovieMgr.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMMakeMovieMgr.h"
#include "MainFrm.h"

// CESMMovieMgr

IMPLEMENT_DYNCREATE(CESMMakeMovieMgr, CWinThread)

CESMMakeMovieMgr::CESMMakeMovieMgr()
{
	m_bThreadStop = FALSE;
	m_bWait = FALSE;
}

CESMMakeMovieMgr::~CESMMakeMovieMgr()
{
}

BOOL CESMMakeMovieMgr::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CESMMakeMovieMgr::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	return CWinThread::ExitInstance();
}

int CESMMakeMovieMgr::Run()
{
	ESMEvent* pMsg	= NULL;
	while(!m_bThreadStop)
	{
		Sleep(1);
		int nAll = m_arMsg.GetSize();

		while(nAll--)
		{
			Sleep(1);
			if(m_bWait)
			{
				nAll++;
				continue;
			}

			pMsg = (ESMEvent*)m_arMsg.GetAt(0);

			if(!pMsg)
			{
				m_arMsg.RemoveAt(0);
				continue;
			}

			m_arMsg.RemoveAt(0);

			switch(pMsg->message)
			{
			case WM_ESM_MOVIE_MAKE_ARRAY_MERGE:
				{
					CMainFrame* pMain = (CMainFrame*)pMsg->pParam;
					pMain->m_wndTimeLineEditor.m_pTimeLineView->MergeArrayMovie();
				}
				break;
			case WM_ESM_MOVIE_MAKE_ARRAY_ADD:
				{
					/*m_bWait = TRUE;
					CMainFrame* pMain = (CMainFrame*)pMsg->pParam;
					pMain->m_wndTimeLineEditor.VmccTamplate( pMsg->nParam2, pMsg->nParam3, pMsg->nParam1);
					pMain->m_wndTimeLineEditor.OnDSCMakeMovie();
					pMain->m_pImgLoader->AllClearImageList();*/

					m_bWait = TRUE;
					ESMEvent* pMovieMsg	= new ESMEvent;
					pMovieMsg->message = WM_ESM_MOVIE_MAKE_THREAD_RUN;
					pMovieMsg->nParam1 = pMsg->nParam1;	// Template
					pMovieMsg->nParam2 = pMsg->nParam2;	// Total RecTime
					pMovieMsg->nParam3 = pMsg->nParam3;	// Select Time

					ESMLog(5, _T("#######total Time = %d, Select Time = %d"), pMovieMsg->nParam2, pMovieMsg->nParam3);
					CMainFrame* pMain = (CMainFrame*)pMsg->pParam;
					//::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);	
					::SendMessage(pMain->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMovieMsg);	
				}
				break;
			}
		}
	}

	return 0;
}

void CESMMakeMovieMgr::AddMsg(ESMEvent* pMsg)
{
	if(!m_bThreadStop)
	{
		m_arMsg.Add((CObject*)pMsg);
	}
}

BEGIN_MESSAGE_MAP(CESMMakeMovieMgr, CWinThread)
END_MESSAGE_MAP()


// CESMMovieMgr 메시지 처리기입니다.
