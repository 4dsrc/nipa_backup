////////////////////////////////////////////////////////////////////////////////
//
//	ESMESMDirRead.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-06-20
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
using namespace std;

class CESMDirRead  
{
public:
	CESMDirRead();
	virtual ~CESMDirRead();
	
	bool	GetDirs(CString strFolder, bool bRecurse);
	bool	GetFiles(CString strFileMask, bool bIncludeFilesInFileList = true, bool bIncludeFoldersInFileList = false);
	// remove all entries
	bool	ClearDirs();
	bool	ClearFiles();
	// if you want to sort the data
	enum {eSortNone, eSortAlpha, eSortWriteDate, eSortSize};
	bool	SortFiles(int iSortStyle, bool bReverse);
	
	// directories
	struct CDirEntry
	{
		CDirEntry()	{}
		CDirEntry(const CString &s)
		{
			m_sName = s;
		}
		CString m_sName;
	};
	
	typedef vector<CDirEntry> DirVector;
	DirVector &Dirs()	{return m_dirs;}
	// files
#ifdef USE_WIN32_FINDFILE
	struct CFileEntry
	{
		CString	m_sName;
		bool     bIsFolder;
		unsigned int attrib;
		unsigned __int64 time_create;
		unsigned __int64 time_access;
		unsigned __int64 time_write;
		unsigned __int64 size;
	};
#else
	struct CFileEntry
	{
		CString	m_sName;
		bool     bIsFolder;
		unsigned int attrib;
		time_t time_create;
		time_t time_access;
		time_t time_write;
		_fsize_t size;
		
	};
#endif
	
	typedef vector<CFileEntry> FileVector;
	
	FileVector &Files()	{return m_files;}
	protected:
		
		bool	GetSubDirs(DirVector &dir_array, const CString &path);
		UINT	FindFiles(const CString & dir, const CString & filter, bool bIncludeFilesInFileList, bool bIncludeFoldersInFileList);
		void	FormatPath(CString &path);
		
		CString	m_sSourceDir;
		
		DirVector m_dirs;
		FileVector m_files;
};


