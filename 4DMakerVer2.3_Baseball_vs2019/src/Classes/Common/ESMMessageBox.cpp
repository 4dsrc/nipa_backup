////////////////////////////////////////////////////////////////////////////////
//
//	ESMMessageBox.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-08-02
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMMessageBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Include this file & insert 'msimg32.lib' into project!!
#include <wingdi.h>

#define	TIMER_WAIT_SEC		0x01
#define	TIMER_DRAW_MSG		0x02
#define	TIMER_HIDE_MSG		0x03

#define TOP_MARGIN			10
#define MIDDLE_MARGIN		5
#define SIDE_MARGIN			15

#define COLOR_TIME			RGB(70,70,70)
#define COLOR_TEXT			RGB(10,10,10)

/////////////////////////////////////////////////////////////////////////////
// CESMMessageBox dialog
CESMMessageBox::CESMMessageBox(CWnd* pParent /*=NULL*/)
	: CDialog(CESMMessageBox::IDD, pParent)
{	
	m_nCount		= 0;
	m_nWaitDraw		= DEFAULT_WAIT_TIME;
	m_nAlphaDelay	= 7;	
	m_nDraw			= 0;
	m_nHide			= 0;
}

void CESMMessageBox::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CESMMessageBox, CDialog)
	//{{AFX_MSG_MAP(CESMMessageBox)
	ON_WM_TIMER()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CESMMessageBox::OnEraseBkgnd(CDC* pDC) { return TRUE;}
void CESMMessageBox::Create(CWnd *pParent)	{ CDialog::Create(CESMMessageBox::IDD, pParent);}

//------------------------------------------------------------------------------
//! @function	Load Splash Bitmap & Initialize BLENDFUNCTION structure			
//! @brief				
//! @date		2012-08-02
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return		BOOL	
//! @revision		
//------------------------------------------------------------------------------
BOOL CESMMessageBox::OnInitDialog() 
{
	CDialog::OnInitDialog();

	//-- 2012-08-02 hongsu@esmlab.com
	//-- Set Default
	m_bf.BlendOp = AC_SRC_OVER;
	m_bf.BlendFlags = 0;
	m_bf.AlphaFormat = AC_SRC_OVER;  // use source alpha  
	m_bf.SourceConstantAlpha = 0xff;  // opaque (disable constant alpha) 

	m_bitmap.LoadBitmap(IDB_ESM_MESSAGEBOX);
	BITMAP BitMap;
	m_bitmap.GetBitmap(&BitMap);
	m_nWidth = BitMap.bmWidth;
	m_nHeight = BitMap.bmHeight;
	
	//-- 2012-08-02 hongsu@esmlab.com
	//-- Default Bitmap Size
	//-- Change Up to Text Size
	SetWindowPos(&wndTopMost, 0, 0, m_nWidth, m_nHeight, SWP_NOMOVE|SWP_NOZORDER|SWP_NOREDRAW );
	m_nCount = 0;

	//-- 2012-08-02 hongsu@esmlab.com
	//-- Move MessageBox to Center 
	CenterWindow();
	return TRUE;  
}

//------------------------------------------------------------------------------
//! @function	Show Message With Timer
//! @brief				
//! @date		2012-08-02
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return		none	
//! @revision		
//------------------------------------------------------------------------------
void CESMMessageBox::OnShowMessage(CString strComment, int nStartGap)
{
	m_nCount	 = 0;
	m_strComment = strComment;
	m_nWaitDraw	 = nStartGap;
	
	//-- 2012-08-02 hongsu@esmlab.com
	//-- Set Start Time
	_ftime(&m_tStart);
	::SetTimer(m_hWnd, TIMER_WAIT_SEC, m_nWaitDraw, NULL);	
}

//------------------------------------------------------------------------------
//! @function	Hide Message With Timer
//! @brief				
//! @date		2012-08-02
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return		none	
//! @revision		
//------------------------------------------------------------------------------
void CESMMessageBox::OnHideMessage()
{
	//-- 2012-08-02 hongsu@esmlab.com
	//-- Check Draw Timer Kill
	KillTimer(TIMER_DRAW_MSG);
	m_nCount = 0;
	::SetTimer(m_hWnd, TIMER_HIDE_MSG, m_nAlphaDelay, NULL);	
}
//------------------------------------------------------------------------------
//! @function	Show Splash Bitmap by growing 'SourceConstantAlpha' value		
//! @brief				
//! @date		2012-08-02
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return		none	
//! @revision		
//------------------------------------------------------------------------------
//-- 2013-05-01 hongsu@esmlab.com
//-- void CESMMessageBox::OnTimer(UINT nIDEvent) 
/*
	error C2440: 'static_cast' :
	cannot convert from 'void (__cdecl CPortScanDlg::* )(UINT)' to
	'void (__cdecl CWnd::* )(UINT_PTR)'
	1> Cast from base to derived requires dynamic_cast or static_cast
*/
void CESMMessageBox::OnTimer(UINT_PTR nIDEvent) 
{
	switch(nIDEvent)
	{
	case TIMER_WAIT_SEC:
		KillTimer(TIMER_WAIT_SEC);
		CenterWindow();
		ShowWindow(SW_SHOW);
		::SetTimer(m_hWnd, TIMER_DRAW_MSG, m_nAlphaDelay, NULL);
		break;
	case TIMER_DRAW_MSG:
		DrawMessageBox();
		break;
	case TIMER_HIDE_MSG:
		HideMessageBox();
		break;
	}
	CDialog::OnTimer(nIDEvent);
}

//------------------------------------------------------------------------------
//! @function	Show Splash Bitmap by growing 'SourceConstantAlpha' value		
//! @brief				
//! @date		2012-08-02
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return		none	
//! @revision		
//------------------------------------------------------------------------------
void CESMMessageBox::DrawMessageBox()
{
	if (m_nDraw >= 0xff)
	{
		m_nDraw = 0xff;
		m_bf.SourceConstantAlpha = m_nDraw;
	}
	else
	{
		//-- Set Alpha
		m_nCount += 1;
		m_nDraw += (m_nCount/100);
		m_bf.SourceConstantAlpha = m_nDraw;
	}

	CClientDC dc(this);	
	CDC  dcMem;
	CBitmap *pOldBitmap = NULL;
	CRect rtBK;
	
	//-- Set DC 
	dcMem.CreateCompatibleDC(&dc);		
	//-- Draw String 
	DrawString(&dcMem, pOldBitmap, &rtBK);		

	//-- AlphaBlend 
	AlphaBlend(dc, 0, 0, rtBK.Width(), rtBK.Height(), dcMem, 0,0,rtBK.Width(), rtBK.Height(),m_bf);		
	//- 2012-08-02
	//TRACE(_T("Show [%d]\n"),m_bf.SourceConstantAlpha);
	dcMem.SelectObject(pOldBitmap);
	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	dcMem.DeleteDC();
}


//------------------------------------------------------------------------------
//! @function	Show Splash Bitmap by growing 'SourceConstantAlpha' value		
//! @brief				
//! @date		2012-08-02
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return		none	
//! @revision		
//------------------------------------------------------------------------------
void CESMMessageBox::HideMessageBox()
{
	if (m_nHide > 255)
	{
		m_nHide = 0; 
		m_nDraw = 0;
		KillTimer(TIMER_HIDE_MSG);
		ShowWindow(SW_HIDE);
		return;
	}
	CClientDC dc(this);	
	CDC  dcMem;
	CBitmap *pOldBitmap = NULL;
	CRect rtBK;

	//-- Set Alpha
	m_nCount += 1;
	m_nHide += 2;
	m_bf.SourceConstantAlpha = 0xff - m_nHide;

	//-- Set DC 
	dcMem.CreateCompatibleDC(&dc);		
	//-- Draw String 
	DrawString(&dcMem, pOldBitmap, &rtBK);
	
	//-- 2012-08-07
	//-- joonho.kim
	FillRect( dc , rtBK , WHITE_BRUSH );
	//- 2012-08-02
	//TRACE(_T("Show [%d]\n"),m_bf.SourceConstantAlpha);
	AlphaBlend(dc, 0, 0, rtBK.Width(), rtBK.Height(), dcMem, 0,0,rtBK.Width(), rtBK.Height(),m_bf);	
	dcMem.SelectObject(pOldBitmap);	
	dcMem.DeleteDC();
}

//------------------------------------------------------------------------------
//! @function	Show Splash Bitmap by growing 'SourceConstantAlpha' value
//! @brief
//! @date		2012-08-02
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return		int
//! @revision
//------------------------------------------------------------------------------
int CESMMessageBox::GetTimeGap()
{	
	int nGap, nSec, nMilliSec;
	struct _timeb timebuffer;	
	_ftime(&timebuffer);
	nSec = (int)(timebuffer.time - m_tStart.time);
	nMilliSec = timebuffer.millitm - m_tStart.millitm;

	nGap = nSec*1000 + nMilliSec;
	return nGap;
}

//------------------------------------------------------------------------------
//! @function	Draw Text And Timestamps
//! @brief				
//! @date		2012-08-02
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return		null	
//! @revision		
//------------------------------------------------------------------------------
void CESMMessageBox::DrawString(CDC* pDC, CBitmap* pBmp, CRect* pRtBK)
{
	//-- 2012-08-02 hongsu@esmlab.com
	//-- Get BITMAP 
	CString strTime;
	CSize szText, szTime; 
	CRect rtText, rtTime;
	CBitmap bitmap;
	HANDLE hNewBitmap = (HBITMAP) CopyImage(m_bitmap.m_hObject,IMAGE_BITMAP,0,0,0);
	bitmap.Attach(hNewBitmap);
	pBmp = pDC->SelectObject(&bitmap);
	
	//-- 2012-08-02 hongsu@esmlab.com
	//-- Get Size 
	pDC->SetBkMode(TRANSPARENT);	
	szText = pDC->GetTextExtent(m_strComment);

	//-- Get Time
	int nGap = GetTimeGap();
	strTime.Format(_T("%02d:%03d"),nGap/1000, nGap%1000);
	szTime = pDC->GetTextExtent(strTime);

	if(szText.cx < szTime.cx)
		szText.cx = szTime.cx;

	//-- Set Test Rect 
	rtText.top		= TOP_MARGIN;
	rtText.bottom	= TOP_MARGIN + szText.cy + MIDDLE_MARGIN;// + szTime.cy + TOP_MARGIN ;
	rtText.left		= SIDE_MARGIN;
	rtText.right	= SIDE_MARGIN + szText.cx + SIDE_MARGIN;

	pRtBK->top		= 0;
	pRtBK->left		= 0;
	pRtBK->bottom	= rtText.bottom + szTime.cy + TOP_MARGIN ;
	pRtBK->right	= rtText.right + 2*SIDE_MARGIN;

	rtTime.top		= rtText.bottom;
	rtTime.bottom	= pRtBK->bottom;
	rtTime.left		= rtText.right - szTime.cx;	
	rtTime.right	= rtText.right;


	//-- Draw Message 
	pDC->SetTextColor(COLOR_TEXT);
	pDC->DrawText(m_strComment	, m_strComment.GetLength()	, &rtText, DT_CENTER );
	
	//-- Draw Time
	pDC->SetTextColor(COLOR_TIME);
	pDC->DrawText(strTime		, strTime.GetLength()		, &rtTime, DT_RIGHT );
}