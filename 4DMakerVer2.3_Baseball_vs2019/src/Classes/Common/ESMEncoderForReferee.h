extern"C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/mem.h>
#include <libavutil/opt.h>

#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
};
#include <atlstr.h>

#include "ESMFunc.h"
#include "ESMUtil.h"
#include "ESMm3u8.h"

class CESMEncoderForReferee
{
public:
	enum ENCODING_SIZE
	{
		ENCODING_SIZE_HD,
		ENCODING_SIZE_FHD,
		ENCODING_SIZE_UHD
	};
public:
	CESMEncoderForReferee(
		CString strInputFileName, 
		CString strOutputFilePath,
		ENCODING_SIZE nEncodingSize);
	~CESMEncoderForReferee();

public:
	int Run();
	int Run_I();
	void SetM3u8(CESMm3u8 * pData) { m_m3u8 = pData;}
private:
	CString m_StrOutputFilePath;

	CString m_strInputFileName;
	CString m_strOutputFileName;

	int m_nResizeWidth;
	int m_nResizeHeight;

	int m_nResizeJPEGWidth;
	int m_nResizeJPEGHeight;

	HANDLE m_hTSEncodingThreadHandle;
	HANDLE m_hJPEGEncodingThreadHandle;

	BOOL m_bIsTSEncodingDone;
	BOOL m_bIsJPEGEncodingDone;

	CESMm3u8 * m_m3u8;
private:
	static unsigned WINAPI EncodingMp4ToTs(LPVOID param);
	static unsigned WINAPI EncodingMp4ToTs_I(LPVOID param);
	static unsigned WINAPI EncodingMp4To3JPEGs(LPVOID param);
	int EncodingMp4To3JPEGs();

	CString GetFileNameMp4ToTs();
	CString GetFileNameMp4ToJPEG(int nIndex);

	void SetEncodingSize(ENCODING_SIZE nEncodingSize);
	double GetMuxDelay();
	
};
