#pragma once
#include "resource.h"
#include "StopWatch.h"
// CTimeViewDlg 대화 상자입니다.

class CTimeViewDlg : public CDialog
{
	DECLARE_DYNAMIC(CTimeViewDlg)

public:
	CTimeViewDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTimeViewDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TIMEVIEWDLG };
public:
	int m_nStartTime;
	BOOL m_bTestMode;
	int m_nEndTime;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	CString m_strSeq1;
	CString m_strSeq2;
	CString m_strTime;
	CString m_strStartTime;
	CTime m_StartTime;
	CStopWatch m_StopWatch;
	BOOL m_bThread;
	CWinThread *m_pWThd;

	DECLARE_MESSAGE_MAP()
public:
	void Create(CWnd *pParent);	
	void SetSeq1(CString strSeq1);
	void SetSeq2(CString strSeq2);
	void SetEndTime(int nTime);
	void InitTime();
	void EndTime();
	void UpdateTime();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg LRESULT OnNcHitTest(CPoint point);
	static UINT TimeUpdate(LPVOID pParam);
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	virtual BOOL DestroyWindow();
};
