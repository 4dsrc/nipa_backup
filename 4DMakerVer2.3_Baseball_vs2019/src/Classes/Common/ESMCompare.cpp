////////////////////////////////////////////////////////////////////////////////
//
//	TGCompare.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-24
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TGCompare.h"


CTGCompare::CTGCompare()	
{
	m_strDest		= _T("");
	m_strReference	= _T("");
	m_strGroup		= _T("");

	m_nCompare		= COMPARE_OPTION_UNKNOWN;
	m_bExpect		= TRUE;
	m_bResult		= FALSE;
}

CTGCompare::~CTGCompare()	{}

void CTGCompare::SetCompare(CString strCompare)
{
	for(int i = 1 ; i < COMPARE_OPTION_ALL ; i ++ )
	{
		if(g_arCompareOption[i] == strCompare)
		{
			m_nCompare = i;
			return;
		}
	}
	m_nCompare = COMPARE_OPTION_UNKNOWN;
}
void CTGCompare::SetExpect(CString strExpect)
{
	//strExpect.MakeUpper();
	for(int i = 0 ; i < ARRAY_SIZE(g_arCompareResult) ; i ++ )
	{
		if(g_arCompareResult[i] == strExpect)
		{
			m_bExpect = i;
			return;
		}
	}
	m_bExpect = TRUE;
}

BOOL CTGCompare::DoCompare(CString strTarget)
{
	m_bResult = FALSE;
	switch(m_nCompare)
	{
	case COMPARE_OPTION_INCLUDE:	// Check include
		{
			int nFind = 0;
			nFind = strTarget.Find(m_strReference);
			if(nFind > TG_UNKNOWN)
				m_bResult = TRUE;
		}
		break;
	case COMPARE_OPTION_EXCLULDE:
		{
			int nFind = 0;
			nFind = strTarget.Find(m_strReference);
			if(nFind == TG_UNKNOWN)
				m_bResult = TRUE;
		}
		break;
	case COMPARE_OPTION_EQUAL:
		{
			if(m_strReference == strTarget)
				m_bResult = TRUE;
		}
		break;
	case COMPARE_OPTION_NOT_EQUAL:
		{
			if(m_strReference != strTarget)
				m_bResult = TRUE;
		}
		break;
	}

	if(m_bResult)
		TGLog(1,_T("[Check Detection] Option: %s / Compare: %s / Destination: %s"),g_arCompareOption[m_nCompare], m_strReference, strTarget);
	return m_bResult;
}

BOOL CTGCompare::DoReturn()
{
	if(m_bExpect == m_bResult)
		return TRUE;
	return FALSE;
}