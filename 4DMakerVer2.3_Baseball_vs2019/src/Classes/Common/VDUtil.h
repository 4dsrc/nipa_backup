////////////////////////////////////////////////////////////////////////////////
//
//	VDUtil.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once

#define MAC_ADDR_CNT	6

class VDUtil
{

public:
	VDUtil(void);
	~VDUtil(void);

	static BOOL GetLineInFile(CFile *fp, CStringArray* pArStr);
	static void	GetParamInLine(char*  strToken, const char*  strDelimit, CStringArray* pArStr);	
	static void	ReplaceFileDelimit(CString strOrgin, CString  strConvertPath);
	//	[3/30/2009 Hongsu.Jung]	
	static CString	GetFileName(CString strFile);
	static CString	GetFolder(CString strFolderName);
	static CString	GetFileID(CString strFileName);
	static CString	GetFolderID(CString strFolderName);	
	static CString	ReplacePathSymbol(CString strSource, CString strSymbol, CString strPath);
	static void		StringToken(const CString& strSource, CStringArray* pArStr, const CString& delimiters);

	static CString	Trim(CString strSource);
	static CString	itoa(int value, unsigned int base = 10);    
	static CString  itocs(const int value, const unsigned int base = 10);
	//-- 2012-06-20 hongsu@esmlab.com
	static int wtoi(CString str16Value);
	static CString	itow(int nVal16, BOOL bTag = FALSE);
	//-- 2012-07-16 hongsu@esmlab.com	
	static char* GetCharString(CString str);
	static CString GetStrChar(char* ch, BOOL bDelete = TRUE);
	//-- 2012-06-15 hongsu@esmlab.com	 
	static CString  I2S(int value);
	static CString  F2S(float value);
	static CString  F2S(float value, int nPointCnt);
	//-- 2011-6-22 keunbae.song
	static void PrintToFile(const TCHAR* strPath, const TCHAR* format, ...);
	//-- 2012-4-16 keunbae.song
	static DWORD ReadFileEx(HANDLE hFile, BYTE* pBuffer, DWORD nNumberOfBytesToRead);
	//-- 2012-06-13 hongsu@esmlab.com
	static CString GetLocalIPAddress();
	static CString GetLocalMacAddress();
	static CString GetLocalHostname();
	//-- 2012-06-20 hongsu@esmlab.com
	static CString GetDate();
	static CString GetTime();
	static CString GetGraphTime();
	//-- 2012-06-25 hongsu@esmlab.com
	static void GetMac(CString strMac, DWORD* dMac);
	static void GetMac(CString strMac, BYTE* btMac);
	static void MultiByteToWideChar(char *cBuffer, wchar_t *strBuffer);
	//-- 2012-06-26 cy.gil@esmlab.com use by Agent
	static void WideCharToMultiByte(wchar_t *strBuffer, char *cBuffer);
	static CString GetBoolToStr(BOOL b);
	static BOOL GetBoolFromStr(CString str);
	
};

