////////////////////////////////////////////////////////////////////////////////
//
//	TGStringUtil.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TGStringUtil.h"

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2012-3-23
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
CTGStringUtil::CTGStringUtil()
{
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2012-3-23
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
CTGStringUtil::~CTGStringUtil()
{
}

//------------------------------------------------------------------------------ 
//! @brief    Converts a sequence of multibyte characters to a corresponding 
//            sequence of wide characters.
//! @date     2012-3-23
//! @owner    keunbae.song (kobysong@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
CString CTGStringUtil::MbsToWcs(const char* pmbstring)
{
	if(NULL == pmbstring)
		return _T("");

	int length = (int)strlen(pmbstring);

	WCHAR* pwcstring = new WCHAR[length+1];
	wmemset(pwcstring, 0, length+1);
	
	mbstowcs(pwcstring, pmbstring, length);

	CString result = pwcstring;

	delete[] pwcstring;
	pwcstring = NULL;
	
	return result;
}

//------------------------------------------------------------------------------ 
//! @brief    Converts a sequence of wide characters to a corresponding sequence
//            of multibyte characters
//! @date     2012-3-23
//! @note     keunbae.song (kobysong@esmlab.com)
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
BOOL CTGStringUtil::WcsToMbs(CString wcstring, char* pmbstring)
{
	if(NULL == pmbstring)
		return FALSE;

	int length = wcstring.GetLength();
	if(0 == length)
		return FALSE;

	strset(pmbstring, 0);
	wcstombs(pmbstring, wcstring, length); 

	return TRUE;
}