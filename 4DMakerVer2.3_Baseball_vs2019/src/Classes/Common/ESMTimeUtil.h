////////////////////////////////////////////////////////////////////////////////
//
//	TGTimeUtil.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
class CTGTimeUtil
{
	CTGTimeUtil();
	~CTGTimeUtil();

public:
	static DWORD SystemTimeToTick(IN UINT year, IN UINT mon, IN UINT day, IN UINT hour, IN UINT min, IN UINT sec);
	static SYSTEMTIME TickToSystemTime(IN DWORD tick);
	static CString TicktoSystemTime(IN DWORD tick);

	
};