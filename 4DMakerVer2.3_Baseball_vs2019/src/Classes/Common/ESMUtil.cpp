////////////////////////////////////////////////////////////////////////////////
//
//	ESMUtil.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMUtil.h"

//-- 2011-06-14
//-- time measure
#include <sys/timeb.h>
#include <time.h>

#include "afxsock.h"


//-- 2012-06-13 hongsu@esmlab.com
//-- For Get IP, Mac Address 
#pragma comment(lib, "Iphlpapi.lib")
#pragma comment(lib, "Version.lib")
#include <Iptypes.h>
#include <Iphlpapi.h>

#define TC_SEPARATOR				_T(";")
#define COUNT( VALUE ) sizeof(VALUE)/sizeof(VALUE[0])
#define	 LEAPYEAR(YEAR)					((YEAR % 4 == 0 && YEAR % 100 != 0) || (YEAR % 400 == 0))

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


ESMUtil::ESMUtil(void){}
ESMUtil::~ESMUtil(void){}

bool ESMUtil::GetLineInFile(CFile *fp, LPSTRVECTOR vecOut)
{
	if(!vecOut->empty())
	{
		printf("LPSTRVECTOR vecOut is not empty.\n");
	}
	ULONGLONG ulFileSize = fp->GetLength();
	if(!ulFileSize)
	{
		printf("File Size is zero\n");
		return false;
	}

	fp->SeekToBegin();

	//
	//- GET LINE, getString
	//	
	char cReadChar;
	int  iReadBufIndex = 0;
	int iSizeCount = 0;
	bool isNotes = false;

	char readBuff[1000];	// command length is limited 1000 char.
	while( fp->Read(&cReadChar, 1) )
	{
		iSizeCount++;

		// if notes, ignore cmd line.
		if(cReadChar == '#' && iReadBufIndex == 0)
		{
			isNotes =true;
		}
		else if( cReadChar == '/' && iReadBufIndex == 0)
		{	
			readBuff[iReadBufIndex] = cReadChar;	
			iReadBufIndex++;

			fp->Read(&cReadChar, 1);
			if( cReadChar == '/')
				isNotes = true;
		}
		else if(cReadChar == '\r')
		{
			//do nothing. skip parse.			
		}
		//-- Load 1 Line
		else if(cReadChar == '\n')		// if encount '\n'
		{				
			readBuff[iReadBufIndex++] = '\0';
			iReadBufIndex = 0;

			if(!isNotes)	// if notes. copy string. and return.
			{	
#ifdef UNICODE
				TCHAR pchCmd[1000] = {0};
				mbstowcs(pchCmd, readBuff, strlen(readBuff));
				vecOut->push_back(pchCmd);
#else
				char* strCmd = new char[strlen(readBuff)+1]; 
				strcpy(strCmd, readBuff);  
				vecOut->push_back(strCmd);
				delete strCmd;
				strCmd = NULL;
#endif		
			}
			else
				isNotes = false;
		}
		//-- Load the end of Line
		else if(iSizeCount == ulFileSize)	// if encount EOF
		{
			readBuff[iReadBufIndex++] = cReadChar;				
			readBuff[iReadBufIndex] = '\0';

			if(!isNotes)	// if notes. do not CreatTestStep.
			{	

#ifdef UNICODE
				TCHAR pchCmd[1000] = {0};
				mbstowcs(pchCmd, readBuff, strlen(readBuff));
				vecOut->push_back(pchCmd);
#else
				char* strCmd = new char[strlen(readBuff)+1]; 
				strcpy(strCmd, readBuff);  
				vecOut->push_back(strCmd);
				delete strCmd;
				strCmd = NULL;
#endif		
			}
			else
				isNotes = false;
		}
		else
		{
			readBuff[iReadBufIndex] = cReadChar;	
			iReadBufIndex++;
		}	
	}

	return true;
}

void ESMUtil::GetParamInLine(char* strToken, const char* strDelimit, LPSTRVECTOR vecOut)
{
	char* token;

	token = strtok( strToken, strDelimit );		//Get Type
	while( token != NULL )
	{
#ifdef UNICODE
		TCHAR pchToken[2048] = {0};
		mbstowcs(pchToken, token, strlen(token));
		vecOut->push_back(pchToken);
#else
		vecOut->push_back(token);
#endif		

		//vecOut->push_back(token);
		if(*token =='#')	//if encount comment. don't push_back
			break;
		if(*token =='/' && *(token+1) =='/')	//if encount comment. don't push_back
			break;

		token = strtok(NULL,strDelimit);		 
	}
}


void ESMUtil::RemoveStrVector(LPSTRVECTOR strVector)
{	
	while(!strVector->empty())
		strVector->pop_back();
	strVector->clear();			
}

void ESMUtil::ReplaceFileDelimit(tstring strOrgin, tstring  strConvertPath)
{	
	unsigned int nIndex = 0;

	for(unsigned int i = 0; i < strOrgin.length(); i++)
	{
		if( strOrgin[i] == '/')		
		{
			strConvertPath[nIndex++] = '\\';	
			strConvertPath[nIndex++] = '\\';	

		}
		else	
			strConvertPath[nIndex++] = strOrgin[i]; 
	}
	strConvertPath[nIndex] = '\0';
}

tstring  ESMUtil::GetFileName(tstring strFile)
{	
	tstring  strFileName;	
	strFile = ReplaceString(strFile, _T("/"), _T("\\"));

	//-- SET FILE NAME
	tstring::size_type	posLastDelimeter = strFile.find_last_of ('\\');
	tstring::size_type	posStringEnd	= strFile.size();

	strFileName = strFile.substr(posLastDelimeter+1, posStringEnd - posLastDelimeter);
	//strFileName .memmove(strFileName, strFileName+1, strlen(strFileName));
	return strFileName;
}

tstring  ESMUtil::GetFileID(tstring strFileName)
{	
	tstring strID;
	strID = strFileName.substr(0, strFileName.size()-4);	
	//-- SET FILE NAME
	return strID;
}

tstring  ESMUtil::GetFolderID(tstring strFolderName)
{	
	tstring  strFileName;	
	strFolderName = ReplaceString(strFolderName, _T("/"), _T("\\"));

	//-- SET FILE NAME
	tstring::size_type	posLastDelimeter;
	tstring::size_type	posStringEnd;
	
	posLastDelimeter = strFolderName.find_last_of ('\\');	
	posStringEnd	= strFolderName.size();
	strFileName = strFolderName.substr(0, posLastDelimeter);

	posLastDelimeter = strFileName.find_last_of ('\\');	
	posStringEnd	= strFileName.size();	
	strFileName = strFileName.substr(posLastDelimeter+1, posStringEnd - posLastDelimeter);
	
	return strFileName;
}

tstring  ESMUtil::ReplaceString(tstring strSource, tstring strSearch, tstring strReplace)
{
	tstring strStorage = strSource;	
	tstring stringSearch = strSearch;

	tstring::size_type offset = 0;  

	while( true )  
	{  
		offset = strStorage.find( stringSearch, offset );            

		if( tstring::npos == offset ) 
		{
			strSource = strStorage;
			return strSource;
		}
		else                       
			strStorage.replace( offset, stringSearch.size(), strReplace );                      
	}  
}


tstring  ESMUtil::ReplacePathSymbol(tstring strSource, tstring strSymbol, tstring strPath)
{	
	tstring stringSymbol = _T("$(") + (tstring)strSymbol +_T(")");	
	strSymbol = (tstring )stringSymbol.c_str();

	ReplaceString(strSource, _T("/"), _T("\\"));				// replace '/' '\'
	ReplaceString(strSource, strSymbol, strPath);	// replace symbol to path

	return strSource;
}

void ESMUtil::StringToken(const tstring& strSource, vector<tstring>& tokens, const tstring& delimiters)
{
	tstring::size_type lastPos = strSource.find_first_not_of(delimiters,0);
	tstring::size_type pos = strSource.find_first_of(delimiters,lastPos);
	tstring strToken;

	while(tstring::npos != pos || tstring::npos != lastPos)
	{
		strToken = strSource.substr(lastPos,pos - lastPos);

		if (strToken.substr(0,1).compare(_T("#")) != 0)
		{		
			tokens.push_back(strToken);			
		}

		lastPos = strSource.find_first_not_of(delimiters,pos);
		pos = strSource.find_first_of(delimiters,lastPos);
	}

}

void ESMUtil::StringToken(const CString& strSource, CStringArray* pArStr, const CString& delimiters)
{
	CString strToken, strDes;	
	int nPos = 0;

	//-- Set Destination
	strDes = strSource;
	while(strDes != _T(""))
	{
		nPos = strDes.Find(delimiters);
		if(nPos == -1)
		{
			if(strDes.GetLength())
				pArStr->Add(strDes);
			break;
		}

		//-- Send the left string
		strToken = strDes.Left(nPos);
		strDes = strDes.Mid(nPos+1);
		pArStr->Add(strToken);
	}
}

tstring ESMUtil::Trim(tstring strSource)
{
	UINT nStartPos = 0;
	UINT nEndPos = (UINT)strSource.length() - 1;

	//-- Trim Left
	for(UINT i = 0; i < nEndPos; i++)
	{
		if(strSource.at(i) != ' ' && strSource.at(i) != '\t')
		{
			nStartPos = i;

			break;
		}
	}

	//-- Trim Right
	for(UINT i = nEndPos; i > nStartPos; i--)
	{
		if(strSource.at(i) != ' ' && strSource.at(i) != '\t')
		{
			nEndPos = i;

			break;
		}
	}

	const int STRLEN = nEndPos - nStartPos + 1;

	return strSource.substr(nStartPos, STRLEN);	
}


//--------------------------------------------------------------------------
//!@fn			itoa
//!@brief		translate integer to string
//
//!@param[in]	int value, unsigned int base	:  0 < base <= 16 
//!@param[out]	none
//!@return		string
//!@note				
//--------------------------------------------------------------------------
tstring ESMUtil::itoa(int value, unsigned int base)
{
	const TCHAR digitMap[] = _T("0123456789abcdef");
	tstring buf;

	// Guard: base 
	if (base == 0 || base > 16) {		
		return buf;
	}

	// negative int
	tstring sign;
	int _value = value;

	// if 0
	if (_value == 0) return _T("0");

	if (value < 0) {
		_value = -value;
		sign = _T("-");
	}

	// Translating number to string with base:
	for (int i = 30; _value && i ; --i) {
		buf = digitMap[ _value % base ] + buf;
		_value /= base;
	}

	return sign.append(buf);
} 

//--------------------------------------------------------------------------
//!@brief	Convert int to CSting	
//
//!@param	value : integer value
//!@param	base  : 2, 4, 8, 10, 16
//!@return	on success, any other value means failure.
//!@note				
//--------------------------------------------------------------------------
//CString ESMUtil::itocs(const int value, const unsigned int base)
tstring ESMUtil::itocs(const int value, const unsigned int base)
{
	tstring strValue = itoa(value, base);	
	return strValue;
} 

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2011-6-22
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
void ESMUtil::PrintToFile(const TCHAR* strPath, const TCHAR* format, ...)
{
	FILE* fp = _tfopen(strPath, _T("a+"));

	va_list args;
	va_start(args, format);

	_vftprintf(fp, format, args);
	_ftprintf(fp, _T("\r\n"));
	fflush(fp);

	va_end(args);

	fclose(fp);
}

//------------------------------------------------------------------------------ 
//! @brief   Reposition video control.
//! @date     2012-4-16
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------
DWORD ESMUtil::ReadFileEx(HANDLE hFile, BYTE* pBuffer, DWORD nNumberOfBytesToRead)
{
	DWORD dwIndex = 0;
	while(TRUE)
	{
		DWORD dwRead = 0;
		if(!ReadFile(hFile, pBuffer+dwIndex, nNumberOfBytesToRead, &dwRead, NULL))
			return GetLastError();
		
		if((dwRead == nNumberOfBytesToRead) || (nNumberOfBytesToRead == 0)) 
			break;
		
		if(dwRead == 0) 
			return ERROR_HANDLE_EOF;

		dwIndex += dwRead;
		nNumberOfBytesToRead -= dwRead;
	}

	return ERROR_SUCCESS;
}

CString ESMUtil::GetLocalMacAddress()
{
	CString strMacAddress = _T("00:00:00:00:00:00");
	IP_ADAPTER_INFO AdapterInfo[5];

	DWORD dwBufLen = sizeof(AdapterInfo);
	DWORD dwStatus = GetAdaptersInfo(AdapterInfo, &dwBufLen);                  

	if(dwStatus != ERROR_SUCCESS)
	{
		strMacAddress.Format(_T("Error : %d "),dwStatus) ;
		return strMacAddress ;
	}
	PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo;

	strMacAddress.Format(_T("%02X:%02X:%02X:%02X:%02X:%02X"),
		pAdapterInfo->Address[0],
		pAdapterInfo->Address[1],
		pAdapterInfo->Address[2],
		pAdapterInfo->Address[3],
		pAdapterInfo->Address[4],
		pAdapterInfo->Address[5]);

	return strMacAddress;
}

CString ESMUtil::GetLocalIPAddress()
{
	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	CString strIP;
	PHOSTENT hostinfo;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 )
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				strIP = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}
		}      
		WSACleanup( );
	} 
	return strIP;
}

int ESMUtil::GetLocalMacAddressArray(CStringArray& strArr)
{
	CString strMacAddress = _T("000000000000");
	IP_ADAPTER_INFO AdapterInfo[5];
	memset(AdapterInfo, NULL, sizeof(AdapterInfo));

	DWORD dwBufLen = sizeof(AdapterInfo);
	DWORD dwStatus = GetAdaptersInfo(AdapterInfo, &dwBufLen);                  

	if(dwStatus != ERROR_SUCCESS)
		return 0;

	int nCount = 0;

	for(int i=0 ; i<5 ; i++)
	{
		PIP_ADAPTER_INFO pAdapterInfo = &AdapterInfo[i];

		CString strIP;
		strIP = AdapterInfo[i].IpAddressList.IpAddress.String[0];

		if(strIP != "0")
		{

			strMacAddress.Format(_T("%02X%02X%02X%02X%02X%02X"),
				pAdapterInfo->Address[0],
				pAdapterInfo->Address[1],
				pAdapterInfo->Address[2],
				pAdapterInfo->Address[3],
				pAdapterInfo->Address[4],
				pAdapterInfo->Address[5]);

			strArr.Add(strMacAddress);

			nCount++;
		}

		if(!AdapterInfo[i].Next)
			break;
	}

	return nCount;
}

DWORD ESMUtil::GetMacaddrNetSpeed(CString strMacaddr)
{
	DWORD dwSize = 0;
    DWORD dwRetVal = 0;

    unsigned int i, j;

    MIB_IFTABLE *pIfTable;
    MIB_IFROW *pIfRow;

    pIfTable = (MIB_IFTABLE *) HeapAlloc(GetProcessHeap(), 0, sizeof (MIB_IFTABLE));
    if (pIfTable == NULL)
        return 0;
    
    dwSize = sizeof (MIB_IFTABLE);
    if (GetIfTable(pIfTable, &dwSize, FALSE) == ERROR_INSUFFICIENT_BUFFER) {
        HeapFree(GetProcessHeap(), 0, pIfTable);
        pIfTable = (MIB_IFTABLE *) HeapAlloc(GetProcessHeap(), 0, dwSize);

        if (pIfTable == NULL)
            return 0;
    }
    
    if ((dwRetVal = GetIfTable(pIfTable, &dwSize, FALSE)) == NO_ERROR) {
        for (i = 0; i < pIfTable->dwNumEntries; i++) 
		{
            pIfRow = (MIB_IFROW *) & pIfTable->table[i];

			CString strTemp;
			CString strTemp2;

			for (j = 0; j < pIfRow->dwPhysAddrLen; j++)
			{
				strTemp.Format(_T("%.2X"),(int) pIfRow->bPhysAddr[j]);
				strTemp2.Append(strTemp);
			}

			if(strMacaddr == strTemp2)
				return ((pIfRow->dwSpeed) /1000/1000);
        }
    } else {
        if (pIfTable != NULL) {
            HeapFree(GetProcessHeap(), 0, pIfTable);
            pIfTable = NULL;
        }  
        return 0;
    }

    if (pIfTable != NULL) {
        HeapFree(GetProcessHeap(), 0, pIfTable);
        pIfTable = NULL;
    }

	return 0;
}

DWORD ESMUtil::GetLocalNetSpeed()
{
	CString strMac;
	CStringArray ArrMac;
	int nSpeed;
	int nCount = GetLocalMacAddressArray(ArrMac);

	if(!nCount)
		return 0;
	else
	{
		for(int i=0; i<ArrMac.GetSize(); i++)
		{
			strMac = ArrMac.GetAt(i);

			nSpeed = GetMacaddrNetSpeed(strMac);

			if(nSpeed < 1000)
				return nSpeed;
		}
	}

	return nSpeed;
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2015-05-29
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
void ESMUtil::WakeOnLan(CString strMacAddr)
{

#ifdef _SEND_MAGIC_PACKET
	CAsyncSocket s;												//Socket to send magic packet
	BYTE magicP[102];							//Buffer for packet
	CString strPort = _T("40000");

	//Fill in magic packet with 102 Bytes of data
	//-------------------------------------------
	
	for (int i = 0; i < 6; i++)										//Header
		magicP[i] = 0xff;										//fill 6 Bytes with 0xFF

	//First 6 bytes 
	//(these must be repeated!!)
	//fill bytes 6-12
	//Get 2 charachters from mac 
	//address and convert it to 
	//int to fill magic packet
	for (int i = 0; i < 6; i++) {
		magicP[i+6] = HexStrToInt(strMacAddr.Mid(i * 2, 2));
	}

	//fill 90 bytes 
	//(15 time repeat)
	//Warning : It is higly recommended
	//not to use functions like memcpy,
	//read MSDN for more details.

	for (int i = 0; i < 15; i++)
		memcpy(&magicP[(i + 2) *6 ],&magicP[6], 6);

	//Create a socket to send data
	char* szFile = ESMUtil::CStringToChar(strPort);
	

	s.Create(atol(szFile), SOCK_DGRAM);
	//Customize socket to BROADCAST
	BOOL bOptVal = TRUE;
	if (s.SetSockOpt(SO_BROADCAST, (char*)&bOptVal, sizeof(BOOL))==SOCKET_ERROR) 
	{
		delete[] szFile;
		return;
	}

	//Broadcast Magic Packet, 
	//Hope appropriate NIC will 
	//take it ;)
	s.SendTo(magicP, 102, atol(szFile));
	delete[] szFile;

	TRACE(_T("Magic packet sent.Remote computer should turn on now."));

	//Close the socket and 
	//release all buffers
	s.Close();
#else
	CString strParam, strWOL;
	strParam.Format(_T("/wakeup %s"), strMacAddr);

	TCHAR strPath[MAX_PATH];
	GetModuleFileName( NULL, strPath, _MAX_PATH);
	PathRemoveFileSpec(strPath);
#if _DEBUG
	strWOL = _T("C:\\Program Files\\ESMLab\\4DMaker\\bin");
#else
	strWOL = strPath;
#endif
	ESMUtil::ExecuteProcess(strWOL,_T("WakeMeOnLan.exe"), strParam, FALSE);
#endif
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2015-05-29
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
UINT ESMUtil::HexStrToInt(CString hexStr)
{
	char *stop;
	char  num[3];
	UINT res = 0;

	//Nothing happens if return 0, just server will not wakeup
	if (hexStr.GetLength()>2) {
		TRACE(_T("(Length) Invalid Input!"));
		return 0;		//or perhaps exit;
	}

	memset(num,'\0',3);

	//In Unicode it's: wcscpy
	char* szFile = ESMUtil::CStringToChar(hexStr);
	strcpy(num,szFile);
	delete[] szFile;
	//In Unicode it's: wcstol
	res = strtol(num,&stop,16);

	if (res==LONG_MAX || res==LONG_MIN || res==0) {
		TRACE(_T("(OverFlow) Invalid Input!"));
		return 0;		//or perhaps exit;
	}

	return res;
}
//------------------------------------------------------------------------------ 
//! @brief
//! @date     2012-06-19
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
BOOL ESMUtil::ParseString(wstring strin, wstring sep, vector<wstring>& vecstr)
{
	wstring::size_type preindex = 0;
	wstring::size_type curindex = 0;
	wstring::size_type maxsize = strin.size();

	wstring substr;
	while(curindex < maxsize)	
	{
		preindex = curindex; 
		curindex = strin.find(sep, curindex);
		if(wstring::npos == curindex)
		{
			substr = strin.substr(preindex, maxsize-curindex);
			vecstr.push_back(substr);
			break;
		}

		substr = strin.substr(preindex, curindex-preindex);
		vecstr.push_back(substr);
		curindex++;
	}

	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2012-06-19
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
BOOL ESMUtil::ParseString(wstring strin, wstring sep, vector<CString>& vecstr)
{
	wstring::size_type preindex = 0;
	wstring::size_type curindex = 0;
	wstring::size_type maxsize = strin.size();

	wstring substr;
	while(curindex < maxsize)	
	{
		preindex = curindex; 
		curindex = strin.find(sep, curindex);
		if(wstring::npos == curindex)
		{
			substr = strin.substr(preindex, maxsize-curindex);
			vecstr.push_back(substr.c_str());
			break;
		}

		substr = strin.substr(preindex, curindex-preindex);
		vecstr.push_back(substr.c_str());
		curindex++;
	}

	return TRUE;
}

CString ESMUtil::GetStringFromTime(CTime time)
{
	CString strRet;
	CString strTime[6];

	strTime[0].Format(_T("%d"),time.GetYear()	);
	strTime[1].Format(_T("%d"),time.GetMonth()	);
	strTime[2].Format(_T("%d"),time.GetDay()	);
	strTime[3].Format(_T("%d"),time.GetHour()	);
	strTime[4].Format(_T("%d"),time.GetMinute()	);
	strTime[5].Format(_T("%d"),time.GetSecond()	);

	strRet		=	strTime[0]	+ TC_SEPARATOR	+	// 2011
					strTime[1]	+ TC_SEPARATOR	+	// 10
					strTime[2]	+ TC_SEPARATOR	+	// 02
					strTime[3]	+ TC_SEPARATOR	+	// 01
					strTime[4]	+ TC_SEPARATOR	+	// 59
					strTime[5];	
	return strRet;
}

time_t ESMUtil::GetTimeFromString(CString strBaseTime)
{
	int nTime[6];
	CString strDes;
	CString strToken;
	CStringArray arList;
	
	StringToken(strBaseTime, &arList, _T(";"));

	nTime[0] = _ttoi(arList.GetAt(0));
	nTime[1] = _ttoi(arList.GetAt(1));
	nTime[2] = _ttoi(arList.GetAt(2));
	nTime[3] = _ttoi(arList.GetAt(3));
	nTime[4] = _ttoi(arList.GetAt(4));
	nTime[5] = _ttoi(arList.GetAt(5));								

	tm tTime = {0,};

	tTime.tm_year = nTime[0]-1900;
	tTime.tm_mon = nTime[1]-1;
	tTime.tm_mday = nTime[2];
	tTime.tm_hour = nTime[3];
	tTime.tm_min = nTime[4];
	tTime.tm_sec = nTime[5];
	tTime.tm_isdst	= false;

	arList.RemoveAll();

	return mktime(&tTime);
}

//------------------------------------------------------------------------------ 
//! @brief    해당 년,월,일까지 전체 일수를 반환
//! @date     2012-1-23
//! @owner    jeongyong.kim
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
long ESMUtil::GetTotalCountOfDays(int nYear,int nMonth, int nDay)
{
	int nArrDaysOfMonth[] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
	long nTotalFirst, nTotalSec, nTotal;

	nTotalFirst = (nYear-1) * 365L + (nYear-1) / 4 - (nYear - 1) / 100 + (nYear - 1) / 400;

	if(LEAPYEAR(nYear))
		nArrDaysOfMonth[2]++;

	nTotalSec = 0;
	for(int i = 1 ; i < nMonth ; i++)
		nTotalSec += nArrDaysOfMonth[i];

	nTotal = nTotalFirst + nTotalSec + nDay;
	return nTotal;
}


CString ESMUtil::SetString(int nValue)
{	
	CString str;
	str.Format(_T("%d"),nValue);
	return str;
}

CString ESMUtil::GetDate()
{	
	CString strDate = _T("");
	CTime time  = CTime::GetCurrentTime();
	int nYear = time.GetYear();
	int nMonth = time.GetMonth();
	int nDay = time.GetDay();

	strDate.Format(_T("%d%02d%02d"), nYear, nMonth, nDay);
	return strDate;
}

CString ESMUtil::GetTime()
{	
	struct _timeb milli_time;
	_ftime( &milli_time );	

	CString strTime = _T("");
	CTime time  = CTime::GetCurrentTime();
	int nHour = time.GetHour();
	int nMin = time.GetMinute();
	int nSec = time.GetSecond();
	int nMilliSec = milli_time.millitm;

	strTime.Format(_T("%02d%02d%02d.%03d"), nHour, nMin, nSec, nMilliSec);

	return strTime;
}

CString ESMUtil::GetTimeStr(SYSTEMTIME tTime)
{	
	CString strTime;
	strTime.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"), tTime.wYear, tTime.wMonth, tTime.wDay, tTime.wHour, tTime.wMinute, tTime.wSecond);

	if(!(1900 <= tTime.wYear && tTime.wYear <= 2100 &&
		1 <= tTime.wMonth && tTime.wMonth <= 12 &&
		1 <= tTime.wDay && tTime.wDay <= 31 &&
		0 <= tTime.wHour && tTime.wHour <= 23 &&
		0 <= tTime.wMinute && tTime.wMinute <= 59 &&
		0 <= tTime.wSecond && tTime.wSecond <= 59))
	{
		//TGSetResultComments(TRUE, strTime);
		strTime.Format(_T("1900-01-01 00:00:00"));
	}

	return strTime;
}

CString ESMUtil::GetGraphTime()
{	
	CString strTime;
	SYSTEMTIME tTime = {0,};
	::GetLocalTime(&tTime);                 
	strTime.Format(_T("%04d%02d%02d%02d%02d%02d"), tTime.wYear, tTime.wMonth, tTime.wDay, tTime.wHour, tTime.wMinute, tTime.wSecond);
	return strTime;
}


//------------------------------------------------------------------------------
//! @brief				
//! @date		2012-07-16
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
char* ESMUtil::GetCharString(CString str)
{
//-- 2013-05-01 hongsu@esmlab.com
//-- Add Last \n 
#ifdef _WIN64
	str.AppendChar('\n');
#endif
	int nLength = str.GetLength()+1;

	static char* pChar = NULL;
	pChar = new char[nLength];
	if(!pChar)
		return NULL;

	memset(pChar, 0, str.GetLength()+1);
	size_t CharCinverted = 0;
	wcstombs_s(&CharCinverted, pChar, nLength, str, _tcslen(str));
	return pChar;

}

//------------------------------------------------------------------------------
//! @brief		
//! @date		2012-07-16
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
CString ESMUtil::GetStrChar(char* ch, BOOL bDelete)
{
	CString str;
//-- 2013-05-01 hongsu@esmlab.com
//-- 32bit / 64bit
#ifdef _WIN64
	char* pPosition = ch;
	while(1)
	{
		str.AppendChar(*ch++);
		if(*ch == '\n' || *ch == NULL)
			break;
	}
	ch = *(&pPosition);
#else
	str.Format(_T("%s"), (CA2T)ch);
#endif

	if(bDelete)
	{
		delete ch;
		ch = NULL;
	}
	return str; 	
}

//------------------------------------------------------------------------------ 


//------------------------------------------------------------------------------ 
//! @brief
//! @date     2011-11-19
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
BOOL ESMUtil::GetSubFolderNames(CString strPath, vector<CString>& vecFolderName)
{
	vector<wstring> vecTemp;
	if(!ESMUtil::GetSubFolderNames(wstring(strPath), vecTemp))
		return FALSE;

	for(wstring::size_type i=0; i<vecTemp.size(); i++)
	{
		vecFolderName.push_back(vecTemp[i].c_str());
	}

	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2011-11-19
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
BOOL ESMUtil::GetSubFolderNames(wstring strPath, vector<wstring>& vecFolderName)
{
	if(!PathFileExists(strPath.c_str()))
		return FALSE; //error

	CFileFind	find;
	strPath += _T("\\*.*");
	BOOL bNext = find.FindFile(strPath.c_str());
	while(bNext)
	{
		bNext = find.FindNextFile();
		CString strFolderName = find.GetFileName();   
		if(strFolderName.GetLength() > 0) 
		{
			if(find.IsDirectory() && !find.IsDots())
			{
				vecFolderName.push_back(wstring(strFolderName));
			}
			else 
				continue; 
		}
	}

	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief	Get File Name And File Path From Full Path
//! @date     2011-11-19
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
void ESMUtil::GetFileNameAndPath(CString strFullPath, CString &FileName,CString &strPath)
{
	int index = strFullPath.ReverseFind(_T('\\'));
	strPath = strFullPath.Left(index+1);
	FileName = strFullPath.Mid(index+1);
}

//------------------------------------------------------------------------------ 
//! @brief	Get number of character in string
//! @date     2014-06-26
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int ESMUtil::GetFindCharCount(CString parm_string, char parm_find_char)
{ 
	int length = parm_string.GetLength(), find_count = 0; 

	for(int i = 0; i < length; i++){ 
		if(parm_string[i] == parm_find_char) find_count++; 
	} 

	return find_count; 
} 

//------------------------------------------------------------------------------ 
//! @brief	Check file exist
//! @date     2014-06-26
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
BOOL ESMUtil::FileExist(CString strPath)
{
	WIN32_FIND_DATA FindData;

	HANDLE file = FindFirstFile(strPath, &FindData);

	if ( file == INVALID_HANDLE_VALUE )
	{
		FindClose(file);
		return FALSE;
	}
	FindClose(file);
	return TRUE;
}

CString ESMUtil::CharToCString(char* str, BOOL bDelete)
{
	if( str == NULL)
		return _T("");

	CString strString;
	int len; 
	BSTR buf; 
	len = MultiByteToWideChar(CP_ACP, 0, str, strlen(str), NULL, NULL); 
	buf = SysAllocStringLen(NULL, len); 
	MultiByteToWideChar(CP_ACP, 0, str, strlen(str), buf, len); 

	strString.Format(_T("%s"), buf); 
	if( bDelete == TRUE && str)
	{
		delete[] str;
		str = NULL;
	}
	strString.Trim();
	return strString;
}

char* ESMUtil::CStringToChar(CString str)
{
	wchar_t* wchar_str = NULL;     //첫번째 단계(CString to wchar_t*)를 위한 변수 
	char*    char_str = NULL;      //char* 형의 변수 
	int      char_str_len;  //char* 형 변수의 길이를 위한 변수   
	//1. CString to wchar_t* conversion 
	wchar_str = str.GetBuffer(str.GetLength());   //2. wchar_t* to char* conversion //char* 형에 대한길이를 구함 
	char_str_len = WideCharToMultiByte(CP_ACP, 0, wchar_str, -1, NULL, 0, NULL, NULL); 
	try
	{
		char_str = new char[char_str_len];  //메모리 할당 //wchar_t* to char* conversion 
		WideCharToMultiByte(CP_ACP, 0, wchar_str, -1, char_str, char_str_len, 0,0);
	}
	catch (CMemoryException* e)
	{
		char_str = NULL;
		e->Delete();
	}
		
	str.ReleaseBuffer();
	return char_str;
}

DWORD ESMUtil::GetProcessPID(CString strProcess)
{
	HANDLE hndl = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	DWORD dwsma = GetLastError();

	DWORD dwExitCode = 0;

	PROCESSENTRY32  procEntry = { 0 };
	procEntry.dwSize = sizeof(PROCESSENTRY32);
	Process32First(hndl, &procEntry);
	do
	{
		if (!_tcsicmp(procEntry.szExeFile, strProcess))
		{
			return procEntry.th32ProcessID;
		}
	} while (Process32Next(hndl, &procEntry));

	if (hndl != INVALID_HANDLE_VALUE)
		CloseHandle(hndl);
	hndl = INVALID_HANDLE_VALUE;

	return 0;
}

HWND ESMUtil::GetWndHandle(DWORD dwPID)
{
	HWND hWnd = ::FindWindow(NULL, NULL);
	while (hWnd != NULL)
	{
		if (::GetParent(hWnd) == NULL) {
			DWORD dwProcId;
			GetWindowThreadProcessId(hWnd, &dwProcId);

			if (dwPID == dwProcId) {
				return hWnd;
			}
		}
		hWnd = ::GetWindow(hWnd, GW_HWNDNEXT);
	}
	return NULL;
}

BOOL ESMUtil::GetIsFile(CString strAppPath, CString strAppName)
{
	CString strFullPath;
	strFullPath = strAppPath + _T("\\") + strAppName;
	CFileStatus status;
	if (CFile::GetStatus(strFullPath, status) == NULL)
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

BOOL ESMUtil::KillProcess(CString strProcess)
{
	HANDLE hndl = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
	DWORD dwsma = GetLastError();
	BOOL bTerminate = FALSE;
	DWORD dwExitCode = 0;

	PROCESSENTRY32  procEntry={0};
	procEntry.dwSize = sizeof( PROCESSENTRY32 );
	Process32First(hndl,&procEntry);
	do
	{
		if(!_tcsicmp(procEntry.szExeFile,strProcess))
		{
			HANDLE hHandle;
			hHandle = ::OpenProcess(PROCESS_TERMINATE, FALSE, procEntry.th32ProcessID);
			if(hHandle)
			{
				::GetExitCodeProcess(hHandle,&dwExitCode);
				bTerminate = ::TerminateProcess(hHandle, dwExitCode);

				CloseHandle(hHandle);
				break;
			}
		}
	}while(Process32Next(hndl,&procEntry));	

	if(hndl != INVALID_HANDLE_VALUE)
		CloseHandle(hndl);
	hndl = INVALID_HANDLE_VALUE;

	return bTerminate;
}

BOOL ESMUtil::ExecuteProcess(CString strAppPath, CString strAppName, CString strParam/* = _T("")*/, BOOL bShow /*= TRUE*/)
{
	SHELLEXECUTEINFO shInfo = { 0 };
	shInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	shInfo.lpFile = strAppName;
	if(bShow)
		shInfo.nShow = SW_SHOW;
	else
		shInfo.nShow = SW_MINIMIZE;
	//shInfo.lpParameters = _T("");
	shInfo.lpParameters = strParam;
	shInfo.lpDirectory = strAppPath;
	shInfo.lpVerb = _T("");
	shInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	shInfo.hwnd = NULL;
	shInfo.hInstApp = AfxGetInstanceHandle();

	if (ShellExecuteEx(&shInfo))
		return TRUE;
	else
		return FALSE;
}

CString ESMUtil::GetIpAddress(CString strDomain)
{
	CString strIp;

	char szBuf[20][20];
	hostent * remoteHost;
	WSADATA wsaData;

	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR)
	{
		TRACE(_T("Error at WSAStartup()"));
	}

	SOCKET connectSocket;
	connectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (connectSocket == INVALID_SOCKET)
	{
		WSACleanup();
		return strIp;
	}

	remoteHost = gethostbyname(CStringToChar(strDomain));

	int nCnt;
	for (nCnt = 0; remoteHost->h_addr_list[nCnt]; nCnt++)
	{
		sprintf(szBuf[nCnt], "%s", inet_ntoa(*(IN_ADDR*)remoteHost->h_addr_list[nCnt]));
	}
	for (int i = 0; i < nCnt; i++)
	{
		TRACE(CharToCString(szBuf[i]));
	}

	if (nCnt > 0)
	{
		strIp = CharToCString(szBuf[0]);
	}

	WSACleanup();

	return strIp;
}

void ESMUtil::GetLocalIPAddressArray(CStringArray& strArr)
{
	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	//CStringArray strIParr;
	PHOSTENT pHostinfo;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 )
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((pHostinfo = gethostbyname(name)) != NULL)
			{
				if(pHostinfo)
				{
					for(int i=0; pHostinfo->h_addr_list[i] != NULL; i++)
					{
						IN_ADDR in;
						memcpy(&in, pHostinfo->h_addr_list[i], pHostinfo->h_length);
						CString strIp = CharToCString(inet_ntoa(in));
						strArr.Add(strIp);
					}
				}
			}
		}      
		WSACleanup( );
	} 
	
}

CString ESMUtil::GetVersionInfo(HMODULE hLib, CString csEntry)
{
	CString csRet;

	if (hLib == NULL)
		hLib = AfxGetResourceHandle();

	HRSRC hVersion = FindResource( hLib, MAKEINTRESOURCE(VS_VERSION_INFO), RT_VERSION );
	if (hVersion != NULL)
	{
		HGLOBAL hGlobal = LoadResource( hLib, hVersion );
		if ( hGlobal != NULL)
		{

			LPVOID versionInfo = LockResource(hGlobal);
			if (versionInfo != NULL)
			{
				DWORD vLen,langD;
				BOOL retVal;

				LPVOID retbuf=NULL;
#ifdef _UNICODE
				static TCHAR fileEntry[256];
				wsprintf(fileEntry,_T("\\VarFileInfo\\Translation"));
#else
				static char fileEntry[256];
				sprintf(fileEntry,_T("\\VarFileInfo\\Translation"));
#endif


				retVal = VerQueryValue(versionInfo,fileEntry,&retbuf,(UINT *)&vLen);
				if (retVal && vLen==4)
				{
					memcpy(&langD,retbuf,4);
#ifdef _UNICODE
					wsprintf(fileEntry, _T("\\StringFileInfo\\%02X%02X%02X%02X\\%s"),
						(langD & 0xff00)>>8,langD & 0xff,(langD & 0xff000000)>>24,
						(langD & 0xff0000)>>16, csEntry);
#else
					sprintf(fileEntry, _T("\\StringFileInfo\\%02X%02X%02X%02X\\%s"),
						(langD & 0xff00)>>8,langD & 0xff,(langD & 0xff000000)>>24,
						(langD & 0xff0000)>>16, csEntry);
#endif
				}
				else
#ifdef _UNICODE
					wsprintf(fileEntry, _T("\\StringFileInfo\\%04X04B0\\%s"), GetUserDefaultLangID(), csEntry);
#else
					sprintf(fileEntry, _T("\\StringFileInfo\\%04X04B0\\%s"), GetUserDefaultLangID(), (LPCTSTR)csEntry);
#endif

				if (VerQueryValue(versionInfo,fileEntry,&retbuf,(UINT *)&vLen))
#ifdef _UNICODE
					csRet = (TCHAR*)retbuf;
#else
					csRet = (char*)retbuf;
#endif
			}
		}

		UnlockResource( hGlobal );
		FreeResource( hGlobal );
	}

	return csRet;
}

CString ESMUtil::FormatVersion(CString cs)
{
	CString csRet;
	if (!cs.IsEmpty())
	{
		cs.TrimRight();
		int iPos = cs.Find(',');
		if (iPos == -1)
			return _T("");
		cs.TrimLeft();
		cs.TrimRight();
		csRet.Format(_T("%s."), cs.Left(iPos));

		while (1)
		{
			cs = cs.Mid(iPos + 1);
			cs.TrimLeft();
			iPos = cs.Find(',');
			if (iPos == -1)
			{
				csRet +=cs;
				break;
			}
			csRet += cs.Left(iPos);
		}
	}

	return csRet;
}

