/////////////////////////////////////////////////////////////////////////////
//
// LogMgr.cpp: implementation of the LogMgr class.
//
//
// Copyright (c) 2003 ESMLab, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of ESMLab, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2003-11-11
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LogMgr.h"

//DELETE blabla.* 2009-11-23 hongsu.jung
#ifndef _4DMAKER_WATCHER_LOG
#include "ESMFunc.h"
#include "ESMFileOperation.h"
#else
#include "ESMLog.h"
#endif

# if defined (_DEBUG) || defined (DEBUG)
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define SIZE_ARRAY		512
#define	ADD_LIMIT_DAY	10

//------------------------------------------------------------------------------ 
//! @brief    Constructor
//! @date     
//! @owner    
//! @return        
//------------------------------------------------------------------------------ 
LogMgr::LogMgr(CString strPath, CString strName, int nLimitDay, int nVerbosity)
{
	m_strPath    = strPath + _T("\\");
	m_strName    = strName;
	m_strFile    = m_strPath + strName;
	m_nVerbosity = nVerbosity;
	
	//Copy the current system date to a buffer. "06/29/11"
	_tstrdate(m_strBeginDate);
	
	m_nLimitDay = nLimitDay;

	ESMCreateAllDirectories(strPath);
	
	InitializeCriticalSection(&m_csLock);
}

//------------------------------------------------------------------------------ 
//! @brief    Destructor
//! @date     
//! @owner    
//! @note
//! @return        
//------------------------------------------------------------------------------ 
LogMgr::~LogMgr()
{
	Stop();
	DeleteCriticalSection(&m_csLock);
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     
//! @owner    
//! @note
//! @return        
//! @revision 2011-6-29 hongsu.jung
//------------------------------------------------------------------------------ 
void LogMgr::Start()
{
	//"2011_06_29"
	SYSTEMTIME tTime = {0,};
	::GetLocalTime(&tTime);

	CString strDate;	
	strDate.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), tTime.wYear, tTime.wMonth, tTime.wDay, tTime.wHour, tTime.wMinute, tTime.wSecond);

	m_strFile.Format(_T("%s%s_%s"), m_strPath, strDate, m_strName);

	if (!m_fWriteFile.Open(m_strFile, CFile::modeWrite | CFile::modeCreate | CFile::modeNoTruncate))
	{
		return;
	}
	_TCHAR bom = (_TCHAR)0xFEFF;
	m_fWriteFile.Write( &bom, sizeof(_TCHAR) );
// 	m_fOut.open(m_strFile, ios::out | ios::app);	
// 	m_fOut.imbue(std::locale("kor")); 	

	
	/*CString _strFile, _strTemp;
	_strTemp = m_strName;
	_strTemp.Replace(_T("4DMaker"), _T("ANSI"));

	_strFile.Format(_T("%s%s_%s"), m_strPath, strDate, _strTemp);
	m_fStdioFile.Open(_strFile, CFile::modeWrite | CFile::modeCreate | CFile::modeNoTruncate);*/
}

void LogMgr::Stop()
{
	CString strLog;
	strLog.Format(_T("Close the log file. \"%s\""), m_strFile);
	OutputLog(1, strLog);

	if(m_fWriteFile.m_hFile != CFile::hFileNull)
		m_fWriteFile.Close();
	//m_fOut.close();

	/*if(m_fStdioFile.m_hFile != CFile::hFileNull)
		m_fStdioFile.Close();*/

}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     
//! @owner    
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
CString LogMgr::GetLogFile() 
{ 
	return m_strFile;
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2002-11-20
//! @owner    
//! @note
//! @return        
//! @revision 2011-1-7 Lee JungTaek
//------------------------------------------------------------------------------ 
void LogMgr::OutputLog(int verbosity, CString logline, bool newline)
{
	if(m_nVerbosity >= verbosity) 		
	{
// 		CTime time = CTime::GetCurrentTime();
// 		time.Format("%Y-%m-%d %H:%M:%S");
// 		CString strDate = time.Format("%Y-%m-%d %H:%M:%S");
		SYSTEMTIME cur_time; 
		CString strDate;
		GetLocalTime(&cur_time); 
		strDate.Format(_T("%02d:%02d:%02d:%03ld"), cur_time.wHour, cur_time.wMinute, cur_time.wSecond, cur_time.wMilliseconds);


		//2011-1-7 Lee JungTaek 
		if(m_csLock.LockCount > -1)
			return;

		logline.Replace(_T(" \""), _T("."));
		/*
		for(int i=0; i<logline.GetLength(); i++)
		{
			if(logline.GetAt(i) == 0x2022)
				logline.SetAt(i, _T('.'));
		}
		*/
		//compare date.
		/*
		if(IsDateChanged()) 
		{
			m_fOut << endl;
			m_fOut << "  ##############################################"<< endl;
			m_fOut << "  #                                            #"<< endl;
			m_fOut << "  #   /A/U/T/O/ /C/R/E/A/T/E/ /N/E/W/ /L/O/G/  #"<< endl;						      
			m_fOut << "  #      " << (LPCTSTR)strDate << "                   #"<< endl;
			m_fOut << "  #                                            #"<< endl;	
			m_fOut << "  ##############################################"<<endl;
			m_fOut << endl;
		}
		*/


		EnterCriticalSection(&m_csLock);
		CString strWriteData;
		strWriteData.Format(_T("[%s]%s"), strDate, logline);


		/*if(m_fStdioFile.m_hFile != CFile::hFileNull)
		{
			m_fStdioFile.WriteString(strWriteData + _T("\n"));
			
		}*/


		if(m_fWriteFile.m_hFile != CFile::hFileNull)
		{
			if(newline)
			{
				strWriteData = strWriteData + _T("\r\n");
				m_fWriteFile.Write(strWriteData, strWriteData.GetLength() * 2);
			}
			else
			{
				m_fWriteFile.Write(strWriteData, strWriteData.GetLength() * 2);
			}

			m_fWriteFile.Flush();
		}else
		{
			if (m_fWriteFile.Open(m_strFile, CFile::modeWrite | CFile::modeCreate | CFile::modeNoTruncate))
			{
				m_fWriteFile.SeekToEnd();
				if(newline)
				{
					strWriteData = strWriteData + _T("\r\n");
					m_fWriteFile.Write(strWriteData, strWriteData.GetLength() * 2);
				}
				else
				{
					m_fWriteFile.Write(strWriteData, strWriteData.GetLength() * 2);
				}

				m_fWriteFile.Flush();
			}
		}

		

		//m_fWriteFile.Close();
		
// 		if(m_fOut.is_open()) 
// 		{
// 			if(newline)		m_fOut << "[" << (LPCTSTR)strDate << "] " << (LPCTSTR)logline << endl;	
// 			else			m_fOut << "[" << (LPCTSTR)strDate << "] " << (LPCTSTR)logline;
// 			m_fOut.flush();
// 		}
		LeaveCriticalSection(&m_csLock);
		
	}
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2008-08-15
//! @owner    
//! @note
//! @return        
//! @revision 2008-08-27
//!           2010-01-28 이정택
//------------------------------------------------------------------------------ 
void LogMgr::Trace(tstring logline)
{
	//if(logline.length () < 255)
	//	TRACE(_T("%s\n"),logline.c_str());
	//-- 2008-08-27
/*
	struct _timeb timebuffer;
	string timeline;
	CString strTempNow;
*/
	
	//Disable > Trace Time 추가로 MainFrm 에서 추가
	//2010-01-28 이정택
// 	_ftime( &timebuffer );
// 	timeline = _strtime(ctime( & ( timebuffer.time ) ));
// 	strTempNow.Format("[%s.%03hu] ", timeline.c_str(), timebuffer.millitm);
	
	EnterCriticalSection(&m_csLock);

	if(m_fWriteFile.m_hFile != CFile::hFileNull)
	{
		CString strWriteData;
		strWriteData.Format(_T("%s"), logline);
		m_fWriteFile.Write(strWriteData, strWriteData.GetLength() * 2);
		m_fWriteFile.Flush();
	}


// 	if(m_fOut.is_open()) 
// 	{
// 		//_fout << (string)strTempNow << logline;
// 		m_fOut << logline.c_str();
// 		m_fOut.flush();
// 	}
	LeaveCriticalSection(&m_csLock);
}

//------------------------------------------------------------------------------ 
//! @brief    Checks the date is changed to the next day.
//! @date     
//! @owner    
//! @note
//! @return        
//------------------------------------------------------------------------------ 
bool LogMgr::IsDateChanged()
{
	if(_tcslen(m_strBeginDate) == 0)
		return false;

	int nCnt= 0;
	for(UINT i=0; i<_tcslen(m_strBeginDate); i++)
	{
		if(m_strBeginDate[i] == _T('/'))
			nCnt++;
	}

	if(nCnt != 2)
		return false;	

	TCHAR today[9] = {0};
	_tstrdate(today);

	//if it is not the same
	if(_tcscmp(m_strBeginDate, today))
	{
		CString strDate;
		CTime t = CTime::GetCurrentTime();
		strDate = t.Format("%Y_%m_%d");			
		m_strFile.Format(_T("%s%s_%s"), m_strPath, strDate, m_strName);

// 		m_fOut.close();				
// 		_tcscpy(m_strBeginDate, today);		
// 		m_fOut.open(m_strFile, ios::out | ios::app);

		m_fWriteFile.Close();
		_tcscpy(m_strBeginDate, today);	
		if (!m_fWriteFile.Open(m_strFile, CFile::modeWrite | CFile::modeCreate | CFile::modeNoTruncate))
			return false;
		
		return true;
	}
	else
	{
		return false;
	}	
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     
//! @owner    
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
void LogMgr::OutputErrLog(CString  errstring, bool newline)
{
	CString str = _T("[ERROR] ");
	str += errstring;
	//OutputLog(1, _T("[ERROR] ") + errstring, newline);
	OutputLog(1, str, newline);	
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     
//! @owner    
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
void LogMgr::SetLimitDay(int limitday)
{
	m_nLimitDay = limitday;
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2009-9-18
//! @owner    
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
void LogMgr::SaveAsNew(CString strNewName)
{
	CString strNewFilePath;
	CString strDate;

	SYSTEMTIME tTime = {0,};
	::GetLocalTime(&tTime);

	strDate.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), tTime.wYear, tTime.wMonth, tTime.wDay, tTime.wHour, tTime.wMinute, tTime.wSecond);

	strNewFilePath.Format(_T("%s%s_%s_ESM.log"), m_strPath, strDate, strNewName);
	
	//** THREAD LOCKING
	EnterCriticalSection(&m_csLock);	
	//** CLose File
	//m_fOut.close();
	m_fWriteFile.Close();
	//** THREAD UNLOCKING
	LeaveCriticalSection(&m_csLock);	

	if(!MoveFile(m_strFile, strNewFilePath))
	{
		CString str = _T("Can't Create New File : ");
		str += strNewFilePath;

		EnterCriticalSection(&m_csLock);	
		OutputErrLog(str);		
		LeaveCriticalSection(&m_csLock);

		return;
	}

	m_strFile = strNewFilePath;

	//** create new log file
	//Start();
}

//------------------------------------------------------------------------------ 
//! @brief   Delete old log files
//! @date     
//! @owner    
//! @note
//! @return        
//------------------------------------------------------------------------------ 
void LogMgr::DeleteOldFile()
{
	//CString strDeleteFile;		
	//for( int i = m_nLimitDay+1 ; i < m_nLimitDay + ADD_LIMIT_DAY ; i ++)
	//{
	//	int nDelDay,nDelMonth,nDelYear;
	//	nDelYear	= t.GetYear();
	//	nDelMonth	= t.GetMonth();
	//	nDelDay		= t.GetDay();

	//	//CHECK DATE YESTERDAY
	//	nDelDay -= i;

	//	//5일전 것들은 지워버린다.
	//	if(nDelDay < 1) 
	//	{
	//		//** Add Day
	//		nDelDay += 31;
	//		//** Set Month
	//		if(nDelMonth == 1)
	//		{
	//			nDelMonth = 12;
	//			nDelYear -= 1;
	//		}
	//		else
	//		{
	//			nDelMonth -= 1;
	//		}
	//	}

	//	//** SET DELETE FILE NAME
	//	strDeleteFile.Format(_T("%s\\%02d_%02d_%02d*"), m_strPath, nDelYear, nDelMonth, nDelDay);

	//	CFileOperation fo;
	//	fo.Delete(strDeleteFile);			
	//	//_unlink(strDeleteFile);
	//}
}

/*
//-- 2009-04-13
void LogMgr::Log(int verbosity, LPCTSTR lpszFormat, ...)
{
	if(lpszFormat == NULL)	
		return ;

	LPTSTR		pszMsg = NULL;	
	int			nBuf;
	va_list		args;

	pszMsg = (LPTSTR)LocalAlloc( LPTR, (_tcslen(lpszFormat) + 1024) * sizeof(TCHAR));
	if( pszMsg == NULL )
		return;

	va_start( args, lpszFormat );
	nBuf = _vstprintf( pszMsg+_tcslen(pszMsg), lpszFormat, args );
	va_end( args );

	log(verbosity,pszMsg);	

	if(pszMsg) 
		LocalFree( pszMsg );
}
*/
