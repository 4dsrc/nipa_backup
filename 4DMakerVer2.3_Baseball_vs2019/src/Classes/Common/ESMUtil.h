////////////////////////////////////////////////////////////////////////////////
//
//	ESMUtil.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include <string>
using namespace std; 

#include <TlHelp32.h>

#ifdef UNICODE
typedef wstring tstring;
typedef wifstream tifstream;
#else
typedef string tstring;
typedef ifstream tifstream;
#endif 

typedef vector<tstring > STRVECTOR, *LPSTRVECTOR;
typedef vector<tstring >::iterator STRITER;

class ESMUtil
{

public:
	ESMUtil(void);
	~ESMUtil(void);

	static bool GetLineInFile(CFile *fp, LPSTRVECTOR vecOut);
	static void	GetParamInLine(char*  strToken, const char*  strDelimit, LPSTRVECTOR vecOut);
	static void	RemoveStrVector(LPSTRVECTOR strVector);
	static void	ReplaceFileDelimit(tstring strOrgin, tstring  strConvertPath);
	//	[3/30/2009 Hongsu.Jung]	
	static tstring	GetFileName(tstring strFile);
	static tstring	GetFileID(tstring strFileName);
	static tstring	GetFolderID(tstring strFolderName);

	static tstring	ReplaceString(tstring strSource, tstring strSearch, tstring strReplace);
	static tstring	ReplacePathSymbol(tstring strSource, tstring strSymbol, tstring strPath);

	static void	StringToken(const tstring& strSource, vector<tstring>& tokens, const tstring& delimiters);
	static void StringToken(const CString& strSource, CStringArray* pArStr, const CString& delimiters);

	static tstring	Trim(tstring strSource);
	static tstring	itoa(int value, unsigned int base = 10);    
	static tstring  itocs(const int value, const unsigned int base = 10);
	static void PrintToFile(const TCHAR* strPath, const TCHAR* format, ...);
	static DWORD ReadFileEx(HANDLE hFile, BYTE* pBuffer, DWORD nNumberOfBytesToRead);
	//-- 2012-06-13 hongsu@esmlab.com
	static CString GetLocalIPAddress();
	static CString GetLocalMacAddress();
	static int GetLocalMacAddressArray(CStringArray& strArr);
	static DWORD GetMacaddrNetSpeed(CString strMacaddr);
	static DWORD GetLocalNetSpeed();
	static void WakeOnLan(CString strMacAddr);
	static UINT HexStrToInt(CString hexStr);

	//-- 2012-07-16 hongsu@esmlab.com
	//-- Create Char
	static char* GetCharString(CString str);
	static CString GetStrChar(char* ch, BOOL bDelete = TRUE);

	static BOOL ParseString(wstring strin, wstring sep, vector<wstring>& vecstr);
	static BOOL ParseString(wstring strin, wstring sep, vector<CString>& vecstr);

	//-- 2012-06-22 hongsu@esmlab.com	 
	static CString SetString(int nValue);
	static CString GetDate();
	static CString GetTime();
	static CString GetTimeStr(SYSTEMTIME tTime);
	static CString GetGraphTime();
	static CString GetStringFromTime(CTime time);
	static time_t GetTimeFromString(CString strBaseTime);
	static time_t SetTimeDSTCheckPreviousFormat(CString strTime);
	//-- 해당 년도까지 전체 날수 계산
	static long GetTotalCountOfDays(int nYear,int nMonth, int nDay);

	static void GetFileNameAndPath(CString strFullPath, CString &FileName, CString &strPath);	
	static BOOL GetSubFolderNames(CString strPath, vector<CString>& vecFolderName);
	static BOOL GetSubFolderNames(wstring strPath, vector<wstring>& vecFolderName);

	static int GetFindCharCount(CString parm_string, char parm_find_char) ;
	static BOOL FileExist(CString strPath);
	static CString CharToCString(char* str, BOOL bDelete = FALSE);
	static char* CStringToChar(CString str);

	//jhhan 16-11-08
	static DWORD GetProcessPID(CString strProcess);
	static HWND GetWndHandle(DWORD dwPID);
	static BOOL GetIsFile(CString strAppPath, CString strAppName);
	static BOOL ExecuteProcess(CString strAppPath, CString strAppName, CString strParam = _T(""), BOOL bShow = TRUE);
	static CString GetIpAddress(CString strDomain);
	static void GetLocalIPAddressArray(CStringArray& strArr);

	static CString GetVersionInfo(HMODULE hLib, CString csEntry);
	static CString FormatVersion(CString cs);

	static BOOL KillProcess(CString strProcess);
};
