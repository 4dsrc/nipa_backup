////////////////////////////////////////////////////////////////////////////////
//
//	ESMTextProgressCtrl.cpp : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMTextProgressCtrl.h"
#include "ESMMemDC.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#pragma warning(disable:4996)

/////////////////////////////////////////////////////////////////////////////
// CESMTextProgressCtrl

CESMTextProgressCtrl::CESMTextProgressCtrl()
{
    m_nPos			= 0;
    m_nStepSize		= 1;
    m_nMax			= 100;
    m_nMin			= 0;
    m_bShowText		= TRUE;
    m_strText.Empty();
	m_colFore		= ::GetSysColor(COLOR_HIGHLIGHT);
	m_colBk			= ::GetSysColor(COLOR_WINDOW);
	m_colTextFore	= ::GetSysColor(COLOR_HIGHLIGHT);
	m_colTextBk		= ::GetSysColor(COLOR_WINDOW);

    m_nBarWidth = -1;
}

CESMTextProgressCtrl::~CESMTextProgressCtrl()
{
}

BEGIN_MESSAGE_MAP(CESMTextProgressCtrl, CProgressCtrl)
	//{{AFX_MSG_MAP(CESMTextProgressCtrl)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
    ON_MESSAGE(WM_SETTEXT, OnSetText)
    ON_MESSAGE(WM_GETTEXT, OnGetText)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CESMTextProgressCtrl message handlers

LRESULT CESMTextProgressCtrl::OnSetText(WPARAM, LPARAM szText)
{
    LRESULT result = Default();

    if ( (!(LPCTSTR)szText && m_strText.GetLength()) ||
         ((LPCTSTR)szText && (m_strText != (LPCTSTR)szText))   )
    {
        m_strText = (LPCTSTR)szText;
        Invalidate();
    }

    return result;
}

LRESULT CESMTextProgressCtrl::OnGetText(WPARAM cchTextMax, LPARAM szText)
{
    if (!_tcsncpy(LPTSTR(LPCTSTR(szText)), m_strText, (UINT)cchTextMax))
        return 0;
    else 
        return min((UINT)cchTextMax, (UINT) m_strText.GetLength());
}

BOOL CESMTextProgressCtrl::OnEraseBkgnd(CDC* /*pDC*/) 
{	
 	return TRUE;
}

void CESMTextProgressCtrl::OnSize(UINT nType, int cx, int cy) 
{
	CProgressCtrl::OnSize(nType, cx, cy);	
    m_nBarWidth	= -1;   // Force update if SetPos called
}

void CESMTextProgressCtrl::OnPaint() 
{
    if (m_nMin >= m_nMax) 
        return;

    CRect LeftRect, RightRect, rcClient;
    GetClientRect(rcClient);

    double Fraction = (double)(m_nPos - m_nMin) / ((double)(m_nMax - m_nMin));

	CPaintDC PaintDC(this); // device context for painting
    CESMMemDC dc(&PaintDC);	

    LeftRect = RightRect = rcClient;

    LeftRect.right = LeftRect.left + (int)((LeftRect.right - LeftRect.left)*Fraction);
    dc.FillSolidRect(LeftRect, m_colFore);

    RightRect.left = LeftRect.right;
    dc.FillSolidRect(RightRect, m_colBk);

    if (m_bShowText)
    {
        CString str;
        if (m_strText.GetLength())
            str = m_strText;
        else
            str.Format(_T("%d%%"), (int)(Fraction*100.0));

        dc.SetBkMode(TRANSPARENT);

        CRgn rgn;
        rgn.CreateRectRgn(LeftRect.left, LeftRect.top, LeftRect.right, LeftRect.bottom);
        dc.SelectClipRgn(&rgn);
        dc.SetTextColor(m_colTextBk);

		dc.DrawText(str, rcClient, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

        rgn.DeleteObject();
        rgn.CreateRectRgn(RightRect.left, RightRect.top, RightRect.right, RightRect.bottom);
        dc.SelectClipRgn(&rgn);
        dc.SetTextColor(m_colTextFore);

        dc.DrawText(str, rcClient, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
    }
}

void CESMTextProgressCtrl::SetForeColour(COLORREF col)
{
	m_colFore = col;
}

void CESMTextProgressCtrl::SetBkColour(COLORREF col)
{
	m_colBk = col;
}

void CESMTextProgressCtrl::SetTextForeColour(COLORREF col)
{
	m_colTextFore = col;
}

void CESMTextProgressCtrl::SetTextBkColour(COLORREF col)
{
	m_colTextBk = col;
}

COLORREF CESMTextProgressCtrl::GetForeColour()
{
	return m_colFore;
}

COLORREF CESMTextProgressCtrl::GetBkColour()
{
	return m_colBk;
}

COLORREF CESMTextProgressCtrl::GetTextForeColour()
{
	return m_colTextFore;
}

COLORREF CESMTextProgressCtrl::GetTextBkColour()
{
	return m_colTextBk;
}
/////////////////////////////////////////////////////////////////////////////
// CESMTextProgressCtrl message handlers

void CESMTextProgressCtrl::SetShowText(BOOL bShow)
{ 
    if (::IsWindow(m_hWnd) && m_bShowText != bShow)
        Invalidate();

    m_bShowText = bShow;
}


void CESMTextProgressCtrl::SetRange(int nLower, int nUpper)
{
    m_nMax = nUpper;
    m_nMin = nLower;
}

int CESMTextProgressCtrl::SetPos(int nPos) 
{	
    if (!::IsWindow(m_hWnd))
        return -1;

    int nOldPos = m_nPos;
    m_nPos = nPos;

    CRect rect;
    GetClientRect(rect);

    double Fraction = (double)(m_nPos - m_nMin) / ((double)(m_nMax - m_nMin));
    int nBarWidth = (int) (Fraction * rect.Width());

    if (nBarWidth != m_nBarWidth)
    {
        m_nBarWidth = nBarWidth;
        RedrawWindow();
    }

    return nOldPos;
}

int CESMTextProgressCtrl::StepIt() 
{	
   return SetPos(m_nPos + m_nStepSize);
}

int CESMTextProgressCtrl::OffsetPos(int nPos)
{
    return SetPos(m_nPos + nPos);
}

int CESMTextProgressCtrl::SetStep(int nStep)
{
    int nOldStep = m_nStepSize;
    m_nStepSize = nStep;
    return nOldStep;
}
