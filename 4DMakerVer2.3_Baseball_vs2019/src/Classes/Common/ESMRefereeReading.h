#pragma once

#include "resource.h"
#include <WinSock2.h>
#include <Windows.h>
#include <process.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include <strsafe.h>

#include "ESMRCManager.h"
#include "ESMFunc.h"
#include "ESMIndexStructure.h"
#include "stdAfx.h"
//#include "ESMUtilMultiplexerDlg.h"
struct REFEREE_MUX_INFO
{
	REFEREE_MUX_INFO()
	{
		nMovieIdx = -1;
		nFrameIdx = -1;
		nGroupIdx = -1;
	}
	int nMovieIdx;
	int nFrameIdx;
	int nGroupIdx;
};
class CESMRefereeReading
{
public:
	CESMRefereeReading();
	~CESMRefereeReading();

	/*Real-time Connection*/
	void SetOpenClose(BOOL b,int nGroupIdx);
	BOOL GetOpenClose(int nGroupIdx){return m_bOpenClose;}
	void RunReceiveThread(char* pData,int nSecIdx,int nGrpIdx,int nBodySize,CString strPath);
	static unsigned WINAPI _ReceiveThread(LPVOID param);
	void SendData(int nSecIdx);
	int GetLiveConnectCnt(){return m_mapLiveMgr.size();}
	static unsigned WINAPI _RunSendThread(LPVOID param);
	void SetSendThreadStop(BOOL b){m_bSendThreadStop = b;}
	BOOL GetSendThreadStop(){return m_bSendThreadStop;}
	/*Mux Method*/
	//void SetMultiplexerDlg(CESMUtilMultiplexerDlg* pMgr){m_pMultiplexer = pMgr;}
	void AddEvent(int nGroupIdx,int nMovieIdx,int nFrameIdx);
	static unsigned WINAPI _ReceiveMsgThread(LPVOID param);
	void LaunchMultiplexer(int nGroupIdx,int nMovieIdx,int nFrameIdx);
	void SetMuxFinish(BOOL b){m_bMuxFinish = b;}
	BOOL GetMuxFinish(){return m_bMuxFinish;}
	void SetFinishMuxPath(CString strPath){m_strMuxFinishPath = strPath;}
	CString GetFinishMuxPath(){return m_strMuxFinishPath;}
	void SetIndex(int nMovieIdx,int nFrameIdx){m_nMuxFirstMovieIdx = nMovieIdx,m_nMuxFirstFrameIdx = nFrameIdx;}
	/*4KMovie Send*/
	void Launch4KMovieSender(int nGroupIdx,int nCameraIdx,int nFrameIdx);
	vector<CString> GetRefereeMoviePath(int nGroupIdx,int nDSCIndex,int nMovieIdx);
	static unsigned WINAPI _SendOriginalThread(LPVOID param);
	
	/*Send Finish Data*/
	void RequestData(CString strPath,int message);

private:
	/*Real-time Connection*/
	map<int,int> m_mapSend;
	CRITICAL_SECTION m_criSend;
	BOOL m_bOpenClose;
	
	/*TEST*/
	map<int,BOOL> m_mapLiveMgr;
	/*Mux Method*/
	//CESMUtilMultiplexerDlg* m_pMultiplexer;
	vector<REFEREE_MUX_INFO>*m_pArrMUX;
	CESMArray	*m_arMsg;
	BOOL m_bThreadStop;
	CRITICAL_SECTION m_cri;
	BOOL m_bMuxFinish;
	int m_nLiveConnectCnt;
	CString m_strMuxFinishPath;
	int m_nMuxFirstMovieIdx;
	int m_nMuxFirstFrameIdx;
	BOOL m_bSendThreadStop;
};
struct RECEIVE_DATA
{
	RECEIVE_DATA()
	{
		pParent = NULL;
		pData = NULL;
		nSecIdx = -1;
		nGroupIdx = -1;
		nBodySize = 0;
		bFinish = FALSE;
	}
	CESMRefereeReading* pParent;
	char* pData;
	int nSecIdx;
	int nGroupIdx;
	int nBodySize;
	CString strPath;
	BOOL bFinish;
};
struct SEND_ORIGINAL_DATA
{
	SEND_ORIGINAL_DATA()
	{
		pMgr = NULL;
		nGroupIdx = -1;
		nCameraIdx = -1;
		nFirstMovieIdx = -1;
		nLastMovieIdx = -1;
	}
	CESMRefereeReading* pMgr;
	int nGroupIdx;
	int nCameraIdx;
	int nFirstMovieIdx;
	int nLastMovieIdx;
};
struct SEND_LIVE_DATA
{
	SEND_LIVE_DATA()
	{
		pMgr = NULL;
		nSecIdx = -1;
	}
	CESMRefereeReading* pMgr;
	int nSecIdx;
};