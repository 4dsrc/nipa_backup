////////////////////////////////////////////////////////////////////////////////
//
//	ESMFunc.h : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-18
//
////////////////////////////////////////////////////////////////////////////
////

#pragma once

#include <afxdb.h>
#include <vector>
#include <map>

#include "ESMIndex.h"
#include "ESMDefine.h"
extern int movie_frame_per_second;
extern int movie_next_frame_time;
#define movie_next_frame_time_margin	2

//-- __int64 type
/*
__int64 start,freq,end;
#define CHECK_TIME_START QueryPerformanceFrequency((_LARGE_INTEGER*)&freq);QueryPerformanceCounter((_LARGE_INTEGER*)&start)
#define CHECK_TIME_END(a) QueryPerformanceFrequency((_LARGE_INTEGER*)&freq);QueryPerformanceCounter((_LARGE_INTEGER*)&end); a=(float)((float)(end-start)/freq)
#define GET_TICK_TIME(t) QueryPerformanceFrequency((_LARGE_INTEGER*)&freq);QueryPerformanceCounter((_LARGE_INTEGER*)&start)
*/

extern	void ESMLog (int nVerbosity, LPCTSTR lpszFormat, ...);
extern	void ESMRegist();
extern	HWND ESMGetMainWnd();
extern	void ESMSleep();
//-- 2012-09-12 joonho.kim
extern	int		ESMPecentToCnt( int nTarget, int nPercent );
//-- 2009-05-25
extern CString  ESMGetFrameRecord();
extern void		ESMSetFrameRecord(CString strRecord);
extern	int		ESMGetValue(int nType);
extern	float	ESMGetFValue(int nType);
extern	void	ESMSetValue(int nType, int nValue = 0);
extern	void	ESMGetMovieTime(int& nTargetTime, int &nSec, int &nMilli, BOOL bNext = TRUE);
extern	void	ESMGetNextFrameTime(int &nTime);
extern	void	ESMGetPreviousFrameTime(int &nTime);
extern	void	ESMCheckFrameTime(int &nTime);
extern	int		ESMGetCntMovieTime(int nDurationTime);
extern	int		ESMGetFrameIndex(int nTargetTime);
extern	int		ESMCheckNetwork(CString strDscIP);
extern	CString ESMGetMoviePath(CString strDscId, int nFrameIndex, BOOL bLocal = FALSE, BOOL bFrameViewer = FALSE);
extern	CString ESMGetPicturePath(CString strDscId);
extern	void	ESMGetMovieIndex(int nIndex, int& nMovieIndex, int& nRealFrameIndex);
extern	int		ESMGetFrameTime(int nIndex);
extern	int		ESMGetRecTime(BOOL bInit = FALSE);
extern	void	ESMSetRecTime(int nRunningTime);
extern	int		ESMGetDSCCount();
extern	BOOL	ESMGetExecuteMode();
extern	void	ESMSetExecuteMode(BOOL bExecuteMovieMode);
extern	void	ESMGetSelectedFrame(CString &strDsc, int &nTime);
extern	int		ESMGetDSCIndex(CString strDsc);
//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가
extern	CBitmap* ESMGetImageFromImageLoader(CString strDSC, int nFIdx, CDC* pDC, int nWidth, int nHeight);
extern	BOOL	ESMIsImageFromImageLoader(CString strDSC, int nFIdx);
extern	CString ESMGetMiddleDSCSearch(int nOrderDirection);
//-- 2013-10-07 hongsu@esmlab.com
//-- 3D Movie 
extern	void	ESMGet3DInfo(ESM3DInfo* p3DInfo);

extern	CString ESMGetOption(int nType);
extern	void	ESMSetOption(int nType, CString str);

extern	void	ESMGetSerPropertyOption(int nType, int nOption, void *pOutPut);
extern	void	ESMSetSerPropertyOption(int nType,int nOption, void * pParam);
extern	void	ESMSerInfoInit(int nType, int nPort);
extern	CString ESMGetFullPath(CString strPath);
//-- 2009-05-22
extern	CString ESMGetSymbolPath(CString strPath);
//-- 2009-05-20
extern	CString ESMGetPath(int nDir, BOOL bFull = TRUE);
extern	int ESMGetShiftFrame(CString strDSCId);
extern	CString ESMGetDataPath(CString strLoaction = _T(""));
extern	CString ESMGetRoot(CString strPath);
extern	CString ESMGetCeremony(int nDir);
//-- 2016-08-02
extern	CString ESMGetManageMent(int nDir);

//-- 2009-05-25
extern	CString MakeImagePath(int nOpt, CString strTC, int nStep = 0, int nType = IMG_TYPE_BMP, CString strModel = _T(""));
extern	CString GetImageFileType(int nType);	
//-- 2009-05-26
extern  void ReloadProperty(int nRow, BOOL bReload = FALSE);
//-- 2009-06-27
extern  void ESMWaitCursor(BOOL bStart = TRUE);
//-- 2009-05-27
extern  CString ChangePathForRecord(CString strFile);
//-- 2009-07-11
extern void ESMSetTestTime(BOOL bStart = TRUE);
//-- 2009-08-18
extern void DeleteFiles(CString strFile);
extern void PreparePath(CString &sPath);
//-- 2009-09-02
extern void SetTCDateTime();
//-- 2009-09-29
extern BOOL ESMTestPlay();
//-- 2009-11-19 hongsu.jung
//-- IsExistProcess
extern BOOL IsExistProcess(CString strProc);

//-- 2010-2-23 hongsu.jung
//-- Find Type
extern int FindType(CString strPath);

//-- 2010-3-2 hongsu.jung
//-- GET TEST MANAGER
extern PVOID ESMGetTestManager();

//-- 2010-3-3 hongsu.jung
//-- Check Connection
extern BOOL ESMTargetConnect();


//-- 2010-3-22 hongsu.jung
//-- Get TaskName
extern void ESMAddStatusInfo(int nID, CString strName);
//-- 2010-3-26 hongsu.jung
//-- UUID
extern CString ESMGetUniqueID();
//-- 2010-3-26 hongsu.jung
//-- GET SELECT
extern CString ESMSqlQuery(int nQuery, CString strQuery);

//-- 2012-10-29 joonho.kim
extern CString ESMGetLocalIP();
extern void ESMSetLocalIP(CString strLocalIP);

extern CString ESMGetTCFile();
extern void ESMSetTCFile(CString str); 

//-- Add Function
extern void ESMCreateAllDirectories(CString csPath);
extern BOOL FileExists(LPCTSTR Path);
extern CString GetNetworkInfo();
extern CString RemoveSlash(LPCTSTR Path);

//-- 2010-6-12 hongsu.jung
//-- Get Date For Insert Date
extern CString ESMGetInsertDate();
extern CString ESMGetTime();
extern CString ESMGetDate();

extern BOOL RegDelValue(HKEY hKey, LPCTSTR lpKey, LPCTSTR lpValue);
extern BOOL ClipCopy(CString txt);

extern BOOL ESMListFocusRow(int nStep);
extern BOOL ESMTestSetStatus(int nStatus, void* pObj);
extern BOOL ESMTestPause();

extern BOOL ESMMonItemDataToExcel(CDatabase* pDB,int nTickcount,CString nTaskname);
extern CString ESMGetSystemErrorString(DWORD nErroCode);
extern void ESMWcharCopy(char* pstrdst, TCHAR* pstrsrc);

//-- 2011-11-26 hongsu.jung
//-- Get Time
extern void ESMSetTime(int nTime);
extern CString ESMGetSaveTime(int nTime);

//-- 2012-04-06 hongsu.jung
extern BOOL ESMDBStatus();
extern int ESMGetMessageCnt();

//-- 2012-04-25 hongsu
extern BOOL ESMGetRecList();
extern void ESMSetRecList(BOOL bState);
extern BOOL ESMGetRecListCnt();

//-- 2012-08-02 hongsu@esmlab.com
//-- Show Message Comment 
extern void ESMMessageBox(BOOL bShow, CString strMessage = _T(""));

//-- 2013-01-23 ChangdoKim
extern float ESMGetVersion(float fVersion);
extern int ESMGetFilmState();
extern void ESMSetFilmState(int nTakeType);
//-- 2018-03-09 hjcho
extern void ESMSaveVersionInfo(float fVersion);
extern float ESMGetVersionInfo();

extern void ESMSetInvalidMovieSize(BOOL bInvalid);
extern BOOL ESMGetInvalidMovieSize();
extern void ESMSetMovieSize(int nSize);
extern int ESMGetMovieSize();
extern int ESMGetMovieFlag();
extern int ESMGetVMCCFlag();
extern int ESMGetCheckValid();
extern int ESMGetBaseBallFlag();
extern void ESMSetBaseBallTamplate(char key);
extern char ESMGetBaseBallTamplate();
extern int ESMGetBaseBallInning();
extern int ESMGetReverseMovie();
extern void ESMSetKeyValue(char ckey);
extern char ESMGetKeyValue();
extern int ESMGetGPUMakeFile();
extern int ESMGetGPUMakeFileCount();
extern void ESMSetGPUMakeFileCount(int nCount);
extern void ESMInitGPUMakeFileCount();
//extern void ESMSetDeviceDSCID(CString strID);
//extern CString ESMGetDeviceDSCID();

extern BOOL ESMGetMakeServerFlag();


extern void		ESMSetScroll(int nScroll);
extern int		ESMGetScroll();
extern int		ESMFileCount(CString strPath, ESMAdjustMovie& info);
extern int		ESMFileSortInfo(CString strPath, ESMAdjustMovie& info);
extern int		ESMFolderCount(CString strPath, ESMAdjustMovie& info);
extern void  ESMGetAdjustMovieInfo(ESMAdjustMovie& info);

extern void ESMInitSelectPointNum();
extern void ESMSetSelectPointNum(int nNum);
extern int  ESMGetSelectPointNum();

extern void ESMInitViewPointNum();
extern void ESMSetViewPointNum(int nNum);
extern int  ESMGetViewPointNum();
extern void* ESMGetMovieMgr();

extern BOOL ESMGetGPUDecodeSupport();
// 2016-03-12 jaewan - get cuda support flag
extern BOOL ESMGetCudaSupport();
//2016-05-17 jaewan - getMaking Use flag
extern BOOL ESMGetMakingUse();
extern void ESMSetMakingUse(BOOL makinguseflag);

extern vector<CString> ESMGetDeleteDSC();
extern vector<CString> ESMGetDeleteDSCList();
extern void ESMSetDeleteDSCList(vector<CString> m_list);

extern void ESMSetMakeMovie(BOOL bSet);
extern BOOL ESMGetMakeMovie();
extern BOOL ESMGetSyncFlag();
extern void ESMSetSyncFlag(BOOL bSet);

//jhhan 181004 Adjust Delete
extern void ESMSetMakeAdjust(BOOL bSet);
extern BOOL ESMGetMakeAdjust();

//------------------------------------------------------------------------------
//-- Date	 : 2010-3-30
//-- Owner	 : hongsu.jung
//-- Comment : Locking Array 
//------------------------------------------------------------------------------
class CESMArray : public CObArray
{
public:
	CESMArray()		{ InitializeCriticalSection (&_ESMArray);	}
	~CESMArray()	{ DeleteCriticalSection (&_ESMArray);		}

private:
	CRITICAL_SECTION _ESMArray;

public:
	int Add(CObject* p)
	{
		int nReturn;
		EnterCriticalSection (&_ESMArray);
		nReturn = (int)CObArray::Add(p);
		LeaveCriticalSection (&_ESMArray);
		return nReturn;
	}

	int GetSize()
	{
		int nReturn;
		EnterCriticalSection (&_ESMArray);
		nReturn = (int)CObArray::GetSize();
		LeaveCriticalSection (&_ESMArray);
		return nReturn;
	}

	CObject* GetAt(int nIndex)
	{
		CObject* pReturn = NULL;
		EnterCriticalSection (&_ESMArray);
		pReturn = (CObject*)CObArray::GetAt(nIndex);
		LeaveCriticalSection (&_ESMArray);
		return pReturn;
	}

	void RemoveAt(int nIndex)
	{
		EnterCriticalSection (&_ESMArray);
		CObArray::RemoveAt(nIndex);
		LeaveCriticalSection (&_ESMArray);
	}
};

extern BOOL ESMStringCompare(CString strReference, CString strTarget);

extern CString ESMGetModel();
extern void ESMSetModel(CString strModel);
//2012-07-24 joonho.kim
extern CString ESMGetMega();
extern void ESMSetMega(CString strMega);
extern CString ESMGetFWModel();
extern void ESMSetFWModel(CString strFWModel);
extern CString ESMGetPort();
extern void ESMSetPort(CString strFWModel);
extern int ESMGetIDfromIP(CString strIP);
extern CString ESMGetIPFromDSCID(CString strID);
extern CString ESMGetIPFromDSCIDInFrameViewer(CString strID);

extern CString ESMGetMACAddress(CString strIp);
extern void ESMSetMACAddress(CString strMACAddress);
extern CString ESMGetFrimwareVersion();
extern void ESMSetFrimwareVersion(CString strFrimwareVersion);
extern BOOL ESMSendMessageToDB(UINT unMessage, PVOID pInfo, BOOL bDeleteArray = TRUE);

extern CString ESMGetCategory();
extern void ESMSetCategory(CString strCategory);
extern CString ESMGetCategorySub();
extern void ESMSetCategorySub(CString strCategorySub);

//-- 2012-10-11 joonho.kim
extern CString ESMGetEncoding();
extern void ESMSetEncoding(CString strEncoding);
extern DWORD ESMGetSequence();
extern void ESMSetSequence(DWORD dwSeq);
extern DWORD ESMGetPreTime();
extern void ESMSetPreTime(DWORD dwPreTime);
extern DWORD ESMGetNextTime();
extern void ESMSetNextTime(DWORD dwNextTime);

extern SYSTEMTIME ESMGetSystemLocalTime();
extern int ESMGetDSTChangeTime(BOOL bPC);
extern int ESMSetTimeDSTCheck(SYSTEMTIME tTime, CString strStartTime, CString strEndTime);
extern void ESMChangeTime(SYSTEMTIME& tTime, int nHour);

//-- 2012-03-11 hongsu@esmlab.com
//-- Temp File
extern void ESMSetDeviceTimeInfoGMT(CString strGMT);
extern CString ESMGetDeviceTimeInfoGMT();

//-- 2012-04-27 hongsu
//-- Get 
extern CString ESMGetConvertLanguage(int nItem, CString strKey);

//-- 2012-05-03 hongsu
extern CString ESMRunTCID(BOOL bReduce = FALSE);

extern BOOL WaitResultResponse();
extern BOOL IsWaitStatus();

extern BOOL WaitResultResponseSerial();
extern BOOL IsWaitStatusSerial();

extern void ESMGetProfileArray(int nItem, CStringArray& arData, CStringArray& arList);
extern void ESMProfileGetValidData(CStringArray& arValid, CStringArray& arData);
extern void ESMProfileGetOptionValidData(CStringArray& arValid, CStringArray& arData);
extern void ESMLoadProfileData(CString strModel);
extern BOOL ESMLoadProfileCheck(CString strModel);
extern CString GetTrimString( CString strData , CString strSrc , CString strDest );

extern BOOL ESMProfileValidCheck(CStringArray& arValid,  CStringArray& arData);
extern BOOL ESMProfileOptionValidCheck(CStringArray& arValid, CStringArray& arData);

//-- 2012-10-08 joonho.kim
extern void ESMSetImage( LPVOID pImage );
extern LPVOID ESMGetImage();

extern CString GetPropertyValuetoStr(int nPropCode, int nValue, CString* pstrModel);
extern void ESMGetDSCList(CObArray* pAr);
extern int  ESMGetDSCSavedTime();

//-- 2013-10-23 hongsu@esmlab.com
//-- Get Tick Count
extern int ESMGetTick();
extern void ESMGetTickInfo(LARGE_INTEGER& swFreq, LARGE_INTEGER& swStart);

//-- Get Local IPAddr
extern CString ESMGetLocalIP();

extern CString ESMGetSaveImagePath();
extern CString ESMGetServerRamPath();
extern CString ESMGetClientRamPath(CString strLoaction = _T(""));
extern CString ESMGetRecordRamPath(CString strLoaction = _T(""));
extern CString ESMGetClientBackupRamPath(CString strLoaction = _T(""));
extern CString ESMGetClientRecordPath(CString strLoaction = _T(""));
extern CString ESMGetDeleteFolder();
extern void ESMSetDeleteFolder(CString strFolder);

extern void ESMLoadProfileSelectPoint(CString strProfilePath);
extern void ESMSetRecordingInfoint(CString strKeyName, int nValue);
extern void ESMSetRecordingInfoString(CString strKeyName, CString strValue);
extern int  ESMGetRecordingInfoInt(CString strKeyName, int nDefault);
extern CString ESMGetRecordingInfoString(CString strKeyName, CString strDefault);

extern void ESMSetCeremonyInfoString(CString strKeyName, CString strValue);
extern CString ESMGetCeremonyInfoString(CString strKeyName, CString strDefault = _T(""));
extern void ESMSetCeremonyInfoint(CString strKeyName, int nValue);

//170320 hjcho
extern void ESMSetRecordingPoint(CString strKeyName,int nValue);
extern BOOL ESMGetGPUCheck();
extern void ESMSetGPUCheck(BOOL bGPU);

extern CString ESMGetModuleFileName();

//CMiLRe 20151013 Template Load INI File
extern void LoadTemplateFile(CString strTemplateFileName, int nNum = 0, BOOL bRecovery = FALSE);
//CMiLRe 20151014 Template Save INI File
extern void ResetLoadTemplate();

//CMiLRe 20151119 동영상 제작 중 Flag 추가
extern void SetMakingMovieFlag(BOOL bValue);
extern BOOL GetMakingMovieFlag();

extern int ESMGetRecState();
extern void ESMSetAdjustPath(CString strPath);
extern CString ESMGetAdjustPath();

extern void ESMSetSelectDSC(CString strDSC);
extern CString ESMGetSelectDSC();

extern void ESMSetFirstConnect(BOOL bSet);
extern BOOL ESMGetFirstConnect();

//CMiLRe 20160128 카메라 연결 count
extern void ESMSetTotalCameraCount(int nValue);
extern int ESMGetTotalCameraCount();

extern void ESMSetCeremonyDBConnect(BOOL bConnect);
extern BOOL ESMGetCeremonyDBConnect();

extern int ESMGetFrameMargin();
extern vector<ESMFrameArray*> ESMGetFrameArray();

extern void ESMInitFrameArray(int nWidth, int nHeight, int ori_col, int ori_row);
extern void ESMSetFrameDecodingFlag(BOOL bFlag);
extern BOOL ESMGetFrameDecodingFlag();
extern void ESMSetMovieGOPMode();

//2016-06-17 hjcho - frameview UI
extern track_info ESMGetTrackInfo();
extern void ESMSetTrackInfo(track_info tinfo);
//extern axis_info ESMGetAxis_Info();
//extern void ESMSetAxis_Info(axis_info ainfo);

//2016-08-25 hjcho - frameview UI
//extern axis_info1 ESMGetAxis_Info1();
//extern void ESMSetAxis_Info1(axis_info1 ainfo1);
//extern ESMAutoATRemote ESMGetATStart();
//extern void ESMSetATStart(ESMAutoATRemote ATSet);

//2016-12-08 hjcho - Frameview Adjust
extern void ESMSetFrameAdjust(BOOL mSet);
extern BOOL ESMGetFrameAdjust();

//2017-03-15 hjcho - Making Time Viewer
extern CString ESMGetCurTime();
extern BOOL ESMGetLoadingFrame();
extern void ESMSetLoadingFrame(BOOL bSet);

extern void ESMDisconnect();
extern void ESMDSCRemoveAll();

//jhhan
extern void ESMSet4DMPath(CString strPath);
extern CString ESMGet4DMPath();
extern void ESMSetLoadRecord(BOOL bSet);
extern BOOL ESMGetLoadRecord();
extern void ESMDSCRemoveAllList();
extern void ESMGetPropertyListView(CString strDscID = _T(""));
extern void ESMReloadCamList();
extern CString ESMGetPropertyValue(int nValue);
extern void ESMSetPropertyValue(int nValue, CString str);

//jhhan 16-10-11
extern vector<CString> ESMGetMakeDeleteDSC();
extern vector<CString> ESMGetMakeDeleteDSCList();
extern void ESMSetMakeDeleteDSCList(vector<CString> m_list);
extern BOOL ESMGetMakeDelete(CString str);

extern void ESMSendFrameDataInfo();
extern void ESMSendFrameDataInfo(BOOL bOpen);

extern CString ESMGetDSCIDFromPath(CString strPath);

extern void ESMSetAdjustLoad(BOOL bLoad);
extern BOOL ESMGetAdjustLoad();

//jhhan 16-11-21 LastPointNum
extern void ESMInitLastSelectPointNum();
extern void ESMSetLastSelectPointNum(int nNum);
extern int  ESMGetLastSelectPointNum();
extern void ESMSetFrameViewFlag(BOOL bFlag);
extern BOOL ESMGetFrameViewFlag();
//jhhan 16-11-25 SCALING
extern BOOL ESMGetViewScaling();

//jhhan 16-11-29 SecIdx
extern CString ESMGetSecIdxFromPath(CString strPath);

//jhhan 16-12-05 TitleBar Init
extern void ESMInitTitleBar();

//jhhan 16-12-06 MovieCheck
extern int ESMGetMovieMgrCnt();

extern void ESMSet4DAFileInit(CString strSourceDSC);

extern CString ESMGet4DAPath(CString strDSCId);

extern void ESMMovieRTThread(CString strIP, CString strDSC);

//joonho.kim 17-02-17
extern making_info* ESMGetMakingInfo();
extern void ESMSetMakingInfo(making_info info);
extern BOOL ESMGetMaingInfoFlag();
extern void ESMSetMaingInfoFlag(BOOL bFlag);

extern void ESMSetGridYPoint(int nY);
extern int	ESMGetGridYPoint();

//jhhan 17-03-24 RecordFrameInfo
extern void ESMInitCheckFrame();
extern void ESMSetCheckFrame(CString strKey);
extern BOOL ESMGetCheckFrame(CString strKey);


extern void ESMSetDump(BOOL bSet);
//(CAMREVISION)
extern void ESMSetRecordStatus(BOOL bSet);
extern BOOL ESMGetRecordStatus();
extern void ESMSetNextRecordStatus(BOOL bSet);
extern BOOL ESMGetNextRecordStatus();

extern void ESMSetSenderGap(int nTime);
extern int	ESMGetSenderGap();

extern void ESMSetMakeDeleteDSC(CString strDSC,BOOL bDelete/* = FALSE*/);

//wgkim 17-05-23 Template
extern void ESMSetCurrentTemplateVector(vector<TEMPLATE_STRUCT> templateVector);
extern vector<TEMPLATE_STRUCT> ESMGetCurrentTemplateVector();

extern void ESMSetFrameRate(int nFPS);
extern int ESMGetFrameRate(BOOL bMode = FALSE); //TRUE:MovieMgr, FALSE:Default

//hjcho 17-07-28 hjcho
extern void ESMSetRecordState(int nSet);
extern int  ESMGetRecordState();

//wgkim 170728
extern void ESMSetSelectedFrameNumber(int nSelectedFrame);
extern int ESMGetSelectedFrameNumber();
extern int ESMGetGopSize();

extern BOOL ESMPeekAndPump();
extern CString ESMGetFrontIP();

extern void ESMSetWait(DWORD dwMillisec);

extern int ESMGetMarginX();
extern int ESMGetMarginY();

//HJCHO 170901
extern void ESMSetEventEmail(CString str);
extern CString ESMGetEventEmail();

extern void ESMSetProperty1Step(int nNum);

extern BOOL ESMSet4DPMgr(CString strDSCId);
extern void ESMSetDelete4DPMgr(int nRemoteID = 0);
extern BOOL ESMSet4DPTransmit(CString strPath);

extern BOOL ESMGetNetworkEx();

//KT 전송 동기화
extern void ESMSetKTSendSyncTime(int nTime);
extern int  ESMGetKTSendSyncTime();
extern void ESMSetKTSendStopTime(int nCount);

//hjcho 171010
extern void ESMMovieRTSend(CString strIP, CString strDSC,BOOL bRefereeMode = FALSE);
//hjcho 171106
extern void ESMMovieRTSend60p(CString strIP, CString strDSC);

//jhhan 171019
extern BOOL ESMGetRemoteUse();
extern CString ESMGetRemoteIP();
extern BOOL ESMGetRemoteSkip();
//hjcho 171020
extern CString ESMDoTranscodeAs720p(CString strPath);
extern int ESMGetFrameCount(CString strPath);
extern int ESMGetRecordTime();

extern void ESMSetRemoteSkip(BOOL bSkip);
extern void ESMSetRemoteState(BOOL bState);

extern void ESMCmdRemote(UINT nMsg, int nCmd = 0);

extern void ESMCalcAdjustDataAsText(CString strCAMID,FILE* pFile,double dRatio);
extern int ESMRound(double dValue);extern void ESMSetInitialTime(int nTime);
extern int ESMGetInitialTime();
extern void ESMSetMakeMovieTime(int nTime);
extern int ESMGetMakeMovieTime();
extern void ESMSetStartMakeTime(int nTime, int nGap);
extern int ESMGetStartMakeTime();
extern int ESMGetGapTime();

//wgkim 171110
extern map<CString, int> ESMGetArrGapAvg();
extern void ESMSetArrGapAvg(map<CString, int> nArrAvgGap);
extern void ESMClearArrGapAvg();


extern void ESMRemoteRecord(int nData);
extern void ESMRemoteRecordStop();
extern void ESMRemoteMake(int nData);

extern int ESMConvertTime(int nTime);

//hjcho 171126
extern void ESMSetRemoteTime(int nTime);

extern int ESMReferenceTime();

extern void		ESMSetDelayMode( CString strMode);
extern CString	ESMGetDelayMode();
extern void		ESMSetTemplatePage(CString strPage);
extern CString	ESMGetTemplatePage();
extern void		ESMSetPropertyCheck(BOOL bStatus);
extern BOOL		ESMGetPropertyCheck();
extern void		ESMSetDelayDirection(CString strDirection);
extern CString	ESMGetDelayDirection();
extern int	    ESMGetTimeFromIndex(int nMovieIdx,int nFrameIdx);

extern BOOL ESMGetProcessorShare();

extern void ESMDeleteFile(CString strPath);

extern int ESMGetFuncKey();
extern void ESMSetFuncKey(int nKey = VK_F1);

extern BOOL ESMGetNumValidate(CString str);

//wgkim 171222
extern void* ESMGetBackupMgr();
extern CString ESMGetBackupString(int nFlag);

//hjcho 171223
extern void ESMSetViewFocusAsRemote();
//hjcho 171229
static unsigned WINAPI RunTranscode(LPVOID param);

extern void ESMDeleteMovie();

//wgkim 180202
extern BOOL ESMGetRecordFileSplit();
extern void ESMSetRecordFileSplit(BOOL nRecordFileSplit);

extern void ESMReStartAgent();

//jhhan 180307
extern void ESMSetDevice(int nDevice);
extern int ESMGetDevice();
extern int ESMGetFramePerSec();
extern void ESMSetIdentify(int nId);
extern int ESMGetIdentify();
extern int ESMGetDivFrame();

extern void ESMVerifyInfo();

extern void ESMSetMuxSendCnt(int n);
extern void ESMSubMuxSendCnt();

extern int  ESMGetMuxSendCnt();

extern void ESMSetCopyObj(void* pObj );
extern void* ESMGetCopyObj();
extern int  ESMGetMuxSendCnt();

//180316 hjcho
extern int ESMGetMakeTotalFrame();
extern void ESMSetMakeTotalFrame(int n);

extern int ESMGetSelectedTime();
extern void ESMSetSelectedTime(int nTime);

extern BOOL ESMGetSyncObj();
extern void ESMSetSyncObj(BOOL bSync);

extern BOOL ESMGetEditObj();
extern void ESMSetEditObj(BOOL bSet);

extern void ESMGetDrawPos(vector<CPoint> &ptS, vector<CPoint> &ptE);
extern void ESMSetDrawPos(CPoint ptS, CPoint ptE);

extern int ESMGetXFromTime(int nTime);
extern int ESMGetYFromLine(int nLine);

extern void ESMDeleteAllDrawPos();

//jhhan 180411
extern BOOL ESMGetFile2Sec();
extern void ESMPushTime(int nTime);
extern void ESMClearPushTime();
extern CString ESMGetDSCIDFromIndex(int nIdx);
extern void ESMCreateStillMovie();

extern void ESMSetPushTime(int nMovieIdx,int nFrameIdx);
extern void ESMPushSelectTime();

extern BOOL CameraGroupLoad(CStringArray& strGroup);
extern void ESMInitDirection();
extern BOOL ESMSetDirection(CString strSection, CString strGroup, CString strDrirection);
extern CString ESMGetDirection(CString strSection, CString strGroup);

//hjcho 180508
extern CString ESMGetAJANetworkIP();
extern void ESMLaunchAJANetwork();
extern void ESMSendAJANetwork(ESMEvent* pMsg);
extern void ESMSetConnectAJANetwork(BOOL b);
extern BOOL ESMGetConnectAJANetwork();
extern BOOL ESMGetAJAMaking();
extern void ESMSetAJAMaking(BOOL b);

//wgkim 180515
extern CString ESMGetFrameViewerSplitVideo4dmPath();
extern BOOL ESMGetCheckIf4DPOrNot(CString strIP);

//wgkim 180625
extern CString ESMGetFrameViewerSplitVideoRootPath();

//hjcho 180531
extern BOOL ESMGetAJAScreen();
extern void ESMSetAJAScreen(BOOL b);
//hjcho 180602
extern CString ESMGetFileName(CString strPath);

extern void ESMSetFirstFlag(BOOL bFlag);
extern BOOL ESMGetFirstFlag();
//hjcho 180611
extern void ESMSetAJADivProcess(BOOL b);
extern BOOL ESMGetAJADivProcess();
//180629 hjcho
extern double ESMGetEncodingFrameRate();

extern void SortFileName(CStringArray& arrStr);
extern int CompareFileName(const void* p1, const void* p2);

extern void ESMSetStoredTime(int nTime);
extern vector<int>ESMGetStoredTime();
extern void ESMInitStoredTime();

//180927 hjcho
extern void ESMMovieRTMovie(CString strIP, CString strDSC,int nIdx,int nFrameRate = 30);
extern void ESMSetRTSPMovieSavePath(CString strPath);

extern void ESMSetViewJump(CString strData);
extern int ESMGetViewJump(int nValue, BOOL bUpDown = TRUE);

//181012 wgkim
extern BOOL ESMGetUsageKZone();

extern void ESMSetRecordSync(BOOL bSync = TRUE);
extern BOOL ESMGetRecordSync();

extern BOOL ESMLoadSelectPoint(int nPoint, CString strProfilePath);
extern void ESMLoadSquarePointData();

extern void ESMSetLiveViewLogFlag(BOOL b);		// liveview buffer 사용 로그 남기기위해...
extern int ESMGet4DAPSender(CString strIp);

extern BOOL ESMMemcpy(void * _Dst, const void * _Src, size_t _Size);
extern BOOL ESMGetDetectInfo(vector<DetectInfo> * _stList, int nDetectTime);

extern void ESMPropertyDiffCheck();
extern void ESMSetPropertyDiffCount(PropertyDiffCount values);
extern PropertyDiffCount ESMGetPropertyDiffCount();

extern void ESMPropertyMinMaxInfoCheck();
extern void ESMSetPropertyMinMaxInfo(PropertyMinMaxInfo values);
extern PropertyMinMaxInfo ESMGetPropertyMinMaxInfo();

extern void ESMSaveProfile();

extern vector<ESMFrameArray*> * ESMGetFrame(CString strKey);

extern void ESMVerfyMake();

extern void ESMSetDefaultCameraID(CString strID);	// 레코딩시 프로퍼티 기준 카메라.
extern CString ESMGetDefaultCameraID();

extern void ESMM3u8Reset();
extern vector<CString> ESMGetLiveList();
extern void ESMSetLiveList(vector<CString> m_list);
extern void ESMSendToLiveList();

extern void ESMTranscoding(CString strFile, int nMode, void * pM3u8);

extern void ESMCreateM3u8(CString strKey);
extern void * ESMGetM3u8(CString strKey, int nMode = 0);
extern void ESMDeleteAllM3u8();
extern void ESMResetAllM3u8();

//extern CESMm3u8 * ESMM3u8Test() {return NULL;}

//190222 hjcho
extern void ESMLaunchFFMPEGConsole(CString strOpt);
extern void ESMRTSPStateUpdate(CString str4DM,int nIndex,int nType,int nData);
extern void ESMSetRTSPCommand(RTSP_RESTFul_COMMAND command,int nCount = 0);
extern BOOL ESMGetRTSPBirdRecordState();
extern void ESMSendRTSPHttpCmd(int nCommand);
extern void ESMSetRTSPStopTime(CString str4DM,int n);
extern void ESMSetRTSPStillImageIdx(int nDSCIdx,int nSelectTime);
extern void ESMSendRTSPHttpSync(int nCamIdx);