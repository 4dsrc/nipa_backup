#include "stdafx.h"
#include "ESMFileMgr.h"
#include "ESMFunc.h"
#include "DSCViewer.h"
#include "DSCItem.h"
#include "MainFrm.h"
#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CMutex g_mtx4DM(FALSE, _T("4DM"));

CESMFileMgr::CESMFileMgr(CMainFrame* pMainFrm)
{
	m_pMainFrame = pMainFrm;
	m_hSaveProfileThread = NULL;
	m_bClose = FALSE;
	m_hFileCopyThread = NULL;
	m_nFilmState	= ESM_ADJ_FILM_STATE_MOVIE;
	m_bInit = FALSE;
}


CESMFileMgr::~CESMFileMgr(void)
{
	WaitForSingleObject(m_hFileCopyThread, 20000);
	CloseHandle(m_hSaveProfileThread);
}

void CESMFileMgr::SaveRecordProfile(CString strFileName)
{
	if(m_bClose) return;
	//-- 2013-10-18 hongsu@esmlab.com
	//-- Set Record Profile
	CString strSrcFolder;
	m_strTargetFolder.Format(_T("%s"),ESMGetPath(ESM_PATH_MOVIE_FILE));
	m_strTargetFile  = strFileName;

	int nAll = m_pMainFrame->m_wndDSCViewer.GetItemCount();
	CDSCItem* pItem = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = m_pMainFrame->m_wndDSCViewer.GetItemData(i);
		int j = 0;
		for(j = 0; j < m_arrIpArray.size(); j++)
		{
			if( m_arrIpArray.at(j) == pItem->GetDSCInfo(DSC_INFO_LOCATION))
				break;
		}
		if(m_arrIpArray.size() == j)
			m_arrIpArray.push_back(pItem->GetDSCInfo(DSC_INFO_LOCATION));
	}
	ESMLog(5, _T("[ESM] File Copy Start"));

	if(m_hSaveProfileThread)
	{
		CloseHandle(m_hSaveProfileThread);
		m_hSaveProfileThread = NULL;
	}

	/*CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	if(ESMGetExecuteMode())
		pMainWnd->m_wndDSCViewer.m_pDSCMgr->OnEventSyncDataSet();*/

	m_hSaveProfileThread = (HANDLE) _beginthreadex(NULL, 0, DoMovieSaveProfileThread, (void *)this, 0, NULL);
}

BOOL CESMFileMgr::MovieDoneSaveProfile(BOOL bAutoSave /*= FALSE*/)
{
	m_strTargetFile  = m_strRecordProfile;
	if(m_hSaveProfileThread)
	{
		CloseHandle(m_hSaveProfileThread);
		m_hSaveProfileThread = NULL;
	}
	/*if(bAutoSave)
	{
		m_bInit = FALSE;
		m_hSaveProfileThread = (HANDLE) _beginthreadex(NULL, 0, DoMovieSaveProfileExThread, (void *)this, 0, NULL);
	}
	else
		m_hSaveProfileThread = (HANDLE) _beginthreadex(NULL, 0, DoMovieSaveProfileThread, (void *)this, 0, NULL);*/

	return DoMovieSaveProfile(this);

}

void CESMFileMgr::PictureDoneSaveProfile()
{
	m_strTargetFile  = m_strRecordProfile;

	if(m_hSaveProfileThread)
	{
		CloseHandle(m_hSaveProfileThread);
		m_hSaveProfileThread = NULL;
	}
	m_hSaveProfileThread = (HANDLE) _beginthreadex(NULL, 0, DoPictureSaveProfileThread, (void *)this, 0, NULL);
}

unsigned WINAPI CESMFileMgr::FileCopyThread(LPVOID param)
{
	CESMFileMgr* pESMFileMgr = (CESMFileMgr*)param;
	CString strPath, strTp;
	for( int i =0 ;i < pESMFileMgr->m_arrIpArray.size(); i++)
	{
		strTp = ESMGetDataPath(pESMFileMgr->m_arrIpArray.at(i));
		strPath = strTp + _T("\\") + pESMFileMgr->m_strRecordProfile;
		CESMFileOperation fo;
		fo.Copy(strPath, pESMFileMgr->m_strTargetFolder,TRUE);
		fo.Delete(strPath, TRUE);
	}
	pESMFileMgr->m_arrIpArray.clear();
	ESMLog(0, _T("[Save Record] File Copy End"));
	return 0;
}

unsigned WINAPI CESMFileMgr::DoPictureSaveProfileThread(LPVOID param)
{
	CESMFileMgr* pESMFileMgr = (CESMFileMgr*)param;
	if(!pESMFileMgr) return 0;
	if(pESMFileMgr->m_bClose) return 0;

	CFile file;
	CString strFileName;

	strFileName.Format(_T("%s\\%s%s"),ESMGetPath(ESM_PATH_PICTURE), pESMFileMgr->m_strTargetFile, _T(".4dm"));
	if(!file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite |CFile::modeNoTruncate))
	{
		return 0;
	}
	CArchive ar(&file, CArchive::store);

	//-- Save DSCItem List
	CDSCViewer* pWndDSCViewer = &(pESMFileMgr->m_pMainFrame->m_wndDSCViewer);

//-- 2015-02-24 cygil@esmlab.com 4DModeler에서는 사용안함
#ifndef _4DMODEL
	CExtWFF<CTimeLineEditor>*	pWndTimeLineEditor = &(pESMFileMgr->m_pMainFrame->m_wndTimeLineEditor);	
#endif
	float nVersion = (float)ESM_4DMAKER_VERSION_CURRENTVER;
	ar<<nVersion;
	ar<<ESM_ADJ_FILM_STATE_PICTURE;
	ar<<pESMFileMgr->m_strRecordProfile;
	pWndDSCViewer->SaveDscInfo(ar);

//-- 2015-02-24 cygil@esmlab.com 4DModeler에서는 사용안함
#ifndef _4DMODEL
	pWndTimeLineEditor->SaveTimeLineInfo(ar);
#endif
	ESMLog(1,_T("[Save Record] Finish Save Record Profile [%s]") , strFileName);

	ar.Close();
	file.Close();
	return 0;
}

unsigned WINAPI CESMFileMgr::DoMovieSaveProfileThread(LPVOID param)
{
	g_mtx4DM.Lock();

	CESMFileMgr* pESMFileMgr = (CESMFileMgr*)param;
	if(!pESMFileMgr) return 0;
	if(pESMFileMgr->m_bClose) return 0;

	CFile file;
	CString strFileName;

	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
		strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_PICTURE), pESMFileMgr->m_strTargetFile);
	else if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_MOVIE), pESMFileMgr->m_strTargetFile);

	if( strFileName.Right(4) != _T(".4dm"))
		strFileName = strFileName + _T(".4dm");
	
	//jhhan 181220
	CString strTemp;
	strTemp.Format(_T("%s\\%s"),ESMGetClientRamPath(), pESMFileMgr->m_strTargetFile);
	if( strTemp.Right(4) != _T(".4dm"))
		strTemp = strTemp + _T(".4dm");

	int nFind = strFileName.ReverseFind('\\') + 1;
	strFileName = strFileName.Left(nFind);

	if(!file.Open(strTemp, CFile::modeCreate | CFile::modeReadWrite |CFile::modeNoTruncate))
	{
		return 0;
	}
	CArchive ar(&file, CArchive::store);
 	//-- Save DSCItem List
 	CDSCViewer* pWndDSCViewer = &(pESMFileMgr->m_pMainFrame->m_wndDSCViewer);

//-- 2015-02-24 cygil@esmlab.com 4DModeler에서는 사용안함
#ifndef _4DMODEL
 	CExtWFF<CTimeLineEditor>*	pWndTimeLineEditor = &(pESMFileMgr->m_pMainFrame->m_wndTimeLineEditor);	
#endif
 	float nVersion = (float)ESM_4DMAKER_VERSION_CURRENTVER;
 	ar<<nVersion;

	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
		ar<<ESM_ADJ_FILM_STATE_PICTURE;
	else if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		ar<<ESM_ADJ_FILM_STATE_MOVIE;

 	ar<<pESMFileMgr->m_strRecordProfile;
 	pWndDSCViewer->SaveDscInfo(ar,pESMFileMgr->m_bInit);

//-- 2015-02-24 cygil@esmlab.com 4DModeler에서는 사용안함
#ifndef _4DMODEL
 	pWndTimeLineEditor->SaveTimeLineInfo(ar);
#endif
	//17  프레임정보
	ar<<ESMGetFrameRate();
	
	//180308 장비정보
	ar<<ESMGetDevice();

 	ESMLog(1,_T("[Save Record] Finish Save Record Profile [%s]") , strFileName);

	ar.Close();
	file.Close();

	//Overwrite
	CESMFileOperation fo;
	fo.CopyEx(strTemp, strFileName);

	g_mtx4DM.Unlock();

	return 0;
}

BOOL CESMFileMgr::DoMovieSaveProfile(LPVOID param)
{
	CESMFileMgr* pESMFileMgr = (CESMFileMgr*)param;
	if(!pESMFileMgr) return 0;
	if(pESMFileMgr->m_bClose) return 0;

	CFile file;
	CString strFileName;

	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
		strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_PICTURE), pESMFileMgr->m_strTargetFile);
	else if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_MOVIE), pESMFileMgr->m_strTargetFile);

	if( strFileName.Right(4) != _T(".4dm"))
		strFileName = strFileName + _T(".4dm");

	//jhhan 181220
	CString strTemp;
	strTemp.Format(_T("%s\\%s"),ESMGetClientRamPath(), pESMFileMgr->m_strTargetFile);
	if( strTemp.Right(4) != _T(".4dm"))
		strTemp = strTemp + _T(".4dm");

	int nFind = strFileName.ReverseFind('\\') + 1;
	strFileName = strFileName.Left(nFind);

	if(!file.Open(strTemp, CFile::modeCreate | CFile::modeReadWrite |CFile::modeNoTruncate))
	{
		return FALSE;
	}

	CArchive ar(&file, CArchive::store);
	//-- Save DSCItem List

	try{
		CDSCViewer* pWndDSCViewer = &(pESMFileMgr->m_pMainFrame->m_wndDSCViewer);

		//-- 2015-02-24 cygil@esmlab.com 4DModeler에서는 사용안함
#ifndef _4DMODEL
		CExtWFF<CTimeLineEditor>*	pWndTimeLineEditor = &(pESMFileMgr->m_pMainFrame->m_wndTimeLineEditor);	
#endif
		float nVersion = (float)ESM_4DMAKER_VERSION_CURRENTVER;
		ar<<nVersion;

		if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
			ar<<ESM_ADJ_FILM_STATE_PICTURE;
		else if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
			ar<<ESM_ADJ_FILM_STATE_MOVIE;

		ar<<pESMFileMgr->m_strRecordProfile;
		pWndDSCViewer->SaveDscInfo(ar,pESMFileMgr->m_bInit);

		//-- 2015-02-24 cygil@esmlab.com 4DModeler에서는 사용안함
#ifndef _4DMODEL
		pWndTimeLineEditor->SaveTimeLineInfo(ar);
#endif
		//17  프레임정보
		ar<<ESMGetFrameRate();

		//180308 장비정보
		ar<<ESMGetDevice();

		ESMLog(1,_T("[Save Record] Finish Save Record Profile [%s]") , strFileName);

	}
	catch(CFileException *fe)
	{
		ESMLog(0,_T("Save 4DM Exception[%s] [%d]") , strFileName, fe->ReportError());
		ar.Close();
		file.Close();
		return FALSE;
	}
	catch(CArchiveException *ae)
	{
		ESMLog(0,_T("Save 4DM Exception[%s] [%d]") , strFileName, ae->ReportError());
		ar.Close();
		file.Close();
		return FALSE;
	}

	ar.Close();
	file.Close();

	//Overwrite
	CESMFileOperation fo;
	fo.CopyEx(strTemp, strFileName);


	return TRUE;
}

unsigned WINAPI CESMFileMgr::DoMovieSaveProfileExThread(LPVOID param)
{
	g_mtx4DM.Lock();

	CESMFileMgr* pESMFileMgr = (CESMFileMgr*)param;
	if(!pESMFileMgr) return 0;
	if(pESMFileMgr->m_bClose) return 0;


	CFile file;
	CString strFileName;

	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
		strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_PICTURE), pESMFileMgr->m_strTargetFile);
	else if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_MOVIE), pESMFileMgr->m_strTargetFile);

	if( strFileName.Right(4) != _T(".4dm"))
		strFileName = strFileName + _T(".4dm");

	//jhhan 181220
	CString strTemp;
	strTemp.Format(_T("%s\\%s"),ESMGetClientRamPath(), pESMFileMgr->m_strTargetFile);
	if( strTemp.Right(4) != _T(".4dm"))
		strTemp = strTemp + _T(".4dm");

	int nFind = strFileName.ReverseFind('\\') + 1;
	strFileName = strFileName.Left(nFind);


	if(!file.Open(strTemp, CFile::modeCreate | CFile::modeReadWrite |CFile::modeNoTruncate))
	{
		return 0;
	}
	CArchive ar(&file, CArchive::store);
	//-- Save DSCItem List
	CDSCViewer* pWndDSCViewer = &(pESMFileMgr->m_pMainFrame->m_wndDSCViewer);

	//-- 2015-02-24 cygil@esmlab.com 4DModeler에서는 사용안함
#ifndef _4DMODEL
	CExtWFF<CTimeLineEditor>*	pWndTimeLineEditor = &(pESMFileMgr->m_pMainFrame->m_wndTimeLineEditor);	
#endif
	float nVersion = (float)ESM_4DMAKER_VERSION_CURRENTVER;
	ar<<nVersion;

	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
		ar<<ESM_ADJ_FILM_STATE_PICTURE;
	else if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		ar<<ESM_ADJ_FILM_STATE_MOVIE;

	ar<<pESMFileMgr->m_strRecordProfile;
	pWndDSCViewer->SaveDscInfoEx(ar);

	//-- 2015-02-24 cygil@esmlab.com 4DModeler에서는 사용안함
#ifndef _4DMODEL
	pWndTimeLineEditor->SaveTimeLineInfo(ar);
#endif
	//17  프레임정보
	ar<<ESMGetFrameRate();

	//180308 장비정보
	ar<<ESMGetDevice();

	//ESMLog(1,_T("[Save Record] Finish Save Record Profile [%s]") , strFileName);

	ar.Close();
	file.Close();

	//Overwrite
	CESMFileOperation fo;
	fo.CopyEx(strTemp, strFileName);

	g_mtx4DM.Unlock();

	return 0;
}

void CESMFileMgr::SetCurTime()
{
	CTime time = CTime::GetCurrentTime();
	CString strProfile;
	strProfile.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());
	ESMSetFrameRecord(strProfile);

	//m_strRecordProfile.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());

	if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER && ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING
		|| ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER && ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_BOTH )
	{
		CString strEntry = _T("RecProfile");
		ESMSetRecordingInfoString(strEntry, strProfile/*m_strRecordProfile*/);
		ESMSetRecordingInfoint(_T("RecTime"), 0);
	}
}

//------------------------------------------------------------------------------
//! @function	Profile Manager (Load/Save)
//! @date		2013-10-02
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMFileMgr::LoadRecordProfile(CString strFileName)
{
	if(m_bClose) return;

	CDSCViewer* pWndDSCViewer = &(m_pMainFrame->m_wndDSCViewer);

//-- 2015-02-24 cygil@esmlab.com 4DModeler에서는 사용안함
#ifndef _4DMODEL
	CExtWFF<CTimeLineEditor>*	pWndTimeLineEditor = &(m_pMainFrame->m_wndTimeLineEditor);	
#endif

	CFile file;
	CString strFile;
	strFile.Format(_T("%s"), strFileName);
	if(!file.Open(strFile, CFile::modeRead))
	{
		ESMLog(0,_T("Open Record File [%s]"),strFile);
		return;
	}
	CArchive ar(&file, CArchive::load);

	TCHAR strTp[_MAX_EXT] = {0};
	TCHAR strFileTk[_MAX_FNAME] = {0};
	_tsplitpath(strFileName,strTp,strTp,strFileTk,strTp);


	ESMLog(1,_T("[Load Record] Start Record Profile [%s]") , strFile);
  	//-- 2013-10-08 hongsu@esmlab.com
  	//-- Reset Work Folder
  	//RemoveWorkFile();
  	//-- Reset Exist List
  	pWndDSCViewer->RemoveListAll();

	//180515 wgkim
	DeleteFiles(ESMGetFrameViewerSplitVideo4dmPath());

//-- 2015-02-24 cygil@esmlab.com 4DModeler에서는 사용안함
#ifndef _4DMODEL
	pWndTimeLineEditor->Reset();
#endif
	//-- 2014-9-18 hongsu@esmlab.com
	//-- Reset Valid Data
	ESMEvent* pMsg = new ESMEvent();  
	pMsg->message = WM_ESM_MOVIE_VALIDRESET;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);

//-- 2015-02-24 cygil@esmlab.com 4DModeler에서는 사용안함
#ifndef _4DMODEL
  	//-- 2013-10-20 hongsu@esmlab.com
  	//-- Clean TimeLineEditor
  	RemoveTimeLineEditor();
#endif
	//-- Save DSCItem List
  	int nAll;
  	ar>>nAll;
  
  	float nVersion = -20.0;
  	m_strRecordProfile = _T("");
	m_nFilmState = ESM_ADJ_FILM_STATE_MOVIE;
  	if( nAll < -1 || nAll > 100)
  	{

		if(-11 <  nAll && nAll < -1)	//Version
			nVersion = (float)nAll;
		else
			memcpy(&nVersion, &nAll, sizeof(float));

  		// 2014-0107 kcd
  		// Versin 관리 시작 임시 코드로 
  		// 추후 version 데이터 데이터가 만들어지면 해당 Line 변경

		if( ESMGetVersion(nVersion) >= ESM_4DMAKER_VERSION_10004)
			ar>>m_nFilmState;

  		if( ESMGetVersion(nVersion) >= ESM_4DMAKER_VERSION_10002)
			ar>>m_strRecordProfile;

		int n_UserFileMode = 1;
		if( ESMGetVersion(nVersion) >= ESM_4DMAKER_VERSION_10003)
		{
			if( ESMGetVersion(nVersion) < ESM_4DMAKER_VERSION_10010 )
			{
				ar>>n_UserFileMode;
				//ESMSetValue(ESM_VALUE_USEFILESERVER, n_UserFileMode);
			}
		}

  		ar>>nAll;
  	}
  
	if( m_strRecordProfile == _T(""))
		m_strRecordProfile = strFileTk;	
  	//-- 2014-01-17 kcd
  	//-- LoadDscInfo nAll 은 필요 없지만 이전 File의 Version 불일치 문제로
  	// 임시적으로 허용 하고 추후에 삭제 한다.

  	pWndDSCViewer->LoadDscInfo(ar, nAll, nVersion);
	
	//-- 2016-06-08 영상을 만들때 체크함
	//(m_pMainFrame->m_wndTimeLineEditor).m_pTimeLineView->CheckImageSize();		

//-- 2015-02-24 cygil@esmlab.com 4DModeler에서는 사용안함
#ifndef _4DMODEL
	pWndTimeLineEditor->LoadTimeLineInfo(ar, nVersion);
#endif

	int nFPS;

	if( ESMGetVersion(nVersion) >= ESM_4DMAKER_VERSION_10012 )
	{
		ar>>nFPS;
		ESMSetFrameRate(nFPS);

		//180308 
		if(nFPS == MOVIE_FPS_UHD_60P_X2 || nFPS == MOVIE_FPS_UHD_30P_X2)
		{
			//기존에 나눠진 1초 파일 사용 못함
			ESMSetDevice(SDI_MODEL_GH5);
		}
	}
	else
	{
		//ESMVerifyInfo();
		//ESMSetFrameRate(MOVIE_FPS_UNKNOWN);
	}
	
	if(ESMGetVersion(nVersion) >= ESM_4DMAKER_VERSION_10014)
	{
		int nDevice;
		ar>>nDevice;
		ESMSetDevice(nDevice);
	}

	ESMSaveVersionInfo(ESMGetVersion(nVersion));

  	//-- 2013-10-18 hongsu@esmlab.com
  	//-- Set Record Profile

  	ar.Close();
  	file.Close();
	if(ESMGetVersion(nVersion) < ESM_4DMAKER_VERSION_10014)
	{
		//AfxMessageBox(_T("Go to help -> Verify 4dm"));
		ESMVerifyInfo();
		ESMLog(5,_T("Verified 4dm"));
	}

	//ESMDisconnect();
}



void CESMFileMgr::RemoveTimeLineEditor()
{
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_VIEW_TIMELINE_RESET;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, WM_ESM_VIEW, (LPARAM)pMsg);
}


//// void CESMFileMgr::RemoveWorkFile()
//{
//	CESMFileOperation fo;
//
//	CString strFolder;
//	//- 2014-01-22 changdoKim
//// 	strFolder.Format(_T("%s"),ESMGetPath(ESM_PATH_PICTURE));
//// 	fo.Delete(strFolder,FALSE);
//// 	strFolder.Format(_T("%s"),ESMGetPath(ESM_PATH_MOVIE));
//// 	fo.Delete(strFolder,FALSE);
//// 	strFolder.Format(_T("%s"),ESMGetPath(ESM_PATH_FRAME));
//// 	fo.Delete(strFolder,FALSE);
//// 	strFolder.Format(_T("%s"),ESMGetPath(ESM_PATH_OBJECT));
//// 	fo.Delete(strFolder,FALSE);
//// 	strFolder.Format(_T("%s"),ESMGetPath(ESM_PATH_ADJUST));
//// 	fo.Delete(strFolder,FALSE);
//}