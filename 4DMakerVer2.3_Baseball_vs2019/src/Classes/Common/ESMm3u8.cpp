#include "stdafx.h"
#include "ESMm3u8.h"


CESMm3u8::CESMm3u8(void)
{
	m_nSeq = 0;
}


CESMm3u8::~CESMm3u8(void)
{
}

void CESMm3u8::SetInfo(int nRate)
{
	m_nRate = nRate;

	m_strTime = _T("#EXTINF:1.000000,\r\n");
}

BOOL CESMm3u8::OnInit(CStringArray& strMovie)
{
	CString strM3u8 = GetFile();

	CString strLocal = ESMGetLocalIP();

	CFile file;
	if(file.Open(strM3u8, CFile::modeCreate|CFile::modeNoTruncate|CFile::modeReadWrite|CFile::shareDenyRead))
	{
		char *pData[2] = {0,};
		CString strTxt;

		strTxt = _T("#EXTM3U\r\n");
		pData[0] = ESMUtil::CStringToChar(strTxt);
		file.Write(pData[0], strlen(pData[0]));
		strTxt = _T("#EXT-X-VERSION:3\r\n");
		pData[1] = ESMUtil::CStringToChar(strTxt);
		file.Write(pData[1], strlen(pData[1]));
		
		for(int i = 0; i< strMovie.GetSize(); i++)
		{
			char *pStream[4] = {0,};

			strTxt.Format(_T("#EXT-X-STREAM-INF:PROGRAM-ID:%d,BANDWIDTH=%d,RESOLUTION=%dx%d\r\n"), i+1, 1000000000, 3840, 2160);
			pStream[0] = ESMUtil::CStringToChar(strTxt);
			file.Write(pStream[0], strlen(pStream[0]));

			strTxt.Format(_T("http://%s/uhd/%s\r\n"), strLocal, strMovie.GetAt(i));
			pStream[1] = ESMUtil::CStringToChar(strTxt);
			file.Write(pStream[1], strlen(pStream[1]));

			strTxt.Format(_T("#EXT-X-STREAM-INF:PROGRAM-ID:%d,BANDWIDTH=%d,RESOLUTION=%dx%d\r\n"), i+1, 1000000, 1280, 720);
			pStream[2] = ESMUtil::CStringToChar(strTxt);
			file.Write(pStream[2], strlen(pStream[2]));

			strTxt.Format(_T("http://%s/hd/%s\r\n"), strLocal,strMovie.GetAt(i));
			pStream[3] = ESMUtil::CStringToChar(strTxt);
			file.Write(pStream[3], strlen(pStream[3]));

			for(int k = 0; k < 4; k++)
			{
				delete[] pStream[k];
				pStream[k] = NULL;
			}
		}

		file.Close();

		for(int i = 0; i < 2; i++)
		{
			delete[] pData[i];
			pData[i] = NULL;
		}


	}else
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CESMm3u8::Write(CString strMovie, BOOL bEnd/* = FALSE*/)
{
	int nIdx = strMovie.ReverseFind('\\') + 1;

	CString _strId;
	_strId = strMovie.Mid(nIdx, strMovie.GetLength());

	if(_strId.IsEmpty())
	{
		_strId = strMovie;
	}

	_strId.Replace(_T("mp4"), _T("ts"));

	while(1)
	{
		if(m_vList.size() < 10)
		{
			m_vList.push_back(_strId);
			break;
		}
		m_nSeq++;

		m_vList.erase(m_vList.begin());
	}

	CString strM3u8 = GetFile();
	//ESMLog(5, _T("Write : %s"), strM3u8);
	
	CFile file;
	if(file.Open(strM3u8, CFile::modeCreate|CFile::modeNoTruncate|CFile::modeReadWrite|CFile::shareDenyRead))
	{
		char *pData[8] = {0,};
		CString strTxt;

		strTxt = _T("#EXTM3U\r\n");
		pData[0] = ESMUtil::CStringToChar(strTxt);
		file.Write(pData[0], strlen(pData[0]));
		strTxt = _T("#EXT-X-VERSION:3\r\n");
		pData[1] = ESMUtil::CStringToChar(strTxt);
		file.Write(pData[1], strlen(pData[1]));
		strTxt = _T("#EXT-X-TARGETDURATION:1\r\n");
		pData[2] = ESMUtil::CStringToChar(strTxt);
		file.Write(pData[2], strlen(pData[2]));
		//strTxt = _T("#EXT-X-MEIDA-SEQUENCE:0\r\n");
		strTxt.Format(_T("#EXT-X-MEDIA-SEQUENCE:%d\r\n"), m_nSeq);
		pData[3] = ESMUtil::CStringToChar(strTxt);
		file.Write(pData[3], strlen(pData[3]));
	
		//strTxt = _T("#EXTINF:1.000000,\r\n");
		strTxt = _T("#EXTINF:0.984322,\r\n");
		pData[4] = ESMUtil::CStringToChar(strTxt);

		strTxt = _T("#EXT-X-ALLOW-CACHE:NO\r\n");
		pData[5] = ESMUtil::CStringToChar(strTxt);
		file.Write(pData[5], strlen(pData[5]));

		/*strTxt = _T("#EXT-X-DISCONTINUITY\r\n");
		pData[6] = ESMUtil::CStringToChar(strTxt);*/

		/*strTxt = _T("#EXT-X-PLAYLIST-TYPE:EVENT\r\n");
		pData[7] = ESMUtil::CStringToChar(strTxt);
		file.Write(pData[7], strlen(pData[7]));*/

		for(int i=0; i< m_vList.size(); i++)
		{
			CString strTxt;
			strTxt = m_vList.at(i);

			if(strTxt.IsEmpty() != TRUE)
			{
				char * pFile = NULL;
				
				file.Write(pData[4], strlen(pData[4]));

				strTxt.Append(_T("\r\n"));
				pFile = ESMUtil::CStringToChar(strTxt);
				file.Write(pFile, strlen(pFile));

				//file.Write(pData[6], strlen(pData[6]));

				delete[] pFile;
				pFile = NULL;
			}
		}
		
		if(bEnd)
		{
			char * pEnd = NULL;
			strTxt = _T("#EXT-X-ENDLIST");
			pEnd = ESMUtil::CStringToChar(strTxt);
			file.Write(pEnd, strlen(pEnd));

			delete[] pEnd;
			pEnd = NULL;
		}

		file.Close();

		for(int i = 0; i < 8; i++)
		{
			delete[] pData[i];
			pData[i] = NULL;
		}


	}else
	{
		//ESMLog(0, _T("Write Err : %s"), strM3u8);
		return FALSE;
	}

	return TRUE;
}

void CESMm3u8::ResetList()
{
	
	CString strM3u8 = GetFile();
	CFile file;
	if(file.Open(strM3u8, CFile::modeCreate/*|CFile::modeNoTruncate*/|CFile::modeReadWrite|CFile::shareDenyRead))
	{
		m_vList.clear();

		char *pData[4] = {0,};
		CString strTxt;

		strTxt = _T("#EXTM3U\r\n");
		pData[0] = ESMUtil::CStringToChar(strTxt);
		file.Write(pData[0], strlen(pData[0]));
		strTxt = _T("#EXT-X-VERSION:3\r\n");
		pData[1] = ESMUtil::CStringToChar(strTxt);
		file.Write(pData[1], strlen(pData[1]));
		strTxt = _T("#EXT-X-TARGETDURATION:1\r\n");
		pData[2] = ESMUtil::CStringToChar(strTxt);
		file.Write(pData[2], strlen(pData[2]));
		strTxt = _T("#EXT-X-MEDIA-SEQUENCE:0\r\n");
		pData[3] = ESMUtil::CStringToChar(strTxt);
		file.Write(pData[3], strlen(pData[3]));

		file.Close();

		for(int i = 0; i < 4; i++)
		{
			delete[] pData[i];
			pData[i] = NULL;
		}
	}
	m_nSeq = 0;
}

void CESMm3u8::SetFile(CString strFile)
{
	m_m3u8 = strFile;
	ResetList();
}