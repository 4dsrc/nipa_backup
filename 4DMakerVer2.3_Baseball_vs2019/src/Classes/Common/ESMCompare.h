////////////////////////////////////////////////////////////////////////////////
//
//	TGCompare.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-24
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

enum COMPARE_OPTION
{
	COMPARE_OPTION_UNKNOWN		= 0,
	COMPARE_OPTION_INCLUDE		, 
	COMPARE_OPTION_EXCLULDE		,
	COMPARE_OPTION_EQUAL		,
	COMPARE_OPTION_NOT_EQUAL	,
	COMPARE_OPTION_ALL			,
};

const CString g_arCompareOption[] = 
{
	_T("Unknown"),
	_T("Include"),
	_T("Exclude"),
	_T("=="),	
	_T("!="),
};

const CString g_arCompareResult[] = 
{
	_T("Fail"),
	_T("Pass"),	
};

class CTGCompare : public CObject
{
public:
	CTGCompare();
	~CTGCompare();

	CString m_strDest;
	CString m_strReference;
	CString m_strGroup;
	
	int		m_nCompare;
	BOOL	m_bExpect;

private:
	//-- 2012-07-24 hongsu@esmlab.com
	//-- Result 
	BOOL	m_bResult;

public:
	void SetCompare(CString strCompare);
	void SetExpect(CString strExpect);
	BOOL DoCompare(CString strTarget);
	BOOL DoReturn();
};