#pragma once
#include <vector>

//#include "DSCViewDefine.h"
#include "ESMUtil.h"
#include "ESMFunc.h"

class CESMm3u8
{
public:
	CESMm3u8(void);
	~CESMm3u8(void);

private:
	CString m_m3u8;
	int m_nRate;
	CString m_strTime;
	vector<CString> m_vList;

	int m_nSeq;
public:
	void SetFile(CString strFile);
	CString GetFile() {return m_m3u8;}

	void SetInfo(int nRate);
	int GetInfo() {return m_nRate;}

	BOOL OnInit(CStringArray& strMovie);

	BOOL Write(CString strMovie, BOOL bEnd = FALSE);
	void ResetList();
};

