#include "StdAfx.h"
#include "ESMRefereeReading.h"
#include "ESMFileOperation.h"
CESMRefereeReading::CESMRefereeReading()
{
	m_bOpenClose = FALSE;
	m_bThreadStop = FALSE;
	m_bSendThreadStop = FALSE;
	m_pArrMUX = new vector<REFEREE_MUX_INFO>;
	InitializeCriticalSection(&m_cri);
	InitializeCriticalSection(&m_criSend);
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,_ReceiveMsgThread,(void*)this,0,NULL);
	CloseHandle(hSyncTime);

	m_bMuxFinish = FALSE;
	m_nLiveConnectCnt = 0;
	m_nMuxFirstMovieIdx = -1;
	m_nMuxFirstFrameIdx = -1;
}
CESMRefereeReading::~CESMRefereeReading()
{
	m_bThreadStop = TRUE;
	m_bSendThreadStop = TRUE;
	DeleteCriticalSection(&m_cri);
	DeleteCriticalSection(&m_criSend);
}
void CESMRefereeReading::SetOpenClose(BOOL b,int nGroupIdx)
{
	m_bOpenClose = b;

	CESMFileOperation fo;
	if(m_bOpenClose == FALSE)
	{
		ESMLog(5,_T("Close...."));
		m_mapLiveMgr[nGroupIdx] = FALSE;

		int nCnt = 0;
		for(int i = 0 ; i < GetLiveConnectCnt(); i++)
		{
			if(m_mapLiveMgr[i] == FALSE)
				nCnt++;
		}
		if(nCnt == GetLiveConnectCnt())
		{
			CString strTemp;
			strTemp.Format(_T("M:\\Movie\\%s\\"),ESMGetFrameRecord());
			fo.Delete(strTemp,TRUE);

			Sleep(500);
			SetSendThreadStop(TRUE);
			m_mapSend.clear();
		}
		//m_mapFinish.clear();
		//m_nLiveConnectCnt = 0;
	}
	else
	{
		m_mapLiveMgr[nGroupIdx] = TRUE;
		SetSendThreadStop(FALSE);
		ESMLog(5,_T("[%d]Connect Mgr : %d"),nGroupIdx,GetLiveConnectCnt());
		//m_nLiveConnectCnt++;
		//ESMLog(5,_T("Open...."));
	}
}
void CESMRefereeReading::RunReceiveThread(char* pData,int nSecIdx,int nGrpIdx,int nBodySize,CString strPath)
{
	/*if(GetOpenClose() == FALSE)
	{
		if(pData)
		{
			delete pData;
			pData = NULL;
		}
		return;
	}*/
	//m_mapFinish[nSecIdx] ++;
	EnterCriticalSection(&m_criSend);
	if(m_mapSend[nSecIdx] == 0)
	{
		SEND_LIVE_DATA* pSendData = new SEND_LIVE_DATA;
		pSendData->pMgr = this;
		pSendData->nSecIdx = nSecIdx;

		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE) _beginthreadex(NULL,0,_RunSendThread,(void*)pSendData,0,NULL);
		CloseHandle(hSyncTime);
	}
	LeaveCriticalSection(&m_criSend);

	RECEIVE_DATA* pReceiveData = new RECEIVE_DATA;
	pReceiveData->pParent = this;
	pReceiveData->pData = pData;
	pReceiveData->nGroupIdx = nGrpIdx;
	pReceiveData->nSecIdx = nSecIdx;
	pReceiveData->strPath = strPath;
	pReceiveData->nBodySize = nBodySize;
	
	/*if(m_mapFinish[nSecIdx] == m_nLiveConnectCnt)
		pReceiveData->bFinish = TRUE;
	else
		pReceiveData->bFinish = FALSE;*/

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_ReceiveThread,(void*)pReceiveData,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI CESMRefereeReading::_ReceiveThread(LPVOID param)
{
	RECEIVE_DATA* pRecive = (RECEIVE_DATA*)param;
	CESMRefereeReading* pParent = pRecive->pParent;
	char* pData = pRecive->pData;
	int nGroupIdx = pRecive->nGroupIdx;
	int nSecIdx = pRecive->nSecIdx;
	int nBodySize = pRecive->nBodySize;
	CString strPath = pRecive->strPath;
	BOOL bFinish = pRecive->bFinish;
	delete pRecive;pRecive = NULL;

	EnterCriticalSection(&pParent->m_criSend);
	pParent->m_mapSend[nSecIdx]++;
	LeaveCriticalSection(&pParent->m_criSend);

	//ESMLog(5,_T("%s"),strPath);

	CFile file;
	if(file.Open(strPath, CFile::modeCreate | CFile::modeReadWrite))
	{
		file.Write((void*)(pData + sizeof(RCP_FileSave_Info)), nBodySize - sizeof(RCP_FileSave_Info));
		file.Close();
	}
	//ESMLog(5,strPath+_T(" Finish"));

	if(pData)
	{
		delete pData;
		pData = NULL;
	}
	/*if(bFinish)
		pParent->SendData(nSecIdx);*/

	return TRUE;
}
unsigned WINAPI CESMRefereeReading::_RunSendThread(LPVOID param)
{
	SEND_LIVE_DATA* pSendData = (SEND_LIVE_DATA*)param;
	CESMRefereeReading* pMgr = pSendData->pMgr;
	int nSecIdx = pSendData->nSecIdx;
	delete pSendData;pSendData = NULL;

	int nConnectCnt = 0;

	for(int i = 0 ; i < pMgr->GetLiveConnectCnt() ; i++)
	{
		if(pMgr->m_mapLiveMgr[i] == TRUE)
			nConnectCnt++;
	}

	int nWaitCnt = 0;
	while(1)
	{
		if(pMgr->GetSendThreadStop() == FALSE)
		{
			EnterCriticalSection(&pMgr->m_criSend);
			int nFinishCnt = pMgr->m_mapSend[nSecIdx];
			LeaveCriticalSection(&pMgr->m_criSend);

			if(nConnectCnt == nFinishCnt)
				break;
		}
		else
			break;

		Sleep(1);
	}

	if(pMgr->GetSendThreadStop() == TRUE)
		return FALSE;
	
	pMgr->SendData(nSecIdx);

	return TRUE;
}
void CESMRefereeReading::SendData(int nSecIdx)
{
	CString strCmd,strOpt,strTemp;
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));
	
	for(int i = 0 ; i < GetLiveConnectCnt(); i++)
	{
		if(m_mapLiveMgr[i] == TRUE)
			strTemp.Format(_T("-i \"M:\\Movie\\%s\\%d_%d.mp4\" "),ESMGetFrameRecord(),i,nSecIdx);
		else
		{
			//strTemp.Format(_T("-i M:\\Movie\\1080.mp4 "));
			strTemp.Format(_T("-i \"%s\\1080.mp4\" "),ESMGetPath(ESM_PATH_IMAGE));
		}

		strOpt.Append(strTemp);
	}

	strOpt.Append(_T("-vcodec mpeg4 -qscale 1 -qmin 2 -intra -an -c copy "));

	for(int i = 0 ; i < GetLiveConnectCnt() ; i++)
	{
		//"-map 0 -map 1 -map 2 -map 3 "
		strTemp.Format(_T("-map %d "),i);
		strOpt.Append(strTemp);
	}

	strTemp.Format(_T("M:\\Movie\\%s\\%d.mp4"),ESMGetFrameRecord(),nSecIdx);
	strOpt.Append(strTemp);
	
	int nStart = GetTickCount();
	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;
	//lpExecInfo.nShow	=SW_SHOW;
	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	lpExecInfo.lpFile = _T("exit");
	lpExecInfo.lpVerb = L"close";
	lpExecInfo.lpParameters = NULL;

	if(TerminateProcess(lpExecInfo.hProcess,0))
	{
		lpExecInfo.hProcess = 0;
		ESMLog(5,_T("End"));
	}
	
	ESMLog(5,_T("[Referee Mode] Mux Finish(%d)"),nSecIdx);

	//Send Data
	//RequestData(strTemp,1);
#if 1
	CString * pPath = new CString;
	pPath->Format(_T("%s"), strTemp);

	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_REFEREE_DATA_TO_CLIENT;
	pMsg->nParam1 = ENUM_REFEREE_LIVE;
	pMsg->nParam2 = nSecIdx*100 + movie_frame_per_second;//secIdx*100+frame index
	pMsg->nParam3 = 0;//Local Time
	pMsg->pParam = (LPARAM)pPath;
	::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_REFEREE, (LPARAM)pMsg);
#endif

	//Delete
	CESMFileOperation fo;
	for(int i = 0 ; i < GetLiveConnectCnt() ; i++)
	{
		strTemp.Format(_T("M:\\Movie\\%s\\%d_%d.mp4"),ESMGetFrameRecord(),i,nSecIdx);
		if(fo.IsFileExist(strTemp))
		{
			//ESMLog(5,_T("[DELETE] %s"),strTemp);
			fo.Delete(strTemp);
		}
	}
	
}
void CESMRefereeReading::AddEvent(int nGroupIdx,int nMovieIdx,int nFrameIdx)
{
	if(ESMGetValue(ESM_VALUE_REFEREEREAD))
	{
		EnterCriticalSection(&m_cri);
		REFEREE_MUX_INFO MuxInfo;
		MuxInfo.nGroupIdx = nGroupIdx;
		MuxInfo.nMovieIdx = nMovieIdx;
		MuxInfo.nFrameIdx = nFrameIdx;

		m_pArrMUX->push_back(MuxInfo);
		LeaveCriticalSection(&m_cri);
	}
}
unsigned WINAPI CESMRefereeReading::_ReceiveMsgThread(LPVOID param)
{
	CESMRefereeReading* pReferee = (CESMRefereeReading*) param;

	while(!pReferee->m_bThreadStop)
	{
		if(pReferee->m_bThreadStop == FALSE)
		{
			if(pReferee->m_pArrMUX->size() > 0)
			{
				EnterCriticalSection(&pReferee->m_cri);
				REFEREE_MUX_INFO* pMuxData = &pReferee->m_pArrMUX->at(0);
				LeaveCriticalSection(&pReferee->m_cri);

				pReferee->LaunchMultiplexer(pMuxData->nGroupIdx,pMuxData->nMovieIdx,pMuxData->nFrameIdx);
				pReferee->m_pArrMUX->erase(pReferee->m_pArrMUX->begin());
			}
		}
		Sleep(1);
	}

	return TRUE;
}
void CESMRefereeReading::LaunchMultiplexer(int nGroupIdx,int nMovieIdx,int nFrameIdx)
{
	//Send message
	SetMuxFinish(FALSE);

	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_REFEREE_MUX;
	pMsg->nParam1 = nGroupIdx;
	pMsg->nParam2 = nMovieIdx;
	pMsg->nParam3 = nFrameIdx;
	::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);

	while(1)
	{
		Sleep(1);
		if(GetMuxFinish() == TRUE)
		{
			CString strPath = GetFinishMuxPath();
			
			CString * pPath = new CString;
			pPath->Format(_T("%s"), strPath);

			ESMEvent* pMsg = new ESMEvent();
			pMsg->message = WM_ESM_NET_REFEREE_DATA_TO_CLIENT;
			pMsg->nParam1 = ENUM_REFEREE_MUX;
			pMsg->nParam2 = m_nMuxFirstMovieIdx;//Movie Index
			pMsg->nParam3 = m_nMuxFirstFrameIdx;//Frame Index
			pMsg->pParam = (LPARAM)pPath;
			::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_REFEREE, (LPARAM)pMsg);
			//RequestData(strPath,2);
			break;
		}
	}
}
void CESMRefereeReading::RequestData(CString strPath,int message)
{
	//FILE *fp;
	//CString strLog;

	//char filename[255] = {0,};	
	//sprintf(filename, "%S", strPath);

	//fp = fopen(filename, "rb");

	//if(fp == NULL)
	//{
	//	strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), strPath);
	//	ESMLog(0,strLog);
	//	return;
	//}

	//strLog.Format(_T("[#NETWORK] Send File [%s]"), strPath);
	////SendLog(1,strLog);

	//fseek(fp, 0L, SEEK_END);
	//int nLength = ftell(fp);
	//char* buf = new char[nLength];
	//fseek(fp, 0, SEEK_SET);
	//fread(buf, sizeof(char), nLength, fp);

	//CString *pPath = new CString;
	//pPath->Format(_T("%s"),strPath);

	//ESMEvent* pMsg = NULL;
	//pMsg = new ESMEvent();
	//pMsg->message = WM_ESM_NET_REFEREE_DATA_TO_CLIENT;
	//pMsg->nParam1;
	//pMsg->nParam2;
	//pMsg->nParam3;
	//pMsg->pParam = (LPARAM)pPath;

	//::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_REFEREE, (LPARAM)pMsg);

	//fclose(fp);

	//ESMLog(5,_T("[%d] - %s"),message,strPath);
	//if(buf)
	//{
	//	delete buf;
	//	buf = NULL;
	//}
}
void CESMRefereeReading::Launch4KMovieSender(int nGroupIdx,int nCameraIdx,int nFrameIdx)
{
	int nStartMovieIdx=0,nStartFrameIdx=0;
	ESMGetMovieIndex(nFrameIdx,nStartMovieIdx,nStartFrameIdx);

	int nFirstMovieIdx,nLastMovieIndex;
	if(nStartFrameIdx > movie_frame_per_second/2)
	{
		nFirstMovieIdx = nStartMovieIdx;
		nLastMovieIndex = nStartMovieIdx + 1;
	}
	else
	{
		nFirstMovieIdx = nStartMovieIdx - 1;
		nLastMovieIndex = nStartMovieIdx;
	}

	SEND_ORIGINAL_DATA* pSendData = new SEND_ORIGINAL_DATA;
	pSendData->pMgr = this;
	pSendData->nGroupIdx = nGroupIdx;
	pSendData->nCameraIdx = nCameraIdx;
	pSendData->nFirstMovieIdx = nFirstMovieIdx;
	pSendData->nLastMovieIdx  = nLastMovieIndex;
	ESMLog(5, _T("SEND_ORIGINAL_DATA : %d, %d, %d, %d "), nGroupIdx, nCameraIdx, nFirstMovieIdx, nLastMovieIndex);
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_SendOriginalThread,(void*)pSendData,0,NULL);
	CloseHandle(hSyncTime);
}
vector<CString> CESMRefereeReading::GetRefereeMoviePath(int nGroupIdx,int nDSCIndex,int nMovieIdx)
{
	vector<CString> strArr;
	CString strGroupName;
	if(nGroupIdx == 0)
		strGroupName = _T('A');
	else if(nGroupIdx == 1)
		strGroupName = _T('B');
	else if(nGroupIdx == 2)
		strGroupName = _T('C');
	else if(nGroupIdx == 3)
		strGroupName = _T('D');
	else if(nGroupIdx == 4)
		strGroupName = _T('E');

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CDSCItem* pItem = NULL;

	for(int i = 0 ; i < arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*) arDSCList.GetAt(i);

		CString strGroup = pItem->GetGroup();
		CString strDSCID = pItem->GetDeviceDSCID();
		if(strGroup.Find(strGroupName) == 0)//find
		{
			int nFirst = strGroup.Find('/');
			int nEnd   = strGroup.ReverseFind('/');
			CString strIndex = strGroup.Mid(nFirst + 1 , nEnd - 2);

			if(_ttoi(strIndex) == nDSCIndex+1)
			{
				strArr.push_back(ESMGetMoviePath(strDSCID,nMovieIdx*60));
				CString strPath;
				strPath.Format(_T("M:\\Movie\\%s_%d.mp4"),strDSCID,nMovieIdx);
				strArr.push_back(strPath);

				for(int i=0; i<strArr.size(); i++)
				{
					ESMLog(5, _T("GetRefereeMoviePath : %s"), strArr.at(i));
				}
				
				return strArr;
			}
		}
	}

	//return strArr;
}
unsigned WINAPI CESMRefereeReading::_SendOriginalThread(LPVOID param)
{
	SEND_ORIGINAL_DATA* pSendData = (SEND_ORIGINAL_DATA*)param;
	CESMRefereeReading* pMgr = pSendData->pMgr;
	int nGroupIdx = pSendData->nGroupIdx;
	int nCameraIdx = pSendData->nCameraIdx;
	int nFirstMovieIdx = pSendData->nFirstMovieIdx;
	int nLastMovieIdx  = pSendData->nLastMovieIdx;
	delete pSendData; pSendData = NULL;

	for(int i = nFirstMovieIdx; i <= nLastMovieIdx; i++)
	{
		vector<CString> strArrPath = pMgr->GetRefereeMoviePath(nGroupIdx,nCameraIdx,i);

		if(strArrPath.size() <= 1)
			return FALSE;
		
		CString * pPath = new CString;
		pPath->Format(_T("%s"), strArrPath.at(0));

		CString * pRamPath = new CString;
		pRamPath->Format(_T("%s"), strArrPath.at(1));

		ESMEvent* pMsg = new ESMEvent();
		pMsg->message = WM_ESM_NET_REFEREE_DATA_TO_CLIENT;
		pMsg->nParam1 = ENUM_REFEREE_ORIGINAL;
		pMsg->nParam2 = i;//Movie Index
		pMsg->pParam = (LPARAM)pPath;
		pMsg->pDest  = (LPARAM)pRamPath;
		::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_REFEREE, (LPARAM)pMsg);

		//pMgr->RequestData(strPath,3);
	}

	return TRUE;
}