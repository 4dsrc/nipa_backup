#include "stdafx.h"
#include "ESMEncoderForReferee.h"

CESMEncoderForReferee::CESMEncoderForReferee(
	CString strInputFileName, 
	CString strOutputFilePath,
	CESMEncoderForReferee::ENCODING_SIZE nEncodingSize)
{
	m_strInputFileName = strInputFileName;
	m_StrOutputFilePath = strOutputFilePath;

	m_hJPEGEncodingThreadHandle = NULL;
	m_hTSEncodingThreadHandle = NULL;

	m_hTSEncodingThreadHandle = FALSE;
	m_hJPEGEncodingThreadHandle = FALSE;

	SetEncodingSize(nEncodingSize);

	m_nResizeJPEGWidth = 1280;
	m_nResizeJPEGHeight = 720;

	m_m3u8 = NULL;
}

CESMEncoderForReferee::~CESMEncoderForReferee()
{

}

int CESMEncoderForReferee::Run()
{
	m_strOutputFileName = GetFileNameMp4ToTs();

	m_hTSEncodingThreadHandle = (HANDLE) _beginthreadex(NULL, 0, EncodingMp4ToTs, (void *)this, 0, NULL);
	WaitForSingleObject(m_hTSEncodingThreadHandle, INFINITE);
	CloseHandle(m_hTSEncodingThreadHandle);
	m_hTSEncodingThreadHandle = NULL;

	EncodingMp4To3JPEGs();

	return 1;
}

int CESMEncoderForReferee::Run_I()
{
	m_strOutputFileName = GetFileNameMp4ToTs();

	m_hTSEncodingThreadHandle = (HANDLE) _beginthreadex(NULL, 0, EncodingMp4ToTs_I, (void *)this, 0, NULL);
	WaitForSingleObject(m_hTSEncodingThreadHandle, INFINITE);
	CloseHandle(m_hTSEncodingThreadHandle);
	m_hTSEncodingThreadHandle = NULL;
	
	return 1;
}


unsigned WINAPI CESMEncoderForReferee::EncodingMp4ToTs(LPVOID param)
{
	//ESMLog(5, _T("EncodingMp4ToTs"));
	CESMEncoderForReferee* pEncoderForReferee = (CESMEncoderForReferee*)param;

	CString strCmd;
	strCmd.Format(_T("\"%s\\bin\\ffmpeg.exe\""),ESMGetPath(ESM_PATH_HOME));

	CString strReEncodingOpt;
	CString strOpt;
	strOpt.Format(
		_T(" -y -c:v libx264 -filter:v scale=%d:%d -preset ultrafast -threads 2 -map 0:0 -muxdelay %f "), 
		pEncoderForReferee->m_nResizeWidth, pEncoderForReferee->m_nResizeHeight, pEncoderForReferee->GetMuxDelay());

	CString strInputFileName = pEncoderForReferee->m_strInputFileName;
	CString strOutputFileName = pEncoderForReferee->m_strOutputFileName;

	strReEncodingOpt.Append(_T(" -i \""));
	strReEncodingOpt.Append(strInputFileName);
	strReEncodingOpt.Append(_T("\""));
	strReEncodingOpt.Append(strOpt);

	strReEncodingOpt.Append(_T("\""));
	strReEncodingOpt.Append(strOutputFileName);
	strReEncodingOpt.Append(_T("\""));

	SHELLEXECUTEINFO lpExecInfoReEncoding;
	lpExecInfoReEncoding.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfoReEncoding.lpFile = strCmd;
	lpExecInfoReEncoding.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;
	lpExecInfoReEncoding.hwnd = NULL;  
	lpExecInfoReEncoding.lpVerb = L"open";
	lpExecInfoReEncoding.lpParameters = strReEncodingOpt;
	lpExecInfoReEncoding.lpDirectory = NULL;

	lpExecInfoReEncoding.nShow = SW_HIDE;
	lpExecInfoReEncoding.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfoReEncoding);

	if (lpExecInfoReEncoding.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfoReEncoding.hProcess, INFINITE);
		::CloseHandle(lpExecInfoReEncoding.hProcess);
	}

	strCmd.ReleaseBuffer();
	strReEncodingOpt.ReleaseBuffer();
	//ESMLog(5, _T("strInputFileName"));
	if(pEncoderForReferee->m_m3u8 != NULL)
	{
		pEncoderForReferee->m_m3u8->Write(strInputFileName);
		ESMLog(5, _T("Write [%d] : %s"), pEncoderForReferee->m_nResizeHeight, strInputFileName);
	}
	pEncoderForReferee->m_bIsTSEncodingDone = TRUE;

	return 1;
}

unsigned WINAPI CESMEncoderForReferee::EncodingMp4ToTs_I(LPVOID param)
{
	//ESMLog(5, _T("EncodingMp4ToTs"));
	CESMEncoderForReferee* pEncoderForReferee = (CESMEncoderForReferee*)param;

	CString strCmd;
	strCmd.Format(_T("\"%s\\bin\\ffmpeg.exe\""),ESMGetPath(ESM_PATH_HOME));

	CString strReEncodingOpt;
	CString strOpt;
	strOpt.Format(
		_T(" -y -c:v libx264 -filter:v scale=%d:%d -preset ultrafast -threads 2 -g 1 -map 0:0 -muxdelay %f "), 
		pEncoderForReferee->m_nResizeWidth, pEncoderForReferee->m_nResizeHeight, pEncoderForReferee->GetMuxDelay());

	CString strInputFileName = pEncoderForReferee->m_strInputFileName;
	CString strOutputFileName = pEncoderForReferee->m_strOutputFileName;

	strReEncodingOpt.Append(_T(" -i \""));
	strReEncodingOpt.Append(strInputFileName);
	strReEncodingOpt.Append(_T("\""));
	strReEncodingOpt.Append(strOpt);

	strReEncodingOpt.Append(_T("\""));
	strReEncodingOpt.Append(strOutputFileName);
	strReEncodingOpt.Append(_T("\""));

	SHELLEXECUTEINFO lpExecInfoReEncoding;
	lpExecInfoReEncoding.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfoReEncoding.lpFile = strCmd;
	lpExecInfoReEncoding.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;
	lpExecInfoReEncoding.hwnd = NULL;  
	lpExecInfoReEncoding.lpVerb = L"open";
	lpExecInfoReEncoding.lpParameters = strReEncodingOpt;
	lpExecInfoReEncoding.lpDirectory = NULL;

	lpExecInfoReEncoding.nShow = SW_HIDE;
	lpExecInfoReEncoding.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfoReEncoding);

	if (lpExecInfoReEncoding.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfoReEncoding.hProcess, INFINITE);
		::CloseHandle(lpExecInfoReEncoding.hProcess);
	}

	strCmd.ReleaseBuffer();
	strReEncodingOpt.ReleaseBuffer();
	//ESMLog(5, _T("strInputFileName"));
	if(pEncoderForReferee->m_m3u8 != NULL)
	{
		pEncoderForReferee->m_m3u8->Write(strInputFileName);
		ESMLog(5, _T("Write [%d] : %s"), pEncoderForReferee->m_nResizeHeight, strInputFileName);
	}
	pEncoderForReferee->m_bIsTSEncodingDone = TRUE;

	return 1;
}

unsigned WINAPI CESMEncoderForReferee::EncodingMp4To3JPEGs(LPVOID param)
{
	CESMEncoderForReferee* pEncoderForReferee = (CESMEncoderForReferee*)param;

	int nGop = 20;

	AVCodec *pCodec;
	AVCodecContext *pCodecCtx = NULL;
	AVFormatContext *pFormatCtx = NULL;

	char* szInputFileName;
	CString strFileName = pEncoderForReferee->m_strInputFileName;
	szInputFileName = ESMUtil::CStringToChar(strFileName);

	CString strOutputFileName = pEncoderForReferee->GetFileNameMp4ToJPEG(0);

	CString strOutputJpegName1 = pEncoderForReferee->GetFileNameMp4ToJPEG(0);
	char* szOutputJpegName1 = ESMUtil::CStringToChar(strOutputJpegName1);
	CString strOutputJpegName2 = pEncoderForReferee->GetFileNameMp4ToJPEG(1);
	char* szOutputJpegName2 = ESMUtil::CStringToChar(strOutputJpegName2);
	CString strOutputJpegName3 = pEncoderForReferee->GetFileNameMp4ToJPEG(2);
	char* szOutputJpegName3 = ESMUtil::CStringToChar(strOutputJpegName3);

	/* open input streams */
	int ret;
	if ((ret = avformat_open_input(&pFormatCtx, szInputFileName, NULL, NULL)) < 0) 
	{
		//ESMLog(5, _T("[EncodingMp4ToJPEGs]Could not open %s"), pEncoderForReferee->m_strInputFileName);
		return 0;
	}

	int nVideoStreamIndex = av_find_best_stream(pFormatCtx, AVMEDIA_TYPE_VIDEO, -1, -1, &pCodec, 0);
	pCodecCtx = pFormatCtx->streams[nVideoStreamIndex]->codec;
	avcodec_open2(pCodecCtx, pCodec, NULL);

	pCodecCtx->thread_type = FF_THREAD_SLICE;
	pCodecCtx->thread_count= 16;

	if(pFormatCtx->streams[nVideoStreamIndex]->nb_frames == 60)
		nGop = 20;
	else if(pFormatCtx->streams[nVideoStreamIndex]->nb_frames == 48)
		nGop = 12;
	else if(pFormatCtx->streams[nVideoStreamIndex]->nb_frames == 30)
		nGop = 10;
	else if(pFormatCtx->streams[nVideoStreamIndex]->nb_frames == 25)
		nGop = 8;
	else
		nGop = 20;

	AVPacket pkt;
	AVFrame *pFrame;
	int nGopFrame;

	double dTime = 0.;

	int nCnt = 0;
	av_seek_frame(pFormatCtx, -1, dTime*AV_TIME_BASE, AVSEEK_FLAG_ANY);
	while (av_read_frame(pFormatCtx, &pkt) >= 0 || !(dTime > .900))
	{
		if (pkt.stream_index == nVideoStreamIndex) 
		{
			pFrame = av_frame_alloc();
			avcodec_decode_video2(pCodecCtx, pFrame, &nGopFrame, &pkt);

			if (nGopFrame) 
			{
				AVCodec *pJpegCodec = avcodec_find_encoder(AV_CODEC_ID_MJPEG);
				if(!pJpegCodec)
					return -1;

				AVCodecContext *pJpegCtx = avcodec_alloc_context3(pJpegCodec);
				if(!pJpegCtx)
					return -2;

				pJpegCtx->time_base.den = 60;
				pJpegCtx->time_base.num = 1;
				pJpegCtx->pix_fmt = AV_PIX_FMT_YUVJ420P;
				pJpegCtx->height = pEncoderForReferee->m_nResizeHeight;
				pJpegCtx->width = pEncoderForReferee->m_nResizeWidth;

				if(avcodec_open2(pJpegCtx, pJpegCodec, NULL) < 0)
					return -3;

				AVFrame* pResizeFrame = av_frame_alloc();
				struct SwsContext *sws_context;

				sws_context = sws_getContext(
					pCodecCtx->width, pCodecCtx->height,
					AV_PIX_FMT_YUV420P,
					pEncoderForReferee->m_nResizeWidth, 
					pEncoderForReferee->m_nResizeHeight,
					AV_PIX_FMT_YUV420P,
					SWS_BICUBIC, NULL, NULL, NULL);

				int nBytes = avpicture_get_size(AV_PIX_FMT_YUV420P, 
					pEncoderForReferee->m_nResizeWidth, 
					pEncoderForReferee->m_nResizeHeight);
				uint8_t* pFrameBuffer = (uint8_t*)av_malloc(nBytes*sizeof(uint8_t));

				avpicture_fill(
					(AVPicture*)pResizeFrame,
					pFrameBuffer,
					AV_PIX_FMT_YUV420P,
					pEncoderForReferee->m_nResizeWidth,
					pEncoderForReferee->m_nResizeHeight);

				sws_scale(sws_context,
					pFrame->data,
					pFrame->linesize, 0,
					pCodecCtx->height,
					pResizeFrame->data, 
					pResizeFrame->linesize);

				AVPacket outpkt;
				outpkt.data = NULL;
				outpkt.size = 0;
				av_init_packet(&outpkt);

				int nGopFrame;
				if(avcodec_encode_video2(pJpegCtx, &outpkt, pResizeFrame, &nGopFrame) < 0)
					return -4;

				char* szOutputJpegName;
				if(nCnt == 0)
					szOutputJpegName = szOutputJpegName1;
				else if(nCnt == 1)
					szOutputJpegName = szOutputJpegName2;
				else if(nCnt == 2)
					szOutputJpegName = szOutputJpegName3;
				else
					szOutputJpegName = szOutputJpegName1;

				FILE* fJPEGFile;
				fJPEGFile = fopen(szOutputJpegName, "wb");
				fwrite(outpkt.data, 1, outpkt.size, fJPEGFile);
				fclose(fJPEGFile);
				nCnt++;

				sws_freeContext(sws_context);
				avcodec_close(pJpegCtx);
				avcodec_free_context(&pJpegCtx);

				av_free(pResizeFrame);
				av_free(pFrameBuffer);
				av_free_packet(&outpkt);

				dTime += .333;
				av_seek_frame(pFormatCtx, -1, dTime*AV_TIME_BASE, AVSEEK_FLAG_ANY);
			}
			av_free(pFrame);
		}
		av_free_packet(&pkt);
	}
	avcodec_close(pCodecCtx);
	avformat_close_input(&pFormatCtx);

	delete szInputFileName;

	delete szOutputJpegName1;
	delete szOutputJpegName2;
	delete szOutputJpegName3;

	pEncoderForReferee->m_bIsJPEGEncodingDone = TRUE;

	return 1;
}

int CESMEncoderForReferee::EncodingMp4To3JPEGs()
{
	int nGop = 20;

	AVCodec *pCodec;
	AVCodecContext *pCodecCtx = NULL;
	AVFormatContext *pFormatCtx = NULL;

	char* szInputFileName;
	CString strFileName = m_strInputFileName;
	szInputFileName = ESMUtil::CStringToChar(strFileName);

	CString strOutputFileName = GetFileNameMp4ToJPEG(0);


	CString strOutputJpegName1 = GetFileNameMp4ToJPEG(0);
	char* szOutputJpegName1 = ESMUtil::CStringToChar(strOutputJpegName1);
	CString strOutputJpegName2 = GetFileNameMp4ToJPEG(1);
	char* szOutputJpegName2 = ESMUtil::CStringToChar(strOutputJpegName2);
	CString strOutputJpegName3 = GetFileNameMp4ToJPEG(2);
	char* szOutputJpegName3 = ESMUtil::CStringToChar(strOutputJpegName3);

	/* open input streams */
	int ret;
	if ((ret = avformat_open_input(&pFormatCtx, szInputFileName, NULL, NULL)) < 0) 
	{
		//ESMLog(5, _T("[EncodingMp4ToJPEGs]Could not open %s"), pEncoderForReferee->m_strInputFileName);
		return 0;
	}

	int nVideoStreamIndex = av_find_best_stream(pFormatCtx, AVMEDIA_TYPE_VIDEO, -1, -1, &pCodec, 0);
	pCodecCtx = pFormatCtx->streams[nVideoStreamIndex]->codec;
	avcodec_open2(pCodecCtx, pCodec, NULL);

	pCodecCtx->thread_type = FF_THREAD_SLICE;
	pCodecCtx->thread_count= 16;

	if(pFormatCtx->streams[nVideoStreamIndex]->nb_frames == 60)
		nGop = 20;
	else if(pFormatCtx->streams[nVideoStreamIndex]->nb_frames == 48)
		nGop = 12;
	else if(pFormatCtx->streams[nVideoStreamIndex]->nb_frames == 30)
		nGop = 10;
	else if(pFormatCtx->streams[nVideoStreamIndex]->nb_frames == 25)
		nGop = 8;
	else
		nGop = 20;

	AVPacket pkt;
	AVFrame *pFrame;
	int nGopFrame;

	double dTime = 0.;

	int nCnt = 0;
	av_seek_frame(pFormatCtx, -1, dTime*AV_TIME_BASE, AVSEEK_FLAG_ANY);
	while (av_read_frame(pFormatCtx, &pkt) >= 0 || !(dTime > .900))
	{
		if (pkt.stream_index == nVideoStreamIndex) 
		{
			pFrame = av_frame_alloc();
			avcodec_decode_video2(pCodecCtx, pFrame, &nGopFrame, &pkt);

			if (nGopFrame) 
			{
				AVCodec *pJpegCodec = avcodec_find_encoder(AV_CODEC_ID_MJPEG);
				if(!pJpegCodec)
					return -1;

				AVCodecContext *pJpegCtx = avcodec_alloc_context3(pJpegCodec);
				if(!pJpegCtx)
					return -2;

				pJpegCtx->time_base.den = 60;
				pJpegCtx->time_base.num = 1;
				pJpegCtx->pix_fmt = AV_PIX_FMT_YUVJ420P;
				pJpegCtx->width = m_nResizeJPEGWidth;
				pJpegCtx->height = m_nResizeJPEGHeight;
				

				if(avcodec_open2(pJpegCtx, pJpegCodec, NULL) < 0)
					return -3;

				AVFrame* pResizeFrame = av_frame_alloc();
				struct SwsContext *sws_context;

				sws_context = sws_getContext(
					pCodecCtx->width, pCodecCtx->height,
					AV_PIX_FMT_YUV420P,
					m_nResizeJPEGWidth, 
					m_nResizeJPEGHeight,
					AV_PIX_FMT_YUV420P,
					SWS_BICUBIC, NULL, NULL, NULL);

				int nBytes = avpicture_get_size(AV_PIX_FMT_YUV420P, 
					m_nResizeJPEGWidth, 
					m_nResizeJPEGHeight);
				uint8_t* pFrameBuffer = (uint8_t*)av_malloc(nBytes*sizeof(uint8_t));

				avpicture_fill(
					(AVPicture*)pResizeFrame,
					pFrameBuffer,
					AV_PIX_FMT_YUV420P,
					m_nResizeJPEGWidth,
					m_nResizeJPEGHeight);

				sws_scale(sws_context,
					pFrame->data,
					pFrame->linesize, 0,
					pCodecCtx->height,
					pResizeFrame->data, 
					pResizeFrame->linesize);

				AVPacket outpkt;
				outpkt.data = NULL;
				outpkt.size = 0;
				av_init_packet(&outpkt);

				int nGopFrame;
				if(avcodec_encode_video2(pJpegCtx, &outpkt, pResizeFrame, &nGopFrame) < 0)
					return -4;

				char* szOutputJpegName;
				if(nCnt == 0)
					szOutputJpegName = szOutputJpegName1;
				else if(nCnt == 1)
					szOutputJpegName = szOutputJpegName2;
				else if(nCnt == 2)
					szOutputJpegName = szOutputJpegName3;
				else
					szOutputJpegName = szOutputJpegName1;

				FILE* fJPEGFile;
				fJPEGFile = fopen(szOutputJpegName, "wb");
				fwrite(outpkt.data, 1, outpkt.size, fJPEGFile);
				fclose(fJPEGFile);
				nCnt++;

				sws_freeContext(sws_context);
				avcodec_close(pJpegCtx);
				avcodec_free_context(&pJpegCtx);

				av_free(pResizeFrame);
				av_free(pFrameBuffer);
				av_free_packet(&outpkt);

				dTime += .333;
				av_seek_frame(pFormatCtx, -1, dTime*AV_TIME_BASE, AVSEEK_FLAG_ANY);
			}
			av_free(pFrame);
		}
		av_free_packet(&pkt);
	}
	avcodec_close(pCodecCtx);
	avformat_close_input(&pFormatCtx);

	delete szInputFileName;

	delete szOutputJpegName1;
	delete szOutputJpegName2;
	delete szOutputJpegName3;

	m_bIsJPEGEncodingDone = TRUE;

	return 1;
}

CString CESMEncoderForReferee::GetFileNameMp4ToTs()
{
	CString strOutputPath = m_StrOutputFilePath;
	CString strOutputFileName = m_strInputFileName;
	strOutputFileName.Replace( _T(".mp4"), _T(".ts") );

	CString strTok, strTokFileName;
	CString strFileNameAndPath = strOutputFileName;

	int nStrCount = ESMUtil::GetFindCharCount(strFileNameAndPath, '\\');

	AfxExtractSubString(strTokFileName, strFileNameAndPath, nStrCount, '\\');
	AfxExtractSubString(strTok, strTokFileName, 0, '.');

	CString strReturn = strOutputPath+strTokFileName;

	return strReturn;
}

CString CESMEncoderForReferee::GetFileNameMp4ToJPEG(int nIndex)
{
	CString strOutputPath = m_StrOutputFilePath;
	CString strOutputFileName = m_strInputFileName;

	CString strIndexAndExtension;
	strIndexAndExtension.Format(_T("_%d.jpg"), nIndex);

	strOutputFileName.Replace(_T(".mp4"), strIndexAndExtension);

	CString strTok, strTokFileName;
	CString strFileNameAndPath = strOutputFileName;

	int nStrCount = ESMUtil::GetFindCharCount(strFileNameAndPath, '\\');

	AfxExtractSubString(strTokFileName, strFileNameAndPath, nStrCount, '\\');
	AfxExtractSubString(strTok, strTokFileName, 0, '.');

	CString strReturn = strOutputPath+strTokFileName;

	return strReturn;
}

void CESMEncoderForReferee::SetEncodingSize(ENCODING_SIZE nEncodingSize)
{
	if(nEncodingSize == ENCODING_SIZE_HD)
	{
		m_nResizeWidth = 1280;
		m_nResizeHeight = 720;
	}
	else if(nEncodingSize == ENCODING_SIZE_FHD)
	{
		m_nResizeWidth = 1920;
		m_nResizeHeight = 1080;
	}
	else if(nEncodingSize == ENCODING_SIZE_UHD)
	{
		m_nResizeWidth = 3840;
		m_nResizeHeight = 2160;
	}
	else
	{
		m_nResizeWidth = 1280;
		m_nResizeHeight = 720;
	}

	GetMuxDelay();
}

double CESMEncoderForReferee::GetMuxDelay()
{
	CString strFileName = m_strInputFileName;
	CString strTok, strTokWithExtension;
	int nStrCount = ESMUtil::GetFindCharCount(strFileName, '_');

	AfxExtractSubString(strTokWithExtension, strFileName, nStrCount, '_');
	AfxExtractSubString(strTok, strTokWithExtension, 0, '.');

	int nSecond = _ttoi(strTok); 

	return nSecond/2.;
}