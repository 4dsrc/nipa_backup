#include "stdAfx.h"
#include "DllFunc.h"

HWND g_hParentFrm = NULL;
void ParentHWndReg(HWND hParentFrm)
{
	g_hParentFrm = hParentFrm;
}

void SendLog(int verbosity, LPCTSTR lpszFormat, ...)
{
	if(NULL == lpszFormat)	
		return;

	TCHAR pszMsg[ESMLOG_LINE_SIZE] = {0};

	va_list args;
	va_start(args, lpszFormat);
	_vstprintf(pszMsg, lpszFormat, args);
	va_end(args);

	int nLength = (int)_tcslen(pszMsg);
	if(!nLength)
		return;

	esm_msg_info* pEmi;
	pEmi = new esm_msg_info;
	pEmi->verbosity = verbosity;
	pEmi->lpszFormat = lpszFormat;
	if(g_hParentFrm)
	{
		::SendMessage(g_hParentFrm, WM_ESM_LOG, (WPARAM)WM_ESM_LOG_MSG, (LPARAM)pEmi);
 		if(pEmi)
 		{
 			delete pEmi;
 			pEmi=NULL;
 		}
	}
}
BOOL ESMMovieMemcpy(void * _Dst, const void * _Src, size_t _Size)
{
	__try
	{
		memcpy(_Dst, _Src, _Size);		
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{

		SendLog(0, _T("memcpy error"));
		return FALSE;
	}

	return TRUE;
}