/////////////////////////////////////////////////////////////////////////////
//
// ESMRegistry.cpp: implementation of the ESMRegistry class.
//
//
// Copyright (c) 2003 ESMLab, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of ESMLab, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2003-11-11
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMRegistry.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CESMRegistry::CESMRegistry(HKEY hKeyRoot)
{
	m_hKey = hKeyRoot;
}

CESMRegistry::~CESMRegistry()
{
	Close();
}


BOOL CESMRegistry::VerifyKey (HKEY hKeyRoot, LPCTSTR pszPath)
{
	ASSERT (hKeyRoot);
	ASSERT (pszPath);

	LONG ReturnValue = RegOpenKeyEx (hKeyRoot, pszPath, 0L,
		KEY_ALL_ACCESS, &m_hKey);
	if(ReturnValue == ERROR_SUCCESS)
		return TRUE;
	
	//m_Info.lMessage = ReturnValue;
	//m_Info.dwSize = 0L;
	//m_Info.dwType = 0L;

	return FALSE;
}

BOOL CESMRegistry::CreateKey (HKEY hKeyRoot, LPCTSTR pszPath)
{
	DWORD dw;

	LONG ReturnValue = RegCreateKeyEx (hKeyRoot, pszPath, 0L, NULL,
		REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, 
		&m_hKey, &dw);

	//m_Info.lMessage = ReturnValue;
	//m_Info.dwSize = 0L;
	//m_Info.dwType = 0L;

	if(ReturnValue == ERROR_SUCCESS)
		return TRUE;

	return FALSE;
}

BOOL CESMRegistry::Open (HKEY hKeyRoot, LPCTSTR pszPath)
{
	//m_sPath = pszPath;

	LONG ReturnValue = RegOpenKeyEx (hKeyRoot, pszPath, 0L,
		KEY_ALL_ACCESS, &m_hKey);

	//m_Info.lMessage = ReturnValue;
	//m_Info.dwSize = 0L;
	//m_Info.dwType = 0L;

	if(ReturnValue == ERROR_SUCCESS)
		return TRUE;

	return FALSE;
}

BOOL CESMRegistry::Read(LPCTSTR pszKey, UINT& iVal)
{
	ASSERT(m_hKey);
	ASSERT(pszKey);

	DWORD dwType;
	DWORD dwSize = sizeof (DWORD);
	DWORD dwDest;

	LONG lReturn = RegQueryValueEx (m_hKey, (LPTSTR) pszKey, NULL,
		&dwType, (BYTE *) &dwDest, &dwSize);

	//m_Info.lMessage = lReturn;
	//m_Info.dwType = dwType;
	//m_Info.dwSize = dwSize;

	if(lReturn == ERROR_SUCCESS)
	{
		iVal = (UINT)dwDest;
		return TRUE;
	}

	return FALSE;
}

void CESMRegistry::Close()
{
	if (m_hKey)
	{
		RegCloseKey (m_hKey);
		m_hKey = NULL;
	}
}

BOOL CESMRegistry::Read (LPCTSTR pszKey, CString& sVal)
{
	ASSERT(m_hKey);
	ASSERT(pszKey);

	DWORD dwType;
	DWORD dwSize = 200;
	TCHAR  szString[255];
	//- 2013-11-27 kcd
	memset(szString, 0, sizeof(szString));
	LONG lReturn = RegQueryValueEx (m_hKey, (LPTSTR) pszKey, NULL,
		&dwType, (BYTE *) szString, &dwSize);

	//m_Info.lMessage = lReturn;
	//m_Info.dwType = dwType;
	//m_Info.dwSize = dwSize;

	if(lReturn == ERROR_SUCCESS)
	{
		sVal = szString;
		return TRUE;
	}

	return FALSE;
}


//------------------------------------------------------------------------------ 
//! @brief
//! @date     2011-11-11
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
BOOL CESMRegistry::Write(LPCTSTR name, LPCTSTR strvalue)
{
	if(ERROR_SUCCESS == RegSetValueEx(m_hKey, name, 0, REG_SZ, (BYTE*)strvalue, (DWORD)_tcslen(strvalue)*2))
	{
		::RegFlushKey(m_hKey);
		return TRUE;
	}
	return FALSE;
}


//------------------------------------------------------------------------------ 
//! @brief
//! @date     2011-11-11
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
BOOL CESMRegistry::Write (LPCTSTR pszKey, DWORD dwVal)
{
	ASSERT(m_hKey);
	ASSERT(pszKey);
	return RegSetValueEx (m_hKey, pszKey, 0L, REG_DWORD,
		(CONST BYTE*) &dwVal, sizeof(DWORD));
}
