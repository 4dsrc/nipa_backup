/////////////////////////////////////////////////////////////////////////////
//
// ESMProcessControl.h: implementation of the Registry class.
//
//
// Copyright (c) 2008 ESMLab, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of ESMLab, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2008-06-26
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

class  CESMProcessControl
{
public:
	CESMProcessControl();
	virtual ~CESMProcessControl();

public:
	//-- CHECK EXIST
	BOOL IsExist(CString strProcess, BOOL bSelf = TRUE);
	int KillProcess(CString strProcess);

	static BOOL StartConsoleProcess(IN CString strCmdLine, IN CString strCurrentDir, IN BOOL bShowWindow, OUT DWORD& dwProcessId, OUT DWORD& dwErrorCode);
	static BOOL IsRunningProcess(IN DWORD dwProcessId);
	
};


