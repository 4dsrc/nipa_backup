/////////////////////////////////////////////////////////////////////////////
//
// VDLogMgr.h: implementation of the LogMgr class.
//
//
// Copyright (c) 2003 ESMLab, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of ESMLab, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2003-11-11
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include <windows.h>
#include <time.h>
#include <fstream>
#include <ostream>

#ifdef UNICODE
typedef wofstream tofstream; 
typedef wstring tstring;
#else
typedef ofstream tofstream; 
typedef string tstring;
#endif


class LogMgr  
{
public:	
	LogMgr(CString strPath, CString strName, int nLimitDay, int nVerbosity);
	virtual ~LogMgr();

	void OutputErrLog(CString  errstring, bool newline = true);
	void OutputLog(int verbosity, CString logline, bool newline = true);
	void Trace(tstring logline);
	void Stop();
	void Start();	
	bool IsDateChanged();
	void SetLimitDay(int limitday);
	void SaveAsNew(CString strNewName);
	CString GetLogFile();
	void DeleteOldFile();
	
private: 
	CString m_strPath;     //"\\10.220.207.128\SQE_Disc\output\10.220.207.116\2011-06-29\log\"
	CString m_strName;     //"ST700.log"
	CString m_strFile;     //"\\10.220.207.128\SQE_Disc\output\10.220.207.116\2011-06-29\log\ST700.log"
	int m_nVerbosity;
	int m_nLimitDay;

	//2014-01-21 kcd
	//잦은 다운으로 인해 변경함
	//tofstream m_fOut; 
	CFile m_fWriteFile;
	TCHAR m_strBeginDate[9];  //"06/29/11"	
	CRITICAL_SECTION m_csLock;

	CStdioFile m_fStdioFile;

};


 
