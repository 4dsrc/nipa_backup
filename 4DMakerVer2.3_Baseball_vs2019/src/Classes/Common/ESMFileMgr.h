#pragma once
#include "vector"
using namespace std;

class CMainFrame;
class CESMFileMgr
{
public:
	CESMFileMgr(CMainFrame* pMainFrm);
	~CESMFileMgr(void);

protected:
	CMainFrame* m_pMainFrame;
	HANDLE		m_hSaveProfileThread, m_hFileCopyThread;
	BOOL		m_bClose;
	CString		m_strTargetFile;
public:
	CString		m_strRecordProfile;
	vector<CString> m_arrIpArray;
	CString m_strTargetFolder;
	int			m_nFilmState;
	BOOL		m_bInit;

	int  GetFilmState() { return m_nFilmState;}
	void SetFilmState(int nFilmState) { m_nFilmState = nFilmState;}
	void SetCurTime();
	BOOL MovieDoneSaveProfile(BOOL bAutoSave = FALSE);
	void PictureDoneSaveProfile();
	void SaveRecordProfile(CString strFileName);
	void LoadRecordProfile(CString strFileName);
	BOOL DoMovieSaveProfile(LPVOID param);
	static unsigned WINAPI DoPictureSaveProfileThread(LPVOID param);
	static unsigned WINAPI DoMovieSaveProfileThread(LPVOID param);
	static unsigned WINAPI DoMovieSaveProfileExThread(LPVOID param);
	static unsigned WINAPI FileCopyThread(LPVOID param);

	//void RemoveWorkFile();
	void RemoveTimeLineEditor();

};
