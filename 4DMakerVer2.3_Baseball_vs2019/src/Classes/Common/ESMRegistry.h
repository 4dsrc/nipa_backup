/////////////////////////////////////////////////////////////////////////////
//
// ESMRegistry.h: implementation of the ESMRegistry class.
//
//
// Copyright (c) 2003 ESMLab, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of ESMLab, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2003-11-11
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include <winreg.h>

#define REG_RECT	0x0001
#define REG_POINT	0x0002

//UNICODE 빌드시 Read() 함수에서 비정상적으로 읽혀
//확인하기 위해 사용안하는 함수는 임시로 주석처리함.
//추후 UNICODE 빌드시 사용하기 위해 확인하고 사용하면 됨.

class CESMRegistry : public CObject
{
// Construction
public:
	CESMRegistry(HKEY hKeyRoot = HKEY_LOCAL_MACHINE);
	virtual ~CESMRegistry();

	//struct REGINFO
	//{
	//	LONG lMessage;
	//	DWORD dwType;
	//	DWORD dwSize;
	//} m_Info;

// Operations
public:

	HKEY 	m_hKey;
	void Close();

	BOOL VerifyKey (HKEY hKeyRoot, LPCTSTR pszPath);
	//BOOL VerifyKey (LPCTSTR pszPath);
	//BOOL VerifyValue (LPCTSTR pszValue);
	BOOL CreateKey (HKEY hKeyRoot, LPCTSTR pszPath);
	BOOL Open (HKEY hKeyRoot, LPCTSTR pszPath);

	BOOL Write (LPCTSTR name, LPCTSTR strvalue);
	BOOL Write (LPCTSTR pszKey, DWORD dwVal);

	BOOL Read (LPCTSTR pszKey, CString& sVal);	
	BOOL Read (LPCTSTR pszKey, UINT& iVal);
	

	//-- DISCOVERY
	//CString GetEventLogLists(HKEY hKeyRoot, LPCTSTR pszPath);

	//-- 2003-11-11
	//-- DISCOVERY - SERVICE
	//CString GetServiceList();
	//CString GetDispName(CString strService, int& nType);

	//-- GET VALUES
	//BOOL GetServiceItem(int nIndex, CString& strESMRegistry, CString& strDisplay, int& nType);

protected:	
	//CString m_sPath;
};
