////////////////////////////////////////////////////////////////////////////////
//
//	ESMTimerEx.h : implementation of the 4DMaker Extended Timer Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	ryumin (ryumin@esmlab.com)
// @Date	2014-11-04
//
////////////////////////////////////////////////////////////////////////////////


#ifndef	__ESMLAB_COMMON_TIMER_EX_HEADER_H__
#define	__ESMLAB_COMMON_TIMER_EX_HEADER_H__

#include <atlbase.h>

static void CALLBACK TimerProc(void*, BOOLEAN);

///////////////////////////////////////////////////////////////////////////////
//
// class CTimerEx
//
class CTimerEx
{
public:
	CTimerEx() : m_hTimer(NULL), m_mutexCount(0) {}

	virtual ~CTimerEx()	{ Stop(); }

	bool Start(unsigned int interval,   // interval in ms
		bool immediately = false,// true to call first event immediately
		bool once = false)       // true to call timed event only once
	{
		if( m_hTimer )
		{
			return false;
		}

		SetCount(0);
		BOOL success = CreateTimerQueueTimer( &m_hTimer,
			NULL,
			TimerProc,
			this,
			immediately ? 0 : interval,
			once ? 0 : interval,
			WT_EXECUTEINTIMERTHREAD);

		return( success != 0 );
	}

	void Stop()
	{
		DeleteTimerQueueTimer( NULL, m_hTimer, NULL );
		m_hTimer = NULL ;
	}

	virtual void OnTimedEvent()
	{
		// Override in derived class
	}

	void SetCount(int value)
	{
		InterlockedExchange( &m_mutexCount, value );
	}

	int GetCount()
	{
		return InterlockedExchangeAdd( &m_mutexCount, 0 );
	}

private:
	HANDLE m_hTimer;
	long m_mutexCount;
};

///////////////////////////////////////////////////////////////////////////////
//
// TimerProc
//
void CALLBACK TimerProc(void* param, BOOLEAN timerCalled)
{
	CTimerEx* timer = static_cast<CTimerEx*>(param);
	timer->SetCount( timer->GetCount()+1 );
	timer->OnTimedEvent();
};

///////////////////////////////////////////////////////////////////////////////
//
// template class TTimer
//

template <class T> class TTimerEx : public CTimerEx
{
public:
	typedef private void (T::*TimedFunction)(void);

	TTimerEx() : m_pTimedFunction(NULL), m_pClass(NULL)	{}

	void SetTimedEvent(T *pClass, TimedFunction pFunc)
	{
		m_pClass         = pClass;
		m_pTimedFunction = pFunc;
	}

protected:
	void OnTimedEvent()
	{
		if (m_pTimedFunction && m_pClass)
		{
			(m_pClass->*m_pTimedFunction)();
		}
	}

private:
	T *m_pClass;
	TimedFunction m_pTimedFunction;
};


//
//template <class T, typename T_Result, typename T_Arg_1>
//class TTimerEx : public CTimerEx
//{
//public:
//	typedef private T_Result (T::*TimedFunction)(T_Arg_1);
//
//	TTimerEx() : m_pTimedFunc(NULL), m_pClass(NULL), m_pResult(NULL)
//	{
//	}
//	virtual ~TTimerEx()
//	{
//	}
//
//	//T_Result operator()(T* pClass) const
//	//{
//	//	return ((pClass->*TimedFunction)(m_arg1));
//	//}
//
//	void SetTimedEvent(T *pClass, TimedFunction pFunc, T_Arg_1 arg_1)
//	{
//		m_pClass	= pClass;
//		m_pTimedFunc= pFunc;
//		m_arg_1		= arg_1;
//	}
//	void OnTimedEvent()
//	{
//		if (m_pTimedFunc && m_pClass)
//		{
//			T_Result result = (m_pClass->*m_pTimedFunc)(m_arg_1);
//			*m_pResult = result;
//		}
//	}
//
//private:
//	T*				m_pClass;
//	T_Result*		m_pResult;
//	T_Arg_1			m_arg_1;
//	TimedFunction	m_pTimedFunc;
//};


#endif	//	__ESMLAB_COMMON_TIMER_EX_HEADER_H__