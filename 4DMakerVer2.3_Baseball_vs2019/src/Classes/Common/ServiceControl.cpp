////////////////////////////////////////////////////////////////////////////////
//
//	ServiceControl.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-06-25
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ServiceControl.h"
#include "TGRegistry.h"

//** DOWN PROCESS VALUE
#define	NORMAL_DATA			1
#define	DOWN_ZERO			0
#define NOT_FOUND			-1

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CServiceControl::CServiceControl()
{
	//** POINTERS
	m_pRegKey = NULL;
	m_pSysService = NULL;
	m_schSCManager = NULL;
	m_myServiceHandle = NULL;
}

CServiceControl::~CServiceControl()
{
	//** 2003-11-22
	ReleaseHandle();

	if(m_pRegKey)
		delete m_pRegKey;
	if(m_pSysService)
		delete m_pSysService;	
}


//** CHECK SERVICE STATUS
void CServiceControl::CheckService(CString strTitle, CString& strDisplay, BOOL& bService, BOOL& bStatus)
{
	if(!IsExistService(strTitle,strDisplay)) 
	{
		//** SET SERVICE LOAD
		bService = FALSE;
		bStatus = FALSE;
	}
	else 
	{
		//** SET SERVICE LOAD
		bService = TRUE;
		switch(GetServiceHandle(strTitle))
		{
		case SERVICE_RUNNING:
			bStatus = TRUE;
			break;
		case SERVICE_STOPPED:
		case SERVICE_START_PENDING:
		case SERVICE_STOP_PENDING:   
		case SERVICE_CONTINUE_PENDING:
		case SERVICE_PAUSE_PENDING:
		case SERVICE_PAUSED:
			bStatus = FALSE;
			break;
		}		
	}
}

//** SERVICE CHECK
BOOL CServiceControl::IsExistService(CString strService, CString &strDisplay)
{
	//** USING REGEDIT KEY
	if(m_pRegKey) {
		delete m_pRegKey;
		m_pRegKey = NULL;	
	}

	//**
	//** REG KEY
 	m_pRegKey = new CSysRegKey(HKEY_LOCAL_MACHINE,
							   "SYSTEM\\CurrentControlSet\\Services",
							   KEY_ALL_ACCESS);
	if (!m_pRegKey->OpenState())
	{
		MessageBox(NULL,L"Unable to open registry services.",L"Registry Error",MB_OK);
		delete m_pRegKey;
		m_pRegKey = NULL;
		return FALSE;
	}

	//** REG KEY
	if(m_pSysService)
	{
		delete m_pSysService;
		m_pSysService = NULL;
	}

	m_pSysService = new CSysService();

	if (!m_pSysService->Open())
	{
		MessageBox(NULL,L"Unable to open service control manager",L"OpenSCManager",MB_OK);
		delete m_pRegKey;
		delete m_pSysService;
		m_pRegKey = NULL;
		m_pSysService = NULL;
		return FALSE;
	}

	if (!m_pRegKey->QueryInfoKey())
	{
		MessageBox(NULL,L"Unable to query registry info.",L"Registry Error",MB_OK);
		return FALSE;
	}

	DWORD	l_Index = 0;
	CString l_ServiceName;
	
	do
	{
		l_ServiceName = m_pRegKey->EnumKey(l_Index);
		if (l_ServiceName.IsEmpty())
		{
			if (m_pRegKey->Result() == ERROR_NO_MORE_ITEMS)
			    break;
			l_Index++;
			continue;
		}
		if( strService == l_ServiceName)
		{
			if (m_pSysService->GetServiceInformation(l_ServiceName))
			{
				strDisplay = (LPCTSTR)m_pSysService->ServiceDisplayName();
			}
			return TRUE;
		}
		l_Index++;
	} while(TRUE);
	return FALSE;
}

UINT CServiceControl::GetServiceHandle(CString strService)
{
	//** SET FREE HANDLE
	ReleaseHandle();
	UINT nStatus = 0;	
	//**
	//** FIRST OPEN THE HANDLE TO THE SCM
	if(m_schSCManager)
		CloseServiceHandle(m_schSCManager);
	
	m_schSCManager = OpenSCManager( NULL, NULL, GENERIC_WRITE);  // full access rights */
	DWORD dwDesiredAccess = SERVICE_ALL_ACCESS|GENERIC_WRITE;	

	//**
	//** NOW OPEN THE HANDLE TO THE SERVICE
	if(m_myServiceHandle)
		CloseServiceHandle(m_myServiceHandle);

	m_myServiceHandle = OpenService(m_schSCManager,
		 (LPCTSTR)strService, dwDesiredAccess);
	
	if (m_myServiceHandle == NULL) 
	{
		DWORD er = GetLastError();
		switch(er)
		{
		case ERROR_ACCESS_DENIED: 
			TRACE("Error type : Access is denied.\n");
			break;
		case ERROR_INVALID_NAME: 
			TRACE("Error type : Invalid service name.\n");
			break;
		case ERROR_SERVICE_DOES_NOT_EXIST: 
			TRACE("Error type : Service does not exist.\n");
			break;
		default:
			TRACE("Error type : Not known.\n");
			break;
		}		
		return NULL;
	}
	
	return GetStatusService();	
}

UINT CServiceControl::GetStatusService()
{
	if(!QueryServiceStatus(m_myServiceHandle,
		&m_ServiceStatus))
	{
		// Error situation
		DWORD er = GetLastError();
		if(er = ERROR_ACCESS_DENIED)
			TRACE("Error : Acces to service is denied.\n");
		else
			TRACE("Error : Invalid Handle.\n");		
	}
	return m_ServiceStatus.dwCurrentState;
}

void CServiceControl::ReleaseHandle()
{
	if(m_schSCManager) 
	{
		CloseServiceHandle(m_schSCManager); // close the handle to SCM
		m_schSCManager = NULL;
	}
	if(m_myServiceHandle)
	{
		CloseServiceHandle(m_myServiceHandle); // close the handle to service, we will reopen it if we need it further
		m_myServiceHandle = NULL;
	}
}


//**
//** INSTALL SERVISCE
BOOL CServiceControl::OnInstall(CString strTitle, CString strDisplay, CString strFileName, BOOL bMsg)
{
	DWORD dwDesiredAccess;
	DWORD dwServiceType;
	DWORD dwStartType;
	DWORD dwErrorControl;
	GetParams(&dwDesiredAccess,&dwServiceType,&dwStartType,&dwErrorControl);
	//** GET SERVICE HANDLE
	SC_HANDLE schSCManager;
	schSCManager = OpenSCManager( NULL, NULL, GENERIC_WRITE);
	if(!schSCManager)
		return FALSE;	

	SC_HANDLE serviceHandle;
	//** First open the handle to the SCM
	serviceHandle = CreateService(	schSCManager,			//** HANDLE TO SCM DATABASE 
									(LPCTSTR)strTitle,		//** TITLE NAME
									(LPCTSTR)strDisplay,	//** DISPLAY NAME
									dwDesiredAccess,		//** TYPE OF ACCESS TO SERVICE
									dwServiceType,			//** TYPE OF SERVICE
									dwStartType,			//** WHEN TO START SERVICE
									dwErrorControl,			//** SEVERITY OF SERVICE FAILUREC
									(LPCTSTR)strFileName,	//** FILE NAME
									L"",						//** NAME OF LOAD ORDERING GROUP
									NULL,					//** TAG IDENTIFIER
									L"",						//** ARRAY OF DEPENDENCY NAMES
									NULL,					//** ACCOUNT NAME 
									NULL);					//** ACCOUNT PASSWORD

	if (serviceHandle == NULL) 
	{
		DWORD er = GetLastError();
		switch(er)
		{
		case ERROR_ACCESS_DENIED:	if(bMsg) AfxMessageBox(L"Error while creating the service."
										L"\rError type:\r\tAccess is denied.",MB_ICONSTOP);break;
		case ERROR_INVALID_NAME:	if(bMsg) AfxMessageBox(L"Error while creating the service."
										L"\rError type:\r\tInvalid service name.",MB_ICONSTOP);break;
		case ERROR_SERVICE_EXISTS:	if(bMsg) AfxMessageBox(L"Error while creating the service."
										L"\rError type:\r\tService already exists.",MB_ICONSTOP);
			CloseServiceHandle(schSCManager); // close the handle to SCM
			CloseServiceHandle(serviceHandle); // close the handle to service, we will reopen it if we need it further
			return TRUE;
			break;
		case ERROR_DUP_NAME:		if(bMsg) AfxMessageBox(L"Error while creating the service."
										L"\rError type:\r\tThe display name is already in use.",MB_ICONSTOP);
			break;
		default:					if(bMsg) AfxMessageBox(L"Error while creating the service."
							L"\rError type:\r\tNot known.",MB_ICONSTOP);
			break;
		}
		CloseServiceHandle(schSCManager); // close the handle to SCM
		CloseServiceHandle(serviceHandle); // close the handle to service, we will reopen it if we need it further
		return FALSE;
	}
	else
	{
		if(bMsg)
			AfxMessageBox(L"Service Registered.\nInstallation completed successfuly",MB_OK|MB_ICONINFORMATION);		
	}
	
	CloseServiceHandle(schSCManager); // close the handle to SCM
	CloseServiceHandle(serviceHandle); // close the handle to service, we will reopen it if we need it further
	return TRUE;
}


//**
//** UNINSTALL
BOOL CServiceControl::OnUninstall(CString strTitle, BOOL bMsg) 
{
	//**
	//** GET SERVICE HANDLE
	SC_HANDLE schSCManager;
	schSCManager = OpenSCManager( NULL, NULL, GENERIC_WRITE);
	if(!schSCManager)
		return FALSE;	

	DWORD dwDesiredAccess = SERVICE_ALL_ACCESS;	
	SC_HANDLE serviceHandle;
	serviceHandle = OpenService(schSCManager, strTitle, dwDesiredAccess);
	if (!serviceHandle) {
		CloseServiceHandle(schSCManager);
		return FALSE;
	}
	
	if (serviceHandle == NULL) 
	{
		DWORD er = GetLastError();
		switch(er)
		{
		case ERROR_ACCESS_DENIED:	if(bMsg) AfxMessageBox(L"Error while deleting the service."
									  L"\rError type:\r\tAccess is denied.",MB_OK|MB_ICONSTOP);break;
		case ERROR_INVALID_NAME:	if(bMsg) AfxMessageBox(L"Error while deleting the service."
									 L"\rError type:\r\tInvalid service name.",MB_OK|MB_ICONSTOP);break;
		case ERROR_SERVICE_DOES_NOT_EXIST: if(bMsg)	AfxMessageBox(L"Error while deleting the service."
											   L"\rError type:\r\tService does not exist.",MB_OK|MB_ICONSTOP);break;
		default:if(bMsg)	AfxMessageBox(L"Error deleting opening the service."
					L"\rError type:\r\tNot known.",MB_OK|MB_ICONSTOP);break;
		}
		return FALSE;
	}
	else
	{
		// Ok delete the service
		if(!DeleteService(serviceHandle))
		{
			DWORD er = GetLastError();
			switch(er)
			{
			case ERROR_ACCESS_DENIED: if(bMsg)	AfxMessageBox(L"Error while deleting the service."
										  L"\rError type:\r\tAccess is denied.",MB_OK|MB_ICONSTOP);break;
			case ERROR_SERVICE_MARKED_FOR_DELETE: if(bMsg)	AfxMessageBox(L"Error while deleting the service."
												   L"\rError type:\r\tThe specified service has already been marked for deletion.",MB_OK|MB_ICONSTOP);break;
			default:if(bMsg)	AfxMessageBox(L"Error while deleting the service."
						L"\rError type:\r\tNot known.",MB_OK|MB_ICONSTOP);break;
			}
			CloseServiceHandle(schSCManager); // close the handle to SCM
			CloseServiceHandle(serviceHandle); // close the handle to service, we will reopen it if we need it further
			return FALSE;
		}
		else
			if(bMsg)	AfxMessageBox(L"The service has been marked for deletion.\rYou will have to restart the system for complete\runinstall.");		
	}

	CloseServiceHandle(schSCManager); // close the handle to SCM
	CloseServiceHandle(serviceHandle); // close the handle to service, we will reopen it if we need it further
	return TRUE;
}

void CServiceControl::GetParams( LPDWORD dwDesiredAccess,LPDWORD dwServiceType, LPDWORD dwStartType, LPDWORD dwErrorControl)
{
	*dwServiceType		= SERVICE_WIN32_OWN_PROCESS ;
	*dwStartType		= SERVICE_AUTO_START;
	*dwErrorControl		= SERVICE_ERROR_NORMAL;
	*dwDesiredAccess	= SERVICE_ALL_ACCESS|GENERIC_WRITE;
}

BOOL CServiceControl::OnStart(CString strTitle, BOOL bMsg)
{
	//**
	//** 2003-08-04
	//** RESTORE REGSTRY
	//** 
	RestoreProcRegistry();

	//** 2003-09-23
	//** DISK LOGICAL,PHYGICAL
	RestoreDiskRegistry();

	//** 2003-10-10
	//** WEB SERVICE
	RestoreW3SVCRegistry();


	//**
	//** GET SERVICE HANDLE
	SC_HANDLE schSCManager;
	schSCManager = OpenSCManager( NULL, NULL, GENERIC_WRITE);
	if(!schSCManager)
		return FALSE;	

	DWORD dwDesiredAccess = SERVICE_ALL_ACCESS;	
	SC_HANDLE serviceHandle;
	serviceHandle = OpenService(schSCManager, strTitle, dwDesiredAccess);
	if (!serviceHandle) {
		CloseServiceHandle(schSCManager);
		return FALSE;
	}

	//**
	if(!StartService(serviceHandle, 0, NULL))
	{
		DWORD er = GetLastError();
		switch(er)
		{
		case ERROR_FILE_NOT_FOUND:				//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tFILE NOT EXIST.",MB_OK|MB_ICONSTOP);break;
		case ERROR_ACCESS_DENIED:				//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tAccess is denied.",MB_OK|MB_ICONSTOP);break;
		case ERROR_PATH_NOT_FOUND:				//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tThe service binary file could not be found.",MB_OK|MB_ICONSTOP);break;
		case ERROR_SERVICE_REQUEST_TIMEOUT:		//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tThe service did not respond to the start request in a timely fashion.",MB_OK|MB_ICONSTOP);break;
		case ERROR_SERVICE_DATABASE_LOCKED:		//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tThe database is locked.",MB_OK|MB_ICONSTOP);break;
		case ERROR_SERVICE_DISABLED:			//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tThe service has been disabled.",MB_OK|MB_ICONSTOP);break;
		case ERROR_SERVICE_LOGON_FAILED:		//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tThe service could not be logged on.",MB_OK|MB_ICONSTOP);break;
		case ERROR_SERVICE_MARKED_FOR_DELETE:	//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tThe service has been marked for deletion.",MB_OK|MB_ICONSTOP);break;
		case ERROR_SERVICE_DEPENDENCY_FAIL:		//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tThe service depends on another service that\r\thas failed to start.",MB_OK|MB_ICONSTOP);break;
		case ERROR_SERVICE_DEPENDENCY_DELETED:	//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tThe service depends on a service that does\r\tnot exist or has been marked for deletion.",MB_OK|MB_ICONSTOP);break;
		default:								//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tNot known.",MB_OK|MB_ICONSTOP);
			break;
		}
		//** RELEASE HANDLE
		CloseServiceHandle(serviceHandle);
		CloseServiceHandle(schSCManager);
		return FALSE;
	}

	//** RELEASE HANDLE
	CloseServiceHandle(serviceHandle);
	CloseServiceHandle(schSCManager);
	return TRUE;
}

BOOL CServiceControl::OnStop(CString strTitle,BOOL bMsg)
{
	//**
	//** GET SERVICE HANDLE
	SC_HANDLE schSCManager;
	SC_HANDLE serviceHandle;
	DWORD dwDesiredAccess = SERVICE_ALL_ACCESS|GENERIC_WRITE;	

	schSCManager = OpenSCManager( NULL, NULL, GENERIC_WRITE);
	if(!schSCManager)
		return FALSE;	

	serviceHandle = OpenService(schSCManager, (LPCTSTR)strTitle, dwDesiredAccess);
	if (!serviceHandle) {
		CloseServiceHandle(schSCManager);
		return FALSE;
	}

	//**
	//** DO THAT
	SERVICE_STATUS ServiceStatus = m_ServiceStatus;
	if(!ControlService(serviceHandle, SERVICE_CONTROL_STOP, &ServiceStatus))
	{
		// Error occured
		DWORD er = GetLastError();
		switch(er)
		{
		case ERROR_ACCESS_DENIED:				//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tAccess is denied.",MB_OK|MB_ICONSTOP);break;
		case ERROR_DEPENDENT_SERVICES_RUNNING:	//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tThe service cannot be stopped because other running services are dependent on it.",MB_OK|MB_ICONSTOP);break;
		case ERROR_SERVICE_REQUEST_TIMEOUT:		//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tThe service did not respond to the start request in a timely fashion.",MB_OK|MB_ICONSTOP);break;
		case ERROR_INVALID_SERVICE_CONTROL:		//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tSpecified action is not availaible for this service.",MB_OK|MB_ICONSTOP);break;
		default:								//if(bMsg) AfxMessageBox("Error while controlling the service.\rError type:\r\tNot known.",MB_OK|MB_ICONSTOP);
			break;
		}
		//** RELEASE HANDLE
		CloseServiceHandle(serviceHandle);
		CloseServiceHandle(schSCManager);
		return FALSE;
	}
	//** RELEASE HANDLE
	CloseServiceHandle(serviceHandle);
	CloseServiceHandle(schSCManager);

	//**
	//** KILL PROCESS
	int n = 30;
	while(n)
	{
		n--;
		if(KillProcess(strTitle))
			break;
		Sleep(500);
	}	

	return TRUE;
}

BOOL CServiceControl::OnContinue(CString strTitle)
{
	//**
	//** GET SERVICE HANDLE
	SC_HANDLE schSCManager;
	SC_HANDLE serviceHandle;
	DWORD dwDesiredAccess = SERVICE_ALL_ACCESS|GENERIC_WRITE;	

	schSCManager = OpenSCManager( NULL, NULL, GENERIC_WRITE);
	if(!schSCManager)
		return FALSE;	

	serviceHandle = OpenService(m_schSCManager, (LPCTSTR)strTitle, dwDesiredAccess);
	if (!serviceHandle) {
		CloseServiceHandle(schSCManager);
		return FALSE;
	}

	//**
	//** DO THAT
	SERVICE_STATUS ServiceStatus = m_ServiceStatus;
	if(!ControlService(serviceHandle, SERVICE_CONTROL_CONTINUE, &ServiceStatus))
	{
		// Error occured
		DWORD er = GetLastError();
		switch(er)
		{
		case ERROR_ACCESS_DENIED:				//if(bMsg)	AfxMessageBox("Error while controlling the service.\rError type:\r\tAccess is denied.",MB_OK|MB_ICONSTOP);break;
		case ERROR_DEPENDENT_SERVICES_RUNNING:	//if(bMsg)	AfxMessageBox("Error while controlling the service.\rError type:\r\tThe service cannot be stopped because other running services are dependent on it.",MB_OK|MB_ICONSTOP);break;
		case ERROR_SERVICE_REQUEST_TIMEOUT:		//if(bMsg)	AfxMessageBox("Error while controlling the service.\rError type:\r\tThe service did not respond to the start request in a timely fashion.",MB_OK|MB_ICONSTOP);break;
		case ERROR_INVALID_SERVICE_CONTROL:		//if(bMsg)	AfxMessageBox("Error while controlling the service.\rError type:\r\tSpecified action is not availaible for this service.",MB_OK|MB_ICONSTOP);break;
		default:								//if(bMsg)	AfxMessageBox("Error while controlling the service.\rError type:\r\tNot known.",MB_OK|MB_ICONSTOP);
			break;
		}
		//** RELEASE HANDLE
		CloseServiceHandle(serviceHandle);
		CloseServiceHandle(schSCManager);
		return FALSE;
	}
	
	//** RELEASE HANDLE
	CloseServiceHandle(serviceHandle);
	CloseServiceHandle(schSCManager);
	return TRUE;
}

BOOL CServiceControl::OnPause(CString strTitle)
{
	//**
	//** GET SERVICE HANDLE
	SC_HANDLE schSCManager;
	SC_HANDLE serviceHandle;
	DWORD dwDesiredAccess = SERVICE_ALL_ACCESS|GENERIC_WRITE;	

	schSCManager = OpenSCManager( NULL, NULL, GENERIC_WRITE);
	if(!schSCManager)
		return FALSE;	

	serviceHandle = OpenService(m_schSCManager, (LPCTSTR)strTitle, dwDesiredAccess);
	if (!serviceHandle) {
		CloseServiceHandle(schSCManager);
		return FALSE;
	}

	//**
	//** DO THAT
	SERVICE_STATUS ServiceStatus = m_ServiceStatus;
	if(!ControlService(serviceHandle, SERVICE_CONTROL_PAUSE, &ServiceStatus))
	{
		// Error occured
		DWORD er = GetLastError();
		switch(er)
		{
		case ERROR_ACCESS_DENIED:				//if(bMsg)	AfxMessageBox("Error while controlling the service.\rError type:\r\tAccess is denied.",MB_OK|MB_ICONSTOP);break;
		case ERROR_DEPENDENT_SERVICES_RUNNING:	//if(bMsg)	AfxMessageBox("Error while controlling the service.\rError type:\r\tThe service cannot be stopped because other running services are dependent on it.",MB_OK|MB_ICONSTOP);break;
		case ERROR_SERVICE_REQUEST_TIMEOUT:		//if(bMsg)	AfxMessageBox("Error while controlling the service.\rError type:\r\tThe service did not respond to the start request in a timely fashion.",MB_OK|MB_ICONSTOP);break;
		case ERROR_INVALID_SERVICE_CONTROL:		//if(bMsg)	AfxMessageBox("Error while controlling the service.\rError type:\r\tSpecified action is not availaible for this service.",MB_OK|MB_ICONSTOP);break;
		default:								//if(bMsg)	AfxMessageBox("Error while controlling the service.\rError type:\r\tNot known.",MB_OK|MB_ICONSTOP);
			break;
		}
		//** RELEASE HANDLE
		CloseServiceHandle(serviceHandle);
		CloseServiceHandle(schSCManager);
		return FALSE;
	}
	//** RELEASE HANDLE
	CloseServiceHandle(serviceHandle);
	CloseServiceHandle(schSCManager);
	return TRUE;
}



BOOL CServiceControl::KillProcess(CString strProcess)
{
	TASK_LIST   tlist[255];
    // Obtain the ability to manipulate other processes 
	DWORD numTasks = GetTaskList( tlist, 255 ); 

	EnableDebugPriv(); 

	CString strUpper;
	strUpper = strProcess;
	strUpper.MakeUpper ();

	//** CHECK EXEPTIONS	
	if(!strUpper.Compare(L"TRACERT"))
		return FALSE;

	for (int i=0; i<(int)numTasks; i++) {
		CString strExistProcess;
		strExistProcess = tlist[i].ProcessName;
		strExistProcess.MakeUpper ();

		if(	(strExistProcess.Find(strUpper) != NOT_FOUND))
			return KillProcess(&tlist[i],true);
    } 
	return FALSE;
}

BOOL CServiceControl::KillProcess(PTASK_LIST tlist,BOOL fForce)
{
	HANDLE            hProcess;
	if (fForce || !tlist->hwnd) {
		hProcess = OpenProcess( PROCESS_TERMINATE, 0, tlist->dwProcessId );
		if (!hProcess)
		{
			::PostMessage((HWND)tlist->hwnd, WM_CLOSE, 0, 0 );
			return FALSE;
		}
		else
		{
			if (!TerminateProcess( hProcess, 1 )) 
			{
				CloseHandle( hProcess );
				return FALSE;
			}			
		}
		CloseHandle( hProcess );
		return TRUE;	
	}
	return FALSE;
}    

#include <winperf.h>   // for Windows NT
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//
// manafest constants
//
#define INITIAL_SIZE        51200
#define EXTEND_SIZE         25600
#define REGKEY_PERF         _T("software\\microsoft\\windows nt\\currentversion\\perflib")
#define REGSUBKEY_COUNTERS  _T("Counters")
#define PROCESS_COUNTER     "process"
#define PROCESSID_COUNTER   "id process"
#define UNKNOWN_TASK        "unknown"



DWORD CServiceControl::GetTaskList(PTASK_LIST pTask, DWORD  dwNumTasks)
{
    DWORD                        rc;
    HKEY                         hKeyNames;
    DWORD                        dwType;
    DWORD                        dwSize;
    LPBYTE                       buf = NULL;
    TCHAR                         szSubKey[1024];
    LANGID                       lid;
    LPSTR                        p;
    LPSTR                        p2;
    PPERF_DATA_BLOCK             pPerf;
    PPERF_OBJECT_TYPE            pObj;
    PPERF_INSTANCE_DEFINITION    pInst;
    PPERF_COUNTER_BLOCK          pCounter;
    PPERF_COUNTER_DEFINITION     pCounterDef;
    DWORD                        i;
    DWORD                        dwProcessIdTitle;
    DWORD                        dwProcessIdCounter;
    CHAR                         szProcessName[MAX_PATH];
    DWORD                        dwLimit = dwNumTasks - 1;

    lid = MAKELANGID( LANG_ENGLISH, SUBLANG_NEUTRAL );
    wsprintf( szSubKey, _T("%s\\%03x"), REGKEY_PERF, lid );
    rc = RegOpenKeyEx( HKEY_LOCAL_MACHINE,
                       szSubKey,
                       0,
                       KEY_READ,
                       &hKeyNames
                     );
    if (rc != ERROR_SUCCESS) {
        goto exit;
    }

    //** GET THE BUFFER SIZE FOR THE COUNTER NAMES
    rc = RegQueryValueEx( hKeyNames,
                          REGSUBKEY_COUNTERS,
                          NULL,
                          &dwType,
                          NULL,
                          &dwSize
                        );

    if (rc != ERROR_SUCCESS) {
        goto exit;
    }

    //** ALLOCATE THE COUNTER NAMES BUFFER    
    buf = (LPBYTE) malloc( dwSize );
    if (buf == NULL) {
        goto exit;
    }
    memset( buf, 0, dwSize );

    //** READ THE COUNTER NAMES FROM THE REGISTRY    
    rc = RegQueryValueEx( hKeyNames,
                          REGSUBKEY_COUNTERS,
                          NULL,
                          &dwType,
                          buf,
                          &dwSize
                        );

    if (rc != ERROR_SUCCESS) {
        goto exit;
    }

    p = (LPSTR)buf;
    while (*p) {
        if (p > (LPSTR)buf) {
            for( p2=p-2; isdigit(*p2); p2--) ;
            }
        if (stricmp(p, PROCESS_COUNTER) == 0) {
            // look backwards for the counter number
            for( p2=p-2; isdigit(*p2); p2--) ;
#ifdef UNICODE
			mbstowcs(szSubKey, p2+1, strlen(p2+1)); 
#else			
			strcpy( szSubKey, p2+1 );
#endif

        }
        else
        if (stricmp(p, PROCESSID_COUNTER) == 0) {
            // look backwards for the counter number
            for( p2=p-2; isdigit(*p2); p2--) ;
            dwProcessIdTitle = atol( p2+1 );
        }
        // next string
        p += (strlen(p) + 1);
    }
    // free the counter names buffer
	free( buf );

	//** ALLOCATE THE INITIAL BUFFER FOR THE PERFORMANCE DATA
    dwSize = INITIAL_SIZE;    
	buf = (unsigned char*)malloc(dwSize); 
    if (!buf) {
        goto exit;
    }
    memset( buf, 0, dwSize );


    while (TRUE) {
        rc = RegQueryValueEx( HKEY_PERFORMANCE_DATA,
                              szSubKey,
                              NULL,
                              &dwType,
                              buf,
                              &dwSize
                            );

        pPerf = (PPERF_DATA_BLOCK) buf;
        //** CHECK FOR SUCCESS AND VALID PERF DATA BLOCK SIGNATURE
        if ((rc == ERROR_SUCCESS) &&
            (dwSize > 0) &&
            (pPerf)->Signature[0] == (WCHAR)'P' &&
            (pPerf)->Signature[1] == (WCHAR)'E' &&
            (pPerf)->Signature[2] == (WCHAR)'R' &&
            (pPerf)->Signature[3] == (WCHAR)'F' ) {
            break;
        }
        //** IF BUFFER IS NOT BIG ENOUGH, REALLOCATE AND TRY AGAIN
        if (rc == ERROR_MORE_DATA) {
            dwSize += EXTEND_SIZE;
			buf = (unsigned char*)realloc( buf, dwSize );			
            memset( buf, 0, dwSize );
        }
        else {
            goto exit;
        }
    }

    //** SET THE PERF_OBJECT_TYPE POINTER
    pObj = (PPERF_OBJECT_TYPE) ((DWORD)pPerf + pPerf->HeaderLength);

    pCounterDef = (PPERF_COUNTER_DEFINITION) ((DWORD)pObj + pObj->HeaderLength);
    for (i=0; i<(DWORD)pObj->NumCounters; i++) {
        if (pCounterDef->CounterNameTitleIndex == dwProcessIdTitle) {
            dwProcessIdCounter = pCounterDef->CounterOffset;
            break;
        }
        pCounterDef++;
    }

    dwNumTasks = min( dwLimit, (DWORD)pObj->NumInstances );

    pInst = (PPERF_INSTANCE_DEFINITION) ((DWORD)pObj + pObj->DefinitionLength);

    for (i=0; i<dwNumTasks; i++) {
        // pointer to the process name
        p = (LPSTR) ((DWORD)pInst + pInst->NameOffset);
	    // convert it to ascii
        rc = WideCharToMultiByte( CP_ACP,
                                  0,
                                  (LPCWSTR)p,
                                  -1,
                                  szProcessName,
                                  sizeof(szProcessName),
                                  NULL,
                                  NULL
                                );

        if (!rc) {
		// if we cant convert the string then use a default value
#ifdef UNICODE
			strcpy(pTask->ProcessName, UNKNOWN_TASK);//, strlen(UNKNOWN_TASK)); 
#else			
			strcpy( pTask->ProcessName, UNKNOWN_TASK );			
#endif  
        }

        if (strlen(szProcessName)+4 <= sizeof(pTask->ProcessName)) {
            strcpy( pTask->ProcessName, szProcessName );
            //strcat( pTask->ProcessName, ".exe" );
        }

        // get the process id
        pCounter = (PPERF_COUNTER_BLOCK) ((DWORD)pInst + pInst->ByteLength);
        pTask->flags = 0;
        pTask->dwProcessId = *((LPDWORD) ((DWORD)pCounter + dwProcessIdCounter));
        if (pTask->dwProcessId == 0) {
            pTask->dwProcessId = (DWORD)-2;
        }

        // next process
        pTask++;
        pInst = (PPERF_INSTANCE_DEFINITION) ((DWORD)pCounter + pCounter->ByteLength);
    }

exit:
    if (buf) {
        free( buf );
    }

    RegCloseKey( hKeyNames );
    RegCloseKey( HKEY_PERFORMANCE_DATA );
    return dwNumTasks;
}

BOOL CServiceControl::EnableDebugPriv()
{
    HANDLE hToken;
    LUID DebugValue;
    TOKEN_PRIVILEGES tkp;

    // Retrieve a handle of the access token
    if (!OpenProcessToken(GetCurrentProcess(),
            TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY,
            &hToken)) {
        printf("OpenProcessToken failed with %d\n", GetLastError());
        return FALSE;
    }

    // Enable the SE_DEBUG_NAME privilege
    if (!LookupPrivilegeValue(NULL,//(LPSTR) NULL,
            SE_DEBUG_NAME,
            &DebugValue)) {
        wprintf(L"LookupPrivilegeValue failed with %d\n", GetLastError());
        return FALSE;
    }

    tkp.PrivilegeCount = 1;
    tkp.Privileges[0].Luid = DebugValue;
    tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

    AdjustTokenPrivileges(hToken,
        FALSE,
        &tkp,
        sizeof(TOKEN_PRIVILEGES),
        (PTOKEN_PRIVILEGES) NULL,
        (PDWORD) NULL);

    // The return value of AdjustTokenPrivileges can't be tested
    if (GetLastError() != ERROR_SUCCESS) {
        printf("AdjustTokenPrivileges failed with %d\n", GetLastError());
	
		//** 2003-01-13
		CloseHandle(hToken);
        return FALSE;
    }

	//** 2003-01-13
	CloseHandle(hToken);
    return TRUE;
}

void CServiceControl::RestoreProcRegistry()
{
	DWORD word = 0;
	CTGRegistry Registry;
	LPCTSTR lpszSection =	REG_PATH_PERFMON;
	LPCTSTR lpszEntry	=	REG_ENTRY_DISABLE_COUNT;
		
    //** Path to the value stored in the Registry
    if(!Registry.VerifyKey(HKEY_LOCAL_MACHINE, lpszSection))  
		return;

	//** open Registry value
	Registry.Open(HKEY_LOCAL_MACHINE, lpszSection);
	//** Read Path
	Registry.Write(lpszEntry,word);	
	//** Close Registry
	Registry.Close();
}

void CServiceControl::RestoreDiskRegistry()
{
	DWORD word = 0;
	CTGRegistry Registry;
	LPCTSTR lpszSection =	REG_PATH_PERFDISK;
	LPCTSTR lpszEntry	=	REG_ENTRY_DISABLE_COUNT;
		
    //** Path to the value stored in the Registry
    if(!Registry.VerifyKey(HKEY_LOCAL_MACHINE, lpszSection))  
		return;

	//** open Registry value
	Registry.Open(HKEY_LOCAL_MACHINE, lpszSection);
	//** Read Path
	Registry.Write(lpszEntry,word);	
	//** Close Registry
	Registry.Close();
}

void CServiceControl::RestoreW3SVCRegistry()
{
	DWORD word = 0;
	CTGRegistry Registry;
	LPCTSTR lpszSection =	REG_PATH_W3SVC;
	LPCTSTR lpszEntry	=	REG_ENTRY_DISABLE_COUNT;
		
    //** Path to the value stored in the Registry
    if(!Registry.VerifyKey(HKEY_LOCAL_MACHINE, lpszSection))  
		return;

	//** open Registry value
	Registry.Open(HKEY_LOCAL_MACHINE, lpszSection);
	//** Read Path
	Registry.Write(lpszEntry,word);	
	//** Close Registry
	Registry.Close();
}

