////////////////////////////////////////////////////////////////////////////////
//
//	ServiceControl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-06-25
//
////////////////////////////////////////////////////////////////////////////////

#pragma once


#include "SysRegKey.h"
#include "SysService.h"

#include <string.h>
#include <string>


#define REG_PATH_SERVICE		_T("SYSTEM\\CurrentControlSet\\Services")
#define REG_PATH_EVENTLOG		_T("SYSTEM\\CurrentControlSet\\Services\\Eventlog")
#define REG_PATH_PERFMON		_T("SYSTEM\\CurrentControlSet\\Services\\PerfProc\\Performance")
#define REG_PATH_PERFDISK		_T("SYSTEM\\CurrentControlSet\\Services\\PerfDisk\\Performance")
#define REG_PATH_W3SVC			_T("SYSTEM\\CurrentControlSet\\Services\\W3SVC\\Performance")

#define REG_ROOT_PATH_ENTRY		_T("Path")
#define REG_PATH_EVENT_MAXSIZE	_T("MaxSize")
#define REG_PATH_EVENT_FILE		_T("File")
#define REG_ENTRY_DISABLE_COUNT	_T("Disable Performance Counters")

class CServiceControl  
{
public:
	CServiceControl();
	virtual ~CServiceControl();

	CSysRegKey	*m_pRegKey;
	CSysService	*m_pSysService;

//** SERVICE
	SERVICE_STATUS m_ServiceStatus;

	SC_HANDLE m_schSCManager;
	SC_HANDLE m_myServiceHandle;

//** FUCNTIONS
	void CheckService(CString strTitle, CString& strDisplay, BOOL& bService, BOOL& bStatus);	

	BOOL IsExistService(CString strService, CString &strDisplay);
	UINT GetServiceHandle(CString strService);

	void ReleaseHandle();
	UINT GetStatusService();

	//** SERVICE REGIST
	BOOL OnInstall(CString strTitle, CString strDisplay, CString strFileName, BOOL bMsg = TRUE);
	BOOL OnUninstall(CString strTitle, BOOL bMsg = TRUE);
	void GetParams( LPDWORD dwDesiredAccess,LPDWORD dwServiceType, LPDWORD dwStartType, LPDWORD dwErrorControl);

	BOOL OnStart(CString strTitle, BOOL bMsg = TRUE);
	BOOL OnStop(CString strTitle,BOOL bMsg = TRUE);
	BOOL OnPause(CString strTitle);
	BOOL OnContinue(CString strTitle);

	
	//** Kill Process
	BOOL KillProcess(CString strProcess);

	//** GET PROCESS PID	
	#define TITLE_SIZE          64 
	#define PROCESS_SIZE        MAX_PATH 
	// task list structure 
	typedef struct _TASK_LIST { 
		DWORD       dwProcessId; 
		DWORD       dwInheritedFromProcessId; 
		BOOL        flags; 
		HANDLE      hwnd; 
		CHAR        ProcessName[PROCESS_SIZE]; 
		CHAR        WindowTitle[TITLE_SIZE]; 
	} TASK_LIST, *PTASK_LIST; 
	typedef struct _TASK_LIST_ENUM { 
		PTASK_LIST  tlist; 
		DWORD       numtasks; 
	} TASK_LIST_ENUM, *PTASK_LIST_ENUM; 
		
	DWORD GetTaskList(PTASK_LIST pTask, DWORD  dwNumTasks);
	BOOL KillProcess(PTASK_LIST tlist,BOOL fForce);
	BOOL EnableDebugPriv();

	//** 2003-08-04
	//** CHECK REGISTY HIDE PROCESS IN PERFMON
	void RestoreProcRegistry();

	//** 2003-09-23
	void RestoreDiskRegistry();

	//** 2003-10-10
	void RestoreW3SVCRegistry();
};

