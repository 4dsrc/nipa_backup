////////////////////////////////////////////////////////////////////////////////
//
//	ESMResizeControlBar.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-29
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMResizeControlBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/*
 * Constructor/Destructor
 */
CESMResizeControlBar::CESMResizeControlBar()
{
	m_szMax = CSize(-1,-1);
}

CESMResizeControlBar::~CESMResizeControlBar(void)
{
}

void CESMResizeControlBar::SetMinSize(CSize sz)
{
	m_szMin = sz;
}

void CESMResizeControlBar::SetMaxSize(CSize sz)
{
	m_szMax = sz;
}


void CESMResizeControlBar::_RowResizingUpdateState()
{
	CExtControlBar::_RowResizingUpdateState();	
}
