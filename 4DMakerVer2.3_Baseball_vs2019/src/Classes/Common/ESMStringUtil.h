////////////////////////////////////////////////////////////////////////////////
//
//	TGStringUtil.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once

#pragma warning(disable:4996)

class CTGStringUtil
{
	CTGStringUtil();
	~CTGStringUtil();

public:
	static CString MbsToWcs(IN const char* pmbstring);
	static BOOL WcsToMbs(IN CString pwcstring, OUT char* pmbstring);
};