#pragma once

#include "ESMIndex.h"

extern void ParentHWndReg(HWND hParentFrm);
extern void SendLog(int verbosity, LPCTSTR lpszFormat, ...);
extern BOOL ESMMovieMemcpy(void * _Dst, const void * _Src, size_t _Size);