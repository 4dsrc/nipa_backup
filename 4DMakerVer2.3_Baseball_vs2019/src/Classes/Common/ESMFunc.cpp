////////////////////////////////////////////////////////////////////////////////
//
//	ESMFunc.cpp : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-18
//
////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "ESMFunc.h"
#include "ESMIni.h"
#include "ESMProcessControl.h"
#include "MainFrm.h"
#include "DSCMgr.h"

#include <icmpapi.h>

#include "ESMFileOperation.h"

#include "winsock2.h"
#define DESIRED_WINSOCK_VERSION        0x0101
#define MINIMUM_WINSOCK_VERSION        0x0001

#include <Iphlpapi.h>
#pragma comment(lib, "Iphlpapi.lib")

CMainFrame* g_pESMMainWnd = NULL;
HANDLE g_hResultEvent = NULL;
int movie_frame_per_second = 30;
int movie_next_frame_time = 33;
//int movie_size = ESM_MOVIESIZE_FHD;

CMutex g_mutexFrame(FALSE, _T("FrameLock"));

//------------------------------------------------------------------------------ 
//! @brief    Print out logging data to a log file and "Output" view.
//! @date     2009-05-25
//! @owner    
//! @note
//! @return        
//------------------------------------------------------------------------------ 
void ESMLog(int verbosity, LPCTSTR lpszFormat, ...)
{	
	if(NULL == lpszFormat)	
		return;

	TCHAR pszMsg[ESMLOG_LINE_SIZE] = {0};

	va_list args;
	va_start(args, lpszFormat);
	_vstprintf(pszMsg, lpszFormat, args);
	va_end(args);

	int nLength = (int)_tcslen(pszMsg);
	if(!nLength)
		return;
	
	CString strMsg;
	
	
	if(g_pESMMainWnd)
	{		
		if(nLength > ESMLOG_LINE_SIZE)
			strMsg.Format(_T("[Too Long Message]"), pszMsg);
		else
			strMsg.Format(_T("[%d] %s"),GetTickCount(), pszMsg );
		g_pESMMainWnd->PrintLog(verbosity, strMsg);		
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		void ESMRegist()
//! @date		
//! @attention	none
//! @note	 	//-- 2009-05-25
//------------------------------------------------------------------------------ 
void ESMRegist()
{
	//-- GET MAINFRAME POINT
	g_pESMMainWnd = (CMainFrame*)AfxGetMainWnd();

	
}

HWND ESMGetMainWnd()
{
	//-- return MAINFRAME POINT
	return g_pESMMainWnd->GetSafeHwnd();
}

//------------------------------------------------------------------------------ 
//! @brief		ESMSleep()
//! @date		
//! @attention	none
//! @note	 	//-- 2009-05-25
//------------------------------------------------------------------------------ 
void ESMSleep()
{
	Sleep(1);
}


int ESMGetValue(int nType)
{
	int nValue = -1;
	switch(nType)
	{
	case ESM_VALUE_ADJ_FLOW_T			: nValue = g_pESMMainWnd->GetESMOption()->m_Adjust.nFlowmoTime;			break;
	case ESM_VALUE_ADJ_AUTOBRIGHTNESS	: nValue = g_pESMMainWnd->GetESMOption()->m_Adjust.nAutoBrightness;		break;
	case ESM_VALUE_ADJ_ORDER_RTL		: nValue = g_pESMMainWnd->GetESMOption()->m_Adjust.bClockwise;			break;		
	case ESM_VALUE_ADJ_ORDER_LTR		: nValue = g_pESMMainWnd->GetESMOption()->m_Adjust.bClockreverse;		break;
	case ESM_VALUE_MAKE_SERVER			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bMakeServer;			break;
	case ESM_VALUE_NET_MODE				: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bRCMode;				break;
	case ESM_VALUE_SERVERMODE			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nServerMode;			break;	
	case ESM_VALUE_NET_PORT				: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nPort;				break;
	case ESM_VALUE_WAIT_SAVE_DSC		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nWaitSaveDSC;			break;
	//jhhan
	case ESM_VALUE_WAIT_SAVE_DSC_SUB	: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nWaitSaveDSCSub;		break;
	case ESM_VALUE_WAIT_SAVE_DSC_SEL	: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nWaitSaveDSCSel;		break;
	case ESM_VALUE_VIEW_INFO			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bViewInfo;			break;
	case ESM_VALUE_TEST_MODE			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bTestMode;			break;
	case ESM_VALUE_MAKEAUTOPLAY			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bMakeAndAotuPlay;		break;
	case ESM_VALUE_INSERTLOGO			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bInsertLogo;			break;
	case ESM_VALUE_3DLOGO				: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.b3DLogo;				break;
	case ESM_VALUE_LOGOZOOM				: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bLogoZoom;			break;
	case ESM_VALUE_INSERT_CAMERAID		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bInsertCameraID;		break;
	case ESM_VALUE_TIMELINEPREVIEW		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bTimeLinePreview;		break;
	case ESM_VALUE_ENABLE_BASEBALL		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bEnableBaseBall;		break;
	case ESM_VALUE_REVERSE_MOVIE		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bReverseMovie;		break;
	case ESM_VALUE_GPU_MAKE_FILE		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bGPUMakeFile;			break;
	case ESM_VALUE_REPEAT_MOVIE			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bRepeatMovie;			break;
	case ESM_VALUE_ENABLE_VMCC			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bEnableVMCC;			break;
	case ESM_VALUE_ENABLE_4DPCOPY		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bEnableCopy;			break;
	case ESM_VALUE_BASEBALL_INNING		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nBaseBallInning;		break;
	case ESM_VALUE_UHD_TO_FHD			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bUHDtoFHD;			break;
	case ESM_VALUE_DELETE_COUNT			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nDeleteCnt;			break;
	case ESM_VALUE_MOVIE_REPEAT_COUNT	: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nRepeatMovieCnt;		break;
	case ESM_VALUE_DSC_SYNC_COUNT		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nDSCSyncCnt;			break;
	case ESM_VALUE_MOVIESAVELOCATION	: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nMovieSaveLocation;	break;
	case ESM_VALUE_SYNC_TIME_MARGIN		: 
		nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nSyncTimeMargin;		
		break;
	case ESM_VALUE_SENSORONTIME			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nSensorOnTime;		break;
	case ESM_VALUE_TEMPLATE_STARTTIME	: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nTemplateStartMargin; break;
	case ESM_VALUE_TEMPLATE_ENDTIME		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nTemplateEndMargin;	break;
	case ESM_VALUE_TEMPLATE_STARTSLEEP	: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nTemplateStartSleep;	break;
	case ESM_VALUE_TEMPLATE_ENDSLEEP	: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nTemplateEndSleep	;	break;			
	case ESM_VALUE_ADJ_CAMERAZOOM		: nValue = g_pESMMainWnd->GetESMOption()->m_Adjust.nCamZoom;			break;
	//-- 2015-02-15 cygil@esmlab.com
	//-- Add picture coutdown
	case ESM_VALUE_COUNTDOWNLASTIP		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nCountdownLastIP;		break;

	case ESM_VALUE_ADJ_TH				: nValue = g_pESMMainWnd->GetESMOption()->m_Adjust.nThresdhold;			break;
	case ESM_VALUE_ADJ_POINTGAPMIN		: nValue = g_pESMMainWnd->GetESMOption()->m_Adjust.nPointGapMin;		break;
	case ESM_VALUE_ADJ_MIN_WIDTH		: nValue = g_pESMMainWnd->GetESMOption()->m_Adjust.nMinWidth;			break;
	case ESM_VALUE_ADJ_MIN_HEIGHT		: nValue = g_pESMMainWnd->GetESMOption()->m_Adjust.nMinHeight;			break;
	case ESM_VALUE_ADJ_MAX_WIDTH		: nValue = g_pESMMainWnd->GetESMOption()->m_Adjust.nMaxWidth;			break;
	case ESM_VALUE_ADJ_MAX_HEIGHT		: nValue = g_pESMMainWnd->GetESMOption()->m_Adjust.nMaxHeight;			break;
	case ESM_VALUE_ADJ_TARGETLENTH		: nValue = g_pESMMainWnd->GetESMOption()->m_Adjust.nTargetLenth;		break;

	//Ceremony
	case ESM_VALUE_CEREMONYUSE			: nValue = g_pESMMainWnd->GetESMOption()->m_Ceremony.bCeremonyUse;		break;
	case ESM_VALUE_CEREMONYPUTIMAGE		: nValue = g_pESMMainWnd->GetESMOption()->m_Ceremony.bPutImage;			break;
	case ESM_VALUE_CEREMONYRATIO		: nValue = g_pESMMainWnd->GetESMOption()->m_Ceremony.bCeremonyRatio;	break;
	case ESM_VALUE_CEREMONY_STILLIMAGE	: nValue = g_pESMMainWnd->GetESMOption()->m_Ceremony.bStillImage;		break;
	case ESM_VALUE_CEREMONY_USE_CANON_SELPHY	: nValue = g_pESMMainWnd->GetESMOption()->m_Ceremony.bUseCanonSelphy;		break;
		
	//Save Image
	case ESM_VALUE_SAVE_IMAGE			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bSaveImg;				break;
	//Load Profile Direct
	case ESM_VALUE_LOADFILE_DIRECT		: nValue = g_pESMMainWnd->GetESMOption()->m_bDirectLoadProfile;			break;

	case ESM_VALUE_CHECK_VALID			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bCheckValid;			break;
	case ESM_VALUE_INSERTPRISM			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bInsertPrism;			break;
	case ESM_VALUE_SYNCSKIP				: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bSyncSkip;			break;
	case ESM_VALUE_PRISMVALUE			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nPrismValue;			break;
	case ESM_VALUE_PRISMVALUE2			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nPrismValue2;			break;	
	case ESM_VALUE_COLORREVISION		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bColorRevision;		break;
	case ESM_VALUE_TEMPLATEPOINT		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bTemplatePoint;		break;
	case ESM_VALUE_TEMPLATE_STABILIZATION : nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bTemplateStabilization;		break;
	case ESM_VALUE_TEMPLATE_MODIFY		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bTemplateModify;		break;
	case ESM_VALUE_AUTO_WHITEBALANCE    : nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bInsertWhiteBalance;  break;
	case ESM_VALUE_AJALOGO				: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bAJALogo;				break;
	case ESM_VALUE_AJAREPLAY			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bAJAReplay;			break;
	case ESM_VALUE_AJAONOFF				: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bAJAOnOff;			break;
	case ESM_VALUE_AJASAMPLECNT			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nAJASapleCnt;			break;
	case ESM_VALUE_GETRAMDISKSIZE		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bGetRamDiskSize;		break;
	//jhhan 170327
	case ESM_VALUE_CHECK_VALID_TIME		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nCheckVaildTime;		break;
	case ESM_VALUE_PRINT_COLORINFO      : nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bPrnColorInfo;		break;
	case ESM_VALUE_DIV_FRAME			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nDivFrame;			break;

	//joonho.kim 170628
	case ESM_VALUE_PC_SYNC_TIME			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nPCSyncTime;			break;
	case ESM_VALUE_DSC_SYNC_TIME		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nDSCSyncTime;			break;
	case ESM_VALUE_DSC_WAIT_TIME		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nDSCSyncWaitTime;		break;

	//hjcho 170706
	case ESM_VALUE_GPUMAKESKIP			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bGPUSkip;				break;

	//wgkim 170727
	case ESM_VALUE_LIGHTWEIGHT			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bLightWeight;			break;
	case ESM_VALUE_AUTODETECT			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bAutoDetect;			break;

	case ESM_VALUE_DIRECTMUX			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bDirectMux;			break;
	case ESM_VALUE_4DPMETHOD			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.n4DPMethod;			break;
	case ESM_VALUE_HORIZON				: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bHorizonAdj;			break;

	//wgkim 171127
	case ESM_VALUE_ENCODING_FOR_SMARTPHONES: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bEncodingForSmartPhones;		break;
	case ESM_VALUE_VIEW_RATIO			: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nFrameZoomRatio;					break;

	//wgkim 180119
	case ESM_VALUE_TEMPLATE_TARGETOUT_PERCENT : nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nTemplateTargetOutPercent;	break;
	case ESM_VALUE_RECORD_FILE_SPLIT	: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bRecordFileSplit;		break;

	//hjcho 180508
	case ESM_VALUE_AJANETWORK: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bAJANetwork; break;

	//hjcho 180629
	case ESM_VALUE_GIF_MAKING: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bGIFMaking; break;
	case ESM_VALUE_GIF_QUALITY: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nGIFQuality; break;
	case ESM_VALUE_GIF_SIZE: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nGIFSize; break;

	case ESM_VALUE_FRAMERATE: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nSetFrameRateIndex;			break;
	case ESM_VALUE_REFEREEREAD: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bRefereeRead; break;
	case ESM_VALUE_USE_RESYNC: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bUseReSync; break;

	case ESM_VALUE_RESYNC_WAIT_TIME		: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nResyncRecWaitTime;		break;

	//jhhan 180828
	case ESM_VALUE_AUTO_DELETE: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bAutoDelete; break;
	case ESM_VALUE_STORED_TIME: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nStoredTime; break; 
	case ESM_VALUE_TEMPLATE_EDITOR: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bTemplateEditor; break;

	//wgkim 180921
	case ESM_VALUE_AUTOADJUST_POSITION_TRACKING: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bAutoAdjustPositionTracking; break;
	case ESM_VALUE_AUTOADJUST_KZONE: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bAutoAdjustKZone; break;
	
	//hjcho 180927 
	case ESM_VALUE_RTSP: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.bRTSP; break;
	case ESM_VALUE_4DAP: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.b4DAP; break;

	//wgkim 181119
	case ESM_VALUE_TEMPLATE_MODIFY_MODE: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nSetTemplateModifyMode;	break;

	case ESM_VALUE_BITRATE_FOR_SMARTPHONE: nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nSmartPhoneBitrate;	break;

	case ESM_VALUE_RTSP_WAITTIME:	nValue = g_pESMMainWnd->GetESMOption()->m_4DOpt.nRTSPWaitTime; break;
	}

	return nValue;
}

void ESMSetValue(int nType, int nValue)
{
	switch(nType)
	{
	case ESM_VALUE_ADJ_FLOW_T			: g_pESMMainWnd->GetESMOption()->m_Adjust.nFlowmoTime			= nValue;	break;
	case ESM_VALUE_ADJ_AUTOBRIGHTNESS	: g_pESMMainWnd->GetESMOption()->m_Adjust.nAutoBrightness		= nValue;	break;

	case ESM_VALUE_ADJ_ORDER_RTL		: g_pESMMainWnd->GetESMOption()->m_Adjust.bClockwise			= nValue;	break;		
	case ESM_VALUE_ADJ_ORDER_LTR		: g_pESMMainWnd->GetESMOption()->m_Adjust.bClockreverse			= nValue;	break;
	case ESM_VALUE_MAKE_SERVER			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bMakeServer			= nValue;	break;
	case ESM_VALUE_NET_MODE				: g_pESMMainWnd->GetESMOption()->m_4DOpt.bRCMode				= nValue;	break;
	case ESM_VALUE_SERVERMODE			: g_pESMMainWnd->GetESMOption()->m_4DOpt.nServerMode			= nValue;	break;
	case ESM_VALUE_NET_PORT				: g_pESMMainWnd->GetESMOption()->m_4DOpt.nPort					= nValue;	break;
	case ESM_VALUE_WAIT_SAVE_DSC		: g_pESMMainWnd->GetESMOption()->m_4DOpt.nWaitSaveDSC			= nValue;	break;
	//jhhan
	case ESM_VALUE_WAIT_SAVE_DSC_SUB	: g_pESMMainWnd->GetESMOption()->m_4DOpt.nWaitSaveDSCSub		= nValue;	break;
	case ESM_VALUE_WAIT_SAVE_DSC_SEL	: g_pESMMainWnd->GetESMOption()->m_4DOpt.nWaitSaveDSCSel		= nValue;	break;
	case ESM_VALUE_VIEW_INFO			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bViewInfo				= nValue;	break;
	case ESM_VALUE_TEST_MODE			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bTestMode				= nValue;	break;
	case ESM_VALUE_MAKEAUTOPLAY			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bMakeAndAotuPlay		= nValue;	break;
	case ESM_VALUE_INSERTLOGO			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bInsertLogo			= nValue;	break;
	case ESM_VALUE_3DLOGO				: g_pESMMainWnd->GetESMOption()->m_4DOpt.b3DLogo				= nValue;	break;
	case ESM_VALUE_LOGOZOOM				: g_pESMMainWnd->GetESMOption()->m_4DOpt.bLogoZoom				= nValue;	break;
	case ESM_VALUE_INSERT_CAMERAID		: g_pESMMainWnd->GetESMOption()->m_4DOpt.bInsertCameraID		= nValue;	break;
	case ESM_VALUE_TIMELINEPREVIEW		: g_pESMMainWnd->GetESMOption()->m_4DOpt.bTimeLinePreview		= nValue;	break;
	case ESM_VALUE_ENABLE_BASEBALL		: g_pESMMainWnd->GetESMOption()->m_4DOpt.bEnableBaseBall		= nValue;	break;
	case ESM_VALUE_REVERSE_MOVIE		: g_pESMMainWnd->GetESMOption()->m_4DOpt.bReverseMovie			= nValue;	break;
	case ESM_VALUE_GPU_MAKE_FILE		: g_pESMMainWnd->GetESMOption()->m_4DOpt.bGPUMakeFile			= nValue;	break;
	case ESM_VALUE_REPEAT_MOVIE			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bRepeatMovie			= nValue;	break;
	case ESM_VALUE_ENABLE_VMCC			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bEnableVMCC			= nValue;	break;
	case ESM_VALUE_ENABLE_4DPCOPY		: g_pESMMainWnd->GetESMOption()->m_4DOpt.bEnableCopy			= nValue;	break;
	case ESM_VALUE_BASEBALL_INNING		: g_pESMMainWnd->GetESMOption()->m_4DOpt.nBaseBallInning		= nValue;	break;
	case ESM_VALUE_DELETE_COUNT			: g_pESMMainWnd->GetESMOption()->m_4DOpt.nDeleteCnt				= nValue;	break;
	case ESM_VALUE_MOVIE_REPEAT_COUNT	: g_pESMMainWnd->GetESMOption()->m_4DOpt.nRepeatMovieCnt		= nValue;	break;
	case ESM_VALUE_DSC_SYNC_COUNT		: g_pESMMainWnd->GetESMOption()->m_4DOpt.nDSCSyncCnt			= nValue;	break;
	case ESM_VALUE_UHD_TO_FHD			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bUHDtoFHD				= nValue;	break;
	case ESM_VALUE_MOVIESAVELOCATION	: g_pESMMainWnd->GetESMOption()->m_4DOpt.nMovieSaveLocation		= nValue;	break;	
	case ESM_VALUE_SYNC_TIME_MARGIN		: g_pESMMainWnd->GetESMOption()->m_4DOpt.nSyncTimeMargin		= nValue;	break;
	case ESM_VALUE_SENSORONTIME			: g_pESMMainWnd->GetESMOption()->m_4DOpt.nSensorOnTime			= nValue;	break;
	case ESM_VALUE_TEMPLATE_STARTTIME	: g_pESMMainWnd->GetESMOption()->m_4DOpt.nTemplateStartMargin	= nValue;	break;
	case ESM_VALUE_TEMPLATE_ENDTIME		: g_pESMMainWnd->GetESMOption()->m_4DOpt.nTemplateEndMargin		= nValue;	break;
	case ESM_VALUE_TEMPLATE_STARTSLEEP	: g_pESMMainWnd->GetESMOption()->m_4DOpt.nTemplateStartSleep	= nValue;	break;
	case ESM_VALUE_TEMPLATE_ENDSLEEP	: g_pESMMainWnd->GetESMOption()->m_4DOpt.nTemplateEndSleep		= nValue;	break;		
	case ESM_VALUE_ADJ_CAMERAZOOM		: g_pESMMainWnd->GetESMOption()->m_Adjust.nCamZoom				= nValue;	break;
	//-- 2015-02-15 cygil@esmlab.com
	//-- Add picture coutdown
	case ESM_VALUE_COUNTDOWNLASTIP		: g_pESMMainWnd->GetESMOption()->m_4DOpt.nCountdownLastIP		= nValue;	break;

	case ESM_VALUE_ADJ_TH				: g_pESMMainWnd->GetESMOption()->m_Adjust.nThresdhold			= nValue;	break;
	case ESM_VALUE_ADJ_POINTGAPMIN		: g_pESMMainWnd->GetESMOption()->m_Adjust.nPointGapMin			= nValue;	break;
	case ESM_VALUE_ADJ_MIN_WIDTH		: g_pESMMainWnd->GetESMOption()->m_Adjust.nMinWidth				= nValue;	break;
	case ESM_VALUE_ADJ_MIN_HEIGHT		: g_pESMMainWnd->GetESMOption()->m_Adjust.nMinHeight			= nValue;	break;
	case ESM_VALUE_ADJ_MAX_WIDTH		: g_pESMMainWnd->GetESMOption()->m_Adjust.nMaxWidth				= nValue;	break;
	case ESM_VALUE_ADJ_MAX_HEIGHT		: g_pESMMainWnd->GetESMOption()->m_Adjust.nMaxHeight			= nValue;	break;
	case ESM_VALUE_ADJ_TARGETLENTH		: g_pESMMainWnd->GetESMOption()->m_Adjust.nTargetLenth			= nValue;	break;

	//Ceremony
	case ESM_VALUE_CEREMONYUSE			: g_pESMMainWnd->GetESMOption()->m_Ceremony.bCeremonyUse		= nValue;	break;
	case ESM_VALUE_CEREMONYPUTIMAGE		: g_pESMMainWnd->GetESMOption()->m_Ceremony.bPutImage			= nValue;	break;
	case ESM_VALUE_CEREMONYRATIO		: g_pESMMainWnd->GetESMOption()->m_Ceremony.bCeremonyRatio		= nValue;	break;
	case ESM_VALUE_CEREMONY_STILLIMAGE	: g_pESMMainWnd->GetESMOption()->m_Ceremony.bStillImage			= nValue;	break;
	case ESM_VALUE_CEREMONY_USE_CANON_SELPHY	: g_pESMMainWnd->GetESMOption()->m_Ceremony.bUseCanonSelphy			= nValue;	break;

	//Save Image
	case ESM_VALUE_SAVE_IMAGE			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bSaveImg				= nValue;	break;
	//Load Profile Direct
	case ESM_VALUE_LOADFILE_DIRECT		: g_pESMMainWnd->GetESMOption()->m_bDirectLoadProfile			= nValue;	break;
	case ESM_VALUE_CHECK_VALID			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bCheckValid			= nValue;	break;
	case ESM_VALUE_INSERTPRISM			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bInsertPrism			= nValue;	break;
	case ESM_VALUE_SYNCSKIP				: g_pESMMainWnd->GetESMOption()->m_4DOpt.bSyncSkip				= nValue;	break;
	case ESM_VALUE_PRISMVALUE			: g_pESMMainWnd->GetESMOption()->m_4DOpt.nPrismValue			= nValue;	break;
	case ESM_VALUE_PRISMVALUE2			: g_pESMMainWnd->GetESMOption()->m_4DOpt.nPrismValue2			= nValue;	break;	
	case ESM_VALUE_COLORREVISION		: g_pESMMainWnd->GetESMOption()->m_4DOpt.bColorRevision			= nValue;	break;
	case ESM_VALUE_TEMPLATEPOINT		: g_pESMMainWnd->GetESMOption()->m_4DOpt.bTemplatePoint			= nValue;	break;
	case ESM_VALUE_TEMPLATE_STABILIZATION: g_pESMMainWnd->GetESMOption()->m_4DOpt.bTemplateStabilization= nValue;	break;
	case ESM_VALUE_TEMPLATE_MODIFY		: g_pESMMainWnd->GetESMOption()->m_4DOpt.bTemplateModify		= nValue;	break;
	case ESM_VALUE_AUTO_WHITEBALANCE    : g_pESMMainWnd->GetESMOption()->m_4DOpt.bInsertWhiteBalance    = nValue;	break;
	case ESM_VALUE_AJALOGO			    : g_pESMMainWnd->GetESMOption()->m_4DOpt.bAJALogo			    = nValue;	break;
	case ESM_VALUE_AJAREPLAY		    : g_pESMMainWnd->GetESMOption()->m_4DOpt.bAJAReplay			    = nValue;	break;
	case ESM_VALUE_AJAONOFF				: g_pESMMainWnd->GetESMOption()->m_4DOpt.bAJAOnOff			    = nValue;	break;
	case ESM_VALUE_AJASAMPLECNT			: g_pESMMainWnd->GetESMOption()->m_4DOpt.nAJASapleCnt			= nValue;	break;
	case ESM_VALUE_GETRAMDISKSIZE		: g_pESMMainWnd->GetESMOption()->m_4DOpt.bGetRamDiskSize		= nValue;	break;
	case ESM_VALUE_PRINT_COLORINFO      : g_pESMMainWnd->GetESMOption()->m_4DOpt.bPrnColorInfo			= nValue;	break;
	case ESM_VALUE_DIV_FRAME			: g_pESMMainWnd->GetESMOption()->m_4DOpt.nDivFrame				= nValue;	break;

		//joonho.kim 170628
	case ESM_VALUE_PC_SYNC_TIME			: g_pESMMainWnd->GetESMOption()->m_4DOpt.nPCSyncTime			= nValue;	break;
	case ESM_VALUE_DSC_SYNC_TIME		: g_pESMMainWnd->GetESMOption()->m_4DOpt.nDSCSyncTime			= nValue;	break;
	case ESM_VALUE_DSC_WAIT_TIME		: g_pESMMainWnd->GetESMOption()->m_4DOpt.nDSCSyncWaitTime		= nValue;	break;
	case ESM_VALUE_GPUMAKESKIP			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bGPUSkip				= nValue;   break;

	//wgkim 170727
	case ESM_VALUE_LIGHTWEIGHT			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bLightWeight			= nValue;	break;
	case ESM_VALUE_AUTODETECT			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bAutoDetect			= nValue;	break;

	//hjcho 170830
	case ESM_VALUE_DIRECTMUX			: g_pESMMainWnd->GetESMOption()->m_4DOpt.bDirectMux				= nValue;	break;
	case ESM_VALUE_4DPMETHOD			: g_pESMMainWnd->GetESMOption()->m_4DOpt.n4DPMethod				= nValue;	break;
	case ESM_VALUE_HORIZON				: g_pESMMainWnd->GetESMOption()->m_4DOpt.bHorizonAdj			= nValue;	break;

	//wgkim 170727
	case ESM_VALUE_ENCODING_FOR_SMARTPHONES	: g_pESMMainWnd->GetESMOption()->m_4DOpt.bEncodingForSmartPhones = nValue;	break;
	case ESM_VALUE_RECORD_FILE_SPLIT	: g_pESMMainWnd->GetESMOption()->m_4DOpt.bRecordFileSplit		= nValue;	break;
	
	//hjcho 171210
	case ESM_VALUE_VIEW_RATIO				: g_pESMMainWnd->GetESMOption()->m_4DOpt.nFrameZoomRatio = nValue;				break;

	//wgkim 180119
	case ESM_VALUE_TEMPLATE_TARGETOUT_PERCENT : g_pESMMainWnd->GetESMOption()->m_4DOpt.nTemplateTargetOutPercent = nValue; break;

	//hjcho 180508
	case ESM_VALUE_AJANETWORK: g_pESMMainWnd->GetESMOption()->m_4DOpt.bAJANetwork = nValue; break;
	//hjcho 180629
	case ESM_VALUE_GIF_MAKING: g_pESMMainWnd->GetESMOption()->m_4DOpt.bGIFMaking = nValue; break;
	case ESM_VALUE_GIF_QUALITY: g_pESMMainWnd->GetESMOption()->m_4DOpt.nGIFQuality = nValue; break;
	case ESM_VALUE_GIF_SIZE: g_pESMMainWnd->GetESMOption()->m_4DOpt.nGIFSize = nValue; break;

	//wgkim 180629
	case ESM_VALUE_FRAMERATE: g_pESMMainWnd->GetESMOption()->m_4DOpt.nSetFrameRateIndex = nValue;	break;
	case ESM_VALUE_REFEREEREAD: g_pESMMainWnd->GetESMOption()->m_4DOpt.bRefereeRead = nValue; break;
	case ESM_VALUE_USE_RESYNC: g_pESMMainWnd->GetESMOption()->m_4DOpt.bUseReSync = nValue; break;

	case ESM_VALUE_RESYNC_WAIT_TIME		: g_pESMMainWnd->GetESMOption()->m_4DOpt.nResyncRecWaitTime		= nValue;	break;

	//jhhan 180828
	case ESM_VALUE_AUTO_DELETE: g_pESMMainWnd->GetESMOption()->m_4DOpt.bAutoDelete = nValue; break;
	case ESM_VALUE_STORED_TIME: g_pESMMainWnd->GetESMOption()->m_4DOpt.nStoredTime = nValue; break;	
	//jhhan 180918
	case ESM_VALUE_TEMPLATE_EDITOR: g_pESMMainWnd->GetESMOption()->m_4DOpt.bTemplateEditor = nValue; break;

	//wgkim 180921
	case ESM_VALUE_AUTOADJUST_POSITION_TRACKING : g_pESMMainWnd->GetESMOption()->m_4DOpt.bAutoAdjustPositionTracking = nValue;	break;
	case ESM_VALUE_AUTOADJUST_KZONE : g_pESMMainWnd->GetESMOption()->m_4DOpt.bAutoAdjustKZone = nValue;	break;
	
	//hjcho 180927
	case ESM_VALUE_RTSP: g_pESMMainWnd->GetESMOption()->m_4DOpt.bRTSP = nValue; break;
	case ESM_VALUE_4DAP: g_pESMMainWnd->GetESMOption()->m_4DOpt.b4DAP = nValue; break;

	//wgkim 181119
	case ESM_VALUE_TEMPLATE_MODIFY_MODE: g_pESMMainWnd->GetESMOption()->m_4DOpt.nSetTemplateModifyMode = nValue;	break;
	
		//hjcho 190103
	case ESM_VALUE_BITRATE_FOR_SMARTPHONE: g_pESMMainWnd->GetESMOption()->m_4DOpt.nSmartPhoneBitrate = nValue; break;
	case ESM_VALUE_RTSP_WAITTIME: g_pESMMainWnd->GetESMOption()->m_4DOpt.nRTSPWaitTime = nValue; break;
	}

	g_pESMMainWnd->m_wndDashBoard.LoadInfo();
	return ;
}

int	ESMGetRecTime(BOOL bInit)
{
	int nTotalRecTime = 0;

	if(bInit)
		nTotalRecTime = 100000;
	else
		nTotalRecTime = g_pESMMainWnd->m_wndDSCViewer.m_pFrameSelector->GetRecTime();
	
	return nTotalRecTime;
}

void ESMSetRecTime(int nRunningTime)
{
	g_pESMMainWnd->m_wndDSCViewer.m_pFrameSelector->SetRecTime(nRunningTime);
}

int	ESMGetDSCCount()
{
	int nTotalDSCCount = g_pESMMainWnd->m_wndDSCViewer.GetItemCount();
	return nTotalDSCCount;
}

BOOL ESMGetExecuteMode()
{
	return g_pESMMainWnd->m_wndDSCViewer.GetExecuteMovieMode();
}

void ESMSetExecuteMode(BOOL bExecuteMovieMode)
{
	return g_pESMMainWnd->m_wndDSCViewer.SetExecuteMovieMode(bExecuteMovieMode);
}

float ESMGetFValue(int nType)
{
	float nValue = -1;
	switch(nType)
	{
	case ESM_VALUE_ADJ_VIEW_LEFT	: nValue = g_pESMMainWnd->m_wndDSCViewer.m_nAdjPosition[DSC_ADJ_VAL_LEFT		]; break;
	case ESM_VALUE_ADJ_VIEW_TOP		: nValue = g_pESMMainWnd->m_wndDSCViewer.m_nAdjPosition[DSC_ADJ_VAL_TOP		]; break;
	case ESM_VALUE_ADJ_VIEW_RIGHT	: nValue = g_pESMMainWnd->m_wndDSCViewer.m_nAdjPosition[DSC_ADJ_VAL_RIGHT		]; break;
	case ESM_VALUE_ADJ_VIEW_BOTTOM	: nValue = g_pESMMainWnd->m_wndDSCViewer.m_nAdjPosition[DSC_ADJ_VAL_BOTTOM		]; break;
	case ESM_VALUE_ADJ_VIEW_MA		: nValue = g_pESMMainWnd->m_wndDSCViewer.m_nAdjPosition[DSC_ADJ_VAL_MAXANGLE	]; break;
	}
	return nValue;
}

CString ESMGetFrameRecord()
{
	CString strRecord;
	if(g_pESMMainWnd->m_FileMgr == NULL)
		return _T("");
	
	g_mutexFrame.Lock();
	strRecord = g_pESMMainWnd->m_FileMgr->m_strRecordProfile;
	g_mutexFrame.Unlock();
	return strRecord;
}

void ESMSetFrameRecord(CString strRecord)
{
	g_mutexFrame.Lock();
	g_pESMMainWnd->m_FileMgr->m_strRecordProfile = strRecord;
	g_mutexFrame.Unlock();
}	

//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가
CBitmap* ESMGetImageFromImageLoader(CString strDSC, int nFIdx, CDC* pDC, int nWidth, int nHeight)
{
	return g_pESMMainWnd->m_pImgLoader->GetImageFromList(strDSC, nFIdx, pDC, nWidth, nHeight);
}

BOOL ESMIsImageFromImageLoader(CString strDSC, int nFIdx)
{
	return g_pESMMainWnd->m_pImgLoader->IsImageFromList(strDSC, nFIdx);
}

CString ESMGetMiddleDSCSearch(int nOrderDirection)
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	if( arDSCList.GetCount() <= 0)
		return _T("");

	int nMiddleDsc = -1;
	if( arDSCList.GetCount() < 3)
		nMiddleDsc = 1;
	else
	{
		if(nOrderDirection == DSC_REC_DELAY_FORWARDDIRECTION)
			nMiddleDsc = arDSCList.GetCount() * 2 / 3;
		else
			nMiddleDsc = arDSCList.GetCount() / 3;

		if(ESMGetValue(ESM_VALUE_CEREMONYUSE))
			nMiddleDsc = arDSCList.GetCount() / 2;
	}
	CDSCItem* pItem = (CDSCItem*)arDSCList.GetAt(nMiddleDsc - 1);

	return pItem->GetDeviceDSCID();
}

//------------------------------------------------------------------------------ 
//! @brief		GetImageFileType(int nType)
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
CString GetImageFileType(int nType)
{
	CString strExt;
	switch(nType)
	{
	case IMG_TYPE_BMP:	strExt = _T("bmp"); break;
	case IMG_TYPE_JPG:	strExt = _T("jpg"); break;
	}
	return strExt;
}

//------------------------------------------------------------------------------ 
//! @brief		GetImageFileType(int nType)
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void ReloadProperty(int nRow, BOOL bReload )
{
	/*
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_ESM_PROPERTY_SET_STEP;
	pMsg->nParam1 = nRow;
	pMsg->nParam2 = bReload;
	::SendMessage(g_pESMMainWnd->m_hWnd, WM_ESM, (WPARAM)WM_ESM_PROPERTY, (LPARAM)pMsg);	
	*/
}

//------------------------------------------------------------------------------ 
//! @brief		ESMWaitCursor(BOOL bStart)
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void PreparePath(CString &sPath)
{
	if(sPath.Right(1) != "\\") sPath += "\\";
}

#define PATH_ERROR			-1
#define PATH_NOT_FOUND		0
#define PATH_IS_FILE		1
#define PATH_IS_FOLDER		2

//------------------------------------------------------------------------------ 
//! @brief		CheckPath(BOOL bStart)
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
int CheckPath(CString sPath)
{
	DWORD dwAttr = GetFileAttributes(sPath);
	if (dwAttr == 0xffffffff) 
	{
		if (GetLastError() == ERROR_FILE_NOT_FOUND || GetLastError() == ERROR_PATH_NOT_FOUND) 
			return PATH_NOT_FOUND;
		return PATH_ERROR;
	}
	if (dwAttr & FILE_ATTRIBUTE_DIRECTORY) return PATH_IS_FOLDER;
	return PATH_IS_FILE;
}

//------------------------------------------------------------------------------ 
//! @brief		DeleteFiles(BOOL bStart)
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void DeleteFiles(CString strFile)
{
	CFileFind ff;
	CString sPath = strFile;

	if (CheckPath(sPath) == PATH_IS_FILE)
	{
		::DeleteFile(sPath);
		//ESMLog(4,_T("[ESM] Delete File : %s"),sPath);
		return;
	}

	PreparePath(sPath);
	sPath += "*.*";

	BOOL bRes = ff.FindFile(sPath);
	while(bRes)
	{
		bRes = ff.FindNextFile();
		if (ff.IsDots()) continue;
		if (ff.IsDirectory())
		{
			sPath = ff.GetFilePath();
			DeleteFiles(sPath);
		}
		else DeleteFiles(ff.GetFilePath());
	}
	ff.Close();
	::RemoveDirectory(strFile);
}

//------------------------------------------------------------------------------ 
//! @brief		Load Mode File
//! @date		IsExistProcess	
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
BOOL IsExistProcess(CString strProc)
{
	CESMProcessControl pc;
	return pc.IsExist (strProc);
}


//------------------------------------------------------------------------------ 
//! @brief	   Check whether the target is connected or not.	
//! @date		
//! @attention	
//! @note	 	
//------------------------------------------------------------------------------ 
BOOL ESMTargetConnect()
{
	return FALSE;//g_pESMMainWnd->IsConnected();
}


//---------------------------------------------------------
//! @brief		RemoveSlash
//! @attention	none
//! @note	 	
//---------------------------------------------------------
CString RemoveSlash(LPCTSTR Path)
{
	CString cs = Path;
	::PathRemoveBackslash(cs.GetBuffer(_MAX_PATH));
	cs.ReleaseBuffer(_MAX_PATH);
	return cs;
}

//---------------------------------------------------------
//! @brief		FileExists
//! @attention	none
//! @note	 	
//---------------------------------------------------------
BOOL FileExists(LPCTSTR Path)
{
	return (::PathFileExists(Path));
}

//------------------------------------------------------------------------------ 
//! @brief    If one or more of the intermediate folders do not exist, they are created
//!           as well.
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
void ESMCreateAllDirectories(CString csPath)
{
	if(csPath.IsEmpty())
		return;

	//Remove ending / if exists
	RemoveSlash(csPath);

	//If this folder already exists no need to create it
	if(FileExists(csPath))
		return;

	//Recursive call, one fewer folders
	int nFound = csPath.ReverseFind(_T('\\'));
	ESMCreateAllDirectories(csPath.Left(nFound));

	//Actually create a folder
	CreateDirectory(csPath, NULL);
	ESMLog(5,_T("Create Directory::%s"),csPath);
	TRACE(_T("Create Directory::%s\n"),csPath);
}


CString ESMGetTime()
{
	CString strTime = _T("");

	CTime time = CTime::GetCurrentTime();
	//time = CTime::GetCurrentTime();
	int nHour = time.GetHour();
	int nMinute = time.GetMinute();
	int nSecond = time.GetSecond();
	strTime.Format(_T("%d_%02d_%02d"), nHour, nMinute, nSecond);

	return strTime;
}

CString ESMGetDate()
{
	CString strDate = _T("");

	CTime time = CTime::GetCurrentTime();
	//time = CTime::GetCurrentTime();
	int nYear = time.GetYear();
	int nMonth = time.GetMonth();
	int nDay = time.GetDay();

    strDate.Format(_T("%04d-%02d-%02d"), nYear, nMonth, nDay);
	return strDate;
}
CString ESMGetCurTime()
{
	CString strDate = _T("");

	//time = CTime::GetCurrentTime();
	//int nMil   = time.
	SYSTEMTIME st;
	//GetSystemTime(&st);
	GetLocalTime(&st);
	int nYear = st.wYear;
	int nMonth = st.wMonth;
	int nDay = st.wDay;
	int nHour = st.wHour;
	int nMin  = st.wMinute;
	int nSec = st.wSecond;
	int nMilli = st.wMilliseconds;
	strDate.Format(_T("%02d%02d_%02d%02d%02d_%03d"), nMonth, nDay,nHour,nMin,nSec,nMilli);
	return strDate;


}
BOOL RegDelValue(HKEY hKey, LPCTSTR lpKey, LPCTSTR lpValue)
{

	HKEY hOpenKey;
	LONG lRet = 0;

	if(RegOpenKey(hKey, lpKey, &hOpenKey) != ERROR_SUCCESS)
		return FALSE;
	lRet = RegDeleteValue(hOpenKey, lpValue);
	RegCloseKey(hOpenKey);

	return ((lRet == ERROR_SUCCESS)? TRUE : FALSE);
}

//-- Copy ClipBoard
BOOL ClipCopy(CString strText)
{
	char pchText[16384] = {0};
	wcstombs(pchText, (LPCTSTR)strText, _tcslen((LPCTSTR)strText));

	HGLOBAL hglbCopy;
	char* lptstrCopy;

	if(!OpenClipboard(AfxGetMainWnd()->GetSafeHwnd()))
		return FALSE;

	EmptyClipboard();

	hglbCopy = GlobalAlloc(GMEM_MOVEABLE, strlen(pchText)+1);

	if(hglbCopy == NULL)
	{
		CloseClipboard();
		return FALSE;
	}

	lptstrCopy = (char*)GlobalLock(hglbCopy);
	memcpy(lptstrCopy, pchText, strlen(pchText) + 1);
	GlobalUnlock(hglbCopy);

	SetClipboardData(CF_TEXT, hglbCopy);

	CloseClipboard();

	return TRUE;
}

CString ESMGetSystemErrorString(DWORD nErroCode)
{
	CString strError = _T("");
	switch(nErroCode)
	{
	case ERROR_FILE_NOT_FOUND: //0x0002
		strError = _T("ERROR_FILE_NOT_FOUND"); break;
	case ERROR_ACCESS_DENIED:  //0x0005
		strError = _T("ERROR_ACCESS_DENIED");	 break;
	default:
		strError = _T("UNKNOWN");
	}

	CString strResult;
	strResult.Format(_T("%s(0x%04X)"), strError, nErroCode);

	return strResult;
}

void ESMWcharCopy(char* pstrdst, TCHAR* pstrsrc)
{
	int nLen = (int)wcslen(pstrsrc);
	wcstombs(pstrdst, pstrsrc, nLen+1);
}

/*
wchar_t* ESMCharToWChar(const char* pstrSrc)
{
	ASSERT(pstrSrc);
	int nLen = strlen(pstrSrc)+1;

	wchar_t* pwstr      = (LPWSTR) malloc ( sizeof( wchar_t )* nLen);
	mbstowcs(pwstr, pstrSrc, nLen);

	return pwstr;
}

char* ESMWCharToChar(const wchar_t* pwstrSrc)
{
	ASSERT(pwstrSrc);

	int nLen = wcslen(pwstrSrc);
	char* pstr      = (char*) malloc ( sizeof( char) * nLen + 1);
	wcstombs(pstr, pwstrSrc, nLen+1);

	return pstr;
}
*/

void ESMAddStatusInfo(int nID, CString strName)
{
//	g_pESMMainWnd->m_wndMonitoringViewChild.AddStackName(nID, strName);
}


BOOL ESMStringCompare(CString strReference, CString strTarget)
{
	//-- 2012-04-20 hongsu
	if(!strReference.GetLength())
		return FALSE;

	BOOL bResult = (wcsncmp(strReference.GetBuffer(), strTarget.GetBuffer(), strReference.GetLength()) == 0) ? TRUE:FALSE;
	strReference.ReleaseBuffer();
	strTarget.ReleaseBuffer();

	return bResult;
}
extern BOOL ESMGetGPUDecodeSupport()
{
	return g_pESMMainWnd->GetGPUDecodeSupport();
}

extern BOOL ESMGetCudaSupport()
{
	return g_pESMMainWnd->GetCudaSupport();
}
extern BOOL ESMGetMakingUse()
{
	return g_pESMMainWnd->GetMakingUse();
}
extern vector<CString> ESMGetDeleteDSC()
{
	return g_pESMMainWnd->GetDeleteDSC();
}

extern vector<CString> ESMGetDeleteDSCList()
{
	return g_pESMMainWnd->GetDeleteDSCList();
}
extern void ESMSetDeleteDSCList(vector<CString> m_list)
{
	g_pESMMainWnd->SetDeleteDSCList(m_list);
}
extern void ESMSetMakingUse(BOOL makinguseflag)
{
	g_pESMMainWnd->SetMakingUse(makinguseflag);
}

extern void ESMSetMakeMovie(BOOL bSet)
{
	g_pESMMainWnd->m_bMakeMovie = bSet;
}

extern BOOL ESMGetMakeMovie()
{
	return g_pESMMainWnd->m_bMakeMovie;
}

void ESMSetMakeAdjust(BOOL bSet)
{
	g_pESMMainWnd->m_bMakeAdjust = bSet;
}
BOOL ESMGetMakeAdjust()
{
	return g_pESMMainWnd->m_bMakeAdjust;
}

extern BOOL ESMGetSyncFlag()
{
	return g_pESMMainWnd->m_bSyncFlag;
}

extern void ESMSetSyncFlag(BOOL bSet)
{
	g_pESMMainWnd->m_bSyncFlag = bSet;
}

//------------------------------------------------------------------------------ 
//! @brief		ESMGetPath(int nDir, BOOL bFull)
//! @date		
//! @attention	none
//! @note	 	//-- 2009-05-20
//------------------------------------------------------------------------------ 
CString ESMGetPath(int nDir, BOOL bFull)
{
	CString strPath;

	if(!g_pESMMainWnd)
		return strPath;

	switch(nDir)
	{
	case ESM_PATH_HOME				: strPath = g_pESMMainWnd->GetESMOption()->m_Path .strPath[ESM_OPT_PATH_HOME];		break;
	case ESM_PATH_CONFIG			: strPath = g_pESMMainWnd->GetESMOption()->m_Path .strPath[ESM_OPT_PATH_CONF];		break;
	case ESM_PATH_FILESERVER		: strPath = g_pESMMainWnd->GetESMOption()->m_Path .strPath[ESM_OPT_PATH_FILESVR];	break;
	case ESM_PATH_OUTPUT			: strPath = g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_OUTPUT];		break;	
	//case ESM_PATH_LOG				: strPath.Format(_T("%s\\%s\\%s")			,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_LOG],STR_IP, _T("log"));						break;									
	//case ESM_PATH_LOG_DATE			: strPath.Format(_T("%s\\%s\\%s\\%s")		,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_LOG],STR_IP, ESMGetDate(), _T("log"));	break;
	case ESM_PATH_LOG				: strPath.Format(_T("%s")			,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_LOG]);	break;
	case ESM_PATH_LOG_DATE			: strPath.Format(_T("%s\\%s")		,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_LOG],ESMGetDate());	break;
	case ESM_PATH_OBJECT			: strPath.Format(_T("%s\\object")			,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_WORK]);		break;	
	case ESM_PATH_ADJUST			: strPath.Format(_T("%s\\adjust")			,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_WORK]);		break;	
	case ESM_PATH_IMAGE				: strPath.Format(_T("%s\\img")				,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_HOME]);		break;	
	case ESM_PATH_SETUP				: strPath.Format(_T("%s\\Setup")			,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_PATH_HOME]);		break;	
	case ESM_PATH_MOVIE				: strPath.Format(_T("%s\\Movie")			,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;	
	case ESM_PATH_MOVIE_FILE		: strPath.Format(_T("%s\\Movie\\files")		,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;	
	case ESM_PATH_LOCALMOVIE		: strPath.Format(_T("%s\\Movie\\files")		,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;
	case ESM_PATH_MOVIE_CONF		: strPath.Format(_T("%s\\Movie\\config")	,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;	
	case ESM_PATH_PICTURE			: strPath.Format(_T("%s\\Picture")			,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;	
	case ESM_PATH_PICTURE_FILE		: strPath.Format(_T("%s\\Picture\\files")	,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;	
	case ESM_PATH_PICTURE_CONF		: strPath.Format(_T("%s\\Picture\\config")	,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;	
	case ESM_PATH_MOVIE_EFFECT		: strPath.Format(_T("%s\\Movie\\template\\Effect")		,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;	
	case ESM_PATH_PICTURE_EFFECT	: strPath.Format(_T("%s\\Picture\\template\\Effect")	,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;	
	case ESM_PATH_MOVIE_TIMELINE	: strPath.Format(_T("%s\\Movie\\template\\Timeline")	,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;	
	case ESM_PATH_PICTURE_TIMELINE	: strPath.Format(_T("%s\\Picture\\template\\Timeline")	,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;	
	case ESM_PATH_MOVIE_TEMPLATE	: strPath.Format(_T("%s\\Movie\\template")	,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;	
	case ESM_PATH_PICTURE_TEMPLATE	: strPath.Format(_T("%s\\Picture\\template")	,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;	
	case ESM_PATH_MOVIE_WEB			: strPath.Format(_T("%s\\webapps\\ROOT")			,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD]);		break;	
	//case ESM_PATH_MOVIE_WEB			: strPath.Format(_T("%s\\webapps\\ROOT")			,_T("C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0"));		break;	
	}
		
	//-- CHANGE FULL PATH $(HOME)/($FILESVR)
	
    strPath = ESMGetFullPath(strPath);
	return strPath;
}

CString ESMGetBackupString(int nFlag)
{
	CString strString;

	if(!g_pESMMainWnd)
		return strString;

	switch(nFlag)
	{
	case ESM_BACKUP_STRING_SRCFOLDER	: strString = g_pESMMainWnd->GetESMOption()->m_BackupString.strString[ESM_BACKUP_STRING_SRCFOLDER];	break;
	case ESM_BACKUP_STRING_DSTFOLDER	: strString = g_pESMMainWnd->GetESMOption()->m_BackupString.strString[ESM_BACKUP_STRING_DSTFOLDER];	break;
	case ESM_BACKUP_STRING_FTPHOST		: strString = g_pESMMainWnd->GetESMOption()->m_BackupString.strString[ESM_BACKUP_STRING_FTPHOST];	break;
	case ESM_BACKUP_STRING_FTPID		: strString = g_pESMMainWnd->GetESMOption()->m_BackupString.strString[ESM_BACKUP_STRING_FTPID];		break;			
	case ESM_BACKUP_STRING_FTPPW		: strString = g_pESMMainWnd->GetESMOption()->m_BackupString.strString[ESM_BACKUP_STRING_FTPPW];		break;
	}
	//-- CHANGE FULL PATH $(HOME)/($FILESVR)	
	return strString;
}

CString ESMGetCeremony(int nDir)
{
	CString strPath;

	if(!g_pESMMainWnd)
		return strPath;

	switch(nDir)
	{
	case ESM_VALUE_CEREMONYDBADDRIP			: strPath = g_pESMMainWnd->GetESMOption()->m_Ceremony.strDbAddrIP;		break;
	case ESM_VALUE_CEREMONYDBID				: strPath = g_pESMMainWnd->GetESMOption()->m_Ceremony.strDbId;			break;
	case ESM_VALUE_CEREMONYDBPW				: strPath = g_pESMMainWnd->GetESMOption()->m_Ceremony.strDbPasswd;		break;
	case ESM_VALUE_CEREMONYDBNAME			: strPath = g_pESMMainWnd->GetESMOption()->m_Ceremony.strDbName;		break;
	case ESM_VALUE_CEREMONYIMAGEPATH		: strPath = g_pESMMainWnd->GetESMOption()->m_Ceremony.strImagePath;		break;
	case ESM_VALUE_CEREMONYMOVIEPATH		: strPath = g_pESMMainWnd->GetESMOption()->m_Ceremony.strMoviePath;		break;
	case ESM_VALUE_CEREMONY_WIDTH			: strPath = g_pESMMainWnd->GetESMOption()->m_Ceremony.strWidth;			break;
	case ESM_VALUE_CEREMONY_HEIGHT			: strPath = g_pESMMainWnd->GetESMOption()->m_Ceremony.strHeight;		break;
	}
	return strPath;
}

CString ESMGetManageMent(int nDir)
{
	CString strPath;

	if(!g_pESMMainWnd)
		return strPath;

	switch(nDir)
	{
	case ESM_MANAGEMENT_SEL_PATH			: strPath = g_pESMMainWnd->GetESMOption()->m_menagement.strSelectFilePath;		break;
	case ESM_MANAGEMENT_SAVE_PATH			: strPath = g_pESMMainWnd->GetESMOption()->m_menagement.strSaveFilePath;		break;
	}
	return strPath;
}

int ESMGetShiftFrame(CString strDSCId)
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\ShiftFrame.csv"), ESMGetPath(ESM_PATH_SETUP));
	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		return FALSE;
	}

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());
	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;
	CString strTpValue1, strTpValue2;
	int nDistance = 0;
	while (iPos  < iLength )
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;
		AfxExtractSubString(strTpValue1, sLine, 0, ',');
		AfxExtractSubString(strTpValue2, sLine, 1, ',');
		if( strTpValue1 == strDSCId)
		{
			nDistance = _ttoi(strTpValue2);
			break;
		}
	}
	return nDistance;
}


CString ESMGetDataPath(CString strLoaction)
{
	CString strFilePath = ESMGetPath(ESM_PATH_MOVIE_FILE);
	CString strTp;
	int nIndex = strFilePath.Find(_T("4DMaker"));
	strFilePath = strFilePath.Right(strFilePath.GetLength() - nIndex);
	strFilePath = _T("\\\\") + strLoaction + _T("\\") + strFilePath;
	return strFilePath;
}

CString ESMGetServerRamPath()
{
	CString strPath;
	strPath.Format(_T("%s")		,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_SERVERRAM]);

	return strPath;
}

CString ESMGetSaveImagePath()
{
	CString strPath;
	strPath.Format(_T("%s")		,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_SAVE_IMG]);

	return strPath;
}

CString ESMGetClientRamPath(CString strLoaction)
{
 	CString strPath;
	if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		if(strLoaction == _T(""))
			strPath = g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_SERVERRAM];
		else
			strPath.Format(_T("\\\\%s%s"), strLoaction, g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_CLIENTRAM]);
	}
	else 
		strPath.Format(_T("%s")	,g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_CLIENTRAM]);

 	return strPath;
}

CString ESMGetClientRecordPath(CString strLoaction)
{
 	CString strPath;
	
	//strPath.Format(_T("\\\\%s\\%s"), strLoaction, ESMGetManageMent(ESM_MANAGEMENT_SEL_PATH));

	int nPos;
	CString strRecordPath = g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_RECORD];
	nPos = strRecordPath.Find(_T("\\"));
	strRecordPath.Delete(0,nPos);

	strPath.Format(_T("\\\\%s%s\\Movie\\files"), strLoaction, strRecordPath);

 	return strPath;
}

CString ESMGetClientBackupRamPath(CString strLoaction)
{
	CString strPath;
	strPath.Format(_T("%s2"), ESMGetClientRamPath(strLoaction));
	return strPath;
}

//------------------------------------------------------------------------------ 
//! @brief		TGGetFullPath(CString strPath)
//! @date		
//! @attention	none
//! @note	 	//-- 2009-05-25
//------------------------------------------------------------------------------ 
CString ESMGetRoot(CString strPath)
{
	if(!g_pESMMainWnd)
		return _T("");	
	return g_pESMMainWnd->GetRoot(strPath);
}


//------------------------------------------------------------------------------ 
//! @brief		TGGetFullPath(CString strPath)
//! @date		
//! @attention	none
//! @note	 	//-- 2009-05-25
//------------------------------------------------------------------------------ 
CString ESMGetFullPath(CString strPath)
{
	if(!g_pESMMainWnd)
		return _T("");	
	if(!strPath.GetLength())
		return _T("");	
	
	CString strHome		= g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_HOME];
	CString strFileSvr	= g_pESMMainWnd->GetESMOption()->m_Path.strPath[ESM_OPT_PATH_FILESVR];	
	CString strIP		= g_pESMMainWnd->GetESMOption()->m_strIP;
	//-- CHANGE From $(HOME) => C;//...blabla
	strPath.Replace(STR_HOME	, strHome);
	//-- CHANGE From $(FILESVR) => C;//...blabla
	strPath.Replace(STR_FILESVR	, strFileSvr);
	//-- 2009-06-25  $(IP)		=> xxx.xxx.xxx.xxx
	strPath.Replace(STR_IP		, strIP);
	
	//-- 2010-7-20 hongsu.jung
	//-- CHANGE REAL TIME
	CTime t = CTime::GetCurrentTime();	
	CString strTCTime = t.Format("%H.%M.%S");
	strPath.Replace(STR_REALTIME,strTCTime);
	return strPath;
}

void ESMGetMovieTime(int& nTargetTime, int &nSec, int &nMilli, BOOL bNext)
{
	nSec	= nTargetTime/1000;
	nMilli	= nTargetTime - (nSec*1000);

	if(ESMGetFrameRate() == MOVIE_FPS_UHD_25P || ESMGetFrameRate() == MOVIE_FPS_FHD_25P)
	{
		if( nMilli > 960)
		{
			if(bNext)
			{
				nMilli = 0;
				nSec++;
				nTargetTime = nSec*1000;
			}
			else
			{
				nMilli = 960;	// Fix Milli second			
				nTargetTime = nSec*1000 + nMilli;
			}
		}
		else if( 0 < nMilli  && nMilli < 40)
		{
			if(bNext)
			{
				nMilli = 40;
				nTargetTime = nSec*1000 + nMilli;
			}
			else
			{
				nMilli = 0;	
			}
		}

	}else if(ESMGetFrameRate() == MOVIE_FPS_UHD_30P || ESMGetFrameRate() == MOVIE_FPS_FHD_30P)
	{
		if( nMilli > 957)
		{
			if(bNext)
			{
				nMilli = 0;
				nSec++;
				nTargetTime = nSec*1000;
			}
			else
			{
				nMilli = 957;	// Fix Milli second			
				nTargetTime = nSec*1000 + nMilli;
			}
		}
		else if( 0 < nMilli  && nMilli < 33)
		{
			if(bNext)
			{
				nMilli = 33;
				nTargetTime = nSec*1000 + nMilli;
			}
			else
			{
				nMilli = 0;	
			}
		}
	}else if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P || ESMGetFrameRate() == MOVIE_FPS_UHD_50P)
	{
		if( nMilli > 980)
		{
			if(bNext)
			{
				nMilli = 0;
				nSec++;
				nTargetTime = nSec*1000;
			}
			else
			{
				nMilli = 980;	// Fix Milli second			
				nTargetTime = nSec*1000 + nMilli;
			}
		}
		else if( 0 < nMilli  && nMilli < 20)
		{
			if(bNext)
			{
				nMilli = 20;
				nTargetTime = nSec*1000 + nMilli;
			}
			else
			{
				nMilli = 0;	
			}
		}
	}else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X2 || ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X1 || ESMGetFrameRate() == MOVIE_FPS_UHD_60P)
	{
		if( nMilli > 944)
		{
			if(bNext)
			{
				nMilli = 0;
				nSec++;
				nTargetTime = nSec*1000;
			}
			else
			{
				nMilli = 944;	// Fix Milli second			
				nTargetTime = nSec*1000 + nMilli;
			}
		}
		else if( 0 < nMilli  && nMilli < 16)
		{
			if(bNext)
			{
				nMilli = 16;
				nTargetTime = nSec*1000 + nMilli;
			}
			else
			{
				nMilli = 0;	
			}
		}
	}else if(ESMGetFrameRate() == MOVIE_FPS_FHD_120P)
	{
		if( nMilli > 952)
		{
			if(bNext)
			{
				nMilli = 0;
				nSec++;
				nTargetTime = nSec*1000;
			}
			else
			{
				nMilli = 952;	// Fix Milli second			
				nTargetTime = nSec*1000 + nMilli;
			}
		}
		else if( 0 < nMilli  && nMilli < 8)
		{
			if(bNext)
			{
				nMilli = 8;
				nTargetTime = nSec*1000 + nMilli;
			}
			else
			{
				nMilli = 0;	
			}
		}
	}
	else
	{
		if( nMilli > 957)
		{
			if(bNext)
			{
				nMilli = 0;
				nSec++;
				nTargetTime = nSec*1000;
			}
			else
			{
				nMilli = 957;	// Fix Milli second			
				nTargetTime = nSec*1000 + nMilli;
			}
		}
		else if( 0 < nMilli  && nMilli < 33)
		{
			if(bNext)
			{
				nMilli = 33;
				nTargetTime = nSec*1000 + nMilli;
			}
			else
			{
				nMilli = 0;	
			}
		}
	}

	//if( nMilli > 960)
	//{
	//	if(bNext)
	//	{
	//		nMilli = 0;
	//		nSec++;
	//		nTargetTime = nSec*1000;
	//	}
	//	else
	//	{
	//		nMilli = 957;	// Fix Milli second			
	//		nTargetTime = nSec*1000 + nMilli;
	//	}
	//}
	//else if( 0 < nMilli  && nMilli < 33)
	//{
	//	if(bNext)
	//	{
	//		nMilli = 33;
	//		nTargetTime = nSec*1000 + nMilli;
	//	}
	//	else
	//	{
	//		nMilli = 0;	
	//	}
	//}
}

int ESMGetFrameIndex(int nTargetTime)
{
	//jhhan 170714
	int nFramePerSec = movie_frame_per_second;
	if(ESMGetFrameRate() == MOVIE_FPS_UHD_25P || ESMGetFrameRate() == MOVIE_FPS_FHD_25P)
	{
		nFramePerSec = 25;
	}
	else if (ESMGetFrameRate() == MOVIE_FPS_UHD_30P || ESMGetFrameRate() == MOVIE_FPS_FHD_30P )
	{
		nFramePerSec = 30;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P || ESMGetFrameRate() == MOVIE_FPS_UHD_50P)//MOVIE_FPS_50P
	{
		nFramePerSec = 50;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X2 || ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X1 || ESMGetFrameRate() == MOVIE_FPS_UHD_60P)
	{
		nFramePerSec = 60;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_120P)
	{
		nFramePerSec = 120;
	}
	else
		nFramePerSec = 30;



	int nFrameCount = 0;
	nFrameCount  = nTargetTime / 1000 * nFramePerSec;
	nFrameCount  += (nTargetTime % 1000) / movie_next_frame_time;
	return nFrameCount;
}

int ESMCheckNetwork(CString strDscIP)
{
	HANDLE hIcmpFile;
	char ipaddrstr[MAX_PATH] = {0};
	unsigned long ipaddr;
	DWORD dwRetVal = 0;
	static const WORD sendSize = 32;
	char sendData[sendSize] = "Data Buffer";
	static const DWORD replySize = sizeof(ICMP_ECHO_REPLY) + sendSize;
	char replyBuffer[replySize];
					
	char pchPath[MAX_PATH] = {0};
	wcstombs(ipaddrstr, strDscIP, MAX_PATH); 
	hIcmpFile = IcmpCreateFile();
	ipaddr = inet_addr(ipaddrstr);
    
	auto ret = IcmpSendEcho(hIcmpFile, ipaddr, sendData, sendSize, NULL, replyBuffer, replySize, 1000);
	
	IcmpCloseHandle(hIcmpFile); 

	return ret;
}

CString ESMGetPicturePath(CString strDscId)
{
	CString strPath;
	CString strDscIp;

	strDscIp = ESMGetIPFromDSCID(strDscId);

	if(!(ESMCheckNetwork(strDscIp) == 0))
		strPath.Format(_T("%s\\%s\\%s.jpg"), ESMGetClientRamPath(strDscIp), ESMGetFrameRecord(), strDscId);
	else
	{
		//ESMLog(5, _T("MP4 File not found in %s"),strPath);
		//ESMLog(5,_T("%s network connection to the Agent could not be established"),strDscIp);
		strPath.Format(_T("%s\\%s\\%s.jpg"), ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), strDscId);
	}

	return strPath;
}

CString ESMGetMoviePath(CString strDscId, int nFrameIndex, BOOL bLocal, BOOL bFrameViewer)
{
	//-- 2015-10.22 joonho.kim GOP 사이즈에따라 Image 얻기
	int nFrame;
	int nMovieIndex = 0,  nRealFrameIndex = 0;
	ESMGetMovieIndex(nFrameIndex, nMovieIndex, nRealFrameIndex);

	CString strMoviePath;
	//-- Get Frame Path
	CString strDscIp;

	if(bFrameViewer == TRUE)
		strDscIp = ESMGetIPFromDSCIDInFrameViewer(strDscId);
	else
		strDscIp = ESMGetIPFromDSCID(strDscId);

	if(ESMGetFrameRecord().GetLength())
	{
		//jhhan 16-09-23
		//ESMLog(5, _T("ESMGetExecuteMode : %s, LoadDirect : %s"), ESMGetExecuteMode()?_T("TRUE"):_T("FALSE"), ESMGetValue(ESM_VALUE_LOADFILE_DIRECT)?_T("TRUE"):_T("FALSE"));

	 	if(ESMGetExecuteMode())
	 	{
#if 1 //-- joonho.kim GOP
	 		strMoviePath.Format(_T("%s\\%s\\%s_%d.mp4"),ESMGetClientRamPath(strDscIp), ESMGetFrameRecord(), strDscId, nMovieIndex);
#else
	 		strMoviePath.Format(_T("%s\\%s\\%s.mp4"),ESMGetClientRamPath(strDscIp), ESMGetFrameRecord(), strDSCID);
#endif
			if(bLocal)
			{
				strMoviePath.Format(_T("%s\\%s\\%s_%d.mp4"),ESMGetClientRamPath(), ESMGetFrameRecord(), strDscId, nMovieIndex);
			}
	 	}
	 	else
		{
#if 1 //-- joonho.kim GOP
			/* // LoadDirect 적용 이전 Code
			if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
				strMoviePath.Format(_T("%s\\%s\\%s_%d.mp4"), ESMGetClientRamPath(strDscIp), ESMGetFrameRecord(), strDscId, nMovieIndex);
			else
				strMoviePath.Format(_T("%s\\%s\\%s_%d.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), strDscId, nMovieIndex);
			*/

			// LoadDirect 적용 이후
			if(!ESMGetValue(ESM_VALUE_LOADFILE_DIRECT))
				// LoadDirect 아닐때
				strMoviePath.Format(_T("%s\\%s\\%s_%d.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), strDscId, nMovieIndex);
			else
			{
				// LoadDirect
				if(!(ESMCheckNetwork(strDscIp) == 0))
				{
					HANDLE hFind;
					WIN32_FIND_DATA fd;

					//strMoviePath.Format(_T("%s\\%s\\%s_%d.mp4"), ESMGetClientRamPath(strDscIp), ESMGetFrameRecord(), strDscId, nMovieIndex);
					/*if(bLocal)
						strMoviePath.Format(_T("%s\\%s\\%s_%d.mp4"),ESMGetClientRamPath(), ESMGetFrameRecord(), strDscId, nMovieIndex);
					else*/
						strMoviePath.Format(_T("%s\\%s\\%s_%d.mp4"), ESMGetClientRecordPath(strDscIp), ESMGetFrameRecord(), strDscId, nMovieIndex);
					
					if ((hFind = ::FindFirstFile(strMoviePath, &fd)) != INVALID_HANDLE_VALUE) 
						FindClose(hFind);
					else
						ESMLog(0, _T("Record File not found in %s"),strMoviePath);
						//strMoviePath.Format(_T("%s\\%s\\%s_%d.mp4"), ESMGetClientRecordPath(strDscIp), ESMGetFrameRecord(), strDscId, nMovieIndex);
				}
				else
					ESMLog(0,_T("%s network connection to the Agent could not be established"),strDscIp);

			}
#else
			strMoviePath.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), strDSCID);
#endif
		}
	
	}
	//ESMLog(0,_T("[ESMGetMoviePath](%s-%d) : %s"), strDscId, nFrameIndex, strMoviePath);
	return strMoviePath;
}

void ESMGetMovieIndex(int nIndex, int& nMovieIndex, int& nRealFrameIndex)
{
	g_pESMMainWnd->SetInfoToMovieMgr();
	g_pESMMainWnd->m_pESMMovieMgr->GetMovieIndex(nIndex, nMovieIndex, nRealFrameIndex, g_pESMMainWnd->m_pESMMovieMgr->GetMovieSize(), ESMGetFrameRate());
}

int ESMGetFrameTime(int nIndex)
{
	int nTime = 0;
	nTime = nIndex / movie_frame_per_second  * 1000;
	nTime += (nIndex%movie_frame_per_second) * movie_next_frame_time;

	return nTime;
}

int ESMGetCntMovieTime(int nDurationTime)
{
	int nCnt, nSec, nMilli;
	ESMGetMovieTime(nDurationTime, nSec, nMilli);
	nCnt = nSec*movie_frame_per_second;
	nCnt += nMilli/movie_next_frame_time;
	return nCnt;
}

void ESMGetNextFrameTime(int &nTime)
{
	int nNextTime = nTime;
	nNextTime += movie_next_frame_time;
	int nSec, nMilli;
	ESMGetMovieTime(nNextTime, nSec, nMilli);
	nTime = nSec*1000 + nMilli;
}

void ESMGetPreviousFrameTime(int &nTime)
{
	int nNextTime = nTime;
	nNextTime -= movie_next_frame_time;
	int nSec, nMilli;
	ESMGetMovieTime(nNextTime, nSec, nMilli, FALSE);
	nTime = nSec*1000 + nMilli;
}

void ESMCheckFrameTime(int &nTime)
{
	int nInputTime	= nTime;
	int nSec		= nInputTime/1000;
	int nMilli		= nInputTime - (nSec*1000);

	int _nRefer = ESMReferenceTime() + movie_next_frame_time; //ReferenceTime = FrameTime * FrameMaxCount;

	int nCheck = nMilli % movie_next_frame_time;
	if(nCheck >= 0)
	{
		nCheck = nMilli / movie_next_frame_time;
		nMilli = nCheck * movie_next_frame_time;

		//jhhan 171128 동영상 프레임별 시간 상이 적용
#if 0
		if( nMilli == 990)
			nTime = (nSec + 1) * 1000;
		else
			nTime = nSec * 1000 + nMilli;
#else
		if(nMilli >= _nRefer)
		{
			nTime = (nSec + 1) * 1000;
		}else
		{
			nTime = nSec * 1000 + nMilli;
		}
#endif
	}
}

void ESMGetSelectedFrame(CString &strDsc, int &nTime)
{
//-- 2015-02-24 cygil@esmlab.com 4DModeler에서는 사용안함
#ifndef _4DMODEL
	strDsc = g_pESMMainWnd->GetFrameWnd()->GetSelectedDSC();
	nTime = g_pESMMainWnd->GetFrameWnd()->GetSelectedTime();
#endif
}

int	ESMGetDSCIndex(CString strDsc)
{
	return g_pESMMainWnd->m_wndDSCViewer.GetItemIndex(strDsc);
}

void ESMGet3DInfo(ESM3DInfo* p3DInfo)
{
	g_pESMMainWnd->m_wndDSCViewer.m_pListView->ESMGet3DInfo(p3DInfo);
}

int ESMGetIDfromIP(CString strIP)
{
	CString strTemp;
	int ret = 4;
	int nCurpos = 0;
	while(ret--)
	{
		strTemp = strIP.Tokenize(_T("."), nCurpos);
		strTemp.Trim();
//		if(nCurpos == -1) 
//			return -1;
	}
	return _ttoi(strTemp);
}

CString ESMGetIPFromDSCID(CString strID)
{
	CString strDscIp;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;
	for( int i =0 ;i < arDSCList.GetSize(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if(pItem)
		{
			if(pItem->GetDeviceDSCID() == strID)
			{
				strDscIp = pItem->GetDSCInfo(DSC_INFO_LOCATION);
				break;
			}
		}
	}
	return strDscIp;
}

CString ESMGetIPFromDSCIDInFrameViewer(CString strID)
{
	CString strDscIp;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;
	for( int i =0 ;i < arDSCList.GetSize(); i++)
	{
	 	pItem = (CDSCItem*)arDSCList.GetAt(i);
	 	if(pItem)
	 	{
	 		if(pItem->GetDeviceDSCID() == strID)
	 		{
				//180115
				/*if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER){}*/
				if(ESMGetGPUMakeFile() && ESMGetRemoteSkip() == TRUE)
				{
					CString strFile;
					strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_EX);
					CESMIni ini;	
					if(ini.SetIniFilename (strFile))
					{
						strDscIp = ini.GetString(_T("4DA"), strID);
						if(strDscIp.IsEmpty())
						{
							strDscIp = pItem->GetDSCInfo(DSC_INFO_LOCATION);
						}
					}
					
				}
				else
				{
	 				strDscIp = pItem->GetDSCInfo(DSC_INFO_LOCATION);
				}
	 			break;
	 		}
	 	}
	}
	return strDscIp;
}

void ESMGetDSCList(CObArray* pAr)
{
	g_pESMMainWnd->m_wndDSCViewer.GetDSCList(pAr);
}

int ESMGetDSCSavedTime()
{
	//-- 2014-07-17 hongsu@esmlab.com
	//-- Change to Max Movie Time 
	//return g_pESMMainWnd->m_wndDSCViewer.GetItemData(0)->GetSavedLastTime();
	
	int nRecTime = g_pESMMainWnd->m_wndDSCViewer.m_pFrameSelector->GetRecTime();
	//-- Change to Movie Time
	ESMCheckFrameTime(nRecTime);
	return nRecTime;
}


int ESMGetFilmState()
{
	return g_pESMMainWnd->m_FileMgr->GetFilmState();
}

float ESMGetVersion(float fVersion)
{
	return fVersion +0.0001;
}

void ESMSetFilmState(int nTakeType)
{
	return g_pESMMainWnd->m_FileMgr->SetFilmState(nTakeType);
}

CString GetPropertyValuetoStr(int nPropCode, int nValue, CString* pstrModel)
{
	CString strValue, strRet, strSection;
	CString strConfigFile;

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Load Config File
	strConfigFile.Format(_T("%s\\%s.info"), ESMGetPath(ESM_PATH_CONFIG), pstrModel->GetString());
	CESMIni ini;
	if(!ini.SetIniFilename (strConfigFile))
		return strRet;

	if(pstrModel->CompareNoCase(_T("GH5")) == 0)
	{
		strValue.Format(_T("%x"),nValue);
	}
	else
	{
		if(
			nPropCode == PTP_DPC_EXPOSURE_INDEX	 ||
			nPropCode == PTP_DPC_F_NUMBER	
			)
			strValue.Format(_T("%d"),nValue);
		else if (nPropCode == PTP_CODE_SAMSUNG_ZOOM_CTRL_GET)
		{
			strRet.Format(_T("%d"), nValue);
			return strRet;
		}
		else
			strValue.Format(_T("%x"),nValue);
	}
	
	strSection.Format(_T("%x"),nPropCode);
	strRet = ini.GetString(strSection, strValue);

	return strRet;
}

void	ESMSetScroll(int nScroll)
{
	if(g_pESMMainWnd)
		g_pESMMainWnd->m_nScrollY = nScroll;
}
int		ESMGetScroll()
{
	return g_pESMMainWnd->m_nScrollY;
}

void  ESMGetAdjustMovieInfo(ESMAdjustMovie& info)
{
	info.arrPathList.Copy(g_pESMMainWnd->m_pOptionDlg->m_pOption->m_AdjustMovie.arrPathList);
	info.strSrcPath = g_pESMMainWnd->m_pOptionDlg->m_pOption->m_AdjustMovie.strSrcPath;
	info.strTarPath = g_pESMMainWnd->m_pOptionDlg->m_pOption->m_AdjustMovie.strTarPath;
	info.strStartPath = g_pESMMainWnd->m_pOptionDlg->m_pOption->m_AdjustMovie.strStartPath;
	info.strEndPath = g_pESMMainWnd->m_pOptionDlg->m_pOption->m_AdjustMovie.strEndPath;
}

void ESMSetInvalidMovieSize(BOOL bInvalid)
{
	if(g_pESMMainWnd)
		g_pESMMainWnd->m_bInvalidMovieSize = bInvalid;
}

BOOL ESMGetInvalidMovieSize()
{
	BOOL bInvalid = FALSE;
	if(g_pESMMainWnd)
		bInvalid = g_pESMMainWnd->m_bInvalidMovieSize;

	return bInvalid;
}

void ESMSetMovieSize(int nSize)
{
	if(g_pESMMainWnd)
		g_pESMMainWnd->m_nMovieSize = nSize;
}

int ESMGetMovieSize()
{
	//CMiLRe 20151119 동영상 사이즈(FHD, UHD) Check
	return g_pESMMainWnd->m_wndTimeLineEditor.m_pTimeLineView->m_nImageSize;	
}

int ESMGetMovieFlag()
{
	//return g_pESMMainWnd->m_pOptionDlg->m_Opt4DMaker.m_bUHDtoFHD;
	return ESMGetValue(ESM_VALUE_UHD_TO_FHD);
}

int ESMGetCheckValid()
{
	return ESMGetValue(ESM_VALUE_CHECK_VALID);
}

int ESMGetVMCCFlag()
{
	return ESMGetValue(ESM_VALUE_ENABLE_VMCC);
}

int ESMGetBaseBallInning()
{
	return ESMGetValue(ESM_VALUE_BASEBALL_INNING);
}

void ESMSetBaseBallTamplate(char key)
{
	g_pESMMainWnd->m_baseballkey = key;
}

char ESMGetBaseBallTamplate()
{
	return g_pESMMainWnd->m_baseballkey;
}

int ESMGetBaseBallFlag()
{
	return ESMGetValue(ESM_VALUE_ENABLE_BASEBALL);
}

int ESMGetReverseMovie()
{
	return ESMGetValue(ESM_VALUE_REVERSE_MOVIE);
}

int ESMGetGPUMakeFile()
{
	return ESMGetValue(ESM_VALUE_GPU_MAKE_FILE);
}

//jhhan 170323
//void ESMSetDeviceDSCID(CString strID)
//{
//	//2017-03-16 joonho.kim
//	try
//	{
//		if(g_pESMMainWnd)
//			g_pESMMainWnd->m_strDSCID = strID;
//	}
//	catch(...)
//	{
//		ESMLog(0,_T("ESMSetDeviceDSCID "));
//	}
//}
//
//CString ESMGetDeviceDSCID()
//{
//	return g_pESMMainWnd->m_strDSCID;
//}

int ESMGetGPUMakeFileCount()
{
	return g_pESMMainWnd->m_nGPUMakeFileCount;
}

void ESMSetGPUMakeFileCount(int nCount)
{
	g_pESMMainWnd->m_nGPUMakeFileCount = nCount;
}

void ESMInitGPUMakeFileCount()
{
	g_pESMMainWnd->m_nGPUMakeFileCount = 0;
}

BOOL ESMGetMakeServerFlag()
{
	return ESMGetValue(ESM_VALUE_MAKE_SERVER);
}

void ESMSetKeyValue(char ckey)
{
	if(g_pESMMainWnd)
		g_pESMMainWnd->m_key = ckey;
}

char ESMGetKeyValue()
{
	return	g_pESMMainWnd->m_key;
}

//-- 2013-10-23 hongsu@esmlab.com
//-- Get Tick Count on PC  
int ESMGetTick()
{
	/*
	timeb tb;
	ftime( &tb );
	return tb.millitm + (tb.time & 0xfffff) * 1000;
	*/
	
	g_pESMMainWnd->m_Clock.End();
	if(g_pESMMainWnd->m_pMovieRTMovie)
	{
		g_pESMMainWnd->m_Clock.GetDurationMilliSecond() * 10;
	}
	return (int)(g_pESMMainWnd->m_Clock.GetDurationMilliSecond() * 10);	

	/*LARGE_INTEGER		m_swCurr;
	float				m_fTimeforDuration;

	QueryPerformanceCounter(&m_swCurr);
	m_fTimeforDuration = (m_swCurr.QuadPart - g_pESMMainWnd->m_swStart.QuadPart)/(float)g_pESMMainWnd->m_swFreq.QuadPart; 
	return (int)(m_fTimeforDuration*1000);

	
	// return timeGetTime();
	*/
	//return (int)GetTickCount64();
}

void ESMGetTickInfo(LARGE_INTEGER& swFreq, LARGE_INTEGER& swStart)
{
	swFreq = g_pESMMainWnd->m_Clock.m_swFreq;
	swStart= g_pESMMainWnd->m_Clock.m_swStart;
}

CString ESMGetLocalIP()
{
	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	CString strIP;
	PHOSTENT hostinfo;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 )
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				strIP = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}
		}      
		WSACleanup( );
	}
	return strIP;
}

void ESMLoadProfileSelectPoint(CString strProfilePath)
{
	//Load Select Point
	CString strSection = _T("Recording");
	CString strKeyName = _T("SelectPointTime");

	strProfilePath.Replace(_T(".4dm"), _T(".sp"));

	/*int nResult = GetPrivateProfileInt( strSection, strKeyName, 0, strProfilePath );
	if(nResult)
	{
	ESMSetRecordingInfoint("SelectPointTime0", nResult);
	ESMSetLastSelectPointNum(0);
	}*/

	//jhhan 181220
	int nCnt = 0;
	while(1)
	{

		CString _strKey;
		_strKey.Format(_T("%s%d"),strKeyName, nCnt);

		int nResult = GetPrivateProfileInt( strSection, _strKey, 0, strProfilePath );
		if(nResult)
		{
			ESMSetRecordingInfoint(_strKey, nResult);
			ESMSetLastSelectPointNum(nCnt);

			ESMSetViewPointNum(nCnt);
			g_pESMMainWnd->OnSelectViewBar(nResult, nCnt);
		}

		if(nResult == 0)
			break;

		nCnt++;

	}
}

void ESMSetRecordingInfoint(CString strKeyName, int nValue)
{
	CString strSection = _T("Recording");
	CString strFileServerPath, strFileServerFile, strValue;

	strValue.Format(_T("%d"), nValue);

	strFileServerPath.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));
	strFileServerFile = strFileServerPath + _T("\\Recorder.ini");

	try
	{
		::WritePrivateProfileString( strSection, strKeyName, strValue, strFileServerFile );
	}
	catch(...)
	{
		ESMLog(0,_T("ESMSetRecordingInfoint: .ini writing error"));
	}
}

void ESMSetRecordingPoint(CString strKeyName,int nValue)
{
	CString strSection = _T("Recording");
	CString  strValue;

	strValue.Format(_T("%d"), nValue);

	CString strRecordProfile = ESMGetFrameRecord();
	if(strRecordProfile.GetLength() && ESMGetExecuteMode())
	{
		CString strFilePath;
		strFilePath.Format(_T("%s\\%s.sp"), ESMGetPath(ESM_PATH_MOVIE), strRecordProfile);
		//jhhan 181023
		::WritePrivateProfileString( strSection, strKeyName, strValue, strFilePath );
		
		strKeyName = "SelectPointTime";
		::WritePrivateProfileString( strSection, strKeyName, strValue, strFilePath );
	}
}

void ESMSetRecordingInfoString(CString strKeyName, CString strValue)
{
	CString strSection = _T("Recording");
	CString strFileServerPath, strFileServerFile;

	strFileServerPath.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));
	strFileServerFile = strFileServerPath + _T("\\Recorder.ini");

	::WritePrivateProfileString( strSection, strKeyName, strValue, strFileServerFile );
}

int ESMGetRecordingInfoInt(CString strKeyName, int nDefault)
{
	CString strSection = _T("Recording");
	CString strFileServerPath, strFileServerFile, strValue;

	strFileServerPath.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));
	strFileServerFile = strFileServerPath + _T("\\Recorder.ini");
	int nResult = GetPrivateProfileInt( strSection, strKeyName, nDefault, strFileServerFile );

	return nResult;
}

CString ESMGetRecordingInfoString(CString strKeyName, CString strDefault)
{
	CString strSection = _T("Recording");
	CString strFileServerPath, strFileServerFile, strValue;

	strFileServerPath.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));
	strFileServerFile = strFileServerPath + _T("\\Recorder.ini");
	TCHAR* pBuffer = new TCHAR[2048];

	long ret = ::GetPrivateProfileString( strSection, strKeyName, strDefault, pBuffer, 2047, strFileServerFile );

	CString s = pBuffer;
	delete pBuffer;

	if( ret==0 ) return strDefault;
	return s;
}

void ESMSetCeremonyInfoString(CString strKeyName, CString strValue)
{
	CString strSection = _T("Ceremony");
	CString strFileServerPath, strFileServerFile;

	strFileServerPath.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));
	strFileServerFile = strFileServerPath + _T("\\Ceremony.ini");

	::WritePrivateProfileString( strSection, strKeyName, strValue, strFileServerFile );
}

CString ESMGetCeremonyInfoString(CString strKeyName, CString strDefault)
{
	CString strSection = _T("Ceremony");
	CString strFileServerPath, strFileServerFile, strValue;

	strFileServerPath.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));
	strFileServerFile = strFileServerPath + _T("\\Ceremony.ini");
	TCHAR* pBuffer = new TCHAR[2048];

	long ret = ::GetPrivateProfileString( strSection, strKeyName, strDefault, pBuffer, 2047, strFileServerFile );

	CString s = pBuffer;
	delete pBuffer;

	if( ret==0 ) return strDefault;
	return s;
}

void ESMSetCeremonyInfoint(CString strKeyName, int nValue)
{
	CString strSection = _T("Ceremony");
	CString strFileServerPath, strFileServerFile, strValue;

	strValue.Format(_T("%d"), nValue);

	strFileServerPath.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));
	strFileServerFile = strFileServerPath + _T("\\Ceremony.ini");

	::WritePrivateProfileString( strSection, strKeyName, strValue, strFileServerFile );
}

CString ESMGetModuleFileName()
{
	TCHAR strFullFilePath[MAX_PATH]={0}, szFile[MAX_PATH]={0}, szExt[5]={0};
	CString strFileName;
	GetModuleFileName(NULL, strFullFilePath, MAX_PATH);
	_tsplitpath(strFullFilePath, NULL, NULL, szFile, szExt);
	strFileName = szFile;
	strFileName.Append(szExt);
	return strFileName;
}


//CMiLRe 20151013 Template Load INI File
void LoadTemplateFile(CString strTemplateFileName, int nNum, BOOL bRecovery)
{
	g_pESMMainWnd->m_wndTimeLineEditor.LoadTemplateFromMvtmFile(strTemplateFileName, nNum);
	
	if(bRecovery)
	{
		g_pESMMainWnd->m_wndTimeLineEditor.LoadTemplateRecovery(strTemplateFileName, nNum);
	}
}


//CMiLRe 20151014 Template Save INI File
void ResetLoadTemplate()
{
	g_pESMMainWnd->m_wndTimeLineEditor.ResetTemplate();	
}

//CMiLRe 20151119 동영상 제작 중 Flag 추가
void SetMakingMovieFlag(BOOL bValue)
{
	g_pESMMainWnd->m_bIsMakeMovie = bValue;
}

//CMiLRe 20151119 동영상 제작 중 Flag 추가
BOOL GetMakingMovieFlag()
{
	return g_pESMMainWnd->m_bIsMakeMovie;
}

int ESMGetRecState()
{
	//-- return MAINFRAME POINT
	return g_pESMMainWnd->m_wndDSCViewer.m_bRecordStep;
}

void ESMSetAdjustPath(CString strPath)
{
	g_pESMMainWnd->m_strAdjustMovieFolder = strPath;
}
CString ESMGetAdjustPath()
{
	return g_pESMMainWnd->m_strAdjustMovieFolder;
}

void ESMSetSelectDSC(CString strDSC)
{
	g_pESMMainWnd->m_strSelectDSC = strDSC;
}

CString ESMGetSelectDSC()
{
	return g_pESMMainWnd->m_strSelectDSC;
}

void ESMSetFirstConnect(BOOL bSet)
{
	g_pESMMainWnd->m_bFirstConnect = bSet;
}

BOOL ESMGetFirstConnect()
{
	return g_pESMMainWnd->m_bFirstConnect;
}

int ESMFolderCount(CString strPath, ESMAdjustMovie& info)
{
	CFileFind find; // file 검색을 위한 클래스
	CString path; 
	CString fname;
	CString pname;
	int res = 1;
	int folderCnt = 0;
	int fildSize;

	path.Format(_T("%s\\*.*"), strPath); 

	find.FindFile((LPCTSTR)path); 
	while(res)
	{
		res = find.FindNextFile();
		fname = find.GetFileName(); // 파일 및 폴더의 이름을 얻어옴
		pname = find.GetFilePath(); // 전체 패스를 얻어옴
		
		if(find.IsDirectory()) // 디렉토리 일때
		{
			if(fname == "." || fname == "..") // find.IsDots() 로도 "." 과 ".."을 구분할수도 있습니다.
				continue;
				
			info.arrFolderList.Add(fname);
			continue;
		}
		else 
			break;

	}
	return folderCnt;
}

int ESMFileCount(CString strPath, ESMAdjustMovie& info)
{
	CFileFind find; // file 검색을 위한 클래스
	CString path; 
	CString fname;
	CString pname;
	int res = 1;
	int fileCnt = 0;
	int fildSize;
	
	path.Format(_T("%s\\*.*"), strPath); 

	find.FindFile((LPCTSTR)path); 
	while(res)
	{
		res = find.FindNextFile();
		fname = find.GetFileName(); // 파일 및 폴더의 이름을 얻어옴
		pname = find.GetFilePath(); // 전체 패스를 얻어옴
		if(res)
		{
			if(find.IsDirectory()) // 디렉토리 일때
			{
				if(fname == "." || fname == "..") // find.IsDots() 로도 "." 과 ".."을 구분할수도 있습니다.
					continue;
				// 디렉토리일때 필요한 작업을 해줍니다.
			}
			else // 파일일때
			{
				info.arrFileList.Add(fname);
				fileCnt++; // 이 부분에서 파일의 갯수를 세어줍니다.
			}
		}
		else
		{
			info.arrFileList.Add(fname);
			fileCnt++;
		}
	}
	return fileCnt;
}

int ESMFileSortInfo(CString strPath, ESMAdjustMovie& info)
{
	CFileFind find; // file 검색을 위한 클래스
	CString path; 
	CString fname;
	CString pname;
	int res = 1;
	int fileCnt = 0;
	int fildSize;
	
	path.Format(_T("%s\\*.*"), strPath); 

	find.FindFile((LPCTSTR)path); 
	while(res)
	{
		res = find.FindNextFile();
		fname = find.GetFileName(); // 파일 및 폴더의 이름을 얻어옴
		pname = find.GetFilePath(); // 전체 패스를 얻어옴
		
		if(find.IsDirectory()) // 디렉토리 일때
		{
			if(fname == "." || fname == "..") // find.IsDots() 로도 "." 과 ".."을 구분할수도 있습니다.
				continue;
			// 디렉토리일때 필요한 작업을 해줍니다.
		}
		else // 파일일때
		{
			int nPos = 0;
			CString strNum1, strNum2;
			CString strToken;
			int nIndex = 0;
			while((strToken = fname.Tokenize(_T("_."),nPos)) != "")
			{
				if(nIndex == 1)
				{
					strNum1.Format(_T("%s"), strToken);
					break;
				}
				nIndex++;
			}


			int nCount = info.arrFileList.GetCount();
			if(nCount)
			{
				for(int i = 0; i < nCount; i++)
				{
					nPos = 0;
					nIndex = 0;
					while((strToken = info.arrFileList.GetAt(i).Tokenize(_T("_."),nPos)) != "")
					{
						if(nIndex == 1)
						{
							strNum2.Format(_T("%s"), strToken);

							if(_ttoi(strNum1) < _ttoi(strNum2))
							{
								info.arrFileList.InsertAt(i,fname );
								i = nCount;
								break;
							}
						}
						nIndex++;
					}	
				}

				if( _ttoi(strNum1) > _ttoi(strNum2))
				{
					info.arrFileList.Add(fname); 
				}
			}
			else
				info.arrFileList.Add(fname);

			fileCnt++; // 이 부분에서 파일의 갯수를 세어줍니다.
		}
	}
	return fileCnt;
}

void ESMInitSelectPointNum()
{
	g_pESMMainWnd->m_nSelectPointNum = 0;
}

void ESMSetSelectPointNum(int nNum)
{
	g_pESMMainWnd->m_nSelectPointNum = nNum;
}

int  ESMGetSelectPointNum()
{
	return g_pESMMainWnd->m_nSelectPointNum;
}

void ESMInitViewPointNum()
{
	g_pESMMainWnd->m_nViewPointNum = 0;
}

void ESMSetViewPointNum(int nNum)
{
	g_pESMMainWnd->m_nViewPointNum = nNum;
}

int  ESMGetViewPointNum()
{
	return g_pESMMainWnd->m_nViewPointNum;
}

void* ESMGetMovieMgr()
{
	return (void*)g_pESMMainWnd->m_pESMMovieMgr;
}

//CMiLRe 20160128 카메라 연결 count
void ESMSetTotalCameraCount(int nValue)
{
	g_pESMMainWnd->m_CarmerTotalCount = nValue;
}

//CMiLRe 20160128 카메라 연결 count
int ESMGetTotalCameraCount()
{
	return g_pESMMainWnd->m_CarmerTotalCount;
}

void ESMSetCeremonyDBConnect(BOOL bConnect)
{
	g_pESMMainWnd->m_bDBConnect = bConnect;
}

BOOL ESMGetCeremonyDBConnect()
{
	return g_pESMMainWnd->m_bDBConnect;
}

int ESMGetFrameMargin()
{
	return g_pESMMainWnd->m_nFrameMargin;
}

vector<ESMFrameArray*> ESMGetFrameArray()
{
	return g_pESMMainWnd->m_pImgArray;
}

void ESMInitFrameArray(int nWidth, int nHeight, int ori_col, int ori_row)
{
	for(int i = 0; i < g_pESMMainWnd->m_pImgArray.size(); i++)
	{
		IplImage* img = (IplImage*)g_pESMMainWnd->m_pImgArray.at(i)->pImg;
		g_pESMMainWnd->m_pImgArray.at(i)->nResizeX = nWidth;
		g_pESMMainWnd->m_pImgArray.at(i)->nResizeY = nHeight;

		//if(!img)
		//	cvReleaseImage(&img);

		g_pESMMainWnd->m_pImgArray.at(i)->bLoad = FALSE;

		if(ESMGetGPUDecodeSupport())  //GTX 1060 Use
		{
			g_pESMMainWnd->m_pImgArray.at(i)->pYUV = NULL;
			
			if(g_pESMMainWnd->m_pImgArray.at(i)->pImg != NULL)
			{
				delete[] g_pESMMainWnd->m_pImgArray.at(i)->pImg;
				g_pESMMainWnd->m_pImgArray.at(i)->pImg = NULL;
			}
		}
		else
		{
			g_pESMMainWnd->m_pImgArray.at(i)->pYUV = NULL;

			if(g_pESMMainWnd->m_pImgArray.at(i)->pImg != NULL)
			{
				delete[] g_pESMMainWnd->m_pImgArray.at(i)->pImg;
				g_pESMMainWnd->m_pImgArray.at(i)->pImg = NULL;
			}
		}
	}
}

void ESMSetFrameDecodingFlag(BOOL bFlag)
{
	g_pESMMainWnd->m_bFrameDecoding = bFlag;
}

BOOL ESMGetFrameDecodingFlag()
{
	return g_pESMMainWnd->m_bFrameDecoding;
}

void ESMSetMovieGOPMode()
{
	g_pESMMainWnd->m_wndProperty.OnSetGOPMode();
}
extern void ESMSetTrackInfo(track_info tinfo)
{
	g_pESMMainWnd->SetTrackinfo(tinfo);
}
extern track_info ESMGetTrackInfo()
{
	return g_pESMMainWnd->GetTrackinfo();
}
/*
extern axis_info ESMGetAxisInfo()
{
	return g_pESMMainWnd->GetAxisInfo();
}
extern void ESMSetAxis_Info(axis_info ainfo)
{
	g_pESMMainWnd->SetAxisInfo(ainfo);
}

extern axis_info1 ESMGetAxis_Info1()
{
	return g_pESMMainWnd->GetAxisInfo1();
}
extern void ESMSetAxis_Info1(axis_info1 ainfo1)
{
	g_pESMMainWnd->SetAxisInfo1(ainfo1);
}

extern ESMAutoATRemote ESMGetATStart()
{
	return g_pESMMainWnd->GetATStart();
}
extern void ESMSetATStart(ESMAutoATRemote ATset)
{
	g_pESMMainWnd->SetATStart(ATset);
}*/
extern BOOL ESMGetFrameAdjust()
{
	return g_pESMMainWnd->GetFrameAdjust();
}
extern void ESMSetFrameAdjust(BOOL mSet)
{
	g_pESMMainWnd->SetFrameAdjust(mSet);
}

BOOL ESMGetLoadingFrame()
{
	return g_pESMMainWnd->m_bLoadFrame;
}
void ESMSetLoadingFrame(BOOL bSet)
{
	g_pESMMainWnd->m_bLoadFrame = bSet;
}

void ESMDisconnect()
{
	g_pESMMainWnd->m_wndNetwork.DisConnectAll();
	
}

void ESMDSCRemoveAll()
{
	g_pESMMainWnd->m_wndDSCViewer.m_arDSCItem.RemoveAll();	
}

//jhhan
void ESMSet4DMPath(CString strPath)
{
	g_pESMMainWnd->m_str4DMPath = strPath;
}
CString ESMGet4DMPath()
{
	return g_pESMMainWnd->m_str4DMPath;
}

void ESMSetLoadRecord(BOOL bSet)
{
	g_pESMMainWnd->m_bLoadRecord = bSet;
	
	//jhhan 190103
	g_pESMMainWnd->m_bLoadPoint = !bSet;
	
}

BOOL ESMGetLoadRecord()
{
	return g_pESMMainWnd->m_bLoadRecord;
}

void ESMDSCRemoveAllList()
{
	g_pESMMainWnd->m_wndDSCViewer.RemoveListAll();	
}

void ESMGetPropertyListView(CString strDscID)
{
	g_pESMMainWnd->m_wndDSCViewer.GetPropertyListView(strDscID);
}

void ESMReloadCamList()
{
	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_VIEW_TIMELINE_RELOAD;
	::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
}

CString ESMGetPropertyValue(int nValue)
{
	return g_pESMMainWnd->m_wndProperty.m_pPropertyCtrl->GetModifyProperty(nValue);
}

extern void ESMSetPropertyValue( int nValue, CString str )
{
	return g_pESMMainWnd->m_wndProperty.m_pPropertyCtrl->SetModifyProperty(nValue, str);
}

//jhhan 16-10-11
extern vector<CString> ESMGetMakeDeleteDSC()
{
	return g_pESMMainWnd->GetMakeDeleteDSC();
}
extern vector<CString> ESMGetMakeDeleteDSCList()
{
	return g_pESMMainWnd->GetMakeDeleteDSCList();
}
extern void ESMSetMakeDeleteDSCList(vector<CString> m_list)
{
	g_pESMMainWnd->SetMakeDeleteDSCList(m_list);
}

extern BOOL ESMGetMakeDelete(CString str)
{
	return g_pESMMainWnd->MakeDeleteDSCList(str);
}

extern void ESMSendFrameDataInfo()
{
	if(g_pESMMainWnd->m_wndNetwork.m_pRCClient)
	{
		if(!(g_pESMMainWnd->m_wndDSCViewer.m_pFrameSelector->GetDSCRecStatus() == DSC_REC_ON))
			g_pESMMainWnd->m_wndNetwork.m_pRCClient->SendFrameInfoOpenClose(2);
		else
			g_pESMMainWnd->m_wndNetwork.m_pRCClient->SendFrameInfoOpenClose(1);
	}
}

extern void ESMSendFrameDataInfo(BOOL bOpen)
{
	if(g_pESMMainWnd->m_wndNetworkEx.m_pRCClient)
	{
		if(!bOpen)
			g_pESMMainWnd->m_wndNetworkEx.m_pRCClient->SendFrameInfoOpenClose(2);
		else
			g_pESMMainWnd->m_wndNetworkEx.m_pRCClient->SendFrameInfoOpenClose(1);
	}
}

extern CString ESMGetDSCIDFromPath(CString strPath)
{
#if 0
	CString strDscId;
	CString strFramePath = strPath;
	int nIndex = strFramePath.ReverseFind('\\') + 1;
	CString strTpPath = strFramePath.Right(strFramePath.GetLength() - nIndex);
	strDscId = strTpPath.Left(5);
	CString strCamID = strDscId;
#else
	int nFind = strPath.ReverseFind('\\') + 1;
	CString csFile = strPath.Right(strPath.GetLength()-nFind);
	csFile.Trim();
	//csFile.Trim(_T("\\"));
	nFind = csFile.Find('.');
	CString csCam = csFile.Left(nFind);
	csCam.Trim();
	nFind = csCam.Find('_');

	CString strCamID = csCam.Mid(0, nFind);
#endif

	return strCamID;
}

extern void ESMSetAdjustLoad(BOOL bLoad)
{
	g_pESMMainWnd->m_bLoadAdj = bLoad;
}
extern BOOL ESMGetAdjustLoad()
{
	return g_pESMMainWnd->m_bLoadAdj;
}

//jhhan 16-11-21 LastPointNum
extern void ESMInitLastSelectPointNum()
{
	g_pESMMainWnd->m_nLastSelectPointNum = 0;
}
extern void ESMSetLastSelectPointNum(int nNum)
{
	g_pESMMainWnd->m_nLastSelectPointNum = nNum;
}
extern int  ESMGetLastSelectPointNum()
{
	return g_pESMMainWnd->m_nLastSelectPointNum;
}

void ESMSetFrameViewFlag(BOOL bFlag)
{
	g_pESMMainWnd->m_bFlag = bFlag;
}
BOOL ESMGetFrameViewFlag()
{
	return g_pESMMainWnd->m_bFlag;
}

extern BOOL ESMGetViewScaling()
{
	return g_pESMMainWnd->m_wndDSCViewer.m_bScaleToWindow;
}

extern CString ESMGetSecIdxFromPath(CString strPath)
{
	int nFind = strPath.ReverseFind('\\') + 1;
	CString csFile = strPath.Right(strPath.GetLength()-nFind);
	csFile.Trim();
	//csFile.Trim(_T("\\"));
	nFind = csFile.Find('.');
	CString csCam = csFile.Left(nFind);
	csCam.Trim();
	nFind = csCam.ReverseFind('_') + 1;

	CString strSecIdx = csCam.Right(csCam.GetLength()-nFind);
	
	return strSecIdx;
}

extern void ESMInitTitleBar()
{
	g_pESMMainWnd->InitTitleBar();
}

extern int ESMGetMovieMgrCnt()
{
	return g_pESMMainWnd->m_pESMMovieMgr->GetMovieCnt();
}

extern void ESMSet4DAFileInit(CString strSourceDSC)
{
	g_pESMMainWnd->InitFileini(strSourceDSC);
}

extern CString ESMGet4DAPath(CString strDSCId)
{
	return g_pESMMainWnd->Load4DAPath(strDSCId);
}

extern void ESMMovieRTThread(CString strIP, CString strDSC)
{
	g_pESMMainWnd->RunESMMovieRTTThread(strIP, strDSC);
}

making_info* ESMGetMakingInfo()
{
	return &g_pESMMainWnd->m_makingInfo;
}
void ESMSetMakingInfo(making_info info)
{
	g_pESMMainWnd->m_makingInfo = info;
}

BOOL ESMGetMaingInfoFlag()
{
	return g_pESMMainWnd->m_bMakingInfo;
}

void ESMSetMaingInfoFlag(BOOL bFlag)
{
	g_pESMMainWnd->m_bMakingInfo = bFlag;
}
void ESMSetGridYPoint(int nY)
{
	g_pESMMainWnd->m_nGridYPoint = ((nY * 15/*20*/) + 60/* + 10*/) - ESMGetScroll();
}
int	ESMGetGridYPoint()
{
	return g_pESMMainWnd->m_nGridYPoint;
}
BOOL ESMGetGPUCheck()
{
	return g_pESMMainWnd->m_bCheckGPU;
}
void ESMSetGPUCheck(BOOL bSet)
{
	g_pESMMainWnd->m_bCheckGPU = bSet;
};

void ESMInitCheckFrame()
{
	EnterCriticalSection(&g_pESMMainWnd->m_criRecord);
	if(g_pESMMainWnd->m_mapRecordFrame.GetCount() > 0)
		g_pESMMainWnd->m_mapRecordFrame.RemoveAll();
	LeaveCriticalSection(&g_pESMMainWnd->m_criRecord);
}
void ESMSetCheckFrame(CString strKey)
{
	EnterCriticalSection(&g_pESMMainWnd->m_criRecord);
	UINT nValue = 100;

	int nFind = strKey.ReverseFind('\\') + 1;
	CString _strKey = strKey.Right(strKey.GetLength()-nFind);

	g_pESMMainWnd->m_mapRecordFrame.SetAt(_strKey, nValue);
	LeaveCriticalSection(&g_pESMMainWnd->m_criRecord);
}
BOOL ESMGetCheckFrame(CString strKey)
{
	BOOL bRet = FALSE;
	UINT nValue = 0;
	EnterCriticalSection(&g_pESMMainWnd->m_criRecord);

	int nFind = strKey.ReverseFind('\\') + 1;
	CString _strKey = strKey.Right(strKey.GetLength()-nFind);

	g_pESMMainWnd->m_mapRecordFrame.Lookup(_strKey, nValue);
	if(nValue == 100)
		bRet = TRUE;
	LeaveCriticalSection(&g_pESMMainWnd->m_criRecord);

	return bRet;
}

void ESMSetDump(BOOL bSet)
{
	g_pESMMainWnd->RunESMDump(bSet);
}
//(CAMREVISION)
void ESMSetRecordStatus(BOOL bSet)
{
	for(int i = 0 ; i < 10 ; i++)
	{
		if(bSet)
			ESMLog(5,_T("Recording Start"));
		else
			ESMLog(5,_T("Recording Finish"));
	}
	
	g_pESMMainWnd->m_bStart = bSet;
}
BOOL ESMGetRecordStatus()
{
	return g_pESMMainWnd->m_bStart;
}

void ESMSetNextRecordStatus(BOOL bSet)
{
	g_pESMMainWnd->m_bNextStatus = bSet;
}
BOOL ESMGetNextRecordStatus()
{
	return g_pESMMainWnd->m_bNextStatus;
}

CString ESMGetDeleteFolder()
{
	CString strDelete;
	
	g_mutexFrame.Lock();
	//strDelete = g_pESMMainWnd->m_strDeleteFolder[RECORD_STORED_COUNT -1];

	strDelete = g_pESMMainWnd->m_strDeleteFolder;
	g_mutexFrame.Unlock();
	
	return strDelete;
}
void ESMSetDeleteFolder(CString strFolder)
{
	if(strFolder.IsEmpty())
		ESMSetFrameRecord(strFolder);

	g_mutexFrame.Lock();
	/*for(int i = 0; i < RECORD_STORED_COUNT - 1; i++)
	{
		g_pESMMainWnd->m_strDeleteFolder[RECORD_STORED_COUNT -1 -i] = g_pESMMainWnd->m_strDeleteFolder[RECORD_STORED_COUNT -2 -i];
	}
	g_pESMMainWnd->m_strDeleteFolder[0] = strFolder;*/

	g_pESMMainWnd->m_strDeleteFolder = strFolder;
	g_mutexFrame.Unlock();
}
void ESMSetSenderGap(int nTime)
{
	g_pESMMainWnd->m_nSenderGap = nTime;
}
int	ESMGetSenderGap()
{
	return g_pESMMainWnd->m_nSenderGap;
}

void ESMSetMakeDeleteDSC(CString strDSC,BOOL bDelete/* = FALSE*/)
{
	g_pESMMainWnd->SetMakeDelete(strDSC,bDelete);
}

void ESMSetCurrentTemplateVector(vector<TEMPLATE_STRUCT> templateVector)
{
	g_pESMMainWnd->m_wndTimeLineEditor.m_pLoadTemplateVector = templateVector;
}

vector<TEMPLATE_STRUCT> ESMGetCurrentTemplateVector()
{
	return g_pESMMainWnd->m_wndTimeLineEditor.m_pLoadTemplateVector;	
}

void ESMSetFrameRate(int nFPS)
{
	//

	//180119
	if(g_pESMMainWnd->m_nFPS == nFPS)
		return;

	g_pESMMainWnd->m_nFPS = nFPS;

	//joonho.kim 17.07.22
	g_pESMMainWnd->SetFrameRate(nFPS);

	//jhhan 170713 / 25P

	if(nFPS == MOVIE_FPS_UHD_25P || nFPS == MOVIE_FPS_FHD_25P)
	{
		movie_frame_per_second = 25/*25*/;
		movie_next_frame_time = 40;
	}
	else if(nFPS == MOVIE_FPS_FHD_50P || nFPS == MOVIE_FPS_UHD_50P) //180308
	{
		movie_frame_per_second = 50;
		movie_next_frame_time = 20;
	}
	else if(nFPS == MOVIE_FPS_FHD_60P || nFPS == MOVIE_FPS_UHD_60P_X2 || nFPS == MOVIE_FPS_UHD_60P_X1 || nFPS == MOVIE_FPS_UHD_60P)
	{
		movie_frame_per_second = 60;
		movie_next_frame_time = 16;
	}
	else if(nFPS == MOVIE_FPS_FHD_120P)
	{
		movie_frame_per_second = 120;
		movie_next_frame_time = 8;
	}
	else
	{
		//MOVIE_FPS_30P
		movie_frame_per_second = 30;
		movie_next_frame_time = 33;
	}
}

int ESMGetFrameRate(BOOL bMode /*= FALSE*/)
{
	if(bMode)
	{
		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT && ESMGetRecordFileSplit() == TRUE)
		{
			if(g_pESMMainWnd->m_nFPS == MOVIE_FPS_UHD_30P_X2)
			{
				 return MOVIE_FPS_UHD_30P_X1;
			}else if(g_pESMMainWnd->m_nFPS == MOVIE_FPS_UHD_60P_X2)
			{
				return MOVIE_FPS_UHD_60P_X1;
			}
		}
	}
	return g_pESMMainWnd->m_nFPS;
}
void ESMSetRecordState(int nSet)
{
	g_pESMMainWnd->m_nRecordState = nSet;
}
int ESMGetRecordState()
{
	return g_pESMMainWnd->m_nRecordState;
}

//wgkim 170728
void ESMSetSelectedFrameNumber(int nSelectedFrame)
{
	g_pESMMainWnd->m_nSelectedFrameNumber = nSelectedFrame;
}

int ESMGetSelectedFrameNumber()
{
	return g_pESMMainWnd->m_nSelectedFrameNumber;
}

map<CString, int> ESMGetArrGapAvg()
{
	return g_pESMMainWnd->m_nArrAvgGap;
}

void ESMSetArrGapAvg(map<CString, int> nArrAvgGap)
{
	g_pESMMainWnd->m_nArrAvgGap = nArrAvgGap;
}

void ESMClearArrGapAvg()
{
	g_pESMMainWnd->m_nArrAvgGap.clear(); 
}

int ESMGetGopSize() //jhhan 180308
{
	//180308
	int nFPS = ESMGetFrameRate();
	int nGopSize = 30;
	//if(nFPS == MOVIE_FPS_UHD_25P || nFPS == MOVIE_FPS_FHD_25P)
	//{
	//	nGopSize = 25;
	//}
	//else if(nFPS == MOVIE_FPS_FHD_50P) // MOVIE_FPS_50P
	//{
	//	nGopSize = 50;
	//}
	//else if(nFPS == MOVIE_FPS_FHD_60P)
	//{
	//	nGopSize = 30;
	//}
	//else if(nFPS == MOVIE_FPS_FHD_120P)
	//{
	//	nGopSize = 30;
	//}
	//else
	//{
	//	//MOVIE_FPS_30P
	//	nGopSize = 30;
	//}

	if(ESMGetDevice() == SDI_MODEL_NX1)
	{
		if(ESMGetFrameRate() == MOVIE_FPS_UHD_25P || ESMGetFrameRate()==MOVIE_FPS_FHD_25P)
		{
			nGopSize = 12;
		}else if (ESMGetFrameRate() == MOVIE_FPS_UHD_30P || ESMGetFrameRate() == MOVIE_FPS_FHD_30P)
		{
			nGopSize = 15;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P)
		{
			nGopSize = 25;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P)
		{
			nGopSize = 30;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_120P)
		{
			nGopSize = 30;
		}
		else
		{
			nGopSize = 30;
		}
	}
	else if(ESMGetDevice() == SDI_MODEL_GH5)
	{
		if(ESMGetFrameRate() == MOVIE_FPS_FHD_30P || ESMGetFrameRate() == MOVIE_FPS_UHD_30P_X1 || ESMGetFrameRate() == MOVIE_FPS_UHD_30P_X2)
		{
			nGopSize = 10;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X1 || ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X2)
		{
			nGopSize = 20;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_25P || ESMGetFrameRate() == MOVIE_FPS_UHD_25P)
		{
			nGopSize = 8;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P || ESMGetFrameRate() == MOVIE_FPS_UHD_50P)
		{
			nGopSize = 16;
		}
		else
		{
			nGopSize = 10;
		}

	}else
	{
		nGopSize = 30;
	}

	return nGopSize;
}

CString ESMGetFrontIP()
{
	CString strData = _T("");
	CString strIP;
	WSADATA wsadata;

	if( !WSAStartup( DESIRED_WINSOCK_VERSION, &wsadata ) )
	{
		if( wsadata.wVersion >= MINIMUM_WINSOCK_VERSION )
		{
			HOSTENT *p_host_info;
			IN_ADDR in;
			char host_name[128]={0, };

			gethostname(host_name, 128);
			p_host_info = gethostbyname( host_name );

			if( p_host_info != NULL )
			{
				for( int i = 0; p_host_info->h_addr_list[i]; i++ )
				{
					memcpy( &in, p_host_info->h_addr_list[i], 4 );
					strIP = inet_ntoa( in );
					
					
					CString strToken, strTemp;
					int nPos = 0, nCount = 0;
					while((strToken = strIP.Tokenize(_T("."),nPos)) != "")
					{
						if(nCount > 2)
							break;

						strTemp.Format(_T("%s."), strToken);
						strData.Append(strTemp);
						nCount++;
					}
				}
			}
		} 
		WSACleanup();
	}

	return strData;
}

BOOL ESMPeekAndPump()
{
	MSG msg;
	while(::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
	{
		if(!AfxGetApp()->PumpMessage())
		{
			::PostQuitMessage(0);
			return FALSE;
		}
	}

	LONG idle = 0;
	while(AfxGetApp()->OnIdle(idle++));
	return TRUE;
}

void ESMSetWait(DWORD dwMillisec)
{
	MSG msg;
	DWORD dwStart;
	dwStart = GetTickCount();
	while(GetTickCount() - dwStart < dwMillisec)
	{
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}


}

extern int ESMGetMarginX()
{
	return g_pESMMainWnd->m_wndDSCViewer.GetMarginX();
}
extern int ESMGetMarginY()
{
	return g_pESMMainWnd->m_wndDSCViewer.GetMarginY();
}

extern void ESMSetEventEmail(CString str)
{
	g_pESMMainWnd->m_strEventEmail = str;
}
extern CString ESMGetEventEmail()
{
	return g_pESMMainWnd->m_strEventEmail;
}

void ESMSetProperty1Step(int nNum)
{
	switch(nNum)
	{
	case ESM_PROPERTY_1SETP_ISO_UP:
		g_pESMMainWnd->m_wndProperty.OnSetISOUp();
		break;
	case ESM_PROPERTY_1SETP_ISO_DOWN:
		g_pESMMainWnd->m_wndProperty.OnSetISODown();
		break;
	case ESM_PROPERTY_1SETP_SS_UP:
		g_pESMMainWnd->m_wndProperty.OnSetShuttersSpeedUp();
		break;
	case ESM_PROPERTY_1SETP_SS_DOWN:
		g_pESMMainWnd->m_wndProperty.OnSetShuttersSpeedDown();
		break;
	case ESM_PROPERTY_1SETP_CT_UP:
		g_pESMMainWnd->m_wndProperty.OnSetCTUp();
		break;
	case ESM_PROPERTY_1SETP_CT_DOWN:
		g_pESMMainWnd->m_wndProperty.OnSetCTDown();
		break;
	default:
		ESMLog(0,_T("ESMSetProperty1Step() Unknown %d"),nNum);
	}
}

BOOL ESMSet4DPMgr(CString strDSCId)
{
	return g_pESMMainWnd->Add4DPMgr(strDSCId);
}

void ESMSetDelete4DPMgr(int nRemoteID/* = 0*/)
{
	g_pESMMainWnd->DeleteAll4DPMgr(nRemoteID);
}

BOOL ESMSet4DPTransmit(CString strPath)
{
	if(ESMGetValue(ESM_VALUE_RTSP))//181001 hjcho
	{
		ESMLog(5,_T("%s input!"),strPath);
		g_pESMMainWnd->m_pMovieRTMovie->SetRTSendPath(strPath);
	}
	else
		return g_pESMMainWnd->RequestData(strPath);
}

BOOL ESMGetNetworkEx()
{
	return g_pESMMainWnd->m_wndNetworkEx.GetOnConnect();
}

//KT 전송 동기화 4DP Sync Time
void ESMSetKTSendSyncTime(int nTime)
{
	ESMLog(5,_T("Recording Start"));
	if(g_pESMMainWnd->m_pMovieRTSend)
	{
		g_pESMMainWnd->m_pMovieRTSend->SetRecordingStatus(TRUE);
	}
	if(ESMGetValue(ESM_VALUE_RTSP))
	{
		ESMLog(5,_T("%s - %d - %d"),ESMGetFrameRecord(),nTime,ESMGetTick());
		g_pESMMainWnd->m_pMovieRTMovie->SetSyncTime(ESMGetFrameRecord(),nTime,ESMGetTick());
		//g_pESMMainWnd->m_pMovieRTMovie->SetSyncTime(nTime,ESMGetTick());
	}
	//g_pESMMainWnd->m_wndNetworkEx.m_pRCClient->m_nKTStopCnt = 999999;
	//g_pESMMainWnd->m_wndNetworkEx.m_pRCClient->m_nKTSendTime = nTime;
	//g_pESMMainWnd->m_wndNetworkEx.m_pRCClient->StartKTSendThread();
}

void ESMSetKTSendStopTime(int nCount)
{
   ESMLog(5,_T("Recording Finish"));
   
   if(ESMGetValue(ESM_VALUE_RTSP))
   {
      if(g_pESMMainWnd->m_pMovieRTMovie)
      {
		  CString str4DM = ESMGetFrameRecord();
		  ESMLog(5,_T("RecordStop: %d - %s"),nCount,str4DM);
         g_pESMMainWnd->m_pMovieRTMovie->SetStopIdx(str4DM,nCount);
      }
   }
   else
   {
      if(g_pESMMainWnd->m_pMovieRTSend)
      {
         g_pESMMainWnd->m_pMovieRTSend->SetRecordingStatus(FALSE);
      }
   }
   
   //ESMLog(5,_T("StopTime: %d"),nCount);
   //g_pESMMainWnd->m_wndNetworkEx.m_pRCClient->m_nKTStopCnt = nCount;
}

int  ESMGetKTSendSyncTime()
{
	return g_pESMMainWnd->m_wndNetworkEx.m_pRCClient->m_nKTSendTime;
}
extern void ESMMovieRTSend(CString strIP, CString strDSC,BOOL bRefereeMode /*= FALSE*/)
{
	g_pESMMainWnd->RunESMMovieRTSend(strIP, strDSC,bRefereeMode);
}
extern void ESMMovieRTSend60p(CString strIP, CString strDSC)
{
	g_pESMMainWnd->RunESMMovieRTSend60p(strIP, strDSC);
}

BOOL ESMGetRemoteUse()
{
	return g_pESMMainWnd->GetESMOption()->m_4DOpt.bRemote;	
}
CString ESMGetRemoteIP()
{
	return g_pESMMainWnd->GetESMOption()->m_4DOpt.strRemote;
}
BOOL ESMGetRemoteSkip()
{
	return g_pESMMainWnd->GetESMOption()->m_4DOpt.bRemoteSkip;
}
extern int ESMGetFrameCount(CString strPath)
{
	int nCount = -1;
	CT2CA pszConvertedAnsiString (strPath);
	string strDst(pszConvertedAnsiString);	
	cv::VideoCapture vc(strDst);

	if(vc.isOpened())
		nCount = (int)vc.get(cv::CAP_PROP_FRAME_COUNT);
	
	vc.release();
	return nCount;
}
extern CString ESMDoTranscodeAs720p(CString strPath)
{
	CString strCamID  = ESMGetDSCIDFromPath(strPath);
	int nSecIdx = _ttoi(ESMGetSecIdxFromPath(strPath));
	int nCount  = ESMGetFrameCount(strPath);
	
	CString strOutputPath;
	strOutputPath.Format(_T("M:\\Movie\\(HD)%s_%d.mp4"),strCamID,nSecIdx);

	CString strCmd,strOpt;
	strCmd.Format(_T("%s\\bin\\ESMGPUProcess.exe"),ESMGetPath(ESM_PATH_HOME));
#if 0
	CString strSecondPath;
	strSecondPath.Format(_T("M:\\Movie\\(4K)%s_%d.mp4"),strCamID,nSecIdx);
	strOpt.Format(_T("5 %s %s %s %d %d"),strPath,strOutputPath,strSecondPath,0,nCount);
	ESMLog(5,strOpt);
#else
	//AfxMessageBox(strCmd);
	//strOpt.Format(_T("%s %s %d"),strPath,strOutputPath,nCount);
	strOpt.Format(_T("2 %s %s %d %d"),strPath,strOutputPath,0,nCount,960,540);
	//ESMLog(5,strOpt);
#endif
	
	if(nCount != -1)
	{
		//execute shell
		SHELLEXECUTEINFO lpExecInfo;
		lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
		lpExecInfo.lpFile = strCmd;
		lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
		lpExecInfo.hwnd = NULL;  
		lpExecInfo.lpVerb = L"open";
		lpExecInfo.lpParameters = strOpt;
		lpExecInfo.lpDirectory = NULL;
		//lpExecInfo.nShow	=SW_SHOW;
		lpExecInfo.nShow = SW_HIDE; // hide shell during execution
		lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;

		BOOL bState = ShellExecuteEx(&lpExecInfo);

		// wait until the process is finished
		if (lpExecInfo.hProcess != NULL)
		{
			::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
			::CloseHandle(lpExecInfo.hProcess);
		}
		lpExecInfo.lpFile = _T("exit");
		lpExecInfo.lpVerb = L"close";
		lpExecInfo.lpParameters = NULL;

		if(TerminateProcess(lpExecInfo.hProcess,0))
		{
			lpExecInfo.hProcess = 0;
			ESMLog(5,_T("End"));
		}

		ESMLog(5, strOutputPath);

		CESMFileOperation fo;
		if(fo.IsFileExist(strPath))
			fo.Delete(strPath);
		//jhhan 180204
		strPath.Replace(_T("\\Temp"), _T(""));
		MoveFile(strOutputPath, strPath);
		//MoveFile(strSecondPath, strPath);
	}
	return strOutputPath;
}

void ESMSetRemoteSkip(BOOL bSkip)
{
	g_pESMMainWnd->GetESMOption()->m_4DOpt.bRemoteSkip = bSkip;
}
void ESMSetRemoteState(BOOL bState)
{
	//if(ESMGetGPUMakeFile() && ESMGetRemoteSkip() == TRUE)
	//{
	//	CESMUtilRemoteCtrl* pRemoteCtrl = g_pESMMainWnd->GetFrameExWnd()->m_pRemoteCtrlDlg;
	//	//BOOL bCreate = g_pESMMainWnd->m_pRemoteCtrlDlg->m_bCreate;
	//	pRemoteCtrl->SetRecordProcess(bState);
	//	pRemoteCtrl->ClearTemplatePoint();

	//	BOOL bCreate = pRemoteCtrl->m_bCreate;

	//	if(!bCreate)
	//	{
	//		int nCount = g_pESMMainWnd->m_wndNetworkEx.m_ctrlList.GetItemCount();

	//		for(int i = 0 ; i < nCount; i++)
	//		{
	//			//Add DSC to Remote Ctrl
	//			CString strDSCID = g_pESMMainWnd->m_wndNetworkEx.m_ctrlList.GetItemText(i,8);

	//			if(strDSCID.GetLength() == 5)
	//			{
	//				//g_pESMMainWnd->m_pRemoteCtrlDlg->AddDSCFromNetworkEX(strDSCID);
	//				pRemoteCtrl->AddDSCFromNetworkEX(strDSCID);
	//			}
	//		}
	//		//g_pESMMainWnd->m_pRemoteCtrlDlg->CreateRemoteFrameDlg();
	//		pRemoteCtrl->CreateRemoteFrameDlg();

	//		//g_pESMMainWnd->m_pRemoteCtrlDlg->m_bCreate = TRUE;
	//		pRemoteCtrl->m_bCreate = TRUE;
	//	}	

	//	//g_pESMMainWnd->m_pRemoteCtrlDlg->SetRecordInit(bState);
	//	pRemoteCtrl->SetRecordInit(bState);
	//}
}
int ESMGetRecordTime()
{
	CDSCMgr* pDSCMgr = g_pESMMainWnd->m_wndDSCViewer.m_pDSCMgr;

	int nTime = 0;
	if(pDSCMgr)
	{
		nTime = pDSCMgr->GetRecordTime();
	}

	return nTime;
}

CString ESMGetMACAddress(CString strIp)
{
	CString strMac;

	char szHostIP[50];
	char* strId = ESMUtil::CStringToChar(strIp);
	memcpy(szHostIP, strId, sizeof(char) * 50);
	if( strId)
	{
		delete strId;
		strId = NULL;
	}

	struct in_addr srcIp;
	srcIp.s_addr = inet_addr(szHostIP);

	WORD wVersionRequested;
	WSADATA wsaData;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 )
	{
		ULONG MacAddr[2];
		ULONG PhyAddrLen = 6;  /* default to length of six bytes */
		unsigned char mac[6];

		DWORD ret = SendARP((IPAddr)srcIp.S_un.S_addr, 0, MacAddr, &PhyAddrLen);
		if(PhyAddrLen)
		{
			BYTE *bMacAddr = (BYTE *) & MacAddr;
			for (int i = 0; i < (int) PhyAddrLen; i++)
			{
				mac[i] = (char)bMacAddr[i];
			}

			strMac.Format(_T("%.2X-%.2X-%.2X-%.2X-%.2X-%.2X"), mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
		}
		WSACleanup( );
	}

	return strMac;
}

void ESMCmdRemote(UINT nMsg, int nCmd)
{
	ESMEvent* pMsg = NULL;
	
	pMsg = new ESMEvent;
	
	pMsg->message = nMsg;
	pMsg->nParam1 = nCmd;
	
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	
}

void ESMSetInitialTime(int nTime)
{
	g_pESMMainWnd->m_nInitialTime = nTime;
}

int ESMGetInitialTime()
{
	return g_pESMMainWnd->m_nInitialTime;
}

void ESMSetMakeMovieTime(int nTime)
{
	g_pESMMainWnd->m_nMakeEndTime = nTime;
	g_pESMMainWnd->m_wndDashBoard.LoadInfo();
}

int ESMGetMakeMovieTime()
{
	return g_pESMMainWnd->m_nMakeEndTime;
}

void ESMSetStartMakeTime(int nTime, int nGap)
{
	g_pESMMainWnd->m_nMakeStartTime = nTime;
	g_pESMMainWnd->m_nMakeGapTime = nGap;
}

int ESMGetStartMakeTime()
{
	return g_pESMMainWnd->m_nMakeStartTime;
}

int ESMGetGapTime()
{
	return g_pESMMainWnd->m_nMakeGapTime;
}
extern void ESMCalcAdjustDataAsText(CString strCAMID,FILE* pFile,double dRatio)
{
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();

	//FILE* pFile = fopen("M:\\AdjData.txt","w");

	stAdjustInfo AdjustData = pMovieMgr->GetAdjustData(strCAMID);
	
	int nWidth = AdjustData.nWidth * dRatio;
	int nHeight = AdjustData.nHeight * dRatio;

	double dbScale = AdjustData.AdjSize;

	double degree = AdjustData.AdjAngle;
	double dbAngleAdjust = -1*(degree+90);

	double dbRotX = ESMRound(AdjustData.AdjptRotate.x * dRatio);
	double dbRotY = ESMRound(AdjustData.AdjptRotate.y * dRatio);
	double dbMovX = ESMRound(AdjustData.AdjMove.x * dRatio);
	double dbMovY = ESMRound(AdjustData.AdjMove.y * dRatio);

	int nMarginX = pMovieMgr->GetMarginX() * dRatio;
	int nMarginY = pMovieMgr->GetMarginY() * dRatio;

	Mat RotMat(2,3,CV_64FC1);
	RotMat = getRotationMatrix2D(Point2f(dbRotX,dbRotY),dbAngleAdjust,dbScale);
	
	RotMat.at<double>(0,2) += (dbMovX / dbScale);
	RotMat.at<double>(1,2) += (dbMovY / dbScale);

	double dbMarginScale = 1 / ((double)(nWidth - nMarginX * 2)/ nWidth);

	(int)((nWidth  * dbMarginScale - nWidth)/2);
	(int)((nHeight * dbMarginScale - nHeight)/2);

	//RotMatrix
	for(int i = 0 ; i < 2; i++)
		for(int j = 0 ; j < 3; j++)
			fprintf(pFile,"%f\n",RotMat.at<double>(i,j));

	//Resize
	fprintf(pFile,"%d\n",(int)(nWidth  * dbMarginScale));
	fprintf(pFile,"%d\n",(int)(nHeight  * dbMarginScale));

	//Rect Point
	fprintf(pFile,"%d\n",(int)((nWidth  * dbMarginScale - nWidth)/2));
	fprintf(pFile,"%d\n",(int)((nHeight * dbMarginScale - nHeight)/2));

	//OriSize
	fprintf(pFile,"%d\n",nWidth);
	fprintf(pFile,"%d\n",nHeight);
}
extern int ESMRound(double dValue)
{
	int nResult = 0;

	if( dValue > 0.0)
		nResult = int(dValue + 0.5);
	else if( dValue < 0.0)
		nResult = int(dValue - 0.5);
	else
		nResult = 0;

	return nResult;
}

void ESMRemoteRecord(int nData)
{
	ESMCmdRemote(WM_ESM_REMOTE_RECORD, nData);
}

void ESMRemoteRecordStop()
{
	ESMCmdRemote(WM_ESM_REMOTE_RECORD);
}

void ESMRemoteMake(int nData)
{
	ESMCmdRemote(WM_ESM_REMOTE_MAKE, nData);
}

int ESMConvertTime(int nTime)
{
	/*
	1 tick is exactly 256/27MHz -> 256/27,000,000=9.481481
	This tick is generated by hardware of camera. 
	*/

	//int nRet = (double)nTime / (double)0.0948;//9.48us
	int nRet = (double)nTime / (256.0 / 27.0 / 100.0);		//9.481481481481... us
	return nRet;
}
void ESMSetRemoteTime(int nTime)
{
	//if(ESMGetGPUMakeFile() && ESMGetRemoteSkip() == TRUE)
	//{
	//	CESMUtilRemoteCtrl* pRemoteCtrl = g_pESMMainWnd->GetFrameExWnd()->m_pRemoteCtrlDlg;
	//	//BOOL bCreate = g_pESMMainWnd->m_pRemoteCtrlDlg->m_bCreate;

	//	pRemoteCtrl->SetSelectTime(nTime);
	//}
}

int ESMReferenceTime()
{
	int _nMilli = 0;

	if(ESMGetFrameRate() == MOVIE_FPS_UHD_25P || ESMGetFrameRate() == MOVIE_FPS_FHD_25P)
	{
		_nMilli = 960;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_UHD_30P || ESMGetFrameRate() == MOVIE_FPS_FHD_30P || ESMGetFrameRate() == MOVIE_FPS_UHD_30P_X2 || ESMGetFrameRate() == MOVIE_FPS_UHD_30P_X1)
	{
		_nMilli = 957;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P || ESMGetFrameRate() == MOVIE_FPS_UHD_50P)
	{
		_nMilli = 980;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X2 || ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X1 || ESMGetFrameRate() == MOVIE_FPS_UHD_60P)
	{
		_nMilli = 944;

	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_120P)
	{
		_nMilli = 952;
	}
	else
	{
		_nMilli = 957;
	}
	
	return _nMilli;
}

void ESMSetDelayMode( CString strMode)
{
	g_pESMMainWnd->m_wndDSCViewer.m_pListView->m_strDelay = strMode;
}
CString	ESMGetDelayMode()
{
	return g_pESMMainWnd->m_wndDSCViewer.m_pListView->m_strDelay;
}

void ESMSetTemplatePage(CString strPage)
{
	g_pESMMainWnd->m_wndDSCViewer.m_pListView->m_strPage = strPage;
}
CString ESMGetTemplatePage()
{
	return g_pESMMainWnd->m_wndDSCViewer.m_pListView->m_strPage;
}

void ESMSetPropertyCheck(BOOL bStatus)
{
	g_pESMMainWnd->m_bPropertyCheck = bStatus;
}
BOOL ESMGetPropertyCheck()
{
	return g_pESMMainWnd->m_bPropertyCheck;
}

void	ESMSetDelayDirection(CString strDirection)
{
	g_pESMMainWnd->m_strDelayDirection = strDirection;
}
CString	ESMGetDelayDirection()
{
	return g_pESMMainWnd->m_strDelayDirection;
}

BOOL ESMGetProcessorShare()
{
	return g_pESMMainWnd->GetESMOption()->m_4DOpt.bProcessorShare;	
}

int	ESMGetTimeFromIndex(int nMovieIdx,int nFrameIdx)
{
	int nTime;
	int nFPS = ESMGetFrameRate();

	if(nFPS == MOVIE_FPS_UHD_25P || nFPS == MOVIE_FPS_FHD_25P)
	{
		nTime = (nMovieIdx * 1000) + (nFrameIdx - nMovieIdx) * movie_next_frame_time;
	}
	else if(nFPS == MOVIE_FPS_FHD_50P || nFPS == MOVIE_FPS_UHD_50P) // MOVIE_FPS_50P
	{
		nTime = (nMovieIdx/2 * 1000) + 
			(movie_next_frame_time * nFrameIdx + 500 * (nMovieIdx % 2));
	}
	else
	{
		nTime = nMovieIdx * 1000 + nFrameIdx * movie_next_frame_time;
	}

	return nTime;
}

void ESMDeleteFile(CString strPath)
{
	//ESMLog(5, strPath);
	DeleteFiles(strPath);
}

int ESMGetFuncKey()
{
	int nKey = VK_F1;

	nKey = g_pESMMainWnd->GetESMOption()->m_4DOpt.nFuncKey;


	/*CString strFile = g_pESMMainWnd->GetESMOptionFile();
	CESMIni ini;
	if(ini.SetIniFilename (g_pESMMainWnd->GetESMOptionFile()))
	{
		nKey = ini.GetInt(INFO_SECTION_RC, INFO_RC_FUNC_KEY);
	}*/
	return nKey;
}
extern void ESMSetFuncKey(int nKey /*= VK_F1*/)
{
	g_pESMMainWnd->GetESMOption()->m_4DOpt.nFuncKey = nKey;

	//g_pESMMainWnd->GetESMOption()->Appl

	CESMIni ini;
	if(ini.SetIniFilename (g_pESMMainWnd->GetESMOptionFile()))
	{
		ini.WriteInt(INFO_SECTION_RC, INFO_RC_FUNC_KEY,	nKey);
	}
}

BOOL ESMGetNumValidate(CString str)
{
	str.Trim();
	if(str.SpanIncluding(_T(".-0123456789")) == str)
		return TRUE;

	return FALSE;
}

//wgkim 171222
extern void* ESMGetBackupMgr()
{
	return (void*)g_pESMMainWnd->m_pBackupMgr;
}
void ESMSetViewFocusAsRemote()
{
	//if(ESMGetGPUMakeFile() == TRUE && ESMGetRemoteSkip() == TRUE)
	//{
		//CESMFrameViewerEx* pFrameEx = g_pESMMainWnd->GetFrameExWnd();
		//pFrameEx->SetFocus();
		//pFrameEx->SetFocusAsFrame();
		/*CESMUtilRemoteCtrl* pRemoteCtrl = g_pESMMainWnd->GetFrameExWnd()->m_pRemoteCtrlDlg;
		if(pRemoteCtrl != NULL)
		{
			pRemoteCtrl->SetFocusFromMainFrm();
		}*/
	//}
}

void ESMDeleteMovie()
{
#ifdef _4DP_FRAME_DELETE
	CString strLocal;
	strLocal = ESMGetLocalIP();
	if(!strLocal.IsEmpty())
	{
		CESMIni ini;
		CString strFile;
		strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_EX);
		if(ini.SetIniFilename (strFile))
		{
			CStringArray ArrIp;
			ESMUtil::GetLocalIPAddressArray(ArrIp);
			if(ArrIp.GetSize() > 0)
			{
				CString strIP, strDscId;

				for(int i=0; i<ArrIp.GetSize(); i++)
				{
					strIP = ArrIp.GetAt(i);
					
					strDscId = ini.GetString(strIP, _T("SourceDSC"));
					if(strDscId.IsEmpty())
					{
						continue;
					}
					else
					{
						CESMFileOperation fo;
						CString strPath;
						strPath.Format(_T("%s"),ESMGetClientRamPath());
						fo.Delete(strPath, FALSE);
						break;
					}

				}
			}
		}
	}
#else
	CESMFileOperation fo;
	CString strPath;
	strPath.Format(_T("%s"),ESMGetClientRamPath());
	fo.Delete(strPath, FALSE);
#endif
}

//wgkim 180202
void ESMSetRecordFileSplit(BOOL bRecordFileSplit)
{
	//g_pESMMainWnd->m_bRecordFileSplit = bRecordFileSplit;
	g_pESMMainWnd->GetESMOption()->m_4DOpt.bRecordFileSplit = bRecordFileSplit;
}
BOOL ESMGetRecordFileSplit()
{
	//return g_pESMMainWnd->m_bRecordFileSplit;
	return g_pESMMainWnd->GetESMOption()->m_4DOpt.bRecordFileSplit;
}

extern void ESMReStartAgent()
{
	g_pESMMainWnd->ReStartAgent();
}

void ESMSetDevice(int nDevice)
{
	g_pESMMainWnd->m_nDevice = nDevice;
}
int ESMGetDevice()
{
	return g_pESMMainWnd->m_nDevice;
}

int ESMGetFramePerSec()
{
	int nFrameRate = 0;
	switch(ESMGetFrameRate())
	{
	case MOVIE_FPS_UHD_25P: nFrameRate = 25; break;
	case MOVIE_FPS_UHD_30P: nFrameRate = 30; break;
	case MOVIE_FPS_FHD_50P: nFrameRate = 50; break;
	case MOVIE_FPS_FHD_30P: nFrameRate = 30; break;
	case MOVIE_FPS_FHD_25P: nFrameRate = 25; break;
	case MOVIE_FPS_FHD_60P: nFrameRate = 60; break;
	case MOVIE_FPS_FHD_120P: nFrameRate = 120; break;
	case MOVIE_FPS_UHD_30P_X2: nFrameRate = 30; break;
	case MOVIE_FPS_UHD_60P_X2: nFrameRate = 60; break;
	case MOVIE_FPS_UHD_30P_X1: nFrameRate = 30; break;
	case MOVIE_FPS_UHD_60P_X1: nFrameRate = 60; break;
	case MOVIE_FPS_UHD_60P: nFrameRate = 60; break;
	default: nFrameRate = 30; break;
	}

	return nFrameRate;
}

void ESMSetIdentify(int nId)
{
	int nDevice = nId % 100;
	int nMode = nId / 100;

	ESMSetDevice(nDevice);
	ESMSetFrameRate(nMode);
}

int ESMGetIdentify()
{
	int nDevice = ESMGetDevice();
	int nMode = ESMGetFrameRate() * 100;
	return nDevice + nMode;
}

int ESMGetDivFrame()
{
	int _nFrameCount = ESMGetValue(ESM_VALUE_DIV_FRAME) /*15*/;						//Default : 15
	int _nDiv = 0;
	if(ESMGetDevice() == SDI_MODEL_NX1)
	{
		if(ESMGetFrameRate() == MOVIE_FPS_UHD_25P || ESMGetFrameRate()==MOVIE_FPS_FHD_25P)
		{
			_nDiv = 12;
		}else if (ESMGetFrameRate() == MOVIE_FPS_UHD_30P || ESMGetFrameRate() == MOVIE_FPS_FHD_30P)
		{
			_nDiv = 15;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P)
		{
			_nDiv = 25;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P)
		{
			_nDiv = 30;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_120P)
		{
			_nDiv = 30;
		}
		else
		{
			_nDiv = 15;
		}
	}
	else if(ESMGetDevice() == SDI_MODEL_GH5)
	{
		if(ESMGetFrameRate() == MOVIE_FPS_FHD_30P || ESMGetFrameRate() == MOVIE_FPS_UHD_30P || ESMGetFrameRate() == MOVIE_FPS_UHD_30P_X1 || ESMGetFrameRate() == MOVIE_FPS_UHD_30P_X2)
		{
			_nDiv = 10;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X1 || ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X2)
		{
			_nDiv = 20;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_25P || ESMGetFrameRate() == MOVIE_FPS_UHD_25P)
		{
			_nDiv = 8;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P || ESMGetFrameRate() == MOVIE_FPS_UHD_50P)
		{
			_nDiv = 16;
		}
		else
		{
			_nDiv = 10;
		}

	}else
	{
		_nDiv = 10;
	}

	if(_nFrameCount % _nDiv != 0)
	{
		_nFrameCount = _nDiv;
	}

	ESMSetValue(ESM_VALUE_DIV_FRAME, _nFrameCount);

	return _nFrameCount;
}
extern void ESMVerifyInfo()
{
	//if(AfxMessageBox(_T("2018년 2월 이전 날짜만 사용 가능합니다.\n적용하시겠습니까?"),MB_YESNO)==IDYES)
	{
		if(g_pESMMainWnd->m_wndDSCViewer.m_arDSCItem.GetCount() <= 0)
		{
			ESMLog(5,_T("No Camera List"));
			return;
		}

		CDSCItem *pItem = (CDSCItem *)(g_pESMMainWnd->m_wndDSCViewer.m_arDSCItem.GetAt(0));
		CString strDeviceInfo = pItem->GetDSCInfo(DSC_INFO_MODEL);
		CString strMovieInfo  = pItem->GetDSCProp(DSC_PROP_MOVIE_SIZE);

		for(int i = 0 ; i < DSC_PROP_CNT; i++)
		{
			strMovieInfo = pItem->GetDSCProp(i);

			if(strDeviceInfo == _T("NX1"))
			{
				ESMSetDevice(SDI_MODEL_NX1);

				if(strMovieInfo.Find(_T("UHD 30P")) != -1)
				{
					ESMSetFrameRate(MOVIE_FPS_UHD_30P);//UHD
					break;
				}
				else if(strMovieInfo.Find(_T("FHD 60P")) != -1)
				{
					ESMSetFrameRate(MOVIE_FPS_FHD_60P);//UHD
					break;
				}
				else if(strMovieInfo.Find(_T("FHD 30P")) != -1)
				{
					ESMSetFrameRate(MOVIE_FPS_FHD_30P);//UHD
					break;
				}
				else if(strMovieInfo.Find(_T("UHD 25P")) != -1)
				{
					ESMSetFrameRate(MOVIE_FPS_UHD_25P);//UHD
					break;
				}
				else if(strMovieInfo.Find(_T("FHD 50P")) != -1)
				{
					ESMSetFrameRate(MOVIE_FPS_FHD_50P);//UHD
					break;
				}
				else if(strMovieInfo.Find(_T("FHD 25P")) != -1)
				{
					ESMSetFrameRate(MOVIE_FPS_FHD_25P);//UHD
					break;
				}
			}
			else
			{
				ESMSetDevice(SDI_MODEL_GH5);

				if(ESMGetVersionInfo() <= ESM_4DMAKER_VERSION_10014)
				{
					if(strMovieInfo.Find(_T("4K/30p")) != -1)
					{
						ESMSetFrameRate(MOVIE_FPS_UHD_30P_X2);
						break;
					}
					else if(strMovieInfo.Find(_T("4K/60p")) != -1)
					{
						ESMSetFrameRate(MOVIE_FPS_UHD_60P_X2);
						break;
					}
				}
			}
		}
		ESMLog(5,_T("Verification: %s - %s"),strDeviceInfo,strMovieInfo);
	}
}
extern void ESMSaveVersionInfo(float fVersion)
{
	g_pESMMainWnd->m_flVerInfo = fVersion;
}
extern float ESMGetVersionInfo()
{
	return g_pESMMainWnd->m_flVerInfo;
}
extern void ESMSetMuxSendCnt(int n)
{
	CESMUtilMultiplexerDlg* pMuxDlg = g_pESMMainWnd->m_pMultiplexerDlg;
	pMuxDlg->m_nMuxSendCnt = n;
}
extern void ESMSubMuxSendCnt()
{
	CESMUtilMultiplexerDlg* pMuxDlg = g_pESMMainWnd->m_pMultiplexerDlg;
	pMuxDlg->m_nMuxSendCnt--;
}
extern int ESMGetMuxSendCnt()
{
	return 0;
}

extern void ESMSetCopyObj(void* pObj )
{
	g_pESMMainWnd->m_pCurrentObj = pObj;
}

extern void* ESMGetCopyObj()
{
	return g_pESMMainWnd->m_pCurrentObj;
}
extern void ESMSetMakeTotalFrame(int n)
{
	g_pESMMainWnd->m_wndDashBoard.SetFrameCnt(n);
}
extern int ESMGetMakeTotalFrame()
{
	return g_pESMMainWnd->m_wndDashBoard.GetFrameCnt();
}

extern int ESMGetSelectedTime()
{
	return g_pESMMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();
}

extern void ESMSetSelectedTime(int nTime)
{
	g_pESMMainWnd->m_wndDSCViewer.m_pFrameSelector->SetSelectedTime(nTime);
}

extern BOOL ESMGetSyncObj()
{
	return g_pESMMainWnd->m_bSyncObj;
}
extern void ESMSetSyncObj(BOOL bSync)
{
	g_pESMMainWnd->m_bSyncObj = bSync;
}

extern BOOL ESMGetEditObj()
{
	return g_pESMMainWnd->m_bEditObj;
}
extern void ESMSetEditObj(BOOL bSet)
{
	g_pESMMainWnd->m_bEditObj = bSet;
}

extern void ESMGetDrawPos(vector<CPoint> &ptS, vector<CPoint> &ptE)
{
	ptS = g_pESMMainWnd->m_arrptS;
	ptE = g_pESMMainWnd->m_arrptE;
}
extern void ESMSetDrawPos(CPoint ptS, CPoint ptE)
{
	g_pESMMainWnd->m_arrptS.push_back(ptS);
	g_pESMMainWnd->m_arrptE.push_back(ptE);
}

extern int ESMGetXFromTime(int nTime)
{
	return g_pESMMainWnd->m_wndDSCViewer.m_pFrameSelector->GetXFromTime(nTime);
}

extern int ESMGetYFromLine(int nLine)
{
	return g_pESMMainWnd->m_wndDSCViewer.m_pFrameSelector->GetYFromLine(nLine);
}

extern void ESMDeleteAllDrawPos()
{
	g_pESMMainWnd->m_arrptS.clear();
	g_pESMMainWnd->m_arrptE.clear();
}

BOOL ESMGetFile2Sec()
{
	return g_pESMMainWnd->GetESMOption()->m_4DOpt.bFile2Sec;}

extern void ESMPushTime(int nTime)
{
	g_pESMMainWnd->m_stillImage.PushTime(nTime);
	//g_pESMMainWnd->m_nArrSelectTime.push_back(nTime);
	/*ESMLog(5,_T("[EVENT] %d:%d"),g_pESMMainWnd->m_nArrSelectTime.size(),nTime);
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_ESM_EVENT_FRAME_DECODE;
	pMsg->nParam1 = g_pESMMainWnd->m_nArrSelectTime.size() - 1;
	::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);*/
}
extern void ESMClearPushTime()
{
	g_pESMMainWnd->m_stillImage.ClaerArrTime();
	g_pESMMainWnd->m_stillImage.ClearRamDisk();
}
extern CString ESMGetDSCIDFromIndex(int nIdx)
{
	CString str = g_pESMMainWnd->m_wndDSCViewer.GetDSCIDFromIndex(nIdx);

	return str;
}
extern void ESMCreateStillMovie()
{
	g_pESMMainWnd->m_stillImage.CreateStillMovie();
}
extern void ESMPushSelectTime()
{
	int nMovieIdx = g_pESMMainWnd->m_stillImage.m_nMovieIdx;
	int nFrameIdx = g_pESMMainWnd->m_stillImage.m_nFrameIdx;

	if(nMovieIdx != -1 && nFrameIdx != -1)
		g_pESMMainWnd->m_stillImage.PushTime(nMovieIdx,nFrameIdx);
	//g_pESMMainWnd->m_stillImage.PushTime(nMovieIdx,nFrameIdx);
}
extern void ESMSetPushTime(int nMovieIdx,int nFrameIdx)
{
	g_pESMMainWnd->m_stillImage.m_nMovieIdx = nMovieIdx;
	g_pESMMainWnd->m_stillImage.m_nFrameIdx = nFrameIdx;
}
BOOL CameraGroupLoad(CStringArray& _strGroup)
{
	CFile ReadFile;
	CESMIni ini;
	
	CString strPath;
	strPath.Format(_T("%s\\%s.ini"), ESMGetPath(ESM_PATH_SETUP), _T("CameraGroup"));

	CESMFileOperation fo;
	if(fo.IsFileExist(strPath) != TRUE)
	{
		if(!ReadFile.Open(strPath, CFile::modeCreate | CFile::modeReadWrite))
			return FALSE;

		/*INT iLength = (INT)(ReadFile.GetLength());

		if(iLength == 0)
		{
		char cText = 'A';
		for(int i = 0; i < 5; i++)
		{
		CString strTemp;
		strTemp.Format(_T("%c\r\n"), cText+i);

		char* pstrData = NULL;
		pstrData = ESMUtil::CStringToChar(strTemp);
		ReadFile.Write(pstrData, strlen(pstrData));
		delete[] pstrData;
		pstrData = NULL;
		}
		}*/

		ReadFile.Close();

		if(ini.SetIniFilename(strPath))
		{
			char cText = 'A';
			for(int i = 0; i < 10; i++)
			{
				CString strTemp, strIdx;
				strTemp.Format(_T("%c"), cText+i);
				strIdx.Format(_T("%d"), i+1);
				ini.WriteString(_T("Group"), strIdx, strTemp);
			}
		}
	}
	
	if(ini.SetIniFilename(strPath))
	{
		CStringArray strArSectionName;
		
		strArSectionName.RemoveAll();
		ini.GetSectionNames(&strArSectionName);

		for(int i = 0; i < strArSectionName.GetCount(); i++)
		{
			
			CStringArray strArEntry;
			strArEntry.RemoveAll();

			CString strSection = strArSectionName.GetAt(i);
			if(strSection.CompareNoCase(_T("Group")) != 0)
			{
				continue;
			}
			ini.GetSectionList(strSection, strArEntry);

			for(int j = 0; j < strArEntry.GetCount(); j++)
			{
				CString strEntry = strArEntry.GetAt(j);
				CString _Group;
				_Group = ini.GetString(strSection, strEntry);
				if(_Group.IsEmpty() != TRUE)
				{
					_strGroup.Add(_Group);
				}
			}
		}
	}

	

	return TRUE;
}
void ESMInitDirection()
{
	if(ESMGetValue(ESM_VALUE_NET_MODE) != ESM_MODESTATE_SERVER)
		return;

	CStringArray strArGroup;
	CameraGroupLoad(strArGroup);

	if(g_pESMMainWnd->m_mapDirection.GetCount() > 0)
		g_pESMMainWnd->m_mapDirection.RemoveAll();

	CString strPath;
	strPath.Format(_T("%s\\%s.ini"), ESMGetPath(ESM_PATH_SETUP), _T("CameraGroup"));

	CESMIni ini;
	if(ini.SetIniFilename(strPath))
	{
		CStringArray strArSectionName;
		CString _Group, _Direction;

		strArSectionName.RemoveAll();
		ini.GetSectionNames(&strArSectionName);

		for(int i = 0; i < strArSectionName.GetCount(); i++)
		{
			CStringArray strArEntry;
			strArEntry.RemoveAll();

			CString strSection = strArSectionName.GetAt(i);
			if(strSection.CompareNoCase(_T("Group")) == 0)
			{
				continue;
			}
			ini.GetSectionList(strSection, strArEntry);

			for(int j = 0; j < strArEntry.GetCount(); j++)
			{
				CString strEntry = strArEntry.GetAt(j);

				_Direction = ini.GetString(strSection, strEntry, _T("None"));

				ESMSetDirection(strSection, strEntry, _Direction);
				//ESMLog(5, _T("[%s] %s : %s"), strSection, strEntry, _Direction);
			}
		}
	}
}
BOOL ESMSetDirection(CString strSection, CString strGroup, CString strDrirection)
{
	CString strValue;

	CString strKey;
	strKey.Format(_T("%s_%s"), strSection, strGroup);

	g_pESMMainWnd->m_mapDirection.SetAt(strKey, strDrirection);
	
	return TRUE;
}
CString ESMGetDirection(CString strSection, CString strGroup)
{
	CString strValue;

	CString strKey;
	strKey.Format(_T("%s_%s"), strSection, strGroup);

	g_pESMMainWnd->m_mapDirection.Lookup(strKey, strValue);

	return strValue;
}
extern CString ESMGetAJANetworkIP()
{
	return g_pESMMainWnd->GetESMOption()->m_4DOpt.strAJAIP;
}
extern void ESMLaunchAJANetwork()
{
	//if(ESMGetValue(ESM_VALUE_AJANETWORK))
	g_pESMMainWnd->m_pAJANetwork->ConnectAJA();
}
extern void ESMSendAJANetwork(ESMEvent* pMsg)
{
	if(pMsg == NULL)
		return;

	if(ESMGetConnectAJANetwork())
	{
		//ESMLog(5,_T("[%d]Send Start"),pMsg->message);
		g_pESMMainWnd->m_pAJANetwork->SendToAJA(pMsg);
	}
}
extern void ESMSetConnectAJANetwork(BOOL b)
{
	//if(ESMGetValue(ESM_VALUE_AJANETWORK))
	//{
		g_pESMMainWnd->m_pAJANetwork->m_bConnect = b;
	//}
}
extern BOOL ESMGetConnectAJANetwork()
{
	//if(ESMGetValue(ESM_VALUE_AJANETWORK))
		return g_pESMMainWnd->m_pAJANetwork->m_bConnect;
	//else
	//	return FALSE;
}

//wgkim 180515
extern CString ESMGetFrameViewerSplitVideo4dmPath()
{
	CString strFrameViewerSplitVideoPath = ESMGetServerRamPath();
	strFrameViewerSplitVideoPath.Append(_T("\\FrameViewer\\") + ESMGetFrameRecord() + _T("\\"));
	return strFrameViewerSplitVideoPath;
}

//wgkim 180515
extern CString ESMGetFrameViewerSplitVideoRootPath()
{
	CString strFrameViewerSplitVideoPath = ESMGetServerRamPath();
	strFrameViewerSplitVideoPath.Append(_T("\\FrameViewer\\"));
	return strFrameViewerSplitVideoPath;
}

//180516 hjcho
extern void ESMSetAJAMaking(BOOL b)
{
	g_pESMMainWnd->m_pAJANetwork->SetAJAMaking(b);
}
extern BOOL ESMGetAJAMaking()
{
	return g_pESMMainWnd->m_pAJANetwork->GetAJAMaking();
}

extern BOOL ESMGetCheckIf4DPOrNot(CString strIP)
{
	//Check if 4DA or not
	bool bCheck = FALSE;

	CESMRCManager* pRCMgr = NULL;

	int nNet4DP = g_pESMMainWnd->m_wndNetworkEx.m_arRCServerList.GetCount();
	for ( int i = 0 ;i < nNet4DP; i++)
	{
		pRCMgr = (CESMRCManager *)g_pESMMainWnd->m_wndNetworkEx.m_arRCServerList.GetAt(i);

		if(pRCMgr->GetIP().Compare(strIP) == 0)
			return TRUE;
	}

	return FALSE;
}
extern void ESMSetAJAScreen(BOOL b)
{
	g_pESMMainWnd->m_pAJANetwork->SetAJAScreen(b);
}
extern BOOL ESMGetAJAScreen()
{
	return g_pESMMainWnd->m_pAJANetwork->GetAJAScreen();
}
extern CString ESMGetFileName(CString strPath)
{
	CString str;
	str = strPath.Right(strPath.GetLength() - strPath.ReverseFind(_T('\\')) - 1);

	return str;
}

extern void ESMSetFirstFlag(BOOL bFlag)
{
	g_pESMMainWnd->m_bFirst = bFlag;
}

extern BOOL ESMGetFirstFlag()
{
	return g_pESMMainWnd->m_bFirst;
}

extern void ESMSetAJADivProcess(BOOL b)
{
	g_pESMMainWnd->m_pAJANetwork->m_bDivState = b;
}
extern BOOL ESMGetAJADivProcess()
{
	return g_pESMMainWnd->m_pAJANetwork->m_bDivState;
}
extern double ESMGetEncodingFrameRate()
{
	double dFrameRate = 30.f;

	switch(ESMGetValue(ESM_VALUE_FRAMERATE))
	{
	case 0:
		{
			dFrameRate = 30.f;
		}
		break;
	case 1:
		{
			dFrameRate = 60.f;
		}
		break;
	case 2:
		{
			dFrameRate = 25.f;
		}
		break;
	case 3:
		{
			dFrameRate = 50.f;
		}
		break;
	default:
		{
			dFrameRate = 30.f;
		}
		break;
	}
	
	return dFrameRate;
}

void SortFileName(CStringArray& arrStr)
{
	qsort(arrStr.GetData(), arrStr.GetSize(), sizeof(CString*), CompareFileName);
} 

int CompareFileName(const void* p1, const void* p2)
{

	/*USES_CONVERSION;
	LPCWSTR lpszName1 = A2CW((LPSTR)(LPCSTR)((CString*)(p1))->GetBuffer(0));
	LPCWSTR lpszName2 = A2CW((LPSTR)(LPCSTR)((CString*)(p2))->GetBuffer(0));
	return StrCmpLogicalW(lpszName1, lpszName2);*/

	TCHAR* name1 = ((CString*)(p1))->GetBuffer(0);
	TCHAR* name2 = ((CString*)(p2))->GetBuffer(0);
	int len1 =_tcslen(name1);
	int len2 =_tcslen(name2);
	int nRet = CompareString(LOCALE_USER_DEFAULT, NORM_IGNORECASE, name1, len1, name2, len2);

	if (nRet == CSTR_LESS_THAN)
		return -1;
	if (nRet == CSTR_GREATER_THAN)
		return 1;
	return 0; //nRet == CSTR_EQUAL
}

void ESMSetStoredTime(int nTime)
{
	g_pESMMainWnd->m_nStoredTime.push_back(nTime/1000);
}
vector<int> ESMGetStoredTime()
{
	return g_pESMMainWnd->m_nStoredTime;
}
void ESMInitStoredTime()
{
	g_pESMMainWnd->m_nStoredTime.clear();
}
void ESMMovieRTMovie(CString strIP, CString strDSC,int nIdx,int nFrameRate /*= 30*/)
{
	g_pESMMainWnd->RunESMMovieRTMovie(strIP, strDSC,nIdx,nFrameRate);
}
void ESMSetRTSPMovieSavePath(CString strPath)
{
	CESMMovieRTMovie* pRTMovie = g_pESMMainWnd->m_pMovieRTMovie;
	if(pRTMovie)
	{
		ESMLog(5,_T("%s"),strPath);
		pRTMovie->SetSavePath(strPath);
	}
}

void ESMSetViewJump(CString strData)
{
	g_pESMMainWnd->m_nViewJump.clear();

	BOOL bToken = TRUE;
	int nToken = 0;
	while(bToken)
	{
		CString strValue;
		bToken = AfxExtractSubString(strValue, strData, nToken++, ',');
		if(strValue.IsEmpty() != TRUE)
		{
			g_pESMMainWnd->m_nViewJump.push_back(_ttoi(strValue)-1);
			/*CString _str;
			_str.Format(_T("[%03d]"), _ttoi(strValue));
			m_cmbView.AddString(_str);*/
		}
	}
}
int ESMGetViewJump(int nValue, BOOL bUpDown/* = TRUE*/)
{
	int nJump = nValue;
	if(bUpDown) //VK_PRIOR
	{
		int d = g_pESMMainWnd->m_nViewJump.size();
		//for(int i=0; i<g_pESMMainWnd->m_nViewJump.size(); i++)
		for(int i=d-1; i >= 0; i--)
		{
			int _value = g_pESMMainWnd->m_nViewJump.at(i);
			if(nValue == _value)
			{
				if(i > 0)
				{
					nJump = g_pESMMainWnd->m_nViewJump.at(i-1);
					break;
				}
			}else if(nValue > _value)
			{
				nJump = _value;
				break;
			}
		}
	}else		//VK_NEXT
	{
		for(int i=0; i<g_pESMMainWnd->m_nViewJump.size(); i++)
		{
			int _value = g_pESMMainWnd->m_nViewJump.at(i);
			if(nValue == _value)
			{
				if(i < g_pESMMainWnd->m_nViewJump.size() - 1)
				{
					nJump = g_pESMMainWnd->m_nViewJump.at(i+1);
					break;
				}
				
			}else if(nValue < _value)
			{
				nJump = _value;
				break;
			}

		}
	}
	return nJump;
}

BOOL ESMGetUsageKZone()
{
	if(ESMGetValue(ESM_VALUE_AUTOADJUST_KZONE) && g_pESMMainWnd->m_pAutoAdjustMgr->GetUsageKZone())
		return TRUE;
	else
		return FALSE;
}

void ESMSetRecordSync(BOOL bSync /*= TRUE*/)
{
	g_pESMMainWnd->m_bRecordSync = bSync;
}
BOOL ESMGetRecordSync()
{
	return g_pESMMainWnd->m_bRecordSync;
}

BOOL ESMLoadSelectPoint(int nPoint, CString strProfilePath)
{
	//Load Select Point
	CString strKeyName;
	strKeyName.Format(_T("SelectPointTime%d"), nPoint);

	int nResult = GetPrivateProfileInt( _T("Recording"), strKeyName, 0, strProfilePath );

	if(nResult == 0)
	{
		return FALSE;
	}
	ESMLog(5, _T("[%d] %s : %d"), nPoint, strKeyName, nResult);

	g_pESMMainWnd->m_wndDSCViewer.m_pFrameSelector->SetSelectTimeLine(nResult, nPoint);
	CESMFrameViewer* pFrame = g_pESMMainWnd->GetFrameWnd();
	pFrame->SetSelectedTime(nResult, nPoint);

	ESMSetRecordingInfoint(strKeyName, nResult);
	ESMSetLastSelectPointNum(nPoint);
	ESMSetSelectPointNum(nPoint);
	return TRUE;
}

void ESMLoadSquarePointData()
{
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), TEMPLATE_TEMP_FILE);
	g_pESMMainWnd->m_pTemplateMgr->LoadSquarePointData(strFileName);
	g_pESMMainWnd->m_wndTimeLineEditor.SetTemplateMgr(g_pESMMainWnd->m_pTemplateMgr);
	CESMFrameViewer* pFrame = g_pESMMainWnd->GetFrameWnd();
	pFrame->m_pDlgFrameList->SetTemplateMgr(g_pESMMainWnd->m_pTemplateMgr);
}

void ESMSetLiveViewLogFlag(BOOL b)
{
	g_pESMMainWnd->m_bLiveViewLogFlag = b;
}

int ESMGet4DAPSender(CString strIp)
{
	int nIp = 0;

	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONNECT);
	//-- Load Config File
	CESMIni ini;
	if(ini.SetIniFilename (strFile))
	{
		CString strIPSection;
		CString strIP, strSendIP;

		int nIndex = 0;
		while(1)
		{
			strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);
			strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);
			strSendIP = ini.GetString(_T("4DAP"), strIP);

			if(!strIP.GetLength())
				break;

			if(strSendIP.IsEmpty() != TRUE)
			{
				nIp = ESMGetIDfromIP(strSendIP);
				break;
			}

			nIndex++;
		}			
	}
	return nIp;
}

BOOL ESMMemcpy(void * _Dst, const void * _Src, size_t _Size)
{
	__try
	{
		memcpy(_Dst, _Src, _Size);		
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		
		ESMLog(0, _T("memcpy error"));
		return FALSE;
	}

	return TRUE;
}

BOOL ESMGetDetectInfo(vector<DetectInfo> * _stList, int nDetectTime)
{
	if( ESMGetValue(ESM_VALUE_AUTODETECT) != TRUE)
		return FALSE;

	_stList->clear();

	int nDelay = 0;
	int nSel = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC_SEL);
	if(nSel == 0)
		nDelay = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC);
	else
		nDelay = ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC);

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	if(arDSCList.GetCount())
	{
		for( int i = 0 ;i < arDSCList.GetCount(); i++)
		{
			CDSCItem * pItem = NULL;
			pItem = (CDSCItem *)arDSCList.GetAt(i);

			if(pItem == NULL)
				continue;
			if(pItem->GetDetect())
			{
				int _time = 0;
				CString strGroup = pItem->GetGroup();
				if(strGroup.IsEmpty() != TRUE)
				{
					CString strName, strIdx, strTotal;
					AfxExtractSubString(strName, strGroup, 0, '/');
					AfxExtractSubString(strIdx, strGroup, 1, '/');
					AfxExtractSubString(strTotal, strGroup, 2, '/');
					strName.Trim();
					strIdx.Trim();
					strTotal.Trim();

					int nIdx = _ttoi(strIdx) - 1;		//0 base
					int nTotal = _ttoi(strTotal) - 1;	//0 base

					int nDelayIndex = 0;

					CString strDir;

					if( g_pESMMainWnd->m_DelayDirection == DSC_REC_DELAY_FORWARDDIRECTION || g_pESMMainWnd->m_DelayDirection == DSC_REC_DELAY_DIRECTION_2)
					{
						nDelayIndex = nIdx;
						//2
						strDir = ESMGetDirection(_T("2"), strName);
					}
					else if( g_pESMMainWnd->m_DelayDirection == DSC_REC_DELAY_REVERSEDIRECTION || g_pESMMainWnd->m_DelayDirection == DSC_REC_DELAY_DIRECTION_1)
					{
						nDelayIndex = nTotal - nIdx;
						//1
						strDir = ESMGetDirection(_T("1"), strName);
					}
					else if(g_pESMMainWnd->m_DelayDirection == DSC_REC_DELAY_DIRECTION_3)
					{
						nDelayIndex = nTotal - nIdx;
						//3
						strDir = ESMGetDirection(_T("3"), strName);
					}else if(g_pESMMainWnd->m_DelayDirection == DSC_REC_DELAY_DIRECTION_4)
					{
						nDelayIndex = nIdx;
						//4
						strDir = ESMGetDirection(_T("4"), strName);
					}
					else if(g_pESMMainWnd->m_DelayDirection == DSC_REC_DELAY_DIRECTION_5)
					{
						nDelayIndex = nTotal - nIdx;
						strDir = ESMGetDirection(_T("5"), strName);
					}
					else if(g_pESMMainWnd->m_DelayDirection == DSC_REC_DELAY_DIRECTION_6)
					{
						nDelayIndex = nIdx;
						strDir = ESMGetDirection(_T("6"), strName);
					}


					if(strDir.IsEmpty() != TRUE)
					{
						if(strDir.CompareNoCase(_T("Forward")) == 0)
						{
							nDelayIndex = nIdx;
						}else if(strDir.CompareNoCase(_T("Backward")) == 0)
						{
							nDelayIndex = nTotal - nIdx;
						}
					}
					int _dscDelay = g_pESMMainWnd->m_wndDSCViewer.GetDelayTime(pItem->GetDeviceDSCID());

					_time = nDelayIndex * nDelay + _dscDelay;
					ESMLog(5, _T("Detect ---- %s - %s - %s(nDelayIndex : %d)"), pItem->GetDeviceDSCID(), strDir, strGroup, nDelayIndex);
				}

				DetectInfo _info;
				_stprintf(_info._strId, _T("%s"), pItem->GetDeviceDSCID());
				_info._nTime = nDetectTime - _time;
				//ESMLog(5, _T("Detect : %s"),  _info._strId);
				_stList->push_back(_info);			
			}
		}
	}

	ESMLog(5, _T("ESMGetDetectInfo"));
	
	return TRUE;
}


extern void ESMPropertyDiffCheck()
{
	g_pESMMainWnd->StartPropertyDiffCheck();
}

extern void ESMSetPropertyDiffCount( PropertyDiffCount values )
{
	g_pESMMainWnd->SetPropertyDiffCount(values);
}

extern PropertyDiffCount ESMGetPropertyDiffCount()
{
	return g_pESMMainWnd->GetPropertyDiffCount();
}

extern void ESMPropertyMinMaxInfoCheck()
{
	g_pESMMainWnd->StartPropertyMinMaxInfoCheck();
}

extern void ESMSetPropertyMinMaxInfo( PropertyMinMaxInfo values )
{
	g_pESMMainWnd->SetPropertyMinMaxInfo(values);
}

extern PropertyMinMaxInfo ESMGetPropertyMinMaxInfo()
{
	return g_pESMMainWnd->GetPropertyMinMaxInfo();
}

void ESMSaveProfile()
{
	g_pESMMainWnd->m_FileMgr->MovieDoneSaveProfile(TRUE);
}

vector<ESMFrameArray*> * ESMGetFrame(CString strKey)
{
	vector<ESMFrameArray*> *pData = NULL;
	
	pData = g_pESMMainWnd->GetFrame(strKey);

	if(pData == NULL)
	{
		g_pESMMainWnd->InsertFrame(strKey);

		pData = g_pESMMainWnd->GetFrame(strKey);
	}
	
	//ESMLog(5, _T("%s"), _T("ESMGetFrame"));
	return pData;
}

void ESMVerfyMake()
{
	if(ESMGetValue(ESM_VALUE_TEST_MODE))
	{

		//WaitTime

		while(1)
		{
			if(GetTickCount() - ESMGetStartMakeTime() > ESMGetGapTime())
			{
				ESMSetMakeMovieTime(GetTickCount() - ESMGetStartMakeTime());
				break;
			}

			Sleep(100);
		}
	}
	else
		ESMSetMakeMovieTime(GetTickCount() - ESMGetStartMakeTime());
}

extern void ESMSetDefaultCameraID( CString strID )
{
	g_pESMMainWnd->m_strDefaultCameraID = strID;
}

extern CString ESMGetDefaultCameraID()
{
	return g_pESMMainWnd->m_strDefaultCameraID;
}

void ESMM3u8Reset()
{
	g_pESMMainWnd->m_wndDSCViewer.m_pDSCMgr->ResetM3u8();
	
	CESMFileOperation fo;

	CString strTxt;
	strTxt.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("hd"));
	fo.DeleteEx(strTxt, FALSE, _T("*.ts"));
	fo.DeleteEx(strTxt, FALSE, _T("*.jpg"));
}

//jhhan 190131
vector<CString> ESMGetLiveList()
{
	return g_pESMMainWnd->GetLiveList();
}
void ESMSetLiveList(vector<CString> m_list)
{
	g_pESMMainWnd->SetLiveList(m_list);
}

void ESMSendToLiveList()
{
	g_pESMMainWnd->InitLiveListParser();
}

void ESMTranscoding(CString strFile, int nMode, void * pM3u8)
{
	g_pESMMainWnd->OnEncoder(strFile, nMode, pM3u8);
	//CString strPath;

	//if(nMode == 0)//hd
	//{
	//	strPath.Format(_T("%s\\%s\\"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("hd"));

	//	CESMEncoderForReferee* pEncoderForReferee = new CESMEncoderForReferee(
	//		strFile, strPath,
	//		CESMEncoderForReferee::ENCODING_SIZE_HD);

	//	pEncoderForReferee->Run(TRUE);

	//	delete pEncoderForReferee;

	//}else if(nMode == 1)//fhd
	//{
	//	strPath.Format(_T("%s\\%s\\"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("fhd"));

	//	CESMEncoderForReferee* pEncoderForReferee = new CESMEncoderForReferee(
	//		strFile, strPath,
	//		CESMEncoderForReferee::ENCODING_SIZE_FHD);

	//	pEncoderForReferee->Run();

	//	delete pEncoderForReferee;
	//}else if(nMode == 2)//uhd
	//{
	//	strPath.Format(_T("%s\\%s\\"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("uhd"));

	//	CESMEncoderForReferee* pEncoderForReferee = new CESMEncoderForReferee(
	//		strFile, strPath,
	//		CESMEncoderForReferee::ENCODING_SIZE_UHD);

	//	pEncoderForReferee->Run();

	//	delete pEncoderForReferee;
	//}else
	//{
	//	/*CESMEncoderForReferee* pEncoderForReferee = new CESMEncoderForReferee(
	//	strFile, _T("M:\\Movie\\"),
	//	CESMEncoderForReferee::ENCODING_SIZE_HD);

	//	pEncoderForReferee->Run();

	//	delete pEncoderForReferee;*/
	//	ESMLog(0, _T("ESMTranscoding : %d"), nMode);
	//}
}
void ESMCreateM3u8(CString strKey)
{
	g_pESMMainWnd->MakeFhdM3u8(strKey);
	g_pESMMainWnd->MakeUhdM3u8(strKey);
}

void * ESMGetM3u8(CString strKey, int nMode/* = 0*/)
{
	CESMm3u8 * pData = NULL;
	if(nMode == 0 ) //fhd
	{
		pData = g_pESMMainWnd->GetFhdM3u8(strKey);
	}else			//uhd
	{
		pData = g_pESMMainWnd->GetUhdM3u8(strKey);
	}

	return (void *)pData;
}

void ESMDeleteAllM3u8()
{
	g_pESMMainWnd->DeleteAllM3u8();
}
void ESMResetAllM3u8()
{
	g_pESMMainWnd->ResetAllM3u8();
}
void ESMLaunchFFMPEGConsole(CString strOpt)
{
	CString strCmd;
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
}
void ESMRTSPStateUpdate(CString str4DM,int nIndex,int nType,int nData)
{
	if(nType == RTSP_MESSAGE_STATE_PLAY_IN)
	{
		//ESMLog(5,_T("[%s][%d] - %d"),str4DM,nIndex,nData);
		g_pESMMainWnd->m_pRTSPStateViewDlg->SetRTSPPlay(str4DM,nIndex,nData);
	}
	else if(nType == RTSP_MESSAGE_STATE_FRAME_COUNT)
	{
		g_pESMMainWnd->m_pRTSPStateViewDlg->SetRTSPFrameCount(str4DM,nIndex,nData);
	}
	else
		g_pESMMainWnd->m_pRTSPStateViewDlg->Update4DAPState(str4DM,nIndex,nType,nData);
}
void ESMSetRTSPCommand(RTSP_RESTFul_COMMAND command,int nCount)
{
	if(ESMGetValue(ESM_VALUE_RTSP))
	{
		g_pESMMainWnd->m_pRTSPStateViewDlg->SetRTSPCommand(command,ESMGetFrameRecord(),nCount);
	}
}
BOOL ESMGetRTSPBirdRecordState()
{
	return g_pESMMainWnd->m_pRTSPStateViewDlg->GetRecordState();
	//return FALSE;
}
void ESMSendRTSPHttpCmd(int nCommand)
{
	g_pESMMainWnd->OnHttpSender(nCommand);
}
void ESMSetRTSPStopTime(CString str4DM,int n)
{
	g_pESMMainWnd->m_pRTSPStateViewDlg->SetRecordStop(str4DM,n);
}
void ESMSendRTSPHttpSync(int nCamIdx)
{
	g_pESMMainWnd->OnHttpSync(nCamIdx);
}