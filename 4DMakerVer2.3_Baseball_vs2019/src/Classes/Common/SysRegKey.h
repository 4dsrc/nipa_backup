////////////////////////////////////////////////////////////////////////////////
//
//	ServiceControl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-06-25
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <atlbase.h>

//////////////////////////////////////////////////////////////////////////////////

class CSysRegKey : public CRegKey
{

public:
					CSysRegKey(HKEY p_hKeyParent = NULL,
							   CString p_KeyName = "",
							   REGSAM p_samDesired=KEY_ALL_ACCESS);
	virtual			~CSysRegKey();

	//////////////////////////////////////////////////////////////////////////////

public:
	int				Error(void);
	bool			OpenState(void);
	long			Result(void);
	bool			State(void);
	virtual bool	ReLoadData(void);
	virtual bool	StoreData(void);

	bool			Open(HKEY p_hKeyParent,
					     CString p_KeyName,
					     REGSAM p_samDesired=KEY_ALL_ACCESS);
	bool			ReOpen(void);
	bool			Create(HKEY p_hKeyParent,
						   CString p_KeyName,
						   DWORD p_Options=REG_OPTION_NON_VOLATILE,
						   REGSAM p_samDesired=KEY_ALL_ACCESS);
	void			Close(void);
	HKEY			RegKeyHandle(void);
	bool			QueryInfoKey(void);
	CString			EnumKey(int p_EnumIndex);

protected:
	bool			GetStringValue(CString p_ValueName, CString * p_pValue);
	bool			SetStringValue(CString p_ValueName, CString p_ValueDefault);
	bool			GetOrCreateString(CString p_ValueName,
								      CString * p_pValue,
								      CString p_ValueDefault);

	bool			GetDWORDValue(CString p_ValueName, DWORD & p_pValue);
	bool			SetDWORDValue(CString p_ValueName, DWORD p_ValueDefault);
	bool			GetOrCreateDWORD(CString p_ValueName,
								     DWORD & p_pValue,
								     DWORD p_ValueDefault);

	//////////////////////////////////////////////////////////////////////////////
	
public:
	DWORD			g_SubKeys;
    DWORD			g_MaxSubKeyNameLen;

protected:
	long			g_regResult;
	bool			g_regOpen;
	int				g_regError;
	bool			g_regState;

	HKEY			g_hKeyParent;
	CString			g_KeyName;
	REGSAM			g_sam;
	DWORD			g_Options;

	TCHAR			g_NameBuffer[MAX_PATH + 1];
	DWORD			g_NameSize;
	FILETIME		g_LastWriteTime;

private:
	TCHAR			g_regCharValue [MAX_PATH + 1];
	DWORD			g_regCharValueSize;
};
