// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#if (!defined __EXT_GEO_CONTROLS_H)
#define __EXT_GEO_CONTROLS_H

#if (!defined __EXT_MFC_DEF_H)
	#include <ExtMfcDef.h>
#endif // __EXT_MFC_DEF_H

#if (!defined __EXT_MFC_NO_GEO_CONTROLS)

	#if (!defined __EXT_POPUP_CTRL_MENU_H)
		#include <ExtPopupCtrlMenu.h>
	#endif // (!defined __EXT_POPUP_CTRL_MENU_H)

	#if (!defined __EXT_DURATIONWND_H)
		#include <ExtDurationWnd.h>
	#endif // (!defined __EXT_DURATIONWND_H)

#endif // (!defined __EXT_MFC_NO_GEO_CONTROLS)

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if (!defined __EXT_MFC_NO_GEO_CONTROLS)

class CExtLongitude;
class CExtLatitude;
class CExtLLBaseControlProperties;
class CExtLLEditWnd;
class CExtLLMapWnd;

extern void AFXAPI __EXT_MFC_DDX_LongitudeLatitude( CDataExchange * pDX, INT nIDC, CExtLongitude & _Longitude, CExtLatitude & _Latitude );

typedef long double CExtLL_t;

#define __EXT_LL_DEFAULT_NORMALIZATION__ false

/////////////////////////////////////////////////////////////////////////////
// CExtLLI template - longitude and latitude computations

template < INT max_degrees, INT max_minutes, INT max_seconds > class CExtLLI // longitude/latitude template based implementation
{
public:
	CExtLL_t m_lf;
	enum e_hemisphere_t
	{
		hemisphere_code_negative		= -1,
		hemisphere_code_positive		= +1,
		hemisphere_west					= -1,
		hemisphere_east					= +1,
		hemisphere_south				= -1,
		hemisphere_north				= +1,
	};
	static CExtLL_t stat_ll_get_value_in_seconds_max()
	{
		static const CExtLL_t g_lf_value_in_seconds_max = CExtLL_t(max_degrees) * CExtLL_t(max_minutes) * CExtLL_t(max_seconds);
		return g_lf_value_in_seconds_max;
	}
	static CExtLL_t stat_ll_get_value_in_seconds_min()
	{
		static const CExtLL_t g_lf_value_in_seconds_min = - stat_ll_get_value_in_seconds_max();
		return g_lf_value_in_seconds_min;
	}
	static bool stat_ll_is_normalized( CExtLL_t lf, bool bNormalizeOverflowEquality = false )
	{
		if( bNormalizeOverflowEquality )
		{
			bool bNormalized =
				( stat_ll_get_value_in_seconds_min() < lf && lf <= stat_ll_get_value_in_seconds_max() )
				? true : false;
			return bNormalized;
		}
		else
		{
			bool bNormalized =
				( stat_ll_get_value_in_seconds_min() <= lf && lf <= stat_ll_get_value_in_seconds_max() )
				? true : false;
			return bNormalized;
		}
	}
	static CExtLL_t stat_ll_normalize( CExtLL_t lf, bool bNormalizeOverflowEquality = false )
	{
		if( ! stat_ll_is_normalized( lf, bNormalizeOverflowEquality ) )
		{
			lf = ::fmodl( lf, stat_ll_get_value_in_seconds_max()*2 );
			if( lf > stat_ll_get_value_in_seconds_max() )
				lf -= stat_ll_get_value_in_seconds_max()*2;
			else
			if( lf < stat_ll_get_value_in_seconds_min() )
				lf += stat_ll_get_value_in_seconds_max()*2;
			ASSERT( stat_ll_is_normalized( lf, false ) );
			if( bNormalizeOverflowEquality )
			{
				if( lf == stat_ll_get_value_in_seconds_min() )
					lf = stat_ll_get_value_in_seconds_max();
				ASSERT( stat_ll_is_normalized( lf, true ) );
			}
		}
		return lf;
	}
	static CExtLL_t stat_ll_make(
		e_hemisphere_t eHS,
		INT nDegrees,
		INT nMinutes,
		INT nSeconds,
		CExtLL_t lfSecondsFraction,
		bool bNormalize,
		bool bNormalizeOverflowEquality = false
		)
	{
		CExtLL_t lf =
				lfSecondsFraction
			+	CExtLL_t(nSeconds)
			+	CExtLL_t(nMinutes) * 60.0
			+	CExtLL_t(nDegrees) * 60.0 * 60.0
			;
		if( INT(eHS) < 0 )
			lf = - lf;
		if( bNormalize )
			lf = stat_ll_normalize( lf, bNormalizeOverflowEquality );
		return lf;
	}
	static void stat_ll_split(
		CExtLL_t lf,
		e_hemisphere_t & eHS,
		INT & nDegrees,
		INT & nMinutes,
		INT & nSeconds,
		CExtLL_t & lfSecondsFraction,
		bool bNormalize,
		bool bNormalizeOverflowEquality = false
		)
	{
		if( bNormalize )
			lf = stat_ll_normalize( lf, bNormalizeOverflowEquality );
		eHS = ( lf <= 0.0 ) ? hemisphere_code_negative : hemisphere_code_positive;
		CExtLL_t lf_abs = lf < 0.0 ? ( - lf ) : lf;
		nDegrees = INT( ::fmodl( lf_abs / 60.0 / 60.0, CExtLL_t( max_degrees + 1 ) ) );
		nMinutes = INT( ::fmodl( lf_abs / 60.0       , 60.0 ) );
		nSeconds = INT( ::fmodl( lf_abs              , 60.0 ) );
		lfSecondsFraction = ::fmodl( lf_abs, 1.0 );
	}
	static int stat_ll_compare( CExtLL_t lf1, CExtLL_t lf2, bool bNormalizeOverflowEquality = true )
	{
		CExtLL_t lf1n = stat_ll_normalize( lf1, bNormalizeOverflowEquality ), lf2n = stat_ll_normalize( lf2, bNormalizeOverflowEquality );
		if( lf1n < lf2n )
			return -1;
		if( lf1n > lf2n )
			return 1;
		return 0;
	}

	void AssignFromOther(
		const CExtLLI < max_degrees, max_minutes, max_seconds > & other,
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		)
	{
		m_lf = other.m_lf;
		if( bNormalize )
			Normalize( bNormalizeOverflowEquality );
	}

	CExtLLI(
		e_hemisphere_t eHS = CExtLLI < max_degrees, max_minutes, max_seconds > :: hemisphere_code_negative,
		INT nDegrees = 0,
		INT nMinutes = 0,
		INT nSeconds = 0,
		CExtLL_t lfSecondsFraction = 0.0,
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		)
		: m_lf( stat_ll_make( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality ) )
	{
	}
	CExtLLI( CExtLL_t lf, bool bNormalize, bool bNormalizeOverflowEquality = false )
		: m_lf( lf )
	{
		if( bNormalize )
			Normalize( bNormalizeOverflowEquality );
	}
	CExtLLI(
		const CExtLLI < max_degrees, max_minutes, max_seconds > & other,
		bool bNormalizeOverflowEquality = false
		)
	{
		AssignFromOther( other, bNormalizeOverflowEquality );
	}
	CExtLLI(
		const CExtLLI < max_degrees, max_minutes, max_seconds > & other,
		bool bNormalize,
		bool bNormalizeOverflowEquality = false
		)
	{
		AssignFromOther( other, bNormalize, bNormalizeOverflowEquality );
	}

	void Get(
		e_hemisphere_t & eHS,
		INT & nDegrees,
		INT & nMinutes,
		INT & nSeconds,
		CExtLL_t & lfSecondsFraction,
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		) const
	{
		stat_ll_split( m_lf, eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
	}
	void Set(
		e_hemisphere_t eHS = CExtLLI < max_degrees, max_minutes, max_seconds > :: hemisphere_code_negative,
		INT nDegrees = 0,
		INT nMinutes = 0,
		INT nSeconds = 0,
		CExtLL_t lfSecondsFraction = 0.0,
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		)
	{
		m_lf = stat_ll_make( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
	}
	e_hemisphere_t HemisphereGet(
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		) const
	{
		e_hemisphere_t eHS;
		INT nDegrees, nMinutes, nSeconds;
		CExtLL_t lfSecondsFraction;
		Get( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		return eHS;
	}
	e_hemisphere_t HemisphereSet(
		e_hemisphere_t eHSset,
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		)
	{
		e_hemisphere_t eHS;
		INT nDegrees, nMinutes, nSeconds;
		CExtLL_t lfSecondsFraction;
		Get( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		if( eHSset != eHS )
			Set( eHSset, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		return eHS;
	}
	INT DegreesGet(
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		) const
	{
		e_hemisphere_t eHS;
		INT nDegrees, nMinutes, nSeconds;
		CExtLL_t lfSecondsFraction;
		Get( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		return nDegrees;
	}
	INT DegreesSet(
		INT nDegreesSet,
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		)
	{
		e_hemisphere_t eHS;
		INT nDegrees, nMinutes, nSeconds;
		CExtLL_t lfSecondsFraction;
		Get( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		if( nDegrees != nDegreesSet )
			Set( eHS, nDegreesSet, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		return nDegrees;
	}
	INT MinutesGet(
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		) const
	{
		e_hemisphere_t eHS;
		INT nDegrees, nMinutes, nSeconds;
		CExtLL_t lfSecondsFraction;
		Get( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		return nMinutes;
	}
	INT MinutesSet(
		INT nMinutesSet,
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		)
	{
		e_hemisphere_t eHS;
		INT nDegrees, nMinutes, nSeconds;
		CExtLL_t lfSecondsFraction;
		Get( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		if( nMinutes != nMinutesSet )
			Set( eHS, nDegrees, nMinutesSet, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		return nMinutes;
	}
	INT SecondsGet(
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		) const
	{
		e_hemisphere_t eHS;
		INT nDegrees, nMinutes, nSeconds;
		CExtLL_t lfSecondsFraction;
		Get( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		return nSeconds;
	}
	INT SecondsSet(
		INT nSecondsSet,
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		)
	{
		e_hemisphere_t eHS;
		INT nDegrees, nMinutes, nSeconds;
		CExtLL_t lfSecondsFraction;
		Get( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		if( nSeconds != nSecondsSet )
			Set( eHS, nDegrees, nMinutes, nSecondsSet, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		return nSeconds;
	}
	CExtLL_t SecondsFractionGet(
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		) const
	{
		e_hemisphere_t eHS;
		INT nDegrees, nMinutes, nSeconds;
		CExtLL_t lfSecondsFraction;
		Get( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		return lfSecondsFraction;
	}
	INT SecondsFractionSet(
		CExtLL_t lfSecondsFractionSet,
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		)
	{
		e_hemisphere_t eHS;
		INT nDegrees, nMinutes, nSeconds;
		CExtLL_t lfSecondsFraction;
		Get( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality );
		if( lfSecondsFraction != lfSecondsFractionSet )
			Set( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFractionSet, bNormalize, bNormalizeOverflowEquality );
		return INT(lfSecondsFraction);
	}

	bool IsNormalized( bool bNormalizeOverflowEquality = false ) const
	{
		return stat_ll_is_normalized( m_lf, bNormalizeOverflowEquality );
	}

	void Normalize( bool bNormalizeOverflowEquality = false )
	{
		m_lf = stat_ll_normalize( m_lf, bNormalizeOverflowEquality );
	}

	int Compare( const CExtLLI < max_degrees, max_minutes, max_seconds > & other ) const
	{
		if( stat_ll_compare( m_lf, other.m_lf, true ) == 0 )
			return 0;
		return stat_ll_compare( m_lf, other.m_lf, false );
	}

	bool operator == ( const CExtLLI < max_degrees, max_minutes, max_seconds > & other ) const
	{
		return ( Compare( other ) == 0 ) ? true : false;
	}
	bool operator != ( const CExtLLI < max_degrees, max_minutes, max_seconds > & other ) const
	{
		return ( Compare( other ) != 0 ) ? true : false;
	}
	bool operator <= ( const CExtLLI < max_degrees, max_minutes, max_seconds > & other ) const
	{
		return ( Compare( other ) <= 0 ) ? true : false;
	}
	bool operator >= ( const CExtLLI < max_degrees, max_minutes, max_seconds > & other ) const
	{
		return ( Compare( other ) >= 0 ) ? true : false;
	}
	bool operator < ( const CExtLLI < max_degrees, max_minutes, max_seconds > & other ) const
	{
		return ( Compare( other ) < 0 ) ? true : false;
	}
	bool operator > ( const CExtLLI < max_degrees, max_minutes, max_seconds > & other ) const
	{
		return ( Compare( other ) > 0 ) ? true : false;
	}

	CExtLLI < max_degrees, max_minutes, max_seconds > & operator = ( const CExtLLI < max_degrees, max_minutes, max_seconds > & other )
	{
		AssignFromOther( other );
		return (*this);
	}

	void Shift(
		const CExtLLI < max_degrees, max_minutes, max_seconds > & other,
		bool bWestOrNorth,
		bool bNormalize = true,
		bool bNormalizeOverflowEquality = false
		)
	{
		if( bWestOrNorth )
			m_lf += other.m_lf;
		else
			m_lf -= other.m_lf;
		if( bNormalize )
			Normalize( bNormalizeOverflowEquality );
	}
	void operator += ( const CExtLLI < max_degrees, max_minutes, max_seconds > & other )
	{
		Shift( other, true );
	}
	void operator -= ( const CExtLLI < max_degrees, max_minutes, max_seconds > & other )
	{
		Shift( other, false );
	}
	CExtLLI < max_degrees, max_minutes, max_seconds > operator + ( const CExtLLI < max_degrees, max_minutes, max_seconds > & other ) const
	{
		CExtLLI < max_degrees, max_minutes, max_seconds > result( *this );
		result += other;
		return result;
	}
	CExtLLI < max_degrees, max_minutes, max_seconds > operator - ( const CExtLLI < max_degrees, max_minutes, max_seconds > & other ) const
	{
		CExtLLI < max_degrees, max_minutes, max_seconds > result( *this );
		result -= other;
		return result;
	}

	void Serialize( CArchive & ar )
	{
		if( ar.IsStoring() )
			ar.Write( LPVOID(&m_lf), sizeof(m_lf) );
		else
			ar.Read( LPVOID(&m_lf), sizeof(m_lf) );
	}
	CArchive & operator << ( CArchive & ar )
	{
		ASSERT( ar.IsStoring() );
		Serialize( ar );
		return ar;
	}
	CArchive & operator >> ( CArchive & ar )
	{
		ASSERT( ar.IsLoading() );
		Serialize( ar );
		return ar;
	}
}; /// template class CExtLLI

/////////////////////////////////////////////////////////////////////////////
// CExtLongitude

class __PROF_UIS_API CExtLongitude : public CExtLLI < 180, 60, 60 >
{
public:
	CExtLongitude(
		e_hemisphere_t eHS = CExtLongitude::hemisphere_code_negative,
		INT nDegrees = 0,
		INT nMinutes = 0,
		INT nSeconds = 0,
		CExtLL_t lfSecondsFraction = 0.0,
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		);
	CExtLongitude(
		CExtLL_t lf,
		bool bNormalize,
		bool bNormalizeOverflowEquality = false
		);
	CExtLongitude(
		const CExtLongitude & other
		);
	CExtLongitude(
		const CExtLongitude & other,
		bool bNormalize,
		bool bNormalizeOverflowEquality = false
		);
}; /// class CExtLongitude

/////////////////////////////////////////////////////////////////////////////
// CExtLatitude

class __PROF_UIS_API CExtLatitude : public CExtLLI < 90, 60, 60 >
{
public:
	CExtLatitude(
		e_hemisphere_t eHS = CExtLatitude::hemisphere_code_negative,
		INT nDegrees = 0,
		INT nMinutes = 0,
		INT nSeconds = 0,
		CExtLL_t lfSecondsFraction = 0.0,
		bool bNormalize = __EXT_LL_DEFAULT_NORMALIZATION__,
		bool bNormalizeOverflowEquality = false
		);
	CExtLatitude(
		CExtLL_t lf,
		bool bNormalize,
		bool bNormalizeOverflowEquality = false
		);
	CExtLatitude(
		const CExtLatitude & other
		);
	CExtLatitude(
		const CExtLatitude & other,
		bool bNormalize,
		bool bNormalizeOverflowEquality = false
		);
}; /// class CExtLatitude

/////////////////////////////////////////////////////////////////////////////
// CExtLLBaseControlProperties

class __PROF_UIS_API CExtLLBaseControlProperties
{
public:
	enum eMode_t
	{
		longitude_latitude			= 0,
		latitude_longitude			= 1,
		longitude					= 2,
		latitude					= 3,
		default_mode				= longitude_latitude,
	};

	enum eItem_t
	{
		longitude_hemisphere		=  1,
		longitude_degrees			=  2,
		longitude_minutes			=  3,
		longitude_seconds			=  4,
		longitude_seconds_fraction	=  5,
		latitude_hemisphere			=  6,
		latitude_degrees			=  7,
		latitude_minutes			=  8,
		latitude_seconds			=  9,
		latitude_seconds_fraction	= 10,
		ll_min_value				=  1,
		ll_max_value				= 10,
		ll_count					= ( ll_max_value - ll_min_value + 1 ),
	};

	CExtLLBaseControlProperties * m_pOther;
	COleDateTime m_dtGMT;
	bool m_bMapChangeSelectionWhenPressed:1, m_bMapShowSelectionLines:1,  m_bMapShowHoverLines:1, m_bMapHR:1,
		m_bMapChangeSelectionOnLbuttonPressed:1, m_bMapChangeSelectionOnLbuttonReleased:1,
		m_bMapDisplayMarkers:1, m_bMapHighlightTimeZones:1, m_bMapDisplayTimeZoneTips:1, m_bMapDisplayMarkerTips:1,
		m_bMapDisplayCurrentLocationTip:1, m_bMapDisplayLocalDate:1, m_bMapDisplayLocalTime:1,
		m_bNoDelayTimeZoneTips:1, m_bNoDelayMarkerTips:1, m_bNoDelayCurrentLocationTips:1;

	__EXT_MFC_SAFE_TCHAR m_tchrNorth, m_tchrSouth, m_tchrEast, m_tchrWest;
	__EXT_MFC_SAFE_TCHAR m_tchrSeparator, m_tchrFractionSeparator;
	INT m_nFractionDigitCount;
	CExtSafeString m_strLLS; // separator string between longitude and latitude field groups

	eMode_t m_eMode;		
	CExtLongitude m_Longitude, m_LongitudeMin, m_LongitudeMax;
	CExtLatitude m_Latitude, m_LatitudeMin, m_LatitudeMax;

	LPARAM m_lParamCookie;
	BYTE m_nTzHtTransparencySelected, m_nTzHtTransparencyNormal;

	CExtLLBaseControlProperties();
	virtual ~CExtLLBaseControlProperties();

	virtual void Serialize( CArchive & ar );
	virtual void AssignProperties(
		const CExtLLBaseControlProperties & other
		);

	virtual void SetMode( eMode_t eMode );
	virtual eMode_t GetMode() const;

	virtual void RangeLongitudeGet(
		CExtLongitude & _LongitudeMin,
		CExtLongitude & _LongitudeMax
		) const;
	virtual void RangeLongitudeSet(
		const CExtLongitude & _LongitudeMin,
		const CExtLongitude & _LongitudeMax
		);
	virtual void RangeLatitudeGet(
		CExtLatitude & _LatitudeMin,
		CExtLatitude & _LatitudeMax
		) const;
	virtual void RangeLatitudeSet(
		const CExtLatitude & _LatitudeMin,
		const CExtLatitude & _LatitudeMax
		);

	virtual void LongitudeGet(
		CExtLongitude & _Longitude
		) const;
	virtual void LongitudeSet(
		const CExtLongitude & _Longitude,
		bool bUpdateNow
		);
	virtual void LatitudeGet(
		CExtLatitude & _Latitude
		) const;
	virtual void LatitudeSet(
		const CExtLatitude & _Latitude,
		bool bUpdateNow
		);

	virtual void OnQueryRangeLongitude(
		CExtLongitude & _LongitudeMin,
		CExtLongitude & _LongitudeMax
		) const;
	virtual void OnQueryRangeLatitude(
		CExtLatitude & _LatitudeMin,
		CExtLatitude & _LatitudeMax
		) const;

	class __PROF_UIS_API CHANGING_NOTIFICATION
	{
	public:
		CHANGING_NOTIFICATION(
			bool bChangedFinally,
			CExtLLBaseControlProperties & _LLBCP,
			const CExtLongitude & _LongitudeOld,
			const CExtLongitude & _LongitudeNew,
			const CExtLatitude & _LatitudeOld,
			const CExtLatitude & _LatitudeNew,
			LPARAM lParamCookie = NULL
			);
		bool m_bChangedFinally;
		CExtLLBaseControlProperties & m_LLBCP;
		LPARAM m_lParamCookie;
		CExtLongitude m_LongitudeOld, m_LongitudeNew;
		CExtLatitude m_LatitudeOld, m_LatitudeNew;
		operator WPARAM() const;
		static const CHANGING_NOTIFICATION * FromWPARAM( WPARAM wParam );
		friend class CExtLLBaseControlProperties;
		friend class CExtLLEditWnd;
		friend class CExtLLMapWnd;
	private:
		LRESULT Notify( HWND hWndNotify ) const;
	}; /// class CHANGING_NOTIFICATION

	static const UINT g_nMsgChangingNotification;

	static bool stat_ParseTextLongitude( 
		__EXT_MFC_SAFE_LPCTSTR & _str,
		__EXT_MFC_SAFE_TCHAR tchrEast,
		__EXT_MFC_SAFE_TCHAR tchrWest,
		__EXT_MFC_SAFE_TCHAR tchrSeparator,
		__EXT_MFC_SAFE_TCHAR tchrFractionSeparator,
		INT nFractionDigitCount,
		CExtLongitude * pLongitude = NULL
		);
	static bool stat_ParseTextLatitude( 
		__EXT_MFC_SAFE_LPCTSTR & _str,
		__EXT_MFC_SAFE_TCHAR tchrNorth,
		__EXT_MFC_SAFE_TCHAR tchrSouth,
		__EXT_MFC_SAFE_TCHAR tchrSeparator,
		__EXT_MFC_SAFE_TCHAR tchrFractionSeparator,
		INT nFractionDigitCount,
		CExtLatitude * pLatitude = NULL
		);
	static bool stat_ParseText( 
		CExtLLBaseControlProperties::eMode_t eMode,
		__EXT_MFC_SAFE_LPCTSTR & _str,
		__EXT_MFC_SAFE_TCHAR tchrNorth,
		__EXT_MFC_SAFE_TCHAR tchrSouth,
		__EXT_MFC_SAFE_TCHAR tchrEast,
		__EXT_MFC_SAFE_TCHAR tchrWest,
		__EXT_MFC_SAFE_TCHAR tchrSeparator,
		__EXT_MFC_SAFE_TCHAR tchrFractionSeparator,
		INT nFractionDigitCount,
		CExtLongitude * pLongitude = NULL,
		CExtLatitude * pLatitude = NULL
		);

	virtual HWND OnQueryNotificationSource() const = 0;
	virtual HWND OnQueryNotificationReceiver() const = 0;
	virtual bool OnValueChanging(
		const CExtLongitude & _LongitudeOld,
		const CExtLongitude & _LongitudeNew,
		const CExtLatitude & _LatitudeOld,
		const CExtLatitude & _LatitudeNew,
		CExtLLBaseControlProperties * pSrc
		);
	virtual bool OnValueChanged(
		const CExtLongitude & _LongitudeOld,
		const CExtLongitude & _LongitudeNew,
		const CExtLatitude & _LatitudeOld,
		const CExtLatitude & _LatitudeNew,
		CExtLLBaseControlProperties * pSrc
		);

	static CExtSafeString stat_TextGet(
		const CExtLongitude & _Longitude,
		__EXT_MFC_SAFE_TCHAR tchrEast,
		__EXT_MFC_SAFE_TCHAR tchrWest,
		__EXT_MFC_SAFE_TCHAR tchrSeparator,
		__EXT_MFC_SAFE_TCHAR tchrFractionSeparator,
		INT nFractionDigitCount
		);
	static CExtSafeString stat_TextGet(
		const CExtLatitude & _Latitude,
		__EXT_MFC_SAFE_TCHAR tchrNorth,
		__EXT_MFC_SAFE_TCHAR tchrSouth,
		__EXT_MFC_SAFE_TCHAR tchrSeparator,
		__EXT_MFC_SAFE_TCHAR tchrFractionSeparator,
		INT nFractionDigitCount
		);
	static CExtSafeString stat_TextGet(
		const CExtLongitude & _Longitude,
		const CExtLatitude & _Latitude,
		CExtLLBaseControlProperties::eMode_t eMode,
		__EXT_MFC_SAFE_TCHAR tchrNorth,
		__EXT_MFC_SAFE_TCHAR tchrSouth,
		__EXT_MFC_SAFE_TCHAR tchrEast,
		__EXT_MFC_SAFE_TCHAR tchrWest,
		__EXT_MFC_SAFE_TCHAR tchrSeparator,
		__EXT_MFC_SAFE_TCHAR tchrFractionSeparator,
		INT nFractionDigitCount,
		__EXT_MFC_SAFE_LPCTSTR strLLS
		);
	virtual CExtSafeString TextGet() const;
	virtual bool TextParse(
		__EXT_MFC_SAFE_LPCTSTR strText,
		CExtLongitude * pLongitude = NULL,
		CExtLatitude * pLatitude = NULL
		) const;
	virtual bool TextSet(
		__EXT_MFC_SAFE_LPCTSTR strText,
		bool bUpdateNow,
		bool bParsePartialText
		);

	struct __PROF_UIS_API MAP_MARKER
	{
		CExtLongitude m_Longitude;
		CExtLatitude m_Latitude;
		CExtSafeString m_strLabel;
		COLORREF m_clr;
		LPARAM m_lParam;
		MAP_MARKER()
			: m_clr( COLORREF(-1L) )
			, m_lParam( 0 )
		{
		}
	}; /// struct MAP_MARKER
	mutable CArray < MAP_MARKER, MAP_MARKER & > m_arrMapMarkers;
	virtual CArray < MAP_MARKER, MAP_MARKER & > & OnQueryMapMarkers();
}; /// class CExtLLBaseControlProperties

/////////////////////////////////////////////////////////////////////////////
// CExtLLEditWnd window

class __PROF_UIS_API CExtLLEditWnd
	: public CExtDurationWnd
	, public CExtLLBaseControlProperties
{
public:
	DECLARE_DYNCREATE( CExtLLEditWnd );
	CExtLLEditWnd();
	virtual ~CExtLLEditWnd();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
 
	virtual void SetMode( CExtLLBaseControlProperties::eMode_t eMode );
	virtual CExtLLBaseControlProperties::eMode_t GetMode() const;

	virtual bool IsDropDownButtonVisible() const;
	virtual void ScrollCurrentItem( INT nDelta );
	void SetBlank(
		bool bUpdate = true
		);
	bool ShowDropDown();
	
	virtual bool TextSet(
		__EXT_MFC_SAFE_LPCTSTR strText,
		bool bUpdateNow,
		bool bParsePartialText
		);

	virtual HWND OnQueryNotificationSource() const;
	virtual HWND OnQueryNotificationReceiver() const;
	void SyncSeparatorSpace();
protected:
	void _SyncSeparatorSpace_WalkRange(
		INT nReviewRangeStart,
		INT nReviewRangeEnd,
		bool bFirstRange,
		bool bLastRange
		);

	bool m_bBlankLongitudeHemisphere:1;
	bool m_bBlankLongitudeDegrees:1;
	bool m_bBlankLongitudeMinutes:1;
	bool m_bBlankLongitudeSeconds:1;
	bool m_bBlankLongitudeSecondsFraction:1;
	bool m_bBlankLatitudeHemisphere:1;
	bool m_bBlankLatitudeDegrees:1;
	bool m_bBlankLatitudeMinutes:1;
	bool m_bBlankLatitudeSeconds:1;
	bool m_bBlankLatitudeSecondsFraction:1;

	virtual bool _CreateHelper();
	virtual void _RecalcDuration();

	virtual ITEM_INFO * OnInitializeItemLongitudeHemisphere( 
		__EXT_MFC_SAFE_LPCTSTR lpszTextBefore = NULL, 
		__EXT_MFC_SAFE_LPCTSTR lpszTextAfter = NULL 
		);
	virtual ITEM_INFO * OnInitializeItemLongitudeDegrees( 
		__EXT_MFC_SAFE_LPCTSTR lpszTextBefore = NULL, 
		__EXT_MFC_SAFE_LPCTSTR lpszTextAfter = NULL 
		);
	virtual ITEM_INFO * OnInitializeItemLongitudeMinutes( 
		__EXT_MFC_SAFE_LPCTSTR lpszTextBefore = NULL, 
		__EXT_MFC_SAFE_LPCTSTR lpszTextAfter = NULL 
		);
	virtual ITEM_INFO * OnInitializeItemLongitudeSeconds( 
		__EXT_MFC_SAFE_LPCTSTR lpszTextBefore = NULL, 
		__EXT_MFC_SAFE_LPCTSTR lpszTextAfter = NULL 
		);
	virtual ITEM_INFO * OnInitializeItemLongitudeSecondsFraction( 
		__EXT_MFC_SAFE_LPCTSTR lpszTextBefore = NULL, 
		__EXT_MFC_SAFE_LPCTSTR lpszTextAfter = NULL 
		);

	virtual ITEM_INFO * OnInitializeItemLatitudeHemisphere( 
		__EXT_MFC_SAFE_LPCTSTR lpszTextBefore = NULL, 
		__EXT_MFC_SAFE_LPCTSTR lpszTextAfter = NULL 
		);
	virtual ITEM_INFO * OnInitializeItemLatitudeDegrees( 
		__EXT_MFC_SAFE_LPCTSTR lpszTextBefore = NULL, 
		__EXT_MFC_SAFE_LPCTSTR lpszTextAfter = NULL 
		);
	virtual ITEM_INFO * OnInitializeItemLatitudeMinutes( 
		__EXT_MFC_SAFE_LPCTSTR lpszTextBefore = NULL, 
		__EXT_MFC_SAFE_LPCTSTR lpszTextAfter = NULL 
		);
	virtual ITEM_INFO * OnInitializeItemLatitudeSeconds( 
		__EXT_MFC_SAFE_LPCTSTR lpszTextBefore = NULL, 
		__EXT_MFC_SAFE_LPCTSTR lpszTextAfter = NULL 
		);
	virtual ITEM_INFO * OnInitializeItemLatitudeSecondsFraction( 
		__EXT_MFC_SAFE_LPCTSTR lpszTextBefore = NULL, 
		__EXT_MFC_SAFE_LPCTSTR lpszTextAfter = NULL 
		);

	virtual void OnInializeAllLongitudeFields(
		__EXT_MFC_SAFE_LPCTSTR lpszTextBefore = NULL, 
		__EXT_MFC_SAFE_LPCTSTR lpszTextAfter = NULL 
		);
	virtual void OnInializeAllLatitudeFields(
		__EXT_MFC_SAFE_LPCTSTR lpszTextBefore = NULL, 
		__EXT_MFC_SAFE_LPCTSTR lpszTextAfter = NULL 
		);

	virtual void OnInitializeItemsArray();
	virtual CExtSafeString OnQueryItemText( 
		const ITEM_INFO * pII 
		) const;

	virtual void OnDigitPressed(
		UINT nDigit
		);
	virtual bool OnShowDropDownMenu();

	virtual void OnItemSelectionChanged();
	virtual void OnItemFinishInput();

public:
	virtual void RangeLongitudeSet(
		const CExtLongitude & _LongitudeMin,
		const CExtLongitude & _LongitudeMax
		);
	virtual void RangeLatitudeSet(
		const CExtLatitude & _LatitudeMin,
		const CExtLatitude & _LatitudeMax
		);
	virtual void LongitudeSet(
		const CExtLongitude & _Longitude,
		bool bUpdateNow
		);
	virtual void LatitudeSet(
		const CExtLatitude & _Latitude,
		bool bUpdateNow
		);

protected:
	//{{AFX_VIRTUAL(CExtLLEditWnd)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CExtLLEditWnd)
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	afx_msg LRESULT OnLLChangeNotification( WPARAM wParam, LPARAM lParam );
	DECLARE_MESSAGE_MAP()
}; /// class CExtLLEditWnd

/////////////////////////////////////////////////////////////////////////////
// CExtLLMapWnd window

#define __EXT_LONGITUDE_LATITUDE_MAP_WND_CLASS_NAME _T("ProfUIS-LLMapWnd")

class __PROF_UIS_API CExtLLMapWnd
	: public CWnd
	, public CExtLLBaseControlProperties
	, public CExtPmBridge
{
public:
	DECLARE_DYNCREATE( CExtLLMapWnd );
	DECLARE_CExtPmBridge_MEMBERS( CExtLLMapWnd );

	HWND m_hWndNotificationReceiver;
	bool m_bAutoDeleteWindow:1;
	CExtBitmap m_bmpMap, m_bmpTZ;
	COLORREF m_clrLocationSelected, m_clrShadowSelected, m_clrLocationHover, m_clrShadowHover, m_clrLocationPressed, m_clrShadowPressed;
	CPoint m_ptHover, m_ptPressed, m_ptShadowOffset;

	struct __PROF_UIS_API WORLD_TIME_ZONE
	{
		COLORREF m_clr;
		CExtBitmap m_bmp;
		double m_lfHoursShift;
		WORLD_TIME_ZONE()
			: m_clr( COLORREF(-1L) )
			, m_lfHoursShift( 0.0 )
		{
		}
		~WORLD_TIME_ZONE()
		{
			m_bmp.Empty();
		}
		bool IsNegativeShift() const
		{
			return ( m_lfHoursShift < 0.0 ) ? true : false;
		}
		INT GetShiftHours( bool bAbs ) const
		{
			INT nHours = INT(m_lfHoursShift);
			if( bAbs && nHours < 0 )
				nHours = - nHours;
			return INT(nHours);
		}
		INT GetShiftMinutes() const
		{
			INT nMinutes = INT( ( m_lfHoursShift - double( GetShiftHours( false ) ) ) * 60.0 );
			if( nMinutes < 0 )
				nMinutes = - nMinutes;
			return nMinutes;
		}
		CExtSafeString GetTimeShiftAsText( bool bIncludeSign, __EXT_MFC_SAFE_TCHAR tchrHoursMinutesSeparator = _T(':') ) const
		{
			CExtSafeString s;
			if( bIncludeSign && m_lfHoursShift == 0.0 )
				bIncludeSign = false;
			s.Format(
				_T("%s%d%c%02d"),
				bIncludeSign ? ( IsNegativeShift() ? _T("-") : _T("+") ) : _T(""),
				GetShiftHours( true ),
				tchrHoursMinutesSeparator,
				GetShiftMinutes()
				);
			return s;
		}
	}; /// struct WORLD_TIME_ZONE
	CExtBitmap m_bmpHTTZ;
	CMap < COLORREF, COLORREF, WORLD_TIME_ZONE *, WORLD_TIME_ZONE * > m_mapWorldTimeZones;
	virtual void OnLLMapWorldMapInit(
		BYTE nTransparencySelected,
		BYTE nTransparencyNormal
		);
	virtual void OnLLMapHtWorldMapClear();

	virtual void WorldMapLoad(
		bool bHR,
		bool bGenerateTZ = true,
		bool bRedraw = true,
		bool bUpdateNow = true
		);
	virtual void WorldMapEmpty();

	CExtLLMapWnd();
	virtual ~CExtLLMapWnd();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	static bool g_bWndClassRegistered;
	bool m_bDirectCreateCall:1, m_bInitialized:1, m_bCanceling:1;
	virtual bool _CreateHelper();
	static bool RegisterWndClass();
	virtual void _CancelActions(
		bool bUpdateNow = true
		);
	virtual void _ChangeLL(
		CPoint & point,
		bool bUpdateNow = true,
		bool * p_bTipDisplayed = NULL
		);
	void _DoSetCursor();
	virtual void _DoSetCursor( const POINT pt, bool bScreenPoint );
	enum e_mouse_tracking_mode_t
	{
		__EMTM_NONE			= 0,
		__EMTM_HOVER		= 1,
		__EMTM_PRESSED		= 2,
	};
	e_mouse_tracking_mode_t m_eMTM;
public:
	INT m_nAdvancedTipStyle; // CExtPopupMenuTipWnd::__ETS_RECTANGLE_NO_ICON

	INT m_nMarkerRadiusMin, m_nMarkerRadiusMax, m_nMarkerRadiusSnap, m_nMarkerRadiusDivider;
	COLORREF m_clrMarker;
	double m_lfMarkerMutliplierLighter, m_lfMarkerMutliplierDarker;

	virtual bool Create(
		CWnd * pParentWnd,
		const RECT & rcWnd = CRect( 0, 0, 0, 0 ),
		UINT nDlgCtrlID = UINT( __EXT_MFC_IDC_STATIC ),
		DWORD dwWindowStyle = WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		CCreateContext * pContext = NULL
		);
	virtual void Update(
		bool bUpdateNow
		);

	virtual void SetMode( CExtLLBaseControlProperties::eMode_t eMode );
	virtual CExtLLBaseControlProperties::eMode_t GetMode() const;

	virtual HWND OnQueryNotificationSource() const;
	virtual HWND OnQueryNotificationReceiver() const;

	virtual void RangeLongitudeSet(
		const CExtLongitude & _LongitudeMin,
		const CExtLongitude & _LongitudeMax
		);
	virtual void RangeLatitudeSet(
		const CExtLatitude & _LatitudeMin,
		const CExtLatitude & _LatitudeMax
		);
	virtual void LongitudeSet(
		const CExtLongitude & _Longitude,
		bool bUpdateNow
		);
	virtual void LatitudeSet(
		const CExtLatitude & _Latitude,
		bool bUpdateNow
		);

	virtual CRect OnLLMapPaintMarginsRect() const;
	virtual CPoint OnLLMapGetSelectionPoint() const;
	virtual CPoint OnLLMapGetHoverPoint() const;
	virtual CPoint OnLLMapGetPressedPoint() const;
	virtual void OnLLMapDrawEntire( CDC & dc );
	virtual void OnLLMapDrawMarkers(
		CDC & dc
		);
	virtual void OnLLMapDrawMap(
		CDC & dc
		);
	virtual void OnLLMapDrawSelection(
		CDC & dc,
		COLORREF clrLocation,
		COLORREF clrShadow,
		bool bDrawHorizontal,
		bool bDrawVertical
		);
	virtual void OnLLMapDrawHover(
		CDC & dc,
		COLORREF clrLocation,
		COLORREF clrShadow,
		bool bDrawHorizontal,
		bool bDrawVertical
		);
	virtual void OnLLMapDrawPressed(
		CDC & dc,
		COLORREF clrLocation,
		COLORREF clrShadow,
		bool bDrawHorizontal,
		bool bDrawVertical
		);
	virtual void OnLLMapDrawLocationLines(
		CDC & dc,
		CPoint ptAligned,
		CPoint ptShadowOffset,
		COLORREF clrLocation,
		COLORREF clrShadow,
		bool bDrawHorizontal,
		bool bDrawVertical
		);

	virtual INT LongitudeToClient( const CExtLongitude & _Longitude ) const;
	virtual void ClientToLongitude( INT nX, CExtLongitude & _Longitude ) const;
	CExtLongitude ClientToLongitude( INT nX ) const;

	virtual INT LatitudeToClient( const CExtLatitude & _Latitude ) const;
	virtual void ClientToLatitude( INT nY, CExtLatitude & _Latitude ) const;
	CExtLatitude ClientToLatitude( INT nY ) const;

	void MapToClient( const CExtLongitude & _Longitude, const CExtLatitude & _Latitude, POINT & ptClient ) const;
	CPoint MapToClientPoint( const CExtLongitude & _Longitude, const CExtLatitude & _Latitude ) const;
	void ClientToMap( const POINT & ptClient, CExtLongitude & _Longitude, CExtLatitude & _Latitude ) const;

	void MapToClient( const CExtLongitude & _Longitude, const CExtLatitude & _Latitude, SIZE & sizeClient ) const;
	CSize MapToClientSize( const CExtLongitude & _Longitude, const CExtLatitude & _Latitude ) const;
	void ClientToMap( const SIZE & sizeClient, CExtLongitude & _Longitude, CExtLatitude & _Latitude ) const;

	void MapToClient(
		const CExtLongitude & _LongitudeMin,
		const CExtLongitude & _LongitudeMax,
		const CExtLatitude & _LatitudeMin,
		const CExtLatitude & _LatitudeMax,
		RECT & rcClient
		);
	CRect MapToClientRect(
		const CExtLongitude & _LongitudeMin,
		const CExtLongitude & _LongitudeMax,
		const CExtLatitude & _LatitudeMin,
		const CExtLatitude & _LatitudeMax
		);
	void ClientToMap(
		const RECT & rcClient,
		CExtLongitude & _LongitudeMin,
		CExtLongitude & _LongitudeMax,
		CExtLatitude & _LatitudeMin,
		CExtLatitude & _LatitudeMax
		);

	virtual bool TextSet(
		__EXT_MFC_SAFE_LPCTSTR strText,
		bool bUpdateNow,
		bool bParsePartialText
		);

	virtual INT OnLLMapCalcMarkerRadius() const;
	virtual CPoint OnLLMapSnapToMarker(
		CPoint point,
		bool bDisplayTip = false,
		bool * p_bTipDisplayed = NULL
		);

	virtual CExtSafeString OnLLMapQueryCurrentLocationTipText(
		CPoint point
		);
	virtual bool OnLLMapDisplayCurrentLocationTip(
		CPoint point
		);

	virtual CExtSafeString OnLLMapQueryLocalDateTimeTipText(
		CPoint ptTZHT,
		CExtLLMapWnd::WORLD_TIME_ZONE * pWTZ
		);
	virtual CExtSafeString OnLLMapQueryTimeZoneTipText(
		CPoint ptTZHT,
		bool bInsideMarker
		);
	virtual COLORREF OnLLMapHitTestTimeZoneColor(
		CPoint ptTZHT
		);
	virtual bool OnLLMapDisplayTimeZoneTip(
		CPoint ptTZHT
		);

	virtual CExtPopupMenuTipWnd * OnAdvancedPopupMenuTipWndGet() const;
	virtual void OnAdvancedPopupMenuTipWndDisplay(
		CExtPopupMenuTipWnd & _ATTW,
		const RECT & rcExcludeArea,
		bool bNoDelay
		) const;

protected:
	//{{AFX_VIRTUAL(CExtLLMapWnd)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void PreSubclassWindow();
	virtual void PostNcDestroy();
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CExtLLMapWnd)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnCancelMode();
	afx_msg void OnCaptureChanged(CWnd *pWnd);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	//}}AFX_MSG
#if _MFC_VER < 0x700
	afx_msg void OnActivateApp(BOOL bActive, HTASK hTask);
#else
	afx_msg void OnActivateApp(BOOL bActive, DWORD hTask);
#endif
	DECLARE_MESSAGE_MAP()
}; /// class CExtLLMapWnd

/////////////////////////////////////////////////////////////////////////////
// CExtLLPopupMapMenuWnd

class __PROF_UIS_API CExtLLPopupMapMenuWnd : public CExtPopupControlMenuWnd
{
public:
	DECLARE_DYNCREATE( CExtLLPopupMapMenuWnd );

	CExtLLPopupMapMenuWnd();
	virtual ~CExtLLPopupMapMenuWnd();

	virtual bool _OnMouseClick(
		UINT nFlags,
		CPoint point,
		bool & bNoEat
		);
	virtual bool _OnKeyDown(
		UINT nChar,
		UINT nRepCnt,
		UINT nFlags,
		bool & bNoEat
		);

// Attributes
public:
	CExtLLMapWnd m_wndMap;

// Implementation
protected:
	virtual HWND OnCreateChildControl(
		const RECT & rcChildControl
		);
	virtual CSize OnAdjustChildControlSize();

protected:
	// Generated message map functions
	//{{AFX_MSG(CExtLLPopupMapMenuWnd)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
}; /// class CExtLLPopupMapMenuWnd

#endif // (!defined __EXT_MFC_NO_GEO_CONTROLS)

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // (!defined __EXT_GEO_CONTROLS_H)
