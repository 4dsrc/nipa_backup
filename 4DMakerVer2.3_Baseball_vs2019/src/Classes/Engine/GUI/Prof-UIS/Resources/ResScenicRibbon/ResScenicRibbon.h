#if (! defined __ResScenicRibbon_H )
#define __ResScenicRibbon_H

#define IDB_7_SCENIC_PAGE_BK                                    29800

#define IDB_7_SCENIC_GROUP_BK                                   29801
#define IDB_7_SCENIC_GROUP_BK_HOVER                             29802

#define IDB_7_SCENIC_GROUP_BTN_IMAGE_BOX                        29803

#define IDB_7_SCENIC_GROUP_BTN_COLLAPSED_FOCUS                  29804
#define IDB_7_SCENIC_GROUP_BTN_COLLAPSED_HOVER                  29805
#define IDB_7_SCENIC_GROUP_BTN_COLLAPSED_NORMAL                 29806
#define IDB_7_SCENIC_GROUP_BTN_COLLAPSED_PRESSED                29807
                                                                    
#define IDB_7_SCENIC_TAB_ITEM_NORMAL_HOVER                      29808
#define IDB_7_SCENIC_TAB_ITEM_SELECTED                          29809
#define IDB_7_SCENIC_TAB_ITEM_SELECTED_ACCENT                   29810
#define IDB_7_SCENIC_TAB_ITEM_SELECTED_HOVER                    29811

#define IDB_7_SCENIC_QATB_GLOW_AT_TOP                           29812

#define IDB_7_SCENIC_DDA                                        29813

#define IDB_7_SCENIC_DLB_DISABLED                               29814
#define IDB_7_SCENIC_DLB_NORMAL                                 29815
#define IDB_7_SCENIC_DLB_HOVER                                  29816
#define IDB_7_SCENIC_DLB_PRESSED                                29817

#define IDB_7_SCENIC_FILE_BTN_NORMAL                            29818
#define IDB_7_SCENIC_FILE_BTN_HOVER                             29819
#define IDB_7_SCENIC_FILE_BTN_PRESSED                           29820

#define IDB_7_SCENIC_SEPARATOR_HORZ                             29821
#define IDB_7_SCENIC_SEPARATOR_VERT                             29822

#define IDB_7_SCENIC_BTNSIMPLE_SMALL_HOVER                      29823
#define IDB_7_SCENIC_BTNSIMPLE_SMALL_PRESSED                    29824

#define IDB_7_SCENIC_BTNSIMPLE_LARGE_HOVER                      29825
#define IDB_7_SCENIC_BTNSIMPLE_LARGE_PRESSED                    29826

#define IDB_7_SCENIC_SPLSIMPLE_SMALL_HOVER                      29827
#define IDB_7_SCENIC_SPLSIMPLE_SMALL_PRESSED                    29828

#define IDB_7_SCENIC_SPLSIMPLE_LARGE_HOVER_T                    29829
#define IDB_7_SCENIC_SPLSIMPLE_LARGE_HOVER_B                    29830
#define IDB_7_SCENIC_SPLSIMPLE_LARGE_PRESSED_T                  29831
#define IDB_7_SCENIC_SPLSIMPLE_LARGE_PRESSED_B                  29832

#define IDB_7_SCENIC_TB_S_DISABLED                              29833
#define IDB_7_SCENIC_TB_S_NORMAL                                29834
#define IDB_7_SCENIC_TB_S_HOVER                                 29835
#define IDB_7_SCENIC_TB_S_PRESSED                               29836

#define IDB_7_SCENIC_TB_L_DISABLED                              29837
#define IDB_7_SCENIC_TB_L_NORMAL                                29838
#define IDB_7_SCENIC_TB_L_HOVER                                 29839
#define IDB_7_SCENIC_TB_L_PRESSED                               29840

#define IDB_7_SCENIC_TB_M_DISABLED                              29841
#define IDB_7_SCENIC_TB_M_NORMAL                                29842
#define IDB_7_SCENIC_TB_M_HOVER                                 29843
#define IDB_7_SCENIC_TB_M_PRESSED                               29844

#define IDB_7_SCENIC_TB_R_DISABLED                              29845
#define IDB_7_SCENIC_TB_R_NORMAL                                29846
#define IDB_7_SCENIC_TB_R_HOVER                                 29847
#define IDB_7_SCENIC_TB_R_PRESSED                               29848

#define IDB_7_SCENIC_COMBOBOX_DDA                               29849
#define IDB_7_SCENIC_COMBOBOX_DDB_DISABLED                      29850
#define IDB_7_SCENIC_COMBOBOX_DDB_NORMAL                        29851
#define IDB_7_SCENIC_COMBOBOX_DDB_HOVER1                        29852
#define IDB_7_SCENIC_COMBOBOX_DDB_HOVER2                        29853
#define IDB_7_SCENIC_COMBOBOX_DDB_PRESSED                       29854

#define IDB_7_SCENIC_RGIBTN_DOWN                                29855
#define IDB_7_SCENIC_RGIBTN_MENU                                29856
#define IDB_7_SCENIC_RGIBTN_UP                                  29857

#define IDB_7_SCENIC_RGIARR_UP_DISABLED                         29858
#define IDB_7_SCENIC_RGIARR_UP_NORMAL                           29859

#define IDB_7_SCENIC_RGIARR_DOWN_DISABLED                       29860
#define IDB_7_SCENIC_RGIARR_DOWN_NORMAL                         29861

#define IDB_7_SCENIC_RGIARR_MENU_DISABLED                       29862
#define IDB_7_SCENIC_RGIARR_MENU_NORMAL                         29863

#define IDB_7_SCENIC_BIG_BORDER                                 29864




#endif /// (! defined __ResScenicRibbon_H )



