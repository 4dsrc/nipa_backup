#if !defined(__PROF_UIS_RES_CUSTOM_H)
#define __PROF_UIS_RES_CUSTOM_H


/////////////////////////////////////////////////////////////////////////////
// Localized Resources
//  affects: Resource.rc

// Disable unwanted languages by un-commenting out the corresponding macros
//  (English is always enabled)

//#define __EXT_MFC_NO_RESOURCES_ARABIC_SAUDI_ARABIA
//#define __EXT_MFC_NO_RESOURCES_BELARUSIAN
//#define __EXT_MFC_NO_RESOURCES_BULGARIAN_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_CHINESE_SIMPLIFIED
//#define __EXT_MFC_NO_RESOURCES_CHINESE_TRADITIONAL
//#define __EXT_MFC_NO_RESOURCES_CROATIAN_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_CZECH_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_DANISH_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_DUTCH_NETHERLANDS
//#define __EXT_MFC_NO_RESOURCES_ESTONIAN
//#define __EXT_MFC_NO_RESOURCES_GERMAN
//#define __EXT_MFC_NO_RESOURCES_GREEK
//#define __EXT_MFC_NO_RESOURCES_HUNGARIAN_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_FRENCH
//#define __EXT_MFC_NO_RESOURCES_FRENCH_CANADIAN
//#define __EXT_MFC_NO_RESOURCES_INDONESIAN
//#define __EXT_MFC_NO_RESOURCES_ITALIAN
//#define __EXT_MFC_NO_RESOURCES_JAPANESE_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_KOREAN_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_LATVIAN_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_LITHUANIAN
//#define __EXT_MFC_NO_RESOURCES_NORWEGIAN_BOKMAL
//#define __EXT_MFC_NO_RESOURCES_POLISH_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_PORTUGUESE_BRAZILIAN
//#define __EXT_MFC_NO_RESOURCES_ROMANIAN_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_RUSSIAN_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_SERBIAN_LATIN
//#define __EXT_MFC_NO_RESOURCES_SLOVAK_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_SLOVENIAN_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_SPANISH_MODERN
//#define __EXT_MFC_NO_RESOURCES_SWEDISH_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_TURKISH_DEFAULT
//#define __EXT_MFC_NO_RESOURCES_UKRAINIAN_DEFAULT


/////////////////////////////////////////////////////////////////////////////
// Resources for common and shell controls
//  affects: ResCommonControls\ResCommonControls.rc


/////////////////////////////////////////////////////////////////////////////
// Resources for Office 2007 paint managers
//  affects: Res2007\Res2007.rc

// Disable unwanted themes by un-commenting out the corresponding macros

//#define __EXT_MFC_NO_OFFICE2007_R1
//#define __EXT_MFC_NO_OFFICE2007_LB // luna blue
//#define __EXT_MFC_NO_OFFICE2007_O  // obsidian
//#define __EXT_MFC_NO_OFFICE2007_S  // silver


/////////////////////////////////////////////////////////////////////////////
// Resources for paint manager switching controls
//  affects: ResPM\ResPM.rc


#endif // !__PROF_UIS_RES_CUSTOM_H
