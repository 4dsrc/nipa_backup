#if (!defined __PROF_UIS_RES_PM_H)
#define __PROF_UIS_RES_PM_H

#define IDB_EXT_PM_16x16                        29740
#define IDB_EXT_PM_24x24                        29741
#define IDB_EXT_PM_32x32                        29742

#define ID_EXT_PM_MENU_MARKER_THEME_CHANGER     29999
#define ID_EXT_PM_TOOLBAR_THEME_CHANGER         29744

// this needs to be matched with the sequence in extpaingmanager.h line 369 om
// this also needs to matched to line 610 in extcontrol.cpp
#define ID_EXT_PM_THEME_Office2000								29750
#define ID_EXT_PM_THEME_OfficeXP									29751
#define ID_EXT_PM_THEME_Office2003								29752
#define ID_EXT_PM_THEME_Office2003NoThemes				29753
#define ID_EXT_PM_THEME_Studio2005								29754
#define ID_EXT_PM_THEME_Studio2008								29755
#define ID_EXT_PM_THEME_NativeXP									29756
// left for compatability
#define ID_EXT_PM_THEME_VISTA  										29755
#define ID_EXT_PM_THEME_Office2007_R1							29757
#define ID_EXT_PM_THEME_Office2007_R2_LunaBlue		29758
#define ID_EXT_PM_THEME_Office2007_R2_Obsidian		29759
#define ID_EXT_PM_THEME_Office2007_R2_Silver			29760
#define ID_EXT_PM_THEME_Office2007_R3_LunaBlue		29761
#define ID_EXT_PM_THEME_Office2007_R3_Obsidian		29762
#define ID_EXT_PM_THEME_Office2007_R3_Silver			29763
#define ID_EXT_PM_THEME_Office2010_R1							29764
#define ID_EXT_PM_THEME_Office2010_R2_Blue				29765
#define ID_EXT_PM_THEME_Office2010_R2_Silver			29766
#define ID_EXT_PM_THEME_Office2010_R2_Black				29767
#define ID_EXT_PM_THEME_Studio2010								29768
#define ID_EXT_PM_THEME_ProfSkinPainter						29769

#define ID_EXT_PM_THEME_LOAD_XML_SKIN_FILE				29770
#define ID_EXT_PM_THEME_LOAD_BINARY_SKIN_FILE			29771

#define ID_EXT_PM_THEME_WINDOWS7									29772
#define ID_EXT_PM_THEME_WINDOWS8									29773
#define ID_EXT_PM_THEME_WINDOWS10									29774

#define ID_EXT_PM_THEME_Office2013_AzureDarkGrey	29775
#define ID_EXT_PM_THEME_Office2013_AzureLightGrey	29776
#define ID_EXT_PM_THEME_Office2013_AzureWhite			29777
#define ID_EXT_PM_THEME_Office2013_GreenDarkGrey	29778
#define ID_EXT_PM_THEME_Office2013_GreenLightGrey	29779
#define ID_EXT_PM_THEME_Office2013_GreenWhite   	29780
#define ID_EXT_PM_THEME_Office2013_OrangeDarkGrey	29781
#define ID_EXT_PM_THEME_Office2013_OrangeLightGrey	29782
#define ID_EXT_PM_THEME_Office2013_OrangeWhite		29783
#define ID_EXT_PM_THEME_Office2013_RedDarkGrey		29784
#define ID_EXT_PM_THEME_Office2013_RedLightGrey		29785
#define ID_EXT_PM_THEME_Office2013_RedWhite				29786

#define ID_EXT_PM_THEME_Studio_2012								29787
#define ID_EXT_PM_THEME_Studio_2013								29788
#define ID_EXT_PM_THEME_Studio_Light_2015					29789
#define ID_EXT_PM_THEME_Studio_Dark_2015					29790
#define ID_EXT_PM_THEME_Studio_Blue_2015					29791

#define ID_EXT_PM_THEME_ProfUIS_BLACK							29792
#define ID_EXT_PM_THEME_ProfUIS_GREY							29793
#define ID_EXT_PM_THEME_ProfUIS_SILVER 						29794
#define ID_EXT_PM_THEME_ProfUIS_BBLUE							29795
#define ID_EXT_PM_THEME_ProfUIS_SKYBLUE						29796
#define ID_EXT_PM_THEME_ProfUIS_GOLD  						29797
#define ID_EXT_PM_THEME_ProfUIS_WHITE  						29798
#define ID_EXT_PM_THEME_ProfUIS_STEELBLUE					29799

#define ID_EXT_PM_THEME_PHOTOSHOP							29800

#endif

