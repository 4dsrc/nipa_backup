// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#if (!defined __EXTFORMULAGRID_H)
#define __EXTFORMULAGRID_H

#if (!defined __EXT_MFC_DEF_H)
	#include <ExtMfcDef.h>
#endif // __EXT_MFC_DEF_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if (!defined __EXT_MFC_NO_FORMULA_GRID)

#if (!defined __EXT_GRIDWND_H)
	#include <ExtGridWnd.h>
#endif 

#if (!defined __EXT_PRINT_H)
	#include <ExtPrint.h>
#endif

#if (defined _DEBUG)
	#define __EXT_DEBUG_HEAVY_FORMULA_PARSER
#endif // (defined _DEBUG)

class CExtFormulaAlphabet;
class CExtFormulaValue;
class CExtFormulaValueArray;
class CExtFormulaFunction;
class CExtFormulaFunctionSet;
class CExtFormulaItem;
class CExtFormulaParser;
class CExtFormulaDataManager;
class CExtFormulaGridCell;
class CExtFormulaGridWnd;
class CExtFormulaInplaceEdit;
class CExtFormulaInplaceListBox;
class CExtFormulaInplaceTipWnd;

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaDataManager

class __PROF_UIS_API CExtFormulaDataManager
{
public:
	typedef CExtHashMapT < CExtGridDataProvider::packed_location_t, CExtGridDataProvider::packed_location_t, CExtFormulaGridCell *, CExtFormulaGridCell * > map_formula_cells_t;
	typedef CExtHashMapT < CExtGridDataProvider::packed_location_t, CExtGridDataProvider::packed_location_t, CExtGridCell *, CExtGridCell * > map_dependency_cells_t;
	typedef CList < CExtGridDataProvider::packed_location_t, CExtGridDataProvider::packed_location_t > list_computation_sequence_t;
	typedef CExtHashMapT < CExtGridDataProvider::packed_location_t, CExtGridDataProvider::packed_location_t, CExtGridDataProvider::packed_location_t, CExtGridDataProvider::packed_location_t > map_cross_reference_t;
protected:
	CExtFormulaParser * m_pFormulaParser;
	CExtFormulaAlphabet * m_pFormulaAlphabet;
	CExtFormulaFunctionSet * m_pFormulaFunctionSet;
	map_formula_cells_t m_mapFC;
	CExtSafeMapStringToPtr m_mapNamedRanges;
	typedef CMap < COLORREF, COLORREF, CExtBitmap *, CExtBitmap * > map_color_bitmaps_t;
	map_color_bitmaps_t m_mapColorBitmaps;
public:
	CArray < COLORREF, COLORREF > m_arrRangeRefColors;
	COLORREF m_clrNumericConstant, m_clrStringConstant, m_clrFuncCall;
	bool m_bDrawLinesForNamedRanges:1, m_bDrawLinesForSimpleRanges:1;
	BYTE m_nAlphaForNamedRanges, m_nAlphaForSimpleRanges;
	INT m_nRangeWalkIndex;
	CList < CExtFormulaItem *, CExtFormulaItem * > m_listEditedFormulaRanges, m_listEditedFunctions;
	CExtFormulaItem * m_pFormulaItemRangeInGridHT;
	bool m_bInGridHtLeft:1, m_bInGridHtTop:1, m_bInGridHtRight:1, m_bInGridHtBottom:1;
	CRect m_rcInGridHT;
	CHARRANGE m_crTracker;
	UINT m_nGridSelectionTrackerTimerPeriod;
	LONG  m_nGridSelectionTrackerLength0,  m_nGridSelectionTrackerLength1, m_nGridSelectionTrackerWidth, m_nGridSelectionTrackerHeight;
	CExtFormulaDataManager();
	virtual ~CExtFormulaDataManager();
	void EmptyEditedLists();
	virtual CExtGridDataProvider & OnQueryDataProvider() = 0;
	const CExtGridDataProvider & OnQueryDataProvider() const;
	virtual CExtFormulaAlphabet & OnQueryFormulaAlphabet();
	const CExtFormulaAlphabet & OnQueryFormulaAlphabet() const;
	virtual CExtFormulaParser & OnQueryFormulaParser();
	const CExtFormulaParser & OnQueryFormulaParser() const;
	virtual CExtFormulaFunctionSet & OnQueryFormulaFunctionSet();
	const CExtFormulaFunctionSet & OnQueryFormulaFunctionSet() const;
	virtual bool OnGetFormulaCell(
		LONG nColNo,
		LONG nRowNo,
		CExtGridCell ** ppCell,
		CRuntimeClass * pInitRTC = NULL,
		bool bAutoFindValue = true, // auto find row/column default value (only when pInitRTC=NULL)
		bool bUseColumnDefaultValue = true // false - use row default value (only when bAutoFindValue=true)
		) = 0;
	CExtFormulaGridCell * GetFormulaCell( LONG nColNo, LONG nRowNo );
	CExtFormulaGridCell * GetFormulaCell( CExtGridDataProvider::packed_location_t _loc );
	virtual bool OnComputeValue(
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
		LONG nColNo,
		LONG nRowNo,
		CExtFormulaValue & _FV,
		const CExtFormulaItem * pExternalFI
		);
	virtual void _RFE_RemoveList( CList < CExtGridDataProvider::packed_location_t, CExtGridDataProvider::packed_location_t > & _list );
	virtual void _RFE_RemoveRange( CExtGR2D & _range );
	virtual void _RFE_RemoveEntry( CExtGridDataProvider::packed_location_t _loc );
	virtual void _RFE_AddEntry( CExtGridDataProvider::packed_location_t _loc, CExtFormulaGridCell * pCell );
	virtual void _RFE_OnRegisterFormulaCell(
		LONG nColNo,
		LONG nRowNo,
		CExtFormulaGridCell * pCell
		);
	virtual void OnComputeMapOfAffectedCells(
		LONG nColNo,
		LONG nRowNo,
		bool bIncludeThis,
		CExtFormulaDataManager::map_formula_cells_t & _mapDependency,
		bool bDeepMode = true,
		bool bAppendMode = false
		);
	void OnComputeMapOfAffectedCells(
		CExtGridDataProvider::packed_location_t _loc,
		bool bIncludeThis,
		CExtFormulaDataManager::map_formula_cells_t & _mapDependency,
		bool bDeepMode = true,
		bool bAppendMode = false
		)
	{
		CPoint pt = CExtGridDataProvider::_L2P( _loc );
		OnComputeMapOfAffectedCells( pt.x, pt.y, bIncludeThis, _mapDependency, bDeepMode, bAppendMode );
	}
	virtual void OnComputeMapOfRequiredCells(
		LONG nColNo,
		LONG nRowNo,
		CExtFormulaDataManager::map_formula_cells_t & _mapDependency
		);
	void OnComputeMapOfRequiredCells(
		CExtGridDataProvider::packed_location_t _loc,
		CExtFormulaDataManager::map_formula_cells_t & _mapDependency
		)
	{
		CPoint pt = CExtGridDataProvider::_L2P( _loc );
		OnComputeMapOfRequiredCells( pt.x, pt.y, _mapDependency );
	}
	virtual void OnComputeCrossReferencesStep(
		CExtGridDataProvider::packed_location_t _loc,
		CExtFormulaDataManager::map_cross_reference_t & _mapCrossReference,
		CExtFormulaDataManager::map_formula_cells_t & _mapChain
//#if (defined _DEBUG)
//		, INT nIndent = 0
//#endif // (defined _DEBUG)
		);
	virtual void OnComputeCrossReferences(
		CExtGridDataProvider::packed_location_t _loc,
		CExtFormulaDataManager::map_cross_reference_t & _mapCrossReference
		);
	virtual void OnComputeDependentCellsSequence(
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsToCompute,
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
		CExtFormulaDataManager::list_computation_sequence_t & _listSequence
		);
	virtual bool OnComputeDependentCells(
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
		LONG nColNo,
		LONG nRowNo,
		bool bComputeThis
		);
	virtual bool NamedRange_IsValidName( __EXT_MFC_SAFE_LPCTSTR strName ) const;
	virtual LONG NamedRange_GetCount() const;
	virtual POSITION NamedRange_GetStartPosition() const;
	virtual CExtGR2D * NamedRange_GetNext( POSITION & pos, CExtSafeString & strName );
	const CExtGR2D * NamedRange_GetNext( POSITION & pos, CExtSafeString & strName ) const;
	virtual CExtGR2D * NamedRange_Get( __EXT_MFC_SAFE_LPCTSTR strName );
	const CExtGR2D * NamedRange_Get( __EXT_MFC_SAFE_LPCTSTR strName ) const;
	virtual bool NamedRange_Set( __EXT_MFC_SAFE_LPCTSTR strName, CExtGR2D * pRange, bool bClone );
	bool NamedRange_Set( __EXT_MFC_SAFE_LPCTSTR strName, const CExtGR2D & _range );
	virtual bool NamedRange_Remove( __EXT_MFC_SAFE_LPCTSTR strName );
	virtual LONG NamedRange_RemoveAll();
	void _ColorBitmap_EmptyMap();
	CExtBitmap * _ColorBitmap_Get( COLORREF clr );
	const CExtBitmap * _ColorBitmap_Get( COLORREF clr ) const;
}; /// class CExtFormulaDataManager

//////////////////////////////////////////////////////////////
// class CExtFormulaValue

class __PROF_UIS_API CExtFormulaValue
{
public:
	enum e_formula_error_code_t
	{
		__EFEC_OK = 0,
		__EFEC_SYNTAX_ERROR = 1, // not parsed successfully
		__EFEC_NOT_ENOUGH_MEMORY = 2,
		__EFEC_DIVISION_BY_ZERO = 3,
		__EFEC_DIVISION_BY_NEGATIVE = 4,
		__EFEC_INVALID_TYPE_FOR_OPERATION = 5,
		__EFEC_FUNCTION_CANNOT_BE_USED_AS_IDENTIFIER = 6,
		__EFEC_CANNOT_COMPUTE_FACTORIAL = 7,
		__EFEC_ERROR_AS_FUNCTION_EXECUTION_RESULT_OR_CANNOT_COMPUTE_RESULT = 8,
		__EFEC_PARAMETER_IS_OUT_OF_RANGE = 9,
		__EFEC_DEPENDENCY_COMPUTATION_FAILURE = 10, // dependent cell is not computed
		__EFEC_UNKNOWN_IDENTIFIER = 11,
		__EFEC_RESULT_IS_RANGE_REFERENCE = 12,

		__EFEC_TYPE_CONVERSION_ERROR = 20,
		__EFEC_TYPE_R8_CONVERSION_ERROR = 21,
		__EFEC_TYPE_R8_CONVERSION_ERROR_LEFT = 22,
		__EFEC_TYPE_R8_CONVERSION_ERROR_RIGHT = 23,
		__EFEC_TYPE_BOOL_CONVERSION_ERROR = 24,
		__EFEC_TYPE_I4_CONVERSION_ERROR = 25,
		__EFEC_TYPE_BSTR_CONVERSION_ERROR = 26,
		__EFEC_TYPE_DATE_CONVERSION_ERROR = 27,

		__EFEC_STRING_TYPE_REQUIRED_LEFT = 30,
		__EFEC_STRING_TYPE_REQUIRED_RIGHT = 31,
		__EFEC_NON_EMPTY_STRING_REQUIRED = 32,

		__EFEC_FUNCTION_IS_NOT_FOUND = 100,
		__EFEC_FUNCTION_IS_NOT_IMPLEMENTED = 101,
		__EFEC_FUNCTION_INVALID_PARAMETER_COUNT = 102,
		__EFEC_FUNCTION_CANNOT_ACCEPT_EMPTY_PARAMETERS = 103,
		__EFEC_FUNCTION_CANNOT_ACCEPT_CELL_RANGE_PARAMETERS = 104,
	};
	enum e_formula_value_type_t
	{
		__EFVT_EMPTY = 0,
		__EFVT_OLE_VARIANT = 1,
		__EFVT_CELL_RANGE = 2,
	};
	e_formula_value_type_t m_eType;
	CExtGridCellVariant m_val;
	CExtGR2D m_gr;
	CExtFormulaValue();
	CExtFormulaValue( const CExtFormulaValue & other );
	virtual ~CExtFormulaValue();
	static CExtSafeString stat_ErrorDescription( CExtFormulaValue::e_formula_error_code_t err );
	virtual bool PreAdjustDT2R8();
	virtual INT Compare( const CExtFormulaValue & other ) const;
	virtual void Assign( const CExtFormulaValue & other );
	virtual bool IsEmpty() const;
	virtual void Empty();
	CExtFormulaValue & operator = ( const CExtFormulaValue & other ){ Assign( other ); return (*this); }
	bool operator == ( const CExtFormulaValue & other ) const { return ( Compare( other ) == 0 ) ? true : false; }
	bool operator != ( const CExtFormulaValue & other ) const { return ( Compare( other ) != 0 ) ? true : false; }
	bool operator <  ( const CExtFormulaValue & other ) const { return ( Compare( other ) <  0 ) ? true : false; }
	bool operator <= ( const CExtFormulaValue & other ) const { return ( Compare( other ) <= 0 ) ? true : false; }
	bool operator >  ( const CExtFormulaValue & other ) const { return ( Compare( other ) >  0 ) ? true : false; }
	bool operator >= ( const CExtFormulaValue & other ) const { return ( Compare( other ) >= 0 ) ? true : false; }
}; /// class CExtFormulaValue

//////////////////////////////////////////////////////////////
// class CExtFormulaValueArray

class __PROF_UIS_API CExtFormulaValueArray
{
	CExtFormulaValue * m_pArr;
	INT m_nCount;
public:
	CExtFormulaValueArray( INT nCount ) : m_pArr( NULL ), m_nCount( 0 ) { __EXT_DEBUG_GRID_VERIFY( Alloc( nCount ) ); }
	~CExtFormulaValueArray() { Empty(); }
	bool IsEmpty() const { return ( m_pArr == NULL ) ? true : false; }
	void Empty()
	{
		if( m_pArr != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT( m_nCount > 0 );
			delete [] m_pArr;
			m_pArr = NULL;
			m_nCount = 0;
			return;
		}
		__EXT_DEBUG_GRID_ASSERT( m_nCount == 0 );
	}
	bool Alloc( INT nCount )
	{
		Empty();
		if( nCount == 0 )
			return true;
		try
		{
			m_pArr = new CExtFormulaValue[ nCount ];
			m_nCount = nCount;
			return true;
		}
		catch( CException * pException )
		{
			pException->Delete();
		}
		return false;
	}
	INT GetSize() const { return m_nCount; }
	INT GetCount() const { return GetSize(); }
	CExtFormulaValue * GetAt( INT nIndex )
	{
		if( nIndex < 0 || nIndex >= m_nCount )
			return NULL;
		__EXT_DEBUG_GRID_ASSERT( ! IsEmpty() );
		CExtFormulaValue * pFV = m_pArr + nIndex;
		return pFV;
	}
	const CExtFormulaValue * GetAt( INT nIndex ) const { return ( const_cast < CExtFormulaValueArray * > ( this ) ) -> GetAt( nIndex ); }
	CExtFormulaValue & GetRefAt( INT nIndex )
	{
		CExtFormulaValue * pFV = GetAt( nIndex );
		if( pFV == NULL )
			::AfxThrowUserException();
		return (*pFV);
	}
	const CExtFormulaValue & GetRefAt( INT nIndex ) const { return ( const_cast < CExtFormulaValueArray * > ( this ) ) -> GetRefAt( nIndex ); }
	CExtFormulaValue & operator [] ( INT nIndex ) { return GetRefAt( nIndex ); }
	const CExtFormulaValue & operator [] ( INT nIndex ) const { return GetRefAt( nIndex ); }
	CExtFormulaValue::e_formula_error_code_t ComputeValue(
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
		INT nIndex,
		CExtFormulaDataManager & _FDM,
		const CExtFormulaItem * pFI,
		const CExtFormulaItem ** ppErrorLocationForumaItem = NULL,
		VARTYPE vtConvertTo = VT_EMPTY, // default - do not perform conversion
		CExtFormulaValue::e_formula_error_code_t errConversionCode = CExtFormulaValue::__EFEC_TYPE_CONVERSION_ERROR
		);
	CExtFormulaValue::e_formula_error_code_t ComputeAllValues(
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
		CExtFormulaDataManager & _FDM,
		const CExtFormulaItem * pFI,
		const CExtFormulaItem ** ppErrorLocationForumaItem = NULL,
		VARTYPE vtConvertTo = VT_EMPTY, // default - do not perform conversion
		CExtFormulaValue::e_formula_error_code_t errConversionCode = CExtFormulaValue::__EFEC_TYPE_CONVERSION_ERROR
		);
	CExtFormulaValue::e_formula_error_code_t ComputeOR(
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
		bool & bLogicalResult,
		CExtFormulaDataManager & _FDM,
		const CExtFormulaItem * pFI,
		const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
		);
	CExtFormulaValue::e_formula_error_code_t ComputeAND(
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
		bool & bLogicalResult,
		CExtFormulaDataManager & _FDM,
		const CExtFormulaItem * pFI,
		const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
		);
}; /// class CExtFormulaValueArray


//////////////////////////////////////////////////////////////
// class CExtFormulaFunction

class __PROF_UIS_API CExtFormulaFunction
{
public:
	enum e_formula_function_category_t
	{
		__FFC_ENUM_ANY = -1,
		__FFC_LOGICAL = 0,
		__FFC_MATH_AND_TRIG = 1,
		__FFC_STATISTICAL = 2,
		__FFC_TEXT = 3,
		__FFC_DATE_AND_TIME = 4,
		__FFC_LOOKUP_AND_REFERENCE = 5,
		__FFC_INFORMATION = 6,
		__FFC_STD_CATEGORY_COUNT = 7,
		__FFC_USER_UNKNOWN = 1000,
		__FFC_USER_DEFINED = 1000,
	};
	CExtFormulaFunction();
	virtual ~CExtFormulaFunction();
	virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const = 0;
	virtual e_formula_function_category_t GetFunctionCategory() const { return __FFC_USER_UNKNOWN; }
	virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionCategoryName() const;
	virtual bool CanBeIdentifier() const;
	INT GetParameterCountStatic() const;
	virtual INT GetParameterCountMin() const = 0;
	virtual INT GetParameterCountMax() const = 0;
	virtual bool IsParameterCountValid( INT nParameterCount ) const;
	virtual bool CanAcceptRangeParameters() const;
	virtual bool CanAcceptEmptyParameters() const;
	virtual void OnQueryTextFDA(
		INT nTextIndex, // 0 - function description, positive - parameter description by index, -negative - parameter abreviation by index
		CExtSafeString & strText
		) const;
	CExtSafeString GetFunctionDescription() const;
	CExtSafeString GetParameterAbreviationFormat( INT nParameterIndex ) const;
	CExtSafeString GetParameterDescription( INT nParameterIndex ) const;
	virtual CExtFormulaValue::e_formula_error_code_t Exec(
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
		CExtFormulaValue & _FV,
		CExtFormulaDataManager & _FDM,
		const CExtFormulaItem * pFI,
		const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
		);
	virtual CExtFormulaValue::e_formula_error_code_t OnExec(
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
		CExtFormulaValue & _FV,
		CExtFormulaDataManager & _FDM,
		const CExtFormulaItem * pFI,
		const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
		) = 0;
	struct __PROF_UIS_API for_each_FF_t : public CExtGridDataProvider::IDataProviderForEachCallBack
	{ // also used as for-each data type
		CExtFormulaDataManager::map_formula_cells_t & m_mapCellsAlreadyComputed;
		CExtFormulaDataManager & m_FDM;
		CExtGridDataProvider & m_DP;
		const CExtFormulaItem & m_FI;
		ULONG m_nColCountReserved, m_nRowCountReserved;
		CExtGR2D m_range;
		const CExtGR2D & m_rangeNS;
		CExtGridDataProvider::IDataProviderForEachCallBack & m_DPFECB;
		ULONG m_nCounter, m_nColNo, m_nRowNo;
		CExtFormulaValue::e_formula_error_code_t m_err;
		const CExtFormulaItem * m_pErrorLocationForumaItem;
		LPVOID m_pCustomData;
		INT m_nParameterIndex;
		bool m_bFirstIsComputedInLogicalChain:1;
		for_each_FF_t( CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed, CExtFormulaDataManager & _FDM, const CExtFormulaItem & _FI, const CExtGR2D & _range, CExtGridDataProvider::IDataProviderForEachCallBack & _DPFECB )
			: m_mapCellsAlreadyComputed( _mapCellsAlreadyComputed )
			, m_FDM( _FDM )
			, m_DP( _FDM.OnQueryDataProvider() )
			, m_FI( _FI )
			, m_range( _range )
			, m_rangeNS( _range )
			, m_DPFECB( _DPFECB )
			, m_nColCountReserved( 0L )
			, m_nRowCountReserved( 0L )
			, m_pCustomData( NULL )
			, m_nCounter( 0L )
			, m_nColNo( ULONG(-1L) )
			, m_nRowNo( ULONG(-1L) )
			, m_err( CExtFormulaValue::__EFEC_OK )
			, m_pErrorLocationForumaItem( NULL )
			, m_nParameterIndex( -1 )
			, m_bFirstIsComputedInLogicalChain( true )
		{
			m_DP.CacheReservedCountsGet( &m_nColCountReserved, &m_nRowCountReserved );
			m_range.Shift( LONG(m_nColCountReserved), LONG(m_nRowCountReserved) );
		}
		virtual ~for_each_FF_t()
		{
		}
		virtual bool OnDataProviderForEach( LPVOID pData, CExtGridCell & _cell, ULONG nCounter, ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			_range;
			return m_DPFECB.OnDataProviderForEach( pData, _cell, nCounter, nColNo - m_nColCountReserved, nRowNo - m_nRowCountReserved, m_rangeNS );
		}
		bool ComputeRange()
		{
			return m_DP.ForEachCell( m_range, this, LPVOID(this), &m_nCounter, &m_nColNo, &m_nRowNo );
		}
	}; /// struct for_each_FF_t
}; /// class CExtFormulaFunction

//////////////////////////////////////////////////////////////
// template class CFFP
// formula function decorator, parameter limits and function category binding

template < class _BFFC, INT nFPPCMin, INT nFPPCMax, CExtFormulaFunction::e_formula_function_category_t eFFC >
	class CFFP
		: public _BFFC
{
public:
	virtual CExtFormulaFunction::e_formula_function_category_t GetFunctionCategory() const { return eFFC; }
	virtual INT GetParameterCountMin() const { return nFPPCMin; }
	virtual INT GetParameterCountMax() const { return nFPPCMax; }
}; /// template class CFFP

//////////////////////////////////////////////////////////////
// template classes CFFPCC1T, CFFPCC1
// formula function decorator, single parameter/value check function implementation (ISERROR, ISBLANK, ISNUMBER etc.)
// CFFPCC1T - customizable decorator, CFFPCC1 - pre-decorated CExtFormulaFunction version

template < class _BFFC, CExtFormulaFunction::e_formula_function_category_t eFFC >
	class CFFPCC1T
		: public CFFP < _BFFC, 1, 1, eFFC >
{
public:
	virtual CExtFormulaValue::e_formula_error_code_t OnCheckError(
		CExtFormulaValue::e_formula_error_code_t err,
		bool & bCheckResult,
		CExtFormulaValue & _FV,
		CExtFormulaDataManager & _FDM,
		const CExtFormulaItem * pFI,
		const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
		)
	{
		__EXT_DEBUG_GRID_ASSERT( err != CExtFormulaValue::__EFEC_OK );
		bCheckResult;
		_FV;
		_FDM;
		if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
			(*ppErrorLocationForumaItem) = pFI;
		return err;
	}
	virtual CExtFormulaValue::e_formula_error_code_t OnCheckValue(
		bool & bCheckResult,
		CExtFormulaValue & _FV,
		CExtFormulaDataManager & _FDM,
		const CExtFormulaItem * pFI,
		const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
		)
	{
		bCheckResult;
		_FV;
		_FDM;
		pFI;
		ppErrorLocationForumaItem;
		return CExtFormulaValue::__EFEC_OK;
	}
	virtual CExtFormulaValue::e_formula_error_code_t OnExec(
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
		CExtFormulaValue & _FV,
		CExtFormulaDataManager & _FDM,
		const CExtFormulaItem * pFI,
		const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
		)
	{
		__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
		__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
		__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
		const CExtFormulaItem * pPFI = pFI->GetChildAt( 0 );
		CExtFormulaValue::e_formula_error_code_t err = pPFI->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
		bool bCheckResult = false;
		if( err != CExtFormulaValue::__EFEC_OK )
			err = OnCheckError( err, bCheckResult, _FV, _FDM, pPFI, ppErrorLocationForumaItem );
		else
			err = OnCheckValue( bCheckResult, _FV, _FDM, pPFI, ppErrorLocationForumaItem );
		if( err == CExtFormulaValue::__EFEC_OK )
		{
			_FV.m_val._VariantAssign( bCheckResult ? true : false );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
		}
		return err;
	}
}; /// template class CFFPCC1T

template < CExtFormulaFunction::e_formula_function_category_t eFFC >
	class CFFPCC1
		: public CFFPCC1T < CExtFormulaFunction, eFFC >
{
}; /// template class CFFPCC1

//////////////////////////////////////////////////////////////
// class CExtFormulaItem

class __PROF_UIS_API CExtFormulaItem
{
public:
	enum formula_atom_type_t
	{
		__FORMULA_ATOM_UNKNOWN,
		__FORMULA_ATOM_IDENTIFIER,
		__FORMULA_ATOM_CONSTANT_INT,
		__FORMULA_ATOM_CONSTANT_FLOAT,
		//__FORMULA_ATOM_CONSTANT_CHAR,
		__FORMULA_ATOM_CONSTANT_STRING,
		__FORMULA_ATOM_EXPR_MULTIPLY,
		__FORMULA_ATOM_EXPR_DIVIDE,
		//__FORMULA_ATOM_EXPR_MODULE,
		__FORMULA_ATOM_EXPR_PERCENT,
		__FORMULA_ATOM_EXPR_PLUS,
		__FORMULA_ATOM_EXPR_MINUS,
		__FORMULA_ATOM_EXPR_LESS,
		__FORMULA_ATOM_EXPR_GREATER,
		__FORMULA_ATOM_EXPR_GREATER_EQUAL,
		__FORMULA_ATOM_EXPR_LESS_EQUAL,
		__FORMULA_ATOM_EXPR_NOT_EQUAL,
		__FORMULA_ATOM_EXPR_STR_CAT,
		__FORMULA_ATOM_EXPR_POW,
		__FORMULA_ATOM_EXPR_EQUAL,
		__FORMULA_ATOM_TERM_UNARY_MINUS,
		__FORMULA_ATOM_FUNC_CALL,
		__FORMULA_ATOM_CELL_RANGE,
		__FORMULA_ATOM_CELL_RANGE_INTERSECTION,
	}; // enum formula_atom_type_t
protected:
	typedef CTypedPtrArray < CPtrArray, CExtFormulaItem * > child_arr_t;
	child_arr_t m_arrChildren;
	CExtFormulaItem * m_pParent;
public:
	CExtSafeString m_strName, m_strNameHelper;
	formula_atom_type_t m_eType;
	INT m_nScanPosStart, m_nScanPosEnd;
	CPoint m_ptScanPosStart, m_ptScanPosEnd;
	bool m_bBold:1, m_bItalic:1;
	COLORREF m_clr;
	mutable CRect m_rcEditedRange, m_rcEditedRangeNN;
	mutable CExtGR2D m_rangeEditedNamed;
	CExtFormulaItem(
		formula_atom_type_t eType = __FORMULA_ATOM_UNKNOWN
		);
	virtual ~CExtFormulaItem();
	virtual INT InsertChildAt( CExtFormulaItem * pChild, INT nIndex );
	virtual void AddChild( CExtFormulaItem * pChild );
	virtual void AddChildrenOf( CExtFormulaItem * pOther, bool bDeleteOther );
	virtual void CopyScanInfoFrom(
		const CExtFormulaItem * pOther,
		bool bCopyFromChildren,
		bool bCopyStartInfo = true,
		bool bCopyEndInfo = true
		);
	virtual void RemoveAllChildren();
	virtual void DetachAllChildren();
	virtual INT GetChildCount() const;
	virtual CExtFormulaItem * GetChildAt( INT nIndex );
	const CExtFormulaItem * GetChildAt( INT nIndex ) const;
	INT GetIndexOfChild( const CExtFormulaItem * pChild ) const;
	CExtFormulaItem * GetParent();
	const CExtFormulaItem * GetParent() const;
	INT GetIndentLevel() const;
	virtual CExtFormulaValue::e_formula_error_code_t ComputeValue(
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
		CExtFormulaValue & _FV,
		CExtFormulaDataManager & _FDM,
		const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
		) const;
	virtual bool ComputeSimpleValue( CExtFormulaValue & _FV ) const;
	virtual CExtSafeString FormulaText() const;
	virtual void FormatFormulaInplaceEdit( CExtFormulaInplaceEdit & wndFIE );
	void UnionScanPos( INT & nScanPosStart, INT & nScanPosEnd ) const;
	virtual void ShiftRangesInText(
		CExtFormulaDataManager & _FDM,
		CExtSafeString & strFormulaText,
		LONG nShiftX,
		LONG nShiftY,
		const CExtGR2D * pRangeToShift = NULL
		);
	virtual CExtFormulaItem * FindFunction( INT nScanPos, INT & nParameterIndex );
	const CExtFormulaItem * FindFunction( INT nScanPos, INT & nParameterIndex ) const;
	virtual CExtFormulaItem * FindItem( INT nScanPosStart, INT nScanPosEnd );
	const CExtFormulaItem * FindItem( INT nScanPosStart, INT nScanPosEnd ) const;
	virtual CExtFormulaItem * FindRangeItem( INT nScanPosStart, INT nScanPosEnd );
	const CExtFormulaItem * FindRangeItem( INT nScanPosStart, INT nScanPosEnd ) const;
	virtual void UnionInplaceHighlightedRange( CExtFormulaDataManager & _FDM, CExtGR2D & _range ) const;
	virtual void ComputeDependencySourceRange(
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
		CExtFormulaDataManager & _FDM,
		CExtGR2D & _range
		) const;
	void RemoveFromAI( CExtHashMapT < CExtFormulaItem *, CExtFormulaItem *, bool, bool > & _mapAI );
#if (defined __EXT_DEBUG_HEAVY_FORMULA_PARSER) 
	bool __Debug_Trace_IsLastSibling() const
	{
		if( this == NULL )
			return true;
		const CExtFormulaItem *  pParent = GetParent();
		if( pParent == NULL )
			return true;
		if( pParent->GetIndexOfChild( this ) == ( pParent->GetChildCount() - 1 ) )
			return true;
		return false;
	}
	CExtSafeString __Debug_Trace_GetString() const
	{
		CExtSafeString strTrace = _T("");
		if( this == NULL )
		{
			strTrace = _T("NULL");
			return strTrace;
		}
		const CExtFormulaItem *  pItem = this;
		for( ; pItem != NULL; pItem = pItem->GetParent() )
		{
			if( ((void*)pItem) == ((void*)this) )
			{
				CExtSafeString s = strTrace;
				strTrace = _T(" |---");
				strTrace += s;
			}
			else
			{
				if( pItem->__Debug_Trace_IsLastSibling() )
				{
					CExtSafeString s = strTrace;
					strTrace = _T("     ");
					strTrace += s;
				}
				else
				{
					CExtSafeString s = strTrace;
					strTrace = _T(" |   ");
					strTrace += s;
				}
			}
		}
		TCHAR buf[128];
		__EXT_MFC_SPRINTF( buf, _T("(%d) "), (INT)m_eType );
    // W4 Changed  The sprintf terminates the buf variable 05012013
#pragma warning(suppress: 6054)
		strTrace += buf;
		strTrace += _T(" ");
		strTrace += m_strName;
		if( m_strNameHelper.GetLength() > 0 )
		{
			strTrace += _T(" (");
			strTrace += m_strNameHelper;
			strTrace += _T(")");
		}
		CExtSafeString strScanPos;
		strScanPos.Format(
			_T("   SR=%d:%d, TR=%d:%d:%d:%d"),
			m_nScanPosStart,
			m_nScanPosEnd,
			m_ptScanPosStart.x,
			m_ptScanPosStart.y,
			m_ptScanPosEnd.x,
			m_ptScanPosEnd.y
			);
		strTrace += strScanPos;
		return strTrace;
	}
	void Trace_Item( FILE * f ) const
	{
		CExtSafeString strTrace = __Debug_Trace_GetString();
		_fputts( LPCTSTR(strTrace), f );
		_fputts( _T("\r\n"), f );
	}
	void __Debug_Trace( FILE * f ) const
	{
		Trace_Item( f );
		if( this == NULL )
			return;
		INT nIndex, nCount = GetChildCount();
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			const CExtFormulaItem * pChild = GetChildAt( nIndex );
			pChild->__Debug_Trace( f );
		}
	}
#endif // (defined __EXT_DEBUG_HEAVY_FORMULA_PARSER) 
}; /// class CExtFormulaItem

//////////////////////////////////////////////////////////////
// class CExtFormulaFunctionSet

#define __EXT_FF_DESCRIPTION( __STR__ ) virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionDescription() const { return __STR__ ; }

#define __EXT_FF_DESCRIPTION_BEGIN \
	virtual void OnQueryTextFDA( INT nTextIndex, CExtSafeString & strText ) const { strText.Empty(); switch( nTextIndex ) {

#define __EXT_FF_DESCRIPTION_END \
		} }

#define __EXT_FF_FUNC( __STR_FUNCTION_DESCRIPTION__ ) \
		case 0 : strText = __STR_FUNCTION_DESCRIPTION__ ; break;
#define __EXT_FF_PARM( __INT_PARAMETER_INDEX__ , __STR_PARAMETER_ABREVIATION__ , __STR_PARAMETER_DESCRIPTION__ ) \
		case (-__INT_PARAMETER_INDEX__) : strText = __STR_PARAMETER_ABREVIATION__ ; break; \
		case   __INT_PARAMETER_INDEX__  : strText = __STR_PARAMETER_DESCRIPTION__ ; break;

class __PROF_UIS_API CExtFormulaFunctionSet
{
	CExtSafeMapStringToPtr m_map;
public:
	CExtFormulaFunctionSet();
	virtual ~CExtFormulaFunctionSet();
	virtual CExtFormulaFunction * GetFunction( __EXT_MFC_SAFE_LPCTSTR strFunctionName );
	virtual bool AddFunction(
		CExtFormulaFunction * pFF,
		bool bErrorIfAlreadyRegistered = false
		);
	bool RemoveFunction( CExtFormulaFunction * pFF );
	virtual bool RemoveFunction( __EXT_MFC_SAFE_LPCTSTR strFunctionName );
	virtual void RemoveAllFunctions();

	virtual LONG EnumGetCount(
		CExtFormulaFunction::e_formula_function_category_t eFFC = CExtFormulaFunction::__FFC_ENUM_ANY
		) const;
	virtual POSITION EnumStart() const;
	virtual CExtFormulaFunction * EnumNext(
		POSITION & pos,
		CExtFormulaFunction::e_formula_function_category_t eFFC = CExtFormulaFunction::__FFC_ENUM_ANY
		);
	const CExtFormulaFunction * EnumNext(
		POSITION & pos,
		CExtFormulaFunction::e_formula_function_category_t eFFC = CExtFormulaFunction::__FFC_ENUM_ANY
		) const;

	virtual bool AddCategoryFunctions(
		CExtFormulaFunction::e_formula_function_category_t eFFC,
		bool bErrorIfAlreadyRegistered = false
		);
	virtual bool AddAllFunctions(
		bool bErrorIfAlreadyRegistered = false
		);
	virtual bool RegisterAllFunctions();

	class __PROF_UIS_API FF_Logical_FALSE : public CFFP < CExtFormulaFunction, 0, 0, CExtFormulaFunction::__FFC_LOGICAL >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("FALSE"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the logical value FALSE.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT(
					pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL
				||	(	pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_IDENTIFIER
					&&	CanBeIdentifier()
					)
				);
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			_mapCellsAlreadyComputed;
			_FDM;
			pFI;
			ppErrorLocationForumaItem;
			_FV.m_val._VariantAssign( false );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_Logical_FALSE

	class __PROF_UIS_API FF_Logical_TRUE : public CFFP < CExtFormulaFunction, 0, 0, CExtFormulaFunction::__FFC_LOGICAL >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("TRUE"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the logical value TRUE.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT(
					pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL
				||	(	pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_IDENTIFIER
					&&	CanBeIdentifier()
					)
				);
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			_mapCellsAlreadyComputed;
			_FDM;
			pFI;
			ppErrorLocationForumaItem;
			_FV.m_val._VariantAssign( true );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_Logical_TRUE

	class __PROF_UIS_API FF_Logical_OR : public CFFP < CExtFormulaFunction, 1, -1, CExtFormulaFunction::__FFC_LOGICAL >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("OR"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Checks whether any of arguments are TRUE, and returns TRUE or FALSE. Returns FALSE only if all arguments are FALSE.") )
			__EXT_FF_PARM( 1, _T("logical%d"), _T("Boolean parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			bool bLogicalResult = false;
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeOR( _mapCellsAlreadyComputed, bLogicalResult, _FDM, pFI, ppErrorLocationForumaItem );
			_FV.m_val._VariantAssign( bLogicalResult );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Logical_OR

	class __PROF_UIS_API FF_Logical_AND : public CFFP < CExtFormulaFunction, 1, -1, CExtFormulaFunction::__FFC_LOGICAL >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("AND"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Checks whether all arguments are TRUE, and returns TRUE if all arguments are TRUE.") )
			__EXT_FF_PARM( 1, _T("logical%d"), _T("Boolean parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			bool bLogicalResult = false;
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAND( _mapCellsAlreadyComputed, bLogicalResult, _FDM, pFI, ppErrorLocationForumaItem );
			_FV.m_val._VariantAssign( bLogicalResult );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Logical_AND

	class __PROF_UIS_API FF_Logical_IF : public CFFP < CExtFormulaFunction, 2, 3, CExtFormulaFunction::__FFC_LOGICAL >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("IF"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Checks whether condition is met, and returns one value if TRUE, and another value if FALSE.") )
			__EXT_FF_PARM( 1, _T("logical_test"), _T("Condition parameter.") )
			__EXT_FF_PARM( 2, _T("value_if_true"), _T("Return value if true.") )
			__EXT_FF_PARM( 3, _T("value_if_false"), _T("Return value if false.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			const CExtFormulaItem * pFI_condition = pFI->GetChildAt( 0 );
			CExtFormulaValue::e_formula_error_code_t err = pFI_condition->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			if( ! _FV.m_val._VariantChangeType( VT_BOOL ) )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI_condition;
				return CExtFormulaValue::__EFEC_TYPE_BOOL_CONVERSION_ERROR;
			}
			if( _FV.m_val.boolVal != VARIANT_FALSE )
			{ // compute and return 2nd
				const CExtFormulaItem * pFI_choice = pFI->GetChildAt( 1 );
				err = pFI_choice->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
				if( err != CExtFormulaValue::__EFEC_OK )
					return err;
				_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			} // compute and return 2nd
			else
			{ // compute and return optional 3rd
				if( nParameterCount < 3 )
				{
					_FV.m_val._VariantAssign( false );
					_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
				}
				else
				{
					const CExtFormulaItem * pFI_choice = pFI->GetChildAt( 2 );
					err = pFI_choice->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
					if( err != CExtFormulaValue::__EFEC_OK )
						return err;
					_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
				}
			} // compute and return optional 3rd
			return err;
		}
	}; /// class FF_Logical_IF

	class __PROF_UIS_API FF_Logical_IFERROR : public CFFP < CExtFormulaFunction, 2, 2, CExtFormulaFunction::__FFC_LOGICAL >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("IFERROR"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns value_if_error if expression is an error and the value of the expression itself otherwise.") )
			__EXT_FF_PARM( 1, _T("value"), _T("Value to check error.") )
			__EXT_FF_PARM( 2, _T("value_if_error"), _T("Return value on error.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem);
			if( err != CExtFormulaValue::__EFEC_OK )
				err = pFI->GetChildAt(1)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem);
			return err;
		}
	}; /// class FF_Logical_IFERROR

	class __PROF_UIS_API FF_Logical_NOT : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_LOGICAL >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("NOT"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Changes FALSE to TRUE, or TRUE to FALSE.") )
			__EXT_FF_PARM( 1, _T("logical"), _T("Boolean parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			if( ! _FV.m_val._VariantChangeType( VT_BOOL ) )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_TYPE_BOOL_CONVERSION_ERROR;
			}
#pragma warning(disable: 4310)
			_FV.m_val.boolVal = (VARIANT_BOOL)( ( _FV.m_val.boolVal == VARIANT_FALSE ) ? VARIANT_TRUE : VARIANT_FALSE );
#pragma warning(default: 4310)
			return err;
		}
	}; /// class FF_Logical_NOT

	class __PROF_UIS_API FF_MathTrig__impl0 : public CFFP < CExtFormulaFunction, 0, 0, CExtFormulaFunction::__FFC_MATH_AND_TRIG >
	{
	public:
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec0( double & lfDst ) = 0;
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT(
					pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL
				||	(	pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_IDENTIFIER
					&&	CanBeIdentifier()
					)
				);
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			_mapCellsAlreadyComputed;
			_FDM;
			pFI;
			ppErrorLocationForumaItem;
			double lfDst = 0.0;
			CExtFormulaValue::e_formula_error_code_t err = OnMathTrigExec0( lfDst );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			_FV.m_val._VariantAssign( lfDst, VT_R8 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_MathTrig__impl0

	class __PROF_UIS_API FF_MathTrig_PI : public FF_MathTrig__impl0
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("PI"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns value of Pi, 3.1415926535897932384626433832795, accurate to 31 digits.") )
		__EXT_FF_DESCRIPTION_END
		virtual bool CanBeIdentifier() const { return false; } // PI should not be used as identifier
		CExtFormulaValue::e_formula_error_code_t OnMathTrigExec0( double & lfDst )
		{
			lfDst = 3.1415926535897932384626433832795;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_PI

	class __PROF_UIS_API FF_MathTrig_RAND : public FF_MathTrig__impl0
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("RAND"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns a random number greater than or equal to 0 and less than 1, evenly distributed (changes or recalculation).") )
		__EXT_FF_DESCRIPTION_END
		CExtFormulaValue::e_formula_error_code_t OnMathTrigExec0( double & lfDst )
		{
			INT nRand = ::rand() % ( INT_MAX / 2 );
			lfDst = double( nRand ) / double( INT_MAX / 2 );
			if( lfDst < 0.0 )
				lfDst = 0.0;
			else if( lfDst > 1.0 )
				lfDst = 1.0;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_RAND

	class __PROF_UIS_API FF_MathTrig__impl1 : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_MATH_AND_TRIG >
	{
	public:
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc ) = 0;
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt( 0 )->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			if( ! _FV.m_val._VariantChangeType( VT_R8 ) )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
			}
			double lfDst = 0.0;
			err = OnMathTrigExec1( lfDst, _FV.m_val.dblVal );
			if( err != CExtFormulaValue::__EFEC_OK )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return err;
			}
			_FV.m_val.dblVal = lfDst;
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_MathTrig__impl1

	class __PROF_UIS_API FF_MathTrig_ABS : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ABS"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the absolute value of a number, a number without its sign.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::fabs( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_SIN : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("SIN"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the sine of an angle.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::sin( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_COS : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("COS"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the cosine of an angle.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::cos( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_TAN : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("TAN"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the tangent of an angle.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::tan( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_SINH : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("SINH"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the hyperbolic sine of an angle.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::sinh( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_COSH : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("COSH"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the hyperbolic cosine of an angle.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::cosh( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_TANH : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("TANH"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the hyperbolic tangent of an angle.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::tanh( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_ASIN : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ASIN"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the arcsine of a number in radians, in the range -Pi/2 to Pi/2.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::asin( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_ACOS : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ACOS"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the arccosine of a number in radians, in the range 0 to Pi. The arccosine is the number whose cosine is Number.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::acos( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_ATAN : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ATAN"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the arctangent of a number in radians, in the range -Pi/2 to Pi/2.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::atan( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

//	class __PROF_UIS_API FF_MathTrig_ASINH : public FF_MathTrig__impl1
//	{
//	public:
//		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ASINH"); }
//		__EXT_FF_DESCRIPTION_BEGIN
//			__EXT_FF_FUNC( _T("Returns the inverse hyperbolic sine of a number.") )
//			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
//		__EXT_FF_DESCRIPTION_END
//		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
//		{
//			lfDst = (double)::asinh( lfSrc );
//			return CExtFormulaValue::__EFEC_OK;
//		}
//	}; /// class FF_MathTrig_ABS
//
//	class __PROF_UIS_API FF_MathTrig_ACOSH : public FF_MathTrig__impl1
//	{
//	public:
//		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ACOSH"); }
//		__EXT_FF_DESCRIPTION_BEGIN
//			__EXT_FF_FUNC( _T("Returns the inverse hyperbolic cosine of a number.") )
//			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
//		__EXT_FF_DESCRIPTION_END
//		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
//		{
//			lfDst = (double)::acosh( lfSrc );
//			return CExtFormulaValue::__EFEC_OK;
//		}
//	}; /// class FF_MathTrig_ABS
//
//	class __PROF_UIS_API FF_MathTrig_ATANH : public FF_MathTrig__impl1
//	{
//	public:
//		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ATANH"); }
//		__EXT_FF_DESCRIPTION_BEGIN
//			__EXT_FF_FUNC( _T("Returns the inverse hyperbolic tangent of a number.") )
//			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
//		__EXT_FF_DESCRIPTION_END
//		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
//		{
//			lfDst = (double)::atanh( lfSrc );
//			return CExtFormulaValue::__EFEC_OK;
//		}
//	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_SQRT : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("SQRT"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the square root of a number.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::sqrt( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_SQRTPI : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("SQRTPI"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the square root of a (number * Pi).") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::sqrt( lfSrc * 3.1415926535897932384626433832795 );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_SIGN : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("SIGN"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the sign of a number: 1 if the number is positive, zero if the number is zero, or -1 if the number is negative.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			if( lfSrc < 0.0 )
				lfDst = -1.0;
			else if( lfSrc > 0.0 )
				lfDst = 1.0;
			else
				lfDst = 0.0;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_DEGREES : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("DEGREES"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Converts radians to degrees.") )
			__EXT_FF_PARM( 1, _T("angle"), _T("Angle parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = lfSrc * 180.0 / 3.1415926535897932384626433832795;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_RADIANS : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("RADIANS"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Converts degrees to radians.") )
			__EXT_FF_PARM( 1, _T("angle"), _T("Angle parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = lfSrc * 3.1415926535897932384626433832795 / 180.0;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_EXP : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("EXP"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns e raised to the power of a given number.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double) ::pow( 2.71828182845904523536, lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_LN : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("LN"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns natural logarithm of a number.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::log( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_LOG : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("LOG"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns logarithm of a number to the base you specify.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("base"), _T("Base number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::log10( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_LOG10 : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("LOG10"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the base-10 logarithm of a number.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::log10( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_FACT : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("FACT"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the factorial of a number, equal to 1*2*3*...*Number.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			INT nIndex =  ::fmodl( lfSrc, 1.0 ) >= 0.5 ? INT( ::ceil(lfSrc) ) : INT( ::floor(lfSrc) );
			if( nIndex < 0 || nIndex > 170 )
				return CExtFormulaValue::__EFEC_CANNOT_COMPUTE_FACTORIAL;
			lfDst = 1.0;
			INT nStep;
			for( nStep = 0; nStep < nIndex; nStep ++ )
			{
				lfDst = lfDst * (nStep+1);
			}
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_EVEN : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("EVEN"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Rounds a positive number up and negative number down to the nearest even integer.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::ceil( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_INT : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("INT"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Rounds a number down to the nearest integer.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			lfDst = (double)::floor( lfSrc );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig_ODD : public FF_MathTrig__impl1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ODD"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Rounds a positive number up and negative number down to the nearest odd integer.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec1( double & lfDst, double lfSrc )
		{
			double lf = (double)::ceil( ::fabs( lfSrc ) );
			INT n = (INT)lf;
			if( ( n & 1 ) == 0 )
				n ++;
			if( n == 0 )
				n = 1;
			lfDst = (double) ( ( lfSrc < 0 ) ? (-n) : n );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ABS

	class __PROF_UIS_API FF_MathTrig__impl2 : public CFFP < CExtFormulaFunction, 2, 2, CExtFormulaFunction::__FFC_MATH_AND_TRIG >
	{
	public:
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec2( double & lfDst, double lfSrc1, double lfSrc2 ) = 0;
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue _FV1, _FV2;
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt( 0 )->ComputeValue( _mapCellsAlreadyComputed, _FV1, _FDM, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			if( ! _FV1.m_val._VariantChangeType( VT_R8 ) )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
			}
			err = pFI->GetChildAt( 1 )->ComputeValue( _mapCellsAlreadyComputed, _FV2, _FDM, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			if( ! _FV2.m_val._VariantChangeType( VT_R8 ) )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
			}
			double lfDst = 0.0;
			err = OnMathTrigExec2( lfDst, _FV1.m_val.dblVal, _FV2.m_val.dblVal );
			if( err != CExtFormulaValue::__EFEC_OK )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return err;
			}
			_FV.m_val._VariantAssign( lfDst, VT_R8 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_MathTrig__impl2

	class __PROF_UIS_API FF_MathTrig_RANDBETWEEN : public FF_MathTrig__impl2
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("RANDBETWEEN"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns a random number between the numbers you specify.") )
			__EXT_FF_PARM( 1, _T("bottom"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("top"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec2( double & lfDst, double lfSrc1, double lfSrc2 )
		{
			double lfMin = min( lfSrc1, lfSrc2 ), lfMax = max( lfSrc1, lfSrc2 );
			if( lfMin == lfMax )
			{
				lfDst = lfMin;
				return CExtFormulaValue::__EFEC_OK;
			}
			double lfDistance = lfMax - lfMin;
			__EXT_DEBUG_GRID_ASSERT( lfDistance > 0.0 );
			INT nDistance = INT(lfDistance);
			nDistance = max( 1, nDistance );
			INT nRand = ::rand() % nDistance;
			lfDst = double( nRand ) + lfMin;
			if( lfDst < lfMin )
				lfDst = lfMin;
			else if( lfDst > lfMax )
				lfDst = lfMax;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_RANDBETWEEN

	class __PROF_UIS_API FF_MathTrig_POWER : public FF_MathTrig__impl2
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("POWER"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the result of a number raised to a power.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("power"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec2( double & lfDst, double lfSrc1, double lfSrc2 )
		{
			lfDst = (double)::pow( lfSrc1, lfSrc2 );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_POWER

	class __PROF_UIS_API FF_MathTrig_MOD : public FF_MathTrig__impl2
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("MOD"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns a remainder after a number is divided by a divisor.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("divisor"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec2( double & lfDst, double lfSrc1, double lfSrc2 )
		{
			if( lfSrc2 == 0.0 )
				return CExtFormulaValue::__EFEC_DIVISION_BY_ZERO;
			if( lfSrc1 < 0.0 )
			{
				lfDst = ::ceil( -lfSrc1 ) + lfSrc1;
				__EXT_DEBUG_GRID_ASSERT( 0.0 <= lfDst && lfDst <= 1.0 );
			}
			else
				lfDst = (double)::fmod( lfSrc1, lfSrc2 );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_MOD

	class __PROF_UIS_API FF_MathTrig_TRUNC : public FF_MathTrig__impl2
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("TRUNC"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Truncates a numer to an iteger by removing the decimal, or fractional, part of the number.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("num_digits"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec2( double & lfDst, double lfSrc1, double lfSrc2 )
		{
			double lfIndex = lfSrc2 - (double)::fmodl( lfSrc2, 1.0 );
			double lfMulShift = (double)::pow( 10.0, lfIndex );
			double lfAdjusted = lfSrc1 * lfMulShift;
			lfAdjusted = lfAdjusted - (double)::fmodl( lfAdjusted, 1.0 );
			lfDst = lfAdjusted / lfMulShift;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_TRUNC

	class __PROF_UIS_API FF_MathTrig_ROUND : public FF_MathTrig__impl2
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ROUND"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Rounds a number to a specified number of digits.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("num_digits"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec2( double & lfDst, double lfSrc1, double lfSrc2 )
		{
			double lfIndex = lfSrc2 - (double)::fmodl( lfSrc2, 1.0 );
			double lfMulShift = (double)::pow( 10.0, lfIndex );
			double lfAdjusted = lfSrc1 * lfMulShift;
			lfAdjusted = ::fmodl( lfAdjusted, 1.0 ) >= 0.5 ? ((double)::ceil(lfAdjusted)) : ((double)::floor(lfAdjusted));
			lfDst = lfAdjusted / lfMulShift;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ROUND

	class __PROF_UIS_API FF_MathTrig_ROUNDUP : public FF_MathTrig__impl2
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ROUNDUP"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Rounds a number up, away from zero.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("num_digits"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec2( double & lfDst, double lfSrc1, double lfSrc2 )
		{
			double lfIndex = lfSrc2 - (double)::fmodl( lfSrc2, 1.0 );
			double lfMulShift = (double)::pow( 10.0, lfIndex );
			double lfAdjusted = lfSrc1 * lfMulShift;
			lfAdjusted = ::ceil( lfAdjusted );
			lfDst = lfAdjusted / lfMulShift;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ROUNDUP

	class __PROF_UIS_API FF_MathTrig_ROUNDDOWN : public FF_MathTrig__impl2
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ROUNDDOWN"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Rounds a number down, toward zero.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("num_digits"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec2( double & lfDst, double lfSrc1, double lfSrc2 )
		{
			double lfIndex = lfSrc2 - (double)::fmodl( lfSrc2, 1.0 );
			double lfMulShift = (double)::pow( 10.0, lfIndex );
			double lfAdjusted = lfSrc1 * lfMulShift;
			lfAdjusted = ::floor( lfAdjusted );
			lfDst = lfAdjusted / lfMulShift;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_ROUNDDOWN

	class __PROF_UIS_API FF_MathTrig_CEILING : public FF_MathTrig__impl2
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("CEILING"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Rounds a number up, to the nearest integer or to the nearest multiple of significance.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("significance"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec2( double & lfDst, double lfSrc1, double lfSrc2 )
		{
			if( lfSrc1 < 0.0 || lfSrc2 < 0.0 )
				return CExtFormulaValue::__EFEC_DIVISION_BY_NEGATIVE;
			if( lfSrc2 == 0.0 )
				lfDst = 0.0;
			else
			{
				lfDst = lfSrc1 - (double)::fmodl( lfSrc1, lfSrc2 );
				if( lfDst != lfSrc1 )
					lfDst += lfSrc2;
			}
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_CEILING

	class __PROF_UIS_API FF_MathTrig_FLOOR : public FF_MathTrig__impl2
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("FLOOR"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Rounds a number down, toward zero, to the nearest multiple of significance.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("significance"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnMathTrigExec2( double & lfDst, double lfSrc1, double lfSrc2 )
		{
			if( lfSrc1 < 0.0 || lfSrc2 < 0.0 )
				return CExtFormulaValue::__EFEC_DIVISION_BY_NEGATIVE;
			if( lfSrc2 == 0.0 )
				lfDst = 0.0;
			else
				lfDst = lfSrc1 - (double)::fmodl( lfSrc1, lfSrc2 );
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_MathTrig_FLOOR

	class __PROF_UIS_API FF_Statistical_MIN
		: public CFFP < CExtFormulaFunction, 1, -1, CExtFormulaFunction::__FFC_STATISTICAL >
		, public CExtGridDataProvider::IDataProviderForEachCallBack
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("MIN"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the smallest number in a set of values. Ignores logical values and text.") )
			__EXT_FF_PARM( 1, _T("number%d"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual bool CanAcceptRangeParameters() const { return true; }
		virtual bool OnDataProviderForEach( LPVOID pData, CExtGridCell & _cell, ULONG nCounter, ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			__EXT_DEBUG_GRID_ASSERT( pData != NULL );
			_range;
			_cell;
			nCounter;
			for_each_FF_t & _FE = *( (for_each_FF_t*) pData );
			__EXT_DEBUG_GRID_ASSERT( _FE.m_nParameterIndex >= 0 );
			__EXT_DEBUG_GRID_ASSERT( _FE.m_nParameterIndex < _FE.m_FI.GetChildCount() );
			CExtFormulaValue _FVd;
			//CExtFormulaDataManager::map_formula_cells_t _mapCellsAlreadyComputed;
			if( ! _FE.m_FDM.OnComputeValue( _FE.m_mapCellsAlreadyComputed, LONG(nColNo), LONG(nRowNo), _FVd, &_FE.m_FI ) )
			{
				_FE.m_err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
				if( _FE.m_pErrorLocationForumaItem == NULL )
					_FE.m_pErrorLocationForumaItem = _FE.m_FI.GetChildAt( _FE.m_nParameterIndex );
				return false;
			}
			__EXT_DEBUG_GRID_ASSERT( _FVd.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT );
			if(		_FVd.m_val.vt == VT_BOOL
				||	_FVd.m_val.vt == VT_BSTR
				)
				return true; // ignore logical values and text
			_FVd.PreAdjustDT2R8();
			if( ! _FVd.m_val._VariantChangeType( VT_R8 ) )
			{
				_FE.m_err = CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
				if( _FE.m_pErrorLocationForumaItem == NULL )
					_FE.m_pErrorLocationForumaItem = _FE.m_FI.GetChildAt( _FE.m_nParameterIndex );
				return false;
			}
			__EXT_DEBUG_GRID_ASSERT( _FVd.m_val.vt == VT_R8 );
			__EXT_DEBUG_GRID_ASSERT( _FE.m_pCustomData != NULL );
			double & lfVal = * ( (double*) _FE.m_pCustomData );
			double lfVal2 = _FVd.m_val.dblVal;
			if( _FE.m_bFirstIsComputedInLogicalChain )
			{
				if( lfVal > lfVal2 )
					lfVal = lfVal2;
			}
			else
			{
				lfVal = lfVal2;
				_FE.m_bFirstIsComputedInLogicalChain = true;
			}
			return true;
		}
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterIndex, nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem, VT_R8, CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			double lfVal = 0.0;
			bool bFirstValueComputed = false;
			for( nParameterIndex = 0; nParameterIndex < nParameterCount; nParameterIndex ++ )
			{
				const CExtFormulaValue & _FVi = _FVA.GetRefAt( nParameterIndex );
				switch( _FVi.m_eType )
				{
				case CExtFormulaValue::__EFVT_OLE_VARIANT:
				{
					__EXT_DEBUG_GRID_ASSERT( _FVi.m_val.vt == VT_R8 );
					double lfVal2 = _FVi.m_val.dblVal;
					if( bFirstValueComputed )
					{
						if( lfVal > lfVal2 )
							lfVal = lfVal2;
					}
					else
					{
						lfVal = lfVal2;
						bFirstValueComputed = true;
					}
				}
				break;
				case CExtFormulaValue::__EFVT_CELL_RANGE:
				{
					for_each_FF_t _FE( _mapCellsAlreadyComputed, _FDM, *pFI, _FVi.m_gr, *this );
					_FE.m_pCustomData = LPVOID( &lfVal );
					_FE.m_nParameterIndex = nParameterIndex;
					_FE.m_bFirstIsComputedInLogicalChain = bFirstValueComputed;
					if( ! _FE.ComputeRange() )
					{
						if( _FE.m_err == CExtFormulaValue::__EFEC_OK )
							_FE.m_err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
						err = ( _FE.m_err == CExtFormulaValue::__EFEC_OK ) ? CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE : _FE.m_err;
						if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
							(*ppErrorLocationForumaItem) = ( _FE.m_pErrorLocationForumaItem == NULL ) ? pFI : _FE.m_pErrorLocationForumaItem;
						return err;
					}
					bFirstValueComputed = _FE.m_bFirstIsComputedInLogicalChain;

//					const CExtGR2D & _range = _FVi.m_gr;
//					CExtGR2D::pt_iterator it( _range );
//					for( ; ! it ; ++ it )
//					{
//						CPoint pt = *it;
//						CExtFormulaValue _FVd;
//						if( ! _FDM.OnComputeValue( pt.x, pt.y, _FVd, pFI ) )
//						{
//							err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
//							if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
//								(*ppErrorLocationForumaItem) = pFI->GetChildAt( nParameterIndex );
//							break;
//						}
//						__EXT_DEBUG_GRID_ASSERT( _FVd.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT );
//						_FVd.PreAdjustDT2R8();
//						if( ! _FVd.m_val._VariantChangeType( VT_R8 ) )
//						{
//							if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
//								(*ppErrorLocationForumaItem) = pFI->GetChildAt( nParameterIndex );
//							return CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
//						}
//						__EXT_DEBUG_GRID_ASSERT( _FVd.m_val.vt == VT_R8 );
//						double lfVal2 = _FVd.m_val.dblVal;
//						if( bFirstValueComputed )
//						{
//							if( lfVal > lfVal2 )
//								lfVal = lfVal2;
//						}
//						else
//						{
//							lfVal = lfVal2;
//							bFirstValueComputed = true;
//						}
//					}

				}
				break;
				default:
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( nParameterIndex );
					return CExtFormulaValue::__EFEC_FUNCTION_CANNOT_ACCEPT_EMPTY_PARAMETERS;
				}
			}
			_FV.m_val._VariantAssign( lfVal, VT_R8 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Statistical_MIN

	class __PROF_UIS_API FF_Statistical_MAX
		: public CFFP < CExtFormulaFunction, 1, -1, CExtFormulaFunction::__FFC_STATISTICAL >
		, public CExtGridDataProvider::IDataProviderForEachCallBack
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("MAX"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the largest number in a set of values. Ignores logical values and text.") )
			__EXT_FF_PARM( 1, _T("number%d"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual bool CanAcceptRangeParameters() const { return true; }
		virtual bool OnDataProviderForEach( LPVOID pData, CExtGridCell & _cell, ULONG nCounter, ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			__EXT_DEBUG_GRID_ASSERT( pData != NULL );
			_range;
			_cell;
			nCounter;
			for_each_FF_t & _FE = *( (for_each_FF_t*) pData );
			__EXT_DEBUG_GRID_ASSERT( _FE.m_nParameterIndex >= 0 );
			__EXT_DEBUG_GRID_ASSERT( _FE.m_nParameterIndex < _FE.m_FI.GetChildCount() );
			CExtFormulaValue _FVd;
			//CExtFormulaDataManager::map_formula_cells_t _mapCellsAlreadyComputed;
			if( ! _FE.m_FDM.OnComputeValue( _FE.m_mapCellsAlreadyComputed, LONG(nColNo), LONG(nRowNo), _FVd, &_FE.m_FI ) )
			{
				_FE.m_err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
				if( _FE.m_pErrorLocationForumaItem == NULL )
					_FE.m_pErrorLocationForumaItem = _FE.m_FI.GetChildAt( _FE.m_nParameterIndex );
				return false;
			}
			__EXT_DEBUG_GRID_ASSERT( _FVd.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT );
			if(		_FVd.m_val.vt == VT_BOOL
				||	_FVd.m_val.vt == VT_BSTR
				)
				return true; // ignore logical values and text
			_FVd.PreAdjustDT2R8();
			if( ! _FVd.m_val._VariantChangeType( VT_R8 ) )
			{
				_FE.m_err = CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
				if( _FE.m_pErrorLocationForumaItem == NULL )
					_FE.m_pErrorLocationForumaItem = _FE.m_FI.GetChildAt( _FE.m_nParameterIndex );
				return false;
			}
			__EXT_DEBUG_GRID_ASSERT( _FVd.m_val.vt == VT_R8 );
			__EXT_DEBUG_GRID_ASSERT( _FE.m_pCustomData != NULL );
			double & lfVal = * ( (double*) _FE.m_pCustomData );
			double lfVal2 = _FVd.m_val.dblVal;
			if( _FE.m_bFirstIsComputedInLogicalChain )
			{
				if( lfVal < lfVal2 )
					lfVal = lfVal2;
			}
			else
			{
				lfVal = lfVal2;
				_FE.m_bFirstIsComputedInLogicalChain = true;
			}
			return true;
		}
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterIndex, nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem, VT_R8, CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			double lfVal = 0.0;
			bool bFirstValueComputed = false;
			for( nParameterIndex = 0; nParameterIndex < nParameterCount; nParameterIndex ++ )
			{
				const CExtFormulaValue & _FVi = _FVA.GetRefAt( nParameterIndex );
				switch( _FVi.m_eType )
				{
				case CExtFormulaValue::__EFVT_OLE_VARIANT:
				{
					__EXT_DEBUG_GRID_ASSERT( _FVi.m_val.vt == VT_R8 );
					double lfVal2 = _FVi.m_val.dblVal;
					if( bFirstValueComputed )
					{
						if( lfVal < lfVal2 )
							lfVal = lfVal2;
					}
					else
					{
						lfVal = lfVal2;
						bFirstValueComputed = true;
					}
				}
				break;
				case CExtFormulaValue::__EFVT_CELL_RANGE:
				{
					for_each_FF_t _FE( _mapCellsAlreadyComputed, _FDM, *pFI, _FVi.m_gr, *this );
					_FE.m_pCustomData = LPVOID( &lfVal );
					_FE.m_nParameterIndex = nParameterIndex;
					_FE.m_bFirstIsComputedInLogicalChain = bFirstValueComputed;
					if( ! _FE.ComputeRange() )
					{
						if( _FE.m_err == CExtFormulaValue::__EFEC_OK )
							_FE.m_err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
						err = ( _FE.m_err == CExtFormulaValue::__EFEC_OK ) ? CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE : _FE.m_err;
						if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
							(*ppErrorLocationForumaItem) = ( _FE.m_pErrorLocationForumaItem == NULL ) ? pFI : _FE.m_pErrorLocationForumaItem;
						return err;
					}
					bFirstValueComputed = _FE.m_bFirstIsComputedInLogicalChain;

//					const CExtGR2D & _range = _FVi.m_gr;
//					CExtGR2D::pt_iterator it( _range );
//					for( ; ! it ; ++ it )
//					{
//						CPoint pt = *it;
//						CExtFormulaValue _FVd;
//						if( ! _FDM.OnComputeValue( pt.x, pt.y, _FVd, pFI ) )
//						{
//							err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
//							if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
//								(*ppErrorLocationForumaItem) = pFI->GetChildAt( nParameterIndex );
//							break;
//						}
//						__EXT_DEBUG_GRID_ASSERT( _FVd.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT );
//						_FVd.PreAdjustDT2R8();
//						if( ! _FVd.m_val._VariantChangeType( VT_R8 ) )
//						{
//							if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
//								(*ppErrorLocationForumaItem) = pFI->GetChildAt( nParameterIndex );
//							return CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
//						}
//						__EXT_DEBUG_GRID_ASSERT( _FVd.m_val.vt == VT_R8 );
//						double lfVal2 = _FVd.m_val.dblVal;
//						if( bFirstValueComputed )
//						{
//							if( lfVal < lfVal2 )
//								lfVal = lfVal2;
//						}
//						else
//						{
//							lfVal = lfVal2;
//							bFirstValueComputed = true;
//						}
//					}

				}
				break;
				default:
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( nParameterIndex );
					return CExtFormulaValue::__EFEC_FUNCTION_CANNOT_ACCEPT_EMPTY_PARAMETERS;
				}
			}
			_FV.m_val._VariantAssign( lfVal, VT_R8 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Statistical_MAX

	class __PROF_UIS_API FF_Statistical_SUM
		: public CFFP < CExtFormulaFunction, 1, -1, CExtFormulaFunction::__FFC_STATISTICAL >
		, public CExtGridDataProvider::IDataProviderForEachCallBack
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("SUM"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Adds all the numbers in a range of cells.") )
			__EXT_FF_PARM( 1, _T("number%d"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual bool CanAcceptRangeParameters() const { return true; }
		virtual bool OnDataProviderForEach( LPVOID pData, CExtGridCell & _cell, ULONG nCounter, ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			__EXT_DEBUG_GRID_ASSERT( pData != NULL );
			_range;
			_cell;
			nCounter;
			for_each_FF_t & _FE = *( (for_each_FF_t*) pData );
			__EXT_DEBUG_GRID_ASSERT( _FE.m_nParameterIndex >= 0 );
			__EXT_DEBUG_GRID_ASSERT( _FE.m_nParameterIndex < _FE.m_FI.GetChildCount() );
			CExtFormulaValue _FVd;
			if( ! _FE.m_FDM.OnComputeValue( _FE.m_mapCellsAlreadyComputed, LONG(nColNo), LONG(nRowNo), _FVd, &_FE.m_FI ) )
			{
				_FE.m_err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
				if( _FE.m_pErrorLocationForumaItem == NULL )
					_FE.m_pErrorLocationForumaItem = _FE.m_FI.GetChildAt( _FE.m_nParameterIndex );
				return false;
			}
			__EXT_DEBUG_GRID_ASSERT( _FVd.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT );
			_FVd.PreAdjustDT2R8();
			if( ! _FVd.m_val._VariantChangeType( VT_R8 ) )
			{
				_FE.m_err = CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
				if( _FE.m_pErrorLocationForumaItem == NULL )
					_FE.m_pErrorLocationForumaItem = _FE.m_FI.GetChildAt( _FE.m_nParameterIndex );
				return false;
			}
			__EXT_DEBUG_GRID_ASSERT( _FVd.m_val.vt == VT_R8 );
			__EXT_DEBUG_GRID_ASSERT( _FE.m_pCustomData != NULL );
			double & lfVal = * ( (double*) _FE.m_pCustomData );
			lfVal += _FVd.m_val.dblVal;
			return true;
		}
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterIndex, nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem, VT_R8, CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			double lfVal = 0.0;
			for( nParameterIndex = 0; nParameterIndex < nParameterCount; nParameterIndex ++ )
			{
				const CExtFormulaValue & _FVi = _FVA.GetRefAt( nParameterIndex );
				switch( _FVi.m_eType )
				{
				case CExtFormulaValue::__EFVT_OLE_VARIANT:
				{
					__EXT_DEBUG_GRID_ASSERT( _FVi.m_val.vt == VT_R8 );
					lfVal += _FVi.m_val.dblVal;
				}
				break;
				case CExtFormulaValue::__EFVT_CELL_RANGE:
				{
					for_each_FF_t _FE( _mapCellsAlreadyComputed, _FDM, *pFI, _FVi.m_gr, *this );
					_FE.m_pCustomData = LPVOID( &lfVal );
					_FE.m_nParameterIndex = nParameterIndex;
					if( ! _FE.ComputeRange() )
					{
						if( _FE.m_err == CExtFormulaValue::__EFEC_OK )
							_FE.m_err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
						err = ( _FE.m_err == CExtFormulaValue::__EFEC_OK ) ? CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE : _FE.m_err;
						if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
							(*ppErrorLocationForumaItem) = ( _FE.m_pErrorLocationForumaItem == NULL ) ? pFI : _FE.m_pErrorLocationForumaItem;
						return err;
					}

//					const CExtGR2D & _range = _FVi.m_gr;
//					CExtGR2D::pt_iterator it( _range );
//					for( ; ! it ; ++ it )
//					{
//						CPoint pt = *it;
//						CExtFormulaValue _FVd;
//						if( ! _FDM.OnComputeValue( pt.x, pt.y, _FVd, pFI ) )
//						{
//							err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
//							if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
//								(*ppErrorLocationForumaItem) = pFI->GetChildAt( nParameterIndex );
//							break;
//						}
//						__EXT_DEBUG_GRID_ASSERT( _FVd.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT );
//						_FVd.PreAdjustDT2R8();
//						if( ! _FVd.m_val._VariantChangeType( VT_R8 ) )
//						{
//							if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
//								(*ppErrorLocationForumaItem) = pFI->GetChildAt( nParameterIndex );
//							return CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
//						}
//						__EXT_DEBUG_GRID_ASSERT( _FVd.m_val.vt == VT_R8 );
//						lfVal += _FVd.m_val.dblVal;
//					}

				}
				break;
				default:
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( nParameterIndex );
					return CExtFormulaValue::__EFEC_FUNCTION_CANNOT_ACCEPT_EMPTY_PARAMETERS;
				}
			}
			_FV.m_val._VariantAssign( lfVal, VT_R8 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Statistical_SUM

	class __PROF_UIS_API FF_Statistical_AVERAGE
		: public CFFP < CExtFormulaFunction, 1, -1, CExtFormulaFunction::__FFC_STATISTICAL >
		, public CExtGridDataProvider::IDataProviderForEachCallBack
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("AVERAGE"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the average (arithmetic mean) of its arguments, which can be numbers or names, arrays, or refereces that contain numbers.") )
			__EXT_FF_PARM( 1, _T("number%d"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual bool CanAcceptRangeParameters() const { return true; }
		virtual bool OnDataProviderForEach( LPVOID pData, CExtGridCell & _cell, ULONG nCounter, ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			__EXT_DEBUG_GRID_ASSERT( pData != NULL );
			_range;
			_cell;
			nCounter;
			for_each_FF_t & _FE = *( (for_each_FF_t*) pData );
			__EXT_DEBUG_GRID_ASSERT( _FE.m_nParameterIndex >= 0 );
			__EXT_DEBUG_GRID_ASSERT( _FE.m_nParameterIndex < _FE.m_FI.GetChildCount() );
			CExtFormulaValue _FVd;
			//CExtFormulaDataManager::map_formula_cells_t _mapCellsAlreadyComputed;
			if( ! _FE.m_FDM.OnComputeValue( _FE.m_mapCellsAlreadyComputed, LONG(nColNo), LONG(nRowNo), _FVd, &_FE.m_FI ) )
			{
				_FE.m_err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
				if( _FE.m_pErrorLocationForumaItem == NULL )
					_FE.m_pErrorLocationForumaItem = _FE.m_FI.GetChildAt( _FE.m_nParameterIndex );
				return false;
			}
			__EXT_DEBUG_GRID_ASSERT( _FVd.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT );
			_FVd.PreAdjustDT2R8();
			if( ! _FVd.m_val._VariantChangeType( VT_R8 ) )
			{
				_FE.m_err = CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
				if( _FE.m_pErrorLocationForumaItem == NULL )
					_FE.m_pErrorLocationForumaItem = _FE.m_FI.GetChildAt( _FE.m_nParameterIndex );
				return false;
			}
			__EXT_DEBUG_GRID_ASSERT( _FVd.m_val.vt == VT_R8 );
			__EXT_DEBUG_GRID_ASSERT( _FE.m_pCustomData != NULL );
			double & lfVal = * ( (double*) _FE.m_pCustomData );
			lfVal += _FVd.m_val.dblVal;
			return true;
		}
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterIndex, nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem, VT_R8, CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			double lfVal = 0.0;
			LONG nCellCount = 0L;
			for( nParameterIndex = 0; nParameterIndex < nParameterCount; nParameterIndex ++ )
			{
				const CExtFormulaValue & _FVi = _FVA.GetRefAt( nParameterIndex );
				switch( _FVi.m_eType )
				{
				case CExtFormulaValue::__EFVT_OLE_VARIANT:
				{
					__EXT_DEBUG_GRID_ASSERT( _FVi.m_val.vt == VT_R8 );
					if( _FVi.m_val.dblVal != 0.0 )
					{
						lfVal += _FVi.m_val.dblVal;
						nCellCount ++;
					}
				}
				break;
				case CExtFormulaValue::__EFVT_CELL_RANGE:
				{
					for_each_FF_t _FE( _mapCellsAlreadyComputed, _FDM, *pFI, _FVi.m_gr, *this );
					_FE.m_pCustomData = LPVOID( &lfVal );
					_FE.m_nParameterIndex = nParameterIndex;
					if( ! _FE.ComputeRange() )
					{
						if( _FE.m_err == CExtFormulaValue::__EFEC_OK )
							_FE.m_err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
						err = ( _FE.m_err == CExtFormulaValue::__EFEC_OK ) ? CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE : _FE.m_err;
						if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
							(*ppErrorLocationForumaItem) = ( _FE.m_pErrorLocationForumaItem == NULL ) ? pFI : _FE.m_pErrorLocationForumaItem;
						return err;
					}
					if( _FE.m_nCounter > 0 )
						nCellCount += _FE.m_nCounter + 1;

//					const CExtGR2D & _range = _FVi.m_gr;
//					CExtGR2D::pt_iterator it( _range );
//					for( ; ! it ; ++ it )
//					{
//						CPoint pt = *it;
//						CExtFormulaValue _FVd;
//						if( ! _FDM.OnComputeValue( pt.x, pt.y, _FVd, pFI ) )
//						{
//							err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
//							if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
//								(*ppErrorLocationForumaItem) = pFI->GetChildAt( nParameterIndex );
//							break;
//						}
//						__EXT_DEBUG_GRID_ASSERT( _FVd.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT );
//						_FVd.PreAdjustDT2R8();
//						if( ! _FVd.m_val._VariantChangeType( VT_R8 ) )
//						{
//							if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
//								(*ppErrorLocationForumaItem) = pFI->GetChildAt( nParameterIndex );
//							return CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
//						}
//						__EXT_DEBUG_GRID_ASSERT( _FVd.m_val.vt == VT_R8 );
//						if( _FVd.m_val.dblVal != 0.0 )
//						{
//							lfVal += _FVd.m_val.dblVal;
//							nCellCount ++;
//						}
//					}

				}
				break;
				default:
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( nParameterIndex );
					return CExtFormulaValue::__EFEC_FUNCTION_CANNOT_ACCEPT_EMPTY_PARAMETERS;
				}
			}
			__EXT_DEBUG_GRID_ASSERT( nCellCount >= 0L );
			if( nCellCount == 0L )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_DIVISION_BY_ZERO;
			}
			lfVal /= double(nCellCount);
			_FV.m_val._VariantAssign( lfVal, VT_R8 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Statistical_AVERAGE

	class __PROF_UIS_API FF_Text_CHAR : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("CHAR"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the character specified by the code number from the character set for your computer.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt( 0 )->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			if( ! _FV.m_val._VariantChangeType( VT_I4 ) )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
			}
			if( ! ( 0 < _FV.m_val.intVal && _FV.m_val.intVal < 256 ) )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
			}
			TCHAR str[2] = { TCHAR( _FV.m_val.intVal), _T('\0') };
			_FV.m_val._VariantAssign( LPCTSTR(str) );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_CHAR

	class __PROF_UIS_API FF_Text_CODE : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("CODE"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns a numeric code for the first character in a text string, in the character set used by your computer.") )
			__EXT_FF_PARM( 1, _T("text"), _T("Text parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt( 0 )->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			if( ! _FV.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			USES_CONVERSION;
			CString str = OLE2CT( _FV.m_val.bstrVal );
			if( str.GetLength() < 1 )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NON_EMPTY_STRING_REQUIRED;
			}
			INT nVal = INT( str[0] );
			_FV.m_val._VariantAssign( nVal, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_CODE

	class __PROF_UIS_API FF_Text_CONCATENATE : public CFFP < CExtFormulaFunction, 1, -1, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("CONCATENATE"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Joins several text strings into one text string.") )
			__EXT_FF_PARM( 1, _T("text%d"), _T("Text parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterIndex, nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem, VT_BSTR, CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			__EXT_DEBUG_GRID_ASSERT( _FVA.GetRefAt( 0 ).m_val.vt == VT_BSTR );
			USES_CONVERSION;
			CString str = OLE2CT( _FVA.GetRefAt( 0 ).m_val.bstrVal );
			for( nParameterIndex = 1; nParameterIndex < nParameterCount; nParameterIndex ++ )
			{
				__EXT_DEBUG_GRID_ASSERT( _FVA.GetRefAt( nParameterIndex ).m_val.vt == VT_BSTR );
				CString strN = OLE2CT( _FVA.GetRefAt( nParameterIndex ).m_val.bstrVal );
				str += LPCTSTR(strN);
			}
			_FV.m_val._VariantAssign( LPCTSTR(str) );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_CONCATENATE

	class __PROF_UIS_API FF_Text_EXACT : public CFFP < CExtFormulaFunction, 2, 2, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("EXACT"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Checks whether two text strings are exactly the same, and returns TRUE or FALSE. EXACT is case-sensitive.") )
			__EXT_FF_PARM( 1, _T("text1"), _T("Text parameter.") )
			__EXT_FF_PARM( 2, _T("text2"), _T("Text parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem, VT_BSTR, CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			__EXT_DEBUG_GRID_ASSERT( _FVA.GetRefAt( 0 ).m_val.vt == VT_BSTR );
			USES_CONVERSION;
			LPCTSTR strLeft  = OLE2CT( _FVA.GetRefAt( 0 ).m_val.bstrVal );
			LPCTSTR strRight = OLE2CT( _FVA.GetRefAt( 1 ).m_val.bstrVal );
			bool bEqual = ( _tcscmp( strLeft, strRight ) == 0 ) ? true : false;
			_FV.m_val._VariantAssign( bEqual );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_EXACT

	class __PROF_UIS_API FF_Text_LEFT : public CFFP < CExtFormulaFunction, 1, 2, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("LEFT"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the specified number of characters from the start of a text string.") )
			__EXT_FF_PARM( 1, _T("text"), _T("Text parameter.") )
			__EXT_FF_PARM( 2, _T("num_chars"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			CExtFormulaValue & _FV_str = _FVA[ 0 ];
			if( ! _FV_str.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			USES_CONVERSION;
			CString strSrc = OLE2CT( _FV_str.m_val.bstrVal );
			INT nCnt = 1, nTextLen = strSrc.GetLength();
			if( nParameterCount == 2 )
			{
				CExtFormulaValue & _FV_cnt = _FVA[ 1 ];
				if( ! _FV_cnt.m_val._VariantChangeType( VT_I4 ) )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
					return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
				}
				nCnt = (INT)_FV_cnt.m_val.intVal;
				if( nCnt < 1 )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
					return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
				}
			}
			if( nCnt > nTextLen )
				nCnt = nTextLen;
			CString strDst;
			if( nCnt > 0 )
				strDst = strSrc.Left( nCnt );
			_FV.m_val._VariantAssign( (!strDst.IsEmpty()) ? LPCTSTR(strDst) : _T("") );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_LEFT

	class __PROF_UIS_API FF_Text_RIGHT : public CFFP < CExtFormulaFunction, 1, 2, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("RIGHT"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the specified number of characters from the end of a text string.") )
			__EXT_FF_PARM( 1, _T("text"), _T("Text parameter.") )
			__EXT_FF_PARM( 2, _T("num_chars"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			CExtFormulaValue & _FV_str = _FVA[ 0 ];
			if( ! _FV_str.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			USES_CONVERSION;
			CString strSrc = OLE2CT( _FV_str.m_val.bstrVal );
			INT nCnt = 1, nTextLen = strSrc.GetLength();
			if( nParameterCount == 2 )
			{
				CExtFormulaValue & _FV_cnt = _FVA[ 1 ];
				if( ! _FV_cnt.m_val._VariantChangeType( VT_I4 ) )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
					return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
				}
				nCnt = (INT)_FV_cnt.m_val.intVal;
				if( nCnt < 1 )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
					return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
				}
			}
			if( nCnt > nTextLen )
				nCnt = nTextLen;
			CString strDst;
			if( nCnt > 0 )
				strDst = strSrc.Right( nCnt );
			_FV.m_val._VariantAssign( (!strDst.IsEmpty()) ? LPCTSTR(strDst) : _T("") );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_RIGHT

	class __PROF_UIS_API FF_Text_MID : public CFFP < CExtFormulaFunction, 3, 3, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("MID"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the characters from the middle of a text string, given a starting position and length.") )
			__EXT_FF_PARM( 1, _T("text"), _T("Text parameter.") )
			__EXT_FF_PARM( 2, _T("start_num"), _T("Number parameter.") )
			__EXT_FF_PARM( 3, _T("num_chars"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			CExtFormulaValue & _FV_str = _FVA[ 0 ];
			if( ! _FV_str.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			USES_CONVERSION;
			CString strSrc = OLE2CT( _FV_str.m_val.bstrVal );
			INT nCnt = 1, nTextLen = strSrc.GetLength();

			CExtFormulaValue & _FV_pos = _FVA[ 1 ];
			if( ! _FV_pos.m_val._VariantChangeType( VT_I4 ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
				return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
			}
			INT nPos = (INT)_FV_pos.m_val.intVal;
			if( nPos < 1 || nPos > nTextLen )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
				return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
			}

			CExtFormulaValue & _FV_cnt = _FVA[ 2 ];
			if( ! _FV_cnt.m_val._VariantChangeType( VT_I4 ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 2 );
				return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
			}
			nCnt = (INT)_FV_cnt.m_val.intVal;
			if( nCnt < 1 )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 2 );
				return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
			}

			INT nMaxCnt = nTextLen - ( nPos - 1 );
			__EXT_DEBUG_GRID_ASSERT( nMaxCnt >= 0 );
			if( nCnt > nMaxCnt )
				nCnt = nMaxCnt;
			CString strDst;
			if( nCnt > 0 )
				strDst = strSrc.Mid( nPos - 1, nCnt );
			_FV.m_val._VariantAssign( (!strDst.IsEmpty()) ? LPCTSTR(strDst) : _T("") );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_MID

	class __PROF_UIS_API FF_Text_TRIM : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("TRIM"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Removes all spaces from a text string except for single spaces between words.") )
			__EXT_FF_PARM( 1, _T("text"), _T("Text parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( ! _FV.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			USES_CONVERSION;
			CString str = OLE2CT( _FV.m_val.bstrVal );
			str.TrimLeft();
			str.TrimRight();
			while( str.Replace( _T("  "), _T(" ") ) > 0 ) { }
			_FV.m_val._VariantAssign( (!str.IsEmpty()) ? LPCTSTR(str) : _T("") );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_TRIM

	class __PROF_UIS_API FF_Text_LOWER : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("LOWER"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Converts all letters in a text string to lowercase.") )
			__EXT_FF_PARM( 1, _T("text"), _T("Text parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( ! _FV.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			USES_CONVERSION;
			CString str = OLE2CT( _FV.m_val.bstrVal );
			str.MakeLower();
			_FV.m_val._VariantAssign( (!str.IsEmpty()) ? LPCTSTR(str) : _T("") );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_LOWER

	class __PROF_UIS_API FF_Text_UPPER : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("UPPER"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Converts a text string to all uppercase letters.") )
			__EXT_FF_PARM( 1, _T("text"), _T("Text parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( ! _FV.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			USES_CONVERSION;
			CString str = OLE2CT( _FV.m_val.bstrVal );
			str.MakeUpper();
			_FV.m_val._VariantAssign( (!str.IsEmpty()) ? LPCTSTR(str) : _T("") );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_UPPER

	class __PROF_UIS_API FF_Text_LEN : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("LEN"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the number of characters in a text string.") )
			__EXT_FF_PARM( 1, _T("text"), _T("Text parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( ! _FV.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			USES_CONVERSION;
			LPCTSTR str = OLE2CT( _FV.m_val.bstrVal );
			INT nTextLenght = ( str == NULL ) ? INT(0) : INT(_tcslen(str));
			_FV.m_val._VariantAssign( nTextLenght, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_LEN

	class __PROF_UIS_API FF_Text_REPT : public CFFP < CExtFormulaFunction, 2, 2, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("REPT"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Repeats text a given number of times. Use REPT to fill a cell with a number of instances of a text string.") )
			__EXT_FF_PARM( 1, _T("text"), _T("Text parameter.") )
			__EXT_FF_PARM( 2, _T("number_times"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			CExtFormulaValue & _FV_str = _FVA[ 0 ];
			if( ! _FV_str.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			USES_CONVERSION;
			LPCTSTR strSrc = OLE2CT( _FV_str.m_val.bstrVal );
			INT nStep, nCnt = 1, nTextLen = ( strSrc == NULL ) ? INT(0) : INT(_tclen(strSrc));
			CExtFormulaValue & _FV_cnt = _FVA[ 1 ];
			if( ! _FV_cnt.m_val._VariantChangeType( VT_I4 ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 2 );
				return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
			}
			nCnt = (INT)_FV_cnt.m_val.intVal;
			if( nCnt < 0 )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 2 );
				return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
			}
			CString strDst = _T("");
			if( nTextLen > 0 )
			{
				for( nStep = 0; nStep < nCnt; nStep ++ )
					strDst += strSrc;
			}
			_FV.m_val._VariantAssign( (!strDst.IsEmpty()) ? LPCTSTR(strDst) : _T("") );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_REPT

	class __PROF_UIS_API FF_Text_FIND : public CFFP < CExtFormulaFunction, 2, 3, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("FIND"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the starting position of one text string withing another text string. FIND is case-sensitive.") )
			__EXT_FF_PARM( 1, _T("text"), _T("Text parameter.") )
			__EXT_FF_PARM( 2, _T("within_text"), _T("Text parameter.") )
			__EXT_FF_PARM( 3, _T("start_num"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			CExtFormulaValue & _FV_str_what = _FVA[ 0 ];
			if( ! _FV_str_what.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			CExtFormulaValue & _FV_str_where = _FVA[ 1 ];
			if( ! _FV_str_where.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			USES_CONVERSION;
			CString strWhat = OLE2CT( _FV_str_what.m_val.bstrVal );
			INT nWhatLength = INT( strWhat.GetLength() );
			CString strWhere = OLE2CT( _FV_str_where.m_val.bstrVal );
			INT nWhereLength = INT( strWhere.GetLength() );
			INT nStartPos = 1, nMaxPos = nWhereLength - nWhatLength + 1;
			if( nParameterCount == 3 )
			{
				CExtFormulaValue & _FV_start_pos = _FVA[ 2 ];
				if( ! _FV_start_pos.m_val._VariantChangeType( VT_I4 ) )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 2 );
					return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
				}
				nStartPos = (INT)_FV_start_pos.m_val.intVal;
				if( nStartPos < 1 || nStartPos > nMaxPos )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 2 );
					return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
				}
			}
			INT nRetVal = -1;
			if( nWhatLength  == 0 )
				nRetVal = nStartPos;
			else
			{
				nRetVal = (INT)strWhere.Find( LPCTSTR(strWhat), nStartPos - 1 );
				if( nRetVal >= 0 )
					nRetVal ++;
			}
			if( nRetVal < 0 )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_ERROR_AS_FUNCTION_EXECUTION_RESULT_OR_CANNOT_COMPUTE_RESULT; // text is not found or can not be found
			}
			_FV.m_val._VariantAssign( nRetVal, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_FIND

	class __PROF_UIS_API FF_Text_REPLACE : public CFFP < CExtFormulaFunction, 4, 4, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("REPLACE"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Replaces part of a text string with a different text string.") )
			__EXT_FF_PARM( 1, _T("text"), _T("Text parameter.") )
			__EXT_FF_PARM( 2, _T("start_num"), _T("Number parameter.") )
			__EXT_FF_PARM( 3, _T("num_chars"), _T("Number parameter.") )
			__EXT_FF_PARM( 4, _T("new_text"), _T("Text parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			CExtFormulaValue & _FV_str_old = _FVA[ 0 ];
			if( ! _FV_str_old.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			USES_CONVERSION;
			CString strOldText = OLE2CT( _FV_str_old.m_val.bstrVal );
			INT nCnt = 1, nOldTextLen = strOldText.GetLength();

			CExtFormulaValue & _FV_pos = _FVA[ 1 ];
			if( ! _FV_pos.m_val._VariantChangeType( VT_I4 ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
				return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
			}
			INT nPos = (INT)_FV_pos.m_val.intVal;
			if( nPos < 1 )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
				return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
			}
			if( nPos > (nOldTextLen+1) )
				nPos = nOldTextLen+1;

			CExtFormulaValue & _FV_cnt = _FVA[ 2 ];
			if( ! _FV_cnt.m_val._VariantChangeType( VT_I4 ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 2 );
				return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
			}
			nCnt = (INT)_FV_cnt.m_val.intVal;
			if( nCnt < 0 )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 2 );
				return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
			}

			CExtFormulaValue & _FV_str_ins = _FVA[ 3 ];
			if( ! _FV_str_ins.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 3 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			CString strDst, strInsText = OLE2CT( _FV_str_ins.m_val.bstrVal );
			INT nInsTextLen = strInsText.GetLength();
			if( nInsTextLen > 0 )
			{
				INT nMaxCnt = nOldTextLen - ( nPos - 1 );
				__EXT_DEBUG_GRID_ASSERT( nMaxCnt >= 0 );
				if( nCnt > nMaxCnt )
					nCnt = nMaxCnt;
				if( nCnt >= 0 )
				{
					strDst = strOldText.Left( nPos - 1 );
					CString strRight = strOldText.Right( nOldTextLen -  nCnt - ( nPos - 1 ) );
					strDst += LPCTSTR( strInsText );
					strDst += LPCTSTR( strRight );
				}
				else
					strDst = strOldText;
			}
			else
				strDst = strOldText;
			_FV.m_val._VariantAssign( (!strDst.IsEmpty()) ? LPCTSTR(strDst) : _T("") );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_REPLACE

	class __PROF_UIS_API FF_Text_SEARCH : public CFFP < CExtFormulaFunction, 2, 3, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("SEARCH"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the number of characters at which a specific character or text string is first found, reading left to right (not case-sensitive).") )
			__EXT_FF_PARM( 1, _T("find_text"), _T("Text parameter.") )
			__EXT_FF_PARM( 2, _T("within_text"), _T("Text parameter.") )
			__EXT_FF_PARM( 3, _T("start_num"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			CExtFormulaValue & _FV_str_what = _FVA[ 0 ];
			if( ! _FV_str_what.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			CExtFormulaValue & _FV_str_where = _FVA[ 1 ];
			if( ! _FV_str_where.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			USES_CONVERSION;
			CString strWhat = OLE2CT( _FV_str_what.m_val.bstrVal );
			INT nWhatLength = INT( strWhat.GetLength() );
			CString strWhere = OLE2CT( _FV_str_where.m_val.bstrVal );
			INT nWhereLength = INT( strWhere.GetLength() );
			INT nStartPos = 1, nMaxPos = nWhereLength - nWhatLength + 1;
			if( nParameterCount == 3 )
			{
				CExtFormulaValue & _FV_start_pos = _FVA[ 2 ];
				if( ! _FV_start_pos.m_val._VariantChangeType( VT_I4 ) )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 2 );
					return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
				}
				nStartPos = (INT)_FV_start_pos.m_val.intVal;
				if( nStartPos < 1 || nStartPos > nMaxPos )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 2 );
					return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
				}
			}
			INT nRetVal = -1;
			if( nWhatLength  == 0 )
				nRetVal = nStartPos;
			else
			{
				strWhere.MakeLower();
				strWhat.MakeLower();
				nRetVal = (INT)strWhere.Find( LPCTSTR(strWhat), nStartPos - 1 );
				if( nRetVal >= 0 )
					nRetVal ++;
			}
			if( nRetVal < 0 )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_ERROR_AS_FUNCTION_EXECUTION_RESULT_OR_CANNOT_COMPUTE_RESULT; // text is not found or can not be found
			}
			_FV.m_val._VariantAssign( nRetVal, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_SEARCH

	class __PROF_UIS_API FF_Text_SUBSTITUTE : public CFFP < CExtFormulaFunction, 3, 4, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("SUBSTITUTE"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Replaces existing text with new text in a text string.") )
			__EXT_FF_PARM( 1, _T("text"), _T("Text parameter.") )
			__EXT_FF_PARM( 2, _T("old_text"), _T("Text parameter.") )
			__EXT_FF_PARM( 3, _T("new_text"), _T("Text parameter.") )
			__EXT_FF_PARM( 4, _T("instance_num"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			CExtFormulaValue & _FV_str_src = _FVA[ 0 ];
			if( ! _FV_str_src.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			CExtFormulaValue & _FV_str_what = _FVA[ 1 ];
			if( ! _FV_str_what.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			CExtFormulaValue & _FV_str_with = _FVA[ 2 ];
			if( ! _FV_str_with.m_val._VariantChangeType( VT_BSTR ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 2 );
				return CExtFormulaValue::__EFEC_TYPE_BSTR_CONVERSION_ERROR;
			}
			USES_CONVERSION;
			CString strSrc  = OLE2CT( _FV_str_src.m_val.bstrVal );
			INT nLengthSrc  = INT( strSrc.GetLength() );
			CString strWhat = OLE2CT( _FV_str_what.m_val.bstrVal );
			INT nLengthWhat = INT( strWhat.GetLength() );
			CString strWith = OLE2CT( _FV_str_with.m_val.bstrVal );
			INT nLengthWith = INT( strWith.GetLength() );
			INT nOuccurNo = -1;
			if( nParameterCount == 4 )
			{
				CExtFormulaValue & _FV_occur_no = _FVA[ 3 ];
				if( ! _FV_occur_no.m_val._VariantChangeType( VT_I4 ) )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 3 );
					return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
				}
				nOuccurNo = (INT)_FV_occur_no.m_val.intVal;
				if( nOuccurNo < 1 )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 3 );
					return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
				}
			}
			CString strDst;
			strDst = LPCTSTR(strSrc);
			if( nLengthWhat > 0 )
			{
				INT nLenghtDst = nLengthSrc, nLenghtDiff = nLengthWith - nLengthWhat, nSearchPos = 0, nSubstNo = 1;
				bool bStop = false;
				for( ; (!bStop) && nSearchPos <= nLenghtDst; )
				{
					INT nPos = strDst.Find( LPCTSTR(strWhat), nSearchPos );
					if( nPos < 0 )
						break;
					bool bDoSubst = true;
					if( nOuccurNo > 0 )
					{
						__EXT_DEBUG_GRID_ASSERT( nOuccurNo >= 1 );
						if( nOuccurNo != nSubstNo )
							bDoSubst = false;
						else
							bStop = true;
					} // if( nOuccurNo > 0 )
					if( bDoSubst )
					{
						CString strLeft  = strDst.Left( nPos );
						CString strRight = strDst.Right( nLenghtDst - ( nPos + nLengthWhat ) );
						strDst = (!strLeft.IsEmpty()) ? LPCTSTR( strLeft ) : _T("");
						strDst += LPCTSTR( strWith );
						if( ! strRight.IsEmpty() )
							strDst += LPCTSTR( strRight );
						nSearchPos = nPos + nLengthWith;
						nLenghtDst += nLenghtDiff;
						__EXT_DEBUG_GRID_ASSERT( nLenghtDst == INT(strDst.GetLength()) );
						__EXT_DEBUG_GRID_ASSERT( nSearchPos <= nLenghtDst );
					} // if( bDoSubst )
					else
					{
						nSearchPos = nPos + nLengthWhat;
						__EXT_DEBUG_GRID_ASSERT( nSearchPos <= nLenghtDst );
					} // else from if( bDoSubst )
					nSubstNo ++;
				} // for( ; nSearchPos <= nLenghtDst; )
				__EXT_DEBUG_GRID_ASSERT( nLenghtDst == INT(strDst.GetLength()) );
			} // if( nLengthWhat > 0 )
			LPCTSTR strAssign = _T("");
			if( ! strDst.IsEmpty() )
				strAssign = LPCTSTR( strDst );
			_FV.m_val._VariantAssign( strAssign );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_SUBSTITUTE

	class __PROF_UIS_API FF_Text_T : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_TEXT >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("T"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Checks whether a value is text, and returns the text if it is, or returns double quotes (empty text) if it is not.") )
			__EXT_FF_PARM( 1, _T("value"), _T("Value parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( _FV.m_val.vt != VT_BSTR )
			{
				_FV.m_val._VariantAssign( _T("") );
				__EXT_DEBUG_GRID_ASSERT( _FV.m_val.vt == VT_BSTR );
			}
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Text_T

	class __PROF_UIS_API FF_DateTime_DATE : public CFFP < CExtFormulaFunction, 3, 3, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("DATE"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the number that represents the date code.") )
			__EXT_FF_PARM( 1, _T("year"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("month"), _T("Number parameter.") )
			__EXT_FF_PARM( 3, _T("day"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem, VT_I4, CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			INT nYear = INT( _FVA[ 0 ].m_val.intVal );
			if( nYear < 0 )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
			}
			if( nYear < 1900 )
				nYear += 1900;
			INT nMonth = INT( _FVA[ 1 ].m_val.intVal );
			INT nDay = INT( _FVA[ 2 ].m_val.intVal );
			if( nMonth <= 0 )
			{
				INT nYearsAdd = ( nMonth - 1 - 12 ) / 12;
				nYear += nYearsAdd;
				nMonth = 12 + ( ( nMonth - 1 ) ) % 12 + 1;
				__EXT_DEBUG_GRID_ASSERT( 1 <= nMonth && nMonth <= 12 );
			}
			else if( nMonth > 12 )
			{
				INT nYearsAdd = ( nMonth - 1 ) / 12;
				nYear += nYearsAdd;
				nMonth = ( ( nMonth - 1 ) ) % 12 + 1;
				__EXT_DEBUG_GRID_ASSERT( 1 <= nMonth && nMonth <= 12 );
			}
			COleDateTime _odt( nYear, nMonth, 1, 0, 0, 0 );
			COleDateTimeSpan _odts( nDay - 1, 0, 0, 0 );
			_odt += _odts;
			_FV.m_val._VariantAssign( _odt.m_dt, VT_DATE );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_DateTime_DATE

	class __PROF_UIS_API FF_DateTime_DATEVALUE : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("DATEVALUE"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Converts a date in the form of text to a number that represents the date code.") )
			__EXT_FF_PARM( 1, _T("date_text"), _T("Text parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( ! _FV.m_val._VariantChangeType( VT_DATE ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_DATE_CONVERSION_ERROR;
			}
			COleDateTime _odt( _FV.m_val.date ), _odtStart( 1900, 1, 1, 0, 0, 0 );
			COleDateTimeSpan _odts = _odt - _odtStart;
			INT nCountOfDays = INT( _odts.GetDays() ) + 2;
			_FV.m_val._VariantAssign( nCountOfDays, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_DateTime_DATEVALUE

	class __PROF_UIS_API FF_DateTime_DAY : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("DAY"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the day of the month, a number from 1 to 31.") )
			__EXT_FF_PARM( 1, _T("serial_number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( ! _FV.m_val._VariantChangeType( VT_DATE ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_DATE_CONVERSION_ERROR;
			}
			COleDateTime _odt( _FV.m_val.date );
			INT nRetVal = INT( _odt.GetDay() );
			_FV.m_val._VariantAssign( nRetVal, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_DateTime_DAY

	class __PROF_UIS_API FF_DateTime_MONTH : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("MONTH"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the month, a number from 1 (January) to 12 (December).") )
			__EXT_FF_PARM( 1, _T("serial_number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( ! _FV.m_val._VariantChangeType( VT_DATE ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_DATE_CONVERSION_ERROR;
			}
			COleDateTime _odt( _FV.m_val.date );
			INT nRetVal = INT( _odt.GetMonth() );
			_FV.m_val._VariantAssign( nRetVal, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_DateTime_MONTH

	class __PROF_UIS_API FF_DateTime_YEAR : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("YEAR"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the year of a date, an integrer in the range 1900 - 9999.") )
			__EXT_FF_PARM( 1, _T("serial_number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( ! _FV.m_val._VariantChangeType( VT_DATE ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_DATE_CONVERSION_ERROR;
			}
			COleDateTime _odt( _FV.m_val.date );
			INT nRetVal = INT( _odt.GetYear() );
			_FV.m_val._VariantAssign( nRetVal, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_DateTime_YEAR

	class __PROF_UIS_API FF_DateTime_TIME : public CFFP < CExtFormulaFunction, 3, 3, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("TIME"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Convets hours, minutes and seconds given as numbers to a serial number, formatted with a time format.") )
			__EXT_FF_PARM( 1, _T("hour"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("minute"), _T("Number parameter.") )
			__EXT_FF_PARM( 3, _T("second"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValueArray _FVA( nParameterCount );
			if( _FVA.IsEmpty() )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI;
				return CExtFormulaValue::__EFEC_NOT_ENOUGH_MEMORY;
			}
			CExtFormulaValue::e_formula_error_code_t err = _FVA.ComputeAllValues( _mapCellsAlreadyComputed, _FDM, pFI, ppErrorLocationForumaItem, VT_I4, CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			INT nHour = INT( _FVA[ 0 ].m_val.intVal ) % 24;
			if( nHour < 0 )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
			}
			INT nMinute = INT( _FVA[ 1 ].m_val.intVal );
			INT nSecond = INT( _FVA[ 2 ].m_val.intVal );
			nMinute += nSecond / 60;
			nSecond %= 60;
			nHour += nMinute / 60;
			nMinute %= 60;
			COleDateTime _odt( 1900, 1, 1, nHour, nMinute, nSecond );
			_FV.m_val._VariantAssign( _odt.m_dt, VT_DATE );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_DateTime_TIME

	class __PROF_UIS_API FF_DateTime_TIMEVALUE : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("TIMEVALUE"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Converts text time to a serial number for a time, a number from 0 (12:00:00 AM) to 0.999988426 (11:59:59 PM). Format the number with at time format after entering the formula.") )
			__EXT_FF_PARM( 1, _T("time_text"), _T("Text parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			double lf = -2.0;
			if( _FV.m_val.vt != VT_DATE )
			{
				if( ! _FV.m_val._VariantChangeType( VT_DATE ) )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
					return CExtFormulaValue::__EFEC_TYPE_DATE_CONVERSION_ERROR;
				}
				lf = 0.0;
			}
			lf += double( _FV.m_val.date );
			_FV.m_val._VariantAssign( lf, VT_R8 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_DateTime_TIMEVALUE

	class __PROF_UIS_API FF_DateTime_HOUR : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("HOUR"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the hour as the number from 0 (12:00 AM) to 23 (11:00 PM).") )
			__EXT_FF_PARM( 1, _T("serial_number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( ! _FV.m_val._VariantChangeType( VT_DATE ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_DATE_CONVERSION_ERROR;
			}
			COleDateTime _odt( _FV.m_val.date );
			INT nRetVal = INT( _odt.GetHour() );
			_FV.m_val._VariantAssign( nRetVal, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_DateTime_HOUR

	class __PROF_UIS_API FF_DateTime_MINUTE : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("MINUTE"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the minute, a number from 0 to 59.") )
			__EXT_FF_PARM( 1, _T("serial_number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( ! _FV.m_val._VariantChangeType( VT_DATE ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_DATE_CONVERSION_ERROR;
			}
			COleDateTime _odt( _FV.m_val.date );
			INT nRetVal = INT( _odt.GetMinute() );
			_FV.m_val._VariantAssign( nRetVal, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_DateTime_MINUTE

	class __PROF_UIS_API FF_DateTime_SECOND : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("SECOND"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the second, a number from 0 to 59.") )
			__EXT_FF_PARM( 1, _T("serial_number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( ! _FV.m_val._VariantChangeType( VT_DATE ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_DATE_CONVERSION_ERROR;
			}
			COleDateTime _odt( _FV.m_val.date );
			INT nRetVal = INT( _odt.GetSecond() );
			_FV.m_val._VariantAssign( nRetVal, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_DateTime_SECOND

	class __PROF_UIS_API FF_DateTime_NOW : public CFFP < CExtFormulaFunction, 0, 0, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("NOW"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the current date and time formatted as date and time.") )
		__EXT_FF_DESCRIPTION_END
		virtual bool CanBeIdentifier() const { return false; } // NOW should not be used as identifier
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT(
					pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL
				||	(	pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_IDENTIFIER
					&&	CanBeIdentifier()
					)
				);
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			_mapCellsAlreadyComputed;
			_FDM;
			pFI;
			ppErrorLocationForumaItem;
			COleDateTime _odtNow = COleDateTime::GetCurrentTime();
			_FV.m_val._VariantAssign( double( _odtNow.m_dt ), VT_DATE );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_DateTime_NOW

	class __PROF_UIS_API FF_DateTime_TODAY : public CFFP < CExtFormulaFunction, 0, 0, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("TODAY"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the current date formatted as date.") )
		__EXT_FF_DESCRIPTION_END
		virtual bool CanBeIdentifier() const { return false; } // TODAY should not be used as identifier
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT(
					pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL
				||	(	pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_IDENTIFIER
					&&	CanBeIdentifier()
					)
				);
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			_mapCellsAlreadyComputed;
			_FDM;
			pFI;
			ppErrorLocationForumaItem;
			COleDateTime _odtNow = COleDateTime::GetCurrentTime();
			COleDateTime _odtToday( _odtNow.GetYear(), _odtNow.GetMonth(), _odtNow.GetDay(), 0, 0, 0 );
			_FV.m_val._VariantAssign( double( _odtToday.m_dt ), VT_DATE );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_DateTime_TODAY

	class __PROF_UIS_API FF_DateTime_WEEKDAY : public CFFP < CExtFormulaFunction, 1, 2, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("WEEKDAY"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns a number from 1 to 7 indentifying the day of the week of a date.") )
			__EXT_FF_PARM( 1, _T("serial_number"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("return_type"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( ! _FV.m_val._VariantChangeType( VT_DATE ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_DATE_CONVERSION_ERROR;
			}
			INT nType = 1;
			if( nParameterCount > 1 )
			{
				CExtFormulaValue _FV_type;
				err = pFI->GetChildAt(1)->ComputeValue( _mapCellsAlreadyComputed, _FV_type, _FDM, ppErrorLocationForumaItem );
				if( err != CExtFormulaValue::__EFEC_OK )
					return err;
				if( ! _FV_type.m_val._VariantChangeType( VT_I4 ) )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
					return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
				}
				nType = INT( _FV_type.m_val.intVal );
				if( ! ( 1 <= nType && nType <= 3 ) )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
					return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
				}
			}
			COleDateTime _odt( _FV.m_val.date );
			INT nRetVal = INT( _odt.GetDayOfWeek() );
			__EXT_DEBUG_GRID_ASSERT( 1 <= nRetVal && nRetVal <= 7 ); // DEFAULT BEHAVIOR: Numbers 1 (Sunday) through 7 (Saturday). 
			switch( nType )
			{
			case 2:
				nRetVal --;
				if( nRetVal == 0 )
					nRetVal = 7;
				__EXT_DEBUG_GRID_ASSERT( 1 <= nRetVal && nRetVal <= 7 ); // Numbers 1 (Monday) through 7 (Sunday).
			break;
			case 3:
				nRetVal -= 2;
				if( nRetVal < 0 )
					nRetVal += 7;
				__EXT_DEBUG_GRID_ASSERT( 0 <= nRetVal && nRetVal <= 6 ); // Numbers 0 (Monday) through 6 (Sunday).
			break;
			} // switch( nType )
			_FV.m_val._VariantAssign( nRetVal, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_DateTime_WEEKDAY

	class __PROF_UIS_API FF_DateTime_WEEKNUM : public CFFP < CExtFormulaFunction, 1, 2, CExtFormulaFunction::__FFC_DATE_AND_TIME >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("WEEKNUM"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns the week number in the year.") )
			__EXT_FF_PARM( 1, _T("serial_number"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("return_type"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( ! _FV.m_val._VariantChangeType( VT_DATE ) )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_DATE_CONVERSION_ERROR;
			}
			INT nType = 1;
			if( nParameterCount > 1 )
			{
				CExtFormulaValue _FV_type;
				err = pFI->GetChildAt(1)->ComputeValue( _mapCellsAlreadyComputed, _FV_type, _FDM, ppErrorLocationForumaItem );
				if( err != CExtFormulaValue::__EFEC_OK )
					return err;
				if( ! _FV_type.m_val._VariantChangeType( VT_I4 ) )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
					return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
				}
				nType = INT( _FV_type.m_val.intVal );
				if( ! ( 1 <= nType && nType <= 2 ) )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI->GetChildAt( 1 );
					return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
				}
			}
			COleDateTime _odt( _FV.m_val.date );
			COleDateTime _odtTYB( _odt.GetYear(), 1, 1, 0, 0, 0 );
			COleDateTimeSpan _odts = _odt - _odtTYB;
			INT nFirstWeekDays = 7 - ( _odtTYB.GetDayOfWeek() - ( ( nType == 2 ) ? 0 : 1 ) );
			INT nDays = _odts.GetDays();
			INT nRetVal = ( nDays + nFirstWeekDays + 1 ) / 7 + 1;
			_FV.m_val._VariantAssign( nRetVal, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_DateTime_WEEKNUM

	class __PROF_UIS_API FF_LookupRef_CHOOSE : public CFFP < CExtFormulaFunction, 2, -1, CExtFormulaFunction::__FFC_LOOKUP_AND_REFERENCE >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("CHOOSE"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Chooses a value or action to perform from a list of values, based on an index number.") )
			__EXT_FF_PARM( 1, _T("index_num"), _T("Number parameter.") )
			__EXT_FF_PARM( 2, _T("value%d"), _T("Value parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			INT nParameterCount = pFI->GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( nParameterCount ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			if( ! _FV.m_val._VariantChangeType( VT_I4 ) )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_BOOL_CONVERSION_ERROR;
			}
			INT nChooseIndex = INT(_FV.m_val.intVal), nMaxChooseIndex = nParameterCount - 1;
			if( ! ( 1 <= nChooseIndex && nChooseIndex <= nMaxChooseIndex ) )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_PARAMETER_IS_OUT_OF_RANGE;
			}
			err = pFI->GetChildAt(nChooseIndex)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			return err;
		}
	}; /// class FF_LookupRef_CHOOSE

	class __PROF_UIS_API FF_Info__impl_check_1 : public CFFPCC1 < CExtFormulaFunction::__FFC_INFORMATION >
	{
	};

	class __PROF_UIS_API FF_Info_ISEVEN : public FF_Info__impl_check_1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ISEVEN"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns TRUE if the number is even.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnCheckValue(
			bool & bCheckResult,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			bCheckResult;
			_FDM;
			if( _FV.m_val.vt == VT_R4 )
			{
				if( _FV.m_val.fltVal < float(0.0) )
					_FV.m_val.fltVal = -_FV.m_val.fltVal;
				_FV.m_val.fltVal = float( _FV.m_val.fltVal - ::fmodl( double(_FV.m_val.fltVal), 1.0 ) );
			}
			if( _FV.m_val.vt == VT_R8 )
			{
				if( _FV.m_val.dblVal < double(0.0) )
					_FV.m_val.dblVal = -_FV.m_val.dblVal;
				_FV.m_val.dblVal = _FV.m_val.dblVal - double( ::fmodl( _FV.m_val.dblVal, 1.0 ) );
			}
			if( ! _FV.m_val._VariantChangeType( VT_I4 ) )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
			}
			INT nCheckVal = INT( _FV.m_val.intVal );
			if( nCheckVal < 0 )
				nCheckVal = -nCheckVal;
			bCheckResult = ( ( nCheckVal & 1 ) == 0 ) ? true : false;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_Info_ISEVEN

	class __PROF_UIS_API FF_Info_ISODD : public FF_Info__impl_check_1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ISODD"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns TRUE if the number is odd.") )
			__EXT_FF_PARM( 1, _T("number"), _T("Number parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnCheckValue(
			bool & bCheckResult,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			bCheckResult;
			_FDM;
			if( _FV.m_val.vt == VT_R4 )
			{
				if( _FV.m_val.fltVal < float(0.0) )
					_FV.m_val.fltVal = -_FV.m_val.fltVal;
				_FV.m_val.fltVal = float( _FV.m_val.fltVal - ::fmodl( double(_FV.m_val.fltVal), 1.0 ) );
			}
			if( _FV.m_val.vt == VT_R8 )
			{
				if( _FV.m_val.dblVal < double(0.0) )
					_FV.m_val.dblVal = -_FV.m_val.dblVal;
				_FV.m_val.dblVal = _FV.m_val.dblVal - double( ::fmodl( _FV.m_val.dblVal, 1.0 ) );
			}
			if( ! _FV.m_val._VariantChangeType( VT_I4 ) )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
				return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
			}
			INT nCheckVal = INT( _FV.m_val.intVal );
			if( nCheckVal < 0 )
				nCheckVal = -nCheckVal;
			bCheckResult = ( ( nCheckVal & 1 ) != 0 ) ? true : false;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_Info_ISODD

	class __PROF_UIS_API FF_Info_ISLOGICAL : public FF_Info__impl_check_1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ISLOGICAL"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Checks whether a value is a logical value (TRUE or FALSE), and returns TRUE or FALSE.") )
			__EXT_FF_PARM( 1, _T("value"), _T("Value parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnCheckValue(
			bool & bCheckResult,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			bCheckResult;
			_FDM;
			pFI;
			ppErrorLocationForumaItem;
			bCheckResult = ( _FV.m_val.vt == VT_BOOL ) ? true : false;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_Info_ISLOGICAL

	class __PROF_UIS_API FF_Info_ISNUMBER : public FF_Info__impl_check_1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ISNUMBER"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Checks whether a value is a number, and returns TRUE or FALSE.") )
			__EXT_FF_PARM( 1, _T("value"), _T("Value parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnCheckValue(
			bool & bCheckResult,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			bCheckResult;
			_FDM;
			pFI;
			ppErrorLocationForumaItem;
			bCheckResult =
				(	_FV.m_val.vt == VT_I2
				||	_FV.m_val.vt == VT_UI2
				||	_FV.m_val.vt == VT_I4
				||	_FV.m_val.vt == VT_UI4
				||	_FV.m_val.vt == VT_I8
				||	_FV.m_val.vt == VT_UI8
				||	_FV.m_val.vt == VT_R4
				||	_FV.m_val.vt == VT_R8
				||	_FV.m_val.vt == VT_DECIMAL // h(?)
				||	_FV.m_val.vt == VT_ERROR // h(?)
				||	_FV.m_val.vt == VT_HRESULT // h(?)
				||	_FV.m_val.vt == VT_INT // h(?)
				||	_FV.m_val.vt == VT_UINT // h(?)
				||	_FV.m_val.vt == VT_DATE // h(?)
				)
				? true : false;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_Info_ISNUMBER

	class __PROF_UIS_API FF_Info_ISTEXT : public FF_Info__impl_check_1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ISTEXT"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Checks whether a value is text, and returns TRUE or FALSE.") )
			__EXT_FF_PARM( 1, _T("value"), _T("Value parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnCheckValue(
			bool & bCheckResult,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			bCheckResult;
			_FDM;
			pFI;
			ppErrorLocationForumaItem;
			bCheckResult = ( _FV.m_val.vt == VT_BSTR ) ? true : false;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_Info_ISTEXT

	class __PROF_UIS_API FF_Info_ISBLANK : public FF_Info__impl_check_1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ISBLANK"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Checks whether a reference is to an empty cell, and returns TRUE or FALSE.") )
			__EXT_FF_PARM( 1, _T("value"), _T("Value parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnCheckValue(
			bool & bCheckResult,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			bCheckResult;
			_FDM;
			pFI;
			ppErrorLocationForumaItem;
			bCheckResult = ( ( _FV.m_val.GetStyleEx() & __EGCS_EX_EMPTY ) != 0 ) ? true : false;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_Info_ISBLANK

	class __PROF_UIS_API FF_Info_ISNOTEXT : public FF_Info__impl_check_1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ISNOTEXT"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Checks whether a value is not text (blank cells are not text), and returns TRUE or FALSE.") )
			__EXT_FF_PARM( 1, _T("value"), _T("Value parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnCheckValue(
			bool & bCheckResult,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			bCheckResult;
			_FDM;
			pFI;
			ppErrorLocationForumaItem;
			bCheckResult = ( ( _FV.m_val.GetStyleEx() & __EGCS_EX_EMPTY ) != 0 ) ? true : false;
			if( ! bCheckResult )
			{
				if( _FV.m_val.vt == VT_BSTR )
				{
					USES_CONVERSION;
					CString strN = OLE2CT( _FV.m_val.bstrVal );
					if( strN.GetLength() == 0 )
						bCheckResult = true;
				}
			}
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_Info_ISNOTEXT

	class __PROF_UIS_API FF_Info_ISERROR : public FF_Info__impl_check_1
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ISERROR"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Checks whether a value is an error, and returns TRUE or FALSE.") )
			__EXT_FF_PARM( 1, _T("value"), _T("Value parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnCheckError(
			CExtFormulaValue::e_formula_error_code_t err,
			bool & bCheckResult,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( err != CExtFormulaValue::__EFEC_OK );
			bCheckResult = true;
			err;
			_FV;
			_FDM;
			pFI;
			ppErrorLocationForumaItem;
			return CExtFormulaValue::__EFEC_OK;
		}
		virtual CExtFormulaValue::e_formula_error_code_t OnCheckValue(
			bool & bCheckResult,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			bCheckResult;
			_FDM;
			pFI;
			ppErrorLocationForumaItem;
			bCheckResult = 
				(	( _FV.m_val.vt == VT_ERROR )
				||	( _FV.m_val.vt == VT_HRESULT && _FV.m_val.intVal != 0 )
				) ? true : false;
			return CExtFormulaValue::__EFEC_OK;
		}
	}; /// class FF_Info_ISERROR

	class __PROF_UIS_API FF_Info_ISERR : public FF_Info_ISERROR // temp.impl.
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ISERR"); }
	}; /// class FF_Info_ISERR

	class __PROF_UIS_API FF_Info_ISNA : public FF_Info_ISERROR // temp.impl.
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("ISNA"); }
	}; /// class FF_Info_ISNA

	class __PROF_UIS_API FF_Info_TYPE : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_INFORMATION >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("TYPE"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Returns an integrer representing the data type of a value: number = 1; text = 2; logical value = 4; error value = 16; array = 64.") )
			__EXT_FF_PARM( 1, _T("value"), _T("Value parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			CExtFormulaValue::e_formula_error_code_t err = pFI->GetChildAt(0)->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			INT nRetVal = 1; // number
			if( err != CExtFormulaValue::__EFEC_OK )
			{
				nRetVal = 16; 
				err = CExtFormulaValue::__EFEC_OK;
			}
			else
			{
				if( _FV.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT )
				{
					switch( _FV.m_val.vt )
					{
					case VT_BSTR:		nRetVal = 2; break;
					case VT_BOOL:		nRetVal = 4; break;
					case VT_ERROR: // h(?)
										nRetVal = 16; break;
					case VT_HRESULT: // h(?)
										nRetVal = ( _FV.m_val.intVal != 0 ) ? 16 : 1; break;
					}
				} // if( _FV.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT )
				else if( _FV.m_eType == CExtFormulaValue::__EFVT_CELL_RANGE )
					nRetVal = 64;
				else
					nRetVal = 2; // assuming text
			}
			_FV.m_val._VariantAssign( nRetVal, VT_I4 );
			_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
			return err;
		}
	}; /// class FF_Info_TYPE

	class __PROF_UIS_API FF_Info_N : public CFFP < CExtFormulaFunction, 1, 1, CExtFormulaFunction::__FFC_INFORMATION >
	{
	public:
		virtual __EXT_MFC_SAFE_LPCTSTR GetFunctionName() const { return _T("N"); }
		__EXT_FF_DESCRIPTION_BEGIN
			__EXT_FF_FUNC( _T("Converts non-number value to a number, dates to serial numbers, TRUE to 1, anything else to 0 (zero).") )
			__EXT_FF_PARM( 1, _T("value"), _T("Value parameter.") )
		__EXT_FF_DESCRIPTION_END
		virtual CExtFormulaValue::e_formula_error_code_t OnExec(
			CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
			CExtFormulaValue & _FV,
			CExtFormulaDataManager & _FDM,
			const CExtFormulaItem * pFI,
			const CExtFormulaItem ** ppErrorLocationForumaItem = NULL
			)
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			__EXT_DEBUG_GRID_ASSERT( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL );
			__EXT_DEBUG_GRID_ASSERT( IsParameterCountValid( pFI->GetChildCount() ) );
			const CExtFormulaItem * pPFI = pFI->GetChildAt( 0 );
			CExtFormulaValue::e_formula_error_code_t err = pPFI->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
				return err;
			if( _FV.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT )
			{
				switch( _FV.m_val.vt )
				{
				case VT_I2:
				case VT_UI2:
				case VT_I4:
				case VT_UI4:
				case VT_I8:
				case VT_UI8:
				case VT_R4:
				case VT_R8:
				break;
				case VT_DECIMAL: // h(?)
				case VT_ERROR: // h(?)
				case VT_HRESULT: // h(?)
				case VT_INT: // h(?)
				case VT_UINT: // h(?)
					if( ! _FV.m_val._VariantChangeType( VT_I4 ) )
					{
						if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
							(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
						return CExtFormulaValue::__EFEC_TYPE_I4_CONVERSION_ERROR;
					}
				break;
				case VT_DATE: // h(?)
					{
						_FV.PreAdjustDT2R8();
						if( ! _FV.m_val._VariantChangeType( VT_R8 ) )
						{
							if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
								(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
							return CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
						}
					}
				break;
				case VT_BSTR:
				case VT_BOOL:
					if( ! _FV.m_val._VariantChangeType( VT_R8 ) )
					{
						if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
							(*ppErrorLocationForumaItem) = pFI->GetChildAt( 0 );
						return CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
					}
				break;
				} // switch( _FV.m_val.vt )
			} // if( _FV.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT )
			else
			{
				_FV.Empty();
				_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
				_FV.m_val._VariantAssign( 0, VT_I4 );
			} // else from if( _FV.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT )
			return err;
		}
	}; /// class FF_Info_N

}; /// class CExtFormulaFunctionSet

//////////////////////////////////////////////////////////////
// class CExtFormulaParser

#define __EXT_FORMULA_BUILT_IN_PARSER__

class __PROF_UIS_API CExtFormulaParser
{
protected:
	__EXT_MFC_SAFE_LPCTSTR m_pExternalBuffer;
	INT m_nExternalBufferLen;
	INT m_nExternalBufferPos;
	static const int g_ebp_gap;
	CExtHashMapT < CExtFormulaItem *, CExtFormulaItem *, bool, bool > m_mapAI; // analyzed items
public:
	bool m_bParserIsInTheErrorState:1;
	CExtSafeString m_strToken;
	CExtFormulaItem * m_pFormulaRoot;
	INT m_nScanPosStart, m_nScanPosEnd;
	CPoint m_ptTextPos, m_ptScanPosStart, m_ptScanPosEnd;
	CExtFormulaParser(
		__EXT_MFC_SAFE_LPTSTR pExternalBuffer = NULL,
		INT nExternalBufferLen = 0
		);
	~CExtFormulaParser();
	void Init(
		__EXT_MFC_SAFE_LPCTSTR pExternalBuffer = NULL,
		INT nExternalBufferLen = 0
		);
	void SaveScanPos( bool bStart, bool bEnd );
	bool IsEOF() const;
	INT PositionGet() const;
	void PositionSet( INT nPosition );
	__EXT_MFC_SAFE_TCHAR ReadChar();
	__EXT_MFC_SAFE_TCHAR UnReadChar();
	INT ReadIdentifier();
	bool ReadConstantIntOct();
	bool ReadConstantIntDecimal();
	bool ReadConstantIntHex();
	bool ReadConstantFloat();
	bool ReadSlashChar( __EXT_MFC_SAFE_LPTSTR pc, INT * p_nValueLength, INT * p_nPos );
	//bool ReadConstantChar();
	bool ReadConstantString();
	INT ReadBasicLexem();
	INT Lex();
#if (defined __EXT_FORMULA_BUILT_IN_PARSER__)
	static const int formula_constant_float;
	static const int formula_constant_int;
	static const int formula_constant_string;
	static const int formula_identifier;
	static const int formula_operator_greater_equal;
	static const int formula_operator_less_equal;
	static const int formula_operator_not_equal;
	int m_nParserLexCounter;
	void * m_pParserStackA, * m_pParserStackB;
	short * yyss;
	short * yysslim;
	CExtFormulaItem ** yyvs;
	short * yyssp;
	CExtFormulaItem ** yyvsp;
	CExtFormulaItem * yyval;
	CExtFormulaItem * yylval;
	__EXT_MFC_UINT_PTR yystacksize;

	bool Parse();
	void OnError( __EXT_MFC_SAFE_LPCTSTR strErrorDescription );
	int OnGrowStack();
	int OnParse();
#endif // (defined __EXT_FORMULA_BUILT_IN_PARSER__)
}; /// class CExtFormulaParser

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaAlphabet

class __PROF_UIS_API CExtFormulaAlphabet
{
public:
	CExtFormulaAlphabet();
	virtual ~CExtFormulaAlphabet();
	virtual LONG GetAlphabetSize() const;
	virtual __EXT_MFC_SAFE_LPCTSTR OnQueryAlphabetArray() const;
	virtual bool PreTranslateAlphaChar( __EXT_MFC_SAFE_TCHAR & _tchr ) const;
	virtual bool CanBeFirstChar( __EXT_MFC_SAFE_TCHAR _tchr ) const;
	virtual LONG GetAlphaCharIndex( __EXT_MFC_SAFE_TCHAR _tchr ) const;
	virtual bool IsAlphaChar( __EXT_MFC_SAFE_TCHAR _tchr ) const;
	virtual LONG GetMaxAlphaStrLen() const;
	virtual LONG GetMaxAlphaStrVal() const;
	virtual LONG GetMaxNumStrVal() const;
	virtual LONG ParseStrPartAlpha( __EXT_MFC_SAFE_LPCTSTR & str ) const;
	virtual LONG ParseStrPartNum( __EXT_MFC_SAFE_LPCTSTR & str ) const;
	virtual bool ParseCheckEmptySpaceChar( __EXT_MFC_SAFE_TCHAR _tchr ) const;
	virtual void ParsePassEmptySpace( __EXT_MFC_SAFE_LPCTSTR & str ) const;
	virtual CPoint ParseCellName( __EXT_MFC_SAFE_LPCTSTR & str ) const;
	virtual CRect ParseRangeName(
		__EXT_MFC_SAFE_LPCTSTR & strLT,
		__EXT_MFC_SAFE_LPCTSTR & strRB,
		bool * p_bErrorInLT = NULL,
		bool * p_bErrorInRB = NULL,
		const CExtFormulaDataManager * pFDM = NULL // if not NULL - use data provider for getting dimensions and generating full row / full column range names
		) const;
	virtual CPoint GetMaxValue() const;
	virtual bool GetInverseNamingMode() const;
	virtual CExtSafeString GetAlphaStr( LONG nVal ) const;
	virtual CExtSafeString GetLocationStr( LONG nColNo, LONG nRowNo ) const;
	CExtSafeString GetLocationStr( const POINT & pt ) const { return GetLocationStr( pt.x, pt.y ); }
	CExtSafeString GetLocationStr( const CExtGridDataProvider::packed_location_t & _loc ) const { return GetLocationStr( CExtGridDataProvider::_L2P( _loc ) ); }
	virtual CExtSafeString GetRangeLocationStr(
		const RECT & _range,
		const CExtFormulaDataManager * pFDM = NULL // if not NULL - use data provider for getting dimensions and generating full row / full column range names
		) const;
}; /// class CExtFormulaAlphabet

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaInplaceTipWnd

class __PROF_UIS_API CExtFormulaInplaceTipWnd : public CExtPopupMenuTipWnd
{
public:
	DECLARE_DYNAMIC( CExtFormulaInplaceTipWnd );
	CExtFormulaInplaceEdit * m_pWndFIE;
	CHARRANGE m_crBold;
	CExtEditBase m_wndEdit;
	CRect m_rcEditMargin;
	CExtFormulaInplaceTipWnd();
	virtual ~CExtFormulaInplaceTipWnd();
	virtual CRect OnCalcEditRect();
	virtual void OnFormatText(
		__EXT_MFC_SAFE_LPCTSTR strText = NULL
		);
	virtual void SetText( __EXT_MFC_SAFE_LPCTSTR lpszText );
protected:
	//{{AFX_VIRTUAL(CExtFormulaInplaceTipWnd)
	protected:
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam );
	//}}AFX_VIRTUAL
protected:
    //{{AFX_MSG(CExtFormulaInplaceTipWnd)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
}; // class CExtFormulaInplaceTipWnd

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaInplaceListBox

#define __EXT_FIPLB_ICON_INDEX_FX		0
#define __EXT_FIPLB_ICON_INDEX_NR		1

class __PROF_UIS_API CExtFormulaInplaceListBox : public CExtGridWnd
{
protected:
	CExtFormulaInplaceEdit * m_pWndFIE;
public:
	CRect m_rcNcAreaMargins;
	COLORREF m_clrNcArea;
	LONG m_nRowCountMin, m_nRowCountMax; // height adjustment
	DECLARE_DYNAMIC( CExtFormulaInplaceListBox );
	CExtFormulaInplaceListBox();
	virtual ~CExtFormulaInplaceListBox();
	
	virtual bool Create( CExtFormulaInplaceEdit & wndFIE );
	virtual void HelpTip();
	virtual bool OnSiwQueryFocusedControlState() const;
	virtual void OnGbwFocusChanged(
		const POINT & ptOldFocus,
		const POINT & ptNewFocus
		);
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump( CDumpContext & dc ) const;
#endif // _DEBUG
protected:
	//{{AFX_VIRTUAL(CExtFormulaInplaceListBox)
	protected:
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam );
	//}}AFX_VIRTUAL
protected:
    //{{AFX_MSG(CExtFormulaInplaceListBox)
	afx_msg void OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR * lpncsp );
	afx_msg void OnNcPaint();
	//}}AFX_MSG
	afx_msg UINT OnNcHitTest( CPoint point );
	DECLARE_MESSAGE_MAP()
}; // class __PROF_UIS_API CExtFormulaInplaceListBox

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaInplaceEdit

class __PROF_UIS_API CExtFormulaInplaceEdit : public CExtGridInplaceEdit
{
public:
	bool m_bParserMode:1, m_bGridFocusMode:1, m_bFormulaRangeDndCreation:1, m_bFormulaRangeDndModification:1;
	UINT m_nGridFocusTimerID;
	LONG m_nGridSelectionTrackerRunningShift;
	virtual CExtFormulaItem * _GridFocusMode_Check(
		bool bRedrawTracker = false
		);
	virtual void _GridFocusMode_Cancel();
	CHARFORMAT m_cfDefault;
	CHARRANGE m_crTracked, m_crSavedBeforeFormat;
	CExtFormulaItem * m_pFormula;
	CExtNSB < CExtFormulaInplaceListBox > m_wndIPLB;
	CExtFormulaInplaceTipWnd m_wndIPT;
	DECLARE_DYNAMIC( CExtFormulaInplaceEdit );
	CExtFormulaInplaceEdit(
		HWND hWndParentForEditor,
		CExtGridWnd & wndGrid,
		CExtGridCell & cell,
		LONG nVisibleColNo,
		LONG nVisibleRowNo,
		LONG nColNo,
		LONG nRowNo,
		INT nColType,
		INT nRowType,
		const RECT & rcCellExtra,
		const RECT & rcCell,
		const RECT & rcInplaceControl,
		LONG nLastEditedColNo,
		LONG nLastEditedRowNo
		);
	virtual ~CExtFormulaInplaceEdit();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump( CDumpContext & dc ) const;
#endif // _DEBUG
	virtual HWND _OnCreateWindowHandleImpl(
		DWORD dwExStyle, LPCTSTR lpClassName, LPCTSTR lpWindowName, DWORD dwStyle,
		int X, int Y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam
		);
	//{{AFX_VIRTUAL(CExtFormulaInplaceEdit)
	protected:
	virtual void PreSubclassWindow();
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam );
	virtual void PostNcDestroy();
	public:
	virtual BOOL PreTranslateMessage( MSG * pMsg );
	//}}AFX_VIRTUAL

    //{{AFX_MSG(CExtFormulaInplaceEdit)
	//}}AFX_MSG
	afx_msg LRESULT OnSetText( WPARAM wParam, LPARAM lParam );
	afx_msg void OnChange();
	afx_msg void OnProtected( NMHDR * pNMHDR, LRESULT * pResult );
	DECLARE_MESSAGE_MAP()
public:
	virtual CExtFormulaGridCell & OnQueryFormulaCell();
	const CExtFormulaGridCell & OnQueryFormulaCell() const;
	virtual CExtFormulaGridWnd & OnQueryFormulaGridWnd();
	const CExtFormulaGridWnd & OnQueryFormulaGridWnd() const;
	virtual CExtFormulaDataManager & OnQueryFDM();
	const CExtFormulaDataManager & OnQueryFDM() const;

	virtual void OnFormatFormulaText();
	virtual void OnFormatFormulaCase( INT nStart, INT nEnd );
	virtual void OnFormatFormulaRange( INT nStart, INT nEnd, bool bBold, bool bItalic, COLORREF clr );

	virtual void OnFormulaRangeDndModification( const CExtGridHitTestInfo & htInfo );
	virtual bool OnFormulaRangeDndCreation( const CExtGridHitTestInfo & htInfo );

	virtual CExtFormulaInplaceListBox * OnQueryIPLB(
		bool nCreate = true
		);
	virtual CExtFormulaInplaceTipWnd * OnQueryIPT();
	virtual void UpdateIPT( bool bDelay );
}; /// class CExtFormulaInplaceEdit

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaGridCell

class __PROF_UIS_API CExtFormulaGridCell : public CExtGridCellVariant
{
protected:
	CExtSafeString m_strFormulaText;
	CExtFormulaItem * m_pFormula;
	const CExtFormulaItem * m_pErrorLocationForumaItem;
	CExtFormulaValue::e_formula_error_code_t m_eFormulaError;
public:
	DECLARE_SERIAL( CExtFormulaGridCell );
	IMPLEMENT_ExtGridCell_Clone( CExtFormulaGridCell, CExtGridCellVariant );
	CExtFormulaGridCell(
		CExtGridDataProvider * pDataProvider = NULL
		);
	CExtFormulaGridCell( const CExtGridCell & other );
	virtual ~CExtFormulaGridCell();
	virtual void Assign( const CExtGridCell & other );
	virtual void Serialize( CArchive & ar );
	virtual void SerializeFormulaData( CArchive & ar );

	virtual bool OnSetCursor(
		CExtGridWnd & wndGrid,
		const CExtGridHitTestInfo & htInfo
		);
	virtual void OnInplaceControlTextInputComplete(
		HWND hWndInplaceControl,
		CExtGridWnd & wndGrid,
		LONG nVisibleColNo,
		LONG nVisibleRowNo,
		LONG nColNo,
		LONG nRowNo,
		INT nColType,
		INT nRowType,
		__EXT_MFC_SAFE_LPCTSTR sTextNew,
		bool bSaveChanges
		);
	virtual HRESULT OnParseText( __EXT_MFC_SAFE_LPCTSTR sText ) const;
	virtual HWND OnInplaceControlCreate(
		HWND hWndParentForEditor,
		CExtGridWnd & wndGrid,
		LONG nVisibleColNo,
		LONG nVisibleRowNo,
		LONG nColNo,
		LONG nRowNo,
		INT nColType,
		INT nRowType,
		const RECT & rcCellExtra,
		const RECT & rcCell,
		const RECT & rcInplaceControl,
		LONG nLastEditedColNo,
		LONG nLastEditedRowNo
		);
	virtual HWND _OnInplaceControlCreateInstanceImpl(
		HWND hWndParentForEditor,
		CExtGridWnd & wndGrid,
		LONG nVisibleColNo,
		LONG nVisibleRowNo,
		LONG nColNo,
		LONG nRowNo,
		INT nColType,
		INT nRowType,
		const RECT & rcCellExtra,
		const RECT & rcCell,
		const RECT & rcInplaceControl,
		const RECT & rcInplaceControlAdjusted,
		LONG nLastEditedColNo,
		LONG nLastEditedRowNo
		);
	virtual UINT OnQueryDrawTextFlagsForInplaceEdit(
		LONG nVisibleColNo,
		LONG nVisibleRowNo,
		LONG nColNo,
		LONG nRowNo,
		INT nColType,
		INT nRowType,
		DWORD dwAreaFlags,
		DWORD dwHelperPaintFlags,
		bool bIncludeHorizontalFlags = true,
		bool bIncludeVerticalFlags = true,
		bool bIncludeOtherFlags = true
		) const;
	virtual void OnQueryTextForInplaceControl( 
		CExtSafeString & strCopy 
		) const;
	virtual bool OnFormatFormulaInplaceEdit( CExtFormulaItem * pFI, CExtFormulaInplaceEdit & wndFIE );

	virtual bool OnFormatShiftedText(
		CExtFormulaDataManager & _FDM,
		LONG nShiftX,
		LONG nShiftY,
		const CExtGR2D * pRangeToShift = NULL
		);
	virtual bool OnQueryFormulaText(
		CExtSafeString & strFormulaText,
		bool bGet
		);
	virtual void FormulaSetError( CExtFormulaValue::e_formula_error_code_t err );
	virtual void FormulaSetDependencyError(
		CExtFormulaDataManager & _FDM,
		CPoint ptError
		);
	virtual void FormulaSetCrossReferenceError(
		CExtFormulaDataManager & _FDM,
		CRect rcLocation
		);
	bool FormulaTextGet(
		CExtSafeString & strFormulaText
		) const;
	bool FormulaTextSet(
		__EXT_MFC_SAFE_LPCTSTR strFormulaText
		);
	virtual bool FormulaParse(
		CExtFormulaDataManager & _FDM
		);
	virtual bool FormulaStateIsEmpty() const;
	virtual void FormulaStateClear();
	virtual CExtFormulaItem * FormulaGet();
	const CExtFormulaItem * FormulaGet() const;
	virtual const CExtFormulaItem * FormulaGetErrorLocation() const;
	virtual CExtFormulaValue::e_formula_error_code_t FormulaGetErrorCode() const;
	virtual bool _FormulaComputeValueImpl(
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
		LONG nColNo,
		LONG nRowNo,
		CExtFormulaValue & _FV,
		CExtFormulaDataManager & _FDM,
		const CExtFormulaItem * pExternalFI
		);
	virtual bool FormulaComputeValue(
		LONG nColNo,
		LONG nRowNo,
		CExtFormulaValue & _FV,
		CExtFormulaDataManager & _FDM,
		const CExtFormulaItem * pExternalFI
		);
	virtual void FormulaComputeDependencySourceRange(
		CExtFormulaDataManager & _FDM,
		CExtGR2D & _range
		) const;
	virtual bool OnAccelCommand( DWORD dwEGSA, CExtGridWnd & wndGrid, LPVOID pData, LONG nCounter, LONG nColNo, LONG nRowNo, const CExtGR2D & _range );
	virtual void OnAccelCommandComplete( DWORD dwEGSA, CExtGridWnd & wndGrid, LONG nColNo, LONG nRowNo, const CExtGR2D & _rangeSelOld, const CExtGR2D & _rangeSelNew, const CSize & _sizeShift );
	virtual void TextSetOnPaste(
		__EXT_MFC_SAFE_LPCTSTR str = __EXT_MFC_SAFE_LPCTSTR(NULL), // empty text
		bool bAllowChangeDataType = false
		);
}; /// class CExtFormulaGridCell

/////////////////////////////////////////////////////////////////////////////
// template class CExtFDP
// formula data provider and data manager

template < class _DPC, class _FDMC > class CExtFDP : public _DPC, public _FDMC
{
public:
	INT m_nDefaultColumnExtent, m_nDefaultRowExtent, m_nMinColumnExtent, m_nMinRowExtent, m_nMaxColumnExtent, m_nMaxRowExtent;
	CRuntimeClass * m_pRtcHdrCells, * m_pRtcDataCells;
	CExtFDP(
		CRuntimeClass * pRtcHdrCells = NULL,
		CRuntimeClass * pRtcDataCells = NULL
		)
		: m_nDefaultColumnExtent( 64 )
		, m_nDefaultRowExtent( 20 )
		, m_nMinColumnExtent( 4 )
		, m_nMinRowExtent( 4 )
		, m_nMaxColumnExtent( -1 )
		, m_nMaxRowExtent( -1 )
		, m_pRtcHdrCells( pRtcHdrCells )
		, m_pRtcDataCells( pRtcDataCells )
	{
		__EXT_DEBUG_GRID_ASSERT( m_pRtcHdrCells  == NULL || m_pRtcHdrCells->IsDerivedFrom(  RUNTIME_CLASS(CExtGridCell) ) );
		__EXT_DEBUG_GRID_ASSERT( m_pRtcDataCells == NULL || m_pRtcDataCells->IsDerivedFrom( RUNTIME_CLASS(CExtGridCell) ) );
	}
	virtual ~CExtFDP()
	{
	}
	virtual CExtGridDataProvider & OnQueryDataProvider()
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( this );
		return (*this);
	}
	virtual bool OnGetFormulaCell(
		LONG nColNo,
		LONG nRowNo,
		CExtGridCell ** ppCell,
		CRuntimeClass * pInitRTC = NULL,
		bool bAutoFindValue = true, // auto find row/column default value (only when pInitRTC=NULL)
		bool bUseColumnDefaultValue = true // false - use row default value (only when bAutoFindValue=true)
		)
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( this );
		__EXT_DEBUG_GRID_ASSERT( ppCell != NULL );
		(*ppCell) = NULL;
		if( nColNo < 0L || nRowNo < 0L )
			return false;
		ULONG nReservedColCount = 0L, nReservedRowCount = 0L;
		CacheReservedCountsGet( &nReservedColCount, &nReservedRowCount );
		ULONG nColCount = ColumnCountGet() - nReservedColCount;
		if( ULONG(nColNo) >= nColCount )
			return false;
		ULONG nRowCount = RowCountGet() - nReservedRowCount;
		if( ULONG(nRowNo) >= nRowCount )
			return false;
		ULONG nEffectiveColNo = ULONG(nColNo) + nReservedColCount;
		ULONG nEffectiveRowNo = ULONG(nRowNo) + nReservedRowCount;
		(*ppCell) = CellGet( nEffectiveColNo, nEffectiveRowNo, pInitRTC, bAutoFindValue, bUseColumnDefaultValue );
		return true;
	}
	virtual CExtGridCell * CellGet(
		ULONG nColNo,
		ULONG nRowNo,
		CRuntimeClass * pInitRTC = NULL,
		bool bAutoFindValue = true, // auto find row/column default value (only when pInitRTC=NULL)
		bool bUseColumnDefaultValue = true // false - use row default value (only when bAutoFindValue=true)
		)
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( this );
		bool bHeaderH = false, bHeaderV = false;
		ULONG nReservedColCount = 0L, nReservedRowCount = 0L;
		CExtGridCell * pCell = NULL;
		if( pInitRTC == NULL )
			pCell = _DPC::CellGet( nColNo, nRowNo, NULL, false, false );
		if( pCell == NULL )
			pCell = _DPC::CellGet( nColNo, nRowNo, pInitRTC, bAutoFindValue, bUseColumnDefaultValue );
		if( pCell == NULL && pInitRTC == NULL )
		{
			CacheReservedCountsGet( &nReservedColCount, &nReservedRowCount );
			if( ULONG(nColNo) < nReservedColCount )
				bHeaderV = true;
			if( ULONG(nRowNo) < nReservedRowCount )
				bHeaderH = true;
			__EXT_DEBUG_GRID_ASSERT( m_pRtcHdrCells  == NULL || m_pRtcHdrCells->IsDerivedFrom(  RUNTIME_CLASS(CExtGridCell) ) );
			__EXT_DEBUG_GRID_ASSERT( m_pRtcDataCells == NULL || m_pRtcDataCells->IsDerivedFrom( RUNTIME_CLASS(CExtGridCell) ) );
			if( bHeaderH && m_pRtcHdrCells != NULL )
			{
				if( nRowNo == 0L )
				{
					pCell = _DPC::CellGet( nColNo, nRowNo, m_pRtcHdrCells, bAutoFindValue, bUseColumnDefaultValue );
					if( pCell != NULL )
					{
						__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
						if( m_nDefaultColumnExtent >= 0 )
							pCell->ExtentSet( g_PaintManager->UiScalingDo( m_nDefaultColumnExtent, CExtPaintManager::__EUIST_X ), 0 );
						if( m_nMinColumnExtent >= 0 )
							pCell->ExtentSet( g_PaintManager->UiScalingDo( m_nMinColumnExtent, CExtPaintManager::__EUIST_X ), -1 );
						if( m_nMaxColumnExtent >= 0 )
							pCell->ExtentSet( g_PaintManager->UiScalingDo( m_nMaxColumnExtent, CExtPaintManager::__EUIST_X ), 1 );
						CExtFormulaAlphabet & _FA = OnQueryFormulaAlphabet();
						bool bINM = _FA.GetInverseNamingMode();
						if( ! bINM )
						{
							pCell->ModifyStyle( __EGCS_TA_HORZ_CENTER );
							LONG nColNoTrack = nColNo - nReservedColCount;
							CExtSafeString str = _FA.GetAlphaStr( nColNoTrack );
							pCell->TextSet( LPCTSTR(str) );
						}
						else
							pCell->ModifyStyle(
								__EGCS_TA_HORZ_RIGHT|__EGCS_HDR_ROW_COLUMN_NUMBER
									//|__EGCS_HDR_FOCUS_ARROW_RESERVE_SPACE|__EGCS_HDR_FOCUS_ARROW_DISPLAY
								);
					} // if( pCell != NULL )
				} // if( nRowNo == 0L )
			} // if( bHeaderH && m_pRtcHdrCells != NULL )
			else if( bHeaderV && m_pRtcHdrCells != NULL )
			{
				if( nColNo == 0L )
				{
					pCell = _DPC::CellGet( nColNo, nRowNo, m_pRtcHdrCells, bAutoFindValue, bUseColumnDefaultValue );
					if( pCell != NULL )
					{
						__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
						if( m_nDefaultRowExtent >= 0 )
							pCell->ExtentSet( g_PaintManager->UiScalingDo( m_nDefaultRowExtent, CExtPaintManager::__EUIST_Y ), 0 );
						if( m_nMinRowExtent >= 0 )
							pCell->ExtentSet( g_PaintManager->UiScalingDo( m_nMinRowExtent, CExtPaintManager::__EUIST_Y ), -1 );
						if( m_nMaxRowExtent >= 0 )
							pCell->ExtentSet( g_PaintManager->UiScalingDo( m_nMaxRowExtent, CExtPaintManager::__EUIST_Y ), 1 );
						CExtFormulaAlphabet & _FA = OnQueryFormulaAlphabet();
						bool bINM = _FA.GetInverseNamingMode();
						if( bINM )
						{
							pCell->ModifyStyle( __EGCS_TA_HORZ_CENTER );
							LONG nRowNoTrack = nRowNo - nReservedRowCount;
							CExtSafeString str = _FA.GetAlphaStr( nRowNoTrack );
							pCell->TextSet( LPCTSTR(str) );
						}
						else
							pCell->ModifyStyle(
								__EGCS_TA_HORZ_RIGHT|__EGCS_HDR_ROW_COLUMN_NUMBER
									//|__EGCS_HDR_FOCUS_ARROW_RESERVE_SPACE|__EGCS_HDR_FOCUS_ARROW_DISPLAY
								);
					} // if( pCell != NULL )
				} // if( nColNo == 0L )
			} // else if( bHeaderV && m_pRtcHdrCells != NULL )
			else if( m_pRtcDataCells != NULL )
			{
				pCell = _DPC::CellGet( nColNo, nRowNo, m_pRtcDataCells, bAutoFindValue, bUseColumnDefaultValue );
				if( pCell != NULL )
				{
					__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
					pCell->TextSet( _T("") );
				} // if( pCell != NULL )
			} // else if( m_pRtcDataCells != NULL )
		} // if( pCell == NULL && pInitRTC == NULL )
		return pCell;
	}
}; /// template class CExtFDP

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaGridWnd

// basic formula grid styles
// CExtGridWnd::FgGetStyle() and CExtGridWnd::FgModifyStyle()

// enable tool rectangle drag-n-dropping
#define __FGS_TOOL_RECT_DND									0x00000001
// enable expanding via drag-n-dropping the box at the right/bottom corner of tool rectangle
#define __FGS_TOOL_RECT_EXPANDING							0x00000002
#define __FGS_TOOL_RECT_MASK								(__FGS_TOOL_RECT_EXPANDING|__FGS_TOOL_RECT_DND)

// use non-selected background in the focused grid cell
#define __FGS_FOCUSED_CELL_WITH_NON_SELECTED_BKGND			0x00000004

// use rich editor in formula cells
#define __FGS_USE_RICH_FORMULA_EDITOR						0x00000008
// use formula tips with bold parameter highlighting
#define __FGS_USE_RICH_FORMULA_TIPS							0x00000010
// show formula ranges highlighted inside
#define __FGS_FORMULA_REFS_HIGHLIGHT						0x00000020
// allow d-n-d and resizing of highlighted formula ranges
#define __FGS_FORMULA_REFS_MODIFY							0x00000040
#define __FGS_FORMULA_REFS_MASK								(__FGS_FORMULA_REFS_HIGHLIGHT|__FGS_FORMULA_REFS_MODIFY)
// use popup list box with function names
#define __FGS_USE_AUTO_COMPLETE_LIST_BOX_FOR_FUNCTIONS		0x00000080
// use popup list box with named ranges
#define __FGS_USE_AUTO_COMPLETE_LIST_BOX_FOR_NAMED_RANGES	0x00000100
// use popup list box with function names and named ranges
#define __FGS_USE_AUTO_COMPLETE_LIST						(__FGS_USE_AUTO_COMPLETE_LIST_BOX_FOR_FUNCTIONS|__FGS_USE_AUTO_COMPLETE_LIST_BOX_FOR_NAMED_RANGES)

#define __FGS_USER_DEFINED_22								0x00000200
#define __FGS_USER_DEFINED_21								0x00000400
#define __FGS_USER_DEFINED_20								0x00000800
#define __FGS_USER_DEFINED_19								0x00001000
#define __FGS_USER_DEFINED_18								0x00002000
#define __FGS_USER_DEFINED_17								0x00004000
#define __FGS_USER_DEFINED_16								0x00008000
#define __FGS_USER_DEFINED_15								0x00010000
#define __FGS_USER_DEFINED_14								0x00020000
#define __FGS_USER_DEFINED_13								0x00040000
#define __FGS_USER_DEFINED_12								0x00080000
#define __FGS_USER_DEFINED_11								0x00100000
#define __FGS_USER_DEFINED_10								0x00200000
#define __FGS_USER_DEFINED_09								0x00400000
#define __FGS_USER_DEFINED_08								0x00800000
#define __FGS_USER_DEFINED_07								0x01000000
#define __FGS_USER_DEFINED_06								0x02000000
#define __FGS_USER_DEFINED_05								0x04000000
#define __FGS_USER_DEFINED_04								0x08000000
#define __FGS_USER_DEFINED_03								0x10000000
#define __FGS_USER_DEFINED_02								0x20000000
#define __FGS_USER_DEFINED_01								0x40000000
#define __FGS_USER_DEFINED_00								0x80000000

#define __FGS_DEFAULT	( \
		 __FGS_TOOL_RECT_DND | __FGS_TOOL_RECT_EXPANDING \
		|__FGS_FOCUSED_CELL_WITH_NON_SELECTED_BKGND \
		|__FGS_USE_RICH_FORMULA_EDITOR \
		|__FGS_USE_RICH_FORMULA_TIPS \
		|__FGS_FORMULA_REFS_HIGHLIGHT | __FGS_FORMULA_REFS_MODIFY \
		|__FGS_USE_AUTO_COMPLETE_LIST_BOX_FOR_FUNCTIONS | __FGS_USE_AUTO_COMPLETE_LIST_BOX_FOR_NAMED_RANGES \
		)

// extended formula grid styles
// CExtGridWnd::FgGetStyleEx() and CExtGridWnd::FgModifyStyleEx()
#define __FGS_EX_USER_DEFINED_31							0x00000001
#define __FGS_EX_USER_DEFINED_30							0x00000002
#define __FGS_EX_USER_DEFINED_29							0x00000004
#define __FGS_EX_USER_DEFINED_28							0x00000008
#define __FGS_EX_USER_DEFINED_27							0x00000010
#define __FGS_EX_USER_DEFINED_26							0x00000020
#define __FGS_EX_USER_DEFINED_25							0x00000040
#define __FGS_EX_USER_DEFINED_24							0x00000080
#define __FGS_EX_USER_DEFINED_23							0x00000100
#define __FGS_EX_USER_DEFINED_22							0x00000200
#define __FGS_EX_USER_DEFINED_21							0x00000400
#define __FGS_EX_USER_DEFINED_20							0x00000800
#define __FGS_EX_USER_DEFINED_19							0x00001000
#define __FGS_EX_USER_DEFINED_18							0x00002000
#define __FGS_EX_USER_DEFINED_17							0x00004000
#define __FGS_EX_USER_DEFINED_16							0x00008000
#define __FGS_EX_USER_DEFINED_15							0x00010000
#define __FGS_EX_USER_DEFINED_14							0x00020000
#define __FGS_EX_USER_DEFINED_13							0x00040000
#define __FGS_EX_USER_DEFINED_12							0x00080000
#define __FGS_EX_USER_DEFINED_11							0x00100000
#define __FGS_EX_USER_DEFINED_10							0x00200000
#define __FGS_EX_USER_DEFINED_09							0x00400000
#define __FGS_EX_USER_DEFINED_08							0x00800000
#define __FGS_EX_USER_DEFINED_07							0x01000000
#define __FGS_EX_USER_DEFINED_06							0x02000000
#define __FGS_EX_USER_DEFINED_05							0x04000000
#define __FGS_EX_USER_DEFINED_04							0x08000000
#define __FGS_EX_USER_DEFINED_03							0x10000000
#define __FGS_EX_USER_DEFINED_02							0x20000000
#define __FGS_EX_USER_DEFINED_01							0x40000000
#define __FGS_EX_USER_DEFINED_00							0x80000000

#define __FGS_EX_DEFAULT	0

#if ( _MFC_VER == 0x700 ) && (! defined __EXT_PROFUIS_STATIC_LINK)
	template class __PROF_UIS_API CExtNSB < CExtPPVW < CExtGridWnd > >;
#endif

class __PROF_UIS_API CExtFormulaGridWnd : public CExtNSB < CExtPPVW < CExtGridWnd > >
{
protected:
	DWORD m_dwFgStyle, m_dwFgStyleEx;
public:
	enum e_FormulaGridToolRectMode_t
	{
		__EFGTRM_SHIFT_FORMULAS			= 0,
		__EFGTRM_COPY_FORMULAS			= 1,
		__EFGTRM_EXPAND_SELECTION		= 2,
	};
protected:
	virtual e_FormulaGridToolRectMode_t _OnFormulaGridToolRect_ModeFromPressedKeys();
	virtual HCURSOR _OnFormulaGridToolRect_CursorFromMode( e_FormulaGridToolRectMode_t eFGTRM, CRect rcSelArea, CRect rcBox );
public:
	struct __PROF_UIS_API for_each_base_t : public CExtGridWnd::for_each_abstract_t
	{ // also used as for-each data type
		CExtFormulaGridWnd & m_wndFG;
		CExtFormulaDataManager & m_FDM;
		ULONG m_nCounterFormula;
		bool m_bWalkFormulasExpl:1;
		for_each_base_t( CExtFormulaGridWnd & _wndFG, const CExtGR2D & _range )
			: CExtGridWnd::for_each_abstract_t( _wndFG, _range )
			, m_wndFG( _wndFG )
			, m_FDM( _wndFG.OnGridQueryFormulaDataManager() )
			, m_nCounterFormula( 0L )
			, m_bWalkFormulasExpl( false )
		{
		}
		virtual bool OnForEachFormula( LPVOID pData, CExtFormulaGridCell & _cellFormula, ULONG nCounter, ULONG nCounterFormula,  ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			pData; _cellFormula; nCounter; nCounterFormula; nColNo; nRowNo; _range;
			return true; // default implementation does nothing, just count of passed through formula grid cells will be saved in the m_nCounterFormula property
		}
		virtual bool OnForEach( LPVOID pData, CExtGridCell & _cell, ULONG nCounter, ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			if( m_bWalkFormulasExpl )
			{
				CExtFormulaGridCell * pFC = DYNAMIC_DOWNCAST( CExtFormulaGridCell, (&_cell) );
				if( pFC != NULL )
				{
					CExtSafeString strFormulaText;
					if( ! pFC->FormulaTextGet( strFormulaText ) )
						return true; // assuming there is no formula
					if( strFormulaText.IsEmpty() )
						return true; // assuming there is no formula
					if( ! OnForEachFormula( pData, *pFC, nCounter, m_nCounterFormula, nColNo, nRowNo, _range ) )
						return false;
					m_nCounterFormula ++;
				}
			}
			return true; // default implementation will simply fill the m_nCounter property with the count of passed through grid cells
		}
	}; /// struct for_each_base_t

	struct __PROF_UIS_API for_each_erase_t : public for_each_base_t
	{
		bool m_bEraseFormulas:1, m_bEraseNonFormulas:1, m_bRecomputeFormulas:1;
		for_each_erase_t( bool bEraseFormulas, bool bEraseNonFormulas, bool bRecomputeFormulas, CExtFormulaGridWnd & _wndFG, const CExtGR2D & _range )
			: for_each_base_t( _wndFG, _range )
			, m_bEraseFormulas( bEraseFormulas )
			, m_bEraseNonFormulas( bEraseNonFormulas )
			, m_bRecomputeFormulas( bRecomputeFormulas )
		{
			__EXT_DEBUG_GRID_ASSERT( m_bEraseFormulas || bEraseNonFormulas );
			m_bWalkFormulasExpl = true;
		}
		virtual bool OnForEachFormula( LPVOID pData, CExtFormulaGridCell & _cellFormula, ULONG nCounter, ULONG nCounterFormula,  ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			pData; _cellFormula; nCounter; nCounterFormula; _range;
			if( m_bEraseFormulas )
			{
				if( ! m_wndFG.FormulaErase( nColNo, nRowNo, m_bRecomputeFormulas, false ) )
					return false;
			}
			return true;
		}
		virtual bool OnForEach( LPVOID pData, CExtGridCell & _cell, ULONG nCounter, ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			ULONG bBefore = m_nCounterFormula;
			if( ! for_each_base_t::OnForEach( pData, _cell, nCounter, nColNo, nRowNo, _range ) )
				return false;
			if( m_bEraseNonFormulas )
			{
				if( bBefore == m_nCounterFormula )
					_cell.Empty();
			}
			return true;
		}
	}; /// struct for_each_erase_t

	struct __PROF_UIS_API for_each_dup_rect_area_t : public for_each_base_t
	{
		CSize m_sizeShiftWalk;
		bool m_bShiftFormulas:1;
		for_each_dup_rect_area_t( CExtFormulaGridWnd & _wndFG, const CExtGR2D & _range, CSize sizeShiftWalk, bool bShiftFormulas )
			: for_each_base_t( _wndFG, _range )
			, m_sizeShiftWalk( sizeShiftWalk )
			, m_bShiftFormulas( bShiftFormulas )
		{
			m_bWalkFormulasExpl = true;
			__EXT_DEBUG_GRID_ASSERT( m_sizeShiftWalk.cx != 0 || m_sizeShiftWalk.cy != 0 );
		}
		virtual bool OnForEachFormula( LPVOID pData, CExtFormulaGridCell & _cellFormula, ULONG nCounter, ULONG nCounterFormula,  ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			pData; _cellFormula; nCounter; nCounterFormula; _range;
			CExtSafeString strFormulaText;
			if( ! _cellFormula.FormulaTextGet( strFormulaText ) )
				strFormulaText.Empty();
			if( ! m_wndFG.FormulaSet( nColNo + m_sizeShiftWalk.cx, nRowNo + m_sizeShiftWalk.cy, strFormulaText.IsEmpty() ? NULL : LPCTSTR(strFormulaText), true, !m_bShiftFormulas, false ) )
				return false;
			if( m_bShiftFormulas )
			{
				//if( !
					m_wndFG.FormulaShift( nColNo + m_sizeShiftWalk.cx, nRowNo + m_sizeShiftWalk.cy, m_sizeShiftWalk, NULL, true, false )
				//	)
				//	return false
					;
			} // if( m_bShiftFormulas )
			return true;
		}
		virtual bool OnForEach( LPVOID pData, CExtGridCell & _cell, ULONG nCounter, ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			ULONG bBefore = m_nCounterFormula;
			CSize sizeJR( 1, 1 );
			CExtGridCell * pCell = m_wndFG.GridCellGet( nColNo + m_sizeShiftWalk.cx, nRowNo + m_sizeShiftWalk.cy );
			if( pCell != NULL )
			{
				__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
				sizeJR = pCell->JoinGet();
			}
			if( ! for_each_base_t::OnForEach( pData, _cell, nCounter, nColNo, nRowNo, _range ) )
				return false;
			if( bBefore == m_nCounterFormula )
			{
				if( sizeJR.cx >= 1 && sizeJR.cy >= 1 )
				{
					if( ! m_wndFG.GridCellSet( nColNo + m_sizeShiftWalk.cx, nRowNo + m_sizeShiftWalk.cy, &_cell, 1L, 1L, 0, 0, false ) )
						return false;
				}
			}
			pCell = m_wndFG.GridCellGet( nColNo + m_sizeShiftWalk.cx, nRowNo + m_sizeShiftWalk.cy );
			if( pCell != NULL )
			{
				__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
				sizeJR = pCell->JoinSet( sizeJR );
			}
			return true;
		}
	}; /// struct for_each_dup_rect_area_t

	struct __PROF_UIS_API for_each_compute_t : public for_each_base_t
	{
		CExtFormulaDataManager::map_formula_cells_t & m_mapCellsAlreadyComputed;
		for_each_compute_t( CExtFormulaGridWnd & _wndFG, const CExtGR2D & _range, CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed )
			: for_each_base_t( _wndFG, _range )
			, m_mapCellsAlreadyComputed( _mapCellsAlreadyComputed )
		{
			m_bWalkFormulasExpl = true;
		}
		virtual bool OnForEachFormula( LPVOID pData, CExtFormulaGridCell & _cellFormula, ULONG nCounter, ULONG nCounterFormula,  ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			pData; _cellFormula; nCounter; nCounterFormula; _range;
			if( ! m_wndFG._FormulaComputeImpl( nColNo, nRowNo, m_mapCellsAlreadyComputed ) )
			{
				//return false;
			}
			return true;
		}
	}; /// struct for_each_compute_t

	struct __PROF_UIS_API for_each_compute_dc_t : public for_each_base_t
	{
		for_each_compute_dc_t( CExtFormulaGridWnd & _wndFG, const CExtGR2D & _range )
			: for_each_base_t( _wndFG, _range )
		{
			m_bWalkFormulasExpl = false;
		}
		virtual bool OnForEach( LPVOID pData, CExtGridCell & _cell, ULONG nCounter, ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			pData; _cell; nCounter; _range;
			if( ! m_wndFG.FormulaComputeDependentCells( nColNo, nRowNo, false ) )
			{
				//return false;
			}
			return true;
		}
	}; /// struct for_each_compute_dc_t

	struct __PROF_UIS_API for_each_copy_move_range_t : public for_each_base_t
	{
		bool m_bMove:1;
		CSize m_sizeShift;
		for_each_copy_move_range_t( bool bMove, CExtFormulaGridWnd & _wndFG, const CExtGR2D & _range, CSize sizeShift )
			: for_each_base_t( _wndFG, _range )
			, m_bMove( bMove )
			, m_sizeShift( sizeShift )
		{
			m_bWalkFormulasExpl = false;
			__EXT_DEBUG_GRID_ASSERT( m_sizeShift.cx != 0 || m_sizeShift.cy != 0 );
		}
		virtual bool OnForEach( LPVOID pData, CExtGridCell & _cell, ULONG nCounter, ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			pData;
			nCounter;
			_range;
			bool bFormula = false;
			CExtFormulaGridCell * pFC = DYNAMIC_DOWNCAST( CExtFormulaGridCell, (&_cell) );
			if( pFC != NULL )
			{
				CExtSafeString strFormulaText;
				if( pFC->FormulaTextGet( strFormulaText ) )
				{
					if( ! strFormulaText.IsEmpty() )
					{
						bFormula = true;
						if( m_bMove )
						{
							pFC->FormulaStateClear();
							m_FDM._RFE_OnRegisterFormulaCell( nColNo, nRowNo, NULL );
						}
					}
				}
			}
			if( ! m_wndFG.GridCellSet( nColNo + m_sizeShift.cx, nRowNo + m_sizeShift.cy, &_cell, 1L, 1L, 0, 0, false ) )
			{
				__EXT_DEBUG_GRID_ASSERT( FALSE );
				return false;
			}
			if( m_bMove )
			{
				_cell.Empty();
				_cell.TextSet( _T("") ); // clear as simple cell
			}
			if( pFC != NULL )
			{
				if( m_bMove )
					pFC->FormulaTextSet( NULL );
				if( bFormula )
				{
					CExtGridCell * pCell = m_wndFG.GridCellGet( nColNo + m_sizeShift.cx, nRowNo + m_sizeShift.cy, 0, 0, NULL, false, false );
					__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
					pFC = STATIC_DOWNCAST( CExtFormulaGridCell, pCell );
					m_FDM._RFE_OnRegisterFormulaCell( nColNo + m_sizeShift.cx, nRowNo + m_sizeShift.cy, pFC );
				}
			}
			return true;
		}
	}; /// struct for_each_copy_move_range_t

	struct __PROF_UIS_API for_each_shift_range_formulas_t : public for_each_base_t
	{
		CSize m_sizeShift;
		for_each_shift_range_formulas_t( CExtFormulaGridWnd & _wndFG, const CExtGR2D & _range, CSize sizeShift )
			: for_each_base_t( _wndFG, _range )
			, m_sizeShift( sizeShift )
		{
			m_bWalkFormulasExpl = true;
			__EXT_DEBUG_GRID_ASSERT( m_sizeShift.cx != 0 || m_sizeShift.cy != 0 );
		}
		virtual bool OnForEachFormula( LPVOID pData, CExtFormulaGridCell & _cellFormula, ULONG nCounter, ULONG nCounterFormula,  ULONG nColNo, ULONG nRowNo, const CExtGR2D & _range )
		{
			pData; _cellFormula; nCounter; nCounterFormula;
				//if( !
					m_wndFG.FormulaShift( nColNo, nRowNo, m_sizeShift, &_range, false, false )
				//	)
				//	return false
					;
			return true;
		}
	}; /// struct for_each_shift_range_formulas_t

	DECLARE_DYNCREATE( CExtFormulaGridWnd );
	CExtFormulaGridWnd();
	virtual ~CExtFormulaGridWnd();

	virtual DWORD FgGetStyle() const;
	virtual DWORD FgModifyStyle(
		DWORD dwStyleAdd,
		DWORD dwStyleRemove = 0L,
		bool bRedraw = true
		);
	virtual DWORD FgGetStyleEx() const;
	virtual DWORD FgModifyStyleEx(
		DWORD dwStyleExAdd,
		DWORD dwStyleExRemove = 0L,
		bool bRedraw = true
		);

	virtual bool DefaultInit(
		LONG nColCount,
		LONG nRowCount
		);
	virtual bool FormulaSet(
		LONG nColNo,
		LONG nRowNo,
		__EXT_MFC_SAFE_LPCTSTR strFormulaText = NULL,
		bool bErasePrev = true,
		bool bRecompute = true,
		bool bRedraw = true
		);
	virtual bool FormulaShift(
		LONG nColNo,
		LONG nRowNo,
		LONG nShiftX,
		LONG nShiftY,
		const CExtGR2D * pRangeToShift = NULL,
		bool bRecompute = true,
		bool bRedraw = true
		);
	virtual bool FormulaShift(
		LONG nColNo,
		LONG nRowNo,
		const SIZE & sizeShift,
		const CExtGR2D * pRangeToShift = NULL,
		bool bRecompute = true,
		bool bRedraw = true
		)
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( this );
		return FormulaShift( nColNo, nRowNo, sizeShift.cx, sizeShift.cy, pRangeToShift, bRecompute, bRedraw );
	}
	virtual bool FormulaErase(
		LONG nColNo,
		LONG nRowNo,
		bool bRecompute = true,
		bool bRedraw = true
		);
	virtual LONG FormulaErase(
		CExtGR2D * pRange = NULL, // NULL - erase all formulas
		bool bEraseNonFormulas = true,
		bool bRecompute = true,
		bool bRedraw = true
		);
	virtual bool FormulaDuplicateRectArea(
		CRect rcSrc,
		CRect rcDst,
		ULONG nDirection, // AFX_IDW_DOCKBAR_LEFT, AFX_IDW_DOCKBAR_TOP, AFX_IDW_DOCKBAR_RIGHT or AFX_IDW_DOCKBAR_BOTTOM
		bool bShiftFormulas,
		bool bRecompute = true,
		bool bRedraw = true
		);
	virtual bool _FormulaComputeImpl(
		LONG nColNo,
		LONG nRowNo,
		CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed
		);
	virtual bool FormulaCompute(
		LONG nColNo,
		LONG nRowNo,
		bool bRedraw = true
		);
	virtual LONG FormulaCompute(
		CExtGR2D * pRange = NULL, // NULL - compute all formulas
		bool bRedraw = true
		);
	virtual bool FormulaComputeDependentCells(
		LONG nColNo,
		LONG nRowNo,
		bool bRedraw = true
		);
	virtual void FormulaComputeDependentCells(
		CExtGR2D & _range,
		bool bRedraw = true
		);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump( CDumpContext & dc ) const;
#endif // _DEBUG

	//{{AFX_VIRTUAL(CExtFormulaGridWnd)
	public:
	virtual BOOL PreTranslateMessage( MSG * pMsg );
	protected:
	virtual BOOL PreCreateWindow( CREATESTRUCT & cs );
	virtual void PreSubclassWindow();
	virtual void PostNcDestroy();
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam );
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CExtFormulaGridWnd)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	HCURSOR m_hCursorFormulaRectTool, m_hCursorFormulaRectToolPlus, m_hCursorFormulaRangeNWSE, m_hCursorFormulaRangeNESW,
		m_hCursorFormulaRangeMove, m_hCursorFormulaRangeCopy, m_hCursorDefaultInnerFC;
protected:
	CRect m_rcFormulaGridToolRectSelArea, m_rcFormulaGridToolRectHighlightArea, m_rcFormulaGridToolRectDND;
	CExtFormulaDataManager * m_pFormulaDataManager;
public:
	virtual bool OnGridFilterInplaceEditingMessageLoop(
		LONG nVisibleColNo,
		LONG nVisibleRowNo,
		LONG nColNo,
		LONG nRowNo,
		INT nColType,
		INT nRowType,
		const RECT & rcCellExtra,
		const RECT & rcCell,
		const RECT & rcInplaceControl,
		CExtGridCell & _cell,
		HWND hWndInplaceControl,
		MSG & _msg,
		bool & bRemoveMessage
		);
	virtual void OnQueryFormulaToolMetrics(
		CSize & _sizeShiftRB,
		CSize & _sizeLinesActive,
		CSize & _sizeLinesInactive,
		CSize & _sizeTool,
		CSize & _sizeToolInactive,
		CSize & _sizeSingleSelectionAreaBorder,
		CSize & _sizeSingleSelectionAreaResizier
		) const;
	virtual bool OnGridCellFormatFormulaInplaceEdit( CExtFormulaItem * pFI, CExtFormulaInplaceEdit & wndFIE );
	virtual CExtFormulaDataManager & OnGridQueryFormulaDataManager();
	const CExtFormulaDataManager & OnGridQueryFormulaDataManager() const;
	virtual CExtGridDataProvider & OnGridQueryDataProvider();
	virtual bool OnFormulaGridRemoveEmptyFormulaCell(
		CExtGridCell & _cell,
		LONG nColNo,
		LONG nRowNo
		);
	virtual void OnGridCellInputComplete(
		CExtGridCell & _cell,
		LONG nColNo,
		LONG nRowNo,
		INT nColType,
		INT nRowType,
		HWND hWndInputControl = NULL
		);
	virtual bool OnGridPaintCellTextHook(
		bool bPostNotification,
		const CExtGridCell & _cell,
		const RECT & rcCellText,
		CDC & dc,
		LONG nVisibleColNo,
		LONG nVisibleRowNo,
		LONG nColNo,
		LONG nRowNo,
		INT nColType,
		INT nRowType,
		const RECT & rcCellExtra,
		const RECT & rcCell,
		const RECT & rcVisibleRange,
		DWORD dwAreaFlags,
		DWORD dwHelperPaintFlags
		) const;
	virtual bool SelectionGetForCellPainting(
		LONG nColNo,
		LONG nRowNo
		) const;
	virtual void OnGbwPaintCell(
		CDC & dc,
		LONG nVisibleColNo,
		LONG nVisibleRowNo,
		LONG nColNo,
		LONG nRowNo,
		const RECT & rcCellExtra,
		const RECT & rcCell,
		const RECT & rcVisibleRange,
		DWORD dwAreaFlags,
		DWORD dwHelperPaintFlags
		) const;
	virtual bool OnFormulaGridToolRectCreationFilterMessageLoop(
		CRect rcSelArea,
		CRect rcBox,
		CPoint ptClient,
		MSG & _msg,
		bool & bRemoveMessage
		);
	virtual bool OnFormulaGridToolRectDndFilterMessageLoop(
		CRect rcSelArea,
		CRect rcSelTrack,
		const CExtGridHitTestInfo & htInfoDND,
		MSG & _msg,
		bool & bRemoveMessage
		);
	virtual void OnFormulaGridToolRectDndTrack(
		CRect rcSelArea,
		const CExtGridHitTestInfo & htInfoDND
		);
	virtual void OnFormulaGridToolRectTrack(
		CRect rcSelArea,
		CRect rcBox,
		CPoint ptClient
		);
	virtual void OnFormulaGridToolRectExpand(
		CRect rcSelArea,
		CRect rcBox,
		e_FormulaGridToolRectMode_t eFGTRM
		);
	virtual void OnFormulaGridToolRectHighlight(
		CRect rcSelArea,
		CRect rcHighlightArea,
		bool bHighlight
		);
	virtual bool OnGbwAnalyzeCellMouseClickEvent(
		UINT nChar, // VK_LBUTTON, VK_RBUTTON or VK_MBUTTON only
		UINT nRepCnt, // 0 - button up, 1 - single click, 2 - double click, 3 - post single click & begin editing
		UINT nFlags, // mouse event flags
		CPoint point // mouse pointer in client coordinates
		);
	virtual bool OnGbwSetCursor(
		const CExtGridHitTestInfo & htInfo
		);
	virtual bool OnGridFilterQueryFilteredState(
		bool bFilteringRows,
		LONG nRowColNo = -1, // if -1 - ignore nRowColNo, check all ranges at both outer sides and fill p_nRowColNoFound & p_bTopLeftFound
		bool bTopLeft = true,
		LONG * p_nRowColNoFound = NULL,
		bool * p_bTopLeftFound = NULL
		);
	virtual bool ColumnInsert(
		LONG nColNo, // -1 or greater than count - append
		LONG nColInsertCount = 1L,
		bool bRedraw = true
		);
	virtual bool RowInsert(
		LONG nRowNo, // -1 or greater than count - append
		LONG nRowInsertCount = 1L,
		bool bRedraw = true
		);
	virtual LONG ColumnRemove(
		LONG nColNo,
		LONG nColRemoveCount = 1L, // -1 - remove up to end (truncate)
		bool bRedraw = true
		);
	virtual LONG RowRemove(
		LONG nRowNo,
		LONG nRowRemoveCount = 1L, // -1 - remove up to end (truncate)
		bool bRedraw = true
		);
	virtual bool OnGbwAccelCommand(
		DWORD dwEGSA,
		CCmdUI * pCmdUI = NULL,
		bool bRedraw = true
		);
	virtual void OnGridDataDndComplete( bool bCanceled );
	virtual BOOL OnGridDropTargetDrop( CWnd * pWnd, COleDataObject * pDataObject, DROPEFFECT dropEffect, CPoint point );
}; /// class CExtFormulaGridWnd

#endif // (!defined __EXT_MFC_NO_FORMULA_GRID)

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // __EXTFORMULAGRID_H

