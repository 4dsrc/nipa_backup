#if (!defined __EXT_VERSION_EX_H)
#define __EXT_VERSION_EX_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if _MFC_VER < 0xA00  

#ifndef _Inout_
#define _Inout_
#endif

#endif

__PROF_UIS_API BOOL ExtGetVersionExA(_Inout_ LPOSVERSIONINFOA lpVersionInformation);

__PROF_UIS_API BOOL ExtGetVersionExW(_Inout_ LPOSVERSIONINFOW lpVersionInformation);

#ifdef UNICODE
#define ExtGetVersionEx  ExtGetVersionExW
#else
#define ExtGetVersionEx  ExtGetVersionExA
#endif // !UNICODE

#endif // __EXT_VERSION_EX_H
