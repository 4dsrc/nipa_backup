// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_GEO_CONTROLS)
	#if (!defined __EXT_GEO_CONTROLS_H)
		#include <ExtGeoControls.h>
	#endif // (!defined __EXT_GEO_CONTROLS_H)
#endif // (!defined __EXT_MFC_NO_GEO_CONTROLS)

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#if (!defined __EXT_BUTTON_H)
	#include <ExtButton.h>
#endif

#include <Resources/Resource.h>

#if (!defined __EXT_MFC_NO_GEO_CONTROLS)
	#if (!defined __PROF_UIS_RES_GEO_H)
		#include <Resources/ResGeoControls/ResGeoControls.h>
	#endif
#endif

#if (!defined __EXT_MEMORY_DC_H)
	#include "ExtMemoryDC.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#if (!defined __EXT_MFC_NO_GEO_CONTROLS)

void AFXAPI __EXT_MFC_DDX_LongitudeLatitude( CDataExchange * pDX, INT nIDC, CExtLongitude & _Longitude, CExtLatitude & _Latitude )
{
HWND hWndCtrl = pDX->PrepareCtrl( nIDC );
	ASSERT( hWndCtrl != NULL );
	ASSERT( ::IsWindow( hWndCtrl ) );
CExtLLEditWnd * pWnd = DYNAMIC_DOWNCAST( CExtLLEditWnd, CWnd::FromHandle( hWndCtrl ) );
	if( pWnd != NULL )
	{
		ASSERT_VALID( pWnd );
		SYSTEMTIME sys_time;
		::memset( &sys_time, 0, sizeof(SYSTEMTIME) );
		if( pDX->m_bSaveAndValidate )
		{
			pWnd->LongitudeGet( _Longitude );
			pWnd->LatitudeGet( _Latitude );
		}
		else
		{
			pWnd->LongitudeSet( _Longitude, false );
			pWnd->LatitudeSet( _Latitude, false );
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CExtLongitude

CExtLongitude::CExtLongitude(
	e_hemisphere_t eHS, // = CExtLongitude::hemisphere_code_negative
	INT nDegrees, // = 0
	INT nMinutes, // = 0
	INT nSeconds, // = 0
	CExtLL_t lfSecondsFraction, // = 0.0
	bool bNormalize, // = __EXT_LL_DEFAULT_NORMALIZATION__
	bool bNormalizeOverflowEquality // = false
	)
	: CExtLLI < 180, 60, 60 > ( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality )
{
}

CExtLongitude::CExtLongitude(
	CExtLL_t lf,
	bool bNormalize,
	bool bNormalizeOverflowEquality // = false
	)
	: CExtLLI < 180, 60, 60 > ( lf, bNormalize, bNormalizeOverflowEquality )
{
}

CExtLongitude::CExtLongitude(
	const CExtLongitude & other
	)
	: CExtLLI < 180, 60, 60 > ( other )
{
}

CExtLongitude::CExtLongitude(
	const CExtLongitude & other,
	bool bNormalize,
	bool bNormalizeOverflowEquality // = false
	)
	: CExtLLI < 180, 60, 60 > ( other, bNormalize, bNormalizeOverflowEquality )
{
}

/////////////////////////////////////////////////////////////////////////////
// CExtLatitude

CExtLatitude::CExtLatitude(
	e_hemisphere_t eHS, // = CExtLatitude::hemisphere_code_negative
	INT nDegrees, // = 0
	INT nMinutes, // = 0
	INT nSeconds, // = 0
	CExtLL_t lfSecondsFraction, // = 0.0
	bool bNormalize, // = __EXT_LL_DEFAULT_NORMALIZATION__
	bool bNormalizeOverflowEquality // = false
	)
	: CExtLLI < 90, 60, 60 > ( eHS, nDegrees, nMinutes, nSeconds, lfSecondsFraction, bNormalize, bNormalizeOverflowEquality )
{
}

CExtLatitude::CExtLatitude(
	CExtLL_t lf,
	bool bNormalize,
	bool bNormalizeOverflowEquality // = false
	)
	: CExtLLI < 90, 60, 60 > ( lf, bNormalize, bNormalizeOverflowEquality )
{
}

CExtLatitude::CExtLatitude(
	const CExtLatitude & other
	)
	: CExtLLI < 90, 60, 60 > ( other )
{
}

CExtLatitude::CExtLatitude(
	const CExtLatitude & other,
	bool bNormalize,
	bool bNormalizeOverflowEquality // = false
	)
	: CExtLLI < 90, 60, 60 > ( other, bNormalize, bNormalizeOverflowEquality )
{
}

/////////////////////////////////////////////////////////////////////////////
// CExtLLBaseControlProperties

const UINT CExtLLBaseControlProperties::g_nMsgChangingNotification =
	::RegisterWindowMessage(
		_T("CExtLLBaseControlProperties::g_nMsgChangingNotification")
		);

CExtLLBaseControlProperties::CExtLLBaseControlProperties()
	: m_eMode ( CExtLLBaseControlProperties::default_mode )
	, m_Longitude(		CExtLongitude::hemisphere_code_positive,   0, 0, 0, 0.0, true )
	, m_LongitudeMin(	CExtLongitude::hemisphere_code_negative, 180, 0, 0, 0.0, true )
	, m_LongitudeMax(	CExtLongitude::hemisphere_code_positive, 180, 0, 0, 0.0, true )
	, m_Latitude(		CExtLatitude ::hemisphere_code_positive,   0, 0, 0, 0.0, true )
	, m_LatitudeMin(	CExtLatitude ::hemisphere_code_negative,  90, 0, 0, 0.0, true )
	, m_LatitudeMax(	CExtLatitude ::hemisphere_code_positive,  90, 0, 0, 0.0, true )
	, m_tchrNorth( _T('N') )
	, m_tchrSouth( _T('S') )
	, m_tchrEast(  _T('E') )
	, m_tchrWest ( _T('W') )
	, m_tchrSeparator( _T(':') )
	, m_tchrFractionSeparator( _T('.') )
	, m_nFractionDigitCount( 3 )
	, m_strLLS( _T("   ") )
	, m_lParamCookie( 0 )
	, m_bMapChangeSelectionWhenPressed( true )
	, m_bMapChangeSelectionOnLbuttonPressed( false )
	, m_bMapChangeSelectionOnLbuttonReleased( false )
	, m_bMapShowSelectionLines( true )
	, m_bMapShowHoverLines( true )
	, m_bMapDisplayMarkers( false )
	, m_bMapDisplayMarkerTips( false )
	, m_bMapHighlightTimeZones( false )
	, m_bMapDisplayTimeZoneTips( false )
	, m_bMapDisplayCurrentLocationTip( false )
	, m_bMapDisplayLocalDate( false )
	, m_bMapDisplayLocalTime( false )
	, m_nTzHtTransparencySelected( 192 )
	, m_nTzHtTransparencyNormal( 20 )
	, m_bMapHR( false )
	, m_bNoDelayTimeZoneTips( false )
	, m_bNoDelayMarkerTips( false )
	, m_bNoDelayCurrentLocationTips( false )
	, m_pOther( NULL )
{
	ASSERT( m_nFractionDigitCount > 0 );
	m_dtGMT.SetStatus( COleDateTime::invalid );
}

CExtLLBaseControlProperties::~CExtLLBaseControlProperties()
{
}

void CExtLLBaseControlProperties::Serialize( CArchive & ar )
{
DWORD dwReserved = 0L, dwTmp = 0L;
__int64 nTmp64;
	if( ar.IsStoring() )
	{
		ar << dwReserved;
		ar << DWORD(m_eMode);
		ar << DWORD(TCHAR(m_tchrNorth));
		ar << DWORD(TCHAR(m_tchrSouth));
		ar << DWORD(TCHAR(m_tchrEast));
		ar << DWORD(TCHAR(m_tchrWest));
		ar << DWORD(TCHAR(m_tchrSeparator));
		ar << DWORD(TCHAR(m_tchrFractionSeparator));
		ar << DWORD(m_nFractionDigitCount);
		ar << m_strLLS;
		nTmp64 = m_lParamCookie;
		ar.Write( &nTmp64, sizeof(__int64) );
		ar << m_dtGMT;
		dwTmp = DWORD(m_nTzHtTransparencySelected) | (DWORD(m_nTzHtTransparencyNormal)<<8);
		dwTmp |= m_bMapChangeSelectionWhenPressed ? (DWORD(1L)<<16) : DWORD(0L);
		dwTmp |= m_bMapChangeSelectionOnLbuttonPressed ? (DWORD(1L)<<17) : DWORD(0L);
		dwTmp |= m_bMapChangeSelectionOnLbuttonReleased ? (DWORD(1L)<<18) : DWORD(0L);
		dwTmp |= m_bMapShowSelectionLines ? (DWORD(1L)<<19) : DWORD(0L);
		dwTmp |= m_bMapShowHoverLines ? (DWORD(1L)<<20) : DWORD(0L);
		dwTmp |= m_bMapDisplayMarkers ? (DWORD(1L)<<21) : DWORD(0L);
		dwTmp |= m_bMapDisplayMarkerTips ? (DWORD(1L)<<22) : DWORD(0L);
		dwTmp |= m_bMapHighlightTimeZones ? (DWORD(1L)<<23) : DWORD(0L);
		dwTmp |= m_bMapDisplayTimeZoneTips ? (DWORD(1L)<<24) : DWORD(0L);
		dwTmp |= m_bMapDisplayCurrentLocationTip ? (DWORD(1L)<<25) : DWORD(0L);
		dwTmp |= m_bMapDisplayLocalDate ? (DWORD(1L)<<26) : DWORD(0L);
		dwTmp |= m_bMapDisplayLocalTime ? (DWORD(1L)<<27) : DWORD(0L);
		dwTmp |= m_bMapHR ? (DWORD(1L)<<28) : DWORD(0L);
		dwTmp |= m_bNoDelayTimeZoneTips ? (DWORD(1L)<<29) : DWORD(0L);
		dwTmp |= m_bNoDelayMarkerTips ? (DWORD(1L)<<30) : DWORD(0L);
		dwTmp |= m_bNoDelayCurrentLocationTips ? (DWORD(1L)<<31) : DWORD(0L);
		ar << dwTmp;
	} // if( ar.IsStoring() )
	else
	{
		ar >> dwReserved;
		ar >> dwTmp;
		m_eMode = (eMode_t)dwTmp;
		ar >> dwTmp;
		m_tchrNorth = TCHAR(dwTmp);
		ar >> dwTmp;
		m_tchrSouth = TCHAR(dwTmp);
		ar >> dwTmp;
		m_tchrEast = TCHAR(dwTmp);
		ar >> dwTmp;
		m_tchrWest = TCHAR(dwTmp);
		ar >> dwTmp;
		m_tchrSeparator = TCHAR(dwTmp);
		ar >> dwTmp;
		m_tchrFractionSeparator = TCHAR(dwTmp);
		ar >> dwTmp;
		m_nFractionDigitCount = INT(dwTmp);
		ar >> m_strLLS;
		ar.Read( &nTmp64, sizeof(__int64) );
		m_lParamCookie = LPARAM(nTmp64);
		ar >> m_dtGMT;
		ar >> dwTmp;
		m_nTzHtTransparencySelected = BYTE(dwTmp&0x0FF);
		m_nTzHtTransparencyNormal = BYTE((dwTmp>>8)&0x0FF);
		m_bMapChangeSelectionWhenPressed = ( ( dwTmp & ( DWORD(1L) << 16 ) ) != 0 ) ? true : false;
		m_bMapChangeSelectionOnLbuttonPressed = ( ( dwTmp & ( DWORD(1L) << 17 ) ) != 0 ) ? true : false;
		m_bMapChangeSelectionOnLbuttonReleased = ( ( dwTmp & ( DWORD(1L) << 18 ) ) != 0 ) ? true : false;
		m_bMapShowSelectionLines = ( ( dwTmp & ( DWORD(1L) << 19 ) ) != 0 ) ? true : false;
		m_bMapShowHoverLines = ( ( dwTmp & ( DWORD(1L) << 20 ) ) != 0 ) ? true : false;
		m_bMapDisplayMarkers = ( ( dwTmp & ( DWORD(1L) << 21 ) ) != 0 ) ? true : false;
		m_bMapDisplayMarkerTips = ( ( dwTmp & ( DWORD(1L) << 22 ) ) != 0 ) ? true : false;
		m_bMapHighlightTimeZones = ( ( dwTmp & ( DWORD(1L) << 23 ) ) != 0 ) ? true : false;
		m_bMapDisplayTimeZoneTips = ( ( dwTmp & ( DWORD(1L) << 24 ) ) != 0 ) ? true : false;
		m_bMapDisplayCurrentLocationTip = ( ( dwTmp & ( DWORD(1L) << 25 ) ) != 0 ) ? true : false;
		m_bMapDisplayLocalDate = ( ( dwTmp & ( DWORD(1L) << 26 ) ) != 0 ) ? true : false;
		m_bMapDisplayLocalTime = ( ( dwTmp & ( DWORD(1L) << 27 ) ) != 0 ) ? true : false;
		m_bMapHR = ( ( dwTmp & ( DWORD(1L) << 28 ) ) != 0 ) ? true : false;
		m_bNoDelayTimeZoneTips = ( ( dwTmp & ( DWORD(1L) << 29 ) ) != 0 ) ? true : false;
		m_bNoDelayMarkerTips = ( ( dwTmp & ( DWORD(1L) << 30 ) ) != 0 ) ? true : false;
		m_bNoDelayCurrentLocationTips = ( ( dwTmp & ( DWORD(1L) << 31 ) ) != 0 ) ? true : false;
	} // else from if( ar.IsStoring() )
	m_Longitude.Serialize( ar );
	m_LongitudeMin.Serialize( ar );
	m_LongitudeMax.Serialize( ar );
	m_Latitude.Serialize( ar );
	m_LatitudeMin.Serialize( ar );
	m_LatitudeMax.Serialize( ar );
}

void CExtLLBaseControlProperties::AssignProperties(
	const CExtLLBaseControlProperties & other
	)
{
	m_eMode = other.m_eMode;
	m_Longitude = other.m_Longitude;
	m_LongitudeMin = other.m_LongitudeMin;
	m_LongitudeMax = other.m_LongitudeMax;
	m_Latitude = other.m_Latitude;
	m_LatitudeMin = other.m_LatitudeMin;
	m_LatitudeMax = other.m_LatitudeMax;
	m_tchrNorth = other.m_tchrNorth;
	m_tchrSouth = other.m_tchrSouth;
	m_tchrEast = other.m_tchrEast;
	m_tchrWest = other.m_tchrWest;
	m_tchrSeparator = other.m_tchrSeparator;
	m_tchrFractionSeparator = other.m_tchrFractionSeparator;
	m_nFractionDigitCount = other.m_nFractionDigitCount;
	m_strLLS = other.m_strLLS;
	m_lParamCookie = other.m_lParamCookie;
	m_dtGMT = other.m_dtGMT;
	m_dtGMT.SetStatus( other.m_dtGMT.GetStatus() );
	m_bMapChangeSelectionWhenPressed = other.m_bMapChangeSelectionWhenPressed;
	m_bMapChangeSelectionOnLbuttonPressed = other.m_bMapChangeSelectionOnLbuttonPressed;
	m_bMapChangeSelectionOnLbuttonReleased = other.m_bMapChangeSelectionOnLbuttonReleased;
	m_bMapShowSelectionLines = other.m_bMapShowSelectionLines;
	m_bMapShowHoverLines = other.m_bMapShowHoverLines;
	m_bMapDisplayMarkers = other.m_bMapDisplayMarkers;
	m_bMapDisplayMarkerTips = other.m_bMapDisplayMarkerTips;
	m_bMapHighlightTimeZones = other.m_bMapHighlightTimeZones;
	m_bMapDisplayTimeZoneTips = other.m_bMapDisplayTimeZoneTips;
	m_bMapDisplayCurrentLocationTip = other.m_bMapDisplayCurrentLocationTip;
	m_bMapDisplayLocalDate = other.m_bMapDisplayLocalDate;
	m_bMapDisplayLocalTime = other.m_bMapDisplayLocalTime;
	m_nTzHtTransparencySelected = other.m_nTzHtTransparencySelected;
	m_nTzHtTransparencyNormal = other.m_nTzHtTransparencyNormal;
	m_bMapHR = other.m_bMapHR;
	m_bNoDelayTimeZoneTips = other.m_bNoDelayTimeZoneTips;
	m_bNoDelayMarkerTips = other.m_bNoDelayMarkerTips;
	m_bNoDelayCurrentLocationTips = other.m_bNoDelayCurrentLocationTips;
}

void CExtLLBaseControlProperties::SetMode( CExtLLBaseControlProperties::eMode_t eMode )
{
	m_eMode = eMode;
}

CExtLLBaseControlProperties::eMode_t CExtLLBaseControlProperties::GetMode() const
{
	return m_eMode;
}

void CExtLLBaseControlProperties::RangeLongitudeGet(
	CExtLongitude & _LongitudeMin,
	CExtLongitude & _LongitudeMax
	) const
{
	_LongitudeMin = m_LongitudeMin;
	_LongitudeMax = m_LongitudeMax;
}

void CExtLLBaseControlProperties::RangeLongitudeSet(
	const CExtLongitude & _LongitudeMin,
	const CExtLongitude & _LongitudeMax
	)
{
	m_LongitudeMin = _LongitudeMin;
	m_LongitudeMax = _LongitudeMax;
}

void CExtLLBaseControlProperties::RangeLatitudeGet(
	CExtLatitude & _LatitudeMin,
	CExtLatitude & _LatitudeMax
	) const
{
	_LatitudeMin = m_LatitudeMin;
	_LatitudeMax = m_LatitudeMax;
}

void CExtLLBaseControlProperties::RangeLatitudeSet(
	const CExtLatitude & _LatitudeMin,
	const CExtLatitude & _LatitudeMax
	)
{
	m_LatitudeMin = _LatitudeMin;
	m_LatitudeMax = _LatitudeMax;
}

void CExtLLBaseControlProperties::LongitudeGet(
	CExtLongitude & _Longitude
	) const
{
	_Longitude = m_Longitude;
}

void CExtLLBaseControlProperties::LongitudeSet(
	const CExtLongitude & _Longitude,
	bool bUpdateNow
	)
{
	bUpdateNow;
CExtLongitude _oldLongitude;
	LongitudeGet( _oldLongitude );
CExtLatitude _oldLatitude;
	LatitudeGet( _oldLatitude );
	if( ! OnValueChanging(
			_oldLongitude,
			_Longitude,
			_oldLatitude,
			_oldLatitude,
			this
			)
		)
		return;
	m_Longitude = _Longitude;
	OnValueChanged(
		_oldLongitude,
		m_Longitude,
		_oldLatitude,
		_oldLatitude,
		this
		);
}

void CExtLLBaseControlProperties::LatitudeGet(
	CExtLatitude & _Latitude
	) const
{
	_Latitude = m_Latitude;
}

void CExtLLBaseControlProperties::LatitudeSet(
	const CExtLatitude & _Latitude,
	bool bUpdateNow
	)
{
	bUpdateNow;
CExtLatitude _oldLatitude;
	LatitudeGet( _oldLatitude );
CExtLongitude _oldLongitude;
	LongitudeGet( _oldLongitude );
	if( ! OnValueChanging(
			_oldLongitude,
			_oldLongitude,
			_oldLatitude,
			_Latitude,
			this
			)
		)
		return;
	m_Latitude = _Latitude;
	OnValueChanged(
		_oldLongitude,
		_oldLongitude,
		_oldLatitude,
		m_Latitude,
		this
		);
}

void CExtLLBaseControlProperties::OnQueryRangeLongitude(
	CExtLongitude & _LongitudeMin,
	CExtLongitude & _LongitudeMax
	) const
{
	RangeLongitudeGet(
		_LongitudeMin,
		_LongitudeMax
		);
}

void CExtLLBaseControlProperties::OnQueryRangeLatitude(
	CExtLatitude & _LatitudeMin,
	CExtLatitude & _LatitudeMax
	) const
{
	RangeLatitudeGet(
		_LatitudeMin,
		_LatitudeMax
		);
}

bool CExtLLBaseControlProperties::stat_ParseTextLongitude(
	__EXT_MFC_SAFE_LPCTSTR & _str,
	__EXT_MFC_SAFE_TCHAR tchrEast,
	__EXT_MFC_SAFE_TCHAR tchrWest,
	__EXT_MFC_SAFE_TCHAR tchrSeparator,
	__EXT_MFC_SAFE_TCHAR tchrFractionSeparator,
	INT nFractionDigitCount,
	CExtLongitude * pLongitude // = NULL
	)
{
CString strLongHemisphere;			// 1
INT		nLongDegrees = 0;			// 2
INT		nLongMinutes = 0;			// 3
INT		nLongSeconds = 0;			// 4
INT		nLongSecondsFraction = 0;	// 5
CExtLongitude::e_hemisphere_t _eHSLong = CExtLongitude::hemisphere_code_positive;
INT		nNumberValue = 1;
CString	strCurrentValue = _T("");
INT		nNumberSpace = 0;
LPCTSTR strWalkTmp = LPCTSTR(_str);
LPCTSTR & strWalk = strWalkTmp;
INT		nLenText = INT( _tcslen( strWalk ) );
	for( INT i = 0; i <= nLenText; i++, strWalk ++ )
	{
		__EXT_MFC_SAFE_TCHAR _tchrSymbol = _T(' ');
		if( i == nLenText )
			_tchrSymbol = tchrSeparator; //_T(':')
		else
			_tchrSymbol = TCHAR(*strWalk);

		if( _tchrSymbol == _T(' ') || _tchrSymbol == _T('\t') || _tchrSymbol == _T('\r') || _tchrSymbol == _T('\n') )
		{
			if( nNumberSpace == 0 && nNumberValue == 1 && strCurrentValue.IsEmpty() )
				continue;
			nNumberSpace ++;
		}
		else
			nNumberSpace = 0;
		if(		(	( _tchrSymbol == tchrSeparator || _tchrSymbol == tchrFractionSeparator )
				&&	nNumberSpace == 0
				&&	(! strCurrentValue.IsEmpty() )
				)
			||	(	nNumberSpace == 1
				&&	(! strCurrentValue.IsEmpty() )
				)
			)
		{
			switch( nNumberValue )
			{
			case 1:
				strLongHemisphere = strCurrentValue;
			break;
			case 2:
				nLongDegrees = INT( _ttoi64( strCurrentValue ) );
			break;
			case 3:
				nLongMinutes = INT( _ttoi64( strCurrentValue ) );
			break;
			case 4:
				nLongSeconds = INT( _ttoi64( strCurrentValue ) );
			break;
			case 5:
				nLongSecondsFraction = INT( _ttoi64( strCurrentValue ) );
			break;
			default:
				_str = strWalk;
				return false;
			} // switch( nNumberValue )
			strCurrentValue = _T("");
			nNumberValue ++;
			continue;
		}
		if( nNumberValue == 6 )
			break;
		if(	! ( _tchrSymbol == tchrSeparator || 
				_tchrSymbol == tchrFractionSeparator || 
				_tchrSymbol == _T(' ') || 
				_tchrSymbol == _T('\t') || 
				_tchrSymbol == _T('\r') || 
				_tchrSymbol == _T('\n') 
				) 
			)
			strCurrentValue += _tchrSymbol;
	}
	_str = strWalk;
	if( strLongHemisphere == tchrEast )
		_eHSLong = CExtLongitude::hemisphere_east;
	else if( strLongHemisphere == tchrWest )
		_eHSLong = CExtLongitude::hemisphere_west;
	else
		return false;
CExtLL_t lfSecondsFraction = (CExtLL_t)nLongSecondsFraction;
	lfSecondsFraction /= (CExtLL_t) ::powl( 10, nFractionDigitCount );
	pLongitude->Set(
		_eHSLong,
		nLongDegrees,
		nLongMinutes,
		nLongSeconds,
		lfSecondsFraction
		);
	return true;
}

bool CExtLLBaseControlProperties::stat_ParseTextLatitude( 
	__EXT_MFC_SAFE_LPCTSTR & _str,
	__EXT_MFC_SAFE_TCHAR tchrNorth,
	__EXT_MFC_SAFE_TCHAR tchrSouth,
	__EXT_MFC_SAFE_TCHAR tchrSeparator,
	__EXT_MFC_SAFE_TCHAR tchrFractionSeparator,
	INT nFractionDigitCount,
	CExtLatitude * pLatitude // = NULL
	)
{
CString strLatHemisphere;			// 6
INT		nLatDegrees = 0;			// 7
INT		nLatMinutes = 0;			// 8
INT		nLatSeconds = 0;			// 9
INT		nLatSecondsFraction = 0;	// 10
CExtLatitude::e_hemisphere_t _eHSLat = CExtLatitude::hemisphere_code_positive;
INT		nNumberValue = 6;
CString	strCurrentValue = _T("");
INT		nNumberSpace = 0;
LPCTSTR strWalkTmp = LPCTSTR(_str);
LPCTSTR & strWalk = strWalkTmp;
INT		nLenText = INT( _tcslen( strWalk ) );
	for( INT i = 0; i <= nLenText; i++, strWalk ++ )
	{
		__EXT_MFC_SAFE_TCHAR _tchrSymbol = _T(' ');
		if( i == nLenText )
			_tchrSymbol = tchrSeparator; // _T(':')
		else
			_tchrSymbol = TCHAR(*strWalk);
		
		if( _tchrSymbol == _T(' ') || _tchrSymbol == _T('\t') || _tchrSymbol == _T('\r') || _tchrSymbol == _T('\n') )
		{
			if( nNumberSpace == 0 && nNumberValue == 6 && strCurrentValue.IsEmpty() )
				continue;
			nNumberSpace ++;
		}
		else
			nNumberSpace = 0;
		if(		(	( _tchrSymbol == tchrSeparator || _tchrSymbol == tchrFractionSeparator )
				&&	nNumberSpace == 0
				&&	(! strCurrentValue.IsEmpty() )
				)
			||	(	nNumberSpace == 1
				&&	(! strCurrentValue.IsEmpty() )
				)
			)
		{
			switch( nNumberValue )
			{
			case 6:
				strLatHemisphere = strCurrentValue;
			break;
			case 7:
				nLatDegrees = INT( _ttoi64( strCurrentValue ) );
			break;
			case 8:
				nLatMinutes = INT( _ttoi64( strCurrentValue ) );
			break;
			case 9:
				nLatSeconds = INT( _ttoi64( strCurrentValue ) );
			break;
			case 10:
				nLatSecondsFraction = INT( _ttoi64( strCurrentValue ) );
			break;
			default:
				_str = strWalk;
				return false;
			} // switch( nNumberValue )
			strCurrentValue = _T("");
			nNumberValue ++;
			continue;
		}
		if( nNumberValue == 11 )
			break;
		if(	! ( _tchrSymbol == tchrSeparator || 
				_tchrSymbol == tchrFractionSeparator || 
				_tchrSymbol == _T(' ') || 
				_tchrSymbol == _T('\t') || 
				_tchrSymbol == _T('\r') || 
				_tchrSymbol == _T('\n') 
				) 
			)
			strCurrentValue += _tchrSymbol;
	}
	_str = strWalk;
	if( strLatHemisphere == tchrNorth )
		_eHSLat = CExtLatitude::hemisphere_north;
	else if( strLatHemisphere == tchrSouth )
			_eHSLat = CExtLatitude::hemisphere_south;
	else
		return false;
CExtLL_t lfSecondsFraction = (CExtLL_t)nLatSecondsFraction;
	lfSecondsFraction /= (CExtLL_t) ::powl( 10, nFractionDigitCount );
	pLatitude->Set(
		_eHSLat,
		nLatDegrees,
		nLatMinutes,
		nLatSeconds,
		lfSecondsFraction
		);
	return true;
}

bool CExtLLBaseControlProperties::stat_ParseText( 
	CExtLLBaseControlProperties::eMode_t eMode,
	__EXT_MFC_SAFE_LPCTSTR & _str,
	__EXT_MFC_SAFE_TCHAR tchrNorth,
	__EXT_MFC_SAFE_TCHAR tchrSouth,
	__EXT_MFC_SAFE_TCHAR tchrEast,
	__EXT_MFC_SAFE_TCHAR tchrWest,
	__EXT_MFC_SAFE_TCHAR tchrSeparator,
	__EXT_MFC_SAFE_TCHAR tchrFractionSeparator,
	INT nFractionDigitCount,
	CExtLongitude * pLongitude, //  = NULL
	CExtLatitude * pLatitude //  = NULL
	)
{
	ASSERT( nFractionDigitCount > 0 );
	switch( eMode )
	{
	case longitude_latitude:
		if(		stat_ParseTextLongitude( _str, tchrEast, tchrWest, tchrSeparator, tchrFractionSeparator, nFractionDigitCount, pLongitude )
			&&	stat_ParseTextLatitude( _str, tchrNorth, tchrSouth, tchrSeparator, tchrFractionSeparator, nFractionDigitCount, pLatitude )
			)
			return true;
		else
			return false;
	case latitude_longitude:
		if(		stat_ParseTextLatitude( _str, tchrNorth, tchrSouth, tchrSeparator, tchrFractionSeparator, nFractionDigitCount, pLatitude )
			&&	stat_ParseTextLongitude( _str, tchrEast, tchrWest, tchrSeparator, tchrFractionSeparator, nFractionDigitCount, pLongitude )
			)
			return true;
		else
			return false;
	case longitude:
		return stat_ParseTextLongitude( _str, tchrEast, tchrWest, tchrSeparator, tchrFractionSeparator, nFractionDigitCount, pLongitude );
	case latitude:
		return stat_ParseTextLatitude( _str, tchrNorth, tchrSouth, tchrSeparator, tchrFractionSeparator, nFractionDigitCount, pLatitude );
	default:
		return false;
	}
}

bool CExtLLBaseControlProperties::OnValueChanging(
	const CExtLongitude & _LongitudeOld,
	const CExtLongitude & _LongitudeNew,
	const CExtLatitude & _LatitudeOld,
	const CExtLatitude & _LatitudeNew,
	CExtLLBaseControlProperties * pSrc
	)
{
	if(		_LongitudeOld == _LongitudeNew
		&&	_LatitudeOld == _LatitudeNew
		)
		return false;
	if( m_pOther != NULL )
	{
		ASSERT( LPVOID(m_pOther) != LPVOID(this) );
		if( ! m_pOther->OnValueChanging( _LongitudeOld, _LongitudeNew, _LatitudeOld, _LatitudeNew, pSrc ) )
			return false;
	}
HWND hWndNotificationReceiver = OnQueryNotificationReceiver();
	if( hWndNotificationReceiver != NULL )
	{
		CExtLLBaseControlProperties::CHANGING_NOTIFICATION _SN( 
			false,
			*this,
			_LongitudeOld,
			_LongitudeNew,
			_LatitudeOld,
			_LatitudeNew,
			m_lParamCookie
			);
		LRESULT lResult = _SN.Notify( hWndNotificationReceiver );
		if( lResult < 0 )
			return false;
	} // if( hWndNotificationReceiver != NULL )
CExtLongitude _LongitudeMin, _LongitudeMax;
	OnQueryRangeLongitude( _LongitudeMin, _LongitudeMax );
	if( ! ( _LongitudeMin <= _LongitudeNew && _LongitudeNew <= _LongitudeMax ) )
		return false;
CExtLatitude _LatitudeMin, _LatitudeMax;
	OnQueryRangeLatitude( _LatitudeMin, _LatitudeMax );
	if( ! ( _LatitudeMin <= _LatitudeNew && _LatitudeNew <= _LatitudeMax ) )
		return false;
	return true;
}

bool CExtLLBaseControlProperties::OnValueChanged(
	const CExtLongitude & _LongitudeOld,
	const CExtLongitude & _LongitudeNew,
	const CExtLatitude & _LatitudeOld,
	const CExtLatitude & _LatitudeNew,
	CExtLLBaseControlProperties * pSrc
	)
{
	if(		_LongitudeOld == _LongitudeNew
		&&	_LatitudeOld == _LatitudeNew
		)
		return false;
	if( m_pOther != NULL )
	{
		ASSERT( LPVOID(m_pOther) != LPVOID(this) );
		m_pOther->OnValueChanged( _LongitudeOld, _LongitudeNew, _LatitudeOld, _LatitudeNew, pSrc );
	}
HWND hWndNotificationReceiver = OnQueryNotificationReceiver();
	if( hWndNotificationReceiver != NULL )
	{
		CExtLLBaseControlProperties::CHANGING_NOTIFICATION _SN( 
			true,
			*this,
			_LongitudeOld,
			_LongitudeNew,
			_LatitudeOld,
			_LatitudeNew,
			m_lParamCookie
			);
		LRESULT lResult = _SN.Notify( hWndNotificationReceiver );
		if( lResult < 0 )
			return false;
	} // if( hWndNotificationReceiver != NULL )
	return true;
}

CExtSafeString CExtLLBaseControlProperties::stat_TextGet(
	const CExtLongitude & _Longitude,
	__EXT_MFC_SAFE_TCHAR tchrEast,
	__EXT_MFC_SAFE_TCHAR tchrWest,
	__EXT_MFC_SAFE_TCHAR tchrSeparator,
	__EXT_MFC_SAFE_TCHAR tchrFractionSeparator,
	INT nFractionDigitCount
	)
{
CExtSafeString strRetVal = _T("");
TCHAR buffer[128+1];
	strRetVal += ( _Longitude.HemisphereGet() == CExtLongitude::hemisphere_east ) ? TCHAR(tchrEast): TCHAR(tchrWest);
	strRetVal += TCHAR(tchrSeparator);
//	__EXT_MFC_ITOA( _Longitude.DegreesGet(), buffer, 128, 10 );
	__EXT_MFC_SPRINTF(
		__EXT_MFC_SPRINTF_PARAM( buffer, 128 ),
		_T("%03d"),
		INT( _Longitude.DegreesGet())
		);
	strRetVal += buffer;
	strRetVal += TCHAR(tchrSeparator);
//	__EXT_MFC_ITOA( _Longitude.MinutesGet(), buffer, 128, 10 );
	__EXT_MFC_SPRINTF(
		__EXT_MFC_SPRINTF_PARAM( buffer, 128 ),
		_T("%02d"),
		INT( _Longitude.MinutesGet())
		);
	strRetVal += buffer;
	strRetVal += TCHAR(tchrSeparator);
//	__EXT_MFC_ITOA( _Longitude.SecondsGet(), buffer, 128, 10 );
	__EXT_MFC_SPRINTF(
		__EXT_MFC_SPRINTF_PARAM( buffer, 128 ),
		_T("%02d"),
		INT( _Longitude.SecondsGet())
		);
	strRetVal += buffer;
	strRetVal += TCHAR(tchrFractionSeparator);
//	__EXT_MFC_ITOA( INT( _Longitude.SecondsFractionGet() * ::powl( 10, nFractionDigitCount ) ), buffer, 128, 10 );
	__EXT_MFC_SPRINTF(
		__EXT_MFC_SPRINTF_PARAM( buffer, 128 ),
		_T("%0*d"),
		INT( nFractionDigitCount ),
		INT( _Longitude.SecondsFractionGet() * ::powl( 10, nFractionDigitCount ) )
		);
	strRetVal += buffer;
	return strRetVal;
}

CExtSafeString CExtLLBaseControlProperties::stat_TextGet(
	const CExtLatitude & _Latitude,
	__EXT_MFC_SAFE_TCHAR tchrNorth,
	__EXT_MFC_SAFE_TCHAR tchrSouth,
	__EXT_MFC_SAFE_TCHAR tchrSeparator,
	__EXT_MFC_SAFE_TCHAR tchrFractionSeparator,
	INT nFractionDigitCount
	)
{
CExtSafeString strRetVal = _T("");
TCHAR buffer[128+1];
	strRetVal += ( _Latitude.HemisphereGet() == CExtLatitude::hemisphere_north ) ? TCHAR(tchrNorth): TCHAR(tchrSouth);
	strRetVal += TCHAR(tchrSeparator);
//	__EXT_MFC_ITOA( _Latitude.DegreesGet(), buffer, 128, 10 );
	__EXT_MFC_SPRINTF(
		__EXT_MFC_SPRINTF_PARAM( buffer, 128 ),
		_T("%02d"),
		INT( _Latitude.DegreesGet())
		);
	strRetVal += buffer;
	strRetVal += TCHAR(tchrSeparator);
//	__EXT_MFC_ITOA( _Latitude.MinutesGet(), buffer, 128, 10 );
	__EXT_MFC_SPRINTF(
		__EXT_MFC_SPRINTF_PARAM( buffer, 128 ),
		_T("%02d"),
		INT( _Latitude.MinutesGet())
		);
	strRetVal += buffer;
	strRetVal += TCHAR(tchrSeparator);
//	__EXT_MFC_ITOA( _Latitude.SecondsGet(), buffer, 128, 10 );
	__EXT_MFC_SPRINTF(
		__EXT_MFC_SPRINTF_PARAM( buffer, 128 ),
		_T("%02d"),
		INT( _Latitude.SecondsGet())
		);
	strRetVal += buffer;
	strRetVal += TCHAR(tchrFractionSeparator);
//	__EXT_MFC_ITOA( INT( _Latitude.SecondsFractionGet() * ::powl( 10, nFractionDigitCount ) ), buffer, 128, 10 );
	__EXT_MFC_SPRINTF(
		__EXT_MFC_SPRINTF_PARAM( buffer, 128 ),
		_T("%0*d"),
		INT( nFractionDigitCount ),
		INT( _Latitude.SecondsFractionGet() * ::powl( 10, nFractionDigitCount ) )
		);
	strRetVal += buffer;
	return strRetVal;
}

CExtSafeString CExtLLBaseControlProperties::stat_TextGet(
	const CExtLongitude & _Longitude,
	const CExtLatitude & _Latitude,
	CExtLLBaseControlProperties::eMode_t eMode,
	__EXT_MFC_SAFE_TCHAR tchrNorth,
	__EXT_MFC_SAFE_TCHAR tchrSouth,
	__EXT_MFC_SAFE_TCHAR tchrEast,
	__EXT_MFC_SAFE_TCHAR tchrWest,
	__EXT_MFC_SAFE_TCHAR tchrSeparator,
	__EXT_MFC_SAFE_TCHAR tchrFractionSeparator,
	INT nFractionDigitCount,
	__EXT_MFC_SAFE_LPCTSTR strLLS
	)
{
CExtSafeString strRetVal = _T("");
CExtSafeString strLongitude =
		stat_TextGet(
			_Longitude,
			tchrEast,
			tchrWest,
			tchrSeparator,
			tchrFractionSeparator,
			nFractionDigitCount
			);
CExtSafeString strLatitude =
		stat_TextGet(
			_Latitude,
			tchrNorth,
			tchrSouth,
			tchrSeparator,
			tchrFractionSeparator,
			nFractionDigitCount
			);
	switch( eMode )
	{
	case longitude_latitude:
		strRetVal  = strLongitude;
		strRetVal += ( strLLS != NULL ) ? strLLS : _T("");
		strRetVal += strLatitude;
	break;
	case latitude_longitude:
		strRetVal  = strLatitude;
		strRetVal += ( strLLS != NULL ) ? strLLS : _T("");
		strRetVal += strLongitude;
	break;
	case longitude:
		strRetVal = strLongitude;
	break;
	case latitude:
		strRetVal = strLatitude;
	break;
	}
	return strRetVal;
}

CExtSafeString CExtLLBaseControlProperties::TextGet() const
{
	ASSERT( m_nFractionDigitCount > 0 );
CExtLongitude _Longitude;
	LongitudeGet( _Longitude );
CExtLatitude _Latitude;
	LatitudeGet( _Latitude );
	return
		stat_TextGet(
			_Longitude,
			_Latitude,
			GetMode(),
			m_tchrNorth,
			m_tchrSouth,
			m_tchrEast,
			m_tchrWest,
			m_tchrSeparator,
			m_tchrFractionSeparator,
			m_nFractionDigitCount,
			LPCTSTR(m_strLLS)
			);
}

bool CExtLLBaseControlProperties::TextParse(
	__EXT_MFC_SAFE_LPCTSTR strText,
	CExtLongitude * pLongitude, // = NULL
	CExtLatitude * pLatitude // = NULL
	) const
{
	ASSERT( m_nFractionDigitCount > 0 );
	return
		stat_ParseText(
			GetMode(),
			strText,
			m_tchrNorth,
			m_tchrSouth,
			m_tchrEast,
			m_tchrWest,
			m_tchrSeparator,
			m_tchrFractionSeparator,
			m_nFractionDigitCount,
			pLongitude,
			pLatitude
			);
}

bool CExtLLBaseControlProperties::TextSet(
	__EXT_MFC_SAFE_LPCTSTR strText,
	bool bUpdateNow,
	bool bParsePartialText
	)
{
	bUpdateNow;
	bParsePartialText;
CExtLongitude _Longitude;
CExtLatitude _Latitude;
	if(	! TextParse( strText, & _Longitude, & _Latitude ) )
		return false;
	LongitudeSet( _Longitude, false );
	LatitudeSet( _Latitude, false );
	return true;
}

CArray < CExtLLBaseControlProperties::MAP_MARKER, CExtLLBaseControlProperties::MAP_MARKER & > & CExtLLBaseControlProperties::OnQueryMapMarkers()
{
	if( ! m_bMapDisplayMarkers )
	{
		m_arrMapMarkers.RemoveAll();
	}
	else if( INT( m_arrMapMarkers.GetSize() ) == 0 )
	{
		static struct  
		{
			CExtLL_t m_lfLatitude, m_lfLongitude;
			LPCSTR m_strCityName, m_strCityCountry;
			COLORREF m_clr;
		} g_arrCities[] =
		{
			{ 61.17, -150.00,"Anchorage", "Alaska, USA", COLORREF(-1L) },
			{ 38.00, 23.73,"Athens", "Greece", COLORREF(-1L) },
			{ 33.4, 44.4,"Baghdad", "Iraq", COLORREF(-1L) },
			{ 13.73, 100.50,"Bangkok", "Thailand", COLORREF(-1L) },
			{ 39.92, 116.43,"Beijing", "China", COLORREF(-1L) },
			{ 52.53, 13.42,"Berlin", "Germany", COLORREF(-1L) },
			{ 32.3, -64.7,"Bermuda", "Bermuda", COLORREF(-1L) },
			{ 42.33, -71.08,"Boston", "Massachusetts, USA", COLORREF(-1L) },
			{ -15.8, -47.9,"Brasilia", "Brazil", COLORREF(-1L) },
			{ -4.2, 15.3,"Brazzaville", "Congo", COLORREF(-1L) },
			{ -34.67, -58.50,"Buenos Aires", "Argentina", COLORREF(-1L) },
			{ 31.05, 31.25,"Cairo", "Egypt", COLORREF(-1L) },
			{ 22.5, 88.3,"Calcutta", "India", COLORREF(-1L) },
			{ -33.93, 18.47,"Cape Town", "South Africa", COLORREF(-1L) },
			{ 33.6, -7.6,"Casablanca", "Morocco (Rabat)", COLORREF(-1L) },
			{ 41.83, -87.75,"Chicago", "Illinois, USA", COLORREF(-1L) },
			{ 32.78, -96.80,"Dallas", "Texas, USA", COLORREF(-1L) },
			{ 28.63, 77.20,"New Delhi", "India", COLORREF(-1L) },
			{ 39.75, -105.00,"Denver", "Colorado, USA", COLORREF(-1L) },
			{ 24.23, 55.28,"Dubai", "UAE (Abu Dhabi)", COLORREF(-1L) },
			{ -27.1, -109.4,"Easter Island", "Easter Island", COLORREF(-1L) },
			{ -18.0, 178.1,"Fiji", "Fiji", COLORREF(-1L) },
			{ 13.5, 144.8,"Guam", "Guam", COLORREF(-1L) },
			{ 60.13, 25.00,"Helsinki", "Finland", COLORREF(-1L) },
			{ 22.2, 114.1,"Hong Kong", "Hong Kong", COLORREF(-1L) },
			{ 21.32, -157.83,"Honolulu", "Hawaii, USA", COLORREF(-1L) },
			{ 52.2, 104.3,"Irkutsk", "Irkutsk, Russia", COLORREF(-1L) },
			{ 41.0, 29.0,"Istanbul", "Turkey (Ankara)", COLORREF(-1L) },
			{ -6.13, 106.75,"Jakarta", "Indonesia", COLORREF(-1L) },
			{ 31.8, 35.2,"Jerusalem", "Israel", COLORREF(-1L) },
			{ 34.5, 69.2,"Kabul", "Afghanistan", COLORREF(-1L) },
			{ 27.7, 85.3,"Kathmandu", "Nepal", COLORREF(-1L) },
			{ 50.4, 30.5,"Kiev", "Ukraine", COLORREF(-1L) },
			{ 3.13, 101.70,"Kuala Lumpur", "Malaysia", COLORREF(-1L) },
			{ 6.45, 3.47,"Lagos", "Nigeria", COLORREF(-1L) },
			{ -12.10, -77.05,"Lima", "Peru", COLORREF(-1L) },
			{ 51.50, -0.17,"London", "United Kingdom", COLORREF(-1L) },
			{ 40.42, -3.72,"Madrid", "Spain", COLORREF(-1L) },
			{ 14.6, 121.0,"Manila", "The Phillipines", COLORREF(-1L) },
			{ 21.5, 39.8,"Mecca", "Saudi Arabia", COLORREF(-1L) },
			{ 19.4, -99.1,"Mexico City", "Mexico", COLORREF(-1L) },
			{ 25.8, -80.2,"Miami", "Florida, USA", COLORREF(-1L) },
			{ 6.2, -10.8,"Monrovia", "Liberia", COLORREF(-1L) },
			{ 45.5, -73.5,"Montreal", "Quebec, Canada", COLORREF(-1L) },
			{ 55.75, 37.70,"Moscow", "Russia", COLORREF(-1L) },
			{ -1.28, 36.83,"Nairobi", "Kenya", COLORREF(-1L) },
			{ 59.93, 10.75,"Oslo", "Norway", COLORREF(-1L) },
			{ 48.87, 2.33,"Paris", "France", COLORREF(-1L) },
			{ -32.0, 115.9,"Perth", "Australia", COLORREF(-1L) },
			{ 45.5, -122.5,"Portland", "Oregon, USA", COLORREF(-1L) },
			{ -0.2, -78.5,"Quito", "Ecuador", COLORREF(-1L) },
			{ 64.15, -21.97,"Reykjavik", "Iceland", COLORREF(-1L) },
			{ -22.88, -43.28,"Rio de Janeiro", "Brazil", COLORREF(-1L) },
			{ 41.88, 12.50,"Rome", "Italy", COLORREF(-1L) },
			{ 11.0, 106.7,"Ho Chi Minh City", "Vietnam (Hanoi)", COLORREF(-1L) },
			{ 37.75, -122.45,"San Francisco", "California, USA", COLORREF(-1L) },
			{ 9.98, -84.07,"San Jose", "Costa Rica", COLORREF(-1L) },
			{ 18.5, -66.1,"San Juan", "Puerto Rico", COLORREF(-1L) },
			{ -33.5, -70.7,"Santiago", "Chile", COLORREF(-1L) },
			{ 1.2, 103.9,"Singapore", "Singapore", COLORREF(-1L) },
			{ 42.67, 23.30,"Sofia", "Bulgaria", COLORREF(-1L) },
			{ 59.33, 18.08,"Stockholm", "Sweden", COLORREF(-1L) },
			{ -33.92, 151.17,"Sydney", "Australia", COLORREF(-1L) },
			{ -17.6, -149.5,"Tahiti", "Tahiti", COLORREF(-1L) },
			{ 16.8, -3.0,"Timbuktu", "Mali (Bamako)", COLORREF(-1L) },
			{ 35.67, 139.75,"Tokyo", "Japan", COLORREF(-1L) },
			{ 43.70, -79.42,"Toronto", "Ontario, Canada", COLORREF(-1L) },
			{ 32.9, 13.2,"Tripoli", "Libya", COLORREF(-1L) },
			{ 47.9, 106.9,"Ulan Bator", "Mongolia", COLORREF(-1L) },
			{ 49.22, -123.10,"Vancouver", "B.C., Canada", COLORREF(-1L) },
			{ 48.22, 16.37,"Vienna", "Austria", COLORREF(-1L) },
			{ 38.9, -77.0,"Washington", "United States", COLORREF(-1L) },
			{ -41.28, 174.78,"Wellington", "New Zealand", COLORREF(-1L) },
			{ 62.5, -114.3,"Yellowknife", "N.T., Canada", COLORREF(-1L) },
			{ 90.00, 0.00,"North Pole", NULL, RGB(255,0,0) },
			{ -90.00, 0.00,"South Pole", NULL, RGB(255,0,0) },
		};
		INT nMakerIndex, nMakerCount = sizeof(g_arrCities) / sizeof(g_arrCities[0]);
		m_arrMapMarkers.SetSize( nMakerCount );
		for( nMakerIndex = 0; nMakerIndex < nMakerCount; nMakerIndex ++ )
		{
			USES_CONVERSION;
			MAP_MARKER & _marker = m_arrMapMarkers.ElementAt( nMakerIndex );
			_marker.m_Longitude.m_lf = g_arrCities[nMakerIndex].m_lfLongitude * 60.0 * 60.0;
			_marker.m_Latitude.m_lf = g_arrCities[nMakerIndex].m_lfLatitude * 60.0 * 60.0;
			bool bHaveSecondString = true;
			if(		g_arrCities[nMakerIndex].m_strCityCountry == NULL
				||	strcmp(
						g_arrCities[nMakerIndex].m_strCityCountry,
						g_arrCities[nMakerIndex].m_strCityName
						) == 0
				)
				bHaveSecondString = false;
			_marker.m_strLabel.Format(
				_T("%s%s%s"),
				A2CT(g_arrCities[nMakerIndex].m_strCityName),
				bHaveSecondString ? _T("\n") : _T(""),
				bHaveSecondString ? (A2CT(g_arrCities[nMakerIndex].m_strCityCountry)) : _T("")
				);
			_marker.m_clr = g_arrCities[nMakerIndex].m_clr;
		}
	}
	return m_arrMapMarkers;
}

//////////////////////////////////////////////////////////////////////////
// CExtLLBaseControlProperties::CHANGING_NOTIFICATION

CExtLLBaseControlProperties::CHANGING_NOTIFICATION::CHANGING_NOTIFICATION(
	bool bChangedFinally,
	CExtLLBaseControlProperties & _LLBCP,
	const CExtLongitude & _LongitudeOld,
	const CExtLongitude & _LongitudeNew,
	const CExtLatitude & _LatitudeOld,
	const CExtLatitude & _LatitudeNew,
	LPARAM lParamCookie
	)
	: m_bChangedFinally( bChangedFinally )
	, m_LLBCP( _LLBCP )
	, m_lParamCookie( lParamCookie )
	, m_LongitudeOld( _LongitudeOld )
	, m_LongitudeNew( _LongitudeNew )
	, m_LatitudeOld( _LatitudeOld )
	, m_LatitudeNew( _LatitudeNew )
{
}

CExtLLBaseControlProperties::CHANGING_NOTIFICATION::operator WPARAM() const
{
	WPARAM wParam = reinterpret_cast < WPARAM > ( this );
	return wParam;
}

const CExtLLBaseControlProperties::CHANGING_NOTIFICATION *
	CExtLLBaseControlProperties::CHANGING_NOTIFICATION::FromWPARAM( WPARAM wParam )
{
	CExtLLBaseControlProperties::CHANGING_NOTIFICATION * pSN =
		reinterpret_cast < CExtLLBaseControlProperties::CHANGING_NOTIFICATION * > ( wParam );
	ASSERT( pSN != NULL );
	return pSN;
}

LRESULT CExtLLBaseControlProperties::CHANGING_NOTIFICATION::Notify( HWND hWndNotify ) const
{
	ASSERT( hWndNotify != NULL && ::IsWindow( hWndNotify ) );
LRESULT lResult =
		::SendMessage(
			hWndNotify,
			CExtLLBaseControlProperties::g_nMsgChangingNotification,
			*this,
			m_lParamCookie
			);
	return lResult;
}

/////////////////////////////////////////////////////////////////////////////
// CExtLLEditWnd window

IMPLEMENT_DYNCREATE( CExtLLEditWnd, CExtDurationWnd );

CExtLLEditWnd::CExtLLEditWnd()
	: m_bBlankLongitudeHemisphere( false )
	, m_bBlankLongitudeDegrees( false )
	, m_bBlankLongitudeMinutes( false )
	, m_bBlankLongitudeSeconds( false )
	, m_bBlankLongitudeSecondsFraction( false )
	, m_bBlankLatitudeHemisphere( false )
	, m_bBlankLatitudeDegrees( false )
	, m_bBlankLatitudeMinutes( false )
	, m_bBlankLatitudeSeconds( false )
	, m_bBlankLatitudeSecondsFraction( false )
{
	SetStatus( CExtDurationWnd::valid );
}

CExtLLEditWnd::~CExtLLEditWnd()
{
}

BEGIN_MESSAGE_MAP(CExtLLEditWnd, CExtDurationWnd)
	//{{AFX_MSG_MAP(CExtLLEditWnd)
	ON_WM_KILLFOCUS()
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE( CExtLLBaseControlProperties::g_nMsgChangingNotification, OnLLChangeNotification )
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtLLEditWnd message handlers

#ifdef _DEBUG
void CExtLLEditWnd::AssertValid() const
{
	CExtDurationWnd::AssertValid();
}
void CExtLLEditWnd::Dump(CDumpContext& dc) const
{
	CExtDurationWnd::Dump( dc );
}
#endif // _DEBUG

void CExtLLEditWnd::SetBlank(
	bool bUpdate // = true
	)
{
	ASSERT_VALID( this );
	m_Longitude.Set(	CExtLongitude::hemisphere_code_positive,   0, 0, 0, 0.0, true );
	m_Latitude.Set(		CExtLatitude ::hemisphere_code_positive,   0, 0, 0, 0.0, true );
	m_bBlankLongitudeHemisphere = true;
	m_bBlankLongitudeDegrees = true;
	m_bBlankLongitudeMinutes = true;
	m_bBlankLongitudeSeconds = true;
	m_bBlankLongitudeSecondsFraction = true;
	m_bBlankLatitudeHemisphere = true;
	m_bBlankLatitudeDegrees = true;
	m_bBlankLatitudeMinutes = true;
	m_bBlankLatitudeSeconds = true;
	m_bBlankLatitudeSecondsFraction = true;
	UpdateDurationWnd( true, bUpdate );
}

void CExtLLEditWnd::SetMode( CExtLLBaseControlProperties::eMode_t eMode )
{
	ASSERT_VALID( this );
bool arrVisibilityFlags[ INT(ll_count) ];
INT nItemIndex;
	for( nItemIndex = INT(ll_min_value); nItemIndex <= INT(ll_max_value); nItemIndex ++ )
		arrVisibilityFlags[ nItemIndex - INT(ll_min_value) ] = IsItemVisible( (CExtDurationWnd::eItem_t)nItemIndex );
	CExtLLBaseControlProperties::SetMode( eMode );
	while( m_arrItems.GetSize() > 0 )
	{
		ITEM_INFO * pII = m_arrItems[0];
		m_arrItems.RemoveAt(0);
		delete pII;
		pII = NULL;
	}
	OnInitializeItemsArray();
	for( nItemIndex = INT(ll_min_value); nItemIndex <= INT(ll_max_value); nItemIndex ++ )
		SetShowItem( (CExtDurationWnd::eItem_t)nItemIndex, arrVisibilityFlags[ nItemIndex - INT(ll_min_value) ], false );
	SyncSeparatorSpace();
}

CExtLLBaseControlProperties::eMode_t CExtLLEditWnd::GetMode() const
{
	ASSERT_VALID( this );
	return CExtLLBaseControlProperties::GetMode();
}

bool CExtLLEditWnd::IsDropDownButtonVisible() const
{
	ASSERT_VALID( this );
	return m_bDropDownButtonVisible;
}

CExtLLEditWnd::ITEM_INFO * CExtLLEditWnd::OnInitializeItemLongitudeHemisphere(
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
ITEM_INFO * pHemisphere = 
		new ITEM_INFO(
			(CExtDurationWnd::eItem_t)longitude_hemisphere,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
			);
	ASSERT( pHemisphere != NULL );
	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pHemisphere->AddDependentItem( pII );
	}
	m_arrItems.Add( pHemisphere );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pHemisphere->AddDependentItem( pII );
	}
	return pHemisphere;
}


CExtLLEditWnd::ITEM_INFO * CExtLLEditWnd::OnInitializeItemLongitudeDegrees( 
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
ITEM_INFO * pLongitudeDegrees = 
		new ITEM_INFO(
			(CExtDurationWnd::eItem_t)longitude_degrees,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
			);
	ASSERT( pLongitudeDegrees != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLongitudeDegrees->AddDependentItem( pII );
	}
	m_arrItems.Add( pLongitudeDegrees );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLongitudeDegrees->AddDependentItem( pII );
	}
	return pLongitudeDegrees;
}

CExtLLEditWnd::ITEM_INFO * CExtLLEditWnd::OnInitializeItemLongitudeMinutes( 
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
ITEM_INFO * pLongitudeMinutes = 
		new ITEM_INFO(
			(CExtDurationWnd::eItem_t)longitude_minutes,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
			);
	ASSERT( pLongitudeMinutes != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLongitudeMinutes->AddDependentItem( pII );
	}
	m_arrItems.Add( pLongitudeMinutes );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLongitudeMinutes->AddDependentItem( pII );
	}
	return pLongitudeMinutes;
}

CExtLLEditWnd::ITEM_INFO * CExtLLEditWnd::OnInitializeItemLongitudeSeconds( 
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
ITEM_INFO * pLongitudeSeconds = 
		new ITEM_INFO(
			(CExtDurationWnd::eItem_t)longitude_seconds,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
			);
	ASSERT( pLongitudeSeconds != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLongitudeSeconds->AddDependentItem( pII );
	}
	m_arrItems.Add( pLongitudeSeconds );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLongitudeSeconds->AddDependentItem( pII );
	}
	return pLongitudeSeconds;
}

CExtLLEditWnd::ITEM_INFO * CExtLLEditWnd::OnInitializeItemLongitudeSecondsFraction( 
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
ITEM_INFO * pLongitudeSecondsFraction = 
		new ITEM_INFO(
			(CExtDurationWnd::eItem_t)longitude_seconds_fraction,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
			);
	ASSERT( pLongitudeSecondsFraction != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLongitudeSecondsFraction->AddDependentItem( pII );
	}
	m_arrItems.Add( pLongitudeSecondsFraction );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLongitudeSecondsFraction->AddDependentItem( pII );
	}
	return pLongitudeSecondsFraction;
}

CExtLLEditWnd::ITEM_INFO * CExtLLEditWnd::OnInitializeItemLatitudeHemisphere( 
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
ITEM_INFO * pLatitudeHemisphere = 
		new ITEM_INFO(
			(CExtDurationWnd::eItem_t)latitude_hemisphere,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
			);
	ASSERT( pLatitudeHemisphere != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLatitudeHemisphere->AddDependentItem( pII );
	}
	m_arrItems.Add( pLatitudeHemisphere );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLatitudeHemisphere->AddDependentItem( pII );
	}
	return pLatitudeHemisphere;
}

CExtLLEditWnd::ITEM_INFO * CExtLLEditWnd::OnInitializeItemLatitudeDegrees( 
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
ITEM_INFO * pLatitudeDegrees = 
		new ITEM_INFO(
			(CExtDurationWnd::eItem_t)latitude_degrees,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
			);
	ASSERT( pLatitudeDegrees != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLatitudeDegrees->AddDependentItem( pII );
	}
	m_arrItems.Add( pLatitudeDegrees );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLatitudeDegrees->AddDependentItem( pII );
	}
	return pLatitudeDegrees;
}

CExtLLEditWnd::ITEM_INFO * CExtLLEditWnd::OnInitializeItemLatitudeMinutes( 
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
ITEM_INFO * pLatitudeMinutes = 
		new ITEM_INFO(
			(CExtDurationWnd::eItem_t)latitude_minutes,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
			);
	ASSERT( pLatitudeMinutes != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLatitudeMinutes->AddDependentItem( pII );
	}
	m_arrItems.Add( pLatitudeMinutes );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLatitudeMinutes->AddDependentItem( pII );
	}
	return pLatitudeMinutes;
}

CExtLLEditWnd::ITEM_INFO * CExtLLEditWnd::OnInitializeItemLatitudeSeconds( 
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
ITEM_INFO * pLatitudeSeconds = 
		new ITEM_INFO(
			(CExtDurationWnd::eItem_t)latitude_seconds,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
			);
	ASSERT( pLatitudeSeconds != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLatitudeSeconds->AddDependentItem( pII );
	}
	m_arrItems.Add( pLatitudeSeconds );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLatitudeSeconds->AddDependentItem( pII );
	}
	return pLatitudeSeconds;
}

CExtLLEditWnd::ITEM_INFO * CExtLLEditWnd::OnInitializeItemLatitudeSecondsFraction( 
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
ITEM_INFO * pLatitudeSecondsFraction = 
		new ITEM_INFO(
			(CExtDurationWnd::eItem_t)latitude_seconds_fraction,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
			);
	ASSERT( pLatitudeSecondsFraction != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLatitudeSecondsFraction->AddDependentItem( pII );
	}
	m_arrItems.Add( pLatitudeSecondsFraction );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pLatitudeSecondsFraction->AddDependentItem( pII );
	}
	return pLatitudeSecondsFraction;
}

void CExtLLEditWnd::OnInializeAllLongitudeFields(
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
	TCHAR strSeparator[] = { m_tchrSeparator, _T('\0') };
	TCHAR strFractionSeparator[] = { m_tchrFractionSeparator, _T('\0') };
	OnInitializeItemLongitudeHemisphere( lpszTextBefore,  _T("") );
	OnInitializeItemLongitudeDegrees( strSeparator,  _T("") );
	OnInitializeItemLongitudeMinutes( strSeparator,  _T("") );
	OnInitializeItemLongitudeSeconds( strSeparator,  _T("") );
	OnInitializeItemLongitudeSecondsFraction( strFractionSeparator, lpszTextAfter );
}

void CExtLLEditWnd::OnInializeAllLatitudeFields(
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
	TCHAR strSeparator[] = { m_tchrSeparator, _T('\0') };
	TCHAR strFractionSeparator[] = { m_tchrFractionSeparator, _T('\0') };
	OnInitializeItemLatitudeHemisphere( lpszTextBefore,  _T("") );
	OnInitializeItemLatitudeDegrees( strSeparator,  _T("") );
	OnInitializeItemLatitudeMinutes( strSeparator,  _T("") );
	OnInitializeItemLatitudeSeconds( strSeparator,  _T("") );
	OnInitializeItemLatitudeSecondsFraction( strFractionSeparator, lpszTextAfter );
}

void CExtLLEditWnd::OnInitializeItemsArray()
{
	ASSERT_VALID( this );
	switch( GetMode() )
	{
	case longitude_latitude:
		OnInializeAllLongitudeFields( _T(""), m_strLLS );
		OnInializeAllLatitudeFields( _T(""), _T("") );
	break;
	case latitude_longitude:
		OnInializeAllLatitudeFields( _T(""), m_strLLS );
		OnInializeAllLongitudeFields( _T(""), _T("") );
	break;
	case longitude:
		OnInializeAllLongitudeFields( _T(""), _T("") );
	break;
	case latitude:
		OnInializeAllLatitudeFields( _T(""), _T("") );
	break;
	} // switch( GetMode() )
}

CExtSafeString CExtLLEditWnd::OnQueryItemText( 
	const ITEM_INFO * pII 
	) const
{
	ASSERT_VALID( this );
	ASSERT( pII != NULL );
	if( pII == NULL )
		return _T("");
CExtSafeString sText;
	switch( INT(pII->m_eItemType) ) 
	{
	case INT(CExtDurationWnd::label):
		sText = LPCTSTR( pII->m_sText );
	break;
	/////////////////////////////////////////////////////////////////
	case INT(longitude_hemisphere):
		sText = ( m_Longitude.HemisphereGet() == CExtLongitude::hemisphere_east ) ? TCHAR(m_tchrEast) : TCHAR(m_tchrWest);
	break;
	case INT(longitude_degrees):
		sText.Format( _T("%03d"), m_Longitude.DegreesGet() );
	break;
	case INT(longitude_minutes):
		sText.Format( _T("%02d"), m_Longitude.MinutesGet() );
	break;
	case INT(longitude_seconds):
		sText.Format( _T("%02d"), m_Longitude.SecondsGet() );
	break;
	case INT(longitude_seconds_fraction):
		ASSERT( m_nFractionDigitCount > 0 );
		sText.Format( _T("%0*d"), m_nFractionDigitCount, INT( m_Longitude.SecondsFractionGet() * ::powl( 10, m_nFractionDigitCount ) ) );
	break;
	/////////////////////////////////////////////////////////////////
	case INT(latitude_hemisphere):
		sText = ( m_Latitude.HemisphereGet() == CExtLatitude::hemisphere_north ) ? TCHAR(m_tchrNorth) : TCHAR(m_tchrSouth);
	break;
	case INT(latitude_degrees):
		sText.Format( _T("%02d"), m_Latitude.DegreesGet() );
	break;
	case INT(latitude_minutes):
		sText.Format( _T("%02d"), m_Latitude.MinutesGet() );
	break;
	case INT(latitude_seconds):
		sText.Format( _T("%02d"), m_Latitude.SecondsGet() );
	break;
	case INT(latitude_seconds_fraction):
		ASSERT( m_nFractionDigitCount > 0 );
		sText.Format( _T("%0*d"), m_nFractionDigitCount, INT( m_Latitude.SecondsFractionGet() * ::powl( 10, m_nFractionDigitCount ) ) );
	break;
	} // switch( INT(pII->m_eItemType) ) 

CExtSafeString sTextBlank = sText;
	for( INT i = 0; i < sTextBlank.GetLength(); i++ )
		sTextBlank.SetAt( i, _T(' ') );
	switch( INT(pII->m_eItemType) ) 
	{
	case INT(longitude_hemisphere):
		if( m_bBlankLongitudeHemisphere )
			sText = sTextBlank;
	break;
	case INT(longitude_degrees):
		if( m_bBlankLongitudeDegrees )
			sText = sTextBlank;
	break;
	case INT(longitude_minutes):
		if( m_bBlankLongitudeMinutes )
			sText = sTextBlank;
	break;
	case INT(longitude_seconds):
		if( m_bBlankLongitudeSeconds )
			sText = sTextBlank;
	break;
	case INT(longitude_seconds_fraction):
		if( m_bBlankLongitudeSecondsFraction )
			sText = sTextBlank;
	break;
	/////////////////////////////////////////////////////////////////
	case INT(latitude_hemisphere):
		if( m_bBlankLatitudeHemisphere )
			sText = sTextBlank;
	break;
	case INT(latitude_degrees):
		if( m_bBlankLatitudeDegrees )
			sText = sTextBlank;
	break;
	case INT(latitude_minutes):
		if( m_bBlankLatitudeMinutes )
			sText = sTextBlank;
	break;
	case INT(latitude_seconds):
		if( m_bBlankLongitudeSeconds )
			sText = sTextBlank;
	break;
	case INT(latitude_seconds_fraction):
		if( m_bBlankLatitudeSecondsFraction )
			sText = sTextBlank;
	break;
	}

	return sText;
}

bool CExtLLEditWnd::_CreateHelper()
{
	ASSERT_VALID( this );
	bool bRet = CExtDurationWnd::_CreateHelper();
	UpdateDurationWnd( true, true );
	return bRet;
}

void CExtLLEditWnd::_RecalcDuration()
{
	ASSERT_VALID( this );
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
	{
		ITEM_INFO * pII = m_arrItems[ nItem ];
		ASSERT( pII != NULL );
		if( pII != NULL )
			pII->m_nValue = 0;
	}

	ASSERT( m_nFractionDigitCount > 0 );

ITEM_INFO * pII = NULL;
CExtLLBaseControlProperties::eMode_t eMode = GetMode();
	if( eMode == longitude || eMode == longitude_latitude || eMode == latitude_longitude )
	{
		CExtLongitude _LongitudeCurrent;
		LongitudeGet( _LongitudeCurrent );

// 		pII = ItemGet( (CExtDurationWnd::eItem_t)CExtLLBaseControlProperties::longitude_hemisphere );
// 		if(		pII != NULL 
// 			&&	pII->m_bVisible
// 			)
// 			pII->m_nValue = ?;

		pII = ItemGet( (CExtDurationWnd::eItem_t)CExtLLBaseControlProperties::longitude_degrees );
		if(		pII != NULL 
			&&	pII->m_bVisible
			)
			pII->m_nValue = _LongitudeCurrent.DegreesGet();

		pII = ItemGet( (CExtDurationWnd::eItem_t)CExtLLBaseControlProperties::longitude_minutes );
		if(		pII != NULL 
			&&	pII->m_bVisible
			)
			pII->m_nValue = _LongitudeCurrent.MinutesGet();

		pII = ItemGet( (CExtDurationWnd::eItem_t)CExtLLBaseControlProperties::longitude_seconds );
		if(		pII != NULL 
			&&	pII->m_bVisible
			)
			pII->m_nValue = _LongitudeCurrent.SecondsGet();

		pII = ItemGet( (CExtDurationWnd::eItem_t)CExtLLBaseControlProperties::longitude_seconds_fraction );
		if(		pII != NULL 
			&&	pII->m_bVisible
			)
			pII->m_nValue = INT( _LongitudeCurrent.SecondsFractionGet() * ::powl( 10, m_nFractionDigitCount ) );
	}

	if( eMode == latitude || eMode == longitude_latitude || eMode == latitude_longitude )
	{
		CExtLatitude _LatitudeCurrent;
		LatitudeGet( _LatitudeCurrent );

// 		pII = ItemGet( (CExtDurationWnd::eItem_t)CExtLLBaseControlProperties::latitude_hemisphere );
// 		if(		pII != NULL 
// 			&&	pII->m_bVisible
// 			)
// 			pII->m_nValue = ?;

		pII = ItemGet( (CExtDurationWnd::eItem_t)CExtLLBaseControlProperties::latitude_degrees );
		if(		pII != NULL 
			&&	pII->m_bVisible
			)
			pII->m_nValue = _LatitudeCurrent.DegreesGet();

		pII = ItemGet( (CExtDurationWnd::eItem_t)CExtLLBaseControlProperties::latitude_minutes );
		if(		pII != NULL 
			&&	pII->m_bVisible
			)
			pII->m_nValue = _LatitudeCurrent.MinutesGet();

		pII = ItemGet( (CExtDurationWnd::eItem_t)CExtLLBaseControlProperties::latitude_seconds );
		if(		pII != NULL 
			&&	pII->m_bVisible
			)
			pII->m_nValue = _LatitudeCurrent.SecondsGet();

		pII = ItemGet( (CExtDurationWnd::eItem_t)CExtLLBaseControlProperties::latitude_seconds_fraction );
		if(		pII != NULL 
			&&	pII->m_bVisible
			)
			pII->m_nValue = INT( _LatitudeCurrent.SecondsFractionGet() * ::powl( 10, m_nFractionDigitCount ) );
	}
}

bool CExtLLEditWnd::OnShowDropDownMenu()
{
	ASSERT_VALID( this );
CExtLLPopupMapMenuWnd * pPopup =
		STATIC_DOWNCAST(
			CExtLLPopupMapMenuWnd,
			CExtPopupMenuWnd::InstantiatePopupMenu( GetSafeHwnd(), RUNTIME_CLASS(CExtLLPopupMapMenuWnd), this )
			);
	if( ! pPopup->CreatePopupMenu( GetSafeHwnd() ) )
	{
		ASSERT( FALSE );
		delete pPopup;
		return false;
	}
CRect rcClient;
	GetClientRect( &rcClient );
	ClientToScreen( &rcClient );
CPoint ptTrack( rcClient.right, rcClient.bottom );
	ptTrack.x += pPopup->OnQueryMenuShadowSize();
CRect rcExclude( ptTrack, ptTrack );
	pPopup->m_wndMap.AssignProperties( *this );
	pPopup->m_wndMap.m_hWndNotificationReceiver = m_hWnd;

	switch( GetMode() )
	{
	case longitude_latitude:
	break;
	case latitude_longitude:
	break;
	case longitude:
		pPopup->m_wndMap.m_bMapDisplayMarkers = false;
		pPopup->m_wndMap.m_bMapDisplayMarkerTips = false;
//		pPopup->m_wndMap.m_bMapDisplayCurrentLocationTip = false;
// 		pPopup->m_wndMap.m_bMapDisplayLocalDate = false;
// 		pPopup->m_wndMap.m_bMapDisplayLocalTime = false;
	break;
	case latitude:
		pPopup->m_wndMap.m_bMapDisplayMarkers = false;
		pPopup->m_wndMap.m_bMapDisplayMarkerTips = false;
		pPopup->m_wndMap.m_bMapHighlightTimeZones = false;
		pPopup->m_wndMap.m_bMapDisplayTimeZoneTips = false;
//		pPopup->m_wndMap.m_bMapDisplayCurrentLocationTip = false;
		pPopup->m_wndMap.m_bMapDisplayLocalDate = false;
		pPopup->m_wndMap.m_bMapDisplayLocalTime = false;
	break;
	} // switch( GetMode() )
	
	if(	! pPopup->TrackPopupMenu(
			TPMX_RIGHTALIGN,
			ptTrack.x, 
			ptTrack.y,
			rcExclude,
			this,
			NULL,
			NULL,
			true
			) 
		)
	{
		//ASSERT( FALSE );
		//delete pPopup;
		return false;
	}
	return true;
}

void CExtLLEditWnd::OnItemFinishInput()
{
	ASSERT_VALID( this );
	CExtDurationWnd::OnItemFinishInput();
}

void CExtLLEditWnd::OnItemSelectionChanged()
{
	ASSERT_VALID( this );
	UpdateDurationWnd();
}

void CExtLLEditWnd::OnKillFocus(CWnd* pNewWnd) 
{
	ASSERT_VALID( this );
	CExtDurationWnd::OnKillFocus(pNewWnd);
	UpdateDurationWnd();
}

void CExtLLEditWnd::ScrollCurrentItem( INT nDelta )
{
	ASSERT_VALID( this );
	ASSERT( m_nFractionDigitCount > 0 );
ITEM_INFO * pII = SelectionGet();
	if( pII != NULL )
	{
		switch( INT(pII->m_eItemType) ) 
		{
		case INT(longitude_seconds_fraction):
			{
				CExtLongitude _LongitudeCurrent;
				LongitudeGet( _LongitudeCurrent );
				CExtLL_t lfShift = ::powl( 10, -m_nFractionDigitCount );
				if( nDelta < 0 )
					lfShift = - lfShift;
				CExtLongitude _LongitudeShift( CExtLongitude::hemisphere_code_positive, 0, 0, 0, lfShift, false );
				_LongitudeCurrent += _LongitudeShift;
				LongitudeSet( _LongitudeCurrent, false );
			}
			break;
		case INT(longitude_seconds):
			{
				CExtLongitude _LongitudeCurrent, _LongitudeShift( CExtLongitude::hemisphere_code_positive, 0, 0, nDelta, 0.0, false );
				LongitudeGet( _LongitudeCurrent );
				_LongitudeCurrent += _LongitudeShift;
				LongitudeSet( _LongitudeCurrent, false );
			}
			break;
		case INT(longitude_minutes):
			{
				CExtLongitude _LongitudeCurrent, _LongitudeShift( CExtLongitude::hemisphere_code_positive, 0, nDelta, 0, 0.0, false );
				LongitudeGet( _LongitudeCurrent );
				_LongitudeCurrent += _LongitudeShift;
				LongitudeSet( _LongitudeCurrent, false );
			}
			break;
		case INT(longitude_degrees):
			{
				CExtLongitude _LongitudeCurrent, _LongitudeShift( CExtLongitude::hemisphere_code_positive, nDelta, 0, 0, 0.0, false );
				LongitudeGet( _LongitudeCurrent );
				_LongitudeCurrent += _LongitudeShift;
				LongitudeSet( _LongitudeCurrent, false );
			}
			break;
		case INT(longitude_hemisphere):
			{
				CExtLongitude _LongitudeCurrent;
				LongitudeGet( _LongitudeCurrent );
				if( _LongitudeCurrent.HemisphereGet() == CExtLongitude::hemisphere_code_positive )
					_LongitudeCurrent.HemisphereSet( CExtLongitude::hemisphere_code_negative );
				else
					_LongitudeCurrent.HemisphereSet( CExtLongitude::hemisphere_code_positive );
				LongitudeSet( _LongitudeCurrent, false );
			}
			break;
		case INT(latitude_seconds_fraction):
			{
				CExtLL_t lfShift = ::powl( 10, -m_nFractionDigitCount );
				if( nDelta < 0 )
					lfShift = - lfShift;
				CExtLatitude _LatitudeCurrent, _LatitudeShift( CExtLatitude::hemisphere_code_positive, 0, 0, 0, lfShift, false );
				LatitudeGet( _LatitudeCurrent );
				_LatitudeCurrent.Shift( _LatitudeShift, false, false );
				if( _LatitudeCurrent.m_lf > CExtLatitude::stat_ll_get_value_in_seconds_max() )
					_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_max();
				else if( _LatitudeCurrent.m_lf < CExtLatitude::stat_ll_get_value_in_seconds_min() )
					_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_min();
				LatitudeSet( _LatitudeCurrent, false );
			}
			break;
		case INT(latitude_seconds):
			{
				CExtLatitude _LatitudeCurrent, _LatitudeShift( CExtLatitude::hemisphere_code_positive, 0, 0, nDelta, 0.0, false );
				LatitudeGet( _LatitudeCurrent );
				_LatitudeCurrent.Shift( _LatitudeShift, false, false );
				if( _LatitudeCurrent.m_lf > CExtLatitude::stat_ll_get_value_in_seconds_max() )
					_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_max();
				else if( _LatitudeCurrent.m_lf < CExtLatitude::stat_ll_get_value_in_seconds_min() )
					_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_min();
				LatitudeSet( _LatitudeCurrent, false );
			}
			break;
		case INT(latitude_minutes):
			{
				CExtLatitude _LatitudeCurrent, _LatitudeShift( CExtLatitude::hemisphere_code_positive, 0, nDelta, 0, 0.0, false );
				LatitudeGet( _LatitudeCurrent );
				_LatitudeCurrent.Shift( _LatitudeShift, false, false );
				if( _LatitudeCurrent.m_lf > CExtLatitude::stat_ll_get_value_in_seconds_max() )
					_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_max();
				else if( _LatitudeCurrent.m_lf < CExtLatitude::stat_ll_get_value_in_seconds_min() )
					_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_min();
				LatitudeSet( _LatitudeCurrent, false );
			}
			break;
		case INT(latitude_degrees):
			{
				CExtLatitude _LatitudeCurrent, _LatitudeShift( CExtLatitude::hemisphere_code_positive, nDelta, 0, 0, 0.0, false );
				LatitudeGet( _LatitudeCurrent );
				_LatitudeCurrent.Shift( _LatitudeShift, false, false );
				if( _LatitudeCurrent.m_lf > CExtLatitude::stat_ll_get_value_in_seconds_max() )
					_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_max();
				else if( _LatitudeCurrent.m_lf < CExtLatitude::stat_ll_get_value_in_seconds_min() )
					_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_min();
				LatitudeSet( _LatitudeCurrent, false );
			}
			break;
		case INT(latitude_hemisphere):
			{
				CExtLatitude _LatitudeCurrent;
				LatitudeGet( _LatitudeCurrent );
				if( _LatitudeCurrent.HemisphereGet() == CExtLatitude::hemisphere_code_positive )
					_LatitudeCurrent.HemisphereSet( CExtLatitude::hemisphere_code_negative );
				else
					_LatitudeCurrent.HemisphereSet( CExtLatitude::hemisphere_code_positive );
				LatitudeSet( _LatitudeCurrent, false );
			}
			break;
		default:
			ASSERT( FALSE );
		}
		UpdateDurationWnd( true, true );
	} // if( pII != NULL )
}

void CExtLLEditWnd::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	ASSERT_VALID( this );
	ASSERT( m_nFractionDigitCount > 0 );
	switch( nChar )
	{
	case VK_ESCAPE:
	{
		HWND hWndParent = ::GetParent( m_hWnd );
		if( hWndParent != NULL )
		{
			::PostMessage( hWndParent, WM_CANCELMODE, 0L, 0L );
			CWnd * pWndParent = CWnd::FromHandlePermanent( hWndParent );
			if(		pWndParent != NULL
				&&	( pWndParent->GetStyle() & WS_CHILD ) == 0
				&&	pWndParent->IsKindOf( RUNTIME_CLASS(CDialog) )
				)
			{
				HWND hWndCancel = ::GetDlgItem( hWndParent, IDCANCEL );
				if(		hWndCancel == NULL
					||	(	::IsWindowEnabled( hWndCancel )
						&&	( ::__EXT_MFC_GetWindowLong( hWndCancel, GWL_STYLE ) & WS_VISIBLE ) != 0
						)
					)
					::PostMessage( hWndParent, WM_COMMAND, WPARAM(IDCANCEL), 0L );
			}
		}
	}
	break;
	case VK_RETURN:
	{
		HWND hWndParent = ::GetParent( m_hWnd );
		if( hWndParent != NULL )
		{
			::PostMessage( hWndParent, WM_CANCELMODE, 0L, 0L );
			CWnd * pWndParent = CWnd::FromHandlePermanent( hWndParent );
			if(		pWndParent != NULL
				&&	( pWndParent->GetStyle() & WS_CHILD ) == 0
				)
			{
				CDialog * pDlg = DYNAMIC_DOWNCAST( CDialog, pWndParent );
				if( pDlg != NULL )
				{
					UINT nDefID = 0;
					HWND hWnd = ::GetNextWindow( hWndParent, GW_CHILD );
					for( ; hWnd != NULL; hWnd = ::GetNextWindow( hWnd, GW_HWNDNEXT ) )
					{
						TCHAR szCompare[512] = _T("");
						::GetClassName( hWnd, szCompare, sizeof( szCompare )/sizeof( szCompare[0] ) );
						if( _tcsicmp( szCompare, _T("BUTTON") ) != 0 )
							continue;
						CWnd * pWnd = CWnd::FromHandlePermanent( hWnd );
						if( pWnd != NULL )
						{
							CExtButton * pBtn = DYNAMIC_DOWNCAST( CExtButton, pWnd );
							if( pBtn != NULL && pBtn->GetDefault() )
							{
								nDefID = (UINT) pBtn->GetDlgCtrlID();
								break;
							}
						}
						DWORD dw = (DWORD) pWnd->GetStyle();
						dw &= 0x0F;
						if( dw == BS_DEFPUSHBUTTON )
						{
							nDefID = (UINT) ::__EXT_MFC_GetWindowLong( hWnd, GWL_ID );
							break;
						}
					}
					if( nDefID == 0 )
					{
						nDefID = (UINT) pDlg->GetDefID();
						if( nDefID == 0 )
							nDefID = IDOK;
						else if( ::GetDlgItem( hWndParent, nDefID ) == NULL )
							nDefID = IDOK;
					}
					if( nDefID != 0 )
					{
						HWND hWndDef = ::GetDlgItem( hWndParent, nDefID );
						if(		hWndDef != NULL
							&&	::IsWindowEnabled( hWndDef )
							&&	( ::__EXT_MFC_GetWindowLong( hWndDef, GWL_STYLE ) & WS_VISIBLE ) != 0
							)
							::PostMessage( hWndParent, WM_COMMAND, WPARAM(nDefID), 0L );
					}
				}
			}
		}
	}
	break;
	case VK_HOME:
	case VK_END:
		if( ! IsReadOnly() )
		{
			ITEM_INFO * pII = SelectionGet();
			if( pII != NULL )
			{
				switch( INT(pII->m_eItemType) )
				{
				case INT(longitude_seconds_fraction):
					{
						CExtLongitude _LongitudeCurrent;
						LongitudeGet( _LongitudeCurrent );
						CExtLongitude _LongitudeNew( 
							_LongitudeCurrent.HemisphereGet(),
							_LongitudeCurrent.DegreesGet(), 
							( _LongitudeCurrent.DegreesGet() == 180 ) ? 0 : _LongitudeCurrent.MinutesGet(), 
							( _LongitudeCurrent.DegreesGet() == 180 ) ? 0 : _LongitudeCurrent.SecondsGet(),
							(	nChar == VK_HOME
							||	_LongitudeCurrent.DegreesGet() == 180
								)
								? 0.0 : 0.999,
							false 
							);
						LongitudeSet( _LongitudeNew, false );
						m_bBlankLongitudeSecondsFraction = false;
					}
					break;
				case INT(longitude_seconds):
					{
						CExtLongitude _LongitudeCurrent;
						LongitudeGet( _LongitudeCurrent );
						CExtLongitude _LongitudeNew( 
							_LongitudeCurrent.HemisphereGet(),
							_LongitudeCurrent.DegreesGet(), 
							( _LongitudeCurrent.DegreesGet() == 180 ) ? 0 : _LongitudeCurrent.MinutesGet(), 
							(	nChar == VK_HOME
							||	_LongitudeCurrent.DegreesGet() == 180
								) 
								? 0 : 59,
							( _LongitudeCurrent.DegreesGet() == 180 ) ? 0 : _LongitudeCurrent.SecondsFractionGet(),
							false 
							);
						LongitudeSet( _LongitudeNew, false );
						m_bBlankLongitudeSeconds = false;
					}
					break;
				case INT(longitude_minutes):
					{
						CExtLongitude _LongitudeCurrent;
						LongitudeGet( _LongitudeCurrent );
						CExtLongitude _LongitudeNew( 
							_LongitudeCurrent.HemisphereGet(),
							_LongitudeCurrent.DegreesGet(), 
							(	nChar == VK_HOME
							||	_LongitudeCurrent.DegreesGet() == 180
								) 
								? 0 : 59,
							( _LongitudeCurrent.DegreesGet() == 180 ) ? 0 : _LongitudeCurrent.SecondsGet(), 
							( _LongitudeCurrent.DegreesGet() == 180 ) ? 0 : _LongitudeCurrent.SecondsFractionGet(),
							false 
							);
						LongitudeSet( _LongitudeNew, false );
						m_bBlankLongitudeMinutes = false;
					}
					break;
				case INT(longitude_degrees):
					{
						CExtLongitude _LongitudeCurrent;
						LongitudeGet( _LongitudeCurrent );
						CExtLongitude _LongitudeNew( 
							_LongitudeCurrent.HemisphereGet(),
							( nChar == VK_HOME ) ? 0 : 180,
							( nChar == VK_END ) ? 0 : _LongitudeCurrent.MinutesGet(),
							( nChar == VK_END ) ? 0 : _LongitudeCurrent.SecondsGet(),
							( nChar == VK_END ) ? 0 : _LongitudeCurrent.SecondsFractionGet(),
							false
							);
						LongitudeSet( _LongitudeNew, false );
						m_bBlankLongitudeDegrees = false;
					}
					break;
				case INT(longitude_hemisphere):
					{
						CExtLongitude _LongitudeCurrent;
						LongitudeGet( _LongitudeCurrent );
						CExtLongitude _LongitudeNew( 
							(nChar == VK_HOME) ? CExtLongitude::hemisphere_east : CExtLongitude::hemisphere_west,
							_LongitudeCurrent.DegreesGet(),
							_LongitudeCurrent.MinutesGet(),
							_LongitudeCurrent.SecondsGet(),
							_LongitudeCurrent.SecondsFractionGet(),
							false
							);
						LongitudeSet( _LongitudeNew, false );
						m_bBlankLongitudeHemisphere = false;
					}
					break;

				case INT(latitude_seconds_fraction):
					{
						CExtLatitude _LatitudeCurrent;
						LatitudeGet( _LatitudeCurrent );
						CExtLatitude _LatitudeNew( 
							_LatitudeCurrent.HemisphereGet(),
							_LatitudeCurrent.DegreesGet(), 
							( _LatitudeCurrent.DegreesGet() == 90 ) ? 0 : _LatitudeCurrent.MinutesGet(), 
							( _LatitudeCurrent.DegreesGet() == 90 ) ? 0 : _LatitudeCurrent.SecondsGet(),
							(	nChar == VK_HOME
							||	_LatitudeCurrent.DegreesGet() == 90
								)
								? 0.0 : 0.999,
							false 
							);
						LatitudeSet( _LatitudeNew, false );
						m_bBlankLatitudeSecondsFraction = false;
					}
					break;
				case INT(latitude_seconds):
					{
						CExtLatitude _LatitudeCurrent;
						LatitudeGet( _LatitudeCurrent );
						CExtLatitude _LatitudeNew( 
							_LatitudeCurrent.HemisphereGet(),
							_LatitudeCurrent.DegreesGet(), 
							( _LatitudeCurrent.DegreesGet() == 90 ) ? 0 : _LatitudeCurrent.MinutesGet(), 
							(	nChar == VK_HOME
							||	_LatitudeCurrent.DegreesGet() == 90
								) 
								? 0 : 59,
							( _LatitudeCurrent.DegreesGet() == 90 ) ? 0 : _LatitudeCurrent.SecondsFractionGet(),
							false 
							);
						LatitudeSet( _LatitudeNew, false );
						m_bBlankLatitudeSeconds = false;
					}
					break;
				case INT(latitude_minutes):
					{
						CExtLatitude _LatitudeCurrent;
						LatitudeGet( _LatitudeCurrent );
						CExtLatitude _LatitudeNew( 
							_LatitudeCurrent.HemisphereGet(),
							_LatitudeCurrent.DegreesGet(), 
							(	nChar == VK_HOME
							||	_LatitudeCurrent.DegreesGet() == 90
								) 
								? 0 : 59,
							( _LatitudeCurrent.DegreesGet() == 90 ) ? 0 : _LatitudeCurrent.SecondsGet(), 
							( _LatitudeCurrent.DegreesGet() == 90 ) ? 0 : _LatitudeCurrent.SecondsFractionGet(),
							false 
							);
						LatitudeSet( _LatitudeNew, false );
						m_bBlankLatitudeMinutes = false;
					}
					break;
				case INT(latitude_degrees):
					{
						CExtLatitude _LatitudeCurrent;
						LatitudeGet( _LatitudeCurrent );
						CExtLatitude _LatitudeNew( 
							_LatitudeCurrent.HemisphereGet(),
							( nChar == VK_HOME ) ? 0 : 90,
							( nChar == VK_END ) ? 0 : _LatitudeCurrent.MinutesGet(),
							( nChar == VK_END ) ? 0 : _LatitudeCurrent.SecondsGet(),
							( nChar == VK_END ) ? 0 : _LatitudeCurrent.SecondsFractionGet(),
							false
							);
						LatitudeSet( _LatitudeNew, false );
						m_bBlankLatitudeDegrees = false;
					}
					break;
				case INT(latitude_hemisphere):
					{
						CExtLatitude _LatitudeCurrent;
						LatitudeGet( _LatitudeCurrent );
						CExtLatitude _LatitudeNew( 
							(nChar == VK_HOME) ? CExtLatitude::hemisphere_north : CExtLatitude::hemisphere_south,
							_LatitudeCurrent.DegreesGet(),
							_LatitudeCurrent.MinutesGet(),
							_LatitudeCurrent.SecondsGet(),
							_LatitudeCurrent.SecondsFractionGet(),
							false
							);
						LatitudeSet( _LatitudeNew, false );
						m_bBlankLatitudeHemisphere = false;
					}
					break;
				default:
					ASSERT( FALSE );
				}
			} // if( pII != NULL )
		}
		break;
	case VK_LEFT:
	case VK_RIGHT:
		{
			ITEM_INFO * pII = SelectionGet();
			if( pII != NULL )
			{
				if( nChar == VK_RIGHT )
					SelectNextItem( pII );
				else
					SelectPrevItem( pII );
			}
			else
				SelectFirstItem();
		}
		break;
	case VK_UP:
	case VK_DOWN:
		if( ! IsReadOnly() )
		{
			ITEM_INFO * pII = SelectionGet();
			if( pII != NULL )
			{
				switch( INT(pII->m_eItemType) )
				{
				case INT(longitude_seconds_fraction):
					{
						CExtLongitude _LongitudeCurrent;
						LongitudeGet( _LongitudeCurrent );
						CExtLL_t lfShift = ::powl( 10, -m_nFractionDigitCount );
						CExtLongitude _LongitudeShift( 
							CExtLongitude::hemisphere_code_positive,
							0, 
							0, 
							0,
							( nChar == VK_DOWN ) ? (- lfShift) : lfShift,
							false
							);
						_LongitudeCurrent += _LongitudeShift;
						LongitudeSet( _LongitudeCurrent, false );
						m_bBlankLongitudeSecondsFraction = false;
					}
					break;
				case INT(longitude_seconds):
					{
						CExtLongitude _LongitudeCurrent;
						LongitudeGet( _LongitudeCurrent );
						CExtLongitude _LongitudeShift( 
							CExtLongitude::hemisphere_code_positive,
							0, 
							0, 
							( nChar == VK_DOWN ) ? (- 1) : 1,
							0.0,
							false
							);
						_LongitudeCurrent += _LongitudeShift;
						LongitudeSet( _LongitudeCurrent, false );
						m_bBlankLongitudeSeconds = false;
					}
					break;
				case INT(longitude_minutes):
					{
						CExtLongitude _LongitudeCurrent;
						LongitudeGet( _LongitudeCurrent );
						CExtLongitude _LongitudeShift( 
							CExtLongitude::hemisphere_code_positive,
							0, 
							( nChar == VK_DOWN ) ? (- 1) : 1,
							0, 
							0.0,
							false
							);
						_LongitudeCurrent += _LongitudeShift;
						LongitudeSet( _LongitudeCurrent, false );
						m_bBlankLongitudeMinutes = false;
					}
					break;
				case INT(longitude_degrees):
					{
						CExtLongitude _LongitudeCurrent;
						LongitudeGet( _LongitudeCurrent );
						CExtLongitude _LongitudeShift( 
							CExtLongitude::hemisphere_code_positive,
							( nChar == VK_DOWN ) ? (- 1) : 1,
							0,
							0,
							0.0,
							false
							);
						_LongitudeCurrent += _LongitudeShift;
						LongitudeSet( _LongitudeCurrent, false );
						m_bBlankLongitudeDegrees = false;
					}
					break;
				case INT(longitude_hemisphere):
					{
						CExtLongitude _LongitudeCurrent;
						LongitudeGet( _LongitudeCurrent );
						CExtLongitude _LongitudeNew( 
							( _LongitudeCurrent.HemisphereGet() == CExtLongitude::hemisphere_west ) 
								? CExtLongitude::hemisphere_east 
								: CExtLongitude::hemisphere_west
								,
							_LongitudeCurrent.DegreesGet(),
							_LongitudeCurrent.MinutesGet(),
							_LongitudeCurrent.SecondsGet(),
							_LongitudeCurrent.SecondsFractionGet(),
							false
							);
						LongitudeSet( _LongitudeNew, false );
						m_bBlankLongitudeHemisphere = false;
					}
					break;

				case INT(latitude_seconds_fraction):
					{
						CExtLatitude _LatitudeCurrent;
						LatitudeGet( _LatitudeCurrent );
						CExtLL_t lfShift = ::powl( 10, -m_nFractionDigitCount );
						CExtLatitude _LatitudeShift( 
							CExtLatitude::hemisphere_code_positive,
							0, 
							0, 
							0,
							( nChar == VK_DOWN ) ? (- lfShift) : lfShift,
							false 
							);
						_LatitudeCurrent.Shift( _LatitudeShift, false, false );
						if( _LatitudeCurrent.m_lf > CExtLatitude::stat_ll_get_value_in_seconds_max() )
							_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_max();
						else if( _LatitudeCurrent.m_lf < CExtLatitude::stat_ll_get_value_in_seconds_min() )
							_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_min();
						LatitudeSet( _LatitudeCurrent, false );
						m_bBlankLatitudeSecondsFraction = false;
					}
					break;
				case INT(latitude_seconds):
					{
						CExtLatitude _LatitudeCurrent;
						LatitudeGet( _LatitudeCurrent );
						CExtLatitude _LatitudeShift( 
							CExtLatitude::hemisphere_code_positive,
							0, 
							0, 
							( nChar == VK_DOWN ) ? (- 1) : 1,
							0.0,
							false
							);
						_LatitudeCurrent.Shift( _LatitudeShift, false, false );
						if( _LatitudeCurrent.m_lf > CExtLatitude::stat_ll_get_value_in_seconds_max() )
							_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_max();
						else if( _LatitudeCurrent.m_lf < CExtLatitude::stat_ll_get_value_in_seconds_min() )
							_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_min();
						LatitudeSet( _LatitudeCurrent, false );
						m_bBlankLatitudeSeconds = false;
					}
					break;
				case INT(latitude_minutes):
					{
						CExtLatitude _LatitudeCurrent;
						LatitudeGet( _LatitudeCurrent );
						CExtLatitude _LatitudeShift( 
							CExtLatitude::hemisphere_code_positive,
							0, 
							( nChar == VK_DOWN ) ? (- 1) : 1,
							0, 
							0.0,
							false 
							);
						_LatitudeCurrent.Shift( _LatitudeShift, false, false );
						if( _LatitudeCurrent.m_lf > CExtLatitude::stat_ll_get_value_in_seconds_max() )
							_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_max();
						else if( _LatitudeCurrent.m_lf < CExtLatitude::stat_ll_get_value_in_seconds_min() )
							_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_min();
						LatitudeSet( _LatitudeCurrent, false );
						m_bBlankLatitudeMinutes = false;
					}
					break;
				case INT(latitude_degrees):
					{
						CExtLatitude _LatitudeCurrent;
						LatitudeGet( _LatitudeCurrent );
						CExtLatitude _LatitudeShift( 
							CExtLatitude::hemisphere_code_positive,
							( nChar == VK_DOWN ) ? (- 1) : 1,
							0,
							0,
							0.0,
							false
							);
						_LatitudeCurrent.Shift( _LatitudeShift, false, false );
						if( _LatitudeCurrent.m_lf > CExtLatitude::stat_ll_get_value_in_seconds_max() )
							_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_max();
						else if( _LatitudeCurrent.m_lf < CExtLatitude::stat_ll_get_value_in_seconds_min() )
							_LatitudeCurrent.m_lf = CExtLatitude::stat_ll_get_value_in_seconds_min();
						LatitudeSet( _LatitudeCurrent, false );
						m_bBlankLatitudeDegrees = false;
					}
					break;
				case INT(latitude_hemisphere):
					{
						CExtLatitude _LatitudeCurrent;
						LatitudeGet( _LatitudeCurrent );
						CExtLatitude _LatitudeNew( 
							( _LatitudeCurrent.HemisphereGet() == CExtLatitude::hemisphere_south )
								? CExtLatitude::hemisphere_north
								: CExtLatitude::hemisphere_south
								,
							_LatitudeCurrent.DegreesGet(),
							_LatitudeCurrent.MinutesGet(),
							_LatitudeCurrent.SecondsGet(),
							_LatitudeCurrent.SecondsFractionGet(),
							false
							);
						LatitudeSet( _LatitudeNew, false );
						m_bBlankLatitudeHemisphere = false;
					}
					break;
				default:
					ASSERT( FALSE );
				}
			} // if( pII != NULL )
		}
		break;
	case 67: // C
	case 86: // V
	case VK_INSERT:
		{
			ITEM_INFO * pII = SelectionGet();
			if( pII != NULL )
			{
				bool bCtrl = ( (::GetAsyncKeyState(VK_CONTROL)&0x8000) != 0 ) ? true : false;
				bool bShift = ( (::GetAsyncKeyState(VK_SHIFT)&0x8000) != 0 ) ? true : false;
				if(		( nChar == 67 && bCtrl ) 
					||	( nChar == VK_INSERT && bCtrl )  
					)
				{
					CExtSafeString sText = TextGet();
					//sText.Remove( _T(' ') );

					// copy to clipboard
					if( OpenClipboard() )
					{
						if( ::EmptyClipboard() )
						{
							HGLOBAL hGlobal =
								::GlobalAlloc( 
									GMEM_DDESHARE, 
									( sText.GetLength() + 1 ) * sizeof(TCHAR)
									);
							ASSERT( hGlobal != NULL );
							if( hGlobal != NULL )
							{
								LPTSTR lpszBuffer = 
									(LPTSTR) ::GlobalLock( hGlobal );
								__EXT_MFC_STRCPY( 
									lpszBuffer, 
									sText.GetLength() + 1,
									LPCTSTR(sText)
									);
								::GlobalUnlock( hGlobal );
								::SetClipboardData( 
#if (defined _UNICODE)
									CF_UNICODETEXT
#else
									CF_TEXT
#endif
									, 
									hGlobal 
									);
							} // if( hGlobal != NULL )
						} // if( ::EmptyClipboard() )
						::CloseClipboard();
					} // if( OpenClipboard() )
				}
				else 
					if( ! IsReadOnly() )
					{
						if(		( nChar == 86 && bCtrl ) 
							||	( nChar == VK_INSERT && bShift ) 
							)
						{
							// paste from clipboard
							if( OpenClipboard() )
							{
								CString strClipboardText;
								bool bHaveClipboardText = false;
								if( g_PaintManager.m_bIsWinNT4orLater && ::IsClipboardFormatAvailable( CF_UNICODETEXT ) )
								{
									HGLOBAL h = ::GetClipboardData( CF_UNICODETEXT );
									if( h != NULL )
									{
										LPWSTR strUnicodeBuffer = (LPWSTR) ::GlobalLock( h );
										if( strUnicodeBuffer != NULL )
										{
											bHaveClipboardText = true;
											USES_CONVERSION;
											LPCTSTR strBuffer = W2CT(strUnicodeBuffer);
											strClipboardText = strBuffer;
											::GlobalUnlock( h );
										}
									}
								} // if( g_PaintManager.m_bIsWinNT4orLater && ::IsClipboardFormatAvailable( CF_UNICODETEXT ) )
								if( ( ! bHaveClipboardText ) && ::IsClipboardFormatAvailable( CF_TEXT ) )
								{
									HGLOBAL h = ::GetClipboardData( CF_TEXT );
									if( h != NULL )
									{
										LPSTR strBuffer = (LPSTR) ::GlobalLock( h );
										if( strBuffer != NULL )
										{
											bHaveClipboardText = true;
											strClipboardText = strBuffer;
											::GlobalUnlock( h );
										} // if( strBuffer != NULL )
									}
								} // if( ( ! bHaveClipboardText ) && ::IsClipboardFormatAvailable( CF_TEXT ) )
								if( bHaveClipboardText )
									TextSet( LPCTSTR(strClipboardText), false, false );
								::CloseClipboard();
							} // if( OpenClipboard() )
						}
					} // if( ! IsReadOnly() )
			} // if( pII != NULL )
		}
 		break;
	default:
		if( ! IsReadOnly() )
		{
			if(		nChar >= VK_NUMPAD0 
				&&	nChar <= VK_NUMPAD9
				)
				OnDigitPressed( nChar - VK_NUMPAD0 );
			else if( isdigit( nChar ) )
				OnDigitPressed( nChar - _T('0') );
			else
				CExtDurationWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		}
		else
			CExtDurationWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		return;
	} // switch( nChar )
	UpdateDurationWnd( true, true );
}

void CExtLLEditWnd::OnDigitPressed(
	UINT nDigit
	)
{
	ASSERT_VALID( this );
 	ASSERT( m_nFractionDigitCount > 0 );
	if( IsReadOnly() )
 		return;

ITEM_INFO * pII = SelectionGet();
	if( pII != NULL )
	{
		INT nValue = 0;
		if( pII == m_pLastInputItem )
			nValue = pII->m_nValue * 10 + nDigit;
		else
			nValue = nDigit;

		CExtLongitude _LongitudeCurrent;
		LongitudeGet( _LongitudeCurrent );

		CExtLatitude _LatitudeCurrent;
		LatitudeGet( _LatitudeCurrent );

		bool bLongitudeChange = false;
		bool bLatitudeChange = false;

		switch( INT(pII->m_eItemType) )
		{
		case INT(longitude_seconds_fraction):
			{
				CExtLL_t lfDiv = ::powl( 10, m_nFractionDigitCount );
				if( ! ( 0 <= nValue && nValue < INT( lfDiv ) ) )
					nValue = 0;

 				pII->m_nValue = nValue;
				CExtLL_t lfSecondsFraction = ((CExtLL_t)nValue) / lfDiv;
				_LongitudeCurrent.SecondsFractionSet( lfSecondsFraction );

				m_bBlankLongitudeSecondsFraction = false;

				bLongitudeChange = true;
			}
			break;
		case INT(longitude_seconds):
			{
				if( nValue < 0 || nValue > 59 )
					nValue = nDigit;

 				pII->m_nValue = nValue;

				if( nValue >= 0 && nValue <= 59 )
					_LongitudeCurrent.SecondsSet( nValue );

				m_bBlankLongitudeSeconds = false;

				bLongitudeChange = true;
			}
			break;
		case INT(longitude_minutes):
			{
				if( nValue < 0 || nValue > 59 )
					nValue = nDigit;

 				pII->m_nValue = nValue;

				if( nValue >= 0 && nValue <= 59 )
					_LongitudeCurrent.MinutesSet( nValue );

				m_bBlankLongitudeMinutes = false;

				bLongitudeChange = true;
			}
			break;
		case INT(longitude_degrees):
			{
				if( nValue < 0 || nValue > 180 )
					nValue = nDigit;

 				pII->m_nValue = nValue;

				if( nValue >= 0 && nValue <= 180 )
					_LongitudeCurrent.DegreesSet( nValue );

				m_bBlankLongitudeDegrees = false;

				bLongitudeChange = true;
			}
			break;

		case INT(latitude_seconds_fraction):
			{
				CExtLL_t lfDiv = ::powl( 10, m_nFractionDigitCount );
				if( ! ( 0 <= nValue && nValue < INT( lfDiv ) ) )
					nValue = 0;

 				pII->m_nValue = nValue;
				CExtLL_t lfSecondsFraction = ((CExtLL_t)nValue) / lfDiv;
				_LatitudeCurrent.SecondsFractionSet( lfSecondsFraction );

				m_bBlankLatitudeSecondsFraction = false;

				bLatitudeChange = true;
			}
			break;
		case INT(latitude_seconds):
			{
				if( nValue < 0 || nValue > 59 )
					nValue = nDigit;

 				pII->m_nValue = nValue;

				if( nValue >= 0 && nValue <= 59 )
					_LatitudeCurrent.SecondsSet( nValue );

				m_bBlankLatitudeSeconds = false;

				bLatitudeChange = true;
			}
			break;
		case INT(latitude_minutes):
			{
				if( nValue < 0 || nValue > 59 )
					nValue = nDigit;

 				pII->m_nValue = nValue;

				if( nValue >= 0 && nValue <= 59 )
					_LatitudeCurrent.MinutesSet( nValue );

				m_bBlankLatitudeMinutes = false;

				bLatitudeChange = true;
			}
			break;
		case INT(latitude_degrees):
			{
				if( nValue < 0 || nValue > 90 )
					nValue = nDigit;

 				pII->m_nValue = nValue;

				if( nValue >= 0 && nValue <= 90 )
					_LatitudeCurrent.DegreesSet( nValue );

				m_bBlankLatitudeDegrees = false;

				bLatitudeChange = true;
			}
			break;
		default:
			//ASSERT( FALSE );
			break;
		}

 		m_pLastInputItem = pII;

		if( bLongitudeChange )
			LongitudeSet( _LongitudeCurrent, false );
		else if ( bLatitudeChange )
			LatitudeSet( _LatitudeCurrent, false );

		UpdateDurationWnd( true, true );
	}
}

LRESULT CExtLLEditWnd::OnLLChangeNotification( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	lParam;
CExtLLBaseControlProperties::CHANGING_NOTIFICATION * p_cn = (CExtLLBaseControlProperties::CHANGING_NOTIFICATION *) wParam;
	ASSERT( p_cn != NULL );
//	if( ! p_cn->m_bChangedFinally )
//		return 0L;
CWnd * pWnd = CWnd::FromHandlePermanent( p_cn->m_LLBCP.OnQueryNotificationSource() );
	if( pWnd == NULL )
		return 0L;
	LatitudeSet( p_cn->m_LatitudeNew, false );
	LongitudeSet( p_cn->m_LongitudeNew, false );
	UpdateDurationWnd( true, true );
	return 0L;
}

bool CExtLLEditWnd::ShowDropDown()
{
	ASSERT_VALID( this );
	return OnShowDropDownMenu();
}

HWND CExtLLEditWnd::OnQueryNotificationSource() const
{
	ASSERT_VALID( this );
	return GetSafeHwnd();
}

HWND CExtLLEditWnd::OnQueryNotificationReceiver() const
{
	ASSERT_VALID( this );
	return CExtDurationWnd::OnQueryNotificationReceiver();
}

bool CExtLLEditWnd::TextSet(
	__EXT_MFC_SAFE_LPCTSTR strText,
	bool bUpdateNow,
	bool bParsePartialText
	)
{
	ASSERT_VALID( this );
	if(	! CExtLLBaseControlProperties::TextSet( strText, false, bParsePartialText ) )
		return false;
	UpdateDurationWnd( true, bUpdateNow );
	return true;
}

void CExtLLEditWnd::SyncSeparatorSpace()
{
	ASSERT_VALID( this );
	switch( GetMode() )
	{
	case longitude_latitude:
		_SyncSeparatorSpace_WalkRange( INT(longitude_hemisphere), INT(longitude_seconds_fraction), true,  false );
		_SyncSeparatorSpace_WalkRange( INT(latitude_hemisphere),  INT(latitude_seconds_fraction),  false, true );
	break;
	case latitude_longitude:
		_SyncSeparatorSpace_WalkRange( INT(latitude_hemisphere),  INT(latitude_seconds_fraction),  true,  false );
		_SyncSeparatorSpace_WalkRange( INT(longitude_hemisphere), INT(longitude_seconds_fraction), false, true  );
	break;
	case longitude:
		_SyncSeparatorSpace_WalkRange( INT(longitude_hemisphere), INT(longitude_seconds_fraction), true,  true  );
	break;
	case latitude:
		_SyncSeparatorSpace_WalkRange( INT(latitude_hemisphere),  INT(latitude_seconds_fraction),  true,  true  );
	break;
	} // switch( GetMode() )
}

void CExtLLEditWnd::_SyncSeparatorSpace_WalkRange(
	INT nReviewRangeStart,
	INT nReviewRangeEnd,
	bool bFirstRange,
	bool bLastRange
	)
{
	ASSERT_VALID( this );
	ASSERT( nReviewRangeStart <= nReviewRangeEnd );
	if( nReviewRangeStart == nReviewRangeEnd )
		return;
bool bLastVisibleFound = false;
INT nItemIndex = nReviewRangeEnd, nMinVisibleIndex = -1;
TCHAR strSeparator[] = { m_tchrSeparator, _T('\0') };
TCHAR strFractionSeparator[] = { m_tchrFractionSeparator, _T('\0') };
	for( ; true; )
	{
		ITEM_INFO * pII = ItemGet( (CExtDurationWnd::eItem_t) nItemIndex );
		ASSERT( pII != NULL );
		ASSERT( INT(pII->m_arrDependentItems.GetSize()) == 2 );
		CExtSafeString strBefore, strAfter;
		if( nItemIndex > nReviewRangeStart )
			strBefore = ( nItemIndex == INT(longitude_seconds_fraction) || nItemIndex == INT(latitude_seconds_fraction) ) ? strFractionSeparator : strSeparator ;
		if( ! bLastVisibleFound )
		{ 
			if( pII->m_bVisible )
			{
				bLastVisibleFound = true;
				if( bFirstRange )
					strAfter = m_strLLS;
			}
			else
			{
			}
		}
		if( pII->m_bVisible )
			nMinVisibleIndex = nItemIndex;
		pII->m_arrDependentItems[0]->m_sText = strBefore;
		pII->m_arrDependentItems[1]->m_sText = strAfter;
		if( nItemIndex == nReviewRangeStart )
			break;
		nItemIndex --;
	}
	if( bLastRange && nMinVisibleIndex >= 0 )
	{
		ITEM_INFO * pII = ItemGet( (CExtDurationWnd::eItem_t) nMinVisibleIndex );
		ASSERT( pII != NULL );
		ASSERT( INT(pII->m_arrDependentItems.GetSize()) == 2 );
		pII->m_arrDependentItems[0]->m_sText = _T("");
	}
	UpdateDurationWnd();
}

void CExtLLEditWnd::RangeLongitudeSet(
	const CExtLongitude & _LongitudeMin,
	const CExtLongitude & _LongitudeMax
	)
{
	ASSERT_VALID( this );
	CExtLLBaseControlProperties::RangeLongitudeSet( _LongitudeMin, _LongitudeMax );
	UpdateDurationWnd( true, true );
}

void CExtLLEditWnd::RangeLatitudeSet(
	const CExtLatitude & _LatitudeMin,
	const CExtLatitude & _LatitudeMax
	)
{
	ASSERT_VALID( this );
	CExtLLBaseControlProperties::RangeLatitudeSet( _LatitudeMin, _LatitudeMax );
	UpdateDurationWnd( true, true );
}

void CExtLLEditWnd::LongitudeSet(
	const CExtLongitude & _Longitude,
	bool bUpdateNow
	)
{
	ASSERT_VALID( this );
	CExtLLBaseControlProperties::LongitudeSet( _Longitude, false );
	UpdateDurationWnd( true, bUpdateNow );
}

void CExtLLEditWnd::LatitudeSet(
	const CExtLatitude & _Latitude,
	bool bUpdateNow
	)
{
	ASSERT_VALID( this );
	CExtLLBaseControlProperties::LatitudeSet( _Latitude, false );
	UpdateDurationWnd( true, bUpdateNow );
}

/////////////////////////////////////////////////////////////////////////////
// CExtLLMapWnd window

IMPLEMENT_DYNCREATE( CExtLLMapWnd, CWnd );
IMPLEMENT_CExtPmBridge_MEMBERS( CExtLLMapWnd );

bool CExtLLMapWnd::g_bWndClassRegistered = false;

CExtLLMapWnd::CExtLLMapWnd()
	: m_hWndNotificationReceiver( NULL )
	, m_bDirectCreateCall( false )
	, m_bInitialized( false )
	, m_bCanceling( false )
	, m_bAutoDeleteWindow( false )
	, m_eMTM( CExtLLMapWnd::__EMTM_NONE )
	, m_clrLocationSelected( RGB(255,255,164) )
	, m_clrShadowSelected( RGB(80,80,80) )
	, m_clrLocationHover( RGB(255,255,255) )
	, m_clrShadowHover( RGB(80,80,80) )
	, m_clrLocationPressed( RGB(192,192,80) )
	, m_clrShadowPressed( RGB(40,40,40) )
	, m_ptHover( -32767, -32767 )
	, m_ptPressed( -32767, -32767 )
	, m_ptShadowOffset( 1, 1 )
	, m_nMarkerRadiusMin( 2 )
	, m_nMarkerRadiusMax( 4 )
	, m_nMarkerRadiusSnap( 5 )
	, m_nMarkerRadiusDivider( 150 )
	, m_clrMarker( RGB(255,255,150) )
	, m_lfMarkerMutliplierLighter( +0.45 )
	, m_lfMarkerMutliplierDarker ( -0.45 )
	, m_nAdvancedTipStyle( INT(CExtPopupMenuTipWnd::__ETS_RECTANGLE_NO_ICON) )
{
	VERIFY( RegisterWndClass() );
	PmBridge_Install();
}

CExtLLMapWnd::~CExtLLMapWnd()
{
	PmBridge_Uninstall();
	WorldMapEmpty();
}

void CExtLLMapWnd::WorldMapLoad(
	bool bHR,
	bool bGenerateTZ, // = true
	bool bRedraw, // = true
	bool bUpdateNow // = true
	)
{
	ASSERT_VALID( this );
	WorldMapEmpty();
	VERIFY( m_bmpMap.LoadBMP_Resource( bHR ? (MAKEINTRESOURCE(IDB_EXT_BMP_LL_MAP_HR)) : (MAKEINTRESOURCE(IDB_EXT_BMP_LL_MAP_LR)) ) );
	if( bGenerateTZ )
	{
		VERIFY( m_bmpTZ.LoadBMP_Resource( bHR ? (MAKEINTRESOURCE(IDB_EXT_BMP_LL_HTTZ_HR)) : (MAKEINTRESOURCE(IDB_EXT_BMP_LL_HTTZ_LR)) ) );
		VERIFY( m_bmpTZ.Make32() );
		OnLLMapWorldMapInit( m_nTzHtTransparencySelected, m_nTzHtTransparencyNormal );
	}
	if( bRedraw && GetSafeHwnd() != NULL )
	{
		Invalidate();
		if( bUpdateNow )
			UpdateWindow();
	}
}

void CExtLLMapWnd::WorldMapEmpty()
{
	ASSERT_VALID( this );
	OnLLMapHtWorldMapClear();
	m_bmpTZ.Empty();
	m_bmpMap.Empty();
}

void CExtLLMapWnd::OnLLMapWorldMapInit(
	BYTE nTransparencySelected,
	BYTE nTransparencyNormal
	)
{
	ASSERT_VALID( this );
	OnLLMapHtWorldMapClear();
	m_bmpHTTZ = m_bmpTZ;
	if( m_bmpHTTZ.IsEmpty() )
		return;
CWaitCursor _waitCursor;
INT nX, nY;
CSize sizeTZ = m_bmpHTTZ.GetSize();
	for( nY = 0; nY < sizeTZ.cy; nY++ )
	{
		for( nX = 0; nX < sizeTZ.cx; nX++ )
		{
			RGBQUAD _pixel;
			VERIFY( m_bmpHTTZ.GetPixel( nX, nY, _pixel ) );
			_pixel.rgbReserved = nTransparencyNormal;
			VERIFY( m_bmpHTTZ.SetPixel( nX, nY, _pixel ) );
		}
	}

	for( nY = 0; nY < sizeTZ.cy; nY++ )
	{
		for( nX = 0; nX < sizeTZ.cx; nX++ )
		{
			RGBQUAD _pixel;
			VERIFY( m_bmpTZ.GetPixel( nX, nY, _pixel ) );
			COLORREF clr = RGB( _pixel.rgbRed,_pixel.rgbGreen,_pixel.rgbBlue );
			WORLD_TIME_ZONE * pWTZ = NULL;
			if( m_mapWorldTimeZones.Lookup( clr, pWTZ ) )
			{
				ASSERT( pWTZ != NULL );
				continue;
			}
			ASSERT( pWTZ == NULL );
			pWTZ = new WORLD_TIME_ZONE;
			m_mapWorldTimeZones.SetAt( clr, pWTZ );
		}
	}

	//TRACE( _T("\r\n\r\n") );
POSITION pos = m_mapWorldTimeZones.GetStartPosition();
	for( ; pos != NULL; )
	{
		COLORREF clr;
		WORLD_TIME_ZONE * pWTZ = NULL;
		m_mapWorldTimeZones.GetNextAssoc( pos, clr, pWTZ );
		ASSERT( pWTZ != NULL );
		pWTZ->m_clr = clr;
#ifdef _DEBUG
		pWTZ->m_lfHoursShift = -100.0;
#endif // _DEBUG
		pWTZ->m_bmp = m_bmpHTTZ;
		pWTZ->m_bmp.AlphaColor( clr, RGB(0,0,0), nTransparencySelected );
		//TRACE3( _T("%d,%d,%d\r\n"), GetRValue(clr), GetGValue(clr), GetBValue(clr) );
	}
	//TRACE1( _T("\r\n===============\r\nWTZ count is %d\r\n\r\n"), m_mapWorldTimeZones.GetCount() );

static const struct  
{
	COLORREF m_clr;
	double m_lfHoursShift;
}
g_arrInitColors[] =
{
	{ RGB(247,245,110), - 11.00 },
	{ RGB(190,218,115), +  5.50 },
	{ RGB(228, 66,100), +  4.50 },
	{ RGB(213,124,194), +  8.00 },
	{ RGB(114,138,241), + 10.50 },
	{ RGB(192,192,192), -  9.50 },
	{ RGB(127,166,230), +  6.00 },
	{ RGB(221,150,150), +  4.00 },
	{ RGB(219,124,124), + 10.00 },
	{ RGB(247,193,108), +  5.00 },
	{ RGB(223,127,205), - 12.00 },
	{ RGB(219,168, 96), + 11.00 },
	{ RGB(101,123,219),    0.00 },
	{ RGB(118,213,146), + 11.50 },
	{ RGB(141,140,101), + 13.00 },
	{ RGB(141,242,101), +  3.50 },
	{ RGB(245,219,182), -  3.50 },
	{ RGB(134,240,120), -  3.00 },
	{ RGB(255,132,132), -  2.00 },
	{ RGB(128,126,181), +  6.50 },
	{ RGB(132,159, 80), +  5.75 },
	{ RGB(127,184,122), +  3.00 },
	{ RGB(251,138,244), +  2.00 },
	{ RGB(219, 96, 96), + 12.00 },
	{ RGB(254, 80, 80), -  8.00 },
	{ RGB(230,181,112), -  1.00 },
	{ RGB(230,105,238), -  4.00 },
	{ RGB(233,182,110), - 12.00 },
	{ RGB(131,162, 72), +  8.50 },
	{ RGB(148,232,122), -  8.50 },
	{ RGB(226,230,112), +  1.00 },
	{ RGB(135,179,123), -  9.00 },
	{ RGB(245,106,237), - 10.00 },
	{ RGB(255,250,108), -  5.00 },
	{ RGB(254,193, 80), -  7.00 },
	{ RGB( 91,169, 93), +  9.00 },
	{ RGB(214,203, 84), +  7.00 },
	{ RGB(143,178,239), -  6.00 },
};

INT nIndex, nCount = sizeof( g_arrInitColors ) / sizeof( g_arrInitColors[0] );
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		WORLD_TIME_ZONE * pWTZ = NULL;
		if( ! m_mapWorldTimeZones.Lookup( g_arrInitColors[ nIndex ].m_clr, pWTZ ) )
		{
			ASSERT( FALSE );
			continue;
		}
		pWTZ->m_lfHoursShift = g_arrInitColors[ nIndex ].m_lfHoursShift;
	}

#ifdef _DEBUG
	pos = m_mapWorldTimeZones.GetStartPosition();
	for( ; pos != NULL; )
	{
		COLORREF clr;
		WORLD_TIME_ZONE * pWTZ = NULL;
		m_mapWorldTimeZones.GetNextAssoc( pos, clr, pWTZ );
		ASSERT( pWTZ != NULL );
		if( pWTZ->m_lfHoursShift == -100.0 )
		{
			TRACE3( "Bad color = %d,%d,%d\r\n", GetRValue(clr), GetGValue(clr), GetBValue(clr) );
			for( nY = 0; nY < sizeTZ.cy; nY++ )
			{
				for( nX = 0; nX < sizeTZ.cx; nX++ )
				{
					RGBQUAD _pixel;
					VERIFY( m_bmpHTTZ.GetPixel( nX, nY, _pixel ) );
					COLORREF clrTest = RGB(_pixel.rgbRed,_pixel.rgbGreen,_pixel.rgbBlue);
					if( clrTest == clr )
					{
						TRACE2("             bad pixel location = %d,%d\r\n", nX, nY );
					}
				}
			}

		}
	}
#endif // _DEBUG

}

void CExtLLMapWnd::OnLLMapHtWorldMapClear()
{
	ASSERT_VALID( this );
	m_bmpHTTZ.Empty();
POSITION pos = m_mapWorldTimeZones.GetStartPosition();
	for( ; pos != NULL; )
	{
		COLORREF clr;
		WORLD_TIME_ZONE * pWTZ = NULL;
		m_mapWorldTimeZones.GetNextAssoc( pos, clr, pWTZ );
		ASSERT( pWTZ != NULL );
		delete pWTZ;
	}
	m_mapWorldTimeZones.RemoveAll();
}

BEGIN_MESSAGE_MAP(CExtLLMapWnd, CWnd)
	//{{AFX_MSG_MAP(CExtLLMapWnd)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_CANCELMODE()
	ON_WM_CAPTURECHANGED()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEWHEEL()
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
	ON_WM_ACTIVATEAPP()
END_MESSAGE_MAP()

#ifdef _DEBUG
void CExtLLMapWnd::AssertValid() const
{
	CWnd::AssertValid();
}
void CExtLLMapWnd::Dump(CDumpContext& dc) const
{
	CWnd::Dump( dc );
}
#endif // _DEBUG

INT CExtLLMapWnd::LongitudeToClient( const CExtLongitude & _Longitude ) const
{
	ASSERT_VALID( this );
	if( ! _Longitude.IsNormalized() )
	{
		CExtLongitude _Longitude2( _Longitude, true );
		return LongitudeToClient( _Longitude2 );
	}
CExtLL_t lfRange = CExtLongitude::stat_ll_get_value_in_seconds_max() - CExtLongitude::stat_ll_get_value_in_seconds_min();
	ASSERT( lfRange > 0.0 );
CExtLL_t lfAlign = _Longitude.m_lf - CExtLongitude::stat_ll_get_value_in_seconds_min();
	if( lfAlign < 0.0 )
		lfAlign = 0.0;
	if( lfAlign > lfRange )
		lfAlign = lfRange;
CRect rcClient;
	GetClientRect( &rcClient );
INT nExtent = rcClient.Width();
CExtLL_t lfExtent = CExtLL_t( nExtent );
CExtLL_t lfClient = lfExtent * lfAlign / lfRange;
INT nX = INT( lfClient );
	return nX;
}

void CExtLLMapWnd::ClientToLongitude( INT nX, CExtLongitude & _Longitude ) const
{
	ASSERT_VALID( this );
CExtLL_t lfRange = CExtLongitude::stat_ll_get_value_in_seconds_max() - CExtLongitude::stat_ll_get_value_in_seconds_min();
	ASSERT( lfRange > 0.0 );
CRect rcClient;
	GetClientRect( &rcClient );
INT nExtent = rcClient.Width();
CExtLL_t lfExtent = CExtLL_t( nExtent );
CExtLL_t lfClient = CExtLL_t( nX );
CExtLL_t lfAlign = lfClient * lfRange / lfExtent;
	_Longitude.m_lf = lfAlign + CExtLongitude::stat_ll_get_value_in_seconds_min();
}

CExtLongitude CExtLLMapWnd::ClientToLongitude( INT nX ) const
{
	ASSERT_VALID( this );
CExtLongitude _Longitude;
	ClientToLongitude( nX, _Longitude );
	return _Longitude;
}

void CExtLLMapWnd::MapToClient( const CExtLongitude & _Longitude, const CExtLatitude & _Latitude, POINT & ptClient ) const
{
	ASSERT_VALID( this );
	ptClient.x = LongitudeToClient( _Longitude );
	ptClient.y = LatitudeToClient( _Latitude );
}

CPoint CExtLLMapWnd::MapToClientPoint( const CExtLongitude & _Longitude, const CExtLatitude & _Latitude ) const
{
	ASSERT_VALID( this );
	return CPoint( LongitudeToClient( _Longitude ), LatitudeToClient( _Latitude ) );
}

void CExtLLMapWnd::ClientToMap( const POINT & ptClient, CExtLongitude & _Longitude, CExtLatitude & _Latitude ) const
{
	ASSERT_VALID( this );
	ClientToLongitude( ptClient.x, _Longitude );
	ClientToLatitude(  ptClient.y, _Latitude );
}

void CExtLLMapWnd::MapToClient( const CExtLongitude & _Longitude, const CExtLatitude & _Latitude, SIZE & sizeClient ) const
{
	ASSERT_VALID( this );
	sizeClient.cx = LongitudeToClient( _Longitude );
	sizeClient.cy = LatitudeToClient( _Latitude );
}

CSize CExtLLMapWnd::MapToClientSize( const CExtLongitude & _Longitude, const CExtLatitude & _Latitude ) const
{
	ASSERT_VALID( this );
	return CSize( LongitudeToClient( _Longitude ), LatitudeToClient( _Latitude ) );
}

void CExtLLMapWnd::ClientToMap( const SIZE & sizeClient, CExtLongitude & _Longitude, CExtLatitude & _Latitude ) const
{
	ASSERT_VALID( this );
	ClientToLongitude( sizeClient.cx, _Longitude );
	ClientToLatitude(  sizeClient.cy, _Latitude );
}

void CExtLLMapWnd::MapToClient(
	const CExtLongitude & _LongitudeMin,
	const CExtLongitude & _LongitudeMax,
	const CExtLatitude & _LatitudeMin,
	const CExtLatitude & _LatitudeMax,
	RECT & rcClient
	)
{
	ASSERT_VALID( this );
	rcClient.left   = LongitudeToClient( _LongitudeMin	);
	rcClient.right  = LongitudeToClient( _LongitudeMax	);
	rcClient.top    = LatitudeToClient(  _LatitudeMin	);
	rcClient.bottom = LatitudeToClient(  _LatitudeMax	);
}

CRect CExtLLMapWnd::MapToClientRect(
	const CExtLongitude & _LongitudeMin,
	const CExtLongitude & _LongitudeMax,
	const CExtLatitude & _LatitudeMin,
	const CExtLatitude & _LatitudeMax
	)
{
	ASSERT_VALID( this );
CRect rcClient;
	MapToClient( _LongitudeMin, _LongitudeMax, _LatitudeMin, _LatitudeMax, rcClient );
	return rcClient;
}

void CExtLLMapWnd::ClientToMap(
	const RECT & rcClient,
	CExtLongitude & _LongitudeMin,
	CExtLongitude & _LongitudeMax,
	CExtLatitude & _LatitudeMin,
	CExtLatitude & _LatitudeMax
	)
{
	ASSERT_VALID( this );
	ClientToLongitude( rcClient.left,   _LongitudeMin );
	ClientToLongitude( rcClient.right,  _LongitudeMax );
	ClientToLatitude(  rcClient.top,    _LatitudeMin  );
	ClientToLatitude(  rcClient.bottom, _LatitudeMax  );
}

INT CExtLLMapWnd::LatitudeToClient( const CExtLatitude & _Latitude ) const
{
	ASSERT_VALID( this );
	if( ! _Latitude.IsNormalized() )
	{
		CExtLatitude _Latitude2( _Latitude, true );
		return LatitudeToClient( _Latitude2 );
	}
CExtLL_t lfRange = CExtLatitude::stat_ll_get_value_in_seconds_max() - CExtLatitude::stat_ll_get_value_in_seconds_min();
	ASSERT( lfRange > 0.0 );
CExtLL_t lfAlign = _Latitude.m_lf - CExtLatitude::stat_ll_get_value_in_seconds_min();
	if( lfAlign < 0.0 )
		lfAlign = 0.0;
	if( lfAlign > lfRange )
		lfAlign = lfRange;
	lfAlign = lfRange - lfAlign;
CRect rcClient;
	GetClientRect( &rcClient );
INT nExtent = rcClient.Height();
CExtLL_t lfExtent = CExtLL_t( nExtent );
CExtLL_t lfClient = lfExtent * lfAlign / lfRange;
INT nY = INT( lfClient );
	return nY;
}

void CExtLLMapWnd::ClientToLatitude( INT nY, CExtLatitude & _Latitude ) const
{
	ASSERT_VALID( this );
CExtLL_t lfRange = CExtLatitude::stat_ll_get_value_in_seconds_max() - CExtLatitude::stat_ll_get_value_in_seconds_min();
	ASSERT( lfRange > 0.0 );
CRect rcClient;
	GetClientRect( &rcClient );
INT nExtent = rcClient.Height();
CExtLL_t lfExtent = CExtLL_t( nExtent );
CExtLL_t lfClient = CExtLL_t( nY );
CExtLL_t lfAlign = lfClient * lfRange / lfExtent;
	lfAlign = lfRange - lfAlign;
	_Latitude.m_lf = lfAlign + CExtLatitude::stat_ll_get_value_in_seconds_min();
}

CExtLatitude CExtLLMapWnd::ClientToLatitude( INT nY ) const
{
	ASSERT_VALID( this );
CExtLatitude _Latitude;
	ClientToLatitude( nY, _Latitude );
	return _Latitude;
}

bool CExtLLMapWnd::RegisterWndClass()
{
	if( g_bWndClassRegistered )
		return true;
WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo( hInst, __EXT_LONGITUDE_LATITUDE_MAP_WND_CLASS_NAME, &_wndClassInfo ) )
	{
		_wndClassInfo.style = CS_GLOBALCLASS|CS_DBLCLKS;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor = ::LoadCursor( NULL, IDC_ARROW );
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL;
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_LONGITUDE_LATITUDE_MAP_WND_CLASS_NAME;
		if( !::AfxRegisterClass( & _wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}
	g_bWndClassRegistered = true;
	return true;
}

bool CExtLLMapWnd::_CreateHelper()
{
	ASSERT_VALID( this );
	if( m_bInitialized )
		return true;
	Update( false );
	return true;
}

bool CExtLLMapWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
	UINT nDlgCtrlID, // = UINT( __EXT_MFC_IDC_STATIC )
	DWORD dwWindowStyle, // = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN
	CCreateContext * pContext // = NULL
	)
{
	ASSERT_VALID( this );
	if( ! RegisterWndClass() )
	{
		ASSERT( FALSE );
		return false;
	}
	m_bDirectCreateCall = true;
	if( ! CWnd::Create(
			__EXT_LONGITUDE_LATITUDE_MAP_WND_CLASS_NAME,
			NULL,
			dwWindowStyle,
			rcWnd,
			pParentWnd,
			nDlgCtrlID,
			pContext
			)
		)
	{
		ASSERT( FALSE );
		return false;
	}
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		AfxThrowMemoryException();
	} // if( ! _CreateHelper() )
	m_bInitialized = true;
	Update( false );
	return true;
}

bool CExtLLMapWnd::TextSet(
	__EXT_MFC_SAFE_LPCTSTR strText,
	bool bUpdateNow,
	bool bParsePartialText
	)
{
	ASSERT_VALID( this );
	if(	! CExtLLBaseControlProperties::TextSet( strText, false, bParsePartialText ) )
		return false;
	Update( bUpdateNow );
	return true;
}

void CExtLLMapWnd::Update(
	bool bUpdateNow
	)
{
	ASSERT_VALID( this );
	if(		GetSafeHwnd() == NULL
		||	( ! m_bInitialized )
		)
		return;
	Invalidate();
	if( bUpdateNow )
		UpdateWindow();
}

HWND CExtLLMapWnd::OnQueryNotificationSource() const
{
	ASSERT_VALID( this );
	return GetSafeHwnd();
}

HWND CExtLLMapWnd::OnQueryNotificationReceiver() const
{
	ASSERT_VALID( this );
	if(		m_hWndNotificationReceiver != NULL
		&&	::IsWindow( m_hWndNotificationReceiver )
		)
		return m_hWndNotificationReceiver;
	if( GetSafeHwnd() == NULL )
		return NULL;
HWND hWndNotificationReceiver = ::GetParent( m_hWnd );
	return hWndNotificationReceiver;
}

void CExtLLMapWnd::SetMode( CExtLLBaseControlProperties::eMode_t eMode )
{
	ASSERT_VALID( this );
	CExtLLBaseControlProperties::SetMode( eMode );
	Update( false );
}

CExtLLBaseControlProperties::eMode_t CExtLLMapWnd::GetMode() const
{
	ASSERT_VALID( this );
	return CExtLLBaseControlProperties::GetMode();
}

void CExtLLMapWnd::RangeLongitudeSet(
	const CExtLongitude & _LongitudeMin,
	const CExtLongitude & _LongitudeMax
	)
{
	ASSERT_VALID( this );
	CExtLLBaseControlProperties::RangeLongitudeSet( _LongitudeMin, _LongitudeMax );
	Update( false );
}

void CExtLLMapWnd::RangeLatitudeSet(
	const CExtLatitude & _LatitudeMin,
	const CExtLatitude & _LatitudeMax
	)
{
	ASSERT_VALID( this );
	CExtLLBaseControlProperties::RangeLatitudeSet( _LatitudeMin, _LatitudeMax );
	Update( false );
}

void CExtLLMapWnd::LongitudeSet(
	const CExtLongitude & _Longitude,
	bool bUpdateNow
	)
{
	ASSERT_VALID( this );
	CExtLLBaseControlProperties::LongitudeSet( _Longitude, false );
	Update( bUpdateNow );
}

void CExtLLMapWnd::LatitudeSet(
	const CExtLatitude & _Latitude,
	bool bUpdateNow
	)
{
	ASSERT_VALID( this );
	CExtLLBaseControlProperties::LatitudeSet( _Latitude, false );
	Update( bUpdateNow );
}

BOOL CExtLLMapWnd::PreCreateWindow(CREATESTRUCT& cs) 
{
	ASSERT_VALID( this );
	if( !CWnd::PreCreateWindow(cs) )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	cs.style &= ~(WS_BORDER);
	return TRUE;
}

void CExtLLMapWnd::PreSubclassWindow() 
{
	CWnd::PreSubclassWindow();
	if( m_bDirectCreateCall )
		return;
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		AfxThrowMemoryException();
	} // if( ! _CreateHelper() )
	m_bInitialized = true;
	Update( false );
}

void CExtLLMapWnd::PostNcDestroy() 
{
	if( m_bAutoDeleteWindow )
		delete this;
}

LRESULT CExtLLMapWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if( message == WM_PRINT || message == WM_PRINTCLIENT )
	{
		CDC * pDC = CDC::FromHandle( (HDC)wParam );
		OnLLMapDrawEntire( *pDC );
		return (!0);
	}
	return CWnd::WindowProc(message, wParam, lParam);
}

BOOL CExtLLMapWnd::OnEraseBkgnd(CDC * pDC) 
{
	ASSERT_VALID( this );
	pDC;
	return TRUE;
}

void CExtLLMapWnd::OnPaint() 
{
	ASSERT_VALID( this );
CPaintDC dcPaint( this );
CRect rcClient;
GetClientRect( &rcClient );
	if( rcClient.IsRectEmpty() )
		return;
CExtMemoryDC dc( &dcPaint, &rcClient );
	OnLLMapDrawEntire( dc );
	PmBridge_GetPM()->OnPaintSessionComplete( this );
}

CRect CExtLLMapWnd::OnLLMapPaintMarginsRect() const
{
	ASSERT_VALID( this );
CRect rcClient;
	GetClientRect( &rcClient );
CRect rcPaintMargins = rcClient;
	rcPaintMargins.DeflateRect( 0, 0, 1, 1 );
	return rcPaintMargins;
}

CPoint CExtLLMapWnd::OnLLMapGetSelectionPoint() const
{
	ASSERT_VALID( this );
CExtLongitude _Longitude;
	LongitudeGet( _Longitude );
CExtLatitude _Latitude;
	LatitudeGet( _Latitude );
CPoint ptSelection = MapToClientPoint( _Longitude, _Latitude );
CRect rcPaintMargins = OnLLMapPaintMarginsRect();
CPoint ptAligned = ptSelection; 
	ptAligned.x = max( ptAligned.x, rcPaintMargins.left );
	ptAligned.y = max( ptAligned.y, rcPaintMargins.top );
	ptAligned.x = min( ptAligned.x, rcPaintMargins.right );
	ptAligned.y = min( ptAligned.y, rcPaintMargins.bottom );
	if( ptAligned.x != ptSelection.x )
		ptAligned.x = -32767;
	if( ptAligned.y != ptSelection.y )
		ptAligned.y = -32767;
	return ptAligned;
}

CPoint CExtLLMapWnd::OnLLMapGetHoverPoint() const
{
	ASSERT_VALID( this );
CPoint ptAligned = m_ptHover;
CRect rcPaintMargins = OnLLMapPaintMarginsRect();
	ptAligned.x = max( ptAligned.x, rcPaintMargins.left );
	ptAligned.y = max( ptAligned.y, rcPaintMargins.top );
	ptAligned.x = min( ptAligned.x, rcPaintMargins.right );
	ptAligned.y = min( ptAligned.y, rcPaintMargins.bottom );
	if( ptAligned.x != m_ptHover.x )
		ptAligned.x = -32767;
	if( ptAligned.y != m_ptHover.y )
		ptAligned.y = -32767;
	return ptAligned;
}

CPoint CExtLLMapWnd::OnLLMapGetPressedPoint() const
{
	ASSERT_VALID( this );
CPoint ptAligned = m_ptPressed;
CRect rcPaintMargins = OnLLMapPaintMarginsRect();
	ptAligned.x = max( ptAligned.x, rcPaintMargins.left );
	ptAligned.y = max( ptAligned.y, rcPaintMargins.top );
	ptAligned.x = min( ptAligned.x, rcPaintMargins.right );
	ptAligned.y = min( ptAligned.y, rcPaintMargins.bottom );
	if( ptAligned.x != m_ptPressed.x )
		ptAligned.x = -32767;
	if( ptAligned.y != m_ptPressed.y )
		ptAligned.y = -32767;
	return ptAligned;
}

INT CExtLLMapWnd::OnLLMapCalcMarkerRadius() const
{
	ASSERT_VALID( this );
CRect rcClient;
	GetClientRect( &rcClient );
INT nMarkerRadius = min( rcClient.Width(), rcClient.Height() );
	nMarkerRadius /= m_nMarkerRadiusDivider;
	nMarkerRadius = min( nMarkerRadius, m_nMarkerRadiusMax );
	nMarkerRadius = max( nMarkerRadius, m_nMarkerRadiusMin );
	return nMarkerRadius;
}

void CExtLLMapWnd::OnLLMapDrawEntire(
	CDC & dc
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
CRect rcClient;
	GetClientRect( &rcClient );
	if( ! dc.RectVisible( rcClient ) )
		return;
	OnLLMapDrawMap( dc );
	OnLLMapDrawMarkers( dc );
bool bDrawHorizontal = false, bDrawVertical = false;
	switch( GetMode() )
	{
	case longitude_latitude:
	case latitude_longitude:
		bDrawHorizontal = true;
		bDrawVertical = true;
	break;
	case longitude:
		bDrawVertical = true;
	break;
	case latitude:
		bDrawHorizontal = true;
	break;
	} // switch( GetMode() )
	OnLLMapDrawSelection( dc, m_clrLocationSelected, m_clrShadowSelected, bDrawHorizontal, bDrawVertical );
	OnLLMapDrawHover( dc, m_clrLocationHover, m_clrShadowHover, bDrawHorizontal, bDrawVertical );
	OnLLMapDrawPressed( dc, m_clrLocationPressed, m_clrShadowPressed, bDrawHorizontal, bDrawVertical );
}

void CExtLLMapWnd::OnLLMapDrawMarkers(
	CDC & dc
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( ! m_bMapDisplayMarkers )
		return;
CArray < MAP_MARKER, MAP_MARKER & > & arrMapMarkers = OnQueryMapMarkers();
INT nMakerIndex, nMakerCount = INT( arrMapMarkers.GetSize() );
	if( nMakerCount == 0 )
		return;
COLORREF clrEllipse0 = m_clrMarker;
COLORREF clrEllipse1 = CExtPaintManager::stat_HLS_Adjust( clrEllipse0, 0.0, m_lfMarkerMutliplierLighter, 0.0 );
COLORREF clrEllipse2 = CExtPaintManager::stat_HLS_Adjust( clrEllipse0, 0.0, m_lfMarkerMutliplierDarker,  0.0 );
	clrEllipse0 = dc.GetNearestColor( clrEllipse0 );
INT nMarkerRadius = OnLLMapCalcMarkerRadius();
CBrush _brush( clrEllipse0 );
CBrush * pOldBrush = dc.SelectObject( &_brush );
	for( nMakerIndex = 0; nMakerIndex < nMakerCount; nMakerIndex ++ )
	{
		MAP_MARKER & _marker = arrMapMarkers.ElementAt( nMakerIndex );
		CPoint ptMarker = MapToClientPoint( _marker.m_Longitude, _marker.m_Latitude );
		if( _marker.m_clr != COLORREF(-1L) )
		{
			COLORREF clrEllipse0 = _marker.m_clr;
			COLORREF clrEllipse1 = CExtPaintManager::stat_HLS_Adjust( clrEllipse0, 0.0, m_lfMarkerMutliplierLighter, 0.0 );
			COLORREF clrEllipse2 = CExtPaintManager::stat_HLS_Adjust( clrEllipse0, 0.0, m_lfMarkerMutliplierDarker,  0.0 );
			clrEllipse0 = dc.GetNearestColor( clrEllipse0 );
			CBrush _brush( clrEllipse0 );
			CBrush * pOldBrush = dc.SelectObject( &_brush );
			dc.Ellipse( ptMarker.x - nMarkerRadius, ptMarker.y - nMarkerRadius, ptMarker.x + nMarkerRadius + 1, ptMarker.y + nMarkerRadius + 1 );
			CExtPaintManager::stat_PaintShadedEllipse( dc.m_hDC, ptMarker, nMarkerRadius, nMarkerRadius, clrEllipse1, clrEllipse2 );
			dc.SelectObject( pOldBrush );
		}
		else
		{
			dc.Ellipse( ptMarker.x - nMarkerRadius, ptMarker.y - nMarkerRadius, ptMarker.x + nMarkerRadius + 1, ptMarker.y + nMarkerRadius + 1 );
			CExtPaintManager::stat_PaintShadedEllipse( dc.m_hDC, ptMarker, nMarkerRadius, nMarkerRadius, clrEllipse1, clrEllipse2 );
		}
	}
	dc.SelectObject( pOldBrush );
}

void CExtLLMapWnd::OnLLMapDrawMap(
	CDC & dc
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( m_bmpMap.IsEmpty() )
		WorldMapLoad( m_bMapHR, true, false, false );
CRect rcClient;
	GetClientRect( &rcClient );
	if( ! m_bmpMap.IsEmpty() )
	{
		int nOldStretchBltMode = ::GetStretchBltMode( dc.m_hDC );
		::SetStretchBltMode( dc.m_hDC, g_PaintManager.m_bIsWinNT ? HALFTONE : COLORONCOLOR );
		CSize sizeBmp = m_bmpMap.GetSize();
		CRect rcSrc( 0, 0, sizeBmp.cx, sizeBmp.cy );
		m_bmpMap.Draw( dc.m_hDC, rcClient, rcSrc );
		::SetStretchBltMode( dc.m_hDC, nOldStretchBltMode );
	}
	if( m_bMapHighlightTimeZones )
	{
		if( m_bmpTZ.IsEmpty() )
			WorldMapLoad( m_bMapHR, true, false, false );
		if( ! m_bmpTZ.IsEmpty() )
		{
			CPoint ptTZHT = ( m_eMTM == __EMTM_PRESSED ) ? m_ptPressed : m_ptHover;
			COLORREF clrTZ = OnLLMapHitTestTimeZoneColor( ptTZHT );
			if( clrTZ != COLORREF(-1L) )
			{
				int nOldStretchBltMode = ::GetStretchBltMode( dc.m_hDC );
				::SetStretchBltMode( dc.m_hDC, g_PaintManager.m_bIsWinNT ? HALFTONE : COLORONCOLOR );
				WORLD_TIME_ZONE * pWTZ = NULL;
				if( m_mapWorldTimeZones.Lookup( clrTZ, pWTZ ) )
				{
					ASSERT( pWTZ != NULL );
					pWTZ->m_bmp.AlphaBlend( dc.m_hDC, rcClient );
				}
				::SetStretchBltMode( dc.m_hDC, nOldStretchBltMode );
			}
		}
	}
}

void CExtLLMapWnd::OnLLMapDrawSelection(
	CDC & dc,
	COLORREF clrLocation,
	COLORREF clrShadow,
	bool bDrawHorizontal,
	bool bDrawVertical
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( ! m_bMapShowSelectionLines )
		return;
CPoint ptAligned = OnLLMapGetSelectionPoint();
	if( ptAligned.x == -32767 )
		bDrawVertical = false;
	if( ptAligned.y == -32767 )
		bDrawHorizontal = false;
	OnLLMapDrawLocationLines( dc, ptAligned, m_ptShadowOffset,clrLocation, clrShadow, bDrawHorizontal, bDrawVertical );
}

void CExtLLMapWnd::OnLLMapDrawHover(
	CDC & dc,
	COLORREF clrLocation,
	COLORREF clrShadow,
	bool bDrawHorizontal,
	bool bDrawVertical
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( m_eMTM != __EMTM_HOVER )
		return;
	if( ! m_bMapShowHoverLines )
		return;
	CPoint ptAligned = OnLLMapGetHoverPoint();
	if( ptAligned.x == -32767 )
		bDrawVertical = false;
	if( ptAligned.y == -32767 )
		bDrawHorizontal = false;
	OnLLMapDrawLocationLines( dc, ptAligned, m_ptShadowOffset, clrLocation, clrShadow, bDrawHorizontal, bDrawVertical );
}

void CExtLLMapWnd::OnLLMapDrawPressed(
	CDC & dc,
	COLORREF clrLocation,
	COLORREF clrShadow,
	bool bDrawHorizontal,
	bool bDrawVertical
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( m_eMTM != __EMTM_PRESSED || m_bMapChangeSelectionWhenPressed )
		return;
CPoint ptAligned = OnLLMapGetPressedPoint();
	if( ptAligned.x == -32767 )
		bDrawVertical = false;
	if( ptAligned.y == -32767 )
		bDrawHorizontal = false;
	OnLLMapDrawLocationLines( dc, ptAligned, m_ptShadowOffset, clrLocation, clrShadow, bDrawHorizontal, bDrawVertical );
}

void CExtLLMapWnd::OnLLMapDrawLocationLines(
	CDC & dc,
	CPoint ptAligned,
	CPoint ptShadowOffset,
	COLORREF clrLocation,
	COLORREF clrShadow,
	bool bDrawHorizontal,
	bool bDrawVertical
	)
{
	ASSERT_VALID( this );
	if( ! ( bDrawHorizontal || bDrawVertical ) )
		return;
	if( clrLocation == COLORREF(-1L) && clrShadow == COLORREF(-1L) )
		return;
CRect rcClient;
	GetClientRect( &rcClient );
	if( clrShadow != COLORREF(-1L) )
	{
		CPoint ptShadow = ptAligned + ptShadowOffset;
		if( ptShadow != ptAligned )
		{
			CPen _pen( PS_SOLID, 1, clrShadow );
			CPen * pOldPen = dc.SelectObject( &_pen );
			if( bDrawVertical && ptShadow.x != ptAligned.x )
			{
				dc.MoveTo( ptShadow.x, rcClient.top    );
				dc.LineTo( ptShadow.x, rcClient.bottom );
			}
			if( bDrawHorizontal && ptShadow.y != ptAligned.y )
			{
				dc.MoveTo( rcClient.left,  ptShadow.y  );
				dc.LineTo( rcClient.right, ptShadow.y  );
			}
			dc.SelectObject( pOldPen );
		}
	}
	if( clrLocation != COLORREF(-1L) )
	{
		CPen _pen( PS_SOLID, 1, clrLocation );
		CPen * pOldPen = dc.SelectObject( &_pen );
		if( bDrawVertical )
		{
			dc.MoveTo( ptAligned.x, rcClient.top    );
			dc.LineTo( ptAligned.x, rcClient.bottom );
		}
		if( bDrawHorizontal )
		{
			dc.MoveTo( rcClient.left,  ptAligned.y  );
			dc.LineTo( rcClient.right, ptAligned.y  );
		}
		dc.SelectObject( pOldPen );
	}
}

#if _MFC_VER < 0x700
void CExtLLMapWnd::OnActivateApp(BOOL bActive, HTASK hTask)
#else
void CExtLLMapWnd::OnActivateApp(BOOL bActive, DWORD hTask)
#endif
{
	CWnd::OnActivateApp(bActive, hTask);
	if( ! bActive )
		_CancelActions();
}

void CExtLLMapWnd::OnCancelMode() 
{
	ASSERT_VALID( this );
	CWnd::OnCancelMode();
	_CancelActions();
}

void CExtLLMapWnd::OnCaptureChanged(CWnd * pWnd) 
{
	ASSERT_VALID( this );
	CWnd::OnCaptureChanged(pWnd);
	if( pWnd != this )
		_CancelActions();
}


CPoint CExtLLMapWnd::OnLLMapSnapToMarker(
	CPoint point,
	bool bDisplayTip, // = false
	bool * p_bTipDisplayed // = NULL
	)
{
	ASSERT_VALID( this );
	if( p_bTipDisplayed != NULL )
		(*p_bTipDisplayed) = false;
	if( ! m_bMapDisplayMarkers )
		return point;
CArray < MAP_MARKER, MAP_MARKER & > & arrMapMarkers = OnQueryMapMarkers();
INT nMakerIndex, nMakerCount = INT( arrMapMarkers.GetSize() );
	if( nMakerCount == 0 )
		return point;
bool bTipDisplayed = false;
INT nHT = -1;
CExtPopupMenuTipWnd * pATTW = NULL;
	if( (!bDisplayTip) || m_bMapDisplayMarkerTips )
	{
		INT nMarkerRadius = m_nMarkerRadiusSnap; // OnLLMapCalcMarkerRadius( rcClient );
		INT nMarkerRadius2 = nMarkerRadius * nMarkerRadius;
		for( nMakerIndex = 0; nMakerIndex < nMakerCount; nMakerIndex ++ )
		{
			MAP_MARKER & _marker = arrMapMarkers.ElementAt( nMakerIndex );
			CPoint ptMarker = MapToClientPoint( _marker.m_Longitude, _marker.m_Latitude );
			CSize sizeDistances = point - ptMarker;
			INT nDistance2 = sizeDistances.cx * sizeDistances.cx + sizeDistances.cy * sizeDistances.cy;
			if( nDistance2 <= nMarkerRadius2 )
			{
				nHT = nMakerIndex;
				if( point == ptMarker )
					nHT = -1;
				else
					point = ptMarker;
				break;
			}
		}
		if( ! bDisplayTip )
		{
			bTipDisplayed = OnLLMapDisplayCurrentLocationTip( point );
			if( p_bTipDisplayed != NULL )
				(*p_bTipDisplayed) = bTipDisplayed;
			return point;
		}
		pATTW = OnAdvancedPopupMenuTipWndGet();
		if( pATTW == NULL )
			return point;
		if( nHT >= 0 )
		{
			if(		m_nAdvancedTipStyle != INT(CExtPopupMenuTipWnd::__ETS_NONE)
				&&	(! 
							(	CExtPopupMenuWnd::IsMenuTracking()
							&&	( ! GetParent()->IsKindOf( RUNTIME_CLASS(CExtLLPopupMapMenuWnd) ) )
							)
					)
				)
			{
				CRect rcClient;
				GetClientRect( &rcClient );
				CRect rcArea( point, point );
				rcArea.InflateRect( nMarkerRadius, nMarkerRadius );
				ClientToScreen( &rcArea );
				MAP_MARKER & _marker = arrMapMarkers.ElementAt( nHT );
				CExtSafeString strMarkerText;
				strMarkerText = LPCTSTR( _marker.m_strLabel );
				if( m_bMapDisplayCurrentLocationTip )
				{
					CExtSafeString strTip = OnLLMapQueryCurrentLocationTipText( point );
					if( ! strTip.IsEmpty() )
					{
						strMarkerText += _T("\n");
						strMarkerText += strTip;
					}
				}
				else if( m_bMapDisplayTimeZoneTips )
				{
					CExtSafeString strTZT = OnLLMapQueryTimeZoneTipText( point, true );
					if( ! strTZT.IsEmpty() )
					{
						strMarkerText += _T("\n");
						strMarkerText += strTZT;
					}
				}
				pATTW->SetText( LPCTSTR(strMarkerText) );
				OnAdvancedPopupMenuTipWndDisplay( *pATTW, rcArea, m_bNoDelayMarkerTips );
				bTipDisplayed = true;
			}
			else
				nHT = -1;
		}
	} // if( (!bDisplayTip) || m_bDisplayMarkerTips )
	if( ! bTipDisplayed )
		bTipDisplayed = OnLLMapDisplayCurrentLocationTip( point );
	if( p_bTipDisplayed != NULL )
		(*p_bTipDisplayed) = bTipDisplayed;
	if( bDisplayTip && (! bTipDisplayed ) && nHT < 0 && pATTW != NULL )
		pATTW->Hide();
	return point;
}

CExtSafeString CExtLLMapWnd::OnLLMapQueryCurrentLocationTipText(
	CPoint point
	)
{
	ASSERT_VALID( this );
CExtSafeString strTip;
CRect rcClient;
	GetClientRect( &rcClient );
	if(	!	(	rcClient.left <= point.x && point.x <= rcClient.right
			&&	rcClient.top  <= point.y && point.y <= rcClient.bottom
			)
		)
		return strTip;
CExtLongitude _Longitude;
CExtLatitude _Latitude;
	ClientToMap( point, _Longitude, _Latitude );
	strTip =
		stat_TextGet(
			_Longitude,
			_Latitude,
			GetMode(),
			m_tchrNorth,
			m_tchrSouth,
			m_tchrEast,
			m_tchrWest,
			m_tchrSeparator,
			m_tchrFractionSeparator,
			m_nFractionDigitCount,
			_T("\n") //LPCTSTR(m_strLLS)
			);
	if( m_bMapDisplayTimeZoneTips )
	{
		CExtSafeString strTZT;
		strTZT += OnLLMapQueryTimeZoneTipText( point, false );
		if( ! strTZT.IsEmpty() )
		{
			strTip += _T("\n");
			strTip += strTZT;
		}
	}
	return strTip;
}

bool CExtLLMapWnd::OnLLMapDisplayCurrentLocationTip(
	CPoint point
	)
{
	ASSERT_VALID( this );
	if( ! m_bMapDisplayCurrentLocationTip )
		return false;
CExtPopupMenuTipWnd * pATTW = OnAdvancedPopupMenuTipWndGet();
	if( pATTW == NULL )
		return false;
	if(		m_nAdvancedTipStyle == INT(CExtPopupMenuTipWnd::__ETS_NONE)
		||	(	CExtPopupMenuWnd::IsMenuTracking()
			&&	( ! GetParent()->IsKindOf( RUNTIME_CLASS(CExtLLPopupMapMenuWnd) ) )
			)
		)
		return false;
CExtSafeString strTip = OnLLMapQueryCurrentLocationTipText( point );
	if( strTip.IsEmpty() )
		return false;
CRect rcClient;
	GetClientRect( &rcClient );
CRect rcArea( point.x, point.y, point.x + 1, point.y + 1 );
	switch( GetMode() )
	{
	case longitude:
		rcArea.top = rcClient.top;
		rcArea.bottom = rcClient.bottom;
	break;
	case latitude:
		rcArea.left = rcClient.left;
		rcArea.right = rcClient.right;
	break;
	}
	ClientToScreen( &rcArea );
	pATTW->SetText( LPCTSTR(strTip) );
	OnAdvancedPopupMenuTipWndDisplay( *pATTW, rcArea, m_bNoDelayCurrentLocationTips );
	return true;
}

CExtSafeString CExtLLMapWnd::OnLLMapQueryLocalDateTimeTipText(
	CPoint ptTZHT,
	CExtLLMapWnd::WORLD_TIME_ZONE * pWTZ
	)
{
	ASSERT_VALID( this );
	ptTZHT;
CExtSafeString strDT;
	if(		pWTZ == NULL
		||	( ! ( m_bMapDisplayLocalDate || m_bMapDisplayLocalTime ) )
		)
		return strDT;
COleDateTime dt;
	if( m_dtGMT.GetStatus() == COleDateTime::valid )
		dt = m_dtGMT;
	else
	{
		//dt = COleDateTime::GetCurrentTime();
		time_t rawtime;
		tm * ptm = NULL;
		time( &rawtime );
#if _MFC_VER >= 0x0800
		tm _tm_buf;
		::memset( &_tm_buf, 0, sizeof(tm) );
		ptm = &_tm_buf;
		gmtime_s( ptm, &rawtime );
#else
		ptm = gmtime( &rawtime );
#endif
		dt.SetDateTime(
			ptm->tm_year + 1900,
			ptm->tm_mon + 1,
			ptm->tm_mday,
			ptm->tm_hour,
			ptm->tm_min,
			ptm->tm_sec
			);
	}
COleDateTimeSpan dts( 0, pWTZ->GetShiftHours( true ), pWTZ->GetShiftMinutes(), 0 );
	if( pWTZ->IsNegativeShift() )
		dt -= dts;
	else
		dt += dts;
	if( m_bMapDisplayLocalDate )
	{
		CString strTmp;
		strTmp = dt.Format( VAR_DATEVALUEONLY );
		strDT += LPCTSTR(strTmp);
	}
	if( m_bMapDisplayLocalTime )
	{
		CString strTmp;
		strTmp = dt.Format( VAR_TIMEVALUEONLY );
		if( m_bMapDisplayLocalDate )
			strDT += _T("\n");
		strDT += LPCTSTR(strTmp);
	}
	return strDT;
}

CExtSafeString CExtLLMapWnd::OnLLMapQueryTimeZoneTipText(
	CPoint ptTZHT,
	bool bInsideMarker
	)
{
	ASSERT_VALID( this );
	bInsideMarker;
CExtSafeString strTZT;
COLORREF clrTZ = OnLLMapHitTestTimeZoneColor( ptTZHT );
	if( clrTZ == COLORREF(-1L) )
		return strTZT;
	//strTZT.Format( _T("#%02X%02X%02X\npt(%d,%d)"), int(GetRValue(clrTZ)), int(GetGValue(clrTZ)), int(GetBValue(clrTZ)), ptTZHT.x, ptTZHT.y );
WORLD_TIME_ZONE * pWTZ = NULL;
	if( ! m_mapWorldTimeZones.Lookup( clrTZ, pWTZ ) )
		return strTZT;
	ASSERT( pWTZ != NULL );
	strTZT = _T("GMT ");
	strTZT += pWTZ->GetTimeShiftAsText( true );
CExtSafeString strDT = OnLLMapQueryLocalDateTimeTipText( ptTZHT, pWTZ );
	if( ! strDT.IsEmpty() )
	{
		strTZT += _T("\n");
		strTZT += strDT;
	}
	return strTZT;
}

COLORREF CExtLLMapWnd::OnLLMapHitTestTimeZoneColor(
	CPoint ptTZHT
	)
{
	ASSERT_VALID( this );
	if( m_bmpHTTZ.IsEmpty() )
		OnLLMapWorldMapInit( m_nTzHtTransparencySelected, m_nTzHtTransparencyNormal );
	if( m_bmpHTTZ.IsEmpty() )
		return COLORREF(-1L);
CRect rcClient;
	GetClientRect( &rcClient );
	if(	!	(	rcClient.left <= ptTZHT.x && ptTZHT.x <= rcClient.right
			&&	rcClient.top  <= ptTZHT.y && ptTZHT.y <= rcClient.bottom
			)
		)
		return COLORREF(-1L);
CSize sizeTZ = m_bmpHTTZ.GetSize();
CPoint ptTZ( ptTZHT.x - rcClient.left, ptTZHT.y - rcClient.top );
	ptTZ.x = ::MulDiv( ptTZ.x, sizeTZ.cx, rcClient.Width() );
	ptTZ.y = ::MulDiv( ptTZ.y, sizeTZ.cy, rcClient.Height() );
	if( ptTZ.x >= sizeTZ.cx )
		ptTZ.x = sizeTZ.cx - 1;
	if( ptTZ.y >= sizeTZ.cy )
		ptTZ.y = sizeTZ.cy - 1;
COLORREF clrTZ = m_bmpHTTZ.GetPixel( ptTZ.x, ptTZ.y );
	return clrTZ;
}

bool CExtLLMapWnd::OnLLMapDisplayTimeZoneTip(
	CPoint ptTZHT
	)
{
	ASSERT_VALID( this );
	if( ! m_bMapDisplayTimeZoneTips )
		return false;
CExtPopupMenuTipWnd * pATTW = OnAdvancedPopupMenuTipWndGet();
	if( pATTW == NULL )
		return false;
	if(		m_nAdvancedTipStyle == INT(CExtPopupMenuTipWnd::__ETS_NONE)
		||	(	CExtPopupMenuWnd::IsMenuTracking()
			&&	( ! GetParent()->IsKindOf( RUNTIME_CLASS(CExtLLPopupMapMenuWnd) ) )
			)
		)
		return false;
CExtSafeString strTZT = OnLLMapQueryTimeZoneTipText( ptTZHT, true );
	if( strTZT.IsEmpty() )
		return false;
CRect rcClient;
	GetClientRect( &rcClient );
CRect rcArea( ptTZHT.x, rcClient.top, ptTZHT.x + 1, rcClient.bottom );
	ClientToScreen( &rcArea );
	pATTW->SetText( LPCTSTR(strTZT) );
	OnAdvancedPopupMenuTipWndDisplay( *pATTW, rcArea, m_bNoDelayTimeZoneTips );
	return true;
}

BOOL CExtLLMapWnd::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	ASSERT_VALID( this );
	return CWnd::OnMouseWheel(nFlags, zDelta, pt);
}

void CExtLLMapWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
	ASSERT_VALID( this );
	//CWnd::OnMouseMove(nFlags, point);
	nFlags;
CPoint ptScreen = point;
	ClientToScreen( &ptScreen );
HWND hWndFromPoint = ::WindowFromPoint( ptScreen );
	switch( m_eMTM )
	{
	case __EMTM_NONE:
	case __EMTM_HOVER:
		{
			if( hWndFromPoint == m_hWnd )
			{
				CPoint pointTzTip = point;
				bool bUpdate = false, bTipDisplayed = false;
				if( m_eMTM == __EMTM_NONE )
				{
					m_eMTM = __EMTM_HOVER;
					SetCapture();
					pointTzTip = m_ptHover = OnLLMapSnapToMarker( point, true, &bTipDisplayed );
					bUpdate = true;
				}
				else
				{
					if( m_ptHover != point )
					{
						pointTzTip = m_ptHover = OnLLMapSnapToMarker( point, true, &bTipDisplayed );
						bUpdate = true;
					}
				}
				if( bUpdate )
					Update( true );
				if( ! bTipDisplayed )
					bTipDisplayed = OnLLMapDisplayCurrentLocationTip( pointTzTip );
				if( ! bTipDisplayed )
					OnLLMapDisplayTimeZoneTip( pointTzTip );
			}
			else
			{
				if( m_eMTM != __EMTM_NONE )
					_CancelActions();
			}
		}
	break;
	case __EMTM_PRESSED:
		{
			CPoint pointTzTip = point;
			bool bTipDisplayed = false;
			if( m_bMapChangeSelectionWhenPressed )
			{
				_ChangeLL( point, true, &bTipDisplayed );
				pointTzTip = point;
			}
			else
			{
				if( m_ptPressed != point )
				{
					pointTzTip = m_ptHover = m_ptPressed = point;
					Update( true );
				}
			}
			if( ! bTipDisplayed )
				bTipDisplayed = OnLLMapDisplayCurrentLocationTip( pointTzTip );
			if( ! bTipDisplayed )
				OnLLMapDisplayTimeZoneTip( pointTzTip );
		}
	break;
	}
	_DoSetCursor( point, false );
}

void CExtLLMapWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	ASSERT_VALID( this );
	//CWnd::OnLButtonDown(nFlags, point);
	nFlags;
bool bTipDisplayed = false;
	point = OnLLMapSnapToMarker( point, false, &bTipDisplayed );
	if( m_bMapChangeSelectionWhenPressed || m_bMapChangeSelectionOnLbuttonPressed )
		_ChangeLL( point, true, &bTipDisplayed );
	if( ! m_bMapChangeSelectionWhenPressed )
		m_ptPressed = point;
	m_eMTM = __EMTM_PRESSED;
	SetCapture();
	Update( true );
	_DoSetCursor( point, false );
	if( ! bTipDisplayed )
		bTipDisplayed = OnLLMapDisplayCurrentLocationTip( point );
	if( ! bTipDisplayed )
		OnLLMapDisplayTimeZoneTip( point );
}

void CExtLLMapWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
	ASSERT_VALID( this );
	//CWnd::OnLButtonUp(nFlags, point);
	nFlags;
	if( m_eMTM != __EMTM_PRESSED )
		return;
	_CancelActions( false );
	if( ! m_bMapChangeSelectionOnLbuttonReleased )
		return;
bool bTipDisplayed = false;
	_ChangeLL( point, true, &bTipDisplayed );
	_DoSetCursor( point, false );
	if( ! bTipDisplayed )
		bTipDisplayed = OnLLMapDisplayCurrentLocationTip( point );
	if( ! bTipDisplayed )
		OnLLMapDisplayTimeZoneTip( point );
}

void CExtLLMapWnd::_DoSetCursor()
{
	ASSERT_VALID( this );
CPoint ptScreeen;
	GetCursorPos( &ptScreeen );
	_DoSetCursor( ptScreeen, true );
}

void CExtLLMapWnd::_DoSetCursor( const POINT pt, bool bScreenPoint )
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CPoint ptScreeen = pt;
	if( ! bScreenPoint )
		ClientToScreen( &ptScreeen );
bool bHideCursor = false;
	if( ::WindowFromPoint( ptScreeen ) == m_hWnd )
	{
		if( m_eMTM != __EMTM_NONE )
			bHideCursor = true;
	}
HCURSOR hCursor = NULL;
	if( ! bHideCursor )
		hCursor = ::AfxGetApp()->LoadStandardCursor( IDC_ARROW );
	::SetCursor( hCursor );
}

void CExtLLMapWnd::_ChangeLL(
	CPoint & point,
	bool bUpdateNow, // = true 
	bool * p_bTipDisplayed // = NULL
	)
{
	ASSERT_VALID( this );
	point = OnLLMapSnapToMarker( point, false, p_bTipDisplayed );

CRect rcClient;
	GetClientRect( rcClient );
	point.x = max( point.x, rcClient.left );
	point.y = max( point.y, rcClient.top );
	point.x = min( point.x, rcClient.right );
	point.y = min( point.y, rcClient.bottom );

CExtLongitude _Longitude;
CExtLatitude _Latitude;
	ClientToMap( point, _Longitude, _Latitude );

// 	_Longitude.MinutesSet( 0 );
// 	_Longitude.SecondsSet( 0 );
	_Longitude.SecondsFractionSet( 0.0 );

// 	_Latitude.MinutesSet( 0 );
// 	_Latitude.SecondsSet( 0 );
 	_Latitude.SecondsFractionSet( 0.0 );

	LongitudeSet( _Longitude, false );
	LatitudeSet( _Latitude, false );

	m_ptHover = m_ptPressed = point;

	Update( bUpdateNow );
}

void CExtLLMapWnd::_CancelActions(
	bool bUpdateNow // = true 
	)
{
	ASSERT_VALID( this );
	if( m_bCanceling )
		return;
	m_bCanceling = true;
e_mouse_tracking_mode_t eOldMTM = m_eMTM;
	if( ::GetCapture() == m_hWnd )
		::ReleaseCapture();
	m_eMTM = __EMTM_NONE;
	m_ptHover.x = m_ptHover.y = m_ptPressed.x = m_ptPressed.y = -32767;
CExtPopupMenuTipWnd * pATTW = OnAdvancedPopupMenuTipWndGet();
	if( pATTW != NULL )
		pATTW->Hide();
	if( eOldMTM != __EMTM_NONE )
		Update( bUpdateNow );
	m_bCanceling = false;
}

void CExtLLMapWnd::OnSetFocus( CWnd * pOldWnd )
{
	ASSERT_VALID( this );
	pOldWnd;
	_DoSetCursor();
}

CExtPopupMenuTipWnd * CExtLLMapWnd::OnAdvancedPopupMenuTipWndGet(
	) const
{
	if( m_nAdvancedTipStyle == INT(CExtPopupMenuTipWnd::__ETS_NONE) )
		return NULL;
	return (&( CExtPopupMenuSite::g_DefPopupMenuSite.GetTip() ));
}

void CExtLLMapWnd::OnAdvancedPopupMenuTipWndDisplay(
	CExtPopupMenuTipWnd & _ATTW,
	const RECT & rcExcludeArea,
	bool bNoDelay
	) const
{
	ASSERT_VALID( this );
	_ATTW.SetTipStyle( (CExtPopupMenuTipWnd::e_tip_style_t)m_nAdvancedTipStyle );
	_ATTW.Show( (CWnd*)this, rcExcludeArea, bNoDelay );
	if( bNoDelay )
	{
		if( _ATTW.GetSafeHwnd() != NULL )
		{
			_ATTW.ShowWindow( SW_SHOWNOACTIVATE );
			_ATTW.UpdateWindow();
		}
		CExtPaintManager::stat_PassPaintMessages();
	}
}

/////////////////////////////////////////////////////////////////////////////
// CExtLLPopupMapMenuWnd

IMPLEMENT_DYNCREATE( CExtLLPopupMapMenuWnd, CExtPopupControlMenuWnd )

BEGIN_MESSAGE_MAP(CExtLLPopupMapMenuWnd, CExtPopupControlMenuWnd)
	//{{AFX_MSG_MAP(CExtLLPopupMapMenuWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CExtLLPopupMapMenuWnd::CExtLLPopupMapMenuWnd()
{
}

CExtLLPopupMapMenuWnd::~CExtLLPopupMapMenuWnd()
{
}

HWND CExtLLPopupMapMenuWnd::OnCreateChildControl(
	const RECT & rcChildControl
	)
{
	ASSERT_VALID( this );
	//pWndMap->m_bAuto
	if(	! m_wndMap.Create( this, rcChildControl ) )
	{
		ASSERT( FALSE );
		return NULL;
	}
bool bRTL = OnQueryLayoutRTL();
	if( bRTL )
		m_wndMap.ModifyStyleEx( 0, WS_EX_LAYOUTRTL, SWP_FRAMECHANGED );
	return m_wndMap.m_hWnd;
}

CSize CExtLLPopupMapMenuWnd::OnAdjustChildControlSize()
{
	ASSERT_VALID( this );
	m_wndMap.WorldMapLoad( m_wndMap.m_bMapHR, m_wndMap.m_bMapHighlightTimeZones || m_wndMap.m_bMapDisplayTimeZoneTips, false, false );
	if( m_wndMap.m_bmpMap.IsEmpty() )
		return m_sizeChildControl;
CSize _size = m_wndMap.m_bmpMap.GetSize();
	return _size;
}

bool CExtLLPopupMapMenuWnd::_OnMouseClick(
	UINT nFlags,
	CPoint point,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return false;
	if( GetSite().GetAnimated() != NULL )
		return true;
bool bLButtonUpCall =
		(nFlags==WM_LBUTTONUP || nFlags==WM_NCLBUTTONUP)
			? true : false;
	if( bLButtonUpCall )
	{
		bNoEat = false;
		CancelMenuTracking();
		return true;
	}
	return CExtPopupControlMenuWnd::_OnMouseClick( nFlags, point, bNoEat );
}

bool CExtLLPopupMapMenuWnd::_OnKeyDown(
	UINT nChar,
	UINT nRepCnt,
	UINT nFlags,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );
	nChar;
	nFlags;
	nRepCnt;
	if( GetSafeHwnd() == NULL )
		return false;
	if( GetSite().GetAnimated() != NULL )
		return true;
	bNoEat = false;
	CancelMenuTracking();
	return true;
}

#endif // (!defined __EXT_MFC_NO_GEO_CONTROLS)


