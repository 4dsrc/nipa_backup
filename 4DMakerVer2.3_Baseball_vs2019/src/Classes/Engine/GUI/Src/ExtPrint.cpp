// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_PRINT)

#if (!defined __EXT_PRINT_H)
	#include <ExtPrint.h>
#endif

#if (!defined __EXT_RICH_CONTENT_H)
	#include <ExtRichContent.h>
#endif // (!defined __EXT_RICH_CONTENT_H)

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#if (!defined __EXT_RESIZABLE_DIALOG_H)
	#include <ExtResizableDialog.h>
#endif

#if (!defined __EXT_LABEL_H)
	#include <ExtLabel.h>
#endif

#if (!defined __EXT_BUTTON_H)
	#include <ExtButton.h>
#endif

#if (!defined __EXT_NC_FRAME_H)
	#include <ExtNcFrame.h>
#endif

#if (!defined __EXT_MEMORY_DC_H)
	#include <../Src/ExtMemoryDC.h>
#endif

#if (!defined __EXT_LOCALIZATION_H)
	#include <../Src/ExtLocalization.h>
#endif

#if (!defined __EXT_MFC_NO_SHELL)
	#if (!defined __EXT_CONTROLS_SHELL_H)
		#include <ExtControlsShell.h>
	#endif
#endif

#if (!defined __EXT_CONTROLS_COMMON_H)
	#include <ExtControlsCommon.h>
#endif

#if _MFC_VER < 0x700
	#include <../src/AfxImpl.h>
#else
	#include <../src/mfc/AfxImpl.h>
#endif

#if (!defined __AFXPRIV_H__)
	#include <AfxPriv.h>
#endif

#include <./Resources/resource.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#pragma message("   Prof-UIS is automatically linking with rpcrt4.lib")
#pragma message("      (UUID API)")
#pragma comment(lib,"rpcrt4.lib") 

#define __EXT_PRINT_OUTER_MIN_EXTENTS_LEFT			0
#define __EXT_PRINT_OUTER_MIN_EXTENTS_TOP			0
#define __EXT_PRINT_OUTER_MIN_EXTENTS_RIGHT			0
#define __EXT_PRINT_OUTER_MIN_EXTENTS_BOTTOM		0

/////////////////////////////////////////////////////////////////////////////
// CExtPPVW_MetafileCache

CExtPPVW_MetafileCache::CExtPPVW_MetafileCache()
	: m_bMetafileCache_DiskMode( true )
	, m_bMetafileCache_Initializing( false )
	, m_bUseEnhancedMetafiles( true )
{
}

CExtPPVW_MetafileCache::~CExtPPVW_MetafileCache()
{
	MetafileCache_FreeAllPages();
}

bool CExtPPVW_MetafileCache::MetafileCache_Initializing() const
{
	ASSERT( this != NULL );
	return m_bMetafileCache_Initializing;
}

bool CExtPPVW_MetafileCache::MetafileCache_DiskModeGet() const
{
	ASSERT( this != NULL );
	return m_bMetafileCache_DiskMode;
}

void CExtPPVW_MetafileCache::MetafileCache_DiskModeSet( bool bDiskMode )
{
	ASSERT( this != NULL );
	if(		MetafileCache_Initializing()
		||	MetafileCache_GetPageCount() != 0
		)
	{
		ASSERT( FALSE );
		return;
	}
	m_bMetafileCache_DiskMode = bDiskMode;
}

void CExtPPVW_MetafileCache::MetafileCache_FreeAllPages()
{
	ASSERT( this != NULL );
	ASSERT( ! MetafileCache_Initializing() );
	ASSERT( INT( m_mapMetafileCache_DiskH.GetCount() ) == 0 );
	ASSERT( INT( m_mapMetafileCache_DiskC.GetCount() ) == 0 );
	if( MetafileCache_DiskModeGet() )
	{
		ASSERT( INT( m_arrMetafileCache_MemoryEmfPages.GetSize() ) == 0 );
		INT nMetafileIndex, nMetafileCount = INT( m_arrMetafileCache_DiskEmfNames.GetSize() );
		for( nMetafileIndex = 0; nMetafileIndex < nMetafileCount; nMetafileIndex ++ )
		{
			CExtSafeString & strMetafileName = m_arrMetafileCache_DiskEmfNames.ElementAt( nMetafileIndex );
			if( strMetafileName.IsEmpty() )
				continue;
			::DeleteFile( strMetafileName );
		}
		m_arrMetafileCache_DiskEmfNames.RemoveAll();
	} // if( MetafileCache_DiskModeGet() )
	else
	{
		ASSERT( INT( m_arrMetafileCache_DiskEmfNames.GetSize() ) == 0 );
		INT nMetafileIndex, nMetafileCount = INT( m_arrMetafileCache_MemoryEmfPages.GetSize() );
		for( nMetafileIndex = 0; nMetafileIndex < nMetafileCount; nMetafileIndex ++ )
		{
			HANDLE hMetafile = m_arrMetafileCache_MemoryEmfPages[ nMetafileIndex ];
			if( hMetafile == NULL )
				continue;
			if( m_bUseEnhancedMetafiles )
				::DeleteEnhMetaFile( (HENHMETAFILE)hMetafile );
			else
				::DeleteMetaFile( (HMETAFILE)hMetafile );
		}
		m_arrMetafileCache_MemoryEmfPages.RemoveAll();
	} // else from if( MetafileCache_DiskModeGet() )
}

INT CExtPPVW_MetafileCache::MetafileCache_GetPageCount() const
{
	ASSERT( this != NULL );
	if( MetafileCache_DiskModeGet() )
	{
		INT nMetafileCount = INT( m_arrMetafileCache_DiskEmfNames.GetSize() );
		return nMetafileCount;
	} // if( MetafileCache_DiskModeGet() )
	else
	{
		INT nMetafileCount = INT( m_arrMetafileCache_MemoryEmfPages.GetSize() );
		return nMetafileCount;
	} // else from if( MetafileCache_DiskModeGet() )
}

HANDLE CExtPPVW_MetafileCache::MetafileCache_LockPage( INT nMetafileIndex )
{
	ASSERT( this != NULL );
	if(		MetafileCache_Initializing()
		||	nMetafileIndex < 0
		)
	{
		ASSERT( FALSE );
		return NULL;
	}
	if( MetafileCache_DiskModeGet() )
	{
		INT nMetafileCount = INT( m_arrMetafileCache_DiskEmfNames.GetSize() );
		if( nMetafileIndex >= nMetafileCount )
		{
			ASSERT( FALSE );
			return NULL;
		}
		INT nLockCount = 0;
		if( m_mapMetafileCache_DiskC.Lookup( nMetafileIndex, nLockCount ) )
		{
			nLockCount ++;
			m_mapMetafileCache_DiskC.SetAt( nMetafileIndex, nLockCount );
			HANDLE hMetafile = NULL;
			VERIFY( m_mapMetafileCache_DiskH.Lookup( nMetafileIndex, hMetafile ) );
			ASSERT( hMetafile != NULL );
			return hMetafile;
		} // if( m_mapMetafileCache_DiskC.Lookup( nMetafileIndex, nLockCount ) )
		CExtSafeString strMetafileName = m_arrMetafileCache_DiskEmfNames[ nMetafileIndex ];
		if( strMetafileName.IsEmpty() )
		{
			ASSERT( FALSE );
			return NULL;
		}
		HANDLE hMetafile = NULL;
		if( m_bUseEnhancedMetafiles )
		{
			hMetafile = (HANDLE)::GetEnhMetaFile( LPCTSTR(strMetafileName) );
			if( hMetafile == NULL )
			{
				ASSERT( FALSE );
				return NULL;
			}
		}
		else
		{
			hMetafile = (HANDLE)::GetMetaFile( LPCTSTR(strMetafileName) );
			if( hMetafile == NULL )
			{
				ASSERT( FALSE );
				return NULL;
			}
		}
		nLockCount = 1;
		m_mapMetafileCache_DiskC.SetAt( nMetafileIndex, nLockCount );
		m_mapMetafileCache_DiskH.SetAt( nMetafileIndex, hMetafile );
		return hMetafile;
	} // if( MetafileCache_DiskModeGet() )
	else
	{
		INT nMetafileCount = INT( m_arrMetafileCache_MemoryEmfPages.GetSize() );
		if( nMetafileIndex >= nMetafileCount )
		{
			ASSERT( FALSE );
			return NULL;
		}
		HANDLE hMetafile = m_arrMetafileCache_MemoryEmfPages[ nMetafileIndex ];
		ASSERT( hMetafile != NULL );
		return hMetafile;
	} // else from if( MetafileCache_DiskModeGet() )
}

bool CExtPPVW_MetafileCache::MetafileCache_UnockPage( INT nMetafileIndex )
{
	ASSERT( this != NULL );
	if(		MetafileCache_Initializing()
		||	nMetafileIndex < 0
		)
	{
		ASSERT( FALSE );
		return false;
	}
	if( MetafileCache_DiskModeGet() )
	{
		INT nMetafileCount = INT( m_arrMetafileCache_DiskEmfNames.GetSize() );
		if( nMetafileIndex >= nMetafileCount )
		{
			ASSERT( FALSE );
			return false;
		}
		INT nLockCount = 0;
		if( ! m_mapMetafileCache_DiskC.Lookup( nMetafileIndex, nLockCount ) )
		{
			ASSERT( FALSE );
			return false;
		}
		ASSERT( nLockCount > 0 );
		nLockCount --;
		if( nLockCount != 0 )
			return true;
		HANDLE hMetafile = NULL;
		VERIFY( m_mapMetafileCache_DiskH.Lookup( nMetafileIndex, hMetafile ) );
		ASSERT( hMetafile != NULL );
		if( m_bUseEnhancedMetafiles )
		{
			if( hMetafile != NULL )
				::DeleteEnhMetaFile( (HENHMETAFILE)hMetafile );
		}
		else
		{
			if( hMetafile != NULL )
				::DeleteMetaFile( (HMETAFILE)hMetafile );
		}
		m_mapMetafileCache_DiskC.RemoveKey( nMetafileIndex );
		m_mapMetafileCache_DiskH.RemoveKey( nMetafileIndex );
		return true;
	} // if( MetafileCache_DiskModeGet() )
	else
	{
		INT nMetafileCount = INT( m_arrMetafileCache_MemoryEmfPages.GetSize() );
		if( nMetafileIndex >= nMetafileCount )
		{
			ASSERT( FALSE );
			return false;
		}
		return true;
	} // else from if( MetafileCache_DiskModeGet() )
}

bool CExtPPVW_MetafileCache::MetafileCache_Init( INT nMetafileCount )
{
	ASSERT( this != NULL );
	if( MetafileCache_Initializing() )
	{
		ASSERT( FALSE );
		return false;
	}
	ASSERT( INT( m_mapMetafileCache_DiskH.GetCount() ) == 0 );
	ASSERT( INT( m_mapMetafileCache_DiskC.GetCount() ) == 0 );
	if( MetafileCache_DiskModeGet() )
	{
		INT nMetafileCountExisting = INT( m_arrMetafileCache_DiskEmfNames.GetSize() );
		if( nMetafileCountExisting != 0 )
		{
			ASSERT( FALSE );
			return false;
		}
		m_arrMetafileCache_DiskEmfNames.SetSize( nMetafileCount );
		INT nMetafileCountNew = INT( m_arrMetafileCache_DiskEmfNames.GetSize() );
		if( nMetafileCountNew != nMetafileCount )
		{
			m_arrMetafileCache_DiskEmfNames.RemoveAll();
			ASSERT( FALSE );
			return false;
		}
		INT nMetafileIndex;
		for( nMetafileIndex = 0; nMetafileIndex < nMetafileCount; nMetafileIndex ++ )
			m_arrMetafileCache_DiskEmfNames[ nMetafileIndex ] = _T("");
		m_bMetafileCache_Initializing = true;
	} // if( MetafileCache_DiskModeGet() )
	else
	{
		INT nMetafileCountExisting = INT( m_arrMetafileCache_MemoryEmfPages.GetSize() );
		if( nMetafileCountExisting != 0 )
		{
			ASSERT( FALSE );
			return false;
		}
		m_arrMetafileCache_MemoryEmfPages.SetSize( nMetafileCount );
		INT nMetafileCountNew = INT( m_arrMetafileCache_MemoryEmfPages.GetSize() );
		if( nMetafileCountNew != nMetafileCount )
		{
			m_arrMetafileCache_MemoryEmfPages.RemoveAll();
			ASSERT( FALSE );
			return false;
		}
		INT nMetafileIndex;
		for( nMetafileIndex = 0; nMetafileIndex < nMetafileCount; nMetafileIndex ++ )
			m_arrMetafileCache_MemoryEmfPages[ nMetafileIndex ] = NULL;
		m_bMetafileCache_Initializing = true;
	} // else from if( MetafileCache_DiskModeGet() )
	return true;
}

bool CExtPPVW_MetafileCache::MetafileCache_InitAt(
	INT nMetafileIndex,
	HANDLE hMetafile,
	__EXT_MFC_SAFE_LPCTSTR strMetafileName
	)
{
	ASSERT( this != NULL );
	if(		( ! MetafileCache_Initializing() )
		||	nMetafileIndex < 0
		||	hMetafile == NULL
		)
	{
		ASSERT( FALSE );
		return false;
	}
	ASSERT( INT( m_mapMetafileCache_DiskH.GetCount() ) == 0 );
	ASSERT( INT( m_mapMetafileCache_DiskC.GetCount() ) == 0 );
	if( MetafileCache_DiskModeGet() )
	{
		if( m_bUseEnhancedMetafiles )
			::DeleteEnhMetaFile( (HENHMETAFILE)hMetafile );
		else
			::DeleteMetaFile( (HMETAFILE)hMetafile );
		if(		LPCTSTR(strMetafileName) == NULL
			||	_tcslen( LPCTSTR(strMetafileName) ) == 0
			)
		{
			ASSERT( FALSE );
			return false;
		}
		INT nMetafileCount = INT( m_arrMetafileCache_DiskEmfNames.GetSize() );
		if( nMetafileIndex >= nMetafileCount )
		{
			ASSERT( FALSE );
			return false;
		}
		CExtSafeString & str = m_arrMetafileCache_DiskEmfNames.ElementAt( nMetafileIndex );
		ASSERT( str.IsEmpty() );
		str = strMetafileName;
		return true;
	} // if( MetafileCache_DiskModeGet() )
	else
	{
		INT nMetafileCount = INT( m_arrMetafileCache_MemoryEmfPages.GetSize() );
		if( nMetafileIndex >= nMetafileCount )
		{
			ASSERT( FALSE );
			return false;
		}
		if( m_arrMetafileCache_MemoryEmfPages[ nMetafileIndex ] != NULL )
		{
			ASSERT( FALSE );
			return false;
		}
		m_arrMetafileCache_MemoryEmfPages[ nMetafileIndex ] = hMetafile;
		return true;
	} // else from if( MetafileCache_DiskModeGet() )
}

void CExtPPVW_MetafileCache::MetafileCache_InitComplete( bool bOK )
{
	ASSERT( this != NULL );
	ASSERT( MetafileCache_Initializing() );
	m_bMetafileCache_Initializing = false;
	if( ! bOK )
		MetafileCache_FreeAllPages();
}

CExtSafeString CExtPPVW_MetafileCache::MetafileCache_GetUniqueName( INT nMetafileIndex )
{
	ASSERT( this != NULL );
CExtSafeString strFolder = MetafileCache_GetFolder();
	ASSERT( ! strFolder.IsEmpty() );
UUID _uuid;
	::UuidCreate( &_uuid );
TCHAR * strUUID = NULL;
#if (defined _UNICODE)
	::UuidToStringW( &_uuid, (unsigned short **)(&strUUID) );
#else
	::UuidToStringA( &_uuid, (unsigned char **)(&strUUID) );
#endif
CExtSafeString strMetafileName;
	strMetafileName.Format(
		_T("%s%d-%d-%p-%s-%d.Metafile"),
		LPCTSTR(strFolder),
		INT( ::GetCurrentProcessId() ),
		INT( ::GetCurrentThreadId() ),
		this,
		( strUUID != NULL ) ? strUUID : NULL,
		nMetafileIndex
		);
	if( strUUID != NULL )
	{
#if (defined _UNICODE)
		::RpcStringFreeW( (unsigned short **)(&strUUID) );
#else
		::RpcStringFreeA( (unsigned char **)(&strUUID) );
#endif
	}
	return strMetafileName;
}

CExtSafeString CExtPPVW_MetafileCache::MetafileCache_GetFolder() const
{
	ASSERT( this != NULL );
TCHAR strTempDirectory[ _MAX_PATH + 24 ];
	::memset( strTempDirectory, 0, sizeof(strTempDirectory) );
	::GetTempPath( _MAX_PATH + 1, strTempDirectory );
CExtSafeString strFolder;
INT nLen = INT( _tcslen(strTempDirectory) );
	if( nLen > 0 )
	{
		if( strTempDirectory[ nLen - 1 ] != _T('\\') )
			__EXT_MFC_STRCAT( strTempDirectory, _MAX_PATH + 12, _T("\\") );
		strFolder = strTempDirectory;
	}
	else
		strFolder = _T(".\\");
	return strFolder;
}

/////////////////////////////////////////////////////////////////////////////
// CExtPPVW_Printable

CExtPPVW_Printable::CExtPPVW_Printable()
	: m_pWndPrintPreviewContainer( NULL )
	, m_pWndPP( NULL )
	, m_bPrintPreviewEnabled( true )
	, m_bPrintPreviewCheckMarkCmdStyle( true )
	, m_bPrintPreviewShowPageMargins( false )
	, m_bRestoreScrollBarVisibilityH( false )
	, m_bRestoreScrollBarVisibilityV( false )
	, m_dwRestoreScrollBarStyles( 0 )
	, m_dwBasicPaintFlags( 0 )
	, m_bPpvwCreated( false )
	, m_bSendingToPrinter( false )
	, m_bUsePrintDialogFromOutside( false )
	, m_bUsePrintDialogFromPreview( false )
	, m_rcPageMarginsHM( 10*100, 10*100, 10*100, 10*100 )
	, m_rcPageHeaderFooterGutters( 0, 0, 0, 0 )
{
	m_strPpvCommandProfileName.Format( _T("CExtPPVW_Printable::m_strPpvCommandProfileName = %p"), this );
	VERIFY( g_CmdManager->ProfileSetup( LPCTSTR( m_strPpvCommandProfileName ) ) );
}

CExtPPVW_Printable::~CExtPPVW_Printable()
{
	VERIFY( g_CmdManager->ProfileDestroy( LPCTSTR( m_strPpvCommandProfileName ), true ) );
}

bool CExtPPVW_Printable::IsPrintPreviewMode() const
{
	ASSERT( this != NULL );
	if( m_pWndPP != NULL )
		return true;
	else
		return false;
}

bool CExtPPVW_Printable::IsPrinterAvailable() const
{
	ASSERT( this != NULL );
	if( CExtPPVW_Printable::friendly_app_t::_IsPrinterAvailable() )
		return true;
	else
		return false;
}

bool CExtPPVW_Printable::IsPrintPreviewAvailable() const
{
	ASSERT( this != NULL );
	if( ! m_bPrintPreviewEnabled )
		return false;
	if( ! IsPrinterAvailable() )
		return false;
	return true;
}

double CExtPPVW_Printable::_Internal_ComputerPreparePrintingProgress(
	LONG nPassNo,
	LONG nPassCount,
	LONG nPageNo,
	LONG nPageCount,
	LONG nContentWidthInPages,
	LONG nDocumentItemNo,
	LONG nDocumentItemCount,
	LONG nInPageDocumentItemFirstNo,
	LONG nInPageDocumentItemLastNo
	)
{
	ASSERT( this != NULL );
	ASSERT( 0 <= nPassNo && nPassNo < nPassCount );
	nContentWidthInPages;
	nInPageDocumentItemFirstNo;
	nInPageDocumentItemLastNo;
double lfTotalJobPercent = 50.0;
	if( nPassCount == 2 )
	{
		LONG nX = nDocumentItemNo, nDX = nDocumentItemCount; 
		if( nDocumentItemCount < 0 && nPageCount > 0 )
		{
			nX = nPageNo;
			nDX = nPageCount;
		}
		if( 0 <= nX && nX < nDX )
		{
			lfTotalJobPercent = ( double( nX ) * 50.0 ) / double( nDX );
			if( nPassNo > 0 )
				lfTotalJobPercent += 50.0;
		}
	}
	return lfTotalJobPercent;
}

bool CExtPPVW_Printable::_Internal_Notify_OnPreparePrinting_Progress(
	LONG nPassNo,
	LONG nPassCount,
	LONG nPageNo,
	LONG nPageCount,
	LONG nContentWidthInPages,
	LONG nDocumentItemNo,
	LONG nDocumentItemCount,
	LONG nInPageDocumentItemFirstNo,
	LONG nInPageDocumentItemLastNo
	)
{
	ASSERT( this != NULL );
	ASSERT( 0 <= nPassNo && nPassNo < nPassCount );
double lfTotalJobPercent =
		_Internal_ComputerPreparePrintingProgress(
			nPassNo,
			nPassCount,
			nPageNo,
			nPageCount,
			nContentWidthInPages,
			nDocumentItemNo,
			nDocumentItemCount,
			nInPageDocumentItemFirstNo,
			nInPageDocumentItemLastNo
			);
	ASSERT( 0.0 <= lfTotalJobPercent && lfTotalJobPercent <= 100.0 );
bool bRetVal =
		OnPreparePrinting_Progress(
			nPassNo,
			nPassCount,
			nPageNo,
			nPageCount,
			nContentWidthInPages,
			nDocumentItemNo,
			nDocumentItemCount,
			nInPageDocumentItemFirstNo,
			nInPageDocumentItemLastNo,
			lfTotalJobPercent
			);
	return bRetVal;
}

void CExtPPVW_Printable::OnPreparePrinting_Begin()
{
	ASSERT( this != NULL );
}

bool CExtPPVW_Printable::OnPreparePrinting_Progress(
	LONG nPassNo,
	LONG nPassCount,
	LONG nPageNo,
	LONG nPageCount,
	LONG nContentWidthInPages,
	LONG nDocumentItemNo,
	LONG nDocumentItemCount,
	LONG nInPageDocumentItemFirstNo,
	LONG nInPageDocumentItemLastNo,
	double lfTotalJobPercent // 0.0 ... 100.0
	)
{
	ASSERT( this != NULL );
	ASSERT( 0 <= nPassNo && nPassNo < nPassCount );
	nPassNo;
	nPassCount;
	nPageNo;
	nPageCount;
	nContentWidthInPages;
	nDocumentItemNo;
	nDocumentItemCount;
	nInPageDocumentItemFirstNo;
	nInPageDocumentItemLastNo;
	lfTotalJobPercent; // 0.0 ... 100.0

//CWnd * pWndPrintable = OnGetPrintableWnd();
//	if( pWndPrintable->GetSafeHwnd() != NULL )
//	{
//		CFrameWnd * pFrame = pWndPrintable->GetParentFrame();
//		if( pFrame != NULL )
//		{
//			CWnd * pWnd = pFrame->GetMessageBar();
//			if( pWnd != NULL )
//			{
//				CStatusBar * pStatusBar = DYNAMIC_DOWNCAST( CStatusBar, pWnd );
//				if( pStatusBar != NULL )
//				{
//					int nMessagePaneIndex = pStatusBar->CommandToIndex( 0 );
//					if( nMessagePaneIndex >= 0 )
//					{
//						CString s;
//						s.Format( _T("Progress: %.2lf %%"), lfTotalJobPercent );
//						pStatusBar->SetPaneText( nMessagePaneIndex, s );
//						//Sleep( 10 );
//					}
//				}
//			}
//		}
//	}

	return true;
}

void CExtPPVW_Printable::OnPreparePrinting_End(
	bool bCanceled
	)
{
	ASSERT( this != NULL );
	bCanceled;
}

BOOL CExtPPVW_Printable::OnPreparePrinting(
	CPrintInfo * pInfo
	)
{
	ASSERT( this != NULL );
//WORD nPageNoMin = 1, nPageNoMax = 1;
//pInfo->m_nNumPreviewPages = nPageNoMax;
//pInfo->m_pPD->m_pd.nMinPage = nPageNoMin;
//pInfo->m_pPD->m_pd.nFromPage = nPageNoMin;
//pInfo->m_pPD->m_pd.nMaxPage = nPageNoMax;
//pInfo->m_pPD->m_pd.nToPage = nPageNoMax;
	MetafileCache_FreeAllPages();
	if( pInfo->m_pPD->m_pd.hDC == NULL )
	{

		friendly_app_t * pApp = (friendly_app_t*) ::AfxGetApp();
#if (defined _UNICODE)
		//pApp->UpdatePrinterSelection( TRUE );
		if( pApp->m_hDevNames != NULL)
		{
			LPDEVNAMES lpDevNames = (LPDEVNAMES)::GlobalLock( pApp->m_hDevNames );
			if( lpDevNames  != NULL )
				::GlobalUnlock( pApp->m_hDevNames );
			else
			{
				::GlobalFree( pApp->m_hDevMode );
				pApp->m_hDevMode = NULL;
				if( pApp->m_hDevNames != NULL )
				{
					::GlobalFree( pApp->m_hDevNames );
					pApp->m_hDevNames = NULL;
				}
			}
		}
#endif // (defined _UNICODE)

		pApp->GetPrinterDeviceDefaults( &pInfo->m_pPD->m_pd );
		pInfo->m_pPD->m_pd.hDC =
			::AfxCreateDC(
				pInfo->m_pPD->m_pd.hDevNames,
				pInfo->m_pPD->m_pd.hDevMode
				);
		if( pInfo->m_pPD->m_pd.hDC == NULL )
			return FALSE;
	} // if( pInfo->m_pPD->m_pd.hDC == NULL )
	return TRUE;
}

void CExtPPVW_Printable::OnBeginPrinting(
	CDC * pDC,
	CPrintInfo * pInfo
	)
{
	ASSERT( this != NULL );
	pDC;
	pInfo;
}

void CExtPPVW_Printable::OnPrint(
	CDC * pDC,
	CPrintInfo * pInfo
	)
{
	ASSERT( this != NULL );
INT nMetafileCount = MetafileCache_GetPageCount();
	if( nMetafileCount == 0 )
		return;
INT nPageIndex = INT(pInfo->m_nCurPage) - INT(pInfo->m_pPD->m_pd.nMinPage);
	ASSERT( 0 <= nPageIndex && nPageIndex < nMetafileCount );
HANDLE hMetafile = MetafileCache_LockPage( nPageIndex );
	if( hMetafile == NULL )
		return;
CDC * pPrinterDC = CDC::FromHandle( pInfo->m_pPD->m_pd.hDC );
INT nPrinterDpiX = ::GetDeviceCaps( pPrinterDC->m_hDC, LOGPIXELSX );
INT nPrinterDpiY = ::GetDeviceCaps( pPrinterDC->m_hDC, LOGPIXELSY );
// INT nPrinterDpiDX = ::GetDeviceCaps( pPrinterDC->m_hDC, HORZRES );
// INT nPrinterDpiDY = ::GetDeviceCaps( pPrinterDC->m_hDC, VERTRES );
// INT nMeasureDpiDX = ::MulDiv( nPrinterDpiDX, g_PaintManager.m_nLPX, nPrinterDpiX );
// INT nMeasureDpiDY = ::MulDiv( nPrinterDpiDY, g_PaintManager.m_nLPY, nPrinterDpiY );

// CSize sizePMLT( m_rcPageMarginsHM.left, m_rcPageMarginsHM.top ), sizePMRB( m_rcPageMarginsHM.right, m_rcPageMarginsHM.bottom ); 
// 	pPrinterDC->HIMETRICtoDP( &sizePMLT );
// 	pPrinterDC->HIMETRICtoDP( &sizePMRB );
// 	sizePMLT.cx = ::MulDiv( sizePMLT.cx, g_PaintManager.m_nLPX, nPrinterDpiX );
// 	sizePMRB.cx = ::MulDiv( sizePMRB.cx, g_PaintManager.m_nLPX, nPrinterDpiX );
// 	sizePMLT.cy = ::MulDiv( sizePMLT.cy, g_PaintManager.m_nLPY, nPrinterDpiY );
// 	sizePMRB.cy = ::MulDiv( sizePMRB.cy, g_PaintManager.m_nLPY, nPrinterDpiY );
// 	nMeasureDpiDX -= sizePMLT.cx + sizePMRB.cx;
// 	nMeasureDpiDY -= sizePMLT.cy + sizePMRB.cy;

// CSize _sizePrinterOffset(
// 			pPrinterDC->GetDeviceCaps(PHYSICALOFFSETX),
// 			pPrinterDC->GetDeviceCaps(PHYSICALOFFSETY)
// 			);
// 	nMeasureDpiDX -= ::MulDiv( _sizePrinterOffset.cx * 2, g_PaintManager.m_nLPX, nPrinterDpiX );
// 	nMeasureDpiDY -= ::MulDiv( _sizePrinterOffset.cy * 2, g_PaintManager.m_nLPY, nPrinterDpiY );
// 	pPrinterDC->DPtoLP( &_sizePrinterOffset );
// 				CSize _sizePrinterHIMETRIC( nPrinterDpiDX, nPrinterDpiDY );
// 				// 	_sizePrinterHIMETRIC -= _sizePrinterOffset;
// 				// 	_sizePrinterHIMETRIC -= _sizePrinterOffset;
// 					pPrinterDC->DPtoHIMETRIC( &_sizePrinterHIMETRIC );
// 					_sizePrinterHIMETRIC.cx -= m_rcPageMarginsHM.left + m_rcPageMarginsHM.right;
// 					_sizePrinterHIMETRIC.cy -= m_rcPageMarginsHM.top + m_rcPageMarginsHM.bottom;
CSize size = pInfo->m_rectDraw.Size();
	size.cx = ::MulDiv( size.cx, nPrinterDpiX, g_PaintManager.m_nLPX );
	size.cy = ::MulDiv( size.cy, nPrinterDpiY, g_PaintManager.m_nLPY );
CRect rr( pInfo->m_rectDraw.left, pInfo->m_rectDraw.top, pInfo->m_rectDraw.left + size.cx, pInfo->m_rectDraw.top + size.cy );
	
CSize size1( rr.left, rr.top ), size2( rr.right, rr.bottom );
	pDC->LPtoHIMETRIC( &size1 );
	pDC->LPtoHIMETRIC( &size2 );
	size1.cx += m_rcPageMarginsHM.left;
	size1.cy += m_rcPageMarginsHM.top;
	size2.cx -= m_rcPageMarginsHM.right;
	size2.cy -= m_rcPageMarginsHM.bottom;
	pDC->HIMETRICtoLP( &size1 );
	pDC->HIMETRICtoLP( &size2 );
	rr.left   = size1.cx;
	rr.top    = size1.cy;
	rr.right  = size2.cx;
	rr.bottom = size2.cy;

	if( m_bUseEnhancedMetafiles )
		::PlayEnhMetaFile(
			pDC->GetSafeHdc(),
			(HENHMETAFILE)hMetafile,
			&rr // &pInfo->m_rectDraw
			);
	else
		::PlayMetaFile(
			pDC->GetSafeHdc(),
			(HMETAFILE)hMetafile
			);
	MetafileCache_UnockPage( nPageIndex );
	if( m_bPrintPreviewShowPageMargins && m_pWndPP != NULL )
	{
		ASSERT_VALID( m_pWndPP );
		ASSERT_VALID( m_pWndPP->m_pPpvPreviewDC );
// 		CRect rcPage(
// 			0,
// 			0,
// 			::GetDeviceCaps( m_pWndPP->m_pPpvPreviewDC->m_hDC, HORZRES ),
// 			::GetDeviceCaps( m_pWndPP->m_pPpvPreviewDC->m_hDC, VERTRES )
// 			);

		CSize sizeLT( m_rcPageMarginsHM.left, m_rcPageMarginsHM.top );
		CSize sizeRB( m_rcPageMarginsHM.right, m_rcPageMarginsHM.bottom );
		m_pWndPP->m_pPpvPreviewDC->HIMETRICtoLP( &sizeLT );
		m_pWndPP->m_pPpvPreviewDC->HIMETRICtoLP( &sizeRB );

		CRect rcPage = pInfo->m_rectDraw;
		m_pWndPP->m_pPpvPreviewDC->DPtoLP( &rcPage );
		rcPage.DeflateRect( sizeLT.cx, sizeLT.cy, sizeRB.cx, sizeRB.cy );

		CPen penPageMargins;
		penPageMargins.CreatePen( PS_DOT, 1, GetSysColor( COLOR_WINDOWFRAME ) );
		CPen * pOldPen = pDC->SelectObject( &penPageMargins );
		HGDIOBJ hOldBrush = ::SelectObject( pDC->m_hDC, ::GetStockObject( HOLLOW_BRUSH ) );
			CRect rcOuter = pInfo->m_rectDraw;
			CRect rcInner = rcPage;
			pDC->MoveTo( rcInner.left,  rcOuter.top );
			pDC->LineTo( rcInner.left,  rcOuter.bottom );
			pDC->MoveTo( rcInner.right, rcOuter.top );
			pDC->LineTo( rcInner.right, rcOuter.bottom );
			pDC->MoveTo( rcOuter.left,  rcInner.top );
			pDC->LineTo( rcOuter.right, rcInner.top );
			pDC->MoveTo( rcOuter.left,  rcInner.bottom );
			pDC->LineTo( rcOuter.right, rcInner.bottom );
		::SelectObject( pDC->m_hDC, hOldBrush );
		pDC->SelectObject( pOldPen );
	} // if( m_bPrintPreviewShowPageMargins && m_pWndPP != NULL )
}

void CExtPPVW_Printable::OnEndPrinting(
	CDC * pDC,
	CPrintInfo * pInfo
	)
{
	ASSERT( this != NULL );
	pDC;
	pInfo;
	MetafileCache_FreeAllPages();
}

void CExtPPVW_Printable::OnBeginPrintPreview(
	bool bPreviewMode
	)
{
	ASSERT( this != NULL );
	bPreviewMode;
}

void CExtPPVW_Printable::OnEndPrintPreview(
	CDC * pDC,
	CPrintInfo * pInfo,
	POINT point,
	CExtPPVW_HostWnd * pWndPrintPreviewHost
	)
{
	ASSERT( this != NULL );
	pDC;
	pInfo;
	point;
	pWndPrintPreviewHost;
	m_bPpvwCreated = false;
bool bRestoreFocusedState = false;

	if( m_pWndPP->GetSafeHwnd() != NULL )
	{
		if( ::GetFocus() == m_pWndPP->m_hWnd )
			bRestoreFocusedState = true;
		m_pWndPP->DestroyWindow();
		m_pWndPP = NULL;
	}
	else if( m_pWndPP != NULL )
	{
		delete m_pWndPP;
		m_pWndPP = NULL;
	}

	if( m_pWndPrintPreviewContainer->GetSafeHwnd() != NULL )
	{
		m_pWndPrintPreviewContainer->m_bPpvwCreated = false;
		m_pWndPrintPreviewContainer->DestroyWindow();
		m_pWndPrintPreviewContainer = NULL;
	}
	else if( m_pWndPrintPreviewContainer != NULL )
	{
		delete m_pWndPrintPreviewContainer;
		m_pWndPrintPreviewContainer = NULL;
	}

	if( m_bRestoreScrollBarVisibilityH )
	{
		CScrollBar * pScrollBarWndH = OnGetPrintableWnd()->GetScrollBarCtrl( SB_HORZ );
		if( pScrollBarWndH->GetSafeHwnd() != NULL && (pScrollBarWndH->GetStyle()&WS_VISIBLE) == 0 )
			pScrollBarWndH->ShowWindow( SW_SHOW );
		m_bRestoreScrollBarVisibilityH = false;
	}
	if( m_bRestoreScrollBarVisibilityV )
	{
		CScrollBar * pScrollBarWndV = OnGetPrintableWnd()->GetScrollBarCtrl( SB_VERT );
		if( pScrollBarWndV->GetSafeHwnd() != NULL && (pScrollBarWndV->GetStyle()&WS_VISIBLE) == 0 )
			pScrollBarWndV->ShowWindow( SW_SHOW );
		m_bRestoreScrollBarVisibilityV = false;
	}
	if( m_dwRestoreScrollBarStyles != 0 )
	{
		OnGetPrintableWnd()->ModifyStyle( 0, m_dwRestoreScrollBarStyles );
		OnGetPrintableWnd()->SetWindowPos(
			NULL, 0, 0, 0, 0,
			SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE|SWP_NOZORDER|SWP_NOOWNERZORDER
				|SWP_FRAMECHANGED
			);
		m_dwRestoreScrollBarStyles = 0;
	}
	if( bRestoreFocusedState )
		OnGetPrintableWnd()->SetFocus();
	MetafileCache_FreeAllPages();
}

void CExtPPVW_Printable::OnPrepareDC(
	CDC * pDC,
	CPrintInfo * pInfo // = NULL
	)
{
	ASSERT( this != NULL );
	ASSERT_VALID( pDC );
	ASSERT( pDC->GetSafeHdc() != NULL );
	pDC;
	pInfo;
}

CWnd * CExtPPVW_Printable::OnGetWndToDisableWhilePrinting()
{
	ASSERT( this != NULL );
	return ::AfxGetMainWnd();
}

void CExtPPVW_Printable::OnGetPrintableDocTitle(
	CExtSafeString & strPrintableDocTitle
	)
{
	ASSERT( this != NULL );
	strPrintableDocTitle = _T("");
CWnd * pWnd = OnGetPrintableWnd();
	if( pWnd->GetSafeHwnd() != NULL )
	{
		CString s;
		pWnd->GetWindowText( s );
		strPrintableDocTitle = LPCTSTR( s );
	}
}

void CExtPPVW_Printable::OnRecalcBars()
{
	ASSERT( this != NULL );
	if(		m_pWndPP->GetSafeHwnd() == NULL
		||	m_pWndPP->m_nPpvViewMode != __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES
		)
		return;
CScrollBar * pSb = m_pWndPP->GetScrollBarCtrl( SB_VERT );
	if( pSb->GetSafeHwnd() == NULL )
		return;
SCROLLINFO current, info;
	::memset( &current, 0, sizeof(SCROLLINFO) );
	current.cbSize = sizeof(SCROLLINFO);
	current.fMask = SIF_ALL&(~(SIF_TRACKPOS));
	::memset( &info, 0, sizeof(SCROLLINFO) );
	info.cbSize = sizeof(SCROLLINFO);
	info.fMask = SIF_ALL&(~(SIF_TRACKPOS));

	info.nMin = m_pWndPP->m_pPpvPrintPreviewInfo->GetMinPage();
	info.nMax =
		m_pWndPP->m_pPpvPrintPreviewInfo->GetMaxPage()
		- m_pWndPP->m_nPpvDisplayedPageCountDesired
		+ 1
		;
	if( info.nMax < info.nMin )
		info.nMax = info.nMin;
	//else
	//	info.nMax += ( m_pWndPP->m_pPpvPrintPreviewInfo->GetMaxPage() % m_pWndPP->m_nPpvDisplayedPageCountDesired ) ? 1 : 0;
 	info.nPage = 1;
	info.nPos = m_pWndPP->m_nPpvDisplayedPageIndex;
	if( ! pSb->GetScrollInfo( &current, SIF_ALL&(~(SIF_TRACKPOS)) ) )
	{
		pSb->GetScrollRange(
			&current.nMin,
			&current.nMax
			);
		current.nPage = 1;
		current.nPos
			= pSb->GetScrollPos();
		if( ::memcmp( &current, &info, sizeof(SCROLLINFO) ) != 0 )
		{
			pSb->SetScrollRange(
				info.nMin,
				info.nMax,
				FALSE
				);
			pSb->SetScrollPos( info.nPos );
		}
	} // if( ! pSb->GetScrollInfo( &current, SIF_ALL&(~(SIF_TRACKPOS)) ) )
	else
	{
		if( ::memcmp( &current, &info, sizeof(SCROLLINFO) ) != 0 )
			pSb->SetScrollInfo(
				&info,
				SIF_ALL
				);
	} // else from if( ! pSb->GetScrollInfo( &current, SIF_ALL&(~(SIF_TRACKPOS)) ) )
}

_AFX_WIN_STATE CExtPPVW_Printable::stat_winState;

BOOL CALLBACK CExtPPVW_Printable::stat_PrintingAbortProc( HDC, int )
{
MSG msg;
	while(
			( ! stat_winState.m_bUserAbort )
		&&	::PeekMessage( &msg, NULL, NULL, NULL, PM_NOREMOVE )
		)
	{
		if( ! ::AfxGetThread()->PumpMessage() )
			return FALSE;
	}
	return ( ! stat_winState.m_bUserAbort );
}

class CExtPPVW_Printable::_PrintingDialog : public CExtNCW < CExtResizableDialog >
{
public:
	enum
	{
		IDD = AFX_IDD_PRINTDLG,
	};
	class _DynamicLabel : public CExtLabel
	{
	protected:
		virtual void PostNcDestroy()
		{
			CExtLabel::PostNcDestroy();
			delete this;
		}
	};
	class _DynamicButton : public CExtButton
	{
	protected:
		virtual void PostNcDestroy()
		{
			CExtButton::PostNcDestroy();
			delete this;
		}
	};
	_PrintingDialog( CWnd * pParent )
	{
		VERIFY( Create( _PrintingDialog::IDD, pParent ) );
		ModifyStyle( 0, WS_CLIPCHILDREN|WS_CLIPSIBLINGS );
		HWND hWnd = ::GetWindow( m_hWnd, GW_CHILD );
		for( ; hWnd != NULL; hWnd = ::GetWindow( hWnd, GW_HWNDNEXT ) )
		{
			TCHAR sClassName[ _MAX_PATH + 1 ];
			::memset( sClassName, 0, sizeof(sClassName) );
			::GetClassName(
				hWnd,
				sClassName,
				_MAX_PATH
				);
			if( _tcsicmp( sClassName, _T("STATIC") ) == 0 )
			{
				_DynamicLabel * ptr = new _DynamicLabel;
				VERIFY( ptr->SubclassWindow( hWnd ) );
				continue;
			}
			if( _tcsicmp( sClassName, _T("BUTTON") ) == 0 )
			{
				_DynamicButton * ptr = new _DynamicButton;
				VERIFY( ptr->SubclassWindow( hWnd ) );
				continue;
			}
		} // for( ; hWnd != NULL; hWnd = ::GetWindow( hWnd, GW_HWNDNEXT ) )
		CExtPPVW_Printable::stat_winState.m_bUserAbort = FALSE;
	}
	virtual ~_PrintingDialog()
	{
	}
	virtual BOOL OnInitDialog()
	{
		SetWindowText( ::AfxGetAppName() );
		CenterWindow();
		ShowSizeGrip( FALSE );
		return CExtNCW < CExtResizableDialog > :: OnInitDialog();
	}
	virtual void OnCancel()
	{
		CExtPPVW_Printable::stat_winState.m_bUserAbort = TRUE;
		CExtNCW < CExtResizableDialog > :: OnCancel();
	}
};

BOOL CExtPPVW_Printable::DoPrintPreview(
	bool bPreviewMode,
	bool bDirectMode, // = false
	CRuntimeClass * pPreviewViewClass, // = NULL
	CPrintPreviewState * pState // = NULL
	)
{
	ASSERT( this != NULL );
	if( m_pWndPP->GetSafeHwnd() != NULL )
	{
		ASSERT_VALID( m_pWndPP );
		return TRUE;
	}
	ASSERT( m_pWndPrintPreviewContainer == NULL );
	if( ! IsPrintPreviewAvailable() )
		return FALSE;
	if( bPreviewMode )
		CExtPaintManager::stat_PassPaintMessages();
	if( pPreviewViewClass == NULL )
	{
		pPreviewViewClass = RUNTIME_CLASS( CExtPPVW_HostWnd );
	}
#if (defined _DEBUG)
	else
	{
		ASSERT( pPreviewViewClass->IsDerivedFrom( RUNTIME_CLASS( CExtPPVW_HostWnd ) ) );
	}
#endif // _DEBUG
	if( pState == NULL )
		pState = new CPrintPreviewState;
CWnd * pWndImpl = OnGetPrintableWnd();
CWnd * pMainWnd = pWndImpl->GetParentFrame();
	if( DYNAMIC_DOWNCAST( CFrameWnd, pMainWnd ) == NULL )
		pMainWnd = ::AfxGetMainWnd();
CFrameWnd * pWndParentFrame = DYNAMIC_DOWNCAST( CFrameWnd, pMainWnd );
CCreateContext context;
	context.m_pCurrentFrame = pWndParentFrame;
	context.m_pCurrentDoc = NULL;
	context.m_pLastView = NULL;
	m_pWndPP = (CExtPPVW_HostWnd*) pPreviewViewClass->CreateObject();
	if( m_pWndPP == NULL )
	{
		TRACE( _T("Error: Failed to create preview view.\n") );
		return FALSE;
	}
	ASSERT_KINDOF( CExtPPVW_HostWnd, m_pWndPP );
	m_pWndPP->m_pPpvPrintPreviewState = pState;
	m_pWndPrintPreviewContainer = new CExtPPVW_ContainerWnd;
	if( bPreviewMode )
	{
		if(	! m_pWndPrintPreviewContainer->Create(
				NULL,
				NULL,
				AFX_WS_DEFAULT_VIEW&(~(WS_BORDER))|WS_CLIPSIBLINGS|WS_CLIPCHILDREN,
				CRect(0,0,0,0),
				pWndImpl,
				AFX_IDW_PANE_FIRST
				)
			)
		{
			ASSERT( FALSE );
			delete m_pWndPP;
			m_pWndPP = NULL;
			return FALSE;
		}
		if( ! m_pWndPP->Create(
				NULL,
				NULL,
				AFX_WS_DEFAULT_VIEW&(~(WS_BORDER))|WS_CLIPSIBLINGS|WS_CLIPCHILDREN,
				CRect(0,0,0,0),
				m_pWndPrintPreviewContainer,
				AFX_IDW_PANE_FIRST,
				&context
				)
			)
		{
			TRACE( _T("Error: couldn't create preview view for frame.\n") );
			if( pWndParentFrame != NULL )
				pWndParentFrame->OnSetPreviewMode( FALSE, pState );
			m_pWndPP->m_pPpvPrintPreviewState = NULL;
			m_pWndPrintPreviewContainer->DestroyWindow();
			m_pWndPrintPreviewContainer = NULL;
			m_pWndPP = NULL;
			return FALSE;
		}
	} // if( bPreviewMode )
	m_pWndPrintPreviewContainer->m_pWndPrintPreviewHost = m_pWndPP;
	if( bPreviewMode )
	{
		CRect rcClient;
		OnGetPrintableWnd()->GetClientRect( &rcClient );
		m_pWndPrintPreviewContainer->SetWindowPos(
			&CWnd::wndTop,
			rcClient.left,
			rcClient.top,
			rcClient.Width(),
			rcClient.Height(),
			SWP_NOACTIVATE
			);
	} // if( bPreviewMode )
	m_bRestoreScrollBarVisibilityH = m_bRestoreScrollBarVisibilityV = false;
	if( bPreviewMode )
	{
		CScrollBar * pScrollBarWndH = OnGetPrintableWnd()->GetScrollBarCtrl( SB_HORZ );
		CScrollBar * pScrollBarWndV = OnGetPrintableWnd()->GetScrollBarCtrl( SB_VERT );
		if( pScrollBarWndH->GetSafeHwnd() != NULL && (pScrollBarWndH->GetStyle()&WS_VISIBLE) != 0 )
		{
			pScrollBarWndH->ShowWindow( SW_HIDE );
			m_bRestoreScrollBarVisibilityH = true;
		}
		if( pScrollBarWndV->GetSafeHwnd() != NULL && (pScrollBarWndV->GetStyle()&WS_VISIBLE) != 0 )
		{
			pScrollBarWndV->ShowWindow( SW_HIDE );
			m_bRestoreScrollBarVisibilityV = true;
		}
		m_dwRestoreScrollBarStyles = OnGetPrintableWnd()->GetStyle()&(WS_HSCROLL|WS_VSCROLL);
		if( m_dwRestoreScrollBarStyles != 0 )
		{
			OnGetPrintableWnd()->ModifyStyle( m_dwRestoreScrollBarStyles, 0 );
			OnGetPrintableWnd()->SetWindowPos(
				NULL, 0, 0, 0, 0,
				SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE|SWP_NOZORDER|SWP_NOOWNERZORDER
					|SWP_FRAMECHANGED
				);
		}
		VERIFY( g_CmdManager->ProfileWndAdd( LPCTSTR( m_strPpvCommandProfileName ), m_pWndPP->m_hWnd ) );
	} // if( bPreviewMode )
	if( bDirectMode )
	{
		CCommandLineInfo * pCmdInfo = ::AfxGetApp()->m_pCmdInfo;
		if(		pCmdInfo != NULL
			&&	pCmdInfo->m_nShellCommand == CCommandLineInfo::FilePrintTo
			)
		{
			m_pWndPP->m_pPpvPrintPreviewInfo->m_pPD->m_pd.hDC =
				::CreateDC(
					pCmdInfo->m_strDriverName,
					pCmdInfo->m_strPrinterName,
					pCmdInfo->m_strPortName,
					NULL
					);
			if( m_pWndPP->m_pPpvPrintPreviewInfo->m_pPD->m_pd.hDC == NULL )
			{
#if (!defined __EXT_MFC_NO_MSG_BOX)
				::ProfUISMsgBox( pWndImpl->GetSafeHwnd(), AFX_IDP_FAILED_TO_START_PRINT );
#else
				::AfxMessageBox( AFX_IDP_FAILED_TO_START_PRINT );
#endif
				m_pWndPP->OnPreviewClose();
				return FALSE;
			}
		}
		m_pWndPP->m_pPpvPrintPreviewInfo->m_bDirect = TRUE;
	} // if( bDirectMode )
	OnBeginPrintPreview( bPreviewMode );
	if( ! m_pWndPP->PpvSetPrintable( this ) )
	{
		m_pWndPP->OnPreviewClose();
		return TRUE;
	}
	if( bPreviewMode )
		OnInitializePrintPreviewToolBar();
	m_pWndPP->_PpvSetCurrentPage( m_pWndPP->m_pPpvPrintPreviewInfo->m_nCurPage, TRUE );
BOOL bRetVal = TRUE;
	if( bPreviewMode )
	{
		OnRecalcBars();
		m_pWndPrintPreviewContainer->OnRecalcBars();
		m_pWndPrintPreviewContainer->SetFocus();
		m_bPpvwCreated = true;
		m_pWndPrintPreviewContainer->m_bPpvwCreated = true;
		m_pWndPP->PostMessage( 0x0363 ); // WM_IDLEUPDATECMDUI
	} // if( bPreviewMode )
	else
	{
		HWND hWndOwn = OnGetPrintableWnd()->GetSafeHwnd();
		ASSERT( hWndOwn != NULL );
		bRetVal = DoPrintDoc( m_bUsePrintDialogFromOutside ? TRUE : FALSE );
		if( ! ::IsWindow( hWndOwn ) )
			return FALSE;
		if( m_pWndPP != NULL )
			m_pWndPP->OnPreviewClose();
	} // else from if( bPreviewMode )
	return TRUE;
}

BOOL CExtPPVW_Printable::DoPrintDoc(
	BOOL bUsePrintDialog // = FALSE
	)
{
	ASSERT( this != NULL );
	ASSERT( m_pWndPP->m_pPpvPrintPreviewInfo->m_pPD->m_pd.hDC != NULL );
CString strOutput;
	if(		( m_pWndPP->m_pPpvPrintPreviewInfo->m_pPD->m_pd.Flags & PD_PRINTTOFILE ) != 0
		&&	(! m_pWndPP->m_pPpvPrintPreviewInfo->m_bDocObject )
		)
	{
		CExtSafeString strDef( MAKEINTRESOURCE( AFX_IDS_PRINTDEFAULTEXT ) );
		CExtSafeString strPrintDef( MAKEINTRESOURCE( AFX_IDS_PRINTDEFAULT ) );
		CExtSafeString strFilter( MAKEINTRESOURCE( AFX_IDS_PRINTFILTER ) );
		CExtSafeString strCaption( MAKEINTRESOURCE( AFX_IDS_PRINTCAPTION ) );

#if ( ! defined __EXT_MFC_NO_SHELL_DIALOG_FILE )

		CExtShellDialogFile dlgShellFile(
			NULL,
			CExtShellDialogFile::__EFDT_SAVE_AS
			);
		dlgShellFile.m_comboFileTypes.SetFilter( strFilter );
		dlgShellFile.m_nInitialFileTypeIndex = dlgShellFile.m_comboFileTypes.FindFilterIndexByExtension( strDef );
		dlgShellFile.m_bFilesMustExist  = false;
		dlgShellFile.m_bPathMustExist   = true;
		dlgShellFile.m_bPromptOverwrite = true;
		if( dlgShellFile.DoModal() != IDOK )
		{
			m_pWndPP->OnPreviewClose();
			return FALSE;
		}
		ASSERT( dlgShellFile.m_arrRetValNames.GetSize() == 1 );
		strOutput = LPCTSTR(dlgShellFile.m_arrRetValNames[0]);

#else // ( ! defined __EXT_MFC_NO_SHELL_DIALOG_FILE )

		CFileDialog dlgFile(
			FALSE,
			strDef,
			strPrintDef,
			OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,
			strFilter
			);
		dlgFile.m_ofn.lpstrTitle = strCaption;
		if( dlgFile.DoModal() != IDOK )
		{
			m_pWndPP->OnPreviewClose();
			return FALSE;
		}
		strOutput = dlgFile.GetPathName();

#endif // else from if ( ! defined __EXT_MFC_NO_SHELL_DIALOG_FILE )

	}


	CExtSafeString strPrintableDocTitle( _T("") );
//		CDocument * pDoc = GetDocument();
//		if( pDoc != NULL )
//			strPrintableDocTitle = LPCTSTR( pDoc->GetTitle() );
//		else
//			GetParentFrame()->GetWindowText( strPrintableDocTitle );
//		if( strPrintableDocTitle.GetLength() > 31 )
//			strPrintableDocTitle.ReleaseBuffer( 31 );
	OnGetPrintableDocTitle( strPrintableDocTitle );
DOCINFO docInfo;
	::memset( &docInfo, 0, sizeof( DOCINFO ) );
	docInfo.cbSize = sizeof( DOCINFO );
	docInfo.lpszDocName = LPCTSTR(strPrintableDocTitle);
CString strPortName;
int nFormatID;
	if( strOutput.IsEmpty() )
	{
		docInfo.lpszOutput = NULL;
		strPortName = m_pWndPP->m_pPpvPrintPreviewInfo->m_pPD->GetPortName();
		nFormatID = AFX_IDS_PRINTONPORT;
	}
	else
	{
		docInfo.lpszOutput = strOutput;
		::AfxGetFileTitle( strOutput, strPortName.GetBuffer(_MAX_PATH), _MAX_PATH );
		nFormatID = AFX_IDS_PRINTTOFILE;
	}

	if( bUsePrintDialog )
	{
		CPrintDialog dlgPrint(
			FALSE //,
//			PD_ALLPAGES|PD_USEDEVMODECOPIES|PD_NOPAGENUMS|PD_HIDEPRINTTOFILE|PD_NOSELECTION,
//			OnGetPrintableWnd()
			);
		dlgPrint.m_pd = m_pWndPP->m_pPpvPrintPreviewInfo->m_pPD->m_pd;
		dlgPrint.m_pd.hInstance = ::AfxGetInstanceHandle();
#if ( ! defined _AFXDLL  )
		dlgPrint.m_pd.lpfnSetupHook = (COMMDLGPROC)_AfxCommDlgProc;
		dlgPrint.m_pd.lpfnPrintHook = (COMMDLGPROC)_AfxCommDlgProc;
#endif
		dlgPrint.m_pd.Flags =
			PD_USEDEVMODECOPIES
				|PD_HIDEPRINTTOFILE
				|PD_NOSELECTION
				|PD_DISABLEPRINTTOFILE
				|PD_HIDEPRINTTOFILE
				|PD_COLLATE
				|PD_RETURNDC
#if ( ! defined _AFXDLL  )
				|PD_ENABLEPRINTHOOK
				|PD_ENABLESETUPHOOK
#endif
				;
		if( dlgPrint.DoModal() != IDOK )
			return FALSE;
		if(		m_pWndPP->m_pPpvPrintPreviewInfo->m_pPD->m_pd.hDC != NULL
			&&	m_pWndPP->m_pPpvPrintPreviewInfo->m_pPD->m_pd.hDC != dlgPrint.m_pd.hDC
			)
			::DeleteDC( m_pWndPP->m_pPpvPrintPreviewInfo->m_pPD->m_pd.hDC );
		m_pWndPP->m_pPpvPrintPreviewInfo->m_pPD->m_pd = dlgPrint.m_pd;
	}

	m_bSendingToPrinter = true;
CDC dcPrint;
	if( ! m_pWndPP->m_pPpvPrintPreviewInfo->m_bDocObject )
	{
		dcPrint.Attach( m_pWndPP->m_pPpvPrintPreviewInfo->m_pPD->m_pd.hDC );
		dcPrint.m_bPrinting = TRUE;
	}
	OnBeginPrinting( &dcPrint, m_pWndPP->m_pPpvPrintPreviewInfo );
	if( ! m_pWndPP->m_pPpvPrintPreviewInfo->m_bDocObject )
		dcPrint.SetAbortProc( stat_PrintingAbortProc );
	CWnd * pWndToDisableWhilePrinting = OnGetWndToDisableWhilePrinting();
	if( pWndToDisableWhilePrinting->GetSafeHwnd() != NULL )
		pWndToDisableWhilePrinting->EnableWindow( FALSE );
CWnd * pWndImpl = OnGetPrintableWnd();
CExtPPVW_Printable::_PrintingDialog dlgPrintStatus( pWndImpl );
CString strTemp;
	dlgPrintStatus.SetDlgItemText(
		AFX_IDC_PRINT_DOCNAME,
		LPCTSTR(strPrintableDocTitle)
		);
	dlgPrintStatus.SetDlgItemText(
		AFX_IDC_PRINT_PRINTERNAME,
		m_pWndPP->m_pPpvPrintPreviewInfo->m_pPD->GetDeviceName()
		);
	::AfxFormatString1( strTemp, nFormatID, strPortName );
	dlgPrintStatus.SetDlgItemText( AFX_IDC_PRINT_PORTNAME, strTemp );
	dlgPrintStatus.ShowWindow( SW_SHOW );
	dlgPrintStatus.UpdateWindow();
	if(		(! m_pWndPP->m_pPpvPrintPreviewInfo->m_bDocObject )
		&&	dcPrint.StartDoc( &docInfo ) == SP_ERROR
		)
	{
		if( pWndToDisableWhilePrinting->GetSafeHwnd() != NULL )
			pWndToDisableWhilePrinting->EnableWindow( TRUE );
		OnEndPrinting( &dcPrint, m_pWndPP->m_pPpvPrintPreviewInfo );
		dlgPrintStatus.DestroyWindow();
		dcPrint.Detach();
#if (!defined __EXT_MFC_NO_MSG_BOX)
		::ProfUISMsgBox( OnGetPrintableWnd()->GetSafeHwnd(), AFX_IDP_FAILED_TO_START_PRINT );
#else
		::AfxMessageBox( AFX_IDP_FAILED_TO_START_PRINT );
#endif
		m_pWndPP->OnPreviewClose();
		m_bSendingToPrinter = false;
		return FALSE;
	}
UINT nEndPage = m_pWndPP->m_pPpvPrintPreviewInfo->GetToPage();
UINT nStartPage = m_pWndPP->m_pPpvPrintPreviewInfo->GetFromPage();
	if( nEndPage < m_pWndPP->m_pPpvPrintPreviewInfo->GetMinPage() )
		nEndPage = m_pWndPP->m_pPpvPrintPreviewInfo->GetMinPage();
	if( nEndPage > m_pWndPP->m_pPpvPrintPreviewInfo->GetMaxPage() )
		nEndPage = m_pWndPP->m_pPpvPrintPreviewInfo->GetMaxPage();
	if( nStartPage < m_pWndPP->m_pPpvPrintPreviewInfo->GetMinPage() )
		nStartPage = m_pWndPP->m_pPpvPrintPreviewInfo->GetMinPage();
	if( nStartPage > m_pWndPP->m_pPpvPrintPreviewInfo->GetMaxPage() )
		nStartPage = m_pWndPP->m_pPpvPrintPreviewInfo->GetMaxPage();
int nStep = (nEndPage >= nStartPage) ? 1 : -1;
	nEndPage = (nEndPage == 0xffff) ? 0xffff : nEndPage + nStep;
	VERIFY( strTemp.LoadString( AFX_IDS_PRINTPAGENUM ) );
BOOL bError = FALSE;
//	if( m_pWndPP->m_pPpvPrintPreviewInfo->m_bDocObject )
//	{
//		OnPrepareDC( &dcPrint, m_pWndPP->m_pPpvPrintPreviewInfo );
//		OnPrint( &dcPrint, m_pWndPP->m_pPpvPrintPreviewInfo );
//	} 
//	else
	{
		for(	m_pWndPP->m_pPpvPrintPreviewInfo->m_nCurPage = nStartPage;
				m_pWndPP->m_pPpvPrintPreviewInfo->m_nCurPage != nEndPage;
				m_pWndPP->m_pPpvPrintPreviewInfo->m_nCurPage += nStep
			)
		{
			OnPrepareDC( &dcPrint, m_pWndPP->m_pPpvPrintPreviewInfo );
			if( ! m_pWndPP->m_pPpvPrintPreviewInfo->m_bContinuePrinting )
				break;
			TCHAR szBuf[ 80 ];
			::wsprintf( szBuf, strTemp, m_pWndPP->m_pPpvPrintPreviewInfo->m_nCurPage );
			dlgPrintStatus.SetDlgItemText( AFX_IDC_PRINT_PAGENUM, szBuf );
			m_pWndPP->m_pPpvPrintPreviewInfo->m_rectDraw.SetRect(
				0,
				0,
				::GetDeviceCaps( dcPrint.m_hDC, HORZRES ),
				::GetDeviceCaps( dcPrint.m_hDC, VERTRES )
				);
			dcPrint.DPtoLP( &m_pWndPP->m_pPpvPrintPreviewInfo->m_rectDraw );
			if( dcPrint.StartPage() < 0 )
			{
				bError = TRUE;
				break;
			}
//			if( afxData.bMarked4 )
				OnPrepareDC( &dcPrint, m_pWndPP->m_pPpvPrintPreviewInfo );
			ASSERT( m_pWndPP->m_pPpvPrintPreviewInfo->m_bContinuePrinting );
			OnPrint( &dcPrint, m_pWndPP->m_pPpvPrintPreviewInfo );
			if(		dcPrint.EndPage() < 0
				||	( ! stat_PrintingAbortProc( dcPrint.m_hDC, 0 ) )
				)
			{
				bError = TRUE;
				break;
			}
		}
	}
	if( ! m_pWndPP->m_pPpvPrintPreviewInfo->m_bDocObject )
	{
		if( ! bError )
			dcPrint.EndDoc();
		else
			dcPrint.AbortDoc();
	}
	if( pWndToDisableWhilePrinting->GetSafeHwnd() != NULL )
		pWndToDisableWhilePrinting->EnableWindow( TRUE );
	OnEndPrinting( &dcPrint, m_pWndPP->m_pPpvPrintPreviewInfo );
	dlgPrintStatus.DestroyWindow();
	dcPrint.Detach();
	m_bSendingToPrinter = false;
	return TRUE;
}

bool CExtPPVW_Printable::DoReGenerateReport()
{
	ASSERT( this != NULL );
	if(		( ! m_bPpvwCreated )
		||	m_pWndPP->GetSafeHwnd() == NULL
		||	m_pWndPrintPreviewContainer->GetSafeHwnd() == NULL
		)
		return false;
	return m_pWndPP->_PpvReGenerate();
}

CRect CExtPPVW_Printable::OnMeasurePageHeaderFooterGutters( // returned CRect is in MM_HIMETRIC units
	CDC & dcMeasure
	)
{
	ASSERT( this != NULL );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	dcMeasure;
CRect rc( 0, 0, 0, 0 );
	return rc;
}

void CExtPPVW_Printable::OnDrawPageHeaderFooterGutters(
	CDC & dc,
	CRect rcEntirePage,
	CRect rcPageHeaderFooterGutters,
	INT nPageIndex,
	INT nPageCount,
	CPrintInfo * pInfo
	)
{
	ASSERT( this != NULL );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( rcEntirePage.Width()  > 0 );
	ASSERT( rcEntirePage.Height() > 0 );
	ASSERT( rcPageHeaderFooterGutters.left   >= 0 );
	ASSERT( rcPageHeaderFooterGutters.right  >= 0 );
	ASSERT( rcPageHeaderFooterGutters.top    >= 0 );
	ASSERT( rcPageHeaderFooterGutters.bottom >= 0 );
	ASSERT( 0 <= nPageIndex && nPageIndex < nPageCount );
	dc; rcEntirePage; rcPageHeaderFooterGutters; nPageIndex; nPageCount; pInfo;
}

void CExtPPVW_Printable::OnInitializePrintPreviewToolBar()
{
	ASSERT( this != NULL );
	if(		m_pWndPP->GetSafeHwnd() == NULL
		||	m_pWndPrintPreviewContainer->GetSafeHwnd() == NULL
		||	m_pWndPP->m_pPpvWndToolBar != NULL
		)
		return;
	m_pWndPP->m_pPpvWndToolBar = new CExtPPVW_ToolBar;
	m_pWndPP->m_pPpvWndToolBar->m_pWndPrintPreview = m_pWndPP;
	if( ! m_pWndPP->m_pPpvWndToolBar->Create(
			_T(""),
			m_pWndPrintPreviewContainer,
			AFX_IDW_DIALOGBAR,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS
				|CBRS_ALIGN_TOP|CBRS_TOOLTIPS
				|CBRS_FLYBY|CBRS_SIZE_DYNAMIC
			)
		)
	{
		delete m_pWndPP->m_pPpvWndToolBar;
		m_pWndPP->m_pPpvWndToolBar = NULL;
		return;
	}
	m_pWndPP->m_pPpvWndToolBar->SetWindowPos(
		&CWnd::wndTop, 0, 0, 0, 0,
		SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE
		);
	VERIFY( g_CmdManager->ProfileWndAdd( LPCTSTR( m_strPpvCommandProfileName ), m_pWndPP->m_pPpvWndToolBar->m_hWnd ) );
	m_pWndPP->m_pPpvWndToolBar->SetOwner( m_pWndPP );
	VERIFY(
		m_pWndPP->m_pPpvWndToolBar->LoadToolBar(
			IDR_EXT_TOOLBAR_PPW
			)
		);
static const UINT g_arrCmdTbbTexts[] =
		{
			//ID_EXT_PPV_PREV,
			//ID_EXT_PPV_NEXT,
			//ID_EXT_PPV_SHOW_MARGINS,
			//ID_EXT_PPV_SETUP,
			ID_EXT_PPV_PRINT,
			ID_EXT_PPV_CLOSE,
		};
	INT nButtonIndex, nButtonCount = sizeof(g_arrCmdTbbTexts)/sizeof(g_arrCmdTbbTexts[0]);
	for( nButtonIndex = 0; nButtonIndex < nButtonCount; nButtonIndex ++ )
	{
		CExtCmdItem * pCmdItem =
			g_CmdManager->CmdGetPtr(
				LPCTSTR(m_strPpvCommandProfileName),
				g_arrCmdTbbTexts[nButtonIndex]
				);
		ASSERT( pCmdItem != NULL );
		pCmdItem->m_sToolbarText = pCmdItem->m_sTipTool;
	}

CExtBarButton * pTBB;
CExtCmdItem * pCmdItem;

	pCmdItem = g_CmdManager->CmdAllocPtr( LPCTSTR(m_strPpvCommandProfileName), ID_EXT_PPV_EDIT_PAGE_NUMBER );
	if( pCmdItem == NULL )
	{
		pCmdItem = g_CmdManager->CmdGetPtr( LPCTSTR(m_strPpvCommandProfileName), ID_EXT_PPV_EDIT_PAGE_NUMBER );
		ASSERT( pCmdItem != NULL );
	}
	VERIFY( g_ResourceManager->LoadString( pCmdItem->m_sToolbarText, pCmdItem->m_nCmdID ) );
	pCmdItem->m_sTipTool = pCmdItem->m_sToolbarText;
	nButtonIndex = m_pWndPP->m_pPpvWndToolBar->CommandToIndex( ID_EXT_PPV_NEXT );
	ASSERT( nButtonIndex >= 0 );
	pTBB = new CExtBarLabelButton( m_pWndPP->m_pPpvWndToolBar, pCmdItem->m_nCmdID );
	ASSERT_VALID( pTBB );
	m_pWndPP->m_pPpvWndToolBar->InsertSpecButton( nButtonIndex, pTBB );

	pCmdItem = g_CmdManager->CmdAllocPtr( LPCTSTR(m_strPpvCommandProfileName), ID_EXT_PPV_PAGE_MODE_LABEL );
	if( pCmdItem == NULL )
	{
		pCmdItem = g_CmdManager->CmdGetPtr( LPCTSTR(m_strPpvCommandProfileName), ID_EXT_PPV_PAGE_MODE_LABEL );
		ASSERT( pCmdItem != NULL );
	}
	VERIFY( g_ResourceManager->LoadString( pCmdItem->m_sToolbarText, pCmdItem->m_nCmdID ) );
	pCmdItem->m_sTipTool = pCmdItem->m_sToolbarText;
	nButtonIndex = m_pWndPP->m_pPpvWndToolBar->CommandToIndex( ID_EXT_PPV_PAGE_MODE_1 );
	ASSERT( nButtonIndex >= 0 );
	pTBB = new CExtBarLabelButton( m_pWndPP->m_pPpvWndToolBar, pCmdItem->m_nCmdID );
	ASSERT_VALID( pTBB );
	m_pWndPP->m_pPpvWndToolBar->InsertSpecButton( nButtonIndex, pTBB );

	pCmdItem = g_CmdManager->CmdAllocPtr( LPCTSTR(m_strPpvCommandProfileName), ID_EXT_PPV_VIEW_MODE_LABEL );
	if( pCmdItem == NULL )
	{
		pCmdItem = g_CmdManager->CmdGetPtr( LPCTSTR(m_strPpvCommandProfileName), ID_EXT_PPV_VIEW_MODE_LABEL );
		ASSERT( pCmdItem != NULL );
	}
	VERIFY( g_ResourceManager->LoadString( pCmdItem->m_sToolbarText, pCmdItem->m_nCmdID ) );
	pCmdItem->m_sTipTool = pCmdItem->m_sToolbarText;
	nButtonIndex = m_pWndPP->m_pPpvWndToolBar->CommandToIndex( ID_EXT_PPV_VIEW_MODE_FIT_WIDTH );
	ASSERT( nButtonIndex >= 0 );
	pTBB = new CExtBarLabelButton( m_pWndPP->m_pPpvWndToolBar, pCmdItem->m_nCmdID );
	ASSERT_VALID( pTBB );
	m_pWndPP->m_pPpvWndToolBar->InsertSpecButton( nButtonIndex, pTBB );

// 	pCmdItem = g_CmdManager->CmdAllocPtr(LPCTSTR(m_strPpvCommandProfileName), ID_EXT_PPV_SETUP);
// 	if (pCmdItem == NULL)
// 	{
// 		pCmdItem = g_CmdManager->CmdGetPtr(LPCTSTR(m_strPpvCommandProfileName), ID_EXT_PPV_SETUP);
// 		ASSERT(pCmdItem != NULL);
// 	}
// 	VERIFY(g_ResourceManager->LoadString(pCmdItem->m_sToolbarText, pCmdItem->m_nCmdID));
// 	pCmdItem->m_sTipTool = pCmdItem->m_sToolbarText;
// 	nButtonIndex = m_pWndPP->m_pPpvWndToolBar->CommandToIndex(ID_EXT_PPV_VIEW_MODE_FIT_WIDTH);
// 	ASSERT(nButtonIndex >= 0);
// 	pTBB = new CExtBarLabelButton(m_pWndPP->m_pPpvWndToolBar, pCmdItem->m_nCmdID);
// 	ASSERT_VALID(pTBB);
// 	m_pWndPP->m_pPpvWndToolBar->InsertSpecButton(nButtonIndex, pTBB);

	pCmdItem = g_CmdManager->CmdAllocPtr( LPCTSTR(m_strPpvCommandProfileName), ID_EXT_PPV_ZOOM_LABEL );
	if( pCmdItem == NULL )
	{
		pCmdItem = g_CmdManager->CmdGetPtr( LPCTSTR(m_strPpvCommandProfileName), ID_EXT_PPV_ZOOM_LABEL );
		ASSERT( pCmdItem != NULL );
	}
	VERIFY( g_ResourceManager->LoadString( pCmdItem->m_sToolbarText, pCmdItem->m_nCmdID ) );
	pCmdItem->m_sTipTool = pCmdItem->m_sToolbarText;
	nButtonIndex = m_pWndPP->m_pPpvWndToolBar->CommandToIndex( ID_EXT_PPV_VIEW_ZOOMIN );
	ASSERT( nButtonIndex >= 0 );
	pTBB = new CExtBarLabelButton( m_pWndPP->m_pPpvWndToolBar, pCmdItem->m_nCmdID );
	ASSERT_VALID( pTBB );
	m_pWndPP->m_pPpvWndToolBar->InsertSpecButton( nButtonIndex, pTBB );

	m_pWndPP->m_pPpvWndToolBar->RemoveButton( m_pWndPP->m_pPpvWndToolBar->CommandToIndex( ID_EXT_PPV_QUICK_PRINT ), FALSE );
	m_pWndPP->m_pPpvWndToolBar->RemoveButton( m_pWndPP->m_pPpvWndToolBar->CommandToIndex( ID_EXT_PPV_PRINT_PREVIEW ), FALSE );

	m_pWndPP->m_pPpvWndToolBar->RemoveButton( m_pWndPP->m_pPpvWndToolBar->CommandToIndex( ID_EXT_PPV_SETUP ), FALSE );

	if( m_pWndPP->m_bPpvUseZoomScrollBar )
	{
		VERIFY( m_pWndPP->m_pPpvWndToolBar->InitZoomScrollBar() );
		if( m_pWndPP->m_pPpvWndToolBar->m_wndZoomScrollBar.GetSafeHwnd() != NULL )
			m_pWndPP->m_nPpvMagnifyLevel = (UINT)m_pWndPP->m_pPpvWndToolBar->m_wndZoomScrollBar.GetScrollPos();
	}
}

/////////////////////////////////////////////////////////////////////////////
// CExtPPVW_ToolBar

IMPLEMENT_DYNCREATE( CExtPPVW_ToolBar, CExtToolControlBar )

CExtPPVW_ToolBar::CExtPPVW_ToolBar()
	: m_pWndPrintPreview( NULL )
{
	m_bAppearInDockSiteControlBarPopupMenu = false;
	m_cxLeftBorder = m_cxRightBorder = m_cyTopBorder = m_cyBottomBorder = 0;
	m_bPresubclassDialogMode = true;
}

CExtPPVW_ToolBar::~CExtPPVW_ToolBar()
{
}

bool CExtPPVW_ToolBar::OnQueryMultiRowLayout() const
{
	ASSERT_VALID( this );
	return true;
}

CExtBarContentExpandButton * CExtPPVW_ToolBar::OnCreateBarRightBtn()
{
	ASSERT_VALID( this );
	return NULL;
}

bool CExtPPVW_ToolBar::InitZoomScrollBar()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
	{
		ASSERT( FALSE );
		return false;
	}
	if( m_wndZoomScrollBar.GetSafeHwnd() != NULL )
		return true;
	g_CmdManager->CmdAllocPtr(
		g_CmdManager->ProfileNameFromWnd( m_hWnd ),
		ID_EXT_PPV_ZOOM_SCROLL_BAR
		);
INT nButtonIndex;
CExtBarButton * pTBB;
	nButtonIndex = CommandToIndex( ID_EXT_PPV_VIEW_ZOOMIN );
	ASSERT( nButtonIndex >= 0 );
	pTBB = GetButton( nButtonIndex );
	ASSERT_VALID( pTBB );
	pTBB->ModifyStyle( TBBS_HIDDEN );
//	if( nButtonIndex > 0 )
//	{
//		pTBB = GetButton( nButtonIndex - 1 );
//		if( pTBB->IsSeparator() )
//			pTBB->ModifyStyle( 0, TBBS_HIDDEN );
//	}
	nButtonIndex = CommandToIndex( ID_EXT_PPV_VIEW_ZOOMOUT );
	ASSERT( nButtonIndex >= 0 );
	pTBB = GetButton( nButtonIndex );
	if( pTBB == NULL )
	{
		ASSERT( FALSE );
		return false;
	}
	ASSERT_VALID( pTBB );
	pTBB->ModifyStyle( TBBS_HIDDEN );
	nButtonIndex++;
	pTBB = OnCreateBarCommandBtn( ID_EXT_PPV_ZOOM_SCROLL_BAR );
	ASSERT_VALID( pTBB );
	if( ! InsertSpecButton( nButtonIndex, pTBB, FALSE ) )
	{
		ASSERT( FALSE );
		delete pTBB;
		return false;
	}
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
	if( ! m_wndZoomScrollBar.Create(
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS
				|SBS_HORZ|SBS_BOTTOMALIGN,
			CRect(
				0,
				0,
				pPM->UiScalingDo(
					__EXT_MFC_MAGNIFY_PX_HORZ,
					CExtPaintManager::__EUIST_X
					),
				pPM->UiScalingDo(
					__EXT_MFC_MAGNIFY_PX_VERT,
					CExtPaintManager::__EUIST_Y
					)
				),
			this,
			ID_EXT_PPV_ZOOM_SCROLL_BAR
			)
		)
	{
		ASSERT( FALSE );
		delete pTBB;
		return false;
	}
	if( ! SetButtonCtrl( nButtonIndex, &m_wndZoomScrollBar ) )
	{
		ASSERT( FALSE );
		delete pTBB;
		return false;
	}
// 	m_wndZoomScrollBar.SetScrollRange(
// 		__EXT_MFC_PRINT_PREVIEW_ZOOM_OUT,
// 		__EXT_MFC_PRINT_PREVIEW_ZOOM_IN
// 		);
// 	m_wndZoomScrollBar.SetScrollPos(
// 		( __EXT_MFC_PRINT_PREVIEW_ZOOM_IN - __EXT_MFC_PRINT_PREVIEW_ZOOM_OUT + 1 ) / 2
// 		);
SCROLLINFO _si =
		{
			sizeof(SCROLLINFO),
			SIF_ALL,
			__EXT_MFC_MAGNIFY_LEVEL_SCROLL_MIN,
			__EXT_MFC_MAGNIFY_LEVEL_SCROLL_MAX,
			__EXT_MFC_MAGNIFY_LEVEL_SCROLL_PAGE,
			__EXT_MFC_MAGNIFY_LEVEL_SCROLL_MIDDLE,
			__EXT_MFC_MAGNIFY_LEVEL_SCROLL_MIDDLE
		};
	VERIFY( m_wndZoomScrollBar.SetScrollInfo( &_si ) );
	m_wndZoomScrollBar.m_bEnabledToolTips = true;
	g_ResourceManager->LoadString(
		m_wndZoomScrollBar.m_strTipTextForDownButton,
		IDS_EXT_TT_BT_ZOOM_IN
		);
	g_ResourceManager->LoadString(
		m_wndZoomScrollBar.m_strTipTextForUpButton,
		IDS_EXT_TT_BT_ZOOM_OUT
		);
	g_ResourceManager->LoadString(
		m_wndZoomScrollBar.m_strTipTextForThumbButton,
		IDS_EXT_TT_BT_ZOOM_THUMB
		);
	return true;
}

LRESULT CExtPPVW_ToolBar::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	switch( message )
	{
	case WM_CONTEXTMENU:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
	case WM_RBUTTONDBLCLK:
		return 0;
	case WM_HSCROLL:
	case WM_VSCROLL:
		if(		m_pWndPrintPreview->GetSafeHwnd() != NULL
			&&	m_wndZoomScrollBar.GetSafeHwnd() != NULL
			&&	LPARAM(m_wndZoomScrollBar.m_hWnd) == lParam
			)
		{
			SCROLLINFO _scroll_info;
			::memset( &_scroll_info, 0, sizeof(SCROLLINFO) );
			_scroll_info.cbSize = sizeof(SCROLLINFO);
			if(	! m_wndZoomScrollBar.GetScrollInfo(
					&_scroll_info,
					SIF_ALL
					)
				)
			{
				m_wndZoomScrollBar.GetScrollRange(
					&_scroll_info.nMin,
					&_scroll_info.nMax
					);
				_scroll_info.nPage = 0;
				_scroll_info.nTrackPos
					= _scroll_info.nPos
					= m_wndZoomScrollBar.GetScrollPos();
			}
			bool bTrackPos = ( (LOWORD(wParam)) == SB_THUMBTRACK ) ? true : false;
			INT nScrollPos = bTrackPos ? _scroll_info.nTrackPos : _scroll_info.nPos;
//			LockWindowUpdate();
			m_pWndPrintPreview->PpvSetModes(
				m_pWndPrintPreview->m_nPpvPageMode,
				m_pWndPrintPreview->m_nPpvViewMode,
				UINT(nScrollPos),
				0,
				CPoint( 0, 0 )
				);
//			UnlockWindowUpdate();
			//m_pWndPrintPreview->UpdateWindow();
			return 0;
		}
	break;
	} // switch( message )
LRESULT lResult = CExtToolControlBar::WindowProc( message, wParam, lParam );
	return lResult;
}

/////////////////////////////////////////////////////////////////////////////
// CExtPPVW_ContainerWnd

IMPLEMENT_DYNCREATE( CExtPPVW_ContainerWnd, CWnd )

CExtPPVW_ContainerWnd::CExtPPVW_ContainerWnd()
	: m_pWndPrintPreviewHost( NULL )
	, m_bPpvwCreated( false )
{
}

CExtPPVW_ContainerWnd::~CExtPPVW_ContainerWnd()
{
}

BEGIN_MESSAGE_MAP( CExtPPVW_ContainerWnd, CWnd )
	//{{AFX_MSG_MAP( CExtPPVW_ContainerWnd )
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#if (defined _DEBUG)

void CExtPPVW_ContainerWnd::AssertValid() const
{
	CWnd::AssertValid();
}

void CExtPPVW_ContainerWnd::Dump( CDumpContext & dc ) const
{
	CWnd::Dump( dc );
}

#endif // (defined _DEBUG)

BOOL CExtPPVW_ContainerWnd::PreCreateWindow( CREATESTRUCT & cs )
{
	if( ! CWnd::PreCreateWindow( cs ) )
	{
		ASSERT( FALSE );
		return FALSE;
	}
WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo( hInst, __EXT_MFC_PPV_CLASS_NAME_CONTAINER, &_wndClassInfo ) )
	{
		_wndClassInfo.style = CS_HREDRAW|CS_VREDRAW;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor = ::LoadCursor( NULL, IDC_ARROW );
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_MFC_PPV_CLASS_NAME_CONTAINER;
		if( !::AfxRegisterClass( &_wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return FALSE;
		}
	}
	cs.lpszClass = __EXT_MFC_PPV_CLASS_NAME_CONTAINER;
	return TRUE;
}

BOOL CExtPPVW_ContainerWnd::PreTranslateMessage( MSG * pMsg )
{
	if(		m_pWndPrintPreviewHost->GetSafeHwnd() != NULL
		&&	m_pWndPrintPreviewHost->PreTranslateMessage( pMsg )
		)
		return TRUE;
	return CWnd::PreTranslateMessage( pMsg );
}

BOOL CExtPPVW_ContainerWnd::OnCmdMsg( UINT nID, int nCode, void * pExtra, AFX_CMDHANDLERINFO * pHandlerInfo )
{
	if(		m_bPpvwCreated
		&&	m_pWndPrintPreviewHost->GetSafeHwnd() != NULL
		&&	m_pWndPrintPreviewHost->OnCmdMsg( nID, nCode, pExtra, pHandlerInfo )
		)
		return TRUE;
	return CWnd::OnCmdMsg( nID, nCode, pExtra, pHandlerInfo );
}

void CExtPPVW_ContainerWnd::PostNcDestroy()
{
	CWnd::PostNcDestroy();
	delete this;
}

LRESULT CExtPPVW_ContainerWnd::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	switch( message )
	{
	case WM_NCCALCSIZE:
		return 0;
	case WM_SETFOCUS:
		if( m_pWndPrintPreviewHost->GetSafeHwnd() != NULL )
		{
			ASSERT_VALID( m_pWndPrintPreviewHost );
			m_pWndPrintPreviewHost->SetFocus();
			return 0;
		}
	break;
	case WM_HSCROLL:
	case WM_VSCROLL:
		if( m_pWndPrintPreviewHost->GetSafeHwnd() != NULL )
		{
			ASSERT_VALID( m_pWndPrintPreviewHost );
			return m_pWndPrintPreviewHost->SendMessage( message, wParam, lParam );
		}
	break;
	case WM_ERASEBKGND:
		return (!0);
	case WM_PAINT:
		{
			CRect rcClient;
			GetClientRect( &rcClient );
			CPaintDC dcPaint( this );
			CExtMemoryDC dc(
				&dcPaint,
				&rcClient,
				CExtMemoryDC::MDCOPT_TO_MEMORY
					//|CExtMemoryDC::MDCOPT_FILL_SURFACE
					|CExtMemoryDC::MDCOPT_RTL_COMPATIBILITY
					//|CExtMemoryDC::MDCOPT_NO_COPY_OPT
				);
			bool bTransparent = false;
			if( g_PaintManager->GetCb2DbTransparentMode(this) )
			{
				if( g_PaintManager->PaintDockerBkgnd( true, dc, this ) )
					bTransparent = true;
			} // if( PmBridge_GetPM()->GetCb2DbTransparentMode(this) )
			if( ! bTransparent )
				dc.FillSolidRect(
					&rcClient,
					g_PaintManager->GetColor(
						CExtPaintManager::CLR_3DFACE_OUT, this
						)
					);
		}
		return 0;
	case WM_PRINT:
	case WM_PRINTCLIENT:
		{
			CDC * pDC = CDC::FromHandle( (HDC)wParam );
			CRect rcClient;
			GetClientRect( &rcClient );
			CExtMemoryDC dc(
				pDC,
				&rcClient,
				CExtMemoryDC::MDCOPT_TO_MEMORY
					//|CExtMemoryDC::MDCOPT_FILL_SURFACE
					|CExtMemoryDC::MDCOPT_RTL_COMPATIBILITY
					//|CExtMemoryDC::MDCOPT_NO_COPY_OPT
				);
			bool bTransparent = false;
			if( g_PaintManager->GetCb2DbTransparentMode(this) )
			{
				if( g_PaintManager->PaintDockerBkgnd( true, dc, this ) )
					bTransparent = true;
			} // if( PmBridge_GetPM()->GetCb2DbTransparentMode(this) )
			if( ! bTransparent )
				dc.FillSolidRect(
					&rcClient,
					g_PaintManager->GetColor(
						CExtPaintManager::CLR_3DFACE_OUT, this
						)
					);
		}
		return (!0);
	} // switch( message )
LRESULT lResult = CWnd::WindowProc( message, wParam, lParam );
	switch( message )
	{
	case WM_CREATE:
		OnRecalcBars();
	break; // case WM_CREATE
	case WM_SIZE:
		OnRecalcBars();
	break; // case WM_SIZE
	} // switch( message )
	return lResult;
}

void CExtPPVW_ContainerWnd::OnRecalcBars()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
CRect rcClient;
	GetClientRect( &rcClient );
	CWnd::RepositionBars(
		0,
		0x0FFFF,
		AFX_IDW_PANE_FIRST,
		CWnd::reposDefault,
		&rcClient,
		&rcClient,
		TRUE
		);
}

/////////////////////////////////////////////////////////////////////////////
// CExtPPVW_HostWnd

IMPLEMENT_DYNCREATE( CExtPPVW_HostWnd, CScrollView )
IMPLEMENT_CExtPmBridge_MEMBERS( CExtPPVW_HostWnd );

CExtPPVW_HostWnd::CExtPPVW_HostWnd()
	: CExtNCSB < CScrollView > ( true, false )
	, m_pPpvPrintable( NULL )
	, m_pPpvPreviewDC( NULL )
	, m_pPpvWndToolBar( NULL )
	, m_bPpvUseDocumentAreaBackground( true )
	, m_bPpvUseZoomScrollBar( true )
	, m_nPpvPageMode( __EXT_MFC_PPV_PAGE_MODE_DEFAULT )
	, m_nPpvViewMode( __EXT_MFC_PPV_VIEW_MODE_DEFAULT )
	, m_nPpvMagnifyLevel( __EXT_MFC_MAGNIFY_LEVEL_MIDDLE )
	, m_pPpvPrintPreviewInfo( NULL )
	, m_pPpvPrintPreviewState( NULL )
	, m_nPpvDisplayedPageCountDesired( 1 )
	, m_nPpvDisplayedPageCountMax( __EXT_MFC_PPV_PAGE_STACK_SIZE_DEFAULT )
	, m_nPpvDisplayedPageCountCurrent( 0 )
	, m_nPpvDisplayedPageIndex( 1 )
	, m_sizePpvDisplayedPagesDimesion( 1, 1 )
	, m_nPpvDistanceBetweenPages( 10 )
	, m_hPpvMagnifyCursorIn( NULL )
	, m_hPpvMagnifyCursorOut( NULL )
	, m_sizePpvPrinterPPI( 0, 0 )
{
	m_arrPpvPI = m_arrPpvPI_default;
CExtLocalResourceHelper _LRH;
	m_hPpvMagnifyCursorIn  = ::AfxGetApp()->LoadCursor( MAKEINTRESOURCE( IDC_EXT_ZOOM_PLUS ) );
	m_hPpvMagnifyCursorOut = ::AfxGetApp()->LoadCursor( MAKEINTRESOURCE( IDC_EXT_ZOOM_MINUS ) );
	m_bCenter = TRUE;
	m_nMapMode = MM_TEXT;
	PmBridge_Install();
}

CExtPPVW_HostWnd::~CExtPPVW_HostWnd()
{
	ASSERT( m_pPpvWndToolBar->GetSafeHwnd() == NULL );
	if( m_pPpvWndToolBar != NULL )
	{
		delete m_pPpvWndToolBar;
		m_pPpvWndToolBar = NULL;
	}
	PmBridge_Uninstall();
	_PpvCleanUp();
	::SetCursor( ::LoadCursor( NULL, IDC_ARROW ) );
	if( m_hPpvMagnifyCursorIn != NULL )
		::DestroyCursor( m_hPpvMagnifyCursorIn );
	if( m_hPpvMagnifyCursorOut != NULL )
		::DestroyCursor( m_hPpvMagnifyCursorOut );
}

void CExtPPVW_HostWnd::_PpvCleanUp()
{
	if( m_dcPpvPrinter.GetSafeHdc() != NULL )
		m_dcPpvPrinter.Detach();
	if( m_pPpvPrintPreviewInfo != NULL )
	{
		delete m_pPpvPrintPreviewInfo;
		m_pPpvPrintPreviewInfo = NULL;
	}
	if( m_pPpvPrintPreviewState != NULL )
	{
		delete m_pPpvPrintPreviewState;
		m_pPpvPrintPreviewState = NULL;
	}
	if( m_pPpvPreviewDC != NULL )
	{
		delete m_pPpvPreviewDC;
		m_pPpvPreviewDC = NULL;
	}
}

BEGIN_MESSAGE_MAP( CExtPPVW_HostWnd, CScrollView )
	//{{AFX_MSG_MAP( CExtPPVW_HostWnd )
	ON_WM_SIZE()
	ON_WM_MOUSEACTIVATE()

	ON_COMMAND( ID_EXT_PPV_NEXT, OnPpv_PageNextCmd )
	ON_COMMAND( AFX_ID_PREVIEW_NEXT, OnPpv_PageNextCmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_NEXT, OnPpv_PageNextUpdate )
	ON_UPDATE_COMMAND_UI( AFX_ID_PREVIEW_NEXT, OnPpv_PageNextUpdate )
	ON_COMMAND( ID_EXT_PPV_PREV, OnPpv_PagePrevCmd )
	ON_COMMAND( AFX_ID_PREVIEW_PREV, OnPpv_PagePrevCmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_PREV, OnPpv_PagePrevUpdate )
	ON_UPDATE_COMMAND_UI( AFX_ID_PREVIEW_PREV, OnPpv_PagePrevUpdate )

	ON_COMMAND( ID_EXT_PPV_VIEW_MODE_MAGNIFY, OnPpv_ViewMode_Magnify_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_VIEW_MODE_MAGNIFY, OnPpv_ViewMode_Magnify_Update )
	ON_COMMAND( ID_EXT_PPV_VIEW_MODE_FIT_WIDTH, OnPpv_ViewMode_FitWidth_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_VIEW_MODE_FIT_WIDTH, OnPpv_ViewMode_FitWidth_Update )
	ON_COMMAND( ID_EXT_PPV_VIEW_MODE_FIT_HEIGHT, OnPpv_ViewMode_FitHeight_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_VIEW_MODE_FIT_HEIGHT, OnPpv_ViewMode_FitHeight_Update )
	ON_COMMAND( ID_EXT_PPV_VIEW_MODE_FIT_PAGES, OnPpv_ViewMode_FitPages_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_VIEW_MODE_FIT_PAGES, OnPpv_ViewMode_FitPages_Update )

	ON_COMMAND( ID_EXT_PPV_ZOOM_SCROLL_BAR, OnPpv_Zoom_SB_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_ZOOM_SCROLL_BAR, OnPpv_Zoom_SB_Update )

	ON_COMMAND( ID_EXT_PPV_VIEW_ZOOMIN, OnPpv_Magnify_In_Cmd )
	ON_COMMAND( AFX_ID_PREVIEW_ZOOMIN, OnPpv_Magnify_In_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_VIEW_ZOOMIN, OnPpv_Magnify_In_Update )
	ON_UPDATE_COMMAND_UI( AFX_ID_PREVIEW_ZOOMIN, OnPpv_Magnify_In_Update )
	ON_COMMAND( ID_EXT_PPV_VIEW_ZOOMOUT, OnPpv_Magnify_Out_Cmd )
	ON_COMMAND( AFX_ID_PREVIEW_ZOOMOUT, OnPpv_Magnify_Out_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_VIEW_ZOOMOUT, OnPpv_Magnify_Out_Update )
	ON_UPDATE_COMMAND_UI( AFX_ID_PREVIEW_ZOOMOUT, OnPpv_Magnify_Out_Update )

	ON_COMMAND( ID_EXT_PPV_PAGE_MODE_1, OnPpv_PageMode_1_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_PAGE_MODE_1, OnPpv_PageMode_1_Update )
	ON_COMMAND( ID_EXT_PPV_PAGE_MODE_2, OnPpv_PageMode_2_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_PAGE_MODE_2, OnPpv_PageMode_2_Update )
	ON_COMMAND( ID_EXT_PPV_PAGE_MODE_3, OnPpv_PageMode_3_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_PAGE_MODE_3, OnPpv_PageMode_3_Update )
	ON_COMMAND( ID_EXT_PPV_PAGE_MODE_4, OnPpv_PageMode_4_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_PAGE_MODE_4, OnPpv_PageMode_4_Update )
	ON_COMMAND( ID_EXT_PPV_PAGE_MODE_5, OnPpv_PageMode_5_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_PAGE_MODE_5, OnPpv_PageMode_5_Update )
	ON_COMMAND( ID_EXT_PPV_PAGE_MODE_6, OnPpv_PageMode_6_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_PAGE_MODE_6, OnPpv_PageMode_6_Update )
	ON_COMMAND( ID_EXT_PPV_PAGE_MODE_7, OnPpv_PageMode_7_Cmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_PAGE_MODE_7, OnPpv_PageMode_7_Update )

	ON_COMMAND( ID_EXT_PPV_PRINT, OnPreviewPrint )
	ON_COMMAND( AFX_ID_PREVIEW_PRINT, OnPreviewPrint )

	ON_COMMAND( ID_FILE_PRINT_SETUP, OnPreviewSetup )
	ON_COMMAND( ID_EXT_PPV_SETUP, OnPreviewSetup )

	ON_COMMAND( ID_EXT_PPV_SHOW_MARGINS, OnPreviewMarginsCmd )
	ON_UPDATE_COMMAND_UI( ID_EXT_PPV_SHOW_MARGINS, OnPreviewMarginsCmdUpdate )

	ON_COMMAND( ID_EXT_PPV_CLOSE, OnPreviewClose )
	ON_COMMAND( AFX_ID_PREVIEW_CLOSE, OnPreviewClose )

	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_SETCURSOR()
	//}}AFX_MSG_MAP

	ON_MESSAGE_VOID( WM_INITIALUPDATE, _OnInitialUpdate )
	ON_MESSAGE( WM_IDLEUPDATECMDUI, _OnIdleUpdateCmdUI )

END_MESSAGE_MAP()

void CExtPPVW_HostWnd::_OnInitialUpdate()
{
	_OnIdleUpdateCmdUI( TRUE, 0L );
}

LRESULT CExtPPVW_HostWnd::_OnIdleUpdateCmdUI( WPARAM wParam, LPARAM lParam )
{
	lParam;
	if(		( GetStyle() & WS_VISIBLE ) != 0
		&&	m_pPpvWndToolBar->GetSafeHwnd() != NULL
		)
		m_pPpvWndToolBar->OnUpdateCmdUI( (CFrameWnd*)this, (BOOL)wParam );
	return 0L;
}

CExtPPVW_HostWnd::PAGE_INFO::PAGE_INFO()
{
}

bool CExtPPVW_HostWnd::PpvSetPrintable( CExtPPVW_Printable * pPpvPrintable )
{
	ASSERT_VALID( this );
	ASSERT( pPpvPrintable != NULL );
	m_pPpvPrintable = pPpvPrintable;
	m_pPpvPrintPreviewInfo = new CPrintInfo;
	m_pPpvPrintPreviewInfo->m_pPD->SetHelpID( AFX_IDD_PRINTSETUP );
	m_pPpvPrintPreviewInfo->m_pPD->m_pd.Flags |= PD_PRINTSETUP;
	m_pPpvPrintPreviewInfo->m_pPD->m_pd.Flags &= ~PD_RETURNDC;
	m_pPpvPrintPreviewInfo->m_bPreview = TRUE;
	ASSERT( m_pPpvPrintPreviewInfo->m_pPD != NULL );
	m_pPpvPreviewDC = new CPreviewDC;
	if( ! m_pPpvPrintable->OnPreparePrinting( m_pPpvPrintPreviewInfo ) )
		return FALSE;
#ifdef _DEBUG
	if (m_pPpvPrintPreviewInfo->m_pPD->m_pd.hDC == NULL)
	{
		ASSERT( FALSE );
	}
#endif //_DEBUG
	m_dcPpvPrinter.Attach( m_pPpvPrintPreviewInfo->m_pPD->m_pd.hDC );
	m_pPpvPreviewDC->SetAttribDC( m_pPpvPrintPreviewInfo->m_pPD->m_pd.hDC );
	m_pPpvPreviewDC->m_bPrinting = TRUE;
	m_dcPpvPrinter.m_bPrinting = TRUE;
	m_dcPpvPrinter.SaveDC();
HDC hDC = ::GetDC(m_hWnd);
	m_pPpvPreviewDC->SetOutputDC( hDC );
	m_pPpvPrintable->OnBeginPrinting( m_pPpvPreviewDC, m_pPpvPrintPreviewInfo );
	m_pPpvPreviewDC->ReleaseOutputDC();
	::ReleaseDC( m_hWnd, hDC );
	m_dcPpvPrinter.RestoreDC( -1 );
	m_sizePpvPrinterPPI.cx = ::GetDeviceCaps( m_dcPpvPrinter.m_hDC, LOGPIXELSX );
	m_sizePpvPrinterPPI.cy = ::GetDeviceCaps( m_dcPpvPrinter.m_hDC, LOGPIXELSY );


//WORD nPageNoMin = 1, nPageNoMax = 1;
//m_pPpvPrintPreviewInfo->m_nNumPreviewPages = nPageNoMax;
//m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMinPage = nPageNoMin;
//m_pPpvPrintPreviewInfo->m_pPD->m_pd.nFromPage = nPageNoMin;
//m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMaxPage = nPageNoMax;
//m_pPpvPrintPreviewInfo->m_pPD->m_pd.nToPage = nPageNoMax;

	if( m_nPpvDisplayedPageCountCurrent == 0 )
		m_nPpvDisplayedPageCountCurrent = 1;
	else if ( m_nPpvDisplayedPageCountCurrent > m_nPpvDisplayedPageCountMax )
		m_nPpvDisplayedPageCountCurrent = m_nPpvDisplayedPageCountMax;

///	m_nPpvDisplayedPageCountDesired = m_nPpvDisplayedPageCountCurrent;
	m_nPpvDisplayedPageCountDesired = 1;

	SetScrollSizes( MM_TEXT, CSize( 1, 1 ) );
	_PpvSetCurrentPage( m_pPpvPrintPreviewInfo->m_nCurPage, TRUE );
	m_pPpvPrintable->OnRecalcBars();
	return TRUE;
}

void CExtPPVW_HostWnd::OnSize( UINT nType, int cx, int cy )
{
	ASSERT_VALID( this );
	if( m_nPpvViewMode != __EXT_MFC_PPV_VIEW_MODE_MAGNIFY )
	{
		for( UINT i = 0; i < m_nPpvDisplayedPageCountMax; i++ )
			m_arrPpvPI[i].sizeScaleRatio.cx = 0;
		CView::OnSize( nType, cx, cy );
	}
	else
	{
		m_pageDev.cx = cx;
		m_pageDev.cy = cy;
		m_lineDev.cx = cx / 10;
		m_lineDev.cy = cy / 10;
		CExtNCSB < CScrollView > :: OnSize(nType, cx, cy);
	}
	for( UINT nPage = 0; nPage < m_nPpvDisplayedPageCountCurrent; nPage++ )
	{
		CSize * pRatio = &m_arrPpvPI[nPage].sizeScaleRatio;
		if( pRatio->cx == 0 )
			PositionPage( nPage );
	}
	if( m_pPpvPrintable != NULL )
		m_pPpvPrintable->OnRecalcBars();
}

void CExtPPVW_HostWnd::OnPreviewMarginsCmd()
{
	ASSERT_VALID( this );
	if( m_pPpvPrintable == NULL )
		return;
	m_pPpvPrintable->m_bPrintPreviewShowPageMargins = ( ! m_pPpvPrintable->m_bPrintPreviewShowPageMargins );
	Invalidate();
}

void CExtPPVW_HostWnd::OnPreviewMarginsCmdUpdate( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	if( m_pPpvPrintable == NULL )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetCheck( 0 );
		return;
	}
	pCmdUI->Enable( TRUE );
	pCmdUI->SetCheck( m_pPpvPrintable->m_bPrintPreviewShowPageMargins ? 1 : 0 );
}

void CExtPPVW_HostWnd::OnPreviewClose()
{
	ASSERT_VALID( this );
	m_pPpvPrintPreviewInfo->m_nCurPage = m_nPpvDisplayedPageIndex;
	m_pPpvPrintable->OnEndPrintPreview(
		m_pPpvPreviewDC,
		m_pPpvPrintPreviewInfo,
		CPoint( 0, 0 ),
		this
		);
}

#define PREVIEW_MARGIN 8

CSize CExtPPVW_HostWnd::CalcScaleRatio( CSize screenSize, CSize actualSize )
{
	ASSERT_VALID( this );
int nNum = screenSize.cy;
int nDen = actualSize.cy;
	if( ::MulDiv( actualSize.cx, nNum, nDen ) > screenSize.cx )
	{
		nNum = screenSize.cx;
		nDen = actualSize.cx;
	}
CSize ratio( nNum, nDen );
	return ratio;
}

void CExtPPVW_HostWnd::PositionPage( UINT nPage )
{
	ASSERT_VALID( this );
CSize windowSize = _PpvCalcPageScringMetrics();
	VERIFY(
		m_dcPpvPrinter.Escape(
			GETPHYSPAGESIZE,
			0,
			NULL,
			(LPVOID)&m_arrPpvPI[nPage].sizeUnscaled
			)
		);
CSize * pSize = &m_arrPpvPI[nPage].sizeUnscaled;
	pSize->cx = ::MulDiv( pSize->cx, g_PaintManager.m_nLPX, m_sizePpvPrinterPPI.cx );
	pSize->cy = ::MulDiv( pSize->cy, g_PaintManager.m_nLPY, m_sizePpvPrinterPPI.cy );
	m_arrPpvPI[nPage].sizeZoomOutRatio = CalcScaleRatio( windowSize, *pSize );
	_PpvScalePageMetrics( nPage );
}

CSize CExtPPVW_HostWnd::_PpvCalcPageScringMetrics()
{
	ASSERT_VALID( this );
CRect rcClient;
	GetClientRect( &rcClient );
CSize windowSize = rcClient.Size();

	windowSize.cx -= PREVIEW_MARGIN * 2;
	windowSize.cx -= ( m_sizePpvDisplayedPagesDimesion.cx + 1 ) * m_nPpvDistanceBetweenPages;
	windowSize.cx -= 10; // shadow
	if( windowSize.cx > 0 )
		windowSize.cx /= m_sizePpvDisplayedPagesDimesion.cx;
	windowSize.cx = max( windowSize.cx, 4 );

	windowSize.cy -= PREVIEW_MARGIN * 2;
	windowSize.cy -= ( m_sizePpvDisplayedPagesDimesion.cy + 1 ) * m_nPpvDistanceBetweenPages;
	windowSize.cy -= 10; // shadow
	if( windowSize.cy > 0 )
		windowSize.cy /= m_sizePpvDisplayedPagesDimesion.cy;
	windowSize.cy = max( windowSize.cy, 4 );

	return windowSize;
}

void CExtPPVW_HostWnd::_PpvScalePageMetrics( UINT nPage )
{
	ASSERT_VALID( this );
UINT _nPpvViewMode = m_nPpvViewMode;
	if( _nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES )
	{
		_PpvScalePageMetricsXY( nPage, __EXT_MFC_PPV_VIEW_MODE_FIT_WIDTH, false );
		double lfXR = double(m_arrPpvPI[nPage].sizeScaleRatio.cx) / double(m_arrPpvPI[nPage].sizeScaleRatio.cy);
		_PpvScalePageMetricsXY( nPage, __EXT_MFC_PPV_VIEW_MODE_FIT_HEIGHT, false );
		double lfYR = double(m_arrPpvPI[nPage].sizeScaleRatio.cx) / double(m_arrPpvPI[nPage].sizeScaleRatio.cy);
		if( lfXR < lfYR )
			_PpvScalePageMetricsXY( nPage, __EXT_MFC_PPV_VIEW_MODE_FIT_WIDTH, true );
		else
			_PpvScalePageMetricsXY( nPage, __EXT_MFC_PPV_VIEW_MODE_FIT_HEIGHT, true );
	}
	else
		_PpvScalePageMetricsXY( nPage, m_nPpvViewMode, true );
}

void CExtPPVW_HostWnd::_PpvScalePageMetricsXY( UINT nPage, UINT _nPpvViewMode, bool bApply )
{
	ASSERT_VALID( this );
CSize * pSize = &m_arrPpvPI[nPage].sizeUnscaled;
CSize * pRatio = &m_arrPpvPI[nPage].sizeScaleRatio;
CSize * pZoomOutRatio = &m_arrPpvPI[nPage].sizeZoomOutRatio;
CSize windowSize = _PpvCalcPageScringMetrics();
BOOL bPaperLarger = pZoomOutRatio->cx < pZoomOutRatio->cy;
CRect rcClient;
	GetClientRect( &rcClient );
CSize sizeClient = rcClient.Size();
	switch( _nPpvViewMode )
	{
	case __EXT_MFC_PPV_VIEW_MODE_FIT_WIDTH:
		pRatio->cx =
			sizeClient.cx
			- PREVIEW_MARGIN * 2
			- m_nPpvDistanceBetweenPages * m_sizePpvDisplayedPagesDimesion.cx
			;
		pRatio->cx = max( pRatio->cx, 0 );
		pRatio->cy = pSize->cx + m_nPpvDistanceBetweenPages;
		pRatio->cy *= m_sizePpvDisplayedPagesDimesion.cx;
		pRatio->cy += PREVIEW_MARGIN * 4;
	break;
	case __EXT_MFC_PPV_VIEW_MODE_FIT_HEIGHT:
		pRatio->cx =
			sizeClient.cy
			- PREVIEW_MARGIN * 2
			- m_nPpvDistanceBetweenPages * m_sizePpvDisplayedPagesDimesion.cy
			;
		pRatio->cx = max( pRatio->cx, 0 );
		pRatio->cy = pSize->cy + m_nPpvDistanceBetweenPages;
		pRatio->cy *= m_sizePpvDisplayedPagesDimesion.cy;
		pRatio->cy += PREVIEW_MARGIN * 4;
	break;
	case __EXT_MFC_PPV_VIEW_MODE_MAGNIFY:
		switch( m_nPpvMagnifyLevel )
		{
		case __EXT_MFC_MAGNIFY_LEVEL_0:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) / 10;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx - pRatio->cy ) / 6;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_1:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) / 9;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx - pRatio->cy ) / 5;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_2:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) / 8;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx - pRatio->cy ) / 4;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_3:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) / 7;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx - pRatio->cy ) / 3;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_4:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) / 6;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx - pRatio->cy ) / 2;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_5:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) / 5;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( 2*pZoomOutRatio->cx - pRatio->cy ) / 2;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_6:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) / 4;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( 3*pZoomOutRatio->cx - pRatio->cy ) / 2;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_7:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) / 3;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( 4*pZoomOutRatio->cx - pRatio->cy ) / 3;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_8:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) / 2;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( 3*pZoomOutRatio->cx - pRatio->cy ) / 2;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_9:
			if( bPaperLarger )
				pRatio->cx = pRatio->cy = 1;
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = 2*pZoomOutRatio->cx - pZoomOutRatio->cy;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_10:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) * 3 / 2;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = 3*pZoomOutRatio->cx - pZoomOutRatio->cy;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_11:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) * 2;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = 4*pZoomOutRatio->cx - pZoomOutRatio->cy;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_12:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) * 3;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = 5*pZoomOutRatio->cx - pZoomOutRatio->cy;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_13:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) * 4;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = 7*pZoomOutRatio->cx - pZoomOutRatio->cy;
			}
		break;
		case __EXT_MFC_MAGNIFY_LEVEL_14:
			if( bPaperLarger )
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = ( pZoomOutRatio->cx + pRatio->cy ) * 5;
			}
			else
			{
				pRatio->cy = pZoomOutRatio->cy;
				pRatio->cx = 7*pZoomOutRatio->cx - pZoomOutRatio->cy;
			}
		break;
#ifdef _DEBUG
		default:
			ASSERT(FALSE);
		break;
#endif // _DEBUG
		} // switch( m_nPpvMagnifyLevel )
	break;
#ifdef _DEBUG
	default:
		ASSERT(FALSE);
	break;
#endif // _DEBUG
	} // switch( switch( _nPpvViewMode ) )
CSize scaledSize(
		::MulDiv( pSize->cx, pRatio->cx, pRatio->cy ),
		::MulDiv( pSize->cy, pRatio->cx, pRatio->cy )
		);
CRect * pRect = &m_arrPpvPI[nPage].rectScreen;
	pRect->SetRect(
		PREVIEW_MARGIN,
		PREVIEW_MARGIN,
		scaledSize.cx + PREVIEW_MARGIN + 3,
		scaledSize.cy + PREVIEW_MARGIN + 3
		);

UINT nPageExX = nPage, nPageExY = 0;
	if(		m_nPpvPageMode >= __EXT_MFC_PPV_PAGE_MODE_5
		&&	nPage >= UINT( m_sizePpvDisplayedPagesDimesion.cx )
		)
	{
		nPageExX -= UINT( m_sizePpvDisplayedPagesDimesion.cx );
		nPageExY ++;
	}
INT nOffsetX = 0, nOffsetY = 0;
CSize sizePage = pRect->Size();
CSize sizeAllPages(
		( sizePage.cx + m_nPpvDistanceBetweenPages ) * m_sizePpvDisplayedPagesDimesion.cx,
		( sizePage.cy + m_nPpvDistanceBetweenPages ) * m_sizePpvDisplayedPagesDimesion.cy
		);
CSize sizeOuterRest(
		( sizeClient.cx - sizeAllPages.cx - PREVIEW_MARGIN * 2 ),
		( sizeClient.cy - sizeAllPages.cy - PREVIEW_MARGIN * 2 )
		);
	if( _nPpvViewMode != __EXT_MFC_PPV_VIEW_MODE_MAGNIFY )
	{
		if( _nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_WIDTH )
		{
			nOffsetX += sizeOuterRest.cx / m_sizePpvDisplayedPagesDimesion.cx - 1;
			if( nOffsetX < 0 )
				nOffsetX = 0;
		}
		if( _nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_HEIGHT )
		{
			nOffsetY += sizeOuterRest.cy / m_sizePpvDisplayedPagesDimesion.cy - 1;
			if( nOffsetY < 0 )
				nOffsetY = 0;
		}
	} // if( _nPpvViewMode != __EXT_MFC_PPV_VIEW_MODE_MAGNIFY )
	if( sizeOuterRest.cx > 0 )
		nOffsetX += sizeOuterRest.cx / 2;
	if( sizeOuterRest.cy > 0 )
		nOffsetY += sizeOuterRest.cy / 2;
 	pRect->OffsetRect(
		nOffsetX + ( sizePage.cx + m_nPpvDistanceBetweenPages ) * nPageExX,
		nOffsetY + ( sizePage.cy + m_nPpvDistanceBetweenPages ) * nPageExY
		);
	if(		m_sizePpvDisplayedPagesDimesion.cx == 1
		&&	m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_WIDTH
		)
	{
		pRect->OffsetRect(
			( rcClient.Width() - pRect->Width() ) / 2 - pRect->left,
			0
			);
	}

	if( ! bApply )
		return;

	if( m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES )
	{
	}
	else
	{
		CSize _size(
			( sizeOuterRest.cx >= 0 )
				? sizeClient.cx
				: ( ( sizePage.cx + m_nPpvDistanceBetweenPages ) * m_sizePpvDisplayedPagesDimesion.cx + PREVIEW_MARGIN * 2 )
				,
			( sizeOuterRest.cy >= 0 )
				? sizeClient.cy
				: ( ( sizePage.cy + m_nPpvDistanceBetweenPages ) * m_sizePpvDisplayedPagesDimesion.cy + PREVIEW_MARGIN * 2 )
			);
		SetScrollSizes(
			MM_TEXT,
			_size,
			windowSize
			);
	} // else from else if( _nPpvViewMode != __EXT_MFC_PPV_VIEW_MODE_MAGNIFY )
}

void CExtPPVW_HostWnd::OnPrepareDC( CDC * pDC, CPrintInfo * pInfo )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC );
	if( m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES )
		CView::OnPrepareDC( pDC, pInfo );
	else if( m_arrPpvPI[0].sizeScaleRatio.cx != 0 )
		CExtNCSB < CScrollView > :: OnPrepareDC( pDC, pInfo );
}

BOOL CExtPPVW_HostWnd::OnScrollBy(
	CSize sizeScroll,
	BOOL bDoScroll //= TRUE
	)
{
	ASSERT_VALID( this );
int xOrig, x, yOrig, y;
DWORD dwStyle = GetStyle();
CScrollBar * pBar = GetScrollBarCtrl( SB_VERT );
	if(		( pBar != NULL && ( ! pBar->IsWindowEnabled() ) )
		||	( pBar == NULL && (dwStyle&WS_VSCROLL) == 0 )
		)
		sizeScroll.cy = 0;
	pBar = GetScrollBarCtrl( SB_HORZ );
	if(		( pBar != NULL && ( ! pBar->IsWindowEnabled() ) )
		||	( pBar == NULL && (dwStyle&WS_HSCROLL) == 0 )
		)
		sizeScroll.cx = 0;
	xOrig = x = GetScrollPos( SB_HORZ );
int xMax = GetScrollLimit( SB_HORZ );
	x += sizeScroll.cx;
	if( x < 0 )
		x = 0;
	else if ( x > xMax )
		x = xMax;
	yOrig = y = GetScrollPos(SB_VERT);
int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if( y < 0 )
		y = 0;
	else if( y > yMax )
		y = yMax;
	if( x == xOrig && y == yOrig )
		return FALSE;
	if( bDoScroll )
	{
		//if( m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES )
			Invalidate();
		//else
		//	ScrollWindow( -( x - xOrig ), -( y - yOrig ) );
		if( x != xOrig )
			SetScrollPos( SB_HORZ, x );
		if (y != yOrig)
			SetScrollPos( SB_VERT, y );
	}
	return TRUE;
}

BOOL CExtPPVW_HostWnd::PreTranslateMessage( MSG * pMsg )
{
	if(		pMsg->message == WM_KEYDOWN
		&&	pMsg->wParam == VK_ESCAPE
		&&	GetSafeHwnd() != NULL
		)
	{
		OnPreviewClose();
		return TRUE;
	}
	return CExtNCSB < CScrollView > :: PreTranslateMessage( pMsg );
}

BOOL CExtPPVW_HostWnd::PreCreateWindow( CREATESTRUCT & cs )
{
	ASSERT_VALID( this );
	if( ! CExtNCSB < CScrollView > :: PreCreateWindow( cs ) )
	{
		ASSERT( FALSE );
		return FALSE;
	}
WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo( hInst, __EXT_MFC_PPV_CLASS_NAME_HOST, &_wndClassInfo ) )
	{
		_wndClassInfo.style = CS_HREDRAW|CS_VREDRAW;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor = ::LoadCursor( NULL, IDC_ARROW );
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_MFC_PPV_CLASS_NAME_HOST;
		if( !::AfxRegisterClass( &_wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return FALSE;
		}
	}
	cs.lpszClass = __EXT_MFC_PPV_CLASS_NAME_HOST;
	return TRUE;
}

CScrollBar * CExtPPVW_HostWnd::GetScrollBarCtrl( int nBar ) const
{
	ASSERT_VALID( this );
	return GetParent()->GetScrollBarCtrl( nBar );
}

LRESULT CExtPPVW_HostWnd::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	switch( message )
	{
	case WM_ERASEBKGND:
		return (!0);
	case WM_PAINT:
		{
			CRect rcClient;
			GetClientRect( &rcClient );
			CPaintDC dcPaint( this );
			CExtMemoryDC dc(
				&dcPaint,
				&rcClient,
				CExtMemoryDC::MDCOPT_TO_MEMORY
					//|CExtMemoryDC::MDCOPT_FILL_SURFACE
					|CExtMemoryDC::MDCOPT_RTL_COMPATIBILITY
					//|CExtMemoryDC::MDCOPT_NO_COPY_OPT
				);
			bool bTransparent = false;
			if( PmBridge_GetPM()->GetCb2DbTransparentMode(this) )
			{
				if( m_bPpvUseDocumentAreaBackground )
				{
					if( PmBridge_GetPM()->PaintDocumentClientAreaBkgnd( dc, this ) )
						bTransparent = true;
				}
				else
				{
					if( PmBridge_GetPM()->PaintDockerBkgnd( true, dc, this ) )
						bTransparent = true;
				}
			} // if( PmBridge_GetPM()->GetCb2DbTransparentMode(this) )
			if( ! bTransparent )
				dc.FillSolidRect(
					&rcClient,
					PmBridge_GetPM()->GetColor(
						CExtPaintManager::CLR_3DFACE_OUT, this
						)
					);
			if(		m_pPpvPrintable != NULL
				&&	( ! m_pPpvPrintable->m_bSendingToPrinter )
				)
			{
				CSize windowSize = _PpvCalcPageScringMetrics();
				INT nMinWidth =
						(	windowSize.cx
						+	m_nPpvDistanceBetweenPages
						) * m_sizePpvDisplayedPagesDimesion.cx
					+	PREVIEW_MARGIN * 2
					;
				INT nMinHeight =
						(	windowSize.cy
						+	m_nPpvDistanceBetweenPages
						) * m_sizePpvDisplayedPagesDimesion.cy
					+	PREVIEW_MARGIN * 2
					;
				if(		rcClient.Width() > nMinWidth
					&&	rcClient.Height() > nMinHeight
					)
				{
					dc.SaveDC();
					dc.SetViewportOrg( 0, 0 );
					OnPrepareDC( &dc );
					OnDraw( &dc );
					dc.RestoreDC( -1 );
				}
			}
		}
	return 0;
	case WM_PRINT:
	case WM_PRINTCLIENT:
		{
			CDC * pDC = CDC::FromHandle( (HDC)wParam );
			CRect rcClient;
			GetClientRect( &rcClient );
			CExtMemoryDC dc(
				pDC,
				&rcClient,
				CExtMemoryDC::MDCOPT_TO_MEMORY
					//|CExtMemoryDC::MDCOPT_FILL_SURFACE
					|CExtMemoryDC::MDCOPT_RTL_COMPATIBILITY
					//|CExtMemoryDC::MDCOPT_NO_COPY_OPT
				);
			bool bTransparent = false;
			if( PmBridge_GetPM()->GetCb2DbTransparentMode(this) )
			{
				if( m_bPpvUseDocumentAreaBackground )
				{
					if( PmBridge_GetPM()->PaintDocumentClientAreaBkgnd( dc, this ) )
						bTransparent = true;
				}
				else
				{
					if( PmBridge_GetPM()->PaintDockerBkgnd( true, dc, this ) )
						bTransparent = true;
				}
			} // if( PmBridge_GetPM()->GetCb2DbTransparentMode(this) )
			if( ! bTransparent )
				dc.FillSolidRect(
					&rcClient,
					PmBridge_GetPM()->GetColor(
						CExtPaintManager::CLR_3DFACE_OUT, this
						)
					);
			CSize windowSize = _PpvCalcPageScringMetrics();
			INT nMinWidth =
					(	windowSize.cx
					+	m_nPpvDistanceBetweenPages
					) * m_sizePpvDisplayedPagesDimesion.cx
				+	PREVIEW_MARGIN * 2
				;
			INT nMinHeight =
					(	windowSize.cy
					+	m_nPpvDistanceBetweenPages
					) * m_sizePpvDisplayedPagesDimesion.cy
				+	PREVIEW_MARGIN * 2
				;
			if(		rcClient.Width() > nMinWidth
				&&	rcClient.Height() > nMinHeight
				)
			{
				dc.SaveDC();
				dc.SetViewportOrg( 0, 0 );
				OnPrepareDC( &dc );
				OnDraw( &dc );
				dc.RestoreDC( -1 );
			}
		}
	return (!0);
	case WM_HSCROLL:
	case WM_VSCROLL:
		Invalidate();
	break;
	case WM_DESTROY:
		if( m_pPpvWndToolBar->GetSafeHwnd() != NULL )
		{
			m_pPpvWndToolBar->DestroyWindow();
			delete m_pPpvWndToolBar;
			m_pPpvWndToolBar = NULL;
		}
	break;
	case WM_CONTEXTMENU:
		return 0;
	} // switch( message )
LRESULT lResult = CExtNCSB < CScrollView > :: WindowProc( message, wParam, lParam );
	switch( message )
	{
	case WM_CREATE:
		{
			NCSB_EnsureContainersCreated();
			CExtNCSB_ScrollContainer * pWnd;
			pWnd = NCSB_GetContainer( CExtNCSB_ScrollContainer::__EM_HORIZONTAL_SCROLL_BAR );
			if( pWnd != NULL )
			{
				CExtScrollBar * pSB = pWnd->GetScrollBarInContainer();
				if( pSB != NULL )
					pSB->m_bHelperLightAccent = (! m_bPpvUseDocumentAreaBackground );
			}
			pWnd = NCSB_GetContainer( CExtNCSB_ScrollContainer::__EM_VERTICAL_SCROLL_BAR );
			if( pWnd != NULL )
			{
				CExtScrollBar * pSB = pWnd->GetScrollBarInContainer();
				if( pSB != NULL )
					pSB->m_bHelperLightAccent = (! m_bPpvUseDocumentAreaBackground );
			}
		}
	break; // case WM_CREATE
	} // switch( message )
	return lResult;
}

void CExtPPVW_HostWnd::OnDraw( CDC * pDC )
{
	ASSERT_VALID( pDC );
	if(		m_pPpvPrintable == NULL
		||	m_dcPpvPrinter.m_hDC == NULL
		)
		return; // init is not complete
CPoint ptVpOrg = pDC->GetViewportOrg();
CPen penPageBorder;
	penPageBorder.CreatePen( PS_SOLID, 2, GetSysColor( COLOR_WINDOWFRAME ) );
	m_pPpvPrintPreviewInfo->m_bContinuePrinting = TRUE;
	UINT nAbsolutePageRange =
		UINT(m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMaxPage)
		- UINT(m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMinPage)
		+ 1;
	for( UINT nPage = 0; nPage < m_nPpvDisplayedPageCountCurrent; nPage++ )
	{
		UINT nAbsolutePageIndex = m_nPpvDisplayedPageIndex + nPage;
		if( nAbsolutePageIndex > nAbsolutePageRange )
			break;
		int nSavedState = m_dcPpvPrinter.SaveDC();
		m_pPpvPreviewDC->SetOutputDC( pDC->GetSafeHdc() );
		m_pPpvPrintPreviewInfo->m_nCurPage = m_nPpvDisplayedPageIndex + nPage;
		if(		(m_nPpvDisplayedPageIndex + nPage) >= m_nPpvDisplayedPageIndex
			&&	(m_nPpvDisplayedPageIndex + nPage) >= nPage
			&&	(m_nPpvDisplayedPageIndex + nPage) <= m_pPpvPrintPreviewInfo->GetMaxPage()
			)
			m_pPpvPrintable->OnPrepareDC( m_pPpvPreviewDC, m_pPpvPrintPreviewInfo );
		m_pPpvPrintPreviewInfo->m_rectDraw.SetRect(
			0,
			0,
			m_pPpvPreviewDC->GetDeviceCaps( HORZRES ),
			m_pPpvPreviewDC->GetDeviceCaps( VERTRES )
			);
		m_pPpvPreviewDC->DPtoLP( &m_pPpvPrintPreviewInfo->m_rectDraw );
		pDC->SaveDC();
		CSize * pRatio = &m_arrPpvPI[nPage].sizeScaleRatio;
		CRect * pRect = &m_arrPpvPI[nPage].rectScreen;
		if( pRatio->cx == 0 )
		{
			PositionPage( nPage );
			if( m_nPpvViewMode != __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES )
			{
				ptVpOrg = - GetDeviceScrollPosition();
				if( m_bCenter )
				{
					CRect rect;
					GetClientRect(&rect);
					if( m_totalDev.cx < rect.Width() )
						ptVpOrg.x = ( rect.Width() - m_totalDev.cx ) / 2;
					if( m_totalDev.cy < rect.Height() )
						ptVpOrg.y = ( rect.Height() - m_totalDev.cy ) / 2;
				}
			}
		}
		pDC->SetMapMode( MM_TEXT );
		pDC->SetViewportOrg( ptVpOrg );
		pDC->SetWindowOrg( 0, 0 );
		if(		pRect->left < pRect->right
			&&	pRect->top < pRect->bottom
			)
		{
//			pDC->SelectStockObject(HOLLOW_BRUSH);
//			CPen * pOldPen = pDC->SelectObject( &penPageBorder) ;
//			pDC->Rectangle(pRect);
//			pDC->SelectObject( pOldPen );
//			UINT nShadowSize = CExtWndShadow::DEF_SHADOW_SIZE*2;
//			if(		nShadowSize < UINT( pRect->Width() / 2 )
//				&&	nShadowSize < UINT( pRect->Height() / 2 )
//				)
//			{
//				CExtWndShadow _shadow;
//				_shadow.Paint(
//					NULL,
//					*pDC, *pRect, CRect(0,0,0,0), CRect(0,0,0,0),
//					nShadowSize,
//					CExtWndShadow::DEF_BRIGHTNESS_MIN, CExtWndShadow::DEF_BRIGHTNESS_MAX,
//					false
//					);
//			}
			CRect rectFill = *pRect;
			rectFill.left += 1;
			rectFill.top += 1;
			rectFill.right -= 2;
			rectFill.bottom -= 2;
			::FillRect( pDC->m_hDC, rectFill, (HBRUSH)::GetStockObject( WHITE_BRUSH ) );
		}
		pDC->RestoreDC( -1 );
		pDC->SaveDC();
		/*if(		( ! m_pPpvPrintPreviewInfo->m_bContinuePrinting )
			||	( m_nPpvDisplayedPageIndex + nPage ) > m_pPpvPrintPreviewInfo->GetMaxPage()
			)
		{
			m_pPpvPreviewDC->ReleaseOutputDC();
			m_dcPpvPrinter.RestoreDC( nSavedState );
			if( nPage == 0 && m_nPpvDisplayedPageIndex > 1 )
				_PpvSetCurrentPage( m_nPpvDisplayedPageIndex - 1, TRUE );
			break;
		}*/
		OnDisplayPageNumber( m_nPpvDisplayedPageIndex, nPage + 1 );
		m_pPpvPreviewDC->SetScaleRatio( pRatio->cx, pRatio->cy );
		CSize sizePrintOffset;
		VERIFY(
			m_pPpvPreviewDC->Escape(
				GETPRINTINGOFFSET,
				0,
				NULL,
				(LPVOID)&sizePrintOffset
				)
			);
		m_pPpvPreviewDC->PrinterDPtoScreenDP(
			(LPPOINT)&sizePrintOffset
			);
		sizePrintOffset += (CSize)pRect->TopLeft();
		sizePrintOffset += CSize( 1, 1 );
		sizePrintOffset += (CSize)ptVpOrg;
		m_pPpvPreviewDC->SetTopLeftOffset( sizePrintOffset );
		m_pPpvPreviewDC->ClipToPage();
//
//		m_pPpvPrintable->OnPrint( m_pPpvPreviewDC, m_pPpvPrintPreviewInfo );
//
		CExtMemoryDC dcX( m_pPpvPreviewDC, pRect );
		m_pPpvPrintable->OnPrint( &dcX, m_pPpvPrintPreviewInfo );
		dcX.__Flush();

		pDC->RestoreDC( -1 );

		pDC->SaveDC();
		pDC->SetMapMode( MM_TEXT );
		pDC->SetViewportOrg( ptVpOrg );
		pDC->SetWindowOrg( 0, 0 );
		if(		pRect->left < pRect->right
			&&	pRect->top < pRect->bottom
			)
		{
			pDC->SelectStockObject(HOLLOW_BRUSH);
			CPen * pOldPen = pDC->SelectObject( &penPageBorder) ;
			pDC->Rectangle(pRect);
			pDC->SelectObject( pOldPen );
			UINT nShadowSize = CExtWndShadow::DEF_SHADOW_SIZE*2;
			if(		nShadowSize < UINT( pRect->Width() / 2 )
				&&	nShadowSize < UINT( pRect->Height() / 2 )
				)
			{
				CExtWndShadow _shadow;
				_shadow.Paint(
					NULL,
					*pDC, *pRect, CRect(0,0,0,0), CRect(0,0,0,0),
					nShadowSize,
					CExtWndShadow::DEF_BRIGHTNESS_MIN, CExtWndShadow::DEF_BRIGHTNESS_MAX,
					false
					);
			}
//			CRect rectFill = *pRect;
//			rectFill.left += 1;
//			rectFill.top += 1;
//			rectFill.right -= 2;
//			rectFill.bottom -= 2;
//			::FillRect( pDC->m_hDC, rectFill, (HBRUSH)::GetStockObject( WHITE_BRUSH ) );
		}
		pDC->RestoreDC( -1 );


		m_pPpvPreviewDC->ReleaseOutputDC();
		m_dcPpvPrinter.RestoreDC( nSavedState );
	}
	penPageBorder.DeleteObject();
}

void CExtPPVW_HostWnd::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar * pScrollBar )
{
	ASSERT_VALID( this );
	if( m_nPpvViewMode != __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES )
		CExtNCSB < CScrollView > :: OnHScroll( nSBCode, nPos, pScrollBar );
}

int CExtPPVW_HostWnd::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message)
{
	ASSERT_VALID( this );
int nResult = CWnd::OnMouseActivate( pDesktopWnd, nHitTest, message );
	if( nResult == MA_NOACTIVATE || nResult == MA_NOACTIVATEANDEAT )
		return nResult;   // frame does not want to activate
//CFrameWnd * pParentFrame = GetParentFrame();
CFrameWnd * pParentFrame = NULL;
CWnd * pParentWnd = GetParent();  // start with one parent up
	for( ; pParentWnd != NULL; pParentWnd = pParentWnd->GetParent() )
	{
		if( pParentWnd->IsFrameWnd() )
		{
			pParentFrame = STATIC_DOWNCAST( CFrameWnd, pParentWnd );
			break;
		}
		if( ( pParentWnd->GetStyle() & WS_CHILD ) == 0 )
			break;
	}
	if( pParentFrame != NULL )
	{
//		// eat it if this will cause activation
//		ASSERT(pParentFrame == pDesktopWnd || pDesktopWnd->IsChild(pParentFrame));
		// either re-activate the current view, or set this view to be active
		CView * pView = pParentFrame->GetActiveView();
		HWND hWndFocus = ::GetFocus();
		if(		pView == this
			&&	m_hWnd != hWndFocus
			&&	( ! ::IsChild( m_hWnd, hWndFocus ) )
			)
		{
			// re-activate this view
			OnActivateView( TRUE, this, this );
		}
		else
		{
			// activate this view
			pParentFrame->SetActiveView( this );
		}
	}
	return nResult;
}


void CExtPPVW_HostWnd::OnVScroll( UINT nSBCode, UINT nPos, CScrollBar * pScrollBar )
{
	ASSERT_VALID( this );
	if(		m_nPpvViewMode != __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES
		||	m_pPpvPrintable == NULL
		||	m_pPpvPrintPreviewInfo == NULL
		)
	{
		CExtNCSB < CScrollView > :: OnVScroll( nSBCode, nPos, pScrollBar );
		return;
	}

	switch( nSBCode )
	{
	case SB_BOTTOM:
		_PpvSetCurrentPage( m_pPpvPrintPreviewInfo->GetMaxPage(), TRUE );
	break;
	case SB_TOP:
		_PpvSetCurrentPage( m_pPpvPrintPreviewInfo->GetMinPage(), TRUE );
	break;
	case SB_PAGEDOWN:
	case SB_LINEDOWN:
		_PpvSetCurrentPage( m_nPpvDisplayedPageIndex + 1, TRUE );
	break;
	case SB_PAGEUP:
	case SB_LINEUP:
		_PpvSetCurrentPage( m_nPpvDisplayedPageIndex - 1, TRUE );
	break;
	case SB_THUMBTRACK:
	case SB_THUMBPOSITION:
		_PpvSetCurrentPage( nPos, TRUE );
	break;
	} // switch( nSBCode )
}

void CExtPPVW_HostWnd::OnPpv_PageNextCmd()
{
	ASSERT_VALID( this );
	_PpvSetCurrentPage(
		m_nPpvDisplayedPageIndex + 1,
		TRUE
		);
}
void CExtPPVW_HostWnd::OnPpv_PageNextUpdate( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
UINT nTest = m_nPpvDisplayedPageIndex + m_nPpvDisplayedPageCountCurrent - 1;
UINT nMax = m_pPpvPrintPreviewInfo->GetMaxPage();
BOOL bEnable = ( nTest < nMax ) ? TRUE : FALSE;
	pCmdUI->Enable( bEnable );
}

void CExtPPVW_HostWnd::OnPpv_PagePrevCmd()
{
	ASSERT_VALID( this );
	_PpvSetCurrentPage(
		m_nPpvDisplayedPageIndex - 1,
		TRUE
		);
}
void CExtPPVW_HostWnd::OnPpv_PagePrevUpdate( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
UINT nMin = m_pPpvPrintPreviewInfo->GetMinPage();
BOOL bEnable = ( m_nPpvDisplayedPageIndex > nMin ) ? TRUE : FALSE;
	pCmdUI->Enable( bEnable );
}

void CExtPPVW_HostWnd::OnPpv_ViewMode_Magnify_Cmd()
{
	ASSERT_VALID( this );
	if(		m_bPpvUseZoomScrollBar
		&&	m_pPpvWndToolBar->GetSafeHwnd() != NULL
		&&	m_pPpvWndToolBar->m_wndZoomScrollBar.GetSafeHwnd() != NULL
		)
		m_nPpvMagnifyLevel = (UINT)m_pPpvWndToolBar->m_wndZoomScrollBar.GetScrollPos();
	PpvSetModes(
		m_nPpvPageMode,
		__EXT_MFC_PPV_VIEW_MODE_MAGNIFY,
		m_nPpvMagnifyLevel,
		0,
		CPoint( 0, 0 )
		);
}
void CExtPPVW_HostWnd::OnPpv_ViewMode_Magnify_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	pCmdUI->Enable();
BOOL bRadio = ( m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_MAGNIFY ) ? TRUE : FALSE;
	pCmdUI->SetRadio( bRadio );
}

void CExtPPVW_HostWnd::OnPpv_ViewMode_FitWidth_Cmd()
{
	ASSERT_VALID( this );
	PpvSetModes(
		m_nPpvPageMode,
		__EXT_MFC_PPV_VIEW_MODE_FIT_WIDTH,
		m_nPpvMagnifyLevel,
		0,
		CPoint( 0, 0 )
		);
}
void CExtPPVW_HostWnd::OnPpv_ViewMode_FitWidth_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	pCmdUI->Enable();
BOOL bRadio = ( m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_WIDTH ) ? TRUE : FALSE;
	pCmdUI->SetRadio( bRadio );
}

void CExtPPVW_HostWnd::OnPpv_ViewMode_FitHeight_Cmd()
{
	ASSERT_VALID( this );
	PpvSetModes(
		m_nPpvPageMode,
		__EXT_MFC_PPV_VIEW_MODE_FIT_HEIGHT,
		m_nPpvMagnifyLevel,
		0,
		CPoint( 0, 0 )
		);
}
void CExtPPVW_HostWnd::OnPpv_ViewMode_FitHeight_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	pCmdUI->Enable();
BOOL bRadio = ( m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_HEIGHT ) ? TRUE : FALSE;
	pCmdUI->SetRadio( bRadio );
}

void CExtPPVW_HostWnd::OnPpv_ViewMode_FitPages_Cmd()
{
	ASSERT_VALID( this );
	PpvSetModes(
		m_nPpvPageMode,
		__EXT_MFC_PPV_VIEW_MODE_FIT_PAGES,
		m_nPpvMagnifyLevel,
		0,
		CPoint( 0, 0 )
		);
}
void CExtPPVW_HostWnd::OnPpv_ViewMode_FitPages_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	pCmdUI->Enable();
BOOL bRadio = ( m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES ) ? TRUE : FALSE;
	pCmdUI->SetRadio( bRadio );
}

void CExtPPVW_HostWnd::OnPpv_Zoom_SB_Cmd()
{
	ASSERT_VALID( this );
	PostMessage( 0x0363 ); // WM_IDLEUPDATECMDUI
}
void CExtPPVW_HostWnd::OnPpv_Zoom_SB_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
BOOL bEnable = ( m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_MAGNIFY ) ? TRUE : FALSE;
	pCmdUI->Enable( bEnable );
}

void CExtPPVW_HostWnd::OnPpv_Magnify_In_Cmd()
{
	ASSERT_VALID( this );
	if(		m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_MAGNIFY
		&&	m_nPpvMagnifyLevel != __EXT_MFC_MAGNIFY_LEVEL_MAX
		)
		PpvSetModes(
			m_nPpvPageMode,
			m_nPpvViewMode,
			m_nPpvMagnifyLevel + 1,
			0,
			CPoint( 0, 0 )
			);
}
void CExtPPVW_HostWnd::OnPpv_Magnify_In_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
BOOL bEnable =
		(	m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_MAGNIFY
		&&	m_nPpvMagnifyLevel != __EXT_MFC_MAGNIFY_LEVEL_MAX
		) ? TRUE : FALSE;
	pCmdUI->Enable( bEnable );
}

void CExtPPVW_HostWnd::OnPpv_Magnify_Out_Cmd()
{
	ASSERT_VALID( this );
	if(		m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_MAGNIFY
		&&	m_nPpvMagnifyLevel != __EXT_MFC_MAGNIFY_LEVEL_MIN
		)
		PpvSetModes(
			m_nPpvPageMode,
			m_nPpvViewMode,
			m_nPpvMagnifyLevel - 1,
			0,
			CPoint( 0, 0 )
			);
}
void CExtPPVW_HostWnd::OnPpv_Magnify_Out_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
BOOL bEnable =
		(	m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_MAGNIFY
		&&	m_nPpvMagnifyLevel != __EXT_MFC_MAGNIFY_LEVEL_MIN
		) ? TRUE : FALSE;
	pCmdUI->Enable( bEnable );
}

void CExtPPVW_HostWnd::OnPpv_PageMode_1_Cmd()
{
	ASSERT_VALID( this );
	m_nPpvDisplayedPageCountCurrent = m_nPpvDisplayedPageCountDesired = 1;
	m_sizePpvDisplayedPagesDimesion.cx = 1;
	m_sizePpvDisplayedPagesDimesion.cy = 1;
	m_nPpvDisplayedPageCountCurrent = min( m_pPpvPrintPreviewInfo->m_nNumPreviewPages, m_nPpvDisplayedPageCountDesired );
	m_nPpvDisplayedPageCountCurrent = max( 1, m_nPpvDisplayedPageCountCurrent );
	PpvSetModes(
		__EXT_MFC_PPV_PAGE_MODE_1,
		m_nPpvViewMode,
		m_nPpvMagnifyLevel,
		0,
		CPoint( 0, 0 )
		);
}
void CExtPPVW_HostWnd::OnPpv_PageMode_1_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	if(		m_pPpvPrintPreviewInfo == NULL
		||	m_pPpvPrintPreviewInfo->m_pPD == NULL
		||	(m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMaxPage-m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMinPage+1) < 1
		)
	{
		pCmdUI->SetRadio( FALSE );
		pCmdUI->Enable( FALSE );
		return;
	}
	pCmdUI->Enable();
BOOL bRadio = ( m_nPpvPageMode == __EXT_MFC_PPV_PAGE_MODE_1 ) ? TRUE : FALSE;
	pCmdUI->SetRadio( bRadio );
}

void CExtPPVW_HostWnd::OnPpv_PageMode_2_Cmd()
{
	ASSERT_VALID( this );
	m_nPpvDisplayedPageCountCurrent = m_nPpvDisplayedPageCountDesired = 2;
	m_sizePpvDisplayedPagesDimesion.cx = 2;
	m_sizePpvDisplayedPagesDimesion.cy = 1;
	m_nPpvDisplayedPageCountCurrent = min( m_pPpvPrintPreviewInfo->m_nNumPreviewPages, m_nPpvDisplayedPageCountDesired );
	m_nPpvDisplayedPageCountCurrent = max( 1, m_nPpvDisplayedPageCountCurrent );
	PpvSetModes(
		__EXT_MFC_PPV_PAGE_MODE_2,
		m_nPpvViewMode,
		m_nPpvMagnifyLevel,
		0,
		CPoint( 0, 0 )
		);
}
void CExtPPVW_HostWnd::OnPpv_PageMode_2_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	if(		m_pPpvPrintPreviewInfo == NULL
		||	m_pPpvPrintPreviewInfo->m_pPD == NULL
		||	(m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMaxPage-m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMinPage+1) < 2
		)
	{
		pCmdUI->SetRadio( FALSE );
		pCmdUI->Enable( FALSE );
		return;
	}
	pCmdUI->Enable();
BOOL bRadio = ( m_nPpvPageMode == __EXT_MFC_PPV_PAGE_MODE_2 ) ? TRUE : FALSE;
	pCmdUI->SetRadio( bRadio );
}

void CExtPPVW_HostWnd::OnPpv_PageMode_3_Cmd()
{
	ASSERT_VALID( this );
	m_nPpvDisplayedPageCountCurrent = m_nPpvDisplayedPageCountDesired = 3;
	m_sizePpvDisplayedPagesDimesion.cx = 3;
	m_sizePpvDisplayedPagesDimesion.cy = 1;
	m_nPpvDisplayedPageCountCurrent = min( m_pPpvPrintPreviewInfo->m_nNumPreviewPages, m_nPpvDisplayedPageCountDesired );
	m_nPpvDisplayedPageCountCurrent = max( 1, m_nPpvDisplayedPageCountCurrent );
	PpvSetModes(
		__EXT_MFC_PPV_PAGE_MODE_3,
		m_nPpvViewMode,
		m_nPpvMagnifyLevel,
		0,
		CPoint( 0, 0 )
		);
}
void CExtPPVW_HostWnd::OnPpv_PageMode_3_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	if(		m_pPpvPrintPreviewInfo == NULL
		||	m_pPpvPrintPreviewInfo->m_pPD == NULL
		||	(m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMaxPage-m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMinPage+1) < 3
		)
	{
		pCmdUI->SetRadio( FALSE );
		pCmdUI->Enable( FALSE );
		return;
	}
	pCmdUI->Enable();
BOOL bRadio = ( m_nPpvPageMode == __EXT_MFC_PPV_PAGE_MODE_3 ) ? TRUE : FALSE;
	pCmdUI->SetRadio( bRadio );
}

void CExtPPVW_HostWnd::OnPpv_PageMode_4_Cmd()
{
	ASSERT_VALID( this );
	m_nPpvDisplayedPageCountCurrent = m_nPpvDisplayedPageCountDesired = 4;
	m_sizePpvDisplayedPagesDimesion.cx = 4;
	m_sizePpvDisplayedPagesDimesion.cy = 1;
	m_nPpvDisplayedPageCountCurrent = min( m_pPpvPrintPreviewInfo->m_nNumPreviewPages, m_nPpvDisplayedPageCountDesired );
	m_nPpvDisplayedPageCountCurrent = max( 1, m_nPpvDisplayedPageCountCurrent );
	PpvSetModes(
		__EXT_MFC_PPV_PAGE_MODE_4,
		m_nPpvViewMode,
		m_nPpvMagnifyLevel,
		0,
		CPoint( 0, 0 )
		);
}
void CExtPPVW_HostWnd::OnPpv_PageMode_4_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	if(		m_pPpvPrintPreviewInfo == NULL
		||	m_pPpvPrintPreviewInfo->m_pPD == NULL
		||	(m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMaxPage-m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMinPage+1) < 4
		)
	{
		pCmdUI->SetRadio( FALSE );
		pCmdUI->Enable( FALSE );
		return;
	}
	pCmdUI->Enable();
BOOL bRadio = ( m_nPpvPageMode == __EXT_MFC_PPV_PAGE_MODE_4 ) ? TRUE : FALSE;
	pCmdUI->SetRadio( bRadio );
}

void CExtPPVW_HostWnd::OnPpv_PageMode_5_Cmd()
{
	ASSERT_VALID( this );
	m_nPpvDisplayedPageCountCurrent = m_nPpvDisplayedPageCountDesired = 4;
	m_sizePpvDisplayedPagesDimesion.cx = 2;
	m_sizePpvDisplayedPagesDimesion.cy = 2;
	m_nPpvDisplayedPageCountCurrent = min( m_pPpvPrintPreviewInfo->m_nNumPreviewPages, m_nPpvDisplayedPageCountDesired );
	m_nPpvDisplayedPageCountCurrent = max( 1, m_nPpvDisplayedPageCountCurrent );
	PpvSetModes(
		__EXT_MFC_PPV_PAGE_MODE_5,
		m_nPpvViewMode,
		m_nPpvMagnifyLevel,
		0,
		CPoint( 0, 0 )
		);
}
void CExtPPVW_HostWnd::OnPpv_PageMode_5_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	if(		m_pPpvPrintPreviewInfo == NULL
		||	m_pPpvPrintPreviewInfo->m_pPD == NULL
		||	(m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMaxPage-m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMinPage+1) < 4
		)
	{
		pCmdUI->SetRadio( FALSE );
		pCmdUI->Enable( FALSE );
		return;
	}
	pCmdUI->Enable();
BOOL bRadio = ( m_nPpvPageMode == __EXT_MFC_PPV_PAGE_MODE_5 ) ? TRUE : FALSE;
	pCmdUI->SetRadio( bRadio );
}

void CExtPPVW_HostWnd::OnPpv_PageMode_6_Cmd()
{
	ASSERT_VALID( this );
	m_nPpvDisplayedPageCountCurrent = m_nPpvDisplayedPageCountDesired = 6;
	m_sizePpvDisplayedPagesDimesion.cx = 3;
	m_sizePpvDisplayedPagesDimesion.cy = 2;
	m_nPpvDisplayedPageCountCurrent = min( m_pPpvPrintPreviewInfo->m_nNumPreviewPages, m_nPpvDisplayedPageCountDesired );
	m_nPpvDisplayedPageCountCurrent = max( 1, m_nPpvDisplayedPageCountCurrent );
	PpvSetModes(
		__EXT_MFC_PPV_PAGE_MODE_6,
		m_nPpvViewMode,
		m_nPpvMagnifyLevel,
		0,
		CPoint( 0, 0 )
		);
}
void CExtPPVW_HostWnd::OnPpv_PageMode_6_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	if(		m_pPpvPrintPreviewInfo == NULL
		||	m_pPpvPrintPreviewInfo->m_pPD == NULL
		||	(m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMaxPage-m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMinPage+1) < 6
		)
	{
		pCmdUI->SetRadio( FALSE );
		pCmdUI->Enable( FALSE );
		return;
	}
	pCmdUI->Enable();
BOOL bRadio = ( m_nPpvPageMode == __EXT_MFC_PPV_PAGE_MODE_6 ) ? TRUE : FALSE;
	pCmdUI->SetRadio( bRadio );
}

void CExtPPVW_HostWnd::OnPpv_PageMode_7_Cmd()
{
	ASSERT_VALID( this );
	m_nPpvDisplayedPageCountCurrent = m_nPpvDisplayedPageCountDesired = 8;
	m_sizePpvDisplayedPagesDimesion.cx = 4;
	m_sizePpvDisplayedPagesDimesion.cy = 2;
	m_nPpvDisplayedPageCountCurrent = min( m_pPpvPrintPreviewInfo->m_nNumPreviewPages, m_nPpvDisplayedPageCountDesired );
	m_nPpvDisplayedPageCountCurrent = max( 1, m_nPpvDisplayedPageCountCurrent );
	PpvSetModes(
		__EXT_MFC_PPV_PAGE_MODE_7,
		m_nPpvViewMode,
		m_nPpvMagnifyLevel,
		0,
		CPoint( 0, 0 )
		);
}
void CExtPPVW_HostWnd::OnPpv_PageMode_7_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	if(		m_pPpvPrintPreviewInfo == NULL
		||	m_pPpvPrintPreviewInfo->m_pPD == NULL
		||	(m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMaxPage-m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMinPage+1) < 8
		)
	{
		pCmdUI->SetRadio( FALSE );
		pCmdUI->Enable( FALSE );
		return;
	}
	pCmdUI->Enable();
BOOL bRadio = ( m_nPpvPageMode == __EXT_MFC_PPV_PAGE_MODE_7 ) ? TRUE : FALSE;
	pCmdUI->SetRadio( bRadio );
}

void CExtPPVW_HostWnd::OnPreviewPrint()
{
	if( m_pPpvPrintable == NULL )
		return;
	GetParent()->ShowWindow( SW_HIDE ); // hide container window
HWND hWndOwn = GetSafeHwnd();
	ASSERT( hWndOwn != NULL );
	m_pPpvPrintable->DoPrintDoc( m_pPpvPrintable->m_bUsePrintDialogFromPreview ? TRUE : FALSE );
	if( ! ::IsWindow( hWndOwn ) )
		return;
	OnPreviewClose();
}

void CExtPPVW_HostWnd::OnPreviewSetup()
{
	if(		m_pPpvPrintPreviewInfo == NULL
		||	m_pPpvPrintPreviewInfo->m_pPD == NULL
		)
		return;
	if( m_pPpvPrintPreviewInfo->m_pPD->DoModal() != IDOK )
		return;
//struct friendly_app_t : public CWinApp { friend class CExtPPVW_HostWnd; };
//	((friendly_app_t*)::AfxGetApp())->UpdatePrinterSelection( FALSE );
	CExtPaintManager::stat_PassPaintMessages();
	VERIFY( _PpvReGenerate() );
}

bool CExtPPVW_HostWnd::_PpvReGenerate()
{
	ASSERT_VALID( this );
	if(		m_pPpvPrintPreviewInfo == NULL
		||	m_pPpvPrintPreviewInfo->m_pPD == NULL
		)
		return false;
	m_sizePpvPrinterPPI.cx = ::GetDeviceCaps( m_dcPpvPrinter.m_hDC, LOGPIXELSX );
	m_sizePpvPrinterPPI.cy = ::GetDeviceCaps( m_dcPpvPrinter.m_hDC, LOGPIXELSY );
	_PpvCleanUp();
	PpvSetPrintable( m_pPpvPrintable );
	_PpvSetCurrentPage( m_pPpvPrintPreviewInfo->m_nCurPage, TRUE );
	return true;
}

BOOL CExtPPVW_HostWnd::_PpvFindPageRect( CPoint & point, UINT & nPage )
{
	if( m_nPpvViewMode != __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES )
		point += (CSize)GetDeviceScrollPosition();
	for( nPage = 0; nPage < m_nPpvDisplayedPageCountCurrent; nPage++ )
	{
		if( m_arrPpvPI[nPage].rectScreen.PtInRect(point) )
		{
			point -= (CSize)m_arrPpvPI[nPage].rectScreen.TopLeft();
			point.x = ::MulDiv( point.x, m_arrPpvPI[nPage].sizeScaleRatio.cy, m_arrPpvPI[nPage].sizeScaleRatio.cx );
			point.y = ::MulDiv( point.y, m_arrPpvPI[nPage].sizeScaleRatio.cy, m_arrPpvPI[nPage].sizeScaleRatio.cx );
			return TRUE;
		}
	}
	return FALSE;
}

void CExtPPVW_HostWnd::OnLButtonDown( UINT nFlags, CPoint point )
{
	ASSERT_VALID( this );
	nFlags;
	if( m_nPpvViewMode != __EXT_MFC_PPV_VIEW_MODE_MAGNIFY )
		return;
UINT nPage;
	if( ! _PpvFindPageRect( point, nPage ) )
		return;
	PpvSetModes(
		m_nPpvPageMode,
		m_nPpvViewMode,
		( m_nPpvMagnifyLevel == __EXT_MFC_MAGNIFY_LEVEL_MAX )
			? __EXT_MFC_MAGNIFY_LEVEL_MIN
			: m_nPpvMagnifyLevel + 1,
		nPage,
		point
		);
}

void CExtPPVW_HostWnd::OnRButtonDown( UINT nFlags, CPoint point )
{
	ASSERT_VALID( this );
	nFlags;
	if( m_nPpvViewMode != __EXT_MFC_PPV_VIEW_MODE_MAGNIFY )
		return;
UINT nPage;
	if( ! _PpvFindPageRect( point, nPage ) )
		return;
	PpvSetModes(
		m_nPpvPageMode,
		m_nPpvViewMode,
		( m_nPpvMagnifyLevel == __EXT_MFC_MAGNIFY_LEVEL_MIN )
			? __EXT_MFC_MAGNIFY_LEVEL_MAX
			: m_nPpvMagnifyLevel - 1,
		nPage,
		point
		);
}

void CExtPPVW_HostWnd::PpvSetModes(
	UINT nPpvPageMode,
	UINT nPpvViewMode,
	UINT nPpvMagnifyLevel,
	UINT nPage,
	CPoint point
	)
{
	nPage;
	point;
	if(		m_nPpvPageMode != nPpvPageMode
		||	m_nPpvViewMode != nPpvViewMode
		||	m_nPpvMagnifyLevel != nPpvMagnifyLevel
		)
	{
		m_nPpvPageMode = nPpvPageMode;
		m_nPpvViewMode = nPpvViewMode;
		m_nPpvMagnifyLevel = nPpvMagnifyLevel;
		switch( m_nPpvPageMode )
		{
		case __EXT_MFC_PPV_PAGE_MODE_1:
			m_nPpvDisplayedPageCountCurrent = 1;
			m_sizePpvDisplayedPagesDimesion.cx = 1;
			m_sizePpvDisplayedPagesDimesion.cy = 1;
		break;
		case __EXT_MFC_PPV_PAGE_MODE_2:
			m_nPpvDisplayedPageCountCurrent = 2;
			m_sizePpvDisplayedPagesDimesion.cx = 2;
			m_sizePpvDisplayedPagesDimesion.cy = 1;
		break;
		case __EXT_MFC_PPV_PAGE_MODE_3:
			m_nPpvDisplayedPageCountCurrent = 3;
			m_sizePpvDisplayedPagesDimesion.cx = 3;
			m_sizePpvDisplayedPagesDimesion.cy = 1;
		break;
		case __EXT_MFC_PPV_PAGE_MODE_4:
			m_nPpvDisplayedPageCountCurrent = 4;
			m_sizePpvDisplayedPagesDimesion.cx = 4;
			m_sizePpvDisplayedPagesDimesion.cy = 1;
		break;
		case __EXT_MFC_PPV_PAGE_MODE_5:
			m_nPpvDisplayedPageCountCurrent = 4;
			m_sizePpvDisplayedPagesDimesion.cx = 2;
			m_sizePpvDisplayedPagesDimesion.cy = 2;
		break;
		case __EXT_MFC_PPV_PAGE_MODE_6:
			m_nPpvDisplayedPageCountCurrent = 6;
			m_sizePpvDisplayedPagesDimesion.cx = 3;
			m_sizePpvDisplayedPagesDimesion.cy = 2;
		break;
		case __EXT_MFC_PPV_PAGE_MODE_7:
			m_nPpvDisplayedPageCountCurrent = 8;
			m_sizePpvDisplayedPagesDimesion.cx = 4;
			m_sizePpvDisplayedPagesDimesion.cy = 2;
		break;
#ifdef _DEBUG
		default:
			ASSERT( FALSE );
		break;
#endif // _DEBUG
		} // switch( m_nPpvPageMode )
		if( m_pPpvPrintable != NULL )
		{
			if( m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES )
			{
				CScrollBar * pSb = GetScrollBarCtrl( SB_VERT );
				if( pSb->GetSafeHwnd() )
				{
					if( ! pSb->IsWindowEnabled() )
						pSb->EnableWindow( TRUE );
					if( ! pSb->IsWindowVisible() )
						pSb->ShowWindow( SW_SHOW );
				} // if( pSb->GetSafeHwnd() )
			} // if( m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES )
		} // if( m_pPpvPrintable != NULL )
		_PpvSetCurrentPage( m_nPpvDisplayedPageIndex, TRUE );
		if(		m_bPpvUseZoomScrollBar
			&&	m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_MAGNIFY
			&&	m_pPpvWndToolBar->GetSafeHwnd() != NULL
			&&	m_pPpvWndToolBar->m_wndZoomScrollBar.GetSafeHwnd() != NULL
			&&	UINT(m_pPpvWndToolBar->m_wndZoomScrollBar.GetScrollPos()) != m_nPpvMagnifyLevel
			)
			m_pPpvWndToolBar->m_wndZoomScrollBar.SetScrollPos( INT(m_nPpvMagnifyLevel) );
		_PpvUpdateBars();
		if( m_pPpvPrintable != NULL )
			m_pPpvPrintable->OnRecalcBars();
	}
}

void CExtPPVW_HostWnd::_PpvSetCurrentPage( UINT nPage, BOOL bClearRatios )
{
	ASSERT_VALID( this );
	m_nPpvDisplayedPageIndex = nPage;
	if (m_nPpvDisplayedPageIndex > m_pPpvPrintPreviewInfo->GetMaxPage())
		m_nPpvDisplayedPageIndex = m_pPpvPrintPreviewInfo->GetMaxPage();
	if (m_nPpvDisplayedPageIndex < m_pPpvPrintPreviewInfo->GetMinPage())
		m_nPpvDisplayedPageIndex = m_pPpvPrintPreviewInfo->GetMinPage();
// 	// 	// 	// 	// 	if( __EXT_MFC_PRINT_PREVIEW_ZOOM_SCROLLABLE( m_nPpvZoomMode ) )
// 	// 	// 	// 	// 		SetScrollPos( SB_VERT, m_nPpvDisplayedPageIndex );
	if( bClearRatios )
	{
		for( UINT i = 0; i < m_nPpvDisplayedPageCountMax; i++ )
			m_arrPpvPI[i].sizeScaleRatio.cx = 0;
	}

	if( m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES )
	{
		UINT nPos = (UINT)GetScrollPos( SB_VERT );
		if( nPos != nPage )
			SetScrollPos( SB_VERT, INT(nPage), FALSE );
	}

	if( m_pPpvPrintable != NULL )
	{
		if( m_pPpvWndToolBar->GetSafeHwnd() != NULL )
		{
			CExtCmdItem * pCmdItem =
				g_CmdManager->CmdGetPtr(
					m_pPpvPrintable->m_strPpvCommandProfileName,
					ID_EXT_PPV_EDIT_PAGE_NUMBER
					);
			if( pCmdItem != NULL )
			{
				INT nLast = INT( m_nPpvDisplayedPageIndex + m_nPpvDisplayedPageCountDesired - 1 );
				INT nMin = INT( m_pPpvPrintPreviewInfo->GetMinPage() );
				INT nMax = INT( m_pPpvPrintPreviewInfo->GetMaxPage() );
				if( nLast > nMax )
					nLast = nMax;
				pCmdItem->m_sToolbarText.Format(
					_T("%d-%d/%d-%d"),
					INT( m_nPpvDisplayedPageIndex ),
					nLast,
					nMin,
					nMax
					);
				m_pPpvPrintable->OnRecalcBars();
				m_pPpvWndToolBar->Invalidate();
			} // if( pCmdItem != NULL )
		} // if( m_pPpvWndToolBar->GetSafeHwnd() != NULL )
		m_pPpvPrintable->OnRecalcBars();
	}
	if( GetSafeHwnd() != NULL )
	{
		Invalidate();
		PostMessage( 0x0363 ); // WM_IDLEUPDATECMDUI
	}
}

void CExtPPVW_HostWnd::_PpvUpdateBars()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	EnableScrollBar( SB_HORZ, FALSE );
	EnableScrollBar( SB_VERT, FALSE );
CScrollBar * pSb = GetScrollBarCtrl( SB_HORZ );
	if( pSb->GetSafeHwnd() != NULL )
	{
		DWORD dwSbStyle = pSb->GetStyle();
		bool bVisible = ( (dwSbStyle&WS_VISIBLE) != 0 ) ? true : false;
		bool bEnabled =
			(	m_nPpvViewMode != __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES
			&&	pSb->IsWindowEnabled()
			) ? true : false;
		if( bVisible != bEnabled )
		{
			pSb->EnableWindow( bEnabled );
			pSb->ShowWindow( bEnabled ? SW_SHOW : SW_HIDE );
		}
		if(		m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_MAGNIFY
			&&	( ! bEnabled )
			)
			pSb->EnableWindow( TRUE );
	}
	pSb = GetScrollBarCtrl( SB_VERT );
	if( pSb->GetSafeHwnd() != NULL )
	{
		DWORD dwSbStyle = pSb->GetStyle();
		bool bVisible = ( (dwSbStyle&WS_VISIBLE) != 0 ) ? true : false;
		bool bEnabled = pSb->IsWindowEnabled() ? true : false;
		if( m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_FIT_PAGES )
		{
			if(		m_pPpvPrintPreviewInfo != NULL
				&&	m_pPpvPrintPreviewInfo->m_pPD != NULL
				&&	m_nPpvDisplayedPageCountDesired > UINT(m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMaxPage-m_pPpvPrintPreviewInfo->m_pPD->m_pd.nMinPage)
				)
				bEnabled = false;
			else
				bEnabled = true;
		}
		if( bVisible != bEnabled )
		{
			pSb->EnableWindow( bEnabled );
			pSb->ShowWindow( bEnabled ? SW_SHOW : SW_HIDE );
		}
		if(		m_nPpvViewMode == __EXT_MFC_PPV_VIEW_MODE_MAGNIFY
			&&	( ! bEnabled )
			)
			pSb->EnableWindow( TRUE );
	}
	PostMessage( 0x0363 ); // WM_IDLEUPDATECMDUI
	return;
}

void CExtPPVW_HostWnd::OnDisplayPageNumber( UINT nPage, UINT nPagesDisplayed )
{
	ASSERT_VALID( this );
	nPage;
	nPagesDisplayed;
// 	// 	// 	// 	// 	UINT nEndPage = nPage + nPagesDisplayed - 1;
// 	// 	// 	// 	// 	CWinThread * pThread = AfxGetThread();
// 	// 	// 	// 	// 		ASSERT( pThread );
// 	// 	// 	// 	// 	CFrameWnd * pParent = (CFrameWnd*)pThread->m_pMainWnd;
// 	// 	// 	// 	// 		ASSERT_VALID(pParent);
// 	// 	// 	// 	// 		ASSERT_KINDOF(CFrameWnd, pParent);
// 	// 	// 	// 	// 	int nSubString = (nPagesDisplayed == 1) ? 0 : 1;
// 	// 	// 	// 	// 	CString s;
// 	// 	// 	// 	// 	BOOL bOK = AfxExtractSubString( s, m_pPpvPrintPreviewInfo->m_strPageDesc, nSubString );
// 	// 	// 	// 	// 		if( bOK )
// 	// 	// 	// 	// 		{
// 	// 	// 	// 	// 			TCHAR szBuf[80];
// 	// 	// 	// 	// 			int nResult;
// 	// 	// 	// 	// 			if (nSubString == 0)
// 	// 	// 	// 	// 			{
// 	// 	// 	// 	// 				nResult =
// 	// 	// 	// 	// 					__EXT_MFC_SPRINTF(
// 	// 	// 	// 	// 						szBuf,
// 	// 	// 	// 	// 						//sizeof(szBuf)/sizeof(szBuf[0]),
// 	// 	// 	// 	// 						s,
// 	// 	// 	// 	// 						nPage
// 	// 	// 	// 	// 						);
// 	// 	// 	// 	// 			}
// 	// 	// 	// 	// 			else
// 	// 	// 	// 	// 			{
// 	// 	// 	// 	// 				nResult =
// 	// 	// 	// 	// 					__EXT_MFC_SPRINTF(
// 	// 	// 	// 	// 						szBuf,
// 	// 	// 	// 	// 						//sizeof(szBuf)/sizeof(szBuf[0]),
// 	// 	// 	// 	// 						s,
// 	// 	// 	// 	// 						nPage,
// 	// 	// 	// 	// 						nEndPage
// 	// 	// 	// 	// 						);
// 	// 	// 	// 	// 			}
// 	// 	// 	// 	// 			if( nResult > 0 )
// 	// 	// 	// 	// 			{
// 	// 	// 	// 	// 				pParent->SendMessage(WM_SETMESSAGESTRING, 0, (LPARAM)(LPVOID)szBuf);
// 	// 	// 	// 	// 			}
// 	// 	// 	// 	// 			else
// 	// 	// 	// 	// 			{
// 	// 	// 	// 	// 				bOK = FALSE;
// 	// 	// 	// 	// 			}
// 	// 	// 	// 	// 		}
// 	// 	// 	// 	// 		if( ! bOK )
// 	// 	// 	// 	// 		{
// 	// 	// 	// 	// 			TRACE1(
// 	// 	// 	// 	// 				"Malformed Page Description string. Could not get string %d.\n",
// 	// 	// 	// 	// 				nSubString
// 	// 	// 	// 	// 				);
// 	// 	// 	// 	// 		}
}

BOOL CExtPPVW_HostWnd::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if(		nHitTest != HTCLIENT
		||	m_nPpvViewMode != __EXT_MFC_PPV_VIEW_MODE_MAGNIFY
		)
		return CExtNCSB < CScrollView > :: OnSetCursor(pWnd, nHitTest, message);
CPoint point;
	::GetCursorPos( &point );
	ScreenToClient( &point );
UINT nPage;
	if( m_nPpvMagnifyLevel == __EXT_MFC_MAGNIFY_LEVEL_MAX )
		::SetCursor( m_hPpvMagnifyCursorOut );
	else if( _PpvFindPageRect( point, nPage ) )
		::SetCursor( m_hPpvMagnifyCursorIn );
	else
		::SetCursor( ::LoadCursor( NULL, IDC_ARROW ) );
	return 0;
}

#ifdef _DEBUG
void CExtPPVW_HostWnd::AssertValid() const
{
	CView::AssertValid();
	ASSERT_VALID( &m_dcPpvPrinter );
	if( m_pPpvPreviewDC != NULL )
	{
		ASSERT_VALID( m_pPpvPreviewDC );
	}
	ASSERT(
			__EXT_MFC_PPV_PAGE_MODE_MIN <= m_nPpvPageMode
		&&	m_nPpvPageMode <= __EXT_MFC_PPV_PAGE_MODE_MAX
		);
	ASSERT(
			__EXT_MFC_PPV_VIEW_MODE_MIN <= m_nPpvViewMode
		&&	m_nPpvViewMode <= __EXT_MFC_PPV_VIEW_MODE_MAX
		);
	ASSERT(
			__EXT_MFC_MAGNIFY_LEVEL_MIN <= m_nPpvMagnifyLevel
		&&	m_nPpvMagnifyLevel <= __EXT_MFC_MAGNIFY_LEVEL_MAX
		);
	switch( m_nMapMode )
	{
	case MM_TEXT:
	case MM_LOMETRIC:
	case MM_HIMETRIC:
	case MM_LOENGLISH:
	case MM_HIENGLISH:
	case MM_TWIPS:
	case MM_ISOTROPIC:
	case MM_ANISOTROPIC:
	break;
	default:
		ASSERT( FALSE );
	break;
	} // switch( m_nMapMode )
}

void CExtPPVW_HostWnd::Dump( CDumpContext & dc ) const
{
	CView::Dump(dc);
}

#endif //_DEBUG


#if (!defined __EXT_MFC_NO_GRIDBASEWND)

/////////////////////////////////////////////////////////////////////////////
// template < > class CExtPPVW < CExtGridWnd >

void CExtPPVW < CExtGridWnd > :: OnPrepareDC(
	CDC * pDC,
	CPrintInfo * pInfo // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC );
	ASSERT( pDC->GetSafeHdc() != NULL );
	pInfo;

CFont * pFont = & OnSiwGetDefaultFont();
 	if( pFont->GetSafeHandle() == NULL )
 		return;
	pDC->SelectObject( pFont );
}

bool CExtPPVW < CExtGridWnd > :: IsPrintPreviewAvailable() const
{
	ASSERT_VALID( this );
	if( ! CExtPPVW_Printable::IsPrintPreviewAvailable() )
		return false;
	if( GetSafeHwnd() == NULL )
		return false;
//bool bExternalData = ExternalDataGet();
//	if( bExternalData )
//		return false;
DWORD dwScrollTypeH = SiwScrollTypeHGet();
	if( dwScrollTypeH == __ESIW_ST_VIRTUAL )
		return false;
DWORD dwScrollTypeV = SiwScrollTypeVGet();
	if( dwScrollTypeV == __ESIW_ST_VIRTUAL )
		return false;
LONG nColCount = ColumnCountGet();
	if( nColCount <= 0 )
		return false;
LONG nRowCount = RowCountGet();
	if( nRowCount <= 0 )
		return false;
	return true;
}

void CExtPPVW < CExtGridWnd > :: OnPreparePrinting_RenderSingleCell(
	CPrintInfo * pInfo,
	const CRect & rcOuterPartCounts,
	const CArray < INT, INT & > & arrColumnWidths,
	const CArray < INT, INT & > & arrRowHeights,
	LONG nColCount,
	LONG nRowCount,
	INT nPageIndex,
	LONG nColNoFrom,
	LONG nColNoTo,
	LONG nRowNoFrom,
	LONG nRowNoTo,
	CExtGridCell * pCell,
	CDC & dc,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	CRect & rcCellExtra,
	CRect & rcCell,
	CRect & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT_VALID( pCell );
	pInfo;
	nPageIndex;
	nColNoTo;
	nRowNoTo;
bool bDrawCell = true;
CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, nColType, nRowType );
	if( _sizeJoin.cx != 1 || _sizeJoin.cy != 1 )
	{
		LONG nColNoA = nColNo, nRowNoA = nRowNo;
		bool bRootMode = ( _sizeJoin.cx >= 1 && _sizeJoin.cy >= 1 ) ? true : false;
		if(		bRootMode
			||	OnGbwCellJoinAdjustCoordinates( nColNoA, nRowNoA, nColType, nRowType )
			)
		{
			bDrawCell = false;
			pCell = NULL;
			if(		bRootMode
				||	( nColType == 0 && nColNo == nColNoFrom && _sizeJoin.cy >= 1 )
				||	( nRowType == 0 && nRowNo == nRowNoFrom && _sizeJoin.cx >= 1 )
				)
				pCell = GridCellGet( nColNoA, nRowNoA, nColType, nRowType );
			if( pCell != NULL )
			{
				bDrawCell = true;
				CPoint _VpShift( 0, 0 );
				CSize _VpSize( 0, 0 );
				INT nStepIndex, nStepCount;
				if( _sizeJoin.cx < 0 )
				{
					nStepCount = - _sizeJoin.cx;
					for( nStepIndex = 0; nStepIndex < nStepCount; nStepIndex ++ )
					{
						INT nEffectiveIndex = nColNo - nStepIndex - 1;
						if( nColType == 0 )
							nEffectiveIndex += rcOuterPartCounts.left;
						else if( nColType > 0 )
							nEffectiveIndex += rcOuterPartCounts.left + nColCount;
						INT nExt = arrColumnWidths[ nEffectiveIndex ];
						_VpShift.x += nExt;
					}
				}
				if( _sizeJoin.cy < 0 )
				{
					nStepCount = - _sizeJoin.cy;
					for( nStepIndex = 0; nStepIndex < nStepCount; nStepIndex ++ )
					{
						INT nEffectiveIndex = nRowNo - nStepIndex - 1;
						if( nRowType == 0 )
							nEffectiveIndex += rcOuterPartCounts.top;
						else if( nRowType > 0 )
							nEffectiveIndex += rcOuterPartCounts.top + nRowCount;
						INT nExt = arrRowHeights[ nEffectiveIndex ];
						_VpShift.y += nExt;
					}
				}
				CSize _sizeJoinA = OnGbwCellJoinQueryInfo( nColNoA, nRowNoA, nColType, nRowType );
				ASSERT( _sizeJoinA.cx >= 1 && _sizeJoinA.cy >= 1 );
				nStepCount = _sizeJoinA.cx;
				for( nStepIndex = 0; nStepIndex < nStepCount; nStepIndex ++ )
				{
					INT nEffectiveIndex = nColNoA + nStepIndex;
					if( nColType == 0 )
						nEffectiveIndex += rcOuterPartCounts.left;
					else if( nColType > 0 )
						nEffectiveIndex += rcOuterPartCounts.left + nColCount;
					INT nExt = arrColumnWidths[ nEffectiveIndex ];
					_VpSize.cx += nExt;
				}
				nStepCount = _sizeJoinA.cy;
				for( nStepIndex = 0; nStepIndex < nStepCount; nStepIndex ++ )
				{
					INT nEffectiveIndex = nRowNoA + nStepIndex;
					if( nRowType == 0 )
						nEffectiveIndex += rcOuterPartCounts.top;
					else if( nRowType > 0 )
						nEffectiveIndex += rcOuterPartCounts.top + nRowCount;
					INT nExt = arrRowHeights[ nEffectiveIndex ];
					_VpSize.cy += nExt;
				}
				rcCell.left  -= _VpShift.x;
				rcCell.top   -= _VpShift.y;
				rcCell.right  = rcCell.left + _VpSize.cx;
				rcCell.bottom = rcCell.top  + _VpSize.cy;
				rcCellExtra = rcCell;
			} // if( pCell != NULL )
		}
	} // if( _sizeJoin.cx != 1 || _sizeJoin.cy != 1 )
	if( bDrawCell )
	{
		ASSERT_VALID( pCell );
		if( m_bDrawSimpleWhiteBackground )
		{
			CRect rcLines = rcCell;
			rcLines.right ++;
			rcLines.bottom ++;
			HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(WHITE_BRUSH) );
			HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_PEN) );
			dc.Rectangle( &rcLines );
			::SelectObject( dc.m_hDC, hOldPen );
			::SelectObject( dc.m_hDC, hOldBrush );
		} // if( m_bDrawSimpleWhiteBackground )
		pCell->OnPaintBackground(
			*this,
			dc,
			nColNo,
			nRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
		pCell->OnPaintForeground(
			*this,
			dc,
			nColNo,
			nRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
		if( m_bDrawSimpleBlackBorders )
		{
			CRect rcLines = rcCell;
			rcLines.right ++;
			rcLines.bottom ++;
			HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_BRUSH) );
			HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(BLACK_PEN) );
			dc.Rectangle( &rcLines );
			::SelectObject( dc.m_hDC, hOldPen );
			::SelectObject( dc.m_hDC, hOldBrush );
		} // if( m_bDrawSimpleBlackBorders )
	} // if( bDrawCell )
}

BOOL CExtPPVW < CExtGridWnd > :: OnPreparePrinting(
	CPrintInfo * pInfo
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return FALSE;

bool bExternalData = ExternalDataGet();
//	if( bExternalData )
//		return FALSE;
DWORD dwScrollTypeH = SiwScrollTypeHGet();
	if( dwScrollTypeH == __ESIW_ST_VIRTUAL )
		return FALSE;
DWORD dwScrollTypeV = SiwScrollTypeVGet();
	if( dwScrollTypeV == __ESIW_ST_VIRTUAL )
		return FALSE;

LONG nColCount = ColumnCountGet();
	if( nColCount <= 0 )
		return FALSE;
LONG nRowCount = RowCountGet();
	if( nRowCount <= 0 )
		return FALSE;

bool bEnsureScrollPosH = false;
bool bEnsureScrollPosV = false;
bool bRedrawWhileEnsureScrollPosH = false;
bool bRedrawWhileEnsureScrollPosV = false;
	if( bExternalData )
		bEnsureScrollPosV = true;
CPoint _ptScrollPosBefore = OnSwGetScrollPos();

	if( ! CExtPPVW_Printable::OnPreparePrinting( pInfo ) )
		return FALSE;
CWaitCursor _WaitCursor;
	ASSERT( pInfo->m_pPD->m_pd.hDC != NULL );
CDC * pDC = CDC::FromHandle( pInfo->m_pPD->m_pd.hDC );
	ASSERT_VALID( pDC );
INT nPrinterDpiX = ::GetDeviceCaps( pDC->m_hDC, LOGPIXELSX );
INT nPrinterDpiY = ::GetDeviceCaps( pDC->m_hDC, LOGPIXELSY );
INT nPrinterDpiDX = ::GetDeviceCaps( pDC->m_hDC, HORZRES );
INT nPrinterDpiDY = ::GetDeviceCaps( pDC->m_hDC, VERTRES );
INT nMeasureDpiDX = ::MulDiv( nPrinterDpiDX, g_PaintManager.m_nLPX, nPrinterDpiX );
INT nMeasureDpiDY = ::MulDiv( nPrinterDpiDY, g_PaintManager.m_nLPY, nPrinterDpiY );
// CSize _sizePrinterOffset(
// 		pDC->GetDeviceCaps(PHYSICALOFFSETX),
// 		pDC->GetDeviceCaps(PHYSICALOFFSETY)
// 		);
// 	nMeasureDpiDX -= ::MulDiv( _sizePrinterOffset.cx * 2, g_PaintManager.m_nLPX, nPrinterDpiX );
// 	nMeasureDpiDY -= ::MulDiv( _sizePrinterOffset.cy * 2, g_PaintManager.m_nLPY, nPrinterDpiY );

CSize sizePMLT( m_rcPageMarginsHM.left, m_rcPageMarginsHM.top ), sizePMRB( m_rcPageMarginsHM.right, m_rcPageMarginsHM.bottom ); 
	pDC->HIMETRICtoDP( &sizePMLT );
	pDC->HIMETRICtoDP( &sizePMRB );
	sizePMLT.cx = ::MulDiv( sizePMLT.cx, g_PaintManager.m_nLPX, nPrinterDpiX );
	sizePMRB.cx = ::MulDiv( sizePMRB.cx, g_PaintManager.m_nLPX, nPrinterDpiX );
	sizePMLT.cy = ::MulDiv( sizePMLT.cy, g_PaintManager.m_nLPY, nPrinterDpiY );
	sizePMRB.cy = ::MulDiv( sizePMRB.cy, g_PaintManager.m_nLPY, nPrinterDpiY );
	nMeasureDpiDX -= sizePMLT.cx + sizePMRB.cx;
	nMeasureDpiDY -= sizePMLT.cy + sizePMRB.cy;

// 	pDC->DPtoLP( &_sizePrinterOffset );
CSize _sizePrinterHIMETRIC( nPrinterDpiDX, nPrinterDpiDY );
// 	_sizePrinterHIMETRIC -= _sizePrinterOffset;
// 	_sizePrinterHIMETRIC -= _sizePrinterOffset;
	pDC->DPtoHIMETRIC( &_sizePrinterHIMETRIC );
	//_sizePrinterHIMETRIC.cx -= m_rcPageMarginsHM.left + m_rcPageMarginsHM.right;
	//_sizePrinterHIMETRIC.cy -= m_rcPageMarginsHM.top + m_rcPageMarginsHM.bottom;
CRect rcEmfExtent( 0, 0, _sizePrinterHIMETRIC.cx, _sizePrinterHIMETRIC.cy );
DWORD dwHelperPaintFlagsBasic = m_dwBasicPaintFlags;
	if( m_pWndPP != NULL )
		dwHelperPaintFlagsBasic |= __EGCPF_PRINT_PREVIEW;
	else
		dwHelperPaintFlagsBasic |= __EGCPF_PRINTER;
	pDC->SaveDC();
	OnPrepareDC( pDC );
bool bOK = false, bMetafileCacheStarted = false;
	try
	{
		OnPreparePrinting_Begin();
		CRect rcOuterPartCounts(
			OuterColumnCountLeftGet(),
			OuterRowCountTopGet(),
			OuterColumnCountRightGet(),
			OuterRowCountBottomGet()
			);
		CRect rcOuterPartExtents( 0, 0, 0, 0 ),
			rcOuterMinExtents(
				g_PaintManager->UiScalingDo( __EXT_PRINT_OUTER_MIN_EXTENTS_LEFT,	CExtPaintManager::__EUIST_X ),
				g_PaintManager->UiScalingDo( __EXT_PRINT_OUTER_MIN_EXTENTS_TOP,		CExtPaintManager::__EUIST_Y ),
				g_PaintManager->UiScalingDo( __EXT_PRINT_OUTER_MIN_EXTENTS_RIGHT,	CExtPaintManager::__EUIST_X ),
				g_PaintManager->UiScalingDo( __EXT_PRINT_OUTER_MIN_EXTENTS_BOTTOM,	CExtPaintManager::__EUIST_Y )
				);
		ASSERT( rcOuterPartCounts.left >= 0 );
		ASSERT( rcOuterPartCounts.top >= 0 );
		ASSERT( rcOuterPartCounts.right >= 0 );
		ASSERT( rcOuterPartCounts.bottom >= 0 );
		CArray < INT, INT & > arrColumnWidths, arrRowHeights;
		arrColumnWidths.SetSize( nColCount + rcOuterPartCounts.left + rcOuterPartCounts.right );
		arrRowHeights.SetSize( nRowCount + rcOuterPartCounts.top + rcOuterPartCounts.bottom );
		CList < LONG, LONG > listColumnIndices, listRowIndices;
		LONG nColNo = 0L, nRowNo = 0L,
			nLastColNo = 0L, nColNoFrom = 0L, nColNoTo = 0L,
			nLastRowNo = 0L, nRowNoFrom = 0L, nRowNoTo = 0L;
		INT nColWidth = 0, nRowHeight = 0, nPageIndex = 0, nPageCount = 0,
			nHorzPageCount = 1, nVertPageCount = 1, nCurrentPageExtent = 0,
			nColOffset = 0, nRowOffset = 0;
		LONG nDocItemCount = nRowCount;
		for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		{
			if( ! _Internal_Notify_OnPreparePrinting_Progress(
					0,             // nPassNo
					2,             // nPassCount
					-1,            // nPageNo
					-1,            // nPageCount
					-1,            // nContentWidthInPages
					nRowNo,        // nDocumentItemNo
					nDocItemCount, // nDocumentItemCount
					-1,            // nInPageDocumentItemFirstNo,
					-1             // nInPageDocumentItemLastNo
					)
				)
				throw 0;
			nRowHeight = 0L;
			if( OnGridQueryPpvwVisibilityForRow( nRowNo, 0 ) )
			{
				if( bEnsureScrollPosV )
					EnsureVisibleRow( nRowNo, bRedrawWhileEnsureScrollPosV );
				for( nColNo = 0; nColNo < nColCount; nColNo ++ )
				{
					if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
						continue;
					if( bEnsureScrollPosH )
						EnsureVisibleColumn( nColNo, bRedrawWhileEnsureScrollPosH );
					if( nRowNo == 0L )
						arrColumnWidths[ nColNo + rcOuterPartCounts.left ] = nColWidth = 0;
					else
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
					INT nReviewColType = 0, nReviewRowType = 0;
					CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
					if( pCell == NULL )
						continue;
					ASSERT_VALID( pCell );
					CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, nReviewColType, nReviewRowType );
					if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
						continue;
					CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
					ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
					if( _sizeJoin.cx != 1 )
						_sizeCell.cx = 0;
					if( _sizeJoin.cy != 1 )
						_sizeCell.cy = 0;
					nColWidth = max( nColWidth, _sizeCell.cx );
					arrColumnWidths[ nColNo + rcOuterPartCounts.left ] = nColWidth;
					nRowHeight = max( nRowHeight, _sizeCell.cy );
				} // for( nColNo = 0; nColNo < nColCount; nColNo ++ )
				if( rcOuterPartCounts.left > 0 )
				{
					for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
							continue;
						INT nReviewColType = -1, nReviewRowType = 0;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
							continue;
						if( nRowNo == 0L )
							arrColumnWidths[ nColNo ] = nColWidth = 0;
						else
							nColWidth = arrColumnWidths[ nColNo ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						if( _sizeJoin.cx != 1 )
							_sizeCell.cx = 0;
						if( _sizeJoin.cy != 1 )
							_sizeCell.cy = 0;
						_sizeCell.cx = max( _sizeCell.cx, rcOuterMinExtents.left );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
				} // if( rcOuterPartCounts.left > 0 )
				if( rcOuterPartCounts.right > 0 )
				{
					for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
							continue;
						INT nReviewColType = 1, nReviewRowType = 0;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
							continue;
						if( nRowNo == 0L )
							arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ] = nColWidth = 0;
						else
							nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						if( _sizeJoin.cx != 1 )
							_sizeCell.cx = 0;
						if( _sizeJoin.cy != 1 )
							_sizeCell.cy = 0;
						_sizeCell.cx = max( _sizeCell.cx, rcOuterMinExtents.right );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
				} // if( rcOuterPartCounts.right > 0 )
			} // if( OnGridQueryPpvwVisibilityForRow( nRowNo, 0 ) )
			arrRowHeights[ nRowNo + rcOuterPartCounts.top ] = nRowHeight;
		} // for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		if( rcOuterPartCounts.top > 0 )
		{
			for( nRowNo = 0; nRowNo < rcOuterPartCounts.top; nRowNo ++ )
			{
				nRowHeight = 0;
				if( OnGridQueryPpvwVisibilityForRow( nRowNo, -1 ) )
				{
					for( nColNo = 0; nColNo < nColCount; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
							continue;
						if( bEnsureScrollPosH )
							EnsureVisibleColumn( nColNo, bRedrawWhileEnsureScrollPosH );
						INT nReviewColType = 0, nReviewRowType = -1;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
							continue;
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						if( _sizeJoin.cx != 1 )
							_sizeCell.cx = 0;
						if( _sizeJoin.cy != 1 )
							_sizeCell.cy = 0;
						_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo + rcOuterPartCounts.left ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // for( nColNo = 0; nColNo < nColCount; nColNo ++ )
					if( rcOuterPartCounts.left > 0 )
					{ // top-left header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
								continue;
							INT nReviewColType = -1, nReviewRowType = -1;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
								continue;
							nColWidth = arrColumnWidths[ nColNo ];
							CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
							ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
							if( _sizeJoin.cx != 1 )
								_sizeCell.cx = 0;
							if( _sizeJoin.cy != 1 )
								_sizeCell.cy = 0;
							_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
							nColWidth = max( nColWidth, _sizeCell.cx );
							arrColumnWidths[ nColNo ] = nColWidth;
							nRowHeight = max( nRowHeight, _sizeCell.cy );
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
					} // top-left header cells
					if( rcOuterPartCounts.right > 0 )
					{ // top-right header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
								continue;
							INT nReviewColType = 1, nReviewRowType = -1;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
								continue;
							nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
							CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
							ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
							if( _sizeJoin.cx != 1 )
								_sizeCell.cx = 0;
							if( _sizeJoin.cy != 1 )
								_sizeCell.cy = 0;
							_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
							nColWidth = max( nColWidth, _sizeCell.cx );
							arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ] = nColWidth;
							nRowHeight = max( nRowHeight, _sizeCell.cy );
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
					} // top-right header cells
				} // if( OnGridQueryPpvwVisibilityForRow( nRowNo, -1 ) )
				arrRowHeights[ nRowNo ] = nRowHeight;
				rcOuterPartExtents.top += nRowHeight;
			} // for( nRowNo = 0; nRowNo < rcOuterPartCounts.top; nRowNo ++ )
		} // if( rcOuterPartCounts.top > 0 )
		if( rcOuterPartCounts.bottom > 0 )
		{
			for( nRowNo = 0; nRowNo < rcOuterPartCounts.bottom; nRowNo ++ )
			{
				nRowHeight = 0;
				if( OnGridQueryPpvwVisibilityForRow( nRowNo, 1 ) )
				{
					for( nColNo = 0; nColNo < nColCount; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
							continue;
						if( bEnsureScrollPosH )
							EnsureVisibleColumn( nColNo, bRedrawWhileEnsureScrollPosH );
						INT nReviewColType = 0, nReviewRowType = 1;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
							continue;
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						if( _sizeJoin.cx != 1 )
							_sizeCell.cx = 0;
						if( _sizeJoin.cy != 1 )
							_sizeCell.cy = 0;
						_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.bottom );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo + rcOuterPartCounts.left ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // for( nColNo = 0; nColNo < nColCount; nColNo ++ )
					if( rcOuterPartCounts.left > 0 )
					{ // bottom-left header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
								continue;
							INT nReviewColType = -1, nReviewRowType = 1;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
								continue;
							nColWidth = arrColumnWidths[ nColNo ];
							CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
							ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
							if( _sizeJoin.cx != 1 )
								_sizeCell.cx = 0;
							if( _sizeJoin.cy != 1 )
								_sizeCell.cy = 0;
							_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
							nColWidth = max( nColWidth, _sizeCell.cx );
							arrColumnWidths[ nColNo ] = nColWidth;
							nRowHeight = max( nRowHeight, _sizeCell.cy );
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
					} // bottom-left header cells
					if( rcOuterPartCounts.right > 0 )
					{ // bottom-right header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
								continue;
							INT nReviewColType = 1, nReviewRowType = 1;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
								continue;
							nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
							CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
							ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
							if( _sizeJoin.cx != 1 )
								_sizeCell.cx = 0;
							if( _sizeJoin.cy != 1 )
								_sizeCell.cy = 0;
							_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
							nColWidth = max( nColWidth, _sizeCell.cx );
							arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ] = nColWidth;
							nRowHeight = max( nRowHeight, _sizeCell.cy );
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
					} // bottom-right header cells
				} // if( OnGridQueryPpvwVisibilityForRow( nRowNo, 1 ) )
				arrRowHeights[ nRowNo + rcOuterPartCounts.top + nRowCount ] = nRowHeight;
				rcOuterPartExtents.bottom += nRowHeight;
			} // for( nRowNo = 0; nRowNo < rcOuterPartCounts.bottom; nRowNo ++ )
		} // if( rcOuterPartCounts.bottom > 0 )
		if( rcOuterPartCounts.left > 0 )
		{
			for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
				rcOuterPartExtents.left += arrColumnWidths[ nColNo ];
		} // if( rcOuterPartCounts.left > 0 )
		if( rcOuterPartCounts.right > 0 )
		{
			for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
				rcOuterPartExtents.right += arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
		} // if( rcOuterPartCounts.right > 0 )
		m_rcPageHeaderFooterGutters = OnMeasurePageHeaderFooterGutters( *pDC );
		ASSERT( m_rcPageHeaderFooterGutters.left   >= 0 );
		ASSERT( m_rcPageHeaderFooterGutters.right  >= 0 );
		ASSERT( m_rcPageHeaderFooterGutters.top    >= 0 );
		ASSERT( m_rcPageHeaderFooterGutters.bottom >= 0 );
		INT nInnerMeasureDpiDX =
			nMeasureDpiDX
			- rcOuterPartExtents.left
			- rcOuterPartExtents.right
			- m_rcPageHeaderFooterGutters.left - m_rcPageHeaderFooterGutters.right;
		if( nInnerMeasureDpiDX <= 0 )
			throw 0;
		INT nInnerMeasureDpiDY =
			nMeasureDpiDY
			- rcOuterPartExtents.top
			- rcOuterPartExtents.bottom
			- m_rcPageHeaderFooterGutters.top - m_rcPageHeaderFooterGutters.bottom;
		if( nInnerMeasureDpiDY <= 0 )
			throw 0;
		listRowIndices.AddTail( 0L );
		nCurrentPageExtent = 0;
		for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		{
			nRowHeight = arrRowHeights[ nRowNo + rcOuterPartCounts.top ];
			nCurrentPageExtent += nRowHeight;
			if( nCurrentPageExtent > nInnerMeasureDpiDY )
			{
				nVertPageCount ++;
				if( nLastRowNo == nRowNo )
				{
					nCurrentPageExtent = 0;
					if( ( nRowNo + 1 ) < nRowCount )
					{
						listRowIndices.AddTail( nRowNo );
						listRowIndices.AddTail( nRowNo + 1 );
						nLastRowNo = nRowNo;
						continue;
					}
				}
				nCurrentPageExtent = nRowHeight;
				listRowIndices.AddTail( nRowNo - 1 );
				listRowIndices.AddTail( nRowNo );
				nLastRowNo = nRowNo;
			}
		} // for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		listRowIndices.AddTail( nRowCount - 1 );
		listColumnIndices.AddTail( 0L );
		for( nColNo = 0, nCurrentPageExtent = 0; nColNo < nColCount; nColNo ++ )
		{
			nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
			nCurrentPageExtent += nColWidth;
			if( nCurrentPageExtent > nInnerMeasureDpiDX )
			{
				nHorzPageCount ++;
				if( nLastColNo == nColNo )
				{
					nCurrentPageExtent = 0;
					if( ( nColNo + 1 ) < nColCount )
					{
						listColumnIndices.AddTail( nColNo );
						listColumnIndices.AddTail( nColNo + 1 );
						nLastColNo = nColNo;
						continue;
					}
				}
				nCurrentPageExtent = nColWidth;
				listColumnIndices.AddTail( nColNo - 1 );
				listColumnIndices.AddTail( nColNo );
				nLastColNo = nColNo;
			}
		} // for( nColNo = 0, nCurrentPageExtent = 0; nColNo < nColCount; nColNo ++ )
		listColumnIndices.AddTail( nColCount - 1 );

		nPageCount = nHorzPageCount * nVertPageCount;
		if( nPageCount > INT(USHRT_MAX) )
			throw 0;
		CRect rcVisibleRange( 0, 0, nColCount, nRowCount );
		if( ! MetafileCache_Init( nPageCount ) )
			throw 0;
		bMetafileCacheStarted = true;
		POSITION posHorz = NULL, posVert = listRowIndices.GetHeadPosition();
		for( nPageIndex = 0; nPageIndex < nPageCount; nPageIndex ++ )
		{
			if( (nPageIndex%nHorzPageCount) == 0 )
			{
				nRowNoFrom = listRowIndices.GetNext( posVert );
				nRowNoTo = listRowIndices.GetNext( posVert );
				posHorz = listColumnIndices.GetHeadPosition();
			}
			nColNoFrom = listColumnIndices.GetNext( posHorz );
			nColNoTo = listColumnIndices.GetNext( posHorz );
			CExtSafeString strMetafileName;
			if( MetafileCache_DiskModeGet() )
				strMetafileName = MetafileCache_GetUniqueName( nPageIndex );
			CMetaFileDC dcMetafile;
			if( m_bUseEnhancedMetafiles )
			{
				if( ! dcMetafile.CreateEnhanced(
						pDC, // NULL,
						( ! strMetafileName.IsEmpty() )
							? LPCTSTR(strMetafileName)
							: NULL
							,
						rcEmfExtent,
						NULL
						)
					)
				{
					ASSERT( FALSE );
					throw 0;
				}
			}
			else
			{
				if( ! dcMetafile.Create(
						( ! strMetafileName.IsEmpty() )
							? LPCTSTR(strMetafileName)
							: NULL
						)
					)
				{
					ASSERT( FALSE );
					throw 0;
				}
			}
			dcMetafile.SetAttribDC( pDC->GetSafeHdc() );
			dcMetafile.SetBkMode( TRANSPARENT );
			dcMetafile.SaveDC();
			OnPrepareDC( &dcMetafile, pInfo );
			nRowOffset = rcOuterPartExtents.top + m_rcPageHeaderFooterGutters.top;
			INT nMaxColOffset = 0;
			bool bProgressAllowsToContinue = false;
			for( nRowNo = nRowNoFrom; nRowNo <= nRowNoTo; nRowNo ++ )
			{
				bProgressAllowsToContinue = 
					_Internal_Notify_OnPreparePrinting_Progress(
						1,              // nPassNo
						2,              // nPassCount
						nPageIndex,     // nPageNo
						nPageCount,     // nPageCount
						nHorzPageCount, // nContentWidthInPages
						nRowNo,         // nDocumentItemNo
						nDocItemCount,  // nDocumentItemCount
						nRowNoFrom,     // nInPageDocumentItemFirstNo
						nRowNoTo        // nInPageDocumentItemLastNo
						);
				if( ! bProgressAllowsToContinue )
					break;
				if( ! OnGridQueryPpvwVisibilityForRow( nRowNo, 0 ) )
					continue;
				if( bEnsureScrollPosV )
					EnsureVisibleRow( nRowNo, bRedrawWhileEnsureScrollPosV );
				nRowHeight = arrRowHeights[ nRowNo + rcOuterPartCounts.top ];
				nColOffset =  m_rcPageHeaderFooterGutters.left;
				if( rcOuterPartCounts.left > 0 )
				{
					for( nColNo = 0L; nColNo < rcOuterPartCounts.left; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
							continue;
						INT nReviewColType = -1, nReviewRowType = 0;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo ];
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + nColWidth,
							nRowOffset + nRowHeight
							);
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_LEFT;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCell(
							pInfo,
							rcOuterPartCounts,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							nRowCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							nReviewColType,
							nReviewRowType,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
						nColOffset += nColWidth;
					} // for( nColNo = 0L; nColNo < rcOuterPartCounts.left; nColNo ++ )
				} // if( rcOuterPartCounts.left > 0 )
				if( nRowNo == nRowNoFrom )
					nMaxColOffset = nColOffset;
				for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
				{
					if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
						continue;
					if( bEnsureScrollPosH )
						EnsureVisibleColumn( nColNo, bRedrawWhileEnsureScrollPosH );
					INT nReviewColType = 0, nReviewRowType = 0;
					CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
					if( pCell == NULL )
						continue;
					ASSERT_VALID( pCell );
					nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
					CRect rcCell(
						nColOffset,
						nRowOffset,
						nColOffset + nColWidth,
						nRowOffset + nRowHeight
						);
					CRect rcCellExtra = rcCell;
					DWORD dwAreaFlags = __EGBWA_INNER_CELLS;
					DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
					OnPreparePrinting_RenderSingleCell(
						pInfo,
						rcOuterPartCounts,
						arrColumnWidths,
						arrRowHeights,
						nColCount,
						nRowCount,
						nPageIndex,
						nColNoFrom,
						nColNoTo,
						nRowNoFrom,
						nRowNoTo,
						pCell,
						dcMetafile,
						nColNo,
						nRowNo,
						nReviewColType,
						nReviewRowType,
						rcCellExtra,
						rcCell,
						rcVisibleRange,
						dwAreaFlags,
						dwHelperPaintFlags
						);
					nColOffset += nColWidth;
				} // for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
				if( rcOuterPartCounts.right > 0 )
				{
					for( nColNo = 0L; nColNo < rcOuterPartCounts.right; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
							continue;
						INT nReviewColType = 1, nReviewRowType = 0;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + nColWidth,
							nRowOffset + nRowHeight
							);
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_RIGHT;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCell(
							pInfo,
							rcOuterPartCounts,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							nRowCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							nReviewColType,
							nReviewRowType,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
						nColOffset += nColWidth;
					} // for( nColNo = 0L; nColNo < rcOuterPartCounts.right; nColNo ++ )
				} // if( rcOuterPartCounts.right > 0 )
				nRowOffset += nRowHeight;
			} // for( nRowNo = nRowNoFrom; nRowNo <= nRowNoTo; nRowNo ++ )
			if( ! bProgressAllowsToContinue )
				break;

			INT nRowOffsetSaved = nRowOffset;
			nRowOffset = m_rcPageHeaderFooterGutters.top;
			if( rcOuterPartCounts.top > 0 )
			{
				for( nRowNo = 0L; nRowNo < rcOuterPartCounts.top; nRowNo ++ )
				{
					if( ! OnGridQueryPpvwVisibilityForRow( nRowNo, -1 ) )
						continue;
					nRowHeight = arrRowHeights[ nRowNo ];
					if( rcOuterPartCounts.left > 0 )
					{ // top-left header cells
						nColOffset = m_rcPageHeaderFooterGutters.left;
						for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
								continue;
							INT nReviewColType = -1, nReviewRowType = -1;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo ];
							CRect rcCell(
								nColOffset,
								nRowOffset,
								nColOffset + nColWidth,
								nRowOffset + nRowHeight
								);
							CRect rcCellExtra = rcCell;
							DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_LEFT|__EGBWA_OUTER_TOP;
							DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
							OnPreparePrinting_RenderSingleCell(
								pInfo,
								rcOuterPartCounts,
								arrColumnWidths,
								arrRowHeights,
								nColCount,
								nRowCount,
								nPageIndex,
								nColNoFrom,
								nColNoTo,
								nRowNoFrom,
								nRowNoTo,
								pCell,
								dcMetafile,
								nColNo,
								nRowNo,
								nReviewColType,
								nReviewRowType,
								rcCellExtra,
								rcCell,
								rcVisibleRange,
								dwAreaFlags,
								dwHelperPaintFlags
								);
							nColOffset += nColWidth;
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
					} // top-left header cells
					nColOffset = nMaxColOffset;
					for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
							continue;
						if( bEnsureScrollPosH )
							EnsureVisibleColumn( nColNo, bRedrawWhileEnsureScrollPosH );
						INT nReviewColType = 0, nReviewRowType = -1;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + nColWidth,
							nRowOffset + nRowHeight
							);
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_TOP;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCell(
							pInfo,
							rcOuterPartCounts,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							nRowCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							nReviewColType,
							nReviewRowType,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
						nColOffset += nColWidth;
					} // for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					if( rcOuterPartCounts.right > 0 )
					{ // top-right header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
								continue;
							INT nReviewColType = 1, nReviewRowType = -1;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
							CRect rcCell(
								nColOffset,
								nRowOffset,
								nColOffset + nColWidth,
								nRowOffset + nRowHeight
								);
							CRect rcCellExtra = rcCell;
							DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_RIGHT|__EGBWA_OUTER_TOP;
							DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
							OnPreparePrinting_RenderSingleCell(
								pInfo,
								rcOuterPartCounts,
								arrColumnWidths,
								arrRowHeights,
								nColCount,
								nRowCount,
								nPageIndex,
								nColNoFrom,
								nColNoTo,
								nRowNoFrom,
								nRowNoTo,
								pCell,
								dcMetafile,
								nColNo,
								nRowNo,
								nReviewColType,
								nReviewRowType,
								rcCellExtra,
								rcCell,
								rcVisibleRange,
								dwAreaFlags,
								dwHelperPaintFlags
								);
							nColOffset += nColWidth;
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
					} // top-right header cells
					nRowOffset += nRowHeight;
				} // for( nRowNo = 0L; nRowNo < rcOuterPartCounts.top; nRowNo ++ )
			} // if( rcOuterPartCounts.top > 0 )
			nRowOffset = nRowOffsetSaved;
			if( rcOuterPartCounts.bottom > 0 )
			{
				for( nRowNo = 0L; nRowNo < rcOuterPartCounts.bottom; nRowNo ++ )
				{
					if( ! OnGridQueryPpvwVisibilityForRow( nRowNo, 1 ) )
						continue;
					nRowHeight = arrRowHeights[ nRowNo + rcOuterPartCounts.top + nRowCount ];
					if( rcOuterPartCounts.left > 0 )
					{ // bottom-left header cells
						nColOffset = m_rcPageHeaderFooterGutters.left;
						for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
								continue;
							INT nReviewColType = -1, nReviewRowType = 1;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo ];
							CRect rcCell(
								nColOffset,
								nRowOffset,
								nColOffset + nColWidth,
								nRowOffset + nRowHeight
								);
							CRect rcCellExtra = rcCell;
							DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_LEFT|__EGBWA_OUTER_BOTTOM;
							DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
							OnPreparePrinting_RenderSingleCell(
								pInfo,
								rcOuterPartCounts,
								arrColumnWidths,
								arrRowHeights,
								nColCount,
								nRowCount,
								nPageIndex,
								nColNoFrom,
								nColNoTo,
								nRowNoFrom,
								nRowNoTo,
								pCell,
								dcMetafile,
								nColNo,
								nRowNo,
								nReviewColType,
								nReviewRowType,
								rcCellExtra,
								rcCell,
								rcVisibleRange,
								dwAreaFlags,
								dwHelperPaintFlags
								);
							nColOffset += nColWidth;
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
					} // bottom-left header cells
					nColOffset = nMaxColOffset;
					for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
							continue;
						if( bEnsureScrollPosH )
							EnsureVisibleColumn( nColNo, bRedrawWhileEnsureScrollPosH );
						INT nReviewColType = 0, nReviewRowType = 1;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + nColWidth,
							nRowOffset + nRowHeight
							);
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_BOTTOM;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCell(
							pInfo,
							rcOuterPartCounts,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							nRowCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							nReviewColType,
							nReviewRowType,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
						nColOffset += nColWidth;
					} // for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					if( rcOuterPartCounts.right > 0 )
					{ // bottom-right header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
								continue;
							INT nReviewColType = 1, nReviewRowType = 1;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
							CRect rcCell(
								nColOffset,
								nRowOffset,
								nColOffset + nColWidth,
								nRowOffset + nRowHeight
								);
							CRect rcCellExtra = rcCell;
							DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_RIGHT|__EGBWA_OUTER_BOTTOM;
							DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
							OnPreparePrinting_RenderSingleCell(
								pInfo,
								rcOuterPartCounts,
								arrColumnWidths,
								arrRowHeights,
								nColCount,
								nRowCount,
								nPageIndex,
								nColNoFrom,
								nColNoTo,
								nRowNoFrom,
								nRowNoTo,
								pCell,
								dcMetafile,
								nColNo,
								nRowNo,
								nReviewColType,
								nReviewRowType,
								rcCellExtra,
								rcCell,
								rcVisibleRange,
								dwAreaFlags,
								dwHelperPaintFlags
								);
							nColOffset += nColWidth;
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
					} // bottom-right header cells
					nRowOffset += nRowHeight;
				} // for( nRowNo = 0L; nRowNo < rcOuterPartCounts.bottom; nRowNo ++ )
			} // if( rcOuterPartCounts.bottom > 0 )

			CRect rcEntirePage( 0, 0, nMeasureDpiDX, nMeasureDpiDY );
			OnDrawPageHeaderFooterGutters( dcMetafile, rcEntirePage, m_rcPageHeaderFooterGutters, nPageIndex, nPageCount, pInfo );

			dcMetafile.RestoreDC( -1 );
			if( ! bProgressAllowsToContinue )
				throw 0;
			HANDLE hMetafile =
				m_bUseEnhancedMetafiles
					? ( (HANDLE) dcMetafile.CloseEnhanced() )
					: ( (HANDLE) dcMetafile.Close() )
					;
			if( ! MetafileCache_InitAt(
					nPageIndex,
					hMetafile,
					( ! strMetafileName.IsEmpty() )
						? LPCTSTR(strMetafileName)
						: NULL
					)
				)
				throw 0;
		} // for( nPageIndex = 0; nPageIndex < nPageCount; nPageIndex ++ )
		WORD nPageNoMin = WORD(1), nPageNoMax = WORD( WORD(nPageCount) + WORD(nPageNoMin) - WORD(1) );
		pInfo->m_nNumPreviewPages = nPageNoMax;
		pInfo->m_pPD->m_pd.nMinPage = nPageNoMin;
		pInfo->m_pPD->m_pd.nFromPage = nPageNoMin;
		pInfo->m_pPD->m_pd.nMaxPage = nPageNoMax;
		pInfo->m_pPD->m_pd.nToPage = nPageNoMax;
		bOK = true;
	} // try
	catch( CException * pException )
	{
		pException->Delete();
	} // catch( CException * pException )
	catch( ... )
	{
	} // catch( ... )
	pDC->RestoreDC( -1 );
	if( bMetafileCacheStarted )
		MetafileCache_InitComplete( bOK );
	if( bEnsureScrollPosH || bEnsureScrollPosV )
		OnSwSetScrollPos( _ptScrollPosBefore );
	OnPreparePrinting_End( ! bOK );
	_WaitCursor;
	if( ! bOK )
		return FALSE;
	else
		return TRUE;
}

#endif // #if (!defined __EXT_MFC_NO_GRIDBASEWND)

#if (!defined __EXT_MFC_NO_TREEGRIDWND)

/////////////////////////////////////////////////////////////////////////////
// template < > class CExtPPVW < CExtTreeGridWnd >

void CExtPPVW < CExtTreeGridWnd > :: OnPrepareDC(
	CDC * pDC,
	CPrintInfo * pInfo // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC );
	ASSERT( pDC->GetSafeHdc() != NULL );
	pInfo;

CFont * pFont = & OnSiwGetDefaultFont();
 	if( pFont->GetSafeHandle() == NULL )
 		return;
	pDC->SelectObject( pFont );
}

bool CExtPPVW < CExtTreeGridWnd > :: OnCalcPrintableTreeItemArrayImpl(
	HTREEITEM htiParent,
	CList < HTREEITEM, HTREEITEM & > & listPrintableItems
	)
{
	ASSERT_VALID( this );
	ASSERT( htiParent != NULL );
	if( ! OnTreeGridQueryPpvwVisibilityForItem( CExtTreeGridCellNode::FromHTREEITEM(htiParent) ) )
		return false;
LONG nIndex, nCount = ItemGetChildCount( htiParent );
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		HTREEITEM hti = ItemGetChildAt( htiParent, nIndex );
		ASSERT( hti != NULL );
		if( CExtTreeGridCellNode::FromHTREEITEM( hti )->TreeNodeHiddenGet() )
			continue;
		listPrintableItems.AddTail( hti );
		POSITION pos = listPrintableItems.GetTailPosition();
		ASSERT( pos != NULL );
		if( ! OnCalcPrintableTreeItemArrayImpl( hti, listPrintableItems ) )
			listPrintableItems.RemoveAt( pos );
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return true;
}

INT CExtPPVW < CExtTreeGridWnd > :: OnCalcPrintableTreeItemIndent(
	CDC & dcMeasure,
	HTREEITEM hti
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&dcMeasure) );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	ASSERT( hti != NULL );
	dcMeasure;
CExtTreeGridCellNode * pNode = (CExtTreeGridCellNode *)hti;
	ASSERT_VALID( pNode );
	ASSERT_KINDOF( CExtTreeGridCellNode, pNode );
INT nIndent = 0;
	for( ; pNode != NULL; )
	{
		CExtTreeGridCellNode * pNodeParent = pNode->TreeNodeGetParent();
		if( pNodeParent == NULL )
			break;
		INT nOneLevelIndent = INT( pNode->TreeNodeIndentPxGet() );
		if( nOneLevelIndent < 0 )
			nOneLevelIndent = GetTreeData().m_nIndentPxDefault;
		nIndent += nOneLevelIndent;
		pNode = pNodeParent;
	}
	return nIndent;
}

void CExtPPVW < CExtTreeGridWnd > :: OnCalcPrintableTreeItemArray(
	CArray < HTREEITEM, HTREEITEM & > & arrPrintableItems
	)
{
	ASSERT_VALID( this );
	arrPrintableItems.RemoveAll();
CList < HTREEITEM, HTREEITEM & > listPrintableItems;
HTREEITEM htiRoot = ItemGetRoot();
	if( htiRoot == NULL )
		return;
	OnCalcPrintableTreeItemArrayImpl( htiRoot, listPrintableItems );
LONG nIndex, nCount = LONG( listPrintableItems.GetCount() );
	if( nCount == 0 )
		return;
	arrPrintableItems.SetSize( nCount );
POSITION pos = listPrintableItems.GetHeadPosition();
	for( nIndex = 0; pos != NULL; nIndex ++ )
	{
		HTREEITEM hti = listPrintableItems.GetNext( pos );
		ASSERT( hti != NULL );
		arrPrintableItems.SetAt( nIndex, hti );
	} // for( nIndex = 0; pos != NULL; nIndex ++ )
	listPrintableItems.RemoveAll();
}

bool CExtPPVW < CExtTreeGridWnd > :: IsPrintPreviewAvailable() const
{
	ASSERT_VALID( this );
	if( ! CExtPPVW_Printable::IsPrintPreviewAvailable() )
		return false;
	if( GetSafeHwnd() == NULL )
		return false;
bool bExternalData = ExternalDataGet();
	if( bExternalData )
		return false;
DWORD dwScrollTypeH = SiwScrollTypeHGet();
	if( dwScrollTypeH == __ESIW_ST_VIRTUAL )
		return false;
DWORD dwScrollTypeV = SiwScrollTypeVGet();
	if( dwScrollTypeV == __ESIW_ST_VIRTUAL )
		return false;
LONG nColCount = ColumnCountGet();
	if( nColCount <= 0 )
		return false;
LONG nRowCount = RowCountGet();
	if( nRowCount <= 0 )
		return false;
	return true;
}

void CExtPPVW < CExtTreeGridWnd > :: OnPreparePrinting_RenderSingleCellAtTopOrBottom(
	CPrintInfo * pInfo,
	const CRect & rcOuterPartCounts,
	const CArray < INT, INT & > & arrColumnWidths,
	const CArray < INT, INT & > & arrRowHeights,
	LONG nColCount,
	LONG nRowCount,
	INT nPageIndex,
	LONG nColNoFrom,
	LONG nColNoTo,
	LONG nRowNoFrom,
	LONG nRowNoTo,
	CExtGridCell * pCell,
	CDC & dc,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	CRect & rcCellExtra,
	CRect & rcCell,
	CRect & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT_VALID( pCell );
	ASSERT( nRowType != 0 );
	pInfo;
	nPageIndex;
	nColNoTo;
	nRowNoTo;
	nRowType;
bool bDrawCell = true;
CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, nColType, nRowType );
	if( _sizeJoin.cx != 1 || _sizeJoin.cy != 1 )
	{
		LONG nColNoA = nColNo, nRowNoA = nRowNo;
		bool bRootMode = ( _sizeJoin.cx >= 1 && _sizeJoin.cy >= 1 ) ? true : false;
		if(		bRootMode
			||	OnGbwCellJoinAdjustCoordinates( nColNoA, nRowNoA, nColType, nRowType )
			)
		{
			bDrawCell = false;
			pCell = NULL;
			if(		bRootMode
				||	( nColType == 0 && nColNo == nColNoFrom && _sizeJoin.cy >= 1 )
				||	( nRowType == 0 && nRowNo == nRowNoFrom && _sizeJoin.cx >= 1 )
				)
				pCell = GridCellGet( nColNoA, nRowNoA, nColType, nRowType );
			if( pCell != NULL )
			{
				bDrawCell = true;
				CPoint _VpShift( 0, 0 );
				CSize _VpSize( 0, 0 );
				INT nStepIndex, nStepCount;
				if( _sizeJoin.cx < 0 )
				{
					nStepCount = - _sizeJoin.cx;
					for( nStepIndex = 0; nStepIndex < nStepCount; nStepIndex ++ )
					{
						INT nEffectiveIndex = nColNo - nStepIndex - 1;
						if( nColType == 0 )
							nEffectiveIndex += rcOuterPartCounts.left;
						else if( nColType > 0 )
							nEffectiveIndex += rcOuterPartCounts.left + nColCount;
						INT nExt = arrColumnWidths[ nEffectiveIndex ];
						_VpShift.x += nExt;
					}
				}
				if( _sizeJoin.cy < 0 )
				{
					nStepCount = - _sizeJoin.cy;
					for( nStepIndex = 0; nStepIndex < nStepCount; nStepIndex ++ )
					{
						INT nEffectiveIndex = nRowNo - nStepIndex - 1;
						if( nRowType == 0 )
							nEffectiveIndex += rcOuterPartCounts.top;
						else if( nRowType > 0 )
							nEffectiveIndex += rcOuterPartCounts.top + nRowCount;
						INT nExt = arrRowHeights[ nEffectiveIndex ];
						_VpShift.y += nExt;
					}
				}
				CSize _sizeJoinA = OnGbwCellJoinQueryInfo( nColNoA, nRowNoA, nColType, nRowType );
				ASSERT( _sizeJoinA.cx >= 1 && _sizeJoinA.cy >= 1 );
				nStepCount = _sizeJoinA.cx;
				for( nStepIndex = 0; nStepIndex < nStepCount; nStepIndex ++ )
				{
					INT nEffectiveIndex = nColNoA + nStepIndex;
					if( nColType == 0 )
						nEffectiveIndex += rcOuterPartCounts.left;
					else if( nColType > 0 )
						nEffectiveIndex += rcOuterPartCounts.left + nColCount;
					INT nExt = arrColumnWidths[ nEffectiveIndex ];
					_VpSize.cx += nExt;
				}
				nStepCount = _sizeJoinA.cy;
				for( nStepIndex = 0; nStepIndex < nStepCount; nStepIndex ++ )
				{
					INT nEffectiveIndex = nRowNoA + nStepIndex;
					if( nRowType == 0 )
						nEffectiveIndex += rcOuterPartCounts.top;
					else if( nRowType > 0 )
						nEffectiveIndex += rcOuterPartCounts.top + nRowCount;
					INT nExt = arrRowHeights[ nEffectiveIndex ];
					_VpSize.cy += nExt;
				}
				rcCell.left  -= _VpShift.x;
				rcCell.top   -= _VpShift.y;
				rcCell.right  = rcCell.left + _VpSize.cx;
				rcCell.bottom = rcCell.top  + _VpSize.cy;
				rcCellExtra = rcCell;
			} // if( pCell != NULL )
		}
	} // if( _sizeJoin.cx != 1 || _sizeJoin.cy != 1 )
	if( bDrawCell )
	{
		ASSERT_VALID( pCell );
		if( m_bDrawSimpleWhiteBackground )
		{
			CRect rcLines = rcCell;
			rcLines.right ++;
			rcLines.bottom ++;
			HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(WHITE_BRUSH) );
			HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_PEN) );
			dc.Rectangle( &rcLines );
			::SelectObject( dc.m_hDC, hOldPen );
			::SelectObject( dc.m_hDC, hOldBrush );
		} // if( m_bDrawSimpleWhiteBackground )
		pCell->OnPaintBackground(
			*this,
			dc,
			nColNo,
			nRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
		pCell->OnPaintForeground(
			*this,
			dc,
			nColNo,
			nRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
		if( m_bDrawSimpleBlackBorders )
		{
			CRect rcLines = rcCell;
			rcLines.right ++;
			rcLines.bottom ++;
			HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_BRUSH) );
			HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(BLACK_PEN) );
			dc.Rectangle( &rcLines );
			::SelectObject( dc.m_hDC, hOldPen );
			::SelectObject( dc.m_hDC, hOldBrush );
		} // if( m_bDrawSimpleBlackBorders )
	} // if( bDrawCell )
}

void CExtPPVW < CExtTreeGridWnd > :: OnPreparePrinting_RenderSingleCellAtCenter(
	CPrintInfo * pInfo,
	const CRect & rcOuterPartCounts,
	const CArray < HTREEITEM, HTREEITEM & > & arrPrintableItems,
	const CArray < INT, INT & > & arrColumnWidths,
	const CArray < INT, INT & > & arrRowHeights,
	LONG nColCount,
	//LONG nRowCount,
	INT nPageIndex,
	LONG nColNoFrom,
	LONG nColNoTo,
	LONG nRowNoFrom,
	LONG nRowNoTo,
	CExtGridCell * pCell,
	CDC & dc,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	//INT nRowType,
	CRect & rcCellExtra,
	CRect & rcCell,
	CRect & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT_VALID( pCell );
	//ASSERT( nRowType == 0 );
	pInfo;
	nPageIndex;
	nColNoTo;
	nRowNoTo;
	//nRowType;
HTREEITEM hti = arrPrintableItems[ nRowNo ];
	ASSERT( hti != NULL );
bool bDrawCell = true;
CSize _sizeJoin = ItemCellJoinGet( hti, nColNo, nColType );
	if( _sizeJoin.cx != 1 || _sizeJoin.cy != 1 )
	{
		LONG nColNoA = nColNo;
		HTREEITEM htiA = hti;
		ASSERT( htiA != NULL );
		bool bRootMode = ( _sizeJoin.cx >= 1 && _sizeJoin.cy >= 1 ) ? true : false;
		if(		bRootMode
			||	ItemCellJoinAdjustCoordinates( htiA, nColNoA, nColType )
			)
		{
			bDrawCell = false;
			pCell = NULL;
			if(		bRootMode
				||	( nColType == 0 && nColNo == nColNoFrom && _sizeJoin.cy >= 1 )
				||	(                  nRowNo == nRowNoFrom && _sizeJoin.cx >= 1 )
				)
				pCell = ItemGetCell( htiA, nColNoA, nColType );
			if( pCell != NULL )
			{
				bDrawCell = true;
				CPoint _VpShift( 0, 0 );
				CSize _VpSize( 0, 0 );
				INT nStepIndex, nStepCount;
				if( _sizeJoin.cx < 0 )
				{
					nStepCount = - _sizeJoin.cx;
					for( nStepIndex = 0; nStepIndex < nStepCount; nStepIndex ++ )
					{
						INT nEffectiveIndex = nColNo - nStepIndex - 1;
						if( nColType == 0 )
							nEffectiveIndex += rcOuterPartCounts.left;
						else if( nColType > 0 )
							nEffectiveIndex += rcOuterPartCounts.left + nColCount;
						INT nExt = arrColumnWidths[ nEffectiveIndex ];
						_VpShift.x += nExt;
					}
				}
				if( _sizeJoin.cy < 0 )
				{
					nStepCount = - _sizeJoin.cy;
					for( nStepIndex = 0; nStepIndex < nStepCount; nStepIndex ++ )
					{
						INT nEffectiveIndex = nRowNo - nStepIndex - 1 + rcOuterPartCounts.top;
						INT nExt = arrRowHeights[ nEffectiveIndex ];
						_VpShift.y += nExt;
					}
				}
				CSize _sizeJoinA = ItemCellJoinGet( htiA, nColNoA, nColType );
				ASSERT( _sizeJoinA.cx >= 1 && _sizeJoinA.cy >= 1 );
				nStepCount = _sizeJoinA.cx;
				for( nStepIndex = 0; nStepIndex < nStepCount; nStepIndex ++ )
				{
					INT nEffectiveIndex = nColNoA + nStepIndex;
					if( nColType == 0 )
						nEffectiveIndex += rcOuterPartCounts.left;
					else if( nColType > 0 )
						nEffectiveIndex += rcOuterPartCounts.left + nColCount;
					INT nExt = arrColumnWidths[ nEffectiveIndex ];
					_VpSize.cx += nExt;
				}
				nStepCount = _sizeJoinA.cy;
				LONG nRowNoA = ItemGetVisibleIndexOf( htiA, false );
				ASSERT( arrPrintableItems[ nRowNoA ] == htiA );
				for( nStepIndex = 0; nStepIndex < nStepCount; nStepIndex ++ )
				{
					INT nEffectiveIndex = nRowNoA + nStepIndex + rcOuterPartCounts.top;
					INT nExt = arrRowHeights[ nEffectiveIndex ];
					_VpSize.cy += nExt;
				}
				rcCell.left  -= _VpShift.x;
				rcCell.top   -= _VpShift.y;
				rcCell.right  = rcCell.left + _VpSize.cx;
				rcCell.bottom = rcCell.top  + _VpSize.cy;
				rcCellExtra = rcCell;
			} // if( pCell != NULL )
		}
	} // if( _sizeJoin.cx != 1 || _sizeJoin.cy != 1 )
	if( bDrawCell )
	{
		ASSERT_VALID( pCell );
		if( m_bDrawSimpleWhiteBackground )
		{
			CRect rcLines = rcCell;
			rcLines.right ++;
			rcLines.bottom ++;
			HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(WHITE_BRUSH) );
			HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_PEN) );
			dc.Rectangle( &rcLines );
			::SelectObject( dc.m_hDC, hOldPen );
			::SelectObject( dc.m_hDC, hOldBrush );
		} // if( m_bDrawSimpleWhiteBackground )
		pCell->OnPaintBackground(
			*this,
			dc,
			nColNo,
			nRowNo,
			nColNo,
			nRowNo,
			nColType,
			0,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
		pCell->OnPaintForeground(
			*this,
			dc,
			nColNo,
			nRowNo,
			nColNo,
			nRowNo,
			nColType,
			0,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
		if( m_bDrawSimpleBlackBorders )
		{
			CRect rcLines = rcCell;
			rcLines.right ++;
			rcLines.bottom ++;
			HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_BRUSH) );
			HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(BLACK_PEN) );
			dc.Rectangle( &rcLines );
			::SelectObject( dc.m_hDC, hOldPen );
			::SelectObject( dc.m_hDC, hOldBrush );
		} // if( m_bDrawSimpleBlackBorders )
		if( nColType == 0 && m_bDrawSimpleBlackOutline )
		{
			if( OnTreeGridQueryColumnOutline( nColNo ) )
			{
				CDC * pDC = CDC::FromHandle( pInfo->m_pPD->m_pd.hDC );
				ASSERT_VALID( pDC );
				INT nOutlineExtent = OnCalcPrintableTreeItemIndent( *pDC, arrPrintableItems[nRowNo] );
				if( nOutlineExtent > 0 )
				{
					HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_BRUSH) );
					HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(BLACK_PEN) );
					COLORREF clrDots = RGB( 0, 0, 0 );
					CRect rcIndent(
						rcCell.left, // - nOutlineExtent,
						rcCell.top,
						rcCell.left,
						rcCell.bottom
						);
					HTREEITEM hRootItem = ItemGetRoot();
					ASSERT( hRootItem != NULL );
					HTREEITEM hTreeItem = hti;
					for( ; hTreeItem != hRootItem; hTreeItem = ItemGetParent(hTreeItem) )
					{
						ASSERT( hTreeItem != NULL );
						CExtTreeGridCellNode * pNode =
							CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
						ASSERT_VALID( pNode );
						INT nPxIndent = (INT)pNode->TreeNodeIndentPxGet();
						if( nPxIndent == ULONG(-1L) )
							nPxIndent = GetTreeData().m_nIndentPxDefault;
						rcIndent.right = rcIndent.left;
						rcIndent.left -= nPxIndent;
						INT nMetricH = rcIndent.Width();
						INT nMetricH2 = nMetricH / 2;
						INT nMetricV = rcIndent.Height();
						INT nMetricV2 = min( nMetricH, nMetricV );
						nMetricV2 /= 2;
						bool bDrawUpperLineV = false;
						if( hti == hTreeItem )
						{
							if( m_bDrawSimpleBlackOutlineUsingDots )
								CExtPaintManager::stat_DrawDotLineH(
									dc,
									rcIndent.left + nMetricH2,
									rcIndent.right,
									rcIndent.top + nMetricV2,
									clrDots
									);
							else
							{
								dc.MoveTo(
									rcIndent.left + nMetricH2,
									rcIndent.top + nMetricV2
									);
								dc.LineTo(
									rcIndent.right,
									rcIndent.top + nMetricV2
									);
							}
							bDrawUpperLineV = true;
						}
						HTREEITEM hTreeItemNext = _ItemBrowseNextImpl( hTreeItem, true, false, false, false );
						if( hTreeItemNext != NULL )
						{
							if( m_bDrawSimpleBlackOutlineUsingDots )
								CExtPaintManager::stat_DrawDotLineV(
									dc,
									rcIndent.left + nMetricH2,
									rcIndent.top + nMetricV2,
									rcIndent.bottom,
									clrDots
									);
							else
							{
								dc.MoveTo(
									rcIndent.left + nMetricH2,
									rcIndent.top + nMetricV2
									);
								dc.LineTo(
									rcIndent.left + nMetricH2,
									rcIndent.bottom
									);
							}
							bDrawUpperLineV = true;
						}
						if( bDrawUpperLineV )
						{
							if( m_bDrawSimpleBlackOutlineUsingDots )
								CExtPaintManager::stat_DrawDotLineV(
									dc,
									rcIndent.left + nMetricH2,
									rcIndent.top,
									rcIndent.top + nMetricV2,
									clrDots
									);
							else
							{
								dc.MoveTo(
									rcIndent.left + nMetricH2,
									rcIndent.top
									);
								dc.LineTo(
									rcIndent.left + nMetricH2,
									rcIndent.top + nMetricV2
									);
							}
						}
						//if( hti == hTreeItem )
						//{
						//	CExtTreeGridCellNode::e_expand_box_shape_t eEBS =
						//		pNode->TreeNodeGetExpandBoxShape();
						//	if( eEBS != CExtTreeGridCellNode::__EEBS_NONE )
						//	{
						//		CRect rcIndentPaint = rcIndent;
						//		if(		rcIndentPaint.left == rcIndentPaint.right
						//			&&	m_bAdustNullIndent
						//			)
						//		{
						//			INT nPxIndentPaint = nPxIndent;
						//			if( nPxIndentPaint == 0 )
						//				nPxIndentPaint = GetTreeData().m_nIndentPxDefault;
						//			rcIndentPaint.left -= nPxIndentPaint;
						//		}
						//		if( rcIndentPaint.left < rcIndentPaint.right )
						//			OnTreeGridPaintExpandButton(
						//				dc,
						//				hTreeItem,
						//				( eEBS == CExtTreeGridCellNode::__EEBS_EXPANDED )
						//					? true : false,
						//				rcIndentPaint
						//				);
						//	}
						//} // if( hti == hTreeItem )
					} // for( ; hTreeItem != hRootItem; hTreeItem = ItemGetParent(hTreeItem) )
					::SelectObject( dc.m_hDC, hOldPen );
					::SelectObject( dc.m_hDC, hOldBrush );
				} // if( nOutlineExtent > 0 )
			} // if( OnTreeGridQueryColumnOutline( nColNo ) )
		} // if( m_bDrawSimpleBlackOutline )
	} // if( bDrawCell )
}

BOOL CExtPPVW < CExtTreeGridWnd > :: OnPreparePrinting(
	CPrintInfo * pInfo
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return FALSE;

bool bExternalData = ExternalDataGet();
	if( bExternalData )
		return FALSE;
DWORD dwScrollTypeH = SiwScrollTypeHGet();
	if( dwScrollTypeH == __ESIW_ST_VIRTUAL )
		return FALSE;
DWORD dwScrollTypeV = SiwScrollTypeVGet();
	if( dwScrollTypeV == __ESIW_ST_VIRTUAL )
		return FALSE;

LONG nColCount = ColumnCountGet();
	if( nColCount <= 0 )
		return FALSE;
CArray < HTREEITEM, HTREEITEM & > arrPrintableItems;
	OnCalcPrintableTreeItemArray( arrPrintableItems );
LONG nRowCount = LONG( arrPrintableItems.GetSize() );
	if( nRowCount <= 0 )
		return FALSE;
	if( ! CExtPPVW_Printable::OnPreparePrinting( pInfo ) )
		return FALSE;
CWaitCursor _WaitCursor;
	ASSERT( pInfo->m_pPD->m_pd.hDC != NULL );
CDC * pDC = CDC::FromHandle( pInfo->m_pPD->m_pd.hDC );
	ASSERT_VALID( pDC );
INT nPrinterDpiX = ::GetDeviceCaps( pDC->m_hDC, LOGPIXELSX );
INT nPrinterDpiY = ::GetDeviceCaps( pDC->m_hDC, LOGPIXELSY );
INT nPrinterDpiDX = ::GetDeviceCaps( pDC->m_hDC, HORZRES );
INT nPrinterDpiDY = ::GetDeviceCaps( pDC->m_hDC, VERTRES );
INT nMeasureDpiDX = ::MulDiv( nPrinterDpiDX, g_PaintManager.m_nLPX, nPrinterDpiX );
INT nMeasureDpiDY = ::MulDiv( nPrinterDpiDY, g_PaintManager.m_nLPY, nPrinterDpiY );
// CSize _sizePrinterOffset(
// 		pDC->GetDeviceCaps(PHYSICALOFFSETX),
// 		pDC->GetDeviceCaps(PHYSICALOFFSETY)
// 		);
// 	nMeasureDpiDX -= ::MulDiv( _sizePrinterOffset.cx * 2, g_PaintManager.m_nLPX, nPrinterDpiX );
// 	nMeasureDpiDY -= ::MulDiv( _sizePrinterOffset.cy * 2, g_PaintManager.m_nLPY, nPrinterDpiY );

CSize sizePMLT( m_rcPageMarginsHM.left, m_rcPageMarginsHM.top ), sizePMRB( m_rcPageMarginsHM.right, m_rcPageMarginsHM.bottom ); 
	pDC->HIMETRICtoDP( &sizePMLT );
	pDC->HIMETRICtoDP( &sizePMRB );
	sizePMLT.cx = ::MulDiv( sizePMLT.cx, g_PaintManager.m_nLPX, nPrinterDpiX );
	sizePMRB.cx = ::MulDiv( sizePMRB.cx, g_PaintManager.m_nLPX, nPrinterDpiX );
	sizePMLT.cy = ::MulDiv( sizePMLT.cy, g_PaintManager.m_nLPY, nPrinterDpiY );
	sizePMRB.cy = ::MulDiv( sizePMRB.cy, g_PaintManager.m_nLPY, nPrinterDpiY );
	nMeasureDpiDX -= sizePMLT.cx + sizePMRB.cx;
	nMeasureDpiDY -= sizePMLT.cy + sizePMRB.cy;

// 	pDC->DPtoLP( &_sizePrinterOffset );
CSize _sizePrinterHIMETRIC( nPrinterDpiDX, nPrinterDpiDY );
// 	_sizePrinterHIMETRIC -= _sizePrinterOffset;
// 	_sizePrinterHIMETRIC -= _sizePrinterOffset;
	pDC->DPtoHIMETRIC( &_sizePrinterHIMETRIC );
	//_sizePrinterHIMETRIC.cx -= m_rcPageMarginsHM.left + m_rcPageMarginsHM.right;
	//_sizePrinterHIMETRIC.cy -= m_rcPageMarginsHM.top + m_rcPageMarginsHM.bottom;
CRect rcEmfExtent( 0, 0, _sizePrinterHIMETRIC.cx, _sizePrinterHIMETRIC.cy );
DWORD dwHelperPaintFlagsBasic = m_dwBasicPaintFlags;
	if( m_pWndPP != NULL )
		dwHelperPaintFlagsBasic |= __EGCPF_PRINT_PREVIEW;
	else
		dwHelperPaintFlagsBasic |= __EGCPF_PRINTER;
	pDC->SaveDC();
	OnPrepareDC( pDC );
bool bOK = false, bMetafileCacheStarted = false;
	try
	{
		OnPreparePrinting_Begin();
		CRect rcOuterPartCounts(
			OuterColumnCountLeftGet(),
			OuterRowCountTopGet(),
			OuterColumnCountRightGet(),
			OuterRowCountBottomGet()
			);
		CRect rcOuterPartExtents( 0, 0, 0, 0 ),
			rcOuterMinExtents(
				g_PaintManager->UiScalingDo( __EXT_PRINT_OUTER_MIN_EXTENTS_LEFT,	CExtPaintManager::__EUIST_X ),
				g_PaintManager->UiScalingDo( __EXT_PRINT_OUTER_MIN_EXTENTS_TOP,		CExtPaintManager::__EUIST_Y ),
				g_PaintManager->UiScalingDo( __EXT_PRINT_OUTER_MIN_EXTENTS_RIGHT,	CExtPaintManager::__EUIST_X ),
				g_PaintManager->UiScalingDo( __EXT_PRINT_OUTER_MIN_EXTENTS_BOTTOM,	CExtPaintManager::__EUIST_Y )
				);
		ASSERT( rcOuterPartCounts.left >= 0 );
		ASSERT( rcOuterPartCounts.top >= 0 );
		ASSERT( rcOuterPartCounts.right >= 0 );
		ASSERT( rcOuterPartCounts.bottom >= 0 );
		CArray < INT, INT & > arrColumnWidths, arrRowHeights;
		arrColumnWidths.SetSize( nColCount + rcOuterPartCounts.left + rcOuterPartCounts.right );
		arrRowHeights.SetSize( nRowCount + rcOuterPartCounts.top + rcOuterPartCounts.bottom );
		CList < LONG, LONG > listColumnIndices, listRowIndices;
		LONG nColNo = 0L, nRowNo = 0L,
			nLastColNo = 0L, nColNoFrom = 0L, nColNoTo = 0L,
			nLastRowNo = 0L, nRowNoFrom = 0L, nRowNoTo = 0L;
		INT nColWidth = 0, nRowHeight = 0, nPageIndex = 0, nPageCount = 0,
			nHorzPageCount = 1, nVertPageCount = 1, nCurrentPageExtent = 0,
			nColOffset = 0, nRowOffset = 0;
		LONG nDocItemCount = nRowCount;
		for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		{
			if( ! _Internal_Notify_OnPreparePrinting_Progress(
					0,             // nPassNo
					2,             // nPassCount
					-1,            // nPageNo
					-1,            // nPageCount
					-1,            // nContentWidthInPages
					nRowNo,        // nDocumentItemNo
					nDocItemCount, // nDocumentItemCount
					-1,            // nInPageDocumentItemFirstNo,
					-1             // nInPageDocumentItemLastNo
					)
				)
				throw 0;
			nRowHeight = 0L;
			for( nColNo = 0; nColNo < nColCount; nColNo ++ )
			{
				if( OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
				{
					if( nRowNo == 0L )
						arrColumnWidths[ nColNo + rcOuterPartCounts.left ] = nColWidth = 0;
					else
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
					CExtGridCell * pCell = ItemGetCell( arrPrintableItems[nRowNo], nColNo, 0 );
					if( pCell == NULL )
						continue;
					ASSERT_VALID( pCell );
					CSize _sizeJoin = ItemCellJoinGet( arrPrintableItems[nRowNo], nColNo, 0 );
					if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
						continue;
					CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
					ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
					if( OnTreeGridQueryColumnOutline( nColNo ) )
						_sizeCell.cx += OnCalcPrintableTreeItemIndent( *pDC, arrPrintableItems[nRowNo] );
					if( _sizeJoin.cx != 1 )
						_sizeCell.cx = 0;
					if( _sizeJoin.cy != 1 )
						_sizeCell.cy = 0;
					nColWidth = max( nColWidth, _sizeCell.cx );
					arrColumnWidths[ nColNo + rcOuterPartCounts.left ] = nColWidth;
					nRowHeight = max( nRowHeight, _sizeCell.cy );
				} // if( OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
			} // for( nColNo = 0; nColNo < nColCount; nColNo ++ )
			if( rcOuterPartCounts.left > 0 )
			{
				for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
				{
					if( OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
					{
						CExtGridCell * pCell = ItemGetCell( arrPrintableItems[nRowNo], nColNo, -1, 0 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						CSize _sizeJoin = ItemCellJoinGet( arrPrintableItems[nRowNo], nColNo, -1 );
						if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
							continue;
						if( nRowNo == 0L )
							arrColumnWidths[ nColNo ] = nColWidth = 0;
						else
							nColWidth = arrColumnWidths[ nColNo ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						if( _sizeJoin.cx != 1 )
							_sizeCell.cx = 0;
						if( _sizeJoin.cy != 1 )
							_sizeCell.cy = 0;
						_sizeCell.cx = max( _sizeCell.cx, rcOuterMinExtents.left );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // if( OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
				} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
			} // if( rcOuterPartCounts.left > 0 )
			if( rcOuterPartCounts.right > 0 )
			{
				for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
				{
					if( OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
					{
						CExtGridCell * pCell = ItemGetCell( arrPrintableItems[nRowNo], nColNo, 1, 0 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						CSize _sizeJoin = ItemCellJoinGet( arrPrintableItems[nRowNo], nColNo, 1 );
						if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
							continue;
						if( nRowNo == 0L )
							arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ] = nColWidth = 0;
						else
							nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						if( _sizeJoin.cx != 1 )
							_sizeCell.cx = 0;
						if( _sizeJoin.cy != 1 )
							_sizeCell.cy = 0;
						_sizeCell.cx = max( _sizeCell.cx, rcOuterMinExtents.right );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // if( OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
				} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
			} // if( rcOuterPartCounts.right > 0 )
			arrRowHeights[ nRowNo + rcOuterPartCounts.top ] = nRowHeight;
		} // for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		if( rcOuterPartCounts.top > 0 )
		{
			for( nRowNo = 0; nRowNo < rcOuterPartCounts.top; nRowNo ++ )
			{
				nRowHeight = 0;
				if( OnGridQueryPpvwVisibilityForRow( nRowNo, -1 ) )
				{
					for( nColNo = 0; nColNo < nColCount; nColNo ++ )
					{
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 0, -1 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, 0, -1 );
						if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
							continue;
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						if( _sizeJoin.cx != 1 )
							_sizeCell.cx = 0;
						if( _sizeJoin.cy != 1 )
							_sizeCell.cy = 0;
						_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo + rcOuterPartCounts.left ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // for( nColNo = 0; nColNo < nColCount; nColNo ++ )
					arrRowHeights[ nRowNo ] = nRowHeight;
					rcOuterPartExtents.top += nRowHeight;
				} // if( OnGridQueryPpvwVisibilityForRow( nRowNo, -1 ) )
				if( rcOuterPartCounts.left > 0 )
				{ // top-left header cells
					for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
							continue;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, -1, -1 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, -1, -1 );
						if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
							continue;
						nColWidth = arrColumnWidths[ nColNo ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						if( _sizeJoin.cx != 1 )
							_sizeCell.cx = 0;
						if( _sizeJoin.cy != 1 )
							_sizeCell.cy = 0;
						_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
				} // top-left header cells
				if( rcOuterPartCounts.right > 0 )
				{ // top-right header cells
					for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
							continue;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 1, -1 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, 1, -1 );
						if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
							continue;
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						if( _sizeJoin.cx != 1 )
							_sizeCell.cx = 0;
						if( _sizeJoin.cy != 1 )
							_sizeCell.cy = 0;
						_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
				} // top-right header cells
			} // for( nRowNo = 0; nRowNo < rcOuterPartCounts.top; nRowNo ++ )
		} // if( rcOuterPartCounts.top > 0 )
		if( rcOuterPartCounts.bottom > 0 )
		{
			for( nRowNo = 0; nRowNo < rcOuterPartCounts.bottom; nRowNo ++ )
			{
				nRowHeight = 0;
				if( OnGridQueryPpvwVisibilityForRow( nRowNo, 1 ) )
				{
					for( nColNo = 0; nColNo < nColCount; nColNo ++ )
					{
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 0, 1 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, 0, 1 );
						if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
							continue;
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						if( _sizeJoin.cx != 1 )
							_sizeCell.cx = 0;
						if( _sizeJoin.cy != 1 )
							_sizeCell.cy = 0;
						_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.bottom );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo + rcOuterPartCounts.left ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // for( nColNo = 0; nColNo < nColCount; nColNo ++ )
					arrRowHeights[ nRowNo + rcOuterPartCounts.top + nRowCount ] = nRowHeight;
					rcOuterPartExtents.bottom += nRowHeight;
				} // if( OnGridQueryPpvwVisibilityForRow( nRowNo, 1 ) )
				if( rcOuterPartCounts.left > 0 )
				{ // bottom-left header cells
					for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
							continue;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, -1, 1 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, -1, 1 );
						if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
							continue;
						nColWidth = arrColumnWidths[ nColNo ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						if( _sizeJoin.cx != 1 )
							_sizeCell.cx = 0;
						if( _sizeJoin.cy != 1 )
							_sizeCell.cy = 0;
						_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
				} // bottom-left header cells
				if( rcOuterPartCounts.right > 0 )
				{ // bottom-right header cells
					for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
							continue;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 1, 1 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						CSize _sizeJoin = OnGbwCellJoinQueryInfo( nColNo, nRowNo, 1, 1 );
						if( _sizeJoin.cx != 1 && _sizeJoin.cy != 1 )
							continue;
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						if( _sizeJoin.cx != 1 )
							_sizeCell.cx = 0;
						if( _sizeJoin.cy != 1 )
							_sizeCell.cy = 0;
						_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
				} // bottom-right header cells
			} // for( nRowNo = 0; nRowNo < rcOuterPartCounts.bottom; nRowNo ++ )
		} // if( rcOuterPartCounts.bottom > 0 )
		if( rcOuterPartCounts.left > 0 )
		{
			for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
				rcOuterPartExtents.left += arrColumnWidths[ nColNo ];
		} // if( rcOuterPartCounts.left > 0 )
		if( rcOuterPartCounts.right > 0 )
		{
			for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
				rcOuterPartExtents.right += arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
		} // if( rcOuterPartCounts.right > 0 )
		m_rcPageHeaderFooterGutters = OnMeasurePageHeaderFooterGutters( *pDC );
		ASSERT( m_rcPageHeaderFooterGutters.left   >= 0 );
		ASSERT( m_rcPageHeaderFooterGutters.right  >= 0 );
		ASSERT( m_rcPageHeaderFooterGutters.top    >= 0 );
		ASSERT( m_rcPageHeaderFooterGutters.bottom >= 0 );
		INT nInnerMeasureDpiDX =
			nMeasureDpiDX
			- rcOuterPartExtents.left
			- rcOuterPartExtents.right
			- m_rcPageHeaderFooterGutters.left - m_rcPageHeaderFooterGutters.right;
		if( nInnerMeasureDpiDX <= 0 )
			throw 0;
		INT nInnerMeasureDpiDY =
			nMeasureDpiDY
			- rcOuterPartExtents.top
			- rcOuterPartExtents.bottom
			- m_rcPageHeaderFooterGutters.top - m_rcPageHeaderFooterGutters.bottom;
		if( nInnerMeasureDpiDY <= 0 )
			throw 0;
		listRowIndices.AddTail( 0L );
		nCurrentPageExtent = 0;
		for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		{
			nRowHeight = arrRowHeights[ nRowNo + rcOuterPartCounts.top ];
			nCurrentPageExtent += nRowHeight;
			if( nCurrentPageExtent > nInnerMeasureDpiDY )
			{
				nVertPageCount ++;
				if( nLastRowNo == nRowNo )
				{
					nCurrentPageExtent = 0;
					if( ( nRowNo + 1 ) < nRowCount )
					{
						listRowIndices.AddTail( nRowNo );
						listRowIndices.AddTail( nRowNo + 1 );
						nLastRowNo = nRowNo;
						continue;
					}
				}
				nCurrentPageExtent = nRowHeight;
				listRowIndices.AddTail( nRowNo - 1 );
				listRowIndices.AddTail( nRowNo );
				nLastRowNo = nRowNo;
			}
		} // for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		listRowIndices.AddTail( nRowCount - 1 );
		listColumnIndices.AddTail( 0L );
		for( nColNo = 0, nCurrentPageExtent = 0; nColNo < nColCount; nColNo ++ )
		{
			nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
			nCurrentPageExtent += nColWidth;
			if( nCurrentPageExtent > nInnerMeasureDpiDX )
			{
				nHorzPageCount ++;
				if( nLastColNo == nColNo )
				{
					nCurrentPageExtent = 0;
					if( ( nColNo + 1 ) < nColCount )
					{
						listColumnIndices.AddTail( nColNo );
						listColumnIndices.AddTail( nColNo + 1 );
						nLastColNo = nColNo;
						continue;
					}
				}
				nCurrentPageExtent = nColWidth;
				listColumnIndices.AddTail( nColNo - 1 );
				listColumnIndices.AddTail( nColNo );
				nLastColNo = nColNo;
			}
		} // for( nColNo = 0, nCurrentPageExtent = 0; nColNo < nColCount; nColNo ++ )
		listColumnIndices.AddTail( nColCount - 1 );

		nPageCount = nHorzPageCount * nVertPageCount;
		if( nPageCount > INT(USHRT_MAX) )
			throw 0;
		CRect rcVisibleRange( 0, 0, nColCount, nRowCount );
		if( ! MetafileCache_Init( nPageCount ) )
			throw 0;
		bMetafileCacheStarted = true;
		POSITION posHorz = NULL, posVert = listRowIndices.GetHeadPosition();
		for( nPageIndex = 0; nPageIndex < nPageCount; nPageIndex ++ )
		{
			if( (nPageIndex%nHorzPageCount) == 0 )
			{
				nRowNoFrom = listRowIndices.GetNext( posVert );
				nRowNoTo = listRowIndices.GetNext( posVert );
				posHorz = listColumnIndices.GetHeadPosition();
			}
			nColNoFrom = listColumnIndices.GetNext( posHorz );
			nColNoTo = listColumnIndices.GetNext( posHorz );
			CExtSafeString strMetafileName;
			if( MetafileCache_DiskModeGet() )
				strMetafileName = MetafileCache_GetUniqueName( nPageIndex );
			CMetaFileDC dcMetafile;
			if( m_bUseEnhancedMetafiles )
			{
				if( ! dcMetafile.CreateEnhanced(
						pDC, // NULL,
						( ! strMetafileName.IsEmpty() )
							? LPCTSTR(strMetafileName)
							: NULL
							,
						rcEmfExtent,
						NULL
						)
					)
				{
					ASSERT( FALSE );
					throw 0;
				}
			}
			else
			{
				if( ! dcMetafile.Create(
						( ! strMetafileName.IsEmpty() )
							? LPCTSTR(strMetafileName)
							: NULL
						)
					)
				{
					ASSERT( FALSE );
					throw 0;
				}
			}
			dcMetafile.SetAttribDC( pDC->GetSafeHdc() );
			dcMetafile.SetBkMode( TRANSPARENT );
			dcMetafile.SaveDC();
			OnPrepareDC( &dcMetafile, pInfo );
			nRowOffset = rcOuterPartExtents.top + m_rcPageHeaderFooterGutters.top;
			INT nMaxColOffset = 0;
			bool bProgressAllowsToContinue = false;
			for( nRowNo = nRowNoFrom; nRowNo <= nRowNoTo; nRowNo ++ )
			{
				bProgressAllowsToContinue = 
					_Internal_Notify_OnPreparePrinting_Progress(
						1,              // nPassNo
						2,              // nPassCount
						nPageIndex,     // nPageNo
						nPageCount,     // nPageCount
						nHorzPageCount, // nContentWidthInPages
						nRowNo,         // nDocumentItemNo
						nDocItemCount,  // nDocumentItemCount
						nRowNoFrom,     // nInPageDocumentItemFirstNo
						nRowNoTo        // nInPageDocumentItemLastNo
						);
				if( ! bProgressAllowsToContinue )
					break;
				nRowHeight = arrRowHeights[ nRowNo + rcOuterPartCounts.top ];
				nColOffset = m_rcPageHeaderFooterGutters.left;
				if( rcOuterPartCounts.left > 0 )
				{
					for( nColNo = 0L; nColNo < rcOuterPartCounts.left; nColNo ++ )
					{
						if(	! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
							continue;
						CExtGridCell * pCell = ItemGetCell( arrPrintableItems[nRowNo], nColNo, -1, 0 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo ];
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + nColWidth,
							nRowOffset + nRowHeight
							);
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_LEFT;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCellAtCenter(
							pInfo,
							rcOuterPartCounts,
							arrPrintableItems,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							//nRowCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							-1,
							//0,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
						nColOffset += nColWidth;
					} // for( nColNo = 0L; nColNo < rcOuterPartCounts.left; nColNo ++ )
				} // if( rcOuterPartCounts.left > 0 )
				if( nRowNo == nRowNoFrom )
					nMaxColOffset = nColOffset;
				for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
				{
					if(	! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
						continue;
					CExtGridCell * pCell = ItemGetCell( arrPrintableItems[nRowNo], nColNo );
					if( pCell == NULL )
						continue;
					ASSERT_VALID( pCell );
					nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
					CRect rcCell(
						nColOffset,
						nRowOffset,
						nColOffset + nColWidth,
						nRowOffset + nRowHeight
						);
					if( OnTreeGridQueryColumnOutline( nColNo ) )
						rcCell.left += OnCalcPrintableTreeItemIndent( *pDC, arrPrintableItems[nRowNo] );
					CRect rcCellExtra = rcCell;
					DWORD dwAreaFlags = __EGBWA_INNER_CELLS;
					DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
					OnPreparePrinting_RenderSingleCellAtCenter(
						pInfo,
						rcOuterPartCounts,
						arrPrintableItems,
						arrColumnWidths,
						arrRowHeights,
						nColCount,
						//nRowCount,
						nPageIndex,
						nColNoFrom,
						nColNoTo,
						nRowNoFrom,
						nRowNoTo,
						pCell,
						dcMetafile,
						nColNo,
						nRowNo,
						0,
						//0,
						rcCellExtra,
						rcCell,
						rcVisibleRange,
						dwAreaFlags,
						dwHelperPaintFlags
						);
					nColOffset += nColWidth;
				} // for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
				if( rcOuterPartCounts.right > 0 )
				{
					for( nColNo = 0L; nColNo < rcOuterPartCounts.right; nColNo ++ )
					{
						if(	! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
							continue;
						CExtGridCell * pCell = ItemGetCell( arrPrintableItems[nRowNo], nColNo, 1, 0 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + nColWidth,
							nRowOffset + nRowHeight
							);
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_RIGHT;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCellAtCenter(
							pInfo,
							rcOuterPartCounts,
							arrPrintableItems,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							//nRowCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							1,
							//0,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
						nColOffset += nColWidth;
					} // for( nColNo = 0L; nColNo < rcOuterPartCounts.right; nColNo ++ )
				} // if( rcOuterPartCounts.right > 0 )
				nRowOffset += nRowHeight;
			} // for( nRowNo = nRowNoFrom; nRowNo <= nRowNoTo; nRowNo ++ )
			if( ! bProgressAllowsToContinue )
				break;

			INT nRowOffsetSaved = nRowOffset;
			nRowOffset = m_rcPageHeaderFooterGutters.top;
			if( rcOuterPartCounts.top > 0 )
			{
				for( nRowNo = 0L; nRowNo < rcOuterPartCounts.top; nRowNo ++ )
				{
					if(	! OnGridQueryPpvwVisibilityForRow( nRowNo, 1 ) )
						continue;
					nRowHeight = arrRowHeights[ nRowNo ];
					if( rcOuterPartCounts.left > 0 )
					{ // top-left header cells
						nColOffset = m_rcPageHeaderFooterGutters.left;
						for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
								continue;
							INT nReviewColType = -1, nReviewRowType = -1;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo ];
							CRect rcCell(
								nColOffset,
								nRowOffset,
								nColOffset + nColWidth,
								nRowOffset + nRowHeight
								);
							CRect rcCellExtra = rcCell;
							DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_LEFT|__EGBWA_OUTER_TOP;
							DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
							OnPreparePrinting_RenderSingleCellAtTopOrBottom(
								pInfo,
								rcOuterPartCounts,
								arrColumnWidths,
								arrRowHeights,
								nColCount,
								nRowCount,
								nPageIndex,
								nColNoFrom,
								nColNoTo,
								nRowNoFrom,
								nRowNoTo,
								pCell,
								dcMetafile,
								nColNo,
								nRowNo,
								nReviewColType,
								nReviewRowType,
								rcCellExtra,
								rcCell,
								rcVisibleRange,
								dwAreaFlags,
								dwHelperPaintFlags
								);
							nColOffset += nColWidth;
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
					} // top-left header cells
					nColOffset = nMaxColOffset;
					for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					{
						if(	! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
							continue;
						INT nReviewColType = 0, nReviewRowType = -1;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + nColWidth,
							nRowOffset + nRowHeight
							);
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_TOP;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCellAtTopOrBottom(
							pInfo,
							rcOuterPartCounts,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							nRowCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							nReviewColType,
							nReviewRowType,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
						nColOffset += nColWidth;
					} // for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					if( rcOuterPartCounts.right > 0 )
					{ // top-right header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
								continue;
							INT nReviewColType = 1, nReviewRowType = -1;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
							CRect rcCell(
								nColOffset,
								nRowOffset,
								nColOffset + nColWidth,
								nRowOffset + nRowHeight
								);
							CRect rcCellExtra = rcCell;
							DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_RIGHT|__EGBWA_OUTER_TOP;
							DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
							OnPreparePrinting_RenderSingleCellAtTopOrBottom(
								pInfo,
								rcOuterPartCounts,
								arrColumnWidths,
								arrRowHeights,
								nColCount,
								nRowCount,
								nPageIndex,
								nColNoFrom,
								nColNoTo,
								nRowNoFrom,
								nRowNoTo,
								pCell,
								dcMetafile,
								nColNo,
								nRowNo,
								nReviewColType,
								nReviewRowType,
								rcCellExtra,
								rcCell,
								rcVisibleRange,
								dwAreaFlags,
								dwHelperPaintFlags
								);
							nColOffset += nColWidth;
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
					} // top-right header cells
					nRowOffset += nRowHeight;
				} // for( nRowNo = 0L; nRowNo < rcOuterPartCounts.top; nRowNo ++ )
			} // if( rcOuterPartCounts.top > 0 )
			nRowOffset = nRowOffsetSaved;
			if( rcOuterPartCounts.bottom > 0 )
			{
				for( nRowNo = 0L; nRowNo < rcOuterPartCounts.bottom; nRowNo ++ )
				{
					if(	! OnGridQueryPpvwVisibilityForRow( nRowNo, -1 ) )
						continue;
					nRowHeight = arrRowHeights[ nRowNo + rcOuterPartCounts.top + nRowCount ];
					if( rcOuterPartCounts.left > 0 )
					{ // bottom-left header cells
						nColOffset = m_rcPageHeaderFooterGutters.left;
						for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
								continue;
							INT nReviewColType = -1, nReviewRowType = 1;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo ];
							CRect rcCell(
								nColOffset,
								nRowOffset,
								nColOffset + nColWidth,
								nRowOffset + nRowHeight
								);
							CRect rcCellExtra = rcCell;
							DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_LEFT|__EGBWA_OUTER_BOTTOM;
							DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
							OnPreparePrinting_RenderSingleCellAtTopOrBottom(
								pInfo,
								rcOuterPartCounts,
								arrColumnWidths,
								arrRowHeights,
								nColCount,
								nRowCount,
								nPageIndex,
								nColNoFrom,
								nColNoTo,
								nRowNoFrom,
								nRowNoTo,
								pCell,
								dcMetafile,
								nColNo,
								nRowNo,
								nReviewColType,
								nReviewRowType,
								rcCellExtra,
								rcCell,
								rcVisibleRange,
								dwAreaFlags,
								dwHelperPaintFlags
								);
							nColOffset += nColWidth;
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
					} // bottom-left header cells
					nColOffset = nMaxColOffset;
					for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					{
						if(	! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
							continue;
						INT nReviewColType = 0, nReviewRowType = 1;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + nColWidth,
							nRowOffset + nRowHeight
							);
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_BOTTOM;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCellAtTopOrBottom(
							pInfo,
							rcOuterPartCounts,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							nRowCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							nReviewColType,
							nReviewRowType,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
						nColOffset += nColWidth;
					} // for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					if( rcOuterPartCounts.right > 0 )
					{ // bottom-right header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
								continue;
							INT nReviewColType = 1, nReviewRowType = 1;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, nReviewColType, nReviewRowType );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
							CRect rcCell(
								nColOffset,
								nRowOffset,
								nColOffset + nColWidth,
								nRowOffset + nRowHeight
								);
							CRect rcCellExtra = rcCell;
							DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_RIGHT|__EGBWA_OUTER_BOTTOM;
							DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
							OnPreparePrinting_RenderSingleCellAtTopOrBottom(
								pInfo,
								rcOuterPartCounts,
								arrColumnWidths,
								arrRowHeights,
								nColCount,
								nRowCount,
								nPageIndex,
								nColNoFrom,
								nColNoTo,
								nRowNoFrom,
								nRowNoTo,
								pCell,
								dcMetafile,
								nColNo,
								nRowNo,
								nReviewColType,
								nReviewRowType,
								rcCellExtra,
								rcCell,
								rcVisibleRange,
								dwAreaFlags,
								dwHelperPaintFlags
								);
							nColOffset += nColWidth;
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
					} // bottom-right header cells
					nRowOffset += nRowHeight;
				} // for( nRowNo = 0L; nRowNo < rcOuterPartCounts.bottom; nRowNo ++ )
			} // if( rcOuterPartCounts.bottom > 0 )

			CRect rcEntirePage( 0, 0, nMeasureDpiDX, nMeasureDpiDY );
			OnDrawPageHeaderFooterGutters( dcMetafile, rcEntirePage, m_rcPageHeaderFooterGutters, nPageIndex, nPageCount, pInfo);

			dcMetafile.RestoreDC( -1 );
			if( ! bProgressAllowsToContinue )
				throw 0;
			HANDLE hMetafile =
				m_bUseEnhancedMetafiles
					? ( (HANDLE) dcMetafile.CloseEnhanced() )
					: ( (HANDLE) dcMetafile.Close() )
					;
			if( ! MetafileCache_InitAt(
					nPageIndex,
					hMetafile,
					( ! strMetafileName.IsEmpty() )
						? LPCTSTR(strMetafileName)
						: NULL
					)
				)
				throw 0;
		} // for( nPageIndex = 0; nPageIndex < nPageCount; nPageIndex ++ )
		WORD nPageNoMin = WORD(1), nPageNoMax = WORD( WORD(nPageCount) + WORD(nPageNoMin) - WORD(1) );
		pInfo->m_nNumPreviewPages = nPageNoMax;
		pInfo->m_pPD->m_pd.nMinPage = nPageNoMin;
		pInfo->m_pPD->m_pd.nFromPage = nPageNoMin;
		pInfo->m_pPD->m_pd.nMaxPage = nPageNoMax;
		pInfo->m_pPD->m_pd.nToPage = nPageNoMax;
		bOK = true;
	} // try
	catch( CException * pException )
	{
		pException->Delete();
	} // catch( CException * pException )
	catch( ... )
	{
	} // catch( ... )
	pDC->RestoreDC( -1 );
	if( bMetafileCacheStarted )
		MetafileCache_InitComplete( bOK );
	OnPreparePrinting_End( ! bOK );
	_WaitCursor;
	if( ! bOK )
		return FALSE;
	else
		return TRUE;
}

#endif // (!defined __EXT_MFC_NO_TREEGRIDWND)

#if (!defined __EXT_MFC_NO_REPORTGRIDWND)

/////////////////////////////////////////////////////////////////////////////
// template < > class CExtPPVW < CExtReportGridWnd >

void CExtPPVW < CExtReportGridWnd > :: OnPrepareDC(
	CDC * pDC,
	CPrintInfo * pInfo // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC );
	ASSERT( pDC->GetSafeHdc() != NULL );
	pInfo;

CFont * pFont = & OnSiwGetDefaultFont();
 	if( pFont->GetSafeHandle() == NULL )
 		return;
	pDC->SelectObject( pFont );
}

bool CExtPPVW < CExtReportGridWnd > :: OnCalcPrintableTreeItemArrayImpl(
	HTREEITEM htiParent,
	CList < HTREEITEM, HTREEITEM & > & listPrintableItems
	)
{
	ASSERT_VALID( this );
	ASSERT( htiParent != NULL );
	if( ! OnTreeGridQueryPpvwVisibilityForItem( CExtTreeGridCellNode::FromHTREEITEM(htiParent) ) )
		return false;
LONG nIndex, nCount = ItemGetChildCount( htiParent );
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		HTREEITEM hti = ItemGetChildAt( htiParent, nIndex );
		ASSERT( hti != NULL );
		if( CExtTreeGridCellNode::FromHTREEITEM( hti )->TreeNodeHiddenGet() )
			continue;
		listPrintableItems.AddTail( hti );
		POSITION pos = listPrintableItems.GetTailPosition();
		ASSERT( pos != NULL );
		if( ! OnCalcPrintableTreeItemArrayImpl( hti, listPrintableItems ) )
			listPrintableItems.RemoveAt( pos );
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return true;
}

INT CExtPPVW < CExtReportGridWnd > :: OnCalcPrintableTreeItemIndent(
	CDC & dcMeasure,
	HTREEITEM hti
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&dcMeasure) );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	ASSERT( hti != NULL );
	dcMeasure;
	if( ItemGetChildCount( hti ) == 0L )
		return 0;
CExtTreeGridCellNode * pNode = (CExtTreeGridCellNode *)hti;
	ASSERT_VALID( pNode );
	ASSERT_KINDOF( CExtTreeGridCellNode, pNode );
INT nIndent = 0;
	for( ; pNode != NULL; )
	{
		CExtTreeGridCellNode * pNodeParent = pNode->TreeNodeGetParent();
		if( pNodeParent == NULL )
			break;
		INT nOneLevelIndent = INT( pNode->TreeNodeIndentPxGet() );
		if( nOneLevelIndent < 0 )
			nOneLevelIndent = GetTreeData().m_nIndentPxDefault;
		nIndent += nOneLevelIndent;
		pNode = pNodeParent;
	}
	return nIndent;
}

void CExtPPVW < CExtReportGridWnd > :: OnCalcPrintableTreeItemArray(
	CArray < HTREEITEM, HTREEITEM & > & arrPrintableItems
	)
{
	ASSERT_VALID( this );
	arrPrintableItems.RemoveAll();
CList < HTREEITEM, HTREEITEM & > listPrintableItems;
HTREEITEM htiRoot = ItemGetRoot();
	if( htiRoot == NULL )
		return;
	OnCalcPrintableTreeItemArrayImpl( htiRoot, listPrintableItems );
LONG nIndex, nCount = LONG( listPrintableItems.GetCount() );
	if( nCount == 0 )
		return;
	arrPrintableItems.SetSize( nCount );
POSITION pos = listPrintableItems.GetHeadPosition();
	for( nIndex = 0; pos != NULL; nIndex ++ )
	{
		HTREEITEM hti = listPrintableItems.GetNext( pos );
		ASSERT( hti != NULL );
		arrPrintableItems.SetAt( nIndex, hti );
	} // for( nIndex = 0; pos != NULL; nIndex ++ )
	listPrintableItems.RemoveAll();
}

bool CExtPPVW < CExtReportGridWnd > :: IsPrintPreviewAvailable() const
{
	ASSERT_VALID( this );
	if( ! CExtPPVW_Printable::IsPrintPreviewAvailable() )
		return false;
	if( GetSafeHwnd() == NULL )
		return false;
bool bExternalData = ExternalDataGet();
	if( bExternalData )
		return false;
DWORD dwScrollTypeH = SiwScrollTypeHGet();
	if( dwScrollTypeH == __ESIW_ST_VIRTUAL )
		return false;
DWORD dwScrollTypeV = SiwScrollTypeVGet();
	if( dwScrollTypeV == __ESIW_ST_VIRTUAL )
		return false;
LONG nColCount = ColumnCountGet();
	if( nColCount <= 0 )
		return false;
LONG nRowCount = RowCountGet();
	if( nRowCount <= 0 )
		return false;
	return true;
}

void CExtPPVW < CExtReportGridWnd > :: OnPreparePrinting_RenderSingleCellAtTopOrBottom(
	CPrintInfo * pInfo,
	const CRect & rcOuterPartCounts,
	const CArray < INT, INT & > & arrColumnWidths,
	const CArray < INT, INT & > & arrRowHeights,
	LONG nColCount,
	LONG nRowCount,
	INT nPageIndex,
	LONG nColNoFrom,
	LONG nColNoTo,
	LONG nRowNoFrom,
	LONG nRowNoTo,
	CExtGridCell * pCell,
	CDC & dc,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	CRect & rcCellExtra,
	CRect & rcCell,
	CRect & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT_VALID( pCell );
	ASSERT( nRowType != 0 );
	pInfo;
	rcOuterPartCounts;
	nPageIndex;
	nColNoFrom;
	nColNoTo;
	nRowNoFrom;
	nRowNoTo;
	nColCount;
	nRowCount;
	arrColumnWidths;
	arrRowHeights;
	if( m_bDrawSimpleWhiteBackground )
	{
		CRect rcLines = rcCell;
		rcLines.right ++;
		rcLines.bottom ++;
		HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(WHITE_BRUSH) );
		HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_PEN) );
		dc.Rectangle( &rcLines );
		::SelectObject( dc.m_hDC, hOldPen );
		::SelectObject( dc.m_hDC, hOldBrush );
	} // if( m_bDrawSimpleWhiteBackground )
	pCell->OnPaintBackground(
		*this,
		dc,
		nColNo,
		nRowNo,
		nColNo,
		nRowNo,
		nColType,
		nRowType,
		rcCellExtra,
		rcCell,
		rcVisibleRange,
		dwAreaFlags,
		dwHelperPaintFlags
		);
	pCell->OnPaintForeground(
		*this,
		dc,
		nColNo,
		nRowNo,
		nColNo,
		nRowNo,
		nColType,
		nRowType,
		rcCellExtra,
		rcCell,
		rcVisibleRange,
		dwAreaFlags,
		dwHelperPaintFlags
		);
	if( m_bDrawSimpleBlackBorders )
	{
		CRect rcLines = rcCell;
		rcLines.right ++;
		rcLines.bottom ++;
		HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_BRUSH) );
		HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(BLACK_PEN) );
		dc.Rectangle( &rcLines );
		::SelectObject( dc.m_hDC, hOldPen );
		::SelectObject( dc.m_hDC, hOldBrush );
	} // if( m_bDrawSimpleBlackBorders )
}

void CExtPPVW < CExtReportGridWnd > :: OnPreparePrinting_RenderGroupRow(
	CPrintInfo * pInfo,
	const CArray < HTREEITEM, HTREEITEM & > & arrPrintableItems,
	const CArray < INT, INT & > & arrColumnWidths,
	const CArray < INT, INT & > & arrRowHeights,
	LONG nColCount,
	INT nPageIndex,
	LONG nColNoFrom,
	LONG nColNoTo,
	LONG nRowNoFrom,
	LONG nRowNoTo,
	CExtReportGridItem * pCell,
	CDC & dc,
	LONG nRowNo,
	CRect & rcCell,
	CRect & rcVisibleRange,
	const CExtSafeString & strRowText,
	CFont * pRgiFont
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT_VALID( pCell );
	ASSERT( pRgiFont->GetSafeHandle() != NULL );
	pInfo;
	nPageIndex;
	nColNoFrom;
	nColNoTo;
	nRowNoFrom;
	nRowNoTo;
	nColCount;
	arrPrintableItems;
	arrColumnWidths;
	arrRowHeights;
	rcVisibleRange;
	nRowNo;
	pCell;
CFont * pOldFont = dc.SelectObject( pRgiFont );
int nOldBkMode = dc.SetBkMode( TRANSPARENT );
COLORREF clrOldTextColor = dc.SetTextColor( RGB(0,0,0) );
	CExtRichContentLayout::stat_DrawText(
		dc.m_hDC,
		LPCTSTR(strRowText), INT( _tcslen( LPCTSTR(strRowText) ) ),
		&rcCell,
		DT_SINGLELINE|DT_LEFT|DT_TOP|DT_NOCLIP, 0
		);
	dc.SetTextColor( clrOldTextColor );
	dc.SetBkMode( nOldBkMode );
	dc.SelectObject( pOldFont );

	if( m_bDrawSimpleBlackOutline )
	{
		CDC * pDC = CDC::FromHandle( pInfo->m_pPD->m_pd.hDC );
		ASSERT_VALID( pDC );
		INT nOutlineExtent = OnCalcPrintableTreeItemIndent( *pDC, arrPrintableItems[nRowNo] );
		if( nOutlineExtent > 0 )
		{
			HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_BRUSH) );
			HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(BLACK_PEN) );
			COLORREF clrDots = RGB( 0, 0, 0 );
			CRect rcIndent(
				rcCell.left, // - nOutlineExtent,
				rcCell.top,
				rcCell.left,
				rcCell.bottom
				);
			HTREEITEM hti = (HTREEITEM) arrPrintableItems[ nRowNo ];
			ASSERT( hti != NULL );
			HTREEITEM hRootItem = ItemGetRoot();
			ASSERT( hRootItem != NULL );
			HTREEITEM hTreeItem = hti;
			for( ; hTreeItem != hRootItem; hTreeItem = ItemGetParent(hTreeItem) )
			{
				ASSERT( hTreeItem != NULL );
				CExtTreeGridCellNode * pNode =
					CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
				ASSERT_VALID( pNode );
				INT nPxIndent = (INT)pNode->TreeNodeIndentPxGet();
				if( nPxIndent == ULONG(-1L) )
					nPxIndent = GetTreeData().m_nIndentPxDefault;
				rcIndent.right = rcIndent.left;
				rcIndent.left -= nPxIndent;
				INT nMetricH = rcIndent.Width();
				INT nMetricH2 = nMetricH / 2;
				INT nMetricV = rcIndent.Height();
				INT nMetricV2 = min( nMetricH, nMetricV );
				nMetricV2 /= 2;
				bool bDrawUpperLineV = false;
				if( hti == hTreeItem )
				{
					if( m_bDrawSimpleBlackOutlineUsingDots )
						CExtPaintManager::stat_DrawDotLineH(
							dc,
							rcIndent.left + nMetricH2,
							rcIndent.right,
							rcIndent.top + nMetricV2,
							clrDots
							);
					else
					{
						dc.MoveTo(
							rcIndent.left + nMetricH2,
							rcIndent.top + nMetricV2
							);
						dc.LineTo(
							rcIndent.right,
							rcIndent.top + nMetricV2
							);
					}
					bDrawUpperLineV = true;
				}
				HTREEITEM hTreeItemNext = _ItemBrowseNextImpl( hTreeItem, true, false, false, false );
				if( hTreeItemNext != NULL )
				{
					if( m_bDrawSimpleBlackOutlineUsingDots )
						CExtPaintManager::stat_DrawDotLineV(
							dc,
							rcIndent.left + nMetricH2,
							rcIndent.top + nMetricV2,
							rcIndent.bottom,
							clrDots
							);
					else
					{
						dc.MoveTo(
							rcIndent.left + nMetricH2,
							rcIndent.top + nMetricV2
							);
						dc.LineTo(
							rcIndent.left + nMetricH2,
							rcIndent.bottom
							);
					}
					bDrawUpperLineV = true;
				}
				if( bDrawUpperLineV )
				{
					if( m_bDrawSimpleBlackOutlineUsingDots )
						CExtPaintManager::stat_DrawDotLineV(
							dc,
							rcIndent.left + nMetricH2,
							rcIndent.top,
							rcIndent.top + nMetricV2,
							clrDots
							);
					else
					{
						dc.MoveTo(
							rcIndent.left + nMetricH2,
							rcIndent.top
							);
						dc.LineTo(
							rcIndent.left + nMetricH2,
							rcIndent.top + nMetricV2
							);
					}
				}
				//if( hti == hTreeItem )
				//{
				//	CExtTreeGridCellNode::e_expand_box_shape_t eEBS =
				//		pNode->TreeNodeGetExpandBoxShape();
				//	if( eEBS != CExtTreeGridCellNode::__EEBS_NONE )
				//	{
				//		CRect rcIndentPaint = rcIndent;
				//		if(		rcIndentPaint.left == rcIndentPaint.right
				//			&&	m_bAdustNullIndent
				//			)
				//		{
				//			INT nPxIndentPaint = nPxIndent;
				//			if( nPxIndentPaint == 0 )
				//				nPxIndentPaint = GetTreeData().m_nIndentPxDefault;
				//			rcIndentPaint.left -= nPxIndentPaint;
				//		}
				//		if( rcIndentPaint.left < rcIndentPaint.right )
				//			OnTreeGridPaintExpandButton(
				//				dc,
				//				hTreeItem,
				//				( eEBS == CExtTreeGridCellNode::__EEBS_EXPANDED )
				//					? true : false,
				//				rcIndentPaint
				//				);
				//	}
				//} // if( hti == hTreeItem )
			} // for( ; hTreeItem != hRootItem; hTreeItem = ItemGetParent(hTreeItem) )
			::SelectObject( dc.m_hDC, hOldPen );
			::SelectObject( dc.m_hDC, hOldBrush );
		} // if( nOutlineExtent > 0 )
	} // if( m_bDrawSimpleBlackOutline )
}

void CExtPPVW < CExtReportGridWnd > :: OnPreparePrinting_RenderSingleCellAtCenter(
	CPrintInfo * pInfo,
	const CRect & rcOuterPartCounts,
	const CArray < HTREEITEM, HTREEITEM & > & arrPrintableItems,
	const CArray < INT, INT & > & arrColumnWidths,
	const CArray < INT, INT & > & arrRowHeights,
	LONG nColCount,
	//LONG nRowCount,
	INT nPageIndex,
	LONG nColNoFrom,
	LONG nColNoTo,
	LONG nRowNoFrom,
	LONG nRowNoTo,
	CExtGridCell * pCell,
	CDC & dc,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	//INT nRowType,
	CRect & rcCellExtra,
	CRect & rcCell,
	CRect & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT_VALID( pCell );
	pInfo;
	rcOuterPartCounts;
	nPageIndex;
	nColNoFrom;
	nColNoTo;
	nRowNoFrom;
	nRowNoTo;
	nColCount;
	arrPrintableItems;
	arrColumnWidths;
	arrRowHeights;
	if( m_bDrawSimpleWhiteBackground )
	{
		CRect rcLines = rcCell;
		rcLines.right ++;
		rcLines.bottom ++;
		HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(WHITE_BRUSH) );
		HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_PEN) );
		dc.Rectangle( &rcLines );
		::SelectObject( dc.m_hDC, hOldPen );
		::SelectObject( dc.m_hDC, hOldBrush );
	} // if( m_bDrawSimpleWhiteBackground )
	pCell->OnPaintBackground(
		*this,
		dc,
		nColNo,
		nRowNo,
		nColNo,
		nRowNo,
		nColType,
		0,
		rcCellExtra,
		rcCell,
		rcVisibleRange,
		dwAreaFlags,
		dwHelperPaintFlags
		);
	pCell->OnPaintForeground(
		*this,
		dc,
		nColNo,
		nRowNo,
		nColNo,
		nRowNo,
		nColType,
		0,
		rcCellExtra,
		rcCell,
		rcVisibleRange,
		dwAreaFlags,
		dwHelperPaintFlags
		);
	if( m_bDrawSimpleBlackBorders )
	{
		CRect rcLines = rcCell;
		rcLines.right ++;
		rcLines.bottom ++;
		HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_BRUSH) );
		HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(BLACK_PEN) );
		dc.Rectangle( &rcLines );
		::SelectObject( dc.m_hDC, hOldPen );
		::SelectObject( dc.m_hDC, hOldBrush );
	} // if( m_bDrawSimpleBlackBorders )
}

void CExtPPVW < CExtReportGridWnd > :: OnPreparePrinting_RenderAutoPreview(
	CPrintInfo * pInfo,
	const CRect & rcOuterPartCounts,
	const CArray < HTREEITEM, HTREEITEM & > & arrPrintableItems,
	const CArray < INT, INT & > & arrColumnWidths,
	const CArray < INT, INT & > & arrRowHeights,
	LONG nColCount,
	//LONG nRowCount,
	INT nPageIndex,
	LONG nColNoFrom,
	LONG nColNoTo,
	LONG nRowNoFrom,
	LONG nRowNoTo,
	CExtGridCell * pCell,
	CDC & dc,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	//INT nRowType,
	CRect & rcCellExtra,
	CRect & rcCell,
	CRect & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT_VALID( pCell );
	pInfo;
	rcOuterPartCounts;
	nPageIndex;
	nColNoFrom;
	nColNoTo;
	nRowNoFrom;
	nRowNoTo;
	nColCount;
	arrPrintableItems;
	arrColumnWidths;
	arrRowHeights;

// { // BLOCK: begin (test custom background with lines)
// CRect rcLines = rcCell;
// rcLines.right ++;
// rcLines.bottom ++;
// HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(WHITE_BRUSH) );
// HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_PEN) );
// dc.Rectangle( &rcLines );
// ::SelectObject( dc.m_hDC, hOldPen );
// ::SelectObject( dc.m_hDC, hOldBrush );
// } // BLOCK: end (test custom background with lines)

	pCell->OnPaintBackground(
		*this,
		dc,
		nColNo,
		nRowNo,
		nColNo,
		nRowNo,
		nColType,
		0,
		rcCellExtra,
		rcCell,
		rcVisibleRange,
		dwAreaFlags,
		dwHelperPaintFlags
		);
	pCell->OnPaintForeground(
		*this,
		dc,
		nColNo,
		nRowNo,
		nColNo,
		nRowNo,
		nColType,
		0,
		rcCellExtra,
		rcCell,
		rcVisibleRange,
		dwAreaFlags,
		dwHelperPaintFlags
		);

// { // BLOCK: begin (test custom background with lines)
// CRect rcLines = rcCell;
// rcLines.right ++;
// rcLines.bottom ++;
// HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_BRUSH) );
// HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(BLACK_PEN) );
// dc.Rectangle( &rcLines );
// ::SelectObject( dc.m_hDC, hOldPen );
// ::SelectObject( dc.m_hDC, hOldBrush );
// } // BLOCK: end (test custom background with lines)

}

BOOL CExtPPVW < CExtReportGridWnd > :: OnPreparePrinting(
	CPrintInfo * pInfo
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return FALSE;

bool bExternalData = ExternalDataGet();
	if( bExternalData )
		return FALSE;
DWORD dwScrollTypeH = SiwScrollTypeHGet();
	if( dwScrollTypeH == __ESIW_ST_VIRTUAL )
		return FALSE;
DWORD dwScrollTypeV = SiwScrollTypeVGet();
	if( dwScrollTypeV == __ESIW_ST_VIRTUAL )
		return FALSE;

LONG nColCount = ColumnCountGet();
	if( nColCount <= 0 )
		return FALSE;
CArray < HTREEITEM, HTREEITEM & > arrPrintableItems;
	OnCalcPrintableTreeItemArray( arrPrintableItems );
LONG nRowCount = LONG( arrPrintableItems.GetSize() );
	if( nRowCount <= 0 )
		return FALSE;
	if( ! CExtPPVW_Printable::OnPreparePrinting( pInfo ) )
		return FALSE;
CWaitCursor _WaitCursor;
bool bAutoPreviewPainting = ReportAutoPreviewModeGet();
bool bBoldGroups = ( (ReportGridGetStyle()&__ERGS_BOLD_GROUPS) != 0 ) ? true : false;
CExtPaintManager * pPM = PmBridge_GetPM();
CFont * pRgiFont =
		bBoldGroups
			? (&(pPM->m_FontBold))
			: (&(pPM->m_FontNormal))
			;
	ASSERT( pInfo->m_pPD->m_pd.hDC != NULL );
CDC * pDC = CDC::FromHandle( pInfo->m_pPD->m_pd.hDC );
	ASSERT_VALID( pDC );
INT nPrinterDpiX = ::GetDeviceCaps( pDC->m_hDC, LOGPIXELSX );
INT nPrinterDpiY = ::GetDeviceCaps( pDC->m_hDC, LOGPIXELSY );
INT nPrinterDpiDX = ::GetDeviceCaps( pDC->m_hDC, HORZRES );
INT nPrinterDpiDY = ::GetDeviceCaps( pDC->m_hDC, VERTRES );
INT nMeasureDpiDX = ::MulDiv( nPrinterDpiDX, g_PaintManager.m_nLPX, nPrinterDpiX );
INT nMeasureDpiDY = ::MulDiv( nPrinterDpiDY, g_PaintManager.m_nLPY, nPrinterDpiY );
// CSize _sizePrinterOffset(
// 		pDC->GetDeviceCaps(PHYSICALOFFSETX),
// 		pDC->GetDeviceCaps(PHYSICALOFFSETY)
// 		);
// 	nMeasureDpiDX -= ::MulDiv( _sizePrinterOffset.cx * 2, g_PaintManager.m_nLPX, nPrinterDpiX );
// 	nMeasureDpiDY -= ::MulDiv( _sizePrinterOffset.cy * 2, g_PaintManager.m_nLPY, nPrinterDpiY );

CSize sizePMLT( m_rcPageMarginsHM.left, m_rcPageMarginsHM.top ), sizePMRB( m_rcPageMarginsHM.right, m_rcPageMarginsHM.bottom ); 
	pDC->HIMETRICtoDP( &sizePMLT );
	pDC->HIMETRICtoDP( &sizePMRB );
	sizePMLT.cx = ::MulDiv( sizePMLT.cx, g_PaintManager.m_nLPX, nPrinterDpiX );
	sizePMRB.cx = ::MulDiv( sizePMRB.cx, g_PaintManager.m_nLPX, nPrinterDpiX );
	sizePMLT.cy = ::MulDiv( sizePMLT.cy, g_PaintManager.m_nLPY, nPrinterDpiY );
	sizePMRB.cy = ::MulDiv( sizePMRB.cy, g_PaintManager.m_nLPY, nPrinterDpiY );
	nMeasureDpiDX -= sizePMLT.cx + sizePMRB.cx;
	nMeasureDpiDY -= sizePMLT.cy + sizePMRB.cy;

// 	pDC->DPtoLP( &_sizePrinterOffset );
CSize _sizePrinterHIMETRIC( nPrinterDpiDX, nPrinterDpiDY );
// 	_sizePrinterHIMETRIC -= _sizePrinterOffset;
// 	_sizePrinterHIMETRIC -= _sizePrinterOffset;
	pDC->DPtoHIMETRIC( &_sizePrinterHIMETRIC );
	//_sizePrinterHIMETRIC.cx -= m_rcPageMarginsHM.left + m_rcPageMarginsHM.right;
	//_sizePrinterHIMETRIC.cy -= m_rcPageMarginsHM.top + m_rcPageMarginsHM.bottom;
CRect rcEmfExtent( 0, 0, _sizePrinterHIMETRIC.cx, _sizePrinterHIMETRIC.cy );
DWORD dwHelperPaintFlagsBasic = m_dwBasicPaintFlags;
	if( m_pWndPP != NULL )
		dwHelperPaintFlagsBasic |= __EGCPF_PRINT_PREVIEW;
	else
		dwHelperPaintFlagsBasic |= __EGCPF_PRINTER;
	pDC->SaveDC();
	OnPrepareDC( pDC );
bool bOK = false, bMetafileCacheStarted = false;
	try
	{
		OnPreparePrinting_Begin();
		CRect rcOuterPartCounts(
			OuterColumnCountLeftGet(),
			OuterRowCountTopGet(),
			OuterColumnCountRightGet(),
			OuterRowCountBottomGet()
			);
		CRect rcOuterPartExtents( 0, 0, 0, 0 ),
			rcOuterMinExtents(
				g_PaintManager->UiScalingDo( __EXT_PRINT_OUTER_MIN_EXTENTS_LEFT,	CExtPaintManager::__EUIST_X ),
				g_PaintManager->UiScalingDo( __EXT_PRINT_OUTER_MIN_EXTENTS_TOP,		CExtPaintManager::__EUIST_Y ),
				g_PaintManager->UiScalingDo( __EXT_PRINT_OUTER_MIN_EXTENTS_RIGHT,	CExtPaintManager::__EUIST_X ),
				g_PaintManager->UiScalingDo( __EXT_PRINT_OUTER_MIN_EXTENTS_BOTTOM,	CExtPaintManager::__EUIST_Y )
				);
		ASSERT( rcOuterPartCounts.left >= 0 );
		ASSERT( rcOuterPartCounts.top >= 0 );
		ASSERT( rcOuterPartCounts.right >= 0 );
		ASSERT( rcOuterPartCounts.bottom >= 0 );
		CArray < INT, INT & > arrColumnWidths, arrRowHeights, arrPreviewAreaWidth, arrPreviewAreaHeights;
		arrColumnWidths.SetSize( nColCount + rcOuterPartCounts.left + rcOuterPartCounts.right );
		arrRowHeights.SetSize( nRowCount + rcOuterPartCounts.top + rcOuterPartCounts.bottom  );
		arrPreviewAreaWidth.SetSize( nRowCount );
		arrPreviewAreaHeights.SetSize( nRowCount );
		CList < LONG, LONG > listColumnIndices, listRowIndices;
		LONG nColNo = 0L, nRowNo = 0L,
			nLastColNo = 0L, nColNoFrom = 0L, nColNoTo = 0L,
			nLastRowNo = 0L, nRowNoFrom = 0L, nRowNoTo = 0L;
		INT nColWidth = 0, nRowHeight = 0, nPageIndex = 0, nPageCount = 0,
			nHorzPageCount = 1, nVertPageCount = 1, nCurrentPageExtent = 0,
			nColOffset = 0, nRowOffset = 0;
		LONG nDocItemCount = nRowCount;
		for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		{
			if( ! _Internal_Notify_OnPreparePrinting_Progress(
					0,             // nPassNo
					2,             // nPassCount
					-1,            // nPageNo
					-1,            // nPageCount
					-1,            // nContentWidthInPages
					nRowNo,        // nDocumentItemNo
					nDocItemCount, // nDocumentItemCount
					-1,            // nInPageDocumentItemFirstNo,
					-1             // nInPageDocumentItemLastNo
					)
				)
				throw 0;
			nRowHeight = 0L;
			CExtReportGridItem * pRGI =
				ReportItemFromTreeItem( arrPrintableItems[nRowNo] );
			ASSERT_VALID( pRGI );
			bool bGroupRow = ( ItemGetChildCount( arrPrintableItems[nRowNo] ) > 0L ) ? true : false;
			if( bGroupRow )
			{ // if group row
				//CSize _sizeCell = pRGI->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
				//ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
				//nRowHeight = max( nRowHeight, _sizeCell.cy );
				LONG nGroupIndex = LONG( pRGI->TreeNodeIndentCompute() );
				ASSERT( nGroupIndex > 0 );
				nGroupIndex --; // suppress root indent
				CExtSafeString strRowText;
				OnReportGridFormatGroupRowText( pRGI, strRowText, nGroupIndex );
				CSize _sizeCell = CExtPaintManager::stat_CalcTextDimension( *pDC, *pRgiFont, strRowText ).Size();
				ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
				nRowHeight = max( nRowHeight, _sizeCell.cy );
			} // if group row
			else
			{ // if data row
				for( nColNo = 0; nColNo < nColCount; nColNo ++ )
				{
					if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
						continue;
					if( nRowNo == 0L )
						arrColumnWidths[ nColNo + rcOuterPartCounts.left ] = nColWidth = 0;
					else
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
					CExtGridCell * pCell = ItemGetCell( arrPrintableItems[nRowNo], nColNo );
					if( pCell == NULL )
						continue;
					ASSERT_VALID( pCell );
					CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
					ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
					if( OnTreeGridQueryColumnOutline( nColNo ) )
						_sizeCell.cx += OnCalcPrintableTreeItemIndent( *pDC, arrPrintableItems[nRowNo] );
					nColWidth = max( nColWidth, _sizeCell.cx );
					arrColumnWidths[ nColNo + rcOuterPartCounts.left ] = nColWidth;
					nRowHeight = max( nRowHeight, _sizeCell.cy );
				} // for( nColNo = 0; nColNo < nColCount; nColNo ++ )
			} // if data row
			if( rcOuterPartCounts.left > 0 )
			{
				for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
				{
					if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
						continue;
					CExtGridCell * pCell = ItemGetCell( arrPrintableItems[nRowNo], nColNo, -1, 0 );
					if( pCell == NULL )
						continue;
					ASSERT_VALID( pCell );
					if( nRowNo == 0L )
						arrColumnWidths[ nColNo ] = nColWidth = 0;
					else
						nColWidth = arrColumnWidths[ nColNo ];
					CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
					ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
					_sizeCell.cx = max( _sizeCell.cx, rcOuterMinExtents.left );
					nColWidth = max( nColWidth, _sizeCell.cx );
					arrColumnWidths[ nColNo ] = nColWidth;
					nRowHeight = max( nRowHeight, _sizeCell.cy );
				} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
			} // if( rcOuterPartCounts.left > 0 )
			if( rcOuterPartCounts.right > 0 )
			{
				for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
				{
					if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
						continue;
					CExtGridCell * pCell = ItemGetCell( arrPrintableItems[nRowNo], nColNo, 1, 0 );
					if( pCell == NULL )
						continue;
					ASSERT_VALID( pCell );
					if( nRowNo == 0L )
						arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ] = nColWidth = 0;
					else
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
					CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
					ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
					_sizeCell.cx = max( _sizeCell.cx, rcOuterMinExtents.right );
					nColWidth = max( nColWidth, _sizeCell.cx );
					arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ] = nColWidth;
					nRowHeight = max( nRowHeight, _sizeCell.cy );
				} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
			} // if( rcOuterPartCounts.right > 0 )
			if( bAutoPreviewPainting && ( ! bGroupRow ) )
			{ // if auto preview painting for data row
				CExtGridCell * pCell = pRGI->PreviewAreaGet();
				if( pCell != NULL )
				{
					ASSERT_VALID( pCell );
					CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
					ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
					_sizeCell.cy += 2;
					arrPreviewAreaWidth[ nRowNo ] = _sizeCell.cx;
					arrPreviewAreaHeights[ nRowNo ] = _sizeCell.cy;
				} // if( pCell != NULL )
			} // if auto preview painting for data row
			arrRowHeights[ nRowNo + rcOuterPartCounts.top ] = nRowHeight;
		} // for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		if( rcOuterPartCounts.top > 0 )
		{
			for( nRowNo = 0; nRowNo < rcOuterPartCounts.top; nRowNo ++ )
			{
				nRowHeight = 0;
				if( OnGridQueryPpvwVisibilityForRow( nRowNo, -1 ) )
				{
					for( nColNo = 0; nColNo < nColCount; nColNo ++ )
					{
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 0, -1 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo + rcOuterPartCounts.left ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // for( nColNo = 0; nColNo < nColCount; nColNo ++ )
					if( rcOuterPartCounts.left > 0 )
					{ // top-left header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
								continue;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, -1, -1 );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo ];
							CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
							ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
							_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
							nColWidth = max( nColWidth, _sizeCell.cx );
							arrColumnWidths[ nColNo ] = nColWidth;
							nRowHeight = max( nRowHeight, _sizeCell.cy );
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
					} // top-left header cells
					if( rcOuterPartCounts.right > 0 )
					{ // top-right header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
								continue;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 1, -1 );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
							CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
							ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
							_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
							nColWidth = max( nColWidth, _sizeCell.cx );
							arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ] = nColWidth;
							nRowHeight = max( nRowHeight, _sizeCell.cy );
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
					} // top-right header cells
				} // if( OnGridQueryPpvwVisibilityForRow( nRowNo, -1 ) )
				arrRowHeights[ nRowNo ] = nRowHeight;
				rcOuterPartExtents.top += nRowHeight;
			} // for( nRowNo = 0; nRowNo < rcOuterPartCounts.top; nRowNo ++ )
		} // if( rcOuterPartCounts.top > 0 )
		if( rcOuterPartCounts.bottom > 0 )
		{
			for( nRowNo = 0; nRowNo < rcOuterPartCounts.bottom; nRowNo ++ )
			{
				nRowHeight = 0;
				if( OnGridQueryPpvwVisibilityForRow( nRowNo, 1 ) )
				{
					for( nColNo = 0; nColNo < nColCount; nColNo ++ )
					{
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 0, 1 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
						CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.bottom );
						nColWidth = max( nColWidth, _sizeCell.cx );
						arrColumnWidths[ nColNo + rcOuterPartCounts.left ] = nColWidth;
						nRowHeight = max( nRowHeight, _sizeCell.cy );
					} // for( nColNo = 0; nColNo < nColCount; nColNo ++ )
					if( rcOuterPartCounts.left > 0 )
					{ // bottom-left header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
								continue;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, -1, 1 );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo ];
							CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
							ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
							_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
							nColWidth = max( nColWidth, _sizeCell.cx );
							arrColumnWidths[ nColNo ] = nColWidth;
							nRowHeight = max( nRowHeight, _sizeCell.cy );
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
					} // bottom-left header cells
					if( rcOuterPartCounts.right > 0 )
					{ // bottom-right header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
								continue;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 1, 1 );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
							CSize _sizeCell = pCell->MeasureCell( this, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
							ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
							_sizeCell.cy = max( _sizeCell.cy, rcOuterMinExtents.top );
							nColWidth = max( nColWidth, _sizeCell.cx );
							arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ] = nColWidth;
							nRowHeight = max( nRowHeight, _sizeCell.cy );
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
					} // bottom-right header cells
				} // if( OnGridQueryPpvwVisibilityForRow( nRowNo, 1 ) )
				arrRowHeights[ nRowNo + rcOuterPartCounts.top + nRowCount ] = nRowHeight;
				rcOuterPartExtents.bottom += nRowHeight;
			} // for( nRowNo = 0; nRowNo < rcOuterPartCounts.bottom; nRowNo ++ )
		} // if( rcOuterPartCounts.bottom > 0 )
		if( rcOuterPartCounts.left > 0 )
		{
			for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
				rcOuterPartExtents.left += arrColumnWidths[ nColNo ];
		} // if( rcOuterPartCounts.left > 0 )
		if( rcOuterPartCounts.right > 0 )
		{
			for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
				rcOuterPartExtents.right += arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
		} // if( rcOuterPartCounts.right > 0 )
		m_rcPageHeaderFooterGutters = OnMeasurePageHeaderFooterGutters( *pDC );
		ASSERT( m_rcPageHeaderFooterGutters.left   >= 0 );
		ASSERT( m_rcPageHeaderFooterGutters.right  >= 0 );
		ASSERT( m_rcPageHeaderFooterGutters.top    >= 0 );
		ASSERT( m_rcPageHeaderFooterGutters.bottom >= 0 );
		INT nInnerMeasureDpiDX =
			nMeasureDpiDX
			- rcOuterPartExtents.left
			- rcOuterPartExtents.right
			- m_rcPageHeaderFooterGutters.left - m_rcPageHeaderFooterGutters.right;
		if( nInnerMeasureDpiDX <= 0 )
			throw 0;
		INT nInnerMeasureDpiDY =
			nMeasureDpiDY
			- rcOuterPartExtents.top
			- rcOuterPartExtents.bottom
			- m_rcPageHeaderFooterGutters.top - m_rcPageHeaderFooterGutters.bottom;
		if( nInnerMeasureDpiDY <= 0 )
			throw 0;
		listRowIndices.AddTail( 0L );
		nCurrentPageExtent = 0;
		for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		{
			nRowHeight =
				  arrRowHeights[ nRowNo + rcOuterPartCounts.top ]
				+ arrPreviewAreaHeights[ nRowNo ]
				;
			nCurrentPageExtent += nRowHeight;
			if( nCurrentPageExtent > nInnerMeasureDpiDY )
			{
				nVertPageCount ++;
				if( nLastRowNo == nRowNo )
				{
					nCurrentPageExtent = 0;
					if( ( nRowNo + 1 ) < nRowCount )
					{
						listRowIndices.AddTail( nRowNo );
						listRowIndices.AddTail( nRowNo + 1 );
						nLastRowNo = nRowNo;
						continue;
					}
				}
				nCurrentPageExtent = nRowHeight;
				listRowIndices.AddTail( nRowNo - 1 );
				listRowIndices.AddTail( nRowNo );
				nLastRowNo = nRowNo;
			}
		} // for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		listRowIndices.AddTail( nRowCount - 1 );
		listColumnIndices.AddTail( 0L );
		for( nColNo = 0, nCurrentPageExtent = 0; nColNo < nColCount; nColNo ++ )
		{
			nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
			nCurrentPageExtent += nColWidth;
			if( nCurrentPageExtent > nInnerMeasureDpiDX )
			{
				nHorzPageCount ++;
				if( nLastColNo == nColNo )
				{
					nCurrentPageExtent = 0;
					if( ( nColNo + 1 ) < nColCount )
					{
						listColumnIndices.AddTail( nColNo );
						listColumnIndices.AddTail( nColNo + 1 );
						nLastColNo = nColNo;
						continue;
					}
				}
				nCurrentPageExtent = nColWidth;
				listColumnIndices.AddTail( nColNo - 1 );
				listColumnIndices.AddTail( nColNo );
				nLastColNo = nColNo;
			}
		} // for( nColNo = 0, nCurrentPageExtent = 0; nColNo < nColCount; nColNo ++ )
		listColumnIndices.AddTail( nColCount - 1 );

		nPageCount = nHorzPageCount * nVertPageCount;
		if( nPageCount > INT(USHRT_MAX) )
			throw 0;
		CRect rcVisibleRange( 0, 0, nColCount, nRowCount );
		if( ! MetafileCache_Init( nPageCount ) )
			throw 0;
		bMetafileCacheStarted = true;
		POSITION posHorz = NULL, posVert = listRowIndices.GetHeadPosition();
		for( nPageIndex = 0; nPageIndex < nPageCount; nPageIndex ++ )
		{
			if( (nPageIndex%nHorzPageCount) == 0 )
			{
				nRowNoFrom = listRowIndices.GetNext( posVert );
				nRowNoTo = listRowIndices.GetNext( posVert );
				posHorz = listColumnIndices.GetHeadPosition();
			}
			nColNoFrom = listColumnIndices.GetNext( posHorz );
			nColNoTo = listColumnIndices.GetNext( posHorz );
			CExtSafeString strMetafileName;
			if( MetafileCache_DiskModeGet() )
				strMetafileName = MetafileCache_GetUniqueName( nPageIndex );
			CMetaFileDC dcMetafile;
			if( m_bUseEnhancedMetafiles )
			{
				if( ! dcMetafile.CreateEnhanced(
						pDC, // NULL,
						( ! strMetafileName.IsEmpty() )
							? LPCTSTR(strMetafileName)
							: NULL
							,
						rcEmfExtent,
						NULL
						)
					)
				{
					ASSERT( FALSE );
					throw 0;
				}
			}
			else
			{
				if( ! dcMetafile.Create(
						( ! strMetafileName.IsEmpty() )
							? LPCTSTR(strMetafileName)
							: NULL
						)
					)
				{
					ASSERT( FALSE );
					throw 0;
				}
			}
			dcMetafile.SetAttribDC( pDC->GetSafeHdc() );
			dcMetafile.SetBkMode( TRANSPARENT );
			dcMetafile.SaveDC();
			OnPrepareDC( &dcMetafile, pInfo );
			nRowOffset = rcOuterPartExtents.top + m_rcPageHeaderFooterGutters.top;
			INT nMaxColOffset = 0;
			bool bProgressAllowsToContinue = false;
			for( nRowNo = nRowNoFrom; nRowNo <= nRowNoTo; nRowNo ++ )
			{
				bProgressAllowsToContinue = 
					_Internal_Notify_OnPreparePrinting_Progress(
						1,              // nPassNo
						2,              // nPassCount
						nPageIndex,     // nPageNo
						nPageCount,     // nPageCount
						nHorzPageCount, // nContentWidthInPages
						nRowNo,         // nDocumentItemNo
						nDocItemCount,  // nDocumentItemCount
						nRowNoFrom,     // nInPageDocumentItemFirstNo
						nRowNoTo        // nInPageDocumentItemLastNo
						);
				if( ! bProgressAllowsToContinue )
					break;
				nRowHeight = arrRowHeights[ nRowNo + rcOuterPartCounts.top ];
				nColOffset = m_rcPageHeaderFooterGutters.left;
				if( rcOuterPartCounts.left > 0 )
				{
					for( nColNo = 0L; nColNo < rcOuterPartCounts.left; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
							continue;
						CExtGridCell * pCell = ItemGetCell( arrPrintableItems[nRowNo], nColNo, -1, 0 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo ];
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + nColWidth,
							nRowOffset + nRowHeight
							);
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_LEFT;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCellAtCenter(
							pInfo,
							rcOuterPartCounts,
							arrPrintableItems,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							//nRowCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							-1,
							//0,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
						nColOffset += nColWidth;
					} // for( nColNo = 0L; nColNo < rcOuterPartCounts.left; nColNo ++ )
				} // if( rcOuterPartCounts.left > 0 )
				if( nRowNo == nRowNoFrom )
					nMaxColOffset = nColOffset;
				CExtReportGridItem * pRGI =
					ReportItemFromTreeItem( arrPrintableItems[nRowNo] );
				ASSERT_VALID( pRGI );
				bool bGroupRow = ( ItemGetChildCount( arrPrintableItems[nRowNo] ) > 0L ) ? true : false;
				if( bGroupRow )
				{ // if group row
					if( (nPageIndex%nHorzPageCount) == 0 )
					{
						CExtReportGridItem * pCell =
							ReportItemFromTreeItem( arrPrintableItems[nRowNo] );
						ASSERT_VALID( pCell );
						LONG nGroupIndex = LONG( pCell->TreeNodeIndentCompute() );
						ASSERT( nGroupIndex > 0 );
						nGroupIndex --; // suppress root indent
/////////////////////////////////////////////////////////////////////////////
// TO-DO: provide virtual method for printing custom report group header rows
/////////////////////////////////////////////////////////////////////////////
						CExtSafeString strRowText;
						OnReportGridFormatGroupRowText( pCell, strRowText, nGroupIndex );
						CSize _sizeCell = CExtPaintManager::stat_CalcTextDimension( *pDC, *pRgiFont, strRowText ).Size();
						ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + _sizeCell.cx,
							nRowOffset + _sizeCell.cy
							);
						rcCell.OffsetRect(
							OnCalcPrintableTreeItemIndent( *pDC, arrPrintableItems[nRowNo] ),
							0
							);
						OnPreparePrinting_RenderGroupRow(
							pInfo,
							arrPrintableItems,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nRowNo,
							rcCell,
							rcVisibleRange,
							strRowText,
							pRgiFont
							);
					} // if( (nPageIndex%nHorzPageCount) == 0 )
				} // if group row
				else
				{ // if data row
					for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
							continue;
						CExtGridCell * pCell = ItemGetCell( arrPrintableItems[nRowNo], nColNo );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + nColWidth,
							nRowOffset + nRowHeight
							);
						if( OnTreeGridQueryColumnOutline( nColNo ) )
							rcCell.left += OnCalcPrintableTreeItemIndent( *pDC, arrPrintableItems[nRowNo] );
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_INNER_CELLS;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCellAtCenter(
							pInfo,
							rcOuterPartCounts,
							arrPrintableItems,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							//nRowCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							0,
							//0,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
						nColOffset += nColWidth;
					} // for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					if( bAutoPreviewPainting && ( ! bGroupRow ) )
					{ // if auto preview painting for data row
						if( (nPageIndex%nHorzPageCount) == 0 )
						{
							CExtGridCell * pCell = pRGI->PreviewAreaGet();
							if( pCell != NULL )
							{
								ASSERT_VALID( pCell );
								CRect rcCell(
									m_rcPageHeaderFooterGutters.left,
									nRowOffset + nRowHeight,
									m_rcPageHeaderFooterGutters.left + arrPreviewAreaWidth[ nRowNo ],
									nRowOffset + arrPreviewAreaHeights[ nRowNo ] + nRowHeight
									);
								rcCell.DeflateRect( 0, 1 );
								CRect rcCellExtra = rcCell;
								DWORD dwAreaFlags = __EGBWA_INNER_CELLS;
								DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
								OnPreparePrinting_RenderAutoPreview(
									pInfo,
									rcOuterPartCounts,
									arrPrintableItems,
									arrColumnWidths,
									arrRowHeights,
									nColCount,
									//nRowCount,
									nPageIndex,
									nColNoFrom,
									nColNoTo,
									nRowNoFrom,
									nRowNoTo,
									pCell,
									dcMetafile,
									nColNo,
									nRowNo,
									0,
									//0,
									rcCellExtra,
									rcCell,
									rcVisibleRange,
									dwAreaFlags,
									dwHelperPaintFlags
									);
							} // if( pCell != NULL )
						} // if( (nPageIndex%nHorzPageCount) == 0 )
					} // if auto preview painting for data row
				} // if data row
				if( rcOuterPartCounts.right > 0 )
				{
					for( nColNo = 0L; nColNo < rcOuterPartCounts.right; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
							continue;
						CExtGridCell * pCell = ItemGetCell( arrPrintableItems[nRowNo], nColNo, 1, 0 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + nColWidth,
							nRowOffset + nRowHeight
							);
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_RIGHT;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCellAtCenter(
							pInfo,
							rcOuterPartCounts,
							arrPrintableItems,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							//nRowCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							1,
							//0,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
						nColOffset += nColWidth;
					} // for( nColNo = 0L; nColNo < rcOuterPartCounts.right; nColNo ++ )
				} // if( rcOuterPartCounts.right > 0 )
				nRowOffset +=
					  nRowHeight
					+ arrPreviewAreaHeights[ nRowNo ]
					;
			} // for( nRowNo = nRowNoFrom; nRowNo <= nRowNoTo; nRowNo ++ )
			if( ! bProgressAllowsToContinue )
				break;

			INT nRowOffsetSaved = nRowOffset;
			nRowOffset = m_rcPageHeaderFooterGutters.top;
			if( rcOuterPartCounts.top > 0 )
			{
				for( nRowNo = 0L; nRowNo < rcOuterPartCounts.top; nRowNo ++ )
				{
					if( ! OnGridQueryPpvwVisibilityForRow( nRowNo, -1 ) )
						continue;
					nRowHeight = arrRowHeights[ nRowNo ];
					if( rcOuterPartCounts.left > 0 )
					{ // top-left header cells
						nColOffset = m_rcPageHeaderFooterGutters.left;
						for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
								continue;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, -1, -1 );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo ];
							CRect rcCell(
								nColOffset,
								nRowOffset,
								nColOffset + nColWidth,
								nRowOffset + nRowHeight
								);
							CRect rcCellExtra = rcCell;
							DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_LEFT|__EGBWA_OUTER_TOP;
							DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
							OnPreparePrinting_RenderSingleCellAtTopOrBottom(
								pInfo,
								rcOuterPartCounts,
								arrColumnWidths,
								arrRowHeights,
								nColCount,
								nRowCount,
								nPageIndex,
								nColNoFrom,
								nColNoTo,
								nRowNoFrom,
								nRowNoTo,
								pCell,
								dcMetafile,
								nColNo,
								nRowNo,
								-1,
								-1,
								rcCellExtra,
								rcCell,
								rcVisibleRange,
								dwAreaFlags,
								dwHelperPaintFlags
								);
							nColOffset += nColWidth;
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
					} // top-left header cells
					//nColOffset = nMaxColOffset;
					nColOffset = rcOuterPartExtents.left + m_rcPageHeaderFooterGutters.left;
					for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
							continue;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 0, -1 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + nColWidth,
							nRowOffset + nRowHeight
							);
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_TOP;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCellAtTopOrBottom(
							pInfo,
							rcOuterPartCounts,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							nRowCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							0,
							-1,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
						nColOffset += nColWidth;
					} // for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					if( rcOuterPartCounts.right > 0 )
					{ // top-right header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
								continue;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 1, -1 );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
							CRect rcCell(
								nColOffset,
								nRowOffset,
								nColOffset + nColWidth,
								nRowOffset + nRowHeight
								);
							CRect rcCellExtra = rcCell;
							DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_RIGHT|__EGBWA_OUTER_TOP;
							DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
							OnPreparePrinting_RenderSingleCellAtTopOrBottom(
								pInfo,
								rcOuterPartCounts,
								arrColumnWidths,
								arrRowHeights,
								nColCount,
								nRowCount,
								nPageIndex,
								nColNoFrom,
								nColNoTo,
								nRowNoFrom,
								nRowNoTo,
								pCell,
								dcMetafile,
								nColNo,
								nRowNo,
								1,
								-1,
								rcCellExtra,
								rcCell,
								rcVisibleRange,
								dwAreaFlags,
								dwHelperPaintFlags
								);
							nColOffset += nColWidth;
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
					} // top-right header cells
					nRowOffset += nRowHeight;
				} // for( nRowNo = 0L; nRowNo < rcOuterPartCounts.top; nRowNo ++ )
			} // if( rcOuterPartCounts.top > 0 )
			nRowOffset = nRowOffsetSaved;
			if( rcOuterPartCounts.bottom > 0 )
			{
				for( nRowNo = 0L; nRowNo < rcOuterPartCounts.bottom; nRowNo ++ )
				{
					if( ! OnGridQueryPpvwVisibilityForRow( nRowNo, 1 ) )
						continue;
					nRowHeight = arrRowHeights[ nRowNo + rcOuterPartCounts.top + nRowCount ];
					if( rcOuterPartCounts.left > 0 )
					{ // bottom-left header cells
						nColOffset = m_rcPageHeaderFooterGutters.left;
						for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, -1 ) )
								continue;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, -1, 1 );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo ];
							CRect rcCell(
								nColOffset,
								nRowOffset,
								nColOffset + nColWidth,
								nRowOffset + nRowHeight
								);
							CRect rcCellExtra = rcCell;
							DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_LEFT|__EGBWA_OUTER_BOTTOM;
							DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
							OnPreparePrinting_RenderSingleCellAtTopOrBottom(
								pInfo,
								rcOuterPartCounts,
								arrColumnWidths,
								arrRowHeights,
								nColCount,
								nRowCount,
								nPageIndex,
								nColNoFrom,
								nColNoTo,
								nRowNoFrom,
								nRowNoTo,
								pCell,
								dcMetafile,
								nColNo,
								nRowNo,
								-1,
								1,
								rcCellExtra,
								rcCell,
								rcVisibleRange,
								dwAreaFlags,
								dwHelperPaintFlags
								);
							nColOffset += nColWidth;
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.left; nColNo ++ )
					} // bottom-left header cells
					nColOffset = nMaxColOffset;
					for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					{
						if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 0 ) )
							continue;
						CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 0, 1 );
						if( pCell == NULL )
							continue;
						ASSERT_VALID( pCell );
						nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left ];
						CRect rcCell(
							nColOffset,
							nRowOffset,
							nColOffset + nColWidth,
							nRowOffset + nRowHeight
							);
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_BOTTOM;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCellAtTopOrBottom(
							pInfo,
							rcOuterPartCounts,
							arrColumnWidths,
							arrRowHeights,
							nColCount,
							nRowCount,
							nPageIndex,
							nColNoFrom,
							nColNoTo,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							0,
							1,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
						nColOffset += nColWidth;
					} // for( nColNo = nColNoFrom; nColNo <= nColNoTo; nColNo ++ )
					if( rcOuterPartCounts.right > 0 )
					{ // bottom-right header cells
						for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
						{
							if( ! OnGridQueryPpvwVisibilityForColumn( nColNo, 1 ) )
								continue;
							CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 1, 1 );
							if( pCell == NULL )
								continue;
							ASSERT_VALID( pCell );
							nColWidth = arrColumnWidths[ nColNo + rcOuterPartCounts.left + nColCount ];
							CRect rcCell(
								nColOffset,
								nRowOffset,
								nColOffset + nColWidth,
								nRowOffset + nRowHeight
								);
							CRect rcCellExtra = rcCell;
							DWORD dwAreaFlags = __EGBWA_OUTER_CELLS|__EGBWA_OUTER_RIGHT|__EGBWA_OUTER_BOTTOM;
							DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
							OnPreparePrinting_RenderSingleCellAtTopOrBottom(
								pInfo,
								rcOuterPartCounts,
								arrColumnWidths,
								arrRowHeights,
								nColCount,
								nRowCount,
								nPageIndex,
								nColNoFrom,
								nColNoTo,
								nRowNoFrom,
								nRowNoTo,
								pCell,
								dcMetafile,
								nColNo,
								nRowNo,
								1,
								1,
								rcCellExtra,
								rcCell,
								rcVisibleRange,
								dwAreaFlags,
								dwHelperPaintFlags
								);
							nColOffset += nColWidth;
						} // for( nColNo = 0; nColNo < rcOuterPartCounts.right; nColNo ++ )
					} // bottom-right header cells
					nRowOffset += nRowHeight;
				} // for( nRowNo = 0L; nRowNo < rcOuterPartCounts.bottom; nRowNo ++ )
			} // if( rcOuterPartCounts.bottom > 0 )

			CRect rcEntirePage( 0, 0, nMeasureDpiDX, nMeasureDpiDY );
			OnDrawPageHeaderFooterGutters( dcMetafile, rcEntirePage, m_rcPageHeaderFooterGutters, nPageIndex, nPageCount, pInfo );

			dcMetafile.RestoreDC( -1 );
			if( ! bProgressAllowsToContinue )
				throw 0;
			HANDLE hMetafile =
				m_bUseEnhancedMetafiles
					? ( (HANDLE) dcMetafile.CloseEnhanced() )
					: ( (HANDLE) dcMetafile.Close() )
					;
			if( ! MetafileCache_InitAt(
					nPageIndex,
					hMetafile,
					( ! strMetafileName.IsEmpty() )
						? LPCTSTR(strMetafileName)
						: NULL
					)
				)
				throw 0;
		} // for( nPageIndex = 0; nPageIndex < nPageCount; nPageIndex ++ )
		WORD nPageNoMin = WORD(1), nPageNoMax = WORD( WORD(nPageCount) + WORD(nPageNoMin) - WORD(1) );
		pInfo->m_nNumPreviewPages = nPageNoMax;
		pInfo->m_pPD->m_pd.nMinPage = nPageNoMin;
		pInfo->m_pPD->m_pd.nFromPage = nPageNoMin;
		pInfo->m_pPD->m_pd.nMaxPage = nPageNoMax;
		pInfo->m_pPD->m_pd.nToPage = nPageNoMax;
		bOK = true;
	} // try
	catch( CException * pException )
	{
		pException->Delete();
	} // catch( CException * pException )
	catch( ... )
	{
	} // catch( ... )
	pDC->RestoreDC( -1 );
	if( bMetafileCacheStarted )
		MetafileCache_InitComplete( bOK );
	OnPreparePrinting_End( ! bOK );
	_WaitCursor;
	if( ! bOK )
		return FALSE;
	else
		return TRUE;
}

#endif // (!defined __EXT_MFC_NO_REPORTGRIDWND)

#if (!defined __EXT_MFC_NO_PROPERTYGRIDWND)

/////////////////////////////////////////////////////////////////////////////
// template < > class CExtPPVW < CExtPropertyGridCtrl >

void CExtPPVW < CExtPropertyGridCtrl > :: OnPrepareDC(
	CDC * pDC,
	CPrintInfo * pInfo // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC );
	ASSERT( pDC->GetSafeHdc() != NULL );
	pInfo;

CFont * pFont = & g_PaintManager->m_FontNormal;
 	if( pFont->GetSafeHandle() == NULL )
 		return;
	pDC->SelectObject( pFont );
}

INT CExtPPVW < CExtPropertyGridCtrl > :: OnCalcPrintableTreeItemIndent(
	CDC & dcMeasure,
	CExtPropertyItem * pPropertyItem
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&dcMeasure) );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	ASSERT_VALID( pPropertyItem );
	dcMeasure;
INT nIndent = 0;
CExtPropertyItem * pPI = pPropertyItem->ItemParentGet();
	for( ; pPI != NULL; )
	{
		if( pPI->IsKindOf( RUNTIME_CLASS( CExtPropertyStore ) ) )
			break;
		nIndent += 20;
		pPI = pPI->ItemParentGet();
	}
	return nIndent;
}

void CExtPPVW < CExtPropertyGridCtrl > :: OnCalcPrintablePropertyItemArray(
	CArray < CExtPropertyItem *, CExtPropertyItem * & > & arrPrintableItems,
	CArray < INT, INT & > & arrPrintableParents
	)
{
	ASSERT_VALID( this );
	arrPrintableItems.RemoveAll();
 	arrPrintableParents.RemoveAll();
CExtPropertyStore * pPS = PropertyStoreGet();
	if( pPS == NULL )
		return;
	ASSERT_VALID( pPS );
CExtPropertyGridWnd * pPGW = GetActiveGrid();
	if( pPGW == NULL )
		return;
	ASSERT_VALID( pPGW );
HTREEITEM hti = pPGW->ItemGetRoot();
	if( hti == NULL )
		return;
	for( hti = pPGW->ItemGetNext( hti, false, false, false ); hti != NULL; hti = pPGW->ItemGetNext( hti, false, false, false ) )
	{
		CExtPropertyItem * pPI = pPGW->PropertyItemFromTreeItem( hti );
		ASSERT( pPI != NULL );
		arrPrintableItems.Add( pPI );
	}
}

bool CExtPPVW < CExtPropertyGridCtrl > :: IsPrintPreviewAvailable() const
{
	ASSERT_VALID( this );
	if( ! CExtPPVW_Printable::IsPrintPreviewAvailable() )
		return false;
	if( GetSafeHwnd() == NULL )
		return false;
	return true;
}

void CExtPPVW < CExtPropertyGridCtrl > :: OnPreparePrinting_RenderSingleCellAtCenter(
	CPrintInfo * pInfo,
	const CArray < CExtPropertyItem *, CExtPropertyItem * & > & arrPrintableItems,
	const CArray < INT, INT & > & arrPrintableParents,
	const INT * arrColumnWidths,
	const CArray < INT, INT & > & arrRowHeights,
	CExtGridWnd * pWndGrid,
	INT nPageIndex,
	LONG nRowNoFrom,
	LONG nRowNoTo,
	CExtGridCell * pCell,
	CDC & dc,
	LONG nColNo,
	LONG nRowNo,
	CRect & rcCellExtra,
	CRect & rcCell,
	CRect & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT_VALID( pCell );
	pInfo;
	nPageIndex;
	nRowNoFrom;
	nRowNoTo;
	arrPrintableItems;
	arrPrintableParents;
	arrColumnWidths;
	arrRowHeights;
	if( m_bDrawSimpleWhiteBackground )
	{
		CRect rcLines = rcCell;
		rcLines.right ++;
		rcLines.bottom ++;
		HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(WHITE_BRUSH) );
		HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_PEN) );
		dc.Rectangle( &rcLines );
		::SelectObject( dc.m_hDC, hOldPen );
		::SelectObject( dc.m_hDC, hOldBrush );
	} // if( m_bDrawSimpleWhiteBackground )
	pCell->OnPaintBackground(
		*pWndGrid,
		dc,
		nColNo,
		nRowNo,
		nColNo,
		nRowNo,
		0,
		0,
		rcCellExtra,
		rcCell,
		rcVisibleRange,
		dwAreaFlags,
		dwHelperPaintFlags
		);
	pCell->OnPaintForeground(
		*pWndGrid,
		dc,
		nColNo,
		nRowNo,
		nColNo,
		nRowNo,
		0,
		0,
		rcCellExtra,
		rcCell,
		rcVisibleRange,
		dwAreaFlags,
		dwHelperPaintFlags
		);
	if( m_bDrawSimpleBlackBorders )
	{
		CRect rcLines = rcCell;
		rcLines.right ++;
		rcLines.bottom ++;
		HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_BRUSH) );
		HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(BLACK_PEN) );
		dc.Rectangle( &rcLines );
		::SelectObject( dc.m_hDC, hOldPen );
		::SelectObject( dc.m_hDC, hOldBrush );
	} // if( m_bDrawSimpleBlackBorders )
}

void CExtPPVW < CExtPropertyGridCtrl > :: OnPreparePrinting_RenderCategoryName(
	CPrintInfo * pInfo,
	const CArray < CExtPropertyItem *, CExtPropertyItem * & > & arrPrintableItems,
	const CArray < INT, INT & > & arrPrintableParents,
	const INT * arrColumnWidths,
	const CArray < INT, INT & > & arrRowHeights,
	CExtGridWnd * pWndGrid,
	INT nPageIndex,
	LONG nRowNoFrom,
	LONG nRowNoTo,
	LONG nRowNo,
	CDC & dc,
	CExtPropertyCategory * pPropertyItem,
	CRect & rcName,
	__EXT_MFC_SAFE_LPCTSTR strName,
	INT nIndentExt
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT_VALID( pPropertyItem );
	ASSERT( LPCTSTR(strName) != NULL );
	ASSERT( 0 <= nRowNo && nRowNo < LONG(arrPrintableItems.GetSize()) );
	ASSERT( LPVOID(arrPrintableItems[nRowNo]) == LPVOID(pPropertyItem) );
	pInfo;
	nPageIndex;
	nRowNoFrom;
	nRowNoTo;
	arrPrintableItems;
	arrPrintableParents;
	arrColumnWidths;
	arrRowHeights;
	pPropertyItem;
	pWndGrid;
	nIndentExt;
	nRowNo;

// { // BLOCK: begin (test custom background with lines)
// CRect rcLines = rcCategoryName;
// rcLines.right ++;
// rcLines.bottom ++;
// HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(WHITE_BRUSH) );
// HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_PEN) );
// dc.Rectangle( &rcLines );
// ::SelectObject( dc.m_hDC, hOldPen );
// ::SelectObject( dc.m_hDC, hOldBrush );
// } // BLOCK: end (test custom background with lines)

INT nNameLen = nNameLen = INT( _tcslen( LPCTSTR(strName) ) );
COLORREF clrOldText = dc.SetTextColor( RGB(0,0,0) );
INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
	CExtRichContentLayout::stat_DrawText( dc.m_hDC, strName, nNameLen, &rcName, DT_SINGLELINE|DT_LEFT|DT_VCENTER|DT_NOCLIP, 0 );
	dc.SetBkMode( nOldBkMode );
	dc.SetTextColor( clrOldText );

// { // BLOCK: begin (test custom background with lines)
// CRect rcLines = rcCategoryName;
// rcLines.right ++;
// rcLines.bottom ++;
// HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_BRUSH) );
// HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(BLACK_PEN) );
// dc.Rectangle( &rcLines );
// ::SelectObject( dc.m_hDC, hOldPen );
// ::SelectObject( dc.m_hDC, hOldBrush );
// } // BLOCK: end (test custom background with lines)

	if( m_bDrawSimpleBlackOutline )
	{
		CExtPropertyStore * pPS = PropertyStoreGet();
		ASSERT_VALID( pPS );
		if( LPVOID(pPropertyItem->ItemParentGet()) != LPVOID(pPS) )
		{
			CExtPropertyItem * pWalkItem = pPropertyItem;
			INT nIndentLevel = 1;
			INT nWalkRowNo = INT(nRowNo);
			for( ; LPVOID(pWalkItem) != LPVOID(pPS); )
			{
				INT nParentWalkRowNo = arrPrintableParents[ nWalkRowNo ];
				nWalkRowNo = nParentWalkRowNo;
				if( nWalkRowNo < 0 )
					break;
				pWalkItem = arrPrintableItems[ nWalkRowNo ];
				ASSERT_VALID( pWalkItem );
				nIndentLevel ++;
			}
			if( nIndentLevel > 1 )
			{
				HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_BRUSH) );
				HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(BLACK_PEN) );
				COLORREF clrDots = RGB( 0, 0, 0 );
				CRect rcIndent(
					rcName.left,
					rcName.top,
					rcName.left,
					rcName.bottom
					);
				pWalkItem = pPropertyItem;
				nWalkRowNo = INT(nRowNo);
				INT nPxIndent = rcName.left / (nIndentLevel-1);
				for( ; LPVOID(pWalkItem) != LPVOID(pPS); )
				{
					ASSERT( pWalkItem != NULL );
					INT nParentWalkRowNo = arrPrintableParents[ nWalkRowNo ];
					if( nParentWalkRowNo >= 0 && LPVOID(arrPrintableItems[ nParentWalkRowNo ]) == LPVOID(pPS) )
						break;
					rcIndent.right = rcIndent.left;
					rcIndent.left -= nPxIndent;
					INT nMetricH = rcIndent.Width();
					INT nMetricH2 = nMetricH / 2;
					INT nMetricV = rcIndent.Height();
					INT nMetricV2 = min( nMetricH, nMetricV );
					nMetricV2 /= 2;
					bool bDrawUpperLineV = false;
					if( LPVOID(pPropertyItem) == LPVOID(pWalkItem) )
					{
						if( m_bDrawSimpleBlackOutlineUsingDots )
							CExtPaintManager::stat_DrawDotLineH(
								dc,
								rcIndent.left + nMetricH2,
								rcIndent.right,
								rcIndent.top + nMetricV2,
								clrDots
								);
						else
						{
							dc.MoveTo(
								rcIndent.left + nMetricH2,
								rcIndent.top + nMetricV2
								);
							dc.LineTo(
								rcIndent.right,
								rcIndent.top + nMetricV2
								);
						}
						bDrawUpperLineV = true;
					}
					CExtPropertyItem * pWalkItemNext = NULL;
					if( nParentWalkRowNo >= 0 )
					{
//						CExtPropertyItem * pParent = arrPrintableItems[ nParentWalkRowNo ];
						CExtPropertyItem * pParent = pWalkItem->ItemParentGet();
						ASSERT_VALID( pParent );
						INT nWalkRealSiblingIndex = pParent->ItemGetIndexOf( pWalkItem );
						ASSERT( nWalkRealSiblingIndex >= 0 );
						INT nRealSiblingCount = pParent->ItemGetCount();
						if( nWalkRealSiblingIndex < ( nRealSiblingCount - 1 ) )
							pWalkItemNext = pParent->ItemGetAt( nWalkRealSiblingIndex + 1 );
					}
					if( pWalkItemNext != NULL )
					{
						if( m_bDrawSimpleBlackOutlineUsingDots )
							CExtPaintManager::stat_DrawDotLineV(
								dc,
								rcIndent.left + nMetricH2,
								rcIndent.top + nMetricV2,
								rcIndent.bottom,
								clrDots
								);
						else
						{
							dc.MoveTo(
								rcIndent.left + nMetricH2,
								rcIndent.top + nMetricV2
								);
							dc.LineTo(
								rcIndent.left + nMetricH2,
								rcIndent.bottom
								);
						}
						bDrawUpperLineV = true;
					}
					if( bDrawUpperLineV )
					{
						if( m_bDrawSimpleBlackOutlineUsingDots )
							CExtPaintManager::stat_DrawDotLineV(
								dc,
								rcIndent.left + nMetricH2,
								rcIndent.top,
								rcIndent.top + nMetricV2,
								clrDots
								);
						else
						{
							dc.MoveTo(
								rcIndent.left + nMetricH2,
								rcIndent.top
								);
							dc.LineTo(
								rcIndent.left + nMetricH2,
								rcIndent.top + nMetricV2
								);
						}
					}
					nWalkRowNo = nParentWalkRowNo;
					if( nWalkRowNo < 0 )
						break;
					pWalkItem = arrPrintableItems[ nWalkRowNo ];
					ASSERT_VALID( pWalkItem );
				} // for( ...
				::SelectObject( dc.m_hDC, hOldPen );
				::SelectObject( dc.m_hDC, hOldBrush );
			} // if( nIndentLevel > 1 )
		} // if( LPVOID(pPropertyItem->ItemParentGet()) != LPVOID(pPS) )
	} // if( m_bDrawSimpleBlackOutline )
}

void CExtPPVW < CExtPropertyGridCtrl > :: OnPreparePrinting_RenderPropertyItemCaption(
	CPrintInfo * pInfo,
	const CArray < CExtPropertyItem *, CExtPropertyItem * & > & arrPrintableItems,
	const CArray < INT, INT & > & arrPrintableParents,
	const INT * arrColumnWidths,
	const CArray < INT, INT & > & arrRowHeights,
	CExtGridWnd * pWndGrid,
	INT nPageIndex,
	LONG nRowNoFrom,
	LONG nRowNoTo,
	LONG nRowNo,
	CDC & dc,
	CExtPropertyItem * pPropertyItem,
	CRect & rcName,
	__EXT_MFC_SAFE_LPCTSTR strName,
	INT nIndentExt
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT_VALID( pPropertyItem );
	ASSERT( LPCTSTR(strName) != NULL );
	ASSERT( 0 <= nRowNo && nRowNo < LONG(arrPrintableItems.GetSize()) );
	ASSERT( LPVOID(arrPrintableItems[nRowNo]) == LPVOID(pPropertyItem) );
	pInfo;
	nPageIndex;
	nRowNoFrom;
	nRowNoTo;
	arrPrintableItems;
	arrPrintableParents;
	arrColumnWidths;
	arrRowHeights;
	pPropertyItem;
	pWndGrid;
	nIndentExt;
	nRowNo;

	if( m_bDrawSimpleWhiteBackground )
	{
		CRect rcLines = rcName;
		rcLines.right ++;
		rcLines.bottom ++;
		HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(WHITE_BRUSH) );
		HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_PEN) );
		dc.Rectangle( &rcLines );
		::SelectObject( dc.m_hDC, hOldPen );
		::SelectObject( dc.m_hDC, hOldBrush );
	} // if( m_bDrawSimpleWhiteBackground )
INT nNameLen = nNameLen = INT( _tcslen( LPCTSTR(strName) ) );
COLORREF clrOldText = dc.SetTextColor( RGB(0,0,0) );
INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
	CExtRichContentLayout::stat_DrawText( dc.m_hDC, strName, nNameLen, &rcName, DT_SINGLELINE|DT_LEFT|DT_VCENTER|DT_NOCLIP, 0 );
	dc.SetBkMode( nOldBkMode );
	dc.SetTextColor( clrOldText );
	if( m_bDrawSimpleBlackBorders )
	{
		CRect rcLines = rcName;
		rcLines.left = 0;
		rcLines.right ++;
		rcLines.bottom ++;
		HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_BRUSH) );
		HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(BLACK_PEN) );
		dc.Rectangle( &rcLines );
		::SelectObject( dc.m_hDC, hOldPen );
		::SelectObject( dc.m_hDC, hOldBrush );
	} // if( m_bDrawSimpleBlackBorders )

	if( m_bDrawSimpleBlackOutline )
	{
		CExtPropertyStore * pPS = PropertyStoreGet();
		ASSERT_VALID( pPS );
		if( LPVOID(pPropertyItem->ItemParentGet()) != LPVOID(pPS) )
		{
			CExtPropertyItem * pWalkItem = pPropertyItem;
			INT nIndentLevel = 1;
			INT nWalkRowNo = INT(nRowNo);
			for( ; LPVOID(pWalkItem) != LPVOID(pPS); )
			{
				INT nParentWalkRowNo = arrPrintableParents[ nWalkRowNo ];
				nWalkRowNo = nParentWalkRowNo;
				if( nWalkRowNo < 0 )
					break;
				pWalkItem = arrPrintableItems[ nWalkRowNo ];
				ASSERT_VALID( pWalkItem );
				nIndentLevel ++;
			}
			if( nIndentLevel > 1 )
			{
				HGDIOBJ hOldBrush = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(NULL_BRUSH) );
				HGDIOBJ hOldPen = ::SelectObject( dc.m_hDC, (HGDIOBJ)::GetStockObject(BLACK_PEN) );
				COLORREF clrDots = RGB( 0, 0, 0 );
				CRect rcIndent(
					rcName.left,
					rcName.top,
					rcName.left,
					rcName.bottom
					);
				pWalkItem = pPropertyItem;
				nWalkRowNo = INT(nRowNo);
				INT nPxIndent = rcName.left / (nIndentLevel-1);
				for( ; LPVOID(pWalkItem) != LPVOID(pPS); )
				{
					ASSERT( pWalkItem != NULL );
					INT nParentWalkRowNo = arrPrintableParents[ nWalkRowNo ];
					if( nParentWalkRowNo >= 0 && LPVOID(arrPrintableItems[ nParentWalkRowNo ]) == LPVOID(pPS) )
						break;
					rcIndent.right = rcIndent.left;
					rcIndent.left -= nPxIndent;
					INT nMetricH = rcIndent.Width();
					INT nMetricH2 = nMetricH / 2;
					INT nMetricV = rcIndent.Height();
					INT nMetricV2 = min( nMetricH, nMetricV );
					nMetricV2 /= 2;
					bool bDrawUpperLineV = false;
					if( LPVOID(pPropertyItem) == LPVOID(pWalkItem) )
					{
						if( m_bDrawSimpleBlackOutlineUsingDots )
							CExtPaintManager::stat_DrawDotLineH(
								dc,
								rcIndent.left + nMetricH2,
								rcIndent.right,
								rcIndent.top + nMetricV2,
								clrDots
								);
						else
						{
							dc.MoveTo(
								rcIndent.left + nMetricH2,
								rcIndent.top + nMetricV2
								);
							dc.LineTo(
								rcIndent.right,
								rcIndent.top + nMetricV2
								);
						}
						bDrawUpperLineV = true;
					}
					CExtPropertyItem * pWalkItemNext = NULL;
					if( nParentWalkRowNo >= 0 )
					{
//						CExtPropertyItem * pParent = arrPrintableItems[ nParentWalkRowNo ];
						CExtPropertyItem * pParent = pWalkItem->ItemParentGet();
						ASSERT_VALID( pParent );
						INT nWalkRealSiblingIndex = pParent->ItemGetIndexOf( pWalkItem );
						ASSERT( nWalkRealSiblingIndex >= 0 );
						INT nRealSiblingCount = pParent->ItemGetCount();
						if( nWalkRealSiblingIndex < ( nRealSiblingCount - 1 ) )
							pWalkItemNext = pParent->ItemGetAt( nWalkRealSiblingIndex + 1 );
					}
					if( pWalkItemNext != NULL )
					{
						if( m_bDrawSimpleBlackOutlineUsingDots )
							CExtPaintManager::stat_DrawDotLineV(
								dc,
								rcIndent.left + nMetricH2,
								rcIndent.top + nMetricV2,
								rcIndent.bottom,
								clrDots
								);
						else
						{
							dc.MoveTo(
								rcIndent.left + nMetricH2,
								rcIndent.top + nMetricV2
								);
							dc.LineTo(
								rcIndent.left + nMetricH2,
								rcIndent.bottom
								);
						}
						bDrawUpperLineV = true;
					}
					if( bDrawUpperLineV )
					{
						if( m_bDrawSimpleBlackOutlineUsingDots )
							CExtPaintManager::stat_DrawDotLineV(
								dc,
								rcIndent.left + nMetricH2,
								rcIndent.top,
								rcIndent.top + nMetricV2,
								clrDots
								);
						else
						{
							dc.MoveTo(
								rcIndent.left + nMetricH2,
								rcIndent.top
								);
							dc.LineTo(
								rcIndent.left + nMetricH2,
								rcIndent.top + nMetricV2
								);
						}
					}
					nWalkRowNo = nParentWalkRowNo;
					if( nWalkRowNo < 0 )
						break;
					pWalkItem = arrPrintableItems[ nWalkRowNo ];
					ASSERT_VALID( pWalkItem );
				} // for( ...
				::SelectObject( dc.m_hDC, hOldPen );
				::SelectObject( dc.m_hDC, hOldBrush );
			} // if( nIndentLevel > 1 )
		} // if( LPVOID(pPropertyItem->ItemParentGet()) != LPVOID(pPS) )
	} // if( m_bDrawSimpleBlackOutline )
}

BOOL CExtPPVW < CExtPropertyGridCtrl > :: OnPreparePrinting(
	CPrintInfo * pInfo
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return FALSE;
CArray < CExtPropertyItem *, CExtPropertyItem * & > arrPrintableItems;
CArray < INT, INT & > arrPrintableParents;
	OnCalcPrintablePropertyItemArray( arrPrintableItems, arrPrintableParents );
LONG nRowCount = LONG( arrPrintableItems.GetSize() );
	if( nRowCount <= 0 )
		return FALSE;
	if( ! CExtPPVW_Printable::OnPreparePrinting( pInfo ) )
		return FALSE;
CWaitCursor _WaitCursor;

CExtGridWnd _wndFake;
CExtGridWnd * pWndActiveGrid = GetActiveGrid();
	if( pWndActiveGrid == NULL )
		pWndActiveGrid = &_wndFake;

	ASSERT( pInfo->m_pPD->m_pd.hDC != NULL );
CDC * pDC = CDC::FromHandle( pInfo->m_pPD->m_pd.hDC );
	ASSERT_VALID( pDC );
INT nPrinterDpiX = ::GetDeviceCaps( pDC->m_hDC, LOGPIXELSX );
INT nPrinterDpiY = ::GetDeviceCaps( pDC->m_hDC, LOGPIXELSY );
INT nPrinterDpiDX = ::GetDeviceCaps( pDC->m_hDC, HORZRES );
INT nPrinterDpiDY = ::GetDeviceCaps( pDC->m_hDC, VERTRES );
INT nMeasureDpiDX = ::MulDiv( nPrinterDpiDX, g_PaintManager.m_nLPX, nPrinterDpiX );
INT nMeasureDpiDY = ::MulDiv( nPrinterDpiDY, g_PaintManager.m_nLPY, nPrinterDpiY );
// CSize _sizePrinterOffset(
// 		pDC->GetDeviceCaps(PHYSICALOFFSETX),
// 		pDC->GetDeviceCaps(PHYSICALOFFSETY)
// 		);
// 	nMeasureDpiDX -= ::MulDiv( _sizePrinterOffset.cx * 2, g_PaintManager.m_nLPX, nPrinterDpiX );
// 	nMeasureDpiDY -= ::MulDiv( _sizePrinterOffset.cy * 2, g_PaintManager.m_nLPY, nPrinterDpiY );

CSize sizePMLT( m_rcPageMarginsHM.left, m_rcPageMarginsHM.top ), sizePMRB( m_rcPageMarginsHM.right, m_rcPageMarginsHM.bottom ); 
	pDC->HIMETRICtoDP( &sizePMLT );
	pDC->HIMETRICtoDP( &sizePMRB );
	sizePMLT.cx = ::MulDiv( sizePMLT.cx, g_PaintManager.m_nLPX, nPrinterDpiX );
	sizePMRB.cx = ::MulDiv( sizePMRB.cx, g_PaintManager.m_nLPX, nPrinterDpiX );
	sizePMLT.cy = ::MulDiv( sizePMLT.cy, g_PaintManager.m_nLPY, nPrinterDpiY );
	sizePMRB.cy = ::MulDiv( sizePMRB.cy, g_PaintManager.m_nLPY, nPrinterDpiY );
	nMeasureDpiDX -= sizePMLT.cx + sizePMRB.cx;
	nMeasureDpiDY -= sizePMLT.cy + sizePMRB.cy;

// 	pDC->DPtoLP( &_sizePrinterOffset );
CSize _sizePrinterHIMETRIC( nPrinterDpiDX, nPrinterDpiDY );
// 	_sizePrinterHIMETRIC -= _sizePrinterOffset;
// 	_sizePrinterHIMETRIC -= _sizePrinterOffset;
	pDC->DPtoHIMETRIC( &_sizePrinterHIMETRIC );
	//_sizePrinterHIMETRIC.cx -= m_rcPageMarginsHM.left + m_rcPageMarginsHM.right;
	//_sizePrinterHIMETRIC.cy -= m_rcPageMarginsHM.top + m_rcPageMarginsHM.bottom;
CRect rcEmfExtent( 0, 0, _sizePrinterHIMETRIC.cx, _sizePrinterHIMETRIC.cy );
DWORD dwHelperPaintFlagsBasic = m_dwBasicPaintFlags;
	if( m_pWndPP != NULL )
		dwHelperPaintFlagsBasic |= __EGCPF_PRINT_PREVIEW;
	else
		dwHelperPaintFlagsBasic |= __EGCPF_PRINTER;
	pDC->SaveDC();
	OnPrepareDC( pDC );
bool bOK = false, bMetafileCacheStarted = false;
	try
	{
		OnPreparePrinting_Begin();
		INT arrColumnWidths[ 2 ] = { 0, 0 }, nCategoryWidth = 0;
		CArray < INT, INT & > arrRowHeights;
		arrRowHeights.SetSize( nRowCount );
		CList < LONG, LONG > listRowIndices;
		LONG nColNo = 0L, nRowNo = 0L, nLastRowNo = 0L, nRowNoFrom = 0L, nRowNoTo = 0L;
		INT nPageIndex = 0, nPageCount = 0, nRowOffset = 0,
			nHorzPageCount = 1, nVertPageCount = 1, nCurrentPageExtent = 0;
		LONG nDocItemCount = nRowCount;
		for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		{
			if( ! _Internal_Notify_OnPreparePrinting_Progress(
					0,             // nPassNo
					2,             // nPassCount
					-1,            // nPageNo
					-1,            // nPageCount
					-1,            // nContentWidthInPages
					nRowNo,        // nDocumentItemNo
					nDocItemCount, // nDocumentItemCount
					-1,            // nInPageDocumentItemFirstNo,
					-1             // nInPageDocumentItemLastNo
					)
				)
				throw 0;
			LONG nRowHeight = 0L;
			CExtPropertyItem * pPropertyItem = arrPrintableItems[nRowNo];
			ASSERT_VALID( pPropertyItem );
			bool bCategory = pPropertyItem->IsKindOf( RUNTIME_CLASS( CExtPropertyCategory ) ) ? true : false;
			CFont & _font = bCategory ? g_PaintManager->m_FontBold : g_PaintManager->m_FontNormal;
			CFont * pOldFont = pDC->SelectObject( &_font );
			CSize _sizeName( 0, 0 );
			LPCTSTR strName = pPropertyItem->NameGet();
			if( strName != NULL && _tcslen( strName ) > 0 )
			{
				CExtSafeString s = strName;
				_sizeName = CExtPaintManager::stat_CalcTextDimension( *pDC, _font, s ).Size();
				_sizeName.cx += OnCalcPrintableTreeItemIndent( *pDC, arrPrintableItems[nRowNo] );
				if( bCategory )
					nCategoryWidth = max( nCategoryWidth, _sizeName.cx );
				else
					arrColumnWidths[ 0 ] = max( arrColumnWidths[ 0 ], _sizeName.cx );
				nRowHeight = max( nRowHeight, _sizeName.cy );
			} // if( strName != NULL && _tcslen( strName ) > 0 )
			CExtGridCell * pCell = pPropertyItem->ValueActiveGet();
			if( pCell != NULL )
			{
				ASSERT_VALID( pCell );
				CSize _sizeCell = pCell->MeasureCell( NULL, *pDC, __EGCPF_SIMPLIFIED_RENDERING_TARGET );
				ASSERT( _sizeCell.cx >= 0 && _sizeCell.cy >= 0 );
				_sizeCell.cx += OnCalcPrintableTreeItemIndent( *pDC, arrPrintableItems[nRowNo] );
				arrColumnWidths[ 1 ] = max( arrColumnWidths[ 1 ], _sizeCell.cx );
				nRowHeight = max( nRowHeight, _sizeCell.cy );
			} // if( pCell != NULL )
			pDC->SelectObject( pOldFont );
			arrRowHeights[ nRowNo ] = nRowHeight;
		} // for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		m_rcPageHeaderFooterGutters = OnMeasurePageHeaderFooterGutters( *pDC );
		ASSERT( m_rcPageHeaderFooterGutters.left   >= 0 );
		ASSERT( m_rcPageHeaderFooterGutters.right  >= 0 );
		ASSERT( m_rcPageHeaderFooterGutters.top    >= 0 );
		ASSERT( m_rcPageHeaderFooterGutters.bottom >= 0 );
		INT nInnerMeasureDpiDX =
			nMeasureDpiDX
		//	- rcOuterPartExtents.left
		//	- rcOuterPartExtents.right
			- m_rcPageHeaderFooterGutters.left - m_rcPageHeaderFooterGutters.right;
		if( nInnerMeasureDpiDX <= 0 )
			throw 0;
		INT nInnerMeasureDpiDY =
			nMeasureDpiDY
		//	- rcOuterPartExtents.top
		//	- rcOuterPartExtents.bottom
			- m_rcPageHeaderFooterGutters.top - m_rcPageHeaderFooterGutters.bottom;
		if( nInnerMeasureDpiDY <= 0 )
			throw 0;
		listRowIndices.AddTail( 0L );
		nCurrentPageExtent = 0;
		for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		{
			LONG nRowHeight = arrRowHeights[ nRowNo ];
			nCurrentPageExtent += nRowHeight;
			if( nCurrentPageExtent > nInnerMeasureDpiDY )
			{
				nVertPageCount ++;
				if( nLastRowNo == nRowNo )
				{
					nCurrentPageExtent = 0;
					if( ( nRowNo + 1 ) < nRowCount )
					{
						listRowIndices.AddTail( nRowNo );
						listRowIndices.AddTail( nRowNo + 1 );
						nLastRowNo = nRowNo;
						continue;
					}
				}
				nCurrentPageExtent = nRowHeight;
				listRowIndices.AddTail( nRowNo - 1 );
				listRowIndices.AddTail( nRowNo );
				nLastRowNo = nRowNo;
			}
		} // for( nRowNo = 0; nRowNo < nRowCount; nRowNo ++ )
		listRowIndices.AddTail( nRowCount - 1 );

		nPageCount = nHorzPageCount * nVertPageCount;
		if( nPageCount > INT(USHRT_MAX) )
			throw 0;
		CRect rcVisibleRange( 0, 0, 2, nRowCount );
		if( ! MetafileCache_Init( nPageCount ) )
			throw 0;
		bMetafileCacheStarted = true;
		POSITION posVert = listRowIndices.GetHeadPosition();
		for( nPageIndex = 0; nPageIndex < nPageCount; nPageIndex ++ )
		{
			nRowOffset = m_rcPageHeaderFooterGutters.top;
			if( (nPageIndex%nHorzPageCount) == 0 )
			{
				nRowNoFrom = listRowIndices.GetNext( posVert );
				nRowNoTo = listRowIndices.GetNext( posVert );
			}
			CExtSafeString strMetafileName;
			if( MetafileCache_DiskModeGet() )
				strMetafileName = MetafileCache_GetUniqueName( nPageIndex );
			CMetaFileDC dcMetafile;
			if( m_bUseEnhancedMetafiles )
			{
				if( ! dcMetafile.CreateEnhanced(
						pDC, // NULL,
						( ! strMetafileName.IsEmpty() )
							? LPCTSTR(strMetafileName)
							: NULL
							,
						rcEmfExtent,
						NULL
						)
					)
				{
					ASSERT( FALSE );
					throw 0;
				}
			}
			else
			{
				if( ! dcMetafile.Create(
						( ! strMetafileName.IsEmpty() )
							? LPCTSTR(strMetafileName)
							: NULL
						)
					)
				{
					ASSERT( FALSE );
					throw 0;
				}
			}
			dcMetafile.SetAttribDC( pDC->GetSafeHdc() );
			dcMetafile.SetBkMode( TRANSPARENT );
			dcMetafile.SaveDC();
			OnPrepareDC( &dcMetafile, pInfo );
			bool bProgressAllowsToContinue = false;
			for( nRowNo = nRowNoFrom; nRowNo <= nRowNoTo; nRowNo ++ )
			{
				bProgressAllowsToContinue = 
					_Internal_Notify_OnPreparePrinting_Progress(
						1,              // nPassNo
						2,              // nPassCount
						nPageIndex,     // nPageNo
						nPageCount,     // nPageCount
						nHorzPageCount, // nContentWidthInPages
						nRowNo,         // nDocumentItemNo
						nDocItemCount,  // nDocumentItemCount
						nRowNoFrom,     // nInPageDocumentItemFirstNo
						nRowNoTo        // nInPageDocumentItemLastNo
						);
				if( ! bProgressAllowsToContinue )
					break;
				LONG nRowHeight = arrRowHeights[ nRowNo ];
				CExtPropertyItem * pPropertyItem = arrPrintableItems[nRowNo];
				ASSERT_VALID( pPropertyItem );
				CExtPropertyCategory * pPropertyCategory = DYNAMIC_DOWNCAST( CExtPropertyCategory, pPropertyItem );
				CFont & _font = ( pPropertyCategory != NULL ) ? g_PaintManager->m_FontBold : g_PaintManager->m_FontNormal;
				CFont * pOldFont = dcMetafile.SelectObject( &_font );
				LPCTSTR strName = pPropertyItem->NameGet();
				if( pPropertyCategory != NULL )
				{
					INT nNameLen = 0;
					if( strName != NULL && ( nNameLen = INT( _tcslen( strName ) ) ) > 0 )
					{
						CRect rcCategoryName(
							0,
							nRowOffset,
							nCategoryWidth,
							nRowOffset + nRowHeight
							);
						INT nIndentExt = OnCalcPrintableTreeItemIndent( *pDC, arrPrintableItems[nRowNo] );
						rcCategoryName.left += nIndentExt;
						OnPreparePrinting_RenderCategoryName(
							pInfo,
							arrPrintableItems,
							arrPrintableParents,
							arrColumnWidths,
							arrRowHeights,
							pWndActiveGrid,
							nPageIndex,
							nRowNoFrom,
							nRowNoTo,
							nRowNo,
							dcMetafile,
							pPropertyCategory,
							rcCategoryName,
							strName,
							nIndentExt
							);
					} // if( strName != NULL && _tcslen( strName ) > 0 )
				} // if( pPropertyCategory != NULL )
				else
				{
					INT nNameLen = 0;
					if( strName != NULL && ( nNameLen = INT( _tcslen( strName ) ) ) > 0 )
					{
						CRect rcName(
							0,
							nRowOffset,
							arrColumnWidths[ 0 ],
							nRowOffset + nRowHeight
							);
						INT nIndentExt = OnCalcPrintableTreeItemIndent( *pDC, arrPrintableItems[nRowNo] );
						rcName.left += nIndentExt;
						OnPreparePrinting_RenderPropertyItemCaption(
							pInfo,
							arrPrintableItems,
							arrPrintableParents,
							arrColumnWidths,
							arrRowHeights,
							pWndActiveGrid,
							nPageIndex,
							nRowNoFrom,
							nRowNoTo,
							nRowNo,
							dcMetafile,
							pPropertyItem,
							rcName,
							strName,
							nIndentExt
							);
					} // if( strName != NULL && _tcslen( strName ) > 0 )
					CExtGridCell * pCell = pPropertyItem->ValueActiveGet();
					if( pCell != NULL )
					{
						ASSERT_VALID( pCell );
						CRect rcCell(
							arrColumnWidths[ 0 ],
							nRowOffset,
							arrColumnWidths[ 0 ] + arrColumnWidths[ 1 ],
							nRowOffset + nRowHeight
							);
						CRect rcCellExtra = rcCell;
						DWORD dwAreaFlags = __EGBWA_INNER_CELLS;
						DWORD dwHelperPaintFlags = dwHelperPaintFlagsBasic|__EGCPF_METAFILE;
						OnPreparePrinting_RenderSingleCellAtCenter(
							pInfo,
							arrPrintableItems,
							arrPrintableParents,
							arrColumnWidths,
							arrRowHeights,
							pWndActiveGrid,
							nPageIndex,
							nRowNoFrom,
							nRowNoTo,
							pCell,
							dcMetafile,
							nColNo,
							nRowNo,
							rcCellExtra,
							rcCell,
							rcVisibleRange,
							dwAreaFlags,
							dwHelperPaintFlags
							);
					} // if( pCell != NULL )
				} // else from if( pPropertyCategory != NULL )
				dcMetafile.SelectObject( pOldFont );
				nRowOffset += nRowHeight;
			} // for( nRowNo = nRowNoFrom; nRowNo <= nRowNoTo; nRowNo ++ )
			
			CRect rcEntirePage( 0, 0, nMeasureDpiDX, nMeasureDpiDY );
			OnDrawPageHeaderFooterGutters( dcMetafile, rcEntirePage, m_rcPageHeaderFooterGutters, nPageIndex, nPageCount, pInfo );

			dcMetafile.RestoreDC( -1 );
			if( ! bProgressAllowsToContinue )
				throw 0;
			HANDLE hMetafile =
				m_bUseEnhancedMetafiles
					? ( (HANDLE) dcMetafile.CloseEnhanced() )
					: ( (HANDLE) dcMetafile.Close() )
					;
			if( ! MetafileCache_InitAt(
					nPageIndex,
					hMetafile,
					( ! strMetafileName.IsEmpty() )
						? LPCTSTR(strMetafileName)
						: NULL
					)
				)
				throw 0;
		} // for( nPageIndex = 0; nPageIndex < nPageCount; nPageIndex ++ )
		WORD nPageNoMin = WORD(1), nPageNoMax = WORD( WORD(nPageCount) + WORD(nPageNoMin) - WORD(1) );
		pInfo->m_nNumPreviewPages = nPageNoMax;
		pInfo->m_pPD->m_pd.nMinPage = nPageNoMin;
		pInfo->m_pPD->m_pd.nFromPage = nPageNoMin;
		pInfo->m_pPD->m_pd.nMaxPage = nPageNoMax;
		pInfo->m_pPD->m_pd.nToPage = nPageNoMax;
		bOK = true;
	} // try
	catch( CException * pException )
	{
		pException->Delete();
	} // catch( CException * pException )
	catch( ... )
	{
	} // catch( ... )
	pDC->RestoreDC( -1 );
	if( bMetafileCacheStarted )
		MetafileCache_InitComplete( bOK );
	OnPreparePrinting_End( ! bOK );
	_WaitCursor;
	if( ! bOK )
		return FALSE;
	else
		return TRUE;
}

BOOL CExtPPVW < CExtPropertyGridCtrl > :: PreTranslateMessage( MSG * pMsg )
{
	if( m_pWndPrintPreviewContainer->GetSafeHwnd() != NULL )
	{
		ASSERT_VALID( m_pWndPP );
		if( m_pWndPrintPreviewContainer->PreTranslateMessage( pMsg ) )
			return TRUE;
	}
	if( pMsg->message == WM_MOUSEWHEEL )
	{
		if( m_pWndPrintPreviewContainer->GetSafeHwnd() != NULL )
		{
			m_pWndPrintPreviewContainer->PreTranslateMessage( pMsg );
			return TRUE;
		} // if( m_pWndPrintPreviewContainer->GetSafeHwnd() != NULL )
	} // if( pMsg->message == WM_MOUSEWHEEL )
	return CExtPropertyGridCtrl::PreTranslateMessage( pMsg );
}

BOOL CExtPPVW < CExtPropertyGridCtrl > :: OnCmdMsg(
	UINT nID,
	int nCode,
	void * pExtra,
	AFX_CMDHANDLERINFO * pHandlerInfo
	)
{
	ASSERT_VALID( this );
	switch( nID )
	{
	case ID_EXT_PPV_SETUP:
	case ID_FILE_PRINT_SETUP:
		switch( nCode )
		{
		case CN_COMMAND:
			CExtPaintManager::stat_PassPaintMessages();
			if( IsPrintPreviewAvailable() )
			{
				if(		IsPrintPreviewMode()
					&&	m_pWndPP->GetSafeHwnd() != NULL
					)
					m_pWndPP->SendMessage( WM_COMMAND, ID_FILE_PRINT_SETUP );
				else
					friendly_app_t::_InvokeOnFilePrintSetup();
			}
		break;
		case CN_UPDATE_COMMAND_UI:
			ASSERT( pExtra != NULL );
			((CCmdUI*)pExtra)->Enable( ( IsPrintPreviewAvailable() && (! IsPrintPreviewMode() ) ) ? TRUE : FALSE );
		break;
		} // switch( nCode )
		return TRUE; // case ID_FILE_PRINT_SETUP

	case ID_EXT_PPV_PRINT:
	case ID_EXT_PPV_QUICK_PRINT:
	case ID_FILE_PRINT:
	case ID_FILE_PRINT_DIRECT:
		switch( nCode )
		{
		case CN_COMMAND:
			CExtPaintManager::stat_PassPaintMessages();
			if( IsPrintPreviewAvailable() )
			{
				if( m_pWndPP->GetSafeHwnd() != NULL )
					m_pWndPP->OnPreviewClose();
				DoPrintPreview(
					false,
					( nID == ID_FILE_PRINT_DIRECT ) ? true : false
					);
			}
		break;
		case CN_UPDATE_COMMAND_UI:
			{
				bool bEnabled = IsPrintPreviewAvailable();
				if( bEnabled )
				{
					CExtPropertyStore * pPS = PropertyStoreGet();
					if( pPS == NULL )
						bEnabled = false;
					else
					{
						ASSERT_VALID( pPS );
						if( pPS->ItemGetCount() == 0 )
							bEnabled = false;
					}
				}
				ASSERT( pExtra != NULL );
				((CCmdUI*)pExtra)->Enable( bEnabled ? TRUE : FALSE );
			}
		break;
		} // switch( nCode )
		return TRUE; // case ID_FILE_PRINT & ID_FILE_PRINT_DIRECT

	case ID_EXT_PPV_PRINT_PREVIEW:
	case ID_FILE_PRINT_PREVIEW:
		switch( nCode )
		{
		case CN_COMMAND:
			CExtPaintManager::stat_PassPaintMessages();
			if( IsPrintPreviewAvailable() )
			{
				if(		m_bPrintPreviewCheckMarkCmdStyle
					&&	m_pWndPP->GetSafeHwnd() != NULL
					)
					m_pWndPP->OnPreviewClose();
				else
					DoPrintPreview( true );
			}
		break;
		case CN_UPDATE_COMMAND_UI:
			ASSERT( pExtra != NULL );
			if( ! IsPrintPreviewAvailable() )
			{
				((CCmdUI*)pExtra)->SetCheck( 0 );
				((CCmdUI*)pExtra)->Enable( FALSE );
			}
			else
			{
				bool bEnabled = m_bPrintPreviewEnabled;
				if( bEnabled )
				{
					CExtPropertyStore * pPS = PropertyStoreGet();
					if( pPS == NULL )
						bEnabled = false;
					else
					{
						ASSERT_VALID( pPS );
						if( pPS->ItemGetCount() == 0 )
							bEnabled = false;
					}
				}
				((CCmdUI*)pExtra)->Enable( bEnabled ? TRUE : FALSE );
				if( m_bPrintPreviewCheckMarkCmdStyle )
					((CCmdUI*)pExtra)->SetCheck(
						( IsPrintPreviewMode() != NULL ) ? 1 : 0
						);
				else
					((CCmdUI*)pExtra)->SetCheck( 0 );
			}
		break;
		} // switch( nCode )
		return TRUE; // case ID_FILE_PRINT_PREVIEW

	} // switch( nID )
	return CExtPropertyGridCtrl::OnCmdMsg( nID, nCode, pExtra, pHandlerInfo );
}

bool CExtPPVW < CExtPropertyGridCtrl > :: DoReGenerateReport()
{
	ASSERT_VALID( this );
	if( ! IsPrintPreviewMode() )
		return false;
	ASSERT_VALID( m_pWndPP );
CExtPropertyStore * pPS = PropertyStoreGet();
	if( pPS == NULL )
	{
		m_pWndPP->OnPreviewClose();
		return true;
	}
	ASSERT_VALID( pPS );
	if( pPS->ItemGetCount() == 0 )
	{
		m_pWndPP->OnPreviewClose();
		return true;
	}
	return CExtPPVW_Printable::DoReGenerateReport();
}

bool CExtPPVW < CExtPropertyGridCtrl > :: OnPgcCreateBars()
{
	ASSERT_VALID( this );
	if( ! CExtPropertyGridCtrl::OnPgcCreateBars() )
		return false;
CWnd * pWnd = GetChildByRTC( RUNTIME_CLASS(CExtPropertyGridToolBar) );
	if( pWnd != NULL )
	{
		ASSERT_VALID( pWnd );
		CExtPropertyGridToolBar * pToolBar = STATIC_DOWNCAST( CExtPropertyGridToolBar, pWnd );
		VERIFY( pToolBar->InsertButton( -1 ) );
		VERIFY( pToolBar->InsertButton( -1, ID_EXT_PPV_QUICK_PRINT ) );
		VERIFY( pToolBar->InsertButton( -1, ID_EXT_PPV_PRINT_PREVIEW ) );
	}
	return true;
}

void CExtPPVW < CExtPropertyGridCtrl > :: PreSubclassWindow()
{
	CExtPropertyGridCtrl::PreSubclassWindow();
	ASSERT( ! m_strCommandProfile.IsEmpty() );
	VERIFY(
		g_CmdManager->UpdateFromToolBar(
			LPCTSTR( m_strCommandProfile ),
			IDR_EXT_TOOLBAR_PPW
			)
		);
}

LRESULT CExtPPVW < CExtPropertyGridCtrl > :: WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	switch( message )
	{
	case 0x0363: // WM_IDLEUPDATECMDUI
		if( m_pWndPP->GetSafeHwnd() != NULL )
			return m_pWndPP->SendMessage( message, wParam, lParam );
		return 0;
	case WM_SETFOCUS:
		if( m_pWndPrintPreviewContainer->GetSafeHwnd() != NULL )
		{
			ASSERT_VALID( m_pWndPP );
			m_pWndPrintPreviewContainer->SetFocus();
			return 0;
		} // if( m_pWndPrintPreviewContainer->GetSafeHwnd() != NULL )
	break; // case WM_SETFOCUS
	case WM_CONTEXTMENU:
		if( m_pWndPrintPreviewContainer->GetSafeHwnd() != NULL )
			return 0L;
	break; // case WM_CONTEXTMENU
	} // switch( message )
LRESULT lResult = CExtPropertyGridCtrl::WindowProc( message, wParam, lParam );
	switch( message )
	{
	case WM_SIZE:
		if( m_pWndPrintPreviewContainer->GetSafeHwnd() != NULL )
		{
			ASSERT_VALID( m_pWndPP );
			CRect rcClient;
			CWnd::GetClientRect( &rcClient );
			if( m_pWndPP->GetSafeHwnd() != NULL )
				m_pWndPP->_PpvUpdateBars();
			m_pWndPrintPreviewContainer->SetWindowPos(
				&CWnd::wndTop, 0, 0, 0, 0,
				SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE
				);
			CWnd::RepositionBars(
				AFX_IDW_PANE_FIRST - 1,
				AFX_IDW_PANE_FIRST + 1,
				AFX_IDW_PANE_FIRST,
				CWnd::reposDefault,
				&rcClient,
				&rcClient,
				TRUE
				);
		} // if( m_pWndPrintPreviewContainer->GetSafeHwnd() != NULL )
	break; // case WM_SIZE
	} // switch( message )
	return lResult;
}

#endif // (!defined __EXT_MFC_NO_PROPERTYGRIDWND)

#endif // __EXT_MFC_NO_PRINT


