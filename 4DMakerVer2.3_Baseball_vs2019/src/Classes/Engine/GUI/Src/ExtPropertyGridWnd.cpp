// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_PROPERTYGRIDWND)

#if (!defined __EXTPROPERTYGRIDWND_H)
	#include <ExtPropertyGridWnd.h>
#endif

#if (!defined __EXT_RICH_CONTENT_H)
	#include <ExtRichContent.h>
#endif // (!defined __EXT_RICH_CONTENT_H)

#if (!defined __ExtCmdManager_H)
	#include <ExtCmdManager.h>
#endif

#if (!defined __EXT_MFC_NO_TAB_CONTROLBARS)
	#if (!defined __EXT_CONTROLBAR_TABBED_FEATURES_H)
		#include "ExtControlBarTabbedFeatures.h"
	#endif // __EXT_CONTROLBAR_TABBED_FEATURES_H
#endif // (!defined __EXT_MFC_NO_TAB_CONTROLBARS)

#if (!defined __EXT_MEMORY_DC_H)
	#include "ExtMemoryDC.h"
#endif

#if (!defined __EXT_LOCALIZATION_H)
	#include <../Src/ExtLocalization.h>
#endif

#include <Resources/Resource.h>

IMPLEMENT_SERIAL( CExtPropertyGridCellArea, CExtGridCellStringDM, VERSIONABLE_SCHEMA|1 );

IMPLEMENT_SERIAL( CExtPropertyGridCellCompound, CExtGridCellString, VERSIONABLE_SCHEMA|1 );

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyStore

IMPLEMENT_SERIAL( CExtPropertyItem, CObject, VERSIONABLE_SCHEMA|1 );

CExtPropertyItem::CExtPropertyItem(
	__EXT_MFC_SAFE_LPCTSTR strName // = NULL
	)
	: m_pParent( NULL )
	, m_strName( ( strName == NULL ) ? _T("") : strName )
	, m_bExpanded( true )
	, m_bInputEnabled( true )
	, m_lParam( 0L )
	, m_nHeightPx( -1L )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

CExtPropertyItem::~CExtPropertyItem()
{
}

CExtPropertyItem * CExtPropertyItem::BrowseNext(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper,
	bool bIncludeHidden
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( (! bSiblingOnly )
		&& bWalkDeeper
		&& ItemGetCount() > 0
		&& ( (! bExpandedWalk ) || ExpandedGet() )
		)
		return ItemGetAt( 0 );
CExtPropertyItem * pNextPI = ItemGetNext();
	if( pNextPI != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pNextPI );
		if(		(!bIncludeHidden)
			&&	(!pNextPI->CanBeInsertedIntoPropertyGrid())
			)
			return pNextPI->BrowseNext( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
		else
			return pNextPI;
	}
	if( bSiblingOnly )
		return NULL;
	if( ! bExpandedWalk )
		return NULL;
CExtPropertyItem * pParentPI = ItemParentGet();
	if(		pParentPI == NULL
		||	pParentPI->ItemParentGet() == NULL
		)
		return NULL;
CExtPropertyItem * pNI = pParentPI;
	for( ; pNI != NULL; )
	{
		CExtPropertyItem * pNIX = pNI->BrowseNext( true, false, false, bIncludeHidden );
		if( pNIX != NULL )
		{
			if(		(!bIncludeHidden)
				&&	(!pNIX->CanBeInsertedIntoPropertyGrid())
				)
				return pNIX->BrowseNext( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
			else
				return pNIX;
		}
		pNI = pNI->ItemParentGet();
		if(		pParentPI == NULL
//			||	pParentPI->ItemParentGet() == NULL
			)
			return NULL;
	}
	return NULL;
}

const CExtPropertyItem * CExtPropertyItem::BrowseNext(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper,
	bool bIncludeHidden
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyItem * > ( this ) )
		-> BrowseNext( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
}

CExtPropertyItem * CExtPropertyItem::BrowsePrev(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper,
	bool bIncludeHidden
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bWalkDeeper;
CExtPropertyItem * pPrevPI = ItemGetPrev();
	if( pPrevPI != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pPrevPI );
		if( bSiblingOnly )
		{
			if(		(!bIncludeHidden)
				&&	(!pPrevPI->CanBeInsertedIntoPropertyGrid())
				)
				return pPrevPI->BrowsePrev( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
			else
				return pPrevPI;
		}
		CExtPropertyItem * pNI = pPrevPI;
		for( ; pNI->ItemGetCount() > 0; )
		{
			if( bExpandedWalk && (! pNI->ExpandedGet() )  )
			{
				if(		(!bIncludeHidden)
					&&	(!pNI->CanBeInsertedIntoPropertyGrid())
					)
					return pNI->BrowsePrev( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
				else
					return pNI;
			}
			pNI = pNI->ItemGetAt( pNI->ItemGetCount() - 1 );
			__EXT_DEBUG_GRID_ASSERT_VALID( pNI );
		}
		return pNI;
	}
	if( bSiblingOnly )
		return NULL;
CExtPropertyItem * pParentPI = ItemParentGet();
	if(		pParentPI == NULL
		||	pParentPI->ItemParentGet() == NULL
		)
		return NULL;
	if(		(!bIncludeHidden)
		&&	pParentPI != NULL
		&&	(!pParentPI->CanBeInsertedIntoPropertyGrid())
		)
		return pParentPI->BrowsePrev( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
	else
		return pParentPI;
}

const CExtPropertyItem * CExtPropertyItem::BrowsePrev(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper,
	bool bIncludeHidden
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyItem * > ( this ) )
		-> BrowsePrev( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
}

bool CExtPropertyItem::CanBeInsertedIntoPropertyGrid() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return true;
}

void CExtPropertyItem::Delete()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	delete this;
}

void CExtPropertyItem::DeleteChildItem(
	CExtPropertyItem * pChildItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pChildItem );
	pChildItem->ItemParentSet( NULL );
	pChildItem->Delete();
}

bool CExtPropertyItem::ChildrenHaveDifferentDefaultValues() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	if( nCount <= 1 )
		return false;
	if( ItemGetRefAt(0).IsKindOf(RUNTIME_CLASS(CExtPropertyCategory)) )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return true;
	}
const CExtGridCell * pValueCmp = ItemGetRefAt(0).ValueDefaultGet();
	if( pValueCmp == NULL )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return true;
	}
	for( nIndex = 1; nIndex < nCount; nIndex++ )
	{
		if( ItemGetRefAt(nIndex).IsKindOf(RUNTIME_CLASS(CExtPropertyCategory)) )
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return true;
		}
		const CExtGridCell * pValue = ItemGetRefAt(nIndex).ValueDefaultGet();
		if( pValue == NULL )
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return true;
		}
		if( pValueCmp->CompareEx( CExtGridCell::__EGCCT_PROPERTY_GRID_EMPTY_MIXED_STATE_DETECTION, *pValue, __EGCS_CHECKED, 0 ) != 0 )
			return true;
	}
	return false;
}

bool CExtPropertyItem::ChildrenHaveDifferentActiveValues() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	if( nCount <= 1 )
		return false;
	if( ItemGetRefAt(0).IsKindOf(RUNTIME_CLASS(CExtPropertyCategory)) )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return true;
	}
const CExtGridCell * pValueCmp = ItemGetRefAt(0).ValueActiveGet();
	if( pValueCmp == NULL )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return true;
	}
	for( nIndex = 1; nIndex < nCount; nIndex++ )
	{
		if( ItemGetRefAt(nIndex).IsKindOf(RUNTIME_CLASS(CExtPropertyCategory)) )
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return true;
		}
		const CExtGridCell * pValue = ItemGetRefAt(nIndex).ValueActiveGet();
		if( pValue == NULL )
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return true;
		}
		if( pValueCmp->CompareEx( CExtGridCell::__EGCCT_PROPERTY_GRID_EMPTY_MIXED_STATE_DETECTION, *pValue, __EGCS_CHECKED, 0 ) != 0 )
			return true;
	}
	return false;
}

void CExtPropertyItem::Combine(
	CExtPropertyItem * pOtherItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pOtherItem );
	pOtherItem;
	__EXT_DEBUG_GRID_ASSERT( FALSE ); // must not be invoked
}

void CExtPropertyItem::AdjustIntersection(
	INT nIntersectionItemCount
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
 	nIntersectionItemCount;
 	__EXT_DEBUG_GRID_ASSERT( FALSE ); // must not be invoked
}

#ifdef _DEBUG

void CExtPropertyItem::AssertValid() const
{
	CObject::AssertValid();
	if( m_pParent != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pParent );
	}
}

void CExtPropertyItem::Dump( CDumpContext & dc ) const
{
	CObject::Dump( dc );
}

#endif

ULONG CExtPropertyItem::Enum(
	IPropertyItemEnumSite * pEnumSite,
	LPVOID pCookie, // = NULL
	bool bEnumThisItem, // = false
	bool bEnumChilrenItems, // = true
	bool * p_bCancelWalking // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( pEnumSite != NULL );
	if( p_bCancelWalking != NULL )
		(*p_bCancelWalking) = false;
ULONG nEnumeratedCount = 0;
	if( bEnumThisItem )
	{
		if( ! pEnumSite->OnPropertyItemEnum(
				this,
				pCookie
				)
			)
		{
			if( p_bCancelWalking != NULL )
				(*p_bCancelWalking) = true;
			return nEnumeratedCount;
		}
		nEnumeratedCount ++;
	} // if( bEnumThisItem )
	if( bEnumChilrenItems )
	{
		INT nIndex, nCount = ItemGetCount();
		for( nIndex = 0; nIndex < nCount; nIndex++ )
		{
			CExtPropertyItem * pItem =
				ItemGetAt( nIndex );
			__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
			if( ! pEnumSite->OnPropertyItemEnum(
					pItem,
					pCookie
					)
				)
			{
				if( p_bCancelWalking != NULL )
					(*p_bCancelWalking) = true;
				return nEnumeratedCount;
			}
			nEnumeratedCount ++;
			bool bCancelWalking = false;
			nEnumeratedCount +=
				pItem->Enum(
					pEnumSite,
					pCookie,
					false,
					true,
					&bCancelWalking
					);
			if( bCancelWalking )
			{
				if( p_bCancelWalking != NULL )
					(*p_bCancelWalking) = true;
				return nEnumeratedCount;
			}
		} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	} // if( bEnumChilrenItems )
	return nEnumeratedCount;
}

INT CExtPropertyItem::QueryHeightPx(
	const CExtPropertyGridWnd * pPGW
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
	__EXT_DEBUG_GRID_ASSERT( pPGW->GetSafeHwnd() != NULL );
	pPGW;
INT nHeightPx = HeightPxGet();
	return nHeightPx;
}

INT CExtPropertyItem::HeightPxGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_nHeightPx;
}

void CExtPropertyItem::HeightPxSet( INT nHeightPx )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_nHeightPx = nHeightPx;
}

LPARAM CExtPropertyItem::LPARAM_Get() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_lParam;
}

void CExtPropertyItem::LPARAM_Set( LPARAM lParam )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_lParam = lParam;
}

bool CExtPropertyItem::ExpandedGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_bExpanded;
}

void CExtPropertyItem::ExpandedSet( bool bExpanded )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_bExpanded = bExpanded;
}

bool CExtPropertyItem::OnInputEnabledGet( bool bUseNestedFlags ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ! InputEnabledGet() )
		return false;
//	if( IsKindOf( RUNTIME_CLASS( CExtPropertyValueMixed ) ) )
//		bUseNestedFlags = true;
	if( bUseNestedFlags )
	{
		const CExtPropertyItem * pPropertyItem = ItemParentGet();
		if( pPropertyItem != NULL )
		{
			if( ! pPropertyItem->OnInputEnabledGet( true ) )
				return false;
		} // if( pPropertyItem != NULL )
	} // if( bUseNestedFlags )
	return true;
}

void CExtPropertyItem::OnInputEnabledSet( bool bUseNestedFlags, bool bInputEnabled )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	InputEnabledSet( bInputEnabled );
	if( bUseNestedFlags )
	{
		INT nIndex, nCount = ItemGetCount();
		for( nIndex = 0; nIndex < nCount; nIndex++ )
		{
			CExtPropertyItem & _item = ItemGetRefAt( nIndex );
			_item.OnInputEnabledSet( true, bInputEnabled );
		} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	} // if( bUseNestedFlags )
}

bool CExtPropertyItem::InputEnabledGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_bInputEnabled;
}

void CExtPropertyItem::InputEnabledSet( bool bInputEnabled )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_bInputEnabled = bInputEnabled;
}

__EXT_MFC_SAFE_LPCTSTR CExtPropertyItem::NameGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ((__EXT_MFC_SAFE_LPCTSTR)( m_strName.IsEmpty() ? _T("") : ((LPCTSTR)(m_strName)) ));
}

void CExtPropertyItem::NameSet( __EXT_MFC_SAFE_LPCTSTR strName )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_strName = ( strName == NULL ) ? _T("") : strName;
}

void CExtPropertyItem::ValueDefaultFromActive()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( FALSE ); // must not be invoked
}

void CExtPropertyItem::ValueActiveFromDefault()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( FALSE ); // must not be invoked
}

const CExtGridCell & CExtPropertyItem::ValueDefaultGetRef() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtGridCell * pValue = ValueDefaultGet();
	__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
	return (*pValue);
}

CExtGridCell & CExtPropertyItem::ValueDefaultGetRef()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridCell * pValue = ValueDefaultGet();
	__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
	return (*pValue);
}

const CExtGridCell * CExtPropertyItem::ValueDefaultGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyItem * > ( this ) )
			-> ValueDefaultGet();
}

CExtGridCell * CExtPropertyItem::ValueDefaultGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return NULL;
}

CExtGridCell * CExtPropertyItem::ValueDefaultGetByRTC(
	CRuntimeClass * pInitRTC
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( pInitRTC != NULL );
	try
	{
		CExtGridCell * pValue =
			(CExtGridCell*)pInitRTC->CreateObject();
		__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
		__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtGridCell, pValue );
		__EXT_DEBUG_GRID_VERIFY( ValueDefaultSet( pValue, false ) );
	}
	catch( ... )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
	}
	return ValueDefaultGet();
}

bool CExtPropertyItem::ValueDefaultSet(
	CExtGridCell * pValue,
	bool bClone // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	pValue;
	bClone;
	__EXT_DEBUG_GRID_ASSERT( FALSE ); // must not be invoked
	return false;
}

const CExtGridCell & CExtPropertyItem::ValueActiveGetRef() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtGridCell * pValue = ValueActiveGet();
	__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
	return (*pValue);
}

CExtGridCell & CExtPropertyItem::ValueActiveGetRef()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridCell * pValue = ValueActiveGet();
	__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
	return (*pValue);
}

const CExtGridCell * CExtPropertyItem::ValueActiveGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyItem * > ( this ) )
			-> ValueActiveGet();
}

CExtGridCell * CExtPropertyItem::ValueActiveGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return NULL;
}

CExtGridCell * CExtPropertyItem::ValueActiveGetByRTC(
	CRuntimeClass * pInitRTC
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( pInitRTC != NULL );
	try
	{
		CExtGridCell * pValue =
			(CExtGridCell*)pInitRTC->CreateObject();
		__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
		__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtGridCell, pValue );
		__EXT_DEBUG_GRID_VERIFY( ValueActiveSet( pValue, false ) );
	}
	catch( ... )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
	}
	return ValueActiveGet();
}

bool CExtPropertyItem::ValueActiveSet(
	CExtGridCell * pValue,
	bool bClone // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	pValue;
	bClone;
	__EXT_DEBUG_GRID_ASSERT( FALSE ); // must not be invoked
	return false;
}

void CExtPropertyItem::Apply(
	CExtGridCell * pValue // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( pValue != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
	}
#endif // _DEBUG
	if( pValue == NULL )
		return;
CExtGridCell * pOwnValue = ValueActiveGet();
	if( pOwnValue == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pOwnValue );
	if( LPVOID(pOwnValue) == LPVOID(pValue) )
		return;
	pOwnValue->Assign( *pValue );
	pOwnValue->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

__EXT_MFC_SAFE_LPCTSTR CExtPropertyItem::DescriptionGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_strDescription.IsEmpty() ? _T("") : LPCTSTR(m_strDescription);
}

void CExtPropertyItem::DescriptionSet(
	__EXT_MFC_SAFE_LPCTSTR strDescription
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_strDescription = (strDescription == NULL) ? _T("") : strDescription;
}

const CExtPropertyStore & CExtPropertyItem::GetStoreRef() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtPropertyStore * pStore = GetStore();
	__EXT_DEBUG_GRID_ASSERT_VALID( pStore );
	__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtPropertyStore, pStore );
	return (*pStore);
}

CExtPropertyStore & CExtPropertyItem::GetStoreRef()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyStore * pStore = GetStore();
	__EXT_DEBUG_GRID_ASSERT_VALID( pStore );
	__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtPropertyStore, pStore );
	return (*pStore);
}

const CExtPropertyStore * CExtPropertyItem::GetStore() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyItem * > ( this ) )
		-> GetStore();
}

CExtPropertyStore * CExtPropertyItem::GetStore()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyItem * pItem = this;
	for( ; true; )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
		CExtPropertyItem * pParentItem =
			pItem->ItemParentGet(); // fix by Maurizio Pesce
		if( pParentItem != NULL )
		{
			pItem = pParentItem;
			continue;
		}
		CExtPropertyStore * pStore =
			DYNAMIC_DOWNCAST(
				CExtPropertyStore,
				pItem
				);
		return pStore;
	} // for( ; true; )
	return NULL;
}

const CExtPropertyItem & CExtPropertyItem::ItemParentGetRef() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtPropertyItem * pItem = ItemParentGet();
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	return (*pItem);
}

CExtPropertyItem & CExtPropertyItem::ItemParentGetRef()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyItem * pItem = ItemParentGet();
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	return (*pItem);
}

const CExtPropertyItem * CExtPropertyItem::ItemParentGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyItem * > ( this ) )
		-> ItemParentGet();
}

CExtPropertyItem * CExtPropertyItem::ItemParentGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return  m_pParent;
}

void CExtPropertyItem::ItemParentSet(
	CExtPropertyItem * pItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_pParent = pItem;
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

INT CExtPropertyItem::ItemGetCount() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return 0;
}

const CExtPropertyItem & CExtPropertyItem::operator [] ( INT nIndex ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ItemGetRefAt( nIndex );
}

CExtPropertyItem & CExtPropertyItem::operator [] ( INT nIndex )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ItemGetRefAt( nIndex );
}

const CExtPropertyItem * CExtPropertyItem::ItemGetNext() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyItem * > ( this ) )
		-> ItemGetNext();
}

CExtPropertyItem * CExtPropertyItem::ItemGetNext()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyItem * pParent = ItemParentGet();
	if( pParent == NULL )
		return NULL;
INT nOwnSiblingIndex = pParent->ItemGetIndexOf( this );
	__EXT_DEBUG_GRID_ASSERT( nOwnSiblingIndex >= 0 );
INT nSiblingCount = pParent->ItemGetCount();
	__EXT_DEBUG_GRID_ASSERT( nOwnSiblingIndex < nSiblingCount );
	if( nOwnSiblingIndex == (nSiblingCount-1) )
		return NULL;
CExtPropertyItem * pItem = pParent->ItemGetAt( nOwnSiblingIndex + 1 );
	return pItem;
}

const CExtPropertyItem * CExtPropertyItem::ItemGetPrev() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyItem * > ( this ) )
		-> ItemGetPrev();
}

CExtPropertyItem * CExtPropertyItem::ItemGetPrev()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyItem * pParent = ItemParentGet();
	if( pParent == NULL )
		return NULL;
INT nOwnSiblingIndex = pParent->ItemGetIndexOf( this );
	if( nOwnSiblingIndex <= 0 )
		return NULL;
CExtPropertyItem * pItem = pParent->ItemGetAt( nOwnSiblingIndex - 1 );
	return pItem;
}

const INT CExtPropertyItem::ItemGetIndexOf( const CExtPropertyItem * pChildItem ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( pChildItem == NULL )
		return -1;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		const CExtPropertyItem * pItem = ItemGetAt( nIndex );
		if( LPVOID(pItem) == LPVOID(pChildItem) )
			return nIndex;
	}
	return -1;
}

const INT CExtPropertyItem::ItemGetSiblingIndex() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtPropertyItem * pParent = ItemParentGet();
	if( pParent == NULL )
		return -1;
INT nIndex = pParent->ItemGetIndexOf( this );
	__EXT_DEBUG_GRID_ASSERT( nIndex >= 0 );
	return nIndex;
}

const CExtPropertyItem & CExtPropertyItem::ItemGetRefAt( INT nIndex ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtPropertyItem * pItem = ItemGetAt( nIndex );
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	return (*pItem);
}

CExtPropertyItem & CExtPropertyItem::ItemGetRefAt( INT nIndex )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyItem * pItem = ItemGetAt( nIndex );
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	return (*pItem);
}

const CExtPropertyItem * CExtPropertyItem::ItemGetAt( INT nIndex ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyItem * > ( this ) )
		-> ItemGetAt( nIndex );
}

CExtPropertyItem * CExtPropertyItem::ItemGetAt( INT nIndex )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nIndex;
	__EXT_DEBUG_GRID_ASSERT( FALSE ); // must not be invoked
	return NULL;
}

CExtPropertyItem * CExtPropertyItem::ItemGetByName(
	__EXT_MFC_SAFE_LPCTSTR strName,
	INT & nIndex,
	CRuntimeClass * pRTC // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		LPCTSTR(strName) == NULL
		||	_tcslen(LPCTSTR(strName)) == 0
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return NULL;
	}
INT nCount = ItemGetCount();
	if( nCount == 0 )
		return NULL;
	if( nIndex < 0 )
		nIndex = 0;
	else if( nIndex >= nCount )
		return NULL;
	else
		nIndex++;
	for( ; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem * pItem =
			ItemGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
		if(		pRTC != NULL
			&&	(! pItem->IsKindOf(pRTC) )
			)
			continue;
		__EXT_MFC_SAFE_LPCTSTR strNameCmp =
			pItem->NameGet();
		if(		LPCTSTR(strNameCmp) == NULL
			||	_tcslen(LPCTSTR(strNameCmp)) == 0
			)
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			continue;
		}
		if(	_tcscmp(
				LPCTSTR(strName),
				LPCTSTR(strNameCmp)
				) == 0
			)
			return pItem;
	} // for( ; nIndex < nCount; nIndex++ )
	return NULL;
}

const CExtPropertyItem * CExtPropertyItem::ItemGetByName(
	__EXT_MFC_SAFE_LPCTSTR strName,
	INT & nIndex,
	CRuntimeClass * pRTC // = NULL
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyItem * > ( this ) )
		-> ItemGetByName(
			strName,
			nIndex,
			pRTC
			);
}

CExtPropertyItem * CExtPropertyItem::ItemGetByName(
	__EXT_MFC_SAFE_LPCTSTR strName,
	CRuntimeClass * pRTC // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex = -1;
	return ItemGetByName( strName, nIndex, pRTC );
}

const CExtPropertyItem * CExtPropertyItem::ItemGetByName(
	__EXT_MFC_SAFE_LPCTSTR strName,
	CRuntimeClass * pRTC // = NULL
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyItem * > ( this ) )
		-> ItemGetByName(
			strName,
			pRTC
			);
}

bool CExtPropertyItem::ItemInsert(
	CExtPropertyItem * pItem,
	INT nIndexInsertBefore // = -1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	pItem;
	nIndexInsertBefore;
	__EXT_DEBUG_GRID_ASSERT( FALSE ); // must not be invoked
	return false;
}

bool CExtPropertyItem::ItemRemove(
	INT nIndex, // = 0
	INT nCount // = -1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nIndex;
	nCount;
	__EXT_DEBUG_GRID_ASSERT( FALSE ); // must not be invoked
	return false;
}

void CExtPropertyItem::OnGridRowInitialized(
	CExtPropertyGridWnd & wndPG,
	HTREEITEM hTreeItem,
	CExtGridCell * pCellCaption,
	CExtGridCell * pCellValue
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&wndPG) );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellCaption );
#ifdef _DEBUG
	if( pCellValue != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellValue );
	}
#endif // _DEBUG
	pCellCaption;
	pCellValue;
	if( wndPG.ItemIsExpanded( hTreeItem ) )
	{
		if( ! ExpandedGet() )
			wndPG.ItemExpand( hTreeItem, TVE_COLLAPSE );
	} // if( wndPG.ItemIsExpanded( hTreeItem ) )
	else
	{
		if( ExpandedGet() )
		{
			wndPG.ItemFocusLock( true );
			wndPG.ItemExpand( hTreeItem, TVE_EXPAND );
			wndPG.ItemFocusLock( false );
		}
	} // else from if( wndPG.ItemIsExpanded( hTreeItem ) )
	if( pCellValue != NULL )
	{
		pCellValue->ModifyStyle( 0, __EGCS_PRESSED );
		pCellValue->ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
		CSize sizePropCell = pCellValue->MeasureCell( &wndPG );
		INT nExt = 0;
		((CExtGridCell*)hTreeItem)->ExtentGet( nExt );
		if( nExt < sizePropCell.cy )
			((CExtGridCell*)hTreeItem)->ExtentSet( sizePropCell.cy );
	}
	wndPG._OnSynchronizeInputEnabledCheckMark( this, false );
}

void CExtPropertyItem::Serialize( CArchive & ar )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ar.IsStoring() )
	{
		ar << m_strName;
		ar << m_strDescription;
		DWORD dwTmp = DWORD( m_lParam );
		ar << dwTmp;
		dwTmp = DWORD(m_nHeightPx);
		ar << dwTmp;
		DWORD dwFlags = 0;
		CExtGridCell * pCellDefault = ValueDefaultGet(), * pCellActive = ValueActiveGet();
		if( pCellDefault != NULL && LPVOID(pCellDefault) != LPVOID(pCellActive) )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellDefault );
			dwFlags |= 0x00000001;
		}
		if( pCellActive != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellActive );
			dwFlags |= 0x00000002;
		}
		if( ExpandedGet() )
			dwFlags |= 0x00000004;
		if( InputEnabledGet() )
			dwFlags |= 0x00000008;
		ar << dwFlags;
		if( pCellDefault != NULL && LPVOID(pCellDefault) != LPVOID(pCellActive) )
		{
			CRuntimeClass * pRTC = pCellDefault->GetRuntimeClass();
			__EXT_DEBUG_GRID_ASSERT( pRTC != NULL );
			ar.WriteClass( pRTC );
			pCellDefault->Serialize( ar );
		}
		if( pCellActive != NULL )
		{
			CRuntimeClass * pRTC = pCellActive->GetRuntimeClass();
			__EXT_DEBUG_GRID_ASSERT( pRTC != NULL );
			ar.WriteClass( pRTC );
			pCellActive->Serialize( ar );
		}
		DWORD dwIndex, dwCount = ItemGetCount();
		ar << dwCount;
		for( dwIndex = 0; dwIndex < dwCount; dwIndex ++ )
		{
			CExtPropertyItem * pItem = ItemGetAt( INT(dwIndex) );
			__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
			__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtPropertyItem, pItem );
			CRuntimeClass * pRTC = pItem->GetRuntimeClass();
			__EXT_DEBUG_GRID_ASSERT( pRTC != NULL );
			ar.WriteClass( pRTC );
			pItem->Serialize( ar );
		} // for( dwIndex = 0; dwIndex < dwCount; dwIndex ++ )
	} // if( ar.IsStoring() )
	else
	{
		if( ItemGetCount() > 0 )
			ItemRemove();
		ar >> m_strName;
		ar >> m_strDescription;
		DWORD dwTmp;
		ar >> dwTmp;
		m_lParam = LPARAM(dwTmp);
		ar >> dwTmp;
		m_nHeightPx = INT(dwTmp);
		DWORD dwFlags;
		ar >> dwFlags;
		if( (dwFlags&0x00000001) != 0 )
		{
			CRuntimeClass * pRTC = ar.ReadClass();
			__EXT_DEBUG_GRID_ASSERT( pRTC != NULL );
			CExtGridCell * pCellDefault = (CExtGridCell*)pRTC->CreateObject();
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellDefault );
			__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtGridCell, pCellDefault );
			pCellDefault->Serialize( ar );
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellDefault );
			__EXT_DEBUG_GRID_VERIFY( ValueDefaultSet( pCellDefault ) );
		} // if( (dwFlags&0x00000001) != 0 )
		if( (dwFlags&0x00000002) != 0 )
		{
			CRuntimeClass * pRTC = ar.ReadClass();
			__EXT_DEBUG_GRID_ASSERT( pRTC != NULL );
			CExtGridCell * pCellActive = (CExtGridCell*)pRTC->CreateObject();
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellActive );
			__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtGridCell, pCellActive );
			pCellActive->Serialize( ar );
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellActive );
			__EXT_DEBUG_GRID_VERIFY( ValueActiveSet( pCellActive ) );
		} // if( (dwFlags&0x00000002) != 0 )
		if( (dwFlags&0x00000004) != 0 )
			ExpandedSet( true );
		else
			ExpandedSet( false );
		if( (dwFlags&0x00000008) != 0 )
			InputEnabledSet( true );
		else
			InputEnabledSet( false );
		DWORD dwIndex, dwCount;
		ar >> dwCount;
		for( dwIndex = 0; dwIndex < dwCount; dwIndex ++ )
		{
			CRuntimeClass * pRTC = ar.ReadClass();
			__EXT_DEBUG_GRID_ASSERT( pRTC != NULL );
			CExtPropertyItem * pItem = (CExtPropertyItem*)pRTC->CreateObject();
			__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
			__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtPropertyItem, pItem );
			pItem->Serialize( ar );
			__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
			__EXT_DEBUG_GRID_VERIFY( ItemInsert( pItem ) );
		} // for( dwIndex = 0; dwIndex < dwCount; dwIndex ++ )
	} // else from if( ar.IsStoring() )
}

void CExtPropertyItem::BuildLinearList(
	CTypedPtrList < CPtrList, CExtPropertyItem * > & _list,
	bool bIncludeThisItem,
	bool bAddToTail // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( bIncludeThisItem )
	{
		if( bAddToTail )
			_list.AddTail( this );
		else
			_list.AddHead( this );
	} // if( bIncludeThisItem )
	if( IsKindOf( RUNTIME_CLASS( CExtPropertyValueMixed ) ) )
		return;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtPropertyItem * pPropertyItem = ItemGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
		pPropertyItem->BuildLinearList( _list, true, bAddToTail );
	}
}

CExtSafeString CExtPropertyItem::BuildCompoundTextDefault() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
TCHAR strListSeparator[8];
	::memset( strListSeparator, 0, sizeof(strListSeparator) );
	if( g_ResourceManager->GetLocaleInfo(
			LOCALE_SLIST,
			strListSeparator,
			4
			) <= 0
		)
		__EXT_MFC_STRCPY( 
			strListSeparator, 
			sizeof(strListSeparator)/sizeof(strListSeparator[0]),
			_T(";") 
			);
CExtSafeString strCompound;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		const CExtPropertyItem & _item = ItemGetRefAt( nIndex );
		if( ! _item.CanBeInsertedIntoPropertyGrid() )
			continue;
		CExtSafeString str = _item.BuildCompoundTextDefault();
		if( ! strCompound.IsEmpty() )
		{
			strCompound += strListSeparator;
			strCompound += _T(' ');
		} // if( ! strCompound.IsEmpty() )
		strCompound += str;
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return strCompound;
}

CExtSafeString CExtPropertyItem::BuildCompoundTextActive() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
TCHAR strListSeparator[8];
	::memset( strListSeparator, 0, sizeof(strListSeparator) );
	if( g_ResourceManager->GetLocaleInfo(
			LOCALE_SLIST,
			strListSeparator,
			4
			) <= 0
		)
		__EXT_MFC_STRCPY( 
			strListSeparator, 
			sizeof(strListSeparator)/sizeof(strListSeparator[0]),
			_T(";") 
			);
CExtSafeString strCompound;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		const CExtPropertyItem & _item = ItemGetRefAt( nIndex );
		if( ! _item.CanBeInsertedIntoPropertyGrid() )
			continue;
		CExtSafeString str = _item.BuildCompoundTextActive();
		if( ! strCompound.IsEmpty() )
		{
			strCompound += strListSeparator;
			strCompound += _T(' ');
		} // if( ! strCompound.IsEmpty() )
		strCompound += str;
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return strCompound;
}

bool CExtPropertyItem::IsModified() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return false;
}

bool CExtPropertyItem::Reset()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return false;
}

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyValue

IMPLEMENT_SERIAL( CExtPropertyValue, CExtPropertyItem, VERSIONABLE_SCHEMA|1 );

CExtPropertyValue::CExtPropertyValue(
	__EXT_MFC_SAFE_LPCTSTR strName, // = NULL
	CExtGridCell * pValueActive, // = NULL
	CExtGridCell * pValueDefault, // = NULL
	bool bCloneActive, // = false
	bool bCloneDefault // = false
	)
	: CExtPropertyItem( strName )
	, m_pValueDefault( NULL )
	, m_pValueActive( NULL )
{
	ValueDefaultSet( pValueDefault, bCloneDefault );
	ValueActiveSet( pValueActive, bCloneActive );
	ExpandedSet( false );
}

CExtPropertyValue::~CExtPropertyValue()
{
	if( m_pValueDefault != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pValueDefault );
		delete m_pValueDefault;
	}
	if( m_pValueActive != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pValueActive );
		delete m_pValueActive;
	}
}

#ifdef _DEBUG

void CExtPropertyValue::AssertValid() const
{
	CExtPropertyItem::AssertValid();
	if( m_pValueDefault != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pValueDefault );
	}
	if( m_pValueActive != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pValueActive );
	}
}

void CExtPropertyValue::Dump( CDumpContext & dc ) const
{
	CExtPropertyItem::Dump( dc );
}

#endif

void CExtPropertyValue::ValueDefaultFromActive()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_pValueActive == NULL )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return;
	}
	if( m_pValueDefault != NULL )
	{
		delete m_pValueDefault;
		m_pValueDefault = NULL;
	}
	ValueDefaultSet( m_pValueActive, true );
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

void CExtPropertyValue::ValueActiveFromDefault()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_pValueDefault == NULL )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return;
	}
	if( m_pValueActive != NULL )
	{
		delete m_pValueActive;
		m_pValueActive = NULL;
	}
	ValueActiveSet( m_pValueDefault, true );
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

CExtGridCell * CExtPropertyValue::ValueActiveGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_pValueActive;
}

bool CExtPropertyValue::ValueActiveSet(
	CExtGridCell * pValue,
	bool bClone // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( LPVOID(m_pValueActive) == LPVOID(pValue) )
	{
		return true;
	}
	if( m_pValueActive != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pValueActive );
		delete m_pValueActive;
		m_pValueActive = NULL;
	}
	if( pValue == NULL )
		return true;
	__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
	if( ! bClone )
	{
		m_pValueActive = pValue;
		return true;
	}
	m_pValueActive = pValue->Clone();
	if( m_pValueActive == NULL )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pValueActive );
	return true;
}

CExtGridCell * CExtPropertyValue::ValueDefaultGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_pValueDefault;
}

bool CExtPropertyValue::ValueDefaultSet(
	CExtGridCell * pValue,
	bool bClone // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( LPVOID(m_pValueDefault) == LPVOID(pValue) )
	{
		return true;
	}
	if( m_pValueDefault != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pValueDefault );
		delete m_pValueDefault;
		m_pValueDefault = NULL;
	}
	if( pValue == NULL )
		return true;
	__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
	if( ! bClone )
	{
		m_pValueDefault = pValue;
		return true;
	}
	m_pValueDefault = pValue->Clone();
	if( m_pValueDefault == NULL )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pValueDefault );
	return true;
}

CExtSafeString CExtPropertyValue::BuildCompoundTextDefault() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtSafeString str = _T("");
const CExtGridCell * pCell =
		((const CExtPropertyItem *)(this)) ->
			ValueDefaultGet();
	if( pCell != NULL )
	{
		pCell->TextGet( str );
		str.TrimLeft(  _T(" \r\n\t") );
		str.TrimRight( _T(" \r\n\t") );
	}
	return str;
}

CExtSafeString CExtPropertyValue::BuildCompoundTextActive() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtSafeString str = _T("");
CExtGridCell * pCell =
		((CExtPropertyItem *)(this)) ->
			ValueActiveGet();
	if( pCell != NULL )
	{
		bool bUndefinedRole = 
			pCell->IsUndefined();
		pCell->ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
		pCell->TextGet( str );
		if( bUndefinedRole )
			pCell->ModifyStyleEx( __EGCS_EX_UNDEFINED_ROLE );
		str.TrimLeft(  _T(" \r\n\t") );
		str.TrimRight( _T(" \r\n\t") );
	}
	return str;
}

bool CExtPropertyValue::IsModified() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtGridCell * pCellValueDefault =
		((CExtPropertyItem*)this)->ValueDefaultGet();
	if( pCellValueDefault == NULL )
		return false;
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellValueDefault );
const CExtGridCell * pCellValueActive =
		((CExtPropertyItem*)this)->ValueActiveGet();
	if( pCellValueActive == NULL )
		return false;
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellValueActive );
	if( LPVOID(pCellValueActive) == LPVOID(pCellValueDefault) )
		return false;
	if( pCellValueDefault->CompareEx( CExtGridCell::__EGCCT_PROPERTY_GRID_MODIFIED_STATE_DETECTION, *pCellValueActive ) != 0 )
		return true;
	return false;
}

bool CExtPropertyValue::Reset()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridCell * pCellValueDefault =
		((CExtPropertyItem*)this)->ValueDefaultGet();
	if( pCellValueDefault == NULL )
		return false;
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellValueDefault );

	Apply( pCellValueDefault );

CExtPropertyItem * pItem = ItemParentGet();
	for( ; pItem != NULL; )
	{
		CExtPropertyValueCompound * pCompoundValue =
			DYNAMIC_DOWNCAST(
				CExtPropertyValueCompound,
				pItem
				);
		if( pCompoundValue == NULL )
			break;
		CExtSafeString strCompound = pCompoundValue->BuildCompoundTextActive();
		pItem->ValueActiveGet()->TextSet(
			strCompound.IsEmpty() ? _T("") : LPCTSTR(strCompound)
			);
		pItem = pCompoundValue->ItemParentGet();
	} // for( ; pItem != NULL; )

	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyValueSingleCell

IMPLEMENT_SERIAL( CExtPropertyValueSingleCell, CExtPropertyValue, VERSIONABLE_SCHEMA|1 );

CExtPropertyValueSingleCell::CExtPropertyValueSingleCell(
	__EXT_MFC_SAFE_LPCTSTR strName, // = NULL
	CExtGridCell * pValue, // = NULL
	bool bClone // = false
	)
	: CExtPropertyValue( strName, pValue, NULL, bClone, false )
{
}

CExtPropertyValueSingleCell::~CExtPropertyValueSingleCell()
{
	__EXT_DEBUG_GRID_ASSERT( m_pValueDefault == NULL );
}

#ifdef _DEBUG

void CExtPropertyValueSingleCell::AssertValid() const
{
	CExtPropertyValue::AssertValid();
	__EXT_DEBUG_GRID_ASSERT( m_pValueDefault == NULL );
}

void CExtPropertyValueSingleCell::Dump( CDumpContext & dc ) const
{
	CExtPropertyValue::Dump( dc );
}

#endif

void CExtPropertyValueSingleCell::ValueDefaultFromActive()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

void CExtPropertyValueSingleCell::ValueActiveFromDefault()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

CExtGridCell * CExtPropertyValueSingleCell::ValueDefaultGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ValueActiveGet();
}

bool CExtPropertyValueSingleCell::ValueDefaultSet(
	CExtGridCell * pValue,
	bool bClone // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ValueActiveSet( pValue, bClone );
}

CExtSafeString CExtPropertyValueSingleCell::BuildCompoundTextDefault() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return BuildCompoundTextActive();
}

bool CExtPropertyValueSingleCell::IsModified() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return false;
}

bool CExtPropertyValueSingleCell::Reset()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyGridCellCompound

CExtPropertyGridCellCompound::CExtPropertyGridCellCompound(
	CExtGridDataProvider * pDataProvider // = NULL
	)
	: CExtGridCellString( pDataProvider )
{
}

CExtPropertyGridCellCompound::CExtPropertyGridCellCompound( const CExtGridCell & other )
	: CExtGridCellString( other )
{
}

void CExtPropertyGridCellCompound::Assign( const CExtGridCell & other )
{
	CExtGridCellString::Assign( other );
}

void CExtPropertyGridCellCompound::Serialize( CArchive & ar )
{
	CExtGridCellString::Serialize( ar );
}

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyValueCompound

IMPLEMENT_SERIAL( CExtPropertyValueCompound, CExtPropertyValue, VERSIONABLE_SCHEMA|1 );

CExtPropertyValueCompound::CExtPropertyValueCompound(
	__EXT_MFC_SAFE_LPCTSTR strName // = NULL
	)
	: CExtPropertyValue( strName )
{
}

CExtPropertyValueCompound::~CExtPropertyValueCompound()
{
	ItemRemove();
}

bool CExtPropertyValueCompound::CanBeInsertedIntoPropertyGrid() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		if( ItemGetRefAt(nIndex).CanBeInsertedIntoPropertyGrid() )
			return true;
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return false;
}

#ifdef _DEBUG

void CExtPropertyValueCompound::AssertValid() const
{
	CExtPropertyValue::AssertValid();
}

void CExtPropertyValueCompound::Dump( CDumpContext & dc ) const
{
	CExtPropertyValue::Dump( dc );
}

#endif

INT CExtPropertyValueCompound::ItemGetCount() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return INT(m_arrItems.GetSize());
}

CExtPropertyItem * CExtPropertyValueCompound::ItemGetAt( INT nIndex )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( nIndex < 0 || nIndex >= m_arrItems.GetSize() )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return NULL;
	}
CExtPropertyItem * pItem = m_arrItems[ nIndex ];
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	__EXT_DEBUG_GRID_ASSERT( LPVOID(pItem->ItemParentGet()) == LPVOID(this) );
	return pItem;
}

bool CExtPropertyValueCompound::ItemInsert(
	CExtPropertyItem * pItem,
	INT nIndexInsertBefore // = -1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	__EXT_DEBUG_GRID_ASSERT( pItem->ItemParentGet() == NULL );
	if(		pItem == NULL
		||	(! pItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue)) )
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	if(		nIndexInsertBefore < 0
		||	nIndexInsertBefore > m_arrItems.GetSize()
		)
		nIndexInsertBefore = INT(m_arrItems.GetSize());
	pItem->ItemParentSet( this );
	m_arrItems.InsertAt(
		nIndexInsertBefore,
		((CExtPropertyValue*)pItem)
		);
	SynchronizeCompoundCellDefault();
	SynchronizeCompoundCellActive();
	return true;
}

bool CExtPropertyValueCompound::ItemRemove(
	INT nIndex, // = 0
	INT nCount // = -1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		nCount == 0
		||	m_arrItems.GetSize() == 0
		)
		return true;
	if(		nIndex < 0
		||	nIndex >= m_arrItems.GetSize()
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	if( nCount < 0 )
		nCount = INT(m_arrItems.GetSize());
	if( nCount > (m_arrItems.GetSize() - nIndex) )
		nCount = INT(m_arrItems.GetSize()) - nIndex;
	if( nCount == 0 )
		return true;
INT nPos = nIndex, nStep = 0;
	for( ; nStep < nCount; nStep ++, nPos ++ )
	{
		CExtPropertyItem * pItem = ItemGetAt( nPos );
		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
		DeleteChildItem( pItem );
	}
	m_arrItems.RemoveAt( nIndex, nCount );
	SynchronizeCompoundCellDefault();
	SynchronizeCompoundCellActive();
	return true;
}

void CExtPropertyValueCompound::ValueDefaultFromActive()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtPropertyValue::ValueDefaultFromActive();
}

void CExtPropertyValueCompound::ValueActiveFromDefault()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtPropertyValue::ValueActiveFromDefault();
}

CExtGridCell * CExtPropertyValueCompound::ValueDefaultGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridCell * pValue = CExtPropertyValue::ValueDefaultGet();
	if( pValue != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
		return pValue;
	}
	pValue = ValueDefaultGetByRTC( RUNTIME_CLASS(CExtPropertyGridCellCompound) );
	__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
	SynchronizeCompoundCellDefault();
	return pValue;
}

bool CExtPropertyValueCompound::ValueDefaultSet(
	CExtGridCell * pValue,
	bool bClone // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		CExtPropertyValue::ValueDefaultSet(
			pValue,
			bClone
			);
}

CExtGridCell * CExtPropertyValueCompound::ValueActiveGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridCell * pValue = CExtPropertyValue::ValueActiveGet();
	if( pValue != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
		return pValue;
	}
	pValue = ValueActiveGetByRTC( RUNTIME_CLASS(CExtPropertyGridCellCompound) );
	__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
	SynchronizeCompoundCellActive();
	return pValue;
}

bool CExtPropertyValueCompound::ValueActiveSet(
	CExtGridCell * pValue,
	bool bClone // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		CExtPropertyValue::ValueActiveSet(
			pValue,
			bClone
			);
}

CExtSafeString CExtPropertyValueCompound::BuildCompoundTextDefault() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return CExtPropertyItem::BuildCompoundTextDefault();
}

CExtSafeString CExtPropertyValueCompound::BuildCompoundTextActive() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return CExtPropertyItem::BuildCompoundTextActive();
}

bool CExtPropertyValueCompound::IsModified() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		const CExtPropertyItem & _item = ItemGetRefAt( nIndex );
		if( _item.IsModified() )
			return true;
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return false;
}

bool CExtPropertyValueCompound::Reset()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ! IsModified() )
		return false;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem & _item = ItemGetRefAt( nIndex );
		_item.Reset();
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	CExtPropertyValue::Reset();
	return true;
}

void CExtPropertyValueCompound::OnGridRowInitialized(
	CExtPropertyGridWnd & wndPG,
	HTREEITEM hTreeItem,
	CExtGridCell * pCellCaption,
	CExtGridCell * pCellValue
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&wndPG) );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellCaption );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellValue );
	CExtPropertyValue::OnGridRowInitialized(
		wndPG,
		hTreeItem,
		pCellCaption,
		pCellValue
		);
}

void CExtPropertyValueCompound::SynchronizeCompoundCellDefault()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridCell * pCell = ValueDefaultGet();
	if( pCell == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
CExtSafeString strCompound = BuildCompoundTextDefault();
	pCell->TextSet( strCompound.IsEmpty() ? _T("") : LPCTSTR(strCompound) );
}

void CExtPropertyValueCompound::SynchronizeCompoundCellActive()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridCell * pCell = ValueActiveGet();
	if( pCell == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
CExtSafeString strCompound = BuildCompoundTextActive();
	pCell->TextSet( strCompound.IsEmpty() ? _T("") : LPCTSTR(strCompound) );
}

void CExtPropertyValueCompound::SynchronizeWithControl(
	CExtPropertyGridCtrl * pPGC,
	bool bResetValues,
	bool bSynchronizeChildren
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
	pPGC->OnPgcQueryGrids( arrGrids );
INT nGridIdx = 0;
	for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
	{
		CExtPropertyGridWnd * pPGW = arrGrids[ nGridIdx ];
		__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
		SynchronizeWithControl(
			pPGW,
			bResetValues,
			bSynchronizeChildren
			);
	}
}

void CExtPropertyValueCompound::SynchronizeWithControl(
	CExtPropertyGridWnd * pPGW,
	bool bResetValues,
	bool bSynchronizeChildren
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
	if( bSynchronizeChildren )
	{
		INT nIndex, nCount = ItemGetCount();
		for( nIndex = 0; nIndex < nCount; nIndex++ )
		{
			CExtPropertyItem * pItem = ItemGetAt( nIndex );
			__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
			CExtPropertyValue * pValue =
				DYNAMIC_DOWNCAST( CExtPropertyValue, pItem );
			if( pValue == NULL )
				continue;
			CExtPropertyValueCompound * pCompoundValue =
				DYNAMIC_DOWNCAST( CExtPropertyValueCompound, pValue );
			CExtGridCell * pCellDefault =
				pValue->ValueDefaultGet();
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellDefault );
			if( pCompoundValue != NULL )
			{
				pCompoundValue->SynchronizeWithControl(
					pPGW,
					bResetValues,
					bSynchronizeChildren
					);
//				continue;
			} // if( pCompoundValue != NULL )
//			if( bResetValues )
//				pValue->Apply( pCellDefault );
			HTREEITEM hTreeItem =
				pPGW->PropertyItemToTreeItem( pValue );
			if( hTreeItem == NULL )
				continue;
			CExtGridCell * pCellSrc =
				pValue->ValueActiveGet();
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
			if( bResetValues )
			{
				pCellSrc->Assign( *pCellDefault );
				pCellSrc->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
			}
			CExtGridCell * pCellDst =
				pPGW->ItemGetCell( hTreeItem, 1 );
			if( pCellDst == NULL )
				continue;
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
			pCellDst->Assign( *pCellSrc );
			pCellDst->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	} // if( bSynchronizeChildren )
	if( bResetValues )
	{
		Reset();
	} // if( bResetValues )
	else
	{
		CExtSafeString strCompound =
			BuildCompoundTextActive();
		HTREEITEM hTreeItem =
			pPGW->PropertyItemToTreeItem( this );
		if( hTreeItem != NULL )
		{
			CExtGridCell * pCellDst =
				pPGW->ItemGetCell( hTreeItem, 1 );
			if( pCellDst != NULL )
			{
				__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
				pCellDst->TextSet(
					strCompound.IsEmpty() ? _T("") : LPCTSTR(strCompound)
					);
			} // if( pCellDst != NULL )
		} // if( hTreeItem != NULL )
		CExtGridCell * pCellActive = ValueActiveGet();
		if( pCellActive != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellActive );
			pCellActive->TextSet(
				strCompound.IsEmpty() ? _T("") : LPCTSTR(strCompound)
				);
		}
	} // else from if( bResetValues )
}

__EXT_MFC_SAFE_LPCTSTR CExtPropertyValueCompound::SynchronizeChildrenTreeDefault(
	__EXT_MFC_SAFE_LPCTSTR strCompoundPath,
	bool bParseOnly,
	CExtPropertyValue ** ppOutValueParseError, // = NULL
	__EXT_MFC_SAFE_LPCTSTR _strListSeparator, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strTrimLeftRight // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( strCompoundPath != NULL );
	if( ppOutValueParseError != NULL )
		(*ppOutValueParseError) = NULL;
TCHAR strListSeparator[8];
	::memset( strListSeparator, 0, sizeof(strListSeparator) );
	if( _strListSeparator == NULL )
	{
		if( g_ResourceManager->GetLocaleInfo(
				LOCALE_SLIST,
				strListSeparator,
				4
				) <= 0
			)
			__EXT_MFC_STRCPY( 
				strListSeparator, 
				sizeof(strListSeparator)/sizeof(strListSeparator[0]),
				_T(";") 
				);
	} // if( _strListSeparator == NULL )
	else
	{
		__EXT_DEBUG_GRID_ASSERT( _tcslen(_strListSeparator) < ( sizeof(strListSeparator)/sizeof(strListSeparator[0]) ) );
		__EXT_MFC_STRNCPY(
			strListSeparator,
			sizeof(strListSeparator)/sizeof(strListSeparator[0]),
			_strListSeparator,
			sizeof(strListSeparator)/sizeof(strListSeparator[0])
			);
	} // else from if( _strListSeparator == NULL )
LPCTSTR strWalk = (LPCTSTR)strCompoundPath;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem * pItem = ItemGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
		CExtPropertyValue * pValue =
			DYNAMIC_DOWNCAST( CExtPropertyValue, pItem );
		if( pValue == NULL )
			continue;
		CExtPropertyValueCompound * pCompoundValue =
			DYNAMIC_DOWNCAST( CExtPropertyValueCompound, pValue );
		if( pCompoundValue != NULL )
		{
			CExtPropertyValue * pOutValueParseError = NULL;
			strWalk =
				pCompoundValue->SynchronizeChildrenTreeDefault(
					strWalk,
					bParseOnly,
					&pOutValueParseError,
					_strListSeparator = NULL,
					strTrimLeftRight = NULL
					);
			if( ppOutValueParseError != NULL )
				(*ppOutValueParseError) = pOutValueParseError;
			if( strWalk[0] == _T('0') )
				return strWalk;
			if( pOutValueParseError != NULL )
				return strWalk;
			CExtGridCell * pCell = pCompoundValue->ValueDefaultGet();
			if( pCell == NULL )
			{
				__EXT_DEBUG_GRID_ASSERT( FALSE );
				return strWalk;
			}
			if( ! bParseOnly )
			{
				CExtSafeString strCompound =
					pCompoundValue->BuildCompoundTextDefault();
				pCell->TextSet( strCompound );
			}
			continue;
		} // if( pCompoundValue != NULL )
		CExtSafeString strOutListItem;
		strWalk =
			CExtPropertyValueCompound::stat_ExpandListPart(
				strWalk,
				strOutListItem,
				strListSeparator,
				strTrimLeftRight
				);
		CExtGridCell * pCell = pValue->ValueDefaultGet();
		if( pCell == NULL )
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return strWalk;
		}
		__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
		LPCTSTR strCellText =
			strOutListItem.IsEmpty()
				? _T("")
				: LPCTSTR( strOutListItem )
				;
		HRESULT hr = pCell->OnParseText( strCellText );
		if( hr != S_OK )
		{
			if( ppOutValueParseError != NULL )
				(*ppOutValueParseError) = pValue;
			return strWalk;
		}
		if( ! bParseOnly )
			pCell->TextSet( strCellText );
		if( strWalk[0] == _T('0') )
			return strWalk;
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return strWalk;
}

__EXT_MFC_SAFE_LPCTSTR CExtPropertyValueCompound::SynchronizeChildrenTreeActive(
	__EXT_MFC_SAFE_LPCTSTR strCompoundPath,
	bool bParseOnly,
	CExtPropertyValue ** ppOutValueParseError, // = NULL
	__EXT_MFC_SAFE_LPCTSTR _strListSeparator, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strTrimLeftRight // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( strCompoundPath != NULL );
	if( ppOutValueParseError != NULL )
		(*ppOutValueParseError) = NULL;
TCHAR strListSeparator[8];
	::memset( strListSeparator, 0, sizeof(strListSeparator) );
	if( _strListSeparator == NULL )
	{
		if( g_ResourceManager->GetLocaleInfo(
				LOCALE_SLIST,
				strListSeparator,
				4
				) <= 0
			)
			__EXT_MFC_STRCPY( 
				strListSeparator, 
				sizeof(strListSeparator)/sizeof(strListSeparator[0]),
				_T(";") 
				);
	} // if( _strListSeparator == NULL )
	else
	{
		__EXT_DEBUG_GRID_ASSERT( _tcslen(_strListSeparator) < ( sizeof(strListSeparator)/sizeof(strListSeparator[0]) ) );
		__EXT_MFC_STRNCPY(
			strListSeparator,
			sizeof(strListSeparator)/sizeof(strListSeparator[0]),
			_strListSeparator,
			sizeof(strListSeparator)/sizeof(strListSeparator[0])
			);
	} // else from if( _strListSeparator == NULL )
LPCTSTR strWalk = (LPCTSTR)strCompoundPath;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem * pItem = ItemGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
		CExtPropertyValue * pValue =
			DYNAMIC_DOWNCAST( CExtPropertyValue, pItem );
		if( pValue == NULL )
			continue;
		CExtPropertyValueCompound * pCompoundValue =
			DYNAMIC_DOWNCAST( CExtPropertyValueCompound, pValue );
		if( pCompoundValue != NULL )
		{
			CExtPropertyValue * pOutValueParseError = NULL;
			strWalk =
				pCompoundValue->SynchronizeChildrenTreeActive(
					strWalk,
					bParseOnly,
					&pOutValueParseError,
					_strListSeparator = NULL,
					strTrimLeftRight = NULL
					);
			if( ppOutValueParseError != NULL )
				(*ppOutValueParseError) = pOutValueParseError;
			if( strWalk[0] == _T('0') )
				return strWalk;
			if( pOutValueParseError != NULL )
				return strWalk;
			CExtGridCell * pCell = pCompoundValue->ValueActiveGet();
			if( pCell == NULL )
			{
				__EXT_DEBUG_GRID_ASSERT( FALSE );
				return strWalk;
			}
			if( ! bParseOnly )
			{
				CExtSafeString strCompound =
					pCompoundValue->BuildCompoundTextActive();
				pCell->TextSet( strCompound );
			}
			continue;
		} // if( pCompoundValue != NULL )
		CExtSafeString strOutListItem;
		strWalk =
			CExtPropertyValueCompound::stat_ExpandListPart(
				strWalk,
				strOutListItem,
				strListSeparator,
				strTrimLeftRight
				);
		CExtGridCell * pCell = pValue->ValueActiveGet();
		if( pCell == NULL )
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return strWalk;
		}
		__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
		LPCTSTR strCellText =
			strOutListItem.IsEmpty()
				? _T("")
				: LPCTSTR( strOutListItem )
				;
		HRESULT hr = pCell->OnParseText( strCellText );
		if( hr != S_OK )
		{
			if( ppOutValueParseError != NULL )
				(*ppOutValueParseError) = pValue;
			return strWalk;
		}
		if( ! bParseOnly )
			pCell->TextSet( strCellText );
		if( strWalk[0] == _T('0') )
			return strWalk;
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return strWalk;
}

__EXT_MFC_SAFE_LPCTSTR CExtPropertyValueCompound::stat_ExpandListPart(
	__EXT_MFC_SAFE_LPCTSTR strWalk,
	CExtSafeString & strOutListItem,
	__EXT_MFC_SAFE_LPCTSTR _strListSeparator, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strTrimLeftRight // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT( strWalk != NULL );
	strOutListItem.Empty();
TCHAR strListSeparator[8];
	::memset( strListSeparator, 0, sizeof(strListSeparator) );
	if( _strListSeparator == NULL )
	{
		if( g_ResourceManager->GetLocaleInfo(
				LOCALE_SLIST,
				strListSeparator,
				4
				) <= 0
			)
			__EXT_MFC_STRCPY( 
				strListSeparator, 
				sizeof(strListSeparator)/sizeof(strListSeparator[0]),
				_T(";") 
				);
	} // if( _strListSeparator == NULL )
	else
	{
		__EXT_DEBUG_GRID_ASSERT(
			_tcslen(_strListSeparator) <
				sizeof(strListSeparator)/sizeof(strListSeparator[0])
				);
		__EXT_MFC_STRNCPY(
			strListSeparator,
			sizeof(strListSeparator)/sizeof(strListSeparator[0]),
			_strListSeparator,
			sizeof(strListSeparator)/sizeof(strListSeparator[0])
			);
	} // else from if( _strListSeparator == NULL )
LPCTSTR strWalkOut = _tcsstr( strWalk, strListSeparator );
	if( strWalkOut == NULL )
	{
		if( strWalk[0] != _T('\0') )
		{
			strOutListItem = strWalk; // last item in list
			if( ! strOutListItem.IsEmpty() )
			{
				if( strTrimLeftRight == NULL )
				{
					strOutListItem.TrimLeft(  _T(" \r\n\t") );
					strOutListItem.TrimRight( _T(" \r\n\t") );
				} // if( strTrimLeftRight == NULL )
				else if( _tcslen(strTrimLeftRight) > 0 )
				{
					strOutListItem.TrimLeft(  strTrimLeftRight );
					strOutListItem.TrimRight( strTrimLeftRight );
				} // else if( _tcslen(strTrimLeftRight) > 0 )
			} // if( ! strOutListItem.IsEmpty() )
		} // if( strWalk[0] != _T('\0') )
		strWalkOut = strWalk;
		strWalkOut += _tcslen( strWalk );
		return strWalkOut;
	} // if( strWalkOut == NULL )
LONG nBufferLen = (LONG)
#if _MFC_VER >= 0x0800
		( INT_PTR(strWalkOut) - INT_PTR(strWalk) ) / INT_PTR( sizeof(TCHAR) );
#else
		( LONG(strWalkOut) - LONG(strWalk) ) / LONG( sizeof(TCHAR) );
#endif
	__EXT_DEBUG_GRID_ASSERT( nBufferLen >= 0 );
	if( nBufferLen > 0 )
	{
		LPTSTR strBuffer =
			strOutListItem.GetBuffer(
				nBufferLen + 1
				);
		if( strBuffer != NULL )
		{
			::memset(
				strBuffer,
				0,
				(nBufferLen+1) * sizeof(TCHAR)
				);
			__EXT_MFC_STRNCPY(
				strBuffer,
				nBufferLen + 1,
				strWalk,
				nBufferLen
				);
			strOutListItem.ReleaseBuffer();
		} // if( strBuffer != NULL )
	} // if( nBufferLen > 0 )
	strWalkOut += _tcslen( strListSeparator );
	if( ! strOutListItem.IsEmpty() )
	{
		if( strTrimLeftRight == NULL )
		{
			strOutListItem.TrimLeft(  _T(" \r\n\t") );
			strOutListItem.TrimRight( _T(" \r\n\t") );
		} // if( strTrimLeftRight == NULL )
		else if( _tcslen(strTrimLeftRight) > 0 )
		{
			strOutListItem.TrimLeft(  strTrimLeftRight );
			strOutListItem.TrimRight( strTrimLeftRight );
		} // else if( _tcslen(strTrimLeftRight) > 0 )
		strOutListItem.TrimLeft( strListSeparator );
		strOutListItem.TrimRight( strListSeparator );
	} // if( ! strOutListItem.IsEmpty() )
	return strWalkOut;
}

void CExtPropertyValueCompound::SynchronizeCompoundParentPathDefault()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	for( ; true; )
	{
		CExtPropertyItem * pParent = ItemParentGet();
		if( pParent == NULL )
			break;
		__EXT_DEBUG_GRID_ASSERT_VALID( pParent );
		CExtPropertyValueCompound * pCompoundValue =
			DYNAMIC_DOWNCAST(
				CExtPropertyValueCompound,
				pParent
				);
		if( pCompoundValue == NULL )
			break;
		CExtGridCell * pCell =
			pCompoundValue->ValueDefaultGet();
		if( pCell == NULL )
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			break;
		}
		__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
		CExtSafeString strCompound =
			pCompoundValue->BuildCompoundTextDefault();
		pCell->TextSet( strCompound );
	} // for( ; true; )
}

void CExtPropertyValueCompound::SynchronizeCompoundParentPathActive()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyItem * pParent = this;
	for( ; true; )
	{
		pParent = pParent->ItemParentGet();
		if( pParent == NULL )
			break;
		__EXT_DEBUG_GRID_ASSERT_VALID( pParent );
		CExtPropertyValueCompound * pCompoundValue =
			DYNAMIC_DOWNCAST(
				CExtPropertyValueCompound,
				pParent
				);
		if( pCompoundValue == NULL )
			break;
		CExtGridCell * pCell =
			pCompoundValue->ValueActiveGet();
		if( pCell == NULL )
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			break;
		}
		__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
		CExtSafeString strCompound =
			pCompoundValue->BuildCompoundTextActive();
		pCell->TextSet( strCompound );
	} // for( ; true; )
}

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyValueMixed

IMPLEMENT_DYNCREATE( CExtPropertyValueMixed, CExtPropertyValue );

CExtPropertyValueMixed::CExtPropertyValueMixed(
	__EXT_MFC_SAFE_LPCTSTR strName // = NULL
	)
	: CExtPropertyValue( strName )
{
}

CExtPropertyValueMixed::~CExtPropertyValueMixed()
{
	ItemRemove();
}

bool CExtPropertyValueMixed::CanBeInsertedIntoPropertyGrid() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		if( ItemGetRefAt(nIndex).CanBeInsertedIntoPropertyGrid() )
			return true;
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return false;
}

void CExtPropertyValueMixed::DeleteChildItem(
	CExtPropertyItem * pChildItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pChildItem );
	__EXT_DEBUG_GRID_ASSERT( LPVOID(pChildItem->ItemParentGet()) != LPVOID(this) );
	pChildItem;
}

void CExtPropertyValueMixed::Combine(
	CExtPropertyItem * pOtherItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pOtherItem );
	__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtPropertyValue, pOtherItem );
	ItemInsert( pOtherItem );
}

void CExtPropertyValueMixed::AdjustIntersection(
	INT nIntersectionItemCount
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nOwnCount = ItemGetCount();
	if( nOwnCount < nIntersectionItemCount )
		ItemRemove();
}

#ifdef _DEBUG

void CExtPropertyValueMixed::AssertValid() const
{
	CExtPropertyValue::AssertValid();
}

void CExtPropertyValueMixed::Dump( CDumpContext & dc ) const
{
	CExtPropertyValue::Dump( dc );
}

#endif

INT CExtPropertyValueMixed::ItemGetCount() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return INT(m_arrItems.GetSize());
}

CExtPropertyItem * CExtPropertyValueMixed::ItemGetAt( INT nIndex )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( nIndex < 0 || nIndex >= m_arrItems.GetSize() )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return NULL;
	}
CExtPropertyItem * pItem = m_arrItems[ nIndex ];
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	__EXT_DEBUG_GRID_ASSERT( LPVOID(pItem->ItemParentGet()) != LPVOID(this) );
	return pItem;
}

bool CExtPropertyValueMixed::ItemInsert(
	CExtPropertyItem * pItem,
	INT nIndexInsertBefore // = -1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
INT nCount = INT(m_arrItems.GetSize());
	if(		nIndexInsertBefore < 0
		||	nIndexInsertBefore > nCount
		)
		nIndexInsertBefore = nCount;
	m_arrItems.InsertAt( nIndexInsertBefore, pItem );
	if( nCount == 0 )
		ExpandedSet( pItem->ExpandedGet() );
	return true;
}

bool CExtPropertyValueMixed::ItemRemove(
	INT nIndex, // = 0
	INT nCount // = -1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		nCount == 0
		||	m_arrItems.GetSize() == 0
		)
		return true;
	if(		nIndex < 0
		||	nIndex >= m_arrItems.GetSize()
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	if( nCount < 0 )
		nCount = INT(m_arrItems.GetSize());
	if( nCount > (INT(m_arrItems.GetSize()) - nIndex) )
		nCount = INT(m_arrItems.GetSize()) - nIndex;
	if( nCount == 0 )
		return true;
INT nPos = nIndex, nStep = 0;
	for( ; nStep < nCount; nStep ++, nPos ++ )
	{
		CExtPropertyItem * pItem = ItemGetAt( nPos );
		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
		DeleteChildItem( pItem );
	}
	m_arrItems.RemoveAt( nIndex, nCount );
	return true;
}

void CExtPropertyValueMixed::ValueDefaultFromActive()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
		ItemGetRefAt(nIndex).ValueDefaultFromActive();
}

void CExtPropertyValueMixed::ValueActiveFromDefault()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
		ItemGetRefAt(nIndex).ValueActiveFromDefault();
}

CExtGridCell * CExtPropertyValueMixed::ValueDefaultGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ItemGetCount() == 0 )
		return NULL;
	return ItemGetRefAt(0).ValueDefaultGet();
}

bool CExtPropertyValueMixed::ValueDefaultSet(
	CExtGridCell * pValue,
	bool bClone // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ItemGetCount() == 0 )
		return true;
	if( ! bClone )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
bool bRetVal = true;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		if( ! ItemGetRefAt(nIndex).
				ValueDefaultSet( pValue, bClone )
			)
			bRetVal = false;
	}
	return bRetVal;
}

CExtGridCell * CExtPropertyValueMixed::ValueActiveGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	if( nCount == 0 )
		return NULL;
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtGridCell * pCell = ItemGetRefAt(nIndex).ValueActiveGet();
		__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
		if( ( pCell->GetStyle() & __EGCS_READ_ONLY ) != 0 )
			return pCell;
	}
	return ItemGetRefAt(0).ValueActiveGet();
}

bool CExtPropertyValueMixed::ValueActiveSet(
	CExtGridCell * pValue,
	bool bClone // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ItemGetCount() == 0 )
		return true;
	if( ! bClone )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
bool bRetVal = true;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		if( ! ItemGetRefAt(nIndex).
				ValueActiveSet( pValue, bClone )
			)
			bRetVal = false;
	}
	return bRetVal;
}

void CExtPropertyValueMixed::Apply(
	CExtGridCell * pValue // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( pValue != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
	}
#endif // _DEBUG
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
		ItemGetRefAt(nIndex).Apply( pValue );
}

void CExtPropertyValueMixed::OnGridRowInitialized(
	CExtPropertyGridWnd & wndPG,
	HTREEITEM hTreeItem,
	CExtGridCell * pCellCaption,
	CExtGridCell * pCellValue
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&wndPG) );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellCaption );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellValue );
	CExtPropertyValue::OnGridRowInitialized(
		wndPG,
		hTreeItem,
		pCellCaption,
		pCellValue
		);
	if( ItemGetCount() > 0 )
		ItemGetRefAt(0).OnGridRowInitialized(
			wndPG,
			hTreeItem,
			pCellCaption,
			pCellValue
			);
	if( ChildrenHaveDifferentActiveValues() )
		pCellValue->ModifyStyleEx( __EGCS_EX_UNDEFINED_ROLE );
	else
		pCellValue->ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
}

void CExtPropertyValueMixed::Serialize( CArchive & ar )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	ar;
	__EXT_DEBUG_GRID_ASSERT( FALSE ); // must not be invoked
#if _MFC_VER >= 0x0800
	::AfxThrowArchiveException( CArchiveException::genericException );
#else
	::AfxThrowArchiveException( CArchiveException::generic );
#endif
}

CExtSafeString CExtPropertyValueMixed::BuildCompoundTextDefault() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return CExtPropertyItem::BuildCompoundTextDefault();
}

CExtSafeString CExtPropertyValueMixed::BuildCompoundTextActive() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return CExtPropertyItem::BuildCompoundTextActive();
}

bool CExtPropertyValueMixed::IsModified() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		const CExtPropertyItem & _item = ItemGetRefAt( nIndex );
		if( _item.IsModified() )
			return true;
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return false;
}

bool CExtPropertyValueMixed::Reset()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ! IsModified() )
		return false;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem & _item = ItemGetRefAt( nIndex );
		_item.Reset();
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return true;
}

void CExtPropertyValueMixed::ExpandedSet( bool bExpanded )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtPropertyValue::ExpandedSet( bExpanded );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem & _item = ItemGetRefAt( nIndex );
		_item.ExpandedSet( bExpanded );
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
}

INT CExtPropertyValueMixed::HeightPxGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ItemGetCount() == 0 )
		return CExtPropertyValue::HeightPxGet();
const CExtPropertyItem & _item = ItemGetRefAt( 0 );
	return _item.HeightPxGet();
}

void CExtPropertyValueMixed::HeightPxSet( INT nHeightPx )
{
	CExtPropertyValue::HeightPxSet( nHeightPx );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem & _item = ItemGetRefAt( nIndex );
		_item.HeightPxSet( nHeightPx );
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
}

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyValueMixedCompound

IMPLEMENT_DYNCREATE( CExtPropertyValueMixedCompound, CExtPropertyValueMixed );

CExtPropertyValueMixedCompound::CExtPropertyValueMixedCompound(
	__EXT_MFC_SAFE_LPCTSTR strName // = NULL
	)
	: CExtPropertyValueMixed( strName )
{
}

CExtPropertyValueMixedCompound::~CExtPropertyValueMixedCompound()
{
	ItemRemove();
	CompoundPartRemove();
}

void CExtPropertyValueMixedCompound::DeleteChildItem(
	CExtPropertyItem * pChildItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pChildItem );
	__EXT_DEBUG_GRID_ASSERT( LPVOID(pChildItem->ItemParentGet()) != LPVOID(this) );
	pChildItem;
}

void CExtPropertyValueMixedCompound::Combine(
	CExtPropertyItem * pOtherItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pOtherItem );
	__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtPropertyValueCompound, pOtherItem );
	ItemInsert( pOtherItem );
INT nIndex, nCount = pOtherItem->ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtPropertyItem * pChildItem =
			pOtherItem->ItemGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pChildItem );
		__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtPropertyValueCompound, pOtherItem );
		__EXT_MFC_SAFE_LPCTSTR strName = pChildItem->NameGet();
		INT nSearchNameIndex = -1;
		CExtPropertyValueMixed * pMixedPart =
			CompoundPartGetByName(
				strName,
				nSearchNameIndex,
				RUNTIME_CLASS( CExtPropertyValueMixed )
				);
		if( pMixedPart == NULL )
		{
			if( pChildItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValueCompound) ) )
				pMixedPart = new CExtPropertyValueMixedCompound( strName );
			else
				pMixedPart = new CExtPropertyValueMixed( strName );
			pMixedPart->DescriptionSet(
				pChildItem->DescriptionGet()
				);
			CompoundPartInsert( pMixedPart );
		} // if( pMixedPart == NULL )
		pMixedPart->Combine( pChildItem );
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
}

void CExtPropertyValueMixedCompound::AdjustIntersection(
	INT nIntersectionItemCount
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtPropertyValueMixed::AdjustIntersection( nIntersectionItemCount );
}

#ifdef _DEBUG

void CExtPropertyValueMixedCompound::AssertValid() const
{
	CExtPropertyValueMixed::AssertValid();
}

void CExtPropertyValueMixedCompound::Dump( CDumpContext & dc ) const
{
	CExtPropertyValueMixed::Dump( dc );
}

#endif

INT CExtPropertyValueMixedCompound::ItemGetCount() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return INT(m_arrItems.GetSize());
}

CExtPropertyItem * CExtPropertyValueMixedCompound::ItemGetAt( INT nIndex )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( nIndex < 0 || nIndex >= m_arrItems.GetSize() )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return NULL;
	}
CExtPropertyItem * pItem = m_arrItems[ nIndex ];
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	__EXT_DEBUG_GRID_ASSERT( LPVOID(pItem->ItemParentGet()) != LPVOID(this) );
	return pItem;
}

bool CExtPropertyValueMixedCompound::ItemInsert(
	CExtPropertyItem * pItem,
	INT nIndexInsertBefore // = -1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	if(		nIndexInsertBefore < 0
		||	nIndexInsertBefore > m_arrItems.GetSize()
		)
		nIndexInsertBefore = INT(m_arrItems.GetSize());
	m_arrItems.InsertAt( nIndexInsertBefore, pItem );
	return true;
}

bool CExtPropertyValueMixedCompound::ItemRemove(
	INT nIndex, // = 0
	INT nCount // = -1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		nCount == 0
		||	m_arrItems.GetSize() == 0
		)
		return true;
	if(		nIndex < 0
		||	nIndex >= m_arrItems.GetSize()
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	if( nCount < 0 )
		nCount = INT(m_arrItems.GetSize());
	if( nCount > (INT(m_arrItems.GetSize()) - nIndex) )
		nCount = INT(m_arrItems.GetSize()) - nIndex;
	if( nCount == 0 )
		return true;
INT nPos = nIndex, nStep = 0;
	for( ; nStep < nCount; nStep ++, nPos ++ )
	{
		CExtPropertyItem * pItem = ItemGetAt( nPos );
		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
		DeleteChildItem( pItem );
	}
	m_arrItems.RemoveAt( nIndex, nCount );
	return true;
}

void CExtPropertyValueMixedCompound::ValueDefaultFromActive()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
		ItemGetRefAt(nIndex).ValueDefaultFromActive();
}

void CExtPropertyValueMixedCompound::ValueActiveFromDefault()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
		ItemGetRefAt(nIndex).ValueActiveFromDefault();
}

CExtGridCell * CExtPropertyValueMixedCompound::ValueDefaultGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ItemGetCount() == 0 )
		return NULL;
	return ItemGetRefAt(0).ValueDefaultGet();
}

bool CExtPropertyValueMixedCompound::ValueDefaultSet(
	CExtGridCell * pValue,
	bool bClone // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ItemGetCount() == 0 )
		return true;
	if( ! bClone )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
bool bRetVal = true;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		if( ! ItemGetRefAt(nIndex).
				ValueDefaultSet( pValue, bClone )
			)
			bRetVal = false;
	}
	return bRetVal;
}

CExtGridCell * CExtPropertyValueMixedCompound::ValueActiveGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	if( nCount == 0 )
		return NULL;
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtGridCell * pCell = ItemGetRefAt(nIndex).ValueActiveGet();
		__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
		if( ( pCell->GetStyle() & __EGCS_READ_ONLY ) != 0 )
			return pCell;
	}
	return ItemGetRefAt(0).ValueActiveGet();
}

bool CExtPropertyValueMixedCompound::ValueActiveSet(
	CExtGridCell * pValue,
	bool bClone // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ItemGetCount() == 0 )
		return true;
	if( ! bClone )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
bool bRetVal = true;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		if( ! ItemGetRefAt(nIndex).
				ValueActiveSet( pValue, bClone )
			)
			bRetVal = false;
	}
	return bRetVal;
}

void CExtPropertyValueMixedCompound::Apply(
	CExtGridCell * pValue // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( pValue != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pValue );
	}
#endif // _DEBUG
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
		ItemGetRefAt(nIndex).Apply( pValue );
}

void CExtPropertyValueMixedCompound::OnGridRowInitialized(
	CExtPropertyGridWnd & wndPG,
	HTREEITEM hTreeItem,
	CExtGridCell * pCellCaption,
	CExtGridCell * pCellValue
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&wndPG) );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellCaption );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellValue );
	CExtPropertyValueMixed::OnGridRowInitialized(
		wndPG,
		hTreeItem,
		pCellCaption,
		pCellValue
		);
	if( ItemGetCount() > 0 )
		ItemGetRefAt(0).OnGridRowInitialized(
			wndPG,
			hTreeItem,
			pCellCaption,
			pCellValue
			);
	if( ChildrenHaveDifferentActiveValues() )
		pCellValue->ModifyStyleEx( __EGCS_EX_UNDEFINED_ROLE );
	else
		pCellValue->ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
}

void CExtPropertyValueMixedCompound::Serialize( CArchive & ar )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	ar;
	__EXT_DEBUG_GRID_ASSERT( FALSE ); // must not be invoked
#if _MFC_VER >= 0x0800
	::AfxThrowArchiveException( CArchiveException::genericException );
#else
	::AfxThrowArchiveException( CArchiveException::generic );
#endif
}

CExtSafeString CExtPropertyValueMixedCompound::BuildCompoundTextDefault() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtSafeString strCompound = _T("");
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		const CExtPropertyItem & _item =
			ItemGetRefAt( nIndex );
		if( ! _item.CanBeInsertedIntoPropertyGrid() )
			continue;
		CExtSafeString str = _item.BuildCompoundTextDefault();
		if( strCompound.IsEmpty() )
		{
			strCompound = str;
			continue;
		} // if( strCompound.IsEmpty() )
		if( str != strCompound )
		{
			strCompound = _T("");
			break;
		} // if( str != strCompound )
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return strCompound;
}

CExtSafeString CExtPropertyValueMixedCompound::BuildCompoundTextActive() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtSafeString strCompound = _T("");
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		const CExtPropertyItem & _item =
			ItemGetRefAt( nIndex );
		if( ! _item.CanBeInsertedIntoPropertyGrid() )
			continue;
		CExtSafeString str = _item.BuildCompoundTextActive();
		if( strCompound.IsEmpty() )
		{
			strCompound = str;
			continue;
		} // if( strCompound.IsEmpty() )
		if( str != strCompound )
		{
			strCompound = _T("");
			break;
		} // if( str != strCompound )
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return strCompound;
}

INT CExtPropertyValueMixedCompound::CompoundPartGetCount() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return INT(m_arrCompoundParts.GetSize());
}

const CExtPropertyValueMixed &
	CExtPropertyValueMixedCompound::
		CompoundPartGetRefAt( INT nIndex ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( 0 <= nIndex && nIndex < m_arrCompoundParts.GetSize() );
CExtPropertyValueMixed * pItem = m_arrCompoundParts[ nIndex ];
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	__EXT_DEBUG_GRID_ASSERT( LPVOID(pItem->ItemParentGet()) == LPVOID(this) );
	return (*pItem);
}

CExtPropertyValueMixed &
	CExtPropertyValueMixedCompound::
		CompoundPartGetRefAt( INT nIndex )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyValueMixed * pItem = CompoundPartGetAt( nIndex );
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	return (*pItem);
}

const CExtPropertyValueMixed *
	CExtPropertyValueMixedCompound::
		CompoundPartGetAt( INT nIndex ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyValueMixed * pItem =
		( const_cast < CExtPropertyValueMixedCompound * > ( this ) )
		-> CompoundPartGetAt( nIndex );
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	return pItem;
}

CExtPropertyValueMixed *
	CExtPropertyValueMixedCompound::
		CompoundPartGetAt( INT nIndex )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( nIndex < 0 || nIndex >= m_arrCompoundParts.GetSize() )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return NULL;
	}
CExtPropertyValueMixed * pItem = m_arrCompoundParts[ nIndex ];
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	__EXT_DEBUG_GRID_ASSERT( LPVOID(pItem->ItemParentGet()) == LPVOID(this) );
	return pItem;
}

CExtPropertyValueMixed * CExtPropertyValueMixedCompound::CompoundPartGetByName(
	__EXT_MFC_SAFE_LPCTSTR strName,
	INT & nIndex,
	CRuntimeClass * pRTC // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		LPCTSTR(strName) == NULL
		||	_tcslen(LPCTSTR(strName)) == 0
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return NULL;
	}
INT nCount = CompoundPartGetCount();
	if( nCount == 0 )
		return NULL;
	if( nIndex < 0 )
		nIndex = 0;
	else if( nIndex >= nCount )
		return NULL;
	else
		nIndex++;
	for( ; nIndex < nCount; nIndex++ )
	{
		CExtPropertyValueMixed * pItem =
			CompoundPartGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
		if(		pRTC != NULL
			&&	(! pItem->IsKindOf(pRTC) )
			)
			continue;
		__EXT_MFC_SAFE_LPCTSTR strNameCmp =
			pItem->NameGet();
		if(		LPCTSTR(strNameCmp) == NULL
			||	_tcslen(LPCTSTR(strNameCmp)) == 0
			)
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			continue;
		}
		if(	_tcscmp(
				LPCTSTR(strName),
				LPCTSTR(strNameCmp)
				) == 0
			)
			return pItem;
	} // for( ; nIndex < nCount; nIndex++ )
	return NULL;
}

const CExtPropertyValueMixed * CExtPropertyValueMixedCompound::CompoundPartGetByName(
	__EXT_MFC_SAFE_LPCTSTR strName,
	INT & nIndex,
	CRuntimeClass * pRTC // = NULL
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyValueMixedCompound * > ( this ) )
		-> CompoundPartGetByName(
			strName,
			nIndex,
			pRTC
			);
}

bool CExtPropertyValueMixedCompound::CompoundPartInsert(
	CExtPropertyValueMixed * pItem,
	INT nIndexInsertBefore // = -1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	__EXT_DEBUG_GRID_ASSERT( pItem->ItemParentGet() == NULL );
	if(		nIndexInsertBefore < 0
		||	nIndexInsertBefore > m_arrCompoundParts.GetSize()
		)
		nIndexInsertBefore = (INT)m_arrCompoundParts.GetSize();
	pItem->ItemParentSet( this );
	m_arrCompoundParts.InsertAt( nIndexInsertBefore, pItem );
	return true;
}

bool CExtPropertyValueMixedCompound::CompoundPartRemove(
	INT nIndex, // = 0
	INT nCount // = -1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		nCount == 0
		||	m_arrCompoundParts.GetSize() == 0
		)
		return true;
	if(		nIndex < 0
		||	nIndex >= m_arrCompoundParts.GetSize()
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	if( nCount < 0 )
		nCount = (INT)m_arrCompoundParts.GetSize();
	if( nCount > (INT(m_arrCompoundParts.GetSize()) - nIndex) )
		nCount = INT(m_arrCompoundParts.GetSize()) - nIndex;
	if( nCount == 0 )
		return true;
INT nPos = nIndex, nStep = 0;
	for( ; nStep < nCount; nStep ++, nPos ++ )
	{
		CExtPropertyValueMixed * pItem = CompoundPartGetAt( nPos );
		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
		delete pItem;
	}
	m_arrCompoundParts.RemoveAt( nIndex, nCount );
	return true;
}

__EXT_MFC_SAFE_LPCTSTR CExtPropertyValueMixedCompound::SynchronizeChildrenTreeDefault(
	__EXT_MFC_SAFE_LPCTSTR strCompoundPath,
	bool bParseOnly,
	CExtPropertyValue ** ppOutValueParseError, // = NULL
	__EXT_MFC_SAFE_LPCTSTR _strListSeparator, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strTrimLeftRight // = NULL
	)
{
//	__EXT_DEBUG_GRID_ASSERT_VALID( this );
//	__EXT_DEBUG_GRID_ASSERT( strCompoundPath != NULL );
//	if( ppOutValueParseError != NULL )
//		(*ppOutValueParseError) = NULL;
//TCHAR strListSeparator[8];
//	::memset( strListSeparator, 0, sizeof(strListSeparator) );
//	if( _strListSeparator == NULL )
//	{
//		if( g_ResourceManager->GetLocaleInfo(
//				LOCALE_SLIST,
//				strListSeparator,
//				4
//				) <= 0
//			)
//			__EXT_MFC_STRCPY( 
//				strListSeparator, 
//				sizeof(strListSeparator)/sizeof(strListSeparator[0])
//				_T(";") 
//				);
//	} // if( _strListSeparator == NULL )
//	else
//	{
//		__EXT_DEBUG_GRID_ASSERT(
//			_tcslen(_strListSeparator) <
//				sizeof(strListSeparator)/sizeof(strListSeparator[0])
//				);
//		__EXT_MFC_STRNCPY(
//			strListSeparator,
//			sizeof(strListSeparator)/sizeof(strListSeparator[0]),
//			_strListSeparator,
//			sizeof(strListSeparator)/sizeof(strListSeparator[0])
//			);
//	} // else from if( _strListSeparator == NULL )
//LPCTSTR strWalk = (LPCTSTR)strCompoundPath;
//INT nIndex, nCount = CompoundPartGetCount();
//	for( nIndex = 0; nIndex < nCount; nIndex++ )
//	{
//		CExtPropertyValueMixed * pItem = CompoundPartGetAt( nIndex );
//		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
//		CExtPropertyValue * pValue =
//			DYNAMIC_DOWNCAST( CExtPropertyValue, pItem );
//		if( pValue == NULL )
//			continue;
//		CExtPropertyValueMixedCompound * pCompoundValue =
//			DYNAMIC_DOWNCAST( CExtPropertyValueMixedCompound, pValue );
//		if( pCompoundValue != NULL )
//		{
//			CExtPropertyValue * pOutValueParseError = NULL;
//			strWalk =
//				pCompoundValue->SynchronizeChildrenTreeDefault(
//					strWalk,
//					bParseOnly,
//					&pOutValueParseError,
//					_strListSeparator = NULL,
//					strTrimLeftRight = NULL
//					);
//			if( ppOutValueParseError != NULL )
//				(*ppOutValueParseError) = pOutValueParseError;
//			if( strWalk[0] == _T('0') )
//				return strWalk;
//			if( pOutValueParseError != NULL )
//				return strWalk;
//			CExtGridCell * pCell = pCompoundValue->ValueDefaultGet();
//			if( pCell == NULL )
//			{
//				__EXT_DEBUG_GRID_ASSERT( FALSE );
//				return strWalk;
//			}
//			if( ! bParseOnly )
//			{
//				CExtSafeString strCompound =
//					pCompoundValue->BuildCompoundTextDefault();
//				pCell->TextSet( strCompound );
//			}
//			continue;
//		} // if( pCompoundValue != NULL )
//		CExtSafeString strOutListItem;
//		strWalk =
//			CExtPropertyValueCompound::stat_ExpandListPart(
//				strWalk,
//				strOutListItem,
//				strListSeparator,
//				strTrimLeftRight
//				);
//		CExtGridCell * pCell = pValue->ValueDefaultGet();
//		if( pCell == NULL )
//		{
//			__EXT_DEBUG_GRID_ASSERT( FALSE );
//			return strWalk;
//		}
//		__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
//		LPCTSTR strCellText =
//			strOutListItem.IsEmpty()
//				? _T("")
//				: LPCTSTR( strOutListItem )
//				;
//		HRESULT hr = pCell->OnParseText( strCellText );
//		if( hr != S_OK )
//		{
//			if( ppOutValueParseError != NULL )
//				(*ppOutValueParseError) = pValue;
//			return strWalk;
//		}
//		if( ! bParseOnly )
//			pCell->TextSet( strCellText );
//		if( strWalk[0] == _T('0') )
//			return strWalk;
//	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
//	return strWalk;

	__EXT_DEBUG_GRID_ASSERT_VALID( this );
__EXT_MFC_SAFE_LPCTSTR strWalkResultCompound = strCompoundPath;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem * pPropertyItem =
			ItemGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
		CExtPropertyValueCompound * pCompoundValue =
			DYNAMIC_DOWNCAST(
				CExtPropertyValueCompound,
				pPropertyItem
				);
		if( pCompoundValue != NULL )
		{
			__EXT_MFC_SAFE_LPCTSTR strWalkResult =
				pCompoundValue->SynchronizeChildrenTreeDefault(
					strCompoundPath,
					bParseOnly,
					ppOutValueParseError, // = NULL
					_strListSeparator, // = NULL
					strTrimLeftRight // = NULL
					);
			if( nIndex == 0 )
				strWalkResultCompound = strWalkResult;
		} // if( pCompoundValue != NULL )
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return strWalkResultCompound;
}

__EXT_MFC_SAFE_LPCTSTR CExtPropertyValueMixedCompound::SynchronizeChildrenTreeActive(
	__EXT_MFC_SAFE_LPCTSTR strCompoundPath,
	bool bParseOnly,
	CExtPropertyValue ** ppOutValueParseError, // = NULL
	__EXT_MFC_SAFE_LPCTSTR _strListSeparator, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strTrimLeftRight // = NULL
	)
{
//	__EXT_DEBUG_GRID_ASSERT_VALID( this );
//	__EXT_DEBUG_GRID_ASSERT( strCompoundPath != NULL );
//	if( ppOutValueParseError != NULL )
//		(*ppOutValueParseError) = NULL;
//TCHAR strListSeparator[8];
//	::memset( strListSeparator, 0, sizeof(strListSeparator) );
//	if( _strListSeparator == NULL )
//	{
//		if( g_ResourceManager->GetLocaleInfo(
//				LOCALE_SLIST,
//				strListSeparator,
//				4
//				) <= 0
//			)
//			__EXT_MFC_STRCPY( 
//				strListSeparator, 
//				sizeof(strListSeparator)/sizeof(strListSeparator[0])
//				_T(";") 
//				);
//	} // if( _strListSeparator == NULL )
//	else
//	{
//		__EXT_DEBUG_GRID_ASSERT(
//			_tcslen(_strListSeparator) <
//				sizeof(strListSeparator)/sizeof(strListSeparator[0])
//				);
//		__EXT_MFC_STRNCPY(
//			strListSeparator,
//			sizeof(strListSeparator)/sizeof(strListSeparator[0]),
//			_strListSeparator,
//			sizeof(strListSeparator)/sizeof(strListSeparator[0])
//			);
//	} // else from if( _strListSeparator == NULL )
//LPCTSTR strWalk = (LPCTSTR)strCompoundPath;
//INT nIndex, nCount = CompoundPartGetCount();
//	for( nIndex = 0; nIndex < nCount; nIndex++ )
//	{
//		CExtPropertyValueMixed * pItem = CompoundPartGetAt( nIndex );
//		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
//		CExtPropertyValue * pValue =
//			DYNAMIC_DOWNCAST( CExtPropertyValue, pItem );
//		if( pValue == NULL )
//			continue;
//		CExtPropertyValueMixedCompound * pCompoundValue =
//			DYNAMIC_DOWNCAST( CExtPropertyValueMixedCompound, pValue );
//		if( pCompoundValue != NULL )
//		{
//			CExtPropertyValue * pOutValueParseError = NULL;
//			strWalk =
//				pCompoundValue->SynchronizeChildrenTreeActive(
//					strWalk,
//					bParseOnly,
//					&pOutValueParseError,
//					_strListSeparator = NULL,
//					strTrimLeftRight = NULL
//					);
//			if( ppOutValueParseError != NULL )
//				(*ppOutValueParseError) = pOutValueParseError;
//			if( strWalk[0] == _T('0') )
//				return strWalk;
//			if( pOutValueParseError != NULL )
//				return strWalk;
//			CExtGridCell * pCell = pCompoundValue->ValueActiveGet();
//			if( pCell == NULL )
//			{
//				__EXT_DEBUG_GRID_ASSERT( FALSE );
//				return strWalk;
//			}
//			if( ! bParseOnly )
//			{
//				CExtSafeString strCompound =
//					pCompoundValue->BuildCompoundTextActive();
//				pCell->TextSet( strCompound );
//			}
//			continue;
//		} // if( pCompoundValue != NULL )
//		CExtSafeString strOutListItem;
//		strWalk =
//			CExtPropertyValueCompound::stat_ExpandListPart(
//				strWalk,
//				strOutListItem,
//				strListSeparator,
//				strTrimLeftRight
//				);
//		CExtGridCell * pCell = pValue->ValueActiveGet();
//		if( pCell == NULL )
//		{
//			__EXT_DEBUG_GRID_ASSERT( FALSE );
//			return strWalk;
//		}
//		__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
//		LPCTSTR strCellText =
//			strOutListItem.IsEmpty()
//				? _T("")
//				: LPCTSTR( strOutListItem )
//				;
//		HRESULT hr = pCell->OnParseText( strCellText );
//		if( hr != S_OK )
//		{
//			if( ppOutValueParseError != NULL )
//				(*ppOutValueParseError) = pValue;
//			return strWalk;
//		}
//		if( ! bParseOnly )
//			pCell->TextSet( strCellText );
//		if( strWalk[0] == _T('0') )
//			return strWalk;
//	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
//	return strWalk;

	__EXT_DEBUG_GRID_ASSERT_VALID( this );
__EXT_MFC_SAFE_LPCTSTR strWalkResultCompound = strCompoundPath;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem * pPropertyItem =
			ItemGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
		CExtPropertyValueCompound * pCompoundValue =
			DYNAMIC_DOWNCAST(
				CExtPropertyValueCompound,
				pPropertyItem
				);
		if( pCompoundValue != NULL )
		{
			__EXT_MFC_SAFE_LPCTSTR strWalkResult =
				pCompoundValue->SynchronizeChildrenTreeActive(
					strCompoundPath,
					bParseOnly,
					ppOutValueParseError, // = NULL
					_strListSeparator, // = NULL
					strTrimLeftRight // = NULL
					);
			if( nIndex == 0 )
				strWalkResultCompound = strWalkResult;
		} // if( pCompoundValue != NULL )
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return strWalkResultCompound;
}

void CExtPropertyValueMixedCompound::SynchronizeCompoundParentPathDefault()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem * pPropertyItem =
			ItemGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
		CExtPropertyValueCompound * pCompoundValue =
			DYNAMIC_DOWNCAST(
				CExtPropertyValueCompound,
				pPropertyItem
				);
		if( pCompoundValue != NULL )
			pCompoundValue->SynchronizeCompoundParentPathDefault();
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
}

void CExtPropertyValueMixedCompound::SynchronizeCompoundParentPathActive()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem * pPropertyItem =
			ItemGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
		CExtPropertyValueCompound * pCompoundValue =
			DYNAMIC_DOWNCAST(
				CExtPropertyValueCompound,
				pPropertyItem
				);
		if( pCompoundValue != NULL )
			pCompoundValue->SynchronizeCompoundParentPathActive();
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
}

bool CExtPropertyValueMixedCompound::IsModified() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		const CExtPropertyItem & _item = ItemGetRefAt( nIndex );
		if( _item.IsModified() )
			return true;
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return false;
}

bool CExtPropertyValueMixedCompound::Reset()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ! IsModified() )
		return false;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem & _item = ItemGetRefAt( nIndex );
		_item.Reset();
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	nCount = CompoundPartGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem & _item = CompoundPartGetRefAt( nIndex );
		_item.Reset();
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return true;
}

void CExtPropertyValueMixedCompound::SynchronizeWithControl(
	CExtPropertyGridCtrl * pPGC,
	bool bResetValues,
	bool bSynchronizeChildren
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
	pPGC->OnPgcQueryGrids( arrGrids );
INT nGridIdx = 0;
	for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
	{
		CExtPropertyGridWnd * pPGW = arrGrids[ nGridIdx ];
		__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
		SynchronizeWithControl(
			pPGW,
			bResetValues,
			bSynchronizeChildren
			);
	}
}

void CExtPropertyValueMixedCompound::SynchronizeWithControl(
	CExtPropertyGridWnd * pPGW,
	bool bResetValues,
	bool bSynchronizeChildren
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
	if( bResetValues )
		Reset();
	if( bSynchronizeChildren )
	{
		INT nCount = CompoundPartGetCount();
		if( nCount > 0 )
		{
			CExtPropertyItem * pItem = CompoundPartGetAt( 0 );
			__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
			CExtPropertyValue * pValue =
				DYNAMIC_DOWNCAST( CExtPropertyValue, pItem );
			if( pValue != NULL )
			{
				CExtPropertyValueMixedCompound * pMixedCompound =
					DYNAMIC_DOWNCAST( CExtPropertyValueMixedCompound, pValue );
				CExtGridCell * pCellDefault =
					pValue->ValueDefaultGet();
				__EXT_DEBUG_GRID_ASSERT_VALID( pCellDefault );
				if( pMixedCompound != NULL )
				{
					pMixedCompound->SynchronizeWithControl(
						pPGW,
						bResetValues,
						bSynchronizeChildren
						);
				} // if( pMixedCompound != NULL )
				HTREEITEM hTreeItem =
					pPGW->PropertyItemToTreeItem( pValue );
				if( hTreeItem != NULL )
				{
					CExtGridCell * pCellSrc =
						pValue->ValueActiveGet();
					__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
					if( bResetValues )
					{
						pCellSrc->Assign( *pCellDefault );
						pCellSrc->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
					}
					CExtGridCell * pCellDst =
						pPGW->ItemGetCell( hTreeItem, 1 );
					if( pCellDst != NULL )
					{
						__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
						pCellDst->Assign( *pCellSrc );
						pCellDst->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
						if( pMixedCompound != NULL )
						{
							if( pMixedCompound->ChildrenHaveDifferentActiveValues() )
								pCellDst->ModifyStyleEx( __EGCS_EX_UNDEFINED_ROLE );
							else
								pCellDst->ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
						} // if( pMixedCompound != NULL )
						else
						{
							CExtPropertyValueMixed * pMixed =
								DYNAMIC_DOWNCAST(
									CExtPropertyValueMixed,
									pValue
									);
							if( pMixed != NULL )
							{
								if( pMixed->ChildrenHaveDifferentActiveValues() )
									pCellDst->ModifyStyleEx( __EGCS_EX_UNDEFINED_ROLE );
								else
									pCellDst->ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
							} // if( pMixed != NULL )
						} // else from if( pMixedCompound != NULL )
					} // if( pCellDst != NULL )
				} // if( hTreeItem != NULL )
			} // if( pValue != NULL )
		} // if( nCount > 0 )
	} // if( bSynchronizeChildren )
	if( bResetValues )
	{
//		Reset();
	} // if( bResetValues )
	else
	{
		CExtSafeString strCompound =
			BuildCompoundTextActive();
		HTREEITEM hTreeItem =
			pPGW->PropertyItemToTreeItem( this );
		if( hTreeItem != NULL )
		{
			CExtGridCell * pCellDst =
				pPGW->ItemGetCell( hTreeItem, 1 );
			if( pCellDst != NULL )
			{
				__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
				pCellDst->TextSet(
					strCompound.IsEmpty() ? _T("") : LPCTSTR(strCompound)
					);
			} // if( pCellDst != NULL )
		} // if( hTreeItem != NULL )
		CExtGridCell * pCellActive = ValueActiveGet();
		if( pCellActive != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellActive );
			pCellActive->TextSet(
				strCompound.IsEmpty() ? _T("") : LPCTSTR(strCompound)
				);
		}
	} // else from if( bResetValues )
}

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyCategory

IMPLEMENT_SERIAL( CExtPropertyCategory, CExtPropertyItem, VERSIONABLE_SCHEMA|1 );

CExtPropertyCategory::CExtPropertyCategory(
	__EXT_MFC_SAFE_LPCTSTR strName // = NULL
	)
	: CExtPropertyItem( strName )
{
}

CExtPropertyCategory::~CExtPropertyCategory()
{
	ItemRemove();
}

bool CExtPropertyCategory::CanBeInsertedIntoPropertyGrid() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		if( ItemGetRefAt(nIndex).CanBeInsertedIntoPropertyGrid() )
			return true;
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return false;
}

void CExtPropertyCategory::Combine(
	CExtPropertyItem * pOtherItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pOtherItem );
CExtPropertyCategory * pOtherCategory =
		DYNAMIC_DOWNCAST( CExtPropertyCategory, pOtherItem );
	if( pOtherCategory == NULL )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return;
	}
	__EXT_DEBUG_GRID_ASSERT( LPVOID(this) != LPVOID(pOtherCategory) );
INT nIndex, nCount = pOtherCategory->ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem * pItem = pOtherCategory->ItemGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
		__EXT_MFC_SAFE_LPCTSTR strName = pItem->NameGet();
		if(		LPCTSTR(strName) == NULL
			||	_tcslen(LPCTSTR(strName)) == 0
			)
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			continue;
		}
		INT nSearchNameIndex = -1;
		CExtPropertyValue * pValue = NULL;
		CExtPropertyCategory * pCategory =
			DYNAMIC_DOWNCAST( CExtPropertyCategory, pItem );
		if( pCategory == NULL )
		{
			pValue =
				STATIC_DOWNCAST( CExtPropertyValue, pItem );
			CExtPropertyItem * pOwnValue =
				ItemGetByName(
					strName,
					nSearchNameIndex,
					RUNTIME_CLASS( CExtPropertyValueMixed )
					);
			if( pOwnValue == NULL )
			{
				CExtPropertyValueCompound * pCompoundValue =
					DYNAMIC_DOWNCAST(
						CExtPropertyValueCompound,
						pValue
						);
				if( pCompoundValue != NULL )
				{
					pOwnValue = new CExtPropertyValueMixedCompound( strName );
				} // if( pCompoundValue != NULL )
				else
				{
					pOwnValue = new CExtPropertyValueMixed( strName );
				} // else from if( pCompoundValue != NULL )
				pOwnValue->DescriptionSet(
					pValue->DescriptionGet()
					);
				ItemInsert( pOwnValue );
			} // if( pOwnValue == NULL )
			pOwnValue->Combine( pValue );
			if( ! pValue->ExpandedGet() )
				pOwnValue->ExpandedSet( false );
		} // if( pCategory == NULL )
		else
		{
			CExtPropertyItem * pOwnCategory =
				ItemGetByName(
					strName,
					nSearchNameIndex,
					RUNTIME_CLASS( CExtPropertyCategory )
					);
			if( pOwnCategory == NULL )
			{
				pOwnCategory = new CExtPropertyCategory( strName );
				pOwnCategory->DescriptionSet(
					pCategory->DescriptionGet()
					);
				ItemInsert( pOwnCategory );
			} // if( pOwnCategory == NULL )
			pOwnCategory->Combine( pCategory );
			if( ! pCategory->ExpandedGet() )
				pOwnCategory->ExpandedSet( false );
		} // if( pCategory == NULL )
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
}

void CExtPropertyCategory::AdjustIntersection(
	INT nIntersectionItemCount
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; )
	{
		CExtPropertyItem * pItem = ItemGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
		pItem->AdjustIntersection( nIntersectionItemCount );
		INT nCC = pItem->ItemGetCount();
		if( nCC == 0 )
		{
			ItemRemove( nIndex, 1 );
			nCount --;
		}
		else
			nIndex ++;
	} // for( nIndex = 0; nIndex < nCount; )
}

#ifdef _DEBUG

void CExtPropertyCategory::AssertValid() const
{
	CExtPropertyItem::AssertValid();
}

void CExtPropertyCategory::Dump( CDumpContext & dc ) const
{
	CExtPropertyItem::Dump( dc );
}

#endif

INT CExtPropertyCategory::ItemGetCount() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return INT(m_arrItems.GetSize());
}

CExtPropertyItem * CExtPropertyCategory::ItemGetAt( INT nIndex )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( nIndex < 0 || nIndex >= m_arrItems.GetSize() )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return NULL;
	}
CExtPropertyItem * pItem = m_arrItems[ nIndex ];
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	__EXT_DEBUG_GRID_ASSERT( LPVOID(pItem->ItemParentGet()) == LPVOID(this) );
	return pItem;
}

bool CExtPropertyCategory::ItemInsert(
	CExtPropertyItem * pItem,
	INT nIndexInsertBefore // = -1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
	__EXT_DEBUG_GRID_ASSERT( pItem->ItemParentGet() == NULL );
	if(		nIndexInsertBefore < 0
		||	nIndexInsertBefore > m_arrItems.GetSize()
		)
		nIndexInsertBefore = INT(m_arrItems.GetSize());
	pItem->ItemParentSet( this );
	m_arrItems.InsertAt( nIndexInsertBefore, pItem );
	return true;
}

bool CExtPropertyCategory::ItemRemove(
	INT nIndex, // = 0
	INT nCount // = -1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		nCount == 0
		||	m_arrItems.GetSize() == 0
		)
		return true;
	if(		nIndex < 0
		||	nIndex >= m_arrItems.GetSize()
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	if( nCount < 0 )
		nCount = INT(m_arrItems.GetSize());
	if( nCount > (INT(m_arrItems.GetSize()) - nIndex) )
		nCount = INT(m_arrItems.GetSize()) - nIndex;
	if( nCount == 0 )
		return true;
INT nPos = nIndex, nStep = 0;
	for( ; nStep < nCount; nStep ++, nPos ++ )
	{
		CExtPropertyItem * pItem = ItemGetAt( nPos );
		__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
		DeleteChildItem( pItem );
	}
	m_arrItems.RemoveAt( nIndex, nCount );
	return true;
}

bool CExtPropertyCategory::IsModified() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		const CExtPropertyItem & _item = ItemGetRefAt( nIndex );
		if( _item.IsModified() )
			return true;
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return false;
}

bool CExtPropertyCategory::Reset()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ! IsModified() )
		return false;
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem & _item = ItemGetRefAt( nIndex );
		_item.Reset();
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return true;
}

bool CExtPropertyCategory::OnInputEnabledGet( bool bUseNestedFlags ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ! CExtPropertyItem::OnInputEnabledGet( bUseNestedFlags ) )
		return false;
//	if( bUseNestedFlags )
//	{
//		INT nIndex, nCount = ItemGetCount();
//		for( nIndex = 0; nIndex < nCount; nIndex++ )
//		{
//			const CExtPropertyItem & _item = ItemGetRefAt( nIndex );
//			const CExtPropertyItem * pParentItem = _item.ItemParentGet();
//			if( LPVOID(pParentItem) == LPVOID(this) )
//			{
//				CExtPropertyValueMixed * pMixedValue =
//					DYNAMIC_DOWNCAST(
//						CExtPropertyValueMixed,
//						(&_item )
//						);
//				if( pMixedValue == NULL )
//					continue;
//				INT nIndex2, nCount2 = pMixedValue->ItemGetCount();
//				for( nIndex2 = 0; nIndex2 < nCount2; nIndex2++ )
//				{
//					const CExtPropertyItem & _item = pMixedValue->ItemGetRefAt( nIndex2 );
//					const CExtPropertyItem * pParentItem = _item.ItemParentGet();
//					if( LPVOID(pParentItem) != LPVOID(this) )
//					{
//						if( ! pParentItem->OnInputEnabledGet( true ) )
//							return false;
//					}
//				} // for( nIndex2 = 0; nIndex2 < nCount2; nIndex2++ )
//				continue;
//			} // if( LPVOID(pParentItem) == LPVOID(this) )
//			if( ! pParentItem->OnInputEnabledGet( true ) )
//				return false;
//		} // for( nIndex = 0; nIndex < nCount; nIndex++ )
//	} // if( bUseNestedFlags )
	return true;
}

void CExtPropertyCategory::OnInputEnabledSet( bool bUseNestedFlags, bool bInputEnabled )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtPropertyItem::OnInputEnabledSet( bUseNestedFlags, bInputEnabled );
	if( bUseNestedFlags )
	{
		INT nIndex, nCount = ItemGetCount();
		for( nIndex = 0; nIndex < nCount; nIndex++ )
		{
			CExtPropertyItem & _item = ItemGetRefAt( nIndex );
			CExtPropertyItem * pParentItem = _item.ItemParentGet();
			if( LPVOID(pParentItem) == LPVOID(this) )
			{
				CExtPropertyValueMixed * pMixedValue =
					DYNAMIC_DOWNCAST(
						CExtPropertyValueMixed,
						(&_item )
						);
				if( pMixedValue == NULL )
					continue;
				INT nIndex2, nCount2 = pMixedValue->ItemGetCount();
				for( nIndex2 = 0; nIndex2 < nCount2; nIndex2++ )
				{
					CExtPropertyItem & _item = pMixedValue->ItemGetRefAt( nIndex2 );
					CExtPropertyItem * pParentItem = _item.ItemParentGet();
					if( LPVOID(pParentItem) != LPVOID(this) )
						pParentItem->OnInputEnabledSet( true, bInputEnabled );
				} // for( nIndex2 = 0; nIndex2 < nCount2; nIndex2++ )
				continue;
			} // if( LPVOID(pParentItem) == LPVOID(this) )
			pParentItem->OnInputEnabledSet( true, bInputEnabled );
		} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	} // if( bUseNestedFlags )
}

void CExtPropertyCategory::ExpandedSet( bool bExpanded )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtPropertyItem::ExpandedSet( bExpanded );
INT nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		CExtPropertyItem & _item = ItemGetRefAt( nIndex );
		CExtPropertyItem * pParentItem = _item.ItemParentGet();
		if( LPVOID(pParentItem) == LPVOID(this) )
		{
			CExtPropertyValueMixed * pMixedValue =
				DYNAMIC_DOWNCAST(
					CExtPropertyValueMixed,
					(&_item )
					);
			if( pMixedValue == NULL )
				continue;
			INT nIndex2, nCount2 = pMixedValue->ItemGetCount();
			for( nIndex2 = 0; nIndex2 < nCount2; nIndex2++ )
			{
				CExtPropertyItem & _item = pMixedValue->ItemGetRefAt( nIndex2 );
				CExtPropertyItem * pParentItem = _item.ItemParentGet();
				if( LPVOID(pParentItem) != LPVOID(this) )
					pParentItem->ExpandedSet( bExpanded );
			} // for( nIndex2 = 0; nIndex2 < nCount2; nIndex2++ )
			continue;
		} // if( LPVOID(pParentItem) == LPVOID(this) )
		pParentItem->ExpandedSet( bExpanded );
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
}

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyStore

IMPLEMENT_SERIAL( CExtPropertyStore, CExtPropertyCategory, VERSIONABLE_SCHEMA|1 );

CExtPropertyStore::CExtPropertyStore(
	__EXT_MFC_SAFE_LPCTSTR strName // = NULL
	)
	: CExtPropertyCategory( strName )
{
}

CExtPropertyStore::~CExtPropertyStore()
{
}

#ifdef _DEBUG

void CExtPropertyStore::AssertValid() const
{
	CExtPropertyCategory::AssertValid();
}

void CExtPropertyStore::Dump( CDumpContext & dc ) const
{
	CExtPropertyCategory::Dump( dc );
}

#endif

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyGridCellArea

CExtPropertyGridCellArea::CExtPropertyGridCellArea(
	CExtGridDataProvider * pDataProvider // = NULL
	)
	: CExtGridCellStringDM( pDataProvider )
{
	ModifyStyle( __EGCS_CHECKED );
}

CExtPropertyGridCellArea::CExtPropertyGridCellArea( const CExtGridCell & other )
	: CExtGridCellStringDM( other )
	, m_bHelperCaseInsensitivePropertyCaptionComparison( false )
{
	ModifyStyle( __EGCS_CHECKED );
}

int CExtPropertyGridCellArea::CompareEx(
	e_grid_cell_comparison_type_t eGCCT,
	const CExtGridCell & other,
	DWORD dwStyleMask, // = __EGCS_COMPARE_MASK
	DWORD dwStyleExMask, // = __EGCS_EX_COMPARE_MASK
	LPARAM lParamCompareEx // = 0L // user defined value
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtSafeString strLeft, strRight;
	TextGet( strLeft );
	other.TextGet( strRight );
int nTextCmdResult = 
		( m_bHelperCaseInsensitivePropertyCaptionComparison )
			? strLeft.CompareNoCase( LPCTSTR(strRight) )
			: strLeft.Compare      ( LPCTSTR(strRight) )
			;
	if( nTextCmdResult != 0 )
		return nTextCmdResult;
	return CExtGridCellStringDM::CompareEx( eGCCT, other, dwStyleMask, dwStyleExMask, lParamCompareEx );
}

bool CExtPropertyGridCellArea::OnClick(
	CExtGridWnd & wndGrid,
	const CExtGridHitTestInfo & htInfo,
	UINT nChar, // VK_LBUTTON, VK_RBUTTON or VK_MBUTTON only
	UINT nRepCnt, // 0 - button up, 1 - single click, 2 - double click, 3 - post single click & begin editing
	UINT nFlags // mouse event flags
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&wndGrid) );
	__EXT_DEBUG_GRID_ASSERT( ! htInfo.IsHoverEmpty() );
	__EXT_DEBUG_GRID_ASSERT( htInfo.IsValidRect() );
	__EXT_DEBUG_GRID_ASSERT( nChar == VK_LBUTTON || nChar == VK_RBUTTON || nChar == VK_MBUTTON );
	__EXT_DEBUG_GRID_ASSERT( 0 <= nRepCnt && nRepCnt <= 3 );
	__EXT_DEBUG_GRID_ASSERT( wndGrid.IsKindOf( RUNTIME_CLASS(CExtPropertyGridWnd) ) );
	if( IsInvisible() )
		return false;
DWORD dwCellStyle = GetStyle();
	if( (dwCellStyle&__EGCS_READ_ONLY) != 0 )
		return false;
INT nColType = htInfo.GetInnerOuterTypeOfColumn();
INT nRowType = htInfo.GetInnerOuterTypeOfRow();
	if( nChar == VK_LBUTTON && ( nRepCnt == 1 || nRepCnt == 2 ) )
	{ // if single left button click
		if( (nFlags&(MK_SHIFT|MK_CONTROL)) == 0 )
		{ // if neither Shift nor Ctrl keys was pressed
			if(		(htInfo.m_dwAreaFlags&__EGBWA_CELL_CHECKBOX) != 0 
				&&	(dwCellStyle&__EGCS_CHK_MASK) != 0
				)
			{
				// move focus and selection to the cell
				if( nColType == 0 && nRowType == 0 )
				{
					CPoint ptFocusOld = wndGrid.FocusGet();
					CPoint ptFocusDesired( htInfo.m_nColNo, htInfo.m_nRowNo );
					CRect rcSelectionOld = wndGrid.SelectionGet( false, -1 );
					CRect rcSelectionDesired( htInfo.m_nColNo, htInfo.m_nRowNo, htInfo.m_nColNo, htInfo.m_nRowNo );
					DWORD dwSiwStyles = wndGrid.SiwGetStyle();
					if( (dwSiwStyles & __EGBS_SFB_MASK) == __EGBS_SFB_FULL_ROWS )
					{
						LONG nColumnCount = wndGrid.ColumnCountGet();
 						rcSelectionDesired.SetRect( 0, htInfo.m_nRowNo, nColumnCount - 1, htInfo.m_nRowNo );
					}
					if( (dwSiwStyles & __EGBS_SFB_MASK) == __EGBS_SFB_FULL_COLUMNS )
					{
						LONG nRowCount = wndGrid.RowCountGet();
 						rcSelectionDesired.SetRect( htInfo.m_nColNo, 0, nRowCount - 1, htInfo.m_nColNo );
					}
					if(		ptFocusDesired != ptFocusOld 
						||	rcSelectionOld != rcSelectionDesired
						)
					{
						if( ptFocusDesired != ptFocusOld )
							wndGrid.FocusSet( ptFocusDesired, true, true, false, false );
						if( rcSelectionOld != rcSelectionDesired )
							wndGrid.SelectionSet( rcSelectionDesired, true, false, false );
						wndGrid.OnSwDoRedraw();
					}
				}
				bool bCheck = ( ( GetStyle() & __EGCS_CHECKED ) != 0 ) ? true : false;
				bCheck = ! bCheck;
				( STATIC_DOWNCAST( CExtPropertyGridWnd, (&wndGrid) ) ) -> 
					_OnPropertyInputEnabled(
						bCheck,
						htInfo.m_nRowNo
						);
				return true;
			} // if( (htInfo.m_dwAreaFlags&__EGBWA_CELL_CHECKBOX) != 0 )
		} // if neither Shift nor Ctrl keys was pressed
	} // if single left button click
	return 
		CExtGridCellStringDM::OnClick(
			wndGrid,
			htInfo,
			nChar,
			nRepCnt,
			nFlags
			);
}

CRect CExtPropertyGridCellArea::OnQueryTextAreaMargins(
	const CExtGridWnd & wndGrid,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CRect rc =
		CExtGridCellStringDM::OnQueryTextAreaMargins(
			wndGrid,
			dc,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			dwAreaFlags,
			dwHelperPaintFlags
			);
CExtPropertyGridWnd * pPGW =
		STATIC_DOWNCAST(
			CExtPropertyGridWnd,
			( const_cast < CExtGridWnd * > ( &wndGrid ) )
			);
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
HTREEITEM hTreeItem =
		pPGW->ItemGetByVisibleRowIndex( nRowNo );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
CExtPropertyItem * pPropertyItem =
		pPGW->PropertyItemFromTreeItem( hTreeItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	if( pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) ) )
	{
		for(	pPropertyItem = pPropertyItem->ItemParentGet();
				pPropertyItem != NULL;
				pPropertyItem = pPropertyItem->ItemParentGet()
				)
		{
			if(		pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValueCompound) )
				||	pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValueMixedCompound) )
				)
				rc.left += __EXT_MGC_PROPERTY_GRID_DEFAULT_LEVEL_INDENT;
		}
	}
	return rc;
}

COLORREF CExtPropertyGridCellArea::OnQueryTextColor(
	const CExtGridWnd & wndGrid,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyGridWnd * pPGW =
		STATIC_DOWNCAST(
			CExtPropertyGridWnd,
			( const_cast < CExtGridWnd * > ( &wndGrid ) )
			);
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
HTREEITEM hTreeItem =
		pPGW->ItemGetByVisibleRowIndex( nRowNo );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
CExtPropertyItem * pPropertyItem =
		pPGW->PropertyItemFromTreeItem( hTreeItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	if(		(! pPGW->m_bFullRowSelection )
		&&	pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyCategory))
		)
		return pPGW->OnSiwGetSysColor( COLOR_3DDKSHADOW );
	return
		CExtGridCellStringDM::OnQueryTextColor(
			wndGrid,
			dc,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			dwAreaFlags,
			dwHelperPaintFlags
			);
}

HFONT CExtPropertyGridCellArea::OnQueryCellFont(
	const CExtGridWnd & wndGrid,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	DWORD dwAreaFlags,
	bool & bFontMustBeDestroyed,
	DWORD dwHelperPaintFlags // = 0
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nColType == 0 && nRowType == 0 );
CExtPropertyGridWnd * pPGW =
		STATIC_DOWNCAST(
			CExtPropertyGridWnd,
			( const_cast < CExtGridWnd * > ( &wndGrid ) )
			);
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
HTREEITEM hTreeItem =
		pPGW->ItemGetByVisibleRowIndex( nRowNo );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
CExtPropertyItem * pPropertyItem =
		pPGW->PropertyItemFromTreeItem( hTreeItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
bool bBold = false;
	if( pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyCategory) ) )
		bBold = true;
	else if(
			pPGW->m_bMakeBoldModifiedNames
		&&	pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) )
		)
		bBold = pPropertyItem->IsModified();
	if( bBold )
	{
		bFontMustBeDestroyed = false;
		return (HFONT)wndGrid.PmBridge_GetPM()->m_FontBold.GetSafeHandle();
	}
	return
		CExtGridCellStringDM::OnQueryCellFont(
			wndGrid,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			dwAreaFlags, 
			bFontMustBeDestroyed,
			dwHelperPaintFlags
			);
}

HWND CExtPropertyGridCellArea::OnInplaceControlCreate(
	HWND hWndParentForEditor,
	CExtGridWnd & wndGrid,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcInplaceControl,
	LONG nLastEditedColNo,
	LONG nLastEditedRowNo
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	hWndParentForEditor;
	wndGrid;
	nVisibleColNo;
	nVisibleRowNo;
	nColNo;
	nRowNo;
	nColType;
	nRowType;
	rcCellExtra;
	rcCell;
	rcInplaceControl;
	nLastEditedColNo;
	nLastEditedRowNo;
	return NULL;
}

void CExtPropertyGridCellArea::OnHitTestInfoAdjust(
	const CExtGridWnd & wndGrid,
	CExtGridHitTestInfo & htInfo,
	bool bComputeOuterDropAfterState
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtGridCellStringDM::OnHitTestInfoAdjust(
		wndGrid,
		htInfo,
		bComputeOuterDropAfterState
		);
	if(		htInfo.GetInnerOuterTypeOfColumn() == 0
		&&	htInfo.GetInnerOuterTypeOfRow() == 0
		&&	htInfo.m_nColNo >= 0
		&&	htInfo.m_nRowNo >= 0
		)
	{
		CExtPropertyGridWnd * pPGW =
			STATIC_DOWNCAST(
				CExtPropertyGridWnd,
				( const_cast < CExtGridWnd * > ( &wndGrid ) )
				);
		__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
		HTREEITEM hTreeItem =
			pPGW->ItemGetByVisibleRowIndex( htInfo.m_nRowNo );
		__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
		const CExtPropertyItem * pPropertyItem =
			pPGW->PropertyItemFromTreeItem( hTreeItem );
		__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
		if( pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyCategory)) )
			htInfo.m_dwAreaFlags &= ~(__EGBWA_NEAR_CELL_BORDER_H);
	}
}

bool CExtPropertyGridCellArea::OnPaintBackground(
	const CExtGridWnd & wndGrid,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&wndGrid) );
	__EXT_DEBUG_GRID_ASSERT( dc.GetSafeHdc() != NULL );
	__EXT_DEBUG_GRID_ASSERT( nColType == 0 && nRowType == 0 );
	if( (dwHelperPaintFlags&__EGCPF_SIMPLIFIED_RENDERING_TARGET) != 0 )
		return true;
CExtPropertyGridWnd * pPGW =
		STATIC_DOWNCAST(
			CExtPropertyGridWnd,
			( const_cast < CExtGridWnd * > ( &wndGrid ) )
			);
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
HTREEITEM hTreeItem =
		pPGW->ItemGetByVisibleRowIndex( nRowNo );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
const CExtPropertyItem * pPropertyItem =
		pPGW->PropertyItemFromTreeItem( hTreeItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
bool bCategory =
		pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyCategory))
			? true : false;
	if( bCategory || nColNo == 0 )
	{
		COLORREF clrFace = 
			wndGrid.PmBridge_GetPM()->GetColor(
				CExtPaintManager::CLR_WRB_FRAME
				);
		if( bCategory )
		{
			if( pPGW->m_brushCategoryBk.GetSafeHandle() != NULL )
				dc.FillRect(
					&rcCell,
					&pPGW->m_brushCategoryBk
					);
			else
				dc.FillSolidRect(
					&rcCell,
					clrFace
					);
		} // if( bCategory )
		if( nColNo == 0 )
		{
			if( pPGW->m_brushOutlineBk.GetSafeHandle() != NULL )
				dc.FillRect(
					&rcCell,
					&pPGW->m_brushOutlineBk
					);
			else
			{
				CRect rc;
				wndGrid.GetClientRect( &rc );
				rc.top = rcCell.top;
				rc.bottom = rcCell.bottom;
				rc.right = rcCell.left;
				dc.FillSolidRect( &rc, clrFace );
			}
		} // if( nColNo == 0 )
	} // if( bCategory || nColNo == 0 )
bool bRetVal =
		CExtGridCellStringDM::OnPaintBackground(
			wndGrid,
			dc,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
	return bRetVal;
}

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyGridWnd window

IMPLEMENT_DYNCREATE( CExtPropertyGridWnd, CExtTreeGridWnd );

CExtPropertyGridWnd::CExtPropertyGridWnd(
	CExtPropertyGridCtrl * pPGC // = NULL
	)
	: m_pPropertyGridCtrl( pPGC )
	, m_bAutoDeleteWindow( false )
	, m_nConstantIndent( -1 )
	, m_bThemeBackground( true )
	, m_bFullRowSelection( false )
	, m_bDrawFocusRect( false )
	, m_bSortedCategories( true )
	, m_bSortedValues( false )
	, m_bInputEnabledCheckMarksInCategories( false )
	, m_bInputEnabledCheckMarksInValues( false )
	, m_bInputEnabledNestedFlags( false )
	, m_bSynchronizeColumnWidth( true )
	, m_bHighlightReadOnlyValues( true )
	, m_nPxResizeStepByKeyboard( 3 )
	, m_bMakeBoldModifiedNames( true )
	, m_bMakeBoldModifiedValues( true )
	, m_bSynchronizingFocus( false )
	, m_bHelperCaseInsensitivePropertyCaptionComparison( false )
{
#ifdef _DEBUG
	if( m_pPropertyGridCtrl != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPropertyGridCtrl );
	}
#endif // _DEBUG

	m_bDrawFilledExpandGlyphs = false;
	m_bAdustNullIndent = true;
}

CExtPropertyGridWnd::~CExtPropertyGridWnd()
{
	if( m_brushCategoryBk.GetSafeHandle() != NULL )
		m_brushCategoryBk.DeleteObject();
	if( m_brushOutlineBk.GetSafeHandle() != NULL )
		m_brushOutlineBk.DeleteObject();
}

#ifdef _DEBUG

void CExtPropertyGridWnd::AssertValid() const
{
	CExtTreeGridWnd::AssertValid();
	if( m_pPropertyGridCtrl != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPropertyGridCtrl );
	}
}

void CExtPropertyGridWnd::Dump( CDumpContext & dc ) const
{
	CExtTreeGridWnd::Dump( dc );
}

#endif

BEGIN_MESSAGE_MAP( CExtPropertyGridWnd, CExtTreeGridWnd )
    //{{AFX_MSG_MAP(CExtPropertyGridWnd)
	ON_WM_NCCALCSIZE()
	ON_WM_NCPAINT()
	ON_WM_SIZE()
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_EXT_PROPERTY_GRID_RESET,OnResetCmdFromCtxMenu)
	//}}AFX_MSG_MAP
	__EXT_MFC_SAFE_ON_WM_SETTINGCHANGE()
END_MESSAGE_MAP()

void CExtPropertyGridWnd::OnSettingChange(UINT uFlags, __EXT_MFC_SAFE_LPCTSTR lpszSection)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtTreeGridWnd::OnSettingChange( uFlags, lpszSection );
	_SyncInnerRowHeight();
}

void CExtPropertyGridWnd::OnSize(UINT nType, int cx, int cy) 
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	_DoColumnLayout();
	CExtTreeGridWnd::OnSize(nType, cx, cy);
}

void CExtPropertyGridWnd::OnContextMenu(CWnd* pWnd, CPoint point)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyItem * pPropertyItem = NULL;
	CExtGridHitTestInfo htInfo;
	htInfo.m_ptClient = point;
	ScreenToClient( &htInfo.m_ptClient );
	HitTest( htInfo, false, false );
	if(		(! htInfo.IsHoverEmpty() )
		&&	0 <= htInfo.m_nColNo
		&&	htInfo.m_nColNo <= 1
		&&	htInfo.m_nRowNo >= 0
		&&	( htInfo.m_dwAreaFlags
				& (__EGBWA_TREE_BOX|__EGBWA_TREE_OUTLINE_AREA)
			) == 0
		)
	{ // if hit-testing result is not emptty
		HTREEITEM htiFocusOld =
			ItemFocusGet();
		HTREEITEM htiFocusNew =
			ItemGetByVisibleRowIndex( htInfo.m_nRowNo );
		if( htiFocusNew == NULL )
			return;
		if( htiFocusNew != htiFocusOld )
			ItemFocusSet( htiFocusNew );
		pPropertyItem =
			PropertyItemFromTreeItem( htiFocusNew );
	} // if hit-testing result is not emptty
	OnPgwContextMenu(
		pWnd,
		htInfo,
		pPropertyItem
		);
}

void CExtPropertyGridWnd::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bCalcValidRects;
	lpncsp->rgrc[0].left++;
	lpncsp->rgrc[0].top++;
	lpncsp->rgrc[0].right--;
	lpncsp->rgrc[0].bottom--;
}

void CExtPropertyGridWnd::OnNcPaint() 
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CWindowDC dcWindow( this );
CRect rcClient, rcBar;
	GetClientRect( rcClient );
	ClientToScreen( rcClient );
	GetWindowRect( rcBar );
	rcClient.OffsetRect( -rcBar.TopLeft() );
	rcBar.OffsetRect( -rcBar.TopLeft() );
	dcWindow.ExcludeClipRect(rcClient);
CExtMemoryDC dc( &dcWindow, &rcBar );
	PmBridge_GetPM()->PaintResizableBarChildNcAreaRect(
		dc,
		rcBar,
		this
		);
}

LRESULT CExtPropertyGridWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if(		message == WM_PRINT
		||	message == WM_PRINTCLIENT
		)
	{
		CDC * pDC = CDC::FromHandle( (HDC) wParam );
		
		CRect rcWnd, rcClient;
		GetWindowRect( &rcWnd );
		GetClientRect( &rcClient );
		ClientToScreen( rcClient );
		rcClient.OffsetRect( -rcWnd.TopLeft() );
		rcWnd.OffsetRect( -rcWnd.TopLeft() );

		if( (lParam&PRF_NONCLIENT) != 0 )
		{
			CExtMemoryDC dc(
				pDC,
				&rcWnd
				);
			dc.ExcludeClipRect(rcClient);
			PmBridge_GetPM()->PaintResizableBarChildNcAreaRect(
				dc,
				rcWnd,
				this
				);
			dc.SelectClipRgn( NULL );
		}
	
		if( (lParam&(PRF_CLIENT|PRF_ERASEBKGND)) != 0 )
		{
			CExtMemoryDC dc(
				pDC,
				&rcClient
				);
			CPoint ptVpOffset( 0, 0 );
			if( (lParam&PRF_NONCLIENT) != 0 )
			{
				ptVpOffset.x = rcWnd.left - rcClient.left;
				ptVpOffset.y = rcWnd.top - rcClient.top;
			}
			if(		ptVpOffset.x != 0
				||	ptVpOffset.y != 0
				)
				dc.OffsetViewportOrg(
					-ptVpOffset.x,
					-ptVpOffset.y
					);
			OnSwPaint( dc );
			if(		ptVpOffset.x != 0
				||	ptVpOffset.y != 0
				)
				dc.OffsetViewportOrg(
					ptVpOffset.x,
					ptVpOffset.y
					);
		} // if( (lParam&(PRF_CLIENT|PRF_ERASEBKGND)) != 0 )
		
		if( (lParam&PRF_CHILDREN) != 0 )
			CExtPaintManager::stat_PrintChildren(
				m_hWnd,
				message,
				pDC->GetSafeHdc(),
				lParam,
				false
				);
		return (!0);
	}
LRESULT lResult = CExtScrollWnd::WindowProc( message, wParam, lParam );
	if( message == WM_GETDLGCODE )
		lResult |= DLGC_WANTTAB;
	return lResult;
}

void CExtPropertyGridWnd::OnResetCmdFromCtxMenu()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet();
	if( pPGC == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
HTREEITEM hTreeItem = ItemFocusGet();
	if( hTreeItem == NULL )
		return;
CExtPropertyItem * pPropertyItem = PropertyItemFromTreeItem( hTreeItem );
	if( pPropertyItem == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	if( ! pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) ) )
		return;
	if( ! pPropertyItem->IsModified() )
		return;
	pPGC->OnPgcResetValue(
		this,
		STATIC_DOWNCAST(
			CExtPropertyValue,
			pPropertyItem
			)
		);
}

bool CExtPropertyGridWnd::Create(
	CWnd * pWndParent,
	UINT nDlgCtrlID,
	bool bVisible // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() == NULL );
	__EXT_DEBUG_GRID_ASSERT_VALID( pWndParent );
	__EXT_DEBUG_GRID_ASSERT( pWndParent->GetSafeHwnd() != NULL );
	__EXT_DEBUG_GRID_ASSERT( pWndParent->GetDlgItem( int(nDlgCtrlID) ) == NULL );
	if( ! CExtTreeGridWnd::Create(
			pWndParent,
			CRect( 0, 0, 100, 100 ),
			nDlgCtrlID,
			0,
			WS_CHILD|WS_CLIPSIBLINGS|
				( bVisible ? (WS_VISIBLE) : 0 )
			)
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	m_wndScrollBarV.m_eSO = CExtScrollBar::__ESO_RIGHT;
	if( ! m_wndScrollBarV.Create(
			WS_CHILD|WS_VISIBLE
				|WS_CLIPSIBLINGS|WS_CLIPCHILDREN
				|SBS_VERT|SBS_RIGHTALIGN,
			CRect(0,0,0,0),
			this,
			1
			)
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		DestroyWindow();
		return false;
	}
	
	SiwModifyStyle(
		__ESIS_STH_NONE|__ESIS_STV_ITEM
		|__EGBS_SFB_FULL_ROWS|__EGBS_NO_HIDE_SELECTION
			|__EGBS_RESIZING_CELLS_INNER_H|__EGBS_DYNAMIC_RESIZING_H
			|__EGBS_GRIDLINES
			,
		__EGBS_FIXED_SIZE_COLUMNS|__EGBS_FIXED_SIZE_ROWS,
		false
		);
	SiwModifyStyleEx(
		__EGBS_EX_CELL_EXPANDING_INNER|__EGWS_EX_PM_COLORS
		,
		0,
		false
		);
	BseModifyStyle(
		__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW,
		__EGWS_BSE_EDIT_SINGLE_FOCUSED_ONLY|__EGWS_BSE_BUTTONS_PERSISTENT,
		false
		);
	RowRemoveAll( false );
	ColumnRemoveAll( false );
	OuterRowCountTopSet( 1L, false );
	OuterRowHeightSet( true, 0, 0 );
	ColumnAdd( 2, false );
	_SyncInnerRowHeight( false );
CExtGridCellHeader * pCellNameColumn =
		STATIC_DOWNCAST(
			CExtGridCellHeader,
			GridCellGetOuterAtTop(
				0L,
				0L,
				RUNTIME_CLASS(CExtGridCellHeader)
				)
			);
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellNameColumn );
	pCellNameColumn->ExtentSet( 30, -1 );
	pCellNameColumn->ExtentSet( 100, 0);
CExtGridCellHeader * pCellValueColumn =
		STATIC_DOWNCAST(
			CExtGridCellHeader,
			GridCellGetOuterAtTop(
				1L,
				0L,
				RUNTIME_CLASS(CExtGridCellHeader)
				)
			);
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellValueColumn );
	pCellValueColumn->ExtentSet( 30, -1 );
	OnSwUpdateScrollBars();
	SetProportionalColumnWidth();
	OnSwDoRedraw();

	return true;
}

void CExtPropertyGridWnd::_OnSynchronizeInputEnabledCheckMark(
	CExtPropertyItem * pPropertyItem,
	bool bSychronizeChildren // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
HTREEITEM hTreeItem = PropertyItemToTreeItem( pPropertyItem );
	if( hTreeItem != NULL )
	{
		CExtGridCell * pCellCaption = ItemGetCell( hTreeItem, 0L );
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellCaption );
		pCellCaption->ModifyStyle( 0, __EGCS_CHK_MASK );
		if( m_bInputEnabledCheckMarksInCategories && pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyCategory) ) )
		{
			pCellCaption->ModifyStyle( __EGCS_CHK_CHECK );
			if( pPropertyItem->OnInputEnabledGet( false /*m_bInputEnabledNestedFlags*/ ) )
				pCellCaption->ModifyStyle( __EGCS_CHECKED );
			else
				pCellCaption->ModifyStyle( 0, __EGCS_CHECKED );
		} // if( m_bInputEnabledCheckMarksInCategories && pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyCategory) ) )
		CExtGridCell * pCellValue = ItemGetCell( hTreeItem, 1L );
		if( pCellValue != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellValue );
			if( m_bInputEnabledCheckMarksInValues && pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) ) )
			{
				pCellCaption->ModifyStyle( __EGCS_CHK_CHECK );
				if( pPropertyItem->OnInputEnabledGet( false /*m_bInputEnabledNestedFlags*/ ) )
				{
					pCellCaption->ModifyStyle( __EGCS_CHECKED, 0 );
					pCellValue->ModifyStyle( 0, __EGCS_READ_ONLY|__EGCS_NO_INPLACE_CONTROL );
				} // if( pPropertyItem->OnInputEnabledGet( false /*m_bInputEnabledNestedFlags*/ ) )
				else
				{
					pCellCaption->ModifyStyle( 0, __EGCS_CHECKED );
					pCellValue->ModifyStyle( __EGCS_READ_ONLY|__EGCS_NO_INPLACE_CONTROL, 0 );
				} // else from if( pPropertyItem->OnInputEnabledGet( false /*m_bInputEnabledNestedFlags*/ ) )
			} // if( m_bInputEnabledCheckMarksInValues && pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) ) )
		} // if( pCellValue != NULL )
	} // if( hTreeItem != NULL )
	if( bSychronizeChildren )
	{
		INT nIndex, nCount = pPropertyItem->ItemGetCount();
		for( nIndex = 0; nIndex < nCount; nIndex++ )
		{
			CExtPropertyItem * pItem = pPropertyItem->ItemGetAt( nIndex );
			__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
			_OnSynchronizeInputEnabledCheckMark( pItem, true );
		} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	} // if( bSychronizeChildren )
}

void CExtPropertyGridWnd::_OnPropertyInputEnabled(
	bool bInputEnabled,
	LONG nRowNo
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
	if( pPGC == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem );
CExtPropertyItem * pPropertyItem = PropertyItemFromTreeItem( hTreeItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	pPGC->OnPgcPropertyInputEnabled( pPropertyItem, bInputEnabled, m_bInputEnabledNestedFlags );
}

bool CExtPropertyGridWnd::OnTreeGridQueryDrawOutline(
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( dc.GetSafeHdc() != NULL );
	//__EXT_DEBUG_GRID_ASSERT( nVisibleColNo >= 0 );
	//__EXT_DEBUG_GRID_ASSERT( nVisibleRowNo >= 0 );
	__EXT_DEBUG_GRID_ASSERT( nColNo >= 0 );
	__EXT_DEBUG_GRID_ASSERT( nRowNo >= 0 );
	dc;
	nVisibleColNo;
	nVisibleRowNo;
	nColNo;
	nRowNo;
	rcCellExtra;
	rcCell;
	rcVisibleRange;
	dwAreaFlags;
	dwHelperPaintFlags;
	return false;
}

CPoint CExtPropertyGridWnd::FocusSet(
	const POINT & ptNewFocus,
	bool bEnsureVisibleColumn, // = true
	bool bEnsureVisibleRow, // = true
	bool bResetSelectionToFocus, // = true
	bool bRedraw, // = true
	bool * p_bCanceled // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CPoint _ptNewFocus = ptNewFocus;
	if( ColumnCountGet() >= 2 )
		_ptNewFocus.x = 1;
CPoint ptRetVal =
		CExtGridWnd::FocusSet(
			_ptNewFocus,
			bEnsureVisibleColumn,
			bEnsureVisibleRow,
			bResetSelectionToFocus,
			bRedraw,
			p_bCanceled
			);
	if( ! m_bSynchronizingFocus )
	{
		m_bSynchronizingFocus = true;
		CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
		if( pPGC != NULL )
		{
			if( _ptNewFocus.y >= 0 )
			{
				HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( _ptNewFocus.y );
				if( hTreeItem != NULL )
				{
					CExtPropertyItem * pPropertyItem =
						PropertyItemFromTreeItem( hTreeItem );
					if( pPropertyItem != NULL )
					{
						__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
						CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
						pPGC->OnPgcQueryGrids( arrGrids );
						INT nGridIdx;
						for( nGridIdx = 0; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
						{
							CExtPropertyGridWnd * pGrid = arrGrids[ nGridIdx ];
							__EXT_DEBUG_GRID_ASSERT_VALID( pGrid );
							if( pGrid == this )
								continue;
							HTREEITEM htiOther =
								pGrid->PropertyItemToTreeItem(
									pPropertyItem
									);
							if( htiOther == NULL )
								continue;
							HTREEITEM htiFocus =
								pGrid->ItemFocusGet();
							if( htiFocus == htiOther )
								continue;
							pGrid->ItemFocusSet( htiOther, false );
						} // for( nGridIdx = 0; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
					} // if( pPropertyItem != NULL )
				} // if( hTreeItem != NULL )
			} // if( ptNewFocus.y >= 0 )
			pPGC->RedrawFocusDependentChildren();
		} // if( pPGC != NULL )
		m_bSynchronizingFocus = false;
	} // if( ! m_bSynchronizingFocus )
	return ptRetVal;
}

void CExtPropertyGridWnd::OnGbwResizingStateApply(
	bool bHorz,
	LONG nItemNo,
	INT nItemExtent
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CRect rcClient = OnSwGetClientRect();
	if( nItemExtent > (rcClient.Width() - 3) )
		nItemExtent = rcClient.Width() - 3;
	if( nItemExtent < 0 )
		nItemExtent = 0;
	CExtTreeGridWnd::OnGbwResizingStateApply(
		bHorz,
		nItemNo,
		nItemExtent + ( (m_nConstantIndent >= 0) ? m_nConstantIndent : 0 )
		);
	if( m_bSynchronizeColumnWidth )
		_SyncColumnWidths();
}

bool CExtPropertyGridWnd::OnGbwQueryCellGridLines(
	bool bHorz,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( dc.GetSafeHdc() != NULL );
	//__EXT_DEBUG_GRID_ASSERT( nVisibleColNo >= 0 );
	//__EXT_DEBUG_GRID_ASSERT( nVisibleRowNo >= 0 );
	__EXT_DEBUG_GRID_ASSERT( nColNo >= 0 );
	__EXT_DEBUG_GRID_ASSERT( nRowNo >= 0 );
HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
const CExtPropertyItem * pPropertyItem =
		PropertyItemFromTreeItem( hTreeItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	if( pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyCategory)) )
	{
		if( ! ( m_bFullRowSelection /*&& m_bDrawFocusRect*/ ) )
			return false;
	}
	return
		CExtTreeGridWnd::OnGbwQueryCellGridLines(
			bHorz,
			dc,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
}

CRect CExtPropertyGridWnd::OnSwGetClientRect() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CRect rcClient;
	GetClientRect( &rcClient );
	if(		m_wndScrollBarV.GetSafeHwnd() != NULL
		&&	(m_wndScrollBarV.GetStyle()&WS_VISIBLE) != 0
		)
	{
		CRect rcWndScrollBar;
		m_wndScrollBarV.GetWindowRect( &rcWndScrollBar );
		rcClient.right -= rcWndScrollBar.Width();
	}
	return rcClient;
}

COLORREF CExtPropertyGridWnd::OnSiwGetReadOnlyTextColor() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
//	return OnSiwGetSysColor( COLOR_BTNTEXT );
//	return OnSiwGetSysColor( COLOR_3DSHADOW );
//	return OnSiwGetSysColor( COLOR_WINDOW );
	return CExtTreeGridWnd::OnSiwGetReadOnlyTextColor();
}

COLORREF CExtPropertyGridWnd::OnSiwGetReadOnlyBackgroundColor() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
//	return OnSiwGetSysColor( COLOR_WINDOW );
//	return OnSiwGetSysColor( COLOR_3DFACE );
	return CExtTreeGridWnd::OnSiwGetReadOnlyBackgroundColor();
}

void CExtPropertyGridWnd::OnGridCellInputComplete(
	CExtGridCell & _cell,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	HWND hWndInputControl // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtTreeGridWnd::OnGridCellInputComplete(
		_cell,
		nColNo,
		nRowNo,
		nColType,
		nRowType,
		hWndInputControl
		);
	if( nColNo != 1 || nColType != 0 || nRowType != 0 )
		return;
HTREEITEM hTreeItem =
		ItemGetByVisibleRowIndex( nRowNo );
	if( hTreeItem == NULL )
		return;
CExtPropertyItem * pPropertyItem =
		PropertyItemFromTreeItem( hTreeItem );
	if(		pPropertyItem == NULL
		||	(! pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValue)) )
		)
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
CExtSafeString strCompoundPath;
	_cell.ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
	_cell.TextGet( strCompoundPath );
CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
CExtPropertyValueCompound * pCompoundValue =
		DYNAMIC_DOWNCAST(
			CExtPropertyValueCompound,
			pPropertyItem
			);
CExtPropertyValueMixedCompound * pMixedCompound =
		DYNAMIC_DOWNCAST(
			CExtPropertyValueMixedCompound,
			pPropertyItem
			);
	if( pCompoundValue != NULL )
	{
		CExtPropertyValue * pValueParseError = NULL;
		pCompoundValue->SynchronizeChildrenTreeActive(
			LPCTSTR( strCompoundPath ),
			true,
			&pValueParseError
			);
		if( pValueParseError != NULL )
		{
			CExtSafeString strCompoundValue =
				pCompoundValue->BuildCompoundTextActive();
			_cell.TextSet( strCompoundValue );
			return;
		}
	} // if( pCompoundValue != NULL )
	if( pMixedCompound != NULL )
	{
		CExtPropertyValue * pValueParseError = NULL;
		pMixedCompound->SynchronizeChildrenTreeActive(
			LPCTSTR( strCompoundPath ),
			true,
			&pValueParseError
			);
		if( pValueParseError != NULL )
		{
			CExtSafeString strCompoundValue =
				pMixedCompound->BuildCompoundTextActive();
			_cell.TextSet( strCompoundValue );
			return;
		}
	} // if( pMixedCompound != NULL )
	if( ( _cell.GetStyle() & __EGCS_READ_ONLY ) == 0 )
		pPropertyItem->Apply( &_cell );
	_cell.ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
	if( pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValueMixed)) )
	{
		if( pPropertyItem->ChildrenHaveDifferentActiveValues() )
			_cell.ModifyStyleEx( __EGCS_EX_UNDEFINED_ROLE );
		else
			_cell.ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
	} // if( pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValueMixed)) )
	if( pPGC != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
		if( pCompoundValue != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
			pCompoundValue->SynchronizeCompoundParentPathActive();
			pCompoundValue->SynchronizeChildrenTreeActive(
				LPCTSTR( strCompoundPath ),
				false
				);
			pPGC->OnPgcSynchronizeCompoundValue(
				pCompoundValue
				);
		} // if( pCompoundValue != NULL )
		if( pMixedCompound != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
			pMixedCompound->SynchronizeCompoundParentPathActive();
			pMixedCompound->SynchronizeChildrenTreeActive(
				LPCTSTR( strCompoundPath ),
				false
				);
			pPGC->OnPgcSynchronizeCompoundValue(
				pMixedCompound
				);
		} // if( pMixedCompound != NULL )
		CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
		pPGC->OnPgcQueryGrids( arrGrids );
		INT nGridIdx;
		for( nGridIdx = 0; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
		{
			CExtPropertyGridWnd * pGrid = arrGrids[ nGridIdx ];
			__EXT_DEBUG_GRID_ASSERT_VALID( pGrid );
			if( pGrid == this )
				continue;
			HTREEITEM htiOther =
				pGrid->PropertyItemToTreeItem(
					pPropertyItem
					);
			if( htiOther == NULL )
				continue;
			CExtGridCell * pCellOther =
				pGrid->ItemGetCell( htiOther, 1 );
			if( pCellOther == NULL )
				continue;
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellOther );
			if( LPVOID(pCellOther) != LPVOID(&_cell) )
				pCellOther->Assign( _cell );
		} // for( nGridIdx = 0; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
		CExtPropertyItem * pParentItem =
			pPropertyItem->ItemParentGet();
		for( ; pParentItem != NULL; )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pParentItem );
			bool bSyncGrids = false;
			CExtGridCell * pCellSrc = NULL;
			CExtPropertyValueCompound * pCompoundValue =
				DYNAMIC_DOWNCAST(
					CExtPropertyValueCompound,
					pParentItem
					);
			if( pCompoundValue != NULL )
			{
				CExtSafeString strCompoundValue =
					pCompoundValue->BuildCompoundTextActive();
				pCompoundValue->ValueActiveGet()->TextSet(
					strCompoundValue.IsEmpty() ? _T("") : LPCTSTR(strCompoundValue)
					);
				HTREEITEM htiCompound =
					PropertyItemToTreeItem(
						pCompoundValue
						);
				if( htiCompound != NULL )
				{
					pCellSrc =
						ItemGetCell(
							htiCompound,
							1
							);
					__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
					pCellSrc->TextSet( strCompoundValue );
					bSyncGrids = true;
				} // if( htiCompound != NULL )
				pCompoundValue->SynchronizeCompoundParentPathActive();
			} // if( pCompoundValue != NULL )
			CExtPropertyValueMixedCompound * pMixedCompound =
				DYNAMIC_DOWNCAST(
					CExtPropertyValueMixedCompound,
					pParentItem
					);
			if( pMixedCompound != NULL )
			{
				INT nMixedIndex, nMixedCount = pMixedCompound->ItemGetCount();
				for( nMixedIndex = 0; nMixedIndex < nMixedCount; nMixedIndex ++ )
				{
					CExtPropertyItem * pMixedPart =
						pMixedCompound->ItemGetAt( nMixedIndex );
					__EXT_DEBUG_GRID_ASSERT_VALID( pMixedPart );
					CExtPropertyValueCompound * pCompoundValue =
						STATIC_DOWNCAST(
							CExtPropertyValueCompound,
							pMixedPart
							);
					CExtSafeString strCompoundValue =
						pCompoundValue->BuildCompoundTextActive();
					pCompoundValue->ValueActiveGet()->TextSet(
						strCompoundValue.IsEmpty() ? _T("") : LPCTSTR(strCompoundValue)
						);
					pCompoundValue->SynchronizeCompoundParentPathActive();
				} // for( nMixedIndex = 0; nMixedIndex < nMixedCount; nMixedIndex ++ )
				HTREEITEM htiCompound =
					PropertyItemToTreeItem(
						pMixedCompound
						);
				if( htiCompound != NULL )
				{
					pCellSrc =
						ItemGetCell(
							htiCompound,
							1
							);
					__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
					CExtSafeString strCompoundValue =
						pMixedCompound->BuildCompoundTextActive();
					pCellSrc->TextSet( strCompoundValue );
					bSyncGrids = true;
				} // if( htiCompound != NULL )
				//pMixedCompound->SynchronizeCompoundParentPathActive();
			} // if( pMixedCompound != NULL )
			if( bSyncGrids )
			{
				__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
				for( nGridIdx = 0; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
				{
					CExtPropertyGridWnd * pGrid = arrGrids[ nGridIdx ];
					__EXT_DEBUG_GRID_ASSERT_VALID( pGrid );
					HTREEITEM htiOther =
						pGrid->PropertyItemToTreeItem(
							pParentItem
							);
					if( htiOther == NULL )
						continue;
					CExtGridCell * pCellOther =
						pGrid->ItemGetCell( htiOther, 1 );
					if( pCellOther == NULL )
						continue;
					__EXT_DEBUG_GRID_ASSERT_VALID( pCellOther );
					if( LPVOID(pCellOther) != LPVOID(pCellSrc) )
						pCellOther->Assign( *pCellSrc );
					pCellOther->ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
					if( pParentItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValueMixed)) )
					{
						if( pParentItem->ChildrenHaveDifferentActiveValues() )
							pCellOther->ModifyStyleEx( __EGCS_EX_UNDEFINED_ROLE );
						else
							pCellOther->ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
					} // if( pParentItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValueMixed)) )
					if( (pGrid->GetStyle()&WS_VISIBLE) == 0 )
						continue;
					LONG nRowNo = pGrid->ItemGetVisibleIndexOf( htiOther );
					if( nRowNo < 0 )
						continue;
					CRect rc;
					if(	pGrid->GridCellRectsGet(
							1,
							nRowNo,
							0,
							0,
							NULL,
							&rc
							)
						)
					{
						CRect rcClient;
						pGrid->GetClientRect( &rcClient );
						rc.left = rcClient.left;
						rc.right = rcClient.right;
						pGrid->InvalidateRect( &rc );
					}
				} // for( nGridIdx = 0; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
			} // if( bSyncGrids )
			pParentItem =
				pParentItem->ItemParentGet();
		} // for( ; pParentItem != NULL; )
	} // if( pPGC != NULL )
CRect rc;
	if(	GridCellRectsGet(
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			NULL,
			&rc
			)
		)
	{
		CRect rcClient;
		GetClientRect( &rcClient );
		rc.left = rcClient.left;
		rc.right = rcClient.right;
		InvalidateRect( &rc );
	}
	if( pPGC != NULL )
		pPGC->OnPgcInputComplete(
			this,
			pPropertyItem
			);
}

COLORREF CExtPropertyGridWnd::OnGridCellQueryTextColor(
	const CExtGridCell & _cell,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&_cell) );
	if(		nColType == 0
		&&	nRowType == 0
		&&	nColNo < 2
		&&	ColumnCountGet() == 2
		)
	{
		HTREEITEM hTreeItem =
			ItemGetByVisibleRowIndex( nRowNo );
		if( hTreeItem != NULL )
		{
			const CExtPropertyItem * pPropertyItem =
				PropertyItemFromTreeItem( hTreeItem );
			if( pPropertyItem != NULL )
			{
				if( pPropertyItem->IsKindOf(
						RUNTIME_CLASS(CExtPropertyValue)
						)
					)
				{
					const CExtGridCell * pCell =
						pPropertyItem->ValueActiveGet();
					if( pCell != NULL )
					{
						__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
						if(		m_bHighlightReadOnlyValues
							&&	(pCell->GetStyle()&__EGCS_READ_ONLY) != 0
							)
						{
							if( nColNo == 0 && FocusGet().y == nRowNo )
							{
								if( (dwHelperPaintFlags&__EGCPF_FOCUSED_CONTROL) != 0 )
									return OnSiwGetSysColor( COLOR_HIGHLIGHTTEXT );
								return OnSiwGetSysColor( COLOR_3DDKSHADOW );
							}
							return OnSiwGetSysColor( COLOR_3DSHADOW );
						} // if( m_bHighlightReadOnlyValues ...
					} // if( pCell != NULL )
				}
			} // if( pPropertyItem != NULL )
		} // if( hTreeItem != NULL )
	}
	return
		CExtTreeGridWnd::OnGridCellQueryTextColor(
			_cell,
			dc,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			dwAreaFlags,
			dwHelperPaintFlags
			);
}

HFONT CExtPropertyGridWnd::OnGridCellQueryFont(
	const CExtGridCell & _cell,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	DWORD dwAreaFlags,
	bool & bFontMustBeDestroyed,
	DWORD dwHelperPaintFlags // = 0
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&_cell) );
	if(		nColType == 0
		&&	nRowType == 0
		&&	nColNo < 2
		&&	ColumnCountGet() == 2
		)
	{
		HTREEITEM hTreeItem =
			ItemGetByVisibleRowIndex( nRowNo );
		if( hTreeItem != NULL )
		{
			const CExtPropertyItem * pPropertyItem =
				PropertyItemFromTreeItem( hTreeItem );
			if( pPropertyItem != NULL )
			{
				if( pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) ) )
				{
					if(		( nColNo == 0 && m_bMakeBoldModifiedNames )
						||	( nColNo == 1 && m_bMakeBoldModifiedValues )
						)
					{
						if( pPropertyItem->IsModified() )
						{
							bFontMustBeDestroyed = false;
							return (HFONT)PmBridge_GetPM()->m_FontBold.GetSafeHandle();
						}
					}
				}
			} // if( pPropertyItem != NULL )
		} // if( hTreeItem != NULL )
	}
	return
		CExtTreeGridWnd::OnGridCellQueryFont(
			_cell,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			dwAreaFlags,
			bFontMustBeDestroyed,
			dwHelperPaintFlags
			);
}

bool CExtPropertyGridWnd::OnGridPaintCellTextHook(
	bool bPostNotification,
	const CExtGridCell & _cell,
	const RECT & rcCellText,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		(! m_bFullRowSelection )
		&&	bPostNotification
		&&	nColNo == 0
		&&	_cell.IsKindOf( RUNTIME_CLASS(CExtPropertyGridCellArea) )
		&&	FocusGet().y == nRowNo
		&&	(dwHelperPaintFlags&__EGCPF_FOCUSED_CONTROL) != 0
		)
	{
		HTREEITEM hTreeItem =
			ItemGetByVisibleRowIndex( nRowNo );
		if( hTreeItem != NULL )
		{
			const CExtPropertyItem * pPropertyItem =
				PropertyItemFromTreeItem( hTreeItem );
			if( pPropertyItem != NULL )
			{
				if( pPropertyItem->IsKindOf(
						RUNTIME_CLASS(CExtPropertyCategory)
						)
					)
				{ // if category item
					CRect rcFocusRect(
						rcCellText.left - 2,
						rcCellText.top,
						rcCellText.left +
							_cell.OnMeasureTextSize(
								*this,
								dc,
								nVisibleColNo,
								nVisibleRowNo,
								nColNo, 
								nRowNo,
								nColType,
								nRowType,
								rcCellExtra,
								rcCell,
								rcCellText,
								dwAreaFlags
								).cx + 2,
						rcCellText.bottom
						);
					dc.DrawFocusRect( &rcFocusRect );
				} // if category item
			} // if( pPropertyItem != NULL )
		} // if( hTreeItem != NULL )
		return true;
	}
	return
		CExtTreeGridWnd::OnGridPaintCellTextHook(
			bPostNotification,
			_cell,
			rcCellText,
			dc,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
}

bool CExtPropertyGridWnd::SelectionGetForCell(
	LONG nColNo,
	LONG nRowNo
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(! m_bFullRowSelection )
	{
		if( nColNo == 1 )
		{
			return false;
		}
		else if( nColNo == 0 )
		{
			HTREEITEM hTreeItem =
				ItemGetByVisibleRowIndex( nRowNo );
			if( hTreeItem != NULL )
			{
				const CExtPropertyItem * pPropertyItem =
					PropertyItemFromTreeItem( hTreeItem );
				if( pPropertyItem != NULL )
				{
					if( pPropertyItem->IsKindOf(
							RUNTIME_CLASS(CExtPropertyCategory)
							)
						)
						return false;
				} // if( pPropertyItem != NULL )
			} // if( hTreeItem != NULL )
		} // if( nColNo == 0 )
	} // if(! m_bFullRowSelection )
	return
		CExtTreeGridWnd::SelectionGetForCell(
			nColNo,
			nRowNo
			);
}

bool CExtPropertyGridWnd::OnGbwBeginEdit(
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcInplaceControl,
	bool bContinueMsgLoop, // = true
	__EXT_MFC_SAFE_LPCTSTR strStartEditText, // = NULL
	HWND hWndParentForEditor // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_nLastEndEditKey = 0;
CPoint ptFocus = FocusGet();
	if( ptFocus.y != nRowNo )
	{
		ptFocus.y = nRowNo;
		bool bCanceled = false;
		FocusSet( ptFocus, true, true, true, true, &bCanceled );
		if( bCanceled )
			return false;
	} // if( ptFocus.y != nRowNo )
bool bRetVal =
		CExtTreeGridWnd::OnGbwBeginEdit(
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcInplaceControl,
			bContinueMsgLoop,
			strStartEditText,
			hWndParentForEditor
			);
	return bRetVal;
}

bool CExtPropertyGridWnd::OnGbwAnalyzeCellMouseClickEvent(
	UINT nChar, // VK_LBUTTON, VK_RBUTTON or VK_MBUTTON only
	UINT nRepCnt, // 0 - button up, 1 - single click, 2 - double click, 3 - post single click & begin editing
	UINT nFlags, // mouse event flags
	CPoint point // mouse pointer in client coordinates
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( 0 <= nRepCnt && nRepCnt <= 3 );
bool bRetVal =
		CExtTreeGridWnd::OnGbwAnalyzeCellMouseClickEvent(
			nChar,
			nRepCnt,
			nFlags,
			point
			);
	if( nChar == VK_LBUTTON && nRepCnt == 2 )
	{
		CExtGridHitTestInfo htInfo( point );
		HitTest( htInfo, false, true );
		if(		(! htInfo.IsHoverEmpty() )
			&&	htInfo.IsValidRect()
			&&	htInfo.m_nColNo == 0
			&&	htInfo.GetInnerOuterTypeOfColumn() == 0
			&&	htInfo.GetInnerOuterTypeOfRow() == 0
			)
		{
			HTREEITEM hTreeItem =
				ItemGetByVisibleRowIndex( htInfo.m_nRowNo );
			if( hTreeItem != NULL )
			{
				CExtPropertyItem * pPropertyItem =
					PropertyItemFromTreeItem( hTreeItem );
				if(		pPropertyItem != NULL
					&&	pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) )
					)
				{
					MSG msg;
					while( ::PeekMessage( &msg, m_hWnd, WM_MOUSEFIRST, WM_MOUSELAST, PM_REMOVE ) );
					while( ::PeekMessage( &msg, m_hWnd, WM_SETFOCUS, WM_KILLFOCUS, PM_REMOVE ) );
					while( ::PeekMessage( &msg, m_hWnd, WM_MOUSEACTIVATE, WM_MOUSEACTIVATE, PM_REMOVE ) );
					while( ::PeekMessage( &msg, m_hWnd, WM_NCACTIVATE, WM_NCACTIVATE, PM_REMOVE ) );
					while( ::PeekMessage( &msg, m_hWnd, WM_NCHITTEST, WM_NCHITTEST, PM_REMOVE ) );
					::Sleep( ::GetDoubleClickTime() / 3 );
					while( ::PeekMessage( &msg, m_hWnd, WM_MOUSEFIRST, WM_MOUSELAST, PM_REMOVE ) );
					while( ::PeekMessage( &msg, m_hWnd, WM_SETFOCUS, WM_KILLFOCUS, PM_REMOVE ) );
					while( ::PeekMessage( &msg, m_hWnd, WM_MOUSEACTIVATE, WM_MOUSEACTIVATE, PM_REMOVE ) );
					while( ::PeekMessage( &msg, m_hWnd, WM_NCACTIVATE, WM_NCACTIVATE, PM_REMOVE ) );
					while( ::PeekMessage( &msg, m_hWnd, WM_NCHITTEST, WM_NCHITTEST, PM_REMOVE ) );
					EditCell( 1, htInfo.m_nRowNo );
					return true;
				}
			}
		}
	} // if( nChar == VK_LBUTTON && nRepCnt == 2 )
	else if( nChar == VK_RBUTTON && nRepCnt == 1 )
	{
		CExtGridHitTestInfo htInfo( point );
		HitTest( htInfo, false, false );
		if(		(! htInfo.IsHoverEmpty() )
			&&	htInfo.GetInnerOuterTypeOfColumn() == 0
			&&	htInfo.GetInnerOuterTypeOfRow() == 0
			)
		{
			CPoint ptFocus = FocusGet();
			if( ptFocus.y != htInfo.m_nRowNo )
			{
				HWND hWndFocus = ::GetFocus();
				if( hWndFocus != m_hWnd )
					SetFocus();
				ptFocus.y = htInfo.m_nRowNo;
				FocusSet( ptFocus );
				bRetVal = true;
			}
		}
	} // else if( nChar == VK_RBUTTON && nRepCnt == 1 )
	return bRetVal;
}

bool CExtPropertyGridWnd::OnGridCellInplaceControlWindowProc(
	CExtGridCell & _cell,
	LRESULT & lResult,
	UINT nMessage,
	WPARAM wParam,
	LPARAM lParam,
	HWND hWndInplaceControl,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcInplaceControl
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	switch( nMessage )
	{
	case WM_KEYDOWN:
		if( wParam == VK_TAB )
		{
			lResult = 0;
			HWND hWndOwn = m_hWnd;
			::SendMessage(
				hWndInplaceControl,
				WM_KEYDOWN,
				VK_RETURN,
				0L
				);
			if( ! ::IsWindow( hWndOwn ) )
				return true;
			HTREEITEM hTreeItem = ItemFocusGet();
			if( hTreeItem == NULL )
				return true;
			bool bShift = ( (::GetAsyncKeyState(VK_SHIFT)&0x8000) != 0 ) ? true : false;
			if(		(! bShift )
				&&	ItemGetChildCount( hTreeItem ) > 0 
				&&	(! ItemIsExpanded( hTreeItem ) )
				)
				ItemExpand( hTreeItem, TVE_EXPAND, true );
			LONG nRowNo = ItemGetVisibleIndexOf( hTreeItem );
			if( bShift )
			{
				if( nRowNo == 0 )
				{
					::SendMessage( ::GetParent( ::GetParent( m_hWnd ) ), WM_NEXTDLGCTL, bShift ? (!0) : 0, 0 );
					return true;
				}
				nRowNo --;
				hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
				__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
				ItemFocusSet( hTreeItem );
				CExtPropertyItem * pPropertyItem = PropertyItemFromTreeItem( hTreeItem );
				if( pPropertyItem == NULL )
					return true;
				if( pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) ) )
					EditCell();
				return true;
			}
			LONG nRowCount = RowCountGet();
			if( nRowNo >= (nRowCount-1) )
			{
				::SendMessage( ::GetParent( ::GetParent( m_hWnd ) ), WM_NEXTDLGCTL, bShift ? (!0) : 0, 0 );
				return true;
			}
			nRowNo ++;
			hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
			__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
			ItemFocusSet( hTreeItem );
			CExtPropertyItem * pPropertyItem = PropertyItemFromTreeItem( hTreeItem );
			if( pPropertyItem == NULL )
				return true;
			if( pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) ) )
				EditCell();
			return true;
		}
	break;
	} // switch( nMessage )
	return
		CExtTreeGridWnd::OnGridCellInplaceControlWindowProc(
			_cell,
			lResult,
			nMessage,
			wParam,
			lParam,
			hWndInplaceControl,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcInplaceControl
			);
}

bool CExtPropertyGridWnd::OnGbwAnalyzeCellKeyEvent(
	bool bKeyDownEvent, // true - key-down event, false - key-up event
	UINT nChar, // virtual key code
	UINT nRepCnt, // key-down/key-up press count
	UINT nFlags // key-down/key-up event flags
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( bKeyDownEvent )
	{
		switch( nChar )
		{
//		case VK_APPS:
//			return true;
		case VK_LEFT:
		case VK_RIGHT:
		{
			bool bAlt = ( (::GetAsyncKeyState(VK_MENU)&0x8000) != 0 ) ? true : false;
			bool bCtrl = ( (::GetAsyncKeyState(VK_CONTROL)&0x8000) != 0 ) ? true : false;
			bool bShift = ( (::GetAsyncKeyState(VK_SHIFT)&0x8000) != 0 ) ? true : false;
			bool bCtrlOnly = bCtrl && (!bAlt) && (!bShift);
			if( bCtrlOnly )
			{
				if( m_nPxResizeStepByKeyboard > 0 && ColumnCountGet() == 2 )
				{
					INT nExtentLeftCurrent, nExtentRightCurrent,
						nExtentLeftMin, nExtentRightMin,
						nExtentLeftMax, nExtentRightMax;
					GridCellGetOuterAtTop(0,0)->ExtentGet( nExtentLeftCurrent );
					GridCellGetOuterAtTop(1,0)->ExtentGet( nExtentRightCurrent );
					GridCellGetOuterAtTop(0,0)->ExtentGet( nExtentLeftMin, -1 );
					GridCellGetOuterAtTop(1,0)->ExtentGet( nExtentRightMin, -1 );
					GridCellGetOuterAtTop(0,0)->ExtentGet( nExtentLeftMax, 1 );
					GridCellGetOuterAtTop(1,0)->ExtentGet( nExtentRightMax, 1 );
					if(		nExtentRightMin < nExtentRightMax
						&&	nExtentRightMin <= nExtentRightCurrent
						&&	nExtentRightCurrent <= nExtentRightMax
						&&	nExtentLeftMin < nExtentLeftMax
						&&	nExtentLeftMin <= nExtentLeftCurrent
						&&	nExtentLeftCurrent <= nExtentLeftMax
						)
					{ // if resizing is allowed
						INT nExtentLeftNew = nExtentLeftCurrent,
							nExtentRightNew = nExtentRightCurrent;
						if( nChar == VK_LEFT )
						{
							nExtentLeftNew -= m_nPxResizeStepByKeyboard;
							nExtentRightNew += m_nPxResizeStepByKeyboard;
						} // if( nChar == VK_LEFT )
						else
						{
							__EXT_DEBUG_GRID_ASSERT( nChar == VK_RIGHT );
							nExtentLeftNew += m_nPxResizeStepByKeyboard;
							nExtentRightNew -= m_nPxResizeStepByKeyboard;
						} // else from if( nChar == VK_LEFT )
						if( nExtentLeftNew < nExtentLeftMin )
							nExtentLeftNew = nExtentLeftMin;
						if( nExtentLeftNew > nExtentLeftMax )
							nExtentLeftNew = nExtentLeftMax;
						if( nExtentRightNew < nExtentRightMin )
							nExtentRightNew = nExtentRightMin;
						if( nExtentRightNew > nExtentRightMax )
							nExtentRightNew = nExtentRightMax;
						if(		nExtentLeftNew != nExtentLeftCurrent
							&&	nExtentRightNew != nExtentRightCurrent
							)
						{ // if extents are inequal
							GridCellGetOuterAtTop(0,0)->ExtentSet( nExtentLeftNew );
							GridCellGetOuterAtTop(1,0)->ExtentSet( nExtentRightNew );
							_DoColumnLayout();
							if( m_bSynchronizeColumnWidth )
								_SyncColumnWidths();
							if( IsWindowVisible() )
								Invalidate();
						} // if extents are unequal
					} // if resizing is allowed
				} // if( m_nPxResizeStepByKeyboard > 0 && ColumnCountGet() == 2 )
				return true;
			} // if( bCtrlOnly )
		}
		break; // VK_LEFT, VK_RIGHT
		case VK_TAB:
			{
				HTREEITEM hTreeItem = ItemFocusGet();
				if( hTreeItem != NULL )
				{
					LONG nRowNo = ItemGetVisibleIndexOf( hTreeItem );
					bool bSkipEditing = false;
 					HWND hWndInplaceActive = GetSafeInplaceActiveHwnd();
					CExtPropertyItem * pPropertyItem = PropertyItemFromTreeItem( hTreeItem );
					bool bShift = ( (::GetAsyncKeyState(VK_SHIFT)&0x8000) != 0 ) ? true : false;
					if(		hWndInplaceActive == NULL
						&&	pPropertyItem != NULL
						)
					{
						if( bShift && nRowNo == 0L )
						{
							::SendMessage( ::GetParent( ::GetParent( m_hWnd ) ), WM_NEXTDLGCTL, bShift ? (!0) : 0, 0 );
							return true;
						}
						if(		ItemGetChildCount( hTreeItem ) > 0 
							&&	(! ItemIsExpanded( hTreeItem ) )
							)
						{
							ItemExpand( hTreeItem, TVE_EXPAND, true );
							return true;
						}
						if(		(! bShift )
							&&	pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) )
							)
						{
							if( EditCell() )
								return true;
							bSkipEditing = true;
						}
					}
					if(		pPropertyItem != NULL
						&&	(	( ! pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) ) )
							||	hWndInplaceActive == NULL
							)
						)
					{
						if( bShift )
						{
							if( nRowNo == 0 )
								return true;
							nRowNo --;
							hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
							__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
							ItemFocusSet( hTreeItem );
							if( bSkipEditing )
								return true;
							pPropertyItem = PropertyItemFromTreeItem( hTreeItem );
							if( pPropertyItem == NULL )
								return true;
							if( pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) ) )
								EditCell();
							return true;
						}
						LONG nRowCount = RowCountGet();
						if( nRowNo >= (nRowCount-1) )
							return true;
						nRowNo ++;
						hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
						__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
						ItemFocusSet( hTreeItem );
						if( bSkipEditing )
							return true;
						pPropertyItem = PropertyItemFromTreeItem( hTreeItem );
						if( pPropertyItem == NULL )
							return true;
						if( pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) ) )
							EditCell();
						return true;
					}
				}
			}
		case VK_RETURN:
		case VK_SPACE:
		{
			HTREEITEM hTreeItem = ItemFocusGet();
			if( hTreeItem != NULL )
			{
				CExtPropertyItem * pPropertyItem =
					PropertyItemFromTreeItem( hTreeItem );
				if( pPropertyItem != NULL )
				{
					if( pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) ) )
					{
						EditCell( 1, FocusGet().y );
						return true;
					}
					else if( pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyCategory) ) )
					{
						ItemExpand( hTreeItem );
					}
				} // if( pPropertyItem != NULL )
			} // if( hTreeItem != NULL )
		}
		break; // VK_TAB, VK_RETURN, VK_SPACE
		} // switch( nChar )
	} // if( bKeyDownEvent )
	else
	{
		if(		nChar == VK_SPACE 
			||	nChar == VK_RETURN
			)
		{
			HTREEITEM hTreeItem = ItemFocusGet();
			if( hTreeItem != NULL )
			{
				CExtPropertyItem * pPropertyItem =
					PropertyItemFromTreeItem( hTreeItem );
				if( pPropertyItem != NULL )
				{
					if( pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) ) )
					{
						CExtGridCell * pCell = ItemGetCell( hTreeItem, 1 );
						if( pCell != NULL )
						{
							__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
							DWORD dwCellStyle = pCell->GetStyle();
							if(		(dwCellStyle&__EGCS_CHK_MASK) != 0 
								&&	(dwCellStyle&__EGCS_READ_ONLY) == 0
								&&	!(nChar == VK_RETURN && (dwCellStyle&__EGCS_NO_INPLACE_CONTROL) == 0)
								)
								{
									CPoint ptFocus = FocusGet();
									__EXT_DEBUG_GRID_ASSERT( ptFocus.x >= 0 && ptFocus.y >= 0 );
									CRect rcCell( 0, 0, 0, 0 ), rcCellExtra( 0, 0, 0, 0 );
									GridCellRectsGet(
												1,
										ptFocus.y,
												0,
												0,
										&rcCell,
										&rcCellExtra
										);
									bool bChecked =
										( (dwCellStyle&__EGCS_CHECKED) != 0 ) ? true : false;
									if( bChecked )
										pCell->OnSetCheck( 
											0, 
											*this, 
											1,
											ptFocus.y,
											0,
											0,
											rcCellExtra, 
											rcCell 
											);
									else
										pCell->OnSetCheck( 
											1, 
											*this, 
											1,
											ptFocus.y,
											0,
											0,
											rcCellExtra, 
											rcCell 
											);
									
									InvalidateRect( &rcCellExtra );
										OnGridCellInputComplete(
											*pCell,
											1,
										ptFocus.y,
											0,
											0
											);
								return true;
							}
						} // if( pCell != NULL )
					}
				} // if( pPropertyItem != NULL )
			} // if( hTreeItem != NULL )
		} // if( nChar == VK_SPACE )
//		if( nChar == VK_APPS )
//		{
//			return true;
//		} // if( nChar == VK_APPS )
	} // else from if( bKeyDownEvent )
bool bRetVal =
		CExtTreeGridWnd::OnGbwAnalyzeCellKeyEvent(
			bKeyDownEvent,
			nChar,
			nRepCnt,
			nFlags
			);
	return bRetVal;
}

void CExtPropertyGridWnd::OnSiwDrawFocusRect(
	CDC &dc,
	LPCRECT pRect,
	CObject * pObjSrc, // = NULL
	LPARAM lParam // = 0L
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&dc) );
	__EXT_DEBUG_GRID_ASSERT( dc.GetSafeHdc() != NULL );
	if( pObjSrc != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pObjSrc );
		if(		pObjSrc->IsKindOf( RUNTIME_CLASS(CExtPropertyGridWnd) )
			||	pObjSrc->IsKindOf( RUNTIME_CLASS(CExtGridCell) )
			)
		{
			if( ! m_bDrawFocusRect )
				return;
			if( ! m_bFullRowSelection )
				return;
		}			
	}
	CExtTreeGridWnd::OnSiwDrawFocusRect(
		dc,
		pRect,
		pObjSrc,
		lParam
		);
}

void CExtPropertyGridWnd::OnGbwAdjustRects(
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	RECT & rcCellExtraA,
	RECT & rcCellA
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
bool bOutlinedColumn =
		(	nColType == 0
		&&	nRowType == 0
		&&	OnTreeGridQueryColumnOutline( nColNo )
		) ? true : false;
	if(		bOutlinedColumn
		&&	m_nConstantIndent >= 0
		)
		rcCellA.left += m_nConstantIndent;
	else
		CExtTreeGridWnd::OnGbwAdjustRects(
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtraA,
			rcCellA
			);
	if( nColType == 0 && nRowType == 0 )
	{
		HTREEITEM hTreeItem =
			ItemGetByVisibleRowIndex( nRowNo );
		__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
		const CExtPropertyItem * pPropertyItem =
			PropertyItemFromTreeItem( hTreeItem );
		__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
		if( pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyCategory)) )
		{
			CRect rcClient = OnSwGetClientRect();
			if( nColNo == 1 )
				rcCellA.left = rcCellExtraA.left = rcClient.right;
			else
				rcCellA.right = rcCellExtraA.right = rcClient.right;
		} // if( pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyCategory)) )
		if(		bOutlinedColumn
			&&	m_nConstantIndent < 0
			)
		{
			// These two lines make all the property values indented into one additional level:
			//if( ! pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyCategory)) )
			//	rcCellA.left += GetTreeData().m_nIndentPxDefault;
			const CExtPropertyItem * pWalk = pPropertyItem;
			for( ; true; )
			{
				const CExtPropertyItem * pParent = pWalk->ItemParentGet();
				if( pParent == NULL )
					break;
				if(		(	pWalk->IsKindOf(RUNTIME_CLASS(CExtPropertyValueCompound))
						||	pWalk->IsKindOf(RUNTIME_CLASS(CExtPropertyValueMixedCompound))
						)
					&&	pParent->IsKindOf( RUNTIME_CLASS(CExtPropertyStore) )
					)
				{
					rcCellA.left += GetTreeData().m_nIndentPxDefault;
					break;
				}
				pWalk = pParent;
			} // for( ; true; )
		}
	} // if( nColType == 0 && nRowType == 0 )
}

void CExtPropertyGridWnd::_InitTreeGridWnd()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtTreeGridWnd::_InitTreeGridWnd();
	GetTreeData().m_nIndentPxDefault = __EXT_MGC_PROPERTY_GRID_DEFAULT_LEVEL_INDENT;
}

void CExtPropertyGridWnd::_SyncInnerRowHeight(
	bool bSyncScrollingParms // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
INT nRowHeightCurrent =
		DefaultRowHeightGet();
INT nRowHeightDesired = 0;
	{ // BLOCK: for DC
		CWindowDC dc( NULL );
		static CExtSafeString g_sTestText( _T("AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789;[]{}\\/=+-_*&^%$#@!~") );
		INT nRowHeightDesiredNormal =
			CExtPaintManager::stat_CalcTextDimension(
				dc,
				OnSiwGetDefaultFont(),
				g_sTestText
				).Height();
		INT nRowHeightDesiredBold =
			CExtPaintManager::stat_CalcTextDimension(
				dc,
				PmBridge_GetPM()->m_FontBold,
				g_sTestText
				).Height();
		nRowHeightDesired =
			max( nRowHeightDesiredNormal, nRowHeightDesiredBold );
		nRowHeightDesired += 6;
	} // BLOCK: for DC
	if( nRowHeightCurrent != nRowHeightDesired )
		DefaultRowHeightSet(
			nRowHeightDesired,
			bSyncScrollingParms
			);
}

void CExtPropertyGridWnd::_SyncColumnWidths()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ColumnCountGet() != 2 )
		return;
CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
	if( pPGC == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
	pPGC->OnPgcQueryGrids( arrGrids );
INT nOwnExtentLeft, nOwnExtentRight;
	GridCellGetOuterAtTop(0,0)->ExtentGet( nOwnExtentLeft );
	GridCellGetOuterAtTop(1,0)->ExtentGet( nOwnExtentRight );
INT nGridIdx = 0;
	for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
	{
		CExtPropertyGridWnd * pGrid = arrGrids[ nGridIdx ];
		__EXT_DEBUG_GRID_ASSERT_VALID( pGrid );
		if( pGrid == this )
			continue;
		if( pGrid->ColumnCountGet() != 2 )
			continue;
		pGrid->GridCellGetOuterAtTop(0,0)->ExtentSet( nOwnExtentLeft );
		pGrid->GridCellGetOuterAtTop(1,0)->ExtentSet( nOwnExtentRight );
		//pGrid->_DoColumnLayout();
	} // for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
}

void CExtPropertyGridWnd::_DoColumnLayout()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CRect rcClient = OnSwGetClientRect();
	_DoColumnLayout( rcClient );
}

void CExtPropertyGridWnd::_DoColumnLayout(
	const CRect & rcClient
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
int nColCount = ColumnCountGet();
	if( nColCount == 0 )
		return;
INT nAvailExtent = rcClient.Width();
	if( nAvailExtent < 0 )
		nAvailExtent = 0;
INT		i, nSummaryExtentCurrent = 0, nNumberToProcess = 0,
		nSummaryExtentMin = 0, nSummaryExtentMax = 0;
	for( i = 0; i < nColCount; i++ )
	{
		CExtGridCell * pHeaderCell =
			GridCellGetOuterAtTop( i, 0 );
		__EXT_DEBUG_GRID_ASSERT_VALID( pHeaderCell );
		INT nExtentCurrent, nExtentMin, nExtentMax;
		pHeaderCell->ExtentGet( nExtentCurrent, 0 );
		pHeaderCell->ExtentGet( nExtentMin, -1 );
		pHeaderCell->ExtentGet( nExtentMax, 1 );
		if( i == 0 && m_nConstantIndent >= 0 )
		{
			__EXT_DEBUG_GRID_ASSERT( nExtentMax > m_nConstantIndent );
			if( nExtentMin > m_nConstantIndent )
			{
				nExtentMin = m_nTrackExtentCurrent;
				pHeaderCell->ExtentSet( nExtentMin, -1 );
			} // if( nExtentMin > m_nConstantIndent )
//			else
//			{
//				nExtentMin = 30;
//				pHeaderCell->ExtentSet( nExtentMin, -1 );
//				if( nExtentCurrent < nExtentMin )
//					nExtentCurrent = nExtentMin;
//			} // else from if( nExtentMin > m_nConstantIndent )
			if( nExtentCurrent < m_nConstantIndent )
			{
				nExtentCurrent = m_nConstantIndent;
				pHeaderCell->ExtentSet( nExtentCurrent, 0 );
			} // if( nExtentCurrent < m_nConstantIndent )
		} // if( i == 0 && m_nConstantIndent >= 0 )
		nSummaryExtentCurrent += nExtentCurrent;
		nSummaryExtentMin += nExtentMin;
		nSummaryExtentMax += nExtentMax;
		if( nExtentMin != nExtentMax )
			nNumberToProcess ++;
	} // for( i = 0; i < nColCount; i++ )
	if(		nSummaryExtentCurrent == nAvailExtent
		||	nNumberToProcess == 0
		)
		return;
INT nSpaceRequest = nAvailExtent - nSummaryExtentCurrent;
INT nAdvance = ( nSpaceRequest > 0 ) ? 1 : (-1);
	if( nAdvance < 0 )
	{
		if( nSummaryExtentCurrent == nSummaryExtentMin )
			return;
	} // if( nAdvance < 0 )
	else
	{
		if( nSummaryExtentCurrent == nSummaryExtentMax )
			return;
	} // else from if( nAdvance < 0 )
	for( ; true; )
	{
		nNumberToProcess = 0;
		for( i = 0; i < nColCount; i++ )
		{
			if( nSpaceRequest == 0 )
				break;
			CExtGridCell * pHeaderCell =
				GridCellGetOuterAtTop( i, 0 );
			__EXT_DEBUG_GRID_ASSERT_VALID( pHeaderCell );
			INT nExtentCurrent, nExtentMin, nExtentMax;
			pHeaderCell->ExtentGet( nExtentCurrent, 0 );
			pHeaderCell->ExtentGet( nExtentMin, -1 );
			pHeaderCell->ExtentGet( nExtentMax, 1 );
			if(		nExtentMin == nExtentMax
				||	(	nAdvance < 0
					&&	nExtentCurrent == nExtentMin
					)
				||	(	nAdvance > 0
					&&	nExtentCurrent == nExtentMax
					)
				)
				continue;
			nNumberToProcess ++;
			if( nAdvance < 0 )
			{
				pHeaderCell->ExtentSet( nExtentCurrent-1, 0 );
				nSpaceRequest++;
			} // if( nAdvance < 0 )
			else
			{
				pHeaderCell->ExtentSet( nExtentCurrent+1, 0 );
				nSpaceRequest--;
			} // else from if( nAdvance < 0 )
		} // for( i = 0; i < nColCount; i++ )
		if( nNumberToProcess == 0 )
			break;
		if( nSpaceRequest == 0 )
			break;
	} // for( ; true; )
}

void CExtPropertyGridWnd::OnSiwPaintBackground(
	CDC & dc,
	bool bFocusedControl
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtTreeGridWnd::OnSiwPaintBackground(
		dc,
		bFocusedControl
		);
	if(		m_bThemeBackground
		&&	(	(! PmBridge_GetPM()->GetCb2DbTransparentMode( (CObject*)this ) )
			||	(! PmBridge_GetPM()->PaintDockerBkgnd(
					true,
					dc,
					(CWnd*)this
				) )
			)
		)
	{
		CRect rcClient = OnSwGetClientRect();
		if( dc.RectVisible( &rcClient ) )
			dc.FillSolidRect(
				&rcClient,
				PmBridge_GetPM()->GetColor(
					CExtPaintManager::CLR_WRB_FRAME
				)
			);
	}
}

CRect CExtPropertyGridWnd::OnSwRecalcLayout(
	bool bDoLayout,
	LPCRECT pRectClientSrc // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CRect rcRetVal =
		CExtTreeGridWnd::OnSwRecalcLayout(
			bDoLayout,
			pRectClientSrc
			);
	if( bDoLayout )
		_DoColumnLayout( /*rcRetVal*/ );
	return rcRetVal;
}

bool CExtPropertyGridWnd::OnSwHasScrollBar( bool bHorz ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( bHorz )
		return false;
	return CExtTreeGridWnd::OnSwHasScrollBar( false );
}

CSize CExtPropertyGridWnd::OnGbwCellJoinQueryInfo(
	LONG nColNo,
	LONG nRowNo,
	INT nColType, // = 0
	INT nRowType // = 0
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nColNo;
	nRowNo;
	nColType;
	nRowType;
	return CSize( 1, 1 );
}

CScrollBar * CExtPropertyGridWnd::GetScrollBarCtrl(int nBar) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_hWnd == NULL || (! ::IsWindow(m_hWnd) ) )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT( nBar == SB_HORZ || nBar == SB_VERT );
	if( nBar == SB_VERT )
	{
		if( m_wndScrollBarV.GetSafeHwnd() != NULL )
			return ( const_cast < CExtScrollBar * > ( &m_wndScrollBarV ) );
	}
	return NULL;
}

void CExtPropertyGridWnd::PreSubclassWindow()
{
	CExtTreeGridWnd::PreSubclassWindow();
}

void CExtPropertyGridWnd::PostNcDestroy()
{
	if( m_bAutoDeleteWindow )
		delete this;
}

void CExtPropertyGridWnd::SetProportionalColumnWidth(
	double lfPercentLeft // = 0.5 // 0.0 < lfPercentLeft < 1.0
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ! ( 0.0 < lfPercentLeft && lfPercentLeft < 1.0 ) )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return;
	}
	if( GetSafeHwnd() == NULL )
		return;
	if( ColumnCountGet() != 2 )
		return;
	_DoColumnLayout();
INT nExtentLeftCurrent, nExtentRightCurrent,
		nExtentLeftMin, nExtentRightMin,
		nExtentLeftMax, nExtentRightMax;
	GridCellGetOuterAtTop(0,0)->ExtentGet( nExtentLeftCurrent );
	GridCellGetOuterAtTop(1,0)->ExtentGet( nExtentRightCurrent );
	GridCellGetOuterAtTop(0,0)->ExtentGet( nExtentLeftMin, -1 );
	GridCellGetOuterAtTop(1,0)->ExtentGet( nExtentRightMin, -1 );
	GridCellGetOuterAtTop(0,0)->ExtentGet( nExtentLeftMax, 1 );
	GridCellGetOuterAtTop(1,0)->ExtentGet( nExtentRightMax, 1 );
INT nExtentTotal = nExtentLeftCurrent + nExtentRightCurrent;
	if( nExtentTotal <= 0 )
		return;
INT nExtentLeftNew = INT( double(nExtentTotal) * lfPercentLeft ),
		nExtentRightNew = INT( double(nExtentTotal) * (1.0 - lfPercentLeft) );
						if( nExtentLeftNew < nExtentLeftMin )
	if( nExtentLeftNew > nExtentLeftMax )
		nExtentLeftNew = nExtentLeftMax;
	if( nExtentRightNew < nExtentRightMin )
		nExtentRightNew = nExtentRightMin;
	if( nExtentRightNew > nExtentRightMax )
		nExtentRightNew = nExtentRightMax;
	if(		nExtentLeftNew != nExtentLeftCurrent
		&&	nExtentRightNew != nExtentRightCurrent
		)
	{ // if extents are inequal
		GridCellGetOuterAtTop(0,0)->ExtentSet( nExtentLeftNew );
		GridCellGetOuterAtTop(1,0)->ExtentSet( nExtentRightNew );
		_DoColumnLayout();
		if( m_bSynchronizeColumnWidth )
			_SyncColumnWidths();
		if( IsWindowVisible() )
			Invalidate();
	} // if extents are inequal
}

CExtPropertyGridCtrl * CExtPropertyGridWnd::PropertyGridCtrlSet(
	CExtPropertyGridCtrl * pPGC
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pPropertyGridCtrl != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPropertyGridCtrl );
	}
	if( pPGC != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
	}
#endif // _DEBUG
CExtPropertyGridCtrl * pOldPS = m_pPropertyGridCtrl;
	m_pPropertyGridCtrl = pPGC;
	return pOldPS;
}

CExtPropertyGridCtrl * CExtPropertyGridWnd::PropertyGridCtrlGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pPropertyGridCtrl != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPropertyGridCtrl );
	}
#endif // _DEBUG
	return m_pPropertyGridCtrl;
}

const CExtPropertyGridCtrl * CExtPropertyGridWnd::PropertyGridCtrlGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyGridWnd * > ( this ) )
			-> PropertyGridCtrlGet();
}

CExtPropertyGridCtrl & CExtPropertyGridWnd::PropertyGridCtrlGetRef()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet(); 
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC ); // should be initialized
	return (*pPGC);
}

const CExtPropertyGridCtrl & CExtPropertyGridWnd::PropertyGridCtrlGetRef() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet(); 
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC ); // should be initialized
	return (*pPGC);
}

CExtPropertyStore * CExtPropertyGridWnd::PropertyStoreGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
	if( pPGC == NULL )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
CExtPropertyStore * pPS = pPGC->PropertyStoreGet();
	if( pPS == NULL )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPS );
	return pPS;
}

const CExtPropertyStore * CExtPropertyGridWnd::PropertyStoreGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
	if( pPGC == NULL )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
const CExtPropertyStore * pPS = pPGC->PropertyStoreGet();
	if( pPS == NULL )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPS );
	return pPS;
}

CExtPropertyStore & CExtPropertyGridWnd::PropertyStoreGetRef()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyStore * pPS = PropertyStoreGet();
	__EXT_DEBUG_GRID_ASSERT_VALID( pPS ); // should be initialized
	return (*pPS);
}

const CExtPropertyStore & CExtPropertyGridWnd::PropertyStoreGetRef() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtPropertyStore * pPS = PropertyStoreGet();
	__EXT_DEBUG_GRID_ASSERT_VALID( pPS ); // should be initialized
	return (*pPS);
}

void CExtPropertyGridWnd::PropertyStoreSynchronizeAll()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
HWND hWnd = GetSafeHwnd();
	if( hWnd != NULL )
	{
		::SendMessage( hWnd, WM_CANCELMODE, 0, 0 );
		hWnd = ::GetWindow( hWnd, GW_CHILD );
		for( ; hWnd != NULL; hWnd = ::GetWindow( hWnd, GW_HWNDNEXT ) )
			::SendMessage( hWnd, WM_CANCELMODE, 0, 0 );
	} // if( hWnd != NULL )
	if( CExtGridCell::g_pCellMenuTracking != NULL )
	{
		CExtGridDataProvider * pDP = CExtGridCell::g_pCellMenuTracking->DataProviderGet();
		if( pDP != NULL )
		{
			CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
			if( pPGC != NULL )
			{
				CExtPropertyGridWnd * pActiveGrid = pPGC->GetActiveGrid();
				if( LPVOID(pActiveGrid) == LPVOID(this) )
				{
					CExtGridDataProvider & _ActiveDP = pActiveGrid->OnGridQueryDataProvider();
					if( LPVOID(&_ActiveDP) == LPVOID(pDP->m_pOuterDataProvider) )
						CExtPopupMenuWnd::CancelMenuTracking();
				}
			}
		}
	}
	PropertyItemRemove( (HTREEITEM)NULL );
CExtPropertyStore * pPS = PropertyStoreGet();
	if(		pPS != NULL
		&&	pPS->CanBeInsertedIntoPropertyGrid()
		)
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pPS );
		PropertyStoreSynchronizeOneLevel( pPS );
//		HTREEITEM htiRoot = ItemGetRoot();
//		__EXT_DEBUG_GRID_ASSERT( htiRoot != NULL );
//		if( ItemGetChildCount(htiRoot) > 0 )
//		{
//			HTREEITEM hTreeItem = ItemGetChildAt( htiRoot, 0 );
//			__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
//			ItemFocusSet( hTreeItem, false );
//		} // if( ItemGetChildCount(htiRoot) > 0 )
	} // if( pPS != NULL ...
	OnSwUpdateScrollBars();
	OnSwDoRedraw();
}

void CExtPropertyGridWnd::PropertyStoreSynchronizeOneLevel(
	CExtPropertyItem * pPropertyItem,
	CExtPropertyItem * pParentItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	if( ! pPropertyItem->CanBeInsertedIntoPropertyGrid() )
		return;
bool bCompoundSynchronizing = false;
	if(		pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValueCompound))
		||	pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValueMixedCompound))
		)
		bCompoundSynchronizing = true;
HTREEITEM hTreeItem = NULL;
	if( ! pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyStore)) )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pParentItem );
		HTREEITEM htiParent = NULL;
		__EXT_DEBUG_GRID_ASSERT_VALID( pParentItem );
		if( ! pParentItem->IsKindOf(RUNTIME_CLASS(CExtPropertyStore)) )
		{
			htiParent =
				PropertyItemToTreeItem( pParentItem );
			__EXT_DEBUG_GRID_ASSERT( htiParent != NULL );
		} // if( ! pParentItem->IsKindOf(RUNTIME_CLASS(CExtPropertyStore)) )
		else
		{
			htiParent = ItemGetRoot();
			__EXT_DEBUG_GRID_ASSERT( htiParent != NULL );
		} // else from if( ! pParentItem->IsKindOf(RUNTIME_CLASS(CExtPropertyStore)) )
		bool bSorted = false;
		if( m_bSortedCategories && m_bSortedValues )
		{
			bSorted = true;
		} // if( m_bSortedCategories && m_bSortedValues )
		else if( (!m_bSortedCategories) && (!m_bSortedValues) )
		{
			bSorted = false;
		} // else if( (!m_bSortedCategories) && (!m_bSortedValues) )
		else
		{
			if( pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValue)) )
				bSorted = m_bSortedValues;
			else if( pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyCategory)) )
				bSorted = m_bSortedCategories;
		} // else from else if( (!m_bSortedCategories!) && (!m_bSortedValues) )
		if( pParentItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValueCompound)) )
			bSorted = false;
		if( bSorted )
		{
			hTreeItem =
				PropertyItemInsertAlphabetic(
					pPropertyItem,
					htiParent,
					false
					);
			__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
//			if( hTreeItem == NULL )
//				return;
		} // if( bSorted )
		else
		{
			hTreeItem =
				PropertyItemInsert(
					pPropertyItem,
					-1,
					htiParent,
					false
					);
			__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
//			if( hTreeItem == NULL )
//				return;
		} // else from if( bSorted )
		if( bCompoundSynchronizing )
		{
			if( hTreeItem == NULL )
				return;
//			CExtTreeGridCellNode * pNode =
//				CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
//			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
//			pNode->TreeNodeIndentPxSet( 0 );
		}
		if(		pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValue))
			&&	(! bCompoundSynchronizing )
			)
			return;
	} // if( ! pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyStore)) )
CExtPropertyValueMixedCompound * pMixedCompound =
		DYNAMIC_DOWNCAST(
			CExtPropertyValueMixedCompound,
			pPropertyItem
			);
	if( pMixedCompound != NULL )
	{
		INT nIndex, nCount = pMixedCompound->CompoundPartGetCount();
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtPropertyItem * pChildItem =
				pMixedCompound->CompoundPartGetAt( nIndex );
			if( ! pChildItem->CanBeInsertedIntoPropertyGrid() )
				continue;
			PropertyStoreSynchronizeOneLevel( pChildItem, pMixedCompound );
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	} // if( pMixedCompound != NULL )
	else
	{
		INT nIndex, nCount = pPropertyItem->ItemGetCount();
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtPropertyItem * pChildItem =
				pPropertyItem->ItemGetAt( nIndex );
			if( ! pChildItem->CanBeInsertedIntoPropertyGrid() )
				continue;
			PropertyStoreSynchronizeOneLevel( pChildItem, pPropertyItem );
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	} // else from if( pMixedCompound != NULL )
}

CRuntimeClass * CExtPropertyGridWnd::OnPgwCellAreaGetRTC( CExtPropertyItem * pPropertyItem )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
	if( pPGC == NULL )
		return RUNTIME_CLASS(CExtPropertyGridCellArea);
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
	return pPGC->OnPgcCellAreaGetRTC( *this, pPropertyItem );
}

void CExtPropertyGridWnd::OnPgwCellAreaConfigure( CExtPropertyItem * pPropertyItem, CExtPropertyGridCellArea * pCellCaption )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellCaption );
CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
	if( pPGC == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
	if( m_bHelperCaseInsensitivePropertyCaptionComparison )
		pCellCaption->m_bHelperCaseInsensitivePropertyCaptionComparison = true;
	pPGC->OnPgcCellAreaConfigure( *this, pPropertyItem, pCellCaption );
}

HTREEITEM CExtPropertyGridWnd::PropertyItemInsert(
	CExtPropertyItem * pPropertyItem,
	LONG nIndex, // = -1
	HTREEITEM htiParent, // = TVI_ROOT
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	__EXT_DEBUG_GRID_ASSERT( PropertyItemToTreeItem(pPropertyItem) == NULL );
	if( ! pPropertyItem->CanBeInsertedIntoPropertyGrid() )
		return NULL;
	if(		htiParent == NULL
		||	htiParent == TVI_ROOT
		)
	{
		htiParent = ItemGetRoot();
		__EXT_DEBUG_GRID_ASSERT( htiParent != NULL );
	}
HTREEITEM hTreeItem =
		ItemInsert(
			htiParent,
			ULONG(nIndex),
			1,
			false
			);
	if( hTreeItem == NULL )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return NULL;
	}
INT nHeighPx = pPropertyItem->QueryHeightPx( this );
	if( nHeighPx < 0 )
		nHeighPx = DefaultRowHeightGet();
CExtTreeGridCellNode * pNode =
		CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	__EXT_DEBUG_GRID_VERIFY( pNode->ExtentSet( nHeighPx ) );

	if(		pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValue))
//		&&	(! pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValueCompound)) )
		)
		CExtTreeGridCellNode::FromHTREEITEM( hTreeItem )
			-> TreeNodeIndentPxSet( 0 );
	m_mapItems.SetAt(
		hTreeItem,
		pPropertyItem
		);
	m_mapProperties.SetAt(
		pPropertyItem,
		hTreeItem
		);
CRuntimeClass * pCellAreaRTC = OnPgwCellAreaGetRTC( pPropertyItem );
	ASSERT( pCellAreaRTC != NULL );
	ASSERT( pCellAreaRTC->IsDerivedFrom( RUNTIME_CLASS(CExtPropertyGridCellArea) ) );
CExtPropertyGridCellArea * pCellCaption = STATIC_DOWNCAST( CExtPropertyGridCellArea, ItemGetCell( hTreeItem, 0L, 0, pCellAreaRTC ) );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellCaption );
	pCellCaption->TextSet( pPropertyItem->NameGet() );
	pCellCaption->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
	OnPgwCellAreaConfigure( pPropertyItem, pCellCaption );
CExtGridCell * pCellValueFrom = pPropertyItem->ValueActiveGet();
	if( pCellValueFrom != NULL )
	{
		CRuntimeClass * pRTC = pCellValueFrom->GetRuntimeClass();
		CExtGridCell * pCellValueIn = ItemGetCell( hTreeItem, 1L, 0, pRTC );
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellValueIn );
		pCellValueIn->Assign( *pCellValueFrom );
		pCellValueIn->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pPropertyItem->OnGridRowInitialized(
			*this,
			hTreeItem,
			pCellCaption,
			pCellValueIn
			);
	} // if( pCellValueFrom != NULL )
	else
	{
		CExtPropertyGridCellArea * pCellValueIn = STATIC_DOWNCAST( CExtPropertyGridCellArea, ItemGetCell( hTreeItem, 1L, 0, pCellAreaRTC ) );
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellValueIn );
		pCellValueIn->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pPropertyItem->OnGridRowInitialized(
			*this,
			hTreeItem,
			pCellCaption,
			pCellValueIn
			);
	} // else from if( pCellValueFrom != NULL )
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	}
	return hTreeItem;
}

HTREEITEM CExtPropertyGridWnd::PropertyItemInsertAlphabetic(
	CExtPropertyItem * pPropertyItem,
	HTREEITEM htiParent, // = TVI_ROOT
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	__EXT_DEBUG_GRID_ASSERT( PropertyItemToTreeItem(pPropertyItem) == NULL );
	if(		htiParent == NULL
		||	htiParent == TVI_ROOT
		)
	{
		htiParent = ItemGetRoot();
		__EXT_DEBUG_GRID_ASSERT( htiParent != NULL );
	}
bool bHelperCaseInsensitivePropertyCaptionComparison = m_bHelperCaseInsensitivePropertyCaptionComparison;
	if( ! bHelperCaseInsensitivePropertyCaptionComparison )
	{
		CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
		if( pPGC != NULL )
			bHelperCaseInsensitivePropertyCaptionComparison = pPGC->m_bHelperCaseInsensitivePropertyCaptionComparison;
	}
CExtSafeString strNameCmp = pPropertyItem->NameGet();
LONG nIndex, nCount = ItemGetChildCount( htiParent );
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		HTREEITEM hTreeItem =
			ItemGetChildAt( htiParent, nIndex );
		__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
		CExtGridCell * pCell =
			ItemGetCell( hTreeItem, 0 );
		__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
		CExtSafeString strName;
		pCell->TextGet( strName );
		int nTextCmdResult = 
			( bHelperCaseInsensitivePropertyCaptionComparison )
				? strNameCmp.CompareNoCase( LPCTSTR(strName) )
				: strNameCmp.Compare      ( LPCTSTR(strName) )
				;
		if( nTextCmdResult < 0 )
			break;
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return
		PropertyItemInsert(
			pPropertyItem,
			nIndex,
			htiParent,
			bRedraw
			);
}

HTREEITEM CExtPropertyGridWnd::PropertyItemToTreeItem(
	const CExtPropertyItem * pPropertyItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( pPropertyItem != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	}
#endif // _DEBUG
	return _PropertyItemToTreeItem_Impl( pPropertyItem );
}

HTREEITEM CExtPropertyGridWnd::_PropertyItemToTreeItem_Impl(
	const CExtPropertyItem * pPropertyItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( pPropertyItem == NULL )
		return NULL;
	//__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
HTREEITEM hTreeItem = NULL;
	if(	m_mapProperties.Lookup(
			(CExtPropertyItem*)pPropertyItem,
			hTreeItem
			)
		)
	{
		__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
		return hTreeItem;
	}
	return NULL;
}

CExtPropertyItem * CExtPropertyGridWnd::PropertyItemFromTreeItem(
	HTREEITEM hTreeItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyItem * pPropertyItem = _PropertyItemFromTreeItem_Impl( hTreeItem );
#ifdef _DEBUG
	if( pPropertyItem != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	}
#endif // _DEBUG
	return pPropertyItem;
}

CExtPropertyItem * CExtPropertyGridWnd::_PropertyItemFromTreeItem_Impl(
	HTREEITEM hTreeItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
HTREEITEM hRootItem = ItemGetRoot();
	__EXT_DEBUG_GRID_ASSERT( hRootItem != NULL );
	if( hTreeItem == hRootItem )
		return NULL;
CExtPropertyItem * pPropertyItem = NULL;
	for( ; true; )
	{
		if(	m_mapItems.Lookup(
				hTreeItem,
				pPropertyItem
				)
			)
		//{
		//	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
			return pPropertyItem;
		//}
		hTreeItem = ItemGetParent( hTreeItem );
		if(		hTreeItem == NULL
			||	hTreeItem == hRootItem
			)
			return NULL;
	} // for( ; true; )
	return NULL;
}

const CExtPropertyItem * CExtPropertyGridWnd::PropertyItemFromTreeItem(
	HTREEITEM hTreeItem
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyGridWnd * > ( this ) )
			-> PropertyItemFromTreeItem( hTreeItem );
}

void CExtPropertyGridWnd::PropertyItemRemove(
	CExtPropertyItem * pPropertyItem, // = NULL // NULL - remove all
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( pPropertyItem == NULL )
	{
		PropertyItemRemove( (HTREEITEM)NULL, bRedraw );
		return;
	}
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
HTREEITEM hTreeItem = PropertyItemToTreeItem( pPropertyItem );
	if( hTreeItem == NULL )
		return;
	ItemRemove( hTreeItem, false, bRedraw );
	m_mapItems.RemoveKey( hTreeItem );
	m_mapProperties.RemoveKey( pPropertyItem );
}

void CExtPropertyGridWnd::PropertyItemRemove(
	HTREEITEM hTreeItem, // = TVI_ROOT // TVI_ROOT - remove all
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		hTreeItem == NULL
		||	hTreeItem == ItemGetRoot()
		)
	{
		ItemFocusLock( true );
		ItemRemove( ItemGetRoot(), true, bRedraw );
		m_mapItems.RemoveAll();
		m_mapProperties.RemoveAll();
		ItemFocusLock( false );
		return;
	}
CExtPropertyItem * pPropertyItem = PropertyItemFromTreeItem( hTreeItem );
	if( pPropertyItem == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	ItemRemove( hTreeItem, false, bRedraw );
	m_mapItems.RemoveKey( hTreeItem );
	m_mapProperties.RemoveKey( pPropertyItem );
}

CExtGridHitTestInfo & CExtPropertyGridWnd::HitTest(
	CExtGridHitTestInfo & htInfo,
	bool bReAlignCellResizing,
	bool bSupressZeroTopCellResizing,
	bool bComputeOuterDropAfterState // = false
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridHitTestInfo & htInfoRetVal =
		CExtTreeGridWnd::HitTest(
				htInfo,
				bReAlignCellResizing,
				bSupressZeroTopCellResizing,
				bComputeOuterDropAfterState
				);
	if(		(! htInfoRetVal.IsHoverEmpty() )
		&&	htInfoRetVal.GetInnerOuterTypeOfColumn() == 0
		&&	htInfoRetVal.GetInnerOuterTypeOfRow() == 0
		&&	htInfoRetVal.m_nColNo == 1
		)
		htInfoRetVal.m_dwAreaFlags &= ~(__EGBWA_NEAR_CELL_BORDER_RIGHT);
	return htInfoRetVal;
}

void CExtPropertyGridWnd::OnPgwSynchronizeCompoundValue(
	CExtPropertyValueCompound * pCompoundValue
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCompoundValue );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
	OnPgwSynchronizeCompoundValueChildren(
		pCompoundValue
		);
	OnPgwSynchronizeCompoundValueParentPath(
		pCompoundValue
		);
}

void CExtPropertyGridWnd::OnPgwSynchronizeCompoundValueChildren(
	CExtPropertyValueCompound * pCompoundValue
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCompoundValue );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
bool bGridIsVisible = ( (GetStyle()&WS_VISIBLE) != 0 ) ? true : false;
INT nIndex, nCount = pCompoundValue->ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtPropertyItem * pChildItem =
			pCompoundValue->ItemGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pChildItem );
		CExtPropertyValue * pValue =
			DYNAMIC_DOWNCAST(
				CExtPropertyValue,
				pChildItem
				);
		if( pValue == NULL )
			continue;
		CExtPropertyValueCompound * pChildCompoundValue =
			DYNAMIC_DOWNCAST(
				CExtPropertyValueCompound,
				pValue
				);
		if( pChildCompoundValue != NULL )
		{
			OnPgwSynchronizeCompoundValueChildren(
				pChildCompoundValue
				);
			continue;
		} // if( pCompoundValue != NULL )
		CExtGridCell * pCellSrc =
			pValue->ValueActiveGet();
		if( pCellSrc == NULL )
			continue;
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
		HTREEITEM hTreeItem =
			PropertyItemToTreeItem( pValue );
		if( hTreeItem == NULL )
			continue;
		CExtGridCell * pCellDst =
			ItemGetCell( hTreeItem, 1 );
		if( pCellDst == NULL )
			continue;
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
		pCellDst->Assign( *pCellSrc );
		pCellDst->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		if( ! bGridIsVisible )
			continue;
		LONG nRowNo = ItemGetVisibleIndexOf( hTreeItem );
		if( nRowNo < 0 )
			continue;
		CRect rcCellExtra;
		if( ! GridCellRectsGet(
				1,
				nRowNo,
				0,
				0,
				NULL,
				&rcCellExtra
				)
			)
			continue;
		InvalidateRect( &rcCellExtra );
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
CExtGridCell * pCellSrc =
		pCompoundValue->ValueActiveGet();
	if( pCellSrc == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
CExtSafeString strCompound =
		pCompoundValue->BuildCompoundTextActive();
	pCellSrc->TextSet( strCompound );
HTREEITEM hTreeItem =
		PropertyItemToTreeItem( pCompoundValue );
	if( hTreeItem == NULL )
		return;
CExtGridCell * pCellDst =
		ItemGetCell( hTreeItem, 1 );
	if( pCellDst == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
	pCellDst->Assign( *pCellSrc );
	pCellDst->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
	if( ! bGridIsVisible )
		return;
LONG nRowNo = ItemGetVisibleIndexOf( hTreeItem );
	if( nRowNo < 0 )
		return;
CRect rcCellExtra;
	if( ! GridCellRectsGet(
			1,
			nRowNo,
			0,
			0,
			NULL,
			&rcCellExtra
			)
		)
		return;
	InvalidateRect( &rcCellExtra );
}

void CExtPropertyGridWnd::OnPgwSynchronizeCompoundValueParentPath(
	CExtPropertyValueCompound * pCompoundValue
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCompoundValue );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
bool bGridIsVisible = ( (GetStyle()&WS_VISIBLE) != 0 ) ? true : false;
	for( ; true; )
	{
		CExtPropertyItem * pParent = pCompoundValue->ItemParentGet();
		if( pParent == NULL )
			break;
		pCompoundValue =
			DYNAMIC_DOWNCAST(
				CExtPropertyValueCompound,
				pParent
				);
		if( pCompoundValue == NULL )
			break;
		CExtGridCell * pCellSrc =
			pCompoundValue->ValueActiveGet();
		if( pCellSrc == NULL )
			continue;
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
		HTREEITEM hTreeItem =
			PropertyItemToTreeItem( pCompoundValue );
		if( hTreeItem == NULL )
			continue;
		CExtGridCell * pCellDst =
			ItemGetCell( hTreeItem, 1 );
		if( pCellDst == NULL )
			continue;
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
		pCellDst->Assign( *pCellSrc );
		pCellDst->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		if( ! bGridIsVisible )
			continue;
		LONG nRowNo = ItemGetVisibleIndexOf( hTreeItem );
		if( nRowNo < 0 )
			continue;
		CRect rcCellExtra;
		if( ! GridCellRectsGet(
				1,
				nRowNo,
				0,
				0,
				NULL,
				&rcCellExtra
				)
			)
			continue;
		InvalidateRect( &rcCellExtra );
	} // for( ; true; )
}

void CExtPropertyGridWnd::OnPgwSynchronizeCompoundValue(
	CExtPropertyValueMixedCompound * pMixedCompound
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pMixedCompound );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
	OnPgwSynchronizeCompoundValueChildren(
		pMixedCompound
		);
	OnPgwSynchronizeCompoundValueParentPath(
		pMixedCompound
		);
}

void CExtPropertyGridWnd::OnPgwSynchronizeCompoundValueChildren(
	CExtPropertyValueMixedCompound * pMixedCompound
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pMixedCompound );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
bool bGridIsVisible = ( (GetStyle()&WS_VISIBLE) != 0 ) ? true : false;
INT nIndex, nCount = pMixedCompound->CompoundPartGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtPropertyItem * pChildItem =
			pMixedCompound->CompoundPartGetAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pChildItem );
		CExtPropertyValue * pValue =
			DYNAMIC_DOWNCAST(
				CExtPropertyValue,
				pChildItem
				);
		if( pValue == NULL )
			continue;
		CExtPropertyValueMixedCompound * pChildCompoundValue =
			DYNAMIC_DOWNCAST(
				CExtPropertyValueMixedCompound,
				pValue
				);
		if( pChildCompoundValue != NULL )
		{
			OnPgwSynchronizeCompoundValueChildren(
				pChildCompoundValue
				);
			continue;
		} // if( pMixedCompound != NULL )
		CExtGridCell * pCellSrc =
			pValue->ValueActiveGet();
		if( pCellSrc == NULL )
			continue;
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
		HTREEITEM hTreeItem =
			PropertyItemToTreeItem( pValue );
		if( hTreeItem == NULL )
			continue;
		CExtGridCell * pCellDst =
			ItemGetCell( hTreeItem, 1 );
		if( pCellDst == NULL )
			continue;
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
		pCellDst->Assign( *pCellSrc );
		pCellDst->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		if( ! bGridIsVisible )
			continue;
		LONG nRowNo = ItemGetVisibleIndexOf( hTreeItem );
		if( nRowNo < 0 )
			continue;
		CRect rcCellExtra;
		if( ! GridCellRectsGet(
				1,
				nRowNo,
				0,
				0,
				NULL,
				&rcCellExtra
				)
			)
			continue;
		InvalidateRect( &rcCellExtra );
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
CExtGridCell * pCellSrc =
		pMixedCompound->ValueActiveGet();
	if( pCellSrc == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
CExtSafeString strCompound =
		pMixedCompound->BuildCompoundTextActive();
	pCellSrc->TextSet( strCompound );
HTREEITEM hTreeItem =
		PropertyItemToTreeItem( pMixedCompound );
	if( hTreeItem == NULL )
		return;
CExtGridCell * pCellDst =
		ItemGetCell( hTreeItem, 1 );
	if( pCellDst == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
	pCellDst->Assign( *pCellSrc );
	pCellDst->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
	if( pMixedCompound->ChildrenHaveDifferentActiveValues() )
		pCellDst->ModifyStyleEx( __EGCS_EX_UNDEFINED_ROLE );
	else
		pCellDst->ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
	if( ! bGridIsVisible )
		return;
LONG nRowNo = ItemGetVisibleIndexOf( hTreeItem );
	if( nRowNo < 0 )
		return;
CRect rcCellExtra;
	if( ! GridCellRectsGet(
			1,
			nRowNo,
			0,
			0,
			NULL,
			&rcCellExtra
			)
		)
		return;
	InvalidateRect( &rcCellExtra );
}

void CExtPropertyGridWnd::OnPgwSynchronizeCompoundValueParentPath(
	CExtPropertyValueMixedCompound * pMixedCompound
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pMixedCompound );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
bool bGridIsVisible = ( (GetStyle()&WS_VISIBLE) != 0 ) ? true : false;
	for( ; true; )
	{
		CExtPropertyItem * pParent = pMixedCompound->ItemParentGet();
		if( pParent == NULL )
			break;
		pMixedCompound =
			DYNAMIC_DOWNCAST(
				CExtPropertyValueMixedCompound,
				pParent
				);
		if( pMixedCompound == NULL )
			break;
		CExtGridCell * pCellSrc =
			pMixedCompound->ValueActiveGet();
		if( pCellSrc == NULL )
			continue;
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
		HTREEITEM hTreeItem =
			PropertyItemToTreeItem( pMixedCompound );
		if( hTreeItem == NULL )
			continue;
		CExtGridCell * pCellDst =
			ItemGetCell( hTreeItem, 1 );
		if( pCellDst == NULL )
			continue;
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
		pCellDst->Assign( *pCellSrc );
		pCellDst->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		if( pMixedCompound->ChildrenHaveDifferentActiveValues() )
			pCellDst->ModifyStyleEx( __EGCS_EX_UNDEFINED_ROLE );
		else
			pCellDst->ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
		if( ! bGridIsVisible )
			continue;
		LONG nRowNo = ItemGetVisibleIndexOf( hTreeItem );
		if( nRowNo < 0 )
			continue;
		CRect rcCellExtra;
		if( ! GridCellRectsGet(
				1,
				nRowNo,
				0,
				0,
				NULL,
				&rcCellExtra
				)
			)
			continue;
		InvalidateRect( &rcCellExtra );
	} // for( ; true; )
}

void CExtPropertyGridWnd::OnPgwContextMenu(
	CWnd * pWndHit,
	CExtGridHitTestInfo & htInfo,
	CExtPropertyItem * pPropertyItem // can be NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
	if( pPGC == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
CPoint ptScreen = htInfo.m_ptClient;
	ClientToScreen( &ptScreen );
	pPGC->OnPgcContextMenuTrack(
		pWndHit,
		ptScreen,
		&htInfo,
		pPropertyItem,
		this
		);
}

void CExtPropertyGridWnd::ItemExpand(
	HTREEITEM hTreeItem,
	INT nActionTVE, // = TVE_TOGGLE, // TVE_COLLAPSE, TVE_EXPAND, TVE_TOGGLE only
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtTreeGridWnd::ItemExpand(
		hTreeItem,
		nActionTVE,
		bRedraw
		);
CExtPropertyItem * pPropertyItem =
		PropertyItemFromTreeItem( hTreeItem );
	if( pPropertyItem == NULL )
		return;
bool bExpandedTreeItem = ItemIsExpanded( hTreeItem );
bool bExpandedPropertyItem = pPropertyItem->ExpandedGet();
	if( bExpandedTreeItem == bExpandedPropertyItem )
		return;
	pPropertyItem->ExpandedSet( bExpandedTreeItem );
CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
	if( pPGC == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
	pPGC->OnPgcQueryGrids( arrGrids );
INT nGridIdx;
	for( nGridIdx = 0; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
	{
		CExtPropertyGridWnd * pPGW = arrGrids[ nGridIdx ];
		__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
		if( pPGW == this )
			continue;
		HTREEITEM htiOther = pPGW->PropertyItemToTreeItem( pPropertyItem );
		if( htiOther == NULL )
			continue;
		bool bExpandedTreeItemOther = pPGW->ItemIsExpanded( htiOther );
		if( bExpandedTreeItemOther == bExpandedTreeItem )
			continue;
		pPGW->ItemExpand( htiOther, TVE_TOGGLE );
	} // for( nGridIdx = 0; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
}

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyGridWndSorted window

IMPLEMENT_DYNCREATE( CExtPropertyGridWndSorted, CExtPropertyGridWnd );

CExtPropertyGridWndSorted::CExtPropertyGridWndSorted(
	CExtPropertyGridCtrl * pPGC // = NULL
	)
	: CExtPropertyGridWnd( pPGC )
{
	m_bSortedValues = true;
	m_nConstantIndent = __EXT_MGC_PROPERTY_GRID_DEFAULT_LEVEL_INDENT;
}

CExtPropertyGridWndSorted::~CExtPropertyGridWndSorted()
{
}

#ifdef _DEBUG

void CExtPropertyGridWndSorted::AssertValid() const
{
	CExtPropertyGridWnd::AssertValid();
}

void CExtPropertyGridWndSorted::Dump( CDumpContext & dc ) const
{
	CExtPropertyGridWnd::Dump( dc );
}

#endif

BEGIN_MESSAGE_MAP( CExtPropertyGridWndSorted, CExtPropertyGridWnd )
    //{{AFX_MSG_MAP(CExtPropertyGridWndSorted)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CExtPropertyGridWndSorted::PropertyStoreSynchronizeOneLevel(
	CExtPropertyItem * pPropertyItem,
	CExtPropertyItem * pParentItem // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	pParentItem;
	if( pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValue)) )
	{
		HTREEITEM htiParent =
			ItemGetRoot();
		__EXT_DEBUG_GRID_ASSERT( htiParent != NULL );
		HTREEITEM hTreeItem = NULL;
		bool bSortedValues = m_bSortedValues;
		if( pParentItem != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pParentItem );
			if(		pParentItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValueCompound) )
				||	pParentItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValueMixedCompound) )
				)
			{
				bSortedValues = false;
				htiParent = PropertyItemToTreeItem( pParentItem );
				__EXT_DEBUG_GRID_ASSERT( htiParent != NULL );
			}
		}
		if( ! bSortedValues )
		{
			hTreeItem = 
				PropertyItemInsert(
					pPropertyItem,
					-1,
					htiParent,
					false
					);
		}
		else
		{
			hTreeItem = 
				PropertyItemInsertAlphabetic(
					pPropertyItem,
					htiParent,
					false
					);
		}
// commented is the bug fix by Maurizio Pesce
// properties are enabled to be hidden in grids
//		__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
//		if( hTreeItem == NULL )
//			return;
		if(		pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValueCompound))
			||	pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValueMixedCompound))
			)
		{
			if( hTreeItem == NULL )
				return;
//			CExtTreeGridCellNode * pNode =
//				CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
//			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
//			pNode->TreeNodeIndentPxSet( 0 );

			
			
//			INT nIndex, nCount = pPropertyItem->ItemGetCount();
//			for( nIndex = 0; nIndex < nCount; nIndex ++ )
//			{
//				CExtPropertyItem * pChildItem =
//					pPropertyItem->ItemGetAt( nIndex );
//				CExtPropertyGridWnd::PropertyStoreSynchronizeOneLevel( pChildItem, pPropertyItem );
//			} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
			CExtPropertyValueMixedCompound * pMixedCompound =
				DYNAMIC_DOWNCAST(
					CExtPropertyValueMixedCompound,
					pPropertyItem
					);
			if( pMixedCompound != NULL )
			{
				INT nIndex, nCount = pMixedCompound->CompoundPartGetCount();
				for( nIndex = 0; nIndex < nCount; nIndex ++ )
				{
					CExtPropertyItem * pChildItem =
						pMixedCompound->CompoundPartGetAt( nIndex );
					if( ! pChildItem->CanBeInsertedIntoPropertyGrid() )
						continue;
					PropertyStoreSynchronizeOneLevel( pChildItem, pMixedCompound );
				} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
			} // if( pMixedCompound != NULL )
			else
			{
				INT nIndex, nCount = pPropertyItem->ItemGetCount();
				for( nIndex = 0; nIndex < nCount; nIndex ++ )
				{
					CExtPropertyItem * pChildItem =
						pPropertyItem->ItemGetAt( nIndex );
					if( ! pChildItem->CanBeInsertedIntoPropertyGrid() )
						continue;
					PropertyStoreSynchronizeOneLevel( pChildItem, pPropertyItem );
				} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
			} // else from if( pMixedCompound != NULL )


//			CExtSafeString strCompound =
//				pPropertyItem->BuildCompoundTextActive();
//			if( ! strCompound.IsEmpty() )
//			{
//				CExtGridCell * pCell = ItemGetCell( hTreeItem, 1 );
//				__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
//				pCell->TextSet( strCompound );
//			} // if( ! strCompound.IsEmpty() )
			ItemExpand(
				hTreeItem,
				pPropertyItem->ExpandedGet()
					? TVE_EXPAND
					: TVE_COLLAPSE
				);
		}
		return;
	} // if( pPropertyItem->IsKindOf(RUNTIME_CLASS(CExtPropertyValue)) )
INT nIndex, nCount = pPropertyItem->ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtPropertyItem * pChildItem =
			pPropertyItem->ItemGetAt( nIndex );
		PropertyStoreSynchronizeOneLevel( pChildItem, pPropertyItem );
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
}


/////////////////////////////////////////////////////////////////////////////
// CExtPropertyGridWndCategorized window

IMPLEMENT_DYNCREATE( CExtPropertyGridWndCategorized, CExtPropertyGridWnd );

CExtPropertyGridWndCategorized::CExtPropertyGridWndCategorized(
	CExtPropertyGridCtrl * pPGC // = NULL
	)
	: CExtPropertyGridWnd( pPGC )
{
}

CExtPropertyGridWndCategorized::~CExtPropertyGridWndCategorized()
{
}

#ifdef _DEBUG

void CExtPropertyGridWndCategorized::AssertValid() const
{
	CExtPropertyGridWnd::AssertValid();
}

void CExtPropertyGridWndCategorized::Dump( CDumpContext & dc ) const
{
	CExtPropertyGridWnd::Dump( dc );
}

#endif

BEGIN_MESSAGE_MAP( CExtPropertyGridWndCategorized, CExtPropertyGridWnd )
    //{{AFX_MSG_MAP(CExtPropertyGridWndCategorized)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyGridComboBoxBar window

IMPLEMENT_DYNCREATE( CExtPropertyGridComboBoxBar, CExtComboBox );

CExtPropertyGridComboBoxBar::CExtPropertyGridComboBoxBar(
	CExtPropertyGridCtrl * pPGC // = NULL
	)
	: m_pPropertyGridCtrl( pPGC )
	, m_bAutoDeleteWindow( false )
{
#ifdef _DEBUG
	if( m_pPropertyGridCtrl != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPropertyGridCtrl );
	}
#endif // _DEBUG
}

CExtPropertyGridComboBoxBar::~CExtPropertyGridComboBoxBar()
{
	PropertyStoreRemove();
}

#ifdef _DEBUG

void CExtPropertyGridComboBoxBar::AssertValid() const
{
	CExtComboBox::AssertValid();
	if( m_pPropertyGridCtrl != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPropertyGridCtrl );
	}
// 	if( GetSafeHwnd() != NULL )
// 	{
// 		__EXT_DEBUG_GRID_ASSERT( GetCount() == m_arrPropertyStores.GetSize() );
// 	}
}

void CExtPropertyGridComboBoxBar::Dump( CDumpContext & dc ) const
{
	CExtComboBox::Dump( dc );
}

#endif

BEGIN_MESSAGE_MAP( CExtPropertyGridComboBoxBar, CExtComboBox )
    //{{AFX_MSG_MAP(CExtPropertyGridComboBoxBar)
	//}}AFX_MSG_MAP
	ON_CONTROL_REFLECT( CBN_SELENDOK, OnSelEndOK )
	ON_MESSAGE( WM_SIZEPARENT, OnSizeParent )
END_MESSAGE_MAP()


void CExtPropertyGridComboBoxBar::PropertyGridCtrlSet(
	CExtPropertyGridCtrl * pPGC
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_pPropertyGridCtrl = pPGC;
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

CExtPropertyGridCtrl * CExtPropertyGridComboBoxBar::PropertyGridCtrlGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pPropertyGridCtrl != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPropertyGridCtrl );
	}
#endif // _DEBUG
	return m_pPropertyGridCtrl;
}

const CExtPropertyGridCtrl * CExtPropertyGridComboBoxBar::PropertyGridCtrlGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyGridComboBoxBar * > ( this ) )
			-> PropertyGridCtrlGet();
}

CExtPropertyGridCtrl & CExtPropertyGridComboBoxBar::PropertyGridCtrlGetRef()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet(); 
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC ); // should be initialized
	return (*pPGC);
}

const CExtPropertyGridCtrl & CExtPropertyGridComboBoxBar::PropertyGridCtrlGetRef() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet(); 
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC ); // should be initialized
	return (*pPGC);
}

INT CExtPropertyGridComboBoxBar::PropertyStoreGetCount() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return INT(m_arrPropertyStores.GetSize());
}

INT CExtPropertyGridComboBoxBar::PropertyStoreInsert(
	CExtPropertyStore * pPropertyStore,
	INT nPos // = -1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyStore );
	if( GetSafeHwnd() == NULL )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return -1;
	}
INT nCount = PropertyStoreGetCount();
	if( nPos < 0 || nPos > nCount )
		nPos = nCount;
	m_arrPropertyStores.InsertAt( nPos, pPropertyStore );
	if( InsertString( nPos, LPCTSTR(pPropertyStore->NameGet()) ) == CB_ERR )
	{
		m_arrPropertyStores.RemoveAt( nPos );
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return -1;
	}
	if( nCount == 0 ) // was zero
	{
		SetCurSel( 0 );
		_SynchronizeCurSel();
	}
	return nPos;
}

bool CExtPropertyGridComboBoxBar::PropertyStoreRemove(
	INT nPos // = -1 // if -1 - remove all
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( nPos < 0 )
	{
		m_arrPropertyStores.RemoveAll();
		if( GetSafeHwnd() != NULL )
			ResetContent();
		return true;
	}
	if( nPos >= PropertyStoreGetCount() )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	DeleteString( UINT(nPos) );
	m_arrPropertyStores.RemoveAt( nPos );
	return true;
}

CExtPropertyStore * CExtPropertyGridComboBoxBar::PropertyStoreGetAt(
	INT nPos
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( nPos < 0 || nPos >= PropertyStoreGetCount() )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
CExtPropertyStore * pPropertyStore = m_arrPropertyStores.GetAt( nPos );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyStore );
	return pPropertyStore;
}

const CExtPropertyStore * CExtPropertyGridComboBoxBar::PropertyStoreGetAt(
	INT nPos
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyGridComboBoxBar * > ( this ) )
		-> PropertyStoreGetAt( nPos );
}

void CExtPropertyGridComboBoxBar::SynchronizeCurSel()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	_SynchronizeCurSel();
}

void CExtPropertyGridComboBoxBar::_SynchronizeCurSel()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet();
	if( pPGC == NULL )
		return;
	if( PropertyStoreGetCount() == 0 )
	{
		pPGC->PropertyStoreSet( NULL );
		return;
	}
INT nCurSel = GetCurSel();
	if( nCurSel < 0 )
	{
		pPGC->PropertyStoreSet( NULL );
		return;
	}
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
CExtPropertyStore * pPropertyStore =
		PropertyStoreGetAt( nCurSel );
	__EXT_DEBUG_GRID_ASSERT( pPropertyStore );
	pPGC->PropertyStoreSet( pPropertyStore );
}

void CExtPropertyGridComboBoxBar::OnSelEndOK()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	Default();
	_SynchronizeCurSel();
	// added by Mr. Eyal Cohen
CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet();
	if( pPGC == NULL )
		return;
INT nCurSel = GetCurSel();
	if( nCurSel < 0 )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
CExtPropertyStore * pPS =
		PropertyStoreGetAt( nCurSel );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPS );
	pPGC->OnPgcStoreSelect( pPS );
}

LRESULT CExtPropertyGridComboBoxBar::OnSizeParent( WPARAM wParam, LPARAM lParam )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	wParam;
AFX_SIZEPARENTPARAMS * lpLayout =
		(AFX_SIZEPARENTPARAMS *) lParam;
	__EXT_DEBUG_GRID_ASSERT( lpLayout != NULL );
CRect rcFrameRest = &lpLayout->rect;
//	if(		rcFrameRest.left >= rcFrameRest.right
//		||	rcFrameRest.top >= rcFrameRest.bottom
//		)
//	{
//		if( lpLayout->hDWP == NULL )
//			return 0;
//		::SetWindowPos(
//			m_hWnd,
//			NULL, 0, 0, 0, 0,
//			SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER
//				|SWP_HIDEWINDOW
//			);
//		return 0;
//	}
DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle&WS_VISIBLE) == 0 )
		return 0;
CRect rcWnd;
	GetWindowRect( &rcWnd );
int nHeight = rcWnd.Height();
CRect rcOwnLayout( rcFrameRest );
	lpLayout->rect.top += nHeight;
	rcOwnLayout.bottom = rcOwnLayout.top + nHeight;
	lpLayout->sizeTotal.cy += nHeight;
//	__EXT_DEBUG_GRID_ASSERT( ! rcOwnLayout.IsRectEmpty() );
	if( lpLayout->hDWP != NULL )
	{
		::AfxRepositionWindow(
			lpLayout,
			m_hWnd,
			&rcOwnLayout
			);
	} // if( lpLayout->hDWP != NULL )
	return 0;
}

bool CExtPropertyGridComboBoxBar::Create(
	CWnd * pWndParent, 
	bool bVisible // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() == NULL );
	__EXT_DEBUG_GRID_ASSERT_VALID( pWndParent );
	__EXT_DEBUG_GRID_ASSERT( pWndParent->GetSafeHwnd() != NULL );
CRect rc( 0, 0, 0, 400 );
	if(	! CExtComboBox::Create(
			WS_CHILD
				| ( bVisible ? WS_VISIBLE : 0 )
				|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VSCROLL
				|CBS_DROPDOWNLIST|CBS_HASSTRINGS,
			rc,
			pWndParent,
			__EXT_MFC_ID_PROPERTY_GRID_COMBO_BOX
			)
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	SetFont(
		CFont::FromHandle(
			(HFONT)::GetStockObject(DEFAULT_GUI_FONT)
			)
		);
	return true;
}

void CExtPropertyGridComboBoxBar::PostNcDestroy()
{
	m_pPropertyGridCtrl = NULL;
	PropertyStoreRemove();
	if( m_bAutoDeleteWindow )
		delete this;
}

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyGridToolBar window

IMPLEMENT_DYNCREATE( CExtPropertyGridToolBar, CExtToolControlBar );

CExtPropertyGridToolBar::CExtPropertyGridToolBar(
	CExtPropertyGridCtrl * pPGC // = NULL
	)
	: m_pPropertyGridCtrl( pPGC )
	, m_bAutoDeleteWindow( false )
{
#ifdef _DEBUG
	if( m_pPropertyGridCtrl != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPropertyGridCtrl );
	}
#endif // _DEBUG

	m_bForceBalloonGradientInDialogs = true;
	m_cxLeftBorder = m_cxRightBorder = m_cyTopBorder = m_cyBottomBorder = 0;
}

CExtPropertyGridToolBar::~CExtPropertyGridToolBar()
{
}

#ifdef _DEBUG

void CExtPropertyGridToolBar::AssertValid() const
{
	CExtToolControlBar::AssertValid();
	if( m_pPropertyGridCtrl != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPropertyGridCtrl );
	}
}

void CExtPropertyGridToolBar::Dump( CDumpContext & dc ) const
{
	CExtToolControlBar::Dump( dc );
}

#endif

BEGIN_MESSAGE_MAP( CExtPropertyGridToolBar, CExtToolControlBar )
    //{{AFX_MSG_MAP(CExtPropertyGridToolBar)
	ON_WM_NCCALCSIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CExtPropertyGridToolBar::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bCalcValidRects;
	lpncsp;
}

void CExtPropertyGridToolBar::PropertyGridCtrlSet(
	CExtPropertyGridCtrl * pPGC
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_pPropertyGridCtrl = pPGC;
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

CExtPropertyGridCtrl * CExtPropertyGridToolBar::PropertyGridCtrlGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pPropertyGridCtrl != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPropertyGridCtrl );
	}
#endif // _DEBUG
	return m_pPropertyGridCtrl;
}

const CExtPropertyGridCtrl * CExtPropertyGridToolBar::PropertyGridCtrlGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyGridToolBar * > ( this ) )
			-> PropertyGridCtrlGet();
}

CExtPropertyGridCtrl & CExtPropertyGridToolBar::PropertyGridCtrlGetRef()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet(); 
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC ); // should be initialized
	return (*pPGC);
}

const CExtPropertyGridCtrl & CExtPropertyGridToolBar::PropertyGridCtrlGetRef() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet(); 
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC ); // should be initialized
	return (*pPGC);
}

CExtBarContentExpandButton * CExtPropertyGridToolBar::OnCreateBarRightBtn()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return NULL;
}

bool CExtPropertyGridToolBar::Create(
	CWnd * pWndParent, 
	bool bVisible // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT( __EXT_MFC_ID_PROPERTY_GRID_CATEGORIZED == ID_EXT_PROPERTY_GRID_CATEGORIZED );
	__EXT_DEBUG_GRID_ASSERT( __EXT_MFC_ID_PROPERTY_GRID_SORTED == ID_EXT_PROPERTY_GRID_SORTED );
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() == NULL );
	__EXT_DEBUG_GRID_ASSERT_VALID( pWndParent );
	__EXT_DEBUG_GRID_ASSERT( pWndParent->GetSafeHwnd() != NULL );
	m_bPresubclassDialogMode = true;
	if(	! CExtToolControlBar::Create(
			NULL,
			pWndParent,
			__EXT_MFC_ID_PROPERTY_GRID_TOOLBAR,
			WS_CHILD
				| ( bVisible ? WS_VISIBLE : 0 )
				|WS_CLIPCHILDREN|WS_CLIPSIBLINGS
				|CBRS_TOP // |CBRS_GRIPPER
					|CBRS_TOOLTIPS
				|CBRS_FLYBY|CBRS_SIZE_DYNAMIC
				|CBRS_HIDE_INPLACE
			)
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	if( ! CExtToolControlBar::LoadToolBar(
			IDR_EXT_TOOLBAR_PROPERTY_GRID
			)
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet();
	if( pPGC != NULL )
		SetOwner( pPGC );
	return true;
}

void CExtPropertyGridToolBar::PostNcDestroy()
{
	if( m_bAutoDeleteWindow )
		delete this;
}

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyGridTipBar window

IMPLEMENT_DYNCREATE( CExtPropertyGridTipBar, CWnd );
IMPLEMENT_CExtPmBridge_MEMBERS( CExtPropertyGridTipBar );

bool CExtPropertyGridTipBar::g_bPropertyTipBarClassRegistered = false;

CExtPropertyGridTipBar::CExtPropertyGridTipBar(
	CExtPropertyGridCtrl * pPGC // = NULL
	)
	: m_pPropertyGridCtrl( pPGC )
	, m_bAutoDeleteWindow( false )
	, m_nSeparatorHeight( min( 3, ::GetSystemMetrics(SM_CYSIZEFRAME) ) )
	, m_nHeightMin( 20 )
	, m_nHeightMax( 400 )
	, m_nHeightDesired( -1 )
	, m_eMTT( CExtPropertyGridTipBar::__EMTT_NONE )
	, m_bCanceling( false )
	, m_bTrackerDrawn( false )
	, m_nUpperSizingGap( 20 )
{
	RegisterPropertyTipBarClass();
#ifdef _DEBUG
	if( m_pPropertyGridCtrl != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPropertyGridCtrl );
	}
#endif // _DEBUG
CExtLocalResourceHelper _LRH;
CWinApp * pApp = ::AfxGetApp();
	__EXT_DEBUG_GRID_ASSERT_VALID( pApp );
	m_hCursorResizing = pApp->LoadCursor( MAKEINTRESOURCE( IDC_EXT_RESIZE_V1 ) );
	if( m_hCursorResizing == NULL )
	{
		m_hCursorResizing = ::LoadCursor( NULL, IDC_SIZENS );
		__EXT_DEBUG_GRID_ASSERT( m_hCursorResizing != NULL );
	}

	PmBridge_Install();
}

CExtPropertyGridTipBar::~CExtPropertyGridTipBar()
{
	PmBridge_Uninstall();

	if( m_hCursorResizing != NULL )
		::DestroyCursor( m_hCursorResizing );
}

bool CExtPropertyGridTipBar::Create(
	CWnd * pWndParent,
	bool bVisible // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() == NULL );
	__EXT_DEBUG_GRID_ASSERT_VALID( pWndParent );
	__EXT_DEBUG_GRID_ASSERT( pWndParent->GetSafeHwnd() != NULL );
	if( ! RegisterPropertyTipBarClass() )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	if(	! CWnd::CreateEx(
			0,
			__EXT_PROPERTY_TIP_BAR_CLASS_NAME,
			_T(""),
			WS_CHILD|WS_CLIPSIBLINGS|WS_CLIPCHILDREN
				| ( bVisible ? WS_VISIBLE : 0 ),
				CRect(0, 0, 0, g_PaintManager->UiScalingDo(60, CExtPaintManager::__EUIST_Y)),
			pWndParent,
			__EXT_MFC_ID_PROPERTY_GRID_TIPBAR,
			NULL
			)
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	return true;
}

bool CExtPropertyGridTipBar::RegisterPropertyTipBarClass()
{
	if( g_bPropertyTipBarClassRegistered )
		return true;
WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo( hInst, __EXT_PROPERTY_TIP_BAR_CLASS_NAME, &_wndClassInfo ) )
	{
		_wndClassInfo.style = CS_GLOBALCLASS|CS_DBLCLKS;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor = ::LoadCursor( NULL, IDC_ARROW );
		__EXT_DEBUG_GRID_ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_PROPERTY_TIP_BAR_CLASS_NAME;
		if( ! ::AfxRegisterClass( &_wndClassInfo ) )
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}
	g_bPropertyTipBarClassRegistered = true;
	return true;
}

bool CExtPropertyGridTipBar::IsResizingAllowed() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( HeightSeparatorGet() == 0 )
		return false;
	if( HeightMinGet() == HeightMaxGet() )
		return false;
	return true;
}

bool CExtPropertyGridTipBar::TrackerIsDrawn() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_bTrackerDrawn;
}

void CExtPropertyGridTipBar::TrackerDraw()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet();
	if( pPGC == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
	m_bTrackerDrawn = ! m_bTrackerDrawn;
CRect rcTracker = TrackerGetRect();
	ClientToScreen( &rcTracker );
	pPGC->ScreenToClient( &rcTracker );
CDC * pDC =
		pPGC->GetDCEx(
			NULL,
			DCX_CACHE
			|DCX_LOCKWINDOWUPDATE
			|DCX_CLIPSIBLINGS
			);
	if( pDC->GetSafeHdc() == NULL )
		return;
CBrush * pBrushOld =
		pDC->SelectObject( CDC::GetHalftoneBrush() );
    pDC->PatBlt(
		rcTracker.left, rcTracker.top,
		rcTracker.Width(), rcTracker.Height(),
		PATINVERT
		);
    pDC->SelectObject(pBrushOld);
    ReleaseDC(pDC);
}

void CExtPropertyGridTipBar::TrackerFlush()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( TrackerIsDrawn() )
		TrackerDraw();
	//m_nHeightDesired = -1;
}

CRect CExtPropertyGridTipBar::TrackerGetRect()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CRect rcTracker;
	GetWindowRect( &rcTracker );
	ScreenToClient( &rcTracker );
	if( m_nHeightDesired >= 0 )
		rcTracker.top = rcTracker.bottom - m_nHeightDesired;
	if( rcTracker.Height() < m_nHeightMin )
		rcTracker.top = rcTracker.bottom - m_nHeightMin;
INT nHeightMax = m_nHeightMax;
CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet();
	if( pPGC != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
		CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
		pPGC->OnPgcQueryGrids( arrGrids );
		if( arrGrids.GetSize() > 0 )
		{
			CRect rcWndGrid, rcWndOwn;
			arrGrids[0]->GetWindowRect( &rcWndGrid );
			GetWindowRect( &rcWndOwn );
			INT nOtherHeightMax =
				rcWndOwn.bottom - rcWndGrid.top - m_nUpperSizingGap;
			if( nOtherHeightMax < m_nUpperSizingGap )
				nOtherHeightMax = m_nUpperSizingGap;
			if( nHeightMax > nOtherHeightMax )
				nHeightMax = nOtherHeightMax;
		} // if( arrGrids.GetSize() > 0 )
	} // if( pPGC != NULL )
	if( rcTracker.Height() > nHeightMax )
		rcTracker.top = rcTracker.bottom - nHeightMax;
	rcTracker.bottom = rcTracker.top + m_nSeparatorHeight + 1;
	return rcTracker;
}

INT CExtPropertyGridTipBar::HeightMinGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_nHeightMin;
}

void CExtPropertyGridTipBar::HeightMinSet( INT nHeight )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_nHeightMin = nHeight;
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

INT CExtPropertyGridTipBar::HeightMaxGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_nHeightMax;
}

void CExtPropertyGridTipBar::HeightMaxSet( INT nHeight )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_nHeightMax = nHeight;
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

INT CExtPropertyGridTipBar::HeightSeparatorGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_nSeparatorHeight;
}

void CExtPropertyGridTipBar::HeightSeparatorSet( INT nHeight )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_nSeparatorHeight = nHeight;
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

#ifdef _DEBUG

void CExtPropertyGridTipBar::AssertValid() const
{
	CWnd::AssertValid();
	__EXT_DEBUG_GRID_ASSERT( m_nSeparatorHeight >= 0 );
	__EXT_DEBUG_GRID_ASSERT( m_nHeightMin >= 0 );
	__EXT_DEBUG_GRID_ASSERT( m_nHeightMax >= 0 );
	__EXT_DEBUG_GRID_ASSERT( m_nHeightMin <= m_nHeightMax );
	if( m_pPropertyGridCtrl != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPropertyGridCtrl );
	}
	__EXT_DEBUG_GRID_ASSERT( m_hCursorResizing != NULL );
}

void CExtPropertyGridTipBar::Dump( CDumpContext & dc ) const
{
	CWnd::Dump( dc );
}

#endif

BEGIN_MESSAGE_MAP( CExtPropertyGridTipBar, CWnd )
    //{{AFX_MSG_MAP(CExtPropertyGridTipBar)
	ON_WM_NCCALCSIZE()
	ON_WM_PAINT()
	ON_WM_CANCELMODE()
	ON_WM_CAPTURECHANGED()
	ON_WM_MOUSEACTIVATE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_SETCURSOR()
	//}}AFX_MSG_MAP
	ON_WM_ACTIVATEAPP()
	ON_MESSAGE( WM_SIZEPARENT, OnSizeParent )
END_MESSAGE_MAP()

void CExtPropertyGridTipBar::OnCancelMode()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CWnd::OnCancelMode();
	if( m_bCanceling )
		ReleaseCapture();
	m_bCanceling = true;
	TrackerFlush();
	if( GetCapture() == this )
		ReleaseCapture();
	m_eMTT = CExtPropertyGridTipBar::__EMTT_NONE;
	m_bCanceling = false;
}

void CExtPropertyGridTipBar::OnCaptureChanged(CWnd *pWnd)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CWnd::OnCaptureChanged( pWnd );
	if( pWnd != this )
		SendMessage( WM_CANCELMODE );
}

void CExtPropertyGridTipBar::OnLButtonDown(UINT nFlags, CPoint point)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nFlags;
	if( m_eMTT != __EMTT_NONE )
	{
		SendMessage( WM_CANCELMODE );
		return;
	}
CRect rcTracker = TrackerGetRect();
	if( rcTracker.PtInRect( point ) )
	{
		m_eMTT = __EMTT_RESIZING;
		CRect rcWnd;
		GetWindowRect( &rcWnd );
		m_nHeightDesired = rcWnd.Height();
		SetCapture();
		TrackerDraw();
		DoSetCursor();
	} // if( rcTracker.PtInRect( point ) )
}

void CExtPropertyGridTipBar::OnLButtonUp(UINT nFlags, CPoint point)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nFlags;
	point;
	switch( m_eMTT )
	{
	case __EMTT_RESIZING:
		{
			SendMessage( WM_CANCELMODE );
			CExtPropertyGridCtrl * pPGC =
				PropertyGridCtrlGet();
			if( pPGC != NULL )
			{
				__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
				pPGC->RecalcLayout();
			}
			Invalidate();
			UpdateWindow();
			DoSetCursor();
		}
	break;
	case __EMTT_NONE:
	break;
	default:
		__EXT_DEBUG_GRID_ASSERT( FALSE );
	break;
	} // switch( m_eMTT )
}

void CExtPropertyGridTipBar::OnMouseMove(UINT nFlags, CPoint point)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nFlags;
	switch( m_eMTT )
	{
	case __EMTT_RESIZING:
		{
			CRect rcWnd;
			GetWindowRect( &rcWnd );
			ScreenToClient( &rcWnd );
			INT nHeightDesired = rcWnd.bottom - point.y;
			if( nHeightDesired < m_nHeightMin )
				nHeightDesired = m_nHeightMin;
			INT nHeightMax = m_nHeightMax;
			CExtPropertyGridCtrl * pPGC =
				PropertyGridCtrlGet();
			if( pPGC != NULL )
			{
				__EXT_DEBUG_GRID_ASSERT_VALID( pPGC );
				CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
				pPGC->OnPgcQueryGrids( arrGrids );
				if( arrGrids.GetSize() > 0 )
				{
					CRect rcWndGrid, rcWndOwn;
					arrGrids[0]->GetWindowRect( &rcWndGrid );
					GetWindowRect( &rcWndOwn );
					INT nOtherHeightMax =
						rcWndOwn.bottom - rcWndGrid.top - m_nUpperSizingGap;
					if( nOtherHeightMax < m_nUpperSizingGap )
						nOtherHeightMax = m_nUpperSizingGap;
					if( nHeightMax > nOtherHeightMax )
						nHeightMax = nOtherHeightMax;
				} // if( arrGrids.GetSize() > 0 )
			} // if( pPGC != NULL )
			if( nHeightDesired > nHeightMax )
				nHeightDesired = nHeightMax;
			if( m_nHeightDesired == nHeightDesired )
				return;
			TrackerFlush();
			m_nHeightDesired = nHeightDesired;
			TrackerDraw();
		}
	break;
	case __EMTT_NONE:
	break;
	default:
		__EXT_DEBUG_GRID_ASSERT( FALSE );
	break;
	} // switch( m_eMTT )
}

bool CExtPropertyGridTipBar::DoSetCursor()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CPoint ptCursor;
	if( ! ::GetCursorPos(&ptCursor) )
		return false;
	ScreenToClient( &ptCursor );
	return DoSetCursor( ptCursor );
}

bool CExtPropertyGridTipBar::DoSetCursor( CPoint ptCursor )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_hCursorResizing == NULL )
		return false;
	if( m_eMTT == __EMTT_RESIZING )
	{
		::SetCursor( m_hCursorResizing );
		return true;
	}
CRect rcTracker = TrackerGetRect();
	if( rcTracker.PtInRect( ptCursor ) )
	{
		::SetCursor( m_hCursorResizing );
		return true;
	}
	return false;
}

BOOL CExtPropertyGridTipBar::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CPoint ptCursor;
	if( ! ::GetCursorPos(&ptCursor) )
		return CWnd::OnSetCursor(pWnd,nHitTest,message);
	ScreenToClient( &ptCursor );
	if( DoSetCursor( ptCursor ) )
		return TRUE;
	return CWnd::OnSetCursor( pWnd, nHitTest, message );
}

#if _MFC_VER < 0x700
void CExtPropertyGridTipBar::OnActivateApp(BOOL bActive, HTASK hTask) 
#else
void CExtPropertyGridTipBar::OnActivateApp(BOOL bActive, DWORD hTask) 
#endif
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CWnd::OnActivateApp(bActive, hTask);
	if( ! bActive )
		SendMessage( WM_CANCELMODE );
}

int CExtPropertyGridTipBar::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message) 
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( (GetStyle()&WS_TABSTOP) == 0 )
		return MA_NOACTIVATE;
	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

void CExtPropertyGridTipBar::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bCalcValidRects;
	lpncsp;
}

void CExtPropertyGridTipBar::OnPaint()
{
CPaintDC dcPaint( this );
	DoPaint( &dcPaint );
}

void CExtPropertyGridTipBar::DoPaint( CDC * pDC )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pDC );
CExtPaintManager * pPM = PmBridge_GetPM();
CRect rcClient;
	GetClientRect( &rcClient );
CExtMemoryDC dc( pDC, &rcClient );
	if(		(! pPM->GetCb2DbTransparentMode( this ) )
		||	(! pPM->PaintDockerBkgnd(
				true,
				dc,
				this
				)
			)
		)
		dc.FillSolidRect(
			&rcClient,
			pPM->GetColor(
				CExtPaintManager::CLR_WRB_FRAME
			)
			);
CRect rcDraw = rcClient;
	rcDraw.top += m_nSeparatorHeight;
	if(		rcDraw.left >= rcDraw.right
		||	rcDraw.top >= rcDraw.bottom
		)
		return;
	pPM->PaintResizableBarChildNcAreaRect(
		dc,
		rcDraw,
		this
		);
	rcDraw.DeflateRect( 5, 3 );
	if(		rcDraw.left >= rcDraw.right
		||	rcDraw.top >= rcDraw.bottom
		)
		return;
CExtSafeString str = PropertyNameGet();
	if( str.IsEmpty() )
		return;
int nOldBkMode = dc.SetBkMode( TRANSPARENT );

COLORREF clrText =
		pPM->QueryObjectTextColor(
			dc,
			true,
			false,
			false,
			false,
			(CObject*)this
			);
	if( clrText == COLORREF(-1L) )
		clrText =
			pPM->GetColor(
				COLOR_BTNTEXT,
				(CObject*)this
				);

COLORREF clrOldTextColor =
		dc.SetTextColor( clrText );
CFont * pOldFont =
		dc.SelectObject(
			&pPM->m_FontBold
			);
CRect rcMeasure( 0, 0, 0, 0 );
	CExtRichContentLayout::stat_DrawText(
		dc.m_hDC,
		LPCTSTR(str), str.GetLength(),
		&rcMeasure,
		DT_SINGLELINE|DT_LEFT|DT_TOP|DT_END_ELLIPSIS|DT_CALCRECT, 0
		);
	CExtRichContentLayout::stat_DrawText(
		dc.m_hDC,
		LPCTSTR(str), str.GetLength(),
		&rcDraw,
		DT_SINGLELINE|DT_LEFT|DT_TOP|DT_END_ELLIPSIS, 0
		);
	str = PropertyDescriptionGet();
	if( ! str.IsEmpty() )
	{
		rcDraw.top += rcMeasure.Height() + 3;
		if(		rcDraw.left < rcDraw.right
			&&	rcDraw.top < rcDraw.bottom
			)
		{
			dc.SelectObject( 
				&pPM->m_FontNormal
				);
			CExtRichContentLayout::stat_DrawText(
				dc.m_hDC,
				LPCTSTR(str), str.GetLength(),
				&rcDraw,
				DT_LEFT|DT_TOP|DT_WORDBREAK, 0
				);
		}
	} // if( ! str.IsEmpty() )
	dc.SelectObject( pOldFont );
	dc.SetTextColor( clrOldTextColor );
	dc.SetBkMode( nOldBkMode );
}

LRESULT CExtPropertyGridTipBar::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if( message == WM_PRINT || message == WM_PRINTCLIENT )
	{
		CDC * pDC = CDC::FromHandle( (HDC)wParam );
		DoPaint( pDC );
		return (!0);
	}	
	return CWnd::WindowProc(message, wParam, lParam);
}

CExtSafeString CExtPropertyGridTipBar::PropertyNameGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
	if( pPGC == NULL )
		return CExtSafeString( _T("") );
CExtPropertyGridWnd * pPGW = pPGC->GetActiveGrid();
	if( pPGW == NULL )
		return CExtSafeString( _T("") );
HTREEITEM hTreeItem =
		pPGW->ItemFocusGet();
	if( hTreeItem == NULL )
		return CExtSafeString( _T("") );
CExtPropertyItem * pPropertyItem =
		pPGW->PropertyItemFromTreeItem( hTreeItem );
	if( pPropertyItem == NULL )
		return CExtSafeString( _T("") );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	return pPropertyItem->NameGet();
}

CExtSafeString CExtPropertyGridTipBar::PropertyDescriptionGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyGridCtrl * pPGC = PropertyGridCtrlGet();
	if( pPGC == NULL )
		return CExtSafeString( _T("") );
CExtPropertyGridWnd * pPGW = pPGC->GetActiveGrid();
	if( pPGW == NULL )
		return CExtSafeString( _T("") );
HTREEITEM hTreeItem =
		pPGW->ItemFocusGet();
	if( hTreeItem == NULL )
		return CExtSafeString( _T("") );
CExtPropertyItem * pPropertyItem =
		pPGW->PropertyItemFromTreeItem( hTreeItem );
	if( pPropertyItem == NULL )
		return CExtSafeString( _T("") );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	return pPropertyItem->DescriptionGet();
}

LRESULT CExtPropertyGridTipBar::OnSizeParent( WPARAM wParam, LPARAM lParam )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	wParam;
AFX_SIZEPARENTPARAMS * lpLayout =
		(AFX_SIZEPARENTPARAMS *) lParam;
	__EXT_DEBUG_GRID_ASSERT( lpLayout != NULL );
CRect rcFrameRest = &lpLayout->rect;
//	if(		rcFrameRest.left >= rcFrameRest.right
//		||	rcFrameRest.top >= rcFrameRest.bottom
//		)
//	{
//		if( lpLayout->hDWP == NULL )
//			return 0;
//		::SetWindowPos(
//			m_hWnd,
//			NULL, 0, 0, 0, 0,
//			SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER
//				|SWP_HIDEWINDOW
//			);
//		return 0;
//	}
DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle&WS_VISIBLE) == 0 )
		return 0;
CRect rcWnd;
	GetWindowRect( &rcWnd );
CSize _sizeNeeded = rcWnd.Size();
	if( m_nHeightDesired >= 0 )
	{
		_sizeNeeded.cy = m_nHeightDesired;
		m_nHeightDesired = -1;
	}
	if( _sizeNeeded.cy < m_nHeightMin )
		_sizeNeeded.cy = m_nHeightMin;
	if( _sizeNeeded.cy > m_nHeightMax )
		_sizeNeeded.cy = m_nHeightMax;
INT nHeightRest = lpLayout->rect.bottom - lpLayout->rect.top - _sizeNeeded.cy;
	if( nHeightRest < m_nUpperSizingGap )
		_sizeNeeded.cy = lpLayout->rect.bottom - lpLayout->rect.top - m_nUpperSizingGap;
	if( _sizeNeeded.cy < m_nUpperSizingGap )
		_sizeNeeded.cy = m_nUpperSizingGap;
CRect rcOwnLayout( rcFrameRest );
	lpLayout->rect.bottom -= _sizeNeeded.cy;
	rcOwnLayout.top = rcOwnLayout.bottom - _sizeNeeded.cy;
	lpLayout->sizeTotal.cy += _sizeNeeded.cy;
//	__EXT_DEBUG_GRID_ASSERT( ! rcOwnLayout.IsRectEmpty() );
	if( lpLayout->hDWP != NULL )
	{
		::AfxRepositionWindow(
			lpLayout,
			m_hWnd,
			&rcOwnLayout
			);
	} // if( lpLayout->hDWP != NULL )
	return 0;
}

void CExtPropertyGridTipBar::PropertyGridCtrlSet(
	CExtPropertyGridCtrl * pPGC
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_pPropertyGridCtrl = pPGC;
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

CExtPropertyGridCtrl * CExtPropertyGridTipBar::PropertyGridCtrlGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pPropertyGridCtrl != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPropertyGridCtrl );
	}
#endif // _DEBUG
	return m_pPropertyGridCtrl;
}

const CExtPropertyGridCtrl * CExtPropertyGridTipBar::PropertyGridCtrlGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyGridTipBar * > ( this ) )
			-> PropertyGridCtrlGet();
}

CExtPropertyGridCtrl & CExtPropertyGridTipBar::PropertyGridCtrlGetRef()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet(); 
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC ); // should be initialized
	return (*pPGC);
}

const CExtPropertyGridCtrl & CExtPropertyGridTipBar::PropertyGridCtrlGetRef() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtPropertyGridCtrl * pPGC =
		PropertyGridCtrlGet(); 
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGC ); // should be initialized
	return (*pPGC);
}

void CExtPropertyGridTipBar::PostNcDestroy()
{
	if( m_bAutoDeleteWindow )
		delete this;
}

/////////////////////////////////////////////////////////////////////////////
// CExtPropertyGridCtrl window

IMPLEMENT_DYNCREATE( CExtPropertyGridCtrl, CWnd );

const UINT CExtPropertyGridCtrl::g_nMsgInitPropertyGridCtrl =
	::RegisterWindowMessage(
		_T("CExtPropertyGridCtrl::g_nMsgInitPropertyGridCtrl")
		);

bool CExtPropertyGridCtrl::g_bPropertyGridCtrlClassRegistered = false;

CExtPropertyGridCtrl::CExtPropertyGridCtrl()
	: m_bHelperDirectCreateCall( false )
	, m_bInPreTranslateMessage( false )
	, m_bInConrolBarUpdate( false )
	, m_bEnabledControlBarUpdate( true )
	, m_pPS( NULL )
	, m_bInitialized( false )
	, m_bCreateComboBoxBar( true )
	, m_bCreateToolBar( true )
	, m_bCreateTipBar( true )
	, m_bCreateCategorizedGrid( true )
	, m_bCreateSortedGrid( true )
	, m_bEnableResetCommand( true )
	, m_bHelperCaseInsensitivePropertyCaptionComparison( false )
{
	__EXT_DEBUG_GRID_ASSERT( __EXT_MFC_ID_PROPERTY_GRID_CATEGORIZED == ID_EXT_PROPERTY_GRID_CATEGORIZED );
	__EXT_DEBUG_GRID_ASSERT( __EXT_MFC_ID_PROPERTY_GRID_SORTED == ID_EXT_PROPERTY_GRID_SORTED );
	__EXT_DEBUG_GRID_VERIFY( RegisterPropertyGridCtrlClass() );
#ifdef _DEBUG
	if( m_pPS != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPS );
	}
#endif // _DEBUG
	m_strCommandProfile.Format(
		_T("ExtPropertyGridCtrl-%p-%d-%d"),
		this,
		int( ::GetCurrentThreadId() ),
		int( ::GetCurrentProcessId() )
		);
}

CExtPropertyGridCtrl::~CExtPropertyGridCtrl()
{
}

bool CExtPropertyGridCtrl::Create(
	CWnd * pWndParent,
	UINT nDlgCtrlID, // = UINT(__EXT_MFC_IDC_STATIC)
	CRect rc, // = CRect( 0, 0, 0, 0 )
	DWORD dwWndStyles, // = __EXT_PROPERTY_GRID_CTRL_WND_STYLES_DEFAULT
	DWORD dwWndStylesEx // = __EXT_PROPERTY_GRID_CTRL_WND_EX_STYLES_DEFAULT
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() == NULL );
	__EXT_DEBUG_GRID_ASSERT_VALID( pWndParent );
	__EXT_DEBUG_GRID_ASSERT( pWndParent->GetSafeHwnd() != NULL );
	if( ! RegisterPropertyGridCtrlClass() )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	m_bHelperDirectCreateCall = true;
	if(	! CWnd::CreateEx(
			dwWndStylesEx,
			__EXT_PROPERTY_GRID_CTRL_CLASS_NAME,
			_T(""),
			dwWndStyles,
			rc,
			pWndParent,
			nDlgCtrlID,
			NULL
			)
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
HWND hWndOwn = m_hWnd;
	if( SendMessage(
			CExtPropertyGridCtrl::g_nMsgInitPropertyGridCtrl
			) == 0
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		if( ::IsWindow(hWndOwn) )
			::DestroyWindow( hWndOwn );
		return false;
	}
	m_bHelperDirectCreateCall = false;
	return true;
}

bool CExtPropertyGridCtrl::RegisterPropertyGridCtrlClass()
{
	if( g_bPropertyGridCtrlClassRegistered )
		return true;
WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo( hInst, __EXT_PROPERTY_GRID_CTRL_CLASS_NAME, &_wndClassInfo ) )
	{
		_wndClassInfo.style = CS_GLOBALCLASS|CS_DBLCLKS;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor = ::LoadCursor( NULL, IDC_ARROW );
		__EXT_DEBUG_GRID_ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_PROPERTY_GRID_CTRL_CLASS_NAME;
		if( ! ::AfxRegisterClass( &_wndClassInfo ) )
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}
	g_bPropertyGridCtrlClassRegistered = true;
	return true;
}

#ifdef _DEBUG

void CExtPropertyGridCtrl::AssertValid() const
{
	CWnd::AssertValid();
	if( m_pPS != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPS );
	}
}

void CExtPropertyGridCtrl::Dump( CDumpContext & dc ) const
{
	CWnd::Dump( dc );
}

#endif

BEGIN_MESSAGE_MAP( CExtPropertyGridCtrl, CWnd )
    //{{AFX_MSG_MAP(CExtPropertyGridCtrl)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_CONTEXTMENU()
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE(
		CExtPropertyGridCtrl::g_nMsgInitPropertyGridCtrl,
		OnMsgInitPropertyGridCtrl
		)
END_MESSAGE_MAP()

void CExtPropertyGridCtrl::OnPgcStoreSelect( // added by Mr. Eyal Cohen
	CExtPropertyStore * pPS
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPS );
	pPS;
}

void CExtPropertyGridCtrl::OnPgcInputComplete(
	CExtPropertyGridWnd * pPGW,
	CExtPropertyItem * pPropertyItem
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	pPGW;
	pPropertyItem;
}

bool CExtPropertyGridCtrl::OnPgcCreateBars()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	try
	{
		if( m_bCreateComboBoxBar )
		{
			CExtPropertyGridComboBoxBar * pComboBoxBar = new CExtPropertyGridComboBoxBar( this );
			pComboBoxBar->m_bAutoDeleteWindow = true;
			if( ! pComboBoxBar->Create(
					this,
					true
					)
				)
			{
				__EXT_DEBUG_GRID_ASSERT( FALSE );
				throw __EXT_MFC_ID_PROPERTY_GRID_COMBO_BOX;
			}
		} // if( m_bCreateComboBoxBar )
		if( m_bCreateToolBar )
		{
			CExtPropertyGridToolBar * pToolBar = new CExtPropertyGridToolBar( this );
			pToolBar->m_bAutoDeleteWindow = true;
			if( ! pToolBar->Create(
					this,
					true
					)
				)
			{
				__EXT_DEBUG_GRID_ASSERT( FALSE );
				throw __EXT_MFC_ID_PROPERTY_GRID_TOOLBAR;
			}
		} // if( m_bCreateToolBar )
		if( m_bCreateTipBar )
		{
			CExtPropertyGridTipBar * pTipBar = new CExtPropertyGridTipBar( this );
			pTipBar->m_bAutoDeleteWindow = true;
			if( ! pTipBar->Create(
					this,
					true
					)
				)
			{
				__EXT_DEBUG_GRID_ASSERT( FALSE );
				throw __EXT_MFC_ID_PROPERTY_GRID_TIPBAR;
			}
		} // if( m_bCreateTipBar )
	}
	catch( ... )
	{
		return false;
	}
	return true;
}

bool CExtPropertyGridCtrl::OnPgcCreateGrids()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	try
	{
		if( m_bCreateCategorizedGrid )
		{
			CExtPropertyGridWndCategorized * pGridCategorized = new CExtPropertyGridWndCategorized( this );
			pGridCategorized->m_bAutoDeleteWindow = true;
			if( ! pGridCategorized->Create(
					this,
					__EXT_MFC_ID_PROPERTY_GRID_CATEGORIZED,
					true
					)
				)
			{
				__EXT_DEBUG_GRID_ASSERT( FALSE );
				throw __EXT_MFC_ID_PROPERTY_GRID_CATEGORIZED;
			}
		} // if( m_bCreateCategorizedGrid )
		if( m_bCreateSortedGrid )
		{
			CExtPropertyGridWndSorted * pGridSorted = new CExtPropertyGridWndSorted( this );
			pGridSorted->m_bAutoDeleteWindow = true;
			if( ! pGridSorted->Create(
					this,
					__EXT_MFC_ID_PROPERTY_GRID_SORTED
					)
				)
			{
				__EXT_DEBUG_GRID_ASSERT( FALSE );
				throw __EXT_MFC_ID_PROPERTY_GRID_SORTED;
			}
		} // if( m_bCreateSortedGrid )
	}
	catch( ... )
	{
		return false;
	}
	return true;
}

void CExtPropertyGridCtrl::OnPgcQueryGrids(
	CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > & arrGrids
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	arrGrids.RemoveAll();
	if( GetSafeHwnd() == NULL )
		return;
HWND hWnd = ::GetWindow( m_hWnd, GW_CHILD );
	for( ; hWnd != NULL; hWnd = ::GetWindow(hWnd,GW_HWNDNEXT) )
	{
		CWnd * pWndPermanent = CWnd::FromHandlePermanent( hWnd );
		if( pWndPermanent == NULL )
			continue;
		CExtPropertyGridWnd * pPropertyGridWnd =
			DYNAMIC_DOWNCAST(
				CExtPropertyGridWnd,
				pWndPermanent
				);
		if( pPropertyGridWnd == NULL )
			continue;
		if( (pPropertyGridWnd->GetStyle()&WS_VISIBLE) != 0 )
			arrGrids.InsertAt( 0, pPropertyGridWnd );
		else
			arrGrids.Add( pPropertyGridWnd );
	} // for( ; hWnd != NULL; hWnd = ::GetWindow(hWnd,GW_HWNDNEXT) )
}

bool CExtPropertyGridCtrl::OnPgcProcessChildWindowCommand(
	CWnd * pWndChild,
	UINT nID,
	int nCode,
	void * pExtra,
	AFX_CMDHANDLERINFO * pHandlerInfo
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
	__EXT_DEBUG_GRID_ASSERT_VALID( pWndChild );
	__EXT_DEBUG_GRID_ASSERT( pWndChild->GetSafeHwnd() != NULL );
	__EXT_DEBUG_GRID_ASSERT( nCode == CN_COMMAND || nCode == CN_UPDATE_COMMAND_UI );
	nID;
	pHandlerInfo;
	if( pWndChild->IsKindOf( RUNTIME_CLASS( CControlBar ) ) )
	{
		BOOL bVisible = ((CControlBar *)pWndChild)->IsVisible();
		if( nCode == CN_COMMAND )
		{
			((CControlBar *)pWndChild)->DelayShow( ! bVisible );
			RecalcLayout();
			return true;
		}
		CCmdUI * pCmdUI = (CCmdUI *)pExtra;
		__EXT_DEBUG_GRID_ASSERT( pCmdUI != NULL );
		pCmdUI->Enable();
		pCmdUI->SetCheck( bVisible );
		return true;
	} // if( pWndChild->IsKindOf( RUNTIME_CLASS( CControlBar ) ) )
	if( pWndChild->IsKindOf( RUNTIME_CLASS( CExtPropertyGridWnd ) ) )
	{
		BOOL bVisible = ( (pWndChild->GetStyle()&WS_VISIBLE) != 0 ) ? TRUE : FALSE;
		if( nCode == CN_COMMAND )
		{
			if( bVisible )
				return true;
			CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
			OnPgcQueryGrids( arrGrids );
			__EXT_DEBUG_GRID_ASSERT( arrGrids.GetSize() >= 1 );
			if( arrGrids.GetSize() == 1 )
			{
				__EXT_DEBUG_GRID_ASSERT( LPVOID(arrGrids[0]) == LPVOID(pWndChild) );
				return true;
			}
			INT nGridIdx = 0;
			for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
			{
				CExtPropertyGridWnd * pGrid = arrGrids[ nGridIdx ];
				__EXT_DEBUG_GRID_ASSERT_VALID( pGrid );
				if( LPVOID(pGrid) == LPVOID(pWndChild) )
					continue;
				if( (pGrid->GetStyle()&WS_VISIBLE) != 0 )
					pGrid->ShowWindow( SW_HIDE );
			} // for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
			pWndChild->ShowWindow( SW_SHOW );
			RecalcLayout();
			pWndChild->SetFocus();
			RedrawFocusDependentChildren();
			return true;
		} // if( nCode == CN_COMMAND )
		CCmdUI * pCmdUI = (CCmdUI *)pExtra;
		__EXT_DEBUG_GRID_ASSERT( pCmdUI != NULL );
		pCmdUI->Enable();
		pCmdUI->SetRadio( bVisible );
		return true;
	} // if( pWndChild->IsKindOf( RUNTIME_CLASS( CExtPropertyGridWnd ) ) )
	return false;
}

void CExtPropertyGridCtrl::RecalcLayout()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	_EnsureInitialized();
CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
	OnPgcQueryGrids( arrGrids );
UINT nIDLeftOver = 0;
	if(		arrGrids.GetSize() > 0
		&&	((arrGrids[0])->GetStyle()&WS_VISIBLE) != 0
		)
	{
		nIDLeftOver = UINT((arrGrids[0])->GetDlgCtrlID());
		__EXT_DEBUG_GRID_ASSERT( nIDLeftOver != 0 );
	}
CRect rc;
	GetClientRect( &rc );
	RepositionBars( 0, 0x0FFFF, nIDLeftOver, CWnd::reposDefault, &rc, &rc );
	if( nIDLeftOver != 0 )
	{
		INT nGridIdx = 1;
		for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
		{
			CExtPropertyGridWnd * pPropertyGridWnd =
				arrGrids[nGridIdx];
			__EXT_DEBUG_GRID_ASSERT( (pPropertyGridWnd->GetStyle()&WS_VISIBLE) == 0 );
			pPropertyGridWnd->MoveWindow( &rc );
		} // for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
	} // if( nIDLeftOver != 0 )
}

BOOL CExtPropertyGridCtrl::OnCmdMsg( UINT nID, int nCode, void * pExtra, AFX_CMDHANDLERINFO * pHandlerInfo )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( nCode == CN_COMMAND || nCode == CN_UPDATE_COMMAND_UI )
	{
		HWND hWnd = ::GetDlgItem( m_hWnd, int(nID) );
		if( hWnd != NULL )
		{
			CWnd * pWndPermanent = CWnd::FromHandlePermanent( hWnd );
			if( pWndPermanent != NULL )
			{
				if( OnPgcProcessChildWindowCommand(
						pWndPermanent,
						nID,
						nCode,
						pExtra,
						pHandlerInfo
						)
					)
					return TRUE;
			} // if( pWndPermanent != NULL )
		} // if( hWnd != NULL )
	} // if( nCode == CN_COMMAND || nCode == CN_UPDATE_COMMAND_UI )
	return CWnd::OnCmdMsg( nID, nCode, pExtra, pHandlerInfo );
}

void CExtPropertyGridCtrl::_EnsureInitialized()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_bInitialized )
		return;
	m_bInitialized = true;
	if( ! OnPgcCreateBars() )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
	}
	if( ! OnPgcCreateGrids() )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
	}
	RecalcLayout();
}

LRESULT CExtPropertyGridCtrl::OnMsgInitPropertyGridCtrl( WPARAM wParam, LPARAM lParam )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	wParam;
	lParam;
	_EnsureInitialized();
	return (!0);
}

void CExtPropertyGridCtrl::OnSize(UINT nType, int cx, int cy) 
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CWnd::OnSize( nType, cx, cy );
	if( nType != SIZE_MINIMIZED )
		RecalcLayout();
}

void CExtPropertyGridCtrl::OnSetFocus(CWnd* pOldWnd) 
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	pOldWnd;
CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
	OnPgcQueryGrids( arrGrids );
	if( arrGrids.GetSize() == 0 )
		return;
CExtPropertyGridWnd * pGrid = arrGrids[ 0 ];
	__EXT_DEBUG_GRID_ASSERT_VALID( pGrid );
	if( (pGrid->GetStyle()&WS_VISIBLE) != 0 )
		pGrid->SetFocus();
}

CWnd * CExtPropertyGridCtrl::GetChildByRTC(
	CRuntimeClass * pRTC,
	CWnd * pWndStartSearchAfter // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( pRTC != NULL );
	if( GetSafeHwnd() == NULL )
		return NULL;
	_EnsureInitialized();
HWND hWnd =
		( pWndStartSearchAfter->GetSafeHwnd() == NULL )
			? ::GetWindow( m_hWnd, GW_CHILD )
			: ::GetWindow( pWndStartSearchAfter->m_hWnd, GW_HWNDNEXT )
			;
	for(	;
			hWnd != NULL;
			hWnd = ::GetWindow( hWnd, GW_HWNDNEXT )
			)
	{
		CWnd * pWnd = CWnd::FromHandlePermanent( hWnd );
		if( pWnd == NULL )
			continue;
		if( pWnd->IsKindOf(pRTC) )
			return pWnd;
	}
	return NULL;
}

void CExtPropertyGridCtrl::RedrawFocusDependentChildren()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
CWnd * pWnd = GetChildByRTC( RUNTIME_CLASS(CExtPropertyGridTipBar) );
	if(		pWnd != NULL
		&&	(pWnd->GetStyle()&WS_VISIBLE) != 0
		)
	{
		pWnd->Invalidate();
		pWnd->UpdateWindow();
	} // if( pWnd != NULL ...
}

const CExtPropertyGridWnd * CExtPropertyGridCtrl::GetActiveGrid() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtPropertyGridCtrl * > ( this ) )
			-> GetActiveGrid();
}

CExtPropertyGridWnd * CExtPropertyGridCtrl::GetActiveGrid()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	_EnsureInitialized();
CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
	OnPgcQueryGrids( arrGrids );
	if( arrGrids.GetSize() == 0 )
		return NULL;
	return arrGrids[0];
}

CRuntimeClass * CExtPropertyGridCtrl::OnPgcCellAreaGetRTC( CExtPropertyGridWnd & wndPGW, CExtPropertyItem * pPropertyItem )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&wndPGW) );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	wndPGW; pPropertyItem;
	return RUNTIME_CLASS(CExtPropertyGridCellArea);
}

void CExtPropertyGridCtrl::OnPgcCellAreaConfigure( CExtPropertyGridWnd & wndPGW, CExtPropertyItem * pPropertyItem, CExtPropertyGridCellArea * pCellCaption )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&wndPGW) );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellCaption );
	wndPGW; pPropertyItem;
	if( m_bHelperCaseInsensitivePropertyCaptionComparison )
		pCellCaption->m_bHelperCaseInsensitivePropertyCaptionComparison = true;
}


void CExtPropertyGridCtrl::PropertyValueSynchronizeSimple(
	const CExtPropertyValue * pPV,
	bool bUseActiveGridCell, // = true
	bool bUpdateActiveTreeGridWindow // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	_EnsureInitialized();
bool bInvalidated = false;
	if( pPV != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT( pPV );
		const CExtGridCell * pCellSrc =
			bUseActiveGridCell
				? ( const_cast < CExtPropertyValue * > ( pPV ) ) -> ValueActiveGet()
				: ( const_cast < CExtPropertyValue * > ( pPV ) ) -> ValueDefaultGet();
		if( pCellSrc != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
			CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
			OnPgcQueryGrids( arrGrids );
			INT nGridIdx, nGridCount = INT( arrGrids.GetSize() );
			for( nGridIdx = 0; nGridIdx < nGridCount; nGridIdx ++ )
			{
				CExtPropertyGridWnd * pGrid = arrGrids[ nGridIdx ];
				__EXT_DEBUG_GRID_ASSERT_VALID( pGrid );
				HTREEITEM hTreeItem = pGrid->PropertyItemToTreeItem( pPV );
				if( hTreeItem == NULL )
					continue;
				CExtGridCell * pCellDst = pGrid->ItemGetCell( hTreeItem, 1 );
				if( pCellDst == NULL )
					continue;
				__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
				pCellDst->Assign( *pCellSrc );
				if( ! pGrid->IsWindowVisible() )
					continue;
				LONG nRowNo = pGrid->ItemGetVisibleIndexOf( hTreeItem );
				if( nRowNo < 0 )
					continue;
				CRect rc;
				if( pGrid->GridCellRectsGet(
							1,
							nRowNo,
							0,
							0,
							NULL,
							&rc
							)
					  )
				{
					CRect rcClient;
					pGrid->GetClientRect( &rcClient );
					rc.left = rcClient.left;
					rc.right = rcClient.right;
					pGrid->InvalidateRect( &rc );
					bInvalidated = true;
				}
			} // for( nGridIdx = 0; nGridIdx < nGridCount; nGridIdx ++ )
		} // if( pCellSrc != NULL )
	} // if( pPV != NULL )
	if( bUpdateActiveTreeGridWindow && bInvalidated )
	{
		CExtPropertyGridWnd * pPGW = GetActiveGrid();
		if( pPGW->GetSafeHwnd() != NULL )
			pPGW->UpdateWindow();
	} // if( bUpdateActiveTreeGridWindow && bInvalidated )
}

void CExtPropertyGridCtrl::PropertyStoreSynchronize()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	_EnsureInitialized();
CExtPropertyGridWnd * pActivePGW = GetActiveGrid();
	if( pActivePGW->GetSafeHwnd() != NULL )
	{
		HWND hWndInplaceEditor = pActivePGW->GetSafeInplaceActiveHwnd();
		if( hWndInplaceEditor != NULL )
		{
			::SendMessage( hWndInplaceEditor, WM_CANCELMODE, 0L, 0L );
			pActivePGW->SendMessage( WM_CANCELMODE );
		}
	}
	if( CExtGridCell::g_pCellMenuTracking != NULL )
	{
		CExtGridDataProvider * pDP = CExtGridCell::g_pCellMenuTracking->DataProviderGet();
		if( pDP != NULL )
		{
			CExtPropertyGridWnd * pActiveGrid = GetActiveGrid();
			if( pActiveGrid != NULL )
			{
				CExtGridDataProvider & _ActiveDP = pActiveGrid->OnGridQueryDataProvider();
				if( LPVOID(&_ActiveDP) == LPVOID(pDP->m_pOuterDataProvider) )
					CExtPopupMenuWnd::CancelMenuTracking();
			}
		}
	}
CExtPropertyStore * pCurrentPS = PropertyStoreGet();
CExtPropertyItem * pRestoreFocusPI = NULL;
	if( pCurrentPS != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pCurrentPS );
		if( pActivePGW->GetSafeHwnd() != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pActivePGW );
			HTREEITEM htiFocus = pActivePGW->ItemFocusGet();
			if( htiFocus != NULL )
			//	pRestoreFocusPI = pActivePGW->PropertyItemFromTreeItem( htiFocus );
				pRestoreFocusPI = pActivePGW->_PropertyItemFromTreeItem_Impl( htiFocus );
		}
	}
CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
	OnPgcQueryGrids( arrGrids );
INT nGridIdx = 0;
	for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
	{
		CExtPropertyGridWnd * pGrid = arrGrids[ nGridIdx ];
		__EXT_DEBUG_GRID_ASSERT_VALID( pGrid );
		pGrid->PropertyStoreSynchronizeAll();
	} // for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
	if( pRestoreFocusPI != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pActivePGW );
		__EXT_DEBUG_GRID_ASSERT( pActivePGW->GetSafeHwnd() != NULL );
	//	HTREEITEM htiSetFocus = pActivePGW->PropertyItemToTreeItem( pRestoreFocusPI );
		HTREEITEM htiSetFocus = pActivePGW->_PropertyItemToTreeItem_Impl( pRestoreFocusPI );
		if( htiSetFocus != NULL )
			pActivePGW->ItemFocusSet( htiSetFocus );
	}
}

CExtPropertyStore * CExtPropertyGridCtrl::PropertyStoreSet(
	CExtPropertyStore * pPS
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pPS != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPS );
	}
	if( pPS != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pPS );
	}
#endif // _DEBUG
CExtPropertyGridWnd * pPGW = GetActiveGrid();
	if( pPGW->GetSafeHwnd() != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
		pPGW->ItemFocusLock( true );
	}
CExtPropertyStore * pOldPS = m_pPS;
	m_pPS = pPS;
	PropertyStoreSynchronize();
	if( pPGW->GetSafeHwnd() != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pPGW );
		pPGW->ItemFocusLock( false );
		HTREEITEM hti = pPGW->ItemGetRoot();
		__EXT_DEBUG_GRID_ASSERT( hti != NULL );
		if( pPGW->ItemGetChildCount( hti ) > 0L )
		{
			hti = pPGW->ItemGetChildAt( hti, 0L );
			__EXT_DEBUG_GRID_ASSERT( hti != NULL );
			pPGW->ItemFocusSet( hti );
		}
	}
	return pOldPS;
}

CExtPropertyStore * CExtPropertyGridCtrl::PropertyStoreGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pPS != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPS );
	}
#endif // _DEBUG
	return m_pPS;
}

const CExtPropertyStore * CExtPropertyGridCtrl::PropertyStoreGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pPS != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pPS );
	}
#endif // _DEBUG
	return m_pPS;
}

CExtPropertyStore & CExtPropertyGridCtrl::PropertyStoreGetRef()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pPS ); // should be initialized
	return (*m_pPS);
}

const CExtPropertyStore & CExtPropertyGridCtrl::PropertyStoreGetRef() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pPS ); // should be initialized
	return (*m_pPS);
}

void CExtPropertyGridCtrl::PostNcDestroy()
{
	m_bInitialized = false;
	m_bHelperDirectCreateCall = false;
	__EXT_DEBUG_GRID_ASSERT( ! m_strCommandProfile.IsEmpty() );
	__EXT_DEBUG_GRID_VERIFY(
		g_CmdManager->ProfileDestroy(
			m_strCommandProfile,
			true
			)
		);
	m_bInConrolBarUpdate = m_bInPreTranslateMessage = false;
	m_bEnabledControlBarUpdate = true;
	CWnd::PostNcDestroy();
}

void CExtPropertyGridCtrl::PreSubclassWindow() 
{
	CWnd::PreSubclassWindow();
	__EXT_DEBUG_GRID_ASSERT( ! m_strCommandProfile.IsEmpty() );
	__EXT_DEBUG_GRID_VERIFY(
		g_CmdManager->ProfileSetup(
			m_strCommandProfile,
			m_hWnd
			)
		);
CExtCmdItem * pCmdItem =
		g_CmdManager->CmdAllocPtr(
			m_strCommandProfile,
			ID_EXT_PROPERTY_GRID_RESET
			);
	__EXT_DEBUG_GRID_VERIFY(
		g_ResourceManager->LoadString(
			pCmdItem->m_sMenuText,
			pCmdItem->m_nCmdID
			)
		);
	if( pCmdItem->m_sMenuText.IsEmpty() )
		pCmdItem->m_sMenuText = _T("Reset");
	__EXT_DEBUG_GRID_ASSERT( pCmdItem != NULL );

	if( ! m_bHelperDirectCreateCall )
		PostMessage(
			CExtPropertyGridCtrl::g_nMsgInitPropertyGridCtrl
			);
}

LRESULT CExtPropertyGridCtrl::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if(		message == WM_PRINT
		||	message == WM_PRINTCLIENT
		)
	{
		CDC * pDC = CDC::FromHandle( (HDC) wParam );
		if( (lParam&PRF_CHILDREN) != 0 )
			CExtPaintManager::stat_PrintChildren(
				m_hWnd,
				message,
				pDC->GetSafeHdc(),
				lParam,
				false
				);
		return (!0);
	}	

	if( message == WM_DESTROY )
		m_bEnabledControlBarUpdate = false;

bool bUpdateBarsOnThisMsg = false;
	if( m_bEnabledControlBarUpdate )
	{
		CWinApp * pApp = AfxGetApp();
			__EXT_DEBUG_GRID_ASSERT( pApp != NULL );
		BOOL bIdleMsg = pApp->IsIdleMessage(
#if _MFC_VER < 0x700
			&pApp->m_msgCur
#else
			&(::AfxGetThreadState()->m_msgCur)
#endif
			);
		if( bIdleMsg )
			bUpdateBarsOnThisMsg = true;
	}

HWND hWndThis = m_hWnd;
LRESULT lResult = CWnd::WindowProc(message, wParam, lParam);

	if(		hWndThis == NULL
		||	( ! ::IsWindow(hWndThis) )
		)
		bUpdateBarsOnThisMsg = false;

	if(		bUpdateBarsOnThisMsg
		&&	( ! m_bInConrolBarUpdate )
		)
	{
		m_bInConrolBarUpdate = true;
		CExtControlBar::DoCustomModeUpdateControlBars( this );
		m_bInConrolBarUpdate = false;
	}

	return lResult;
}

BOOL CExtPropertyGridCtrl::PreTranslateMessage(MSG* pMsg) 
{
	if(		WM_KEYFIRST <= pMsg->message
		&&	pMsg->message <= WM_KEYLAST
		&&	GetSafeHwnd() != NULL
		&&	(GetStyle()&(WS_VISIBLE|WS_CHILD)) == (WS_VISIBLE|WS_CHILD)
		)
	{ // if child/visible dialog
		HWND hWndParent = ::GetParent( m_hWnd );
		CWnd * pWndParentPermanent = CWnd::FromHandlePermanent( hWndParent );
		if(		pWndParentPermanent != NULL
			&&	(	pWndParentPermanent->IsKindOf( RUNTIME_CLASS(CExtControlBar) )
#if (!defined __EXT_MFC_NO_TAB_CONTROLBARS)
				||	pWndParentPermanent->IsKindOf( RUNTIME_CLASS(CExtDynAutoHideSlider) )
#endif // (!defined __EXT_MFC_NO_TAB_CONTROLBARS)
				)
			&&	pWndParentPermanent->PreTranslateMessage(pMsg)
			)
			return TRUE;
	} // if child/visible dialog
	if( m_bInPreTranslateMessage )
		return FALSE;
	m_bInPreTranslateMessage = true;
	if(		( GetStyle() & WS_VISIBLE ) != 0
		&&	CExtControlBar::DoCustomModePreTranslateMessage( this, pMsg )
		)
	{
		m_bInPreTranslateMessage = false;
		return TRUE;
	}
	m_bInPreTranslateMessage = false;
	return CWnd::PreTranslateMessage( pMsg );
}

BOOL CExtPropertyGridCtrl::PreCreateWindow(CREATESTRUCT& cs) 
{
	if(		( ! RegisterPropertyGridCtrlClass() )
		||	( ! CWnd::PreCreateWindow( cs ) )
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return FALSE;
	}
	cs.lpszClass = __EXT_PROPERTY_GRID_CTRL_CLASS_NAME;
	return TRUE;
}

void CExtPropertyGridCtrl::OnDestroy() 
{
	CWnd::OnDestroy();
}

void CExtPropertyGridCtrl::OnContextMenu(CWnd* pWnd, CPoint point)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	OnPgcContextMenuTrack(
		pWnd,
		point,
		NULL,
		NULL,
		NULL
		);
}

void CExtPropertyGridCtrl::OnPgcSynchronizeCompoundValue(
	CExtPropertyValueCompound * pCompoundValue
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCompoundValue );
CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
	OnPgcQueryGrids( arrGrids );
INT nGridIdx = 0;
	for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
	{
		CExtPropertyGridWnd * pGrid = arrGrids[ nGridIdx ];
		__EXT_DEBUG_GRID_ASSERT_VALID( pGrid );
		pGrid->OnPgwSynchronizeCompoundValue(
			pCompoundValue
			);
	} // for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
}

void CExtPropertyGridCtrl::OnPgcSynchronizeCompoundValue(
	CExtPropertyValueMixedCompound * pMixedCompound
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pMixedCompound );
CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
	OnPgcQueryGrids( arrGrids );
INT nGridIdx = 0;
	for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
	{
		CExtPropertyGridWnd * pGrid = arrGrids[ nGridIdx ];
		__EXT_DEBUG_GRID_ASSERT_VALID( pGrid );
		pGrid->OnPgwSynchronizeCompoundValue(
			pMixedCompound
			);
	} // for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
}

void CExtPropertyGridCtrl::OnPgcContextMenuTrack(
	CWnd * pWndHit,
	CPoint ptScreen,
	CExtGridHitTestInfo * pHtInfo, // can be NULL
	CExtPropertyItem * pPropertyItem, // can be NULL
	CExtPropertyGridWnd * pPGW // can be NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
	pWndHit;
	if( pPropertyItem == NULL )
		return;
	if( ! pPropertyItem->OnInputEnabledGet( ( pPGW != NULL ) ? pPGW->m_bInputEnabledNestedFlags : false ) )
		return;
CExtPopupMenuWnd * pPopup =
		CExtPopupMenuWnd::InstantiatePopupMenu(
			m_hWnd,
			RUNTIME_CLASS(CExtPopupMenuWnd),
			this
			);
	if( ! pPopup->CreatePopupMenu( m_hWnd ) )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		delete pPopup;
		return;
	}
HWND hWndResetCmd = m_hWnd;
	if(		pHtInfo != NULL
		&&	pPropertyItem != NULL
		&&	pPropertyItem->IsKindOf( RUNTIME_CLASS(CExtPropertyValue) )
		&&	pPGW->GetSafeHwnd() != NULL
		&&	pPropertyItem->IsModified()
		)
		hWndResetCmd = pPGW->m_hWnd;
	if( m_bEnableResetCommand )
	{
		__EXT_DEBUG_GRID_VERIFY(
			pPopup->ItemInsertCommand(
				ID_EXT_PROPERTY_GRID_RESET,
				-1,
				g_CmdManager->CmdGetPtr(
					m_strCommandProfile,
					ID_EXT_PROPERTY_GRID_RESET
					) -> m_sMenuText,
				NULL,
				(HICON)NULL,
				false,
				0,
				hWndResetCmd
				)
			);
		CExtPopupMenuWnd::MENUITEMDATA & _mii =
			pPopup->ItemGetInfo( pPopup->ItemGetCount() - 1 );
		_mii.Enable( ( hWndResetCmd == m_hWnd ) ? false : true );
	} // if( m_bEnableResetCommand )
	if(	! OnPgcContextMenuReconstruct(
			pPopup,
			pWndHit,
			ptScreen,
			pHtInfo,
			pPropertyItem,
			pPGW
			)
		)
	{
		delete pPopup;
		return;
	}
	if( pPopup->ItemGetCount() == 0 )
	{
		delete pPopup;
		return;
	}
	if( ! pPopup->TrackPopupMenu(
			0,
			ptScreen.x,
			ptScreen.y,
			NULL,
			this,
			NULL,
			NULL,
			true
			)
		)
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		//delete pPopup;
		return;
	}
}

bool CExtPropertyGridCtrl::OnPgcContextMenuReconstruct(
	CExtPopupMenuWnd * pPopup,
	CWnd * pWndHit,
	CPoint ptScreen,
	CExtGridHitTestInfo * pHtInfo, // can be NULL
	CExtPropertyItem * pPropertyItem, // can be NULL
	CExtPropertyGridWnd * pPGW // can be NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPopup );
	pPopup;
	pWndHit;
	ptScreen;
	pHtInfo;
	pPropertyItem;
	pPGW;
	return true;
}

void CExtPropertyGridCtrl::OnPgcPropertyInputEnabled(
	CExtPropertyItem * pPropertyItem,
	bool bInputEnabled,
	bool bInputEnabledNestedFlags
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyItem );
CExtPropertyStore * pPS = PropertyStoreGet();
	__EXT_DEBUG_GRID_ASSERT_VALID( pPS );
	pPropertyItem->OnInputEnabledSet( bInputEnabledNestedFlags, bInputEnabled );
	if( bInputEnabledNestedFlags )
	{
		CExtPropertyItem * pItem = pPropertyItem->ItemParentGet();
		for( ; pItem != NULL; pItem = pItem->ItemParentGet() )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pItem );
			if( bInputEnabled )
				pItem->OnInputEnabledSet( false, true );
			else
			{
				bool bAllChildrenInputDisabled = true;
				INT nItemIndex, nItemCount = pItem->ItemGetCount();
				for( nItemIndex = 0; nItemIndex < nItemCount; nItemIndex ++ )
				{
					CExtPropertyItem * pChildItem = pItem->ItemGetAt( nItemIndex );
					__EXT_DEBUG_GRID_ASSERT_VALID( pChildItem );
					if( pChildItem->OnInputEnabledGet( false ) )
					{
						bAllChildrenInputDisabled = false;
						break;
					} // if( pChildItem->OnInputEnabledGet( false ) )
				} // for( nItemIndex = 0; nItemIndex < nItemCount; nItemIndex ++ )
				pItem->OnInputEnabledSet( false, ! bAllChildrenInputDisabled );
			} // else from if( bInputEnabled )
		} // for( ; pItem != NULL; pItem = pItem->ItemParentGet() )
	} // if( bInputEnabledNestedFlags )
CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
	OnPgcQueryGrids( arrGrids );
INT nGridIdx, nGridCount = INT(arrGrids.GetSize());
	for( nGridIdx = 0; nGridIdx < nGridCount; nGridIdx ++ )
	{
		CExtPropertyGridWnd * pGrid = arrGrids[ nGridIdx ];
		__EXT_DEBUG_GRID_ASSERT_VALID( pGrid );
		pGrid->_OnSynchronizeInputEnabledCheckMark( pPS );
		if( ( pGrid->GetStyle() & WS_VISIBLE ) != 0 )
			pGrid->Invalidate();
	} // for( nGridIdx = 0; nGridIdx < nGridCount; nGridIdx ++ )
}

void CExtPropertyGridCtrl::OnPgcResetValue(
	CExtPropertyGridWnd * pPGW,
	CExtPropertyValue * pPropertyValue
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
	pPGW;
	__EXT_DEBUG_GRID_ASSERT_VALID( pPropertyValue );
	pPropertyValue->Reset();
CExtPropertyValueCompound * pCompoundValue =
		DYNAMIC_DOWNCAST(
			CExtPropertyValueCompound,
			pPropertyValue
			);
	if( pCompoundValue != NULL )
		pCompoundValue->SynchronizeWithControl(
			this,
			true,
			true
			);
CExtPropertyValueMixedCompound * pMixedCompound =
		DYNAMIC_DOWNCAST(
			CExtPropertyValueMixedCompound,
			pPropertyValue
			);
	if( pMixedCompound != NULL )
	{
		pMixedCompound->SynchronizeWithControl(
			this,
			true,
			true
			);
		OnPgcSynchronizeCompoundValue( pMixedCompound );
	}
CExtGridCell * pCellSrc = pPropertyValue->ValueDefaultGet();
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
	OnPgcQueryGrids( arrGrids );
INT nGridIdx = 0;
	for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
	{
		CExtPropertyGridWnd * pGrid = arrGrids[ nGridIdx ];
		__EXT_DEBUG_GRID_ASSERT_VALID( pGrid );
		HTREEITEM hTreeItem =
			pGrid->PropertyItemToTreeItem(
				pPropertyValue
				);
		if( hTreeItem == NULL )
			continue;
		CExtGridCell * pCellDst =
			pGrid->ItemGetCell( hTreeItem, 1 );
		if( pCellDst == NULL )
			continue;
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
		pCellDst->Assign( *pCellSrc );
		pCellDst->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		if(		pPropertyValue->IsKindOf( RUNTIME_CLASS(CExtPropertyValueMixed) )
			&&	pPropertyValue->ChildrenHaveDifferentActiveValues()
			)
			pCellDst->ModifyStyleEx( __EGCS_EX_UNDEFINED_ROLE );
		else
			pCellDst->ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );

		if( pGrid->IsWindowVisible() )
			pGrid->Invalidate();

		CExtPropertyItem * pOwnerItem = 
			pPropertyValue->ItemParentGet();
		if( pOwnerItem != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pOwnerItem );
			CExtPropertyValueCompound * pCompoundValue =
				DYNAMIC_DOWNCAST(
					CExtPropertyValueCompound,
					pOwnerItem
					);
			for( ; pCompoundValue != NULL; )
			{
				HTREEITEM hTreeItem =
					pGrid->PropertyItemToTreeItem(
						pCompoundValue
						);
				if( hTreeItem == NULL )
					continue;
				CExtGridCell * pCellDst =
					pGrid->ItemGetCell( hTreeItem, 1 );
				if( pCellDst == NULL )
					continue;
				__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
				CExtGridCell * pCellActive = pCompoundValue->ValueActiveGet();
				__EXT_DEBUG_GRID_ASSERT_VALID( pCellActive );
				CExtSafeString strCompound;
				strCompound = pCompoundValue->BuildCompoundTextActive();
				pCellDst->TextSet(
					strCompound.IsEmpty() ? _T("") : LPCTSTR(strCompound)
					);
				pCellActive->TextSet(
					strCompound.IsEmpty() ? _T("") : LPCTSTR(strCompound)
					);
				pCellDst->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
				if( pGrid->IsWindowVisible() )
					pGrid->Invalidate();
				CExtPropertyItem * pOwnerItem = 
					pCompoundValue->ItemParentGet();
				if( pOwnerItem == NULL )
					break;
				__EXT_DEBUG_GRID_ASSERT_VALID( pOwnerItem );
				pCompoundValue =
					DYNAMIC_DOWNCAST(
						CExtPropertyValueCompound,
						pOwnerItem
						);
			} // for( ; pCompoundValue != NULL; )

			CExtPropertyValueMixedCompound * pMixedCompound =
				DYNAMIC_DOWNCAST(
					CExtPropertyValueMixedCompound,
					pOwnerItem
					);
			for( ; pMixedCompound != NULL; )
			{
				HTREEITEM hTreeItem =
					pGrid->PropertyItemToTreeItem(
						pMixedCompound
						);
				if( hTreeItem == NULL )
					continue;
				CExtGridCell * pCellDst =
					pGrid->ItemGetCell( hTreeItem, 1 );
				if( pCellDst == NULL )
					continue;
				__EXT_DEBUG_GRID_ASSERT_VALID( pCellDst );
				CExtGridCell * pCellActive = pMixedCompound->ValueActiveGet();
				__EXT_DEBUG_GRID_ASSERT_VALID( pCellActive );
				CExtSafeString strCompound;
				strCompound = pMixedCompound->BuildCompoundTextActive();
				pCellDst->TextSet(
					strCompound.IsEmpty() ? _T("") : LPCTSTR(strCompound)
					);
				pCellActive->TextSet(
					strCompound.IsEmpty() ? _T("") : LPCTSTR(strCompound)
					);
				pCellDst->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
				if( pMixedCompound->ChildrenHaveDifferentActiveValues() )
					pCellDst->ModifyStyleEx( __EGCS_EX_UNDEFINED_ROLE );
				else
					pCellDst->ModifyStyleEx( 0, __EGCS_EX_UNDEFINED_ROLE );
				if( pGrid->IsWindowVisible() )
					pGrid->Invalidate();
				CExtPropertyItem * pOwnerItem = 
					pMixedCompound->ItemParentGet();
				if( pOwnerItem == NULL )
					break;
				__EXT_DEBUG_GRID_ASSERT_VALID( pOwnerItem );
				pMixedCompound =
					DYNAMIC_DOWNCAST(
						CExtPropertyValueMixedCompound,
						pOwnerItem
						);
			} // for( ; pMixedCompound != NULL; )
		} // if( pOwnerItem != NULL )
	} // for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
}

#endif // (!defined __EXT_MFC_NO_PROPERTYGRIDWND)





