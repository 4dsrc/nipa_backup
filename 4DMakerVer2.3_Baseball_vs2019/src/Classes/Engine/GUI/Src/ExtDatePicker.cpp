// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_DATE_PICKER) || (!defined __EXT_MFC_NO_DATE_BROWSER)

#if (!defined __EXT_DATE_PICKER_H)
	#include <ExtDatePicker.h>
#endif // (!defined __EXT_DATE_PICKER_H)

#if (!defined __EXT_RICH_CONTENT_H)
	#include <ExtRichContent.h>
#endif // (!defined __EXT_RICH_CONTENT_H)

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#if (!defined __ExtCmdManager_H)
	#include <ExtCmdManager.h>
#endif

#if (!defined __EXT_MEMORY_DC_H)
	#include <../Src/ExtMemoryDC.h>
#endif

#if (!defined __EXT_POPUP_CTRL_MENU_H)
	#include <ExtPopupCtrlMenu.h>
#endif

#include <math.h>

#include <Resources/Resource.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#if (!defined __EXT_MFC_NO_DATE_PICKER)

//////////////////////////////////////////////////////////////////////////
// CExtDatePickerHeaderPopupWnd
//////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC( CExtDatePickerHeaderPopupWnd, CWnd );

CExtDatePickerHeaderPopupWnd::CExtDatePickerHeaderPopupWnd(
	CExtDatePickerWnd * pDPW,
	bool bUseShadow
	)
	: m_pDPW( pDPW )
	, m_nMonth( 0 )
	, m_nYear( 0 )
	, m_nItemsPerPage( 7 )
	, m_nSelMonth( 0 )
	, m_nSelYear( 0 )
	, m_sizeDesiredSize( CSize( 0, 0 ) )
	, m_sizeItemSize( CSize( 0, 0 ) )
	, m_bUseShadow( bUseShadow )
	, m_bScrollingUp( false )
	, m_bScrollingDown( false )
	, m_nLastElapseTimerUp( 0 )
	, m_nLastElapseTimerDown( 0 )
{
	ASSERT( pDPW != NULL );
	_CalcSize();
}

CExtDatePickerHeaderPopupWnd::~CExtDatePickerHeaderPopupWnd()
{
}

BOOL CExtDatePickerHeaderPopupWnd::PreCreateWindow(CREATESTRUCT& cs) 
{
	if( ! CWnd::PreCreateWindow(cs) )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	cs.cx = m_sizeDesiredSize.cx;
	cs.cy = m_sizeDesiredSize.cy;
	return TRUE;
}

void CExtDatePickerHeaderPopupWnd::PostNcDestroy()
{
	delete this;
}

void CExtDatePickerHeaderPopupWnd::SetInitialDate( INT nMonth, INT nYear )
{
	ASSERT_VALID( this );
	ASSERT(
			nYear >= __EXT_DATE_YEAR_MIN
		&&	nYear <= __EXT_DATE_YEAR_MAX
		&&	nMonth >= 1
		&&	nMonth <= 12
		);
	if( nMonth >= 1 && nMonth <= 12 )
		m_nMonth = nMonth;
	if( nYear >= __EXT_DATE_YEAR_MIN && nYear <= __EXT_DATE_YEAR_MAX )
		m_nYear = nYear;
}

bool CExtDatePickerHeaderPopupWnd::GetSelectedDate( INT & nMonth, INT & nYear ) const
{
	ASSERT_VALID( this );
	ASSERT( m_hWnd != NULL && ::IsWindow(m_hWnd) );
	if( m_nSelMonth == 0 || m_nSelYear == 0 )
		return false;
	nMonth = m_nSelMonth;
	nYear = m_nSelYear;
	return true;
}

void CExtDatePickerHeaderPopupWnd::SetItemsPerPage( INT nItemsPerPage )
{
	ASSERT_VALID( this );
	ASSERT(
			nItemsPerPage >= 3
		&&	fmod( double(nItemsPerPage), 2.0 ) != 0.0
		);
	if(		nItemsPerPage >= 3
		&&	fmod( double(nItemsPerPage), 2.0 ) != 0.0
		)
		m_nItemsPerPage = nItemsPerPage;
}

CSize CExtDatePickerHeaderPopupWnd::GetSize(
	bool bOnlyClientArea // = false
	) const
{
	ASSERT_VALID( this );
bool bDynamicShadowAvailable = false;
	if( m_bUseShadow )
		bDynamicShadowAvailable = m_ctrlShadow.IsAvailable();
CSize _size(
		m_sizeDesiredSize.cx -
			(	( m_bUseShadow && bOnlyClientArea && (! bDynamicShadowAvailable ) )
					? CExtWndShadow::DEF_SHADOW_SIZE
					: 0
			),
		m_sizeDesiredSize.cy -
			(	( m_bUseShadow && bOnlyClientArea && (! bDynamicShadowAvailable ) )
					? CExtWndShadow::DEF_SHADOW_SIZE
					: 0
			)
		);
	return _size;
}

UINT CExtDatePickerHeaderPopupWnd::GetScrollingSpeed( int nPixels )
{
	ASSERT_VALID( this );
double dAcceleration = floor( double(nPixels)/double(__EDPW_SCROLL_MONTH_LIST_ACCELERATION_STEP) );
INT nAcceleration = (INT)dAcceleration;
UINT nElapse = 0;
	switch( nAcceleration ) 
	{
	case 0:  nElapse = __EDPW_SCROLL_MONTH_LIST_TIMER_ELAPSE1; break;
	case 1:  nElapse = __EDPW_SCROLL_MONTH_LIST_TIMER_ELAPSE2; break;
	case 2:  nElapse = __EDPW_SCROLL_MONTH_LIST_TIMER_ELAPSE3; break;
	case 3:  nElapse = __EDPW_SCROLL_MONTH_LIST_TIMER_ELAPSE4; break;
	case 4:  nElapse = __EDPW_SCROLL_MONTH_LIST_TIMER_ELAPSE5; break;
	default: nElapse = __EDPW_SCROLL_MONTH_LIST_TIMER_ELAPSE5; break;
	}
	return nElapse;
}

void CExtDatePickerHeaderPopupWnd::AnalyzeChangings()
{
	ASSERT_VALID( this );
	ASSERT( m_hWnd != NULL && ::IsWindow(m_hWnd) );
POINT point;
	if( ! ::GetCursorPos(&point) )
		return;
	ScreenToClient( &point );
	if( point.y > 0 && point.y < m_sizeDesiredSize.cy )
	{
		if( m_bScrollingDown )
		{
			KillTimer( __EDPW_SCROLL_MONTH_LIST_DOWN_TIMER_ID );
			m_bScrollingDown = false;
		}
		if( m_bScrollingUp )
		{
			KillTimer( __EDPW_SCROLL_MONTH_LIST_UP_TIMER_ID );
			m_bScrollingUp = false;
		}
	} // if( point.y > 0 && point.y < m_sizeDesiredSize.cy )
INT nRow = _HitTest( point );
	m_nSelMonth = m_nSelYear = 0;
CSize sizeDesiredSize = GetSize( true );
	if( nRow > -1 )
	{
		INT nHalfCount = m_nItemsPerPage / 2;
		if( (nHalfCount * 2) > m_nItemsPerPage )
			nHalfCount--;
		INT nMonth = m_nMonth;
		INT nYear = m_nYear;
		INT i = 0;
		for( i = 0; i < nHalfCount; i++ )
		{
			nMonth--;
			if( nMonth < 1 )
			{
				nMonth = 12;
				nYear--;
			}
		} // for( i = 0; i < nHalfCount; i++ )
		
		for( i = 0; i < m_nItemsPerPage; i++ )
		{
			if(		nRow == i 
				&&	nYear >= __EXT_DATE_YEAR_MIN
				&&	nYear <= __EXT_DATE_YEAR_MAX
			)
			{
				m_nSelMonth = nMonth;
				m_nSelYear = nYear;
				break;
			}
			nMonth++;
			if( nMonth > 12 )
			{
				nMonth = 1;
				nYear++;
			}
		} // for( i = 0; i < m_nItemsPerPage; i++ )
	} // if( nRow > -1 )
	else if( !(point.y > 0 && point.y < sizeDesiredSize.cy) )
	{
		if( point.y < 0 )
		{
			UINT nElapse = GetScrollingSpeed( abs(0 - point.y) );
			bool bNeedToResetTimer =
				(m_nLastElapseTimerUp != (UINT)nElapse);
			if( (!m_bScrollingUp) || bNeedToResetTimer )
			{
//				TRACE1(" * * * nElapse = %d\n",nElapse);
				if( bNeedToResetTimer )
					KillTimer( __EDPW_SCROLL_MONTH_LIST_UP_TIMER_ID );
				SetTimer( 
					__EDPW_SCROLL_MONTH_LIST_UP_TIMER_ID, 
					nElapse, 
					NULL 
					);
				m_nLastElapseTimerUp = nElapse;
				m_bScrollingUp = true;
			} // if( (!m_bScrollingUp) || bNeedToResetTimer )
		} // if( point.y < 0 )
		if( point.y > 0 )
		{
			UINT nElapse = GetScrollingSpeed( abs( point.y - sizeDesiredSize.cy ) );
			bool bNeedToResetTimer = (m_nLastElapseTimerDown != (UINT)nElapse);
			if( (!m_bScrollingDown) || bNeedToResetTimer )
			{
//				TRACE1(" * * * nElapse = %d\n",nElapse);
				if( bNeedToResetTimer )
					KillTimer( __EDPW_SCROLL_MONTH_LIST_UP_TIMER_ID );
				SetTimer( 
					__EDPW_SCROLL_MONTH_LIST_DOWN_TIMER_ID, 
					nElapse, 
					NULL 
					);
				m_nLastElapseTimerDown = nElapse;
				m_bScrollingDown = true;
			} // if( (!m_bScrollingDown) || bNeedToResetTimer )
		} // if( point.y > 0 m_bScrollingDown)
	} // else if( !(point.y > 0 && point.y < sizeDesiredSize.cy) )
	Invalidate( FALSE );
}

void CExtDatePickerHeaderPopupWnd::_CalcSize()
{
	ASSERT_VALID( this );
	ASSERT( m_pDPW != NULL );
	// determine the maximal width and height 
CWindowDC dc( NULL );
	for( INT nMonth = 1; nMonth <= 12; nMonth++ )
	{
		CExtSafeString sMonthName =
			m_pDPW->OnDatePickerGetMonthName( nMonth );
		sMonthName += _T(" 0000");
		CFont font;
		m_pDPW->OnDatePickerQueryHeaderPopupFont( &font );
		ASSERT( font.GetSafeHandle() != NULL );
		CRect rcText = 
			CExtPaintManager::stat_CalcTextDimension(
			dc,
			font,
			sMonthName
			);
		if( m_sizeItemSize.cx < rcText.Width() )
			m_sizeItemSize.cx = rcText.Width();
		if( m_sizeItemSize.cy < rcText.Height() )
			m_sizeItemSize.cy = rcText.Height();
	} // for( INT nMonth = 1; nMonth <= 12; nMonth++ )
	m_sizeItemSize.cy += 2;
	m_sizeItemSize.cx += 2*12;
bool bDynamicShadowAvailable = false;
	if( m_bUseShadow )
		bDynamicShadowAvailable = m_ctrlShadow.IsAvailable();
	m_sizeDesiredSize =
		CSize(
			m_sizeItemSize.cx + 1
				+ ( ( m_bUseShadow && (! bDynamicShadowAvailable ) ) ? CExtWndShadow::DEF_SHADOW_SIZE : 0 ),
			m_sizeItemSize.cy*m_nItemsPerPage + 2
				+ ( ( m_bUseShadow && (! bDynamicShadowAvailable ) ) ? CExtWndShadow::DEF_SHADOW_SIZE : 0 )
			);
}

INT CExtDatePickerHeaderPopupWnd::_HitTest( CPoint pt )
{
	ASSERT_VALID( this );
	for( INT i = 0; i<m_nItemsPerPage; i++ )
	{
		CRect rcItem( 
			0, 
			i*m_sizeItemSize.cy, 
			m_sizeItemSize.cx, 
			i*m_sizeItemSize.cy + m_sizeItemSize.cy
			);
		if( rcItem.PtInRect( pt ) )
			return i;
	}
	return -1;
}

BEGIN_MESSAGE_MAP(CExtDatePickerHeaderPopupWnd, CWnd)
	//{{AFX_MSG_MAP(CExtDatePickerHeaderPopupWnd)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
END_MESSAGE_MAP()

void CExtDatePickerHeaderPopupWnd::OnPaint() 
{
	ASSERT_VALID( this );
	ASSERT( m_pDPW != NULL );
CPaintDC dcPaint( this );
CRect rcClient;
	GetClientRect( &rcClient );
	if( rcClient.IsRectEmpty() )
		return;
CExtMemoryDC dc(
		&dcPaint //,
//		&rcClient
		);
INT nHalfCount = m_nItemsPerPage / 2;
	if( (nHalfCount * 2) > m_nItemsPerPage )
		nHalfCount--;
INT nMonth = m_nMonth;
INT nYear = m_nYear;
INT i = 0;
	for( i = 0; i < nHalfCount; i++ )
	{
		nMonth--;
		if( nMonth < 1 )
		{
			nMonth = 12;
			nYear--;
		}
	} // for( i = 0; i < nHalfCount; i++ )
COLORREF clrBackColor = m_pDPW->PmBridge_GetPM()->GetColor( COLOR_WINDOW, this );
	dc.FillSolidRect( rcClient, clrBackColor );
INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
COLORREF clrText = m_pDPW->PmBridge_GetPM()->GetColor( COLOR_BTNTEXT, this );
COLORREF clrTextOld = dc.SetTextColor( clrText );
CFont font;
	m_pDPW->OnDatePickerQueryHeaderPopupFont( &font );
	ASSERT( font.GetSafeHandle() != NULL );
CFont * pOldFont = dc.SelectObject( &font );
	for( i = 0; i < m_nItemsPerPage; i++ )
	{
		if(		nYear >= __EXT_DATE_YEAR_MIN
			&&	nYear <= __EXT_DATE_YEAR_MAX
			)
		{
			CExtSafeString sText;
      //  W4 Change  Parameter 2 is really a CString and should be accepatable 05012013
#pragma warning(suppress: 6284)
			sText.Format(
				_T("%s %d"),
				(LPCTSTR)m_pDPW->OnDatePickerGetMonthName( nMonth ),
				nYear
				);
			CRect rcItem( 
				0, 
				i*m_sizeItemSize.cy, 
				m_sizeItemSize.cx, 
				i*m_sizeItemSize.cy + m_sizeItemSize.cy
				);
			CRect rcText( rcItem );
			rcText.OffsetRect( 0, -1 );
			if(		m_nSelMonth != 0
				&&	m_nSelYear != 0
				&&	m_nSelMonth == nMonth 
				&&	m_nSelYear == nYear
				)
			{
				dc.FillSolidRect( rcItem, RGB(0,0,0) );
				COLORREF clrTextOld = dc.SetTextColor( RGB(255,255,255) );
				CExtRichContentLayout::stat_DrawText(
					dc.m_hDC,
					LPCTSTR(sText), sText.GetLength(),
					rcText,
					DT_CENTER|DT_VCENTER|DT_SINGLELINE, 0
					);
				dc.SetTextColor( clrTextOld );
			}
			else
			{
				CExtRichContentLayout::stat_DrawText(
					dc.m_hDC,
					LPCTSTR(sText), sText.GetLength(),
					rcText,
					DT_CENTER|DT_VCENTER|DT_SINGLELINE, 0
					);
			}
		}
		nMonth++;
		if( nMonth > 12 )
		{
			nMonth = 1;
			nYear++;
		}
	} // for( i = 0; i < m_nItemsPerPage; i++ )
	dc.SelectObject( pOldFont );
	dc.SetTextColor( clrTextOld );
	dc.SetBkMode( nOldBkMode );
	m_pDPW->PmBridge_GetPM()->OnPaintSessionComplete( this );
}

void CExtDatePickerHeaderPopupWnd::OnTimer(__EXT_MFC_UINT_PTR nIDEvent) 
{
	switch( nIDEvent ) 
	{
	case __EDPW_SCROLL_MONTH_LIST_UP_TIMER_ID:
		m_nMonth--;
		if( m_nMonth < 1 )
		{
			if( m_nYear > __EXT_DATE_YEAR_MIN )
			{
				m_nMonth = 12;
				m_nYear--;
			}
			else
				m_nMonth = 1;
		}
		break;
	case __EDPW_SCROLL_MONTH_LIST_DOWN_TIMER_ID:
		m_nMonth++;
		if( m_nMonth > 12 )
		{
			if( m_nYear < __EXT_DATE_YEAR_MAX )
			{
				m_nMonth = 1;
				m_nYear++;
			}
			else
				m_nMonth = 12;
		}
		break;
	default:
		CWnd::OnTimer(nIDEvent);
	}
	Invalidate( FALSE );
}

LRESULT CExtDatePickerHeaderPopupWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if( message == WM_MOUSEACTIVATE )
	{
		if( (GetStyle() & WS_TABSTOP) != 0 )
			return MA_ACTIVATE;
		return MA_NOACTIVATE;
	}
	if( message == WM_ERASEBKGND )
		return 0L;
	if( message == WM_NCCALCSIZE )
	{
		bool bDynamicShadowAvailable = false;
		if( m_bUseShadow )
			bDynamicShadowAvailable = m_ctrlShadow.IsAvailable();
		NCCALCSIZE_PARAMS * pNCCSP =
			reinterpret_cast < NCCALCSIZE_PARAMS * > ( lParam );
		ASSERT( pNCCSP != NULL );
		CRect rcInBarWnd( pNCCSP->rgrc[0] );
		rcInBarWnd.DeflateRect(
			1,
			1,
			1 + ( ( m_bUseShadow && (! bDynamicShadowAvailable ) ) ? CExtWndShadow::DEF_SHADOW_SIZE : 0 ),
			1 + ( ( m_bUseShadow && (! bDynamicShadowAvailable ) ) ? CExtWndShadow::DEF_SHADOW_SIZE : 0 )
			);
		::CopyRect( &(pNCCSP->rgrc[0]), rcInBarWnd );
		return 0;
	}
	if( message == WM_NCPAINT )
	{
		CRect rcInBarWnd, rcInBarClient;
		GetWindowRect( &rcInBarWnd );
		GetClientRect( &rcInBarClient );
		ClientToScreen( &rcInBarClient );
		if( rcInBarWnd == rcInBarClient )
			return 0;
		CPoint ptDevOffset = -rcInBarWnd.TopLeft();
		rcInBarWnd.OffsetRect( ptDevOffset );
		rcInBarClient.OffsetRect( ptDevOffset );
		
		CWindowDC dc( this );
		ASSERT( dc.GetSafeHdc() != NULL );
		dc.ExcludeClipRect( &rcInBarClient );
		CRect rcBorder( rcInBarClient );
		rcBorder.InflateRect( 1, 1 );
		bool bDynamicShadowAvailable = false;
		if( m_bUseShadow )
			bDynamicShadowAvailable = m_ctrlShadow.IsAvailable();
		if( m_bUseShadow && (! bDynamicShadowAvailable ) )
			m_wndShadow.Paint( m_pDPW->PmBridge_GetPM(), dc, rcBorder );
		dc.FillSolidRect( &rcBorder, RGB(0,0,0) );
		
		return 0;
	}

	if(		message == WM_CREATE
		&&	m_bUseShadow
		&&	m_ctrlShadow.IsAvailable()
		)
		m_ctrlShadow.Create( m_hWnd, CExtWndShadow::DEF_SHADOW_SIZE );

LRESULT lResult = CWnd::WindowProc( message, wParam, lParam );
	return lResult;
}

/////////////////////////////////////////////////////////////////////////////
// CExtDatePickerWnd::MONTH_INFO
/////////////////////////////////////////////////////////////////////////////

CExtDatePickerWnd::MONTH_INFO::MONTH_INFO( 
	INT nRow, 
	INT nCol, 
	INT nMonth, 
	INT nYear, 
	CRect rcClient,
	CExtDatePickerWnd * pDPW
	)
	: m_pDPW( pDPW )
	, m_nRow( nRow )
	, m_nCol( nCol )
	, m_nMonth( nMonth )
	, m_nYear( nYear )
	, m_rcClient( rcClient )
	, m_rcScrollBtnBackward( 0, 0, 0, 0 )
	, m_rcScrollBtnForward( 0, 0, 0, 0 )
	, m_rcHeader( 0, 0, 0, 0 )
	, m_rcHeaderDate( 0, 0, 0, 0 )
	, m_rcHeaderWithoutBorders( 0, 0, 0, 0 )
	, m_rcDaysOfWeek( 0, 0, 0, 0 )
	, m_rcDaysCells( 0, 0, 0, 0 )
{
	ASSERT( m_pDPW != NULL );
DWORD dwDatePickerStyle = m_pDPW->GetDatePickerStyle();
INT nFirstDayOfWeek = m_pDPW->OnDatePickerGetFirstDayOfWeek();
	for( INT i = 0; i < 2; i++ )
	{
		// 1=Sunday, 2=Monday, and so on.
		nFirstDayOfWeek += 1;
		if( nFirstDayOfWeek > 7 ) 
			nFirstDayOfWeek = 1;
	}
INT nYearTmp = m_nYear;
	if( nYearTmp <= __EXT_DATE_YEAR_MIN )
		nYearTmp += 28;
	else if( nYearTmp >= __EXT_DATE_YEAR_MAX )
		nYearTmp -= 28;
CExtOleDateTime dtFirstDay =
		CExtOleDateTime( nYearTmp, m_nMonth, 1, 0, 0, 0 );
	ASSERT( dtFirstDay.GetStatus() == COleDateTime::valid );
	if( dtFirstDay.GetDayOfWeek() == nFirstDayOfWeek )
		dtFirstDay -= 7;
	else
	{
		while( dtFirstDay.GetDayOfWeek() != nFirstDayOfWeek )
			dtFirstDay -= 1;
	}

CExtOleDateTime dtStart = 
	CExtOleDateTime( m_nYear, m_nMonth, 1, 0, 0, 0 );

	for( INT nWeek = 0; nWeek < 6; nWeek ++ )
	{
		for( INT nDay = 0; nDay < 7; nDay ++ )
		{
			m_aDays[nWeek][nDay].SetDatePickerWnd( m_pDPW, this );
			CSize sizeCalendarDimensionsCurrent;
			m_pDPW->DimGet( 
				NULL,
				NULL,
				&sizeCalendarDimensionsCurrent
				);

			CExtOleDateTime dtFirstDayReal = dtFirstDay;
			if( m_nYear <= __EXT_DATE_YEAR_MIN )
				dtFirstDayReal.SetDateTime(
					dtFirstDay.GetYear() - 28,
					dtFirstDay.GetMonth(),
					dtFirstDay.GetDay(),
					0, 0, 0
					);
			else if( m_nYear >= __EXT_DATE_YEAR_MAX )
				dtFirstDayReal.SetDateTime(
					dtFirstDay.GetYear() + 28,
					dtFirstDay.GetMonth(),
					dtFirstDay.GetDay(),
					0, 0, 0
					);

			CExtOleDateTime dt;
			dt.SetStatus( COleDateTime::null );
			if( dtFirstDayReal.GetStatus() == COleDateTime::valid )
			{
				if(		(	dtStart.GetMonth() == dtFirstDayReal.GetMonth()
						&&	dtStart.GetYear() == dtFirstDayReal.GetYear()
						)
					||	(	sizeCalendarDimensionsCurrent.cy-1 == m_nRow 
						&&	sizeCalendarDimensionsCurrent.cx-1 == m_nCol 
						&&  dtStart < dtFirstDayReal
						)
					||	(	m_nRow == 0 
						&&  m_nCol == 0
						&&  dtStart > dtFirstDayReal
						)
					||	(dwDatePickerStyle & __EDPWS_HIDE_INNER_NON_MONTH_DAYS) == 0
					)
					dt = dtFirstDayReal;
			}
			m_aDays[nWeek][nDay].SetDate( dt );

			dtFirstDay += 1;
		} // for( INT nDay = 0; nDay < 7; nDay ++ )
	} // for( INT nWeek = 0; nWeek < 6; nWeek ++ )
	_RecalcLayout();
}

#ifdef _DEBUG
void CExtDatePickerWnd::MONTH_INFO::AssertValid() const
{
	CObject::AssertValid();
	ASSERT(
			m_nYear >= __EXT_DATE_YEAR_MIN
		&&	m_nYear <= __EXT_DATE_YEAR_MAX
		&&	m_nMonth >= 1
		&&	m_nMonth <= 12
		);
	ASSERT(
			m_pDPW != NULL
		//&& ::IsWindow(m_pDPW->m_hWnd)
		);
	ASSERT_VALID( m_pDPW );
}

void CExtDatePickerWnd::MONTH_INFO::Dump( CDumpContext & dc) const
{
	CObject::Dump( dc );
}
#endif // _DEBUG

void CExtDatePickerWnd::MONTH_INFO::_RecalcLayout()
{
	m_rcScrollBtnBackward.SetRectEmpty();
	m_rcScrollBtnForward.SetRectEmpty();
	m_rcHeader.SetRectEmpty();
	m_rcHeaderDate.SetRectEmpty();
	m_rcHeaderWithoutBorders.SetRectEmpty();
	m_rcDaysCells.SetRectEmpty();
	m_rcDaysOfWeek.SetRectEmpty();
	// header's client area for displaying sel
	m_rcHeader.CopyRect( m_rcClient );
INT nMonthHeaderHeight = m_pDPW->OnDatePickerQueryMonthHeaderHeight();
	m_rcHeader.bottom = m_rcHeader.top + nMonthHeaderHeight;
	// header, but excluding border area
	m_rcHeaderWithoutBorders.CopyRect( m_rcHeader );
	m_rcHeaderWithoutBorders.DeflateRect( 1, 1 );
	// backward button
CSize sizeScrollButton = m_pDPW->OnDatePickerQueryScrollButtonSize();
	m_rcScrollBtnBackward.SetRect( 
		m_rcHeaderWithoutBorders.left,
		m_rcHeaderWithoutBorders.top,
		m_rcHeaderWithoutBorders.left + sizeScrollButton.cx,
		m_rcHeaderWithoutBorders.top + sizeScrollButton.cy
		);
	// forward button
	m_rcScrollBtnForward.SetRect( 
		m_rcHeaderWithoutBorders.right - sizeScrollButton.cx + 2*2,
		m_rcHeaderWithoutBorders.top,
		m_rcHeaderWithoutBorders.right,
		m_rcHeaderWithoutBorders.top + sizeScrollButton.cy
		);
	// header area for drawing selected date text
	m_rcHeaderDate.SetRect(
		m_rcHeaderWithoutBorders.left + sizeScrollButton.cx,
		m_rcHeaderWithoutBorders.top,
		m_rcHeaderWithoutBorders.right - sizeScrollButton.cx + 2*2,
		m_rcHeaderWithoutBorders.bottom
		);
	// day cells
//CRect rcBorderMetrics = m_pDPW->OnDatePickerGetBorderMetrics();
INT nIndentSpace = m_pDPW->OnDatePickerQueryIndentSpace();
	m_rcDaysCells.SetRect(
		m_rcClient.left + nIndentSpace,
		m_rcHeader.bottom,
		m_rcClient.right - nIndentSpace - 2,
		m_rcClient.bottom //- rcBorderMetrics.bottom
		);
	// days of week
	m_rcDaysOfWeek.CopyRect( &m_rcDaysCells );
INT nDaysOfWeekHeight = m_pDPW->OnDatePickerQueryDaysOfWeekHeight();
	m_rcDaysOfWeek.bottom = m_rcDaysOfWeek.top + nDaysOfWeekHeight;
	// days cells (continue...)
	m_rcDaysCells.top += m_rcDaysOfWeek.Height();
	// day's cells
CSize sizeDateCell = m_pDPW->OnDatePickerQueryDateCellSize();
	for( INT nWeek = 0; nWeek < 6; nWeek ++)
	{
		for( INT nDay = 0; nDay < 7; nDay ++ )
		{
			CRect rcCell( 
				m_rcDaysCells.left + nDay*sizeDateCell.cx,
				m_rcDaysCells.top + nWeek*sizeDateCell.cy, 
				m_rcDaysCells.left + nDay*sizeDateCell.cx + sizeDateCell.cx,
				m_rcDaysCells.top + nWeek*sizeDateCell.cy + sizeDateCell.cy
				);
			m_aDays[nWeek][nDay].SetRect( rcCell );
		} // for( INT nDay = 0; nDay < 7; nDay ++ )
	} // for( INT nWeek = 0; nWeek < 6; nWeek ++)
}

CRect CExtDatePickerWnd::MONTH_INFO::GetRect() const
{
	ASSERT_VALID( this );
	return m_rcClient;
}

CRect CExtDatePickerWnd::MONTH_INFO::GetHeaderRect() const
{
	ASSERT_VALID( this );
	return m_rcHeader;
}

void CExtDatePickerWnd::MONTH_INFO::GetMonth( INT & nMonth, INT & nYear ) const
{
	ASSERT_VALID( this );
	nMonth = m_nMonth;
	nYear = m_nYear;
}

COleDateTime CExtDatePickerWnd::MONTH_INFO::GetMonthInfoDT() const
{
	ASSERT_VALID( this );
COleDateTime dt( m_nYear, m_nMonth, 1, 1, 1, 1 );
	return dt;
}

const CExtDatePickerWnd::MONTH_INFO::DATE_INFO * CExtDatePickerWnd::MONTH_INFO::HitTestDay( const POINT & ptClient ) const
{
	for( INT nWeek = 0; nWeek < 6; nWeek ++ )
	{
		for( INT nDay = 0; nDay < 7; nDay ++ )
		{
			const CExtDatePickerWnd::MONTH_INFO::DATE_INFO * pDay = &(m_aDays[nWeek][nDay]);
			if( pDay->GetRect().PtInRect( ptClient ) )
			{
				return pDay;
			}
		} // for( INT nWeek = 0; nWeek < 6; nWeek ++ )
	} // for( INT nWeek = 0; nWeek < 6; nWeek ++ )
	return NULL;
}

LONG CExtDatePickerWnd::MONTH_INFO::HitTest(
	const POINT & ptClient,
	COleDateTime * pDT // = NULL
	) const
{
	ASSERT_VALID( this );
	if( pDT != NULL )
		pDT->SetStatus( COleDateTime::null );
	if( m_rcClient.IsRectEmpty()
		|| (! m_rcClient.PtInRect(ptClient) )
		)
		return __EDPWH_NOWHERE;
CSize sizeCalendarDimensionsCurrent;
	m_pDPW->DimGet( 
		NULL,
		NULL,
		&sizeCalendarDimensionsCurrent
		);
	if(		(!m_rcScrollBtnBackward.IsRectEmpty())
		&&	m_rcScrollBtnBackward.PtInRect(ptClient)
		&&	m_nRow == 0 
		&&  m_nCol == 0
		)
		return __EDPWH_BTN_BACKWARD;
	if(		(!m_rcScrollBtnForward.IsRectEmpty())
		&&	m_rcScrollBtnForward.PtInRect(ptClient)
		&&	m_nRow == 0 
		&&	(sizeCalendarDimensionsCurrent.cx - 1) == m_nCol 
		)
		return __EDPWH_BTN_FORWARD;
	if(		(!m_rcHeaderDate.IsRectEmpty())
		&&	m_rcHeaderDate.PtInRect(ptClient)
		)
	{
		if( pDT != NULL )
			(*pDT) = GetMonthInfoDT();
		return __EDPWH_HEADER_DATE;
	}
	if(		(!m_rcDaysOfWeek.IsRectEmpty())
		&&	m_rcDaysOfWeek.PtInRect(ptClient)
		)
	{
		if( pDT != NULL )
			(*pDT) = GetMonthInfoDT();
		return __EDPWH_DAYS_OF_WEEK;
	}
	if(		(!m_rcDaysCells.IsRectEmpty())
		&&	m_rcDaysCells.PtInRect(ptClient)
		)
	{
		if( pDT != NULL )
		{
			const DATE_INFO * pDayCell = HitTestDay( ptClient );
			if( pDayCell != NULL )
				(*pDT) = pDayCell->GetDate();
			else
				(*pDT) = GetMonthInfoDT();
		} // if( pDT != NULL )
		DATE_INFO * HitTestDay( const POINT & ptClient );
		return __EDPWH_DAYS_AREA;
	}
	return __EDPWH_NOWHERE;
}

void CExtDatePickerWnd::MONTH_INFO::Draw( CDC & dc ) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( m_pDPW != NULL );
	// draw header
	m_pDPW->OnDatePickerDrawHeader(
		m_nCol,
		m_nRow,
		dc,
		m_nYear,
		m_nMonth,
		m_rcHeader,
		m_rcHeaderWithoutBorders,
		m_rcHeaderDate,
		m_rcScrollBtnBackward,
		m_rcScrollBtnForward
		);
	// draw days of week
	m_pDPW->OnDatePickerDrawDaysOfWeek(
		m_nYear,
		m_nMonth,
		dc,
		m_rcDaysOfWeek
		);
	// draw day cells
DWORD dwDatePickerStyle = m_pDPW->GetDatePickerStyle();
	for( INT nWeek = 0; nWeek < 6; nWeek ++ )
	{
		for( INT nDay = 0; nDay < 7; nDay ++ )
		{
			m_aDays[nWeek][nDay].Draw( 
				dc,
				((dwDatePickerStyle & __EDPWS_SHOW_NON_MONTH_DAYS) != 0 ) ? true : false,
				((dwDatePickerStyle & __EDPWS_SHOW_TODAY_HIGHLIGHT) != 0 ) ? true : false
				);
		} // for( INT nDay = 0; nDay < 7; nDay ++ )
	} // for( INT nWeek = 0; nWeek < 6; nWeek ++ )
}

/////////////////////////////////////////////////////////////////////////////
// CExtDatePickerWnd::MONTH_INFO::DATE_INFO
/////////////////////////////////////////////////////////////////////////////

CExtDatePickerWnd::MONTH_INFO::DATE_INFO::DATE_INFO(
	CExtDatePickerWnd * pDPW // = NULL
	)
	: m_pDPW( pDPW )
	, m_pMI( NULL )
	, m_rcCell( 0, 0, 0, 0 )
{
	m_dt.SetStatus( COleDateTime::null );
}

CExtDatePickerWnd::MONTH_INFO::DATE_INFO::~DATE_INFO()
{
}

#ifdef _DEBUG

void CExtDatePickerWnd::MONTH_INFO::DATE_INFO::AssertValid() const
{
	CObject::AssertValid();
	ASSERT(	m_dt.GetStatus() == COleDateTime::null
		||	(		m_dt.GetStatus() == COleDateTime::valid 
				&&	m_dt.GetHour() == 0
				&&	m_dt.GetMinute() == 0
				&&	m_dt.GetSecond() == 0
			)
		);
	ASSERT(
			m_pDPW != NULL
		//&& ::IsWindow( m_pDPW->m_hWnd )
		);
	ASSERT_VALID( m_pDPW );
	ASSERT_VALID( m_pMI );
}

void CExtDatePickerWnd::MONTH_INFO::DATE_INFO::Dump( CDumpContext & dc ) const
{
	CObject::Dump( dc );
}
#endif //_DEBUG

void CExtDatePickerWnd::MONTH_INFO::DATE_INFO::Clear()
{
	ASSERT_VALID( this );
	m_dt.SetStatus( COleDateTime::null );
	m_rcCell.SetRect( 0, 0, 0, 0 );
}

void CExtDatePickerWnd::MONTH_INFO::DATE_INFO::SetDatePickerWnd( 
	CExtDatePickerWnd * pDPW,
	CExtDatePickerWnd::MONTH_INFO * pMI
	)
{
	ASSERT(
			pDPW != NULL
		//&& ::IsWindow(pDPW->m_hWnd)
		);
	ASSERT_VALID( pDPW );
	m_pDPW = pDPW;
	ASSERT_VALID( pMI );
	m_pMI = pMI;
}

void CExtDatePickerWnd::MONTH_INFO::DATE_INFO::SetRect( const RECT & rcCell )
{
	ASSERT_VALID( this );
	m_rcCell = rcCell;
}

CRect CExtDatePickerWnd::MONTH_INFO::DATE_INFO::GetRect() const
{
	ASSERT_VALID( this );
	return m_rcCell;
}

void CExtDatePickerWnd::MONTH_INFO::DATE_INFO::SetDate( const COleDateTime & dt )
{
	ASSERT_VALID( this );
	m_dt.m_dt = dt.m_dt;
	m_dt.m_status = dt.m_status;
}

COleDateTime CExtDatePickerWnd::MONTH_INFO::DATE_INFO::GetDate() const
{
	ASSERT_VALID( this );
	return m_dt;
}

void CExtDatePickerWnd::MONTH_INFO::DATE_INFO::Draw( 
	CDC & dc,
	bool bShowNonMonthDays, // = true
	bool bShowTodayHighlight // = true
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( COleDateTime::null == m_dt.GetStatus() )
		return;
INT nMonth = 0, nYear = 0;
	m_pMI->GetMonth( nMonth, nYear );
bool bNonMonthDay = 
		( m_dt.GetMonth() == nMonth )
			? false
			: true;
bool bDrawSelected = m_pDPW->SelectionGetForDate( m_dt );
CRect rcCell( m_rcCell );
bool bToday = false;
COleDateTime dtNow = m_pDPW->OnDatePickerGetToday();
	if(		m_dt.GetYear() == dtNow.GetYear() 
		&&	m_dt.GetMonth() == dtNow.GetMonth() 
		&&	m_dt.GetDay() == dtNow.GetDay() 				
		)
		bToday = true;
	m_pDPW->OnDatePickerDrawDateCell(
		dc,
		bShowNonMonthDays,
		bShowTodayHighlight,
		bNonMonthDay,
		bToday,
		rcCell,
		m_dt,
		bDrawSelected
		);
}

/////////////////////////////////////////////////////////////////////////////
// CExtDatePickerWnd
/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE( CExtDatePickerWnd, CWnd );

CExtDatePickerWnd::CExtDatePickerWnd()
	: m_bDirectCreateCall( false )
	, m_bCanceling( false )
	, m_bUpdatingLayout( false )
	, m_rcDatePickerClient( 0, 0, 0, 0 )
	, m_rcDatePickerInnerArea( 0, 0, 0, 0 )
	, m_rcBtnNone( 0, 0, 0, 0 )
	, m_rcBtnToday( 0, 0, 0, 0 )
	, m_dwDatePickerStyle( __EDPWS_DEFAULT )
	, m_eMTT( CExtDatePickerWnd::__EMTT_NOTHING )
	, m_sizeCalendarDimensionsMin( 1, 1 )
	, m_sizeCalendarDimensionsCurrent( 1, 1 )
	, m_sizeCalendarDimensionsMax( 1, 1 )
	, m_sizeMonthPicker( 0, 0 )
	, m_sizeDateCell( 0, 0 )
	, m_sizePushButton( 0, 0 )
	, m_sizeScrollButton( 0, 0 )
	, m_nMonthHeaderHeight( 0 )
	, m_nButtonAreaHeight( 0 )
	, m_nDaysOfWeekHeight( 0 )
	, m_nIndentSpace( 0 )
	, m_lParamCookie( 0 )
	, m_bAutoDeleteWindow( false )
	, m_hWndNotificationReceiver( NULL )
	, m_pExternalSelectionInfo( NULL )
	, m_hFont( NULL )
{ 
	VERIFY( RegisterDatePickerWndClass() );
	SelectionClear();
	CurrentDateSet( COleDateTime::GetCurrentTime() );

	PmBridge_Install();
}

CExtDatePickerWnd::~CExtDatePickerWnd()
{
	PmBridge_Uninstall();

CExtAnimationSite * pAcAS = AnimationClient_SiteGet();
	if( pAcAS != NULL )
		pAcAS->AnimationSite_ClientRemove( this );

	for( INT nMonth = 0; nMonth < m_arrMonths.GetSize(); nMonth++ )
	{
		MONTH_INFO * pMonth = m_arrMonths.GetAt(nMonth);
		ASSERT( pMonth );
		if( pMonth != NULL )
		{
			delete pMonth;
			pMonth = NULL;
		}
	}
}

BEGIN_MESSAGE_MAP(CExtDatePickerWnd, CWnd)
	//{{AFX_MSG_MAP(CExtDatePickerWnd)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_CANCELMODE()
	ON_WM_CAPTURECHANGED()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_GETDLGCODE()
	ON_WM_KEYDOWN()
	ON_WM_SIZE()
	ON_WM_KILLFOCUS()
	ON_WM_TIMECHANGE()
	ON_WM_ACTIVATEAPP()
	ON_WM_MOUSEACTIVATE()
	ON_MESSAGE(WM_SETFONT, OnSetFont)
	ON_MESSAGE(WM_GETFONT, OnGetFont)	
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
	ON_WM_SYSCOLORCHANGE()
	__EXT_MFC_SAFE_ON_WM_SETTINGCHANGE()
	ON_MESSAGE(__ExtMfc_WM_THEMECHANGED, OnThemeChanged)
END_MESSAGE_MAP()

#ifdef _DEBUG
void CExtDatePickerWnd::AssertValid() const
{
	CWnd::AssertValid();

	ASSERT( m_sizeCalendarDimensionsMin.cx >= 1 );
	ASSERT( m_sizeCalendarDimensionsMin.cy >= 1 );
	ASSERT( m_sizeCalendarDimensionsMax.cx >= 1 );
	ASSERT( m_sizeCalendarDimensionsMax.cy >= 1 );
	ASSERT( m_sizeCalendarDimensionsMax.cx >= m_sizeCalendarDimensionsMin.cx );
	ASSERT( m_sizeCalendarDimensionsMax.cy >= m_sizeCalendarDimensionsMin.cy );
	
	ASSERT(
			(	m_dtSelBegin.GetStatus() == COleDateTime::null
			&&	m_dtSelEnd.GetStatus() == COleDateTime::null
			)
		||	(	m_dtSelBegin.GetStatus() == COleDateTime::valid
			&&	m_dtSelEnd.GetStatus() == COleDateTime::valid
			)
		);
	ASSERT(		
			m_dtSelBegin.GetStatus() == COleDateTime::null
		||	(	m_dtSelBegin.GetStatus() == COleDateTime::valid 
			&&	m_dtSelBegin.GetHour() == 0
			&&	m_dtSelBegin.GetMinute() == 0
			&&	m_dtSelBegin.GetSecond() == 0
			)
		);
	ASSERT(		
			m_dtSelEnd.GetStatus() == COleDateTime::null
		||	(	m_dtSelEnd.GetStatus() == COleDateTime::valid 
			&&	m_dtSelEnd.GetHour() == 0
			&&	m_dtSelEnd.GetMinute() == 0
			&&	m_dtSelEnd.GetSecond() == 0
			)
		);
	ASSERT(		
			m_dtCurrentDate.GetStatus() == COleDateTime::null
		||	(	m_dtCurrentDate.GetStatus() == COleDateTime::valid 
			&&	m_dtCurrentDate.GetHour() == 0
			&&	m_dtCurrentDate.GetMinute() == 0
			&&	m_dtCurrentDate.GetSecond() == 0
			)
		);

	ASSERT(
			(	m_dtDisplayedBegin.GetStatus() == COleDateTime::null
			&&	m_dtDisplayedEnd.GetStatus() == COleDateTime::null
			)
		||	(	m_dtDisplayedBegin.GetStatus() == COleDateTime::valid
			&&	m_dtDisplayedEnd.GetStatus() == COleDateTime::valid
			)
		);
}

void CExtDatePickerWnd::Dump(CDumpContext& dc) const
{
	CWnd::Dump( dc );
}
#endif

CExtDatePickerWnd::SELECTION_NOTIFICATION::SELECTION_NOTIFICATION(
	CExtDatePickerWnd::SELECTION_INFO * pExternalSelectionInfo,
	HWND hWndDatePicker,
	LPARAM lParamCookie,
	bool bFinalSelectionChanging,
	const COleDateTime & dtBegin,
	const COleDateTime & dtEnd,
	bool bAllowFinalParentCancelMode // = true
	)
	: m_hWndDatePicker( hWndDatePicker )
	, m_bFinalSelectionChanging( bFinalSelectionChanging )
	, m_bAllowFinalParentCancelMode( bAllowFinalParentCancelMode )
	, m_pExternalSelectionInfo( pExternalSelectionInfo )
{
	m_lParamCookie = lParamCookie;
	m_dtBegin = dtBegin;
	m_dtEnd = dtEnd;
	ASSERT(
			m_hWndDatePicker != NULL
		&&	::IsWindow( m_hWndDatePicker )
		);
}

CExtDatePickerWnd::SELECTION_NOTIFICATION::SELECTION_NOTIFICATION(
	CExtDatePickerWnd::SELECTION_INFO * pExternalSelectionInfo,
	const CExtDatePickerWnd & wndDatePicker,
	bool bFinalSelectionChanging,
	bool bAllowFinalParentCancelMode // = true
	)
	: m_hWndDatePicker( wndDatePicker.GetSafeHwnd() )
	, m_bFinalSelectionChanging( bFinalSelectionChanging )
	, m_bAllowFinalParentCancelMode( bAllowFinalParentCancelMode )
	, m_pExternalSelectionInfo( pExternalSelectionInfo )
{
	m_lParamCookie = wndDatePicker.m_lParamCookie;
	ASSERT(
			m_hWndDatePicker != NULL
		&&	::IsWindow( m_hWndDatePicker )
		);
	wndDatePicker.SelectionGet( m_dtBegin, m_dtEnd );
}

CExtDatePickerWnd::SELECTION_NOTIFICATION::operator WPARAM() const
{
WPARAM wParam = reinterpret_cast < WPARAM > ( this );
	return wParam;
}

const CExtDatePickerWnd::SELECTION_NOTIFICATION *
	CExtDatePickerWnd::SELECTION_NOTIFICATION::FromWPARAM( WPARAM wParam )
{
CExtDatePickerWnd::SELECTION_NOTIFICATION * pSN =
		reinterpret_cast < CExtDatePickerWnd::SELECTION_NOTIFICATION * > ( wParam );
	ASSERT( pSN != NULL );
	ASSERT(
			pSN->m_hWndDatePicker != NULL
		&&	::IsWindow( pSN->m_hWndDatePicker )
		);
	return pSN;
}

void CExtDatePickerWnd::SELECTION_NOTIFICATION::Notify(
	HWND hWndNotify
	) const
{
	if(		m_bFinalSelectionChanging
		&&	m_pExternalSelectionInfo != NULL
		)
		(*m_pExternalSelectionInfo) = (*this);
	ASSERT(
			m_hWndDatePicker != NULL
		&&	::IsWindow( m_hWndDatePicker )
		&&	hWndNotify != NULL
		&&	::IsWindow( hWndNotify )
		);
	::SendMessage(
		hWndNotify,
		CExtDatePickerWnd::g_nMsgSelectionNotification,
		*this,
		m_lParamCookie
		);
	if( m_bFinalSelectionChanging && m_bAllowFinalParentCancelMode )
	{
		HWND hWndParent = ::GetParent( m_hWndDatePicker );
		if(		hWndParent != NULL
			&&	::IsWindow( hWndParent )
			)
			::SendMessage( hWndParent, WM_CANCELMODE, 0L, 0L );
	} // if( m_bFinalSelectionChanging && m_bAllowFinalParentCancelMode )
}

const UINT CExtDatePickerWnd::g_nMsgSelectionNotification =
	::RegisterWindowMessage(
		_T("CExtDatePickerWnd::g_nMsgSelectionNotification")
		);
bool CExtDatePickerWnd::g_bDatePickerWndClassRegistered = false;

bool CExtDatePickerWnd::RegisterDatePickerWndClass()
{
	if( g_bDatePickerWndClassRegistered )
		return true;
WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo( hInst, __EXT_DATE_PICKER_CLASS_NAME, &_wndClassInfo ) )
	{
		_wndClassInfo.style = CS_GLOBALCLASS|CS_DBLCLKS;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor = ::LoadCursor( NULL, IDC_ARROW );
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_DATE_PICKER_CLASS_NAME;
		if( ! ::AfxRegisterClass( &_wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}
	g_bDatePickerWndClassRegistered = true;
	return true;
}

bool CExtDatePickerWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
	UINT nDlgCtrlID, // = UINT( __EXT_MFC_IDC_STATIC )
	CSize sizeCalendarDimensionsMin, // = CSize(1,1),
	CSize sizeCalendarDimensionsMax, // = CSize(1,1),
	DWORD dwWindowStyle, // = WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN
	DWORD dwDatePickerStyle, // = __EDPWS_DEFAULT
	CCreateContext * pContext // = NULL
	)
{
	if( ! RegisterDatePickerWndClass() )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	m_bDirectCreateCall = true;
	m_dwDatePickerStyle = dwDatePickerStyle;
	if( ! CWnd::Create(
			__EXT_DATE_PICKER_CLASS_NAME,
			NULL,
			dwWindowStyle,
			rcWnd,
			pParentWnd,
			nDlgCtrlID,
			pContext
			)
		)
	{
		ASSERT( FALSE );
		return false;
	}
	DimSet(
		sizeCalendarDimensionsMin,
		sizeCalendarDimensionsMax
		);
	_RecalcMetrics();
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		return false;
	}
	return true;
}

void CExtDatePickerWnd::AdjustSize( const SIZE & sizeDim )
{
	ASSERT_VALID( this );
	ASSERT( sizeDim.cx >= 1 && sizeDim.cy >= 1 );
	if( GetSafeHwnd() == NULL )
		return;
CSize sizeContent = OnDatePickerCalcContentSize( sizeDim );
CRect rcWnd;
	GetWindowRect( &rcWnd );
	if( rcWnd.Size() == sizeContent )
		return;
	rcWnd.right = rcWnd.left + sizeContent.cx;
	rcWnd.bottom = rcWnd.top + sizeContent.cy;
	if( (GetStyle() & WS_CHILD) )
		GetParent()->ScreenToClient( &rcWnd );
	MoveWindow( &rcWnd, FALSE );
	UpdateDatePickerWnd( true, true, true );
}

void CExtDatePickerWnd::AdjustSize( INT nDimCols, INT nDimRows )
{
	ASSERT_VALID( this );
	ASSERT( nDimCols >= 1 && nDimRows >= 1 );
CSize sizeDim( nDimCols, nDimRows );
	AdjustSize( sizeDim );
}

bool CExtDatePickerWnd::_CreateHelper()
{
DWORD dwDatePickerStyle = GetDatePickerStyle();
	if( (dwDatePickerStyle & __EDPWS_BUTTON_ANY) != 0 )
	{
		INT nButtonAreaHeight = OnDatePickerQueryButtonAreaHeight();
		m_rcDatePickerClient.bottom += nButtonAreaHeight;
	}
	UpdateDatePickerWnd( true, true, false );

	AnimationSite_ClientProgressStop( this );
	AnimationClient_StateGet( false ).Empty();
	AnimationClient_StateGet( true ).Empty();
CRect rcClient;
	GetClientRect( &rcClient );
	AnimationClient_TargetRectSet( rcClient );

	return true;
}

void CExtDatePickerWnd::_RecalcLayout()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	m_rcDatePickerClient.SetRectEmpty();
	m_rcDatePickerInnerArea.SetRectEmpty();
	m_rcBtnNone.SetRectEmpty();
	m_rcBtnToday.SetRectEmpty();
DWORD dwDatePickerStyle = GetDatePickerStyle();
bool bBtnTodayVisible =
		((dwDatePickerStyle & __EDPWS_BUTTON_TODAY) != 0 )
			? true : false;
bool bBtnNoneVisible =
		((dwDatePickerStyle & __EDPWS_BUTTON_NONE) != 0 )
			? true : false;
CRect rcClient;
	GetClientRect( &rcClient );
CRect rcBorderMetrics = OnDatePickerGetBorderMetrics();
INT nButtonAreaHeight = OnDatePickerQueryButtonAreaHeight();
CSize sizeMonthPicker = OnDatePickerQueryMonthPickerSize();
	if( (dwDatePickerStyle & __EDPWS_AUTO_DIMENSION_MODE) != 0 )
	{
		CRect rcTmpClient(rcClient);
		rcTmpClient.DeflateRect( rcBorderMetrics );
		if( bBtnTodayVisible || bBtnNoneVisible )
			rcTmpClient.bottom -= nButtonAreaHeight;
		INT nDesiredCols =
			(INT)floor( double(rcTmpClient.Width())/double(sizeMonthPicker.cx) );
		INT nDesiredRows =
			(INT)floor( double(rcTmpClient.Height())/double(sizeMonthPicker.cy) );
		if( nDesiredCols > m_sizeCalendarDimensionsMax.cx )
			nDesiredCols = m_sizeCalendarDimensionsMax.cx;
		if( nDesiredRows > m_sizeCalendarDimensionsMax.cy )
			nDesiredRows = m_sizeCalendarDimensionsMax.cy;
		if( nDesiredCols < m_sizeCalendarDimensionsMin.cx )
			nDesiredCols = m_sizeCalendarDimensionsMin.cx;
		if( nDesiredRows < m_sizeCalendarDimensionsMin.cy )
			nDesiredRows = m_sizeCalendarDimensionsMin.cy;
		if(		m_sizeCalendarDimensionsCurrent.cx != nDesiredCols
			||	m_sizeCalendarDimensionsCurrent.cy != nDesiredRows
			)
		{
			m_sizeCalendarDimensionsCurrent.cx = nDesiredCols;
			m_sizeCalendarDimensionsCurrent.cy = nDesiredRows;
		}
	} // if( (dwDatePickerStyle & __EDPWS_AUTO_DIMENSION_MODE) != 0 )
	else
	{
		m_sizeCalendarDimensionsCurrent = m_sizeCalendarDimensionsMin;
	}
	
	// inner client area including border area
	m_rcDatePickerClient.CopyRect( &rcClient );
	m_rcDatePickerClient.right =
		m_sizeCalendarDimensionsCurrent.cx*sizeMonthPicker.cx
		+ rcBorderMetrics.left + rcBorderMetrics.right;
	m_rcDatePickerClient.bottom =
		m_sizeCalendarDimensionsCurrent.cy*sizeMonthPicker.cy
		+ rcBorderMetrics.top + rcBorderMetrics.bottom;

	// increase client area if the bottom buttons are visible
	INT nButtonsHeight = 
		( bBtnTodayVisible || bBtnNoneVisible ) ? nButtonAreaHeight : 0;
	
	// center calendar client area
	bool bCenterX = ( (dwDatePickerStyle & __EDPWS_CENTER_HORIZONTALLY) != 0 );
	bool bCenterY = ( (dwDatePickerStyle & __EDPWS_CENTER_VERTICALLY) != 0 );
	INT nOffsetX = (rcClient.Width() - m_rcDatePickerClient.Width() )/2;
	INT nOffsetY = (rcClient.Height() - m_rcDatePickerClient.Height() - nButtonsHeight )/2;
	m_rcDatePickerClient.OffsetRect( 
		(nOffsetX > 0 && bCenterX) ? nOffsetX : 0, 
		(nOffsetY > 0 && bCenterY) ? nOffsetY : 0
		);

	// inner client area excluding border area
	m_rcDatePickerInnerArea.CopyRect( &m_rcDatePickerClient );
	m_rcDatePickerInnerArea.DeflateRect( rcBorderMetrics );

	// increase client area if the bottom buttons are visible
	m_rcDatePickerClient.bottom += nButtonsHeight;
	
	// buttons area
	if( bBtnTodayVisible || bBtnNoneVisible )
	{
		CRect rcButtons( &m_rcDatePickerClient );
		rcButtons.top = m_rcDatePickerInnerArea.bottom + rcBorderMetrics.bottom;
		rcButtons.bottom -= rcBorderMetrics.bottom;
		CSize sizePushButton = OnDatePickerQueryPushButtonSize();

		// center buttons vertically
		INT nBtnTop =
			rcButtons.top
			+ (rcButtons.Height() - sizePushButton.cy)/2;
		INT nBtnBottom =
			rcButtons.top
			+ (rcButtons.Height() + sizePushButton.cy)/2;
		// center buttons horizontally
		INT nBtnLeftNone =
			rcButtons.left
			+ ( rcButtons.Width() - sizePushButton.cx)/2;
		INT nBtnLeftToday =
			rcButtons.left
			+ ( rcButtons.Width() - sizePushButton.cx)/2;
		
		if( bBtnTodayVisible )
			nBtnLeftNone += sizePushButton.cx/2 + 5;
		if( bBtnNoneVisible )
			nBtnLeftToday -= sizePushButton.cx/2 + 5;
			
		if( bBtnNoneVisible )
			m_rcBtnNone.SetRect( 
				nBtnLeftNone, 
				nBtnTop, 
				nBtnLeftNone + sizePushButton.cx, 
				nBtnBottom 
				);
		if( bBtnTodayVisible )
			m_rcBtnToday.SetRect( 
				nBtnLeftToday, 
				nBtnTop, 
				nBtnLeftToday + sizePushButton.cx, 
				nBtnBottom  
				);
	} // if( bBtnTodayVisible || bBtnNoneVisible )
}

void CExtDatePickerWnd::_RecalcCalendar()
{
	ASSERT_VALID( this );
INT nMonthsCount = (INT)m_arrMonths.GetSize();
INT nMonth = 0;
	for( nMonth = 0; nMonth < nMonthsCount; nMonth++ )
	{
		MONTH_INFO * pMonth = m_arrMonths.GetAt( nMonth );
		if( pMonth == NULL )
			continue;
		delete pMonth;
		pMonth = NULL;
	} // for( nMonth = 0; nMonth < nMonthsCount; nMonth++ )
	m_arrMonths.RemoveAll();
COleDateTime dtCurrentDate = CurrentDateGet();
INT nStartMonth = dtCurrentDate.GetMonth();
INT nStartYear = dtCurrentDate.GetYear();
	nMonthsCount =
		m_sizeCalendarDimensionsCurrent.cy
		* m_sizeCalendarDimensionsCurrent.cx;
	m_dtDisplayedBegin.SetStatus( COleDateTime::null );
	m_dtDisplayedEnd.SetStatus( COleDateTime::null );
CRect rcBorderMetrics = OnDatePickerGetBorderMetrics();
CSize sizeMonthPicker = OnDatePickerQueryMonthPickerSize();

INT nCheckMonth = nStartMonth;
INT nCheckYear = nStartYear;
	for( INT i = 0; i < nMonthsCount - 1; i++ )
	{
		nCheckMonth++;
		if( nCheckMonth > 12 )
		{
			nCheckMonth = 1;
			nCheckYear++;
		}
	}
	bool bNeedToChangeCurrentDate = false;
	while( nCheckYear > __EXT_DATE_YEAR_MAX )
	{
		nCheckMonth--;
		if( nCheckMonth < 1 )
		{
			nCheckMonth = 12;
			nCheckYear--;
		}
		nStartMonth--;
		if( nStartMonth < 1 )
		{
			nStartMonth = 12;
			nStartYear--;
		}
		bNeedToChangeCurrentDate = true;
	}
	if( bNeedToChangeCurrentDate )
		CurrentDateSet( nStartYear, nStartMonth, 1, false );

	for( nMonth = 0; nMonth < nMonthsCount; nMonth++ )
	{
		ldiv_t div_result;
		div_result = ldiv( nMonth, m_sizeCalendarDimensionsCurrent.cx );
		INT nRow = div_result.quot;
		INT nCol = div_result.rem;
		CRect rcMonth;
		rcMonth.SetRect(
			nCol*sizeMonthPicker.cx,
			nRow*sizeMonthPicker.cy,
			sizeMonthPicker.cx + nCol*sizeMonthPicker.cx,
			sizeMonthPicker.cy + nRow*sizeMonthPicker.cy
			);
		rcMonth.OffsetRect(
			rcBorderMetrics.left + m_rcDatePickerClient.left,
			rcBorderMetrics.top  + m_rcDatePickerClient.top
			);
		MONTH_INFO * pMonth = 
			new MONTH_INFO( 
				nRow, 
				nCol, 
				nStartMonth, 
				nStartYear, 
				rcMonth, 
				this 
			);
		m_arrMonths.Add( pMonth );
		if( m_dtDisplayedBegin.GetStatus() == COleDateTime::null )
			m_dtDisplayedBegin.SetDate( nStartYear, nStartMonth, 1 );
		if( nStartMonth == 12 )
		{
			if( nStartYear < __EXT_DATE_YEAR_MAX )
				m_dtDisplayedEnd.SetDate( nStartYear + 1, 1, 1 );
			else
				m_dtDisplayedEnd.SetDate( nStartYear, 12, 31 );
		}
		else
			m_dtDisplayedEnd.SetDate( nStartYear, nStartMonth + 1, 1 );
		nStartMonth += 1;
		if( nStartMonth > 12 )
		{
			nStartMonth = 1;
			nStartYear++;
		}
	} // for( nMonth = 0; nMonth < nMonthsCount; nMonth++ )
	
	if( m_dtDisplayedEnd.GetStatus() != COleDateTime::null )
		m_dtDisplayedEnd -= 1;
}

CExtDatePickerWnd::MONTH_INFO * CExtDatePickerWnd::_HitTestMonth( const POINT & ptClient ) const
{
	ASSERT_VALID( this );
	for( INT nMonth = 0; nMonth < m_arrMonths.GetSize(); nMonth++ )
	{
		MONTH_INFO * pMonth = m_arrMonths.GetAt( nMonth );
		if( pMonth == NULL )
			continue;
		if( pMonth->GetRect().PtInRect( ptClient ) )
			return pMonth;
	}
	return NULL;
}

const CExtDatePickerWnd::MONTH_INFO * CExtDatePickerWnd::_FindMonth( INT nRow, INT nCol ) const
{
	ASSERT_VALID( this );
	for( INT nMonth = 0; nMonth < m_arrMonths.GetSize(); nMonth++ )
	{
		const MONTH_INFO * pMonth = m_arrMonths.GetAt( nMonth );
		if( pMonth == NULL )
			continue;
		if( pMonth->m_nRow == nRow && pMonth->m_nCol == nCol )
			return pMonth;
	}
	return NULL; 
}
 
LRESULT CExtDatePickerWnd::OnSetFont( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
HFONT hFont = (HFONT) wParam;
BOOL bRedraw = (BOOL) lParam;
	m_hFont = hFont; 
	UpdateDatePickerWnd( true, true, bRedraw ? true : false );
	return 0L;
}
  
LRESULT CExtDatePickerWnd::OnGetFont( WPARAM, LPARAM )
{
	ASSERT_VALID( this );
    return (LRESULT) m_hFont;
}

BOOL CExtDatePickerWnd::OnEraseBkgnd(CDC* pDC) 
{
	pDC;
	return FALSE;
}

void CExtDatePickerWnd::OnPaint() 
{
	ASSERT_VALID( this );
CPaintDC dcPaint( this );
CRect rcClient;
	GetClientRect( &rcClient );
	if( rcClient.IsRectEmpty() )
		return;
CExtMemoryDC dc(
		&dcPaint //,
//		&rcClient
		);
	OnDatePickerDrawEntire( dc, rcClient );
	PmBridge_GetPM()->OnPaintSessionComplete( this );
}

LRESULT CExtDatePickerWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if( message == CExtPopupControlMenuWnd::g_nMsgControlInputRetranslate )
	{
		ASSERT( wParam != 0 );
		CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO & _ciri =
			*((CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO*)wParam);
		if( _ciri.m_eRTT == CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO::__ERTT_KEYBOARD )
		{
			OnKeyDown( _ciri.m_nChar, _ciri.m_nRepCnt, _ciri.m_nFlags );
			_ciri.m_bNoEat = false;
			return (!0);
		}
		return 0;
	}
	if( message == WM_PRINT || message == WM_PRINTCLIENT )
	{
		CDC * pDC = CDC::FromHandle( (HDC)wParam );
		CRect rcClient;
		GetClientRect( &rcClient );
		OnDatePickerDrawEntire( *pDC, rcClient );
		return (!0);
	}	
	if( message == WM_DISPLAYCHANGE )
	{
		_RecalcMetrics();
		LRESULT lRes = CWnd::WindowProc(message, wParam, lParam);
		UpdateDatePickerWnd( true, true, false );
		return lRes;
	}
	return CWnd::WindowProc(message, wParam, lParam);
}

void CExtDatePickerWnd::PreSubclassWindow() 
{
	CWnd::PreSubclassWindow();
	if( ! m_bDirectCreateCall )
	{
	ModifyStyle( 0, WS_CLIPCHILDREN );
	_RecalcMetrics();
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		AfxThrowMemoryException();
	}
	} // if( ! m_bDirectCreateCall )
	if( m_pExternalSelectionInfo != NULL )
	{
		SelectionSet(
			m_pExternalSelectionInfo->m_dtBegin,
			m_pExternalSelectionInfo->m_dtEnd
			);
		if( m_pExternalSelectionInfo->m_dtBegin.GetStatus() == COleDateTime::valid )
		{
			CurrentDateSet( m_pExternalSelectionInfo->m_dtBegin, true );
		}
		else if( m_pExternalSelectionInfo->m_dtEnd.GetStatus() == COleDateTime::valid )
		{
			CurrentDateSet( m_pExternalSelectionInfo->m_dtEnd, true );
		}
	} // if( m_pExternalSelectionInfo != NULL )
}

void CExtDatePickerWnd::PostNcDestroy()
{
	if( m_bAutoDeleteWindow )
		delete this;
}

DWORD CExtDatePickerWnd::GetDatePickerStyle() const
{
	ASSERT_VALID( this );
	return m_dwDatePickerStyle;
}

DWORD CExtDatePickerWnd::ModifyDatePickerStyle(
	DWORD dwRemove,
	DWORD dwAdd, // = 0
	bool bUpdateDatePickerWnd // = false
	)
{
	ASSERT_VALID( this );
DWORD dwOldStyle = m_dwDatePickerStyle;
	m_dwDatePickerStyle &= ~dwRemove;
	m_dwDatePickerStyle |= dwAdd;
	if( GetSafeHwnd() == NULL )
		return dwOldStyle;
bool bRecalcCalendar = false;
	if( (dwRemove & __EDPWS_AUTO_DIMENSION_MODE) != 0 )
	{
		m_sizeCalendarDimensionsCurrent = m_sizeCalendarDimensionsMin;
		bRecalcCalendar = true;
	}
	if( (dwRemove & __EDPWS_MULTIPLE_SELECTION) != 0 )
	{
		m_dtSelEnd = m_dtSelBegin;
	}
const DWORD dwRecalcStyles =
		__EDPWS_HIDE_INNER_NON_MONTH_DAYS
		| __EDPWS_BORDER_RESERVE
		| __EDPWS_BOLD_ANY;
	if(		(dwRemove & dwRecalcStyles) != 0
		||	(dwAdd & dwRecalcStyles) != 0
		)
		bRecalcCalendar = true;
	if( bRecalcCalendar )
		_RecalcMetrics();
	UpdateDatePickerWnd( true, bRecalcCalendar, bUpdateDatePickerWnd );
	return dwOldStyle;
}

void CExtDatePickerWnd::UpdateDatePickerWnd(
	bool bRecalcLayout, // = true,
	bool bRecalcCalendar, // = true,
	bool bUpdate // = true
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	if(		bRecalcLayout
		&&	(!m_bUpdatingLayout)
		)
	{
		m_bUpdatingLayout = true;
		_RecalcLayout();
		m_bUpdatingLayout = false;
	}
	
	if( bRecalcCalendar )
		_RecalcCalendar();

	if(		bUpdate
		&&	GetSafeHwnd() != NULL
		&&	(GetStyle() & WS_VISIBLE) != 0
		)
	{
		Invalidate();
		UpdateWindow();
	}
}

bool CExtDatePickerWnd::SelectionGetForDate( 
	const COleDateTime & dt 
	) const
{
	ASSERT_VALID( this );
	bool bReturn = false;
	if(		m_dtSelBegin.GetStatus() == COleDateTime::valid 
		&&	m_dtSelEnd.GetStatus() == COleDateTime::valid
		)
	{
		COleDateTime dtSelBegin, dtSelEnd;
		if( m_dtSelBegin > m_dtSelEnd )
		{
			dtSelBegin = m_dtSelEnd;
			dtSelEnd = m_dtSelBegin;
		}
		else
		{
			dtSelBegin = m_dtSelBegin;
			dtSelEnd = m_dtSelEnd;
		}

		if (dt >= dtSelBegin && dt <= dtSelEnd)
			bReturn = true;
	}
	return bReturn;
}

bool CExtDatePickerWnd::EnsureVisible(
	const COleDateTime & dt,
	bool bUpdateNow // = false
	)
{
	ASSERT_VALID( this );
CExtOleDateTime dtDate( dt );
	if( dtDate.GetStatus() != COleDateTime::valid )
		return false;
COleDateTime dtDisplayedBegin, dtDisplayedEnd;
	GetDisplayedRange( dtDisplayedBegin, dtDisplayedEnd );
	if(		dtDisplayedBegin.GetStatus() == COleDateTime::null
		||	dtDisplayedEnd.GetStatus() == COleDateTime::null
		)
	{
		CurrentDateSet( dt, bUpdateNow );
	}
	else if(
			dt < dtDisplayedBegin
		||	dtDisplayedEnd < dt 
		)
	{
		CurrentDateSet( dt, bUpdateNow );
	}
	return true;
}

void CExtDatePickerWnd::SelectionClear( 
	bool bUpdateNow // = false
	)
{
	ASSERT_VALID( this );
COleDateTime dtSel;
	dtSel.SetStatus( COleDateTime::null );
	SelectionSet(
		dtSel, 
		dtSel, 
		false,
		bUpdateNow
		);
}

void CExtDatePickerWnd::SelectionSet(
	const COleDateTime & dtSelBegin, 
	const COleDateTime & dtSelEnd, 
	bool bNormalize, // = false
	bool bUpdateNow // = false
	)
{
	ASSERT_VALID( this );
	if(		dtSelBegin.GetStatus() == m_dtSelBegin.GetStatus() 
		&&	dtSelEnd.GetStatus() == m_dtSelEnd.GetStatus() 
		&&	dtSelBegin.GetYear() == m_dtSelBegin.GetYear()
		&&	dtSelBegin.GetMonth() == m_dtSelBegin.GetMonth()
		&&	dtSelBegin.GetDay() == m_dtSelBegin.GetDay()
		&&	dtSelEnd.GetYear() == m_dtSelEnd.GetYear()
		&&	dtSelEnd.GetMonth() == m_dtSelEnd.GetMonth()
		&&	dtSelEnd.GetDay() == m_dtSelEnd.GetDay()
		)
		return;
	if(		dtSelBegin.GetStatus() == COleDateTime::null
		||	dtSelEnd.GetStatus() == COleDateTime::null
		)
	{
		m_dtSelBegin.SetStatus( COleDateTime::null );
		m_dtSelEnd.SetStatus( COleDateTime::null );
	}
	else
	{
		m_dtSelBegin.SetDate(
			dtSelBegin.GetYear(),
			dtSelBegin.GetMonth(),
			dtSelBegin.GetDay()
			);
		m_dtSelEnd.SetDate(
			dtSelEnd.GetYear(),
			dtSelEnd.GetMonth(),
			dtSelEnd.GetDay()
			);
		if( bNormalize && m_dtSelBegin > m_dtSelEnd )
		{
			CExtOleDateTime dtTemp = m_dtSelBegin;
			m_dtSelBegin = m_dtSelEnd;
			m_dtSelEnd = dtTemp;
		}
	}
	if( GetSafeHwnd() != NULL )
	{
		UpdateDatePickerWnd( true, false, bUpdateNow );
		HWND hWndNotificationReceiver =
			OnDatePickerGetNotificationReceiver();
		if( hWndNotificationReceiver != NULL )
		{
			SELECTION_NOTIFICATION _SN( m_pExternalSelectionInfo, *this, false );
			_SN.Notify( hWndNotificationReceiver );
		} // if( hWndNotificationReceiver != NULL )
	} // if( GetSafeHwnd() != NULL )
}

HWND CExtDatePickerWnd::OnDatePickerGetNotificationReceiver() const
{
	ASSERT_VALID( this );
	if(		m_hWndNotificationReceiver != NULL
		&&	::IsWindow( m_hWndNotificationReceiver )
		)
		return m_hWndNotificationReceiver;
	if( GetSafeHwnd() == NULL )
		return NULL;
HWND hWndNotificationReceiver = ::GetParent( m_hWnd );
	return hWndNotificationReceiver;
}

void CExtDatePickerWnd::SelectionGet(
	COleDateTime & dtSelBegin, 
	COleDateTime & dtSelEnd
	) const
{
	ASSERT_VALID( this );
	dtSelBegin = m_dtSelBegin;
	dtSelEnd = m_dtSelEnd;
}

void CExtDatePickerWnd::GetDisplayedRange(
	COleDateTime & dtBegin, 
	COleDateTime & dtEnd
	) const
{
	ASSERT_VALID( this );
	dtBegin = m_dtDisplayedBegin;
	dtEnd = m_dtDisplayedEnd;
}

void CExtDatePickerWnd::CurrentDateSet( 
	const COleDateTime & dtCurrentDate, 
	bool bUpdateNow // = false
	)
{
	ASSERT_VALID( this );
	if( dtCurrentDate.GetStatus() != COleDateTime::valid )
	{
		ASSERT( FALSE );
		return;
	}
	m_dtCurrentDate.SetDate(
		dtCurrentDate.GetYear(),
		dtCurrentDate.GetMonth(),
		dtCurrentDate.GetDay()
		);
	UpdateDatePickerWnd( true, true, bUpdateNow );
}

void CExtDatePickerWnd::CurrentDateSet( 
	INT nYear,
	INT nMonth,
	INT nDay, // = 1
	bool bUpdateNow // = false
	)
{
	ASSERT_VALID( this );
	ASSERT( nYear >= __EXT_DATE_YEAR_MIN
		&&	nYear <= __EXT_DATE_YEAR_MAX 
		);
COleDateTime dtCurrentDate;
	dtCurrentDate.SetDate(
		nYear,
		nMonth,
		nDay
		);
	CurrentDateSet( dtCurrentDate, bUpdateNow );
}

COleDateTime CExtDatePickerWnd::CurrentDateGet() const
{
	ASSERT_VALID( this );
	return m_dtCurrentDate;
}

void CExtDatePickerWnd::CurrentDateGet( 
	INT & nYear, 
	INT & nMonth, 
	INT & nDay 
	) const
{
	ASSERT_VALID( this );
	COleDateTime dtCurrentDate = CurrentDateGet();
	ASSERT( dtCurrentDate.GetStatus() == COleDateTime::valid );
	nYear = dtCurrentDate.GetYear();
	nMonth = dtCurrentDate.GetMonth();
	nDay = dtCurrentDate.GetDay();
}

void CExtDatePickerWnd::DimSet( 
	CSize sizeCalendarDimensionsMin, // = CSize(0,0)
	CSize sizeCalendarDimensionsMax, // = CSize(0,0)
	bool bUpdateNow // = false
	)
{
	ASSERT_VALID( this );
	if( sizeCalendarDimensionsMin != CSize(0,0) )
	{
		if( sizeCalendarDimensionsMin.cx < 1 )
			sizeCalendarDimensionsMin.cx = 1;
		if( sizeCalendarDimensionsMin.cy < 1 )
			sizeCalendarDimensionsMin.cy = 1;
		m_sizeCalendarDimensionsMin = sizeCalendarDimensionsMin;
	}
	if( sizeCalendarDimensionsMax != CSize(0,0) )
	{
		if( sizeCalendarDimensionsMax.cx < 1 )
			sizeCalendarDimensionsMax.cx = 1;
		if( sizeCalendarDimensionsMax.cy < 1 )
			sizeCalendarDimensionsMax.cy = 1;
		if( sizeCalendarDimensionsMax.cx < sizeCalendarDimensionsMin.cx )
			sizeCalendarDimensionsMax.cx = sizeCalendarDimensionsMin.cx; 
		if( sizeCalendarDimensionsMax.cy < sizeCalendarDimensionsMin.cy )
			sizeCalendarDimensionsMax.cy = sizeCalendarDimensionsMin.cy; 
		m_sizeCalendarDimensionsMax = sizeCalendarDimensionsMax;
	}
	if( GetSafeHwnd() == NULL )
		return;
	UpdateDatePickerWnd( true, true, bUpdateNow );
}

void CExtDatePickerWnd::DimGet( 
	SIZE * pSizeCalendarDimensionsMin, // = NULL
	SIZE * pSizeCalendarDimensionsMax, // = NULL
	SIZE * pSizeCalendarDimensionsCurrent // = NULL
	) const
{
	ASSERT_VALID( this );
	if( pSizeCalendarDimensionsMin != NULL )
		(*pSizeCalendarDimensionsMin) = m_sizeCalendarDimensionsMin;
	if( pSizeCalendarDimensionsMax != NULL )
		(*pSizeCalendarDimensionsMax) = m_sizeCalendarDimensionsMax;
	if( pSizeCalendarDimensionsCurrent != NULL )
		(*pSizeCalendarDimensionsCurrent) = m_sizeCalendarDimensionsCurrent;
}

void CExtDatePickerWnd::OnTimeChange() 
{
	ASSERT_VALID( this );
	CWnd::OnTimeChange();
	UpdateDatePickerWnd( true, true, true );	
}

void CExtDatePickerWnd::OnSysColorChange() 
{
	ASSERT_VALID( this );
	CWnd::OnSysColorChange();
CExtPaintManager * pPM = PmBridge_GetPM();
	g_PaintManager.OnSysColorChange( this );
	g_CmdManager.OnSysColorChange( pPM, this );
	UpdateDatePickerWnd( true, true, true );	
}

LRESULT CExtDatePickerWnd::OnThemeChanged( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	wParam;
	lParam;
	_RecalcMetrics();
LRESULT lResult = Default();
//CExtPaintManager * pPM = PmBridge_GetPM();
//	g_PaintManager.OnThemeChanged( this, wParam, lParam );
//	g_CmdManager.OnThemeChanged( pPM, this, wParam, lParam );
	UpdateDatePickerWnd( true, true, false );	
	return lResult;
}

void CExtDatePickerWnd::OnSettingChange(UINT uFlags, __EXT_MFC_SAFE_LPCTSTR lpszSection) 
{
	ASSERT_VALID( this );
	_RecalcMetrics();
	CWnd::OnSettingChange(uFlags, lpszSection);
//CExtPaintManager * pPM = PmBridge_GetPM();
//	g_PaintManager.OnSettingChange( this, uFlags, lpszSection );
//	g_CmdManager.OnSettingChange( pPM, this, uFlags, lpszSection );
	UpdateDatePickerWnd( true, true, false );	
}

CExtSafeString CExtDatePickerWnd::OnDatePickerGetMonthName(
	INT nMonth,
	bool bMakeUpper // = false
	) const
{
INT nMonthNameLocale = LOCALE_SMONTHNAME1 + nMonth - 1;
LPTSTR lpMonthBuf = (LPTSTR)::LocalAlloc(LPTR, 200);
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			nMonthNameLocale,
			lpMonthBuf,
			200
			) != 0
		);
CExtSafeString sLongMonthName = (LPTSTR)lpMonthBuf;
	::LocalFree( lpMonthBuf );
	if( bMakeUpper )
		sLongMonthName.MakeUpper();
	return sLongMonthName;
}

INT CExtDatePickerWnd::OnDatePickerGetFirstDayOfWeek() const
{
LPTSTR lpFirstDayOfWeek = (LPTSTR)::LocalAlloc( LPTR, 24 );
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_IFIRSTDAYOFWEEK,
			lpFirstDayOfWeek,
			24
			) != 0
		);
	// 0 Monday
	// 1 Tuesday
	// 2 Wednesday
	// 3 Thursday
	// 4 Friday 
	// 5 Saturday
	// 6 Sunday
INT nFirstDayOfWeek = _ttoi( lpFirstDayOfWeek );
	::LocalFree( lpFirstDayOfWeek );
	return nFirstDayOfWeek;
}

CExtSafeString CExtDatePickerWnd::OnDatePickerGetShortDayName(
	INT nDayOfWeek,
	bool bMakeUpper // = false
	) const
{
INT nDayNameLocale = LOCALE_SABBREVDAYNAME1 + nDayOfWeek - 1;
LPTSTR lpDayOfWeekBuf = (LPTSTR)::LocalAlloc( LPTR, 100 );
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			nDayNameLocale,
			lpDayOfWeekBuf,
			100
			) != 0
		);
CExtSafeString sLongDayName = (LPTSTR)lpDayOfWeekBuf;
	::LocalFree( lpDayOfWeekBuf );
	if( bMakeUpper )
		sLongDayName.MakeUpper();
//CExtSafeString sShortDayName;
//	if( sLongDayName.GetLength() > 0 )
//		sShortDayName = sLongDayName.GetAt( 0 );
INT nLen = INT( sLongDayName.GetLength() );
CExtSafeString sShortDayName;
	if( nLen > 0 )
	{
		if( nLen >= 2 )
			sShortDayName = sLongDayName.Left( 2 );
		else
			sShortDayName = sLongDayName.GetAt( 0 );
	}
	return sShortDayName;
}

COleDateTime CExtDatePickerWnd::OnDatePickerGetToday() const
{
	ASSERT_VALID( this );
COleDateTime dtNow = COleDateTime::GetCurrentTime();
	return dtNow;
}

CRect CExtDatePickerWnd::OnDatePickerGetBorderMetrics() const
{
	ASSERT_VALID( this );
DWORD dwDatePickerStyle = GetDatePickerStyle();
	if( (dwDatePickerStyle & __EDPWS_BORDER_RESERVE) == 0 )
		return CRect( 0, 0, 0, 0 );
	return CRect( 4, 4, 4, 4 );
}

HFONT CExtDatePickerWnd::OnQueryFont() const
{
	ASSERT_VALID( this );
HFONT hFont = NULL;
	if( GetSafeHwnd() != NULL )
		hFont = (HFONT) ::SendMessage( m_hWnd, WM_GETFONT, 0L, 0L );
	if( hFont == NULL )
	{
		HWND hWndParent = ::GetParent( m_hWnd );
		if( hWndParent != NULL )
			hFont = (HFONT) ::SendMessage( hWndParent, WM_GETFONT, 0L, 0L );
	}
	if( hFont == NULL )
		hFont = (HFONT) PmBridge_GetPM()->m_FontNormal;
	return hFont;
}

void CExtDatePickerWnd::OnDatePickerQueryHeaderPopupFont(
	CFont * pFont
	) const
{
	ASSERT_VALID( this );

HFONT hFont = OnQueryFont();
	ASSERT( hFont != NULL );

LOGFONT lf;
	::GetObject( 
		(HGDIOBJ) hFont, 
        sizeof( lf ), 
        (LPVOID) & lf 
		);
	hFont = NULL;
	
	if( (GetDatePickerStyle() & __EDPWS_BOLD_HEADER_POPUP) != 0 )
		lf.lfWeight = FW_BOLD;

	hFont = ::CreateFontIndirect( &lf );
	ASSERT( hFont != NULL );
	pFont->Attach( hFont );
}

void CExtDatePickerWnd::OnDatePickerQueryDayCellFont(
	CFont * pFont,
	const COleDateTime & dt
	) const
{
	ASSERT_VALID( this );
	dt;

HFONT hFont = OnQueryFont();
	ASSERT( hFont != NULL );

LOGFONT lf;
	::GetObject( 
		(HGDIOBJ) hFont, 
        sizeof( lf ), 
        (LPVOID) & lf 
		);
	hFont = NULL;
	
	if( (GetDatePickerStyle() & __EDPWS_BOLD_DAY_CELLS) != 0 )
		lf.lfWeight = FW_BOLD;

	hFont = ::CreateFontIndirect( &lf );
	ASSERT( hFont != NULL );
	pFont->Attach( hFont );
}

void CExtDatePickerWnd::OnDatePickerQueryMonthHeaderFont(
	CFont * pFont,
	INT nYear,
	INT nMonth
	) const
{
	ASSERT_VALID( this );
	nYear;
	nMonth;

HFONT hFont = OnQueryFont();
	ASSERT( hFont != NULL );

LOGFONT lf;
	::GetObject( 
		(HGDIOBJ) hFont, 
        sizeof( lf ), 
        (LPVOID) & lf 
		);
	hFont = NULL;
	
	if( (GetDatePickerStyle() & __EDPWS_BOLD_HEADER) != 0 )
		lf.lfWeight = FW_BOLD;

	hFont = ::CreateFontIndirect( &lf );
	ASSERT( hFont != NULL );
	pFont->Attach( hFont );
}

void CExtDatePickerWnd::OnDatePickerQueryDaysOfWeekFont(
	CFont * pFont,
	INT nYear,
	INT nMonth
	) const
{
	ASSERT_VALID( this );
	nYear;
	nMonth;

HFONT hFont = OnQueryFont();
	ASSERT( hFont != NULL );

LOGFONT lf;
	::GetObject( 
		(HGDIOBJ) hFont, 
        sizeof( lf ), 
        (LPVOID) & lf 
		);
	hFont = NULL;
	
	if( (GetDatePickerStyle() & __EDPWS_BOLD_WEEK_DAYS) != 0 )
		lf.lfWeight = FW_BOLD;

	hFont = ::CreateFontIndirect( &lf );
	ASSERT( hFont != NULL );
	pFont->Attach( hFont );
}

INT CExtDatePickerWnd::OnDatePickerQueryIndentSpace() const
{
	ASSERT_VALID( this );
	return m_nIndentSpace;
}

CSize CExtDatePickerWnd::OnDatePickerQueryDateCellSize() const
{
	ASSERT_VALID( this );
	return m_sizeDateCell;
}

CSize CExtDatePickerWnd::OnDatePickerQueryPushButtonSize() const
{
	ASSERT_VALID( this );
	return m_sizePushButton;
}

CSize CExtDatePickerWnd::OnDatePickerQueryScrollButtonSize() const
{
	ASSERT_VALID( this );
	return m_sizeScrollButton;
}

INT CExtDatePickerWnd::OnDatePickerQueryMonthHeaderHeight() const
{
	ASSERT_VALID( this );
	return m_nMonthHeaderHeight;
}

INT CExtDatePickerWnd::OnDatePickerQueryDaysOfWeekHeight() const
{
	ASSERT_VALID( this );
	return m_nDaysOfWeekHeight;
}

INT CExtDatePickerWnd::OnDatePickerQueryButtonAreaHeight() const
{
	ASSERT_VALID( this );
	return m_nButtonAreaHeight;
}

CSize CExtDatePickerWnd::OnDatePickerQueryMonthPickerSize() const
{
	ASSERT_VALID( this );
	return m_sizeMonthPicker;
}

void CExtDatePickerWnd::OnDatePickerRecalcMetrics(
	CDC & dcMeasure,
	CSize & sizeMonthPicker,
	CSize & sizeDateCell,
	CSize & sizePushButton,
	CSize & sizeScrollButton,
	INT & nMonthHeaderHeight,
	INT & nButtonAreaHeight,
	INT & nDaysOfWeekHeight,
	INT & nIndentSpace
	)
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );

HFONT hFont = OnQueryFont();
	ASSERT( hFont != NULL );

LOGFONT lf;
	::GetObject( 
		(HGDIOBJ) hFont, 
        sizeof( lf ), 
        (LPVOID) & lf 
		);
	hFont = NULL;
	
	if( (GetDatePickerStyle() & __EDPWS_BOLD_ANY) != 0 )
		lf.lfWeight = FW_BOLD;

	hFont = ::CreateFontIndirect( &lf );
	ASSERT( hFont != NULL );
CFont font;
	font.Attach( hFont );

CFont * pOldFont =
		dcMeasure.SelectObject( &font );

CRect rcText(0,0,0,0);
CSize sizeText(0,0);

CExtPaintManager * pPM = PmBridge_GetPM();

	nIndentSpace = pPM->UiScalingDo( 11, CExtPaintManager::__EUIST_X );
	
	// measure week-day cell size
CSize sizeWD(
		pPM->UiScalingDo( 10, CExtPaintManager::__EUIST_X ),
		pPM->UiScalingDo( 10, CExtPaintManager::__EUIST_Y )
		);
INT i = 0;
	for( i = 0; i < 7; i++ )
	{
		CExtSafeString sWD = OnDatePickerGetShortDayName( i );
		rcText.SetRect( 0, 0, 0, 0 );
		CExtRichContentLayout::stat_DrawText(
			dcMeasure.m_hDC,
			LPCTSTR(sWD), sWD.GetLength(),
			rcText,
			DT_SINGLELINE|DT_LEFT|DT_TOP|DT_CALCRECT, 0
			);
		sizeText = rcText.Size();
		sizeText.cx += pPM->UiScalingDo( 2, CExtPaintManager::__EUIST_X );
		sizeText.cy += pPM->UiScalingDo( 2, CExtPaintManager::__EUIST_Y );
		if( sizeWD.cx < sizeText.cx )
			sizeWD.cx = sizeText.cx;
		if( sizeWD.cy < sizeText.cy )
			sizeWD.cy = sizeText.cy;
	} // for( i = 0; i < 7; i++ )

	// measure date cell size
	sizeDateCell.cx = pPM->UiScalingDo( 17, CExtPaintManager::__EUIST_X );
	sizeDateCell.cy = pPM->UiScalingDo( 15, CExtPaintManager::__EUIST_Y );
	for( i = 1; i <= 31; i++ )
	{
		CExtSafeString strMeasure;
		strMeasure.Format( _T("%02d"), i );
		rcText.SetRect( 0, 0, 0, 0 );
		CExtRichContentLayout::stat_DrawText(
			dcMeasure.m_hDC,
			LPCTSTR(strMeasure), strMeasure.GetLength(),
			rcText,
			DT_SINGLELINE|DT_LEFT|DT_TOP|DT_CALCRECT, 0
			);
		sizeText = rcText.Size();
		sizeText.cx += pPM->UiScalingDo( 2, CExtPaintManager::__EUIST_X );
		sizeText.cy += pPM->UiScalingDo( 2, CExtPaintManager::__EUIST_Y );
		if( sizeDateCell.cx < sizeText.cx )
			sizeDateCell.cx = sizeText.cx;
		if( sizeDateCell.cy < sizeText.cy )
			sizeDateCell.cy = sizeText.cy;
	} // for( i = 1; i <= 31; i++ )

	// update week-day cell size
	if( sizeDateCell.cx < sizeWD.cx )
		sizeDateCell.cx = sizeWD.cx;
	else if( sizeDateCell.cx > sizeWD.cx )
		sizeWD.cx = sizeDateCell.cx;

	// measure push-button size
	sizePushButton.cx = pPM->UiScalingDo( 47, CExtPaintManager::__EUIST_X );
	sizePushButton.cy = pPM->UiScalingDo( 18, CExtPaintManager::__EUIST_Y );
CExtSafeString strCaption = OnDatePickerQueryPushButtonCaptionText( __EDPWH_BTN_TODAY );
	rcText.SetRect( 0, 0, 0, 0 );
	CExtRichContentLayout::stat_DrawText(
		dcMeasure.m_hDC,
		LPCTSTR(strCaption), strCaption.GetLength(),
		rcText,
		DT_SINGLELINE|DT_LEFT|DT_TOP|DT_CALCRECT, 0
		);
	sizeText = rcText.Size();
	sizeText.cx += pPM->UiScalingDo( 10, CExtPaintManager::__EUIST_X );
	sizeText.cy += pPM->UiScalingDo( 6, CExtPaintManager::__EUIST_Y );
	if( sizePushButton.cx < sizeText.cx )
		sizePushButton.cx = sizeText.cx;
	if( sizePushButton.cy < sizeText.cy )
		sizePushButton.cy = sizeText.cy;
	strCaption = OnDatePickerQueryPushButtonCaptionText( __EDPWH_BTN_NONE );
	rcText.SetRect( 0, 0, 0, 0 );
	CExtRichContentLayout::stat_DrawText(
		dcMeasure.m_hDC,
		LPCTSTR(strCaption), strCaption.GetLength(),
		rcText,
		DT_SINGLELINE|DT_LEFT|DT_TOP|DT_CALCRECT, 0
		);
	sizeText = rcText.Size();
	sizeText.cx += pPM->UiScalingDo( 10, CExtPaintManager::__EUIST_X );
	sizeText.cy += pPM->UiScalingDo( 6, CExtPaintManager::__EUIST_Y );
	if( sizePushButton.cx < sizeText.cx )
		sizePushButton.cx = sizeText.cx;
	if( sizePushButton.cy < sizeText.cy )
		sizePushButton.cy = sizeText.cy;

	// measure scroll-button size
	sizeScrollButton.cx = pPM->UiScalingDo( 16, CExtPaintManager::__EUIST_X );
	sizeScrollButton.cy = pPM->UiScalingDo( 16, CExtPaintManager::__EUIST_Y );
CFont * pTempFont = dcMeasure.SelectObject( &PmBridge_GetPM()->m_FontMarlett );
	dcMeasure.SelectObject( &PmBridge_GetPM()->m_FontMarlett );
	rcText.SetRect( 0, 0, 0, 0 );
LPCTSTR strDrawArrow = _T("3");
	CExtRichContentLayout::stat_DrawText(
		dcMeasure.m_hDC,
		strDrawArrow, // popup rightArrow
		1,
		&rcText,
		DT_SINGLELINE|DT_LEFT|DT_TOP|DT_CALCRECT, 0
		);
	sizeText = rcText.Size();
	if( sizeScrollButton.cx < sizeText.cx )
		sizeScrollButton.cx = sizeText.cx;
	if( sizeScrollButton.cy < sizeText.cy )
		sizeScrollButton.cy = sizeText.cy;
	rcText.SetRect( 0, 0, 0, 0 );
	strDrawArrow = _T("4");
	CExtRichContentLayout::stat_DrawText(
		dcMeasure.m_hDC,
		strDrawArrow, // popup rightArrow
		1,
		&rcText,
		DT_SINGLELINE|DT_LEFT|DT_TOP|DT_CALCRECT, 0
		);
	sizeText = rcText.Size();
	if( sizeScrollButton.cx < sizeText.cx )
		sizeScrollButton.cx = sizeText.cx;
	if( sizeScrollButton.cy < sizeText.cy )
		sizeScrollButton.cy = sizeText.cy;
	dcMeasure.SelectObject( pTempFont );

	// measure header text
CSize sizeHeaderText(
		pPM->UiScalingDo( 10, CExtPaintManager::__EUIST_X ),
		pPM->UiScalingDo( 10, CExtPaintManager::__EUIST_Y )
		);
	for( i = 1; i <= 12; i++ )
	{
		CExtSafeString sMonthName =
			OnDatePickerGetMonthName( i );
		sMonthName += _T(" 0000");
		CFont font;
		OnDatePickerQueryHeaderPopupFont( &font );
		ASSERT( font.GetSafeHandle() != NULL );
		rcText = 
			CExtPaintManager::stat_CalcTextDimension(
			dcMeasure,
			font,
			sMonthName
			);
		sizeText = rcText.Size();
		if( sizeHeaderText.cx < sizeText.cx )
			sizeHeaderText.cx = sizeText.cx;
		if( sizeHeaderText.cy < sizeText.cy )
			sizeHeaderText.cy = sizeText.cy;
	} // for( i = 1; i <= 12; i++ )
	
	// measure header area
CSize sizeHeaderArea( sizeHeaderText );
	sizeHeaderArea.cx += ( sizeScrollButton.cx * 2 + pPM->UiScalingDo( 4, CExtPaintManager::__EUIST_X ) );
	if( ( sizeScrollButton.cy + 2 ) > sizeHeaderArea.cy )
		sizeHeaderArea.cy = ( sizeScrollButton.cy + 2 );

	if( sizeHeaderArea.cx < ( sizeWD.cx * 7 ) )
		sizeHeaderArea.cx = ( sizeWD.cx * 7 );

	// put header's and weekdays heights
	nMonthHeaderHeight = sizeHeaderArea.cy;
INT nMinMonthHeaderHeight = pPM->UiScalingDo( 18, CExtPaintManager::__EUIST_Y );
	if( nMonthHeaderHeight < nMinMonthHeaderHeight )
		nMonthHeaderHeight = nMinMonthHeaderHeight;
	nDaysOfWeekHeight = sizeWD.cy;
	nDaysOfWeekHeight += pPM->UiScalingDo( 2, CExtPaintManager::__EUIST_Y );
INT nMinDaysOfWeekHeight = pPM->UiScalingDo( 16, CExtPaintManager::__EUIST_Y );
	if( nDaysOfWeekHeight < nMinDaysOfWeekHeight )
		nDaysOfWeekHeight = nMinDaysOfWeekHeight;

	// put buttons-area height
	nButtonAreaHeight = sizePushButton.cy + pPM->UiScalingDo( 10, CExtPaintManager::__EUIST_Y );
INT nMinButtonAreaHeight = pPM->UiScalingDo( 30, CExtPaintManager::__EUIST_Y );
	if( nButtonAreaHeight < nMinButtonAreaHeight )
		nButtonAreaHeight = nMinButtonAreaHeight;

	sizeMonthPicker.cx = sizeDateCell.cx * 7;
	sizeMonthPicker.cy = sizeDateCell.cy * 6 + nDaysOfWeekHeight + nMonthHeaderHeight;
	if( sizeMonthPicker.cx < sizeWD.cx )
		sizeMonthPicker.cx = sizeWD.cx;
	if( sizeMonthPicker.cx < sizeHeaderArea.cx )
		sizeMonthPicker.cx = sizeHeaderArea.cx;
	sizeMonthPicker.cx += nIndentSpace * 2;

INT nMinX = 143;
INT nMinY = 125;
	if( sizeMonthPicker.cx < nMinX )
		sizeMonthPicker.cx = nMinX;
	if( sizeMonthPicker.cy < nMinY )
		sizeMonthPicker.cy = nMinY;

	dcMeasure.SelectObject( pOldFont );
}

CSize CExtDatePickerWnd::OnDatePickerCalcContentSize(
	const SIZE & sizeDim
	) const
{
	ASSERT_VALID( this );
	ASSERT( sizeDim.cx >= 1 && sizeDim.cy >= 1 );
	if( GetSafeHwnd() == NULL )
		( const_cast < CExtDatePickerWnd * > ( this ) ) ->
			_RecalcMetrics();
CSize sizeMonthPicker = OnDatePickerQueryMonthPickerSize();
CRect rcBorderMetrics = OnDatePickerGetBorderMetrics();
CSize sizeContent(
		sizeMonthPicker.cx*sizeDim.cx
			+ rcBorderMetrics.left + rcBorderMetrics.right,
		sizeMonthPicker.cy*sizeDim.cy
			+ rcBorderMetrics.top + rcBorderMetrics.bottom
		);
DWORD dwDatePickerStyle = GetDatePickerStyle();
	if( (dwDatePickerStyle & __EDPWS_BUTTON_ANY ) != 0 )
		sizeContent.cy += OnDatePickerQueryButtonAreaHeight();
	return sizeContent;
}

void CExtDatePickerWnd::_RecalcMetrics()
{
	ASSERT_VALID( this );
CWindowDC dcMeasure( NULL );
	OnDatePickerRecalcMetrics(
		dcMeasure,
		m_sizeMonthPicker,
		m_sizeDateCell,
		m_sizePushButton,
		m_sizeScrollButton,
		m_nMonthHeaderHeight,
		m_nButtonAreaHeight,
		m_nDaysOfWeekHeight,
		m_nIndentSpace
		);
}

void CExtDatePickerWnd::OnDatePickerEraseClientArea(
	CDC & dc,
	const CRect & rcClient
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	PmBridge_GetPM()->PaintDatePickerClientArea(
		dc,
		rcClient,
		m_rcDatePickerClient,
		(CObject*)this
		);
}

void CExtDatePickerWnd::OnDatePickerDrawBorder(
	CDC & dc,
	const CRect & rcClient
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	PmBridge_GetPM()->PaintDatePickerBorder(
		dc,
		rcClient,
		(CObject*)this
		);
}

void CExtDatePickerWnd::OnDatePickerDrawTodayBorder(
	CDC & dc,
	bool bShowNonMonthDays,
	bool bShowTodayHighlight,
	bool bDrawNonMonthDay,
	bool bDrawToday,
	const CRect & rcCell,
	const COleDateTime & dt,
	bool bDrawSelected
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	bShowNonMonthDays;
	bDrawNonMonthDay;
	dt;
	bDrawSelected;
	if(		bShowTodayHighlight
		&&	bDrawToday
		)
	{
		COLORREF clrTodayBorder =
			dc.GetNearestColor( RGB(128,0,0) );
		dc.Draw3dRect(
			(LPRECT)&rcCell,
			clrTodayBorder,
			clrTodayBorder
			);
	}
}

void CExtDatePickerWnd::OnDatePickerDrawDateCell(
	CDC & dc,
	bool bShowNonMonthDays,
	bool bShowTodayHighlight,
	bool bDrawNonMonthDay,
	bool bDrawToday,
	const CRect & rcCell,
	const COleDateTime & dt,
	bool bDrawSelected
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if(	(!bShowNonMonthDays) && bDrawNonMonthDay )
		return;
COLORREF clrText =
		PmBridge_GetPM()->GetColor( COLOR_BTNTEXT, (CObject*)this );
COLORREF clrBk =
		PmBridge_GetPM()->GetColor( CExtPaintManager::CLR_3DFACE_IN, (CObject*)this );
	if( bDrawNonMonthDay && (!bDrawSelected) )
		clrText = PmBridge_GetPM()->GetSysColor( COLOR_3DSHADOW );
INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
COLORREF clrTextOld = dc.SetTextColor( clrText );
CFont font;
	OnDatePickerQueryDayCellFont( &font, dt );
	ASSERT( font.GetSafeHandle() != NULL );
CFont * pOldFont = dc.SelectObject( &font );
CString strDay;
	strDay.Format( _T("%d"), dt.GetDay() );
	if( bDrawSelected )
		dc.FillSolidRect( &rcCell, clrBk );
CRect rcText( rcCell );
	rcText.right -= 2;
	CExtRichContentLayout::stat_DrawText(
		dc.m_hDC,
		LPCTSTR(strDay), 
		strDay.GetLength(),
		&rcText,
		DT_RIGHT|DT_VCENTER|DT_SINGLELINE, 0
		);
	OnDatePickerDrawTodayBorder(
		dc,
		bShowNonMonthDays,
		bShowTodayHighlight,
		bDrawNonMonthDay,
		bDrawToday,
		rcCell,
		dt,
		bDrawSelected
		);
	dc.SelectObject( pOldFont );
	dc.SetTextColor( clrTextOld );
	dc.SetBkMode( nOldBkMode );
}

void CExtDatePickerWnd::OnDatePickerDrawButtonsSeparator(
	CDC & dc,
	const CRect & rcSeparator
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	PmBridge_GetPM()->PaintDatePickerButtonsSeparator(
		dc,
		rcSeparator,
		(CObject*)this
		);
}

CExtSafeString CExtDatePickerWnd::OnDatePickerQueryPushButtonCaptionText(
	LONG nButtonType // __EDPWH_BTN_NONE or __EDPWH_BTN_TODAY
	) const
{
CExtSafeString strButtonCaption( _T("") );;
	switch( nButtonType )
	{
	case __EDPWH_BTN_NONE:
		if( ! g_ResourceManager->LoadString( strButtonCaption, IDS_EXT_DP_BTN_NONE ) )
			strButtonCaption = _T("None");
		break;
	case __EDPWH_BTN_TODAY:
		if( ! g_ResourceManager->LoadString( strButtonCaption, IDS_EXT_DP_BTN_TODAY ) )
			strButtonCaption = _T("Today");
		break;
	} // switch( nButtonType )
	return strButtonCaption;
}

void CExtDatePickerWnd::OnDatePickerDrawPushButton(
	CDC & dc,
	const CRect & rcButton,
	LONG nButtonType, // __EDPWH_BTN_NONE or __EDPWH_BTN_TODAY
	__EXT_MFC_SAFE_LPCTSTR strCaption,
	bool bFlat,
	bool bDrawBorder,
	bool bPushed,
	bool bHover,
	HFONT hFont
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	PmBridge_GetPM()->PaintDatePickerPushButton(
		dc,
		rcButton,
		nButtonType,
		strCaption,
		bFlat,
		bDrawBorder,
		bPushed,
		bHover,
		hFont,
		(CObject*)this
		);
}

void CExtDatePickerWnd::OnDatePickerEraseHeader(
	INT nCol,
	INT nRow,
	CDC & dc,
	INT nYear,
	INT nMonth,
	const CRect & rcHeader,
	const CRect & rcHeaderWithoutBorders,
	const CRect & rcHeaderDate,
	const CRect & rcScrollBtnBackward,
	const CRect & rcScrollBtnForward
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	PmBridge_GetPM()->PaintDatePickerHeaderBackground(
		nCol,
		nRow,
		dc,
		nYear,
		nMonth,
		rcHeader,
		rcHeaderWithoutBorders,
		rcHeaderDate,
		rcScrollBtnBackward,
		rcScrollBtnForward,
		(CObject*)this
		);
}

void CExtDatePickerWnd::OnDatePickerDrawHeader(
	INT nCol,
	INT nRow,
	CDC & dc,
	INT nYear,
	INT nMonth,
	const CRect & rcHeader,
	const CRect & rcHeaderWithoutBorders,
	const CRect & rcHeaderDate,
	const CRect & rcScrollBtnBackward,
	const CRect & rcScrollBtnForward
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
DWORD dwDatePickerStyle = GetDatePickerStyle();
	if( (dwDatePickerStyle & __EDPWS_NO_HEADER_BACKGROUND) == 0 )
		OnDatePickerEraseHeader(
			nCol,
			nRow,
			dc,
			nYear,
			nMonth,
			rcHeader,
			rcHeaderWithoutBorders,
			rcHeaderDate,
			rcScrollBtnBackward,
			rcScrollBtnForward
			);
CExtSafeString sCaption = OnDatePickerGetMonthName( nMonth );
	sCaption += _T(" ");
CExtSafeString sYear;
	sYear.Format( _T("%d"), nYear );
	sCaption += sYear;
INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
COLORREF clrText = PmBridge_GetPM()->GetColor( COLOR_BTNTEXT, (CObject*)this );
COLORREF clrTextOld = dc.SetTextColor( clrText );
CFont font;
	OnDatePickerQueryMonthHeaderFont( 
		&font, 
		nYear, 
		nMonth 
		);
	ASSERT( font.GetSafeHandle() != NULL );
CFont * pOldFont = dc.SelectObject( &font );
	CExtRichContentLayout::stat_DrawText(
		dc.m_hDC,
		LPCTSTR(sCaption),
		sCaption.GetLength(),
		(LPRECT)&rcHeaderDate,
		DT_CENTER | DT_VCENTER | DT_SINGLELINE, 0
		);
	dc.SelectObject( pOldFont );

__EXT_MFC_LONG_PTR dwExStyle = ::__EXT_MFC_GetWindowLong( GetSafeHwnd(), GWL_EXSTYLE );
bool bRTL = ( (dwExStyle & WS_EX_LAYOUTRTL) != 0 ) ? true : false;

	// draw scrolling buttons
	pOldFont = dc.SelectObject( &PmBridge_GetPM()->m_FontMarlett );
	if(		(! rcScrollBtnBackward.IsRectEmpty() )
		&&	nRow == 0 
		&&  nCol == 0
		)
	{
		COLORREF clrTextArrow = 
			PmBridge_GetPM()->GetColor( 
				(nYear == __EXT_DATE_YEAR_MIN && nMonth == 1)
					? COLOR_3DSHADOW
					: COLOR_BTNTEXT,
				(CObject*)this 
				);
		COLORREF clrTextArrowOld = dc.SetTextColor( clrTextArrow );
		LPCTSTR strDrawArrow = bRTL ? _T("4") : _T("3");
		CExtRichContentLayout::stat_DrawText(
			dc.m_hDC,
			strDrawArrow,
			1,
			(LPRECT)&rcScrollBtnBackward,
			DT_CENTER|DT_VCENTER|DT_SINGLELINE, 0
			);
		dc.SetTextColor( clrTextArrowOld );
	}
CSize szDimensions;
	DimGet( 
		NULL,
		NULL,
		&szDimensions
		);
	if(		(! rcScrollBtnForward.IsRectEmpty() ) 
		&&	nRow == 0 
		&&	(szDimensions.cx - 1) == nCol 
		)
	{
		INT nCheckMonth = nMonth;
		INT nCheckYear = nYear;
		for( INT i = 0; i < (szDimensions.cx * szDimensions.cy - szDimensions.cx); i++ )
		{
			nCheckMonth++;
			if( nCheckMonth > 12 )
			{
				nCheckMonth = 1;
				nCheckYear++;
			}
		}
		INT nColorIndex = COLOR_3DSHADOW;
		if(		(nCheckYear < __EXT_DATE_YEAR_MAX && nCheckMonth <= 12)
			||	(nCheckYear == __EXT_DATE_YEAR_MAX && nCheckMonth < 12)
			)
			nColorIndex = COLOR_BTNTEXT;
		COLORREF clrTextArrow = 
			PmBridge_GetPM()->GetColor( 
				nColorIndex,
				(CObject*)this 
				);
		COLORREF clrTextArrowOld = dc.SetTextColor( clrTextArrow );
		LPCTSTR strDrawArrow = bRTL ? _T("3") : _T("4");
		CExtRichContentLayout::stat_DrawText(
			dc.m_hDC,
			strDrawArrow,
			1,
			(LPRECT)&rcScrollBtnForward,
			DT_CENTER|DT_VCENTER|DT_SINGLELINE, 0
			);
		dc.SetTextColor( clrTextArrowOld );
	}
	dc.SelectObject( pOldFont );
	dc.SetTextColor( clrTextOld );
	dc.SetBkMode( nOldBkMode );
}

void CExtDatePickerWnd::OnDatePickerDrawDaysOfWeek(
	INT nYear,
	INT nMonth,
	CDC & dc,
	const CRect & rcClient
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	// day headers
	INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
	COLORREF clrText = PmBridge_GetPM()->GetColor( COLOR_BTNTEXT, (CObject*)this );
	COLORREF clrTextOld = dc.SetTextColor( clrText );
CFont font;
	OnDatePickerQueryDaysOfWeekFont( 
		&font, 
		nYear, 
		nMonth 
		);
	ASSERT( font.GetSafeHandle() != NULL );
CFont * pOldFont = dc.SelectObject( &font );
INT nFirstDayOfWeek = OnDatePickerGetFirstDayOfWeek();
CSize sizeDateCell = OnDatePickerQueryDateCellSize();
	for( INT i = 0; i < 7; i++ )
	{
		CRect rcCell( 
			rcClient.left + i*sizeDateCell.cx,
			rcClient.top, 
			rcClient.left + i*sizeDateCell.cx + sizeDateCell.cx,
			rcClient.top + sizeDateCell.cy
			);
		rcCell.right -= 2; // text indent from right border
		rcCell.OffsetRect(0,-1);
		INT nDay = i + 1;
		for( INT j = 0; j < nFirstDayOfWeek; j++ )
		{
			if( nDay == 7 )
				nDay = 1;
			else
				nDay++;
		}
		CExtSafeString sShortDayName = OnDatePickerGetShortDayName( nDay );
		CExtRichContentLayout::stat_DrawText(
			dc.m_hDC,
			LPCTSTR(sShortDayName), sShortDayName.GetLength(),
			rcCell,
			DT_RIGHT|DT_VCENTER|DT_SINGLELINE, 0
			);
	} // pOldFont = dc.SelectObject
	dc.SelectObject( pOldFont );
	dc.SetTextColor( clrTextOld );
	dc.SetBkMode( nOldBkMode );
	// bottom line
	CRect rcBL(
		rcClient.left,
		rcClient.bottom - 2,
		rcClient.right,
		rcClient.bottom - 1
		);
	dc.FillSolidRect( 
		&rcBL, 
		PmBridge_GetPM()->GetSysColor( COLOR_3DSHADOW )
		);
}

void CExtDatePickerWnd::OnDatePickerDrawEntire(
	CDC & dc,
	const CRect & rcClient
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	
	if( ! dc.RectVisible( rcClient ) )
		return;

	if( AnimationClient_StatePaint( dc ) )
		return;

HFONT hFont = OnQueryFont();
	ASSERT( hFont != NULL );

	// draw background
	OnDatePickerEraseClientArea( dc, rcClient );
	// draw border
DWORD dwDatePickerStyle = GetDatePickerStyle();
	if( (dwDatePickerStyle&__EDPWS_BORDER) == __EDPWS_BORDER )
		OnDatePickerDrawBorder( dc, m_rcDatePickerClient );
	// draw dates
	for( INT nRow = 0; nRow < m_sizeCalendarDimensionsCurrent.cy; nRow++ )
	{
		for( INT nCol = 0; nCol < m_sizeCalendarDimensionsCurrent.cx; nCol++ )
		{
			const MONTH_INFO * pMonth = _FindMonth( nRow, nCol );	
			if( pMonth != NULL )
				pMonth->Draw( dc );
		}
	}
	// draw button's separator
	if(		(dwDatePickerStyle & __EDPWS_BUTTON_ANY ) != 0
		&&	(dwDatePickerStyle & __EDPWS_BUTTON_SEPARATOR) != 0
		)
	{
		INT nIndentSpace = OnDatePickerQueryIndentSpace();
		CRect rcSeparator(
			m_rcDatePickerInnerArea.left + nIndentSpace, 
			m_rcDatePickerInnerArea.bottom + 1, 
			m_rcDatePickerInnerArea.right - nIndentSpace, 
			m_rcDatePickerInnerArea.bottom + 2
			);
		OnDatePickerDrawButtonsSeparator(
			dc,
			rcSeparator
			);
	}
	// draw TODAY button
	if( ! m_rcBtnToday.IsRectEmpty() )
	{
		CExtSafeString strCaption = 
			OnDatePickerQueryPushButtonCaptionText( __EDPWH_BTN_TODAY );
		OnDatePickerDrawPushButton(
			dc,
			m_rcBtnToday,
			__EDPWH_BTN_TODAY,
			(LPCTSTR)strCaption,
			false,
			true,
			( m_eMTT == __EMTT_BTN_TODAY_PRESSED ) ? true : false,
			( m_eMTT == __EMTT_BTN_TODAY_HOVERED ) ? true : false,
			hFont
			);
	}
	// draw NONE button
	if( ! m_rcBtnNone.IsRectEmpty() )
	{
		CExtSafeString strCaption = OnDatePickerQueryPushButtonCaptionText( __EDPWH_BTN_NONE );
		OnDatePickerDrawPushButton(
			dc,
			m_rcBtnNone,
			__EDPWH_BTN_NONE,
			(LPCTSTR)strCaption,
			false,
			true,
			( m_eMTT == __EMTT_BTN_NONE_PRESSED ) ? true : false,
			( m_eMTT == __EMTT_BTN_NONE_HOVERED ) ? true : false,
			hFont
			);
	}
}

void CExtDatePickerWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	ASSERT_VALID( this );
	if( !_ProcessMouseClick( point, true, MK_LBUTTON, nFlags ) )
		CWnd::OnLButtonDown(nFlags, point);
}

void CExtDatePickerWnd::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	ASSERT_VALID( this );
	if( !_ProcessMouseClick( point, true, MK_LBUTTON, nFlags ) )
		CWnd::OnLButtonDblClk(nFlags, point);
}

void CExtDatePickerWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
	ASSERT_VALID( this );
	if( !_ProcessMouseClick( point, false, MK_LBUTTON, nFlags ) )
		CWnd::OnLButtonUp(nFlags, point);
}

void CExtDatePickerWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
	ASSERT_VALID( this );
	if( !_ProcessMouseMove( point, nFlags ) )
		CWnd::OnMouseMove(nFlags, point);
}

bool CExtDatePickerWnd::_ProcessMouseMove(
	CPoint point,
	UINT nMouseEventFlags
	)
{
	nMouseEventFlags;
	if(	! CExtPopupMenuWnd::TestHoverEnabledFromActiveHWND( 
			GetSafeHwnd(), 
			::GetActiveWindow(),
			true,
			true,
			false
			) 
		)
		return false;
e_MouseTrackType_t eMTT = __EMTT_NOTHING;
	if(		m_eMTT == __EMTT_SCROLLING_BACKWARD
		||	m_eMTT == __EMTT_SCROLLING_FORWARD
		||	m_eMTT == __EMTT_DAYS_SELECTING
		)
		eMTT = m_eMTT;
LONG nHitTest = HitTest( point );
DWORD dwDatePickerStyle = GetDatePickerStyle();
	switch( nHitTest )
	{
	case __EDPWH_INNER_AREA:
		{
			MONTH_INFO * pMonth = _HitTestMonth( point );
			if( pMonth != NULL )
			{
				LONG nHitTestMonth = pMonth->HitTest( point );
				switch( nHitTestMonth )
				{
				case __EDPWH_DAYS_AREA:
					if( m_eMTT == __EMTT_DAYS_SELECTING )
					{
						const MONTH_INFO::DATE_INFO * pDayCell = pMonth->HitTestDay( point );
						if( pDayCell != NULL )
						{
							INT nMonth = 0, nYear = 0;
							pMonth->GetMonth( nMonth, nYear );
							COleDateTime dtCell = pDayCell->GetDate();
							if( dtCell.GetStatus() == COleDateTime::valid )
							{
								bool bNonMonthDay = 
									dtCell.GetMonth() == nMonth
									? false
									: true;
								
							if(		!(bNonMonthDay 
								&&	!(dwDatePickerStyle & __EDPWS_SHOW_NON_MONTH_DAYS)) 
								)
							{
									COleDateTime dt = pDayCell->GetDate();
									if( dt.GetStatus() == COleDateTime::valid )
									{
										if(	(dwDatePickerStyle & __EDPWS_MULTIPLE_SELECTION) != 0 )
										{
											COleDateTime dtSelBegin, dtSelEnd;
											SelectionGet( dtSelBegin, dtSelEnd );
											SelectionSet( dtSelBegin, pDayCell->GetDate(), false, true );
										}
										else
										{
											//EnsureVisible( dt );
											SelectionSet( dt, dt, false, true );
										}
									} // if( dt.GetStatus() == COleDateTime::valid )
								}
							}
						} // if( pDayCell != NULL )
					} // if( m_eMTT == __EMTT_DAYS_SELECTING )
					break;
				}
			} // if( pMonth != NULL )
			break;
		}
		
	case __EDPWH_BTN_TODAY:
		eMTT = __EMTT_BTN_TODAY_HOVERED;
		break;
		
	case __EDPWH_BTN_NONE:
		eMTT = __EMTT_BTN_NONE_HOVERED;
		break;

	} // switch( nHitTest )

bool bTracking = false;
	if(		m_eMTT == __EMTT_BTN_NONE_PRESSED 
		||	m_eMTT == __EMTT_BTN_TODAY_PRESSED 
		||	m_eMTT == __EMTT_DAYS_SELECTING
		||	m_eMTT == __EMTT_SCROLLING_BACKWARD
		||	m_eMTT == __EMTT_SCROLLING_FORWARD
		)
		bTracking = true;
	if( eMTT != m_eMTT )
	{
		bool bHoverStateChanged = 
			(
				(	(   m_eMTT == __EMTT_BTN_NONE_HOVERED
					||	m_eMTT == __EMTT_BTN_TODAY_HOVERED
					)
					&&	
					(	eMTT != __EMTT_BTN_NONE_HOVERED
					&&	eMTT != __EMTT_BTN_TODAY_HOVERED
					)
				)
				||
				(	(   m_eMTT != __EMTT_BTN_NONE_HOVERED
					&&	m_eMTT != __EMTT_BTN_TODAY_HOVERED
					)
					&&	
					(	eMTT == __EMTT_BTN_NONE_HOVERED
					||	eMTT == __EMTT_BTN_TODAY_HOVERED
					)
				)
				||	
				(
						m_eMTT != eMTT
					&&	
					(	eMTT == __EMTT_BTN_NONE_HOVERED
					||	eMTT == __EMTT_BTN_TODAY_HOVERED
					)
					&&
					(   m_eMTT == __EMTT_BTN_NONE_HOVERED
					||	m_eMTT == __EMTT_BTN_TODAY_HOVERED
					)
				)
			) 
			? true : false;

		bool bAnimationLocked = AnimationClient_CacheGeneratorIsLocked();
		INT eAPT = 
			(		eMTT == __EMTT_BTN_NONE_HOVERED 
				||	eMTT == __EMTT_BTN_TODAY_HOVERED 
				)
				? __EAPT_BY_HOVERED_STATE_TURNED_ON 
				: __EAPT_BY_HOVERED_STATE_TURNED_OFF;

		if(		bHoverStateChanged
			&&	(!bAnimationLocked) 
			)
		{
			AnimationClient_CacheGeneratorLock();
 			AnimationClient_CacheNextStateMinInfo( false, eAPT );
		}

		if( !bTracking )
			m_eMTT = eMTT;

		if(		bHoverStateChanged
			&&	(!bAnimationLocked) 
			)
		{
 			AnimationClient_CacheNextStateMinInfo( true, eAPT );
			AnimationClient_CacheGeneratorUnlock();
		}

		if( bHoverStateChanged )
			Invalidate();
	}

	if( eMTT != __EMTT_NOTHING )
	{
		if( !bTracking )
			if( ::GetCapture() != GetSafeHwnd() )
				::SetCapture( GetSafeHwnd() );
	}
	else
	{
		if( !bTracking )
			if( ::GetCapture() == GetSafeHwnd() )
				::ReleaseCapture();
	}

	return true;
}

volatile HWND CExtDatePickerWnd::g_hWndHeaderPopup = NULL;

bool CExtDatePickerWnd::_ProcessMouseClick(
	CPoint point,
	bool bButtonPressed,
	INT nMouseButton, // MK_... values
	UINT nMouseEventFlags
	)
{
	nMouseEventFlags;
	ASSERT_VALID( this );

	if( (GetStyle() & WS_TABSTOP) != 0 )
		SetFocus();

	if( MK_LBUTTON != nMouseButton )
		return false;

HWND hWndOwn = GetSafeHwnd();
	ASSERT( hWndOwn != NULL	&& ::IsWindow(hWndOwn) );
			
DWORD dwDatePickerStyle = GetDatePickerStyle();

bool bSetCapture = false;
e_MouseTrackType_t eMTT = __EMTT_NOTHING;

LONG nHitTest = HitTest( point );
	switch( nHitTest )
	{
	case __EDPWH_INNER_AREA:
		{
			MONTH_INFO * pMonth = _HitTestMonth( point );
			if( pMonth != NULL )
			{
				LONG nHitTestMonth = pMonth->HitTest( point );
				switch( nHitTestMonth )
				{
				case __EDPWH_BTN_BACKWARD:
					if( bButtonPressed )
					{
 						eMTT = __EMTT_SCROLLING_BACKWARD;
						bSetCapture = true;
					}
					break;
					
				case __EDPWH_BTN_FORWARD:
					if( bButtonPressed )
					{
 						eMTT = __EMTT_SCROLLING_FORWARD;
 						bSetCapture = true;
					}
					break;

				case __EDPWH_DAYS_AREA:
					{
						const MONTH_INFO::DATE_INFO * pDayCell = 
							pMonth->HitTestDay( point );
						if( pDayCell != NULL )
						{
							INT nMonth = 0, nYear = 0;
							pMonth->GetMonth( nMonth, nYear );
							bool bNonMonthDay = 
								( pDayCell->GetDate().GetMonth() == nMonth )
									? false
									: true;
							if(	!(bNonMonthDay && !(dwDatePickerStyle & __EDPWS_SHOW_NON_MONTH_DAYS)) )
							{
								COleDateTime dt = pDayCell->GetDate();
								if( bButtonPressed )
								{
 									eMTT = __EMTT_DAYS_SELECTING;
 									bSetCapture = true;
									if( dt.GetStatus() == COleDateTime::valid )
										SelectionSet( dt, dt, false, true );
								}
								if(		dt.GetStatus() == COleDateTime::valid
 									&&	m_eMTT == __EMTT_DAYS_SELECTING
									)
								{
									if( !bButtonPressed )
										EnsureVisible( dt, false );
									COleDateTime dtBegin, dtEnd;
									SelectionGet( dtBegin, dtEnd );
									SelectionSet( dtBegin, dt, false, false );
									Invalidate();
									HWND hWndNotificationReceiver =
										OnDatePickerGetNotificationReceiver();
									if( hWndNotificationReceiver != NULL )
									{
										SELECTION_NOTIFICATION _SN( m_pExternalSelectionInfo, *this, true );
										_SN.Notify( hWndNotificationReceiver );
										if( ! ::IsWindow(hWndOwn) )
											return true;
									} // if( hWndNotificationReceiver != NULL )
								}
							}						
						} // if( pDayCell != NULL )
					} // if( bButtonPressed )
					break;
					
				case __EDPWH_HEADER_DATE:
					{
						if( bButtonPressed )
						{
							// header popup list
							CExtDatePickerHeaderPopupWnd * pHeaderPopupWnd =
								new CExtDatePickerHeaderPopupWnd(
									this,
									((dwDatePickerStyle & __EDPWS_SHOW_MONTH_LIST_SHADOW) != 0 ) ? true : false
									);
							ASSERT_VALID( pHeaderPopupWnd );

							CSize sizeHeaderPopupWnd = pHeaderPopupWnd->GetSize( false );
							CSize sizeHeaderPopupWndClient = pHeaderPopupWnd->GetSize( true );

							CRect rcHeader( pMonth->GetHeaderRect() );
							ClientToScreen( &rcHeader );

							LPCTSTR lpszWndClass = 
								::AfxRegisterWndClass( 
									CS_SAVEBITS, 
									NULL, 
									NULL, 
									NULL 
									);
							if( !pHeaderPopupWnd->CreateEx(
									WS_EX_TOPMOST | WS_EX_WINDOWEDGE,
									lpszWndClass,
									_T(""),
									WS_POPUP,
									rcHeader.CenterPoint().x - sizeHeaderPopupWndClient.cx/2,
									rcHeader.CenterPoint().y - sizeHeaderPopupWndClient.cy/2,
									sizeHeaderPopupWnd.cx,
									sizeHeaderPopupWnd.cy,
									m_hWnd,
									NULL,
									NULL
									)
								)
							{
								ASSERT( FALSE );
								return true;
							}
							
							HWND hWndHdr = pHeaderPopupWnd->m_hWnd;
							ASSERT( hWndHdr != NULL && ::IsWindow(hWndHdr) );
							INT nMonth = 0, nYear = 0;
							pMonth->GetMonth( nMonth, nYear );
							pHeaderPopupWnd->SetInitialDate( 
								nMonth,
								nYear
								);
							
							pHeaderPopupWnd->SetWindowPos(
								NULL,
								0,0,0,0,
								SWP_SHOWWINDOW | SWP_NOZORDER | SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE
								);

							::SetCapture( hWndHdr );
							CWinThread * pThread = ::AfxGetThread();
							ASSERT_VALID( pThread );
							g_hWndHeaderPopup = hWndHdr;
							bool bAnalyzeDataChanging = false;
							for( bool bStopFlag = false; (!bStopFlag) && ::IsWindow(hWndHdr) ; )
							{
								MSG msg;
								// Process all the messages in the message queue
								while( ::IsWindow(hWndHdr) && PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) )
								{
									switch( msg.message )
									{
									case WM_LBUTTONUP:
									case WM_LBUTTONDOWN:
									case WM_LBUTTONDBLCLK:
									case WM_RBUTTONUP:
									case WM_RBUTTONDOWN:
									case WM_RBUTTONDBLCLK:
									case WM_MBUTTONUP:
									case WM_MBUTTONDOWN:
									case WM_MBUTTONDBLCLK:
									case WM_NCLBUTTONUP:
									case WM_NCLBUTTONDOWN:
									case WM_NCLBUTTONDBLCLK:
									case WM_NCRBUTTONUP:
									case WM_NCRBUTTONDOWN:
									case WM_NCRBUTTONDBLCLK:
									case WM_NCMBUTTONUP:
									case WM_NCMBUTTONDOWN:
									case WM_NCMBUTTONDBLCLK:
									case WM_MOUSEWHEEL:
									case WM_CONTEXTMENU:
										bStopFlag = true;
										if( msg.message == WM_LBUTTONUP )
											bAnalyzeDataChanging = true;
										//::PeekMessage(&msg,msg.hwnd,msg.message,msg.message,PM_REMOVE);
										break;
									case WM_MOUSEMOVE:
										if( ::IsWindow(hWndHdr) )
											pHeaderPopupWnd->AnalyzeChangings();
										else
											bStopFlag = true;
										//::PeekMessage(&msg,msg.hwnd,msg.message,msg.message,PM_REMOVE);
										break;
									case WM_CAPTURECHANGED:
										if( (HWND)msg.wParam != hWndHdr )
											bStopFlag = true;
										break;
									case WM_CANCELMODE:
									case WM_ACTIVATEAPP:
									case WM_SETTINGCHANGE:
									case WM_SYSCOLORCHANGE:
										bStopFlag = true;
										break;
									default:
										if( WM_KEYFIRST <= msg.message && msg.message <= WM_KEYLAST )
											bStopFlag = true;
										break;
									}
									if( ::GetCapture() != hWndHdr )
										bStopFlag = true;
									else if(
											(! CExtPopupMenuWnd::IsKeyPressed(VK_LBUTTON) )
										||	CExtPopupMenuWnd::IsKeyPressed(VK_MBUTTON)
										||	CExtPopupMenuWnd::IsKeyPressed(VK_RBUTTON)
										||	(	CExtPopupMenuWnd::IsMenuTracking()
											&&	(!(	( (GetStyle()&(WS_POPUP|WS_CHILD|WS_TABSTOP)) == WS_CHILD )
												&&	( GetParent()->IsKindOf( RUNTIME_CLASS(CExtPopupMenuWnd) ) )
												))
											)
										)
										bStopFlag = true;
									if( bStopFlag )
										break;
									ASSERT_VALID( pThread );
									if( ! pThread->PumpMessage() )
										bStopFlag = true;
								} // while( ::IsWindow(hWndHdr) && PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) )
								if( bStopFlag )
									break;
								if( ! ::WaitMessage() )
									break;
							} // for( bool bStopFlag = false; (!bStopFlag) && ::IsWindow(hWndHdr) ; )
							if( ::IsWindow(hWndHdr) )
							{
								if( ::GetCapture() == hWndHdr )
									::ReleaseCapture();
								if( bAnalyzeDataChanging )
								{
									pHeaderPopupWnd->AnalyzeChangings();
									INT	nSelMonth = 0, nSelYear = 0;
									if( pHeaderPopupWnd->GetSelectedDate(
											nSelMonth,
											nSelYear 
											)
										)
									{
										CSize sizeCalendarDimensionsCurrent;
										DimGet( 
											NULL,
											NULL,
											&sizeCalendarDimensionsCurrent
											);

										INT nMonthsDiff = 
											(pMonth->m_nRow)*(sizeCalendarDimensionsCurrent.cx) + (pMonth->m_nCol);
										
										for( INT nMonth=0; nMonth<nMonthsDiff; nMonth++ )
										{
											nSelMonth -= 1;
											if (nSelMonth < 1) 
											{
												nSelMonth = 12;
												nSelYear--;
											}
										}
										CurrentDateSet( nSelYear, nSelMonth, 1, true );
									}
								} // if( bAnalyzeDataChanging )
								::DestroyWindow( hWndHdr );
							} // if( ::IsWindow(hWndHdr) )
						} // if( bButtonPressed )
						CExtPopupMenuWnd::PassMsgLoop( false );
						g_hWndHeaderPopup = NULL;
					}
					break;					
				} // switch( nHitTestMonth )
			} // if( pMonth != NULL )
		} // case __EDPWH_INNER_AREA:
		break;
		
	case __EDPWH_BTN_NONE:
		if( bButtonPressed )
		{
			eMTT = __EMTT_BTN_NONE_PRESSED;
			bSetCapture = true;
		}
		else if( m_eMTT == __EMTT_BTN_NONE_PRESSED )
		{
			SelectionClear( true );
			HWND hWndNotificationReceiver =
				OnDatePickerGetNotificationReceiver();
			if( hWndNotificationReceiver != NULL )
			{
				SELECTION_NOTIFICATION _SN( m_pExternalSelectionInfo, *this, true );
				_SN.Notify( hWndNotificationReceiver );
				if( ! ::IsWindow(hWndOwn) )
					return true;
			} // if( hWndNotificationReceiver != NULL )
		}
		break;
		
	case __EDPWH_BTN_TODAY:
		if( bButtonPressed )
		{
			eMTT = __EMTT_BTN_TODAY_PRESSED;
			bSetCapture = true;
		}
		else if( m_eMTT == __EMTT_BTN_TODAY_PRESSED )
		{
			CurrentDateSet( OnDatePickerGetToday(), false );
			COleDateTime dt = CurrentDateGet();
			if( EnsureVisible( dt, false ) )
				SelectionSet( dt, dt, false, false );
			HWND hWndNotificationReceiver =
				OnDatePickerGetNotificationReceiver();
			if( hWndNotificationReceiver != NULL )
			{
				SELECTION_NOTIFICATION _SN( m_pExternalSelectionInfo, *this, true );
				_SN.Notify( hWndNotificationReceiver );
				if( ! ::IsWindow(hWndOwn) )
					return true;
			} // if( hWndNotificationReceiver != NULL )
		}
		break;

	} // switch( nHitTest )

	if( bSetCapture )
	{
		if( ::GetCapture() != GetSafeHwnd() )
			::SetCapture( GetSafeHwnd() );
	}
	else
	{
		if( ::GetCapture() == GetSafeHwnd() )
			::ReleaseCapture();
	}

	if( ! ::IsWindow(hWndOwn) )
		return true;

	if( eMTT != m_eMTT )
	{
		bool bAnimationNeeded = 
			(	eMTT != __EMTT_SCROLLING_BACKWARD 
			&&	eMTT != __EMTT_SCROLLING_FORWARD
			&&	eMTT != __EMTT_DAYS_SELECTING
			) ? true : false;
		bool bAnimationLocked = AnimationClient_CacheGeneratorIsLocked();
		INT eAPT = 
			(	(	( eMTT == __EMTT_BTN_NONE_PRESSED && m_eMTT != __EMTT_BTN_NONE_PRESSED )
				||	( eMTT == __EMTT_BTN_TODAY_PRESSED && m_eMTT != __EMTT_BTN_TODAY_PRESSED )
				)
				&&	(	eMTT == __EMTT_BTN_NONE_PRESSED 
					||	eMTT == __EMTT_BTN_TODAY_PRESSED 
					)
			)
			? __EAPT_BY_PRESSED_STATE_TURNED_ON 
			: __EAPT_BY_PRESSED_STATE_TURNED_OFF;

		if(		(!bAnimationLocked)
			&&	bAnimationNeeded
			)
		{
			AnimationClient_CacheGeneratorLock();
 			AnimationClient_CacheNextStateMinInfo( false, eAPT );
		}

		m_eMTT = eMTT;

		if(		(!bAnimationLocked)
			&&	bAnimationNeeded
			)
		{
 			AnimationClient_CacheNextStateMinInfo( true, eAPT );
			AnimationClient_CacheGeneratorUnlock();
		}

		Invalidate();
	}

	if(		m_eMTT == __EMTT_SCROLLING_BACKWARD 
		||	m_eMTT == __EMTT_SCROLLING_FORWARD 
		)
		OnDatePickerDoScroll(
			1,
			true
			);

	return true;
}

LONG CExtDatePickerWnd::HitTest(
	const POINT & ptClient,
	COleDateTime * pDT // = NULL
	) const
{
	ASSERT_VALID( this );
	if( pDT != NULL )
		pDT->SetStatus( COleDateTime::null );
	if( GetSafeHwnd() == NULL )
		return __EDPWH_NOWHERE;
CRect rcClient;
	GetClientRect( &rcClient );
	if(		rcClient.IsRectEmpty()
		||	(! rcClient.PtInRect(ptClient) )
		)
		return __EDPWH_NOWHERE;
	if(		(! m_rcBtnNone.IsRectEmpty() )
		&&	m_rcBtnNone.PtInRect(ptClient)
		)
		return __EDPWH_BTN_NONE;
	if(		(! m_rcBtnToday.IsRectEmpty() )
		&&	m_rcBtnToday.PtInRect(ptClient)
		)
		return __EDPWH_BTN_TODAY;
	if(		(! m_rcDatePickerInnerArea.IsRectEmpty() )
		&&	m_rcDatePickerInnerArea.PtInRect(ptClient)
		)
	{
		if( pDT == NULL )
			return __EDPWH_INNER_AREA;
		const MONTH_INFO * pMonth = _HitTestMonth( ptClient );
		if( pMonth == NULL )
			return __EDPWH_INNER_AREA;
		LONG nHitTestMonth = pMonth->HitTest( ptClient, pDT );
		if( nHitTestMonth == __EDPWH_NOWHERE )
			return __EDPWH_INNER_AREA;
		return nHitTestMonth;
	}
	return __EDPWH_NOWHERE;
}

void CExtDatePickerWnd::OnDatePickerDoScroll( 
	INT nMonthCount, // = 1,
	bool bSmoothScroll // = true
	)
{
	ASSERT_VALID( this );
COleDateTime dtCurrentDate = CurrentDateGet();
INT nCurrentMonth = dtCurrentDate.GetMonth();
INT nCurrentYear = dtCurrentDate.GetYear();
	if( m_eMTT == __EMTT_SCROLLING_BACKWARD )
	{ 
		nCurrentMonth -= nMonthCount;
		if( nCurrentMonth < 1 )
		{
			nCurrentMonth= 12;
			nCurrentYear--;
		}
		if( nCurrentYear >= __EXT_DATE_YEAR_MIN )
		{
			CurrentDateSet( nCurrentYear, nCurrentMonth, 1, true );
			if( bSmoothScroll )	
			{
				::Sleep( 2 * __EDPW_SCROLL_TIMER_PERIOD );
				SetTimer(
					__EDPW_SCROLL_TIMER_ID,
					__EDPW_SCROLL_TIMER_PERIOD,
					NULL
					);
			}
		}
	}
	else if( m_eMTT == __EMTT_SCROLLING_FORWARD )
	{ 
		nCurrentMonth += nMonthCount;
		if( nCurrentMonth > 12 )
		{
			nCurrentMonth = 1;
			nCurrentYear++;
		}

		CSize szDimensions;
		DimGet( 
			NULL,
			NULL,
			&szDimensions
			);

		INT nMonth = dtCurrentDate.GetMonth();
		INT nYear = dtCurrentDate.GetYear();
		for( INT i = 0; i < (szDimensions.cx * szDimensions.cy - 1 + nMonthCount); i++ )
		{
			nMonth++;
			if( nMonth > 12 )
			{
				nMonth = 1;
				nYear++;
			}
		}
		if(		(nYear < __EXT_DATE_YEAR_MAX && nMonth <= 12)
			||	(nYear == __EXT_DATE_YEAR_MAX && nMonth <= 12)
			)
		{
			CurrentDateSet( nCurrentYear, nCurrentMonth, 1, true );
			if( bSmoothScroll ) 
			{
				::Sleep( 2 * __EDPW_SCROLL_TIMER_PERIOD );
				SetTimer(
					__EDPW_SCROLL_TIMER_ID,
					__EDPW_SCROLL_TIMER_PERIOD,
					NULL
					);
			}
		}
	}
}

void CExtDatePickerWnd::OnTimer(__EXT_MFC_UINT_PTR nIDEvent) 
{
	if( nIDEvent == __EDPW_SCROLL_TIMER_ID )
	{
		if( m_eMTT == __EMTT_NOTHING )
			KillTimer( __EDPW_SCROLL_TIMER_ID );
		else
			OnDatePickerDoScroll(
				1,
				false
				);
	}

	if( AnimationSite_OnHookTimer( (UINT)nIDEvent ) )
		return;

	CWnd::OnTimer(nIDEvent);
}

void CExtDatePickerWnd::OnCancelMode() 
{
	CWnd::OnCancelMode();
	_CancelActions();
}

void CExtDatePickerWnd::OnCaptureChanged(CWnd *pWnd) 
{
	CWnd::OnCaptureChanged(pWnd);
	if(		m_eMTT != __EMTT_NOTHING
		&&	::GetCapture() != m_hWnd
		)
		_CancelActions();
}

UINT CExtDatePickerWnd::OnGetDlgCode() 
{
	if( (GetStyle() & WS_TABSTOP) != 0 )
		return DLGC_WANTALLKEYS;
	return CWnd::OnGetDlgCode();
}

void CExtDatePickerWnd::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	nRepCnt;
	nFlags;
bool bPopupMode = false;
	if( GetParent()->IsKindOf( RUNTIME_CLASS(CExtPopupMenuWnd) ) )
		bPopupMode = true;
COleDateTime dtSelBegin, dtSelEnd;
	SelectionGet( dtSelBegin, dtSelEnd );
	if( bPopupMode )
	{
		if( dtSelBegin.GetStatus() == COleDateTime::null )
			dtSelBegin = COleDateTime::GetCurrentTime();
		if( dtSelEnd.GetStatus() == COleDateTime::null )
			dtSelEnd = COleDateTime::GetCurrentTime();
	} // if( bPopupMode )

	if(		dtSelBegin.GetStatus() == COleDateTime::null
		&&	dtSelEnd.GetStatus() == COleDateTime::null 
		)
		dtSelBegin = dtSelEnd = COleDateTime::GetCurrentTime();

DWORD dwDatePickerStyle = GetDatePickerStyle();
bool bEnableMultipeSelect =
		((dwDatePickerStyle & __EDPWS_MULTIPLE_SELECTION) != 0 )
			? true : false;
	if( bEnableMultipeSelect )
	{
		bool bAlt = ( (::GetAsyncKeyState(VK_MENU)&0x8000) != 0 ) ? true : false;
		bool bCtrl = ( (::GetAsyncKeyState(VK_CONTROL)&0x8000) != 0 ) ? true : false;
		bool bShift = ( (::GetAsyncKeyState(VK_SHIFT)&0x8000) != 0 ) ? true : false;
		bool bShiftOnly = (!bCtrl) && (!bAlt) && bShift;
		if( ! bShiftOnly )
			bEnableMultipeSelect = false;
	} // if( bEnableMultipeSelect )
	switch( nChar )
	{
	case VK_LEFT:
		dtSelEnd -= 1;
		if( ! bEnableMultipeSelect )
			dtSelBegin = dtSelEnd;
		break;
	case VK_RIGHT:
		dtSelEnd += 1;
		if( ! bEnableMultipeSelect )
			dtSelBegin = dtSelEnd;
		break;
	case VK_UP:
		dtSelEnd -= 7;
		if( ! bEnableMultipeSelect )
			dtSelBegin = dtSelEnd;
		break;
	case VK_DOWN:
		dtSelEnd += 7;
		if( ! bEnableMultipeSelect )
			dtSelBegin = dtSelEnd;
		break;
	case VK_TAB:
		if( (GetStyle() & WS_TABSTOP) != 0 )
		{
			HWND hWndParent = ::GetParent(m_hWnd);
			if( hWndParent != NULL )
			{
				bool bShift = ( (::GetAsyncKeyState(VK_SHIFT)&0x8000) != 0 ) ? true : false;
				::SendMessage(
					hWndParent,
					WM_NEXTDLGCTL,
					bShift ? (!0) : 0,
					0
					);
			}
		} // if( (GetStyle() & WS_TABSTOP) != 0 )
		return;
	case VK_RETURN:
		if( m_pExternalSelectionInfo == NULL )
			bPopupMode = false;
		else
			SelectionGet(
				m_pExternalSelectionInfo->m_dtBegin,
				m_pExternalSelectionInfo->m_dtEnd
				);
		GetParent()->PostMessage( WM_CANCELMODE );
		break;
	case VK_ESCAPE:
		if( ! bPopupMode )
			return;
		GetParent()->PostMessage( WM_CANCELMODE );
		break;
	default:
		return;
	} // switch( nChar )

	if( dtSelEnd.GetStatus() != COleDateTime::valid )
		return;
	SelectionSet( dtSelBegin, dtSelEnd, false, false );
//	CurrentDateSet( dtSelEnd, false );
	EnsureVisible( dtSelEnd, false );
	Invalidate();

HWND hWndNotificationReceiver =
		OnDatePickerGetNotificationReceiver();
	if( hWndNotificationReceiver != NULL )
	{
		HWND hWndOwn = GetSafeHwnd();
		SELECTION_NOTIFICATION _SN(
			m_pExternalSelectionInfo,
			*this,
			bPopupMode ? false : true // true
			);
		_SN.Notify( hWndNotificationReceiver );
		if( ! ::IsWindow(hWndOwn) )
			return;
	} // if( hWndNotificationReceiver != NULL )
}

void CExtDatePickerWnd::OnSize(UINT nType, int cx, int cy) 
{
	ASSERT_VALID( this );

	AnimationSite_ClientProgressStop( this );
	AnimationClient_StateGet( false ).Empty();
	AnimationClient_StateGet( true ).Empty();
CRect rcClient;
	GetClientRect( &rcClient );
	AnimationClient_TargetRectSet( rcClient );

	CWnd::OnSize(nType, cx, cy);
	UpdateDatePickerWnd( true, true, true );
}

void CExtDatePickerWnd::OnKillFocus(CWnd* pNewWnd) 
{
	CWnd::OnKillFocus(pNewWnd);
	if( (GetStyle() & WS_TABSTOP) != 0 )
		_CancelActions();
}

void CExtDatePickerWnd::_CancelActions()
{
	ASSERT_VALID( this );

	if( m_bCanceling )
		return;
	m_bCanceling = true;

	KillTimer( __EDPW_SCROLL_TIMER_ID );
	KillTimer( __EDPW_SCROLL_MONTH_LIST_UP_TIMER_ID );
	KillTimer( __EDPW_SCROLL_MONTH_LIST_DOWN_TIMER_ID );

	if( m_eMTT != __EMTT_NOTHING )
	{
		bool bExitingPushedState = 
			(	m_eMTT == __EMTT_BTN_NONE_PRESSED
			||	m_eMTT == __EMTT_BTN_TODAY_PRESSED
			) 
			? true : false;
		bool bAnimationLocked = AnimationClient_CacheGeneratorIsLocked();
		if( ! bAnimationLocked )
		{
			AnimationClient_CacheGeneratorLock();
 			AnimationClient_CacheNextStateMinInfo(
				false,
				bExitingPushedState 
					? __EAPT_BY_PRESSED_STATE_TURNED_OFF 
					: __EAPT_BY_HOVERED_STATE_TURNED_OFF
				);
		}

		// cancel any kind of mouse tracking
		m_eMTT = __EMTT_NOTHING;

		if( ! bAnimationLocked )
		{
 			AnimationClient_CacheNextStateMinInfo(
				true,
				bExitingPushedState 
					? __EAPT_BY_PRESSED_STATE_TURNED_OFF 
					: __EAPT_BY_HOVERED_STATE_TURNED_OFF
				);
			AnimationClient_CacheGeneratorUnlock();
		}

		Invalidate();
	}
	
	if( ::GetCapture() == m_hWnd )
		::ReleaseCapture();

	m_bCanceling = false;
}

#if _MFC_VER < 0x700
void CExtDatePickerWnd::OnActivateApp(BOOL bActive, HTASK hTask) 
#else
void CExtDatePickerWnd::OnActivateApp(BOOL bActive, DWORD hTask) 
#endif
{
	CWnd::OnActivateApp(bActive, hTask);
	if( ! bActive )
		_CancelActions();
}

int CExtDatePickerWnd::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message) 
{
	if( (GetStyle()&WS_TABSTOP) == 0 )
		return MA_NOACTIVATE;
	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

HWND CExtDatePickerWnd::AnimationSite_GetSafeHWND() const
{
__PROF_UIS_MANAGE_STATE;
HWND hWnd = GetSafeHwnd();
	return hWnd;
}

const CExtAnimationParameters *
	CExtDatePickerWnd::AnimationClient_OnQueryAnimationParameters(
		INT eAPT // __EAPT_*** animation type
		) const
{
	ASSERT_VALID( this );
const CExtAnimationParameters * pAnimationParameters =
		PmBridge_GetPM()->Animation_GetParameters(
			eAPT,
			(CObject*)this,
			this
			);
	return pAnimationParameters;
}

bool CExtDatePickerWnd::AnimationClient_CacheNextState(
	CDC & dc,
	const RECT & rcAcAnimationTarget,
	bool bAnimate,
	INT eAPT // __EAPT_*** animation type
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&dc) );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( AnimationClient_CacheGeneratorIsLocked() );

	OnDatePickerDrawEntire( dc, &rcAcAnimationTarget );

	return
		CExtAnimationSingleton::AnimationClient_CacheNextState(
			dc,
			rcAcAnimationTarget,
			bAnimate,
			eAPT
			);
}

void AFXAPI __EXT_MFC_DDX_DatePickerWnd( CDataExchange * pDX, INT nIDC, CTime & value1, CTime & value2 )
{
HWND hWndCtrl = pDX->PrepareCtrl( nIDC );
	ASSERT( hWndCtrl != NULL );
	ASSERT( ::IsWindow( hWndCtrl ) );
CExtDatePickerWnd * pWnd = 
		DYNAMIC_DOWNCAST( CExtDatePickerWnd, CWnd::FromHandle( hWndCtrl ) );
	if( pWnd != NULL )
	{
		ASSERT_VALID( pWnd );
		SYSTEMTIME sys_time1, sys_time2;
		::memset( &sys_time1, 0, sizeof(SYSTEMTIME) );
		::memset( &sys_time2, 0, sizeof(SYSTEMTIME) );
		if( pDX->m_bSaveAndValidate )
		{
			COleDateTime dtBegin, dtEnd;
			pWnd->SelectionGet( dtBegin, dtEnd );
			dtBegin.GetAsSystemTime( sys_time1 );
			dtEnd.GetAsSystemTime( sys_time2 );
			value1 = CTime( sys_time1 );
			value2 = CTime( sys_time2 );
		}
		else
		{
			if(		value1.GetAsSystemTime( sys_time1 )
				&&	value2.GetAsSystemTime( sys_time2 )
				)
				pWnd->SelectionSet( sys_time1, sys_time2, false, true );
		}
	}
}

void AFXAPI __EXT_MFC_DDX_DatePickerWnd( CDataExchange * pDX, INT nIDC, COleDateTime & value1, COleDateTime & value2 )
{
HWND hWndCtrl = pDX->PrepareCtrl( nIDC );
	ASSERT( hWndCtrl != NULL );
	ASSERT( ::IsWindow( hWndCtrl ) );
CExtDatePickerWnd * pWnd = 
		DYNAMIC_DOWNCAST( CExtDatePickerWnd, CWnd::FromHandle( hWndCtrl ) );
	if( pWnd != NULL )
	{
		ASSERT_VALID( pWnd );
		if( pDX->m_bSaveAndValidate )
			pWnd->SelectionGet( value1, value2 );
		else
			pWnd->SelectionSet( value1, value2, false, true );
	}
}

void AFXAPI __EXT_MFC_DDX_DatePickerWnd( CDataExchange * pDX, INT nIDC, CTime & value )
{
CTime value2 = value;
	__EXT_MFC_DDX_DatePickerWnd( pDX, nIDC, value, value2 );
}

void AFXAPI __EXT_MFC_DDX_DatePickerWnd( CDataExchange * pDX, INT nIDC, COleDateTime & value )
{
COleDateTime value2 = value;
	__EXT_MFC_DDX_DatePickerWnd( pDX, nIDC, value, value2 );
}

#endif // (!defined __EXT_MFC_NO_DATE_PICKER)

#if (!defined __EXT_MFC_NO_DATE_BROWSER)

/////////////////////////////////////////////////////////////////////////////
// CExtDateBrowserWnd

IMPLEMENT_DYNCREATE( CExtDateBrowserWnd, CWnd );
IMPLEMENT_CExtPmBridge_MEMBERS_GENERIC( CExtDateBrowserWnd );

// #define INTERNAL_TRACE_TIME( _caption_, _ole_time_ ) \
// 	{ \
// 		CExtSafeString s = _ole_time_.Format(); \
// 		afxDump << LPCTSTR(_caption_); \
// 		afxDump << LPCTSTR(s); \
// 		afxDump << "\n"; \
// 	}

CExtDateBrowserWnd::CExtDateBrowserWnd()
	: m_bDirectCreateCall( false )
	, m_eVM( CExtDateBrowserWnd::__EVM_MONTH )
	, m_clrBackground( RGB(255,255,255) )
	, m_clrMonthSeparator( RGB(245,245,245) )
	, m_timeCurrent( COleDateTime::GetCurrentTime() )

 	, m_timeMin( 1900,  1,  1,  0,  0,  0 )
 	, m_timeMax( 2099, 12, 31, 23, 59, 59 )

// 	, m_timeMin( 2007,  1,  5,  0,  0,  0 )
// 	, m_timeMax( 2009,  1, 15, 23, 59, 59 )

	, m_bPressedTracking( false )
	, m_bPressedLeftRightButton( false )
	, m_nScrollingDirection( 0 )
	, m_nMonthSeparatorHeight( 1 )
	, m_nTimerID( 987 )
	, m_nTimerEllapse( 250 )
	, m_ePS( CExtDateBrowserWnd::__EPST_SIMPLE )
	, m_rcFocusRectPadding( 1, 1, 1, 1 )
	, m_nAnimationStepCountZoom( __EXT_DATE_BROWSER_ANIMATION_ZOOM_STEP_COUNT )
	, m_nAnimationStepTimeZoom( __EXT_DATE_BROWSER_ANIMATION_ZOOM_STEP_TIME )
	, m_nAnimationStepCountScroll( __EXT_DATE_BROWSER_ANIMATION_SCROLL_STEP_COUNT )
	, m_nAnimationStepTimeScroll( __EXT_DATE_BROWSER_ANIMATION_SCROLL_STEP_TIME )
	, m_nFirstDayOfWeek( -1 ) // 0-Sun, 1-Mon, ..., -1 - get from locale
	, m_bShowItemsAfterCentury( true )
	, m_bShowItemsBeforeCentury( true )
	, m_bShowItemsAfterYearDecade( true )
	, m_bShowItemsBeforeYearDecade( true )
	, m_bShowDaysAfterMonth( true )
	, m_bShowDaysBeforeMonth( true )
	, m_bShowOutOfRangeItems( false )
{
	VERIFY( RegisterDateBrowserWndClass() );

	VERIFY( m_iconBtnPrevious.m_bmpNormal.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBB_LEFT_NORMAL) ) );
	VERIFY( m_iconBtnPrevious.m_bmpHover.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBB_LEFT_HOVER) ) );
	VERIFY( m_iconBtnPrevious.m_bmpDisabled.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBB_LEFT_DISABLED) ) );
	VERIFY( m_iconBtnPrevious.m_bmpPressed.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBB_LEFT_NORMAL) ) );
	m_iconBtnPrevious.m_dwFlags = __EXT_ICON_PERSISTENT_BITMAP_DISABLED|__EXT_ICON_PERSISTENT_BITMAP_HOVER|__EXT_ICON_PERSISTENT_BITMAP_PRESSED;

	VERIFY( m_iconBtnNext.m_bmpNormal.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBB_RIGHT_NORMAL) ) );
	VERIFY( m_iconBtnNext.m_bmpHover.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBB_RIGHT_HOVER) ) );
	VERIFY( m_iconBtnNext.m_bmpDisabled.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBB_RIGHT_DISABLED) ) );
	VERIFY( m_iconBtnNext.m_bmpPressed.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBB_RIGHT_NORMAL) ) );
	m_iconBtnNext.m_dwFlags = __EXT_ICON_PERSISTENT_BITMAP_DISABLED|__EXT_ICON_PERSISTENT_BITMAP_HOVER|__EXT_ICON_PERSISTENT_BITMAP_PRESSED;

	m_skin.LoadVistaSkin();

INT nSlice, nColorIndex;
	for( nSlice = 0; nSlice < 4; nSlice++ )
	{
		for( nColorIndex = 0; nColorIndex < INT(__EIST_COUNT); nColorIndex++ )
		{
			m_arrColorsItemText[nSlice][nColorIndex] = COLORREF(-1L);
			m_arrColorsItemBackground[nSlice][nColorIndex] = COLORREF(-1L);
		}
	}

	PmBridge_Install();
}

CExtDateBrowserWnd::~CExtDateBrowserWnd()
{
	PmBridge_Uninstall();
}

HWND CExtDateBrowserWnd::PmBridge_GetSafeHwnd() const
{
__PROF_UIS_MANAGE_STATE;
HWND hWnd = GetSafeHwnd();
	return hWnd;
}

void CExtDateBrowserWnd::PmBridge_OnPaintManagerChanged(
	CExtPaintManager * pGlobalPM
	)
{
	if( GetSafeHwnd() != NULL )
		Invalidate();
	CExtPmBridge::PmBridge_OnPaintManagerChanged( pGlobalPM );
}


BEGIN_MESSAGE_MAP(CExtDateBrowserWnd, CWnd)
	//{{AFX_MSG_MAP(CExtDateBrowserWnd)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_CANCELMODE()
	ON_WM_KEYDOWN()
	ON_WM_GETDLGCODE()
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_WM_MOUSEACTIVATE()
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
	ON_WM_SYSCOLORCHANGE()
	__EXT_MFC_SAFE_ON_WM_SETTINGCHANGE()
	ON_MESSAGE( __ExtMfc_WM_THEMECHANGED, OnThemeChanged )
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CExtDateBrowserWnd message handlers

BOOL CExtDateBrowserWnd::OnEraseBkgnd(CDC* pDC) 
{
	ASSERT_VALID( this );
	pDC;
	return TRUE;
}

void CExtDateBrowserWnd::OnPaint() 
{
	ASSERT_VALID( this );
CPaintDC dcPaint( this );
CExtMemoryDC dc( &dcPaint );
CRect rcEntire;
	GetClientRect( &rcEntire );
COLORREF clrBackground = OnDateBrowserGetBackgroundColor();
	dc.FillSolidRect( &rcEntire, clrBackground );
bool bWindowFocused = OnDateBrowserQueryWindowFocusedState();
e_view_mode_t eVM = ViewModeGet();
COleDateTime _time = TimeGet();
	OnDateBrowserPaintEntire(
		dc,
		_time,
		eVM,
		rcEntire,
		bWindowFocused
		);
}

void CExtDateBrowserWnd::OnDateBrowserPaintEntire(
	CDC & dc,
	const COleDateTime & _time,
	CExtDateBrowserWnd::e_view_mode_t eVM,
	CRect rcEntire,
	bool bWindowFocused
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );

	switch( eVM )
	{
	case __EVM_CENTURY:
		OnDateBrowserPaintEntireCentury(
			dc,
			_time,
			rcEntire,
			bWindowFocused
			);
	break;
	case __EVM_YEAR_DECADE:
		OnDateBrowserPaintEntireYearDecade(
			dc,
			_time,
			rcEntire,
			bWindowFocused
			);
	break;
	case __EVM_ONE_YEAR:
		OnDateBrowserPaintEntireOneYear(
			dc,
			_time,
			rcEntire,
			bWindowFocused
			);
	break;
	case __EVM_MONTH:
		OnDateBrowserPaintEntireMonth(
			dc,
			_time,
			rcEntire,
			bWindowFocused
			);
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( eVM )
}

void CExtDateBrowserWnd::OnDateBrowserPaintCaption(
	CDC & dc,
	CExtDateBrowserWnd::e_view_mode_t _eVM,
	__EXT_MFC_SAFE_LPCTSTR strText,
	CRect rcItem,
	CExtDateBrowserWnd::e_item_state_t eIST,
	CExtDateBrowserWnd::e_painted_item_type_t ePIT,
	bool bWindowFocused
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );

	if( strText == NULL )
		return;
INT nTextLen = (INT)_tcslen( strText );
	if( nTextLen == 0 )
		return;

COLORREF clrText = OnDateBrowserQueryCaptionTextColor( _eVM, eIST, ePIT, bWindowFocused );
COLORREF clrBack = OnDateBrowserQueryCaptionBackgroundColor( _eVM, eIST, ePIT, bWindowFocused );
	if( clrBack != COLORREF(-1L) )
		dc.FillSolidRect( &rcItem, clrBack );
COLORREF clrOldTextColor = dc.SetTextColor( clrText );

CRect rcPadding = OnDateBrowserQueryCaptionPadding();
CRect rcCaption = rcItem;
	rcCaption.DeflateRect( &rcPadding );
CFont * pFont = OnDateBrowserQueryCaptionFont( _eVM );
CFont * pOldFont = dc.SelectObject( pFont );
INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
	CExtRichContentLayout::stat_DrawText(
		dc.m_hDC,
		strText, nTextLen,
		&rcCaption,
		DT_CENTER|DT_VCENTER|DT_SINGLELINE, 0
		);
	dc.SetBkMode( nOldBkMode );
	dc.SelectObject( pOldFont );
	dc.SetTextColor( clrOldTextColor );
}

void CExtDateBrowserWnd::OnDateBrowserPaintCenturyItem(
	CDC & dc,
	__EXT_MFC_SAFE_LPCTSTR strText,
	CRect rcItem,
	CExtDateBrowserWnd::e_item_state_t eIST,
	CExtDateBrowserWnd::e_painted_item_type_t ePIT,
	bool bWindowFocused,
	bool bToday
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( strText == NULL )
		return;
INT nTextLen = (INT)_tcslen( strText );
	if( nTextLen == 0 )
		return;
CRect rcPadding = OnDateBrowserQueryCenturyItemPadding();
bool bDrawFocusRect = false;
CFont * pFont = OnDateBrowserQueryItemFont( __EVM_CENTURY );
	switch( PaintStyleGet() )
	{
	case __EPST_PAINT_MANAGER:
		switch( eIST )
		{
		case __EIST_SELECTED_NORMAL:
		case __EIST_SELECTED_HOVER:
		case __EIST_SELECTED_PRESSED:
			bDrawFocusRect = bWindowFocused;
			// continue falling
		case __EIST_HOVER:
		case __EIST_PRESSED:
		{
			CExtPaintManager::PAINTPUSHBUTTONDATA _ppbd(
				this,
				true,
				rcItem,
				(LPCTSTR)strText,
				NULL,
				true,
				( eIST == __EIST_SELECTED_HOVER || eIST == __EIST_HOVER ) ? true : false,
				( eIST == __EIST_SELECTED_PRESSED || eIST == __EIST_PRESSED ) ? true : false,
				false,
				true,
				true,
				false, // bDrawFocusRect
				false,
				CExtPaintManager::__ALIGN_HORIZ_LEFT|CExtPaintManager::__ALIGN_VERT_TOP,
				(HFONT)pFont->GetSafeHandle(), 
				false, 
				0, 
				true
				);
			_ppbd.m_bWordBreak = true;
			_ppbd.m_rcBorderSizes = rcPadding;
			CExtMemoryDC dc2( &dc, &rcItem );	//must edit
			PmBridge_GetPM()->PaintPushButton( dc2, _ppbd );
			if(		bDrawFocusRect
				&&	m_rcFocusRectPadding.left >= 0
				&&	m_rcFocusRectPadding.right >= 0
				&&	m_rcFocusRectPadding.top >= 0
				&&	m_rcFocusRectPadding.bottom >= 0
				)
			{
				CRect rcFR = rcItem;
				//rcFR.DeflateRect( &m_rcFocusRectPadding );
				rcFR.DeflateRect( 3, 3 );
				COLORREF clrTextOld = 
					dc2.SetTextColor( RGB(255,255,255) );
				COLORREF clrBkOld =
					dc2.SetBkColor( RGB(0,0,0) );
				dc2.DrawFocusRect( &rcFR );
				dc2.SetBkColor( clrBkOld );
				dc2.SetTextColor( clrTextOld );
			}
		}
		return;
		} // switch( eIST )
	break;
	case __EPST_SKIN:
		m_skin.PaintItemBackground( dc, rcItem, eIST, ePIT, bWindowFocused, bToday );
	break;
	case __EPST_SIMPLE:
	{
		COLORREF clrBack = OnDateBrowserQueryPaletteItemBackgroundColor( __EVM_CENTURY, eIST, ePIT, bWindowFocused, bToday );
		if( clrBack != COLORREF(-1L) )
			dc.FillSolidRect( &rcItem, clrBack );
		if(		eIST == __EIST_SELECTED_NORMAL
			||	eIST == __EIST_SELECTED_HOVER
			||	eIST == __EIST_SELECTED_PRESSED
			)
			bDrawFocusRect = bWindowFocused;
	}
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	}
COLORREF clrText = OnDateBrowserQueryPaletteItemTextColor( __EVM_CENTURY, eIST, ePIT, bWindowFocused, bToday );
COLORREF clrOldTextColor = dc.SetTextColor( clrText );

CRect rcText = rcItem;
	rcText.DeflateRect( &rcPadding );
CFont * pOldFont = dc.SelectObject( pFont );
INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
	CExtRichContentLayout::stat_DrawText(
		dc.m_hDC,
		strText, nTextLen,
		&rcText,
		DT_LEFT|DT_TOP, 0
		);
	dc.SetBkMode( nOldBkMode );
	dc.SelectObject( pOldFont );
	dc.SetTextColor( clrOldTextColor );

	if(		bDrawFocusRect
		&&	m_rcFocusRectPadding.left >= 0
		&&	m_rcFocusRectPadding.right >= 0
		&&	m_rcFocusRectPadding.top >= 0
		&&	m_rcFocusRectPadding.bottom >= 0
		)
	{
		CRect rcFR = rcItem;
		rcFR.DeflateRect( &m_rcFocusRectPadding );
		COLORREF clrTextOld = 
			dc.SetTextColor( RGB(255,255,255) );
		COLORREF clrBkOld =
			dc.SetBkColor( RGB(0,0,0) );
		dc.DrawFocusRect( &rcFR );
		dc.SetBkColor( clrBkOld );
		dc.SetTextColor( clrTextOld );
	}
}

void CExtDateBrowserWnd::OnDateBrowserPaintYearDecadeItem(
	CDC & dc,
	__EXT_MFC_SAFE_LPCTSTR strText,
	CRect rcItem,
	CExtDateBrowserWnd::e_item_state_t eIST,
	CExtDateBrowserWnd::e_painted_item_type_t ePIT,
	bool bWindowFocused,
	bool bToday
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( strText == NULL )
		return;
INT nTextLen = (INT)_tcslen( strText );
	if( nTextLen == 0 )
		return;
CRect rcPadding = OnDateBrowserQueryYearDecadeItemPadding();
bool bDrawFocusRect = false;
CFont * pFont = OnDateBrowserQueryItemFont( __EVM_YEAR_DECADE );
	switch( PaintStyleGet() )
	{
	case __EPST_PAINT_MANAGER:
		switch( eIST )
		{
		case __EIST_SELECTED_NORMAL:
		case __EIST_SELECTED_HOVER:
		case __EIST_SELECTED_PRESSED:
			bDrawFocusRect = bWindowFocused;
			// continue falling
		case __EIST_HOVER:
		case __EIST_PRESSED:
		{
			CExtPaintManager::PAINTPUSHBUTTONDATA _ppbd(
				this,
				true,
				rcItem,
				(LPCTSTR)strText,
				NULL,
				true,
				( eIST == __EIST_SELECTED_HOVER || eIST == __EIST_HOVER ) ? true : false,
				( eIST == __EIST_SELECTED_PRESSED || eIST == __EIST_PRESSED ) ? true : false,
				false,
				true,
				true,
				false, // bDrawFocusRect
				false,
				CExtPaintManager::__ALIGN_HORIZ_CENTER|CExtPaintManager::__ALIGN_VERT_CENTER,
				(HFONT)pFont->GetSafeHandle(), 
				false, 
				0, 
				true
				);
			_ppbd.m_bWordBreak = true;
			_ppbd.m_rcBorderSizes = rcPadding;
			CExtMemoryDC dc2( &dc, &rcItem );	//must edit
			PmBridge_GetPM()->PaintPushButton( dc2, _ppbd );
			if(		bDrawFocusRect
				&&	m_rcFocusRectPadding.left >= 0
				&&	m_rcFocusRectPadding.right >= 0
				&&	m_rcFocusRectPadding.top >= 0
				&&	m_rcFocusRectPadding.bottom >= 0
				)
			{
				CRect rcFR = rcItem;
				//rcFR.DeflateRect( &m_rcFocusRectPadding );
				rcFR.DeflateRect( 3, 3 );
				COLORREF clrTextOld = 
					dc2.SetTextColor( RGB(255,255,255) );
				COLORREF clrBkOld =
					dc2.SetBkColor( RGB(0,0,0) );
				dc2.DrawFocusRect( &rcFR );
				dc2.SetBkColor( clrBkOld );
				dc2.SetTextColor( clrTextOld );
			}
		}
		return;
		} // switch( eIST )
	break;
	case __EPST_SKIN:
		m_skin.PaintItemBackground( dc, rcItem, eIST, ePIT, bWindowFocused, bToday );
	break;
	case __EPST_SIMPLE:
	{
		COLORREF clrBack = OnDateBrowserQueryPaletteItemBackgroundColor( __EVM_YEAR_DECADE, eIST, ePIT, bWindowFocused, bToday );
		if( clrBack != COLORREF(-1L) )
			dc.FillSolidRect( &rcItem, clrBack );
		if(		eIST == __EIST_SELECTED_NORMAL
			||	eIST == __EIST_SELECTED_HOVER
			||	eIST == __EIST_SELECTED_PRESSED
			)
			bDrawFocusRect = bWindowFocused;
	}
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	}
COLORREF clrText = OnDateBrowserQueryPaletteItemTextColor( __EVM_YEAR_DECADE, eIST, ePIT, bWindowFocused, bToday );
COLORREF clrOldTextColor = dc.SetTextColor( clrText );

CRect rcText = rcItem;
	rcText.DeflateRect( &rcPadding );
CFont * pOldFont = dc.SelectObject( pFont );
INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
	CExtRichContentLayout::stat_DrawText(
		dc.m_hDC,
		strText, nTextLen,
		&rcText,
		DT_CENTER|DT_VCENTER, 0
		);
	dc.SetBkMode( nOldBkMode );
	dc.SelectObject( pOldFont );
	dc.SetTextColor( clrOldTextColor );

	if(		bDrawFocusRect
		&&	m_rcFocusRectPadding.left >= 0
		&&	m_rcFocusRectPadding.right >= 0
		&&	m_rcFocusRectPadding.top >= 0
		&&	m_rcFocusRectPadding.bottom >= 0
		)
	{
		CRect rcFR = rcItem;
		rcFR.DeflateRect( &m_rcFocusRectPadding );
		COLORREF clrTextOld = 
			dc.SetTextColor( RGB(255,255,255) );
		COLORREF clrBkOld =
			dc.SetBkColor( RGB(0,0,0) );
		dc.DrawFocusRect( &rcFR );
		dc.SetBkColor( clrBkOld );
		dc.SetTextColor( clrTextOld );
	}
}

void CExtDateBrowserWnd::OnDateBrowserPaintOneYearItem(
	CDC & dc,
	__EXT_MFC_SAFE_LPCTSTR strText,
	CRect rcItem,
	CExtDateBrowserWnd::e_item_state_t eIST,
	CExtDateBrowserWnd::e_painted_item_type_t ePIT,
	bool bWindowFocused,
	bool bToday
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( strText == NULL )
		return;
INT nTextLen = (INT)_tcslen( strText );
	if( nTextLen == 0 )
		return;
CRect rcPadding = OnDateBrowserQueryYearDecadeItemPadding();
bool bDrawFocusRect = false;
CFont * pFont = OnDateBrowserQueryItemFont( __EVM_YEAR_DECADE );
	switch( PaintStyleGet() )
	{
	case __EPST_PAINT_MANAGER:
		switch( eIST )
		{
		case __EIST_SELECTED_NORMAL:
		case __EIST_SELECTED_HOVER:
		case __EIST_SELECTED_PRESSED:
			bDrawFocusRect = bWindowFocused;
			// continue falling
		case __EIST_HOVER:
		case __EIST_PRESSED:
		{
			CExtPaintManager::PAINTPUSHBUTTONDATA _ppbd(
				this,
				true,
				rcItem,
				(LPCTSTR)strText,
				NULL,
				true,
				( eIST == __EIST_SELECTED_HOVER || eIST == __EIST_HOVER ) ? true : false,
				( eIST == __EIST_SELECTED_PRESSED || eIST == __EIST_PRESSED ) ? true : false,
				false,
				true,
				true,
				false, // bDrawFocusRect
				false,
				CExtPaintManager::__ALIGN_HORIZ_CENTER|CExtPaintManager::__ALIGN_VERT_CENTER,
				(HFONT)pFont->GetSafeHandle(), 
				false, 
				0, 
				true
				);
			_ppbd.m_bWordBreak = true;
			_ppbd.m_rcBorderSizes = rcPadding;
			CExtMemoryDC dc2( &dc, &rcItem );	//must edit
			PmBridge_GetPM()->PaintPushButton( dc2, _ppbd );
			if(		bDrawFocusRect
				&&	m_rcFocusRectPadding.left >= 0
				&&	m_rcFocusRectPadding.right >= 0
				&&	m_rcFocusRectPadding.top >= 0
				&&	m_rcFocusRectPadding.bottom >= 0
				)
			{
				CRect rcFR = rcItem;
				//rcFR.DeflateRect( &m_rcFocusRectPadding );
				rcFR.DeflateRect( 3, 3 );
				COLORREF clrTextOld = 
					dc2.SetTextColor( RGB(255,255,255) );
				COLORREF clrBkOld =
					dc2.SetBkColor( RGB(0,0,0) );
				dc2.DrawFocusRect( &rcFR );
				dc2.SetBkColor( clrBkOld );
				dc2.SetTextColor( clrTextOld );
			}
		}
		return;
		} // switch( eIST )
	break;
	case __EPST_SKIN:
		m_skin.PaintItemBackground( dc, rcItem, eIST, ePIT, bWindowFocused, bToday );
	break;
	case __EPST_SIMPLE:
	{
		COLORREF clrBack = OnDateBrowserQueryPaletteItemBackgroundColor( __EVM_ONE_YEAR, eIST, ePIT, bWindowFocused, bToday );
		if( clrBack != COLORREF(-1L) )
			dc.FillSolidRect( &rcItem, clrBack );
		if(		eIST == __EIST_SELECTED_NORMAL
			||	eIST == __EIST_SELECTED_HOVER
			||	eIST == __EIST_SELECTED_PRESSED
			)
			bDrawFocusRect = bWindowFocused;
	}
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	}
COLORREF clrText = OnDateBrowserQueryPaletteItemTextColor( __EVM_ONE_YEAR, eIST, ePIT, bWindowFocused, bToday );
COLORREF clrOldTextColor = dc.SetTextColor( clrText );

CRect rcText = rcItem;
	rcText.DeflateRect( &rcPadding );
CFont * pOldFont = dc.SelectObject( pFont );
INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
	CExtRichContentLayout::stat_DrawText(
		dc.m_hDC,
		strText, nTextLen,
		&rcText,
		DT_CENTER|DT_VCENTER|DT_SINGLELINE, 0
		);
	dc.SetBkMode( nOldBkMode );
	dc.SelectObject( pOldFont );
	dc.SetTextColor( clrOldTextColor );

	if(		bDrawFocusRect
		&&	m_rcFocusRectPadding.left >= 0
		&&	m_rcFocusRectPadding.right >= 0
		&&	m_rcFocusRectPadding.top >= 0
		&&	m_rcFocusRectPadding.bottom >= 0
		)
	{
		CRect rcFR = rcItem;
		rcFR.DeflateRect( &m_rcFocusRectPadding );
		COLORREF clrTextOld = 
			dc.SetTextColor( RGB(255,255,255) );
		COLORREF clrBkOld =
			dc.SetBkColor( RGB(0,0,0) );
		dc.DrawFocusRect( &rcFR );
		dc.SetBkColor( clrBkOld );
		dc.SetTextColor( clrTextOld );
	}
}

void CExtDateBrowserWnd::OnDateBrowserPaintMonthItem(
	CDC & dc,
	__EXT_MFC_SAFE_LPCTSTR strText,
	CRect rcItem,
	CExtDateBrowserWnd::e_item_state_t eIST,
	CExtDateBrowserWnd::e_painted_item_type_t ePIT,
	bool bWindowFocused,
	bool bToday,
	INT nX,
	INT nY,
	INT _dayOfWeek // 0-Sun, 1-Mon, ...
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( 0 <= _dayOfWeek && _dayOfWeek <=6 );
	nX;
	_dayOfWeek;
	if( strText == NULL )
		return;
INT nTextLen = (INT)_tcslen( strText );
	if( nTextLen == 0 )
		return;
CRect rcPadding = OnDateBrowserQueryYearDecadeItemPadding();
bool bDrawFocusRect = false;
CFont * pFont = OnDateBrowserQueryItemFont( __EVM_YEAR_DECADE );
	switch( PaintStyleGet() )
	{
	case __EPST_PAINT_MANAGER:
		if( nY == 0 )
			break;
		switch( eIST )
		{
		case __EIST_SELECTED_NORMAL:
		case __EIST_SELECTED_HOVER:
		case __EIST_SELECTED_PRESSED:
			bDrawFocusRect = bWindowFocused;
			// continue falling
		case __EIST_HOVER:
		case __EIST_PRESSED:
		{
			CExtPaintManager::PAINTPUSHBUTTONDATA _ppbd(
				this,
				true,
				rcItem,
				(LPCTSTR)strText,
				NULL,
				true,
				( eIST == __EIST_SELECTED_HOVER || eIST == __EIST_HOVER ) ? true : false,
				( eIST == __EIST_SELECTED_PRESSED || eIST == __EIST_PRESSED ) ? true : false,
				false,
				true,
				true,
				false, // bDrawFocusRect
				false,
				CExtPaintManager::__ALIGN_HORIZ_CENTER|CExtPaintManager::__ALIGN_VERT_CENTER,
				(HFONT)pFont->GetSafeHandle(), 
				false, 
				0, 
				true
				);
			_ppbd.m_bWordBreak = true;
			//_ppbd.m_rcBorderSizes = rcPadding;
			CExtMemoryDC dc2( &dc, &rcItem );	//must edit
			PmBridge_GetPM()->PaintPushButton( dc2, _ppbd );
			if(		bDrawFocusRect
				&&	m_rcFocusRectPadding.left >= 0
				&&	m_rcFocusRectPadding.right >= 0
				&&	m_rcFocusRectPadding.top >= 0
				&&	m_rcFocusRectPadding.bottom >= 0
				)
			{
				CRect rcFR = rcItem;
				//rcFR.DeflateRect( &m_rcFocusRectPadding );
				rcFR.DeflateRect( 2, 2 );
				COLORREF clrTextOld = 
					dc2.SetTextColor( RGB(255,255,255) );
				COLORREF clrBkOld =
					dc2.SetBkColor( RGB(0,0,0) );
				dc2.DrawFocusRect( &rcFR );
				dc2.SetBkColor( clrBkOld );
				dc2.SetTextColor( clrTextOld );
			}
		}
		return;
		} // switch( eIST )
	break;
	case __EPST_SKIN:
		m_skin.PaintItemBackground( dc, rcItem, eIST, ePIT, bWindowFocused, bToday );
	break;
	case __EPST_SIMPLE:
	{
		COLORREF clrBack = OnDateBrowserQueryPaletteItemBackgroundColor( __EVM_MONTH, eIST, ePIT, bWindowFocused, bToday );
		if( clrBack != COLORREF(-1L) )
			dc.FillSolidRect( &rcItem, clrBack );
		if(		eIST == __EIST_SELECTED_NORMAL
			||	eIST == __EIST_SELECTED_HOVER
			||	eIST == __EIST_SELECTED_PRESSED
			)
			bDrawFocusRect = bWindowFocused;
	}
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	}
COLORREF clrText = OnDateBrowserQueryPaletteItemTextColor( __EVM_MONTH, eIST, ePIT, bWindowFocused, bToday );
COLORREF clrOldTextColor = dc.SetTextColor( clrText );

CRect rcText = rcItem;
	//rcText.DeflateRect( &rcPadding );
CFont * pOldFont = dc.SelectObject( pFont );
INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
	CExtRichContentLayout::stat_DrawText(
		dc.m_hDC,
		strText, nTextLen,
		&rcText,
		DT_CENTER|DT_VCENTER|DT_SINGLELINE, 0
		);
	dc.SetBkMode( nOldBkMode );
	dc.SelectObject( pOldFont );
	dc.SetTextColor( clrOldTextColor );

	if(		bDrawFocusRect
		&&	m_rcFocusRectPadding.left >= 0
		&&	m_rcFocusRectPadding.right >= 0
		&&	m_rcFocusRectPadding.top >= 0
		&&	m_rcFocusRectPadding.bottom >= 0
		)
	{
		CRect rcFR = rcItem;
		// rcFR.DeflateRect( &m_rcFocusRectPadding );
 		COLORREF clrTextOld = 
 			dc.SetTextColor( RGB(255,255,255) );
		COLORREF clrBkOld =
			dc.SetBkColor( RGB(0,0,0) );
		dc.DrawFocusRect( &rcFR );
		dc.SetBkColor( clrBkOld );
		dc.SetTextColor( clrTextOld );
	}
}

void CExtDateBrowserWnd::OnDateBrowserPaintEntireCentury(
	CDC & dc,
	const COleDateTime & _time,
	CRect rcEntire,
	bool bWindowFocused
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
CRect rcCaption( 0, 0, 0, 0 ), rcButtonPrevious( 0, 0, 0, 0 ), rcButtonNext( 0, 0, 0, 0 );
CList < CRect, CRect & > listItems;
CList < COleDateTime, COleDateTime & > listTimes;
INT nInvisibleCountAtStart = 0, nInvisibleCountAtEnd = 0;
	OnDateBrowserCalcLayoutCentury(
		dc,
		_time,
		rcEntire,
		rcCaption,
		rcButtonPrevious,
		rcButtonNext,
		listItems,
		listTimes,
		nInvisibleCountAtStart,
		nInvisibleCountAtEnd
		);
	ASSERT( listItems.GetCount() == listTimes.GetCount() );
	ASSERT( listItems.GetCount() == __EXT_DATE_BROWSER_CENTURY_ITEM_COUNT );
	
CExtSafeString s =
		OnDateBrowserQueryCaptionText(
			_time,
			__EVM_CENTURY
			);
	OnDateBrowserPaintCaption(
		dc,
		__EVM_CENTURY,
		LPCTSTR(s),
		rcCaption,
		__EIST_NORMAL, // m_hitTestLast.GetCaptionItemState( m_bPressedTracking ),
		__EPIT_NORMAL_ITEM,
		bWindowFocused
		);

e_item_state_t eIST_PrevBtn = __EIST_DISABLED;
	if( nInvisibleCountAtStart == 0 )
	{
		COleDateTime _timeTest = listTimes.GetHead();
		_timeTest -= COleDateTimeSpan( 1, 0, 0, 0 );
		if( OnDateBrowserQueryCenturyItemVisibility( _timeTest ) )
			eIST_PrevBtn = m_hitTestLast.GetButtonItemState( true, m_bPressedTracking );
	}
	OnDateBrowserPaintButton( dc, true, rcButtonPrevious, eIST_PrevBtn );
e_item_state_t eIST_NextBtn = __EIST_DISABLED;
	if( nInvisibleCountAtEnd == 0 )
	{
		COleDateTime _timeTest = listTimes.GetTail();
		COleDateTime _time2( _timeTest.GetYear() + 10, _timeTest.GetMonth(), 1, 0, 0, 0 );
		if( OnDateBrowserQueryCenturyItemVisibility( _time2 ) )
			eIST_NextBtn = m_hitTestLast.GetButtonItemState( false, m_bPressedTracking );
	}
	OnDateBrowserPaintButton( dc, false, rcButtonNext, eIST_NextBtn );

POSITION posItems = listItems.GetHeadPosition();
POSITION posTimes = listTimes.GetHeadPosition();
	ASSERT(
			( posItems != NULL && posTimes != NULL )
		||	( posItems == NULL && posTimes == NULL )
		);
INT nPaletteItemIndex = 0;

bool bShowCenturyBeforeCurrentCentury = true;
bool bShowCenturyAfterCurrentCentury = true;
	OnDateBrowserQueryShowPrevNextCenturyItems( bShowCenturyBeforeCurrentCentury, bShowCenturyAfterCurrentCentury );

LONG nFirstYear = 0;
LONG nLastYear	= 0;

CExtDateBrowserWnd::stat_CalcCenturyDisplayYearRange( _time.GetYear(), nFirstYear, nLastYear );

int nDecadeCounter = 0;

	for( ; posItems != NULL; nPaletteItemIndex ++, nDecadeCounter ++ )
	{
		CRect & rcItem = listItems.GetNext( posItems );
		COleDateTime & _timeItem = listTimes.GetNext( posTimes );
		ASSERT(
				( posItems != NULL && posTimes != NULL )
			||	( posItems == NULL && posTimes == NULL )
			);
		
		if( ! OnDateBrowserQueryCenturyItemVisibility( _timeItem ) )
			continue;

		int nCurrentYear = _timeItem.GetYear();

		bool bThisDecade = ( nCurrentYear >= nFirstYear && nCurrentYear <= nLastYear ) ? true : false;
		bool bToday = OnDateBrowserQueryTodayFlag( _timeItem, __EVM_CENTURY );
		CExtSafeString s;
		if( nDecadeCounter == 0 )
		{
			if( bShowCenturyBeforeCurrentCentury )
			{
				s = OnDateBrowserQueryCenturyItemText(
						_timeItem
						);
				OnDateBrowserPaintCenturyItem(
					dc,
					LPCTSTR(s),
					rcItem,
					m_hitTestLast.GetPaletteItemState( nPaletteItemIndex, OnDateBrowserQueryPaletteItemIndexSelectedState( _timeItem, _time, __EVM_CENTURY ), m_bPressedTracking ),
					( nPaletteItemIndex == 0 || nPaletteItemIndex == 11 ) ? __EPIT_OUT_OF_RANGE_ITEM : __EPIT_NORMAL_ITEM,
					bWindowFocused,
					bToday
					);
			}
		}
		else
		{
			if( bShowCenturyAfterCurrentCentury || bThisDecade )
			{
				s = OnDateBrowserQueryCenturyItemText(
						_timeItem
						);
				OnDateBrowserPaintCenturyItem(
					dc,
					LPCTSTR(s),
					rcItem,
					m_hitTestLast.GetPaletteItemState( nPaletteItemIndex, OnDateBrowserQueryPaletteItemIndexSelectedState( _timeItem, _time, __EVM_CENTURY ), m_bPressedTracking ),
					( nPaletteItemIndex == 0 || nPaletteItemIndex == 11 ) ? __EPIT_OUT_OF_RANGE_ITEM : __EPIT_NORMAL_ITEM,
					bWindowFocused,
					bToday
					);
			}
		}
	} // for( ; posItems != NULL; nPaletteItemIndex++ )
}

void CExtDateBrowserWnd::OnDateBrowserPaintEntireYearDecade(
	CDC & dc,
	const COleDateTime & _time,
	CRect rcEntire,
	bool bWindowFocused
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
CRect rcCaption( 0, 0, 0, 0 ), rcButtonPrevious( 0, 0, 0, 0 ), rcButtonNext( 0, 0, 0, 0 );
CList < CRect, CRect & > listItems;
CList < COleDateTime, COleDateTime & > listTimes;
INT nInvisibleCountAtStart = 0, nInvisibleCountAtEnd = 0;
	OnDateBrowserCalcLayoutYearDecade(
		dc,
		_time,
		rcEntire,
		rcCaption,
		rcButtonPrevious,
		rcButtonNext,
		listItems,
		listTimes,
		nInvisibleCountAtStart,
		nInvisibleCountAtEnd
		);
	ASSERT( listItems.GetCount() == listTimes.GetCount() );
	ASSERT( listItems.GetCount() == __EXT_DATE_BROWSER_YEAR_DECADE_ITEM_COUNT );

CExtSafeString s =
		OnDateBrowserQueryCaptionText(
			_time,
			__EVM_YEAR_DECADE
			);
	OnDateBrowserPaintCaption(
		dc,
		__EVM_YEAR_DECADE,
		LPCTSTR(s),
		rcCaption,
		m_hitTestLast.GetCaptionItemState( m_bPressedTracking ),
		__EPIT_NORMAL_ITEM,
		bWindowFocused
		);

e_item_state_t eIST_PrevBtn = __EIST_DISABLED;
	if( nInvisibleCountAtStart == 0 )
	{
		COleDateTime _timeTest = listTimes.GetHead();
		_timeTest -= COleDateTimeSpan( 1, 0, 0, 0 );
		if( OnDateBrowserQueryCenturyItemVisibility( _timeTest ) )
			eIST_PrevBtn = m_hitTestLast.GetButtonItemState( true, m_bPressedTracking );
	}
	OnDateBrowserPaintButton( dc, true, rcButtonPrevious, eIST_PrevBtn );
e_item_state_t eIST_NextBtn = __EIST_DISABLED;
	if( nInvisibleCountAtEnd == 0 )
	{
		COleDateTime _timeTest = listTimes.GetTail();
		COleDateTime _time2( _timeTest.GetYear() + 1, _timeTest.GetMonth(), 1, 0, 0, 0 );
		if( OnDateBrowserQueryCenturyItemVisibility( _time2 ) )
			eIST_NextBtn = m_hitTestLast.GetButtonItemState( false, m_bPressedTracking );
	}
	OnDateBrowserPaintButton( dc, false, rcButtonNext, eIST_NextBtn );

POSITION posItems = listItems.GetHeadPosition();
POSITION posTimes = listTimes.GetHeadPosition();
	ASSERT(
			( posItems != NULL && posTimes != NULL )
		||	( posItems == NULL && posTimes == NULL )
		);
INT nPaletteItemIndex = 0;

bool bShowDecadeBeforeCurrentDecade = true;
bool bShowDecadeAfterCurrentDecade = true;
	OnDateBrowserQueryShowPrevNextYearDecadeItems( bShowDecadeBeforeCurrentDecade, bShowDecadeAfterCurrentDecade );

LONG nFirstYear = 0;
LONG nLastYear = 0;

	CExtDateBrowserWnd::stat_CalcYearDecadeDisplayYearRange( _time.GetYear(), nFirstYear, nLastYear );

int nYearCounter = 0;

	for( ; posItems != NULL; nPaletteItemIndex ++, nYearCounter ++ )
	{
		CRect & rcItem = listItems.GetNext( posItems );
		COleDateTime & _timeItem = listTimes.GetNext( posTimes );
		ASSERT(
				( posItems != NULL && posTimes != NULL )
			||	( posItems == NULL && posTimes == NULL )
			);
		
		if( ! OnDateBrowserQueryYearDecadeItemVisibility( _timeItem ) )
			continue;

		int nCurrentYear = _timeItem.GetYear();

		bool bThisYear = ( nCurrentYear >= nFirstYear && nCurrentYear <= nLastYear ) ? true : false;
		bool bToday = OnDateBrowserQueryTodayFlag( _timeItem, __EVM_YEAR_DECADE );
		CExtSafeString s;
		if( nYearCounter == 0 )
		{
			if( bShowDecadeBeforeCurrentDecade )
			{
				s = OnDateBrowserQueryYearDecadeItemText(
						_timeItem
						);
				OnDateBrowserPaintYearDecadeItem(
					dc,
					LPCTSTR(s),
					rcItem,
					m_hitTestLast.GetPaletteItemState( nPaletteItemIndex, OnDateBrowserQueryPaletteItemIndexSelectedState( _timeItem, _time, __EVM_YEAR_DECADE ), m_bPressedTracking ),
					( nPaletteItemIndex == 0 || nPaletteItemIndex == 11 ) ? __EPIT_OUT_OF_RANGE_ITEM : __EPIT_NORMAL_ITEM,
					bWindowFocused,
					bToday
					);
			}
		}
		else
		{
			if( bShowDecadeAfterCurrentDecade || bThisYear )
			{
				s = OnDateBrowserQueryYearDecadeItemText(
						_timeItem
						);
				OnDateBrowserPaintYearDecadeItem(
					dc,
					LPCTSTR(s),
					rcItem,
					m_hitTestLast.GetPaletteItemState( nPaletteItemIndex, OnDateBrowserQueryPaletteItemIndexSelectedState( _timeItem, _time, __EVM_YEAR_DECADE ), m_bPressedTracking ),
					( nPaletteItemIndex == 0 || nPaletteItemIndex == 11 ) ? __EPIT_OUT_OF_RANGE_ITEM : __EPIT_NORMAL_ITEM,
					bWindowFocused,
					bToday
					);
			}
		}
	} // for( ; posItems != NULL; nPaletteItemIndex ++ )
}

void CExtDateBrowserWnd::OnDateBrowserPaintEntireOneYear(
	CDC & dc,
	const COleDateTime & _time,
	CRect rcEntire,
	bool bWindowFocused
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
CRect rcCaption( 0, 0, 0, 0 ), rcButtonPrevious( 0, 0, 0, 0 ), rcButtonNext( 0, 0, 0, 0 );
CList < CRect, CRect & > listItems;
CList < COleDateTime, COleDateTime & > listTimes;
INT nInvisibleCountAtStart = 0, nInvisibleCountAtEnd = 0;
	OnDateBrowserCalcLayoutOneYear(
		dc,
		_time,
		rcEntire,
		rcCaption,
		rcButtonPrevious,
		rcButtonNext,
		listItems,
		listTimes,
		nInvisibleCountAtStart,
		nInvisibleCountAtEnd
		);
	ASSERT( listItems.GetCount() == listTimes.GetCount() );
	ASSERT( listItems.GetCount() == __EXT_DATE_BROWSER_ONE_YEAR_ITEM_COUNT );

CExtSafeString s =
		OnDateBrowserQueryCaptionText(
			_time,
			__EVM_ONE_YEAR
			);
	OnDateBrowserPaintCaption(
		dc,
		__EVM_ONE_YEAR,
		LPCTSTR(s),
		rcCaption,
		m_hitTestLast.GetCaptionItemState( m_bPressedTracking ),
		__EPIT_NORMAL_ITEM,
		bWindowFocused
		);

e_item_state_t eIST_PrevBtn = __EIST_DISABLED;
	if( nInvisibleCountAtStart == 0 )
	{
		COleDateTime _timeTest = listTimes.GetHead();
		_timeTest -= COleDateTimeSpan( 1, 0, 0, 0 );
		if( OnDateBrowserQueryCenturyItemVisibility( _timeTest ) )
			eIST_PrevBtn = m_hitTestLast.GetButtonItemState( true, m_bPressedTracking );
	}
	OnDateBrowserPaintButton( dc, true, rcButtonPrevious, eIST_PrevBtn );
e_item_state_t eIST_NextBtn = __EIST_DISABLED;
	if( nInvisibleCountAtEnd == 0 )
	{
		COleDateTime _timeTest = listTimes.GetTail();
		_timeTest += COleDateTimeSpan( 32, 0, 0, 0 );
		if( OnDateBrowserQueryCenturyItemVisibility( _timeTest ) )
			eIST_NextBtn = m_hitTestLast.GetButtonItemState( false, m_bPressedTracking );
	}
	OnDateBrowserPaintButton( dc, false, rcButtonNext, eIST_NextBtn );

POSITION posItems = listItems.GetHeadPosition();
POSITION posTimes = listTimes.GetHeadPosition();
	ASSERT(
			( posItems != NULL && posTimes != NULL )
		||	( posItems == NULL && posTimes == NULL )
		);
INT nPaletteItemIndex = 0;
	for( ; posItems != NULL; nPaletteItemIndex ++ )
	{
		CRect & rcItem = listItems.GetNext( posItems );
		COleDateTime & _timeItem = listTimes.GetNext( posTimes );
		ASSERT(
				( posItems != NULL && posTimes != NULL )
			||	( posItems == NULL && posTimes == NULL )
			);
		
		if( ! OnDateBrowserQueryOneYearItemVisibility( _timeItem ) )
			continue;

		CExtSafeString s =
			OnDateBrowserQueryOneYearItemText(
				_timeItem
				);
		bool bToday = OnDateBrowserQueryTodayFlag( _timeItem, __EVM_ONE_YEAR );
		OnDateBrowserPaintOneYearItem(
			dc,
			LPCTSTR(s),
			rcItem,
			m_hitTestLast.GetPaletteItemState( nPaletteItemIndex, OnDateBrowserQueryPaletteItemIndexSelectedState( _timeItem, _time, __EVM_ONE_YEAR ), m_bPressedTracking ),
			__EPIT_NORMAL_ITEM,
			bWindowFocused,
			bToday
			);
	} // for( ; posItems != NULL; nPaletteItemIndex ++ )
}

void CExtDateBrowserWnd::OnDateBrowserPaintEntireMonth(
	CDC & dc,
	const COleDateTime & _time,
	CRect rcEntire,
	bool bWindowFocused
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );

CRect rcCaption( 0, 0, 0, 0 ), rcButtonPrevious( 0, 0, 0, 0 ), rcButtonNext( 0, 0, 0, 0 ), rcMonthSeparator( 0, 0, 0, 0 );
CList < CRect, CRect & > listItems;
CList < COleDateTime, COleDateTime & > listTimes;
INT nInvisibleCountAtStart = 0, nInvisibleCountAtEnd = 0;
	OnDateBrowserCalcLayoutMonth(
		dc,
		_time,
		rcEntire,
		rcCaption,
		rcMonthSeparator,
		rcButtonPrevious,
		rcButtonNext,
		listItems,
		listTimes,
		nInvisibleCountAtStart,
		nInvisibleCountAtEnd
		);
	ASSERT( listItems.GetCount() == listTimes.GetCount() );
	ASSERT( listItems.GetCount() == __EXT_DATE_BROWSER_MONTH_ITEM_COUNT );

CExtSafeString s =
		OnDateBrowserQueryCaptionText(
			_time,
			__EVM_MONTH
			);
	OnDateBrowserPaintCaption(
		dc,
		__EVM_MONTH,
		LPCTSTR(s),
		rcCaption,
		m_hitTestLast.GetCaptionItemState( m_bPressedTracking ),
		__EPIT_NORMAL_ITEM,
		bWindowFocused
		);

e_item_state_t eIST_PrevBtn = __EIST_DISABLED;
	if( nInvisibleCountAtStart == 0 )
	{
		INT nDayOfWeek = 0;
		POSITION pos = listTimes.GetHeadPosition();
		for( ; nDayOfWeek < 7 && pos != NULL; nDayOfWeek++ )
			listTimes.GetNext( pos );
		if( pos != NULL )
		{
			COleDateTime _timeTest = listTimes.GetNext( pos );
			_timeTest -= COleDateTimeSpan( 1, 0, 0, 0 );
			if( OnDateBrowserQueryCenturyItemVisibility( _timeTest ) )
				eIST_PrevBtn = m_hitTestLast.GetButtonItemState( true, m_bPressedTracking );
		}
	}
	OnDateBrowserPaintButton( dc, true, rcButtonPrevious, eIST_PrevBtn );
e_item_state_t eIST_NextBtn = __EIST_DISABLED;
	if( nInvisibleCountAtEnd == 0 )
	{
		COleDateTime _timeTest = listTimes.GetTail();
		_timeTest += COleDateTimeSpan( 1, 0, 0, 0 );
		if( OnDateBrowserQueryCenturyItemVisibility( _timeTest ) )
			eIST_NextBtn = m_hitTestLast.GetButtonItemState( false, m_bPressedTracking );
	}
	OnDateBrowserPaintButton( dc, false, rcButtonNext, eIST_NextBtn );

COleDateTime _firstDayOfMonth = _time;
_firstDayOfMonth.SetDateTime( _time.GetYear(), _time.GetMonth(), 1, 0, 0, 0 );

INT nFirstDayOfWeekInThisMonth = _firstDayOfMonth.GetDayOfWeek() - 1;

POSITION posItems = listItems.GetHeadPosition();
POSITION posTimes = listTimes.GetHeadPosition();
	ASSERT(
			( posItems != NULL && posTimes != NULL )
		||	( posItems == NULL && posTimes == NULL )
		);
INT nDayOfWeek = 0, nPaletteItemIndex = 0;
	for( ; nDayOfWeek < 7; nDayOfWeek++, nPaletteItemIndex ++ )
	{
		CRect & rcItem = listItems.GetNext( posItems );
		COleDateTime & _time = listTimes.GetNext( posTimes );
		ASSERT(
				( posItems != NULL && posTimes != NULL )
			||	( posItems == NULL && posTimes == NULL )
			);

		INT nEffectiveDayOfWeek = _time.GetDayOfWeek() - 1;
		CExtSafeString s = OnDateBrowserQueryWeekItemText( nEffectiveDayOfWeek );

		ASSERT( (nPaletteItemIndex%7) == nDayOfWeek );
		OnDateBrowserPaintMonthItem(
			dc,
			LPCTSTR(s),
			rcItem,
			m_hitTestLast.GetPaletteItemState(
				nPaletteItemIndex,
				__EIST_NORMAL, // OnDateBrowserQueryPaletteItemIndexSelectedState( _time, eVM ),
				m_bPressedTracking
				),
			__EPIT_WEEK_CAPTION_ITEM,
			bWindowFocused,
			false,
			nPaletteItemIndex%7,
			nPaletteItemIndex/7,
			nEffectiveDayOfWeek
			);
	} // for( ; nDayOfWeek < 7; nDayOfWeek++, nPaletteItemIndex ++ )

bool bShowDaysBeforeMonth = true;
bool bShowDaysAfterMonth = true;
	OnDateBrowserQueryShowPrevNextMonthDays( bShowDaysBeforeMonth, bShowDaysAfterMonth );

INT nCurrentMonth = _time.GetMonth();
INT nDayCounter = 0;
	nDayOfWeek = 0;
		ASSERT(
				( posItems != NULL && posTimes != NULL )
			||	( posItems == NULL && posTimes == NULL )
			);
	for( ; posItems != NULL; nPaletteItemIndex ++ )
	{
		CRect & rcItem = listItems.GetNext( posItems );
		COleDateTime & _timeItem = listTimes.GetNext( posTimes );
		ASSERT(
				( posItems != NULL && posTimes != NULL )
			||	( posItems == NULL && posTimes == NULL )
			);

		if( OnDateBrowserQueryMonthItemVisibility( _timeItem ) )
		{
			bool bThisMonth = ( nCurrentMonth == _timeItem.GetMonth() ) ? true : false;
			bool bToday = OnDateBrowserQueryTodayFlag( _timeItem, __EVM_MONTH );
			CExtSafeString s;
			if( nDayOfWeek < nFirstDayOfWeekInThisMonth && nDayCounter < 7 )
			{
				if( bShowDaysBeforeMonth )
				{
					s = OnDateBrowserQueryMonthItemText( _timeItem );
					OnDateBrowserPaintMonthItem(
						dc,
						LPCTSTR(s),
						rcItem,
						m_hitTestLast.GetPaletteItemState( nPaletteItemIndex, OnDateBrowserQueryPaletteItemIndexSelectedState( _timeItem, _time, __EVM_MONTH ), m_bPressedTracking ),
						bThisMonth ? __EPIT_NORMAL_ITEM : __EPIT_OUT_OF_RANGE_ITEM,
						bWindowFocused,
						bToday,
						nPaletteItemIndex%7,
						nPaletteItemIndex/7,
						_timeItem.GetDayOfWeek() - 1
						);
				}
			}
			else
			{
				if( bShowDaysAfterMonth || bThisMonth )
				{
					s = OnDateBrowserQueryMonthItemText( _timeItem );
					OnDateBrowserPaintMonthItem(
						dc,
						LPCTSTR(s),
						rcItem,
						m_hitTestLast.GetPaletteItemState( nPaletteItemIndex, OnDateBrowserQueryPaletteItemIndexSelectedState( _timeItem, _time, __EVM_MONTH ), m_bPressedTracking ),
						bThisMonth ? __EPIT_NORMAL_ITEM : __EPIT_OUT_OF_RANGE_ITEM,
						bWindowFocused,
						bToday,
						nPaletteItemIndex%7,
						nPaletteItemIndex/7,
						_timeItem.GetDayOfWeek() - 1
						);
				}
			}
		} // if( OnDateBrowserQueryMonthItemVisibility( _timeItem ) )
		nDayOfWeek = ( nDayOfWeek == 6 ) ? 0 : ( nDayOfWeek + 1 );
		nDayCounter ++;
	} // for( ; posItems != NULL; nPaletteItemIndex ++ )

	if( ! rcMonthSeparator.IsRectEmpty() )
	{
		OnDateBrowserPaintMonthSeparator( dc, rcMonthSeparator );
	}
}

CSize CExtDateBrowserWnd::OnDateBrowserCalcButtonSize(
	CDC & dcMeasure,
	bool bPrevious
	)
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	dcMeasure;
CExtCmdIcon & _icon = OnDateBrowserQueryButtonIcon( bPrevious );
CSize _size = _icon.GetSize();
CRect rcPadding = OnDateBrowserQueryButtonPadding( bPrevious );
	_size.cx += rcPadding.left + rcPadding.right;
	_size.cy += rcPadding.top + rcPadding.bottom;
	return _size;
}

CExtCmdIcon & CExtDateBrowserWnd::OnDateBrowserQueryButtonIcon(
		bool bPrevious
		)
{
	ASSERT_VALID( this );
	if( bPrevious )
		return m_iconBtnPrevious;
	else
		return m_iconBtnNext;
}

void CExtDateBrowserWnd::OnDateBrowserPaintButton(
	CDC & dc,
	bool bPrevious,
	CRect rcItem,
	CExtDateBrowserWnd::e_item_state_t eIST
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
CRect rcPadding = OnDateBrowserQueryButtonPadding( bPrevious );
CRect rcIcon = rcItem;
	rcIcon.DeflateRect( &rcPadding );
CExtCmdIcon & _icon = OnDateBrowserQueryButtonIcon( bPrevious );
	switch( eIST )	
	{
	case __EIST_HOVER:
		_icon.Paint( NULL, dc, rcIcon, CExtCmdIcon::__PAINT_HOVER );
	break;
	case __EIST_NORMAL:
		_icon.Paint( NULL, dc, rcIcon );
	break;
	case __EIST_PRESSED:
		_icon.Paint( NULL, dc, rcIcon, CExtCmdIcon::__PAINT_PRESSED );
	break;
	case __EIST_DISABLED:
		_icon.Paint( NULL, dc, rcIcon, CExtCmdIcon::__PAINT_DISABLED );
	break;
	}
}

CSize CExtDateBrowserWnd::OnDateBrowserCalcCaptionSize(
	CDC & dcMeasure,
	CExtDateBrowserWnd::e_view_mode_t _eVM,
	const COleDateTime & _time
	) const
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
CExtSafeString s = OnDateBrowserQueryCaptionText( _time, _eVM );
CFont * pFont = OnDateBrowserQueryCaptionFont( _eVM );
CFont * pOldFont = dcMeasure.SelectObject( pFont );
CRect rc( 0, 0, 0, 0 );
	CExtRichContentLayout::stat_DrawText(
		dcMeasure.m_hDC,
		LPCTSTR(s), s.GetLength(),
		&rc,
		DT_LEFT|DT_TOP|DT_CALCRECT, 0
		);
	dcMeasure.SelectObject( pOldFont );
CSize _size = rc.Size();
CRect rcPadding = OnDateBrowserQueryCaptionPadding();
	_size.cx += rcPadding.left + rcPadding.right;
	_size.cy += rcPadding.top + rcPadding.bottom;
	return _size;
}

void CExtDateBrowserWnd::OnDateBrowserCalcLayoutGeneral(
	CDC & dcMeasure,
	CExtDateBrowserWnd::e_view_mode_t _eVM,
	const COleDateTime & _time,
	CRect rcEntire,
	CRect & rcCaption,
	CRect & rcButtonPrevious,
	CRect & rcButtonNext,
	CRect & rcRest
	)
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
CSize sizeButtonPrevious  = OnDateBrowserCalcButtonSize( dcMeasure, true );
CSize sizeButtonNext = OnDateBrowserCalcButtonSize( dcMeasure, false );

CSize sizeCaption    = OnDateBrowserCalcCaptionSize( dcMeasure, _eVM, _time );
	rcButtonPrevious.SetRect(
		rcEntire.left,
		rcEntire.top,
		rcEntire.left + sizeButtonPrevious.cx,
		rcEntire.top + sizeButtonPrevious.cy
		);
	rcButtonNext.SetRect(
		rcEntire.right - sizeButtonNext.cx,
		rcEntire.top,
		rcEntire.right,
		rcEntire.top + sizeButtonNext.cy
		);
	rcCaption.SetRect(
		rcButtonPrevious.right,
		rcButtonPrevious.top,
		rcButtonPrevious.right + sizeCaption.cx,
		rcButtonPrevious.top + sizeCaption.cy
		);
	rcCaption.OffsetRect(
		( rcButtonNext.left - rcButtonPrevious.right - sizeCaption.cx ) / 2,
		0
		);
int nMaxHeight = max( sizeButtonPrevious.cy, sizeButtonNext.cy );
	nMaxHeight = max( nMaxHeight, sizeCaption.cy );
	if( sizeButtonPrevious.cy < nMaxHeight )
		rcButtonPrevious.OffsetRect(
			0,
			( nMaxHeight - sizeButtonPrevious.cy ) / 2
			);
	if( sizeButtonNext.cy < nMaxHeight )
		rcButtonNext.OffsetRect(
			0,
			( nMaxHeight - sizeButtonNext.cy ) / 2
			);
	if( sizeCaption.cy < nMaxHeight )
		rcCaption.OffsetRect(
			0,
			( nMaxHeight - sizeCaption.cy ) / 2
			);
	rcRest.SetRect(
		rcEntire.left,
		rcEntire.top + nMaxHeight,
		rcEntire.right,
		rcEntire.bottom
		);
}

CSize CExtDateBrowserWnd::OnDateBrowserCalcCenturyItemSize(
	CDC & dcMeasure
	) const
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
COleDateTime _time( 2000, 1, 1, 1, 1, 1 );
CExtSafeString s = OnDateBrowserQueryCenturyItemText( _time );
	CFont * pFont = OnDateBrowserQueryItemFont( __EVM_CENTURY );
	CFont * pOldFont = dcMeasure.SelectObject( pFont );
	CRect rc( 0, 0, 0, 0 );
		CExtRichContentLayout::stat_DrawText(
			dcMeasure.m_hDC,
			LPCTSTR(s), s.GetLength(),
			&rc,
			DT_LEFT|DT_TOP|DT_CALCRECT, 0
			);
		dcMeasure.SelectObject( pOldFont );
CSize _size = rc.Size();
CRect rcPadding = OnDateBrowserQueryCenturyItemPadding();
	_size.cx += rcPadding.left + rcPadding.right;
	_size.cy += rcPadding.top + rcPadding.bottom;
	return _size;
}

CSize CExtDateBrowserWnd::OnDateBrowserCalcYearDecadeItemSize(
	CDC & dcMeasure
	) const
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
COleDateTime _time( 2000, 1, 1, 1, 1, 1 );
CExtSafeString s = OnDateBrowserQueryYearDecadeItemText( _time );
	CFont * pFont = OnDateBrowserQueryItemFont( __EVM_YEAR_DECADE );
	CFont * pOldFont = dcMeasure.SelectObject( pFont );
	CRect rc( 0, 0, 0, 0 );
		CExtRichContentLayout::stat_DrawText(
			dcMeasure.m_hDC,
			LPCTSTR(s), s.GetLength(),
			&rc,
			DT_LEFT|DT_TOP|DT_CALCRECT, 0
			);
		dcMeasure.SelectObject( pOldFont );
CSize _size = rc.Size();
CRect rcPadding = OnDateBrowserQueryYearDecadeItemPadding();
	_size.cx += rcPadding.left + rcPadding.right;
	_size.cy += rcPadding.top + rcPadding.bottom;
	return _size;
}

CSize CExtDateBrowserWnd::OnDateBrowserCalcOneYearItemSize(
	CDC & dcMeasure
	) const
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
CSize _size( 0, 0 );
INT nMonth = 1;
	for( ; nMonth <= 12; nMonth ++ )
	{
		COleDateTime _time( 2000, nMonth, 1, 1, 1, 1 );
		CExtSafeString s = OnDateBrowserQueryOneYearItemText( _time );
		CFont * pFont = OnDateBrowserQueryItemFont( __EVM_ONE_YEAR );
		CFont * pOldFont = dcMeasure.SelectObject( pFont );
		CRect rc( 0, 0, 0, 0 );
			CExtRichContentLayout::stat_DrawText(
				dcMeasure.m_hDC,
				LPCTSTR(s), s.GetLength(),
				&rc,
				DT_LEFT|DT_TOP|DT_CALCRECT, 0
				);
			dcMeasure.SelectObject( pOldFont );
		CSize _sizeTmp = rc.Size();
		_size.cx = max( _size.cx, _sizeTmp.cx );
		_size.cy = max( _size.cy, _sizeTmp.cy );
	}
CRect rcPadding = OnDateBrowserQueryOneYearItemPadding();
	_size.cx += rcPadding.left + rcPadding.right;
	_size.cy += rcPadding.top + rcPadding.bottom;
	return _size;
}

CSize CExtDateBrowserWnd::OnDateBrowserCalcMonthItemSize(
	CDC & dcMeasure
	) const
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	dcMeasure;
CExtPaintManager * pPM = PmBridge_GetPM();
	return CSize(
		pPM->UiScalingDo( 30, CExtPaintManager::__EUIST_X ),
		pPM->UiScalingDo( 20, CExtPaintManager::__EUIST_Y )
		);
}

CSize CExtDateBrowserWnd::OnDateBrowserMeasureLayoutCentury(
	CDC & dcMeasure,
	const COleDateTime & _time
	) const
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
CSize sizeCaption = OnDateBrowserCalcCaptionSize( dcMeasure, __EVM_CENTURY, _time );
CSize sizePaletteItem = OnDateBrowserCalcCenturyItemSize( dcMeasure );
CSize sizeEntireItems(
		sizePaletteItem.cx * __EXT_DATE_BROWSER_CENTURY_COLUMN_COUNT,
		sizePaletteItem.cy * __EXT_DATE_BROWSER_CENTURY_ROW_COUNT
		);
CSize sizeEntire(
		max( sizeCaption.cx, sizeEntireItems.cx ),
		sizeCaption.cy + sizeEntireItems.cy
		);
	return sizeEntire;
}

CSize CExtDateBrowserWnd::OnDateBrowserMeasureLayoutYearDecade(
	CDC & dcMeasure,
	const COleDateTime & _time
	) const
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
CSize sizeCaption = OnDateBrowserCalcCaptionSize( dcMeasure, __EVM_YEAR_DECADE, _time );
CSize sizePaletteItem = OnDateBrowserCalcYearDecadeItemSize( dcMeasure );
CSize sizeEntireItems(
		sizePaletteItem.cx * __EXT_DATE_BROWSER_YEAR_DECADE_COLUMN_COUNT,
		sizePaletteItem.cy * __EXT_DATE_BROWSER_YEAR_DECADE_ROW_COUNT
		);
CSize sizeEntire(
		max( sizeCaption.cx, sizeEntireItems.cx ),
		sizeCaption.cy + sizeEntireItems.cy
		);
	return sizeEntire;
}

CSize CExtDateBrowserWnd::OnDateBrowserMeasureLayoutOneYear(
	CDC & dcMeasure,
	const COleDateTime & _time
	) const
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
CSize sizeCaption = OnDateBrowserCalcCaptionSize( dcMeasure, __EVM_ONE_YEAR, _time );
CSize sizePaletteItem = OnDateBrowserCalcOneYearItemSize( dcMeasure );
CSize sizeEntireItems(
		sizePaletteItem.cx * __EXT_DATE_BROWSER_ONE_YEAR_COLUMN_COUNT,
		sizePaletteItem.cy * __EXT_DATE_BROWSER_ONE_YEAR_ROW_COUNT
		);
CSize sizeEntire(
		max( sizeCaption.cx, sizeEntireItems.cx ),
		sizeCaption.cy + sizeEntireItems.cy
		);
	return sizeEntire;
}

CSize CExtDateBrowserWnd::OnDateBrowserMeasureLayoutMonth(
	CDC & dcMeasure,
	const COleDateTime & _time
	) const
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
CSize sizeCaption = OnDateBrowserCalcCaptionSize( dcMeasure, __EVM_MONTH, _time );
CSize sizePaletteItem = OnDateBrowserCalcMonthItemSize( dcMeasure );
CSize sizeEntireItems(
		sizePaletteItem.cx * __EXT_DATE_BROWSER_MONTH_COLUMN_COUNT,
		sizePaletteItem.cy * __EXT_DATE_BROWSER_MONTH_ROW_COUNT
		);
INT nMonthSeparatorHeight = OnDateBrowserQueryMonthSeparatorHeight();
CSize sizeEntire(
		max( sizeCaption.cx, sizeEntireItems.cx ),
		sizeCaption.cy + sizeEntireItems.cy + nMonthSeparatorHeight
		);
	return sizeEntire;
}

CSize CExtDateBrowserWnd::OnDateBrowserCalcSize(
	CDC & dcMeasure,
	const COleDateTime & _time
	) const
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
CSize sizeRetVal = OnDateBrowserMeasureLayoutCentury(    dcMeasure, _time );
CSize size1      = OnDateBrowserMeasureLayoutYearDecade( dcMeasure, _time );
CSize size2      = OnDateBrowserMeasureLayoutOneYear(    dcMeasure, _time );
CSize size3      = OnDateBrowserMeasureLayoutMonth(      dcMeasure, _time );
	sizeRetVal.cx = max( sizeRetVal.cx, size1.cx );
	sizeRetVal.cx = max( sizeRetVal.cx, size2.cx );
	sizeRetVal.cx = max( sizeRetVal.cx, size3.cx );
	sizeRetVal.cy = max( sizeRetVal.cy, size1.cy );
	sizeRetVal.cy = max( sizeRetVal.cy, size2.cy );
	sizeRetVal.cy = max( sizeRetVal.cy, size3.cy );
	return sizeRetVal;
}

CSize CExtDateBrowserWnd::CalcSize(
	const COleDateTime & _time
	) const
{
	ASSERT_VALID( this );
CClientDC dc( (CWnd*)(this) );
CSize sizeRetVal = OnDateBrowserCalcSize( dc, _time );
	return sizeRetVal;
}

CSize CExtDateBrowserWnd::CalcSize() const
{
	ASSERT_VALID( this );
COleDateTime _time = TimeGet();
CSize sizeRetVal = CalcSize( _time );
	return sizeRetVal;
}

void CExtDateBrowserWnd::OnDateBrowserCalcLayoutCentury(
	CDC & dcMeasure,
	const COleDateTime & _time,
	CRect rcEntire,
	CRect & rcCaption,
	CRect & rcButtonPrevious,
	CRect & rcButtonNext,
	CList < CRect, CRect & > & listItems,
	CList < COleDateTime, COleDateTime & > & listTimes,
	INT & nInvisibleCountAtStart,
	INT & nInvisibleCountAtEnd
	)
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	nInvisibleCountAtStart = nInvisibleCountAtEnd = 0;
	listItems.RemoveAll();
	listTimes.RemoveAll();
CRect rcRest( 0, 0, 0, 0 );
	OnDateBrowserCalcLayoutGeneral(
		dcMeasure,
		__EVM_CENTURY,
		_time,
		rcEntire,
		rcCaption,
		rcButtonPrevious,
		rcButtonNext,
		rcRest
		);
CSize sizeCenturyItem = OnDateBrowserCalcCenturyItemSize( dcMeasure );
CSize sizeEntireItems(
		sizeCenturyItem.cx * __EXT_DATE_BROWSER_CENTURY_COLUMN_COUNT,
		sizeCenturyItem.cy * __EXT_DATE_BROWSER_CENTURY_ROW_COUNT
		);
CSize sizeDistanceBetweenItems(
		( rcRest.Width() - sizeEntireItems.cx ) / ( __EXT_DATE_BROWSER_CENTURY_COLUMN_COUNT + 1 ),
		( rcRest.Height() - sizeEntireItems.cy ) / ( __EXT_DATE_BROWSER_CENTURY_ROW_COUNT + 1 )
		);
CPoint ptLeftTop = rcRest.TopLeft();
INT nColNo, nRowNo;
LONG nYear, nTmp;
	stat_CalcCenturyDisplayYearRange(
		_time.GetYear(),
		nYear,
		nTmp
		);
	nYear -= 10;
	for( nRowNo = 0; nRowNo < __EXT_DATE_BROWSER_CENTURY_ROW_COUNT; nRowNo++ )
	{
		for( nColNo = 0; nColNo < __EXT_DATE_BROWSER_CENTURY_COLUMN_COUNT; nColNo++ )
		{
			CPoint ptItem(
				rcRest.left + nColNo * sizeCenturyItem.cx + ( nColNo + 1 ) * sizeDistanceBetweenItems.cx,
				rcRest.top + nRowNo * sizeCenturyItem.cy + ( nRowNo + 1 ) * sizeDistanceBetweenItems.cy
				);
			CRect rcItem( ptItem, sizeCenturyItem );
			listItems.AddTail( rcItem );
			COleDateTime _timeStep(
				nYear, _time.GetMonth(), _time.GetDay(),
				_time.GetHour(), _time.GetMinute(), _time.GetSecond() );
			listTimes.AddTail( _timeStep );
			nYear += 10;
		}
	}
	ASSERT( listItems.GetCount() == listTimes.GetCount() );

POSITION pos = listTimes.GetHeadPosition();
	for( ; pos != NULL; )
	{
		COleDateTime & _time = listTimes.GetNext( pos );
		if( OnDateBrowserQueryCenturyItemVisibility( _time ) )
			break;
		nInvisibleCountAtStart ++;
	}
	if( nInvisibleCountAtStart < listTimes.GetCount() )
	{
		pos = listTimes.GetTailPosition();
		for( ; pos != NULL; )
		{
			COleDateTime & _time = listTimes.GetPrev( pos );
			if( OnDateBrowserQueryCenturyItemVisibility( _time ) )
				break;
			nInvisibleCountAtEnd ++;
		}
	}
}

void CExtDateBrowserWnd::OnDateBrowserCalcLayoutYearDecade(
	CDC & dcMeasure,
	const COleDateTime & _time,
	CRect rcEntire,
	CRect & rcCaption,
	CRect & rcButtonPrevious,
	CRect & rcButtonNext,
	CList < CRect, CRect & > & listItems,
	CList < COleDateTime, COleDateTime & > & listTimes,
	INT & nInvisibleCountAtStart,
	INT & nInvisibleCountAtEnd
	)
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	nInvisibleCountAtStart = nInvisibleCountAtEnd = 0;
	listItems.RemoveAll();
	listTimes.RemoveAll();
CRect rcRest( 0, 0, 0, 0 );
	OnDateBrowserCalcLayoutGeneral(
		dcMeasure,
		__EVM_YEAR_DECADE,
		_time,
		rcEntire,
		rcCaption,
		rcButtonPrevious,
		rcButtonNext,
		rcRest
		);
CSize sizeYearDecadeItem = OnDateBrowserCalcYearDecadeItemSize( dcMeasure );
CSize sizeEntireItems(
		sizeYearDecadeItem.cx * __EXT_DATE_BROWSER_YEAR_DECADE_COLUMN_COUNT,
		sizeYearDecadeItem.cy * __EXT_DATE_BROWSER_YEAR_DECADE_ROW_COUNT
		);
CSize sizeDistanceBetweenItems(
		( rcRest.Width() - sizeEntireItems.cx ) / ( __EXT_DATE_BROWSER_YEAR_DECADE_COLUMN_COUNT + 1 ),
		( rcRest.Height() - sizeEntireItems.cy ) / ( __EXT_DATE_BROWSER_YEAR_DECADE_ROW_COUNT + 1 )
		);
CPoint ptLeftTop = rcRest.TopLeft();
INT nColNo, nRowNo;
LONG nYear, nTmp;
	stat_CalcYearDecadeDisplayYearRange(
		_time.GetYear(),
		nYear,
		nTmp
		);
	nYear --;
	for( nRowNo = 0; nRowNo < __EXT_DATE_BROWSER_YEAR_DECADE_ROW_COUNT; nRowNo++ )
	{
		for( nColNo = 0; nColNo < __EXT_DATE_BROWSER_YEAR_DECADE_COLUMN_COUNT; nColNo++ )
		{
			CPoint ptItem(
				rcRest.left + nColNo * sizeYearDecadeItem.cx + ( nColNo + 1 ) * sizeDistanceBetweenItems.cx,
				rcRest.top + nRowNo * sizeYearDecadeItem.cy + ( nRowNo + 1 ) * sizeDistanceBetweenItems.cy
				);
			CRect rcItem( ptItem, sizeYearDecadeItem );
			listItems.AddTail( rcItem );
			COleDateTime _timeStep(
				nYear, _time.GetMonth(), _time.GetDay(),
				_time.GetHour(), _time.GetMinute(), _time.GetSecond() );
			listTimes.AddTail( _timeStep );
			nYear ++;
		}
	}
	ASSERT( listItems.GetCount() == listTimes.GetCount() );

POSITION pos = listTimes.GetHeadPosition();
	for( ; pos != NULL; )
	{
		COleDateTime & _time = listTimes.GetNext( pos );
		if( OnDateBrowserQueryYearDecadeItemVisibility( _time ) )
			break;
		nInvisibleCountAtStart ++;
	}
	if( nInvisibleCountAtStart < listTimes.GetCount() )
	{
		pos = listTimes.GetTailPosition();
		for( ; pos != NULL; )
		{
			COleDateTime & _time = listTimes.GetPrev( pos );
			if( OnDateBrowserQueryYearDecadeItemVisibility( _time ) )
				break;
			nInvisibleCountAtEnd ++;
		}
	}
}

void CExtDateBrowserWnd::OnDateBrowserCalcLayoutOneYear(
	CDC & dcMeasure,
	const COleDateTime & _time,
	CRect rcEntire,
	CRect & rcCaption,
	CRect & rcButtonPrevious,
	CRect & rcButtonNext,
	CList < CRect, CRect & > & listItems,
	CList < COleDateTime, COleDateTime & > & listTimes,
	INT & nInvisibleCountAtStart,
	INT & nInvisibleCountAtEnd
	)
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	nInvisibleCountAtStart = nInvisibleCountAtEnd = 0;
	listItems.RemoveAll();
	listTimes.RemoveAll();
CRect rcRest( 0, 0, 0, 0 );
	OnDateBrowserCalcLayoutGeneral(
		dcMeasure,
		__EVM_ONE_YEAR,
		_time,
		rcEntire,
		rcCaption,
		rcButtonPrevious,
		rcButtonNext,
		rcRest
		);
CSize sizeOneYearItem = OnDateBrowserCalcOneYearItemSize( dcMeasure );
CSize sizeEntireItems(
		sizeOneYearItem.cx * __EXT_DATE_BROWSER_ONE_YEAR_COLUMN_COUNT,
		sizeOneYearItem.cy * __EXT_DATE_BROWSER_ONE_YEAR_ROW_COUNT
		);
CSize sizeDistanceBetweenItems(
		( rcRest.Width() - sizeEntireItems.cx ) / ( __EXT_DATE_BROWSER_ONE_YEAR_COLUMN_COUNT + 1 ),
		( rcRest.Height() - sizeEntireItems.cy ) / ( __EXT_DATE_BROWSER_ONE_YEAR_ROW_COUNT + 1 )
		);
CPoint ptLeftTop = rcRest.TopLeft();
INT nColNo, nRowNo;
INT nMonth = 1;
	for( nRowNo = 0; nRowNo < __EXT_DATE_BROWSER_ONE_YEAR_ROW_COUNT; nRowNo++ )
	{
		for( nColNo = 0; nColNo < __EXT_DATE_BROWSER_ONE_YEAR_COLUMN_COUNT; nColNo++ )
		{
			CPoint ptItem(
				rcRest.left + nColNo * sizeOneYearItem.cx + ( nColNo + 1 ) * sizeDistanceBetweenItems.cx,
				rcRest.top + nRowNo * sizeOneYearItem.cy + ( nRowNo + 1 ) * sizeDistanceBetweenItems.cy
				);
			CRect rcItem( ptItem, sizeOneYearItem );
			listItems.AddTail( rcItem );
			COleDateTime _timeStep(
				_time.GetYear(), nMonth, 1,
				_time.GetHour(), _time.GetMinute(), _time.GetSecond() );
			listTimes.AddTail( _timeStep );
//INTERNAL_TRACE_TIME( "     ", _timeStep );
			nMonth ++;
		}
	}
	ASSERT( listItems.GetCount() == listTimes.GetCount() );

POSITION pos = listTimes.GetHeadPosition();
	for( ; pos != NULL; )
	{
		COleDateTime & _time = listTimes.GetNext( pos );
		if( OnDateBrowserQueryOneYearItemVisibility( _time ) )
			break;
		nInvisibleCountAtStart ++;
	}
	if( nInvisibleCountAtStart < listTimes.GetCount() )
	{
		pos = listTimes.GetTailPosition();
		for( ; pos != NULL; )
		{
			COleDateTime & _time = listTimes.GetPrev( pos );
			if( OnDateBrowserQueryOneYearItemVisibility( _time ) )
				break;
			nInvisibleCountAtEnd ++;
		}
	}
}

void CExtDateBrowserWnd::OnDateBrowserCalcLayoutMonth(
	CDC & dcMeasure,
	const COleDateTime & _time,
	CRect rcEntire,
	CRect & rcCaption,
	CRect & rcMonthSeparator,
	CRect & rcButtonPrevious,
	CRect & rcButtonNext,
	CList < CRect, CRect & > & listItems,
	CList < COleDateTime, COleDateTime & > & listTimes,
	INT & nInvisibleCountAtStart,
	INT & nInvisibleCountAtEnd
	)
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	nInvisibleCountAtStart = nInvisibleCountAtEnd = 0;
	listItems.RemoveAll();
	listTimes.RemoveAll();
CRect rcRest( 0, 0, 0, 0 );
	OnDateBrowserCalcLayoutGeneral(
		dcMeasure,
		__EVM_MONTH,
		_time,
		rcEntire,
		rcCaption,
		rcButtonPrevious,
		rcButtonNext,
		rcRest
		);
INT nMonthSeparatorHeight = OnDateBrowserQueryMonthSeparatorHeight();
	rcMonthSeparator = rcRest;
	rcMonthSeparator.bottom = rcMonthSeparator.top + nMonthSeparatorHeight;
	rcRest.top += nMonthSeparatorHeight;
CSize sizeMonthItem = OnDateBrowserCalcMonthItemSize( dcMeasure );
CSize sizeEntireItems(
		sizeMonthItem.cx * __EXT_DATE_BROWSER_MONTH_COLUMN_COUNT,
		sizeMonthItem.cy * __EXT_DATE_BROWSER_MONTH_ROW_COUNT
		);
CSize sizeDistanceBetweenItems(
		( rcRest.Width() - sizeEntireItems.cx ) / ( __EXT_DATE_BROWSER_MONTH_COLUMN_COUNT + 1 ),
		( rcRest.Height() - sizeEntireItems.cy ) / ( __EXT_DATE_BROWSER_MONTH_ROW_COUNT + 1 )
		);
CPoint ptLeftTop = rcRest.TopLeft();
INT nColNo, nRowNo;
INT nDisplayedFirstDayOfWeek = OnDateBrowserQueryFirstDayOfWeekInMonthView(); // 0-Sun, 1-Mon, ...
	ASSERT( 0 <= nDisplayedFirstDayOfWeek && nDisplayedFirstDayOfWeek <= 6 );
INT nDayOfWeek = 0;
INT nCurrentMonth = _time.GetMonth();
COleDateTime _timeStep(
		_time.GetYear(), nCurrentMonth, 1,
		_time.GetHour(), _time.GetMinute(), _time.GetSecond()
		);
INT nFirstDayOfWeekInThisMonth = _timeStep.GetDayOfWeek() - 1;
INT nDaysShift = 0;
	if( nDisplayedFirstDayOfWeek > nFirstDayOfWeekInThisMonth )
	{
		nDaysShift = -7;
	}
COleDateTimeSpan _DaysShift( nDaysShift, 0, 0, 0 );
INT nDayCounter = 0;
POSITION posTimesSaved = NULL;
	for( nRowNo = 0; nRowNo < __EXT_DATE_BROWSER_MONTH_ROW_COUNT; nRowNo++ )
	{
		for( nColNo = 0; nColNo < __EXT_DATE_BROWSER_MONTH_COLUMN_COUNT; nColNo++ )
		{
			CPoint ptItem(
				rcRest.left + nColNo * sizeMonthItem.cx + ( nColNo + 1 ) * sizeDistanceBetweenItems.cx,
				rcRest.top + nRowNo * sizeMonthItem.cy + ( nRowNo + 1 ) * sizeDistanceBetweenItems.cy
				);
			CRect rcItem( ptItem, sizeMonthItem );
			listItems.AddTail( rcItem );
			if( nRowNo == 0 )
			{
				COleDateTime _timeStep;
				_timeStep.SetStatus( COleDateTime::invalid );
				listTimes.AddTail( _timeStep );
				if( nColNo == 0 )
					posTimesSaved = listTimes.GetHeadPosition();
			}
			else
			{
				COleDateTime _timeEffective;
				if( nDayOfWeek < nFirstDayOfWeekInThisMonth && nDayCounter < 7 )
				{
					COleDateTime _timeStepBack =
						_timeStep - COleDateTimeSpan( nFirstDayOfWeekInThisMonth - nDayCounter, 0, 0, 0 );
					_timeEffective =
						_timeStepBack + COleDateTimeSpan( nDisplayedFirstDayOfWeek, 0, 0, 0 );
					//listTimes.AddTail( _timeEffective );
					COleDateTime _timeToSave = _timeEffective + _DaysShift;
					listTimes.AddTail( _timeToSave );
				}
				else
				{
					_timeEffective =
						_timeStep + COleDateTimeSpan( nDisplayedFirstDayOfWeek, 0, 0, 0 );
					//listTimes.AddTail( _timeEffective );
					COleDateTime _timeToSave = _timeEffective + _DaysShift;
					listTimes.AddTail( _timeToSave );
					_timeStep += COleDateTimeSpan( 1L, 0L, 0L, 0L );
				}
				if( nRowNo == 1 )
				{
					ASSERT( posTimesSaved != NULL );
					//listTimes.SetAt( posTimesSaved, _timeEffective );
					COleDateTime _timeToSave = _timeEffective + _DaysShift;
					listTimes.SetAt( posTimesSaved, _timeToSave );
					listTimes.GetNext( posTimesSaved ); // move position to next
					ASSERT( posTimesSaved != NULL );
				}
				nDayOfWeek = ( nDayOfWeek == 6 ) ? 0 : ( nDayOfWeek + 1 );
				nDayCounter ++;
			}
		}
		if( nRowNo == 0 )
		{
			INT nMax = rcMonthSeparator.bottom;
			POSITION pos = listItems.GetHeadPosition();
			for( ; pos != NULL; )
			{
				CRect & rcItem = listItems.GetNext( pos );
				rcItem.OffsetRect( 0, -nMonthSeparatorHeight );
				nMax = max( nMax, rcItem.bottom );
			}
			rcMonthSeparator.OffsetRect( 0, nMax - rcMonthSeparator.top );
		}
	}
	ASSERT( listItems.GetCount() == listTimes.GetCount() );

POSITION pos = listTimes.GetHeadPosition();
	for( nColNo = 0; nColNo < __EXT_DATE_BROWSER_MONTH_COLUMN_COUNT; nColNo++ )
	{
		ASSERT( pos != NULL );
		listTimes.GetNext( pos );
	}
	for( ; pos != NULL; )
	{
		COleDateTime & _time = listTimes.GetNext( pos );
		if( OnDateBrowserQueryMonthItemVisibility( _time ) )
			break;
		nInvisibleCountAtStart ++;
	}
	if( nInvisibleCountAtStart < ( listTimes.GetCount() - __EXT_DATE_BROWSER_MONTH_COLUMN_COUNT ) )
	{
		pos = listTimes.GetTailPosition();
		for( ; pos != NULL; )
		{
			COleDateTime & _time = listTimes.GetPrev( pos );
			if( OnDateBrowserQueryMonthItemVisibility( _time ) )
				break;
			nInvisibleCountAtEnd ++;
		}
	}
}

void CExtDateBrowserWnd::stat_CalcMilleniumDisplayYearRange(
	LONG nSourceYear,
	LONG & nFirstYear,
	LONG & nLastYear
	)
{
	// nSourceYear == 1983 --> nFirstYear=1000   nLastYear=1999
	nFirstYear = nSourceYear / 1000;
	nFirstYear *= 1000;
	nLastYear = nFirstYear + 999;
}

void CExtDateBrowserWnd::stat_CalcCenturyDisplayYearRange(
	LONG nSourceYear,
	LONG & nFirstYear,
	LONG & nLastYear
	)
{
	// nSourceYear == 1983 --> nFirstYear=1900   nLastYear=1999
	nFirstYear = nSourceYear / 100;
	nFirstYear *= 100;
	nLastYear = nFirstYear + 99;
}

void CExtDateBrowserWnd::stat_CalcYearDecadeDisplayYearRange(
	LONG nSourceYear,
	LONG & nFirstYear,
	LONG & nLastYear
	)
{
	// nSourceYear == 1983 --> nFirstYear=1980   nLastYear=1989
	nFirstYear = nSourceYear / 10;
	nFirstYear *= 10;
	nLastYear = nFirstYear + 9;
}


CFont * CExtDateBrowserWnd::OnDateBrowserQueryCaptionFont(
	CExtDateBrowserWnd::e_view_mode_t _eVM
	) const
{
	ASSERT_VALID( this );
	_eVM;
	return CFont::FromHandle( (HFONT)::GetStockObject( DEFAULT_GUI_FONT ) );
}

CFont * CExtDateBrowserWnd::OnDateBrowserQueryItemFont(
	CExtDateBrowserWnd::e_view_mode_t _eVM
	) const
{
	ASSERT_VALID( this );
	_eVM;
	return CFont::FromHandle( (HFONT)::GetStockObject( DEFAULT_GUI_FONT ) );
}


CExtSafeString CExtDateBrowserWnd::OnDateBrowserQueryCaptionText(
	const COleDateTime & _time,
	CExtDateBrowserWnd::e_view_mode_t _eVM
	) const
{
	ASSERT_VALID( this );
CExtSafeString s;
	switch( _eVM )
	{
	case __EVM_CENTURY:
	{
		LONG nSourceYear = _time.GetYear(), nFirstYear, nLastYear;
		stat_CalcCenturyDisplayYearRange( nSourceYear, nFirstYear, nLastYear );
		s.Format( _T("%d-%d"), nFirstYear, nLastYear );
	}
	break;
	case __EVM_YEAR_DECADE:
	{
		LONG nSourceYear = _time.GetYear(), nFirstYear, nLastYear;
		stat_CalcYearDecadeDisplayYearRange( nSourceYear, nFirstYear, nLastYear );
		s.Format( _T("%d-%d"), nFirstYear, nLastYear );
	}
	break;
	case __EVM_ONE_YEAR:
	{
		s.Format( _T("%d"), _time.GetYear() );
	}
	break;
	case __EVM_MONTH:
	{
		INT nMonthNameLocale = LOCALE_SMONTHNAME1 + _time.GetMonth() - 1;
		LPTSTR lpMonthBuf = (LPTSTR)::LocalAlloc(LPTR, 200);
		VERIFY(
			g_ResourceManager->GetLocaleInfo(
				nMonthNameLocale,
				lpMonthBuf,
				200
				) != 0
			);
		CExtSafeString sLongMonthName = (LPTSTR)lpMonthBuf;
		::LocalFree( lpMonthBuf );
		// 	if( bMakeUpper )
		// 		sLongMonthName.MakeUpper();
		CExtSafeString strYear = _time.Format( _T("%Y") );
		s.Format( _T("%s, %s"), LPCTSTR(sLongMonthName), LPCTSTR(strYear) );
	}
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( _eVM )
	return s;
}

CExtSafeString CExtDateBrowserWnd::OnDateBrowserQueryCenturyItemText(
	const COleDateTime & _time
	) const
{
	ASSERT_VALID( this );
LONG nSourceYear = _time.GetYear(), nFirstYear, nLastYear;
	stat_CalcYearDecadeDisplayYearRange( nSourceYear, nFirstYear, nLastYear );
CExtSafeString s;
	s.Format( _T("%d-\n%d"), nFirstYear, nLastYear );
	return s;
}

CExtSafeString CExtDateBrowserWnd::OnDateBrowserQueryYearDecadeItemText(
	const COleDateTime & _time
	) const
{
	ASSERT_VALID( this );
LONG nSourceYear = _time.GetYear();
CExtSafeString s;
	s.Format( _T("%d"), nSourceYear );
	return s;
}

CExtSafeString CExtDateBrowserWnd::OnDateBrowserQueryOneYearItemText(
	const COleDateTime & _time
	) const
{
	ASSERT_VALID( this );
INT nMonthNameLocale = LOCALE_SABBREVMONTHNAME1 + _time.GetMonth() - 1;
LPTSTR lpMonthBuf = (LPTSTR)::LocalAlloc(LPTR, 200);
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			nMonthNameLocale,
			lpMonthBuf,
			200
			) != 0
		);
CExtSafeString sShortMonthName = (LPTSTR)lpMonthBuf;
	::LocalFree( lpMonthBuf );
// 	if( bMakeUpper )
// 		sShortMonthName.MakeUpper();
	return sShortMonthName;
}

CExtSafeString CExtDateBrowserWnd::OnDateBrowserQueryMonthItemText(
	const COleDateTime & _time
	) const
{
	ASSERT_VALID( this );
CExtSafeString s = _time.Format( _T("%d") );
	s.TrimLeft( _T("0") );
	return s;
}

CExtSafeString CExtDateBrowserWnd::OnDateBrowserQueryWeekItemText(
	INT _nDayOfWeek // 0-Sun, 1-Mon, ...
	) const
{
	ASSERT_VALID( this );
INT nDayNameLocale = LOCALE_SABBREVDAYNAME1 + _nDayOfWeek - 1;
LPTSTR lpDayOfWeekBuf = (LPTSTR)::LocalAlloc( LPTR, 100 );
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			nDayNameLocale,
			lpDayOfWeekBuf,
			100
			) != 0
		);
CExtSafeString sLongDayName = (LPTSTR)lpDayOfWeekBuf;
	::LocalFree( lpDayOfWeekBuf );
CExtSafeString sShortDayName;
	if( sLongDayName.GetLength() > 0 )
		sShortDayName = sLongDayName.GetAt(0);
	return sShortDayName;
}

INT CExtDateBrowserWnd::OnDateBrowserQueryFirstDayOfWeekInMonthView() const
{
	ASSERT_VALID( this );
	if( m_nFirstDayOfWeek < 0 )
	{
		LPTSTR lpFirstDayOfWeek = (LPTSTR)::LocalAlloc( LPTR, 24 );
		VERIFY(
			g_ResourceManager->GetLocaleInfo(
				LOCALE_IFIRSTDAYOFWEEK,
				lpFirstDayOfWeek,
				24
				) != 0
			);
		INT nFirstDayOfWeek = _ttoi( lpFirstDayOfWeek );
		::LocalFree( lpFirstDayOfWeek );
		if( nFirstDayOfWeek == 6 )
			nFirstDayOfWeek = 0;
		else
			nFirstDayOfWeek++;
		ASSERT( 0 <= nFirstDayOfWeek && nFirstDayOfWeek <= 6 );
		return nFirstDayOfWeek;
	}
	ASSERT( m_nFirstDayOfWeek <= 6 );
	return m_nFirstDayOfWeek;  // 0-Sun, 1-Mon, ...
}

CRect CExtDateBrowserWnd::OnDateBrowserQueryButtonPadding( bool bPrevious ) const
{
	ASSERT_VALID( this );
	bPrevious;
CExtPaintManager * pPM = PmBridge_GetPM();
	return
		CRect(
			pPM->UiScalingDo( 5, CExtPaintManager::__EUIST_X ),
			pPM->UiScalingDo( 5, CExtPaintManager::__EUIST_Y ),
			pPM->UiScalingDo( 5, CExtPaintManager::__EUIST_X ),
			pPM->UiScalingDo( 5, CExtPaintManager::__EUIST_Y )
			);
}

CRect CExtDateBrowserWnd::OnDateBrowserQueryCaptionPadding() const
{
	ASSERT_VALID( this );
CExtPaintManager * pPM = PmBridge_GetPM();
	return
		CRect(
			pPM->UiScalingDo( 30, CExtPaintManager::__EUIST_X ),
			pPM->UiScalingDo(  3, CExtPaintManager::__EUIST_Y ),
			pPM->UiScalingDo( 30, CExtPaintManager::__EUIST_X ),
			pPM->UiScalingDo(  3, CExtPaintManager::__EUIST_Y )
			);
}

CRect CExtDateBrowserWnd::OnDateBrowserQueryCenturyItemPadding() const
{
	ASSERT_VALID( this );
CExtPaintManager * pPM = PmBridge_GetPM();
	return 
		CRect( 
			pPM->UiScalingDo( 14, CExtPaintManager::__EUIST_X ),
			pPM->UiScalingDo( 11, CExtPaintManager::__EUIST_Y ),
			pPM->UiScalingDo( 12, CExtPaintManager::__EUIST_X ),
			pPM->UiScalingDo( 11, CExtPaintManager::__EUIST_Y )
			);
}

CRect CExtDateBrowserWnd::OnDateBrowserQueryYearDecadeItemPadding() const
{
	ASSERT_VALID( this );
CExtPaintManager * pPM = PmBridge_GetPM();
	return 
		CRect( 
			pPM->UiScalingDo( 15, CExtPaintManager::__EUIST_X ),
			pPM->UiScalingDo( 17, CExtPaintManager::__EUIST_Y ),
			pPM->UiScalingDo( 14, CExtPaintManager::__EUIST_X ),
			pPM->UiScalingDo( 17, CExtPaintManager::__EUIST_Y )
			);
}

CRect CExtDateBrowserWnd::OnDateBrowserQueryOneYearItemPadding() const
{
	ASSERT_VALID( this );
CExtPaintManager * pPM = PmBridge_GetPM();
	return 
		CRect( 
			pPM->UiScalingDo( 17, CExtPaintManager::__EUIST_X ),
			pPM->UiScalingDo( 17, CExtPaintManager::__EUIST_Y ),
			pPM->UiScalingDo( 16, CExtPaintManager::__EUIST_X ),
			pPM->UiScalingDo( 17, CExtPaintManager::__EUIST_Y )
			);
}

CRect CExtDateBrowserWnd::OnDateBrowserQueryMonthItemPadding() const
{
	ASSERT_VALID( this );
CExtPaintManager * pPM = PmBridge_GetPM();
	return 
		CRect( 
			pPM->UiScalingDo( 2, CExtPaintManager::__EUIST_X ),
			pPM->UiScalingDo( 2, CExtPaintManager::__EUIST_Y ),
			pPM->UiScalingDo( 2, CExtPaintManager::__EUIST_X ),
			pPM->UiScalingDo( 1, CExtPaintManager::__EUIST_Y )
			);
}

bool CExtDateBrowserWnd::OnDateBrowserQueryPaletteItemIndexSelectedState(
	const COleDateTime & _time,
	const COleDateTime & _timeCurrent,
	CExtDateBrowserWnd::e_view_mode_t _eVM
	) const
{
	ASSERT_VALID( this );
	switch( _eVM )
	{
	case __EVM_CENTURY:
	{
		LONG nSourceYear = _timeCurrent.GetYear(), nFirstYear, nLastYear;
		stat_CalcYearDecadeDisplayYearRange( nSourceYear, nFirstYear, nLastYear );
		LONG nTestYear = _time.GetYear();
		if( nFirstYear <= nTestYear && nTestYear < nLastYear )
			return true;
	}
	break;
	case __EVM_YEAR_DECADE:
	{
		LONG nSourceYear = _timeCurrent.GetYear();
		LONG nTestYear = _time.GetYear();
		if( nSourceYear == nTestYear )
			return true;
	}
	break;
	case __EVM_ONE_YEAR:
	{
		LONG nSourceYear = _timeCurrent.GetYear();
		LONG nTestYear = _time.GetYear();
		if( nSourceYear == nTestYear )
		{
			LONG nSourceMonth = _timeCurrent.GetMonth();
			LONG nTestMonth = _time.GetMonth();
			if( nSourceMonth == nTestMonth )
				return true;
		}
	}
	break;
	case __EVM_MONTH:
	{
		LONG nSourceYear = _timeCurrent.GetYear();
		LONG nTestYear = _time.GetYear();
		if( nSourceYear == nTestYear )
		{
			LONG nSourceMonth = _timeCurrent.GetMonth();
			LONG nTestMonth = _time.GetMonth();
			if( nSourceMonth == nTestMonth )
			{
				LONG nSourceDay = _timeCurrent.GetDay();
				LONG nTestDay = _time.GetDay();
				if( nSourceDay == nTestDay )
					return true;
			}
		}
	}
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( _eVM )
	return false;
}

CExtDateBrowserWnd::HITTESTINFO & CExtDateBrowserWnd::HitTest(
	CExtDateBrowserWnd::HITTESTINFO & _hitTestInfo
	) const
{
	ASSERT_VALID( this );
	_hitTestInfo.m_nAreaFlags = INT(__EHTIAF_NOWHERE);
	if( GetSafeHwnd() == NULL )
		return _hitTestInfo;
CRect rcEntire;
	GetClientRect( &rcEntire );
	if( ! rcEntire.PtInRect( _hitTestInfo.m_ptClient ) )
	{
		if(			_hitTestInfo.m_ptClient.x <= rcEntire.left )
			_hitTestInfo.m_nAreaFlags |= INT(__EHTIAF_OUTSIDE_AT_LEFT);
		else if(	_hitTestInfo.m_ptClient.x >= rcEntire.right )
			_hitTestInfo.m_nAreaFlags |= INT(__EHTIAF_OUTSIDE_AT_RIGHT);

		if(			_hitTestInfo.m_ptClient.y <= rcEntire.top )
			_hitTestInfo.m_nAreaFlags |= INT(__EHTIAF_OUTSIDE_AT_TOP);
		else if(	_hitTestInfo.m_ptClient.y >= rcEntire.bottom )
			_hitTestInfo.m_nAreaFlags |= INT(__EHTIAF_OUTSIDE_AT_BOTTOM);

		return _hitTestInfo;
	}
CPoint ptScreen = _hitTestInfo.m_ptClient;
	ClientToScreen( &ptScreen );
	if( ::WindowFromPoint( ptScreen ) != m_hWnd )
		return _hitTestInfo;

CRect rcCaption( 0, 0, 0, 0 ), rcButtonPrevious( 0, 0, 0, 0 ), rcButtonNext( 0, 0, 0, 0 ), rcMonthSeparator( 0, 0, 0, 0 );
CList < CRect, CRect & > listItems;
CList < COleDateTime, COleDateTime & > listTimes;
CClientDC dc( const_cast < CExtDateBrowserWnd * > ( this ) );
e_view_mode_t eVM = ViewModeGet();
INT nInvisibleCountAtStart = 0, nInvisibleCountAtEnd = 0;
	switch( eVM )
	{
	case __EVM_CENTURY:
	{
		( const_cast < CExtDateBrowserWnd * > ( this ) ) ->
			OnDateBrowserCalcLayoutCentury(
				dc,
				TimeGet(),
				rcEntire,
				rcCaption,
				rcButtonPrevious,
				rcButtonNext,
				listItems,
				listTimes,
				nInvisibleCountAtStart,
				nInvisibleCountAtEnd
				);
	}
	break;
	case __EVM_YEAR_DECADE:
	{
		( const_cast < CExtDateBrowserWnd * > ( this ) ) ->
			OnDateBrowserCalcLayoutYearDecade(
				dc,
				TimeGet(),
				rcEntire,
				rcCaption,
				rcButtonPrevious,
				rcButtonNext,
				listItems,
				listTimes,
				nInvisibleCountAtStart,
				nInvisibleCountAtEnd
				);
	}
	break;
	case __EVM_ONE_YEAR:
	{
		( const_cast < CExtDateBrowserWnd * > ( this ) ) ->
			OnDateBrowserCalcLayoutOneYear(
				dc,
				TimeGet(),
				rcEntire,
				rcCaption,
				rcButtonPrevious,
				rcButtonNext,
				listItems,
				listTimes,
				nInvisibleCountAtStart,
				nInvisibleCountAtEnd
				);
	}
	break;
	case __EVM_MONTH:
	{
		( const_cast < CExtDateBrowserWnd * > ( this ) ) ->
			OnDateBrowserCalcLayoutMonth(
				dc,
				TimeGet(),
				rcEntire,
				rcCaption,
				rcMonthSeparator,
				rcButtonPrevious,
				rcButtonNext,
				listItems,
				listTimes,
				nInvisibleCountAtStart,
				nInvisibleCountAtEnd
				);
	}
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( eVM )
	ASSERT( listItems.GetCount() == listTimes.GetCount() );

POSITION posItems = listItems.GetHeadPosition();
POSITION posTimes = listTimes.GetHeadPosition();
	ASSERT(
			( posItems != NULL && posTimes != NULL )
		||	( posItems == NULL && posTimes == NULL )
		);
INT nItemIndex = 0;
	for( ; posItems != NULL; nItemIndex++ )
	{
		CRect & rcItem = listItems.GetNext( posItems );
		COleDateTime & _time = listTimes.GetNext( posTimes );
		ASSERT(
				( posItems != NULL && posTimes != NULL )
			||	( posItems == NULL && posTimes == NULL )
			);
		if( rcItem.PtInRect( _hitTestInfo.m_ptClient ) )
		{
			_hitTestInfo.m_nAreaFlags |= INT(__EHTIAF_ON_ITEM);
			_hitTestInfo.m_nItemIndex = nItemIndex;
			_hitTestInfo.m_time = _time;
			if( eVM == __EVM_MONTH && nItemIndex < 7 )
				_hitTestInfo.m_nAreaFlags |= INT(__EHTIAF_ON_MONTH_HEADER_ITEM);
			return _hitTestInfo;
		}
	}

	if( rcButtonPrevious.PtInRect( _hitTestInfo.m_ptClient ) )
	{
		_hitTestInfo.m_nAreaFlags |= INT(__EHTIAF_ON_BUTTON_PREVIOUS);
		return _hitTestInfo;
	}
	if( rcButtonNext.PtInRect( _hitTestInfo.m_ptClient ) )
	{
		_hitTestInfo.m_nAreaFlags |= INT(__EHTIAF_ON_BUTTON_NEXT);
		return _hitTestInfo;
	}
	if( rcCaption.PtInRect( _hitTestInfo.m_ptClient ) )
	{
		_hitTestInfo.m_nAreaFlags |= INT(__EHTIAF_ON_CAPTION);
		return _hitTestInfo;
	}
	return _hitTestInfo;
}

void CExtDateBrowserWnd::OnSetFocus(CWnd* pOldWnd) 
{
	CWnd::OnSetFocus(pOldWnd);
	Invalidate();
	UpdateWindow();
}

void CExtDateBrowserWnd::OnKillFocus(CWnd* pNewWnd) 
{
	CWnd::OnKillFocus(pNewWnd);
	Invalidate();
	UpdateWindow();
}

int CExtDateBrowserWnd::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message) 
{
	pDesktopWnd;
	nHitTest;
	message;
	return MA_ACTIVATE;
}

void CExtDateBrowserWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
	nFlags;
	point;

	if( m_bPressedLeftRightButton )
		return;

CExtDateBrowserWnd::HITTESTINFO	_hitTest( point );
	HitTest( _hitTest );
	if( _hitTest == m_hitTestLast )
		return;
	if( ! OnDateBrowserHoverChanging( m_hitTestLast, _hitTest ) )
		return;
CExtDateBrowserWnd::HITTESTINFO _hitTestPrevious = m_hitTestLast;
	m_hitTestLast = _hitTest;
	OnDateBrowserHoverChanged( _hitTestPrevious, m_hitTestLast );

int nScrollingDirection = 0;

	if( ( m_hitTestLast.m_nAreaFlags & __EHTIAF_INSIDE_CONTROL_MASK ) != 0 )
	{
		if( GetCapture() != this )
			SetCapture();
	}
	else
	{
		if( ! m_bPressedTracking )
		{
			if( GetCapture() == this )
				ReleaseCapture();
		}
		else
		{
			if(		( m_hitTestLast.m_nAreaFlags & __EHTIAF_OUTSIDE_AT_LEFT ) != 0 
				||	(	( m_hitTestLast.m_nAreaFlags & __EHTIAF_OUTSIDE_AT_TOP ) != 0 
					&&	( m_hitTestLast.m_nAreaFlags & __EHTIAF_OUTSIDE_AT_RIGHT ) == 0 
					)
				)
				nScrollingDirection = -1;
			else if(	( m_hitTestLast.m_nAreaFlags & __EHTIAF_OUTSIDE_AT_RIGHT ) != 0 
				||	(	( m_hitTestLast.m_nAreaFlags & __EHTIAF_OUTSIDE_AT_BOTTOM ) != 0 
					&&	( m_hitTestLast.m_nAreaFlags & __EHTIAF_OUTSIDE_AT_LEFT ) == 0 
					)
				)
				nScrollingDirection = 1;
		}
	}

	if( m_nScrollingDirection != nScrollingDirection )
	{
		m_nScrollingDirection = nScrollingDirection;
		if( m_nScrollingDirection != 0 )
			SetTimer( m_nTimerID, m_nTimerEllapse, NULL );
		else
			KillTimer( m_nTimerID );
	}

	Invalidate();
}

void CExtDateBrowserWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	nFlags;
	point;

	m_bPressedTracking = true;
	if( ::GetFocus() != m_hWnd )
		SetFocus();
CExtDateBrowserWnd::HITTESTINFO	_hitTest( point );
	HitTest( _hitTest );
	if( m_hitTestLast != _hitTest )
	{
		if( ! OnDateBrowserHoverChanging( m_hitTestLast, _hitTest ) )
			return;
		CExtDateBrowserWnd::HITTESTINFO _hitTestPrevious = m_hitTestLast;
		m_hitTestLast = _hitTest;
		OnDateBrowserHoverChanged( _hitTestPrevious, m_hitTestLast );
	}
	if( GetCapture() != this )
		SetCapture();

	if( ( _hitTest.m_nAreaFlags & __EHTIAF_ON_BUTTON_PREVIOUS ) != 0)
	{
		if( m_nScrollingDirection == 0 )
			SetTimer( m_nTimerID, m_nTimerEllapse, NULL );
		m_nScrollingDirection = -1;
		m_bPressedLeftRightButton = true;
	}
	if( ( _hitTest.m_nAreaFlags & __EHTIAF_ON_BUTTON_NEXT ) != 0)
	{
		if( m_nScrollingDirection == 0 )
			SetTimer( m_nTimerID, m_nTimerEllapse, NULL );
		m_nScrollingDirection = 1;
		m_bPressedLeftRightButton = true;
	}

	Invalidate();
}

void CExtDateBrowserWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
	nFlags;
	point;
	KillTimer( m_nTimerID );
	m_bPressedLeftRightButton = false;

	if( ! m_bPressedTracking )
		return;

COleDateTime _timeOld = TimeGet();
CExtDateBrowserWnd::HITTESTINFO	_hitTest( point );
	HitTest( _hitTest );
	if( m_hitTestLast != _hitTest )
	{
		CExtDateBrowserWnd::HITTESTINFO _hitTestPrevious = m_hitTestLast;
		m_hitTestLast = _hitTest;
		OnDateBrowserHoverChanged( _hitTestPrevious, m_hitTestLast );
	}
	m_bPressedTracking = false;
	if( GetCapture() == this )
	{
		ReleaseCapture();
		Invalidate();
	}

e_view_mode_t eVM_old = ViewModeGet();

	if(	( _hitTest.m_nAreaFlags & __EHTIAF_ON_CAPTION ) != 0 )
	{
		bool bRepainted;

		switch( eVM_old )
		{
		case __EVM_CENTURY:
			// do nothing
		break;
		case __EVM_YEAR_DECADE:
		{
			e_view_mode_t eVM_new = __EVM_CENTURY;
			if( ! OnDateBrowserViewModeChanging( eVM_old, eVM_new ) )
				return;
			ViewModeSet( eVM_new );
			bRepainted = OnDateBrowserAnimateViewModeChanging( eVM_old, eVM_new );
			OnDateBrowserViewModeChanged( eVM_old, eVM_new );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
		case __EVM_ONE_YEAR:
		{
			e_view_mode_t eVM_new = __EVM_YEAR_DECADE;
			if( ! OnDateBrowserViewModeChanging( eVM_old, eVM_new ) )
				return;
			ViewModeSet( eVM_new );
			bRepainted = OnDateBrowserAnimateViewModeChanging( eVM_old, eVM_new );
			OnDateBrowserViewModeChanged( eVM_old, eVM_new );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
		case __EVM_MONTH:
		{
			e_view_mode_t eVM_new = __EVM_ONE_YEAR;
			if( ! OnDateBrowserViewModeChanging( eVM_old, eVM_new ) )
				return;
			ViewModeSet( eVM_new );
			bRepainted = OnDateBrowserAnimateViewModeChanging( eVM_old, eVM_new );
			OnDateBrowserViewModeChanged( eVM_old, eVM_new );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
#ifdef _DEBUG
		default:
			ASSERT( FALSE );
		break;
#endif // _DEBUG
		} // switch( eVM_old )
		return;
	} // if( ( _hitTest.m_nAreaFlags & __EHTIAF_ON_CAPTION ) != 0 )

	if( ( _hitTest.m_nAreaFlags & __EHTIAF_ON_ITEM ) != 0 )
	{
		ASSERT( _hitTest.m_nItemIndex >= 0 );
		CRect rcEntire, rcCaption( 0, 0, 0, 0 ), rcButtonPrevious( 0, 0, 0, 0 ), rcButtonNext( 0, 0, 0, 0 ), rcMonthSeparator( 0, 0, 0, 0 );
		GetClientRect( &rcEntire );
		CClientDC dc( this );
		CList < CRect, CRect & > listItems;
		CList < COleDateTime, COleDateTime & > listTimes;
		INT nInvisibleCountAtStart = 0, nInvisibleCountAtEnd = 0;
		switch( eVM_old )
		{
		case __EVM_CENTURY:
			OnDateBrowserCalcLayoutCentury(
				dc,
				TimeGet(),
				rcEntire,
				rcCaption,
				rcButtonPrevious,
				rcButtonNext,
				listItems,
				listTimes,
				nInvisibleCountAtStart,
				nInvisibleCountAtEnd
				);
			ASSERT( listItems.GetCount() == listTimes.GetCount() );
			ASSERT( listItems.GetCount() == __EXT_DATE_BROWSER_CENTURY_ITEM_COUNT );
		break;
		case __EVM_YEAR_DECADE:
			OnDateBrowserCalcLayoutYearDecade(
				dc,
				TimeGet(),
				rcEntire,
				rcCaption,
				rcButtonPrevious,
				rcButtonNext,
				listItems,
				listTimes,
				nInvisibleCountAtStart,
				nInvisibleCountAtEnd
				);
			ASSERT( listItems.GetCount() == listTimes.GetCount() );
			ASSERT( listItems.GetCount() == __EXT_DATE_BROWSER_YEAR_DECADE_ITEM_COUNT );
		break;
		case __EVM_ONE_YEAR:
			OnDateBrowserCalcLayoutOneYear(
				dc,
				TimeGet(),
				rcEntire,
				rcCaption,
				rcButtonPrevious,
				rcButtonNext,
				listItems,
				listTimes,
				nInvisibleCountAtStart,
				nInvisibleCountAtEnd
				);
			ASSERT( listItems.GetCount() == listTimes.GetCount() );
			ASSERT( listItems.GetCount() == __EXT_DATE_BROWSER_ONE_YEAR_ITEM_COUNT );
		break;
		case __EVM_MONTH:
			if( ( _hitTest.m_nAreaFlags & __EHTIAF_ON_MONTH_HEADER_ITEM ) != 0 )
				return;
			OnDateBrowserCalcLayoutMonth(
				dc,
				TimeGet(),
				rcEntire,
				rcCaption,
				rcMonthSeparator,
				rcButtonPrevious,
				rcButtonNext,
				listItems,
				listTimes,
				nInvisibleCountAtStart,
				nInvisibleCountAtEnd
				);
			ASSERT( listItems.GetCount() == listTimes.GetCount() );
			ASSERT( listItems.GetCount() == __EXT_DATE_BROWSER_MONTH_ITEM_COUNT );
		break;
#ifdef _DEBUG
		default:
			ASSERT( FALSE );
		break;
#endif // _DEBUG
		} // switch( eVM_old )
		POSITION posItems = listItems.GetHeadPosition();
		POSITION posTimes = listTimes.GetHeadPosition();
		ASSERT(
				( posItems != NULL && posTimes != NULL )
			||	( posItems == NULL && posTimes == NULL )
			);
		COleDateTime _timeNew = TimeGet();
// 		if( eVM_old == __EVM_MONTH )
// 		{
// 			_timeNew = _hitTest.m_time;
// 		}
// 		else
		{
			INT nShift;
			for( nShift = 0; true; nShift++ )
			{
				ASSERT( posItems != NULL );
				CRect & rcItem = listItems.GetNext( posItems );
				rcItem;
				COleDateTime & _time = listTimes.GetNext( posTimes );
				_timeNew = _time;
				ASSERT(
						( posItems != NULL && posTimes != NULL )
					||	( posItems == NULL && posTimes == NULL )
					);
				if( nShift >= _hitTest.m_nItemIndex )
					break;
			}
		}
		if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
			return;
		VERIFY( TimeSet( _timeNew ) );
//bool bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
		bool bRepainted = false;
		if( eVM_old == __EVM_MONTH )
			bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
		OnDateBrowserTimeChanged( _timeOld, _timeNew );
		switch( eVM_old )
		{
		case __EVM_CENTURY:
		{
			e_view_mode_t eVM_new = __EVM_YEAR_DECADE;
			if( ! OnDateBrowserViewModeChanging( eVM_old, eVM_new ) )
				return;
			ViewModeSet( eVM_new );
			bRepainted = OnDateBrowserAnimateViewModeChanging( eVM_old, eVM_new );
			OnDateBrowserViewModeChanged( eVM_old, eVM_new );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
		case __EVM_YEAR_DECADE:
		{
			e_view_mode_t eVM_new = __EVM_ONE_YEAR;
			if( ! OnDateBrowserViewModeChanging( eVM_old, eVM_new ) )
				return;
			ViewModeSet( eVM_new );
			bRepainted = OnDateBrowserAnimateViewModeChanging( eVM_old, eVM_new );
			OnDateBrowserViewModeChanged( eVM_old, eVM_new );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
		case __EVM_ONE_YEAR:
		{
			e_view_mode_t eVM_new = __EVM_MONTH;
			if( ! OnDateBrowserViewModeChanging( eVM_old, eVM_new ) )
				return;
			ViewModeSet( eVM_new );
			bRepainted = OnDateBrowserAnimateViewModeChanging( eVM_old, eVM_new );
			OnDateBrowserViewModeChanged( eVM_old, eVM_new );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
		case __EVM_MONTH:
			Invalidate();
		break;
#ifdef _DEBUG
		default:
			ASSERT( FALSE );
		break;
#endif // _DEBUG
		} // switch( eVM_old )
		return;
	} // if( ( _hitTest.m_nAreaFlags & __EHTIAF_ON_ITEM ) != 0 )

	if( ( _hitTest.m_nAreaFlags & __EHTIAF_ON_BUTTON_NEXT ) != 0 )
	{
		bool bRepainted;
		switch( eVM_old )
		{
		case __EVM_CENTURY:
		{
			COleDateTime _timeNew;
			_timeNew.SetDateTime(_timeOld.GetYear()+100 - _timeOld.GetYear()%100, _timeOld.GetMonth(), /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
			if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
				return;
			VERIFY( TimeSet( _timeNew ) );
			bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
			OnDateBrowserTimeChanged( _timeOld, _timeNew );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
		case __EVM_YEAR_DECADE:
		{
			COleDateTime _timeNew;
			_timeNew.SetDateTime(_timeOld.GetYear()+10 - _timeOld.GetYear()%10, _timeOld.GetMonth(), /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
			if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
				return;
			VERIFY( TimeSet( _timeNew ) );
			bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
			OnDateBrowserTimeChanged( _timeOld, _timeNew );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
		case __EVM_ONE_YEAR:
		{
			COleDateTime _timeNew;
			_timeNew.SetDateTime(_timeOld.GetYear()+1, /*_timeOld.GetMonth()*/ 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
			if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
				return;
			VERIFY( TimeSet( _timeNew ) );
			bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
			OnDateBrowserTimeChanged( _timeOld, _timeNew );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
		case __EVM_MONTH:
		{
			COleDateTime _timeNew;
			if ( _timeOld.GetMonth() == 12 )
				_timeNew.SetDateTime(_timeOld.GetYear() + 1, 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
			else
				_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() + 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
			if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
				return;
			VERIFY( TimeSet( _timeNew ) );
			bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
			OnDateBrowserTimeChanged( _timeOld, _timeNew );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
#ifdef _DEBUG
		default:
			ASSERT( FALSE );
		break;
#endif // _DEBUG
		} // switch( eVM_old )
		return;
	} // if( ( _hitTest.m_nAreaFlags & __EHTIAF_ON_BUTTON_NEXT ) != 0 )

	if( ( _hitTest.m_nAreaFlags & __EHTIAF_ON_BUTTON_PREVIOUS ) != 0 )
	{
		bool bRepainted;
		switch( eVM_old )
		{
		case __EVM_CENTURY:
		{
			COleDateTime _timeNew;
			_timeNew.SetDateTime(_timeOld.GetYear()-100 - _timeOld.GetYear()%100, _timeOld.GetMonth(), /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
			if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
				return;
			VERIFY( TimeSet( _timeNew ) );
			bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
			OnDateBrowserTimeChanged( _timeOld, _timeNew );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
		case __EVM_YEAR_DECADE:
		{
			COleDateTime _timeNew;
			_timeNew.SetDateTime(_timeOld.GetYear()-10 - _timeOld.GetYear()%10, _timeOld.GetMonth(), /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
			if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
				return;
			VERIFY( TimeSet( _timeNew ) );
			bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
			OnDateBrowserTimeChanged( _timeOld, _timeNew );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
		case __EVM_ONE_YEAR:
		{
			COleDateTime _timeNew;
			_timeNew.SetDateTime(_timeOld.GetYear()-1, /*_timeOld.GetMonth()*/ 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
			if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
				return;
			VERIFY( TimeSet( _timeNew ) );
			bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
			OnDateBrowserTimeChanged( _timeOld, _timeNew );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
		case __EVM_MONTH:
		{
			COleDateTime _timeNew;
			if ( _timeOld.GetMonth() == 1 )
				_timeNew.SetDateTime(_timeOld.GetYear() - 1, 12, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
			else
				_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() - 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
			if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
				return;
			VERIFY( TimeSet( _timeNew ) );
			bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
			OnDateBrowserTimeChanged( _timeOld, _timeNew );
			if( ! bRepainted )
			{
				Invalidate();
				UpdateWindow();
			}
		}
		break;
#ifdef _DEBUG
		default:
			ASSERT( FALSE );
		break;
#endif // _DEBUG
		} // switch( eVM_old )
		return;
	} // if( ( _hitTest.m_nAreaFlags & __EHTIAF_ON_BUTTON_PREVIOUS ) != 0 )

}

void CExtDateBrowserWnd::OnTimer(__EXT_MFC_UINT_PTR nIDEvent) 
{
	if( nIDEvent == m_nTimerID )
	{
		COleDateTime _timeOld = TimeGet(), _timeNew;
		e_view_mode_t eVM = ViewModeGet();
		if( m_nScrollingDirection < 0 )
		{
			bool bRepainted;
			switch( eVM )
			{
			case __EVM_CENTURY:
				_timeNew.SetDateTime(_timeOld.GetYear()-100 - _timeOld.GetYear()%100, _timeOld.GetMonth(), /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			break;
			case __EVM_YEAR_DECADE:
				_timeNew.SetDateTime(_timeOld.GetYear()-10 - _timeOld.GetYear()%10, _timeOld.GetMonth(), /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			break;
			case __EVM_ONE_YEAR:
				_timeNew.SetDateTime(_timeOld.GetYear()-1, /*_timeOld.GetMonth()*/ 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			break;
			case __EVM_MONTH:
				if ( _timeOld.GetMonth() == 1 )
					_timeNew.SetDateTime(_timeOld.GetYear() - 1, 12, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				else
					_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() - 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			break;
#ifdef _DEBUG
			default:
				ASSERT( FALSE );
			break;
#endif // _DEBUG
			} // switch( eVM )		
		} // if( m_nScrollingDirection < 0 )
		else if( m_nScrollingDirection > 0 )
		{
			bool bRepainted;
			switch( eVM )
			{
			case __EVM_CENTURY:
				_timeNew.SetDateTime(_timeOld.GetYear()+100 - _timeOld.GetYear()%100, _timeOld.GetMonth(), /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			break;
			case __EVM_YEAR_DECADE:
				_timeNew.SetDateTime(_timeOld.GetYear()+10 - _timeOld.GetYear()%10, _timeOld.GetMonth(), /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			break;
			case __EVM_ONE_YEAR:
				_timeNew.SetDateTime(_timeOld.GetYear()+1, /*_timeOld.GetMonth()*/ 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			break;
			case __EVM_MONTH:
				if ( _timeOld.GetMonth() == 12 )
					_timeNew.SetDateTime(_timeOld.GetYear() + 1, 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				else
					_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() + 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			break;
#ifdef _DEBUG
			default:
				ASSERT( FALSE );
			break;
#endif // _DEBUG
			} // switch( eVM )
		} // else if( m_nScrollingDirection > 0 )
		else
			KillTimer( m_nTimerID );
		return;
	}

	CWnd::OnTimer( nIDEvent );
}

void CExtDateBrowserWnd::OnCancelMode()
{
	CWnd::OnCancelMode();
	
	KillTimer( m_nTimerID );
	if( GetCapture() == this )
	{
		ReleaseCapture();
		Invalidate();
	}
	if( m_bPressedTracking || m_nScrollingDirection != 0 )
	{
		m_nScrollingDirection = 0;
		m_bPressedTracking = false;
		Invalidate();
	}
}

COleDateTime CExtDateBrowserWnd::TimeGet(
	INT nType // = 0
	) const
{
	ASSERT_VALID( this );
	if( nType < 0 )
		return m_timeMin;
	if( nType > 0 )
		return m_timeMax;
	return m_timeCurrent;
}

bool CExtDateBrowserWnd::TimeSet(
	const COleDateTime & _time,
	INT nType // = 0
	)
{
	ASSERT_VALID( this );
	if( nType < 0 )
	{
		m_timeMin = _time;
		return true;
	}
	if( nType > 0 )
	{
		m_timeMax = _time;
		return true;
	}
	if( TimeGet(-1) <= _time && _time <= TimeGet(1) )
	{
		m_timeCurrent = _time;
		return true;
	}
	return false;
}

bool CExtDateBrowserWnd::OnDateBrowserTimeChanging(
	const COleDateTime & _timeOld,
	const COleDateTime & _timeNew
	)
{
	ASSERT_VALID( this );
	_timeOld;
	if( TimeGet(-1) <= _timeNew && _timeNew <= TimeGet(1) )
		return true;
	return false;
}

void CExtDateBrowserWnd::OnDateBrowserTimeChanged(
	const COleDateTime & _timeOld,
	const COleDateTime & _timeNew
	)
{
	ASSERT_VALID( this );
	_timeOld;
	_timeNew;
}
	
CExtDateBrowserWnd::e_view_mode_t CExtDateBrowserWnd::ViewModeGet() const
{
	ASSERT_VALID( this );
	return m_eVM;
}

void CExtDateBrowserWnd::ViewModeSet(
	CExtDateBrowserWnd::e_view_mode_t eVM
	)
{
	ASSERT_VALID( this );
	m_eVM = eVM;
}

bool CExtDateBrowserWnd::OnDateBrowserViewModeChanging(
	CExtDateBrowserWnd::e_view_mode_t eVM_old,
	CExtDateBrowserWnd::e_view_mode_t eVM_new
	)
{
	ASSERT_VALID( this );
	eVM_old;
	eVM_new;
	return true;
}

void CExtDateBrowserWnd::OnDateBrowserViewModeChanged(
	CExtDateBrowserWnd::e_view_mode_t eVM_old,
	CExtDateBrowserWnd::e_view_mode_t eVM_new
	)
{
	ASSERT_VALID( this );
	eVM_old;
	eVM_new;
}

bool CExtDateBrowserWnd::OnDateBrowserQueryTodayFlag(
	const COleDateTime & _time,
	CExtDateBrowserWnd::e_view_mode_t eVM,
	const COleDateTime * pCurrentTime // = NULL
	) const
{
	ASSERT_VALID( this );
COleDateTime _currentTime;
	if( pCurrentTime != NULL && pCurrentTime->GetStatus() == COleDateTime::valid )
		_currentTime = *pCurrentTime;
	else
	{
		COleDateTime _ct = COleDateTime::GetCurrentTime();
		_currentTime.SetDateTime( _ct.GetYear(), _ct.GetMonth(), _ct.GetDay(), _ct.GetHour(), _ct.GetMinute(), _ct.GetSecond() );
	}
	switch( eVM )
	{
	case __EVM_CENTURY:
		{
			LONG nSourceYear = _time.GetYear(), nFirstYear, nLastYear, nTestYear = _currentTime.GetYear();
			stat_CalcYearDecadeDisplayYearRange( nSourceYear, nFirstYear, nLastYear );
			if( nFirstYear <= nTestYear && nTestYear <= nLastYear )
				return true;
		}
	break;
	case __EVM_YEAR_DECADE:
		if( _time.GetYear() == _currentTime.GetYear() )
			return true;
	break;
	case __EVM_ONE_YEAR:
		if(		_time.GetYear() == _currentTime.GetYear()
			&&	_time.GetMonth() == _currentTime.GetMonth()
			)
			return true;
	break;
	case __EVM_MONTH:
	{
		if(		_time.GetYear() == _currentTime.GetYear()
			&&	_time.GetMonth() == _currentTime.GetMonth()
			&&	_time.GetDay() == _currentTime.GetDay()
			)
			return true;
	}
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( eVM )
	return false;
}

COLORREF CExtDateBrowserWnd::OnDateBrowserGetBackgroundColor() const
{
	ASSERT_VALID( this );
	if( PaintStyleGet() == __EPST_SKIN )
		return m_skin.m_clrBackground;
COLORREF clr = BackgroundColorGet();
	if( clr == COLORREF(-1L) )
		clr = PmBridge_GetPM()->GetSysColor( COLOR_WINDOW );
	return clr;
}

COLORREF CExtDateBrowserWnd::BackgroundColorGet() const
{
	ASSERT_VALID( this );
	return m_clrBackground;
}

void CExtDateBrowserWnd::BackgroundColorSet( COLORREF clr )
{
	ASSERT_VALID( this );
	if( m_clrBackground == clr )
		return;
	m_clrBackground = clr;
	if( GetSafeHwnd() != NULL )
		Invalidate();
}

CExtDateBrowserWnd::e_paint_style_t CExtDateBrowserWnd::PaintStyleGet() const
{
	ASSERT_VALID( this );
	return m_ePS;
}

void CExtDateBrowserWnd::PaintStyleSet( CExtDateBrowserWnd::e_paint_style_t _ePS )
{
	ASSERT_VALID( this );
	m_ePS = _ePS;
}


COLORREF CExtDateBrowserWnd::OnDateBrowserQueryCaptionTextColor(
	CExtDateBrowserWnd::e_view_mode_t _eVM,
	CExtDateBrowserWnd::e_item_state_t eIST,
	CExtDateBrowserWnd::e_painted_item_type_t ePIT,
	bool bWindowFocused
	)
{
	ASSERT_VALID( this );
	_eVM;
	if( PaintStyleGet() == __EPST_SKIN )
		return m_skin.GetCaptionTextColor( eIST, ePIT, bWindowFocused );
	switch( eIST )
	{
	case __EIST_DISABLED:
		return PmBridge_GetPM()->GetSysColor( COLOR_BTNFACE );
	break;
	case __EIST_HOVER:
	case __EIST_SELECTED_HOVER:
		return CExtBitmap::stat_HLS_Adjust( PmBridge_GetPM()->GetSysColor(COLOR_HIGHLIGHT), 0.0, 0.5, 0.0 );
	break;
	case __EIST_NORMAL:
	case __EIST_SELECTED_NORMAL:
		return PmBridge_GetPM()->GetSysColor( COLOR_WINDOWTEXT );
	break;
	case __EIST_PRESSED:
	case __EIST_SELECTED_PRESSED:
		return CExtBitmap::stat_HLS_Adjust( PmBridge_GetPM()->GetSysColor(COLOR_HIGHLIGHT), 0.0, -0.15, 0.0 );
	break;
	}
	ASSERT( FALSE );
	return RGB(0,0,0);
}

COLORREF CExtDateBrowserWnd::OnDateBrowserQueryCaptionBackgroundColor(
	CExtDateBrowserWnd::e_view_mode_t _eVM,
	CExtDateBrowserWnd::e_item_state_t eIST,
	CExtDateBrowserWnd::e_painted_item_type_t ePIT,
	bool bWindowFocused
	)
{
	ASSERT_VALID( this );
	_eVM;
	eIST;
	ePIT;
	bWindowFocused;
	return COLORREF(-1L);
}

COLORREF CExtDateBrowserWnd::OnDateBrowserQueryPaletteItemTextColor(
	CExtDateBrowserWnd::e_view_mode_t _eVM,
	CExtDateBrowserWnd::e_item_state_t eIST,
	CExtDateBrowserWnd::e_painted_item_type_t ePIT,
	bool bWindowFocused,
	bool bToday
	)
{
	ASSERT_VALID( this );
	_eVM;
	if( PaintStyleGet() == __EPST_SKIN )
		return m_skin.GetItemTextColor( eIST, ePIT, bWindowFocused, bToday );
	if( ePIT == __EPIT_WEEK_CAPTION_ITEM )
		return PmBridge_GetPM()->GetSysColor( COLOR_WINDOWTEXT );
COLORREF clr = ItemTextColorGet( eIST, bWindowFocused, bToday );
	if( clr != COLORREF(-1L) )
		return clr;
	switch( eIST )
	{
	case __EIST_DISABLED:
		return PmBridge_GetPM()->GetSysColor( COLOR_BTNFACE );
	break;
	case __EIST_HOVER:
	case __EIST_SELECTED_HOVER:
		return PmBridge_GetPM()->GetSysColor(COLOR_HIGHLIGHTTEXT);
	break;
	case __EIST_NORMAL:
		if( bToday )
		{
			if( PaintStyleGet() == __EPST_PAINT_MANAGER )
				return RGB(128,128,255);
			return RGB(255,255,128); // PmBridge_GetPM()->GetSysColor( COLOR_INFOTEXT );
		}
		if( ePIT == __EPIT_OUT_OF_RANGE_ITEM )
			return PmBridge_GetPM()->GetSysColor( COLOR_BTNFACE );
		return PmBridge_GetPM()->GetSysColor( COLOR_WINDOWTEXT );
	break;
	case __EIST_PRESSED:
	case __EIST_SELECTED_PRESSED:
	case __EIST_SELECTED_NORMAL:
		if( bWindowFocused )
			return PmBridge_GetPM()->GetSysColor(COLOR_HIGHLIGHTTEXT);
		return PmBridge_GetPM()->GetSysColor(COLOR_BTNTEXT);
	break;
	}
	ASSERT( FALSE );
	return RGB(0,0,0);
}

COLORREF CExtDateBrowserWnd::OnDateBrowserQueryPaletteItemBackgroundColor(
	CExtDateBrowserWnd::e_view_mode_t _eVM,
	CExtDateBrowserWnd::e_item_state_t eIST,
	CExtDateBrowserWnd::e_painted_item_type_t ePIT,
	bool bWindowFocused,
	bool bToday
	)
{
	ASSERT_VALID( this );
	_eVM;
	if( ePIT == __EPIT_WEEK_CAPTION_ITEM )
		return COLORREF(-1L);
COLORREF clr = ItemBackgroundColorGet( eIST, bWindowFocused, bToday );
	if( clr != COLORREF(-1L) )
		return clr;
	switch( eIST )
	{
	case __EIST_DISABLED:
		return COLORREF(-1L);
	break;
	case __EIST_HOVER:
	case __EIST_SELECTED_HOVER:
		return CExtBitmap::stat_HLS_Adjust( PmBridge_GetPM()->GetSysColor(COLOR_HIGHLIGHT), 0.0, 0.15, 0.0 );
	break;
	case __EIST_NORMAL:
		if( bToday )
			return RGB(128,0,0); // PmBridge_GetPM()->GetSysColor( COLOR_INFOBK );
		return COLORREF(-1L);
	break;
	case __EIST_PRESSED:
	case __EIST_SELECTED_PRESSED:
		if( bWindowFocused )
			return CExtBitmap::stat_HLS_Adjust( PmBridge_GetPM()->GetSysColor(COLOR_HIGHLIGHT), 0.0, -0.15, 0.0 );
		return PmBridge_GetPM()->GetSysColor(COLOR_BTNFACE);
	break;
	case __EIST_SELECTED_NORMAL:
		if( bWindowFocused )
			return PmBridge_GetPM()->GetSysColor(COLOR_HIGHLIGHT);
		return PmBridge_GetPM()->GetSysColor(COLOR_BTNFACE);
	break;
	}
	ASSERT( FALSE );
	return RGB(0,0,0);
}

void CExtDateBrowserWnd::SKINDATA::LoadVistaSkin()
{
	EmptySkin();
	m_rcPadding.SetRect( 3, 3, 3, 3 );
	VERIFY( m_bmpTodayOver.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBBK_TODAY_OVER) ) );
	VERIFY( m_bmpAnyHover.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBBK_ANY_HOVER) ) );
	VERIFY( m_bmpAnyPressed.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBBK_ANY_PRESSED) ) );
	VERIFY( m_bmpSelectedNormal.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBBK_SELECTED_NORMAL) ) );
	VERIFY( m_bmpSelectedHover.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBBK_SELECTED_HOVER) ) );
	VERIFY( m_bmpSelectedPressed.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBBK_SELECTED_PRESSED) ) );
	VERIFY( m_bmpSelectedUnfocusedWnd.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_DBBK_SELECTED_UNFOCUSED_WND) ) );
	m_clrTextColorNormal = m_clrTextColorSelected = m_clrTextColorPressed = m_clrTextColorDisabled =
		m_clrCaptionTextColorNormal = m_clrCaptionTextColorPressed = RGB(0,0,0);
	m_clrTextColorOutOfRange = RGB(192,192,192);
	m_clrTextColorHover = m_clrTextColorToday = m_clrCaptionTextColorHover = RGB(0,102,204);
	m_clrBackground = RGB(255,255,255);
	m_rcFocusRectPadding.SetRect( 2, 2, 2, 2 );
}

COLORREF CExtDateBrowserWnd::SKINDATA::GetCaptionTextColor(
	CExtDateBrowserWnd::e_item_state_t eIST,
	CExtDateBrowserWnd::e_painted_item_type_t ePIT,
	bool bWindowFocused
	)
{
	eIST;
	ePIT;
	bWindowFocused;
	switch( eIST )
	{
	case __EIST_DISABLED:
	case __EIST_NORMAL:
	case __EIST_SELECTED_NORMAL:
		return m_clrCaptionTextColorNormal;
	break;
	case __EIST_HOVER:
	case __EIST_SELECTED_HOVER:
		return m_clrCaptionTextColorHover;
	break;
	case __EIST_PRESSED:
	case __EIST_SELECTED_PRESSED:
		return m_clrCaptionTextColorPressed;
	break;
	}
	ASSERT( FALSE );
	return RGB(0,0,0);
}

COLORREF CExtDateBrowserWnd::SKINDATA::GetItemTextColor(
	CExtDateBrowserWnd::e_item_state_t eIST,
	CExtDateBrowserWnd::e_painted_item_type_t ePIT,
	bool bWindowFocused,
	bool bToday
	)
{
	bWindowFocused;
	if( bToday )
		return m_clrTextColorToday;
	if( ePIT == __EPIT_WEEK_CAPTION_ITEM )
		return m_clrTextColorNormal;
	switch( eIST )
	{
	case __EIST_NORMAL:
		if( ePIT == __EPIT_OUT_OF_RANGE_ITEM )
			return m_clrTextColorOutOfRange;
		return m_clrTextColorNormal;
	case __EIST_DISABLED:
		return m_clrTextColorDisabled;
	case __EIST_HOVER:
	case __EIST_SELECTED_HOVER:
		return m_clrTextColorHover;
	case __EIST_PRESSED:
	case __EIST_SELECTED_PRESSED:
		return m_clrTextColorPressed;
	case __EIST_SELECTED_NORMAL:
		return m_clrTextColorSelected;
	}
	return RGB(0,0,0);
}

void CExtDateBrowserWnd::SKINDATA::PaintItemBackground(
	CDC & dc,
	CRect rcItem,
	CExtDateBrowserWnd::e_item_state_t eIST,
	CExtDateBrowserWnd::e_painted_item_type_t ePIT,
	bool bWindowFocused,
	bool bToday
	)
{
	ASSERT( dc.GetSafeHdc() != NULL );
	if( ePIT != __EPIT_NORMAL_ITEM && ePIT != __EPIT_OUT_OF_RANGE_ITEM )
		return;
CExtBitmap * pBmp = NULL;
bool bDrawFocusRect = false;
	switch( eIST )
	{
	case __EIST_NORMAL:
		pBmp = bWindowFocused ? (&m_bmpAnyNormal) : (&m_bmpAnyUnfocusedWnd);
	break;
	case __EIST_DISABLED:
		pBmp = &m_bmpAnyDisabled;
	break;
	case __EIST_HOVER:
		pBmp = &m_bmpAnyHover;
	break;
	case __EIST_PRESSED:
		pBmp = &m_bmpAnyPressed;
	break;
	case __EIST_SELECTED_NORMAL:
		bDrawFocusRect = bWindowFocused;
		pBmp = bWindowFocused ? (&m_bmpSelectedNormal) : (&m_bmpSelectedUnfocusedWnd);
	break;
	case __EIST_SELECTED_HOVER:
		bDrawFocusRect = bWindowFocused;
		pBmp = &m_bmpSelectedHover;
	break;
	case __EIST_SELECTED_PRESSED:
		bDrawFocusRect = bWindowFocused;
		pBmp = &m_bmpSelectedPressed;
	break;
	}
	if( pBmp != NULL && (! pBmp->IsEmpty() ) )
		pBmp->AlphaBlendSkinParts(
			dc.m_hDC,
			rcItem,
			m_rcPadding,
			CExtBitmap::__EDM_STRETCH,
			true,
			true
			);
	if( bToday && ( ! m_bmpTodayOver.IsEmpty() ) )
		m_bmpTodayOver.AlphaBlendSkinParts(
			dc.m_hDC,
			rcItem,
			m_rcPadding,
			CExtBitmap::__EDM_STRETCH,
			true,
			true
			);
	if(		bDrawFocusRect
		&&	m_rcFocusRectPadding.left >= 0
		&&	m_rcFocusRectPadding.right >= 0
		&&	m_rcFocusRectPadding.top >= 0
		&&	m_rcFocusRectPadding.bottom >= 0
		)
	{
		CRect rcFR = rcItem;
		rcFR.DeflateRect( &m_rcFocusRectPadding );
		COLORREF clrTextOld = 
			dc.SetTextColor( RGB(255,255,255) );
		COLORREF clrBkOld =
			dc.SetBkColor( RGB(0,0,0) );
		dc.DrawFocusRect( &rcFR );
		dc.SetBkColor( clrBkOld );
		dc.SetTextColor( clrTextOld );
	}
}

bool CExtDateBrowserWnd::OnDateBrowserQueryWindowFocusedState() const
{
	if( GetSafeHwnd() == NULL )
		return false;
	ASSERT_VALID( this );
	if( ::GetFocus() != m_hWnd )
		return false;
	return true;
}

int CExtDateBrowserWnd::MonthSeparatorHeightGet() const
{
	ASSERT_VALID( this );
	return m_nMonthSeparatorHeight;
}

void CExtDateBrowserWnd::MonthSeparatorHeightSet( int nMonthSeparatorHeight )
{
	ASSERT_VALID( this );
	m_nMonthSeparatorHeight = nMonthSeparatorHeight;
}

int CExtDateBrowserWnd::OnDateBrowserQueryMonthSeparatorHeight() const
{
	ASSERT_VALID( this );
	return MonthSeparatorHeightGet();
}

void CExtDateBrowserWnd::OnDateBrowserPaintMonthSeparator( 
	CDC & dc,
	CRect rcItem
	)
{
	ASSERT_VALID( this );
	rcItem.DeflateRect( 8, 0 );
	dc.FillSolidRect(rcItem, OnDateBrowserQueryMonthSeparatorColor() );
}

COLORREF CExtDateBrowserWnd::MonthSeparatorColorGet() const
{
	 ASSERT_VALID( this );
	 return m_clrMonthSeparator;
}

void CExtDateBrowserWnd::MonthSeparatorColorSet( COLORREF clrMonthSeparator )
{
	 ASSERT_VALID( this );
	 m_clrMonthSeparator = clrMonthSeparator;
}

COLORREF CExtDateBrowserWnd::OnDateBrowserQueryMonthSeparatorColor() const
{
	ASSERT_VALID( this );
	return MonthSeparatorColorGet();
}

void CExtDateBrowserWnd::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	ASSERT_VALID( this );

	if( nChar == VK_TAB )
	{
		if( ( GetStyle() & WS_TABSTOP ) != 0 )
		{
			GetParent()->SendMessage(
				WM_NEXTDLGCTL,
				CExtPopupMenuWnd::IsKeyPressed(VK_SHIFT) ? 1 : 0,
				0
				);
		}
		return;
	}

e_view_mode_t eVM_old = ViewModeGet();
	if( nChar == VK_RETURN || nChar == VK_SPACE )
	{
		e_view_mode_t eVM_new;
		bool bRepainted;

		switch( eVM_old )
		{
		case __EVM_CENTURY:
			{
				eVM_new = __EVM_YEAR_DECADE;
				if( ! OnDateBrowserViewModeChanging( eVM_old, eVM_new ) )
					return;
				ViewModeSet( eVM_new );
				bRepainted = OnDateBrowserAnimateViewModeChanging( eVM_old, eVM_new );
				OnDateBrowserViewModeChanged( eVM_old, eVM_new );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_YEAR_DECADE:
			{
				eVM_new = __EVM_ONE_YEAR;
				if( ! OnDateBrowserViewModeChanging( eVM_old, eVM_new ) )
					return;
				ViewModeSet( eVM_new );
				bRepainted = OnDateBrowserAnimateViewModeChanging( eVM_old, eVM_new );
				OnDateBrowserViewModeChanged( eVM_old, eVM_new );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_ONE_YEAR:
			{
				eVM_new = __EVM_MONTH;
				if( ! OnDateBrowserViewModeChanging( eVM_old, eVM_new ) )
					return;
				ViewModeSet( eVM_new );
				bRepainted = OnDateBrowserAnimateViewModeChanging( eVM_old, eVM_new );
				OnDateBrowserViewModeChanged( eVM_old, eVM_new );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_MONTH:
			return;
		} // switch( eVM_old )
	} // if( nChar == VK_RETURN || nChar == VK_SPACE )

	if( CExtPopupMenuWnd::IsKeyPressed( VK_CONTROL ) )
	{
		if( nChar == VK_LEFT || nChar == VK_RIGHT )
		{
			bool bRepainted;

			switch( eVM_old )
			{
			case __EVM_CENTURY:
				{
					COleDateTime _timeOld = TimeGet();
					COleDateTime _timeNew;

					if( nChar == VK_LEFT )
						_timeNew.SetDateTime( _timeOld.GetYear() - 100, _timeOld.GetMonth(), 1, 0, 0, 0 );
					else
						_timeNew.SetDateTime( _timeOld.GetYear() + 100, _timeOld.GetMonth(), 1, 0, 0, 0 );

					if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
						return;
					VERIFY( TimeSet( _timeNew ) );
					bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
					OnDateBrowserTimeChanged( _timeOld, _timeNew );
					if( ! bRepainted )
					{
						Invalidate();
						UpdateWindow();
					}
				}
				return;
			break;
			case __EVM_YEAR_DECADE:
				{
					COleDateTime _timeOld = TimeGet();
					COleDateTime _timeNew;

					if( nChar == VK_LEFT )
						_timeNew.SetDateTime( _timeOld.GetYear() - 10, _timeOld.GetMonth(), 1, 0, 0, 0 );
					else
						_timeNew.SetDateTime( _timeOld.GetYear() + 10, _timeOld.GetMonth(), 1, 0, 0, 0 );

					if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
						return;
					VERIFY( TimeSet( _timeNew ) );
					bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
					OnDateBrowserTimeChanged( _timeOld, _timeNew );
					if( ! bRepainted )
					{
						Invalidate();
						UpdateWindow();
					}
				}
				return;
			break;
			case __EVM_ONE_YEAR:
				{
					COleDateTime _timeOld = TimeGet();
					COleDateTime _timeNew;

					if( nChar == VK_LEFT )
						_timeNew.SetDateTime( _timeOld.GetYear() - 1, _timeOld.GetMonth(), 1, 0, 0, 0 );
					else
						_timeNew.SetDateTime( _timeOld.GetYear() + 1, _timeOld.GetMonth(), 1, 0, 0, 0 );

					if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
						return;
					VERIFY( TimeSet( _timeNew ) );
					bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
					OnDateBrowserTimeChanged( _timeOld, _timeNew );
					if( ! bRepainted )
					{
						Invalidate();
						UpdateWindow();
					}
				}
				return;
			break;
			case __EVM_MONTH:
				{
					COleDateTime _timeOld = TimeGet();
					COleDateTime _timeNew;
					INT	nDay = _timeOld.GetDay();
					
					if( nChar == VK_RIGHT )
					{
						if( nDay > 28 )
						{
							if( _timeOld.GetMonth() == 12)
								_timeNew.SetDateTime(_timeOld.GetYear() + 1, 1, 28, 0, 0, 0 );
							else
								_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() + 1, 28, 0, 0, 0 );

							COleDateTime _t( _timeNew.GetYear(), _timeNew.GetMonth(), _timeNew.GetDay(), _timeNew.GetHour(), _timeNew.GetMinute(), _timeNew.GetSecond() );

							INT nMonth = _timeNew.GetMonth();

							int nDayCount = 27;

							for( ; _t.GetMonth() == nMonth; )
							{
								_t += COleDateTimeSpan( 1, 0, 0, 0 );
								nDayCount ++;
							}

							if( nDay < nDayCount )
							{
								if( _timeOld.GetMonth() == 12)
									_timeNew.SetDateTime(_timeOld.GetYear() + 1, 1, _timeOld.GetDay(), 0, 0, 0 );
								else
									_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() + 1, _timeOld.GetDay(), 0, 0, 0 );
							}
							else
							{
								if( _timeOld.GetMonth() == 12)
									_timeNew.SetDateTime(_timeOld.GetYear() + 1, 1, nDayCount, 0, 0, 0 );
								else
									_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() + 1, nDayCount, 0, 0, 0 );
							}
						}
						else
						{
							if( _timeOld.GetMonth() == 12)
 								_timeNew.SetDateTime(_timeOld.GetYear() + 1, 1, _timeOld.GetDay(), 0, 0, 0 );
							else
								_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() + 1, _timeOld.GetDay(), 0, 0, 0 );
						}
					}
					else
					{
						if( nDay > 28 )
						{
							if( _timeOld.GetMonth() == 1)
								_timeNew.SetDateTime(_timeOld.GetYear() - 1, 12, 28, 0, 0, 0 );
							else
								_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() - 1, 28, 0, 0, 0 );

							COleDateTime _t( _timeNew.GetYear(), _timeNew.GetMonth(), _timeNew.GetDay(), _timeNew.GetHour(), _timeNew.GetMinute(), _timeNew.GetSecond() );

							INT nMonth = _timeNew.GetMonth();

							int nDayCount = 27;

							for( ; _t.GetMonth() == nMonth; )
							{
								_t += COleDateTimeSpan( 1, 0, 0, 0 );
								nDayCount ++;
							}

							if( nDay < nDayCount )
							{
								if( _timeOld.GetMonth() == 1)
									_timeNew.SetDateTime(_timeOld.GetYear() - 1, 12, _timeOld.GetDay(), 0, 0, 0 );
								else
									_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() - 1, _timeOld.GetDay(), 0, 0, 0 );
							}
							else
							{
								if( _timeOld.GetMonth() == 1)
									_timeNew.SetDateTime(_timeOld.GetYear() - 1, 12, nDayCount, 0, 0, 0 );
								else
									_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() - 1, nDayCount, 0, 0, 0 );
							}
						}
						else
						{
							if( _timeOld.GetMonth() == 1 )
 								_timeNew.SetDateTime(_timeOld.GetYear() - 1, 12, _timeOld.GetDay(), 0, 0, 0 );
							else
								_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() - 1, _timeOld.GetDay(), 0, 0, 0 );
						}
					}

					if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
						return;
					VERIFY( TimeSet( _timeNew ) );
					bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
					OnDateBrowserTimeChanged( _timeOld, _timeNew );
					if( ! bRepainted )
					{
						Invalidate();
						UpdateWindow();
					}
				}
				return;
			} // switch( eVM_old )
		} // if( nChar == VK_LEFT || nChar == VK_RIGHT )
		
		if( nChar == VK_UP || nChar == VK_DOWN )
		{
			e_view_mode_t eVM_new;
			bool bRepainted = false;

			switch( eVM_old )
			{
			case __EVM_CENTURY:
				{
					if( nChar == VK_DOWN )
					{
						eVM_new = __EVM_YEAR_DECADE;

						if( ! OnDateBrowserViewModeChanging( eVM_old, eVM_new ) )
							return;
						ViewModeSet( eVM_new );
						bRepainted = OnDateBrowserAnimateViewModeChanging( eVM_old, eVM_new );
						OnDateBrowserViewModeChanged( eVM_old, eVM_new );
					}
					if( ! bRepainted )
					{
						Invalidate();
						UpdateWindow();
					}
				}
				return;
			break;
			case __EVM_YEAR_DECADE:
				{
					if( nChar == VK_UP )
						eVM_new = __EVM_CENTURY;
					else
						eVM_new = __EVM_ONE_YEAR;

					if( ! OnDateBrowserViewModeChanging( eVM_old, eVM_new ) )
						return;
					ViewModeSet( eVM_new );
					bRepainted = OnDateBrowserAnimateViewModeChanging( eVM_old, eVM_new );
					OnDateBrowserViewModeChanged( eVM_old, eVM_new );
					if( ! bRepainted )
					{
						Invalidate();
						UpdateWindow();
					}
				}
				return;
			break;
			case __EVM_ONE_YEAR:
				{
					if( nChar == VK_UP )
						eVM_new = __EVM_YEAR_DECADE;
					else
						eVM_new = __EVM_MONTH;

					if( ! OnDateBrowserViewModeChanging( eVM_old, eVM_new ) )
						return;
					ViewModeSet( eVM_new );
					bRepainted = OnDateBrowserAnimateViewModeChanging( eVM_old, eVM_new );
					OnDateBrowserViewModeChanged( eVM_old, eVM_new );
					if( ! bRepainted )
					{
						Invalidate();
						UpdateWindow();
					}
				}
				return;
			break;
			case __EVM_MONTH:
				{
					if( nChar == VK_UP )
					{
						eVM_new = __EVM_ONE_YEAR;

						if( ! OnDateBrowserViewModeChanging( eVM_old, eVM_new ) )
							return;
						ViewModeSet( eVM_new );
						bRepainted = OnDateBrowserAnimateViewModeChanging( eVM_old, eVM_new );
						OnDateBrowserViewModeChanged( eVM_old, eVM_new );
					}
					if( ! bRepainted )
					{
						Invalidate();
						UpdateWindow();
					}
				}
				return;
			} // switch( eVM_old )
		} // if( nChar == VK_UP || nChar == VK_DOWN )
	} // if( CExtPopupMenuWnd::IsKeyPressed( VK_CONTROL ) )

	if( nChar == VK_LEFT || nChar == VK_RIGHT )
	{
		bool bRepainted;
		switch( eVM_old )
		{
		case __EVM_CENTURY:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;

				if( nChar == VK_LEFT )
					_timeNew.SetDateTime( _timeOld.GetYear() - 10, _timeOld.GetMonth(), _timeOld.GetDay(), 0, 0, 0 );
				else
					_timeNew.SetDateTime( _timeOld.GetYear() + 10, _timeOld.GetMonth(), _timeOld.GetDay(), 0, 0, 0 );

				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_YEAR_DECADE:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;

				if( nChar == VK_LEFT )
					_timeNew.SetDateTime( _timeOld.GetYear() - 1, _timeOld.GetMonth(), _timeOld.GetDay(), 0, 0, 0 );
				else
					_timeNew.SetDateTime( _timeOld.GetYear() + 1, _timeOld.GetMonth(), _timeOld.GetDay(), 0, 0, 0 );

				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_ONE_YEAR:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;

				if( nChar == VK_LEFT )
				{
					if( _timeOld.GetMonth() == 1 )
						_timeNew.SetDateTime(_timeOld.GetYear() - 1, 12, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
					else
						_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() - 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				}
				else
				{
					if( _timeOld.GetMonth() == 12 )
						_timeNew.SetDateTime(_timeOld.GetYear() + 1, 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
					else
						_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() + 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				}
				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_MONTH:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _t( _timeOld.GetYear(), _timeOld.GetMonth(), _timeOld.GetDay(), _timeOld.GetHour(), _timeOld.GetMinute(), _timeOld.GetSecond() );
				if( nChar == VK_LEFT )
					_t -= COleDateTimeSpan( 1, 0, 0, 0 );
				else
					_t += COleDateTimeSpan( 1, 0, 0, 0 );
				COleDateTime _timeNew( _t.GetYear(), _t.GetMonth(), _t.GetDay(), _t.GetHour(), _t.GetMinute(), _t.GetSecond() );
				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		} // switch( eVM_old )
	} // if( nChar == VK_LEFT || nChar == VK_RIGHT )

	if( nChar == VK_UP || nChar == VK_DOWN )
	{
		bool bRepainted;
		switch( eVM_old )
		{
		case __EVM_CENTURY:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;

				if( nChar == VK_UP )
					_timeNew.SetDateTime( _timeOld.GetYear() - __EXT_DATE_BROWSER_CENTURY_COLUMN_COUNT * 10, _timeOld.GetMonth(), _timeOld.GetDay(), 0, 0, 0 );
				else
					_timeNew.SetDateTime( _timeOld.GetYear() + __EXT_DATE_BROWSER_CENTURY_COLUMN_COUNT * 10, _timeOld.GetMonth(), _timeOld.GetDay(), 0, 0, 0 );

				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_YEAR_DECADE:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;

				if( nChar == VK_UP )
					_timeNew.SetDateTime( _timeOld.GetYear() - __EXT_DATE_BROWSER_YEAR_DECADE_COLUMN_COUNT, _timeOld.GetMonth(), _timeOld.GetDay(), 0, 0, 0 );
				else
					_timeNew.SetDateTime( _timeOld.GetYear() + __EXT_DATE_BROWSER_YEAR_DECADE_COLUMN_COUNT, _timeOld.GetMonth(), _timeOld.GetDay(), 0, 0, 0 );

				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_ONE_YEAR:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;
				INT nMonth = _timeOld.GetMonth() - 1, nYear = _timeOld.GetYear();
				if( nChar == VK_UP )
				{
					nMonth -= __EXT_DATE_BROWSER_ONE_YEAR_COLUMN_COUNT;
					if( nMonth < 0 )
					{
						nMonth += 12;
						nYear --;
					}
				}
				else
				{
					nMonth += __EXT_DATE_BROWSER_ONE_YEAR_COLUMN_COUNT;
					if( nMonth >= 12 )
					{
						nMonth -= 12;
						nYear ++;
					}
				}
				_timeNew.SetDateTime( nYear, nMonth + 1, 1, 0, 0, 0 );
				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_MONTH:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _t( _timeOld.GetYear(), _timeOld.GetMonth(), _timeOld.GetDay(), _timeOld.GetHour(), _timeOld.GetMinute(), _timeOld.GetSecond() );
				if( nChar == VK_UP )
					_t -= COleDateTimeSpan( __EXT_DATE_BROWSER_MONTH_COLUMN_COUNT, 0, 0, 0 );
				else
					_t += COleDateTimeSpan( __EXT_DATE_BROWSER_MONTH_COLUMN_COUNT, 0, 0, 0 );
				COleDateTime _timeNew( _t.GetYear(), _t.GetMonth(), _t.GetDay(), _t.GetHour(), _t.GetMinute(), _t.GetSecond() );
				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		} // switch( eVM_old )
	} // if( nChar == VK_UP || nChar == VK_DOWN )

	if( nChar == VK_HOME || nChar == VK_END )
	{
		bool bRepainted;

		switch( eVM_old )
		{
		case __EVM_CENTURY:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;
				LONG nFirstYear, nLastYear;
				stat_CalcCenturyDisplayYearRange( _timeOld.GetYear(), nFirstYear, nLastYear );

				if( nChar == VK_HOME )
					_timeNew.SetDateTime( nFirstYear, _timeOld.GetMonth(), 1, 0, 0, 0 );
				else
					_timeNew.SetDateTime( nLastYear, _timeOld.GetMonth(), 1, 0, 0, 0 );

				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_YEAR_DECADE:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;
				LONG nFirstYear, nLastYear;
				stat_CalcYearDecadeDisplayYearRange( _timeOld.GetYear(), nFirstYear, nLastYear );

				if( nChar == VK_HOME )
					_timeNew.SetDateTime( nFirstYear, _timeOld.GetMonth(), 1, 0, 0, 0 );
				else
					_timeNew.SetDateTime( nLastYear, _timeOld.GetMonth(), 1, 0, 0, 0 );

				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_ONE_YEAR:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;

				if( nChar == VK_HOME )
					_timeNew.SetDateTime(_timeOld.GetYear(), 1, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				else
					_timeNew.SetDateTime(_timeOld.GetYear(), 12, /*_timeOld.GetDay()*/ 1, 0, 0, 0 );

				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_MONTH:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;
				
				COleDateTime _t( _timeOld.GetYear(), _timeOld.GetMonth(), _timeOld.GetDay(), _timeOld.GetHour(), _timeOld.GetMinute(), _timeOld.GetSecond() );

				INT nMonth = _timeOld.GetMonth();

				int nDayCount = _timeOld.GetDay() - 1;

				for( ; _t.GetMonth() == nMonth; )
				{
					_t += COleDateTimeSpan( 1, 0, 0, 0 );
					nDayCount ++;
				}

				if( nChar == VK_HOME )
					_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth(), /*_timeOld.GetDay()*/ 1, 0, 0, 0 );
				else
					_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth(), /*_timeOld.GetDay()*/ nDayCount, 0, 0, 0 );

				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		} // switch( eVM_old )
	} // if( nChar == VK_HOME || nChar == VK_END )

	if( nChar == VK_PRIOR || nChar == VK_NEXT )
	{
		bool bRepainted;

		switch( eVM_old )
		{
		case __EVM_CENTURY:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;

				if( nChar == VK_PRIOR )
					_timeNew.SetDateTime( _timeOld.GetYear() - 100, _timeOld.GetMonth(), 1, 0, 0, 0 );
				else
					_timeNew.SetDateTime( _timeOld.GetYear() + 100, _timeOld.GetMonth(), 1, 0, 0, 0 );

				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_YEAR_DECADE:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;

				if( nChar == VK_PRIOR )
					_timeNew.SetDateTime( _timeOld.GetYear() - 10, _timeOld.GetMonth(), 1, 0, 0, 0 );
				else
					_timeNew.SetDateTime( _timeOld.GetYear() + 10, _timeOld.GetMonth(), 1, 0, 0, 0 );

				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_ONE_YEAR:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;

				if( nChar == VK_PRIOR )
					_timeNew.SetDateTime( _timeOld.GetYear() - 1, _timeOld.GetMonth(), 1, 0, 0, 0 );
				else
					_timeNew.SetDateTime( _timeOld.GetYear() + 1, _timeOld.GetMonth(), 1, 0, 0, 0 );

				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		break;
		case __EVM_MONTH:
			{
				COleDateTime _timeOld = TimeGet();
				COleDateTime _timeNew;
				INT	nDay = _timeOld.GetDay();
				
				if( nChar == VK_NEXT )
				{
					if( nDay > 28 )
					{
						if( _timeOld.GetMonth() == 12)
							_timeNew.SetDateTime(_timeOld.GetYear() + 1, 1, 28, 0, 0, 0 );
						else
							_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() + 1, 28, 0, 0, 0 );

						COleDateTime _t( _timeNew.GetYear(), _timeNew.GetMonth(), _timeNew.GetDay(), _timeNew.GetHour(), _timeNew.GetMinute(), _timeNew.GetSecond() );

						INT nMonth = _timeNew.GetMonth();

						int nDayCount = 27;

						for( ; _t.GetMonth() == nMonth; )
						{
							_t += COleDateTimeSpan( 1, 0, 0, 0 );
							nDayCount ++;
						}

						if( nDay < nDayCount )
						{
							if( _timeOld.GetMonth() == 12)
								_timeNew.SetDateTime(_timeOld.GetYear() + 1, 1, _timeOld.GetDay(), 0, 0, 0 );
							else
								_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() + 1, _timeOld.GetDay(), 0, 0, 0 );
						}
						else
						{
							if( _timeOld.GetMonth() == 12)
								_timeNew.SetDateTime(_timeOld.GetYear() + 1, 1, nDayCount, 0, 0, 0 );
							else
								_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() + 1, nDayCount, 0, 0, 0 );
						}
					}
					else
					{
						if( _timeOld.GetMonth() == 12)
 							_timeNew.SetDateTime(_timeOld.GetYear() + 1, 1, _timeOld.GetDay(), 0, 0, 0 );
						else
							_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() + 1, _timeOld.GetDay(), 0, 0, 0 );
					}
				}
				else
				{
					if( nDay > 28 )
					{
						if( _timeOld.GetMonth() == 1)
							_timeNew.SetDateTime(_timeOld.GetYear() - 1, 12, 28, 0, 0, 0 );
						else
							_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() - 1, 28, 0, 0, 0 );

						COleDateTime _t( _timeNew.GetYear(), _timeNew.GetMonth(), _timeNew.GetDay(), _timeNew.GetHour(), _timeNew.GetMinute(), _timeNew.GetSecond() );

						INT nMonth = _timeNew.GetMonth();

						int nDayCount = 27;

						for( ; _t.GetMonth() == nMonth; )
						{
							_t += COleDateTimeSpan( 1, 0, 0, 0 );
							nDayCount ++;
						}

						if( nDay < nDayCount )
						{
							if( _timeOld.GetMonth() == 1)
								_timeNew.SetDateTime(_timeOld.GetYear() - 1, 12, _timeOld.GetDay(), 0, 0, 0 );
							else
								_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() - 1, _timeOld.GetDay(), 0, 0, 0 );
						}
						else
						{
							if( _timeOld.GetMonth() == 1)
								_timeNew.SetDateTime(_timeOld.GetYear() - 1, 12, nDayCount, 0, 0, 0 );
							else
								_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() - 1, nDayCount, 0, 0, 0 );
						}
					}
					else
					{
						if( _timeOld.GetMonth() == 1 )
 							_timeNew.SetDateTime(_timeOld.GetYear() - 1, 12, _timeOld.GetDay(), 0, 0, 0 );
						else
							_timeNew.SetDateTime(_timeOld.GetYear(), _timeOld.GetMonth() - 1, _timeOld.GetDay(), 0, 0, 0 );
					}
				}

				if( ! OnDateBrowserTimeChanging( _timeOld, _timeNew ) )
					return;
				VERIFY( TimeSet( _timeNew ) );
				bRepainted = OnDateBrowserAnimateTimeChanging( _timeOld, _timeNew );
				OnDateBrowserTimeChanged( _timeOld, _timeNew );
				if( ! bRepainted )
				{
					Invalidate();
					UpdateWindow();
				}
			}
			return;
		} // switch( eVM_old )
	} // if( nChar == VK_PRIOR || nChar == VK_NEXT )

	CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}

UINT CExtDateBrowserWnd::OnGetDlgCode() 
{
	ASSERT_VALID( this );
	return DLGC_WANTALLKEYS;
}

bool CExtDateBrowserWnd::OnDateBrowserQueryCenturyItemVisibility( const COleDateTime & _time ) const
{
	ASSERT_VALID( this );
	if( m_bShowOutOfRangeItems )
		return true;
	if( TimeGet(-1) <= _time && _time <= TimeGet(1) )
		return true;
	return false;
}

bool CExtDateBrowserWnd::OnDateBrowserQueryYearDecadeItemVisibility( const COleDateTime & _time ) const
{
	ASSERT_VALID( this );
	if( m_bShowOutOfRangeItems )
		return true;
	if( TimeGet(-1) <= _time && _time <= TimeGet(1) )
		return true;
	return false;
}

bool CExtDateBrowserWnd::OnDateBrowserQueryOneYearItemVisibility( const COleDateTime & _time ) const
{
	ASSERT_VALID( this );
	if( m_bShowOutOfRangeItems )
		return true;
	if( TimeGet(-1) <= _time && _time <= TimeGet(1) )
		return true;
	return false;
}

bool CExtDateBrowserWnd::OnDateBrowserQueryMonthItemVisibility( const COleDateTime & _time ) const
{
	ASSERT_VALID( this );
	if( m_bShowOutOfRangeItems )
		return true;
	if( TimeGet(-1) <= _time && _time <= TimeGet(1) )
		return true;
	return false;
}

bool CExtDateBrowserWnd::OnDateBrowserAnimateTimeChanging( const COleDateTime & _timeOld, const COleDateTime  & _timeNew ) const
{
	ASSERT_VALID( this );
	if( m_nAnimationStepCountScroll <= 0 || m_nAnimationStepTimeScroll <= 0 )
		return false;
e_view_mode_t eVM = ViewModeGet();
	switch( eVM )
	{
	case __EVM_CENTURY:
	{
		LONG nFirstYearOld, nFirstYearNew, nTmp;
		stat_CalcCenturyDisplayYearRange( _timeOld.GetYear(), nFirstYearOld, nTmp );
		stat_CalcCenturyDisplayYearRange( _timeNew.GetYear(), nFirstYearNew, nTmp );
		if( nFirstYearOld == nFirstYearNew )
			return false;
	}
	break;
	case __EVM_YEAR_DECADE:
	{
		LONG nFirstYearOld, nFirstYearNew, nTmp;
		stat_CalcYearDecadeDisplayYearRange( _timeOld.GetYear(), nFirstYearOld, nTmp );
		stat_CalcYearDecadeDisplayYearRange( _timeNew.GetYear(), nFirstYearNew, nTmp );
		if( nFirstYearOld == nFirstYearNew )
			return false;
	}
	break;
	case __EVM_ONE_YEAR:
	{
		if( _timeOld.GetYear() == _timeNew.GetYear() )
			return false;
	}
	break;
	case __EVM_MONTH:
	{
		if(		_timeOld.GetYear() == _timeNew.GetYear()
			&&	_timeOld.GetMonth() == _timeNew.GetMonth()
			)
			return false;
	}
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( eVM )
bool bWindowFocused = OnDateBrowserQueryWindowFocusedState();
CRect rcEntire;
	GetClientRect( &rcEntire );
CExtDateBrowserWnd * pThis = const_cast < CExtDateBrowserWnd * > ( this );
CClientDC dcClient( pThis );
	dcClient.SelectClipRgn( NULL );
CExtMemoryDC dc( &dcClient, &rcEntire, CExtMemoryDC::MDCOPT_TO_MEMORY|CExtMemoryDC::MDCOPT_FORCE_DIB );
CExtBitmap bmpStart, bmpEnd;
bool bRetVal = false;
COLORREF clrBackground = OnDateBrowserGetBackgroundColor();
	dc.FillSolidRect( &rcEntire, clrBackground );
//pThis->TimeSet( _timeOld );
	pThis->OnDateBrowserPaintEntire( dc, _timeOld, eVM, rcEntire, bWindowFocused );
	if( bmpStart.FromSurface( dc.m_hDC, rcEntire ) )
	{
		dc.FillSolidRect( &rcEntire, clrBackground );
//pThis->TimeSet( _timeNew );
		pThis->OnDateBrowserPaintEntire( dc, _timeNew, eVM, rcEntire, bWindowFocused );
		if( bmpEnd.FromSurface( dc.m_hDC, rcEntire ) )
		{
			CSize sizeCaption = OnDateBrowserCalcCaptionSize( dc, eVM, _timeNew );
			CRect rcScroll = rcEntire, rcCaption = rcEntire;
			rcScroll.top += sizeCaption.cy;
			rcCaption.bottom = rcCaption.top + sizeCaption.cy;
			bool bNextGreater = ( _timeNew > _timeOld ) ? true : false;
			INT nAnimationStepIndex;
			for( nAnimationStepIndex = 0; nAnimationStepIndex < m_nAnimationStepCountScroll; nAnimationStepIndex ++ )
			{
				dc.FillSolidRect( &rcEntire, clrBackground );
				BYTE nAlpha = BYTE( ::MulDiv( 255, nAnimationStepIndex, m_nAnimationStepCountScroll ) );
				bmpStart.AlphaBlend( dc.m_hDC, rcCaption, rcCaption, BYTE(255-nAlpha) );
				bmpEnd.  AlphaBlend( dc.m_hDC, rcCaption, rcCaption, nAlpha );
				INT nOffset = ::MulDiv( rcScroll.Width(), nAnimationStepIndex + 1, m_nAnimationStepCountScroll );
				CRect rcScrollStart = rcScroll, rcScrollEnd = rcScroll;
				if( bNextGreater )
				{
					rcScrollEnd.OffsetRect( rcScrollEnd.Width(), 0 );
					rcScrollStart.OffsetRect( -nOffset, 0 );
					rcScrollEnd.OffsetRect( -nOffset, 0 );
				}
				else
				{
					rcScrollStart.OffsetRect( -rcScrollEnd.Width(), 0 );
					rcScrollStart.OffsetRect( nOffset, 0 );
					rcScrollEnd.OffsetRect( nOffset, 0 );
				}
				bmpStart.Draw( dc.m_hDC, rcScrollStart, rcScroll );
				bmpEnd.  Draw( dc.m_hDC, rcScrollEnd, rcScroll );
				dcClient.BitBlt(
					rcEntire.left, rcEntire.top, rcEntire.Width(), rcEntire.Height(),
					&dc,
					rcEntire.left, rcEntire.top,
					SRCCOPY
					);			
				::Sleep( DWORD(m_nAnimationStepTimeScroll) );
			}
			bmpEnd.Draw( dc.m_hDC, rcEntire );
			dc.__Flush();
			bRetVal = true;
		}
	}
	if( ! bRetVal )
		dc.__Flush( FALSE );
	return bRetVal;
}

CRect CExtDateBrowserWnd::GetSelectedItemRect(
	CDC & dcMeasure,
	CExtDateBrowserWnd::e_view_mode_t _eVM,
	const COleDateTime & _time,
	CRect rcEntire
	) const
{
	ASSERT_VALID( this );
CRect rcCaption( 0, 0, 0, 0 ), rcButtonPrevious( 0, 0, 0, 0 ), rcButtonNext( 0, 0, 0, 0 ), rcMonthSeparator( 0, 0, 0, 0 );
CList < CRect, CRect & > listItems;
CList < COleDateTime, COleDateTime & > listTimes;
INT nInvisibleCountAtStart = 0, nInvisibleCountAtEnd = 0;
	switch( _eVM )
	{
	case __EVM_CENTURY:
		( const_cast < CExtDateBrowserWnd * > ( this ) ) ->
			OnDateBrowserCalcLayoutCentury(
				dcMeasure,
				_time,
				rcEntire,
				rcCaption,
				rcButtonPrevious,
				rcButtonNext,
				listItems,
				listTimes,
				nInvisibleCountAtStart,
				nInvisibleCountAtEnd
				);
	break;
	case __EVM_YEAR_DECADE:
		( const_cast < CExtDateBrowserWnd * > ( this ) ) ->
			OnDateBrowserCalcLayoutYearDecade(
				dcMeasure,
				_time,
				rcEntire,
				rcCaption,
				rcButtonPrevious,
				rcButtonNext,
				listItems,
				listTimes,
				nInvisibleCountAtStart,
				nInvisibleCountAtEnd
				);
	break;
	case __EVM_ONE_YEAR:
		( const_cast < CExtDateBrowserWnd * > ( this ) ) ->
			OnDateBrowserCalcLayoutOneYear(
				dcMeasure,
				_time,
				rcEntire,
				rcCaption,
				rcButtonPrevious,
				rcButtonNext,
				listItems,
				listTimes,
				nInvisibleCountAtStart,
				nInvisibleCountAtEnd
				);
	break;
	case __EVM_MONTH:
		( const_cast < CExtDateBrowserWnd * > ( this ) ) ->
			OnDateBrowserCalcLayoutMonth(
				dcMeasure,
				_time,
				rcEntire,
				rcCaption,
				rcMonthSeparator,
				rcButtonPrevious,
				rcButtonNext,
				listItems,
				listTimes,
				nInvisibleCountAtStart,
				nInvisibleCountAtEnd
				);
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( _eVM )
	ASSERT( listItems.GetCount() == listTimes.GetCount() );

POSITION posItems = listItems.GetHeadPosition();
POSITION posTimes = listTimes.GetHeadPosition();
	ASSERT(
			( posItems != NULL && posTimes != NULL )
		||	( posItems == NULL && posTimes == NULL )
		);
INT nItemIndex = 0;
	for( ; posItems != NULL; nItemIndex++ )
	{
		CRect & rcItem = listItems.GetNext( posItems );
		COleDateTime & _timeItem = listTimes.GetNext( posTimes );
		if( OnDateBrowserQueryPaletteItemIndexSelectedState( _timeItem, _time, _eVM ) )
			return rcItem;
	}
CRect rc( rcEntire.CenterPoint(), CSize(1,1) );
	return rc;
}

bool CExtDateBrowserWnd::OnDateBrowserAnimateViewModeChanging(
	CExtDateBrowserWnd::e_view_mode_t eVM_old,
	CExtDateBrowserWnd::e_view_mode_t eVM_new
	) const
{
	ASSERT_VALID( this );
	if( m_nAnimationStepCountZoom <= 0 || m_nAnimationStepTimeZoom <= 0 )
		return false;
bool bWindowFocused = OnDateBrowserQueryWindowFocusedState();
CRect rcEntire;
	GetClientRect( &rcEntire );
CExtDateBrowserWnd * pThis = const_cast < CExtDateBrowserWnd * > ( this );
CClientDC dcClient( pThis );
	dcClient.SelectClipRgn( NULL );
CExtMemoryDC dc( &dcClient, &rcEntire, CExtMemoryDC::MDCOPT_TO_MEMORY|CExtMemoryDC::MDCOPT_FORCE_DIB );
CExtBitmap bmpStart, bmpEnd;
bool bRetVal = false;
COLORREF clrBackground = OnDateBrowserGetBackgroundColor();
COleDateTime _time = TimeGet();
	dc.FillSolidRect( &rcEntire, clrBackground );
	pThis->OnDateBrowserPaintEntire( dc, _time, eVM_old, rcEntire, bWindowFocused );
	if( bmpStart.FromSurface( dc.m_hDC, rcEntire ) )
	{
		dc.FillSolidRect( &rcEntire, clrBackground );
		pThis->OnDateBrowserPaintEntire( dc, _time, eVM_new, rcEntire, bWindowFocused );
		if( bmpEnd.FromSurface( dc.m_hDC, rcEntire ) )
		{
			CRect rcSelStart = GetSelectedItemRect( dc, eVM_old, _time, rcEntire );
			CRect rcSelEnd = GetSelectedItemRect( dc, eVM_new, _time, rcEntire );

			CSize sizeCaption = OnDateBrowserCalcCaptionSize( dc, eVM_new, _time );
			CRect rcMagnify = rcEntire, rcCaption = rcEntire;
			rcMagnify.top += sizeCaption.cy;
			rcCaption.bottom = rcCaption.top + sizeCaption.cy;
			INT nAnimationStepIndex;

			CRect rcWalk1(0,0,0,0), rcWalk2(0,0,0,0), rcDstStart(0,0,0,0),
				rcImagination1(0,0,0,0), rcImagination2(0,0,0,0),
				rcDeflate1(0,0,0,0), rcDeflate2(0,0,0,0), rcDeflate3(0,0,0,0), rcDeflate4(0,0,0,0);
			if( INT(eVM_old) > INT(eVM_new) )
			{
				rcWalk1 = rcMagnify;
				rcDstStart = rcSelEnd;
				rcDeflate1.SetRect(
					rcDstStart.left - rcMagnify.left,
					rcDstStart.top - rcMagnify.top,
					rcMagnify.right - rcDstStart.right,
					rcMagnify.bottom - rcDstStart.bottom
					);
				rcImagination1 = rcMagnify;
				rcImagination1.InflateRect(
					::MulDiv( rcDeflate1.left,   rcMagnify.Width(),  rcSelEnd.Width()  ),
					::MulDiv( rcDeflate1.top,    rcMagnify.Height(), rcSelEnd.Height() ),
					::MulDiv( rcDeflate1.right,  rcMagnify.Width(),  rcSelEnd.Width()  ),
					::MulDiv( rcDeflate1.bottom, rcMagnify.Height(), rcSelEnd.Height() )
					);
				rcWalk2 = rcImagination1;
				rcDeflate2.SetRect(
					rcImagination1.left - rcMagnify.left,
					rcImagination1.top - rcMagnify.top,
					rcMagnify.right - rcImagination1.right,
					rcMagnify.bottom - rcImagination1.bottom
					);
			}
			else
			{
				rcDeflate3.SetRect(
					rcSelStart.left - rcMagnify.left,
					rcSelStart.top - rcMagnify.top,
					rcMagnify.right - rcSelStart.right,
					rcMagnify.bottom - rcSelStart.bottom
					);
				rcImagination2 = rcMagnify;
				rcImagination2.InflateRect(
					::MulDiv( rcDeflate3.left,   rcMagnify.Width(),  rcSelStart.Width()  ),
					::MulDiv( rcDeflate3.top,    rcMagnify.Height(), rcSelStart.Height() ),
					::MulDiv( rcDeflate3.right,  rcMagnify.Width(),  rcSelStart.Width()  ),
					::MulDiv( rcDeflate3.bottom, rcMagnify.Height(), rcSelStart.Height() )
					);
				rcDeflate4.SetRect(
					rcImagination2.left - rcMagnify.left,
					rcImagination2.top - rcMagnify.top,
					rcMagnify.right - rcImagination2.right,
					rcMagnify.bottom - rcImagination2.bottom
					);
			}

			int nOldStretchBltMode = ::GetStretchBltMode( dc.m_hDC );
			::SetStretchBltMode( dc.m_hDC, g_PaintManager.m_bIsWinNT ? HALFTONE : COLORONCOLOR );
			for( nAnimationStepIndex = 0; nAnimationStepIndex < m_nAnimationStepCountZoom; nAnimationStepIndex ++ )
			{
				dc.FillSolidRect( &rcEntire, clrBackground );
				BYTE nAlpha = BYTE( ::MulDiv( 255, nAnimationStepIndex, m_nAnimationStepCountZoom ) );

				if( INT(eVM_old) > INT(eVM_new) )
				{
					rcWalk1 = rcMagnify;
					rcWalk1.left   += ::MulDiv( rcDeflate1.left,  nAnimationStepIndex, m_nAnimationStepCountZoom );
					rcWalk1.top    += ::MulDiv( rcDeflate1.top,   nAnimationStepIndex, m_nAnimationStepCountZoom );
					rcWalk1.right  -= ::MulDiv( rcDeflate1.right, nAnimationStepIndex, m_nAnimationStepCountZoom );
					rcWalk1.bottom -= ::MulDiv( rcDeflate1.bottom,nAnimationStepIndex, m_nAnimationStepCountZoom );
 					bmpStart.AlphaBlend( dc.m_hDC, rcWalk1, rcMagnify, BYTE(255-nAlpha) );

					rcWalk2 = rcImagination1;
					rcWalk2.left   -= ::MulDiv( rcDeflate2.left,  nAnimationStepIndex, m_nAnimationStepCountZoom );
					rcWalk2.top    -= ::MulDiv( rcDeflate2.top,   nAnimationStepIndex, m_nAnimationStepCountZoom );
					rcWalk2.right  += ::MulDiv( rcDeflate2.right, nAnimationStepIndex, m_nAnimationStepCountZoom );
					rcWalk2.bottom += ::MulDiv( rcDeflate2.bottom,nAnimationStepIndex, m_nAnimationStepCountZoom );
					bmpEnd.AlphaBlend( dc.m_hDC, rcWalk2, rcMagnify, nAlpha );
				}
				else
				{
					rcWalk1 = rcSelStart;
					rcWalk1.left   -= ::MulDiv( rcDeflate3.left,  nAnimationStepIndex, m_nAnimationStepCountZoom );
					rcWalk1.top    -= ::MulDiv( rcDeflate3.top,   nAnimationStepIndex, m_nAnimationStepCountZoom );
					rcWalk1.right  += ::MulDiv( rcDeflate3.right, nAnimationStepIndex, m_nAnimationStepCountZoom );
					rcWalk1.bottom += ::MulDiv( rcDeflate3.bottom,nAnimationStepIndex, m_nAnimationStepCountZoom );
					bmpEnd.AlphaBlend( dc.m_hDC, rcWalk1, rcMagnify, nAlpha );

					rcWalk2 = rcMagnify;
					rcWalk2.left   += ::MulDiv( rcDeflate4.left,  nAnimationStepIndex, m_nAnimationStepCountZoom );
					rcWalk2.top    += ::MulDiv( rcDeflate4.top,   nAnimationStepIndex, m_nAnimationStepCountZoom );
					rcWalk2.right  -= ::MulDiv( rcDeflate4.right, nAnimationStepIndex, m_nAnimationStepCountZoom );
					rcWalk2.bottom -= ::MulDiv( rcDeflate4.bottom,nAnimationStepIndex, m_nAnimationStepCountZoom );
					bmpStart.AlphaBlend( dc.m_hDC, rcWalk2, rcMagnify, BYTE(255-nAlpha) );
				}

				dc.FillSolidRect( &rcCaption, clrBackground );
				bmpStart.AlphaBlend( dc.m_hDC, rcCaption, rcCaption, BYTE(255-nAlpha) );
				bmpEnd.  AlphaBlend( dc.m_hDC, rcCaption, rcCaption, nAlpha );
				dcClient.BitBlt(
					rcEntire.left, rcEntire.top, rcEntire.Width(), rcEntire.Height(),
					&dc,
					rcEntire.left, rcEntire.top,
					SRCCOPY
					);			
				::Sleep( DWORD(m_nAnimationStepTimeZoom) );
			}
			bmpEnd.Draw( dc.m_hDC, rcEntire );
			::SetStretchBltMode( dc.m_hDC, nOldStretchBltMode );
			dc.__Flush();
			bRetVal = true;
		}
	}
	if( ! bRetVal )
		dc.__Flush( FALSE );
	return bRetVal;
}

void CExtDateBrowserWnd::OnSysColorChange() 
{
	ASSERT_VALID( this );
	CWnd::OnSysColorChange();
//CExtPaintManager * pPM = PmBridge_GetPM();
//	g_PaintManager.OnSysColorChange( this );
//	g_CmdManager.OnSysColorChange( pPM, this );
	Invalidate();
}

LRESULT CExtDateBrowserWnd::OnThemeChanged( WPARAM wParam, LPARAM lParam )
{
	wParam;
	lParam;
LRESULT lResult = Default();
//CExtPaintManager * pPM = PmBridge_GetPM();
//	g_PaintManager.OnThemeChanged( this, wParam, lParam );
//	g_CmdManager.OnThemeChanged( pPM, this, wParam, lParam );
	return lResult;
}

void CExtDateBrowserWnd::OnSettingChange(UINT uFlags, __EXT_MFC_SAFE_LPCTSTR lpszSection) 
{
	ASSERT_VALID( this );
	CWnd::OnSettingChange(uFlags, lpszSection);
//CExtPaintManager * pPM = PmBridge_GetPM();
//	g_PaintManager.OnSettingChange( this, uFlags, lpszSection );
//	g_CmdManager.OnSettingChange( pPM, this, uFlags, lpszSection );
	Invalidate();
}

void CExtDateBrowserWnd::OnDateBrowserQueryShowPrevNextCenturyItems( bool & bShowItemsBeforeCentury, bool & bShowItemsAfterCentury )
{
	ASSERT_VALID( this );
	bShowItemsAfterCentury = m_bShowItemsAfterCentury;
	bShowItemsBeforeCentury = m_bShowItemsBeforeCentury;
}

void CExtDateBrowserWnd::OnDateBrowserQueryShowPrevNextYearDecadeItems( bool & bShowItemsBeforeYearDecade, bool & bShowItemsAfterYearDecade )
{
	ASSERT_VALID( this );
	bShowItemsAfterYearDecade = m_bShowItemsAfterYearDecade;
	bShowItemsBeforeYearDecade = m_bShowItemsBeforeYearDecade;
}

void CExtDateBrowserWnd::OnDateBrowserQueryShowPrevNextMonthDays( bool & bShowDaysBeforeMonth, bool & bShowDaysAfterMonth )
{
	ASSERT_VALID( this );
	bShowDaysAfterMonth = m_bShowDaysAfterMonth;
	bShowDaysBeforeMonth = m_bShowDaysBeforeMonth;
}

INT CExtDateBrowserWnd::stat_GetColorArraySliceIndex(
	bool bWindowFocused,
	bool bToday
	)
{
INT nSlice = 0;
	if( bWindowFocused )
	{
		if( bToday )
			nSlice = 0;
		else
			nSlice = 1;
	}
	else
	{
		if( bToday )
			nSlice = 2;
		else
			nSlice = 3;
	}
	return nSlice;
}

COLORREF CExtDateBrowserWnd::ItemTextColorGet(
	CExtDateBrowserWnd::e_item_state_t eIST,
	bool bWindowFocused,
	bool bToday
	) const
{
	ASSERT_VALID( this );
	ASSERT( 0 <= INT(eIST) && INT(eIST) < INT(__EIST_COUNT) );
INT nSlice = stat_GetColorArraySliceIndex( bWindowFocused, bToday );
  // W4 Change The eIst value is checked by the assert above. 05012013
#pragma warning(suppress: 6385)
COLORREF clr = m_arrColorsItemText[nSlice][INT(eIST)];
	return clr;
}

void CExtDateBrowserWnd::ItemTextColorSet( 
	CExtDateBrowserWnd::e_item_state_t eIST,
	bool bWindowFocused,
	bool bToday,
	COLORREF clr
	)
{
	ASSERT_VALID( this );
	ASSERT( 0 <= INT(eIST) && INT(eIST) < INT(__EIST_COUNT) );
INT nSlice = stat_GetColorArraySliceIndex( bWindowFocused, bToday );
  // W4 Change The eIst value is checked by the assert above. 05012013
#pragma warning(suppress: 6385)
	if( m_arrColorsItemText[nSlice][INT(eIST)] == clr )
		return;
  // W4 Change The eIst value is checked by the assert above. 05012013
#pragma warning(suppress: 6386)
	m_arrColorsItemText[nSlice][INT(eIST)] = clr;
	if( GetSafeHwnd() != NULL )
		Invalidate();
}

void CExtDateBrowserWnd::ItemTextColorSet( 
	CExtDateBrowserWnd::e_item_state_t eIST,
	bool bWindowFocused,
	COLORREF clr
	)
{
	ASSERT_VALID( this );
	ASSERT( 0 <= INT(eIST) && INT(eIST) < INT(__EIST_COUNT) );
	ItemTextColorSet( eIST, bWindowFocused, true, clr );
	ItemTextColorSet( eIST, bWindowFocused, false, clr );
}

void CExtDateBrowserWnd::ItemTextColorSet( 
	CExtDateBrowserWnd::e_item_state_t eIST,
	COLORREF clr
	)
{
	ASSERT_VALID( this );
	ASSERT( 0 <= INT(eIST) && INT(eIST) < INT(__EIST_COUNT) );
	ItemTextColorSet( eIST, true, clr );
	ItemTextColorSet( eIST, false, clr );
}

COLORREF CExtDateBrowserWnd::ItemBackgroundColorGet(
	CExtDateBrowserWnd::e_item_state_t eIST,
	bool bWindowFocused,
	bool bToday
	) const
{
	ASSERT_VALID( this );
	ASSERT( 0 <= INT(eIST) && INT(eIST) < INT(__EIST_COUNT) );
INT nSlice = stat_GetColorArraySliceIndex( bWindowFocused, bToday );
  // W4 Change The eIst value is checked by the assert above. 05012013
#pragma warning(suppress: 6385)
COLORREF clr = m_arrColorsItemBackground[nSlice][INT(eIST)];
	return clr;
}

void CExtDateBrowserWnd::ItemBackgroundColorSet( 
	CExtDateBrowserWnd::e_item_state_t eIST,
	bool bWindowFocused,
	bool bToday,
	COLORREF clr
	)
{
	ASSERT_VALID( this );
	ASSERT( 0 <= INT(eIST) && INT(eIST) < INT(__EIST_COUNT) );
INT nSlice = stat_GetColorArraySliceIndex( bWindowFocused, bToday );
  // W4 Change The eIst value is checked by the assert above. 05012013
#pragma warning(suppress: 6385)
	if( m_arrColorsItemBackground[nSlice][INT(eIST)] == clr )
		return;
  // W4 Change The eIst value is checked by the assert above. 05012013
#pragma warning(suppress: 6386)
	m_arrColorsItemBackground[nSlice][INT(eIST)] = clr;
	if( GetSafeHwnd() != NULL )
		Invalidate();
}

void CExtDateBrowserWnd::ItemBackgroundColorSet( 
	CExtDateBrowserWnd::e_item_state_t eIST,
	bool bWindowFocused,
	COLORREF clr
	)
{
	ASSERT_VALID( this );
	ASSERT( 0 <= INT(eIST) && INT(eIST) < INT(__EIST_COUNT) );
	ItemBackgroundColorSet( eIST, bWindowFocused, true, clr );
	ItemBackgroundColorSet( eIST, bWindowFocused, false, clr );
}

void CExtDateBrowserWnd::ItemBackgroundColorSet( 
	CExtDateBrowserWnd::e_item_state_t eIST,
	COLORREF clr
	)
{
	ASSERT_VALID( this );
	ASSERT( 0 <= INT(eIST) && INT(eIST) < INT(__EIST_COUNT) );
	ItemBackgroundColorSet( eIST, true, clr );
	ItemBackgroundColorSet( eIST, false, clr );
}

bool CExtDateBrowserWnd::OnDateBrowserHoverChanging(
	const CExtDateBrowserWnd::HITTESTINFO & _hitTestOld,
	const CExtDateBrowserWnd::HITTESTINFO & _hitTestNew
	)
{
	ASSERT_VALID( this );
	_hitTestOld;
	_hitTestNew;
	return true;
}

void CExtDateBrowserWnd::OnDateBrowserHoverChanged(
	const CExtDateBrowserWnd::HITTESTINFO & _hitTestOld,
	const CExtDateBrowserWnd::HITTESTINFO & _hitTestNew
	)
{
	ASSERT_VALID( this );
	_hitTestOld;
	_hitTestNew;
}

void CExtDateBrowserWnd::PreSubclassWindow() 
{
	CWnd::PreSubclassWindow();
	if( m_bDirectCreateCall )
		return;
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		::AfxThrowMemoryException();
	}
}

BOOL CExtDateBrowserWnd::PreCreateWindow(CREATESTRUCT& cs) 
{
	if(		( ! RegisterDateBrowserWndClass() )
		||	( ! CWnd::PreCreateWindow( cs ) )
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	cs.lpszClass = __EXT_DATE_BROWSER_WND_CLASS_NAME;
	return TRUE;
}

LRESULT CExtDateBrowserWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if( message == WM_PRINT || message == WM_PRINTCLIENT )
	{
		CDC & dc = * ( CDC::FromHandle( (HDC)wParam ) );
		CRect rcEntire;
		GetClientRect( &rcEntire );
		COLORREF clrBackground = OnDateBrowserGetBackgroundColor();
		dc.FillSolidRect( &rcEntire, clrBackground );
		bool bWindowFocused = OnDateBrowserQueryWindowFocusedState();
		e_view_mode_t eVM = ViewModeGet();
		COleDateTime _time = TimeGet();
		OnDateBrowserPaintEntire(
			dc,
			_time,
			eVM,
			rcEntire,
			bWindowFocused
			);
		return (!0);
	} // if( message == WM_PRINT || message == WM_PRINTCLIENT )
	return CWnd::WindowProc(message, wParam, lParam);
}

bool CExtDateBrowserWnd::_CreateHelper()
{
	ASSERT_VALID( this );
	return true;
}

bool CExtDateBrowserWnd::g_bDateBrowserWndClassRegistered = false;

bool CExtDateBrowserWnd::RegisterDateBrowserWndClass()
{
	if( g_bDateBrowserWndClassRegistered )
		return true;
WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo( hInst, __EXT_DATE_BROWSER_WND_CLASS_NAME, &_wndClassInfo ) )
	{
		_wndClassInfo.style = CS_GLOBALCLASS|CS_DBLCLKS|CS_HREDRAW|CS_VREDRAW;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor = ::LoadCursor( NULL, IDC_ARROW );
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_DATE_BROWSER_WND_CLASS_NAME;
		if( ! ::AfxRegisterClass( &_wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}
	g_bDateBrowserWndClassRegistered = true;
	return true;
}

bool CExtDateBrowserWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
	UINT nDlgCtrlID, // = UINT( __EXT_MFC_IDC_STATIC )
	DWORD dwWindowStyle, // = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN
	CCreateContext * pContext // = NULL
	)
{
	if( ! RegisterDateBrowserWndClass() )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	m_bDirectCreateCall = true;
	if( ! CWnd::Create(
			__EXT_DATE_BROWSER_WND_CLASS_NAME,
			NULL,
			dwWindowStyle,
			rcWnd,
			pParentWnd,
			nDlgCtrlID,
			pContext
			)
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		::AfxThrowMemoryException();
	}
	return TRUE;
}

#endif // (!defined __EXT_MFC_NO_DATE_BROWSER)

#endif // (!defined __EXT_MFC_NO_DATE_PICKER) || (!defined __EXT_MFC_NO_DATE_BROWSER)

