// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_FORMULA_GRID)

	#if (!defined __EXT_FORMULAGRID_H)
		#include <ExtFormulaGrid.h>
	#endif // (!defined __EXT_FORMULAGRID_H)

	#include <Resources/Resource.h>

IMPLEMENT_SERIAL( CExtFormulaGridCell, CExtGridCellVariant, VERSIONABLE_SCHEMA|1 );

#endif // (!defined __EXT_MFC_NO_FORMULA_GRID)

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#if (!defined __EXT_MFC_NO_FORMULA_GRID)

//////////////////////////////////////////////////////////////
// class CExtFormulaValue

CExtFormulaValue::CExtFormulaValue()
	: m_eType( CExtFormulaValue::__EFVT_EMPTY )
{
	m_val.ModifyStyleEx( 0, __EGCS_EX_EMPTY );
}

CExtFormulaValue::CExtFormulaValue( const CExtFormulaValue & other )
	: m_eType( CExtFormulaValue::__EFVT_EMPTY )
{
	Assign( other );
}

CExtFormulaValue::~CExtFormulaValue()
{
	Empty();
}

CExtSafeString CExtFormulaValue::stat_ErrorDescription( CExtFormulaValue::e_formula_error_code_t err )
{
CExtSafeString str;
	switch( err )
	{
	case __EFEC_OK:																		str = _T("No error."); break;
	case __EFEC_SYNTAX_ERROR:															str = _T("Syntax error."); break;
	case __EFEC_NOT_ENOUGH_MEMORY:														str = _T("Not enough memory."); break;
	case __EFEC_DIVISION_BY_ZERO:														str = _T("Division by zero."); break;
	case __EFEC_DIVISION_BY_NEGATIVE:													str = _T("Division by negative."); break;
	case __EFEC_INVALID_TYPE_FOR_OPERATION:												str = _T("Invalid type for operation."); break;
	case __EFEC_FUNCTION_CANNOT_BE_USED_AS_IDENTIFIER:									str = _T("Function name cannot be used as identifier."); break;
	case __EFEC_CANNOT_COMPUTE_FACTORIAL:												str = _T("Factorial cannot be computed."); break;
	case __EFEC_ERROR_AS_FUNCTION_EXECUTION_RESULT_OR_CANNOT_COMPUTE_RESULT:			str = _T("Cannot compute function result."); break;
	case __EFEC_PARAMETER_IS_OUT_OF_RANGE:												str = _T("Parameter is out of range."); break;
	case __EFEC_DEPENDENCY_COMPUTATION_FAILURE:											str = _T("Dependency computation error."); break;
	case __EFEC_UNKNOWN_IDENTIFIER:														str = _T("Unknown identifier or variable name."); break;
	case __EFEC_RESULT_IS_RANGE_REFERENCE:												str = _T("Expression result is the reference to the range of cells."); break;

	case __EFEC_TYPE_CONVERSION_ERROR:													str = _T("Type conversion error."); break;
	case __EFEC_TYPE_R8_CONVERSION_ERROR:												str = _T("Floating point type conversion error."); break;
	case __EFEC_TYPE_R8_CONVERSION_ERROR_LEFT:											str = _T("Floating point type conversion error for the left part of expression."); break;
	case __EFEC_TYPE_R8_CONVERSION_ERROR_RIGHT:											str = _T("Floating point type conversion error for the right part of expression."); break;
	case __EFEC_TYPE_BOOL_CONVERSION_ERROR:												str = _T("Boolean type conversion error."); break;
	case __EFEC_TYPE_I4_CONVERSION_ERROR:												str = _T("Numeric type (fixed point) conversion error."); break;
	case __EFEC_TYPE_BSTR_CONVERSION_ERROR:												str = _T("String type conversion error."); break;
	case __EFEC_TYPE_DATE_CONVERSION_ERROR:												str = _T("Date/time type conversion error."); break;

	case __EFEC_STRING_TYPE_REQUIRED_LEFT:												str = _T("String value is required in the left part of expression."); break;
	case __EFEC_STRING_TYPE_REQUIRED_RIGHT:												str = _T("String value is required in the right part of expression."); break;
	case __EFEC_NON_EMPTY_STRING_REQUIRED:												str = _T("Non empty string value is required."); break;

	case __EFEC_FUNCTION_IS_NOT_FOUND:													str = _T("Unknown function."); break;
	case __EFEC_FUNCTION_IS_NOT_IMPLEMENTED:											str = _T("Function is not implemented."); break;
	case __EFEC_FUNCTION_INVALID_PARAMETER_COUNT:										str = _T("Invalid parameter count."); break;
	case __EFEC_FUNCTION_CANNOT_ACCEPT_EMPTY_PARAMETERS:								str = _T("Function cannot accept empty/unknown values in parameters."); break;
	case __EFEC_FUNCTION_CANNOT_ACCEPT_CELL_RANGE_PARAMETERS:							str = _T("Function cannot accept cell ranges in parameters."); break;

	default:																			str.Format( _T("Unknown error (%d)."), INT(err) ); break;
	} // switch( err )
	return str;
}

bool CExtFormulaValue::PreAdjustDT2R8()
{
	if( m_val.vt != VT_DATE )
		return false;
	m_val.vt = VT_R8;
COleDateTime _odt( m_val.date );
	if(		_odt.GetYear() == 1900
		&&	_odt.GetMonth() == 1
		&&	_odt.GetDay() == 1
		)
		m_val.dblVal -= 2.0;
	return true;
}

INT CExtFormulaValue::Compare( const CExtFormulaValue & other ) const
{
	__EXT_DEBUG_GRID_ASSERT( m_eType == other.m_eType );
INT nRetVal = INT( m_val.Compare( other.m_val ) );
	return nRetVal;
}

void CExtFormulaValue::Assign( const CExtFormulaValue & other )
{
	m_eType = other.m_eType;
	m_val = other.m_val;
}

bool CExtFormulaValue::IsEmpty() const
{
	if( m_eType == __EFVT_EMPTY )
		return true;
	return false;
}

void CExtFormulaValue::Empty()
{
	m_eType = __EFVT_EMPTY;
	m_val.Empty();
	m_val.ModifyStyleEx( 0, __EGCS_EX_EMPTY );
	m_gr.Empty();
}

//////////////////////////////////////////////////////////////
// class CExtFormulaValueArray

CExtFormulaValue::e_formula_error_code_t CExtFormulaValueArray::ComputeValue(
	CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
	INT nIndex,
	CExtFormulaDataManager & _FDM,
	const CExtFormulaItem * pFI,
	const CExtFormulaItem ** ppErrorLocationForumaItem, // = NULL
	VARTYPE vtConvertTo, // = VT_EMPTY // default - do not perform conversion
	CExtFormulaValue::e_formula_error_code_t errConversionCode // = CExtFormulaValue::__EFEC_TYPE_CONVERSION_ERROR
	)
{
	__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
CExtFormulaValue & _FV = GetRefAt( nIndex );
CExtFormulaValue::e_formula_error_code_t err = pFI->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem  );
	if( err != CExtFormulaValue::__EFEC_OK && ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
		(*ppErrorLocationForumaItem) = pFI;
	if( vtConvertTo != VT_EMPTY )
	{
		if( ! _FV.m_val._VariantChangeType( vtConvertTo ) )
		{
			err = errConversionCode;
			if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
				(*ppErrorLocationForumaItem) = pFI;
		}
	}
	return err;
}

CExtFormulaValue::e_formula_error_code_t CExtFormulaValueArray::ComputeAllValues(
	CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
	CExtFormulaDataManager & _FDM,
	const CExtFormulaItem * pFI,
	const CExtFormulaItem ** ppErrorLocationForumaItem, // = NULL
	VARTYPE vtConvertTo, // = VT_EMPTY // default - do not perform conversion
	CExtFormulaValue::e_formula_error_code_t errConversionCode // = CExtFormulaValue::__EFEC_TYPE_CONVERSION_ERROR
	)
{
	__EXT_DEBUG_GRID_ASSERT( errConversionCode != CExtFormulaValue::__EFEC_OK );
CExtFormulaValue::e_formula_error_code_t err = CExtFormulaValue::__EFEC_OK;
INT nIndex, nCount = GetCount();
	__EXT_DEBUG_GRID_ASSERT( nCount == pFI->GetChildCount() );
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		const CExtFormulaItem * pChild = pFI->GetChildAt( nIndex );
		err = ComputeValue( _mapCellsAlreadyComputed, nIndex, _FDM, pChild,  ppErrorLocationForumaItem, vtConvertTo, errConversionCode );
		if( err != CExtFormulaValue::__EFEC_OK )
			break;
	}
	return err;
}

CExtFormulaValue::e_formula_error_code_t CExtFormulaValueArray::ComputeOR(
	CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
	bool & bLogicalResult,
	CExtFormulaDataManager & _FDM,
	const CExtFormulaItem * pFI,
	const CExtFormulaItem ** ppErrorLocationForumaItem // = NULL
	)
{
CExtFormulaValue::e_formula_error_code_t err = CExtFormulaValue::__EFEC_OK;
INT nIndex, nCount = GetCount();
	__EXT_DEBUG_GRID_ASSERT( nCount == pFI->GetChildCount() );
	bLogicalResult = false;
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		const CExtFormulaItem * pChild = pFI->GetChildAt( nIndex );
		err = ComputeValue( _mapCellsAlreadyComputed, nIndex, _FDM, pChild,  ppErrorLocationForumaItem, VT_BOOL, CExtFormulaValue::__EFEC_TYPE_BOOL_CONVERSION_ERROR );
		if( err != CExtFormulaValue::__EFEC_OK )
			break;
		CExtFormulaValue & _FV = GetRefAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT( _FV.m_val.vt == VT_BOOL );
		if( _FV.m_val.boolVal != VARIANT_FALSE )
		{
			bLogicalResult = true;
			break;
		}
	}
	return err;
}

CExtFormulaValue::e_formula_error_code_t CExtFormulaValueArray::ComputeAND(
	CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
	bool & bLogicalResult,
	CExtFormulaDataManager & _FDM,
	const CExtFormulaItem * pFI,
	const CExtFormulaItem ** ppErrorLocationForumaItem // = NULL
	)
{
CExtFormulaValue::e_formula_error_code_t err = CExtFormulaValue::__EFEC_OK;
INT nIndex, nCount = GetCount();
	__EXT_DEBUG_GRID_ASSERT( nCount == pFI->GetChildCount() );
	bLogicalResult = true;
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		const CExtFormulaItem * pChild = pFI->GetChildAt( nIndex );
		err = ComputeValue( _mapCellsAlreadyComputed, nIndex, _FDM, pChild,  ppErrorLocationForumaItem, VT_BOOL, CExtFormulaValue::__EFEC_TYPE_BOOL_CONVERSION_ERROR );
		if( err != CExtFormulaValue::__EFEC_OK )
			break;
		CExtFormulaValue & _FV = GetRefAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT( _FV.m_val.vt == VT_BOOL );
		if( _FV.m_val.boolVal == VARIANT_FALSE )
		{
			bLogicalResult = false;
			break;
		}
	}
	return err;
}

//////////////////////////////////////////////////////////////
// class CExtFormulaFunction

CExtFormulaFunction::CExtFormulaFunction()
{
}

CExtFormulaFunction::~CExtFormulaFunction()
{
}

__EXT_MFC_SAFE_LPCTSTR CExtFormulaFunction::GetFunctionCategoryName() const
{
e_formula_function_category_t eFFC = GetFunctionCategory();
	switch( eFFC )
	{
	case __FFC_LOGICAL:					return _T("Logical");
	case __FFC_MATH_AND_TRIG:			return _T("Math & Trig");
	case __FFC_STATISTICAL:				return _T("Statistical");
	case __FFC_TEXT:					return _T("Text");
	case __FFC_DATE_AND_TIME:			return _T("Date & Time");
	case __FFC_LOOKUP_AND_REFERENCE:	return _T("Lookup & Reference");
	case __FFC_INFORMATION:				return _T("Information");
	} // switch( eFFC )
	return _T("Unknown");
}

CExtFormulaValue::e_formula_error_code_t CExtFormulaFunction::Exec(
	CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
	CExtFormulaValue & _FV,
	CExtFormulaDataManager & _FDM,
	const CExtFormulaItem * pFI,
	const CExtFormulaItem ** ppErrorLocationForumaItem // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
	if( pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_IDENTIFIER )
	{
		if( ! CanBeIdentifier() )
		{
			if( ppErrorLocationForumaItem != NULL )
				(*ppErrorLocationForumaItem) = pFI;
			return CExtFormulaValue::__EFEC_FUNCTION_CANNOT_BE_USED_AS_IDENTIFIER;
		}
	}
	__EXT_DEBUG_GRID_ASSERT(
			pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL
		||	(	pFI->m_eType == CExtFormulaItem::__FORMULA_ATOM_IDENTIFIER
			&&	CanBeIdentifier()
			)
		);
INT nParameterIndex, nParameterCount = pFI->GetChildCount();
	if( ! IsParameterCountValid( nParameterCount ) )
	{
		if( ppErrorLocationForumaItem != NULL )
			(*ppErrorLocationForumaItem) = pFI;
		return CExtFormulaValue::__EFEC_FUNCTION_INVALID_PARAMETER_COUNT;
	}
bool bAcceptRangeParameters = CanAcceptRangeParameters();
bool bAcceptEmptyParameters = CanAcceptEmptyParameters();
	if( ! ( bAcceptRangeParameters || bAcceptEmptyParameters ) )
	{
		for( nParameterIndex = 0; nParameterIndex < nParameterCount; nParameterIndex ++ )
		{
			const CExtFormulaItem * pChild = pFI->GetChildAt( nParameterIndex );
			switch( pChild->m_eType )
			{
			case CExtFormulaItem::__FORMULA_ATOM_UNKNOWN:
				if( ! bAcceptEmptyParameters )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI;
					return CExtFormulaValue::__EFEC_FUNCTION_CANNOT_ACCEPT_EMPTY_PARAMETERS;
				}
			break;
			case CExtFormulaItem::__FORMULA_ATOM_CELL_RANGE:
			case CExtFormulaItem::__FORMULA_ATOM_CELL_RANGE_INTERSECTION:
				if( ! bAcceptRangeParameters )
				{
					if( ppErrorLocationForumaItem != NULL )
						(*ppErrorLocationForumaItem) = pFI;
					return CExtFormulaValue::__EFEC_FUNCTION_CANNOT_ACCEPT_CELL_RANGE_PARAMETERS;
				}
			break;
			}
		}
	}
	_FV.Empty();
	return OnExec( _mapCellsAlreadyComputed, _FV, _FDM, pFI, ppErrorLocationForumaItem );
}

bool CExtFormulaFunction::CanBeIdentifier() const
{
INT n = GetParameterCountStatic();
	return ( n == 0 ) ? true : false;
}

INT CExtFormulaFunction::GetParameterCountStatic() const
{
INT n1 = GetParameterCountMin(), n2 = GetParameterCountMax();
	if( n1 == n2 )
		return n1;
	else
		return -1;
}

bool CExtFormulaFunction::IsParameterCountValid( INT nParameterCount ) const
{
	__EXT_DEBUG_GRID_ASSERT( nParameterCount >= 0 );
	if( nParameterCount < 0 )
		return false;
INT n;
	// check whether static parameter count is required
	n = GetParameterCountStatic();
	if( n >= 0 )
	{
		if( nParameterCount != n )
			return false;
		return true;
	}
	// check minimal parameter count
	n = GetParameterCountMin();
	if( n >= 0 )
	{
		if( nParameterCount < n )
			return false;
	}
	// check maximal parameter count
	n = GetParameterCountMax();
	if( n >= 0 )
	{
		if( nParameterCount > n )
			return false;
	}
	return true;
}

bool CExtFormulaFunction::CanAcceptRangeParameters() const
{
	return false;
}

bool CExtFormulaFunction::CanAcceptEmptyParameters() const
{
	return false;
}

void CExtFormulaFunction::OnQueryTextFDA(
	INT nTextIndex, // 0 - function description, positive - parameter description by index, -negative - parameter abreviation by index
	CExtSafeString & strText
	) const
{
	nTextIndex;
	strText.Empty();
}

CExtSafeString CExtFormulaFunction::GetFunctionDescription() const
{
CExtSafeString strText;
	OnQueryTextFDA( 0, strText );
	return strText;
}

CExtSafeString CExtFormulaFunction::GetParameterAbreviationFormat( INT nParameterIndex ) const
{
	__EXT_DEBUG_GRID_ASSERT( nParameterIndex >= 1 );
INT n = GetParameterCountMax();
	if( n >= 0 )
		nParameterIndex = min( n, nParameterIndex );
	else
	{
		n = GetParameterCountMin();
		if( n >= 0 )
			nParameterIndex = min( n, nParameterIndex );
	}
CExtSafeString strText;
	OnQueryTextFDA( -nParameterIndex, strText );
	return strText;
}

CExtSafeString CExtFormulaFunction::GetParameterDescription( INT nParameterIndex ) const
{
	__EXT_DEBUG_GRID_ASSERT( nParameterIndex >= 1 );
INT n = GetParameterCountMax();
	if( n >= 0 )
		nParameterIndex = min( n, nParameterIndex );
	else
	{
		n = GetParameterCountMin();
		if( n >= 0 )
			nParameterIndex = min( n, nParameterIndex );
	}
CExtSafeString strText;
	OnQueryTextFDA( nParameterIndex, strText );
	return strText;
}

//////////////////////////////////////////////////////////////
// class CExtFormulaFunctionSet

CExtFormulaFunctionSet::CExtFormulaFunctionSet()
{
}

CExtFormulaFunctionSet::~CExtFormulaFunctionSet()
{
	RemoveAllFunctions();
}

LONG CExtFormulaFunctionSet::EnumGetCount(
	CExtFormulaFunction::e_formula_function_category_t eFFC // = CExtFormulaFunction::__FFC_ENUM_ANY
	) const
{
LONG nCount = 0L;
	if( eFFC == CExtFormulaFunction::__FFC_ENUM_ANY )
		nCount = LONG( m_map.GetCount() );
	else
	{
		POSITION pos = EnumStart();
		for( ; pos != NULL; EnumNext( pos, eFFC ) )
			nCount ++;
	}
	return nCount;
}

POSITION CExtFormulaFunctionSet::EnumStart() const
{
POSITION pos = m_map.GetStartPosition();
	return pos;
}

CExtFormulaFunction * CExtFormulaFunctionSet::EnumNext(
	POSITION & pos,
	CExtFormulaFunction::e_formula_function_category_t eFFC // = CExtFormulaFunction::__FFC_ENUM_ANY
	)
{
	if( pos == NULL )
		return NULL;
	for( ; pos != NULL; )
	{
		CExtSafeString strFunctionName;
		CExtFormulaFunction * pFF = NULL;
		m_map.GetNextAssoc( pos, strFunctionName, (void*&)pFF );
		__EXT_DEBUG_GRID_ASSERT( ! strFunctionName.IsEmpty() );
		__EXT_DEBUG_GRID_ASSERT( pFF != NULL );
		if( eFFC == CExtFormulaFunction::__FFC_ENUM_ANY )
			return pFF;
		if( pFF->GetFunctionCategory() == eFFC )
		{
			POSITION pos2 = pos;
			if( EnumNext( pos2, eFFC ) == NULL )
				pos = NULL;
			return pFF;
		}
	}
	return NULL;
}

const CExtFormulaFunction * CExtFormulaFunctionSet::EnumNext(
	POSITION & pos,
	CExtFormulaFunction::e_formula_function_category_t eFFC // = CExtFormulaFunction::__FFC_ENUM_ANY
	) const
{
	return ( const_cast < CExtFormulaFunctionSet * > ( this ) ) -> EnumNext( pos, eFFC );
}

CExtFormulaFunction * CExtFormulaFunctionSet::GetFunction( __EXT_MFC_SAFE_LPCTSTR strFunctionName )
{
	if( strFunctionName == NULL )
		return NULL;
CExtFormulaFunction * pFF = NULL;
	if( ! m_map.Lookup( strFunctionName, (void*&)pFF ) )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT( pFF != NULL );
	return pFF;
}

bool CExtFormulaFunctionSet::AddFunction(
	CExtFormulaFunction * pFF,
	bool bErrorIfAlreadyRegistered // = false
	)
{
	if( pFF == NULL )
		return false;
LPCTSTR strFunctionName = LPCTSTR( pFF->GetFunctionName() );
	__EXT_DEBUG_GRID_ASSERT( strFunctionName != NULL );
	if( GetFunction( strFunctionName ) != NULL )
		return (!bErrorIfAlreadyRegistered);
	m_map.SetAt( strFunctionName, pFF );
	return true;
}

bool CExtFormulaFunctionSet::RemoveFunction( CExtFormulaFunction * pFF )
{
	if( pFF == NULL )
		return false;
LPCTSTR strFunctionName = LPCTSTR( pFF->GetFunctionName() );
	__EXT_DEBUG_GRID_ASSERT( strFunctionName != NULL );
	return RemoveFunction( strFunctionName );
}

bool CExtFormulaFunctionSet::RemoveFunction( __EXT_MFC_SAFE_LPCTSTR strFunctionName )
{
	if( strFunctionName == NULL )
		return NULL;
CExtFormulaFunction * pFF = GetFunction( strFunctionName );
	if( pFF == NULL )
		return false;
	delete pFF;
	m_map.RemoveKey( strFunctionName );
	return true;
}

void CExtFormulaFunctionSet::RemoveAllFunctions()
{
POSITION pos = m_map.GetStartPosition();
	for( ; pos != NULL; )
	{
		CExtSafeString strFunctionName;
		CExtFormulaFunction * pFF = NULL;
		m_map.GetNextAssoc( pos, strFunctionName, (void*&)pFF );
		__EXT_DEBUG_GRID_ASSERT( ! strFunctionName.IsEmpty() );
		__EXT_DEBUG_GRID_ASSERT( pFF != NULL );
		delete pFF;
	}
	m_map.RemoveAll();
}

bool CExtFormulaFunctionSet::AddCategoryFunctions(
	CExtFormulaFunction::e_formula_function_category_t eFFC,
	bool bErrorIfAlreadyRegistered // = false
	)
{
	try
	{
		switch( eFFC )
		{
		case CExtFormulaFunction::__FFC_LOGICAL:
			if( ! AddFunction( new FF_Logical_FALSE, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Logical_TRUE, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Logical_OR, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Logical_AND, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Logical_IF, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Logical_IFERROR, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Logical_NOT, bErrorIfAlreadyRegistered ) )
				return false;
		break;
		case CExtFormulaFunction::__FFC_MATH_AND_TRIG:
			if( ! AddFunction( new FF_MathTrig_PI, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_RAND, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_RANDBETWEEN, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_ABS, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_SIN, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_COS, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_TAN, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_ASIN, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_ACOS, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_ATAN, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_SINH, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_COSH, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_TANH, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_SQRT, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_SQRTPI, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_SIGN, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_DEGREES, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_RADIANS, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_EXP, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_LN, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_LOG, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_LOG10, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_FACT, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_POWER, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_EVEN, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_INT, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_ODD, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_MOD, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_TRUNC, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_ROUND, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_ROUNDDOWN, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_ROUNDUP, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_CEILING, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_MathTrig_FLOOR, bErrorIfAlreadyRegistered ) )
				return false;
		break;
		case CExtFormulaFunction::__FFC_STATISTICAL:
			if( ! AddFunction( new FF_Statistical_MIN, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Statistical_MAX, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Statistical_SUM, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Statistical_AVERAGE, bErrorIfAlreadyRegistered ) )
				return false;
		break;
		case CExtFormulaFunction::__FFC_TEXT:
			if( ! AddFunction( new FF_Text_CHAR, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_CODE, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_CONCATENATE, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_EXACT, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_LEFT, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_MID, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_RIGHT, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_TRIM, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_LOWER, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_UPPER, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_LEN, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_REPT, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_FIND, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_REPLACE, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_SEARCH, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_SUBSTITUTE, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Text_T, bErrorIfAlreadyRegistered ) )
				return false;
		break;
		case CExtFormulaFunction::__FFC_DATE_AND_TIME:
			if( ! AddFunction( new FF_DateTime_DATE, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_DateTime_DATEVALUE, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_DateTime_DAY, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_DateTime_MONTH, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_DateTime_YEAR, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_DateTime_TIME, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_DateTime_TIMEVALUE, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_DateTime_HOUR, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_DateTime_MINUTE, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_DateTime_SECOND, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_DateTime_NOW, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_DateTime_TODAY, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_DateTime_WEEKDAY, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_DateTime_WEEKNUM, bErrorIfAlreadyRegistered ) )
				return false;
		break;
		case CExtFormulaFunction::__FFC_LOOKUP_AND_REFERENCE:
			if( ! AddFunction( new FF_LookupRef_CHOOSE, bErrorIfAlreadyRegistered ) )
				return false;
		break;
		case CExtFormulaFunction::__FFC_INFORMATION:
			if( ! AddFunction( new FF_Info_ISEVEN, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Info_ISODD, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Info_ISLOGICAL, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Info_ISNUMBER, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Info_ISTEXT, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Info_ISBLANK, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Info_ISNOTEXT, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Info_ISERROR, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Info_ISERR, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Info_ISNA, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Info_TYPE, bErrorIfAlreadyRegistered ) )
				return false;
			if( ! AddFunction( new FF_Info_N, bErrorIfAlreadyRegistered ) )
				return false;
		break;
		} // switch( eFFC )
	}
	catch( CException * pException )
	{
		pException->Delete();
		return false;
	}
	return true;
}

bool CExtFormulaFunctionSet::AddAllFunctions(
	bool bErrorIfAlreadyRegistered // = false
	)
{
INT nIndex, nCount = INT(CExtFormulaFunction::__FFC_STD_CATEGORY_COUNT);
	for( nIndex = 0; nIndex < nCount; nIndex ++  )
	{
		CExtFormulaFunction::e_formula_function_category_t eFFC =
			(CExtFormulaFunction::e_formula_function_category_t)nIndex;
		if( ! AddCategoryFunctions( eFFC, bErrorIfAlreadyRegistered ) )
			return false;
	}
	return true;
}

bool CExtFormulaFunctionSet::RegisterAllFunctions()
{
	RemoveAllFunctions();
	return AddAllFunctions( true );
}

//////////////////////////////////////////////////////////////
// class CExtFormulaItem

CExtFormulaItem::CExtFormulaItem(
	CExtFormulaItem::formula_atom_type_t eType //= CExtFormulaItem::__FORMULA_ATOM_UNKNOWN
	)
	: m_eType( eType )
	, m_pParent( NULL )
	, m_nScanPosStart( 0 )
	, m_nScanPosEnd( 0 )
	, m_ptScanPosStart( 0, 0 )
	, m_ptScanPosEnd( 0, 0 )
	, m_bBold( false )
	, m_bItalic( false )
	, m_clr( COLORREF(-1L) )
	, m_rcEditedRange( -1L, -1L, -1L, -1L )
	, m_rcEditedRangeNN( -1L, -1L, -1L, -1L )
{
}

CExtFormulaItem::~CExtFormulaItem()
{
	RemoveAllChildren();
}

INT CExtFormulaItem::InsertChildAt( CExtFormulaItem * pChild, INT nIndex )
{
INT nCount = GetChildCount();
	if( nIndex < 0 || nIndex > nCount )
		nIndex = nCount;
	pChild->m_pParent = this;
	m_arrChildren.InsertAt( nIndex, pChild );
	return nIndex;
}

void CExtFormulaItem::AddChild( CExtFormulaItem * pChild )
{
	InsertChildAt( pChild, -1 );
}

void CExtFormulaItem::AddChildrenOf( CExtFormulaItem * pOther, bool bDeleteOther )
{
	if( pOther == NULL )
		return;
INT nIndex, nCount = pOther->GetChildCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtFormulaItem * pChild = pOther->GetChildAt( nIndex );
		__EXT_DEBUG_GRID_ASSERT( pChild != NULL );
		AddChild( pChild );
	}
	pOther->m_arrChildren.RemoveAll();
	if( bDeleteOther )
		delete pOther;
}

void CExtFormulaItem::CopyScanInfoFrom(
	const CExtFormulaItem * pOther,
	bool bCopyFromChildren,
	bool bCopyStartInfo, // = true
	bool bCopyEndInfo // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT( pOther != NULL );
INT nCount = pOther->GetChildCount();
	if( bCopyFromChildren && nCount > 0 )
	{
		if( bCopyStartInfo )
		{
			const CExtFormulaItem * pChild1 = pOther->GetChildAt( 0 );
			m_nScanPosStart = pChild1->m_nScanPosStart;
			m_ptScanPosStart = pChild1->m_ptScanPosStart;
		}
		if( bCopyEndInfo )
		{
			const CExtFormulaItem * pChild2 = pOther->GetChildAt( nCount - 1 );
			m_nScanPosEnd = pChild2->m_nScanPosEnd;
			m_ptScanPosEnd = pChild2->m_ptScanPosEnd;
		}
		return;
	}
	if( bCopyStartInfo )
	{
		m_nScanPosStart = pOther->m_nScanPosStart;
		m_ptScanPosStart = pOther->m_ptScanPosStart;
	}
	if( bCopyEndInfo )
	{
		m_nScanPosEnd = pOther->m_nScanPosEnd;
		m_ptScanPosEnd = pOther->m_ptScanPosEnd;
	}
}

void CExtFormulaItem::RemoveAllChildren()
{
INT nIndex, nCount = INT( m_arrChildren.GetSize() );
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtFormulaItem * pChild = m_arrChildren[ nIndex ];
		delete pChild;
	}
	m_arrChildren.RemoveAll();
}

void CExtFormulaItem::DetachAllChildren()
{
	m_arrChildren.RemoveAll();
}

INT CExtFormulaItem::GetChildCount() const
{
	INT nCount = INT( m_arrChildren.GetSize() );
	return nCount;
}

CExtFormulaItem * CExtFormulaItem::GetChildAt( INT nIndex )
{
	if( nIndex < 0 )
		return NULL;
INT nCount = GetChildCount();
	if( nIndex >= nCount )
		return NULL;
CExtFormulaItem * pChild = m_arrChildren[ nIndex ];
	return pChild;
}

const CExtFormulaItem * CExtFormulaItem::GetChildAt( INT nIndex ) const
{
	return const_cast < CExtFormulaItem * > ( this ) -> GetChildAt( nIndex );
}

INT CExtFormulaItem::GetIndexOfChild( const CExtFormulaItem * pChild ) const
{
	if( pChild == NULL )
		return -1;
INT nIndex, nCount = GetChildCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		const CExtFormulaItem * pChild2 = GetChildAt( nIndex );
		if( ((void*)pChild) == ((void*)pChild2) )
			return nIndex;
	}
	return -1;
}

CExtFormulaItem * CExtFormulaItem::GetParent()
{
	return m_pParent;
}

const CExtFormulaItem * CExtFormulaItem::GetParent() const
{
	return m_pParent;
}

INT CExtFormulaItem::GetIndentLevel() const
{
const CExtFormulaItem *  pParent = GetParent();
	if( pParent != NULL )
		return pParent->GetIndentLevel() + 1;
	return 0;
}

CExtFormulaValue::e_formula_error_code_t CExtFormulaItem::ComputeValue(
	CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
	CExtFormulaValue & _FV,
	CExtFormulaDataManager & _FDM,
	const CExtFormulaItem ** ppErrorLocationForumaItem // = NULL
	) const
{
	_FV.Empty();
	if( ppErrorLocationForumaItem != NULL )
		(*ppErrorLocationForumaItem) = NULL;
	if( ComputeSimpleValue( _FV ) )
		return CExtFormulaValue::__EFEC_OK;
CExtFormulaValue::e_formula_error_code_t err = CExtFormulaValue::__EFEC_OK;
bool bChangeTypeToR8 = false;
	switch( m_eType )
	{
	case __FORMULA_ATOM_EXPR_MULTIPLY:
	case __FORMULA_ATOM_EXPR_DIVIDE:
	//case __FORMULA_ATOM_EXPR_MODULE:
	case __FORMULA_ATOM_EXPR_PLUS:
	case __FORMULA_ATOM_EXPR_MINUS:
	case __FORMULA_ATOM_EXPR_POW:
		bChangeTypeToR8 = true;
		// continue falling
	case __FORMULA_ATOM_EXPR_STR_CAT:
	case __FORMULA_ATOM_EXPR_LESS:
	case __FORMULA_ATOM_EXPR_LESS_EQUAL:
	case __FORMULA_ATOM_EXPR_GREATER:
	case __FORMULA_ATOM_EXPR_GREATER_EQUAL:
	case __FORMULA_ATOM_EXPR_EQUAL:
	case __FORMULA_ATOM_EXPR_NOT_EQUAL:
		__EXT_DEBUG_GRID_ASSERT( GetChildCount() == 2 );
		{ // block-begin
			CExtFormulaValue _FV_left;
			const CExtFormulaItem * pFI_left = GetChildAt( 0 );
			err = pFI_left->ComputeValue( _mapCellsAlreadyComputed, _FV_left, _FDM, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI_left;
				return err;
			}
			VARTYPE vtRealLeft = _FV_left.m_val.vt;
			if( vtRealLeft == VT_DATE )
				_FV_left.PreAdjustDT2R8();
			CExtFormulaValue _FV_right;
			const CExtFormulaItem * pFI_right = GetChildAt( 1 );
			err = pFI_right->ComputeValue( _mapCellsAlreadyComputed, _FV_right, _FDM, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = pFI_right;
				return err;
			}
			VARTYPE vtRealRight = _FV_right.m_val.vt;
			if( vtRealRight == VT_DATE )
				_FV_right.PreAdjustDT2R8();
			if(		_FV_left.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT
				&&	_FV_right.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT
				)
			{ // if both left and right are variants
				if( bChangeTypeToR8 )
				{
					if( ! _FV_left.m_val._VariantChangeType( VT_R8 ) )
					{
						if( ppErrorLocationForumaItem != NULL )
							(*ppErrorLocationForumaItem) = this;
						return CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR_LEFT;
					}
					if( ! _FV_right.m_val._VariantChangeType( VT_R8 ) )
					{
						if( ppErrorLocationForumaItem != NULL )
							(*ppErrorLocationForumaItem) = this;
						return CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR_RIGHT;
					}
				} // if( bChangeTypeToR8 )
				switch( m_eType )
				{
				case __FORMULA_ATOM_EXPR_MULTIPLY:
					{
						__EXT_DEBUG_GRID_ASSERT( _FV_left.m_val.vt == VT_R8 );
						__EXT_DEBUG_GRID_ASSERT( _FV_right.m_val.vt == VT_R8 );
						double lfVal = (double)( _FV_left.m_val.dblVal * _FV_right.m_val.dblVal );
						_FV.m_val._VariantAssign( lfVal, VT_R8 );
						if( vtRealLeft == VT_DATE )
							_FV.m_val.vt = vtRealLeft;
						_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
					}
					return CExtFormulaValue::__EFEC_OK;
				case __FORMULA_ATOM_EXPR_DIVIDE:
					{
						if( _FV_right.m_val.dblVal == 0 )
						{
							if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
								(*ppErrorLocationForumaItem) = this;
							return CExtFormulaValue::__EFEC_DIVISION_BY_ZERO;
						}
						__EXT_DEBUG_GRID_ASSERT( _FV_left.m_val.vt == VT_R8 );
						__EXT_DEBUG_GRID_ASSERT( _FV_right.m_val.vt == VT_R8 );
						double lfVal = (double)( _FV_left.m_val.dblVal / _FV_right.m_val.dblVal );
						_FV.m_val._VariantAssign( lfVal, VT_R8 );
						if( vtRealLeft == VT_DATE )
							_FV.m_val.vt = vtRealLeft;
						_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
					}
					return CExtFormulaValue::__EFEC_OK;
//				case __FORMULA_ATOM_EXPR_MODULE:
//					__EXT_DEBUG_GRID_ASSERT( FALSE );
//					return CExtFormulaValue::__EFEC_TYPE_PERCENT_NOT_IMPLEMENTED;
				case __FORMULA_ATOM_EXPR_PLUS:
					{
						__EXT_DEBUG_GRID_ASSERT( _FV_left.m_val.vt == VT_R8 );
						__EXT_DEBUG_GRID_ASSERT( _FV_right.m_val.vt == VT_R8 );
						double lfVal = (double)( _FV_left.m_val.dblVal + _FV_right.m_val.dblVal );
						_FV.m_val._VariantAssign( lfVal, VT_R8 );
						if( vtRealLeft == VT_DATE )
							_FV.m_val.vt = vtRealLeft;
						_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
					}
					return CExtFormulaValue::__EFEC_OK;
				case __FORMULA_ATOM_EXPR_MINUS:
					{
						__EXT_DEBUG_GRID_ASSERT( _FV_left.m_val.vt == VT_R8 );
						__EXT_DEBUG_GRID_ASSERT( _FV_right.m_val.vt == VT_R8 );
						double lfVal = (double)( _FV_left.m_val.dblVal - _FV_right.m_val.dblVal );
						_FV.m_val._VariantAssign( lfVal, VT_R8 );
						if( vtRealLeft == VT_DATE )
							_FV.m_val.vt = vtRealLeft;
						_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
					}
					return CExtFormulaValue::__EFEC_OK;
				case __FORMULA_ATOM_EXPR_POW:
					{
						__EXT_DEBUG_GRID_ASSERT( _FV_left.m_val.vt == VT_R8 );
						__EXT_DEBUG_GRID_ASSERT( _FV_right.m_val.vt == VT_R8 );
						double lfVal = (double)::pow( _FV_left.m_val.dblVal, _FV_right.m_val.dblVal );
						_FV.m_val._VariantAssign( lfVal, VT_R8 );
						_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
					}
					return CExtFormulaValue::__EFEC_OK;
				case __FORMULA_ATOM_EXPR_STR_CAT:
					{
						_FV_left.m_val._VariantChangeType( VT_BSTR );
						_FV_right.m_val._VariantChangeType( VT_BSTR );
						if( _FV_left.m_val.vt != VT_BSTR )
						{
							if( ppErrorLocationForumaItem != NULL )
								(*ppErrorLocationForumaItem) = pFI_left;
							return CExtFormulaValue::__EFEC_STRING_TYPE_REQUIRED_LEFT;
						}
						if( _FV_right.m_val.vt != VT_BSTR )
						{
							if( ppErrorLocationForumaItem != NULL )
								(*ppErrorLocationForumaItem) = pFI_right;
							return CExtFormulaValue::__EFEC_STRING_TYPE_REQUIRED_RIGHT;
						}
						CExtSafeString strLeft, strRight, strResult;
						_FV_left.m_val.TextGet( strLeft );
						_FV_right.m_val.TextGet( strRight );
						strResult = strLeft + strRight;
						_FV.m_val._VariantAssign( LPCTSTR(strResult) );
						_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
					}
					return CExtFormulaValue::__EFEC_OK;
				case __FORMULA_ATOM_EXPR_LESS:
				case __FORMULA_ATOM_EXPR_LESS_EQUAL:
				case __FORMULA_ATOM_EXPR_GREATER:
				case __FORMULA_ATOM_EXPR_GREATER_EQUAL:
				case __FORMULA_ATOM_EXPR_EQUAL:
				case __FORMULA_ATOM_EXPR_NOT_EQUAL:
				{ // comparison operators
					int nCmpResult = _FV_left.m_val.CompareEx( CExtGridCell::__EGCCT_FORMULA_ATOM_COMPUTATION, _FV_right.m_val );
					bool bVal = false;
					switch( m_eType )
					{
					case __FORMULA_ATOM_EXPR_LESS:
						bVal = ( nCmpResult < 0 ) ? true : false;
					break;
					case __FORMULA_ATOM_EXPR_LESS_EQUAL:
						bVal = ( nCmpResult <= 0 ) ? true : false;
					break;
					case __FORMULA_ATOM_EXPR_GREATER:
						bVal = ( nCmpResult > 0 ) ? true : false;
					break;
					case __FORMULA_ATOM_EXPR_GREATER_EQUAL:
						bVal = ( nCmpResult >= 0 ) ? true : false;
					break;
					case __FORMULA_ATOM_EXPR_EQUAL:
						bVal = ( nCmpResult == 0 ) ? true : false;
					break;
					case __FORMULA_ATOM_EXPR_NOT_EQUAL:
						bVal = ( nCmpResult != 0 ) ? true : false;
					break;
					} // switch( m_eType )
					_FV.m_val._VariantAssign( bVal );
					_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
					return CExtFormulaValue::__EFEC_OK;
				} // comparison operators
				break;
				} // switch( m_eType )
			} // if both left and right are variants
		} // block-end
		if( ppErrorLocationForumaItem != NULL )
			(*ppErrorLocationForumaItem) = this;
		return CExtFormulaValue::__EFEC_INVALID_TYPE_FOR_OPERATION;
	break;
	case __FORMULA_ATOM_EXPR_PERCENT:
	case __FORMULA_ATOM_TERM_UNARY_MINUS:
		__EXT_DEBUG_GRID_ASSERT( GetChildCount() == 1 );
		{ // block-begin
			const CExtFormulaItem * pFI = GetChildAt( 0 );
			err = pFI->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, ppErrorLocationForumaItem );
			if( err != CExtFormulaValue::__EFEC_OK )
			{
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = this;
				return err;
			}
			if( _FV.m_eType == CExtFormulaValue::__EFVT_OLE_VARIANT )
			{
				if( m_eType == __FORMULA_ATOM_EXPR_PERCENT )
				{
					if( ! _FV.m_val._VariantChangeType( VT_R8 ) )
					{
						if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
							(*ppErrorLocationForumaItem) = this;
						return CExtFormulaValue::__EFEC_TYPE_R8_CONVERSION_ERROR;
					}
				}
				if( _FV.m_val.vt == VT_I4 )
				{
					if( m_eType == __FORMULA_ATOM_TERM_UNARY_MINUS )
						_FV.m_val.intVal = - _FV.m_val.intVal;
					else
						_FV.m_val.intVal /= 100;
					break; // success
				}
//				else if( _FV.m_val.vt == VT_UI4 )
//				{
//					if( m_eType == __FORMULA_ATOM_TERM_UNARY_MINUS )
//						_FV.m_val.intVal = - _FV.m_val.intVal;
//					else
//						_FV.m_val.intVal /= 100;
//					break; // success
//				}
				else if( _FV.m_val.vt == VT_I8 )
				{
					signed __int64 * ptr =
						reinterpret_cast
						< signed __int64 * >
						( LPVOID(&(_FV.m_val.lVal)) );
					if( m_eType == __FORMULA_ATOM_TERM_UNARY_MINUS )
						(*ptr) = - (*ptr);
					else
						(*ptr) /= 100;
					break; // success
				}
//				else if( _FV.m_val.vt == VT_UI8 )
//				{
//					unsigned __int64 * ptr =
//						reinterpret_cast
//						< unsigned __int64 * >
//						( LPVOID(&(_FV.m_val.lVal)) );
//					if( m_eType == __FORMULA_ATOM_TERM_UNARY_MINUS )
//						(*ptr) = - (*ptr);
//					else
//						(*ptr) = - (*ptr);
//					break; // success
//				}
				else if( _FV.m_val.vt == VT_R4 )
				{
					if( m_eType == __FORMULA_ATOM_TERM_UNARY_MINUS )
						_FV.m_val.fltVal = - _FV.m_val.fltVal;
					else
						_FV.m_val.fltVal = _FV.m_val.fltVal / float(100.0);
					break; // success
				}
				else if( _FV.m_val.vt == VT_R8 )
				{
					if( m_eType == __FORMULA_ATOM_TERM_UNARY_MINUS )
						_FV.m_val.dblVal = - _FV.m_val.dblVal;
					else
						_FV.m_val.dblVal = _FV.m_val.dblVal / 100.0;
					break; // success
				}
			}
		} // block-end
		if( ppErrorLocationForumaItem != NULL )
			(*ppErrorLocationForumaItem) = this;
		return CExtFormulaValue::__EFEC_INVALID_TYPE_FOR_OPERATION;
	case __FORMULA_ATOM_FUNC_CALL:
		{
			CExtFormulaFunction * pFS = _FDM.OnQueryFormulaFunctionSet().GetFunction( LPCTSTR(m_strName) );
			if( pFS == NULL )
			{
				if( ppErrorLocationForumaItem != NULL )
					(*ppErrorLocationForumaItem) = this;
				return CExtFormulaValue::__EFEC_FUNCTION_IS_NOT_FOUND;
			}
			err = pFS->Exec( _mapCellsAlreadyComputed, _FV, _FDM, this, ppErrorLocationForumaItem );
		}
	break;
	case __FORMULA_ATOM_IDENTIFIER:
		{
			CExtGR2D * pRange = _FDM.NamedRange_Get( LPCTSTR(m_strName) );
			if( pRange != NULL )
			{
				//_FV.m_eType = CExtFormulaValue::__EFVT_CELL_RANGE;
				//_FV.m_gr = (*pRange);
				if( pRange->IsSinglePoint() )
				{
					CPoint pt = pRange->Range( 0 ).TopLeft();
					if( ! _FDM.OnComputeValue( _mapCellsAlreadyComputed, pt.x, pt.y, _FV, this ) )
					{
						err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
						if( ppErrorLocationForumaItem != NULL )
							(*ppErrorLocationForumaItem) = this;
						break;
					}
				}
				else
				{
					_FV.m_eType = CExtFormulaValue::__EFVT_CELL_RANGE;
					_FV.m_gr = (*pRange);
				}
				break;
			}

			CExtFormulaFunction * pFS = _FDM.OnQueryFormulaFunctionSet().GetFunction( LPCTSTR(m_strName) );
			if(		pFS != NULL
				&&	pFS->CanBeIdentifier()
				)
			{ // execute function without parameters as variable
				err = pFS->Exec( _mapCellsAlreadyComputed, _FV, _FDM, this, ppErrorLocationForumaItem );
				break;
			}

			CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
			CExtSafeString strU = LPCTSTR(m_strName);
			strU.MakeUpper();
			__EXT_MFC_SAFE_LPCTSTR strUP = LPCTSTR(strU);
			CPoint pt = _FA.ParseCellName( strUP );
			if( pt.x < 0L || pt.y < 0L )
			{
				err = CExtFormulaValue::__EFEC_UNKNOWN_IDENTIFIER;
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = this;
				break;
			}
			if( ! _FDM.OnComputeValue( _mapCellsAlreadyComputed, pt.x, pt.y, _FV, this ) )
			{
				err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = this;
				break;
			}
		}
	break;
	case __FORMULA_ATOM_CELL_RANGE:
		{
			__EXT_DEBUG_GRID_ASSERT( GetChildCount() == 2 );
			CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
			const CExtFormulaItem * pChild0 = GetChildAt( 0 );
			__EXT_DEBUG_GRID_ASSERT( pChild0 != NULL );
			const CExtFormulaItem * pChild1 = GetChildAt( 1 );
			__EXT_DEBUG_GRID_ASSERT( pChild1 != NULL );
			CExtSafeString strLTU = pChild0->m_strName;
			CExtSafeString strRBU = pChild1->m_strName;
			strLTU.MakeUpper();
			strRBU.MakeUpper();
			__EXT_MFC_SAFE_LPCTSTR strLT = LPCTSTR(strLTU);
			__EXT_MFC_SAFE_LPCTSTR strRB = LPCTSTR(strRBU);
			bool bErrorInLT = false, bErrorInRB = false;
			CRect rcRange = _FA.ParseRangeName( strLT, strRB, &bErrorInLT, &bErrorInRB, &_FDM );
			if( bErrorInLT || bErrorInRB )
			{
				err = CExtFormulaValue::__EFEC_UNKNOWN_IDENTIFIER;
				if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
					(*ppErrorLocationForumaItem) = this;
				break;
			}
			_FV.m_eType = CExtFormulaValue::__EFVT_CELL_RANGE;
			_FV.m_gr = rcRange;
		}
	break;
	case __FORMULA_ATOM_CELL_RANGE_INTERSECTION:
		{
			_FV.m_eType = CExtFormulaValue::__EFVT_CELL_RANGE;
			CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
			INT nIndex, nCount = GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( nCount >= 2 );
			for( nIndex = 0; nIndex < nCount; nIndex ++ )
			{
				const CExtFormulaItem * pChild = GetChildAt( nIndex );
				if( pChild->m_eType == __FORMULA_ATOM_IDENTIFIER )
				{
					LPCTSTR str = pChild->m_strName;

					CExtGR2D * pRange = _FDM.NamedRange_Get( str );
					if( pRange != NULL )
					{
						if( nIndex == 0 )
							_FV.m_gr = (*pRange);
						else
							_FV.m_gr &= (*pRange);
						continue;
					}

					CExtSafeString strU = str;
					strU.MakeUpper();
					__EXT_MFC_SAFE_LPCTSTR strUP = LPCTSTR(strU);
					CPoint pt = _FA.ParseCellName( strUP );
					if( pt.x < 0L || pt.y < 0L )
					{
						err = CExtFormulaValue::__EFEC_UNKNOWN_IDENTIFIER;
						if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
							(*ppErrorLocationForumaItem) = this;
						break;
					}
					if( nIndex == 0 )
						_FV.m_gr = pt;
					else
						_FV.m_gr &= pt;
					continue;
				}
				CExtFormulaValue _FV_child;
				err = pChild->ComputeValue( _mapCellsAlreadyComputed, _FV_child, _FDM, ppErrorLocationForumaItem );
				if( err != CExtFormulaValue::__EFEC_OK )
					break;
				if( _FV_child.m_eType != CExtFormulaValue::__EFVT_CELL_RANGE )
				{
					err = CExtFormulaValue::__EFEC_INVALID_TYPE_FOR_OPERATION;
					if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
						(*ppErrorLocationForumaItem) = pChild;
				}
				if( nIndex == 0 )
					_FV.m_gr = _FV_child.m_gr;
				else
					_FV.m_gr &= _FV_child.m_gr;
			} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
		}
	break;
	case __FORMULA_ATOM_UNKNOWN:
	default:
		//__EXT_DEBUG_GRID_ASSERT( FALSE );
		err = CExtFormulaValue::__EFEC_FUNCTION_IS_NOT_IMPLEMENTED;
		if( ppErrorLocationForumaItem != NULL && (*ppErrorLocationForumaItem) == NULL )
			(*ppErrorLocationForumaItem) = this;
	break;
	} // switch( m_eType )
	return err;
}

bool CExtFormulaItem::ComputeSimpleValue( CExtFormulaValue & _FV ) const
{
	switch( m_eType )
	{
	case __FORMULA_ATOM_CONSTANT_INT:
	{
		_FV.Empty();
		int nVal = _ttoi( LPCTSTR(m_strName) );
		_FV.m_val._VariantAssign( nVal, VT_I4 );
		_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
	}
	return true;
	case __FORMULA_ATOM_CONSTANT_FLOAT:
	{
		_FV.Empty();
		LPTSTR pStopScan = NULL;
		double lfVal = _tcstod( LPCTSTR(m_strName), &pStopScan );
		_FV.m_val._VariantAssign( lfVal, VT_R8 );
		_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
	}
	return true;
//	case __FORMULA_ATOM_CONSTANT_CHAR:
//	return true;
	case __FORMULA_ATOM_CONSTANT_STRING:
	{
		_FV.Empty();
		_FV.m_val._VariantAssign( LPCTSTR(m_strName) );
		_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
	}
	return true;
	default:
		return false;
	} // switch( m_eType )
}

CExtSafeString CExtFormulaItem::FormulaText() const
{
CExtSafeString strFormulaText = _T("");
	if( GetParent() == NULL )
		strFormulaText += _T("=");
	switch( m_eType )
	{
	case __FORMULA_ATOM_IDENTIFIER:
	case __FORMULA_ATOM_CONSTANT_INT:
	case __FORMULA_ATOM_CONSTANT_FLOAT:
		strFormulaText += m_strName;
	break;
//	case __FORMULA_ATOM_CONSTANT_CHAR:
//		strFormulaText += _T("\'");
//		strFormulaText += m_strName;
//		strFormulaText += _T("\'");
//	break;
	case __FORMULA_ATOM_CONSTANT_STRING:
		strFormulaText += _T("\"");
		strFormulaText += LPCTSTR(m_strName);
		strFormulaText += _T("\"");
	break;
	case __FORMULA_ATOM_EXPR_MULTIPLY:
	case __FORMULA_ATOM_EXPR_DIVIDE:
	//case __FORMULA_ATOM_EXPR_MODULE:
	case __FORMULA_ATOM_EXPR_PLUS:
	case __FORMULA_ATOM_EXPR_MINUS:
	case __FORMULA_ATOM_EXPR_LESS:
	case __FORMULA_ATOM_EXPR_GREATER:
	case __FORMULA_ATOM_EXPR_GREATER_EQUAL:
	case __FORMULA_ATOM_EXPR_LESS_EQUAL:
	case __FORMULA_ATOM_EXPR_NOT_EQUAL:
	case __FORMULA_ATOM_EXPR_STR_CAT:
	case __FORMULA_ATOM_EXPR_POW:
	case __FORMULA_ATOM_EXPR_EQUAL:
		__EXT_DEBUG_GRID_ASSERT( GetChildCount() == 2 );
		strFormulaText += _T("(");
		strFormulaText += GetChildAt(0)->FormulaText();
		strFormulaText += m_strName;
		strFormulaText += GetChildAt(1)->FormulaText();
		strFormulaText += _T(")");
	break;
	case __FORMULA_ATOM_EXPR_PERCENT:
		__EXT_DEBUG_GRID_ASSERT( GetChildCount() == 1 );
		strFormulaText += _T("((");
		strFormulaText += GetChildAt(0)->FormulaText();
		strFormulaText += _T(")%)");
	break;
	case __FORMULA_ATOM_TERM_UNARY_MINUS:
		__EXT_DEBUG_GRID_ASSERT( GetChildCount() == 1 );
		strFormulaText += _T("(-(");
		strFormulaText += GetChildAt(0)->FormulaText();
		strFormulaText += _T("))");
	break;
	case __FORMULA_ATOM_FUNC_CALL:
		{
			strFormulaText += m_strName;
			strFormulaText += _T("(");
			INT nIndex, nCount = GetChildCount();
			for( nIndex = 0; nIndex < nCount; nIndex ++ )
			{
				if( nIndex > 0 )
					strFormulaText += _T(",");
				const CExtFormulaItem * pChild = GetChildAt( nIndex );
				strFormulaText += LPCTSTR(pChild->FormulaText());
			}
			strFormulaText += _T(")");
		}
	break;
	case __FORMULA_ATOM_CELL_RANGE:
		__EXT_DEBUG_GRID_ASSERT( GetChildCount() == 2 );
		strFormulaText += GetChildAt( 0 )->FormulaText();
		strFormulaText += _T(":");
		strFormulaText += GetChildAt( 1 )->FormulaText();
	break;
	case __FORMULA_ATOM_CELL_RANGE_INTERSECTION:
		{
			INT nIndex, nCount = GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( nCount >= 2 );
			for( nIndex = 0; nIndex < nCount; nIndex ++ )
			{
				if( nIndex > 0 )
					strFormulaText += _T(" ");
				const CExtFormulaItem * pChild = GetChildAt( nIndex );
				//__EXT_DEBUG_GRID_ASSERT( pChild->m_eType == __FORMULA_ATOM_CELL_RANGE );
				strFormulaText += pChild->FormulaText();
			}
		}
	break;
	case __FORMULA_ATOM_UNKNOWN:
	default:
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		strFormulaText = _T("");
	break;
	} // switch( m_eType )
	return strFormulaText;
}

void CExtFormulaItem::FormatFormulaInplaceEdit( CExtFormulaInplaceEdit & wndFIE )
{
	if( wndFIE.OnQueryFormulaCell().OnFormatFormulaInplaceEdit( this, wndFIE )  )
		return;
	m_bBold = m_bItalic = false;
	m_clr = COLORREF(-1L);
CExtFormulaDataManager & _FDM = wndFIE.OnQueryFDM();
	switch( m_eType )
	{
	case __FORMULA_ATOM_IDENTIFIER:
	{
		INT nRangeCount = INT(_FDM.m_arrRangeRefColors.GetSize());
		if( nRangeCount == 0 )
			return;

		bool bRange = false;
		CExtGR2D * pRange = _FDM.NamedRange_Get( LPCTSTR(m_strName) );
		if( pRange != NULL )
			bRange = true;
		else
		{
			CExtSafeString strU = LPCTSTR(m_strName);
			strU.MakeUpper();
			__EXT_MFC_SAFE_LPCTSTR strUP = LPCTSTR(strU);
			CPoint pt = _FDM.OnQueryFormulaAlphabet().ParseCellName( strUP );
			if( pt.x >= 0 && pt.y >= 0 )
				bRange = true;
		}

		if( bRange )
		{
			INT nColorIndex = _FDM.m_nRangeWalkIndex % nRangeCount;
			m_clr = _FDM.m_arrRangeRefColors[nColorIndex];
			if( m_clr == COLORREF(-1L) )
				return;
			_FDM.m_nRangeWalkIndex ++;
			_FDM.m_listEditedFormulaRanges.AddTail( this );
			wndFIE.OnFormatFormulaCase( m_nScanPosStart, m_nScanPosEnd );
			wndFIE.OnFormatFormulaRange( m_nScanPosStart, m_nScanPosEnd, m_bBold, m_bItalic, m_clr );
		}
	}
	return;
	case __FORMULA_ATOM_CONSTANT_INT:
	case __FORMULA_ATOM_CONSTANT_FLOAT:
//	case __FORMULA_ATOM_CONSTANT_CHAR:
		m_clr = _FDM.m_clrNumericConstant;
		if( m_clr == COLORREF(-1L) )
			return;
		wndFIE.OnFormatFormulaRange( m_nScanPosStart, m_nScanPosEnd, m_bBold, m_bItalic, m_clr );
	return;
	case __FORMULA_ATOM_CONSTANT_STRING:
		m_clr = _FDM.m_clrStringConstant;
		if( m_clr == COLORREF(-1L) )
			return;
		wndFIE.OnFormatFormulaRange( m_nScanPosStart, m_nScanPosEnd, m_bBold, m_bItalic, m_clr );
	return;
	case __FORMULA_ATOM_FUNC_CALL:
	{
		m_clr = _FDM.m_clrFuncCall;
		_FDM.m_listEditedFunctions.AddTail( this );
		CExtFormulaFunctionSet & _FFS = wndFIE.OnQueryFDM().OnQueryFormulaFunctionSet();
		CExtFormulaFunction * pFS = _FFS.GetFunction( LPCTSTR(m_strName) );
		if( pFS == NULL )
		{
			bool bReplace = false;
			CExtSafeString strName = LPCTSTR(m_strName);
			strName.MakeUpper();
			if( strName != m_strName )
			{
				pFS = _FFS.GetFunction( LPCTSTR(strName) );
				if( pFS != NULL )
					bReplace = true;
			} // if( strName != m_strName )
			if( ! bReplace  )
			{
				strName.MakeLower();
				if( strName != m_strName )
				{
					pFS = _FFS.GetFunction( LPCTSTR(strName) );
					if( pFS != NULL )
						bReplace = true;
				} // if( strName != m_strName )
			} // if( ! bReplace  )
			if( bReplace )
			{
				wndFIE.Rich_SetSel( m_nScanPosStart, m_nScanPosEnd );
				wndFIE.ReplaceSel( LPCTSTR(strName), FALSE );
			} // 	if( bReplace )
		}
		if( m_clr == COLORREF(-1L) )
			break;
		wndFIE.OnFormatFormulaCase( m_nScanPosStart, m_nScanPosEnd );
		wndFIE.OnFormatFormulaRange( m_nScanPosStart, m_nScanPosEnd, m_bBold, m_bItalic, m_clr );
	}
	break;
	case __FORMULA_ATOM_CELL_RANGE:
	{
		INT nRangeCount = INT(_FDM.m_arrRangeRefColors.GetSize());
		if( nRangeCount == 0 )
			return;
		INT nColorIndex = _FDM.m_nRangeWalkIndex % nRangeCount;
		m_clr = _FDM.m_arrRangeRefColors[nColorIndex];
		if( m_clr == COLORREF(-1L) )
			return;
		_FDM.m_nRangeWalkIndex ++;
		_FDM.m_listEditedFormulaRanges.AddTail( this );
		__EXT_DEBUG_GRID_ASSERT( GetChildCount() == 2 );
		const CExtFormulaItem * pChild0 = GetChildAt( 0 );
		__EXT_DEBUG_GRID_ASSERT( pChild0 != NULL );
		const CExtFormulaItem * pChild1 = GetChildAt( 1 );
		__EXT_DEBUG_GRID_ASSERT( pChild1 != NULL );
		wndFIE.OnFormatFormulaCase( pChild0->m_nScanPosStart, pChild0->m_nScanPosEnd );
		wndFIE.OnFormatFormulaCase( pChild1->m_nScanPosStart, pChild1->m_nScanPosEnd );
		wndFIE.OnFormatFormulaRange( pChild0->m_nScanPosStart, pChild0->m_nScanPosEnd, m_bBold, m_bItalic, m_clr );
		wndFIE.OnFormatFormulaRange( pChild1->m_nScanPosStart, pChild1->m_nScanPosEnd, m_bBold, m_bItalic, m_clr );
	}
	return;
//	case __FORMULA_ATOM_CELL_RANGE_INTERSECTION:
//	{
//		INT nRangeCount = INT(_FDM.m_arrRangeRefColors.GetSize());
//		if( nRangeCount == 0 )
//			return;
///		INT nColorIndex = _FDM.m_nRangeWalkIndex % nRangeCount;
//		m_clr = _FDM.m_arrRangeRefColors[nColorIndex];
//		if( m_clr == COLORREF(-1L) )
//			return;
//		_FDM.m_nRangeWalkIndex ++;
//		_FDM.m_listEditedFormulaRanges.AddTail( this );
//		INT nIndex, nCount = GetChildCount();
//		for( nIndex = 0; nIndex < nCount; nIndex ++ )
//		{
//			CExtFormulaItem * pChild = GetChildAt( nIndex );
//			wndFIE.OnFormatFormulaCase( pChild->m_nScanPosStart, pChild->m_nScanPosEnd );
//			wndFIE.OnFormatFormulaRange( pChild->m_nScanPosStart, pChild->m_nScanPosEnd, m_bBold, m_bItalic, m_clr );
//		}
//	}
//	return;
	} // switch( m_eType )

INT nIndex, nCount = GetChildCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtFormulaItem * pChild = GetChildAt( nIndex );
		pChild->FormatFormulaInplaceEdit( wndFIE );
	}
}

void CExtFormulaItem::UnionScanPos( INT & nScanPosStart, INT & nScanPosEnd ) const
{
	nScanPosStart = m_nScanPosStart;
	nScanPosEnd = m_nScanPosEnd;
INT nCount = GetChildCount();
	if( nCount > 0 )
	{
		const CExtFormulaItem * pChild = NULL;
		for( pChild = GetChildAt( 0 ) ; pChild != NULL; )
		{
			nScanPosStart = min( nScanPosStart, pChild->m_nScanPosStart );
			INT nCount2 = pChild->GetChildCount();
			if( nCount2 == 0 )
				pChild = NULL;
			else
				pChild = pChild->GetChildAt( 0 );
		}
		for( pChild = GetChildAt( nCount - 1 ) ; pChild != NULL; )
		{
			nScanPosEnd = max( nScanPosEnd, pChild->m_nScanPosEnd );
			INT nCount2 = pChild->GetChildCount();
			if( nCount2 == 0 )
				pChild = NULL;
			else
				pChild = pChild->GetChildAt( nCount2 - 1 );
		}
	}
}

void CExtFormulaItem::ShiftRangesInText(
	CExtFormulaDataManager & _FDM,
	CExtSafeString & strFormulaText,
	LONG nShiftX,
	LONG nShiftY,
	const CExtGR2D * pRangeToShift // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT( nShiftX != 0 || nShiftY != 0 );
	switch( m_eType )
	{
	case __FORMULA_ATOM_IDENTIFIER:
	{
		CExtGR2D * pRange = _FDM.NamedRange_Get( LPCTSTR(m_strName) );
		if( pRange != NULL )
			return; // not shift-able

		CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
		CExtSafeString strU = m_strName;
		strU.MakeUpper();
		__EXT_MFC_SAFE_LPCTSTR strUP = LPCTSTR(strU);
		CPoint pt = _FA.ParseCellName( strUP );
		if( pt.x >= 0 && pt.y >= 0 )
		{
			if(		pRangeToShift != NULL
				&&	(! pRangeToShift->PtInRegion( pt ) )
				)
				return;
			pt.x += nShiftX;
			pt.y += nShiftY;
			CExtSafeString strInsText = _FA.GetLocationStr( pt.x, pt.y );
			INT nInsTextLen = strInsText.GetLength();
			if( nInsTextLen > 0 )
			{
				INT nOldTextLen = INT( strFormulaText.GetLength() );
				CExtSafeString strLeft = strFormulaText.Left( m_nScanPosStart );
				CString strRight = strFormulaText.Right( nOldTextLen -  m_nScanPosEnd );
				strFormulaText  = LPCTSTR( strLeft );
				strFormulaText += LPCTSTR( strInsText );
				strFormulaText += LPCTSTR( strRight );
			}
		}
	}
	return;
	} // switch( m_eType )
INT nIndex, nCount = GetChildCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtFormulaItem * pChild = GetChildAt( nCount - nIndex - 1 ); // walk from back to forward
		pChild->ShiftRangesInText( _FDM, strFormulaText, nShiftX, nShiftY, pRangeToShift );
	}
}

CExtFormulaItem * CExtFormulaItem::FindFunction( INT nScanPos, INT & nParameterIndex )
{
INT nScanPosStart = -1, nScanPosEnd = -1;
	UnionScanPos( nScanPosStart, nScanPosEnd );
	if( ! ( nScanPosStart <= nScanPos && nScanPos <= nScanPosEnd ) )
		return NULL;
INT nIndex, nCount = GetChildCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtFormulaItem * pChild = GetChildAt( nIndex );
		CExtFormulaItem * pFI = pChild->FindFunction( nScanPos, nParameterIndex );
		if( pFI != NULL )
			return pFI;
	}
	if( m_eType != __FORMULA_ATOM_FUNC_CALL )
		return NULL;
	nParameterIndex = 0;
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtFormulaItem * pChild = GetChildAt( nIndex );
		INT nChildScanPosStart = -1, nChildScanPosEnd = -1;
		pChild->UnionScanPos( nChildScanPosStart, nChildScanPosEnd );
		if( nChildScanPosStart <= nScanPos && nScanPos <= nChildScanPosEnd )
		{
			nParameterIndex = nIndex + 1;
			break;
		}
		if( nScanPos > nChildScanPosStart )
			nParameterIndex = nIndex + 1;
	}
	return this;
}

const CExtFormulaItem * CExtFormulaItem::FindFunction( INT nScanPos, INT & nParameterIndex ) const
{
	return ( const_cast < CExtFormulaItem * > ( this ) ) -> FindFunction( nScanPos, nParameterIndex );
}


CExtFormulaItem * CExtFormulaItem::FindItem( INT nScanPosStart, INT nScanPosEnd )
{
	if( nScanPosStart < 0 && nScanPosEnd < 0 )
		return NULL;
	if( m_nScanPosStart < 0 && m_nScanPosEnd < 0 )
		return NULL;
	if( nScanPosStart >= 0 && nScanPosStart == m_nScanPosStart )
	{
		if( nScanPosEnd < 0 )
			return this;
		if( nScanPosEnd >= 0 && nScanPosEnd == m_nScanPosEnd )
			return this;
	}
	else if( nScanPosStart < 0 )
	{
		if( nScanPosEnd >= 0 && nScanPosEnd == m_nScanPosEnd )
			return this;
	}
INT nIndex, nCount = GetChildCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtFormulaItem * pChild = GetChildAt( nIndex );
		CExtFormulaItem * pFI = pChild->FindItem( nScanPosStart, nScanPosEnd );
		if( pFI != NULL )
			return pFI;
	}
	return NULL;
}

const CExtFormulaItem * CExtFormulaItem::FindItem( INT nScanPosStart, INT nScanPosEnd ) const
{
	return ( const_cast < CExtFormulaItem * > ( this ) ) -> FindItem( nScanPosStart, nScanPosEnd );
}

CExtFormulaItem * CExtFormulaItem::FindRangeItem( INT nScanPosStart, INT nScanPosEnd )
{
	__EXT_DEBUG_GRID_ASSERT( nScanPosStart <= nScanPosEnd );
	if( m_rcEditedRange.left >= 0 && m_rcEditedRange.right >= 0 && m_rcEditedRange.top >= 0 && m_rcEditedRange.bottom >= 0 )
	{
		if( nScanPosStart != nScanPosEnd )
		{
			__EXT_DEBUG_GRID_ASSERT( nScanPosStart < nScanPosEnd );
			if( nScanPosStart == m_nScanPosStart && nScanPosEnd == m_nScanPosEnd )
				return this;
		} // if( nScanPosStart != nScanPosEnd )
		else
		{
			if( nScanPosEnd == m_nScanPosEnd )
				return this;
		} // else from if( nScanPosStart != nScanPosEnd )
	} // if( m_rcEditedRange.left >= 0 && m_rcEditedRange.right >= 0 && m_rcEditedRange.top >= 0 && m_rcEditedRange.bottom >= 0 )
INT nIndex, nCount = GetChildCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtFormulaItem * pChild = GetChildAt( nIndex );
		CExtFormulaItem * pFI = pChild->FindRangeItem( nScanPosStart, nScanPosEnd );
		if( pFI != NULL )
			return pFI;
	}
	return NULL;
}

const CExtFormulaItem * CExtFormulaItem::FindRangeItem( INT nScanPosStart, INT nScanPosEnd ) const
{
	return ( const_cast < CExtFormulaItem * > ( this ) ) -> FindRangeItem( nScanPosStart, nScanPosEnd );
}

void CExtFormulaItem::UnionInplaceHighlightedRange( CExtFormulaDataManager & _FDM, CExtGR2D & _range ) const
{
	switch( m_eType )
	{
	case __FORMULA_ATOM_IDENTIFIER:
	{
		CExtGR2D * pRange = _FDM.NamedRange_Get( LPCTSTR(m_strName) );
		if( pRange != NULL )
		{
			_range += (*pRange);
			// m_rcEditedRange and m_rcEditedRangeNN are not used for named ranges
			m_rangeEditedNamed = (*pRange);
			return;
		}

		CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
		CExtSafeString strU = m_strName;
		strU.MakeUpper();
		__EXT_MFC_SAFE_LPCTSTR strUP = LPCTSTR(strU);
		CPoint pt = _FA.ParseCellName( strUP );
		if( pt.x >= 0 && pt.y >= 0 )
			_range += pt;
		m_rcEditedRange.SetRect( pt, pt );
		m_rcEditedRangeNN = m_rcEditedRange;
	}
	return;
	case __FORMULA_ATOM_CELL_RANGE:
	{
		CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
		__EXT_DEBUG_GRID_ASSERT( GetChildCount() == 2 );
		const CExtFormulaItem * pChild0 = GetChildAt( 0 );
		__EXT_DEBUG_GRID_ASSERT( pChild0 != NULL );
		const CExtFormulaItem * pChild1 = GetChildAt( 1 );
		CExtSafeString strLTU = pChild0->m_strName;
		CExtSafeString strRBU = pChild1->m_strName;
		strLTU.MakeUpper();
		strRBU.MakeUpper();
		__EXT_MFC_SAFE_LPCTSTR strLT = LPCTSTR(strLTU);
		__EXT_MFC_SAFE_LPCTSTR strRB = LPCTSTR(strRBU);
		bool bErrorInLT = false, bErrorInRB = false;
		CRect rcRange = _FA.ParseRangeName( strLT, strRB, &bErrorInLT, &bErrorInRB, &_FDM );
		if( bErrorInLT || bErrorInRB )
			break;
		m_rcEditedRangeNN = rcRange;
		m_rcEditedRange.SetRect(
			min( rcRange.left, rcRange.right ),
			min( rcRange.top,  rcRange.bottom ),
			max( rcRange.left, rcRange.right ),
			max( rcRange.top,  rcRange.bottom )
			);
		_range += m_rcEditedRange;
	}
	return;
//	case __FORMULA_ATOM_CELL_RANGE_INTERSECTION:
//	break;
	} // switch( m_eType )
INT nIndex, nCount = GetChildCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		const CExtFormulaItem * pChild = GetChildAt( nIndex );
		pChild->UnionInplaceHighlightedRange( _FDM, _range );
	}
}

void CExtFormulaItem::ComputeDependencySourceRange(
	CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
	CExtFormulaDataManager & _FDM,
	CExtGR2D & _range
	) const
{
	switch( m_eType )
	{
	case __FORMULA_ATOM_IDENTIFIER:
		{
			CExtGR2D * pRange = _FDM.NamedRange_Get( LPCTSTR(m_strName) );
			if( pRange != NULL )
			{
				_range += (*pRange);
				break;
			}

			CExtFormulaFunction * pFS = _FDM.OnQueryFormulaFunctionSet().GetFunction( LPCTSTR(m_strName) );
			if(		pFS != NULL
				&&	pFS->CanBeIdentifier()
				)
				break;
			CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();

			CExtSafeString strU = LPCTSTR(m_strName);
			strU.MakeUpper();
			__EXT_MFC_SAFE_LPCTSTR strUP = LPCTSTR(strU);
			CPoint pt = _FA.ParseCellName( strUP );
			if( pt.x < 0L || pt.y < 0L )
				break;
			_range += pt;
		}
	break;
	case __FORMULA_ATOM_CELL_RANGE:
		{
			__EXT_DEBUG_GRID_ASSERT( GetChildCount() == 2 );
			CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
			const CExtFormulaItem * pChild0 = GetChildAt( 0 );
			__EXT_DEBUG_GRID_ASSERT( pChild0 != NULL );
			const CExtFormulaItem * pChild1 = GetChildAt( 1 );
			__EXT_DEBUG_GRID_ASSERT( pChild1 != NULL );
			CExtSafeString strLTU = pChild0->m_strName;
			CExtSafeString strRBU = pChild1->m_strName;
			strLTU.MakeUpper();
			strRBU.MakeUpper();
			__EXT_MFC_SAFE_LPCTSTR strLT = LPCTSTR(strLTU);
			__EXT_MFC_SAFE_LPCTSTR strRB = LPCTSTR(strRBU);
			bool bErrorInLT = false, bErrorInRB = false;
			CRect rcRange = _FA.ParseRangeName( strLT, strRB, &bErrorInLT, &bErrorInRB, &_FDM );
			if( bErrorInLT || bErrorInRB )
				break;
			_range += rcRange;
		}
	break;
	case __FORMULA_ATOM_CELL_RANGE_INTERSECTION:
		{
			CExtGR2D _rangeI;
			CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
			INT nIndex, nCount = GetChildCount();
			__EXT_DEBUG_GRID_ASSERT( nCount >= 2 );
			for( nIndex = 0; nIndex < nCount; nIndex ++ )
			{
				const CExtFormulaItem * pChild = GetChildAt( nIndex );
				if( pChild->m_eType == __FORMULA_ATOM_IDENTIFIER )
				{
					LPCTSTR str = LPCTSTR(pChild->m_strName);

					CExtGR2D * pRange = _FDM.NamedRange_Get( str );
					if( pRange != NULL )
					{
						if( nIndex == 0 )
							_rangeI = (*pRange);
						else
							_rangeI &= (*pRange);
						continue;
					}

					CExtSafeString strU = str;
					strU.MakeUpper();
					__EXT_MFC_SAFE_LPCTSTR strUP = LPCTSTR(strU);
					CPoint pt = _FA.ParseCellName( strUP );
					if( pt.x < 0L || pt.y < 0L )
						break;
					if( nIndex == 0 )
						_rangeI = pt;
					else
						_rangeI &= pt;
					continue;
				}
				CExtFormulaValue _FV_child;
				CExtFormulaValue::e_formula_error_code_t err = pChild->ComputeValue( _mapCellsAlreadyComputed, _FV_child, _FDM, NULL );
				if( err != CExtFormulaValue::__EFEC_OK )
					break;
				if( _FV_child.m_eType != CExtFormulaValue::__EFVT_CELL_RANGE )
					break;
				if( nIndex == 0 )
					_rangeI = _FV_child.m_gr;
				else
					_rangeI &= _FV_child.m_gr;
			} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
			_range += _rangeI;
		}
	break;
	default:
		{
			INT nIndex, nCount = GetChildCount();
			for( nIndex = 0; nIndex < nCount; nIndex ++ )
			{
				const CExtFormulaItem * pChild = GetChildAt( nIndex );
				pChild->ComputeDependencySourceRange( _mapCellsAlreadyComputed, _FDM, _range );
			}
		}
	break;
	} // switch( m_eType )
}

void CExtFormulaItem::RemoveFromAI( CExtHashMapT < CExtFormulaItem *, CExtFormulaItem *, bool, bool > & _mapAI )
{
	_mapAI.RemoveKey( this );
INT nIndex, nCount = GetChildCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtFormulaItem * pChild = GetChildAt( nIndex );
		pChild->RemoveFromAI( _mapAI );
	}
}

//////////////////////////////////////////////////////////////
// class CExtFormulaItem

const int CExtFormulaParser::g_ebp_gap = 13;

CExtFormulaParser::CExtFormulaParser(
	__EXT_MFC_SAFE_LPTSTR pExternalBuffer, // = NULL
	INT nExternalBufferLen // = 0
	)
	: m_pExternalBuffer( LPTSTR(pExternalBuffer) )
	, m_nExternalBufferLen( ( pExternalBuffer != NULL ) ? ( ( nExternalBufferLen > 0 ) ? nExternalBufferLen : INT( _tcslen( LPTSTR(pExternalBuffer) ) ) ) : 0 )
	, m_nExternalBufferPos( 0 )
	, m_bParserIsInTheErrorState( false )
	, m_pFormulaRoot( NULL )
	, m_ptTextPos( 0, 0 )
	, m_nScanPosStart( 0 )
	, m_nScanPosEnd( 0 )
	, m_ptScanPosStart( 0, 0 )
	, m_ptScanPosEnd( 0, 0 )
{
	m_nParserLexCounter = 0;
	m_pParserStackA = NULL;
	m_pParserStackB = NULL;
	yyss = NULL;
	yysslim = NULL;
	yyvs = NULL;
	yyssp = NULL;
	yyvsp = NULL;
	yyval = NULL;
	yylval = NULL;
	yystacksize = 0;
}

CExtFormulaParser::~CExtFormulaParser()
{
	if( m_pFormulaRoot != NULL )
	{
		m_pFormulaRoot->RemoveFromAI( m_mapAI );
		delete m_pFormulaRoot;
		m_pFormulaRoot = NULL;
	}

POSITION pos = m_mapAI.GetStartPosition();
	for( ; pos != NULL; )
	{
		CExtFormulaItem * pFI = NULL;
		bool bTmp = false;
		m_mapAI.GetNextAssoc( pos, pFI, bTmp );
		__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
		pFI->DetachAllChildren();
		delete pFI;
	}
	m_mapAI.RemoveAll();

	if( m_pParserStackA != NULL )
	{
		::free( m_pParserStackA );
		m_pParserStackA = NULL;
	}
	if( m_pParserStackB != NULL )
	{
		::free( m_pParserStackB );
		m_pParserStackB = NULL;
	}
}

void CExtFormulaParser::Init(
	__EXT_MFC_SAFE_LPCTSTR pExternalBuffer, // = NULL
	INT nExternalBufferLen // = 0
	)
{
	m_pExternalBuffer = pExternalBuffer;
	m_nExternalBufferLen = ( pExternalBuffer != NULL ) ? ( ( nExternalBufferLen > 0 ) ? nExternalBufferLen : INT( _tcslen( pExternalBuffer ) ) ) : 0;
	m_nExternalBufferPos = 0;

	if( m_pFormulaRoot != NULL )
	{
		m_pFormulaRoot->RemoveFromAI( m_mapAI );
		delete m_pFormulaRoot;
		m_pFormulaRoot = NULL;
	}

POSITION pos = m_mapAI.GetStartPosition();
	for( ; pos != NULL; )
	{
		CExtFormulaItem * pFI = NULL;
		bool bTmp = false;
		m_mapAI.GetNextAssoc( pos, pFI, bTmp );
		__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
		pFI->DetachAllChildren();
		delete pFI;
	}
	m_mapAI.RemoveAll();

	m_ptTextPos.x = m_ptTextPos.y = 0;
	m_nScanPosStart = 0;
	m_nScanPosEnd = 0;
	m_ptScanPosStart.x = m_ptScanPosStart.y = 0;
	m_ptScanPosEnd.x = m_ptScanPosEnd.y = 0;
}

void CExtFormulaParser::SaveScanPos( bool bStart, bool bEnd )
{
	if( bStart )
	{
		m_ptScanPosStart = m_ptTextPos;
		m_nScanPosStart = m_nExternalBufferPos;
	}
	if( bEnd )
	{
		m_ptScanPosEnd = m_ptTextPos;
		m_nScanPosEnd = m_nExternalBufferPos;
	}
}

bool CExtFormulaParser::IsEOF() const
{
	if( m_pExternalBuffer == NULL || m_nExternalBufferLen <= 0 || m_nExternalBufferPos >= (m_nExternalBufferLen+g_ebp_gap) )
		return true;
	else
		return false;
}

INT CExtFormulaParser::PositionGet() const
{
	return m_nExternalBufferPos;
}

void CExtFormulaParser::PositionSet( INT nPosition )
{
	if( m_nExternalBufferPos == nPosition )
		return;
	if( nPosition < 0 || nPosition > (m_nExternalBufferLen+g_ebp_gap) )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return;
	}
	for( ; nPosition != m_nExternalBufferPos; )
	{
		if( nPosition > m_nExternalBufferPos )
			ReadChar();
		else
			UnReadChar();
	}
}

__EXT_MFC_SAFE_TCHAR CExtFormulaParser::ReadChar()
{
	if( IsEOF() )
		return _T('\0');
TCHAR _chr = ( m_nExternalBufferPos < m_nExternalBufferLen ) ? m_pExternalBuffer[ m_nExternalBufferPos ] : _T(' ');
	m_nExternalBufferPos ++;
	if( _chr == _T('\n') )
	{
		m_ptTextPos.x = 0;
		m_ptTextPos.y ++;
	}
	else
		m_ptTextPos.x ++;
	return _chr;
}

__EXT_MFC_SAFE_TCHAR CExtFormulaParser::UnReadChar()
{
	if( m_nExternalBufferPos == 0 )
		return _T('\0');
	m_nExternalBufferPos --;
	__EXT_DEBUG_GRID_ASSERT( m_nExternalBufferPos >= 0 );
TCHAR _chr = ( m_nExternalBufferPos < m_nExternalBufferLen ) ? m_pExternalBuffer[ m_nExternalBufferPos ] : _T(' ');
	if( _chr == _T('\n') )
	{
		m_ptTextPos.y --;
		m_ptTextPos.x = 0;
		INT nCheckPos = m_nExternalBufferPos;
		for( ; nCheckPos >= 0; )
		{
			_chr = ( nCheckPos < m_nExternalBufferLen ) ? m_pExternalBuffer[ nCheckPos ] : _T(' ');
			if( _chr == _T('\n') )
				break;
			if( nCheckPos == 0 )
				break;
			nCheckPos --;
			m_ptTextPos.x ++;
		}
	}
	else
		m_ptTextPos.x --;
	return _chr;
}

INT CExtFormulaParser::Lex()
{
	if( m_bParserIsInTheErrorState )
		return 0;
	yylval = NULL;
INT nRetVal = ReadBasicLexem();
	if( nRetVal == 0 )
	{
		if( yylval != NULL )
		{
			yylval->RemoveFromAI( m_mapAI );
			delete yylval;
			yylval = NULL;
		}
		return nRetVal;
	}
	SaveScanPos( false, true );
INT nCount = m_strToken.GetLength();
	yylval = new CExtFormulaItem;
	m_mapAI.SetAt( yylval, false );
	yylval->m_nScanPosStart = m_nScanPosStart;
	yylval->m_nScanPosEnd = m_nScanPosEnd;
	yylval->m_ptScanPosStart = m_ptScanPosStart;
	yylval->m_ptScanPosEnd = m_ptScanPosEnd;
	yylval->m_strName = m_strToken;
	if(		nRetVal == formula_constant_string
//		||	nRetValretVal == formula_constant_char
		)
	{
		__EXT_DEBUG_GRID_ASSERT( yylval != NULL );
		LPTSTR p = LPTSTR(LPCTSTR(yylval->m_strName));
		if( nCount > 2 )
		{
			for( INT i=0; i< ( nCount - 2 ); i++, p++ )
				*p = *(p+1);
			*p = _T('\0');
		}
		else
		{
			if(		(! _tcscmp(LPCTSTR(yylval->m_strName), _T("\"\"") ) )
				||	(! _tcscmp(LPCTSTR(yylval->m_strName), _T("\'\'") ) )
				)
				*p = _T('\0');
		}
	}
	m_nParserLexCounter ++;
	return nRetVal;
}

INT CExtFormulaParser::ReadBasicLexem()
{
INT nPos;
TCHAR _tchr, _tchrHelper;
	if( IsEOF() )
		return 0;
	// pass empty space
passEmptySpace:
	SaveScanPos( true, true );
bool bSpace = false;
	for(	_tchr = ReadChar();
				( _tchr == _T('\t') || _tchr == _T('\r') || _tchr == _T('\n') || _tchr == _T(' ') )
			&&	( ! IsEOF() )
			;
			_tchr = ReadChar()
		)
	{
		if( m_nParserLexCounter > 0 )
		{
			SaveScanPos( false, true );
			bSpace = true;
		}
		else
			SaveScanPos( true, true );
	}
	if( IsEOF() )
		return 0;
	nPos = PositionGet() - 1;
	if( bSpace )
	{
		if( _tchr == _T(':') )
		{
			if( ! IsEOF() )
			{
				for(	_tchr = ReadChar();
							( _tchr == _T('\t') || _tchr == _T('\r') || _tchr == _T('\n') || _tchr == _T(' ') )
						&&	( ! IsEOF() )
						;
						_tchr = ReadChar()
					)
				{
				}
			}
			nPos = PositionGet() - 1;
			PositionSet( nPos );
			return _T(':');
		}
		else
		{
			PositionSet( nPos );
			return _T(' ');
		}
	}
	// detect and pass comments
	if( _tchr == _T('/') )
	{
		if( IsEOF() )
		{
			PositionSet( nPos );
			OnError( _T("unexpected EOF") );
			return 0;
		}
		_tchr = ReadChar();
		switch(_tchr)
		{
		case _T('/'): //  - single line comment
		{	// C++ like comment to EOL, pass it
			for( ; ! IsEOF(); _tchr = ReadChar() )
			{
				if( _tchr == _T('\n') )
					goto passEmptySpace;
			}
			goto passEmptySpace;
			//return 0; // comment at EOF
		}
		break; // '/' - single line comment
		case _T('*'): // - multiline comment
		{	// C like comment from DIV MUL to MUL DIV, pass it
			if( IsEOF() )
			{
				PositionSet( nPos );
				OnError( _T("unexpected EOF") );
				return 0;
			}
			for( _tchr = ReadChar(); ! IsEOF(); _tchr = ReadChar() )
			{
				if( _tchr == _T('*') )
				{
					_tchr = ReadChar();
					if( _tchr == _T('/') )
						goto passEmptySpace;;
					PositionSet( PositionGet() - 1 );
				}
			}
			OnError( _T("unexpected EOF") );
			return 0;
		}
		break; // '*' - multiline comment
		default: // - not a comment
			_tchr = _T('/'); // <-- return to previous state
			PositionSet( PositionGet() - 1 );
		break; // default - not a comment
		} // switch(_tchr)
	} // detect and pass comments
	// return to start position
	PositionSet( nPos );
	// prepare m_strToken
	m_strToken.Empty();
	if( isalpha( _tchr ) || _tchr == _T('_') || _tchr == _T('$') )
	{
#ifdef _DEBUG
		INT iPosBefore = PositionGet();
		iPosBefore;
#endif // _DEBUG
		// scan identifier and return it
		ReadIdentifier();
#ifdef _DEBUG
		INT iPosAfter = PositionGet();
		iPosAfter;
#endif // _DEBUG
__EXT_DEBUG_GRID_ASSERT( m_strToken.GetLength() > 0 );
__EXT_DEBUG_GRID_ASSERT( iPosAfter > iPosBefore );
//		if( lex_subst_pp(iPosBefore,iPosAfter) )
//			goto passEmptySpace;
//		// try to find keyword
//		INT tokenNo = lex_query_token();
//		if( tokenNo != 0 )
//			return tokenNo;	// keyword
		return formula_identifier;
	}

	// return to start position
	PositionSet( nPos );

	if( isdigit( _tchr ) || _tchr == _T('.') )
	{	// try to find numeric(oct,hex,decimal),
		// float, double, long double
		if( _tchr != _T('.') )
		{
			if( ReadConstantIntOct() )
				return formula_constant_int;
			if( ReadConstantIntDecimal() )
				return formula_constant_int;
			if(ReadConstantIntHex())
				return formula_constant_int;
		}
		if( ReadConstantFloat() )
			return formula_constant_float;
	}
//	// return to start position+1
//	PositionSet( nPos + 1 );
//	if( _tchr == _T('\'') )
//	{	// try io find character constant (returned as numeric)
//		PositionSet( PositionGet() - 1 );
//		if( ReadConstantChar() )
//		{
//			__EXT_DEBUG_GRID_ASSERT( m_strToken.GetLength() >= 2 );
//			return formula_constant_char;
//		}
//	}
	// return to start position+1
	PositionSet( nPos + 1 );
	if( _tchr == _T('\"') )
	{	// try to find string constant
		PositionSet( PositionGet() - 1 );
		if(ReadConstantString())
		{
			__EXT_DEBUG_GRID_ASSERT( m_strToken.GetLength() >= 2 );
			return formula_constant_string;
		}
	}
	if(	_tchr != _T('+') && _tchr != _T('-') && _tchr != _T('*') && _tchr != _T('/') && _tchr != _T('%') &&
		_tchr != _T('&') && _tchr != _T('|') && _tchr != _T('!') && _tchr != _T('~') && _tchr != _T('^') &&
		_tchr != _T('=') && _tchr != _T('>') && _tchr != _T('<') && _tchr != _T(':') && _tchr != _T('.')
		)
	{
		// return letter as-is
		PositionSet( nPos + 1 );
		m_strToken += _tchr;
		return _tchr;
	}
	// because all the detected in this place operators have more than 2 characters -> read next character
	// return to start position+1
	PositionSet( nPos + 1 );
	m_strToken += _tchr;
	if( IsEOF() )
	{
		PositionSet( nPos );
		OnError( _T("unexpected EOF") );
		return 0;
	}
	_tchrHelper = ReadChar();
	if( IsEOF() )
	{
		PositionSet( nPos );
		OnError( _T("unexpected EOF") );
		return 0;
	}
// 		if( _tchr == _T('&') )
// 		{	// try to find formula_operator_and_equal &= or formula_operator_double_ampersand &&
// 	// 		if( _tchrHelper == _T('=') )
// 	// 		{
// 	// 			m_strToken = _tchrHelper;
// 	// 			return formula_operator_and_equal;
// 	// 		}
// 			if( _tchrHelper == _T('&') )
// 			{
// 				m_strToken = _tchrHelper;
// 				return formula_operator_double_ampersand;
// 			}
// 		}
// 		if( _tchr == _T('|') )
// 		{	// try to find formula_operator_or_equal |= or formula_operator_double_vertical_bar ||
// 	// 		if( _tchrHelper == _T('=') )
// 	// 		{
// 	// 			m_strToken = _tchrHelper;
// 	// 			return formula_operator_or_equal;
// 	// 		}
// 			if( _tchrHelper == _T('|') )
// 			{
// 				m_strToken = _tchrHelper;
// 				return formula_operator_double_vertical_bar;
// 			}
// 		}
// 	if( _tchr == _T('+') )
// 	{	// try to find formula_operator_plus_equal += or formula_operator_double_plus ++
// 		if( _tchrHelper == _T('=') )
// 		{
// 			m_strToken = _tchrHelper;
// 			return formula_operator_plus_equal;
// 		}
// 		if( _tchrHelper == _T('+') )
// 		{
// 			m_strToken = _tchrHelper;
// 			return formula_operator_double_plus;
// 		}
// 	}
// 	if( _tchr == _T('-') )
// 	{	// try to find formula_operator_minus_equal -= or formula_operator_double_minus -- or POINTER ->
// 		if( _tchrHelper == _T('=') )
// 		{
// 			m_strToken = _tchrHelper;
// 			return formula_operator_minus_equal;
// 		}
// 		if( _tchrHelper == _T('-') )
// 		{
// 			m_strToken = _tchrHelper;
// 			return formula_operator_double_minus;
// 		}
// 	}
// 	if( _tchr == _T('*') )
// 	{	// try to find formula_operator_times_equal *=
// 		if( _tchrHelper == _T('=') )
// 		{
// 			m_strToken = _tchrHelper;
// 			return formula_operator_times_equal;
// 		}
// 	}
// 	if( _tchr == _T('/') )
// 	{	// try to find formula_operator_divide_equal /=
// 		if( _tchrHelper == _T('=') )
// 		{
// 			m_strToken = _tchrHelper;
// 			return formula_operator_divide_equal;
// 		}
// 	}
// 	if( _tchr == _T('%') )
// 	{	// try to find formula_operator_mod_equal %=
// 		if( _tchrHelper == _T('=') )
// 		{
// 			m_strToken = _tchrHelper;
// 			return formula_operator_mod_equal;
// 		}
// 	}
// 	if( _tchr == _T('=') )
// 	{	// try to find formula_operator_double_equal ==
// 		if( _tchrHelper == _T('=') )
// 		{
// 			m_strToken = _tchrHelper;
// 			return formula_operator_double_equal;
// 		}
// 	}
	if( _tchr == _T('>') )
	{	// try to find
		//   formula_operator_left_shift_equal <<= or
		//   formula_operator_double_left_angle << or
		//   formula_operator_less_equal <=
		if( _tchrHelper == _T('=') )
		{
			m_strToken += _tchrHelper;
			return formula_operator_greater_equal;
		}
// 			if( _tchrHelper == _T('>') )
// 			{
// 				m_strToken = _tchrHelper;
// 	// 			_tchrHelper = ReadChar();
// 	// 			if( IsEOF() )
// 	// 			{
// 	// 				PositionSet( nPos );
// 	// 				OnError( _T("unexpected EOF") );
// 	// 				return 0;
// 	// 			}
// 	// 			if( _tchrHelper == _T('=') )
// 	// 			{
// 	// 				m_strToken = _tchrHelper;
// 	// 				return formula_operator_right_shift_equal;
// 	// 			}
// 	// 			else
// 	// 			{
// 	// 				PositionSet( PositionGet() - 1 );
// 					return formula_operator_double_right_angle;
// 	// 			}
// 			}
	}
	if( _tchr == _T('<') )
	{	// try to find
		//   formula_operator_right_shift_equal >>= or
		//   formula_operator_double_right_angle >> or
		//   formula_operator_greater_equal >=
		if( _tchrHelper == _T('=') )
		{
			m_strToken += _tchrHelper;
			return formula_operator_less_equal;
		}
		if( _tchrHelper == _T('>') )
		{
			m_strToken += _tchrHelper;
			return formula_operator_not_equal;
		}
// 			if( _tchrHelper == _T('<') )
// 			{
// 				m_strToken = _tchrHelper;
// 	// 			_tchrHelper = ReadChar();
// 	// 			if( IsEOF() )
// 	// 			{
// 	// 				PositionSet( nPos );
// 	// 				OnError( _T("unexpected EOF") );
// 	// 				return 0;
// 	// 			}
// 	// 			if( _tchrHelper == _T('=') )
// 	// 			{
// 	// 				m_strToken = _tchrHelper;
// 	// 				return formula_operator_left_shift_equal;
// 	// 			}
// 	// 			else
// 	// 			{
// 	// 				PositionSet( PositionGet() - 1 );
// 					return formula_operator_double_left_angle;
// 	// 			}
// 			}
	}
	if( _tchr == _T('^') )
	{	// try to find formula_operator_exor_equal ^=
// 		if( _tchrHelper == _T('=') )
// 		{
// 			m_strToken = _tchrHelper;
// 			return formula_operator_exor_equal;
// 		}
	}
// 	if( _tchr == _T('!') )
// 	{	// try to find formula_operator_not_equal !=
// 		if( _tchrHelper == _T('=') )
// 		{
// 			m_strToken = _tchrHelper;
// 			return formula_operator_not_equal;
// 		}
// 	}
	// return letter as-is
	PositionSet( nPos + 1 );
	return _tchr;
}

// scan identifier (or keyword)
INT CExtFormulaParser::ReadIdentifier()
{
TCHAR _tchr;
INT nPos, nIdentifierLength;
	__EXT_DEBUG_GRID_ASSERT( ! IsEOF() );
	nPos = PositionGet();
	_tchr = ReadChar();
	if( ! ( isalpha( _tchr ) || _tchr == _T('_') || _tchr == _T('$') ) )
	{
		PositionSet( nPos );
		//__EXT_DEBUG_GRID_ASSERT( FALSE );
		return 0;
	}
	m_strToken += _tchr;
	nIdentifierLength = 1;
	for(	_tchr = ReadChar();
			( ! IsEOF() ) && ( isalpha( _tchr ) || isdigit( _tchr ) || _tchr == _T('_') || _tchr == _T('!') );
			nIdentifierLength++, _tchr = ReadChar()
			)
	{
		m_strToken += _tchr;
	}
	PositionSet( nPos + nIdentifierLength );
	return nIdentifierLength;
}

// scan for numeric oct
bool CExtFormulaParser::ReadConstantIntOct()
{
TCHAR _tchr, _tchrHelper;
INT nPos, nValueLength;
	__EXT_DEBUG_GRID_ASSERT( ! IsEOF() );
	nPos = PositionGet();
	_tchr = ReadChar();
	if( _tchr != _T('0') )
	{
		PositionSet( nPos );
		return false;
	}
	if( IsEOF() )
	{
		PositionSet( nPos );
		OnError( _T("unexpected EOF") );
		return false;
	}
	_tchr = ReadChar();
	if( _tchr == _T('x') || _tchr == _T('X') || _tchr == _T('e') || _tchr == _T('E') || _tchr == _T('.') )
	{
		PositionSet( nPos );
		return false;
	}
	if( ( ! isdigit( _tchr ) ) || _tchr == _T('9') )
	{
		_tchr = _T('0');
		m_strToken += _tchr;
		PositionSet( PositionGet() - 1 );
		return true;
	}
	m_strToken = _tchr;
	_tchrHelper = _T('0');
	m_strToken.Insert( 0, _tchrHelper );
	for( nValueLength = 2; ! IsEOF(); nValueLength ++ )
	{
		_tchr = ReadChar();
		if( ( ! isdigit( _tchr ) ) )
			break;
		if( _tchr == _T('9') )
		{
			m_strToken.Empty();
			PositionSet( nPos );
			return false;
		}
		m_strToken += _tchr;
	}
	if( _tchr == _T('x') || _tchr == _T('X') || _tchr == _T('e') || _tchr == _T('E') || _tchr == _T('.') )
	{
		m_strToken.Empty();
		PositionSet( nPos );
		return false;
	}
	__EXT_DEBUG_GRID_ASSERT( m_strToken.GetLength() == nValueLength );
	PositionSet( nPos + nValueLength );
	return true;
}

// scan for numeric decimal
bool CExtFormulaParser::ReadConstantIntDecimal()
{
TCHAR _tchr;
INT nPos, nValueLength;
	__EXT_DEBUG_GRID_ASSERT( ! IsEOF() );
	nPos = PositionGet();
	_tchr = ReadChar();
	if( ! isdigit( _tchr ) )
	{
		PositionSet( nPos );
		return false;
	}
	if( IsEOF() )
	{
		PositionSet( nPos );
		OnError( _T("unexpected EOF") );
		return false;
	}
	m_strToken += _tchr;
	for( nValueLength = 1; ! IsEOF(); nValueLength ++ )
	{
		_tchr = ReadChar();
		if( ( ! isdigit( _tchr ) ) )
			break;
		m_strToken += _tchr;
	}
	if( _tchr == _T('x') || _tchr == _T('X') || _tchr == _T('e') || _tchr == _T('E') || _tchr == _T('.') )
	{
		m_strToken.Empty();
		PositionSet( nPos );
		return false;
	}
	__EXT_DEBUG_GRID_ASSERT( m_strToken.GetLength() == nValueLength );
	PositionSet( nPos + nValueLength );
	return true;
}

// scan for numeric hex
bool CExtFormulaParser::ReadConstantIntHex()
{
TCHAR _tchr;
INT nPos, nValueLength;
	__EXT_DEBUG_GRID_ASSERT( ! IsEOF() );
	nPos = PositionGet();
	_tchr = ReadChar();
	if( _tchr != _T('0') )
	{
		PositionSet( nPos );
		return false;
	}
	if( IsEOF() )
	{
		PositionSet( nPos );
		OnError( _T("unexpected EOF") );
		return false;
	}
	_tchr = ReadChar();
	if( _tchr != _T('X') && _tchr != _T('x') )
	{
		PositionSet( nPos );
		return false;
	}
	if( IsEOF() )
	{
		PositionSet( nPos );
		OnError( _T("unexpected EOF") );
		return false;
	}
	_tchr = _T('0');
	m_strToken += _tchr;
	_tchr = _T('x');
	m_strToken += _tchr;
	for( nValueLength = 2; ! IsEOF(); nValueLength ++ )
	{
		_tchr = ReadChar();
		if( !(	isdigit( _tchr ) ||
				( _tchr >= _T('A') && _tchr <= _T('F') ) ||
				( _tchr >= _T('a') && _tchr <= _T('f') )
				)
			)
			break;
		m_strToken += _tchr;
	}
	__EXT_DEBUG_GRID_ASSERT( m_strToken.GetLength() == nValueLength );
	PositionSet( nPos + nValueLength );
	return true;
}

// scan for float, double or long double value
bool CExtFormulaParser::ReadConstantFloat()
{
TCHAR _tchr, _tchrHelper;
INT nPos, nManLength, nTailLength, nOrderLength, nRealManLength, nRealTailLength;
	__EXT_DEBUG_GRID_ASSERT( ! IsEOF() );
	nPos = PositionGet();
	nManLength = nTailLength = nOrderLength = nRealManLength = nRealTailLength = 0;
	_tchr = ReadChar();
	if( ! ( isdigit( _tchr ) || ( _tchr == _T('.') ) ) )
	{
		PositionSet( nPos );
		return false;
	}
	// mantissa
	if( _tchr != _T('.') )
	{
		m_strToken += _tchr;
		nManLength = nRealManLength = 1;
		for( ; ! IsEOF(); nManLength++, nRealManLength++ )
		{
			_tchr = ReadChar();
			if( ! isdigit( _tchr ) )
				break;
			m_strToken += _tchr;
		}
	}
	else
	{
		_tchrHelper = _T('0');
		m_strToken = _tchrHelper;
		nManLength = 1;
	}
	// point between mantissa and tail
	_tchrHelper = _T('.');
	m_strToken += _tchrHelper;
	// tail
	if( _tchr == _T('.') )
	{
		if( IsEOF() )
		{
			OnError( _T("unexpected EOF") );
			return false;
		}
		_tchr = ReadChar();
		for( ; isdigit( _tchr ) && ( ! IsEOF() ); _tchr = ReadChar(), nTailLength++, nRealTailLength++ )
			m_strToken += _tchr;
	}
	else
	{
		_tchrHelper = _T('0');
		m_strToken += _tchrHelper;
		nTailLength = 1;
	}
	// 'e'  between tail and order
	_tchrHelper = _T('e');
	m_strToken += _tchrHelper;
	// order
	if( _tchr == _T('E') || _tchr == _T('e') )
	{
		INT f_order;
		TCHAR sign;
		if( IsEOF() )
		{
			PositionSet( nPos );
			OnError( _T("unexpected EOF") );
			return false;
		}
		_tchr = ReadChar();
		if( IsEOF() )
		{
			PositionSet( nPos );
			OnError( _T("unexpected EOF") );
			return false;
		}
		if( _tchr == _T('+') || _tchr == _T('-') )
		{
			sign = _tchr;
			f_order = 0;
		}
		else
		{
			sign = _T('+');
			if( ! isdigit( _tchr ) )
			{ 
				PositionSet( nPos );
				m_strToken.Empty();
				return false;
			}
			f_order = _tchr - _T('0');
		}
		if( sign == _T('-') )
		{
			m_strToken += sign;
			nOrderLength++;
		}
		if( f_order != 0 )
		{
			__EXT_DEBUG_GRID_ASSERT( f_order == (INT) ( _tchr - _T('0') ) );
			m_strToken += _tchr;
			nOrderLength++;
		}

		for( _tchr = ReadChar(); isdigit( _tchr ); )
		{
			m_strToken += _tchr;
			f_order = f_order * 10 + ( _tchr - _T('0') );
			if( IsEOF() )
				break;
			_tchr = ReadChar();
			nOrderLength++;
		}
	}
	else
	{
		if( nRealManLength == 0 && nRealTailLength == 0 )
		{
			// just '.' character encountered, this is not floating value
			PositionSet( nPos );
			m_strToken.Empty();
			return false;
		}
		_tchrHelper = _T('0');
		m_strToken += _tchrHelper;
		nOrderLength = 1;
	}
	PositionSet( PositionGet() - 1 );
	//__EXT_DEBUG_GRID_ASSERT( m_strToken.GetLength() == ( nManLength + nTailLength + nOrderLength + 2 ) );
	return true;
}

bool CExtFormulaParser::ReadSlashChar( __EXT_MFC_SAFE_LPTSTR pc, INT * p_nValueLength, INT * p_nPos )
{
TCHAR _tchr, _tchrHelper;
	__EXT_DEBUG_GRID_ASSERT( pc != NULL );
	__EXT_DEBUG_GRID_ASSERT( p_nValueLength != NULL );
	__EXT_DEBUG_GRID_ASSERT( p_nPos != NULL );
	if( IsEOF() )
	{
		PositionSet( *p_nPos );
		OnError( _T("unexpected EOF") );
		return false;
	}
	(*((LPTSTR)pc)) = ReadChar();
	if( IsEOF() )
	{
		PositionSet( *p_nPos );
		OnError( _T("unexpected EOF") );
		return false;
	}
	switch( *LPTSTR(pc) )
	{
	case _T('x'):
	case _T('X'):
		// code through oct
		//__EXT_DEBUG_GRID_ASSERT( FALSE );
		OnError( _T("hex char codes in string values not supported") );
		PositionSet( *p_nPos );
		return false;
	break;
	case _T('0'):
		// code through oct or hex
		_tchrHelper = ReadChar();
		if( IsEOF() )
		{
			PositionSet( *p_nPos );
			OnError( _T("unexpected EOF") );
			return false;
		}
		if( _tchrHelper == _T('X') || _tchrHelper == _T('x') )
		{
			// code through hex
			//__EXT_DEBUG_GRID_ASSERT( FALSE );
			OnError( _T("hex char codes in string values not supported") );
			PositionSet( *p_nPos );
			return false;
		}
		else
		{
			// code through oct
			//__EXT_DEBUG_GRID_ASSERT( FALSE );
			OnError( _T("hex char codes in string values not supported") );
			PositionSet( *p_nPos );
			return false;
		}
	break;
	case _T('1'):	case _T('2'):	case _T('3'):
	case _T('4'):	case _T('5'):	case _T('6'):
	case _T('7'):	case _T('8'):	case _T('9'):
		// code through decimal
		//__EXT_DEBUG_GRID_ASSERT( FALSE );
		OnError( _T("hex char codes in string values not supported") );
		PositionSet( *p_nPos );
		return false;
	break;
	case _T('t'):
		_tchr = _T('\t');
		m_strToken += _tchr;
		(*p_nValueLength)++;
	break;
	case _T('r'):
		_tchr = _T('\r');
		(*p_nValueLength)++;
		m_strToken += _tchr;
	break;
	case _T('n'):
		_tchr = _T('\n');
		m_strToken += _tchr;
		(*p_nValueLength)++;
	break;
	default:
		m_strToken += TCHAR(*LPTSTR(pc));
		(*p_nValueLength)++;
	break;
	};
	return true;
}

//	// scan for char value in ''
//	bool CExtFormulaParser::ReadConstantChar()
//	{
//	TCHAR _tchr;
//	INT nValueLength, nPos;
//		__EXT_DEBUG_GRID_ASSERT( ! IsEOF() );
//		nPos = PositionGet();
//		_tchr = ReadChar();
//		if( _tchr != _T('\'') )
//		{
//			PositionSet( nPos );
//			return false;
//		}
//		if( IsEOF() )
//		{
//			PositionSet( nPos );
//			OnError( _T("unexpected EOF") );
//			return false;
//		}
//		_tchr = ReadChar();
//		if( IsEOF() )
//		{
//			PositionSet( nPos );
//			OnError( _T("unexpected EOF") );
//			return false;
//		}
//		nValueLength = 0;
//		if( _tchr == _T('\\') )
//		{
//			if( ! ReadSlashChar( &_tchr, &nValueLength, &nPos ) )
//				return false;
//		}
//		else
//		{
//			m_strToken += _tchr;
//			nValueLength++;
//		}
//		if( IsEOF() )
//		{
//			PositionSet( nPos );
//			OnError( _T("unexpected EOF") );
//			return false;
//		}
//		_tchr = ReadChar();
//		if( _tchr != _T('\'') )
//		{
//			PositionSet( nPos );
//			OnError( _T("bad character constant definion") );
//			return false;
//		}
//		if( IsEOF() )
//		{
//			PositionSet( nPos );
//			OnError( _T("unexpected EOF") );
//			return false;
//		}
//	// begin TEMPORARY CODE
//		_tchr = _T('\'');
//		m_strToken += _tchr;
//		m_strToken.Insert( 0, _tchr );
//	// end   TEMPORARY CODE
//		return true;
//	}

// scan for string value in ""
bool CExtFormulaParser::ReadConstantString()
{
TCHAR _tchr;
INT nValueLength, nPos;
	__EXT_DEBUG_GRID_ASSERT( ! IsEOF() );
	nPos = PositionGet();
	_tchr = ReadChar();
	if( _tchr != _T('\"') )
	{
		PositionSet( nPos );
		return false;
	}
	if( IsEOF() )
	{
		PositionSet( nPos );
		OnError( _T("unexpected EOF") );
		return false;
	}
	for( _tchr = ReadChar(), nValueLength = 0; ( _tchr != _T('\"') ) && ( ! IsEOF() ); _tchr = ReadChar() )
	{
		if( IsEOF() )
		{
			PositionSet( nPos );
			OnError( _T("unexpected EOF") );
			return false;
		}
		if( _tchr == _T('\\') )
		{
			if(!ReadSlashChar( &_tchr, &nValueLength, &nPos ) )
			{
				return false;
			}
		}
		else
		{
			m_strToken += _tchr;
			nValueLength++;
		}
	}
	if( IsEOF() )
	{
		PositionSet( nPos );
		OnError( _T("unexpected EOF") );
		return false;
	}
	__EXT_DEBUG_GRID_ASSERT( m_strToken.GetLength() == nValueLength );
	_tchr = _T('\"');
	m_strToken += _tchr;
	m_strToken.Insert( 0, _tchr );
	return true;
}

#if (defined __EXT_FORMULA_BUILT_IN_PARSER__)

bool CExtFormulaParser::Parse()
{
	m_nParserLexCounter = 0;
	yystacksize = 0;
	if( m_pParserStackA != NULL )
	{
		::free( m_pParserStackA );
		m_pParserStackA = NULL;
	}
	if( m_pParserStackB != NULL )
	{
		::free( m_pParserStackB );
		m_pParserStackB = NULL;
	}
	try
	{
		while( ( ! IsEOF() ) && ( ! m_bParserIsInTheErrorState ) )
			OnParse();
	}
	catch( CException * pException )
	{
		pException->Delete();
	}
	yystacksize = 0;
	if( m_pParserStackA != NULL )
	{
		::free( m_pParserStackA );
		m_pParserStackA = NULL;
	}
	if( m_pParserStackB != NULL )
	{
		::free( m_pParserStackB );
		m_pParserStackB = NULL;
	}

	if( m_pFormulaRoot != NULL )
		m_pFormulaRoot->RemoveFromAI( m_mapAI );

POSITION pos = m_mapAI.GetStartPosition();
	for( ; pos != NULL; )
	{
		CExtFormulaItem * pFI = NULL;
		bool bTmp = false;
		m_mapAI.GetNextAssoc( pos, pFI, bTmp );
		__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
		pFI->DetachAllChildren();
		delete pFI;
	}
	m_mapAI.RemoveAll();

	return ( m_pFormulaRoot != NULL ) ? true : false;
}

void CExtFormulaParser::OnError( __EXT_MFC_SAFE_LPCTSTR strErrorDescription )
{
	strErrorDescription;
#if (defined __EXT_DEBUG_HEAVY_FORMULA_PARSER)
	_ftprintf( stdout, _T("%s"), LPCTSTR(strErrorDescription) );
#endif // (defined __EXT_DEBUG_HEAVY_FORMULA_PARSER)
	::AfxThrowUserException();
}

int CExtFormulaParser::OnGrowStack()
{
__EXT_MFC_INT_PTR i;
__EXT_MFC_UINT_PTR newsize;
short * newss = NULL;
CExtFormulaItem ** newvs = NULL;
    if ((newsize = yystacksize) == 0)
        newsize = 500;
    else if (newsize >= 500)
        return -1;
    else if ((newsize *= 2) > 500)
        newsize = 500;

    i = yyssp - yyss;
    newss =
		(yyss != 0)
			? (short *)( m_pParserStackA = ::realloc( yyss, newsize * sizeof(*newss) ) )
			: (short *)( m_pParserStackA = ::malloc( newsize * sizeof(*newss) ) )
			;
    if (newss == 0)
        return -1;

    yyss  = newss;
    yyssp = newss + i;
    newvs =
		(yyvs != 0)
			? (CExtFormulaItem **)( m_pParserStackB = ::realloc( yyvs, newsize * sizeof(*newvs) ) )
			: (CExtFormulaItem **)( m_pParserStackB = ::malloc( newsize * sizeof(*newvs) ) )
			;
    if (newvs == 0)
        return -1;

    yyvs = newvs;
    yyvsp = newvs + i;
    yystacksize = newsize;
    yysslim = yyss + newsize - 1;
    return 0;
}

const int CExtFormulaParser::formula_constant_float = 257;
const int CExtFormulaParser::formula_constant_int = 258;
const int CExtFormulaParser::formula_constant_string = 259;
const int CExtFormulaParser::formula_identifier = 260;
const int CExtFormulaParser::formula_operator_greater_equal = 261;
const int CExtFormulaParser::formula_operator_less_equal = 262;
const int CExtFormulaParser::formula_operator_not_equal = 263;

int CExtFormulaParser::OnParse()
{
	yyss = NULL;
	yysslim = NULL;
	yyvs = NULL;
	yyssp = NULL;
	yyvsp = NULL;
	yyval = NULL;
	yylval = NULL;
static const short yylhs[] = {                           -1,
    0,    1,    1,    1,    1,    1,    1,    1,    1,    1,
    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
    1,    2,    2,    2,    4,    4,    4,    3,    3,    3,
    3,    3,    5,    5,
};
static const short yylen[] = {                            2,
    2,    1,    3,    3,    3,    3,    3,    3,    3,    3,
    3,    3,    3,    3,    3,    3,    2,    2,    2,    2,
    2,    1,    2,    2,    1,    1,    1,    1,    1,    3,
    3,    4,    1,    3,
};
static const short yydefred[] = {                         0,
    0,    0,   26,   25,   27,    0,    0,    0,    0,    0,
    0,    0,    2,   22,   29,    0,    0,    0,   23,   24,
   21,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,   20,   31,    0,    0,
   30,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,   13,    0,   32,    0,
};
static const short yydgoto[] = {                          2,
   18,   13,   14,   15,   40,
};
static const short yysindex[] = {                       -57,
  190,    0,    0,    0,    0,  -33,  269,  213,  213,  190,
  190,  334,    0,    0,    0,  207,  269,  334,    0,    0,
    0,  242,  190,  190,  190,  190,  190,  190,  190,  190,
  190,  190,  190,  190,  190,  190,    0,    0,  334,  -29,
    0,  -37,  -37,  343,  334,  303,  303,  343,  -37,  -37,
  -21,  -21,  -19,  -19,    0,  190,    0,  334,
};
static const short yyrindex[] = {                         0,
    0,    0,    0,    0,    0,    1,    0,    0,    0,    0,
    0,   11,    0,    0,    0,    0,   64,   14,    0,    0,
    0,    0,    0,    0,    0,   72,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,  -24,    0,
    0,  138,  146,  188,   49,   60,  315,  193,  153,  180,
   99,  107,    9,   36,    0,    0,    0,  -22,
};
static const short yygindex[] = {                         0,
  452,   -6,    0,    0,    0,
};
const int YYTABLESIZE = 605;
static const short yytable[] = {                         37,
   28,   19,   20,    1,   34,   32,   16,   33,    3,   35,
    1,   57,    0,   18,   56,   37,   33,   37,   34,   33,
   34,   34,    0,    0,    0,   35,    0,    0,    0,    0,
    0,    0,   28,    0,    0,    4,    0,   28,   28,    0,
    3,   28,   28,   28,   28,   28,    3,   28,   15,    3,
    3,    3,    3,    3,   18,    3,   36,   18,   28,   16,
   28,   28,   28,   19,    0,    0,    3,    4,    3,    3,
    3,   17,   36,    4,   36,    0,    4,    4,    4,    4,
    4,    0,    4,    0,    0,    0,    0,    0,    0,   15,
    0,   16,   15,    4,   28,    4,    4,    4,    5,    0,
   16,   19,    0,   16,   19,   19,    6,   19,    0,   17,
   19,    0,   17,   17,    0,   17,    0,   16,   17,    0,
    0,   19,    0,   19,   19,   19,    0,    0,    0,   17,
    5,   17,   17,   17,    0,    0,    5,    9,    6,    5,
    0,    5,    5,    5,    6,   10,    0,    6,    0,    6,
    6,    6,    7,    0,    0,    0,    5,   19,    5,    5,
    5,    0,    0,    0,    6,   17,    6,    6,    6,    9,
    0,    0,    0,    0,    0,    9,    0,   10,    9,    8,
    0,    9,    0,   10,    7,    0,   10,   11,    0,   10,
    7,    0,   12,    7,    0,    9,    7,    9,    9,    9,
    0,    0,    0,   10,    0,   10,   10,   10,    0,    0,
    7,    8,    7,    7,    7,    0,    0,    8,    0,   11,
    8,    7,    0,    8,   12,   11,   10,    0,   11,   11,
   12,   11,    8,   12,    9,    0,   12,    8,    7,    8,
    8,    8,    0,   10,    0,   11,   11,   38,   11,    8,
   12,    9,   11,   12,    0,    8,    0,    9,    0,    0,
    0,   28,   28,   28,    0,    0,    0,    0,    0,    3,
    3,    3,    0,   26,    0,    0,    0,    0,   37,   29,
    0,    0,   41,   34,   32,    0,   33,    0,   35,    0,
    0,    0,    0,    0,    0,    0,    4,    4,    4,   27,
   17,   30,   28,   31,    0,   10,    0,    0,   11,    0,
    0,    8,    0,    9,   14,    0,    0,    0,    0,    0,
    0,    0,    0,    0,   19,   19,   19,    0,    0,    0,
    0,    0,   17,   17,   17,   36,    0,    0,    0,   37,
   29,    0,    0,    0,   34,   32,   14,   33,    0,   35,
    0,    0,    0,    0,    0,   14,    0,    0,   14,    5,
    5,    5,   30,   28,   31,   26,    0,    6,    6,    6,
   37,   29,   14,    0,    0,   34,   32,    0,   33,   37,
   35,    0,    0,    0,   34,   32,    0,   33,    0,   35,
    0,   27,    0,   30,   28,   31,   36,    0,    9,    9,
    9,    0,   30,    0,   31,    0,   10,   10,   10,    0,
    0,    0,    0,    7,    7,    7,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,   36,    0,    0,
    0,    0,    0,    0,    0,    0,   36,    0,    0,    0,
    8,    8,    8,    0,    0,    0,    3,    4,    5,    6,
   11,    0,   12,    0,    0,   12,    0,    0,    0,    0,
    0,   21,   22,    3,    4,    5,    6,   39,    0,    3,
    4,    5,    6,    0,   42,   43,   44,   45,   46,   47,
   48,   49,   50,   51,   52,   53,   54,   55,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,   23,   24,   25,    0,    0,   58,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    3,    4,    5,    6,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,   23,   24,   25,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,   23,   24,   25,    0,    0,    0,
    0,    0,    0,   23,   24,
};
static const short yycheck[] = {                         37,
    0,    8,    9,   61,   42,   43,   40,   45,    0,   47,
    0,   41,   -1,    0,   44,   37,   41,   37,   41,   44,
   42,   44,   -1,   -1,   -1,   47,   -1,   -1,   -1,   -1,
   -1,   -1,   32,   -1,   -1,    0,   -1,   37,   38,   -1,
   32,   41,   42,   43,   44,   45,   38,   47,    0,   41,
   42,   43,   44,   45,   41,   47,   94,   44,   58,    0,
   60,   61,   62,    0,   -1,   -1,   58,   32,   60,   61,
   62,    0,   94,   38,   94,   -1,   41,   42,   43,   44,
   45,   -1,   47,   -1,   -1,   -1,   -1,   -1,   -1,   41,
   -1,   32,   44,   58,   94,   60,   61,   62,    0,   -1,
   41,   38,   -1,   44,   41,   42,    0,   44,   -1,   38,
   47,   -1,   41,   42,   -1,   44,   -1,   58,   47,   -1,
   -1,   58,   -1,   60,   61,   62,   -1,   -1,   -1,   58,
   32,   60,   61,   62,   -1,   -1,   38,    0,   32,   41,
   -1,   43,   44,   45,   38,    0,   -1,   41,   -1,   43,
   44,   45,    0,   -1,   -1,   -1,   58,   94,   60,   61,
   62,   -1,   -1,   -1,   58,   94,   60,   61,   62,   32,
   -1,   -1,   -1,   -1,   -1,   38,   -1,   32,   41,    0,
   -1,   44,   -1,   38,   32,   -1,   41,    0,   -1,   44,
   38,   -1,    0,   41,   -1,   58,   44,   60,   61,   62,
   -1,   -1,   -1,   58,   -1,   60,   61,   62,   -1,   -1,
   58,   32,   60,   61,   62,   -1,   -1,   38,   -1,   32,
   41,   32,   -1,   44,   32,   38,   37,   -1,   41,   40,
   38,   44,   43,   41,   45,   -1,   44,   58,   32,   60,
   61,   62,   -1,   37,   -1,   58,   40,   41,   61,   43,
   58,   45,   40,   61,   -1,   43,   -1,   45,   -1,   -1,
   -1,  261,  262,  263,   -1,   -1,   -1,   -1,   -1,  261,
  262,  263,   -1,   32,   -1,   -1,   -1,   -1,   37,   38,
   -1,   -1,   41,   42,   43,   -1,   45,   -1,   47,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,  261,  262,  263,   58,
   32,   60,   61,   62,   -1,   37,   -1,   -1,   40,   -1,
   -1,   43,   -1,   45,    0,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,  261,  262,  263,   -1,   -1,   -1,
   -1,   -1,  261,  262,  263,   94,   -1,   -1,   -1,   37,
   38,   -1,   -1,   -1,   42,   43,   32,   45,   -1,   47,
   -1,   -1,   -1,   -1,   -1,   41,   -1,   -1,   44,  261,
  262,  263,   60,   61,   62,   32,   -1,  261,  262,  263,
   37,   38,   58,   -1,   -1,   42,   43,   -1,   45,   37,
   47,   -1,   -1,   -1,   42,   43,   -1,   45,   -1,   47,
   -1,   58,   -1,   60,   61,   62,   94,   -1,  261,  262,
  263,   -1,   60,   -1,   62,   -1,  261,  262,  263,   -1,
   -1,   -1,   -1,  261,  262,  263,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   94,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   94,   -1,   -1,   -1,
  261,  262,  263,   -1,   -1,   -1,  257,  258,  259,  260,
  263,   -1,    1,   -1,   -1,  263,   -1,   -1,   -1,   -1,
   -1,   10,   11,  257,  258,  259,  260,   16,   -1,  257,
  258,  259,  260,   -1,   23,   24,   25,   26,   27,   28,
   29,   30,   31,   32,   33,   34,   35,   36,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,  261,  262,  263,   -1,   -1,   56,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,  257,  258,  259,  260,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,  261,  262,  263,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,  261,  262,  263,   -1,   -1,   -1,
   -1,   -1,   -1,  261,  262,
};

int yym, yyn, yystate;
int      yynerrs;
int      yyerrflag;
int      yychar;
    yynerrs = 0;
    yyerrflag = 0;
    yychar = (-1);
    yystate = 0;
    if( yyss == NULL && OnGrowStack() )
		goto yyoverflow;
    yyssp = yyss;
    yyvsp = yyvs;
    yystate = 0;
    *yyssp = 0;
yyloop:
    if( (yyn = yydefred[yystate]) != 0)
		goto yyreduce;
    if( yychar < 0 )
    {
        if( ( yychar = Lex() ) < 0) 
			yychar = 0;
    }
    if( (yyn = yysindex[yystate]) != 0 && (yyn += yychar) >= 0 && yyn <= YYTABLESIZE && yycheck[yyn] == yychar )
    {
        if( yyssp >= yysslim && OnGrowStack() )
            goto yyoverflow;
        yystate = yytable[yyn];
        *++yyssp = yytable[yyn];
        *++yyvsp = yylval;
        yychar = (-1);
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if( (yyn = yyrindex[yystate]) != 0 && (yyn += yychar) >= 0 && yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if( yyerrflag )
		goto yyinrecovery;
	    OnError( _T("syntax error") );
    goto yyerrlab;
yyerrlab:
    ++yynerrs;
yyinrecovery:
    if( yyerrflag < 3 )
    {
        yyerrflag = 3;
        for(;;)
        {
            if( (yyn = yysindex[*yyssp]) != 0 && (yyn += 256) >= 0 && yyn <= YYTABLESIZE && yycheck[yyn] == 256)
            {
                if( yyssp >= yysslim && OnGrowStack() )
                    goto yyoverflow;
                yystate = yytable[yyn];
                *++yyssp = yytable[yyn];
                *++yyvsp = yylval;
                goto yyloop;
            }
            else
            {
                if (yyssp <= yyss) goto yyabort;
                --yyssp;
                --yyvsp;
            }
        }
    }
    else
    {
        if( yychar == 0 )
			goto yyabort;
        yychar = (-1);
        goto yyloop;
    }
yyreduce:
    yym = yylen[yyn];
    if (yym)
        yyval = yyvsp[1-yym];
    else
        memset(&yyval, 0, sizeof yyval);
    switch (yyn)
    {
		case 1:
			{
					yyval = yyvsp[0];
					__EXT_DEBUG_GRID_ASSERT( m_pFormulaRoot == NULL );
					m_pFormulaRoot = yyval;
					yyvsp[-1]->RemoveFromAI( m_mapAI );
					delete yyvsp[-1];
					yyvsp[-1] = NULL;
				}
		break;
		case 2:
			{
					yyval = yyvsp[0];
				}
		break;
		case 3:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_MULTIPLY;
					yyval = yyvsp[-1];
					yyval->AddChild(yyvsp[-2]);
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 4:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_DIVIDE;
					yyval = yyvsp[-1];
					yyval->AddChild(yyvsp[-2]);
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 5:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_PLUS;
					yyval = yyvsp[-1];
					yyval->AddChild(yyvsp[-2]);
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 6:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_MINUS;
					yyval = yyvsp[-1];
					yyval->AddChild(yyvsp[-2]);
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 7:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_LESS;
					yyval = yyvsp[-1];
					yyval->AddChild(yyvsp[-2]);
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 8:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_GREATER;
					yyval = yyvsp[-1];
					yyval->AddChild(yyvsp[-2]);
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 9:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_GREATER_EQUAL;
					yyval = yyvsp[-1];
					yyval->AddChild(yyvsp[-2]);
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 10:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_LESS_EQUAL;
					yyval = yyvsp[-1];
					yyval->AddChild(yyvsp[-2]);
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 11:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_NOT_EQUAL;
					yyval = yyvsp[-1];
					yyval->AddChild(yyvsp[-2]);
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 12:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_STR_CAT;
					yyval = yyvsp[-1];
					yyval->AddChild(yyvsp[-2]);
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 13:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_POW;
					yyval = yyvsp[-1];
					yyval->AddChild(yyvsp[-2]);
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 14:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_EQUAL;
					yyval = yyvsp[-1];
					yyval->AddChild(yyvsp[-2]);
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 15:
			{
					yyval = yyvsp[-1];
					yyval->CopyScanInfoFrom( yyvsp[-2], false, true, false );
					yyval->CopyScanInfoFrom( yyvsp[0], false, false, true );
					if( yyvsp[-2]->m_eType == CExtFormulaItem::__FORMULA_ATOM_CELL_RANGE_INTERSECTION )
					{
						yyvsp[-2]->RemoveFromAI( m_mapAI );
						yyval->AddChildrenOf( yyvsp[-2], true );
					}
					else
						yyval->AddChild( yyvsp[-2] );
					if( yyvsp[0]->m_eType == CExtFormulaItem::__FORMULA_ATOM_CELL_RANGE_INTERSECTION )
					{
						yyvsp[0]->RemoveFromAI( m_mapAI );
						yyval->AddChildrenOf( yyvsp[0], true );
					}
					else
						yyval->AddChild( yyvsp[0] );
					yyval->m_eType = CExtFormulaItem::__FORMULA_ATOM_CELL_RANGE_INTERSECTION;
					yyval->m_strNameHelper = _T("range intersection");
				}
		break;
		case 16:
			{
					yyval = new CExtFormulaItem;
					m_mapAI.SetAt( yyval, false );
					/*$$->m_strName = $1->m_strName;*/
					/*$$->m_strName += ':';*/
					/*$$->m_strName += $3->m_strName;*/
					yyval->CopyScanInfoFrom( yyvsp[-2], false, true, false );
					yyval->CopyScanInfoFrom( yyvsp[0], false, false, true );
					yyval->AddChild( yyvsp[-2] );
					yyval->AddChild( yyvsp[0] );
					yyval->m_eType = CExtFormulaItem::__FORMULA_ATOM_CELL_RANGE;
					yyval->m_strNameHelper = _T("range");
					yyvsp[-1]->RemoveFromAI( m_mapAI );
					delete yyvsp[-1];
					yyvsp[-1] = NULL;
				}
		break;
		case 17:
			{
					yyval = yyvsp[-1];
					yyvsp[0]->RemoveFromAI( m_mapAI );
					delete yyvsp[0];
					yyvsp[0] = NULL;
				}
		break;
		case 18:
			{
					yyval = yyvsp[0];
					yyvsp[-1]->RemoveFromAI( m_mapAI );
					delete yyvsp[-1];
					yyvsp[-1] = NULL;
				}
		break;
		case 19:
			{
					yyval = yyvsp[-1];
					yyvsp[0]->RemoveFromAI( m_mapAI );
					delete yyvsp[0];
					yyvsp[0] = NULL;
				}
		break;
		case 20:
			{
					yyvsp[0]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_PERCENT;
					yyval = yyvsp[0];
					yyval->m_strName = _T("%");
					yyval->m_strNameHelper = _T("unary");
					yyval->AddChild(yyvsp[-1]);
				}
		break;
		case 21:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_EXPR_PERCENT;
					yyval = yyvsp[-1];
					yyval->m_strName = _T("%");
					yyval->m_strNameHelper = _T("unary");
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 22:
			{
					yyval = yyvsp[0];
				}
		break;
		case 23:
			{
					yyval = yyvsp[0];
				}
		break;
		case 24:
			{
					yyvsp[-1]->m_eType = CExtFormulaItem::__FORMULA_ATOM_TERM_UNARY_MINUS;
					yyval = yyvsp[-1];
					yyval->m_strName = _T("-");
					yyval->m_strNameHelper = _T("unary");
					yyval->AddChild(yyvsp[0]);
				}
		break;
		case 25:
			{
					yyval = yyvsp[0];
					yyval->m_eType = CExtFormulaItem::__FORMULA_ATOM_CONSTANT_INT;
				}
		break;
		case 26:
			{
					yyval = yyvsp[0];
					yyval->m_eType = CExtFormulaItem::__FORMULA_ATOM_CONSTANT_FLOAT;
				}
		break;
		case 27:
			{
					yyval = yyvsp[0];
					yyval->m_eType = CExtFormulaItem::__FORMULA_ATOM_CONSTANT_STRING;
				}
		break;
		case 28:
			{ /* simplest scope access case */
					yyval = yyvsp[0];
					yyval->m_eType = CExtFormulaItem::__FORMULA_ATOM_IDENTIFIER;
				}
		break;
		case 29:
			{
					yyval = yyvsp[0];
				}
		break;
		case 30:
			{
					yyval = yyvsp[-1];
				}
		break;
		case 31:
			{
					yyval = new CExtFormulaItem;
					m_mapAI.SetAt( yyval, false );
					yyval->m_strName = yyvsp[-2]->m_strName;
					yyval->CopyScanInfoFrom( yyvsp[-2], false );
					yyval->m_eType = CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL;
					yyval->m_strNameHelper = _T("function call");
					yyvsp[-2]->RemoveFromAI( m_mapAI );
					delete yyvsp[-2];
					yyvsp[-2] = NULL;
					yyvsp[-1]->RemoveFromAI( m_mapAI );
					delete yyvsp[-1];
					yyvsp[-1] = NULL;
					yyvsp[0]->RemoveFromAI( m_mapAI );
					delete yyvsp[0];
					yyvsp[0] = NULL;
				}
		break;
		case 32:
			{
					yyval = new CExtFormulaItem;
					m_mapAI.SetAt( yyval, false );
					yyval->m_strName = yyvsp[-3]->m_strName;
					yyval->CopyScanInfoFrom( yyvsp[-3], true );
					yyval->m_eType = CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL;
					yyval->m_strNameHelper = _T("function call");
					yyval->AddChildrenOf( yyvsp[-1], false );
					yyvsp[-3]->RemoveFromAI( m_mapAI );
					delete yyvsp[-3];
					yyvsp[-3] = NULL;
					yyvsp[-2]->RemoveFromAI( m_mapAI );
					delete yyvsp[-2];
					yyvsp[-2] = NULL;
					yyvsp[-1]->RemoveFromAI( m_mapAI );
					delete yyvsp[-1];
					yyvsp[-1] = NULL;
					yyvsp[0]->RemoveFromAI( m_mapAI );
					delete yyvsp[0];
					yyvsp[0] = NULL;
				}
		break;
		case 33:
			{
					yyval = new CExtFormulaItem; /* temporarily used as collection*/
					m_mapAI.SetAt( yyval, false );
					yyval->AddChild( yyvsp[0] );
				}
		break;
		case 34:
			{
					yyvsp[-2]->AddChild( yyvsp[0] );
					yyval = yyvsp[-2];
					yyvsp[-1]->RemoveFromAI( m_mapAI );
					delete yyvsp[-1];
					yyvsp[-1] = NULL;
				}
		break;
    }
    yyssp -= yym;
    yystate = *yyssp;
    yyvsp -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
        yystate = 2;
        *++yyssp = 2;
        *++yyvsp = yyval;
        if( yychar < 0 )
        {
            if( (yychar = Lex() ) < 0 )
				yychar = 0;
        }
        if (yychar == 0) goto yyaccept;
        goto yyloop;
    }
    if( (yyn = yygindex[yym]) != 0 && (yyn += yystate) >= 0 && yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
    if( yyssp >= yysslim && OnGrowStack() )
        goto yyoverflow;
    *++yyssp = (short) yystate;
    *++yyvsp = yyval;
    goto yyloop;
yyoverflow:
    OnError( _T("parser stack overflow") );
yyabort:
    return (1);
yyaccept:
    return (0);
}

#endif // (defined __EXT_FORMULA_BUILT_IN_PARSER__)

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaAlphabet

CExtFormulaAlphabet::CExtFormulaAlphabet()
{
}

CExtFormulaAlphabet::~CExtFormulaAlphabet()
{
}

LONG CExtFormulaAlphabet::GetAlphabetSize() const
{
	return 26L;
}

__EXT_MFC_SAFE_LPCTSTR CExtFormulaAlphabet::OnQueryAlphabetArray() const
{
static const TCHAR arr[27] = _T("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	return arr;
}

bool CExtFormulaAlphabet::PreTranslateAlphaChar( __EXT_MFC_SAFE_TCHAR & _tchr ) const
{
TCHAR s[2] = { _tchr, _T('\0') };
	__EXT_MFC_STRUPR( s, 2 );
	if( _tchr == s[0] )
		return false;
	_tchr = s[0];
	return true;
}

bool CExtFormulaAlphabet::CanBeFirstChar( __EXT_MFC_SAFE_TCHAR _tchr ) const
{
	if( _tchr == _T('$') )
		return true;
	return false;
}

LONG CExtFormulaAlphabet::GetAlphaCharIndex( __EXT_MFC_SAFE_TCHAR _tchr ) const
{
LONG nAlphabetSize = GetAlphabetSize();
	__EXT_DEBUG_GRID_ASSERT( nAlphabetSize > 0 );
LPCTSTR pAA = (LPCTSTR) OnQueryAlphabetArray();
	__EXT_DEBUG_GRID_ASSERT( pAA != NULL );
	PreTranslateAlphaChar( _tchr );
	if( ! ( pAA[0] <= TCHAR(_tchr) && TCHAR(_tchr) <= pAA[nAlphabetSize-1] ) )
		return -1;
LONG nAlphaCharIndex = LONG(TCHAR(_tchr)) - LONG(pAA[0]); // assuming here that the alphabet is the serial sequence of letter codes
	return nAlphaCharIndex;
}

bool CExtFormulaAlphabet::IsAlphaChar( __EXT_MFC_SAFE_TCHAR _tchr ) const
{
INT nAlphaCharIndex = GetAlphaCharIndex( _tchr );
bool bIsAlphaChar = ( nAlphaCharIndex >= 0 ) ? true : false;
	return bIsAlphaChar;
}

LONG CExtFormulaAlphabet::GetMaxAlphaStrLen() const
{
	return 4L;
}

LONG CExtFormulaAlphabet::GetMaxAlphaStrVal() const
{
	return (26L*26L*26L*26L*26L*26L)-1L;
}

LONG CExtFormulaAlphabet::ParseStrPartAlpha( __EXT_MFC_SAFE_LPCTSTR & str ) const
{
LPCTSTR & strWalk = ( LPCTSTR & ) ( LPVOID & ) str;
	if( strWalk == NULL )
		return -1L;
	if( strWalk[0] == _T('\0') )
		return -1L;
LONG nRetVal = 0L, nSqIdx, nSqAdj = 0L, nAlphabetSize = GetAlphabetSize(), nMaxLen = GetMaxAlphaStrLen(), nMaxVal = GetMaxAlphaStrVal();
	__EXT_DEBUG_GRID_ASSERT( nAlphabetSize > 0 );
	for( nSqIdx = 0; (*strWalk) != _T('\0'); strWalk++, nSqIdx++ )
	{
		if( ( nSqIdx + nSqAdj ) > nMaxLen )
			return -1;
		TCHAR _tchr = *((LPCTSTR)strWalk);
		LONG nAlphaCharIndex = GetAlphaCharIndex( _tchr );
		if( nAlphaCharIndex < 0L )
		{
			if(		nSqIdx == 0L
				&&	CanBeFirstChar( _tchr )
				)
			{
				nSqAdj = -1L;
				continue;
			}
			if( nSqIdx == 0L )
				return -1L;
			break;
		}
		nRetVal *= nAlphabetSize;
		nRetVal += nAlphaCharIndex;
		if( nRetVal > nMaxVal )
			return -1L;
	}
	return nRetVal;
}

LONG CExtFormulaAlphabet::GetMaxNumStrVal() const
{
	return LONG(INT_MAX/2-1);
}

LONG CExtFormulaAlphabet::ParseStrPartNum( __EXT_MFC_SAFE_LPCTSTR & str ) const
{
LPCTSTR & strWalk = ( LPCTSTR & ) ( LPVOID & ) str;
	if( strWalk == NULL )
		return -1L;
	if( strWalk[0] == _T('\0') )
		return -1L;
LONG nRetVal = 0L, nMaxVal = GetMaxNumStrVal();
	for( ; (*strWalk) != _T('\0'); strWalk++ )
	{
		TCHAR _tchr = *((LPCTSTR)strWalk);
		if( ! ( _T('0') <= _tchr && _tchr <= _T('9') ) )
			return -1L;
		LONG nNum = LONG( _tchr - _T('0') );
		nRetVal *= 10L;
		nRetVal += nNum;
		if( nRetVal > nMaxVal )
			return -1L;
	}
	if( nRetVal == 0L )
		return -1;
	nRetVal --;
	return nRetVal;
}

bool CExtFormulaAlphabet::ParseCheckEmptySpaceChar( __EXT_MFC_SAFE_TCHAR _tchr ) const
{
	switch( TCHAR(_tchr) )
	{
	case _T(' '):
	case _T('\t'):
	case _T('\r'):
	case _T('\n'):
		return true;
	default:
		return false;
	}
}

void CExtFormulaAlphabet::ParsePassEmptySpace( __EXT_MFC_SAFE_LPCTSTR & str ) const
{
	if( LPCTSTR(str) == NULL )
		return;
LPCTSTR & strWalk = ( LPCTSTR & ) ( LPVOID & ) str;
	for( ; (*strWalk) != _T('\0'); strWalk++ )
	{
		TCHAR _tchr = (*strWalk);
		if( ! ParseCheckEmptySpaceChar( _tchr ) )
			break;
	}
}

CPoint CExtFormulaAlphabet::ParseCellName( __EXT_MFC_SAFE_LPCTSTR & str ) const
{
CPoint pt( -1L, -1L );
	if( LPCTSTR(str) == NULL )
		return pt;
	if( str[0] == _T('\0') )
		return pt;
	ParsePassEmptySpace( str );
LONG nAlphaIndex = ParseStrPartAlpha( str );
	if( nAlphaIndex < 0L )
		return pt;
LONG nNumIndex = ParseStrPartNum( str );
	if( nNumIndex < 0L )
		return pt;
	if( GetInverseNamingMode() )
	{
		pt.y = nAlphaIndex;
		pt.x = nNumIndex;
	}
	else
	{
		pt.x = nAlphaIndex;
		pt.y = nNumIndex;
	}
	return pt;
}

CRect CExtFormulaAlphabet::ParseRangeName(
	__EXT_MFC_SAFE_LPCTSTR & strLT,
	__EXT_MFC_SAFE_LPCTSTR & strRB,
	bool * p_bErrorInLT, // = NULL
	bool * p_bErrorInRB, // = NULL
	const CExtFormulaDataManager * pFDM // = NULL // if not NULL - use data provider for getting dimensions and generating full row / full column range names
	) const
{
	if( p_bErrorInLT != NULL )
		(*p_bErrorInLT) = false;
	if( p_bErrorInRB != NULL )
		(*p_bErrorInRB) = false;
CRect rcRange( -1L, -1L, -1L, -1L );
	if( LPCTSTR(strLT) == NULL )
	{
		if( p_bErrorInLT != NULL )
			(*p_bErrorInLT) = true;
		return rcRange;
	}
	if( strLT[0] == _T('\0') )
	{
		if( p_bErrorInLT != NULL )
			(*p_bErrorInLT) = true;
		return rcRange;
	}
	if( LPCTSTR(strRB) == NULL )
	{
		if( p_bErrorInRB != NULL )
			(*p_bErrorInRB) = true;
		return rcRange;
	}
	if( strRB[0] == _T('\0') )
	{
		if( p_bErrorInRB != NULL )
			(*p_bErrorInRB) = true;
		return rcRange;
	}
__EXT_MFC_SAFE_LPCTSTR strLTsaved = strLT, strRBsaved = strRB; // remember strings for searching full row / full column ranges on the second pass
CPoint pt0 = ParseCellName( strLT );
	if( pt0.x >= 0L && pt0.y >= 0L )
	{
		CPoint pt1 = ParseCellName( strRB );
		if( pt1.x >= 0L && pt1.y >= 0L )
			rcRange.SetRect( pt0, pt1 );
		else if( p_bErrorInRB != NULL )
			(*p_bErrorInRB) = true;
	} // if( pt0.x >= 0L && pt0.y >= 0L )
	else if( pFDM != NULL )
	{
		// the second pass
		const CExtGridDataProvider & _DP = pFDM->OnQueryDataProvider();
		ULONG nColCountReserved = 0L, nRowCountReserved = 0L;
		_DP.CacheReservedCountsGet( &nColCountReserved, &nRowCountReserved );
		LONG nMaxColumn = _DP.ColumnCountGet() - LONG(nColCountReserved) - 1L; // GetMaxAlphaStrVal();
		__EXT_DEBUG_GRID_ASSERT( nMaxColumn >= 0L );
		LONG nMaxRow = _DP.RowCountGet() - LONG(nRowCountReserved) - 1L; // GetMaxNumStrVal();
		__EXT_DEBUG_GRID_ASSERT( nMaxRow >= 0L );
		__EXT_MFC_SAFE_LPCTSTR str0 = LPCTSTR(strLTsaved), str1 = LPCTSTR(strRBsaved);
		ParsePassEmptySpace( str0 );
		LONG nIndexFrom = ParseStrPartAlpha( str0 ), nIndexTo = -1L;
		if( nIndexFrom >= 0L )
		{
			ParsePassEmptySpace( str1 );
			nIndexTo = ParseStrPartAlpha( str1 );
			if( nIndexTo >= 0L)
			{
				if( GetInverseNamingMode() )
					rcRange.SetRect( 0L, nIndexFrom, nMaxColumn, nIndexTo );
				else
					rcRange.SetRect( nIndexFrom, 0L, nIndexTo, nMaxRow );
			} // if( nIndexTo >= 0L)
			else if( p_bErrorInRB != NULL )
				(*p_bErrorInRB) = true;
		} // if( nIndexFrom >= 0L )
		else
		{
			str0 = strLTsaved; ParsePassEmptySpace( str0 );
			nIndexFrom = ParseStrPartNum( str0 );
			if( nIndexFrom >= 0L )
			{
				ParsePassEmptySpace( str1 );
				nIndexTo = ParseStrPartNum( str1 );
				if( nIndexTo >= 0L)
				{
					if( GetInverseNamingMode() )
						rcRange.SetRect( nIndexFrom, 0L, nIndexTo, nMaxRow );
					else
						rcRange.SetRect( 0L, nIndexFrom, nMaxColumn, nIndexTo );
				} // if( nIndexTo >= 0L)
				else if( p_bErrorInRB != NULL )
					(*p_bErrorInRB) = true;
			} // if( nIndexFrom >= 0L )
			else if( p_bErrorInLT != NULL )
				(*p_bErrorInLT) = true;
		} // else from if( nIndexFrom >= 0L )
	} // else if( pFDM != NULL )
	else if( p_bErrorInLT != NULL )
		(*p_bErrorInLT) = true;
	return rcRange;
}

CPoint CExtFormulaAlphabet::GetMaxValue() const
{
CPoint pt( 0, 0 );
LONG nAlphaIndex = GetMaxAlphaStrVal();
LONG nNumIndex = GetMaxNumStrVal();
	if( GetInverseNamingMode() )
	{
		pt.y = nAlphaIndex;
		pt.x = nNumIndex;
	}
	else
	{
		pt.x = nAlphaIndex;
		pt.y = nNumIndex;
	}
	return pt;
}

bool CExtFormulaAlphabet::GetInverseNamingMode() const
{
	return false;
}

CExtSafeString CExtFormulaAlphabet::GetAlphaStr( LONG nVal ) const
{
CExtSafeString str;
	if( nVal < 0 )
		return str;
LONG nMaxVal = GetMaxAlphaStrVal();
	if( nVal > nMaxVal )
		return str;
LONG nAlphabetSize = GetAlphabetSize();
LPCTSTR pAA = (LPCTSTR) OnQueryAlphabetArray();
	for( ; true; )
	{
		LONG nCharIndex = nVal % nAlphabetSize;
		str.Insert( 0, pAA[ nCharIndex ] );
		nVal /= nAlphabetSize;
		if( nVal == 0 )
			break;
	}
	return str;
}

CExtSafeString CExtFormulaAlphabet::GetLocationStr( LONG nColNo, LONG nRowNo ) const
{
CExtSafeString str1, str2;
	if( GetInverseNamingMode() )
	{
		str1 = GetAlphaStr( nRowNo );
		str2.Format( _T("%d"), INT(nColNo+1) );
	}
	else
	{
		str1 = GetAlphaStr( nColNo );
		str2.Format( _T("%d"), INT(nRowNo+1) );
	}
	str1 += str2;
	return str1;
}

CExtSafeString CExtFormulaAlphabet::GetRangeLocationStr(
	const RECT & _range,
	const CExtFormulaDataManager * pFDM // = NULL // if not NULL - use data provider for getting dimensions and generating full row / full column range names
	) const
{
CExtSafeString str0, str1, str2;
	if( pFDM != NULL && ( _range.left == 0L || _range.top == 0L ) )
	{
		const CExtGridDataProvider & _DP = pFDM->OnQueryDataProvider();
		ULONG nColCountReserved = 0L, nRowCountReserved = 0L;
		_DP.CacheReservedCountsGet( &nColCountReserved, &nRowCountReserved );
		if( _range.left == 0L )
		{
			LONG nMaxColumn = _DP.ColumnCountGet() - LONG(nColCountReserved) - 1L; // _FA.GetMaxAlphaStrVal();
			__EXT_DEBUG_GRID_ASSERT( nMaxColumn >= 0L );
			if( _range.right == nMaxColumn )
			{
				if( GetInverseNamingMode() )
				{
					str1 = GetAlphaStr( _range.top );
					str2 = GetAlphaStr( _range.bottom );
					str0 = str1 + _T(":") + str2;
				}
				else
					str0.Format( _T("%d:%d"), INT(_range.top+1), INT(_range.bottom+1) );
				return str0;
			} // if( _range.right == nMaxColumn )
		} // if( _range.left == 0L )
		if( _range.top == 0L )
		{
			LONG nMaxRow = _DP.RowCountGet() - LONG(nRowCountReserved) - 1L; // _FA.GetMaxNumStrVal();
			__EXT_DEBUG_GRID_ASSERT( nMaxRow >= 0L );
			if( _range.bottom == nMaxRow )
			{
				if( GetInverseNamingMode() )
					str0.Format( _T("%d:%d"), INT(_range.left+1), INT(_range.right+1) );
				else
				{
					str1 = GetAlphaStr( _range.left );
					str2 = GetAlphaStr( _range.right );
					str0 = str1 + _T(":") + str2;
				}
				return str0;
			} // if( _range.bottom == nMaxRow )
		} // if( _range.top == 0L )
	} // if( pFDM != NULL && ( _range.left == 0L || _range.top == 0L ) )
	str1 = GetLocationStr( _range.left, _range.top );
	str2 = GetLocationStr( _range.right, _range.bottom );
	str0 = str1 + _T(":") + str2;
	return str0;
}

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaInplaceTipWnd

IMPLEMENT_DYNAMIC( CExtFormulaInplaceTipWnd, CExtPopupMenuTipWnd );

CExtFormulaInplaceTipWnd::CExtFormulaInplaceTipWnd()
	: m_rcEditMargin( 1, 1, 1, 1 )
	, m_pWndFIE( NULL )
{
	m_bNoHideDetection = true;
	SetTipStyle( CExtPopupMenuTipWnd::__ETS_RECTANGLE_NO_ICON );
	m_crBold.cpMin = m_crBold.cpMax = 0;
}

CExtFormulaInplaceTipWnd::~CExtFormulaInplaceTipWnd()
{
}

BEGIN_MESSAGE_MAP( CExtFormulaInplaceTipWnd, CExtPopupMenuTipWnd )
	//{{AFX_MSG_MAP(CExtFormulaInplaceTipWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CRect CExtFormulaInplaceTipWnd::OnCalcEditRect()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
CRect rcEdit;
	GetClientRect( &rcEdit );
	rcEdit.DeflateRect( &m_rcEditMargin );
	return rcEdit;
}

void CExtFormulaInplaceTipWnd::SetText( __EXT_MFC_SAFE_LPCTSTR lpszText )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtPopupMenuTipWnd::SetText( lpszText );
	OnFormatText( LPCTSTR(lpszText) != NULL ? LPCTSTR(lpszText) : _T("") );
}

void CExtFormulaInplaceTipWnd::OnFormatText(
	__EXT_MFC_SAFE_LPCTSTR strText // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_wndEdit.GetSafeHwnd() == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pWndFIE );
	m_wndEdit.Rich_HideSelection( true, false );
CHARRANGE cr = { 0, -1 };
	if( strText != NULL )
	{
		m_wndEdit.Rich_SetSel( cr );
		m_wndEdit.ReplaceSel( strText );
	}
CHARFORMAT cfDefault;
	::memcpy( &cfDefault, &m_pWndFIE->m_cfDefault, sizeof(CHARFORMAT) );
	cfDefault.dwMask |= CFM_BOLD|CFM_ITALIC|CFM_UNDERLINE|CFM_STRIKEOUT;
	cfDefault.dwEffects &= ~(CFE_BOLD|CFE_ITALIC|CFE_UNDERLINE|CFE_STRIKEOUT);
	cfDefault.yHeight = 180;
LOGFONT _lf; ::memset( &_lf, 0, sizeof(LOGFONT) );
	PmBridge_GetPM()->m_FontNormal.GetLogFont( &_lf );

#if (! defined _RICHEDIT_VER )
		USES_CONVERSION;
		strcpy( cfDefault.szFaceName, T2CA(_lf.lfFaceName) );
#else
	#if ( _RICHEDIT_VER < 0x0200 )
		USES_CONVERSION;
		strcpy( cfDefault.szFaceName, T2CA(_lf.lfFaceName) );
	#else
		__EXT_MFC_STRCPY( cfDefault.szFaceName, sizeof(cfDefault.szFaceName)/sizeof(cfDefault.szFaceName[0]), _lf.lfFaceName );
	#endif
#endif

	m_wndEdit.Rich_SetSel( cr );
	m_wndEdit.SendMessage( EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&cfDefault );
	if( m_crBold.cpMin != m_crBold.cpMax )
	{
		m_wndEdit.Rich_SetSel( m_crBold );
		CHARFORMAT _cf;
		::memcpy( (void *)&_cf, (void *)&cfDefault, sizeof(CHARFORMAT) );
		_cf.dwMask |= CFM_BOLD;
		_cf.dwEffects |= CFE_BOLD;
		m_wndEdit.SendMessage( EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&_cf );
	}
	cr.cpMin = cr.cpMax = 0;
	m_wndEdit.Rich_SetSel( cr );
	m_wndEdit.Rich_HideSelection( false, false );
}

LRESULT CExtFormulaInplaceTipWnd::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
LRESULT lResult = CExtPopupMenuTipWnd::WindowProc( message, wParam, lParam );
	switch( message )
	{
	case WM_CREATE:
	{
		if( m_pWndFIE == NULL )
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return -1;
		}
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pWndFIE );
		CRect rcEdit = OnCalcEditRect();
		HWND hWnd =
			::CreateWindowEx(
				WS_EX_TRANSPARENT, _T("RICHEDIT"), _T(""), WS_CHILD|WS_VISIBLE|WS_DISABLED|WS_CLIPSIBLINGS|WS_CLIPCHILDREN|ES_LEFT|ES_READONLY,
				rcEdit.left, rcEdit.top, rcEdit.Width(), rcEdit.Height(), m_hWnd, (HMENU)NULL, ::AfxGetInstanceHandle(), NULL
				);
		if( hWnd == NULL )
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return -1;
		}
		__EXT_DEBUG_GRID_VERIFY( m_wndEdit.SubclassWindow( hWnd ) );
		m_wndEdit.m_bRichMode = true;
		__EXT_MFC_SAFE_LPCTSTR strText = GetText();
		SetText( strText ); // re-set text to editor
		m_wndEdit.Rich_SetBackgroundColor( FALSE, PmBridge_GetPM()->GetSysColor(COLOR_WINDOW) );
		if( m_fontAdvTip.GetSafeHandle() != NULL )
			m_fontAdvTip.DeleteObject();
		LOGFONT _lf;
		::memset( &_lf, 0, sizeof(LOGFONT) );
		g_PaintManager->m_FontBold.GetLogFont( &_lf );
		__EXT_DEBUG_GRID_VERIFY( m_fontAdvTip.CreateFontIndirect( &_lf ) );
	}
	break;
	case WM_SHOWWINDOW:
		if( wParam != 0 && m_wndEdit.GetSafeHwnd() != NULL )
			OnFormatText();
	break;
	case WM_SIZE:
		if( m_wndEdit.GetSafeHwnd() != NULL )
		{
			OnFormatText();
			CRect rcEdit = OnCalcEditRect();
			m_wndEdit.MoveWindow( &rcEdit );
		}
	break;
	case WM_DESTROY:
		if( m_wndEdit.GetSafeHwnd() != NULL )
			m_wndEdit.DestroyWindow();
		if( m_fontAdvTip.GetSafeHandle() != NULL )
			m_fontAdvTip.DeleteObject();
		m_crBold.cpMin = m_crBold.cpMax = 0;
	break;
	} // switch( message )
	return lResult;
}

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaInplaceListBox

IMPLEMENT_DYNAMIC( CExtFormulaInplaceListBox, CExtGridWnd );

CExtFormulaInplaceListBox::CExtFormulaInplaceListBox()
	: m_pWndFIE( NULL )
	, m_rcNcAreaMargins( 1, 1, 1, 1 )
	, m_clrNcArea( RGB(0,0,0) )
	, m_nRowCountMin( 1L )
	, m_nRowCountMax( 5L )
{
	m_nMouseActivateCode = MA_NOACTIVATE;
}

CExtFormulaInplaceListBox::~CExtFormulaInplaceListBox()
{
}

#ifdef _DEBUG
void CExtFormulaInplaceListBox::AssertValid() const
{
	CExtGridWnd::AssertValid();
	__EXT_DEBUG_GRID_ASSERT( m_nRowCountMin >= 1L );
	__EXT_DEBUG_GRID_ASSERT( m_nRowCountMax >= 1L );
	__EXT_DEBUG_GRID_ASSERT( m_nRowCountMin <= m_nRowCountMax );
	if( m_pWndFIE != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pWndFIE );
	}
}
void CExtFormulaInplaceListBox::Dump( CDumpContext & dc ) const
{
	CExtGridWnd::Dump( dc );
}
#endif // _DEBUG

BEGIN_MESSAGE_MAP( CExtFormulaInplaceListBox, CExtGridWnd )
	//{{AFX_MSG_MAP(CExtFormulaInplaceListBox)
	ON_WM_NCCALCSIZE()
	ON_WM_NCPAINT()
	//}}AFX_MSG_MAP
	__EXT_MFC_ON_WM_NCHITTEST()
END_MESSAGE_MAP()

void CExtFormulaInplaceListBox::HelpTip()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL || m_pWndFIE->GetSafeHwnd() == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pWndFIE );
CExtFormulaInplaceTipWnd * pWndIPT = m_pWndFIE->OnQueryIPT();
	if( pWndIPT == NULL )
		return;
	pWndIPT->m_crBold.cpMin = pWndIPT->m_crBold.cpMax = 0;
LONG nRowNo = -1L;
	if( ( GetStyle() & WS_VISIBLE ) != 0 )
		nRowNo = FocusGet().y;
	if( nRowNo >= 0 )
	{
		CExtGridCell * pCell = GridCellGet( 0L, nRowNo );
		__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
		if( pCell->IconIndexGet() == __EXT_FIPLB_ICON_INDEX_FX )
		{
			CExtFormulaFunction * pFS = (CExtFormulaFunction*)pCell->LParamGet();
			__EXT_DEBUG_GRID_ASSERT( pFS != NULL );
			CExtSafeString strTipText = pFS->GetFunctionDescription();
			if( ! strTipText.IsEmpty() )
			{
				if(		pWndIPT->GetSafeHwnd() != NULL
					&&	( pWndIPT->GetStyle() & WS_VISIBLE ) != 0
					&&	strTipText == LPCTSTR(pWndIPT->GetText())
					)
					return;
				pWndIPT->Hide();
				pWndIPT->SetText( LPCTSTR(strTipText) );
				CRect rcExcludeArea;
//				if( GridCellRectsGet( 0L, nRowNo, 0, 0, &rcExcludeArea ) )
				GetWindowRect( &rcExcludeArea ); // temp.
				{
//					CRect wr;
//					GetWindowRect( &wr );
							//ClientToScreen( &rcExcludeArea );
//					rcExcludeArea.left = wr.right;
//					rcExcludeArea.right = rcExcludeArea.left + 20;
//					rcExcludeArea.OffsetRect( 0, -rcExcludeArea.Height() );
//					pWndIPT->m_rcAlignment = rcExcludeArea;
					pWndIPT->OnFormatText();
					if( pWndIPT->Show( this, rcExcludeArea, true ) )
						return;
				}
			} // if( ! str.IsEmpty() )
		} // if( pCell->IconIndexGet() == __EXT_FIPLB_ICON_INDEX_FX )
	} // if( nRowNo >= 0 )
	pWndIPT->Hide();
}

LRESULT CExtFormulaInplaceListBox::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
LRESULT lResult = CExtGridWnd::WindowProc( message, wParam, lParam );
	switch( message )
	{
	case WM_SHOWWINDOW:
		PostMessage( (WM_USER+33) );
	break;
	case (WM_USER+33):
		HelpTip();
	break;
	}
	return lResult;
}

bool CExtFormulaInplaceListBox::OnSiwQueryFocusedControlState() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return true;
}

void CExtFormulaInplaceListBox::OnGbwFocusChanged(
	const POINT & ptOldFocus,
	const POINT & ptNewFocus
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtGridWnd::OnGbwFocusChanged( ptOldFocus, ptNewFocus );
	if( ptOldFocus.y != ptNewFocus.y )
		//HelpTip();
		PostMessage( (WM_USER+33) );
}

bool CExtFormulaInplaceListBox::Create( CExtFormulaInplaceEdit & wndFIE )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( m_pWndFIE == NULL );
	__EXT_DEBUG_GRID_ASSERT( m_hWnd == NULL );
	m_pWndFIE = &wndFIE;
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pWndFIE );
	__EXT_DEBUG_GRID_ASSERT( m_pWndFIE->GetSafeHwnd() );
CExtFormulaGridWnd * pWndFormulaGrid = STATIC_DOWNCAST( CExtFormulaGridWnd, (&m_pWndFIE->m_wndGrid) );
	__EXT_DEBUG_GRID_ASSERT_VALID( pWndFormulaGrid );
	__EXT_DEBUG_GRID_ASSERT( pWndFormulaGrid->GetSafeHwnd() );
DWORD dwFgStyle = pWndFormulaGrid->FgGetStyle();
	__EXT_DEBUG_GRID_ASSERT( ( dwFgStyle & __FGS_USE_AUTO_COMPLETE_LIST ) != 0 );
CWnd * pWndParent = wndFIE.GetTopLevelParent();
	if( pWndParent == NULL )
		pWndParent = CWnd::GetDesktopWindow();
CRect rc( 0, 0, 100, 300 );
	if( ! CExtGridWnd::Create( pWndParent, rc, 0L, 0L, WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN ) )
	{
		m_pWndFIE = NULL;
		return false;
	}
	ModifyStyleEx( 0L, WS_EX_TOPMOST );
	SiwModifyStyle( __ESIS_STH_NONE|__ESIS_STV_ITEM|__EGBS_SFB_FULL_ROWS|__EGBS_NO_HIDE_SELECTION, 0, false );
	SiwModifyStyleEx( __EGWS_EX_PM_COLORS|__EGWS_EX_USE_THEME_API|__EGBS_EX_CELL_EXPANDING, 0, false );
	BseModifyStyleEx( __EGBS_BSE_EX_PROPORTIONAL_COLUMN_WIDTHS, 0, false );
	OuterRowCountTopSet( 1L, false );
	OuterRowHeightSet( true, 0L, 0 );
	ColumnAdd( 1L, false );
CExtCmdIcon _icon;
	__EXT_DEBUG_GRID_VERIFY( _icon.m_bmpNormal.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_FORMULA_GRID_FX) ) );
	GridIconInsert( &_icon, __EXT_FIPLB_ICON_INDEX_FX );
	__EXT_DEBUG_GRID_VERIFY( _icon.m_bmpNormal.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_FORMULA_GRID_NR) ) );
	GridIconInsert( &_icon, __EXT_FIPLB_ICON_INDEX_NR );
CExtGridCellHeaderFilter * pHeaderFilterCell = STATIC_DOWNCAST( CExtGridCellHeaderFilter, GridCellGetOuterAtTop( 0L, 0L, RUNTIME_CLASS(CExtGridCellHeaderFilter) ) );
	__EXT_DEBUG_GRID_ASSERT_VALID( pHeaderFilterCell );
	pHeaderFilterCell->ExtentPercentSet( 1.0 );
CExtFormulaDataManager & _FDM = m_pWndFIE->OnQueryFDM();
CExtFormulaFunctionSet & _FFS = _FDM.OnQueryFormulaFunctionSet();
LONG nFunctionCount = ( ( dwFgStyle & __FGS_USE_AUTO_COMPLETE_LIST_BOX_FOR_FUNCTIONS ) == 0 ) ? 0L : _FFS.EnumGetCount();
LONG nNamedRangeCount = ( ( dwFgStyle & __FGS_USE_AUTO_COMPLETE_LIST_BOX_FOR_NAMED_RANGES ) == 0 ) ? 0L : _FDM.NamedRange_GetCount();
LONG nRowNo = 0L, nRowCount = nFunctionCount + nNamedRangeCount;
	RowAdd( nRowCount, false );
	if( nRowCount > 0L )
	{
		if( ( dwFgStyle & __FGS_USE_AUTO_COMPLETE_LIST_BOX_FOR_FUNCTIONS ) != 0 )
		{
			POSITION pos = _FFS.EnumStart();
			for( ; pos != NULL; nRowNo ++ )
			{
				CExtFormulaFunction * pFF = _FFS.EnumNext( pos );
				__EXT_DEBUG_GRID_ASSERT( pFF != NULL );
				CExtGridCell * pCell = GridCellGet( 0L, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellString) );
				__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
				pCell->TextSet( LPCTSTR( pFF->GetFunctionName() ) );
				pCell->LParamSet( LPARAM( pFF ) );
				pCell->IconIndexSet( __EXT_FIPLB_ICON_INDEX_FX );
			}
		}
		if( ( dwFgStyle & __FGS_USE_AUTO_COMPLETE_LIST_BOX_FOR_NAMED_RANGES ) != 0 )
		{
			POSITION pos = _FDM.NamedRange_GetStartPosition();
			for( ; pos != NULL; nRowNo ++ )
			{
				CExtSafeString strName;
				CExtGR2D * pRange = _FDM.NamedRange_GetNext( pos, strName );
				__EXT_DEBUG_GRID_ASSERT( pRange != NULL );
				CExtGridCell * pCell = GridCellGet( 0L, nRowNo, 0, 0, RUNTIME_CLASS(CExtGridCellString) );
				__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
				pCell->TextSet( LPCTSTR( strName ) );
				pCell->LParamSet( LPARAM( pRange ) );
				pCell->IconIndexSet( __EXT_FIPLB_ICON_INDEX_NR );
			}
		}
	} // if( nRowCount > 0L )
	OnSwUpdateScrollBars();
CExtGridDataSortOrder _gdso;
CExtGridDataSortOrder::ITEM_INFO _ii( 0L, true );
	_gdso.m_arrItems.Add( _ii );
	GridSortOrderSetup( false, _gdso );
	return true;
}

void CExtFormulaInplaceListBox::OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR * lpncsp )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bCalcValidRects;
CRect rcNc = lpncsp->rgrc[0];
 	rcNc.DeflateRect( &m_rcNcAreaMargins );
	::CopyRect( &lpncsp->rgrc[0], &rcNc );
}

void CExtFormulaInplaceListBox::OnNcPaint()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CWindowDC dcWindow( this );
CRect rcClient, rcBar;
	GetClientRect( rcClient );
	ClientToScreen( rcClient );
	GetWindowRect( rcBar );
	rcClient.OffsetRect( -rcBar.TopLeft() );
	rcBar.OffsetRect( -rcBar.TopLeft() );
	dcWindow.ExcludeClipRect(rcClient);
CExtMemoryDC dc( &dcWindow, &rcBar );
	dc.FillSolidRect( &rcBar, m_clrNcArea );
}

UINT CExtFormulaInplaceListBox::OnNcHitTest( CPoint point )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	point;
	return HTCLIENT;
}

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaInplaceEdit

IMPLEMENT_DYNAMIC( CExtFormulaInplaceEdit, CExtGridInplaceEdit );

BEGIN_MESSAGE_MAP( CExtFormulaInplaceEdit, CExtGridInplaceEdit )
	//{{AFX_MSG_MAP(CExtFormulaInplaceEdit)
	//}}AFX_MSG_MAP
	ON_MESSAGE( WM_SETTEXT, OnSetText )
	ON_CONTROL_REFLECT( EN_CHANGE, OnChange )
	ON_NOTIFY_REFLECT( EN_PROTECTED, OnProtected )
END_MESSAGE_MAP()

CExtFormulaInplaceEdit::CExtFormulaInplaceEdit(
	HWND hWndParentForEditor,
	CExtGridWnd & wndGrid,
	CExtGridCell & cell,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcInplaceControl,
	LONG nLastEditedColNo,
	LONG nLastEditedRowNo
	)
	: CExtGridInplaceEdit(
		hWndParentForEditor, wndGrid, cell, nVisibleColNo, nVisibleRowNo, nColNo, nRowNo, nColType, nRowType,
		rcCellExtra, rcCell, rcInplaceControl, nLastEditedColNo, nLastEditedRowNo
		)
	, m_pFormula( NULL )
	, m_bParserMode( false )
	, m_bGridFocusMode( false )
	, m_bFormulaRangeDndCreation( false )
	, m_bFormulaRangeDndModification( false )
	, m_nGridFocusTimerID( 777 )
	, m_nGridSelectionTrackerRunningShift( 0 )
{
	m_wndIPT.m_pWndFIE = this;

	m_crTracked.cpMin = m_crTracked.cpMax = 0;
	::memset( &m_cfDefault, 0, sizeof(CHARFORMAT) ); m_cfDefault.cbSize = sizeof(CHARFORMAT);
	m_cfDefault.dwEffects = CFE_PROTECTED; 
	m_cfDefault.dwMask = CFM_BOLD|CFM_ITALIC|CFM_UNDERLINE|CFM_STRIKEOUT|CFM_FACE|CFM_SIZE|CFM_CHARSET|CFM_PROTECTED|CFM_COLOR;
	//m_cfDefault.yHeight = 180;
	m_cfDefault.bCharSet = DEFAULT_CHARSET;
	m_cfDefault.crTextColor = PmBridge_GetPM()->GetSysColor( COLOR_WINDOWTEXT );

bool bFontMustBeDestroyed = false;
HFONT hCellFont = NULL;
	if( hWndParentForEditor != NULL && hWndParentForEditor != wndGrid.GetSafeHwnd() )
	{
		hCellFont = (HFONT)(PmBridge_GetPM()->m_FontNormal.GetSafeHandle());
		if( hCellFont == NULL )
		{
			hCellFont = (HFONT)::GetStockObject( DEFAULT_GUI_FONT );
			if( hCellFont != NULL )
				bFontMustBeDestroyed = true;
		}
	}
	if( hCellFont == NULL )
		hCellFont = cell.OnQueryCellFont( wndGrid, nVisibleColNo, nVisibleRowNo, nColNo, nRowNo, nColType, nRowType, 0, bFontMustBeDestroyed, 0 );
	if( hCellFont == NULL )
	{
		bFontMustBeDestroyed = false;
		hCellFont = (HFONT)wndGrid.OnSiwGetDefaultFont().GetSafeHandle();
	}
LOGFONT _lf; ::memset( &_lf, 0, sizeof(LOGFONT) );
	::GetObject( (HGDIOBJ)hCellFont, sizeof(LOGFONT), &_lf );
	if( _lf.lfWeight >= 700 )
		m_cfDefault.dwEffects |= CFE_BOLD;
	if( _lf.lfItalic )
		m_cfDefault.dwEffects |= CFE_ITALIC;
	if( _lf.lfUnderline )
		m_cfDefault.dwEffects |= CFE_UNDERLINE;
	if( _lf.lfStrikeOut )
		m_cfDefault.dwEffects |= CFE_STRIKEOUT;
CWindowDC dcDesktop( NULL );
	m_cfDefault.yHeight = ::MulDiv( _lf.lfHeight, 72 * 20, dcDesktop.GetDeviceCaps( LOGPIXELSY ) );
	m_cfDefault.yHeight = abs( m_cfDefault.yHeight );
	if( bFontMustBeDestroyed )
		::DeleteObject( hCellFont );

//LOGFONT _lf; ::memset( &_lf, 0, sizeof(LOGFONT) );
//	PmBridge_GetPM()->m_FontNormal.GetLogFont( &_lf );

#if (! defined _RICHEDIT_VER )
		USES_CONVERSION;
		strcpy( m_cfDefault.szFaceName, T2CA(_lf.lfFaceName) );
#else
	#if ( _RICHEDIT_VER < 0x0200 )
		USES_CONVERSION;
		strcpy( m_cfDefault.szFaceName, T2CA(_lf.lfFaceName) );
	#else
		__EXT_MFC_STRCPY( m_cfDefault.szFaceName, sizeof(m_cfDefault.szFaceName)/sizeof(m_cfDefault.szFaceName[0]), _lf.lfFaceName );
	#endif
#endif

	m_bForceSetSelToEnd = true;
}

CExtFormulaInplaceEdit::~CExtFormulaInplaceEdit()
{
	if( m_pFormula != NULL )
	{
		delete m_pFormula;
		m_pFormula = NULL;
	}
}

#ifdef _DEBUG
void CExtFormulaInplaceEdit::AssertValid() const
{
	CExtGridInplaceEdit::AssertValid();
}
void CExtFormulaInplaceEdit::Dump( CDumpContext & dc ) const
{
	CExtGridInplaceEdit::Dump( dc );
}
#endif // _DEBUG

CExtFormulaInplaceListBox * CExtFormulaInplaceEdit::OnQueryIPLB(
	bool nCreate // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return NULL;
	if( ! m_bRichMode )
		return NULL;
CExtFormulaGridWnd * pWndFormulaGrid = DYNAMIC_DOWNCAST( CExtFormulaGridWnd, (&m_wndGrid) );
	if(		pWndFormulaGrid == NULL
		||	( pWndFormulaGrid->FgGetStyle() & __FGS_USE_AUTO_COMPLETE_LIST ) == 0
		)
		return NULL;
	if( m_wndIPLB.GetSafeHwnd() != NULL )
		return (&m_wndIPLB);
	if( ! nCreate )
		return NULL;
	if( ! m_wndIPLB.Create( *this ) )
		return NULL;
	return (&m_wndIPLB);
}

CExtFormulaInplaceTipWnd * CExtFormulaInplaceEdit::OnQueryIPT()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return NULL;
	if( ! m_bRichMode )
		return NULL;
CExtFormulaGridWnd * pWndFormulaGrid = DYNAMIC_DOWNCAST( CExtFormulaGridWnd, (&m_wndGrid) );
	if(		pWndFormulaGrid == NULL
		||	( pWndFormulaGrid->FgGetStyle() & __FGS_USE_RICH_FORMULA_TIPS ) == 0
		)
		return NULL;
	return (&m_wndIPT);
}

CExtFormulaGridCell & CExtFormulaInplaceEdit::OnQueryFormulaCell()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtFormulaGridCell * pCell = DYNAMIC_DOWNCAST( CExtFormulaGridCell, (&m_cell) );
	if( pCell == NULL )
		::AfxThrowUserException();
	return (*pCell);
}

const CExtFormulaGridCell & CExtFormulaInplaceEdit::OnQueryFormulaCell() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ( const_cast < CExtFormulaInplaceEdit * > ( this ) ) -> OnQueryFormulaCell();
}

CExtFormulaGridWnd & CExtFormulaInplaceEdit::OnQueryFormulaGridWnd()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtFormulaGridWnd * pWndFormulaGrid = DYNAMIC_DOWNCAST( CExtFormulaGridWnd, (&m_wndGrid) );
	if( pWndFormulaGrid == NULL )
		::AfxThrowUserException();
	return (*pWndFormulaGrid);
}

const CExtFormulaGridWnd & CExtFormulaInplaceEdit::OnQueryFormulaGridWnd() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ( const_cast < CExtFormulaInplaceEdit * > ( this ) ) -> OnQueryFormulaGridWnd();
}

CExtFormulaDataManager & CExtFormulaInplaceEdit::OnQueryFDM()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return OnQueryFormulaGridWnd().OnGridQueryFormulaDataManager();
}

const CExtFormulaDataManager & CExtFormulaInplaceEdit::OnQueryFDM() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ( const_cast < CExtFormulaInplaceEdit * > ( this ) ) -> OnQueryFDM();
}

void CExtFormulaInplaceEdit::OnFormatFormulaText()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this ); __EXT_DEBUG_GRID_ASSERT( ::IsWindow( m_hWnd ) ); __EXT_DEBUG_GRID_ASSERT( m_bRichMode );
CExtFormulaDataManager & _FDM = OnQueryFDM();
CExtGR2D rangeA, rangeB;
	_FDM.EmptyEditedLists();
	if( m_pFormula != NULL )
	{
		m_pFormula->UnionInplaceHighlightedRange( _FDM, rangeA );
		delete m_pFormula;
		m_pFormula = NULL;
	}
CExtSafeString strFormulaText = Rich_PlainText_Get();
CExtFormulaParser & _FormulaParser = _FDM.OnQueryFormulaParser();
	_FormulaParser.Init( strFormulaText );
	if( _FormulaParser.Parse() )
	{
		__EXT_DEBUG_GRID_ASSERT( _FormulaParser.m_pFormulaRoot != NULL );
		m_pFormula = _FormulaParser.m_pFormulaRoot;
		_FormulaParser.m_pFormulaRoot = NULL;
	}
	else
	{
		CExtSafeString str2 = strFormulaText += _T(")"); // the most common syntax error
		_FormulaParser.Init( str2 );
		if( _FormulaParser.Parse() )
		{
			__EXT_DEBUG_GRID_ASSERT( _FormulaParser.m_pFormulaRoot != NULL );
			m_pFormula = _FormulaParser.m_pFormulaRoot;
			_FormulaParser.m_pFormulaRoot = NULL;
		}
		else
		{
			str2 = strFormulaText;
			INT nLen = INT( str2.GetLength() );
			for( ; nLen > 1; nLen = INT( str2.GetLength() ) )
			{
				_FormulaParser.Init( str2 );
				if( _FormulaParser.Parse() )
				{
					__EXT_DEBUG_GRID_ASSERT( _FormulaParser.m_pFormulaRoot != NULL );
					m_pFormula = _FormulaParser.m_pFormulaRoot;
					_FormulaParser.m_pFormulaRoot = NULL;
					break;
				}
				str2 = str2.Left( nLen - 1 );
			}
			if( m_pFormula == NULL )
			{
				str2 = strFormulaText + _T("()"); // also common syntax error
				_FormulaParser.Init( str2 );
				if( _FormulaParser.Parse() )
				{
					__EXT_DEBUG_GRID_ASSERT( _FormulaParser.m_pFormulaRoot != NULL );
					m_pFormula = _FormulaParser.m_pFormulaRoot;
					_FormulaParser.m_pFormulaRoot = NULL;
				}
				else
				{
					str2 = strFormulaText + _T("\""); // also common syntax error
					_FormulaParser.Init( str2 );
					if( _FormulaParser.Parse() )
					{
						__EXT_DEBUG_GRID_ASSERT( _FormulaParser.m_pFormulaRoot != NULL );
						m_pFormula = _FormulaParser.m_pFormulaRoot;
						_FormulaParser.m_pFormulaRoot = NULL;
					}
				}
			}
		}
	}
	m_bParserMode = true;
CHARRANGE crSaved;
	Rich_GetSel( crSaved );
	::memcpy( &m_crSavedBeforeFormat, &crSaved, sizeof(CHARRANGE) );
	Rich_HideSelection( true, false );
	OnFormatFormulaRange( 0, Rich_GetTextLength(), false, false, PmBridge_GetPM()->GetSysColor( COLOR_WINDOWTEXT ) );
	_FDM.m_nRangeWalkIndex = 0;
	if( m_pFormula != NULL )
	{
		m_pFormula->FormatFormulaInplaceEdit( *this );
		m_pFormula->UnionInplaceHighlightedRange( _FDM, rangeB );
	}
	Rich_SetSel( crSaved );
	Rich_HideSelection( false, false );
	m_bParserMode = false;
	rangeB ^= rangeA;
	if( ! rangeB.IsEmpty() )
	{
		CExtFormulaGridWnd & _wndFG = OnQueryFormulaGridWnd();
		_wndFG.OnSwInvalidate( true );
		//_wndFG.OnSwUpdateWindow();
	}
	UpdateIPT( false );
}

void CExtFormulaInplaceEdit::OnFormatFormulaCase( INT nStart, INT nEnd )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this ); __EXT_DEBUG_GRID_ASSERT( ::IsWindow( m_hWnd ) ); __EXT_DEBUG_GRID_ASSERT( m_bRichMode );
	Rich_SetSel( nStart, nEnd );
CExtSafeString strCurrent = Rich_GetSelText();
CExtSafeString strValid = strCurrent;
	strValid.MakeUpper();
	if( strValid != strCurrent )
		ReplaceSel( LPCTSTR(strValid), FALSE );
}

void CExtFormulaInplaceEdit::OnFormatFormulaRange( INT nStart, INT nEnd, bool bBold, bool bItalic, COLORREF clr )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this ); __EXT_DEBUG_GRID_ASSERT( ::IsWindow( m_hWnd ) ); __EXT_DEBUG_GRID_ASSERT( m_bRichMode );
	if( nStart >= nEnd )
		return;
	Rich_SetSel( nStart, nEnd );
CHARFORMAT _cf;
	::memcpy( (void *)&_cf, (void *)&m_cfDefault, sizeof(CHARFORMAT) );
	if( bBold )
		_cf.dwEffects |= CFE_BOLD;
	else
		_cf.dwEffects &= ~CFE_BOLD;
	if( bItalic )
		_cf.dwEffects |= CFE_ITALIC;
	else
		_cf.dwEffects &= ~CFE_ITALIC;
	_cf.crTextColor = clr;
	_cf.dwMask = CFM_BOLD|CFM_ITALIC|CFM_FACE|CFM_SIZE|CFM_CHARSET|CFM_COLOR;
	::SendMessage( m_hWnd, EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&_cf );
}

LRESULT CExtFormulaInplaceEdit::OnSetText( WPARAM wParam, LPARAM lParam )
{
	wParam;
	lParam;
CExtFormulaInplaceListBox * pWndIPLB = OnQueryIPLB( false );
	if( pWndIPLB != NULL && ( pWndIPLB->GetStyle() & WS_VISIBLE ) != 0 )
		pWndIPLB->ShowWindow( SW_HIDE );
LRESULT lResult = Default();
	if( m_bRichMode )
		OnFormatFormulaText();
	return lResult;	
}

void CExtFormulaInplaceEdit::UpdateIPT( bool bDelay )
{
	if( ! m_bRichMode )
		return;
	if( bDelay )
	{
		PostMessage( (WM_USER+22) );
		return;
	}
	if( m_pFormula == NULL )
		return;
	if( m_bParserMode )
		return;
CExtFormulaInplaceTipWnd * pWndIPT = OnQueryIPT();
	if( pWndIPT == NULL )
		return;
bool bHide = true;
	if( ! m_bGridFocusMode )
	{
		CHARRANGE cr = { -1, -1 };
		Rich_GetSel( cr );
		INT nParameterIndex = -1;
		CExtFormulaItem * pFI = m_pFormula->FindFunction( INT(cr.cpMax), nParameterIndex );
		if( pFI != NULL && nParameterIndex >= 0 )
		{
			CExtSafeString strFunctionName = LPCTSTR(pFI->m_strName);
			CExtFormulaDataManager & _FDM = OnQueryFDM();
			CExtFormulaFunctionSet & _FFS = _FDM.OnQueryFormulaFunctionSet();
			CExtFormulaFunction * pFS = _FFS.GetFunction( LPCTSTR(strFunctionName) );
			if( pFS == NULL )
			{
				strFunctionName.MakeUpper();
				pFS = _FFS.GetFunction( LPCTSTR(strFunctionName) );
				if( pFS == NULL )
				{
					strFunctionName.MakeLower();
					pFS = _FFS.GetFunction( LPCTSTR(strFunctionName) );
				}
			} // if( pFS == NULL )
			if( pFS != NULL )
			{
				pWndIPT->m_crBold.cpMin = pWndIPT->m_crBold.cpMax = 0;
				INT nFormatIndex, nDrawIndex, nDrawCount = nParameterIndex, nEnteredCount = pFI->GetChildCount();
				INT nParmCountMax = pFS->GetParameterCountMax();
				if( nParmCountMax < 0 )
					nDrawCount ++;
				else
					nDrawCount = nParmCountMax;
				INT nParmCountMin = pFS->GetParameterCountMin();
				if( nParmCountMin >= 0 && nDrawCount < nParmCountMin )
					nDrawCount = nParmCountMin;
				if( nDrawCount < nEnteredCount )
				{
					nDrawCount = nEnteredCount;
					if( nParmCountMax > 0 && nDrawCount > nParmCountMax )
						nDrawCount = nParmCountMax;
				}
				CExtSafeString strTipText = _T("");
				if( nParameterIndex == 0 )
					//strTipText += _T("     >>>");
					pWndIPT->m_crBold.cpMin = LONG( _tcslen( strTipText ) );
				strTipText += LPCTSTR(strFunctionName);
				if( nParameterIndex == 0 )
					//strTipText += _T("<<<     ");
					pWndIPT->m_crBold.cpMax = LONG( _tcslen( strTipText ) );
				strTipText += _T("(");
				if( nDrawCount > 0 )
					strTipText += _T(" ");
				nFormatIndex = 0;
				if( nParmCountMin > 0 )
					nFormatIndex = - nParmCountMin + 2;
				for( nDrawIndex = 0; nDrawIndex < nDrawCount; nDrawIndex ++, nFormatIndex ++ )
				{
					if( nDrawIndex != 0 )
						strTipText += _T(", ");
					CExtSafeString strAbr = pFS->GetParameterAbreviationFormat( nDrawIndex + 1 );
					CExtSafeString strParmName;
					strParmName.Format( ( ! strAbr.IsEmpty() ) ? LPCTSTR(strAbr) : _T("parm%d"), /*nDrawIndex + 1*/ nFormatIndex );
					if( nParameterIndex > 0 && nDrawIndex == ( nParameterIndex - 1 ) )
						//strTipText += _T("     >>>");
						pWndIPT->m_crBold.cpMin = LONG( _tcslen( strTipText ) );
					if( nParmCountMin >= 0 && nDrawIndex >= nParmCountMin )
						strTipText += _T("[");
					strTipText += LPCTSTR(strParmName);
					if( nParmCountMin >= 0 && nDrawIndex >= nParmCountMin )
						strTipText += _T("]");
					if( nParameterIndex > 0 && nDrawIndex == ( nParameterIndex - 1 ) )
						//strTipText += _T("<<<     ");
						pWndIPT->m_crBold.cpMax = LONG( _tcslen( strTipText ) );
				} // for( nDrawIndex = 0; nDrawIndex < nDrawCount; nDrawIndex ++ )
				if( nParmCountMax < 0 )
				{
					if( nDrawCount > 0 )
						strTipText += _T(", ");
					strTipText += _T("...");
				}
				if( nDrawCount > 0 )
					strTipText += _T(" ");
				strTipText += _T(")");
				if(		pWndIPT->GetSafeHwnd() != NULL
					&&	( pWndIPT->GetStyle() & WS_VISIBLE ) != 0
					&&	strTipText == LPCTSTR(pWndIPT->GetText())
					)
				{
					bHide = false;
					pWndIPT->OnFormatText();
				}
				else
				{
					pWndIPT->Hide();
					CRect rcExcludeArea;
					GetWindowRect( &rcExcludeArea );
					pWndIPT->SetText( LPCTSTR(strTipText) );
					pWndIPT->OnFormatText();
					bHide = ! pWndIPT->Show( this, rcExcludeArea, true );
				}
			} // if( pFS != NULL )
		} // if( pFI != NULL && nParameterIndex >= 0 )
	}
	if( bHide )
		pWndIPT->Hide();
}

void CExtFormulaInplaceEdit::OnChange()
{
	if( ! m_bRichMode )
		return;
	if( m_bParserMode )
		return;
CHARRANGE crCurSel; 
	Rich_GetSel( crCurSel );
	OnFormatFormulaText();
CExtFormulaInplaceListBox * pWndIPLB = OnQueryIPLB();
	if( pWndIPLB != NULL )
	{
//CExtFormulaDataManager & _FDM = OnQueryFDM();
		CHARRANGE cr = { -1, -1 };
		CExtFormulaItem * pFI = NULL;
		if(		m_pFormula != NULL
			&&	( ! m_bGridFocusMode )
			&&	( ! m_bFormulaRangeDndCreation )
			&&	( ! m_bFormulaRangeDndModification )
			)
		{
			Rich_GetSel( cr );
			if( cr.cpMin == cr.cpMax )
			{
				pFI = m_pFormula->FindItem( -1, cr.cpMax );
				if( pFI != NULL )
				{
					if(		pFI->m_eType != CExtFormulaItem::__FORMULA_ATOM_IDENTIFIER
						&&	pFI->m_eType != CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL
						)
						pFI = NULL;
					else
					{
						CExtGridCellHeaderFilter * pHeaderFilterCell = STATIC_DOWNCAST( CExtGridCellHeaderFilter, pWndIPLB->GridCellGetOuterAtTop( 0L, 0L ) );
						__EXT_DEBUG_GRID_ASSERT_VALID( pHeaderFilterCell );
						pHeaderFilterCell->m_strTextFilterBeginsWith = LPCTSTR( pFI->m_strName );
						pHeaderFilterCell->m_strTextFilterBeginsWith.MakeUpper();
						pWndIPLB->OnGridFilterUpdateForRows( 0L, -1L, true, pHeaderFilterCell, true );
						pWndIPLB->OnSwUpdateScrollBars();
						if( pWndIPLB->RowCountGet() > 0L )
						{
							pWndIPLB->FocusSet( CPoint( 0L, 0L ) );
							CExtSafeString str;
							pWndIPLB->GridCellGet( 0L, 0L )->TextGet( str );
							if( str.CompareNoCase( LPCTSTR(pFI->m_strName) ) == 0 )
								pFI = NULL;
						}
						else
						{
							pWndIPLB->FocusUnset();
							pFI = NULL;
						}
					}
				}
			}
		}
		if( pFI != NULL )
		{
			INT nRowHeight = pWndIPLB->DefaultRowHeightGet();
			LONG nRowCountAdjustHeight = pWndIPLB->RowCountGet();
			if( nRowCountAdjustHeight < pWndIPLB->m_nRowCountMin )
				nRowCountAdjustHeight = pWndIPLB->m_nRowCountMin;
			if( nRowCountAdjustHeight > pWndIPLB->m_nRowCountMax )
				nRowCountAdjustHeight = pWndIPLB->m_nRowCountMax;
			INT nHeightDesired = INT( nRowHeight * nRowCountAdjustHeight ) + pWndIPLB->m_rcNcAreaMargins.top + pWndIPLB->m_rcNcAreaMargins.bottom + 1;
			LONG nTextPos = cr.cpMax - pFI->m_strName.GetLength();
			CRect wrEdit, wrIPLP;
			GetWindowRect( &wrEdit );
			pWndIPLB->GetWindowRect( &wrIPLP );
			wrIPLP.bottom = wrIPLP.top + nHeightDesired;
			CExtPaintManager::monitor_parms_t _mp;
			CExtPaintManager::stat_GetMonitorParms( _mp, wrEdit );
			CPoint point = Rich_GetCharPos( nTextPos );
			ClientToScreen( &point );
			wrIPLP.OffsetRect( point.x - wrIPLP.left, wrEdit.bottom - wrIPLP.top );
			CRect rcAlign = _mp.m_rcWorkArea;
			if( wrIPLP.right > rcAlign.right )
				wrIPLP.OffsetRect( rcAlign.right - wrIPLP.right, 0 );
			if( wrIPLP.left < rcAlign.left )
				wrIPLP.OffsetRect( rcAlign.left - wrIPLP.left, 0 );
			if( wrIPLP.bottom > rcAlign.bottom )
				wrIPLP.OffsetRect( 0, wrEdit.top - wrIPLP.top - wrIPLP.Height() );
			if( wrIPLP.bottom > rcAlign.bottom )
				wrIPLP.OffsetRect( 0, rcAlign.bottom - wrIPLP.bottom );
			if( wrIPLP.top < rcAlign.top )
				wrIPLP.OffsetRect( 0, rcAlign.top - wrIPLP.top );
			pWndIPLB->SetWindowPos( NULL, wrIPLP.left, wrIPLP.top, wrIPLP.Width(), wrIPLP.Height(), SWP_NOACTIVATE|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_SHOWWINDOW );
			//if( ( pWndIPLB->GetStyle() & WS_VISIBLE ) == 0 )
			//	pWndIPLB->ShowWindow( SW_SHOWNOACTIVATE );
		} // if( pFI != NULL )
		else
		{
			if( ( pWndIPLB->GetStyle() & WS_VISIBLE ) != 0 )
				pWndIPLB->ShowWindow( SW_HIDE );
		} // if( pFI != NULL )
		pWndIPLB->HelpTip();
	} // if( pWndIPLB != NULL )
}

void CExtFormulaInplaceEdit::OnProtected( NMHDR * pNMHDR, LRESULT * pResult )
{
	if( ! m_bRichMode )
		return;
ENPROTECTED * pEP = (ENPROTECTED*)pNMHDR;
	if( pEP->msg != EM_SETCHARFORMAT )
		m_crTracked = pEP->chrg;
	(*pResult) = FALSE;
}

HWND CExtFormulaInplaceEdit::_OnCreateWindowHandleImpl(
    DWORD dwExStyle, LPCTSTR lpClassName, LPCTSTR lpWindowName, DWORD dwStyle,
    int X, int Y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
HWND hWnd = NULL;
CExtFormulaGridWnd * pWndFormulaGrid = DYNAMIC_DOWNCAST( CExtFormulaGridWnd, (&m_wndGrid) );
	if(		pWndFormulaGrid != NULL
		&&	( pWndFormulaGrid->FgGetStyle() & __FGS_USE_RICH_FORMULA_EDITOR ) != 0
		)
	{
		CExtSafeString strHelperWindowClassName = m_strHelperWindowClassName;
		m_strHelperWindowClassName = _T("RICHEDIT");
		hWnd =
			CExtGridInplaceEdit::_OnCreateWindowHandleImpl(
				dwExStyle, LPCTSTR(m_strHelperWindowClassName), lpWindowName, dwStyle,
				X, Y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam
				);
		if( hWnd != NULL )
		{
			m_bRichMode = true;
			return hWnd;
		}
		m_strHelperWindowClassName = strHelperWindowClassName;
	}
	m_bRichMode = false;
	hWnd =
		CExtGridInplaceEdit::_OnCreateWindowHandleImpl(
			dwExStyle, lpClassName, lpWindowName, dwStyle,
			X, Y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam
			);
	return hWnd;
}

void CExtFormulaInplaceEdit::PreSubclassWindow()
{
	CExtGridInplaceEdit::PreSubclassWindow();
	if( ! m_bRichMode )
		return;
PARAFORMAT _pf; ::memset( &_pf, 0, sizeof(PARAFORMAT) ); _pf.cbSize = sizeof(PARAFORMAT);
	_pf.dwMask = PFM_TABSTOPS;
	_pf.cTabCount = MAX_TAB_STOPS;
INT nTabIndex;
	for( nTabIndex = 0 ; nTabIndex < _pf.cTabCount; nTabIndex++ )
		_pf.rgxTabs[ nTabIndex ] = ( nTabIndex + 1 ) * 1440 / 5 ;
	::SendMessage( m_hWnd, EM_SETPARAFORMAT, 0, (LPARAM)&_pf );
	::SendMessage( m_hWnd, EM_SETCHARFORMAT, SCF_DEFAULT, LPARAM(&m_cfDefault) );
	Rich_SetSel( 0, -1 );
	::SendMessage( m_hWnd, EM_SETCHARFORMAT, SCF_SELECTION, LPARAM(&m_cfDefault) );
	Rich_SetSel( 0, 0 );
	::SendMessage( m_hWnd, EM_SETEVENTMASK, 0, ENM_CHANGE|ENM_SELCHANGE|ENM_PROTECTED );
	::SendMessage( m_hWnd, EM_SETOPTIONS, WPARAM(ECOOP_AND), LPARAM(ECO_AUTOWORDSELECTION|ECO_AUTOHSCROLL|ECO_NOHIDESEL) );

	OnQueryFDM().EmptyEditedLists();
}

CExtFormulaItem * CExtFormulaInplaceEdit::_GridFocusMode_Check(
	bool bRedrawTracker // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
	if( ! m_bGridFocusMode )
		return NULL ;
CExtFormulaDataManager & _FDM = OnQueryFDM();
CExtFormulaItem * pFI = NULL;
LONG nGridSelectionTrackerShiftSize = 0L;
	if(		( _FDM.m_crTracker.cpMin < 0L || _FDM.m_crTracker.cpMax < 0L )
		||	m_pFormula == NULL
		||	( pFI = m_pFormula->FindRangeItem( INT(_FDM.m_crTracker.cpMin), INT(_FDM.m_crTracker.cpMax) ) ) == NULL
		||	pFI->m_nScanPosStart < 0L
		||	pFI->m_nScanPosEnd < 0L
		||	pFI->m_rcEditedRange.left < 0L
		||	( nGridSelectionTrackerShiftSize = _FDM. m_nGridSelectionTrackerLength0 + _FDM. m_nGridSelectionTrackerLength1 ) <= 0L
		)
	{
		_GridFocusMode_Cancel();
		return NULL;
	}
	else if( bRedrawTracker )
	{
		__EXT_DEBUG_GRID_ASSERT( pFI->m_rcEditedRange.right >= 0L );
		__EXT_DEBUG_GRID_ASSERT( pFI->m_rcEditedRange.top >= 0L );
		__EXT_DEBUG_GRID_ASSERT( pFI->m_rcEditedRange.bottom >= 0L );
		m_nGridSelectionTrackerRunningShift ++;
		m_nGridSelectionTrackerRunningShift %= nGridSelectionTrackerShiftSize;
		CExtFormulaGridWnd & _wndFG = OnQueryFormulaGridWnd();
		_wndFG.OnSwInvalidate( true );
		//_wndFG.OnSwUpdateWindow();
	} // else if( bRedrawTracker )
	if( ! m_bGridFocusMode )
	{
		_GridFocusMode_Cancel();
		return NULL;
	}
	__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
	return pFI;
}

void CExtFormulaInplaceEdit::_GridFocusMode_Cancel()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
	m_bGridFocusMode = false;
	KillTimer( m_nGridFocusTimerID );
CExtFormulaDataManager & _FDM = OnQueryFDM();
	_FDM.m_crTracker.cpMin = _FDM.m_crTracker.cpMax = -1L;
CExtFormulaGridWnd & _wndFG = OnQueryFormulaGridWnd();
	_wndFG.OnSwInvalidate( true );
	//_wndFG.OnSwUpdateWindow();
}

BOOL CExtFormulaInplaceEdit::PreTranslateMessage( MSG * pMsg )
{
CExtFormulaInplaceListBox * pWndIPLB = OnQueryIPLB( false );
	if( pWndIPLB != NULL && ( pWndIPLB->GetStyle() & WS_VISIBLE ) != 0 && pWndIPLB->PreTranslateMessage( pMsg ) )
		return TRUE;
	return CExtGridInplaceEdit::PreTranslateMessage( pMsg );
}

LRESULT CExtFormulaInplaceEdit::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	switch( message )
	{
//	case EM_EXSETSEL:
//	case EM_SETSEL:
//		UpdateIPT( true );
//	break;
	case WM_DESTROY:
		if( m_wndIPLB.GetSafeHwnd() != NULL )
			m_wndIPLB.DestroyWindow();
		OnQueryFDM().EmptyEditedLists();
	break;
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_LBUTTONDBLCLK:
	case WM_MBUTTONDOWN:
	case WM_MBUTTONUP:
	case WM_MBUTTONDBLCLK:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
	case WM_RBUTTONDBLCLK:
	case WM_CHAR:
		if( m_bGridFocusMode )
			_GridFocusMode_Cancel();
		UpdateIPT( true );
	break;
	case WM_KEYDOWN:
	{
		CExtFormulaInplaceListBox * pWndIPLB = OnQueryIPLB( false );
		if( pWndIPLB != NULL && ( pWndIPLB->GetStyle() & WS_VISIBLE ) != 0 )
		{
			if( wParam == VK_UP || wParam == VK_DOWN || wParam == VK_PRIOR || wParam == VK_NEXT )
				return pWndIPLB->SendMessage( message, wParam, lParam );
			if( wParam == VK_LEFT || wParam == VK_RIGHT || wParam == VK_HOME || wParam == VK_END )
			{
				pWndIPLB->ShowWindow( SW_HIDE );
				UpdateIPT( true );
			}
			else if( wParam == VK_TAB )
			{
				LONG nRowNo = pWndIPLB->FocusGet().y;
				if( nRowNo >= 0L )
				{
					CExtSafeString str;
					CHARRANGE cr = { -1, -1 };
					CExtFormulaItem * pFI = NULL;
					if(		m_pFormula != NULL
						&&	( ! m_bGridFocusMode )
						&&	( ! m_bFormulaRangeDndCreation )
						&&	( ! m_bFormulaRangeDndModification )
						)
					{
						Rich_GetSel( cr );
						if( cr.cpMin == cr.cpMax )
						{
							pFI = m_pFormula->FindItem( -1, cr.cpMax );
							if( pFI != NULL )
							{
								if(		pFI->m_eType != CExtFormulaItem::__FORMULA_ATOM_IDENTIFIER
									&&	pFI->m_eType != CExtFormulaItem::__FORMULA_ATOM_FUNC_CALL
									)
									pFI = NULL;
								else
									pWndIPLB->GridCellGet( 0L, nRowNo ) -> TextGet( str );
							}
						}
					}
					pWndIPLB->ShowWindow( SW_HIDE );
					if( pFI != NULL )
					{
						cr.cpMin = cr.cpMax - pFI->m_strName.GetLength();
						LONG nTextLength = Rich_GetTextLength();
						if( pWndIPLB->GridCellGet( 0L, nRowNo ) -> IconIndexGet() == __EXT_FIPLB_ICON_INDEX_FX )
						{
							if( nTextLength > cr.cpMax )
							{
								CExtSafeString strText = Rich_PlainText_Get();
								LONG nWalk = cr.cpMax;
								for( ; nWalk < nTextLength; nWalk ++ )
								{
									TCHAR _tchr = strText.GetAt( nWalk );
									if( _tchr == _T('\t') || _tchr == _T('\r') || _tchr == _T('\n') || _tchr == _T(' ') )
										continue;
									if( _tchr != _T('(') )
										str += _T('(');
									break;
								}
							} // if( nTextLength > cr.cpMax )
							else
										str += _T('(');
						} // if( pWndIPLB->GridCellGet( 0L, nRowNo ) -> IconIndexGet() == __EXT_FIPLB_ICON_INDEX_FX )
						Rich_HideSelection( true, false );
						Rich_SetSel( cr );
						ReplaceSel( LPCTSTR(str), FALSE );
						Rich_HideSelection( false, false );
						for( MSG _msg; ::PeekMessage( &_msg, m_hWnd, WM_KEYFIRST, WM_KEYLAST, PM_REMOVE); );
						return 0L;
					} // if( pFI != NULL )
				} // if( nRowNo >= 0L )
			} // else if( wParam == VK_TAB )
		} // if( pWndIPLB != NULL && ( pWndIPLB->GetStyle() & WS_VISIBLE ) != 0 )
		CExtFormulaItem * pFI = _GridFocusMode_Check();
		if( m_bGridFocusMode )
		{
			__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
			bool bAlt = ( (::GetAsyncKeyState(VK_MENU)&0x8000) != 0 ) ? true : false;
			if( bAlt )
			{
				_GridFocusMode_Cancel();
				break;
			}
			bool bCtrl = ( (::GetAsyncKeyState(VK_CONTROL)&0x8000) != 0 ) ? true : false;
			bool bShift = ( (::GetAsyncKeyState(VK_SHIFT)&0x8000) != 0 ) ? true : false;
			bool bCtrlOnly = bCtrl && (!bAlt) && (!bShift);
			bool bShiftOnly = (!bCtrl) && (!bAlt) && bShift;
			//bool bCtrlShiftOnly = bCtrl && (!bAlt) && bShift;
			bool bNoModifiers = (!bCtrl) && (!bAlt) && (!bShift);
			if( bNoModifiers || bShiftOnly || bCtrlOnly )
			{
				CExtFormulaDataManager & _FDM = OnQueryFDM();
				CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
				CRect rcNewRange = pFI->m_rcEditedRangeNN;
				
				CExtFormulaGridWnd & _wndFG = OnQueryFormulaGridWnd();
				LONG nColCount = _wndFG.ColumnCountGet();
				LONG nRowCount = _wndFG.RowCountGet();
				LONG nMaxColNo = nColCount - 1L;
				LONG nMaxRowNo = nRowCount - 1L;
				switch( wParam )
				{
				case VK_LEFT:
					if( bCtrlOnly )
					{
						rcNewRange.OffsetRect( -1L, 0L );
						CRect rcNormalized = rcNewRange; CExtGR2D::Range_stat_Normalize( rcNormalized );
						if( rcNormalized.left < 0L )
							rcNewRange.OffsetRect( 1L, 0L );
					}
					else
						rcNewRange.right  --; rcNewRange.right  = max( rcNewRange.right,  0L ); rcNewRange.right  = min( rcNewRange.right,  nMaxColNo );
					if( bNoModifiers )
						{ rcNewRange.left = rcNewRange.right; rcNewRange.top = rcNewRange.bottom; }
				break;
				case VK_RIGHT:
					if( bCtrlOnly )
					{
						rcNewRange.OffsetRect( 1L, 0L );
						CRect rcNormalized = rcNewRange; CExtGR2D::Range_stat_Normalize( rcNormalized );
						if( rcNormalized.left < 0L )
							rcNewRange.OffsetRect( -1L, 0L );
					}
					else
						rcNewRange.right  ++; rcNewRange.right  = max( rcNewRange.right,  0L ); rcNewRange.right  = min( rcNewRange.right,  nMaxColNo );
					if( bNoModifiers )
						{ rcNewRange.left = rcNewRange.right; rcNewRange.top = rcNewRange.bottom; }
				break;
				case VK_UP:
					if( bCtrlOnly )
					{
						rcNewRange.OffsetRect( 0L, -1L );
						CRect rcNormalized = rcNewRange; CExtGR2D::Range_stat_Normalize( rcNormalized );
						if( rcNormalized.left < 0L )
							rcNewRange.OffsetRect( 0L, 1L );
					}
					else
						rcNewRange.bottom --; rcNewRange.bottom = max( rcNewRange.bottom, 0L ); rcNewRange.bottom = min( rcNewRange.bottom, nMaxRowNo );
					if( bNoModifiers )
						{ rcNewRange.left = rcNewRange.right; rcNewRange.top = rcNewRange.bottom; }
				break;
				case VK_DOWN:
					if( bCtrlOnly )
					{
						rcNewRange.OffsetRect( 0L, 1L );
						CRect rcNormalized = rcNewRange; CExtGR2D::Range_stat_Normalize( rcNormalized );
						if( rcNormalized.left < 0L )
							rcNewRange.OffsetRect( 0L, -1L );
					}
					else
						rcNewRange.bottom ++; rcNewRange.bottom = max( rcNewRange.bottom, 0L ); rcNewRange.bottom = min( rcNewRange.bottom, nMaxRowNo );
					if( bNoModifiers )
						{ rcNewRange.left = rcNewRange.right; rcNewRange.top = rcNewRange.bottom; }
				break;
				} // switch( wParam )
				if( rcNewRange != pFI->m_rcEditedRangeNN )
				{ // if need to update
// 					CExtGR2D::Range_stat_Normalize( rcNewRange );
					CExtSafeString strNewRange;
					if(		rcNewRange.left == rcNewRange.right
						&&	rcNewRange.top == rcNewRange.bottom
						)
						strNewRange = _FA.GetLocationStr( rcNewRange.left, rcNewRange.top );
					else
						strNewRange = _FA.GetRangeLocationStr( rcNewRange, &_FDM );
					Rich_HideSelection( true, false );
					Rich_SetSel( _FDM.m_crTracker );
					ReplaceSel( LPCTSTR(strNewRange), FALSE );
					_FDM.m_crTracker.cpMax = _FDM.m_crTracker.cpMin + strNewRange.GetLength();
					CHARRANGE crAfter;
					crAfter.cpMin = _FDM.m_crTracker.cpMax;
					crAfter.cpMax = _FDM.m_crTracker.cpMax;
					Rich_SetSel( crAfter );
					Rich_HideSelection( true, false );
					pFI = m_pFormula->FindRangeItem( INT(_FDM.m_crTracker.cpMin), INT(_FDM.m_crTracker.cpMax) );
					if( pFI == NULL )
						_GridFocusMode_Cancel();
					else
					{
						_wndFG.OnSwInvalidate( true );
						//_wndFG.OnSwUpdateWindow();
					}
					return 0L;
				} // if need to update
			} // if( bNoModifiers || bShiftOnly || bCtrlOnly )
		} // if( m_bGridFocusMode )
		else
		{
			if( wParam == VK_UP || wParam == VK_DOWN || wParam == VK_LEFT || wParam == VK_RIGHT || wParam == VK_HOME || wParam == VK_END || wParam == VK_PRIOR || wParam == VK_NEXT )
				UpdateIPT( true );
		}
	}
	break;
	case WM_TIMER:
		if( wParam == WPARAM(m_nGridFocusTimerID) )
		{
			_GridFocusMode_Check( true );
			return 0L;
		} // if( wParam == WPARAM(m_nGridFocusTimerID) )
	break;
	case (WM_USER+22):
		UpdateIPT( false );
		return 0L;
	} // switch( message )
LRESULT lResult = CExtGridInplaceEdit::WindowProc( message, wParam, lParam );
	return lResult;
}

void CExtFormulaInplaceEdit::PostNcDestroy()
{
	CExtGridInplaceEdit::PostNcDestroy();
}

void CExtFormulaInplaceEdit::OnFormulaRangeDndModification( const CExtGridHitTestInfo & htInfo )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( ( ! htInfo.IsHoverEmpty() ) );
	__EXT_DEBUG_GRID_ASSERT( htInfo.IsValidRect() );
CExtFormulaGridWnd * pWndFormulaGrid = DYNAMIC_DOWNCAST( CExtFormulaGridWnd, (&m_wndGrid) );
	if(		pWndFormulaGrid == NULL
		||	( pWndFormulaGrid->FgGetStyle() & __FGS_FORMULA_REFS_MASK ) != __FGS_FORMULA_REFS_MASK
		)
		return;
CExtFormulaGridWnd & _wndFG = (*pWndFormulaGrid);
CExtFormulaInplaceListBox * pWndIPLB = OnQueryIPLB( false );
	if( pWndIPLB != NULL && ( pWndIPLB->GetStyle() & WS_VISIBLE ) != 0 )
		pWndIPLB->ShowWindow( SW_HIDE );
CExtFormulaDataManager & _FDM = OnQueryFDM();
	if( m_bGridFocusMode )
		_GridFocusMode_Cancel();
CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
CExtFormulaItem * pFIHI = _FDM.m_pFormulaItemRangeInGridHT;
	__EXT_DEBUG_GRID_ASSERT( pFIHI != NULL );
	__EXT_DEBUG_GRID_ASSERT(
			pFIHI->m_eType == CExtFormulaItem::__FORMULA_ATOM_IDENTIFIER
		||	pFIHI->m_eType == CExtFormulaItem::__FORMULA_ATOM_CELL_RANGE
		);
CRect rcRangeDND = pFIHI->m_rcEditedRange;
	__EXT_DEBUG_GRID_ASSERT( rcRangeDND.left >= 0 && rcRangeDND.right >= 0 && rcRangeDND.top >= 0 && rcRangeDND.bottom >= 0 );
bool bLeft = _FDM.m_bInGridHtLeft;
bool bRight = _FDM.m_bInGridHtRight;
bool bTop = _FDM.m_bInGridHtTop;
bool bBottom = _FDM.m_bInGridHtBottom;
	__EXT_DEBUG_GRID_ASSERT( bLeft || bRight || bTop || bBottom );
HWND hWndGrid = _wndFG.GetSafeHwnd();
	__EXT_DEBUG_GRID_ASSERT( hWndGrid != NULL );
HWND hWndInplaceControl = GetSafeHwnd();
	__EXT_DEBUG_GRID_ASSERT( hWndInplaceControl != NULL );
CWinThread * pThread = ::AfxGetThread();
	__EXT_DEBUG_GRID_ASSERT_VALID( pThread );
bool bStopFlag = false;
	SetCapture();
	_FDM.m_crTracker.cpMin = _FDM.m_pFormulaItemRangeInGridHT->m_nScanPosStart;
	_FDM.m_crTracker.cpMax = _FDM.m_pFormulaItemRangeInGridHT->m_nScanPosEnd;
CExtGridHitTestInfo htLast = htInfo;
	for(	MSG msg;
				::IsWindow( hWndInplaceControl )
			&&	::IsWindow( hWndGrid )
			&&	(!bStopFlag)
			;
		)
	{ // drag-n-drop message loop
		if( ! ::PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) )
		{
			if(		hWndInplaceControl == NULL
				||	( ! ::IsWindow( hWndInplaceControl ) )
				||	( ! ::IsWindow( hWndGrid ) )
				||	bStopFlag
				)
				break;
			LONG nIdleCounter = 0L;
			if( CExtGridWnd::g_bEnableOnIdleCalls )
			{
				for( nIdleCounter = 0L; pThread->OnIdle(nIdleCounter); nIdleCounter ++ );
			}
			::WaitMessage();
			continue;
		} // if( ! ::PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) )
		switch( msg.message )
		{
		case WM_KILLFOCUS:
			if( msg.hwnd == hWndInplaceControl )
				bStopFlag = true;
		break;
		case WM_CANCELMODE:
		case WM_ACTIVATEAPP:
		case WM_SYSCOMMAND:
		case WM_SETTINGCHANGE:
		case WM_SYSCOLORCHANGE:
			bStopFlag = true;
		break;
		case WM_COMMAND:
			if(		(HIWORD(msg.wParam)) == 0
				||	(HIWORD(msg.wParam)) == 1
				)
				bStopFlag = true;
		break;
		case WM_CAPTURECHANGED:
			if( (HWND)msg.wParam != hWndInplaceControl )
				bStopFlag = true;
		break;

		case WM_SETCURSOR:
			{
				CPoint pt;
				if(		::GetCursorPos(&pt)
					&&	::WindowFromPoint( pt ) == hWndGrid
					)
				{
					::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
					continue;
				}
			}
		break;

		case WM_LBUTTONDOWN:
			::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
			if( msg.hwnd != hWndInplaceControl )
				bStopFlag = true;
			else
				continue;
		break;

		case WM_LBUTTONUP:
			::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
			bStopFlag = true;
		break;

		case WM_LBUTTONDBLCLK:
		case WM_RBUTTONUP:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONDBLCLK:
		case WM_MBUTTONUP:
		case WM_MBUTTONDOWN:
		case WM_MBUTTONDBLCLK:
		case WM_NCLBUTTONUP:
		case WM_NCLBUTTONDOWN:
		case WM_NCLBUTTONDBLCLK:
		case WM_NCRBUTTONUP:
		case WM_NCRBUTTONDOWN:
		case WM_NCRBUTTONDBLCLK:
		case WM_NCMBUTTONUP:
		case WM_NCMBUTTONDOWN:
		case WM_NCMBUTTONDBLCLK:
		case WM_CONTEXTMENU:
			bStopFlag = true;
		break;
		case WM_MOUSEMOVE:
			{
				::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
				CPoint pt;
				if( ! ::GetCursorPos( &pt ) )
				{
					bStopFlag = true;
					break;
				}
				_wndFG.ScreenToClient( &pt );
				CExtGridHitTestInfo htNew( pt );
				_wndFG.HitTest( htNew, false, false );
				if(		( ! htNew.IsHoverEmpty() )
					&&	htNew.IsValidRect()
					&&	htNew.GetInnerOuterTypeOfColumn() == 0
					&&	htNew.GetInnerOuterTypeOfRow() == 0
					&&	( htNew.m_nColNo != htLast.m_nColNo || htNew.m_nRowNo != htLast.m_nRowNo )
					)
				{
					//if( nColType == 0 && nRowType == 0 )
					//{
						CSize sizeJoin = _wndFG.OnGbwCellJoinQueryInfo( htNew.m_nColNo, htNew.m_nRowNo );
						if( sizeJoin.cx < 1L || sizeJoin.cy < 1L )
						{ // jump to join root
							htNew.m_nColNo += sizeJoin.cx;
							htNew.m_nRowNo += sizeJoin.cy;
							sizeJoin = _wndFG.OnGbwCellJoinQueryInfo( htNew.m_nColNo, htNew.m_nRowNo );
							__EXT_DEBUG_GRID_ASSERT( sizeJoin.cx >= 1L && sizeJoin.cy >= 1L );
							__EXT_DEBUG_GRID_ASSERT( sizeJoin.cx > 1L || sizeJoin.cy > 1L );
						} // jump to join root
						if( sizeJoin.cx > 1L || sizeJoin.cy > 1L )
						{ // check need to jump to right bottom join margins
							//if( rcRangeDND.right >= rcRangeDND.left )
							if( htNew.m_nColNo >= rcRangeDND.left )
								htNew.m_nColNo += sizeJoin.cx - 1L;
							//if( rcRangeDND.bottom >= rcRangeDND.top )
							if( htNew.m_nRowNo >= rcRangeDND.top )
								htNew.m_nRowNo += sizeJoin.cy - 1L;
						} // check need to jump to right bottom join margins
					//}
					CRect rcRangeNew = rcRangeDND;
					CPoint ptRangeShiftDND( 0, 0 );
					if(		( bLeft || bRight )
						&&	( ! ( bTop || bBottom ) )
						)
					{
						::SetCursor( ( _wndFG.m_hCursorFormulaRangeMove != NULL ) ? _wndFG.m_hCursorFormulaRangeMove : ::LoadCursor( NULL, IDC_SIZEALL /*IDC_SIZEWE*/ ) );
						ptRangeShiftDND.x = htNew.m_nColNo - htLast.m_nColNo;
						ptRangeShiftDND.y = htNew.m_nRowNo - htLast.m_nRowNo;
					}
					else
					if(		( bTop || bBottom )
						&&	( ! ( bLeft || bRight ) )
						)
					{
						::SetCursor( ( _wndFG.m_hCursorFormulaRangeMove != NULL ) ? _wndFG.m_hCursorFormulaRangeMove : ::LoadCursor( NULL, IDC_SIZEALL /*IDC_SIZENS*/ ) );
						ptRangeShiftDND.x = htNew.m_nColNo - htLast.m_nColNo;
						ptRangeShiftDND.y = htNew.m_nRowNo - htLast.m_nRowNo;
					}
					else
					if(		( bLeft && bTop )
						||	( bRight && bBottom )
						)
					{
						::SetCursor( ( _wndFG.m_hCursorFormulaRangeNWSE != NULL ) ? _wndFG.m_hCursorFormulaRangeNWSE : ::LoadCursor( NULL, IDC_SIZENWSE ) );
						if( bLeft && bTop )
						{
							rcRangeNew.left = htNew.m_nColNo;
							rcRangeNew.top = htNew.m_nRowNo;
						}
						else
						{
							rcRangeNew.right = htNew.m_nColNo;
							rcRangeNew.bottom = htNew.m_nRowNo;
						}
					}
					else
					if(		( bLeft && bBottom )
						||	( bRight && bTop )
						)
					{
						::SetCursor( ( _wndFG.m_hCursorFormulaRangeNESW != NULL ) ? _wndFG.m_hCursorFormulaRangeNESW : ::LoadCursor( NULL, IDC_SIZENESW ) );
						if( bLeft && bTop )
						{
							rcRangeNew.left = htNew.m_nColNo;
							rcRangeNew.bottom = htNew.m_nRowNo;
						}
						else
						{
							rcRangeNew.right = htNew.m_nColNo;
							rcRangeNew.top = htNew.m_nRowNo;
						}
					}
					else
					{
						::SetCursor( ::LoadCursor( NULL, IDC_ARROW ) );
						bStopFlag = true;
					}
					if( ptRangeShiftDND.x != 0L || ptRangeShiftDND.y != 0L )
					{
						__EXT_DEBUG_GRID_ASSERT( ! bStopFlag );
						rcRangeNew.OffsetRect( ptRangeShiftDND );
						LONG nColCount = _wndFG.ColumnCountGet();
						LONG nRowCount = _wndFG.RowCountGet();
						if( nColCount >= 0L && rcRangeNew.right >= nColCount )
							rcRangeNew.OffsetRect( nColCount - rcRangeNew.right - 1L, 0L );
						if( nRowCount >= 0L && rcRangeNew.bottom >= nRowCount )
							rcRangeNew.OffsetRect( 0L, nRowCount - rcRangeNew.bottom - 1L );
						if( rcRangeNew.left < 0L )
							rcRangeNew.OffsetRect( - rcRangeNew.left, 0L );
						if( rcRangeNew.top < 0L )
							rcRangeNew.OffsetRect( 0L, - rcRangeNew.top );
					} // if( ptOffset.x != 0L || ptOffset.y != 0L )
					if( rcRangeNew != rcRangeDND )
					{
						__EXT_DEBUG_GRID_ASSERT( ! bStopFlag );
						htLast = htNew;
						rcRangeDND = rcRangeNew;
						CExtGR2D::Range_stat_Normalize( rcRangeNew );
						CExtSafeString strNewRange;
						if(		rcRangeNew.left == rcRangeNew.right
							&&	rcRangeNew.top == rcRangeNew.bottom
							)
							strNewRange = _FA.GetLocationStr( rcRangeNew.left, rcRangeNew.top );
						else
							strNewRange = _FA.GetRangeLocationStr( rcRangeNew, &_FDM );
						Rich_SetSel( _FDM.m_crTracker );
						m_bFormulaRangeDndModification = true;
						ReplaceSel( LPCTSTR(strNewRange), FALSE );
						m_bFormulaRangeDndModification = false;
						_FDM.m_crTracker.cpMax = _FDM.m_crTracker.cpMin + strNewRange.GetLength();
						_FDM.m_pFormulaItemRangeInGridHT = m_pFormula->FindRangeItem( INT(_FDM.m_crTracker.cpMin), INT(_FDM.m_crTracker.cpMax) );
						if( _FDM.m_pFormulaItemRangeInGridHT != NULL )
						{
							_wndFG.OnSwInvalidate( true );
							//_wndFG.OnSwUpdateWindow();
						}
					} // if( rcRangeNew != rcRangeDND )
				} // if( htLast != htLastNew )
				if( ! bStopFlag )
					continue;
			}
		break;
		default:
			if(		( ! bStopFlag )
				&&	WM_KEYFIRST <= msg.message
				&&	msg.message <= WM_KEYLAST
				)
				bStopFlag = true;
		break;
		} // switch( msg.message )
		if( bStopFlag )
 			break;
		if( ! pThread->PumpMessage() )
 			break;

	} // drag-n-drop message loop
	if( ! ::IsWindow( hWndGrid ) )
		return;
	if( ! ::IsWindow( hWndInplaceControl ) )
		return;
	if( GetCapture() == this )
		ReleaseCapture();
	if( _FDM.m_pFormulaItemRangeInGridHT != NULL )
	{
		_FDM.m_pFormulaItemRangeInGridHT = NULL;
		_wndFG.OnSwInvalidate( true );
		//_wndFG.OnSwUpdateWindow();
	}
	m_bGridFocusMode = true;
	SetTimer( m_nGridFocusTimerID, _FDM.m_nGridSelectionTrackerTimerPeriod, NULL );
}

bool CExtFormulaInplaceEdit::OnFormulaRangeDndCreation( const CExtGridHitTestInfo & htInfo )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtFormulaGridWnd * pWndFormulaGrid = DYNAMIC_DOWNCAST( CExtFormulaGridWnd, (&m_wndGrid) );
	if(		pWndFormulaGrid == NULL
		||	( pWndFormulaGrid->FgGetStyle() & __FGS_FORMULA_REFS_MASK ) != __FGS_FORMULA_REFS_MASK
		)
		return false;
CExtFormulaGridWnd & _wndFG = (*pWndFormulaGrid);
CExtFormulaInplaceListBox * pWndIPLB = OnQueryIPLB( false );
	if( pWndIPLB != NULL && ( pWndIPLB->GetStyle() & WS_VISIBLE ) != 0 )
		pWndIPLB->ShowWindow( SW_HIDE );
	if(		htInfo.IsHoverEmpty()
		||	( ! htInfo.IsValidRect() )
		)
		return false;
INT nColType = htInfo.GetInnerOuterTypeOfColumn();
INT nRowType = htInfo.GetInnerOuterTypeOfRow();
	if( nColType != 0 && nRowType != 0 )
		return false; // if corner cells
CExtFormulaDataManager & _FDM = OnQueryFDM();
	if( m_bGridFocusMode )
		_GridFocusMode_Cancel();
CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
CExtSafeString strFormulaText = Rich_PlainText_Get();
//bool bAppendMode = false;
bool bSetRangeInitially = false;
	Rich_GetSel( _FDM.m_crTracker );
const CExtGridDataProvider & _DP = _FDM.OnQueryDataProvider();
ULONG nColCountReserved = 0L, nRowCountReserved = 0L;
	_DP.CacheReservedCountsGet( &nColCountReserved, &nRowCountReserved );
LONG nMaxColumn = _DP.ColumnCountGet() - LONG(nColCountReserved) - 1L; // GetMaxAlphaStrVal();
	__EXT_DEBUG_GRID_ASSERT( nMaxColumn >= 0L );
LONG nMaxRow = _DP.RowCountGet() - LONG(nRowCountReserved) - 1L; // GetMaxNumStrVal();
	__EXT_DEBUG_GRID_ASSERT( nMaxRow >= 0L );
CRect rcTrackRangeCration(
		( nColType != 0 ) ? 0L : htInfo.m_nColNo,
		( nRowType != 0 ) ? 0L : htInfo.m_nRowNo,
		( nColType != 0 ) ? nMaxColumn : htInfo.m_nColNo,
		( nRowType != 0 ) ? nMaxRow : htInfo.m_nRowNo
		);
	if( m_pFormula == NULL )
	{
		CExtSafeString strCheck;
		strCheck = strFormulaText;
		strCheck.TrimLeft( _T(" \r\n\t") );
		if( (! strCheck.IsEmpty() ) && strCheck[0] == _T('=') )
		{
			//bAppendMode = true;
			_FDM.m_crTracker.cpMin = _FDM.m_crTracker.cpMax = LONG(strFormulaText.GetLength());
			Rich_SetSel( _FDM.m_crTracker );
			bSetRangeInitially = true;
		}
		else
			return false;
	} // if( m_pFormula == NULL )
	else
	{
		INT nScanPosStart = INT( min( _FDM.m_crTracker.cpMin, _FDM.m_crTracker.cpMax ) );
		INT nScanPosEnd   = INT( max( _FDM.m_crTracker.cpMin, _FDM.m_crTracker.cpMax ) );
		CExtFormulaItem * pFIHI = m_pFormula->FindRangeItem( nScanPosStart, nScanPosEnd );
		if( pFIHI == NULL )
		{
			if( nScanPosStart != nScanPosEnd || nScanPosEnd == 0 )
				return false;
			TCHAR _tchr = strFormulaText[ nScanPosEnd - 1 ];
			if( ! (		_tchr == _T(' ') || _tchr == _T('=') || _tchr == _T('\n') || _tchr == _T('\r') || _tchr == _T('\t')
					||	_tchr == _T(')') || _tchr == _T('(') || _tchr == _T('+') || _tchr == _T('-') || _tchr == _T('*') || _tchr == _T('/')
					)
				)
			{
				m_bFormulaRangeDndCreation = true;
				ReplaceSel( _T(" "), FALSE );
				m_bFormulaRangeDndCreation = false;
				nScanPosEnd ++;
				_FDM.m_crTracker.cpMin = _FDM.m_crTracker.cpMax = nScanPosStart = nScanPosEnd;
				Rich_SetSel( _FDM.m_crTracker );
			}
			if( nScanPosEnd < INT(strFormulaText.GetLength()-1) )
			{
				TCHAR _tchr = strFormulaText[ nScanPosEnd - 1 ];
				if( ! (		_tchr == _T(' ') || _tchr == _T('=') || _tchr == _T('\n') || _tchr == _T('\r') || _tchr == _T('\t')
						||	_tchr == _T(')') || _tchr == _T('(') || _tchr == _T('+') || _tchr == _T('-') || _tchr == _T('*') || _tchr == _T('/')
						)
					)
				{
					m_bFormulaRangeDndCreation = true;
					ReplaceSel( _T(" "), FALSE );
					m_bFormulaRangeDndCreation = false;
					Rich_SetSel( _FDM.m_crTracker );
				}
			}
			bSetRangeInitially = true;
		} // if( pFIHI == NULL )
		else
		{
			_FDM.m_crTracker.cpMin = pFIHI->m_nScanPosStart;
			_FDM.m_crTracker.cpMax = pFIHI->m_nScanPosEnd;
			if( rcTrackRangeCration != pFIHI->m_rcEditedRange )
				bSetRangeInitially = true;
		} // else from if( pFIHI == NULL )
	} // else from if( m_pFormula == NULL )
	if( bSetRangeInitially )
	{ // if need to replace initially
		CExtSafeString strNewRange;
		if(		rcTrackRangeCration.left == rcTrackRangeCration.right
			&&	rcTrackRangeCration.top == rcTrackRangeCration.bottom
			)
			strNewRange = _FA.GetLocationStr( rcTrackRangeCration.left, rcTrackRangeCration.top );
		else
			strNewRange = _FA.GetRangeLocationStr( rcTrackRangeCration, &_FDM );
		Rich_SetSel( _FDM.m_crTracker );
		m_bFormulaRangeDndCreation = true;
		ReplaceSel( LPCTSTR(strNewRange), FALSE );
		m_bFormulaRangeDndCreation = false;
		_FDM.m_crTracker.cpMax = _FDM.m_crTracker.cpMin + strNewRange.GetLength();
		_FDM.m_pFormulaItemRangeInGridHT = m_pFormula->FindRangeItem( INT(_FDM.m_crTracker.cpMin), INT(_FDM.m_crTracker.cpMax) );
		if( _FDM.m_pFormulaItemRangeInGridHT != NULL )
		{
			_wndFG.OnSwInvalidate( true );
			//_wndFG.OnSwUpdateWindow();
		}
	} // if need to replace initially
CHARRANGE crAfter;
	crAfter.cpMin = _FDM.m_crTracker.cpMax;
	crAfter.cpMax = _FDM.m_crTracker.cpMax;
	Rich_SetSel( crAfter );

HWND hWndGrid = _wndFG.GetSafeHwnd();
	__EXT_DEBUG_GRID_ASSERT( hWndGrid != NULL );
HWND hWndInplaceControl = GetSafeHwnd();
	__EXT_DEBUG_GRID_ASSERT( hWndInplaceControl != NULL );
CWinThread * pThread = ::AfxGetThread();
	__EXT_DEBUG_GRID_ASSERT_VALID( pThread );
bool bStopFlag = false;
	SetCapture();
CExtGridHitTestInfo htLast = htInfo;
	for(	MSG msg;
				::IsWindow( hWndInplaceControl )
			&&	::IsWindow( hWndGrid )
			&&	(!bStopFlag)
			;
		)
	{ // drag-n-drop message loop
		if( ! ::PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) )
		{
			if(		hWndInplaceControl == NULL
				||	( ! ::IsWindow( hWndInplaceControl ) )
				||	( ! ::IsWindow( hWndGrid ) )
				||	bStopFlag
				)
				break;
			LONG nIdleCounter = 0L;
			if( CExtGridWnd::g_bEnableOnIdleCalls )
			{
				for( nIdleCounter = 0L; pThread->OnIdle(nIdleCounter); nIdleCounter ++ );
			}
			::WaitMessage();
			continue;
		} // if( ! ::PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) )
		switch( msg.message )
		{
		case WM_KILLFOCUS:
			if( msg.hwnd == hWndInplaceControl )
				bStopFlag = true;
		break;
		case WM_CANCELMODE:
		case WM_ACTIVATEAPP:
		case WM_SYSCOMMAND:
		case WM_SETTINGCHANGE:
		case WM_SYSCOLORCHANGE:
			bStopFlag = true;
		break;
		case WM_COMMAND:
			if(		(HIWORD(msg.wParam)) == 0
				||	(HIWORD(msg.wParam)) == 1
				)
				bStopFlag = true;
		break;
		case WM_CAPTURECHANGED:
			if( (HWND)msg.wParam != hWndInplaceControl )
				bStopFlag = true;
		break;

		case WM_SETCURSOR:
			{
				CPoint pt;
				if(		::GetCursorPos(&pt)
					&&	::WindowFromPoint( pt ) == hWndGrid
					)
				{
					::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
					continue;
				}
			}
		break;
		case WM_LBUTTONDOWN:
			::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
			if( msg.hwnd != hWndInplaceControl )
				bStopFlag = true;
			else
				continue;
		break;

		case WM_LBUTTONUP:
			::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
			bStopFlag = true;
		break;

		case WM_LBUTTONDBLCLK:
		case WM_RBUTTONUP:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONDBLCLK:
		case WM_MBUTTONUP:
		case WM_MBUTTONDOWN:
		case WM_MBUTTONDBLCLK:
		case WM_NCLBUTTONUP:
		case WM_NCLBUTTONDOWN:
		case WM_NCLBUTTONDBLCLK:
		case WM_NCRBUTTONUP:
		case WM_NCRBUTTONDOWN:
		case WM_NCRBUTTONDBLCLK:
		case WM_NCMBUTTONUP:
		case WM_NCMBUTTONDOWN:
		case WM_NCMBUTTONDBLCLK:
		case WM_CONTEXTMENU:
			bStopFlag = true;
		break;
		case WM_MOUSEMOVE:
			{
				::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
				CPoint pt;
				if( ! ::GetCursorPos( &pt ) )
				{
					bStopFlag = true;
					break;
				}
				_wndFG.ScreenToClient( &pt );
				if( nColType != 0 )
				{ // if full row selection
					pt.x = htInfo.m_ptClient.x;
				} // if full row selection
				else if( nRowType != 0 )
				{ // if full column selection
					pt.y = htInfo.m_ptClient.y;
				} // if full column selection
				CExtGridHitTestInfo htNew( pt );
				_wndFG.HitTest( htNew, false, false );
				if(		( ! htNew.IsHoverEmpty() )
					&&	htNew.IsValidRect()
					&&	htNew.GetInnerOuterTypeOfColumn() == nColType
					&&	htNew.GetInnerOuterTypeOfRow() == nRowType
					&&	( htNew.m_nColNo != htLast.m_nColNo || htNew.m_nRowNo != htLast.m_nRowNo )
					)
				{
					if( nColType == 0 && nRowType == 0 )
					{
						CSize sizeJoin = _wndFG.OnGbwCellJoinQueryInfo( htNew.m_nColNo, htNew.m_nRowNo );
						if( sizeJoin.cx < 1L || sizeJoin.cy < 1L )
						{ // jump to join root
							htNew.m_nColNo += sizeJoin.cx;
							htNew.m_nRowNo += sizeJoin.cy;
							sizeJoin = _wndFG.OnGbwCellJoinQueryInfo( htNew.m_nColNo, htNew.m_nRowNo );
							__EXT_DEBUG_GRID_ASSERT( sizeJoin.cx >= 1L && sizeJoin.cy >= 1L );
							__EXT_DEBUG_GRID_ASSERT( sizeJoin.cx > 1L || sizeJoin.cy > 1L );
						} // jump to join root
						if( sizeJoin.cx > 1L || sizeJoin.cy > 1L )
						{ // check need to jump to right bottom join margins
							if( rcTrackRangeCration.right >= rcTrackRangeCration.left )
								htNew.m_nColNo += sizeJoin.cx - 1L;
							if( rcTrackRangeCration.bottom >= rcTrackRangeCration.top )
								htNew.m_nRowNo += sizeJoin.cy - 1L;
						} // check need to jump to right bottom join margins
					}
					CRect rcNewRange = rcTrackRangeCration;
					if( nColType != 0 )
					{ // if full row selection
						rcNewRange.bottom = htNew.m_nRowNo;
					} // if full row selection
					else if( nRowType != 0 )
					{ // if full column selection
						rcNewRange.right = htNew.m_nColNo;
					} // if full column selection
					else
					{ // if data range selection
						rcNewRange.right = htNew.m_nColNo;
						rcNewRange.bottom = htNew.m_nRowNo;
					} // if data range selection
					if( rcNewRange != rcTrackRangeCration )
					{ // if need to update
						htLast = htNew;
						rcTrackRangeCration = rcNewRange;
						CExtGR2D::Range_stat_Normalize( rcNewRange );
						CExtSafeString strNewRange;
						if(		rcNewRange.left == rcNewRange.right
							&&	rcNewRange.top == rcNewRange.bottom
							)
							strNewRange = _FA.GetLocationStr( rcNewRange.left, rcNewRange.top );
						else
							strNewRange = _FA.GetRangeLocationStr( rcNewRange, &_FDM );
						Rich_SetSel( _FDM.m_crTracker );
						m_bFormulaRangeDndCreation = true;
						ReplaceSel( LPCTSTR(strNewRange), FALSE );
						m_bFormulaRangeDndCreation = false;
						_FDM.m_crTracker.cpMax = _FDM.m_crTracker.cpMin + strNewRange.GetLength();
						_FDM.m_pFormulaItemRangeInGridHT = m_pFormula->FindRangeItem( INT(_FDM.m_crTracker.cpMin), INT(_FDM.m_crTracker.cpMax) );
						if( _FDM.m_pFormulaItemRangeInGridHT != NULL )
						{
							_wndFG.OnSwInvalidate( true );
							//_wndFG.OnSwUpdateWindow();
						}
					} // if need to update
				} // if( htLast != htLastNew )
				if( ! bStopFlag )
					continue;
			}
		break;
		default:
			if(		( ! bStopFlag )
				&&	WM_KEYFIRST <= msg.message
				&&	msg.message <= WM_KEYLAST
				)
				bStopFlag = true;
		break;
		} // switch( msg.message )
		if( bStopFlag )
 			break;
		if( ! pThread->PumpMessage() )
 			break;

	} // drag-n-drop message loop
	if( ! ::IsWindow( hWndGrid ) )
		return true;
	if( ! ::IsWindow( hWndInplaceControl ) )
		return true;
	if( GetCapture() == this )
		ReleaseCapture();
	if( _FDM.m_pFormulaItemRangeInGridHT != NULL )
	{
		_FDM.m_pFormulaItemRangeInGridHT = NULL;
		_wndFG.OnSwInvalidate( true );
		//_wndFG.OnSwUpdateWindow();
	}
	m_bGridFocusMode = true;
	SetTimer( m_nGridFocusTimerID, _FDM.m_nGridSelectionTrackerTimerPeriod, NULL );
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaGridCell

CExtFormulaGridCell::CExtFormulaGridCell(
	CExtGridDataProvider * pDataProvider // = NULL
	)
	: CExtGridCellVariant( pDataProvider )
	, m_pFormula( NULL )
	, m_pErrorLocationForumaItem( NULL )
	, m_eFormulaError( CExtFormulaValue::__EFEC_OK )
{
	_VariantAssign( LPCTSTR(_T("")) );
	ModifyStyleEx( __EGCS_EX_AUTO_SIZE_INPLACE_CONTROL|__EGCS_EX_NO_VALIDATE_AUTO_EDIT_TEXT|__EGCS_EX_EMPTY );
}

CExtFormulaGridCell::CExtFormulaGridCell( const CExtGridCell & other )
	: m_pFormula( NULL )
	, m_pErrorLocationForumaItem( NULL )
	, m_eFormulaError( CExtFormulaValue::__EFEC_OK )
{
	Assign( other );
}

CExtFormulaGridCell::~CExtFormulaGridCell()
{
	FormulaStateClear();
}

void CExtFormulaGridCell::Assign( const CExtGridCell & other )
{
	CExtGridCellVariant::Assign( other );
	FormulaStateClear();
CExtFormulaGridCell * pCell = DYNAMIC_DOWNCAST( CExtFormulaGridCell, ( const_cast < CExtGridCell * > ( &other ) ) );
	if( pCell != NULL )
		m_strFormulaText = pCell->m_strFormulaText;
	else
		m_strFormulaText.Empty();
}

void CExtFormulaGridCell::Serialize( CArchive & ar )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtGridCellVariant::Serialize( ar );
	SerializeFormulaData( ar );
}

void CExtFormulaGridCell::SerializeFormulaData( CArchive & ar )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
DWORD dwFlags = 0L;
CExtSafeString strFormulaText;
	if( ar.IsStoring() )
	{
		if( FormulaTextGet( strFormulaText ) )
		{
			if( ! strFormulaText.IsEmpty() )
				dwFlags |= 0x00000001;
		}
		ar << dwFlags;
		if( dwFlags & 0x00000001 )
			ar << strFormulaText;
	} // if( ar.IsStoring() )
	else
	{
		ar >> dwFlags;
		if( dwFlags & 0x00000001 )
			ar >> strFormulaText;
		FormulaTextSet( strFormulaText );
	} // else from if( ar.IsStoring() )
}

bool CExtFormulaGridCell::OnSetCursor(
	CExtGridWnd & wndGrid,
	const CExtGridHitTestInfo & htInfo
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&wndGrid) );
	__EXT_DEBUG_GRID_ASSERT( ! htInfo.IsHoverEmpty() );
	__EXT_DEBUG_GRID_ASSERT( htInfo.IsValidRect() );
	if( ( htInfo.m_dwAreaFlags & __EGBWA_INNER_CELLS ) != 0 )
	{
		CExtFormulaGridWnd * pWndFG = DYNAMIC_DOWNCAST( CExtFormulaGridWnd, (&wndGrid) );
		if( pWndFG != NULL && pWndFG->m_hCursorDefaultInnerFC )
		{
			::SetCursor( pWndFG->m_hCursorDefaultInnerFC );
			return true;
		}
	}
	return CExtGridCell::OnSetCursor( wndGrid, htInfo );
}

void CExtFormulaGridCell::OnInplaceControlTextInputComplete(
	HWND hWndInplaceControl,
	CExtGridWnd & wndGrid,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	__EXT_MFC_SAFE_LPCTSTR sTextNew,
	bool bSaveChanges
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtFormulaGridWnd * pFG = NULL;
	if( (!bSaveChanges) || nColType != 0 || nRowType != 0 || ( pFG = DYNAMIC_DOWNCAST( CExtFormulaGridWnd, (&wndGrid) ) ) == NULL )
	{
		CExtGridCellVariant::OnInplaceControlTextInputComplete( hWndInplaceControl, wndGrid, nVisibleColNo, nVisibleRowNo, nColNo, nRowNo, nColType, nRowType, sTextNew, bSaveChanges );
		return;
	}
	__EXT_DEBUG_GRID_ASSERT_VALID( pFG );
	ModifyStyleEx( 0, __EGCS_EX_EMPTY );
CExtFormulaDataManager & _FDM = pFG->OnGridQueryFormulaDataManager();
	if( LPCTSTR(sTextNew) != NULL )
	{
		CExtSafeString strCheck;
		strCheck = sTextNew;
		strCheck.TrimLeft( _T(" \r\n\t") );
		if( (! strCheck.IsEmpty() ) && strCheck[0] == _T('=') )
		{
			_FDM._RFE_OnRegisterFormulaCell( nColNo, nRowNo, this );

			// text is formula

			CExtSafeString strFormulaText = sTextNew;
			CExtFormulaParser & _FormulaParser = _FDM.OnQueryFormulaParser();
			_FormulaParser.Init( strFormulaText );
			if( _FormulaParser.Parse() )
			{
				__EXT_DEBUG_GRID_ASSERT( _FormulaParser.m_pFormulaRoot != NULL );
			}
			else
			{
				CExtSafeString str2 = LPCTSTR(strFormulaText);
				str2 += _T(")"); // the most common syntax error
				_FormulaParser.Init( str2 );
				if( _FormulaParser.Parse() )
				{
					__EXT_DEBUG_GRID_ASSERT( _FormulaParser.m_pFormulaRoot != NULL );
					strFormulaText = LPCTSTR(str2);
				}
			}

			FormulaTextSet( LPCTSTR(strFormulaText) );

			CExtFormulaValue _FV;
			CExtFormulaDataManager::map_formula_cells_t _mapCellsAlreadyComputed;
			_mapCellsAlreadyComputed.SetAt( CExtGridDataProvider::_LL2L( nColNo, nRowNo ), this );
			if( _FormulaComputeValueImpl( _mapCellsAlreadyComputed, nColNo, nRowNo, _FV, _FDM, NULL ) )
				_VariantAssign( _FV.m_val );
			_FDM.OnComputeDependentCells( _mapCellsAlreadyComputed, nColNo, nRowNo, false );
//			if( wndGrid.m_nLastEndEditKey == VK_RETURN && ( wndGrid.BseGetStyle() & __EGWS_BSE_EDIT_RETURN_MOVES_NEXT_ROW ) != 0 && (! wndGrid.ExternalDataGet() ) )
//			{
//				DWORD dwSiwStyle = wndGrid.SiwGetStyle();
//				DWORD dwSFB = dwSiwStyle & __EGBS_SFB_MASK;
//				if( dwSFB == __EGBS_SFB_CELLS || dwSFB == __EGBS_SFB_FULL_ROWS )
//				{
//					LONG nRowCount = wndGrid.RowCountGet();
//					if( nRowNo < ( nRowCount - 1L ) )
//					{
//						nRowNo ++;
//						wndGrid.FocusSet( CPoint( nColNo, nRowNo ) );
//					}
//				} // if( dwSFB == __EGBS_SFB_CELLS || dwSFB == __EGBS_SFB_FULL_ROWS )
//			} // if( wndGrid.m_nLastEndEditKey == VK_RETURN && ( wndGrid.BseGetStyle() & __EGWS_BSE_EDIT_RETURN_MOVES_NEXT_ROW ) != 0 && (! wndGrid.ExternalDataGet() ) )
			wndGrid.OnGridCellInputComplete( *this, nColNo, nRowNo, nColType, nRowType, hWndInplaceControl );
			return;
		}
	}
	FormulaTextSet( _T("") );
	_FDM._RFE_OnRegisterFormulaCell( nColNo, nRowNo, NULL );
bool bVariantParseError = false, bEmptyText = ( LPCTSTR(sTextNew) == NULL || _tcslen( LPCTSTR(sTextNew) ) == 0 ) ? true : false;
	if( ! bEmptyText )
		bVariantParseError = ( OnParseText( LPCTSTR(sTextNew) ) == S_OK ) ? false : true;
	if( bEmptyText || bVariantParseError )
	{
		_VariantClear();
		_VariantAssign( _T("") );
	}
	CExtGridCellVariant::OnInplaceControlTextInputComplete( hWndInplaceControl, wndGrid, nVisibleColNo, nVisibleRowNo, nColNo, nRowNo, nColType, nRowType, sTextNew, bSaveChanges );
}

HRESULT CExtFormulaGridCell::OnParseText( __EXT_MFC_SAFE_LPCTSTR sText ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
//CExtSafeString str;
//	if( FormulaTextGet( str ) )
//		return S_OK;
	if( LPCTSTR(sText) != NULL )
	{
		CExtSafeString strCheck;
		strCheck = sText;
		strCheck.TrimLeft( _T(" \r\n\t") );
		if( (! strCheck.IsEmpty() ) && strCheck[0] == _T('=') )
			return S_OK; // text is formula
	}
	return CExtGridCellVariant::OnParseText( sText );
}

HWND CExtFormulaGridCell::OnInplaceControlCreate(
	HWND hWndParentForEditor,
	CExtGridWnd & wndGrid,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcInplaceControl,
	LONG nLastEditedColNo,
	LONG nLastEditedRowNo
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&wndGrid) );
	if( this->vt == VT_EMPTY )
		_VariantAssign( _T("") );
	return
		CExtGridCellVariant::OnInplaceControlCreate(
			hWndParentForEditor, wndGrid, nVisibleColNo, nVisibleRowNo, nColNo, nRowNo, nColType, nRowType,
			rcCellExtra, rcCell, rcInplaceControl, nLastEditedColNo, nLastEditedRowNo
			);
}

HWND CExtFormulaGridCell::_OnInplaceControlCreateInstanceImpl(
	HWND hWndParentForEditor,
	CExtGridWnd & wndGrid,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcInplaceControl,
	const RECT & rcInplaceControlAdjusted,
	LONG nLastEditedColNo,
	LONG nLastEditedRowNo
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	rcInplaceControl;
CExtFormulaInplaceEdit * pEdit = NULL;
	try
	{
		pEdit =
			new CExtFormulaInplaceEdit(
				hWndParentForEditor, wndGrid, *this, nVisibleColNo, nVisibleRowNo, nColNo, nRowNo, nColType,
				nRowType, rcCellExtra, rcCell,
				rcInplaceControlAdjusted, // rcInplaceControl /// Ulrich Heinicke
				nLastEditedColNo, nLastEditedRowNo
				);
		__EXT_DEBUG_GRID_ASSERT_VALID( pEdit );
		if( ! pEdit->Create() )
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return NULL;
		} // if( ! pEdit->Create() )
	} // try
	catch( CException * pXept )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		pXept->Delete();
		return NULL;
	} // catch( CException * pXept )
	catch( ... )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return NULL;
	} // catch( ... )
	__EXT_DEBUG_GRID_ASSERT_VALID( pEdit );
HWND hWnd = pEdit->GetSafeHwnd();
	__EXT_DEBUG_GRID_ASSERT( hWnd != NULL && ::IsWindow(hWnd) );
	return hWnd;
}

UINT CExtFormulaGridCell::OnQueryDrawTextFlagsForInplaceEdit(
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags,
	bool bIncludeHorizontalFlags, // = true
	bool bIncludeVerticalFlags, // = true
	bool bIncludeOtherFlags // = true
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
UINT nDT =
		CExtGridCellVariant::OnQueryDrawTextFlagsForInplaceEdit(
			nVisibleColNo, nVisibleRowNo, nColNo, nRowNo, nColType, nRowType, dwAreaFlags, dwHelperPaintFlags,
			bIncludeHorizontalFlags, bIncludeVerticalFlags, bIncludeOtherFlags
			);
//CExtSafeString str;
//	if( FormulaTextGet( str ) )
	{
		nDT &= ~(DT_CENTER|DT_RIGHT);
		nDT |= DT_LEFT;
	}
	return nDT;
}

bool CExtFormulaGridCell::OnFormatFormulaInplaceEdit( CExtFormulaItem * pFI, CExtFormulaInplaceEdit & wndFIE )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return wndFIE.OnQueryFormulaGridWnd().OnGridCellFormatFormulaInplaceEdit( pFI, wndFIE );
}

void CExtFormulaGridCell::OnQueryTextForInplaceControl( 
	CExtSafeString & strCopy 
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( FormulaTextGet( strCopy ) )
		return;
	CExtGridCellVariant::OnQueryTextForInplaceControl( strCopy );
}

bool CExtFormulaGridCell::OnFormatShiftedText(
	CExtFormulaDataManager & _FDM,
	LONG nShiftX,
	LONG nShiftY,
	const CExtGR2D * pRangeToShift // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( nShiftX == 0L && nShiftY == 0L )
		return true;
CExtSafeString strFormulaText;
	if( ! FormulaTextGet( strFormulaText ) )
		return false;
CExtFormulaItem * pFI = FormulaGet();
	if( pFI == NULL )
	{
		if( ! FormulaParse( _FDM ) )
			return false;
		pFI = FormulaGet();
	}
	__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
	m_pErrorLocationForumaItem = NULL;
	pFI->ShiftRangesInText( _FDM, strFormulaText, nShiftX, nShiftY, pRangeToShift );
	FormulaTextSet( LPCTSTR(strFormulaText) );
	FormulaStateClear();
	return true;
}

bool CExtFormulaGridCell::OnQueryFormulaText(
	CExtSafeString & strFormulaText,
	bool bGet
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtGridCell, this );
	if( bGet )
	{
		if( m_strFormulaText.IsEmpty() )
			return false;
		strFormulaText = m_strFormulaText;
	}
	else
	{
		FormulaStateClear();
		m_strFormulaText = strFormulaText;
	}
	return true;
}

void CExtFormulaGridCell::FormulaSetError( CExtFormulaValue::e_formula_error_code_t err )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_eFormulaError == err )
		return;
	m_eFormulaError = err;
	_VariantClear();
CExtSafeString strErrorDescription = _T(""), str1 = CExtFormulaValue::stat_ErrorDescription( m_eFormulaError );
	if( m_eFormulaError != CExtFormulaValue::__EFEC_OK )
		strErrorDescription.Format( _T("Error: %d - %s"), INT(m_eFormulaError), LPCTSTR(str1) );
	_VariantAssign( LPCTSTR(strErrorDescription) );
}

void CExtFormulaGridCell::FormulaSetDependencyError(
	CExtFormulaDataManager & _FDM,
	CPoint ptError
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_pErrorLocationForumaItem = NULL;
	m_eFormulaError =  CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
	_VariantClear();
CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
CExtSafeString strErrorDescription, str1 = _FA.GetLocationStr( ptError );
	strErrorDescription.Format( _T("Error: error in %s"), LPCTSTR(str1) );
	_VariantAssign( LPCTSTR(strErrorDescription) );
}

void CExtFormulaGridCell::FormulaSetCrossReferenceError(
	CExtFormulaDataManager & _FDM,
	CRect rcLocation
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_pErrorLocationForumaItem = NULL;
	m_eFormulaError =  CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
	_VariantClear();
CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
CExtSafeString strErrorDescription, str1 = _FA.GetLocationStr( rcLocation.TopLeft() ), str2( _FA.GetLocationStr( rcLocation.BottomRight() ) );
	strErrorDescription.Format( _T("Error: cross reference between %s and %s"), LPCTSTR(str1), LPCTSTR(str2) );
	_VariantAssign( LPCTSTR(strErrorDescription) );
}

bool CExtFormulaGridCell::FormulaTextGet(
	CExtSafeString & strFormulaText
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtFormulaGridCell * > ( this ) )
			-> OnQueryFormulaText( strFormulaText, true );
}

bool CExtFormulaGridCell::FormulaTextSet(
	__EXT_MFC_SAFE_LPCTSTR strFormulaText
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtSafeString _strFormulaText( ( LPCTSTR(strFormulaText) != NULL ) ? LPCTSTR( strFormulaText ) : NULL );
	return OnQueryFormulaText( _strFormulaText, false );
}

bool CExtFormulaGridCell::FormulaStateIsEmpty() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CSize sizeJoin = JoinGet();
	if( sizeJoin.cx != 1L || sizeJoin.cy != 1L )
		return false;
	if( IsEmpty() )
		return true;
CExtSafeString str;
	if( FormulaTextGet( str ) )
		return false;
	str.Empty();
	TextGet( str );
	if( ! str.IsEmpty() )
		return false;
	return true;
}

void CExtFormulaGridCell::FormulaStateClear()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_pErrorLocationForumaItem = NULL;
	m_eFormulaError = CExtFormulaValue::__EFEC_OK;
	if( m_pFormula != NULL )
	{
		delete m_pFormula;
		m_pFormula = NULL;
	}
}

bool CExtFormulaGridCell::FormulaParse(
	CExtFormulaDataManager & _FDM
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	FormulaStateClear();
CExtSafeString strFormulaText;
	if( ! FormulaTextGet( strFormulaText ) )
		return false;
CExtFormulaParser & _FormulaParser = _FDM.OnQueryFormulaParser();
	_FormulaParser.Init( strFormulaText );
	if( ! _FormulaParser.Parse() )
		return false;
	__EXT_DEBUG_GRID_ASSERT( _FormulaParser.m_pFormulaRoot != NULL );
	m_pFormula = _FormulaParser.m_pFormulaRoot;
	_FormulaParser.m_pFormulaRoot = NULL;
	return true;
}

CExtFormulaItem * CExtFormulaGridCell::FormulaGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_pFormula;
}

const CExtFormulaItem * CExtFormulaGridCell::FormulaGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ( const_cast < CExtFormulaGridCell * > ( this ) ) -> FormulaGet();
}

const CExtFormulaItem * CExtFormulaGridCell::FormulaGetErrorLocation() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_pErrorLocationForumaItem;
}

CExtFormulaValue::e_formula_error_code_t CExtFormulaGridCell::FormulaGetErrorCode() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_eFormulaError;
}

bool CExtFormulaGridCell::_FormulaComputeValueImpl(
	CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
	LONG nColNo,
	LONG nRowNo,
	CExtFormulaValue & _FV,
	CExtFormulaDataManager & _FDM,
	const CExtFormulaItem * pExternalFI
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nColNo;
	nRowNo;
	pExternalFI;
	_FV.Empty();
	_mapCellsAlreadyComputed.SetAt( CExtGridDataProvider::_LL2L( nColNo, nRowNo ), this );
CExtFormulaItem * pFI = FormulaGet();
	if( pFI == NULL )
	{
		if( ! FormulaParse( _FDM ) )
		{
//			return false;
			CExtSafeString strFormulaText;
			if( ! FormulaTextGet( strFormulaText ) )
			{
//				m_pErrorLocationForumaItem = NULL;
//				FormulaSetError( CExtFormulaValue::__EFEC_SYNTAX_ERROR );
//				return false;
				_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
				GetVariant( _FV.m_val );
				return true;
			}
			FormulaSetError( CExtFormulaValue::__EFEC_OK );
			_VariantAssign( LPCTSTR(strFormulaText) );
			return true;
		}
		pFI = FormulaGet();
	}
	__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
	m_pErrorLocationForumaItem = NULL;
CExtFormulaValue::e_formula_error_code_t err = pFI->ComputeValue( _mapCellsAlreadyComputed, _FV, _FDM, &m_pErrorLocationForumaItem );
	if(  err != CExtFormulaValue::__EFEC_OK )
	{
		FormulaSetError( err );
		return false;
	}
	if( _FV.m_eType == CExtFormulaValue::__EFVT_CELL_RANGE )
	{
		if( _FV.m_gr.IsSinglePoint() )
		{
			CPoint pt = _FV.m_gr.Range( 0 ).TopLeft();
			if( ! _FDM.OnComputeValue( _mapCellsAlreadyComputed, pt.x, pt.y, _FV, pExternalFI ) )
			{
				err = CExtFormulaValue::__EFEC_DEPENDENCY_COMPUTATION_FAILURE;
				FormulaSetError( err );
				return false;
			}
		}
		else
		{
			m_pErrorLocationForumaItem = pFI;
			FormulaSetError( CExtFormulaValue::__EFEC_RESULT_IS_RANGE_REFERENCE );
			return false;
		}
	}
	return true;
}

bool CExtFormulaGridCell::FormulaComputeValue(
	LONG nColNo,
	LONG nRowNo,
	CExtFormulaValue & _FV,
	CExtFormulaDataManager & _FDM,
	const CExtFormulaItem * pExternalFI
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtFormulaDataManager::map_formula_cells_t _mapCellsAlreadyComputed;
	return _FormulaComputeValueImpl( _mapCellsAlreadyComputed, nColNo, nRowNo, _FV, _FDM, pExternalFI );
}

void CExtFormulaGridCell::FormulaComputeDependencySourceRange(
	CExtFormulaDataManager & _FDM,
	CExtGR2D & _range
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	_range.Empty();
const CExtFormulaItem * pFI = FormulaGet();
	if( pFI == NULL )
//	{
//		if( ! FormulaParse( _FDM ) )
//			return;
//		pFI = FormulaGet();
//	}
		return;
	__EXT_DEBUG_GRID_ASSERT( pFI != NULL );
CExtFormulaDataManager::map_formula_cells_t _mapCellsAlreadyComputed;
	pFI->ComputeDependencySourceRange( _mapCellsAlreadyComputed, _FDM, _range );
}

bool CExtFormulaGridCell::OnAccelCommand( DWORD dwEGSA, CExtGridWnd & wndGrid, LPVOID pData, LONG nCounter, LONG nColNo, LONG nRowNo, const CExtGR2D & _range )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&wndGrid) );
	switch( dwEGSA )
	{
	case __EGSA_SELECTED_RANGE_LOAD:
	case __EGSA_PASTE: // cut and paste only do cleaning here
	case __EGSA_CUT:
	case __EGSA_DELETE_SELECTION:
	{
		FormulaTextSet( NULL );
		FormulaStateClear();
		Empty();
		CExtFormulaGridWnd * pWndFG = DYNAMIC_DOWNCAST( CExtFormulaGridWnd, (&wndGrid) );
		if( pWndFG != NULL )
		{
			CExtFormulaDataManager & _FDM = pWndFG->OnGridQueryFormulaDataManager();
			_FDM._RFE_OnRegisterFormulaCell( nColNo, nRowNo, NULL );
		}
	}
	return true;
	} // switch( dwEGSA )
	return CExtGridCell::OnAccelCommand( dwEGSA, wndGrid, pData, nCounter, nColNo, nRowNo, _range );
}

void CExtFormulaGridCell::OnAccelCommandComplete( DWORD dwEGSA, CExtGridWnd & wndGrid, LONG nColNo, LONG nRowNo, const CExtGR2D & _rangeSelOld, const CExtGR2D & _rangeSelNew, const CSize & _sizeShift )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&wndGrid) );
	CExtGridCell::OnAccelCommandComplete( dwEGSA, wndGrid, nColNo, nRowNo, _rangeSelOld, _rangeSelNew, _sizeShift );
	switch( dwEGSA )
	{
	case __EGSA_SELECTED_RANGE_LOAD:
	case __EGSA_PASTE:
		{
			CExtFormulaGridWnd * pWndFG = DYNAMIC_DOWNCAST( CExtFormulaGridWnd, (&wndGrid) );
			if( pWndFG != NULL )
			{
				bool bEmptyFormulaText = false;
				CExtSafeString strFormulaText;
				if( ! FormulaTextGet( strFormulaText ) )
					bEmptyFormulaText = true;
				else if( strFormulaText.IsEmpty() )
					bEmptyFormulaText = true;
				CExtFormulaDataManager & _FDM = pWndFG->OnGridQueryFormulaDataManager();
				_FDM._RFE_OnRegisterFormulaCell( nColNo, nRowNo, bEmptyFormulaText ? NULL : this );
				pWndFG->FormulaShift( nColNo, nRowNo, _sizeShift, NULL, false, false );
			}
		}
	break;
	} // switch( dwEGSA )
}

void CExtFormulaGridCell::TextSetOnPaste(
	__EXT_MFC_SAFE_LPCTSTR str, // = __EXT_MFC_SAFE_LPCTSTR(NULL) // empty text
	bool bAllowChangeDataType // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	FormulaTextSet( NULL );
	FormulaStateClear();
	CExtGridCell::TextSetOnPaste( str, bAllowChangeDataType );
}

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaDataManager

CExtFormulaDataManager::CExtFormulaDataManager()
	: m_pFormulaParser( NULL )
	, m_pFormulaAlphabet( NULL )
	, m_pFormulaFunctionSet( NULL )
	, m_nRangeWalkIndex( 0 )
	, m_clrNumericConstant( COLORREF(-1L) )
	, m_clrStringConstant( COLORREF(-1L) )
	, m_clrFuncCall( COLORREF(-1L) )
	, m_pFormulaItemRangeInGridHT( NULL )
	, m_bInGridHtLeft( false )
	, m_bInGridHtTop( false )
	, m_bInGridHtRight( false )
	, m_bInGridHtBottom( false )
	, m_rcInGridHT( -32767, -32767, -32767, -32767 )
	, m_bDrawLinesForNamedRanges( true )
	, m_bDrawLinesForSimpleRanges( true )
	, m_nAlphaForNamedRanges( BYTE(0x10) )
	, m_nAlphaForSimpleRanges( BYTE(0x10) )
	, m_nGridSelectionTrackerTimerPeriod( 100 )
	, m_nGridSelectionTrackerLength0( 2 )
	, m_nGridSelectionTrackerLength1( 2 )
	, m_nGridSelectionTrackerWidth( 1 )
	, m_nGridSelectionTrackerHeight( 1 )
{
	m_crTracker.cpMin = m_crTracker.cpMax = -1L;
	m_arrRangeRefColors.Add( RGB(0,0,255) );
	m_arrRangeRefColors.Add( RGB(0,128,0) );
	m_arrRangeRefColors.Add( RGB(153,0,204) );
	m_arrRangeRefColors.Add( RGB(128,0,0) );
	m_arrRangeRefColors.Add( RGB(0,204,51) );
	m_arrRangeRefColors.Add( RGB(255,102,0) );
	m_arrRangeRefColors.Add( RGB(204,0,153) );
}

CExtFormulaDataManager::~CExtFormulaDataManager()
{
	if( m_pFormulaParser != NULL )
	{
		delete m_pFormulaParser;
		m_pFormulaParser = NULL;
	}
	if( m_pFormulaAlphabet != NULL )
	{
		delete m_pFormulaAlphabet;
		m_pFormulaAlphabet = NULL;
	}
	if( m_pFormulaFunctionSet != NULL )
	{
		delete m_pFormulaFunctionSet;
		m_pFormulaFunctionSet = NULL;
	}
	EmptyEditedLists();
	NamedRange_RemoveAll();
	_ColorBitmap_EmptyMap();
}

void CExtFormulaDataManager::_ColorBitmap_EmptyMap()
{
POSITION pos = m_mapColorBitmaps.GetStartPosition();
	for( ; pos != NULL; )
	{
		COLORREF clr;
		CExtBitmap * pBmp = NULL;
		m_mapColorBitmaps.GetNextAssoc( pos, clr, pBmp );
		__EXT_DEBUG_GRID_ASSERT( pBmp != NULL );
		delete pBmp;
	}
	m_mapColorBitmaps.RemoveAll();
}

CExtBitmap * CExtFormulaDataManager::_ColorBitmap_Get( COLORREF clr )
{
CExtBitmap * pBmp = NULL;
	if( m_mapColorBitmaps.Lookup( clr, pBmp ) )
	{
		__EXT_DEBUG_GRID_ASSERT( pBmp != NULL );
		return pBmp;
	}
	pBmp = NULL;
	try
	{
		pBmp = new CExtBitmapCache;
		static SIZE g_dimension = { 8, 8 };
		__EXT_DEBUG_GRID_VERIFY( pBmp->FromColor( clr, g_dimension, BYTE(255), true ) );
		m_mapColorBitmaps.SetAt( clr, pBmp );
		return pBmp;
	}
	catch( CException * pException )
	{
		pException->Delete();
	}
	if( pBmp != NULL )
		delete pBmp;
	return false;
}

const CExtBitmap * CExtFormulaDataManager::_ColorBitmap_Get( COLORREF clr ) const
{
	return ( const_cast < CExtFormulaDataManager * > ( this ) ) -> _ColorBitmap_Get( clr );
}

void CExtFormulaDataManager::EmptyEditedLists()
{
	m_listEditedFormulaRanges.RemoveAll();
	m_listEditedFunctions.RemoveAll();
	m_pFormulaItemRangeInGridHT = NULL;
	m_bInGridHtLeft = m_bInGridHtTop = m_bInGridHtRight = m_bInGridHtBottom = false;
	m_rcInGridHT.SetRect( -32767, -32767, -32767, -32767 );
}

const CExtGridDataProvider & CExtFormulaDataManager::OnQueryDataProvider() const
{
	return ( const_cast < CExtFormulaDataManager * > ( this ) ) -> OnQueryDataProvider();
}

CExtFormulaAlphabet & CExtFormulaDataManager::OnQueryFormulaAlphabet()
{
	if( m_pFormulaAlphabet == NULL )
		m_pFormulaAlphabet = new CExtFormulaAlphabet;
	return (*m_pFormulaAlphabet);
}

const CExtFormulaAlphabet & CExtFormulaDataManager::OnQueryFormulaAlphabet() const
{
	return ( const_cast < CExtFormulaDataManager * > ( this ) ) -> OnQueryFormulaAlphabet();
}

CExtFormulaParser & CExtFormulaDataManager::OnQueryFormulaParser()
{
	if( m_pFormulaParser == NULL )
		m_pFormulaParser = new CExtFormulaParser;
	return (*m_pFormulaParser);
}

const CExtFormulaParser & CExtFormulaDataManager::OnQueryFormulaParser() const
{
	return ( const_cast < CExtFormulaDataManager * > ( this ) ) -> OnQueryFormulaParser();
}

CExtFormulaFunctionSet & CExtFormulaDataManager::OnQueryFormulaFunctionSet()
{
	if( m_pFormulaFunctionSet == NULL )
	{
		m_pFormulaFunctionSet = new CExtFormulaFunctionSet;
		__EXT_DEBUG_GRID_VERIFY( m_pFormulaFunctionSet->RegisterAllFunctions() );
	}
	return (*m_pFormulaFunctionSet);
}

const CExtFormulaFunctionSet & CExtFormulaDataManager::OnQueryFormulaFunctionSet() const
{
	return ( const_cast < CExtFormulaDataManager * > ( this ) ) -> OnQueryFormulaFunctionSet();
}

CExtFormulaGridCell * CExtFormulaDataManager::GetFormulaCell( LONG nColNo, LONG nRowNo )
{
CExtGridCell * pCell = NULL;
	if( ! OnGetFormulaCell( nColNo, nRowNo, &pCell, RUNTIME_CLASS(CExtFormulaGridCell) ) )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
CExtFormulaGridCell * pFC = DYNAMIC_DOWNCAST( CExtFormulaGridCell, pCell );
	//if( pFC == NULL )
	//	return NULL;
	return pFC;
}

CExtFormulaGridCell * CExtFormulaDataManager::GetFormulaCell( CExtGridDataProvider::packed_location_t _loc )
{
CPoint pt = CExtGridDataProvider::_L2P( _loc );
	return GetFormulaCell( pt.x, pt.y );
}

bool CExtFormulaDataManager::OnComputeValue(
	CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
	LONG nColNo,
	LONG nRowNo,
	CExtFormulaValue & _FV,
	const CExtFormulaItem * pExternalFI
	)
{
	pExternalFI;
	_FV.Empty();
CExtGridCell * pCell = NULL;
	if( ! OnGetFormulaCell( nColNo, nRowNo, &pCell ) )
		return false;
	if( pCell == NULL )
	{
		_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
		_FV.m_val.vt = VT_R8;
		_FV.m_val.dblVal = 0.0;
		return true;
	}
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
CSize sizeJoin = pCell->JoinGet();
	if( sizeJoin.cx <= 0 || sizeJoin.cy <= 0 )
	{
		_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
		_FV.m_val.vt = VT_R8;
		_FV.m_val.dblVal = 0.0;
		return true;
	}
CExtFormulaGridCell * pFC = DYNAMIC_DOWNCAST( CExtFormulaGridCell, pCell );
	if( pFC == NULL )
	{
		_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
		pCell->GetVariant( _FV.m_val );
		return true;
	}
CExtSafeString strFormulaText;
	if( ! pFC->FormulaTextGet( strFormulaText ) )
	{
		_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
		pCell->GetVariant( _FV.m_val );
		return true;
	}
//CExtGridDataProvider::packed_location_t _locTest = 0L;
CExtGridDataProvider::packed_location_t _locTest = CExtGridDataProvider::_LL2L( nColNo, nRowNo );
CExtFormulaGridCell * pCellTest = NULL;
	if( _mapCellsAlreadyComputed.Lookup( _locTest, pCellTest ) )
	{
		_FV.m_eType = CExtFormulaValue::__EFVT_OLE_VARIANT;
		pCell->GetVariant( _FV.m_val );
		return true;
	}
	//_mapCellsAlreadyComputed.SetAt( CExtGridDataProvider::_LL2L( nColNo, nRowNo ), pFC );
	_mapCellsAlreadyComputed.SetAt( _locTest, pFC );
	if( ! pFC->_FormulaComputeValueImpl( _mapCellsAlreadyComputed, nColNo, nRowNo, _FV, *this, pExternalFI ) )
		return false;
	return OnComputeDependentCells( _mapCellsAlreadyComputed, nColNo, nRowNo, false );
}

void CExtFormulaDataManager::_RFE_RemoveList( CList < CExtGridDataProvider::packed_location_t, CExtGridDataProvider::packed_location_t > & _list )
{
POSITION pos = _list.GetHeadPosition();
	for( ; pos != NULL; )
	{
		CExtGridDataProvider::packed_location_t _locWalk = _list.GetNext( pos );
		m_mapFC.RemoveKey( _locWalk );
	}
}

void CExtFormulaDataManager::_RFE_RemoveRange( CExtGR2D & _range )
{
	if( _range.IsEmpty() )
		return;
CList < CExtGridDataProvider::packed_location_t, CExtGridDataProvider::packed_location_t > _list;
POSITION pos = NULL;
	for( pos = m_mapFC.GetStartPosition(); pos != NULL; )
	{
		CExtGridDataProvider::packed_location_t _locWalk = 0L;
		CExtFormulaGridCell * pCellWalk = NULL;
		m_mapFC.GetNextAssoc( pos, _locWalk, pCellWalk );
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellWalk );
		CPoint ptWalk = CExtGridDataProvider::_L2P( _locWalk );
		if( ! _range.PtInRegion( ptWalk ) )
			continue;
		_list.AddTail( _locWalk );
	}
	_RFE_RemoveList( _list );
}

void CExtFormulaDataManager::_RFE_RemoveEntry( CExtGridDataProvider::packed_location_t _loc )
{
	m_mapFC.RemoveKey( _loc );
}

void CExtFormulaDataManager::_RFE_AddEntry( CExtGridDataProvider::packed_location_t _loc, CExtFormulaGridCell * pCell )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
	m_mapFC.SetAt( _loc, pCell );
}

void CExtFormulaDataManager::_RFE_OnRegisterFormulaCell(
	LONG nColNo,
	LONG nRowNo,
	CExtFormulaGridCell * pCell
	)
{
CExtGridDataProvider::packed_location_t _loc = CExtGridDataProvider::_UU2L( nColNo, nRowNo );
	if( pCell != NULL )
		_RFE_AddEntry( _loc, pCell );
	else
		_RFE_RemoveEntry( _loc );
}

void CExtFormulaDataManager::OnComputeMapOfAffectedCells(
	LONG nColNo,
	LONG nRowNo,
	bool bIncludeThis,
	CExtFormulaDataManager::map_formula_cells_t & _mapDependency,
	bool bDeepMode, // = true
	bool bAppendMode // = false
	)
{
	if( ! bAppendMode )
		_mapDependency.RemoveAll();
CExtGridDataProvider::packed_location_t _locThis = CExtGridDataProvider::_UU2L( nColNo, nRowNo );
CExtFormulaGridCell * pCell = NULL;
	if( m_mapFC.Lookup( _locThis, pCell ) )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
	}
POSITION pos = NULL;
	for( pos = m_mapFC.GetStartPosition(); pos != NULL; )
	{
		CExtGridDataProvider::packed_location_t _locWalk = 0L;
		CExtFormulaGridCell * pCellWalk = NULL;
		m_mapFC.GetNextAssoc( pos, _locWalk, pCellWalk );
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellWalk );
		if( (!bIncludeThis) && _locThis == _locWalk )
			continue;
		CPoint ptWalk = CExtGridDataProvider::_L2P( _locWalk );
		if( bAppendMode )
		{
			CExtFormulaGridCell * pCellWalk2 = NULL;
			if( _mapDependency.Lookup( _locWalk, pCellWalk2 ) )
			{
//				CRect rcLocation( ptWalk.x, ptWalk.y, nColNo, nRowNo );
//				pCellWalk->FormulaSetCrossReferenceError( *this, rcLocation );
//				if( pCell != NULL )
//				{
//					__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
//					rcLocation.SetRect( nColNo, nRowNo, ptWalk.x, ptWalk.y );
//					pCell->FormulaSetCrossReferenceError( *this, rcLocation );
//				}
				continue;
			}
		}
		CExtGR2D _range;
		pCellWalk->FormulaComputeDependencySourceRange( *this, _range );
		if( _range.PtInRegion( nColNo, nRowNo ) )
		{
			_mapDependency.SetAt( _locWalk, pCellWalk );
			//CPoint ptWalk = CExtGridDataProvider::_L2P( _locWalk );
			if( bDeepMode )
				OnComputeMapOfAffectedCells( ptWalk.x, ptWalk.y, false, _mapDependency, true, true );
		}
	}
}

void CExtFormulaDataManager::OnComputeMapOfRequiredCells(
	LONG nColNo,
	LONG nRowNo,
	CExtFormulaDataManager::map_formula_cells_t & _mapDependency
	)
{
	_mapDependency.RemoveAll();
CExtGridDataProvider::packed_location_t _loc = CExtGridDataProvider::_UU2L( nColNo, nRowNo );
CExtFormulaGridCell * pCell = NULL;
	if( ! m_mapFC.Lookup( _loc, pCell ) )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
CExtGR2D _range;
	pCell->FormulaComputeDependencySourceRange( *this, _range );
POSITION pos = NULL;
	for( pos = m_mapFC.GetStartPosition(); pos != NULL; )
	{
		CExtGridDataProvider::packed_location_t _locWalk = 0L;
		CExtFormulaGridCell * pCellWalk = NULL;
		m_mapFC.GetNextAssoc( pos, _locWalk, pCellWalk );
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellWalk );
		CPoint ptWalk = CExtGridDataProvider::_L2P( _locWalk );
		if( _range.PtInRegion( ptWalk ) )
			_mapDependency.SetAt( _locWalk, pCellWalk );
	}
}

//#if (defined _DEBUG)
//CExtSafeString stat_DebugIndent( INT nIndent )
//{
//CExtSafeString s = _T("");
//	for( INT i = 0; i < nIndent; i ++ )
//		s += _T("     ");
//	return s;
//}
//CExtSafeString stat_DebugTraceMap( CExtFormulaDataManager & _FDM, CExtFormulaDataManager::map_formula_cells_t & _map )
//{
//CExtSafeString s = _T("");
//bool bFirst = true;
//POSITION pos = _map.GetStartPosition();
//	for( ; pos != NULL; )
//	{
//		if( ! bFirst )
//			s += _T(", ");
//		bFirst = false;
//		CExtGridDataProvider::packed_location_t _locWalk = 0L;
//		CExtFormulaGridCell * pCellWalk = NULL;
//		_map.GetNextAssoc( pos, _locWalk, pCellWalk );
//		s += _FDM.OnQueryFormulaAlphabet().GetLocationStr( _locWalk );
//	}
//	if( bFirst )
//		s = _T("*nothing*");
//	return s;
//}
//#endif // (defined _DEBUG)

void CExtFormulaDataManager::OnComputeDependentCellsSequence(
	CExtFormulaDataManager::map_formula_cells_t & _mapCellsToCompute,
	CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
	CExtFormulaDataManager::list_computation_sequence_t & _listSequence
	)
{
POSITION pos = NULL;
	for( pos = _mapCellsToCompute.GetStartPosition(); pos != NULL; pos = _mapCellsToCompute.GetStartPosition() )
	{
		CExtGridDataProvider::packed_location_t _locWalk = 0L;
		CExtFormulaGridCell * pCellWalk = NULL, * pCellWalk2 = NULL;
		_mapCellsToCompute.GetNextAssoc( pos, _locWalk, pCellWalk );
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellWalk );
		_mapCellsToCompute.RemoveKey( _locWalk );
		if( _mapCellsAlreadyComputed.Lookup( _locWalk, pCellWalk2 )  )
		{
			__EXT_DEBUG_GRID_ASSERT( LPVOID(pCellWalk) == LPVOID(pCellWalk2) );
			continue;
		}
		CPoint ptWalk = CExtGridDataProvider::_L2P( _locWalk );
		_mapCellsAlreadyComputed.SetAt( _locWalk, pCellWalk );
		CExtFormulaDataManager::map_formula_cells_t _mapCellsToCompute_NextStep;
		OnComputeMapOfRequiredCells( ptWalk.x, ptWalk.y, _mapCellsToCompute_NextStep );
		OnComputeDependentCellsSequence( _mapCellsToCompute_NextStep, _mapCellsAlreadyComputed, _listSequence );
		_listSequence.AddTail( _locWalk );
	}
}

void CExtFormulaDataManager::OnComputeCrossReferencesStep(
	CExtGridDataProvider::packed_location_t _loc,
	CExtFormulaDataManager::map_cross_reference_t & _mapCrossReference,
	CExtFormulaDataManager::map_formula_cells_t & _mapChain
//#if (defined _DEBUG)
//		, INT nIndent // = 0
//#endif // (defined _DEBUG)
	)
{
	_mapChain.SetAt( _loc, GetFormulaCell( _loc ) );
CExtFormulaDataManager::map_formula_cells_t _mapAffected;
	OnComputeMapOfAffectedCells( _loc, true, _mapAffected, false );
//TRACE3(
//	"%string %s which affects to %s\r\n",
//	LPCTSTR(stat_DebugIndent(nIndent)),
//	LPCTSTR(OnQueryFormulaAlphabet().GetLocationStr( _loc ) ),
//	LPCTSTR(stat_DebugTraceMap( *this, _mapAffected ))
//	);
POSITION pos = _mapAffected.GetStartPosition();
	for( ; pos != NULL; )
	{
		CExtGridDataProvider::packed_location_t _locAffected;
		CExtFormulaGridCell * pCellAffected = NULL;
		_mapAffected.GetNextAssoc( pos, _locAffected, pCellAffected );
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellAffected );
		bool bCross = false;
 			CExtFormulaGridCell * pCellBack = NULL;
			if( _mapChain.Lookup( _locAffected, pCellBack ) )
			{
				_mapCrossReference.SetAt( _locAffected, _loc );
				_mapCrossReference.SetAt( _loc, _locAffected );
//TRACE3(
//	"%s-- -- -- -- -- -- -- cross reference %s <--> %s\r\n",
//	LPCTSTR(stat_DebugIndent(nIndent)),
//	LPCTSTR(OnQueryFormulaAlphabet().GetLocationStr( _locAffected ) ),
//	LPCTSTR(OnQueryFormulaAlphabet().GetLocationStr( _loc ) )
//	);
				bCross = true;
			}

		if( ! bCross )
			OnComputeCrossReferencesStep( _locAffected, _mapCrossReference, _mapChain
//#if (defined _DEBUG)
//				, nIndent + 1
//#endif // (defined _DEBUG)
				);
	}
	_mapChain.RemoveKey( _loc );
}

void CExtFormulaDataManager::OnComputeCrossReferences(
	CExtGridDataProvider::packed_location_t _loc,
	CExtFormulaDataManager::map_cross_reference_t & _mapCrossReference
	)
{
CExtFormulaDataManager::map_formula_cells_t _mapChain;
	OnComputeCrossReferencesStep( _loc, _mapCrossReference, _mapChain );
}

bool CExtFormulaDataManager::OnComputeDependentCells(
	CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed,
	LONG nColNo,
	LONG nRowNo,
	bool bComputeThis
	)
{
CExtFormulaDataManager::map_formula_cells_t _mapCellsToCompute;
	OnComputeMapOfAffectedCells( nColNo, nRowNo, true, _mapCellsToCompute );
CExtFormulaDataManager::list_computation_sequence_t _listSequence;
CExtFormulaDataManager::map_cross_reference_t _mapCrossReference;
	OnComputeDependentCellsSequence( _mapCellsToCompute, _mapCellsAlreadyComputed, _listSequence );
bool bRetVal = true;
POSITION pos = _listSequence.GetHeadPosition();
	for( ; pos != NULL; )
	{
		CExtGridDataProvider::packed_location_t _loc = _listSequence.GetNext( pos );
		CPoint pt = CExtGridDataProvider::_L2P( _loc );
		CExtGridCell * pCellTmp = NULL;
		__EXT_DEBUG_GRID_VERIFY( OnGetFormulaCell( pt.x, pt.y, &pCellTmp ) );
//TRACE1( "computing %s\r\n", LPCTSTR(OnQueryFormulaAlphabet().GetLocationStr(pt)) );
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellTmp );
		CExtFormulaGridCell * pCell = STATIC_DOWNCAST( CExtFormulaGridCell, pCellTmp );
		pCell->FormulaSetError( CExtFormulaValue::__EFEC_OK );
		if( bComputeThis || ( ! ( pt.x == nColNo && pt.y == nRowNo ) ) )
		{
			CExtFormulaValue _FV;
			if( pCell->_FormulaComputeValueImpl( _mapCellsAlreadyComputed, pt.x, pt.y, _FV, *this, NULL ) )
				pCell->_VariantAssign( _FV.m_val );
			else
				bRetVal = false;
		}
		OnComputeCrossReferences( _loc, _mapCrossReference );
	}
	pos = _mapCrossReference.GetStartPosition();
	for( ; pos != NULL; )
	{
		CExtGridDataProvider::packed_location_t _locFrom = 0L, _locTo = 0L;
		_mapCrossReference.GetNextAssoc( pos, _locFrom, _locTo );
		CPoint ptFrom = CExtGridDataProvider::_L2P( _locFrom );
		CExtGridCell * pCellTmp = NULL;
		if( ! OnGetFormulaCell( ptFrom.x, ptFrom.y, &pCellTmp ) )
			continue;
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellTmp );
		CExtFormulaGridCell * pCell = DYNAMIC_DOWNCAST( CExtFormulaGridCell, pCellTmp );
		CPoint ptTo = CExtGridDataProvider::_L2P( _locTo );
		CRect rcLocation( ptFrom, ptTo );
		pCell->FormulaSetCrossReferenceError( *this, rcLocation );
	}
	return bRetVal;
}

bool CExtFormulaDataManager::NamedRange_IsValidName( __EXT_MFC_SAFE_LPCTSTR strName ) const
{
	if( LPCTSTR(strName) == NULL )
		return false;
INT nLen = INT( _tcslen(strName) );
	if( nLen == 0 )
		return false;
	return true;
}

LONG CExtFormulaDataManager::NamedRange_GetCount() const
{
LONG nCount = LONG( m_mapNamedRanges.GetCount() );
	return nCount;
}

POSITION CExtFormulaDataManager::NamedRange_GetStartPosition() const
{
	return m_mapNamedRanges.GetStartPosition();
}

CExtGR2D * CExtFormulaDataManager::NamedRange_GetNext( POSITION & pos, CExtSafeString & strName )
{
	if( pos == NULL )
	{
		strName.Empty();
		return NULL;
	}
void * pRange = NULL;
	m_mapNamedRanges.GetNextAssoc( pos, strName, pRange );
	return ((CExtGR2D*)pRange);
}

const CExtGR2D * CExtFormulaDataManager::NamedRange_GetNext( POSITION & pos, CExtSafeString & strName ) const
{
	return ( const_cast < CExtFormulaDataManager * > ( this ) -> NamedRange_GetNext( pos, strName ) );
}

CExtGR2D * CExtFormulaDataManager::NamedRange_Get( __EXT_MFC_SAFE_LPCTSTR strName )
{
void * pRangeVoid = NULL;
	if( ! m_mapNamedRanges.Lookup( strName, pRangeVoid ) )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT( pRangeVoid != NULL );
CExtGR2D * pRange = (CExtGR2D*)pRangeVoid;
	return pRange;
}

const CExtGR2D * CExtFormulaDataManager::NamedRange_Get( __EXT_MFC_SAFE_LPCTSTR strName ) const
{
	return ( const_cast < CExtFormulaDataManager * > ( this ) -> NamedRange_Get( strName ) );
}

bool CExtFormulaDataManager::NamedRange_Set( __EXT_MFC_SAFE_LPCTSTR strName, CExtGR2D * pRange, bool bClone )
{
	if( ! NamedRange_IsValidName( strName ) )
		return false;
CExtGR2D * pRange2 = NamedRange_Get( strName );
	if( pRange2 != NULL )
	{
		if( LPVOID(pRange2) != LPVOID(pRange) )
		{
			if( pRange != NULL )
			{
				if( bClone )
					(*pRange2) = (*pRange);
				else
				{
					m_mapNamedRanges.SetAt( strName, pRange );
					delete pRange2;
				}
			}
			else
				pRange2->Empty();
		} // if( LPVOID(pRange2) != LPVOID(pRange) )
		return true;
	} // if( pRange2 != NULL )
	if( bClone )
	{
		try
		{
			pRange2 = new CExtGR2D;
			(*pRange2) = (*pRange);
		}
		catch( CException * pException )
		{
			pException->Delete();
			return false;
		}
	} // if( bClone )
	else
		pRange2 = pRange;
	m_mapNamedRanges.SetAt( strName, pRange2 );
	return true;
}

bool CExtFormulaDataManager::NamedRange_Set( __EXT_MFC_SAFE_LPCTSTR strName, const CExtGR2D & _range )
{
	return NamedRange_Set( strName, const_cast < CExtGR2D * > ( &_range ), true );
}

bool CExtFormulaDataManager::NamedRange_Remove( __EXT_MFC_SAFE_LPCTSTR strName )
{
void * pRangeVoid = NULL;
	if( ! m_mapNamedRanges.Lookup( strName, pRangeVoid ) )
		return false;
	__EXT_DEBUG_GRID_ASSERT( pRangeVoid != NULL );
	m_mapNamedRanges.RemoveKey( strName );
CExtGR2D * pRange = (CExtGR2D*)pRangeVoid;
	delete pRange;
	return true;
}

LONG CExtFormulaDataManager::NamedRange_RemoveAll()
{
LONG nRemovedCount = 0L;
POSITION pos = m_mapNamedRanges.GetStartPosition();
	for( ; pos != NULL; )
	{
		CExtSafeString strName;
		void * pRangeVoid = NULL;
		m_mapNamedRanges.GetNextAssoc( pos, strName, pRangeVoid );
		__EXT_DEBUG_GRID_ASSERT( pRangeVoid != NULL );
		CExtGR2D * pRange = (CExtGR2D*)pRangeVoid;
		delete pRange;
		nRemovedCount ++;
	}
	m_mapNamedRanges.RemoveAll();
	return nRemovedCount;
}

/////////////////////////////////////////////////////////////////////////////
// CExtFormulaGridWnd

IMPLEMENT_DYNCREATE( CExtFormulaGridWnd, CExtGridWnd );

CExtFormulaGridWnd::CExtFormulaGridWnd()
	: m_pFormulaDataManager( NULL )
	, m_dwFgStyle( __FGS_DEFAULT )
	, m_dwFgStyleEx( __FGS_EX_DEFAULT )
	, m_rcFormulaGridToolRectSelArea( -1, -1, -1, -1 )
	, m_rcFormulaGridToolRectHighlightArea( -1, -1, -1, -1 )
	, m_rcFormulaGridToolRectDND( -1, -1, -1, -1 )
{
	m_dwSupportedAccelCommands		= __EGSA_ALL & (~(__EGSA_IN_COLUMN_TAB|__EGSA_TAB_NO_FOCUS));
	m_hCursorFormulaRangeNWSE		= ::LoadCursor( NULL, IDC_SIZENWSE );
	m_hCursorFormulaRangeNESW		= ::LoadCursor( NULL, IDC_SIZENESW );
	m_hCursorFormulaRangeMove		= ::LoadCursor( NULL, IDC_SIZEALL );
CWinApp * pApp = ::AfxGetApp();
	__EXT_DEBUG_GRID_ASSERT_VALID( pApp );
	m_hCursorDefaultInnerFC			= pApp->LoadCursor( MAKEINTRESOURCE( IDC_EXT_HOLLOW_CROSS_NORMAL ) );
	__EXT_DEBUG_GRID_ASSERT( m_hCursorDefaultInnerFC != NULL );
	m_hCursorFormulaRectTool		= pApp->LoadCursor( MAKEINTRESOURCE( IDC_EXT_SMALL_TOOL_CROSS_NORMAL ) );
	__EXT_DEBUG_GRID_ASSERT( m_hCursorFormulaRectTool != NULL );
	m_hCursorFormulaRectToolPlus	= pApp->LoadCursor( MAKEINTRESOURCE( IDC_EXT_SMALL_TOOL_CROSS_NORMAL_PLUS ) );
	__EXT_DEBUG_GRID_ASSERT( m_hCursorFormulaRectToolPlus != NULL );
	m_hCursorFormulaRangeCopy		= pApp->LoadCursor( MAKEINTRESOURCE( IDC_EXT_COPY_EFFECT_ARROW ) );
	__EXT_DEBUG_GRID_ASSERT( m_hCursorFormulaRangeCopy != NULL );
	//AdvModifyStyle( __EGWS_ADV_ENABLE_DATA_DND_DRAG|__EGWS_ADV_ENABLE_DATA_DND_DROP, 0, false );
}

CExtFormulaGridWnd::~CExtFormulaGridWnd()
{
	if( m_pFormulaDataManager != NULL )
	{
		delete m_pFormulaDataManager;
		m_pFormulaDataManager = NULL;
	}
	if( m_hCursorFormulaRangeNWSE != NULL )
		::DestroyCursor( m_hCursorFormulaRangeNWSE );
	if( m_hCursorFormulaRangeNESW != NULL )
		::DestroyCursor( m_hCursorFormulaRangeNESW );
	if( m_hCursorFormulaRangeMove != NULL )
		::DestroyCursor( m_hCursorFormulaRangeMove );
	if( m_hCursorFormulaRangeCopy != NULL )
		::DestroyCursor( m_hCursorFormulaRangeCopy );
	if( m_hCursorDefaultInnerFC != NULL )
		::DestroyCursor( m_hCursorDefaultInnerFC );
	if( m_hCursorFormulaRectTool != NULL )
		::DestroyCursor( m_hCursorFormulaRectTool );
	if( m_hCursorFormulaRectToolPlus != NULL )
		::DestroyCursor( m_hCursorFormulaRectToolPlus );
}

DWORD CExtFormulaGridWnd::FgGetStyle() const
{
	__EXT_DEBUG_GRID_ASSERT( this != NULL );
	return m_dwFgStyle;
}

DWORD CExtFormulaGridWnd::FgModifyStyle(
	DWORD dwStyleAdd,
	DWORD dwStyleRemove, // = 0L
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT( this != NULL );
	if( bRedraw )
	{
		if(		m_hWnd == NULL
			||	( ! ::IsWindow(m_hWnd) )
			)
			bRedraw = false;
	}
	if( bRedraw )
		SendMessage( WM_CANCELMODE );
DWORD dwFgStyleOld = m_dwFgStyle;
	m_dwFgStyle &= ~dwStyleRemove;
	m_dwFgStyle |= dwStyleAdd;
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw )
	return dwFgStyleOld;
}

DWORD CExtFormulaGridWnd::FgGetStyleEx() const
{
	__EXT_DEBUG_GRID_ASSERT( this != NULL );
	return m_dwFgStyleEx;
}

DWORD CExtFormulaGridWnd::FgModifyStyleEx(
	DWORD dwStyleExAdd,
	DWORD dwStyleExRemove, // = 0L
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT( this != NULL );
	if( bRedraw )
	{
		if(		m_hWnd == NULL
			||	( ! ::IsWindow(m_hWnd) )
			)
			bRedraw = false;
	} 
	if( bRedraw )
		SendMessage( WM_CANCELMODE );
DWORD dwFgStyleExOld = m_dwFgStyleEx;
	m_dwFgStyleEx &= ~dwStyleExRemove;
	m_dwFgStyleEx |= dwStyleExAdd;
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw )
	return dwFgStyleExOld;
}

bool CExtFormulaGridWnd::DefaultInit(
	LONG nColCount,
	LONG nRowCount
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return false;
	if( nColCount <= 0L )
		return false;
	if( nRowCount < 0L )
		return false;
CExtFormulaDataManager & _FDM = OnGridQueryFormulaDataManager();
CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
CPoint ptAlphabetMax = _FA.GetMaxValue();
	if( nColCount > 0L && (nColCount-1L) > ptAlphabetMax.x )
		return false;
	if( nRowCount > 0L && (nRowCount-1L) > ptAlphabetMax.y )
		return false;
	RowRemoveAll( false );
	ColumnRemoveAll( false );
	OuterRowCountTopSet( 1L, false );
	OuterRowCountBottomSet( 0L, false );
	OuterColumnCountLeftSet( 1L, false );
	OuterColumnCountRightSet( 0L, false );
	OuterColumnWidthSet( true, 0L, PmBridge_GetPM()->UiScalingDo( 80, CExtPaintManager::__EUIST_X ) );
	SiwModifyStyle(
		__ESIS_STH_ITEM|__ESIS_STV_ITEM
			|__EGBS_SFM_CELLS_HV
			|__EGBS_SF_SELECT_OUTER
			//|__EGBS_AUTO_FOCUS_BOTTOM_RIGHT
			|__EGBS_RESIZING_CELLS_OUTER //|__EGBS_DYNAMIC_RESIZING
			|__EGBS_MULTI_AREA_SELECTION|__EGBS_NO_HIDE_SELECTION|__EGBS_LBEXT_SELECTION
				// |__EGBS_SUBTRACT_SEL_AREAS|__EGBS_AUTO_FOCUS_BOTTOM_RIGHT
			|__EGBS_GRIDLINES
			,
		0, false
		);
	SiwModifyStyleEx(
		__EGWS_EX_PM_COLORS|__EGWS_EX_USE_THEME_API
			|__EGBS_EX_HVI_EVENT_CELLS|__EGBS_EX_HVO_EVENT_CELLS
			|__EGBS_EX_HVO_HIGHLIGHT_CELL
			|__EGBS_EX_SO_HIGHLIGHT_COLUMNS|__EGBS_EX_SO_HIGHLIGHT_ROWS
			|__EGBS_EX_CELL_EXPANDING
			|__EGBS_EX_CORNER_AREAS_3D
			|__EGBS_EX_CORNER_AREAS_CURVE
			,
		0, false
		);
	BseModifyStyle(
		__EGWS_BSE_EDIT_CELLS_INNER|__EGWS_BSE_EDIT_RETURN_MOVES_NEXT_ROW,
		__EGWS_BSE_EDIT_RETURN_CLICK, false
		);
	BseModifyStyleEx(
			__EGBS_BSE_EX_HIGHLIGHT_PRESSING_COLUMNS_OUTER|__EGBS_BSE_EX_HIGHLIGHT_PRESSING_ROWS_OUTER|__EGBS_BSE_EX_HIGHLIGHT_PRESSING_STAY
				|__EGBS_BSE_EX_DBLCLK_BEST_FIT_COLUMN_OUTER
				|__EGBS_BSE_EX_DBLCLK_BEST_FIT_ROW_OUTER
				|__EGBS_BSE_EX_DBLCLK_BEST_FIT_VISIBLE_COLUMNS
				|__EGBS_BSE_EX_DBLCLK_BEST_FIT_VISIBLE_ROWS
				|__EGBS_BSE_EX_DBLCLK_BEST_FIT_COLUMN_MEASURE_OUTER
				|__EGBS_BSE_EX_DBLCLK_BEST_FIT_ROW_MEASURE_OUTER
				|__EGBS_BSE_EX_DBLCLK_BEST_FIT_COLUMN_MEASURE_INNER
				|__EGBS_BSE_EX_DBLCLK_BEST_FIT_ROW_MEASURE_INNER
			,
		0, false
		);
	ColumnAdd( nColCount, false );
	RowAdd( nRowCount, false );
	GridCellGet( 0L, 0L, -1, -1, RUNTIME_CLASS(CExtGridCellHeader) ); // top-left cell
	OnSwUpdateScrollBars();
	OnSwInvalidate( true );
	return true;
}

bool CExtFormulaGridWnd::FormulaSet(
	LONG nColNo,
	LONG nRowNo,
	__EXT_MFC_SAFE_LPCTSTR strFormulaText, // = NULL
	bool bErasePrev, // = true
	bool bRecompute, // = true
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtFormulaGridCell * pFC = NULL;
CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 0, 0, RUNTIME_CLASS( CExtFormulaGridCell ) );
	if( pCell != NULL )
		pFC = DYNAMIC_DOWNCAST( CExtFormulaGridCell, pCell );
	if( pFC == NULL )
	{
		if( ! bErasePrev )
			return 0L;
		if( ! GridCellSet( nColNo, nRowNo, NULL, 1L, 1L, 0, 0, false ) )
			return false;
			pCell = GridCellGet( nColNo, nRowNo, 0, 0, RUNTIME_CLASS( CExtFormulaGridCell ) );
		if( pCell == NULL )
			return false;
		pFC = STATIC_DOWNCAST( CExtFormulaGridCell, pCell );
	} // if( pFC == NULL )
	__EXT_DEBUG_GRID_ASSERT_VALID( pFC );
bool bEqualFormulaText = false, bEmptyFormulaText = ( LPCTSTR(strFormulaText) == NULL || _tcslen( LPCTSTR(strFormulaText) ) == 0 ) ? true : false;
CExtSafeString strFormulaTextOld;
	if( ! bEmptyFormulaText )
	{
		if( ! pFC->FormulaTextGet( strFormulaTextOld ) )
			strFormulaTextOld.Empty();
		else if( strFormulaTextOld == LPCTSTR(strFormulaText) )
			bEqualFormulaText = true;
	} // if( ! bEmptyFormulaText )
CExtFormulaDataManager & _FDM = OnGridQueryFormulaDataManager();
	if( ! bEqualFormulaText )
	{
		if( ! pFC->FormulaTextSet( bEmptyFormulaText ? _T("") : LPCTSTR(strFormulaText) ) )
			return false;
	} // if( ! bEqualFormulaText )
	_FDM._RFE_OnRegisterFormulaCell( nColNo, nRowNo, bEmptyFormulaText ? NULL : pFC );
bool bRetVal = true;
	if( bRecompute )
	{
		CExtFormulaDataManager::map_formula_cells_t _mapCellsAlreadyComputed;
		if( bEmptyFormulaText )
			pFC->_VariantAssign( _T("") );
		else
		{
			CExtFormulaValue _FV;
			if( ( bRetVal = pFC->_FormulaComputeValueImpl( _mapCellsAlreadyComputed, nColNo, nRowNo, _FV, _FDM, NULL ) ) != false )
				pFC->_VariantAssign( _FV.m_val );
		}
		_mapCellsAlreadyComputed.SetAt( CExtGridDataProvider::_LL2L( nColNo, nRowNo ), pFC );
		_FDM.OnComputeDependentCells( _mapCellsAlreadyComputed, nColNo, nRowNo, false );
	}
	if( bRedraw )
	{
		OnSwInvalidate( true );
		//OnSwUpdateWindow();
	}
	return bRetVal;
}

bool CExtFormulaGridWnd::FormulaShift(
	LONG nColNo,
	LONG nRowNo,
	LONG nShiftX,
	LONG nShiftY,
	const CExtGR2D * pRangeToShift, // = NULL
	bool bRecompute, // = true
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( nShiftX == 0L && nShiftY == 0L )
		return true;
CExtFormulaGridCell * pFC = NULL;
CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 0, 0, RUNTIME_CLASS( CExtFormulaGridCell ) );
	if( pCell != NULL )
		pFC = DYNAMIC_DOWNCAST( CExtFormulaGridCell, pCell );
	if( pFC == NULL )
		return false;
	__EXT_DEBUG_GRID_ASSERT_VALID( pFC );
CExtFormulaDataManager & _FDM = OnGridQueryFormulaDataManager();
	if( pFC->OnFormatShiftedText( _FDM, nShiftX, nShiftY, pRangeToShift ) )
		return false;
	//_FDM._RFE_OnRegisterFormulaCell( nColNo, nRowNo, bEmptyFormulaText ? NULL : pFC );
bool bRetVal = true;
	if( bRecompute )
	{
		CExtFormulaDataManager::map_formula_cells_t _mapCellsAlreadyComputed;
		CExtFormulaValue _FV;
		if( ( bRetVal = pFC->_FormulaComputeValueImpl( _mapCellsAlreadyComputed, nColNo, nRowNo, _FV, _FDM, NULL ) ) != false )
			pFC->_VariantAssign( _FV.m_val );
		_mapCellsAlreadyComputed.SetAt( CExtGridDataProvider::_LL2L( nColNo, nRowNo ), pFC );
		_FDM.OnComputeDependentCells( _mapCellsAlreadyComputed, nColNo, nRowNo, false );
	}
	if( bRedraw )
	{
		OnSwInvalidate( true );
		//OnSwUpdateWindow();
	}
	return bRetVal;
}

bool CExtFormulaGridWnd::FormulaErase(
	LONG nColNo,
	LONG nRowNo,
	bool bRecompute, // = true
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return FormulaSet( nColNo, nRowNo, NULL, false, bRecompute, bRedraw );
}

LONG CExtFormulaGridWnd::FormulaErase(
	CExtGR2D * pRange, // = NULL // NULL - erase all formulas
	bool bEraseNonFormulas, // = true
	bool bRecompute, // = true
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( pRange == NULL )
	{
		LONG nColCount = ColumnCountGet();
		if( nColCount == 0L )
			return 0L;
		LONG nRowCount = RowCountGet();
		if( nRowCount == 0L )
			return 0L;
		CRect rcRange( 0L, 0L, nColCount - 1L, nRowCount - 1L );
		CExtGR2D _range( rcRange );
		return FormulaErase( &_range, bRecompute, bRedraw );
	} // if( pRange == NULL )
for_each_erase_t _FE( true, bEraseNonFormulas, false, *this, *pRange );
	if( ! _FE.ComputeRange() )
		return 0L;
LONG nCount = bEraseNonFormulas ? _FE.m_nCounter : _FE.m_nCounterFormula;
	if( bRedraw && nCount == 0L )
		bRedraw = false;
	if( bRecompute )
		FormulaCompute( NULL, bRedraw );
	else if( bRedraw )
	{
		OnSwInvalidate( true );
		//OnSwUpdateWindow();
	}
	return nCount;
}

bool CExtFormulaGridWnd::FormulaDuplicateRectArea(
	CRect rcSrc,
	CRect rcDst,
	ULONG nDirection, // AFX_IDW_DOCKBAR_LEFT, AFX_IDW_DOCKBAR_TOP, AFX_IDW_DOCKBAR_RIGHT or AFX_IDW_DOCKBAR_BOTTOM
	bool bShiftFormulas,
	bool bRecompute, // = true
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( !(		nDirection == AFX_IDW_DOCKBAR_LEFT	||	nDirection == AFX_IDW_DOCKBAR_TOP
			||	nDirection == AFX_IDW_DOCKBAR_RIGHT	||	nDirection == AFX_IDW_DOCKBAR_BOTTOM
			)
		)
		return 0L;
CExtGR2D _rangeDst( rcDst );
	FormulaErase( &_rangeDst, true, false, false );
CExtGR2D _rangeSrc( rcSrc );
CRect rcWalk = rcDst;
CSize sizeSrc( rcSrc.right - rcSrc.left + 1, rcSrc.bottom - rcSrc.top + 1 );
	switch( nDirection )
	{
	case AFX_IDW_DOCKBAR_LEFT:
	{
		CSize sizeShift( -sizeSrc.cx, 0 );
		for( rcWalk.left = rcWalk.right - sizeSrc.cx + 1; rcWalk.right >= rcDst.left; rcWalk.OffsetRect( sizeShift ) )
		{
			if( rcWalk.left < rcDst.left )
			{
				rcWalk.left = rcDst.left;
				__EXT_DEBUG_GRID_ASSERT( rcWalk.left <= rcWalk.right );
				rcSrc.left = rcSrc.right - rcWalk.Width();
				_rangeSrc.Empty();
				_rangeSrc = rcSrc;
			}
			CSize sizeShiftWalk( rcWalk.TopLeft() - rcSrc.TopLeft() );
			for_each_dup_rect_area_t _FE( *this, _rangeSrc, sizeShiftWalk, bShiftFormulas );
			if( ! _FE.ComputeRange() )
				return false;
		}
	}
	break;
	case AFX_IDW_DOCKBAR_TOP:
	{
		CSize sizeShift( 0, -sizeSrc.cy );
		for( rcWalk.top = rcWalk.bottom - sizeSrc.cy + 1; rcWalk.bottom >= rcDst.top; rcWalk.OffsetRect( sizeShift ) )
		{
			if( rcWalk.top < rcDst.top )
			{
				rcWalk.top = rcDst.top;
				__EXT_DEBUG_GRID_ASSERT( rcWalk.top <= rcWalk.bottom );
				rcSrc.top = rcSrc.bottom - rcWalk.Height();
				_rangeSrc.Empty();
				_rangeSrc = rcSrc;
			}
			CSize sizeShiftWalk( rcWalk.TopLeft() - rcSrc.TopLeft() );
			for_each_dup_rect_area_t _FE( *this, _rangeSrc, sizeShiftWalk, bShiftFormulas );
			if( ! _FE.ComputeRange() )
				return false;
		}
	}
	break;
	case AFX_IDW_DOCKBAR_RIGHT:
	{
		CSize sizeShift( sizeSrc.cx, 0 );
		for( rcWalk.right = rcWalk.left + sizeSrc.cx - 1; rcWalk.left <= rcDst.right; rcWalk.OffsetRect( sizeShift ) )
		{
			if( rcWalk.right > rcDst.right )
			{
				rcWalk.right = rcDst.right;
				__EXT_DEBUG_GRID_ASSERT( rcWalk.left <= rcWalk.right );
				rcSrc.right = rcSrc.left + rcWalk.Width();
				_rangeSrc.Empty();
				_rangeSrc = rcSrc;
			}
			CSize sizeShiftWalk( rcWalk.TopLeft() - rcSrc.TopLeft() );
			for_each_dup_rect_area_t _FE( *this, _rangeSrc, sizeShiftWalk, bShiftFormulas );
			if( ! _FE.ComputeRange() )
				return false;
		}
	}
	break;
	case AFX_IDW_DOCKBAR_BOTTOM:
	{
		CSize sizeShift( 0, sizeSrc.cy );
		for( rcWalk.bottom = rcWalk.top + sizeSrc.cy - 1; rcWalk.top <= rcDst.bottom; rcWalk.OffsetRect( sizeShift ) )
		{
			if( rcWalk.bottom > rcDst.bottom )
			{
				rcWalk.bottom = rcDst.bottom;
				__EXT_DEBUG_GRID_ASSERT( rcWalk.top <= rcWalk.bottom );
				rcSrc.bottom = rcSrc.top + rcWalk.Height();
				_rangeSrc.Empty();
				_rangeSrc = rcSrc;
			}
			CSize sizeShiftWalk( rcWalk.TopLeft() - rcSrc.TopLeft() );
			for_each_dup_rect_area_t _FE( *this, _rangeSrc, sizeShiftWalk, bShiftFormulas );
			if( ! _FE.ComputeRange() )
				return false;
		}
	}
	break;
#ifdef _DEBUG
	default:
		__EXT_DEBUG_GRID_ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( nDirection )

	if( bRecompute )
	{
		CExtGR2D _rangeDst( rcDst );
		FormulaCompute( &_rangeDst, false );
	}

	if( bRedraw )
	{
		OnSwInvalidate( true );
		//OnSwUpdateWindow();
	}
	return true;
}

bool CExtFormulaGridWnd::_FormulaComputeImpl(
	LONG nColNo,
	LONG nRowNo,
	CExtFormulaDataManager::map_formula_cells_t & _mapCellsAlreadyComputed
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtFormulaDataManager & _FDM = OnGridQueryFormulaDataManager();
CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 0, 0, NULL, false, false );
	if( pCell == NULL )
	{
		_FDM._RFE_OnRegisterFormulaCell( nColNo, nRowNo, NULL );
		return false;
	}
CExtFormulaGridCell * pFC = DYNAMIC_DOWNCAST( CExtFormulaGridCell, pCell );
	if( pFC == NULL )
	{
		_FDM._RFE_OnRegisterFormulaCell( nColNo, nRowNo, NULL );
		return false;
	}
	__EXT_DEBUG_GRID_ASSERT_VALID( pFC );
CExtSafeString strFormulaText;
	_FDM._RFE_OnRegisterFormulaCell( nColNo, nRowNo, ( pFC->FormulaTextGet( strFormulaText ) && (! strFormulaText.IsEmpty() ) ) ? pFC : NULL );
bool bRetVal = true;
CExtFormulaValue _FV;
	_mapCellsAlreadyComputed.SetAt( CExtGridDataProvider::_LL2L( nColNo, nRowNo ), pFC );
	if( ( bRetVal = pFC->_FormulaComputeValueImpl( _mapCellsAlreadyComputed, nColNo, nRowNo, _FV, _FDM, NULL ) ) != false )
		pFC->_VariantAssign( _FV.m_val );
	_FDM.OnComputeDependentCells( _mapCellsAlreadyComputed, nColNo, nRowNo, false );
	return bRetVal;
}

bool CExtFormulaGridWnd::FormulaCompute(
	LONG nColNo,
	LONG nRowNo,
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtFormulaDataManager::map_formula_cells_t _mapCellsAlreadyComputed;
bool bRetVal =  _FormulaComputeImpl( nColNo, nRowNo, _mapCellsAlreadyComputed );
	if( bRedraw )
	{
		OnSwInvalidate( true );
		//OnSwUpdateWindow();
	}
	return bRetVal;
}

LONG CExtFormulaGridWnd::FormulaCompute(
	CExtGR2D * pRange, // = NULL // NULL - compute all formulas
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( pRange == NULL )
	{
		LONG nColCount = ColumnCountGet();
		if( nColCount == 0L )
			return 0L;
		LONG nRowCount = RowCountGet();
		if( nRowCount == 0L )
			return 0L;
		CRect rcRange( 0L, 0L, nColCount - 1L, nRowCount - 1L );
		CExtGR2D _range( rcRange );
		return FormulaCompute( &_range, bRedraw );
	} // if( pRange == NULL )
CExtFormulaDataManager::map_formula_cells_t _mapCellsAlreadyComputed;
for_each_compute_t _FE( *this, *pRange, _mapCellsAlreadyComputed );
	if( ! _FE.ComputeRange() )
		return 0L;
//	if( bRedraw && nCount == _FE.m_nCounterFormula )
//		bRedraw = false;
	if( bRedraw )
	{
		OnSwInvalidate( true );
		//OnSwUpdateWindow();
	}
	return _FE.m_nCounterFormula;
}

bool CExtFormulaGridWnd::FormulaComputeDependentCells(
	LONG nColNo,
	LONG nRowNo,
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtFormulaDataManager & _FDM = OnGridQueryFormulaDataManager();
CExtFormulaDataManager::map_formula_cells_t _mapCellsAlreadyComputed;
bool bRetVal = _FDM.OnComputeDependentCells( _mapCellsAlreadyComputed, nColNo, nRowNo, false );
	if( bRedraw )
	{
		OnSwInvalidate( true );
		//OnSwUpdateWindow();
	}
	return bRetVal;
}

void CExtFormulaGridWnd::FormulaComputeDependentCells(
	CExtGR2D & _range,
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( _range.IsEmpty() )
		return;
for_each_compute_dc_t _FE( *this, _range );
	if( ! _FE.ComputeRange() )
		return;
	if( bRedraw )
	{
		OnSwInvalidate( true );
		//OnSwUpdateWindow();
	}
}

#ifdef _DEBUG

void CExtFormulaGridWnd::AssertValid() const
{
	CExtNSB < CExtPPVW < CExtGridWnd > > :: AssertValid();
}

void CExtFormulaGridWnd::Dump( CDumpContext & dc ) const
{
	CExtNSB < CExtPPVW < CExtGridWnd > > :: Dump( dc );
}

#endif // _DEBUG

BEGIN_MESSAGE_MAP( CExtFormulaGridWnd, CExtGridWnd )
	//{{AFX_MSG_MAP(CExtFormulaGridWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CExtFormulaGridWnd::PreTranslateMessage( MSG * pMsg )
{
	return CExtNSB < CExtPPVW < CExtGridWnd > > :: PreTranslateMessage( pMsg );
}

BOOL CExtFormulaGridWnd::PreCreateWindow( CREATESTRUCT & cs )
{
	if( ! CExtNSB < CExtPPVW < CExtGridWnd > > :: PreCreateWindow( cs ) )
		return FALSE;
	return TRUE;
}

void CExtFormulaGridWnd::PreSubclassWindow()
{
	CExtNSB < CExtPPVW < CExtGridWnd > > :: PreSubclassWindow();
	m_rcFormulaGridToolRectSelArea.SetRect( -1, -1, -1, -1 );
	m_rcFormulaGridToolRectHighlightArea.SetRect( -1, -1, -1, -1 );
	m_rcFormulaGridToolRectDND.SetRect( -1, -1, -1, -1 );
///	PostMessage( (WM_USER+321) );
}

void CExtFormulaGridWnd::PostNcDestroy()
{
	m_rcFormulaGridToolRectSelArea.SetRect( -1, -1, -1, -1 );
	m_rcFormulaGridToolRectHighlightArea.SetRect( -1, -1, -1, -1 );
	m_rcFormulaGridToolRectDND.SetRect( -1, -1, -1, -1 );
	CExtNSB < CExtPPVW < CExtGridWnd > > :: PostNcDestroy();
}

LRESULT CExtFormulaGridWnd::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
LRESULT lResult = CExtNSB < CExtPPVW < CExtGridWnd > > :: WindowProc( message, wParam, lParam );
//	switch( message )
//	{
//	case (WM_USER+321):
//	{
//		CExtScrollBar * pExtSB = NULL;
//		CScrollBar * pSB = NULL;
//		pSB = GetScrollBarCtrl( SB_HORZ );
//		if( pSB != NULL )
//		{
//			ASSERT_VALID( pSB );
//			pExtSB = DYNAMIC_DOWNCAST( CExtScrollBar, pSB );
//			if( pExtSB != NULL )
//				pExtSB->m_bHelperLightAccent = false;
//		}
//		pSB = GetScrollBarCtrl( SB_VERT );
//		if( pSB != NULL )
//		{
//			ASSERT_VALID( pSB );
//			pExtSB = DYNAMIC_DOWNCAST( CExtScrollBar, pSB );
//			if( pExtSB != NULL )
//				pExtSB->m_bHelperLightAccent = false;
//		}
//	}
//	break;
//	} // switch( message )
	return lResult;
}

bool CExtFormulaGridWnd::OnGridFilterInplaceEditingMessageLoop(
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcInplaceControl,
	CExtGridCell & _cell,
	HWND hWndInplaceControl,
	MSG & _msg,
	bool & bRemoveMessage
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nVisibleColNo; nVisibleRowNo; nColNo; nRowNo; nColType; nRowType;
	rcCellExtra; rcCell; rcInplaceControl; _cell; hWndInplaceControl; _msg;
CExtFormulaGridCell * pFC = DYNAMIC_DOWNCAST( CExtFormulaGridCell, (&_cell) );
	if( pFC != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pFC );
		CWnd * pWndInplaceControl = CWnd::FromHandlePermanent( hWndInplaceControl );
		if( pWndInplaceControl != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pWndInplaceControl );
			CExtFormulaInplaceEdit * pWndFIE = DYNAMIC_DOWNCAST( CExtFormulaInplaceEdit, pWndInplaceControl );
			if( pWndFIE != NULL )
			{
				CExtFormulaInplaceListBox * pWndIPLB = pWndFIE->OnQueryIPLB( false );
				if( pWndIPLB != NULL && ( pWndIPLB->GetStyle() & WS_VISIBLE ) != 0 )
				{
					if(		_msg.hwnd == pWndIPLB->m_hWnd
						||	::IsChild( pWndIPLB->m_hWnd, _msg.hwnd )
						)
					{
						//DispatchMessage( &_msg );
						//::SendMessage( _msg.hwnd, _msg.message, _msg.wParam, _msg.lParam );
						bRemoveMessage = false;
						::AfxGetThread()->PumpMessage();
						return true;
					}
				}

				if(		(	//( WM_MOUSEFIRST <= _msg.message && _msg.message <= WM_MOUSELAST )
							_msg.message == WM_MOUSEMOVE
						||	_msg.message == WM_LBUTTONDOWN
						||	_msg.message == WM_MOUSEACTIVATE
						||	_msg.message == WM_SETCURSOR
						//||	_msg.message == WM_NCHITTEST
						)
					&&	(	_msg.hwnd == m_hWnd
					||	_msg.hwnd == ::GetParent( m_hWnd )
						)
					)
				{ // if grid's mouse message
					CPoint pt; // = _msg.pt;
					if( ::GetCursorPos( &pt ) )
					{
						ScreenToClient( &pt );
						CExtGridHitTestInfo htInfo( pt );
						HitTest( htInfo, false, false );
						if(		(! htInfo.IsHoverEmpty() )
							&&	htInfo.IsValidRect()
							&&	( htInfo.GetInnerOuterTypeOfColumn() == 0 || htInfo.GetInnerOuterTypeOfRow() == 0 )
							&&	(	( htInfo.GetInnerOuterTypeOfColumn() == 0 && htInfo.GetInnerOuterTypeOfRow() == 0 )
								||	( htInfo.m_dwAreaFlags & __EGBWA_NEAR_CELL_BORDER_SIDE_MASK ) == 0
								)
							)
						{ // if event of inner data cell or l/t/r/b non-corner header cell
							CExtFormulaDataManager & _FDM = OnGridQueryFormulaDataManager();
									///_FDM.m_crTracker.cpMin = _FDM.m_crTracker.cpMax = -1L;
							CExtFormulaItem * pOldFormulaItemRangeInGridHT = _FDM.m_pFormulaItemRangeInGridHT;
							CRect rcOldInGridHT = _FDM.m_rcInGridHT;
							_FDM.m_pFormulaItemRangeInGridHT = NULL;
							_FDM.m_bInGridHtLeft = _FDM.m_bInGridHtTop = _FDM.m_bInGridHtRight = _FDM.m_bInGridHtBottom = false;
							_FDM.m_rcInGridHT.SetRect( -32767, -32767, -32767, -32767 );
							if( _FDM.m_listEditedFormulaRanges.GetCount() > 0 )
							{
								CSize _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier;
								OnQueryFormulaToolMetrics( _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier );
								CSize _sizeLines = _sizeLinesActive, _sizeTool = _sizeToolActive; // use active for hit-testing
								POSITION pos = _FDM.m_listEditedFormulaRanges.GetHeadPosition();
								for( ; pos != NULL; )
								{
									CExtFormulaItem * pFI = _FDM.m_listEditedFormulaRanges.GetNext( pos );
									if( pFI->m_rcEditedRange.left < 0L )
										continue;
									__EXT_DEBUG_GRID_ASSERT( pFI->m_rcEditedRange.right >= 0L );
									__EXT_DEBUG_GRID_ASSERT( pFI->m_rcEditedRange.top >= 0L );
									__EXT_DEBUG_GRID_ASSERT( pFI->m_rcEditedRange.bottom >= 0L );
									if(	!(		( pFI->m_rcEditedRange.left <= htInfo.m_nColNo && htInfo.m_nColNo <= pFI->m_rcEditedRange.right )
											&&	( pFI->m_rcEditedRange.top <= htInfo.m_nRowNo && htInfo.m_nRowNo <= pFI->m_rcEditedRange.bottom )
										) )
										continue;
									bool bLeft   = ( pFI->m_rcEditedRange.left   == htInfo.m_nColNo ) ? true : false;
									bool bTop    = ( pFI->m_rcEditedRange.top    == htInfo.m_nRowNo ) ? true : false;
									bool bRight  = ( pFI->m_rcEditedRange.right  == htInfo.m_nColNo ) ? true : false;
									bool bBottom = ( pFI->m_rcEditedRange.bottom == htInfo.m_nRowNo ) ? true : false;
									if( ! ( bLeft || bTop || bRight || bBottom ) )
										continue;
									LONG nWidth = ( bTop || bBottom ) ? ( htInfo.m_rcItem.right - htInfo.m_rcItem.left ) : 0L;
									LONG nHeight = ( bLeft || bRight ) ? ( htInfo.m_rcItem.bottom - htInfo.m_rcItem.top ) : 0L;
									if( ( _FDM.m_pFormulaItemRangeInGridHT == NULL ) && bLeft && bTop )
									{
										CRectWH rc( htInfo.m_rcItem.left, htInfo.m_rcItem.top, _sizeTool.cx, _sizeTool.cy );
										if( rc.PtInRect( pt ) )
											{ _FDM.m_pFormulaItemRangeInGridHT = pFI; _FDM.m_rcInGridHT = rc; _FDM.m_bInGridHtLeft = _FDM.m_bInGridHtTop = true; }
									}
									if( ( _FDM.m_pFormulaItemRangeInGridHT == NULL ) && bRight && bTop )
									{
										CRectWH rc( htInfo.m_rcItem.right - _sizeShiftRB.cx - _sizeTool.cx + 1, htInfo.m_rcItem.top, _sizeTool.cx, _sizeTool.cy );
										if( rc.PtInRect( pt ) )
											{ _FDM.m_pFormulaItemRangeInGridHT = pFI; _FDM.m_rcInGridHT = rc; _FDM.m_bInGridHtRight = _FDM.m_bInGridHtTop = true; }
									}
									if( ( _FDM.m_pFormulaItemRangeInGridHT == NULL ) && bLeft && bBottom )
									{
										CRectWH rc( htInfo.m_rcItem.left, htInfo.m_rcItem.bottom - _sizeShiftRB.cy - _sizeTool.cy + 1, _sizeTool.cx, _sizeTool.cy );
										if( rc.PtInRect( pt ) )
											{ _FDM.m_pFormulaItemRangeInGridHT = pFI; _FDM.m_rcInGridHT = rc; _FDM.m_bInGridHtLeft = _FDM.m_bInGridHtBottom = true; }
									}
									if( ( _FDM.m_pFormulaItemRangeInGridHT == NULL ) && bRight && bBottom )
									{
										CRectWH rc( htInfo.m_rcItem.right - _sizeShiftRB.cx - _sizeTool.cx + 1, htInfo.m_rcItem.bottom - _sizeShiftRB.cy - _sizeTool.cy + 1, _sizeTool.cx, _sizeTool.cy );
										if( rc.PtInRect( pt ) )
											{ _FDM.m_pFormulaItemRangeInGridHT = pFI; _FDM.m_rcInGridHT = rc; _FDM.m_bInGridHtRight = _FDM.m_bInGridHtBottom = true; }
									}
									if( ( _FDM.m_pFormulaItemRangeInGridHT == NULL ) && bLeft && nHeight > 0L )
									{
										CRectWH rc( htInfo.m_rcItem.left, htInfo.m_rcItem.top, _sizeLinesActive.cx, nHeight );
//CString s; s.Format( _T("pt(x=%d,y=%d) rc(l=%d,r=%d,t=%d,b=%d) result=%s"), pt.x, pt.y, rc.left, rc.right, rc.top, rc.bottom, rc.PtInRect( pt ) ? "yes" : "no" );
//TRACE( "trying left %s ... \r\n", LPCTSTR(s) );										
										if( rc.PtInRect( pt ) )
											{ _FDM.m_pFormulaItemRangeInGridHT = pFI; _FDM.m_rcInGridHT = rc; _FDM.m_bInGridHtLeft = true; }
									}
									if( ( _FDM.m_pFormulaItemRangeInGridHT == NULL ) && bTop && nWidth > 0L )
									{
										CRectWH rc( htInfo.m_rcItem.left, htInfo.m_rcItem.top, nWidth, _sizeLinesActive.cy );
										if( rc.PtInRect( pt ) )
											{ _FDM.m_pFormulaItemRangeInGridHT = pFI; _FDM.m_rcInGridHT = rc; _FDM.m_bInGridHtTop = true; }
									}
									if( ( _FDM.m_pFormulaItemRangeInGridHT == NULL ) && bRight && nHeight > 0L )
									{
										CRectWH rc( htInfo.m_rcItem.right - _sizeShiftRB.cx - _sizeLinesActive.cx + 1, htInfo.m_rcItem.top, _sizeLinesActive.cx, nHeight );
										if( rc.PtInRect( pt ) )
											{ _FDM.m_pFormulaItemRangeInGridHT = pFI; _FDM.m_rcInGridHT = rc; _FDM.m_bInGridHtRight = true; }
									}
									if( ( _FDM.m_pFormulaItemRangeInGridHT == NULL ) && bBottom && nWidth > 0L )
									{
										CRectWH rc( htInfo.m_rcItem.left, htInfo.m_rcItem.bottom - _sizeShiftRB.cy - _sizeLinesActive.cy + 1, nWidth, _sizeLinesActive.cy );
										if( rc.PtInRect( pt ) )
											{ _FDM.m_pFormulaItemRangeInGridHT = pFI; _FDM.m_rcInGridHT = rc; _FDM.m_bInGridHtBottom = true; }
									}
									if( _FDM.m_pFormulaItemRangeInGridHT == NULL )
										continue;
									break; // break-for-loop
								} // for( ; pos != NULL; )
								bool bSetCursor = false, bDoDND = false, bNonEmptyHT = ( _FDM.m_pFormulaItemRangeInGridHT != NULL ) ? true : false;
								switch( _msg.message )
								{
								case WM_MOUSEMOVE:
									if( rcOldInGridHT != _FDM.m_rcInGridHT || LPVOID(pOldFormulaItemRangeInGridHT) != LPVOID(_FDM.m_pFormulaItemRangeInGridHT) )
									{
										OnSwInvalidate( true );
										bSetCursor = true;
									}
								break;
								case WM_LBUTTONDOWN:
									if( bNonEmptyHT /*&& CExtControlBar::stat_DoDragDetect( m_hWnd, pt )*/ )
										bDoDND = true;
									else if( pWndFIE->OnFormulaRangeDndCreation( htInfo ) )
										return true;
								break;
								case WM_MOUSEACTIVATE:
								break;
								case WM_SETCURSOR:
									bSetCursor = true;
								break;
								} // switch( _msg.message )
								if( bSetCursor )
								{
									if(		( _FDM.m_bInGridHtLeft || _FDM.m_bInGridHtRight )
										&&	( ! ( _FDM.m_bInGridHtTop || _FDM.m_bInGridHtBottom ) )
										)
										::SetCursor( ( m_hCursorFormulaRangeMove != NULL ) ? m_hCursorFormulaRangeMove : ::LoadCursor( NULL, IDC_SIZEALL /*IDC_SIZEWE*/ ) );
									else
									if(		( _FDM.m_bInGridHtTop || _FDM.m_bInGridHtBottom )
										&&	( ! ( _FDM.m_bInGridHtLeft || _FDM.m_bInGridHtRight ) )
										)
										::SetCursor( ( m_hCursorFormulaRangeMove != NULL ) ? m_hCursorFormulaRangeMove : ::LoadCursor( NULL, IDC_SIZEALL /*IDC_SIZENS*/ ) );
									else
									if(		( _FDM.m_bInGridHtLeft && _FDM.m_bInGridHtTop )
										||	( _FDM.m_bInGridHtRight && _FDM.m_bInGridHtBottom )
										)
										::SetCursor( ( m_hCursorFormulaRangeNWSE != NULL ) ? m_hCursorFormulaRangeNWSE : ::LoadCursor( NULL, IDC_SIZENWSE ) );
									else
									if(		( _FDM.m_bInGridHtLeft && _FDM.m_bInGridHtBottom )
										||	( _FDM.m_bInGridHtRight && _FDM.m_bInGridHtTop )
										)
										::SetCursor( ( m_hCursorFormulaRangeNESW != NULL ) ? m_hCursorFormulaRangeNESW : ::LoadCursor( NULL, IDC_SIZENESW ) );
									else
										::SetCursor( ::LoadCursor( NULL, IDC_ARROW ) );
								} // if( bSetCursor )
								if(		bDoDND
									&&	htInfo.GetInnerOuterTypeOfColumn() == 0
									&&	htInfo.GetInnerOuterTypeOfRow() == 0
									)
									pWndFIE->OnFormulaRangeDndModification( htInfo );
								if( bNonEmptyHT || bDoDND || bSetCursor )
									return true;
							} // if( _FDM.m_listEditedFormulaRanges.GetCount() > 0 )
							else
							{
								switch( _msg.message )
								{
								case WM_LBUTTONDOWN:
									if( pWndFIE->OnFormulaRangeDndCreation( htInfo ) )
										return true;
								break;
								case WM_MOUSEACTIVATE:
								break;
								} // switch( _msg.message )
							} // else from if( _FDM.m_listEditedFormulaRanges.GetCount() > 0 )
						} // if event of inner data cell or l/t/r/b non-corner header cell
					} // if( ::GetCursorPos( &pt ) )
				} // if grid's mouse message
			} // if( pWndFIE != NULL )
		} // if( pWndInplaceControl != NULL )
	} // if( pFC != NULL )
	return false;
}

bool CExtFormulaGridWnd::OnGridCellFormatFormulaInplaceEdit( CExtFormulaItem * pFI, CExtFormulaInplaceEdit & wndFIE )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	pFI;
	wndFIE;
	return false;
}

CExtFormulaDataManager & CExtFormulaGridWnd::OnGridQueryFormulaDataManager()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_pFormulaDataManager != NULL )
		return (*m_pFormulaDataManager);
	m_pFormulaDataManager =
		new CExtFDP < CExtGDPJ < CExtGridDataProviderSparse >, CExtFormulaDataManager >
			( RUNTIME_CLASS( CExtGridCellHeader ), RUNTIME_CLASS( CExtFormulaGridCell ) );
	return (*m_pFormulaDataManager);
}

const CExtFormulaDataManager & CExtFormulaGridWnd::OnGridQueryFormulaDataManager() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ( const_cast < CExtFormulaGridWnd * > ( this ) ) -> OnGridQueryFormulaDataManager();
}

CExtGridDataProvider & CExtFormulaGridWnd::OnGridQueryDataProvider()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return OnGridQueryFormulaDataManager().OnQueryDataProvider();
}

bool CExtFormulaGridWnd::OnFormulaGridRemoveEmptyFormulaCell(
	CExtGridCell & _cell,
	LONG nColNo,
	LONG nRowNo
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtFormulaGridCell * pFC = DYNAMIC_DOWNCAST( CExtFormulaGridCell, (&_cell) );
	if( pFC == NULL )
		return false;
	__EXT_DEBUG_GRID_ASSERT_VALID( pFC );
	if( ! pFC->FormulaStateIsEmpty() )
		return false;
bool bRetVal = GridCellSet( nColNo, nRowNo, NULL );
	if( bRetVal )
	{
		CExtFormulaDataManager::map_formula_cells_t _mapCellsAlreadyComputed;
		OnGridQueryFormulaDataManager().OnComputeDependentCells( _mapCellsAlreadyComputed, nColNo, nRowNo, false );
	}
	return bRetVal;
}

void CExtFormulaGridWnd::OnGridCellInputComplete(
	CExtGridCell & _cell,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	HWND hWndInputControl // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtNSB < CExtPPVW < CExtGridWnd > > :: OnGridCellInputComplete( _cell, nColNo, nRowNo, nColType, nRowType, hWndInputControl );
	if( nColType == 0 && nRowType == 0 )
	{
		if( ! OnFormulaGridRemoveEmptyFormulaCell( _cell, nColNo, nRowNo ) )
		{
			CExtFormulaDataManager::map_formula_cells_t _mapCellsAlreadyComputed;
			OnGridQueryFormulaDataManager().OnComputeDependentCells( _mapCellsAlreadyComputed, nColNo, nRowNo, false );
		}
	}
CExtFormulaGridCell * pFC = DYNAMIC_DOWNCAST( CExtFormulaGridCell, (&_cell) );
	if( pFC != NULL )
	{
		//pFC->_VariantAssign( LPCTSTR(sTextNew) );
		//if( ! bEmptyText )
		{
			//pFC->_VariantChangeType( VT_BSTR, NULL, false );
			//if( pFC->vt != VT_I4 && ( ! pFC->_VariantChangeType( VT_I4, NULL, false ) ) )
			{
				if( pFC->vt != VT_R8 && ( ! pFC->_VariantChangeType( VT_R8, NULL, false ) ) )
				{
					if( pFC->vt != VT_BOOL && ( ! pFC->_VariantChangeType( VT_BOOL, NULL, false ) ) )
					{
						if( pFC->vt != VT_DATE && ( ! pFC->_VariantChangeType( VT_DATE, NULL, false ) ) )
						{
						}
					}
				}
			}
		}
	} // if( pFC != NULL )
}

bool CExtFormulaGridWnd::OnGridPaintCellTextHook(
	bool bPostNotification,
	const CExtGridCell & _cell,
	const RECT & rcCellText,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
bool bAutoFormattingRow = ( nColType == 0 && nRowType < 0 && nRowNo == 0L ) ? true : false, bAutoFormattingCol = ( nColType < 0 && nRowType == 0 && nColNo == 0L ) ? true : false;
	if( bAutoFormattingRow || bAutoFormattingCol )
	{
		const CExtFormulaDataManager & _FDM = OnGridQueryFormulaDataManager();
		const CExtFormulaAlphabet & _FA = _FDM.OnQueryFormulaAlphabet();
		bool bINM = _FA.GetInverseNamingMode();
		if( bAutoFormattingRow && (!bINM) )
			bAutoFormattingRow = false;
		if( bAutoFormattingCol && bINM )
			bAutoFormattingCol = false;
		if( bAutoFormattingRow || bAutoFormattingCol )
		{
			CExtSafeString strCurrent, strValid;
			_cell.TextGet( strCurrent );
			if( bAutoFormattingRow )
				strValid = _FA.GetAlphaStr( nColNo );
			else if( bAutoFormattingCol )
				strValid = _FA.GetAlphaStr( nRowNo );
			if( strCurrent != strValid )
				( const_cast < CExtGridCell & > ( _cell ) ).TextSet( LPCTSTR(strValid) );
		} // if( bAutoFormattingRow || bAutoFormattingCol )
	} // if( bAutoFormattingRow || bAutoFormattingCol )
	return
		CExtNSB < CExtPPVW < CExtGridWnd > > :: OnGridPaintCellTextHook(
			bPostNotification,
			_cell,
			rcCellText,
			dc,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
}

void CExtFormulaGridWnd::OnQueryFormulaToolMetrics(
	CSize & _sizeShiftRB,
	CSize & _sizeLinesActive,
	CSize & _sizeLinesInactive,
	CSize & _sizeToolActive,
	CSize & _sizeToolInactive,
	CSize & _sizeSingleSelectionAreaBorder,
	CSize & _sizeSingleSelectionAreaResizier
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtPaintManager * pPM = PmBridge_GetPM();
	__EXT_DEBUG_GRID_ASSERT_VALID( pPM );
	_sizeShiftRB.cx         = 2;
	_sizeShiftRB.cy         = 2;
	_sizeLinesActive.cx     = pPM->UiScalingDo( 2, CExtPaintManager::__EUIST_X );
	_sizeLinesActive.cy     = pPM->UiScalingDo( 2, CExtPaintManager::__EUIST_Y );
	_sizeLinesInactive.cx   = pPM->UiScalingDo( 1, CExtPaintManager::__EUIST_X );
	_sizeLinesInactive.cy   = pPM->UiScalingDo( 1, CExtPaintManager::__EUIST_Y );
	_sizeToolActive.cx      = pPM->UiScalingDo( 5, CExtPaintManager::__EUIST_X );
	_sizeToolActive.cy      = pPM->UiScalingDo( 5, CExtPaintManager::__EUIST_Y );
	_sizeToolInactive.cx    = pPM->UiScalingDo( 4, CExtPaintManager::__EUIST_X );
	_sizeToolInactive.cy    = pPM->UiScalingDo( 4, CExtPaintManager::__EUIST_Y );
	_sizeSingleSelectionAreaBorder = _sizeLinesActive; // (2,2) at 96dpi
	_sizeSingleSelectionAreaResizier = _sizeToolInactive; // (4,4) at 96dpi
}

bool CExtFormulaGridWnd::SelectionGetForCellPainting(
	LONG nColNo,
	LONG nRowNo
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
bool bSel = CExtGridBaseWnd::SelectionGetForCell( nColNo, nRowNo );
	if( bSel && ( FgGetStyle() & __FGS_FOCUSED_CELL_WITH_NON_SELECTED_BKGND ) != 0 )
	{
		CPoint ptFocus = FocusGet();
		if( ptFocus.x == nColNo && ptFocus.y == nRowNo )
			bSel = false;
	}
	return bSel;
}

void CExtFormulaGridWnd::OnGbwPaintCell(
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtNSB < CExtPPVW < CExtGridWnd > > :: OnGbwPaintCell( dc, nVisibleColNo, nVisibleRowNo, nColNo, nRowNo, rcCellExtra, rcCell, rcVisibleRange, dwAreaFlags, dwHelperPaintFlags );
	if( ( dwAreaFlags & __EGBWA_INNER_CELLS ) != 0 )
	{
#if (defined _DEBUG)
		INT nColType = CExtGridHitTestInfo::GetInnerOuterTypeOfColumn( dwAreaFlags );
		INT nRowType = CExtGridHitTestInfo::GetInnerOuterTypeOfRow( dwAreaFlags );
		nColType;
		nRowType;
		__EXT_DEBUG_GRID_ASSERT( nColType == 0 && nRowType == 0 );
#endif // (defined _DEBUG)

		DWORD dwFgStyle = FgGetStyle();
		if(		( dwFgStyle & __FGS_TOOL_RECT_MASK ) != 0
			&&	( ( SiwGetStyle() & __EGBS_NO_HIDE_SELECTION ) != 0 || OnSiwQueryFocusedControlState() )
			&&	SelectionGetAreaCount() == 1
			)
		{ // if single area selection
			CRect rcSel = SelectionGet( true, 0 );
			bool bLeft   = ( rcSel.left   == nColNo ) ? true : false;
			bool bTop    = ( rcSel.top    == nRowNo ) ? true : false;
			bool bRight  = ( rcSel.right  == nColNo ) ? true : false;
			bool bBottom = ( rcSel.bottom == nRowNo ) ? true : false;
			if( ( bLeft || bRight ) && ( ! ( rcSel.top <= nRowNo && nRowNo <= rcSel.bottom ) ) )
				bLeft = bRight = false;
			if( ( bTop || bBottom ) && ( ! ( rcSel.left <= nColNo && nColNo <= rcSel.right ) ) )
				bTop = bBottom = false;
			if( bLeft || bTop || bRight || bBottom )
			{
				COLORREF clrBkColorOld = dc.GetBkColor();
				CSize _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier;
				OnQueryFormulaToolMetrics( _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier );
				COLORREF clr = OnSiwGetSysColor( COLOR_WINDOWTEXT );
				LONG nWidth = ( bTop || bBottom ) ? ( rcCell.right - rcCell.left - _sizeShiftRB.cx ) : 0L;
				LONG nHeight = ( bLeft || bRight ) ? ( rcCell.bottom - rcCell.top - _sizeShiftRB.cy ) : 0L;
				if( bLeft && nHeight > 0L )
					dc.FillSolidRect( rcCell.left, rcCell.top, _sizeSingleSelectionAreaBorder.cx, nHeight, clr );
				if( bTop && nWidth > 0L )
					dc.FillSolidRect( rcCell.left, rcCell.top, nWidth, _sizeSingleSelectionAreaBorder.cy, clr );
				if( bRight && nHeight > 0L )
					dc.FillSolidRect( rcCell.right - _sizeShiftRB.cx - _sizeSingleSelectionAreaBorder.cx + 1, rcCell.top, _sizeSingleSelectionAreaBorder.cx, nHeight, clr );
				if( bBottom && nWidth > 0L )
					dc.FillSolidRect( rcCell.left, rcCell.bottom - _sizeShiftRB.cy - _sizeSingleSelectionAreaBorder.cy + 1, nWidth, _sizeSingleSelectionAreaBorder.cy, clr );
				if( bRight && bBottom && ( dwFgStyle & __FGS_TOOL_RECT_EXPANDING ) != 0 )
				{
					COLORREF clrErase = OnSiwGetSysColor( COLOR_WINDOW );
					CSize _sizeEraseBox( _sizeSingleSelectionAreaResizier.cx + 1, _sizeSingleSelectionAreaResizier.cy + 1 );
					dc.FillSolidRect( rcCell.right - _sizeShiftRB.cx - _sizeSingleSelectionAreaBorder.cx + 1, rcCell.bottom - _sizeShiftRB.cy - _sizeEraseBox.cy + 1, _sizeSingleSelectionAreaBorder.cx, _sizeEraseBox.cy, clrErase );
					dc.FillSolidRect( rcCell.right - _sizeShiftRB.cx - _sizeEraseBox.cx + 1, rcCell.bottom - _sizeShiftRB.cy - _sizeSingleSelectionAreaBorder.cy + 1, _sizeEraseBox.cx, _sizeSingleSelectionAreaBorder.cy, clrErase );
					dc.FillSolidRect( rcCell.right - _sizeShiftRB.cx - _sizeSingleSelectionAreaResizier.cx + 1, rcCell.bottom - _sizeShiftRB.cy - _sizeSingleSelectionAreaResizier.cy + 1, _sizeSingleSelectionAreaResizier.cx, _sizeSingleSelectionAreaResizier.cy, clr );
				}
				dc.SetBkColor( clrBkColorOld );
			} // if( bLeft || bTop || bRight || bBottom )
		} // if single area selection

		const CExtFormulaDataManager & _FDM = OnGridQueryFormulaDataManager();
		HWND hWndInplaceActive = NULL;
		CExtFormulaInplaceEdit * pWndFIE = NULL;
		if(		(	_FDM.m_bDrawLinesForNamedRanges
				||	_FDM.m_bDrawLinesForSimpleRanges
				||	_FDM.m_nAlphaForNamedRanges != BYTE(0)
				||	_FDM.m_nAlphaForSimpleRanges != BYTE(0)
				||	( _FDM.m_crTracker.cpMin >= 0L && _FDM.m_crTracker.cpMax >= 0L )
				)
			&&	( dwFgStyle & __FGS_FORMULA_REFS_HIGHLIGHT ) != 0
			&&	( ( hWndInplaceActive = ( const_cast < CExtFormulaGridWnd * > ( this ) ) -> GetSafeInplaceActiveHwnd() ) != NULL )
			&&	( _FDM.m_listEditedFormulaRanges.GetCount() > 0 )
			&&	( pWndFIE = DYNAMIC_DOWNCAST( CExtFormulaInplaceEdit, CWnd::FromHandlePermanent( hWndInplaceActive ) ) ) != NULL
			)
		{ // if highlight named or simple ranges
			COLORREF clrBkColorOld = dc.GetBkColor();
			LONG nColCount = ColumnCountGet();
			LONG nRowCount = RowCountGet();
			CSize _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier;
			OnQueryFormulaToolMetrics( _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier );
			CSize _sizeAnt( _FDM.m_nGridSelectionTrackerWidth, _FDM.m_nGridSelectionTrackerHeight );
			POSITION pos = _FDM.m_listEditedFormulaRanges.GetHeadPosition();
			for( ; pos != NULL; )
			{
				const CExtFormulaItem * pFI = _FDM.m_listEditedFormulaRanges.GetNext( pos );
				if( pFI->m_rcEditedRange.left < 0L )
				{
					if( ( ! pFI->m_rangeEditedNamed.IsEmpty() ) && pFI->m_rangeEditedNamed.PtInRegion( nColNo, nRowNo ) )
					{
						COLORREF clr = pFI->m_clr;
						if( clr == COLORREF(-1L) )
							clr = OnSiwGetSysColor( COLOR_WINDOWTEXT );
						if( _FDM.m_nAlphaForNamedRanges != BYTE(0) )
						{
							const CExtBitmap * pBmp = _FDM._ColorBitmap_Get( clr );
							if( pBmp != NULL )
							{
								__EXT_DEBUG_GRID_ASSERT( ! pBmp->IsEmpty() );
								pBmp->AlphaBlend( dc.m_hDC, rcCell, _FDM.m_nAlphaForNamedRanges );
							} // if( pBmp != NULL )
						} // if( _FDM.m_nAlphaForNamedRanges != BYTE(0) )
						if( _FDM.m_bDrawLinesForNamedRanges && ( dwFgStyle & __FGS_FORMULA_REFS_HIGHLIGHT ) != 0 )
						{
							CSize _sizeLines = _sizeLinesInactive;
							LONG nRangeIndex, nRangeIndex2, nColNo2, nRowNo2, nRangeCount = pFI->m_rangeEditedNamed.RangeCount();
							for( nRangeIndex = 0L; nRangeIndex < nRangeCount; nRangeIndex ++ )
							{
								CRect rcSubRange = pFI->m_rangeEditedNamed.Range( nRangeIndex );
								__EXT_DEBUG_GRID_ASSERT( rcSubRange.bottom >= 0L );
								if(	!(		( rcSubRange.left <= nColNo && nColNo <= rcSubRange.right )
										&&	( rcSubRange.top <= nRowNo && nRowNo <= rcSubRange.bottom )
									) )
									continue;
								bool bLeft   = ( rcSubRange.left   == nColNo ) ? true : false;
								bool bTop    = ( rcSubRange.top    == nRowNo ) ? true : false;
								bool bRight  = ( rcSubRange.right  == nColNo ) ? true : false;
								bool bBottom = ( rcSubRange.bottom == nRowNo ) ? true : false;
								if(  nRangeCount > 1L )
								{
									if( bLeft && nColNo > 0L )
									{
										nColNo2 = nColNo - 1L;
										nRowNo2 = nRowNo;
										for( nRangeIndex2 = 0; nRangeIndex2 < nRangeCount; nRangeIndex2 ++ )
										{
											if( nRangeIndex2 == nRangeIndex )
												continue;
											CRect rcSubRange2 = pFI->m_rangeEditedNamed.Range( nRangeIndex2 );
											if(	!(		( rcSubRange2.left <= nColNo2 && nColNo2 <= rcSubRange2.right )
													&&	( rcSubRange2.top <= nRowNo2 && nRowNo2 <= rcSubRange2.bottom )
												) )
												continue;
											bool bRight2 = ( rcSubRange2.right == nColNo2 ) ? true : false;
											if( bRight2 )
											{
												bLeft = false;
												break;
											}
										}
									}
									if( bTop && nRowNo > 0L )
									{
										nColNo2 = nColNo;
										nRowNo2 = nRowNo - 1L;
										for( nRangeIndex2 = 0; nRangeIndex2 < nRangeCount; nRangeIndex2 ++ )
										{
											if( nRangeIndex2 == nRangeIndex )
												continue;
											CRect rcSubRange2 = pFI->m_rangeEditedNamed.Range( nRangeIndex2 );
											if(	!(		( rcSubRange2.left <= nColNo2 && nColNo2 <= rcSubRange2.right )
													&&	( rcSubRange2.top <= nRowNo2 && nRowNo2 <= rcSubRange2.bottom )
												) )
												continue;
											bool bBottom2 = ( rcSubRange2.bottom == nRowNo2 ) ? true : false;
											if( bBottom2 )
											{
												bTop = false;
												break;
											}
										}
									}
									if( bRight && nColNo < ( nColCount - 1L ) )
									{
										nColNo2 = nColNo + 1L;
										nRowNo2 = nRowNo;
										for( nRangeIndex2 = 0; nRangeIndex2 < nRangeCount; nRangeIndex2 ++ )
										{
											if( nRangeIndex2 == nRangeIndex )
												continue;
											CRect rcSubRange2 = pFI->m_rangeEditedNamed.Range( nRangeIndex2 );
											if(	!(		( rcSubRange2.left <= nColNo2 && nColNo2 <= rcSubRange2.right )
													&&	( rcSubRange2.top <= nRowNo2 && nRowNo2 <= rcSubRange2.bottom )
												) )
												continue;
											bool bLeft2 = ( rcSubRange2.left == nColNo2 ) ? true : false;
											if( bLeft2 )
											{
												bRight = false;
												break;
											}
										}
									}
									if( bBottom && nRowNo < ( nRowCount - 1L ) )
									{
										nColNo2 = nColNo;
										nRowNo2 = nRowNo + 1L;
										for( nRangeIndex2 = 0; nRangeIndex2 < nRangeCount; nRangeIndex2 ++ )
										{
											if( nRangeIndex2 == nRangeIndex )
												continue;
											CRect rcSubRange2 = pFI->m_rangeEditedNamed.Range( nRangeIndex2 );
											if(	!(		( rcSubRange2.left <= nColNo2 && nColNo2 <= rcSubRange2.right )
													&&	( rcSubRange2.top <= nRowNo2 && nRowNo2 <= rcSubRange2.bottom )
												) )
												continue;
											bool bTop2 = ( rcSubRange2.top == nRowNo2 ) ? true : false;
											if( bTop2 )
											{
												bBottom = false;
												break;
											}
										}
									}
								} // if(  nRangeCount > 1L )
								if( ! ( bLeft || bTop || bRight || bBottom ) )
									continue;
								LONG nWidth = ( bTop || bBottom ) ? ( rcCell.right - rcCell.left ) : 0L;
								LONG nHeight = ( bLeft || bRight ) ? ( rcCell.bottom - rcCell.top ) : 0L;
								if( bLeft && nHeight > 0L )
									dc.FillSolidRect( rcCell.left, rcCell.top, _sizeLines.cx, nHeight, clr );
								if( bTop && nWidth > 0L )
									dc.FillSolidRect( rcCell.left, rcCell.top, nWidth, _sizeLines.cy, clr );
								if( bRight && nHeight > 0L )
									dc.FillSolidRect( rcCell.right - _sizeShiftRB.cx - _sizeLines.cx + 1, rcCell.top, _sizeLines.cx, nHeight, clr );
								if( bBottom && nWidth > 0L )
									dc.FillSolidRect( rcCell.left, rcCell.bottom - _sizeShiftRB.cy - _sizeLines.cy + 1, nWidth, _sizeLines.cy, clr );
							} // for( nRangeIndex = 0L; nRangeIndex < nRangeCount; nRangeIndex ++ )
						} // if( _FDM.m_bDrawLinesForNamedRanges && ( dwFgStyle & __FGS_FORMULA_REFS_HIGHLIGHT ) != 0 )
					} // if( ( ! pFI->m_rangeEditedNamed.IsEmpty() ) && pFI->m_rangeEditedNamed.PtInRegion( nColNo, nRowNo ) )
					continue;
				}
				__EXT_DEBUG_GRID_ASSERT( pFI->m_rcEditedRange.right >= 0L );
				__EXT_DEBUG_GRID_ASSERT( pFI->m_rcEditedRange.top >= 0L );
				__EXT_DEBUG_GRID_ASSERT( pFI->m_rcEditedRange.bottom >= 0L );
				if(	!(		( pFI->m_rcEditedRange.left <= nColNo && nColNo <= pFI->m_rcEditedRange.right )
						&&	( pFI->m_rcEditedRange.top <= nRowNo && nRowNo <= pFI->m_rcEditedRange.bottom )
					) )
					continue;
				bool bLeft   = ( pFI->m_rcEditedRange.left   == nColNo ) ? true : false;
				bool bTop    = ( pFI->m_rcEditedRange.top    == nRowNo ) ? true : false;
				bool bRight  = ( pFI->m_rcEditedRange.right  == nColNo ) ? true : false;
				bool bBottom = ( pFI->m_rcEditedRange.bottom == nRowNo ) ? true : false;
				COLORREF clr = COLORREF(-1L);
				if(		pFI->m_rcEditedRange.left <= nColNo && nColNo <= pFI->m_rcEditedRange.right
					&&	pFI->m_rcEditedRange.top <= nRowNo && nRowNo <= pFI->m_rcEditedRange.bottom
					)
				{
					if( clr == COLORREF(-1L) )
						clr = pFI->m_clr;
					if( clr == COLORREF(-1L) )
						clr = OnSiwGetSysColor( COLOR_WINDOWTEXT );
					if( _FDM.m_nAlphaForSimpleRanges != BYTE(0) )
					{
						const CExtBitmap * pBmp = _FDM._ColorBitmap_Get( clr );
						if( pBmp != NULL )
						{
							__EXT_DEBUG_GRID_ASSERT( ! pBmp->IsEmpty() );
							pBmp->AlphaBlend( dc.m_hDC, rcCell, _FDM.m_nAlphaForSimpleRanges );
						} // if( pBmp != NULL )
					} // if( _FDM.m_nAlphaForSimpleRanges != BYTE(0) )
				} // if( ( ! pFI->m_rangeEditedNamed.IsEmpty() ) && pFI->m_rangeEditedNamed.PtInRegion( nColNo, nRowNo ) )
				if( ! ( bLeft || bTop || bRight || bBottom ) )
					continue;
				LONG nGridSelectionTrackerShiftSize = 0L;
				if(		pWndFIE->m_bGridFocusMode
					&&	_FDM.m_crTracker.cpMin == LONG(pFI->m_nScanPosStart)
					&&	_FDM.m_crTracker.cpMax == LONG(pFI->m_nScanPosEnd)
					&&	_FDM. m_nGridSelectionTrackerLength0 > 0L
					&&	_FDM. m_nGridSelectionTrackerLength1 > 0L
					)
					nGridSelectionTrackerShiftSize = _FDM. m_nGridSelectionTrackerLength0 + _FDM. m_nGridSelectionTrackerLength1;
				if( ( _FDM.m_bDrawLinesForSimpleRanges || nGridSelectionTrackerShiftSize > 0L ) && ( dwFgStyle & __FGS_FORMULA_REFS_HIGHLIGHT ) != 0 )
				{
					if( clr == COLORREF(-1L) )
						clr = pFI->m_clr;
					if( clr == COLORREF(-1L) )
						clr = OnSiwGetSysColor( COLOR_WINDOWTEXT );
					LONG nWidth = ( bTop || bBottom ) ? ( rcCell.right - rcCell.left - _sizeShiftRB.cx ) : 0L;
					LONG nHeight = ( bLeft || bRight ) ? ( rcCell.bottom - rcCell.top - _sizeShiftRB.cy ) : 0L;
					CSize _sizeLines = ( ( dwFgStyle & __FGS_FORMULA_REFS_MODIFY ) != 0 && LPVOID(_FDM.m_pFormulaItemRangeInGridHT) == LPVOID(pFI) ) ? _sizeLinesActive : _sizeLinesInactive;
					CSize _sizeTool = ( ( dwFgStyle & __FGS_FORMULA_REFS_MODIFY ) != 0 && LPVOID(_FDM.m_pFormulaItemRangeInGridHT) == LPVOID(pFI) ) ? _sizeToolActive : _sizeToolInactive;
					if( bLeft && nHeight > 0L )
					{
						dc.FillSolidRect( rcCell.left, rcCell.top, _sizeLines.cx, nHeight, clr );
						if( nGridSelectionTrackerShiftSize > 0L )
						{
							//LONG nStep = ( rcCell.top & (~(1L)) ) + pWndFIE->m_nGridSelectionTrackerRunningShift;
							LONG nStep = ( rcCell.top - rcCell.top%nGridSelectionTrackerShiftSize ) + pWndFIE->m_nGridSelectionTrackerRunningShift;
							LONG nEnd = rcCell.bottom - _FDM. m_nGridSelectionTrackerLength1 + 1;
							LONG nStepTo = nStep + _FDM.m_nGridSelectionTrackerLength1;
							LONG nCrossPosFrom = rcCell.left + _sizeLines.cx;
							LONG nCrossPosTo   = nCrossPosFrom + _sizeAnt.cx;
							for( ; nStep < nEnd; nStep += nGridSelectionTrackerShiftSize, nStepTo += nGridSelectionTrackerShiftSize )
							{
								CRect rc(
									nCrossPosFrom,	nStep,
									nCrossPosTo,	nStepTo
									);
								dc.FillSolidRect( &rc, clr );
							}
						}
					}
					if( bTop && nWidth > 0L )
					{
						dc.FillSolidRect( rcCell.left, rcCell.top, nWidth, _sizeLines.cy, clr );
						if( nGridSelectionTrackerShiftSize > 0L )
						{
							//LONG nStep = ( rcCell.left & (~(1L)) ) + nGridSelectionTrackerShiftSize - pWndFIE->m_nGridSelectionTrackerRunningShift;
							LONG nStep = ( rcCell.left - rcCell.left%nGridSelectionTrackerShiftSize ) + nGridSelectionTrackerShiftSize - pWndFIE->m_nGridSelectionTrackerRunningShift;
							LONG nEnd = rcCell.right - _FDM. m_nGridSelectionTrackerLength1 + 1;
							LONG nStepTo = nStep + _FDM.m_nGridSelectionTrackerLength1;
							LONG nCrossPosFrom = rcCell.top + _sizeLines.cy;
							LONG nCrossPosTo   = nCrossPosFrom + _sizeAnt.cy;
							for( ; nStep < nEnd; nStep += nGridSelectionTrackerShiftSize, nStepTo += nGridSelectionTrackerShiftSize )
							{
								CRect rc(
									nStep,   nCrossPosFrom,
									nStepTo, nCrossPosTo
									);
								dc.FillSolidRect( &rc, clr );
							}
						}
					}
					if( bRight && nHeight > 0L )
					{
						dc.FillSolidRect( rcCell.right - _sizeShiftRB.cx - _sizeLines.cx + 1, rcCell.top, _sizeLines.cx, nHeight, clr );
						if( nGridSelectionTrackerShiftSize > 0L )
						{
							//LONG nStep = ( rcCell.top & (~(1L)) ) + nGridSelectionTrackerShiftSize - pWndFIE->m_nGridSelectionTrackerRunningShift - 1L;
							LONG nStep = ( rcCell.top - rcCell.top%nGridSelectionTrackerShiftSize ) + nGridSelectionTrackerShiftSize - pWndFIE->m_nGridSelectionTrackerRunningShift - 1L;
							LONG nEnd = rcCell.bottom - _FDM. m_nGridSelectionTrackerLength1 + 1;
							LONG nStepTo = nStep + _FDM.m_nGridSelectionTrackerLength1;
							LONG nCrossPosTo   = rcCell.right - _sizeLines.cx - 1;
							LONG nCrossPosFrom = nCrossPosTo - _sizeAnt.cx;
							for( ; nStep < nEnd; nStep += nGridSelectionTrackerShiftSize, nStepTo += nGridSelectionTrackerShiftSize )
							{
								CRect rc(
									nCrossPosFrom,	nStep,
									nCrossPosTo,	nStepTo
									);
								dc.FillSolidRect( &rc, clr );
							}
						}
					}
					if( bBottom && nWidth > 0L )
					{
						dc.FillSolidRect( rcCell.left, rcCell.bottom - _sizeShiftRB.cy - _sizeLines.cy + 1, nWidth, _sizeLines.cy, clr );
						if( nGridSelectionTrackerShiftSize > 0L )
						{
							//LONG nStep = ( rcCell.left & (~(1L)) ) + pWndFIE->m_nGridSelectionTrackerRunningShift - 1L;
							LONG nStep = ( rcCell.left - rcCell.left%nGridSelectionTrackerShiftSize ) + pWndFIE->m_nGridSelectionTrackerRunningShift - 1L;
							LONG nEnd = rcCell.right - _FDM. m_nGridSelectionTrackerLength1 + 1;
							LONG nStepTo = nStep + _FDM.m_nGridSelectionTrackerLength1;
							LONG nCrossPosTo   = rcCell.bottom - _sizeLines.cy - 1;
							LONG nCrossPosFrom = nCrossPosTo - _sizeAnt.cy;
							for( ; nStep < nEnd; nStep += nGridSelectionTrackerShiftSize, nStepTo += nGridSelectionTrackerShiftSize )
							{
								CRect rc(
									nStep,   nCrossPosFrom,
									nStepTo, nCrossPosTo
									);
								dc.FillSolidRect( &rc, clr );
							}
						}
					}
					if( ( dwFgStyle & __FGS_FORMULA_REFS_MODIFY ) != 0 )
					{
						if( bLeft && bTop )
							dc.FillSolidRect( rcCell.left, rcCell.top, _sizeTool.cx, _sizeTool.cy, clr );
						if( bRight && bTop )
							dc.FillSolidRect( rcCell.right - _sizeShiftRB.cx - _sizeTool.cx + 1, rcCell.top, _sizeTool.cx, _sizeTool.cy, clr );
						if( bLeft && bBottom )
							dc.FillSolidRect( rcCell.left, rcCell.bottom - _sizeShiftRB.cy - _sizeTool.cy + 1, _sizeTool.cx, _sizeTool.cy, clr );
						if( bRight && bBottom )
							dc.FillSolidRect( rcCell.right - _sizeShiftRB.cx - _sizeTool.cx + 1, rcCell.bottom - _sizeShiftRB.cy - _sizeTool.cy + 1, _sizeTool.cx, _sizeTool.cy, clr );
					} // if( ( dwFgStyle & __FGS_FORMULA_REFS_MODIFY ) != 0 )
				} // if( ( _FDM.m_bDrawLinesForSimpleRanges || nGridSelectionTrackerShiftSize > 0L ) && ( dwFgStyle & __FGS_FORMULA_REFS_HIGHLIGHT ) != 0 )
			} // for( ; pos != NULL; )
			dc.SetBkColor( clrBkColorOld );
		} // if highlight named or simple ranges

		if( m_rcFormulaGridToolRectSelArea.left >= 0 )
		{
			__EXT_DEBUG_GRID_ASSERT( m_rcFormulaGridToolRectSelArea.right >= 0 );
			__EXT_DEBUG_GRID_ASSERT( m_rcFormulaGridToolRectSelArea.top >= 0 );
			__EXT_DEBUG_GRID_ASSERT( m_rcFormulaGridToolRectSelArea.bottom >= 0 );
			__EXT_DEBUG_GRID_ASSERT( m_rcFormulaGridToolRectHighlightArea.left >= 0 );
			__EXT_DEBUG_GRID_ASSERT( m_rcFormulaGridToolRectHighlightArea.right >= 0 );
			__EXT_DEBUG_GRID_ASSERT( m_rcFormulaGridToolRectHighlightArea.top >= 0 );
			__EXT_DEBUG_GRID_ASSERT( m_rcFormulaGridToolRectHighlightArea.bottom >= 0 );
			bool bInsideSelArea =
				(	m_rcFormulaGridToolRectSelArea.left <= nColNo && nColNo <= m_rcFormulaGridToolRectSelArea.right
				&&	m_rcFormulaGridToolRectSelArea.top <= nRowNo && nRowNo <= m_rcFormulaGridToolRectSelArea.bottom
				) ? true : false;
			bool bInsideHighlightArea =
				(	m_rcFormulaGridToolRectHighlightArea.left <= nColNo && nColNo <= m_rcFormulaGridToolRectHighlightArea.right
				&&	m_rcFormulaGridToolRectHighlightArea.top <= nRowNo && nRowNo <= m_rcFormulaGridToolRectHighlightArea.bottom
				) ? true : false;
			if( bInsideHighlightArea )
			{
				bool bLeft   = ( m_rcFormulaGridToolRectHighlightArea.left   == nColNo ) ? true : false;
				bool bTop    = ( m_rcFormulaGridToolRectHighlightArea.top    == nRowNo ) ? true : false;
				bool bRight  = ( m_rcFormulaGridToolRectHighlightArea.right  == nColNo ) ? true : false;
				bool bBottom = ( m_rcFormulaGridToolRectHighlightArea.bottom == nRowNo ) ? true : false;
				if( ( bLeft || bRight ) && ( ! ( m_rcFormulaGridToolRectHighlightArea.top <= nRowNo && nRowNo <= m_rcFormulaGridToolRectHighlightArea.bottom ) ) )
					bLeft = bRight = false;
				if( ( bTop || bBottom ) && ( ! ( m_rcFormulaGridToolRectHighlightArea.left <= nColNo && nColNo <= m_rcFormulaGridToolRectHighlightArea.right ) ) )
					bTop = bBottom = false;
				if( bLeft || bTop || bRight || bBottom )
				{
					CSize _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier;
					OnQueryFormulaToolMetrics( _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier );
					CRect rcCellX = rcCell;
					if( bLeft || bRight )
					{
						if( bTop )
							rcCellX.top += _sizeSingleSelectionAreaBorder.cy;
						if( bBottom )
							rcCellX.bottom -= _sizeSingleSelectionAreaBorder.cy;
					}
					LONG nWidth = ( bTop || bBottom ) ? ( rcCellX.right - rcCellX.left - _sizeShiftRB.cx ) : 0L;
					LONG nHeight = ( bLeft || bRight ) ? ( rcCellX.bottom - rcCellX.top - _sizeShiftRB.cy ) : 0L;
					HBRUSH hBrush = (HBRUSH) CDC::GetHalftoneBrush()->GetSafeHandle();
					__EXT_DEBUG_GRID_ASSERT( hBrush != NULL );
					HGDIOBJ hBrushOld = dc.SelectObject( hBrush );
					COLORREF clrBkColorOld = dc.SetBkColor( RGB(255,255,255) );
					if( bLeft && nHeight > 0L )
						dc.PatBlt( rcCell.left, rcCellX.top, _sizeSingleSelectionAreaBorder.cx, nHeight, PATINVERT );
					if( bTop && nWidth > 0L )
						dc.PatBlt( rcCellX.left, rcCell.top, nWidth, _sizeSingleSelectionAreaBorder.cy, PATINVERT );
					if( bRight && nHeight > 0L )
						dc.PatBlt( rcCell.right - _sizeShiftRB.cx - _sizeSingleSelectionAreaBorder.cx + 1, rcCellX.top, _sizeSingleSelectionAreaBorder.cx, nHeight, PATINVERT );
					if( bBottom && nWidth > 0L )
						dc.PatBlt( rcCellX.left, rcCell.bottom - _sizeShiftRB.cy - _sizeSingleSelectionAreaBorder.cy + 1, nWidth, _sizeSingleSelectionAreaBorder.cy, PATINVERT );
					dc.SetBkColor( clrBkColorOld );
					dc.SelectObject( hBrushOld );
				} // if( bLeft || bTop || bRight || bBottom )
//dc.MoveTo( rcCell.left, rcCell.top ); dc.LineTo( rcCell.right, rcCell.bottom );
			} // if( bInsideHighlightArea )
			else if( bInsideSelArea )
			{
				HBRUSH hBrush = (HBRUSH) CDC::GetHalftoneBrush()->GetSafeHandle();
				__EXT_DEBUG_GRID_ASSERT( hBrush != NULL );
				HGDIOBJ hBrushOld = dc.SelectObject( hBrush );
				COLORREF clrBkColorOld = dc.SetBkColor( RGB(255,255,255) );
				dc.PatBlt( rcCell.left, rcCell.top, rcCell.right - rcCell.left, rcCell.bottom - rcCell.top, PATINVERT );
				dc.SetBkColor( clrBkColorOld );
				dc.SelectObject( hBrushOld );
//dc.MoveTo( rcCell.left, rcCell.bottom ); dc.LineTo( rcCell.right, rcCell.top );
			} // else if( bInsideSelArea )
		} // if( m_rcFormulaGridToolRectSelArea.left >= 0 )

		if( m_rcFormulaGridToolRectDND.left >= 0 )
		{
			__EXT_DEBUG_GRID_ASSERT( m_rcFormulaGridToolRectDND.right >= 0 );
			__EXT_DEBUG_GRID_ASSERT( m_rcFormulaGridToolRectDND.top >= 0 );
			__EXT_DEBUG_GRID_ASSERT( m_rcFormulaGridToolRectDND.bottom >= 0 );
			bool bLeft   = ( m_rcFormulaGridToolRectDND.left   == nColNo ) ? true : false;
			bool bTop    = ( m_rcFormulaGridToolRectDND.top    == nRowNo ) ? true : false;
			bool bRight  = ( m_rcFormulaGridToolRectDND.right  == nColNo ) ? true : false;
			bool bBottom = ( m_rcFormulaGridToolRectDND.bottom == nRowNo ) ? true : false;
			if( ( bLeft || bRight ) && ( ! ( m_rcFormulaGridToolRectDND.top <= nRowNo && nRowNo <= m_rcFormulaGridToolRectDND.bottom ) ) )
				bLeft = bRight = false;
			if( ( bTop || bBottom ) && ( ! ( m_rcFormulaGridToolRectDND.left <= nColNo && nColNo <= m_rcFormulaGridToolRectDND.right ) ) )
				bTop = bBottom = false;
			if( bLeft || bTop || bRight || bBottom )
			{
				CSize _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier;
				OnQueryFormulaToolMetrics( _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier );
				CRect rcCellX = rcCell;
				if( bLeft || bRight )
				{
					if( bTop )
						rcCellX.top += _sizeSingleSelectionAreaBorder.cy;
					if( bBottom )
						rcCellX.bottom -= _sizeSingleSelectionAreaBorder.cy;
				}
				LONG nWidth = ( bTop || bBottom ) ? ( rcCellX.right - rcCellX.left - _sizeShiftRB.cx ) : 0L;
				LONG nHeight = ( bLeft || bRight ) ? ( rcCellX.bottom - rcCellX.top - _sizeShiftRB.cy ) : 0L;
				HBRUSH hBrush = (HBRUSH) CDC::GetHalftoneBrush()->GetSafeHandle();
				__EXT_DEBUG_GRID_ASSERT( hBrush != NULL );
				HGDIOBJ hBrushOld = dc.SelectObject( hBrush );
				COLORREF clrBkColorOld = dc.SetBkColor( RGB(255,255,255) );
				if( bLeft && nHeight > 0L )
					dc.PatBlt( rcCell.left, rcCellX.top, _sizeSingleSelectionAreaBorder.cx, nHeight, PATINVERT );
				if( bTop && nWidth > 0L )
					dc.PatBlt( rcCellX.left, rcCell.top, nWidth, _sizeSingleSelectionAreaBorder.cy, PATINVERT );
				if( bRight && nHeight > 0L )
					dc.PatBlt( rcCell.right - _sizeShiftRB.cx - _sizeSingleSelectionAreaBorder.cx + 1, rcCellX.top, _sizeSingleSelectionAreaBorder.cx, nHeight, PATINVERT );
				if( bBottom && nWidth > 0L )
					dc.PatBlt( rcCellX.left, rcCell.bottom - _sizeShiftRB.cy - _sizeSingleSelectionAreaBorder.cy + 1, nWidth, _sizeSingleSelectionAreaBorder.cy, PATINVERT );
				dc.SetBkColor( clrBkColorOld );
				dc.SelectObject( hBrushOld );
			} // if( bLeft || bTop || bRight || bBottom )
		} // if( m_rcFormulaGridToolRectDND.left >= 0 )

	} // if( ( dwAreaFlags & __EGBWA_INNER_CELLS ) != 0 )
}

bool CExtFormulaGridWnd::OnFormulaGridToolRectCreationFilterMessageLoop(
	CRect rcSelArea,
	CRect rcBox,
	CPoint ptClient,
	MSG & _msg,
	bool & bRemoveMessage
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	rcSelArea; rcBox; ptClient; _msg; bRemoveMessage;
	return false;
}

bool CExtFormulaGridWnd::OnFormulaGridToolRectDndFilterMessageLoop(
	CRect rcSelArea,
	CRect rcSelTrack,
	const CExtGridHitTestInfo & htInfoDND,
	MSG & _msg,
	bool & bRemoveMessage
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	rcSelArea; rcSelTrack; htInfoDND; _msg; bRemoveMessage;
	return false;
}

void CExtFormulaGridWnd::OnFormulaGridToolRectHighlight(
	CRect rcSelArea,
	CRect rcHighlightArea,
	bool bHighlight
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
	rcSelArea.NormalizeRect();
	rcHighlightArea.NormalizeRect();
	if(		( m_rcFormulaGridToolRectSelArea == rcSelArea && m_rcFormulaGridToolRectHighlightArea == rcHighlightArea )
		&&	( ! ( (!bHighlight) && m_rcFormulaGridToolRectSelArea.left >= 0 ) )
		)
		return;
	if( bHighlight )
	{
		m_rcFormulaGridToolRectSelArea = rcSelArea;
		m_rcFormulaGridToolRectHighlightArea = rcHighlightArea;
	} // if( bHighlight )
	else
	{
		m_rcFormulaGridToolRectSelArea.SetRect( -1, -1, -1, -1 );
		m_rcFormulaGridToolRectHighlightArea.SetRect( -1, -1, -1, -1 );
	} // else from if( bHighlight )
	OnSwInvalidate( true );
	//OnSwUpdateWindow();
}

void CExtFormulaGridWnd::OnFormulaGridToolRectDndTrack(
	CRect rcSelArea,
	const CExtGridHitTestInfo & htInfoDND
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
	rcSelArea;
DWORD dwMask = __EGWS_ADV_ENABLE_DATA_DND_DRAG | __EGWS_ADV_ENABLE_DATA_DND_DROP;
DWORD dwAdvStyle = AdvGetStyle();
DWORD dwAdvPresentStyles = ( dwAdvStyle & dwMask ); 
	if( dwAdvPresentStyles != dwMask )
		AdvModifyStyle( dwMask, 0, false );
	OnGbwDataDndDo( htInfoDND );
	if( dwAdvPresentStyles != dwMask )
		AdvModifyStyle( 0, (~dwAdvPresentStyles) & dwMask, false );
	return;
}

CExtFormulaGridWnd::e_FormulaGridToolRectMode_t CExtFormulaGridWnd::_OnFormulaGridToolRect_ModeFromPressedKeys()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
bool bShift = CExtPopupMenuWnd::IsKeyPressed( VK_SHIFT ) ? true : false;
	if( bShift )
		return __EFGTRM_EXPAND_SELECTION;
bool bCtrl = CExtPopupMenuWnd::IsKeyPressed( VK_CONTROL ) ? true : false;
	if( bCtrl )
		return __EFGTRM_COPY_FORMULAS;
	return __EFGTRM_SHIFT_FORMULAS;
}

HCURSOR CExtFormulaGridWnd::_OnFormulaGridToolRect_CursorFromMode( CExtFormulaGridWnd::e_FormulaGridToolRectMode_t eFGTRM, CRect rcSelArea, CRect rcBox )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
HCURSOR hCursor = NULL;
	switch( eFGTRM )
	{
	case __EFGTRM_SHIFT_FORMULAS:
		hCursor = ( m_hCursorFormulaRectTool != NULL ) ? m_hCursorFormulaRectTool : ::LoadCursor( NULL, IDC_SIZENWSE );
	break;
	case __EFGTRM_COPY_FORMULAS:
		hCursor = ( m_hCursorFormulaRectToolPlus != NULL ) ? m_hCursorFormulaRectToolPlus : ::LoadCursor( NULL, IDC_SIZENWSE );
	break;
	case __EFGTRM_EXPAND_SELECTION:
		rcSelArea.NormalizeRect();
		rcBox.NormalizeRect();
		if(	!(		( rcSelArea.left != rcBox.left && rcSelArea.right == rcBox.right && rcSelArea.top == rcBox.top && rcSelArea.bottom == rcBox.bottom )
				||	( rcSelArea.left == rcBox.left && rcSelArea.right != rcBox.right && rcSelArea.top == rcBox.top && rcSelArea.bottom == rcBox.bottom )
				||	( rcSelArea.left == rcBox.left && rcSelArea.right == rcBox.right && rcSelArea.top != rcBox.top && rcSelArea.bottom == rcBox.bottom )
				||	( rcSelArea.left == rcBox.left && rcSelArea.right == rcBox.right && rcSelArea.top == rcBox.top && rcSelArea.bottom != rcBox.bottom )
				)
			)
			hCursor = ::LoadCursor( NULL, IDC_SIZEALL );
		else
		{
			if( rcSelArea.left != rcBox.left || rcSelArea.right != rcBox.right )
				hCursor = m_hCursorResizingH2;
			else if( rcSelArea.top != rcBox.top || rcSelArea.bottom != rcBox.bottom )
				hCursor = m_hCursorResizingV2;
			if( hCursor == NULL )
				hCursor = ::LoadCursor( NULL, IDC_SIZEALL );
		}
	break;
#if (defined _DEBUG)
	default:
		__EXT_DEBUG_GRID_ASSERT( FALSE );
	break;
#endif // if (defined _DEBUG)
	} // switch( eFGTRM )
	return hCursor;
}

void CExtFormulaGridWnd::OnFormulaGridToolRectTrack(
	CRect rcSelArea,
	CRect rcBox,
	CPoint ptClient
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( GetSafeHwnd() != NULL );
CPoint ptInitLoc( rcSelArea.right, rcSelArea.bottom );
CPoint ptLastLoc( ptInitLoc );
CRect rcLastCell;
	if( ! GridCellRectsGet( ptLastLoc.x, ptLastLoc.y, 0, 0, &rcLastCell ) )
		return;
	if( ! rcLastCell.PtInRect( ptClient ) )
		return;
CRect rcHighlightAreaInit = rcSelArea;
CRect rcHighlightArea = rcSelArea;
	OnFormulaGridToolRectHighlight( rcSelArea, rcHighlightArea, true );
CPoint ptTrack = ptClient;
e_FormulaGridToolRectMode_t eFGTRM = _OnFormulaGridToolRect_ModeFromPressedKeys();
HCURSOR hCursor = _OnFormulaGridToolRect_CursorFromMode( eFGTRM, rcSelArea, rcHighlightArea );
	if( hCursor == NULL )
		hCursor = ::LoadCursor( NULL, IDC_ARROW );
	::SetCursor( hCursor );
CWinThread * pThread = ::AfxGetThread();
	__EXT_DEBUG_GRID_ASSERT_VALID( pThread );
HWND hWndGrid = m_hWnd;
bool bStopFlag = false, bExpand = false;
	CExtMouseCaptureSink::SetCapture( hWndGrid );
	for( MSG msg; ::IsWindow( hWndGrid ) && (!bStopFlag) ; )
	{ // area tracker message loop
		if( ! ::PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) )
		{
			if(		( ! ::IsWindow( hWndGrid ) )
				||	bStopFlag
				)
				break;
			if( CExtGridWnd::g_bEnableOnIdleCalls )
			{
				for( LONG nIdleCounter = 0L; pThread->OnIdle(nIdleCounter); nIdleCounter ++ );
			}
			::WaitMessage();
			continue;
		} // if( ! ::PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) )
		bool bRemoveMessage = true;
		if( OnFormulaGridToolRectCreationFilterMessageLoop( rcSelArea, rcBox, ptClient, msg, bRemoveMessage ) )
		{
			if( bRemoveMessage )
				::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
			continue;
		}
		switch( msg.message )
		{
		case WM_KILLFOCUS:
			if( msg.hwnd == hWndGrid )
				bStopFlag = true;
		break;
		case WM_CANCELMODE:
		case WM_ACTIVATEAPP:
		case WM_SYSCOMMAND:
		case WM_SETTINGCHANGE:
		case WM_SYSCOLORCHANGE:
			bStopFlag = true;
		break;
		case WM_COMMAND:
			if(		(HIWORD(msg.wParam)) == 0
				||	(HIWORD(msg.wParam)) == 1
				)
				bStopFlag = true;
		break;
		case WM_CAPTURECHANGED:
			if( (HWND)msg.wParam != hWndGrid )
				bStopFlag = true;
		break;

		case WM_SETCURSOR:
			::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
			hCursor = _OnFormulaGridToolRect_CursorFromMode( eFGTRM, rcSelArea, rcHighlightArea );
			if( hCursor == NULL )
				hCursor = ::LoadCursor( NULL, IDC_ARROW );
			::SetCursor( hCursor );
			continue;

		case WM_MOUSEWHEEL:
			if( msg.hwnd != hWndGrid )
				bStopFlag = true;
			else
			{
				::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
				continue;
			} // else from if( msg.hwnd != hWndGrid )
		break;

		case WM_MOUSEMOVE:
			if( msg.hwnd != hWndGrid )
				bStopFlag = true;
			else
			{
				__EXT_DEBUG_GRID_ASSERT_VALID( this );
				::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
 				CPoint point;
				point = DWORD( msg.lParam );
				if( ptTrack != point )
				{
					ptTrack = point;
					if( ! rcLastCell.PtInRect( ptTrack ) )
					{
						CExtGridHitTestInfo htInfo( ptTrack );
						HitTest( htInfo, false, false );
						if(		( ! htInfo.IsHoverEmpty() )
							&&	htInfo.IsValidRect()
							&&	( htInfo.m_dwAreaFlags & __EGBWA_INNER_CELLS ) != 0
							&&	( htInfo.m_nColNo != ptLastLoc.x || htInfo.m_nRowNo != ptLastLoc.y )
							)
						{
							if( htInfo.m_nColNo == ptInitLoc.x || htInfo.m_nRowNo == ptInitLoc.y )
							{
								rcHighlightArea = rcHighlightAreaInit;
								if( htInfo.m_nColNo < rcHighlightAreaInit.left )
									rcHighlightArea.left = htInfo.m_nColNo;
								else
									rcHighlightArea.right = htInfo.m_nColNo;
								if( htInfo.m_nRowNo < rcHighlightAreaInit.top )
									rcHighlightArea.top = htInfo.m_nRowNo;
								else
									rcHighlightArea.bottom = htInfo.m_nRowNo;
								ptLastLoc.x = htInfo.m_nColNo;
								ptLastLoc.y = htInfo.m_nRowNo;
								rcLastCell = htInfo.m_rcItem;
								OnFormulaGridToolRectHighlight( rcSelArea, rcHighlightArea, true );
								hCursor = _OnFormulaGridToolRect_CursorFromMode( eFGTRM, rcSelArea, rcHighlightArea );
								if( hCursor == NULL )
									hCursor = ::LoadCursor( NULL, IDC_ARROW );
								::SetCursor( hCursor );
							} // if( htInfo.m_nColNo == ptInitLoc.x || htInfo.m_nRowNo == ptInitLoc.y )
							else
							{
								CPoint ptLocByX( htInfo.m_nColNo, ptInitLoc.y ), ptLocByY( ptInitLoc.x, htInfo.m_nRowNo );
								CRect rcCellByX, rcCellByY;
								if(		GridCellRectsGet( ptLocByX.x, ptLocByX.y, 0, 0, &rcCellByX )
									&&	GridCellRectsGet( ptLocByY.x, ptLocByY.y, 0, 0, &rcCellByY )
									)
								{ // if valid cell rectangles
									CPoint ptCenterByX = rcCellByX.CenterPoint(), ptCenterByY = rcCellByY.CenterPoint();
									LONG nDistByX = ptCenterByX.x - ptClient.x, nDistByY = ptCenterByY.y - ptClient.y;
									if( nDistByX < 0 )
										nDistByX = - nDistByX;
									if( nDistByY < 0 )
										nDistByY = - nDistByY;
									CPoint & ptLocNew = ( nDistByX >= nDistByY ) ? ptLocByX : ptLocByY;
									if( ptLocNew != ptLastLoc  )
									{
										rcHighlightArea = rcHighlightAreaInit;
										if( ptLocNew.x < rcHighlightAreaInit.left )
											rcHighlightArea.left = ptLocNew.x;
										else
											rcHighlightArea.right = ptLocNew.x;
										if( ptLocNew.y < rcHighlightAreaInit.top )
											rcHighlightArea.top = ptLocNew.y;
										else
											rcHighlightArea.bottom = ptLocNew.y;
										ptLastLoc.x = ptLocNew.x;
										ptLastLoc.y = ptLocNew.y;
										rcLastCell = ( nDistByX >= nDistByY ) ? rcCellByX : rcCellByY;
										OnFormulaGridToolRectHighlight( rcSelArea, rcHighlightArea, true );
										hCursor = _OnFormulaGridToolRect_CursorFromMode( eFGTRM, rcSelArea, rcHighlightArea );
										if( hCursor == NULL )
											hCursor = ::LoadCursor( NULL, IDC_ARROW );
										::SetCursor( hCursor );
									} // if( ptLocNew != ptLastLoc  )
								} // if valid cell rectangles
							} // else from if( htInfo.m_nColNo == ptInitLoc.x || htInfo.m_nRowNo == ptInitLoc.y )
						}
					} // if( ! rcLastCell.PtInRect( ptTrack ) )
				} // if( ptTrack != point )
				continue;
			} // else from if( msg.hwnd != hWndGrid )
		break;

		case WM_LBUTTONUP:
			bStopFlag = true;
			if( msg.hwnd == hWndGrid )
			{
				__EXT_DEBUG_GRID_ASSERT_VALID( this );
				::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
				if( rcHighlightAreaInit != rcHighlightArea )
					bExpand = true;
			}
		break;

		case WM_LBUTTONDOWN:
		case WM_LBUTTONDBLCLK:
		case WM_RBUTTONUP:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONDBLCLK:
		case WM_MBUTTONUP:
		case WM_MBUTTONDOWN:
		case WM_MBUTTONDBLCLK:
		case WM_NCLBUTTONUP:
		case WM_NCLBUTTONDOWN:
		case WM_NCLBUTTONDBLCLK:
		case WM_NCRBUTTONUP:
		case WM_NCRBUTTONDOWN:
		case WM_NCRBUTTONDBLCLK:
		case WM_NCMBUTTONUP:
		case WM_NCMBUTTONDOWN:
		case WM_NCMBUTTONDBLCLK:
		case WM_CONTEXTMENU:
			//( msg.hwnd != hWndGrid )
				bStopFlag = true;
		break;
		default:
			if(		( ! bStopFlag )
				&&	WM_KEYFIRST <= msg.message
				&&	msg.message <= WM_KEYLAST
				)
			{
				if(		msg.hwnd == hWndGrid
					&&	( msg.wParam == VK_CONTROL || msg.wParam == VK_SHIFT )
					)
				{
					::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
					eFGTRM = _OnFormulaGridToolRect_ModeFromPressedKeys();
					hCursor = _OnFormulaGridToolRect_CursorFromMode( eFGTRM, rcSelArea, rcHighlightArea );
					if( hCursor == NULL )
						hCursor = ::LoadCursor( NULL, IDC_ARROW );
					::SetCursor( hCursor );
					continue;
				}
				else
					bStopFlag = true;
			}
		break;
		} // switch( msg.message )
		if( bStopFlag )
 			break;
		if( ! pThread->PumpMessage() )
 			break;
	} // area tracker message loop
	if( ! ::IsWindow( hWndGrid ) )
		return;
	__EXT_DEBUG_GRID_ASSERT( CWnd::FromHandlePermanent(hWndGrid) == this );
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( CExtMouseCaptureSink::GetCapture() == hWndGrid )
		CExtMouseCaptureSink::ReleaseCapture();
	_DoSetCursor();
	OnFormulaGridToolRectHighlight( rcSelArea, rcHighlightArea, false );
	if( bExpand )
		OnFormulaGridToolRectExpand( rcSelArea, rcHighlightArea, eFGTRM );
}

void CExtFormulaGridWnd::OnFormulaGridToolRectExpand(
	CRect rcSelArea,
	CRect rcBox,
	CExtFormulaGridWnd::e_FormulaGridToolRectMode_t eFGTRM
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	rcSelArea.NormalizeRect();
	rcBox.NormalizeRect();
	if( rcSelArea == rcBox )
		return;
	if(	!(		( rcSelArea.left != rcBox.left && rcSelArea.right == rcBox.right && rcSelArea.top == rcBox.top && rcSelArea.bottom == rcBox.bottom )
			||	( rcSelArea.left == rcBox.left && rcSelArea.right != rcBox.right && rcSelArea.top == rcBox.top && rcSelArea.bottom == rcBox.bottom )
			||	( rcSelArea.left == rcBox.left && rcSelArea.right == rcBox.right && rcSelArea.top != rcBox.top && rcSelArea.bottom == rcBox.bottom )
			||	( rcSelArea.left == rcBox.left && rcSelArea.right == rcBox.right && rcSelArea.top == rcBox.top && rcSelArea.bottom != rcBox.bottom )
			)
		)
		return;
bool bErase = false, bShiftFormulas = false, bCloneFormulas = false;
	switch( eFGTRM )
	{
	case __EFGTRM_SHIFT_FORMULAS:
		bCloneFormulas = bShiftFormulas = bErase = true;
	break;
	case __EFGTRM_COPY_FORMULAS:
		bCloneFormulas = bErase = true;
	break;
	case __EFGTRM_EXPAND_SELECTION:
	break;
#if (defined _DEBUG)
	default:
		__EXT_DEBUG_GRID_ASSERT( FALSE );
	break;
#endif // if (defined _DEBUG)
	} // switch( eFGTRM )
	if( bCloneFormulas && rcBox.left < rcSelArea.left )
	{ // expand to left
		CRect rcTarget( rcBox.left, rcSelArea.top, rcSelArea.left - 1, rcSelArea.bottom );
		FormulaDuplicateRectArea( rcSelArea, rcTarget, AFX_IDW_DOCKBAR_LEFT, bShiftFormulas );
	} // expand to left
	if( bCloneFormulas && rcBox.top < rcSelArea.top )
	{ // expand to top
		CRect rcTarget( rcSelArea.left, rcBox.top, rcSelArea.right, rcSelArea.top - 1 );
		FormulaDuplicateRectArea( rcSelArea, rcTarget, AFX_IDW_DOCKBAR_TOP, bShiftFormulas );
	} // expand to top
	if( bErase && rcBox.right < rcSelArea.right )
	{ // erase at right
		CRect rcErase( rcBox.right + 1, rcSelArea.top, rcSelArea.right, rcSelArea.bottom );
		CExtGR2D _range( rcErase );
		FormulaErase( &_range, true, true, false );
	} // erase at right
	if( bErase && rcBox.bottom < rcSelArea.bottom )
	{ // erase at bottom
		CRect rcErase( rcSelArea.left, rcBox.bottom + 1, rcSelArea.right, rcSelArea.bottom );
		CExtGR2D _range( rcErase );
		FormulaErase( &_range, true, true, false );
	} // erase at bottom
	if( bCloneFormulas && rcBox.right > rcSelArea.right )
	{ // expand to right
		CRect rcTarget( rcSelArea.right + 1, rcSelArea.top, rcBox.right, rcSelArea.bottom );
		FormulaDuplicateRectArea( rcSelArea, rcTarget, AFX_IDW_DOCKBAR_RIGHT, bShiftFormulas );
	} // expand to right
	if( bCloneFormulas && rcBox.bottom > rcSelArea.bottom )
	{ // expand to bottom
		CRect rcTarget( rcSelArea.left, rcSelArea.bottom + 1, rcSelArea.right, rcBox.bottom );
		FormulaDuplicateRectArea( rcSelArea, rcTarget, AFX_IDW_DOCKBAR_BOTTOM, bShiftFormulas );
	} // expand to bottom
	SelectionSet( rcBox, true, false, false );
	FocusSet( rcBox.BottomRight(), true, true, false );
}

bool CExtFormulaGridWnd::OnGbwAnalyzeCellMouseClickEvent(
	UINT nChar, // VK_LBUTTON, VK_RBUTTON or VK_MBUTTON only
	UINT nRepCnt, // 0 - button up, 1 - single click, 2 - double click, 3 - post single click & begin editing
	UINT nFlags, // mouse event flags
	CPoint point // mouse pointer in client coordinates
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( 0 <= nRepCnt && nRepCnt <= 3 );
DWORD dwFgStyle = FgGetStyle();
	if( ( dwFgStyle & __FGS_TOOL_RECT_MASK ) != 0 )
	{
		HWND hWndInplaceActive = GetSafeInplaceActiveHwnd();
		if( hWndInplaceActive == NULL )
		{
			CExtGridHitTestInfo htInfo( point );
			HitTest( htInfo, false, true );
			if(		( htInfo.m_dwAreaFlags & __EGBWA_INNER_CELLS ) != 0
					// &&	m_eMTT == __EMTT_NOTHING
				&&	( ! htInfo.IsHoverEmpty() )
				&&	htInfo.IsValidRect()
				&&	( ( SiwGetStyle() & __EGBS_NO_HIDE_SELECTION ) != 0 || OnSiwQueryFocusedControlState() )
				&&	SelectionGetAreaCount() == 1
				)
			{ // if inner cells, if single area selection
				CRect rcSel = SelectionGet( true, 0 );
				bool bLeft   = ( rcSel.left   == htInfo.m_nColNo ) ? true : false;
				bool bTop    = ( rcSel.top    == htInfo.m_nRowNo ) ? true : false;
				bool bRight  = ( rcSel.right == htInfo.m_nColNo ) ? true : false;
				bool bBottom = ( rcSel.bottom == htInfo.m_nRowNo ) ? true : false;
				if( ( bLeft || bRight ) && ( ! ( rcSel.top <= htInfo.m_nRowNo && htInfo.m_nRowNo <= rcSel.bottom ) ) )
					bLeft = bRight = false;
				if( ( bTop || bBottom ) && ( ! ( rcSel.left <= htInfo.m_nColNo && htInfo.m_nColNo <= rcSel.right ) ) )
					bTop = bBottom = false;
				if( bLeft || bTop || bRight || bBottom )
				{
					CSize _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier;
					OnQueryFormulaToolMetrics( _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier );
					if( bRight && bBottom && ( dwFgStyle & __FGS_TOOL_RECT_EXPANDING ) != 0 )
					{ // hit-test corner resize gripper first
						CRect rcBox;
						rcBox.left   = htInfo.m_rcItem.right  - _sizeShiftRB.cx - _sizeSingleSelectionAreaResizier.cx + 1;
						rcBox.top    = htInfo.m_rcItem.bottom - _sizeShiftRB.cy - _sizeSingleSelectionAreaResizier.cy + 1;
						rcBox.right  = rcBox.left + _sizeSingleSelectionAreaResizier.cx;
						rcBox.bottom = rcBox.top  + _sizeSingleSelectionAreaResizier.cy;
						if( rcBox.PtInRect( htInfo.m_ptClient ) )
						{
							if( nRepCnt == 1 )
								OnFormulaGridToolRectTrack( rcSel, rcBox, htInfo.m_ptClient );
							return true;
						}
					} // hit-test corner resize gripper first
					if( ( dwFgStyle & __FGS_TOOL_RECT_DND ) != 0 )
					{ // check tool rect d-n-d
						bool bDND = false;
						LONG nWidth = ( bTop || bBottom ) ? ( htInfo.m_rcItem.right - htInfo.m_rcItem.left ) : 0L;
						LONG nHeight = ( bLeft || bRight ) ? ( htInfo.m_rcItem.bottom - htInfo.m_rcItem.top ) : 0L;
						if( ( ! bDND ) && bLeft && nHeight > 0L )
						{
							CRectWH rc( htInfo.m_rcItem.left, htInfo.m_rcItem.top, _sizeSingleSelectionAreaResizier.cx, nHeight );
							if( rc.PtInRect( htInfo.m_ptClient ) )
								bDND = true;
						}
						if( ( ! bDND ) && bTop && nWidth > 0L )
						{
							CRectWH rc( htInfo.m_rcItem.left, htInfo.m_rcItem.top, nWidth, _sizeSingleSelectionAreaResizier.cy );
							if( rc.PtInRect( htInfo.m_ptClient ) )
								bDND = true;
						}
						if( ( ! bDND ) && bRight && nHeight > 0L )
						{
							CRectWH rc( htInfo.m_rcItem.right - _sizeShiftRB.cx - _sizeSingleSelectionAreaResizier.cx, htInfo.m_rcItem.top, _sizeSingleSelectionAreaResizier.cx, nHeight );
							if( rc.PtInRect( htInfo.m_ptClient ) )
								bDND = true;
						}
						if( ( ! bDND ) && bBottom && nWidth > 0L )
						{
							CRectWH rc( htInfo.m_rcItem.left, htInfo.m_rcItem.bottom - _sizeShiftRB.cy - _sizeSingleSelectionAreaResizier.cy, nWidth, _sizeSingleSelectionAreaResizier.cy );
							if( rc.PtInRect( htInfo.m_ptClient ) )
								bDND = true;
						}
						if( bDND )
						{
							if( nRepCnt == 1 )
								OnFormulaGridToolRectDndTrack( rcSel, htInfo );
							return true;
						} // if( bDND )
					} // check tool rect d-n-d
				} // if( bLeft || bTop || bRight || bBottom )
			} // if inner cells, if single area selection
		} // if( hWndInplaceActive == NULL )
	} // if( ( dwFgStyle & __FGS_TOOL_RECT_MASK ) != 0 )
	return CExtNSB < CExtPPVW < CExtGridWnd > > :: OnGbwAnalyzeCellMouseClickEvent( nChar, nRepCnt, nFlags, point );
}

bool CExtFormulaGridWnd::OnGbwSetCursor(
	const CExtGridHitTestInfo & htInfo
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
HWND hWndInplaceActive = GetSafeInplaceActiveHwnd();
const CExtFormulaDataManager & _FDM = OnGridQueryFormulaDataManager();
	if( hWndInplaceActive != NULL )
	{
		if(		_FDM.m_listEditedFormulaRanges.GetCount() > 0
			&&	( ! htInfo.IsHoverEmpty() )
			&&	htInfo.IsValidRect()
			&&	htInfo.GetInnerOuterTypeOfColumn() == 0
			&&	htInfo.GetInnerOuterTypeOfRow() == 0
			)
			//return true; // the CExtFormulaGridWnd::OnGridFilterInplaceEditingMessageLoop() method will set cursor
		{
			if(		( _FDM.m_bInGridHtLeft || _FDM.m_bInGridHtRight )
				&&	( ! ( _FDM.m_bInGridHtTop || _FDM.m_bInGridHtBottom ) )
				)
				::SetCursor( ( m_hCursorFormulaRangeMove != NULL ) ? m_hCursorFormulaRangeMove : ::LoadCursor( NULL, IDC_SIZEALL /*IDC_SIZEWE*/ ) );
			else
			if(		( _FDM.m_bInGridHtTop || _FDM.m_bInGridHtBottom )
				&&	( ! ( _FDM.m_bInGridHtLeft || _FDM.m_bInGridHtRight ) )
				)
				::SetCursor( ( m_hCursorFormulaRangeMove != NULL ) ? m_hCursorFormulaRangeMove : ::LoadCursor( NULL, IDC_SIZEALL /*IDC_SIZENS*/ ) );
			else
			if(		( _FDM.m_bInGridHtLeft && _FDM.m_bInGridHtTop )
				||	( _FDM.m_bInGridHtRight && _FDM.m_bInGridHtBottom )
				)
				::SetCursor( ( m_hCursorFormulaRangeNWSE != NULL ) ? m_hCursorFormulaRangeNWSE : ::LoadCursor( NULL, IDC_SIZENWSE ) );
			else
			if(		( _FDM.m_bInGridHtLeft && _FDM.m_bInGridHtBottom )
				||	( _FDM.m_bInGridHtRight && _FDM.m_bInGridHtTop )
				)
				::SetCursor( ( m_hCursorFormulaRangeNESW != NULL ) ? m_hCursorFormulaRangeNESW : ::LoadCursor( NULL, IDC_SIZENESW ) );
			else
				::SetCursor( ::LoadCursor( NULL, IDC_ARROW ) );
			return true;
		}
	} // if( hWndInplaceActive != NULL )
	else
	{
		if(		( htInfo.m_dwAreaFlags & __EGBWA_INNER_CELLS ) != 0
				// &&	m_eMTT == __EMTT_NOTHING
			&&	( ! htInfo.IsHoverEmpty() )
			&&	htInfo.IsValidRect()
			)
		{ // if inner cells
			DWORD dwFgStyle = FgGetStyle();
			if(		( dwFgStyle & __FGS_TOOL_RECT_MASK ) != 0
				&&	( ( SiwGetStyle() & __EGBS_NO_HIDE_SELECTION ) != 0 || OnSiwQueryFocusedControlState() )
				&&	SelectionGetAreaCount() == 1
				)
			{ // if single area selection
				CRect rcSel = SelectionGet( true, 0 );
				bool bLeft   = ( rcSel.left   == htInfo.m_nColNo ) ? true : false;
				bool bTop    = ( rcSel.top    == htInfo.m_nRowNo ) ? true : false;
				bool bRight  = ( rcSel.right  == htInfo.m_nColNo ) ? true : false;
				bool bBottom = ( rcSel.bottom == htInfo.m_nRowNo ) ? true : false;
				if( ( bLeft || bRight ) && ( ! ( rcSel.top <= htInfo.m_nRowNo && htInfo.m_nRowNo <= rcSel.bottom ) ) )
					bLeft = bRight = false;
				if( ( bTop || bBottom ) && ( ! ( rcSel.left <= htInfo.m_nColNo && htInfo.m_nColNo <= rcSel.right ) ) )
					bTop = bBottom = false;
				if( bLeft || bTop || bRight || bBottom )
				{
					CSize _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier;
					OnQueryFormulaToolMetrics( _sizeShiftRB, _sizeLinesActive, _sizeLinesInactive, _sizeToolActive, _sizeToolInactive, _sizeSingleSelectionAreaBorder, _sizeSingleSelectionAreaResizier );
					if( bRight && bBottom && ( dwFgStyle & __FGS_TOOL_RECT_EXPANDING ) != 0 )
					{ // hit-test corner resize gripper first
						CRect rcBox;
						rcBox.left   = htInfo.m_rcItem.right  - _sizeShiftRB.cx - _sizeSingleSelectionAreaResizier.cx + 1;
						rcBox.top    = htInfo.m_rcItem.bottom - _sizeShiftRB.cy - _sizeSingleSelectionAreaResizier.cy + 1;
						rcBox.right  = rcBox.left + _sizeSingleSelectionAreaResizier.cx;
						rcBox.bottom = rcBox.top  + _sizeSingleSelectionAreaResizier.cy;
						if( rcBox.PtInRect( htInfo.m_ptClient ) )
						{
							::SetCursor( ( m_hCursorFormulaRectTool != NULL ) ? m_hCursorFormulaRectTool : ::LoadCursor( NULL, IDC_SIZENWSE ) );
							return true;
						}
					} // hit-test corner resize gripper first
					if( ( dwFgStyle & __FGS_TOOL_RECT_DND ) != 0 )
					{ // check tool rect d-n-d
						bool bDND = false;
						LONG nWidth = ( bTop || bBottom ) ? ( htInfo.m_rcItem.right - htInfo.m_rcItem.left ) : 0L;
						LONG nHeight = ( bLeft || bRight ) ? ( htInfo.m_rcItem.bottom - htInfo.m_rcItem.top ) : 0L;
						if( ( ! bDND ) && bLeft && nHeight > 0L )
						{
							CRectWH rc( htInfo.m_rcItem.left, htInfo.m_rcItem.top, _sizeSingleSelectionAreaResizier.cx, nHeight );
							if( rc.PtInRect( htInfo.m_ptClient ) )
								bDND = true;
						}
						if( ( ! bDND ) && bTop && nWidth > 0L )
						{
							CRectWH rc( htInfo.m_rcItem.left, htInfo.m_rcItem.top, nWidth, _sizeSingleSelectionAreaResizier.cy );
							if( rc.PtInRect( htInfo.m_ptClient ) )
								bDND = true;
						}
						if( ( ! bDND ) && bRight && nHeight > 0L )
						{
							CRectWH rc( htInfo.m_rcItem.right - _sizeSingleSelectionAreaResizier.cx - _sizeShiftRB.cx, htInfo.m_rcItem.top, _sizeSingleSelectionAreaResizier.cx, nHeight );
							if( rc.PtInRect( htInfo.m_ptClient ) )
								bDND = true;
						}
						if( ( ! bDND ) && bBottom && nWidth > 0L )
						{
							CRectWH rc( htInfo.m_rcItem.left, htInfo.m_rcItem.bottom - _sizeSingleSelectionAreaResizier.cy - _sizeShiftRB.cy, nWidth, _sizeSingleSelectionAreaResizier.cy );
							if( rc.PtInRect( htInfo.m_ptClient ) )
								bDND = true;
						}
						if( bDND )
						{
							//bool bMove = CExtPopupMenuWnd::IsKeyPressed( VK_CONTROL ) ? false : true;
							::SetCursor(
							//	bMove
							//		?
									  ( ( m_hCursorFormulaRangeMove != NULL ) ? m_hCursorFormulaRangeMove : ::LoadCursor( NULL, IDC_SIZEALL ) )
							//		: ( ( m_hCursorFormulaRangeCopy != NULL ) ? m_hCursorFormulaRangeCopy : ::LoadCursor( NULL, IDC_SIZEALL ) )
								);
							return true;
						} // if( bDND )
					} // check tool rect d-n-d
				} // if( bLeft || bTop || bRight || bBottom )
			} // if single area selection
			if( CExtNSB < CExtPPVW < CExtGridWnd > > :: OnGbwSetCursor( htInfo ) )
				return true;
			if( m_hCursorDefaultInnerFC != NULL )
			{
				::SetCursor( m_hCursorDefaultInnerFC );
				return true;
			}
			return false;
		} // if inner cells
	} // else from if( hWndInplaceActive != NULL )
	return CExtNSB < CExtPPVW < CExtGridWnd > > :: OnGbwSetCursor( htInfo );
}

bool CExtFormulaGridWnd::OnGridFilterQueryFilteredState(
	bool bFilteringRows,
	LONG nRowColNo, // = -1, // if -1 - ignore nRowColNo, check all ranges at both outer sides and fill p_nRowColNoFound & p_bTopLeftFound
	bool bTopLeft, // = true
	LONG * p_nRowColNoFound, // = NULL
	bool * p_bTopLeftFound // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bFilteringRows;
	nRowColNo;
	bTopLeft;
	p_nRowColNoFound;
	p_bTopLeftFound;
	return false;
}

bool CExtFormulaGridWnd::ColumnInsert(
	LONG nColNo, // -1 or greater than count - append
	LONG nColInsertCount, // = 1L
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ! CExtNSB < CExtPPVW < CExtGridWnd > > :: ColumnInsert( nColNo, nColInsertCount, false ) )
		return false;
LONG nColCount = ColumnCountGet();
LONG nRowCount = RowCountGet();
CRectWH rcBefore( nColNo,                   0L, nColCount - 1L, nRowCount - 1L );
CRectWH rcAfter(  nColNo + nColInsertCount, 0L, nColCount - 1L, nRowCount - 1L );
CExtGR2D _rangeBefore = rcBefore, _rangeAfter = rcAfter;
CExtFormulaDataManager & _FDM = OnGridQueryFormulaDataManager();
	_FDM._RFE_RemoveRange( _rangeBefore );
CSize sizeShift( nColInsertCount, 0L );
CExtGR2D _rangeShift = _rangeBefore + _rangeAfter;
for_each_shift_range_formulas_t _FE1( *this, _rangeShift, sizeShift );
	if( ! _FE1.ComputeRange() )
		return false;
	if( ! FormulaCompute( &_rangeAfter, false ) )
		return false;
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw )
	return true;
}

bool CExtFormulaGridWnd::RowInsert(
	LONG nRowNo, // -1 or greater than count - append
	LONG nRowInsertCount, // = 1L
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ! CExtNSB < CExtPPVW < CExtGridWnd > > :: RowInsert( nRowNo, nRowInsertCount, false ) )
		return false;
LONG nColCount = ColumnCountGet();
LONG nRowCount = RowCountGet();
CRectWH rcBefore( 0L, nRowNo,                   nColCount - 1L, nRowCount - 1L );
CRectWH rcAfter(  0L, nRowNo + nRowInsertCount, nColCount - 1L, nRowCount - 1L );
CExtGR2D _rangeBefore = rcBefore, _rangeAfter = rcAfter;
CExtFormulaDataManager & _FDM = OnGridQueryFormulaDataManager();
	_FDM._RFE_RemoveRange( _rangeBefore );
CSize sizeShift( 0L, nRowInsertCount );
CExtGR2D _rangeShift = _rangeBefore + _rangeAfter;
for_each_shift_range_formulas_t _FE1( *this, _rangeShift, sizeShift );
	if( ! _FE1.ComputeRange() )
		return false;
	if( ! FormulaCompute( &_rangeAfter, false ) )
		return false;
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw )
	return true;
}

LONG CExtFormulaGridWnd::ColumnRemove(
	LONG nColNo,
	LONG nColRemoveCount, // = 1L // -1 - remove up to end (truncate)
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
LONG nColCount = ColumnCountGet();
LONG nRowCount = RowCountGet();
CRectWH rcBefore( nColNo, 0L, nColCount - 1L, nRowCount - 1L );
LONG nRemovedCount = CExtNSB < CExtPPVW < CExtGridWnd > > :: ColumnRemove( nColNo, nColRemoveCount, false );
	if( nRemovedCount == 0L )
		return 0L;
	nColCount = ColumnCountGet();
CRectWH rcAfter( nColNo, 0L, nColCount - 1L, nRowCount - 1L );
CExtGR2D _rangeBefore = rcBefore, _rangeAfter = rcAfter;
CExtFormulaDataManager & _FDM = OnGridQueryFormulaDataManager();
	_FDM._RFE_RemoveRange( _rangeBefore );
CSize sizeShift( -nColRemoveCount, 0L );
CExtGR2D _rangeShift = _rangeAfter;
	_rangeShift -= CRectWH( nColNo, 0L, 1L, nRowCount - 1L );
	// TO-FIX: range deletion does not cause formula reference shifting in all the cases
for_each_shift_range_formulas_t _FE1( *this, _rangeShift, sizeShift );
	if( ! _FE1.ComputeRange() )
		return false;
	if( ! FormulaCompute( &_rangeAfter, false ) )
		return false;
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw )
	return nRemovedCount;
}

LONG CExtFormulaGridWnd::RowRemove(
	LONG nRowNo,
	LONG nRowRemoveCount, // = 1L // -1 - remove up to end (truncate)
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
LONG nColCount = ColumnCountGet();
LONG nRowCount = RowCountGet();
CRectWH rcBefore( 0L, nRowNo, nColCount - 1L, nRowCount - 1L );
LONG nRemovedCount = CExtNSB < CExtPPVW < CExtGridWnd > > :: RowRemove( nRowNo, nRowRemoveCount, false );
	if( nRemovedCount == 0L )
		return 0L;
	nRowCount = RowCountGet();
CRectWH rcAfter( 0L, nRowNo, nColCount - 1L, nRowCount - 1L );
CExtGR2D _rangeBefore = rcBefore, _rangeAfter = rcAfter;
CExtFormulaDataManager & _FDM = OnGridQueryFormulaDataManager();
	_FDM._RFE_RemoveRange( _rangeBefore );
CSize sizeShift( 0L, -nRowRemoveCount );
CExtGR2D _rangeShift = _rangeAfter;
	_rangeShift -= CRectWH( 0L, nRowNo, nColCount - 1L, 1L );
	// TO-FIX: range deletion does not cause formula reference shifting in all the cases
for_each_shift_range_formulas_t _FE1( *this, _rangeShift, sizeShift );
	if( ! _FE1.ComputeRange() )
		return false;
	if( ! FormulaCompute( &_rangeAfter, false ) )
		return false;
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw )
	return nRemovedCount;
}

bool CExtFormulaGridWnd::OnGbwAccelCommand(
	DWORD dwEGSA,
	CCmdUI * pCmdUI, // = NULL
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	switch( dwEGSA )
	{
	case __EGSA_IN_ROW_TAB_NEXT:
	case __EGSA_IN_ROW_TAB_PREV:
	case __EGSA_IN_COLUMN_TAB_NEXT:
	case __EGSA_IN_COLUMN_TAB_PREV:
		{
			HWND hWnd = GetSafeInplaceActiveHwnd();
			if( hWnd != NULL )
			{
				CWnd * pWndInplaceControl = CWnd::FromHandlePermanent( hWnd );
				if( pWndInplaceControl != NULL )
				{
					CExtFormulaInplaceEdit * pWndFIE = DYNAMIC_DOWNCAST( CExtFormulaInplaceEdit, pWndInplaceControl );
					if( pWndFIE != NULL )
					{
						CExtFormulaInplaceListBox * pWndIPLB = pWndFIE->OnQueryIPLB( false );
						if( pWndIPLB != NULL && ( pWndIPLB->GetStyle() & WS_VISIBLE ) != 0 )
							return false; // do not use tab navigation when the auto complete list box is visible
					}
				}
			}
		}
	break;
	case __EGSA_SELECTED_RANGE_LOAD:
	case __EGSA_PASTE:
	case __EGSA_CUT:
	case __EGSA_DELETE_SELECTION:
		if( ! CExtNSB < CExtPPVW < CExtGridWnd > > :: OnGbwAccelCommand( dwEGSA, pCmdUI, false ) )
			return false;
		else if( pCmdUI == NULL )
			FormulaCompute( NULL, bRedraw );
		return true;
	} // switch( dwEGSA )
	// finally, handle/update commands implemented in parent class
	return CExtNSB < CExtPPVW < CExtGridWnd > > :: OnGbwAccelCommand( dwEGSA, pCmdUI, bRedraw );
}

void CExtFormulaGridWnd::OnGridDataDndComplete( bool bCanceled )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	CExtNSB < CExtPPVW < CExtGridWnd > > :: OnGridDataDndComplete( bCanceled );
	if( ! bCanceled )
		FormulaCompute();
}

BOOL CExtFormulaGridWnd::OnGridDropTargetDrop( CWnd * pWnd, COleDataObject * pDataObject, DROPEFFECT dropEffect, CPoint point )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
BOOL bRetVal = CExtNSB < CExtPPVW < CExtGridWnd > > :: OnGridDropTargetDrop( pWnd, pDataObject, dropEffect, point );
	if( bRetVal && m_ptCellDndStart.x < 0L && m_ptCellDndStart.y < 0L )
		FormulaCompute();
	return bRetVal;
}
#endif // (!defined __EXT_MFC_NO_FORMULA_GRID)

