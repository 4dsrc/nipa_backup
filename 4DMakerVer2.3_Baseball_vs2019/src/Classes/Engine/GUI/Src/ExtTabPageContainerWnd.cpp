// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_CTRL)

#if (! defined __EXT_TAB_PAGE_CONTAINER_WND_H)
	#include <ExtTabPageContainerWnd.h>
#endif

#if (!defined __EXT_MEMORY_DC_H)
	#include <../Src/ExtMemoryDC.h>
#endif

#if (!defined __EXT_POPUP_MENU_WND_H)
	#include <ExtPopupMenuWnd.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerWnd

IMPLEMENT_DYNCREATE(CExtTabPageContainerWnd, CWnd)
IMPLEMENT_CExtPmBridge_MEMBERS( CExtTabPageContainerWnd );

bool CExtTabPageContainerWnd::g_bHandlePriorNextKeyEvents = true;
bool CExtTabPageContainerWnd::g_bHandleTabKeyEvents = false;
bool CExtTabPageContainerWnd::g_bAllowAccelBasedPageSelection = true;

CExtTabPageContainerWnd::CExtTabPageContainerWnd()
	: m_bDirectCreateCall( false )
	, m_bTabWndVisible( true )
	, m_pWndTab( NULL )
	, m_hFont( NULL )
	, m_bAllowAccelBasedPageSelection( CExtTabPageContainerWnd::g_bAllowAccelBasedPageSelection )
	, m_bHandlePriorNextKeyEvents( CExtTabPageContainerWnd::g_bHandlePriorNextKeyEvents )
	, m_bHandleTabKeyEvents( CExtTabPageContainerWnd::g_bHandleTabKeyEvents )
{
	VERIFY( RegisterTabCtrlWndClass() );

	PmBridge_Install();
}

CExtTabPageContainerWnd::~CExtTabPageContainerWnd()
{
	PmBridge_Uninstall();
}

BEGIN_MESSAGE_MAP(CExtTabPageContainerWnd, CWnd)
	//{{AFX_MSG_MAP(CExtTabPageContainerWnd)
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_SETFONT, OnSetFont)
	ON_MESSAGE(WM_GETFONT, OnGetFont)	
	ON_WM_NCPAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CExtTabWnd * CExtTabPageContainerWnd::GetSafeTabWindow()
{
	ASSERT_VALID( this );
	VERIFY( _CreateHelper() );
	if( m_pWndTab == NULL )
		return NULL;
	ASSERT_VALID( m_pWndTab );
	if( m_pWndTab->GetSafeHwnd() == NULL )
		return NULL;
	return m_pWndTab;
}

const CExtTabWnd * CExtTabPageContainerWnd::GetSafeTabWindow() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtTabPageContainerWnd * > ( this ) )
			-> GetSafeTabWindow();
}

/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerWnd message handlers
 
LRESULT CExtTabPageContainerWnd::OnSetFont( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
HFONT hFont = (HFONT) wParam;
BOOL bRedraw = (BOOL) lParam;
	m_hFont = hFont; 
	_RepositionBarsImpl();
    if( bRedraw )
        Invalidate();
	return 0L;
}
  
LRESULT CExtTabPageContainerWnd::OnGetFont( WPARAM, LPARAM )
{
	ASSERT_VALID( this );
    return (LRESULT) m_hFont;
}

//////////////////////////////////////////////////////////////////////////
void CExtTabPageContainerWnd::OnNcPaint()
{
	__super::OnNcPaint();

	CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID(pPM);

	pPM->OnNcPaintFrameRect(this);
}

HCURSOR CExtTabPageContainerWnd::g_hCursor = ::LoadCursor( NULL, IDC_ARROW );
bool CExtTabPageContainerWnd::g_bTabCtrlWndClassRegistered = false;

bool CExtTabPageContainerWnd::RegisterTabCtrlWndClass()
{
	if( g_bTabCtrlWndClassRegistered )
		return true;
WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo( hInst, __EXT_TAB_PAGE_CONTAINER_CLASS_NAME, &_wndClassInfo ) )
	{
		_wndClassInfo.style = CS_GLOBALCLASS|CS_DBLCLKS|CS_HREDRAW|CS_VREDRAW;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor = ( g_hCursor != NULL ) ? g_hCursor : ::LoadCursor( NULL, IDC_ARROW );
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_TAB_PAGE_CONTAINER_CLASS_NAME;
		if( ! ::AfxRegisterClass( &_wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}
	g_bTabCtrlWndClassRegistered = true;
	return true;
}

BOOL CExtTabPageContainerWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
	UINT nDlgCtrlID, // = UINT( __EXT_MFC_IDC_STATIC )
	DWORD dwWindowStyle, // = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS
	CCreateContext * pContext // = NULL
	)
{
	if( ! RegisterTabCtrlWndClass() )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	m_bDirectCreateCall = true;
	if( ! CWnd::Create(
			__EXT_TAB_PAGE_CONTAINER_CLASS_NAME,
			NULL,
			dwWindowStyle,
			rcWnd,
			pParentWnd,
			nDlgCtrlID,
			pContext
			)
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		::AfxThrowMemoryException();
	}
	return TRUE;
}

HFONT CExtTabPageContainerWnd::OnQueryFont() const
{
	ASSERT_VALID( this );
HFONT hFont = NULL;
	if( GetSafeHwnd() != NULL )
		hFont = (HFONT) ::SendMessage( m_hWnd, WM_GETFONT, 0L, 0L );
	if( hFont == NULL )
	{
		HWND hWndParent = ::GetParent( m_hWnd );
		if( hWndParent != NULL )
			hFont = (HFONT)
				::SendMessage( hWndParent, WM_GETFONT, 0L, 0L );
	} // if( hFont == NULL )
	if( hFont == NULL )
	{
		hFont = (HFONT)::GetStockObject( DEFAULT_GUI_FONT );
		if( hFont == NULL )
			hFont = (HFONT)::GetStockObject( SYSTEM_FONT );
	} // if( hFont == NULL )
	return hFont;
}

void CExtTabPageContainerWnd::DoPaint( CDC * pDC )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC );
CRect rcClient;
	GetClientRect( &rcClient );
CExtPaintManager::stat_ExcludeChildAreas(
		pDC->GetSafeHdc(),
		GetSafeHwnd()
		);
	if(		(! PmBridge_GetPM()->GetCb2DbTransparentMode(this) )
		||	(! PmBridge_GetPM()->PaintDockerBkgnd( true, *pDC, this ) )
		)
		pDC->FillSolidRect(
			&rcClient,
			PmBridge_GetPM()->GetColor(
				CExtPaintManager::CLR_3DFACE_OUT, this
				)
			);
}

LRESULT CExtTabPageContainerWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if( message == (WM_USER+200) )
	{
		VERIFY( _CreateHelper() );
		return 0L;
	}
	if(		message == WM_PRINT
		||	message == WM_PRINTCLIENT
		)
	{
		CDC * pDC = CDC::FromHandle( (HDC) wParam );
		
		CRect rcWnd, rcClient;
		GetWindowRect( &rcWnd );
		GetClientRect( &rcClient );
		ClientToScreen( rcClient );
		rcClient.OffsetRect( -rcWnd.TopLeft() );
		rcWnd.OffsetRect( -rcWnd.TopLeft() );

		if( (lParam&PRF_NONCLIENT) != 0 )
		{
			pDC->ExcludeClipRect(rcClient);
			PmBridge_GetPM()->PaintResizableBarChildNcAreaRect(
				*pDC,
				rcWnd,
				this
				);
			pDC->SelectClipRgn( NULL );
		}
	
		if( (lParam&(PRF_CLIENT|PRF_ERASEBKGND)) != 0 )
		{
			CPoint ptVpOffset( 0, 0 );
			if( (lParam&PRF_NONCLIENT) != 0 )
			{
				ptVpOffset.x = rcWnd.left - rcClient.left;
				ptVpOffset.y = rcWnd.top - rcClient.top;
			}
			if(		ptVpOffset.x != 0
				||	ptVpOffset.y != 0
				)
				pDC->OffsetViewportOrg(
					-ptVpOffset.x,
					-ptVpOffset.y
					);
			DoPaint( pDC );
			if(		ptVpOffset.x != 0
				||	ptVpOffset.y != 0
				)
				pDC->OffsetViewportOrg(
					ptVpOffset.x,
					ptVpOffset.y
					);
		} // if( (lParam&(PRF_CLIENT|PRF_ERASEBKGND)) != 0 )
		
		if( (lParam&PRF_CHILDREN) != 0 )
			CExtPaintManager::stat_PrintChildren(
				m_hWnd,
				message,
				pDC->GetSafeHdc(),
				lParam,
				false
				);
		return (!0);
	}

	switch( message )
	{
	case WM_CLOSE:
		if( (GetStyle() & WS_CHILD) != 0 )
			return 0L;
	break;

	case WM_PAINT:
		{
			CPaintDC dcPaint( this );
			DoPaint( &dcPaint );
		}
		return TRUE;
		
	case WM_ERASEBKGND:
		return FALSE;
		
	case WM_SIZE:
	case WM_WINDOWPOSCHANGED:
		_RepositionBarsImpl();
		break;
	case WM_CREATE:
		if( ! _CreateHelper() )
		{
			ASSERT( FALSE );
			::AfxThrowMemoryException();
		}
		break;
	case WM_DESTROY:
		HookSpyUnregister();
		m_pWndTab = NULL;
		break;
	} // switch( message )
	
	return CWnd::WindowProc(message, wParam, lParam);
}

BOOL CExtTabPageContainerWnd::PreTranslateMessage(MSG* pMsg) 
{
	if(		pMsg->message == WM_KEYDOWN
// 		&&	(	pMsg->wParam == VK_PRIOR
// 			||	pMsg->wParam == VK_NEXT
// 			)
// 		&&	CExtPopupMenuWnd::IsKeyPressed( VK_CONTROL )
// 		&&	(! CExtPopupMenuWnd::IsKeyPressed( VK_SHIFT ) )
// 		&&	(! CExtPopupMenuWnd::IsKeyPressed( VK_MENU ) )
		&&	(
				(	m_bHandlePriorNextKeyEvents
				&&	( pMsg->wParam == VK_PRIOR || pMsg->wParam == VK_NEXT	)
				&&	CExtPopupMenuWnd::IsKeyPressed( VK_CONTROL )
				&&	(! CExtPopupMenuWnd::IsKeyPressed( VK_SHIFT ) )
				&&	(! CExtPopupMenuWnd::IsKeyPressed( VK_MENU ) )
				)
			||
				(	m_bHandleTabKeyEvents
				&&	( pMsg->wParam == VK_TAB	)
				&&	CExtPopupMenuWnd::IsKeyPressed( VK_CONTROL )
				&&	(! CExtPopupMenuWnd::IsKeyPressed( VK_MENU ) )
				)
			)
		)
	{
		if(	pMsg->wParam == VK_PRIOR || pMsg->wParam == VK_NEXT	)
			PagesNavigate( ( pMsg->wParam == VK_NEXT ) ? TRUE : FALSE );
		else
			PagesNavigate( (! CExtPopupMenuWnd::IsKeyPressed( VK_SHIFT ) ) ? TRUE : FALSE );
		return TRUE;
	}
	return CWnd::PreTranslateMessage( pMsg );
}

BOOL CExtTabPageContainerWnd::PageInsert(
	HWND hWnd,
	__EXT_MFC_SAFE_LPCTSTR sItemText, // = NULL
	HICON hIcon, // = NULL
	bool bCopyIcon, // = true
	LONG nPos, // = -1L
	bool bSelect // = false
	)
{
CExtCmdIcon pageIcon;
	pageIcon.AssignFromHICON( hIcon, bCopyIcon );
	return
		PageInsert(
			hWnd,
			pageIcon,
			sItemText,
			nPos,
			bSelect
			);
}

BOOL CExtTabPageContainerWnd::PageInsert(
	HWND hWnd,
	const CExtCmdIcon & pageIcon,
	__EXT_MFC_SAFE_LPCTSTR sItemText, // = NULL
	LONG nPos, // = -1L
	bool bSelect // = false
	)
{
	ASSERT_VALID( this );
	if(		hWnd   == NULL || (! ::IsWindow(hWnd)   )
		||	m_hWnd == NULL || (! ::IsWindow(m_hWnd) )
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	VERIFY( _CreateHelper() );
LONG nCount = m_pWndTab->ItemGetCount();
	if( nCount == 0L )
		bSelect = true;
	if( nPos < 0L || nPos > nCount )
		nPos = nCount; // append
	if( ::GetParent(hWnd) != m_hWnd )
		::SetParent( hWnd, m_hWnd );
	if( bSelect )
	{
		if( nCount > 0L )
		{
			LONG i = m_pWndTab->SelectionGet();
			if( i >= 0L )
				::ShowWindow(
					(HWND)m_pWndTab->ItemLParamGet(i),
					SW_HIDE
					);
		}
		if( ( ::__EXT_MFC_GetWindowLong( hWnd, GWL_STYLE ) & WS_VISIBLE ) == 0 )
			::ShowWindow( hWnd, SW_SHOW );
	} // if( bSelect )
	else
	{
		if( ( ::__EXT_MFC_GetWindowLong( hWnd, GWL_STYLE ) & WS_VISIBLE ) != 0 )
			::ShowWindow( hWnd, SW_HIDE );
	} // else from if( bSelect )
	
	m_pWndTab->ItemInsert(
		sItemText,
		pageIcon,
		0,
		nPos,
		(LPARAM)hWnd,
		false
		);
	
	nCount++;
	for( LONG i = nPos; i < nCount; i++ )
		::__EXT_MFC_SetWindowLong(
			(HWND)m_pWndTab->ItemLParamGet(i),
			GWL_ID,
			0x101 + i
			);
	if( bSelect )
		m_pWndTab->SelectionSet( nPos, true, true );
	else
		m_pWndTab->UpdateTabWnd( true );

	_RepositionBarsImpl();
				
	return true;
}

BOOL CExtTabPageContainerWnd::PageInsert(
	CWnd * pWnd,
	__EXT_MFC_SAFE_LPCTSTR sItemText,// = NULL,
	HICON hIcon,// = NULL,
	bool bCopyIcon,// = true,
	LONG nPos, // = -1L
	bool bSelect // = false
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pWnd );
	return
		PageInsert(
			pWnd->GetSafeHwnd(),
			sItemText,
			hIcon,
			bCopyIcon,
			nPos,
			bSelect
			);
}

BOOL CExtTabPageContainerWnd::PageInsert(
	CWnd * pWnd,
	const CExtCmdIcon & pageIcon,
	__EXT_MFC_SAFE_LPCTSTR sItemText,// = NULL,
	LONG nPos, // = -1L
	bool bSelect // = false
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pWnd );
	return
		PageInsert(
			pWnd->GetSafeHwnd(),
			pageIcon,
			sItemText,
			nPos,
			bSelect
			);
}

LONG CExtTabPageContainerWnd::PageRemove( 
	LONG nIndex,
	LONG nCountToRemove, // = 1
	bool bDestroyPageWnd // = true
	)
{
	ASSERT_VALID( this );
	VERIFY( _CreateHelper() );
	if( nIndex > m_pWndTab->ItemGetCount() - 1L )
		return 0L;

LONG nCountRemoved = 0L; // returns count of removed items

LONG nCount = m_pWndTab->ItemGetCount();
LONG nAvailToRemove = nCount - nIndex;

	if( nCountToRemove > nAvailToRemove )
		nCountToRemove = nAvailToRemove;
LONG i;
	for( i = 0L; i < nCountToRemove; i++ )
	{
		HWND hWnd = (HWND) m_pWndTab->ItemLParamGet( nIndex );
		if(		hWnd != NULL 
			&&	::IsWindow( hWnd )
			)
		{
			::ShowWindow( hWnd, SW_HIDE );
			if(	bDestroyPageWnd )
				::DestroyWindow( hWnd );
		}
		if( m_pWndTab->ItemRemove( nIndex, 1L, false ) > 0L )
			nCountRemoved++;
	}

	nCount = m_pWndTab->ItemGetCount();
	for( i = 0L; i < nCount; i++ )
		::__EXT_MFC_SetWindowLong(
			(HWND)m_pWndTab->ItemLParamGet(i),
			GWL_ID,
			0x101 + i
			);

	// select previous page
	if( nCount > 0L )
	{
		if( nIndex > 0L )
			nIndex--;
		PageSelectionSet( nIndex );
	}

	_RepositionBarsImpl();

	if( nCountRemoved > 0L )
		m_pWndTab->UpdateTabWnd( true );

	return nCountRemoved;
}

LONG CExtTabPageContainerWnd::PageRemoveAll( 
	bool bDestroyPageWnd // = true 
	)
{
	// returns count of removed items
	ASSERT_VALID( this );
	LONG nRemovedCount = 0L;
	while ( PageRemove( 0L, 1L, bDestroyPageWnd ) > 0 ) 
		nRemovedCount++;
	return nRemovedCount;
}

HWND CExtTabPageContainerWnd::PageHwndSet(
	LONG nIndex,
	HWND hWndNew
	)
{
	ASSERT_VALID( this );
	ASSERT( hWndNew != NULL && ::IsWindow(hWndNew) );
	if( GetSafeTabWindow() == NULL )
		return NULL;
HWND hWndOld = PageHwndGetSafe(nIndex);
	ASSERT( hWndOld != NULL && ::IsWindow(hWndOld) );
	if( hWndNew == hWndOld )
		return hWndNew;
	if( ::GetParent(hWndNew) != m_hWnd )
		::SetParent( hWndNew, m_hWnd );
	m_pWndTab->ItemLParamSet( nIndex, LPARAM(hWndNew) );
	LONG nSelIdx = PageSelectionGet();
	if( nSelIdx == nIndex )
	{
		if( ( ::__EXT_MFC_GetWindowLong( hWndNew, GWL_STYLE ) & WS_VISIBLE ) == 0 )
			::ShowWindow( hWndNew, SW_SHOW );
	} // if( nSelIdx == nIndex )
	else
	{
		if( ( ::__EXT_MFC_GetWindowLong( hWndNew, GWL_STYLE ) & WS_VISIBLE ) != 0 )
			::ShowWindow( hWndNew, SW_HIDE );
	} // else from if( nSelIdx == nIndex )
	::__EXT_MFC_SetWindowLong(
		hWndNew,
		GWL_ID,
		::__EXT_MFC_GetWindowLong( hWndOld, GWL_ID )
		);
	_RepositionBarsImpl();
	m_pWndTab->UpdateTabWnd( true );
	return hWndOld;
}

HWND CExtTabPageContainerWnd::PageHwndGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return NULL;
LPARAM lParam = m_pWndTab->ItemLParamGet( nIndex );
	HWND hWnd = (HWND)lParam;
	return hWnd;
}

HWND CExtTabPageContainerWnd::PageHwndGetSafe( LONG nIndex ) const
{
	ASSERT_VALID( this );
	HWND hWnd = PageHwndGet(nIndex);
	if( hWnd != NULL )
	{
		if( ! ::IsWindow(hWnd) )
			hWnd = NULL;
	}
	return hWnd;
}

const CWnd * CExtTabPageContainerWnd::PagePermanentWndGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	HWND hWnd = PageHwndGetSafe( nIndex );
	if( hWnd == NULL )
		return NULL;
	CWnd * pWnd = CWnd::FromHandlePermanent( hWnd );
	return pWnd;
}

CWnd * CExtTabPageContainerWnd::PagePermanentWndGet( LONG nIndex )
{
	ASSERT_VALID( this );
	HWND hWnd = PageHwndGetSafe( nIndex );
	if( hWnd == NULL )
		return NULL;
	CWnd * pWnd = CWnd::FromHandlePermanent( hWnd );
	return pWnd;
}

bool CExtTabPageContainerWnd::PageMove(
	LONG nIndex,
	LONG nNewIndex,
	bool bUpdateTabWnd // = false
	)
{
	 // move page into the new position
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
bool bRet = m_pWndTab->ItemMove( nIndex, nNewIndex, bUpdateTabWnd );
	CWnd::RepositionBars( 0, 0xFFFF, 0x101 + nIndex );		
	return bRet;
}

LONG CExtTabPageContainerWnd::PageGetCount() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return 0L;
	return m_pWndTab->ItemGetCount();
}

LONG CExtTabPageContainerWnd::PageSelectionGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return -1L;
	return m_pWndTab->SelectionGet();
}

void CExtTabPageContainerWnd::PageSelectionSet( LONG nPos )
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return;
	m_pWndTab->SelectionSet( nPos, true, true );
}

void CExtTabPageContainerWnd::PageSelectionSetByWnd( CWnd * pWnd )
{
	ASSERT_VALID( this );
	if( pWnd == NULL )
	{
		ASSERT( FALSE );
		return;
	}
	ASSERT_VALID( pWnd );
	PageSelectionSetByHWND( pWnd->m_hWnd );
}

void CExtTabPageContainerWnd::PageSelectionSetByHWND( HWND hWnd )
{
	ASSERT_VALID( this );
	if(		hWnd == NULL 
		||	( !::IsWindow( hWnd ) )
		)
	{
		ASSERT( FALSE );
		return;
	}
LONG nPos = PageFindByHWND( hWnd );
	if( nPos >= 0L )
		PageSelectionSet( nPos );
}

CExtCmdIcon & CExtTabPageContainerWnd::PageIconGet( LONG nIndex )
{
	ASSERT_VALID( this );
	VERIFY( _CreateHelper() );
	return m_pWndTab->ItemIconGet( nIndex );
}

const CExtCmdIcon & CExtTabPageContainerWnd::PageIconGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	VERIFY( ( const_cast < CExtTabPageContainerWnd * > ( this ) ) -> _CreateHelper() );
	return m_pWndTab->ItemIconGet( nIndex );
}

void CExtTabPageContainerWnd::PageIconSet(
	LONG nIndex,
	const CExtCmdIcon & _icon,
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	VERIFY( _CreateHelper() );
	m_pWndTab->ItemIconSet( nIndex, _icon, bUpdateTabWnd );
}

void CExtTabPageContainerWnd::PageIconSet(
	LONG nIndex,
	HICON hIcon, // = NULL
	bool bCopyIcon, // = true
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	VERIFY( _CreateHelper() );
	m_pWndTab->ItemIconSet( nIndex, hIcon, bCopyIcon, bUpdateTabWnd
		);
}

void CExtTabPageContainerWnd::PageCenterTextSet( 
	LONG nIndex,
	bool bSet, // = true 
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	VERIFY( _CreateHelper() );
	m_pWndTab->ItemStyleModify( nIndex, bSet ? 0 : __ETWI_CENTERED_TEXT, bSet ? __ETWI_CENTERED_TEXT : 0, bUpdateTabWnd );
}

bool CExtTabPageContainerWnd::PageCenterTextGet(
	LONG nIndex
	) const
{
	ASSERT_VALID( this );
	VERIFY( ( const_cast < CExtTabPageContainerWnd * > ( this ) ) -> _CreateHelper() );
bool bCenteredText = false;
	if( (m_pWndTab->ItemStyleGet( nIndex ) & __ETWI_CENTERED_TEXT) != 0 )
		bCenteredText = true;
	return bCenteredText;
}

__EXT_MFC_SAFE_LPCTSTR CExtTabPageContainerWnd::PageTextGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	VERIFY( ( const_cast < CExtTabPageContainerWnd * > ( this ) ) -> _CreateHelper() );
	return m_pWndTab->ItemTextGet( nIndex );
}

void CExtTabPageContainerWnd::PageTextSet(
	LONG nIndex,
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	VERIFY( _CreateHelper() );
	m_pWndTab->ItemTextSet( nIndex, sText, bUpdateTabWnd );
}

__EXT_MFC_SAFE_LPCTSTR CExtTabPageContainerWnd::PageTooltipTextGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	VERIFY( ( const_cast < CExtTabPageContainerWnd * > ( this ) ) -> _CreateHelper() );
	return m_pWndTab->ItemTooltipTextGet( nIndex );
}

void CExtTabPageContainerWnd::PageTooltipTextSet(
	LONG nIndex,
	__EXT_MFC_SAFE_LPCTSTR sTooltipText, // = NULL
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	VERIFY( _CreateHelper() );
	m_pWndTab->ItemTooltipTextSet( nIndex, sTooltipText, bUpdateTabWnd );
}

bool CExtTabPageContainerWnd::_CreateHelper()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return false;
	if( m_pWndTab == NULL )
		m_pWndTab = OnTabWndGetTabImpl();
	ASSERT_VALID( m_pWndTab );
	if(		m_pWndTab != NULL 
		&&	m_pWndTab->m_hWnd == NULL 
		)
	{
		if( !	m_pWndTab->Create(
					this,
					CRect( 0, 0, 0, 0 ),
					0x100,
					WS_CHILD|WS_VISIBLE, //|WS_TABSTOP,
					__ETWS_ORIENT_BOTTOM,
					NULL
					)
			)
			return false;
		_RepositionBarsImpl();
		HookSpyRegister( __EHSEF_KEYBOARD );
	}	
	return true;
}

void CExtTabPageContainerWnd::PreSubclassWindow() 
{
	CWnd::PreSubclassWindow();
	if( m_bDirectCreateCall )
		return;
//	if( ! _CreateHelper() )
//	{
//		ASSERT( FALSE );
//		::AfxThrowMemoryException();
//	}
	PostMessage( (WM_USER+200) );
}

void CExtTabPageContainerWnd::_ResetAllPageIdentifiersImpl()
{
	if( GetSafeTabWindow() == NULL )
		return;
LONG i, nCount = m_pWndTab->ItemGetCount();
	for( i = 0L; i < nCount; i++ )
		::__EXT_MFC_SetWindowLong(
			(HWND)m_pWndTab->ItemLParamGet(i),
			GWL_ID,
			0x101 + i
			);
}

void CExtTabPageContainerWnd::_RepositionBarsImpl()
{
	if( GetSafeTabWindow() == NULL )
		return;
LONG nPos = 0L, nAdjustPos = -1L;
	if( m_pWndTab->GetSafeHwnd() != NULL )
	{
		nPos = m_pWndTab->SelectionGet();
		if( nPos < 0L )
			nPos = 0L;
		else
			nAdjustPos = nPos;
	} // if( m_pWndTab->GetSafeHwnd() != NULL )
	CWnd::RepositionBars( 0, 0xFFFF, 0x101 + nPos );
	if( nAdjustPos >= 0L )
	{
		HWND hWnd = (HWND) m_pWndTab->ItemLParamGet( nAdjustPos );
		if(		hWnd != NULL
			&&	::IsWindow( hWnd )
			&&	( ::__EXT_MFC_GetWindowLong( hWnd, GWL_STYLE ) & WS_VISIBLE ) == 0
			)
			::ShowWindow( hWnd, SW_SHOW );
	} // if( nAdjustPos >= 0 )
}

void CExtTabPageContainerWnd::_RealignAllImpl()
{
	if( m_pWndTab->GetSafeHwnd() == NULL )
		return;
	LONG nCount = m_pWndTab->ItemGetCount();
	if( nCount == 0L )
		return;
	LONG nSel = m_pWndTab->SelectionGet();
	if( nSel < 0L )
		return;
	_RepositionBarsImpl();
	CRect rcAlign;
	CWnd * pWnd = GetDlgItem( 0x101 + nSel );
	if( pWnd )
	{
		pWnd->GetWindowRect( &rcAlign );
		ScreenToClient( &rcAlign );
	}
	LONG nPos = 0L;
	for( nPos = 0L; nPos < nCount; nPos++ )
	{
		if( nPos == nSel )
			continue;
		CWnd * pWnd = GetDlgItem( 0x101 + nPos );
		if( pWnd )
		{
			CRect rcItem;
			pWnd->GetWindowRect( &rcItem );
			ScreenToClient( &rcItem );
			if( rcItem == rcAlign )
				continue;
			pWnd->MoveWindow( &rcAlign );
		}
	}
}

bool CExtTabPageContainerWnd::OnHookSpyKeyMsg(
	MSG * pMSG
	)
{
	ASSERT( pMSG != NULL );
	if(		m_hWnd == NULL 
		||	( ! ::IsWindow( m_hWnd ) )
		||	( GetStyle() & WS_VISIBLE ) == 0
		)
		return CExtHookSpy::OnHookSpyKeyMsg( pMSG );
CWnd * pWnd = GetFocus();	
	if(		pWnd != NULL 
		&&	IsChild( pWnd )
		)
	{
		if(		pMSG->message == WM_KEYDOWN
			&&	(
					(	m_bHandlePriorNextKeyEvents
					&&	(	pMSG->wParam == VK_PRIOR || pMSG->wParam == VK_NEXT	)
					&&	CExtPopupMenuWnd::IsKeyPressed( VK_CONTROL )
					&&	(! CExtPopupMenuWnd::IsKeyPressed( VK_SHIFT ) )
					&&	(! CExtPopupMenuWnd::IsKeyPressed( VK_MENU ) )
					)
				||
					(	m_bHandleTabKeyEvents
					&&	(	pMSG->wParam == VK_TAB	)
					&&	CExtPopupMenuWnd::IsKeyPressed( VK_CONTROL )
					&&	(! CExtPopupMenuWnd::IsKeyPressed( VK_MENU ) )
					)
				)
			)
		{
			if(	pMSG->wParam == VK_PRIOR || pMSG->wParam == VK_NEXT	)
				PagesNavigate( ( pMSG->wParam == VK_NEXT ) ? TRUE : FALSE );
			else
				PagesNavigate( (! CExtPopupMenuWnd::IsKeyPressed( VK_SHIFT ) ) ? TRUE : FALSE );
			return true;
		}

		if(		WM_KEYFIRST <= pMSG->message
			&&	pMSG->message <= WM_KEYLAST
			&&	m_bAllowAccelBasedPageSelection
			&&	( ! CExtPopupMenuWnd::IsKeyPressed( VK_SHIFT ) )
			&&	( ! CExtPopupMenuWnd::IsKeyPressed( VK_CONTROL ) )
			&&	CExtPopupMenuWnd::IsKeyPressed( VK_MENU )
			)
		{
			CExtTabWnd * pWndTabs = GetSafeTabWindow();
			if( pWndTabs->GetSafeHwnd() != NULL )
			{
				UINT nChar = UINT( pMSG->wParam );
				ASSERT_VALID( pWndTabs );
				BYTE lpKeyState[256];
				::GetKeyboardState( lpKeyState );
				UINT wScanCode = ::MapVirtualKey( nChar, 0 );
				HKL hKeyboardLayout =
					::GetKeyboardLayout(
						( ::AfxGetThread() ) -> m_nThreadID
						);
#if (defined _UNICODE)
				TCHAR szChar[2] = { _T('\0'), _T('\0') };
				::ToUnicodeEx( nChar, wScanCode, lpKeyState, szChar, 1, 1, hKeyboardLayout );
				WORD nMapped = WORD( szChar[0] );
#else
				WORD nMapped = 0;
				::ToAsciiEx( nChar, wScanCode, lpKeyState, &nMapped, 1, hKeyboardLayout );
				//TCHAR szChar[2] = { (TCHAR)nMapped, _T('\0') };
#endif
				LONG nPrevIdx = PageSelectionGet();
				LONG nNextIdx =
					pWndTabs->ItemFindByAccessChar(
						(TCHAR)nMapped,
						nPrevIdx
						);
				if( nNextIdx >= 0 && nNextIdx != nPrevIdx )
				{
					PageSelectionSet( nNextIdx );
					return true;
				}
			} // if( pWndTabs->GetSafeHwnd() != NULL )
		} // if( WM_KEYFIRST <= pMSG->message ...
	}
	
	return CExtHookSpy::OnHookSpyKeyMsg( pMSG );
}

BOOL CExtTabPageContainerWnd::PagesNavigate(BOOL bNext)
{
	if( GetSafeTabWindow() == NULL )
		return FALSE;
LONG nCount = ( m_pWndTab->GetSafeHwnd() != NULL ) ? m_pWndTab->ItemGetCount() : 0L;
	if( nCount > 1L )
	{
		HWND hWndFocus = ::GetFocus();
		if( hWndFocus != NULL && ::IsChild(m_hWnd,hWndFocus) )
		{
			LONG nCurSel = m_pWndTab->SelectionGet();
			CExtTabWnd::TAB_ITEM_INFO * pTII = NULL;
			if( bNext )
			{
				LONG nCurSelPrev = nCurSel;
				do 
				{
					nCurSel ++;
					if( nCurSel >= nCount )
						nCurSel = 0;
					pTII = m_pWndTab->ItemGet( nCurSel );
					ASSERT( pTII != NULL );
					ASSERT_VALID( pTII );
				} 
				while( (!pTII->VisibleGet() || !pTII->EnabledGet()) && nCurSelPrev != nCurSel );
			}
			else
			{
				LONG nCurSelPrev = nCurSel;
				do 
				{
					nCurSel --;
					if( nCurSel < 0L )
				nCurSel = nCount - 1L;
					pTII = m_pWndTab->ItemGet( nCurSel );
					ASSERT( pTII != NULL );
					ASSERT_VALID( pTII );
				} 
				while( (!pTII->VisibleGet() || !pTII->EnabledGet()) && nCurSelPrev != nCurSel );
			}
			m_pWndTab->SelectionSet( nCurSel, true, true );
			return TRUE;
		} // if( hWndFocus != NULL && ::IsChild(m_hWnd,hWndFocus) )
	}
	return FALSE;
}

bool CExtTabPageContainerWnd::OnTabWndClickedButton(
	LONG nHitTest,
	bool bButtonPressed,
	INT nMouseButton, // MK_... values
	UINT nMouseEventFlags
	)
{
	ASSERT_VALID( this );
	bButtonPressed;
	nMouseButton;
	nMouseEventFlags;
	nHitTest;
#if (!defined __EXT_MFC_NO_DYNAMIC_BAR_SITE)
	if(		(GetStyle()&(WS_CHILD|WS_VISIBLE)) == (WS_CHILD|WS_VISIBLE)
		&&	GetDlgCtrlID() == AFX_IDW_PANE_FIRST
		)
	{
		CFrameWnd * pFrame =
			DYNAMIC_DOWNCAST(
				CFrameWnd,
				GetParent()
				);
		if(		pFrame != NULL
			&&	(! pFrame->IsKindOf(RUNTIME_CLASS(CMDIFrameWnd)) )
			&&	(! pFrame->IsKindOf(RUNTIME_CLASS(CMiniFrameWnd)) )
			)
		{ // if SDI frame
			CExtDynamicBarSite * pDBS = 
				CExtDynamicBarSite::FindBarSite( this );
			if(		pDBS != NULL
				&&	pDBS->GetTabPageContainer() == this
				)
				return
					pDBS->OnTabPageContainerClickedButton(
						nHitTest,
						bButtonPressed,
						nMouseButton,
						nMouseEventFlags
						);
		} // if SDI frame
	} // if( (GetStyle()&(WS_CHILD|WS_VISIBLE)) == (WS_CHILD|WS_VISIBLE) ...
#endif // (!defined __EXT_MFC_NO_DYNAMIC_BAR_SITE)
	return true;
}

void CExtTabPageContainerWnd::OnTabWndClickedItemCloseButton(
	LONG nItemIndex
	)
{
	ASSERT_VALID( this );
	nItemIndex;
#if (!defined __EXT_MFC_NO_DYNAMIC_BAR_SITE)
	if(		(GetStyle()&(WS_CHILD|WS_VISIBLE)) == (WS_CHILD|WS_VISIBLE)
		&&	GetDlgCtrlID() == AFX_IDW_PANE_FIRST
		)
	{
		CFrameWnd * pFrame =
			DYNAMIC_DOWNCAST(
				CFrameWnd,
				GetParent()
				);
		if(		pFrame != NULL
			&&	(! pFrame->IsKindOf(RUNTIME_CLASS(CMDIFrameWnd)) )
			&&	(! pFrame->IsKindOf(RUNTIME_CLASS(CMiniFrameWnd)) )
			)
		{ // if SDI frame
			CExtDynamicBarSite * pDBS = 
				CExtDynamicBarSite::FindBarSite( this );
			if(		pDBS != NULL
				&&	pDBS->GetTabPageContainer() == this
				)
			{
				HWND hWndChild = PageHwndGetSafe( nItemIndex );
				if( hWndChild != NULL )
				{
					CExtDynamicControlBar * pBar =
						pDBS->BarFindByChildHWND( hWndChild );
					if( pBar != NULL )
					{
						if( PageSelectionGet() != nItemIndex )
							PageSelectionSet( nItemIndex );
						pDBS->OnTabPageContainerClickedButton(
							__ETWH_BUTTON_CLOSE,
							false,
							MK_LBUTTON,
							0
							);
						return;
					}
				}
			}
		} // if SDI frame
	} // if( (GetStyle()&(WS_CHILD|WS_VISIBLE)) == (WS_CHILD|WS_VISIBLE) ...
#endif // (!defined __EXT_MFC_NO_DYNAMIC_BAR_SITE)
}

void CExtTabPageContainerWnd::OnTabWndItemPosChanged(
	LONG nItemIndex,
	LONG nItemNewIndex
	)
{
	ASSERT_VALID( this );
	nItemIndex;
	nItemNewIndex;
	_ResetAllPageIdentifiersImpl();
}

bool CExtTabPageContainerWnd::OnTabWndSelectionChange(
	LONG nOldItemIndex,
	LONG nNewItemIndex,
	bool bPreSelectionTest
	)
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	if(		(! bPreSelectionTest ) 
		&&	nNewItemIndex >= 0 
		&&	m_pWndTab->ItemGetCount() > 0 
		)
	{
		CWnd::RepositionBars( 0, 0xFFFF, 0x101 + nNewItemIndex );
		HWND hWndNew = NULL;
		if( nNewItemIndex >= 0 )
		{
			hWndNew = (HWND) m_pWndTab->ItemLParamGet( nNewItemIndex );
			if( hWndNew != NULL && ::IsWindow( hWndNew ) )
				::ShowWindow( hWndNew, SW_SHOW );
			else
				hWndNew = NULL;
		}
		if( nOldItemIndex >= 0 )
		{
			HWND hWndOld = (HWND) m_pWndTab->ItemLParamGet( nOldItemIndex );
			if( hWndOld != NULL && ::IsWindow( hWndOld ) )
				::ShowWindow( hWndOld, SW_HIDE );
		}
		HWND hWndFocus = ::GetFocus();
		if( hWndFocus != NULL && hWndNew != NULL )
			::SetFocus( hWndNew );
	}
	if( ! bPreSelectionTest )
		_RealignAllImpl();
	return true;
}

void CExtTabPageContainerWnd::OnTabWndSyncVisibility()
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return;
LONG nItemCount = m_pWndTab->ItemGetCount();
LONG nItemVisibleCount = m_pWndTab->ItemGetVisibleCount();
DWORD dwWndStyle = m_pWndTab->GetStyle();
	if(		nItemCount > 0 
		&&	nItemVisibleCount > 0
		&&	m_bTabWndVisible
		)
	{
		if( (dwWndStyle & WS_VISIBLE) == 0 )
			m_pWndTab->SetWindowPos(
				NULL, 0, 0, 0, 0,
				SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_FRAMECHANGED|SWP_SHOWWINDOW
				);
	}
	else
	{
		if( (dwWndStyle & WS_VISIBLE) != 0 )
			m_pWndTab->SetWindowPos(
				NULL, 0, 0, 0, 0,
				SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_HIDEWINDOW
				);
	}
}

void CExtTabPageContainerWnd::OnSetFocus(CWnd* pOldWnd) 
{
	CWnd::OnSetFocus(pOldWnd);
	if( GetSafeTabWindow() == NULL )
		return; // destruction mode entered
LONG nIndex = PageSelectionGet();
	if( nIndex != -1 )
	{
		HWND hWnd = (HWND)m_pWndTab->ItemLParamGet( PageSelectionGet() );
		::SetFocus( hWnd );
	}
}

DWORD CExtTabPageContainerWnd::ModifyTabStyle( DWORD dwTabWndStyle, bool bSet )
{
	ASSERT_VALID( this );
	VERIFY( _CreateHelper() );
	if( GetSafeTabWindow() == NULL )
		return 0;
DWORD dwTabWndStyleRemove = dwTabWndStyle;
DWORD dwTabWndStyleAdd = dwTabWndStyle;
	if( bSet )	
		dwTabWndStyleRemove = 0;
	else		
		dwTabWndStyleAdd = 0;
	DWORD dwRet = 
		m_pWndTab->ModifyTabWndStyle( dwTabWndStyleRemove, dwTabWndStyleAdd, true );
	_RepositionBarsImpl();
	return dwRet;
}

DWORD CExtTabPageContainerWnd::ModifyTabStyleEx( DWORD dwTabWndStyleEx, bool bSet )
{
	ASSERT_VALID( this );
	VERIFY( _CreateHelper() );
	if( GetSafeTabWindow() == NULL )
		return 0;
DWORD dwTabWndStyleRemove = dwTabWndStyleEx;
DWORD dwTabWndStyleAdd = dwTabWndStyleEx;
	if( bSet )	
		dwTabWndStyleRemove = 0;
	else		
		dwTabWndStyleAdd = 0;
	DWORD dwRet = 
		m_pWndTab->ModifyTabWndStyleEx( dwTabWndStyleRemove, dwTabWndStyleAdd, true );
	_RepositionBarsImpl();
	return dwRet;
}

LONG CExtTabPageContainerWnd::PageFindByHWND(
	HWND hWnd,
	LONG nIndexStartSearch, // = -1
	bool bIncludeVisible, // = true
	bool bIncludeInvisible // = false
	) const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return -1L;
	return m_pWndTab->ItemFindByLParam( (LPARAM)hWnd, nIndexStartSearch, bIncludeVisible, bIncludeInvisible );
}

bool CExtTabPageContainerWnd::PageEnsureVisible(
	INT nItemIndex,
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	return m_pWndTab->ItemEnsureVisible( nItemIndex, bUpdateTabWnd );
}

bool CExtTabPageContainerWnd::PageEnabledGet(
	INT nItemIndex
	) const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
CExtTabWnd::TAB_ITEM_INFO * pTII = m_pWndTab->ItemGet( nItemIndex );
	ASSERT( pTII != NULL );
	ASSERT_VALID( pTII );
	return pTII->EnabledGet();
}

bool CExtTabPageContainerWnd::PageEnabledSet( 
	INT nItemIndex,
	bool bEnable // = true 
	)
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
CExtTabWnd::TAB_ITEM_INFO * pTII = m_pWndTab->ItemGet( nItemIndex );
	ASSERT( pTII != NULL );
	ASSERT_VALID( pTII );
bool bRet = 
		pTII->EnabledSet( bEnable );
HWND hWnd = PageHwndGetSafe( nItemIndex );
	if( hWnd != NULL )
	{
		if( bEnable && (!::IsWindowEnabled(hWnd)) )
			::EnableWindow( hWnd, TRUE );
		if( (!bEnable) && ::IsWindowEnabled(hWnd) )
			::EnableWindow( hWnd, FALSE );
	}
	m_pWndTab->Invalidate();
	m_pWndTab->UpdateWindow();
	return bRet;
}

bool CExtTabPageContainerWnd::PageVisibleGet( 
	INT nItemIndex 
	) const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
CExtTabWnd::TAB_ITEM_INFO * pTII = m_pWndTab->ItemGet( nItemIndex );
	ASSERT( pTII != NULL );
	ASSERT_VALID( pTII );
	return pTII->VisibleGet();
}

bool CExtTabPageContainerWnd::PageVisibleSet( 
	INT nItemIndex, 
	bool bVisible // = true 
	)
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
CExtTabWnd::TAB_ITEM_INFO * pTII = m_pWndTab->ItemGet( nItemIndex );
	ASSERT( pTII != NULL );
	ASSERT_VALID( pTII );
LONG nSelIndex = PageSelectionGet();
bool bRet = pTII->VisibleSet( bVisible );
	if( ! bVisible ) 
	{
		HWND hWnd = PageHwndGetSafe( nItemIndex );
		ASSERT( hWnd != NULL && ::IsWindow( hWnd ) );
		if( hWnd != NULL )
			::ShowWindow( hWnd, SW_HIDE );
		if( nSelIndex == nItemIndex )
			PageSelectionSet( -1 );
	}
	_RepositionBarsImpl();
	return bRet;	
}

void CExtTabPageContainerWnd::ShowTabStrip( 
	bool bShow // = true 
	)
{
	ASSERT_VALID( this );
	m_bTabWndVisible = bShow;
	OnTabWndSyncVisibility();
}

bool CExtTabPageContainerWnd::IsTabStripVisible() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return m_pWndTab->IsWindowVisible() ? true : false;
}

DWORD CExtTabPageContainerWnd::CenterTextSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_CENTERED_TEXT, bSet);
}

bool CExtTabPageContainerWnd::CenterTextGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_CENTERED_TEXT) ? true : false;
}

DWORD CExtTabPageContainerWnd::AutoHideScrollSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_AUTOHIDE_SCROLL, bSet);
}

bool CExtTabPageContainerWnd::AutoHideScrollGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_AUTOHIDE_SCROLL) ? true : false;
}

DWORD CExtTabPageContainerWnd::ShowBtnScrollHomeSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_SHOW_BTN_SCROLL_HOME, bSet);
}

bool CExtTabPageContainerWnd::ShowBtnScrollHomeGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_SHOW_BTN_SCROLL_HOME) ? true : false;
}

DWORD CExtTabPageContainerWnd::ShowBtnScrollEndSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_SHOW_BTN_SCROLL_END, bSet);
}

bool CExtTabPageContainerWnd::ShowBtnScrollEndGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_SHOW_BTN_SCROLL_END) ? true : false;
}

DWORD CExtTabPageContainerWnd::ShowBtnCloseSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_SHOW_BTN_CLOSE, bSet);
}

bool CExtTabPageContainerWnd::ShowBtnCloseGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_SHOW_BTN_CLOSE) ? true : false;
}

DWORD CExtTabPageContainerWnd::EnabledBtnCloseSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_ENABLED_BTN_CLOSE, bSet);
}

bool CExtTabPageContainerWnd::EnabledBtnCloseGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_ENABLED_BTN_CLOSE) ? true : false;
}

DWORD CExtTabPageContainerWnd::ShowBtnHelpSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_SHOW_BTN_HELP, bSet);
}

bool CExtTabPageContainerWnd::ShowBtnHelpGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_SHOW_BTN_HELP) ? true : false;
}

DWORD CExtTabPageContainerWnd::ShowBtnTabListSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_SHOW_BTN_TAB_LIST, bSet);
}

bool CExtTabPageContainerWnd::ShowBtnTabListGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_SHOW_BTN_TAB_LIST) ? true : false;
}

DWORD CExtTabPageContainerWnd::EnabledBtnTabListSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_ENABLED_BTN_TAB_LIST, bSet);
}

bool CExtTabPageContainerWnd::EnabledBtnTabListGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_ENABLED_BTN_TAB_LIST) ? true : false;
}

DWORD CExtTabPageContainerWnd::EnabledBtnHelpSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_ENABLED_BTN_HELP, bSet);
}

bool CExtTabPageContainerWnd::EnabledBtnHelpGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_ENABLED_BTN_HELP) ? true : false;
}

DWORD CExtTabPageContainerWnd::EqualWidthsSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_EQUAL_WIDTHS, bSet);
}

bool CExtTabPageContainerWnd::EqualWidthsGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_EQUAL_WIDTHS) ? true : false;
}

DWORD CExtTabPageContainerWnd::FullWidthSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_FULL_WIDTH, bSet);
}

bool CExtTabPageContainerWnd::FullWidthGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_FULL_WIDTH) ? true : false;
}

DWORD CExtTabPageContainerWnd::HoverFocusSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_HOVER_FOCUS, bSet);
}

bool CExtTabPageContainerWnd::HoverFocusGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_HOVER_FOCUS) ? true : false;
}

DWORD CExtTabPageContainerWnd::ItemDraggingSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_ITEM_DRAGGING, bSet);
}

bool CExtTabPageContainerWnd::ItemDraggingGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_ITEM_DRAGGING) ? true : false;
}

DWORD CExtTabPageContainerWnd::InvertVertFontSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_INVERT_VERT_FONT, bSet);
}

bool CExtTabPageContainerWnd::InvertVertFontGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return (m_pWndTab->GetTabWndStyle() & __ETWS_INVERT_VERT_FONT) ? true : false;
}

DWORD CExtTabPageContainerWnd::OrientationGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return __ETWS_ORIENT_TOP;
	return m_pWndTab->OrientationGet();
}

DWORD CExtTabPageContainerWnd::OrientationSet( DWORD dwOrientation )
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return __ETWS_ORIENT_TOP;
DWORD dwRet = m_pWndTab->OrientationSet(dwOrientation,true);
	_RepositionBarsImpl();
	if( m_pWndTab->ItemGetCount() > 0L )
	{
		LONG i = m_pWndTab->SelectionGet();
		if( i >= 0L )
			m_pWndTab->ItemEnsureVisible( i, true );
	}
	return dwRet;		
}

bool CExtTabPageContainerWnd::OrientationIsHorizontal() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return m_pWndTab->OrientationIsHorizontal();
}

bool CExtTabPageContainerWnd::OrientationIsVertical() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return true;
	return m_pWndTab->OrientationIsVertical();
}

bool CExtTabPageContainerWnd::OrientationIsTopLeft() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return true;
	return m_pWndTab->OrientationIsTopLeft();
}

bool CExtTabPageContainerWnd::SelectionBoldGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return m_pWndTab->SelectionBoldGet();
}

void CExtTabPageContainerWnd::SelectionBoldSet( 
	bool bBold // = true 
	)
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return;
	m_pWndTab->SelectionBoldSet( bBold );
	_RepositionBarsImpl();
}

DWORD CExtTabPageContainerWnd::CloseOnTabsSet( 
	bool bSet, // = true
	bool bSelectedOnly // = false
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyleEx( __ETWS_EX_CLOSE_ON_TABS | (bSelectedOnly ? __ETWS_EX_CLOSE_ON_SELECTED_ONLY : 0), bSet );
}

bool CExtTabPageContainerWnd::CloseOnTabsGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return ( m_pWndTab->GetTabWndStyleEx() & __ETWS_EX_CLOSE_ON_TABS ) ? true : false;
}

bool CExtTabPageContainerWnd::CloseOnTabsSelectedOnlyGet() const
{
	ASSERT_VALID( this );
	if( GetSafeTabWindow() == NULL )
		return false;
	return ( m_pWndTab->GetTabWndStyleEx() & __ETWS_EX_CLOSE_ON_SELECTED_ONLY ) ? true : false;
}

void AFXAPI __EXT_MFC_DDX_TabPageContainerIndex( CDataExchange * pDX, INT nIDC, LONG & index )
{
HWND hWndCtrl = pDX->PrepareCtrl( nIDC );
	ASSERT( hWndCtrl != NULL );
	ASSERT( ::IsWindow( hWndCtrl ) );
CExtTabPageContainerWnd * pWnd = 
		DYNAMIC_DOWNCAST( CExtTabPageContainerWnd, CWnd::FromHandle( hWndCtrl ) );
	if( pWnd != NULL )
	{
		ASSERT_VALID( pWnd );
		if( pDX->m_bSaveAndValidate )
			index = pWnd->PageSelectionGet();
		else
			pWnd->PageSelectionSet( index );
	}
}

#if (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_FLAT_CTRL)

/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerFlatWnd
/////////////////////////////////////////////////////////////////////////////

CExtTabPageContainerFlatWnd::CExtTabPageContainerFlatWnd()
	: m_bRenderConsistentPageBackground( false )
{
}

CExtTabPageContainerFlatWnd::~CExtTabPageContainerFlatWnd()
{
}

IMPLEMENT_DYNCREATE(CExtTabPageContainerFlatWnd, CExtTabPageContainerWnd)

BEGIN_MESSAGE_MAP(CExtTabPageContainerFlatWnd, CExtTabPageContainerWnd)
	//{{AFX_MSG_MAP(CExtTabPageContainerFlatWnd)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerFlatWnd message handlers

BOOL CExtTabPageContainerFlatWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd,
	UINT nDlgCtrlID,
	DWORD dwWindowStyle,
	CCreateContext * pContext
	)
{
	BOOL bRet = CExtTabPageContainerWnd::Create(
		pParentWnd,
		rcWnd,
		nDlgCtrlID,
		dwWindowStyle,
		pContext);

	CenterTextSet();
	AutoHideScrollSet();

	return bRet;
}

bool CExtTabPageContainerFlatWnd::ItemsHasInclineGet( 
	bool bBefore 
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	return ((CExtTabFlatWnd*)m_pWndTab)->ItemsHasInclineGet(bBefore);
}

bool CExtTabPageContainerFlatWnd::ItemsHasInclineBeforeGet() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	return ((CExtTabFlatWnd*)m_pWndTab)->ItemsHasInclineBeforeGet();
}

bool CExtTabPageContainerFlatWnd::ItemsHasInclineAfterGet() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	return ((CExtTabFlatWnd*)m_pWndTab)->ItemsHasInclineAfterGet();
}

void CExtTabPageContainerFlatWnd::ItemsHasInclineSet( 
	bool bBefore, 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	((CExtTabFlatWnd*)m_pWndTab)->ItemsHasInclineSet( bBefore, bSet );
}

void CExtTabPageContainerFlatWnd::ItemsHasInclineBeforeSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	((CExtTabFlatWnd*)m_pWndTab)->ItemsHasInclineBeforeSet( bSet);
}

void CExtTabPageContainerFlatWnd::ItemsHasInclineAfterSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	((CExtTabFlatWnd*)m_pWndTab)->ItemsHasInclineAfterSet( bSet );
}

LRESULT CExtTabPageContainerFlatWnd::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	if(		message == CExtPaintManager::g_nMsgPaintInheritedBackground
		&&	m_bRenderConsistentPageBackground
		)
	{
		if( PageGetCount() == 0L )
			return 0L;
		LONG nSelIndex = PageSelectionGet();
		if( nSelIndex < 0L )
			return 0L;
		CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA * pPIBD =
			CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA::FromWPARAM( wParam );
		ASSERT( pPIBD != NULL );
		if(		pPIBD->m_pWnd == this
			||	pPIBD->m_pWnd == GetSafeTabWindow()
			)
			return 0L;
		pPIBD->m_bBackgroundDrawn = true;
		CExtTabFlatWnd * pWndTab =
			STATIC_DOWNCAST( CExtTabFlatWnd, GetSafeTabWindow() );
		ASSERT_VALID( pWndTab );
		bool bEnabled = 
			pWndTab->ItemEnabledGet( nSelIndex );
		COLORREF clrLight = COLORREF(-1L);
		COLORREF clrShadow = COLORREF(-1L);
		COLORREF clrDkShadow = COLORREF(-1L);
		COLORREF clrTabBk = COLORREF(-1L);
		COLORREF clrText = COLORREF(-1L);
		pWndTab->OnFlatTabWndGetItemColors(
			nSelIndex,
			true,
			false,
			bEnabled,
			clrLight,
			clrShadow,
			clrDkShadow, 
			clrTabBk, 
			clrText
			);
		CRect rc = pPIBD->GetRenderingRect();
		pPIBD->m_dc.FillSolidRect( &rc, clrTabBk );
		return 0L;
	}
	return CExtTabPageContainerWnd::WindowProc( message, wParam, lParam );
}

#endif // (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_FLAT_CTRL)

#if (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_BUTTONS_CTRL)

/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerButtonsWnd
/////////////////////////////////////////////////////////////////////////////

CExtTabPageContainerButtonsWnd::CExtTabPageContainerButtonsWnd()
{
}

CExtTabPageContainerButtonsWnd::~CExtTabPageContainerButtonsWnd()
{
}

IMPLEMENT_DYNCREATE(CExtTabPageContainerButtonsWnd, CExtTabPageContainerFlatWnd)

BEGIN_MESSAGE_MAP(CExtTabPageContainerButtonsWnd, CExtTabPageContainerFlatWnd)
	//{{AFX_MSG_MAP(CExtTabPageContainerButtonsWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#endif // (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_BUTTONS_CTRL)

/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerOneNoteWnd
/////////////////////////////////////////////////////////////////////////////

#if (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_ONENOTE_CTRL)

CExtTabPageContainerOneNoteWnd::CExtTabPageContainerOneNoteWnd()
	: m_bRenderConsistentPageBackground( false )
	, m_bRenderGradientInheritance( false )
{
}

CExtTabPageContainerOneNoteWnd::~CExtTabPageContainerOneNoteWnd()
{
}

BOOL CExtTabPageContainerOneNoteWnd::PageInsert(
	HWND hWnd,
	__EXT_MFC_SAFE_LPCTSTR sItemText, // = NULL`
	HICON hIcon, // = NULL
	bool bCopyIcon, // = true
	LONG nPos, // = -1L // append
	bool bSelect, // = false
	COLORREF clrBkLight, // = (COLORREF)(-1L)
	COLORREF clrBkDark // = (COLORREF)(-1L)
	)
{
	ASSERT_VALID( this );
CExtCmdIcon pageIcon;
	pageIcon.AssignFromHICON( hIcon, bCopyIcon );
	return
		PageInsert(
			hWnd,
			pageIcon,
			sItemText,
			nPos,
			bSelect,
			clrBkLight,
			clrBkDark
			);
}

BOOL CExtTabPageContainerOneNoteWnd::PageInsert(
	HWND hWnd,
	const CExtCmdIcon & pageIcon,
	__EXT_MFC_SAFE_LPCTSTR sItemText, // = NULL`
	LONG nPos, // = -1L // append
	bool bSelect, // = false
	COLORREF clrBkLight, // = (COLORREF)(-1L)
	COLORREF clrBkDark // = (COLORREF)(-1L)
	)
{
	ASSERT_VALID( this );
	if(		hWnd   == NULL || (! ::IsWindow(hWnd)   )
		||	m_hWnd == NULL || (! ::IsWindow(m_hWnd) )
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	VERIFY( _CreateHelper() );
CExtTabOneNoteWnd * pWndTab = DYNAMIC_DOWNCAST( CExtTabOneNoteWnd, m_pWndTab );
	ASSERT( pWndTab != NULL );
	LONG nCount = pWndTab->ItemGetCount();
	if( nCount == 0L )
		bSelect = true;
	if( nPos < 0L || nPos > nCount )
		nPos = nCount; // append
	if( ::GetParent(hWnd) != m_hWnd )
		::SetParent( hWnd, m_hWnd );
	if( bSelect )
	{
		if( nCount > 0L )
		{
			LONG i = pWndTab->SelectionGet();
			if( i >= 0L )
				::ShowWindow(
					(HWND)pWndTab->ItemLParamGet(i),
					SW_HIDE
					);
		}
		if( ( ::__EXT_MFC_GetWindowLong( hWnd, GWL_STYLE ) & WS_VISIBLE ) == 0 )
			::ShowWindow( hWnd, SW_SHOW );
	} // if( bSelect )
	else
	{
		if( ( ::__EXT_MFC_GetWindowLong( hWnd, GWL_STYLE ) & WS_VISIBLE ) != 0 )
			::ShowWindow( hWnd, SW_HIDE );
	} // else from if( bSelect )
	
	pWndTab->ItemInsert(
		sItemText,
		pageIcon,
		0,
		(LONG)nPos,
		(LPARAM)hWnd,
		clrBkLight,
		clrBkDark,
		false
		);
	
	nCount++;
	for( LONG i = nPos; i < nCount; i++ )
		::__EXT_MFC_SetWindowLong(
			(HWND)m_pWndTab->ItemLParamGet(i),
			GWL_ID,
			0x101 + i
			);
	if( bSelect )
		m_pWndTab->SelectionSet( nPos, true, true );
	else
		m_pWndTab->UpdateTabWnd( true );

	_RepositionBarsImpl();
				
	return true;
}

BOOL CExtTabPageContainerOneNoteWnd::PageInsert(
	CWnd * pWnd,
	__EXT_MFC_SAFE_LPCTSTR sItemText, // = NULL
	HICON hIcon, // = NULL,
	bool bCopyIcon, // = true
	LONG nPos, //= -1L, // append
	bool bSelect, // = false
	COLORREF clrBkLight, // = (COLORREF)(-1L)
	COLORREF clrBkDark // = (COLORREF)(-1L)
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pWnd );
	return
		PageInsert(
			pWnd->GetSafeHwnd(),
			sItemText,
			hIcon,
			bCopyIcon,
			nPos,
			bSelect,
			clrBkLight,
			clrBkDark
			);
}

BOOL CExtTabPageContainerOneNoteWnd::PageInsert(
	CWnd * pWnd,
		const CExtCmdIcon & pageIcon,
	__EXT_MFC_SAFE_LPCTSTR sItemText, // = NULL
	LONG nPos, //= -1L, // append
	bool bSelect, // = false
	COLORREF clrBkLight, // = (COLORREF)(-1L)
	COLORREF clrBkDark // = (COLORREF)(-1L)
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pWnd );
	return
		PageInsert(
			pWnd->GetSafeHwnd(),
			pageIcon,
			sItemText,
			nPos,
			bSelect,
			clrBkLight,
			clrBkDark
			);
}

IMPLEMENT_DYNCREATE(CExtTabPageContainerOneNoteWnd, CExtTabPageContainerWnd)

LRESULT CExtTabPageContainerOneNoteWnd::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	if(		message == CExtPaintManager::g_nMsgPaintInheritedBackground
		&&	m_bRenderConsistentPageBackground
		)
	{
		if( PageGetCount() == 0L )
			return 0;
		LONG nSelIndex = PageSelectionGet();
		if( nSelIndex < 0L )
			return 0;
		CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA * pPIBD =
			CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA::FromWPARAM( wParam );
		ASSERT( pPIBD != NULL );
		if(		pPIBD->m_pWnd == this
			||	pPIBD->m_pWnd == GetSafeTabWindow()
			)
			return 0L;
		pPIBD->m_bBackgroundDrawn = true;
		CExtTabOneNoteWnd * pWndTab =
			STATIC_DOWNCAST( CExtTabOneNoteWnd, GetSafeTabWindow() );
		ASSERT_VALID( pWndTab );
		bool bEnabled = 
			pWndTab->ItemEnabledGet( nSelIndex );
		if( m_bRenderGradientInheritance )
		{
			COLORREF clrLight, clrDark;
			pWndTab->OnTabWndQueryItemColors(
				nSelIndex,
				true,
				false,
				bEnabled,
				NULL,
				NULL,
				&clrLight,
				&clrDark
				);
			DWORD dwOrientaton = OrientationGet();
			CRect rc;
			GetClientRect( &rc );

			if(		dwOrientaton == __ETWS_ORIENT_TOP
				||	dwOrientaton == __ETWS_ORIENT_LEFT
				) // TO FIX: condition
				RepositionBars( 0, 0xFFFF, 0x101 + nSelIndex, CWnd::reposQuery, &rc, &rc );
			
			ClientToScreen( &rc );
			if( pPIBD->m_bClientMapping )
				pPIBD->m_pWnd->ScreenToClient( &rc );
			else
			{
				CRect rcTarget;
				pPIBD->m_pWnd->GetWindowRect( &rcTarget );
				rc.OffsetRect( - rcTarget.TopLeft() );
			}
			COLORREF clrTmp;
			bool bHorz = false;
			switch( dwOrientaton)
			{
			case __ETWS_ORIENT_TOP:
				bHorz = true;
				clrTmp = clrDark;
				clrDark = clrLight;
				clrLight = clrTmp;
			break;
			case __ETWS_ORIENT_BOTTOM:
				bHorz = true;
			break;
			case __ETWS_ORIENT_LEFT:
			break;
			case __ETWS_ORIENT_RIGHT:
				clrTmp = clrDark;
				clrDark = clrLight;
				clrLight = clrTmp;
			break;
#ifdef _DEBUG
			default:
				ASSERT( FALSE );
			break;
#endif // _DEBUG
			} // switch( dwOrientaton)
			CExtPaintManager::stat_PaintGradientRect(
				pPIBD->m_dc,
				&rc,
				clrDark,
				clrLight,
				bHorz
				);
		} // if( m_bRenderGradientInheritance )
		else
		{
			CRect rc = pPIBD->GetRenderingRect();
			COLORREF clr;
			pWndTab->OnTabWndQueryItemColors(
				nSelIndex,
				true,
				false,
				bEnabled,
				NULL,
				NULL,
				NULL,
				&clr
				);
			pPIBD->m_dc.FillSolidRect( &rc, clr );
		} // else from if( m_bRenderGradientInheritance )
		return 0L;
	}
	return CExtTabPageContainerWnd::WindowProc( message, wParam, lParam );
}


#endif // (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_ONENOTE_CTRL)


/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerWhidbeyWnd
/////////////////////////////////////////////////////////////////////////////

#if (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_WHIDBEY_CTRL)

CExtTabPageContainerWhidbeyWnd::CExtTabPageContainerWhidbeyWnd()
	: m_bRenderConsistentPageBackground( false )
{
}

CExtTabPageContainerWhidbeyWnd::~CExtTabPageContainerWhidbeyWnd()
{
}

IMPLEMENT_DYNCREATE(CExtTabPageContainerWhidbeyWnd, CExtTabPageContainerWnd)

LRESULT CExtTabPageContainerWhidbeyWnd::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	if(		message == CExtPaintManager::g_nMsgPaintInheritedBackground
		&&	m_bRenderConsistentPageBackground
		)
	{
		if( PageGetCount() == 0L )
			return 0;
		LONG nSelIndex = PageSelectionGet();
		if( nSelIndex < 0L )
			return 0L;
		CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA * pPIBD =
			CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA::FromWPARAM( wParam );
		ASSERT( pPIBD != NULL );
		if(		pPIBD->m_pWnd == this
			||	pPIBD->m_pWnd == GetSafeTabWindow()
			)
			return 0;
		pPIBD->m_bBackgroundDrawn = true;
		CExtTabWhidbeyWnd * pWndTab =
			STATIC_DOWNCAST( CExtTabWhidbeyWnd, GetSafeTabWindow() );
		ASSERT_VALID( pWndTab );
		bool bEnabled = 
			pWndTab->ItemEnabledGet( nSelIndex );
		COLORREF clr = COLORREF( -1L );
		pWndTab->OnTabWndQueryItemColors(
			nSelIndex,
			true,
			false,
			bEnabled,
			NULL,
			NULL,
			&clr,
			NULL
			);
		CRect rc = pPIBD->GetRenderingRect();
		pPIBD->m_dc.FillSolidRect( &rc, clr );
		return 0L;
	}
	return CExtTabPageContainerWnd::WindowProc( message, wParam, lParam );
}

#endif // (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_WHIDBEY_CTRL)


#endif // (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_CTRL)
