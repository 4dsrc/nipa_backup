// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_TREEGRIDWND)

#if (!defined __EXTTREEGRIDWND_H)
	#include <ExtTreeGridWnd.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

IMPLEMENT_DYNCREATE( CExtTreeGridCellNode, CExtGridCell );

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtTreeGridCellNode

CExtTreeGridCellNode::CExtTreeGridCellNode(
	CExtGridDataProvider * pDataProvider // = NULL
	)
	: CExtGCE < CExtGridCell > ( pDataProvider )
	, m_pNodeParent( NULL )
	, m_pNodeNext( NULL )
	, m_pNodePrev( NULL )
	, m_nIndentPx( ULONG(-1L) )
	, m_nContentWeightAll( 0 )
	, m_nContentWeightExpanded( 0 )
	, m_bExpanded( false )
	, m_nOptIndex( 0 )
{
}

CExtTreeGridCellNode::~CExtTreeGridCellNode()
{
CExtGridDataProvider * pDP = DataProviderGet();
	if( pDP != NULL && pDP->m_pOuterDataProvider != NULL )
	{
		CExtTreeGridDataProvider * pTreeDP =
			DYNAMIC_DOWNCAST( CExtTreeGridDataProvider, pDP->m_pOuterDataProvider );
		if( pTreeDP != NULL )
			pTreeDP->OnNodeRemoved( this );
	} // if( pDP != NULL && pDP->m_pOuterDataProvider != NULL )
}

ULONG CExtTreeGridCellNode::_ContentWeight_Get(
	bool bExpandedOnly
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
	return (bExpandedOnly ? m_nContentWeightExpanded : m_nContentWeightAll);
}

void CExtTreeGridCellNode::_ContentWeight_Increment(
	ULONG nWeight,
	bool bExpandedOnly
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
	if( TreeNodeHiddenGet() )
		return;
	m_nContentWeightExpanded += nWeight;
	if( ! bExpandedOnly )
		m_nContentWeightAll += nWeight;
	__EXT_DEBUG_GRID_ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if(		pNodeParent != NULL
		&&	pNodeParent->TreeNodeIsExpanded()
		)
		pNodeParent->_ContentWeight_Increment(
			nWeight,
			bExpandedOnly
			);
}

void CExtTreeGridCellNode::_ContentWeight_Decrement(
	ULONG nWeight,
	bool bExpandedOnly
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	//__EXT_DEBUG_GRID_ASSERT( m_nContentWeightExpanded >= nWeight );
	__EXT_DEBUG_GRID_ASSERT( m_nContentWeightAll >= nWeight );
	__EXT_DEBUG_GRID_ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
	if( TreeNodeHiddenGet() )
		return;
	if( m_nContentWeightExpanded <= nWeight )
		m_nContentWeightExpanded = 0;
	else
		m_nContentWeightExpanded -= nWeight;
	if( ! bExpandedOnly )
		m_nContentWeightAll -= nWeight;
	__EXT_DEBUG_GRID_ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if(		pNodeParent != NULL
		&&	pNodeParent->TreeNodeIsExpanded()
		)
		pNodeParent->_ContentWeight_Decrement(
			nWeight,
			bExpandedOnly
			);
}

void CExtTreeGridCellNode::_ContentWeight_IncrementNonExpanded(
	ULONG nWeight
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
//	if( TreeNodeHiddenGet() )
//		return;
	m_nContentWeightAll += nWeight;
	__EXT_DEBUG_GRID_ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if( pNodeParent != NULL )
		pNodeParent->_ContentWeight_IncrementNonExpanded( nWeight );
}

void CExtTreeGridCellNode::_ContentWeight_DecrementNonExpanded(
	ULONG nWeight
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
//	__EXT_DEBUG_GRID_ASSERT( m_nContentWeightExpanded >= nWeight );
//	__EXT_DEBUG_GRID_ASSERT( m_nContentWeightAll >= nWeight );
//	__EXT_DEBUG_GRID_ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
	m_nContentWeightAll -= nWeight;
//	__EXT_DEBUG_GRID_ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if( pNodeParent != NULL )
		pNodeParent->_ContentWeight_DecrementNonExpanded( nWeight );
}

void CExtTreeGridCellNode::_ContentWeight_Adjust()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_arrChildren.GetSize() == 0 )
	{
		m_nContentWeightAll = m_nContentWeightExpanded = 0;
		if( TreeNodeGetParent() != NULL )
			m_bExpanded = false;
	}
	else
	{
		m_nContentWeightAll = _ContentWeight_CalcAll();
		if( TreeNodeIsDisplayed() && TreeNodeIsExpanded() )
		{
			m_nContentWeightExpanded = _ContentWeight_CalcVisible();
			__EXT_DEBUG_GRID_ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
		}
		else
			m_nContentWeightExpanded = 0;
	}
	if( m_pNodeParent != NULL )
		m_pNodeParent->_ContentWeight_Adjust();
}

ULONG CExtTreeGridCellNode::_ContentWeight_CalcAll()
{
ULONG nCount = TreeNodeGetChildCount();
ULONG nCalcVal = 0;
	for( ULONG nIdx = 0; nIdx < nCount; nIdx++ )
	{
		CExtTreeGridCellNode * pNode = TreeNodeGetChildAt( nIdx );
		__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		nCalcVal ++;
		nCalcVal += pNode->_ContentWeight_CalcAll();
	} // for( ULONG nIdx = 0; nIdx < nCount; nIdx++ )
	return nCalcVal;
}

ULONG CExtTreeGridCellNode::_ContentWeight_CalcVisible() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	//__EXT_DEBUG_GRID_ASSERT( TreeNodeIsExpanded() );
	if( TreeNodeHiddenGet() )
		return 0L;
	if( ! TreeNodeIsExpanded() )
		return 0L;
ULONG nCount = TreeNodeGetChildCount();
ULONG nCalcVal = 0;
	for( ULONG nIdx = 0; nIdx < nCount; nIdx++ )
	{
		const CExtTreeGridCellNode * pNode = TreeNodeGetChildAt( nIdx );
		__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		if( pNode->TreeNodeHiddenGet() )
			continue;
		nCalcVal ++;
		if( pNode->TreeNodeIsExpanded() )
			nCalcVal += pNode->_ContentWeight_CalcVisible();
	} // for( ULONG nIdx = 0; nIdx < nCount; nIdx++ )
	return nCalcVal;
}

void CExtTreeGridCellNode::_Content_FillVisibleArray(
	CExtTreeGridCellNode::NodeArr_t & arr,
	ULONG & nOffset
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( TreeNodeHiddenGet() )
		return;
ULONG nCount = TreeNodeGetChildCount();
//	__EXT_DEBUG_GRID_ASSERT( (nOffset+nCount) <= ULONG(arr.GetSize()) );
	for( ULONG nIdx = 0; nIdx < nCount; nIdx++ )
	{
		CExtTreeGridCellNode * pNode = TreeNodeGetChildAt( nIdx );
		__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		if( pNode->TreeNodeHiddenGet() )
			continue;
		arr.SetAt( nOffset++, pNode );
		if( pNode->TreeNodeIsExpanded() )
			pNode->_Content_FillVisibleArray( arr, nOffset );
	} // for( ULONG nIdx = 0; nIdx < nCount; nIdx++ )
}

CExtTreeGridCellNode::operator HTREEITEM () const
{
#ifdef _DEBUG
	if( this != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( this );
	}
#endif // _DEBUG
	return reinterpret_cast < HTREEITEM > ( LPVOID(this) );
}

CExtTreeGridCellNode * CExtTreeGridCellNode::FromHTREEITEM(
	HTREEITEM hTreeItem
	)
{
	if( hTreeItem == NULL )
		return NULL;
CExtTreeGridCellNode * pCell = reinterpret_cast < CExtTreeGridCellNode * > ( hTreeItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
	__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtTreeGridCellNode, pCell );
	return pCell;
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetRoot()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtTreeGridCellNode * pNode = this;
	for( ; true; )
	{
		CExtTreeGridCellNode * pNodeParent = pNode->TreeNodeGetParent();
		if( pNodeParent == NULL )
			break;
		pNode = pNodeParent;
	} // for( ; true; )
	return pNode;
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetRoot() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ( const_cast < CExtTreeGridCellNode * > ( this ) ) -> TreeNodeGetRoot();
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetParent()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pNodeParent != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pNodeParent );
	}
#endif // _DEBUG
	return m_pNodeParent;
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetParent() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pNodeParent != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pNodeParent );
	}
#endif // _DEBUG
	return m_pNodeParent;
}

CExtTreeGridCellNode * CExtTreeGridCellNode::_TreeNodeBrowseNextImpl(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper,
	bool bIncludeHidden
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( (! bSiblingOnly )
		&& bWalkDeeper
		&& TreeNodeGetChildCount() > 0
		&& ( (! bExpandedWalk ) || TreeNodeIsExpanded() )
		)
		return TreeNodeGetChildAt( 0 );
	if( m_pNodeNext != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pNodeNext );
		if(		(!bIncludeHidden)
			&&	m_pNodeNext->TreeNodeHiddenGet()
			)
			return m_pNodeNext->_TreeNodeBrowseNextImpl( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
		else
			return m_pNodeNext;
	}
	if( bSiblingOnly )
		return NULL;
	if( ! bExpandedWalk )
		return NULL;
CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if(		pNodeParent == NULL
		||	pNodeParent->TreeNodeGetParent() == NULL
		)
		return NULL;
CExtTreeGridCellNode * pNode = pNodeParent;
	for( ; pNode != NULL; )
	{
		CExtTreeGridCellNode * pNodeX = pNode->_TreeNodeBrowseNextImpl( true, false, false, bIncludeHidden );
		if( pNodeX != NULL )
		{
			if(		(!bIncludeHidden)
				&&	pNodeX->TreeNodeHiddenGet()
				)
				return pNodeX->_TreeNodeBrowseNextImpl( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
			else
				return pNodeX;
		}
		pNode = pNode->TreeNodeGetParent();
		if(		pNodeParent == NULL
//			||	pNodeParent->TreeNodeGetParent() == NULL
			)
			return NULL;
	}
	return NULL;
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::_TreeNodeBrowseNextImpl(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper,
	bool bIncludeHidden
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridCellNode * > ( this ) )
		-> _TreeNodeBrowseNextImpl( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
}

CExtTreeGridCellNode * CExtTreeGridCellNode::_TreeNodeBrowsePrevImpl(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper,
	bool bIncludeHidden
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bWalkDeeper;
	if( m_pNodePrev != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pNodePrev );
		if( bSiblingOnly )
		{
			if(		(!bIncludeHidden)
				&&	m_pNodePrev->TreeNodeHiddenGet()
				)
				return m_pNodePrev->_TreeNodeBrowsePrevImpl( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
			else
				return m_pNodePrev;
		}
		CExtTreeGridCellNode * pNode = m_pNodePrev;
		for( ; pNode->TreeNodeGetChildCount() > 0; )
		{
			if( bExpandedWalk && (! pNode->TreeNodeIsExpanded() )  )
			{
				if(		(!bIncludeHidden)
					&&	pNode->TreeNodeHiddenGet()
					)
					return pNode->_TreeNodeBrowsePrevImpl( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
				else
					return pNode;
			}
			pNode = pNode->TreeNodeGetChildAt( pNode->TreeNodeGetChildCount() - 1 );
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		}
		return pNode;
	}
	if( bSiblingOnly )
		return NULL;
CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if(		pNodeParent == NULL
		||	pNodeParent->TreeNodeGetParent() == NULL
		)
		return NULL;
	if(		(!bIncludeHidden)
		&&	pNodeParent != NULL
		&&	pNodeParent->TreeNodeHiddenGet()
		)
		return pNodeParent->_TreeNodeBrowsePrevImpl( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
	else
		return pNodeParent;
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::_TreeNodeBrowsePrevImpl(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper,
	bool bIncludeHidden
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridCellNode * > ( this ) )
		-> _TreeNodeBrowsePrevImpl( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden );
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetNext(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bIncludeHidden
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( bSiblingOnly )
	{
		if( m_pNodeNext == NULL )
			return NULL;
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pNodeNext );
		if( bIncludeHidden )
			return m_pNodeNext;
		CExtTreeGridCellNode * pNode = m_pNodeNext;
		for( ; pNode != NULL; pNode = pNode->m_pNodeNext )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
			if( ! pNode->TreeNodeHiddenGet() )
				return pNode;
		}
		return NULL;
	} // if( bSiblingOnly )
	if(		TreeNodeGetChildCount() > 0
		&&	(	( ! bExpandedWalk )
			||	TreeNodeIsExpanded()
			)
		)
	{
		CExtTreeGridCellNode * pNode = TreeNodeGetChildAt( 0 );
		if( bIncludeHidden )
			return pNode;
		for( ; pNode != NULL; pNode = pNode->m_pNodeNext )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
			if( ! pNode->TreeNodeHiddenGet() )
				return pNode;
		}
		return NULL;
	}
CExtTreeGridCellNode * pNode = TreeNodeGetNext( true, false, bIncludeHidden );
	if( pNode != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		return pNode;
	}
	pNode = this;
CExtTreeGridCellNode * pNodeParent = pNode->TreeNodeGetParent();
	for( ; pNodeParent != NULL;  )
	{
		ULONG nSiblingIndex = pNode->TreeNodeGetSiblingIndex();
		ULONG nSiblingCount = pNodeParent->TreeNodeGetChildCount();
		__EXT_DEBUG_GRID_ASSERT( nSiblingIndex < nSiblingCount );
		if( nSiblingIndex < (nSiblingCount-1) )
		{
			CExtTreeGridCellNode * pNodeTestNext = pNodeParent->TreeNodeGetChildAt( nSiblingIndex + 1 );
			__EXT_DEBUG_GRID_ASSERT_VALID( pNodeTestNext );
			if(		bIncludeHidden
				||	( ! pNodeTestNext->TreeNodeHiddenGet() )
				)
			{
				if(		( ! bExpandedWalk )
					||	pNodeParent->TreeNodeIsExpanded()
					)
					return pNodeTestNext;
			}
			return pNodeTestNext->TreeNodeGetNext( false, false, bIncludeHidden );
		}
		pNode = pNodeParent;
		pNodeParent = pNodeParent->TreeNodeGetParent();
	}
	return NULL;
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetNext(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bIncludeHidden
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridCellNode * > ( this ) )
		-> TreeNodeGetNext( bSiblingOnly, bExpandedWalk, bIncludeHidden );
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetPrev(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bIncludeHidden
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtTreeGridCellNode * pNode = this, * pNodeTestPrev = NULL;
	for( ; true; )
	{
		if( bSiblingOnly )
		{
			if( pNode->m_pNodePrev == NULL )
				return NULL;
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode->m_pNodePrev );
			if( bIncludeHidden )
				return pNode->m_pNodePrev;
			CExtTreeGridCellNode * pNode2 = pNode->m_pNodePrev;
			for( ; pNode2 != NULL; pNode2 = pNode2->m_pNodePrev )
			{
				__EXT_DEBUG_GRID_ASSERT_VALID( pNode2 );
				if( ! pNode2->TreeNodeHiddenGet() )
					return pNode2;
			}
			return NULL;
		} // if( bSiblingOnly )
		CExtTreeGridCellNode * pNodeParent = pNode->TreeNodeGetParent();
		if( pNodeParent == NULL )
			return NULL;
		ULONG nSiblingIndex = pNode->TreeNodeGetSiblingIndex();
#ifdef _DEBUG
		ULONG nSiblingCount = pNodeParent->TreeNodeGetChildCount();
		nSiblingCount;
		__EXT_DEBUG_GRID_ASSERT( nSiblingIndex < nSiblingCount );
#endif // _DEBUG
		if( nSiblingIndex == 0 )
			return pNodeParent;
		pNodeTestPrev = pNodeParent->TreeNodeGetChildAt( nSiblingIndex - 1 );
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodeTestPrev );
		if(		(! bIncludeHidden )
			&&	pNodeTestPrev->TreeNodeHiddenGet()
			)
		{
			pNode =  pNodeTestPrev;
			continue;
		}
		break;
	}
	__EXT_DEBUG_GRID_ASSERT( pNodeTestPrev != NULL );
CExtTreeGridCellNode * pNodeDeep = pNodeTestPrev;
	for( ; pNodeDeep != NULL; )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodeDeep );
		ULONG nDeepChildCount = pNodeDeep->TreeNodeGetChildCount();
		if(		(	bExpandedWalk
				&&	( ! pNodeDeep->TreeNodeIsExpanded() )
				)
			||	nDeepChildCount == 0
			)
			return pNodeDeep;
		if(		(! bIncludeHidden )
			&&	pNodeDeep->TreeNodeHiddenGet()
			)
			return pNodeDeep->TreeNodeGetPrev( false, true, bIncludeHidden );
		pNodeDeep = pNodeDeep->TreeNodeGetChildAt( nDeepChildCount - 1 );
	}
	return NULL;
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetPrev(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bIncludeHidden
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridCellNode * > ( this ) )
		-> TreeNodeGetPrev( bSiblingOnly, bExpandedWalk, bIncludeHidden );
}

ULONG CExtTreeGridCellNode::TreeNodeGetSiblingIndex(
	bool bIncludeHidden // = true
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if( pNodeParent == NULL )
		return 0;
	if( bIncludeHidden )
		return m_nOptIndex;
ULONG nWalkNo = 0, nIdx, nCount = pNodeParent->TreeNodeGetChildCount();
	for( nIdx = 0; nIdx < nCount; nIdx ++ )
	{
		const CExtTreeGridCellNode * pNode = pNodeParent->TreeNodeGetChildAt( nIdx );
		__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		if( pNode->TreeNodeHiddenGet() )
			continue;
		if( pNode == this )
			return nWalkNo;
		nWalkNo ++;
	} // for( nIdx = 0; nIdx < nCount; nIdx ++ )
	return ULONG(-1L);
}

ULONG CExtTreeGridCellNode::TreeNodeGetChildCount() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ULONG(m_arrChildren.GetSize());
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetChildAt( ULONG nPos )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_arrChildren.GetSize() == 0 )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT( nPos < ULONG(m_arrChildren.GetSize()) );
CExtTreeGridCellNode * pNode = m_arrChildren.GetAt( nPos );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	__EXT_DEBUG_GRID_ASSERT( pNode->m_nOptIndex == nPos );
	return pNode;
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetChildAt( ULONG nPos ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nPos < ULONG(m_arrChildren.GetSize()) );
CExtTreeGridCellNode * pNode = m_arrChildren.GetAt( nPos );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	__EXT_DEBUG_GRID_ASSERT( pNode->m_nOptIndex == nPos );
	return pNode;
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetFirstSibling()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_pNodeParent == NULL )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pNodeParent );
	return m_pNodeParent->TreeNodeGetFirstChild();
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetFirstSibling() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_pNodeParent == NULL )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pNodeParent );
	return m_pNodeParent->TreeNodeGetFirstChild();
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetLastSibling()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_pNodeParent == NULL )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pNodeParent );
	return m_pNodeParent->TreeNodeGetLastChild();
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetLastSibling() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_pNodeParent == NULL )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pNodeParent );
	return m_pNodeParent->TreeNodeGetLastChild();
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetFirstChild()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return TreeNodeGetChildAt( 0 );
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetFirstChild() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return TreeNodeGetChildAt( 0 );
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetLastChild()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return TreeNodeGetChildAt( TreeNodeGetChildCount() - 1 );
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetLastChild() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return TreeNodeGetChildAt( TreeNodeGetChildCount() - 1 );
}

ULONG CExtTreeGridCellNode::TreeNodeIndentCompute() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( TreeNodeHiddenGet() )
		return 0L;
ULONG nCalcIndent = 0;
const CExtTreeGridCellNode * pNode = TreeNodeGetParent();
	for( ; pNode != NULL; pNode = pNode->TreeNodeGetParent() )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		if( TreeNodeHiddenGet() )
			return 0L;
		nCalcIndent ++;
	} // for( ; pNode != NULL; pNode = pNode->TreeNodeGetParent() )
	return nCalcIndent;
}

ULONG CExtTreeGridCellNode::TreeNodeIndentPxGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_nIndentPx;
}

void CExtTreeGridCellNode::TreeNodeIndentPxSet(
	ULONG nIndentPx
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_nIndentPx = nIndentPx;
}

CExtTreeGridCellNode::e_expand_box_shape_t CExtTreeGridCellNode::TreeNodeGetExpandBoxShape() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( TreeNodeGetChildCount() == 0 )
		return __EEBS_NONE;
	if( TreeNodeGetParent() == NULL )
		__EEBS_EXPANDED;
	return m_bExpanded ? __EEBS_EXPANDED : __EEBS_COLLAPSED;
}

bool CExtTreeGridCellNode::TreeNodeHiddenGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
DWORD dwStyleEx = GetStyleEx();
bool bHidden = ( ( dwStyleEx & __EGCS_EX_HIDDEN_TREE_NODE ) != 0 ) ? true : false;
	return bHidden;
}

void CExtTreeGridCellNode::TreeNodeHiddenSet(
	CExtTreeGridDataProvider & _DP,
	bool bHide
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( (&_DP) );
	__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtTreeGridDataProvider, (&_DP) );
bool bHidden = TreeNodeHiddenGet();
	if( ( bHidden && bHide ) || ( (!bHidden) && (!bHide) ) )
		return;
	if( bHide )
	{
		if( TreeNodeIsDisplayed() )
		{
			ULONG nOffset = TreeNodeCalcOffset( true, false );
			if( nOffset != ULONG(-1L) )
			{
				__EXT_DEBUG_GRID_ASSERT( _DP.m_arrGridVis[ nOffset ] == this );
				INT nCountToRemove = _ContentWeight_Get( true );
				_DP.m_arrGridVis.RemoveAt( nOffset, nCountToRemove + 1 );
				CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
				if( pNodeParent != NULL )
				{
					__EXT_DEBUG_GRID_ASSERT_VALID( pNodeParent );
					pNodeParent->_ContentWeight_Decrement( nCountToRemove + 1, true );
				}
			}
		}
		ModifyStyleEx( __EGCS_EX_HIDDEN_TREE_NODE, 0 );
	} // if( bHide )
	else
	{
		ModifyStyleEx( 0, __EGCS_EX_HIDDEN_TREE_NODE );
		if( TreeNodeIsDisplayed() )
		{
			_ContentWeight_Adjust();
			ULONG nOffset = TreeNodeCalcOffset( true, false );
			if( nOffset != ULONG(-1L) )
			{
//				m_nContentWeightAll = ULONG( m_arrChildren.GetSize() );
				INT nCountToInsert = _ContentWeight_CalcVisible();
				//INT nCountToInsert = _ContentWeight_Get( true );
//				ULONG nOffset = TreeNodeCalcOffset( true );
//				__EXT_DEBUG_GRID_ASSERT( nWeightVisible >= pNode->TreeNodeGetChildCount() );
				_DP.m_arrGridVis.InsertAt( nOffset, NULL, nCountToInsert + 1 );
				_DP.m_arrGridVis.SetAt( nOffset, this );
				nOffset ++;
				if( TreeNodeIsExpanded() )
					_Content_FillVisibleArray( _DP.m_arrGridVis, nOffset );
// 				CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
// 				if( pNodeParent != NULL )
// 				{
// 					__EXT_DEBUG_GRID_ASSERT_VALID( pNodeParent );
// 					pNodeParent->_ContentWeight_Increment( nCountToInsert + 1, true );
// 				}
			}
		}
	} // else from if( bHide )
}

bool CExtTreeGridCellNode::TreeNodeIsDisplayed() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( TreeNodeHiddenGet() )
		return false;
const CExtTreeGridCellNode * pNode = this;
	for( ; true; )
	{
		if( pNode->TreeNodeHiddenGet() )
			return false;
		const CExtTreeGridCellNode * pNodeParent =
			pNode->TreeNodeGetParent();
		if( pNodeParent == NULL )
			return true;
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodeParent );
		pNode = pNodeParent;
		if( ! pNode->TreeNodeIsExpanded() )
			return false;
	} // for( ; true; )
	return false;
}

void CExtTreeGridCellNode::TreeNodeMarkExpanded(
	bool bExpand
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( TreeNodeGetParent() != NULL || bExpand );
	m_bExpanded = bExpand;
}

bool CExtTreeGridCellNode::TreeNodeIsExpanded() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( TreeNodeGetParent() == NULL )
		return true;
	return m_bExpanded;
}

ULONG CExtTreeGridCellNode::TreeNodeCalcOffset(
	bool bExpandedOnly,
	bool bIncludeHidden // = true
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if( pNodeParent == NULL )
		return 0;
	if( (!bIncludeHidden) && pNodeParent->TreeNodeHiddenGet() )
		return ULONG(-1L);
ULONG nOffset = 0;
const CExtTreeGridCellNode * pNodeCompute = this;
	for( ;	pNodeCompute != NULL; )
	{
		const CExtTreeGridCellNode * pNode =
			pNodeCompute->_TreeNodeBrowsePrevImpl( true, false, false, true );
		for(	; pNode != NULL;
				pNode = pNode->_TreeNodeBrowsePrevImpl( true, false, false, true )
			)
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
			if( (!bIncludeHidden) && pNode->TreeNodeHiddenGet() )
				continue;
			nOffset ++;
			ULONG nContentWeight = pNode->_ContentWeight_Get( bExpandedOnly );
			nOffset += nContentWeight;
		}
		pNodeParent = pNodeCompute->TreeNodeGetParent();
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodeParent );
		if( pNodeParent->TreeNodeGetParent() == NULL )
			break;
		if( (!bIncludeHidden) && pNodeParent->TreeNodeHiddenGet() )
			return ULONG(-1L);
		nOffset ++;
		pNodeCompute = pNodeParent;
	}
	return nOffset;
}

/////////////////////////////////////////////////////////////////////////////
// CExtTreeGridDataProvider

IMPLEMENT_DYNCREATE( CExtTreeGridDataProvider, CExtGridDataProvider );

CExtTreeGridDataProvider::CExtTreeGridDataProvider()
	: m_pCellRoot( NULL )
	, m_pTreeNodeDefaultRTC( RUNTIME_CLASS( CExtTreeGridCellNode ) )
	, m_nIndentPxDefault( __EXT_MGC_TREE_GRID_DEFAULT_LEVEL_INDENT )
	, m_nReservedColCount( 1L )
	, m_nReservedRowCount( 1L )
	, m_pTreeGridWnd( NULL )
{
	m_DP.m_pOuterDataProvider = this;
	m_DP.m_bEnableSortOrderUpdatingRows = false;
}

CExtTreeGridDataProvider::~CExtTreeGridDataProvider()
{
	m_DP.m_pOuterDataProvider = NULL;
	m_pTreeGridWnd = NULL;
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::TreeNodeGetRoot()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return _Tree_NodeGetRoot();
}

const CExtTreeGridCellNode * CExtTreeGridDataProvider::TreeNodeGetRoot() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridDataProvider * > ( this ) )
		-> TreeNodeGetRoot();
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::TreeNodeGetByVisibleRowIndex( ULONG nRowNo )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return _Tree_NodeGetByVisibleRowIndex( nRowNo );
}

const CExtTreeGridCellNode * CExtTreeGridDataProvider::TreeNodeGetByVisibleRowIndex( ULONG nRowNo ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridDataProvider * > ( this ) )
		-> TreeNodeGetByVisibleRowIndex( nRowNo );
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::_Tree_NodeGetByVisibleRowIndex( ULONG nRowNo )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	nRowNo = _Tree_MapRowToCache( nRowNo );
CExtTreeGridCellNode * pNode = STATIC_DOWNCAST( CExtTreeGridCellNode, _DP.CellGet( 0, nRowNo ) );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	return pNode;
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::_Tree_NodeGetRoot()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_pCellRoot == NULL )
		_Tree_GetCacheDP(); // init
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pCellRoot );
	return m_pCellRoot;
}

ULONG CExtTreeGridDataProvider::TreeGetDisplayedCount() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return _Tree_GetDisplayedCount();
}

void CExtTreeGridDataProvider::OnNodeRemoved(
	CExtTreeGridCellNode * pNode
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	if( m_pTreeGridWnd != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pTreeGridWnd );
		m_pTreeGridWnd->OnTreeGridNodeRemoved( pNode );
	} // if( m_pTreeGridWnd != NULL )
}

ULONG CExtTreeGridDataProvider::TreeGetRowIndentPx( ULONG nRowNo ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
//ULONG nReservedRowCount = 0;
//	CacheReservedCountsGet( NULL, &nReservedRowCount );
//	nRowNo += nReservedRowCount;
	return
		( const_cast < CExtTreeGridDataProvider * > ( this ) )
		-> _Tree_GetRowIndentPx( nRowNo );
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::TreeNodeInsert(
	CExtTreeGridCellNode * pNodeParent, // = NULL // if NULL - root
	ULONG nIdxInsertBefore, // = (ULONG(-1L)) // if (ULONG(-1L)) - insert to end
	ULONG nInsertCount // = 1
	) // returns pointer to first inserted
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return _Tree_NodeInsert( pNodeParent, nIdxInsertBefore, nInsertCount );
}

bool CExtTreeGridDataProvider::_Tree_NodeCopyMoveTest(
	CExtTreeGridCellNode * pNodeCopyMove,
	CExtTreeGridCellNode * pNodeNewParent, // if NULL - root
	ULONG nIdxInsertBefore, // = (ULONG(-1L)) // if (ULONG(-1L)) - insert to end
	bool bEnableCopyingIntoInnerBranch, // = true
	bool * p_bCopyingIntoInnerBranch // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( p_bCopyingIntoInnerBranch != NULL )
		(*p_bCopyingIntoInnerBranch) = false;
	if( pNodeCopyMove == NULL )
		return false;
CExtTreeGridCellNode * pNodeRoot = _Tree_NodeGetRoot();
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeRoot );
	if( pNodeCopyMove == pNodeRoot )
		return false;
	if( pNodeNewParent == NULL )
		pNodeNewParent = pNodeRoot;
CExtTreeGridCellNode * pNodeOldParent =
		pNodeCopyMove->TreeNodeGetParent();
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeOldParent );
	if( pNodeNewParent == pNodeOldParent )
	{
		ULONG nSiblingIndexCurrent = pNodeCopyMove->TreeNodeGetSiblingIndex();
		ULONG nSiblingCount = pNodeOldParent->TreeNodeGetChildCount();
		ULONG nSiblingIndexNew = nIdxInsertBefore;
		if(		nSiblingIndexNew == (ULONG(-1L))
			||	nSiblingIndexNew >= nSiblingCount
			)
			nSiblingIndexNew = nSiblingCount - 1;
		if( nSiblingIndexNew == nSiblingIndexCurrent )
			return false;
		return true;
	} // if( pNodeNewParent == pNodeOldParent )
	if(		bEnableCopyingIntoInnerBranch
		&&	p_bCopyingIntoInnerBranch == NULL
		)
		return true;
CExtTreeGridCellNode * pNodeSearchIntersection = pNodeNewParent; //->TreeNodeGetParent();
	for( ; pNodeSearchIntersection != pNodeRoot; pNodeSearchIntersection = pNodeSearchIntersection->TreeNodeGetParent() )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodeSearchIntersection );
		if( pNodeSearchIntersection != pNodeCopyMove )
			continue;
		if( p_bCopyingIntoInnerBranch != NULL )
			(*p_bCopyingIntoInnerBranch) = true;
		if( bEnableCopyingIntoInnerBranch )
			return true;
		return false;
	} // for( ; pNodeSearchIntersection != pNodeRoot; pNodeSearchIntersection = pNodeSearchIntersection->TreeNodeGetParent() )
	return true;
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::_Tree_NodeCopyMove(
	CExtTreeGridCellNode * pNodeCopyMove,
	CExtTreeGridCellNode * pNodeNewParent, // if NULL - root
	ULONG nIdxInsertBefore, // = (ULONG(-1L)) // if (ULONG(-1L)) - insert to end
	bool bMove, // = true
	bool bCloneOnMove, // = true
	bool bCloneCellObjects, // = true
	INT nExpandAction, // = TVE_TOGGLE // TVE_TOGGLE in this case - keep expanded state
	bool bEnableCopyingIntoInnerBranch, // = true
	bool * p_bCopyingIntoInnerBranch // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( bMove )
		bEnableCopyingIntoInnerBranch = false;
bool bCopyingIntoInnerBranch = false;
bool bCopyMoveTestRetVal =
		_Tree_NodeCopyMoveTest(
			pNodeCopyMove,
			pNodeNewParent,
			nIdxInsertBefore,
			bEnableCopyingIntoInnerBranch,
			&bCopyingIntoInnerBranch
			);
	if( p_bCopyingIntoInnerBranch != NULL )
		(*p_bCopyingIntoInnerBranch) = bCopyingIntoInnerBranch;
	if( ! bCopyMoveTestRetVal )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeCopyMove );
CExtTreeGridCellNode * pNodeRoot = _Tree_NodeGetRoot();
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeRoot );
	if( pNodeNewParent == NULL )
		pNodeNewParent = pNodeRoot;
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeNewParent );
ULONG nTargetParentSiblingCount = pNodeNewParent->TreeNodeGetChildCount();
	if(		nIdxInsertBefore == ULONG(-1L)
		||	nIdxInsertBefore > nTargetParentSiblingCount
		)
		nIdxInsertBefore = nTargetParentSiblingCount;
CExtTreeGridCellNode * pNodeRetVal = NULL;
	if( bMove && ( ! bCloneOnMove ) )
	{
		if(	_Tree_NodeMoveImpl(
				pNodeCopyMove,
				pNodeNewParent,
				nIdxInsertBefore,
				nExpandAction
				)
			)
			pNodeRetVal = pNodeCopyMove;
	}
	else
		pNodeRetVal =
			_Tree_NodeCopyImpl(
				pNodeCopyMove,
				pNodeNewParent,
				NULL,
				nIdxInsertBefore,
				nExpandAction,
				bCloneCellObjects
				);
	if( pNodeRetVal == NULL )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeRetVal );
	if( bMove && bCloneOnMove )
		_Tree_NodeRemove( pNodeCopyMove );
	return pNodeRetVal;
}

bool CExtTreeGridDataProvider::_Tree_NodeMoveImpl(
	CExtTreeGridCellNode * pNodeCopyMove,
	CExtTreeGridCellNode * pNodeNewParent,
	ULONG nIdxInsertBefore,
	INT nExpandAction
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeCopyMove );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeNewParent );
	nExpandAction;
ULONG nIndex;
CExtTreeGridCellNode * pNode = NULL;
bool b3 = pNodeCopyMove->TreeNodeIsDisplayed();
//bool b4 = pNodeNewParent->TreeNodeIsDisplayed() && pNodeNewParent->TreeNodeIsExpanded();
//	if( ! ( ( b3 && b4 ) || ( (!b3) && (!b4) ) ) )
//	{
//		__EXT_DEBUG_GRID_ASSERT( FALSE );
//		return false;
//	}
	if( b3 )
	{
		CExtTreeGridCellNode * pNodeRoot = TreeNodeGetRoot();
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodeRoot );
		__EXT_DEBUG_GRID_ASSERT( pNodeCopyMove != pNodeRoot );
		pNode = pNodeCopyMove->TreeNodeGetParent();
		__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		for( ; LPCVOID(pNodeRoot) != LPCVOID(pNode); )
		{
			TreeNodeExpand( pNode, TVE_EXPAND );
			pNode = pNode->TreeNodeGetParent();
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		} // for( ; LPCVOID(pNodeRoot) == LPCVOID(pNode); )
		pNode = pNodeNewParent;
		for( ; LPCVOID(pNodeRoot) != LPCVOID(pNode); )
		{
			TreeNodeExpand( pNode, TVE_EXPAND );
			pNode = pNode->TreeNodeGetParent();
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		} // for( ; LPCVOID(pNodeRoot) == LPCVOID(pNode); )
	}

CExtTreeGridCellNode * pNodeOldParent = pNodeCopyMove->m_pNodeParent;
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeOldParent );
ULONG nOldOptIndex = pNodeCopyMove->TreeNodeGetSiblingIndex();

ULONG nTargetChildrenCount = pNodeNewParent->TreeNodeGetChildCount();
	__EXT_DEBUG_GRID_ASSERT( nIdxInsertBefore <= nTargetChildrenCount );
CExtTreeGridCellNode * pNodeTargetCalc = NULL;
	if( nIdxInsertBefore == nTargetChildrenCount )
		pNodeTargetCalc = pNodeNewParent;
	else
		pNodeTargetCalc = pNodeNewParent->TreeNodeGetChildAt( nIdxInsertBefore );
ULONG nExpandedOffsetSrc = pNodeCopyMove->TreeNodeCalcOffset( true, false );
ULONG nPhysicalOffsetSrc = pNodeCopyMove->TreeNodeCalcOffset( false );
ULONG nExpandedOffsetDst = pNodeTargetCalc->TreeNodeCalcOffset( true, false );
ULONG nPhysicalOffsetDst = pNodeTargetCalc->TreeNodeCalcOffset( false );
//ULONG nMoveWeightExpanded = pNodeCopyMove->_ContentWeight_Get( true ) + 1;
ULONG nMoveWeightExpanded = pNodeCopyMove->_ContentWeight_CalcVisible() + 1;
ULONG nMoveWeightPhysical = pNodeCopyMove->_ContentWeight_Get( false ) + 1;
	if( nIdxInsertBefore == nTargetChildrenCount )
	{
//		nExpandedOffsetDst += pNodeTargetCalc->_ContentWeight_Get( true ) + 1;
		nExpandedOffsetDst += pNodeTargetCalc->_ContentWeight_CalcVisible() + 1;
		nPhysicalOffsetDst += pNodeTargetCalc->_ContentWeight_Get( false ) + 1;
	}
CExtTreeGridCellNode::NodeArr_t _arrNodes;
	if( b3 )
	{
		_arrNodes.SetSize( nMoveWeightExpanded );
		for( nIndex = 0; nIndex < nMoveWeightExpanded; nIndex ++ )
		{
			pNode = m_arrGridVis[ nExpandedOffsetSrc + nIndex ];
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
			_arrNodes.SetAt( nIndex, pNode );
		}
		m_arrGridVis.RemoveAt( nExpandedOffsetSrc, nMoveWeightExpanded );
		ULONG _nExpandedOffsetDst = nExpandedOffsetDst;
		if( _nExpandedOffsetDst > nExpandedOffsetSrc )
			_nExpandedOffsetDst -= nMoveWeightExpanded;
		m_arrGridVis.InsertAt( _nExpandedOffsetDst, &_arrNodes );
	}

	_arrNodes.SetSize( nMoveWeightPhysical );
	for( nIndex = 0; nIndex < nMoveWeightPhysical; nIndex ++ )
	{
		pNode = m_arrGridRef[ nPhysicalOffsetSrc + nIndex ];
		__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		_arrNodes.SetAt( nIndex, pNode );
	}
	m_arrGridRef.RemoveAt( nPhysicalOffsetSrc, nMoveWeightPhysical );
ULONG _nPhysicalOffsetDst = nPhysicalOffsetDst;
	if( _nPhysicalOffsetDst > nPhysicalOffsetSrc )
		_nPhysicalOffsetDst -= nMoveWeightPhysical;
	m_arrGridRef.InsertAt( _nPhysicalOffsetDst, &_arrNodes );

ULONG nReservedRowCount = 0L;
	m_DP.CacheReservedCountsGet( NULL, &nReservedRowCount );
	nPhysicalOffsetSrc += nReservedRowCount;
	nPhysicalOffsetDst += nReservedRowCount;
LONG nTmp = 0;
	for( nIndex = 0; nIndex < nMoveWeightPhysical; nIndex ++ )
	{
		if( nPhysicalOffsetSrc < nPhysicalOffsetDst )
		{
			ULONG nOffsetSrc = nPhysicalOffsetSrc + nMoveWeightPhysical - 1 - nIndex;
			ULONG nOffsetDst = nPhysicalOffsetDst + nMoveWeightPhysical - 1 - nIndex;
			for( ; nOffsetSrc != nOffsetDst; )
			{
				m_DP._SortSwapSeries(
					nOffsetSrc,
					nOffsetSrc + 1,
					nTmp,
					NULL,
					false
					);
				nOffsetSrc ++;
			}
		}
		else
		{
			ULONG nOffsetSrc = nPhysicalOffsetSrc + nIndex;
			ULONG nOffsetDst = nPhysicalOffsetDst + nIndex;
			for( ; nOffsetSrc != nOffsetDst; )
			{
				m_DP._SortSwapSeries(
					nOffsetSrc,
					nOffsetSrc - 1,
					nTmp,
					NULL,
					false
					);
				nOffsetSrc --;
			}
		}
	}

CExtTreeGridCellNode * pNodePrev = pNodeCopyMove->m_pNodePrev;
CExtTreeGridCellNode * pNodeNext = pNodeCopyMove->m_pNodeNext;
	if( pNodePrev != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodePrev );
		pNodePrev->m_pNodeNext = pNodeNext;
	}
	if( pNodeNext != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodeNext );
		pNodeNext->m_pNodePrev = pNodePrev;
	}
	pNodeCopyMove->m_pNodePrev = pNodeCopyMove->m_pNodeNext = NULL;
	if( nIdxInsertBefore == nTargetChildrenCount )
	{
		if( nTargetChildrenCount > 0 )
		{
			pNode = pNodeNewParent->TreeNodeGetLastChild();
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
			__EXT_DEBUG_GRID_ASSERT( pNode->m_pNodeNext == NULL );
			pNode->m_pNodeNext = pNodeCopyMove;
			pNodeCopyMove->m_pNodePrev = pNode;
		}
	}
	else
	{
		pNodeCopyMove->m_pNodeNext = pNodeTargetCalc;
		pNodeCopyMove->m_pNodePrev = pNodeTargetCalc->m_pNodePrev;
		if( pNodeCopyMove->m_pNodePrev != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pNodeCopyMove->m_pNodePrev );
			pNodeCopyMove->m_pNodePrev->m_pNodeNext = pNodeCopyMove;
		}
		if( pNodeCopyMove->m_pNodeNext != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pNodeCopyMove->m_pNodeNext );
			pNodeCopyMove->m_pNodeNext->m_pNodePrev = pNodeCopyMove;
		}
	}

	if( LPVOID(pNodeNewParent) != LPVOID(pNodeOldParent) )
	{
		pNodeOldParent->m_arrChildren.RemoveAt( nOldOptIndex, 1 );
		ULONG nStartIndex = nOldOptIndex;
		ULONG nEndIndex = ULONG( pNodeOldParent->m_arrChildren.GetSize() );
		for( nIndex = nStartIndex; nIndex < nEndIndex; nIndex ++ )
		{
			pNode = pNodeOldParent->m_arrChildren[ nIndex ];
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
			pNode->m_nOptIndex = nIndex;
		}
		pNodeNewParent->m_arrChildren.InsertAt( nIdxInsertBefore, pNodeCopyMove, 1 );
		nStartIndex = nIdxInsertBefore;
		nEndIndex = ULONG( pNodeNewParent->m_arrChildren.GetSize() );
		for( nIndex = nStartIndex; nIndex < nEndIndex; nIndex ++ )
		{
			pNode = pNodeNewParent->m_arrChildren[ nIndex ];
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
			pNode->m_nOptIndex = nIndex;
		}
		
		for( pNode = pNodeOldParent; pNode != NULL && pNode->TreeNodeIsExpanded(); pNode = pNode->m_pNodeParent )
		{
			pNode->m_nContentWeightAll -= nMoveWeightPhysical;
			pNode->m_nContentWeightExpanded -= nMoveWeightExpanded;
		}
		for( pNode = pNodeNewParent; pNode != NULL && pNode->TreeNodeIsExpanded(); pNode = pNode->m_pNodeParent )
		{
			pNode->m_nContentWeightAll += nMoveWeightPhysical;
			pNode->m_nContentWeightExpanded += nMoveWeightExpanded;
		}
	}
	else
	{
		pNodeOldParent->m_arrChildren.RemoveAt( nOldOptIndex, 1 );
		ULONG _nIdxInsertBefore = nIdxInsertBefore;
		if( _nIdxInsertBefore  >= nOldOptIndex )
			_nIdxInsertBefore --;
		pNodeOldParent->m_arrChildren.InsertAt( _nIdxInsertBefore , pNodeCopyMove, 1 );
		ULONG nStartIndex = min( nOldOptIndex, nIdxInsertBefore );
		ULONG nEndIndex = ( max( nOldOptIndex, nIdxInsertBefore ) ) + 1;
		if( nEndIndex >= ULONG(pNodeOldParent->m_arrChildren.GetSize()) )
			nEndIndex = ULONG(pNodeOldParent->m_arrChildren.GetSize()) - 1;
		for( nIndex = nStartIndex; nIndex <= nEndIndex; nIndex ++ )
		{
			pNode = pNodeOldParent->m_arrChildren[ nIndex ];
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
			pNode->m_nOptIndex = nIndex;
		}
	}

	return true;
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::_Tree_NodeCopyImpl(
	CExtTreeGridCellNode * pNodeCopyMove,
	CExtTreeGridCellNode * pNodeNewParent,
	CExtTreeGridCellNode * pNodeSkip,
	ULONG nIdxInsertBefore,
	INT nExpandAction,
	bool bCloneCellObjects
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeCopyMove );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeNewParent );
	__EXT_DEBUG_GRID_ASSERT( nIdxInsertBefore <= pNodeNewParent->TreeNodeGetChildCount() );
CExtTreeGridCellNode * pNodeRetVal =
		_Tree_NodeInsert(
			pNodeNewParent,
			nIdxInsertBefore,
			1
			);
	if( pNodeRetVal == NULL )
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeRetVal );
	if( pNodeSkip == NULL )
		pNodeSkip = pNodeRetVal;
INT nActionTVE = TVE_COLLAPSE;
	switch( nExpandAction )
	{
	case TVE_COLLAPSE:
	break;
	case TVE_EXPAND:
		nActionTVE = TVE_EXPAND;
	break;
	case TVE_TOGGLE:
		if( pNodeCopyMove->TreeNodeIsExpanded() )
			nActionTVE = TVE_EXPAND;
	break;
#ifdef _DEBUG
	default:
		__EXT_DEBUG_GRID_ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( nExpandAction )
	_Tree_NodeExpand( pNodeRetVal, nActionTVE );
ULONG nChildIndex = 0, nChildIndexEffective = 0, nChildCount = pNodeCopyMove->TreeNodeGetChildCount();
	for( ; nChildIndex < nChildCount; nChildIndex ++ )
	{
		CExtTreeGridCellNode * pNodeChildSrc = pNodeCopyMove->TreeNodeGetChildAt( nChildIndex );
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodeChildSrc );
		if( pNodeChildSrc == pNodeSkip )
			continue;
		CExtTreeGridCellNode * pNodeChildDst =
			_Tree_NodeCopyImpl(
				pNodeChildSrc,
				pNodeRetVal,
				pNodeSkip,
				nChildIndexEffective,
				nExpandAction,
				bCloneCellObjects
				);
		if( pNodeChildDst == NULL )
		{
			_Tree_NodeRemove( pNodeRetVal );
			return NULL;
		} // if( pNodeChildDst == NULL )
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodeChildDst );
		nChildIndexEffective ++;
	} // for( ; nChildIndex < nChildCount; nChildIndex ++ )
	if( bCloneCellObjects )
	{
		CExtGridDataProvider & _DP = _Tree_GetCacheDP();
		ULONG nReservedColCount = 0, nReservedRowCount = 0;
		_DP.CacheReservedCountsGet( &nReservedColCount, &nReservedRowCount );
		ULONG nRowNoDst = pNodeRetVal->TreeNodeCalcOffset( false ) + nReservedRowCount;
		ULONG nRowNoSrc = pNodeCopyMove->TreeNodeCalcOffset( false ) + nReservedRowCount;
		//ULONG nReservedColCount = _Tree_ReservedColumnsGet();
		ULONG nColIdx = _Tree_ReservedColumnsGet(), nColCount = _DP.ColumnCountGet(); // - nReservedColCount;
		for( ; nColIdx < nColCount; nColIdx ++ )
		{
			CExtGridCell * pCellSrc =
				_DP.CellGet(
					nColIdx,
					nRowNoSrc
					);
			if( pCellSrc == NULL )
				continue;
			__EXT_DEBUG_GRID_ASSERT_VALID( pCellSrc );
			bool bCellClonedSuccessfully = _DP.CellRangeSet( nColIdx, nRowNoDst, 1L, 1L, pCellSrc, true );
			if( ! bCellClonedSuccessfully )
			{
				_Tree_NodeRemove( pNodeRetVal );
				return NULL;
			} // if( ! bCellClonedSuccessfully )
		} // for( ; nColIdx < nColCount; nColIdx ++ )
	} // if( bCloneCellObjects )
	return pNodeRetVal;
}

bool CExtTreeGridDataProvider::_Tree_SortOrderCheck(
	const CExtGridDataSortOrder & _gdso
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
LONG nCount = (LONG)_gdso.m_arrItems.GetSize();
	if( nCount == 0L )
		return true;
LONG nSize = ColumnCountGet();
	__EXT_DEBUG_GRID_ASSERT( nSize >= 0L );
	for( LONG i = 0L; i < nCount; i++ )
	{
		const CExtGridDataSortOrder::ITEM_INFO & _itemSrc =
			_gdso.m_arrItems[ i ];
		if(		_itemSrc.m_nRowColNo < 0L
			||	_itemSrc.m_nRowColNo >= nSize
			)
			return false;
	}
	return true;
}

bool  CExtTreeGridDataProvider::_Tree_NodeSortChildren(
	CExtTreeGridCellNode * pNode,
	const CExtGridDataSortOrder & _gdso,
	CExtTreeGridDataProvider::ITreeDataProviderEvents * pDPE
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pCellRoot );
	if( _gdso.IsEmpty() )
		return true;
LONG nSize = LONG( pNode->TreeNodeGetChildCount() );
	if( nSize < 2 )
		return true;
	if( ! _Tree_SortOrderCheck( _gdso ) )
		return false;
	if( pDPE != NULL )
		pDPE->OnTreeDataProviderSortEnter();
bool bRetVal = true;
LONG nSwapCounter = 0L;
	for( ; true; )
	{
		if( ! _Tree_SortStep(
				pNode,
				pDPE,
				_gdso,
				nSwapCounter,
				0L,
				nSize - 1L
				)
			)
		{
			bRetVal = false;
			break;
		}
		if( nSwapCounter == 0L )
			break;
		nSwapCounter = 0L;
	}
	if( pDPE != NULL )
		pDPE->OnTreeDataProviderSortLeave();
	return bRetVal;
}

bool CExtTreeGridDataProvider::_Tree_SortStep(
	CExtTreeGridCellNode * pNodeParent,
	CExtTreeGridDataProvider::ITreeDataProviderEvents * pDPE,
	const CExtGridDataSortOrder & _gdso,
	LONG & nSwapCounter,
	LONG nLow,
	LONG nHigh
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( ! _gdso.IsEmpty() );
// 	__EXT_DEBUG_GRID_ASSERT( nLow <= nHigh );
// 	if( nLow == nHigh )
// 		return true;
// 	if( nLow > nHigh )
// 	{
// 		//__EXT_DEBUG_GRID_ASSERT( FALSE );
// 		return false;
// 	} // if( nLow > nHigh )
 	if( nLow > nHigh )
		return true;
LONG nViewMin = nLow, nViewMax = nHigh, nViewMiddle = ( nLow + nHigh ) / 2L;
	for( ; nViewMin <= nViewMax; )
	{
		for( ;	( nViewMin < nHigh )
			&&	( _Tree_SortCmpSeries( nViewMin, nViewMiddle, pNodeParent, _gdso ) < 0 );
			++ nViewMin
			);
		for( ;	( nViewMax > nLow )
			&&	( _Tree_SortCmpSeries( nViewMax, nViewMiddle, pNodeParent, _gdso ) > 0 );
			-- nViewMax
			);
		if( nViewMin <= nViewMax )
		{
			if(		nViewMin != nViewMax
				&&	_Tree_SortCmpSeries(
						nViewMin,
						nViewMax,
						pNodeParent, 
						_gdso
						) != 0
				)
			{
				if( nViewMiddle == nViewMin )
					nViewMiddle = nViewMax;
				else if( nViewMiddle == nViewMax )
					nViewMiddle = nViewMin;
				if( ! _Tree_SortSwapSeries(
						nViewMin,
						nViewMax,
						nSwapCounter,
						pDPE,
						pNodeParent
						)
					)
				{
					__EXT_DEBUG_GRID_ASSERT( FALSE );
					return false;
				}
			} // if( nViewMin != nViewMax && ...
			++ nViewMin;
			-- nViewMax;
		}
	}
	if( nLow < nViewMax )
	{
		if( ! _Tree_SortStep(
				pNodeParent,
				pDPE,
				_gdso,
				nSwapCounter,
				nLow,
				nViewMax
				)
			)
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return false;
		}
	} // if( nLow < nViewMax )
	if( nViewMin < nHigh )
	{
		if( ! _Tree_SortStep(
				pNodeParent,
				pDPE,
				_gdso,
				nSwapCounter,
				nViewMin,
				nHigh
				)
			)
		{
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return false;
		}
	} // if( nViewMin < nHigh )
	return true;
}

int CExtTreeGridDataProvider::_Tree_SortCmpCells(
	CExtGridCell * pCell_1,
	CExtGridCell * pCell_2
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( pCell_1 == NULL )
	{
		if( pCell_2 == NULL )
			return 0;
		__EXT_DEBUG_GRID_ASSERT_VALID( pCell_2 );
		return -1;
	} // if( pCell_1 == NULL )
	else
	{
		if( pCell_2 == NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pCell_1 );
			return 1;
		} // if( pCell_2 == NULL )
	} // else from if( pCell_1 == NULL )
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell_1 );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell_2 );
int nRetVal = pCell_1->CompareEx( CExtGridCell::__EGCCT_TREE_GRID_SORTING, *pCell_2 );
	return nRetVal;
}

int CExtTreeGridDataProvider::_Tree_SortCmpSeries(
	LONG nRowNo1,
	LONG nRowNo2,
	CExtTreeGridCellNode * pNodeParent,
	const CExtGridDataSortOrder & _gdso
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( ! _gdso.IsEmpty() );
	__EXT_DEBUG_GRID_ASSERT( nRowNo1 >= 0L );
	__EXT_DEBUG_GRID_ASSERT( nRowNo2 >= 0L );
	if( nRowNo1 == nRowNo2 )
		return 0;
CExtTreeGridCellNode * pNode1 = pNodeParent->TreeNodeGetChildAt( ULONG(nRowNo1) );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode1 );
CExtTreeGridCellNode * pNode2 = pNodeParent->TreeNodeGetChildAt( ULONG(nRowNo2) );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode2 );
LONG nCount = (LONG)_gdso.m_arrItems.GetSize();
	__EXT_DEBUG_GRID_ASSERT( nCount > 0L );
int nPointerRetVal = 0;
	for( LONG i = 0L; i < nCount; i++ )
	{
		const CExtGridDataSortOrder::ITEM_INFO & _itemSrc =
			_gdso.m_arrItems[ i ];
		int nRetVal = 0;
		CExtGridCell * pCell_1 =
			ExtractGridCell(
				pNode1,
				_itemSrc.m_nRowColNo
				);
		CExtGridCell * pCell_2 =
			ExtractGridCell(
				pNode2,
				_itemSrc.m_nRowColNo
				);
		nRetVal =
			_itemSrc.m_bAscending
				? _Tree_SortCmpCells( pCell_1, pCell_2 )
				: _Tree_SortCmpCells( pCell_2, pCell_1 );
		if( i == 0L && nRetVal == 0 )
		{
			// the following statements will will provide a better
			// sort order behavior after double re-sortings
			if( __EXT_MFC_LONG_PTR(pCell_1) < __EXT_MFC_LONG_PTR(pCell_2) )
				nPointerRetVal = -1;
			else if( __EXT_MFC_LONG_PTR(pCell_1) > __EXT_MFC_LONG_PTR(pCell_2) )
				nPointerRetVal = 1;
		} // if( i == 0L && nRetVal == 0 )
		if( nRetVal != 0 )
			return nRetVal;
	} // for( LONG i = 0L; i < nCount; i++ )
	return nPointerRetVal;
}

bool CExtTreeGridDataProvider::_Tree_SortSwapSeries(
	LONG nRowNo1,
	LONG nRowNo2,
	LONG & nSwapCounter,
	CExtTreeGridDataProvider::ITreeDataProviderEvents * pDPE,
	CExtTreeGridCellNode * pNodeParent
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nRowNo1 >= 0L );
	__EXT_DEBUG_GRID_ASSERT( nRowNo2 >= 0L );
	__EXT_DEBUG_GRID_ASSERT( nRowNo1 != nRowNo2 );
	__EXT_DEBUG_GRID_ASSERT( nSwapCounter >= 0L );
CExtTreeGridCellNode * pNode1 = pNodeParent->TreeNodeGetChildAt( ULONG(nRowNo1) );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode1 );
CExtTreeGridCellNode * pNode2 = pNodeParent->TreeNodeGetChildAt( ULONG(nRowNo2) );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode2 );
	if( pDPE != NULL )
		pDPE->OnTreeDataProviderSwapSeries( *pNode1, *pNode2, nSwapCounter );
	nSwapCounter ++;
CExtTreeGridCellNode * pNodeCopyMove = ( nRowNo2 > nRowNo1 ) ? pNode2 : pNode1;
ULONG nIdxInsertBefore = ULONG( ( nRowNo2 > nRowNo1 ) ? nRowNo1 : nRowNo2 );
//CExtTreeGridCellNode * pNode3 = _Tree_NodeCopyMove( pNodeCopyMove, pNodeParent, nIdxInsertBefore, true, false );
//	if( pNode3 == NULL )
//		return false;
	__EXT_DEBUG_GRID_VERIFY( _Tree_NodeMoveImpl( pNodeCopyMove, pNodeParent, nIdxInsertBefore, TVE_TOGGLE ) );
	return true;
}

ULONG CExtTreeGridDataProvider::_Tree_NodeRemove(
	CExtTreeGridCellNode * pNode,
	bool bChildrenOnly // = false
	) // returns count of removed items
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pCellRoot );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nReservedRowCount = 0;
	 _DP.CacheReservedCountsGet( NULL, &nReservedRowCount );
ULONG nAdjustOffset = 1;
	if( LPVOID(pNode) == LPVOID(m_pCellRoot) )
	{
		bChildrenOnly = true;
		nAdjustOffset = 0;
	} // if( LPVOID(pNode) == LPVOID(m_pCellRoot) )

bool bNodeIsHidden = pNode->TreeNodeHiddenGet();
	if( bNodeIsHidden )
		pNode->TreeNodeHiddenSet( *this, false );
ULONG	nCountRemoved = 0,
		nIdx,
		nChildrenCount = pNode->TreeNodeGetChildCount();
	for( nIdx = 0; nIdx < nChildrenCount; nIdx++ )
	{
		CExtTreeGridCellNode * pChildNode =
			pNode->TreeNodeGetChildAt( nIdx );
		__EXT_DEBUG_GRID_ASSERT_VALID( pChildNode );
		ULONG nSubCountRemoved = _Tree_NodeRemove( pChildNode, true );
		nCountRemoved += nSubCountRemoved;
	} // for( nIdx = 0; nIdx < nChildrenCount; nIdx++ )
//	if( bNodeIsHidden && bChildrenOnly )
//		pNode->TreeNodeHiddenSet( *this, true );
ULONG nContentWeight = pNode->_ContentWeight_Get( false );
ULONG nOffset = pNode->TreeNodeCalcOffset( false );
bool bDisplayed = pNode->TreeNodeIsDisplayed();
bool bExpanded = pNode->TreeNodeIsExpanded();
ULONG nVisibleOffset = 0;
	if( bDisplayed )
		nVisibleOffset = pNode->TreeNodeCalcOffset( true, false );
	if( nContentWeight > 0 )
	{
// #ifdef _DEBUG
// 		if( LPVOID(pNode) != LPVOID(m_pCellRoot) )
// 		{
// 			CExtTreeGridCellNode * pDebugTestNode = m_arrGridRef.GetAt( nOffset );
// 			__EXT_DEBUG_GRID_ASSERT_VALID( pDebugTestNode );
// 			__EXT_DEBUG_GRID_ASSERT( LPCVOID(pDebugTestNode) == LPCVOID(pNode) );
// 		} // if( LPVOID(pNode) == LPVOID(m_pCellRoot) )
// #endif // _DEBUG
		m_arrGridRef.RemoveAt(
			nOffset + nAdjustOffset,
			nContentWeight
			);
		if( bDisplayed && bExpanded )
		{
#ifdef _DEBUG
			if( LPVOID(pNode) != LPVOID(m_pCellRoot) )
			{
				CExtTreeGridCellNode * pDebugTestNode = m_arrGridVis.GetAt( nVisibleOffset );
				pDebugTestNode;
				__EXT_DEBUG_GRID_ASSERT_VALID( pDebugTestNode );
				__EXT_DEBUG_GRID_ASSERT( LPCVOID(pDebugTestNode) == LPCVOID(pNode) );
			} // if( LPVOID(pNode) == LPVOID(m_pCellRoot) )
#endif // _DEBUG
			ULONG nVisibleContentWeight =
				pNode->_ContentWeight_CalcVisible();
			m_arrGridVis.RemoveAt(
				nVisibleOffset + nAdjustOffset,
				nVisibleContentWeight
				);
			pNode->_ContentWeight_Decrement( nVisibleContentWeight, false );
			pNode->m_arrChildren.RemoveAll();
		} // if( bDisplayed && bExpanded )
		else
		{
			pNode->m_arrChildren.RemoveAll();
			pNode->_ContentWeight_DecrementNonExpanded( nContentWeight );
			pNode->_ContentWeight_Adjust();
		} // else from if( bDisplayed && bExpanded )
		__EXT_DEBUG_GRID_ASSERT( pNode->_ContentWeight_Get(true) == 0 );
		__EXT_DEBUG_GRID_ASSERT( pNode->_ContentWeight_Get(false) == 0 );
		nCountRemoved += nContentWeight;
		__EXT_DEBUG_GRID_VERIFY( _DP.RowRemove( nOffset + nAdjustOffset + nReservedRowCount, nContentWeight ) );
	} // if( nContentWeight > 0 )
	if( ! bChildrenOnly )
	{
		CExtTreeGridCellNode * pNodeNext = pNode->m_pNodeNext;
		CExtTreeGridCellNode * pNodePrev = pNode->m_pNodePrev;
		CExtTreeGridCellNode * pNodeParent = pNode->m_pNodeParent;
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodeParent );
		ULONG nSiblingIdx = pNode->TreeNodeGetSiblingIndex();
		if( pNodeNext != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pNodeNext );
			__EXT_DEBUG_GRID_ASSERT( LPCVOID(pNodeNext->m_pNodePrev) == LPCVOID(pNode) );
			pNodeNext->m_pNodePrev = pNodePrev;
		} // if( pNodeNext != NULL )
		if( pNodePrev != NULL )
		{
			__EXT_DEBUG_GRID_ASSERT_VALID( pNodePrev );
			__EXT_DEBUG_GRID_ASSERT( LPCVOID(pNodePrev->m_pNodeNext) == LPCVOID(pNode) );
			pNodePrev->m_pNodeNext = pNodeNext;
		} // if( pNodePrev != NULL )
		pNodeParent->m_arrChildren.RemoveAt( nSiblingIdx );
		ULONG nResetIdx = nSiblingIdx, nResetCnt = ULONG( pNodeParent->m_arrChildren.GetSize() );
		for( ; nResetIdx < nResetCnt; nResetIdx++ )
		{
			CExtTreeGridCellNode * pNode = (CExtTreeGridCellNode *)
				pNodeParent->m_arrChildren.GetAt( nResetIdx );
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
			__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtTreeGridCellNode, pNode );
			pNode->m_nOptIndex = nResetIdx;
		}
		if( bDisplayed )
		{
#ifdef _DEBUG
			if( LPVOID(pNode) != LPVOID(m_pCellRoot) )
			{
				CExtTreeGridCellNode * pDebugTestNode = m_arrGridVis.GetAt( nVisibleOffset );
				pDebugTestNode;
				__EXT_DEBUG_GRID_ASSERT_VALID( pDebugTestNode );
				__EXT_DEBUG_GRID_ASSERT( LPCVOID(pDebugTestNode) == LPCVOID(pNode) );
			} // if( LPVOID(pNode) == LPVOID(m_pCellRoot) )
#endif // _DEBUG
			m_arrGridVis.RemoveAt( nVisibleOffset );
			pNodeParent->_ContentWeight_Decrement( 1, false );
		} // if( bDisplayed && bExpanded )
		else
		{
			pNodeParent->_ContentWeight_DecrementNonExpanded( 1 );
			pNodeParent->_ContentWeight_Adjust();
		} // else from if( bDisplayed && bExpanded )
		__EXT_DEBUG_GRID_VERIFY( _DP.RowRemove( nOffset + nReservedRowCount ) );
		nCountRemoved ++;
	} // if( ! bChildrenOnly )
	return nCountRemoved;
}

CRuntimeClass * CExtTreeGridDataProvider::_Tree_NodeGetRTC()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_pTreeNodeDefaultRTC;
}

void CExtTreeGridDataProvider::_Tree_NodeAdjustProps(
	CExtTreeGridCellNode * pNode
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtTreeGridCellNode, pNode );
	//pNode->TreeNodeIndentPxSet( m_nIndentPxDefault );
	pNode;
}

bool CExtTreeGridDataProvider::TreeNodeCopyMoveTest(
	const CExtTreeGridCellNode * pNodeCopyMove,
	const CExtTreeGridCellNode * pNodeNewParent, // if NULL - root
	ULONG nIdxInsertBefore, // = (ULONG(-1L)) // if (ULONG(-1L)) - insert to end
	bool bEnableCopyingIntoInnerBranch, // = true
	bool * p_bCopyingIntoInnerBranch // = NULL
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridDataProvider * > ( this ) ) ->
		_Tree_NodeCopyMoveTest(
			const_cast < CExtTreeGridCellNode * > ( pNodeCopyMove ),
			const_cast < CExtTreeGridCellNode * > ( pNodeNewParent ),
			nIdxInsertBefore,
			bEnableCopyingIntoInnerBranch,
			p_bCopyingIntoInnerBranch
			);
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::TreeNodeCopyMove(
	CExtTreeGridCellNode * pNodeCopyMove,
	CExtTreeGridCellNode * pNodeNewParent, // if NULL - root
	ULONG nIdxInsertBefore, // = (ULONG(-1L)) // if (ULONG(-1L)) - insert to end
	bool bMove, // = true
	bool bCloneCellObjects, // = true
	INT nExpandAction, // = TVE_TOGGLE // TVE_TOGGLE in this case - keep expanded state
	bool bEnableCopyingIntoInnerBranch, // = true
	bool * p_bCopyingIntoInnerBranch // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		_Tree_NodeCopyMove(
			pNodeCopyMove,
			pNodeNewParent,
			nIdxInsertBefore,
			bMove,
			true,
			bCloneCellObjects,
			nExpandAction,
			bEnableCopyingIntoInnerBranch,
			p_bCopyingIntoInnerBranch
			);
}

bool  CExtTreeGridDataProvider::TreeNodeSortChildren(
	CExtTreeGridCellNode * pNode,
	const CExtGridDataSortOrder & _gdso,
	CExtTreeGridDataProvider::ITreeDataProviderEvents * pDPE
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return _Tree_NodeSortChildren( pNode, _gdso, pDPE );
}

ULONG CExtTreeGridDataProvider::TreeNodeRemove(
	CExtTreeGridCellNode * pNode,
	bool bChildrenOnly // = false
	) // returns count of removed items
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	return _Tree_NodeRemove( pNode, bChildrenOnly );
}

bool CExtTreeGridDataProvider::TreeNodeExpand(
	CExtTreeGridCellNode * pNode,
	INT nActionTVE // = TVE_TOGGLE // TVE_COLLAPSE, TVE_EXPAND, TVE_TOGGLE only
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT(
			nActionTVE == TVE_COLLAPSE
		||	nActionTVE == TVE_EXPAND
		||	nActionTVE == TVE_TOGGLE
		);
	return _Tree_NodeExpand( pNode, nActionTVE );
}

ULONG CExtTreeGridDataProvider::_Tree_GetRowIndentPx( ULONG nRowNo )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nRowNo < RowCountGet() );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nRowCountReservedForTree = _Tree_ReservedRowsGet();
//	nRowNo += nRowCountReservedForTree;
ULONG nReservedRowCount = 0;
	_DP.CacheReservedCountsGet( NULL, &nReservedRowCount );
	nRowNo += nReservedRowCount - nRowCountReservedForTree;
	nRowNo = _Tree_MapRowToCache( nRowNo );
//	nRowNo += nRowCountReservedForTree;
CExtTreeGridCellNode * pNode = STATIC_DOWNCAST( CExtTreeGridCellNode, _DP.CellGet( 0, nRowNo ) );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
CExtTreeGridCellNode * pNodeRoot = _Tree_NodeGetRoot();
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeRoot );
ULONG nIndentPx = pNode->TreeNodeIndentPxGet();
	if( nIndentPx == ULONG(-1L) )
		nIndentPx = m_nIndentPxDefault;
	for( ; true; )
	{
		pNode = pNode->TreeNodeGetParent();
		__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		if( LPCVOID(pNodeRoot) == LPCVOID(pNode) )
			break;
		ULONG nLevel = pNode->TreeNodeIndentPxGet();
		if( nLevel == ULONG(-1L) )
			nLevel = m_nIndentPxDefault;
		nIndentPx += nLevel;
	} // for( ; true; )
	return nIndentPx;
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::_Tree_NodeInsert(
	CExtTreeGridCellNode * pNodeParent,
	ULONG nIdxInsertBefore, // = (ULONG(-1L)) // if (ULONG(-1L)) - insert to end
	ULONG nInsertCount // = 1
	) // returns pointer to first inserted
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( nInsertCount == 0 )
		return NULL;
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
CExtTreeGridCellNode * pNodeRoot = _Tree_NodeGetRoot();
	if( pNodeParent == NULL )
	{
		pNodeParent = pNodeRoot;
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodeParent );
	} // if( pNodeParent == NULL )
	else
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodeParent );
		__EXT_DEBUG_GRID_ASSERT( LPCVOID(pNodeParent->DataProviderGet()) == LPCVOID(&_DP) );
	} // else from if( pNodeParent == NULL )
ULONG nParentChildCount = pNodeParent->TreeNodeGetChildCount();
	if(		nIdxInsertBefore == ULONG(-1L)
		||	nIdxInsertBefore > nParentChildCount
		)
		nIdxInsertBefore = nParentChildCount;

ULONG nReservedRowCount = 0;
	 _DP.CacheReservedCountsGet( NULL, &nReservedRowCount );
ULONG nAdditionalOffsetTotal = 0;
ULONG nAdditionalOffsetVisible = 0;
ULONG nVisibleOffset = 0;
ULONG nInsertOffset = nReservedRowCount;
CExtTreeGridCellNode * pNodeCompute = pNodeParent;
	if( nIdxInsertBefore != 0 )
	{
		__EXT_DEBUG_GRID_ASSERT( nIdxInsertBefore <= nParentChildCount );
		if( nIdxInsertBefore == nParentChildCount )
		{
			nAdditionalOffsetTotal =
				pNodeParent->_ContentWeight_Get( false );
			nAdditionalOffsetVisible =
				pNodeParent->_ContentWeight_Get( true );
			if( pNodeParent != pNodeRoot )
			{
				nAdditionalOffsetTotal ++;
				nAdditionalOffsetVisible ++;
			}
		}
		else
			pNodeCompute = pNodeParent->TreeNodeGetChildAt( nIdxInsertBefore );
	}
	else
	{
		if( pNodeParent != pNodeRoot )
		{
			nAdditionalOffsetTotal = 1;
			nAdditionalOffsetVisible = 1;
		}
	}
	nInsertOffset += pNodeCompute->TreeNodeCalcOffset( false );
	if(		pNodeParent->TreeNodeIsDisplayed()
		&&	pNodeParent->TreeNodeIsExpanded()
		)
		nVisibleOffset += pNodeCompute->TreeNodeCalcOffset( true, false ) + nAdditionalOffsetVisible;
	else
		nVisibleOffset = ULONG(-1L);
	nInsertOffset += nAdditionalOffsetTotal;

	if( ! _DP.RowInsert( nInsertOffset, nInsertCount ) )
		return NULL;

CExtTreeGridCellNode * pNodeRetVal = NULL;
ULONG nIdx = 0;
	for( ; nIdx < nInsertCount; nIdx++ )
	{
		ULONG nEffectiveIdx = nInsertOffset + nIdx;
		CExtTreeGridCellNode * pNodeCurr =
			STATIC_DOWNCAST( CExtTreeGridCellNode, _DP.CellGet( 0, nEffectiveIdx, _Tree_NodeGetRTC() ) );
		__EXT_DEBUG_GRID_ASSERT_VALID( pNodeCurr );
		_Tree_NodeAdjustProps( pNodeCurr );
		m_arrGridRef.InsertAt( nEffectiveIdx - nReservedRowCount, pNodeCurr );
		if( nVisibleOffset != ULONG(-1L) )
			m_arrGridVis.InsertAt( nVisibleOffset + nIdx, pNodeCurr );
		pNodeCurr->m_pNodeParent = pNodeParent;
		ULONG nSiblingIdx = nIdxInsertBefore + nIdx;
		pNodeParent->m_arrChildren.InsertAt( nSiblingIdx, pNodeCurr, 1 );
		pNodeCurr->m_nOptIndex = nSiblingIdx;
		if( nIdx == 0 )
			pNodeRetVal = pNodeCurr;
		CExtTreeGridCellNode * pNodePrev = NULL, * pNodeNext = NULL;
		if( nSiblingIdx > 0 )
		{
			pNodePrev = pNodeParent->m_arrChildren.GetAt( nSiblingIdx - 1 );
			__EXT_DEBUG_GRID_ASSERT_VALID( pNodePrev );
		} // if( nSiblingIdx > 0 )
		if( nSiblingIdx < ULONG(pNodeParent->m_arrChildren.GetSize()-1) )
		{
			pNodeNext = pNodeParent->m_arrChildren.GetAt( nSiblingIdx + 1 );
			__EXT_DEBUG_GRID_ASSERT_VALID( pNodeNext );
		} // if( nSiblingIdx < ULONG(pNodeParent->m_arrChildren.GetSize()-1) )
		if( pNodePrev != NULL )
		{
			pNodePrev->m_pNodeNext = pNodeCurr;
			pNodeCurr->m_pNodePrev = pNodePrev;
		} // if( pNodePrev != NULL )
		if( pNodeNext != NULL )
		{
			pNodeNext->m_pNodePrev = pNodeCurr;
			pNodeCurr->m_pNodeNext = pNodeNext;
		} // if( pNodeNext != NULL )
	} // for( ; nIdx < nInsertCount; nIdx++ )

ULONG nResetIdx, nResetCnt = ULONG(pNodeParent->m_arrChildren.GetSize());
	for( nResetIdx = nIdxInsertBefore; nResetIdx < nResetCnt; nResetIdx++ )
	{
		CExtTreeGridCellNode * pNode = (CExtTreeGridCellNode *) pNodeParent->m_arrChildren.GetAt( nResetIdx );
		__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtTreeGridCellNode, pNode );
		pNode->m_nOptIndex = nResetIdx;
	} // for( nResetIdx = nIdxInsertBefore; nResetIdx < nResetCnt; nResetIdx++ )
	
	if( nVisibleOffset != ULONG(-1L) )
		pNodeParent->_ContentWeight_Increment( nInsertCount, false );
	else
		pNodeParent->_ContentWeight_IncrementNonExpanded( nInsertCount );
	return pNodeRetVal;
}

bool CExtTreeGridDataProvider::_Tree_NodeExpand(
	CExtTreeGridCellNode * pNode,
	INT nActionTVE // = TVE_TOGGLE // TVE_COLLAPSE, TVE_EXPAND, TVE_TOGGLE only
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	__EXT_DEBUG_GRID_ASSERT(
			nActionTVE == TVE_COLLAPSE
		||	nActionTVE == TVE_EXPAND
		||	nActionTVE == TVE_TOGGLE
		);
bool bExpanded = pNode->TreeNodeIsExpanded();
	if( nActionTVE == TVE_TOGGLE )
	{
		if( bExpanded )
			nActionTVE = TVE_COLLAPSE;
		else
			nActionTVE = TVE_EXPAND;
	} // if( nActionTVE == TVE_TOGGLE )
	if( nActionTVE == TVE_EXPAND )
	{
		if( bExpanded )
			return false;
		pNode->TreeNodeMarkExpanded( true );
		ULONG nWeightVisible = pNode->_ContentWeight_CalcVisible();
		if( pNode->TreeNodeIsDisplayed() )
		{
			ULONG nOffset = pNode->TreeNodeCalcOffset( true, false );
//			__EXT_DEBUG_GRID_ASSERT( nWeightVisible >= pNode->TreeNodeGetChildCount() );
			if( nWeightVisible != 0 )
			{
				m_arrGridVis.InsertAt( ++ nOffset, NULL, nWeightVisible );
				pNode->_Content_FillVisibleArray( m_arrGridVis, nOffset );
			} // if( nWeightVisible != 0 )
		} // if( pNode->TreeNodeIsDisplayed() )
		pNode->_ContentWeight_Increment( nWeightVisible, true );
	} // if( nActionTVE == TVE_EXPAND )
	else
	{
		if( ! bExpanded )
			return false;
		ULONG nWeightVisible =
			//pNode->_ContentWeight_Get( true );
			pNode->_ContentWeight_CalcVisible();
		pNode->TreeNodeMarkExpanded( false );
		if( pNode->TreeNodeIsDisplayed() )
		{
			ULONG nOffset = pNode->TreeNodeCalcOffset( true, false );
			if( nWeightVisible != 0 )
			{
				m_arrGridVis.RemoveAt( nOffset + 1, nWeightVisible );
			} // if( nWeightVisible != 0 )
		} // if( pNode->TreeNodeIsDisplayed() )
		pNode->_ContentWeight_Decrement( nWeightVisible, true );
	} // else from if( nActionTVE == TVE_EXPAND )
	return true;
}

ULONG CExtTreeGridDataProvider::_Tree_GetDisplayedCount() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ULONG( m_arrGridVis.GetSize() );
}

ULONG CExtTreeGridDataProvider::_Tree_MapColToCache( ULONG nColNo ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return nColNo + _Tree_ReservedColumnsGet();
}

ULONG CExtTreeGridDataProvider::_Tree_MapRowToCache( ULONG nRowNo ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nReservedRowCount = 0;
	 _DP.CacheReservedCountsGet( NULL, &nReservedRowCount );
ULONG nRowCountReservedForTree = _Tree_ReservedRowsGet();
	__EXT_DEBUG_GRID_ASSERT( nReservedRowCount >= nRowCountReservedForTree );
	nReservedRowCount -= nRowCountReservedForTree;
ULONG nOffset = 0;
	if( nRowNo >= nReservedRowCount )
	{
		nRowNo -= nReservedRowCount;
		__EXT_DEBUG_GRID_ASSERT( nRowNo < ULONG(m_arrGridVis.GetSize()) );
		CExtTreeGridCellNode * pNode = m_arrGridVis.GetAt( nRowNo );
		__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		nOffset = pNode->TreeNodeCalcOffset( false, true );
		nOffset += nReservedRowCount;
	} // if( nRowNo >= nReservedRowCount )
	else
		nOffset = nRowNo;
	return nOffset + nRowCountReservedForTree;
}

ULONG CExtTreeGridDataProvider::_Tree_ReservedColumnsGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_nReservedColCount;
}

bool CExtTreeGridDataProvider::_Tree_ReservedColumnsSet( ULONG nReservedCount )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nReservedCount >= 1 ); // including tree structure column
	if( nReservedCount == 0 )
		return false;
	if( m_pCellRoot == NULL )
		return false; // not initialized yet
	if( m_nReservedColCount == nReservedCount )
		return true;
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pCellRoot );
ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
	m_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
	if( m_nReservedColCount > nReservedCount )
	{
		ULONG nCountToRemove = m_nReservedColCount - nReservedCount;
		if( ! m_DP.ColumnRemove( nReservedCount, nCountToRemove ) )
			return false;
		m_DP.CacheReserveForOuterCells(
			nReservedColumnCount - nCountToRemove,
			nReservedRowCount
			);
	} // if( m_nReservedColCount > nReservedCount )
	else
	{
		ULONG nCountToInsert = nReservedCount - m_nReservedColCount;
		if( ! m_DP.ColumnInsert( m_nReservedColCount, nCountToInsert ) )
			return false;
		m_DP.CacheReserveForOuterCells(
			nReservedColumnCount + nCountToInsert,
			nReservedRowCount
			);
	} // else from if( m_nReservedColCount > nReservedCount )
	m_nReservedColCount = nReservedCount;
	return true;
}

bool CExtTreeGridDataProvider::_Tree_ReservedColumnsMoveToReserve(
	ULONG nSrcIdx,
	ULONG nSrcCount,
	ULONG nDstIdx
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nDstIdx >= 1 );
	if( nDstIdx == 0 )
		return false;
ULONG nReservedColCount = _Tree_ReservedColumnsGet();
	__EXT_DEBUG_GRID_ASSERT( nReservedColCount >= 1 ); // including tree structure
	__EXT_DEBUG_GRID_ASSERT( nDstIdx <= nReservedColCount );
	if( nDstIdx > nReservedColCount )
		return false;
ULONG nPublishedColCount = ColumnCountGet();
	__EXT_DEBUG_GRID_ASSERT( nSrcIdx < nPublishedColCount );
	if( nSrcIdx >= nPublishedColCount )
		return false;
	__EXT_DEBUG_GRID_ASSERT( ( nSrcIdx + nSrcCount ) <= nPublishedColCount );
	if( ( nSrcIdx + nSrcCount ) > nPublishedColCount )
		return false;
	if( nSrcCount == 0 )
		return true;
ULONG nSrcIndex = nSrcIdx, nDstIndex = nDstIdx, nIdx = 0;
ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
	m_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
	for( ; nIdx < nSrcCount; nSrcIndex ++, nDstIndex ++, nIdx++ )
		m_DP.SwapDroppedSeries(
			true,
			nSrcIndex + nReservedColumnCount,
			nDstIndex,
			NULL
			);
	m_nReservedColCount += nSrcCount;
//ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
//	m_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
	m_DP.CacheReserveForOuterCells(
		nReservedColumnCount + nSrcCount,
		nReservedRowCount
		);
	return true;
}

bool CExtTreeGridDataProvider::_Tree_ReservedColumnsMoveFromReserve(
	ULONG nSrcIdx,
	ULONG nSrcCount,
	ULONG nDstIdx
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nSrcIdx >= 1 );
	if( nSrcIdx == 0 )
		return false;
ULONG nReservedColCount = _Tree_ReservedColumnsGet();
	__EXT_DEBUG_GRID_ASSERT( nReservedColCount >= 1 ); // including tree structure
	__EXT_DEBUG_GRID_ASSERT( nSrcIdx < nReservedColCount );
	if( nSrcIdx >= nReservedColCount )
		return false;
	__EXT_DEBUG_GRID_ASSERT( ( nSrcIdx + nSrcCount ) <= nReservedColCount );
	if( ( nSrcIdx + nSrcCount ) > nReservedColCount )
		return false;
	if( nSrcCount == 0 )
		return true;
ULONG nPublishedColCount = ColumnCountGet();
	__EXT_DEBUG_GRID_ASSERT( nDstIdx <= nPublishedColCount );
	if( nDstIdx > nPublishedColCount )
		return false;
ULONG nSrcIndex = nSrcIdx, nDstIndex = nDstIdx, nIdx = 0;
ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
	m_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
	for( ; nIdx < nSrcCount; /*nSrcIndex ++, nDstIndex ++,*/ nIdx++ )
		m_DP.SwapDroppedSeries(
			true,
			nSrcIndex,
			nDstIndex + nReservedColumnCount,
			NULL
			);
	m_nReservedColCount -= nSrcCount;
	__EXT_DEBUG_GRID_ASSERT( m_nReservedColCount >= 1 );
//ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
//	m_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
	m_DP.CacheReserveForOuterCells(
		nReservedColumnCount - nSrcCount,
		nReservedRowCount
		);
	return true;
}

ULONG CExtTreeGridDataProvider::_Tree_ReservedRowsGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return m_nReservedRowCount;
}

bool CExtTreeGridDataProvider::_Tree_ReservedRowsSet( ULONG nReservedCount )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nReservedCount >= 1 ); // including tree structure row
	if( nReservedCount == 0 )
		return false;
	if( m_pCellRoot == NULL )
		return false; // not initialized yet
	if( m_nReservedRowCount == nReservedCount )
		return true;
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pCellRoot );
	if( m_nReservedRowCount > nReservedCount )
	{
		ULONG nCountToRemove = m_nReservedRowCount - nReservedCount;
		if( ! m_DP.RowRemove( nReservedCount, nCountToRemove ) )
			return false;
	} // if( m_nReservedRowCount > nReservedCount )
	else
	{
		ULONG nCountToInsert = nReservedCount - m_nReservedRowCount;
		if( ! m_DP.RowInsert( m_nReservedRowCount, nCountToInsert ) )
			return false;
	} // else from if( m_nReservedRowCount > nReservedCount )
	m_nReservedRowCount = nReservedCount;
	return true;
}

CExtGridDataProvider & CExtTreeGridDataProvider::_Tree_GetCacheDP()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_pCellRoot == NULL )
	{
		// reserve range for root cell and tree structure
		ULONG nReservedColCount = _Tree_ReservedColumnsGet();
		ULONG nReservedRowCount = _Tree_ReservedRowsGet();
		__EXT_DEBUG_GRID_VERIFY( m_DP.ColumnInsert( 0, nReservedColCount ) );
		__EXT_DEBUG_GRID_VERIFY( m_DP.RowInsert( 0, nReservedRowCount ) );
		m_DP.CacheReserveForOuterCells(
			nReservedColCount,
			nReservedRowCount
			);
		m_pCellRoot =
			STATIC_DOWNCAST(
				CExtTreeGridCellNode,
				m_DP.CellGet(
					0,
					0,
					_Tree_NodeGetRTC()
					)
				);
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pCellRoot );
		__EXT_DEBUG_GRID_ASSERT_KINDOF( CExtTreeGridCellNode, m_pCellRoot );
		m_pCellRoot->TreeNodeMarkExpanded( true );
		_Tree_NodeAdjustProps( m_pCellRoot );
	} // if( m_pCellRoot == NULL )
	return m_DP;
}

const CExtGridDataProvider & CExtTreeGridDataProvider::_Tree_GetCacheDP() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ( const_cast < CExtTreeGridDataProvider * > ( this ) ) -> _Tree_GetCacheDP();
}

#ifdef _DEBUG

void CExtTreeGridDataProvider::AssertValid() const
{
	CExtGridDataProvider::AssertValid();
	m_DP.AssertValid();
	__EXT_DEBUG_GRID_ASSERT( m_nReservedColCount >= 1L );
	__EXT_DEBUG_GRID_ASSERT( m_nReservedRowCount >= 1L );
}

void CExtTreeGridDataProvider::Dump( CDumpContext & dc ) const
{
	CExtGridDataProvider::Dump( dc );
	_Tree_GetCacheDP().Dump( dc );
}

#endif // _DEBUG

__EXT_MFC_SAFE_LPTSTR CExtTreeGridDataProvider::StringAlloc(
	INT nCharCountIncZT
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.StringAlloc( nCharCountIncZT );
}

void CExtTreeGridDataProvider::StringFree(
	__EXT_MFC_SAFE_LPTSTR strToFree
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	_DP.StringFree( strToFree );
}

bool CExtTreeGridDataProvider::ColumnInsert(
	ULONG nColNo,
	ULONG nInsertCount // = 1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nColNo >= 0 );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.ColumnInsert( _Tree_MapColToCache( nColNo ), nInsertCount );
}

bool CExtTreeGridDataProvider::RowInsert(
	ULONG nRowNo,
	ULONG nInsertCount // = 1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nRowCountReservedInCache = 0;
	_DP.CacheReservedCountsGet( NULL, &nRowCountReservedInCache );
	if( nRowNo <= nRowCountReservedInCache )
		return _DP.RowInsert( nRowNo, nInsertCount );
	// this method must never be invoked in other case
	__EXT_DEBUG_GRID_ASSERT( FALSE );
	return false;
}

ULONG CExtTreeGridDataProvider::ColumnCountGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
	_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
//	return _DP.ColumnCountGet() - nReservedColumnCount; // - _Tree_ReservedColumnsGet();
	return _DP.ColumnCountGet() - _Tree_ReservedColumnsGet();
}

ULONG CExtTreeGridDataProvider::RowCountGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nRowCountReservedInCache = 0;
	_DP.CacheReservedCountsGet( NULL, &nRowCountReservedInCache );
ULONG nRowCountReservedForTree = _Tree_ReservedRowsGet();
	__EXT_DEBUG_GRID_ASSERT( nRowCountReservedInCache >= nRowCountReservedForTree );
ULONG nRowCountDisplayed = _Tree_GetDisplayedCount();
ULONG nRowCountGridOriented =
		nRowCountReservedInCache
		- nRowCountReservedForTree
		+ nRowCountDisplayed;
	return nRowCountGridOriented;
}

bool CExtTreeGridDataProvider::ColumnRemove(
	ULONG nColNo,
	ULONG nRemoveCount // = 1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.ColumnRemove( _Tree_MapColToCache( nColNo ), nRemoveCount );
}

bool CExtTreeGridDataProvider::RowRemove(
	ULONG nRowNo,
	ULONG nRemoveCount // = 1
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nRowNo;
	nRemoveCount;
	// this method must never be invoked
	__EXT_DEBUG_GRID_ASSERT( FALSE );
	return false;
}

void CExtTreeGridDataProvider::MinimizeMemoryUsage()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	_DP.MinimizeMemoryUsage();
}

bool CExtTreeGridDataProvider::RowDefaultValueBind(
	ULONG nRowNo, // = (ULONG(-1L)) // if (ULONG(-1L)) - default value for all rows
	CExtGridCell * pCell // = NULL // if NULL - remove default value
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	if( nRowNo != (ULONG(-1L)) )
		nRowNo = _Tree_MapRowToCache( nRowNo );
	return _DP.RowDefaultValueBind( nRowNo, pCell );
}

bool CExtTreeGridDataProvider::ColumnDefaultValueBind(
	ULONG nColNo, // = (ULONG(-1L)) // if (ULONG(-1L)) - default value for all columns
	CExtGridCell * pCell // = NULL // if NULL - remove default value
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	if( nColNo != (ULONG(-1L)) )
		nColNo = _Tree_MapColToCache( nColNo );
	return _DP.ColumnDefaultValueBind( nColNo, pCell );
}

void CExtTreeGridDataProvider::RowDefaultValueUnbindAll()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	_DP.RowDefaultValueUnbindAll();
}

void CExtTreeGridDataProvider::ColumnDefaultValueUnbindAll()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	_DP.ColumnDefaultValueUnbindAll();
}

CExtGridCell * CExtTreeGridDataProvider::RowDefaultValueGet(
	ULONG nRowNo
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nRowNo >= 0 );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.RowDefaultValueGet( _Tree_MapRowToCache( nRowNo ) );
}

CExtGridCell * CExtTreeGridDataProvider::ColumnDefaultValueGet(
	ULONG nColNo
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nColNo >= 0 );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.ColumnDefaultValueGet( _Tree_MapColToCache( nColNo ) );
}

CExtGridCell * CExtTreeGridDataProvider::ExtractGridCell(
	CExtTreeGridCellNode * pNode,
	ULONG nColNo,
	CRuntimeClass * pInitRTC, // = NULL
	bool bAutoFindValue, // = true
	bool bUseColumnDefaultValue // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nColNo >= 0 );
	if(		pNode == NULL
		||	pNode == TreeNodeGetRoot()
		)
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	__EXT_DEBUG_GRID_ASSERT( pNode->TreeNodeGetParent() != NULL );
ULONG nRowNo = pNode->TreeNodeCalcOffset( false );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
	_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
	nColNo += nReservedColumnCount;
	nRowNo += nReservedRowCount;
	return
		_DP.CellGet(
			nColNo,
			nRowNo,
			pInitRTC,
			bAutoFindValue,
			bUseColumnDefaultValue
			);
}

CExtGridCell * CExtTreeGridDataProvider::ExtractGridCellWithReplacing(
	CExtTreeGridCellNode * pNode,
	ULONG nColNo,
	const CExtGridCell * pCellNewValue, // = NULL // if NULL - empty existing cell values
	bool bReplace, // = false // false - assign to existing cell instances or column/row type created cells, true - create new cloned copies of pCellNewValue
	CRuntimeClass * pInitRTC // = NULL // runtime class for new cell instance (used if bReplace=false)
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nColNo >= 0 );
	if(		pNode == NULL
		||	pNode == TreeNodeGetRoot()
		)
		return NULL;
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	__EXT_DEBUG_GRID_ASSERT( pNode->TreeNodeGetParent() != NULL );
ULONG nRowNo = pNode->TreeNodeCalcOffset( false );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
	_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
	nColNo += nReservedColumnCount;
	nRowNo += nReservedRowCount;
	if( ! _DP.CellRangeSet(
			nColNo,
			nRowNo,
			1L,
			1L,
			pCellNewValue,
			bReplace,
			( pCellNewValue != NULL ) ? NULL : pInitRTC
			)
		)
		return NULL;
CExtGridCell * pCellInstantiated =
		_DP.CellGet(
			nColNo,
			nRowNo,
			( pCellNewValue != NULL ) ? NULL : pInitRTC,
			false
			);
#ifdef _DEBUG
	if( pCellInstantiated != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellInstantiated );
	}
#endif // _DEBUG
	return pCellInstantiated;
}

CExtGridCell * CExtTreeGridDataProvider::CellGet(
	ULONG nColNo,
	ULONG nRowNo,
	CRuntimeClass * pInitRTC, // = NULL
	bool bAutoFindValue, // = true // auto find row/column default value (only when pInitRTC=NULL)
	bool bUseColumnDefaultValue // = true // false - use row default value (only when bAutoFindValue=true)
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nColNo >= 0 );
	__EXT_DEBUG_GRID_ASSERT( nRowNo >= 0 );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	nColNo = _Tree_MapColToCache(  nColNo );
	nRowNo = _Tree_MapRowToCache( nRowNo );
	return
		_DP.CellGet(
			nColNo,
			nRowNo,
			pInitRTC,
			bAutoFindValue,
			bUseColumnDefaultValue
			);
}

bool CExtTreeGridDataProvider::CellRangeSet(
	ULONG nColNo,
	ULONG nRowNo,
	ULONG nColCount, // = 1L
	ULONG nRowCount, // = 1L
	const CExtGridCell * pCellNewValue, // = NULL // if NULL - empty existing cell values
	bool bReplace, // = false // false - assign to existing cell instances or column/row type created cells, true - create new cloned copies of pCellNewValue
	CRuntimeClass * pInitRTC, // = NULL // runtime class for new cell instance (used if bReplace=false)
	bool bAutoFindValue, // = true // auto find row/column default value (only when pInitRTC=NULL)
	bool bUseColumnDefaultValue, // = true // false - use row default value (only when bAutoFindValue=true)
	ULONG * p_nUpdatedCellCount // = NULL // returns count of really updated cells (zero also may be treated as success)
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nColNo >= 0 );
	__EXT_DEBUG_GRID_ASSERT( nRowNo >= 0 );
	__EXT_DEBUG_GRID_ASSERT( nColCount == 1 );
	__EXT_DEBUG_GRID_ASSERT( nRowCount == 1 );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return
		_DP.CellRangeSet(
			_Tree_MapColToCache( nColNo ),
			_Tree_MapRowToCache( nRowNo ),
			nColCount,
			nRowCount,
			pCellNewValue,
			bReplace,
			pInitRTC,
			bAutoFindValue,
			bUseColumnDefaultValue,
			p_nUpdatedCellCount
			);
}

bool CExtTreeGridDataProvider::CacheReserveForOuterCells(
	ULONG nColCount,
	ULONG nRowCount
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return
		_DP.CacheReserveForOuterCells(
			nColCount + _Tree_ReservedColumnsGet(),
			nRowCount + _Tree_ReservedRowsGet()
			);
}

void CExtTreeGridDataProvider::CacheReservedCountsGet(
	ULONG * p_nColCount,
	ULONG * p_nRowCount
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	_DP.CacheReservedCountsGet(
		p_nColCount,
		p_nRowCount
		);
	if( p_nColCount != NULL )
		(*p_nColCount) -= _Tree_ReservedColumnsGet();
	if( p_nRowCount != NULL )
		(*p_nRowCount) -= _Tree_ReservedRowsGet();
}

bool CExtTreeGridDataProvider::CacheData(
	const CExtScrollItemCacheInfo & _sciNew,
	const CExtScrollItemCacheInfo & _sciOld
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	_sciNew;
	_sciOld;
	return true;
}

bool CExtTreeGridDataProvider::CacheIsVisibleFirstRecord( bool bHorz )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bHorz;
	return true;
}

bool CExtTreeGridDataProvider::CacheIsVisibleLastRecord( bool bHorz )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bHorz;
	return true;
}

ULONG CExtTreeGridDataProvider::CacheColumnCountGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ColumnCountGet();
}

ULONG CExtTreeGridDataProvider::CacheRowCountGet()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return RowCountGet();
}

INT CExtTreeGridDataProvider::IconGetCount()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.IconGetCount();
}

CExtCmdIcon * CExtTreeGridDataProvider::IconGetAt( INT nIdx )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.IconGetAt( nIdx );
}

INT CExtTreeGridDataProvider::IconInsert( // returns index or -1
	CExtCmdIcon * pIcon,
	INT nIdx, // = -1 // append
	bool bCopyIcon // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.IconInsert( pIcon, nIdx, bCopyIcon );
}

INT CExtTreeGridDataProvider::IconRemove(
	INT nIdx, // = 0
	INT nCountToRemove // = -1 // all
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.IconRemove( nIdx, nCountToRemove );
}

INT CExtTreeGridDataProvider::FontGetCount()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.FontGetCount();
}

HFONT CExtTreeGridDataProvider::FontGetAt( INT nIdx )
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.FontGetAt( nIdx );
}

INT CExtTreeGridDataProvider::FontInsert( // returns index or -1
	HFONT hFont,
	INT nIdx, // = -1 // append
	bool bCopyFont // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.FontInsert( hFont, nIdx, bCopyFont );
}

INT CExtTreeGridDataProvider::FontRemove(
	INT nIdx, // = 0
	INT nCountToRemove // = -1 // all
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.FontRemove( nIdx, nCountToRemove );
}

bool CExtTreeGridDataProvider::SortOrderUpdate(
	bool bColumns, // true = sort order for columns, false - for rows
	IDataProviderEvents * pDPE // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bColumns;
	pDPE;
	// this method must never be invoked
//	__EXT_DEBUG_GRID_ASSERT( FALSE );
	return true;
}

bool CExtTreeGridDataProvider::SortOrderSet(
	const CExtGridDataSortOrder & _gdso,
	bool bColumns, // true = sort order for columns, false - for rows
	IDataProviderEvents * pDPE // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
//	_gdso;
//	bColumns;
//	pDPE;
//	// this method must never be invoked
//	__EXT_DEBUG_GRID_ASSERT( FALSE );
//	return true;
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.SortOrderSet( _gdso, bColumns, pDPE );
}

bool CExtTreeGridDataProvider::SortOrderGet(
	CExtGridDataSortOrder & _gdso,
	bool bColumns // true = sort order for columns, false - for rows
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
//	_gdso;
//	bColumns;
//	// this method must never be invoked
//	__EXT_DEBUG_GRID_ASSERT( FALSE );
//	return true;
const CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.SortOrderGet( _gdso, bColumns );
}

bool CExtTreeGridDataProvider::SwapDroppedSeries(
	bool bColumns, // true = swap columns, false - rows
	ULONG nRowColNoSrc,
	ULONG nRowColNoDropBefore,
	IDataProviderEvents * pDPE // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ! bColumns )
	{
		// this method must never be invoked for rows
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return true;
	}
	return
		_Tree_GetCacheDP().SwapDroppedSeries(
			true,
			_Tree_MapColToCache( nRowColNoSrc ),
			_Tree_MapColToCache( nRowColNoDropBefore ),
			pDPE
			);
}

/////////////////////////////////////////////////////////////////////////////
// CExtTreeGridWnd

IMPLEMENT_DYNCREATE( CExtTreeGridWnd, CExtGridWnd );

CExtTreeGridWnd::CExtTreeGridWnd()
	: m_nGbwAdjustRectsLock( 0 )
	, m_bDrawFilledExpandGlyphs( true )
	, m_bAdustNullIndent( false )
	, m_nItemFocusLockCounter( 0L )
	, m_bSingleExpandMode( false )
{
//	if( m_iconTreeBoxExpanded.IsEmpty() )
//		PmBridge_GetPM()->LoadWinXpTreeBox(
//			m_iconTreeBoxExpanded,
//			true
//			);
//	if( m_iconTreeBoxCollapsed.IsEmpty() )
//		PmBridge_GetPM()->LoadWinXpTreeBox(
//			m_iconTreeBoxCollapsed,
//			false
//			);
}

CExtTreeGridWnd::~CExtTreeGridWnd()
{
}

#ifdef _DEBUG

void CExtTreeGridWnd::AssertValid() const
{
	CExtGridWnd::AssertValid();
	__EXT_DEBUG_GRID_ASSERT( m_nItemFocusLockCounter >= 0L );
}

void CExtTreeGridWnd::Dump( CDumpContext & dc ) const
{
	CExtGridWnd::Dump( dc );
}

#endif // _DEBUG

CExtGridDataProvider & CExtTreeGridWnd::OnGridQueryDataProvider()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_pDataProvider != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( m_pDataProvider );
		return (*m_pDataProvider);
	} // if( m_pDataProvider != NULL )
CExtTreeGridDataProvider * pTreeDP = new CExtMDP < CExtTreeGridDataProvider >;
	pTreeDP->m_pTreeGridWnd = this;
	m_pDataProvider = pTreeDP;
	__EXT_DEBUG_GRID_ASSERT_VALID( m_pDataProvider );
	return (*m_pDataProvider);
}

CExtTreeGridDataProvider & CExtTreeGridWnd::GetTreeData()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CExtGridDataProvider & _DP = OnGridQueryDataProvider();
	return (*(STATIC_DOWNCAST(CExtTreeGridDataProvider,(&_DP))));
}

const CExtTreeGridDataProvider & CExtTreeGridWnd::GetTreeData() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridWnd * > ( this ) )
			-> GetTreeData();
}

CExtTreeGridDataProvider & CExtTreeGridWnd::_GetTreeData() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridWnd * > ( this ) )
			-> GetTreeData();
}

HTREEITEM CExtTreeGridWnd::ItemGetRoot() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
//	if(		OuterRowCountTopGet() == 0
//		||	OuterColumnCountRightGet() == 0
//		)
//		return NULL;
CExtTreeGridDataProvider & _DP = _GetTreeData();
	return (*_DP.TreeNodeGetRoot());
}

HTREEITEM CExtTreeGridWnd::ItemGetByVisibleRowIndex( LONG nRowNo ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( nRowNo < 0L )
		return NULL;
LONG nRowCount = RowCountGet();
	if( nRowNo >= nRowCount )
		return NULL;
CExtTreeGridDataProvider & _DP = _GetTreeData();
ULONG nReservedRowCount = 0;
	_DP.CacheReservedCountsGet( NULL, &nReservedRowCount );
	nRowNo += nReservedRowCount;
	return (*_DP.TreeNodeGetByVisibleRowIndex(ULONG(nRowNo)));
}

HTREEITEM CExtTreeGridWnd::ItemGetParent( HTREEITEM hTreeItem ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetParent()));
}

HTREEITEM CExtTreeGridWnd::_ItemBrowseNextImpl(
	HTREEITEM hTreeItem,
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper,
	bool bIncludeHidden
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->_TreeNodeBrowseNextImpl( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden )));
}

HTREEITEM CExtTreeGridWnd::_ItemBrowsePrevImpl(
	HTREEITEM hTreeItem,
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper,
	bool bIncludeHidden
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->_TreeNodeBrowsePrevImpl( bSiblingOnly, bExpandedWalk, bWalkDeeper, bIncludeHidden )));
}

HTREEITEM CExtTreeGridWnd::ItemGetNext(
	HTREEITEM hTreeItem,
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bIncludeHidden
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
HTREEITEM htiComputed =
		( *
			(
				CExtTreeGridCellNode::FromHTREEITEM( hTreeItem ) ->
					TreeNodeGetNext(
						bSiblingOnly,
						bExpandedWalk,
						bIncludeHidden
						)
			)
		);
HTREEITEM htiRoot = ItemGetRoot();
	if( htiComputed == htiRoot)
		return NULL;
	return htiComputed;
}

HTREEITEM CExtTreeGridWnd::ItemGetPrev(
	HTREEITEM hTreeItem,
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bIncludeHidden
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
HTREEITEM htiComputed =
		( *
			(
				CExtTreeGridCellNode::FromHTREEITEM( hTreeItem ) ->
					TreeNodeGetPrev(
						bSiblingOnly,
						bExpandedWalk,
						bIncludeHidden
						)
			)
		);
HTREEITEM htiRoot = ItemGetRoot();
	if( htiComputed == htiRoot)
		return NULL;
	return htiComputed;
}

HTREEITEM CExtTreeGridWnd::ItemJumpNext(
	HTREEITEM hTreeItem,
	LONG nJump,
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bIncludeHidden
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	if( nJump == 0 )
		return hTreeItem;
	for( ; true; )
	{
		if( nJump > 0 )
		{
			hTreeItem = ItemGetNext( hTreeItem, bSiblingOnly, bExpandedWalk, bIncludeHidden );
			nJump --;
		}
		else
		{
			hTreeItem = ItemGetPrev( hTreeItem, bSiblingOnly, bExpandedWalk, bIncludeHidden );
			nJump ++;
		}
		if( nJump == 0 )
			return hTreeItem;
	}
	return NULL;
}

HTREEITEM CExtTreeGridWnd::ItemJumpPrev(
	HTREEITEM hTreeItem,
	LONG nJump,
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bIncludeHidden
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	if( nJump == 0 )
		return hTreeItem;
	for( ; true; )
	{
		if( nJump > 0 )
		{
			hTreeItem = ItemGetPrev( hTreeItem, bSiblingOnly, bExpandedWalk, bIncludeHidden );
			nJump --;
		}
		else
		{
			hTreeItem = ItemGetNext( hTreeItem, bSiblingOnly, bExpandedWalk, bIncludeHidden );
			nJump ++;
		}
		if( nJump == 0 )
			return hTreeItem;
	}
	return NULL;
}

CSize CExtTreeGridWnd::ItemCellJoinGet(
	HTREEITEM hti,
	LONG nColNo,
	INT nColType // = 0
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
const CExtGridCell * pCell = ItemGetCell( hti, nColNo, nColType );
	if( pCell == NULL )
		return CSize( 1, 1 );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
CSize sizeJoin = pCell->JoinGet();
	return sizeJoin;
}

bool CExtTreeGridWnd::ItemCellJoinTest(
	CSize sizeJoin,
	HTREEITEM hti,
	LONG nColNo,
	INT nColType, // = 0
	bool bCheckCellsPresentInJoinedArea, // = false
	bool bCheckFrozenAreaIntersection // = true
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( sizeJoin.cx <= 0 || sizeJoin.cy <= 0 )
		return false; // invalid parameters, cannot join negative count of rows/columns
	if( nColNo < 0L || hti == NULL )
		return false; // invalid parameters, out of range
LONG nColCount = ( nColType == 0 ) ? ColumnCountGet() : ( ( nColType < 0 ) ? OuterColumnCountLeftGet() : OuterColumnCountRightGet() );
HTREEITEM htiParent = ItemGetParent( hti );
LONG nSiblingRowCount = ItemGetChildCount( htiParent );
	if( nColCount <= 0 || nSiblingRowCount <= 0 )
		return false; // virtual mode is not supported or target grid area is empty
LONG nSiblingRowNo = ItemGetSiblingIndexOf( hti );
	if( nColNo >= nColCount || nSiblingRowNo >= nSiblingRowCount )
		return false; // invalid parameters, out of range
const CExtGridCell * pCell = ItemGetCell( hti, nColNo, nColType );
	if( pCell == NULL )
		return false; // no grid cell at the specified location
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
CSize sizeJoinCurrent = pCell->JoinGet();
	if( sizeJoinCurrent.cx <= 0 || sizeJoinCurrent.cy <= 0 )
		return false; // grid cell at the specified location is already joined by other cell
	if( sizeJoinCurrent.cx <= sizeJoin.cx && sizeJoinCurrent.cy <= sizeJoin.cy )
	{
		if(		( ! bCheckCellsPresentInJoinedArea )
			||	( sizeJoin.cx == 1 && sizeJoin.cy == 1 )
			)
			return true; // the same join area as already is or less join area
	} // if( sizeJoinCurrent.cx <= sizeJoin.cx && sizeJoinCurrent.cy <= sizeJoin.cy )
LONG nEndColNo = -1L, nSiblingEndRowNo = -1L;
	if( sizeJoin.cx >= 1 )
	{
		nEndColNo = nColNo + sizeJoin.cx - 1;
		if( nEndColNo >= nColCount )
			return false; // right part of area to join is out of range
	} // if( sizeJoin.cx >= 1 )
	if( sizeJoin.cy >= 1 )
	{
		nSiblingEndRowNo = nSiblingRowNo + sizeJoin.cy - 1;
		if( nSiblingEndRowNo >= nSiblingRowCount )
			return false; // bottom part of area to join is out of range
	} // if( sizeJoin.cy >= 1 )
	if( bCheckFrozenAreaIntersection && nColType == 0 )
	{
		CRect rcFrozenRange = OnSiwGetFrozenRange();
		__EXT_DEBUG_GRID_ASSERT( rcFrozenRange.left >= 0 && rcFrozenRange.right >= 0 );
		if( nColType == 0 && nColNo < rcFrozenRange.right )
		{
			if( nColNo < rcFrozenRange.left )
			{
				if( nEndColNo >= rcFrozenRange.left )
					return false; // join area is partially in the left frozen range and in the horizontally scrollable range
			}
			else
			{
				if( nEndColNo >= rcFrozenRange.right )
					return false; // join area is partially in the horizontally scrollable range and in the right frozen range
			}
		} // if( nColType == 0 && nColNo < rcFrozenRange.right )
	} // if( bCheckFrozenAreaIntersection && ( nColType == 0 || nRowType == 0 ) )
LONG nWalkColNo, nSiblingWalkRowNo;
	for( nSiblingWalkRowNo = nSiblingRowNo; nSiblingWalkRowNo <= nSiblingEndRowNo; nSiblingWalkRowNo++ )
	{
		HTREEITEM htiSibling = ItemGetChildAt( htiParent, nSiblingWalkRowNo );
		__EXT_DEBUG_GRID_ASSERT( htiSibling != NULL );
		for( nWalkColNo = nColNo; nWalkColNo <= nEndColNo; nWalkColNo++ )
		{
			if( nWalkColNo == nColNo && nSiblingWalkRowNo == nSiblingRowNo )
				continue;
			const CExtGridCell * pCell = ItemGetCell( htiSibling, nWalkColNo, nColType );
			if( pCell == NULL )
			{
				if( bCheckCellsPresentInJoinedArea )
					return false; // no grid cell at the specified location
				else
					continue;
			} // if( pCell == NULL )
			__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
			CSize sizeJoinCell = pCell->JoinGet();
			if( sizeJoinCell.cx == 1 && sizeJoinCell.cy == 1 )
				continue;
			if( sizeJoinCell.cx != 1 )
			{
				LONG nTest = nWalkColNo + sizeJoinCell.cx;
				if( sizeJoinCell.cx < 0 )
				{
					if( nTest < nColNo )
						return false; // other join intersects with tested
				}
				else
				{
					nTest --;
					if( nTest > nEndColNo )
						return false; // other join intersects with tested
				}
			} // if( sizeJoinCell.cx != 1 )
			if( sizeJoinCell.cy != 1 )
			{
				LONG nTest = nSiblingWalkRowNo + sizeJoinCell.cy;
				if( sizeJoinCell.cy < 0 )
				{
					if( nTest < nSiblingRowNo )
						return false; // other join intersects with tested
				}
				else
				{
					nTest --;
					if( nTest > nSiblingEndRowNo )
						return false; // other join intersects with tested
				}
			} // if( sizeJoinCell.cy != 1 )
		} // for( nWalkColNo = nColNo; nWalkColNo <= nEndColNo; nWalkColNo++ )
	} // for( nSiblingWalkRowNo = nSiblingRowNo; nSiblingWalkRowNo <= nSiblingEndRowNo; nSiblingWalkRowNo++ )
	return true;
}

bool CExtTreeGridWnd::ItemCellJoinSet(
	CSize sizeJoin,
	HTREEITEM hti,
	LONG nColNo,
	INT nColType, // = 0
	bool bValidateJoin, // = false
	bool bCheckCellsPresentInJoinedArea, // = false
	bool bCheckFrozenAreaIntersection // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( bValidateJoin )
	{
		if( ! ItemCellJoinTest( sizeJoin, hti, nColNo, nColType, bCheckCellsPresentInJoinedArea, bCheckFrozenAreaIntersection ) )
			return false;
	} // if( bValidateJoin )
#ifdef _DEBUG
	else
	{
		__EXT_DEBUG_GRID_ASSERT( ItemCellJoinTest( sizeJoin, hti, nColNo, nColType, bCheckCellsPresentInJoinedArea, bCheckFrozenAreaIntersection ) );
	} // else from if( bValidateJoin )
#endif // _DEBUG
CExtGridCell * pCellCurrent = ItemGetCell( hti, nColNo, nColType );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCellCurrent );
CSize sizeJoinCurrent = pCellCurrent->JoinGet();
	if( sizeJoinCurrent.cx == sizeJoin.cx && sizeJoinCurrent.cy == sizeJoin.cy )
		return true; // the same join area as already is
	__EXT_DEBUG_GRID_ASSERT( sizeJoinCurrent.cx >= 1 && sizeJoinCurrent.cy >= 1 ); // must be not joined by other area
	pCellCurrent->JoinSet( sizeJoin );
HTREEITEM htiParent = ItemGetParent( hti );
//LONG nSiblingRowCount = ItemGetChildCount( htiParent );
LONG nSiblingRowNo = ItemGetSiblingIndexOf( hti );
LONG nWalkColNo, nSiblingWalkRowNo, nStepX, nStepY;
LONG nEndColNo = nColNo + max( sizeJoin.cx, sizeJoinCurrent.cx ) - 1;
LONG nSiblingEndRowNo = nSiblingRowNo + max( sizeJoin.cy, sizeJoinCurrent.cy ) - 1;
	for( nSiblingWalkRowNo = nSiblingRowNo, nStepY = 0; nSiblingWalkRowNo <= nSiblingEndRowNo; nSiblingWalkRowNo++, nStepY++ )
	{
		HTREEITEM htiSibling = ItemGetChildAt( htiParent, nSiblingWalkRowNo );
		__EXT_DEBUG_GRID_ASSERT( htiSibling != NULL );
		INT nJoinY = ( nStepY >= sizeJoin.cy ) ? 1 : ( -( nSiblingWalkRowNo - nSiblingRowNo ) );
		for( nWalkColNo = nColNo, nStepX = 0; nWalkColNo <= nEndColNo; nWalkColNo++, nStepX++ )
		{
			if( nWalkColNo == nColNo && nSiblingWalkRowNo == nSiblingRowNo )
				continue;
			CExtGridCell * pCell = ItemGetCell( htiSibling, nWalkColNo, nColType );
			if( pCell == NULL )
				continue;
			__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
			INT nJoinX = ( nStepX >= sizeJoin.cx || nStepY >= sizeJoin.cy ) ? 1 : ( -( nWalkColNo - nColNo ) );
			CSize sizeJoinCell( nJoinX, nJoinY );
			pCell->JoinSet( sizeJoinCell );
		} // for( nWalkColNo = nColNo, nStepX = 0; nWalkColNo <= nEndColNo; nWalkColNo++, nStepX++ )
	} // for( nSiblingWalkRowNo = nSiblingRowNo, nStepY = 0; nSiblingWalkRowNo <= nSiblingEndRowNo; nSiblingWalkRowNo++, nStepY++ )
	return true;
}

bool CExtTreeGridWnd::ItemCellJoinAdjustCoordinates(
	HTREEITEM & hti,
	LONG & nColNo,
	INT nColType // = 0
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CSize sizeJoin = ItemCellJoinGet( hti, nColNo, nColType );
	if( sizeJoin.cx == 1 && sizeJoin.cy == 1 )
		return false;
bool bRetVal = false;
	if( nColType != 0 && nColNo < 0 )
	{
		nColNo = (-nColNo) - 1;
		bRetVal = true;
		__EXT_DEBUG_GRID_ASSERT( nColNo >= 0 );
	} // if( nColType != 0 && nColNo < 0 )
	else
	{
		__EXT_DEBUG_GRID_ASSERT( nColNo >= 0 );
	} // else if( nColType != 0 && nColNo < 0 )
	if( sizeJoin.cx < 0 )
	{
		bRetVal = true;
		nColNo += sizeJoin.cx;
	}
	if( sizeJoin.cy < 0 )
	{
		HTREEITEM htiParent = ItemGetParent( hti );
		if( htiParent == NULL )
			return false;
		bRetVal = true;
//		nRowNo += sizeJoin.cy;
		LONG nSiblingRowNo = ItemGetSiblingIndexOf( hti );
		nSiblingRowNo += sizeJoin.cy;
		hti = ItemGetChildAt( htiParent, nSiblingRowNo );
		if( hti == NULL )
			return false;
	}
	return bRetVal;
}

LONG CExtTreeGridWnd::ItemGetChildCount( HTREEITEM hTreeItem ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetChildCount();
}

HTREEITEM CExtTreeGridWnd::ItemGetChildAt( HTREEITEM hTreeItem, LONG nPos ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetChildAt(nPos)));
}

HTREEITEM CExtTreeGridWnd::ItemGetFirstSibling( HTREEITEM hTreeItem ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetFirstSibling()));
}

HTREEITEM CExtTreeGridWnd::ItemGetLastSibling( HTREEITEM hTreeItem ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetLastSibling()));
}

HTREEITEM CExtTreeGridWnd::ItemGetFirstChild( HTREEITEM hTreeItem ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetFirstChild()));
}

HTREEITEM CExtTreeGridWnd::ItemGetLastChild( HTREEITEM hTreeItem ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetLastChild()));
}

HTREEITEM CExtTreeGridWnd::ItemInsert(
	HTREEITEM hTreeItemParent, // if NULL - root
	ULONG nIdxInsertBefore, // = (ULONG(-1L)) // if (ULONG(-1L)) - insert to end
	ULONG nInsertCount, // = 1
	bool bRedraw // = true
	) // returns handle of first inserted
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
HTREEITEM hTreeItem =
		(*(	_GetTreeData().TreeNodeInsert(
				( hTreeItemParent != NULL )
					? CExtTreeGridCellNode::FromHTREEITEM(hTreeItemParent)
					: NULL,
				nIdxInsertBefore,
				nInsertCount
				)
			) );
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	}
	return hTreeItem;
}

bool CExtTreeGridWnd::ItemCopyMoveTest(
	HTREEITEM hTreeItemCopyMove,
	HTREEITEM hTreeItemNewParent, // if NULL - root
	ULONG nIdxInsertBefore, // = (ULONG(-1L)) // if (ULONG(-1L)) - insert to end
	bool bEnableCopyingIntoInnerBranch, // = true
	bool * p_bCopyingIntoInnerBranch // = NULL
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		hTreeItemCopyMove == NULL
		||	hTreeItemCopyMove == hTreeItemNewParent
		)
		return false;
const CExtTreeGridDataProvider & _DP = _GetTreeData();
const CExtTreeGridCellNode * pNodeRoot =
		_DP.TreeNodeGetRoot();
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeRoot );
const CExtTreeGridCellNode * pNodeCopyMove =
		CExtTreeGridCellNode::FromHTREEITEM( hTreeItemCopyMove );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeCopyMove );
const CExtTreeGridCellNode * pNodeNewParent =
		( hTreeItemNewParent == NULL )
			? pNodeRoot
			: CExtTreeGridCellNode::FromHTREEITEM( hTreeItemNewParent )
			;
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeNewParent );
bool bRetVal =
		_DP.TreeNodeCopyMoveTest(
			pNodeCopyMove,
			pNodeNewParent,
			nIdxInsertBefore,
			bEnableCopyingIntoInnerBranch,
			p_bCopyingIntoInnerBranch
			);
	return bRetVal;
}

HTREEITEM CExtTreeGridWnd::ItemCopyMove(
	HTREEITEM hTreeItemCopyMove,
	HTREEITEM hTreeItemNewParent, // if NULL - root
	ULONG nIdxInsertBefore, // = (ULONG(-1L)) // if (ULONG(-1L)) - insert to end
	bool bMove, // = true
	bool bCloneCellObjects, // = true
	INT nExpandAction, // = TVE_TOGGLE // TVE_TOGGLE in this case - keep expanded state
	bool bEnableCopyingIntoInnerBranch, // = true
	bool bRedraw, // = true
	bool * p_bCopyingIntoInnerBranch // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItemCopyMove == NULL )
		return NULL;
CExtTreeGridDataProvider & _DP = _GetTreeData();
CExtTreeGridCellNode * pNodeRoot =
		_DP.TreeNodeGetRoot();
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeRoot );
CExtTreeGridCellNode * pNodeCopyMove =
		CExtTreeGridCellNode::FromHTREEITEM( hTreeItemCopyMove );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeCopyMove );
CExtTreeGridCellNode * pNodeNewParent =
		( hTreeItemNewParent == NULL )
			? pNodeRoot
			: CExtTreeGridCellNode::FromHTREEITEM( hTreeItemNewParent )
			;
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeNewParent );
	ItemFocusSet( NULL, false );
const CExtTreeGridCellNode * pNodeRetVal =
		_DP.TreeNodeCopyMove(
			pNodeCopyMove,
			pNodeNewParent,
			nIdxInsertBefore,
			bMove,
			bCloneCellObjects,
			nExpandAction,
			bEnableCopyingIntoInnerBranch,
			p_bCopyingIntoInnerBranch
			);
	if( pNodeRetVal == NULL )
		return NULL;
	OnSwUpdateScrollBars();
	if( bRedraw )
		OnSwDoRedraw();
HTREEITEM htiRetVal = (*pNodeRetVal);
	return htiRetVal;
}

bool CExtTreeGridWnd::GridSortOrderSetup(
	bool bSortColumns,
	const CExtGridDataSortOrder & _gdsoUpdate,
	bool bUpdateExistingSortOrder, // = false
	bool bInvertIntersectionSortOrder, // = true
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( bSortColumns )
		return false;
	return
		TreeGridSortOrderSetup(
			_gdsoUpdate,
			bUpdateExistingSortOrder,
			bInvertIntersectionSortOrder,
			true,
			NULL,
			bRedraw
			);
}

bool CExtTreeGridWnd::TreeGridSortOrderSetup( // default paremters will sort root with all deep children
	const CExtGridDataSortOrder & _gdsoUpdate,
	bool bUpdateExistingSortOrder, // = false
	bool bInvertIntersectionSortOrder, // = true
	bool bUseDeepSorting, // = true
	HTREEITEM htiSort, // = NULL // NULL - use root
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ! _gdsoUpdate.ItemsUnique() )
	{
		// items inside sort order must be unique
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	if( htiSort == NULL )
	{
		htiSort = ItemGetRoot();
		if( htiSort == NULL )
			return false;
	}
bool bSortColumns = false;
CExtGridDataProvider & _DataProvider = OnGridQueryDataProvider();
ULONG nReserved = 0L;
	_DataProvider.CacheReservedCountsGet(
		bSortColumns ? NULL : (&nReserved),
		bSortColumns ? (&nReserved) : NULL
		);
CExtGridDataSortOrder _gdsoDP;
	if( ! GridSortOrderGet(bSortColumns,_gdsoDP) )
		return false;
	if( ! bUpdateExistingSortOrder )
	{
		CExtGridDataSortOrder _gdsoSetup( _gdsoUpdate );
		if(		bInvertIntersectionSortOrder
			&&	_gdsoDP.m_arrItems.GetSize() == 1
			&&	_gdsoSetup.m_arrItems.GetSize() == 1
			)
		{
			const CExtGridDataSortOrder::ITEM_INFO & _soiiOld =
				_gdsoDP.m_arrItems[ 0 ];
			CExtGridDataSortOrder::ITEM_INFO _soiiNew(
				_gdsoSetup.m_arrItems[ 0 ] );
			if( _soiiOld.m_nRowColNo == _soiiNew.m_nRowColNo )
			{
				_soiiNew.m_bAscending = (! _soiiOld.m_bAscending);
				_gdsoSetup.m_arrItems.SetAt( 0, _soiiNew );
			}
		}
		_gdsoSetup.UpdateIndices( 0, nReserved, true );
		bool bRetVal =
			_DataProvider.SortOrderSet(
				_gdsoSetup,
				bSortColumns,
				this
				);
		if( bRetVal )
		{
			_gdsoSetup.UpdateIndices( 0, nReserved, false );
			bRetVal =
				bUseDeepSorting
					? ItemSortChildrenDeep(
						htiSort,
						_gdsoSetup,
						true,
						false
						)
					: ItemSortChildren(
						htiSort,
						_gdsoSetup,
						true,
						false
						)
					;
		}
		if( bRetVal )
		{
			OnGridSyncCellSortArrows(
				bSortColumns,
				_gdsoDP,
				false
				);
			OnGridSyncCellSortArrows(
				bSortColumns,
				_gdsoSetup,
				true
				);
		} // if( bRetVal )
		if( bRedraw )
		{
			OnSwUpdateScrollBars();
			OnSwDoRedraw();
		} // if( bRedraw )
		return bRetVal;
	} // if( ! bUpdateExistingSortOrder )
CExtGridDataSortOrder _gdsoModified( _gdsoDP );
	_gdsoModified.SetupOrder(
		_gdsoUpdate,
		bInvertIntersectionSortOrder
		);
	__EXT_DEBUG_GRID_ASSERT( _gdsoModified.ItemsUnique() );
	_gdsoModified.UpdateIndices( 0, nReserved, true );
bool bRetVal =
		_DataProvider.SortOrderSet(
			_gdsoModified,
			bSortColumns,
			this
			);
	if( bRetVal )
	{
		_gdsoModified.UpdateIndices( 0, nReserved, false );
		bRetVal =
			bUseDeepSorting
				? ItemSortChildrenDeep(
					htiSort,
					_gdsoModified,
					true,
					false
					)
				: ItemSortChildren(
					htiSort,
					_gdsoModified,
					true,
					false
					)
				;
	}
	if( bRetVal )
	{
		OnGridSyncCellSortArrows(
			bSortColumns,
			_gdsoDP,
			false
			);
		OnGridSyncCellSortArrows(
			bSortColumns,
			_gdsoModified,
			true
			);
	} // if( bRetVal )
	if( bRedraw )
	{
		CExtPopupMenuTipWnd * pATTW =
			OnAdvancedPopupMenuTipWndGet();
		if(		pATTW->GetSafeHwnd() != NULL
			&&	( pATTW->GetStyle() & WS_VISIBLE ) != 0
			)
			pATTW->Hide();
		if( m_wndToolTip.GetSafeHwnd() != NULL )
			m_wndToolTip.DelTool( this, 1 );
		CWnd::CancelToolTips();

		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw )
	return bRetVal;
}

bool CExtTreeGridWnd::ItemSortChildren(
	HTREEITEM hTreeItem,
	const CExtGridDataSortOrder & _gdso,
	bool bRestoreFocus, // = true
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return false;
CExtTreeGridCellNode * pNode =
		CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
HTREEITEM hTreeItemFocus = NULL;
	if( bRestoreFocus )
		hTreeItemFocus = ItemFocusGet();
CExtTreeGridDataProvider & _DP = _GetTreeData();
	if( ! _DP.TreeNodeSortChildren( pNode, _gdso, this ) )
		return false;
	OnSwUpdateScrollBars();
	if( hTreeItemFocus != NULL )
		ItemFocusSet( hTreeItemFocus, false );
	if( bRedraw )
		OnSwDoRedraw();
	return true;
}

bool CExtTreeGridWnd::ItemSortChildrenDeep(
	HTREEITEM hTreeItem,
	const CExtGridDataSortOrder & _gdso,
	bool bRestoreFocus, // = true
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return false;
LONG nChildIndex, nChildCount = ItemGetChildCount( hTreeItem );
	if( nChildCount == 0 )
		return true;
	if( ! ItemSortChildren( hTreeItem, _gdso, bRestoreFocus, false ) )
		return false;
	for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
	{
		HTREEITEM htiChild = ItemGetChildAt( hTreeItem, nChildIndex );
		__EXT_DEBUG_GRID_ASSERT( htiChild != NULL );
		if( ! ItemSortChildrenDeep( htiChild, _gdso, bRestoreFocus, false ) )
			return false;
	}
	if( bRedraw )
		OnSwDoRedraw();
	return true;
}

LONG CExtTreeGridWnd::ItemRemove(
	HTREEITEM hTreeItem,
	bool bChildrenOnly, // = false
	bool bRedraw // = true
	) // returns count of removed items
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return 0;
	if( ! bChildrenOnly )
	{
		HTREEITEM htiRoot = ItemGetRoot();
		if( hTreeItem == htiRoot )
			bChildrenOnly = true;
	}
	ItemFocusSet( NULL, false );
LONG nCountRemoved = (LONG)
		_GetTreeData().TreeNodeRemove(
			CExtTreeGridCellNode::FromHTREEITEM(hTreeItem),
			bChildrenOnly
			);
	if( GetSafeHwnd() != NULL )
	{
		OnSwUpdateScrollBars();
		if( bRedraw )
			OnSwDoRedraw();
	}
	return nCountRemoved;
}

CExtGridCell * CExtTreeGridWnd::ItemGetCell(
	HTREEITEM hTreeItem,
	LONG nColNo,
	INT nColType, // = 0
	CRuntimeClass * pInitRTC, // = NULL
	bool bAutoFindValue, // = true
	bool bUseColumnDefaultValue // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		hTreeItem == NULL
		||	hTreeItem == ItemGetRoot()
		)
		return NULL;
LONG nEffectiveColNo = nColNo;
	if( nColType < 0 )
	{
		LONG nOuterColCountLeft = CExtGridBaseWnd::OuterColumnCountLeftGet();
		if( nColNo >= nOuterColCountLeft )
		{
			// invalid cell position
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return NULL;
		}
		LONG nOuterColCountRight = CExtGridBaseWnd::OuterColumnCountRightGet();
		LONG nEffectiveShift = nOuterColCountLeft + nOuterColCountRight;
		nEffectiveColNo -= nEffectiveShift;
	} // if( nColType < 0 )
	else if( nColType > 0 )
	{
		LONG nOuterColCountRight = CExtGridBaseWnd::OuterColumnCountRightGet();
		if( nColNo >= nOuterColCountRight )
		{
			// invalid cell position
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return NULL;
		}
		nEffectiveColNo -= nOuterColCountRight;
	} // else if( nColType > 0 )
	else
	{
		LONG nInnerColCount = ColumnCountGet();
		if( nColNo >= nInnerColCount )
		{
			// invalid cell position
//			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return NULL;
		} // if( nColNo >= nInnerColCount )
	} // else from else if( nColType > 0 )
CExtGridCell * pCell =
		_GetTreeData().ExtractGridCell(
			CExtTreeGridCellNode::FromHTREEITEM(hTreeItem),
			(ULONG)nEffectiveColNo,
			pInitRTC,
			bAutoFindValue,
			bUseColumnDefaultValue
			);
	return pCell;
}

const CExtGridCell * CExtTreeGridWnd::ItemGetCell(
	HTREEITEM hTreeItem,
	LONG nColNo,
	INT nColType, // = 0
	CRuntimeClass * pInitRTC, // = NULL
	bool bAutoFindValue, // = true
	bool bUseColumnDefaultValue // = true
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridWnd * > ( this ) ) ->
			ItemGetCell(
				hTreeItem,
				nColNo,
				nColType,
				pInitRTC,
				bAutoFindValue,
				bUseColumnDefaultValue
				);
}

bool CExtTreeGridWnd::ItemSetCell(
	HTREEITEM hTreeItem,
	LONG nColNo,
	INT nColType, // = 0
	const CExtGridCell * pCellNewValue, // = NULL // if NULL - empty existing cell values
	bool bReplace, // = false // false - assign to existing cell instances or column/row type created cells, true - create new cloned copies of pCellNewValue
	CRuntimeClass * pInitRTC // = NULL // runtime class for new cell instance (used if bReplace=false)
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		hTreeItem == NULL
		||	hTreeItem == ItemGetRoot()
		)
		return false;
LONG nEffectiveColNo = nColNo;
	if( nColType < 0 )
	{
		LONG nOuterColCountLeft = CExtGridBaseWnd::OuterColumnCountLeftGet();
		if( nColNo >= nOuterColCountLeft )
		{
			// invalid cell position
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return false;
		}
		LONG nOuterColCountRight = CExtGridBaseWnd::OuterColumnCountRightGet();
		LONG nEffectiveShift = nOuterColCountLeft + nOuterColCountRight;
		nEffectiveColNo -= nEffectiveShift;
	} // if( nColType < 0 )
	else if( nColType > 0 )
	{
		LONG nOuterColCountRight = CExtGridBaseWnd::OuterColumnCountRightGet();
		if( nColNo >= nOuterColCountRight )
		{
			// invalid cell position
			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return false;
		}
		nEffectiveColNo -= nOuterColCountRight;
	} // else if( nColType > 0 )
	else
	{
		LONG nInnerColCount = ColumnCountGet();
		if( nColNo >= nInnerColCount )
		{
			// invalid cell position
//			__EXT_DEBUG_GRID_ASSERT( FALSE );
			return false;
		} // if( nColNo >= nInnerColCount )
	} // else from else if( nColType > 0 )
CExtGridCell * pCellInstantiated =
		_GetTreeData().ExtractGridCellWithReplacing(
			CExtTreeGridCellNode::FromHTREEITEM(hTreeItem),
			(ULONG)nEffectiveColNo,
			pCellNewValue,
			bReplace,
			pInitRTC
			);
	if( pCellInstantiated != NULL )
	{
		__EXT_DEBUG_GRID_ASSERT_VALID( pCellInstantiated );
		return true;
	}
	else
		return false;
}

bool CExtTreeGridWnd::ItemIsExpanded(
	HTREEITEM hTreeItem
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return false;
	}
	return
		CExtTreeGridCellNode::FromHTREEITEM( hTreeItem )
			-> TreeNodeIsExpanded();
}

CExtTreeGridCellNode::e_expand_box_shape_t CExtTreeGridWnd::ItemGetExpandBoxShape(
	HTREEITEM hTreeItem
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
	{
		__EXT_DEBUG_GRID_ASSERT( FALSE );
		return CExtTreeGridCellNode::__EEBS_NONE;
	}
	return
		CExtTreeGridCellNode::FromHTREEITEM( hTreeItem )
			-> TreeNodeGetExpandBoxShape();
}

void CExtTreeGridWnd::ItemExpand(
	HTREEITEM hTreeItem,
	INT nActionTVE, // = TVE_TOGGLE // TVE_COLLAPSE, TVE_EXPAND, TVE_TOGGLE only
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT(
			nActionTVE == TVE_COLLAPSE
		||	nActionTVE == TVE_EXPAND
		||	nActionTVE == TVE_TOGGLE
		);
	if( hTreeItem == NULL )
		return;
LONG nColNoFocused = FocusGet().x;
HTREEITEM htiFocus = ItemFocusGet();
HTREEITEM htiResetFocus = htiFocus;
	if( htiFocus != NULL )
	{
		ItemFocusSet( NULL, false );
		if(		htiResetFocus != hTreeItem
			&&	(	nActionTVE == TVE_COLLAPSE
				||	(	nActionTVE == TVE_TOGGLE
				&&	ItemIsExpanded( hTreeItem )
					)
				)
			)
		{
			{
				HTREEITEM htiTest = htiFocus, htiRoot = ItemGetRoot();
				for( ; htiTest != htiRoot; htiTest = ItemGetParent( htiTest ) )
				{
					if( htiTest == hTreeItem )
					{
						htiResetFocus = hTreeItem;
						break;
					}
				}
			}
		}
	}
	if( _GetTreeData().TreeNodeExpand(
			CExtTreeGridCellNode::FromHTREEITEM(hTreeItem),
			nActionTVE
			)
		)
	{
		if( htiResetFocus != NULL )
		{
			FocusUnset( false );
			ItemFocusSet( htiResetFocus, nColNoFocused, bRedraw );
		}
		if( bRedraw && GetSafeHwnd() != NULL )
			OnSwUpdateScrollBars();
		if( bRedraw )
			ItemEnsureVisibleBranch( hTreeItem, bRedraw );
	}
}

void CExtTreeGridWnd::ItemEnsureVisibleBranch(
	HTREEITEM hTreeItem,
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		hTreeItem == NULL
		||	GetSafeHwnd() == NULL
		)
		return;
LONG nRowOffset = ItemGetVisibleIndexOf( hTreeItem );
CRect rcVisibleRange = OnSiwGetVisibleRange();
	if( nRowOffset == rcVisibleRange.top )
	{
		if( bRedraw )
			OnSwDoRedraw();
		return;
	}
	if( nRowOffset < rcVisibleRange.top )
	{
		if( nRowOffset >= 0 )
			EnsureVisibleRow( nRowOffset, bRedraw );
		return;
	}
LONG nItemCountToShow = 1L;
	if( ItemIsExpanded( hTreeItem ) )
		nItemCountToShow += (LONG)
			CExtTreeGridCellNode::FromHTREEITEM( hTreeItem ) ->
				_ContentWeight_Get( true );
LONG nHeightAvailable = OnSwGetClientRect().Height();
LONG nHeightPassed = 0L, nRowNo;
	__EXT_DEBUG_GRID_ASSERT( nRowOffset >= 0 );
	for( nRowNo = 0L; nRowNo < nItemCountToShow; nRowNo ++ )
	{
		LONG nRowHeight = OnSiwQueryItemExtentV( nRowNo + nRowOffset );
		LONG nTest = nHeightPassed + nRowHeight;
		if( nTest > nHeightAvailable )
			bRedraw;
		nHeightPassed = nTest;
	}
	nItemCountToShow = nRowNo;
LONG nRowEnd = nRowOffset + nItemCountToShow - 1;
	if( nRowEnd <= rcVisibleRange.bottom )
	{
		if( bRedraw )
			OnSwDoRedraw();
		return;
	}
	EnsureVisibleRow( nRowEnd, false );
	EnsureVisibleRow( nRowOffset, bRedraw );
}

void CExtTreeGridWnd::ItemExpandAll(
	HTREEITEM hTreeItem,
	INT nActionTVE, // = TVE_TOGGLE // TVE_COLLAPSE, TVE_EXPAND, TVE_TOGGLE only
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT(
			nActionTVE == TVE_COLLAPSE
		||	nActionTVE == TVE_EXPAND
		||	nActionTVE == TVE_TOGGLE
		);
	if( hTreeItem == NULL )
		return;
	if( hTreeItem != ItemGetRoot() )
		ItemExpand( hTreeItem, nActionTVE, bRedraw );
	else
		bRedraw = false;
LONG nChildIndex, nChildCount = ItemGetChildCount( hTreeItem );
	for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
	{
		HTREEITEM htiChild = ItemGetChildAt( hTreeItem, nChildIndex );
		__EXT_DEBUG_GRID_ASSERT( htiChild != NULL );
		ItemExpandAll( htiChild, nActionTVE, false );
	}
	if( bRedraw && GetSafeHwnd() != NULL )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	}
}

HTREEITEM CExtTreeGridWnd::ItemFocusGet() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
CPoint ptFocus = FocusGet();
	if( ptFocus.y < 0 || ptFocus.y >= RowCountGet() )
		return NULL;
HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( ptFocus.y );
	return hTreeItem;
}

bool CExtTreeGridWnd::ItemFocusIsLocked() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ( m_nItemFocusLockCounter > 0 ) ? true : false;
}

void CExtTreeGridWnd::ItemFocusLock( bool bLock ) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( bLock )
		m_nItemFocusLockCounter ++;
	else
		m_nItemFocusLockCounter --;
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

bool CExtTreeGridWnd::ItemFocusSet(
	HTREEITEM hTreeItem,
	LONG nColNo,
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == ItemGetRoot() )
		return false;
	if( ! ItemFocusIsLocked() )
	{
		CPoint ptFocus = FocusGet();
		if( ptFocus.y < 0 || ptFocus.y >= RowCountGet() )
		{
			if( hTreeItem == NULL )
				return false;
		} // if( ptFocus.y < 0 || ptFocus.y >= RowCountGet() )
		else
		{
			if( hTreeItem == NULL )
			{
				SelectionUnset( false, false );
				FocusUnset( bRedraw );
				return true;
			}
			HTREEITEM htiFocus = ItemGetByVisibleRowIndex( ptFocus.y );
			__EXT_DEBUG_GRID_ASSERT( htiFocus != NULL );
			if( htiFocus == hTreeItem && nColNo == ptFocus.x )
				return false;
		} // else from if( ptFocus.y < 0 || ptFocus.y >= RowCountGet() )
		if( ! ItemEnsureExpanded( hTreeItem, false ) )
			return false;
		ptFocus.x = nColNo;
		ptFocus.y = ItemGetVisibleIndexOf( hTreeItem );
		__EXT_DEBUG_GRID_ASSERT( ptFocus.y >= 0 );
		if( bRedraw )
			OnSwUpdateScrollBars();
		FocusSet( ptFocus, bRedraw, true, true, bRedraw );
	} // if( ! ItemFocusIsLocked() )
	else
	{
		if( bRedraw )
		{
			OnSwUpdateScrollBars();
			OnSwInvalidate( true );
		} // if( bRedraw )
	} // else from if( ! ItemFocusIsLocked() )
	return true;
}

bool CExtTreeGridWnd::ItemFocusSet(
	HTREEITEM hTreeItem,
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ItemFocusSet( hTreeItem, FocusGet().x, bRedraw );
}

bool CExtTreeGridWnd::ItemEnsureExpanded(
	HTREEITEM hTreeItem,
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return false;
CExtTreeGridDataProvider & _DP = _GetTreeData();
CExtTreeGridCellNode * pNodeRoot =
		_DP.TreeNodeGetRoot();
	__EXT_DEBUG_GRID_ASSERT_VALID( pNodeRoot );
CExtTreeGridCellNode * pNode =
		CExtTreeGridCellNode::FromHTREEITEM(hTreeItem);
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	if( LPCVOID(pNodeRoot) != LPCVOID(pNode) )
	{
		pNode = pNode->TreeNodeGetParent();
		__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		for( ; LPCVOID(pNodeRoot) != LPCVOID(pNode); )
		{
			_DP.TreeNodeExpand(
				pNode,
				TVE_EXPAND
				);
			pNode = pNode->TreeNodeGetParent();
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
		} // for( ; LPCVOID(pNodeRoot) == LPCVOID(pNode); )
	} // if( LPCVOID(pNodeRoot) != LPCVOID(pNode) )
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	}
	return true;
}

LONG CExtTreeGridWnd::ItemGetSiblingIndexOf(
	HTREEITEM hTreeItem,
	bool bIncludeHidden // = true
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return -1;
CExtTreeGridCellNode * pNode =
		CExtTreeGridCellNode::FromHTREEITEM(hTreeItem);
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
LONG i = (LONG)pNode->TreeNodeGetSiblingIndex( bIncludeHidden );
	return i;
}

LONG CExtTreeGridWnd::ItemGetVisibleIndexOf(
	HTREEITEM hTreeItem,
	bool bExpandedOnly, // = true
	bool bIncludeHidden // = false
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return -1;
CExtTreeGridCellNode * pNode =
		CExtTreeGridCellNode::FromHTREEITEM(hTreeItem);
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	if( ! pNode->TreeNodeIsDisplayed() )
		return -1;
LONG i = (LONG)pNode->TreeNodeCalcOffset( bExpandedOnly, bIncludeHidden );
	return i;
}

bool CExtTreeGridWnd::_IsTreeGridWndInitialized() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return ( ItemGetRoot() != NULL ) ? true : false;
}

void CExtTreeGridWnd::_InitTreeGridWnd()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	ItemGetRoot();
}

void CExtTreeGridWnd::PreSubclassWindow() 
{
	CExtGridWnd::PreSubclassWindow();
	_InitTreeGridWnd();
}

CExtGridHitTestInfo & CExtTreeGridWnd::HitTest(
	CExtGridHitTestInfo & htInfo,
	bool bReAlignCellResizing,
	bool bSupressZeroTopCellResizing,
	bool bComputeOuterDropAfterState // = false
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( bReAlignCellResizing || bComputeOuterDropAfterState )
		m_nGbwAdjustRectsLock ++;
CExtGridHitTestInfo & htInfoRetVal =
		CExtGridWnd::HitTest(
			htInfo,
			bReAlignCellResizing,
			bSupressZeroTopCellResizing,
			bComputeOuterDropAfterState
			);
	if(		(htInfoRetVal.m_dwAreaFlags&__EGBWA_NEAR_CELL_BORDER_SIDE_MASK) == 0
		&&	(htInfoRetVal.m_dwAreaFlags&__EGBWA_INNER_CELLS) != 0
		&&	OnTreeGridQueryColumnOutline(htInfoRetVal.m_nColNo)
		)
	{ // if hit testing inside inner cell and not near the cell's border
		CRect rcOutlineArea(
			htInfoRetVal.m_rcExtra.left,
			htInfoRetVal.m_rcExtra.top,
			min(htInfoRetVal.m_rcItem.left,htInfoRetVal.m_rcItem.right),
			htInfoRetVal.m_rcExtra.bottom
			);
		if( rcOutlineArea.PtInRect(htInfoRetVal.m_ptClient) )
		{ // if inside outline area
			htInfoRetVal.m_dwAreaFlags |= __EGBWA_TREE_OUTLINE_AREA;
			HTREEITEM hTreeItemHT = ItemGetByVisibleRowIndex( htInfoRetVal.m_nRowNo );
			__EXT_DEBUG_GRID_ASSERT( hTreeItemHT != NULL );
			CExtTreeGridCellNode * pNode =
				CExtTreeGridCellNode::FromHTREEITEM( hTreeItemHT );
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
			CExtTreeGridCellNode::e_expand_box_shape_t eEBS =
				pNode->TreeNodeGetExpandBoxShape();
			if( eEBS != CExtTreeGridCellNode::__EEBS_NONE )
			{
				CRect rcIndent(
					htInfoRetVal.m_rcItem.left, htInfoRetVal.m_rcItem.top,
					htInfoRetVal.m_rcItem.left, htInfoRetVal.m_rcItem.bottom
					);
				INT nPxIndent = (INT)pNode->TreeNodeIndentPxGet();
				if( nPxIndent == ULONG(-1L) )
					nPxIndent = GetTreeData().m_nIndentPxDefault;
				rcIndent.right = rcIndent.left;
				rcIndent.left -= nPxIndent;
				if(		rcIndent.left == rcIndent.right
					&&	m_bAdustNullIndent
					)
				{
					INT nPxIndentPaint = nPxIndent;
					if( nPxIndentPaint == 0 )
						nPxIndentPaint = GetTreeData().m_nIndentPxDefault;
					rcIndent.left -= nPxIndentPaint;
				}
				if( rcIndent.PtInRect(htInfoRetVal.m_ptClient) )
				{
					htInfoRetVal.m_dwAreaFlags |= __EGBWA_TREE_BOX;
					if( eEBS != CExtTreeGridCellNode::__EEBS_EXPANDED )
						htInfoRetVal.m_dwAreaFlags |= __EGBWA_TREE_BOX_IS_EXPANDED;
				} // if( rcIndent.PtInRect(htInfoRetVal.m_ptClient) )
			} // if( eEBS != CExtTreeGridCellNode::__EEBS_NONE )
		} // if inside outline area
	} // if hit testing inside inner cell and not near the cell's border
	if( bReAlignCellResizing || bComputeOuterDropAfterState )
		m_nGbwAdjustRectsLock --;
	return htInfoRetVal;
}

void CExtTreeGridWnd::OnGbwResizingStateUpdate(
	bool bInitial,
	bool bFinal,
	const CPoint * p_ptClient // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_nGbwAdjustRectsLock ++;
	CExtGridWnd::OnGbwResizingStateUpdate(
		bInitial,
		bFinal,
		p_ptClient
		);
	m_nGbwAdjustRectsLock --;
	if( m_nGbwAdjustRectsLock == 0 )
	{
		OnSwRecalcLayout( true );
		OnSwDoRedraw();
	} // if( m_nGbwAdjustRectsLock == 0 )
}

void CExtTreeGridWnd::OnGbwResizingStateApply(
	bool bHorz,
	LONG nItemNo,
	INT nItemExtent
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_nGbwAdjustRectsLock ++;
	CExtGridWnd::OnGbwResizingStateApply(
		bHorz,
		nItemNo,
		nItemExtent
		);
	m_nGbwAdjustRectsLock --;
	if( m_nGbwAdjustRectsLock == 0 )
	{
		OnSwRecalcLayout( true );
		OnSwDoRedraw();
	} // if( m_nGbwAdjustRectsLock == 0 )
}

bool CExtTreeGridWnd::OnTreeGridQueryDrawOutline(
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( dc.GetSafeHdc() != NULL );
	//__EXT_DEBUG_GRID_ASSERT( nVisibleColNo >= 0 );
	//__EXT_DEBUG_GRID_ASSERT( nVisibleRowNo >= 0 );
	__EXT_DEBUG_GRID_ASSERT( nColNo >= 0 );
	__EXT_DEBUG_GRID_ASSERT( nRowNo >= 0 );
	dc;
	nVisibleColNo;
	nVisibleRowNo;
	nColNo;
	nRowNo;
	rcCellExtra;
	rcCell;
	rcVisibleRange;
	dwAreaFlags;
	dwHelperPaintFlags;
	return true;
}

bool CExtTreeGridWnd::OnTreeGridQueryPpvwVisibilityForItem(
	const CExtTreeGridCellNode * pNode
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
bool bHiddenInPrintPreview = pNode->HiddenInPrintPreviewGet();
	return (! bHiddenInPrintPreview );
}

void CExtTreeGridWnd::OnTreeGridNodeRemoved(
	CExtTreeGridCellNode * pNode
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
	pNode;
}

void CExtTreeGridWnd::OnTreeGridPaintExpandButton(
	CDC & dc,
	HTREEITEM hTreeItem,
	bool bExpanded,
	const CRect & rcIndent
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( dc.GetSafeHdc() != NULL );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
CExtTreeGridCellNode * pNode =
		CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
INT nPxIndent = (INT)pNode->TreeNodeIndentPxGet();
	if( nPxIndent == ULONG(-1L) )
		nPxIndent = GetTreeData().m_nIndentPxDefault;
//			rcIndent.right = rcIndent.left;
//			rcIndent.left -= nPxIndent;
INT nMetricH = rcIndent.Width();
//			INT nMetricH2 = nMetricH / 2;
INT nMetricV = rcIndent.Height();
INT nMetricV2 = min( nMetricH, nMetricV );
	nMetricV2 /= 2;

	if(		( ! m_iconTreeBoxExpanded.IsEmpty() )
		&&	( ! m_iconTreeBoxCollapsed.IsEmpty() )
		)
	{
		const CExtCmdIcon * pIcon =
			bExpanded
				? (&m_iconTreeBoxExpanded)
				: (&m_iconTreeBoxCollapsed)
				;
		if( ! pIcon->IsEmpty() )
		{
			CSize _sizeIcon = pIcon->GetSize();
			CPoint ptOutput =
				rcIndent.TopLeft()
				+ CSize(
					( rcIndent.Width()  - _sizeIcon.cx ) / 2,
					( /*rcIndent.Height()*/ nMetricV2*2 - _sizeIcon.cy ) / 2
					)
				;
			pIcon->Paint(
				PmBridge_GetPM(),
				dc.m_hDC,
				ptOutput.x,
				ptOutput.y,
				-1,
				-1
				);
			return;
		} // if( ! pIcon->IsEmpty() )
	}
CExtPaintManager::glyph_t * pGlyph =
		&CExtPaintManager::g_DockingCaptionGlyphs[
				bExpanded
					? (	m_bDrawFilledExpandGlyphs
							? CExtPaintManager::__DCBT_TREE_MINUS_FILLED
							: CExtPaintManager::__DCBT_TREE_MINUS
						)
					: (	m_bDrawFilledExpandGlyphs
							? CExtPaintManager::__DCBT_TREE_PLUS_FILLED
							: CExtPaintManager::__DCBT_TREE_PLUS
						)
				];
COLORREF ColorValues[] =
{
	RGB(0,0,0),
	PmBridge_GetPM()->GetColor(
		CExtPaintManager::CLR_TEXT_OUT,
		(CObject*)this
		),
	PmBridge_GetPM()->GetColor(
		COLOR_WINDOW,
		(CObject*)this
		),
};
CPoint ptGlyph = rcIndent.TopLeft();
	ptGlyph.x += ( rcIndent.Width() - pGlyph->Size().cx ) / 2;
	ptGlyph.y += ( /*rcIndent.Height()*/ nMetricV2*2 - pGlyph->Size().cy ) / 2;
	PmBridge_GetPM()->PaintGlyph(
		dc,
		ptGlyph,
		*pGlyph,
		ColorValues
		);
}

void CExtTreeGridWnd::OnGbwPaintCell(
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( dc.GetSafeHdc() != NULL );
	//__EXT_DEBUG_GRID_ASSERT( nVisibleColNo >= 0 );
	//__EXT_DEBUG_GRID_ASSERT( nVisibleRowNo >= 0 );
	__EXT_DEBUG_GRID_ASSERT( nColNo >= 0 );
	__EXT_DEBUG_GRID_ASSERT( nRowNo >= 0 );
	CExtGridWnd::OnGbwPaintCell(
		dc,
		nVisibleColNo,
		nVisibleRowNo,
		nColNo,
		nRowNo,
		rcCellExtra,
		rcCell,
		rcVisibleRange,
		dwAreaFlags,
		dwHelperPaintFlags
		);
	if(		( dwAreaFlags & __EGBWA_INNER_CELLS ) != 0
		&&	OnTreeGridQueryColumnOutline( nColNo )
		)
	{
		CRect rcOutlineArea(
			rcCellExtra.left,
			rcCellExtra.top,
			min(rcCell.left,rcCell.right),
			rcCellExtra.bottom
			);
		CDC & dcOutline = dc;
		COLORREF clrDots = PmBridge_GetPM()->GetColor( CExtPaintManager::CLR_3DSHADOW_OUT, (CObject*)this );
		HTREEITEM hRootItem = ItemGetRoot();
		__EXT_DEBUG_GRID_ASSERT( hRootItem != NULL );
		HTREEITEM hTreeItemFirst = ItemGetByVisibleRowIndex( nRowNo );
		if( hTreeItemFirst == NULL )
			return;
		CRect rcIndent( rcCell.left, rcCell.top, rcCell.left, rcCell.bottom );
		HTREEITEM hTreeItem = hTreeItemFirst;
		bool bDrawOutline =
			OnTreeGridQueryDrawOutline(
				dcOutline,
				nVisibleColNo,
				nVisibleRowNo,
				nColNo,
				nRowNo,
				rcCellExtra,
				rcCell,
				rcVisibleRange,
				dwAreaFlags,
				dwHelperPaintFlags
				);
		for( ; hTreeItem != hRootItem; hTreeItem = ItemGetParent(hTreeItem) )
		{
			__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
			CExtTreeGridCellNode * pNode =
				CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
			__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
			INT nPxIndent = (INT)pNode->TreeNodeIndentPxGet();
			if( nPxIndent == ULONG(-1L) )
				nPxIndent = GetTreeData().m_nIndentPxDefault;
			rcIndent.right = rcIndent.left;
			rcIndent.left -= nPxIndent;
			INT nMetricH = rcIndent.Width();
			INT nMetricH2 = nMetricH / 2;
			INT nMetricV = rcIndent.Height();
			INT nMetricV2 = min( nMetricH, nMetricV );
			nMetricV2 /= 2;
			bool bDrawUpperLineV = false;
			if( bDrawOutline && hTreeItemFirst == hTreeItem )
			{
				CExtPaintManager::stat_DrawDotLineH(
					dcOutline,
					rcIndent.left + nMetricH2,
					rcIndent.right,
					rcIndent.top + nMetricV2,
					clrDots
					);
				bDrawUpperLineV = true;
			}
			HTREEITEM hTreeItemNext = _ItemBrowseNextImpl( hTreeItem, true, false, false, false );
			if( bDrawOutline && hTreeItemNext != NULL )
			{
				CExtPaintManager::stat_DrawDotLineV(
					dcOutline,
					rcIndent.left + nMetricH2,
					rcIndent.top + nMetricV2,
					rcIndent.bottom,
					clrDots
					);
				bDrawUpperLineV = true;
			}
			if( bDrawOutline && bDrawUpperLineV )
				CExtPaintManager::stat_DrawDotLineV(
					dcOutline,
					rcIndent.left + nMetricH2,
					rcIndent.top,
					rcIndent.top + nMetricV2,
					clrDots
					);
			if( hTreeItemFirst == hTreeItem )
			{
				CExtTreeGridCellNode::e_expand_box_shape_t eEBS =
					pNode->TreeNodeGetExpandBoxShape();
				if( eEBS != CExtTreeGridCellNode::__EEBS_NONE )
				{
					CRect rcIndentPaint = rcIndent;
					if(		rcIndentPaint.left == rcIndentPaint.right
						&&	m_bAdustNullIndent
						)
					{
						INT nPxIndentPaint = nPxIndent;
						if( nPxIndentPaint == 0 )
							nPxIndentPaint = GetTreeData().m_nIndentPxDefault;
						rcIndentPaint.left -= nPxIndentPaint;
					}
					if( rcIndentPaint.left < rcIndentPaint.right )
						OnTreeGridPaintExpandButton(
							dcOutline,
							hTreeItem,
							( eEBS == CExtTreeGridCellNode::__EEBS_EXPANDED )
								? true : false,
							rcIndentPaint
							);
				}
			} // if( hTreeItemFirst == hTreeItem )
		} // for( ; hTreeItem != hRootItem; hTreeItem = ItemGetParent(hTreeItem) )
		// finally, over-paint top line
// 		bool bHighlightPressing = ( (dwHelperPaintFlags&(__EGCPF_HIGHLIGHTED_BY_PRESSED_COLUMN|__EGCPF_HIGHLIGHTED_BY_PRESSED_ROW)) != 0 ) ? true : false;
// 		bool bGridLinesHorz = false;
// 		if( ! bHighlightPressing )
// 			bGridLinesHorz =
// 				OnGbwQueryCellGridLines(
// 					true, dc,
// 					nVisibleColNo, nVisibleRowNo, nColNo, nRowNo,
// 					rcCellExtra, rcCell, rcVisibleRange, dwAreaFlags, dwHelperPaintFlags
// 					);
// 		if( bGridLinesHorz )
// 		{
// 			COLORREF clrFace = OnGbwQueryGridLinesColor();
// 			dc.FillSolidRect( rcCell.left - 1, rcCell.top - 1, rcCell.right - rcCell.left, 1, clrFace );
// 			dc.FillSolidRect( rcCell.left - 1, rcCell.bottom - 1, rcCell.right - rcCell.left, 1, clrFace );
// 		}
	} // if( ( dwAreaFlags & __EGBWA_INNER_CELLS ) != 0 ...
}

bool CExtTreeGridWnd::OnTreeGridQueryColumnOutline(
	LONG nColNo
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return (nColNo == 0) ? true : false;
}

void CExtTreeGridWnd::OnTreeGridToggleItemExpandedState(
	LONG nRowNo,
	CExtGridHitTestInfo * pHtInfo, // = NULL
	INT nActionTVE // = TVE_TOGGLE // TVE_COLLAPSE, TVE_EXPAND, TVE_TOGGLE only
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT(
			nActionTVE == TVE_COLLAPSE
		||	nActionTVE == TVE_EXPAND
		||	nActionTVE == TVE_TOGGLE
		);
	pHtInfo;
HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
	if( hTreeItem == NULL )
		return;
	ItemExpand( hTreeItem, nActionTVE );
}

bool CExtTreeGridWnd::OnGbwBeginEdit(
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcInplaceControl,
	bool bContinueMsgLoop, // = true
	__EXT_MFC_SAFE_LPCTSTR strStartEditText, // = NULL
	HWND hWndParentForEditor // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	m_nLastEndEditKey = 0;
bool bRetVal =
		CExtGridWnd::OnGbwBeginEdit(
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcInplaceControl,
			bContinueMsgLoop,
			strStartEditText,
			hWndParentForEditor
			);
	return bRetVal;
}

bool CExtTreeGridWnd::OnGbwAnalyzeCellMouseClickEvent(
	UINT nChar, // VK_LBUTTON, VK_RBUTTON or VK_MBUTTON only
	UINT nRepCnt, // 0 - button up, 1 - single click, 2 - double click, 3 - post single click & begin editing
	UINT nFlags, // mouse event flags
	CPoint point // mouse pointer in client coordinates
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( 0 <= nRepCnt && nRepCnt <= 3 );
	if( nChar == VK_LBUTTON )
	{
		CExtGridHitTestInfo htInfo( point );
		HitTest( htInfo, false, true );
		if(		htInfo.IsHoverEmpty()
			||	(! htInfo.IsValidRect() )
			)
			return false;
		INT nColType = htInfo.GetInnerOuterTypeOfColumn();
		INT nRowType = htInfo.GetInnerOuterTypeOfRow();
		if(		nColType == 0
			&&	nRowType == 0
			)
		{
			if( nRepCnt == 1 || nRepCnt == 2 )
			{
				//DWORD dwSiwStyle = SiwGetStyle();
				if(		(	(	nRepCnt == 1
							&&	OnTreeGridQueryColumnOutline( htInfo.m_nColNo )
							&&	(htInfo.m_dwAreaFlags&__EGBWA_TREE_OUTLINE_AREA) != 0
							)
						||	(	nRepCnt == 2
						//	&&	( dwSiwStyle & __EGBS_SFB_MASK ) == __EGBS_SFB_FULL_ROWS
							)
						)
					&&	(htInfo.m_dwAreaFlags&(__EGBWA_CELL_BUTTON|__EGBWA_CELL_CHECKBOX)) == 0
					)
				{
					HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( htInfo.m_nRowNo );
					if(		(	nRepCnt == 2
							&&	hTreeItem != NULL
							&&	ItemGetChildCount( hTreeItem ) > 0
							)
						||	(htInfo.m_dwAreaFlags&__EGBWA_TREE_BOX) != 0
						)
					{
						OnTreeGridToggleItemExpandedState(
							htInfo.m_nRowNo,
							&htInfo
							);
						return true;
					}
				}
			} // if( nRepCnt == 1 || nRepCnt == 2 )
		}
	} // if( nChar == VK_LBUTTON && ( nRepCnt == 1 || nRepCnt == 2 ) )
	if( CExtGridWnd::OnGbwAnalyzeCellMouseClickEvent(
			nChar,
			nRepCnt,
			nFlags,
			point
			)
		)
		return true;
	return false;
}

bool CExtTreeGridWnd::OnTreeGridQueryKbCollapseEnabled(
	HTREEITEM hTreeItem,
	LONG nColNo
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
	hTreeItem;
	nColNo;
	return true;
}

bool CExtTreeGridWnd::OnTreeGridQueryKbExpandEnabled(
	HTREEITEM hTreeItem,
	LONG nColNo
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
	hTreeItem;
	nColNo;
	return true;
}

bool CExtTreeGridWnd::OnTreeGridQueryKbJumpParentEnabled(
	HTREEITEM hTreeItem,
	LONG nColNo
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( hTreeItem != NULL );
	hTreeItem;
//	nColNo;
//	return true;
	if( nColNo < 0 )
		return true;
DWORD dwSiwStyles = SiwGetStyle();
	if( (dwSiwStyles & __EGBS_SFB_MASK) == __EGBS_SFB_CELLS )
	{
		if( OnTreeGridQueryColumnOutline( nColNo ) )
			return true;
		return false;
	}
	return true;
}

bool CExtTreeGridWnd::OnGbwAnalyzeCellKeyEvent(
	bool bKeyDownEvent, // true - key-down event, false - key-up event
	UINT nChar, // virtual key code
	UINT nRepCnt, // key-down/key-up press count
	UINT nFlags // key-down/key-up event flags
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( CExtGridWnd::OnGbwAnalyzeCellKeyEvent(
			bKeyDownEvent,
			nChar,
			nRepCnt,
			nFlags
			)
		)
		return true;
	if( bKeyDownEvent )
	{
		if( nChar == VK_LEFT )
		{
			HTREEITEM hTreeItem = ItemFocusGet();
			if( hTreeItem == NULL )
				return true;
			CExtTreeGridCellNode::e_expand_box_shape_t eEBT =
				ItemGetExpandBoxShape( hTreeItem );
			if( eEBT == CExtTreeGridCellNode::__EEBS_EXPANDED )
			{
				if( ! OnTreeGridQueryKbCollapseEnabled( hTreeItem, FocusGet().x ) )
					return false;
				ItemExpand( hTreeItem, TVE_COLLAPSE );
				return true;
			} // if( eEBT == CExtTreeGridCellNode::__EEBS_EXPANDED )
			HTREEITEM htiParent = ItemGetParent( hTreeItem );
			if(		htiParent == NULL
				||	htiParent == ItemGetRoot()
				)
				return false;
			if( ! OnTreeGridQueryKbJumpParentEnabled( hTreeItem, FocusGet().x ) )
				return false;
			ItemFocusSet( htiParent );
			return true;
		} // if( nChar == VK_LEFT )
		if( nChar == VK_RIGHT )
		{
			HTREEITEM hTreeItem = ItemFocusGet();
			if( hTreeItem == NULL )
				return true;
			CExtTreeGridCellNode::e_expand_box_shape_t eEBT =
				ItemGetExpandBoxShape( hTreeItem );
			if( eEBT != CExtTreeGridCellNode::__EEBS_COLLAPSED )
				return false;
			if( ! OnTreeGridQueryKbExpandEnabled( hTreeItem, FocusGet().x ) )
				return false;
			ItemExpand( hTreeItem, TVE_EXPAND );
			return true;
		} // if( nChar == VK_RIGHT )
		if( nChar == VK_SUBTRACT )
		{
			HTREEITEM hTreeItem = ItemFocusGet();
			if( hTreeItem == NULL )
				return true;
			CExtTreeGridCellNode::e_expand_box_shape_t eEBT =
				ItemGetExpandBoxShape( hTreeItem );
			if( eEBT != CExtTreeGridCellNode::__EEBS_EXPANDED )
				return true;
			ItemExpand( hTreeItem, TVE_COLLAPSE );
			return true;
		} // if( nChar == VK_SUBTRACT )
		if( nChar == VK_ADD )
		{
			HTREEITEM hTreeItem = ItemFocusGet();
			if( hTreeItem == NULL )
				return true;
			CExtTreeGridCellNode::e_expand_box_shape_t eEBT =
				ItemGetExpandBoxShape( hTreeItem );
			if( eEBT != CExtTreeGridCellNode::__EEBS_COLLAPSED )
				return true;
			ItemExpand( hTreeItem, TVE_EXPAND );
			return true;
		} // if( nChar == VK_ADD )
	} // if( bKeyDownEvent )
	return false;
}

void CExtTreeGridWnd::OnGbwAdjustRects(
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	RECT & rcCellExtraA,
	RECT & rcCellA
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	rcCellExtraA;
	if( 	m_nGbwAdjustRectsLock == 0
		&&	nRowType == 0
		&&	nColType == 0
		&&	OnTreeGridQueryColumnOutline( nColNo )
		)
	{
		if( nRowNo < 0L )
			return;
		LONG nRowCount = RowCountGet();
		if( nRowNo >= nRowCount )
			return;
		ULONG nRowIndentPx = _GetTreeData().TreeGetRowIndentPx( ULONG(nRowNo) );
		rcCellA.left += nRowIndentPx;
	} // if( m_nGbwAdjustRectsLock == 0 ...
}

void CExtTreeGridWnd::OnSwUpdateWindow()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_nGbwAdjustRectsLock != 0 )
		return;
	CExtGridWnd::OnSwUpdateWindow();
}

LONG CExtTreeGridWnd::RowCountGet() const
{
	(*((LONG*)(&m_nCountOfRows))) =
		LONG( _GetTreeData().TreeGetDisplayedCount() );
	return m_nCountOfRows;
}

BEGIN_MESSAGE_MAP( CExtTreeGridWnd, CExtGridWnd )
	//{{AFX_MSG_MAP(CExtTreeGridWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

INT CExtTreeGridWnd::OnSiwQueryItemExtentH(
	LONG nColNo,
	INT * p_nExtraSpaceBefore, // = NULL
	INT * p_nExtraSpaceAfter   // = NULL
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		CExtGridWnd::OnSiwQueryItemExtentH(
			nColNo,
			p_nExtraSpaceBefore,
			p_nExtraSpaceAfter
			);
}

INT CExtTreeGridWnd::OnSiwQueryItemExtentV(
	LONG nRowNo,
	INT * p_nExtraSpaceBefore, // = NULL
	INT * p_nExtraSpaceAfter   // = NULL
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT( nRowNo >= 0 );
	if( p_nExtraSpaceBefore != NULL )
		(*p_nExtraSpaceBefore) = 0;
	if( p_nExtraSpaceAfter != NULL )
		(*p_nExtraSpaceAfter) = 0;
	if( OnTreeQueryPlainGridModeVeticalLayout() )
		return
			CExtGridWnd::OnSiwQueryItemExtentV(
				nRowNo,
				p_nExtraSpaceBefore,
				p_nExtraSpaceAfter
				);
HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
	if( hTreeItem == NULL )
		return
			CExtGridWnd::OnSiwQueryItemExtentV(
				nRowNo,
				p_nExtraSpaceBefore,
				p_nExtraSpaceAfter
				);
CExtTreeGridCellNode * pCell = CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
INT nItemExtent, nExtraSpaceAfter = 0, nExtraSpaceBefore = 0;
	if( ! pCell->ExtentGet( nItemExtent, 0 ) )
		return
			CExtGridWnd::OnSiwQueryItemExtentV(
				nRowNo,
				p_nExtraSpaceBefore,
				p_nExtraSpaceAfter
				);
	pCell->ExtraSpaceGet( nExtraSpaceAfter, true );
	pCell->ExtraSpaceGet( nExtraSpaceBefore, false );
	__EXT_DEBUG_GRID_ASSERT( nItemExtent >= 0 );
	__EXT_DEBUG_GRID_ASSERT( nExtraSpaceAfter >= 0 );
	__EXT_DEBUG_GRID_ASSERT( nExtraSpaceBefore >= 0 );
	if( p_nExtraSpaceBefore != NULL )
		(*p_nExtraSpaceBefore) = nExtraSpaceBefore;
	if( p_nExtraSpaceAfter != NULL )
		(*p_nExtraSpaceAfter) = nExtraSpaceAfter;
	return (nItemExtent+nExtraSpaceAfter+nExtraSpaceBefore);
}

bool CExtTreeGridWnd::OnTreeQueryPlainGridModeVeticalLayout() const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if(		FixedSizeRowsGet()
		||	(	(SiwGetStyle()&(__EGBS_RESIZING_CELLS_OUTER_V|__EGBS_RESIZING_CELLS_INNER_V)) != 0
			&&	(	OuterColumnCountLeftGet() > 0
				||	OuterColumnCountRightGet() > 0
				)
			)
		)
		return true;
	else
		return false;
}

bool CExtTreeGridWnd::OnGbwCanResizeColumn(
	LONG nColNo,
	INT * p_nExtentMin, // = NULL
	INT * p_nExtentMax  // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	return
		CExtGridWnd::OnGbwCanResizeColumn(
			nColNo,
			p_nExtentMin,
			p_nExtentMax
			);
}

bool CExtTreeGridWnd::OnGbwCanResizeRow(
	LONG nRowNo,
	INT * p_nExtentMin, // = NULL
	INT * p_nExtentMax  // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( FixedSizeRowsGet() )
		return
			CExtGridWnd::OnGbwCanResizeRow(
				nRowNo,
				p_nExtentMin,
				p_nExtentMax
				);
INT nExtentMin = 0, nExtentMax = 32767;
	__EXT_DEBUG_GRID_ASSERT( nRowNo >= 0 );
HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
	if( hTreeItem == NULL )
		return
			CExtGridWnd::OnGbwCanResizeRow(
				nRowNo,
				p_nExtentMin,
				p_nExtentMax
				);
CExtTreeGridCellNode * pCell = CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
	if( pCell->ExtentGet( nExtentMin, -1 ) )
	{
		__EXT_DEBUG_GRID_VERIFY( pCell->ExtentGet( nExtentMax, 1 ) );
	}
	__EXT_DEBUG_GRID_ASSERT(
			nExtentMin >=  0
		&&	nExtentMax >= 0
		&&	nExtentMin <= nExtentMax
		);
	if( p_nExtentMin != NULL )
		(*p_nExtentMin) = nExtentMin;
	if( p_nExtentMax != NULL )
		(*p_nExtentMax) = nExtentMax;
bool bRetVal = (nExtentMin != nExtentMax) ? true : false;
	return bRetVal;
}

void CExtTreeGridWnd::OnDataProviderSortEnter(
	bool bColumns // true = sorting/swapping columns, false - rows
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bColumns;
}

void CExtTreeGridWnd::OnDataProviderSortLeave(
	bool bColumns // true = sorting/swapping columns, false - rows
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bColumns;
}

void CExtTreeGridWnd::OnDataProviderSwapSeries(
	bool bColumns, // true = sorting/swapping columns, false - rows
	LONG nRowColNo1,
	LONG nRowColNo2,
	LONG nSwapCounter
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bColumns;
	nRowColNo1;
	nRowColNo2;
	nSwapCounter;
}

void CExtTreeGridWnd::OnDataProviderSwapDroppedSeries(
	bool bColumns, // true = swapping columns, false - rows
	LONG nRowColNoSrc,
	LONG nRowColNoDropBefore
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bColumns;
	nRowColNoSrc;
	nRowColNoDropBefore;
}

void CExtTreeGridWnd::OnTreeDataProviderSortEnter()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

void CExtTreeGridWnd::OnTreeDataProviderSortLeave()
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
}

void CExtTreeGridWnd::OnTreeDataProviderSwapSeries(
	HTREEITEM hTreeItem1,
	HTREEITEM hTreeItem2,
	LONG nSwapCounter
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	hTreeItem1;
	hTreeItem2;
	nSwapCounter;
}

void CExtTreeGridWnd::OnBestFitAdjustColumn(
	INT & nBFE,
	const CExtGridCell * pCell,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
	CExtGridWnd::OnBestFitAdjustColumn(
		nBFE,
		pCell,
		nColNo,
		nRowNo,
		nColType,
		nRowType
		);
	if(		nColType == 0
		&&	nRowType == 0
		&&	OnTreeGridQueryColumnOutline( nColNo )
		)
	{
		ULONG nRowIndentPx = _GetTreeData().TreeGetRowIndentPx( ULONG(nRowNo) );
		nBFE += INT(nRowIndentPx);
	}
}

void CExtTreeGridWnd::OnBestFitAdjustRow(
	INT & nBFE,
	const CExtGridCell * pCell,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
	CExtGridWnd::OnBestFitAdjustRow(
		nBFE,
		pCell,
		nColNo,
		nRowNo,
		nColType,
		nRowType
		);
}

bool CExtTreeGridWnd::RowHide(
	LONG nRowNo,
	LONG nHideCount, // = 1
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nRowNo;
	nHideCount;
	bRedraw;
//	__EXT_DEBUG_GRID_ASSERT( FALSE );
	// This method can not be used with the CExtTreeGridWnd class and derived from it classes.
	return false;
}

bool CExtTreeGridWnd::RowUnHideAll(
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bRedraw;
//	__EXT_DEBUG_GRID_ASSERT( FALSE );
	// This method can not be used with the CExtTreeGridWnd class and derived from it classes.
	return false;
}

void CExtTreeGridWnd::OnGridFilterCalcRowIndicesForFilteringColumns(
	LONG nScanColNo,
	bool bLeft,
	CList < LONG, LONG & > & _listIndices
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nScanColNo;
	bLeft;
	_listIndices;
	// Column filtering is not supported the CExtTreeGridWnd class and derived from it classes.
}

/*
bool CExtTreeGridWnd::OnGridFilterSortCommand(
	LONG nRowColNo,
	bool bSortColumns,
	bool bAscending,
	bool bUpdateExistingSortOrder
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nRowColNo;
	bSortColumns;
	bAscending;
	bUpdateExistingSortOrder;
	return true;
}
*/

void CExtTreeGridWnd::UpdateStaticFilterValueForOuterColumn(
	LONG nRowNo,
	bool bLeft, // = true
	LONG nColNo // = 0L
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nRowNo;
	bLeft;
	nColNo;
	// This method should not be invoked for the CExtTreeGridWnd class and derived from it classes.
	__EXT_DEBUG_GRID_ASSERT( FALSE );
}

void CExtTreeGridWnd::UpdateStaticFilterValueForOuterRow(
	LONG nColNo,
	bool bTop, // = true
	LONG nRowNo // = 0L
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( nColNo < 0 )
	{
		LONG nColCount = ColumnCountGet();
		for( nColNo = 0L; nColNo < nColCount; nColNo++ )
			UpdateStaticFilterValueForOuterRow( nColNo, bTop, nRowNo );
		return;
	}
CExtGridCell * pCell = GridCellGet( nColNo, nRowNo, 0, bTop ? -1 : 1, RUNTIME_CLASS(CExtGridCellHeaderFilter) );
	if( pCell == NULL )
		return;
	__EXT_DEBUG_GRID_ASSERT_VALID( pCell );
CExtGridCellHeaderFilter * pHeaderFilterCell = DYNAMIC_DOWNCAST( CExtGridCellHeaderFilter, pCell );
	if( pHeaderFilterCell == NULL )
		return;
	pHeaderFilterCell->m_arrStaticStringValues.RemoveAll();
	OnTreeGridFilterCalcPlainUniqueValuesForColumn(
		NULL,
		pHeaderFilterCell->m_arrStaticStringValues,
		nColNo,
		false
		);
}

void CExtTreeGridWnd::OnTreeGridFilterCalcPlainUniqueValuesForColumn(
	HTREEITEM hti,
	CExtArray < CExtSafeString > & arrUniqueStringValues,
	LONG nColNo,
	bool bLeavesOnly,
	bool bIncludeHidden // = true
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hti == NULL )
		hti = ItemGetRoot();
	else
	{ // put value from hti item into arrUniqueStringValues array
		if( ! bIncludeHidden )
		{
			bool bHidden = ItemIsHidden( hti );
			if( bHidden )
				return;
		} // if( ! bIncludeHidden )
		if( ( ! bLeavesOnly ) || ItemGetChildCount( hti ) == 0 )
		{
			const CExtGridCell * pCell = ItemGetCell( hti, nColNo );
			if( pCell != NULL )
			{
				CExtSafeString str( _T("") );
				pCell->TextGet( str );
				arrUniqueStringValues.InsertUnique( str );
			}
		} // if( ( ! bLeavesOnly ) || ItemGetChildCount( hti ) > 0 )
	} // put value from hti item into arrUniqueStringValues array

LONG nChildIndex, nChildCount = ItemGetChildCount( hti );
	for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
	{
		HTREEITEM htiChild = ItemGetChildAt( hti, nChildIndex );
		__EXT_DEBUG_GRID_ASSERT( htiChild != NULL );
		OnTreeGridFilterCalcPlainUniqueValuesForColumn(
			htiChild,
			arrUniqueStringValues,
			nColNo,
			bLeavesOnly,
			bIncludeHidden
			);
	}
}

bool CExtTreeGridWnd::OnGridFilterCalcPlainUniqueValuesForColumn(
	CExtArray < CExtSafeString > & arrUniqueStringValues,
	LONG nColNo
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	OnTreeGridFilterCalcPlainUniqueValuesForColumn(
		NULL,
		arrUniqueStringValues,
		nColNo,
		false
		);
	return true;
}

bool CExtTreeGridWnd::OnGridFilterCalcPlainUniqueValuesForRow(
	CExtArray < CExtSafeString > & arrUniqueStringValues,
	LONG nRowNo
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	arrUniqueStringValues;
	nRowNo;
	// Column filtering is not supported the CExtTreeGridWnd class and derived from it classes.
	return false;
}

void CExtTreeGridWnd::OnGridFilterUpdateForRows(
	LONG nScanRowNo,
	LONG nEventSrcColNo,
	bool bTop,
	CExtGridCellHeaderFilter * pHeaderFilterCell,
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	nEventSrcColNo;
	pHeaderFilterCell;
LONG nFocusedColNo = FocusGet().x;
HTREEITEM htiFocus = ItemFocusGet();
	SelectionUnset( false, false );
	FocusUnset( false );
CList < LONG, LONG & > _listIndices;
	OnAdjustTreeRowFilteredState( NULL, _listIndices, nScanRowNo, bTop, 0, false ); // un-hide all tree rows
	OnGridFilterCalcColumnIndicesForFilteringRows( nScanRowNo, bTop, _listIndices ); // compute columns set
	if( _listIndices.GetCount() > 0 )
		OnAdjustTreeRowFilteredState( NULL, _listIndices, nScanRowNo, bTop, 0, false ); // filter all tree rows
	if(		htiFocus != NULL
		&&	ItemIsDisplayed( htiFocus )
		)
		ItemFocusSet( htiFocus, nFocusedColNo, false );
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	}
}

void CExtTreeGridWnd::OnAdjustTreeRowFilteredState(
	HTREEITEM hti,
	CList < LONG, LONG & > & _listIndices,
	LONG nScanRowNo,
	bool bTop,
	INT nIndentLevel,
	bool bLeavesOnly
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hti == NULL )
		hti = ItemGetRoot();
	else if( _listIndices.GetCount() > 0 )
	{ // filter the hti item
		bool bHide = false;
		if( (! bLeavesOnly ) || ItemGetChildCount( hti ) == 0 )
		{
			POSITION pos = _listIndices.GetHeadPosition();
			for( ; pos != NULL; )
			{
				LONG nColNo = _listIndices.GetNext( pos );
				CExtGridCellHeaderFilter * pHeaderFilterCell =
					STATIC_DOWNCAST(
						CExtGridCellHeaderFilter,
						GridCellGet(
							nColNo,
							nScanRowNo,
							0,
							bTop ? -1 : 1
							)
						);
				CExtGridCell * pDataCell = ItemGetCell( hti, nColNo );
				if(		pDataCell != NULL
					&&	pHeaderFilterCell->OnQueryCellFilteredState( pDataCell )
					)
				{
					bHide = true;
					break;
				}
			} // for( ; pos != NULL; )
		} // if( (! bLeavesOnly ) || ItemGetChildCount( hti ) == 0 )
		ItemHide( hti, bHide, false );
		if( bHide )
			return;
	} // filter the hti item
	else
	{ // un-hide the hti item
		ItemHide( hti, false, false );
	} // un-hide the hti item
LONG nChildIndex, nChildCount = ItemGetChildCount( hti );
	for( nChildIndex = 0L; nChildIndex < nChildCount; nChildIndex ++ )
	{
		HTREEITEM htiChild = ItemGetChildAt( hti, nChildIndex );
		__EXT_DEBUG_GRID_ASSERT( htiChild != NULL );
		OnAdjustTreeRowFilteredState( htiChild, _listIndices, nScanRowNo, bTop, nIndentLevel + 1, bLeavesOnly );
	}
}

bool CExtTreeGridWnd::FilterStateForOuterColumnSerialize(
	CArchive & ar,
	bool bLeft, // = true
	LONG nColNo, // = 0L
	bool bRedrawAfterLoading, // = true
	bool bEnableThrowExcetions // = false
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	ar;
	bLeft;
	nColNo;
	bRedrawAfterLoading;
	bEnableThrowExcetions;
	__EXT_DEBUG_GRID_ASSERT( FALSE );
	// This method can not be used with the CExtTreeGridWnd class and derived from it classes.
	return false;
}

bool CExtTreeGridWnd::OnGridFilterQueryFilteredState(
	bool bFilteringRows,
	LONG nRowColNo, // = -1 // if -1 - ignore nRowColNo, check all ranges at both outer sides and fill p_nRowColNoFound & p_bTopLeftFound
	bool bTopLeft, // = true
	LONG * p_nRowColNoFound, // = NULL
	bool * p_bTopLeftFound // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( ! bFilteringRows )
		return false;
	return
		CExtGridWnd::OnGridFilterQueryFilteredState(
			bFilteringRows,
			nRowColNo,
			bTopLeft,
			p_nRowColNoFound,
			p_bTopLeftFound
			);
}

void CExtTreeGridWnd::FilterStateClearAll(
	bool bClearAtTop, // = true
	bool bClearAtBottom, // = true
	bool bClearAtLeft, // = true
	bool bClearAtRight // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	bClearAtLeft;
	bClearAtRight;
CList < LONG, LONG & > _listIndices;
	OnAdjustTreeRowFilteredState( NULL, _listIndices, 0L, true, 0, false ); // un-hide all tree rows
	CExtGridWnd::FilterStateClearAll(
		bClearAtTop,
		bClearAtBottom,
		false,
		false
		);
}

bool CExtTreeGridWnd::ItemIsDisplayed(
	HTREEITEM hti
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hti == NULL )
		return false;
CExtTreeGridCellNode * pNode =
		CExtTreeGridCellNode::FromHTREEITEM(hti);
	__EXT_DEBUG_GRID_ASSERT_VALID( pNode );
bool bDisplayed = pNode->TreeNodeIsDisplayed();
	return bDisplayed;
}

bool CExtTreeGridWnd::ItemIsHidden(
	HTREEITEM hti
	) const
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hti == NULL || hti == ItemGetRoot() )
		return false;
bool bHidden = CExtTreeGridCellNode::FromHTREEITEM( hti ) -> TreeNodeHiddenGet();
	return bHidden;
}

void CExtTreeGridWnd::ItemHide(
	HTREEITEM hti,
	bool bHide,
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hti == NULL || hti == ItemGetRoot() )
		return;
CExtTreeGridCellNode * pNode = CExtTreeGridCellNode::FromHTREEITEM( hti );
bool bAlreadyHidden =  pNode->TreeNodeHiddenGet();
	if(		( bAlreadyHidden && bHide )
		||	( (!bAlreadyHidden) && (!bHide) )
		)
		return;
LONG nFocusedColumn = FocusGet().x;
HTREEITEM htiFocus = ItemFocusGet();
CExtTreeGridDataProvider & _DP = _GetTreeData();
	pNode->TreeNodeHiddenSet( _DP, bHide );
	if( htiFocus != NULL )
	{
		bool bUnsetFocus = false;
		if( bHide )
		{
			if( htiFocus == hti )
				bUnsetFocus = true;
			else
			{
				HTREEITEM hti2 = ItemGetParent( htiFocus );
				for( ; hti2 != NULL; hti2 = ItemGetParent( hti2 ) )
				{
					if( hti2 == hti )
					{
						bUnsetFocus = true;
						break;
					}
				}
			}
		}
		if( bUnsetFocus )
			FocusUnset( false );
		else
			ItemFocusSet( htiFocus, nFocusedColumn, false );
	}
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw )
}

void CExtTreeGridWnd::ItemHideSubTree(
	HTREEITEM hti,
	bool bHideItem,
	bool bHideChildren,
	bool bRedraw // = true
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( hti == NULL )
		return;
bool bCheckItem = true;
	if( hti == ItemGetRoot() )
		bCheckItem = false;
bool bAny = false;
LONG nIndex, nCount = ItemGetChildCount( hti );
	for( nIndex = 0L; nIndex < nCount; nIndex ++ )
	{
		HTREEITEM htiChild = ItemGetChildAt( hti, nIndex );
		ItemHide( htiChild, bHideChildren, false );
		bAny = true;
	}
	if( bCheckItem )
	{
		ItemHide( hti, bHideItem, false );
		bAny = true;
	}
	if( bRedraw && bAny )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw )
}

CPoint CExtTreeGridWnd::FocusSet(
	const POINT & ptNewFocus,
	bool bEnsureVisibleColumn, // = true
	bool bEnsureVisibleRow, // = true
	bool bResetSelectionToFocus, // = true
	bool bRedraw, // = true
	bool * p_bCanceled // = NULL
	)
{
	__EXT_DEBUG_GRID_ASSERT_VALID( this );
	if( m_bSingleExpandMode )
	{
		if( ItemFocusIsLocked() )
		{
			if( p_bCanceled != NULL )
				(*p_bCanceled) = true;
			return ptNewFocus;
		}
	}
CPoint ptOldFocus( FocusGet() );
bool bCanceled = false;
CPoint ptRetVal = CExtGridWnd::FocusSet( ptNewFocus, bEnsureVisibleColumn, bEnsureVisibleRow, bResetSelectionToFocus, bRedraw, &bCanceled );
	if( m_bSingleExpandMode )
	{
		if( ptNewFocus.y != ptOldFocus.y )
		{
			HTREEITEM htiFocusNew = ItemFocusGet(), htiFocusOld = ( ptOldFocus.y >= 0 ) ? ItemGetByVisibleRowIndex( ptOldFocus.y ) : NULL;
			ItemFocusLock( true );
			bool bAnyChanges = false;
			if( htiFocusOld != NULL && htiFocusNew != NULL && ItemIsExpanded( htiFocusOld ) )
			{
				HTREEITEM htiRoot = ItemGetRoot();
				bool bDoCollapsePrevious = false;
				if( ptNewFocus.y < ptOldFocus.y )
					bDoCollapsePrevious = true;
				else
				{
					bDoCollapsePrevious = true;
					HTREEITEM htiTest = ItemGetParent( htiFocusNew );
					for( ; htiTest != htiRoot; htiTest = ItemGetParent( htiTest ) )
					{
						if( htiTest == htiFocusOld )
						{
							bDoCollapsePrevious = false;
							break;
						}
					}
				}
				if( bDoCollapsePrevious )
				{
					bAnyChanges = true;
					ItemExpand( htiFocusOld, TVE_COLLAPSE, false );
					HTREEITEM hti = ItemGetParent( htiFocusOld );
					for( ; hti != htiRoot; hti = ItemGetParent( hti ) )
					{
						bool bDoCollapseLevel = true;
						HTREEITEM htiTest = ItemGetParent( htiFocusNew );
						for( ; htiTest != htiRoot; htiTest = ItemGetParent( htiTest ) )
						{
							if( htiTest == hti )
							{
								bDoCollapseLevel = false;
								break;
							}
						}
						if( ! bDoCollapseLevel )
							break;
						ItemExpand( hti, TVE_COLLAPSE, false );
					}
					bCanceled = true;
				}
			}
			if( htiFocusNew != NULL && ( ! ItemIsExpanded( htiFocusNew ) ) )
			{
				bAnyChanges = true;
				ItemExpand( htiFocusNew, TVE_EXPAND, false );
			}
			ItemFocusLock( false );
			if( bAnyChanges )
			{
				if( htiFocusNew != NULL )
				{
					SelectionUnset( false, false );
					OnSwUpdateScrollBars();
					ItemFocusSet( htiFocusNew, false );
					SelectionUnset( true, bRedraw );
				}
				else if( bRedraw )
					OnSwInvalidate( true );
			}
		}
	}
	if( p_bCanceled != NULL )
		(*p_bCanceled) = bCanceled;
	return ptRetVal;
}

#endif // (!defined __EXT_MFC_NO_TREEGRIDWND)


