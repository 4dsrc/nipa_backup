// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_CONTROLS_SHELL_H)
	#include <ExtControlsShell.h>
#endif

#if (!defined __EXT_RICH_CONTENT_H)
	#include <ExtRichContent.h>
#endif // (!defined __EXT_RICH_CONTENT_H)

#if (!defined __EXT_BUTTON_H)
	#include <ExtButton.h>
#endif

#if (!defined __EXT_EXTINTEGRITYCHECKSUM_H)
	#include <../Src/ExtIntegrityCheckSum.h>
#endif

#include <io.h>

#include <Resources/Resource.h>

#if _MFC_VER < 0x700
	#include <../src/AfxImpl.h>
#else
	#include <../src/mfc/AfxImpl.h>
#endif

#if (! defined __EXT_MFC_SHELL_MENU_RESOURCE_ID )
	#define __EXT_MFC_SHELL_MENU_RESOURCE_ID 32777
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#if ( ! defined __EXT_MFC_NO_SHELL )

#if (defined __EXT_MFC_SWDC_USES_THREAD_POOL)

/////////////////////////////////////////////////////////////////////////////
// CExtSWDC_ThreadPool

CExtSWDC_ThreadPool::CExtSWDC_ThreadPool_Ptr g_ShellThreadPool;

CExtSWDC_ThreadPool::CExtSWDC_ThreadPool()
	: m_dwThreadCreateFlags( 0 )
	, m_nThreadStackSize( 0 )
	, m_pThreadSecurityAttributes( NULL )
{
}

CExtSWDC_ThreadPool::~CExtSWDC_ThreadPool()
{
	// check all the watchers were stopped correctly
	ASSERT( m_mapWatcherToThread.GetCount() == 0 );
	ASSERT( m_mapThreadCounts.GetCount() == 0 );
	ASSERT( m_listThreadReceptors.GetCount() == 0 );
}

bool CExtSWDC_ThreadPool::Start( CExtSWDC * pSWDC )
{
	if( pSWDC == NULL )
	{
		ASSERT( FALSE );
		return false;
	}
	return Start( *pSWDC );
}

bool CExtSWDC_ThreadPool::Start( CExtSWDC & _SWDC )
{
CSingleLock _SL( &m_CS, TRUE );
CExtSWDC_Thread * pThread = NULL;
	if( m_mapWatcherToThread.Lookup( &_SWDC, pThread ) )
	{
		ASSERT( pThread != NULL );
		ASSERT( pThread->IsPresent( _SWDC ) );
		return true;
	}
bool bReceptor = false;
	if( m_listThreadReceptors.GetCount() > 0 )
	{
		bReceptor = true;
		pThread = m_listThreadReceptors.GetHead();
	}
	else
	{
		try
		{
			pThread = new CExtSWDC_Thread( *this );
//TRACE1( "+thread %p\r\n", LPVOID(pThread) );
		}
		catch( CException * pException )
		{
			pException->Delete();
			return false;
		}
		catch( ... )
		{
			return false;
		}
	}
	pThread->Add( _SWDC );
	m_mapWatcherToThread.SetAt( &_SWDC, pThread );
	if( bReceptor )
	{
		if( ! pThread->CanAdd() )
			m_listThreadReceptors.RemoveHead();
	}
	else
	{
		if( pThread->CanAdd() )
			m_listThreadReceptors.AddHead( pThread );
	}
INT nBusyCount = pThread->GetBusyCount();
	ASSERT( 1 <= nBusyCount && nBusyCount <= __EXT_MFC_SWDC_TP_OBJECT_WATCH_COUNT );
	m_mapThreadCounts.SetAt( pThread, nBusyCount );
	return true;
}

bool CExtSWDC_ThreadPool::Stop( CExtSWDC * pSWDC )
{
	if( pSWDC == NULL )
	{
		ASSERT( FALSE );
		return false;
	}
	return Stop( *pSWDC );
}

bool CExtSWDC_ThreadPool::Stop( CExtSWDC & _SWDC )
{
CSingleLock _SL( &m_CS, TRUE );
CExtSWDC_Thread * pThread = NULL;
	if( ! m_mapWatcherToThread.Lookup( &_SWDC, pThread ) )
		return true;
	ASSERT( pThread != NULL );
	ASSERT( pThread->IsPresent( _SWDC ) );
	pThread->Remove( _SWDC );
	// m_mapWatcherToThread, m_mapThreadCounts and m_listThreadReceptors are modified from thread
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtSWDC_ThreadPool::CExtSWDC_Thread

CExtSWDC_ThreadPool::CExtSWDC_Thread::CExtSWDC_Thread(
	CExtSWDC_ThreadPool & _ThreadPool
	)
	: m_ThreadPool( _ThreadPool )
	, m_eventRequest( FALSE, FALSE, NULL, NULL )
	, m_eventResponse( FALSE, FALSE, NULL, NULL )
{
}

CExtSWDC_ThreadPool::CExtSWDC_Thread::~CExtSWDC_Thread()
{
	ASSERT( m_listSWDC.GetCount() == 0 ); // check everything was removed correctly
}

BOOL CExtSWDC_ThreadPool::CExtSWDC_Thread::InitInstance()
{
	CWinThread::InitInstance();
bool bReInitialize = true;
CExtSWDC * arrSWDC[ __EXT_MFC_SWDC_TP_OBJECT_WATCH_COUNT ];
	::memset( arrSWDC, 0, sizeof( arrSWDC ) );
HANDLE arrHandles[ __EXT_MFC_SWDC_TP_OBJECT_COUNT ];
INT nIndex;
	for( nIndex = 0; nIndex < __EXT_MFC_SWDC_TP_OBJECT_COUNT; nIndex ++ )
		arrHandles[ nIndex ] = INVALID_HANDLE_VALUE;
INT nBusyCount = 0;
DWORD dwWaitCount = 0;
DWORD dwWaitResult = WAIT_OBJECT_0;
bool bReIndexingIsRequired = false;
	for( ; true; )
	{
		if( bReInitialize )
		{
			CSingleLock _SL( &m_CS, TRUE );
			//CSingleLock _SLTP( &m_ThreadPool.m_CS, TRUE );
			nBusyCount = GetBusyCount();
			if( nBusyCount == 0 )
				break; // exit thread (nothing to watch)
			ASSERT( 0 < nBusyCount && nBusyCount <= __EXT_MFC_SWDC_TP_OBJECT_WATCH_COUNT );
			bReInitialize = false;
			dwWaitCount = DWORD( __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT + nBusyCount );
			arrHandles[ 0 ] = m_eventRequest;
			ASSERT( arrHandles[ 0 ] != NULL );
			POSITION pos = m_listSWDC.GetHeadPosition();
			::memset( arrSWDC, 0, sizeof( arrSWDC ) );
			bReIndexingIsRequired = false;
			for( nIndex = 0; nIndex < nBusyCount; nIndex ++ )
			{
				HANDLE hFCN = arrHandles[ __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT + nIndex ];
				if( hFCN != INVALID_HANDLE_VALUE )
				{
					::FindCloseChangeNotification( hFCN );
					arrHandles[ __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT + nIndex ] = hFCN = INVALID_HANDLE_VALUE;
				}
				ASSERT( pos != NULL );
				CExtSWDC * pSWDC = m_listSWDC.GetNext( pos );
				ASSERT( pSWDC != NULL );
				CExtSafeString strItemPath = pSWDC->WatchDirChanges_GetWatchPath();
				hFCN = ::FindFirstChangeNotification(  LPCTSTR(strItemPath), FALSE, pSWDC->m_dwNotifyFilter );
				arrHandles[ __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT + nIndex ] = hFCN;
				if( hFCN == INVALID_HANDLE_VALUE )
				{
					// break; // exit thread (re-initialization incomplete)
					arrSWDC[ nIndex ] = NULL;
					bReIndexingIsRequired = true;
//TRACE1("+error watching \"%s\"\r\n",LPCTSTR(strItemPath));
				}
				else
				{
					arrSWDC[ nIndex ] = pSWDC;
				}
			}
			if( nIndex != nBusyCount )
				break; // exit thread (re-initialization incomplete)
			m_eventResponse.SetEvent();
		} // if( bReInitialize )
		if( bReIndexingIsRequired )
		{
			HANDLE arrHandlesReIndexed[ __EXT_MFC_SWDC_TP_OBJECT_COUNT ];
			INT arrReIndex[ __EXT_MFC_SWDC_TP_OBJECT_COUNT ];
			for( nIndex = 0; nIndex < __EXT_MFC_SWDC_TP_OBJECT_COUNT; nIndex ++ )
			{
				arrHandlesReIndexed[ nIndex ] = INVALID_HANDLE_VALUE;
				arrReIndex[ nIndex ] = -1;
			}
			for( nIndex = 0; nIndex < __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT; nIndex ++ )
			{
				arrHandlesReIndexed[ nIndex ] = arrHandles[ nIndex ];
				arrReIndex[ nIndex ] = nIndex;
			}
			dwWaitCount = __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT;
			for( nIndex = 0; nIndex < nBusyCount; nIndex ++ )
			{
				HANDLE hObject = arrHandles[ nIndex + __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT ];
				if( hObject != INVALID_HANDLE_VALUE )
				{
					arrHandlesReIndexed[ INT(dwWaitCount) ] =
						arrHandles[ nIndex + __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT ];
					arrReIndex[ INT(dwWaitCount) ] = nIndex + __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT;
					dwWaitCount++;
				}
			}
			ASSERT( dwWaitCount >= __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT );
			dwWaitResult = ::WaitForMultipleObjects( dwWaitCount, arrHandlesReIndexed, FALSE, INFINITE );
			if(		DWORD( WAIT_OBJECT_0 ) <= dwWaitResult
				&&	dwWaitResult < DWORD( WAIT_OBJECT_0 + dwWaitCount )
				)
			{
				nIndex = arrReIndex[ INT( dwWaitResult - WAIT_OBJECT_0 ) ];
				dwWaitResult = DWORD( nIndex + WAIT_OBJECT_0 );
				ASSERT(
						DWORD( WAIT_OBJECT_0 ) <= dwWaitResult
					&&	dwWaitResult < DWORD( WAIT_OBJECT_0 + /*dwWaitCount*/ __EXT_MFC_SWDC_TP_OBJECT_COUNT )
					);
			}
		} // if( bReIndexingIsRequired )
		else
			dwWaitResult = ::WaitForMultipleObjects( dwWaitCount, arrHandles, FALSE, INFINITE );
		if( dwWaitResult == WAIT_OBJECT_0 )
		{
			bReInitialize = true;
			continue;
		}
		else if(	DWORD( WAIT_OBJECT_0 + __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT ) <= dwWaitResult
			&&		dwWaitResult < DWORD( WAIT_OBJECT_0 + __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT + nBusyCount )
			)
		{
			CSingleLock _SL( &m_CS, TRUE );
			//CSingleLock _SLTP( &m_ThreadPool.m_CS, TRUE );
			nIndex = INT( dwWaitResult - DWORD( WAIT_OBJECT_0 + __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT ) );
			ASSERT( 0 <= nIndex && nIndex < nBusyCount );
			CExtSWDC * pSWDC = arrSWDC[ nIndex ];
			ASSERT( pSWDC != NULL );
			HWND hWnd = pSWDC->WatchDirChanges_GetSafeHwnd();
			if( hWnd != NULL )
			{
				if( pSWDC->m_bSubFolder )
					::PostMessage( hWnd, CExtShellBase::g_nMsgChangeChildrenState, WPARAM(pSWDC), 0L );
				else
					::PostMessage( hWnd, CExtShellBase::g_nMsgUpdateItemContent, WPARAM(pSWDC), 0L );
				HANDLE hFCN = arrHandles[ __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT + nIndex ];
				ASSERT( hFCN != INVALID_HANDLE_VALUE );
				if( ! ::FindNextChangeNotification( hFCN ) )
				{
					::FindCloseChangeNotification( hFCN );
					arrHandles[ __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT + nIndex ] = INVALID_HANDLE_VALUE;
					// some error occurred, try to re-initialize
					bReInitialize = true;
					continue;
				} // if( ! ::FindNextChangeNotification( hFCN ) )
			} // if( hWnd != NULL )
			else
			{
				// some error occurred, try to re-initialize
				bReInitialize = true;
				continue;
			} // else from if( hWnd != NULL )
		}
		else
		{
			// some error occurred, try to re-initialize
			bReInitialize = true;
			continue;
		}
	} // for( ; true; )
	for( nIndex = 0; nIndex < __EXT_MFC_SWDC_TP_OBJECT_WATCH_COUNT; nIndex ++ )
	{
		HANDLE hFCN = arrHandles[ __EXT_MFC_SWDC_TP_OBJECT_INTERNAL_COUNT + nIndex ];
		if( hFCN != INVALID_HANDLE_VALUE )
			::FindCloseChangeNotification( hFCN );
	}
	m_eventResponse.SetEvent();
	return FALSE;
}

int CExtSWDC_ThreadPool::CExtSWDC_Thread::ExitInstance()
{
//int nRetVal = CWinThread::ExitInstance();
//	return nRetVal;
	return 0;
}

INT CExtSWDC_ThreadPool::CExtSWDC_Thread::GetBusyCount() const
{
INT nBusyCount = INT( m_listSWDC.GetCount() );
	ASSERT( 0 <= nBusyCount && nBusyCount <= __EXT_MFC_SWDC_TP_OBJECT_WATCH_COUNT );
	return nBusyCount;
}

bool CExtSWDC_ThreadPool::CExtSWDC_Thread::CanAdd() const
{
bool bRetVal = ( GetBusyCount() < __EXT_MFC_SWDC_TP_OBJECT_WATCH_COUNT ) ? true : false;
	return bRetVal;
}

bool CExtSWDC_ThreadPool::CExtSWDC_Thread::Add( CExtSWDC * pSWDC )
{
	if( pSWDC == NULL )
		return false;
	return Add( *pSWDC );
}

bool CExtSWDC_ThreadPool::CExtSWDC_Thread::Add( CExtSWDC & _SWDC )
{
CSingleLock _SL( &m_CS, TRUE );
	ASSERT( 0 <= GetBusyCount() && GetBusyCount() <= __EXT_MFC_SWDC_TP_OBJECT_WATCH_COUNT );
	if( GetBusyCount() == __EXT_MFC_SWDC_TP_OBJECT_WATCH_COUNT )
	{
		ASSERT( FALSE );
		return false;
	}
POSITION pos = m_listSWDC.Find( &_SWDC );
	if( pos != NULL )
	{
//		ASSERT( FALSE );
//		return false;
		return true;
	}
//TRACE1( "+watcher \"%s\"\r\n", LPCTSTR(_SWDC.WatchDirChanges_GetWatchPath()) );
//TRACE1( "+watcher %p\r\n", LPVOID(&_SWDC) );
//TRACE2( "+watcher %p   \"%s\"\r\n", LPVOID(&_SWDC), LPCTSTR(_SWDC.WatchDirChanges_GetWatchPath()) );
	m_listSWDC.AddTail( &_SWDC );
	ASSERT( 0 <= GetBusyCount() && GetBusyCount() <= __EXT_MFC_SWDC_TP_OBJECT_WATCH_COUNT );
	if( m_hThread == NULL )
	{
		if( ! CreateThread() )
		{
			ASSERT( FALSE );
			//AfxThrowUserException();
			return false;
		}
	}
	m_eventRequest.SetEvent(); // PulseEvent()
	// we do not need to wait for m_eventResponse here
//	_SL.Unlock();
//	::WaitForSingleObject( m_eventResponse, INFINITE );
	return true;
}

bool CExtSWDC_ThreadPool::CExtSWDC_Thread::Remove( CExtSWDC * pSWDC )
{
	if( pSWDC == NULL )
		return false;
	return Remove( *pSWDC );
}

bool CExtSWDC_ThreadPool::CExtSWDC_Thread::Remove( CExtSWDC & _SWDC )
{
CSingleLock _SL( &m_CS, TRUE );
CSingleLock _SLTP( &m_ThreadPool.m_CS, TRUE );
	ASSERT( 0 <= GetBusyCount() && GetBusyCount() <= __EXT_MFC_SWDC_TP_OBJECT_WATCH_COUNT );
POSITION pos = m_listSWDC.Find( &_SWDC );
	if( pos == NULL )
		return false;
//TRACE1( "-watcher \"%s\"\r\n", LPCTSTR(_SWDC.WatchDirChanges_GetWatchPath()) );
//TRACE1( "-watcher %p\r\n", LPVOID(&_SWDC) );
	m_listSWDC.RemoveAt( pos );
	ASSERT( 0 <= GetBusyCount() && GetBusyCount() < __EXT_MFC_SWDC_TP_OBJECT_WATCH_COUNT );

	ASSERT( ! IsPresent( _SWDC ) );

	m_ThreadPool.m_mapWatcherToThread.RemoveKey( &_SWDC );
POSITION posReceptor = m_ThreadPool.m_listThreadReceptors.Find( this );
INT nBusyCount = GetBusyCount();
	if( nBusyCount < __EXT_MFC_SWDC_TP_OBJECT_WATCH_COUNT )
	{
		if( posReceptor == NULL )
			m_ThreadPool.m_listThreadReceptors.AddTail( this );
	}
	else
	{
		if( posReceptor != NULL )
		{
			m_ThreadPool.m_listThreadReceptors.RemoveAt( posReceptor );
			posReceptor = NULL;
		}
	}
	if( nBusyCount == 0 )
	{
		m_ThreadPool.m_mapThreadCounts.RemoveKey( this );
		if( posReceptor != NULL )
			m_ThreadPool.m_listThreadReceptors.RemoveAt( posReceptor );
//TRACE1( "-thread %p\r\n", LPVOID(this) );
//_SL.Unlock();
//_SLTP.Unlock();
//delete this;
		HANDLE hThread = m_hThread;
		m_eventRequest.SetEvent(); // PulseEvent()
		_SL.Unlock();
		_SLTP.Unlock();
		::WaitForSingleObject( hThread, INFINITE );
		// do not wait for m_eventResponse here, thread should delete itself in CWinThread::ExitInstance()
	}
	else
	{
		ASSERT( nBusyCount > 0 );
		m_ThreadPool.m_mapThreadCounts.SetAt( this, nBusyCount );
		m_eventRequest.SetEvent(); // PulseEvent()
		_SL.Unlock();
		_SLTP.Unlock();
		::WaitForSingleObject( m_eventResponse, INFINITE );
	}
	return true;
}

bool CExtSWDC_ThreadPool::CExtSWDC_Thread::IsPresent( const CExtSWDC * pSWDC ) const
{
	if( pSWDC == NULL )
		return false;
	return IsPresent( *pSWDC );
}

bool CExtSWDC_ThreadPool::CExtSWDC_Thread::IsPresent( const CExtSWDC & _SWDC ) const
{
CSingleLock _SL( &m_CS, TRUE );
bool bRetVal = ( m_listSWDC.Find( const_cast < CExtSWDC * > ( &_SWDC ) ) != NULL ) ? true : false;
	return bRetVal;
}

/////////////////////////////////////////////////////////////////////////////
// CExtSWDC_ThreadPool::CExtSWDC_ThreadPool_Ptr

CExtSWDC_ThreadPool::CExtSWDC_ThreadPool_Ptr::CExtSWDC_ThreadPool_Ptr()
	: m_pThreadPool( NULL )
{
	ASSERT( __EXT_MFC_SWDC_TP_OBJECT_COUNT <= 64 ); // ::WaitForMultipleObjects() API limitation
}

CExtSWDC_ThreadPool::CExtSWDC_ThreadPool_Ptr::~CExtSWDC_ThreadPool_Ptr()
{
	if( m_pThreadPool != NULL )
		delete m_pThreadPool;
}

CExtSWDC_ThreadPool * CExtSWDC_ThreadPool::CExtSWDC_ThreadPool_Ptr::GetShellThreadPool()
{
	if( m_pThreadPool == NULL )
		m_pThreadPool = new CExtSWDC_ThreadPool;
	return m_pThreadPool;
}

CExtSWDC_ThreadPool * CExtSWDC_ThreadPool::CExtSWDC_ThreadPool_Ptr::operator -> ()
{
	return GetShellThreadPool();
}

#endif // if (defined __EXT_MFC_SWDC_USES_THREAD_POOL)

/////////////////////////////////////////////////////////////////////////////
// CExtSWDC

CExtSWDC::CExtSWDC()
	: m_bSubFolder( false )
	, m_dwNotifyFilter(
		FILE_NOTIFY_CHANGE_FILE_NAME
			|FILE_NOTIFY_CHANGE_DIR_NAME
			|FILE_NOTIFY_CHANGE_CREATION
				|FILE_NOTIFY_CHANGE_ATTRIBUTES
						|FILE_NOTIFY_CHANGE_SIZE
						//|FILE_NOTIFY_CHANGE_LAST_WRITE
						//|FILE_NOTIFY_CHANGE_LAST_ACCESS
				|FILE_NOTIFY_CHANGE_SECURITY
			)
#if (!defined __EXT_MFC_SWDC_USES_THREAD_POOL)
	, m_hThread( NULL )
	, m_eventThreadStop( FALSE, TRUE )
	, m_eventThreadComplete( FALSE, TRUE )
#endif // if (!defined __EXT_MFC_SWDC_USES_THREAD_POOL)
{
}

CExtSWDC::~CExtSWDC()
{
	WatchDirChanges_Stop();
}

bool CExtSWDC::WatchDirChanges_CanStart() const
{
CExtSafeString strItemPath = WatchDirChanges_GetWatchPath();
	if( strItemPath.IsEmpty() )
		return false;
INT nLength = INT( strItemPath.GetLength() );
	ASSERT( nLength > 0 );
	if(		nLength > 4
		&&	strItemPath[0] == _T(':')
		&&	strItemPath[1] == _T(':')
		&&	strItemPath[2] == _T('{')
		&&	strItemPath[nLength-1] == _T('}')
		)
		return false; // GUID is not path
//HANDLE hFCN = ::FindFirstChangeNotification(  LPCTSTR(strItemPath), FALSE, m_dwNotifyFilter );
//	if( hFCN == INVALID_HANDLE_VALUE )
//		return false;
//	::FindCloseChangeNotification( hFCN );
	return true;
}

void CExtSWDC::WatchDirChanges_Start(
	bool bForceRestrart,
	bool bSubFolder // = false
	)
{
	if( bForceRestrart )
		WatchDirChanges_Stop();

	if( ! WatchDirChanges_CanStart() )
		return;

#if (defined __EXT_MFC_SWDC_USES_THREAD_POOL)

	m_bSubFolder = bSubFolder;
	g_ShellThreadPool->Start( this );

#else // if (defined __EXT_MFC_SWDC_USES_THREAD_POOL)

	if( m_hThread != NULL )
		return;
	m_bSubFolder = bSubFolder;
CWinThread * pWinThread = AfxBeginThread( (AFX_THREADPROC)stat_WatchDirChanges_ThreadProc, (LPVOID)this );
	if( pWinThread != NULL )
	{
		m_hThread = pWinThread->m_hThread;
		ASSERT( m_hThread != NULL );
	}

#endif // else from if (defined __EXT_MFC_SWDC_USES_THREAD_POOL)
}

void CExtSWDC::WatchDirChanges_Stop()
{
#if (defined __EXT_MFC_SWDC_USES_THREAD_POOL)

	g_ShellThreadPool->Stop( this );

#else // if (defined __EXT_MFC_SWDC_USES_THREAD_POOL)

	if( m_hThread == NULL )
		 return;
	m_eventThreadStop.SetEvent();
	::WaitForSingleObject( (HANDLE)m_eventThreadComplete, INFINITE );
//DWORD dwWaitResult = ::WaitForSingleObject( (HANDLE)m_eventThreadComplete, 5*1000 );
//	if( dwWaitResult != WAIT_OBJECT_0 )
//		TerminateThread( m_hThread, 0 );
	m_eventThreadComplete.ResetEvent();
	m_eventThreadStop.ResetEvent();
	m_hThread = NULL;

#endif // else from if (defined __EXT_MFC_SWDC_USES_THREAD_POOL)
}

#if (!defined __EXT_MFC_SWDC_USES_THREAD_POOL)

UINT CExtSWDC::stat_WatchDirChanges_ThreadProc( CExtSWDC * pSWDC )
{
	pSWDC->WatchDirChanges_ThreadProc();
	return 0;
}

void CExtSWDC::WatchDirChanges_ThreadProc()
{
	{ // BLOCK BEGIN (for C++ class destructors)
		HANDLE hFCN = INVALID_HANDLE_VALUE;
		CExtSafeString strItemPath = WatchDirChanges_GetWatchPath();
		if( ! strItemPath.IsEmpty() )
		{
			HWND hWndOwn = WatchDirChanges_GetSafeHwnd();
			bool bStop = false;
			for( ; ! bStop ; )
			{
				if( hFCN == INVALID_HANDLE_VALUE )
				{
					hFCN = ::FindFirstChangeNotification(  LPCTSTR(strItemPath), FALSE, m_dwNotifyFilter );
					if( hFCN == INVALID_HANDLE_VALUE )
						break;
				}
				HANDLE arrHandles[2] = { hFCN, m_eventThreadStop };
				DWORD dwWaitResult = ::WaitForMultipleObjects( 2, arrHandles, FALSE, INFINITE );
				switch( dwWaitResult )
				{
				case WAIT_OBJECT_0:
					if( m_bSubFolder )
						::PostMessage( hWndOwn, CExtShellBase::g_nMsgChangeChildrenState, WPARAM(this), 0L );
					else
						::PostMessage( hWndOwn, CExtShellBase::g_nMsgUpdateItemContent, WPARAM(this), 0L );

					if( ! FindNextChangeNotification( hFCN ) )
					{
						::FindCloseChangeNotification( hFCN );
						hFCN = INVALID_HANDLE_VALUE;
					}
				break;
				case (WAIT_OBJECT_0+1):
					bStop = true;
				break;
				} // switch( dwWaitResult )
			} // for( ; ! bStop ; )
			if( hFCN != INVALID_HANDLE_VALUE )
				::FindCloseChangeNotification( hFCN );
		} // if( ! strItemPath.IsEmpty() )
	} // BLOCK END (for C++ class destructors)
	m_eventThreadComplete.SetEvent();
}

#endif // if (!defined __EXT_MFC_SWDC_USES_THREAD_POOL)

/////////////////////////////////////////////////////////////////////////////
// CExtShellBase

const UINT CExtShellBase::g_nMsgDelayFocusPIDL = ::RegisterWindowMessage( _T("CExtShellBase::g_nMsgDelayFocusPIDL") );
const UINT CExtShellBase::g_nMsgUpdateItemContent = ::RegisterWindowMessage( _T("CExtShellBase::g_nMsgUpdateItemContent") );
const UINT CExtShellBase::g_nMsgChangeChildrenState = ::RegisterWindowMessage( _T("CExtShellBase::g_nMsgChangeChildrenState") );
const UINT CExtShellBase::g_nMsgShellItemExecute = ::RegisterWindowMessage( _T("CExtShellBase::g_nMsgShellItemExecute") );

int CExtShellBase::g_nInstanceCounter = 0;
IMalloc * CExtShellBase::g_pMalloc = NULL;
LPITEMIDLIST CExtShellBase::g_pidlDesktop = NULL;
IShellFolder * CExtShellBase::g_pDesktopFolder = NULL;
CExtImageList CExtShellBase::g_ilShellSmall;
CExtImageList CExtShellBase::g_ilShellBig;
__EXT_MFC_SAFE_LPTSTR CExtShellBase::g_strTextOfMenuItemInEmptyShellContextMenu = _T("---"); // _T("(empty)");
CExtCmdIcon CExtShellBase::g_iconOverlayShareSmall;
CExtCmdIcon CExtShellBase::g_iconOverlayShareBig;
CExtCmdIcon CExtShellBase::g_iconOverlayLinkSmall;
CExtCmdIcon CExtShellBase::g_iconOverlayLinkBig;
CExtCmdIcon CExtShellBase::g_iconOverlaySlowFileSmall;
CExtCmdIcon CExtShellBase::g_iconOverlaySlowFileBig;

CExtShellBase::CExtShellBase()
	: m_pRootFolder( NULL )
{
}

CExtShellBase::~CExtShellBase()
{
	ASSERT( m_pRootFolder == NULL );
}

bool CExtShellBase::Shell_Init(
	bool bEnableThrowExceptions // = false
	)
{
	ASSERT( g_nInstanceCounter >= 0 );

	if( g_nInstanceCounter == 0 )
	{
		ASSERT( g_pMalloc == NULL );
		::SHGetMalloc( &g_pMalloc );
		if( g_pMalloc == NULL )
		{
			ASSERT( FALSE );
			Shell_DoneImpl();
			if( bEnableThrowExceptions )
				::AfxThrowUserException();
			return false;
		}

		ASSERT( g_pidlDesktop == NULL );
		HWND hWnd = Shell_GetHWND();
		::SHGetSpecialFolderLocation( hWnd, CSIDL_DESKTOP, &g_pidlDesktop );
		if( g_pidlDesktop == NULL )
		{
			ASSERT( FALSE );
			Shell_DoneImpl();
			if( bEnableThrowExceptions )
				::AfxThrowUserException();
			return false;
		}

		ASSERT( g_pDesktopFolder == NULL );
		if( ! SUCCEEDED( ::SHGetDesktopFolder( &g_pDesktopFolder ) ) )
		{
			ASSERT( FALSE );
			Shell_DoneImpl();
			if( bEnableThrowExceptions )
				::AfxThrowUserException();
			return false;
		}
		ASSERT( g_pDesktopFolder != NULL );

		if( g_ilShellSmall.GetSafeHandle() == NULL )
		{
			SHFILEINFO _sfi;
			::memset( &_sfi, 0, sizeof(SHFILEINFO) );
			HIMAGELIST hSysImageList = (HIMAGELIST)
				::SHGetFileInfo(
					(LPCTSTR)g_pidlDesktop,
					0,
					&_sfi,
					sizeof(SHFILEINFO),
					SHGFI_PIDL|SHGFI_SYSICONINDEX|SHGFI_SMALLICON
					);
			if( hSysImageList == NULL )
			{
				ASSERT( FALSE );
				Shell_DoneImpl();
				if( bEnableThrowExceptions )
					::AfxThrowUserException();
				return false;
			}
			g_ilShellSmall.Attach( hSysImageList );
		}
		if( g_ilShellBig.GetSafeHandle() == NULL )
		{
			SHFILEINFO _sfi;
			::memset( &_sfi, 0, sizeof(SHFILEINFO) );
			HIMAGELIST hSysImageList = (HIMAGELIST)
				::SHGetFileInfo(
					(LPCTSTR)g_pidlDesktop,
					0,
					&_sfi,
					sizeof(SHFILEINFO),
					SHGFI_PIDL|SHGFI_SYSICONINDEX|SHGFI_LARGEICON
					);
			if( hSysImageList == NULL )
			{
				ASSERT( FALSE );
				Shell_DoneImpl();
				if( bEnableThrowExceptions )
					::AfxThrowUserException();
				return false;
			}
			g_ilShellBig.Attach( hSysImageList );
		}

		//stat_OverlayIconsDone();
		stat_OverlayIconsInit();

	}
	m_pRootFolder = g_pDesktopFolder;
    g_nInstanceCounter ++;
	return true;
}

void CExtShellBase::Shell_DoneImpl()
{
	ASSERT( g_nInstanceCounter >= 0 );
//	if( g_ilShellSmall.GetSafeHandle() != NULL )
//		g_ilShellSmall.DeleteImageList();
//	if( g_ilShellBig.GetSafeHandle() != NULL )
//		g_ilShellBig.DeleteImageList();
	if( g_pDesktopFolder != NULL )
	{
		g_pDesktopFolder->Release();
		g_pDesktopFolder = NULL;
	}
	if( g_pidlDesktop != NULL )
	{
		if( g_pMalloc != NULL )
			g_pMalloc->Free( g_pidlDesktop );
		g_pidlDesktop = NULL;
	}
	if( g_pMalloc != NULL )
	{
		g_pMalloc->Release();
		g_pMalloc = NULL;
	}
	//stat_OverlayIconsDone();
}

bool CExtShellBase::stat_OverlayIconsInit()
{
	if(		( ! g_iconOverlayShareSmall.IsEmpty() )
		&&	( ! g_iconOverlayShareBig.IsEmpty() )
		&&	( ! g_iconOverlayLinkSmall.IsEmpty() )
		&&	( ! g_iconOverlayLinkBig.IsEmpty() )
		&&	( ! g_iconOverlaySlowFileSmall.IsEmpty() )
		&&	( ! g_iconOverlaySlowFileBig.IsEmpty() )
		)
		return true;
	if(		g_ilShellSmall.GetSafeHandle() == NULL
		||	g_ilShellBig.GetSafeHandle() == NULL
		)
		return false;
HMODULE hShell32 = ::GetModuleHandleA( "shell32.dll" );
	if( hShell32 == NULL )
		return false;
int ( STDAPICALLTYPE * pSHGetIconOverlayIndex )( LPCTSTR, int ) =
		( int ( STDAPICALLTYPE * )( LPCTSTR, int ) )
		::GetProcAddress(
			hShell32,
#if (defined _UNICODE)
			"SHGetIconOverlayIndexW"
#else
			"SHGetIconOverlayIndexA"
#endif
			);
	if( pSHGetIconOverlayIndex == NULL )
		return false;
	if(		g_iconOverlayShareSmall.IsEmpty()
		||	g_iconOverlayShareBig.IsEmpty()
		)
	{
		int nOverlayIconIndex = pSHGetIconOverlayIndex( NULL, __EXT_MFC_IDO_SHGIOI_SHARE );
		if( nOverlayIconIndex > 0 )
		{
			if( g_PaintManager.m_bIsWinVistaOrLater )
				nOverlayIconIndex ++;
			else
				nOverlayIconIndex --;
			if( g_iconOverlayShareSmall.IsEmpty() )
			{
				HICON hIcon = g_ilShellSmall.ExtractIcon( nOverlayIconIndex );
				if( hIcon != NULL )
					g_iconOverlayShareSmall.AssignFromHICON( hIcon, false );
			}
			if( g_iconOverlayShareBig.IsEmpty() )
			{
				HICON hIcon = g_ilShellBig.ExtractIcon( nOverlayIconIndex );
				if( hIcon != NULL )
					g_iconOverlayShareBig.AssignFromHICON( hIcon, false );
			}
		} // if( nOverlayIconIndex > 0 )
	}
	if(		g_iconOverlayLinkSmall.IsEmpty()
		||	g_iconOverlayLinkBig.IsEmpty()
		)
	{
		int nOverlayIconIndex = pSHGetIconOverlayIndex( NULL, __EXT_MFC_IDO_SHGIOI_LINK );
		if( nOverlayIconIndex > 0 )
		{
			if( g_PaintManager.m_bIsWinVistaOrLater )
				nOverlayIconIndex ++;
			else
				nOverlayIconIndex --;
			if( g_iconOverlayLinkSmall.IsEmpty() )
			{
				HICON hIcon = g_ilShellSmall.ExtractIcon( nOverlayIconIndex );
				if( hIcon != NULL )
					g_iconOverlayLinkSmall.AssignFromHICON( hIcon, false );
			}
			if( g_iconOverlayLinkBig.IsEmpty() )
			{
				HICON hIcon = g_ilShellBig.ExtractIcon( nOverlayIconIndex );
				if( hIcon != NULL )
					g_iconOverlayLinkBig.AssignFromHICON( hIcon, false );
			}
		} // if( nOverlayIconIndex > 0 )
	}
	if(		g_iconOverlaySlowFileSmall.IsEmpty()
		||	g_iconOverlaySlowFileBig.IsEmpty()
		)
	{
		int nOverlayIconIndex = pSHGetIconOverlayIndex( NULL, __EXT_MFC_IDO_SHGIOI_SLOWFILE );
		if( nOverlayIconIndex > 0 )
		{
			if( g_PaintManager.m_bIsWinVistaOrLater )
				nOverlayIconIndex ++;
			else
				nOverlayIconIndex --;
			if( g_iconOverlaySlowFileSmall.IsEmpty() )
			{
				HICON hIcon = g_ilShellSmall.ExtractIcon( nOverlayIconIndex );
				if( hIcon != NULL )
					g_iconOverlaySlowFileSmall.AssignFromHICON( hIcon, false );
			}
			if( g_iconOverlaySlowFileBig.IsEmpty() )
			{
				HICON hIcon = g_ilShellBig.ExtractIcon( nOverlayIconIndex );
				if( hIcon != NULL )
					g_iconOverlaySlowFileBig.AssignFromHICON( hIcon, false );
			}
		} // if( nOverlayIconIndex > 0 )
	}
	return true;
}

void CExtShellBase::stat_OverlayIconsDone()
{
	g_iconOverlayShareSmall.Empty();
	g_iconOverlayShareBig.Empty();
	g_iconOverlayLinkSmall.Empty();
	g_iconOverlayLinkBig.Empty();
	g_iconOverlaySlowFileSmall.Empty();
	g_iconOverlaySlowFileBig.Empty();
}

HRESULT CExtShellBase::stat_SHAutoComplete( HWND hWnd, DWORD dwFlags )
{
    if( hWnd == NULL || (! ::IsWindow( hWnd ) ) )
        return E_INVALIDARG;
    if( g_PaintManager.m_pfnSHAutoComplete == NULL )
		return E_NOTIMPL;
HRESULT hr = g_PaintManager.m_pfnSHAutoComplete( hWnd, dwFlags );
	return hr;
}

void CExtShellBase::Shell_Done()
{
	if( m_pRootFolder != g_pDesktopFolder )
		m_pRootFolder->Release();
	m_pRootFolder = NULL;
	ASSERT( g_nInstanceCounter >= 0 );
	g_nInstanceCounter--;
	if( g_nInstanceCounter == 0 )
		Shell_DoneImpl();
}

bool CExtShellBase::Shell_IsInitialized() const
{
	ASSERT( g_nInstanceCounter >= 0 );
	if( g_nInstanceCounter == 0 )
		return false;
	if( g_pMalloc == NULL )
		return false;
	if( g_pidlDesktop == NULL )
		return false;
	if( g_pDesktopFolder == NULL || m_pRootFolder == NULL )
		return false;
	if( g_ilShellBig.GetSafeHandle() == NULL )
		return false;
	if( g_ilShellSmall.GetSafeHandle() == NULL )
		return false;
	return true;
}

HWND CExtShellBase::Shell_GetHWND()
{
	ASSERT( g_nInstanceCounter >= 0 );
	return ::GetDesktopWindow();
}

void CExtShellBase::stat_RemoveDateTimeField(
	CExtSafeString & sFormatStr,
	__EXT_MFC_SAFE_TCHAR chField,
	__EXT_MFC_SAFE_LPCTSTR sSeparator
	)
{
INT nPos = sFormatStr.Find( chField, 0 );
	if( nPos >= 0 )
	{
		INT nPos1 = nPos;
		CString sFieldL = chField;
		sFieldL.MakeLower();
		CString sFieldR = chField;
		sFieldR.MakeUpper();
		while(		(nPos + 1) <= sFormatStr.GetLength() - 1 
			&&	(	sFormatStr[ nPos + 1 ] == sFieldL[0] 
				||	sFormatStr[ nPos + 1 ] == sFieldR[0] 
				)
			) 
			nPos++;
		sFormatStr.Delete( nPos1, nPos - nPos1 + 1 );
		INT nSeparatorLen = INT( _tcslen( sSeparator ) );
		bool bFound = false;
		for( INT nIndex = nPos1 - nSeparatorLen; nIndex >= 0; nIndex-- )
		{
			if( sFormatStr.Mid( nIndex, nSeparatorLen ) == LPCTSTR(sSeparator) )
			{
				bFound = true;
				sFormatStr.Delete( nIndex, nPos1 - nIndex );
				break;
			}
		}
		if( !bFound )
		{
			for( INT nIndex = nPos; (nIndex + nSeparatorLen) < sFormatStr.GetLength(); nIndex++ )
			{
				if( sFormatStr.Mid( nIndex, nSeparatorLen ) == LPCTSTR(sSeparator) )
				{
					bFound = true;
					sFormatStr.Delete( nIndex, nIndex - nPos + 1 );
					break;
				}
			}
		}
	}
}

CExtSafeString CExtShellBase::stat_GetDateTimeFieldText(
	const COleDateTime & _time,
	bool bIncludeDate,              // = true
	bool bIncludeTime,              // = true
	int  nTimeFormat,               // = 0    // 0-default, 12-with AM/PM, 24-...
	bool bIncludeFieldYear,         // = true // used only if bIncludeDate is !false
	bool bIncludeFieldMonth,        // = true // used only if bIncludeDate is !false
	bool bIncludeFieldDay,          // = true // used only if bIncludeDate is !false
	bool bIncludeFieldHour,         // = true // used only if bIncludeTime is !false
	bool bIncludeFieldMinute,       // = true // used only if bIncludeTime is !false
	bool bIncludeFieldSecond,       // = true // used only if bIncludeTime is !false
	bool bIncludeFieldDesignator    // = true // used only if bIncludeTime is !false
	)
{
CExtSafeString str = _T("");
	if( (!bIncludeDate) && (!bIncludeTime) )
		return str;
	if( _time.GetStatus() != COleDateTime::valid )
		return str;
CExtSafeString sFormat;
CExtSafeString sTimeFormat;
CExtSafeString sTimeSeparator;
CExtSafeString sDateFormat;
CExtSafeString sDateSeparator;
bool bTimeFormat24Hours = false;
const INT nBufferSize = 64;
LPTSTR lpszBuf = (LPTSTR)::LocalAlloc( LPTR, nBufferSize );
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_STIMEFORMAT,
			lpszBuf,
			nBufferSize
			) != 0
		);
	sTimeFormat = (LPTSTR)lpszBuf;
	memset( lpszBuf, 0, nBufferSize );
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_SSHORTDATE,
			lpszBuf,
			nBufferSize
			) != 0
		);
	sDateFormat = (LPTSTR)lpszBuf;
	memset( lpszBuf, 0, nBufferSize );
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_STIME,
			lpszBuf,
			10
			) != 0
		);
	sTimeSeparator = (LPTSTR)lpszBuf;
	memset( lpszBuf, 0, nBufferSize );
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_SDATE,
			lpszBuf,
			10
			) != 0
		);
	sDateSeparator = (LPTSTR)lpszBuf;
	if( nTimeFormat == 12 )
		bTimeFormat24Hours = false;
	else if( nTimeFormat == 24 )
		bTimeFormat24Hours = true;
	else
	{
		memset( lpszBuf, 0, nBufferSize );
		VERIFY(
			g_ResourceManager->GetLocaleInfo(
				LOCALE_ITIME,
				lpszBuf,
				nBufferSize
				) != 0
			);
		CString sITime = (LPTSTR)lpszBuf;
		bTimeFormat24Hours = ( _tcscmp( sITime, _T("0") ) == 0) ? false : true;
	}
	::LocalFree( lpszBuf );
	lpszBuf = NULL;
	if( bTimeFormat24Hours )
		sTimeFormat.Replace( _T('h'), _T('H') );
	else
		sTimeFormat.Replace( _T('H'), _T('h') );

	if( bIncludeDate )
	{
		if( ! bIncludeFieldYear )
		{
			stat_RemoveDateTimeField( sDateFormat, _T('y'), sDateSeparator );
		}
		if( ! bIncludeFieldMonth )
		{
			stat_RemoveDateTimeField( sDateFormat, _T('M'), sDateSeparator );
		}
		if( ! bIncludeFieldDay )
		{
			stat_RemoveDateTimeField( sDateFormat, _T('d'), sDateSeparator );
		}
	}
	if( bIncludeTime )
	{
		if( ! bIncludeFieldHour )
		{
			stat_RemoveDateTimeField( sTimeFormat, _T('h'), sTimeSeparator );
			stat_RemoveDateTimeField( sTimeFormat, _T('H'), sTimeSeparator );
		}			
		if( ! bIncludeFieldMinute )
		{
			stat_RemoveDateTimeField( sTimeFormat, _T('m'), sTimeSeparator );
		}
		if( ! bIncludeFieldSecond )
		{
			stat_RemoveDateTimeField( sTimeFormat, _T('s'), sTimeSeparator );
		}
		if(		! bIncludeFieldDesignator
			||	bTimeFormat24Hours
			)
		{
			stat_RemoveDateTimeField( sTimeFormat, _T('t'), _T(" ") );
		}
	}
SYSTEMTIME sysTime;
	VERIFY( _time.GetAsSystemTime( sysTime ) );
CString sTime, sDate;
INT nRet = g_ResourceManager->GetTimeFormat( 0, &sysTime, sTimeFormat, sTime.GetBuffer( nBufferSize ), nBufferSize );
	sTime.ReleaseBuffer();
	if( nRet == 0 )
		sTime = _time.Format( VAR_TIMEVALUEONLY );
	nRet =  g_ResourceManager->GetDateFormat( 0, &sysTime, sDateFormat, LPTSTR( sDate.GetBuffer( nBufferSize ) ), nBufferSize );
	sDate.ReleaseBuffer();
	if( nRet == 0 )
	{
		// d	Day of month as digits with no leading zero for single-digit days. 
		// dd	Day of month as digits with leading zero for single-digit days. 
		// M	Month as digits with no leading zero for single-digit months. 
		// MM	Month as digits with leading zero for single-digit months. 
		// y	Year as last two digits, but with no leading zero for years less than 10. 
		// yy	Year as last two digits, but with leading zero for years less than 10. 
		// yyyy	Year represented by full four digits.

		INT nPos = -1;
		CString s, sFind;
		sDate = sDateFormat;

		// day
		sFind = _T("dd");
		nPos = sDate.Find( sFind );
		if( nPos >= 0 )
		{
			s.Format( _T("%02d"), sysTime.wDay );
			sDate.Replace( sFind, s );
		}
		sFind = _T("d");
		nPos = sDate.Find( sFind );
		if( nPos >= 0 )
		{
			s.Format( _T("%d"), sysTime.wDay );
			sDate.Replace( sFind, s );
		}

		// month
		sFind = _T("MM");
		nPos = sDate.Find( sFind );
		if( nPos >= 0 )
		{
			s.Format( _T("%02d"), sysTime.wMonth );
			sDate.Replace( sFind, s );
		}
		sFind = _T("M");
		nPos = sDate.Find( sFind );
		if( nPos >= 0 )
		{
			s.Format( _T("%d"), sysTime.wMonth );
			sDate.Replace( sFind, s );
		}

		// year
		sFind = _T("yyyy");
		nPos = sDate.Find( sFind );
		if( nPos >= 0 )
		{
			s.Format( _T("%04d"), sysTime.wYear );
			sDate.Replace( sFind, s );
		}
		sFind = _T("yy");
		nPos = sDate.Find( sFind );
		if( nPos >= 0 )
		{
			s.Format( _T("%04d"), sysTime.wYear );
			sDate.Replace( sFind, s.Right( 2 ) );
		}
		sFind = _T("y");
		nPos = sDate.Find( sFind );
		if( nPos >= 0 )
		{
			s.Format( _T("%d"), sysTime.wYear );
			sDate.Replace( sFind, s );
		}
	}
	if( bIncludeDate && bIncludeTime )
		str = sDate + _T(" ") + sTime;
	else if( bIncludeDate )
		str = sDate;
	else if( bIncludeTime )
		str = sTime;
	return str;
}

LPITEMIDLIST CExtShellBase::stat_CopyItem( LPCITEMIDLIST pidlSrc )
{
	ASSERT( g_pMalloc != NULL );
	if( pidlSrc == NULL || g_pMalloc == NULL )
		return NULL;
UINT nSize = stat_GetItemSize( pidlSrc );
LPITEMIDLIST pidlDst = LPITEMIDLIST( g_pMalloc->Alloc( nSize ) );
	if( pidlDst == NULL )
		return NULL;
	::memcpy( pidlDst, pidlSrc, nSize );
	return pidlDst;
}

LPITEMIDLIST CExtShellBase::stat_CreateItem( UINT nSize )
{
	VERIFY( g_pMalloc != NULL );
LPITEMIDLIST pidl = LPITEMIDLIST( g_pMalloc->Alloc( nSize ) );
	if( pidl != NULL )
		::memset( pidl, 0, nSize );
	return pidl;
}

UINT CExtShellBase::stat_GetItemCount( LPCITEMIDLIST pidl )
{
	if( pidl == NULL )
		return 0;
UINT nCount = 0;
	for( UINT nSizeCurr = pidl->mkid.cb; nSizeCurr != 0; nCount++ )
	{
		pidl = stat_GetNextItem( pidl );
		nSizeCurr = pidl->mkid.cb;
	}
	return nCount;
}

UINT CExtShellBase::stat_GetItemSize( LPCITEMIDLIST pidl )
{
	if( pidl == NULL )
		return 0;
UINT nSize = 2 * sizeof(BYTE);
	for( LPITEMIDLIST pidlWalk = LPITEMIDLIST( pidl ); pidlWalk->mkid.cb != 0; )
	{
		nSize += pidlWalk->mkid.cb;
		pidlWalk = stat_GetNextItem( pidlWalk );
	}
	return nSize;
}

LPITEMIDLIST CExtShellBase::stat_GetNextItem( LPCITEMIDLIST pidl )
{
	if( pidl == NULL )
		return NULL;
	return LPITEMIDLIST( (LPBYTE)( ((LPBYTE)pidl) + pidl->mkid.cb ) );
}

INT CExtShellBase::stat_GetParentItem( LPCITEMIDLIST pidl, LPITEMIDLIST& pidlParent )
{
	ASSERT( g_pMalloc != NULL );
UINT nCount = stat_GetItemCount( pidl );
	if( nCount == 0 )
		return -1;
	if( nCount == 1 )
	{
		VERIFY( SUCCEEDED( ::SHGetSpecialFolderLocation( NULL, CSIDL_DESKTOP, &pidlParent ) ) );
		return 0;
	}
USHORT nParentSize = 0;
LPCITEMIDLIST pidlWalk = pidl;
	for( UINT i = 0; i < (nCount - 1); i++ )
	{
		nParentSize = (USHORT)( nParentSize + pidlWalk->mkid.cb );
		pidlWalk = stat_GetNextItem( pidlWalk );
	}
	pidlParent = stat_CreateItem( nParentSize + 2 );
	::memcpy( (LPBYTE) pidlParent, (LPBYTE) pidl, nParentSize );
	return (nCount-1);
}

LPITEMIDLIST CExtShellBase::stat_GetParentItem( LPCITEMIDLIST pidl )
{
LPITEMIDLIST pidlParent = NULL;
INT n = stat_GetParentItem( pidl, pidlParent );
	if( n < 0 )
		return NULL;
	ASSERT( pidlParent != NULL );
	return pidlParent;
}

LPCITEMIDLIST CExtShellBase::stat_ILGetLast( LPCITEMIDLIST pidl )
{
LPCITEMIDLIST pidlRet = pidl, pidlTmp = stat_ILGetNext( pidl );
	for( ; pidlTmp != NULL; )
	{
		pidlRet = pidlTmp;
		pidlTmp = stat_ILGetNext( pidlTmp );
	}
	return pidlRet;
}

LPITEMIDLIST CExtShellBase::stat_ILGetLast( LPITEMIDLIST pidl )
{
LPITEMIDLIST pidlRet = pidl, pidlTmp = stat_ILGetNext( pidl );
	for( ; pidlTmp != NULL; )
	{
		pidlRet = pidlTmp;
		pidlTmp = stat_ILGetNext( pidlTmp );
	}
	return pidlRet;
}

INT CExtShellBase::stat_ILGetLength( LPCITEMIDLIST pidl )
{
	if( pidl == NULL )
		return 0;
INT nCB = 0, nLength = 0;
	do
	{
		nCB = pidl->mkid.cb;
		pidl = LPCITEMIDLIST( ((LPBYTE)pidl) + nCB );
		nLength += nCB;
	}
	while( nCB != 0 );
	return nLength;
}

LPCITEMIDLIST CExtShellBase::stat_ILGetNext( LPCITEMIDLIST pidl )
{
	if( pidl == NULL )
		return NULL;
INT nCB = pidl->mkid.cb;
	if( nCB == 0 )
		return NULL;
	pidl = LPCITEMIDLIST( ((LPBYTE)pidl) + nCB );
	return (pidl->mkid.cb == 0) ? NULL : pidl;
}

LPITEMIDLIST CExtShellBase::stat_ILGetNext( LPITEMIDLIST pidl )
{
	if( pidl == NULL )
		return NULL;
INT nCB = pidl->mkid.cb;
	if( nCB == 0 )
		return NULL;
	pidl = LPITEMIDLIST( ((LPBYTE)pidl) + nCB );
	return (pidl->mkid.cb == 0) ? NULL : pidl;
}

LPITEMIDLIST CExtShellBase::stat_ILCloneFirst( LPCITEMIDLIST pidl )
{
	if( pidl == NULL )
		return NULL;
INT nCB = pidl->mkid.cb; 
LPITEMIDLIST pidlNew = (LPITEMIDLIST) g_pMalloc->Alloc( nCB + sizeof(USHORT) ); 
	if( pidlNew == NULL ) 
		return NULL; 
	::memcpy( pidlNew, pidl, nCB ); 
	*((USHORT *) (((LPBYTE) pidlNew) + nCB)) = 0; 
	return pidlNew; 
}

LPITEMIDLIST CExtShellBase::stat_ILClone( LPCITEMIDLIST pidl )
{
	return stat_ILCombine( pidl, NULL );
}

LPITEMIDLIST CExtShellBase::stat_ILCombine( LPCITEMIDLIST pidl1, LPCITEMIDLIST pidl2 )
{
INT nCB1 = stat_ILGetLength( pidl1 ), nCB2 = stat_ILGetLength( pidl2 );
LPITEMIDLIST pidlNew = (LPITEMIDLIST) g_pMalloc->Alloc( nCB1 + nCB2 + sizeof(USHORT) );
	if( pidlNew == NULL )
		return NULL;
	if( nCB1 > 0 )
		::memcpy( pidlNew, pidl1, nCB1 + sizeof(USHORT) );
	if( nCB2 > 0 )
		::memcpy( (((LPBYTE) pidlNew) + nCB1), pidl2, nCB2 + sizeof(USHORT) );
	if( nCB1 == 0 && nCB2 == 0 )
	{
//g_pMalloc->Free( pidlNew );
//return NULL;
		if( pidl1 == NULL )
		{
			g_pMalloc->Free( pidlNew );
			return NULL;
		}
		::memcpy( pidlNew, pidl1, sizeof(USHORT) );
	}
	return pidlNew;
}

bool CExtShellBase::stat_ILExtractName( IShellFolder * pShellFolder, LPITEMIDLIST pItemIDList, DWORD dwFlags, CExtSafeString & strName )
{
	ASSERT( pShellFolder != NULL );
	ASSERT( pItemIDList != NULL );
STRRET data;
	if( pShellFolder->GetDisplayNameOf( pItemIDList, dwFlags, &data ) != NOERROR )
		return false;
	switch( data.uType )
	{
	case STRRET_WSTR:
	{
		USES_CONVERSION;
    // W4 Change This problem is in the MFC code We are not going to change it 05012013
#pragma warning(suppress: 6255)
		strName = W2CT(data.pOleStr);
		ASSERT( g_pMalloc != NULL );
		g_pMalloc->Free( data.pOleStr );
	}
	break;
	case STRRET_OFFSET:
	{
		USES_CONVERSION;
		strName = A2CT(LPSTR(LPVOID(pItemIDList+data.uOffset)));
	}
	break;
	case STRRET_CSTR:
	{
		USES_CONVERSION;
		strName = A2CT(data.cStr);
		ASSERT( g_pMalloc != NULL );
		g_pMalloc->Free( data.cStr );
	}
	break;
	default:
		return false;
	} // switch( data.uType )
	return true;
}

LPITEMIDLIST CExtShellBase::stat_ILFromPath( __EXT_MFC_SAFE_LPCTSTR pszPath, HWND hWndOwner )
{
	USES_CONVERSION;
ULONG count = 0;
LPITEMIDLIST pidlNew = NULL;
	ASSERT(  g_pDesktopFolder != NULL );
  // W4 Change this warning is inthe MFC code and we are not going to change it 05012013
#pragma warning(suppress: 6255)
	g_pDesktopFolder->ParseDisplayName( hWndOwner, NULL, LPWSTR(T2COLE(LPCTSTR(pszPath))), &count, &pidlNew, NULL );
	return pidlNew;
}

LPITEMIDLIST CExtShellBase::stat_ILCloneParent( LPCITEMIDLIST pidl )
{
	UINT cb = (UINT)stat_ILGetLast(pidl) - (UINT)pidl;
	ASSERT( g_pMalloc != NULL );
	LPITEMIDLIST pidlNew = (LPITEMIDLIST)g_pMalloc->Alloc(cb + sizeof(USHORT)); 
	if( pidlNew != NULL )
	{			
		::memcpy( pidlNew, pidl, cb ); 
		((LPITEMIDLIST)((LPBYTE)pidlNew + cb))->mkid.cb = 0; 
	}
	return pidlNew;
}

bool CExtShellBase::stat_ValidPath( __EXT_MFC_SAFE_LPCTSTR strPath )
{
	ASSERT( g_pDesktopFolder != NULL );
LPITEMIDLIST  pidl = NULL;
OLECHAR       szOleChar[MAX_PATH];
ULONG         chEaten = 0L;
ULONG         dwAttributes = 0L;
HRESULT       hr;
#if !defined( _UNICODE )
	::MultiByteToWideChar( CP_ACP, MB_PRECOMPOSED, LPCTSTR(strPath), -1, szOleChar, MAX_PATH );
#else
	__EXT_MFC_STRCPY( szOleChar, MAX_PATH, LPCTSTR(strPath) );
#endif
	hr = g_pDesktopFolder->ParseDisplayName( NULL, NULL, szOleChar, &chEaten, &pidl, &dwAttributes );
	return ( SUCCEEDED( hr ) ) ? true : false;
}

bool CExtShellBase::stat_ValidPIDL( LPCITEMIDLIST pidlAbsolute )
{
	if( pidlAbsolute == NULL )
		return false;
CExtPIDL pidl;
	pidl.Attach( (ITEMIDLIST*)(LPITEMIDLIST)pidlAbsolute );
CExtSafeString strPath = pidl.GetPath();
	pidl.Detach();
	if( strPath.IsEmpty() )
		return true;
	return stat_ValidPath( LPCTSTR(strPath) );
}

bool CExtShellBase::stat_CreateFolder( __EXT_MFC_SAFE_LPCTSTR strPath )
{
	ASSERT( strPath != NULL );
	if( LPCTSTR(strPath) == NULL )
		return false;
	if( (*LPCTSTR(strPath)) == _T('\0') )
		return true;
TCHAR	strDrive[    _MAX_PATH + 1 ],
		strDir[      _MAX_PATH + 1 ],
		strFileName[ _MAX_PATH + 1 ],
		strFileExt[  _MAX_PATH + 1 ];
	::memset( strDrive,    0, ( _MAX_PATH + 1 ) * sizeof(TCHAR) );
	::memset( strDir,      0, ( _MAX_PATH + 1 ) * sizeof(TCHAR) );
	::memset( strFileName, 0, ( _MAX_PATH + 1 ) * sizeof(TCHAR) );
	::memset( strFileExt,  0, ( _MAX_PATH + 1 ) * sizeof(TCHAR) );
	if( _tcsncmp( LPCTSTR(strPath), _T("\\\\"), 2 ) )
		__EXT_MFC_SPLITPATH(
			LPCTSTR(strPath),
			strDrive, _MAX_PATH,
			strDir, _MAX_PATH,
			strFileName, _MAX_PATH,
			strFileExt, _MAX_PATH
			);
	else
	{
		__EXT_MFC_STRCPY( strDrive, _MAX_PATH, _T("\\\\") );
		INT nSlashes = 0;
		TCHAR * ptrTCHAR = (TCHAR *)LPCTSTR(strPath);
		for( ptrTCHAR += 2; (*ptrTCHAR) != _T('\0'); )
		{
			if( (*ptrTCHAR) == _T('\\') || (*ptrTCHAR) == _T('/') )
			{
				nSlashes ++;
				if( nSlashes == 2 )
					break;
			}
			TCHAR strTmp[2];
			strTmp[0] = (*ptrTCHAR);
			strTmp[1] = _T('\0');
			__EXT_MFC_STRCAT( strDrive, _MAX_PATH, strTmp );
			ptrTCHAR ++;
		}
		__EXT_MFC_STRCPY( strDir, _MAX_PATH, ptrTCHAR );
	}
CExtSafeString strPathDir;
	strPathDir = LPCTSTR(strDir);
  // W4 Change strFileName is memset to zero about one item longer than MAX_PATH 05012013
#pragma warning(suppress: 6054)
	strPathDir += strFileName;
  // W4 Change strFileName is memset to zero about one item longer than MAX_PATH 05012013
#pragma warning(suppress: 6054)
	strPathDir += strFileExt;
CExtSafeString strTemplate( strDrive );
INT i = 0, l = INT( _tcslen( LPCTSTR(strPathDir) ) );
	for( ; i <= l; i++ )
	{
		ASSERT( i <= l );
		TCHAR _tchr = _T('\0');
		if( i < l )
			_tchr = strPathDir[ i ];
		if(		( ( _tchr == _T('/') || _tchr == _T('\\') ) && i != 0 )
			||	_tchr == _T('\0')
			)
		{ // if checkpoint TCHARacter
			::CreateDirectory( (LPCTSTR)strTemplate, NULL );
			// check directory exist
			bool bPortionExist = false;
#if (defined _UNICODE)
			struct _wfinddata_t fd;
#else
			struct _finddata_t fd;
#endif
			CExtSafeString strSearch;
			strSearch = LPCTSTR(strTemplate);
			INT nLength = INT( strSearch.GetLength() );
			if( nLength != 0 )
			{
				TCHAR _tchr = strSearch[ nLength - 1 ];
				if( _tchr == _T('/') || _tchr == _T('\\') )
				{
					TCHAR * pc = (TCHAR *)(LPCTSTR)strSearch;
					pc += nLength - 1;
					(*pc) = _T('\0');
				}
			} // if( nLength != 0 )
			long hFile = (long)
#if (defined _UNICODE)
				_wfindfirst(
					(wchar_t *) (LPVOID) (LPCTSTR) strSearch,
#else
				_findfirst(
					(const char *) strSearch,
#endif
					&fd
					);
			if( hFile > -1 )
			{
				if( ( fd.attrib & _A_SUBDIR ) != 0 )
					bPortionExist = true;
			}
			_findclose( hFile );
			if( ! bPortionExist )
				return false;
			if( _tchr == _T('\0') )
			{
				ASSERT( i == l );
				return true;
			}
		} // if checkpoint TCHARacter
		ASSERT( _tchr != _T('\0') );
		strTemplate += _tchr;
	} // for( ; i <= l; i++ )
	return true;
}

bool CExtShellBase::stat_FileExist( __EXT_MFC_SAFE_LPCTSTR strPath )
{
bool bRetVal = true;
#if (defined _UNICODE)
struct _wfinddata_t fd;
#else
struct _finddata_t fd;
#endif
long hFile = (long)
#if (defined _UNICODE)
			_wfindfirst(
				(wchar_t *) (LPVOID) (LPCTSTR) strPath,
#else
			_findfirst(
				(const char *) (LPCTSTR) strPath,
#endif
				&fd
				);
	if( hFile == -1 )
		bRetVal = false;
	_findclose( hFile );
	return bRetVal;
}

bool CExtShellBase::stat_IsFileAnywhereOpen( __EXT_MFC_SAFE_LPCTSTR strPath )
{
HANDLE hFile = ::CreateFile( LPCTSTR(strPath), GENERIC_WRITE|GENERIC_READ, 0,NULL,OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
	if( hFile == INVALID_HANDLE_VALUE )
		return true;
	CloseHandle( hFile );
	return false;
}

bool CExtShellBase::stat_DirExist( __EXT_MFC_SAFE_LPCTSTR strPath )
{
	if( strPath == NULL )
		return false;
WIN32_FIND_DATA _fd;
	_fd.dwFileAttributes |= FILE_ATTRIBUTE_DIRECTORY;
HANDLE hFind = ::FindFirstFile( LPCTSTR(strPath), &_fd );
	if( hFind == INVALID_HANDLE_VALUE )
		return false;
	::FindClose( hFind );
	if( ( _fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) == 0 )
	{
		static const TCHAR g_strAllFilesMaskEnding[] = _T("*.*");
		static const INT g_nAllFilesMaskEndingLen = INT(_tcslen(g_strAllFilesMaskEnding));
		INT nPathLen = INT(_tcslen(strPath));
		if(		nPathLen >= g_nAllFilesMaskEndingLen
			&&	_tcscmp( LPCTSTR(strPath) + nPathLen - g_nAllFilesMaskEndingLen, g_strAllFilesMaskEnding ) == 0
			)
			return true;
		return false;
	}
	return true;
}

CExtShellBase::NotifyShellItemExecuteData::NotifyShellItemExecuteData(
	HWND hWndControl,
	LPARAM lParamItemID,
	UINT nMessage,
	LPARAM lParamReserved // = 0L
	)
	: m_hWndControl( hWndControl )
	, m_lParamItemID( lParamItemID )
	, m_nMessage( nMessage )
	, m_lParamReserved( lParamReserved )
	, m_bHandled( false )
{
}

CExtShellBase::NotifyShellItemExecuteData::operator LPARAM () const
{
	return LPARAM( LPVOID(this) );
}

CExtShellBase::NotifyShellItemExecuteData & CExtShellBase::NotifyShellItemExecuteData::FromLPARAM( LPARAM lParam )
{
	return (*((CExtShellBase::NotifyShellItemExecuteData*)lParam));
}

bool CExtShellBase::NotifyShellItemExecuteData::Notify()
{
	if( m_hWndControl == NULL || (! ::IsWindow(m_hWndControl) ) )
		return false;
HWND hWndNotify = ::GetParent( m_hWndControl );
	if( hWndNotify == NULL || (! ::IsWindow(hWndNotify) ) )
		return false;
	m_bHandled = false;
	::SendMessage( hWndNotify, g_nMsgShellItemExecute, 0, *this );
	return m_bHandled;
}

bool CExtShellBase::stat_NotifyShellItemExecute(
	HWND hWndControl,
	LPARAM lParamItemID,
	UINT nMessage,
	LPARAM lParamReserved // = 0L
	)
{
NotifyShellItemExecuteData _NSIED( hWndControl, lParamItemID, nMessage, lParamReserved );
	return _NSIED.Notify();
}

bool CExtShellBase::stat_ILIsDesktop( LPCITEMIDLIST pidl )
{
	return ( pidl == NULL || ( pidl != NULL && pidl->mkid.cb == 0x00 ) ) ? true : false;
}

bool CExtShellBase::stat_ILIsSimple( LPCITEMIDLIST pidl )
{
bool bRetVal = true;
	if( ! stat_ILIsDesktop( pidl ) )
	{
		WORD wLen = pidl->mkid.cb;
		LPCITEMIDLIST pidlNext = (LPCITEMIDLIST)(((LPBYTE)pidl) + wLen );
		if( pidlNext->mkid.cb )
			bRetVal = false;
	}
	return bRetVal;
}

bool CExtShellBase::stat_ILRemoveLast( LPITEMIDLIST pidl )
{
	if( pidl == NULL || pidl->mkid.cb == 0 )
		return false;
	stat_ILGetLast( pidl )->mkid.cb = 0;
	return true;
}

bool CExtShellBase::stat_ILSerialize(
	LPITEMIDLIST * p_pidl,
	CArchive & ar,
	bool bEnableThrowExceptions // = false
	)
{
	ASSERT( g_pMalloc != NULL );
	ASSERT( g_pDesktopFolder != NULL );
BYTE byteHdrFlags = 0L;
	try
	{
		if( p_pidl == NULL )
			::AfxThrowUserException();
		if( ar.IsStoring() )
		{ // saving
			if( (*p_pidl) != NULL )
				byteHdrFlags |= BYTE(1);
			ar << byteHdrFlags;
			if( (*p_pidl) == NULL )
				return true;
			// continue with saving PIDL data
			DWORD dwBytesToWrite = (DWORD)stat_GetItemSize( (*p_pidl) );
			ar << dwBytesToWrite;
			ar.Write( (*p_pidl), dwBytesToWrite );
		} // saving
		else
		{ // loading
			if( (*p_pidl) != NULL )
			{
				g_pMalloc->Free( (*p_pidl) );
				(*p_pidl) = NULL;
			}
			ar >> byteHdrFlags;
			if( ( byteHdrFlags & BYTE(1) ) == 0 )
				return true;
			// continue with loading PIDL data
			DWORD dwBytesToRead = 0;
			ar >> dwBytesToRead;
			if( dwBytesToRead == 0 )
				::AfxThrowUserException();
			(*p_pidl) = LPITEMIDLIST( g_pMalloc->Alloc( dwBytesToRead ) );
			ar.Read( (*p_pidl), dwBytesToRead );
		} // loading
		return true;
	} // try
	catch( CException * pException )
	{
		pException;
		if( bEnableThrowExceptions )
			throw;
	} // catch( CException * pException )
	catch( ... )
	{
		if( bEnableThrowExceptions )
			throw;
	} // catch( ... )
	return false;
}

static CExtSafeString stat_productsection2regkeypath_for_pidl(
	__EXT_MFC_SAFE_LPCTSTR sPIDLName,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany, // under HKEY_CURRENT_USER\Software
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%
	)
{
	return CExtCmdManager::GetSubSystemRegKeyPath(
		__PROF_UIS_REG_SHELL_ITEM_ID,
		sPIDLName,
		sSectionNameCompany,
		sSectionNameProduct
		);
}

static bool stat_fileobj_to_registry_for_pidl(
	CFile & _file,
	__EXT_MFC_SAFE_LPCTSTR sPIDLName,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany, // under HKEY_CURRENT_USER\Software
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct, // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%
	HKEY hKeyRoot,
	bool bEnableThrowExceptions
	)
{
	ASSERT( sPIDLName != NULL );
	ASSERT( sSectionNameCompany != NULL );
	ASSERT( sSectionNameProduct != NULL );
CExtSafeString sRegKeyPath =
		stat_productsection2regkeypath_for_pidl(
			sPIDLName,
			sSectionNameCompany,
			sSectionNameProduct
			);

	return
		CExtCmdManager::FileObjToRegistry(
			_file,
			sRegKeyPath,
			hKeyRoot,
			bEnableThrowExceptions
			);
}

static bool stat_fileobj_from_registry_for_pidl(
	CFile & _file,
	__EXT_MFC_SAFE_LPCTSTR sPIDLName,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany, // under HKEY_CURRENT_USER\Software
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct, // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%
	HKEY hKeyRoot,
	bool bEnableThrowExceptions
	)
{
	ASSERT( sPIDLName != NULL );
	ASSERT( sSectionNameCompany != NULL );
	ASSERT( sSectionNameProduct != NULL );
	ASSERT( _file.GetLength() == 0 );
CExtSafeString sRegKeyPath =
		stat_productsection2regkeypath_for_pidl(
			sPIDLName,
			sSectionNameCompany,
			sSectionNameProduct
			);
	return
		CExtCmdManager::FileObjFromRegistry(
			_file,
			sRegKeyPath,
			hKeyRoot,
			bEnableThrowExceptions
			);
}

bool CExtShellBase::stat_ILSerialize(
	LPITEMIDLIST * p_pidl,
	__EXT_MFC_SAFE_LPCTSTR sPIDLName,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany, // under HKEY_CURRENT_USER\Software
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct, // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%
	bool bSave,
	HKEY hKeyRoot, //  = HKEY_CURRENT_USER
	bool bEnableThrowExceptions // = false
	)
{
	ASSERT( sSectionNameCompany != NULL );
	ASSERT( sSectionNameProduct != NULL );
bool bRetVal = false;
	try
	{
		CMemFile _file;
		if( bSave )
		{
			{ // BLOCK: CArchive usage
				CArchive ar(
					&_file,
					CArchive::store
					);
				if( ! stat_ILSerialize( p_pidl, ar, bEnableThrowExceptions ) )
					return false;
				ar.Flush();
			} // BLOCK: CArchive usage

			// ... write _file to registry
			_file.Seek(0,CFile::begin);
			if( ! stat_fileobj_to_registry_for_pidl(
					_file,
					sPIDLName,
					sSectionNameCompany,
					sSectionNameProduct,
					hKeyRoot,
					bEnableThrowExceptions
					)
				)
				return false;

		} // if( bSave )
		else
		{
			// ... read _file from registry
			if( ! stat_fileobj_from_registry_for_pidl(
					_file,
					sPIDLName,
					sSectionNameCompany,
					sSectionNameProduct,
					hKeyRoot,
					bEnableThrowExceptions
					)
				)
				return false;
			_file.Seek(0,CFile::begin);

			CArchive ar(
				&_file,
				CArchive::load
				);
			if( ! stat_ILSerialize( p_pidl, ar, bEnableThrowExceptions ) )
				return false;
		} // else from if( bSave )

		bRetVal = true;
	} // try
	catch( CException * pXept )
	{
		pXept->Delete();
		ASSERT( FALSE );
	} // catch( CException * pXept )
	catch( ... )
	{
		ASSERT( FALSE );
	} // catch( ... )
	return bRetVal;
}


HRESULT CExtShellBase::stat_SHBindToParent( LPCITEMIDLIST pidl, REFIID riid, LPVOID * ppv, LPCITEMIDLIST * ppidlLast )
{
	ASSERT( g_pMalloc != NULL );
	ASSERT( g_pDesktopFolder != NULL );
	if( pidl == NULL || ppv == NULL )
		return E_INVALIDARG;
HRESULT hr = E_FAIL;
	(*ppv) = NULL;
	if( ppidlLast != NULL )
		(*ppidlLast) = NULL;
	if( stat_ILIsSimple( pidl ) )
	{
		if( ppidlLast != NULL )
			(*ppidlLast) = stat_ILClone( pidl );
		hr = ::SHGetDesktopFolder( (IShellFolder**)ppv );
		hr = S_OK;
	}
	else
	{
		LPITEMIDLIST pidlChild = stat_ILClone ( stat_ILGetLast( (LPITEMIDLIST)pidl ) );
		LPITEMIDLIST pidlParent = stat_ILClone ( (LPITEMIDLIST)pidl );
		stat_ILRemoveLast( pidlParent );
		IShellFolder * pShellFolder = NULL;
		hr = ::SHGetDesktopFolder( &pShellFolder );
		if( SUCCEEDED( hr ) )
			hr = pShellFolder->BindToObject( pidlParent, NULL, riid, ppv );
		if( SUCCEEDED( hr ) && ppidlLast != NULL )
			(*ppidlLast) = pidlChild;
		else
			g_pMalloc->Free( pidlChild );
		g_pMalloc->Free( pidlParent );
		if( pShellFolder != NULL )
			pShellFolder->Release();
	}
	return hr;
}

HRESULT CExtShellBase::stat_SHGetFolderLocation( HWND hWnd, int csidl, HANDLE hToken, DWORD dwFlags, LPITEMIDLIST * ppidl )
{
	hToken;
	dwFlags;
	return ::SHGetSpecialFolderLocation( hWnd, csidl, ppidl );
}

/////////////////////////////////////////////////////////////////////////////
// CExtShellLock

CExtShellLock::CExtShellLock()
{
	Shell_Init();
}

CExtShellLock::~CExtShellLock()
{
	Shell_Done();
}

/////////////////////////////////////////////////////////////////////////////
// CExtCIP_SF

CExtCIP_SF::CExtCIP_SF()
{
}

CExtCIP_SF::CExtCIP_SF( IShellFolder * ptr )
{
	AssignFrom( ptr );
}

CExtCIP_SF::CExtCIP_SF( IShellFolder * pParentFolder, LPCITEMIDLIST pidlRelative )
{
	if( pidlRelative != NULL && pidlRelative->mkid.cb == 0 )
		AssignFrom( pParentFolder );
	else
		pParentFolder->BindToObject( pidlRelative, NULL, IID_IShellFolder, (LPVOID*)&m_ptr );
}

CExtCIP_SF::CExtCIP_SF( LPCITEMIDLIST pidlAbsolute )
{
	ASSERT( CExtShellBase::g_pDesktopFolder != NULL );
	if( pidlAbsolute != NULL && pidlAbsolute->mkid.cb == 0 )
		AssignFrom( CExtShellBase::g_pDesktopFolder );
	else
	{
		Empty();
		CExtShellBase::g_pDesktopFolder -> BindToObject( pidlAbsolute, NULL, IID_IShellFolder, GetPtrPtrVoid() );
	}
}

/////////////////////////////////////////////////////////////////////////////
// CExtCIP_SCM

CExtCIP_SCM::CExtCIP_SCM()
{
}

CExtCIP_SCM::CExtCIP_SCM(
	IShellFolder * pParentFolder,
	LPCITEMIDLIST pidlRelative,
	HWND hWndOwner // = NULL
	)
{
	ASSERT( pParentFolder != NULL );
	ASSERT( pidlRelative != NULL );
HRESULT hr = pParentFolder->GetUIObjectOf( hWndOwner, 1, &pidlRelative, IID_IContextMenu3, NULL, GetPtrPtrVoid() );
	if( FAILED( hr ) )
	{
		hr = pParentFolder->GetUIObjectOf( hWndOwner, 1, &pidlRelative, IID_IContextMenu2, NULL, GetPtrPtrVoid() );
		if( FAILED( hr ) )
		{
			hr = pParentFolder->GetUIObjectOf( hWndOwner, 1, &pidlRelative, IID_IContextMenu, NULL, GetPtrPtrVoid() );
			ASSERT( SUCCEEDED( hr ) );
			hr;
		}
	}
}

CExtCIP_SCM::CExtCIP_SCM(
	IShellFolder * pParentFolder,
	LPCITEMIDLIST * pidlArrayRelative,
	INT nCount,
	HWND hWndOwner // = NULL
	)
{
	ASSERT( pParentFolder != NULL );
	ASSERT( nCount > 0 );
	ASSERT( pidlArrayRelative != NULL );
HRESULT hr = pParentFolder->GetUIObjectOf( hWndOwner, UINT(nCount), pidlArrayRelative, IID_IContextMenu3, NULL, GetPtrPtrVoid() );
	if( FAILED( hr ) )
	{
		hr = pParentFolder->GetUIObjectOf( hWndOwner, UINT(nCount), pidlArrayRelative, IID_IContextMenu2, NULL, GetPtrPtrVoid() );
		if( FAILED( hr ) )
		{
			hr = pParentFolder->GetUIObjectOf( hWndOwner, UINT(nCount), pidlArrayRelative, IID_IContextMenu, NULL, GetPtrPtrVoid() );
			ASSERT( SUCCEEDED( hr ) );
			hr;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CExtCIP_SCM2

CExtCIP_SCM2::CExtCIP_SCM2( IContextMenu * ptr )
{
	if( ptr != NULL )
		ptr->QueryInterface( IID_IContextMenu2, GetPtrPtrVoid() );
}

/////////////////////////////////////////////////////////////////////////////
// CExtCIP_SCM3

CExtCIP_SCM3::CExtCIP_SCM3( IContextMenu * ptr )
{
	if( ptr != NULL )
		ptr->QueryInterface( IID_IContextMenu3, GetPtrPtrVoid() );
}

/////////////////////////////////////////////////////////////////////////////
// CExtSCM

const __EXT_MFC_SAFE_LPCTSTR CExtSCM::g_str_SH_CTXMENU_OBJ = _T("ShellContextMenu");

CExtSCM::CExtSCM()
	: m_nCmdFirstID( 0 )
	, m_nCmdLastID( 0 )
	, m_pWndOwner( NULL )
	, m_pfnWndProcRestore( NULL )
{
}

CExtSCM::~CExtSCM()
{
	_WatchOwnerWndMessages_Stop();
}

bool CExtSCM::Create( IShellFolder * pParentFolder, LPCITEMIDLIST pidl )
{
	ASSERT( pParentFolder != NULL );
	ASSERT( pidl != NULL );
	LPCITEMIDLIST pidl2 = CExtShellBase::stat_ILGetLast( pidl );
	ASSERT( pidl2 != NULL );
	m_pSCM = CExtCIP_SCM( pParentFolder, pidl2, m_pWndOwner->GetSafeHwnd() );
	return m_pSCM.IsEmpty() ? false : true;
}

bool CExtSCM::Create( IShellFolder * pParentFolder, LPCITEMIDLIST * pidlArrayRelative, INT nCount )
{
	ASSERT( pParentFolder != NULL );
	ASSERT( nCount > 0 );
	ASSERT( pidlArrayRelative != NULL );
	m_pSCM = CExtCIP_SCM( pParentFolder, pidlArrayRelative, nCount, m_pWndOwner->GetSafeHwnd() );
	return m_pSCM.IsEmpty() ? false : true;
}

bool CExtSCM::InvokeCommand( UINT nCmdID )
{
	if( m_pSCM.IsEmpty() )
		return false;
	if( ! IsCommandInRange( nCmdID ) )
		return false;
CMINVOKECOMMANDINFO cmi;
	::memset( &cmi, 0, sizeof(CMINVOKECOMMANDINFO) );
	cmi.cbSize = sizeof(CMINVOKECOMMANDINFO);
	cmi.hwnd = m_pWndOwner->GetSafeHwnd();
	cmi.lpVerb = (LPCSTR)MAKEINTRESOURCE(nCmdID - m_nCmdFirstID);
	cmi.nShow = SW_SHOWNORMAL;
	return ( m_pSCM->InvokeCommand( &cmi ) == NOERROR ) ? true : false;
}

BOOL CExtSCM::FillMenu(
	CMenu * pMenu,
	UINT nStartIndex, // = 0
	UINT nCmdFirstID, // = 1
	UINT nCmdLastID, // = 0x7FFF
	UINT nFlags // = 0
	)
{
	if( m_pSCM.IsEmpty() )
		return false;
	m_nCmdFirstID = nCmdFirstID;
	m_nCmdLastID = nCmdLastID;
UINT nDefID = pMenu->GetDefaultItem( GMDI_USEDISABLED, TRUE );
HRESULT hr =
			m_pSCM->QueryContextMenu(
				pMenu->GetSafeHmenu(),
				nStartIndex,
				m_nCmdFirstID,
				m_nCmdLastID,
				nFlags
				);
	if( ( nFlags & CMF_NODEFAULT ) != 0 )
		pMenu->SetDefaultItem( nDefID, TRUE );
	if( FAILED(hr) )
	{
		m_nCmdLastID = m_nCmdFirstID = 0;
		return false;
	}
	m_nCmdLastID = m_nCmdFirstID + HRESULT_CODE(hr) - 1;
	return true;
}

bool CExtSCM::GetCommandDescription( UINT nCmdID, CExtSafeString & strDescriptionText )
{
	if( m_pSCM.IsEmpty() )
		return false;
	if( ! IsCommandInRange( nCmdID ) )
		return false;
WCHAR strHelpTextBuffer[ MAX_PATH ];
	::memset( strHelpTextBuffer, 0, sizeof(WCHAR)*MAX_PATH );
HRESULT hr =
		m_pSCM->GetCommandString(
			nCmdID - m_nCmdFirstID,
			GCS_HELPTEXTW,
			NULL,
			(LPSTR)strHelpTextBuffer,
			MAX_PATH
			);
	if( (*(LPWSTR)strHelpTextBuffer) != 0 )
		strDescriptionText = (LPWSTR)strHelpTextBuffer;
	else
	{
		hr =
			m_pSCM->GetCommandString(
				nCmdID - m_nCmdFirstID,
				GCS_HELPTEXTA,
				NULL,
				(LPSTR)strHelpTextBuffer,
				MAX_PATH
				);
		if( (*(LPSTR)strHelpTextBuffer) != 0 )
		{
			USES_CONVERSION;
			strDescriptionText = W2CT(strHelpTextBuffer);
		}
	}
	if( SUCCEEDED(hr) )
		return true;
	return false;
}

void CExtSCM::GetCommandRange( UINT & nCmdFirstID, UINT & nCmdLastID ) const
{
	nCmdFirstID = m_nCmdFirstID;
	nCmdLastID = m_nCmdLastID;
}

bool CExtSCM::IsCommandInRange( UINT nCmdID ) const
{
	if( m_pSCM.IsEmpty() )
		return false;
	if(  m_nCmdFirstID <= nCmdID && nCmdID <= m_nCmdLastID )
		return true;
	return false;
}

CWnd * CExtSCM::GetOwner()
{
	return m_pWndOwner;
}

const CWnd * CExtSCM::GetOwner() const
{
	return m_pWndOwner;
}

void CExtSCM::SetOwner( CWnd * pWndOwner )
{
	if( pWndOwner->GetSafeHwnd() == m_pWndOwner->GetSafeHwnd() )
		return;
	_WatchOwnerWndMessages_Stop();
	m_pWndOwner = pWndOwner;
	_WatchOwnerWndMessages_Start();
}

void CExtSCM::_WatchOwnerWndMessages_Start()
{
	if( m_pWndOwner->GetSafeHwnd() == NULL )
		return;
	m_pfnWndProcRestore = (WNDPROC)
		__EXT_MFC_GetWindowLong( m_pWndOwner->m_hWnd, __EXT_MFC_GWL_WNDPROC );
	::SetProp( *m_pWndOwner, g_str_SH_CTXMENU_OBJ, (HANDLE)this );
	__EXT_MFC_SetWindowLong(
		m_pWndOwner->m_hWnd,
		__EXT_MFC_GWL_WNDPROC,
		(__EXT_MFC_DWORD_PTR)stat_WindowProc
		);
}

void CExtSCM::_WatchOwnerWndMessages_Stop()
{
	if( m_pWndOwner->GetSafeHwnd() == NULL )
	{
		m_pWndOwner = NULL;
		return;
	}
	::RemoveProp( *m_pWndOwner, g_str_SH_CTXMENU_OBJ );
	__EXT_MFC_SetWindowLong( *m_pWndOwner, __EXT_MFC_GWL_WNDPROC, (__EXT_MFC_LONG_PTR)m_pfnWndProcRestore );
	m_pWndOwner = NULL;
}

LRESULT CALLBACK CExtSCM::stat_WindowProc( HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam )
{
__PROF_UIS_MANAGE_STATE;
CExtSCM * pSCM = (CExtSCM*) ::GetProp( hwnd, g_str_SH_CTXMENU_OBJ );
LRESULT lResult;
	if( pSCM->_WatchOwnerWndMessages_WindowProc( message, wParam, lParam, &lResult ) )
		return lResult;	// success, no default processing
	if( message == WM_DESTROY )
		pSCM->_WatchOwnerWndMessages_Stop();
	return ::CallWindowProc( pSCM->m_pfnWndProcRestore, hwnd, message, wParam, lParam );
}

bool CExtSCM::_WatchOwnerWndMessages_WindowProc( UINT message, WPARAM wParam, LPARAM lParam, LRESULT * pResult )
{
	if( m_pSCM.IsEmpty() )
		return FALSE;
	switch( message )
	{
	case WM_MENUSELECT:
	{
		if( m_pWndOwner->GetSafeHwnd() == NULL )
			return false;
		CFrameWnd * pFrame = m_pWndOwner->GetTopLevelFrame();
		if( pFrame == NULL )
			return false;
		*pResult = 0L;
		UINT nFlags = (UINT) HIWORD(wParam);
		if( nFlags == 0xFFFF) // if the menu was closed
			return false;
		UINT nItemPosOrID = (UINT) LOWORD(wParam);
		if (nFlags & MF_POPUP)
			nItemPosOrID = ::GetMenuItemID( ::GetSubMenu( (HMENU)lParam, nItemPosOrID ), 0 );
		if(		( nFlags & MF_SEPARATOR ) == 0
			&&	( nItemPosOrID == 0 || ( ! IsCommandInRange( nItemPosOrID ) ) )
			)
			return false;
		CExtSafeString strStatusText;
		if( ( nFlags & MF_SEPARATOR ) == 0 )
			GetCommandDescription( nItemPosOrID, strStatusText );
		pFrame->SetMessageText( strStatusText );
		return true;
	}
	break; // case WM_MENUSELECT
	case WM_INITMENUPOPUP:
	case WM_MEASUREITEM:
	case WM_DRAWITEM:
	{
		*pResult = TRUE;
		if( message == WM_INITMENUPOPUP )
			*pResult = 0;
		if(		message == WM_MEASUREITEM
			&&	((LPMEASUREITEMSTRUCT)lParam)->CtlType != ODT_MENU
			)
			return false;
		if(		message == WM_DRAWITEM
			&&	((LPDRAWITEMSTRUCT)lParam)->CtlType != ODT_MENU
			)
			return false;
		CExtCIP_SCM3 pCtxMenu3( m_pSCM );
		if( ! pCtxMenu3.IsEmpty() )
			return ( pCtxMenu3->HandleMenuMsg( message, wParam, lParam ) == NOERROR ) ? true : false;
		CExtCIP_SCM2 pCtxMenu2( m_pSCM );
		if( pCtxMenu2.IsEmpty() )
			return false;
		return ( pCtxMenu2->HandleMenuMsg( message, wParam, lParam ) == NOERROR ) ? true : false;
	} // cases WM_INITMENUPOPUP, WM_MEASUREITEM, WM_DRAWITEM
	} // switch( message )
	return false;
}

/////////////////////////////////////////////////////////////////////////////
// CExtPIDL

CExtPIDL::CExtPIDL()
{
}

CExtPIDL::CExtPIDL( LPCITEMIDLIST pidl )
{
	Attach( (ITEMIDLIST*)CExtShellBase::stat_ILClone( pidl ) );
}

CExtPIDL::CExtPIDL( LPCITEMIDLIST pidlParent, LPCITEMIDLIST pidlRelative )
{
	Attach( (ITEMIDLIST*)CExtShellBase::stat_ILCombine( pidlParent, pidlRelative ) );
}

CExtPIDL::CExtPIDL( UINT nSpecialFolder, HWND hWndOwner )
{
	::SHGetSpecialFolderLocation( hWndOwner, nSpecialFolder, GetPtrPtr() );
}

CExtPIDL::CExtPIDL( __EXT_MFC_SAFE_LPCTSTR pszPath, HWND hWndOwner )
{
	Attach( (ITEMIDLIST*)CExtShellBase::stat_ILFromPath( pszPath, hWndOwner ) );
}

CExtPIDL::~CExtPIDL()
{
}

bool CExtPIDL::FromFolder( UINT nSpecialFolder, HWND hWndOwner )
{
	Empty();
	::SHGetSpecialFolderLocation( hWndOwner, nSpecialFolder, GetPtrPtr() );
	return IsEmpty() ? false : true;
}

bool CExtPIDL::FromFolder( __EXT_MFC_SAFE_LPCTSTR pszPath, HWND hWndOwner )
{
	Empty();
	Attach( (ITEMIDLIST*)CExtShellBase::stat_ILFromPath( pszPath, hWndOwner ) );
	return IsEmpty() ? false : true;
}

bool CExtPIDL::operator == ( const CExtSO < ITEMIDLIST > & other ) const
{
	ASSERT( CExtShellBase::g_pDesktopFolder != NULL );
	if( CExtShellBase::g_pDesktopFolder->CompareIDs( 0, GetPtr(), other.GetPtr() ) == 0 )
		return true;
	else
		return false;
}

bool CExtPIDL::operator == ( LPCITEMIDLIST pidl ) const
{
	ASSERT( CExtShellBase::g_pDesktopFolder != NULL );
LPCITEMIDLIST pidlOwn = GetPtr();
	if( pidlOwn == NULL )
	{
		if( pidl == NULL )
			return true;
		else
			return false;
	}
	else
	{
		if( pidl == NULL )
			return false;
	}
	if( CExtShellBase::g_pDesktopFolder->CompareIDs( 0, pidlOwn, pidl ) == 0 )
		return true;
	else
		return false;
}

bool CExtPIDL::IsRoot() const
{
	if( IsEmpty() )
		return false;
	return ( m_ptr->mkid.cb == 0 ) ? true : false;
}

int CExtPIDL::GetIconIndex(
	UINT uFlags // = SHGFI_SMALLICON
	) const
{
SHFILEINFO _sfi;
	::memset( &_sfi, 0, sizeof(SHFILEINFO) );
	uFlags |= SHGFI_PIDL | SHGFI_SYSICONINDEX;
	SHGetFileInfo((LPCTSTR)m_ptr, 0, &_sfi, sizeof(SHFILEINFO), uFlags);
	return _sfi.iIcon;
}

void CExtPIDL::Combine( LPCITEMIDLIST pidl1, LPCITEMIDLIST pidl2 )
{
	Empty();
	Attach( (ITEMIDLIST*)CExtShellBase::stat_ILCombine( pidl1, pidl2 ) );
}

void CExtPIDL::CloneParent( LPCITEMIDLIST pidl )
{
	Empty();
	Attach( (ITEMIDLIST*)CExtShellBase::stat_ILCloneParent( pidl ) );
}

void CExtPIDL::CloneLast( LPCITEMIDLIST pidl )
{
	Empty();
	Attach( (ITEMIDLIST*)CExtShellBase::stat_ILClone( CExtShellBase::stat_ILGetLast( pidl ) ) );
}

void CExtPIDL::CloneFirst( LPCITEMIDLIST pidl )
{
	Empty();
	Attach( (ITEMIDLIST*)CExtShellBase::stat_ILClone( CExtShellBase::stat_ILGetNext( pidl ) ) );
}

LPCITEMIDLIST CExtPIDL::GetFirst() const
{
	return CExtShellBase::stat_ILGetNext( m_ptr );
}

LPITEMIDLIST CExtPIDL::GetFirst()
{
	return CExtShellBase::stat_ILGetNext( m_ptr );
}

LPCITEMIDLIST CExtPIDL::GetLast() const
{
	return CExtShellBase::stat_ILGetLast( m_ptr );
}

LPITEMIDLIST CExtPIDL::GetLast()
{
	return CExtShellBase::stat_ILGetLast( m_ptr );
}

CExtSafeString CExtPIDL::GetPath() const
{
CExtSafeString path;
	if( ! IsEmpty() )
	{
		BOOL bSuccess = ::SHGetPathFromIDList( m_ptr, path.GetBuffer( MAX_PATH ) );
		path.ReleaseBuffer();
		if( ! bSuccess )
			path.Empty();
	}
	return path;
}

void CExtPIDL::AttachLast( LPITEMIDLIST pidl )
{
	ASSERT( m_ptr == NULL );
	Attach( (ITEMIDLIST*)CExtShellBase::stat_ILClone( CExtShellBase::stat_ILGetLast( pidl ) ) );
	ASSERT( CExtShellBase::g_pMalloc != NULL );
	CExtShellBase::g_pMalloc->Free( LPVOID(pidl) );
}

IShellFolder * CExtPIDL::GetParentFolder() const
{
IShellFolder * pParentFolder = NULL;
HRESULT hr = CExtShellBase::stat_SHBindToParent( GetPtr(), IID_IShellFolder, (void**)&pParentFolder, NULL );
	if( FAILED( hr ) )
		return NULL;
	ASSERT( pParentFolder != NULL );
	return pParentFolder;
}

bool CExtPIDL::_OnQueryWatchFS()
{
	if( IsEmpty() )
		return false;
CExtSafeString strPath = GetPath();
	if( strPath.IsEmpty() )
		return false;
	if(		_tcsnicmp( strPath, _T("A:"), 2 ) == 0
		||	_tcsnicmp( strPath, _T("B:"), 2 ) == 0
		||	_tcsnicmp( strPath, _T("\\\\"), 2 ) == 0
		)
		return false;
	return true;
}

DWORD CExtPIDL::GetAttributesOf(
	DWORD dwAttributesRequested // = DWORD(-1L)
	) const
{
	if( dwAttributesRequested == 0 )
		return false;
	if( IsEmpty() )
		return 0;
	if( ! IsSimplePIDL() )
	{
		CExtSafeString strPath = GetPath();
		if(		strPath.CompareNoCase( _T("A:\\") ) == 0
			||	strPath.CompareNoCase( _T("B:\\") ) == 0
			)
			return dwAttributesRequested&(SFGAO_FOLDER|SFGAO_REMOVABLE|SFGAO_FILESYSTEM|SFGAO_FILESYSANCESTOR);
	}
IShellFolder * pParentFolder = GetParentFolder();
	if( pParentFolder == NULL )
		return 0;
DWORD dwAttributes = dwAttributesRequested;
LPCITEMIDLIST pidl = CExtShellBase::stat_ILGetLast( GetPtr() );
HRESULT hr = pParentFolder->GetAttributesOf( 1, (LPCITEMIDLIST*)&pidl, &dwAttributes );
	pParentFolder->Release();
	if( FAILED( hr ) )
		return 0;
	return dwAttributes;
}

bool CExtPIDL::CheckAttributesOf( DWORD dwAttributesRequested ) const
{
DWORD dwAttributes = GetAttributesOf( dwAttributesRequested );
DWORD dwAttributesPresent = dwAttributes & dwAttributesRequested;
bool bRetVal = ( dwAttributesPresent == dwAttributesRequested ) ? true : false;
	return bRetVal;
}

bool CExtPIDL::IsReadOnly() const
{
bool bRetVal = CheckAttributesOf( SFGAO_READONLY );
	return bRetVal;
}

bool CExtPIDL::IsHidden() const
{
bool bRetVal = CheckAttributesOf( SFGAO_HIDDEN );
	return bRetVal;
}

bool CExtPIDL::IsFolder() const
{
	// basic folder detection
bool bRetVal = CheckAttributesOf( SFGAO_FOLDER );
	if( ! bRetVal )
		return false;
	// exclude ZIP folders
DWORD dwAttributes = GetAttributesOf( SFGAO_FILESYSTEM|SFGAO_FILESYSANCESTOR|SFGAO_BROWSABLE );
	if( dwAttributes != (SFGAO_BROWSABLE|SFGAO_FILESYSTEM) )
		return false;
	return true;
}

bool CExtPIDL::IsSimplePIDL() const
{
bool bRetVal = CExtShellBase::stat_ILIsSimple( GetPtr() );
	return bRetVal;
}

CExtSafeString CExtPIDL::GetDisplayNameOf(
	IShellFolder * pShellFolder, // = NULL
	DWORD _shgdnf // = __EXT_MFC_SHGDN_NORMAL
	) const
{
	if( pShellFolder == NULL )
	{
		ASSERT( CExtShellBase::g_pDesktopFolder != NULL );
		pShellFolder = CExtShellBase::g_pDesktopFolder;
	}
CExtSafeString strNameBuffer;
STRRET strRetData;
	::memset( &strRetData, 0, sizeof(STRRET) );
	strRetData.uType = STRRET_WSTR;
	pShellFolder->GetDisplayNameOf( GetPtr(), _shgdnf, &strRetData );
	switch( strRetData.uType )
	{
	case STRRET_WSTR:
	{
		USES_CONVERSION;
		strNameBuffer = W2CT(strRetData.pOleStr);
	}
	break;
	case STRRET_CSTR:
	{
		USES_CONVERSION;
		strNameBuffer = A2CT(strRetData.cStr);
		strRetData.pOleStr = NULL;
	}
	break;
	case STRRET_OFFSET:
	{
		USES_CONVERSION;
		strNameBuffer = A2CT(((char*)((LPBYTE)GetPtr()+strRetData.uOffset)));
		strRetData.pOleStr = NULL;
	}
	break;
	}
	if( strRetData.pOleStr != NULL )
		::CoTaskMemFree( strRetData.pOleStr );
	return strNameBuffer;
}

bool CExtPIDL::GetSpecialFolderLocation(
	HWND hWnd,
	int nFolder
	)
{
	Empty();
HRESULT hr = ::SHGetSpecialFolderLocation( hWnd, nFolder, &m_ptr );
	if( FAILED(hr) )
		return false;
	ASSERT( ! IsEmpty() );
	return true;
}

bool CExtPIDL::Serialize(
	CArchive & ar,
	bool bEnableThrowExceptions // = false
	)
{
	return CExtShellBase::stat_ILSerialize( GetPtrPtr(), ar, bEnableThrowExceptions );
}

bool CExtPIDL::Serialize(
	__EXT_MFC_SAFE_LPCTSTR sPIDLName,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany, // under HKEY_CURRENT_USER\Software
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct, // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%
	bool bSave,
	HKEY hKeyRoot, //  = HKEY_CURRENT_USER
	bool bEnableThrowExceptions // = false
	)
{
	return
		CExtShellBase::stat_ILSerialize(
			GetPtrPtr(),
			sPIDLName,
			sSectionNameCompany,
			sSectionNameProduct,
			bSave,
			hKeyRoot,
			bEnableThrowExceptions
			);
}

/////////////////////////////////////////////////////////////////////////////
// CExtArrayOfPIDLs

CExtArrayOfPIDLs::CExtArrayOfPIDLs()
{
}

CExtArrayOfPIDLs::~CExtArrayOfPIDLs()
{
	Empty();
}

INT CExtArrayOfPIDLs::GetSize() const
{
	ASSERT( this != NULL );
	INT nSize = INT( CTypedPtrArray < CPtrArray, CExtPIDL * > :: GetSize() );
	return nSize;
}

INT CExtArrayOfPIDLs::GetCount() const
{
	ASSERT( this != NULL );
	return GetSize();
}

CExtPIDL * CExtArrayOfPIDLs::GetObjAt( INT nIndex )
{
	ASSERT( this != NULL );
	if( nIndex < 0 )
	{
		//ASSERT( FALSE );
		return NULL;
	}
	INT nCount = GetCount();
	if( nIndex >= nCount )
	{
		//ASSERT( FALSE );
		return NULL;
	}
	CExtPIDL * ptr = CTypedPtrArray < CPtrArray, CExtPIDL * > :: ElementAt( nIndex );
	return ptr;
}

const CExtPIDL * CExtArrayOfPIDLs::GetObjAt( INT nIndex ) const
{
	ASSERT( this != NULL );
	return ( const_cast < CExtArrayOfPIDLs * > ( this ) ) -> GetObjAt( nIndex );
}

LPITEMIDLIST CExtArrayOfPIDLs::GetAt( INT nIndex )
{
	ASSERT( this != NULL );
CExtPIDL * ptr = GetObjAt( nIndex );
	if( ptr == NULL )
		return NULL;
LPITEMIDLIST pidl = ptr->GetPtr();
	return pidl;
}

LPCITEMIDLIST CExtArrayOfPIDLs::GetAt( INT nIndex ) const
{
	ASSERT( this != NULL );
	return ( const_cast < CExtArrayOfPIDLs * > ( this ) ) -> GetAt( nIndex );
}

LPITEMIDLIST CExtArrayOfPIDLs::operator [] ( INT nIndex )
{
	ASSERT( this != NULL );
	return GetAt( nIndex );
}

LPCITEMIDLIST CExtArrayOfPIDLs::operator [] ( INT nIndex ) const
{
	ASSERT( this != NULL );
	return GetAt( nIndex );
}

bool CExtArrayOfPIDLs::SetAt( INT nIndex, LPCITEMIDLIST pidl )
{
	ASSERT( this != NULL );
	if( nIndex < 0 )
	{
		//ASSERT( FALSE );
		return false;
	}
INT nCount = GetCount();
	if( nIndex >= nCount )
	{
		//ASSERT( FALSE );
		return false;
	}
CExtPIDL * ptr = CTypedPtrArray < CPtrArray, CExtPIDL * > :: ElementAt( nIndex );
	if( pidl == NULL )
	{
		if( ptr != NULL )
		{
			delete ptr;
			CTypedPtrArray < CPtrArray, CExtPIDL * > :: SetAt( nIndex, (CExtPIDL*)NULL );
		}
	}
	else
	{
		if( ptr == NULL )
		{
			ptr = new CExtPIDL;
			CTypedPtrArray < CPtrArray, CExtPIDL * > :: SetAt( nIndex, ptr );
		}
		(*ptr) = pidl;
	}
	return true;
}

bool CExtArrayOfPIDLs::SetAt( INT nIndex, const CExtPIDL & pidl )
{
	ASSERT( this != NULL );
	return SetAt( nIndex, pidl.GetPtr() );
}

void CExtArrayOfPIDLs::RemoveAll()
{
	ASSERT( this != NULL );
INT nIndex, nCount = GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
		SetAt( nIndex, NULL );
	CTypedPtrArray < CPtrArray, CExtPIDL * > :: RemoveAll();
}

bool CExtArrayOfPIDLs::InsertAt( INT nIndex, LPCITEMIDLIST pidl )
{
	ASSERT( this != NULL );
//	if( nIndex < 0 )
//	{
//		//ASSERT( FALSE );
//		return false;
//	}
INT nCount = GetCount();
	if( nIndex < 0 )
		nIndex = nCount;
	else if( nIndex > nCount )
	{
		//ASSERT( FALSE );
		return false;
	}
	CTypedPtrArray < CPtrArray, CExtPIDL * > :: InsertAt( nIndex, (CExtPIDL*)NULL );
	SetAt( nIndex, pidl );
	return true;
}

bool CExtArrayOfPIDLs::InsertAt( INT nIndex, const CExtPIDL & pidl )
{
	ASSERT( this != NULL );
	return InsertAt( nIndex, pidl.GetPtr() );
}

INT CExtArrayOfPIDLs::RemoveAt(
	INT nIndex,
	INT nCountToRemove // = 1
	) // returns removed count
{
	ASSERT( this != NULL );
INT nCount = GetCount();
	if( nIndex < 0 || nCount <= 0 || nIndex >= nCount )
		return 0;
INT nCountAvailableToRemove =  nCount - nIndex;
	if( nCountAvailableToRemove == 0 )
		return 0;
INT nCountRemoved = min( nCountAvailableToRemove, nCountToRemove );
INT nIndexRemoving = nIndex, nLastIndexToRemove = nIndex + nCountRemoved - 1;
	for( ; nIndexRemoving <= nLastIndexToRemove; nIndexRemoving ++ )
		SetAt( nIndexRemoving, NULL );
	CTypedPtrArray < CPtrArray, CExtPIDL * > :: RemoveAt( nIndex, nCountRemoved );
	return nCountRemoved;
}

INT CExtArrayOfPIDLs::RemoveFromUpToTail( INT nIndex )
{
	ASSERT( this != NULL );
	if( nIndex < 0 )
		return 0;
	return RemoveAt( nIndex, GetCount() - nIndex );
}

bool CExtArrayOfPIDLs::Add( LPCITEMIDLIST pidl )
{
	ASSERT( this != NULL );
	return InsertAt( -1, pidl );
}

bool CExtArrayOfPIDLs::AddHead( LPCITEMIDLIST pidl )
{
	ASSERT( this != NULL );
	return InsertAt( 0, pidl );
}

bool CExtArrayOfPIDLs::AddTail( LPCITEMIDLIST pidl )
{
	ASSERT( this != NULL );
	return InsertAt( -1, pidl );
}

bool CExtArrayOfPIDLs::RemoveHead()
{
	ASSERT( this != NULL );
INT nCountRemoved = RemoveAt( 0, 1 );
	return ( nCountRemoved > 0 ) ? true : false;
}

bool CExtArrayOfPIDLs::RemoveTail()
{
	ASSERT( this != NULL );
INT nCount = GetCount();
	if( nCount == 0 )
		return false;
INT nCountRemoved = RemoveAt( nCount - 1, 1 );
	return ( nCountRemoved > 0 ) ? true : false;
}

bool CExtArrayOfPIDLs::IsEmpty() const
{
	ASSERT( this != NULL );
	if( GetSize() == 0 )
		return true;
	else
		return false;
}

void CExtArrayOfPIDLs::Empty()
{
	ASSERT( this != NULL );
	RemoveAll();
}

bool CExtArrayOfPIDLs::InsertAt( INT nStartIndex, const CExtArrayOfPIDLs & _arr )
{
	ASSERT( this != NULL );
INT nOtherCount = _arr.GetCount();
	if( nOtherCount == 0 )
		return true;
INT nCount = GetCount();
	if( nStartIndex < 0 )
		nStartIndex = nCount;
	else if( nStartIndex > nCount )
		return false;
INT nOtherIndex = 0;
	for( ; nOtherCount > 0; nOtherCount ++, nStartIndex ++, nOtherIndex ++ )
		InsertAt( nStartIndex, _arr.GetAt( nOtherIndex ) );
	return true;
}

bool CExtArrayOfPIDLs::InsertAt( INT nStartIndex , const CExtArrayOfPIDLs * pArray )
{
	ASSERT( this != NULL );
	if( pArray == NULL )
		return false;
	return InsertAt( nStartIndex, *pArray );
}

bool CExtArrayOfPIDLs::Append( const CExtArrayOfPIDLs & _arr )
{
	ASSERT( this != NULL );
	return InsertAt( -1, _arr );
}

bool CExtArrayOfPIDLs::Append( const CExtArrayOfPIDLs * pArray )
{
	ASSERT( this != NULL );
	if( pArray == NULL )
		return false;
	return Append( *pArray );
}

bool CExtArrayOfPIDLs::Copy( const CExtArrayOfPIDLs & _arr )
{
	ASSERT( this != NULL );
	Empty();
	return Append( _arr );
}

bool CExtArrayOfPIDLs::Copy( const CExtArrayOfPIDLs * pArray )
{
	ASSERT( this != NULL );
	if( pArray == NULL )
		return false;
	return Copy( *pArray );
}

INT CExtArrayOfPIDLs::Find(
	LPCITEMIDLIST pidl,
	INT nStartSearchIndex // = -1
	) const
{
	ASSERT( this != NULL );
	if( nStartSearchIndex < 0 )
		nStartSearchIndex = 0;
	else
		nStartSearchIndex ++;
INT nCount = GetCount();
INT nIndex = nStartSearchIndex;
	for( ; nIndex < nCount; nIndex ++ )
	{
		const CExtPIDL * ptr = GetObjAt( nIndex );
		if( ptr == NULL )
		{
			if( pidl == NULL )
				return nIndex;
			continue;
		}
		if( (*ptr) == pidl )
			return nIndex;
	}
	return -1;
}

INT CExtArrayOfPIDLs::Find(
	const CExtPIDL & pidl,
	INT nStartSearchIndex // = -1
	) const
{
	ASSERT( this != NULL );
	return Find( pidl.GetPtr(), nStartSearchIndex );
}

INT CExtArrayOfPIDLs::MakeUnique() // returns count of removed items
{
INT nIndexOuter, nIndexInner, nCount = GetSize(), nCountRemoved = 0;
	for( nIndexOuter = 0; nIndexOuter < nCount; )
	{
		CExtPIDL * pOuter = GetObjAt( nIndexOuter );
		if( pOuter == NULL )
		{
			nCountRemoved ++;
			RemoveAt( nIndexOuter ); // remove NULL items
			nCount --;
			continue;
		}
		ASSERT( ! pOuter->IsEmpty() );
		for( nIndexInner = nIndexOuter + 1; nIndexInner < nCount; nIndexInner ++ )
		{
			CExtPIDL * pInner = GetObjAt( nIndexInner );
			ASSERT( pInner != NULL );
			ASSERT( ! pInner->IsEmpty() );
			if( (*pOuter) != (*pInner) )
				continue;
			nCountRemoved ++;
			RemoveAt( nIndexInner );
			nCount --;
		}
		nIndexOuter ++;
	}
	return nCountRemoved;
}

/////////////////////////////////////////////////////////////////////////////
// CExtShellItemData

CExtShellItemData::CExtShellItemData()
	: m_eSIT( __ESIT_NORMAL )
	, m_pSWDC( NULL )
	, m_dwCachedAttributes( 0 )
{
}

CExtShellItemData::~CExtShellItemData()
{
	if( m_pSWDC != NULL )
		delete m_pSWDC;
}

bool CExtShellItemData::IsValid() const
{
	if( this == NULL )
		return false;
	if( m_pidlAbsolute.IsEmpty() )
		return false;
	return true;
}

bool CExtShellItemData::UpdateCachedAttributes() const
{
DWORD dwOldCachedAttributes = m_dwCachedAttributes;
	m_dwCachedAttributes = m_pidlAbsolute.GetAttributesOf( __EXT_DEFAULT_ATTRIBUTES_FOR_CExtPIDL_GetAttributesOf__ );
bool bAnyChanged = ( m_dwCachedAttributes == dwOldCachedAttributes ) ? false : true;
	return bAnyChanged;
}

/////////////////////////////////////////////////////////////////////////////
// CExtShellContentNode

CExtShellContentNode::CExtShellContentNode(
	CExtShellContentNode * pParent // = NULL
	)
	: m_pParent( pParent )
{
}

CExtShellContentNode::~CExtShellContentNode()
{
	Empty();
}

void CExtShellContentNode::RemoveChildren()
{
POSITION pos = m_listChildren.GetHeadPosition();
	for( ; pos != NULL; )
	{
		CExtShellContentNode * pNode = m_listChildren.GetNext( pos );
		ASSERT( pNode != NULL );
		delete pNode;
	}
	m_listChildren.RemoveAll();
}

void CExtShellContentNode::Empty()
{
	m_pidlRelative.Empty();
	m_pParent = NULL;
	RemoveChildren();
}

CExtPIDL CExtShellContentNode::GetAbsPIDL() const
{
	if( m_pParent == NULL )
		return m_pidlRelative;
CExtPIDL pidlParentAbs = m_pParent->GetAbsPIDL();
	ASSERT( ! pidlParentAbs.IsEmpty() );
CExtPIDL pidl;
	ASSERT( ! m_pidlRelative.IsEmpty() );
	pidl.Combine( pidlParentAbs.GetPtr(), m_pidlRelative.GetPtr() );
	return pidl;
}

IShellFolder * CExtShellContentNode::GetSF() const
{
CExtPIDL pidl = GetAbsPIDL();
CExtCIP_SF _SF( pidl.GetPtr() );
	return _SF.Detach();
}

IShellFolder * CExtShellContentNode::GetParentSF() const
{
	if( m_pParent == NULL )
		return NULL;
	return m_pParent->GetSF();
}

CExtSafeString CExtShellContentNode::GetDisplayNameOf(
	DWORD _shgdnf // = __EXT_MFC_SHGDN_NORMAL
	) const
{
IShellFolder * pSF = GetParentSF();
	if( pSF == NULL )
		return m_pidlRelative.GetDisplayNameOf( NULL, _shgdnf );
	CExtSafeString s = m_pidlRelative.GetDisplayNameOf( pSF, _shgdnf );
	pSF->Release();
	return s;
}

void CExtShellContentNode::_ExpandChildren(
	HWND hWnd,
	bool bIncludeHidden,
	INT nIndentLimit,
	INT nCurrentIndent,
	bool bAddToTail,
	IShellFolder * pSF,
	DWORD dwAttributeFilterAny,
	DWORD dwAttributeFilterAllPresent,
	DWORD dwAttributeFilterAllAbsent
	)
{
	ASSERT( ! m_pidlRelative.IsEmpty() );
	ASSERT( pSF != NULL );
	ASSERT( LPVOID(m_pParent) != LPVOID(this) );
	ASSERT( nCurrentIndent <= nIndentLimit );
	RemoveChildren();
	ASSERT( CExtShellBase::g_pDesktopFolder != NULL );
	ASSERT( CExtShellBase::g_pidlDesktop != NULL );
	ASSERT( CExtShellBase::g_pMalloc != NULL );
CExtPIDL pidlAbsThis = GetAbsPIDL();
	if( pidlAbsThis.IsEmpty() )
	{
		ASSERT( FALSE );
		return;
	}
IEnumIDList * pEnumIDList = NULL;
	if(	pSF->EnumObjects(
			hWnd,
			SHCONTF_FOLDERS | ( bIncludeHidden ? SHCONTF_INCLUDEHIDDEN : 0 ),
			&pEnumIDList
			) == NOERROR
		)
	{
		LPITEMIDLIST pidl = NULL;
		for( ; pEnumIDList->Next( 1, &pidl, NULL ) == NOERROR; )
		{
			ASSERT( pidl != NULL );
			CExtPIDL pidlAbsolute( (LPCITEMIDLIST)pidlAbsThis, pidl );
			if( ! pidlAbsolute.IsEmpty() )
			{
				DWORD dwAttributes = pidlAbsolute.GetAttributesOf( __EXT_DEFAULT_ATTRIBUTES_FOR_CExtPIDL_GetAttributesOf__ );
				bool bAdd = pidlAbsolute.IsRoot();
				if( ! bAdd )
				{
					bAdd = ( ( dwAttributes & dwAttributeFilterAllPresent ) == dwAttributeFilterAllPresent ) ? true : false;
					if( bAdd )
					{
						bAdd = ( ( dwAttributes & dwAttributeFilterAllAbsent ) == 0 ) ? true : false;
						if( bAdd )
							bAdd = ( ( dwAttributes & dwAttributeFilterAny ) != 0 ) ? true : false;
					}
				}
				if( bAdd )
				{
					CExtShellContentNode * pNode = new CExtShellContentNode;
					pNode->_BuildContent_Impl(
						hWnd,
						bIncludeHidden,
						nIndentLimit,
						this,
						pidl,
						nCurrentIndent + 1,
						bAddToTail,
						dwAttributeFilterAny,
						dwAttributeFilterAllPresent,
						dwAttributeFilterAllAbsent
						);
					if( bAddToTail )
						m_listChildren.AddTail( pNode );
					else
						m_listChildren.AddHead( pNode );
				}
			} // if( ! pidlAbsolute.IsEmpty() )
			CExtShellBase::g_pMalloc->Free( pidl );
			pidl = NULL;
		} // for( ; pEnumIDList->Next( 1, &pidl, NULL ) == NOERROR; )
		pEnumIDList->Release();
	}
}

void CExtShellContentNode::_BuildContent_Impl(
	HWND hWnd,
	bool bIncludeHidden,
	INT nIndentLimit,
	CExtShellContentNode * pParent,
	LPITEMIDLIST pidlRelative,
	INT nCurrentIndent,
	bool bAddToTail,
	DWORD dwAttributeFilterAny,
	DWORD dwAttributeFilterAllPresent,
	DWORD dwAttributeFilterAllAbsent
	)
{
	ASSERT( LPVOID(m_pParent) != LPVOID(this) );
	ASSERT( nCurrentIndent <= nIndentLimit );
	RemoveChildren();
	ASSERT( CExtShellBase::g_pDesktopFolder != NULL );
	ASSERT( CExtShellBase::g_pidlDesktop != NULL );
	ASSERT( CExtShellBase::g_pMalloc != NULL );
CExtCIP_SF _SF;
	if( pParent == NULL )
	{
		// make itself desktop folder
		ASSERT( pidlRelative == NULL );
		m_pParent = NULL;
		m_pidlRelative = CExtShellBase::g_pidlDesktop;
		if( nCurrentIndent < nIndentLimit )
			_SF = CExtShellBase::g_pDesktopFolder;
	}
	else
	{
		ASSERT( pidlRelative != NULL );
		m_pParent = pParent;
		m_pidlRelative = pidlRelative;
		if( nCurrentIndent < nIndentLimit )
		{
			IShellFolder * pSF = GetSF();
			ASSERT( pSF != NULL );
			_SF.Attach( pSF );
		}
	}
	if( nCurrentIndent == nIndentLimit )
		return;
	ASSERT( ! _SF.IsEmpty() );
	_ExpandChildren(
		hWnd,
		bIncludeHidden,
		nIndentLimit,
		nCurrentIndent,
		bAddToTail,
		_SF.GetPtr(),
		dwAttributeFilterAny,
		dwAttributeFilterAllPresent,
		dwAttributeFilterAllAbsent
		);
}

void CExtShellContentNode::ExpandChildren(
	HWND hWnd,
	bool bIncludeHidden,
	DWORD dwAttributeFilterAny,
	DWORD dwAttributeFilterAllPresent,
	DWORD dwAttributeFilterAllAbsent,
	INT nIndentLimit, // = 1
	bool bAddToTail // = true
	)
{
	if( nIndentLimit <= 0 )
		return;
	ASSERT( CExtShellBase::g_pDesktopFolder != NULL );
IShellFolder * pSF = GetSF();
	_ExpandChildren(
		hWnd,
		bIncludeHidden,
		nIndentLimit,
		0,
		bAddToTail,
		pSF == NULL ? CExtShellBase::g_pDesktopFolder : pSF,
		dwAttributeFilterAny,
		dwAttributeFilterAllPresent,
		dwAttributeFilterAllAbsent
		);
	pSF->Release();
}

void CExtShellContentNode::ExpandPIDL(
	LPCITEMIDLIST pidlToExpand
	)
{
	ASSERT( m_pParent == NULL ); // this method should be invoked for root node only
	ASSERT( CExtShellBase::g_pDesktopFolder != NULL );
	ASSERT( CExtShellBase::g_pidlDesktop != NULL );
	ASSERT( CExtShellBase::g_pMalloc != NULL );
	if( CExtShellBase::g_pDesktopFolder->CompareIDs( 0, CExtShellBase::g_pidlDesktop, pidlToExpand ) == 0 )
		return;
LPITEMIDLIST pidlWalk = (LPITEMIDLIST)pidlToExpand, pidlNext = (LPITEMIDLIST)pidlToExpand;
CTypedPtrList < CPtrList, LPITEMIDLIST > _listWalk;
POSITION pos = NULL;
	_listWalk.AddHead( CExtShellBase::stat_CopyItem( pidlNext ) );
	for( ; CExtShellBase::stat_GetParentItem( pidlNext, pidlWalk ) > 0; )
	{
		_listWalk.AddHead( pidlWalk );
		pidlNext = pidlWalk;
	}
	CExtShellContentNode * pNodeLast = this;
	for( pos = _listWalk.GetHeadPosition(); pos != NULL; )
	{
		LPITEMIDLIST pidlFromWalkList = _listWalk.GetNext( pos );
		ASSERT( pidlFromWalkList != NULL );
		LPCITEMIDLIST pidlRelative = CExtShellBase::stat_ILGetLast( pidlFromWalkList );
		CExtShellContentNode * pNode = pNodeLast->FindChild( pidlRelative );
		if( pNode != NULL )
		{
			pNodeLast = pNode;
			continue;
		}
		pNode = new CExtShellContentNode;
		pNode->m_pParent = pNodeLast;
		pNode->m_pidlRelative = pidlRelative;
		pNodeLast->m_listChildren.AddTail( pNode );
		pNodeLast = pNode;
	}
	for( pos = _listWalk.GetHeadPosition(); pos != NULL; )
	{
		LPITEMIDLIST pidlFromWalkList = _listWalk.GetNext( pos );
		ASSERT( pidlFromWalkList != NULL );
		if( pidlFromWalkList != NULL && LPVOID(pidlFromWalkList) != LPVOID(CExtShellBase::g_pidlDesktop) )
			CExtShellBase::g_pMalloc->Free( pidlFromWalkList );
	}
	_listWalk.RemoveAll();
}

void CExtShellContentNode::BuildContent(
	HWND hWnd,
	bool bIncludeHidden,
	DWORD dwAttributeFilterAny,
	DWORD dwAttributeFilterAllPresent,
	DWORD dwAttributeFilterAllAbsent,
	INT nIndentLimit, // = 1
	CExtShellContentNode * pParent, // = NULL
	LPITEMIDLIST pidlRelative, // = NULL
	bool bAddToTail // = true
	)
{
	if( nIndentLimit < 0 )
		return;
	Empty();
	_BuildContent_Impl(
		hWnd,
		bIncludeHidden,
		nIndentLimit,
		pParent,
		pidlRelative,
		0,
		bAddToTail,
		dwAttributeFilterAny,
		dwAttributeFilterAllPresent,
		dwAttributeFilterAllAbsent
		);
}

CExtShellContentNode * CExtShellContentNode::Find(
	LPITEMIDLIST pidlAbsolute
	)
{
	if( pidlAbsolute == NULL )
	{
		ASSERT( FALSE );
		return NULL;
	}
	if( m_pidlRelative.IsEmpty() )
		return NULL;
	ASSERT( CExtShellBase::g_pDesktopFolder != NULL );
CExtPIDL pidlAbsOwn = GetAbsPIDL();
	if( pidlAbsOwn == pidlAbsolute )
		return this;
POSITION pos = m_listChildren.GetHeadPosition();
	for( ; pos != NULL; )
	{
		CExtShellContentNode * pNode = m_listChildren.GetNext( pos );
		ASSERT( pNode != NULL );
		CExtShellContentNode * pNodeFound = pNode->Find( pidlAbsolute );
		if( pNodeFound != NULL )
			return pNodeFound;
	}
	return NULL;
}

CExtShellContentNode * CExtShellContentNode::FindChild(
	LPCITEMIDLIST pidlRelative
	)
{
	if( pidlRelative == NULL )
	{
		ASSERT( FALSE );
		return NULL;
	}
	if( m_pidlRelative.IsEmpty() )
		return NULL;
	ASSERT( CExtShellBase::g_pDesktopFolder != NULL );
CExtPIDL pidlAbsOwn = GetAbsPIDL();
CExtPIDL pidlFindAbs;
	pidlFindAbs.Combine( pidlAbsOwn.GetPtr(), pidlRelative );
POSITION pos = m_listChildren.GetHeadPosition();
	for( ; pos != NULL; )
	{
		CExtShellContentNode * pNode = m_listChildren.GetNext( pos );
		ASSERT( pNode != NULL );
		CExtPIDL pidlChildAbs = pNode->GetAbsPIDL();
		if( pidlChildAbs == pidlFindAbs )
			return pNode;
	}
	return NULL;
}

void CExtShellContentNode::_FillLinearList_Impl(
	CTypedPtrList < CPtrList, CExtShellContentNode * > & _list,
	bool bAddToTail
	)
{
	if( bAddToTail )
		_list.AddTail( this );
	else
		_list.AddHead( this );
POSITION pos = m_listChildren.GetHeadPosition();
	for( ; pos != NULL; )
	{
		CExtShellContentNode * pNode = m_listChildren.GetNext( pos );
		ASSERT( pNode != NULL );
		pNode->_FillLinearList_Impl( _list, bAddToTail );
	}
}

void CExtShellContentNode::FillLinearList(
	CTypedPtrList < CPtrList, CExtShellContentNode * > & _list,
	bool bAddToTail // = true
	)
{
	_list.RemoveAll();
	_FillLinearList_Impl( _list, bAddToTail );
}

void CExtShellContentNode::FillLinearArray(
	CTypedPtrArray < CPtrArray, CExtShellContentNode * > & _arr,
	bool bAddToTail // = true
	)
{
CTypedPtrList < CPtrList, CExtShellContentNode * > _list;
	_FillLinearList_Impl( _list, bAddToTail );
INT nIndex, nCount = INT( _list.GetCount() );
	_arr.RemoveAll();
	_arr.SetSize( nCount );
POSITION pos = _list.GetHeadPosition();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		ASSERT( pos != NULL );
		CExtShellContentNode * pNode = _list.GetNext( pos );
		ASSERT( pNode != NULL );
		_arr.SetAt( nIndex, pNode );
	}
}

INT CExtShellContentNode::GetIndent() const
{
	if( m_pParent == NULL )
		return 0;
	else
		return ( m_pParent->GetIndent() + 1 );
}

bool CExtShellContentNode::GetIcon(
	CExtBitmap & _bmpIcon,
	bool bBigIcon // = false
	) const
{
	_bmpIcon.Empty();
	if( m_pidlRelative.IsEmpty() )
		return false;
SHFILEINFO _sfi;
	::memset( &_sfi, 0, sizeof(SHFILEINFO) );
CExtPIDL pidl = GetAbsPIDL();
	::SHGetFileInfo(
		(LPCTSTR)pidl.GetPtr(),
		0,
		&_sfi,
		sizeof(SHFILEINFO),
		SHGFI_PIDL | SHGFI_ICON | ( bBigIcon ? SHGFI_LARGEICON : SHGFI_SMALLICON )
		);
	if( _sfi.hIcon == NULL )
		return false;
bool bRetVal = _bmpIcon.AssignFromHICON( _sfi.hIcon );
	::DestroyIcon( _sfi.hIcon );
	return bRetVal;
}

#endif // ( ! defined __EXT_MFC_NO_SHELL )

#if ( ! defined __EXT_MFC_NO_SHELL_COMBO_BOX )

/////////////////////////////////////////////////////////////////////////////
// CExtShellComboBox

const UINT CExtShellComboBox::g_nMsgShellLocationChanged = ::RegisterWindowMessage( _T("CExtShellComboBox::g_nMsgShellLocationChanged") );

CExtShellComboBox::CExtShellComboBox()
	: m_bIncludeHiddenFolders( true )
	, m_nListBoxIndentPxAt96DPI( 19 )
	, m_nListBoxItemHeightPxAt96DPI( 18 )
	, m_nIconToTextMargin( 3 )
	, m_nDrawTextFlags( DT_LEFT|DT_VCENTER|DT_SINGLELINE|DT_NOPREFIX|DT_END_ELLIPSIS )
	, m_bFullRowSelection( false )
	, m_bDisplayFullPathIfAvailable( true )
	, m_bHelperEnableShellAutoComplete( false )
	, m_dwHelperFlagsSHACF(
			__EXT_MFC_SHACF_FILESYSTEM
				|__EXT_MFC_SHACF_AUTOSUGGEST_FORCE_ON
				|__EXT_MFC_SHACF_AUTOAPPEND_FORCE_ON
				|__EXT_MFC_SHACF_USETAB
			)
	, m_dwAttributeFilterAny( __EXT_MFC_SFGAO_DEFAULT_ATTRIBUTE_FILTER & (~SFGAO_HIDDEN) )
	, m_dwAttributeFilterAllPresent( 0 /*SFGAO_FILESYSANCESTOR*/ )
	, m_dwAttributeFilterAllAbsent( 0 )
{
	m_bEnableAutoFilter = true;
}

CExtShellComboBox::~CExtShellComboBox()
{
	m_pidlFolder.Empty();
	m_pidlDelayedFocus.Empty();
	m_arrLinear.RemoveAll();
	m_node.Empty();
}

HWND CExtShellComboBox::Shell_GetHWND()
{
	return GetSafeHwnd();
}

void CExtShellComboBox::UpdateContent()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	ResetContent();
	if( m_pidlFolder.IsEmpty() )
		return;
	m_node.BuildContent(
		m_hWnd,
		m_bIncludeHiddenFolders,
		m_dwAttributeFilterAny,
		m_dwAttributeFilterAllPresent,
		m_dwAttributeFilterAllAbsent
		);
CExtPIDL pidlMyComputer;
	VERIFY( pidlMyComputer.GetSpecialFolderLocation( m_hWnd, CSIDL_DRIVES ) );
CExtShellContentNode * pNode = m_node.Find( pidlMyComputer.GetPtr() );
	if( pNode != NULL )
		pNode->ExpandChildren(
			m_hWnd,
			m_bIncludeHiddenFolders,
			m_dwAttributeFilterAny,
			m_dwAttributeFilterAllPresent,
			m_dwAttributeFilterAllAbsent
			);
	m_node.ExpandPIDL( m_pidlFolder );
	m_node.FillLinearArray( m_arrLinear );
bool bDisplayFullPathIfAvailable =
		( m_bDisplayFullPathIfAvailable && ( GetStyle() & 0x0003L ) != CBS_DROPDOWNLIST )
			? true : false;
INT nIndex, nCount = INT( m_arrLinear.GetSize() ), nCurSel = -1;
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtShellContentNode * pNode = m_arrLinear[ nIndex ];
		ASSERT( pNode != NULL );
		CExtPIDL pidlAbsolute = pNode->GetAbsPIDL();
		CExtSafeString strText;
		if( bDisplayFullPathIfAvailable )
			strText = pidlAbsolute.GetPath();
		if( strText.IsEmpty() )
			strText = pidlAbsolute.GetDisplayNameOf();
		AddString( LPCTSTR( strText ) );
		if( nCurSel < 0 )
		{
			CExtPIDL pidlAbsolute = pNode->GetAbsPIDL();
			if( pidlAbsolute == m_pidlFolder )
				nCurSel = nIndex;
		}
	}
	if( nCurSel >= 0 && nCurSel != GetCurSel() )
		SetCurSel( nCurSel );
}

void CExtShellComboBox::OnFocusedItemChanged()
{
	ASSERT_VALID( this );
	GetParent()->SendMessage( CExtShellComboBox::g_nMsgShellLocationChanged, WPARAM(this) );
}

void CExtShellComboBox::DelayFocusPIDL(
	LPCITEMIDLIST pidlAbsolute // = NULL
	)
{
	ASSERT_VALID( this );
	if(		m_pidlDelayedFocus == pidlAbsolute
		||	GetSafeHwnd() == NULL
		)
		return;
	for( MSG _msg; ::PeekMessage( &_msg, m_hWnd, CExtShellBase::g_nMsgDelayFocusPIDL, CExtShellBase::g_nMsgDelayFocusPIDL, PM_NOREMOVE ) ; )
		::PeekMessage( &_msg, m_hWnd, CExtShellBase::g_nMsgDelayFocusPIDL, CExtShellBase::g_nMsgDelayFocusPIDL, PM_REMOVE );
	m_pidlDelayedFocus = pidlAbsolute;
	PostMessage( CExtShellBase::g_nMsgDelayFocusPIDL );
}

bool CExtShellComboBox::FocusPIDL(
	LPCITEMIDLIST pidlAbsolute // = NULL // absolute folder's PIDL
	)
{
	ASSERT_VALID( this );
	if( pidlAbsolute == NULL )
	{
		pidlAbsolute = g_pidlDesktop;
		ASSERT( pidlAbsolute != NULL );
	}
	if(		( ! m_pidlFolder.IsEmpty() )
		&&	m_pidlFolder == pidlAbsolute
		)
		return true;
	m_pidlFolder = pidlAbsolute;
	if( GetSafeHwnd() == NULL )
		return true;
bool bDisplayFullPathIfAvailable =
		( m_bDisplayFullPathIfAvailable && ( GetStyle() & 0x0003L ) != CBS_DROPDOWNLIST )
			? true : false;
CExtSafeString strText;
	if( bDisplayFullPathIfAvailable )
		strText = m_pidlFolder.GetPath();
	if( strText.IsEmpty() )
	{
		strText = m_pidlFolder.GetDisplayNameOf();
		bDisplayFullPathIfAvailable = false;
	}
	UpdateContent();
	if( bDisplayFullPathIfAvailable )
		SetWindowText( LPCTSTR(strText) );
	OnFocusedItemChanged();
	return true;
}

bool CExtShellComboBox::FocusPath(
	__EXT_MFC_SAFE_LPCTSTR strPath
	)
{
	ASSERT_VALID( this );
	if( LPCTSTR(strPath) == NULL || _tcslen( LPCTSTR(strPath) ) == 0 )
		return FocusPIDL( (LPITEMIDLIST)NULL );
LPITEMIDLIST pidlAbsolute = stat_ILFromPath( strPath, m_hWnd );
	if( pidlAbsolute == NULL )
		return false;
bool bRetVal = FocusPIDL ( pidlAbsolute );
	ASSERT( g_pMalloc != NULL );
	g_pMalloc->Free( pidlAbsolute );
	return bRetVal;
}

LPITEMIDLIST CExtShellComboBox::GetCurrentFolderPIDL()
{
	ASSERT_VALID( this );
	return m_pidlFolder.GetPtr(); // absolute folder's PIDL
}

LPCITEMIDLIST CExtShellComboBox::GetCurrentFolderPIDL() const
{
	ASSERT_VALID( this );
	return m_pidlFolder.GetPtr(); // absolute folder's PIDL
}

BEGIN_MESSAGE_MAP( CExtShellComboBox, CExtComboBox )
	//{{AFX_MSG_MAP(CExtShellComboBox)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CExtShellComboBox::PreSubclassWindow()
{
	VERIFY( Shell_Init() );
	m_arrLinear.RemoveAll();
	m_node.Empty();
	CExtComboBox::PreSubclassWindow();
	if( m_bHelperEnableShellAutoComplete )
	{
		CEdit * pEdit = GetInnerEditCtrl();
		if( pEdit->GetSafeHwnd() != NULL )
			CExtShellBase::stat_SHAutoComplete( pEdit->m_hWnd, m_dwHelperFlagsSHACF );
	}
}

void CExtShellComboBox::PostNcDestroy()
{
	m_pidlFolder.Empty();
	m_pidlDelayedFocus.Empty();
	m_arrLinear.RemoveAll();
	m_node.Empty();
	Shell_Done();
	CExtComboBox::PostNcDestroy();
}

void CExtShellComboBox::MeasureItem( LPMEASUREITEMSTRUCT pMIS )
{
	ASSERT_VALID( this );
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
	pMIS->itemHeight = pPM->UiScalingDo( m_nListBoxItemHeightPxAt96DPI, CExtPaintManager::__EUIST_Y );
}

void CExtShellComboBox::DrawItem( LPDRAWITEMSTRUCT pDIS )
{
	ASSERT_VALID( this );
INT nIndex = INT( pDIS->itemID ), nCount = INT( m_arrLinear.GetSize() );
	if( nIndex < 0 || nIndex >= nCount )
		return;

CDC dc;
	dc.Attach( pDIS->hDC );
CRect rcItem = pDIS->rcItem;
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
CFont * pFont = &( pPM->m_FontNormal );
CFont * pOldFont = dc.SelectObject( pFont );

INT nIndentPxOneLevel = pPM->UiScalingDo( m_nListBoxIndentPxAt96DPI, CExtPaintManager::__EUIST_Y );
CExtShellContentNode * pNode = m_arrLinear.GetAt( nIndex );
	ASSERT( pNode != NULL );
CExtSafeString strText = pNode->GetDisplayNameOf();
CSize _sizeText = CExtPaintManager::stat_CalcTextDimension( dc, *pFont, strText, (m_nDrawTextFlags&(DT_NOPREFIX))|DT_CALCRECT ).Size();

CRect rcText = rcItem, rcIcon( 0, 0, 0, 0 );
	rcText.DeflateRect( 3, 0 );
	if( ( pDIS->itemState & ODS_COMBOBOXEDIT ) == 0 )
	{
		INT nIndentLevel = pNode->GetIndent();
		INT nIndentPx = nIndentPxOneLevel * nIndentLevel;
		rcText.left += nIndentPx;
	}

CExtBitmap _bmpIcon;
	if( pNode->GetIcon( _bmpIcon ) )
	{
		ASSERT( ! _bmpIcon.IsEmpty() );
		CSize _sizeIcon = _bmpIcon.GetSize();
		ASSERT( _sizeIcon.cx > 0  && _sizeIcon.cy > 0 );
		rcIcon = rcText;
		rcIcon.right = rcIcon.left + _sizeIcon.cx;
		if( rcIcon.Height() != _sizeIcon.cy )
		{
			rcIcon.top += ( rcIcon.Height() - _sizeIcon.cy ) / 2;
			rcIcon.bottom = rcIcon.top + _sizeIcon.cy;
		}
		rcText.left = rcIcon.right + m_nIconToTextMargin;
	}

	if( ! m_bFullRowSelection )
	{
		rcText.right = rcText.left + _sizeText.cx + 6;
		rcText.right = min( rcText.right, rcItem.right );
	}
CRect rcItemBackGround = m_bFullRowSelection ? rcItem : rcText;
	rcText.DeflateRect( 3, 1 );

bool bSelection = ( ( pDIS->itemState & ( ODS_SELECTED | ODS_FOCUS ) ) != 0 ) ? true : false;
COLORREF clrItemBackground = COLORREF(0L), clrItemText = COLORREF(0L), clrWindow = PmBridge_GetPM()->GetSysColor( COLOR_WINDOW );
	dc.FillSolidRect( &rcItem, clrWindow );
	if( bSelection )
	{
		clrItemBackground = PmBridge_GetPM()->GetSysColor( COLOR_HIGHLIGHT );
		clrItemText = PmBridge_GetPM()->GetSysColor( COLOR_HIGHLIGHTTEXT );
	}
	else
	{
		clrItemBackground = clrWindow;
		clrItemText = PmBridge_GetPM()->GetSysColor( COLOR_BTNTEXT );
	}
	dc.FillSolidRect( &rcItemBackGround, clrItemBackground );
INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
COLORREF clrOldTextColor = dc.SetTextColor( clrItemText );
	CExtRichContentLayout::stat_DrawText( dc.m_hDC, LPCTSTR(strText), strText.GetLength(), &rcText, m_nDrawTextFlags, 0 );
	dc.SetTextColor( clrOldTextColor );
	dc.SetBkMode( nOldBkMode );
	if( ! rcIcon.IsRectEmpty() )
	{
		ASSERT( ! _bmpIcon.IsEmpty() );
		_bmpIcon.AlphaBlend( dc.m_hDC, rcIcon );
	}
	if( bSelection)
		dc.DrawFocusRect( rcItemBackGround );
	dc.SelectObject( pOldFont );
	dc.Detach();
}

BOOL CExtShellComboBox::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult) 
{
	ASSERT_VALID( this );
BOOL bRetVal = CExtComboBox::OnChildNotify( message, wParam, lParam, pLResult );
	if( message == WM_COMMAND )
	{
		INT nCode = HIWORD(wParam);
		switch( nCode )
		{
		case CBN_SELENDOK:
		{
			INT nCurSel = INT(GetCurSel());
			if( nCurSel >= 0 )
			{
#ifdef _DEBUG
				INT nCount = GetCount();
				ASSERT( nCount > 0 );
				ASSERT( nCurSel < nCount );
				ASSERT( INT(m_arrLinear.GetSize()) == nCount );
#endif // _DEBUG
				CExtShellContentNode * pNode = m_arrLinear[ nCurSel ];
				ASSERT( pNode != NULL );
				CExtPIDL pidlAbsolute = pNode->GetAbsPIDL();
				DelayFocusPIDL( pidlAbsolute.GetPtr() );
			} // if( nCurSel >= 0 )
		}
		break;
		} // switch( nCode )
	} // if( message == WM_COMMAND )
	return bRetVal;
}

LRESULT CExtShellComboBox::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
//	switch( message )
//	{
//	default:
		if( message == CExtShellBase::g_nMsgDelayFocusPIDL )
		{
			if( ! m_pidlDelayedFocus.IsEmpty() )
				FocusPIDL( m_pidlDelayedFocus.GetPtr() );
			return 0L;
		}
//	break;
//	} // switch( message )
LRESULT lResult = CExtComboBox::WindowProc( message, wParam, lParam );
	return lResult;
}

bool CExtShellComboBox::OnHookSpyPreTranslateMessage(
	MSG * pMSG
	)
{
	switch( pMSG->message )
	{
	case WM_KEYDOWN:
		if(		GetSafeHwnd() != NULL
			&&	( GetStyle() & WS_VISIBLE ) != 0
			)
		{
			switch( pMSG->wParam )
			{
			case VK_RETURN:
			{
				CEdit * pEdit = GetInnerEditCtrl();
				if( pEdit->GetSafeHwnd() != NULL )
				{
					HWND hWndFocus = ::GetFocus();
					if( hWndFocus != NULL && hWndFocus == pEdit->GetSafeHwnd() )
					{
						SendMessage( WM_CANCELMODE );
						CString strTextTest;
						GetWindowText( strTextTest );
						CExtSafeString strTextCorrect = LPCTSTR(strTextTest);
						strTextCorrect.TrimLeft( _T(" \r\t\n") );
						strTextCorrect.TrimRight( _T(" \r\t\n") );
						INT nNewSel = INT( FindStringExact( -1, LPCTSTR(strTextCorrect) ) );
						if( nNewSel >= 0 )
						{
							CExtShellContentNode * pNode = m_arrLinear[ nNewSel ];
							ASSERT( pNode != NULL );
							CExtPIDL pidlAbsolute = pNode->GetAbsPIDL();
							DelayFocusPIDL( pidlAbsolute );
						}
						else
						{
							if( FocusPath( LPCTSTR( strTextCorrect ) ) )
							{
								if( strTextCorrect != LPCTSTR(strTextTest) )
									SetWindowText( LPCTSTR(strTextCorrect) );
							}
						}
					}
				}
			}
			break;
			} // switch( pMSG->wParam )
		}
	break;
	} // switch( pMSG->message )
	return CExtComboBox::OnHookSpyPreTranslateMessage( pMSG );
}

#endif //  ( ! defined __EXT_MFC_NO_SHELL_COMBO_BOX )

#if ( ! defined __EXT_MFC_NO_SHELL_EXTENSIONS_COMBO_BOX )

/////////////////////////////////////////////////////////////////////////////
// CExtShellExtensionsComboBox

IMPLEMENT_DYNCREATE( CExtShellExtensionsComboBox, CExtComboBox );

CExtShellExtensionsComboBox::CExtShellExtensionsComboBox()
{
}

CExtShellExtensionsComboBox::~CExtShellExtensionsComboBox()
{
	_ClearExtensionsInfo();
}

void CExtShellExtensionsComboBox::_ClearExtensionsInfo()
{
POSITION pos = m_mapExtensions.GetStartPosition();
	for( ; pos != NULL; )
	{
		CExtSafeString s;
		CExtSafeStringList * pStringList = NULL;
		m_mapExtensions.GetNextAssoc( pos, s, (void*&) pStringList);
		if( pStringList != NULL )
			delete pStringList;
	}
	m_mapExtensions.RemoveAll();
	m_listCurrentExtensions.RemoveAll();
	m_listTitleFilter.RemoveAll();
}


BEGIN_MESSAGE_MAP( CExtShellExtensionsComboBox, CExtComboBox )
	//{{AFX_MSG_MAP(CExtShellExtensionsComboBox)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CExtShellExtensionsComboBox::PreSubclassWindow()
{
#if ( _MFC_VER == 0x700 )
		CExtComboBox
#else
		CExtNCSB < CExtComboBox >
#endif
				:: PreSubclassWindow();
	if( m_listTitleFilter.GetCount() > 0 )
	{
		POSITION pos = m_listTitleFilter.GetHeadPosition();
		for( ; pos != NULL; )
			AddString( m_listTitleFilter.GetNext( pos ) );

		SetCurSel( 0 );
	}
}

bool CExtShellExtensionsComboBox::GetExtensionsByTitle(
	__EXT_MFC_SAFE_LPCTSTR strTitle, // if NULL or empty - returns currently selected extensions
	CExtSafeStringList & listOfExtensions
	)
{
CString _strTitle;
	if(		LPCTSTR(strTitle) == NULL
		||	_tcslen( LPCTSTR(strTitle) ) == 0
		)
		GetWindowText( _strTitle );
	else
		_strTitle = LPCTSTR(strTitle);
	if( _strTitle.IsEmpty() )
		return false;

	listOfExtensions.RemoveAll();
CExtSafeStringList * pStringList = NULL;
	if( ! m_mapExtensions.Lookup( LPCTSTR(_strTitle), (void *&) pStringList ) )
		return false;
	ASSERT( pStringList != NULL );
	listOfExtensions.AddTail( pStringList );
	return true;
}

void CExtShellExtensionsComboBox::SetFilter( __EXT_MFC_SAFE_LPCTSTR strFilter )
{
	ASSERT_VALID( this );
int _nFilterLen = (int) _tcslen( strFilter );
	if( strFilter == NULL || _nFilterLen == 0 )
		return;
CString s = strFilter;
	s.TrimLeft( _T(" \r\n\t") );
	s.TrimRight( _T(" \r\n\t") );
	_nFilterLen = s.GetLength();
	if( _nFilterLen == 0 )
		return;
	_ClearExtensionsInfo();

int _nCountDelimiter = 0;
CString _strTitleFilter = _T("");
CString _strMaskFilter = _T("");
TCHAR _strCurrentSymbol;
CExtSafeStringList _listFilterExtensions;
bool _bTitle = true;
	for( int i = 0; i < _nFilterLen; i++ )
	{
		_strCurrentSymbol = s.GetAt(i);

		if( _strCurrentSymbol == _T(' ') && (! _bTitle) )
			continue;

		if( _strCurrentSymbol == _T('|') )
		{
			_nCountDelimiter ++;
			if( _nCountDelimiter == 2 )
				break; // end of specification on ||

			if( ! _bTitle )
			{
				_listFilterExtensions.AddTail( __EXT_MFC_SAFE_LPCTSTR(LPCTSTR(_strMaskFilter)) );
				AddFilter( LPCTSTR(_strTitleFilter), _listFilterExtensions );
				_strMaskFilter = _T("");
				_strTitleFilter = _T("");
				_listFilterExtensions.RemoveAll();
			}

			_bTitle = ! _bTitle;
			continue;
		}
		else
			_nCountDelimiter = 0;

		if( _strCurrentSymbol == _T(';') && ! _bTitle )
		{
			_listFilterExtensions.AddTail( __EXT_MFC_SAFE_LPCTSTR(LPCTSTR(_strMaskFilter)) );
			_strMaskFilter = _T("");
			continue;
		}

		if( _bTitle )
			_strTitleFilter += _strCurrentSymbol;
		else
			_strMaskFilter += _strCurrentSymbol;
	}
}

INT CExtShellExtensionsComboBox::FindFilterIndexByExtension( __EXT_MFC_SAFE_LPCTSTR strExtension ) const
{
	ASSERT_VALID( this );
	if( LPCTSTR(strExtension) == NULL || _tcslen(LPCTSTR(strExtension)) == 0 )
		return -1;
CExtSafeString strSearchExtension = strExtension;
	strSearchExtension.TrimLeft( _T(" \r\n\t\"") );
	strSearchExtension.TrimRight( _T(" \r\n\t\"") );
INT nLen = INT( strSearchExtension.GetLength() );
	if( nLen == 0 )
		return -1;
	if( nLen >= 2 )
	{
		TCHAR tchr0 = strSearchExtension[0];
		TCHAR tchr1 = strSearchExtension[1];
		if( tchr0 != _T('*') )
		{
			if( tchr1 != _T('.') )
				strSearchExtension.Insert( 0, _T("*.") );
			else
				strSearchExtension.Insert( 0, _T("*") );
		}
	}
INT nFilterIndex = 0;
POSITION pos = m_listTitleFilter.GetHeadPosition();
	for( ; pos != NULL; nFilterIndex ++ )
	{
		const CExtSafeString & _strFilterTitle = m_listCurrentExtensions.GetNext( pos );
		CExtSafeStringList * pStringList = NULL;
		if( ! m_mapExtensions.Lookup( LPCTSTR(_strFilterTitle), (void *&) pStringList ) )
			continue;
		ASSERT( pStringList != NULL );
		if( pStringList->Find( strSearchExtension ) == NULL )
			continue;
		return nFilterIndex;
	}
	return -1;
}

CExtSafeString CExtShellExtensionsComboBox::GetFilterHash() const
{
	ASSERT_VALID( this );
USES_CONVERSION;
LPBYTE pConvertedBuffer = NULL;
INT nSizeConvertedBuffer = 0;
CExtIntegrityCheckSum _chk;
POSITION pos = m_mapExtensions.GetStartPosition();
	for( ; pos != NULL; )
	{
		CExtSafeString s;
		CExtSafeStringList * pStringList = NULL;
		m_mapExtensions.GetNextAssoc( pos, s, (void*&) pStringList);
		pConvertedBuffer = LPBYTE( LPVOID( T2CW( LPCTSTR( s ) ) ) );
		nSizeConvertedBuffer = s.GetLength() * sizeof(WCHAR);
		_chk.Update( pConvertedBuffer, nSizeConvertedBuffer );
		if( pStringList != NULL )
		{
			POSITION posList = pStringList->GetHeadPosition();
			for( ; posList != NULL; )
			{
				const CExtSafeString & s2 = pStringList->GetNext( posList );
				pConvertedBuffer = LPBYTE( LPVOID( T2CW( LPCTSTR( s2 ) ) ) );
				nSizeConvertedBuffer = s2.GetLength() * sizeof(WCHAR);
				_chk.Update( pConvertedBuffer, nSizeConvertedBuffer );
			} // for( ; posList != NULL; )
		}
	} // for( ; pos != NULL; )
CExtSafeString str = _chk.Final();
	return str;
}

void CExtShellExtensionsComboBox::AddFilter(
	__EXT_MFC_SAFE_LPCTSTR strFilterTitle,
	const CExtSafeStringList & listFilterExtensions
	)
{
	ASSERT_VALID( this );
	if(		LPCTSTR(strFilterTitle) == NULL
		||	_tcslen( LPCTSTR(strFilterTitle) ) == 0
		||	listFilterExtensions.GetCount() == 0
		)
		return;
	m_listTitleFilter.AddTail( strFilterTitle );
CExtSafeStringList * pStringList = new CExtSafeStringList;
	pStringList->AddTail( (CExtSafeStringList *)&listFilterExtensions );
	m_mapExtensions.SetAt( strFilterTitle, (void*&)pStringList );

// 	POSITION pos = pStringList->GetHeadPosition();
// 	CExtSafeString s;
// 	for( ; pos != NULL; )
// 		s = pStringList->GetNext( pos );
}

LRESULT CExtShellExtensionsComboBox::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
LRESULT lResult =
#if ( _MFC_VER == 0x700 )
		CExtComboBox
#else
		CExtNCSB < CExtComboBox >
#endif
					:: WindowProc( message, wParam, lParam );
	return lResult;
}

BOOL CExtShellExtensionsComboBox::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult) 
{
	ASSERT_VALID( this );
	if( message == WM_COMMAND )
	{
		INT nCode = HIWORD(wParam);
		switch( nCode )
		{
        case CBN_SELENDOK:
		{	
			int _indexItem = GetCurSel();
			if( _indexItem < 0 )
				break;
			m_listCurrentExtensions.RemoveAll();
			CString _strFilterTitle;
			GetLBText( _indexItem, _strFilterTitle );
			CExtSafeStringList * pStringList = NULL;
			if( m_mapExtensions.Lookup( LPCTSTR(_strFilterTitle), (void *&) pStringList ) )
			{
				ASSERT( pStringList != NULL );
				m_listCurrentExtensions.AddTail( pStringList );
			}
		}
		break;
		} // switch( nCode )
	} // if( message == WM_COMMAND )
	return 
#if ( _MFC_VER == 0x700 )
		CExtComboBox
#else
		CExtNCSB < CExtComboBox >
#endif
						:: OnChildNotify(message, wParam, lParam, pLResult);
}

#endif //  ( ! defined __EXT_MFC_NO_SHELL_EXTENSIONS_COMBO_BOX )

#if ( ! defined __EXT_MFC_NO_SHELL_LIST_VIEW_CTRL )

/////////////////////////////////////////////////////////////////////////////
// CExtShellListCtrl

const UINT CExtShellListCtrl::g_nMsgShellLocationChanged = ::RegisterWindowMessage( _T("CExtShellListCtrl::g_nMsgShellLocationChanged") );

CExtShellListCtrl::CExtShellListCtrl()
	: m_nNameColumnWidth( 150 )
	, m_uFlags( 0xFFFFFFFF )
	, m_bFoldersBeforeOthers( true )
	, m_bShowParentFolderItem( false )
	, m_bShowRootFolderItem( false )
	, m_bIncludeFiles( true )
	, m_bShowExtensions( true )
	, m_bWatchFileSystem( true )
//	, m_bDragEnabled( true )
//	, m_bDropEnabled( true )
	, m_bContentUpdating( false )
	, m_dwAttributeFilterAny( SFGAO_FILESYSANCESTOR|SFGAO_FILESYSTEM ) // ( __EXT_MFC_SFGAO_DEFAULT_ATTRIBUTE_FILTER & (~SFGAO_HIDDEN) )
	, m_dwAttributeFilterAllPresent( 0 )
	, m_dwAttributeFilterAllAbsent( 0 )
	, m_bDateTimeFieldIncludeDate( true )
	, m_bDateTimeFieldIncludeTime( true )
	, m_nDateTimeFieldFormat( 0 )    // 0-default, 12-with AM/PM, 24-...
	, m_bDateTimeFieldIncludeFieldYear( true )
	, m_bDateTimeFieldIncludeFieldMonth( true )
	, m_bDateTimeFieldIncludeFieldDay( true )
	, m_bDateTimeFieldIncludeFieldHour( true )
	, m_bDateTimeFieldIncludeFieldMinute( true )
	, m_bDateTimeFieldIncludeFieldSecond( false )
	, m_bDateTimeFieldIncludeFieldDesignator( true )
	, m_bHelperWinExplorerLikeFixOfMyComputerAndMyNetworkPlaces( true )
	, m_bRunItems( false )
	, m_bShowShellContextMenus( true )
	, m_bHelperShellCtxMenuTracking( false )
{
	m_arrColumnTypes.Add( INT(__ESLCT_NAME) );
	m_arrColumnTypes.Add( INT(__ESLCT_SIZE) );
	m_arrColumnTypes.Add( INT(__ESLCT_TYPE) );
	m_arrColumnTypes.Add( INT(__ESLCT_DATE_MODIFIED) );
	m_arrColumnTypes.Add( INT(__ESLCT_DATE_CREATED) );
	m_arrColumnTypes.Add( INT(__ESLCT_DATE_ACCESSED) );
	m_arrColumnTypes.Add( INT(__ESLCT_ATTRIBUTES) );
}

CExtShellListCtrl::~CExtShellListCtrl()
{
	WatchDirChanges_Stop(); // to avoid pure virtual function calls from parallel thread
	m_SF.Empty();
	m_pidlFolder.Empty();
}

HWND CExtShellListCtrl::Shell_GetHWND()
{
	return GetSafeHwnd();
}

void CExtShellListCtrl::PreSubclassWindow()
{
	m_bContentUpdating = false;
	//VERIFY( Shell_Init() );
	CExtListCtrl::PreSubclassWindow();
	m_bHelperShellCtxMenuTracking = false;
	ModifyStyle( 0, LVS_SHAREIMAGELISTS );
	ASSERT( CExtShellBase::g_ilShellSmall.GetSafeHandle() != NULL );
	ASSERT( CExtShellBase::g_ilShellBig.GetSafeHandle() != NULL );
	SetImageList( &CExtShellBase::g_ilShellSmall, LVSIL_SMALL );
	SetImageList( &CExtShellBase::g_ilShellBig, LVSIL_NORMAL );
	OnShellListInitializeColumns();
}

void CExtShellListCtrl::PostNcDestroy()
{
	m_bHelperShellCtxMenuTracking = false;
	m_bContentUpdating = false;
	m_SF.Empty();
	m_pidlFolder.Empty();
	//Shell_Done();
	CExtListCtrl::PostNcDestroy();
}

BEGIN_MESSAGE_MAP( CExtShellListCtrl, CExtListCtrl )
	//{{AFX_MSG_MAP(CExtShellListCtrl)
	ON_NOTIFY_REFLECT( LVN_ENDLABELEDIT, OnLvnEndLabelEdit )
	ON_NOTIFY_REFLECT( LVN_DELETEITEM, OnDeleteItem )
	ON_NOTIFY_REFLECT( LVN_DELETEALLITEMS, OnDeleteAllItems )
	ON_NOTIFY_REFLECT( LVN_BEGINLABELEDIT, OnBeginLabelEdit )
	//}}AFX_MSG_MAP
	ON_WM_CONTEXTMENU()
	ON_COMMAND_RANGE( IDM_FIRST_SHELLMENUID, IDM_LAST_SHELLMENUID, OnShellCommand )
	ON_REGISTERED_MESSAGE( CExtShellBase::g_nMsgUpdateItemContent, OnUpdateItemContent )
END_MESSAGE_MAP()

void CExtShellListCtrl::OnLvnEndLabelEdit( NMHDR * pNMHDR, LRESULT * pResult )
{
NMLVDISPINFO * pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
	ASSERT( pDispInfo != NULL );
	if( pDispInfo->item.pszText == NULL )
		return;
	(*pResult) = 0L;
CExtSafeString _strNewText = pDispInfo->item.pszText;
CExtSafeString _strOldText = GetItemText( pDispInfo->item.iItem, NULL );
	_strNewText.TrimLeft( _T(" ") );
	_strNewText.TrimRight( _T(" ") );
	if( _strNewText == _strOldText )
		return;
	(*pResult) = 1L;

CExtShellItemData * pShellItemData = (CExtShellItemData *)pDispInfo->item.lParam;
LPITEMIDLIST pidl = pShellItemData->m_pidlAbsolute.GetPtr();
LPITEMIDLIST pidlResultRelative = NULL;
IShellFolder * pParentFolder = NULL;
LPITEMIDLIST pidlParent = NULL;
	CExtShellBase::stat_SHBindToParent( pidl, IID_IShellFolder, (void**)&pParentFolder, (LPCITEMIDLIST*)&pidlParent );
USES_CONVERSION;
LPCWSTR pwstr = T2CW(_strNewText);
HRESULT hr =
		pParentFolder->SetNameOf(
			NULL,
			pidlParent,
			pwstr,
			0,
			&pidlResultRelative
			);
	pParentFolder->Release();
	if( hr != S_OK )
	{
		(*pResult) = 0L;
		return;
	}
	ASSERT( CExtShellBase::g_pMalloc != NULL );
	pShellItemData->m_pidlAbsolute.Combine( m_pidlFolder.GetPtr(), pidlResultRelative );
	if( pidlResultRelative != pidlResultRelative && LPVOID(pidl) != LPVOID(g_pidlDesktop) )
		CExtShellBase::g_pMalloc->Free( pidlResultRelative );
	SetItemText( pDispInfo->item.iItem, NULL, _strNewText );
	SortItems();
}

void CExtShellListCtrl::DelayFocusPIDL(
	LPCITEMIDLIST pidlAbsolute // = NULL
	)
{
	ASSERT_VALID( this );
	if(		m_pidlDelayedFocus == pidlAbsolute
		||	GetSafeHwnd() == NULL
		)
		return;
	for( MSG _msg; ::PeekMessage( &_msg, m_hWnd, CExtShellBase::g_nMsgDelayFocusPIDL, CExtShellBase::g_nMsgDelayFocusPIDL, PM_NOREMOVE ) ; )
		::PeekMessage( &_msg, m_hWnd, CExtShellBase::g_nMsgDelayFocusPIDL, CExtShellBase::g_nMsgDelayFocusPIDL, PM_REMOVE );
	m_pidlDelayedFocus = pidlAbsolute;
	PostMessage( CExtShellBase::g_nMsgDelayFocusPIDL );
}

bool CExtShellListCtrl::FocusPIDL(
	LPCITEMIDLIST pidlAbsolute // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT( g_pDesktopFolder != NULL );
	if( pidlAbsolute == NULL )
	{
		ASSERT( CExtShellBase::g_pidlDesktop != NULL );
		return FocusPIDL( CExtShellBase::g_pidlDesktop );
	}
	if( (!m_pidlFolder.IsEmpty()) && m_pidlFolder == pidlAbsolute )
		return true;
	return UpdateContent( (LPITEMIDLIST)pidlAbsolute, true ) ? true : false;
}

bool CExtShellListCtrl::FocusPath(
	__EXT_MFC_SAFE_LPCTSTR strPath
	)
{
	ASSERT_VALID( this );
	ASSERT( g_pDesktopFolder != NULL );
	ASSERT( m_pRootFolder != NULL );
	if( LPCTSTR(strPath) == NULL || _tcslen( LPCTSTR(strPath) ) == 0 )
		return FocusPIDL( (LPITEMIDLIST)NULL );
	m_SF.Empty();
CExtShellItemData lpTVID;
CExtPIDL	pidl;
OLECHAR		szOleChar[MAX_PATH];
ULONG		chEaten;
ULONG		dwAttributes;
HRESULT		hr;
#if !defined( _UNICODE )
	::MultiByteToWideChar( CP_ACP, MB_PRECOMPOSED, LPCTSTR(strPath), -1, szOleChar, MAX_PATH );
#else
	__EXT_MFC_STRCPY( szOleChar, MAX_PATH, LPCTSTR(strPath) );
#endif
	hr = g_pDesktopFolder->ParseDisplayName( NULL, NULL, szOleChar, &chEaten, pidl.GetPtrPtr(), &dwAttributes );

	if( pidl.IsEmpty() )
		return false;

	return UpdateContent( pidl.GetPtr(), true ) ? true : false;
}

bool CExtShellListCtrl::UpdateListViewContent(
	CExtShellItemData * lptvid,
	IShellFolder * pShellFolder
	)
{
	ASSERT_VALID( this );
	lptvid;
	SetRedraw( FALSE );
bool bRetVal = false;
LPENUMIDLIST lpe = NULL;
	if( ! ( SUCCEEDED( pShellFolder->EnumObjects( m_hWnd, SHCONTF_FOLDERS, &lpe ) ) ) )
		return false;
LPITEMIDLIST lpi = NULL;
CExtPIDL pidlParent;
ULONG ulFetched = 0;
	lpe->Next( 1, &lpi, &ulFetched );
	pidlParent.Attach( (ITEMIDLIST*)stat_ILCloneParent( lpi ) );
	bRetVal = UpdateContent( pidlParent.GetPtr(), true ) ? true : false;
	SetRedraw( TRUE );
	return bRetVal;
}

void CExtShellListCtrl::_InsertParentFolderItem( LPITEMIDLIST pidl, bool bRoot )
{
	ASSERT_VALID( this );
CExtPIDL pidlTmp;
bool bRootFolderState = ShowRootFolderItemGet();
	if( ! bRoot )
		pidlTmp.Attach( (ITEMIDLIST*)stat_GetParentItem( pidl ) );
	else
		pidlTmp.Combine( pidl, NULL );
	if( pidlTmp.IsEmpty() && ( ! bRoot ) )
		return;
CExtShellItemData * pShellItemData = new CExtShellItemData;
	m_mapItemData.SetAt( pShellItemData, false );
CExtSafeString strName = _T( ".." );
	if( bRoot )
		strName = _T( "\\" );
	pShellItemData->m_pidlAbsolute = pidlTmp.GetPtr();
	if( bRoot )
		pShellItemData->m_eSIT	= CExtShellItemData::__ESIT_JUMP_TO_ROOT;
	else
		pShellItemData->m_eSIT	= CExtShellItemData::__ESIT_JUMP_ONE_LEVEL_UP;
LV_ITEM _lvi;
	_lvi.mask			= LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
	if( bRootFolderState && ! bRoot )
		_lvi.iItem = 1;
	else
		_lvi.iItem = 0;
	_lvi.iSubItem   = 0;
	_lvi.pszText    = (LPTSTR)(LPCTSTR)strName;
	_lvi.cchTextMax = MAX_PATH;
// 	if( ! bRoot )
// 	{
// 		_lvi.state = LVIS_SELECTED | LVIS_FOCUSED;
// 		_lvi.mask |= LVIF_STATE;
// 	}
SHFILEINFO _sfi;
	::memset( &_sfi, 0, sizeof(SHFILEINFO) );
UINT  uFlags  = SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_SMALLICON;
	if( bRoot )
		::SHGetFileInfo( (TCHAR*)pidl, 0, &_sfi, sizeof(SHFILEINFO), uFlags );
	else
		::SHGetFileInfo( (TCHAR*)pShellItemData->m_pidlAbsolute.GetPtr(), 0, &_sfi, sizeof(SHFILEINFO), uFlags );
	_lvi.iImage = _sfi.iIcon;
	_lvi.lParam = (LPARAM)pShellItemData;
	INT nIndex = InsertItem( &_lvi );
	if( nIndex < 0 )
	{
		ULONG ulAttrs = SFGAO_DISPLAYATTRMASK | SFGAO_REMOVABLE;
		_SetAttributes( nIndex, ulAttrs );
	}
}

void CExtShellListCtrl::_InsertRootFolderItem()
{
	ASSERT_VALID( this );
	_InsertParentFolderItem( g_pidlDesktop, true );
}

void CExtShellListCtrl::_SetAttributes( int iItem, DWORD dwAttributes )
{
	ASSERT_VALID( this );
	_ItemStateFromShellFlags( iItem, dwAttributes, LVIS_CUT );
}

CExtSafeString CExtShellListCtrl::OnShellListQueryColumnTypeText(
	CExtShellListCtrl::e_shell_list_column_type_t eSLCT
	)
{
	ASSERT_VALID( this );
UINT nResourceID = 0;
	switch( INT(eSLCT) )
	{
	case INT(__ESLCT_NAME):
		nResourceID = IDS_EXT_SHELL_LIST_VIEW_COLUMN_NAME;
	break;
	case INT(__ESLCT_SIZE):
		nResourceID = IDS_EXT_SHELL_LIST_VIEW_COLUMN_SIZE;
	break;
	case INT(__ESLCT_TYPE):
		nResourceID = IDS_EXT_SHELL_LIST_VIEW_COLUMN_TYPE;
	break;
	case INT(__ESLCT_DATE_MODIFIED):
		nResourceID = IDS_EXT_SHELL_LIST_VIEW_COLUMN_DATE_MODIFIED;
	break;
	case INT(__ESLCT_DATE_CREATED):
		nResourceID = IDS_EXT_SHELL_LIST_VIEW_COLUMN_DATE_CREATED;
	break;
	case INT(__ESLCT_DATE_ACCESSED):
		nResourceID = IDS_EXT_SHELL_LIST_VIEW_COLUMN_DATE_ACCESSED;
	break;
	case INT(__ESLCT_ATTRIBUTES):
		nResourceID = IDS_EXT_SHELL_LIST_VIEW_COLUMN_ATTRIBUTES;
	break;
	} // switch( INT(eSLCT) )
CExtSafeString str;
	if(		nResourceID != 0
		&&	g_ResourceManager->LoadString( str, nResourceID )
		&&	( ! str.IsEmpty() )
		)
		return str;
	switch( INT(eSLCT) )
	{
	case INT(__ESLCT_NAME):
		str = _T("Name");
	break;
	case INT(__ESLCT_SIZE):
		str = _T("Size");
	break;
	case INT(__ESLCT_TYPE):
		str = _T("Type");
	break;
	case INT(__ESLCT_DATE_MODIFIED):
		str = _T("Date Modified");
	break;
	case INT(__ESLCT_DATE_CREATED):
		str = _T("Date Created");
	break;
	case INT(__ESLCT_DATE_ACCESSED):
		str = _T("Date Accessed");
	break;
	case INT(__ESLCT_ATTRIBUTES):
		str = _T("Attributes");
	break;
	default:
		str = _T("---");
	break;
	} // switch( INT(eSLCT) )
	return str;
}

void CExtShellListCtrl::OnShellListInitializeColumns()
{
	ASSERT_VALID( this );
INT nIndex, nCount = INT(m_arrColumnTypes.GetSize());
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		e_shell_list_column_type_t eSLCT = (e_shell_list_column_type_t)m_arrColumnTypes[ nIndex ];
		CExtSafeString str = OnShellListQueryColumnTypeText( eSLCT );
		InsertColumn( nIndex, str, LVCFMT_LEFT, 120, nIndex );
	}
}

void CExtShellListCtrl::OnShellListInitializeItem(
	INT nIndexItem,
	CExtShellItemData * pShellItemData,
	__EXT_MFC_SAFE_LPCTSTR strPath,
	CFileFind & _ff
	)
{
	ASSERT_VALID( this );
	ASSERT( nIndexItem >= 0 );
	ASSERT( nIndexItem < GetItemCount() );
	ASSERT( pShellItemData != NULL );
	ASSERT( strPath == NULL || _tcslen(strPath) > 0 );
CExtHeaderCtrl & wndHeader = GetHeaderCtrl();
INT nIndexSubItem, nSubItemCount = INT( wndHeader.GetItemCount() );
	for( nIndexSubItem = 0; nIndexSubItem < nSubItemCount; nIndexSubItem ++ )
		OnShellListInitializeSubItem(
			nIndexItem,
			nIndexSubItem,
			pShellItemData,
			strPath,
			_ff
			);
}

void CExtShellListCtrl::OnShellListInitializeSubItem(
	INT nIndexItem,
	INT nIndexSubItem,
	CExtShellItemData * pShellItemData,
	__EXT_MFC_SAFE_LPCTSTR strPath,
	CFileFind & _ff
	)
{
	ASSERT_VALID( this );
	ASSERT( pShellItemData != NULL );
	ASSERT( strPath == NULL || _tcslen(strPath) > 0 );
CExtSafeString strSubItemText = _T("");
e_shell_list_column_type_t eSLCT = (e_shell_list_column_type_t) m_arrColumnTypes[ nIndexSubItem ];
	switch( INT(eSLCT) )
	{
	case INT(__ESLCT_NAME):
	break;
	case INT(__ESLCT_SIZE):
		if( strPath != NULL && ( pShellItemData->m_dwCachedAttributes & SFGAO_FOLDER ) == 0 )
			strSubItemText.Format(_T("%ldKB"), (_ff.GetLength()+1024)/1024);
	break;
	case INT(__ESLCT_TYPE):
		if( strPath != NULL )
		{
			SHFILEINFO _sfi;
			::memset( &_sfi, 0, sizeof(SHFILEINFO) );
			::SHGetFileInfo( (TCHAR*)pShellItemData->m_pidlAbsolute.GetPtr(), 0, &_sfi, sizeof(SHFILEINFO), SHGFI_PIDL|SHGFI_TYPENAME );
			strSubItemText = LPCTSTR(_sfi.szTypeName);
		}
	break;
	case INT(__ESLCT_DATE_MODIFIED):
		if( strPath != NULL )
		{
			CTime _timeTmp;
			COleDateTime _oleDateTimeTmp;
			_ff.GetLastWriteTime( _timeTmp );
			_oleDateTimeTmp = _timeTmp.GetTime();
			strSubItemText =
				OnShellListGetDateTimeFieldText(
					_oleDateTimeTmp,
					m_bDateTimeFieldIncludeDate,
					m_bDateTimeFieldIncludeTime,
					m_nDateTimeFieldFormat,
					m_bDateTimeFieldIncludeFieldYear,
					m_bDateTimeFieldIncludeFieldMonth,
					m_bDateTimeFieldIncludeFieldDay,
					m_bDateTimeFieldIncludeFieldHour,
					m_bDateTimeFieldIncludeFieldMinute,
					m_bDateTimeFieldIncludeFieldSecond,
					m_bDateTimeFieldIncludeFieldDesignator
					);
		}
	break;
	case INT(__ESLCT_DATE_CREATED):
		if( strPath != NULL )
		{
			CTime _timeTmp;
			COleDateTime _oleDateTimeTmp;
			_ff.GetCreationTime( _timeTmp );
			_oleDateTimeTmp = _timeTmp.GetTime();
			strSubItemText =
				OnShellListGetDateTimeFieldText(
					_oleDateTimeTmp,
					m_bDateTimeFieldIncludeDate,
					m_bDateTimeFieldIncludeTime,
					m_nDateTimeFieldFormat,
					m_bDateTimeFieldIncludeFieldYear,
					m_bDateTimeFieldIncludeFieldMonth,
					m_bDateTimeFieldIncludeFieldDay,
					m_bDateTimeFieldIncludeFieldHour,
					m_bDateTimeFieldIncludeFieldMinute,
					m_bDateTimeFieldIncludeFieldSecond,
					m_bDateTimeFieldIncludeFieldDesignator
					);
		}
	break;
	case INT(__ESLCT_DATE_ACCESSED):
		if( strPath != NULL )
		{
			CTime _timeTmp;
			COleDateTime _oleDateTimeTmp;
			_ff.GetLastAccessTime( _timeTmp );
			_oleDateTimeTmp = _timeTmp.GetTime();
			strSubItemText =
				OnShellListGetDateTimeFieldText(
					_oleDateTimeTmp,
					m_bDateTimeFieldIncludeDate,
					m_bDateTimeFieldIncludeTime,
					m_nDateTimeFieldFormat,
					m_bDateTimeFieldIncludeFieldYear,
					m_bDateTimeFieldIncludeFieldMonth,
					m_bDateTimeFieldIncludeFieldDay,
					m_bDateTimeFieldIncludeFieldHour,
					m_bDateTimeFieldIncludeFieldMinute,
					m_bDateTimeFieldIncludeFieldSecond,
					m_bDateTimeFieldIncludeFieldDesignator
					);
		}
	break;
	case INT(__ESLCT_ATTRIBUTES):
		if( strPath != NULL )
		{
			if( _ff.IsArchived() )
				strSubItemText += _T("A");
			if( _ff.IsCompressed() )
				strSubItemText += _T("C");
			if( _ff.IsHidden() )
				strSubItemText += _T("H");
			if( _ff.IsReadOnly() )
				strSubItemText += _T("R");
			if( _ff.IsSystem() )
				strSubItemText += _T("S");
		}
	break;
	default:
	break;
	} // switch( INT(eSLCT) )
	if( ! strSubItemText.IsEmpty() )
		SetItemText( nIndexItem, nIndexSubItem, LPCTSTR(strSubItemText) );
}

void CExtShellListCtrl::OnDeleteItem( NMHDR*  pNMHDR, LRESULT * pResult ) 
{
NM_LISTVIEW * pNMListView = (NM_LISTVIEW *)pNMHDR;
CExtShellItemData * pShellItemData = (CExtShellItemData *)pNMListView->lParam;
	if( pShellItemData != NULL )
	{
		pNMListView->lParam = NULL;
		if( m_mapItemData.RemoveKey( pShellItemData ) )
			delete pShellItemData;
		SetItemData( pNMListView->iItem, NULL );
	}
	CExtListCtrl::OnDeleteItem( pNMHDR,  pResult );
}

void CExtShellListCtrl::OnDeleteAllItems( NMHDR * pNMHDR, LRESULT * pResult ) 
{
NM_LISTVIEW * pNMListView = (NM_LISTVIEW *)pNMHDR;
POSITION pos = m_mapItemData.GetStartPosition();
	for( ; pos != NULL ;)
	{
		CExtShellItemData * pShellItemData = NULL;
		bool bTmp = false;
		m_mapItemData.GetNextAssoc( pos, pShellItemData, bTmp );
//		CString str = pShellItemData->m_pidlAbsolute.GetPath();
		if( pShellItemData != NULL )
			delete pShellItemData;
		SetItemData( pNMListView->iItem, NULL );
	}
	m_mapItemData.RemoveAll();
	CExtListCtrl::OnDeleteAllItems( pNMHDR, pResult );
}

void CExtShellListCtrl::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	if( pWnd->GetSafeHwnd() != m_hWnd )
	{
		Default();
		return;
	}

	if( m_bHelperShellCtxMenuTracking )
		return;
	if( ! ShowShellContextMenusGet() )
		return;

UINT nFillMenuFlags = 0;
CExtCIP_SF _SF;
LPITEMIDLIST pidl = NULL;
UINT nFlags = 0;

	ScreenToClient( &point );

CList < INT, INT > _listSelectedItems;
	GetSelectedItemsList( _listSelectedItems );

INT nItemCount = INT( _listSelectedItems.GetCount() );

INT nItem = HitTest( point, &nFlags );

	if( nItemCount == 1 || nItemCount == 0 )
	{
		if(		( nFlags & LVHT_ONITEM ) != 0
			&&	nItem >= 0
			)
		{ // if item context menu
			CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( nItem );
 			pidl = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetLast();
			CString strItem = pShellItemData->m_pidlAbsolute.GetPath();

			if( g_pDesktopFolder->CompareIDs( 0, pidl, g_pidlDesktop ) == 0 )
				_SF =  g_pDesktopFolder;
			else if( strItem == _T("..") )
			{
				pidl = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetPtr();
				LPITEMIDLIST pidlUpFolder = NULL;
				pidlUpFolder = stat_GetParentItem( pidl );
				IShellFolder * pShellUpFolder = NULL;
				g_pDesktopFolder->BindToObject( pidlUpFolder, NULL, IID_IShellFolder, (LPVOID*)&pShellUpFolder );
				if( pShellUpFolder == NULL )
					_SF = g_pDesktopFolder;
				else
					_SF.Attach( pShellUpFolder );
			}
			else
				_SF = m_SF;
 			nFillMenuFlags = CMF_EXPLORE|CMF_CANRENAME|CMF_NODEFAULT;
		} // if item context menu
		else
		{ // if container context menu]
			pidl = m_pidlFolder.GetPtr();
			LPITEMIDLIST pidlParent = stat_GetParentItem( (LPITEMIDLIST)m_pidlFolder.GetPtr() );
			TCHAR szItemPath[_MAX_PATH];
			::SHGetPathFromIDList( pidlParent, szItemPath );
 			g_pDesktopFolder->BindToObject( pidlParent, NULL, IID_IShellFolder, (void**)_SF.GetPtrPtr() );
			if( _SF.IsEmpty() )
				_SF = g_pDesktopFolder;
			nFillMenuFlags = CMF_EXPLORE|CMF_CANRENAME|CMF_VERBSONLY|CMF_NODEFAULT;
		} // if container context menu
		
		ASSERT( ! _SF.IsEmpty() );
		ASSERT( pidl != NULL );
	}

	if( CExtPopupMenuWnd::IsKeyPressed( VK_SHIFT ) )
		nFillMenuFlags |= __EXT_MFC_CMF_EXTENDEDVERBS;

	m_myShellContextMenu.SetOwner( this );
bool bPopup = ( GetAsyncKeyState(VK_CONTROL) & 0x8000 ) ? true : false;
CMenu _menu;
	if( ! _menu.CreatePopupMenu() )
		return;

	if( nItemCount == 1 || nItemCount == 0 )
	{
		ASSERT( ! _SF.IsEmpty() );
		if( ! m_myShellContextMenu.Create( _SF.GetPtr(), pidl ) )
			return;
	}
	else
	{
		if( ! _GetContextMenu( _listSelectedItems, m_myShellContextMenu ) )
			return;
	}
	if( ! m_myShellContextMenu.FillMenu( &_menu, 0, IDM_FIRST_SHELLMENUID, IDM_LAST_SHELLMENUID, nFillMenuFlags ) )
	{
		if( bPopup )
			_menu.AppendMenu(MF_GRAYED, __EXT_MFC_SHELL_MENU_RESOURCE_ID, CExtShellBase::g_strTextOfMenuItemInEmptyShellContextMenu );
	}
	ClientToScreen( &point );
	if( ( GetStyle() & LVS_EDITLABELS ) == 0 )
		_menu.RemoveMenu( IDM_SHELL_RENAME_COMMAND_ID, MF_BYCOMMAND );
	m_bHelperShellCtxMenuTracking = true;
UINT nRetCmd = (UINT)_menu.TrackPopupMenu( TPM_LEFTALIGN|TPM_RIGHTBUTTON|TPM_RETURNCMD, point.x, point.y, this );
	if( IDM_FIRST_SHELLMENUID <= nRetCmd && nRetCmd <= IDM_LAST_SHELLMENUID )
		SendMessage( WM_COMMAND, WPARAM(nRetCmd) );
	m_bHelperShellCtxMenuTracking = false;
	m_myShellContextMenu.SetOwner( NULL );
}

bool CExtShellListCtrl::_SpecificItem_CheckStatus( CList < INT, INT > & _listSelectedItems )
{
	ASSERT_VALID( this );

INT nItemIndex, nItemCount = INT( _listSelectedItems.GetCount() );
	if( nItemCount == 0 )
		return false;

POSITION pos = _listSelectedItems.GetHeadPosition();
int nObjectIndex = -1;

// CExtPIDL pidlMyComputer, pidlMyNetworkPlaces, pidlNetworkConnections;
// 	VERIFY( pidlMyComputer.GetSpecialFolderLocation( m_hWnd, CSIDL_DRIVES ) );
// 	VERIFY( pidlMyNetworkPlaces.GetSpecialFolderLocation( m_hWnd, CSIDL_NETWORK ) );
// 	VERIFY( pidlNetworkConnections.GetSpecialFolderLocation( m_hWnd, __EXT_MFC_CSIDL_CONNECTIONS ) );

	for( nItemIndex = 0; nItemIndex < nItemCount; nItemIndex++ )
	{
		ASSERT( pos != NULL );
		nObjectIndex = _listSelectedItems.GetNext( pos );
		if( _SpecificItem_CheckStatus( nObjectIndex ) )
			return true;
// 		CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( nObjectIndex );
// 		LPITEMIDLIST pidlFolder = pShellItemData->m_pidlAbsolute.GetPtr();
// 		if(		pidlMyComputer == pidlFolder
// 			||	pidlMyNetworkPlaces == pidlFolder
// 			||	pidlNetworkConnections == pidlFolder
// 			)
// 			return true;
// 
// 		ASSERT( nObjectIndex >=0 );
// 		CString str = GetItemText( nObjectIndex, 0 );
// 		ASSERT( ! str.IsEmpty() );
// 		if( str == _T("\\") || str == _T("..") )
// 			return true;
	}

	return false;
}

bool CExtShellListCtrl::_SpecificItem_CheckStatus( int _indexItem )
{
	ASSERT_VALID( this );
	if( _indexItem < 0 || _indexItem >= GetItemCount() )
		return false;

CExtPIDL pidlMyComputer, pidlMyNetworkPlaces, pidlNetworkConnections, pidlRecycleBin, pidlControlPanel;
	VERIFY( pidlMyComputer.GetSpecialFolderLocation( m_hWnd, CSIDL_DRIVES ) );
	VERIFY( pidlMyNetworkPlaces.GetSpecialFolderLocation( m_hWnd, CSIDL_NETWORK ) );
	VERIFY( pidlNetworkConnections.GetSpecialFolderLocation( m_hWnd, __EXT_MFC_CSIDL_CONNECTIONS ) );
	VERIFY( pidlRecycleBin.GetSpecialFolderLocation( m_hWnd, CSIDL_BITBUCKET ) );
	VERIFY( pidlControlPanel.GetSpecialFolderLocation( m_hWnd, CSIDL_CONTROLS ) );

	CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( _indexItem );
		LPITEMIDLIST pidlFolder = pShellItemData->m_pidlAbsolute.GetPtr();
		CExtPIDL pidlFolderParent = stat_GetParentItem( pidlFolder );

		if(		pidlMyComputer == pidlFolder
			||	pidlMyNetworkPlaces == pidlFolder
			||	pidlNetworkConnections == pidlFolder
			||	pidlRecycleBin == pidlFolder
			||	pidlControlPanel == pidlFolder
			||	( pidlFolderParent != NULL && pidlFolderParent == pidlMyComputer )
			)
			return true;

		ASSERT( _indexItem >=0 );
		CString str = GetItemText( _indexItem, 0 );
		ASSERT( ! str.IsEmpty() );
		if( str == _T("\\") || str == _T("..") )
			return true;

	return false;
}

int CExtShellListCtrl::_SpecificItems_GetCount( CList < INT, INT > & _listSelectedItems )
{
	ASSERT_VALID( this );
	if( _listSelectedItems.GetCount() == 0 )
		return 0;

	int _countSpecificItems = 0;
	int _objectIndex = -1;
	POSITION pos = _listSelectedItems.GetHeadPosition();

	for( ; pos != NULL; )
	{
		_objectIndex = _listSelectedItems.GetNext( pos );
		if( _SpecificItem_CheckStatus( _objectIndex ) )
			_countSpecificItems ++;
	}

	return _countSpecificItems;
}

void CExtShellListCtrl::_DeleteSpecifiedItems( CList < INT, INT > & _listSelectedItems )
{
	ASSERT_VALID( this );
	if( _listSelectedItems.GetCount() == 0 )
		return;

POSITION pos = _listSelectedItems.GetTailPosition();
int nObjectIndex = -1;

CExtPIDL pidlMyComputer, pidlMyNetworkPlaces, pidlNetworkConnections;
	VERIFY( pidlMyComputer.GetSpecialFolderLocation( m_hWnd, CSIDL_DRIVES ) );
	VERIFY( pidlMyNetworkPlaces.GetSpecialFolderLocation( m_hWnd, CSIDL_NETWORK ) );
	VERIFY( pidlNetworkConnections.GetSpecialFolderLocation( m_hWnd, __EXT_MFC_CSIDL_CONNECTIONS ) );

	for( ; pos != NULL; )
	{
		ASSERT( pos != NULL );
		POSITION currentPos = pos;
		nObjectIndex = _listSelectedItems.GetPrev( pos );
		CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( nObjectIndex );
		LPITEMIDLIST pidlFolder = pShellItemData->m_pidlAbsolute.GetPtr();
		if(		pidlMyComputer == pidlFolder
			||	pidlMyNetworkPlaces == pidlFolder
			||	pidlNetworkConnections == pidlFolder
			)
		{
			_listSelectedItems.RemoveAt( currentPos );
			continue;
		}
			

		ASSERT( nObjectIndex >=0 );
		CString str = GetItemText( nObjectIndex, 0 );
		ASSERT( ! str.IsEmpty() );
		if( str == _T("\\") || str == _T("..") )
		{
			_listSelectedItems.RemoveAt( currentPos );
			continue;
		}
	}
}

bool CExtShellListCtrl::_GetContextMenu( INT nItemIndex, CExtSCM & shellContextMenu )
{
	ASSERT_VALID( this );
CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( nItemIndex );
	ASSERT( pShellItemData != NULL );
CExtCIP_SF _SF;
	 _SF.Attach( pShellItemData->m_pidlAbsolute.GetParentFolder() );
	ASSERT( ! _SF.IsEmpty() );
LPITEMIDLIST pidl = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetLast();
	return shellContextMenu.Create( _SF.GetPtr(), pidl );
}

bool CExtShellListCtrl::_GetContextMenu( CList < INT, INT > & _listSelectedItems, CExtSCM & shellContextMenu )
{
	ASSERT_VALID( this );
INT nItemIndex, nItemCount = INT( _listSelectedItems.GetCount() );
	if( nItemCount == 0 )
		return false;

	if( nItemCount > 1 && _SpecificItem_CheckStatus( _listSelectedItems ) )
		return false;

	// first, ensure all items have one parent item
POSITION pos = _listSelectedItems.GetHeadPosition();
	ASSERT( pos != NULL );
INT nObjectIndex = _listSelectedItems.GetNext( pos );
	ASSERT( nObjectIndex >= 0 );
CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( nObjectIndex );
	ASSERT( pShellItemData != NULL );
CExtCIP_SF _SF;
	 _SF.Attach( pShellItemData->m_pidlAbsolute.GetParentFolder() );
	ASSERT( ! _SF.IsEmpty() );
	// prepare data
CArray < LPITEMIDLIST, LPITEMIDLIST > _arrPidlRelative;
	_arrPidlRelative.SetSize( nItemCount );
	pos = _listSelectedItems.GetHeadPosition();
	for( nItemIndex = 0; nItemIndex < nItemCount; nItemIndex++ )
	{
		ASSERT( pos != NULL );
		nObjectIndex = _listSelectedItems.GetNext( pos );
		ASSERT( nObjectIndex >=0 );
		pShellItemData = (CExtShellItemData*)GetItemData( nObjectIndex );
		ASSERT( pShellItemData != NULL );
		LPITEMIDLIST pidl = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetLast();
		ASSERT( pidl != NULL );
		_arrPidlRelative.SetAt( nItemIndex, pidl );
	}
	return shellContextMenu.Create( _SF.GetPtr(), (LPCITEMIDLIST *)_arrPidlRelative.GetData(), INT(_arrPidlRelative.GetSize()) );
}

void CExtShellListCtrl::OnShellCommand( UINT nID ) 
{
	if( nID == IDM_SHELL_RENAME_COMMAND_ID )
	{
		INT nItem = FocusedItemGet();
		if( nItem >= 0 )
			SendMessage( LVM_EDITLABEL, WPARAM( nItem ), 0 );
		return;
	}

	m_myShellContextMenu.InvokeCommand( nID );
}

bool CExtShellListCtrl::_OnQueryWatchFS()
{
	ASSERT_VALID( this );
	return m_pidlFolder._OnQueryWatchFS();
}

void CExtShellListCtrl::_EnterFolderItem( LPITEMIDLIST pidl )
{
	ASSERT( g_pDesktopFolder != NULL );
	ASSERT( m_pRootFolder != NULL );
	ASSERT( g_pidlDesktop != NULL );
int n_fItem = FocusedItemGet();
LVITEM * lvItem;
	lvItem = (LVITEM *)GetItemData( n_fItem );
	if( lvItem != NULL )
	{
		m_pidlPrevSel = m_pidlFolder;
		UpdateContent( pidl, true );
		_UpdateWalkingFocusAndSelection();
	}
}

void CExtShellListCtrl::_UpdateWalkingFocusAndSelection()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
INT nOldFocusedItemIndex = FocusedItemGet();
	if( nOldFocusedItemIndex == 0 && ShowRootFolderItemGet() )
	{
		SetItemState( nOldFocusedItemIndex, 0, LVIS_FOCUSED|LVIS_SELECTED );
		nOldFocusedItemIndex = GetNextItem( nOldFocusedItemIndex, LVNI_FOCUSED );
	}
INT nNewFocusedItemIndex = FindItemByPIDL( m_pidlPrevSel.GetPtr(), false, ShowRootFolderItemGet() ? 0 : -1 );
	if( nNewFocusedItemIndex < 0 && GetItemCount() > 0 )
		nNewFocusedItemIndex = 0;
	if(		nNewFocusedItemIndex >= 0
		&&	(	nNewFocusedItemIndex != nOldFocusedItemIndex
			||	GetItemState( nNewFocusedItemIndex, LVIS_FOCUSED|LVIS_SELECTED )
					!= GetItemState( nOldFocusedItemIndex, LVIS_FOCUSED|LVIS_SELECTED )
			)
		)
		SetItemState( nNewFocusedItemIndex, LVIS_FOCUSED|LVIS_SELECTED, LVIS_FOCUSED|LVIS_SELECTED );
INT nItemIndex = GetNextItem( -1, LVIS_SELECTED );
	for( ; nItemIndex >= 0; nItemIndex = GetNextItem( nItemIndex, LVIS_SELECTED ) )
	{
		if( nItemIndex == nNewFocusedItemIndex )
			continue;
		SetItemState( nItemIndex, 0, LVIS_SELECTED );
	}
}

void CExtShellListCtrl::_ItemStateFromShellFlags(
	INT nItemIndex,
	DWORD dwAttributesBits,
	DWORD dwAttributesMask
	)
{
	ASSERT_VALID( this );
	if( ( dwAttributesBits & SFGAO_GHOSTED ) != 0 )
		SetItemState( nItemIndex, dwAttributesMask, dwAttributesMask );
	else
		SetItemState( nItemIndex, 0, dwAttributesMask );
UINT nImage = UINT(-1);
	if( ( dwAttributesBits & SFGAO_LINK ) != 0 )
		nImage = INDEXTOOVERLAYMASK(2);
	else if( ( dwAttributesBits & SFGAO_SHARE ) != 0 )
		nImage = INDEXTOOVERLAYMASK(1);
	else
		nImage = 0;
	SetItemState( nItemIndex, nImage, LVIS_OVERLAYMASK ); // TVIS_OVERLAYMASK in the same case in tree
}

bool CExtShellListCtrl::ShellItemExecute(
	INT nItemIndex, // -1 - use all selected items
	UINT nMessage
	)
{
	ASSERT_VALID( this );

	if( CExtShellBase::stat_NotifyShellItemExecute( m_hWnd, LPARAM(nItemIndex), nMessage ) )
		return true;

	if( nItemIndex < 0 )
	{
		CList < INT, INT > _listSelectedItems;
		GetSelectedItemsList( _listSelectedItems );
		INT nCountSelected = INT( _listSelectedItems.GetCount() );
		if( nCountSelected == 0 )
			return false;
		POSITION pos = _listSelectedItems.GetHeadPosition();
		for( ; pos != NULL; )
		{
			nItemIndex = _listSelectedItems.GetNext( pos );
			if( nCountSelected > 1 )
			{
				CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( nItemIndex );
				ASSERT( pShellItemData != NULL );
				LPITEMIDLIST pidl = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetLast();
				ULONG uAttributes = SFGAO_FOLDER | SFGAO_REMOVABLE | SFGAO_FILESYSTEM;
				CString strItem = pShellItemData->m_pidlAbsolute.GetPath();
				m_SF->GetAttributesOf( 1, (LPCITEMIDLIST*)&pidl, &uAttributes );
				if( ( uAttributes & SFGAO_FOLDER ) != 0 )
					continue;
			}
			ShellItemExecute( nItemIndex, nMessage );
		}
		return true;
	}
	if( nItemIndex >= GetItemCount() )
		return false;
	if( CExtShellBase::stat_NotifyShellItemExecute( m_hWnd, LPARAM(nItemIndex), nMessage ) )
		return true;
CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( nItemIndex );
	ASSERT( pShellItemData != NULL );
LPITEMIDLIST pidl = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetLast();
ULONG uAttributes = SFGAO_FOLDER | SFGAO_REMOVABLE | SFGAO_FILESYSTEM;
CString strItem = pShellItemData->m_pidlAbsolute.GetPath();
	m_SF->GetAttributesOf( 1, (LPCITEMIDLIST*)&pidl, &uAttributes );
	if(		(uAttributes & SFGAO_FOLDER) != 0
		||	pShellItemData->m_eSIT == CExtShellItemData::__ESIT_JUMP_ONE_LEVEL_UP
		||	pShellItemData->m_eSIT == CExtShellItemData::__ESIT_JUMP_TO_ROOT
		)
	{
		CExtPIDL pidlEnter = pShellItemData->m_pidlAbsolute;
		_EnterFolderItem( pidlEnter.GetPtr() );
		return true;
	}

 	if( ! m_bRunItems )
 		return false;

	///////////////////

UINT nFillMenuFlags = CMF_EXPLORE|CMF_CANRENAME;
CExtCIP_SF _SF;
	if( g_pDesktopFolder->CompareIDs( 0, pidl, g_pidlDesktop ) == 0 )
		_SF = g_pDesktopFolder;
	else if( strItem == _T("..") )
	{
		pidl = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetPtr();
		LPITEMIDLIST pidlUpFolder = NULL;
		pidlUpFolder = stat_GetParentItem( pidl );
		IShellFolder * pShellUpFolder = NULL;
		g_pDesktopFolder->BindToObject( pidlUpFolder, NULL, IID_IShellFolder, (LPVOID*)&pShellUpFolder );
		if( pShellUpFolder == NULL )
			_SF = g_pDesktopFolder;
		else
			_SF.Attach( pShellUpFolder );
	}
	else
		_SF = m_SF;
	ASSERT( ! _SF.IsEmpty() );
CMenu _menu;
UINT nCommandID = UINT(-1);
	if(		_menu.CreatePopupMenu()
		&&	m_myShellContextMenu.Create( _SF.GetPtr(), pidl )
		&&	m_myShellContextMenu.FillMenu( &_menu, 0, IDM_FIRST_SHELLMENUID, IDM_LAST_SHELLMENUID, nFillMenuFlags )
		&&	( nCommandID = _menu.GetDefaultItem( 0, FALSE ) ) != UINT(-1)
		&&	m_myShellContextMenu.InvokeCommand( nCommandID )
		)
		return true;
	///////////////////
CString strNameBuffer;
STRRET strRetData;
	::memset( &strRetData, 0, sizeof(STRRET) );
	strRetData.uType = STRRET_WSTR;
DWORD _shgdnf = __EXT_MFC_SHGDN_FORPARSING|__EXT_MFC_SHGDN_INFOLDER; //__EXT_MFC_SHGDN_INCLUDE_NONFILESYS; //|__EXT_MFC_SHGDN_FORPARSING;
	m_SF->GetDisplayNameOf( pidl, _shgdnf, &strRetData );
	switch( strRetData.uType )
	{
	case STRRET_WSTR:
	{
		USES_CONVERSION;
		strNameBuffer = W2CT(strRetData.pOleStr);
	}
	break;
	case STRRET_CSTR:
	{
		USES_CONVERSION;
		strNameBuffer = A2CT(strRetData.cStr);
		strRetData.pOleStr = NULL;
	}
	break;
	case STRRET_OFFSET:
	{
		USES_CONVERSION;
		strNameBuffer = A2CT(((char*)((LPBYTE)pidl+strRetData.uOffset)));
		strRetData.pOleStr = NULL;
	}
	break;
	}
	if( strRetData.pOleStr != NULL )
		::CoTaskMemFree( strRetData.pOleStr );
INT nBufferLen = (INT)strNameBuffer.GetLength();
	if(		(		nBufferLen > 4
				&&	strNameBuffer.GetAt(0) == _T(':')
				&&	strNameBuffer.GetAt(1) == _T(':')
				&&	strNameBuffer.GetAt(2) == _T('{')
				&&	strNameBuffer.GetAt(nBufferLen-1) == _T('}')
			)
			||
			(		nBufferLen > 2
				&&	strNameBuffer.GetAt(0) == _T('{')
				&&	strNameBuffer.GetAt(nBufferLen-1) == _T('}')
			)
		) // GUID is not path
	{
		::ShellExecute(
			::GetDesktopWindow(),
			g_PaintManager.m_bIsWin2000orLater ? LPCTSTR(NULL) : _T("open"),
			LPCTSTR(strNameBuffer),
			NULL,
			NULL,
			SW_SHOW
			);
		return true;
	}
LPCTSTR strInvoke = strItem.IsEmpty() ? LPCTSTR(strNameBuffer) : LPCTSTR(strItem);
	if( strInvoke != NULL && _tcslen(strInvoke) > 0 )
	{
		CExtHyperLinkButton::stat_HyperLinkOpen( strInvoke );
		return true;
	}
	return false;
}

bool CExtShellListCtrl::_FillItem(
	LVITEM & _lvi,
	LPCITEMIDLIST pidl,
	IShellFolder * pParentFolder,
	LPCITEMIDLIST pidlRelative,
	CExtSafeString * strNameBuffer // = NULL
	)
{
SHFILEINFO _sfi;
DWORD dwAttributes = SFGAO_FOLDER|SFGAO_REMOVABLE;
	//pParentFolder->GetAttributesOf( 1, &pidlRelative, &dwAttributes );

CExtPIDL _pidlTest;
_pidlTest = pidl;
dwAttributes = _pidlTest.GetAttributesOf( dwAttributes );
	
	if( ( _lvi.mask & TVIF_TEXT ) != 0 )
	{
		ASSERT( strNameBuffer != NULL );
		strNameBuffer->Empty();
		STRRET strRetData;
		::memset( &strRetData, 0, sizeof(STRRET) );
		strRetData.uType = STRRET_WSTR;
		CExtPIDL pidlRecycleBin;
		VERIFY( pidlRecycleBin.GetSpecialFolderLocation( m_hWnd, CSIDL_BITBUCKET ) );

		bool bShowExtensions =  ( pidlRecycleBin == m_pidlFolder ) ? false : ShowExtensionsGet(), pParseMode = false;
		if( bShowExtensions )
		{
			if( ( dwAttributes & SFGAO_FOLDER ) == 0 )
				pParseMode = true;
		}
		label_repeat_non_parse:
		DWORD _shgdnf = __EXT_MFC_SHGDN_INCLUDE_NONFILESYS;
		if( pParseMode )
			_shgdnf |= __EXT_MFC_SHGDN_FORPARSING;
		else
			_shgdnf |= __EXT_MFC_SHGDN_INFOLDER;
		pParentFolder->GetDisplayNameOf( pidlRelative, _shgdnf, &strRetData );
		switch( strRetData.uType )
		{
		case STRRET_WSTR:
		{
			USES_CONVERSION;
			(*strNameBuffer) = W2CT(strRetData.pOleStr);
		}
		break;
		case STRRET_CSTR:
		{
			USES_CONVERSION;
			(*strNameBuffer) = A2CT(strRetData.cStr);
			strRetData.pOleStr = NULL;
		}
		break;
		case STRRET_OFFSET:
		{
			USES_CONVERSION;
			(*strNameBuffer) = A2CT(((char*)((LPBYTE)pidlRelative+strRetData.uOffset)));
			strRetData.pOleStr = NULL;
		}
		break;
		}
		if( strRetData.pOleStr != NULL )
			::CoTaskMemFree( strRetData.pOleStr );
		if( pParseMode )
		{
			INT nBufferLen = (INT)strNameBuffer->GetLength();
			if(		(		nBufferLen > 4
						&&	strNameBuffer->GetAt(0) == _T(':')
						&&	strNameBuffer->GetAt(1) == _T(':')
						&&	strNameBuffer->GetAt(2) == _T('{')
						&&	strNameBuffer->GetAt(nBufferLen-1) == _T('}')
					)
					||
					(		nBufferLen > 2
						&&	strNameBuffer->GetAt(0) == _T('{')
						&&	strNameBuffer->GetAt(nBufferLen-1) == _T('}')
					)
				)
			{
				pParseMode = false;
				goto label_repeat_non_parse;
			}
			int nPos = strNameBuffer->ReverseFind( _T('\\') );
			if( nPos >= 0 )
				(*strNameBuffer) = strNameBuffer->Right( nBufferLen - nPos - 1 );
		}
		_lvi.pszText = (LPTSTR)(LPCTSTR)( * strNameBuffer );
	}
	if( ( _lvi.mask & ( TVIF_IMAGE | TVIF_SELECTEDIMAGE ) ) != 0 )
	{
		dwAttributes = SFGAO_FOLDER|SFGAO_LINK;
//		pParentFolder->GetAttributesOf( 1, &pidlRelative, &dwAttributes );

CExtPIDL _pidlTest;
_pidlTest = pidl;
dwAttributes = _pidlTest.GetAttributesOf( dwAttributes );
		
		UINT uFlags = SHGFI_PIDL|SHGFI_SYSICONINDEX|SHGFI_SMALLICON;
		if( ( dwAttributes & SFGAO_LINK ) != 0 )
			uFlags |= SHGFI_LINKOVERLAY;
		if( ( _lvi.mask & TVIF_IMAGE ) != 0 )
		{
			::memset( &_sfi, 0, sizeof(SHFILEINFO) );
			::SHGetFileInfo( (LPCTSTR)pidl, 0, &_sfi, sizeof(SHFILEINFO), uFlags );
			_lvi.iImage = _sfi.iIcon;
		}
		if( ( _lvi.mask & TVIF_SELECTEDIMAGE ) != 0 )
		{
			if( ( dwAttributes & SFGAO_FOLDER ) != 0 )
				uFlags |= SHGFI_OPENICON;
			::memset( &_sfi, 0, sizeof(SHFILEINFO) );
			::SHGetFileInfo( (LPCTSTR)pidl, 0, &_sfi, sizeof(SHFILEINFO), uFlags );
		}
	}
	if( ( _lvi.mask & TVIF_CHILDREN ) != 0 )
	{
		_lvi.iSubItem = 0;
		if( ( dwAttributes & SFGAO_FOLDER ) != 0 )
			_lvi.iSubItem = 1;
	}
SHDESCRIPTIONID sdi;
	::memset( &sdi, 0, sizeof(SHDESCRIPTIONID) );
	if( SUCCEEDED(
			::SHGetDataFromIDList(
				pParentFolder,
				pidlRelative,
				SHGDFIL_DESCRIPTIONID,
				&sdi,
				sizeof(SHDESCRIPTIONID)
				)
			)
		)
	{
		switch (sdi.dwDescriptionId)
		{
		case SHDID_COMPUTER_REMOVABLE:
		case SHDID_COMPUTER_DRIVE35:
		case SHDID_COMPUTER_DRIVE525:
		case SHDID_COMPUTER_NETDRIVE:
		case SHDID_COMPUTER_CDROM:
		case SHDID_NET_DOMAIN:
		case SHDID_NET_SERVER:
		case SHDID_NET_SHARE:
		case SHDID_NET_RESTOFNET:
		case SHDID_NET_OTHER:
			return false;
		}
	}
	return true;
}

bool CExtShellListCtrl::UpdateContent( 
	LPITEMIDLIST pidlParent, 
	bool bCompleteRescan,
	CExtShellItemData * pShellItemData // = NULL 
	)
{
	if( m_bContentUpdating )
		return false;
	m_bContentUpdating = true;
CExtCIP_SF _ParentFolderOfItem;
CExtPIDL pidlAbsOfItem;
CExtCIP_SF _ParentSF;
	ASSERT( g_pDesktopFolder != NULL );
	ASSERT( m_pRootFolder != NULL );
	g_pDesktopFolder->BindToObject( pidlParent, NULL, IID_IShellFolder, (LPVOID*)_ParentSF.GetPtrPtr() );
	if( _ParentSF.IsEmpty() )
		_ParentSF = m_pRootFolder;
	if( pShellItemData != NULL )
	{
		 _ParentFolderOfItem.Attach( pShellItemData->m_pidlAbsolute.GetParentFolder() );
		ASSERT( ! _ParentFolderOfItem.IsEmpty() );
		pidlAbsOfItem = pShellItemData->m_pidlAbsolute.GetPtr();
	}
	else
	{
		_ParentFolderOfItem = _ParentSF;
		pidlAbsOfItem = pidlParent;
	}
	m_SF = _ParentSF;
CExtPIDL pidlPrevFolder = m_pidlFolder;
	if( m_pidlFolder != pidlParent )
	{
		m_pidlFolder.Empty();
		m_pidlFolder = pidlParent;
	}
bool bIncludeFiles = IncludeFilesGet();
bool bFoldersBeforeOthers = FoldersBeforeOthersGet();
CExtPIDL pidlMyComputer, pidlMyNetworkPlaces, pidlNetworkConnections;
	VERIFY( pidlMyComputer.GetSpecialFolderLocation( m_hWnd, CSIDL_DRIVES ) );
	VERIFY( pidlMyNetworkPlaces.GetSpecialFolderLocation( m_hWnd, CSIDL_NETWORK ) );
	VERIFY( pidlNetworkConnections.GetSpecialFolderLocation( m_hWnd, __EXT_MFC_CSIDL_CONNECTIONS ) );
bool bSpecialFolders =
		(	pidlMyComputer == pidlParent
		||	pidlMyNetworkPlaces == pidlParent
		||	pidlNetworkConnections == pidlParent
		) ? true : false;
IEnumIDList * pEnumIDList1 = NULL;
IEnumIDList * pEnumIDList2 = NULL;
bool bHaveLists = true;
	if(	_ParentSF->EnumObjects(
			m_hWnd,
			( ( bFoldersBeforeOthers || ! bIncludeFiles ) ?  SHCONTF_FOLDERS : SHCONTF_NONFOLDERS )
			|	( ( ( m_dwAttributeFilterAny & SFGAO_HIDDEN ) != 0 ) ? SHCONTF_INCLUDEHIDDEN : 0 ), //( ShowHiddenStateGet() ? SHCONTF_INCLUDEHIDDEN : 0 ),
			&pEnumIDList1
			) == NOERROR
		)
	{
		if( bIncludeFiles )
		{
			if(	_ParentSF->EnumObjects(
					m_hWnd,
						( bFoldersBeforeOthers ? /*SHCONTF_PROF_UIS_FLATLIST |*/SHCONTF_NONFOLDERS : SHCONTF_FOLDERS )
					|	( ( ( m_dwAttributeFilterAny & SFGAO_HIDDEN ) != 0 ) ? SHCONTF_INCLUDEHIDDEN : 0 ), //( ShowHiddenStateGet() ? SHCONTF_INCLUDEHIDDEN : 0 ),
					&pEnumIDList2
					) != NOERROR
				)
				bHaveLists = false;
		}
	}
	else
		bHaveLists = false;
	if( (! bHaveLists ) || pEnumIDList1 == NULL )
	{
		if( pEnumIDList1 != NULL )
			pEnumIDList1->Release();
		if( pEnumIDList2 != NULL )
			pEnumIDList2->Release();

		m_bContentUpdating = false;
		m_pidlFolder.Empty();
		m_pidlFolder = pidlPrevFolder;
		UpdateContent( m_pidlFolder.GetPtr(), true );

		return false;
	}

	SetRedraw( FALSE );
	if( bCompleteRescan )
		DeleteAllItems();
	else
		_DeleteNotExistingItems();
int iCtr = 0;
bool bShowParentFolderItem = ShowParentFolderItemGet();
bool bShowRootFolderItem = ShowRootFolderItemGet();
	if( bCompleteRescan && bShowRootFolderItem && g_pDesktopFolder->CompareIDs( 0, pidlParent, g_pidlDesktop ) != 0 && pidlParent != NULL )
	{
		_InsertRootFolderItem();
		iCtr ++;
	}
	if( bCompleteRescan && bShowParentFolderItem && g_pDesktopFolder->CompareIDs( 0, pidlParent, g_pidlDesktop ) != 0 && pidlParent != NULL )
	{
		_InsertParentFolderItem( m_pidlFolder.GetPtr() );
		iCtr ++;
	}
	if( ! bCompleteRescan )
		iCtr = GetItemCount();
	ASSERT( pEnumIDList1 != NULL );
bool bRetVal =
		_InsertItemsFromIDLIst(
			pEnumIDList1,
			_ParentFolderOfItem,
			_ParentSF,
			iCtr,
			bSpecialFolders,
			! bCompleteRescan,
			! bFoldersBeforeOthers,
			false
			);
	if( bRetVal && pEnumIDList2 != NULL )
	{
		if( ! bCompleteRescan )
			iCtr = GetItemCount();
		bRetVal =
			_InsertItemsFromIDLIst(
				pEnumIDList2,
				_ParentFolderOfItem,
				_ParentSF,
				iCtr,
				bSpecialFolders,
				! bCompleteRescan,
				bFoldersBeforeOthers,
				false
				);
	}
	SortItems();
	SetRedraw( TRUE );
	if( pEnumIDList1 != NULL )
		pEnumIDList1->Release();
	if( pEnumIDList2 != NULL )
		pEnumIDList2->Release();
	m_bContentUpdating = false;
	if( ! bRetVal )
		return false;
	if( ! CExtShellBase::stat_ValidPIDL( m_pidlFolder.GetPtr() ) )
		return false;
	if(		WatchFileSystemGet()
		&&	_OnQueryWatchFS()
		)
		WatchDirChanges_Start( true );
	GetParent()->SendMessage( CExtShellListCtrl::g_nMsgShellLocationChanged, WPARAM(this) );
	return true;
}

void CExtShellListCtrl::_DeleteNotExistingItems()
{
INT nItemCount = GetItemCount();
	if( nItemCount <= 0 )
		return;
INT nItemIndex = 0;
bool bUpFolderState = ShowParentFolderItemGet();
bool bRootFolderState = ShowRootFolderItemGet();
	if(		( bUpFolderState && ! bRootFolderState )
		||	( ! bUpFolderState && bRootFolderState )
		)
		nItemIndex = 1;
	else if( bUpFolderState && bRootFolderState )
			nItemIndex = 2;
TCHAR szItemPath[_MAX_PATH];
	for( nItemCount --; nItemCount >= nItemIndex; nItemCount -- )
	{
		CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( nItemCount );
		LPITEMIDLIST pidlItem = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetPtr();
		::SHGetPathFromIDList( pidlItem, szItemPath );
		if( ! CExtShellBase::stat_ValidPath( szItemPath ) )
		{
			DeleteItem( nItemCount );
		}
	}
}

INT CExtShellListCtrl::FindItemByPIDL(
	LPITEMIDLIST pidl,
	bool bRelative,
	INT nStartIndex // = -1
	)
{
	ASSERT_VALID( this );
	if( pidl == NULL )
		return -1;
	if( GetSafeHwnd() == NULL )
		return -1;
INT nItemCount = GetItemCount();
	if( nItemCount <= 0 )
		return -1;
CExtPIDL pidlAbsolute;
	if( bRelative )
	{
		if( m_pidlFolder.IsEmpty() )
			return -1;
		LPITEMIDLIST pidlTmp = stat_ILCombine( m_pidlFolder.GetPtr(), pidl );
		if( pidlTmp == NULL )
			return -1;
		pidlAbsolute.Attach( (ITEMIDLIST*)pidlTmp );
	}
	else
		pidlAbsolute = pidl;
INT nItemIndex, nRetVal = -1;
	for(	nItemIndex = ( nStartIndex < 0 ) ? 0 : ( nStartIndex + 1 );
			nItemIndex < nItemCount;
			nItemIndex ++
		)
	{
		CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( nItemIndex );
		LPITEMIDLIST pidlItem = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetPtr();
		HRESULT hr = g_pDesktopFolder->CompareIDs( 0, pidlItem, pidlAbsolute.GetPtr() );
		if(  hr == 0 )
		{
			nRetVal = nItemIndex;
			break;
		}
	}
	return nRetVal;
}

INT CExtShellListCtrl::FindItemByName(
	__EXT_MFC_SAFE_LPCTSTR strName,
	INT nStartIndex, // = -1
	bool bCompareNoCase // = true
	)
{
	ASSERT_VALID( this );
	if( _tcslen( strName ) == 0 )
		return -1;
	if( GetSafeHwnd() == NULL )
		return -1;
INT nItemCount = GetItemCount();
	if( nItemCount <= 0 )
		return -1;

	INT nItemIndex, nRetVal = -1;
	for(	nItemIndex = ( nStartIndex < 0 ) ? 0 : ( nStartIndex + 1 );
			nItemIndex < nItemCount;
			nItemIndex ++
		)
	{
		CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( nItemIndex );
		// first, try display name
		CExtSafeString _strNameBuffer = pShellItemData->m_pidlAbsolute.GetDisplayNameOf();
		LPCTSTR _strName = LPCTSTR(_strNameBuffer);
		bool bEqual = false;
		if( bCompareNoCase )
			bEqual = ( ::_tcsicmp( _strName, LPCTSTR(strName) ) == 0 ) ? true : false;
		else
			bEqual = ( ::_tcscmp( _strName, LPCTSTR(strName) ) == 0 ) ? true : false;
		if( bEqual )
		{
			nRetVal = nItemIndex;
			break;
		}
		// second, try file name
		_strNameBuffer = pShellItemData->m_pidlAbsolute.GetPath();
		_strName = LPCTSTR(_strNameBuffer);
		INT nPosChr = _strNameBuffer.ReverseFind( _T('\\') );
		if( nPosChr > 0 )
			_strName += nPosChr + 1;
		bEqual = false;
		if( bCompareNoCase )
			bEqual = ( ::_tcsicmp( _strName, LPCTSTR(strName) ) == 0 ) ? true : false;
		else
			bEqual = ( ::_tcscmp( _strName, LPCTSTR(strName) ) == 0 ) ? true : false;
		if( bEqual )
		{
			nRetVal = nItemIndex;
			break;
		}
	}
	return nRetVal;
}

CExtSafeString CExtShellListCtrl::OnShellListGetDateTimeFieldText(
	const COleDateTime & _time,
	bool bIncludeDate,              // = true
	bool bIncludeTime,              // = true
	int  nTimeFormat,               // = 0    // 0-default, 12-with AM/PM, 24-...
	bool bIncludeFieldYear,         // = true // used only if bIncludeDate is !false
	bool bIncludeFieldMonth,        // = true // used only if bIncludeDate is !false
	bool bIncludeFieldDay,          // = true // used only if bIncludeDate is !false
	bool bIncludeFieldHour,         // = true // used only if bIncludeTime is !false
	bool bIncludeFieldMinute,       // = true // used only if bIncludeTime is !false
	bool bIncludeFieldSecond,       // = true // used only if bIncludeTime is !false
	bool bIncludeFieldDesignator    // = true // used only if bIncludeTime is !false
	) const
{
	ASSERT_VALID( this );
	return
		CExtShellBase::stat_GetDateTimeFieldText(
			_time,
			bIncludeDate,
			bIncludeTime,
			nTimeFormat,
			bIncludeFieldYear,
			bIncludeFieldMonth,
			bIncludeFieldDay,
			bIncludeFieldHour,
			bIncludeFieldMinute,
			bIncludeFieldSecond,
			bIncludeFieldDesignator
			);
}

bool CExtShellListCtrl::_InsertItemsFromIDLIst(
	IEnumIDList * pEnumIDList,
	CExtCIP_SF pParentFolderOfItem,
	IShellFolder * pParentFolder,
	int	& iCtr,
	bool bSpecialFolder,
	bool bCheckExisting,
	bool bSkipFolders,
	bool bSkipFiles
	)
{
	ASSERT_VALID( this );
	if( bSpecialFolder )
		bCheckExisting = true;
CExtPIDL pidlMyComputer, pidlMyNetworkPlaces; //, pidlNetworkConnections;
	VERIFY( pidlMyComputer.GetSpecialFolderLocation( m_hWnd, CSIDL_DRIVES ) );
	VERIFY( pidlMyNetworkPlaces.GetSpecialFolderLocation( m_hWnd, CSIDL_NETWORK ) );
	//VERIFY( pidlNetworkConnections.GetSpecialFolderLocation( m_hWnd, __EXT_MFC_CSIDL_CONNECTIONS ) );
LPITEMIDLIST pidlWalk = NULL;
IShellFolder * pShellFolder = (IShellFolder *)pParentFolderOfItem;
	for( ; pEnumIDList->Next( 1, &pidlWalk, NULL ) == NOERROR; )
	{
		ASSERT( pidlWalk != NULL );
		CExtPIDL pidlRelative;
		pidlRelative.Attach( (ITEMIDLIST*)pidlWalk );
		INT nIndex = ( bCheckExisting ) ? FindItemByPIDL( pidlRelative.GetPtr(), true ) : -1;
		CExtPIDL pidlAbsolute;
		pidlAbsolute.Combine( m_pidlFolder.GetPtr(), pidlWalk );
//CExtSafeString strPathValid = pidlAbsolute.GetPath();
		DWORD dwAttributes = pidlAbsolute.GetAttributesOf( __EXT_DEFAULT_ATTRIBUTES_FOR_CExtPIDL_GetAttributesOf__ );
		if(		( dwAttributes & SFGAO_FILESYSTEM ) != 0
			&&	( dwAttributes & SFGAO_FOLDER ) == 0
			&&	(! pidlAbsolute.IsRoot() )
			&&	m_listExtensions.GetCount() > 0
			&&	(! _IsFileExtensionPresentInExtensionsList( pidlAbsolute ) )
			)
			continue;

		bool bAdd = true;
		if( ! bSpecialFolder )
		{
			bAdd = pidlAbsolute.IsRoot();
			if( ! bAdd )
			{
				bAdd = ( ( dwAttributes & m_dwAttributeFilterAllPresent ) == m_dwAttributeFilterAllPresent ) ? true : false;
				if( bAdd )
				{
					bAdd = ( ( dwAttributes & m_dwAttributeFilterAllAbsent ) == 0 ) ? true : false;
					if( bAdd )
						bAdd = ( ( dwAttributes & m_dwAttributeFilterAny ) != 0 ) ? true : false;
				}
			}
			if( bAdd )
			{
				if( bSkipFolders && ( dwAttributes & SFGAO_FOLDER ) != 0 )
					//bAdd = false;
					continue;
				else if( bSkipFiles && ( dwAttributes & SFGAO_FOLDER ) == 0 )
					//bAdd = false;
					continue;
			}
		}
		if(		bAdd
			&&	( m_dwAttributeFilterAny & ( SFGAO_FILESYSANCESTOR|SFGAO_FILESYSTEM ) ) == ( SFGAO_FILESYSANCESTOR|SFGAO_FILESYSTEM )
			&&	m_bHelperWinExplorerLikeFixOfMyComputerAndMyNetworkPlaces
			)
		{
			CExtSafeString strPath = pidlAbsolute.GetPath();
			if(		(	pidlMyComputer == m_pidlFolder
					&&	strPath.IsEmpty()
					)
				||	(	pidlMyNetworkPlaces == m_pidlFolder
					//&&	( dwAttributes & SFGAO_HASSUBFOLDER) == 0
					&&	( pidlAbsolute.GetAttributesOf(SFGAO_HASSUBFOLDER) & SFGAO_HASSUBFOLDER ) == 0
					)
				)
			{
				 bAdd = false;
			}
		}
		if( ! bAdd )
		{
			if( nIndex >= 0 )
				DeleteItem( nIndex );
			continue;
		}
		CExtShellItemData * pShellItemData = NULL;
		LV_ITEM _lvi;
		::memset( &_lvi, 0, sizeof(LV_ITEM) );
		if( bCheckExisting && nIndex >= 0 )
		{
			_lvi.mask = LVIF_PARAM;
			_lvi.iItem = nIndex;
			VERIFY( GetItem( &_lvi ) );
			pShellItemData = (CExtShellItemData *)_lvi.lParam;
			ASSERT( pShellItemData != NULL );
		}
		else
		{
			nIndex = -1;
			pShellItemData = new CExtShellItemData;
			m_mapItemData.SetAt( pShellItemData, false );
			_lvi.iItem = iCtr++;
			_lvi.lParam = (LPARAM)pShellItemData;
		}
		
		pShellItemData->m_pidlAbsolute = pidlAbsolute;
		
		//pShellItemData->UpdateCachedAttributes();
		pShellItemData->m_dwCachedAttributes = dwAttributes;


		CExtSafeString strName;
		CExtShellBase::stat_ILExtractName( pShellFolder, pidlRelative.GetPtr(), SHGDN_NORMAL, strName );
		_lvi.mask       = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		_lvi.iSubItem   = 0;
		_lvi.pszText    = (LPTSTR)(LPCTSTR)strName;
		_lvi.cchTextMax = MAX_PATH;
			
		UINT  uFlags  = SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_SMALLICON;
		SHFILEINFO _sfi;
		::memset( &_sfi, 0, sizeof(SHFILEINFO) );
		::SHGetFileInfo( (TCHAR*)pidlAbsolute.GetPtr(), 0, &_sfi, sizeof(SHFILEINFO), uFlags );
		_lvi.iImage = _sfi.iIcon;
			
		CExtSafeString strNameBuffer;
		_FillItem( _lvi, pidlAbsolute, pParentFolder, pidlRelative, &strNameBuffer );
		if( nIndex < 0 )
			nIndex = InsertItem( &_lvi );
		else
			SetItem( &_lvi );
		if( nIndex < 0 )
			continue;
		_SetAttributes( nIndex, dwAttributes );
		CExtSafeString strPath = pShellItemData->m_pidlAbsolute.GetPath();
		CFileFind _ff;
		if( _ff.FindFile( LPCTSTR(strPath) ) )
			_ff.FindNextFile();
		else
			strPath.Empty();
		OnShellListInitializeItem( nIndex, pShellItemData, strPath.IsEmpty() ? LPCTSTR(NULL) : LPCTSTR(strPath), _ff );
	} // for( ; pEnumIDList->Next( 1, &pidlWalk, NULL ) == NOERROR; )

	return true;
}

bool CExtShellListCtrl::_IsFileExtensionPresentInExtensionsList( const CExtPIDL & pidlItem ) const
{
	ASSERT_VALID( this );
	if( m_listExtensions.GetCount() == 0 )
		return false;
POSITION pos = m_listExtensions.Find( _T("*.*") );
	if( pos != NULL )
		return true;
CExtSafeString _strPath = pidlItem.GetPath(), _strExt;
	__EXT_MFC_SPLITPATH(
		_strPath,
		NULL, 0,
		NULL, 0,
		NULL, 0,
		_strExt.GetBuffer( _MAX_PATH + 1 ), _MAX_PATH
		);
	_strExt.ReleaseBuffer();
	if( _strExt.IsEmpty() )
		return false;
	_strExt = _T("*") + _strExt;
	_strExt.MakeLower();
	pos = m_listExtensions.Find( _strExt );
	if( pos != NULL )
		return true;
	return false;
}

LRESULT CExtShellListCtrl::OnUpdateItemContent( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	wParam;
	lParam;
bool bDoNow = true;
	for( MSG _msg; ::PeekMessage( &_msg, m_hWnd, CExtShellBase::g_nMsgUpdateItemContent, CExtShellBase::g_nMsgUpdateItemContent, PM_NOREMOVE ); )
	{
		::PeekMessage( &_msg, m_hWnd, CExtShellBase::g_nMsgUpdateItemContent, CExtShellBase::g_nMsgUpdateItemContent, PM_REMOVE );
		bDoNow = false;
	}
	if( bDoNow )
		UpdateContent( m_pidlFolder.GetPtr(), false );
	else
		PostMessage( CExtShellBase::g_nMsgUpdateItemContent );
	return 0L;
}

LRESULT CExtShellListCtrl::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
DWORD dwHelperRestoreStyles = 0L;
	switch( message )
	{
	case LVM_EDITLABEL:
		{
			INT nItemIndex = INT(wParam);
			if( ! ( 0 <= nItemIndex && nItemIndex <= GetItemCount() ) )
				return LRESULT(NULL);
			CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( nItemIndex );
			ASSERT( pShellItemData != NULL );
			if( pShellItemData->m_eSIT != CExtShellItemData::__ESIT_NORMAL )
				return LRESULT(NULL);
		}
	break;
	case WM_KEYDOWN:
	{
		switch( wParam )
		{
		case UINT('R'):
			if( ! ( GetAsyncKeyState( VK_CONTROL ) & 0x8000 ) )
				return 0L;
		case VK_F5:
			Refresh();
			return 0L;
		break;
		case VK_RETURN:
			if(		( ! m_bHelperShellCtxMenuTracking )
				&&	( ! CExtPopupMenuWnd::IsMenuTracking() )
				)
			{
				ShellItemExecute( -1, WM_KEYDOWN );
				return 0L;
			}
		break;
		case VK_BACK:
		{
			if( g_pDesktopFolder->CompareIDs( 0, m_pidlFolder.GetPtr(), g_pidlDesktop ) == 0 )
				return 0L;
			m_pidlPrevSel = m_pidlFolder;
			LPITEMIDLIST pLastId = m_pidlFolder.GetPtr();
			USHORT temp;
			if( pLastId != NULL )
			{
				for( ; true; )
				{
					int offset = pLastId->mkid.cb;
					temp = *(USHORT*)((BYTE*)pLastId + offset);
					if(temp == 0)
					   break;
					pLastId = (LPITEMIDLIST)((BYTE*)pLastId + offset);
				}
				pLastId->mkid.cb = 0;
				if( UpdateContent( m_pidlFolder.GetPtr(), true ) )
				{
					_UpdateWalkingFocusAndSelection();
					return 0L;
				}
			}
		}
		break;
		case VK_DELETE:
		{
			_DeleteSelectedItems();
			return 0L;
		}
		break;
		} // switch( wParam )
	}
	break;
	case WM_LBUTTONDOWN:
		if(		::GetFocus() != m_hWnd
			&&	( GetStyle() & LVS_EDITLABELS ) != 0
			)
		{
			dwHelperRestoreStyles = LVS_EDITLABELS;
			ModifyStyle( LVS_EDITLABELS, 0, 0 );
		}
		if( ( GetStyle() & WS_TABSTOP ) != 0 )
			SetFocus();
	break;
	case WM_LBUTTONDBLCLK:
		{
			CPoint point;
			if( GetCursorPos( &point ) )
			{
				ScreenToClient( &point );
				UINT nFlags = 0;
				INT nItem = HitTest( point, &nFlags );
				if( nItem >= 0 )
				{
					SelectItem( nItem );
					ShellItemExecute( -1, WM_LBUTTONDBLCLK );
				}
			}
		}
		return 0L;
	break;
	default:
		if( message == CExtShellBase::g_nMsgDelayFocusPIDL )
		{
			if( ! m_pidlDelayedFocus.IsEmpty() )
				FocusPIDL( m_pidlDelayedFocus.GetPtr() );
			return 0L;
		}
	break;
	} // switch( message )
LRESULT lResult = CExtListCtrl::WindowProc( message, wParam, lParam );
	if( dwHelperRestoreStyles != 0 )
		ModifyStyle( 0L, dwHelperRestoreStyles );
	return lResult;
}

void CExtShellListCtrl::_DeleteSelectedItems()
{
	ASSERT_VALID( this );
CList < INT, INT > _listSelectedItems;
	GetSelectedItemsList( _listSelectedItems );
	if( _listSelectedItems.GetCount() == 0 )
		return;

	if( _SpecificItem_CheckStatus( _listSelectedItems ) )
		_DeleteSpecifiedItems( _listSelectedItems );
	
CExtSafeStringList _listToCopy;
INT nCountOfTCHARs = 0;
POSITION pos = _listSelectedItems.GetHeadPosition();
	for( ; pos != NULL; )
	{
		INT nItemPos = _listSelectedItems.GetNext( pos );
		CString str = GetItemText( nItemPos, 0 );
		CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( nItemPos );
		CExtSafeString strFullPath = pShellItemData->m_pidlAbsolute.GetPath();
		_listToCopy.AddTail( strFullPath );
		INT nStrLen = INT( strFullPath.GetLength() );
		nCountOfTCHARs += nStrLen + 1;
	}
	nCountOfTCHARs += 2;
TCHAR * pBuffer = new TCHAR[nCountOfTCHARs];
TCHAR * pWalk = pBuffer;
	::memset( pBuffer, 0, nCountOfTCHARs * sizeof(TCHAR) );
	pos = _listToCopy.GetHeadPosition();
	for( ; pos != NULL; )
	{
		CExtSafeString & strFullPath = _listToCopy.GetNext( pos );
		INT nStrLen = INT( strFullPath.GetLength() );
		::memcpy( pWalk, LPVOID(LPCTSTR(strFullPath)), nStrLen * sizeof(TCHAR) );
		pWalk += nStrLen + 1;
	}

// use pBuffer ....
HWND hWndOwn = GetSafeHwnd();
SHFILEOPSTRUCT shf;
	::memset( &shf, 0, sizeof( shf ) );
	shf.hwnd = hWndOwn;
	shf.wFunc = FO_DELETE;
	shf.pFrom = pBuffer;
	shf.fFlags = FOF_ALLOWUNDO|FOF_SIMPLEPROGRESS; // FOF_NOCONFIRMATION|
int nSHRetVal = ::SHFileOperation( &shf );
	nSHRetVal;
	// W4 Change This matches the array new.
	delete[] pBuffer;

	if( ! ::IsWindow( hWndOwn ) )
		return;
	if( ::GetFocus() != hWndOwn )
	{
		ActivateTopParent();
		::SetFocus( hWndOwn );
	}
	if( WatchFileSystemGet() )
		return;

	pos = _listSelectedItems.GetTailPosition(); // do not invoke this loop when WDC thread runs - optimization issue
	for( ; pos != NULL; )
	{
		INT nItem = _listSelectedItems.GetPrev( pos );
		DeleteItem( nItem );
	}
}

bool CExtShellListCtrl::ShowParentFolderItemGet() const
{
	ASSERT_VALID( this );
	return m_bShowParentFolderItem;
}

void CExtShellListCtrl::ShowParentFolderItemSet( bool bShowParentFolderItem )
{
	ASSERT_VALID( this );
	m_bShowParentFolderItem = bShowParentFolderItem;

	if( ! m_pidlFolder.IsEmpty() )
		UpdateContent( m_pidlFolder.GetPtr(), true );
}

void CExtShellListCtrl::IncludeFilesSet(
	bool bIncludeFiles,
	bool bRescanNow // = true
	)
{
	ASSERT_VALID( this );
	if(		( m_bIncludeFiles && bIncludeFiles )
		||	( (!m_bIncludeFiles) && (!bIncludeFiles) )
		)
		return;
	m_bIncludeFiles = bIncludeFiles;
	if( ( ! bRescanNow ) || GetSafeHwnd() == NULL )
		return;
	DeleteAllItems();
	UpdateContent( m_pidlFolder.GetPtr(), true );
}

bool CExtShellListCtrl::IncludeFilesGet() const
{
	ASSERT_VALID( this );
	return m_bIncludeFiles;
}

void CExtShellListCtrl::ShowExtensionsSet(
	bool bShowExtensions,
	bool bRescanNow // = true
	)
{
	ASSERT_VALID( this );
	if(		( m_bShowExtensions && bShowExtensions )
		||	( (!m_bShowExtensions) && (!bShowExtensions) )
		)
		return;
	m_bShowExtensions = bShowExtensions;
	if( ( ! bRescanNow ) || GetSafeHwnd() == NULL )
		return;
	DeleteAllItems();
	UpdateContent( m_pidlFolder.GetPtr(), true );
}

bool CExtShellListCtrl::ShowExtensionsGet() const
{
	ASSERT_VALID( this );
	return m_bShowExtensions;
}

bool CExtShellListCtrl::FoldersBeforeOthersGet() const
{
	ASSERT_VALID( this );
	return m_bFoldersBeforeOthers;
}

void CExtShellListCtrl::FoldersBeforeOthersSet( bool bFoldersBeforeOthers )
{
	ASSERT_VALID( this );
	m_bFoldersBeforeOthers = bFoldersBeforeOthers;
	UpdateContent( m_pidlFolder.GetPtr(), true );
}

bool CExtShellListCtrl::WatchFileSystemGet() const
{
	ASSERT_VALID( this );
	return m_bWatchFileSystem;
}

void CExtShellListCtrl::WatchFileSystemSet( bool bWatchFileSystem )
{
	ASSERT_VALID( this );

	if( m_bWatchFileSystem == bWatchFileSystem )
		return;

	m_bWatchFileSystem = bWatchFileSystem;
	if( ! m_bWatchFileSystem )
		WatchDirChanges_Stop();
	UpdateContent( m_pidlFolder.GetPtr(), true );
}

bool CExtShellListCtrl::ShowRootFolderItemGet()
{
	ASSERT_VALID( this );
	return m_bShowRootFolderItem;
}

void CExtShellListCtrl::ShowRootFolderItemSet( bool bShowRootFolderItem )
{
	ASSERT_VALID( this );
	m_bShowRootFolderItem = bShowRootFolderItem;

	if( ! m_pidlFolder.IsEmpty() )
		UpdateContent( m_pidlFolder.GetPtr(), true );
}

void CExtShellListCtrl::OnBeginLabelEdit( NMHDR * pNMHDR, LRESULT * pResult )
{
	CExtListCtrl::OnBeginLabelEdit( pNMHDR, pResult );
	(*pResult) = 0;
bool bCanRename = false;
	if( ! m_SF.IsEmpty() )
	{
		NMLVDISPINFO * pNMDI = (NMLVDISPINFO*)pNMHDR;
		ASSERT( pNMDI != NULL );
		INT nItemIndex = pNMDI->item.iItem;
		ASSERT( 0 <= nItemIndex && nItemIndex < GetItemCount() );
		CExtShellItemData * pShellItemData = (CExtShellItemData*)GetItemData( nItemIndex );
		ASSERT( pShellItemData != NULL );
		if( pShellItemData->m_eSIT == CExtShellItemData::__ESIT_NORMAL )
		{
			if( ! pShellItemData->m_pidlAbsolute.IsEmpty() )
			{
				DWORD dwAttributes = SFGAO_CANRENAME;
				LPCITEMIDLIST pidl = (LPCITEMIDLIST)pShellItemData->m_pidlAbsolute.GetLast();
				m_SF->GetAttributesOf( 1, &pidl, &dwAttributes );
				if( ( dwAttributes & SFGAO_CANRENAME ) != 0 )
					bCanRename = true;
			}
		}
		else
		{
			HWND hWndEditor = (HWND) ::SendMessage( m_hWnd, LVM_GETEDITCONTROL, 0L, 0L );
			if( hWndEditor != NULL )
				::SendMessage( hWndEditor, WM_CANCELMODE, 0L, 0L );
			SendMessage( WM_CANCELMODE );
			(*pResult) = 1L;
		}
	}
	if( ! bCanRename )
	{
		CEdit * pEdit = GetEditControl();
		if( pEdit->GetSafeHwnd() != NULL )
			pEdit->SetReadOnly( TRUE );
	}
}

void CExtShellListCtrl::Refresh()
{
	ASSERT_VALID( this );
	UpdateContent( m_pidlFolder.GetPtr(), true );
}

//	bool CExtShellListCtrl::DragEnabledGet() const
//	{
//		ASSERT_VALID( this );
//		return m_bDragEnabled;
//	}
//
//	void CExtShellListCtrl::DragEnabledSet( bool bDragEnabledd )
//	{
//		ASSERT_VALID( this );
//		m_bDragEnabled = bDragEnabled;
//	}
//
//	bool CExtShellListCtrl::DropEnableGet() const
//	{
//		ASSERT_VALID( this );
//		return m_bDropEnable;
//	}
//
//	void CExtShellListCtrl::DropEnagleSet( bool bDropEnable )
//	{
//		ASSERT_VALID( this );
//		m_bDropEnable = bDropEnable;
//	}

bool CExtShellListCtrl::RunItemsGet() const
{
	ASSERT_VALID( this );
	return m_bRunItems;
}

void CExtShellListCtrl::RunItemsSet( bool bRunItems )
{
	ASSERT_VALID( this );
	m_bRunItems = bRunItems;
}

bool CExtShellListCtrl::ShowShellContextMenusGet() const
{
	ASSERT_VALID( this );
	return m_bShowShellContextMenus;
}

void CExtShellListCtrl::ShowShellContextMenusSet( bool bShowShellContextMenus )
{
	ASSERT_VALID( this );
	m_bShowShellContextMenus = bShowShellContextMenus;
}

// void CExtShellListCtrl::IncludeFilesSet(
// 	bool bIncludeFiles,
// 	bool bRescanNow // = true
// 	)
// {
// 	ASSERT_VALID( this );
// 	if(		( m_bIncludeFiles && bIncludeFiles )
// 		||	( (!m_bIncludeFiles) && (!bIncludeFiles) )
// 		)
// 		return;
// 	m_bIncludeFiles = bIncludeFiles;
// 	if( ( ! bRescanNow ) || GetSafeHwnd() == NULL )
// 		return;
// 	DeleteAllItems();
// 	UpdateContent( m_pidlFolder.GetPtr(), true );
// }
// 
// bool CExtShellListCtrl::IncludeFilesGet() const
// {
// 	ASSERT_VALID( this );
// 	return m_bIncludeFiles;
// }

CExtSafeString CExtShellListCtrl::GetCurrentFolderPath() const
{
	ASSERT_VALID( this );
	return m_pidlFolder.GetPath();
}

LPITEMIDLIST CExtShellListCtrl::GetCurrentFolderPIDL()
{
	ASSERT_VALID( this );
	return m_pidlFolder.GetPtr();
}

LPCITEMIDLIST CExtShellListCtrl::GetCurrentFolderPIDL() const
{
	ASSERT_VALID( this );
	return m_pidlFolder.GetPtr();
}

IShellFolder * CExtShellListCtrl::GetCurrentFolder()
{
	ASSERT_VALID( this );
	return m_SF.GetPtr();
}

CExtSafeString CExtShellListCtrl::WatchDirChanges_GetWatchPath() const
{
	return m_pidlFolder.GetPath();
}

HWND CExtShellListCtrl::WatchDirChanges_GetSafeHwnd() const
{
	return GetSafeHwnd();
}

void CExtShellListCtrl::RescanContent()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	DeleteAllItems();
	UpdateContent( m_pidlFolder.GetPtr(), true );
}

void CExtShellListCtrl::ExtensionsListGet( CExtSafeStringList & listExtensions ) const
{
	ASSERT_VALID( this );
	listExtensions.RemoveAll();
	listExtensions.AddTail( const_cast < CExtSafeStringList * > ( & m_listExtensions ) );
}

void CExtShellListCtrl::ExtensionsListSet( const CExtSafeStringList & listExtensions )
{
	ASSERT_VALID( this );
	if( listExtensions.GetCount() == 0 )
		return;
	m_listExtensions.RemoveAll();
	m_listExtensions.AddTail( const_cast < CExtSafeStringList * > ( & listExtensions ) );
POSITION pos = m_listExtensions.GetHeadPosition();
	for( ; pos != NULL; )
	{
		CExtSafeString & str = m_listExtensions.GetNext( pos );
		str.MakeLower();
	}
}

INT CExtShellListCtrl::CompareItems(
	const CExtListCtrlDataSortOrder & _sortOrder,
	INT nItemIndex1,
	INT nItemIndex2
	) const
{
	ASSERT_VALID( this );
	ASSERT( ! _sortOrder.IsEmpty() );
	if( nItemIndex1 == nItemIndex2 )
		return 0;
static const INT g_nShellItemTypeLess = 1;
static const INT g_nShellItemTypeGreater = -1;
CExtShellItemData * pShellItemData1 = (CExtShellItemData*)GetItemData( nItemIndex1 );
CExtShellItemData * pShellItemData2 = (CExtShellItemData*)GetItemData( nItemIndex2 );
	if( pShellItemData1 == NULL )
	{
		if( pShellItemData2 == NULL )
			return 0;
		else
			return g_nShellItemTypeLess;
	}
	else if( pShellItemData2 == NULL )
		return g_nShellItemTypeGreater;
	else if( INT(pShellItemData1->m_eSIT) < INT(pShellItemData2->m_eSIT) )
		return g_nShellItemTypeLess;
	else if( INT(pShellItemData1->m_eSIT) > INT(pShellItemData2->m_eSIT) )
		return g_nShellItemTypeGreater;

	if( FoldersBeforeOthersGet() )
	{
		int n1 = ( ( pShellItemData1->m_dwCachedAttributes & SFGAO_FOLDER ) == 0 ) ? 1 : 0;
		int n2 = ( ( pShellItemData2->m_dwCachedAttributes & SFGAO_FOLDER ) == 0 ) ? 1 : 0;
		if( n1 < n2 )
			return -1;
		if( n1 > n2 )
			return 1;
	}

INT nSortColNo, nSortColCount = INT(_sortOrder.m_arrItems.GetSize());
	for( nSortColNo = 0; nSortColNo < nSortColCount; nSortColNo ++ )
	{
		const CExtListCtrlDataSortOrder::COLUMN_INFO & _columnInfo = _sortOrder.m_arrItems[ nSortColNo ];

		int nCmpResult = 0;
		if( _columnInfo.m_nColNo < m_arrColumnTypes.GetSize() )
		{
			e_shell_list_column_type_t eSLCT = (e_shell_list_column_type_t) m_arrColumnTypes[ _columnInfo.m_nColNo ];
			switch( INT(eSLCT) )
			{
//			case INT(__ESLCT_NAME):
//			break;
			case INT(__ESLCT_SIZE):
				{
					INT nSize1 = 0, nSize2 = 0;
					if(		pShellItemData1->m_eSIT == CExtShellItemData::__ESIT_NORMAL
						&&	( pShellItemData1->m_dwCachedAttributes & SFGAO_FOLDER ) == 0
						)
					{
						CExtSafeString strPath = pShellItemData1->m_pidlAbsolute.GetPath();
						if( ! strPath.IsEmpty() )
						{
							CFileFind _ff;
							if( _ff.FindFile( LPCTSTR(strPath) ) )
							{
								_ff.FindNextFile();
								nSize1 = INT( ( _ff.GetLength() + 1024 ) / 1024 );
							}
						}
					}
					if(		pShellItemData2->m_eSIT == CExtShellItemData::__ESIT_NORMAL
						&&	( pShellItemData2->m_dwCachedAttributes & SFGAO_FOLDER ) == 0
						)
					{
						CExtSafeString strPath = pShellItemData2->m_pidlAbsolute.GetPath();
						if( ! strPath.IsEmpty() )
						{
							CFileFind _ff;
							if( _ff.FindFile( LPCTSTR(strPath) ) )
							{
								_ff.FindNextFile();
								nSize2 = INT( ( _ff.GetLength() + 1024 ) / 1024 );
							}
						}
					}
					if( nSize1 < nSize2 )
						nCmpResult = -1;
					else if( nSize1 > nSize2 )
						nCmpResult = 1;
					return _columnInfo.m_bAscending ? (nCmpResult) : (-nCmpResult);
				}
			break;
//			case INT(__ESLCT_TYPE):
 //			break;
			case INT(__ESLCT_DATE_MODIFIED):
				{
					CTime dt1, dt2;
					if(		pShellItemData1->m_eSIT == CExtShellItemData::__ESIT_NORMAL
						//&&	( pShellItemData1->m_dwCachedAttributes & SFGAO_FOLDER ) == 0
						)
					{
						CExtSafeString strPath = pShellItemData1->m_pidlAbsolute.GetPath();
						if( ! strPath.IsEmpty() )
						{
							CFileFind _ff;
							if( _ff.FindFile( LPCTSTR(strPath) ) )
							{
								_ff.FindNextFile();
								_ff.GetLastWriteTime( dt1 );
							}
						}
					}
					if(		pShellItemData2->m_eSIT == CExtShellItemData::__ESIT_NORMAL
						//&&	( pShellItemData2->m_dwCachedAttributes & SFGAO_FOLDER ) == 0
						)
					{
						CExtSafeString strPath = pShellItemData2->m_pidlAbsolute.GetPath();
						if( ! strPath.IsEmpty() )
						{
							CFileFind _ff;
							if( _ff.FindFile( LPCTSTR(strPath) ) )
							{
								_ff.FindNextFile();
								_ff.GetLastWriteTime( dt2 );
							}
						}
					}
					if( dt1 < dt2 )
						nCmpResult = -1;
					else if( dt1 > dt2 )
						nCmpResult = 1;
					return _columnInfo.m_bAscending ? (nCmpResult) : (-nCmpResult);
				}
			break;
			case INT(__ESLCT_DATE_CREATED):
				{
					CTime dt1, dt2;
					if(		pShellItemData1->m_eSIT == CExtShellItemData::__ESIT_NORMAL
						//&&	( pShellItemData1->m_dwCachedAttributes & SFGAO_FOLDER ) == 0
						)
					{
						CExtSafeString strPath = pShellItemData1->m_pidlAbsolute.GetPath();
						if( ! strPath.IsEmpty() )
						{
							CFileFind _ff;
							if( _ff.FindFile( LPCTSTR(strPath) ) )
							{
								_ff.FindNextFile();
								_ff.GetCreationTime( dt1 );
							}
						}
					}
					if(		pShellItemData2->m_eSIT == CExtShellItemData::__ESIT_NORMAL
						//&&	( pShellItemData2->m_dwCachedAttributes & SFGAO_FOLDER ) == 0
						)
					{
						CExtSafeString strPath = pShellItemData2->m_pidlAbsolute.GetPath();
						if( ! strPath.IsEmpty() )
						{
							CFileFind _ff;
							if( _ff.FindFile( LPCTSTR(strPath) ) )
							{
								_ff.FindNextFile();
								_ff.GetCreationTime( dt2 );
							}
						}
					}
					if( dt1 < dt2 )
						nCmpResult = -1;
					else if( dt1 > dt2 )
						nCmpResult = 1;
					return _columnInfo.m_bAscending ? (nCmpResult) : (-nCmpResult);
				}
			break;
			case INT(__ESLCT_DATE_ACCESSED):
				{
					CTime dt1, dt2;
					if(		pShellItemData1->m_eSIT == CExtShellItemData::__ESIT_NORMAL
						//&&	( pShellItemData1->m_dwCachedAttributes & SFGAO_FOLDER ) == 0
						)
					{
						CExtSafeString strPath = pShellItemData1->m_pidlAbsolute.GetPath();
						if( ! strPath.IsEmpty() )
						{
							CFileFind _ff;
							if( _ff.FindFile( LPCTSTR(strPath) ) )
							{
								_ff.FindNextFile();
								_ff.GetLastAccessTime( dt1 );
							}
						}
					}
					if(		pShellItemData2->m_eSIT == CExtShellItemData::__ESIT_NORMAL
						//&&	( pShellItemData2->m_dwCachedAttributes & SFGAO_FOLDER ) == 0
						)
					{
						CExtSafeString strPath = pShellItemData2->m_pidlAbsolute.GetPath();
						if( ! strPath.IsEmpty() )
						{
							CFileFind _ff;
							if( _ff.FindFile( LPCTSTR(strPath) ) )
							{
								_ff.FindNextFile();
								_ff.GetLastAccessTime( dt2 );
							}
						}
					}
					if( dt1 < dt2 )
						nCmpResult = -1;
					else if( dt1 > dt2 )
						nCmpResult = 1;
					return _columnInfo.m_bAscending ? (nCmpResult) : (-nCmpResult);
				}
			break;
			case INT(__ESLCT_ATTRIBUTES):
				{
					DWORD dw1 = 0, dw2 = 0;
					if(		pShellItemData1->m_eSIT == CExtShellItemData::__ESIT_NORMAL
						//&&	( pShellItemData1->m_dwCachedAttributes & SFGAO_FOLDER ) == 0
						)
					{
						CExtSafeString strPath = pShellItemData1->m_pidlAbsolute.GetPath();
						if( ! strPath.IsEmpty() )
						{
							CFileFind _ff;
							if( _ff.FindFile( LPCTSTR(strPath) ) )
							{
								_ff.FindNextFile();
								if( _ff.IsArchived() )
									dw1 |= ( 1 << 0 );
								if( _ff.IsCompressed() )
									dw1 |= ( 1 << 1 );
								if( _ff.IsHidden() )
									dw1 |= ( 1 << 2 );
								if( _ff.IsReadOnly() )
									dw1 |= ( 1 << 3 );
								if( _ff.IsSystem() )
									dw1 |= ( 1 << 4 );
							}
						}
					}
					if(		pShellItemData2->m_eSIT == CExtShellItemData::__ESIT_NORMAL
						//&&	( pShellItemData2->m_dwCachedAttributes & SFGAO_FOLDER ) == 0
						)
					{
						CExtSafeString strPath = pShellItemData2->m_pidlAbsolute.GetPath();
						if( ! strPath.IsEmpty() )
						{
							CFileFind _ff;
							if( _ff.FindFile( LPCTSTR(strPath) ) )
							{
								_ff.FindNextFile();
								if( _ff.IsArchived() )
									dw2 |= ( 1 << 0 );
								if( _ff.IsCompressed() )
									dw2 |= ( 1 << 1 );
								if( _ff.IsHidden() )
									dw2 |= ( 1 << 2 );
								if( _ff.IsReadOnly() )
									dw2 |= ( 1 << 3 );
								if( _ff.IsSystem() )
									dw2 |= ( 1 << 4 );
							}
						}
					}
					if( dw1 < dw2 )
						nCmpResult = -1;
					else if( dw1 > dw2 )
						nCmpResult = 1;
					return _columnInfo.m_bAscending ? (nCmpResult) : (-nCmpResult);
				}
			break;
			} // switch( INT(eSLCT) )
		}

		CExtSafeString strText1 = GetItemText( nItemIndex1, _columnInfo.m_nColNo );
		CExtSafeString strText2 = GetItemText( nItemIndex2, _columnInfo.m_nColNo );
		nCmpResult = strText1.Compare( strText2 );
		if( nCmpResult == 0 )
			continue;
		return _columnInfo.m_bAscending ? (nCmpResult) : (-nCmpResult);
	}
	return 0;
}

#endif //  ( ! defined __EXT_MFC_NO_SHELL_LIST_VIEW_CTRL )

#if ( ! defined __EXT_MFC_NO_SHELL_TREE_VIEW_CTRL )

/////////////////////////////////////////////////////////////////////////////
// CExtShellTreeCtrl

IMPLEMENT_DYNCREATE( CExtShellTreeCtrl, CExtTreeCtrl )

CExtShellTreeCtrl::CExtShellTreeCtrl()
	: m_bIncludeFiles( false )
	, m_bShowExtensions( false )
	, m_dwAttributeFilterAny( __EXT_MFC_SFGAO_DEFAULT_ATTRIBUTE_FILTER & (~SFGAO_HIDDEN) )
	, m_dwAttributeFilterAllPresent( 0 /*SFGAO_FILESYSANCESTOR*/ )
	, m_dwAttributeFilterAllAbsent( 0 )
	, m_bWatchFileSystem( true )
	, m_htiLastCtxMenu( NULL )
//	, m_bDragEnabled( true )
//	, m_bDropEnabled( true )
	, m_bDelayedIconRescanning( false )
	, m_nSCA_Hidden( __EXT_MFC_DEF_SHELL_ITEM_SCA_HIDDEN )
	, m_nSCA_ReadOnly( __EXT_MFC_DEF_SHELL_ITEM_SCA_READ_ONLY )
	, m_bRunItems( false )
	, m_bShowShellContextMenus( true )
	, m_bHelperShellCtxMenuTracking( false )
{
}

CExtShellTreeCtrl::~CExtShellTreeCtrl()
{
}

CExtShellTreeCtrl::CExtSWDCTI::CExtSWDCTI( HWND hWndNotify, HTREEITEM hti, __EXT_MFC_SAFE_LPCTSTR strFullPath )
	: m_hWndNotify( hWndNotify )
	, m_hti( hti )
	, m_strFullPath( strFullPath )
{
}

CExtShellTreeCtrl::CExtSWDCTI::~CExtSWDCTI()
{
	WatchDirChanges_Stop(); // to avoid pure virtual function calls from parallel threads
}

CExtSafeString CExtShellTreeCtrl::CExtSWDCTI::WatchDirChanges_GetWatchPath() const
{
	return m_strFullPath;
}

HWND CExtShellTreeCtrl::CExtSWDCTI::WatchDirChanges_GetSafeHwnd() const
{
	return m_hWndNotify;
}

CExtShellTreeCtrl::SortLParamData::SortLParamData(
	IShellFolder * pSF,
	CExtShellTreeCtrl & wndShellTree
	)
	: m_pSF( pSF )
	, m_wndShellTree( wndShellTree )
{
	ASSERT( m_pSF != NULL );
	ASSERT( m_wndShellTree.GetSafeHwnd() != NULL );
}

CExtShellTreeCtrl::SortLParamData:: operator LPARAM () const
{
	return reinterpret_cast < LPARAM > ( this );
}

CExtShellTreeCtrl::SortLParamData & CExtShellTreeCtrl::SortLParamData::FromLPARAM( LPARAM lParam )
{
	SortLParamData * pSLD =  reinterpret_cast < SortLParamData * > ( lParam );
	if( pSLD == NULL )
	{
		ASSERT( FALSE );
		::AfxThrowUserException();
	}
	return (*pSLD);
}

BEGIN_MESSAGE_MAP( CExtShellTreeCtrl, CExtTreeCtrl )
	//{{AFX_MSG_MAP(CExtShellTreeCtrl)
	//}}AFX_MSG_MAP
	ON_COMMAND_RANGE( IDM_FIRST_SHELLMENUID, IDM_LAST_SHELLMENUID, OnShellCommand )
	ON_REGISTERED_MESSAGE( CExtShellBase::g_nMsgUpdateItemContent, OnUpdateItemContent )
	ON_REGISTERED_MESSAGE( CExtShellBase::g_nMsgChangeChildrenState, OnChangeChildrenStateItem )
	//ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

void CExtShellTreeCtrl::PreSubclassWindow()
{
	//VERIFY( Shell_Init() );
	_InitShellTree( false );
	m_mapItemInfo.SetAt( TVI_ROOT, new TREEITEMINFO_t );
	_InsertItemFromIDList( NULL, NULL, NULL, NULL, true );
	HookSpyRegister( __EHSEF_KEYBOARD );
	m_bHelperShellCtxMenuTracking = false;
	CExtTreeCtrl::PreSubclassWindow();
}

void CExtShellTreeCtrl::PostNcDestroy()
{
	HookSpyUnregister( __EHSEF_KEYBOARD );
	m_bHelperShellCtxMenuTracking = false;
	m_htiLastCtxMenu = NULL;
	m_pidlRoot.Empty();
	m_pidlDelayedFocus.Empty();
	_UnregisterItemsFromMap();
	//Shell_Done();
	CExtTreeCtrl::PostNcDestroy();
}

void CExtShellTreeCtrl::_CleanupUpNeededChildren( HTREEITEM hParent )
{
HTREEITEM hChild = GetChildItem(hParent);
	for( ; hChild != NULL; )
	{
		HTREEITEM hNext = GetNextSiblingItem(hChild);
		TREEITEMINFO_t & _TII = TreeItemInfoGet( hChild );
		CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
		CExtSWDCTI * pSWDC = (CExtSWDCTI*)pShellItemData->m_pSWDC;
		if( ! CExtShellBase::stat_ValidPath( pSWDC->m_strFullPath ) )
			DeleteItem( hChild );
		hChild = hNext;
	}
}

void CExtShellTreeCtrl::_InitShellTree( bool bDelayedIconRescanning )
{
	ASSERT_VALID( this );
	m_htiLastCtxMenu = NULL;
	ASSERT( CExtShellBase::g_ilShellSmall.GetSafeHandle() != NULL );
	if( bDelayedIconRescanning )
		m_bDelayedIconRescanning = true;
	else
		_InitShellTreeIcons();
}

void CExtShellTreeCtrl::_InitShellTreeIcons()
{
	ASSERT_VALID( this );
	if( ! m_bDelayedIconRescanning )
		return;
	m_bDelayedIconRescanning = false;
	TreeIconRemoveAll();
INT nIndex, nCount = CExtShellBase::g_ilShellSmall.GetImageCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		HICON hIcon = CExtShellBase::g_ilShellSmall.ExtractIcon( nIndex );
//		ASSERT( hIcon != NULL );
		CExtCmdIcon _icon;
		_icon.AssignFromHICON( hIcon, false );
//_icon.m_bmpNormal.PreMultipliedRGBChannelsSet( true );
		TreeIconAdd( _icon );
	}
}

void CExtShellTreeCtrl::_DoDelayedTreeIconInit()
{
	ASSERT_VALID( this );
	_InitShellTreeIcons();
}

void CExtShellTreeCtrl::OnPaintTreeItemIcon(
	HTREEITEM hti,
	CDC & dc,
	const CExtCmdIcon & _icon,
	CRect rcTreeIconArea,
	BYTE nSCA // = BYTE(255)
	)
{
	ASSERT_VALID( this );
	ASSERT( hti != NULL );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( ! _icon.IsEmpty() );

TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
ASSERT( pShellItemData != NULL );
//DWORD dwAttributes = pShellItemData->m_dwCachedAttributes;
DWORD dwAttributes = pShellItemData->m_pidlAbsolute.GetAttributesOf( SFGAO_HIDDEN | SFGAO_READONLY | SFGAO_LINK | SFGAO_SHARE );
	if(		( ( m_dwAttributeFilterAny & ( SFGAO_HIDDEN | SFGAO_READONLY ) ) != 0 )
		&&	( m_nSCA_Hidden != 255 || m_nSCA_ReadOnly != 255 )
		)
	{
		if( ( dwAttributes & SFGAO_READONLY ) != 0 )
			nSCA = m_nSCA_ReadOnly;
		if( ( dwAttributes & SFGAO_HIDDEN ) != 0 )
			nSCA = m_nSCA_Hidden;
	}
	// Test Add for Shell Problem in Windows 8. alw/nelu 02112014 
	if(g_PaintManager.m_osVerData.dwMajorVersion >= 6 && g_PaintManager.m_osVerData.dwMinorVersion >= 2) // window 8
	{
		SHFILEINFO _sfi;
		::memset( &_sfi, 0, sizeof(SHFILEINFO) );
		if (SHGetFileInfo ((LPCTSTR)(LPCTSTR)pShellItemData->m_pidlAbsolute.GetPtr(), 0, &_sfi, sizeof (SHFILEINFO), 
			SHGFI_PIDL | SHGFI_ICON | 0x000000020 ))
		{
			if( _sfi.hIcon != NULL )
			{
				CExtCmdIcon icon;
				icon.AssignFromHICON( _sfi.hIcon, false );
				icon.Scale(rcTreeIconArea.Size());
				CExtTreeCtrl::OnPaintTreeItemIcon( hti, dc, icon, rcTreeIconArea, nSCA );
				return;
			}
		}
	}

	CExtTreeCtrl::OnPaintTreeItemIcon( hti, dc, _icon, rcTreeIconArea, nSCA );
	if( ( dwAttributes & ( SFGAO_LINK | SFGAO_SHARE ) ) != 0 )
	{
		CExtPaintManager * pPM = g_PaintManager.GetPM();
		ASSERT_VALID( pPM );
		CExtCmdIcon::e_paint_type_t ePT = IsItemEnabled( hti ) ? CExtCmdIcon::__PAINT_NORMAL : CExtCmdIcon::__PAINT_DISABLED;
		if( ( dwAttributes & SFGAO_LINK ) != 0 )
			CExtShellBase::g_iconOverlayLinkSmall.Paint( pPM, dc.m_hDC, rcTreeIconArea, ePT, nSCA );
		if( ( dwAttributes & SFGAO_SHARE ) != 0 )
			CExtShellBase::g_iconOverlayShareSmall.Paint( pPM, dc.m_hDC, rcTreeIconArea, ePT, nSCA );
	}
}

BOOL CExtShellTreeCtrl::Expand( HTREEITEM hti, UINT nCode )
{
	ASSERT_VALID( this );
	SetRedraw( FALSE );
bool bCollapse = false;
TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
	ASSERT( pShellItemData != NULL );
CExtSWDCTI * pSWDC = (CExtSWDCTI*)pShellItemData->m_pSWDC;
	if(		nCode == TVE_COLLAPSE
		||	( nCode == TVE_TOGGLE && TreeItemIsExpanded( hti ) )
		)
	{
		if( WatchFileSystemGet() )
		{
			_SWDC_SubFolders_Stop( hti );
			pSWDC->WatchDirChanges_Stop();
		}
		bCollapse = true;
	}
BOOL nRetVal = CExtWTC < CExtTreeCtrl > :: Expand( hti, nCode );
	if( ! bCollapse )
	{
		if(		(	nCode == TVE_EXPAND
				||	( nCode == TVE_TOGGLE && TreeItemIsExpanded( hti ) ) )
			&&	WatchFileSystemGet()
			&&	_OnQueryWatchFS( hti )
			)
		{
			pSWDC->WatchDirChanges_Start( true );
			_SWDC_SubFolders_Start( hti );
		}
	}
	SetRedraw( TRUE );
	UpdateWindow();
	return nRetVal;
}

void CExtShellTreeCtrl::_SWDC_SubFolders_Start( HTREEITEM htiParent )
{
	ASSERT_VALID( this );
HTREEITEM htiChild = GetNextItem( htiParent, TVGN_CHILD );
	for( ; htiChild != NULL; htiChild = GetNextItem( htiChild, TVGN_NEXT ) )
	{
		CExtSWDCTI * pSWDC = _SWDC_Get( htiChild );
		ASSERT( pSWDC != NULL );
		pSWDC->WatchDirChanges_Start( true, true );
	}
}

void CExtShellTreeCtrl::_SWDC_SubFolders_Stop( HTREEITEM htiParent )
{
	ASSERT_VALID( this );
HTREEITEM htiChild = GetNextItem( htiParent, TVGN_CHILD );
	for( ; htiChild != NULL; htiChild = GetNextItem( htiChild, TVGN_NEXT ) )
	{
		CExtSWDCTI * pSWDC = _SWDC_Get( htiChild );
		ASSERT( pSWDC != NULL );
		pSWDC->WatchDirChanges_Stop();
	}
}

CExtShellTreeCtrl::CExtSWDCTI * CExtShellTreeCtrl::_SWDC_Get( HTREEITEM hti )
{
	ASSERT_VALID( this );
TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
CExtShellItemData* pShellItemData = (CExtShellItemData*)_TII.m_lParam;
CExtSWDCTI * pSWDC = (CExtSWDCTI*)pShellItemData->m_pSWDC;
	ASSERT( pSWDC != NULL );
	return pSWDC;
}

bool CExtShellTreeCtrl::_FillItem(
	TVITEM & _tvi,
	LPCITEMIDLIST pidl,
	IShellFolder * pParentFolder,
	LPCITEMIDLIST pidlRelative,
	CExtSafeString * strNameBuffer // = NULL
	)
{
SHFILEINFO _sfi;
DWORD dwAttributes = SFGAO_FOLDER | SFGAO_REMOVABLE;
//	pParentFolder->GetAttributesOf( 1, &pidlRelative, &dwAttributes );

CExtPIDL _pidlTest;
_pidlTest = pidl;
dwAttributes = _pidlTest.GetAttributesOf( dwAttributes );

	if( ( _tvi.mask & TVIF_TEXT ) != 0 )
	{
		ASSERT( strNameBuffer != NULL );
		strNameBuffer->Empty();
		STRRET strRetData;
		::memset( &strRetData, 0, sizeof(STRRET) );
		strRetData.uType = STRRET_WSTR;
		bool bShowExtensions = ShowExtensionsGet(), pParseMode = false;
		if( bShowExtensions )
		{
			if( ( dwAttributes & SFGAO_FOLDER ) == 0 )
				pParseMode = true;
		}
		label_repeat_non_parse:
		DWORD _shgdnf = __EXT_MFC_SHGDN_INCLUDE_NONFILESYS;
		if( pParseMode )
			_shgdnf |= __EXT_MFC_SHGDN_FORPARSING;
		else
			_shgdnf |= __EXT_MFC_SHGDN_INFOLDER;
		pParentFolder->GetDisplayNameOf( pidlRelative, _shgdnf, &strRetData );
		switch( strRetData.uType )
		{
		case STRRET_WSTR:
		{
			USES_CONVERSION;
			(*strNameBuffer) = W2CT(strRetData.pOleStr);
		}
		break;
		case STRRET_CSTR:
		{
			USES_CONVERSION;
			(*strNameBuffer) = A2CT(strRetData.cStr);
			strRetData.pOleStr = NULL;
		}
		break;
		case STRRET_OFFSET:
		{
			USES_CONVERSION;
			(*strNameBuffer) = A2CT(((char*)((LPBYTE)pidlRelative+strRetData.uOffset)));
			strRetData.pOleStr = NULL;
		}
		break;
		}
		if( strRetData.pOleStr != NULL )
			::CoTaskMemFree( strRetData.pOleStr );
		if( pParseMode )
		{
			INT nBufferLen = (INT)strNameBuffer->GetLength();
			if(		nBufferLen > 4
				&&	strNameBuffer->GetAt(0) == _T(':')
				&&	strNameBuffer->GetAt(1) == _T(':')
				&&	strNameBuffer->GetAt(2) == _T('{')
				&&	strNameBuffer->GetAt(nBufferLen-1) == _T('}')
				) // GUID is not path
			{
				pParseMode = false;
				goto label_repeat_non_parse;
			}
			int nPos = strNameBuffer->ReverseFind( _T('\\') );
			if( nPos >= 0 )
				(*strNameBuffer) = strNameBuffer->Right( nBufferLen - nPos - 1 );
		}
		_tvi.pszText = (LPTSTR)(LPCTSTR)(*strNameBuffer);
	}
	if( ( _tvi.mask & ( TVIF_IMAGE | TVIF_SELECTEDIMAGE ) ) != 0 )
	{
		dwAttributes = SFGAO_FOLDER | SFGAO_LINK;
//		pParentFolder->GetAttributesOf( 1, &pidlRelative, &dwAttributes );

CExtPIDL _pidlTest;
_pidlTest = pidl;
dwAttributes = _pidlTest.GetAttributesOf( dwAttributes );
		
		UINT uFlags = SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_SMALLICON;
		//if( ( dwAttributes & SFGAO_LINK ) != 0 )
		//	uFlags |= SHGFI_LINKOVERLAY;
		if( ( _tvi.mask & TVIF_IMAGE ) != 0 )
		{
			::memset( &_sfi, 0, sizeof(SHFILEINFO) );
			::SHGetFileInfo( (LPCTSTR)pidl, 0, &_sfi, sizeof(SHFILEINFO), uFlags );
			_tvi.iImage = _sfi.iIcon;
		}
		if( ( _tvi.mask & TVIF_SELECTEDIMAGE ) != 0 )
		{
			if( ( dwAttributes & SFGAO_FOLDER ) != 0 )
				uFlags |= SHGFI_OPENICON;
			::memset( &_sfi, 0, sizeof(SHFILEINFO) );
			::SHGetFileInfo( (LPCTSTR)pidl, 0, &_sfi, sizeof(SHFILEINFO), uFlags );
			_tvi.iSelectedImage = _sfi.iIcon;
		}
	}
	if( ( _tvi.mask & TVIF_CHILDREN ) != 0 )
	{
		_tvi.cChildren = 0;
		if( ( dwAttributes & SFGAO_FOLDER ) != 0 )
		{
			if( IncludeFilesGet() )
				_tvi.cChildren = 1;
			else if ( dwAttributes & SFGAO_REMOVABLE )
				_tvi.cChildren = 1;
			else
			{
				dwAttributes = SFGAO_HASSUBFOLDER;
//				pParentFolder->GetAttributesOf( 1, &pidlRelative, &dwAttributes );

CExtPIDL _pidlTest;
_pidlTest = pidl;
dwAttributes = _pidlTest.GetAttributesOf( dwAttributes );
				
				_tvi.cChildren = (dwAttributes & SFGAO_HASSUBFOLDER) ? 1 : 0;
			}
		}
	}
SHDESCRIPTIONID sdi;
	::memset( &sdi, 0, sizeof(SHDESCRIPTIONID) );
	if( SUCCEEDED(
			::SHGetDataFromIDList(
				pParentFolder,
				pidlRelative,
				SHGDFIL_DESCRIPTIONID,
				&sdi,
				sizeof(SHDESCRIPTIONID)
				)
			)
		)
	{
		switch( sdi.dwDescriptionId )
		{
		case SHDID_COMPUTER_REMOVABLE:
		case SHDID_COMPUTER_DRIVE35:
		case SHDID_COMPUTER_DRIVE525:
		case SHDID_COMPUTER_NETDRIVE:
		case SHDID_COMPUTER_CDROM:
		case SHDID_NET_DOMAIN:
		case SHDID_NET_SERVER:
		case SHDID_NET_SHARE:
		case SHDID_NET_RESTOFNET:
		case SHDID_NET_OTHER:
			return false;
		}
	}
	return true;
}

void CExtShellTreeCtrl::RescanContent()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	DeleteAllItems();
	UpdateContent( TVI_ROOT, true );
}

bool CExtShellTreeCtrl::UpdateContent( HTREEITEM hti, bool bCompleteRescan )
{
	ASSERT_VALID( this );
	ASSERT( g_pDesktopFolder != NULL );
	ASSERT( m_pRootFolder != NULL );
CExtPIDL pidlParent;
bool bRootFolder = false;
	if( hti == TVI_ROOT )
		pidlParent = m_pidlRoot;
	else
	{
		CExtShellItemData * pShellItemData = (CExtShellItemData*)TreeItemInfoGet( hti ).m_lParam;
		pidlParent = pShellItemData->m_pidlAbsolute;
		if( bCompleteRescan )
			_DeleteChildren( hti );
		else
			_DeleteNotExistingItems( hti );
	}

CExtCIP_SF _ParentSF;
	g_pDesktopFolder->BindToObject( pidlParent.GetPtr(), NULL, IID_IShellFolder, (LPVOID*)_ParentSF.GetPtrPtr() );
HRESULT hr = g_pDesktopFolder->CompareIDs( 0, pidlParent.GetPtr(), g_pidlDesktop );
	if( _ParentSF.IsEmpty() )
	{
		_ParentSF = m_pRootFolder;
		if( hti == TVI_ROOT )
		{
			hti = _InsertRootItem();
			bRootFolder = true;
		}
		else if( hr != 0 )
			return true;

	}
	else if( hti == TVI_ROOT )
	{
		hti = _InsertRootItem( pidlParent.GetPtr() );
		bRootFolder = true;
	}

IEnumIDList * pEnumIDList = NULL;
	if(	NOERROR ==
			_ParentSF->EnumObjects(
				m_hWnd,
				SHCONTF_FOLDERS
					| ( IncludeFilesGet() ? SHCONTF_NONFOLDERS : 0 )
					| ( ( ( m_dwAttributeFilterAny & SFGAO_HIDDEN ) != 0 ) ? SHCONTF_INCLUDEHIDDEN : 0 ),
				&pEnumIDList
				)
		)
	{
		LPITEMIDLIST pidl = NULL;
		for( ; NOERROR == pEnumIDList->Next( 1, &pidl, NULL ); )
			_InsertItemFromIDList( hti, pidlParent.GetPtr(), pidl, _ParentSF.GetPtr(), false, ! bCompleteRescan );
		pEnumIDList->Release();
	}

	if( bRootFolder )
		Expand( hti, TVE_EXPAND );

	return true;
}

HTREEITEM CExtShellTreeCtrl::_InsertRootItem(
	LPITEMIDLIST pidlRoot // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT( CExtShellBase::g_pMalloc != NULL );

bool bRetVal = false;
LPITEMIDLIST pidlAbsolute;
	if( pidlRoot == NULL )
		pidlAbsolute = g_pidlDesktop;
	else
		pidlAbsolute = pidlRoot;

HTREEITEM htiRootBe = NULL;
	htiRootBe = GetNextItem( TVI_ROOT, TVGN_CHILD );
	if( htiRootBe != NULL )
		return htiRootBe;

TVINSERTSTRUCT tvis;
	::memset( &tvis, 0, sizeof(TVINSERTSTRUCT) );
	tvis.item.mask = TVIF_TEXT|TVIF_CHILDREN|TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_PARAM;
	tvis.hParent = TVI_ROOT;
	tvis.hInsertAfter = TVI_LAST; // TVI_SORT
	tvis.item.lParam = (LPARAM)pidlAbsolute;
CExtSafeString strNameBuffer;
HTREEITEM hti = NULL;
	bRetVal = _FillItem( tvis.item, pidlAbsolute, m_pRootFolder, pidlAbsolute, &strNameBuffer );
	_InitShellTree( false );
	hti = InsertItem( &tvis );
	if( hti != NULL )
	{
		TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
		_TII.m_dwAdditionalLabelEditorStyles |= ES_READONLY;
		CExtShellItemData * pShellItemData = new CExtShellItemData;
		ASSERT( pidlAbsolute != NULL );
		pShellItemData->m_pidlAbsolute = pidlAbsolute;
		pShellItemData->UpdateCachedAttributes();
		ASSERT( pShellItemData->IsValid() );
		TCHAR strFullPath[MAX_PATH];
		if( pidlRoot == NULL )
			SHGetPathFromIDList( g_pidlDesktop, strFullPath );
		else
			SHGetPathFromIDList( pidlAbsolute, strFullPath );
		pShellItemData->m_pSWDC = new CExtSWDCTI( m_hWnd, hti, strFullPath );
		_TII.m_lParam = (LPARAM)pShellItemData;
		_TII.m_nIconIndexExpanded = _TII.m_nIconIndexCollapsed = tvis.item.iImage;
	}
	if( pidlAbsolute != NULL && LPVOID(pidlAbsolute) != LPVOID(g_pidlDesktop) && LPVOID(pidlAbsolute) != LPVOID(pidlRoot) )
		CExtShellBase::g_pMalloc->Free( pidlAbsolute );
	return hti;
}

void CExtShellTreeCtrl::_InsertItemFromIDList(
	HTREEITEM hParent, // = NULL
	LPITEMIDLIST pidlParent, // = NULL
	LPITEMIDLIST pidl, // = NULL
	IShellFolder * pParentFolder, // = NULL
	bool bRoot, // = false
	bool bCheckExisting // = false
	)
{
	ASSERT_VALID( this );
	ASSERT( CExtShellBase::g_pidlDesktop != NULL );
	ASSERT( CExtShellBase::g_pMalloc != NULL );
CExtPIDL pidlAbsolute;
	if( pidlParent == NULL && pidl == NULL )
		pidlAbsolute = CExtShellBase::g_pidlDesktop;
	else
		pidlAbsolute.Combine( pidlParent, pidl );
HTREEITEM htiFound = NULL;

	if( bCheckExisting )
		htiFound = FindItemByPIDL( hParent, pidlAbsolute.GetPtr() );
DWORD dwAttributes = pidlAbsolute.GetAttributesOf( __EXT_DEFAULT_ATTRIBUTES_FOR_CExtPIDL_GetAttributesOf__ );
bool bAdd = pidlAbsolute.IsRoot();
	if( ! bAdd )
	{
		bAdd = ( ( dwAttributes & m_dwAttributeFilterAllPresent ) == m_dwAttributeFilterAllPresent ) ? true : false;
		if( bAdd )
		{
			bAdd = ( ( dwAttributes & m_dwAttributeFilterAllAbsent ) == 0 ) ? true : false;
			if( bAdd )
				bAdd = ( ( dwAttributes & m_dwAttributeFilterAny ) != 0 ) ? true : false;
		}
	}
	if( htiFound != NULL )
	{
		if( ! bAdd  )
		{
			DeleteItem( htiFound );
			return;
		}
		TREEITEMINFO_t & _TII = TreeItemInfoGet( htiFound );
		CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
		ASSERT( pShellItemData != NULL );
		pShellItemData->m_pidlAbsolute = pidlAbsolute;
		//bool bChanged = pShellItemData->UpdateCachedAttributes();
		//DWORD dwAttributes = pShellItemData->m_dwCachedAttributes;
		bool bChanged = false;
		if( pShellItemData->m_dwCachedAttributes != dwAttributes )
		{
			bChanged = TRUE;
			pShellItemData->m_dwCachedAttributes = dwAttributes;
		}
		TVITEM _tvi;
		::memset( &_tvi, 0, sizeof(TVITEM) );
		_tvi.hItem = htiFound;
		_tvi.mask = TVIF_CHILDREN;
		VERIFY( GetItem( &_tvi ) );
		int cChildrenOld = _tvi.cChildren;
		_tvi.cChildren = 0;
		if( ( dwAttributes & SFGAO_FOLDER ) != 0 )
		{
			if( IncludeFilesGet() )
				_tvi.cChildren = 1;
			else if ( dwAttributes & SFGAO_REMOVABLE )
				_tvi.cChildren = 1;
			else
				//_tvi.cChildren = (dwAttributes & SFGAO_HASSUBFOLDER) ? 1 : 0;
				_tvi.cChildren = ( pidlAbsolute.GetAttributesOf(SFGAO_HASSUBFOLDER) & SFGAO_HASSUBFOLDER ) ? 1 : 0;
		}
		if( cChildrenOld != _tvi.cChildren )
			VERIFY( SetItem( &_tvi ) );
		if( cChildrenOld != _tvi.cChildren || bChanged )
			Invalidate();
		return;
	}
	if( ! bAdd  )
		return;
bool bRetVal = false;
TVINSERTSTRUCT tvis;
	::memset( &tvis, 0, sizeof(TVINSERTSTRUCT) );
	tvis.item.mask = TVIF_TEXT|TVIF_CHILDREN|TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_PARAM;
	tvis.hParent = hParent;
	tvis.hInsertAfter = TVI_LAST; // TVI_SORT
	tvis.item.lParam = (LPARAM)pidlAbsolute.GetPtr();
CExtSafeString strNameBuffer;
HTREEITEM hti = NULL;
	if( ! bRoot )
	{
		bRetVal = _FillItem( tvis.item, pidlAbsolute, pParentFolder, pidl, &strNameBuffer );
		_InitShellTree( true );
		hti = InsertItem( &tvis );
	}
	else
		hti = TVI_ROOT;
	if( hti != NULL || bRoot )
	{
		TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
		CExtShellItemData * pShellItemData = new CExtShellItemData;
		_TII.m_lParam = (LPARAM)pShellItemData;
		if( ! bRoot )
		{
			ASSERT( pidl != NULL );
			pShellItemData->m_pidlAbsolute = pidlAbsolute;
			ASSERT( pShellItemData->IsValid() );
		}
//		pShellItemData->UpdateCachedAttributes();
//		DWORD dwAttributes = pShellItemData->m_dwCachedAttributes;
		pShellItemData->m_dwCachedAttributes = dwAttributes;
		//if( ( dwAttributes & SFGAO_CANRENAME ) == 0 )
		if( ( pShellItemData->m_pidlAbsolute.GetAttributesOf( SFGAO_CANRENAME ) & SFGAO_CANRENAME ) == 0 )
			_TII.m_dwAdditionalLabelEditorStyles |= ES_READONLY;
		CExtSafeString strFullPath = pShellItemData->m_pidlAbsolute.GetPath();
		pShellItemData->m_pSWDC = new CExtSWDCTI( m_hWnd, hti, strFullPath );
		_TII.m_nIconIndexExpanded = _TII.m_nIconIndexCollapsed = tvis.item.iImage;
	}
	if( pidl != NULL && LPVOID(pidl) != LPVOID(g_pidlDesktop) )
		CExtShellBase::g_pMalloc->Free( pidl );
}

void CExtShellTreeCtrl::RefreshShellRoot(
	LPCITEMIDLIST pidlRoot // = NULL
	)
{
	ASSERT_VALID( this );
	m_pidlRoot = pidlRoot;
	RefreshSubItems( TVI_ROOT );
}

void CExtShellTreeCtrl::DelayFocusPIDL(
	LPCITEMIDLIST pidlAbsolute // = NULL
	)
{
	ASSERT_VALID( this );
	if(		m_pidlDelayedFocus == pidlAbsolute
		||	GetSafeHwnd() == NULL
		)
		return;
	for( MSG _msg; ::PeekMessage( &_msg, m_hWnd, CExtShellBase::g_nMsgDelayFocusPIDL, CExtShellBase::g_nMsgDelayFocusPIDL, PM_NOREMOVE ) ; )
		::PeekMessage( &_msg, m_hWnd, CExtShellBase::g_nMsgDelayFocusPIDL, CExtShellBase::g_nMsgDelayFocusPIDL, PM_REMOVE );

CExtPIDL pidlFocusedCurrently;
HTREEITEM htiCurrentlyFocused = GetFocusedItem();
	if( htiCurrentlyFocused != NULL )
	{
		TREEITEMINFO_t & _TII = TreeItemInfoGet( htiCurrentlyFocused );
		CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
		pidlFocusedCurrently = pShellItemData->m_pidlAbsolute;
	}
	if(		( pidlFocusedCurrently.IsEmpty() && pidlAbsolute == NULL )
		||	pidlFocusedCurrently == pidlAbsolute
		)
		return;

	m_pidlDelayedFocus = pidlAbsolute;
	PostMessage( CExtShellBase::g_nMsgDelayFocusPIDL );
}

HTREEITEM CExtShellTreeCtrl::FocusPIDL(
	LPCITEMIDLIST pidlAbsolute,
	bool bFocusItem, // = true
	bool bSelectItem, // = true
	bool bUnselectOtherItems, // = true
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	if( pidlAbsolute == NULL )
		return NULL;

CExtPIDL pidlFocusedCurrently;
HTREEITEM htiCurrentlyFocused = GetFocusedItem();
	if( htiCurrentlyFocused != NULL )
	{
		TREEITEMINFO_t & _TII = TreeItemInfoGet( htiCurrentlyFocused );
		CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
		pidlFocusedCurrently = pShellItemData->m_pidlAbsolute;
	}
	if(		( pidlFocusedCurrently.IsEmpty() && pidlAbsolute == NULL )
		||	pidlFocusedCurrently == pidlAbsolute
		)
		return htiCurrentlyFocused;

	ASSERT( CExtShellBase::g_pMalloc != NULL );
LPITEMIDLIST pidlWalk = (LPITEMIDLIST)pidlAbsolute, pidlNext = (LPITEMIDLIST)pidlAbsolute;
CTypedPtrList < CPtrList, LPITEMIDLIST > _listWalk;
POSITION pos;
	_listWalk.AddHead( CExtShellBase::stat_CopyItem( pidlNext ) );
	for( ; CExtShellBase::stat_GetParentItem( pidlNext, pidlWalk ) > 0; )
	{
		_listWalk.AddHead( pidlWalk );
		pidlNext = pidlWalk;
	}
HTREEITEM hti = GetChildItem( TVI_ROOT );
	if( bRedraw )
		SetRedraw( FALSE );
bool bFound = false;
	if( _listWalk.GetCount() == 1 )
	{
		pos = _listWalk.GetHeadPosition();
		ASSERT( pos != NULL );
		LPITEMIDLIST pidlFromWalkList = _listWalk.GetNext( pos );
		ASSERT( pidlFromWalkList != NULL );
		const TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
		SHFILEINFO sfi1, sfi2;
		CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;///////////
		if(		::SHGetFileInfo( (LPCTSTR)pShellItemData->m_pidlAbsolute.GetPtr(), 0, &sfi1, sizeof(sfi1), SHGFI_PIDL|SHGFI_DISPLAYNAME )
			&&	::SHGetFileInfo( (LPCTSTR)pidlFromWalkList, 0, &sfi2, sizeof(sfi2), SHGFI_PIDL|SHGFI_DISPLAYNAME )
      // W4 Change This is set in the previous statment 
#pragma warning(suppress: 6001)
			&&	::lstrcmpi( sfi1.szDisplayName, sfi2.szDisplayName ) == 0
			)
			bFound = true;
	} // if( _listWalk.GetCount() == 1 )
	if( ! bFound )
	{
		for( pos = _listWalk.GetHeadPosition(); pos != NULL; )
		{
			LPITEMIDLIST pidlFromWalkList = _listWalk.GetNext( pos );
			ASSERT( pidlFromWalkList != NULL );
			if( hti != NULL )
			{
				if( GetChildItem( hti ) == NULL )
					Expand( hti, TVE_EXPAND );
				bFound = false;
				for(	HTREEITEM hTreeChild = GetChildItem( hti );
						(!bFound) && hTreeChild != NULL;
						hTreeChild = GetNextSiblingItem( hTreeChild )
					)
				{
					const TREEITEMINFO_t & _TII = TreeItemInfoGet( hTreeChild );
					SHFILEINFO sfi1, sfi2;
					CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;///////////
					if(		::SHGetFileInfo( (LPCTSTR)pShellItemData->m_pidlAbsolute.GetPtr(), 0, &sfi1, sizeof(sfi1), SHGFI_PIDL|SHGFI_DISPLAYNAME )
						&&	::SHGetFileInfo( (LPCTSTR)pidlFromWalkList, 0, &sfi2, sizeof(sfi2), SHGFI_PIDL|SHGFI_DISPLAYNAME )
            // W4 Change The parameter is set in the previous statement 05012013
#pragma warning(suppress: 6001)
						&&	::lstrcmpi( sfi1.szDisplayName, sfi2.szDisplayName ) == 0
						)
					{
						bFound = true;
						hti = hTreeChild;
					}
				}
				if( ! bFound )
					hti = NULL;
			}
			if( pidlFromWalkList != NULL && LPVOID(pidlFromWalkList) != LPVOID(g_pidlDesktop) )
				CExtShellBase::g_pMalloc->Free( pidlFromWalkList );
		}
	} // if( ! bFound )
	_listWalk.RemoveAll();
	if( hti != NULL )
	{
		if( bRedraw )
		{
			if( bFocusItem )
				FocusItem( hti, bSelectItem, bUnselectOtherItems );
			EnsureVisible( hti );
		}
	}
	if( bRedraw )
	{
		SetRedraw( TRUE );
		Invalidate();
	}
	return hti;
}

HTREEITEM CExtShellTreeCtrl::FocusPath(
	__EXT_MFC_SAFE_LPCTSTR strPathToSelect,
	bool bFocusItem, // = true
	bool bSelectItem, // = true
	bool bUnselectOtherItems, // = true
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	if( LPCTSTR(strPathToSelect) == NULL )
		return NULL;
	ASSERT( g_pDesktopFolder != NULL );
	ASSERT( m_pRootFolder != NULL );
CExtSafeString strPath = LPCTSTR(strPathToSelect);
	if( strPath.IsEmpty() )
		strPath = _T(".");
LPITEMIDLIST pidlToSelect = NULL;
ULONG cbEaten = 0L;
DWORD dwAttribs = SFGAO_FILESYSTEM;
USES_CONVERSION;
HRESULT hr =
	g_pDesktopFolder->ParseDisplayName( 
			NULL,
			NULL,
			T2W(LPTSTR(LPCTSTR(strPath))), // T2W(LPCTSTR(strFullPath))
			&cbEaten,
			&pidlToSelect,
			&dwAttribs
			);
	if( hr != S_OK )
		return NULL;
HTREEITEM hti =
		FocusPIDL(
			pidlToSelect,
			bFocusItem,
			bSelectItem,
			bUnselectOtherItems,
			bRedraw
			);
	g_pMalloc->Free( pidlToSelect );
	return hti;
}

HTREEITEM CExtShellTreeCtrl::_CreateFolder(
	__EXT_MFC_SAFE_LPCTSTR strNewFolderName,
	HTREEITEM htiParent // = NULL // NULL - get from focus
	)
{
	ASSERT_VALID(this);
	if( LPCTSTR(strNewFolderName) == NULL || _tcslen( LPCTSTR(strNewFolderName) ) == 0 )
		return NULL;
	ASSERT( g_pDesktopFolder != NULL );
	ASSERT( m_pRootFolder != NULL );
HTREEITEM hti = htiParent;
	if( hti == NULL )
		hti = GetSelectedItem();
	if( hti == NULL )
		return NULL;
HRESULT hr = S_OK;
TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
LPITEMIDLIST pidl = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetPtr();
	if( pidl == NULL )
		return NULL;
STRRET strRetData;
	::memset( &strRetData, 0, sizeof(STRRET) );
	strRetData.uType = STRRET_WSTR;
	strRetData.pOleStr = (LPWSTR)::CoTaskMemAlloc( MAX_PATH * sizeof(TCHAR) );
	if( strRetData.pOleStr == NULL )
		return NULL;
CExtSafeString str;
	hr = g_pDesktopFolder->GetDisplayNameOf( pidl, SHGDN_FORPARSING, &strRetData );
	if( hr == S_OK )
	{
		switch( strRetData.uType )
		{
		case STRRET_WSTR:
		{
			USES_CONVERSION;
			str = W2CT(strRetData.pOleStr);
		}
		break;
		case STRRET_CSTR:
		{
			USES_CONVERSION;
			str = A2CT(strRetData.cStr);
			strRetData.pOleStr = NULL;
		}
		break;
		case STRRET_OFFSET:
		{
			USES_CONVERSION;
			str = A2CT(((char*)((LPBYTE)pidl+strRetData.uOffset)));
			strRetData.pOleStr = NULL;
		}
		break;
		}
		if( strRetData.pOleStr != NULL )
			::CoTaskMemFree( strRetData.pOleStr );
	}
	if( hr != S_OK )
		return NULL;
	ASSERT( ! str.IsEmpty() );
CExtSafeString strFullPath;
bool bRetVal = false;
LPTSTR _strFullPath_buffer = strFullPath.GetBuffer( MAX_PATH + 1 );
	if( _strFullPath_buffer != NULL )
	{
		bRetVal = ::AfxFullPath( _strFullPath_buffer, str ) ? true : false;
		strFullPath.ReleaseBuffer();
	}
	if( ! bRetVal )
		return NULL;
INT nLen = INT( strFullPath.GetLength() );
	if( nLen < 2 )
		return NULL;
	if( strFullPath[1] == _T(':') )
	{
		CExtSafeString strTmp = strFullPath.Mid( 0, 1 );
		strTmp.MakeUpper();
		TCHAR tChr = strTmp[0];
		if( ! ( _T('A') <= tChr && tChr <= _T('Z') ) )
			return NULL;
	}
	else if( strFullPath[0] == _T('\\') && strFullPath[1] == _T('\\') )
	{
		if( nLen < 5 )
			return NULL;
		LPCTSTR str = LPCTSTR( strFullPath );
		str += 3;
		str = _tcschr( str, _T('\\') );
		if( str == NULL )
			return NULL;
		str ++;
		if( (*str) == _T('\0') )
			return NULL;
	}
	else
		return NULL;
	strFullPath += _T("\\");
	strFullPath += strNewFolderName;
	if( ! ::CreateDirectory( LPCTSTR(strFullPath), NULL ) )
		return NULL;
	SetRedraw( FALSE );
	FocusItem( hti, false, true );
	Expand( hti, TVE_COLLAPSE );
	SetRedraw( TRUE );
HTREEITEM htiRetVal = FocusPath( LPCTSTR(strFullPath) );
	return htiRetVal;
}

void CExtShellTreeCtrl::IncludeFilesSet(
	bool bIncludeFiles,
	bool bRescanNow // = true
	)
{
	ASSERT_VALID( this );
	if(		( m_bIncludeFiles && bIncludeFiles )
		||	( (!m_bIncludeFiles) && (!bIncludeFiles) )
		)
		return;
	m_bIncludeFiles = bIncludeFiles;
	if( ( ! bRescanNow ) || GetSafeHwnd() == NULL )
		return;
	DeleteAllItems();
	UpdateContent( TVI_ROOT, true );
}

bool CExtShellTreeCtrl::IncludeFilesGet() const
{
	ASSERT_VALID( this );
	return m_bIncludeFiles;
}

bool CExtShellTreeCtrl::WatchFileSystemGet() const
{
	ASSERT_VALID( this );
	return m_bWatchFileSystem;
}

void CExtShellTreeCtrl::WatchFileSystemSet( bool bWatchFileSystem )
{
	ASSERT_VALID( this );
	if( m_bWatchFileSystem == bWatchFileSystem )
		return;

	m_bWatchFileSystem = bWatchFileSystem;

	if( GetSafeHwnd() == NULL )
		return;
	
	DeleteAllItems();
	UpdateContent( TVI_ROOT, true );
}

void CExtShellTreeCtrl::ShowExtensionsSet(
	bool bShowExtensions,
	bool bRescanNow // = true
	)
{
	ASSERT_VALID( this );
	if(		( m_bShowExtensions && bShowExtensions )
		||	( (!m_bShowExtensions) && (!bShowExtensions) )
		)
		return;
	m_bShowExtensions = bShowExtensions;
	if( ( ! bRescanNow ) || GetSafeHwnd() == NULL )
		return;
	DeleteAllItems();
	UpdateContent( TVI_ROOT, true );
}

bool CExtShellTreeCtrl::ShowExtensionsGet() const
{
	ASSERT_VALID( this );
	return m_bShowExtensions;
}

void CExtShellTreeCtrl::OnInplaceControlComplete(
	__EXT_MFC_SAFE_LPCTSTR strEditedText,
	bool bEditingLabel
	)
{
	ASSERT_VALID( this );
	if( ! bEditingLabel )
	{
		CExtTreeCtrl::OnInplaceControlComplete( strEditedText, bEditingLabel );
		return;
	}
	ASSERT( g_pDesktopFolder != NULL );
	ASSERT( m_pRootFolder != NULL );
	ASSERT( g_pMalloc != NULL );
HTREEITEM hti = GetInPlaceEditedItem();
	if( hti == NULL )
		return;
	//Expand( hti, TVE_COLLAPSE );
HTREEITEM htiParent = GetNextItem( hti, TVGN_PARENT );
TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
CExtSafeString _strEditedText = strEditedText;
	_strEditedText.TrimLeft( _T(" \t\r\n") );
	_strEditedText.TrimRight( _T(" \t\r\n") );
CExtSafeString strCurrentItemText = GetItemText( hti );
	if( strCurrentItemText == LPCTSTR(strEditedText) )
		return;
CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
IShellFolder * pParentFolder = NULL;
LPITEMIDLIST _pidlParent = NULL;
LPITEMIDLIST pidl = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetPtr();
	CExtShellBase::stat_SHBindToParent( pidl, IID_IShellFolder, (void**)&pParentFolder, (LPCITEMIDLIST*)&_pidlParent );
	if( pParentFolder == NULL )
		return;
USES_CONVERSION;
LPITEMIDLIST pidlOut = NULL; // stat_CreateItem( stat_GetItemSize(LPITEMIDLIST(_TII.m_lParam)), m_pMalloc );
LPCWSTR pwstr = T2CW(strEditedText);
HRESULT hr =
		pParentFolder->SetNameOf(
			NULL,
			_pidlParent,
			pwstr,
			0,
			&pidlOut
			);
	pParentFolder->Release();
	if( hr != S_OK )
		return;
CExtPIDL pidlParent;
	if( htiParent != NULL )
	{
		TREEITEMINFO_t & _TII_Parent = TreeItemInfoGet( htiParent );
		CExtShellItemData * pShellItemDataParent = (CExtShellItemData*)_TII_Parent.m_lParam;
		pidlParent = pShellItemDataParent->m_pidlAbsolute.GetPtr();
	}
	else
	{
		CExtShellBase::stat_SHGetFolderLocation( NULL, CSIDL_DESKTOP, 0, 0, pidlParent.GetPtrPtr() );
	}
	pShellItemData->m_pidlAbsolute.Combine( pidlParent, pidlOut );
	if( pidlOut != NULL && LPVOID(pidlOut) != LPVOID(g_pidlDesktop) )
		CExtShellBase::g_pMalloc->Free( pidlOut );
	if( pShellItemData->m_pSWDC != NULL )
	{
		CExtSWDCTI * pSWDC = (CExtSWDCTI*)pShellItemData->m_pSWDC;
		if( pSWDC != NULL )
			pSWDC->m_strFullPath = pShellItemData->m_pidlAbsolute.GetPath();
	}

	SetItemText( hti, strEditedText );
}

bool CExtShellTreeCtrl::_GetContextMenu( HTREEITEM hti, CExtSCM & shellContextMenu )
{
TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
	ASSERT( pShellItemData != NULL );
CExtCIP_SF _SF;
	 _SF.Attach( pShellItemData->m_pidlAbsolute.GetParentFolder() );
	ASSERT( ! _SF.IsEmpty() );
LPITEMIDLIST pidl = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetLast();
	ASSERT( pidl != NULL );
	return shellContextMenu.Create( _SF.GetPtr(), pidl );
}

bool CExtShellTreeCtrl::_GetContextMenu( CList < HTREEITEM, HTREEITEM > & _listSelectedItems, CExtSCM & shellContextMenu )
{
	ASSERT_VALID( this );
INT nItemIndex, nItemCount = INT( _listSelectedItems.GetCount() );
	if( nItemCount == 0 )
		return false;
	// ensure all items have one parent item
POSITION pos = _listSelectedItems.GetHeadPosition();
	ASSERT( pos != NULL );
HTREEITEM hti = _listSelectedItems.GetNext( pos );
	ASSERT( hti != NULL );
TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
	ASSERT( pShellItemData != NULL );
CExtCIP_SF _SF;
	 _SF.Attach( pShellItemData->m_pidlAbsolute.GetParentFolder() );
	ASSERT( ! _SF.IsEmpty() );
	if( nItemCount > 1 )
	{
		HTREEITEM htiParent = GetNextItem( hti, TVGN_PARENT );
		for( ; pos != NULL; )
		{
			hti = _listSelectedItems.GetNext( pos );
			ASSERT( hti != NULL );
			HTREEITEM htiTest = GetNextItem( hti, TVGN_PARENT );
			if( htiTest != htiParent )
				return false;
		}

	}
	// prepare data
CArray < LPITEMIDLIST, LPITEMIDLIST > _arrPidlRelative;
	_arrPidlRelative.SetSize( nItemCount );
	pos = _listSelectedItems.GetHeadPosition();
	for( nItemIndex = 0; nItemIndex < nItemCount; nItemIndex++ )
	{
		ASSERT( pos != NULL );
		hti = _listSelectedItems.GetNext( pos );
		ASSERT( hti != NULL );
		TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
		pShellItemData = (CExtShellItemData*)_TII.m_lParam;
		ASSERT( pShellItemData != NULL );
		LPITEMIDLIST pidl = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetLast();
		ASSERT( pidl != NULL );
		_arrPidlRelative.SetAt( nItemIndex, pidl );
	}
	return shellContextMenu.Create( _SF.GetPtr(), (LPCITEMIDLIST *)_arrPidlRelative.GetData(), INT(_arrPidlRelative.GetSize()) );
}

void CExtShellTreeCtrl::OnShellCommand( UINT nID )
{
HTREEITEM hti = m_htiLastCtxMenu;
	m_htiLastCtxMenu = NULL;
	if( nID == IDM_SHELL_RENAME_COMMAND_ID )
	{
		if( hti != NULL )
			SendMessage( TVM_EDITLABEL, 0, LPARAM( hti ) );
		return;
	}
	m_myShellContextMenu.InvokeCommand( nID );
}


HWND CExtShellTreeCtrl::Shell_GetHWND()
{
	return GetSafeHwnd();
}

LRESULT CExtShellTreeCtrl::OnUpdateItemContent( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	lParam;
CList < HTREEITEM, HTREEITEM > _listFocusPath;
	_CheckDeepFocusBegin( _listFocusPath );
CMap < CExtSWDCTI *, CExtSWDCTI *, bool, bool > _mapDelayedQueries;
bool bTmp = false;
CExtSWDCTI * p = (CExtSWDCTI*)wParam;
	ASSERT( p != NULL );
	_mapDelayedQueries.SetAt( p, false );
	_CleanupUpNeededChildren( p->m_hti );
 	UpdateContent( p->m_hti, false );
	for( MSG _msg; ::PeekMessage( &_msg, m_hWnd, CExtShellBase::g_nMsgUpdateItemContent, CExtShellBase::g_nMsgUpdateItemContent, PM_REMOVE ); )
	{
		p = (CExtSWDCTI*)wParam;
		ASSERT( p != NULL );
		if( _mapDelayedQueries.Lookup( p, bTmp ) )
			continue;
		_mapDelayedQueries.SetAt( p, false );
		_CleanupUpNeededChildren( p->m_hti );
 		UpdateContent( p->m_hti, false );
	}
HTREEITEM hti = _CheckDeepFocusEnd( _listFocusPath );
	if( hti != NULL && hti != GetFocusedItem() )
		FocusItem( hti, true, true );
	return 0L;
}

LRESULT CExtShellTreeCtrl::OnChangeChildrenStateItem( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	lParam;

CExtSWDCTI * p = (CExtSWDCTI*)wParam;
	if( ! HasItem( p->m_hti ) )
		return 0L;

TVITEM _tvi;
	::memset( &_tvi, 0, sizeof(TVITEM) );
	_tvi.hItem = p->m_hti;
	_tvi.mask = TVIF_CHILDREN;
	if( ! GetItem( &_tvi ) )
		return 0L;
TREEITEMINFO_t & _TII = TreeItemInfoGet( p->m_hti );
CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
	ASSERT( pShellItemData != NULL );
int cChildrenOld = _tvi.cChildren;
bool bChanged = pShellItemData->UpdateCachedAttributes();
	DWORD dwAttributes = pShellItemData->m_dwCachedAttributes;
	if( ( dwAttributes & SFGAO_FOLDER ) != 0 )
	{
		_tvi.cChildren = 0;
		if( IncludeFilesGet() )
			_tvi.cChildren = 1;
		else if ( ( dwAttributes & SFGAO_REMOVABLE ) != 0 )
			_tvi.cChildren = 1;
		else
			//_tvi.cChildren = ( ( dwAttributes & SFGAO_HASSUBFOLDER ) != 0 ) ? 1 : 0;
			_tvi.cChildren = ( pShellItemData->m_pidlAbsolute.GetAttributesOf(SFGAO_HASSUBFOLDER) & SFGAO_HASSUBFOLDER ) ? 1 : 0;
	}
	if( cChildrenOld != _tvi.cChildren )
		VERIFY( SetItem( &_tvi ) );
	if( cChildrenOld != _tvi.cChildren || bChanged )
		Invalidate();
	return 0L;
}

HTREEITEM CExtShellTreeCtrl::FindItemByPIDL(
	HTREEITEM htiParent,
	LPCITEMIDLIST pidlAbsolute
	)
{
	ASSERT_VALID( this );
	ASSERT( g_pDesktopFolder != NULL );
	if( pidlAbsolute == NULL )
		return NULL;
	if( GetSafeHwnd() == NULL )
		return NULL;
	if( htiParent == NULL )
		htiParent = GetRootItem();
HTREEITEM htiChild = GetNextItem( htiParent, TVGN_CHILD );
	for( ; htiChild != NULL; htiChild = GetNextItem( htiChild, TVGN_NEXT ) )
	{
		TREEITEMINFO_t & _TII = TreeItemInfoGet( htiChild );
		CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
		HRESULT hr = g_pDesktopFolder->CompareIDs( 0, pidlAbsolute, pShellItemData->m_pidlAbsolute );
		if( hr == 0 )
			return htiChild;
	}
 	return NULL;
}

bool CExtShellTreeCtrl::OnHookSpyKeyMsg(
	MSG * pMSG
	)
{
	if(		pMSG->hwnd == m_hWnd
		&&	( ! m_bHelperShellCtxMenuTracking )
		&&	( ! CExtPopupMenuWnd::IsMenuTracking() )
		&&	( GetStyle() & WS_VISIBLE ) != 0
		&&	::GetFocus() == m_hWnd
		)
	{
		if( pMSG->message == WM_KEYDOWN )
		{
			UINT nChar = UINT(pMSG->wParam);
			switch( nChar )
			{
			case UINT('R'):
				if( ! ( GetAsyncKeyState( VK_CONTROL ) & 0x8000 ) )
					break;
			case VK_F5:
				Refresh();
				return true;
			case VK_RETURN:
				ShellItemExecute( NULL, WM_KEYDOWN );
				return true;
			case VK_DELETE:
			{
				CList < HTREEITEM, HTREEITEM > _listSelectedItems;
				GetSelectedItemsList( _listSelectedItems );
				if( _listSelectedItems.GetCount() == 0 )
					return true;
				HTREEITEM htiSelectAfterDelete = _GetItemToSelectAfterDeletion();
				CExtSafeStringList _listToCopy;
				INT nCountOfTCHARs = 0;
				POSITION pos = _listSelectedItems.GetHeadPosition();
				for( ; pos != NULL; )
				{
					HTREEITEM hti = _listSelectedItems.GetNext( pos );
					TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
					CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
					CString strFullPath = pShellItemData->m_pidlAbsolute.GetPath();
					_listToCopy.AddTail( __EXT_MFC_SAFE_LPCTSTR(LPCTSTR(strFullPath)) );
					INT nStrLen = INT( strFullPath.GetLength() );
					nCountOfTCHARs += nStrLen + 1;
				}
				nCountOfTCHARs += 2;
				TCHAR * pBuffer = new TCHAR[nCountOfTCHARs];
				TCHAR * pWalk = pBuffer;
				::memset( pBuffer, 0, nCountOfTCHARs*sizeof(TCHAR) );
				pos = _listToCopy.GetHeadPosition();
				for( ; pos != NULL; )
				{
					CString strFullPath = _listToCopy.GetNext( pos );
					INT nStrLen = INT( strFullPath.GetLength() );
					::memcpy( pWalk, LPVOID(LPCTSTR(strFullPath)), nStrLen*sizeof(TCHAR) );
					pWalk += nStrLen + 1;
				}
				HWND hWndOwn = GetSafeHwnd();
				// use pBuffer ....
				SHFILEOPSTRUCT shf;
				::memset( &shf, 0, sizeof( shf ) );
				shf.hwnd = hWndOwn;
				shf.wFunc = FO_DELETE;
				shf.pFrom = pBuffer;
				shf.fFlags = FOF_ALLOWUNDO|FOF_SIMPLEPROGRESS; // FOF_NOCONFIRMATION|
				int nSHRetVal = ::SHFileOperation( &shf );
				delete[] pBuffer;
				if( nSHRetVal != 0 || shf.fAnyOperationsAborted )
					htiSelectAfterDelete = NULL;
				if( htiSelectAfterDelete != NULL && HasItem( htiSelectAfterDelete ) )
				{
					SelectItem( htiSelectAfterDelete, true );
					FocusItem( htiSelectAfterDelete, true );
				}
				if( ! ::IsWindow( hWndOwn ) )
					return true;
				if( ::GetFocus() != hWndOwn )
				{
					ActivateTopParent();
					::SetFocus( hWndOwn );
				}
				if( WatchFileSystemGet() )
					return true;
				pos = _listSelectedItems.GetTailPosition(); // do not invoke this loop when WDC thread runs - optimization issue
				for( ; pos != NULL; )
				{
					HTREEITEM hti = _listSelectedItems.GetPrev( pos );
					DeleteItem( hti );
				}
			}
			return true;
			} // switch( nChar )
		}
	}
	return CExtTreeCtrl::OnHookSpyKeyMsg( pMSG );
}

HTREEITEM CExtShellTreeCtrl::_GetItemToSelectAfterDeletion(
	HTREEITEM htiToDelete // = NULL // NULL - get focused item
	)
{
	ASSERT_VALID( this );
HTREEITEM htiFocused = ( htiToDelete != NULL && HasItem( htiToDelete ) ) ? htiToDelete : GetFocusedItem();
HTREEITEM htiNext = htiFocused;
HTREEITEM htiRetVal = NULL;
	for( ; htiNext != NULL; htiNext = GetNextItem( htiNext, TVGN_NEXT ) )
	{
		if( TVIS_SELECTED & GetItemState( htiNext, TVIS_SELECTED ) )
			continue;
		htiRetVal = htiNext;
		break;
	}
	if( htiRetVal != NULL )
		return htiRetVal;
HTREEITEM htiPrevious = htiFocused;
	for( ; htiPrevious != NULL; htiPrevious = GetNextItem( htiPrevious, TVGN_PREVIOUS ) )
	{
		if( TVIS_SELECTED & GetItemState( htiPrevious, TVIS_SELECTED ) )
			continue;
		htiRetVal = htiPrevious;
		break;
	}
	if( htiRetVal != NULL )
		return htiRetVal;
	htiRetVal = GetNextItem( htiFocused, TVGN_PARENT );
	return htiRetVal;
}

bool CExtShellTreeCtrl::ShellItemExecute(
	HTREEITEM hti, // = NULL // NULL means use all selected items
	UINT nMessage
	)
{
	ASSERT_VALID( this );

	if( CExtShellBase::stat_NotifyShellItemExecute( m_hWnd, LPARAM(hti), nMessage ) )
		return true;
	if( hti == NULL )
	{
		CList < HTREEITEM, HTREEITEM > _listSelectedItems;
		GetSelectedItemsList( _listSelectedItems, NULL, true, true );
		INT nCountSelected = INT( _listSelectedItems.GetCount() );
		if( nCountSelected == 0 )
			return false;
		POSITION pos = _listSelectedItems.GetHeadPosition();
		for( ; pos != NULL; )
		{
			HTREEITEM hti = _listSelectedItems.GetNext( pos );
			ASSERT( hti != NULL && HasItem( hti ) );
			if( nCountSelected > 1 )
			{
				TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
				CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
				ASSERT( pShellItemData != NULL );
				pShellItemData->UpdateCachedAttributes();
				DWORD dwAttributes = pShellItemData->m_dwCachedAttributes;
				if( ( dwAttributes & SFGAO_FOLDER ) != 0 )
					continue;
			}
			ShellItemExecute( hti, nMessage );
		}
		return true;
	}
	if( ! HasItem( hti ) )
		return false;
	if( CExtShellBase::stat_NotifyShellItemExecute( m_hWnd, LPARAM(hti), nMessage ) )
		return true;
TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
	ASSERT( pShellItemData != NULL );
	pShellItemData->UpdateCachedAttributes();
DWORD dwAttributes = pShellItemData->m_dwCachedAttributes;
	if(		( dwAttributes & SFGAO_FOLDER ) != 0
		||	! m_bRunItems
		)
		return false;

CString strItem = pShellItemData->m_pidlAbsolute.GetPath();
	///////////////////
UINT nFillMenuFlags = CMF_EXPLORE|CMF_CANRENAME;
CExtCIP_SF _SF;
LPCITEMIDLIST pidl = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetPtr();
	if( pShellItemData->m_pidlAbsolute.IsRoot() )
		_SF = g_pDesktopFolder;
	else if( strItem == _T("..") )
	{
		LPITEMIDLIST pidlUpFolder = NULL;
		pidlUpFolder = stat_GetParentItem( pidl );
		IShellFolder * pShellUpFolder = NULL;
		g_pDesktopFolder->BindToObject( pidlUpFolder, NULL, IID_IShellFolder, (LPVOID*)&pShellUpFolder );
		if( pShellUpFolder == NULL )
			_SF = g_pDesktopFolder;
		else
			_SF.Attach( pShellUpFolder );
	}
	else
	{
		 _SF.Attach( pShellItemData->m_pidlAbsolute.GetParentFolder() );
		ASSERT( ! _SF.IsEmpty() );
	}
	ASSERT( ! _SF.IsEmpty() );
CMenu _menu;
UINT nCommandID = UINT(-1);
	if(		_menu.CreatePopupMenu()
		&&	m_myShellContextMenu.Create( _SF.GetPtr(), pidl )
		&&	m_myShellContextMenu.FillMenu( &_menu, 0, IDM_FIRST_SHELLMENUID, IDM_LAST_SHELLMENUID, nFillMenuFlags )
		&&	( nCommandID = _menu.GetDefaultItem( 0, FALSE ) ) != UINT(-1)
		&&	m_myShellContextMenu.InvokeCommand( nCommandID )
		)
		return true;
	///////////////////
	CString strNameBuffer;
	STRRET strRetData;
	::memset( &strRetData, 0, sizeof(STRRET) );
	strRetData.uType = STRRET_WSTR;
	DWORD _shgdnf = __EXT_MFC_SHGDN_FORPARSING|__EXT_MFC_SHGDN_INFOLDER; //__EXT_MFC_SHGDN_INCLUDE_NONFILESYS; //|__EXT_MFC_SHGDN_FORPARSING;
	_SF->GetDisplayNameOf( pidl, _shgdnf, &strRetData );
	switch( strRetData.uType )
	{
	case STRRET_WSTR:
	{
		USES_CONVERSION;
		strNameBuffer = W2CT(strRetData.pOleStr);
	}
	break;
	case STRRET_CSTR:
	{
		USES_CONVERSION;
		strNameBuffer = A2CT(strRetData.cStr);
		strRetData.pOleStr = NULL;
	}
	break;
	case STRRET_OFFSET:
	{
		USES_CONVERSION;
		strNameBuffer = A2CT(((char*)((LPBYTE)pidl+strRetData.uOffset)));
		strRetData.pOleStr = NULL;
	}
	break;
	}
	if( strRetData.pOleStr != NULL )
		::CoTaskMemFree( strRetData.pOleStr );
	INT nBufferLen = (INT)strNameBuffer.GetLength();
	if(		(		nBufferLen > 4
				&&	strNameBuffer.GetAt(0) == _T(':')
				&&	strNameBuffer.GetAt(1) == _T(':')
				&&	strNameBuffer.GetAt(2) == _T('{')
				&&	strNameBuffer.GetAt(nBufferLen-1) == _T('}')
			)
			||
			(		nBufferLen > 2
				&&	strNameBuffer.GetAt(0) == _T('{')
				&&	strNameBuffer.GetAt(nBufferLen-1) == _T('}')
			)
		)
	{
			::ShellExecute(
				::GetDesktopWindow(),
				g_PaintManager.m_bIsWin2000orLater ? LPCTSTR(NULL) : _T("open"),
				LPCTSTR(strNameBuffer),
				NULL,
				NULL,
				SW_SHOW
				);
		return true;
	}

	LPCTSTR strInvoke = strItem.IsEmpty() ? LPCTSTR(strNameBuffer) : LPCTSTR(strItem);
	if( strInvoke != NULL && _tcslen(strInvoke) > 0 )
		CExtHyperLinkButton::stat_HyperLinkOpen( strInvoke );

	return false;
}

bool CExtShellTreeCtrl::_OnQueryWatchFS( HTREEITEM hti )
{
	ASSERT_VALID( this );
	ASSERT( hti != NULL );
TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
	if( pShellItemData == NULL )
		return false;
	return pShellItemData->m_pidlAbsolute._OnQueryWatchFS();
}

void CExtShellTreeCtrl::_UnregisterItemsFromMap(
	HTREEITEM hti // = NULL
	)
{
	ASSERT_VALID( this );
	if( hti == m_htiLastCtxMenu )
		m_htiLastCtxMenu = NULL;
	CExtTreeCtrl::_UnregisterItemsFromMap( hti );
}

LRESULT CExtShellTreeCtrl::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	switch( message )
	{
	case TVM_DELETEITEM:
	{
		HTREEITEM hti = HTREEITEM(lParam);
		if( hti == NULL || hti == m_htiLastCtxMenu )
			m_htiLastCtxMenu = NULL;
		if( hti != NULL )
		{
			if( hti == GetFocusedItem() )
			{
				TREEITEMINFO_t & _TII = TreeItemInfoGet( hti );
				CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
				CExtSWDCTI * pSWDC = (CExtSWDCTI*)pShellItemData->m_pSWDC;
				pSWDC->WatchDirChanges_Stop();
				hti = GetNextItem( hti, TVGN_PARENT );
				FocusItem( hti, true, true );
				//UpdateContent( hti, false );
				if( GetNextItem( GetNextItem( hti, TVGN_CHILD ), TVGN_NEXT ) == NULL )
				{
					TVITEM _tvi;
					::memset( &_tvi, 0, sizeof(TVITEM) );
					_tvi.hItem = hti;
					_tvi.mask = TVIF_CHILDREN;
					VERIFY( GetItem( &_tvi ) );
					if( _tvi.cChildren != 0 )
					{
						_tvi.cChildren = 0;
						VERIFY( SetItem( &_tvi ) );
					} // if( _tvi.cChildren != 0 )
				} // if( GetNextItem( GetNextItem( hti, TVGN_CHILD ), TVGN_NEXT ) == NULL )
			} // if( hti == GetFocusedItem() )
		} // if( hti != NULL )
	}
	break;
	default:
		if( message == CExtShellBase::g_nMsgDelayFocusPIDL )
		{
			if( ! m_pidlDelayedFocus.IsEmpty() )
				FocusPIDL( m_pidlDelayedFocus.GetPtr() );
			return 0L;
		}
	break;
	}
LRESULT lResult = CExtTreeCtrl::WindowProc( message, wParam, lParam );
//	switch( message )
//	{
//	case WM_GETDLGCODE:
//		lResult |= DLGC_WANTALLKEYS;
	//break;
//	break;
//	}
	return lResult;
}

bool CExtShellTreeCtrl::OnTreeMouseClick(
	HTREEITEM hti,
	DWORD dwHitTestFlags,
	int nMouseButton,
	int nClick,
	UINT nMouseEventFlags,
	CPoint point
	)
{
	ASSERT_VALID( this );
	if(		( dwHitTestFlags & (__EXT_TVHT_ONITEMLABEL|__EXT_TVHT_ONITEMICON|__EXT_TVHT_ONITEMINDENT) ) != 0
		&&	( nMouseButton == VK_LBUTTON )
		&&	( nClick == 2 )
		)
	{
		if( ShellItemExecute( NULL, WM_LBUTTONDBLCLK ) )
			return true;
	}

	return CExtTreeCtrl::OnTreeMouseClick( hti, dwHitTestFlags, nMouseButton, nClick, nMouseEventFlags, point );
}


void CExtShellTreeCtrl::OnTreeItemUnRegisterData(
	TREEITEMINFO_t & _TII
	)
{
	ASSERT_VALID( this );
CExtShellItemData * pShellItemData = (CExtShellItemData *)_TII.m_lParam;
	if( pShellItemData != NULL )
	{
		delete pShellItemData;
		_TII.m_lParam = LPARAM(NULL);
	}
}

void CExtShellTreeCtrl::OnTreeItemContextMenu(
	HTREEITEM hti,
	CPoint point, // client coordinates
	DWORD dwHitTestFlags
	)
{
	ASSERT_VALID( this );
	ASSERT( hti != NULL );
	if( m_bHelperShellCtxMenuTracking )
		return;
	if( ! ShowShellContextMenusGet() )
		return;
	if( ( dwHitTestFlags & __EXT_TVHT_ONITEM ) == 0 )
		return;
CList < HTREEITEM, HTREEITEM > _listSelectedItems;
	if( IsItemSelected( hti ) )
	{
		GetSelectedItemsList( _listSelectedItems, NULL, true, true );
		if( _listSelectedItems.GetCount() == 0 )
			return;
	}
	else
		_listSelectedItems.AddTail( hti );

UINT nFillMenuFlags = CMF_EXPLORE|CMF_CANRENAME|CMF_NODEFAULT;
	if( CExtPopupMenuWnd::IsKeyPressed( VK_SHIFT ) )
		nFillMenuFlags |= __EXT_MFC_CMF_EXTENDEDVERBS;
	m_myShellContextMenu.SetOwner( this );
BOOL bPopup = ( GetAsyncKeyState(VK_CONTROL) & 0x8000 );
CMenu _menu;
	if(		_menu.CreatePopupMenu()
		&&	_GetContextMenu( _listSelectedItems, m_myShellContextMenu )
		)
	{
		if( ! m_myShellContextMenu.FillMenu( &_menu, 0, IDM_FIRST_SHELLMENUID, IDM_LAST_SHELLMENUID, nFillMenuFlags ) )
		{
			if( bPopup )
				_menu.AppendMenu(MF_GRAYED, __EXT_MFC_SHELL_MENU_RESOURCE_ID, CExtShellBase::g_strTextOfMenuItemInEmptyShellContextMenu );
		}
		ClientToScreen( &point );
		m_htiLastCtxMenu = hti;
		if( ( GetStyle() & TVS_EDITLABELS ) == 0  )
			_menu.RemoveMenu( IDM_SHELL_RENAME_COMMAND_ID, MF_BYCOMMAND );
		m_bHelperShellCtxMenuTracking = true;
		UINT nRetCmd = (UINT)_menu.TrackPopupMenu( TPM_LEFTALIGN|TPM_RIGHTBUTTON|TPM_RETURNCMD, point.x, point.y, this );
		if( IDM_FIRST_SHELLMENUID <= nRetCmd && nRetCmd <= IDM_LAST_SHELLMENUID )
			SendMessage( WM_COMMAND, WPARAM(nRetCmd) );
		m_bHelperShellCtxMenuTracking = false;
	}
	m_myShellContextMenu.SetOwner( NULL );
}

void CExtShellTreeCtrl::Refresh(
	HTREEITEM hti // = TVI_ROOT
	)
{
	ASSERT_VALID( this );

	if( hti == TVI_ROOT )
	{
		hti = GetNextItem( hti, TVGN_CHILD );
		if( hti == NULL )
			return;
	}

	if( TreeItemIsExpanded( hti ) )
		UpdateContent( hti, false );

	HTREEITEM htiChild = GetNextItem( hti, TVGN_CHILD );

	if( htiChild != NULL )
		Refresh( htiChild );

	HTREEITEM htiNext = GetNextItem( hti, TVGN_NEXT );

	if( htiNext != NULL )
		Refresh( htiNext );

	return;
}

void CExtShellTreeCtrl::_DeleteNotExistingItems( HTREEITEM htiParent )
{
HTREEITEM htiChild = GetNextItem( htiParent, TVGN_CHILD );
	if( htiChild == NULL )
		return;

HTREEITEM htiNext = htiChild;
TCHAR szItemPath[_MAX_PATH];

	for( ; htiNext != NULL; )
	{
		TREEITEMINFO_t & _TII = TreeItemInfoGet( htiNext );
		CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
		LPITEMIDLIST pidlItem = (LPITEMIDLIST)pShellItemData->m_pidlAbsolute.GetPtr();

		::SHGetPathFromIDList( pidlItem, szItemPath );

		HTREEITEM htiControl = NULL;
		if( ! CExtShellBase::stat_ValidPath( szItemPath ) )
		{
			htiControl = GetNextItem( htiNext, TVGN_PREVIOUS );

			bool _bNext = false;
			if( htiControl == NULL )
			{
				htiControl = GetNextItem( htiNext, TVGN_NEXT );
				_bNext = true;
			}

			DeleteItem( htiNext );

			if( htiControl == NULL )
				return;

			htiNext = htiControl;

			if( _bNext )
				continue;
		}

		htiNext = GetNextItem( htiNext, TVGN_NEXT );
	}
}

//	bool CExtShellTreeCtrl::DragEnabledGet() const
//	{
//		ASSERT_VALID( this );
//		return m_bDragEnabled;
//	}
//
//	void CExtShellTreeCtrl::DragEnagleSet( bool bDragEnabled )
//	{
//		ASSERT_VALID( this );
//		m_bDragEnabled = bDragEnabled;
//	}
//
//	bool CExtShellTreeCtrl::DropEnabledGet() const
//	{
//		ASSERT_VALID( this );
//		return m_bDropEnabled;
//	}
//
//	void CExtShellTreeCtrl::DropEnagledSet( bool bDropEnabled )
//	{
//		ASSERT_VALID( this );
//		m_bDropEnabled = bDropEnabled;
//	}

bool CExtShellTreeCtrl::RunItemsGet() const
{
	ASSERT_VALID( this );
	return m_bRunItems;
}

void CExtShellTreeCtrl::RunItemsSet( bool bRunItems )
{
	ASSERT_VALID( this );
	m_bRunItems	= bRunItems;
}

bool CExtShellTreeCtrl::ShowShellContextMenusGet() const
{
	ASSERT_VALID( this );
	return m_bShowShellContextMenus;
}

void CExtShellTreeCtrl::ShowShellContextMenusSet( bool bShowShellContextMenus )
{
	ASSERT_VALID( this );
	m_bShowShellContextMenus = bShowShellContextMenus;
}

#endif //  ( ! defined __EXT_MFC_NO_SHELL_TREE_VIEW_CTRL )

#if ( ! defined __EXT_MFC_NO_SHELL_DIALOG_BFF )

/////////////////////////////////////////////////////////////////////////////
// CExtShellDialogBrowseFor dialog

CExtShellDialogBrowseFor::CExtShellDialogBrowseFor(
		CWnd * pParent // = NULL
		)
	:
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		(
		IDD_EXT_DIALOG_BROWSE_FOR, // CExtShellDialogBrowseFor::IDD
		pParent
		)
	, m_bShowFolderEdit( false )
	, m_bShowMakeNewFolderButton( false )
	, m_bShowFullPath( false )
	, m_bCurrentDirectorySetInitially( true )
	, m_bCurrentDirectorySetOnWalk( true )
	, m_bCurrentDirectoryRestoreFinallyOnOK( false )
	, m_bCurrentDirectoryRestoreFinallyOnCancel( false )
	, m_bSaveRestoreShellLocation( true )
	, m_bSaveRestoreWindowPosition( true )
	, m_bHelperEnableShellAutoCompleteInEditor( true )
	, m_dwHelperFlagsSHACFforEditor(
			__EXT_MFC_SHACF_FILESYSTEM
				|__EXT_MFC_SHACF_AUTOSUGGEST_FORCE_ON
				|__EXT_MFC_SHACF_AUTOAPPEND_FORCE_ON
				|__EXT_MFC_SHACF_USETAB
			)
	, m_bHelperAutoCompleteInEditPerformed( false )
{
	//{{AFX_DATA_INIT(CExtShellDialogBrowseFor)
	//}}AFX_DATA_INIT
	SetAutoSubclassChildControls();
	
	m_wndShellTree.m_dwAttributeFilterAllPresent = SFGAO_FILESYSANCESTOR;
}

void CExtShellDialogBrowseFor::DoDataExchange(CDataExchange* pDX)
{
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExtShellDialogBrowseFor)
	DDX_Control(pDX, IDC_EXT_BFF_STATIC_AT_TOP, m_labelAtTop);
	DDX_Control(pDX, IDC_EXT_BFF_STATIC_AT_BOTTOM, m_labelAtBottom);
	DDX_Control(pDX, IDC_EXT_BFF_EDIT, m_edit);
	DDX_Control(pDX, ID_EXT_BFF_MAKE_NEW_FOLDER, m_buttonMakeNewFolder);
	DDX_Control(pDX, IDCANCEL, m_buttonCancel);
	DDX_Control(pDX, IDOK, m_buttonOK);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_EXT_TREE_SHELL, m_wndShellTree);
}


BEGIN_MESSAGE_MAP( CExtShellDialogBrowseFor, CExtResizableDialog )
	//{{AFX_MSG_MAP(CExtShellDialogBrowseFor)
	ON_EN_CHANGE(IDC_EXT_BFF_EDIT, OnChangeExtBffEdit)
	ON_BN_CLICKED(ID_EXT_BFF_MAKE_NEW_FOLDER, OnExtBffMakeNewFolder)
	ON_NOTIFY(TVN_SELCHANGED, IDC_EXT_TREE_SHELL, OnSelChangedExtTreeShell)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE( CExtTreeCtrl::g_nMsgTreeItemDelayedFocus, OnShellTreeDelayedItemFocus )
	ON_REGISTERED_MESSAGE( CExtShellBase::g_nMsgShellItemExecute, OnShellItemExecute )
END_MESSAGE_MAP()

void CExtShellDialogBrowseFor::_GetSettingsLocationStrings(
	CExtSafeString & strSectionSettingsEntry,
	CExtSafeString & strSectionNameCompany,
	CExtSafeString & strSectionNameProduct
	) const
{
	ASSERT_VALID( this );
	strSectionSettingsEntry.Empty();
	strSectionNameCompany.Empty();
	strSectionNameProduct.Empty();

	if( m_strSectionSettingsEntry.IsEmpty() )
	{
		strSectionSettingsEntry += _T("ShellBrowseForDialog-");
//strSectionSettingsEntry += m_comboFileTypes.GetFilterHash();
	}
	else
		strSectionSettingsEntry = m_strSectionSettingsEntry;

CWinApp * pApp = ::AfxGetApp();
	ASSERT_VALID( pApp );
	if( m_strSectionNameCompany.IsEmpty() )
	{
		strSectionNameCompany = pApp->m_pszRegistryKey;
		if( strSectionNameCompany.IsEmpty() )
			strSectionNameCompany = _T("Prof-UIS");
	}
	else
		strSectionNameCompany = m_strSectionNameCompany;

	if( m_strSectionNameProduct.IsEmpty() )
	{
		strSectionNameProduct = pApp->m_pszProfileName;
		if( strSectionNameProduct.IsEmpty() )
			strSectionNameProduct = _T("Shell-BrowseFor-Dialog");
	}
	else
		strSectionNameProduct = m_strSectionNameProduct;
}


/////////////////////////////////////////////////////////////////////////////
// CExtShellDialogBrowseFor message handlers

BOOL CExtShellDialogBrowseFor::OnInitDialog() 
{
	ASSERT_VALID( this );
CExtSafeString strSectionSettingsEntry, strSectionNameCompany, strSectionNameProduct;
	_GetSettingsLocationStrings( strSectionSettingsEntry, strSectionNameCompany, strSectionNameProduct );
CExtSafeString strInitialFodler = m_strRetValPath;
	m_strRetValPath.Empty();
CExtPIDL pidlFolderToSelectInitially = m_pidlResult;
	if( ! CExtShellBase::stat_ValidPath( pidlFolderToSelectInitially.GetPath() ) )
		pidlFolderToSelectInitially.Empty();
	m_pidlResult.Empty();
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		::OnInitDialog();

	m_wndShellTree.ModifyStyle( TVS_SINGLEEXPAND, TVS_TRACKSELECT|TVS_EDITLABELS|TVS_SHOWSELALWAYS );

CRect rcTree;
	m_wndShellTree.GetWindowRect( &rcTree );
	ScreenToClient( &rcTree );
CRect rcTreeInitial = rcTree;

	if( m_strLabelAtTop.IsEmpty() )
	{
		CRect rcLabelAtTop;
		m_labelAtTop.GetWindowRect( &rcLabelAtTop );
		ScreenToClient( &rcLabelAtTop );

		rcTree.top = rcLabelAtTop.top;
		m_labelAtTop.ShowWindow( SW_HIDE );
	}
	else
		m_labelAtTop.SetWindowText( LPCTSTR(m_strLabelAtTop) );

	if( ! m_strCustomDialogCaption.IsEmpty() )
		SetWindowText( LPCTSTR(m_strCustomDialogCaption) );

	if( ! m_strCustomOkButtonCaption.IsEmpty() )
		m_buttonOK.SetWindowText( LPCTSTR(m_strCustomOkButtonCaption) );
	if( ! m_strCustomCancelButtonCaption.IsEmpty() )
		m_buttonCancel.SetWindowText( LPCTSTR(m_strCustomCancelButtonCaption) );
	if( ! m_strCustomNewFolderButtonCaption.IsEmpty() )
		m_buttonMakeNewFolder.SetWindowText( LPCTSTR(m_strCustomNewFolderButtonCaption) );
	if( ! m_strCustomEditLabelCaption.IsEmpty() )
		m_labelAtBottom.SetWindowText( LPCTSTR(m_strCustomEditLabelCaption) );

bool _bShowMakeNewFolderButton = m_bShowMakeNewFolderButton;
	if( ! m_bShowFolderEdit )
	{
		_bShowMakeNewFolderButton = false;

		CRect rcEdit;
		m_edit.GetWindowRect( &rcEdit );
		ScreenToClient( &rcEdit );

		rcTree.bottom = rcEdit.bottom;
		m_edit.ShowWindow( SW_HIDE );
		m_labelAtBottom.ShowWindow( SW_HIDE );
	}

	if( ! _bShowMakeNewFolderButton )
		m_buttonMakeNewFolder.ShowWindow( SW_HIDE );

	if( rcTreeInitial != rcTree )
		m_wndShellTree.MoveWindow( &rcTree );

	if( ! m_pidlRoot.IsEmpty() )
		m_wndShellTree.RefreshShellRoot( m_pidlRoot.GetPtr() );
	else
		m_wndShellTree.RefreshShellRoot();

// 	if( pidlToFocusInitially.IsEmpty() || (! pidlToFocusInitially.IsFolder() ) )
// 	{
// 		pidlToFocusInitially.Empty();
// 		pidlToFocusInitially.GetSpecialFolderLocation( m_hWnd, CSIDL_DRIVES );
// 	}
// 	if( ! pidlToFocusInitially.IsEmpty() )
// 	{
// 		HTREEITEM hti = m_wndShellTree.FindItemByPIDL( NULL, pidlToFocusInitially );
// 		if( hti == NULL )
// 			hti = m_wndShellTree.FocusPIDL( (LPCITEMIDLIST)pidlToFocusInitially, true, true );
// 		else
// 			m_wndShellTree.FocusItem( hti, true, true );
// 
// 		if( hti != NULL )
// 			m_wndShellTree.Expand( hti, TVE_EXPAND );
// 	}

TCHAR strCurrentDirectory[ _MAX_PATH + 1 ];
	::memset( strCurrentDirectory, 0, _MAX_PATH + 1 );
	::GetCurrentDirectory( _MAX_PATH, strCurrentDirectory );

	if( m_bCurrentDirectoryRestoreFinallyOnOK || m_bCurrentDirectoryRestoreFinallyOnCancel )
		m_strCurrentDirectoryToRestore = strCurrentDirectory;
	else
		m_strCurrentDirectoryToRestore.Empty();


bool bSetFocusFirstRootChildren = true;
	if( m_bSaveRestoreShellLocation )
	{
		CExtPIDL pidl;
		if(		pidl.Serialize( LPCTSTR(strSectionSettingsEntry), LPCTSTR(strSectionNameCompany), LPCTSTR(strSectionNameProduct), false )
			&&	(! pidl.IsEmpty() )
			)
		{
			pidlFolderToSelectInitially.Empty();
			pidlFolderToSelectInitially.Attach( pidl.Detach() );
			if( ! CExtShellBase::stat_ValidPath( pidlFolderToSelectInitially.GetPath() ) )
				pidlFolderToSelectInitially.Empty();
			else
				bSetFocusFirstRootChildren = false;
		}
	}
	if( bSetFocusFirstRootChildren && m_bCurrentDirectorySetInitially )
	{
		if( m_wndShellTree.FocusPath( strCurrentDirectory ) != NULL )
		{
			bSetFocusFirstRootChildren = false;
			pidlFolderToSelectInitially.Empty();
		}
	}
	if(		bSetFocusFirstRootChildren
		&&	( pidlFolderToSelectInitially.IsEmpty() || ( ! pidlFolderToSelectInitially.IsFolder() ) )
		&&	( ! strInitialFodler.IsEmpty() )
		)
	{
		CExtPIDL pidlVerify;
		if( pidlVerify.FromFolder( LPCTSTR(strInitialFodler), m_hWnd ) )
		{
			if( (! pidlVerify.IsEmpty()  ) && pidlVerify.IsFolder() )
			{
				pidlFolderToSelectInitially.Empty();
				pidlFolderToSelectInitially.Attach( pidlVerify.Detach() );
				if( ! CExtShellBase::stat_ValidPath( pidlFolderToSelectInitially.GetPath() ) )
					pidlFolderToSelectInitially.Empty();
				else
					bSetFocusFirstRootChildren = false;
			}
		}
	}
	if( bSetFocusFirstRootChildren && pidlFolderToSelectInitially.IsEmpty() )
	{
		if( pidlFolderToSelectInitially.GetSpecialFolderLocation( m_hWnd, CSIDL_DRIVES ) )
			bSetFocusFirstRootChildren = false;
	}
	if( ! pidlFolderToSelectInitially.IsEmpty() )
	{
		HTREEITEM hti = m_wndShellTree.FindItemByPIDL( NULL, pidlFolderToSelectInitially );
		if( hti != NULL )
		{
			bSetFocusFirstRootChildren = false;
			m_wndShellTree.FocusItem( hti, true, true );
			m_wndShellTree.Expand( hti, TVE_EXPAND );
		}
		else
		{
			hti = m_wndShellTree.FocusPIDL( pidlFolderToSelectInitially, true, true, true );
			if( hti != NULL )
			{
				bSetFocusFirstRootChildren = false;
				m_wndShellTree.Expand( hti, TVE_EXPAND );
			}
		}
	}
	if( bSetFocusFirstRootChildren )
	{
		m_wndShellTree.FocusItem( m_wndShellTree.GetChildItem( TVI_ROOT ), true, false, true );
	}

	m_bHelperAutoCompleteInEditPerformed = false;
	if( m_bHelperEnableShellAutoCompleteInEditor && m_edit.GetSafeHwnd() != NULL )
		CExtShellBase::stat_SHAutoComplete( m_edit.m_hWnd, m_dwHelperFlagsSHACFforEditor );

	AddAnchor( m_labelAtTop.m_hWnd, __RDA_LT, __RDA_RT );
	AddAnchor( m_labelAtBottom.m_hWnd, __RDA_LB );
	AddAnchor( m_edit.m_hWnd, __RDA_LB, __RDA_RB );
	AddAnchor( m_buttonMakeNewFolder.m_hWnd, __RDA_LB );
	AddAnchor( m_buttonOK.m_hWnd, __RDA_RB );
	AddAnchor( m_buttonCancel.m_hWnd, __RDA_RB );
	AddAnchor( m_wndShellTree.m_hWnd, __RDA_LT, __RDA_RB );

	if( m_bSaveRestoreWindowPosition )
		EnableSaveRestore( _T("ShellBrowseForDialog"), LPCTSTR(strSectionSettingsEntry) );

	return TRUE;
}

void CExtShellDialogBrowseFor::OnOK() 
{
	ASSERT_VALID( this );
	if( m_bSaveRestoreShellLocation )
	{
		CExtSafeString strSectionSettingsEntry, strSectionNameCompany, strSectionNameProduct;
		_GetSettingsLocationStrings( strSectionSettingsEntry, strSectionNameCompany, strSectionNameProduct );
		HTREEITEM hti = m_wndShellTree.GetFocusedItem();
		if( hti != NULL )
		{
			CExtTreeCtrl::TREEITEMINFO_t & _TII = m_wndShellTree.TreeItemInfoGet( hti );
			CExtShellItemData * pData = (CExtShellItemData*)_TII.m_lParam;
			ASSERT( pData != NULL );
			if( ! pData->m_pidlAbsolute.IsEmpty() )
			{
				VERIFY( pData->m_pidlAbsolute.Serialize( LPCTSTR(strSectionSettingsEntry), LPCTSTR(strSectionNameCompany), LPCTSTR(strSectionNameProduct), true ) );
			}
		}
	}
	else if(	m_bCurrentDirectoryRestoreFinallyOnOK
			&&	(! m_strCurrentDirectoryToRestore.IsEmpty() )
			)
			SetCurrentDirectory( LPCTSTR(m_strCurrentDirectoryToRestore) );
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		::OnOK();
	m_strCurrentDirectoryToRestore.Empty();
}

void CExtShellDialogBrowseFor::OnCancel() 
{
	ASSERT_VALID( this );
	if( m_bSaveRestoreShellLocation )
	{
		CExtSafeString strSectionSettingsEntry, strSectionNameCompany, strSectionNameProduct;
		_GetSettingsLocationStrings( strSectionSettingsEntry, strSectionNameCompany, strSectionNameProduct );
		HTREEITEM hti = m_wndShellTree.GetFocusedItem();
		if( hti != NULL )
		{
			CExtTreeCtrl::TREEITEMINFO_t & _TII = m_wndShellTree.TreeItemInfoGet( hti );
			CExtShellItemData * pData = (CExtShellItemData*)_TII.m_lParam;
			ASSERT( pData != NULL );
			if( ! pData->m_pidlAbsolute.IsEmpty() )
			{
				VERIFY( pData->m_pidlAbsolute.Serialize( LPCTSTR(strSectionSettingsEntry), LPCTSTR(strSectionNameCompany), LPCTSTR(strSectionNameProduct), true ) );
			}
		}
	}
	else if(	m_bCurrentDirectoryRestoreFinallyOnCancel
			&&	(! m_strCurrentDirectoryToRestore.IsEmpty() )
			)
			SetCurrentDirectory( LPCTSTR(m_strCurrentDirectoryToRestore) );
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		::OnCancel();
	m_strCurrentDirectoryToRestore.Empty();
	m_pidlResult.Empty();
	m_strDisplayNameResult.Empty();
	m_strRetValPath.Empty();
}

LRESULT CExtShellDialogBrowseFor::OnShellItemExecute( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	wParam;
CExtShellBase::NotifyShellItemExecuteData & _NSIED = CExtShellBase::NotifyShellItemExecuteData::FromLPARAM( lParam );
	if( _NSIED.m_hWndControl != m_wndShellTree.GetSafeHwnd() )
		return 0L;
HTREEITEM hti = (HTREEITEM)_NSIED.m_lParamItemID;
	if( hti == NULL )
		return 0L;
	if( _NSIED.m_nMessage != WM_KEYDOWN )
		return 0L;
	_NSIED.m_bHandled = true;
	SendMessage( WM_COMMAND, IDOK );
	return 0L;
}

LRESULT CExtShellDialogBrowseFor::OnShellTreeDelayedItemFocus( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
CExtShellTreeCtrl * pST = (CExtShellTreeCtrl*)wParam;
 	ASSERT( pST != NULL );
 	if( pST->m_hWnd != m_wndShellTree.m_hWnd )
 		return 0L;
	if( ( lParam & CExtTreeCtrl::__PARM_DELAYED_FOCUS_BEGIN_WAITING ) != 0 )
	{
		if( ( lParam & CExtTreeCtrl::__PARM_DELAYED_FOCUS_MOUSE_CLICK ) == 0 )
			return 0L;
	}
HTREEITEM htiSelect = m_wndShellTree.GetFocusedItem();
 	ASSERT( htiSelect != NULL );
CExtTreeCtrl::TREEITEMINFO_t & _TII = m_wndShellTree.TreeItemInfoGet( htiSelect );
CExtShellItemData * pData = (CExtShellItemData*)_TII.m_lParam; //new CExtShellItemData;
	ASSERT( pData != NULL );
CExtSafeString strFullPath = pData->m_pidlAbsolute.GetPath();

	if( ! m_bHelperAutoCompleteInEditPerformed )
	{
		if( ! m_bShowFullPath )
			m_edit.SetWindowText( m_wndShellTree.GetItemText( htiSelect ) );
		else
		{
			if( ! strFullPath.IsEmpty() )
				m_edit.SetWindowText( strFullPath );
			else
				m_edit.SetWindowText( m_wndShellTree.GetItemText( htiSelect ) );
		}
	}

	if( m_bCurrentDirectorySetOnWalk )
	{
		if( ! strFullPath.IsEmpty() )
			SetCurrentDirectory( LPCTSTR(strFullPath) );
	}

	return 0L;
}

void CExtShellDialogBrowseFor::OnChangeExtBffEdit() 
{
	ASSERT_VALID( this );
	m_bHelperAutoCompleteInEditPerformed = false;
	if( m_bHelperEnableShellAutoCompleteInEditor && m_wndShellTree.GetSafeHwnd() != NULL )
	{
		ASSERT( m_edit.GetSafeHwnd() != NULL );
		CString strText;
		m_edit.GetWindowText( strText );
		if( CExtShellBase::stat_ValidPath( LPCTSTR(strText) ) )
		{
			CExtPIDL _pidl;
			if( _pidl.FromFolder( LPCTSTR(strText), m_hWnd ) )
			{
				m_bHelperAutoCompleteInEditPerformed = true;
				m_wndShellTree.FocusPIDL( _pidl );
			}
		} // if( CExtShellBase::stat_ValidPath( LPCTSTR(strText) ) )
	} // if( m_bHelperEnableShellAutoCompleteInEditor && m_wndShellTree.GetSafeHwnd() != NULL )
}

void CExtShellDialogBrowseFor::OnExtBffMakeNewFolder() 
{
	ASSERT_VALID( this );
HTREEITEM hti = m_wndShellTree.GetFocusedItem();
	if( hti == NULL )
		return;
CExtTreeCtrl::TREEITEMINFO_t & _TII = m_wndShellTree.TreeItemInfoGet( hti );
CExtShellItemData * pData = (CExtShellItemData*)_TII.m_lParam;
	ASSERT( pData != NULL );
	if( pData->m_pidlAbsolute.IsEmpty() )
		return;
	// alw 07/16/2013 Will not update string otherwise
	//if( ! pData->m_pidlAbsolute.IsFolder() )
	//	return;
CExtSafeString strFocusedPath = pData->m_pidlAbsolute.GetPath();
	if( strFocusedPath.IsEmpty() )
		return;
	if( ! CExtShellBase::stat_DirExist( LPCTSTR(strFocusedPath) ) )
	{
	//	return;
		CExtSafeString str;
		str.Format( _T("%s*.*"), LPCTSTR(strFocusedPath) );
		if( ! CExtShellBase::stat_DirExist( LPCTSTR(str) ) )
			return;
	}

CString str;
	m_edit.GetWindowText( str );
	str.TrimLeft( _T(" \r\n\t") );
	str.TrimRight( _T(" \r\n\t") );
	if( str.IsEmpty() )
		return;
TCHAR strFullPath[ _MAX_PATH + 1 ], strCurrentDirectory[ _MAX_PATH + 1 ];
	::memset( strFullPath, 0, _MAX_PATH + 1 );
	::memset( strCurrentDirectory, 0, _MAX_PATH + 1 );
	::GetCurrentDirectory( _MAX_PATH, strCurrentDirectory );
	if( ! ::SetCurrentDirectory( LPCTSTR(strFocusedPath) ) )
		return;
	if( ! ::AfxFullPath( strFullPath, LPCTSTR(str) ) )
		return;
	::SetCurrentDirectory( strCurrentDirectory );
	if( ! CExtShellBase::stat_CreateFolder( strFullPath ) )
	{
		return;
	}
	// focus it in tree
	if( m_wndShellTree.TreeItemIsExpanded( hti ) )
		m_wndShellTree.UpdateContent( hti, false );
	else
		m_wndShellTree.Expand( hti, TVE_EXPAND );
HTREEITEM htiNewItem = m_wndShellTree.FindItemByText( hti, LPCTSTR(str) );
	if( htiNewItem != NULL )
	{
		m_wndShellTree.FocusItem( htiNewItem, true, true );
		m_wndShellTree.Invalidate();
	}
	else
		m_wndShellTree.FocusPath( strFullPath );
	m_wndShellTree.UpdateWindow();
}

void CExtShellDialogBrowseFor::OnSelChangedExtTreeShell(NMHDR* pNMHDR, LRESULT* pResult) 
{
	ASSERT_VALID( this );
//	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	pNMHDR;
HTREEITEM hti = m_wndShellTree.GetFocusedItem();
CExtTreeCtrl::TREEITEMINFO_t & _TII = m_wndShellTree.TreeItemInfoGet( hti );
CExtShellItemData * pShellItemData = (CExtShellItemData*)_TII.m_lParam;
	ASSERT( pShellItemData != NULL );
	m_pidlResult = pShellItemData->m_pidlAbsolute;
	m_strDisplayNameResult = pShellItemData->m_pidlAbsolute.GetDisplayNameOf();
	// alw 07/16/2013
	m_strRetValPath = pShellItemData->m_pidlAbsolute.GetPath();
	if(m_bShowFullPath==TRUE && m_bShowFolderEdit==TRUE)  //WDO 7/11/13
		m_edit.SetWindowText(m_strRetValPath);  //WDO 7/11/13
	*pResult = 0;
}

#endif //  ( ! defined __EXT_MFC_NO_SHELL_DIALOG_BFF )

#if ( ! defined __EXT_MFC_NO_SHELL_DIALOG_FILE )

/////////////////////////////////////////////////////////////////////////////
// CExtShellDialogCreateFolder dialog

CExtShellDialogCreateFolder::CExtShellDialogCreateFolder(
		CWnd * pParent // = NULL
		)
	: 
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		(
		IDD_EXT_DIALOG_SHELL_CREATE_FOLDER, // CExtShellDialogCreateFolder::IDD
		pParent
		)
{
	//{{AFX_DATA_INIT(CExtShellDialogCreateFolder)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CExtShellDialogCreateFolder::DoDataExchange( CDataExchange* pDX )
{
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
	::DoDataExchange( pDX );
	//{{AFX_DATA_MAP(CExtShellDialogCreateFolder)
	DDX_Control(pDX, IDOK, m_buttonOk);
	DDX_Control(pDX, IDCANCEL, m_buttonCancel);
	DDX_Control(pDX, IDC_EXT_STATIC_NAME_FOLDER, m_staticNameFolder);
	DDX_Control(pDX, IDC_EXT_EDIT_NAME_FOLDER, m_editNameFolder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP( CExtShellDialogCreateFolder, CExtResizableDialog )
	//{{AFX_MSG_MAP(CExtShellDialogCreateFolder)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtShellDialogCreateFolder message handlers

BOOL CExtShellDialogCreateFolder::OnInitDialog() 
{
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		::OnInitDialog();
	m_strNameFolder.Empty();
	//m_editNameFolder.SetFocus();
	PostMessage( WM_NEXTDLGCTL, 1L, 0L ); // set focus to editor
	return TRUE;
}

void CExtShellDialogCreateFolder::OnOK() 
{
CString strName;
	m_editNameFolder.GetWindowText( strName );

	if( strName.IsEmpty() )
	{
#if (!defined __EXT_MFC_NO_MSG_BOX)
		::ProfUISMsgBox( GetSafeHwnd(), IDS_EXT_EMPTY_FOLDER_NAME_MSG_BOX_TEXT, NULL, MB_OK );
#else
		::AfxMessageBox( GetSafeHwnd(), IDS_EXT_EMPTY_FOLDER_NAME_MSG_BOX_TEXT, NULL, MB_OK );
#endif
		m_strNameFolder.Empty();
		return;
	}

	m_strNameFolder = LPCTSTR(strName);

#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		::OnOK();
}

void CExtShellDialogCreateFolder::OnCancel()
{
	m_strNameFolder.Empty();
	
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		::OnCancel();
}

__EXT_MFC_SAFE_LPCTSTR CExtShellDialogCreateFolder::GetNameFolder()
{
	ASSERT_VALID( this );
	return LPCTSTR( m_strNameFolder );
}

/////////////////////////////////////////////////////////////////////////////
// CExtShellDialogFile dialog

CExtShellDialogFile::CExtShellDialogFile(
	CWnd* pParent, // = NULL
	CExtShellDialogFile::e_file_dialog_type_t _eFDT // = CExtShellDialogFile::__EFDT_OPEN_SINGLE
	)
	:
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		(
		IDD_EXT_DIALOG_FILE, // CExtShellDialogFile::IDD
		pParent
		)
	, m_eFDT( _eFDT )
	, m_bHaldingSelectionInTree( false )
	, m_bHaldingSelectionInList( false )
	, m_bLockChangingsValidation( false )
	, m_bHaldingSelectionInCombo( false )
	, m_bHaldingHistory( false )
	, m_nCurrentHistoryPosition( -1 )
	//, m_bCheckSelectedFilesExist( true )
	, m_nValidateNamesTimerID( 999 )
	, m_nValidateNamesTimerPeriod( 100 )
	, m_nValidateListViewSelectionTimerID( 998 )
	, m_nValidateListViewSelectionTimerPeriod( 1 )
	, m_nSavedCollapsedHeight( 0 )
	, m_nCollapsedWindowHeight( 0 )
	, m_bHelperValidatingEditor( false )
	, m_bHelperValidatingListViewSelection( false )
	, m_nInitialListViewMode( LVS_LIST )
	, m_nInitialFileTypeIndex( -1 )
	, m_bEnableCollapsing( false )
	, m_bCollapseInitially( false )
	, m_bShowFileNamesEdit( true )
	, m_bReadOnlyFileNamesEdit( false )
	, m_bShowFileTypesComboBox( true )
	, m_bPathMustExist( false )
	, m_bFilesMustExist( false )
	, m_bCheckReadAccess( false )
	, m_bCheckWriteAccess( false )
	, m_bPromptOverwrite( true ) // used in save dialogs only
	, m_bResolveLinks( true )
	, m_bHelperCollapsignAnimation( false )
	, m_bEnableCollapsingAnimation( false )
	, m_bUseSplitter( true )
	, m_bShowTree( true )
	, m_bCurrentDirectorySetInitially( true )
	, m_bCurrentDirectorySetOnWalk( true )
	, m_bCurrentDirectoryRestoreFinallyOnOK( false )
	, m_bCurrentDirectoryRestoreFinallyOnCancel( false )
	, m_bSaveRestoreInterriorOptions( true )
	, m_bSaveRestoreShellLocation( true )
	, m_bSaveRestoreWindowPosition( true )
	, m_bHelperEnableShellAutoCompleteInFileNameEditor( true )
	, m_dwHelperFlagsSHACFforFileNameEditor(
			__EXT_MFC_SHACF_FILESYSTEM
				|__EXT_MFC_SHACF_AUTOSUGGEST_FORCE_ON
				|__EXT_MFC_SHACF_AUTOAPPEND_FORCE_ON
				|__EXT_MFC_SHACF_USETAB
			)
	, m_bEnableButtonCreateNewFolder( true )
	, m_bEnableButtonDelete( true )
  , m_bLocationFlag( false )
{
	//{{AFX_DATA_INIT(CExtShellDialogFile)
	//}}AFX_DATA_INIT
	ASSERT( __EFDT_MIN_VALUE <= m_eFDT && m_eFDT <= __EFDT_MAX_VALUE );
	SetAutoSubclassChildControls();
	m_strCommandProfileName.Format(
		_T("CExtShellDialogFile-%d-%d-%p"),
		INT( ::GetCurrentProcessId() ),
		INT( ::GetCurrentThreadId() ),
		this
		);

	m_wndShellTree.m_dwAttributeFilterAllPresent = SFGAO_FILESYSANCESTOR;
	m_wndShellList.m_bSelectAllOnCtrlA = true;
}

CExtShellDialogFile::~CExtShellDialogFile()
{
	m_arrRetVal.Empty();
}

void CExtShellDialogFile::DoDataExchange(CDataExchange* pDX)
{
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExtShellDialogFile)
	//}}AFX_DATA_MAP
	DDX_Control( pDX, IDC_EXT_SFD_BUTTON_REFRESH,					m_buttonRefresh );
	DDX_Control( pDX, IDC_EXT_SFD_BUTTON_COLLAPSE,					m_buttonCollapse );
	DDX_Control( pDX, IDC_EXT_SFD_BUTTON_EXPAND,					m_buttonExpand );
	DDX_Control( pDX, IDC_EXT_SFD_BUTTON_FORWARD,					m_buttonForward );
	DDX_Control( pDX, IDC_EXT_SFD_BUTTON_BACK,						m_buttonBack );
	DDX_Control( pDX, IDC_EXT_SFD_BUTTON_LIST_VIEW_TYPE,			m_buttonViewMenu );
	DDX_Control( pDX, IDC_EXT_SFD_BUTTON_CREATE_NEW_FOLDER,			m_buttonCreateNewFolder );
	DDX_Control( pDX, IDC_EXT_SFD_BUTTON_DELETE,					m_buttonDelete );
	DDX_Control( pDX, IDC_EXT_SFD_BUTTON_UP_ONE_LEVEL,				m_buttonUpOneLevel );
	DDX_Control( pDX, IDC_EXT_SFD_STATIC_FILE_NAME,					m_labelFileName );
	DDX_Control( pDX, IDC_EXT_SFD_STATIC_FILES_OF_TYPE,				m_labelFilesOfType );
	DDX_Control( pDX, IDC_EXT_SFD_STATIC_LOOK_IN,					m_labelLookIn );
	DDX_Control( pDX, IDCANCEL,										m_buttonCancel );
	DDX_Control( pDX, IDOK,											m_buttonOK );
	DDX_Control( pDX, IDC_EXT_SFD_EDIT_FILE_NAME,					m_editFileName );
	DDX_Control( pDX, IDC_EXT_SFD_COMBO_FILES_OF_TYPE,				m_comboFileTypes );
	DDX_Control( pDX, IDC_EXT_SFD_COMBO_LOOK_IN,					m_comboLookIn );
	DDX_Control( pDX, IDC_EXT_SFD_SHELL_LIST,						m_wndShellList );
	DDX_Control( pDX, IDC_EXT_SFD_SHELL_TREE,						m_wndShellTree );
}


BEGIN_MESSAGE_MAP( CExtShellDialogFile, CExtResizableDialog )
	//{{AFX_MSG_MAP(CExtShellDialogFile)
	ON_CBN_SELENDOK(IDC_EXT_SFD_COMBO_FILES_OF_TYPE, OnSelendokComboFileTypes)
	ON_BN_CLICKED(IDC_EXT_SFD_BUTTON_COLLAPSE, OnButtonCollapse)
	ON_BN_CLICKED(IDC_EXT_SFD_BUTTON_EXPAND, OnButtonExpand)
	ON_BN_CLICKED(IDC_EXT_SFD_BUTTON_REFRESH, OnButtonRefresh)
	ON_BN_CLICKED(IDC_EXT_SFD_BUTTON_BACK, OnButtonBack)
	ON_BN_CLICKED(IDC_EXT_SFD_BUTTON_FORWARD, OnButtonForward)
	ON_BN_CLICKED(IDC_EXT_SFD_BUTTON_UP_ONE_LEVEL, OnButtonUpOneLevel)
	ON_BN_CLICKED(IDC_EXT_SFD_BUTTON_DELETE, OnButtonDelete)
	ON_BN_CLICKED(IDC_EXT_SFD_BUTTON_CREATE_NEW_FOLDER, OnButtonCreateNewFolder)
	ON_EN_CHANGE(IDC_EXT_SFD_EDIT_FILE_NAME, OnChangeEditFileName)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
//	ON_NOTIFY( LVN_ITEMCHANGED, IDC_EXT_SFD_SHELL_LIST, OnItemChangedShellList )
	ON_REGISTERED_MESSAGE( CExtShellListCtrl::g_nMsgShellLocationChanged, OnShellListLocationChanged )
	ON_REGISTERED_MESSAGE( CExtShellComboBox::g_nMsgShellLocationChanged, OnShellComboLocationChanged )
	ON_REGISTERED_MESSAGE( CExtTreeCtrl::g_nMsgTreeItemDelayedFocus, OnShellTreeDelayedItemFocus )
	ON_REGISTERED_MESSAGE( CExtShellBase::g_nMsgShellItemExecute, OnShellItemExecute )
	ON_COMMAND( ID_EXT_SLVM_TILES, OnListViewMode_Tiles )
	ON_UPDATE_COMMAND_UI( ID_EXT_SLVM_TILES, OnListViewMode_UpdateTiles )
	ON_COMMAND( ID_EXT_SLVM_ICONS, OnListViewMode_Icons )
	ON_UPDATE_COMMAND_UI( ID_EXT_SLVM_ICONS, OnListViewMode_UpdateIcons )
	ON_COMMAND( ID_EXT_SLVM_LIST, OnListViewMode_List )
	ON_UPDATE_COMMAND_UI( ID_EXT_SLVM_LIST, OnListViewMode_UpdateList )
	ON_COMMAND( ID_EXT_SLVM_DETAILS, OnListViewMode_Details )
	ON_UPDATE_COMMAND_UI( ID_EXT_SLVM_DETAILS, OnListViewMode_UpdateDetails )
END_MESSAGE_MAP()

void CExtShellDialogFile::_GetSettingsLocationStrings(
	CExtSafeString & strSectionSettingsEntry,
	CExtSafeString & strSectionNameCompany,
	CExtSafeString & strSectionNameProduct
	) const
{
	ASSERT_VALID( this );
	strSectionSettingsEntry.Empty();
	strSectionNameCompany.Empty();
	strSectionNameProduct.Empty();

	if( m_strSectionSettingsEntry.IsEmpty() )
	{
		strSectionSettingsEntry += _T("ShellFileDialog-");
		strSectionSettingsEntry += m_comboFileTypes.GetFilterHash();
	}
	else
		strSectionSettingsEntry = m_strSectionSettingsEntry;

CWinApp * pApp = ::AfxGetApp();
	ASSERT_VALID( pApp );
	if( m_strSectionNameCompany.IsEmpty() )
	{
		strSectionNameCompany = pApp->m_pszRegistryKey;
		if( strSectionNameCompany.IsEmpty() )
			strSectionNameCompany = _T("Prof-UIS");
	}
	else
		strSectionNameCompany = m_strSectionNameCompany;

	if( m_strSectionNameProduct.IsEmpty() )
	{
		strSectionNameProduct = pApp->m_pszProfileName;
		if( strSectionNameProduct.IsEmpty() )
			strSectionNameProduct = _T("Shell-File-Dialog");
	}
	else
		strSectionNameProduct = m_strSectionNameProduct;
}

static CExtSafeString stat_productsection2regkeypath_for_file_dialog_interrior(
	__EXT_MFC_SAFE_LPCTSTR sUniqueName,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany, // under HKEY_CURRENT_USER\Software
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%
	)
{
	return CExtCmdManager::GetSubSystemRegKeyPath(
		__PROF_UIS_REG_SHELL_FILE_DIALOG_INTERRIOR,
		sUniqueName,
		sSectionNameCompany,
		sSectionNameProduct
		);
}

static bool stat_fileobj_to_registry_for_file_dialog_interrior(
	CFile & _file,
	__EXT_MFC_SAFE_LPCTSTR sUniqueName,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany, // under HKEY_CURRENT_USER\Software
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct, // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%
	HKEY hKeyRoot,
	bool bEnableThrowExceptions
	)
{
	ASSERT( sUniqueName != NULL );
	ASSERT( sSectionNameCompany != NULL );
	ASSERT( sSectionNameProduct != NULL );
CExtSafeString sRegKeyPath =
		stat_productsection2regkeypath_for_file_dialog_interrior(
			sUniqueName,
			sSectionNameCompany,
			sSectionNameProduct
			);

	return
		CExtCmdManager::FileObjToRegistry(
			_file,
			sRegKeyPath,
			hKeyRoot,
			bEnableThrowExceptions
			);
}

static bool stat_fileobj_from_registry_for_file_dialog_interrior(
	CFile & _file,
	__EXT_MFC_SAFE_LPCTSTR sUniqueName,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany, // under HKEY_CURRENT_USER\Software
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct, // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%
	HKEY hKeyRoot,
	bool bEnableThrowExceptions
	)
{
	ASSERT( sUniqueName != NULL );
	ASSERT( sSectionNameCompany != NULL );
	ASSERT( sSectionNameProduct != NULL );
	ASSERT( _file.GetLength() == 0 );
CExtSafeString sRegKeyPath =
		stat_productsection2regkeypath_for_file_dialog_interrior(
			sUniqueName,
			sSectionNameCompany,
			sSectionNameProduct
			);
	return
		CExtCmdManager::FileObjFromRegistry(
			_file,
			sRegKeyPath,
			hKeyRoot,
			bEnableThrowExceptions
			);
}

bool CExtShellDialogFile::_InterriorOptionsSerialize(
	__EXT_MFC_SAFE_LPCTSTR sUniqueName,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany, // under HKEY_CURRENT_USER\Software
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct, // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%
	bool bSave,
	HKEY hKeyRoot, // = HKEY_CURRENT_USER
	bool bEnableThrowExceptions // = false
	)
{
	ASSERT_VALID( this );
	ASSERT( sSectionNameCompany != NULL );
	ASSERT( sSectionNameProduct != NULL );
bool bRetVal = false;
	try
	{
		CMemFile _file;
		if( bSave )
		{
			{ // BLOCK: CArchive usage
				CArchive ar(
					&_file,
					CArchive::store
					);
				if( ! _InterriorOptionsSerialize( ar, bEnableThrowExceptions ) )
					return false;
				ar.Flush();
			} // BLOCK: CArchive usage

			// ... write _file to registry
			_file.Seek(0,CFile::begin);
			if( ! stat_fileobj_to_registry_for_file_dialog_interrior(
					_file,
					sUniqueName,
					sSectionNameCompany,
					sSectionNameProduct,
					hKeyRoot,
					bEnableThrowExceptions
					)
				)
				return false;

		} // if( bSave )
		else
		{
			// ... read _file from registry
			if( ! stat_fileobj_from_registry_for_file_dialog_interrior(
					_file,
					sUniqueName,
					sSectionNameCompany,
					sSectionNameProduct,
					hKeyRoot,
					bEnableThrowExceptions
					)
				)
				return false;
			_file.Seek(0,CFile::begin);

			CArchive ar(
				&_file,
				CArchive::load
				);
			if( ! _InterriorOptionsSerialize( ar, bEnableThrowExceptions ) )
				return false;
		} // else from if( bSave )

		bRetVal = true;
	} // try
	catch( CException * pXept )
	{
		pXept->Delete();
		ASSERT( FALSE );
	} // catch( CException * pXept )
	catch( ... )
	{
		ASSERT( FALSE );
	} // catch( ... )
	return bRetVal;
}

bool CExtShellDialogFile::_InterriorOptionsSerialize(
	CArchive & ar,
	bool bEnableThrowExceptions // = false
	)
{
	ASSERT_VALID( this );
	try
	{
		if( GetSafeHwnd() == NULL )
			::AfxThrowUserException();
		DWORD dwMainStateFlags = 0, dwTreeWidth = 0, dwListWidth = 0, dwListViewMode = 0;
		if( ar.IsStoring() )
		{ // saving
			if( m_bEnableCollapsing && ( m_listCollapsedHWNDs.GetCount() != 0 ) )
				dwMainStateFlags |= 0x00000001;
			CRect rcTree, rcList;
			m_wndShellTree.GetWindowRect( &rcTree );
			ScreenToClient( &rcTree );
			m_wndShellList.GetWindowRect( &rcList );
			ScreenToClient( &rcList );
			dwTreeWidth = DWORD( rcTree.Width() );
			dwListWidth = DWORD( rcList.Width() );
			dwListViewMode = DWORD( m_wndShellList.GetStyle() & LVS_TYPEMASK );
			ar << dwMainStateFlags;
			ar << dwTreeWidth;
			ar << dwListWidth;
			ar << dwListViewMode;
		} // saving
		else
		{ // loading
			ar >> dwMainStateFlags;
			ar >> dwTreeWidth;
			ar >> dwListWidth;
			ar >> dwListViewMode;
			m_wndShellList.ModifyStyle( LVS_TYPEMASK, 0 );
			m_wndShellList.ModifyStyle( 0, dwListViewMode );
			if(		m_bUseSplitter
				&&	m_wndSplitter.GetSafeHwnd() != NULL
				)
			{
				int cxCur = 0, cxMin = 0;
				m_wndSplitter.GetColumnInfo( 0, cxCur, cxMin );
				cxCur = int(dwTreeWidth);
				m_wndSplitter.SetColumnInfo( 0, cxCur, cxMin );
			}
			if( m_bEnableCollapsing && ( dwMainStateFlags & 0x00000001 ) != 0 && ( m_listCollapsedHWNDs.GetCount() == 0 ) )
			{
				bool bOldEnableCollapsingAnimation = m_bEnableCollapsingAnimation;
				m_bEnableCollapsingAnimation = false;
				OnButtonCollapse();
				m_bEnableCollapsingAnimation = bOldEnableCollapsingAnimation;
			}
		} // loading
		m_wndShellList.StateSerialize( ar );
		return true;
	} // try
	catch( CException * pException )
	{
		pException;
		if( bEnableThrowExceptions )
			throw;
	} // catch( CException * pException )
	catch( ... )
	{
		if( bEnableThrowExceptions )
			throw;
	} // catch( ... )
	return false;
}

/////////////////////////////////////////////////////////////////////////////
// CExtShellDialogFile locator
void CExtShellDialogFile::SetLocation(CRect *cRect)
{
  m_cRect = *(cRect);
  m_bLocationFlag = true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtShellDialogFile message handlers

BOOL CExtShellDialogFile::OnInitDialog()
{
	ASSERT_VALID( this );
	m_strRetValNames.Empty();

CExtSafeString strSectionSettingsEntry, strSectionNameCompany, strSectionNameProduct;
	_GetSettingsLocationStrings( strSectionSettingsEntry, strSectionNameCompany, strSectionNameProduct );

CExtSafeString strInitialFodler = m_strRetValFolder;
CExtPIDL pidlFolderToSelectInitially = m_pidlRetValFolder;
	if( ! CExtShellBase::stat_ValidPath( pidlFolderToSelectInitially.GetPath() ) )
		pidlFolderToSelectInitially.Empty();
	m_strRetValFolder.Empty();
	m_pidlRetValFolder.Empty();
	m_arrRetVal.Empty();
	m_listCollapsedHWNDs.RemoveAll();
	m_listMovedHWNDs.RemoveAll();
	m_nSavedCollapsedHeight = 0;
	m_nCollapsedWindowHeight = 0;
	m_bHelperCollapsignAnimation = false;
	if(		m_bEnableCollapsing
//		||	m_eFDT == __EFDT_SAVE
//		||	m_eFDT == __EFDT_SAVE_AS
		)
		m_bShowFileNamesEdit = true;
bool bCollapseInitially = m_bCollapseInitially;
	if( ! m_bEnableCollapsing )
		bCollapseInitially = false;

#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		::OnInitDialog();

	if( m_bEnableCollapsing )
	{
		GetSystemMenu(FALSE)->EnableMenuItem( SC_MAXIMIZE, FALSE );
		ModifyStyle( WS_MINIMIZEBOX|WS_MAXIMIZEBOX, 0 );
	}

	ASSERT( __EFDT_MIN_VALUE <= m_eFDT && m_eFDT <= __EFDT_MAX_VALUE );
CString strInitialCaption, strTmp;
CStringArray arrCaptionParts;
static const TCHAR strDelimiter[2] = _T("|");
INT nCaptionIndex, nCharPosA = 0, nCharPosB = 0, nCaptionLength = 0;
	// parse dialog caption; format must be: Open|Save|Save As
	if( m_strCustomDialogCaption.IsEmpty() )
	{
		GetWindowText( strInitialCaption );
		nCaptionLength = strInitialCaption.GetLength();
		nCharPosA = strInitialCaption.Find( strDelimiter );
		ASSERT( nCharPosA >= 0 );
		nCharPosB = strInitialCaption.Find( strDelimiter, nCharPosA + 1 );
		ASSERT( nCharPosB > nCharPosA );
		strTmp = strInitialCaption.Left( nCharPosA );
		strTmp.TrimLeft( _T(" \r\n\t") );
		strTmp.TrimRight( _T(" \r\n\t") );
		arrCaptionParts.Add( strTmp );
		strTmp = strInitialCaption.Mid( nCharPosA+1, nCharPosB-nCharPosA-1 );
		strTmp.TrimLeft( _T(" \r\n\t") );
		strTmp.TrimRight( _T(" \r\n\t") );
		arrCaptionParts.Add( strTmp );
		strTmp = strInitialCaption.Right( nCaptionLength-nCharPosB-1 );
		strTmp.TrimLeft( _T(" \r\n\t") );
		strTmp.TrimRight( _T(" \r\n\t") );
		arrCaptionParts.Add( strTmp );
		nCaptionIndex = 0;
		if( m_eFDT == __EFDT_SAVE )
			nCaptionIndex = 1;
		else if( m_eFDT == __EFDT_SAVE_AS )
			nCaptionIndex = 2;
		strTmp = arrCaptionParts[ nCaptionIndex ];
		SetWindowText( LPCTSTR(strTmp) );
	} // if( m_strCustomDialogCaption.IsEmpty() )
	else
		SetWindowText( LPCTSTR(m_strCustomDialogCaption) );

	// parse open/save button's caption; format must be: &Open|&Save
	if( m_strCustomOpenSaveButtonCaption.IsEmpty() )
	{
		arrCaptionParts.RemoveAll();
		m_buttonOK.GetWindowText( strInitialCaption );
		nCaptionLength = strInitialCaption.GetLength();
		nCharPosA = strInitialCaption.Find( strDelimiter );
		ASSERT( nCharPosA >= 0 );
		strTmp = strInitialCaption.Left( nCharPosA );
		strTmp.TrimLeft( _T(" \r\n\t") );
		arrCaptionParts.Add( strTmp );
		strTmp = strInitialCaption.Right( nCaptionLength-nCharPosA-1 );
		strTmp.TrimLeft( _T(" \r\n\t") );
		arrCaptionParts.Add( strTmp );
		nCaptionIndex = 0;
		if( m_eFDT == __EFDT_SAVE || m_eFDT == __EFDT_SAVE_AS )
			nCaptionIndex = 1;
		strTmp = arrCaptionParts[ nCaptionIndex ];
		m_buttonOK.SetWindowText( LPCTSTR(strTmp) );
	} // if( m_strCustomOpenSaveButtonCaption.IsEmpty() )
	else
		m_buttonOK.SetWindowText( LPCTSTR(m_strCustomOpenSaveButtonCaption) );

	// parse file name static control's caption; format must be: File &name:|File &names:
	arrCaptionParts.RemoveAll();
	m_labelFileName.GetWindowText( strInitialCaption );
	nCaptionLength = strInitialCaption.GetLength();
	nCharPosA = strInitialCaption.Find( strDelimiter );
	ASSERT( nCharPosA >= 0 );
	strTmp = strInitialCaption.Left( nCharPosA );
	strTmp.TrimLeft( _T(" \r\n\t") );
	arrCaptionParts.Add( strTmp );
	strTmp = strInitialCaption.Right( nCaptionLength-nCharPosA-1 );
	strTmp.TrimLeft( _T(" \r\n\t") );
	arrCaptionParts.Add( strTmp );
	nCaptionIndex = 0;
	if( m_eFDT == __EFDT_OPEN_MULTIPLE )
		nCaptionIndex = 1;
	strTmp = arrCaptionParts[ nCaptionIndex ];
	m_labelFileName.SetWindowText( LPCTSTR(strTmp) );

	// parse file type static control's caption; format must be: Files of &type:|Save as &type:
	arrCaptionParts.RemoveAll();
	m_labelFilesOfType.GetWindowText( strInitialCaption );
	nCaptionLength = strInitialCaption.GetLength();
	nCharPosA = strInitialCaption.Find( strDelimiter );
	ASSERT( nCharPosA >= 0 );
	strTmp = strInitialCaption.Left( nCharPosA );
	strTmp.TrimLeft( _T(" \r\n\t") );
	arrCaptionParts.Add( strTmp );
	strTmp = strInitialCaption.Right( nCaptionLength-nCharPosA-1 );
	strTmp.TrimLeft( _T(" \r\n\t") );
	arrCaptionParts.Add( strTmp );
	nCaptionIndex = 0;
	if( m_eFDT == __EFDT_SAVE || m_eFDT == __EFDT_SAVE_AS )
		nCaptionIndex = 1;
	strTmp = arrCaptionParts[ nCaptionIndex ];
	m_labelFilesOfType.SetWindowText( LPCTSTR(strTmp) );

	//
	if( ! m_strCustomCancelButtonCaption.IsEmpty() )
		m_buttonCancel.SetWindowText( LPCTSTR(m_strCustomCancelButtonCaption) );
	if( ! m_strCustomFileNameLabelCaption.IsEmpty() )
		m_labelFileName.SetWindowText( LPCTSTR(m_strCustomFileNameLabelCaption) );
	if( ! m_strCustomFileTypeLabelCaption.IsEmpty() )
		m_labelFilesOfType.SetWindowText( LPCTSTR(m_strCustomFileTypeLabelCaption) );

	//
	VERIFY( g_CmdManager->ProfileSetup( LPCTSTR(m_strCommandProfileName), m_hWnd ) );
	VERIFY( g_ResourceManager->LoadMenu( m_buttonViewMenu.m_menu, IDR_EXT_SHELL_LIST_VIEW_MENU ) );
	VERIFY( g_CmdManager->UpdateFromMenu( LPCTSTR(m_strCommandProfileName), m_buttonViewMenu.m_menu.GetSafeHmenu() ) );
static UINT g_arrBasicCommands[] =
{
	ID_EXT_SLVM_TILES, ID_EXT_SLVM_ICONS, ID_EXT_SLVM_LIST, ID_EXT_SLVM_DETAILS, 0
};
	VERIFY( g_CmdManager->SetBasicCommands( LPCTSTR(m_strCommandProfileName), g_arrBasicCommands ) );

CWnd * pWnd = GetWindow( GW_CHILD );
	for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )
	{
		CExtIconButton * pIconButton = DYNAMIC_DOWNCAST( CExtIconButton, pWnd );
		if( pIconButton == NULL )
			continue;
		if( ! pIconButton->m_icon.IsEmpty() )
			continue;
		UINT nID = (UINT)pIconButton->GetDlgCtrlID();
		pIconButton->m_icon.m_bmpNormal.LoadBMP_Resource( MAKEINTRESOURCE(nID) );
		pIconButton->m_icon.m_bmpHover = pIconButton->m_icon.m_bmpNormal;
		//pIconButton->m_icon.m_bmpPressed = pIconButton->m_icon.m_bmpNormal;
		pIconButton->m_icon.m_bmpDisabled = pIconButton->m_icon.m_bmpNormal;
		pIconButton->m_icon.m_bmpDisabled.AdjustAlpha( -0.75 );
		pIconButton->m_icon.m_bmpNormal.AdjustAlpha( -0.35 );
		pIconButton->m_icon.m_bmpPressed = pIconButton->m_icon.m_bmpNormal;
		pIconButton->m_icon.m_dwFlags |=
			  __EXT_ICON_PERSISTENT_BITMAP_DISABLED
			| __EXT_ICON_PERSISTENT_BITMAP_HOVER
			| __EXT_ICON_PERSISTENT_BITMAP_PRESSED
			;
		CString s;
		pIconButton->GetWindowText( s );
		pIconButton->SetTooltipText( LPCTSTR(s) );
	} // for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )

	_ShellListExtensionSet();

	if( ! m_pidlRoot.IsEmpty() )
		m_wndShellTree.RefreshShellRoot( m_pidlRoot.GetPtr() );
	else
	 	m_wndShellTree.RefreshShellRoot();

	m_wndShellTree.ModifyStyle( TVS_SINGLEEXPAND, TVS_TRACKSELECT|TVS_EDITLABELS|TVS_SHOWSELALWAYS );
	//m_wndShellTree.IncludeFilesSet( true );
	m_wndShellList.ModifyExtendedStyle( 0, LVS_EX_UNDERLINEHOT|LVS_EX_UNDERLINECOLD );
	m_wndShellList.ModifyStyle( LVS_SHOWSELALWAYS, LVS_EDITLABELS|LVS_AUTOARRANGE );

//	if( m_eFDT == __EFDT_OPEN_SINGLE )
	if( m_eFDT != __EFDT_OPEN_MULTIPLE )
		m_wndShellList.ModifyStyle( 0, LVS_SINGLESEL );

	ASSERT(
			m_nInitialListViewMode == LVS_SMALLICON
		||	m_nInitialListViewMode == LVS_ICON
		||	m_nInitialListViewMode == LVS_LIST
		||	m_nInitialListViewMode == LVS_REPORT
		);
	if( INT( m_wndShellList.GetStyle() & LVS_TYPEMASK ) != m_nInitialListViewMode )
		m_wndShellList.ModifyStyle( LVS_TYPEMASK, m_nInitialListViewMode );

TCHAR strCurrentDirectory[ _MAX_PATH + 1 ];
	::memset( strCurrentDirectory, 0, _MAX_PATH + 1 );
	::GetCurrentDirectory( _MAX_PATH, strCurrentDirectory );

	if( m_bCurrentDirectoryRestoreFinallyOnOK || m_bCurrentDirectoryRestoreFinallyOnCancel )
		m_strCurrentDirectoryToRestore = strCurrentDirectory;
	else
		m_strCurrentDirectoryToRestore.Empty();


bool bSetFocusFirstRootChildren = true;
	if( m_bSaveRestoreShellLocation )
	{
		CExtPIDL pidl;
		if(		pidl.Serialize( LPCTSTR(strSectionSettingsEntry), LPCTSTR(strSectionNameCompany), LPCTSTR(strSectionNameProduct), false )
			&&	(! pidl.IsEmpty() )
			)
		{
			pidlFolderToSelectInitially.Empty();
			pidlFolderToSelectInitially.Attach( pidl.Detach() );
			if( ! CExtShellBase::stat_ValidPath( pidlFolderToSelectInitially.GetPath() ) )
				pidlFolderToSelectInitially.Empty();
			else
				bSetFocusFirstRootChildren = false;
		}
	}
	if( bSetFocusFirstRootChildren && m_bCurrentDirectorySetInitially )
	{
		if( m_wndShellTree.FocusPath( strCurrentDirectory ) != NULL )
		{
			bSetFocusFirstRootChildren = false;
			pidlFolderToSelectInitially.Empty();
		}
	}
	if(		bSetFocusFirstRootChildren
		&&	( pidlFolderToSelectInitially.IsEmpty() || ( ! pidlFolderToSelectInitially.IsFolder() ) )
		&&	( ! strInitialFodler.IsEmpty() )
		)
	{
		CExtPIDL pidlVerify;
		if( pidlVerify.FromFolder( LPCTSTR(strInitialFodler), m_hWnd ) )
		{
			if( (! pidlVerify.IsEmpty()  ) && pidlVerify.IsFolder() )
			{
				pidlFolderToSelectInitially.Empty();
				pidlFolderToSelectInitially.Attach( pidlVerify.Detach() );
				if( ! CExtShellBase::stat_ValidPath( pidlFolderToSelectInitially.GetPath() ) )
					pidlFolderToSelectInitially.Empty();
				else
					bSetFocusFirstRootChildren = false;
			}
		}
	}
	if( bSetFocusFirstRootChildren && pidlFolderToSelectInitially.IsEmpty() )
	{
		if( pidlFolderToSelectInitially.GetSpecialFolderLocation( m_hWnd, CSIDL_DRIVES ) )
			bSetFocusFirstRootChildren = false;
	}
	if( ! pidlFolderToSelectInitially.IsEmpty() )
	{
		HTREEITEM hti = m_wndShellTree.FindItemByPIDL( NULL, pidlFolderToSelectInitially );
		if( hti != NULL )
		{
			bSetFocusFirstRootChildren = false;
			m_wndShellTree.FocusItem( hti, true, true );
			m_wndShellTree.Expand( hti, TVE_EXPAND );
		}
		else
		{
			hti = m_wndShellTree.FocusPIDL( pidlFolderToSelectInitially, true, true, true );
			if( hti != NULL )
			{
				bSetFocusFirstRootChildren = false;
				m_wndShellTree.Expand( hti, TVE_EXPAND );
			}
		}
	}
	if( bSetFocusFirstRootChildren )
	{
		m_wndShellTree.FocusItem( m_wndShellTree.GetChildItem( TVI_ROOT ), true, false, true );
	}
HTREEITEM htiSelect = m_wndShellTree.GetFocusedItem();
	if( htiSelect != NULL )
	{
		CExtTreeCtrl::TREEITEMINFO_t & _TII = m_wndShellTree.TreeItemInfoGet( htiSelect );
		CExtShellItemData * pData = (CExtShellItemData*)_TII.m_lParam; //new CExtShellItemData;
		ASSERT( pData != NULL );
		LPITEMIDLIST pidlSelect = (LPITEMIDLIST)pData->m_pidlAbsolute.GetPtr();
		ASSERT( pidlSelect != NULL );
		m_wndShellList.FocusPIDL( pidlSelect );
	}

	m_labelFileName.ShowWindow( m_bShowFileNamesEdit ? SW_SHOW : SW_HIDE );
	m_editFileName.ShowWindow( m_bShowFileNamesEdit ? SW_SHOW : SW_HIDE );
	m_editFileName.ModifyStyle(
		m_bReadOnlyFileNamesEdit ? 0 : ES_READONLY,
		m_bReadOnlyFileNamesEdit ? ES_READONLY : 0
		);

	m_labelFilesOfType.ShowWindow( m_bShowFileTypesComboBox ? SW_SHOW : SW_HIDE );
	m_comboFileTypes.ShowWindow( m_bShowFileTypesComboBox ? SW_SHOW : SW_HIDE );

CRect rcCollapseExpand;
	m_buttonCollapse.GetWindowRect( &rcCollapseExpand );
	ScreenToClient( &rcCollapseExpand );
	m_buttonExpand.MoveWindow( &rcCollapseExpand );
	if( m_bEnableCollapsing )
	{
		m_buttonCollapse.EnableWindow( TRUE );
		m_buttonCollapse.ShowWindow( SW_SHOWNOACTIVATE );
	}

	if( m_bUseSplitter )
	{
		if( ! m_wndSplitter.Create(
				m_wndShellTree,
				m_wndShellList,
				*this,
				m_bShowTree
				)
			)
		{
			PostMessage( WM_COMMAND, IDCANCEL );
			return FALSE;
		}
		m_wndShellTree.NCSB_GetContainer( CExtNCSB_ScrollContainer::__EM_CORNER_AREA )->SetParent( &m_wndSplitter );
		m_wndShellTree.NCSB_GetContainer( CExtNCSB_ScrollContainer::__EM_HORIZONTAL_SCROLL_BAR )->SetParent( &m_wndSplitter );
		m_wndShellTree.NCSB_GetContainer( CExtNCSB_ScrollContainer::__EM_VERTICAL_SCROLL_BAR )->SetParent( &m_wndSplitter );
		m_wndShellList.NCSB_GetContainer( CExtNCSB_ScrollContainer::__EM_CORNER_AREA )->SetParent( &m_wndSplitter );
		m_wndShellList.NCSB_GetContainer( CExtNCSB_ScrollContainer::__EM_HORIZONTAL_SCROLL_BAR )->SetParent( &m_wndSplitter );
		m_wndShellList.NCSB_GetContainer( CExtNCSB_ScrollContainer::__EM_VERTICAL_SCROLL_BAR )->SetParent( &m_wndSplitter );
	}
	else
	{
		if( ! m_bShowTree )
		{
			CRect rcTree, rcList;
			m_wndShellTree.GetWindowRect( &rcTree );
			ScreenToClient( &rcTree );
			m_wndShellList.GetWindowRect( &rcList );
			ScreenToClient( &rcList );
			rcList.left = rcTree.left;
			m_wndShellTree.ShowWindow( SW_HIDE );
			m_wndShellList.MoveWindow( &rcList );
		}
	}

	m_wndShellList.ModifyExtendedStyle( 0, LVS_EX_HEADERDRAGDROP );

	if( m_bSaveRestoreInterriorOptions )
		_InterriorOptionsSerialize( LPCTSTR(strSectionSettingsEntry), LPCTSTR(strSectionNameCompany), LPCTSTR(strSectionNameProduct), false );

	m_comboLookIn.SetItemHeight(-1, g_PaintManager->UiScalingDo(18, CExtPaintManager::__EUIST_Y));
	_ReAnchor();

	if( bCollapseInitially && ( m_listCollapsedHWNDs.GetCount() == 0 ) )
	{
		bool bOldEnableCollapsingAnimation = m_bEnableCollapsingAnimation;
		m_bEnableCollapsingAnimation = false;
		OnButtonCollapse();
		m_bEnableCollapsingAnimation = bOldEnableCollapsingAnimation;
	}

	m_bLockChangingsValidation = true;

INT nFileTypeIndex = INT( m_comboFileTypes.GetCurSel() );
	if(		nFileTypeIndex != m_nInitialFileTypeIndex
		&&	m_nInitialFileTypeIndex >= 0
		&&	m_nInitialFileTypeIndex < INT( m_comboFileTypes.GetCount() )
		)
	{
		m_comboFileTypes.SetCurSel( m_nInitialFileTypeIndex );
		OnSelendokComboFileTypes();
		for( MSG _msg; ::PeekMessage( &_msg, m_hWnd, WM_COMMAND, WM_COMMAND, PM_REMOVE ); );
	}

	m_buttonForward.EnableWindow( FALSE );
	m_buttonBack.EnableWindow( FALSE );
	m_buttonDelete.EnableWindow( FALSE );
	if( ! m_bEnableButtonCreateNewFolder )
		m_buttonCreateNewFolder.EnableWindow( FALSE );

	if( m_strEditorTextInitial.IsEmpty() )
	{
		if( m_bFilesMustExist || m_eFDT == __EFDT_SAVE || m_eFDT == __EFDT_SAVE_AS )
			m_buttonOK.EnableWindow( FALSE );
	}
	else
	{
//		m_bLockChangingsValidation = true;
		m_editFileName.SetWindowText( LPCTSTR(m_strEditorTextInitial) );
//		bool bOldFilesMustExist = m_bFilesMustExist;
//		m_bFilesMustExist = true;
		m_buttonOK.EnableWindow( _ValidateEditor( true ) ? TRUE : FALSE );
//		m_bFilesMustExist = bOldFilesMustExist;
//		m_bLockChangingsValidation = false;
	}

	if( m_wndShellList.FocusedItemGet() < 0 )
	{
		POSITION pos = m_wndShellList.GetFirstSelectedItemPosition();
		if( pos != NULL )
		{
			INT nItem = m_wndShellList.GetNextSelectedItem( pos );
			m_wndShellList.FocusedItemSet( nItem, true, false );
		}
		if( m_wndShellList.FocusedItemGet() < 0 )
			m_wndShellList.FocusedItemSet( 0 );
	}

	m_bLockChangingsValidation = false;
  // this is the relocation code alw 09/27/2013
  if (m_bLocationFlag == true){
    // Get the owner window and dialog box rectangles. 
    SetWindowPos(this, 
                 m_cRect.left, 
                 m_cRect.top, 
                 0, 0,          // Ignores size arguments. 
                 SWP_NOSIZE); 
  } // m_bLocationFlag
//	if( m_bSaveRestoreWindowPosition )
//		EnableSaveRestore( _T("ShellFileDialog"), LPCTSTR(strSectionSettingsEntry) );
// test
	if( m_bHelperEnableShellAutoCompleteInFileNameEditor && m_editFileName.GetSafeHwnd() != NULL )
		CExtShellBase::stat_SHAutoComplete( m_editFileName.m_hWnd, m_dwHelperFlagsSHACFforFileNameEditor );
	return TRUE;
}

void CExtShellDialogFile::_ReAnchor()
{
	ASSERT_VALID( this );
	RemoveAllAnchors();

	AddAnchor( m_buttonBack.m_hWnd,					__RDA_LT );
	AddAnchor( m_buttonForward.m_hWnd,				__RDA_LT );

	AddAnchor( m_labelLookIn.m_hWnd,				__RDA_LT );
	AddAnchor( m_comboLookIn.m_hWnd,				__RDA_LT, __RDA_RT );

	AddAnchor( m_buttonUpOneLevel.m_hWnd,			__RDA_RT );
	AddAnchor( m_buttonDelete.m_hWnd,				__RDA_RT );
	AddAnchor( m_buttonCreateNewFolder.m_hWnd,		__RDA_RT );
	AddAnchor( m_buttonRefresh.m_hWnd,				__RDA_RT );
	AddAnchor( m_buttonViewMenu.m_hWnd,				__RDA_RT );

	if( m_wndSplitter.GetSafeHwnd() != NULL )
		AddAnchor( m_wndSplitter.m_hWnd,			__RDA_LT, __RDA_RB );
	else
	{
		AddAnchor( m_wndShellTree.m_hWnd,			__RDA_LT, __RDA_LB );
		AddAnchor( m_wndShellList.m_hWnd,			__RDA_LT, __RDA_RB );
	}

	AddAnchor( m_buttonCollapse.m_hWnd,				__RDA_LB );
	AddAnchor( m_buttonExpand.m_hWnd,				__RDA_LB );

	AddAnchor( m_labelFileName.m_hWnd,				__RDA_LB );
	AddAnchor( m_labelFilesOfType.m_hWnd,			__RDA_LB );
	AddAnchor( m_editFileName.m_hWnd,				__RDA_LB, __RDA_RB );
	AddAnchor( m_comboFileTypes.m_hWnd,				__RDA_LB, __RDA_RB );

	AddAnchor( m_buttonOK.m_hWnd,					__RDA_RB );
	AddAnchor( m_buttonCancel.m_hWnd,				__RDA_RB );
}

LRESULT CExtShellDialogFile::OnShellItemExecute( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	wParam;
	if( m_bLockChangingsValidation )
		return 0L;
//	if( ! ( m_eFDT == __EFDT_OPEN_SINGLE || m_eFDT == __EFDT_SAVE || m_eFDT == __EFDT_SAVE_AS ) )
//		return 0L;
CExtShellBase::NotifyShellItemExecuteData & _NSIED = CExtShellBase::NotifyShellItemExecuteData::FromLPARAM( lParam );
	if( _NSIED.m_hWndControl != m_wndShellList.GetSafeHwnd() )
		return 0L;
INT nItemIndex = INT( _NSIED.m_lParamItemID );
	if( nItemIndex < 0 || nItemIndex >= INT(m_wndShellList.GetItemCount()) )
		return 0L;
CExtShellItemData * pShellItemData = (CExtShellItemData*)m_wndShellList.GetItemData( nItemIndex );
DWORD dwAttributes = pShellItemData->m_pidlAbsolute.GetAttributesOf( SFGAO_FOLDER );
	if(		(dwAttributes & SFGAO_FOLDER) != 0
		||	pShellItemData->m_eSIT == CExtShellItemData::__ESIT_JUMP_ONE_LEVEL_UP
		||	pShellItemData->m_eSIT == CExtShellItemData::__ESIT_JUMP_TO_ROOT
		)
		return 0L;
	_NSIED.m_bHandled = true;
	_ValidateListViewSelection();
	SendMessage( WM_COMMAND, IDOK );
	return 0L;
}

LRESULT CExtShellDialogFile::OnShellTreeDelayedItemFocus( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	if( m_bLockChangingsValidation )
		return 0L;
CExtShellTreeCtrl * pST = (CExtShellTreeCtrl*)wParam;
	ASSERT( pST != NULL );
	if( pST->m_hWnd != m_wndShellTree.m_hWnd )
		return 0L;
	if( ( lParam & CExtTreeCtrl::__PARM_DELAYED_FOCUS_BEGIN_WAITING ) != 0 )
	{
		if( ( lParam & CExtTreeCtrl::__PARM_DELAYED_FOCUS_MOUSE_CLICK ) == 0 )
			return 0L;
	}
	if( m_bHaldingSelectionInList || m_bHaldingSelectionInCombo || m_bHaldingHistory )
		return 0L;
	m_bHaldingSelectionInTree = true;
HTREEITEM htiSelect = m_wndShellTree.GetFocusedItem();
	ASSERT( htiSelect != NULL );
CExtTreeCtrl::TREEITEMINFO_t & _TII = m_wndShellTree.TreeItemInfoGet( htiSelect );
CExtShellItemData * pData = (CExtShellItemData*)_TII.m_lParam; //new CExtShellItemData;
	ASSERT( pData != NULL );
LPITEMIDLIST pidlSelect = (LPITEMIDLIST)pData->m_pidlAbsolute.GetPtr();
	ASSERT( pidlSelect != NULL );

	if( pData->m_pidlAbsolute.GetPath().IsEmpty() && ( m_eFDT == __EFDT_SAVE || m_eFDT == __EFDT_SAVE_AS ) )
		m_buttonOK.EnableWindow( FALSE );

	m_wndShellList.DelayFocusPIDL( pidlSelect );
	m_comboLookIn.DelayFocusPIDL( pidlSelect );
	if( m_wndShellList.FocusedItemGet() < 0 && ::GetFocus() == m_wndShellList.GetSafeHwnd() )
		m_wndShellList.FocusedItemSet( 0 );

	if( m_bCurrentDirectorySetOnWalk )
	{
		CExtSafeString strPath = pData->m_pidlAbsolute.GetPath();
		if( ! strPath.IsEmpty() )
			SetCurrentDirectory( LPCTSTR(strPath) );
	}

	m_bHaldingSelectionInTree = false;
	return 0L;
}

LRESULT CExtShellDialogFile::OnShellListLocationChanged( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	lParam;
	if( m_bLockChangingsValidation )
		return 0L;
CExtShellListCtrl * pSL = (CExtShellListCtrl*)wParam;
	ASSERT( pSL != NULL );
	if( pSL->m_hWnd != m_wndShellList.m_hWnd )
		return 0L;
	if( m_bHaldingSelectionInTree || m_bHaldingSelectionInCombo || m_bHaldingHistory )
		return 0L;
	m_bHaldingSelectionInList = true;

	m_buttonDelete.EnableWindow( FALSE );

CExtPIDL pidlSelect = m_wndShellList.GetCurrentFolderPIDL();
	m_wndShellTree.DelayFocusPIDL( pidlSelect.GetPtr() );
	m_comboLookIn.DelayFocusPIDL( pidlSelect.GetPtr() );

INT _nHistoryCount = INT( m_arrayHistory.GetCount() );
	if( ( m_nCurrentHistoryPosition + 1 ) < _nHistoryCount )
		m_arrayHistory.RemoveFromUpToTail( m_nCurrentHistoryPosition + 1 );
bool bAddTailMode = true;
INT _nPrevHC = INT( m_arrayHistory.GetCount() );
	if( _nPrevHC > 0 )
	{
		LPCITEMIDLIST pidlLast = m_arrayHistory.GetAt( _nPrevHC - 1 );
		if( pidlLast != NULL && pidlSelect == pidlLast )
			bAddTailMode = false; // do not add the same folder reference as the last folder in the history PIDL list
	}
	if( bAddTailMode )
	{
		m_arrayHistory.AddTail( pidlSelect.GetPtr() );
		m_nCurrentHistoryPosition ++;
	}

	if( m_nCurrentHistoryPosition > 0 )
		m_buttonBack.EnableWindow();
	else
		m_buttonBack.EnableWindow( FALSE );

	if( pidlSelect.IsRoot() )
		m_buttonUpOneLevel.EnableWindow( FALSE );
	else
		m_buttonUpOneLevel.EnableWindow();

	m_arrRetVal.RemoveAll();
	m_bLockChangingsValidation = true;
bool bEmptyFileNameEdit = true;
	if( ! m_strEditorTextInitial.IsEmpty() )
	{
		CString strCurrentWindowText, strCheckInitialEquality = LPCTSTR(m_strEditorTextInitial);
		m_editFileName.GetWindowText( strCurrentWindowText );
		strCurrentWindowText.MakeLower();
		strCheckInitialEquality.MakeLower();
		if( strCheckInitialEquality == strCurrentWindowText )
			bEmptyFileNameEdit = false;
	}
	if( bEmptyFileNameEdit )
		m_editFileName.SetWindowText( _T("") );
	m_bLockChangingsValidation = false;

	if( m_bCurrentDirectorySetOnWalk )
	{

		CExtSafeString strPath = pidlSelect.GetPath();
		if( ! strPath.IsEmpty() )
			SetCurrentDirectory( LPCTSTR(strPath) );
	}

	if( m_bFilesMustExist )
		m_buttonOK.EnableWindow( FALSE );

	m_bHaldingSelectionInList = false;
	return 0L;
}

LRESULT CExtShellDialogFile::OnShellComboLocationChanged( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	lParam;
	if( m_bLockChangingsValidation )
		return 0L;
CExtShellComboBox * pSC = (CExtShellComboBox*)wParam;
	ASSERT( pSC != NULL );
	if( pSC->m_hWnd != m_comboLookIn.m_hWnd )
		return 0L;
	if( m_bHaldingSelectionInTree || m_bHaldingSelectionInList || m_bHaldingHistory )
		return 0L;
	m_bHaldingSelectionInCombo = true;
LPITEMIDLIST pidlSelect = m_comboLookIn.GetCurrentFolderPIDL();
	m_wndShellTree.DelayFocusPIDL( pidlSelect );
	m_wndShellList.DelayFocusPIDL( pidlSelect );
	if( m_wndShellList.FocusedItemGet() < 0 && ::GetFocus() == m_wndShellList.GetSafeHwnd() )
		m_wndShellList.FocusedItemSet( 0 );
	m_bHaldingSelectionInCombo = false;
	return 0L;
}

void CExtShellDialogFile::OnOK()
{
	ASSERT_VALID( this );
	//m_bHelperValidatingListViewSelection = false;
	//_ValidateListViewSelection();

	if( _IfFolderThenOpen() )
		return;

bool bOldFilesMustExist = m_bFilesMustExist;
	m_bFilesMustExist = true;
	m_bHelperValidatingEditor= false;
bool bEditorContentIsValid = _ValidateEditor( false );
bool bAnyRemoved = m_arrRetVal.MakeUnique() > 0 ? true : false;;
	m_bFilesMustExist = bOldFilesMustExist;

	if(		( m_eFDT == __EFDT_SAVE || m_eFDT == __EFDT_SAVE_AS )
		&&	bEditorContentIsValid
		&&	m_arrRetVal.GetSize() == 1
		&&	m_bPromptOverwrite
		)
	{
#if (!defined __EXT_MFC_NO_MSG_BOX)
		if( ::ProfUISMsgBox( GetSafeHwnd(), IDS_EXT_OVERWRITE_THIS_FILE_MSG_BOX_TEXT, NULL, MB_ICONQUESTION|MB_YESNO ) != IDYES )
			return;
#else
		if( ::AfxMessageBox( IDS_EXT_OVERWRITE_THIS_FILE_MSG_BOX_TEXT, MB_ICONQUESTION|MB_YESNO ) != IDYES )
			return;
#endif
	}

	if( (! bEditorContentIsValid) && ( m_bFilesMustExist || m_bPathMustExist ) )
	{
		m_buttonOK.EnableWindow( FALSE );
		return;
	}

CString _strRetVal2;
bool	_bRetValReady = false;

	if( m_bFilesMustExist )
	{
		if( m_arrRetVal.GetSize() == 0 )
		{
			m_buttonOK.EnableWindow( FALSE );
			return;
		}
	}
	else
	{
		INT i, _count = INT( m_arrRetValNames.GetSize() );
		for( i = 0; i < _count; i++ )
		{
			_strRetVal2 += _T(" \"");
			CExtSafeString strArrItem = m_arrRetValNames.GetAt( i );
			_strRetVal2 += LPCTSTR(strArrItem);
			_strRetVal2 += _T("\"");
		}

		if( _count >= 1 )
		{
			_strRetVal2.TrimLeft( _T(" ") );
			_bRetValReady = true;
		}
	}


	if( m_bResolveLinks )
		_ResolveLinksInResult();
CString _strRetVal;
	if( bAnyRemoved )
	{
		INT nIndex, nCount = m_arrRetVal.GetSize();
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtPIDL * pPIDL = m_arrRetVal.GetObjAt( nIndex );
			ASSERT( pPIDL != NULL );
			ASSERT( ! pPIDL->IsEmpty() );
			CExtSafeString strTmp = pPIDL->GetPath();
			if( strTmp.IsEmpty() )
				strTmp = pPIDL->GetDisplayNameOf();
			if( nIndex > 0 )
				_strRetVal += _T(" ");
			_strRetVal += _T("\"");
			_strRetVal += LPCTSTR(strTmp);
			_strRetVal += _T("\"");
		}
	}
	else
		m_editFileName.GetWindowText( _strRetVal );

	m_strRetValNames = (
			_bRetValReady
		&&	( m_eFDT == __EFDT_OPEN_SINGLE || m_eFDT == __EFDT_OPEN_MULTIPLE ) 
		) 
		? LPCTSTR(_strRetVal2) : LPCTSTR(_strRetVal);
	m_pidlRetValFolder = m_wndShellList.GetCurrentFolderPIDL();
	m_strRetValFolder = m_wndShellList.GetCurrentFolderPath();
CExtSafeString strSectionSettingsEntry, strSectionNameCompany, strSectionNameProduct;
	_GetSettingsLocationStrings( strSectionSettingsEntry, strSectionNameCompany, strSectionNameProduct );

	if( m_bSaveRestoreInterriorOptions )
		_InterriorOptionsSerialize( LPCTSTR(strSectionSettingsEntry), LPCTSTR(strSectionNameCompany), LPCTSTR(strSectionNameProduct), true );

	if( m_bSaveRestoreShellLocation )
	{
		CExtPIDL pidl = m_wndShellList.GetCurrentFolderPIDL();
		if( ! pidl.IsEmpty() )
		{
			VERIFY( pidl.Serialize( LPCTSTR(strSectionSettingsEntry), LPCTSTR(strSectionNameCompany), LPCTSTR(strSectionNameProduct), true ) );
		}
	}
	else if(	m_bCurrentDirectoryRestoreFinallyOnOK
			&&	(! m_strCurrentDirectoryToRestore.IsEmpty() )
			)
			SetCurrentDirectory( LPCTSTR(m_strCurrentDirectoryToRestore) );
	_CleanUpOnClose();

	#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		::OnOK();
}

void CExtShellDialogFile::OnCancel()
{
	ASSERT_VALID( this );
CExtSafeString strSectionSettingsEntry, strSectionNameCompany, strSectionNameProduct;
	_GetSettingsLocationStrings( strSectionSettingsEntry, strSectionNameCompany, strSectionNameProduct );

	if( m_bSaveRestoreInterriorOptions )
		_InterriorOptionsSerialize( LPCTSTR(strSectionSettingsEntry), LPCTSTR(strSectionNameCompany), LPCTSTR(strSectionNameProduct), true );

	if( m_bSaveRestoreShellLocation )
	{
		CExtPIDL pidl = m_wndShellList.GetCurrentFolderPIDL();
		if( ! pidl.IsEmpty() )
		{
			VERIFY( pidl.Serialize( LPCTSTR(strSectionSettingsEntry), LPCTSTR(strSectionNameCompany), LPCTSTR(strSectionNameProduct), true ) );
		}
	}
	else if(	m_bCurrentDirectoryRestoreFinallyOnCancel
			&&	(! m_strCurrentDirectoryToRestore.IsEmpty() )
			)
			SetCurrentDirectory( LPCTSTR(m_strCurrentDirectoryToRestore) );
	_CleanUpOnClose();

#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		::OnCancel();
}

bool CExtShellDialogFile::_IfFolderThenOpen()
{
	ASSERT_VALID( this );

CString str;
	m_editFileName.GetWindowText( str );
	if( ! str.IsEmpty() )
	{
		LVFINDINFO _info;
		::memset( &_info, 0, sizeof(LVFINDINFO) );
		_info.flags = LVFI_STRING;
		_info.psz = str;
		int nIndex = m_wndShellList.FindItem( & _info );
		if( nIndex < 0 )
		{
			::memset( &_info, 0, sizeof(LVFINDINFO) );
			_info.flags = LVFI_PARTIAL|LVFI_STRING;
			_info.psz = str;
			nIndex = m_wndShellList.FindItem( & _info );
		}
		if( nIndex != -1 )
		{
			CExtShellItemData * pShellItemData = (CExtShellItemData*)m_wndShellList.GetItemData( nIndex );
			ASSERT( pShellItemData != NULL );
			CExtCIP_SF _SF;
			if( pShellItemData->m_pidlAbsolute.IsFolder() )
			{
				m_wndShellTree.DelayFocusPIDL( pShellItemData->m_pidlAbsolute.GetPtr() );
				return true;
			}
		}
		else
		{
			CExtPIDL pidlFolder( LPCTSTR(str), m_hWnd );
			if( pidlFolder != NULL )
			{
				if( pidlFolder.IsFolder() )
				{
					m_wndShellTree.DelayFocusPIDL( pidlFolder.GetPtr() );
					return true;
				}
			}
		}
	}

	return false;
}

void CExtShellDialogFile::_CleanUpOnClose()
{
	ASSERT_VALID( this );
	m_listCollapsedHWNDs.RemoveAll();
	m_listMovedHWNDs.RemoveAll();
	m_nSavedCollapsedHeight = 0;
	m_nCollapsedWindowHeight = 0;
	g_CmdManager->ProfileDestroy( LPCTSTR(m_strCommandProfileName), true );
	m_bHelperCollapsignAnimation = false;
	m_bLockChangingsValidation = false;
	m_strCurrentDirectoryToRestore.Empty();
}

void CExtShellDialogFile::_ResolveLinksInResult()
{
	ASSERT_VALID( this );
INT nIndex, nCount = INT( m_arrRetVal.GetSize() );
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtPIDL * pPIDL = m_arrRetVal.GetObjAt( nIndex );
		ASSERT( pPIDL != NULL );
		ASSERT( ! pPIDL->IsEmpty() );
		CExtSafeString strTmp = pPIDL->GetPath();
		if( ! strTmp.IsEmpty() )
		{
			TCHAR pszPath[MAX_PATH] = { 0 };
			BOOL bRetVal = ::AfxResolveShortcut( this, strTmp, pszPath, MAX_PATH );
			if( bRetVal && m_bResolveLinks )
			{
				CExtPIDL pidlFromShortcut( pszPath, m_hWnd );
				if( ! pidlFromShortcut.IsEmpty() )
					m_arrRetVal.SetAt( nIndex, pidlFromShortcut );
			}
		}
	}

	nCount = INT( m_arrRetValNames.GetSize() );
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtSafeString strTmp = m_arrRetValNames.GetAt( nIndex );
		if( ! strTmp.IsEmpty() )
		{
			TCHAR pszPath[MAX_PATH] = { 0 };
			BOOL bRetVal = ::AfxResolveShortcut( this, strTmp, pszPath, MAX_PATH );
			if( bRetVal && m_bResolveLinks )
				m_arrRetValNames.SetAt( nIndex, __EXT_MFC_SAFE_LPCTSTR(LPCTSTR(pszPath)) );
		}
	}
}

void CExtShellDialogFile::OnItemChangedShellList(NMHDR* pNMHDR, LRESULT* pResult)
{
	ASSERT_VALID( this );
	(*pResult) = 0;
	if( m_bLockChangingsValidation )
		return;
	if( pNMHDR->code == LVN_ITEMCHANGED )
	{
		NM_LISTVIEW* pnmlv = (NM_LISTVIEW*)pNMHDR;
		if( ( pnmlv->uNewState & LVIS_SELECTED ) != ( pnmlv->uOldState & LVIS_SELECTED ) )
		{
			CList< INT, INT > _listSelectedItem;
			m_wndShellList.GetSelectedItemsList( _listSelectedItem );
			if(		( ! m_bEnableButtonDelete )
				||	( _listSelectedItem.GetCount() == 1 && m_wndShellList._SpecificItem_CheckStatus( _listSelectedItem ) ) 
				||	( _listSelectedItem.GetCount() > 1 && _listSelectedItem.GetCount() == m_wndShellList._SpecificItems_GetCount( _listSelectedItem ) )
				||	_listSelectedItem.GetCount() == 0
				)
				m_buttonDelete.EnableWindow( FALSE );
			else
				m_buttonDelete.EnableWindow();

			HTREEITEM htiSelect = m_wndShellTree.GetFocusedItem();
				ASSERT( htiSelect != NULL );
			CExtTreeCtrl::TREEITEMINFO_t & _TII = m_wndShellTree.TreeItemInfoGet( htiSelect );
			CExtShellItemData * pData = (CExtShellItemData*)_TII.m_lParam; //new CExtShellItemData;
			ASSERT( pData != NULL );
			CExtSafeString strPath = pData->m_pidlAbsolute.GetPath();
			if( strPath.IsEmpty() && ( m_eFDT == __EFDT_SAVE || m_eFDT == __EFDT_SAVE_AS ) )
				m_buttonOK.EnableWindow( FALSE );
			else
				m_buttonOK.EnableWindow( _ValidateEditor( false ) ? TRUE : FALSE );

			KillTimer( m_nValidateListViewSelectionTimerID );
			if( ! ( m_bHelperValidatingListViewSelection || m_bHelperValidatingEditor ) )
				SetTimer( m_nValidateListViewSelectionTimerID, m_nValidateListViewSelectionTimerPeriod, NULL );
		}
	}
}

void CExtShellDialogFile::OnSelendokComboFileTypes() 
{
	ASSERT_VALID( this );
CExtSafeStringList _listExtensions;
	m_comboFileTypes.GetExtensionsByTitle( NULL, _listExtensions );
	if( _listExtensions.GetCount() == 0 )
		return;

CString strRestore;
bool bAppendExt = false;
	if( ( m_eFDT == __EFDT_SAVE || m_eFDT == __EFDT_SAVE_AS ) && m_editFileName.GetSafeHwnd() != NULL )
	{
		m_editFileName.GetWindowText( strRestore );
		strRestore.TrimLeft( _T(" \r\n\t") );
		strRestore.TrimRight( _T(" \r\n\t") );
		if( ! strRestore.IsEmpty() )
		{
			int nPos = (int)strRestore.ReverseFind( _T('.') );
			if( nPos >= 0 )
			{
				strRestore = strRestore.Left( nPos );
				bAppendExt = true;
			}
		}
	}

	m_wndShellList.ExtensionsListSet( _listExtensions );
	m_wndShellList.Refresh();
	if( m_wndShellList.FocusedItemGet() < 0 && ::GetFocus() == m_wndShellList.GetSafeHwnd() )
		m_wndShellList.FocusedItemSet( 0 );

	if( ! strRestore.IsEmpty() )
	{
		if( bAppendExt && _listExtensions.GetCount() > 0 )
		{
			bAppendExt = false;
			CString strExtSpec = LPCTSTR( _listExtensions.GetHead() );
			int nPos = (int)strExtSpec.ReverseFind( _T('.') );
			if( nPos >= 0 )
			{
				CString strAdd = LPCTSTR( strExtSpec.Right( strExtSpec.GetLength() - nPos ) );
				if( strAdd.Find( _T('*') ) < 0 && strAdd.Find( _T('?') ) < 0 )
				{
					bAppendExt = true;
					strRestore += strAdd;
				}
			}
		}
		if( bAppendExt )
		{
			int nLen = int( strRestore.GetLength() );
			if( nLen > 1 && strRestore[0] == _T('\"') && strRestore[nLen-1] != _T('\"') )
				strRestore += _T('\"');
			m_editFileName.SetWindowText( LPCTSTR(strRestore) );
		}
	}
}

void CExtShellDialogFile::OnButtonBack() 
{
	ASSERT_VALID( this );
	_HistoryBack();
}

void CExtShellDialogFile::OnButtonForward() 
{
	ASSERT_VALID( this );
	_HistoryForward();
}

void CExtShellDialogFile::OnButtonUpOneLevel()
{
	ASSERT_VALID( this );
HTREEITEM htiSelect = m_wndShellTree.GetFocusedItem();
	ASSERT( htiSelect != NULL );
HTREEITEM htiParent = m_wndShellTree.GetNextItem( htiSelect, TVGN_PARENT );
	if( htiParent == TVI_ROOT || htiParent == NULL )
		return;

CExtTreeCtrl::TREEITEMINFO_t & _TII = m_wndShellTree.TreeItemInfoGet( htiParent );
CExtShellItemData * pData = (CExtShellItemData*)_TII.m_lParam;
	ASSERT( pData != NULL );
LPITEMIDLIST pidlSelect = (LPITEMIDLIST)pData->m_pidlAbsolute.GetPtr();
	ASSERT( pidlSelect != NULL );

	m_wndShellTree.DelayFocusPIDL( pidlSelect );
	m_wndShellList.DelayFocusPIDL( pidlSelect );
	m_comboLookIn.DelayFocusPIDL( pidlSelect );
}

void CExtShellDialogFile::OnButtonDelete() 
{
	ASSERT_VALID( this );
	m_wndShellList.PostMessage( WM_KEYDOWN, (WPARAM) VK_DELETE );
}

void CExtShellDialogFile::_HistoryBack()
{
	ASSERT_VALID( this );
INT _nHistoryCount = INT( m_arrayHistory.GetCount() );
	if( _nHistoryCount == 0 || _nHistoryCount == 1 || m_nCurrentHistoryPosition == 0 )
	{
		m_buttonBack.EnableWindow( FALSE );
		return;
	}
	if( m_nCurrentHistoryPosition > _nHistoryCount )
		m_nCurrentHistoryPosition = _nHistoryCount;
	m_buttonForward.EnableWindow();
LPCITEMIDLIST pidlBack = m_arrayHistory.GetAt( m_nCurrentHistoryPosition - 1 );
	m_nCurrentHistoryPosition --;
	if( m_nCurrentHistoryPosition == 0 )
		m_buttonBack.EnableWindow( FALSE );
	m_bHaldingHistory = true;
	m_wndShellTree.FocusPIDL( pidlBack );
	m_wndShellList.FocusPIDL( pidlBack );
	m_comboLookIn.FocusPIDL( pidlBack );
	m_bHaldingHistory = false;
CExtPIDL pidlSelect = m_wndShellList.GetCurrentFolderPIDL();
BOOL bEnable = pidlSelect.IsRoot() ? FALSE : TRUE;
	m_buttonUpOneLevel.EnableWindow( bEnable );
}

void CExtShellDialogFile::_HistoryForward()
{
	ASSERT_VALID( this );
INT _nHistoryCount = INT( m_arrayHistory.GetCount() );
	if(		_nHistoryCount == 0
		||	( m_nCurrentHistoryPosition + 1 ) >= _nHistoryCount
		)
	{
		m_buttonForward.EnableWindow( FALSE );
		return;
	}
	m_buttonBack.EnableWindow();
LPCITEMIDLIST pidlForward = m_arrayHistory.GetAt( m_nCurrentHistoryPosition + 1 );
	m_nCurrentHistoryPosition ++;
	if( ( m_nCurrentHistoryPosition + 1 ) >= _nHistoryCount )
		m_buttonForward.EnableWindow( FALSE );
	m_bHaldingHistory = true;
	m_wndShellTree.FocusPIDL( pidlForward );
	m_wndShellList.FocusPIDL( pidlForward );
	m_comboLookIn.FocusPIDL( pidlForward );
	m_bHaldingHistory = false;
CExtPIDL pidlSelect = m_wndShellList.GetCurrentFolderPIDL();
BOOL bEnable = pidlSelect.IsRoot() ? FALSE : TRUE;
	m_buttonUpOneLevel.EnableWindow( bEnable );
}

void CExtShellDialogFile::OnButtonCreateNewFolder() 
{
	ASSERT_VALID( this );
CExtShellDialogCreateFolder dlgShellCreateFolder( this );
INT retVal = (INT)dlgShellCreateFolder.DoModal();
	if( retVal != IDOK )
		return;
HTREEITEM htiCurrent = m_wndShellTree.GetFocusedItem();
	m_wndShellTree.Expand( htiCurrent, TVE_EXPAND );
HTREEITEM htiNew = m_wndShellTree._CreateFolder( dlgShellCreateFolder.GetNameFolder(), htiCurrent );
	if( htiNew != NULL )
		m_wndShellTree.FocusItem( htiNew, true, true );
}

void CExtShellDialogFile::_ShellListExtensionSet()
{
	ASSERT_VALID( this );
CExtSafeStringList _listExtensions;
	m_comboFileTypes.GetExtensionsByTitle( NULL, _listExtensions );
	if( _listExtensions.GetCount() == 0 )
		return;
	m_wndShellList.ExtensionsListSet( _listExtensions );
}


void CExtShellDialogFile::OnChangeEditFileName() 
{
	KillTimer( m_nValidateNamesTimerID );
	if( m_bLockChangingsValidation )
		return;
	if( ! ( m_bHelperValidatingListViewSelection || m_bHelperValidatingEditor ) )
		SetTimer( m_nValidateNamesTimerID, m_nValidateNamesTimerPeriod, NULL );
}

void CExtShellDialogFile::OnTimer(__EXT_MFC_UINT_PTR nIDEvent) 
{
	ASSERT_VALID( this );
	if( nIDEvent == m_nValidateNamesTimerID )
	{
		KillTimer( nIDEvent );
		if( m_bLockChangingsValidation )
			return;

		HTREEITEM htiSelect = m_wndShellTree.GetFocusedItem();
		ASSERT( htiSelect != NULL );
		CExtTreeCtrl::TREEITEMINFO_t & _TII = m_wndShellTree.TreeItemInfoGet( htiSelect );
		CExtShellItemData * pData = (CExtShellItemData*)_TII.m_lParam; //new CExtShellItemData;
		ASSERT( pData != NULL );
		CExtSafeString strPath = pData->m_pidlAbsolute.GetPath();
		if( strPath.IsEmpty() && ( m_eFDT == __EFDT_SAVE || m_eFDT == __EFDT_SAVE_AS ) )
			m_buttonOK.EnableWindow( FALSE );
		else
			m_buttonOK.EnableWindow( _ValidateEditor( true ) ? TRUE : FALSE );

		return;
	} // if( nIDEvent == m_nValidateNamesTimerID )
	if( nIDEvent == m_nValidateListViewSelectionTimerID )
	{
		KillTimer( nIDEvent );
		if( m_bLockChangingsValidation )
			return;
		_ValidateListViewSelection();

		HTREEITEM htiSelect = m_wndShellTree.GetFocusedItem();
		ASSERT( htiSelect != NULL );
		CExtTreeCtrl::TREEITEMINFO_t & _TII = m_wndShellTree.TreeItemInfoGet( htiSelect );
		CExtShellItemData * pData = (CExtShellItemData*)_TII.m_lParam; //new CExtShellItemData;
		ASSERT( pData != NULL );
		CExtSafeString strPath = pData->m_pidlAbsolute.GetPath();
		if( strPath.IsEmpty() && ( m_eFDT == __EFDT_SAVE || m_eFDT == __EFDT_SAVE_AS ) )
			m_buttonOK.EnableWindow( FALSE );
		else
			m_buttonOK.EnableWindow( _ValidateEditor( false ) ? TRUE : FALSE );

		return;
	} // if( nIDEvent == m_nValidateListViewSelectionTimerID )

#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		::OnTimer( nIDEvent );
}

void CExtShellDialogFile::_ValidateListViewSelection()
{
	ASSERT_VALID( this );
	if( m_bHelperValidatingListViewSelection || m_bHelperValidatingEditor )
		return;
	m_bHelperValidatingListViewSelection = true;

INT nSubItemIndex = -1, nWalkIndex, nCount = INT( m_wndShellList.m_arrColumnTypes.GetSize() );
	for( nWalkIndex = 0; nWalkIndex < nCount; nWalkIndex ++ )
	{
		if( m_wndShellList.m_arrColumnTypes[ nWalkIndex ] == INT(CExtShellListCtrl::__ESLCT_NAME) )
		{
			nSubItemIndex = nWalkIndex;
			break;
		}
	}
	if( nSubItemIndex < 0 )
		nSubItemIndex = 0;

CString strComposedEditorText;
CList < INT, INT > _listSelectedItems;
	m_wndShellList.GetSelectedItemsList( _listSelectedItems );
	nCount = INT( _listSelectedItems.GetCount() );
POSITION pos = _listSelectedItems.GetHeadPosition();
	for( nWalkIndex = 0; pos != NULL; nWalkIndex ++ )
	{
		ASSERT( nWalkIndex < nCount );
		INT nItemIndex = _listSelectedItems.GetNext( pos );
		CExtShellItemData * pShellItemData = (CExtShellItemData*)m_wndShellList.GetItemData( nItemIndex );
		ASSERT( pShellItemData != NULL );
		DWORD dwAttributes = pShellItemData->m_pidlAbsolute.GetAttributesOf( __EXT_DEFAULT_ATTRIBUTES_FOR_CExtPIDL_GetAttributesOf__ );
		if(		( dwAttributes & SFGAO_FILESYSTEM ) == 0
			||	( dwAttributes & SFGAO_FOLDER ) != 0
			||	pShellItemData->m_pidlAbsolute.IsRoot()
			)
			continue;
		//CExtSafeString strTmp = pShellItemData->m_pidlAbsolute.GetDisplayNameOf();
		CString strTmp = m_wndShellList.GetItemText( nItemIndex, nSubItemIndex );
		if( nCount > 1 || strTmp.Find( _T(' ') ) > 0 )
		{
			strTmp.TrimLeft( _T("\" ") );
			strTmp.TrimRight( _T("\" ") );
			strTmp.Insert( 0, _T('\"') );
			strTmp += _T("\"");
			if( nWalkIndex < ( nCount - 1 ) )
				strTmp += _T(" ");
		}
		strComposedEditorText += LPCTSTR(strTmp);
	} // for( nWalkIndex = 0; pos != NULL; nWalkIndex ++ )

CString strCurrentText;
	m_editFileName.GetWindowText( strCurrentText );
	if( _tcsicmp( LPCTSTR(strCurrentText), LPCTSTR(strComposedEditorText) ) != 0 )
	{
		if( strComposedEditorText.IsEmpty() )
		{
			bool bEmptyFileNameEdit = true;
			if( ! m_strEditorTextInitial.IsEmpty() )
			{
				CString strCurrentWindowText, strCheckInitialEquality = LPCTSTR(m_strEditorTextInitial);
				m_editFileName.GetWindowText( strCurrentWindowText );
				strCurrentWindowText.MakeLower();
				strCheckInitialEquality.MakeLower();
				if( strCheckInitialEquality == strCurrentWindowText )
					bEmptyFileNameEdit = false;
			}
			if( bEmptyFileNameEdit )
				m_editFileName.SetWindowText( _T("") );
			m_buttonOK.EnableWindow( FALSE );
			m_arrRetVal.RemoveAll();
		}
		else if( ::GetFocus() == m_wndShellList.GetSafeHwnd() )
			m_editFileName.SetWindowText( LPCTSTR(strComposedEditorText) );
		m_editFileName.SetSel( -1, -1 );
	}
	m_bHelperValidatingListViewSelection = false;
}

bool CExtShellDialogFile::_ValidateEditor( bool bAllowChangingListView )
{
	ASSERT_VALID( this );
	if( m_bHelperValidatingListViewSelection || m_bHelperValidatingEditor )
		return false;

// 	if( ! m_bFilesMustExist )
// 		return true;
// 	m_bHelperValidatingEditor = true;
bool bRetVal = true;
CString _strMultiplePathValue;
	m_editFileName.GetWindowText( _strMultiplePathValue );
	_strMultiplePathValue.TrimLeft( _T(" \r\n\t") );
	_strMultiplePathValue.TrimRight( _T(" \r\n\t") );

	if( _strMultiplePathValue.IsEmpty() )
		return false;

	if( ! ( m_bFilesMustExist || m_bPathMustExist ) )
		return true;

	m_bHelperValidatingEditor = true;
	if( m_eFDT != __EFDT_OPEN_MULTIPLE && _strMultiplePathValue.Find( _T(' ') ) >= 0 )
	{
		CString strTmp = LPCTSTR(_strMultiplePathValue);
		strTmp.Replace( _T("\""), _T("") );
		_strMultiplePathValue.Format( _T("\"%s\""), (LPCTSTR)strTmp );
	}

	if( ! _strMultiplePathValue.IsEmpty() )
	{
		// build string map first
		m_arrRetVal.Empty();
		CMapStringToString _mapPaths;
		CString strTmp;
		bool bCommaMode = false;
		LPCTSTR ptr = LPCTSTR(_strMultiplePathValue);
		for( ; (*ptr) != _T('\0'); ptr ++ )
		{
			if(		( ( (*ptr) == _T(' ') || (*ptr) == _T('\t') ) && ( ! bCommaMode ) )
				||	( (*ptr) == _T('\"') && bCommaMode )
				)
			{
				strTmp.TrimLeft( _T(" \r\n\t\"") );
				strTmp.TrimRight( _T(" \r\n\t\"") );
				if( ! strTmp.IsEmpty() )
				{
					CString strLower = strTmp;
					strLower.MakeLower();
					_mapPaths.SetAt( LPCTSTR(strLower), LPCTSTR(strTmp) );
				}
				strTmp.Empty();
				bCommaMode = false;
				continue;
			}
			if( (*ptr) == _T('\"') )
			{
				bCommaMode = true;
				continue;
			}
			strTmp += (*ptr);
		} // for( ; (*ptr) != _T('\0'); ptr ++ )
		strTmp.TrimLeft( _T(" \r\n\t\"") );
		strTmp.TrimRight( _T(" \r\n\t\"") );
		if( ! strTmp.IsEmpty() )
		{
			CString strLower = strTmp;
			strLower.MakeLower();
			_mapPaths.SetAt( LPCTSTR(strLower), LPCTSTR(strTmp) );
		}
		strTmp.Empty();

		// validate existing selection in list control
		m_arrRetValNames.RemoveAll();

		CList < INT, INT > _listSelectedItems;
		m_wndShellList.GetSelectedItemsList( _listSelectedItems );
		POSITION pos = _listSelectedItems.GetHeadPosition();
		for( ; pos != NULL; )
		{
			INT nItemIndex = _listSelectedItems.GetNext( pos );
			CExtShellItemData * pShellItemData = (CExtShellItemData*)m_wndShellList.GetItemData( nItemIndex );
			ASSERT( pShellItemData != NULL );
			DWORD dwAttributes = pShellItemData->m_pidlAbsolute.GetAttributesOf( __EXT_DEFAULT_ATTRIBUTES_FOR_CExtPIDL_GetAttributesOf__ );
			if(		( dwAttributes & SFGAO_FILESYSTEM ) == 0
				||	( dwAttributes & SFGAO_FOLDER ) != 0
				||	pShellItemData->m_pidlAbsolute.IsRoot()
				)
			{
				// un-select such items
				if( bAllowChangingListView )
 					m_wndShellList.SetItemState( nItemIndex, 0, LVIS_SELECTED ); // un-select item
				continue;
			}
			CExtSafeString strTmp = pShellItemData->m_pidlAbsolute.GetDisplayNameOf();
			strTmp.MakeLower();
			if( _mapPaths.RemoveKey( LPCTSTR(strTmp) ) )
			{
				m_arrRetVal.Add( pShellItemData->m_pidlAbsolute );
				m_arrRetValNames.Add( pShellItemData->m_pidlAbsolute.GetPath() );
				continue;
			}
			if( bAllowChangingListView )
 				m_wndShellList.SetItemState( nItemIndex, 0, LVIS_SELECTED ); // un-select item
		} // for( ; pos != NULL; )

		// add selection from map
		pos = _mapPaths.GetStartPosition();
		for( ; pos != NULL; )
		{
			CString strFileName, strFileNameCS;
			_mapPaths.GetNextAssoc( pos, strFileName, strFileNameCS );
			ASSERT( ! strFileName.IsEmpty() );
//TRACE1( "file %s\r\n", LPCTSTR(strFileName) );
			INT nItemIndex = m_wndShellList.FindItemByName( LPCTSTR(strFileName) );
			if( nItemIndex < 0 )
			{

				TCHAR strCurrentDirectory[ _MAX_PATH + 1 ], strFullPath[ _MAX_PATH + 1 ];
				::memset( strCurrentDirectory, 0, _MAX_PATH + 1 );
				::GetCurrentDirectory( _MAX_PATH, strCurrentDirectory );
				CString strBrowsedDirectory = m_wndShellList.GetCurrentFolderPath();
				if( strBrowsedDirectory.IsEmpty() )
					strBrowsedDirectory = strCurrentDirectory;
				VERIFY( ::SetCurrentDirectory( strBrowsedDirectory ) );
				bool bFoundExplicitlyEntered = false;
				if(		CExtShellBase::stat_FileExist( LPCTSTR(strFileNameCS) ) 
					&&	( ! CExtShellBase::stat_DirExist( LPCTSTR(strFileNameCS) ) )
					&&	::AfxFullPath( strFullPath, LPCTSTR(strFileNameCS) )
					)
					bFoundExplicitlyEntered = true; // OK, file is in some other folder
				else
					::memset( strFullPath, 0, _MAX_PATH + 1 );

				if( ! bFoundExplicitlyEntered )
				{ // at this step, try to append all the currently browsed extensions

					CExtSafeStringList _listExtensions;
					m_comboFileTypes.GetExtensionsByTitle( NULL, _listExtensions );
					CExtSafeString strTryLeftPart = strFileNameCS;
					strTryLeftPart.TrimLeft( _T(" \r\n\t\"") );
					strTryLeftPart.TrimRight( _T(" \r\n\t\"") );
					POSITION pos = _listExtensions.GetHeadPosition();
					for( ; pos != NULL ; )
					{
						CExtSafeString strExtension = _listExtensions.GetNext( pos );
						strExtension.TrimLeft( _T(" \r\n\t\"*?") );
						strExtension.TrimRight( _T(" \r\n\t\"*?") );
						if(		strExtension.IsEmpty()
							||	strExtension.Find( _T('*') ) > 0
							||	strExtension.Find( _T('?') ) > 0
							)
							continue;
						CExtSafeString strTry = strTryLeftPart;
						if( strExtension[0] != _T('.') )
							strTry += _T('.');
						strTry += strExtension;
						if(		CExtShellBase::stat_FileExist( LPCTSTR(strTry) ) 
							&&	( ! CExtShellBase::stat_DirExist( LPCTSTR(strTry) ) )
							&&	::AfxFullPath( strFullPath, LPCTSTR(strTry) )
							)
						{
							bFoundExplicitlyEntered = true; // OK, file name was entered without extension
							break;
						}
					}

				} // at this step, try to append all the currently browsed extensions
				VERIFY( ::SetCurrentDirectory( strCurrentDirectory ) );

				if( bFoundExplicitlyEntered )
				{
					CExtPIDL pidlFromPath( strFullPath, m_hWnd );
					m_arrRetVal.Add( pidlFromPath );
					m_arrRetValNames.Add( __EXT_MFC_SAFE_LPCTSTR(LPCTSTR(strFullPath)) );
					continue;
				}

				CExtSafeStringList _listExtensions;
				m_comboFileTypes.GetExtensionsByTitle( NULL, _listExtensions );
				POSITION pos = _listExtensions.GetHeadPosition();
				CExtSafeString strExtension = _T("");
				if( pos != NULL )
				{
					strExtension = _listExtensions.GetNext( pos );
					strExtension.TrimLeft( _T(" \r\n\t\"*?") );
					strExtension.TrimRight( _T(" \r\n\t\"") );
					if( strExtension == _T(".*") )
						strExtension.Empty();
				}
				CExtSafeString _strNothingPath;
				bool bAddBrowsedDirectory = true;
				INT nLenFileName = INT( strFileName.GetLength() );
				if( nLenFileName >= 1 && ( strFileName.GetAt(0) == _T('\\') || strFileName.GetAt(0) == _T('/') ) )
					bAddBrowsedDirectory = false;
				else if ( nLenFileName >= 2 && IsCharAlpha(strFileName.GetAt(0)) && strFileName.GetAt(1) == _T(':') )
				{
//					if( nLenFileName >= 3 && strFileName.GetAt(2) != _T('\\') && strFileName.GetAt(2) != _T('/') )
//					{
//						strFileName = strFileName.Right( nLenFileName - 2 );
//					}
//					else
						bAddBrowsedDirectory = false;
				}
				if( bAddBrowsedDirectory )
				{
					_strNothingPath += LPCTSTR(strBrowsedDirectory);
					INT nLenCheck = INT( _strNothingPath.GetLength() );
					if( nLenCheck > 0 && _strNothingPath.GetAt( nLenCheck - 1 ) != _T('\\') )
					_strNothingPath += _T("\\");
				}
				if( ! strFileName.IsEmpty() )
				{
					_strNothingPath += LPCTSTR(strFileNameCS);
					bool bAddExtension = true;
					if( ! strExtension.IsEmpty() )
					{
						CExtSafeString strCheckExtension = LPCTSTR(strExtension);
						strCheckExtension.MakeLower();
						CExtSafeString strCheckFileName = LPCTSTR(strFileName);
						strCheckFileName.MakeLower();
						INT nExtLen = INT( strCheckExtension.GetLength() );
						if( nExtLen < INT( strCheckFileName.GetLength() ) )
						{
							CExtSafeString strRightPartOfFileName = strCheckFileName.Right( nExtLen );
							if( strRightPartOfFileName == strCheckExtension )
								bAddExtension = false;
						}
					}
					if( bAddExtension )
						_strNothingPath += strExtension;
					m_arrRetValNames.Add( _strNothingPath );
				}

				bRetVal = false;
				continue;
			}

			CExtShellItemData * pShellItemData = (CExtShellItemData*)m_wndShellList.GetItemData( nItemIndex );
			ASSERT( pShellItemData != NULL );
			DWORD dwAttributes = pShellItemData->m_pidlAbsolute.GetAttributesOf( __EXT_DEFAULT_ATTRIBUTES_FOR_CExtPIDL_GetAttributesOf__ );
			if(		( dwAttributes & SFGAO_FILESYSTEM ) == 0
				||	( dwAttributes & SFGAO_FOLDER ) != 0
				||	pShellItemData->m_pidlAbsolute.IsRoot()
				)
			{
				bRetVal = false;
				continue;
			}

			m_arrRetVal.Add( pShellItemData->m_pidlAbsolute );
			CExtSafeString strPathOfItem = pShellItemData->m_pidlAbsolute.GetPath();
			m_arrRetValNames.Add( strPathOfItem );
			if( bAllowChangingListView )
				m_wndShellList.SetItemState( nItemIndex, LVIS_SELECTED, LVIS_SELECTED ); // select item
		} // for( ; pos != NULL; )

	} // if( ! _strMultiplePathValue.IsEmpty() )
	else
		bRetVal = false;
bool bRetValCheckedFiles = true;
	if( m_bFilesMustExist )
		bRetValCheckedFiles = bRetVal;
	if( m_bPathMustExist )
	{
		if( _strMultiplePathValue.IsEmpty() )
			bRetVal = false;
		else
		{
			if( bRetVal )
			{
				if( m_arrRetValNames.GetSize() != 1 )
					bRetVal = false;
			} // if( bRetVal )
			else
			{
				if( CExtShellBase::stat_DirExist( LPCTSTR(_strMultiplePathValue) ) )
					bRetVal = true;
				else
				{
					TCHAR	strDrive[    _MAX_PATH + 1 ],
							strDir[      _MAX_PATH + 1 ],
							strFileName[ _MAX_PATH + 1 ],
							strFileExt[  _MAX_PATH + 1 ];
					::memset( strDrive,    0, _MAX_PATH + 1 );
					::memset( strDir,      0, _MAX_PATH + 1 );
					::memset( strFileName, 0, _MAX_PATH + 1 );
					::memset( strFileExt,  0, _MAX_PATH + 1 );
					__EXT_MFC_SPLITPATH(
						LPCTSTR(_strMultiplePathValue),
						strDrive, _MAX_PATH,
						strDir, _MAX_PATH,
						strFileName, _MAX_PATH,
						strFileExt, _MAX_PATH
						);
					CString strCheck = strDrive;

// 					if( ! CExtShellBase::stat_DirExist( LPCTSTR(strDir) ) )
// 						::memset( strDir, 0, _MAX_PATH + 1 );

					strCheck += strDir;

					if( strCheck.IsEmpty() )
						bRetVal = true;
					else if( CExtShellBase::stat_DirExist( LPCTSTR(strCheck) ) )
						bRetVal = true;
					else
					{
						strCheck.TrimRight( _T("\\/") );
						strCheck += _T("\\*.*");
						if( CExtShellBase::stat_DirExist( LPCTSTR(strCheck) ) )
							bRetVal = true;
					}
				}
			} // else from if( bRetVal )
		}
	} // if( m_bPathMustExist )
	else if( m_bFilesMustExist )
		bRetVal = bRetValCheckedFiles;
	else
		bRetVal = true;
	m_bHelperValidatingEditor = false;
	return bRetVal;
}

void CExtShellDialogFile::OnListViewMode_Tiles()
{
	ASSERT_VALID( this );
	if( ( ( m_wndShellList.GetStyle() & LVS_TYPEMASK ) == LVS_SMALLICON ) ? TRUE : FALSE )
		return;
	m_wndShellList.ModifyStyle( LVS_TYPEMASK, LVS_SMALLICON );
	m_wndShellList.Invalidate();
	m_wndShellList.UpdateWindow();
}

void CExtShellDialogFile::OnListViewMode_UpdateTiles( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	pCmdUI->Enable();
	pCmdUI->SetRadio( ( ( m_wndShellList.GetStyle() & LVS_TYPEMASK ) == LVS_SMALLICON ) ? TRUE : FALSE );
}

void CExtShellDialogFile::OnListViewMode_Icons()
{
	ASSERT_VALID( this );
	if( ( ( m_wndShellList.GetStyle() & LVS_TYPEMASK ) == LVS_ICON ) ? TRUE : FALSE )
		return;
	m_wndShellList.ModifyStyle( LVS_TYPEMASK, LVS_ICON );
	m_wndShellList.Invalidate();
	m_wndShellList.UpdateWindow();
}

void CExtShellDialogFile::OnListViewMode_UpdateIcons( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	pCmdUI->Enable();
	pCmdUI->SetRadio( ( ( m_wndShellList.GetStyle() & LVS_TYPEMASK ) == LVS_ICON ) ? TRUE : FALSE );
}

void CExtShellDialogFile::OnListViewMode_List()
{
	ASSERT_VALID( this );
	if( ( ( m_wndShellList.GetStyle() & LVS_TYPEMASK ) == LVS_LIST ) ? TRUE : FALSE )
		return;
	m_wndShellList.ModifyStyle( LVS_TYPEMASK, LVS_LIST );
	m_wndShellList.Invalidate();
	m_wndShellList.UpdateWindow();
}

void CExtShellDialogFile::OnListViewMode_UpdateList( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	pCmdUI->Enable();
	pCmdUI->SetRadio( ( ( m_wndShellList.GetStyle() & LVS_TYPEMASK ) == LVS_LIST ) ? TRUE : FALSE );
}

void CExtShellDialogFile::OnListViewMode_Details()
{
	ASSERT_VALID( this );
	if( ( ( m_wndShellList.GetStyle() & LVS_TYPEMASK ) == LVS_REPORT ) ? TRUE : FALSE )
		return;
	m_wndShellList.ModifyStyle( LVS_TYPEMASK, LVS_REPORT );
	m_wndShellList.Invalidate();
	m_wndShellList.UpdateWindow();
}

void CExtShellDialogFile::OnListViewMode_UpdateDetails( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	pCmdUI->Enable();
	pCmdUI->SetRadio( ( ( m_wndShellList.GetStyle() & LVS_TYPEMASK ) == LVS_REPORT ) ? TRUE : FALSE );
}

void CExtShellDialogFile::OnButtonRefresh()
{
	ASSERT_VALID( this );
	m_wndShellTree.Refresh();
	m_wndShellList.Refresh();
	if( m_wndShellList.FocusedItemGet() < 0 && ::GetFocus() == m_wndShellList.GetSafeHwnd() )
		m_wndShellList.FocusedItemSet( 0 );
	OnShellListLocationChanged( WPARAM(&m_wndShellList), 0L );
}

void CExtShellDialogFile::_SwitchTwoButtons( CWnd * pButtonDisappear, CWnd * pButtonAppear )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pButtonDisappear );
	ASSERT_VALID( pButtonAppear );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT( pButtonDisappear->GetSafeHwnd() != NULL );
	ASSERT( pButtonAppear->GetSafeHwnd() != NULL );
bool bFocus = ( ::GetFocus() == pButtonDisappear->m_hWnd ) ? true : false;
	pButtonAppear->EnableWindow( TRUE );
	pButtonAppear->ShowWindow( SW_SHOWNOACTIVATE );
	pButtonDisappear->ShowWindow( SW_HIDE );
	pButtonDisappear->EnableWindow( FALSE );
	if( bFocus )
		pButtonAppear->SetFocus();
}

bool CExtShellDialogFile::_CanPorcessWndCollapsing(
	bool bCollapsingDialog, // true - collapsing dialog, false - expanding dialog
	bool bHidingOrShowingWnd, // true - hiding/showing hWnd, false - moving hWnd
	HWND hWnd
	)
{
	ASSERT_VALID( this );
	bCollapsingDialog;
	bHidingOrShowingWnd;
	hWnd;
	return true;
}

bool CExtShellDialogFile::_AnimateCollapsing(
	bool bCollapsingDialog, // true - collapsing dialog, false - expanding dialog
	CRect rcFrom, // initial window rectangle
	CRect rcTo // final window rectangle
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	bCollapsingDialog;
#if ( _MFC_VER != 0x700 )
	if( ! m_bEnableCollapsingAnimation )
		return false;
#endif // ( _MFC_VER != 0x700 )
	m_bHelperCollapsignAnimation = true;
CRect rcOffset(
		rcFrom.left - rcTo.left,
		rcFrom.top - rcTo.top,
		rcTo.right - rcFrom.right,
		rcTo.bottom - rcFrom.bottom
		);
INT nStepCount = 2;
INT nStepTime = 100;
bool bRedrawEachStepDlg = true;
bool bRedrawEachStepBack = true;
INT nStepIndex;
CRect rcWalk;
clock_t nLastAnimTime = clock();
	for( nStepIndex = 0; nStepIndex < nStepCount; nStepIndex ++ )
	{
		if( nStepIndex == nStepCount )
			rcWalk = rcTo;
		else
		{
			CRect rcInflate(
				::MulDiv( rcOffset.left, nStepIndex + 1, nStepCount ),
				::MulDiv( rcOffset.top, nStepIndex + 1, nStepCount ),
				::MulDiv( rcOffset.right, nStepIndex + 1, nStepCount ),
				::MulDiv( rcOffset.bottom, nStepIndex + 1, nStepCount )
				);
			rcWalk = rcFrom;
			rcWalk.InflateRect( rcInflate );
		}
		MoveWindow( rcWalk );
		if( bRedrawEachStepDlg )
			RedrawWindow(
				NULL,
				NULL,
				RDW_INVALIDATE|RDW_UPDATENOW|RDW_ERASE|RDW_ERASENOW
					|RDW_ALLCHILDREN|RDW_FRAME
				);
		if( bRedrawEachStepBack )
			CExtPaintManager::stat_PassPaintMessages();
		if( nStepIndex < (nStepCount-1) )
		{
			clock_t nNewAnimTime = clock();
			clock_t nDuration =
				nLastAnimTime - nNewAnimTime;
			if( nDuration < nStepTime )
				::Sleep( nStepTime - nDuration );
		}
	}
	m_bHelperCollapsignAnimation = false;
	ArrangeLayout();
	return true;
}

void CExtShellDialogFile::OnButtonCollapse()
{
	ASSERT_VALID( this );
	if( m_listCollapsedHWNDs.GetCount() != 0 )
		return;
CRect rcClient;
	GetClientRect( &rcClient );
CRect rcCollapseExpand;
	m_buttonCollapse.GetWindowRect( &rcCollapseExpand );
	ScreenToClient( &rcCollapseExpand );
CRect rcButtonOK;
	m_buttonOK.GetWindowRect( &rcButtonOK );
	ScreenToClient( &rcButtonOK );
CRect rcButtonBack;
	m_buttonBack.GetWindowRect( &rcButtonBack );
	ScreenToClient( &rcButtonBack );
	m_nSavedCollapsedHeight = rcButtonOK.top - rcButtonBack.top;
	ASSERT( m_nSavedCollapsedHeight > 0 );
CRect rcFrom, rcTo;
	GetWindowRect( &rcFrom );
	rcTo = rcFrom;
	m_nCollapsedWindowHeight = rcFrom.Height() - m_nSavedCollapsedHeight;
	rcTo.bottom -= m_nSavedCollapsedHeight;
HWND hWndFocusOld = ::GetFocus();
	if(		hWndFocusOld == m_buttonCollapse.m_hWnd
		||	hWndFocusOld == m_buttonExpand.m_hWnd
		)
		hWndFocusOld = NULL;
	if(		hWndFocusOld != NULL
		&&	(	(! ::IsChild( m_hWnd, hWndFocusOld ) )
			||	( ::__EXT_MFC_GetClassLong( hWndFocusOld, GWL_STYLE ) & WS_VISIBLE ) == 0
			)
		)
		hWndFocusOld = NULL;
	RemoveAllAnchors();
HWND hWnd = ::GetWindow( m_hWnd, GW_CHILD );
HWND hWndFocusNew = NULL;
	for( ; hWnd != NULL; hWnd = ::GetWindow( hWnd, GW_HWNDNEXT ) )
	{
		if(		( ! m_bShowTree )
			&&	hWnd == m_wndShellTree.m_hWnd
			)
			continue;
		CRect rcChild;
		::GetWindowRect( hWnd, &rcChild );
		ScreenToClient( &rcChild );
		if( rcChild.bottom >= rcCollapseExpand.top )
		{
			if(		hWndFocusOld != NULL
				&&	hWndFocusNew == NULL
				&&	( ::__EXT_MFC_GetClassLong( hWndFocusOld, GWL_STYLE ) & (WS_VISIBLE|WS_TABSTOP) ) == (WS_VISIBLE|WS_TABSTOP)
				)
				hWndFocusNew = hWnd;
			m_listMovedHWNDs.AddTail( hWnd );
			continue;
		}
		m_listCollapsedHWNDs.AddTail( hWnd );
	}
INT nCountMoved = INT(m_listMovedHWNDs.GetCount());
INT nCountCollapsed = INT(m_listCollapsedHWNDs.GetCount());
INT nCountDeferred = nCountMoved + nCountCollapsed;
HDWP hDWP = ::BeginDeferWindowPos( nCountDeferred );
POSITION pos = m_listCollapsedHWNDs.GetHeadPosition();
	for( ; pos != NULL; )
	{
		HWND hWnd = m_listCollapsedHWNDs.GetNext( pos );
		ASSERT( hWnd != NULL && ::IsWindow( hWnd ) );
		if(		( ! m_bShowTree )
			&&	hWnd == m_wndShellTree.m_hWnd
			)
			continue;
		if( _CanPorcessWndCollapsing( true, true, hWnd ) )
			hDWP =
				::DeferWindowPos(
					hDWP, hWnd, NULL, 0, 0, 0, 0,
					SWP_NOMOVE|SWP_NOSIZE|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOACTIVATE
						|SWP_NOREDRAW|SWP_NOCOPYBITS|SWP_FRAMECHANGED|SWP_HIDEWINDOW
					);
	}
	pos = m_listMovedHWNDs.GetHeadPosition();
	for( ; pos != NULL; )
	{
		HWND hWnd = m_listMovedHWNDs.GetNext( pos );
		ASSERT( hWnd != NULL && ::IsWindow( hWnd ) );
		if(		( ! m_bShowTree )
			&&	hWnd == m_wndShellTree.m_hWnd
			)
			continue;
		CRect rcChild;
		::GetWindowRect( hWnd, &rcChild );
		ScreenToClient( &rcChild );
		rcChild.OffsetRect( 0, - m_nSavedCollapsedHeight );
		if( _CanPorcessWndCollapsing( true, false, hWnd ) )
			hDWP =
				::DeferWindowPos(
					hDWP, hWnd, NULL, rcChild.left, rcChild.top, rcChild.Width(), rcChild.Height(),
					SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOACTIVATE
						|SWP_NOREDRAW|SWP_NOCOPYBITS
					);
	}
	::EndDeferWindowPos( hDWP );
	if( ! _AnimateCollapsing( true, rcFrom, rcTo ) )
		MoveWindow( rcTo );
	_ReAnchor();
	_SwitchTwoButtons( &m_buttonCollapse, &m_buttonExpand );
	if( hWndFocusNew != NULL )
		::SetFocus( hWndFocusNew );
	else if( m_editFileName.GetSafeHwnd() != NULL && (m_editFileName.GetStyle()&WS_VISIBLE) != 0 )
		m_editFileName.SetFocus();
	else if( m_buttonOK.GetSafeHwnd() != NULL && (m_buttonOK.GetStyle()&WS_VISIBLE) != 0 )
		m_editFileName.SetFocus();
	RedrawWindow( NULL, NULL, RDW_INVALIDATE|RDW_UPDATENOW|RDW_ERASE|RDW_ERASENOW|RDW_ALLCHILDREN|RDW_FRAME );
}

void CExtShellDialogFile::OnButtonExpand()
{
	ASSERT_VALID( this );
	if( m_listCollapsedHWNDs.GetCount() == 0 )
		return;
	RemoveAllAnchors();
INT nCountMoved = INT(m_listMovedHWNDs.GetCount());
INT nCountCollapsed = INT(m_listCollapsedHWNDs.GetCount());
INT nCountDeferred = nCountMoved + nCountCollapsed;
HDWP hDWP = ::BeginDeferWindowPos( nCountDeferred );
POSITION pos = m_listMovedHWNDs.GetHeadPosition();
    for( ; pos != NULL; )
	{
		HWND hWnd = m_listMovedHWNDs.GetNext( pos );
		ASSERT( hWnd != NULL && ::IsWindow( hWnd ) );
		if(		( ! m_bShowTree )
			&&	hWnd == m_wndShellTree.m_hWnd
			)
			continue;
		CRect rcChild;
		::GetWindowRect( hWnd, &rcChild );
		ScreenToClient( &rcChild );
		rcChild.OffsetRect( 0, m_nSavedCollapsedHeight );
		if( _CanPorcessWndCollapsing( false, false, hWnd ) )
			hDWP =
				::DeferWindowPos(
					hDWP, hWnd, NULL, rcChild.left, rcChild.top, rcChild.Width(), rcChild.Height(),
					SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOACTIVATE
						|SWP_NOREDRAW|SWP_NOCOPYBITS
					);
	}
	pos = m_listCollapsedHWNDs.GetHeadPosition();
    for( ; pos != NULL; )
	{
		ASSERT( pos != NULL );
		HWND hWnd = m_listCollapsedHWNDs.GetNext( pos );
		if(		( ! m_bShowTree )
			&&	hWnd == m_wndShellTree.m_hWnd
			)
			continue;
		ASSERT( hWnd != NULL && ::IsWindow( hWnd ) );
		if( _CanPorcessWndCollapsing( false, true, hWnd ) )
			hDWP =
				::DeferWindowPos(
					hDWP, hWnd, NULL, 0, 0, 0, 0,
					SWP_NOMOVE|SWP_NOSIZE|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOACTIVATE
						|SWP_NOREDRAW|SWP_NOCOPYBITS|SWP_FRAMECHANGED|SWP_SHOWWINDOW
					);
	}
	::EndDeferWindowPos( hDWP );
	m_listCollapsedHWNDs.RemoveAll();
	m_listMovedHWNDs.RemoveAll();
CRect rcFrom, rcTo;
	GetWindowRect( &rcFrom );
	rcTo = rcFrom;
	rcTo.bottom += m_nSavedCollapsedHeight;
	m_nSavedCollapsedHeight = 0;
	m_nCollapsedWindowHeight = 0;
	if( ! _AnimateCollapsing( true, rcFrom, rcTo ) )
		MoveWindow( rcTo );
	_ReAnchor();
	_SwitchTwoButtons( &m_buttonExpand, &m_buttonCollapse );
	if( m_editFileName.GetSafeHwnd() != NULL && (m_editFileName.GetStyle()&WS_VISIBLE) != 0 )
		m_editFileName.SetFocus();
	else if( m_buttonOK.GetSafeHwnd() != NULL && (m_buttonOK.GetStyle()&WS_VISIBLE) != 0 )
		m_editFileName.SetFocus();
	RedrawWindow( NULL, NULL, RDW_INVALIDATE|RDW_UPDATENOW|RDW_ERASE|RDW_ERASENOW|RDW_ALLCHILDREN|RDW_FRAME );
}

BOOL CExtShellDialogFile::PreTranslateMessage(MSG* pMsg) 
{
	if(		pMsg->message == WM_KEYDOWN
		&&	pMsg->wParam == VK_RETURN
		&&	m_editFileName.GetSafeHwnd() != NULL
		&&	::GetFocus() == m_editFileName.m_hWnd
		)
	{
		bool bHandled = false;
		TCHAR strFullPath[ _MAX_PATH + 1 ];
		::memset( strFullPath, 0, sizeof(strFullPath) );
		CString strEnteredPath, strCheckPath;
		m_editFileName.GetWindowText( strEnteredPath );
		strEnteredPath.TrimLeft( _T(" \r\n\t") );
		strEnteredPath.TrimRight( _T(" \r\n\t") );
		strCheckPath = strEnteredPath; 
		strCheckPath.TrimLeft( _T(" \r\n\t\"") );
		strCheckPath.TrimRight( _T(" \r\n\t\"") );
		if( strCheckPath == _T(".") )
		{
			bHandled = true;
		}
		else if( strCheckPath == _T("..") )
		{
			bHandled = true;
			HTREEITEM hti = m_wndShellTree.GetFocusedItem();
			if( hti != NULL )
			{
				hti = m_wndShellTree.GetNextItem( hti, TVGN_PARENT );
				if( hti != NULL )
					m_wndShellTree.FocusItem( hti );
			}
		}
		else if( strCheckPath == _T("\\") || strCheckPath == _T("/") )
		{
			bHandled = true;
			HTREEITEM htiNewFocus = NULL, hti = m_wndShellTree.GetFocusedItem();
			for( ; hti != NULL; hti = m_wndShellTree.GetNextItem( hti, TVGN_PARENT ) )
				htiNewFocus = hti;
			if( htiNewFocus != NULL )
				m_wndShellTree.FocusItem( htiNewFocus );
		}
		else
		{
			TCHAR strCurrentDirectory[ _MAX_PATH + 1 ];
			::memset( strCurrentDirectory, 0, _MAX_PATH + 1 );
			::GetCurrentDirectory( _MAX_PATH, strCurrentDirectory );
			CString strBrowsedDirectory = m_wndShellList.GetCurrentFolderPath();
			if( strBrowsedDirectory.IsEmpty() )
				strBrowsedDirectory = strCurrentDirectory;
			CString strCheckCurrentDiskFolderJump = strCheckPath;
			strCheckCurrentDiskFolderJump.MakeLower();
			bool bChangeCurrentDirectory = true;
			if(		strCheckCurrentDiskFolderJump.GetLength() == 2
				&&	strCheckCurrentDiskFolderJump[1] == _T(':')
				&&	( _T('a') <= strCheckCurrentDiskFolderJump[0] && strCheckCurrentDiskFolderJump[0] <= _T('z') )
				)
			{
				bChangeCurrentDirectory = false;
				strBrowsedDirectory = strCurrentDirectory;
			}
			if(		( ! strBrowsedDirectory.IsEmpty() )
				&&	(	(! bChangeCurrentDirectory )
					||	(	bChangeCurrentDirectory
						&&	::SetCurrentDirectory( LPCTSTR(strBrowsedDirectory) )
						)
					)
				)
			{
				if(		// CExtShellBase::stat_DirExist( LPCTSTR(strEnteredPath) ) &&
						::AfxFullPath( strFullPath, LPCTSTR(strEnteredPath) )
					&&	m_wndShellTree.FocusPath( strFullPath )
					)
					bHandled = true;
			}
			VERIFY( ::SetCurrentDirectory( strCurrentDirectory ) );
		}
		if( bHandled )
		{
			m_editFileName.SetWindowText( _T("") );
			m_buttonOK.EnableWindow( FALSE );
			m_arrRetVal.RemoveAll();
			return TRUE;
		}
	}
	return
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
			::PreTranslateMessage(pMsg);
}

void CExtShellDialogFile::ArrangeLayout(
	int cx, // = -1
	int cy  // = -1
	)
{
	if( m_bHelperCollapsignAnimation )
		return;
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
		::ArrangeLayout( cx, cy );
}

#if ( _MFC_VER != 0x700 )
bool CExtShellDialogFile::NcFrameImpl_GetMinMaxInfo(
	LPMINMAXINFO pMMI
	) const
{
	CExtNCW < CExtResizableDialog > :: NcFrameImpl_GetMinMaxInfo( pMMI );
	if( m_bHelperCollapsignAnimation )
		pMMI->ptMinTrackSize.x = pMMI->ptMinTrackSize.y = 0;
	else if( m_listCollapsedHWNDs.GetCount() > 0 )
		pMMI->ptMinTrackSize.y = pMMI->ptMaxTrackSize.y = pMMI->ptMaxSize.y = m_nCollapsedWindowHeight;
	return true;
}
#endif // ( _MFC_VER != 0x700 )


LRESULT CExtShellDialogFile::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	if(		m_bHelperCollapsignAnimation
		&&	(	message == WM_SIZE
			||	message == WM_WINDOWPOSCHANGING
			||	message == WM_WINDOWPOSCHANGED
			)
		)
		return 0L;
LRESULT lResult = 
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
			:: WindowProc( message, wParam, lParam );
	if(		message == WM_NEXTDLGCTL
		&&	m_wndSplitter.GetSafeHwnd() != NULL
		&&	::GetFocus() == m_wndSplitter.m_hWnd
		)
	{
		m_wndShellTree.SetFocus();
	}
	return lResult;
}

BOOL CExtShellDialogFile::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
NMHDR * pNMHDR = (NMHDR *)lParam;
	if( pNMHDR != NULL )
	{
		if( pNMHDR->hwndFrom == m_wndShellList.m_hWnd )
		{
			if( pNMHDR->code == LVN_ITEMCHANGED )
			{
				OnItemChangedShellList( pNMHDR, pResult );
				return TRUE;
			}
			else if( pNMHDR->code == NM_SETFOCUS )
			{
				if( m_wndShellList.FocusedItemGet() < 0 )
				{
					POSITION pos = m_wndShellList.GetFirstSelectedItemPosition();
					if( pos != NULL )
					{
						INT nItem = m_wndShellList.GetNextSelectedItem( pos );
						m_wndShellList.FocusedItemSet( nItem, true, false );
					}
					if( m_wndShellList.FocusedItemGet() < 0 )
					{
						m_wndShellList.FocusedItemSet( 0 );
						_ValidateListViewSelection();
					}
				}
			}
		}
	}
	return 
#if ( _MFC_VER == 0x700 )
		CExtResizableDialog
#else
		CExtNCW < CExtResizableDialog >
#endif
			::OnNotify(wParam, lParam, pResult);
}

CExtShellDialogFile::SplitterWndForTreeAndList::SplitterWndForTreeAndList()
	: m_nSavedShellTreeDlgCtrlID( 0 )
	, m_nSavedShellListDlgCtrlID( 0 )
{
	m_cxBorder = m_cyBorder = 0;
	m_cxBorderShare = m_cyBorderShare = 0;
	m_cxSplitterGap = m_cxSplitter = PmBridge_GetPM()->UiScalingDo( 4, CExtPaintManager::__EUIST_X );
	m_cySplitterGap = m_cySplitter = PmBridge_GetPM()->UiScalingDo( 4, CExtPaintManager::__EUIST_Y );
}

CExtShellDialogFile::SplitterWndForTreeAndList::~SplitterWndForTreeAndList()
{
}

bool CExtShellDialogFile::SplitterWndForTreeAndList::Create(
	CExtShellTreeCtrl & wndShellTree,
	CExtShellListCtrl & wndShellList,
	CWnd & wndParent,
	bool bShowTree,
	DWORD dwStyle, // = WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP
	UINT nID // = UINT(__EXT_MFC_IDC_STATIC)
	)
{
	ASSERT_VALID( this );
	if(		GetSafeHwnd() != NULL
		||	wndParent.GetSafeHwnd() == NULL
		||	wndShellTree.GetSafeHwnd() == NULL
		||	wndShellList.GetSafeHwnd() == NULL
		)
	{
		ASSERT( FALSE );
		return false;
	}
	if( ! CExtSplitterWnd::CreateStatic(
			&wndParent,
			1,
			2,
			dwStyle,
			nID
			)
		)
	{
		ASSERT( FALSE );
		return false;
	}
	SetWindowPos(
		&wndShellList, 0, 0, 0, 0,
		SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOACTIVATE|SWP_NOREDRAW|SWP_NOCOPYBITS //|SWP_NOSENDCHANGING
		);
CRect rcTree, rcList, rcExplorer;
	wndShellTree.GetWindowRect( &rcTree );
	wndShellList.GetWindowRect( &rcList );
	rcExplorer.UnionRect( &rcTree, &rcList );
	wndParent.ScreenToClient( &rcExplorer );
	m_nSavedShellTreeDlgCtrlID = (UINT)wndShellTree.GetDlgCtrlID();
	m_nSavedShellListDlgCtrlID = (UINT)wndShellList.GetDlgCtrlID();
	wndShellTree.SetParent( this );
	wndShellList.SetParent( this );
	wndShellList.SetWindowPos(
		&wndShellTree, 0, 0, 0, 0,
		SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOACTIVATE|SWP_NOREDRAW|SWP_NOCOPYBITS //|SWP_NOSENDCHANGING
		);
	wndShellTree.SetDlgCtrlID( IdFromRowCol( 0, 0 ) );
	wndShellList.SetDlgCtrlID( IdFromRowCol( 0, 1 ) );
	SetColumnInfo( 0, bShowTree ? rcTree.Width() : 0, 0 );
	SetColumnInfo( 1, bShowTree ? rcList.Width() : (rcTree.Width()+rcList.Width()), 0 );
	MoveWindow( &rcExplorer );
	RecalcLayout();
	return true;
}

LRESULT CExtShellDialogFile::SplitterWndForTreeAndList::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	if(		message == WM_COMMAND
		||	message == WM_NOTIFY
		||	message == CExtShellBase::g_nMsgDelayFocusPIDL
		||	message == CExtShellBase::g_nMsgUpdateItemContent
		||	message == CExtShellBase::g_nMsgChangeChildrenState
		||	message == CExtShellBase::g_nMsgShellItemExecute
		||	message == CExtTreeCtrl::g_nMsgTreeItemDelayedFocus
		||	message == CExtShellComboBox::g_nMsgShellLocationChanged
		||	message == CExtShellListCtrl::g_nMsgShellLocationChanged
		)
	{
		if(		message == WM_COMMAND
			||	message == WM_NOTIFY
			)
		{
			UINT nControlID = UINT(LOWORD(wParam));
			if( nControlID == UINT(IdFromRowCol(0,0)) )
				nControlID = m_nSavedShellTreeDlgCtrlID;
			else if( nControlID == UINT(IdFromRowCol(0,1)) )
				nControlID = m_nSavedShellListDlgCtrlID;
			wParam = MAKEWPARAM( nControlID, HIWORD(wParam) );
			if( message == WM_NOTIFY )
			{
				NMHDR * pNMHDR = (NMHDR *)lParam;
				if( pNMHDR != NULL )
					pNMHDR->idFrom = nControlID;
			}
		}
		return ::SendMessage( ::GetParent( m_hWnd ), message, wParam, lParam );
	}
LRESULT lResult = CExtSplitterWnd::WindowProc( message, wParam, lParam );
	return lResult;
}

#endif //  ( ! defined __EXT_MFC_NO_SHELL_DIALOG_FILE )










