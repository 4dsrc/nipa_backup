// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// Portions are a Derived Copyright (c) 2016 Eugene Ustinenkov for The Software Establishment, LLC.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "StdAfx.h"

// functions required
//	RGB_I
// vector rotation
// bool CExtBitmap::LoadBMP_Resource_I
// graphic color rotations
// graphic control, round, flat, border, multiple
// mapping control graphics colors, map to colors

// disable warning 4706, 4996
#pragma warning( push )
#pragma warning ( disable : 4706 )
#pragma warning ( disable : 4996 )
#include <multimon.h>
// rollback warning 4706, 4996
#pragma warning( pop )

#if _MFC_VER < 0x700
#include <../src/AfxImpl.h>
#else
#include <../src/mfc/AfxImpl.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
#include <ExtPaintManager.h>
#endif

#if (!defined __EXT_RICH_CONTENT_H)
#include <ExtRichContent.h>
#endif // (!defined __EXT_RICH_CONTENT_H)

#if (!defined __ExtCmdManager_H)
#include <ExtCmdManager.h>
#endif

#if (!defined __EXT_MEMORY_DC_H)
#include <../Src/ExtMemoryDC.h>
#endif

#if (!defined __EXT_POPUP_MENU_WND_H)
#include <ExtPopupMenuWnd.h>
#endif

#if (!defined __EXT_MENUCONTROLBAR_H)
#include <ExtMenuControlBar.h>
#endif

#if (!defined __EXTDOCKBAR_H)
#include <../Src/ExtDockBar.h>
#endif

#if (!defined __EXT_STATUSCONTROLBAR_H)
#include <ExtStatusControlBar.h>
#endif

#if (!defined __EXT_MFC_NO_TAB_CONTROLBARS)
#if (!defined __EXT_CONTROLBAR_TABBED_FEATURES_H)
#include "ExtControlBarTabbedFeatures.h"
#endif // __EXT_CONTROLBAR_TABBED_FEATURES_H
#endif

#if( !defined __EXTMINIDOCKFRAMEWND_H)
#include "ExtMiniDockFrameWnd.h"
#endif

#if (!defined __EXT_MFC_NO_SHORTCUTLIST_CTRL)
#if (!defined __EXTSHORTCUTLISTWND_H)
#include <ExtShortcutListWnd.h>
#endif
#endif // (!defined __EXT_MFC_NO_SHORTCUTLIST_CTRL)

#if (!defined __EXT_BUTTON_H)
#include <ExtButton.h>
#endif

#if (!defined __EXT_GROUPBOX_H)
#include <ExtGroupBox.h>
#endif

#if (!defined __PROF_UIS_RES_2007_H)
#include <Resources/Res2007/Res2007.h>
#endif

#if (!defined __PROF_UIS_RES_2010_OFFICE_H)
#include <Resources/Res2010office/Res2010office.h>
#endif

#if (!defined __EXT_SCROLLWND_H)
#include <ExtScrollWnd.h>
#endif 

#if (!defined __EXT_MFC_NO_REPORTGRIDWND)
#if (!defined __EXT_REPORT_GRID_WND_H)
#include <ExtReportGridWnd.h>
#endif
#endif 

#if (!defined __EXT_MFC_NO_DATE_PICKER)
#if (!defined __EXT_DATE_PICKER_H)
#include <ExtDatePicker.h>
#endif
#endif 

#if (!defined __EXT_MFC_NO_TOOLBOX_CTRL)
#if (!defined __EXTTOOLBOXWND_H)
#include <ExtToolBoxWnd.h>
#endif
#endif 

#if (!defined __EXT_MFC_NO_RIBBON_BAR)
#if (!defined __EXT_RIBBON_BAR_H)
#include <ExtRibbonBar.h>
#endif // (!defined __EXT_RIBBON_BAR_H)
#endif

#if (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)
#if (!defined __EXT_POPUP_CTRL_MENU_H)
#include <ExtPopupCtrlMenu.h>
#endif
#endif

#if (!defined __EXT_MFC_NO_GRIDWND)
#if (!defined __EXT_GRIDWND_H)
#include <ExtGridWnd.h>
#endif 
#endif

#if (!defined __EXT_TEMPL_H)
#include <ExtTempl.h>
#endif

#if (!defined __EXT_SPIN_H)
#include <ExtSpinWnd.h>
#endif

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
#if (!defined __EXTCUSTOMIZE_H)
#include <ExtCustomize.h>
#endif
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

#if (!defined __EXT_MFC_NO_DURATIONWND)
#if (!defined __EXT_DURATIONWND_H)
#include "ExtDurationWnd.h"
#endif
#endif

#if (!defined __EXT_MFC_NO_PAGECONTAINER_CTRL)
#if (!defined __EXT_PAGECONTAINERWND_H)
#include <ExtPageContainerWnd.h>
#endif
#endif

#if (!defined __EXT_RESIZABLE_DIALOG_H)
#include <ExtResizableDialog.h>
#endif

#if (!defined __EXT_EDIT_H)
#include <ExtEdit.h>
#endif

#if (!defined __EXT_COMBO_BOX_H)
#include <ExtComboBox.h>
#endif

#if (!defined __EXT_VERSION_EX_H)
#include "ExtVersionEx.h"
#endif

#if (! defined __VSSYM32_H__)
#include <vssym32/vssym32.h>
#endif // (! defined __VSSYM32_H__)

#include <math.h>
#include <shlwapi.h>

#include <Resources/Resource.h>
#include <vector>

#if (! defined CLEARTYPE_QUALITY )
#define CLEARTYPE_QUALITY 5
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE [] = __FILE__;
#endif

// VC+ 6.0 static builds specific: disable warnings 4305, 4309
// warning C4305: 'initializing' : truncation from 'int' to 'short'
// warning C4309: 'initializing' : truncation of constant value
#if _MFC_VER < 0x700
#ifdef __EXT_PROFUIS_STATIC_LINK
#pragma warning( push )
#pragma warning ( disable : 4305 )
#pragma warning ( disable : 4309 )
#endif
#endif

//////////////////////////////////////////////////////////////////////////
int _Index(const std::vector<int> v, int nIndex);
int _IndexDark(int nIndex);
int _IndexTab(int nIndex);

//CExtPaintManager::CExtPaintManagerAutoPtr g_PaintManager;
//////////////////////////////////////////////////////////////////////////
// CExtPaintManagerOffice 2010_Imp
//////////////////////////////////////////////////////////////////////////

CExtPaintManagerOffice2010_Impl::CExtPaintManagerOffice2010_Impl()
{
#if (!defined __EXT_MFC_NO_PAGE_NAVIGATOR )
	m_bPnPaneCaptFontBold = false;
	m_strPnPaneCaptCustomFontFaceName = _T("");
#endif // (!defined __EXT_MFC_NO_PAGE_NAVIGATOR )

}

CExtPaintManagerOffice2010_Impl::~CExtPaintManagerOffice2010_Impl()
{

}

// the office R1 Constructor
CExtPaintManagerOffice2010_R1::CExtPaintManagerOffice2010_R1()
{
	m_nThemeIndex = IndexMapper4(Office2010_R1, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2010_R1::~CExtPaintManagerOffice2010_R1()
{
}

void CExtPaintManagerOffice2010_R1::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2010_R1::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

// Constructor R2_Blue
CExtPaintManagerOffice2010_R2_Blue::CExtPaintManagerOffice2010_R2_Blue()
{
	m_nThemeIndex = IndexMapper4(Office2010_R2_Blue, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2010_R2_Blue::~CExtPaintManagerOffice2010_R2_Blue()
{
}

void CExtPaintManagerOffice2010_R2_Blue::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2010_R2_Blue::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

// constructor R2_Silver
CExtPaintManagerOffice2010_R2_Silver::CExtPaintManagerOffice2010_R2_Silver()
{
	m_nThemeIndex = IndexMapper4(Office2010_R2_Silver, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2010_R2_Silver::~CExtPaintManagerOffice2010_R2_Silver()
{
}

void CExtPaintManagerOffice2010_R2_Silver::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2010_R2_Silver::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

// Constructor Blakc
CExtPaintManagerOffice2010_R2_Black::CExtPaintManagerOffice2010_R2_Black()
{
	m_nThemeIndex = IndexMapper4(Office2010_R2_Black, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2010_R2_Black::~CExtPaintManagerOffice2010_R2_Black()
{
}

void CExtPaintManagerOffice2010_R2_Black::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2010_R2_Black::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

#if (!defined __EXT_MFC_NO_RIBBON_BAR)

INT CExtPaintManagerOffice2010_Impl::Ribbon_GetOuterGroupDistance(
	const CExtBarButton * pTBB,
	bool bDistanceBefore,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID(this);
	ASSERT_VALID(pTBB);
	if (IsHighContrast())
		return CExtPaintManagerXP::Ribbon_GetOuterGroupDistance(pTBB, bDistanceBefore, lParam);
	return 0;
}

#endif // (!defined __EXT_MFC_NO_RIBBON_BAR)

void CExtPaintManagerOffice2010_Impl::PaintPushButtonMdiRight(
	CDC & dc,
	CExtPaintManager::PAINTPUSHBUTTONDATA & _ppbd
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
#if (!defined __EXT_MFC_NO_RIBBON_BAR)
	if (_ppbd.m_pHelperSrc != NULL)
	{
		CExtBarMdiRightButton * pTBB = DYNAMIC_DOWNCAST(CExtBarMdiRightButton, _ppbd.m_pHelperSrc);
		if (pTBB != NULL)
		{
			CExtToolControlBar * pBar = pTBB->GetSafeBar();
			if (pBar != NULL)
			{
				CExtRibbonBar * pRibbonBar = DYNAMIC_DOWNCAST(CExtRibbonBar, pBar);
				if (pRibbonBar != NULL
					&&	pRibbonBar->m_pExtNcFrameImpl != NULL
					//					&&	pRibbonBar->m_pExtNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement()
					)
				{
					_ppbd.m_pIcon = NULL;
					CExtCmdIcon * pIcon = NULL;
					switch (_ppbd.m_nHelperSysCmdID)
					{
					case SC_MINIMIZE:
						pIcon = &m_arrMdiRightIcons[0];
						break;
					case SC_MAXIMIZE:
					case SC_RESTORE:
						pIcon = &m_arrMdiRightIcons[1];
						break;
					case SC_CLOSE:
						pIcon = &m_arrMdiRightIcons[2];
						break;
					}
					if (pIcon != NULL)
					{
						static const CRect g_rcInf(3, 3, 1, 1);
						_ppbd.m_rcClient.InflateRect(&g_rcInf);
						Ribbon_PaintPushButton(dc, _ppbd);
						_ppbd.m_rcClient.DeflateRect(&g_rcInf);

						CRect rectCaption = _ppbd.m_rcClient;
						rectCaption.DeflateRect(_ppbd.m_rcBorderSizes);
						CRect rectClient(rectCaption);
						CRect rcFocus(rectCaption);
						if (rectClient.bottom > rectClient.top
							&&	rectClient.right > rectClient.left
							)
						{
							_ppbd.m_pIcon = pIcon;
							PaintPushButtonIcon(dc, rectCaption, rectCaption, rcFocus, _ppbd);
							_ppbd.m_pIcon = NULL;
						}
						return;
					}
				}
			}
		}
	}
#endif // (!defined __EXT_MFC_NO_RIBBON_BAR)
	CExtPaintManagerOffice2007_Impl::PaintPushButtonMdiRight(dc, _ppbd);
}


// include reverse list
void CExtPaintManagerOffice2010_Impl::PaintPushButton(
	CDC & dc,
	CExtPaintManager::PAINTPUSHBUTTONDATA & _ppbd
	)
{
	ASSERT_VALID(this);

#if _MFC_VER < 0xC00 

	static const int pIndex[] = { 3, 19, 23, 30, 32 };
	static const std::vector<int> vIndex(pIndex, pIndex + _countof(pIndex));

#else // _MFC_VER < 0xC00  

	static const std::vector<int> vIndex = { 3, 19, 23, 30, 32 };

#endif // _MFC_VER < 0xC00  

	if (_Index(vIndex, m_nThemeIndex))
	{
		ASSERT(dc.GetSafeHdc() != NULL);
		if (IsHighContrast())
		{
			CExtPaintManagerXP::PaintPushButton(dc, _ppbd);
			return;
		}
		if (_ppbd.m_pHelperSrc != NULL
			&&	_ppbd.m_pHelperSrc->IsKindOf(RUNTIME_CLASS(CButton))
			)
		{
			CExtPaintManagerOffice2007_Impl::PaintPushButton(dc, _ppbd);
			return;
		}
		bool bInvertNormalTextColor = true;
		if (_ppbd.m_bHover || _ppbd.m_bPushed || _ppbd.m_bPushedDropDown)
			bInvertNormalTextColor = false;
		else if (_ppbd.m_clrForceTextNormal == COLORREF(-1L) && _ppbd.m_pHelperSrc != NULL)
		{
			if (_ppbd.m_pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtBarButton))
				&& (!_ppbd.m_pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtBarMdiRightButton)))
				)
			{
				if (LPVOID(CExtPopupMenuWnd::g_pCombinedHelper) == LPVOID(_ppbd.m_pHelperSrc))
					bInvertNormalTextColor = false;
				else if (_ppbd.m_pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtBarButton)))
				{
					CExtToolControlBar * pToolBar = ((CExtBarButton*) _ppbd.m_pHelperSrc)->GetBar();
					ASSERT_VALID(pToolBar);
					if (!pToolBar->IsKindOf(RUNTIME_CLASS(CExtMenuControlBar)))
					{
						if (pToolBar->m_pDockSite == NULL)
						{
							if (pToolBar->m_bForceBalloonGradientInDialogs)
								bInvertNormalTextColor = false;
						}
						else
						{
							if (!pToolBar->m_bForceNoBalloonWhenRedockable)
								bInvertNormalTextColor = false;
						}
					}
#if (!defined __EXT_MFC_NO_RIBBON_BAR)
					else if (pToolBar->IsKindOf(RUNTIME_CLASS(CExtRibbonPage)))
					{
						bInvertNormalTextColor = false;
						if (LPVOID(LPCTSTR(_ppbd.m_sText)) != NULL && (*LPCTSTR(_ppbd.m_sText)) != _T('\0'))
						{
							CExtCustomizeCmdTreeNode * pNode = ((CExtBarButton*) _ppbd.m_pHelperSrc)->GetCmdNode();
							if (pNode != NULL)
							{
								pNode = pNode->GetParentNode();
								if (pNode != NULL && pNode->IsKindOf(RUNTIME_CLASS(CExtRibbonNodeRightButtonsCollection)))
								{
									bInvertNormalTextColor = true;
								}
							}
						}
					}
#endif // (!defined __EXT_MFC_NO_RIBBON_BAR)
				}
			}
			else if (_ppbd.m_pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtPopupBaseWnd))
				|| _ppbd.m_pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtMenuControlBar))
#if (!defined __EXT_MFC_NO_DATE_PICKER)
				|| _ppbd.m_pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtDatePickerWnd))
#endif // (!defined __EXT_MFC_NO_DATE_PICKER)
				)
				bInvertNormalTextColor = false;
		}
		if (bInvertNormalTextColor
			&&	_ppbd.m_clrForceTextNormal == COLORREF(-1L)
			)
			_ppbd.m_clrForceTextNormal = RGB(220, 220, 220);
		if (_ppbd.m_clrForceTextDisabled == COLORREF(-1L))
			_ppbd.m_clrForceTextDisabled = RGB(141, 141, 141);
	}
	CExtPaintManagerOffice2007_Impl::PaintPushButton(dc, _ppbd);
}

// include reverse list
void CExtPaintManagerOffice2010_Impl::PaintTabButton(
	CDC & dc,
	CRect & rcButton,
	LONG nHitTest,
	bool bTopLeft,
	bool bHorz,
	bool bEnabled,
	bool bHover,
	bool bPushed,
	bool bGroupedMode,
	CObject * pHelperSrc,
	LPARAM lParam, // = 0L
	bool bFlat // = false
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);

#if _MFC_VER < 0xC00 

	static const int pIndex[] = { 3, 19, 23, 30, 32 };
	static const std::vector<int> vIndex(pIndex, pIndex + _countof(pIndex));

#else // _MFC_VER < 0xC00  

	static const std::vector<int> vIndex = { 3, 19, 23, 30, 32 };

#endif // _MFC_VER < 0xC00  

	if (_Index(vIndex, m_nThemeIndex))
	{
		if (IsHighContrast())
 		{
			CExtPaintManagerOffice2003::PaintTabButton(dc, rcButton, nHitTest, bTopLeft, bHorz, bEnabled, bHover, bPushed, bGroupedMode, pHelperSrc, lParam, bFlat);
			return;
 		}
		COLORREF clrGlyph = RGB(141, 141, 141);
		if (bEnabled)
		{
			if (pHelperSrc != NULL
				&&	pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtPopupMenuWnd))
				)
				clrGlyph = RGB(0, 0, 0);
			else
				clrGlyph = RGB(220, 220, 220);
		}
		COLORREF clrTL = GetColor(bPushed ? COLOR_3DDKSHADOW : COLOR_3DHILIGHT, pHelperSrc, lParam);
		COLORREF clrBR = GetColor(bPushed ? COLOR_3DHILIGHT : COLOR_3DDKSHADOW, pHelperSrc, lParam);
		CExtPaintManager::stat_PaintTabButtonImpl(dc, rcButton, nHitTest, bTopLeft, bHorz, bEnabled, bHover, bPushed, bGroupedMode, clrGlyph, clrTL, clrBR, bFlat);
	}
	else{
		CExtPaintManagerOffice2007_Impl::PaintTabButton(dc, rcButton, nHitTest, bTopLeft, bHorz, bEnabled, bHover, bPushed, bGroupedMode, pHelperSrc);
	}
}


#if (!defined __EXT_MFC_NO_PAGE_NAVIGATOR )
void CExtPaintManagerOffice2010_Impl::PaintPageNavigatorSplitterDots(
	CDC & dc,
	const CRect & rcSplitter,
	INT nOffsetV // = 0
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	if (IsHighContrast())
	{
		CExtPaintManagerOffice2007_Impl::PaintPageNavigatorSplitterDots(dc, rcSplitter, nOffsetV);
		return;
	}
	// do not paint dots in 2010 themes
}
#endif // (!defined __EXT_MFC_NO_PAGE_NAVIGATOR )

bool CExtPaintManagerOffice2010_Impl::PaintDocumentClientAreaBkgnd(
	CDC & dc,
	CWnd * pWnd,
	LPARAM lParam // = NULL
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	ASSERT(pWnd->GetSafeHwnd() != NULL);
	if (IsHighContrast())
		return CExtPaintManagerXP::PaintDocumentClientAreaBkgnd(dc, pWnd, lParam);
	if (!m_bmpDCA.IsEmpty())
	{
		CRect rcClient;
		pWnd->GetClientRect(&rcClient);
		if (dc.RectVisible(&rcClient))
		{
			int nOldStretchBltMode = ::GetStretchBltMode(dc.m_hDC);
			::SetStretchBltMode(dc.m_hDC, (g_PaintManager.m_bIsWinNT) ? HALFTONE : COLORONCOLOR);
			m_bmpDCA.Draw(dc.m_hDC, rcClient);
			::SetStretchBltMode(dc.m_hDC, nOldStretchBltMode);
		} // if( dc.RectVisible( &rcClient ) )
		return true;
	} // if( ! m_bmpDCA.IsEmpty() )
	return CExtPaintManagerOffice2003::PaintDocumentClientAreaBkgnd(dc, pWnd, lParam);
}
// this has a reverse list
COLORREF CExtPaintManagerOffice2010_Impl::QueryObjectTextColor(
	CDC & dc,
	bool bEnabled,
	bool bFocused,
	bool bHovered,
	bool bPressed,
	CObject * pHelperSrc,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID(this);

#if _MFC_VER < 0xC00 

	static const int pIndex[] = { 3, 19, 23, 30, 32 };
	static const std::vector<int> vIndex(pIndex, pIndex + _countof(pIndex));

#else // _MFC_VER < 0xC00  

	static const std::vector<int> vIndex = { 3, 19, 23, 30, 32 };

#endif // _MFC_VER < 0xC00  

	if (_Index(vIndex, m_nThemeIndex))
	{
		if (IsHighContrast())
			return CExtPaintManagerXP::QueryObjectTextColor(dc, bEnabled, bFocused, bHovered, bPressed, pHelperSrc, lParam);
		if (pHelperSrc != NULL)
		{
			if (pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtButton)))
			{
				if (bEnabled)
				{
					if (bPressed)
					{
						COLORREF clr = ((CExtButton*) pHelperSrc)->GetTextColorPressed();
						if (clr != COLORREF(-1L))
							return clr;
					}
					else if (bHovered)
					{
						COLORREF clr = ((CExtButton*) pHelperSrc)->GetTextColorHover();
						if (clr != COLORREF(-1L))
							return clr;
					}
					COLORREF clr = ((CExtButton*) pHelperSrc)->GetTextColorNormal();
					if (clr != COLORREF(-1L))
						return clr;
				} // if( bEnabled )
				else
				{
					COLORREF clr = ((CExtButton*) pHelperSrc)->GetTextColorDisabled();
					if (clr != COLORREF(-1L))
						return clr;
				} // else from if( bEnabled )
			} // if( pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtButton)) )
			else if (pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtGroupBox)))
			{
				COLORREF clr = ((CExtGroupBox*) pHelperSrc)->GetTextColor(bEnabled);
				if (clr != COLORREF(-1L))
					return clr;
			}
#if (!defined __EXT_MFC_NO_TAB_CTRL)
			else if (pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtTabWnd)))
			{
				if (bEnabled)
					return COLORREF(-1L);
			}
#endif // (!defined __EXT_MFC_NO_TAB_CTRL)
			if (bEnabled)
				return RGB(220, 220, 220);
			else
				return RGB(141, 141, 141);
		}
	}
	return CExtPaintManagerOffice2007_Impl::QueryObjectTextColor(dc, bEnabled, bFocused, bHovered, bPressed, pHelperSrc, lParam);
}

#if (!defined __EXT_MFC_NO_PAGE_NAVIGATOR )
void CExtPaintManagerOffice2010_Impl::PaintPageNavigatorSplitter(
	CDC & dc,
	const CRect & rcSplitter,
	bool bDrawDots // = true
	)
{
	ASSERT_VALID(this);
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	if (IsHighContrast())
	{
		CExtPaintManagerXP::PaintPageNavigatorSplitter(dc, rcSplitter, bDrawDots);
		return;
	}
	if (::GetDeviceCaps(dc.m_hDC, BITSPIXEL) > 8)
	{
		stat_PaintGradientRect(dc, &rcSplitter, m_arrClrPnSplitter[1], m_arrClrPnSplitter[0], true);
		dc.FillSolidRect(rcSplitter.left, rcSplitter.top, rcSplitter.Width(), 1, GetColor(_2003CLR_PN_BORDER, this));
		dc.FillSolidRect(rcSplitter.left, rcSplitter.bottom - 1, rcSplitter.Width(), 1, GetColor(_2003CLR_PN_BORDER, this));
		if (bDrawDots)
			PaintPageNavigatorSplitterDots(dc, rcSplitter, 0);
	}
	else
		CExtPaintManagerOffice2003::PaintPageNavigatorSplitter(dc, rcSplitter, true);
}
#endif // (!defined __EXT_MFC_NO_PAGE_NAVIGATOR )

#if (!defined __EXT_MFC_NO_RIBBON_BAR)
bool CExtPaintManagerOffice2010_Impl::Ribbon_DwmAreaCoversTabs() const
{
	ASSERT_VALID(this); return true;
}

INT CExtPaintManagerOffice2010_Impl::Ribbon_GetGroupHeight(
	const CExtRibbonButtonGroup * pGroupTBB,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID(this);
	if (IsHighContrast())
		return CExtPaintManagerXP::Ribbon_GetGroupHeight(pGroupTBB, lParam);
	CFont * pMeasureFont = (CFont*) &m_FontRibbonNormal;
	ASSERT(pMeasureFont->GetSafeHandle() != NULL);
	CWindowDC dc(NULL);
	CFont * pOldFont = dc.SelectObject(pMeasureFont);
	TEXTMETRIC _TxtM;
	::memset(&_TxtM, 0, sizeof(TEXTMETRIC));
	if (!dc.GetTextMetrics(&_TxtM))
		_TxtM.tmHeight = 17;
	dc.SelectObject(pOldFont);
	// 				//INT nBasicHeight = 92;
	// 				INT nBasicHeight = _TxtM.tmHeight*3 + 48;
	// 				INT nRetVal = UiScalingDo( nBasicHeight, __EUIST_Y );
	INT nRetVal = _TxtM.tmHeight * 3;
	if (g_PaintManager.m_nLPY > 120)
		nRetVal += UiScalingDo(48, __EUIST_Y);
	else
		nRetVal += UiScalingDo(40, __EUIST_Y) + 8;
	if (pGroupTBB != NULL)
	{
		ASSERT_VALID(pGroupTBB);
		const CExtRibbonPage * pRibbonPage = pGroupTBB->GetRibbonPage();
		if (pRibbonPage != NULL)
		{
			ASSERT_VALID(pRibbonPage);
			if (pRibbonPage->m_bHelperPopupMode
				&& (!pRibbonPage->m_bHelperAutoHideMode)
				&& (!Ribbon_IsPopupGroupWithCaption((CObject*) pRibbonPage))
				)
			{
				const INT nCaptionHeight = Ribbon_GroupCaptionGetHeight(NULL);
				nRetVal -= nCaptionHeight;
			}
		} // if( pRibbonPage != NULL )
	} // if( pGroupTBB != NULL )
	return nRetVal;
}

CRect CExtPaintManagerOffice2010_Impl::Ribbon_GetContentPadding(
	const CExtBarButton * pTBB,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID(this);
	ASSERT_VALID(pTBB);
	lParam;
	if (pTBB->IsSeparator())
		return CRect(1, 1, 1, 1);
	const CExtRibbonButtonGroup * pGroupTBB = DYNAMIC_DOWNCAST(CExtRibbonButtonGroup, pTBB);
	if (pGroupTBB != NULL)
	{
		if (pGroupTBB->ParentButtonGet() == NULL)
			return CRect(2, 2, 2, 0);
		if (pGroupTBB->IsKindOf(RUNTIME_CLASS(CExtRibbonButtonToolGroup)))
			//			return CRect( 1, 0, 1, 0 );
			return CRect(1, 0, 1, 1);  // alw 7/15/2013
		return CRect(0, 0, 0, 0);
	}
	if (pTBB->IsKindOf(RUNTIME_CLASS(CExtRibbonButton))
		|| pTBB->IsKindOf(RUNTIME_CLASS(CExtBarColorButton))
		)
	{
		const CExtBarButton * pParentTBB = pTBB->ParentButtonGet();
		if (pParentTBB != NULL)
		{
			if (pParentTBB->IsKindOf(RUNTIME_CLASS(CExtRibbonButtonToolGroup)))
				return CRect(0, 0, 0, 0);
			if (pParentTBB->ParentButtonGet() == NULL)
				return CRect(2, 0, 2, 0);
		}
		return CRect(3, 0, 3, 0);
	}
	return CRect(0, 0, 0, 0);
}

CSize CExtPaintManagerOffice2010_Impl::Ribbon_CalcButtonSize(
	CDC & dc,
	INT nILV,
	const CExtBarButton * pTBB,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID(this);
	ASSERT_VALID(pTBB);
	if (pTBB->IsSeparator())
		return g_ribbon_settings_t.GetSeparatorSize();// CSize(2, 2);
	const CExtRibbonNode * pRibbonNode = pTBB->Ribbon_GetNode();
	if (pRibbonNode == NULL)
		return CSize(0, 0);
	ASSERT_VALID(pRibbonNode);
	if (nILV < 0)
		nILV = pTBB->RibbonILV_Get(0);
	CSize _sizeIcon = Ribbon_GetIconSize(pTBB, nILV, lParam);
	const CExtCmdIcon * pCmdIcon = pTBB->GetIconPtr();
	bool bHaveIcon = (pCmdIcon != NULL && (!pCmdIcon->IsEmpty())) ? true : false;
	CExtSafeString strText;
	if (!(nILV == __EXT_RIBBON_ILV_SIMPLE_SMALL && bHaveIcon))
		strText = pTBB->GetText();
	if (nILV != __EXT_RIBBON_ILV_SIMPLE_LARGE && pCmdIcon == 0)
		_sizeIcon.cx = _sizeIcon.cy = 0;
	CSize _sizeText(0, 0);
	UINT nDT = 0;
	if (nILV != __EXT_RIBBON_ILV_SIMPLE_LARGE)
	{
		strText.Replace(_T("\r"), _T(" "));
		strText.Replace(_T("\n"), _T(" "));
		strText.Replace(_T("\t"), _T(" "));
		strText.Replace(_T("  "), _T(" "));
		strText.TrimLeft(_T(" "));
		strText.TrimRight(_T(" "));
		nDT = DT_SINGLELINE | DT_LEFT | DT_TOP;
	} // if( nILV != __EXT_RIBBON_ILV_SIMPLE_LARGE )
	else
		nDT = DT_LEFT | DT_TOP;
	if (!strText.IsEmpty())
	{
		CFont * pFont = ((CExtToolControlBar*) (pTBB->GetBar()))->OnGetToolbarFont(false, true, const_cast <CExtBarButton *> (pTBB));
		_sizeText = stat_CalcTextDimension(dc, *pFont, strText, nDT | DT_CALCRECT).Size();
		// 		_sizeText.cx = UiScalingDo( _sizeText.cx, __EUIST_X );
		// 		_sizeText.cy = UiScalingDo( _sizeText.cy, __EUIST_Y );
		if (_sizeText.cx > 0)
			_sizeText.cx += 4;
	} // if( ! strText.IsEmpty() )
	CSize _sizeDDA(0, 0);
	if (pTBB->IsAbleToTrackMenu())
		_sizeDDA = Ribbon_GetDropDownArrowSize(dc, nILV, pTBB, lParam);
	CRect rcCP = pTBB->OnRibbonGetContentPadding();
	rcCP.left = UiScalingDo(rcCP.left, __EUIST_X);
	rcCP.top = UiScalingDo(rcCP.top, __EUIST_Y);
	rcCP.right = UiScalingDo(rcCP.right, __EUIST_X);
	rcCP.bottom = UiScalingDo(rcCP.bottom, __EUIST_Y);
	INT nTextToIconDistance = Ribbon_GetTextToIconDistance(dc, nILV, pTBB, lParam);
	CSize _size = _sizeIcon;
	CFont * pMeasureFont = (CFont*) &m_FontRibbonNormal;
	ASSERT(pMeasureFont->GetSafeHandle() != NULL);
	CFont * pOldFont = dc.SelectObject(pMeasureFont);
	TEXTMETRIC _TxtM;
	::memset(&_TxtM, 0, sizeof(TEXTMETRIC));
	if (!dc.GetTextMetrics(&_TxtM))
		_TxtM.tmHeight = 17;
	dc.SelectObject(pOldFont);
	INT nBasicHeight = _TxtM.tmHeight;

	if (nILV == __EXT_RIBBON_ILV_SIMPLE_LARGE)
	{
		_sizeText.cx += _sizeDDA.cx;
		if (!strText.IsEmpty())
		{
			_sizeText.cy = max(_sizeText.cy, _sizeDDA.cy);
			_sizeText.cy += nTextToIconDistance;
		}
		_size.cy += _sizeText.cy;
		_size.cx = max(_sizeText.cx, _size.cx);
		_size.cx += rcCP.left + rcCP.right;
		_size.cy += rcCP.top + rcCP.bottom;
		//		 CSize _sizeAdjust( 42, 66 );
		CSize _sizeAdjust(
			UiScalingDo(42, __EUIST_X),
			nBasicHeight * 2 + UiScalingDo(40, __EUIST_Y)
			);
		_size.cx = max(_size.cx, _sizeAdjust.cx);
		_size.cy = max(_size.cy, _sizeAdjust.cy);
	}
	else
	{
		if (!strText.IsEmpty())
		{
			_sizeText.cy = max(_sizeText.cy, _sizeDDA.cy);
			_sizeText.cx += _sizeDDA.cx;
			if (bHaveIcon)
				_sizeText.cx += nTextToIconDistance;
			_size.cx += 4;
		}
		else
		{
			const CExtBarButton * pParentTBB = pTBB->ParentButtonGet();
			if ( pParentTBB != NULL
				&&	pParentTBB->IsKindOf(RUNTIME_CLASS(CExtRibbonButtonToolGroup))
				)
				_size.cx += 8;
			else if (
				pTBB->IsAbleToTrackMenu()
				&& ( !pTBB->IsKindOf(RUNTIME_CLASS(CExtRibbonButton)) )
				)
				_size.cx += 7;
		}
		_size.cx += _sizeText.cx;
		_size.cy = max(_sizeText.cy, _size.cy);
		_size.cx += rcCP.left + rcCP.right;
		_size.cy += rcCP.top + rcCP.bottom;
		if (pTBB->IsAbleToTrackMenu())
		{
			const CExtBarButton * pParentTBB = pTBB->ParentButtonGet();
			bool bInToolGroup =
				(pParentTBB != NULL
				&&	pParentTBB->IsKindOf(RUNTIME_CLASS(CExtRibbonButtonToolGroup))
				) ? true : false;
			if (bInToolGroup
				|| (nILV < __EXT_RIBBON_ILV_SIMPLE_LARGE
				&& (!pTBB->IsKindOf(RUNTIME_CLASS(CExtRibbonButtonGroup)))
				)
				)
			{
				// for axel.lehnert@innoplus.de, 2/2/2016
				// INT nDropDownButtonWidth = GetDropDownButtonWidth((CObject*) pTBB, lParam);
				// _size.cx += nDropDownButtonWidth;
				_size.cx += g_ribbon_settings_t.GetDropDownButtonWidth(this, ( CObject* ) pTBB, lParam);
				// for axel.lehnert@innoplus.de, 2/2/2016
			}
		}
		//		 CSize _sizeAdjust( 22, 22 );
		CSize _sizeAdjust(nBasicHeight + 6, nBasicHeight + 6);
		_sizeAdjust.cx = max(_sizeAdjust.cx, 22);
		_sizeAdjust.cy = max(_sizeAdjust.cy, 22);
		// 		_sizeAdjust.cx = UiScalingDo( _sizeAdjust.cx, __EUIST_X );
		// 		_sizeAdjust.cy = UiScalingDo( _sizeAdjust.cy, __EUIST_Y );
		_size.cx = max(_size.cx, _sizeAdjust.cx);
		_size.cy = max(_size.cy, _sizeAdjust.cy);
	}
	return _size;
}

bool CExtPaintManagerOffice2010_Impl::Ribbon_HaveSeparatorsBetweenToolGroups() const
{
	ASSERT_VALID(this); return true;
}

bool CExtPaintManagerOffice2010_Impl::Ribbon_FileButtonCalcLayout(
	CDC & dc,
	CExtNcFrameImpl * pExtNcFrameImpl,
	CExtRibbonButtonFile * pFileTBB,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	ASSERT(pExtNcFrameImpl != NULL);
	ASSERT_VALID(pFileTBB);
	if (IsHighContrast())
		return CExtPaintManagerXP::Ribbon_FileButtonCalcLayout(dc, pExtNcFrameImpl, pFileTBB, lParam);
	if (m_bmpArrRibbonFileButton.IsEmpty())
		return false;
	CExtRibbonBar * pRibbonBar = DYNAMIC_DOWNCAST(CExtRibbonBar, pFileTBB->GetBar());
	if (pRibbonBar == NULL)
		return false;
	ASSERT_VALID(pRibbonBar);
	INT nTextBasedWidth = 0;
	if (pRibbonBar->m_bFileTabButtonDisplaysText)
	{
		CExtSafeString sText = pFileTBB->GetText();
		if (!sText.IsEmpty())
		{
			CRect rcMeasure(0, 0, 0, 0);
			CFont * pFont = pRibbonBar->OnGetToolbarFont(false, false, pFileTBB);
			ASSERT(pFont->GetSafeHandle() != NULL);
			CFont * pOldFont = dc.SelectObject(pFont);
			INT nOldBkMode = dc.SetBkMode(TRANSPARENT);
			COLORREF clrText = RGB(0x0FF, 0x0FF, 0x0FF);
			COLORREF clrOldTextColor = dc.SetTextColor(clrText);
			if (g_PaintManager.m_bIsWinVistaOrLater)
			{
				CExtUxTheme::__EXT_UX_DTTOPTS dto = { sizeof(CExtUxTheme::__EXT_UX_DTTOPTS) };
				dto.dwFlags = __EXT_UX_DTT_COMPOSITED | __EXT_UX_DTT_TEXTCOLOR;
				dto.crText = clrText;
				CExtRichContentLayout::stat_DrawText(
					dc.m_hDC, LPCTSTR(sText), &rcMeasure,
					DT_SINGLELINE | DT_CENTER | DT_VCENTER | DT_NOPREFIX | DT_END_ELLIPSIS | DT_NOCLIP | DT_CALCRECT /*| ( bRTL ? DT_RTLREADING : 0 )*/, 0,
					pRibbonBar->m_hWnd, VSCLASS_WINDOW, /*__EXT_UX_OTD_NONCLIENT*/ 0, 0, 0, &dto
					);
			}
			else
				CExtRichContentLayout::stat_DrawText(
				dc.m_hDC, LPCTSTR(sText), &rcMeasure,
				DT_SINGLELINE | DT_CENTER | DT_VCENTER | DT_NOPREFIX | DT_END_ELLIPSIS | DT_NOCLIP | DT_CALCRECT /*| ( bRTL ? DT_RTLREADING : 0 )*/, 0
				);
			dc.SelectObject(pOldFont);
			dc.SetTextColor(clrOldTextColor);
			dc.SetBkMode(nOldBkMode);
			dc.SelectObject(pOldFont);
			nTextBasedWidth = rcMeasure.Width() + 10;
		} // if( ! sText.IsEmpty() )
	}
	CSize _sizeTBB(m_bmpArrRibbonFileButton.GetSize().cx, m_nRibbonFileButtonHeight - 1);
	_sizeTBB.cx = UiScalingDo(_sizeTBB.cx, __EUIST_X);
	_sizeTBB.cy = UiScalingDo(_sizeTBB.cy, __EUIST_Y);
	INT nPageIndex, nPageCount = pRibbonBar->RibbonTabPageButton_GetCount();
	for (nPageIndex = 0; nPageIndex < nPageCount; nPageIndex++)
	{
		CExtRibbonButtonTabPage * pTabPageTBB = pRibbonBar->RibbonTabPageButton_GetAt(nPageIndex);
		ASSERT(pTabPageTBB);
		if ((pTabPageTBB->GetStyle() & TBBS_HIDDEN) != 0)
			continue;
		if (!pTabPageTBB->IsVisible())
			continue;
		CSize sizeTabPageButton = pTabPageTBB->Size();
		_sizeTBB.cy = max(_sizeTBB.cy, sizeTabPageButton.cy);
		break;
	} // for( . . .
	if (nTextBasedWidth > 0)
		_sizeTBB.cx = max(_sizeTBB.cx, nTextBasedWidth);
	INT nTabLineHeight = pRibbonBar->RibbonLayout_GetTabLineHeight();
	INT nTopBorderHeight = 0;
	INT nFrameCaptionHeight = pRibbonBar->RibbonLayout_GetFrameCaptionHeight(&nTopBorderHeight);
	INT nHeightAtTheTop = nTabLineHeight + nFrameCaptionHeight;
	INT nBottomLineHeight = pRibbonBar->RibbonLayout_GetBottomLineHeight();
	CRect rcClient;
	pRibbonBar->GetClientRect(&rcClient);
	CRect rcPageBk = rcClient;
	rcPageBk.top += nHeightAtTheTop;
	rcPageBk.bottom -= nBottomLineHeight;
	if (pRibbonBar->RibbonLayout_IsFrameIntegrationEnabled()
		&& (!pRibbonBar->RibbonLayout_IsDwmCaptionIntegration())
		)
		rcPageBk.top += nFrameCaptionHeight + nTopBorderHeight;
	CRect rcTabLine(
		rcPageBk.left,
		rcPageBk.top - nTabLineHeight,
		rcPageBk.right,
		rcPageBk.top
		);
	pRibbonBar->RibbonLayout_AdjustTabLineRect(rcTabLine);
	INT nHorzOffset = UiScalingDo(2, __EUIST_X);
	CRect rcTBB(nHorzOffset, rcTabLine.bottom - _sizeTBB.cy, nHorzOffset + _sizeTBB.cx, rcTabLine.bottom);
	pFileTBB->Size(_sizeTBB);
	pFileTBB->SetRect(rcTBB);
	return true;
}

INT CExtPaintManagerOffice2010_Impl::RibbonLayout_CalcQatbAboveRibbonHorzStart(
	CExtRibbonBar * pRibbonBar,
	LPARAM lParam //= 0L
	)
{
	ASSERT_VALID(this);
	ASSERT_VALID(pRibbonBar);
	return CExtPaintManager::RibbonLayout_CalcQatbAboveRibbonHorzStart(pRibbonBar, lParam);
}

bool CExtPaintManagerOffice2010_Impl::Ribbon_IsSystemMenuIconPresent() const
{
	ASSERT_VALID(this); return true;
}
#endif // (!defined __EXT_MFC_NO_RIBBON_BAR)

// table of settings				Frame Text				Frame Icons					Dialog Text
//	case 3:		// Black				x						x							x
//	case 7:		// Azure dark grey		x						x							x
//	case 10:	// green dark grey		x						x							x
//	case 13:	// Orange Dark Grey		x						x							x
//	case 16:	// Red Dark Grey		x						x							x
//	case 19:	// ProfUIS_BLACK		x						x							x
//	case 22:	// ProfUIS_BBLUE		x						x							x
//	case 23:	// ProfUIS_SKYBLUE		x						x							x
//	case 24:	// ProfUIS_STEELBLUE	x						x							x
//	case 25:	// ProfUIS_GOLD			x						x							x

//	nIndex == 3 || nIndex == 8 || nIndex == 9 || nIndex == 11 || nIndex == 12 || nIndex == 14 || nIndex == 15 ||
//	nIndex == 17 || nIndex == 18 ||
//	nIndex == 19 || nIndex == 22 || nIndex == 23 || nIndex == 24 || nIndex == 25 || nIndex == 30){


void CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(int nIndex)
{
	m_bThemedMenuItemsEverywhere = true;
	m_clrFillHint = RGBIndexAuto(nIndex, RGB(205, 205, 205), RGB(192, 211, 235), RGB(233, 235, 238), RGB(188, 188, 188), &m_sImageElement);
	m_clrAccentHint = RGBIMapper(RGB(255, 227, 140), &m_sImageElement);
	m_clrIconAlphaColor = RGBIMapper(RGB(114, 121, 138), &m_sImageElement);
	//m_clrAdvTipGradientBottom = RGB(255, 255, 255);
	m_clrAdvTipGradientBottom = RGB(204, 219, 240);
	m_clrAdvTipGradientBottom = RGB(229, 229, 240);
	VERIFY(m_arrMdiRightIcons[0].m_bmpNormal.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_MDI_ICON_SC_MINIMIZE)));
	VERIFY(m_arrMdiRightIcons[1].m_bmpNormal.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_MDI_ICON_SC_RESTORE)));
	VERIFY(m_arrMdiRightIcons[2].m_bmpNormal.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_MDI_ICON_SC_CLOSE)));
	// this is the ClientAreaBkgnd settings
	COLORREF clrColorTest = RGBIndexAuto(nIndex, RGB(220, 226, 232), COLORREF(-1), COLORREF(-1), COLORREF(-1), &m_sImageElement);
	if (clrColorTest == COLORREF(-1))
	{
		// the first entry is just a filler
		VERIFY(m_bmpDCA.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_DCA_R2_BLUE,
			IDB_EXT_2010_OFFICE_DCA_R2_BLUE,
			IDB_EXT_2010_OFFICE_DCA_R2_SILVER,
			IDB_EXT_2010_OFFICE_DCA_R2_BLACK)), &m_sNCImageElement));		// Map Image 0
	}
	else
	{
		m_clrDCA_Solid = clrColorTest;
	}
	// this is a frame section
	m_bUseNcFrameBmpForDockerBkgnd = true;
	m_clrCustomDropDownGlyphEnabled = RGB(0, 0, 0);
	m_clrCustomDropDownGlyphDisabled = RGBIMapper(RGB(141, 141, 141), &m_sImageElement);
	m_clrDockingFrameDark = m_clrFloatingToolbarGripperSolid = RGBIMapper(RGB(83, 96, 125), &m_sImageElement);

	m_clrDockingFrameCaptionTextActive = RGBIMapper(RGB(51, 51, 51), &m_sImageElement);
	m_clrDockingFrameCaptionTextInactive = RGBIMapper(RGB(51, 51, 51), &m_sImageElement);
	m_clrDockingFrameCaptionTextFloatingFixed = RGBIMapper(RGB(255, 255, 255), &m_sImageElement);
	m_clrDockingCaptionShapeFixedNormal = RGBIMapper(RGB(255, 255, 255), &m_sImageElement);
	// this is a frame section
	// this is the distance from the edge to the inside of the NC area.
	m_rcNcFrameBordersActive.SetRect(m_nFrameSides, 4, m_nFrameSides, m_nFrameBottom);
	m_rcNcFrameBordersInactive.SetRect(m_nFrameSides, 4, m_nFrameSides, m_nFrameBottom);

	m_nCaptionHeightActive = m_nCaptionHeightInactive = 26;
	// change these to make header color change
	if (nIndex < 5)
	{
		VERIFY(m_bmpNcFrameActive.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_FRAME_ACTIVE_R1,
			IDB_EXT_2010_OFFICE_NC_FRAME_ACTIVE_R2_BLUE,
			IDB_EXT_2010_OFFICE_NC_FRAME_ACTIVE_R2_SILVER,
			IDB_EXT_2010_OFFICE_NC_FRAME_ACTIVE_R2_BLACK)), &m_sImageElement));		// Map Image 1
		VERIFY(m_bmpNcFrameInactive.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_FRAME_INACTIVE_R1,
			IDB_EXT_2010_OFFICE_NC_FRAME_INACTIVE_R2_BLUE,
			IDB_EXT_2010_OFFICE_NC_FRAME_INACTIVE_R2_SILVER,
			IDB_EXT_2010_OFFICE_NC_FRAME_INACTIVE_R2_BLACK)), &m_sImageElement));  // Map Image 2
	}
	else
	{
		// change the active bitmap to make the menu color change
		// the 32 is because the change occurred at release 3.20
		VERIFY(m_bmpNcFrameActive.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_FRAME_ACTIVE_R1_320,
			IDB_EXT_2010_OFFICE_NC_FRAME_ACTIVE_R2_320_BLUE,
			IDB_EXT_2010_OFFICE_NC_FRAME_ACTIVE_R2_320_SILVER,
			IDB_EXT_2010_OFFICE_NC_FRAME_ACTIVE_R2_320_BLACK)), &m_sNCImageElement));		// Map Image 1
		VERIFY(m_bmpNcFrameInactive.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_FRAME_INACTIVE_R1_320,
			IDB_EXT_2010_OFFICE_NC_FRAME_INACTIVE_R2_320_BLUE,
			IDB_EXT_2010_OFFICE_NC_FRAME_INACTIVE_R2_320_SILVER,
			IDB_EXT_2010_OFFICE_NC_FRAME_INACTIVE_R2_320_BLACK)), &m_sNCINAImageElement));  // Map Image 2
		VERIFY(m_bmpDialogBackGround.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_FRAME_ACTIVE_R1_320,
			IDB_EXT_2010_OFFICE_NC_FRAME_ACTIVE_R2_320_BLUE,
			IDB_EXT_2010_OFFICE_NC_FRAME_ACTIVE_R2_320_SILVER,
			IDB_EXT_2010_OFFICE_NC_FRAME_ACTIVE_R2_320_BLACK)), &m_sImageElement));		// Map Image 1
	}
	VERIFY(m_bmpNcFrameActive.Make32());
	VERIFY(m_bmpNcFrameInactive.Make32());
	m_bmpNcFrameActive.AlphaColor(m_clrTransparentNcFrameActive, RGB(0, 0, 0), 0);
	m_bmpNcFrameInactive.AlphaColor(m_clrTransparentNcFrameInactive, RGB(0, 0, 0), 0);

	int IndexDark = _IndexDark(nIndex);

	m_clrNcFrameTextActive = RGBIndexAutoLD(IndexDark, RGB(69, 69, 69), RGB(240, 240, 240));
	m_clrNcFrameTextInactive = RGBIndexAutoLD(IndexDark, RGB(100, 100, 100), RGB(220, 220, 220));

	m_bmpNcFrameActive.PreMultiplyRGBChannels();
	m_bmpNcFrameActive.PreMultipliedRGBChannelsSet(true);
	m_bmpNcFrameInactive.PreMultiplyRGBChannels();
	m_bmpNcFrameInactive.PreMultipliedRGBChannelsSet(true);
	if (nIndex < 5)
	{
		m_sizeNcButtonShapeInArr.cx = m_sizeNcButtonShapeInArr.cy = 21;
		m_nNcBtnIdxNormalActive = m_nNcBtnIdxNormalInactive = 0;

		// this sets all of the rect width for the NC icons
		m_rcNcButtonBkPadding.SetRect(4, 4, 4, 4);
		VERIFY(m_bmpNcButtonBkHover.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_HOVER_R1,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_HOVER_R2_BLUE,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_HOVER_R2_SILVER,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_HOVER_R2_BLACK)), &m_sImageElement));		// Map Image 3
		VERIFY(m_bmpNcButtonBkPressed.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_R1,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_R2_BLUE,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_R2_SILVER,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_R2_BLACK)), &m_sImageElement));		// Map Image 4
		VERIFY(m_bmpNcButtonBkHover.Make32());
		VERIFY(m_bmpNcButtonBkPressed.Make32());
		m_bmpNcButtonBkHover.AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), 0);
		m_bmpNcButtonBkPressed.AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), 0);
		m_bmpNcButtonBkHover.PreMultiplyRGBChannels();
		m_bmpNcButtonBkHover.PreMultipliedRGBChannelsSet(true);
		m_bmpNcButtonBkPressed.PreMultiplyRGBChannels();
		m_bmpNcButtonBkPressed.PreMultipliedRGBChannelsSet(true);
		VERIFY(m_bmpNcButtonBkHoverX.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_HOVER_R1_X,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_HOVER_X_R2_BLUE,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_HOVER_X_R2_SILVER,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_HOVER_X_R2_BLACK)), &m_sImageElement));		// Map Image 5
		VERIFY(m_bmpNcButtonBkPressedX.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_R1_X,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_X_R2_BLUE,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_X_R2_SILVER,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_X_R2_BLACK)), &m_sImageElement));	// Map Image 6
		VERIFY(m_bmpNcButtonBkHoverX.Make32());
		VERIFY(m_bmpNcButtonBkPressedX.Make32());
		m_bmpNcButtonBkHoverX.AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), 0);
		m_bmpNcButtonBkPressedX.AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), 0);
		m_bmpNcButtonBkHoverX.PreMultiplyRGBChannels();
		m_bmpNcButtonBkHoverX.PreMultipliedRGBChannelsSet(true);
		m_bmpNcButtonBkPressedX.PreMultiplyRGBChannels();
		m_bmpNcButtonBkPressedX.PreMultipliedRGBChannelsSet(true);
		m_sizeNcButtonShapeInArr.cx = 13;		// was 13
		m_sizeNcButtonShapeInArr.cy = 12;
		m_nNcBtnIdxNormalActive = 0;
		m_nNcBtnIdxHover = 1;
		m_nNcBtnIdxPressed = 2;
		m_nNcBtnIdxNormalInactive = 3;
		m_nNcBtnIdxDisabled = 4;
		VERIFY(m_bmpArrNcButtonsClose.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_ARR_CLOSE_R1,
			IDB_EXT_2010_OFFICE_NC_ARR_CLOSE_R2_BLUE,
			IDB_EXT_2010_OFFICE_NC_ARR_CLOSE_R2_SILVER,
			IDB_EXT_2010_OFFICE_NC_ARR_CLOSE_R2_BLACK)), &m_sImageElement));		// Map Image 7
		//	m_bmpArrNcButtonsClose.PreMultiplyRGBChannels();
		m_bmpArrNcButtonsClose.PreMultipliedRGBChannelsSet(true);
		CSize sz = m_bmpArrNcButtonsClose.GetSize();
		m_bmpArrNcButtonsClose.Scale(g_PaintManager->UiScalingDo(sz.cx, CExtPaintManager::__EUIST_X), g_PaintManager->UiScalingDo(sz.cy, CExtPaintManager::__EUIST_Y));
		VERIFY(m_bmpArrNcButtonsMaximize.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_ARR_MAXIMIZE_R1,
			IDB_EXT_2010_OFFICE_NC_ARR_MAXIMIZE_R2_BLUE,
			IDB_EXT_2010_OFFICE_NC_ARR_MAXIMIZE_R2_SILVER,
			IDB_EXT_2010_OFFICE_NC_ARR_MAXIMIZE_R2_BLACK)), &m_sImageElement));		// Map Image 8
		//	m_bmpArrNcButtonsMaximize.PreMultiplyRGBChannels();
		m_bmpArrNcButtonsMaximize.PreMultipliedRGBChannelsSet(true);
		VERIFY(m_bmpArrNcButtonsRestore.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_ARR_RESTORE_R1,
			IDB_EXT_2010_OFFICE_NC_ARR_RESTORE_R2_BLUE,
			IDB_EXT_2010_OFFICE_NC_ARR_RESTORE_R2_SILVER,
			IDB_EXT_2010_OFFICE_NC_ARR_RESTORE_R2_BLACK)), &m_sImageElement));		// Map Image 9
		//	m_bmpArrNcButtonsRestore.PreMultiplyRGBChannels();
		m_bmpArrNcButtonsRestore.PreMultipliedRGBChannelsSet(true);
		VERIFY(m_bmpArrNcButtonsMinimize.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_ARR_MINIMIZE_R1,
			IDB_EXT_2010_OFFICE_NC_ARR_MINIMIZE_R2_BLUE,
			IDB_EXT_2010_OFFICE_NC_ARR_MINIMIZE_R2_SILVER,
			IDB_EXT_2010_OFFICE_NC_ARR_MINIMIZE_R2_BLACK)), &m_sImageElement));		// Map Image 10
		//	m_bmpArrNcButtonsMinimize.PreMultiplyRGBChannels();
		m_bmpArrNcButtonsMinimize.PreMultipliedRGBChannelsSet(true);
		// in 2010 this only has one image
		VERIFY(m_bmpArrNcButtonsHelp.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_NC_ARR_HELP_R1)));

		m_bmpArrNcButtonsHelp.PreMultiplyRGBChannels();
	}
	else{
		// this is the 320 frame Icons 
		m_sizeNcButtonShapeInArr.cx = m_sizeNcButtonShapeInArr.cy = 21;
		m_nNcBtnIdxNormalActive = m_nNcBtnIdxNormalInactive = 0;

		// this sets all of the rect width for the ur icons
		m_rcNcButtonBkPadding.SetRect(4, 4, 4, 4);
		VERIFY(m_bmpNcButtonBkHover.LoadBMP_Resource(MAKEINTRESOURCE(ResourceIndexedLD(nIndex,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_HOVER_R1_320,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_HOVER_LIGHT_R1_320))));
		VERIFY(m_bmpNcButtonBkPressed.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_R1_320,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_R2_320_BLUE,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_R2_320_SILVER,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_R2_320_BLACK)), &m_sImageElement));		// Map Image 4
		VERIFY(m_bmpNcButtonBkHover.Make32());
		VERIFY(m_bmpNcButtonBkPressed.Make32());
		m_bmpNcButtonBkHover.AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), 0);
		m_bmpNcButtonBkPressed.AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), 0);
		m_bmpNcButtonBkHover.PreMultiplyRGBChannels();
		m_bmpNcButtonBkHover.PreMultipliedRGBChannelsSet(true);
		m_bmpNcButtonBkPressed.PreMultiplyRGBChannels();
		m_bmpNcButtonBkPressed.PreMultipliedRGBChannelsSet(true);
		VERIFY(m_bmpNcButtonBkActive_X.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_NC_CLOSE_BK_ACTIVE_R1_320)));
		VERIFY(m_bmpNcButtonBkInActive_X.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_NC_CLOSE_BK_INACTIVE_R1_320)));
		VERIFY(m_bmpNcButtonBkHoverX.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_NC_BUTTON_BK_HOVER_R1_X_320)));
		VERIFY(m_bmpNcButtonBkPressedX.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_R1_X_320,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_X_R2_320_BLUE,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_X_R2_320_SILVER,
			IDB_EXT_2010_OFFICE_NC_BUTTON_BK_PRESSED_X_R2_320_BLACK)), &m_sImageElement));	// Map Image 6
		VERIFY(m_bmpNcButtonBkHoverX.Make32());
		VERIFY(m_bmpNcButtonBkPressedX.Make32());
		m_bmpNcButtonBkHoverX.AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), 0);
		m_bmpNcButtonBkPressedX.AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), 0);
		m_bmpNcButtonBkHoverX.PreMultiplyRGBChannels();
		m_bmpNcButtonBkHoverX.PreMultipliedRGBChannelsSet(true);
		m_bmpNcButtonBkPressedX.PreMultiplyRGBChannels();
		m_bmpNcButtonBkPressedX.PreMultipliedRGBChannelsSet(true);
		m_sizeNcButtonShapeInArr.cx = 13;		// was 13
		m_sizeNcButtonShapeInArr.cy = 12;
		m_nNcBtnIdxNormalActive = 0;
		m_nNcBtnIdxHover = 1;
		m_nNcBtnIdxPressed = 2;
		m_nNcBtnIdxNormalInactive = 3;
		m_nNcBtnIdxDisabled = 4;
		VERIFY(m_bmpArrNcButtonsClose.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_NC_ARR_CLOSE_R1_320)));
		m_bmpArrNcButtonsClose.PreMultiplyRGBChannels();
		CSize sz = m_bmpArrNcButtonsClose.GetSize();
		m_bmpArrNcButtonsClose.Scale(g_PaintManager->UiScalingDo(sz.cx, CExtPaintManager::__EUIST_X), g_PaintManager->UiScalingDo(sz.cy, CExtPaintManager::__EUIST_Y));
		//m_bmpArrNcButtonsClose.PreMultipliedRGBChannelsSet(true);
		VERIFY(m_bmpArrNcButtonsMaximize.LoadBMP_Resource(MAKEINTRESOURCE(ResourceIndexedLD(nIndex,
			IDB_EXT_2010_OFFICE_NC_ARR_MAXIMIZE_R1_320,
			IDB_EXT_2010_OFFICE_NC_ARR_MAXIMIZE_R2_320_BLACK))));		// Map Image 8
		m_bmpArrNcButtonsMaximize.PreMultiplyRGBChannels();
		//m_bmpArrNcButtonsMaximize.PreMultipliedRGBChannelsSet(true);
		VERIFY(m_bmpArrNcButtonsRestore.LoadBMP_Resource(MAKEINTRESOURCE(ResourceIndexedLD(nIndex,
			IDB_EXT_2010_OFFICE_NC_ARR_RESTORE_R1_320,
			IDB_EXT_2010_OFFICE_NC_ARR_RESTORE_R2_320_BLACK))));		// Map Image 9
		m_bmpArrNcButtonsRestore.PreMultiplyRGBChannels();
		//m_bmpArrNcButtonsRestore.PreMultipliedRGBChannelsSet(true);
		VERIFY(m_bmpArrNcButtonsMinimize.LoadBMP_Resource(MAKEINTRESOURCE(ResourceIndexedLD(nIndex,
			IDB_EXT_2010_OFFICE_NC_ARR_MINIMIZE_R1_320,
			IDB_EXT_2010_OFFICE_NC_ARR_MINIMIZE_R2_320_BLACK))));		// Map Image 10
		m_bmpArrNcButtonsMinimize.PreMultiplyRGBChannels();
		//m_bmpArrNcButtonsMinimize.PreMultipliedRGBChannelsSet(true);
		// in 2010 this only has one image
		VERIFY(m_bmpArrNcButtonsHelp.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_NC_ARR_HELP_R1_320)));

		m_bmpArrNcButtonsHelp.PreMultiplyRGBChannels();

	}
	// end of frame section

#if (!defined __EXT_MFC_NO_STATUSBAR)
	//This switches the text color, dark and light
	if ( IndexDark )
	{
		// dark colors
		clrColorTest = RGB(235, 245, 255);
		m_clrStatusPaneTextColorNormal = clrColorTest;
		m_clrStatusPaneTextColorDisabled = clrColorTest;
	}
	else
	{
		// lite colors
		clrColorTest = RGB(30, 40, 50);
		m_clrStatusPaneTextColorNormal = clrColorTest;
		m_clrStatusPaneTextColorDisabled = clrColorTest;
	}
	m_rcStatusBkPadding.SetRect(0, 2, 0, 2);
	VERIFY(m_bmpStatusBkLight.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_STATUSBAR_BK_LIGHT_R1,
		IDB_EXT_2010_OFFICE_STATUSBAR_BK_LIGHT_R2_BLUE,
		IDB_EXT_2010_OFFICE_STATUSBAR_BK_LIGHT_R2_SILVER,
		IDB_EXT_2010_OFFICE_STATUSBAR_BK_LIGHT_R2_BLACK)), &m_sNCINAImageElement));		// Map Image 11
	VERIFY(m_bmpStatusBkDark.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_STATUSBAR_BK_DARK_R1,
		IDB_EXT_2010_OFFICE_STATUSBAR_BK_DARK_R2_BLUE,
		IDB_EXT_2010_OFFICE_STATUSBAR_BK_DARK_R2_SILVER,
		IDB_EXT_2010_OFFICE_STATUSBAR_BK_DARK_R2_BLACK)), &m_sNCINAImageElement));		// Map Image 12
	m_rcStatusSeparatorPadding.SetRect(0, 2, 0, 2);
	VERIFY(m_bmpStatusSeparator.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_STATUSBAR_SEPARATOR_R1,
		IDB_EXT_2010_OFFICE_STATUSBAR_SEPARATOR_R2_BLUE,
		IDB_EXT_2010_OFFICE_STATUSBAR_SEPARATOR_R2_SILVER,
		IDB_EXT_2010_OFFICE_STATUSBAR_SEPARATOR_R2_BLACK)), &m_sNCINAImageElement));		// Map Image 13
#endif // (!defined __EXT_MFC_NO_STATUSBAR)

#if (!defined __EXT_MFC_NO_TAB_CTRL)
	m_clrTabTextNormal = RGBIndexAuto(nIndex, RGB(52, 52, 64), RGB(52, 52, 64), RGB(52, 52, 64), RGB(226, 226, 226), &m_sImageElement);
	// m_clrTabTextSelected = RGB(52, 52, 64);			//  black 4 only
	clrColorTest = RGBIndexAuto(nIndex, RGB(52, 52, 64), RGB(52, 52, 64), RGB(52, 52, 64), COLORREF(-1), &m_sImageElement);
	if (clrColorTest != -1){
		m_clrTabTextSelected = clrColorTest;
	}
	m_clrTabTextNormalDWM = RGBIndexAuto(nIndex, RGB(76, 83, 92), RGB(30, 57, 91), RGB(76, 83, 92), RGB(226, 226, 226), &m_sImageElement);
	// m_clrTabTextSelectedDWM = RGB(0, 0, 0); // black 4 only
	clrColorTest = RGBIndexAuto(nIndex, RGB(76, 83, 92), RGB(30, 57, 91), RGB(76, 83, 92), COLORREF(-1), &m_sImageElement);
	if (clrColorTest != -1)
	{
		m_clrTabTextSelectedDWM = clrColorTest;
	}
	else
	{
		// this could be broken apart into two if statements
		m_rcStatusBkPadding.SetRect(0, 2, 0, 1);
		VERIFY(m_bmpStatusBkLight.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_STATUSBAR_BK_LIGHT_R1,
			IDB_EXT_2010_OFFICE_STATUSBAR_BK_LIGHT_R2_BLUE,
			IDB_EXT_2010_OFFICE_STATUSBAR_BK_LIGHT_R2_SILVER,
			IDB_EXT_2010_OFFICE_STATUSBAR_BK_LIGHT_R2_BLACK)), &m_sImageElement));
		VERIFY(m_bmpStatusBkDark.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_STATUSBAR_BK_DARK_R1,
			IDB_EXT_2010_OFFICE_STATUSBAR_BK_DARK_R2_BLUE,
			IDB_EXT_2010_OFFICE_STATUSBAR_BK_DARK_R2_SILVER,
			IDB_EXT_2010_OFFICE_STATUSBAR_BK_DARK_R2_BLACK)), &m_sImageElement));
		m_rcStatusSeparatorPadding.SetRect(0, 2, 0, 1);
		VERIFY(m_bmpStatusSeparator.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_STATUSBAR_SEPARATOR_R1,
			IDB_EXT_2010_OFFICE_STATUSBAR_SEPARATOR_R2_BLUE,
			IDB_EXT_2010_OFFICE_STATUSBAR_SEPARATOR_R2_SILVER,
			IDB_EXT_2010_OFFICE_STATUSBAR_SEPARATOR_R2_BLACK)), &m_sImageElement));
	}

	int IndexTab = _IndexTab(nIndex);

	if ( IndexTab )
	{
		m_nIdxTabShapeHover = 1;
		m_nIdxTabShapePressed = 0;
		m_nIdxTabShapeSelected = 0;
		m_nIdxTabShapeSelectedHover = 0;
	}
	else
	{
		// this is all of the other nIndex types
		m_nIdxTabShapeHover = 0;
		m_nIdxTabShapePressed = 1;
		m_nIdxTabShapeSelected = 1;
		m_nIdxTabShapeSelectedHover = 1;
	}
	m_arrSizeTabShape[__ETSOI_TOP].cx
		= m_arrSizeTabShape[__ETSOI_BOTTOM].cx
		= m_arrSizeTabShape[__ETSOI_LEFT].cy
		= m_arrSizeTabShape[__ETSOI_RIGHT].cy
		= 13;
	m_arrSizeTabShape[__ETSOI_TOP].cy
		= m_arrSizeTabShape[__ETSOI_BOTTOM].cy
		= m_arrSizeTabShape[__ETSOI_LEFT].cx
		= m_arrSizeTabShape[__ETSOI_RIGHT].cx
		= 23;
	m_arrRectTabShapePadding[__ETSOI_TOP].SetRect(4, 3, 4, 3);
	m_arrRectTabShapePadding[__ETSOI_BOTTOM].SetRect(4, 3, 4, 3);
	m_arrRectTabShapePadding[__ETSOI_LEFT].SetRect(3, 4, 3, 4);
	m_arrRectTabShapePadding[__ETSOI_RIGHT].SetRect(3, 4, 3, 4);
	VERIFY(
		m_arrBmpTabShapeNormal[__ETSOI_TOP].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_TAB_SHAPE_ARR_R1,
		IDB_EXT_2010_OFFICE_TAB_SHAPE_ARR_R2_BLUE,
		IDB_EXT_2010_OFFICE_TAB_SHAPE_ARR_R2_SILVER,
		IDB_EXT_2010_OFFICE_TAB_SHAPE_ARR_R2_BLACK)), &m_sImageElement)); // Map Image 14

	m_arrBmpTabShapeNormal[__ETSOI_TOP].PreMultipliedRGBChannelsSet(true);
	VERIFY(
		m_arrBmpTabShapeNormal[__ETSOI_BOTTOM].CreateRotated9xStack(
		m_arrBmpTabShapeNormal[__ETSOI_TOP],
		180,
		4,
		m_arrSizeTabShape[__ETSOI_TOP].cx,
		m_arrSizeTabShape[__ETSOI_TOP].cy,
		false,
		false
		)
		);
	VERIFY(
		m_arrBmpTabShapeNormal[__ETSOI_LEFT].CreateRotated9xStack(
		m_arrBmpTabShapeNormal[__ETSOI_TOP],
		270,
		4,
		m_arrSizeTabShape[__ETSOI_TOP].cx,
		m_arrSizeTabShape[__ETSOI_TOP].cy,
		false,
		false
		)
		);
	VERIFY(
		m_arrBmpTabShapeNormal[__ETSOI_RIGHT].CreateRotated9xStack(
		m_arrBmpTabShapeNormal[__ETSOI_TOP],
		90,
		4,
		m_arrSizeTabShape[__ETSOI_TOP].cx,
		m_arrSizeTabShape[__ETSOI_TOP].cy,
		false,
		false
		)
		);

	VERIFY(
		m_arrBmpTabShapeSelArea[__ETSOI_TOP].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_TAB_SHAPE_ARR_SEL_AREA_R1,
		IDB_EXT_2010_OFFICE_TAB_SHAPE_ARR_SEL_AREA_R2_BLUE,
		IDB_EXT_2010_OFFICE_TAB_SHAPE_ARR_SEL_AREA_R2_SILVER,
		IDB_EXT_2010_OFFICE_TAB_SHAPE_ARR_SEL_AREA_R2_BLACK)), &m_sImageElement));		// Map Image 15

	if ( !IndexTab )
	{
		m_arrBmpTabShapeSelArea[__ETSOI_TOP].PreMultipliedRGBChannelsSet(true);
	}
	VERIFY(
		m_arrBmpTabShapeSelArea[__ETSOI_BOTTOM].CreateRotated9xStack(
		m_arrBmpTabShapeSelArea[__ETSOI_TOP],
		180,
		4,
		m_arrSizeTabShape[__ETSOI_TOP].cx,
		m_arrSizeTabShape[__ETSOI_TOP].cy,
		false,
		false
		)
		);
	VERIFY(
		m_arrBmpTabShapeSelArea[__ETSOI_LEFT].CreateRotated9xStack(
		m_arrBmpTabShapeSelArea[__ETSOI_TOP],
		270,
		4,
		m_arrSizeTabShape[__ETSOI_TOP].cx,
		m_arrSizeTabShape[__ETSOI_TOP].cy,
		false,
		false
		)
		);

	VERIFY(
		m_arrBmpTabShapeSelArea[__ETSOI_RIGHT].CreateRotated9xStack(
		m_arrBmpTabShapeSelArea[__ETSOI_TOP],
		90,
		4,
		m_arrSizeTabShape[__ETSOI_TOP].cx,
		m_arrSizeTabShape[__ETSOI_TOP].cy,
		false,
		false
		)
		);

	m_arrRectTabAreaPadding[__ETSOI_TOP].SetRect(3, 3, 3, 3);
	m_arrRectTabAreaPadding[__ETSOI_BOTTOM].SetRect(3, 3, 3, 3);
	m_arrRectTabAreaPadding[__ETSOI_LEFT].SetRect(3, 3, 3, 3);
	m_arrRectTabAreaPadding[__ETSOI_RIGHT].SetRect(3, 3, 3, 3);
	m_arrTabAreaMargins[__ETSOI_TOP] = 1;
	m_arrTabAreaMargins[__ETSOI_BOTTOM] = 1;
	m_arrTabAreaMargins[__ETSOI_LEFT] = 1;
	m_arrTabAreaMargins[__ETSOI_RIGHT] = 1;
	VERIFY(
		m_arrBmpTabArea[__ETSOI_TOP].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_TAB_AREA_BK_GRAY,
		IDB_EXT_2010_TAB_AREA_BK_BLUE,
		IDB_EXT_2010_TAB_AREA_BK_GRAY,
		IDB_EXT_2010_TAB_AREA_BK_BLACK)), &m_sImageElement));	// Map Image 16

	VERIFY(m_arrBmpTabArea[__ETSOI_TOP].Make32());
	CSize _sizeTabArea = m_arrBmpTabArea[__ETSOI_TOP].GetSize();
	m_arrBmpTabArea[__ETSOI_TOP].PreMultipliedRGBChannelsSet(true);
	VERIFY(
		m_arrBmpTabArea[__ETSOI_BOTTOM].CreateRotated9xStack(
		m_arrBmpTabArea[__ETSOI_TOP],
		180,
		1,
		_sizeTabArea.cx,
		_sizeTabArea.cy,
		false,
		false
		)
		);
	VERIFY(
		m_arrBmpTabArea[__ETSOI_LEFT].CreateRotated9xStack(
		m_arrBmpTabArea[__ETSOI_TOP],
		270,
		1,
		_sizeTabArea.cx,
		_sizeTabArea.cy,
		false,
		false
		)
		);
	VERIFY(
		m_arrBmpTabArea[__ETSOI_RIGHT].CreateRotated9xStack(
		m_arrBmpTabArea[__ETSOI_TOP],
		90,
		1,
		_sizeTabArea.cx,
		_sizeTabArea.cy,
		false,
		false
		)
		);

	m_arrSizeTabSeparator[__ETSOI_TOP].cx
		= m_arrSizeTabSeparator[__ETSOI_BOTTOM].cx
		= m_arrSizeTabSeparator[__ETSOI_LEFT].cy
		= m_arrSizeTabSeparator[__ETSOI_RIGHT].cy
		= 2;
	m_arrSizeTabSeparator[__ETSOI_TOP].cy
		= m_arrSizeTabSeparator[__ETSOI_BOTTOM].cy
		= m_arrSizeTabSeparator[__ETSOI_LEFT].cx
		= m_arrSizeTabSeparator[__ETSOI_RIGHT].cx
		= 23;
	m_arrRectTabSeparatorPadding[__ETSOI_TOP].SetRect(0, 2, 0, 2);
	m_arrRectTabSeparatorPadding[__ETSOI_BOTTOM].SetRect(0, 2, 0, 2);
	m_arrRectTabSeparatorPadding[__ETSOI_LEFT].SetRect(2, 0, 2, 0);
	m_arrRectTabSeparatorPadding[__ETSOI_RIGHT].SetRect(2, 0, 2, 0);
	VERIFY(
		m_arrBmpTabSeparator[__ETSOI_TOP].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_TAB_SEPARATOR_R1,
		IDB_EXT_2010_OFFICE_TAB_SEPARATOR_R2_BLUE,
		IDB_EXT_2010_OFFICE_TAB_SEPARATOR_R2_SILVER,
		IDB_EXT_2010_OFFICE_TAB_SEPARATOR_R2_BLACK)), &m_sImageElement));	// Map Image 17

	VERIFY(m_arrBmpTabSeparator[__ETSOI_TOP].Make32());
	if ( IndexTab )
	{
		m_arrBmpTabSeparator[__ETSOI_TOP].PreMultiplyRGBChannels();
		m_arrBmpTabSeparator[__ETSOI_TOP].PreMultipliedRGBChannelsSet(true);
	}
	VERIFY(
		m_arrBmpTabSeparator[__ETSOI_BOTTOM].CreateRotated9xStack(
		m_arrBmpTabSeparator[__ETSOI_TOP],
		180,
		1,
		m_arrSizeTabSeparator[__ETSOI_TOP].cx,
		m_arrSizeTabSeparator[__ETSOI_TOP].cy,
		false,
		false
		)
		);
	VERIFY(
		m_arrBmpTabSeparator[__ETSOI_LEFT].CreateRotated9xStack(
		m_arrBmpTabSeparator[__ETSOI_TOP],
		270,
		1,
		m_arrSizeTabSeparator[__ETSOI_TOP].cx,
		m_arrSizeTabSeparator[__ETSOI_TOP].cy,
		false,
		false
		)
		);
	VERIFY(
		m_arrBmpTabSeparator[__ETSOI_RIGHT].CreateRotated9xStack(
		m_arrBmpTabSeparator[__ETSOI_TOP],
		90,
		1,
		m_arrSizeTabSeparator[__ETSOI_TOP].cx,
		m_arrSizeTabSeparator[__ETSOI_TOP].cy,
		false,
		false
		)
		);
#endif // (!defined __EXT_MFC_NO_TAB_CTRL)

	VERIFY(
		m_bmpArrStatesCheckBox.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_CHECK_BOX_R1)
		)
		);
	VERIFY(
		m_bmpArrStatesRadioButton.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_RADIO_BUTTON_R1)
		)
		);

	m_clrMenuItemLeftAreaMarginL = RGB(226, 228, 231);
	m_clrMenuItemLeftAreaMarginR = RGB(245, 245, 245);
	m_rcMenuItemPaddingLargeLeft.SetRect(3, 3, 3, 16);
	m_rcMenuItemPaddingLargeRight.SetRect(1, 3, 3, 16);
	m_rcMenuItemPaddingSmallLeft.SetRect(3, 3, 3, 10);
	m_rcMenuItemPaddingSmallRight.SetRect(1, 3, 3, 10);

	VERIFY(
		m_bmpMenuItemLargeLeft.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_MENU_ITEM_R1_LARGE_LEFT)
		)
		);
	VERIFY(
		m_bmpMenuItemLargeRight.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_MENU_ITEM_R1_LARGE_RIGHT)
		)
		);
	VERIFY(
		m_bmpMenuItemSmallLeft.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_MENU_ITEM_R1_SMALL_LEFT)
		)
		);
	VERIFY(
		m_bmpMenuItemSmallRight.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_MENU_ITEM_R1_SMALL_RIGHT)
		)
		);
	VERIFY(
		m_bmpMenuArrow.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_MENU_ITEM_ARROW)
		)
		);

	m_sizeMenuCheckAreaChecked.cx = g_bAutoScaleIcons ? INT(16 * g_PaintManager.m_dDpiAwareScaling) : 16;
	m_sizeMenuCheckAreaChecked.cy = g_bAutoScaleIcons ? INT(16 * g_PaintManager.m_dDpiAwareScaling) : 16;
	m_sizeMenuCheckAreaUnchecked.cx = g_bAutoScaleIcons ? INT(20 * g_PaintManager.m_dDpiAwareScaling) : 20;
	m_sizeMenuCheckAreaUnchecked.cy = g_bAutoScaleIcons ? INT(20 * g_PaintManager.m_dDpiAwareScaling) : 20;

	m_arrRectMenuCheckAreaCheckedPadding[0].SetRect(1, 1, 1, 1);
	m_arrRectMenuCheckAreaCheckedPadding[1].SetRect(1, 1, 1, 1);
	m_arrRectMenuCheckAreaUncheckedPadding[0].SetRect(1, 1, 1, 1);
	m_arrRectMenuCheckAreaUncheckedPadding[1].SetRect(1, 1, 1, 1);
	// These two have been checked
	VERIFY(m_bmpMenuCheckAreaChecked.LoadBMP_Resource_AutoScale(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_MENU_CHECK_AREA_CHECKED_R1)));
	VERIFY(m_bmpMenuCheckAreaUnchecked.LoadBMP_Resource_AutoScale(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_MENU_CHECK_AREA_UNCHECKED_R1)));

#if (!defined __EXT_MFC_NO_PAGE_NAVIGATOR )
	m_arrClrPnCaptNormal[0] = RGBIMapper(RGB(235, 238, 250), &m_sImageElement);
	m_arrClrPnCaptNormal[1] = RGBIMapper(RGB(215, 219, 229), &m_sImageElement);
	m_arrClrPnCaptNormal[2] = RGBIMapper(RGB(197, 199, 209), &m_sImageElement);
	m_arrClrPnCaptNormal[3] = RGBIMapper(RGB(212, 216, 226), &m_sImageElement);
	m_arrClrPnSplitter[0] = RGBIndexAuto(nIndex, RGB(240, 243, 247), RGB(219, 235, 255), RGB(240, 243, 247), RGB(102, 102, 102), &m_sImageElement);
	m_arrClrPnSplitter[1] = RGBIndexAuto(nIndex, RGB(215, 219, 225), RGB(198, 216, 237), RGB(215, 219, 225), RGB(79, 79, 79), &m_sImageElement);
	m_clrPnCaptText = RGBIMapper(RGB(30, 57, 91), &m_sImageElement);
	m_arrClrPnItemText[0] = RGBIMapper(RGB(0, 0, 0), &m_sImageElement);
	m_arrClrPnItemText[1] = RGBIMapper(RGB(76, 83, 92), &m_sImageElement);
	m_clrPnConfigButton = RGBIMapper(RGB(101, 104, 112), &m_sImageElement);
	// these two have been checked
	static const UINT g_arrResIdPnItemExpanded[ePn_Count] =
	{
		IDB_EXT_2010_OFFICE_PN_ITEM_EXPANDED_NORMAL_R1,
		IDB_EXT_2010_OFFICE_PN_ITEM_EXPANDED_HOVER_R1,
		IDB_EXT_2010_OFFICE_PN_ITEM_EXPANDED_PRESSED_R1,
		IDB_EXT_2010_OFFICE_PN_ITEM_EXPANDED_SELECTED_R1,
		IDB_EXT_2010_OFFICE_PN_ITEM_EXPANDED_SELECTED_HOVER_R1,
	};
	static const UINT g_arrResIdPnItemCollapsed[ePn_Count] =
	{
		IDB_EXT_2010_OFFICE_PN_ITEM_COLLAPSED_NORMAL_R1,
		IDB_EXT_2010_OFFICE_PN_ITEM_COLLAPSED_HOVER_R1,
		IDB_EXT_2010_OFFICE_PN_ITEM_COLLAPSED_PRESSED_R1,
		IDB_EXT_2010_OFFICE_PN_ITEM_COLLAPSED_SELECTED_R1,
		IDB_EXT_2010_OFFICE_PN_ITEM_COLLAPSED_SELECTED_HOVER_R1,
	};
	INT nPnIndex;
	for (nPnIndex = 0; nPnIndex < INT(ePn_Count); nPnIndex++)
	{
		VERIFY(m_arrBmpPnItemExpanded[nPnIndex].LoadBMP_Resource(MAKEINTRESOURCE(g_arrResIdPnItemExpanded[nPnIndex])));
		VERIFY(m_arrBmpPnItemCollapsed[nPnIndex].LoadBMP_Resource(MAKEINTRESOURCE(g_arrResIdPnItemCollapsed[nPnIndex])));
		m_arrRcPnItemPaddingExpanded[nPnIndex].SetRect(0, 2, 0, 2);
		m_arrRcPnItemPaddingCollapsed[nPnIndex].SetRect(0, 0, 0, 0);
		m_arrClrPnItemTextBmpVer[nPnIndex] = RGB(0, 0, 0);
	}
#endif // (!defined __EXT_MFC_NO_PAGE_NAVIGATOR )

	if ( g_PaintManager.m_nDpiAwareValue == CExtPaintManagerAutoPtr::__PROFUISDPI_SIZENORMAL )
	{
		VERIFY(m_bmpArrComboBoxDropDown.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_COMBO_BOX_BUTTON_R1)));
		m_bmpArrComboBoxDropDown.PreMultipliedRGBChannelsSet(true);
		//XXXXX Nelu m_arrClrComboBoxBorder[0] = RGBIMapper(RGB(212, 214, 217), &m_sImageElement);
		m_arrClrComboBoxBorder[0] = RGBIMapper(RGB(164, 164, 164), &m_sImageElement);		
		m_arrClrComboBoxBorder[1] = RGBIndexAuto(nIndex, RGB(180, 180, 180), RGB(164, 164, 164), RGB(164, 164, 164), RGB(164, 164, 164), &m_sImageElement);
		m_arrClrComboBoxBorder[2] = RGBIndexAuto(nIndex, RGB(212, 214, 217), RGB(164, 164, 164), RGB(164, 164, 164), RGB(164, 164, 164), &m_sImageElement);
		m_arrClrComboBoxBorder[3] = RGBIMapper(RGB(228, 231, 235), &m_sImageElement);
	}

	CRect SbSkinDataPaddingH(5, 3, 5, 3), SbSkinDataPaddingV(3, 5, 3, 5);
	CRect rcEntirePaddingRH(2, 2, 2, 2), rcEntirePaddingRV(2, 2, 2, 2);
	CRect rcEntirePaddingCH(1, 1, 1, 1), rcEntirePaddingCV(1, 1, 1, 1);

	if ( !IndexDark )
	{
		m_SbSkinDataT_Zoom.Load2010_Zoom_R1(
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_PLUS_R1,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_MINUS_R1,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_THUMB_R1,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLACK),	// Map Image 19 - 21
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(157, 166, 176), RGB(138, 156, 184), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(157, 166, 176), RGB(138, 156, 184), &m_sImageElement),
			AFX_IDW_DOCKBAR_TOP,
			&m_sImageElement
			);
		m_SbSkinDataB_Zoom.Load2010_Zoom_R1(
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_PLUS_R1,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_MINUS_R1,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_THUMB_R1,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLACK),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(157, 166, 176), RGB(138, 156, 184), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(138, 156, 184), RGB(157, 166, 176), &m_sImageElement),
			AFX_IDW_DOCKBAR_BOTTOM,
			&m_sImageElement
			);
		m_SbSkinDataL_Zoom.Load2010_Zoom_R1(
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_PLUS_R1,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_MINUS_R1,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_THUMB_R1,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLACK),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(157, 166, 176), RGB(138, 156, 184), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(138, 156, 184), RGB(157, 166, 176), &m_sImageElement),
			AFX_IDW_DOCKBAR_LEFT,
			&m_sImageElement
			);
		m_SbSkinDataR_Zoom.Load2010_Zoom_R1(
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_PLUS_R1,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_MINUS_R1,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_THUMB_R1,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLACK),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(222, 234, 247), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(157, 166, 176), RGB(138, 156, 184), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(138, 156, 184), RGB(157, 166, 176), &m_sImageElement),
			AFX_IDW_DOCKBAR_RIGHT,
			&m_sImageElement
			);
	}
	else
	{
		m_SbSkinDataT_Zoom.Load2010_Zoom_R2(
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_PLUS_R1,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_MINUS_R1,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_THUMB_R1,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLACK),	// Map Image 19 - 21
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(157, 166, 176), RGB(138, 156, 184), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(157, 166, 176), RGB(138, 156, 184), &m_sImageElement),
			AFX_IDW_DOCKBAR_TOP,
			&m_sImageElement
			);
		m_SbSkinDataB_Zoom.Load2010_Zoom_R2(
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_PLUS_R1,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_MINUS_R1,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_THUMB_R1,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLACK),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(157, 166, 176), RGB(138, 156, 184), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(138, 156, 184), RGB(157, 166, 176), &m_sImageElement),
			AFX_IDW_DOCKBAR_BOTTOM,
			&m_sImageElement
			);
		m_SbSkinDataL_Zoom.Load2010_Zoom_R2(
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_PLUS_R1,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_MINUS_R1,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_THUMB_R1,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLACK),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(157, 166, 176), RGB(138, 156, 184), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(138, 156, 184), RGB(157, 166, 176), &m_sImageElement),
			AFX_IDW_DOCKBAR_LEFT,
			&m_sImageElement
			);
		m_SbSkinDataR_Zoom.Load2010_Zoom_R2(
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_PLUS_R1,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_PLUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_MINUS_R1,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_MINUS_R2_BLACK),
			ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_ZS_THUMB_R1,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLUE,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_SILVER,
			IDB_EXT_2010_OFFICE_ZS_THUMB_R2_BLACK),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(222, 234, 247), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(157, 166, 176), RGB(138, 156, 184), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(204, 204, 204), RGB(222, 234, 247), RGB(255, 255, 255), RGB(222, 234, 247), &m_sImageElement),
			RGBIndexAuto(nIndex, RGB(37, 37, 37), RGB(138, 156, 184), RGB(138, 156, 184), RGB(157, 166, 176), &m_sImageElement),
			AFX_IDW_DOCKBAR_RIGHT,
			&m_sImageElement
			);
	}

#if _MFC_VER < 0xC00 

	static const int pIndex[] = { 0, 4, 5, 6, 26, 27, 28, 29, 30, 31 };
	static const std::vector<int> vIndex(pIndex, pIndex + _countof(pIndex));

#else // _MFC_VER < 0xC00  

	static const std::vector<int> vIndex = { 0, 4, 5, 6, 26, 27, 28, 29, 30, 31 };

#endif // _MFC_VER < 0xC00

	int nValue = _Index(vIndex, nIndex) ? 15 : 16;

	m_SbSkinDataH_Light.Load2007_R1(
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R1_HORZ_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R2_BLUE_HORZ_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R2_SILVER_HORZ_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R2_BLACK_HORZ_LIGHT),
		// Map Image 22
		nValue,
		//15 or 16,
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R1_HORZ,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R2_LIGHT_HORZ,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R2_LIGHT_HORZ,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R2_LIGHT_HORZ),
		SbSkinDataPaddingH,
		COLORREF(-1L),
		COLORREF(-1L),
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_NORMAL_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_NORMAL_R2_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_NORMAL_R2_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_NORMAL_R2_LIGHT
		),
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_DISABLED_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_DISABLED_R2_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_DISABLED_R2_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_DISABLED_R2_LIGHT
		),
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_NORMAL_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_NORMAL_R2_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_NORMAL_R2_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_NORMAL_R2_LIGHT
		),
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_DISABLED_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_DISABLED_R2_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_DISABLED_R2_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_DISABLED_R2_LIGHT
		),
		true,
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ENTIRE_BK_R1_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ENTIRE_BK_R2_BLUE_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ENTIRE_BK_R2_SILVER_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ENTIRE_BK_R2_BLACK_LIGHT),
		// Map Image 24
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_PAGE_BK_R1_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_PAGE_BK_R2_BLUE_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_PAGE_BK_R2_SILVER_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_PAGE_BK_R2_BLACK_LIGHT),
		// Map Image 25
		rcEntirePaddingRH,
		rcEntirePaddingCH,
		&m_sImageElement
		);

	m_SbSkinDataV_Light.Load2007_R1(
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R1_VERT_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R2_BLUE_VERT_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R2_SILVER_VERT_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R2_BLACK_VERT_LIGHT),
		17,
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R1_VERT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R2_LIGHT_VERT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R2_LIGHT_VERT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R2_LIGHT_VERT),
		SbSkinDataPaddingV,
		COLORREF(-1L),
		COLORREF(-1L),
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_NORMAL_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_DISABLED_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_NORMAL_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_DISABLED_R1,
		false,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ENTIRE_BK_R1_LIGHT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_PAGE_BK_R1_LIGHT,
		rcEntirePaddingRV,
		rcEntirePaddingCV
		);

	m_SbSkinDataH_Dark.Load2007_R1(
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R1_HORZ_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R2_BLUE_HORZ_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R2_SILVER_HORZ_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R2_BLACK_HORZ_DARK),
		// Map Image 29
		nValue,
		//16,
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R1_HORZ,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R2_BLUE_HORZ,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R2_SILVER_HORZ,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R2_BLACK_HORZ),
		// Map Image 30
		SbSkinDataPaddingH,
		COLORREF(-1L),
		COLORREF(-1L),
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_NORMAL_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_NORMAL_R2_BLUE,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_NORMAL_R2_SILVER,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_NORMAL_R2_BLACK),
		// Map Image 31
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_DISABLED_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_DISABLED_R2_BLUE,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_DISABLED_R2_SILVER,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_DISABLED_R2_BLACK),
		// Map Image 32
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_NORMAL_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_NORMAL_R2_BLUE,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_NORMAL_R2_SILVER,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_NORMAL_R2_BLACK),
		// Map Image 33
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_DISABLED_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_DISABLED_R2_BLUE,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_DISABLED_R2_SILVER,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_DISABLED_R2_BLACK),
		// Map Image 34
		true,
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ENTIRE_BK_R1_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ENTIRE_BK_R2_BLUE_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ENTIRE_BK_R2_SILVER_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ENTIRE_BK_R2_BLACK_DARK),
		// Map Image 35
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_PAGE_BK_R1_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_PAGE_BK_R2_BLUE_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_PAGE_BK_R2_SILVER_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_PAGE_BK_R2_BLACK_DARK),
		// Map Image 36
		rcEntirePaddingRH,
		rcEntirePaddingCH
		);

	m_SbSkinDataV_Dark.Load2007_R1(
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R1_VERT_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R2_BLUE_VERT_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R2_SILVER_VERT_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_STACK_BUTTONS_R2_BLACK_VERT_DARK),

		// Map Image 37
		17,
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R1_VERT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R2_BLUE_VERT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R2_SILVER_VERT,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_GRIPPER_R2_BLACK_VERT),

		// Map Image 38
		SbSkinDataPaddingV,
		COLORREF(-1L),
		COLORREF(-1L),
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_NORMAL_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_NORMAL_R2_BLUE,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_NORMAL_R2_SILVER,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_NORMAL_R2_BLACK),
		// Map Image 39
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_DISABLED_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_DISABLED_R2_BLUE,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_DISABLED_R2_SILVER,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_UP_DISABLED_R2_BLACK),
		// Map Image 40
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_NORMAL_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_NORMAL_R2_BLUE,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_NORMAL_R2_SILVER,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_NORMAL_R2_BLACK),
		// Map Image 41
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_DISABLED_R1,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_DISABLED_R2_BLUE,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_DISABLED_R2_SILVER,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ARROW_DOWN_DISABLED_R2_BLACK),
		// Map Image 42
		false,
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_ENTIRE_BK_R1_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ENTIRE_BK_R2_BLUE_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ENTIRE_BK_R2_SILVER_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_ENTIRE_BK_R2_BLACK_DARK),
		// Map Image 43
		ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_SCROLL_BAR_PAGE_BK_R1_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_PAGE_BK_R2_BLUE_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_PAGE_BK_R2_SILVER_DARK,
		IDB_EXT_2010_OFFICE_SCROLL_BAR_PAGE_BK_R2_BLACK_DARK),

		// Map Image 44
		rcEntirePaddingRV,
		rcEntirePaddingCV
		);

	m_clrGridHeaderBkTop = RGBIMapper(RGB(253, 253, 253), &m_sImageElement);
	m_clrGridHeaderBkBottom = RGBIMapper(RGB(212, 215, 220), &m_sImageElement);
	m_clrGridHeaderSeparator = RGBIMapper(RGB(145, 153, 164), &m_sImageElement);
	m_clrGridHeaderSortArrow = RGBIMapper(RGB(145, 153, 164), &m_sImageElement);
	m_clrGridHeaderBorder = RGBIMapper(RGB(145, 153, 164), &m_sImageElement);

	m_clrRgHeaderBkTop = RGBIMapper(RGB(253, 253, 253), &m_sImageElement);
	m_clrRgHeaderBkBottom = RGBIMapper(RGB(212, 215, 220), &m_sImageElement);
	m_clrRgHeaderSeparator = RGBIMapper(RGB(145, 153, 164), &m_sImageElement);
	m_clrRgHeaderSortArrow = RGBIMapper(RGB(145, 153, 164), &m_sImageElement);
	m_clrRgHeaderText = RGB(0, 0, 0);
	m_clrRgHeaderBorder = RGBIMapper(RGB(76, 83, 92), &m_sImageElement);

#if (!defined __EXT_MFC_NO_REPORTGRIDWND)
	m_clrRgGroupAreaBk = RGBIMapper(RGB(235, 235, 235), &m_sImageElement);
	m_clrRgGroupAreaText = RGBIMapper(RGB(70, 70, 70), &m_sImageElement);
#endif // (!defined __EXT_MFC_NO_REPORTGRIDWND)

	m_nPushBtnSimpleGlyphHeightH = 43;
	m_nPushBtnSimpleGlyphHeightV = 43;
	m_nPushBtnSDDLeftGlyphHeightH = 43;
	m_nPushBtnSDDLeftGlyphHeightV = 43;
	m_nPushBtnSDDRightGlyphHeightH = 43;
	m_nPushBtnSDDRightGlyphHeightV = 9;
	m_rcPushBtnSimplePaddingH.SetRect(3, 3, 3, 3);
	m_rcPushBtnSimplePaddingV.SetRect(3, 3, 3, 3);
	m_rcPushBtnSDDLeftPaddingH.SetRect(3, 3, 3, 3);
	m_rcPushBtnSDDLeftPaddingV.SetRect(3, 3, 3, 3);
	m_rcPushBtnSDDRightPaddingH.SetRect(3, 3, 3, 3);
	m_rcPushBtnSDDRightPaddingV.SetRect(3, 3, 3, 3);
	VERIFY(
		m_arrBmpPushBtnSimpleH.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_BUTTON_R1_SIMPLE)
		)
		);
	VERIFY(
		m_arrBmpPushBtnSDDLeftH.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_BUTTON_R1_LEFT_PART)
		)
		);
	VERIFY(
		m_arrBmpPushBtnSDDRightH.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_BUTTON_R1_RIGHT_PART)
		)
		);
	VERIFY(
		m_arrBmpPushBtnSimpleV.CreateRotated9xStack(
		m_arrBmpPushBtnSimpleH,
		90,
		INT(__EPBGT_GLYPH_COUNT),
		m_arrBmpPushBtnSimpleH.GetSize().cx,
		m_nPushBtnSimpleGlyphHeightH,
		false,
		false
		)
		);
	VERIFY(
		m_arrBmpPushBtnSDDLeftV.CreateRotated9xStack(
		m_arrBmpPushBtnSDDLeftH,
		90,
		INT(__EPBGT_GLYPH_COUNT),
		m_arrBmpPushBtnSDDLeftH.GetSize().cx,
		m_nPushBtnSDDLeftGlyphHeightH,
		false,
		false
		)
		);
	VERIFY(
		m_arrBmpPushBtnSDDRightV.CreateRotated9xStack(
		m_arrBmpPushBtnSDDRightH,
		90,
		INT(__EPBGT_GLYPH_COUNT),
		m_arrBmpPushBtnSDDRightH.GetSize().cx,
		m_nPushBtnSDDRightGlyphHeightH,
		false,
		false
		)
		);
	m_arrBmpPushBtnSimpleV.PreMultipliedRGBChannelsSet(true);
	m_arrBmpPushBtnSDDLeftV.PreMultipliedRGBChannelsSet(true);
	m_arrBmpPushBtnSDDRightV.PreMultipliedRGBChannelsSet(true);

#if (!defined __EXT_MFC_NO_RIBBON_BAR)

	if (g_PaintManager.m_bIsWinVistaOrLater)
	{
		VERIFY(m_bmpRibbonTabAreaBlur.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_RIBBON_TAB_AREA_DWM_BLUR,
			IDB_EXT_2010_OFFICE_RIBBON_TAB_AREA_DWM_BLUR,
			IDB_EXT_2010_OFFICE_RIBBON_TAB_AREA_DWM_BLUR,
			IDB_EXT_2010_OFFICE_RIBBON_TAB_AREA_DWM_BLUR_R2_BLACK)), &m_sImageElement));
		m_rcRibbonTabAreaBlurPadding.SetRect(35, 0, 35, 36);
	}
	// this is the background area
	m_clrRibbonTabClientAreaActive = RGBIndexAuto(nIndex, RGB(227, 230, 232), RGB(192, 211, 235), RGB(233, 235, 238), RGB(117, 117, 117), &m_sImageElement);
	m_clrRibbonTabClientAreaInactive = RGBIndexAuto(nIndex, RGB(252, 252, 252), RGB(223, 235, 247), RGB(252, 252, 252), RGB(158, 158, 158), &m_sImageElement);
	m_clrContractedMargin1 = RGBIMapper(RGB(128, 128, 128), &m_sImageElement);
	m_clrContractedMargin2 = RGBIMapper(RGB(128, 128, 128), &m_sImageElement);

	m_bRibbonGroupCaptionAtTop = false;
	m_bRibbonGroupCaptionAreaHasPadding = true;
	m_nRibbonGroupCaptionAreaHeightExpanded = 16;
	m_nRibbonGroupCaptionAreaHeightCollapsed = 16;
	m_nRibbonGroupIconBkVertOffset = 2;
	m_nRibbonCaptionPartHeight = 17;
	m_rcRibbonPageOuterContentPadding.SetRect(0, 0, 0, 0);
	m_clrRibbonComboBackGroundNormal = RGB(255, 255, 255);
	m_clrRibbonComboBackGroundDisabled = RGB(255, 255, 255);

	m_nRibbonTabIntersectionHeight = 1;

	m_clrRibbonGroupCaptionTextCollapsed
		= m_clrRibbonButtonNormal
		= RGBIMapper(RGB(57, 60, 63), &m_sImageElement);
	m_clrRibbonButtonDisabled = RGBIMapper(RGB(128, 128, 128), &m_sImageElement);
	m_clrRibbonGroupCaptionTextExpanded = RGBIMapper(RGB(52, 52, 64), &m_sImageElement);
	m_nRibbonGroupCaptionTextDF
		= m_nRibbonGroupCaptionTextShadowDF
		= DT_SINGLELINE | DT_CENTER | DT_VCENTER | DT_END_ELLIPSIS;

	m_rcRibbonPageBkPadding.SetRect(1, 1, 1, 1);
	VERIFY(
		m_bmpRibbonPageBk.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_PAGE_BK_R1,
		IDB_EXT_2010_OFFICE_RIBBON_PAGE_BK_R2_BLUE,
		IDB_EXT_2010_OFFICE_RIBBON_PAGE_BK_R2_SILVER,
		IDB_EXT_2010_OFFICE_RIBBON_PAGE_BK_R2_BLACK)), &m_sImageElement));
	m_rcRibbonGroupPaddingBkExpanded.SetRect(2, 6, 4, 2);
	// panel normal background
	VERIFY(
		m_bmpRibbonGroupBkExpanded[0].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_EXPANDED_NORMAL_BK_R1,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_EXPANDED_NORMAL_BK_R2_BLUE,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_EXPANDED_NORMAL_BK_R2_SILVER,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_EXPANDED_NORMAL_BK_R2_BLACK)), &m_sImageElement));
	// Map Image 46
	m_bmpRibbonGroupBkExpanded[0].PreMultipliedRGBChannelsSet(false);
	// panel hover background
	VERIFY(
		m_bmpRibbonGroupBkExpanded[1].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_EXPANDED_HOVER_BK_R1,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_EXPANDED_HOVER_BK_R2_BLUE,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_EXPANDED_HOVER_BK_R2_SILVER,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_EXPANDED_HOVER_BK_R2_BLACK)), &m_sImageElement)); 
	// Map Image 47
	m_bmpRibbonGroupBkExpanded[1].PreMultipliedRGBChannelsSet(false);
	m_rcRibbonGroupPaddingBkCollapsed.SetRect(2, 6, 4, 2);
	VERIFY(
		m_bmpRibbonGroupBkCollapsed[0].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_NORMAL_BK_R1,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_NORMAL_BK_R2_BLUE,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_NORMAL_BK_R2_SILVER,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_NORMAL_BK_R2_BLACK)), &m_sImageElement));
	// Map Image 48
	m_bmpRibbonGroupBkCollapsed[0].PreMultipliedRGBChannelsSet(false);
	VERIFY(
		m_bmpRibbonGroupBkCollapsed[1].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_HOVER_BK_R1,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_HOVER_BK_R2_BLUE,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_HOVER_BK_R2_SILVER,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_HOVER_BK_R2_BLACK)), &m_sImageElement));
	// Map Image 49
	m_bmpRibbonGroupBkCollapsed[1].PreMultipliedRGBChannelsSet(false);
	VERIFY(
		m_bmpRibbonGroupBkCollapsed[2].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_PRESSED_BK_R1,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_PRESSED_BK_R2_BLUE,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_PRESSED_BK_R2_SILVER,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_PRESSED_BK_R2_BLACK)), &m_sImageElement));
	// Map Image 50
	m_bmpRibbonGroupBkCollapsed[2].PreMultipliedRGBChannelsSet(false);
	VERIFY(
		// selected same as hover in R1
		m_bmpRibbonGroupBkCollapsed[3].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_HOVER_BK_R1,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_HOVER_BK_R2_BLUE,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_HOVER_BK_R2_SILVER,
		IDB_EXT_2010_OFFICE_RIBBON_GROUP_COLLAPSED_HOVER_BK_R2_BLACK)), &m_sImageElement));
	// Map Image 51
	m_bmpRibbonGroupBkCollapsed[3].PreMultipliedRGBChannelsSet(false);

	if ( IndexTab )
	{
		VERIFY(m_bmpRibbonGroupBkQATB[0].LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_NORMAL_BK_R1)));
		m_bmpRibbonGroupBkQATB[0].PreMultiplyRGBChannels(false);
		VERIFY(m_bmpRibbonGroupBkQATB[1].LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_HOVER_BK_R1)));
		m_bmpRibbonGroupBkQATB[1].PreMultiplyRGBChannels(false);
		VERIFY(m_bmpRibbonGroupBkQATB[2].LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_PRESSED_BK_R1)));
		m_bmpRibbonGroupBkQATB[2].PreMultiplyRGBChannels(false);
		m_bmpRibbonGroupBkQATB[3] = m_bmpRibbonGroupBkQATB[1];
	}
	else
	{
		// Luna Blue
		VERIFY(m_bmpRibbonGroupBkQATB[0].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_NORMAL_BK_R1,
			IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_NORMAL_BK_R2_BLUE,
			IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_NORMAL_BK_R2_SILVER,
			IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_NORMAL_BK_R2_BLACK)), &m_sImageElement));
		// Map Image 52
		VERIFY(m_bmpRibbonGroupBkQATB[0].Make32());
		m_bmpRibbonGroupBkQATB[0].AlphaColor(RGB(255, 0, 255),
											 RGB(0, 0, 0), BYTE(0));
		VERIFY(m_bmpRibbonGroupBkQATB[1].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_HOVER_BK_R1,
			IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_HOVER_BK_R2_BLUE,
			IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_HOVER_BK_R2_SILVER,
			IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_HOVER_BK_R2_BLACK)), &m_sImageElement));
		// Map Image 53
		VERIFY(m_bmpRibbonGroupBkQATB[1].Make32());
		m_bmpRibbonGroupBkQATB[1].AlphaColor(RGB(255, 0, 255),
											 RGB(0, 0, 0), BYTE(0));
		VERIFY(m_bmpRibbonGroupBkQATB[2].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
			IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_PRESSED_BK_R1,
			IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_PRESSED_BK_R2_BLUE,
			IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_PRESSED_BK_R2_SILVER,
			IDB_EXT_2010_OFFICE_RIBBON_GROUP_BUTTON_QATB_PRESSED_BK_R2_BLACK)), &m_sImageElement));
		// Map Image 54
		VERIFY(m_bmpRibbonGroupBkQATB[2].Make32());
		m_bmpRibbonGroupBkQATB[2].AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), BYTE(0));
		m_bmpRibbonGroupBkQATB[3] = m_bmpRibbonGroupBkQATB[1];
	}
	m_nRibbonSeparatorDrawModeH = INT(CExtBitmap::__EDM_STRETCH);
	m_nRibbonSeparatorDrawModeV = INT(CExtBitmap::__EDM_STRETCH);
	m_rcRibbonSeparatorPaddingH.SetRect(0, 0, 0, 0);
	VERIFY(
		m_bmpRibbonSeparatorH.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_SEPARATOR_H_R1,
		IDB_EXT_2010_OFFICE_RIBBON_SEPARATOR_H_R2_BLUE,
		IDB_EXT_2010_OFFICE_RIBBON_SEPARATOR_H_R2_SILVER,
		IDB_EXT_2010_OFFICE_RIBBON_SEPARATOR_H_R2_BLACK)), &m_sImageElement));		// Map Image 55

	VERIFY(m_bmpRibbonSeparatorH.Make32());
	m_bmpRibbonSeparatorH.AlphaColor(RGB(255, 0, 255),
		RGB(0, 0, 0), 0);
	m_rcRibbonSeparatorPaddingV.SetRect(0, 0, 0, 0);
	VERIFY(
		m_bmpRibbonSeparatorV.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_SEPARATOR_V_R1,
		IDB_EXT_2010_OFFICE_RIBBON_SEPARATOR_V_R2_BLUE,
		IDB_EXT_2010_OFFICE_RIBBON_SEPARATOR_V_R2_SILVER,
		IDB_EXT_2010_OFFICE_RIBBON_SEPARATOR_V_R2_BLACK)), &m_sImageElement));		// Map Image 56

	VERIFY(m_bmpRibbonSeparatorV.Make32());
	m_bmpRibbonSeparatorV.AlphaColor(RGB(255, 0, 255),
		RGB(0, 0, 0), 0);

	m_rcRibbonPaddingQACEB.SetRect(3, 3, 3, 3);
	m_nRibbonHeightOfQACEB = 22;
	m_nIdxRibbonQACEB_Normal = 3;
	m_nIdxRibbonQACEB_Hover = 0;
	m_nIdxRibbonQACEB_Pressed = 1;
	m_nIdxRibbonQACEB_Selected = 2;
	VERIFY(
		m_bmpArrRibbonQACEB.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_QATB_CEB_R1,
		IDB_EXT_2010_OFFICE_QATB_CEB_R2_BLUE,
		IDB_EXT_2010_OFFICE_QATB_CEB_R2_SILVER,
		IDB_EXT_2010_OFFICE_QATB_CEB_R2_BLACK)), &m_sImageElement));		// Map Image 57

	m_bmpArrRibbonQACEB.PreMultiplyRGBChannels(false);
	if ( !_IndexTab(nIndex) )
	{
		//m_bmpArrRibbonQACEB.PreMultipliedRGBChannelsSet(true);
	}

	m_nRibbonEmbeddedCaptionTextAlignmentFlags = (DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	m_nQatbAdditionalSpaceAtRightDWM = 0;
	m_rcRibbonNcCornerLeftCP.SetRect(53, 2, 2, 2);
	m_rcRibbonNcCornerRightCP.SetRect(2, 2, 42, 2);
	// image checked
	VERIFY(m_bmpArrRibbonFileButton.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_RIBBON_FILE_BUTTON)));
	m_bmpArrRibbonFileButton.PreMultiplyRGBChannels(true);
	m_nRibbonFileButtonHeight = m_bmpArrRibbonFileButton.GetSize().cy / 3;

	m_rcPaddingRibbonQuickAccessBarBkAtTheBottom.SetRect(2, 2, 2, 2);
	VERIFY(m_bmpRibbonQuickAccessBarBkAtTheBottom.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_QATB_BK_R1,
		IDB_EXT_2010_OFFICE_QATB_BK_R2_BLUE,
		IDB_EXT_2010_OFFICE_QATB_BK_R2_SILVER,
		IDB_EXT_2010_OFFICE_QATB_BK_R2_BLACK)), &m_sImageElement));
	// Map Image 58
	m_nRibbonHeightOfOneDLB = 14;
	m_nDlbIdxDisabled = m_nDlbIdxNormal = 0;
	m_nDlbIdxHover = 1;
	m_nDlbIdxPressed = 2;
	rcRibbonContentPaddingDLB.SetRect(2, 2, 2, 2);
	VERIFY(m_arrBmpRibbonDLB.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_GCB_R1,
		IDB_EXT_2010_OFFICE_GCB_R2_BLUE,
		IDB_EXT_2010_OFFICE_GCB_R2_SILVER,
		IDB_EXT_2010_OFFICE_GCB_R2_BLACK)), &m_sImageElement));
	// Map Image 59
	m_arrBmpRibbonDLB.PreMultiplyRGBChannels(false);

	m_clrRibbonGalleryInplaceBorderNormal = m_clrRibbonGalleryInplaceBorderHover = RGBIMapper(RGB(198, 202, 205), &m_sImageElement);
	m_clrRibbonGalleryInplaceBkgndNormal = m_clrRibbonGalleryInplaceBkgndHover = RGBIMapper(RGB(255, 255, 255), &m_sImageElement);

	m_rcRibbonGalleryScrollFill = RGB(255, 255, 255);
	m_rcRibbonGalleryScrollButtonPadding.SetRect(3, 3, 3, 3);
	m_nHeightRibbonGalleryScrollButton = -1; //20;
	m_nIndexRibbonGalleryScrollButtonNormal = 0;
	m_nIndexRibbonGalleryScrollButtonHover = 1;
	m_nIndexRibbonGalleryScrollButtonPressed = 2;
	m_nIndexRibbonGalleryScrollButtonDisabled = 3;
	VERIFY(
		m_bmpArrRibbonGalleryScrollButtonDown.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RGIBTN_DOWN_R1,
		IDB_EXT_2010_OFFICE_RGIBTN_DOWN_R2_BLUE,
		IDB_EXT_2010_OFFICE_RGIBTN_DOWN_R2_SILVER,
		IDB_EXT_2010_OFFICE_RGIBTN_DOWN_R2_BLACK)), &m_sImageElement));
	// Map Image 60
	m_bmpArrRibbonGalleryScrollButtonDown.PreMultipliedRGBChannelsSet(true);
	VERIFY(
		m_bmpArrRibbonGalleryScrollButtonUp.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RGIBTN_UP_R1,
		IDB_EXT_2010_OFFICE_RGIBTN_UP_R2_BLUE,
		IDB_EXT_2010_OFFICE_RGIBTN_UP_R2_SILVER,
		IDB_EXT_2010_OFFICE_RGIBTN_UP_R2_BLACK)), &m_sImageElement));
	// Map Image 61

	m_bmpArrRibbonGalleryScrollButtonUp.PreMultipliedRGBChannelsSet(true);
	VERIFY(
		m_bmpArrRibbonGalleryScrollButtonMenu.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RGIBTN_MENU_R1,
		IDB_EXT_2010_OFFICE_RGIBTN_MENU_R2_BLUE,
		IDB_EXT_2010_OFFICE_RGIBTN_MENU_R2_SILVER,
		IDB_EXT_2010_OFFICE_RGIBTN_MENU_R2_BLACK)), &m_sImageElement));
	// Map Image 61

	m_bmpArrRibbonGalleryScrollButtonMenu.PreMultipliedRGBChannelsSet(true);
#endif // (!defined __EXT_MFC_NO_RIBBON_BAR)
	// image checked
	VERIFY(m_bmpRibbonDDA.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_RIBBON_DDA_R1)));
	VERIFY(m_bmpRibbonDDA.Make32());
	m_bmpRibbonDDA.AlphaColor(RGB(255, 0, 255),
		RGB(0, 0, 0), 0);

	m_rcRibbonPaddingComboBoxDropDown.SetRect(2, 2, 2, 2);
	// image checked
	VERIFY(m_bmpArrRibbonComboBoxDropDown.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_2010_OFFICE_RIBBON_COMBO_BOX_BUTTON_R1)));
	m_bmpArrRibbonComboBoxDropDown.PreMultipliedRGBChannelsSet(true);

	// buttons 2010
	m_nHeightOfButtonLSM = 66;
	m_nHeightOfButtonLSXM = 39;
	m_nHeightOfButtonLSXB = 27;

	// tool buttons 2010
	m_rcPaddingOfButtonTS.SetRect(3, 3, 3, 3);
	m_arrIndicesButtonTS[INT(__E07BI_NORMAL)] = -1;
	m_arrIndicesButtonTS[INT(__E07BI_HOVER)] = 0;
	m_arrIndicesButtonTS[INT(__E07BI_PRESSED)] = 1;
	m_arrIndicesButtonTS[INT(__E07BI_HDD)] = 2;
	m_arrIndicesButtonTS[INT(__E07BI_SEL_T)] = 3;
	m_arrIndicesButtonTS[INT(__E07BI_INDETERMINATE)] = 4;

	m_rcPaddingOfButtonSS.SetRect(3, 3, 3, 3);
	m_arrIndicesButtonSS_Left[INT(__E07BI_NORMAL)] = -1;
	m_arrIndicesButtonSS_Left[INT(__E07BI_HOVER)] = 0;
	m_arrIndicesButtonSS_Left[INT(__E07BI_PRESSED)] = 1;
	m_arrIndicesButtonSS_Left[INT(__E07BI_HDD)] = 2;
	m_arrIndicesButtonSS_Left[INT(__E07BI_SEL_T)] = 3;
	m_arrIndicesButtonSS_Left[INT(__E07BI_INDETERMINATE)] = 4;
	m_arrIndicesButtonSS_Right[INT(__E07BI_NORMAL)] = -1;
	m_arrIndicesButtonSS_Right[INT(__E07BI_HOVER)] = 0;
	m_arrIndicesButtonSS_Right[INT(__E07BI_PRESSED)] = 1;
	m_arrIndicesButtonSS_Right[INT(__E07BI_HDD)] = 2;
	m_arrIndicesButtonSS_Right[INT(__E07BI_SEL_T)] = 3;
	m_arrIndicesButtonSS_Right[INT(__E07BI_INDETERMINATE)] = 4;

	m_rcPaddingOfButtonLSM.SetRect(4, 4, 4, 4);
	m_arrIndicesButtonLSM[INT(__E07BI_NORMAL)] = -1;
	m_arrIndicesButtonLSM[INT(__E07BI_HOVER)] = 0;
	m_arrIndicesButtonLSM[INT(__E07BI_PRESSED)] = 1;
	m_arrIndicesButtonLSM[INT(__E07BI_HDD)] = 2;
	m_arrIndicesButtonLSM[INT(__E07BI_SEL_T)] = 3;
	m_arrIndicesButtonLSM[INT(__E07BIX_INDETERMINATE)] = 4;

	m_rcPaddingOfButtonLSXM.SetRect(4, 4, 4, 4);
	m_arrIndicesButtonLSXM[INT(__E07BIX_NORMAL)] = -1;
	m_arrIndicesButtonLSXM[INT(__E07BIX_HOVER)] = 0;
	m_arrIndicesButtonLSXM[INT(__E07BIX_HOVER2)] = 0;
	m_arrIndicesButtonLSXM[INT(__E07BIX_PRESSED)] = 1;
	m_arrIndicesButtonLSXM[INT(__E07BIX_PRESSED2)] = 1;
	m_arrIndicesButtonLSXM[INT(__E07BIX_PRESSED3)] = 1;
	m_arrIndicesButtonLSXM[INT(__E07BIX_INDETERMINATE)] = 5;
	m_arrIndicesButtonLSXM[INT(__E07BIX_SELECTED)] = 3;

	m_rcPaddingOfButtonLSXB.SetRect(4, 4, 4, 4);
	m_arrIndicesButtonLSXB[INT(__E07BIX_NORMAL)] = -1;
	m_arrIndicesButtonLSXB[INT(__E07BIX_HOVER)] = 0;
	m_arrIndicesButtonLSXB[INT(__E07BIX_HOVER2)] = 0;
	m_arrIndicesButtonLSXB[INT(__E07BIX_PRESSED)] = 1;
	m_arrIndicesButtonLSXB[INT(__E07BIX_PRESSED2)] = 1;
	m_arrIndicesButtonLSXB[INT(__E07BIX_PRESSED3)] = 1;
	m_arrIndicesButtonLSXB[INT(__E07BIX_INDETERMINATE)] = 5;
	m_arrIndicesButtonLSXB[INT(__E07BIX_SELECTED)] = 3;

	VERIFY(
		m_bmpArrButtonTS[INT(__EBTSA_SOLID)].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_TB_SOLID_R1,
		IDB_EXT_2007_RIBBON_TB_SOLID_R2_LUNA_BLUE,
		IDB_EXT_2007_RIBBON_TB_SOLID_R2_OBSIDIAN,
		IDB_EXT_2007_RIBBON_TB_SOLID_R2_OBSIDIAN)), &m_sImageElement));

	m_bmpArrButtonTS[INT(__EBTSA_SOLID)].PreMultipliedRGBChannelsSet(true);
	VERIFY(
		m_bmpArrButtonTS[INT(__EBTSA_LEFT)].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_TB_SOLID_R1,
		IDB_EXT_2007_RIBBON_TB_LEFT_R2_LUNA_BLUE,
		IDB_EXT_2007_RIBBON_TB_LEFT_R2_OBSIDIAN,
		IDB_EXT_2007_RIBBON_TB_LEFT_R2_OBSIDIAN)), &m_sImageElement));

	m_bmpArrButtonTS[INT(__EBTSA_LEFT)].PreMultipliedRGBChannelsSet(true);
	VERIFY(
		m_bmpArrButtonTS[INT(__EBTSA_MIDDLE)].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_TB_SOLID_R1,
		IDB_EXT_2007_RIBBON_TB_MIDDLE_R2_LUNA_BLUE,
		IDB_EXT_2007_RIBBON_TB_MIDDLE_R2_OBSIDIAN,
		IDB_EXT_2007_RIBBON_TB_MIDDLE_R2_OBSIDIAN)), &m_sImageElement));

	m_bmpArrButtonTS[INT(__EBTSA_MIDDLE)].PreMultipliedRGBChannelsSet(true);
	VERIFY(
		m_bmpArrButtonTS[INT(__EBTSA_RIGHT)].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_TB_SOLID_R1,
		IDB_EXT_2007_RIBBON_TB_RIGHT_R2_LUNA_BLUE,
		IDB_EXT_2007_RIBBON_TB_RIGHT_R2_OBSIDIAN,
		IDB_EXT_2007_RIBBON_TB_RIGHT_R2_OBSIDIAN)), &m_sImageElement));

	m_bmpArrButtonTS[INT(__EBTSA_RIGHT)].PreMultipliedRGBChannelsSet(true);

	VERIFY(
		m_bmpArrButtonSS_DD[INT(__EBSSA_LEFT)].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_BTN_PART_SMALL_LEFT_R1,
		IDB_EXT_2007_RIBBON_BTN_PART_SMALL_LEFT_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_SMALL_LEFT_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_SMALL_LEFT_R2)), &m_sImageElement));

	m_bmpArrButtonSS_DD[INT(__EBSSA_LEFT)].PreMultipliedRGBChannelsSet(true);
	VERIFY(
		m_bmpArrButtonSS_DD[INT(__EBSSA_RIGHT)].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_BTN_PART_SMALL_RIGHT_R1,
		IDB_EXT_2007_RIBBON_BTN_PART_SMALL_RIGHT_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_SMALL_RIGHT_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_SMALL_RIGHT_R2)), &m_sImageElement));

	m_bmpArrButtonSS_DD[INT(__EBSSA_RIGHT)].PreMultipliedRGBChannelsSet(true);
	m_bmpArrButtonSS_simple = m_bmpArrButtonSS_DD[INT(__EBSSA_LEFT)];
	if (!m_bmpArrButtonSS_simple.IsEmpty())
	{
		CSize _sizeBmp = m_bmpArrButtonSS_simple.GetSize();
		INT nY = 0, nXRange = _sizeBmp.cx / 2;
		for (; nY < _sizeBmp.cy; nY++)
		{
			INT nXSrc = nXRange - 1, nXDst = _sizeBmp.cx - nXRange;
			for (; nXDst < _sizeBmp.cx; nXDst++, nXSrc--)
			{
				RGBQUAD _pixel;
				VERIFY(m_bmpArrButtonSS_simple.GetPixel(nXSrc, nY, _pixel));
				VERIFY(m_bmpArrButtonSS_simple.SetPixel(nXDst, nY, _pixel));
			} // for( ; nXDst < _sizeBmp.cx; nXDst ++, nXSrc -- )
		} // for( ; nY < _sizeBmp.cy; nY ++ )
	} // if( m_bmpArrButtonSS_simple.IsEmpty() )

	VERIFY(
		m_bmpArrButtonLSM.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_BTN_PART_LARGE_ALL_R1,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_ALL_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_ALL_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_ALL_R2)), &m_sImageElement));

	m_bmpArrButtonLSM.PreMultipliedRGBChannelsSet(true);
	VERIFY(
		m_bmpArrButtonLSXM.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_BTN_PART_LARGE_TOP_R1,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_TOP_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_TOP_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_TOP_R2)), &m_sImageElement));

	m_bmpArrButtonLSXM.PreMultipliedRGBChannelsSet(true);
	VERIFY(
		m_bmpArrButtonLSXB.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RIBBON_BTN_PART_LARGE_BOTTOM_R1,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_BOTTOM_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_BOTTOM_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_BOTTOM_R2)), &m_sImageElement));

	m_bmpArrButtonLSXB.PreMultipliedRGBChannelsSet(true);

	VERIFY(
		m_bmpToolBtnSeparator[INT(__ETBS_HOVER)].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_RIBBON_TB_SEPARATOR_HOVER_R1,
		IDB_EXT_2010_OFFICE_RIBBON_TB_SEPARATOR_HOVER_R2_BLUE,
		IDB_EXT_2010_OFFICE_RIBBON_TB_SEPARATOR_HOVER_R2_SILVER,
		IDB_EXT_2010_OFFICE_RIBBON_TB_SEPARATOR_HOVER_R2_BLACK)), &m_sImageElement));
	// Map Image 62

	m_bmpToolBtnSeparator[INT(__ETBS_HOVER)].PreMultipliedRGBChannelsSet(true);
	VERIFY(
		m_bmpToolBtnSeparator[INT(__ETBS_PRESSED)].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_RIBBON_TB_SEPARATOR_PRESSED_R1,
		IDB_EXT_2010_OFFICE_RIBBON_TB_SEPARATOR_PRESSED_R2_BLUE,
		IDB_EXT_2010_OFFICE_RIBBON_TB_SEPARATOR_PRESSED_R2_SILVER,
		IDB_EXT_2010_OFFICE_RIBBON_TB_SEPARATOR_PRESSED_R2_BLACK)), &m_sImageElement));
	// Map Image 63
	m_bmpToolBtnSeparator[INT(__ETBS_PRESSED)].PreMultipliedRGBChannelsSet(true);

	VERIFY(
		m_bmpLargeBtnSeparator[INT(__ELBS_HOVER)].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_SEPARATOR_HOVER_R1,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_SEPARATOR_HOVER_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_SEPARATOR_HOVER_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_SEPARATOR_HOVER_R2)), &m_sImageElement));

	m_bmpLargeBtnSeparator[INT(__ELBS_HOVER)].Make32();
	m_bmpLargeBtnSeparator[INT(__ELBS_HOVER)].AlphaColor(RGB(255, 0, 255),
		RGB(0, 0, 0), 0);
	VERIFY(
		m_bmpLargeBtnSeparator[INT(__ELBS_PRESSED)].LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_SEPARATOR_PRESSED_R1,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_SEPARATOR_PRESSED_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_SEPARATOR_PRESSED_R2,
		IDB_EXT_2007_RIBBON_BTN_PART_LARGE_SEPARATOR_PRESSED_R2)), &m_sImageElement));

	m_bmpLargeBtnSeparator[INT(__ELBS_PRESSED)].Make32();
	m_bmpLargeBtnSeparator[INT(__ELBS_PRESSED)].AlphaColor(RGB(255, 0, 255),
		RGB(0, 0, 0), 0);

	VERIFY(m_bmpRibbonMenuResizingMarginHV.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex, IDB_EXT_2007_MRB_HV_OBSIDIAN,
		IDB_EXT_2007_MRB_HV_LUNA_BLUE,
		IDB_EXT_2007_MRB_HV_OBSIDIAN,
		IDB_EXT_2007_MRB_HV_OBSIDIAN)), &m_sImageElement));
	// Map Image 64
	VERIFY(m_bmpRibbonMenuResizingMarginV.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex, IDB_EXT_2007_MRB_V_OBSIDIAN,
		IDB_EXT_2007_MRB_V_LUNA_BLUE,
		IDB_EXT_2007_MRB_V_OBSIDIAN,
		IDB_EXT_2007_MRB_V_OBSIDIAN)), &m_sImageElement));
	// Map Image 65
	VERIFY(m_bmpRibbonMenuResizingGripperHV.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex, IDB_EXT_2007_MRG_HV_OBSIDIAN,
		IDB_EXT_2007_MRG_HV_LUNA_BLUE,
		IDB_EXT_2007_MRG_HV_OBSIDIAN,
		IDB_EXT_2007_MRG_HV_OBSIDIAN)), &m_sImageElement));
	// Map Image 66
	m_bmpRibbonMenuResizingGripperHV.Make32();
	m_bmpRibbonMenuResizingGripperHV.AlphaColor(RGB(255, 0, 255),
		RGB(0, 0, 0), 0);
	VERIFY(m_bmpRibbonMenuResizingGripperV.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex, IDB_EXT_2007_MRG_V_OBSIDIAN,
		IDB_EXT_2007_MRG_V_LUNA_BLUE,
		IDB_EXT_2007_MRG_V_OBSIDIAN,
		IDB_EXT_2007_MRG_V_OBSIDIAN)), &m_sImageElement));
	// Map Image 67
	m_bmpRibbonMenuResizingGripperV.Make32();
	m_bmpRibbonMenuResizingGripperV.AlphaColor(RGB(255, 0, 255),
		RGB(0, 0, 0), 0);
	m_clrRibbonModeMenuTextNormal = RGBIMapper(RGB(57, 60, 63), &m_sImageElement);

	VERIFY(
		m_bmpRibbonModeMenuBorder.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_MENU_BORDER_R1,
		IDB_EXT_2010_OFFICE_MENU_BORDER_R2_BLUE,
		IDB_EXT_2010_OFFICE_MENU_BORDER_R2_SILVER,
		IDB_EXT_2010_OFFICE_MENU_BORDER_R2_BLACK)), &m_sImageElement));
	// Map Image 68

	m_bmpRibbonModeMenuBorder.Make32();
	m_bmpRibbonModeMenuBorder.AlphaColor((255, 0, 255),
		RGB(0, 0, 0), 0);
	m_clrMenuCaptionText = RGBIMapper(RGB(57, 60, 63), &m_sImageElement);

	m_rcMenuCaptionBmpPadding.SetRect(0, 2, 0, 2);
	VERIFY(
		m_bmpMenuCaptionBk.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex, IDB_EXT_2010_OFFICE_MENU_CAPTION_BK_R1,
		IDB_EXT_2010_OFFICE_MENU_CAPTION_BK_R2_BLUE,
		IDB_EXT_2010_OFFICE_MENU_CAPTION_BK_R2_SILVER,
		IDB_EXT_2010_OFFICE_MENU_CAPTION_BK_R2_BLACK)), &m_sImageElement));
	// Map Image 69

	m_rcPaddingRibbonFileMenuBigBorder.SetRect(6, 6, 6, 29);
	VERIFY(m_bmpRibbonFileMenuBigBorder.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RM_BIG_BORDER_R1,
		IDB_EXT_2010_OFFICE_RM_BIG_BORDER_R2_BLUE,
		IDB_EXT_2010_OFFICE_RM_BIG_BORDER_R2_SILVER,
		IDB_EXT_2010_OFFICE_RM_BIG_BORDER_R2_BLACK)), &m_sImageElement));
	// Map Image 70
	m_bmpRibbonFileMenuBigBorder.Make32();
	m_bmpRibbonFileMenuBigBorder.AlphaColor(RGB(255, 0, 255),
		RGB(0, 0, 0), 0);

	m_rcPaddingRibbonFileMenuOptionsButton.SetRect(2, 2, 2, 2);
	VERIFY(
		m_bmpRibbonFileMenuOptionsButtonHot.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2007_RM_OPTION_HOT_R1,
		IDB_EXT_2007_RM_OPTION_HOT_R2,
		IDB_EXT_2007_RM_OPTION_HOT_R2,
		IDB_EXT_2007_RM_OPTION_HOT_R2)), &m_sImageElement));

	m_bmpRibbonFileMenuOptionsButtonHot.Make32();
	m_bmpRibbonFileMenuOptionsButtonHot.AlphaColor(RGB(255, 0, 255),
		RGB(0, 0, 0), 0);

	VERIFY(m_bmpResizingGripper.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_RESIZING_GRIPPER_R1,
		IDB_EXT_2010_OFFICE_RESIZING_GRIPPER_R2_BLUE,
		IDB_EXT_2010_OFFICE_RESIZING_GRIPPER_R2_SILVER,
		IDB_EXT_2010_OFFICE_RESIZING_GRIPPER_R2_BLACK)), &m_sImageElement));
	// Map Image 72
	m_bmpResizingGripper.Make32();
	m_bmpResizingGripper.AlphaColor(RGB(255, 0, 255),
		RGB(0, 0, 0), 0);
	// spin bitmaps
	VERIFY(m_arrBmpSpinArrowDown.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_SPIN_ARROW_DOWN_R1,
		IDB_EXT_2007_SPIN_ARROW_DOWN_LUNA_BLUE,
		IDB_EXT_2007_SPIN_ARROW_DOWN_SILVER,
		IDB_EXT_2007_SPIN_ARROW_DOWN_OBSIDIAN)), &m_sImageElement));

	VERIFY(m_arrBmpSpinArrowUp.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_SPIN_ARROW_UP_R1,
		IDB_EXT_2007_SPIN_ARROW_UP_LUNA_BLUE,
		IDB_EXT_2007_SPIN_ARROW_UP_SILVER,
		IDB_EXT_2007_SPIN_ARROW_UP_OBSIDIAN)), &m_sImageElement));

	VERIFY(m_arrBmpSpinDown.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_SPIN_DOWN_R1,
		IDB_EXT_2007_SPIN_DOWN_LUNA_BLUE,
		IDB_EXT_2007_SPIN_DOWN_SILVER,
		IDB_EXT_2007_SPIN_DOWN_OBSIDIAN)), &m_sImageElement));

	VERIFY(m_arrBmpSpinUp.LoadBMP_ResourceMap(MAKEINTRESOURCE(ResourceIndexed4(nIndex,
		IDB_EXT_2010_OFFICE_SPIN_UP_R1,
		IDB_EXT_2007_SPIN_UP_LUNA_BLUE,
		IDB_EXT_2007_SPIN_UP_SILVER,
		IDB_EXT_2007_SPIN_UP_OBSIDIAN)), &m_sImageElement));

}



void CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors(void)
{
	CExtPaintManagerOffice2007_Impl::InitTranslatedColors();

	int nIndex = m_nThemeIndex;
#if (!defined __EXT_MFC_NO_PAGE_NAVIGATOR )
	INT nPnCaptionBkClrIdx = InstallColor(RGBIMapper(RGB(169, 193, 222), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_LIGHT] = nPnCaptionBkClrIdx;
	m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_DARK] = nPnCaptionBkClrIdx;
	m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_HOVER_LIGHT] = nPnCaptionBkClrIdx;
	m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_HOVER_DARK] = nPnCaptionBkClrIdx;
	m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_BOTTOM_LINE] = nPnCaptionBkClrIdx;
	m_mapColorTranslate[_2003CLR_PN_ITEM_LIGHT] = InstallColor(RGBIMapper(RGB(223, 226, 229), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_PN_ITEM_DARK] = InstallColor(RGBIMapper(RGB(225, 227, 230), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_PN_GRIPPER_LIGHT] = InstallColor(RGBIMapper(RGB(246, 247, 248), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_PN_GRIPPER_DARK] = InstallColor(RGBIMapper(RGB(218, 223, 231), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_PN_BORDER] = InstallColor(RGBIMapper(RGB(162, 166, 171), &m_sImageElement));
	m_mapColorTranslate[CLR_PN_CAPTION_TEXT] = InstallColor(RGBIMapper(RGB(101, 109, 117), &m_sImageElement));
#endif // (!defined __EXT_MFC_NO_PAGE_NAVIGATOR )

	m_mapColorTranslate[CLR_GROUP_BOX_FRAME] = InstallColor(RGBIMapper(RGB(169, 177, 184), &m_sImageElement));
	m_mapColorTranslate[CLR_WRB_FRAME] = InstallColor(RGBIMapper(RGB(169, 169, 169), &m_sImageElement));

	m_mapColorTranslate[_2003CLR_EXPBTN_LIGHT] = InstallColor(RGBIndexAuto(nIndex, RGB(209, 213, 219), RGB(216, 228, 242), RGB(238, 240, 243), RGB(145, 145, 145), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_EXPBTN_DARK] = InstallColor(RGBIndexAuto(nIndex, RGB(181, 184, 191), RGB(139, 160, 188), RGB(139, 144, 151), RGB(78, 78, 78), &m_sImageElement));

	m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_LIGHT] = InstallColor(RGBIndexAuto(nIndex, RGB(235, 237, 240), RGB(237, 245, 253), RGB(255, 255, 255), RGB(188, 188, 188), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_MIDDLE] = InstallColor(RGBIndexAuto(nIndex, RGB(232, 234, 236), RGB(216, 229, 244), RGB(251, 250, 250), RGB(188, 188, 188), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_DARK] = InstallColor(RGBIndexAuto(nIndex, RGB(222, 224, 226), RGB(216, 228, 242), RGB(238, 240, 243), RGB(145, 145, 145), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_TOOLBAR_BOTTOM_LINE] = InstallColor(RGBIndexAuto(nIndex, RGB(181, 184, 191), RGB(139, 160, 188), RGB(139, 144, 151), RGB(78, 78, 78), &m_sImageElement));

	m_mapColorTranslate[_2003CLR_GRIPPER_DOT_LIGHT] = InstallColor(RGB(255, 255, 255));
	m_mapColorTranslate[_2003CLR_GRIPPER_DOT_DARK] = InstallColor(RGBIndexAuto(nIndex, RGB(144, 144, 144), RGB(139, 160, 188), RGB(139, 144, 151), RGB(78, 78, 78), &m_sImageElement));

	m_mapColorTranslate[_2003CLR_SEPARATOR_LIGHT] = InstallColor(RGBIndexAuto(nIndex, RGB(255, 255, 255), RGB(255, 255, 255), RGB(255, 255, 255), RGB(188, 188, 188), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_SEPARATOR_DARK] = InstallColor(RGBIndexAuto(nIndex, RGB(170, 170, 170), RGB(139, 160, 188), RGB(139, 144, 151), RGB(78, 78, 78), &m_sImageElement));

	m_mapColorTranslate[_2003CLR_MLA_NORM_LEFT] = InstallColor(RGB(255, 255, 255));
	m_mapColorTranslate[_2003CLR_MLA_NORM_MIDDLE] = InstallColor(RGB(255, 255, 255));
	m_mapColorTranslate[_2003CLR_MLA_NORM_RIGHT] = InstallColor(RGB(255, 255, 255));
	m_mapColorTranslate[_2003CLR_MLA_RARELY_LEFT] = InstallColor(RGBIMapper(RGB(230, 230, 230), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_MLA_RARELY_MIDDLE] = InstallColor(RGBIMapper(RGB(230, 230, 230), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_MLA_RARELY_RIGHT] = InstallColor(RGBIMapper(RGB(230, 230, 230), &m_sImageElement));

	m_mapColorTranslate[_2003CLR_TBB_BK_COMBINED_TOP] = InstallColor(RGB(0xFF, 0xFF, 0xE0));
	m_mapColorTranslate[_2003CLR_TBB_BK_COMBINED_BOTTOM] = InstallColor(RGB(0xFF, 0xFF, 0x00));

	m_mapColorTranslate[_2003CLR_BTN_HOVER_LEFT] = InstallColor(RGBIMapper(RGB(255, 245, 204), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_BTN_HOVER_RIGHT] = InstallColor(RGBIMapper(RGB(255, 219, 117), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_BTN_PRESSED_LEFT] = InstallColor(RGBIndexAuto(nIndex, RGB(255, 245, 204), RGB(255, 219, 117), RGB(255, 245, 204), RGB(255, 245, 204), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_BTN_PRESSED_RIGHT] = InstallColor(RGBIMapper(RGB(255, 219, 117), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_BTN_HP_LEFT] = InstallColor(RGBIMapper(RGB(252, 151, 61), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_BTN_HP_RIGHT] = InstallColor(RGBIMapper(RGB(255, 184, 94), &m_sImageElement));

	m_mapColorTranslate[CLR_3DFACE_IN] = InstallColor(RGBIMapper(RGB(220, 226, 232), &m_sImageElement));

	m_mapColorTranslate[XPCLR_MENU_BORDER] = InstallColor(RGBIMapper(RGB(167, 171, 176), &m_sImageElement));
	m_mapColorTranslate[XPCLR_HILIGHT_BORDER] = InstallColor(RGBIMapper(RGB(255, 189, 105), &m_sImageElement));
	m_mapColorTranslate[XPCLR_HILIGHT_BORDER_SELECTED] = m_mapColorTranslate[XPCLR_HILIGHT_BORDER];

	m_mapColorTranslate[_2003CLR_EXPBTN_CIRCLE_LIGHT] = InstallColor(RGBIMapper(RGB(249, 249, 255), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_EXPBTN_CIRCLE_DARK] = InstallColor(RGBIMapper(RGB(159, 157, 185), &m_sImageElement));

	m_mapColorTranslate[COLOR_3DSHADOW] = InstallColor(RGBIMapper(RGB(141, 141, 141), &m_sImageElement));

	m_mapColorTranslate[XPCLR_3DFACE_FLOAT_R] = InstallColor(RGBIMapper(RGB(219, 218, 228), &m_sImageElement));
	m_mapColorTranslate[XPCLR_3DFACE_FLOAT_F] = InstallColor(RGBIMapper(RGB(219, 218, 228), &m_sImageElement));

	m_mapColorTranslate[XPCLR_TEXT_FIELD_BORDER_NORMAL] = InstallColor(m_arrClrComboBoxBorder[0]);
	m_mapColorTranslate[XPCLR_TEXT_FIELD_BORDER_DISABLED] = InstallColor(m_arrClrComboBoxBorder[3]);

	m_mapColorTranslate[_2003CLR_GRADIENT_LIGHT] = InstallColor(RGBIndexAuto(nIndex, RGB(243, 243, 247), RGB(237, 245, 253), RGB(255, 255, 255), RGB(189, 189, 189), &m_sImageElement));
	m_mapColorTranslate[_2003CLR_GRADIENT_DARK] = InstallColor(RGBIndexAuto(nIndex, RGB(215, 215, 229), RGB(216, 228, 242), RGB(234, 237, 241), RGB(145, 145, 145), &m_sImageElement));

	int nTextColorIndex = InstallColor(RGBIMapper(RGB(57, 60, 63), &m_sImageElement));
	m_mapColorTranslate[CLR_MENUTEXT_IN] = nTextColorIndex;
	m_mapColorTranslate[CLR_MENUTEXT_OUT] = nTextColorIndex;
	m_mapColorTranslate[CLR_TEXT_IN] = nTextColorIndex;
	m_mapColorTranslate[CLR_TEXT_OUT] = nTextColorIndex;
	m_mapColorTranslate[COLOR_MENUTEXT] = nTextColorIndex;
	m_mapColorTranslate[CLR_TEXT_DISABLED] = InstallColor(RGBIMapper(RGB(141, 141, 141), &m_sImageElement));


}

//////////////////////////////////////////////////////////////
// CExtPaintManagerOffice2013_AzureDarkGrey
//////////////////////////////////////////////////////////////

CExtPaintManagerOffice2013_AzureDarkGrey::CExtPaintManagerOffice2013_AzureDarkGrey()
{
	m_nThemeIndex = IndexMapper4(Office2013_AzureDarkGrey, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2013_AzureDarkGrey::~CExtPaintManagerOffice2013_AzureDarkGrey()
{
}

void CExtPaintManagerOffice2013_AzureDarkGrey::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2013_AzureDarkGrey::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}
////////////////////////////////////////////////////////////////
// CExtPaintManagerOffice2013_AzureLightGrey
////////////////////////////////////////////////////////////////

CExtPaintManagerOffice2013_AzureLightGrey::CExtPaintManagerOffice2013_AzureLightGrey()
{
	m_nThemeIndex = IndexMapper4(Office2013_AzureLightGrey, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2013_AzureLightGrey::~CExtPaintManagerOffice2013_AzureLightGrey()
{
}

void CExtPaintManagerOffice2013_AzureLightGrey::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2013_AzureLightGrey::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManagerOffice2013_AzureWhite
////////////////////////////////////////////////////////////////

CExtPaintManagerOffice2013_AzureWhite::CExtPaintManagerOffice2013_AzureWhite()
{
	m_nThemeIndex = IndexMapper4(Office2013_AzureWhite, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2013_AzureWhite::~CExtPaintManagerOffice2013_AzureWhite()
{
}

void CExtPaintManagerOffice2013_AzureWhite::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2013_AzureWhite::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManagerOffice2013_GreenDarkGrey
////////////////////////////////////////////////////////////////

CExtPaintManagerOffice2013_GreenDarkGrey::CExtPaintManagerOffice2013_GreenDarkGrey()
{
	m_nThemeIndex = IndexMapper4(Office2013_GreenDarkGrey, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2013_GreenDarkGrey::~CExtPaintManagerOffice2013_GreenDarkGrey()
{
}

void CExtPaintManagerOffice2013_GreenDarkGrey::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2013_GreenDarkGrey::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManagerOffice2013_GreenLightGrey
////////////////////////////////////////////////////////////////

CExtPaintManagerOffice2013_GreenLightGrey::CExtPaintManagerOffice2013_GreenLightGrey()
{
	m_nThemeIndex = IndexMapper4(Office2013_GreenLightGrey, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2013_GreenLightGrey::~CExtPaintManagerOffice2013_GreenLightGrey()
{
}

void CExtPaintManagerOffice2013_GreenLightGrey::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2013_GreenLightGrey::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManagerOffice2013_GreenWhite
////////////////////////////////////////////////////////////////

CExtPaintManagerOffice2013_GreenWhite::CExtPaintManagerOffice2013_GreenWhite()
{
	m_nThemeIndex = IndexMapper4(Office2013_GreenWhite, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2013_GreenWhite::~CExtPaintManagerOffice2013_GreenWhite()
{
}

void CExtPaintManagerOffice2013_GreenWhite::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2013_GreenWhite::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}
////////////////////////////////////////////////////////////////
// CExtPaintManagerOffice2013_OrangeDarkGrey
////////////////////////////////////////////////////////////////

CExtPaintManagerOffice2013_OrangeDarkGrey::CExtPaintManagerOffice2013_OrangeDarkGrey()
{
	m_nThemeIndex = IndexMapper4(Office2013_OrangeDarkGrey, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2013_OrangeDarkGrey::~CExtPaintManagerOffice2013_OrangeDarkGrey()
{
}

void CExtPaintManagerOffice2013_OrangeDarkGrey::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2013_OrangeDarkGrey::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManagerOffice2013_OrangeLightGrey
////////////////////////////////////////////////////////////////

CExtPaintManagerOffice2013_OrangeLightGrey::CExtPaintManagerOffice2013_OrangeLightGrey()
{
	m_nThemeIndex = IndexMapper4(Office2013_OrangeLightGrey, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2013_OrangeLightGrey::~CExtPaintManagerOffice2013_OrangeLightGrey()
{
}

void CExtPaintManagerOffice2013_OrangeLightGrey::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2013_OrangeLightGrey::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManagerOffice2013_OrangeWhite
////////////////////////////////////////////////////////////////

CExtPaintManagerOffice2013_OrangeWhite::CExtPaintManagerOffice2013_OrangeWhite()
{
	m_nThemeIndex = IndexMapper4(Office2013_OrangeWhite, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2013_OrangeWhite::~CExtPaintManagerOffice2013_OrangeWhite()
{
}

void CExtPaintManagerOffice2013_OrangeWhite::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2013_OrangeWhite::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManagerOffice2013_RedDarkGrey
////////////////////////////////////////////////////////////////

CExtPaintManagerOffice2013_RedDarkGrey::CExtPaintManagerOffice2013_RedDarkGrey()
{
	m_nThemeIndex = IndexMapper4(Office2013_RedDarkGrey, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2013_RedDarkGrey::~CExtPaintManagerOffice2013_RedDarkGrey()
{
}

void CExtPaintManagerOffice2013_RedDarkGrey::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2013_RedDarkGrey::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManagerOffice2013_RedLightGrey
////////////////////////////////////////////////////////////////

CExtPaintManagerOffice2013_RedLightGrey::CExtPaintManagerOffice2013_RedLightGrey()
{
	m_nThemeIndex = IndexMapper4(Office2013_RedLightGrey, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2013_RedLightGrey::~CExtPaintManagerOffice2013_RedLightGrey()
{
}

void CExtPaintManagerOffice2013_RedLightGrey::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2013_RedLightGrey::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManagerOffice2013_RedWhite
////////////////////////////////////////////////////////////////

CExtPaintManagerOffice2013_RedWhite::CExtPaintManagerOffice2013_RedWhite()
{
	m_nThemeIndex = IndexMapper4(Office2013_RedWhite, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerOffice2013_RedWhite::~CExtPaintManagerOffice2013_RedWhite()
{
}

void CExtPaintManagerOffice2013_RedWhite::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerOffice2013_RedWhite::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}


// left for expansion
////////////////////////////////////////////////////////////////
// CExtPaintManager_windows 7
////////////////////////////////////////////////////////////////

CExtPaintManagerWindows7::CExtPaintManagerWindows7()
{
	m_nThemeIndex = IndexMapper4(Windows7, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerWindows7::~CExtPaintManagerWindows7()
{
}

void CExtPaintManagerWindows7::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerWindows7::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

//////////////////////////////////////////////////////////////////////////
void CExtPaintManagerWindows7::PaintTabItem(
	CDC & dc,
	CRect & rcTabItemsArea,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon,
	CObject * pHelperSrc,
	LPARAM lParam,
	COLORREF clrForceText,
	COLORREF clrForceTabBk,
	COLORREF clrForceTabBorderLT,
	COLORREF clrForceTabBorderRB,
	COLORREF clrForceTabSeparator,
	bool bDwmMode
	)
{
	CExtPaintManagerXP::PaintTabItem(
		dc,
		rcTabItemsArea,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		rcEntireItem,
		sizeTextMeasured,
		pFont,
		sText,
		pIcon,
		pHelperSrc,
		lParam,
		clrForceText,
		clrForceTabBk,
		clrForceTabBorderLT,
		clrForceTabBorderRB,
		clrForceTabSeparator,
		bDwmMode
		);
}

////////////////////////////////////////////////////////////////
// CExtPaintManager_Windows 8
////////////////////////////////////////////////////////////////
CExtPaintManagerWindows8::CExtPaintManagerWindows8()
{
	m_nThemeIndex = IndexMapper4(Windows8, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerWindows8::~CExtPaintManagerWindows8()
{
}

void CExtPaintManagerWindows8::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerWindows8::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

//////////////////////////////////////////////////////////////////////////
void CExtPaintManagerWindows8::PaintTabItem(
	CDC & dc,
	CRect & rcTabItemsArea,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon,
	CObject * pHelperSrc,
	LPARAM lParam,
	COLORREF clrForceText,
	COLORREF clrForceTabBk,
	COLORREF clrForceTabBorderLT,
	COLORREF clrForceTabBorderRB,
	COLORREF clrForceTabSeparator,
	bool bDwmMode
	)
{
	CExtPaintManagerXP::PaintTabItem(
		dc,
		rcTabItemsArea,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		rcEntireItem,
		sizeTextMeasured,
		pFont,
		sText,
		pIcon,
		pHelperSrc,
		lParam,
		clrForceText,
		clrForceTabBk,
		clrForceTabBorderLT,
		clrForceTabBorderRB,
		clrForceTabSeparator,
		bDwmMode
		);
}

////////////////////////////////////////////////////////////////
// CExtPaintManager_Windows 10
////////////////////////////////////////////////////////////////
CExtPaintManagerWindows10::CExtPaintManagerWindows10()
{
	m_nThemeIndex = IndexMapper4(Windows10, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);

}

CExtPaintManagerWindows10::~CExtPaintManagerWindows10()
{
}

void CExtPaintManagerWindows10::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerWindows10::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

//////////////////////////////////////////////////////////////////////////
void CExtPaintManagerWindows10::PaintTabItem(
	CDC & dc,
	CRect & rcTabItemsArea,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon,
	CObject * pHelperSrc,
	LPARAM lParam,
	COLORREF clrForceText,
	COLORREF clrForceTabBk,
	COLORREF clrForceTabBorderLT,
	COLORREF clrForceTabBorderRB,
	COLORREF clrForceTabSeparator,
	bool bDwmMode
	)
{
	CExtPaintManagerXP::PaintTabItem(
		dc,
		rcTabItemsArea,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		rcEntireItem,
		sizeTextMeasured,
		pFont,
		sText,
		pIcon,
		pHelperSrc,
		lParam,
		clrForceText,
		clrForceTabBk,
		clrForceTabBorderLT,
		clrForceTabBorderRB,
		clrForceTabSeparator,
		bDwmMode
		);
}

////////////////////////////////////////////////////////////////
// CExtPaintManager_ProfUIS_Theme
////////////////////////////////////////////////////////////////
CExtPaintManager_ProfUIS_Black::CExtPaintManager_ProfUIS_Black()
{
	m_nThemeIndex = IndexMapper4(ProfUIS_Theme_Black, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManager_ProfUIS_Black::~CExtPaintManager_ProfUIS_Black()
{
}

void CExtPaintManager_ProfUIS_Black::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManager_ProfUIS_Black::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManager_ProfUIS_Grey
////////////////////////////////////////////////////////////////
CExtPaintManager_ProfUIS_Grey::CExtPaintManager_ProfUIS_Grey()
{
	m_nThemeIndex = IndexMapper4(ProfUIS_Theme_Grey, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManager_ProfUIS_Grey::~CExtPaintManager_ProfUIS_Grey()
{

}

void CExtPaintManager_ProfUIS_Grey::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManager_ProfUIS_Grey::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManager_ProfUIS_Silver
////////////////////////////////////////////////////////////////
CExtPaintManager_ProfUIS_Silver::CExtPaintManager_ProfUIS_Silver()
{
	m_nThemeIndex = IndexMapper4(ProfUIS_Theme_Silver, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManager_ProfUIS_Silver::~CExtPaintManager_ProfUIS_Silver()
{
}

void CExtPaintManager_ProfUIS_Silver::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManager_ProfUIS_Silver::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManager_ProfUIS_SteelBlue
////////////////////////////////////////////////////////////////
CExtPaintManager_ProfUIS_SteelBlue::CExtPaintManager_ProfUIS_SteelBlue()
{
	m_nThemeIndex = IndexMapper4(ProfUIS_Theme_SteelBlue, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManager_ProfUIS_SteelBlue::~CExtPaintManager_ProfUIS_SteelBlue()
{
}

void CExtPaintManager_ProfUIS_SteelBlue::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManager_ProfUIS_SteelBlue::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManager_ProfUIS_BBlue
////////////////////////////////////////////////////////////////
CExtPaintManager_ProfUIS_BBlue::CExtPaintManager_ProfUIS_BBlue()
{
	m_nThemeIndex = IndexMapper4(ProfUIS_Theme_BBlue, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManager_ProfUIS_BBlue::~CExtPaintManager_ProfUIS_BBlue()
{
}

void CExtPaintManager_ProfUIS_BBlue::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManager_ProfUIS_BBlue::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManager_ProfUIS_SkyBlue
////////////////////////////////////////////////////////////////
CExtPaintManager_ProfUIS_SkyBlue::CExtPaintManager_ProfUIS_SkyBlue()
{
	m_nThemeIndex = IndexMapper4(ProfUIS_Theme_SkyBlue, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManager_ProfUIS_SkyBlue::~CExtPaintManager_ProfUIS_SkyBlue()
{
}

void CExtPaintManager_ProfUIS_SkyBlue::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManager_ProfUIS_SkyBlue::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManager_ProfUIS_Gold
////////////////////////////////////////////////////////////////
CExtPaintManager_ProfUIS_Gold::CExtPaintManager_ProfUIS_Gold()
{
	m_nThemeIndex = IndexMapper4(ProfUIS_Theme_Gold, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManager_ProfUIS_Gold::~CExtPaintManager_ProfUIS_Gold()
{
}

void CExtPaintManager_ProfUIS_Gold::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManager_ProfUIS_Gold::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

////////////////////////////////////////////////////////////////
// CExtPaintManager_ProfUIS_White
////////////////////////////////////////////////////////////////
CExtPaintManager_ProfUIS_White::CExtPaintManager_ProfUIS_White()
{
	m_nThemeIndex = IndexMapper4(ProfUIS_Theme_White, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManager_ProfUIS_White::~CExtPaintManager_ProfUIS_White()
{
}

void CExtPaintManager_ProfUIS_White::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManager_ProfUIS_White::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}


////////////////////////////////////////////////////////////////
// CExtPaintManager_Photoshop
////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
CExtPaintManager_Photoshop::CExtPaintManager_Photoshop()
{
	m_nThemeIndex = IndexMapper4(Photoshop, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
	m_clrFillHint = RGB(72, 72, 72);
	m_bUseNcFrameBmpForDockerBkgnd = true;
}

//////////////////////////////////////////////////////////////////////////
CExtPaintManager_Photoshop::~CExtPaintManager_Photoshop()
{
}

//////////////////////////////////////////////////////////////////////////
void CExtPaintManager_Photoshop::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

//////////////////////////////////////////////////////////////////////////
void CExtPaintManager_Photoshop::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();

	m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_LIGHT] = InstallColor(RGB(72, 72, 72));
	m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_MIDDLE] = InstallColor(RGB(72, 72, 72) /*RGB(147, 147, 147)*/);
	m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_DARK] = InstallColor(RGB(72, 72, 72));
	m_mapColorTranslate[_2003CLR_TOOLBAR_BOTTOM_LINE] = InstallColor(RGB(72, 72, 72));

	m_mapColorTranslate[_2003CLR_GRIPPER_DOT_LIGHT] = InstallColor(RGB(120, 120, 120));
	m_mapColorTranslate[_2003CLR_GRIPPER_DOT_DARK] = InstallColor(RGB(78, 78, 78));

	m_mapColorTranslate[_2003CLR_SEPARATOR_LIGHT] = InstallColor(RGB(120, 120, 120));
	m_mapColorTranslate[_2003CLR_SEPARATOR_DARK] = InstallColor(RGB(78, 78, 78));

	m_mapColorTranslate[_2003CLR_MLA_NORM_LEFT] = InstallColor(RGB(72, 72, 72));
	m_mapColorTranslate[_2003CLR_MLA_NORM_MIDDLE] = InstallColor(RGB(72, 72, 72));
	m_mapColorTranslate[_2003CLR_MLA_NORM_RIGHT] = InstallColor(RGB(72, 72, 72));
	m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_LIGHT] = InstallColor(RGB(72, 72, 72));
	m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_MIDDLE] = InstallColor(RGB(72, 72, 72) /*RGB(147, 147, 147)*/);
	m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_DARK] = InstallColor(RGB(72, 72, 72));
	m_mapColorTranslate[_2003CLR_TOOLBAR_BOTTOM_LINE] = InstallColor(RGB(72, 72, 72));
}

//////////////////////////////////////////////////////////////////////////
