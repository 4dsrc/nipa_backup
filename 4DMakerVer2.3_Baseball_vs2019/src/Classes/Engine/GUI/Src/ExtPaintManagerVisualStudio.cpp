
// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "StdAfx.h"

// disable warning 4706, 4996
#pragma warning( push )
#pragma warning ( disable : 4706 )
#pragma warning ( disable : 4996 )
#include <multimon.h>
// rollback warning 4706, 4996
#pragma warning( pop )

#if _MFC_VER < 0x700
#include <../src/AfxImpl.h>
#else
#include <../src/mfc/AfxImpl.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
#include <ExtPaintManager.h>
#endif

#if (!defined __EXT_RICH_CONTENT_H)
#include <ExtRichContent.h>
#endif // (!defined __EXT_RICH_CONTENT_H)

#if (!defined __ExtCmdManager_H)
#include <ExtCmdManager.h>
#endif

#if (!defined __EXT_MEMORY_DC_H)
#include <../Src/ExtMemoryDC.h>
#endif

#if (!defined __EXT_POPUP_MENU_WND_H)
#include <ExtPopupMenuWnd.h>
#endif

#if (!defined __EXT_MENUCONTROLBAR_H)
#include <ExtMenuControlBar.h>
#endif

#if (!defined __EXTDOCKBAR_H)
#include <../Src/ExtDockBar.h>
#endif

#if (!defined __EXT_STATUSCONTROLBAR_H)
#include <ExtStatusControlBar.h>
#endif

#if (!defined __EXT_MFC_NO_TAB_CONTROLBARS)
#if (!defined __EXT_CONTROLBAR_TABBED_FEATURES_H)
#include "ExtControlBarTabbedFeatures.h"
#endif // __EXT_CONTROLBAR_TABBED_FEATURES_H
#endif

#if( !defined __EXTMINIDOCKFRAMEWND_H)
#include "ExtMiniDockFrameWnd.h"
#endif

#if (!defined __EXT_MFC_NO_SHORTCUTLIST_CTRL)
#if (!defined __EXTSHORTCUTLISTWND_H)
#include <ExtShortcutListWnd.h>
#endif
#endif // (!defined __EXT_MFC_NO_SHORTCUTLIST_CTRL)

#if (!defined __EXT_BUTTON_H)
#include <ExtButton.h>
#endif

#if (!defined __EXT_GROUPBOX_H)
#include <ExtGroupBox.h>
#endif

#if (!defined __PROF_UIS_RES_2007_H)
#include <Resources/Res2007/Res2007.h>
#endif

#if (!defined __PROF_UIS_RES_2010_OFFICE_H)
#include <Resources/Res2010office/Res2010office.h>
#endif

#if (!defined __EXT_SCROLLWND_H)
#include <ExtScrollWnd.h>
#endif 

#if (!defined __EXT_MFC_NO_REPORTGRIDWND)
#if (!defined __EXT_REPORT_GRID_WND_H)
#include <ExtReportGridWnd.h>
#endif
#endif 

#if (!defined __EXT_MFC_NO_DATE_PICKER)
#if (!defined __EXT_DATE_PICKER_H)
#include <ExtDatePicker.h>
#endif
#endif 

#if (!defined __EXT_MFC_NO_TOOLBOX_CTRL)
#if (!defined __EXTTOOLBOXWND_H)
#include <ExtToolBoxWnd.h>
#endif
#endif 

#if (!defined __EXT_MFC_NO_RIBBON_BAR)
#if (!defined __EXT_RIBBON_BAR_H)
#include <ExtRibbonBar.h>
#endif // (!defined __EXT_RIBBON_BAR_H)
#endif

#if (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)
#if (!defined __EXT_POPUP_CTRL_MENU_H)
#include <ExtPopupCtrlMenu.h>
#endif
#endif

#if (!defined __EXT_MFC_NO_GRIDWND)
#if (!defined __EXT_GRIDWND_H)
#include <ExtGridWnd.h>
#endif 
#endif

#if (!defined __EXT_TEMPL_H)
#include <ExtTempl.h>
#endif

#if (!defined __EXT_SPIN_H)
#include <ExtSpinWnd.h>
#endif

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
#if (!defined __EXTCUSTOMIZE_H)
#include <ExtCustomize.h>
#endif
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

#if (!defined __EXT_MFC_NO_DURATIONWND)
#if (!defined __EXT_DURATIONWND_H)
#include "ExtDurationWnd.h"
#endif
#endif

#if (!defined __EXT_MFC_NO_PAGECONTAINER_CTRL)
#if (!defined __EXT_PAGECONTAINERWND_H)
#include <ExtPageContainerWnd.h>
#endif
#endif

#if (!defined __EXT_RESIZABLE_DIALOG_H)
#include <ExtResizableDialog.h>
#endif

#if (!defined __EXT_EDIT_H)
#include <ExtEdit.h>
#endif

#if (!defined __EXT_COMBO_BOX_H)
#include <ExtComboBox.h>
#endif

#if (!defined __EXT_VERSION_EX_H)
#include "ExtVersionEx.h"
#endif

#if (! defined __VSSYM32_H__)
#include <vssym32/vssym32.h>
#endif // (! defined __VSSYM32_H__)

#include <math.h>
#include <shlwapi.h>

#include <Resources/Resource.h>

#if (! defined CLEARTYPE_QUALITY )
#define CLEARTYPE_QUALITY 5
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE [] = __FILE__;
#endif

// VC+ 6.0 static builds specific: disable warnings 4305, 4309
// warning C4305: 'initializing' : truncation from 'int' to 'short'
// warning C4309: 'initializing' : truncation of constant value
#if _MFC_VER < 0x700
#ifdef __EXT_PROFUIS_STATIC_LINK
#pragma warning( push )
#pragma warning ( disable : 4305 )
#pragma warning ( disable : 4309 )
#endif
#endif

////////////////////////////////////////////////////////////////
// The Visual Studio Constructor, destructors, and translators
// the visual studio functions will probably go into a function file.
////////////////////////////////////////////////////////////////
// CExtPaintManagerOffice2003
CExtPaintManagerOffice2003::CExtPaintManagerOffice2003()
	: m_nIdxClrTbFillMargin(-1)
{
	m_bEnabledHoverIconShadows = false;
	m_bHelperXpStyle8BitBarRect = false;
	m_bExpBtnSwapVGlyphs = true;

	m_pGlyphTbEpBtnH0 = &CExtPaintManager::g_DockingCaptionGlyphs[__DCBT_BUTTON_EXPAND_BOTTOM_2003];
	m_pGlyphTbEpBtnH1 = &CExtPaintManager::g_DockingCaptionGlyphs[__DCBT_BUTTON_EXPAND_RIGHT2_2003];
	m_pGlyphTbEpBtnV0 = &CExtPaintManager::g_DockingCaptionGlyphs[__DCBT_BUTTON_EXPAND_LEFT_2003];
	m_pGlyphTbEpBtnV1 = &CExtPaintManager::g_DockingCaptionGlyphs[__DCBT_BUTTON_EXPAND_BOTTOM2_2003];

	m_nIdxClrMlaNormLeft = _2003CLR_MLA_NORM_LEFT;
	m_nIdxClrMlaNormMiddle = _2003CLR_MLA_NORM_MIDDLE;
	m_nIdxClrMlaNormRight = _2003CLR_MLA_NORM_RIGHT;
	m_nIdxClrMlaRarelyLeft = _2003CLR_MLA_RARELY_LEFT;
	m_nIdxClrMlaRarelyMiddle = _2003CLR_MLA_RARELY_MIDDLE;
	m_nIdxClrMlaRarelyRight = _2003CLR_MLA_RARELY_RIGHT;
#if (!defined __EXT_MFC_NO_PAGECONTAINER_CTRL)
	m_bPageContainerUseButtonStyle = true;
	m_bPageContainerUseGroupBoxStyle = true;
	m_bPageContainerNoEmbossDisabledText = true;
#endif // (!defined __EXT_MFC_NO_PAGECONTAINER_CTRL)
#if (!defined __EXT_MFC_NO_SHORTCUTLIST_CTRL)
	m_bShortcutListUseButtonStyleInScrollButtons = true;
	//	m_bShortcutListUseDockerBkgnd = true;
	m_bShortcutListItemUseButtonStyle = true;
#endif // (!defined __EXT_MFC_NO_SHORTCUTLIST_CTRL)
	m_nThemeIndex = 0;
}

CExtPaintManagerOffice2003::~CExtPaintManagerOffice2003()
{
}

void CExtPaintManagerOffice2003::InitTranslatedColors()
{
	ASSERT_VALID(this);

	m_nIdxClrBtnHoverLeft = -1;
	m_nIdxClrBtnHoverRight = -1;
	m_nIdxClrBtnPressedLeft = -1;
	m_nIdxClrBtnPressedRight = -1;
	m_nIdxClrBtnHovPresLeft = -1;
	m_nIdxClrBtnHovPresRight = -1;
	m_nIdxClrTbFillMargin = -1;
	g_PaintManager.m_bUxValidColorsExtracted = false;

	g_PaintManager.InitUserExApi();

	CExtPaintManagerXP::InitTranslatedColors();

	if (stat_GetBPP() > 8)
	{
		if (OnQueryUseThemeColors())
		{
			// if use WinXP themed colors
			COLORREF clrFillHint, clrAccentHint;
			OnQueryThemeColors(&clrFillHint, &clrAccentHint);

			// xp - menu face
			COLORREF xpclr_MenuLight = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.95, -0.05);
			xpclr_MenuLight = RGB(
				GetRValue(xpclr_MenuLight) + ::MulDiv(255 - GetRValue(xpclr_MenuLight), 40, 100),
				GetGValue(xpclr_MenuLight) + ::MulDiv(255 - GetGValue(xpclr_MenuLight), 20, 100),
				GetBValue(xpclr_MenuLight));
			xpclr_MenuLight = CExtBitmap::stat_HLS_Adjust(xpclr_MenuLight, 0.00, 0.06, 0.05);

			// 2003 - float light
			COLORREF xpclr_FloatFace = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.50, 0.00);

			// xp - rarely used item left side
			COLORREF xpclr_RarelyUsedMenuLeft = RGB(
				(999L * long(GetRValue(clrFillHint))) / 1000L,
				(995L * long(GetGValue(clrFillHint))) / 1000L,
				(995L * long(GetBValue(clrFillHint))) / 1000L);

			// xp - control bar background
			COLORREF xpclr_ControlBarBk = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.20, 0.00);

			// xp - orange like light ver
			COLORREF xpclr_Highlight = CExtBitmap::stat_HLS_Adjust(clrAccentHint, 0.02, 0.60, 0.45);

			// 2003 - dark orange
			COLORREF clr2003faceIn = CExtBitmap::stat_HLS_Adjust(clrAccentHint, 0.00, -0.30, 0.45);

			// xp - blue like dark ver
			COLORREF clrTmp = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.20, 0.00);
			COLORREF xpclr_HighlightDarked = CExtBitmap::stat_HLS_Adjust(clrTmp, 0.00, -0.25, 0.10);

			// xp - dark selected border
			COLORREF xpclr_HighlightBorder = CExtBitmap::stat_HLS_Adjust(xpclr_HighlightDarked, 0.00, -0.50, 0.00);

			// xp - dark gray separator
			COLORREF xpclr_Separator = RGB(
				(857L * long(GetRValue(clrFillHint))) / 1000L,
				(857L * long(GetGValue(clrFillHint))) / 1000L,
				(857L * long(GetBValue(clrFillHint))) / 1000L);

			// xp - dark panel border (for floating controlbars)
			COLORREF xpclr_PanelBorder =
				CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.25, 0.00);

			m_mapColorTranslate[CLR_3DFACE_OUT] = InstallColor(xpclr_ControlBarBk);
			m_mapColorTranslate[CLR_3DFACE_IN] = InstallColor(xpclr_Highlight);
			m_mapColorTranslate[CLR_3DFACE_DISABLED] = COLOR_3DFACE;

			int idxClrDark = InstallColor(xpclr_PanelBorder);

			m_mapColorTranslate[CLR_3DLIGHT_OUT] = idxClrDark;
			m_mapColorTranslate[CLR_3DLIGHT_IN] = idxClrDark;
			m_mapColorTranslate[CLR_3DLIGHT_DISABLED] = idxClrDark;

			m_mapColorTranslate[CLR_3DHILIGHT_OUT] = idxClrDark;
			m_mapColorTranslate[CLR_3DHILIGHT_IN] = idxClrDark;
			m_mapColorTranslate[CLR_3DHILIGHT_DISABLED] = idxClrDark;

			m_mapColorTranslate[CLR_3DSHADOW_OUT] = idxClrDark;
			m_mapColorTranslate[CLR_3DSHADOW_IN] = idxClrDark;
			m_mapColorTranslate[CLR_3DSHADOW_DISABLED] = idxClrDark;

			m_mapColorTranslate[CLR_3DDKSHADOW_OUT] = idxClrDark;
			m_mapColorTranslate[CLR_3DDKSHADOW_IN] = idxClrDark;
			m_mapColorTranslate[CLR_3DDKSHADOW_DISABLED] = idxClrDark;

			m_mapColorTranslate[CLR_TEXT_OUT] = COLOR_WINDOWTEXT;
			m_mapColorTranslate[CLR_TEXT_IN] = COLOR_WINDOWTEXT;
			m_mapColorTranslate[CLR_TEXT_DISABLED] = COLOR_GRAYTEXT;
			m_mapColorTranslate[XPCLR_PUSHEDHOVERTEXT] = COLOR_BTNTEXT;

			// Menu text colors
			m_mapColorTranslate[CLR_MENUTEXT_OUT] = InstallColor(RGB(0, 0, 0));
			m_mapColorTranslate[CLR_MENUTEXT_IN] = InstallColor(RGB(0, 0, 0));
			m_mapColorTranslate[CLR_MENUTEXT_DISABLED] = COLOR_GRAYTEXT;

			m_mapColorTranslate[XPCLR_TEXT_FIELD_BORDER_NORMAL] = COLOR_3DFACE;
			m_mapColorTranslate[XPCLR_TEXT_FIELD_BORDER_DISABLED] = InstallColor(xpclr_Separator);

			m_mapColorTranslate[XPCLR_3DFACE_DARK] = InstallColor(xpclr_ControlBarBk);
			m_mapColorTranslate[XPCLR_3DFACE_NORMAL] = InstallColor(xpclr_MenuLight);
			m_mapColorTranslate[XPCLR_SEPARATOR] = InstallColor(xpclr_Separator);

			m_mapColorTranslate[XPCLR_HILIGHT] =
				//InstallColor( xpclr_HighlightDarked );
				InstallColor(clr2003faceIn);

			m_mapColorTranslate[XPCLR_HILIGHT_BORDER] = InstallColor(xpclr_HighlightBorder);
			m_mapColorTranslate[XPCLR_HILIGHT_BORDER_SELECTED] = InstallColor(xpclr_HighlightBorder);

			m_mapColorTranslate[XPCLR_RARELY_BORDER] = InstallColor(xpclr_RarelyUsedMenuLeft);

			m_mapColorTranslate[XPCLR_3DFACE_FLOAT_F] = InstallColor(xpclr_FloatFace);

			// install new colors
			m_mapColorTranslate[_2003CLR_GRADIENT_LIGHT] =
				InstallColor(
				(OnQuerySystemTheme() == ThemeLunaSilver)
				? CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.845, 0.10)
				: CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.43, 0.00)
				);
			m_mapColorTranslate[_2003CLR_GRADIENT_DARK] =
				InstallColor(
				(OnQuerySystemTheme() == ThemeLunaSilver)
				? CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.45, 0.10)
				: CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.02, 0.07, 0.00)
				);

			m_mapColorTranslate[_2003CLR_SEPARATOR_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.98, 0.00));
			m_mapColorTranslate[_2003CLR_SEPARATOR_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.45, -0.00));
			m_mapColorTranslate[_2003CLR_GRIPPER_DOT_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.60, 0.00));
			m_mapColorTranslate[_2003CLR_GRIPPER_DOT_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.95, 0.00));
			m_mapColorTranslate[_2003CLR_EXPBTN_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.10, 0.00));
			m_mapColorTranslate[_2003CLR_EXPBTN_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.60, 0.00));
			m_mapColorTranslate[_2003CLR_EXPBTN_HOVER_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrAccentHint, 0.00, 0.85, 0.00));
			m_mapColorTranslate[_2003CLR_EXPBTN_HOVER_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrAccentHint, 0.00, -0.05, 0.00));
			m_mapColorTranslate[_2003CLR_EXPBTN_PRESSED_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrAccentHint, 0.00, 0.30, 0.00));
			m_mapColorTranslate[_2003CLR_EXPBTN_PRESSED_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrAccentHint, 0.00, -0.30, 0.00));
			m_mapColorTranslate[_2003CLR_EXPGLYPH_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.97, 0.00));
			m_mapColorTranslate[_2003CLR_EXPGLYPH_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.97, 0.00));
			m_mapColorTranslate[_2003CLR_STATUSBAR_ITEM] = COLOR_3DSHADOW;

			COLORREF clrTbGradientLight = (COLORREF) (-1L);
			COLORREF clrTbGradientMiddle = (COLORREF) (-1L);
			COLORREF clrTbGradientDark = (COLORREF) (-1L);
			COLORREF clrTbBottomLine = (COLORREF) (-1L);
			COLORREF clrMlaNormLeft = (COLORREF) (-1L);
			COLORREF clrMlaNormMiddle = (COLORREF) (-1L);
			COLORREF clrMlaNormRight = (COLORREF) (-1L);
			COLORREF clrMlaRarelyLeft = (COLORREF) (-1L);
			COLORREF clrMlaRarelyMiddle = (COLORREF) (-1L);
			COLORREF clrMlaRarelyRight = (COLORREF) (-1L);
			COLORREF clrMenuBorder = (COLORREF) (-1L);
			COLORREF clrTbbBkTop = (COLORREF) (-1L);
			COLORREF clrTbbBkBottom = (COLORREF) (-1L);

			e_system_theme_t eCurrentTheme = OnQuerySystemTheme();
			switch (eCurrentTheme)
			{
			case ThemeLunaRoyale:       // +2.87
			case ThemeVistaOrLaterUX:   // +2.87
			case ThemeVistaOrLaterDWM:  // +2.87
			case ThemeLunaBlue:
				clrTbGradientLight = RGB(221, 236, 254);
				clrTbGradientMiddle = RGB(202, 225, 252);
				clrTbGradientDark = RGB(110, 155, 216);
				clrTbBottomLine = RGB(59, 97, 156);
				clrMlaNormLeft = RGB(227, 239, 255);
				clrMlaNormMiddle = RGB(202, 225, 252);
				clrMlaNormRight = RGB(135, 173, 228);
				clrMlaRarelyLeft = RGB(203, 221, 246);
				clrMlaRarelyMiddle = RGB(161, 196, 248);
				clrMlaRarelyRight = RGB(121, 161, 220);
				clrMenuBorder = RGB(0, 45, 150);
				clrTbbBkTop = RGB(227, 239, 255);
				clrTbbBkBottom = RGB(123, 164, 224);
				break;
			case ThemeLunaOlive:
				clrTbGradientLight = RGB(244, 247, 222);
				clrTbGradientMiddle = RGB(206, 220, 167);
				clrTbGradientDark = RGB(177, 192, 139);
				clrTbBottomLine = RGB(96, 128, 88);
				clrMlaNormLeft = RGB(255, 255, 237);
				clrMlaNormMiddle = RGB(206, 219, 167);
				clrMlaNormRight = RGB(184, 199, 146);
				clrMlaRarelyLeft = RGB(230, 230, 209);
				clrMlaRarelyMiddle = RGB(186, 200, 143);
				clrMlaRarelyRight = RGB(164, 180, 120);
				clrMenuBorder = RGB(117, 141, 94);
				clrTbbBkTop = RGB(237, 239, 214);
				clrTbbBkBottom = RGB(181, 196, 143);
				break;
			case ThemeLunaSilver:
				clrTbGradientLight = RGB(243, 244, 250);
				clrTbGradientMiddle = RGB(225, 226, 236);
				clrTbGradientDark = RGB(134, 130, 166);
				clrTbBottomLine = RGB(124, 124, 148);
				clrMlaNormLeft = RGB(249, 249, 255);
				clrMlaNormMiddle = RGB(225, 226, 236);
				clrMlaNormRight = RGB(159, 157, 185);
				clrMlaRarelyLeft = RGB(215, 215, 226);
				clrMlaRarelyMiddle = RGB(184, 185, 202);
				clrMlaRarelyRight = RGB(128, 126, 158);
				clrMenuBorder = RGB(124, 124, 148);
				clrTbbBkTop = RGB(231, 233, 241);
				clrTbbBkBottom = RGB(172, 170, 194);
				break;
			default:
				clrTbGradientLight = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.75, 0.15);
				clrTbGradientMiddle = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.65, 0.15);
				clrTbGradientDark = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.22, -0.25);
				clrTbBottomLine = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.60, 0.00);
				clrMlaNormLeft = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.75, 0.00);
				clrMlaNormRight = CExtBitmap::stat_HLS_Adjust(GetColor(XPCLR_3DFACE_DARK, this), 0.00, -0.10, 0.00);
				clrMlaNormMiddle = CExtBitmap::stat_HLS_Adjust(clrMlaNormRight, 0.00, 0.55, 0.00);
				clrMlaRarelyLeft = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.30, 0.00);
				clrMlaRarelyRight = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.25, 0.00);
				clrMlaRarelyMiddle = CExtBitmap::stat_HLS_Adjust(clrMlaRarelyRight, 0.00, 0.55, 0.00);
				clrMenuBorder = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.68, 0.00);
				clrTbbBkTop = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.75, 0.00);
				clrTbbBkBottom = CExtBitmap::stat_HLS_Adjust(GetColor(XPCLR_3DFACE_DARK, this), 0.00, -0.10, 0.00);
				break;
			} // switch( eCurrentTheme )

			m_mapColorTranslate[_2003CLR_TBB_BK_COMBINED_TOP] = InstallColor(clrTbbBkTop);
			m_mapColorTranslate[_2003CLR_TBB_BK_COMBINED_BOTTOM] = InstallColor(clrTbbBkBottom);
			m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_LIGHT] = InstallColor(clrTbGradientLight);
			m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_MIDDLE] = InstallColor(clrTbGradientMiddle);
			m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_DARK] = InstallColor(clrTbGradientDark);
			m_mapColorTranslate[_2003CLR_TOOLBAR_BOTTOM_LINE] = InstallColor(clrTbBottomLine);
			m_mapColorTranslate[_2003CLR_MLA_NORM_LEFT] = InstallColor(clrMlaNormLeft);
			m_mapColorTranslate[_2003CLR_MLA_NORM_MIDDLE] = InstallColor(clrMlaNormMiddle);
			m_mapColorTranslate[_2003CLR_MLA_NORM_RIGHT] = InstallColor(clrMlaNormRight);
			m_mapColorTranslate[_2003CLR_MLA_RARELY_LEFT] = InstallColor(clrMlaRarelyLeft);
			m_mapColorTranslate[_2003CLR_MLA_RARELY_MIDDLE] = InstallColor(clrMlaRarelyMiddle);
			m_mapColorTranslate[_2003CLR_MLA_RARELY_RIGHT] = InstallColor(clrMlaRarelyRight);
			m_mapColorTranslate[XPCLR_MENU_BORDER] = InstallColor(clrMenuBorder);
			m_mapColorTranslate[_2003CLR_BTN_HOVER_LEFT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrAccentHint, 0.05, 0.75, 0.00));
			m_mapColorTranslate[_2003CLR_BTN_HOVER_RIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrAccentHint, -0.05, -0.02, 0.00));
			m_mapColorTranslate[_2003CLR_BTN_PRESSED_LEFT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrAccentHint, 0.02, 0.40, 0.00));
			m_mapColorTranslate[_2003CLR_BTN_PRESSED_RIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrAccentHint, -0.07, -0.31, 0.00));
			m_mapColorTranslate[_2003CLR_BTN_HP_LEFT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrAccentHint, -0.07, -0.31, 0.00));
			m_mapColorTranslate[_2003CLR_BTN_HP_RIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrAccentHint, 0.02, 0.40, 0.00));
			// page navigator colors
			m_mapColorTranslate[_2003CLR_PN_BORDER] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.10, -0.55, 0.00));
			m_mapColorTranslate[_2003CLR_PN_GRIPPER_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.02, -0.23, -0.25));
			m_mapColorTranslate[_2003CLR_PN_GRIPPER_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.05, -0.60, 0.00));
			m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.45, 0.00));
			m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.00, 0.00));
			m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_HOVER_LIGHT] = m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_LIGHT];
			m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_HOVER_DARK] = m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_DARK];
			m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_BOTTOM_LINE] = m_mapColorTranslate[_2003CLR_PN_BORDER];
			m_mapColorTranslate[_2003CLR_PN_ITEM_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.55, 0.20));
			m_mapColorTranslate[_2003CLR_PN_ITEM_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.13, -0.15));

			// Popup menu Expand Button
			m_mapColorTranslate[_2003CLR_EXPBTN_CIRCLE_LIGHT] = InstallColor(GetColor(_2003CLR_MLA_NORM_LEFT, this));
			m_mapColorTranslate[_2003CLR_EXPBTN_CIRCLE_DARK] = InstallColor(GetColor(_2003CLR_MLA_NORM_RIGHT, this));

			// Button colors
			m_nIdxClrBtnHoverLeft = _2003CLR_BTN_HOVER_LEFT;
			m_nIdxClrBtnHoverRight = _2003CLR_BTN_HOVER_RIGHT;
			m_nIdxClrBtnPressedLeft = _2003CLR_BTN_PRESSED_LEFT;
			m_nIdxClrBtnPressedRight = _2003CLR_BTN_PRESSED_RIGHT;
			m_nIdxClrBtnHovPresLeft = _2003CLR_BTN_HP_LEFT;
			m_nIdxClrBtnHovPresRight = _2003CLR_BTN_HP_RIGHT;

			m_nIdxClrTbFillMargin = _2003CLR_TOOLBAR_BOTTOM_LINE;

			m_colors[COLOR_3DFACE] = clrFillHint;
			m_colors[COLOR_3DLIGHT] = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.45, 0.10);
			m_colors[COLOR_3DHIGHLIGHT] = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.75, 0.00);
			m_colors[COLOR_3DSHADOW] = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.45, 0.10);
			m_colors[COLOR_3DDKSHADOW] = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.75, 0.00);

			// Task Pane Colors
			COLORREF clrTPBkTop = (COLORREF) (-1L);
			COLORREF clrTPBkBottom = (COLORREF) (-1L);
			COLORREF clrTPGroupCaptionTextNormal = (COLORREF) (-1L);
			COLORREF clrTPGroupCaptionTextNormalHover = (COLORREF) (-1L);
			COLORREF clrTPGroupCaptionBkLeftNormal = (COLORREF) (-1L);
			COLORREF clrTPGroupCaptionBkRightNormal = (COLORREF) (-1L);

			switch (eCurrentTheme)
			{
			case ThemeLunaRoyale:       // +2.87
			case ThemeVistaOrLaterUX:   // +2.87
			case ThemeVistaOrLaterDWM:  // +2.87
			case ThemeLunaBlue:
				clrTPBkTop = RGB(221, 236, 254);
				clrTPBkBottom = RGB(74, 122, 201);
				clrTPGroupCaptionTextNormal = RGB(0, 45, 134);
				clrTPGroupCaptionTextNormalHover = RGB(0, 45, 134);
				clrTPGroupCaptionBkLeftNormal = RGB(196, 219, 249);
				clrTPGroupCaptionBkRightNormal = RGB(101, 143, 224);
				break;
			case ThemeLunaOlive:
				clrTPBkTop = RGB(243, 242, 231);
				clrTPBkBottom = RGB(190, 198, 152);
				clrTPGroupCaptionTextNormal = RGB(90, 107, 70);
				clrTPGroupCaptionTextNormalHover = RGB(90, 107, 70);
				clrTPGroupCaptionBkLeftNormal = RGB(210, 223, 174);
				clrTPGroupCaptionBkRightNormal = RGB(161, 176, 128);
				break;
			case ThemeLunaSilver:
				clrTPBkTop = RGB(238, 238, 244);
				clrTPBkBottom = RGB(177, 176, 195);
				clrTPGroupCaptionTextNormal = RGB(92, 91, 121);
				clrTPGroupCaptionTextNormalHover = RGB(92, 91, 121);
				clrTPGroupCaptionBkLeftNormal = RGB(207, 207, 222);
				clrTPGroupCaptionBkRightNormal = RGB(169, 168, 191);
				break;
			default:
				clrTPBkTop = CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_3DFACE, this), GetColor(COLOR_WINDOW, this), 50);
				clrTPBkBottom = GetColor(COLOR_3DFACE, this);
				clrTPGroupCaptionTextNormal = GetColor(COLOR_WINDOWTEXT, this);
				clrTPGroupCaptionTextNormalHover = GetColor(COLOR_WINDOWTEXT, this);
				clrTPGroupCaptionBkLeftNormal = CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_3DFACE, this), GetColor(COLOR_WINDOW, this), 39);
				clrTPGroupCaptionBkRightNormal = CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_3DFACE, this), GetColor(COLOR_WINDOW, this), 70);
				break;
			} // switch( eCurrentTheme )
			m_mapColorTranslate[CLR_TASK_PANE_BK_TOP] = InstallColor(clrTPBkTop);
			m_mapColorTranslate[CLR_TASK_PANE_BK_BOTTOM] = InstallColor(clrTPBkBottom);

			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_NORMAL] = InstallColor(clrTPGroupCaptionTextNormal);
			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_NORMAL_HOVERED] = InstallColor(clrTPGroupCaptionTextNormalHover);
			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_HIGHLIGHTED] = m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_NORMAL];
			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_HIGHLIGHTED_HOVERED] = m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_NORMAL_HOVERED];

			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_LEFT_NORMAL] = InstallColor(clrTPGroupCaptionBkLeftNormal);
			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_LEFT_HIGHLIGHTED] = InstallColor(clrTPGroupCaptionBkRightNormal);
			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_RIGHT_NORMAL] = InstallColor(clrTPGroupCaptionBkRightNormal);
			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_RIGHT_HIGHLIGHTED] = InstallColor(clrTPGroupCaptionBkLeftNormal);

		} // if use WinXP themed colors
		else
		{ // if use system colors
			// install new colors
			m_mapColorTranslate[_2003CLR_GRADIENT_LIGHT] = InstallColor(CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_3DFACE, this), GetColor(COLOR_WINDOW, this), 222));
			m_mapColorTranslate[_2003CLR_GRADIENT_DARK] = COLOR_3DFACE;
			m_mapColorTranslate[_2003CLR_SEPARATOR_LIGHT] = InstallColor(RGB(255, 255, 255));
			m_mapColorTranslate[_2003CLR_SEPARATOR_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(GetColor(COLOR_WINDOW, this), 0.0, -0.36, -0.80));
			m_mapColorTranslate[_2003CLR_GRIPPER_DOT_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(GetColor(COLOR_3DSHADOW, this), 0.0, 0.20, 0.0));
			m_mapColorTranslate[_2003CLR_GRIPPER_DOT_LIGHT] = COLOR_WINDOW;
			m_mapColorTranslate[_2003CLR_EXPBTN_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(GetColor(COLOR_3DSHADOW, this), 0.0, 0.70, 0.10));
			m_mapColorTranslate[_2003CLR_EXPBTN_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(GetColor(COLOR_3DSHADOW, this), 0.0, -0.05, 0.0));
			m_mapColorTranslate[_2003CLR_EXPBTN_HOVER_LIGHT] = m_mapColorTranslate[_2003CLR_EXPBTN_HOVER_DARK] =
				InstallColor(CExtBitmap::stat_HLS_Adjust(GetColor(XPCLR_HILIGHT, this), 0.0, 0.30, 0.0));
			m_mapColorTranslate[_2003CLR_EXPBTN_PRESSED_LIGHT] = m_mapColorTranslate[_2003CLR_EXPBTN_PRESSED_DARK] =
				InstallColor(CExtBitmap::stat_HLS_Adjust(GetColor(XPCLR_HILIGHT, this), 0.0, 0.60, 0.0));
			m_mapColorTranslate[_2003CLR_EXPGLYPH_LIGHT] = COLOR_WINDOW;
			m_mapColorTranslate[_2003CLR_EXPGLYPH_DARK] = COLOR_BTNTEXT;
			m_mapColorTranslate[_2003CLR_STATUSBAR_ITEM] = COLOR_3DSHADOW;

			m_mapColorTranslate[_2003CLR_MLA_NORM_LEFT] = InstallColor(CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_WINDOW, this), GetColor(XPCLR_3DFACE_NORMAL, this), 0));
			m_mapColorTranslate[_2003CLR_MLA_NORM_MIDDLE] = InstallColor(CExtBitmap::stat_HLS_Adjust(GetColor(XPCLR_3DFACE_DARK, this), 0.00, 0.40, 0.00));
			m_mapColorTranslate[_2003CLR_MLA_NORM_RIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(GetColor(XPCLR_3DFACE_DARK, this), 0.00, -0.04, 0.00));
			m_mapColorTranslate[_2003CLR_MLA_RARELY_LEFT] = InstallColor(CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_WINDOW, this), GetColor(XPCLR_3DFACE_DARK, this), 500));
			m_mapColorTranslate[_2003CLR_MLA_RARELY_MIDDLE] = InstallColor(CExtBitmap::stat_HLS_Adjust(GetColor(_2003CLR_MLA_NORM_RIGHT, this), 0.00, 0.30, 0.00));
			m_mapColorTranslate[_2003CLR_MLA_RARELY_RIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(GetColor(_2003CLR_MLA_NORM_RIGHT, this), 0.00, -0.04, 0.00));

			m_mapColorTranslate[_2003CLR_TBB_BK_COMBINED_TOP] = m_mapColorTranslate[_2003CLR_MLA_NORM_LEFT];
			m_mapColorTranslate[_2003CLR_TBB_BK_COMBINED_BOTTOM] = m_mapColorTranslate[_2003CLR_MLA_NORM_RIGHT];

			// Button colors
			m_mapColorTranslate[_2003CLR_BTN_HOVER_LEFT] = InstallColor(GetColor(CLR_3DFACE_IN, this));
			m_mapColorTranslate[_2003CLR_BTN_HOVER_RIGHT] = InstallColor(GetColor(CLR_3DFACE_IN, this));
			m_mapColorTranslate[_2003CLR_BTN_PRESSED_LEFT] = InstallColor(GetColor(CLR_3DFACE_IN, this));
			m_mapColorTranslate[_2003CLR_BTN_PRESSED_RIGHT] = InstallColor(GetColor(CLR_3DFACE_IN, this));
			m_mapColorTranslate[_2003CLR_BTN_HP_LEFT] = InstallColor(GetColor(XPCLR_HILIGHT, this));
			m_mapColorTranslate[_2003CLR_BTN_HP_RIGHT] = InstallColor(GetColor(XPCLR_HILIGHT, this));

			// page navigator colors
			m_mapColorTranslate[_2003CLR_PN_BORDER] = InstallColor(GetColor(COLOR_3DSHADOW, this));
			m_mapColorTranslate[_2003CLR_PN_GRIPPER_LIGHT] = InstallColor(GetColor(COLOR_3DLIGHT, this));
			m_mapColorTranslate[_2003CLR_PN_GRIPPER_DARK] = InstallColor(GetColor(COLOR_3DSHADOW, this));
			m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_LIGHT] = InstallColor(GetColor(COLOR_3DHILIGHT, this));
			m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_DARK] = InstallColor(GetColor(COLOR_3DLIGHT, this));
			m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_HOVER_LIGHT] = m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_LIGHT];
			m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_HOVER_DARK] = m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_DARK];
			m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_BOTTOM_LINE] = m_mapColorTranslate[_2003CLR_PN_BORDER];
			m_mapColorTranslate[_2003CLR_PN_ITEM_LIGHT] = InstallColor(GetColor(COLOR_3DHILIGHT, this));
			m_mapColorTranslate[_2003CLR_PN_ITEM_DARK] = InstallColor(GetColor(COLOR_3DLIGHT, this));

			// Popup menu Expand Button
			m_mapColorTranslate[_2003CLR_EXPBTN_CIRCLE_LIGHT] = InstallColor(GetColor(XPCLR_3DFACE_NORMAL, this));
			m_mapColorTranslate[_2003CLR_EXPBTN_CIRCLE_DARK] = InstallColor(CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_WINDOW, this), GetColor(COLOR_3DFACE, this), 550));

			// Task Pane Colors
			m_mapColorTranslate[CLR_TASK_PANE_BK_TOP] = InstallColor(CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_3DFACE, this), GetColor(COLOR_WINDOW, this), 50));
			m_mapColorTranslate[CLR_TASK_PANE_BK_BOTTOM] = InstallColor(GetColor(COLOR_3DFACE, this));
			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_NORMAL] = COLOR_WINDOWTEXT;
			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_NORMAL_HOVERED] = COLOR_WINDOWTEXT;
			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_HIGHLIGHTED] = m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_NORMAL];
			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_HIGHLIGHTED_HOVERED] = m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_NORMAL_HOVERED];

			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_LEFT_NORMAL] =
				InstallColor(CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_3DFACE, this), GetColor(COLOR_WINDOW, this), 39));
			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_RIGHT_NORMAL] =
				InstallColor(CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_3DFACE, this), GetColor(COLOR_WINDOW, this), 70));
			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_LEFT_HIGHLIGHTED] = m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_RIGHT_NORMAL];
			m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_RIGHT_HIGHLIGHTED] = m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_LEFT_NORMAL];
		} // if use system colors
	} // if( stat_GetBPP() > 8 )
	else
	{
		m_mapColorTranslate[_2003CLR_STATUSBAR_ITEM] = InstallColor(GetColor(COLOR_3DSHADOW, this));
	} // else from if( stat_GetBPP() > 8 )
}

//////////////////////////////////////////////////////////////////////////
// CExtPaintManagerStudio2005
//////////////////////////////////////////////////////////////////////////

CExtPaintManagerStudio2005::CExtPaintManagerStudio2005()
{
	m_nThemeIndex = 0;
}

CExtPaintManagerStudio2005::~CExtPaintManagerStudio2005()
{
}

////////////////////////////////////////////////////////////////
// 2005 Translate
void CExtPaintManagerStudio2005::InitTranslatedColors()
{
	ASSERT_VALID(this);

	CExtPaintManagerOffice2003::InitTranslatedColors();

	if (stat_GetBPP() > 8)
	{
		e_system_theme_t eCurrentTheme = OnQuerySystemTheme();
		if (OnQueryUseThemeColors())
		{
			// if use WinXP themed colors
			COLORREF clrFillHint, clrAccentHint;
			OnQueryThemeColors(&clrFillHint, &clrAccentHint);

			// re-install XP colors
			// xp - menu area light ver, menu items background color
			COLORREF xpclr_MenuLight = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.95, -0.05);
			xpclr_MenuLight = RGB(
				GetRValue(xpclr_MenuLight) + ::MulDiv(255 - GetRValue(xpclr_MenuLight), 40, 100),
				GetGValue(xpclr_MenuLight) + ::MulDiv(255 - GetGValue(xpclr_MenuLight), 20, 100),
				GetBValue(xpclr_MenuLight));

			// 2003 - float light
			COLORREF xpclr_FloatFace = clrFillHint;

			// xp - rarely used item left side
			COLORREF xpclr_RarelyUsedMenuLeft = RGB(
				(999L * long(GetRValue(clrFillHint))) / 1000L,
				(995L * long(GetGValue(clrFillHint))) / 1000L,
				(995L * long(GetBValue(clrFillHint))) / 1000L);

			// xp - control bar background
			COLORREF xpclr_ControlBarBk = clrFillHint;

			// xp - blue like light ver
			// was .0 .7 .0
			COLORREF xpclr_Highlight = CExtBitmap::stat_HLS_Adjust(clrAccentHint, 0.00, 0.70, 0.00);
			// xp - blue like dark ver
			COLORREF xpclr_HighlightDarked = CExtBitmap::stat_HLS_Adjust(clrAccentHint, 0.00, 0.50, 0.00);
			// xp - blue dark selected border
			COLORREF xpclr_HighlightBorder = clrAccentHint;

			// xp - dark gray separator
			COLORREF xpclr_Separator = RGB(
				(857L * long(GetRValue(clrFillHint))) / 1000L,
				(857L * long(GetGValue(clrFillHint))) / 1000L,
				(857L * long(GetBValue(clrFillHint))) / 1000L);

			// xp - dark panel border (for floating controlbars)
			COLORREF xpclr_PanelBorder = xpclr_MenuLight;

			if (eCurrentTheme != ThemeLunaSilver)
			{
				m_mapColorTranslate[CLR_3DFACE_OUT] = InstallColor(xpclr_ControlBarBk);
				m_mapColorTranslate[CLR_3DFACE_IN] = InstallColor(xpclr_Highlight);
				m_mapColorTranslate[CLR_3DFACE_DISABLED] = COLOR_3DFACE;

				int idxClrDark = InstallColor(xpclr_PanelBorder);

				m_mapColorTranslate[CLR_3DLIGHT_OUT] = idxClrDark;
				m_mapColorTranslate[CLR_3DLIGHT_IN] = idxClrDark;
				m_mapColorTranslate[CLR_3DLIGHT_DISABLED] = idxClrDark;

				m_mapColorTranslate[CLR_3DHILIGHT_OUT] = idxClrDark;
				m_mapColorTranslate[CLR_3DHILIGHT_IN] = idxClrDark;
				m_mapColorTranslate[CLR_3DHILIGHT_DISABLED] = idxClrDark;

				m_mapColorTranslate[CLR_3DDKSHADOW_OUT] = idxClrDark;
				m_mapColorTranslate[CLR_3DDKSHADOW_IN] = idxClrDark;
				m_mapColorTranslate[CLR_3DDKSHADOW_DISABLED] = idxClrDark;

				m_mapColorTranslate[CLR_TEXT_OUT] = COLOR_WINDOWTEXT;
				m_mapColorTranslate[CLR_TEXT_IN] = COLOR_WINDOWTEXT;
				m_mapColorTranslate[CLR_TEXT_DISABLED] = COLOR_GRAYTEXT;

				m_mapColorTranslate[XPCLR_TEXT_FIELD_BORDER_NORMAL] = COLOR_3DFACE;
				m_mapColorTranslate[XPCLR_TEXT_FIELD_BORDER_DISABLED] = InstallColor(xpclr_Separator);

				m_mapColorTranslate[XPCLR_PUSHEDHOVERTEXT] = COLOR_BTNTEXT;

				m_mapColorTranslate[XPCLR_3DFACE_DARK] = InstallColor(xpclr_ControlBarBk);
				m_mapColorTranslate[XPCLR_3DFACE_NORMAL] = InstallColor(xpclr_MenuLight);
				m_mapColorTranslate[XPCLR_SEPARATOR] = InstallColor(xpclr_Separator);
				m_mapColorTranslate[XPCLR_HILIGHT] = InstallColor(xpclr_HighlightDarked);
				m_mapColorTranslate[XPCLR_HILIGHT_BORDER] = InstallColor(xpclr_HighlightBorder);
				m_mapColorTranslate[XPCLR_HILIGHT_BORDER_SELECTED] = InstallColor(xpclr_HighlightBorder);
				m_mapColorTranslate[XPCLR_RARELY_BORDER] = InstallColor(xpclr_RarelyUsedMenuLeft);
				m_mapColorTranslate[XPCLR_3DFACE_FLOAT_F] = InstallColor(xpclr_FloatFace);

				// install new colors
				m_mapColorTranslate[_2003CLR_GRADIENT_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.80, 0.00));
				m_mapColorTranslate[_2003CLR_GRADIENT_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.15, 0.00));
				m_mapColorTranslate[_2003CLR_SEPARATOR_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.98, 0.00));
				m_mapColorTranslate[_2003CLR_SEPARATOR_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.40, 0.00));
				m_mapColorTranslate[_2003CLR_GRIPPER_DOT_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.20, 0.00));
				m_mapColorTranslate[_2003CLR_GRIPPER_DOT_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.95, 0.00));
				m_mapColorTranslate[_2003CLR_EXPBTN_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.60, 0.00));
				m_mapColorTranslate[_2003CLR_EXPBTN_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.40, -0.40));
				m_mapColorTranslate[_2003CLR_EXPBTN_HOVER_LIGHT] = InstallColor(xpclr_Highlight);
				m_mapColorTranslate[_2003CLR_EXPBTN_HOVER_DARK] = InstallColor(xpclr_Highlight);
				m_mapColorTranslate[_2003CLR_EXPBTN_PRESSED_LIGHT] = InstallColor(xpclr_HighlightDarked);
				m_mapColorTranslate[_2003CLR_EXPBTN_PRESSED_DARK] = InstallColor(xpclr_HighlightDarked);
				m_mapColorTranslate[_2003CLR_EXPGLYPH_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.97, 0.00));
				m_mapColorTranslate[_2003CLR_EXPGLYPH_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.97, 0.00));
				m_mapColorTranslate[_2003CLR_STATUSBAR_ITEM] = COLOR_3DSHADOW;

				COLORREF clrTbGradientLight = (COLORREF) (-1L);
				COLORREF clrTbGradientMiddle = (COLORREF) (-1L);
				COLORREF clrTbGradientDark = (COLORREF) (-1L);
				COLORREF clrTbBottomLine = (COLORREF) (-1L);
				COLORREF clrMlaNormLeft = (COLORREF) (-1L);
				COLORREF clrMlaNormMiddle = (COLORREF) (-1L);
				COLORREF clrMlaNormRight = (COLORREF) (-1L);
				COLORREF clrMlaRarelyLeft = (COLORREF) (-1L);
				COLORREF clrMlaRarelyMiddle = (COLORREF) (-1L);
				COLORREF clrMlaRarelyRight = (COLORREF) (-1L);
				COLORREF clrMenuBorder = (COLORREF) (-1L);
				COLORREF clrTbbBkTop = (COLORREF) (-1L);
				COLORREF clrTbbBkBottom = (COLORREF) (-1L);

				switch (eCurrentTheme)
				{
				case ThemeLunaRoyale:       // +2.87
					//	case ThemeVistaOrLaterUX:   // +2.87 -2.88
					//	case ThemeVistaOrLaterDWM:  // +2.87 -2.88
				case ThemeLunaBlue:
				case ThemeLunaOlive:
					clrTbGradientLight = RGB(250, 249, 245);
					clrTbGradientMiddle = RGB(235, 231, 224);
					clrTbGradientDark = RGB(180, 182, 153);
					clrTbBottomLine = RGB(163, 163, 124);
					clrMlaNormLeft = RGB(252, 252, 249);
					clrMlaNormMiddle = RGB(246, 244, 236);
					clrMlaNormRight = RGB(186, 186, 160);
					clrMlaRarelyLeft = RGB(245, 245, 241);
					clrMlaRarelyMiddle = RGB(222, 222, 209);
					clrMlaRarelyRight = RGB(165, 165, 127);
					clrMenuBorder = RGB(138, 134, 122);
					clrTbbBkTop = RGB(251, 251, 249);
					clrTbbBkBottom = RGB(247, 245, 239);
					break;
				case ThemeLunaSilver:
					ASSERT(FALSE);
					break;
				case ThemeVistaOrLaterUX:   // +2.88
				case ThemeVistaOrLaterDWM:  // +2.88
					m_mapColorTranslate[_2003CLR_EXPBTN_LIGHT] = InstallColor(RGB(236, 236, 236));
					m_mapColorTranslate[_2003CLR_EXPBTN_DARK] = InstallColor(RGB(160, 160, 160));
					m_mapColorTranslate[_2003CLR_GRADIENT_LIGHT] = InstallColor(RGB(251, 251, 251));
					m_mapColorTranslate[_2003CLR_GRADIENT_DARK] = InstallColor(RGB(240, 240, 240));
					clrTbGradientLight = RGB(251, 251, 251);
					clrTbGradientMiddle = RGB(244, 244, 244);
					clrTbGradientDark = RGB(242, 242, 242);
					clrTbBottomLine = RGB(242, 242, 242);
					clrMlaNormLeft = RGB(252, 252, 252);
					clrMlaNormRight = RGB(241, 241, 241);
					clrMlaNormMiddle = RGB(248, 248, 248);
					clrMlaRarelyLeft = CExtBitmap::stat_HLS_Adjust(clrMlaNormLeft, 0.00, -0.10, 0.00);
					clrMlaRarelyRight = CExtBitmap::stat_HLS_Adjust(clrMlaNormRight, 0.00, -0.10, 0.00);
					clrMlaRarelyMiddle = CExtBitmap::stat_HLS_Adjust(clrMlaNormMiddle, 0.00, -0.10, 0.00);
					clrMenuBorder = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.50, 0.00);
					clrTbbBkTop = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.98, 0.00);
					clrTbbBkBottom = CExtBitmap::stat_HLS_Adjust(GetColor(XPCLR_3DFACE_DARK, this), 0.00, -0.20, -0.10);
					break;
				default:
					clrTbGradientLight = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.75, 0.15);
					clrTbGradientMiddle = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.65, 0.15);
					clrTbGradientDark = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.22, -0.25);
					clrTbBottomLine = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.60, 0.00);
					clrMlaNormLeft = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.98, 0.00);
					clrMlaNormRight = CExtBitmap::stat_HLS_Adjust(GetColor(XPCLR_3DFACE_DARK, this), 0.00, -0.20, -0.10);
					clrMlaNormMiddle = CExtBitmap::stat_HLS_Adjust(clrMlaNormRight, 0.00, 0.55, 0.00);
					clrMlaRarelyLeft = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.98, 0.00);
					clrMlaRarelyRight = CExtBitmap::stat_HLS_Adjust(GetColor(XPCLR_3DFACE_DARK, this), 0.00, -0.40, -0.10);
					clrMlaRarelyMiddle = CExtBitmap::stat_HLS_Adjust(clrMlaRarelyRight, 0.00, 0.55, 0.00);
					clrMenuBorder = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.50, 0.00);
					clrTbbBkTop = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.98, 0.00);
					clrTbbBkBottom = CExtBitmap::stat_HLS_Adjust(GetColor(XPCLR_3DFACE_DARK, this), 0.00, -0.20, -0.10);
					break;
				} // switch( eCurrentTheme )

				m_mapColorTranslate[_2003CLR_TBB_BK_COMBINED_TOP] = InstallColor(clrTbbBkTop);
				m_mapColorTranslate[_2003CLR_TBB_BK_COMBINED_BOTTOM] = InstallColor(clrTbbBkBottom);

				m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_LIGHT] = InstallColor(clrTbGradientLight);
				m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_MIDDLE] = InstallColor(clrTbGradientMiddle);
				m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_DARK] = InstallColor(clrTbGradientDark);
				m_mapColorTranslate[_2003CLR_TOOLBAR_BOTTOM_LINE] = InstallColor(clrTbBottomLine);

				m_mapColorTranslate[_2003CLR_MLA_NORM_LEFT] = InstallColor(clrMlaNormLeft);
				m_mapColorTranslate[_2003CLR_MLA_NORM_MIDDLE] = InstallColor(clrMlaNormMiddle);
				m_mapColorTranslate[_2003CLR_MLA_NORM_RIGHT] = InstallColor(clrMlaNormRight);

				m_mapColorTranslate[_2003CLR_MLA_RARELY_LEFT] = InstallColor(clrMlaRarelyLeft);
				m_mapColorTranslate[_2003CLR_MLA_RARELY_MIDDLE] = InstallColor(clrMlaRarelyMiddle);
				m_mapColorTranslate[_2003CLR_MLA_RARELY_RIGHT] = InstallColor(clrMlaRarelyRight);

				m_mapColorTranslate[XPCLR_MENU_BORDER] = InstallColor(clrMenuBorder);

				m_mapColorTranslate[_2003CLR_BTN_HOVER_LEFT] = InstallColor(xpclr_Highlight);
				m_mapColorTranslate[_2003CLR_BTN_HOVER_RIGHT] = InstallColor(xpclr_Highlight);

				m_mapColorTranslate[_2003CLR_BTN_PRESSED_LEFT] = InstallColor(xpclr_HighlightDarked);
				m_mapColorTranslate[_2003CLR_BTN_PRESSED_RIGHT] = InstallColor(xpclr_HighlightDarked);

				m_mapColorTranslate[_2003CLR_BTN_HP_LEFT] = InstallColor(xpclr_HighlightDarked);
				m_mapColorTranslate[_2003CLR_BTN_HP_RIGHT] = InstallColor(xpclr_HighlightDarked);

				// page navigator colors
				m_mapColorTranslate[_2003CLR_PN_BORDER] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.10, -0.55, 0.00));
				m_mapColorTranslate[_2003CLR_PN_GRIPPER_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.02, -0.23, -0.25));
				m_mapColorTranslate[_2003CLR_PN_GRIPPER_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.05, -0.60, 0.00));
				m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.45, 0.00));
				m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.00, 0.00));
				m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_HOVER_LIGHT] = m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_LIGHT];
				m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_HOVER_DARK] = m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_DARK];
				m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_BOTTOM_LINE] = m_mapColorTranslate[_2003CLR_PN_BORDER];
				m_mapColorTranslate[_2003CLR_PN_ITEM_LIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.55, 0.20));
				m_mapColorTranslate[_2003CLR_PN_ITEM_DARK] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.13, -0.15));

				// Popup menu Expand Button
				m_mapColorTranslate[_2003CLR_EXPBTN_CIRCLE_LIGHT] = InstallColor(GetColor(_2003CLR_MLA_NORM_LEFT, this));
				m_mapColorTranslate[_2003CLR_EXPBTN_CIRCLE_DARK] = InstallColor(GetColor(_2003CLR_MLA_NORM_RIGHT, this));

				m_nIdxClrBtnHoverLeft = _2003CLR_BTN_HOVER_LEFT;
				m_nIdxClrBtnHoverRight = _2003CLR_BTN_HOVER_RIGHT;
				m_nIdxClrBtnPressedLeft = _2003CLR_BTN_PRESSED_LEFT;
				m_nIdxClrBtnPressedRight = _2003CLR_BTN_PRESSED_RIGHT;
				m_nIdxClrBtnHovPresLeft = _2003CLR_BTN_HP_LEFT;
				m_nIdxClrBtnHovPresRight = _2003CLR_BTN_HP_RIGHT;
				m_nIdxClrTbFillMargin = _2003CLR_EXPBTN_DARK;

				m_colors[COLOR_3DFACE] = clrFillHint;
				m_colors[COLOR_3DLIGHT] = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.10, 0.00);
				m_colors[COLOR_3DHIGHLIGHT] = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.95, 0.00);
				m_colors[COLOR_3DSHADOW] = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.30, -0.50);
				m_colors[COLOR_3DDKSHADOW] = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.70, -0.50);
//				m_colors[CLR_WRB_FRAME] = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.20, -0.70, -0.50);

			} // if( eCurrentTheme != ThemeLunaSilver ....

			COLORREF clrBarCaptionInactive = (COLORREF) (-1L);
			COLORREF clrBarCaptionActiveTop = (COLORREF) (-1L);
			COLORREF clrBarCaptionActiveBottom = (COLORREF) (-1L);
			switch (eCurrentTheme)
			{
			case ThemeLunaRoyale:       // +2.87
				//	case ThemeVistaOrLaterUX:   // +2.87 -2.88
				//	case ThemeVistaOrLaterDWM:  // +2.87 -2.88
			case ThemeLunaBlue:
				clrBarCaptionInactive = RGB(204, 199, 186);
				clrBarCaptionActiveTop = RGB(59, 128, 237);
				clrBarCaptionActiveBottom = RGB(49, 106, 197);
				break;
			case ThemeLunaOlive:
				clrBarCaptionInactive = RGB(204, 199, 186);
				clrBarCaptionActiveTop = RGB(182, 195, 146);
				clrBarCaptionActiveBottom = RGB(145, 160, 117);
				break;
			case ThemeLunaSilver:
				clrBarCaptionInactive = RGB(240, 240, 245);
				clrBarCaptionActiveTop = RGB(211, 212, 221);
				clrBarCaptionActiveBottom = RGB(166, 165, 191);
				break;
			case ThemeVistaOrLaterUX:   // +2.88
			case ThemeVistaOrLaterDWM:  // +2.88
			default:
				clrBarCaptionInactive = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.80, 0.00);
				clrBarCaptionActiveTop = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, 0.40, 0.00);
				clrBarCaptionActiveBottom = CExtBitmap::stat_HLS_Adjust(clrFillHint, 0.00, -0.15, 0.00);
				break;
			} // switch( eCurrentTheme )
			m_mapColorTranslate[_STUDIO_2005CLR_BAR_CAPTION_INACTIVE] = InstallColor(clrBarCaptionInactive);
			m_mapColorTranslate[_STUDIO_2005CLR_BAR_CAPTION_ACTIVE_TOP] = InstallColor(clrBarCaptionActiveTop);
			m_mapColorTranslate[_STUDIO_2005CLR_BAR_CAPTION_ACTIVE_BOTTOM] = InstallColor(clrBarCaptionActiveBottom);

		} // if use WinXP themed colors
		else
		{
			m_mapColorTranslate[_STUDIO_2005CLR_BAR_CAPTION_INACTIVE] = COLOR_INACTIVECAPTION;
			m_mapColorTranslate[_STUDIO_2005CLR_BAR_CAPTION_ACTIVE_TOP] = COLOR_ACTIVECAPTION;
			m_mapColorTranslate[_STUDIO_2005CLR_BAR_CAPTION_ACTIVE_BOTTOM] = COLOR_ACTIVECAPTION;
		}

		// Task Pane Colors
		m_mapColorTranslate[CLR_TASK_PANE_BK_TOP] = InstallColor(CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_3DFACE, this), GetColor(COLOR_WINDOW, this), 50));
		m_mapColorTranslate[CLR_TASK_PANE_BK_BOTTOM] = InstallColor(CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_3DSHADOW, this), GetColor(COLOR_WINDOW, this), 60));
		m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_NORMAL] = InstallColor(GetColor(COLOR_WINDOWTEXT, this));
		m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_NORMAL_HOVERED] = InstallColor(GetColor(COLOR_WINDOWTEXT, this));
		m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_HIGHLIGHTED] = m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_NORMAL];
		m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_HIGHLIGHTED_HOVERED] = m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_TEXT_NORMAL_HOVERED];
		m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_LEFT_NORMAL] = InstallColor(CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_3DFACE, this), GetColor(COLOR_WINDOW, this), 39));
		m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_RIGHT_NORMAL] = InstallColor(CExtBitmap::stat_RGB_Enlight(GetColor(COLOR_3DSHADOW, this), GetColor(COLOR_WINDOW, this), 60));
		m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_LEFT_HIGHLIGHTED] = m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_RIGHT_NORMAL];
		m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_RIGHT_HIGHLIGHTED] = m_mapColorTranslate[CLR_TASK_PANE_GROUP_CAPTION_BK_LEFT_NORMAL];
	} // if( stat_GetBPP() > 8 )
}

//////////////////////////////////////////////////////////////////////////
// CExtPaintManagerStudio2008
//////////////////////////////////////////////////////////////////////////

CExtPaintManagerStudio2008::CExtPaintManagerStudio2008()
{
	VERIFY(
		m_bmpMenuItemLunaBlue.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_VS2008_MENU_ITEM_LUNA_BLUE)
		)
		);
	m_bmpMenuItemLunaBlue.Make32();
	m_bmpMenuItemLunaBlue.AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), BYTE(0));

	VERIFY(
		m_bmpMenuItemLunaOliveGreen.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_VS2008_MENU_ITEM_LUNA_OLIVE_GREEN)
		)
		);
	m_bmpMenuItemLunaOliveGreen.Make32();
	m_bmpMenuItemLunaOliveGreen.AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), BYTE(0));

	VERIFY(
		m_bmpMenuItemLunaSilver.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_VS2008_MENU_ITEM_LUNA_SILVER)
		)
		);
	m_bmpMenuItemLunaSilver.Make32();
	m_bmpMenuItemLunaSilver.AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), BYTE(0));

	VERIFY(
		m_bmpMenuItemLunaRoyale.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_VS2008_MENU_ITEM_LUNA_ROYALE)
		)
		);
	m_bmpMenuItemLunaRoyale.Make32();
	m_bmpMenuItemLunaRoyale.AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), BYTE(0));

	VERIFY(
		m_bmpMenuItemVista.LoadBMP_Resource(
		MAKEINTRESOURCE(IDB_EXT_VS2008_MENU_ITEM_VISTA)
		)
		);
	m_bmpMenuItemVista.Make32();
	m_bmpMenuItemVista.AlphaColor(RGB(255, 0, 255), RGB(0, 0, 0), BYTE(0));
	m_nThemeIndex = 0;
}

CExtPaintManagerStudio2008::~CExtPaintManagerStudio2008()
{
}

void CExtPaintManagerStudio2008::InitTranslatedColors()
{
	ASSERT_VALID(this);
	CExtPaintManagerStudio2005::InitTranslatedColors();
	VERIFY(m_bmpMenuItemOther.LoadBMP_Resource(MAKEINTRESOURCE(IDB_EXT_VS2008_MENU_ITEM_OTHER)));
	if (stat_GetBPP() > 8)
	{
		COLORREF clrMlaNormLeft = COLORREF(-1L);
		COLORREF clrMlaNormMiddle = COLORREF(-1L);
		COLORREF clrMlaNormRight = COLORREF(-1L);
		COLORREF clrMlaMarginLeft = COLORREF(-1L);
		COLORREF clrMlaMarginRight = COLORREF(-1L);
		COLORREF clrMlaRarelyLeft = COLORREF(-1L);
		COLORREF clrMlaRarelyRight = COLORREF(-1L);
		COLORREF clrMlaRarelyMiddle = COLORREF(-1L);
		e_system_theme_t eCurrentTheme = OnQuerySystemTheme();
		switch (eCurrentTheme)
		{
			//	case ThemeVistaOrLaterUX:   // +2.87 -2.88
			//	case ThemeVistaOrLaterDWM:  // +2.87 -2.88
		case ThemeLunaBlue:
			clrMlaNormLeft = RGB(241, 241, 241);
			clrMlaNormMiddle = RGB(241, 241, 241);
			clrMlaNormRight = RGB(241, 241, 241);
			clrMlaMarginLeft = RGB(197, 194, 184);
			clrMlaMarginRight = RGB(255, 255, 255);
			break;
		case ThemeLunaOlive:
			clrMlaNormLeft = RGB(247, 246, 239);
			clrMlaNormMiddle = RGB(242, 240, 229);
			clrMlaNormRight = RGB(233, 230, 214);
			clrMlaMarginLeft = RGB(197, 194, 184);
			clrMlaMarginRight = RGB(255, 255, 255);
			break;
		case ThemeVistaOrLaterUX:   // +2.88
		case ThemeVistaOrLaterDWM:  // +2.88
			m_mapColorTranslate[_2003CLR_EXPBTN_LIGHT] = InstallColor(RGB(195, 202, 218));
			m_mapColorTranslate[_2003CLR_EXPBTN_DARK] = InstallColor(RGB(233, 236, 250));
			m_mapColorTranslate[_2003CLR_GRADIENT_LIGHT] = InstallColor(RGB(244, 247, 252));
			m_mapColorTranslate[_2003CLR_GRADIENT_DARK] = InstallColor(RGB(233, 236, 250));
			clrMlaNormLeft = RGB(243, 245, 248);
			clrMlaNormRight = RGB(213, 217, 231);
			clrMlaNormMiddle = RGB(233, 236, 250);
			clrMlaRarelyLeft = CExtBitmap::stat_HLS_Adjust(clrMlaNormLeft, 0.00, -0.10, 0.00);
			clrMlaRarelyRight = CExtBitmap::stat_HLS_Adjust(clrMlaNormRight, 0.00, -0.10, 0.00);
			clrMlaRarelyMiddle = CExtBitmap::stat_HLS_Adjust(clrMlaNormMiddle, 0.00, -0.10, 0.00);
			clrMlaMarginLeft = RGB(170, 171, 190);
			clrMlaMarginRight = RGB(252, 252, 252);
			m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_LIGHT] = InstallColor(RGB(250, 250, 253));
			m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_MIDDLE] = InstallColor(RGB(223, 227, 241));
			m_mapColorTranslate[_2003CLR_TOOLBAR_GRADIENT_DARK] = InstallColor(RGB(196, 203, 219));
			m_mapColorTranslate[_2003CLR_TOOLBAR_BOTTOM_LINE] = InstallColor(RGB(181, 190, 206));
			break;
		case ThemeLunaSilver:
			clrMlaNormLeft = RGB(232, 233, 242);
			clrMlaNormMiddle = RGB(216, 216, 227);
			clrMlaNormRight = RGB(193, 193, 211);
			clrMlaMarginLeft = RGB(110, 109, 143);
			clrMlaMarginRight = RGB(255, 255, 255);
			break;
		case ThemeLunaRoyale:
			clrMlaNormLeft = RGB(247, 246, 248);
			clrMlaNormMiddle = RGB(241, 240, 242);
			clrMlaNormRight = RGB(241, 240, 242);
			clrMlaMarginLeft = RGB(193, 193, 196);
			clrMlaMarginRight = RGB(255, 255, 255);
			break;
		default:
			clrMlaMarginLeft = GetColor(COLOR_3DSHADOW, this);
			clrMlaMarginRight = GetColor(COLOR_WINDOW, this);
			break;
		} // switch( eCurrentTheme )
		if (clrMlaNormLeft != COLORREF(-1L))
		{
			m_mapColorTranslate[_2003CLR_MLA_NORM_LEFT] = InstallColor(clrMlaNormLeft);
			m_mapColorTranslate[_2003CLR_MLA_RARELY_LEFT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrMlaNormLeft, 0.00, -0.10, 0.0));
		}
		if (clrMlaNormMiddle != COLORREF(-1L))
		{
			m_mapColorTranslate[_2003CLR_MLA_NORM_MIDDLE] = InstallColor(clrMlaNormMiddle);
			m_mapColorTranslate[_2003CLR_MLA_RARELY_MIDDLE] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrMlaNormMiddle, 0.00, -0.10, 0.0));
		}
		if (clrMlaNormRight != COLORREF(-1L))
		{
			m_mapColorTranslate[_2003CLR_MLA_NORM_RIGHT] = InstallColor(clrMlaNormRight);
			m_mapColorTranslate[_2003CLR_MLA_RARELY_RIGHT] = InstallColor(CExtBitmap::stat_HLS_Adjust(clrMlaNormRight, 0.00, -0.10, 0.0));
		}
		m_mapColorTranslate[_STUDIO_2008CLR_MENU_ITEM_LEFT_AREA_MARGIN_LEFT] = InstallColor(clrMlaMarginLeft);
		m_mapColorTranslate[_STUDIO_2008CLR_MENU_ITEM_LEFT_AREA_MARGIN_RIGHT] = InstallColor(clrMlaMarginRight);
		if (clrMlaRarelyLeft != COLORREF(-1L))
			m_mapColorTranslate[_2003CLR_MLA_RARELY_LEFT] = InstallColor(clrMlaRarelyLeft);
		if (clrMlaRarelyMiddle != COLORREF(-1L))
			m_mapColorTranslate[_2003CLR_MLA_RARELY_MIDDLE] = InstallColor(clrMlaRarelyMiddle);
		if (clrMlaRarelyRight != COLORREF(-1L))
			m_mapColorTranslate[_2003CLR_MLA_RARELY_RIGHT] = InstallColor(clrMlaRarelyRight);

		// colorize menu item selection bitmap
		if ((!g_PaintManager.m_bIsWinVistaOrLater)
			&& eCurrentTheme == ThemeUnknown
			)
		{
			m_mapColorTranslate[CLR_MENUTEXT_IN] = COLOR_HIGHLIGHTTEXT;
			COLORREF clrHighlight = GetColor(COLOR_HIGHLIGHT, this);
			BYTE nRed = BYTE(GetRValue(clrHighlight));
			BYTE nGreen = BYTE(GetGValue(clrHighlight));
			BYTE nBlue = BYTE(GetBValue(clrHighlight));
			CSize _sizeBmp = m_bmpMenuItemOther.GetSize();
			INT nX, nY;
			for (nY = 0; nY < _sizeBmp.cy; nY++)
			{
				for (nX = 0; nX < _sizeBmp.cx; nX++)
				{
					RGBQUAD _pixel;
					m_bmpMenuItemOther.GetPixel(nX, nY, _pixel);
					_pixel.rgbRed = nRed;
					_pixel.rgbGreen = nGreen;
					_pixel.rgbBlue = nBlue;
					m_bmpMenuItemOther.SetPixel(nX, nY, _pixel);
				}
			}
		}
		else
			m_mapColorTranslate[CLR_MENUTEXT_IN] = COLOR_WINDOWTEXT;
	} // if( stat_GetBPP() > 8 )
	else
	{
		m_mapColorTranslate[_STUDIO_2008CLR_MENU_ITEM_LEFT_AREA_MARGIN_LEFT] = COLOR_3DSHADOW;
		m_mapColorTranslate[_STUDIO_2008CLR_MENU_ITEM_LEFT_AREA_MARGIN_RIGHT] = COLOR_WINDOW;
	} // else from if( stat_GetBPP() > 8 )
}

// this structure is different in that only one translatis used
//////////////////////////////////////////////////////////////////////////
// CExtPaintManagerStudio2010
//////////////////////////////////////////////////////////////////////////
CExtPaintManagerStudio2010::CExtPaintManagerStudio2010()
{
	m_nVisualStudioThemeIndex = enumVisualStudio2010;
	m_nThemeIndex = 0;
	ConstructorTranslateColors(m_nVisualStudioThemeIndex);
}

CExtPaintManagerStudio2010::~CExtPaintManagerStudio2010()
{
}

void CExtPaintManagerStudio2010::InitTranslatedColors()
{
	CExtPaintManagerStudio2010::InitTranslatedColorsIndex();
}

//////////////////////////////////////////////////////////////////////////
// CExtPaintManagerStudio2012
//////////////////////////////////////////////////////////////////////////

CExtPaintManagerStudio2012::CExtPaintManagerStudio2012()
{
	m_nThemeIndex = IndexMapper4(Studio2012, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerStudio2012::~CExtPaintManagerStudio2012()
{

}

void CExtPaintManagerStudio2012::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerStudio2012::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

//////////////////////////////////////////////////////////////
// Studio 2013
//////////////////////////////////////////////////////////////
	CExtPaintManagerStudio2013::CExtPaintManagerStudio2013()
	{
		m_nThemeIndex = IndexMapper4(Studio2013, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
		ConstructorTranslateColors(m_nThemeIndex);
	}

	CExtPaintManagerStudio2013::~CExtPaintManagerStudio2013()
	{

	}

	void CExtPaintManagerStudio2013::ConstructorTranslateColors(int nIndex)
	{
		CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
	}

	void CExtPaintManagerStudio2013::InitTranslatedColors(void)
	{
		CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
	}

//////////////////////////////////////////////////////////////
// Studio Light 2015
//////////////////////////////////////////////////////////////
CExtPaintManagerStudioLight2015::CExtPaintManagerStudioLight2015()
{
	m_nThemeIndex = IndexMapper4(StudioLight2015, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerStudioLight2015::~CExtPaintManagerStudioLight2015()
{

}

void CExtPaintManagerStudioLight2015::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerStudioLight2015::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}

//////////////////////////////////////////////////////////////
// Studio Dark 2015
//////////////////////////////////////////////////////////////
CExtPaintManagerStudioDark2015::CExtPaintManagerStudioDark2015()
{
	m_nThemeIndex = IndexMapper4(StudioDark2015, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerStudioDark2015::~CExtPaintManagerStudioDark2015()
{

}

void CExtPaintManagerStudioDark2015::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerStudioDark2015::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}


//////////////////////////////////////////////////////////////
// Studio Blue 2015
//////////////////////////////////////////////////////////////
CExtPaintManagerStudioBlue2015::CExtPaintManagerStudioBlue2015()
{
	m_nThemeIndex = IndexMapper4(StudioBlue2015, &m_sImageElement, &m_sNCImageElement, &m_sNCINAImageElement);
	ConstructorTranslateColors(m_nThemeIndex);
}

CExtPaintManagerStudioBlue2015::~CExtPaintManagerStudioBlue2015()
{

}

void CExtPaintManagerStudioBlue2015::ConstructorTranslateColors(int nIndex)
{
	CExtPaintManagerOffice2010_Impl::ConstructorTranslateColors(nIndex);
}

void CExtPaintManagerStudioBlue2015::InitTranslatedColors(void)
{
	CExtPaintManagerOffice2010_Impl::RunTimeTranslateColors();
}


// this is a the worker functions
void CExtPaintManagerStudio2010::ConstructorTranslateColors(int nIndex)
{
	memset((char *) &m_sImageElement, 0, sizeof(sImageElement));
	memset((char *) &m_sNCImageElement, 0, sizeof(sImageElement));

	switch (nIndex){
	case 0:		// VS 2010
		break;
	case 1: // VS 2012
		// this has blue hightlight colors
		m_sImageElement.MapInst[0].nFlag = EXT_MFC_HSL_LIGHT;
		m_sImageElement.MapInst[0].nTargetHue = 151;
		m_sImageElement.MapInst[0].dHDistance = 20;
		m_sImageElement.MapInst[0].nTargetLuminosity = 190;
		m_sImageElement.MapInst[0].dLDistance = 65;
		m_sImageElement.MapInst[0].nDeltaLuminosity = 30;
		break;
	case 2:  // VS 2013
		// 2010 rgb 189 199 215
		// 2010 h 154 S 63 L 202
		// 2013 rgb 214 219 233
		// 2013 hsl 159, 77, 224
		m_sImageElement.MapInst[0].nFlag = EXT_MFC_HSL_HUE;
		m_sImageElement.MapInst[0].nTargetHue = 154;
		m_sImageElement.MapInst[0].dHDistance = 35;
		m_sImageElement.MapInst[0].nTargetLuminosity = 202;
		m_sImageElement.MapInst[0].dLDistance = 65;
		m_sImageElement.MapInst[0].nDeltaHue = 4;
		m_sImageElement.MapInst[0].nDeltaSaturation = 14;
		m_sImageElement.MapInst[0].nDeltaLuminosity = 20;	
		break;
	case 3:	// VS 2015 blue to grey to Light
		m_sImageElement.MapInst[0].nFlag = EXT_MFC_HSL_HUE;
		m_sImageElement.MapInst[0].nTargetHue = 151;
		m_sImageElement.MapInst[0].dHDistance = 20;
		m_sImageElement.MapInst[0].nTargetLuminosity = 190;
		m_sImageElement.MapInst[0].dLDistance = 65;
		m_sImageElement.MapInst[0].nDeltaSaturation = -30;
		m_sImageElement.MapInst[0].nDeltaLuminosity = 60;	
		break;
	case 4:	// VS 2015 Dark
		m_sImageElement.MapInst[0].nFlag = EXT_MFC_HSL_LIGHT;
		m_sImageElement.MapInst[0].nTargetHue = 151;
		m_sImageElement.MapInst[0].dHDistance = 20;
		m_sImageElement.MapInst[0].nTargetLuminosity = 190;
		m_sImageElement.MapInst[0].dLDistance = 65;
		m_sImageElement.MapInst[0].nDeltaLuminosity = -10;
		break;
	case 5:	// VS 2015 Blue
		m_sImageElement.MapInst[0].nFlag = EXT_MFC_HSL_HUE;
		m_sImageElement.MapInst[0].nTargetHue = 0;
		m_sImageElement.MapInst[0].dHDistance = 10;
		m_sImageElement.MapInst[0].nTargetLuminosity = 190;
		m_sImageElement.MapInst[0].dLDistance = 65;
		m_sImageElement.MapInst[0].nDeltaHue = 151;
		m_sImageElement.MapInst[0].nDeltaLuminosity = 10;
		break;
	}
}

// this is a the worker functions
void CExtPaintManagerStudio2010::InitTranslatedColorsIndex(void)
{
	ASSERT_VALID(this);
	CExtPaintManagerStudio2005::InitTranslatedColors();

	m_clr3dHighlight = RGB(233,236,238);
	m_clr3dFace = RGB( 216, 222, 230);
	m_clr3dShadow = RGB(155, 167, 183);
	m_clr3dDkShadow = RGB(73, 91, 102);
	m_clrBtnText = RGB(27, 41, 62);
	m_clrTextDisabled = RGB(144, 144, 144);
	m_clrXpHighlight = RGB(255, 236, 181);
	m_clrXpHighlightBorder = RGB(229, 195, 101);
	m_clrSeparator = RGB(133, 145, 162);

	INT nIdxClr3dHiglight = InstallColor(m_clr3dHighlight);
	m_colors[COLOR_3DHIGHLIGHT] = m_clr3dHighlight;
	m_mapColorTranslate[COLOR_3DHIGHLIGHT] = nIdxClr3dHiglight;
	m_mapColorTranslate[CLR_3DLIGHT_OUT] = nIdxClr3dHiglight;
	m_mapColorTranslate[CLR_3DLIGHT_IN] = nIdxClr3dHiglight;
	m_mapColorTranslate[CLR_3DLIGHT_DISABLED] = nIdxClr3dHiglight;
	m_mapColorTranslate[CLR_3DHILIGHT_OUT] = nIdxClr3dHiglight;
	m_mapColorTranslate[CLR_3DHILIGHT_IN] = nIdxClr3dHiglight;
	m_mapColorTranslate[CLR_3DHILIGHT_DISABLED] = nIdxClr3dHiglight;
	INT nIdxClr3dFace = InstallColor(m_clr3dFace);
	m_colors[COLOR_3DFACE] = m_clr3dFace;
	m_mapColorTranslate[COLOR_3DFACE] = nIdxClr3dFace;
	m_mapColorTranslate[CLR_3DFACE_OUT] = nIdxClr3dFace;
	m_mapColorTranslate[CLR_3DFACE_IN] = nIdxClr3dFace;
	m_mapColorTranslate[CLR_3DFACE_DISABLED] = nIdxClr3dFace;
	INT nIdxClr3dShadow = InstallColor(m_clr3dShadow);
	m_colors[COLOR_3DSHADOW] = m_clr3dShadow;
	m_mapColorTranslate[COLOR_3DSHADOW] = nIdxClr3dShadow;
	m_mapColorTranslate[CLR_3DSHADOW_OUT] = nIdxClr3dShadow;
	m_mapColorTranslate[CLR_3DSHADOW_IN] = nIdxClr3dShadow;
	m_mapColorTranslate[CLR_3DSHADOW_DISABLED] = nIdxClr3dShadow;
	INT nIdxClr3dDkShadow = InstallColor(m_clr3dDkShadow);
	m_colors[COLOR_3DDKSHADOW] = m_clr3dDkShadow;
	m_mapColorTranslate[COLOR_3DDKSHADOW] = nIdxClr3dDkShadow;
	m_mapColorTranslate[CLR_3DDKSHADOW_OUT] = nIdxClr3dDkShadow;
	m_mapColorTranslate[CLR_3DDKSHADOW_IN] = nIdxClr3dDkShadow;
	m_mapColorTranslate[CLR_3DDKSHADOW_DISABLED] = nIdxClr3dDkShadow;
	INT nIdxClrBtnTextNormal = InstallColor(m_clrBtnText);
	INT nIdxClrBtnTextDisabled = InstallColor(m_clrTextDisabled);
	m_colors[COLOR_BTNTEXT] = m_clrBtnText;
	m_colors[COLOR_MENUTEXT] = m_clrBtnText;
	m_colors[COLOR_GRAYTEXT] = m_clrTextDisabled;
	m_mapColorTranslate[COLOR_MENUTEXT] = nIdxClrBtnTextNormal;
	m_mapColorTranslate[CLR_MENUTEXT_IN] = nIdxClrBtnTextNormal;
	m_mapColorTranslate[CLR_MENUTEXT_OUT] = nIdxClrBtnTextNormal;
	m_mapColorTranslate[CLR_MENUTEXT_DISABLED] = nIdxClrBtnTextDisabled;
	m_mapColorTranslate[COLOR_BTNTEXT] = nIdxClrBtnTextNormal;
	m_mapColorTranslate[XPCLR_PUSHEDHOVERTEXT] = nIdxClrBtnTextNormal;
	m_mapColorTranslate[CLR_TEXT_IN] = nIdxClrBtnTextNormal;
	m_mapColorTranslate[CLR_TEXT_OUT] = nIdxClrBtnTextNormal;
	m_mapColorTranslate[XPCLR_PUSHEDHOVERTEXT] = nIdxClrBtnTextNormal;
	m_mapColorTranslate[COLOR_GRAYTEXT] = nIdxClrBtnTextDisabled;
	m_mapColorTranslate[CLR_TEXT_DISABLED] = nIdxClrBtnTextDisabled;
	m_mapColorTranslate[XPCLR_TEXT_FIELD_BORDER_NORMAL] = nIdxClr3dDkShadow;
	m_mapColorTranslate[XPCLR_TEXT_FIELD_BORDER_DISABLED] = nIdxClr3dDkShadow;
	m_mapColorTranslate[XPCLR_RARELY_BORDER] = nIdxClr3dDkShadow;

	m_mapColorTranslate[_2003CLR_TBB_BK_COMBINED_TOP] = nIdxClr3dHiglight;
	m_mapColorTranslate[_2003CLR_TBB_BK_COMBINED_BOTTOM] = nIdxClr3dHiglight;
	m_mapColorTranslate[_2003CLR_MLA_NORM_LEFT] = nIdxClr3dHiglight;
	m_mapColorTranslate[_2003CLR_MLA_NORM_MIDDLE] = nIdxClr3dHiglight;
	m_mapColorTranslate[_2003CLR_MLA_NORM_RIGHT] = nIdxClr3dHiglight;
	COLORREF clrRarelyMenuGutter = stat_RGB_Blend(m_clr3dHighlight, m_clr3dShadow, 128);
	INT nIdxClrRarelyMenuGutter = InstallColor(clrRarelyMenuGutter);
	m_mapColorTranslate[_2003CLR_MLA_RARELY_LEFT] = nIdxClrRarelyMenuGutter;
	m_mapColorTranslate[_2003CLR_MLA_RARELY_MIDDLE] = nIdxClrRarelyMenuGutter;
	m_mapColorTranslate[_2003CLR_MLA_RARELY_RIGHT] = nIdxClrRarelyMenuGutter;
	m_mapColorTranslate[XPCLR_MENU_BORDER] = nIdxClrRarelyMenuGutter;

	INT nIdxClrXpHighlight = InstallColor(m_clrXpHighlight);
	INT nIdxClrXpHighlightBorder = InstallColor(m_clrXpHighlightBorder);
	m_mapColorTranslate[XPCLR_HILIGHT] = nIdxClrXpHighlight;
	m_mapColorTranslate[XPCLR_HILIGHT_BORDER] = nIdxClrXpHighlightBorder;
	m_mapColorTranslate[XPCLR_HILIGHT_BORDER_SELECTED] = nIdxClrXpHighlightBorder;

	m_mapColorTranslate[XPCLR_3DFACE_DARK] = nIdxClr3dShadow;
	m_mapColorTranslate[XPCLR_3DFACE_NORMAL] = nIdxClr3dFace;

	INT nIdxClrSeparator = InstallColor(m_clrSeparator);
	m_mapColorTranslate[XPCLR_SEPARATOR] = nIdxClrSeparator;
	m_arrClrMenuItemSelectionAreaGradient[0] = RGB(255, 251, 240); // top
	m_arrClrMenuItemSelectionAreaGradient[1] = RGB(255, 245, 211); // middle1
	m_arrClrMenuItemSelectionAreaGradient[2] = RGB(255, 236, 181); // middle2
	m_arrClrMenuItemSelectionAreaGradient[3] = RGB(255, 236, 181); // bottom
	m_arrClrMenuItemSelectionAreaGradient[4] = RGB(229, 195, 101); // border (same as clrHighlightBorder here)

	m_mapColorTranslate[_2003CLR_BTN_HOVER_LEFT] = InstallColor(m_arrClrMenuItemSelectionAreaGradient[0]);
	m_mapColorTranslate[_2003CLR_BTN_HOVER_RIGHT] = InstallColor(m_arrClrMenuItemSelectionAreaGradient[1]);
	m_mapColorTranslate[_2003CLR_BTN_PRESSED_LEFT] = InstallColor(m_arrClrMenuItemSelectionAreaGradient[0]);
	m_mapColorTranslate[_2003CLR_BTN_PRESSED_RIGHT] = InstallColor(m_arrClrMenuItemSelectionAreaGradient[1]);
	m_mapColorTranslate[_2003CLR_BTN_HP_LEFT] = InstallColor(m_arrClrMenuItemSelectionAreaGradient[4]);
	m_mapColorTranslate[_2003CLR_BTN_HP_RIGHT] = InstallColor(m_arrClrMenuItemSelectionAreaGradient[4]);
	m_bmpMenuItemSelectionArea.Empty();
	{ // BLOCK-BEGIN: generating m_bmpMenuItemSelectionArea
		static const SIZE g_sizeBmpMenuItemSelectionArea = { 48, 34 };
		VERIFY(m_bmpMenuItemSelectionArea.FromColor(m_arrClrMenuItemSelectionAreaGradient[4], g_sizeBmpMenuItemSelectionArea));
		m_bmpMenuItemSelectionArea.GradientRect(m_arrClrMenuItemSelectionAreaGradient[1], m_arrClrMenuItemSelectionAreaGradient[0], true, CRect(1, 1, g_sizeBmpMenuItemSelectionArea.cx - 1, g_sizeBmpMenuItemSelectionArea.cy / 2));
		m_bmpMenuItemSelectionArea.GradientRect(m_arrClrMenuItemSelectionAreaGradient[3], m_arrClrMenuItemSelectionAreaGradient[2], true, CRect(1, g_sizeBmpMenuItemSelectionArea.cy / 2, g_sizeBmpMenuItemSelectionArea.cx - 1, g_sizeBmpMenuItemSelectionArea.cy - 1));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(0, 0, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(0)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(0, 1, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(128)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(1, 0, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(128)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(1, 1, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(128)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(g_sizeBmpMenuItemSelectionArea.cx - 1, 0, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(0)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(g_sizeBmpMenuItemSelectionArea.cx - 2, 0, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(128)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(g_sizeBmpMenuItemSelectionArea.cx - 1, 1, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(128)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(g_sizeBmpMenuItemSelectionArea.cx - 2, 1, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(128)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(0, g_sizeBmpMenuItemSelectionArea.cy - 1, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(0)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(1, g_sizeBmpMenuItemSelectionArea.cy - 1, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(128)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(0, g_sizeBmpMenuItemSelectionArea.cy - 2, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(128)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(1, g_sizeBmpMenuItemSelectionArea.cy - 2, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(128)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(g_sizeBmpMenuItemSelectionArea.cx - 1, g_sizeBmpMenuItemSelectionArea.cy - 1, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(0)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(g_sizeBmpMenuItemSelectionArea.cx - 2, g_sizeBmpMenuItemSelectionArea.cy - 1, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(128)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(g_sizeBmpMenuItemSelectionArea.cx - 1, g_sizeBmpMenuItemSelectionArea.cy - 2, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(128)));
		VERIFY(m_bmpMenuItemSelectionArea.SetPixel(g_sizeBmpMenuItemSelectionArea.cx - 2, g_sizeBmpMenuItemSelectionArea.cy - 2, m_arrClrMenuItemSelectionAreaGradient[4], BYTE(128)));
	} // BLOCK-END: generating m_bmpMenuItemSelectionArea

	INT x, y;
	m_clrDockerBkgndTop = RGB(43, 59, 88);
	m_clrDockerBkgndMiddle = RGB(53, 73, 106);
	m_clrDockerBkgndBottom = RGB(42, 58, 87);
	m_clrDockerBkgndDot1 = RGB(41, 57, 85);
	m_clrDockerBkgndDot2 = RGB(53, 73, 106);

	m_bmpDockerBkgndDots.Empty();
	static SIZE g_sizeDockerBkgndBmpMultiplier = { 200, 200 };
	CSize sizeBmpDockerBkgndDots(4 * g_sizeDockerBkgndBmpMultiplier.cx, 4 * g_sizeDockerBkgndBmpMultiplier.cy);
	if (m_bmpDockerBkgndDots.FromColor(RGB(0, 0, 0), sizeBmpDockerBkgndDots, BYTE(0)))
	{
		int x, y, xpx, ypx;
		for (y = 0, ypx = 0; y < g_sizeDockerBkgndBmpMultiplier.cy; y++, ypx += 4)
		{
			for (x = 0, xpx = 0; x < g_sizeDockerBkgndBmpMultiplier.cx; x++, xpx += 4)
			{
				VERIFY(m_bmpDockerBkgndDots.SetPixel(xpx + 0, ypx + 0, m_clrDockerBkgndDot1, 255));
				VERIFY(m_bmpDockerBkgndDots.SetPixel(xpx + 0, ypx + 1, m_clrDockerBkgndDot2, 255));
				VERIFY(m_bmpDockerBkgndDots.SetPixel(xpx + 2, ypx + 2, m_clrDockerBkgndDot1, 255));
				VERIFY(m_bmpDockerBkgndDots.SetPixel(xpx + 2, ypx + 3, m_clrDockerBkgndDot2, 255));
			}
		}
	}
	m_clrOuterDockBarBkgnd = RGB(156, 170, 193);
	m_clrMenuBarGriadient1 = RGB(202, 211, 226);
	m_clrMenuBarGriadient2 = RGB(176, 187, 207);
	m_arrClrBarCaptionActive[0] = RGB(255, 252, 242);
	m_arrClrBarCaptionActive[1] = RGB(255, 244, 209);
	m_arrClrBarCaptionActive[2] = RGB(255, 237, 186);
	m_arrClrBarCaptionActive[3] = RGB(255, 232, 166);
	m_arrClrBarCaptionActive[4] = RGB(255, 232, 166);
	m_arrClrBarCaptionInactive[0] = RGB(77, 96, 130);
	m_arrClrBarCaptionInactive[1] = RGB(69, 89, 124);
	m_arrClrBarCaptionInactive[2] = RGB(69, 89, 124);
	m_arrClrBarCaptionInactive[3] = RGB(69, 89, 124);
	m_arrClrBarCaptionInactive[4] = RGB(61, 82, 119);
	m_clrCaptBtnBorder = RGB(229, 195, 101);
	m_clrCaptBtnGlyphBkgndHover = RGB(255, 252, 244);
	m_clrCaptBtnGlyphBkgndPressed = RGB(255, 232, 166);
	m_clrCaptBtnGlyphDisabled = RGB(192, 192, 192);
	m_clrCaptBtnGlyphInactiveNormal = RGB(206, 212, 221);
	m_clrCaptBtnGlyphActiveNormal = RGB(0, 0, 0);
	m_clrCaptBtnGlyphHover = RGB(0, 0, 0);
	m_clrCaptBtnGlyphPressed = RGB(0, 0, 0);
	m_clrCaptBtnTextNormal = RGB(255, 255, 255);
	m_clrCaptBtnTextActive = RGB(0, 0, 0);

	m_clrDockingFrameBorder = RGB(53, 73, 106);

	INT nCaptGenMode;
	for (nCaptGenMode = 0; nCaptGenMode < 2; nCaptGenMode++)
	{
		COLORREF * arrCaptColors = (nCaptGenMode == 0) ? m_arrClrBarCaptionActive : m_arrClrBarCaptionInactive;
		CExtBitmap & bmpCapt = (nCaptGenMode == 0) ? m_bmpBarCaptionActive : m_bmpBarCaptionInactive;
		bmpCapt.Empty();
		CWindowDC dcDesktop(NULL);
		CDC dc;
		if (!dc.CreateCompatibleDC(&dcDesktop))
			continue;
		static const SIZE g_sizeBmpCapt = { 64, 13 };
		static const CRect g_rcTop(0, 0, g_sizeBmpCapt.cx, g_sizeBmpCapt.cy / 2);
		static const CRect g_rcMiddle(0, g_sizeBmpCapt.cy / 2, g_sizeBmpCapt.cx, g_sizeBmpCapt.cy / 2 + 1);
		static const CRect g_rcBottom(0, g_sizeBmpCapt.cy / 2 + 1, g_sizeBmpCapt.cx, g_sizeBmpCapt.cy);
		BITMAPINFOHEADER bih; bih.biSize = sizeof(BITMAPINFOHEADER);
		bih.biWidth = g_sizeBmpCapt.cx; bih.biHeight = g_sizeBmpCapt.cy; bih.biPlanes = 1; bih.biBitCount = 32; bih.biCompression = BI_RGB;
		bih.biSizeImage = bih.biWidth*bih.biHeight; bih.biXPelsPerMeter = 0; bih.biYPelsPerMeter = 0; bih.biClrUsed = 0; bih.biClrImportant = 0;
		COLORREF * pClrSurface = NULL;
		HBITMAP hDIB = ::CreateDIBSection(dcDesktop.GetSafeHdc(), (LPBITMAPINFO) &bih, DIB_RGB_COLORS, (void **) &pClrSurface, NULL, NULL);
		if (hDIB == NULL)
			continue;
		ASSERT(pClrSurface != NULL);
		CBitmap bmp; // works as destructor for hDIB
		bmp.Attach(hDIB);
		CBitmap * pOldBmp = dc.SelectObject(&bmp);
		int nOldStretchBltMode = ::GetStretchBltMode(dc.m_hDC);
		::SetStretchBltMode(dc.m_hDC, (g_PaintManager.m_bIsWinNT) ? HALFTONE : COLORONCOLOR);
		stat_PaintGradientRect(dc, g_rcTop, arrCaptColors[1], arrCaptColors[0], true);
		dc.FillSolidRect(&g_rcMiddle, arrCaptColors[2]);
		stat_PaintGradientRect(dc, g_rcBottom, arrCaptColors[4], arrCaptColors[3], true);
		::SetStretchBltMode(dc.m_hDC, nOldStretchBltMode);
		dc.SelectObject(pOldBmp);
		if (!bmpCapt.FromBitmap(hDIB))
			continue;
		if (!bmpCapt.Make32())
			continue;
		static INT g_nAlphaCornerMiddle = 128, g_nAlphaCornerLight = 64;
		bmpCapt.SetPixel(0, 0, bmpCapt.GetPixel(0, 0), g_nAlphaCornerLight);
		bmpCapt.SetPixel(0, 1, bmpCapt.GetPixel(0, 1), g_nAlphaCornerMiddle);
		bmpCapt.SetPixel(1, 0, bmpCapt.GetPixel(1, 0), g_nAlphaCornerMiddle);
		bmpCapt.SetPixel(g_sizeBmpCapt.cx - 1, 0, bmpCapt.GetPixel(g_sizeBmpCapt.cx - 1, 0), g_nAlphaCornerLight);
		bmpCapt.SetPixel(g_sizeBmpCapt.cx - 1, 1, bmpCapt.GetPixel(g_sizeBmpCapt.cx - 1, 1), g_nAlphaCornerMiddle);
		bmpCapt.SetPixel(g_sizeBmpCapt.cx - 2, 0, bmpCapt.GetPixel(g_sizeBmpCapt.cx - 2, 0), g_nAlphaCornerMiddle);
	} // for( nCaptGenMode = 0; nCaptGenMode < 2; nCaptGenMode ++ )

	m_mapColorTranslate[_2003CLR_GRIPPER_DOT_DARK] = InstallColor(RGB(96, 114, 140));
	m_mapColorTranslate[_2003CLR_GRIPPER_DOT_LIGHT] = InstallColor(RGB(188, 199, 216));
	
	m_clrToolbarBkgnd = RGB(188, 199, 216);
	m_clrToolbarBorder = RGB(213, 220, 232);

	static const SIZE g_sizeBmpToolBarBkgnd = { 10, 10 };
	m_bmpToolBarBkgnd.Empty();
	VERIFY(m_bmpToolBarBkgnd.FromColor(m_clrToolbarBkgnd, g_sizeBmpToolBarBkgnd));
	m_bmpToolBarBkgnd.Make32();
	m_bmpToolBarBkgnd.AlphaFrame(CRect(0, 0, g_sizeBmpToolBarBkgnd.cx, g_sizeBmpToolBarBkgnd.cy), CRect(1, 1, 1, 1), BYTE(0));
	for (x = 1; x <= (g_sizeBmpToolBarBkgnd.cx - 2); x++)
	{
		VERIFY(m_bmpToolBarBkgnd.SetPixel(x, 1, m_clrToolbarBorder, BYTE(255)));
		VERIFY(m_bmpToolBarBkgnd.SetPixel(x, g_sizeBmpToolBarBkgnd.cy - 2, m_clrToolbarBorder, BYTE(255)));
	}
	for (y = 1; y <= (g_sizeBmpToolBarBkgnd.cy - 2); y++)
	{
		VERIFY(m_bmpToolBarBkgnd.SetPixel(1, y, m_clrToolbarBorder, BYTE(255)));
		VERIFY(m_bmpToolBarBkgnd.SetPixel(g_sizeBmpToolBarBkgnd.cx - 2, y, m_clrToolbarBorder, BYTE(255)));
	}
	VERIFY(m_bmpToolBarBkgnd.SetPixel(1, 1, m_clrToolbarBkgnd, BYTE(0)));
	VERIFY(m_bmpToolBarBkgnd.SetPixel(1, g_sizeBmpToolBarBkgnd.cy - 2, m_clrToolbarBkgnd, BYTE(0)));
	VERIFY(m_bmpToolBarBkgnd.SetPixel(g_sizeBmpToolBarBkgnd.cx - 2, 1, m_clrToolbarBkgnd, BYTE(0)));
	VERIFY(m_bmpToolBarBkgnd.SetPixel(g_sizeBmpToolBarBkgnd.cx - 2, g_sizeBmpToolBarBkgnd.cy - 2, m_clrToolbarBkgnd, BYTE(0)));
	COLORREF clrMergedBorderWidthInnerBkgnd = stat_RGB_Blend(m_clrToolbarBkgnd, m_clrToolbarBorder, 128);
	VERIFY(m_bmpToolBarBkgnd.SetPixel(2, 2, clrMergedBorderWidthInnerBkgnd, BYTE(255)));
	VERIFY(m_bmpToolBarBkgnd.SetPixel(2, g_sizeBmpToolBarBkgnd.cy - 3, clrMergedBorderWidthInnerBkgnd, BYTE(255)));
	VERIFY(m_bmpToolBarBkgnd.SetPixel(g_sizeBmpToolBarBkgnd.cx - 3, 2, clrMergedBorderWidthInnerBkgnd, BYTE(255)));
	VERIFY(m_bmpToolBarBkgnd.SetPixel(g_sizeBmpToolBarBkgnd.cx - 3, g_sizeBmpToolBarBkgnd.cy - 3, clrMergedBorderWidthInnerBkgnd, BYTE(255)));
	COLORREF clrMergedBorderWidthOuterBkgnd = stat_RGB_Blend(m_clrToolbarBkgnd, m_clrToolbarBkgnd, 128);
	VERIFY(m_bmpToolBarBkgnd.SetPixel(1, 2, clrMergedBorderWidthOuterBkgnd, BYTE(255)));
	VERIFY(m_bmpToolBarBkgnd.SetPixel(2, 1, clrMergedBorderWidthOuterBkgnd, BYTE(255)));
	VERIFY(m_bmpToolBarBkgnd.SetPixel(1, g_sizeBmpToolBarBkgnd.cy - 3, clrMergedBorderWidthOuterBkgnd, BYTE(255)));
	VERIFY(m_bmpToolBarBkgnd.SetPixel(2, g_sizeBmpToolBarBkgnd.cy - 2, clrMergedBorderWidthOuterBkgnd, BYTE(255)));
	VERIFY(m_bmpToolBarBkgnd.SetPixel(g_sizeBmpToolBarBkgnd.cx - 2, 2, clrMergedBorderWidthOuterBkgnd, BYTE(255)));
	VERIFY(m_bmpToolBarBkgnd.SetPixel(g_sizeBmpToolBarBkgnd.cx - 3, 1, clrMergedBorderWidthOuterBkgnd, BYTE(255)));
	VERIFY(m_bmpToolBarBkgnd.SetPixel(g_sizeBmpToolBarBkgnd.cx - 2, g_sizeBmpToolBarBkgnd.cy - 3, clrMergedBorderWidthOuterBkgnd, BYTE(255)));
	VERIFY(m_bmpToolBarBkgnd.SetPixel(g_sizeBmpToolBarBkgnd.cx - 3, g_sizeBmpToolBarBkgnd.cy - 2, clrMergedBorderWidthOuterBkgnd, BYTE(255)));

	m_clrTbExpHoverLight1 = RGB(255, 252, 243);
	m_clrTbExpHoverLight2 = RGB(255, 243, 206);
	m_clrTbExpHoverDark1 = RGB(255, 236, 181);
	m_clrTbExpHoverDark2 = RGB(255, 236, 181);
	m_clrTbExpPressedLight1 = RGB(255, 232, 166);
	m_clrTbExpPressedLight2 = RGB(255, 232, 166);
	m_clrTbExpPressedDark1 = RGB(255, 232, 166);
	m_clrTbExpPressedDark2 = RGB(255, 232, 166);
	m_clrTbExpGlyphNormal = RGB(27, 41, 62);
	m_clrTbExpGlyphHover = RGB(27, 41, 62);
	m_clrTbExpGlyphPressed = RGB(27, 41, 62);
	// __TW_OFF
	static const SIZE g_sizeBmpTbExpBtnH = { 12, 32 };
	VERIFY(m_bmpTbExpNormalH.FromColor(m_clrToolbarBorder, g_sizeBmpTbExpBtnH));
	VERIFY(m_bmpTbExpNormalH.SetPixel(g_sizeBmpTbExpBtnH.cx - 1, 0, m_clrToolbarBkgnd, BYTE(0)));
	VERIFY(m_bmpTbExpNormalH.SetPixel(g_sizeBmpTbExpBtnH.cx - 1, g_sizeBmpTbExpBtnH.cy - 1, m_clrToolbarBkgnd, BYTE(0)));
	VERIFY(m_bmpTbExpNormalH.SetPixel(g_sizeBmpTbExpBtnH.cx - 1, 1, m_clrToolbarBorder, BYTE(128)));
	VERIFY(m_bmpTbExpNormalH.SetPixel(g_sizeBmpTbExpBtnH.cx - 2, 0, m_clrToolbarBorder, BYTE(128)));
	VERIFY(m_bmpTbExpNormalH.SetPixel(g_sizeBmpTbExpBtnH.cx - 1, g_sizeBmpTbExpBtnH.cy - 2, m_clrToolbarBorder, BYTE(128)));
	VERIFY(m_bmpTbExpNormalH.SetPixel(g_sizeBmpTbExpBtnH.cx - 2, g_sizeBmpTbExpBtnH.cy - 1, m_clrToolbarBorder, BYTE(128)));
	m_bmpTbExpNormalH.AlphaRect(CRect(0, 1, 1, g_sizeBmpTbExpBtnH.cy - 2), BYTE(0));
	m_bmpTbExpNormalH.AlphaRect(CRect(1, 2, 2, g_sizeBmpTbExpBtnH.cy - 3), BYTE(128));
	m_bmpTbExpHoverH = m_bmpTbExpNormalH;
	m_bmpTbExpHoverH.GradientRect(m_clrTbExpHoverLight1, m_clrTbExpHoverLight2, true, CRect(0, 0, g_sizeBmpTbExpBtnH.cx, g_sizeBmpTbExpBtnH.cy / 2 + 1));
	m_bmpTbExpHoverH.GradientRect(m_clrTbExpHoverDark1, m_clrTbExpHoverDark2, true, CRect(0, g_sizeBmpTbExpBtnH.cy / 2, g_sizeBmpTbExpBtnH.cx, g_sizeBmpTbExpBtnH.cy));
	VERIFY(m_bmpTbExpHoverH.SetPixel(g_sizeBmpTbExpBtnH.cx - 1, 0, m_clrToolbarBkgnd, BYTE(0)));
	VERIFY(m_bmpTbExpHoverH.SetPixel(g_sizeBmpTbExpBtnH.cx - 1, g_sizeBmpTbExpBtnH.cy - 1, m_clrToolbarBkgnd, BYTE(0)));
	VERIFY(m_bmpTbExpHoverH.SetPixel(g_sizeBmpTbExpBtnH.cx - 1, 1, m_clrTbExpHoverLight1, BYTE(128)));
	VERIFY(m_bmpTbExpHoverH.SetPixel(g_sizeBmpTbExpBtnH.cx - 2, 0, m_clrTbExpHoverLight1, BYTE(128)));
	VERIFY(m_bmpTbExpHoverH.SetPixel(g_sizeBmpTbExpBtnH.cx - 1, g_sizeBmpTbExpBtnH.cy - 2, m_clrTbExpHoverDark2, BYTE(128)));
	VERIFY(m_bmpTbExpHoverH.SetPixel(g_sizeBmpTbExpBtnH.cx - 2, g_sizeBmpTbExpBtnH.cy - 1, m_clrTbExpHoverDark2, BYTE(128)));
	m_bmpTbExpHoverH.AlphaRect(CRect(0, 1, 1, g_sizeBmpTbExpBtnH.cy - 2), BYTE(0));
	m_bmpTbExpHoverH.AlphaRect(CRect(1, 2, 2, g_sizeBmpTbExpBtnH.cy - 3), BYTE(128));
	m_bmpTbExpPressedH = m_bmpTbExpNormalH;
	m_bmpTbExpPressedH.GradientRect(m_clrTbExpPressedLight1, m_clrTbExpPressedLight2, true, CRect(0, 0, g_sizeBmpTbExpBtnH.cx, g_sizeBmpTbExpBtnH.cy / 2 + 1));
	m_bmpTbExpPressedH.GradientRect(m_clrTbExpPressedDark1, m_clrTbExpPressedDark2, true, CRect(0, g_sizeBmpTbExpBtnH.cy / 2, g_sizeBmpTbExpBtnH.cx, g_sizeBmpTbExpBtnH.cy));
	VERIFY(m_bmpTbExpPressedH.SetPixel(g_sizeBmpTbExpBtnH.cx - 1, 0, m_clrToolbarBkgnd, BYTE(0)));
	VERIFY(m_bmpTbExpPressedH.SetPixel(g_sizeBmpTbExpBtnH.cx - 1, g_sizeBmpTbExpBtnH.cy - 1, m_clrToolbarBkgnd, BYTE(0)));
	VERIFY(m_bmpTbExpPressedH.SetPixel(g_sizeBmpTbExpBtnH.cx - 1, 1, m_clrTbExpPressedLight1, BYTE(128)));
	VERIFY(m_bmpTbExpPressedH.SetPixel(g_sizeBmpTbExpBtnH.cx - 2, 0, m_clrTbExpPressedLight1, BYTE(128)));
	VERIFY(m_bmpTbExpPressedH.SetPixel(g_sizeBmpTbExpBtnH.cx - 1, g_sizeBmpTbExpBtnH.cy - 2, m_clrTbExpPressedDark2, BYTE(128)));
	VERIFY(m_bmpTbExpPressedH.SetPixel(g_sizeBmpTbExpBtnH.cx - 2, g_sizeBmpTbExpBtnH.cy - 1, m_clrTbExpPressedDark2, BYTE(128)));
	m_bmpTbExpPressedH.AlphaRect(CRect(0, 1, 1, g_sizeBmpTbExpBtnH.cy - 2), BYTE(0));
	m_bmpTbExpPressedH.AlphaRect(CRect(1, 2, 2, g_sizeBmpTbExpBtnH.cy - 3), BYTE(128));
	VERIFY(m_bmpTbExpNormalV.CreateRotated9xStack(m_bmpTbExpNormalH, 90, 1, g_sizeBmpTbExpBtnH.cx, g_sizeBmpTbExpBtnH.cy, true, false));
	VERIFY(m_bmpTbExpHoverV.CreateRotated9xStack(m_bmpTbExpHoverH, 90, 1, g_sizeBmpTbExpBtnH.cx, g_sizeBmpTbExpBtnH.cy, true, false));
	VERIFY(m_bmpTbExpPressedV.CreateRotated9xStack(m_bmpTbExpPressedH, 90, 1, g_sizeBmpTbExpBtnH.cx, g_sizeBmpTbExpBtnH.cy, true, false));
	VERIFY(m_bmpTbExpNormalV.FlipHorizontal());
	VERIFY(m_bmpTbExpHoverV.FlipHorizontal());
	VERIFY(m_bmpTbExpPressedV.FlipHorizontal());

#if (!defined __EXT_MFC_NO_TAB_CTRL)

	m_tabDefs[__ETI2010_DISABLED][0].m_clrText = __EXT_MFC_RGBA(192, 192, 192, 255);
	// __TW_OFF
	m_tabDefs[__ETI2010_NORMAL][0].m_clrText =
		m_tabDefs[__ETI2010_NORMAL_GROUPPED][0].m_clrText =
		m_tabDefs[__ETI2010_HOVER][0].m_clrText =
		RGB(255, 255, 255);
	m_tabDefs[__ETI2010_SELECTED][0].m_clrText =
		m_tabDefs[__ETI2010_SELECTED_ACTIVE][0].m_clrText =
		m_tabDefs[__ETI2010_SELECTED_CB][0].m_clrText =
		RGB(0, 0, 0);

	m_tabDefs[__ETI2010_NORMAL_GROUPPED][0].m_clrGradient[0] = __EXT_MFC_RGBA(61, 82, 119, 255);
	m_tabDefs[__ETI2010_NORMAL_GROUPPED][0].m_clrGradient[1] = __EXT_MFC_RGBA(76, 95, 129, 255);
	m_tabDefs[__ETI2010_NORMAL_GROUPPED][0].m_clrBorder = __EXT_MFC_RGBA(54, 78, 111, 255);
	m_tabDefs[__ETI2010_HOVER][0].m_clrGradient[0] = __EXT_MFC_RGBA(111, 119, 118, 255);
	m_tabDefs[__ETI2010_HOVER][0].m_clrGradient[1] = __EXT_MFC_RGBA(79, 95, 116, 255);
	m_tabDefs[__ETI2010_HOVER][0].m_clrBorder = __EXT_MFC_RGBA(155, 167, 183, 255);
	m_tabDefs[__ETI2010_SELECTED][0].m_clrGradient[0] = __EXT_MFC_RGBA(251, 252, 252, 255);
	m_tabDefs[__ETI2010_SELECTED][0].m_clrGradient[1] = __EXT_MFC_RGBA(215, 220, 228, 255);
	m_tabDefs[__ETI2010_SELECTED][0].m_clrGradient[2] = __EXT_MFC_RGBA(206, 212, 223, 255);
	m_tabDefs[__ETI2010_SELECTED][0].m_clrGradient[3] = __EXT_MFC_RGBA(206, 212, 223, 255);
	m_tabDefs[__ETI2010_SELECTED_ACTIVE][0].m_clrGradient[0] = __EXT_MFC_RGBA(255, 252, 242, 255);
	m_tabDefs[__ETI2010_SELECTED_ACTIVE][0].m_clrGradient[1] = __EXT_MFC_RGBA(255, 243, 207, 255);
	m_tabDefs[__ETI2010_SELECTED_ACTIVE][0].m_clrGradient[2] = __EXT_MFC_RGBA(255, 232, 166, 255);
	m_tabDefs[__ETI2010_SELECTED_ACTIVE][0].m_clrGradient[3] = __EXT_MFC_RGBA(255, 232, 166, 255);
	m_tabDefs[__ETI2010_SELECTED_CB][0].m_clrGradient[0] = __EXT_MFC_RGBA(255, 255, 255, 255);
	m_tabDefs[__ETI2010_SELECTED_CB][0].m_clrGradient[1] = __EXT_MFC_RGBA(255, 255, 255, 255);
	// __TW_OFF
	INT nTabDefIdx, nSideIdx;
	for (nTabDefIdx = 0; nTabDefIdx < INT(__ETI2010_COUNT); nTabDefIdx++)
		m_tabDefs[nTabDefIdx][0].GenerateBmp();
	//	m_tabDefs[__ETI2010_NORMAL_GROUPPED][0].m_bmpTabItem.FlipVertical();

	for (nSideIdx = 1; nSideIdx <= 3; nSideIdx++)
	{
		for (nTabDefIdx = 0; nTabDefIdx < INT(__ETI2010_COUNT); nTabDefIdx++)
			m_tabDefs[nTabDefIdx][nSideIdx].CreateRotatedVersionFromTopOrientedVersion(m_tabDefs[nTabDefIdx][0], nSideIdx);
	}
#endif // (!defined __EXT_MFC_NO_TAB_CTRL)

#if (!defined __EXT_MFC_NO_PAGE_NAVIGATOR )
	m_nPnCaptionBkClrIdx = InstallColor(RGB(169, 193, 222));

	m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_LIGHT] = m_nPnCaptionBkClrIdx;
	m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_DARK] = m_nPnCaptionBkClrIdx;
	m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_HOVER_LIGHT] = m_nPnCaptionBkClrIdx;
	m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_HOVER_DARK] = m_nPnCaptionBkClrIdx;
	m_mapColorTranslate[_2003CLR_PN_PANE_CAPTION_BOTTOM_LINE] = m_nPnCaptionBkClrIdx;
	m_mapColorTranslate[_2003CLR_PN_ITEM_LIGHT] = InstallColor(RGB(223, 226, 229));
	m_mapColorTranslate[_2003CLR_PN_ITEM_DARK] = InstallColor(RGB(225, 227, 230));
	m_mapColorTranslate[_2003CLR_PN_GRIPPER_LIGHT] = InstallColor(RGB(246, 247, 248));
	m_mapColorTranslate[_2003CLR_PN_GRIPPER_DARK] = InstallColor(RGB(218, 223, 231));
	m_mapColorTranslate[_2003CLR_PN_BORDER] = InstallColor(RGB(162, 166, 171));
	m_mapColorTranslate[CLR_PN_CAPTION_TEXT] = InstallColor(RGB(101, 109, 117)); // 30, 57, 91
#endif // (!defined __EXT_MFC_NO_PAGE_NAVIGATOR )

	/// BEGIN: scroll bars //////////////////////////////////////////////////////////////////////////////////////////////////
	INT nScrollBarTypeIndex, nScrollBarTypeCount = 2;
	for (nScrollBarTypeIndex = 0; nScrollBarTypeIndex < nScrollBarTypeCount; nScrollBarTypeIndex++)
	{
		SCROLLBARSKINDATA & _SbSkinDataH = (nScrollBarTypeIndex == 0) ? m_SbSkinDataH_Light : m_SbSkinDataH_Dark;
		SCROLLBARSKINDATA & _SbSkinDataV = (nScrollBarTypeIndex == 0) ? m_SbSkinDataV_Light : m_SbSkinDataV_Dark;
		// prepare scroll bar skinning
		_SbSkinDataH.Empty();
		_SbSkinDataV.Empty();
		// entire scroll bar background
		CSize sizeSbPart2010(32, 32);
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA].FromGradient(m_clrMenuBarGriadient1, m_clrMenuBarGriadient2, true, sizeSbPart2010));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA].FromGradient(m_clrMenuBarGriadient2, m_clrMenuBarGriadient1, false, sizeSbPart2010));
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA].Make32());
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA].Make32());
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_SOLID_SCROLLABLE_AREA] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_SOLID_SCROLLABLE_AREA] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_SOLID_SCROLLABLE_AREA].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, 0.2, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_SOLID_SCROLLABLE_AREA].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, 0.2, 0.0, NULL));
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_SOLID_SCROLLABLE_AREA] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_SOLID_SCROLLABLE_AREA] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_SOLID_SCROLLABLE_AREA].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, 0.1, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_SOLID_SCROLLABLE_AREA].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, 0.1, 0.0, NULL));

		// scroll bar's up/down/thumb buttons
		double lfLuminocityAdjustment_Darker = (nScrollBarTypeIndex == 0) ? (-0.05) : (-0.2);
		double lfLuminocityAdjustment_Dark = (nScrollBarTypeIndex == 0) ? (-0.10) : (-0.4);
		double lfLuminocityAdjustment_Darkest = (nScrollBarTypeIndex == 0) ? (-0.15) : (-0.6);
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_BUTTON_UP] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_BUTTON_UP] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_BUTTON_UP] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_BUTTON_UP] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_BUTTON_UP].FlipVertical();
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_BUTTON_UP].FlipHorizontal();
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_BUTTON_UP].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Dark, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_BUTTON_UP].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Dark, 0.0, NULL));
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_BUTTON_UP] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_BUTTON_UP] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_BUTTON_UP] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_BUTTON_UP] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_BUTTON_UP].FlipVertical();
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_BUTTON_UP].FlipHorizontal();
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_BUTTON_UP].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Darker, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_BUTTON_UP].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Darker, 0.0, NULL));
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_BUTTON_UP] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_BUTTON_UP] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_BUTTON_UP].FlipVertical();
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_BUTTON_UP].FlipHorizontal();
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_BUTTON_UP].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Darkest, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_BUTTON_UP].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Darkest, 0.0, NULL));

		_SbSkinDataH.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_BUTTON_DOWN] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_BUTTON_DOWN] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_BUTTON_DOWN] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_BUTTON_DOWN] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_BUTTON_DOWN].FlipVertical();
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_BUTTON_DOWN].FlipHorizontal();
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_BUTTON_DOWN].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Dark, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_BUTTON_DOWN].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Dark, 0.0, NULL));
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_BUTTON_DOWN] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_BUTTON_DOWN] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_BUTTON_DOWN] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_BUTTON_DOWN] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_BUTTON_DOWN].FlipVertical();
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_BUTTON_DOWN].FlipHorizontal();
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_BUTTON_DOWN].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Darker, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_BUTTON_DOWN].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Darker, 0.0, NULL));
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_BUTTON_DOWN] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_BUTTON_DOWN] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_BUTTON_DOWN].FlipVertical();
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_BUTTON_DOWN].FlipHorizontal();
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_BUTTON_DOWN].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Darkest, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_BUTTON_DOWN].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Darkest, 0.0, NULL));

		_SbSkinDataH.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_THUMB] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_THUMB] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_THUMB] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_THUMB] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_THUMB].FlipVertical();
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_THUMB].FlipHorizontal();
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_THUMB].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Dark, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_THUMB].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Dark, 0.0, NULL));
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_THUMB] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_THUMB] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_SOLID_SCROLLABLE_AREA];
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_THUMB].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Dark, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_THUMB].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Dark, 0.0, NULL));
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_THUMB].FlipVertical();
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_THUMB].FlipHorizontal();

		_SbSkinDataH.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_THUMB] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_THUMB] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_THUMB].FlipVertical();
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_THUMB].FlipHorizontal();
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_THUMB].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Darker, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_THUMB].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Darker, 0.0, NULL));
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_THUMB] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_THUMB] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_THUMB].FlipVertical();
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_THUMB].FlipHorizontal();
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_THUMB].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Darkest, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_THUMB].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, lfLuminocityAdjustment_Darkest, 0.0, NULL));

		// scroll bar's page up/down area
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_PAGE_UP] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_PAGE_UP] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_PAGE_UP].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.1, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_PAGE_UP].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.1, 0.0, NULL));
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_PAGE_UP] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_PAGE_UP] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_PAGE_UP].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.3, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_PAGE_UP].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.3, 0.0, NULL));
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_PAGE_DOWN] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_PAGE_DOWN] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_PAGE_DOWN].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.1, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_HOT][__ESBMHT_PAGE_DOWN].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.1, 0.0, NULL));
		_SbSkinDataH.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_PAGE_DOWN] = _SbSkinDataH.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_PAGE_DOWN] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_SOLID_SCROLLABLE_AREA];
		VERIFY(_SbSkinDataH.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_PAGE_DOWN].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.3, 0.0, NULL));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_PAGE_DOWN].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.3, 0.0, NULL));
		// scroll bar thumb button's gripper
		COLORREF clrThumbLineTop = stat_HLS_Adjust(m_clrMenuBarGriadient2, 0.0, -0.7, 0.0);
		COLORREF clrThumbLineBottom = stat_HLS_Adjust(m_clrMenuBarGriadient1, 0.0, -0.2, 0.0);
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_GLYPH_THUMB_GRIPPER].FromColor(clrThumbLineTop, CSize(8, 8)));
		CRect rcWalkThumb(0, 1, 8, 2);
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_GLYPH_THUMB_GRIPPER].ColorRect(clrThumbLineBottom, rcWalkThumb, BYTE(255));
		rcWalkThumb.OffsetRect(0, 1);
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_GLYPH_THUMB_GRIPPER].ColorRect(clrThumbLineBottom, rcWalkThumb, BYTE(0));
		rcWalkThumb.OffsetRect(0, 2);
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_GLYPH_THUMB_GRIPPER].ColorRect(clrThumbLineBottom, rcWalkThumb, BYTE(255));
		rcWalkThumb.OffsetRect(0, 1);
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_GLYPH_THUMB_GRIPPER].ColorRect(clrThumbLineBottom, rcWalkThumb, BYTE(0));
		rcWalkThumb.OffsetRect(0, 2);
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_GLYPH_THUMB_GRIPPER].ColorRect(clrThumbLineBottom, rcWalkThumb, BYTE(255));
		VERIFY(
			_SbSkinDataH.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_GLYPH_THUMB_GRIPPER].CreateRotated9xStack(
			_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_GLYPH_THUMB_GRIPPER],
			270, 1, 8, 8, true, true
			));

		// scroll bar up/down button's arrows
		VERIFY(
			_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_GLYPH_ARROW_DOWN].LoadBMP_Resource(
			MAKEINTRESOURCE(IDB_EXT_2007_SCROLL_BAR_ARROW_DOWN_DISABLED_R2)
			));
		VERIFY(
			_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_GLYPH_ARROW_DOWN].LoadBMP_Resource(
			MAKEINTRESOURCE(IDB_EXT_2007_SCROLL_BAR_ARROW_DOWN_NORMAL_R2)
			));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_GLYPH_ARROW_DOWN].Make32());
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_GLYPH_ARROW_DOWN].MakeMono();
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_GLYPH_ARROW_DOWN] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_GLYPH_ARROW_DOWN];
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_GLYPH_ARROW_DOWN].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.3, 0.0, NULL));
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_GLYPH_ARROW_DOWN] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_GLYPH_ARROW_DOWN];
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_GLYPH_ARROW_DOWN].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, +0.75, 0.0, NULL));
		VERIFY(
			_SbSkinDataV.m_arrPartBmp[__ESBSPT_DISABLED][__ESBMHT_GLYPH_ARROW_UP].LoadBMP_Resource(
			MAKEINTRESOURCE(IDB_EXT_2007_SCROLL_BAR_ARROW_UP_DISABLED_R2)
			));
		VERIFY(
			_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_GLYPH_ARROW_UP].LoadBMP_Resource(
			MAKEINTRESOURCE(IDB_EXT_2007_SCROLL_BAR_ARROW_UP_NORMAL_R2)
			));
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_GLYPH_ARROW_UP].Make32());
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_GLYPH_ARROW_UP].MakeMono();
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_GLYPH_ARROW_UP] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_GLYPH_ARROW_UP];
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_COLD][__ESBMHT_GLYPH_ARROW_UP].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.3, 0.0, NULL));
		_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_GLYPH_ARROW_UP] = _SbSkinDataV.m_arrPartBmp[__ESBSPT_NORMAL][__ESBMHT_GLYPH_ARROW_UP];
		VERIFY(_SbSkinDataV.m_arrPartBmp[__ESBSPT_PRESSED][__ESBMHT_GLYPH_ARROW_UP].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, +0.75, 0.0, NULL));
		INT nScrollBarPartIndex, nScrollBarStateIndex;
		for (nScrollBarStateIndex = 0; nScrollBarStateIndex < INT(__ESBSPT_PART_COUNT); nScrollBarStateIndex++)
		{
			for (nScrollBarPartIndex = INT(__ESBMHT_GLYPH_ARROW_UP); nScrollBarPartIndex <= INT(__ESBMHT_GLYPH_ARROW_DOWN); nScrollBarPartIndex++)
			{
				CSize sizeSbArrowGlyph = _SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].GetSize();
				if (sizeSbArrowGlyph.cx > 0 && sizeSbArrowGlyph.cy > 0)
				{
					VERIFY(
						_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].CreateRotated9xStack(
						_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex],
						270, 1, sizeSbArrowGlyph.cx, sizeSbArrowGlyph.cy, true, true
						));
				}
			} // for( nScrollBarPartIndex = INT(__ESBMHT_GLYPH_ARROW_UP); nScrollBarPartIndex <= INT(__ESBMHT_GLYPH_ARROW_DOWN); nScrollBarPartIndex ++ )
		} // for( nScrollBarStateIndex = 0; nScrollBarStateIndex < INT(__ESBSPT_PART_COUNT); nScrollBarStateIndex ++ )
		// scroll bar's element paddings, adjusted frames
		CRect rcSbFrameTopOuter(1, 0, sizeSbPart2010.cx - 2, 1);
		CRect rcSbFrameTopInner(1, 1, sizeSbPart2010.cx - 2, 2);
		CRect rcSbFrameBottomOuter(1, sizeSbPart2010.cy - 1, sizeSbPart2010.cx - 2, sizeSbPart2010.cy);
		CRect rcSbFrameBottomInner(1, sizeSbPart2010.cy - 2, sizeSbPart2010.cx - 2, sizeSbPart2010.cy - 1);
		CRect rcSbFrameLeftOuter(0, 1, 1, sizeSbPart2010.cy - 1);
		CRect rcSbFrameLeftInner(1, 2, 2, sizeSbPart2010.cy - 2);
		CRect rcSbFrameRightOuter(sizeSbPart2010.cx - 1, 1, sizeSbPart2010.cx, sizeSbPart2010.cy - 1);
		CRect rcSbFrameRightInner(sizeSbPart2010.cx - 2, 2, sizeSbPart2010.cx - 1, sizeSbPart2010.cy - 2);
		CRect rcSbFrameCornerTopLeft(0, 0, 1, 1);
		CRect rcSbFrameCornerTopRight(sizeSbPart2010.cx - 1, 0, sizeSbPart2010.cx, 1);
		CRect rcSbFrameCornerBottomLeft(0, sizeSbPart2010.cy - 1, 1, sizeSbPart2010.cy);
		CRect rcSbFrameCornerBottomRight(sizeSbPart2010.cx - 1, sizeSbPart2010.cy - 1, sizeSbPart2010.cx, sizeSbPart2010.cy);
		for (nScrollBarStateIndex = 0; nScrollBarStateIndex < INT(__ESBSPT_PART_COUNT); nScrollBarStateIndex++)
		{
			for (nScrollBarPartIndex = 0; nScrollBarPartIndex < INT(__ESBMHT_PART_COUNT); nScrollBarPartIndex++)
			{
				if (nScrollBarPartIndex == INT(__ESBMHT_BUTTON_UP)
					|| nScrollBarPartIndex == INT(__ESBMHT_BUTTON_DOWN)
					|| nScrollBarPartIndex == INT(__ESBMHT_THUMB)
					|| nScrollBarPartIndex == INT(__ESBMHT_SOLID_SCROLLABLE_AREA)
					|| nScrollBarPartIndex == INT(__ESBMHT_PAGE_UP)
					|| nScrollBarPartIndex == INT(__ESBMHT_PAGE_DOWN)
					)
				{ // padding
					if (!_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].IsEmpty())
						_SbSkinDataH.m_arrPartPadding[nScrollBarStateIndex][nScrollBarPartIndex].SetRect(2, 2, 2, 2);
					if (!_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].IsEmpty())
						_SbSkinDataV.m_arrPartPadding[nScrollBarStateIndex][nScrollBarPartIndex].SetRect(2, 2, 2, 2);
				} // padding
				if (((nScrollBarPartIndex == INT(__ESBMHT_BUTTON_UP)
					|| nScrollBarPartIndex == INT(__ESBMHT_BUTTON_DOWN)
					)
					&& nScrollBarStateIndex >= INT(__ESBSPT_NORMAL)
					)
					|| nScrollBarPartIndex == INT(__ESBMHT_THUMB)
					)
				{ // adjusted frames
					if (!_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].IsEmpty())
					{
						ASSERT(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].GetSize() == sizeSbPart2010);
						ASSERT(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].GetSize() == sizeSbPart2010);
						
						VERIFY(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.5, 0.0, &rcSbFrameTopOuter));
						VERIFY(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, +0.3, 0.0, &rcSbFrameTopInner));
						VERIFY(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.5, 0.0, &rcSbFrameBottomOuter));
						VERIFY(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, +0.3, 0.0, &rcSbFrameBottomInner));
						VERIFY(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.5, 0.0, &rcSbFrameLeftOuter));
						VERIFY(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, +0.3, 0.0, &rcSbFrameLeftInner));
						VERIFY(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.5, 0.0, &rcSbFrameRightOuter));
						VERIFY(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, +0.3, 0.0, &rcSbFrameRightInner));
						VERIFY(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustAlpha(-0.4, rcSbFrameCornerTopLeft));
						VERIFY(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustAlpha(-0.4, rcSbFrameCornerTopRight));
						VERIFY(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustAlpha(-0.4, rcSbFrameCornerBottomLeft));
						VERIFY(_SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustAlpha(-0.4, rcSbFrameCornerBottomRight));
					} // if( ! _SbSkinDataH.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].IsEmpty() )
					if (!_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].IsEmpty())
					{
						ASSERT(_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].GetSize() == sizeSbPart2010);
						VERIFY(_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.5, 0.0, &rcSbFrameTopOuter));
						VERIFY(_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, +0.3, 0.0, &rcSbFrameTopInner));
						VERIFY(_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.5, 0.0, &rcSbFrameBottomOuter));
						VERIFY(_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, +0.3, 0.0, &rcSbFrameBottomInner));
						VERIFY(_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.5, 0.0, &rcSbFrameLeftOuter));
						VERIFY(_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, +0.3, 0.0, &rcSbFrameLeftInner));
						VERIFY(_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, -0.5, 0.0, &rcSbFrameRightOuter));
						VERIFY(_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustHLS(RGB(0, 0, 0), RGB(0, 0, 0), 0.0, +0.3, 0.0, &rcSbFrameRightInner));
						VERIFY(_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustAlpha(-0.4, rcSbFrameCornerTopLeft));
						VERIFY(_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustAlpha(-0.4, rcSbFrameCornerTopRight));
						VERIFY(_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustAlpha(-0.4, rcSbFrameCornerBottomLeft));
						VERIFY(_SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].AdjustAlpha(-0.4, rcSbFrameCornerBottomRight));
					} // if( ! _SbSkinDataV.m_arrPartBmp[nScrollBarStateIndex][nScrollBarPartIndex].IsEmpty() )
				} // adjusted frames
			} // for( nScrollBarPartIndex = 0; nScrollBarPartIndex < INT(__ESBMHT_PART_COUNT); nScrollBarPartIndex ++ )
		} // for( nScrollBarStateIndex = 0; nScrollBarStateIndex < INT(__ESBSPT_PART_COUNT); nScrollBarStateIndex ++ )
	} // for( nScrollBarTypeIndex = 0; nScrollBarTypeIndex < nScrollBarTypeCount; nScrollBarTypeIndex ++ )
	/// END: scroll bars ////////////////////////////////////////////////////////////////////////////////////////////////////
}

bool CExtPaintManagerStudio2010::FixedBar_IsPaintRowBkMode(
	const CExtToolControlBar * pBar
	)
{
	ASSERT_VALID(this);
	ASSERT_VALID(pBar);
	ASSERT_KINDOF(CExtToolControlBar, pBar);
	//	ASSERT( pBar->IsFixedMode() );
	if (IsHighContrast())
		return CExtPaintManagerStudio2005::FixedBar_IsPaintRowBkMode(pBar);
	if (pBar->m_bPaletteMode
		|| pBar->m_pDockBar == NULL
		|| (!pBar->IsFloating())
		|| stat_GetBPP() <= 8
		|| (
		(pBar->m_pDockSite == NULL || pBar->m_bPresubclassDialogMode)
		&& (!pBar->m_bForceBalloonGradientInDialogs)
		)
		)
		return false;
	return true;
}

void CExtPaintManagerStudio2010::PaintToolbarExpandButton(
	CDC & dc,
	const RECT & rcButtonArea,
	bool bHorz, // if false - down
	bool bBarIsCompletelyVisible,
	bool bEnabled,
	bool bPushed,
	bool bHover,
	CObject * pHelperSrc,
	LPARAM lParam, // = 0L
	bool bTransparentBackground // = false
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	if (IsHighContrast())
	{
		CExtPaintManagerStudio2005::PaintToolbarExpandButton(
			dc, rcButtonArea, bHorz, bBarIsCompletelyVisible,
			bEnabled, bPushed, bHover,
			pHelperSrc, lParam, bTransparentBackground
			);
		return;
	}
	CRect rect(rcButtonArea);
	rect.NormalizeRect();
	if (!dc.RectVisible(&rect))
		return;
	CExtToolControlBar * pToolBar = NULL;
	if (pHelperSrc != NULL
		&&	pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtBarContentExpandButton))
		)
	{
		pToolBar = ((CExtBarContentExpandButton*) pHelperSrc)->GetBar();
		ASSERT_VALID(pToolBar);
		if (pToolBar->m_bPaletteMode
			|| pToolBar->IsFloating()
			|| pToolBar->IsKindOf(RUNTIME_CLASS(CExtMenuControlBar))
			|| pToolBar->IsKindOf(RUNTIME_CLASS(CExtPanelControlBar))
			|| (
			(pToolBar->m_pDockSite == NULL || pToolBar->m_bPresubclassDialogMode)
			&& (!pToolBar->m_bForceBalloonGradientInDialogs)
			)
			)
			pToolBar = NULL;
	}
	if (pToolBar == NULL)
	{
		CExtPaintManagerStudio2005::PaintToolbarExpandButton(
			dc, rcButtonArea, bHorz, bBarIsCompletelyVisible,
			bEnabled, bPushed, bHover,
			pHelperSrc, lParam, bTransparentBackground
			);
		return;
	}
	ASSERT_VALID(pToolBar);
	CRect rcToolbarClientArea;
	pToolBar->GetClientRect(&rcToolbarClientArea);
	CRect rectButton(rect);
	if (bHorz)
	{
		rectButton.OffsetRect(rcToolbarClientArea.right - rectButton.right, 0);
		rectButton.InflateRect(1, 0, 0, 0);
		rectButton.top = rcToolbarClientArea.top;
		rectButton.bottom = rcToolbarClientArea.bottom - 1;
	} // if( bHorz )
	else
	{
		rectButton.OffsetRect(0, rcToolbarClientArea.bottom - rectButton.bottom);
		rectButton.InflateRect(0, 1, 0, 0);
		rectButton.left = rcToolbarClientArea.left;
		rectButton.right = rcToolbarClientArea.right - 1;
		rectButton.OffsetRect(1, 0);
	} // else from if( bHorz )
	CExtBitmap & bmpBkgnd =
		bHorz
		? (bPushed ? m_bmpTbExpPressedH : (bHover ? m_bmpTbExpHoverH : m_bmpTbExpNormalH))
		: (bPushed ? m_bmpTbExpPressedV : (bHover ? m_bmpTbExpHoverV : m_bmpTbExpNormalV))
		;
	VERIFY(bmpBkgnd.AlphaBlendSkinParts(dc.m_hDC, rectButton, CRect(3, 3, 3, 3), CExtBitmap::__EDM_STRETCH, true, true));
	const glyph_t * pGlyph = NULL, *pGlyph2 = NULL;
	if (bHorz)
	{
		pGlyph = m_pGlyphTbEpBtnH0;
		pGlyph2 = m_pGlyphTbEpBtnH1;
	} // if( bHorz )
	else
	{
		pGlyph = m_pGlyphTbEpBtnV0;
		pGlyph2 = m_pGlyphTbEpBtnV1;
	} // else from if( bHorz )
	ASSERT(pGlyph != NULL);
	ASSERT(pGlyph2 != NULL);
	COLORREF ColorValues [] = { 0, bPushed ? m_clrTbExpGlyphPressed : (bHover ? m_clrTbExpGlyphHover : m_clrTbExpGlyphNormal) };
	CRect rectGlyph(rectButton.TopLeft(), pGlyph->Size());
	CRect rectGlyph2(rectGlyph);
	CSize sizePushedOffset = GetPushedOffset();
	if (bHorz)
	{
		int nGap = (max(sizePushedOffset.cy, 1)) * 3;
		rectGlyph.OffsetRect(
			(rectButton.Size().cx - pGlyph->Size().cx) / 2 + 1,
			rectButton.Size().cy - pGlyph->Size().cy - nGap - 1
			);
		rectGlyph2.OffsetRect(
			(rectButton.Size().cx - pGlyph2->Size().cx) / 2 + 1,
			nGap + 1
			);
	} // if( bHorz )
	else
	{
		int nGap = (max(sizePushedOffset.cx, 1)) * 3;
		rectGlyph.OffsetRect(
			nGap + 1,
			(rectButton.Size().cy - pGlyph->Size().cy) / 2 + 1
			);
		rectGlyph2.OffsetRect(
			rectButton.Size().cx - pGlyph2->Size().cx - nGap - 1,
			(rectButton.Size().cy - pGlyph2->Size().cy) / 2 + 1
			);
		ASSERT(m_bExpBtnSwapVGlyphs);
		CRect rcTmp(rectGlyph);
		rectGlyph = rectGlyph2;
		rectGlyph2 = rcTmp;
	} // else from if( bHorz )
	PaintGlyph(dc, rectGlyph.TopLeft(), *pGlyph, ColorValues);
	if (!bBarIsCompletelyVisible)
		PaintGlyph(dc, rectGlyph2.TopLeft(), *pGlyph2, ColorValues);
	dc.SelectClipRgn(NULL);
}


#if (!defined __EXT_MFC_NO_DOCK_MARKERS)

BYTE CExtPaintManagerStudio2010::DockMarker_GetAlpha(
	bool bHighlight,
	CObject * pHelperSrc,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID(this);
	ASSERT_VALID(this);
	if ((!g_PaintManager.m_bIsWin2000orLater)
		|| stat_GetBPP() < 24
		|| IsHighContrast()
		)
		return
		CExtPaintManagerStudio2005::DockMarker_GetAlpha(
		bHighlight,
		pHelperSrc,
		lParam
		);
	return bHighlight ? BYTE(255) : BYTE(144);
}
#endif // (!defined __EXT_MFC_NO_DOCK_MARKERS)

bool CExtPaintManagerStudio2010::PaintMenuItemSelectionAreaXP(
	CDC & dc,
	CExtPaintManager::PAINTMENUITEMDATA & _pmid,
	const RECT & rcSelectionArea
	)
{
	ASSERT_VALID(this);
	if (m_bmpMenuItemSelectionArea.IsEmpty() || IsHighContrast())
		return CExtPaintManagerStudio2005::PaintMenuItemBackgroundXP(dc, _pmid, rcSelectionArea);
	if (!_pmid.m_bSelected)
		return false;
	m_bmpMenuItemSelectionArea.AlphaBlendSkinParts(
		dc.m_hDC,
		rcSelectionArea,
		CRect(3, 3, 3, 3),
		CExtBitmap::__EDM_STRETCH,
		true,
		false
		);
	return true;
}

CExtPaintManager::SCROLLBARSKINDATA * CExtPaintManagerStudio2010::ScrollBar_GetSkinData(
	bool bHorzBar,
	CObject * pHelperSrc,
	LPARAM lParam, // = 0L
	bool bLightAccent // = true
	)
{
	ASSERT_VALID(this);
	if (IsHighContrast())
		return CExtPaintManagerStudio2005::ScrollBar_GetSkinData(bHorzBar, pHelperSrc, lParam, bLightAccent);
	if (pHelperSrc != NULL)
	{
		ASSERT_VALID(pHelperSrc);
		if (pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtZoomScrollBar)))
			return CExtPaintManagerStudio2005::ScrollBar_GetSkinData(bHorzBar, pHelperSrc, lParam, bLightAccent);
	} // if( pHelperSrc != NULL )
	if (bLightAccent)
		return bHorzBar ? (&m_SbSkinDataH_Light) : (&m_SbSkinDataV_Light);
	else
		return bHorzBar ? (&m_SbSkinDataH_Dark) : (&m_SbSkinDataV_Dark);
}

#if (!defined __EXT_MFC_NO_TAB_CTRL)

CExtPaintManagerStudio2010::vs_2010_tab_item_def_t::vs_2010_tab_item_def_t()
{
	int i;
	for (i = 0; i < (sizeof(m_clrGradient) / sizeof(m_clrGradient[0])); i++)
		m_clrGradient[i] = __EXT_MFC_RGBA(255, 255, 255, 0);
	m_clrText = m_clrBorder = __EXT_MFC_RGBA(255, 255, 255, 0);
	m_rcBmpPadding.SetRect(3, 3, 3, 3);
}

void CExtPaintManagerStudio2010::vs_2010_tab_item_def_t::GenerateBmp()
{
	m_bmpTabItem.Empty();
	CSize sizeBmp(m_rcBmpPadding.left + m_rcBmpPadding.right + 32, m_rcBmpPadding.top + m_rcBmpPadding.bottom + 32);
	if (m_clrGradient[0] != __EXT_MFC_RGBA(255, 255, 255, 0)
		&& m_bmpTabItem.FromColor(m_clrGradient[0], sizeBmp)
		&& m_bmpTabItem.Make32()
		)
	{
		if ((m_clrGradient[2] == __EXT_MFC_RGBA(255, 255, 255, 0) && m_clrGradient[3] == __EXT_MFC_RGBA(255, 255, 255, 0))
			|| m_clrGradient[1] == __EXT_MFC_RGBA(255, 255, 255, 0)
			)
		{
			COLORREF clr1 = RGB(GetRValue(m_clrGradient[0]), GetGValue(m_clrGradient[0]), GetBValue(m_clrGradient[0]));
			BYTE alpha1 = __EXT_MFC_GetAValue(m_clrGradient[0]);
			COLORREF clr2 = clr1;
			BYTE alpha2 = alpha1;
			if (m_clrGradient[1] != __EXT_MFC_RGBA(255, 255, 255, 0))
			{
				clr2 = RGB(GetRValue(m_clrGradient[1]), GetGValue(m_clrGradient[1]), GetBValue(m_clrGradient[1]));
				alpha2 = __EXT_MFC_GetAValue(m_clrGradient[1]);
			}
			CRect rcFill(0, 0, sizeBmp.cx, sizeBmp.cy);
			m_bmpTabItem.GradientRect(clr2, clr1, true, rcFill, 256, alpha2, alpha1);
		}
		else
		{
			CRect rcFillTop(0, 0, sizeBmp.cx, sizeBmp.cy / 2), rcFillBottom(0, sizeBmp.cy / 2, sizeBmp.cx, sizeBmp.cy);
			COLORREF clr1 = RGB(GetRValue(m_clrGradient[0]), GetGValue(m_clrGradient[0]), GetBValue(m_clrGradient[0]));
			BYTE alpha1 = __EXT_MFC_GetAValue(m_clrGradient[0]);
			COLORREF clr2 = RGB(GetRValue(m_clrGradient[1]), GetGValue(m_clrGradient[1]), GetBValue(m_clrGradient[1]));
			BYTE alpha2 = __EXT_MFC_GetAValue(m_clrGradient[1]);
			COLORREF clr3 = RGB(GetRValue(m_clrGradient[2]), GetGValue(m_clrGradient[2]), GetBValue(m_clrGradient[2]));
			BYTE alpha3 = __EXT_MFC_GetAValue(m_clrGradient[2]);
			COLORREF clr4 = RGB(GetRValue(m_clrGradient[3]), GetGValue(m_clrGradient[3]), GetBValue(m_clrGradient[3]));
			BYTE alpha4 = __EXT_MFC_GetAValue(m_clrGradient[3]);
			m_bmpTabItem.GradientRect(clr2, clr1, true, rcFillTop, 256, alpha2, alpha1);
			m_bmpTabItem.GradientRect(clr4, clr3, true, rcFillBottom, 256, alpha4, alpha3);
		}
		if (!m_bmpTabItem.IsEmpty())
		{
			if (m_clrBorder != __EXT_MFC_RGBA(255, 255, 255, 0))
			{
				COLORREF clr = RGB(GetRValue(m_clrBorder), GetGValue(m_clrBorder), GetBValue(m_clrBorder));
				BYTE alpha = __EXT_MFC_GetAValue(m_clrBorder);
				int i;
				for (i = 1; i <= (sizeBmp.cx - 2); i++)
					m_bmpTabItem.SetPixel(i, 0, clr, alpha);
				for (i = 1; i < sizeBmp.cy; i++)
				{
					m_bmpTabItem.SetPixel(0, i, clr, alpha);
					m_bmpTabItem.SetPixel(sizeBmp.cx - 1, i, clr, alpha);
				}
			}
			static INT g_nAlphaCornerMiddle = 128, g_nAlphaCornerLight = 64;
			m_bmpTabItem.SetPixel(0, 0, m_bmpTabItem.GetPixel(0, 0), g_nAlphaCornerLight);
			m_bmpTabItem.SetPixel(0, 1, m_bmpTabItem.GetPixel(0, 1), g_nAlphaCornerMiddle);
			m_bmpTabItem.SetPixel(1, 0, m_bmpTabItem.GetPixel(1, 0), g_nAlphaCornerMiddle);
			m_bmpTabItem.SetPixel(sizeBmp.cx - 1, 0, m_bmpTabItem.GetPixel(sizeBmp.cx - 1, 0), g_nAlphaCornerLight);
			m_bmpTabItem.SetPixel(sizeBmp.cx - 1, 1, m_bmpTabItem.GetPixel(sizeBmp.cx - 1, 1), g_nAlphaCornerMiddle);
			m_bmpTabItem.SetPixel(sizeBmp.cx - 2, 0, m_bmpTabItem.GetPixel(sizeBmp.cx - 2, 0), g_nAlphaCornerMiddle);
		}
	}
}

void CExtPaintManagerStudio2010::vs_2010_tab_item_def_t::CreateRotatedVersionFromTopOrientedVersion(
	const vs_2010_tab_item_def_t & _srcDef,
	INT nTargetVersionType // CExtPaintManagerOffice2007_Impl::__ETSOI_***
	)
{
	m_clrText = _srcDef.m_clrText;
	m_clrBorder = _srcDef.m_clrBorder;
	int i;
	for (i = 0; i < (sizeof(m_clrGradient) / sizeof(m_clrGradient[0])); i++)
		m_clrGradient[i] = _srcDef.m_clrGradient[i];
	m_rcBmpPadding = _srcDef.m_rcBmpPadding;
	switch (nTargetVersionType)
	{
	case INT(CExtPaintManagerOffice2007_Impl::__ETSOI_BOTTOM):
		i = m_rcBmpPadding.top; m_rcBmpPadding.top = m_rcBmpPadding.bottom; m_rcBmpPadding.bottom = i;
		i = m_rcBmpPadding.left; m_rcBmpPadding.left = m_rcBmpPadding.right; m_rcBmpPadding.right = i;
		break;
	case INT(CExtPaintManagerOffice2007_Impl::__ETSOI_RIGHT):
		i = m_rcBmpPadding.top;
		m_rcBmpPadding.top = m_rcBmpPadding.left;
		m_rcBmpPadding.left = m_rcBmpPadding.bottom;
		m_rcBmpPadding.bottom = m_rcBmpPadding.right;
		m_rcBmpPadding.right = i;
		break;
	case INT(CExtPaintManagerOffice2007_Impl::__ETSOI_LEFT):
		i = m_rcBmpPadding.top;
		m_rcBmpPadding.top = m_rcBmpPadding.right;
		m_rcBmpPadding.right = m_rcBmpPadding.bottom;
		m_rcBmpPadding.bottom = m_rcBmpPadding.left;
		m_rcBmpPadding.left = i;
		break;
	}
	if ((!_srcDef.m_bmpTabItem.IsEmpty()) || (!_srcDef.m_bmpNearItemsArea.IsEmpty()))
	{
		INT nAngleCw90 = 0;
		switch (nTargetVersionType)
		{
		case INT(CExtPaintManagerOffice2007_Impl::__ETSOI_BOTTOM):	nAngleCw90 = 180; break;
		case INT(CExtPaintManagerOffice2007_Impl::__ETSOI_RIGHT):	nAngleCw90 = 90;  break;
		case INT(CExtPaintManagerOffice2007_Impl::__ETSOI_LEFT):	nAngleCw90 = 270; break;
		}
		if (!_srcDef.m_bmpTabItem.IsEmpty())
		{
			CSize sizeBmp = _srcDef.m_bmpTabItem.GetSize();
			m_bmpTabItem.CreateRotated9xStack(_srcDef.m_bmpTabItem, nAngleCw90, 1, sizeBmp.cx, sizeBmp.cy, true, true);
		}
		if (!_srcDef.m_bmpNearItemsArea.IsEmpty())
		{
			CSize sizeBmp = _srcDef.m_bmpNearItemsArea.GetSize();
			m_bmpTabItem.CreateRotated9xStack(_srcDef.m_bmpNearItemsArea, nAngleCw90, 1, sizeBmp.cx, sizeBmp.cy, true, true);
		}
	}
}

void CExtPaintManagerStudio2010::PaintTabItem(
	CDC & dc,
	CRect & rcTabItemsArea,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon,
	CObject * pHelperSrc,
	LPARAM lParam, // = 0L
	COLORREF clrForceText, // = COLORREF(-1L)
	COLORREF clrForceTabBk, // = COLORREF(-1L)
	COLORREF clrForceTabBorderLT, // = COLORREF(-1L)
	COLORREF clrForceTabBorderRB, // = COLORREF(-1L)
	COLORREF clrForceTabSeparator, // = COLORREF(-1L)
	bool bDwmMode // = false
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	if (IsHighContrast())
	{
		CExtPaintManagerStudio2005::PaintTabItem(
			dc,
			rcTabItemsArea,
			bTopLeft,
			bHorz,
			bSelected,
			bCenteredText,
			bGroupedMode,
			bInGroupActive,
			bInvertedVerticalMode,
			rcEntireItem,
			sizeTextMeasured,
			pFont,
			sText,
			pIcon,
			pHelperSrc,
			lParam,
			clrForceText,
			clrForceTabBk,
			clrForceTabBorderLT,
			clrForceTabBorderRB,
			clrForceTabSeparator,
			bDwmMode
			);
		return;
	}

	CExtCmdIcon * pIconTabItemCloseButton = NULL;
	CExtCmdIcon::e_paint_type_t ePaintStateITICB =
		(CExtCmdIcon::e_paint_type_t) CExtCmdIcon::__PAINT_INVISIBLE;
	CRect rcTabItemCloseButton(0, 0, 0, 0);
	CExtBarButton * pTBB = NULL;
	CExtTabWnd * pTabs = NULL;
	CExtTabWnd::TAB_ITEM_INFO * pTII = NULL;
	bool bNoPrefix = false;
	if (pHelperSrc != NULL)
	{
		pTabs = DYNAMIC_DOWNCAST(CExtTabWnd, pHelperSrc);
		if (pTabs != NULL)
		{
			ASSERT_VALID(pTabs);
			pTII = pTabs->ItemGet(LONG(lParam));
			ASSERT(pTII != NULL);
			ASSERT_VALID(pTII);
			pIconTabItemCloseButton =
				pTabs->OnTabWndQueryItemCloseButtonShape(pTII);
			if (pIconTabItemCloseButton != NULL)
			{
				rcTabItemCloseButton = pTII->CloseButtonRectGet();
				ePaintStateITICB = (CExtCmdIcon::e_paint_type_t)
					pTabs->OnTabWndQueryItemCloseButtonPaintState(pTII);
			} // if( pIconTabItemCloseButton != NULL )
			bNoPrefix = ((pTabs->GetTabWndStyleEx() & __ETWS_EX_NO_PREFIX) != 0) ? true : false;
		} // if( pTabs != NULL )
		else
		{
			pTBB = DYNAMIC_DOWNCAST(CExtBarButton, pHelperSrc);
#ifdef _DEBUG
			if (pTBB != NULL)
			{
				ASSERT_VALID(pTBB);
	} // if( pTBB != NULL )
#endif // _DEBUG
#if (!defined __EXT_MFC_NO_RIBBON_BAR)
			CExtToolControlBar * pBar = pTBB->GetSafeBar();
			if (pBar != NULL && pBar->IsKindOf(RUNTIME_CLASS(CExtRibbonBar)))
			{
				CExtPaintManagerStudio2005::PaintTabItem(
					dc,
					rcTabItemsArea,
					bTopLeft,
					bHorz,
					bSelected,
					bCenteredText,
					bGroupedMode,
					bInGroupActive,
					bInvertedVerticalMode,
					rcEntireItem,
					sizeTextMeasured,
					pFont,
					sText,
					pIcon,
					pHelperSrc,
					lParam,
					clrForceText,
					clrForceTabBk,
					clrForceTabBorderLT,
					clrForceTabBorderRB,
					clrForceTabSeparator,
					bDwmMode
					);
				return;
			}
#endif // (!defined __EXT_MFC_NO_RIBBON_BAR)
} // else from if( pTabs != NULL )
	} // if( pHelperSrc != NULL )

	LPCTSTR _sText = LPCTSTR(sText);

	bool bDrawIcon = (
		pIcon != NULL
		&& (!pIcon->IsEmpty())
		&& (pTabs == NULL || (pTabs->GetTabWndStyle()&__ETWS_HIDE_ICONS) == 0)
		);
	if (bGroupedMode)
		bSelected = false;

	bool bEnabled = true;
	if (pTII != NULL)
		bEnabled = pTII->EnabledGet();
	else if (pTBB != NULL)
		bEnabled = pTBB->IsEnabled();

	INT nTabShapeIndex = INT(__ETI2010_DISABLED); // for disabled state
	if (bEnabled)
	{
		nTabShapeIndex = INT(__ETI2010_NORMAL);
		if (!bGroupedMode)
		{
			if (bSelected)
				nTabShapeIndex = INT(__ETI2010_SELECTED);
			else
			{
				if (pTII != NULL)
				{
					if (pTabs->GetHoverTrackingItem() == lParam)
						nTabShapeIndex = INT(__ETI2010_HOVER);
				}
				else if (pTBB != NULL)
				{
					if (pTBB->IsHover())
						nTabShapeIndex = INT(__ETI2010_HOVER);
				}
			}
		}
		else
		{
			nTabShapeIndex = INT(__ETI2010_NORMAL_GROUPPED);
			if (pTII != NULL)
			{
				if (pTabs->GetHoverTrackingItem() == lParam)
					nTabShapeIndex = INT(__ETI2010_HOVER);
			}
		}
	}

	bool bDetectedDynTPC = false;
	bDetectedDynTPC;
#if (!defined __EXT_MFC_NO_DYNAMIC_BAR_SITE)
	if (pTII != NULL)
	{
		CObject * pObject = pTII->EventProviderGet();
		if (pObject != NULL)
		{
			CExtDynamicControlBar * pBar = DYNAMIC_DOWNCAST(CExtDynamicControlBar, pObject);
			if (pBar != NULL)
			{
				bool bFlashCaptionHighlightedState = false;
				if (pBar->FlashCaptionIsInProgress(&bFlashCaptionHighlightedState))
				{
					if (bFlashCaptionHighlightedState)
					{
						clrForceText = pBar->m_clrFlashCaptionText;
						clrForceTabBk = pBar->m_clrFlashCaptionBackground;
						bSelected = true;
#if (!defined __EXT_MFC_NO_TAB_CONTROLBARS)
						bDetectedDynTPC = true;
#endif // (!defined __EXT_MFC_NO_TAB_CONTROLBARS)
					}
				}
			}
		}
	}
#endif// (!defined __EXT_MFC_NO_DYNAMIC_BAR_SITE)

	CExtPaintManagerOffice2007_Impl::e_TabShapeOrientationIndex_t eTSOI = CExtPaintManagerOffice2007_Impl::__ETSOI_TOP;
	if (bTopLeft)
	{
		if (bHorz)
			eTSOI = CExtPaintManagerOffice2007_Impl::__ETSOI_TOP;
		else
			eTSOI = CExtPaintManagerOffice2007_Impl::__ETSOI_LEFT;
	}
	else
	{
		if (bHorz)
			eTSOI = CExtPaintManagerOffice2007_Impl::__ETSOI_BOTTOM;
		else
			eTSOI = CExtPaintManagerOffice2007_Impl::__ETSOI_RIGHT;
	}
	if (bGroupedMode)
	{
		switch (eTSOI)
		{
		case CExtPaintManagerOffice2007_Impl::__ETSOI_TOP:
			eTSOI = CExtPaintManagerOffice2007_Impl::__ETSOI_BOTTOM;
			break;
		case CExtPaintManagerOffice2007_Impl::__ETSOI_BOTTOM:
			eTSOI = CExtPaintManagerOffice2007_Impl::__ETSOI_TOP;
			break;
		case CExtPaintManagerOffice2007_Impl::__ETSOI_LEFT:
			eTSOI = CExtPaintManagerOffice2007_Impl::__ETSOI_RIGHT;
			break;
		case CExtPaintManagerOffice2007_Impl::__ETSOI_RIGHT:
			eTSOI = CExtPaintManagerOffice2007_Impl::__ETSOI_LEFT;
			break;
		}
	}

	COLORREF clrText = COLORREF(-1L);
	if (clrText == COLORREF(-1L))
		clrText = QueryObjectTextColor(dc, bEnabled, false, (nTabShapeIndex == INT(__ETI2010_HOVER)) ? true : false, false, pHelperSrc, lParam);
	if (clrText == COLORREF(-1L))
		clrText = m_tabDefs[nTabShapeIndex][INT(eTSOI)].m_clrText;
	if (clrText == COLORREF(-1L))
		clrText = RGB(0, 0, 0);


	COLORREF clrColorizeTabShape = COLORREF(-1L);
#if (!defined __EXT_MFC_NO_TAB_CONTROLBARS)
	if (pHelperSrc != NULL
		&&	clrForceTabBk != COLORREF(-1L)
		&& (bDetectedDynTPC
		|| pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtDynTabWnd))
		|| pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtDynAutoHideArea))
		)
		)
	{
		if (clrForceText != COLORREF(-1L))
			clrText = clrForceText;
		clrColorizeTabShape = clrForceTabBk;
	}
#endif // (!defined __EXT_MFC_NO_TAB_CONTROLBARS)
#if (!defined __EXT_MFC_NO_TABMDI_CTRL)
	if (pHelperSrc != NULL
		&&	clrForceTabBk != COLORREF(-1L)
		&& pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtTabWnd))
		&& ((CExtTabWnd*) pHelperSrc)->_IsMdiTabCtrl()
		)
	{
		if (clrForceText != COLORREF(-1L))
			clrText = clrForceText;
		clrColorizeTabShape = clrForceTabBk;
	}
#endif // (!defined __EXT_MFC_NO_TABMDI_CTRL)
	if (nTabShapeIndex >= 0)
	{
#if (!defined __EXT_MFC_NO_RIBBON_BAR)
		if (pTBB != NULL
			&&	pTBB->IsKindOf(RUNTIME_CLASS(CExtRibbonButtonTabPage))
			&& ((CExtRibbonButtonTabPage*) pTBB)->IsSelectedRibbonPage()
			)
		{
			CExtToolControlBar * pToolBar = pTBB->GetBar();
			if (pToolBar != NULL)
			{
				CExtMenuControlBar * pMenuBar = DYNAMIC_DOWNCAST(CExtMenuControlBar, pToolBar);
				if (pMenuBar != NULL
					&&	pMenuBar->IsMenuBarTracking()
					)
				{
					int nFlatTrackingIndex = pMenuBar->_FlatTrackingIndexGet();
					if (nFlatTrackingIndex >= 0)
					{
						int nIndexOfTBB = pMenuBar->_GetIndexOf(pTBB);
						if (nFlatTrackingIndex == nIndexOfTBB)
							nTabShapeIndex = INT(__ETI2010_SELECTED_ACTIVE);
					}
				}
			}
		}
#endif // (!defined __EXT_MFC_NO_RIBBON_BAR)
		const CExtBitmap & _bmpTabShape = m_tabDefs[nTabShapeIndex][INT(eTSOI)].m_bmpTabItem;
		if (!_bmpTabShape.IsEmpty())
		{
			CSize sizeBmp = _bmpTabShape.GetSize();
			DWORD dwDcLayout = dc.GetLayout();
			if (bDwmMode)
				dc.SetLayout(LAYOUT_LTR);
			CRect rcPadding = m_tabDefs[nTabShapeIndex][INT(eTSOI)].m_rcBmpPadding;
			CRect rcSrc(0, 0, sizeBmp.cx, sizeBmp.cy);
			CRect rcPaintTabShape = rcEntireItem;
			switch (eTSOI)
			{
			case CExtPaintManagerOffice2007_Impl::__ETSOI_TOP:
			case CExtPaintManagerOffice2007_Impl::__ETSOI_BOTTOM:
				rcPaintTabShape.right--;
				break;
			case CExtPaintManagerOffice2007_Impl::__ETSOI_LEFT:
			case CExtPaintManagerOffice2007_Impl::__ETSOI_RIGHT:
				rcPaintTabShape.bottom--;
				break;
			}
			//dc.FillSolidRect( &rcPaintTabShape, m_tabDefs[ nTabShapeIndex ][ INT(eTSOI) ].m_clrGradient[0] );
			if (clrColorizeTabShape != COLORREF(-1L))
			{
				CExtBitmap _bmpTabShape2 = _bmpTabShape;
				_bmpTabShape2.Make32();
				_bmpTabShape2.AdjustHLS(COLORREF(-1L), COLORREF(-1L), 0.0, -0.5, -1.0);
				_bmpTabShape2.MakeMono(clrColorizeTabShape);
				_bmpTabShape2.AlphaBlendSkinParts(
					dc.m_hDC,
					rcPaintTabShape,
					rcSrc,
					rcPadding,
					CExtBitmap::__EDM_STRETCH,
					true,
					true
					);
			}
			else
				_bmpTabShape.AlphaBlendSkinParts(
				dc.m_hDC,
				rcPaintTabShape,
				rcSrc,
				rcPadding,
				CExtBitmap::__EDM_STRETCH,
				true,
				true
				);
			dc.SetLayout(dwDcLayout);
		}
	}
	CRect rcEntireItemX = rcEntireItem;
	//	rcEntireItemX.DeflateRect( 3, 3 );

	CExtPaintManager::stat_PaintTabItemImpl(
		dc,
		rcTabItemsArea,
		bTopLeft,
		bHorz,
		bSelected,
		bEnabled,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		bDrawIcon,
		rcEntireItemX,
		sizeTextMeasured,
		pFont,
		_sText,
		bNoPrefix,
		pIcon,
		//		bTopLeft
		//			? ( bSelected ? RGB(255,255,255) : RGB(0,0,0) )
		//			: ( bSelected ? RGB(0,0,0) : RGB(255,255,255) )
		pIconTabItemCloseButton,
		INT(ePaintStateITICB),
		rcTabItemCloseButton,
		clrText,
		COLORREF(-1L), // clrTabBk,
		COLORREF(-1L), // clrTabBorderLT,
		COLORREF(-1L), // clrTabBorderRB,
		COLORREF(-1L),  // clrTabSeparator
		(pTBB != NULL) ? false : true,
		pHelperSrc,
		bDwmMode
		);

	/*
	const CExtBitmap & _bmpTabSeparator = m_arrBmpTabSeparator[ int(eTSOI) ];
	if( ! _bmpTabSeparator.IsEmpty() )
	{
	CRect rcPaintSeparator = rcEntireItem;
	CRect rcPadding = m_arrRectTabSeparatorPadding[ int(eTSOI) ];
	CRect rcSrc( 0, 0, m_arrSizeTabSeparator[int(eTSOI)].cx, m_arrSizeTabSeparator[int(eTSOI)].cy );
	e_paint_manager_name_t ePMN = OnQueryPaintManagerName();
	switch( eTSOI )
	{
	case CExtPaintManagerOffice2007_Impl::__ETSOI_TOP:
	rcPaintSeparator.left = rcPaintSeparator.right - m_arrSizeTabSeparator[int(eTSOI)].cx;
	rcPaintSeparator.bottom --;
	if( ePMN == Office2007_R2_Obsidian )
	rcPaintSeparator.bottom -= 2;
	break;
	case CExtPaintManagerOffice2007_Impl::__ETSOI_BOTTOM:
	rcPaintSeparator.left = rcPaintSeparator.right - m_arrSizeTabSeparator[int(eTSOI)].cx;
	rcPaintSeparator.top ++;
	break;
	case CExtPaintManagerOffice2007_Impl::__ETSOI_LEFT:
	rcPaintSeparator.top = rcPaintSeparator.bottom - m_arrSizeTabSeparator[int(eTSOI)].cy;
	rcPaintSeparator.right --;
	break;
	case CExtPaintManagerOffice2007_Impl::__ETSOI_RIGHT:
	rcPaintSeparator.top = rcPaintSeparator.bottom - m_arrSizeTabSeparator[int(eTSOI)].cy;
	//			rcPaintSeparator.top ++;
	rcPaintSeparator.left ++;
	break;
	}
	BYTE nSCA = BYTE(0x0FF);
	#if (!defined __EXT_MFC_NO_RIBBON_BAR)
	if( pTBB != NULL )
	{
	CExtRibbonButtonTabPage * pRibbonButtonTabPage =
	DYNAMIC_DOWNCAST( CExtRibbonButtonTabPage, pTBB );
	if( pRibbonButtonTabPage != NULL )
	nSCA = pRibbonButtonTabPage->Get2007SeparatorAlpha();
	} // if( pTBB != NULL )
	#endif // (!defined __EXT_MFC_NO_RIBBON_BAR)
	DWORD dwDcLayout = dc.GetLayout();
	if( bDwmMode )
	dc.SetLayout( LAYOUT_LTR );
	_bmpTabSeparator.AlphaBlendSkinParts(
	dc.m_hDC,
	rcPaintSeparator,
	rcSrc,
	rcPadding,
	CExtBitmap::__EDM_STRETCH,
	true,
	true,
	nSCA
	);
	dc.SetLayout( dwDcLayout );
	} // if( ! _bmpTabSeparator.IsEmpty() )
	*/
}

void CExtPaintManagerStudio2010::PaintTabClientArea(
	CDC & dc,
	CRect & rcClient,
	CRect & rcTabItemsArea,
	CRect & rcTabNearBorderArea,
	DWORD dwOrientation,
	bool bGroupedMode,
	CObject * pHelperSrc,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	if (IsHighContrast()
		|| pHelperSrc == NULL
		|| ((!pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtTabWnd)))
		&& (!pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtThemedTabCtrl)))
		)
		)
	{
		CExtPaintManagerStudio2005::PaintTabClientArea(
			dc,
			rcClient,
			rcTabItemsArea,
			rcTabNearBorderArea,
			dwOrientation,
			bGroupedMode,
			pHelperSrc,
			lParam
			);
		return;
	}
	PaintDockerBkgnd(true, dc, (CWnd*) pHelperSrc, lParam);
	if (pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtThemedTabCtrl)))
	{
		CRect rcDoc(rcTabItemsArea.left, rcTabItemsArea.top, rcTabItemsArea.right, rcTabNearBorderArea.top);
		CExtMemoryDC dcMem(&dc, &rcDoc);
		PaintDocumentClientAreaBkgnd(dcMem, (CWnd*) pHelperSrc, lParam);
	}
	if (!bGroupedMode)
	{
		COLORREF clrTabNearBorderAreaBk = m_tabDefs[INT(__ETI2010_SELECTED)][INT(CExtPaintManagerOffice2007_Impl::__ETSOI_TOP)].m_clrGradient[3];
		if (clrTabNearBorderAreaBk != __EXT_MFC_RGBA(255, 255, 255, 0))
		{
			clrTabNearBorderAreaBk = RGB(GetRValue(clrTabNearBorderAreaBk), GetGValue(clrTabNearBorderAreaBk), GetBValue(clrTabNearBorderAreaBk));
			dc.FillSolidRect(&rcTabNearBorderArea, clrTabNearBorderAreaBk);
		}
	}
}

void CExtPaintManagerStudio2010::PaintTabButton(
	CDC & dc,
	CRect & rcButton,
	LONG nHitTest,
	bool bTopLeft,
	bool bHorz,
	bool bEnabled,
	bool bHover,
	bool bPushed,
	bool bGroupedMode,
	CObject * pHelperSrc,
	LPARAM lParam, // = 0L
	bool bFlat // = false
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	//	if( IsHighContrast() )
	{
		CExtPaintManagerStudio2005::PaintTabButton(
			dc,
			rcButton,
			nHitTest,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			bPushed,
			bGroupedMode,
			pHelperSrc,
			lParam,
			bFlat
			);
		return;
	}
}

void CExtPaintManagerStudio2010::PaintTabNcAreaRect(
	CDC & dc,
	const RECT & rc,
	CObject * pHelperSrc,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	if (IsHighContrast()
		|| pHelperSrc == NULL
		|| (!pHelperSrc->IsKindOf(RUNTIME_CLASS(CWnd)))
		)
	{
		CExtPaintManagerStudio2005::PaintTabNcAreaRect(dc, rc, pHelperSrc, lParam);
		return;
	}
	PaintDockerBkgnd(false, dc, (CWnd*) pHelperSrc, lParam);
}

bool CExtPaintManagerStudio2010::QueryTabWndHoverChangingRedraw(
	const CExtTabWnd * pWndTab,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID(this);
	ASSERT_VALID(pWndTab);
	if (IsHighContrast())
		return CExtPaintManagerStudio2005::QueryTabWndHoverChangingRedraw(pWndTab, lParam);
	return true;
}

void CExtPaintManagerStudio2010::TabWnd_MeasureItemAreaMargins(
	CExtTabWnd * pTabWnd,
	LONG & nSpaceBefore,
	LONG & nSpaceAfter,
	LONG & nSpaceOver
	)
{
	ASSERT_VALID(this);
	ASSERT(pTabWnd != NULL && ::IsWindow(pTabWnd->m_hWnd));
	ASSERT_VALID(pTabWnd);
	if (IsHighContrast())
	{
		CExtPaintManagerStudio2005::TabWnd_MeasureItemAreaMargins(pTabWnd, nSpaceBefore, nSpaceAfter, nSpaceOver);
		return;
	}
	nSpaceAfter = 4;
	nSpaceBefore = 4;
	nSpaceOver = 2;
}

void CExtPaintManagerStudio2010::TabWnd_UpdateItemMeasure(
	CExtTabWnd * pTabWnd,
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	CDC & dcMeasure,
	CSize & sizePreCalc
	)
{
	ASSERT_VALID(this);
	ASSERT(pTabWnd != NULL && ::IsWindow(pTabWnd->m_hWnd));
	ASSERT_VALID(pTabWnd);
	ASSERT_VALID(pTii);
	ASSERT(dcMeasure.GetSafeHdc() != NULL);
	ASSERT(pTii->GetTabWnd() == pTabWnd);
	if (IsHighContrast())
	{
		CExtPaintManagerStudio2005::TabWnd_UpdateItemMeasure(pTabWnd, pTii, dcMeasure, sizePreCalc);
		return;
	}
	sizePreCalc.cx += 2;
	sizePreCalc.cy += 2;
	if (pTabWnd->OrientationIsHorizontal())
		sizePreCalc.cx += 2;
	else
		sizePreCalc.cy += 2;
}

bool CExtPaintManagerStudio2010::TabWnd_GetParentSizingMargin(
	INT & nMargin,
	DWORD dwOrientation,
	CExtTabWnd * pTabWnd
	) const
{
	ASSERT_VALID(this);
	ASSERT_VALID(pTabWnd);
	nMargin; dwOrientation; pTabWnd;
	return false;
}

bool CExtPaintManagerStudio2010::PaintTabMdiOuterBorder(
	CDC & dc,
	const CRect & rcOuterBorder,
	const CRect & rcMdiAreaClient,
	const CRect & rcMdiAreaWnd,
	HWND hWndHooked,
	CExtTabWnd * pTabWnd,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	ASSERT(pTabWnd->GetSafeHwnd() != NULL);
	ASSERT(hWndHooked != NULL && ::IsWindow(hWndHooked));
	rcOuterBorder; rcMdiAreaClient; rcMdiAreaWnd; pTabWnd;
	PaintDockerBkgnd(false, dc, CWnd::FromHandle(hWndHooked), lParam);
	return true;
}

#endif // #if (!defined __EXT_MFC_NO_TAB_CTRL)


bool CExtPaintManagerStudio2010::PaintDockerBkgnd(
	bool bClientMapping,
	CDC & dc,
	CWnd * pWnd,
	LPARAM lParam // = NULL
	)
{
	ASSERT_VALID(this);
	if (IsHighContrast())
		return CExtPaintManagerStudio2005::PaintDockerBkgnd(bClientMapping, dc, pWnd, lParam);
	if (RenderCustomBackground(bClientMapping, dc, pWnd, lParam))
		return true;
	if (!GetCb2DbTransparentMode(pWnd))
		return false;
	bool bToolbarShade = false, bToolbarHorz = false, bMenuBarShade = false, bOuterDockBarShade = false, bDialogShade = true;
	CWnd * pFrame = pWnd;
	for (; pFrame != NULL; pFrame = pFrame->GetParent())
	{
		if ((pFrame->GetStyle() & WS_CHILD) == 0)
			break;
		if (pFrame->IsKindOf(RUNTIME_CLASS(CFrameWnd)))
		{
			bDialogShade = false;
			break;
		}
		if (pFrame->IsKindOf(RUNTIME_CLASS(CExtDockOuterBar)))
		{
			bDialogShade = false;
			bOuterDockBarShade = true;
			break;
		}
		if (pFrame->IsKindOf(RUNTIME_CLASS(CExtToolControlBar)))
		{
			if (pFrame->IsKindOf(RUNTIME_CLASS(CExtPanelControlBar)))
			{
				pFrame = pWnd;
				bToolbarShade = true;
				DWORD dwBarStyle = ((CExtToolControlBar*) pFrame)->GetBarStyle();
				bToolbarHorz = ((dwBarStyle & (CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT)) != 0) ? false : true;
				break;
			}
			if (((CExtToolControlBar*) pFrame)->m_pDockSite != NULL)
			{
				if (		/*
								(!(	pFrame->IsKindOf( RUNTIME_CLASS(CExtPanelControlBar) )
								||	pFrame->IsKindOf( RUNTIME_CLASS(CExtMenuControlBar) )
								) )
								&&*/
								(!((CExtToolControlBar*) pFrame)->m_bForceNoBalloonWhenRedockable)
								)
				{
					if (((CExtToolControlBar*) pFrame)->m_pDockBar == NULL)
					{
						DWORD dwBarStyle = ((CExtToolControlBar*) pFrame)->GetBarStyle();
						bToolbarHorz = ((dwBarStyle & (CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT)) != 0) ? false : true;
						if (((CExtToolControlBar*) pFrame)->m_bForceBalloonGradientInDialogs != 0)
							bToolbarShade = true;
						else
							bMenuBarShade = true;
						break;
					}
					if (!((CExtToolControlBar*) pFrame)->IsFloating())
					{
						bToolbarHorz = ((CExtToolControlBar*) pFrame)->IsDockedHorizontally();
						if (!pFrame->IsKindOf(RUNTIME_CLASS(CExtMenuControlBar)))
							bToolbarShade = true;
						else
							bMenuBarShade = true;
					}
					else if (((CExtToolControlBar*) pFrame)->m_bPaletteMode || pFrame->IsKindOf(RUNTIME_CLASS(CExtPanelControlBar)))
						bMenuBarShade = bToolbarHorz = true;
					break;
				}
				else
				{
					if (((CExtToolControlBar*) pFrame)->m_bPaletteMode || pFrame->IsKindOf(RUNTIME_CLASS(CExtPanelControlBar)))
					{
						bMenuBarShade = bToolbarHorz = true;
						break;
					}
					DWORD dwBarStyle = ((CExtToolControlBar*) pFrame)->GetBarStyle();
					bToolbarHorz = ((dwBarStyle & (CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT)) != 0) ? false : true;
					if (((CExtToolControlBar*) pFrame)->m_bForceBalloonGradientInDialogs != 0)
						bToolbarShade = true;
					else
						bMenuBarShade = true;
					break;
				}
			}
			else
			{
				DWORD dwBarStyle = ((CExtToolControlBar*) pFrame)->GetBarStyle();
				bToolbarHorz = ((dwBarStyle & (CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT)) != 0) ? false : true;
				if (((CExtToolControlBar*) pFrame)->m_bForceBalloonGradientInDialogs != 0)
					bToolbarShade = true;
				else
					bMenuBarShade = true;
				break;
			}
		} // if( pFrame->IsKindOf( RUNTIME_CLASS(CExtToolControlBar) ) )
		if (pFrame->IsKindOf(RUNTIME_CLASS(CDialog))
			|| pFrame->IsKindOf(RUNTIME_CLASS(CFormView))
			)
		{
			CWnd * pWnd = pFrame->GetParent();
			if (pWnd != NULL && pWnd->IsKindOf(RUNTIME_CLASS(CExtPanelControlBar)))
			{
				pFrame = pWnd;
				bToolbarShade = true;
				DWORD dwBarStyle = ((CExtToolControlBar*) pFrame)->GetBarStyle();
				bToolbarHorz = ((dwBarStyle & (CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT)) != 0) ? false : true;
			}
			break;
		}
		if (pFrame->IsKindOf(RUNTIME_CLASS(CExtControlBar))
			&& ((CExtControlBar*) pFrame)->m_pDockSite != NULL
			&& (!((CExtControlBar*) pFrame)->IsFixedMode())
			)
			break;
	} // for( ; pFrame != NULL; pFrame = pFrame->GetParent() )
	if (pFrame == NULL)
		return false;
	if (bToolbarShade || bMenuBarShade || bOuterDockBarShade)
		bDialogShade = false;
#if (!defined __EXT_MFC_NO_TAB_CTRL )
	if (pWnd->IsKindOf(RUNTIME_CLASS(CExtTabWnd)))
		bToolbarShade = bMenuBarShade = bOuterDockBarShade = bDialogShade = false;
#endif // (!defined __EXT_MFC_NO_TAB_CTRL )
	if (bDialogShade)
	{
		CRect rcPaintGradient;
		pFrame->GetWindowRect(&rcPaintGradient);
		if (bClientMapping)
			pWnd->ScreenToClient(&rcPaintGradient);
		else
			rcPaintGradient.OffsetRect(-rcPaintGradient.left, -rcPaintGradient.top);
		if (::IsRectEmpty(&rcPaintGradient)
			|| (!dc.RectVisible(&rcPaintGradient))
			)
			return true;
		COLORREF clrT = stat_HLS_Adjust(m_clrOuterDockBarBkgnd, 0.0, +0.30, 0.0);
		COLORREF clrB = stat_HLS_Adjust(m_clrOuterDockBarBkgnd, 0.0, +0.10, 0.0);
		stat_PaintGradientRect(dc, rcPaintGradient, clrB, clrT, true);
		return true;
	}

	CExtToolControlBar::FtLayoutQuery _FtLayoutQuery;
	if (_FtLayoutQuery.Query(pWnd->m_hWnd))
	{
		if (bClientMapping)
			_FtLayoutQuery.ReMapRectsToTargetClient();
		else
			_FtLayoutQuery.ReMapRectsToTargetWindow();
		_FtLayoutQuery.DrawRects(dc);
		return true;
	} // if( _FtLayoutQuery.Query( pWnd->m_hWnd ) )

	CRect rcPaintGradient;
	pFrame->GetWindowRect(&rcPaintGradient);
	if (bClientMapping)
		pWnd->ScreenToClient(&rcPaintGradient);
	else
		rcPaintGradient.OffsetRect(-rcPaintGradient.left, -rcPaintGradient.top);
	if (::IsRectEmpty(&rcPaintGradient)
		|| (!dc.RectVisible(&rcPaintGradient))
		)
		return true;

	if (bMenuBarShade)
	{
		dc.FillSolidRect(rcPaintGradient, m_clrOuterDockBarBkgnd);
		if (bToolbarHorz)
			rcPaintGradient.bottom--;
		else
			rcPaintGradient.left++;
		COLORREF clr1 = bToolbarHorz ? m_clrMenuBarGriadient2 : m_clrMenuBarGriadient1;
		COLORREF clr2 = bToolbarHorz ? m_clrMenuBarGriadient1 : m_clrMenuBarGriadient2;
		stat_PaintGradientRect(dc, rcPaintGradient, clr1, clr2, bToolbarHorz);
		return true;
	}
	if (bOuterDockBarShade)
	{
		dc.FillSolidRect(rcPaintGradient, m_clrOuterDockBarBkgnd);
		return true;
	}
	if (bToolbarShade)
	{
		dc.FillSolidRect(rcPaintGradient, m_clrOuterDockBarBkgnd);
		if (bToolbarHorz)
			rcPaintGradient.top--;
		else
			rcPaintGradient.right++;
		VERIFY(m_bmpToolBarBkgnd.AlphaBlendSkinParts(dc.m_hDC, rcPaintGradient, CRect(3, 3, 3, 3), CExtBitmap::__EDM_STRETCH, true, true));
		return true;
	}

	CRect rcTop = rcPaintGradient, rcBottom = rcPaintGradient;
	rcTop.bottom = rcBottom.top = rcTop.top + rcPaintGradient.Height() / 2;
	stat_PaintGradientRect(dc, rcTop, m_clrDockerBkgndMiddle, m_clrDockerBkgndTop, true);
	stat_PaintGradientRect(dc, rcBottom, m_clrDockerBkgndBottom, m_clrDockerBkgndMiddle, true);
	static const RECT g_rcPaddingZero = { 0, 0, 0, 0 };
	VERIFY(m_bmpDockerBkgndDots.AlphaBlendSkinParts(dc.m_hDC, rcPaintGradient, g_rcPaddingZero, CExtBitmap::__EDM_TILE, true, true));
	return true;
}

bool CExtPaintManagerStudio2010::PaintDocumentClientAreaBkgnd(
	CDC & dc,
	CWnd * pWnd,
	LPARAM lParam // = NULL
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	if (IsHighContrast())
		return CExtPaintManagerStudio2005::PaintDocumentClientAreaBkgnd(dc, pWnd, lParam);
	CRect rcPaintGradient;
	pWnd->GetClientRect(&rcPaintGradient);
	if (!dc.RectVisible(&rcPaintGradient))
		return true;
	CRect rcTop = rcPaintGradient, rcBottom = rcPaintGradient;
	rcTop.bottom = rcBottom.top = rcTop.top + rcPaintGradient.Height() / 2;
	stat_PaintGradientRect(dc, rcTop, m_clrDockerBkgndMiddle, m_clrDockerBkgndTop, true);
	stat_PaintGradientRect(dc, rcBottom, m_clrDockerBkgndBottom, m_clrDockerBkgndMiddle, true);
	static const RECT g_rcPaddingZero = { 0, 0, 0, 0 };
	VERIFY(m_bmpDockerBkgndDots.AlphaBlendSkinParts(dc.m_hDC, rcPaintGradient, g_rcPaddingZero, CExtBitmap::__EDM_TILE, true, true));
	return true;
}

void CExtPaintManagerStudio2010::PaintControlBarClientArea(
	CDC & dc,
	const RECT & rcClient,
	CObject * pHelperSrc,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	if (IsHighContrast()
		|| (!pHelperSrc->IsKindOf(RUNTIME_CLASS(CWnd)))
		)
	{
		CExtPaintManagerStudio2005::PaintControlBarClientArea(dc, rcClient, pHelperSrc, lParam);
		return;
	}
	CWnd * pWnd = STATIC_DOWNCAST(CWnd, pHelperSrc);
	if (!PaintDockerBkgnd(true, dc, pWnd, lParam))
		CExtPaintManagerStudio2005::PaintControlBarClientArea(dc, rcClient, pHelperSrc, lParam);
}

void CExtPaintManagerStudio2010::PaintDockBarClientArea(
	CDC & dc,
	const RECT & rcClient,
	CObject * pHelperSrc,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	if (IsHighContrast()
		|| pHelperSrc == NULL
		)
	{
		CExtPaintManagerStudio2005::PaintDockBarClientArea(dc, rcClient, pHelperSrc, lParam);
		return;
	}
	CWnd * pWnd = DYNAMIC_DOWNCAST(CWnd, pHelperSrc);
	if (pWnd->GetSafeHwnd() == NULL)
	{
		CExtPaintManagerStudio2005::PaintDockBarClientArea(dc, rcClient, pHelperSrc, lParam);
		return;
	}
	CExtDockOuterBar * pDockOuterBar = DYNAMIC_DOWNCAST(CExtDockOuterBar, pWnd);
	if (pDockOuterBar != NULL)
	{
		dc.FillSolidRect(&rcClient, m_clrOuterDockBarBkgnd);
		return;
	}
	if (!PaintDockerBkgnd(true, dc, pDockOuterBar, lParam))
		CExtPaintManagerStudio2005::PaintDockBarClientArea(dc, rcClient, pHelperSrc, lParam);
}

void CExtPaintManagerStudio2010::PaintFloatToolbarRowBk(
	CDC & dc,
	const CExtToolControlBar * pBar,
	int nLastReviewBtnIdx,
	CRect & rcRowBk
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	ASSERT_VALID(pBar);
	ASSERT_KINDOF(CExtToolControlBar, pBar);
	ASSERT(nLastReviewBtnIdx >= 0);
	if (IsHighContrast())
	{
		CExtPaintManagerStudio2005::PaintFloatToolbarRowBk(dc, pBar, nLastReviewBtnIdx, rcRowBk);
		return;
	}
	//OnPaintToolBarGradient( dc, rcRowBk, true, (CObject*)pBar );
	stat_PaintGradientRect(dc, rcRowBk, m_clrMenuBarGriadient2, m_clrMenuBarGriadient1, true);
}

void CExtPaintManagerStudio2010::OnPaintToolBarGradient(
	CDC & dc,
	CRect rcPaintGradient,
	bool bHorz,
	CObject * pHelperSrc,
	LPARAM lParam, // = 0L
	COLORREF clrLeft, // = COLORREF(-1L)
	COLORREF clrRight // = COLORREF(-1L)
	) const
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	dc; rcPaintGradient; bHorz; pHelperSrc; lParam; clrLeft; clrRight;
	//	if( IsHighContrast() )
	//	{
	//		CExtPaintManagerStudio2005::OnPaintToolBarGradient( dc, rcPaintGradient, bHorz, pHelperSrc, lParam, clrLeft, clrRight );
	//		return;
	//	}
}

void CExtPaintManagerStudio2010::PaintGripper(
	CDC & dc,
	CExtPaintManager::PAINTGRIPPERDATA & _pgd
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	if (IsHighContrast())
	{
		CExtPaintManagerStudio2005::PaintGripper(dc, _pgd);
		return;
	}
	// 	if(		_pgd.m_pHelperSrc != NULL
	// 		&&	_pgd.m_pHelperSrc->IsKindOf( RUNTIME_CLASS(CExtControlBar) )
	// 		)
	// 		PaintDockerBkgnd( false, dc, ((CWnd*)_pgd.m_pHelperSrc) );
	CWnd * pBar = NULL;
	bool /*bInactiveResizableBarCapt = false,*/ bForceNoBaloon = false;
#if (!defined __EXT_MFC_NO_TAB_CONTROLBARS)
	if (_pgd.m_pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtDynAutoHideSlider)))
	{ // auto-hide slider
		pBar = STATIC_DOWNCAST(CWnd, _pgd.m_pHelperSrc);
		/*
		bInactiveResizableBarCapt = true;
		*/
	} // auto-hide slider
	else
#endif
		pBar = STATIC_DOWNCAST(CWnd, _pgd.m_pHelperSrc);
	ASSERT_VALID(pBar);
	if (_pgd.m_pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtToolControlBar))
		&& ((CControlBar*) _pgd.m_pHelperSrc)->m_pDockSite != NULL
		&& ((CExtToolControlBar*) _pgd.m_pHelperSrc)->m_bForceNoBalloonWhenRedockable
		)
		bForceNoBaloon = true;
	if (_pgd.m_pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtControlBar)))
	{ // if control bar
		if (((CExtControlBar*) pBar)->m_pDockSite == NULL)
		{ // if simple
			CExtToolControlBar *pToolBar = DYNAMIC_DOWNCAST(CExtToolControlBar, _pgd.m_pHelperSrc);
			if (pToolBar == NULL || (!pToolBar->m_bForceBalloonGradientInDialogs))
				bForceNoBaloon = true;
		} // if simple
	} // if control bar
	else
	{ // if NOT control bar
		if (_pgd.m_pHelperSrc != NULL && _pgd.m_pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtMiniDockFrameWnd)))
			return;
		CExtPaintManagerStudio2005::PaintGripper(dc, _pgd);
		return;
	} // if NOT control bar
	ASSERT_VALID(pBar);
	CRect rcBarWnd, rcBarClient;
	pBar->GetWindowRect(&rcBarWnd);
	pBar->GetClientRect(&rcBarClient);
	pBar->ClientToScreen(&rcBarClient);
	if (_pgd.m_bSideBar /*|| bInactiveResizableBarCapt*/ || _pgd.m_bFloating)
	{ // if resizable bar caption
		if (_pgd.m_bFlashCaptionHighlightedState)
		{
			ASSERT(_pgd.m_clrFlashCaptionBackground != COLORREF(-1L));
			ASSERT(_pgd.m_clrFlashCaptionText != COLORREF(-1L));
			dc.FillSolidRect(_pgd.m_rcGripper, _pgd.m_clrFlashCaptionBackground);
		} // if( _pgd.m_bFlashCaptionHighlightedState )
		else
		{
			if (!_pgd.m_bHorz)
			{
				CExtBitmap & bmpBarCaptBkgnd = (_pgd.m_bActive && _pgd.m_bSideBar) ? m_bmpBarCaptionActive : m_bmpBarCaptionInactive;
				bmpBarCaptBkgnd.AlphaBlendSkinParts(
					dc,
					_pgd.m_rcGripper,
					CRect(2, bmpBarCaptBkgnd.GetSize().cy / 2 + 1, 2, 0),
					CExtBitmap::__EDM_STRETCH,
					true,
					true
					);
			}
			else
				dc.FillSolidRect(&_pgd.m_rcGripper, (_pgd.m_bActive && _pgd.m_bSideBar) ? m_arrClrBarCaptionActive[2] : m_arrClrBarCaptionInactive[2]);
		} // else from  if( _pgd.m_bFlashCaptionHighlightedState )
		int nTextLen = 0;
		if (_pgd.m_sCaption != NULL
			&& (nTextLen = int(_tcslen(_pgd.m_sCaption))) > 0
			&& (!_pgd.m_rcText.IsRectEmpty())
			&& _pgd.m_rcText.right > _pgd.m_rcText.left
			&&	_pgd.m_rcText.bottom > _pgd.m_rcText.top
			)
		{
			COLORREF clrText =
				_pgd.m_bFlashCaptionHighlightedState
				? _pgd.m_clrFlashCaptionText
				: ((_pgd.m_bActive && _pgd.m_bSideBar) ? m_clrCaptBtnTextActive : m_clrCaptBtnTextNormal)
				;
			COLORREF clrOldText = dc.SetTextColor(clrText);
			int nOldBkMode = dc.SetBkMode(TRANSPARENT);
			CFont * pCurrFont = (_pgd.m_bSideBar ? (&m_FontNormalBC) : (&m_FontBoldBC));
			CFont * pOldFont = dc.SelectObject(pCurrFont);
			CRect rcDrawText(_pgd.m_rcText);
			CExtRichContentLayout::e_layout_orientation_t eLO = CExtRichContentLayout::__ELOT_NORMAL;
			if (_pgd.m_bHorz)
				eLO = CExtRichContentLayout::__ELOT_270_CW;
			UINT nDtAlign = _pgd.m_bForceRTL ? DT_RIGHT : DT_LEFT;
			CExtRichContentLayout::stat_DrawText(
				CExtRichContentLayout::__ELFMT_AUTO_DETECT, eLO,
				dc.m_hDC, _pgd.m_sCaption, nTextLen, rcDrawText, nDtAlign | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, 0
				);
			dc.SelectObject(pOldFont);
			dc.SetBkMode(nOldBkMode);
			dc.SetTextColor(clrOldText);
		}
		return;
	} // if resizable bar caption

	/*
	if(		(! pBar->IsKindOf(RUNTIME_CLASS(CExtMenuControlBar)) )
	&&	(! bForceNoBaloon )
	)
	{ // if toolbar balloon style
	CRect rcPaintGradient( rcBarWnd );
	if( _pgd.m_bHorz )
	{
	rcPaintGradient.top = rcBarClient.top;
	rcPaintGradient.bottom = rcBarClient.bottom; // + 1;
	rcPaintGradient.right = rcBarClient.right;
	rcPaintGradient.OffsetRect( -rcPaintGradient.left, -rcPaintGradient.top + rcBarClient.top - rcBarWnd.top );
	} // if( _pgd.m_bHorz )
	else
	{
	rcPaintGradient.left = rcBarClient.left;
	rcPaintGradient.right = rcBarClient.right; // + 1;
	rcPaintGradient.bottom = rcBarClient.bottom;
	rcPaintGradient.OffsetRect( -rcPaintGradient.left + rcBarClient.left - rcBarWnd.left, -rcPaintGradient.top );
	} // else from if( _pgd.m_bHorz )
	int nIdxClrTbFillMargin = -1;
	CRgn _rgnBaloonSet;
	const CSize _sizeRoundedAreaMerics = FixedBar_GetRoundedAreaMerics();
	if(		_sizeRoundedAreaMerics.cx > 0
	&&	_sizeRoundedAreaMerics.cy > 0
	)
	{ // if balloon style
	nIdxClrTbFillMargin = m_nIdxClrTbFillMargin;
	if(	_rgnBaloonSet.CreateRoundRectRgn(
	rcPaintGradient.left,
	rcPaintGradient.top,
	rcPaintGradient.Width(),
	rcPaintGradient.Height(),
	_sizeRoundedAreaMerics.cx,
	_sizeRoundedAreaMerics.cy
	)
	)
	dc.SelectClipRgn( &_rgnBaloonSet );
	}  // if balloon style
	OnPaintToolBarGradient( dc, rcPaintGradient, _pgd.m_bHorz, _pgd.m_pHelperSrc, _pgd.m_lParam );
	if( nIdxClrTbFillMargin >= 0 )
	{
	CPen _pen( PS_SOLID, 1, GetColor( m_nIdxClrTbFillMargin, _pgd.m_pHelperSrc, _pgd.m_lParam ) );
	CPen  * pOldPen = dc.SelectObject( &_pen );
	if( _pgd.m_bHorz )
	{
	dc.MoveTo( rcPaintGradient.left, rcPaintGradient.bottom-2 );
	dc.LineTo( rcPaintGradient.right, rcPaintGradient.bottom-2 );
	} // if( _pgd.m_bHorz )
	else
	{
	dc.MoveTo( rcPaintGradient.right-2, rcPaintGradient.top );
	dc.LineTo( rcPaintGradient.right-2, rcPaintGradient.bottom );
	} // else from if( _pgd.m_bHorz )
	dc.SelectObject( pOldPen );
	} // if( nIdxClrTbFillMargin >= 0 )

	if( _rgnBaloonSet.GetSafeHandle() != NULL )
	dc.SelectClipRgn( NULL );
	} // if toolbar balloon style
	*/
	COLORREF clrDotShadow = GetColor(_2003CLR_GRIPPER_DOT_LIGHT, _pgd.m_pHelperSrc, _pgd.m_lParam);
	COLORREF clrDotFace = GetColor(_2003CLR_GRIPPER_DOT_DARK, _pgd.m_pHelperSrc, _pgd.m_lParam);
	static const CSize g_sizeGripDot(2, 2);
	static const CSize g_sizeGripDist(1, 1);
	static const CSize g_sizeGripShadowOffset(1, 1);
	CRect rcGripHelper(_pgd.m_rcGripper);
	CRect rcBarClientW(rcBarClient);
	rcBarClientW.OffsetRect(-rcBarWnd.TopLeft());
	if (_pgd.m_bHorz)
	{
		rcGripHelper.top = rcBarClientW.top;
		rcGripHelper.bottom = rcBarClientW.bottom;
		rcGripHelper.OffsetRect(1, 0);
		rcGripHelper.left += (rcGripHelper.Width() - g_sizeGripDot.cx) / 2;
		rcGripHelper.right = rcGripHelper.left + g_sizeGripDot.cx;
		rcGripHelper.DeflateRect(0, g_sizeGripDot.cy + g_sizeGripDist.cy + g_sizeGripShadowOffset.cy);
		rcGripHelper.DeflateRect(0, 2, 0, 0);
		rcGripHelper.OffsetRect(0, -1);
		int nDotCount = rcGripHelper.Height() / (g_sizeGripDot.cy + g_sizeGripDist.cy + g_sizeGripShadowOffset.cy);
		rcGripHelper.top += rcGripHelper.Height() - (g_sizeGripDot.cy + g_sizeGripDist.cy + g_sizeGripShadowOffset.cy) * nDotCount;
		CRect rcDotFace(rcGripHelper);
		rcDotFace.bottom = rcDotFace.top + g_sizeGripDot.cy;
		CRect rcDotShadow(rcDotFace);
		rcDotShadow.OffsetRect(g_sizeGripShadowOffset);
		for (int nDot = 0; nDot < nDotCount; nDot++)
		{
			dc.FillSolidRect(&rcDotShadow, clrDotShadow);
			dc.FillSolidRect(&rcDotFace, clrDotFace);
			rcDotFace.OffsetRect(0, g_sizeGripDot.cy + g_sizeGripDist.cy + g_sizeGripShadowOffset.cy);
			rcDotShadow.OffsetRect(0, g_sizeGripDot.cy + g_sizeGripDist.cy + g_sizeGripShadowOffset.cy);
		} // for( int nDot = 0; nDot < nDotCount; nDot++ )
	} // if( _pgd.m_bHorz )
	else
	{
		rcGripHelper.left = rcBarClientW.left;
		rcGripHelper.right = rcBarClientW.right;
		rcGripHelper.OffsetRect(0, 1);
		rcGripHelper.top += (rcGripHelper.Height() - g_sizeGripDot.cy) / 2;
		rcGripHelper.bottom = rcGripHelper.top + g_sizeGripDot.cy;
		rcGripHelper.DeflateRect(g_sizeGripDot.cx + g_sizeGripDist.cx + g_sizeGripShadowOffset.cx, 0);
		rcGripHelper.OffsetRect(-1, 0);
		rcGripHelper.DeflateRect(3, 0, 0, 0);
		int nDotCount = rcGripHelper.Width() / (g_sizeGripDot.cx + g_sizeGripDist.cx + g_sizeGripShadowOffset.cx);
		rcGripHelper.left += rcGripHelper.Width() - (g_sizeGripDot.cx + g_sizeGripDist.cx + g_sizeGripShadowOffset.cx) * nDotCount;
		CRect rcDotFace(rcGripHelper);
		rcDotFace.right = rcDotFace.left + g_sizeGripDot.cx;
		CRect rcDotShadow(rcDotFace);
		rcDotShadow.OffsetRect(g_sizeGripShadowOffset);
		for (int nDot = 0; nDot < nDotCount; nDot++)
		{
			dc.FillSolidRect(&rcDotShadow, clrDotShadow);
			dc.FillSolidRect(&rcDotFace, clrDotFace);
			rcDotFace.OffsetRect(g_sizeGripDot.cx + g_sizeGripDist.cx + g_sizeGripShadowOffset.cx, 0);
			rcDotShadow.OffsetRect(g_sizeGripDot.cx + g_sizeGripDist.cx + g_sizeGripShadowOffset.cx, 0);
		} // for( int nDot = 0; nDot < nDotCount; nDot++ )
	} // else from if( _pgd.m_bHorz )
}

void CExtPaintManagerStudio2010::PaintDockingFrame(
	CDC & dc,
	CExtPaintManager::PAINTDOCKINGFRAMEDATA & _pdfd
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	if (IsHighContrast())
	{
		CExtPaintManagerStudio2005::PaintDockingFrame(dc, _pdfd);
		return;
	}
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	if (_pdfd.m_rcWindow.IsRectEmpty())
		return;
	if (_pdfd.m_pHelperSrc != NULL
		&&	_pdfd.m_pHelperSrc->IsKindOf(RUNTIME_CLASS(CExtToolControlBar))
		)
	{
		if ((((CExtToolControlBar*) _pdfd.m_pHelperSrc)->m_pDockSite != NULL))
		{
			if (((CExtToolControlBar*) _pdfd.m_pHelperSrc)->m_bForceNoBalloonWhenRedockable
				|| (!((CExtToolControlBar*) _pdfd.m_pHelperSrc)->IsFloating())
				)
				return;
		}
		else
			return;
	}
	CRect rcWnd(_pdfd.m_rcWindow);
	CExtMemoryDC dc2(&dc, &rcWnd);
	dc2.ExcludeClipRect(_pdfd.m_rcClient);
	dc2.FillSolidRect(&rcWnd, m_clrDockingFrameBorder);
}

void CExtPaintManagerStudio2010::PaintDockingCaptionButton(
	CDC & dc,
	CExtPaintManager::PAINTDOCKINGCAPTIONBUTTONDATA & _pdcbd
	)
{
	ASSERT_VALID(this);
	ASSERT(dc.GetSafeHdc() != NULL);
	ASSERT(__DCBT_VALUE_MIN <= _pdcbd.m_eType && _pdcbd.m_eType <= __DCBT_VALUE_MAX);
	if (_pdcbd.m_rcClient.IsRectEmpty())
		return;
	if (IsHighContrast())
	{
		CExtPaintManagerStudio2005::PaintDockingCaptionButton(dc, _pdcbd);
		return;
	}
	bool bActive = _pdcbd.m_bBarWndActive;
	//	if( bActive && _pdcbd.m_pHelperSrc != NULL && _pdcbd.m_pHelperSrc->IsKindOf( RUNTIME_CLASS(CExtBarNcAreaButton) ) )
	//	{
	//		CExtControlBar * pBar = ((CExtBarNcAreaButton*)_pdcbd.m_pHelperSrc)->GetBar();
	//		if( pBar->GetSafeHwnd() != NULL && pBar->m_pDockSite != NULL && (! pBar->IsFixedMode() ) && (! pBar->_IsSingleVisibleInFloatingPalette() ) )
	//			bActive = false; // caption of compound floating palette window with more than one resizable bar visible in it
	//	}
	bool bHover = _pdcbd.m_bHover;
	if (!_pdcbd.m_bEnabled)
		bHover = false;
	COLORREF ColorValues[2] = { RGB(0, 0, 0), RGB(0, 0, 0), };
	ColorValues[1] =
		_pdcbd.m_bEnabled
		?
		(_pdcbd.m_bHover
		? (_pdcbd.m_bPushed ? m_clrCaptBtnGlyphPressed : m_clrCaptBtnGlyphHover)
		: (bActive ? m_clrCaptBtnGlyphActiveNormal : m_clrCaptBtnGlyphInactiveNormal)
		)
		: m_clrCaptBtnGlyphDisabled
		;
	CRect rcGlyph(_pdcbd.m_rcClient);
	rcGlyph.NormalizeRect();
	if (rcGlyph.Width() & 1)
		rcGlyph.left++;
	if (rcGlyph.Height() & 1)
		rcGlyph.top++;
	if (_pdcbd.m_eType == __DCBT_CLOSE)
		_pdcbd.m_eType = __DCBT_CLOSE_DC2K;
	// W4 Changed value was checked earlier
#pragma warning(suppress: 6385)
	glyph_t * pGlyph = &g_DockingCaptionGlyphs[_pdcbd.m_eType];
	ASSERT(pGlyph != NULL);
	if (!_pdcbd.m_bFloating)
		rcGlyph.InflateRect(1, 1);
	rcGlyph.InflateRect(1, 1, 0, 0);
	if (_pdcbd.m_bEnabled && (_pdcbd.m_bHover || _pdcbd.m_bPushed))
	{
		dc.FillSolidRect(&rcGlyph, _pdcbd.m_bPushed ? m_clrCaptBtnGlyphBkgndPressed : m_clrCaptBtnGlyphBkgndHover);
		dc.Draw3dRect(&rcGlyph, m_clrCaptBtnBorder, m_clrCaptBtnBorder);
	}
	CRect rcGlyphShape(rcGlyph);
	PaintGlyphCentered(dc, rcGlyphShape, *pGlyph, ColorValues);
}


