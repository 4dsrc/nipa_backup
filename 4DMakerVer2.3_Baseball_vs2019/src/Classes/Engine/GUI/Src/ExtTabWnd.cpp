// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_TAB_CTRL)

	#if (!defined __AFXPRIV_H__)
		#include <AfxPriv.h>
	#endif 

	#if _MFC_VER < 0x700
		#include <../src/AfxImpl.h>
	#else
		#include <../src/mfc/AfxImpl.h>
	#endif

	#if (!defined __EXT_TABWND_H)
		#include <ExtTabWnd.h>
	#endif

	#if (!defined __EXT_RICH_CONTENT_H)
		#include <ExtRichContent.h>
	#endif // (!defined __EXT_RICH_CONTENT_H)

	#if (!defined __EXT_PAINT_MANAGER_H)
		#include <ExtPaintManager.h>
	#endif

	#if (!defined __EXT_MEMORY_DC_H)
		#include <../Src/ExtMemoryDC.h>
	#endif

	#if (!defined __EXT_CONTROLBAR_H)
		#include <ExtControlBar.h>
	#endif

	#if (!defined __EXT_POPUP_MENU_WND_H)
		#include <ExtPopupMenuWnd.h>
	#endif

	#if (!defined __EXT_LOCALIZATION_H)
		#include <../Src/ExtLocalization.h>
	#endif

	#include <Resources/Resource.h>

	#if (!defined __EXT_CONTROLBAR_H)
		#include <ExtControlBar.h>
	#endif

#endif // (!defined __EXT_MFC_NO_TAB_CTRL )

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
	#if (!defined __EXTCUSTOMIZE_H)
		#include <ExtCustomize.h>
	#endif
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#if (!defined __EXT_MFC_NO_TAB_CTRL)

/////////////////////////////////////////////////////////////////////////////
// CExtTabWnd

CExtTabWnd::TAB_ITEM_INFO::TAB_ITEM_INFO(
	CExtTabWnd * pWndTab,  // = NULL
	TAB_ITEM_INFO * pPrev, // = NULL
	TAB_ITEM_INFO * pNext, // = NULL
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL
	const CExtCmdIcon * pIcon, // = NULL // CHANGED 2.53
	DWORD dwStyle, // = 0 // visible & non-group
	LPARAM lParam, // = 0
	__EXT_MFC_SAFE_LPCTSTR sTooltipText // = NULL
	)
	: m_pWndTab( pWndTab )
	, m_pPrev( pPrev )
	, m_pNext( pNext )
	, m_sText( ( sText == NULL ) ? _T("") : sText )
	, m_dwItemStyle( dwStyle )
	, m_dwItemStyleEx( 0 )
	, m_sizeLastMeasuredItem( __EXTTAB_MIN_HORZ_WIDTH, __EXTTAB_MIN_HORZ_HEIGHT )
	, m_sizeLastMeasuredText( 0, 0 )
	, m_sizeLastMeasuredIcon( 0, 0 )
	, m_rcItem( 0, 0, __EXTTAB_MIN_HORZ_WIDTH, __EXTTAB_MIN_HORZ_HEIGHT )
	, m_rcCloseButton( 0, 0, 0, 0 )
	, m_lParam( lParam )
	, m_pEventProvider( NULL )
	, m_sTooltipText( ( sTooltipText == NULL ) ? _T("") : sTooltipText )
{
	if ( pIcon != NULL && ( !pIcon->IsEmpty() ) )
	{
		m_icon = ( *pIcon );
		if ( g_PaintManager->g_bAutoScaleIcons )
			m_icon.Scale(CSize(g_PaintManager->UiScalingDo(m_icon.GetSize().cx, CExtPaintManager::__EUIST_X), g_PaintManager->UiScalingDo(m_icon.GetSize().cy, CExtPaintManager::__EUIST_Y)));
	}
	if( m_pPrev != NULL )
		m_pPrev->m_pNext = this;
	if( m_pNext != NULL )
		m_pNext->m_pPrev = this;
}

CExtTabWnd::TAB_ITEM_INFO::TAB_ITEM_INFO(
	const TAB_ITEM_INFO & other
	)
	: m_pWndTab( NULL )
	, m_pPrev( NULL )
	, m_pNext( NULL )
	, m_sText( _T("") )
	, m_dwItemStyle( 0 )
	, m_dwItemStyleEx( 0 )
	, m_sizeLastMeasuredItem( __EXTTAB_MIN_HORZ_WIDTH, __EXTTAB_MIN_HORZ_HEIGHT )
	, m_sizeLastMeasuredText( 0, 0 )
	, m_sizeLastMeasuredIcon( 0, 0 )
	, m_rcItem( 0, 0, __EXTTAB_MIN_HORZ_WIDTH, __EXTTAB_MIN_HORZ_HEIGHT )
	, m_rcCloseButton( 0, 0, 0, 0 )
	, m_lParam( 0L )
	, m_pEventProvider( NULL )
	, m_bHelperToolTipAvail( false )
	, m_bMultiRowWrap( false )
	, m_sTooltipText( _T("") )
{
	_AssignFromOther( other );
}

CExtTabWnd::TAB_ITEM_INFO::~TAB_ITEM_INFO()
{
}

void CExtTabWnd::TAB_ITEM_INFO::_AssignFromOther(
	const TAB_ITEM_INFO & other
	)
{
	ASSERT_VALID( (&other) );
	m_pWndTab = other.m_pWndTab;
	m_dwItemStyle = other.m_dwItemStyle;
	m_dwItemStyleEx = other.m_dwItemStyleEx;
	m_sText = other.m_sText;
	m_sTooltipText = other.m_sTooltipText;
	m_icon = other.m_icon;
	m_pPrev = other.m_pPrev;
	m_pNext = other.m_pNext;
	m_sizeLastMeasuredItem = other.m_sizeLastMeasuredItem;
	m_sizeLastMeasuredText = other.m_sizeLastMeasuredText;
	m_sizeLastMeasuredIcon = other.m_sizeLastMeasuredIcon;
	m_rcItem = other.m_rcItem;
	m_rcCloseButton = other.m_rcCloseButton;
	m_lParam = other.m_lParam;
	m_pEventProvider = other.m_pEventProvider;
	m_bHelperToolTipAvail = other.m_bHelperToolTipAvail;
	m_bMultiRowWrap = other.m_bMultiRowWrap;
	ASSERT_VALID( this );
}

__EXT_MFC_SAFE_LPCTSTR CExtTabWnd::TAB_ITEM_INFO::TextGet() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );

__EXT_MFC_SAFE_LPCTSTR sExternItemText =
		m_pWndTab->OnTabWndQueryItemText( this );
	if( sExternItemText != NULL )
		return sExternItemText;

	return m_sText;
}

__EXT_MFC_SAFE_LPCTSTR CExtTabWnd::TAB_ITEM_INFO::TooltipTextGet() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
__EXT_MFC_SAFE_LPCTSTR sTooltipItemText =
		m_pWndTab->OnTabWndQueryItemTooltipText( this );
	if(		sTooltipItemText != NULL
		&&	_tcslen(sTooltipItemText) > 0
		)
		return sTooltipItemText;
	return m_sTooltipText;
}

CSize CExtTabWnd::TAB_ITEM_INFO::IconGetSize() const
{
	ASSERT_VALID( this );

CExtCmdIcon * pExternIcon = m_pWndTab->OnTabWndQueryItemIcon( this );
	if( pExternIcon != NULL && (! pExternIcon->IsEmpty()) )
	{
		CSize _sizeIcon = pExternIcon->GetSize();
		return _sizeIcon;
	}

	return m_icon.GetSize();
}

CExtCmdIcon * CExtTabWnd::TAB_ITEM_INFO::IconGetPtr() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );

CExtCmdIcon * pExternIcon = m_pWndTab->OnTabWndQueryItemIcon( this );
	if( pExternIcon != NULL )
		return pExternIcon;

	return (CExtCmdIcon *)&m_icon;
}

CSize CExtTabWnd::TAB_ITEM_INFO::Measure(
	CDC * pDcMeasure // = NULL
	)
{
	ASSERT_VALID( this );
CDC * pDC = pDcMeasure;
	if( pDcMeasure == NULL )
	{
		pDC = new CWindowDC( NULL );
		ASSERT( pDC != NULL );
	}
	ASSERT( pDC->GetSafeHdc() != NULL );
CExtTabWnd * pWndTab = GetTabWnd();
	ASSERT_VALID( pWndTab );
bool bSelected = SelectedGet();
//bool bSelected = pWndTab->SelectionBoldGet();
CFont font;
	pWndTab->_GetTabWndFont( &font, bSelected, __ETWS_ORIENT_TOP );
	ASSERT( font.GetSafeHandle() != NULL );
DWORD dwTabWndStyle = pWndTab->GetTabWndStyle();
bool bGrouped = (dwTabWndStyle&__ETWS_GROUPED) ? true : false;
bool bGroupedExpandItems = (dwTabWndStyle&__ETWS_GROUPED_EXPAND_ITEMS) ? true : false;
CExtSafeString sText = TextGet();
CExtRichContentLayout::e_layout_orientation_t eLO = CExtRichContentLayout::__ELOT_NORMAL;
	if( pWndTab->OrientationIsVertical() )
	{
		bool bInvertedVerticalMode = (dwTabWndStyle & __ETWS_INVERT_VERT_FONT) ? true : false;
		eLO = bInvertedVerticalMode ? CExtRichContentLayout::__ELOT_270_CW : CExtRichContentLayout::__ELOT_90_CW;
	}
//CRect rcMeasureText = CExtPaintManager::stat_CalcTextDimension( CExtRichContentLayout::__ELFMT_AUTO_DETECT, eLO, *pDC, font, sText );
	UINT uFlags = DT_LEFT | DT_CALCRECT | ( sText.Find(_T('\n')) != -1 ? 0 : DT_SINGLELINE );
	CRect rcMeasureText = CExtPaintManager::stat_CalcTextDimension(CExtRichContentLayout::__ELFMT_AUTO_DETECT, eLO, *pDC, font, sText, uFlags);
	m_sizeLastMeasuredText = rcMeasureText.Size();
bool bTextIsDisplayedOnTab = true;
	if( bGrouped && (! bGroupedExpandItems ) && (! InGroupActiveGet() ) )
		bTextIsDisplayedOnTab = false;
	m_sizeLastMeasuredIcon = IconGetSize();
CSize _sizeTab( 0, 0 );
bool bVertical = pWndTab->OrientationIsVertical();
	if( bVertical )
	{
		_sizeTab.cx = max(  m_sizeLastMeasuredIcon.cx, m_sizeLastMeasuredText.cx );
		_sizeTab.cy =
			m_sizeLastMeasuredIcon.cy
			+ ((m_sizeLastMeasuredIcon.cy > 0) ? __EXTTAB_MARGIN_ICON2TEXT_Y : 0)
			+ ( bTextIsDisplayedOnTab ? m_sizeLastMeasuredText.cy : 0 )
			+ __EXTTAB_SEPARATOR_AREA_Y
			;
		_sizeTab.cx += __EXTTAB_MARGIN_BORDER_VX * 2;
		_sizeTab.cy += __EXTTAB_MARGIN_BORDER_VY * 2;
		CSize _sizeCloseButton = pWndTab->OnTabWndQueryItemCloseButtonSize( this );
		if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )
		{
			_sizeTab.cx = max( _sizeTab.cx, _sizeCloseButton.cx );
			_sizeTab.cy += _sizeCloseButton.cy + __EXTTAB_MARGIN_ICON2TEXT_Y;
		}
		_sizeTab.cx = max( _sizeTab.cx, __EXTTAB_MIN_VERT_WIDTH );
		_sizeTab.cy = max( _sizeTab.cy, __EXTTAB_MIN_VERT_HEIGHT );
	} // if( bVertical )
	else
	{
		_sizeTab.cx =
			m_sizeLastMeasuredIcon.cx
			+ ((m_sizeLastMeasuredIcon.cx > 0) ? __EXTTAB_MARGIN_ICON2TEXT_X : 0)
			+ ( bTextIsDisplayedOnTab ? m_sizeLastMeasuredText.cx : 0 )
			+ __EXTTAB_SEPARATOR_AREA_X
			;
		_sizeTab.cy = max(  m_sizeLastMeasuredIcon.cy, m_sizeLastMeasuredText.cy );
		_sizeTab.cx += __EXTTAB_MARGIN_BORDER_HX * 2;
		_sizeTab.cy += __EXTTAB_MARGIN_BORDER_HY * 2;
		CSize _sizeCloseButton = pWndTab->OnTabWndQueryItemCloseButtonSize( this );
		if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )
		{
			_sizeTab.cx += _sizeCloseButton.cx + __EXTTAB_MARGIN_ICON2TEXT_X;
			_sizeTab.cy = max( _sizeTab.cy, _sizeCloseButton.cy );
		}
		_sizeTab.cx = max( _sizeTab.cx, __EXTTAB_MIN_HORZ_WIDTH );
		_sizeTab.cy = max( _sizeTab.cy, __EXTTAB_MIN_HORZ_HEIGHT );
	} // else from if( bVertical )
	pWndTab->OnTabWndUpdateItemMeasure( this, *pDC, _sizeTab );
	if( pDcMeasure == NULL )
	{
		ASSERT( pDC != NULL );
		delete pDC;
	}
	m_sizeLastMeasuredItem = _sizeTab;
	if( ! bTextIsDisplayedOnTab )
		m_sizeLastMeasuredText.cx = m_sizeLastMeasuredText.cy = 0;
	return _sizeTab;
}

bool CExtTabWnd::TAB_ITEM_INFO::HitTest(
	const POINT & ptClient
	) const
{
	if( !VisibleGet() )
		return false;
const CRect & rcItem = ItemRectGet();
	if( rcItem.IsRectEmpty() )
		return false;
	if( !rcItem.PtInRect(ptClient) )
		return false;
	return true;
}

CExtTabWnd * CExtTabWnd::TAB_ITEM_INFO::GetTabWnd()
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	return m_pWndTab;
}

const CExtTabWnd * CExtTabWnd::TAB_ITEM_INFO::GetTabWnd() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	return m_pWndTab;
}

void CExtTabWnd::TAB_ITEM_INFO::TextSet(
	__EXT_MFC_SAFE_LPCTSTR sText // = NULL
	)
{
	ASSERT_VALID( this );
	m_sText = ( sText == NULL ) ? _T("") : sText;
}

void CExtTabWnd::TAB_ITEM_INFO::TooltipTextSet(
	__EXT_MFC_SAFE_LPCTSTR sTooltipText // = NULL
	)
{
	ASSERT_VALID( this );
	m_sTooltipText = ( sTooltipText == NULL ) ? _T("") : sTooltipText;
}

void CExtTabWnd::TAB_ITEM_INFO::IconSet(
	const CExtCmdIcon & _icon
	)
{
	ASSERT_VALID( this );
	m_icon = _icon;
}

void CExtTabWnd::TAB_ITEM_INFO::IconSet(
	HICON hIcon, // = NULL
	bool bCopyIcon // = true
	)
{
	ASSERT_VALID( this );
	if( hIcon == NULL )
		m_icon.Empty();
	else
		m_icon.AssignFromHICON( hIcon, bCopyIcon );
}

DWORD CExtTabWnd::TAB_ITEM_INFO::GetItemStyle() const
{
	ASSERT_VALID( this );
DWORD dwItemStyle = m_dwItemStyle;
	if( m_pWndTab != NULL )
	{
		ASSERT_VALID( m_pWndTab );
		m_pWndTab->_OnTabItemHook_GetItemStyle( *this, dwItemStyle );
	}
	return dwItemStyle;
}

DWORD CExtTabWnd::TAB_ITEM_INFO::ModifyItemStyle(
	DWORD dwRemove,
	DWORD dwAdd // = 0
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );

	if( m_pWndTab != NULL )
	{
		ASSERT_VALID( m_pWndTab );
		m_pWndTab->_OnTabItemHook_ModifyItemStyle( *this, m_dwItemStyle, dwRemove, dwAdd );
	}

DWORD dwOldStyle = m_dwItemStyle;
	m_dwItemStyle &= ~dwRemove;
	m_dwItemStyle |= dwAdd;

bool bWasVisible =
		( dwOldStyle & __ETWI_INVISIBLE )
			? false
			: true;
bool bNowVisible =
		( m_dwItemStyle & __ETWI_INVISIBLE )
			? false
			: true;
	if( bWasVisible != bNowVisible )
	{
		ASSERT(
				m_pWndTab->m_nVisibleItemCount >= 0
			&&	m_pWndTab->m_nVisibleItemCount <= m_pWndTab->m_arrItems.GetSize()
			);

		if( bNowVisible )
		{
			if( m_pWndTab->m_nVisibleItemCount < m_pWndTab->m_arrItems.GetSize() )
				m_pWndTab->m_nVisibleItemCount ++;
		}
		else
		{
			if( m_pWndTab->m_nVisibleItemCount > 0 )
				m_pWndTab->m_nVisibleItemCount --;
		}
		
		ASSERT(
				m_pWndTab->m_nVisibleItemCount >= 0
			&&	m_pWndTab->m_nVisibleItemCount <= m_pWndTab->m_arrItems.GetSize()
			);
	} // if( bWasVisible != bNowVisible )

	bool bNowSelected =
		( m_dwItemStyle & __ETWI_SELECTED )
			? true
			: false;
	if( bNowSelected )
		m_dwItemStyle |= __ETWI_IN_GROUP_ACTIVE;

	bool bWasInGroupActive =
		( dwOldStyle & __ETWI_IN_GROUP_ACTIVE )
			? true
			: false;
	bool bNowInGroupActive =
		( m_dwItemStyle & __ETWI_IN_GROUP_ACTIVE )
			? true
			: false;

	if( bWasInGroupActive != bNowInGroupActive
		&& (m_pPrev != NULL || m_pNext != NULL)
		&& bNowInGroupActive
		)
	{
		bool bInGroupActiveWasFoundBeforeThis = false;
		if( (m_dwItemStyle & __ETWI_GROUP_START) == 0 )
		{
			for(	TAB_ITEM_INFO * pTii = m_pPrev;
					pTii != NULL;
					pTii = pTii->m_pPrev
					)
			{ // find active in group item before this
				if( pTii->m_dwItemStyle & __ETWI_IN_GROUP_ACTIVE )
				{
					pTii->m_dwItemStyle &= ~__ETWI_IN_GROUP_ACTIVE;
					bInGroupActiveWasFoundBeforeThis = true;
					break;
				}
				if( pTii->m_dwItemStyle & __ETWI_GROUP_START )
					break;
			} // find active in group item before this
		}
		if( !bInGroupActiveWasFoundBeforeThis )
		{
			for(	TAB_ITEM_INFO * pTii = m_pNext;
					pTii != NULL;
					pTii = pTii->m_pNext
					)
			{ // find active in group item after this
				if( pTii->m_dwItemStyle & __ETWI_GROUP_START )
					break;
				if( pTii->m_dwItemStyle & __ETWI_IN_GROUP_ACTIVE )
				{
					pTii->m_dwItemStyle &= ~__ETWI_IN_GROUP_ACTIVE;
					break;
				}
			} // find active in group item after this
		} // if( !bInGroupActiveWasFoundBeforeThis )
	} // if( bWasInGroupActive != bNowInGroupActive )

	return dwOldStyle;
}

DWORD CExtTabWnd::TAB_ITEM_INFO::GetItemStyleEx() const
{
	ASSERT_VALID( this );
DWORD dwItemStyleEx = m_dwItemStyleEx;
	if( m_pWndTab != NULL )
	{
		ASSERT_VALID( m_pWndTab );
		m_pWndTab->_OnTabItemHook_GetItemStyleEx( *this, dwItemStyleEx );
	}
	return dwItemStyleEx;
}

DWORD CExtTabWnd::TAB_ITEM_INFO::ModifyItemStyleEx(
	DWORD dwRemove,
	DWORD dwAdd // = 0
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	if( m_pWndTab != NULL )
	{
		ASSERT_VALID( m_pWndTab );
		m_pWndTab->_OnTabItemHook_ModifyItemStyleEx( *this, m_dwItemStyleEx, dwRemove, dwAdd );
	}
DWORD dwOldStyleEx = m_dwItemStyleEx;
	m_dwItemStyleEx &= ~dwRemove;
	m_dwItemStyleEx |= dwAdd;
	return dwOldStyleEx;
}

const CSize & CExtTabWnd::TAB_ITEM_INFO::GetLastMeasuredItemSize() const
{
	return m_sizeLastMeasuredItem;
}

const CSize & CExtTabWnd::TAB_ITEM_INFO::GetLastMeasuredTextSize() const
{
	return m_sizeLastMeasuredText;
}

const CSize & CExtTabWnd::TAB_ITEM_INFO::GetLastMeasuredIconSize() const
{
	return m_sizeLastMeasuredIcon;
}

const CRect & CExtTabWnd::TAB_ITEM_INFO::ItemRectGet() const
{
	return m_rcItem;
}

void CExtTabWnd::TAB_ITEM_INFO::ItemRectSet( const RECT & rcItem )
{
	m_rcItem = rcItem;
}

const CRect & CExtTabWnd::TAB_ITEM_INFO::CloseButtonRectGet() const
{
	return m_rcCloseButton;
}

void CExtTabWnd::TAB_ITEM_INFO::CloseButtonRectSet( const RECT & rcCloseButton )
{
	m_rcCloseButton = rcCloseButton;
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetPrev()
{
	ASSERT_VALID( this );
	return m_pPrev;
}

const CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetPrev() const
{
	ASSERT_VALID( this );
	return m_pPrev;
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetNext()
{
	ASSERT_VALID( this );
	return m_pNext;
}

const CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetNext() const
{
	ASSERT_VALID( this );
	return m_pNext;
}

bool CExtTabWnd::TAB_ITEM_INFO::IsFirst() const
{
	return (GetPrev() == NULL) ? true : false;
}

bool CExtTabWnd::TAB_ITEM_INFO::IsLast() const
{
	return (GetNext() == NULL) ? true : false;
}

bool CExtTabWnd::TAB_ITEM_INFO::VisibleGet() const
{
	bool bVisible =
		( (GetItemStyle() & __ETWI_INVISIBLE) != 0 )
			? false : true;
	return bVisible;
}

bool CExtTabWnd::TAB_ITEM_INFO::VisibleSet( 
	bool bVisible // = true 
	)
{
	bool bWasVisible = VisibleGet();
	ModifyItemStyle(
		bVisible ? __ETWI_INVISIBLE : 0,
		bVisible ? 0 : __ETWI_INVISIBLE
		);
	return bWasVisible;
}

bool CExtTabWnd::TAB_ITEM_INFO::EnabledGet() const
{
	bool bEnable =
		( (GetItemStyle() & __ETWI_DISABLED) != 0 )
			? false : true;
	return bEnable;
}

bool CExtTabWnd::TAB_ITEM_INFO::EnabledSet( 
	bool bEnable // = true 
	)
{
bool bEnablePrev = EnabledGet();
	ModifyItemStyle(
		bEnable ? __ETWI_DISABLED : 0,
		bEnable ? 0 : __ETWI_DISABLED
		);
	return bEnablePrev;
}

bool CExtTabWnd::TAB_ITEM_INFO::SelectedGet() const
{
	bool bSelected =
		( (GetItemStyle() & __ETWI_SELECTED) != 0 )
			? true : false;
	return bSelected;
}

bool CExtTabWnd::TAB_ITEM_INFO::SelectedSet( 
	bool bSelected // = true 
	)
{
	bool bWasSelected = SelectedGet();
	ModifyItemStyle(
		bSelected ? 0 : __ETWI_SELECTED,
		bSelected ? __ETWI_SELECTED : 0
		);
	return bWasSelected;
}

bool CExtTabWnd::TAB_ITEM_INFO::GroupStartGet() const
{
	bool bGroupStart =
		( (GetItemStyle() & __ETWI_GROUP_START) != 0 )
			? true : false;
	return bGroupStart;
}

bool CExtTabWnd::TAB_ITEM_INFO::GroupStartSet( 
	bool bGroupStart // = true 
	)
{
	bool bWasGroupStart = GroupStartGet();
	ModifyItemStyle(
		bGroupStart ? 0 : __ETWI_GROUP_START,
		bGroupStart ? __ETWI_GROUP_START : 0
		);
	return bWasGroupStart;
}

bool CExtTabWnd::TAB_ITEM_INFO::InGroupActiveGet() const
{
	bool bInGroupActive =
		( (GetItemStyle() & __ETWI_IN_GROUP_ACTIVE) != 0 )
			? true : false;
	return bInGroupActive;
}

bool CExtTabWnd::TAB_ITEM_INFO::InGroupActiveSet( 
	bool bInGroupActive // = true 
	)
{
	bool bWasInGroupActive = InGroupActiveGet();
	ModifyItemStyle(
		bInGroupActive ? 0 : __ETWI_IN_GROUP_ACTIVE,
		bInGroupActive ? __ETWI_IN_GROUP_ACTIVE : 0
		);
	return bWasInGroupActive;
}

const CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetInGroupFirst() const
{
	ASSERT_VALID( this );
	const TAB_ITEM_INFO * pTii = this;
	for( ; pTii != NULL; pTii = pTii->GetPrev() )
	{
		ASSERT_VALID( pTii );
		if(		pTii->GroupStartGet()
			||	pTii->GetPrev() == NULL
			)
			return pTii;
	}
	ASSERT( FALSE );
	return NULL;
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetInGroupFirst()
{
	const TAB_ITEM_INFO * pTii = this;
	pTii = pTii->GetInGroupFirst();
	return
		const_cast < TAB_ITEM_INFO * > ( pTii );
}

const CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetInGroupLast() const
{
	ASSERT_VALID( this );
	const TAB_ITEM_INFO * pTii = GetNext();
	const TAB_ITEM_INFO * pTii2 = this;
	for( ; pTii != NULL; )
	{
		ASSERT_VALID( pTii );
		if( pTii->GroupStartGet() )
			return pTii2;
		pTii2 = pTii;
		pTii = pTii->GetNext();
	}
	return pTii2;
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetInGroupLast()
{
	const TAB_ITEM_INFO * pTii = this;
	pTii = pTii->GetInGroupLast();
	return
		const_cast < TAB_ITEM_INFO * > ( pTii );
}

LONG CExtTabWnd::TAB_ITEM_INFO::GetIndexOf() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	return m_pWndTab->ItemGetIndexOf( this );
}

LPARAM CExtTabWnd::TAB_ITEM_INFO::LParamGet() const
{
	ASSERT_VALID( this );
	return m_lParam;
}

LPARAM CExtTabWnd::TAB_ITEM_INFO::LParamSet( 
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID( this );
LPARAM lParamOld = m_lParam;
	m_lParam = lParam;
	return lParamOld;
}

CObject * CExtTabWnd::TAB_ITEM_INFO::EventProviderGet() const
{
	ASSERT_VALID( this );
	return m_pEventProvider;
}

void CExtTabWnd::TAB_ITEM_INFO::EventProviderSet(
	CObject * pEventProvider // = NULL
	)
{
	ASSERT_VALID( this );
	m_pEventProvider = pEventProvider;
}

bool CExtTabWnd::TAB_ITEM_INFO::IsToolTipAvailByExtent() const
{
	return m_bHelperToolTipAvail;
}

bool CExtTabWnd::TAB_ITEM_INFO::MultiRowWrapGet() const
{
	return m_bMultiRowWrap;
}

void CExtTabWnd::TAB_ITEM_INFO::MultiRowWrapSet(
	bool bMultiRowWrap // = true
	)
{
	m_bMultiRowWrap = bMultiRowWrap;
}

#ifdef _DEBUG

void CExtTabWnd::TAB_ITEM_INFO::AssertValid() const
{
	CObject::AssertValid();

	ASSERT( m_pWndTab != NULL );

	if( m_pPrev != NULL )
	{
		ASSERT( m_pPrev->m_pNext == this );
		ASSERT( m_pPrev->m_pWndTab == m_pWndTab );
	}
	
	if( m_pNext != NULL )
	{
		ASSERT( m_pNext->m_pPrev == this );
		ASSERT( m_pNext->m_pWndTab == m_pWndTab );
	}

}

void CExtTabWnd::TAB_ITEM_INFO::Dump(CDumpContext& dc) const
{
	CObject::Dump( dc );
}

#endif

/////////////////////////////////////////////////////////////////////////////
// CExtTabWnd window

bool CExtTabWnd::g_bEnableOnIdleCalls = false;

IMPLEMENT_DYNCREATE( CExtTabWnd, CWnd );
IMPLEMENT_CExtPmBridge_MEMBERS_GENERIC( CExtTabWnd );

CExtTabWnd::CExtTabWnd()
	: m_bDirectCreateCall( false )
	, m_bDelayRecalcLayout( true )
	, m_bReflectQueryRepositionCalcEffect( false )
	, m_bReflectParentSizing( true )
	, m_bTrackingButtonPushed( false )
	, m_bTrackingButtonHover( false )
	, m_dwTabWndStyle( 0 )
	, m_dwTabWndStyleEx( 0 )
	, m_nSelIndex( -1 )
	, m_nDelayedSelIndex( -1 )
	, m_nIndexVisFirst( 0 )
	, m_nIndexVisLast( 0 )
	, m_nVisibleItemCount( 0 )
	, m_rcTabItemsArea( 0, 0, 0, 0 )
	, m_rcTabNearBorderArea( 0, 0, 0, 0 )
	, m_rcBtnUp( 0, 0, 0, 0 )
	, m_rcBtnDown( 0, 0, 0, 0 )
	, m_rcBtnScrollHome( 0, 0, 0, 0 )
	, m_rcBtnScrollEnd( 0, 0, 0, 0 )
	, m_rcBtnClose( 0, 0, 0, 0 )
	, m_rcBtnHelp( 0, 0, 0, 0 )
	, m_rcBtnTabList( 0, 0, 0, 0 )
	, m_nPushedTrackingButton( (-1L) )
	, m_nPushedTrackingHitTest( __ETWH_NOWHERE )
	, m_bPushedTrackingCloseButton( false )
	, m_nHoverTrackingHitTest( __ETWH_NOWHERE )
	, m_nItemsExtent( 0 )
	, m_nScrollPos( 0 )
	, m_nScrollMaxPos( 0 )
	, m_nScrollDirectionRest( 0 )
	, m_rcRecalcLayout( 0, 0, 0, 0 )
	, m_ptStartDrag( -1, -1 )
	, m_bEnableTrackToolTips( false )
	, m_bPushedUp( false )
	, m_bPushedDown( false )
	, m_bPushedScrollHome( false )
	, m_bPushedScrollEnd( false )
	, m_bPushedHelp( false )
	, m_bPushedClose( false )
	, m_bPushedTabList( false )
	, m_pHelperLastPaintManagerRTC( NULL )
	, m_rcLastMovedItemRect( 0, 0, 0, 0 )
	, m_bDragging( false )
	, m_nHoverTrackedIndex( -1L )
	, m_hFont( NULL )
{
	VERIFY( RegisterTabWndClass() );

	PmBridge_Install();
}

CExtTabWnd::~CExtTabWnd()
{
	PmBridge_Uninstall();

	_RemoveAllItemsImpl();
}

bool CExtTabWnd::_IsMdiTabCtrl() const
{
	return false;
}

bool CExtTabWnd::_IsCustomLayoutTabWnd() const
{
	return false;
}

BEGIN_MESSAGE_MAP(CExtTabWnd, CWnd)
	//{{AFX_MSG_MAP(CExtTabWnd)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_SIZE()
	ON_WM_WINDOWPOSCHANGED()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDBLCLK()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEACTIVATE()
	ON_WM_SETFOCUS()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_MBUTTONDBLCLK()
	ON_WM_CAPTURECHANGED()
	ON_WM_CANCELMODE()
	ON_WM_TIMER()
	ON_WM_SHOWWINDOW()
	ON_WM_SETCURSOR()
	ON_MESSAGE(WM_SETFONT, OnSetFont)
	ON_MESSAGE(WM_GETFONT, OnGetFont)	
	//}}AFX_MSG_MAP
	ON_MESSAGE( WM_SIZEPARENT, OnSizeParent )
	ON_WM_SYSCOLORCHANGE()
	__EXT_MFC_SAFE_ON_WM_SETTINGCHANGE()
	ON_MESSAGE( __ExtMfc_WM_THEMECHANGED, OnThemeChanged )
	ON_REGISTERED_MESSAGE( CExtControlBar::g_nMsgQueryRepositionCalcEffect, _OnQueryRepositionCalcEffect )
	ON_REGISTERED_MESSAGE( CExtContentExpandWnd::g_nMsgPaintItemContent, _OnPaintExpandedItemContent )
END_MESSAGE_MAP()

#ifdef _DEBUG

void CExtTabWnd::AssertValid() const
{
	CWnd::AssertValid();
__EXT_MFC_INT_PTR nItemCount = m_arrItems.GetSize();
	ASSERT(
			m_nVisibleItemCount >= 0
		&&	m_nVisibleItemCount <= nItemCount
		);
}

void CExtTabWnd::Dump(CDumpContext& dc) const
{
	CWnd::Dump( dc );
}

#endif

LRESULT CExtTabWnd::OnSetFont( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
HFONT hFont = (HFONT) wParam;
BOOL bRedraw = (BOOL) lParam;
	m_hFont = hFont; 
	UpdateTabWnd( true );
    if( bRedraw )
        Invalidate();
	return 0L;
}
  
LRESULT CExtTabWnd::OnGetFont( WPARAM, LPARAM )
{
	ASSERT_VALID( this );
    return (LRESULT) m_hFont;
}

LONG CExtTabWnd::GetHoverTrackingItem() const
{
	ASSERT_VALID( this );
	if( _IsDND() )
		return -1L;
	return m_nHoverTrackingHitTest;
}

LONG CExtTabWnd::GetPushedTrackingItem() const
{
	ASSERT_VALID( this );
	if( _IsDND() )
		return -1L;
	return m_nPushedTrackingButton;
}

HFONT CExtTabWnd::OnQueryFont() const
{
//	ASSERT_VALID( this );
//HFONT hFont = NULL;
//	if( GetSafeHwnd() != NULL )
//		hFont = (HFONT) ::SendMessage( m_hWnd, WM_GETFONT, 0L, 0L );
//	if( hFont == NULL )
//	{
//		HWND hWndParent = ::GetParent( m_hWnd );
//		if( hWndParent != NULL )
//			hFont = (HFONT)
//				::SendMessage( hWndParent, WM_GETFONT, 0L, 0L );
//	} // if( hFont == NULL )
//	if( hFont == NULL )
//	{
//		hFont = (HFONT)::GetStockObject( DEFAULT_GUI_FONT );
//		if( hFont == NULL )
//			hFont = (HFONT)::GetStockObject( SYSTEM_FONT );
//	} // if( hFont == NULL )
//	return hFont;
	return NULL;
}

void CExtTabWnd::_GetTabWndFont(
	CFont * pFont,
	bool bSelected,
	DWORD dwOrientation // = DWORD(-1) // default orientation
	) const
{
	ASSERT_VALID( this );
HFONT hPreferredFont = OnQueryFont();
	if( hPreferredFont != NULL )
	{
		LOGFONT _lf;
		::memset( &_lf, 0, sizeof(LOGFONT) );
		::GetObject( (HGDIOBJ)hPreferredFont, sizeof(LOGFONT), &_lf );
		if( pFont->GetSafeHandle() != NULL )
			pFont->DeleteObject();
		pFont->CreateFontIndirect( &_lf );
		return;
	}

	if( dwOrientation == DWORD(-1) )
		dwOrientation = OrientationGet();
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
bool bBold =
		( bSelected && SelectionBoldGet() )
			? true
			: false
			;
CFont * pSrcFont = bBold ? (&pPM->m_FontBold) : (&pPM->m_FontNormal);
	ASSERT( pSrcFont != NULL );
	ASSERT( pSrcFont->GetSafeHandle() != NULL );
LOGFONT _lf;
	::memset( &_lf, 0, sizeof(LOGFONT) );
	pSrcFont->GetLogFont( &_lf );
	if( pFont->GetSafeHandle() != NULL )
		pFont->DeleteObject();
	pFont->CreateFontIndirect( &_lf );
}

void CExtTabWnd::_RemoveAllItemsImpl()
{
	ASSERT_VALID( this );

LONG nCount = (LONG)m_arrItems.GetSize();
	
	if( nCount > 0 )
	{
		OnTabWndRemoveAllItems( true );

		for( LONG nItem = 0; nItem < nCount; nItem ++ )
		{
			TAB_ITEM_INFO * pTII = m_arrItems[ nItem ];
			delete pTII;
		}
		m_arrItems.RemoveAll();
		m_arrItems.FreeExtra();

		m_nVisibleItemCount = 0;
		m_nSelIndex = -1;
		m_bDelayRecalcLayout = true;

		ASSERT_VALID( this );

		OnTabWndRemoveAllItems( false );
	} // if( nCount > 0 )
}

void CExtTabWnd::_OnTabItemHook_GetItemStyle(
	const CExtTabWnd::TAB_ITEM_INFO & _TII,
	DWORD & dwItemStyle
	) const
{
	ASSERT_VALID( this );
	_TII;
	dwItemStyle;
}

void CExtTabWnd::_OnTabItemHook_ModifyItemStyle(
	CExtTabWnd::TAB_ITEM_INFO & _TII,
	DWORD dwItemStyle,
	DWORD & dwRemove,
	DWORD & dwAdd
	)
{
	ASSERT_VALID( this );
	_TII;
	dwItemStyle;
	dwRemove;
	dwAdd;
}

void CExtTabWnd::_OnTabItemHook_GetItemStyleEx(
	const CExtTabWnd::TAB_ITEM_INFO & _TII,
	DWORD & dwItemStyleEx
	) const
{
	ASSERT_VALID( this );
	_TII;
	dwItemStyleEx;
}

void CExtTabWnd::_OnTabItemHook_ModifyItemStyleEx(
	CExtTabWnd::TAB_ITEM_INFO & _TII,
	DWORD dwItemStyleEx,
	DWORD & dwRemove,
	DWORD & dwAdd
	)
{
	ASSERT_VALID( this );
	_TII;
	dwItemStyleEx;
	dwRemove;
	dwAdd;
}

DWORD CExtTabWnd::GetTabWndStyle() const
{
	ASSERT_VALID( this );
	return m_dwTabWndStyle;
}

DWORD CExtTabWnd::ModifyTabWndStyle(
	DWORD dwRemove,
	DWORD dwAdd, // = 0,
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
DWORD dwOldStyle = m_dwTabWndStyle;
	m_dwTabWndStyle &= ~dwRemove;
	m_dwTabWndStyle |= dwAdd;
	if( m_dwTabWndStyle & __ETWS_EQUAL_WIDTHS )
		m_nScrollPos = 0;
	UpdateTabWnd( bUpdateTabWnd );
	return dwOldStyle;
}

DWORD CExtTabWnd::GetTabWndStyleEx() const
{
	ASSERT_VALID( this );
	return m_dwTabWndStyleEx;
}

DWORD CExtTabWnd::ModifyTabWndStyleEx(
	DWORD dwRemove,
	DWORD dwAdd, // = 0,
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
DWORD dwOldStyleEx = m_dwTabWndStyleEx;
	m_dwTabWndStyleEx &= ~dwRemove;
	m_dwTabWndStyleEx |= dwAdd;
	UpdateTabWnd( bUpdateTabWnd );
	return dwOldStyleEx;
}

DWORD CExtTabWnd::OrientationGet() const
{
	return GetTabWndStyle() & __ETWS_ORIENT_MASK;
}

DWORD CExtTabWnd::OrientationSet(
	DWORD dwOrientation,
	bool bUpdateTabWnd // = false
	)
{
	DWORD dwOldOrientation =
		GetTabWndStyle() & __ETWS_ORIENT_MASK;
	ModifyTabWndStyle(
		__ETWS_ORIENT_MASK,
		dwOrientation & __ETWS_ORIENT_MASK
		);
	UpdateTabWnd( bUpdateTabWnd );
	return dwOldOrientation;
}

bool CExtTabWnd::OrientationIsHorizontal() const
{
	DWORD dwOrientation = OrientationGet();
	if( dwOrientation == __ETWS_ORIENT_TOP
		|| dwOrientation == __ETWS_ORIENT_BOTTOM
		)
		return true;
	return false;
}

bool CExtTabWnd::OrientationIsVertical() const
{
	return !OrientationIsHorizontal();
}

bool CExtTabWnd::OrientationIsTopLeft() const
{
	DWORD dwOrientation = OrientationGet();
	if( dwOrientation == __ETWS_ORIENT_TOP
		|| dwOrientation == __ETWS_ORIENT_LEFT
		)
		return true;
	return false;
}

HWND CExtTabWnd::PmBridge_GetSafeHwnd() const
{
__PROF_UIS_MANAGE_STATE;
HWND hWnd = GetSafeHwnd();
	return hWnd;
}

void CExtTabWnd::PmBridge_OnPaintManagerChanged(
	CExtPaintManager * pGlobalPM
	)
{
LONG nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		TAB_ITEM_INFO * pTII = ItemGet( nIndex );
		if( pTII != NULL )
		{
			CExtCmdIcon * pIcon = pTII->IconGetPtr();
			if( pIcon != NULL )
				pIcon->OnEmptyGeneratedBitmaps();
		}
	}
	CExtPmBridge::PmBridge_OnPaintManagerChanged( pGlobalPM );
}

LONG CExtTabWnd::ItemGetCount() const
{
	ASSERT_VALID( this );
	return (LONG)m_arrItems.GetSize();
}

LONG CExtTabWnd::ItemGetVisibleCount() const
{
	ASSERT_VALID( this );
INT nVisibleItemsCount = 0L;
	for( LONG nIndex = 0; nIndex < m_arrItems.GetSize(); nIndex++ )
	{
		const TAB_ITEM_INFO * pTii = ItemGet( nIndex );
		if( pTii != NULL ) 
		{
			ASSERT_VALID( pTii );
			if( pTii->VisibleGet() )
				nVisibleItemsCount ++;			
		}	
	}
	return nVisibleItemsCount;
//	return m_nVisibleItemCount;
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::ItemGet( LONG nIndex )
{
	ASSERT_VALID( this );
LONG nCount = ItemGetCount();
	if( nIndex < 0 || nIndex >= nCount )
	{
		ASSERT( FALSE );
		return NULL;
	}
	TAB_ITEM_INFO * pTii = m_arrItems[ nIndex ];
	ASSERT_VALID( pTii );
	ASSERT( pTii->m_pWndTab == this );
	return pTii;
}

const CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::ItemGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	return
		(const_cast < CExtTabWnd * > ( this ))->ItemGet( nIndex );
}

__EXT_MFC_SAFE_LPCTSTR CExtTabWnd::ItemTextGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	return ItemGet(nIndex)->TextGet();
}

void CExtTabWnd::ItemTextSet(
	LONG nIndex,
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	ItemGet(nIndex)->TextSet( sText );
	UpdateTabWnd( bUpdateTabWnd );
}

bool CExtTabWnd::ItemEnabledGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	return ItemGet( nIndex )->EnabledGet();
}

void CExtTabWnd::ItemEnabledSet(
	LONG nIndex,
	bool bEnabled, // = true
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	ItemGet( nIndex )->EnabledSet( bEnabled );
	UpdateTabWnd( bUpdateTabWnd );
}

__EXT_MFC_SAFE_LPCTSTR CExtTabWnd::ItemTooltipTextGet( LONG nIndex ) const
{
	return ItemGet(nIndex)->TooltipTextGet();
}

void CExtTabWnd::ItemTooltipTextSet(
	LONG nIndex,
	__EXT_MFC_SAFE_LPCTSTR sTooltipText, // = NULL
	bool bUpdateTabWnd // = false
	)
{
	ItemGet(nIndex)->TooltipTextSet( sTooltipText );
	UpdateTabWnd( bUpdateTabWnd );
}

LPARAM CExtTabWnd::ItemLParamGet( LONG nIndex ) const
{
	return ItemGet(nIndex)->LParamGet();
}

void CExtTabWnd::ItemLParamSet(
	LONG nIndex,
	LPARAM lParam // = 0
	)
{
	ItemGet(nIndex)->LParamSet( lParam );
}

CExtCmdIcon & CExtTabWnd::ItemIconGet( LONG nIndex )
{
CExtCmdIcon * pIcon =
		ItemGet(nIndex)->IconGetPtr();
	if( pIcon == NULL )
	{
		static CExtCmdIcon g_EmptyIcon;
		return g_EmptyIcon;
	}
	return *pIcon;
}

const CExtCmdIcon & CExtTabWnd::ItemIconGet( LONG nIndex ) const
{
	return
		( const_cast < CExtTabWnd * > ( this ) )
		-> ItemIconGet( nIndex );
}

void CExtTabWnd::ItemIconSet( // ADDED 2.53
	LONG nIndex,
	const CExtCmdIcon & _icon,
	bool bUpdateTabWnd // = false
	)
{
	ItemGet(nIndex)->IconSet( _icon );
	UpdateTabWnd( bUpdateTabWnd );
}

void CExtTabWnd::ItemIconSet(
	LONG nIndex,
	HICON hIcon, // = NULL
	bool bCopyIcon, // = true,
	bool bUpdateTabWnd // = false
	)
{
	ItemGet(nIndex)->IconSet( hIcon, bCopyIcon );
	UpdateTabWnd( bUpdateTabWnd );
}

DWORD CExtTabWnd::ItemStyleGet( LONG nIndex )
{
	return ItemGet(nIndex)->GetItemStyle();
}

DWORD CExtTabWnd::ItemStyleModify(
	LONG nIndex,
	DWORD dwRemove,
	DWORD dwAdd, // = 0
	bool bUpdateTabWnd // = false
	)
{
	DWORD dwOldItemStyle =
		ItemGet(nIndex)->ModifyItemStyle(dwRemove,dwAdd);
	UpdateTabWnd( bUpdateTabWnd );
	return dwOldItemStyle;
}

LONG CExtTabWnd::ItemGetIndexOf( const CExtTabWnd::TAB_ITEM_INFO * pTii ) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	LONG nCount = ItemGetCount();
	for( LONG nIndex = 0; nIndex < nCount; nIndex++ )
	{
		const TAB_ITEM_INFO * pTii2 = ItemGet( nIndex );
		if( pTii2 == pTii )
			return nIndex;
	} // for( LONG nIndex = 0; nIndex < nCount; nIndex++ )
	ASSERT( FALSE );
	return -1;
}

LONG CExtTabWnd::SelectionGet() const
{
	ASSERT_VALID( this );
	return m_nSelIndex;
}

const CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::SelectionGetPtr() const
{
	ASSERT_VALID( this );
	if( m_nSelIndex < 0 || ItemGetCount() == 0 )
		return NULL;
	return ItemGet( m_nSelIndex );
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::SelectionGetPtr()
{
	ASSERT_VALID( this );
	LONG nCount = ItemGetCount();
	if( m_nSelIndex < 0 || nCount == 0 )
		return NULL;
	return ItemGet( m_nSelIndex );
}

bool CExtTabWnd::SelectionBoldGet() const
{
	bool bSelectionBold =
		(GetTabWndStyle() & __ETWS_BOLD_SELECTION) ? true : false;
	if( bSelectionBold
		&& (GetTabWndStyle() & __ETWS_GROUPED)
		)
		bSelectionBold = false;
	return bSelectionBold;
}

void CExtTabWnd::SelectionBoldSet(
	bool bBold, // = true
	bool bUpdateTabWnd // = false
	)
{
	ModifyTabWndStyle(
		bBold ? 0 : __ETWS_BOLD_SELECTION,
		bBold ? __ETWS_BOLD_SELECTION : 0,
		bUpdateTabWnd
		);
}

void CExtTabWnd::ItemGetGroupRange(
	LONG nIndex,
	LONG & nGroupStart,
	LONG & nGroupEnd,
	TAB_ITEM_INFO ** ppTiiStart, // = NULL
	TAB_ITEM_INFO ** ppTiiEnd // = NULL
	)
{
	ASSERT_VALID( this );
	TAB_ITEM_INFO * pTii = ItemGet( nIndex );
	ASSERT_VALID( pTii );
	TAB_ITEM_INFO * pTiiStart = pTii->GetInGroupFirst();
	ASSERT_VALID( pTiiStart );
	TAB_ITEM_INFO * pTiiEnd = pTii->GetInGroupLast();
	ASSERT_VALID( pTiiEnd );
	nGroupStart = ItemGetIndexOf( pTiiStart );
	nGroupEnd = ItemGetIndexOf( pTiiEnd );
	if( ppTiiStart != NULL )
		*ppTiiStart = pTiiStart;
	if( ppTiiEnd != NULL )
		*ppTiiEnd = pTiiEnd;
}

void CExtTabWnd::ItemGetGroupRange(
	LONG nIndex,
	LONG & nGroupStart,
	LONG & nGroupEnd,
	const TAB_ITEM_INFO ** ppTiiStart, // = NULL
	const TAB_ITEM_INFO ** ppTiiEnd // = NULL
	) const
{
	ASSERT_VALID( this );
	const TAB_ITEM_INFO * pTii = ItemGet( nIndex );
	ASSERT_VALID( pTii );
	const TAB_ITEM_INFO * pTiiStart = pTii->GetInGroupFirst();
	ASSERT_VALID( pTiiStart );
	const TAB_ITEM_INFO * pTiiEnd = pTii->GetInGroupFirst();
	ASSERT_VALID( pTiiEnd );
	nGroupStart = ItemGetIndexOf( pTiiStart );
	nGroupEnd = ItemGetIndexOf( pTiiEnd );
	if( ppTiiStart != NULL )
		*ppTiiStart = pTiiStart;
	if( ppTiiEnd != NULL )
		*ppTiiEnd = pTiiEnd;
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::ItemInsert(
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL
	HICON hIcon, // = NULL
	bool bCopyIcon, // = true
	DWORD dwItemStyle, // = 0 // visible & non-group
	LONG nIndex, // = -1 // default - append
	LPARAM lParam, // = 0
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
CExtCmdIcon _icon;
	if( hIcon != NULL )
		_icon.AssignFromHICON( hIcon, bCopyIcon );
	return
		ItemInsert(
			sText,
			_icon,
			dwItemStyle,
			nIndex,
			lParam,
			bUpdateTabWnd
			);
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::ItemInsert(
	__EXT_MFC_SAFE_LPCTSTR sText,
	const CExtCmdIcon & _icon,
	DWORD dwItemStyle, // = 0 // visible & non-group
	LONG nIndex, // = -1 // default - append
	LPARAM lParam, // = 0
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
LONG nCount = ItemGetCount();
	if( nIndex < 0 || nIndex > nCount )
		nIndex = nCount;

TAB_ITEM_INFO * pTiiPrev = NULL;
	if( nIndex > 0 )
	{
		pTiiPrev = ItemGet( nIndex - 1 );
		ASSERT_VALID( pTiiPrev );
	}

TAB_ITEM_INFO * pTiiNext = NULL;
	if( nIndex < nCount )
	{
		pTiiNext = ItemGet( nIndex );
		ASSERT_VALID( pTiiNext );
	}

	TAB_ITEM_INFO * pTii =
			new TAB_ITEM_INFO(
			this,
			pTiiPrev,
			pTiiNext,
			sText,
			&_icon,
			dwItemStyle,
			lParam
			);

	m_arrItems.InsertAt( nIndex, pTii );
	ASSERT_VALID( pTii );
	if( m_nSelIndex >= nIndex ) 
		m_nSelIndex++;

	if( pTii->VisibleGet() )
		m_nVisibleItemCount ++;

	ASSERT_VALID( this );

	OnTabWndItemInsert(
		nIndex,
		pTii
		);

	UpdateTabWnd( bUpdateTabWnd );
	return pTii;
}

LONG CExtTabWnd::ItemRemove( // returns count of removed items
	LONG nIndex,
	LONG nCountToRemove, // = 1
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	if( SelectionGet() == nIndex )
		SelectionSet( -1, false, false );
	if( nCountToRemove > 0L )
	{
		LONG nCount = ItemGetCount();
		if( nCount > 0L )
		{
			ASSERT( nIndex >= 0 && nIndex < nCount );
		
			OnTabWndRemoveItem( nIndex, nCountToRemove, true );
			
			LONG nAvailToRemove = nCount - nIndex;
			if( nCountToRemove > nAvailToRemove )
				nCountToRemove = nAvailToRemove;

			if( m_nSelIndex >= ( nIndex + nCountToRemove ) )
				m_nSelIndex -= nCountToRemove;

			for( LONG i = 0L; i < nCountToRemove; i++ )
			{
				TAB_ITEM_INFO * pTii = ItemGet( nIndex );
				ASSERT_VALID( pTii );

				if( pTii->VisibleGet() && m_nVisibleItemCount > 0L )
					m_nVisibleItemCount --;

				if( pTii->SelectedGet() )
					m_nSelIndex = -1L;

				m_arrItems.RemoveAt( nIndex, 1 );

				TAB_ITEM_INFO * pTiiPrev = pTii->GetPrev();
				TAB_ITEM_INFO * pTiiNext = pTii->GetNext();

				if( pTiiPrev != NULL )
					pTiiPrev->m_pNext = pTiiNext;
				if( pTiiNext != NULL )
					pTiiNext->m_pPrev = pTiiPrev;

				delete pTii;
				pTii = NULL;
			}
			
			OnTabWndRemoveItem( nIndex, nCountToRemove, false );
		
			UpdateTabWnd( bUpdateTabWnd );
		
		} // if( nCount > 0 )
		else
			nCountToRemove = 0;
	} // if( nCountToRemove > 0L )
	return nCountToRemove;
}

void CExtTabWnd::ItemRemoveGroup(
	LONG nIndex,
	LONG * p_nGroupStart, // = NULL
	LONG * p_nCountInGroup, // = NULL
	bool bUpdateTabWnd // = false
	)
{
	LONG nGroupStart;
	LONG nGroupEnd;
	ItemGetGroupRange(
		nIndex,
		nGroupStart,
		nGroupEnd
		);
	ASSERT( nGroupEnd >= nGroupStart );
	LONG nCountToRemove = nGroupEnd - nGroupStart + 1;
	ItemRemove(
		nGroupStart,
		nCountToRemove,
		bUpdateTabWnd
		);
	if( p_nGroupStart != NULL )
		*p_nGroupStart = nGroupStart;
	if( p_nCountInGroup != NULL )
		*p_nCountInGroup = nCountToRemove;
}

void CExtTabWnd::ItemRemoveAll(
	bool bUpdateTabWnd // = false
	)
{
	_RemoveAllItemsImpl();
	UpdateTabWnd( bUpdateTabWnd );
}

LONG CExtTabWnd::ItemHitTest(
	const POINT & ptClient
	) const
{
	ASSERT_VALID( this );
	if(	GetSafeHwnd() == NULL )
		return __ETWH_NOWHERE;
CRect rcClient;
	GetClientRect( &rcClient );
	if( rcClient.IsRectEmpty()
		|| (! rcClient.PtInRect(ptClient) )
		)
		return __ETWH_NOWHERE;
	
	if(		(! m_rcTabNearBorderArea.IsRectEmpty() )
		&&	m_rcTabNearBorderArea.PtInRect(ptClient)
		)
		return __ETWH_BORDER_AREA;

	if(		(! m_rcBtnUp.IsRectEmpty() )
		&&	m_rcBtnUp.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_LEFTUP;
	if(		(! m_rcBtnDown.IsRectEmpty() )
		&&	m_rcBtnDown.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_RIGHTDOWN;
	if(		(! m_rcBtnScrollHome.IsRectEmpty() )
		&&	m_rcBtnScrollHome.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_SCROLL_HOME;
	if(		(! m_rcBtnScrollEnd.IsRectEmpty() )
		&&	m_rcBtnScrollEnd.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_SCROLL_END;
	if(		(! m_rcBtnHelp.IsRectEmpty() )
		&&	m_rcBtnHelp.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_HELP;
	if(		(! m_rcBtnClose.IsRectEmpty() )
		&&	m_rcBtnClose.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_CLOSE;
	if(		(! m_rcBtnTabList.IsRectEmpty() )
		&&	m_rcBtnTabList.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_TAB_LIST;
	if(		m_rcTabItemsArea.IsRectEmpty()
		||	(! m_rcTabItemsArea.PtInRect(ptClient) )
		)
		return __ETWH_NOWHERE;

	if( ItemGetVisibleCount() == 0 )
		return __ETWH_ITEMS_AREA;

bool bHorz = OrientationIsHorizontal();

CPoint ptClientTest(
		ptClient.x +
			( bHorz
				? (m_nScrollPos - m_rcTabItemsArea.left)
				: 0
			),
		ptClient.y +
			( bHorz
				? 0
				: (m_nScrollPos - m_rcTabItemsArea.top)
			)
		);

LONG nItemCount = ItemGetCount();
LONG nVisCount = ItemGetVisibleCount();
	if(		nItemCount > 0
		&&	nVisCount > 0
		&&	m_nIndexVisFirst >= 0
		&&	m_nIndexVisLast >= 0
		&&	m_nIndexVisFirst < nItemCount
		&&	m_nIndexVisLast < nItemCount
		)
	{
		ASSERT( m_nIndexVisFirst <= m_nIndexVisLast );
		ASSERT( 0 <= m_nIndexVisFirst && m_nIndexVisFirst < nItemCount );
		ASSERT( 0 <= m_nIndexVisLast && m_nIndexVisLast < nItemCount );
		for( LONG nIndex = m_nIndexVisFirst; nIndex <= m_nIndexVisLast; nIndex++ )
		{
			const TAB_ITEM_INFO * pTii = ItemGet( nIndex );
			ASSERT_VALID( pTii );
			if( !pTii->VisibleGet() )
				continue;
			if( pTii->HitTest( ptClientTest ) )
				//return nIndex;
				return pTii->GetIndexOf();
			
		} // for( LONG nIndex = m_nIndexVisFirst; nIndex <= m_nIndexVisLast; nIndex++ )
	} // if( nItemCount > 0 && nVisCount > 0 )

	return __ETWH_ITEMS_AREA;
}

bool CExtTabWnd::ItemEnsureVisible(
	INT nItemIndex,
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	ASSERT( nItemIndex >= 0 && nItemIndex < ItemGetCount() );
bool bRetVal = false;
	if( _IsScrollAvail() )
	{
		TAB_ITEM_INFO * pTii = ItemGet( nItemIndex );
		ASSERT_VALID( pTii );
		if( pTii->VisibleGet() )
		{
			bool bHorz = OrientationIsHorizontal();
			
			CRect rcItem = pTii->ItemRectGet();
			rcItem.OffsetRect(
				bHorz ? (-m_nScrollPos) : 0,
				bHorz ? 0 : (-m_nScrollPos)
				);
			
			INT nItemExtentMinVal =
				bHorz ? rcItem.left : rcItem.top;
			
			INT nItemExtentMaxVal =
				bHorz ? rcItem.right : rcItem.bottom;
			
			INT nAreaExtentMinVal = 0;
			INT nAreaExtentMaxVal =
				bHorz
					? (m_rcTabItemsArea.right - m_rcTabItemsArea.left)
					: (m_rcTabItemsArea.bottom - m_rcTabItemsArea.top)
					;
			nAreaExtentMaxVal += __EXTTAB_ADD_END_SCROLL_SPACE;

			if( nItemExtentMaxVal > nAreaExtentMaxVal  )
			{
				INT nShift = nItemExtentMaxVal - nAreaExtentMaxVal;
				ASSERT( nShift > 0 );
				m_nScrollPos += nShift;
				
				INT nScrollRest = m_nScrollMaxPos - m_nScrollPos;
				nScrollRest = min( nScrollRest, __EXTTAB_ADD_END_SCROLL_SPACE );
				m_nScrollPos += nScrollRest;
				
				ASSERT( m_nScrollPos >= 0 && m_nScrollPos <= m_nScrollMaxPos );
			}
			if( nItemExtentMinVal < nAreaExtentMinVal  )
			{
				INT nShift = nAreaExtentMinVal - nItemExtentMinVal;
				ASSERT( nShift > 0 );
				m_nScrollPos -= nShift;

				INT nScrollRest = min( m_nScrollPos, __EXTTAB_ADD_END_SCROLL_SPACE );
				m_nScrollPos -= nScrollRest;
				
				ASSERT( m_nScrollPos >= 0 && m_nScrollPos <= m_nScrollMaxPos );
			}
			
			if( m_nScrollPos > m_nScrollMaxPos )
				m_nScrollPos = m_nScrollMaxPos;
			
			bRetVal = true;
		} // if( pTii->VisibleGet() )
	} // if( _IsScrollAvail() )
	else
		bRetVal = true;
	UpdateTabWnd( bUpdateTabWnd );
	return bRetVal;
}

bool CExtTabWnd::g_bTabWndClassRegistered = false;

bool CExtTabWnd::RegisterTabWndClass()
{
	if( g_bTabWndClassRegistered )
		return true;
WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo( hInst, __EXT_TAB_WND_CLASS_NAME, &_wndClassInfo ) )
	{
		_wndClassInfo.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS | CS_OWNDC | CS_GLOBALCLASS;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor = ( g_hCursor != NULL ) ? g_hCursor : ::LoadCursor( NULL, IDC_ARROW );
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_TAB_WND_CLASS_NAME;
		if( ! ::AfxRegisterClass( &_wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}
	g_bTabWndClassRegistered = true;
	return true;
}

CRect CExtTabWnd::CalcPreviewLayout( const CRect & rcAvail )
{
	ASSERT_VALID( this );
	m_rcRecalcLayout = rcAvail;
	//m_bDelayRecalcLayout = true;
	_RecalcLayoutImpl();
	m_rcRecalcLayout.SetRect( 0, 0, 0, 0 );
	
	CRect rcResult;
	if( m_rcTabItemsArea.IsRectEmpty() )
		GetWindowRect( &rcResult );
	else
		rcResult = m_rcTabItemsArea;

	m_bDelayRecalcLayout = true;
	return rcResult;
}

BOOL CExtTabWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
	UINT nDlgCtrlID, // = UINT( __EXT_MFC_IDC_STATIC )
	DWORD dwWindowStyle, // = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS
	DWORD dwTabStyle, // = __ETWS_DEFAULT
	CCreateContext * pContext // = NULL
	)
{
	if( ! RegisterTabWndClass() )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	m_bDirectCreateCall = true;
	m_dwTabWndStyle = dwTabStyle;
	if( ! CWnd::Create(
			__EXT_TAB_WND_CLASS_NAME,
			NULL,
			dwWindowStyle,
			rcWnd,
			pParentWnd,
			nDlgCtrlID,
			pContext
			)
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		::AfxThrowMemoryException();
	}
	return TRUE;
}

bool CExtTabWnd::_CreateHelper()
{
	EnableToolTips( TRUE );

	if( OnTabWndToolTipQueryEnabled() )
	{ // tooltips enabled at all?
		if( ! m_wndToolTip.Create( this ) )
		{
			ASSERT( FALSE );
			return false;
		}
		m_wndToolTip.Activate( TRUE );
	}

	UpdateTabWnd( false );
	return true;
}

void CExtTabWnd::PreSubclassWindow() 
{
	CWnd::PreSubclassWindow();

	if( m_bDirectCreateCall )
		return;

__EXT_MFC_LONG_PTR dwStyle = ::__EXT_MFC_GetWindowLong( m_hWnd, GWL_STYLE );
	m_dwTabWndStyle = DWORD( dwStyle & __EXTMFC_ALL_FORM_MOVABLE_WND_STYLES );
	::__EXT_MFC_SetWindowLong( m_hWnd, GWL_STYLE, dwStyle & (~__EXTMFC_ALL_FORM_MOVABLE_WND_STYLES) );
	
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		::AfxThrowMemoryException();
	}
}

LRESULT CExtTabWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if( message == WM_PRINT || message == WM_PRINTCLIENT )
	{
		CDC * pDC = CDC::FromHandle( (HDC)wParam );
		CRect rcClient;
		GetClientRect( &rcClient );
		DoPaint( pDC, rcClient );
		return (!0);
	}	
	if(		message == WM_NOTIFY
		&&	m_wndToolTip.GetSafeHwnd() != NULL
		&&	((LPNMHDR)lParam) != NULL
		&&	((LPNMHDR)lParam)->hwndFrom == m_wndToolTip.GetSafeHwnd()
		&&	((LPNMHDR)lParam)->code == TTN_SHOW
		)
		::SetWindowPos(
			m_wndToolTip.GetSafeHwnd(),
			HWND_TOP,
			0,0,0,0,
			SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE
			);
	return CWnd::WindowProc(message, wParam, lParam);
}

BOOL CExtTabWnd::PreTranslateMessage(MSG* pMsg) 
{
	if(		OnTabWndToolTipQueryEnabled()
		&&	OnAdvancedPopupMenuTipWndGet() == NULL
		&&	m_wndToolTip.GetSafeHwnd() != NULL
		)
		m_wndToolTip.RelayEvent( pMsg );

	return CWnd::PreTranslateMessage(pMsg);
}

CExtPopupMenuTipWnd * CExtTabWnd::OnAdvancedPopupMenuTipWndGet() const
{
	if( ! CExtControlBar::g_bUseAdvancedToolTips )
		return NULL;
	return (&( CExtPopupMenuSite::g_DefPopupMenuSite.GetTip() ));
}

void CExtTabWnd::OnAdvancedPopupMenuTipWndDisplay(
	CExtPopupMenuTipWnd & _ATTW,
	const RECT & rcExcludeArea,
	__EXT_MFC_SAFE_LPCTSTR strTipText
	) const
{
	ASSERT_VALID( this );
	ASSERT( strTipText != NULL && _tcslen( strTipText ) > 0 );
	_ATTW.SetTipStyle( CExtPopupMenuTipWnd::__ETS_RECTANGLE_NO_ICON );
	_ATTW.SetText( strTipText );
	_ATTW.Show( (CWnd*)this, rcExcludeArea );
}

BOOL CExtTabWnd::PreCreateWindow(CREATESTRUCT& cs) 
{
	if(		( ! RegisterTabWndClass() )
		||	( ! CWnd::PreCreateWindow( cs ) )
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	cs.lpszClass = __EXT_TAB_WND_CLASS_NAME;
	return TRUE;
}

bool CExtTabWnd::_ProcessMouseClick(
	CPoint point,
	bool bButtonPressed,
	INT nMouseButton, // MK_... values
	UINT nMouseEventFlags
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return false;
	if( bButtonPressed )
	{
		CWnd * pWndTestChildFrame = GetParentFrame();
		if( pWndTestChildFrame != NULL && pWndTestChildFrame->IsKindOf( RUNTIME_CLASS( CMDIChildWnd ) ) )
		{
			CFrameWnd * pWndFrame = pWndTestChildFrame->GetParentFrame();
			if( pWndFrame != NULL )
			{
				CMDIFrameWnd * pWndMDIFrame = DYNAMIC_DOWNCAST( CMDIFrameWnd, pWndFrame );
				if( pWndMDIFrame != NULL )
				{
					CMDIChildWnd * pActive = pWndMDIFrame->MDIGetActive();
					if( pWndTestChildFrame != pActive )
						((CMDIChildWnd*)pWndTestChildFrame)->MDIActivate();
				}
			}
		}
	} // if( bButtonPressed )
LONG nHitTest = ItemHitTest( point );
	switch( nHitTest )
	{
	case __ETWH_BUTTON_LEFTUP:
	case __ETWH_BUTTON_RIGHTDOWN:
	case __ETWH_BUTTON_SCROLL_HOME:
	case __ETWH_BUTTON_SCROLL_END:
	case __ETWH_BUTTON_HELP:
	case __ETWH_BUTTON_CLOSE:
	case __ETWH_BUTTON_TAB_LIST:
		return OnTabWndClickedButton( nHitTest, bButtonPressed, nMouseButton, nMouseEventFlags );
	default:
		if( nHitTest < __ETWH_TAB_FIRST )
		{
			OnTabWndMouseTrackingPushedStop();
			Invalidate();
			UpdateWindow();
			return false; //true;
		}
	break;
	}
	ASSERT( nHitTest >= 0 && nHitTest < ItemGetCount() );
	if( bButtonPressed )
		m_ptStartDrag = point;
	else
	{
		if( m_nPushedTrackingButton >= 0 && m_bPushedTrackingCloseButton )
		{
			CPoint ptCursor;
			if( ::GetCursorPos( &ptCursor ) )
			{
				LONG nPushedTrackingButton = m_nPushedTrackingButton;
				ScreenToClient( &ptCursor );
				if( ItemGet( m_nPushedTrackingButton )->CloseButtonRectGet().PtInRect( ptCursor ) )
				{
					if( ::GetCapture() == m_hWnd )
						::ReleaseCapture();
					OnTabWndClickedItemCloseButton( nPushedTrackingButton );
				}
			}
			return true;
		}
	}
	return OnTabWndClickedItem( nHitTest, bButtonPressed, nMouseButton, nMouseEventFlags );
}

void CExtTabWnd::UpdateTabWnd(
	bool bImmediate // = true
	)
{
	ASSERT_VALID( this );
	m_bDelayRecalcLayout = true;
	if( GetSafeHwnd() == NULL )
		return;
	if( bImmediate && ( (GetStyle() & WS_VISIBLE) != 0 ) )
	{
		_RecalcLayoutImpl();
		Invalidate();
		UpdateWindow();
	}
}

int CExtTabWnd::OnTabWndQueryMultiRowColumnDistance() const
{
	ASSERT_VALID( this );
	return 0;
}

bool CExtTabWnd::OnTabWndQueryMultiRowColumnLayout() const
{
	ASSERT_VALID( this );
DWORD dwTabWndStyle = GetTabWndStyle();
	if( (dwTabWndStyle&__ETWS_MULTI_ROW_COLUMN) != 0 )
		return true;
	return false;
}

void CExtTabWnd::OnTabWndSyncVisibility()
{
	ASSERT_VALID( this );
}

void CExtTabWnd::OnTabWndButtonsRecalcLayout(
	CRect &rcButtonsArea,
	INT nButtonExtent,
	INT nBetweenButtonExtent
	)
{
	ASSERT_VALID( this );

	m_rcBtnUp.SetRectEmpty();
	m_rcBtnDown.SetRectEmpty();
	m_rcBtnScrollHome.SetRectEmpty();
	m_rcBtnScrollEnd.SetRectEmpty();
	m_rcBtnHelp.SetRectEmpty();
	m_rcBtnClose.SetRectEmpty();
	m_rcBtnTabList.SetRectEmpty();

DWORD dwTabWndStyle = GetTabWndStyle();
bool bShowHelp = (dwTabWndStyle&__ETWS_SHOW_BTN_HELP) ? true : false;
bool bShowClose = (dwTabWndStyle&__ETWS_SHOW_BTN_CLOSE) ? true : false;
bool bShowTabList = (dwTabWndStyle&__ETWS_SHOW_BTN_TAB_LIST) ? true : false;
bool bShowScrollHome = (dwTabWndStyle&__ETWS_SHOW_BTN_SCROLL_HOME) ? true : false;
bool bShowScrollEnd = (dwTabWndStyle&__ETWS_SHOW_BTN_SCROLL_END) ? true : false;
bool bEqualWidth = (dwTabWndStyle&__ETWS_EQUAL_WIDTHS) ? true : false;
bool bMultiRowColumn = OnTabWndQueryMultiRowColumnLayout();
	if( bMultiRowColumn )
		bShowScrollHome = bShowScrollEnd = false;

bool bHorz = OrientationIsHorizontal();

INT nButtonShift = - nBetweenButtonExtent - nButtonExtent;

	if( bShowClose )
	{
		m_rcBtnClose = rcButtonsArea;
		rcButtonsArea.OffsetRect(
			bHorz ? nButtonShift : 0,
			bHorz ? 0 : nButtonShift
			);
	} // if( bShowClose )
	if( bShowTabList )
	{
		m_rcBtnTabList = rcButtonsArea;
		rcButtonsArea.OffsetRect(
			bHorz ? nButtonShift : 0,
			bHorz ? 0 : nButtonShift
			);
	} // if( bShowTabList )
	if( bShowHelp )
	{
		m_rcBtnHelp = rcButtonsArea;
		rcButtonsArea.OffsetRect(
			bHorz ? nButtonShift : 0,
			bHorz ? 0 : nButtonShift
			);
	} // if( bShowHelp )

	// auto enable/disable scroll buttons
	m_dwTabWndStyle &= ~(__ETWS_ENABLED_BTN_UP|__ETWS_ENABLED_BTN_DOWN);
	m_dwTabWndStyle &= ~(__ETWS_ENABLED_BTN_SCROLL_HOME|__ETWS_ENABLED_BTN_SCROLL_END);

	if(		(! bEqualWidth )
		&&	(	_IsScrollAvail()
			||	(	(m_dwTabWndStyle & __ETWS_AUTOHIDE_SCROLL) == 0
				&&	(! bMultiRowColumn )
				)
			)
		)
	{
		if( bShowScrollEnd )
	{
			m_rcBtnScrollEnd = rcButtonsArea;
			rcButtonsArea.OffsetRect(
				bHorz ? nButtonShift : 0,
				bHorz ? 0 : nButtonShift
				);
		} // if( bShowScrollEnd )

		m_rcBtnDown = rcButtonsArea;
		rcButtonsArea.OffsetRect(
			bHorz ? nButtonShift : 0,
			bHorz ? 0 : nButtonShift
			);
		m_rcBtnUp = rcButtonsArea;
		rcButtonsArea.OffsetRect(
			bHorz ? nButtonShift : 0,
			bHorz ? 0 : nButtonShift
			);

		if( bShowScrollHome )
		{
			m_rcBtnScrollHome = rcButtonsArea;
			rcButtonsArea.OffsetRect(
				bHorz ? nButtonShift : 0,
				bHorz ? 0 : nButtonShift
				);
		} // if( bShowScrollHome )

		if( _IsScrollAvail() && m_nScrollPos > 0 )
			m_dwTabWndStyle |= (__ETWS_ENABLED_BTN_UP|__ETWS_ENABLED_BTN_SCROLL_HOME);
		if( _IsScrollAvail() && m_nScrollPos < m_nScrollMaxPos )
			m_dwTabWndStyle |= (__ETWS_ENABLED_BTN_DOWN|__ETWS_ENABLED_BTN_SCROLL_END);
	}
}

INT CExtTabWnd::OnTabWndButtonsCalcWidth(
	INT nButtonExtent,
	INT nBetweenButtonExtent
	)
{
	ASSERT_VALID( this );

DWORD dwTabWndStyle = GetTabWndStyle();
bool bShowHelp = (dwTabWndStyle&__ETWS_SHOW_BTN_HELP) ? true : false;
bool bShowClose = (dwTabWndStyle&__ETWS_SHOW_BTN_CLOSE) ? true : false;
bool bShowTabList = (dwTabWndStyle&__ETWS_SHOW_BTN_TAB_LIST) ? true : false;
bool bShowScrollHome = (dwTabWndStyle&__ETWS_SHOW_BTN_SCROLL_HOME) ? true : false;
bool bShowScrollEnd = (dwTabWndStyle&__ETWS_SHOW_BTN_SCROLL_END) ? true : false;
bool bEqualWidth = (dwTabWndStyle&__ETWS_EQUAL_WIDTHS) ? true : false;
bool bMultiRowColumn = OnTabWndQueryMultiRowColumnLayout();
	if( bMultiRowColumn )
		bShowScrollHome = bShowScrollEnd = false;

INT nWidth = 0;
bool bFirstShiftPassed = false;
	if( bShowClose )
	{
		if( ! bFirstShiftPassed )
		{
			nWidth += nBetweenButtonExtent;
			bFirstShiftPassed = true;
		}
		nWidth += nButtonExtent + nBetweenButtonExtent;
	} // if( bShowClose )
	if( bShowHelp )
	{
		if( ! bFirstShiftPassed )
		{
			nWidth += nBetweenButtonExtent;
			bFirstShiftPassed = true;
		}
		nWidth += nButtonExtent + nBetweenButtonExtent;
	} // if( bShowHelp )

	if( bShowTabList )
	{
		if( ! bFirstShiftPassed )
		{
			nWidth += nBetweenButtonExtent;
			bFirstShiftPassed = true;
		}
		nWidth += nButtonExtent + nBetweenButtonExtent;
	} // if( bShowTabList )

	if( (! bEqualWidth ) && (! bMultiRowColumn ) )
	{
		if( bShowScrollHome )
		{
			if( ! bFirstShiftPassed )
			{
				nWidth += nBetweenButtonExtent;
				bFirstShiftPassed = true;
			}
			nWidth += nButtonExtent + nBetweenButtonExtent;
		} // if( bShowScrollHome )

		if( ! bFirstShiftPassed )
		{
			nWidth += nBetweenButtonExtent;
			bFirstShiftPassed = true;
		}
		nWidth += (nButtonExtent + nBetweenButtonExtent)*2;

		if( bShowScrollEnd )
		{
			if( ! bFirstShiftPassed )
			{
				nWidth += nBetweenButtonExtent;
				bFirstShiftPassed = true;
			}
			nWidth += nButtonExtent + nBetweenButtonExtent;
		} // if( bShowScrollEnd )
	} // if( (! bEqualWidth ) && (! bMultiRowColumn ) )

	return nWidth;
}

bool CExtTabWnd::_RecalcLayoutImpl()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return true;
	if( ! m_bDelayRecalcLayout )
		return true;
	m_bDelayRecalcLayout = false;
	m_rcTabItemsArea.SetRectEmpty();
	m_rcTabNearBorderArea.SetRectEmpty();
	m_nIndexVisFirst = m_nIndexVisLast = -1;
CRect rcClient;
	_RecalcLayout_GetClientRect( rcClient );
	if(		rcClient.IsRectEmpty()
		||	rcClient.right <= rcClient.left
		||	rcClient.bottom <=  rcClient.top
		)
		return true;
	m_rcTabItemsArea = rcClient;
DWORD dwOrientation = OrientationGet();
bool bHorz = OrientationIsHorizontal();
CWindowDC dcMeasure( this );
LONG nTabAreaMetric = 0;
bool bMultiRowColumn = OnTabWndQueryMultiRowColumnLayout();
LONG nItemCount = ItemGetCount();
DWORD dwTabWndStyle = GetTabWndStyle();
bool bEqualWidth = (dwTabWndStyle&__ETWS_EQUAL_WIDTHS) ? true : false;
bool bEnableScrollButtons = ! bEqualWidth;
bool bGrouped = (dwTabWndStyle&__ETWS_GROUPED) ? true : false;
bool bGroupedExpandItems = (dwTabWndStyle&__ETWS_GROUPED_EXPAND_ITEMS) ? true : false;
int nPartExtent = 0;
int nPartCrossExtent = 0;
int nMaxAvailPartExtent = 0;
int nPartDistance = 0;
	if( bMultiRowColumn )
	{
		nPartDistance = OnTabWndQueryMultiRowColumnDistance();
		CSize _sizeButton = OnTabWndCalcButtonSize( dcMeasure, 20 );
		ASSERT( _sizeButton.cx > 0 && _sizeButton.cy > 0 );
		INT nButtonExtent = bHorz ? _sizeButton.cx : _sizeButton.cy;
		INT nBetweenButtonExtent = bHorz ? __EXTTAB_BETWEEN_BTN_GAP_DX : __EXTTAB_BETWEEN_BTN_GAP_DY;
		int nButtonsPreCalcExtent = OnTabWndButtonsCalcWidth( nButtonExtent, nBetweenButtonExtent );
		bEnableScrollButtons = false;
		m_nItemsExtent = m_nScrollMaxPos = 0;
		switch( dwOrientation )
		{
		case __ETWS_ORIENT_TOP:
		case __ETWS_ORIENT_BOTTOM:
			nMaxAvailPartExtent = rcClient.Width();
			m_rcTabItemsArea.bottom = m_rcTabItemsArea.top;
		break;
		case __ETWS_ORIENT_LEFT:
		case __ETWS_ORIENT_RIGHT:
			nMaxAvailPartExtent = rcClient.Height();
			m_rcTabItemsArea.right = m_rcTabItemsArea.left;
		break;
#ifdef _DEBUG
		default:
			ASSERT( FALSE );
		break;
#endif // _DEBUG
		} // switch( dwOrientation )
		nMaxAvailPartExtent -= nButtonsPreCalcExtent;
	} // if( bMultiRowColumn )
	else
	{
		if( bGrouped )
			m_nItemsExtent = m_nScrollMaxPos = 10;
		else
			m_nItemsExtent = m_nScrollMaxPos = 0;
	} // else from if( bMultiRowColumn )
	if( bGrouped )
		bEqualWidth = false;
INT nVisibleNo = 0;
LONG nIndex = 0;
TAB_ITEM_INFO * pTiiLV = NULL;
	for( nIndex = 0; nIndex < nItemCount; )
	{ // compute extent of items
		TAB_ITEM_INFO * pTii = ItemGet( nIndex );
		ASSERT_VALID( pTii );
		pTii->MultiRowWrapSet( false );
		if( bGrouped )
		{
			if( ! pTii->VisibleGet() )
			{
				nIndex++;
				continue;
			}
			if( ! bGroupedExpandItems )
			{
				INT nMaxInGroupExtent = 0;
				INT nNormalGroupItemExtent = 0;
				INT nGroupStartIndex = nIndex;
				INT nCountInGroup = 0;
				for( nCountInGroup = 0; true ; )
				{
					CSize _size = pTii->Measure( &dcMeasure );
					CSize _sizeText = pTii->GetLastMeasuredTextSize();
					CSize _sizeIcon = pTii->GetLastMeasuredIconSize();
					bool bGroupStart = (nVisibleNo == 0) || pTii->GroupStartGet();
					switch( dwOrientation )
					{
					case __ETWS_ORIENT_TOP:
					case __ETWS_ORIENT_BOTTOM:
					{
						nMaxInGroupExtent = max( nMaxInGroupExtent, _size.cx );
						INT nGroupItemExtent =
							_size.cx
							- _sizeText.cx
							- ((_sizeIcon.cx > 0) ? __EXTTAB_MARGIN_ICON2TEXT_X : 0)
							;
						nNormalGroupItemExtent = max( nNormalGroupItemExtent, nGroupItemExtent );
						if( nVisibleNo != 0 && bGroupStart )
							m_nItemsExtent += __EXTTAB_BETWEEN_GROUP_GAP_DX;
						nTabAreaMetric = max( nTabAreaMetric, _size.cy );
					}
					break;
					case __ETWS_ORIENT_LEFT:
					case __ETWS_ORIENT_RIGHT:
					{
						nMaxInGroupExtent = max( nMaxInGroupExtent, _size.cy );
						INT nGroupItemExtent =
							_size.cy
							- _sizeText.cy
							- ((_sizeIcon.cy > 0) ? __EXTTAB_MARGIN_ICON2TEXT_Y : 0)
							;
						nNormalGroupItemExtent = max( nNormalGroupItemExtent, nGroupItemExtent );
						if( nVisibleNo != 0 && bGroupStart )
							m_nItemsExtent += __EXTTAB_BETWEEN_GROUP_GAP_DY;
						nTabAreaMetric = max( nTabAreaMetric, _size.cx );
					}
					break;
				#ifdef _DEBUG
					default:
						ASSERT( FALSE );
					break;
				#endif // _DEBUG
					} // switch( dwOrientation )
					nIndex++;
					nVisibleNo++;
					nCountInGroup++;
					ASSERT( nIndex <= nItemCount );
					if( nIndex == nItemCount )
						break;
					pTii = ItemGet( nIndex );
					ASSERT_VALID( pTii );
					if( pTii->GroupStartGet() )
						break;
				} // for( nCountInGroup = 0; true ; )

				ASSERT( nCountInGroup >= 1 );
				INT nGroupExtent =
					nMaxInGroupExtent
					+ nNormalGroupItemExtent * (nCountInGroup-1);
				m_nItemsExtent += nGroupExtent;

				for( INT n = 0; n < nCountInGroup; n++ )
				{ // reset in group minimal sizes
					pTii = ItemGet( nGroupStartIndex + n );
					ASSERT_VALID( pTii );
					bool bInGroupActive = pTii->InGroupActiveGet();
					switch( dwOrientation )
					{
					case __ETWS_ORIENT_TOP:
					case __ETWS_ORIENT_BOTTOM:
						if( bInGroupActive )
							pTii->m_sizeLastMeasuredItem.cx = nMaxInGroupExtent;
						else
							pTii->m_sizeLastMeasuredItem.cx = nNormalGroupItemExtent;
					break;
					case __ETWS_ORIENT_LEFT:
					case __ETWS_ORIENT_RIGHT:
						if( bInGroupActive )
							pTii->m_sizeLastMeasuredItem.cy = nMaxInGroupExtent;
						else
							pTii->m_sizeLastMeasuredItem.cy = nNormalGroupItemExtent;
					break;
#ifdef _DEBUG
					default:
						ASSERT( FALSE );
					break;
#endif // _DEBUG
					} // switch( dwOrientation )
				} // reset in group minimal sizes
			} // if( ! bGroupedExpandItems )
			else
			{
				CSize _size = pTii->Measure( &dcMeasure );
				bool bGroupStart = (nVisibleNo == 0) ||	pTii->GroupStartGet();
				switch( dwOrientation )
				{
				case __ETWS_ORIENT_TOP:
				case __ETWS_ORIENT_BOTTOM:
					m_nItemsExtent += _size.cx;
					nTabAreaMetric = max( nTabAreaMetric, _size.cy );
					if( nVisibleNo != 0 && bGroupStart )
						m_nItemsExtent += __EXTTAB_BETWEEN_GROUP_GAP_DX;
				break;
				case __ETWS_ORIENT_LEFT:
				case __ETWS_ORIENT_RIGHT:
					m_nItemsExtent += _size.cy;
					nTabAreaMetric = max( nTabAreaMetric, _size.cx );
					if( nVisibleNo != 0 && bGroupStart )
						m_nItemsExtent += __EXTTAB_BETWEEN_GROUP_GAP_DY;
				break;
#ifdef _DEBUG
				default:
					ASSERT( FALSE );
				break;
#endif // _DEBUG
				} // switch( dwOrientation )
				nIndex++;
				nVisibleNo++;
			} // else from if( ! bGroupedExpandItems )
		} // if( bGrouped )
		else
		{
			CSize _size( 0, 0 );
			if( pTii->VisibleGet() )
				_size = pTii->Measure( &dcMeasure );
			switch( dwOrientation )
			{
			case __ETWS_ORIENT_TOP:
			case __ETWS_ORIENT_BOTTOM:
				if( bMultiRowColumn )
				{
					int nTestPartExtent = nPartExtent + _size.cx;
					if( nTestPartExtent > nMaxAvailPartExtent /*|| nIndex == (nItemCount-1)*/ )
					{
						nPartCrossExtent = max( nPartCrossExtent, _size.cy );
						nPartCrossExtent += nPartDistance;
						m_nItemsExtent = max( m_nItemsExtent, nPartCrossExtent );
						m_rcTabItemsArea.bottom += nPartCrossExtent;
						nPartExtent = _size.cx;
						nPartCrossExtent = _size.cy;
						if( pTiiLV != NULL )
							pTiiLV->MultiRowWrapSet();
						else
							pTii->MultiRowWrapSet();
					}
					else
					{
						nPartExtent += _size.cx;
						nPartCrossExtent = max( nPartCrossExtent, _size.cy );
						m_nItemsExtent = max( m_nItemsExtent, nPartCrossExtent );
					}
					if( nIndex == (nItemCount-1) )
					{
						m_nItemsExtent = max( m_nItemsExtent, nPartCrossExtent );
						m_rcTabItemsArea.bottom += nPartCrossExtent;
					}
				} // if( bMultiRowColumn )
				else
				{
					if( ! pTii->VisibleGet() )
					{
						nIndex++;
						continue;
					}
					m_nItemsExtent += _size.cx;
					nTabAreaMetric = max( nTabAreaMetric, _size.cy );
				} // else from if( bMultiRowColumn )
			break;
			case __ETWS_ORIENT_LEFT:
			case __ETWS_ORIENT_RIGHT:
				if( bMultiRowColumn )
				{
					int nTestPartExtent = nPartExtent + _size.cy;
					if( nTestPartExtent > nMaxAvailPartExtent /*|| nIndex == (nItemCount-1)*/ )
					{
						nPartCrossExtent = max( nPartCrossExtent, _size.cx );
						m_nItemsExtent = max( m_nItemsExtent, nPartCrossExtent );
						nPartCrossExtent += nPartDistance;
						m_rcTabItemsArea.right += nPartCrossExtent;
						nPartExtent = _size.cy;
						nPartCrossExtent = _size.cx;
						if( pTiiLV != NULL )
							pTiiLV->MultiRowWrapSet();
						else
							pTii->MultiRowWrapSet();
					}
					else
					{
						nPartExtent += _size.cy;
						nPartCrossExtent = max( nPartCrossExtent, _size.cx );
						m_nItemsExtent = max( m_nItemsExtent, nPartCrossExtent );
					}
					if( nIndex == (nItemCount-1) )
					{
						m_nItemsExtent = max( m_nItemsExtent, nPartCrossExtent );
						m_rcTabItemsArea.right += nPartCrossExtent;
					}
				} // if( bMultiRowColumn )
				else
				{
					if( ! pTii->VisibleGet() )
					{
						nIndex++;
						continue;
					}
					m_nItemsExtent += _size.cy;
					nTabAreaMetric = max( nTabAreaMetric, _size.cx );
				} // else from if( bMultiRowColumn )
			break;
#ifdef _DEBUG
			default:
				ASSERT( FALSE );
			break;
#endif // _DEBUG
			} // switch( dwOrientation )
			nIndex++;
			nVisibleNo++;
		} // else from if( bGrouped )
		if( pTii->VisibleGet() )
			pTiiLV = pTii;
	} // compute extent of items
	PmBridge_GetPM()->TabWnd_AdjustTabAreaMetric( this, dwOrientation, nTabAreaMetric );
LONG nSpaceBefore = 0, nSpaceAfter = 0, nSpaceOver = 0;
	OnTabWndMeasureItemAreaMargins( nSpaceBefore, nSpaceAfter, nSpaceOver );
LONG nAddShiftForBtns = nSpaceAfter;
	// pre-compute button size
CSize _sizeButton = OnTabWndCalcButtonSize( dcMeasure, nTabAreaMetric );
	ASSERT( _sizeButton.cx > 0 && _sizeButton.cy > 0 );
INT nButtonExtent = bHorz ? _sizeButton.cx : _sizeButton.cy;
INT nBetweenButtonExtent = bHorz ? __EXTTAB_BETWEEN_BTN_GAP_DX : __EXTTAB_BETWEEN_BTN_GAP_DY;
	nSpaceAfter += OnTabWndButtonsCalcWidth( nButtonExtent, nBetweenButtonExtent );
LONG nItemRectStartPos = 0;
LONG nItemRectOffs = 0;

	CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID(pPM);

	// compute tab window areas
	switch( dwOrientation )
	{
	case __ETWS_ORIENT_TOP:

		nTabAreaMetric += pPM->UiScalingDo(2, CExtPaintManager::__EUIST_Y);

		nTabAreaMetric = max( nTabAreaMetric, __EXTTAB_MIN_HORZ_HEIGHT );
		if( ! bMultiRowColumn )
		{
			m_rcTabItemsArea.top += nSpaceOver;
			m_rcTabItemsArea.bottom =
				m_rcTabItemsArea.top + nTabAreaMetric;
			m_rcTabItemsArea.bottom =
				min( m_rcTabItemsArea.bottom, rcClient.bottom );
			if( m_rcTabItemsArea.bottom < rcClient.bottom )
			{
				m_rcTabNearBorderArea = rcClient;
				m_rcTabNearBorderArea.top = m_rcTabItemsArea.bottom;
			}
			m_rcTabItemsArea.left += nSpaceBefore;
			m_rcTabItemsArea.right -= nSpaceAfter;
			nItemRectOffs += m_rcTabItemsArea.top;
			m_nScrollMaxPos = m_nItemsExtent - m_rcTabItemsArea.Width();
			if( m_nScrollMaxPos < 0 )
				m_nScrollMaxPos = 0;
		}
		else
		{
			m_rcTabItemsArea.top += nSpaceOver;
			m_rcTabItemsArea.bottom += nSpaceOver;
					m_rcTabItemsArea.left += nSpaceBefore;
					m_rcTabItemsArea.right -= nSpaceAfter;
					nItemRectOffs += nSpaceOver;
			if( m_rcTabItemsArea.bottom < rcClient.bottom )
			{
				m_rcTabNearBorderArea = rcClient;
				m_rcTabNearBorderArea.top = m_rcTabItemsArea.bottom;
			}
		}
	break;
	case __ETWS_ORIENT_BOTTOM:

		nTabAreaMetric += pPM->UiScalingDo(2, CExtPaintManager::__EUIST_Y);

		nTabAreaMetric = max( nTabAreaMetric, __EXTTAB_MIN_HORZ_HEIGHT );
		if( ! bMultiRowColumn )
		{
			m_rcTabItemsArea.bottom -= nSpaceOver;
			m_rcTabItemsArea.top =
				m_rcTabItemsArea.bottom - nTabAreaMetric;
			m_rcTabItemsArea.top =
				max( m_rcTabItemsArea.top, rcClient.top );
			if( m_rcTabItemsArea.top > rcClient.top )
			{
				m_rcTabNearBorderArea = rcClient;
				m_rcTabNearBorderArea.bottom = m_rcTabItemsArea.top;
			}
			m_rcTabItemsArea.left += nSpaceBefore;
			m_rcTabItemsArea.right -= nSpaceAfter;
			nItemRectOffs += m_rcTabItemsArea.top;
			m_nScrollMaxPos = m_nItemsExtent - m_rcTabItemsArea.Width();
			if( m_nScrollMaxPos < 0 )
				m_nScrollMaxPos = 0;
		}
		else
		{
			m_rcTabItemsArea.top += nSpaceOver;
			m_rcTabItemsArea.bottom += nSpaceOver;
					m_rcTabItemsArea.left += nSpaceBefore;
					m_rcTabItemsArea.right -= nSpaceAfter;
					nItemRectOffs += nSpaceOver;
			if( m_rcTabItemsArea.top > rcClient.top )
			{
				m_rcTabNearBorderArea = rcClient;
				m_rcTabNearBorderArea.bottom = m_rcTabItemsArea.top;
			}
		}
	break;
	case __ETWS_ORIENT_LEFT:

		nTabAreaMetric += pPM->UiScalingDo(2, CExtPaintManager::__EUIST_X);

		nTabAreaMetric =
			max( nTabAreaMetric, __EXTTAB_MIN_VERT_WIDTH );
		if( ! bMultiRowColumn )
		{
			m_rcTabItemsArea.left += nSpaceOver;
			m_rcTabItemsArea.right =
				m_rcTabItemsArea.left + nTabAreaMetric;
			m_rcTabItemsArea.right =
				min( m_rcTabItemsArea.right, rcClient.right );
			if( m_rcTabItemsArea.right < rcClient.right )
			{
				m_rcTabNearBorderArea = rcClient;
				m_rcTabNearBorderArea.left = m_rcTabItemsArea.right;
			}
			m_rcTabItemsArea.top += nSpaceBefore;
			m_rcTabItemsArea.bottom -= nSpaceAfter;
			nItemRectOffs += m_rcTabItemsArea.left;
			m_nScrollMaxPos = m_nItemsExtent - m_rcTabItemsArea.Height();
			if( m_nScrollMaxPos < 0 )
				m_nScrollMaxPos = 0;
		}
		else
		{
			m_rcTabItemsArea.left += nSpaceOver;
			m_rcTabItemsArea.right += nSpaceOver;
					m_rcTabItemsArea.top += nSpaceBefore;
					m_rcTabItemsArea.bottom -= nSpaceAfter;
					nItemRectOffs += nSpaceOver;
			if( m_rcTabItemsArea.right < rcClient.right )
			{
				m_rcTabNearBorderArea = rcClient;
				m_rcTabNearBorderArea.left = m_rcTabItemsArea.right;
			}
		}
	break;
	case __ETWS_ORIENT_RIGHT:

		nTabAreaMetric += pPM->UiScalingDo(2, CExtPaintManager::__EUIST_X);

		nTabAreaMetric =
			max( nTabAreaMetric, __EXTTAB_MIN_VERT_WIDTH );
		if( ! bMultiRowColumn )
		{
			m_rcTabItemsArea.right -= nSpaceOver;
			m_rcTabItemsArea.left =
				m_rcTabItemsArea.right - nTabAreaMetric;
			m_rcTabItemsArea.left =
				max( m_rcTabItemsArea.left, rcClient.left );
			if( m_rcTabItemsArea.left > rcClient.left )
			{
				m_rcTabNearBorderArea = rcClient;
				m_rcTabNearBorderArea.right = m_rcTabItemsArea.left;
			}
			m_rcTabItemsArea.top += nSpaceBefore;
			m_rcTabItemsArea.bottom -= nSpaceAfter;
			nItemRectOffs += m_rcTabItemsArea.left;
			m_nScrollMaxPos = m_nItemsExtent - m_rcTabItemsArea.Height();
			if( m_nScrollMaxPos < 0 )
				m_nScrollMaxPos = 0;
		}
		else
		{
			m_rcTabItemsArea.left += nSpaceOver;
			m_rcTabItemsArea.right += nSpaceOver;
					m_rcTabItemsArea.top += nSpaceBefore;
					m_rcTabItemsArea.bottom -= nSpaceAfter;
					nItemRectOffs += nSpaceOver;
			if( m_rcTabItemsArea.left > rcClient.left )
			{
				m_rcTabNearBorderArea = rcClient;
				m_rcTabNearBorderArea.right = m_rcTabItemsArea.left;
			}
		}
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( dwOrientation )
	if( ! bMultiRowColumn )
	{
		ASSERT( m_nScrollPos >= 0 );
		ASSERT( m_nScrollMaxPos >= 0 );
		if( m_nScrollPos > m_nScrollMaxPos )
			m_nScrollPos = m_nScrollMaxPos;
	}
	else
		m_nScrollPos = m_nScrollMaxPos = 0;
LONG nEqualItemExtent = bHorz ? __EXTTAB_MIN_HORZ_WIDTH : __EXTTAB_MIN_VERT_HEIGHT;
bool bCancelEqualWidth = false;
	if( bEqualWidth && m_nVisibleItemCount > 0 )
	{
		if( ! _IsScrollAvail() )
		{
			if( (m_dwTabWndStyle & __ETWS_FULL_WIDTH) == 0 )
				bCancelEqualWidth = true;
		}
		if( ! bCancelEqualWidth )
		{
			INT nTestExtent = bHorz ? m_rcTabItemsArea.Width() : m_rcTabItemsArea.Height();
			nTestExtent /= m_nVisibleItemCount;
			nEqualItemExtent = max( nTestExtent, nEqualItemExtent );
		}
	} // if( bEqualWidth && m_nVisibleItemCount > 0 )

CRect rcButton( m_rcTabItemsArea );
	if( bHorz )
		rcButton.right = rcClient.right - nAddShiftForBtns;
	else
		rcButton.bottom = rcClient.bottom - nAddShiftForBtns;
	rcButton.OffsetRect(
		bHorz ? 0 : (m_rcTabItemsArea.Width() - _sizeButton.cx) / 2,
		bHorz ? (m_rcTabItemsArea.Height() - _sizeButton.cy) / 2 : 0
		);
	rcButton.OffsetRect(
		bHorz ? (-nBetweenButtonExtent) : 0,
		bHorz ? 0 : (-nBetweenButtonExtent)
		);
	if( bHorz )
	{
		rcButton.left = rcButton.right - _sizeButton.cx;
		rcButton.bottom = rcButton.top + _sizeButton.cy;
	}
	else
	{
		rcButton.right = rcButton.left + _sizeButton.cx;
		rcButton.top = rcButton.bottom - _sizeButton.cy;
	}
	OnTabWndButtonsRecalcLayout( rcButton, nButtonExtent, nBetweenButtonExtent );
	m_nIndexVisFirst = -1;
	m_nIndexVisLast = -1;
	nVisibleNo = 0;
	m_bEnableTrackToolTips = false;
	if( bMultiRowColumn )
	{
		int nCurSel = SelectionGet(), nSelRangeStart = -1, nSelRangeEnd = -1;
		if( nCurSel >= 0 )
		{
			TAB_ITEM_INFO * pTii = ItemGet( nCurSel );
			ASSERT_VALID( pTii );
			if( pTii->VisibleGet() )
			{
				nSelRangeStart = nSelRangeEnd = nCurSel;
				for( nIndex = nCurSel; nIndex < nItemCount; nIndex ++ )
				{
					TAB_ITEM_INFO * pTii = ItemGet( nIndex );
					ASSERT_VALID( pTii );
					if( ! pTii->VisibleGet() )
						continue;
					nSelRangeEnd = nIndex;
					bool bMultiRowWrap =
						(	LPVOID(pTii) == LPVOID(pTiiLV)
						||	pTii->MultiRowWrapGet()
						) ? true : false;
					if( bMultiRowWrap )
						break;
				}
				for( nIndex = nCurSel - 1; nIndex >= 0; nIndex -- )
				{
					TAB_ITEM_INFO * pTii = ItemGet( nIndex );
					ASSERT_VALID( pTii );
					if( ! pTii->VisibleGet() )
						continue;
					if( nIndex == 0 )
					{
						nSelRangeStart = nIndex;
						break;
					}
					bool bMultiRowWrap =
						(	( nIndex == 0 )
						||	pTii->MultiRowWrapGet()
						) ? true : false;
					if( bMultiRowWrap )
						break;
					nSelRangeStart = nIndex;
				}
			} // if( pTii->VisibleGet() )
			else
				nCurSel = -1;
		} // if( nCurSel >= 0 )
		bool bTopLeft = OrientationIsTopLeft();
		int nPartStart = 0;
		nPartCrossExtent = 0;
		if( bTopLeft )
		{
			nPartStart = ( nSelRangeEnd >= 0 ) ? ( nSelRangeEnd + 1 ) : 0;
			if( nPartStart >= nItemCount )
			{
				nPartStart = 0;
				nCurSel = nSelRangeStart = nSelRangeEnd = -1;
			}
		}
		else
		{
			nPartStart = ( nSelRangeStart >= 0 ) ? nSelRangeStart : 0;
		}
		int nWalkStart = nPartStart;
		int nWalkEnd = nItemCount - 1;
		int nWalkShift = 1;
		int nPartVisibleCount = 0;
		int nEqualItemExtentSaved = nEqualItemExtent;
		for( nIndex = nWalkStart; nIndex <= nWalkEnd; )
		{ // setup item rectangles (1st part)
			TAB_ITEM_INFO * pTii = ItemGet( nIndex );
			ASSERT_VALID( pTii );
			if( ! pTii->VisibleGet() )
			{
				nIndex += nWalkShift;
				pTii->MultiRowWrapSet( false );
				continue;
			}
			if( m_nIndexVisFirst < 0 || nIndex < m_nIndexVisFirst )
				m_nIndexVisFirst = nIndex;
			if( m_nIndexVisLast < 0 || m_nIndexVisLast < nIndex )
				m_nIndexVisLast = nIndex;
			nVisibleNo++;

			bool bMultiRowWrap =
				(	LPVOID(pTii) == LPVOID(pTiiLV) // ( nIndex == ( nItemCount - 1 ) )
				||	pTii->MultiRowWrapGet()
				) ? true : false;
			if( ! pTii->VisibleGet() )
			{
				bMultiRowWrap = false;
				pTii->MultiRowWrapSet( false );
			}
			if( nIndex == ( nItemCount - 1 ) )
				bMultiRowWrap = true;
			if( ! bMultiRowWrap )
			{
				if( pTii->VisibleGet() )
				{
					nPartVisibleCount ++;
					const CSize & _size = pTii->GetLastMeasuredItemSize();
					switch( dwOrientation )
					{
					case __ETWS_ORIENT_TOP:
					case __ETWS_ORIENT_BOTTOM:
						nPartCrossExtent = max( nPartCrossExtent, _size.cy );
					break;
					case __ETWS_ORIENT_LEFT:
					case __ETWS_ORIENT_RIGHT:
						nPartCrossExtent = max( nPartCrossExtent, _size.cx );
					break;
#ifdef _DEBUG
					default:
						ASSERT( FALSE );
					break;
#endif // _DEBUG
					} // switch( dwOrientation )
				} // if( pTii->VisibleGet() )
				nIndex += nWalkShift;
				continue;
			} // if( ! bMultiRowWrap )
			if( pTii->VisibleGet() )
				nPartVisibleCount ++;
			if( bEqualWidth && (! bCancelEqualWidth ) && nPartVisibleCount > 0 )
			{
				nEqualItemExtent = bHorz ? m_rcTabItemsArea.Width() : m_rcTabItemsArea.Height();
				nEqualItemExtent /= nPartVisibleCount;
			}
			else
				nEqualItemExtent = nEqualItemExtentSaved;
			int nReviewIndex = nPartStart;
			for( ; nReviewIndex <= nIndex; nReviewIndex ++ )
			{
				TAB_ITEM_INFO * pTii = ItemGet( nReviewIndex );
				ASSERT_VALID( pTii );
				if( ! pTii->VisibleGet() )
					continue;

				pTii->m_bHelperToolTipAvail = false;
				if( bEqualWidth && (! bCancelEqualWidth ) )
				{
					if( bHorz )
					{
						if( pTii->m_sizeLastMeasuredItem.cx > nEqualItemExtent )
						{
							m_bEnableTrackToolTips = true;
							pTii->m_bHelperToolTipAvail = true;
						}
						pTii->m_sizeLastMeasuredItem.cx = nEqualItemExtent;
					}
					else
					{
						if( pTii->m_sizeLastMeasuredItem.cy > nEqualItemExtent )
						{
							m_bEnableTrackToolTips = true;
							pTii->m_bHelperToolTipAvail = true;
						}
						pTii->m_sizeLastMeasuredItem.cy = nEqualItemExtent;
					}
				} // if( bEqualWidth && (! bCancelEqualWidth ) )
				const CSize & _size = pTii->GetLastMeasuredItemSize();
				CRect rcItem( 0, 0, _size.cx, _size.cy );
				switch( dwOrientation )
				{
				case __ETWS_ORIENT_TOP:
				case __ETWS_ORIENT_BOTTOM:
					nPartCrossExtent = max( nPartCrossExtent, _size.cy );
					rcItem.OffsetRect( nItemRectStartPos, nItemRectOffs );
					nItemRectStartPos += _size.cx;
				break;
				case __ETWS_ORIENT_LEFT:
				case __ETWS_ORIENT_RIGHT:
					nPartCrossExtent = max( nPartCrossExtent, _size.cx );
					rcItem.OffsetRect( nItemRectOffs, nItemRectStartPos );
					nItemRectStartPos += _size.cy;
				break;
#ifdef _DEBUG
				default:
					ASSERT( FALSE );
				break;
#endif // _DEBUG
				} // switch( dwOrientation )
				pTii->ItemRectSet( rcItem );

				CSize _sizeCloseButton = OnTabWndQueryItemCloseButtonSize( pTii );
				if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )
				{
					CRect rcTabItemCloseButton = rcItem;
					if( bHorz )
					{
						rcTabItemCloseButton.left = rcTabItemCloseButton.right - _sizeCloseButton.cx;
						rcTabItemCloseButton.bottom = rcTabItemCloseButton.top + _sizeCloseButton.cy;
						rcTabItemCloseButton.OffsetRect(
							- __EXTTAB_MARGIN_ICON2TEXT_X,
							( rcItem.Height() - rcTabItemCloseButton.Height() ) / 2
							);
					} // if( bHorz )
					else
					{
						rcTabItemCloseButton.right = rcTabItemCloseButton.left + _sizeCloseButton.cx;
						rcTabItemCloseButton.top = rcTabItemCloseButton.bottom - _sizeCloseButton.cy;
						rcTabItemCloseButton.OffsetRect(
							( rcItem.Width() - rcTabItemCloseButton.Width() ) / 2,
							- __EXTTAB_MARGIN_ICON2TEXT_Y - 2
							);
					} // else from if( bHorz )

					CExtPaintManager * pPM = PmBridge_GetPM();
					ASSERT_VALID( pPM );
					pPM->TabWnd_AdjustItemCloseButtonRect( rcTabItemCloseButton, this );
					pTii->CloseButtonRectSet( rcTabItemCloseButton );
				} // if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )

				nVisibleNo++;
			} // for( ; nReviewIndex <= nIndex; nReviewIndex ++ )
			nIndex += nWalkShift;
			nPartStart = nIndex;
			nItemRectStartPos = 0;
			nPartCrossExtent += nPartDistance;
			nItemRectOffs += nPartCrossExtent;
			nPartCrossExtent = 0;
			nPartVisibleCount = 0;
		} // setup item rectangles (1st part)
		
		if( nSelRangeStart >= 0 )
		{ // setup item rectangles (2nd part)
			ASSERT( nSelRangeStart <= nSelRangeEnd );
			nItemRectStartPos = 0;
			nPartStart = 0;
			nPartVisibleCount = 0;
			if( bTopLeft )
			{
				nWalkStart = nPartStart;
				nWalkEnd = nSelRangeEnd;
			}
			else
			{
				nWalkStart = 0;
				nWalkEnd = nSelRangeStart - 1;
			}
			for( nIndex = nWalkStart; nIndex <= nWalkEnd; )
			{ // setup item rectangles (2nd part)
				TAB_ITEM_INFO * pTii = ItemGet( nIndex );
				ASSERT_VALID( pTii );
				if( ! pTii->VisibleGet() )
				{
					nIndex += nWalkShift;
					pTii->MultiRowWrapSet( false );
					continue;
				}
				if( m_nIndexVisFirst < 0 || nIndex < m_nIndexVisFirst )
					m_nIndexVisFirst = nIndex;
				if( m_nIndexVisLast < 0 || m_nIndexVisLast < nIndex )
					m_nIndexVisLast = nIndex;
				nVisibleNo++;

				bool bMultiRowWrap =
					(	LPVOID(pTii) == LPVOID(pTiiLV) // ( nIndex == ( nItemCount - 1 ) )
					||	pTii->MultiRowWrapGet()
					) ? true : false;
				if( ! pTii->VisibleGet() )
				{
					bMultiRowWrap = false;
					pTii->MultiRowWrapSet( false );
				}
				if( nIndex == ( nItemCount - 1 ) )
					bMultiRowWrap = true;
				if( ! bMultiRowWrap )
				{
					if( pTii->VisibleGet() )
					{
						nPartVisibleCount ++;
						const CSize & _size = pTii->GetLastMeasuredItemSize();
						switch( dwOrientation )
						{
						case __ETWS_ORIENT_TOP:
						case __ETWS_ORIENT_BOTTOM:
							nPartCrossExtent = max( nPartCrossExtent, _size.cy );
						break;
						case __ETWS_ORIENT_LEFT:
						case __ETWS_ORIENT_RIGHT:
							nPartCrossExtent = max( nPartCrossExtent, _size.cx );
						break;
	#ifdef _DEBUG
						default:
							ASSERT( FALSE );
						break;
	#endif // _DEBUG
						} // switch( dwOrientation )
					} // if( pTii->VisibleGet() )
					nIndex += nWalkShift;
					continue;
				} // if( ! bMultiRowWrap )
				if( pTii->VisibleGet() )
					nPartVisibleCount ++;
				if( bEqualWidth && (! bCancelEqualWidth ) && nPartVisibleCount > 0 )
				{
					nEqualItemExtent = bHorz ? m_rcTabItemsArea.Width() : m_rcTabItemsArea.Height();
					nEqualItemExtent /= nPartVisibleCount;
				}
				else
					nEqualItemExtent = nEqualItemExtentSaved;
				int nReviewIndex = nPartStart;
				for( ; nReviewIndex <= nIndex; nReviewIndex ++ )
				{
					TAB_ITEM_INFO * pTii = ItemGet( nReviewIndex );
					ASSERT_VALID( pTii );
					if( ! pTii->VisibleGet() )
						continue;

					pTii->m_bHelperToolTipAvail = false;
					if( bEqualWidth && (! bCancelEqualWidth ) )
					{
						if( bHorz )
						{
							if( pTii->m_sizeLastMeasuredItem.cx > nEqualItemExtent )
							{
								m_bEnableTrackToolTips = true;
								pTii->m_bHelperToolTipAvail = true;
							}
							pTii->m_sizeLastMeasuredItem.cx = nEqualItemExtent;
						}
						else
						{
							if( pTii->m_sizeLastMeasuredItem.cy > nEqualItemExtent )
							{
								m_bEnableTrackToolTips = true;
								pTii->m_bHelperToolTipAvail = true;
							}
							pTii->m_sizeLastMeasuredItem.cy = nEqualItemExtent;
						}
					} // if( bEqualWidth && (! bCancelEqualWidth ) )

					const CSize & _size = pTii->GetLastMeasuredItemSize();
					CRect rcItem( 0, 0, _size.cx, _size.cy );
					switch( dwOrientation )
					{
					case __ETWS_ORIENT_TOP:
					case __ETWS_ORIENT_BOTTOM:
						nPartCrossExtent = max( nPartCrossExtent, _size.cy );
						rcItem.OffsetRect( nItemRectStartPos, nItemRectOffs );
						nItemRectStartPos += _size.cx;
					break;
					case __ETWS_ORIENT_LEFT:
					case __ETWS_ORIENT_RIGHT:
						nPartCrossExtent = max( nPartCrossExtent, _size.cx );
						rcItem.OffsetRect( nItemRectOffs, nItemRectStartPos );
						nItemRectStartPos += _size.cy;
					break;
#ifdef _DEBUG
					default:
						ASSERT( FALSE );
					break;
#endif // _DEBUG
					} // switch( dwOrientation )
					pTii->ItemRectSet( rcItem );

					CSize _sizeCloseButton = OnTabWndQueryItemCloseButtonSize( pTii );
					if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )
					{
						CRect rcTabItemCloseButton = rcItem;
						if( bHorz )
						{
							rcTabItemCloseButton.left = rcTabItemCloseButton.right - _sizeCloseButton.cx;
							rcTabItemCloseButton.bottom = rcTabItemCloseButton.top + _sizeCloseButton.cy;
							rcTabItemCloseButton.OffsetRect(
								- __EXTTAB_MARGIN_ICON2TEXT_X,
								( rcItem.Height() - rcTabItemCloseButton.Height() ) / 2
								);
						} // if( bHorz )
						else
						{
							rcTabItemCloseButton.right = rcTabItemCloseButton.left + _sizeCloseButton.cx;
							rcTabItemCloseButton.top = rcTabItemCloseButton.bottom - _sizeCloseButton.cy;
							rcTabItemCloseButton.OffsetRect(
								( rcItem.Width() - rcTabItemCloseButton.Width() ) / 2,
								- __EXTTAB_MARGIN_ICON2TEXT_Y - 2
								);
						} // else from if( bHorz )

						CExtPaintManager * pPM = PmBridge_GetPM();
						ASSERT_VALID( pPM );
						pPM->TabWnd_AdjustItemCloseButtonRect( rcTabItemCloseButton, this );
						pTii->CloseButtonRectSet( rcTabItemCloseButton );
					} // if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )

					nVisibleNo++;
				} // for( ; nReviewIndex <= nIndex; nReviewIndex ++ )
				nIndex += nWalkShift;
				nPartStart = nIndex;
				nItemRectStartPos = 0;
				nPartCrossExtent += nPartDistance;
				nItemRectOffs += nPartCrossExtent;
				nPartCrossExtent = 0;
				nPartVisibleCount = 0;
			} // setup item rectangles (2nd part)
		} // setup item rectangles (2nd part)
	} // if( bMultiRowColumn )
	else
	{
		for( nIndex = 0; nIndex < nItemCount; nIndex++ )
		{ // setup item rectangles
			TAB_ITEM_INFO * pTii = ItemGet( nIndex );
			ASSERT_VALID( pTii );
			if( ! pTii->VisibleGet() )
				continue;
			pTii->m_bHelperToolTipAvail = false;
			if( (! bMultiRowColumn ) && bEqualWidth && (! bCancelEqualWidth ) )
			{
				if( bHorz )
				{
					if( pTii->m_sizeLastMeasuredItem.cx > nEqualItemExtent )
					{
						m_bEnableTrackToolTips = true;
						pTii->m_bHelperToolTipAvail = true;
					}
					pTii->m_sizeLastMeasuredItem.cx = nEqualItemExtent;
				}
				else
				{
					if( pTii->m_sizeLastMeasuredItem.cy > nEqualItemExtent )
					{
						m_bEnableTrackToolTips = true;
						pTii->m_bHelperToolTipAvail = true;
					}
					pTii->m_sizeLastMeasuredItem.cy = nEqualItemExtent;
				}
			}
			
			const CSize & _size = pTii->GetLastMeasuredItemSize();
			CRect rcItem( 0, 0, _size.cx, _size.cy );

			bool bGroupStart = bGrouped && pTii->GroupStartGet();
			
			switch( dwOrientation )
			{
			case __ETWS_ORIENT_TOP:
			case __ETWS_ORIENT_BOTTOM:
				if(		nVisibleNo != 0
					&&	bGroupStart
					)
					nItemRectStartPos += __EXTTAB_BETWEEN_GROUP_GAP_DX;
				rcItem.OffsetRect( nItemRectStartPos, nItemRectOffs );
				nItemRectStartPos += _size.cx;
			break;
			case __ETWS_ORIENT_LEFT:
			case __ETWS_ORIENT_RIGHT:
				if(		nVisibleNo != 0
					&&	bGroupStart
					)
					nItemRectStartPos += __EXTTAB_BETWEEN_GROUP_GAP_DY;
				rcItem.OffsetRect( nItemRectOffs, nItemRectStartPos );
				nItemRectStartPos += _size.cy;
			break;
#ifdef _DEBUG
			default:
				ASSERT( FALSE );
			break;
#endif // _DEBUG
			} // switch( dwOrientation )

			pTii->ItemRectSet( rcItem );
			rcItem.OffsetRect(
				bHorz ? ( m_rcTabItemsArea.left - m_nScrollPos ) : 0,
				bHorz ? 0 : (m_rcTabItemsArea.top - m_nScrollPos)
				);
			CSize _sizeCloseButton = OnTabWndQueryItemCloseButtonSize( pTii );
			if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )
			{
				CRect rcTabItemCloseButton = rcItem;
				if( bHorz )
				{
					rcTabItemCloseButton.left = rcTabItemCloseButton.right - _sizeCloseButton.cx;
					rcTabItemCloseButton.bottom = rcTabItemCloseButton.top + _sizeCloseButton.cy;
					rcTabItemCloseButton.OffsetRect(
						- __EXTTAB_MARGIN_ICON2TEXT_X,
						( rcItem.Height() - rcTabItemCloseButton.Height() ) / 2
						);
				} // if( bHorz )
				else
				{
					rcTabItemCloseButton.right = rcTabItemCloseButton.left + _sizeCloseButton.cx;
					rcTabItemCloseButton.top = rcTabItemCloseButton.bottom - _sizeCloseButton.cy;
					rcTabItemCloseButton.OffsetRect(
						( rcItem.Width() - rcTabItemCloseButton.Width() ) / 2,
						- __EXTTAB_MARGIN_ICON2TEXT_Y - 2
						);
				} // else from if( bHorz )

				CExtPaintManager * pPM = PmBridge_GetPM();
				ASSERT_VALID( pPM );
				pPM->TabWnd_AdjustItemCloseButtonRect( rcTabItemCloseButton, this );
				pTii->CloseButtonRectSet( rcTabItemCloseButton );
			} // if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )
			INT nItemExtentMinVal = bHorz ? rcItem.left : rcItem.top;
			INT nItemExtentMaxVal = bHorz ? rcItem.right : rcItem.bottom;
			INT nAreaExtentMinVal = 0;
			INT nAreaExtentMaxVal =
				bHorz
					? (m_rcTabItemsArea.right - m_rcTabItemsArea.left)
					: (m_rcTabItemsArea.bottom - m_rcTabItemsArea.top)
					;
			nAreaExtentMaxVal += __EXTTAB_ADD_END_SCROLL_SPACE + 2;
			if( m_nIndexVisFirst < 0 )
			{
				if( nItemExtentMaxVal >= nAreaExtentMinVal )
					m_nIndexVisFirst = nIndex;
			}
			if( nItemExtentMinVal <= nAreaExtentMaxVal )
				m_nIndexVisLast = nIndex;

			nVisibleNo++;
		} // setup item rectangles
	} // else from if( bMultiRowColumn )
	if( nVisibleNo != m_nVisibleItemCount )
		return false;
	if( m_nIndexVisFirst < 0 || m_nIndexVisLast < 0 )
	{
		m_nIndexVisFirst = m_nIndexVisLast = -1;
		bEnableScrollButtons = false;
	}
	else
	{
		if( m_nIndexVisFirst > m_nIndexVisLast )
			m_nIndexVisLast = m_nIndexVisFirst;
		ASSERT( m_nIndexVisFirst <= m_nIndexVisLast );
		ASSERT( 0 <= m_nIndexVisFirst && m_nIndexVisFirst < nItemCount );
		ASSERT( 0 <= m_nIndexVisLast && m_nIndexVisLast < nItemCount );
	}
	ASSERT_VALID( this );
	return true;
}

void CExtTabWnd::_RecalcLayout_GetClientRect(
	CRect & rcClient
	)
{
	ASSERT_VALID( this );
	if( ! m_rcRecalcLayout.IsRectEmpty() )
		rcClient = m_rcRecalcLayout;
	else
	{
		ASSERT( GetSafeHwnd() != NULL );
		GetClientRect( &rcClient );
	}
}

void CExtTabWnd::DoPaint( 
	CDC * pDC,
	CRect & rcClient
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC );

CRuntimeClass * pPaintManagerRTC =
		PmBridge_GetPM()->GetRuntimeClass();
	if( m_pHelperLastPaintManagerRTC != pPaintManagerRTC )
	{
		m_pHelperLastPaintManagerRTC = pPaintManagerRTC;
		m_bDelayRecalcLayout = true;
	}

	// recalc delayed layout first
	_RecalcLayoutImpl();

CExtMemoryDC dc(
		pDC,
		&rcClient
		);
	OnTabWndDrawEntire( dc, rcClient );

	PmBridge_GetPM()->OnPaintSessionComplete( this );
}

/////////////////////////////////////////////////////////////////////////////
// CExtTabWnd message handlers

void CExtTabWnd::OnPaint() 
{
	ASSERT_VALID( this );
CPaintDC dcPaint( this );
CRect rcClient;
	GetClientRect( &rcClient );
	if( rcClient.IsRectEmpty() )
		return;
	DoPaint( &dcPaint, rcClient );
}


void CExtTabWnd::OnTabWndMeasureItemAreaMargins(
	LONG & nSpaceBefore,
	LONG & nSpaceAfter,
	LONG & nSpaceOver
	)
{
	ASSERT_VALID( this );
	PmBridge_GetPM()->TabWnd_MeasureItemAreaMargins(
		this,
		nSpaceBefore,
		nSpaceAfter,
		nSpaceOver
		);
}

void CExtTabWnd::_DrawItem(
	CDC & dc,
	LONG nItemIndex
	)
{
TAB_ITEM_INFO * pTii = ItemGet( nItemIndex );
	ASSERT_VALID( pTii );
	if( ! pTii->VisibleGet() )
		return;
bool bHorz = OrientationIsHorizontal();
bool bTopLeft = OrientationIsTopLeft();
bool bCenteredText = (GetTabWndStyle() & __ETWS_CENTERED_TEXT) ? true : false;
bool bInvertedVerticalMode = (GetTabWndStyle() & __ETWS_INVERT_VERT_FONT) ? true : false;
bool bGroupedMode = (GetTabWndStyle() & __ETWS_GROUPED) ? true : false;

CRect rcItem = pTii->ItemRectGet();
	rcItem.OffsetRect(
		bHorz
			? (m_rcTabItemsArea.left - m_nScrollPos)
			: 0
		,
		bHorz
			? 0
			: (m_rcTabItemsArea.top - m_nScrollPos)
		);

	if( (! dc.RectVisible( &rcItem ) ) )
	{
//		if( CExtControlBar::_DraggingGetBar() != NULL )
//			SetTimer(
//				__EXTTAB_FLOATING_BAR_TIMER_ID,
//				__EXTTAB_FLOATING_BAR_TIMER_PERIOD,
//				NULL
//				);
		return;
	}

CSize _sizeTextMeasured = pTii->GetLastMeasuredTextSize();
bool bSelected = pTii->SelectedGet();
CExtSafeString sText = pTii->TextGet();
CExtCmdIcon * pIcon = pTii->IconGetPtr();
bool bInGroupActive = pTii->InGroupActiveGet();
CFont font;
	_GetTabWndFont( &font, bSelected );
	OnTabWndDrawItem(
		dc,
		m_rcTabItemsArea,
		nItemIndex,
		pTii,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		rcItem,
		_sizeTextMeasured,
		&font,
		sText,
		pIcon
		);
}

void CExtTabWnd::OnTabWndDrawEntire(
	CDC & dc,
	CRect & rcClient
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
bool bGroupedMode = (GetTabWndStyle() & __ETWS_GROUPED) ? true : false;
	OnTabWndEraseClientArea( dc, rcClient, m_rcTabItemsArea, m_rcTabNearBorderArea, OrientationGet(), bGroupedMode );
LONG nItemCount = ItemGetCount();
LONG nVisCount = ItemGetVisibleCount();
	if( nItemCount > 0 && nVisCount > 0 && m_nIndexVisFirst >= 0 )
	{
		if( m_nIndexVisFirst < 0 )
			m_nIndexVisFirst = nItemCount - 1;
		if( m_nIndexVisLast < 0 )
			m_nIndexVisLast = nItemCount - 1;
		ASSERT( m_nIndexVisFirst <= m_nIndexVisLast );
		ASSERT( 0 <= m_nIndexVisFirst && m_nIndexVisFirst < nItemCount );
		ASSERT( 0 <= m_nIndexVisLast && m_nIndexVisLast < nItemCount );

		bool bHorz = OrientationIsHorizontal();
		bool bTopLeft = OrientationIsTopLeft();

		CRect rcSetMargins(
			( (!bHorz) && (!bTopLeft) ) ? 1 : 0,
			(   bHorz  && (!bTopLeft) ) ? 1 : 0,
			( (!bHorz) &&   bTopLeft  ) ? 1 : 0,
			(   bHorz  &&   bTopLeft  ) ? 1 : 0
			);
		CRect rcPaintItems( m_rcTabItemsArea );
		rcPaintItems.InflateRect(
			bHorz ? 1 : 0,
			bHorz ? 0 : 1
			);
		if( bGroupedMode )
			rcPaintItems.InflateRect(
				bHorz ? 0 : 1,
				bHorz ? 1 : 0
				);
		rcPaintItems.InflateRect(
			rcSetMargins.left,
			rcSetMargins.top,
			rcSetMargins.right,
			rcSetMargins.bottom
			);
		CRgn rgnPaint;
		if( !rgnPaint.CreateRectRgnIndirect(&rcPaintItems) )
		{
			ASSERT( FALSE );
			return;
		}
		dc.SelectClipRgn( &rgnPaint );

		LONG nIndexSelected = -1L;
		for( LONG nIndex = m_nIndexVisFirst; nIndex <= m_nIndexVisLast; nIndex++ )
		{ 
			if( SelectionGet() != nIndex )
			{
				_DrawItem(
					dc,
					nIndex
					);
				dc.SelectClipRgn( &rgnPaint );
			}
			else
				nIndexSelected = nIndex;
		}

		if( nIndexSelected >= 0L )
			_DrawItem(
				dc,
				nIndexSelected
				);

		dc.SelectClipRgn( NULL );
	} // if( nItemCount > 0 && nVisCount > 0 && m_nIndexVisFirst >= 0 )

//bool bAnyButtonTracked =
//			m_nPushedTrackingButton == __ETWH_BUTTON_LEFTUP
//		||	m_nPushedTrackingButton == __ETWH_BUTTON_RIGHTDOWN
//		||	m_nPushedTrackingButton == __ETWH_BUTTON_HELP
//		||	m_nPushedTrackingButton == __ETWH_BUTTON_CLOSE
//		;
//bool bAnyMouseButtonPressed = false;
//	if( bAnyButtonTracked )
//		bAnyMouseButtonPressed =
//			CExtPopupMenuWnd::IsKeyPressed(VK_LBUTTON)
//		||	CExtPopupMenuWnd::IsKeyPressed(VK_MBUTTON)
//		||	CExtPopupMenuWnd::IsKeyPressed(VK_RBUTTON)
//		;

CPoint ptCursor( 0, 0 );
	::GetCursorPos( &ptCursor );
	ScreenToClient( &ptCursor );

bool bTopLeft = OrientationIsTopLeft();
bool bHorz = OrientationIsHorizontal();

	if(		(! m_rcBtnUp.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnUp )
		)
	{
//		bool bEnabled =
//			_IsScrollAvail() && m_nScrollPos > 0;
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_UP) ? true : false;
		bool bHover = bEnabled && m_rcBtnUp.PtInRect(ptCursor) ? true : false;
		//bool bPushed = bEnabled && bHover && bAnyButtonTracked;
		OnTabWndDrawButton(
			dc,
			m_rcBtnUp,
			__ETWH_BUTTON_LEFTUP,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedUp,
			bGroupedMode
			);
	}
	if(		(! m_rcBtnDown.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnDown )
		)
	{
//		bool bEnabled =
//			_IsScrollAvail() && m_nScrollPos < m_nScrollMaxPos;
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_DOWN) ? true : false;
		bool bHover = bEnabled && m_rcBtnDown.PtInRect(ptCursor) ? true : false;
		//bool bPushed = bEnabled && bHover && bAnyButtonTracked;
		OnTabWndDrawButton(
			dc,
			m_rcBtnDown,
			__ETWH_BUTTON_RIGHTDOWN,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedDown,
			bGroupedMode
			);
	}
	if(		(! m_rcBtnHelp.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnHelp )
		)
	{
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_HELP) ? true : false;
		bool bHover = bEnabled && m_rcBtnHelp.PtInRect(ptCursor) ? true : false;
		//bool bPushed = bEnabled && bHover && bAnyButtonTracked;
		OnTabWndDrawButton(
			dc,
			m_rcBtnHelp,
			__ETWH_BUTTON_HELP,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedHelp,
			bGroupedMode
			);
	}
	if(		(! m_rcBtnClose.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnClose )
		)
	{
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_CLOSE) ? true : false;
		bool bHover = bEnabled && m_rcBtnClose.PtInRect(ptCursor) ? true : false;
		//bool bPushed = bEnabled && bHover && bAnyButtonTracked;
		OnTabWndDrawButton(
			dc,
			m_rcBtnClose,
			__ETWH_BUTTON_CLOSE,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedClose,
			bGroupedMode
			);
	}
	if(		(! m_rcBtnTabList.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnTabList )
		)
	{
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_TAB_LIST) ? true : false;
		bEnabled = (bEnabled && ItemGetCount() > 0 );
		bool bHover = bEnabled && m_rcBtnTabList.PtInRect(ptCursor) ? true : false;
		OnTabWndDrawButton(
			dc,
			m_rcBtnTabList,
			__ETWH_BUTTON_TAB_LIST,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedTabList,
			bGroupedMode
			);
	}
	if(		(! m_rcBtnScrollHome.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnScrollHome )
		)
	{
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_SCROLL_HOME) ? true : false;
		bool bHover = bEnabled && m_rcBtnScrollHome.PtInRect(ptCursor) ? true : false;
		OnTabWndDrawButton(
			dc,
			m_rcBtnScrollHome,
			__ETWH_BUTTON_SCROLL_HOME,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedScrollHome,
			bGroupedMode
			);
	}
	if(		(! m_rcBtnScrollEnd.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnScrollEnd )
		)
	{
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_SCROLL_END) ? true : false;
		bool bHover = bEnabled && m_rcBtnScrollEnd.PtInRect(ptCursor) ? true : false;
		OnTabWndDrawButton(
			dc,
			m_rcBtnScrollEnd,
			__ETWH_BUTTON_SCROLL_END,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedScrollEnd,
			bGroupedMode
			);
	}
}

void CExtTabWnd::OnTabWndDrawButton(
	CDC & dc,
	CRect & rcButton,
	LONG nHitTest,
	bool bTopLeft,
	bool bHorz,
	bool bEnabled,
	bool bHover,
	bool bPushed,
	bool bGroupedMode
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );

	PmBridge_GetPM()->PaintTabButton(
		dc,
		rcButton,
		nHitTest,
		bTopLeft,
		bHorz,
		bEnabled,
		bHover,
		bPushed,
		bGroupedMode,
		this
		);
}

void CExtTabWnd::OnTabWndEraseClientArea(
	CDC & dc,
	CRect & rcClient,
	CRect & rcTabItemsArea,
	CRect & rcTabNearBorderArea,
	DWORD dwOrientation,
	bool bGroupedMode
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	PmBridge_GetPM()->PaintTabClientArea(
		dc,
		rcClient,
		rcTabItemsArea,
		rcTabNearBorderArea,
		dwOrientation,
		bGroupedMode,
		this
		);
}

void CExtTabWnd::OnTabWndDrawItem(
	CDC & dc,
	CRect & rcTabItemsArea,
	LONG nItemIndex,
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( pFont != NULL );
	ASSERT( pFont->GetSafeHandle() != NULL );

//bool bEnabled = pTii->EnabledGet();
//bool bHover = ( (!_IsDND()) &&  m_nHoverTrackingHitTest == nItemIndex ) ? true : false;
//	bEnabled;
// 	bHover;

	if( (pTii->GetItemStyle() & __ETWI_CENTERED_TEXT) != 0 )
		bCenteredText = true;

	PmBridge_GetPM()->PaintTabItem(
		dc,
		rcTabItemsArea,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		rcEntireItem,
		sizeTextMeasured,
		pFont,
		sText,
		pIcon,
		this,
		nItemIndex
		);
}

bool CExtTabWnd::OnTabWndMouseTrackingPushedStart(
	INT nMouseButton, // MK_... values
	LONG nHitTest
	)
{
	ASSERT_VALID( this );

	if(		m_nPushedTrackingHitTest != __ETWH_NOWHERE
		&&	m_nPushedTrackingButton != (-1L)
		)
	{
		if(		m_nPushedTrackingHitTest != nMouseButton
			||	m_nPushedTrackingButton != nHitTest
			)
			return false;
	}

	m_bPushedUp = false;
	m_bPushedDown = false;
	m_bPushedScrollHome = false;
	m_bPushedScrollEnd = false;
	m_bPushedHelp = false;
	m_bPushedClose = false;
	m_bPushedTabList = false;

	switch( nHitTest )
	{
	case __ETWH_BUTTON_LEFTUP:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if( (GetTabWndStyle() & __ETWS_ENABLED_BTN_UP) == 0 )
			return false;
		m_bPushedUp = true;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_RIGHTDOWN:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if( (GetTabWndStyle() & __ETWS_ENABLED_BTN_DOWN) == 0 )
			return false;
		m_bPushedDown = true;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_SCROLL_HOME:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if( (GetTabWndStyle() & __ETWS_ENABLED_BTN_SCROLL_HOME) == 0 )
			return false;
		m_bPushedScrollHome = true;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_SCROLL_END:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if( (GetTabWndStyle() & __ETWS_ENABLED_BTN_SCROLL_END) == 0 )
			return false;
		m_bPushedScrollEnd = true;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_HELP:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if( (GetTabWndStyle() & __ETWS_ENABLED_BTN_HELP) == 0 )
			return false;
		m_bPushedHelp = true;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_CLOSE:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if( (GetTabWndStyle() & __ETWS_ENABLED_BTN_CLOSE) == 0 )
			return false;
		m_bPushedClose = true;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_TAB_LIST:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if(		(GetTabWndStyle() & __ETWS_ENABLED_BTN_TAB_LIST) == 0 
			|| 	(ItemGetCount() <= 0)
			)
			return false;
		m_bPushedTabList = true;
		UpdateTabWnd( true );
		break;
	} // switch( nHitTest )

	m_nPushedTrackingHitTest = nMouseButton;
	m_nPushedTrackingButton = nHitTest;
	m_bTrackingButtonPushed = true;
	m_bPushedTrackingCloseButton = false;
	if( m_nPushedTrackingButton >= 0 )
	{
		CPoint ptCursor;
		if( GetCursorPos( &ptCursor ) )
		{
			ScreenToClient( &ptCursor );
			TAB_ITEM_INFO * pTII = ItemGet( m_nPushedTrackingButton );
			if( pTII->CloseButtonRectGet().PtInRect( ptCursor ) )
			{
				if(		(GetTabWndStyleEx()&__ETWS_EX_CLOSE_ON_SELECTED_ONLY) == 0
					||	SelectionGet() == m_nPushedTrackingButton
					)
					m_bPushedTrackingCloseButton = true;
			}
		} // if( GetCursorPos( &ptCursor ) )
	} // if( m_nPushedTrackingButton >= 0 )

	if( CExtMouseCaptureSink::GetCapture() != m_hWnd )
		CExtMouseCaptureSink::SetCapture( m_hWnd );

	return true;
}

void CExtTabWnd::OnTabWndMouseTrackingPushedStop(
	bool bEnableReleaseCapture // = true
	)
{
	ASSERT_VALID( this );

	if( (GetTabWndStyle() & __ETWS_SCROLL_BY_PAGE) == 0
		&&	m_nScrollDirectionRest != 0
		)
		SendMessage( WM_CANCELMODE );

	m_ptStartDrag.x = m_ptStartDrag.y = -1;
	switch( m_nPushedTrackingButton )
	{
	case __ETWH_BUTTON_LEFTUP:
		m_bPushedUp = false;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_RIGHTDOWN:
		m_bPushedDown = false;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_SCROLL_HOME:
		m_bPushedScrollHome = false;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_SCROLL_END:
		m_bPushedScrollEnd = false;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_HELP:
		m_bPushedHelp = false;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_CLOSE:
		m_bPushedClose = false;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_TAB_LIST:
		m_bPushedTabList = false;
		UpdateTabWnd( true );
		break;
	} // switch( m_nPushedTrackingButton )

	m_nPushedTrackingHitTest = __ETWH_NOWHERE;
	m_bPushedTrackingCloseButton = false;
	m_nPushedTrackingButton = (-1L);
	m_bTrackingButtonPushed = false;
	if( bEnableReleaseCapture && CExtMouseCaptureSink::GetCapture() == GetSafeHwnd() )
		CExtMouseCaptureSink::ReleaseCapture();
}

bool CExtTabWnd::OnTabWndStartDrag( LONG nIndex )
{
	ASSERT_VALID( this );
	ASSERT( ItemGetCount() > 0 );
	ASSERT(
		nIndex >= 0
		&& nIndex < ItemGetCount()
		);

	if(		nIndex >= 0
		&&	nIndex < ItemGetCount()
		&&	(GetTabWndStyle()&__ETWS_ITEM_DRAGGING) != 0
		&&	(GetTabWndStyle()&__ETWS_GROUPED) == 0
		)
	{
#if (!defined __EXT_MFC_NO_DYNAMIC_BAR_SITE)
#if (!defined __EXT_MFC_NO_TABMDI_CTRL)
		HWND hWnd = (HWND) ItemLParamGet( nIndex );
		if(		hWnd != NULL
			&&	::IsWindow( hWnd )
			)
		{
			CWnd * pWnd = CWnd::FromHandlePermanent( hWnd );
			if(		pWnd != NULL
				&&	pWnd->IsKindOf( RUNTIME_CLASS( CExtDynamicMDIChildWnd ) )
				)
			{
				hWnd = ::GetWindow( hWnd, GW_CHILD );
				if( hWnd != NULL )
				{
					CExtDynamicBarSite * pDBS = CExtDynamicBarSite::FindBarSite( pWnd );
					if( pDBS != NULL )
					{
						CExtDynamicControlBar * pBar = pDBS->BarFindByChildHWND( hWnd );
						if( pBar != NULL )
						{
							ASSERT_VALID( pBar );
							if( pDBS->OnStartDraggingDocumentTabItem( pBar, this, nIndex ) )
								return false;
						}
					}
				}
			}
			else
			{
					CExtDynamicBarSite * pDBS = CExtDynamicBarSite::FindBarSite( hWnd );
					if( pDBS != NULL )
					{
						CExtDynamicControlBar * pBar = pDBS->BarFindByChildHWND( hWnd );
						if( pBar != NULL )
						{
							ASSERT_VALID( pBar );
							if( pDBS->OnStartDraggingDocumentTabItem( pBar, this, nIndex ) )
								return false;
						}
					}
			}
		}
#endif // (!defined __EXT_MFC_NO_TABMDI_CTRL)
#endif // (!defined __EXT_MFC_NO_DYNAMIC_BAR_SITE)
		return true;
	}
	
	return false;
}

bool CExtTabWnd::OnTabWndClickedButton(
	LONG nHitTest,
	bool bButtonPressed,
	INT nMouseButton, // MK_... values
	UINT nMouseEventFlags
	)
{
	ASSERT_VALID( this );
	nMouseEventFlags;

	if( bButtonPressed )
	{
		if( OnTabWndMouseTrackingPushedStart( nMouseButton, nHitTest ) )
		{
			Invalidate();
			UpdateWindow();
			if(		nHitTest == __ETWH_BUTTON_LEFTUP
				||	nHitTest == __ETWH_BUTTON_RIGHTDOWN
				)
			{
				OnTabWndDoScroll(
					(nHitTest == __ETWH_BUTTON_LEFTUP) ? -1 : +1,
					true
					);
			}
		}
	}
	else
	{
		OnTabWndMouseTrackingPushedStop();

		if(		nMouseButton == MK_LBUTTON 
			&&	(ItemGetCount() > 0)
			&&	(
					(	nHitTest == __ETWH_BUTTON_SCROLL_HOME
					&&	(GetTabWndStyle() & __ETWS_ENABLED_BTN_SCROLL_HOME) != 0
					)
				||	(	nHitTest == __ETWH_BUTTON_SCROLL_END
					&&	(GetTabWndStyle() & __ETWS_ENABLED_BTN_SCROLL_END) != 0
					)
				)
			)
		{
			LONG nItem = 
				nHitTest == __ETWH_BUTTON_SCROLL_HOME
					? 0
					: ItemGetCount() - 1;
			ASSERT( nItem >= 0 && nItem < ItemGetCount() );
			ItemEnsureVisible( nItem, true );
		}
		
		if(		nMouseButton == MK_LBUTTON 
			&&	nHitTest == __ETWH_BUTTON_TAB_LIST
			&&	(GetTabWndStyle() & __ETWS_ENABLED_BTN_TAB_LIST) != 0
			&&	(ItemGetCount() > 0)
			)
		{
			// show tab list popup menu
			CExtPopupMenuWnd * pPopup =
				CExtPopupMenuWnd::InstantiatePopupMenu(
					GetSafeHwnd(),
					RUNTIME_CLASS(CExtPopupMenuWnd),
					this
					);
			if( pPopup->CreatePopupMenu( GetSafeHwnd() ) )
			{
				UINT nStartID = 0xffff;
				for( LONG nIndex = 0; nIndex < m_arrItems.GetSize(); nIndex++ )
				{
					TAB_ITEM_INFO * pTii = ItemGet( nIndex );
					if( pTii != NULL ) 
					{
						bool bGrouped = (GetTabWndStyle()&__ETWS_GROUPED) ? true : false;
						if(		bGrouped 
							&&	nIndex != 0 
							)
						{
							LONG nGroupStart;
							LONG nGroupEnd;
							ItemGetGroupRange(
								nIndex,
								nGroupStart,
								nGroupEnd
								);
							if( nGroupStart == nIndex )
								VERIFY( pPopup->ItemInsertCommand( CExtPopupMenuWnd::TYPE_SEPARATOR, -1 ) );
						}

						if( !pTii->VisibleGet() )
							continue;

						CExtCmdIcon * pCmdIcon = OnTabWndQueryItemIcon( pTii );
						static CExtCmdIcon g_EmptyIcon;
						CExtCmdIcon & _icon = (pCmdIcon != NULL) ? (*pCmdIcon) : g_EmptyIcon;
						VERIFY( 
							pPopup->ItemInsertCommand(
								nStartID + nIndex,
								-1,
								pTii->TextGet(),
								NULL,
								_icon,
								( SelectionGet() == nIndex )
								) 
							);
						
						INT nItemsCount = pPopup->ItemGetCount();
						pPopup->ItemEnabledSet( 
							nItemsCount - 1, 
							pTii->EnabledGet()
							);
					}	
				}
				CPoint ptTrack( m_rcBtnTabList.left, m_rcBtnTabList.bottom );
				ClientToScreen( &ptTrack );
				CRect rcExcludeArea( m_rcBtnTabList );
				ClientToScreen( &rcExcludeArea );
				
				DWORD nAlign = TPMX_LEFTALIGN; 
				switch( __ETWS_ORIENT_MASK & OrientationGet() ) 
				{
				case __ETWS_ORIENT_TOP:
					nAlign = TPMX_TOPALIGN;
					break;
				case __ETWS_ORIENT_BOTTOM:
					nAlign = TPMX_BOTTOMALIGN;
					break;
				case __ETWS_ORIENT_LEFT:
					nAlign = TPMX_LEFTALIGN;
					break;
				case __ETWS_ORIENT_RIGHT:
					nAlign = TPMX_RIGHTALIGN;
					break;
				}

				UINT nCmdRetVal = 0;
				HWND hWndOwn = m_hWnd;
				if(	pPopup->TrackPopupMenu(
						TPMX_OWNERDRAW_FIXED
							| nAlign
							| TPMX_COMBINE_DEFAULT
							| TPMX_DO_MESSAGE_LOOP
							| TPMX_NO_WM_COMMAND
							| TPMX_NO_CMD_UI
							| TPMX_NO_HIDE_RARELY,
						ptTrack.x, 
						ptTrack.y,
						&rcExcludeArea,
						pPopup, // NULL,
						_CbPaintCombinedTabListBtnContent,
						&nCmdRetVal,
						true
						)
					)
				{
					if( ! ::IsWindow(hWndOwn) )
						return true;
					// user selected the nCmdRetVal item
					if( nCmdRetVal != 0 )
						SelectionSet( nCmdRetVal - nStartID , true, true );
					
					Invalidate();
				}
				else
				{
					ASSERT( FALSE );
					//delete pPopup;
				}
				if( ! ::IsWindow(hWndOwn) )
					return true;
			}
			else
			{
				ASSERT( FALSE );
				delete pPopup;
			}
		}
	}

	return true;
}

void CExtTabWnd::OnTabWndClickedItemCloseButton(
	LONG nItemIndex
	)
{
	ASSERT_VALID( this );
	nItemIndex;
}

void CExtTabWnd::_CbPaintCombinedTabListBtnContent(
	LPVOID pCookie,
	CDC & dc,
	const CWnd & refWndMenu,
	const CRect & rcExcludeArea, // in screen coords
	int eCombineAlign // CExtPopupMenuWnd::e_combine_align_t values
	)
{
	dc;
	refWndMenu;
	rcExcludeArea;
	eCombineAlign;
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( refWndMenu.GetSafeHwnd() != NULL );
	ASSERT( eCombineAlign != CExtPopupMenuWnd::__CMBA_NONE );
	
	if( rcExcludeArea.IsRectEmpty() )
		return;
	
	CRect rcExcludeAreaX(rcExcludeArea);
	refWndMenu.ScreenToClient( &rcExcludeAreaX );

CExtPopupMenuWnd * pPopup = (CExtPopupMenuWnd*)pCookie;
	ASSERT_VALID( pPopup );
	ASSERT_KINDOF( CExtPopupMenuWnd, pPopup );
	pPopup->PmBridge_GetPM()->PaintTabButton(
		dc,
		rcExcludeAreaX,
		__ETWH_BUTTON_TAB_LIST,
		true,	// bTopLeft
		false,	// bHorz
		true,	// bEnabled,
		false,	// bHover,
		false,	// bPushed,
		false,	// bGroupedMode
		(CObject*)&refWndMenu
		);
}

bool CExtTabWnd::OnTabWndClickedItem(
	LONG nItemIndex,
	bool bButtonPressed,
	INT nMouseButton, // MK_... values
	UINT nMouseEventFlags
	)
{
	ASSERT_VALID( this );
	nMouseEventFlags;

	if( bButtonPressed )
	{
		ASSERT( ItemGetCount() > 0 );
		ASSERT( nItemIndex >= 0 && nItemIndex < ItemGetCount() );
		if( OnTabWndMouseTrackingPushedStart( nMouseButton, nItemIndex ) )
		{
			SelectionSet( nItemIndex, true, true );
			return ( nMouseButton == MK_RBUTTON ) ? false : true;
		}
	}
	OnTabWndMouseTrackingPushedStop();
	return ( nMouseButton == MK_RBUTTON ) ? false : true;
}

void CExtTabWnd::OnTabWndDoScroll(
	LONG nStep,
	bool bSmoothScroll // = true
	)
{
	ASSERT_VALID( this );
	ASSERT( nStep != 0 );
	
	if( !_IsScrollAvail() )
		return;
//	if( _IsScrolling() )
//		return;
	if(		m_rcTabItemsArea.left >= m_rcTabItemsArea.right
		||	m_rcTabItemsArea.top >= m_rcTabItemsArea.bottom
		||	ItemGetCount() == 0
		||	ItemGetVisibleCount() == 0
		)
		return;

bool bHorz = OrientationIsHorizontal();
INT nScrollRange = bHorz
		? m_rcTabItemsArea.right - m_rcTabItemsArea.left
		: m_rcTabItemsArea.bottom - m_rcTabItemsArea.top
		;
	ASSERT( nScrollRange > 0 );

	if( nStep < 0 )
	{ // up
		if( m_nScrollPos == 0 )
			return;
		ASSERT( (GetTabWndStyle() & __ETWS_ENABLED_BTN_UP) != 0 );

		if( bSmoothScroll )
		{
			if( GetTabWndStyle() & __ETWS_SCROLL_BY_PAGE )
				m_nScrollDirectionRest = -nScrollRange;
			else
				m_nScrollDirectionRest = -m_nScrollPos;
			SetTimer(
				__EXTTAB_SCROLL_TIMER_ID,
				__EXTTAB_SCROLL_TIMER_PERIOD,
				NULL
				);
			return;
		}

		//m_nScrollPos -= nScrollRange;
		//m_nScrollPos--;
		m_nScrollPos += nStep;
		if( m_nScrollPos < 0 )
			m_nScrollPos = 0;

	} // up
	else
	{ // down
		if( m_nScrollPos == m_nScrollMaxPos )
			return;
		ASSERT( (GetTabWndStyle() & __ETWS_ENABLED_BTN_DOWN) != 0 );

		if( bSmoothScroll )
		{
			if( GetTabWndStyle() & __ETWS_SCROLL_BY_PAGE )
				m_nScrollDirectionRest = nScrollRange;
			else
				m_nScrollDirectionRest = m_nScrollMaxPos - m_nScrollPos;
			SetTimer(
				__EXTTAB_SCROLL_TIMER_ID,
				__EXTTAB_SCROLL_TIMER_PERIOD,
				NULL
				);
			return;
		}

		//m_nScrollPos += nScrollRange;
		//m_nScrollPos++;
		m_nScrollPos += nStep;
		if( m_nScrollPos > m_nScrollMaxPos )
			m_nScrollPos = m_nScrollMaxPos;
	} // down

	UpdateTabWnd( true );
}

LONG CExtTabWnd::SelectionSet(
	LONG nSelIndex,
	bool bEnsureVisible, // = false
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
LONG nOldSelection = m_nSelIndex;
	if( ! OnTabWndSelectionChange( m_nSelIndex, nSelIndex, true ) )
		return m_nSelIndex;
LONG nCount = ItemGetCount();
	if( nSelIndex < 0 )
		m_nSelIndex = -1;
	else
	{
		ASSERT( nSelIndex < nCount );
		m_nSelIndex = nSelIndex;
	} // else from if( nSelIndex < 0 )
	if( nOldSelection >= 0 )
	{
		ASSERT( nOldSelection < nCount );
		
		TAB_ITEM_INFO * pTii = ItemGet( nOldSelection );
		pTii->ModifyItemStyle(
			__ETWI_SELECTED,
			0
			);
	}
	if( m_nSelIndex >= 0 )
	{
		ASSERT( m_nSelIndex < nCount );

		TAB_ITEM_INFO * pTii = ItemGet( m_nSelIndex );
		ASSERT_VALID( pTii );
		//ASSERT( pTii->VisibleGet() );
		
		pTii->ModifyItemStyle(
			0,
			__ETWI_SELECTED
			);
	}
	if( ! OnTabWndSelectionChange( nOldSelection, m_nSelIndex, false ) )
		return m_nSelIndex;

	if( bEnsureVisible && m_nSelIndex >= 0 && ItemGetCount() > 0 )
	{
		ASSERT( m_nSelIndex < ItemGetCount() );
		// item should not have "invisible" style
		VERIFY(
			ItemEnsureVisible(
				m_nSelIndex,
				bUpdateTabWnd
				)
			);
	} // if( bEnsureVisible )
	else
		UpdateTabWnd( bUpdateTabWnd );
	return nOldSelection;
}

LONG CExtTabWnd::SelectionDelayedGet() const
{
	ASSERT_VALID( this );
	return m_nDelayedSelIndex;
}

bool CExtTabWnd::SelectionDelay(
	LONG nSelIndex, // = -1, // -1 - to cancel
	DWORD dwMilliseconds // = 0 // should be > 0 if nSelIndex >= 0
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	if( nSelIndex < 0 )
	{
		if( m_nDelayedSelIndex >= 0 )
		{
			KillTimer( __EXTTAB_SELECTION_DELAY_TIMER_ID );
			m_nDelayedSelIndex = -1;
		}
		return true;
	}

	if( m_nDelayedSelIndex >= 0 )
	{
		KillTimer( __EXTTAB_SELECTION_DELAY_TIMER_ID );
		m_nDelayedSelIndex = -1;
	}

INT nVisibleItemCount = ItemGetVisibleCount();
	if( nVisibleItemCount == 0 )
		return false;
INT nItemCount = ItemGetCount();
	if( nSelIndex >= nItemCount )
	{
		ASSERT( FALSE );
		return false;
	}
	if( ! (ItemGet( nSelIndex )->VisibleGet()) )
	{
		ASSERT( FALSE );
		return false;
	}
	ASSERT( dwMilliseconds > 0 );
	m_nDelayedSelIndex = nSelIndex;
	SetTimer(
		__EXTTAB_SELECTION_DELAY_TIMER_ID,
		dwMilliseconds,
		NULL
		);
	return true;
}

LONG CExtTabWnd::ItemFindByLParam(
	LPARAM lParam,
	LONG nIndexStartSearch, // = -1
	bool bIncludeVisible, // = true,
	bool bIncludeInvisible // = false
	) const
{
	ASSERT_VALID( this );
LONG nIndex = ( nIndexStartSearch < 0 )
		? 0
		: (nIndexStartSearch + 1)
		;
LONG nCount = ItemGetCount();
	for( ; nIndex < nCount; nIndex++ )
	{
		const TAB_ITEM_INFO * pTii = ItemGet( nIndex );
		ASSERT_VALID( pTii );
		bool bItemVisible = pTii->VisibleGet();
		if( bIncludeVisible  && !bIncludeInvisible && !bItemVisible )
			continue;
		if( !bIncludeVisible && bIncludeInvisible  && bItemVisible )
			continue;
		LPARAM lParam2 = pTii->LParamGet();
		if( lParam == lParam2 )
			return nIndex;
	} // for( ; nIndex < nCount; nIndex++ )
	return -1;
}

LONG CExtTabWnd::ItemFindByStyle(
	DWORD dwItemStyleInclude,
	DWORD dwItemStyleExclude, // = __ETWI_INVISIBLE
	LONG nIndexStartSearch // = -1
	) const
{
	ASSERT_VALID( this );
LONG nIndex = ( nIndexStartSearch < 0 )
		? 0
		: (nIndexStartSearch + 1)
		;
LONG nCount = ItemGetCount();
	for( ; nIndex < nCount; nIndex++ )
	{
		const TAB_ITEM_INFO * pTii = ItemGet( nIndex );
		ASSERT_VALID( pTii );
		DWORD dwItemStyle = pTii->GetItemStyle();
		if( dwItemStyleInclude != 0 )
		{
			DWORD dwItemStylesReallyIncluded = dwItemStyle & dwItemStyleInclude;
			if( dwItemStylesReallyIncluded == 0 )
				continue;
		}
		if( dwItemStyleExclude != 0 )
		{
			DWORD dwItemStylesReallyExculded = dwItemStyle & dwItemStyleExclude;
			if( dwItemStylesReallyExculded != 0 )
				continue;
		}
		return nIndex;
	} // for( ; nIndex < nCount; nIndex++ )
	return -1;
}

INT CExtTabWnd::ItemFindByAccessChar(
	__EXT_MFC_SAFE_TCHAR chrAccess,
	INT nStartIdx, // = -1
	BOOL bRestartAt0 // = TRUE
	) const
{
	ASSERT_VALID( this );
	if( chrAccess == _T('0') )
		return -1;
TCHAR szChar[2] = { chrAccess, _T('\0') };
	::CharUpper( szChar );
int cAccelSearch = szChar[0];
	if( cAccelSearch == _T('\0') )
		return -1;
LONG nCount = ItemGetCount();
LONG nIdx = ( nStartIdx >= 0L ) ? ( nStartIdx + 1L ) : 0L;
	if( nIdx >= nCount && bRestartAt0 )
		nIdx = 0L;
	for( ; nIdx < nCount; nIdx++ )
	{
		const TAB_ITEM_INFO * pTII = ItemGet( nIdx );
		ASSERT_VALID( pTII );
		if( ! pTII->VisibleGet() )
			continue;
		if( ! pTII->EnabledGet() )
			continue;
		CExtSafeString str = pTII->TextGet();
		if( str.IsEmpty() )
			continue;
		INT nPos = str.Find( _T("&") );
		if(		nPos < 0
			||	nPos == (str.GetLength() - 1)
			)
			continue;
		++ nPos;
		TCHAR szAccel[2] = { str.GetAt( nPos ), _T('\0') };
		::CharUpper( szAccel );
		if( szAccel[0] == _T('\0') )
			return -1;
		if( cAccelSearch == szAccel[0] )
		{
			if( nStartIdx != nIdx )
				return nIdx;
		}
	} // for( nIdx = nStartIdx; nIdx < nCount; nIdx++ )
	if( nStartIdx == 0 )
		return -1;
	for( nIdx = 0; nIdx<nStartIdx; nIdx++ )
	{
		const TAB_ITEM_INFO * pTII = ItemGet( nIdx );
		ASSERT_VALID( pTII );
		if( ! pTII->VisibleGet() )
			continue;
		if( ! pTII->EnabledGet() )
			continue;
		CExtSafeString str = pTII->TextGet();
		if( str.IsEmpty() )
			continue;
		INT nPos = str.Find( _T("&") );
		if(		nPos < 0
			||	nPos == (str.GetLength() - 1)
			)
			continue;
		++ nPos;
		TCHAR szAccel[2] = { str.GetAt( nPos ), _T('\0') };
		::CharUpper( szAccel );
		if( szAccel[0] == _T('\0') )
			return -1;
		if( cAccelSearch == szAccel[0] )
		{
			if( nStartIdx != nIdx )
				return nIdx;
		}
	} // for( nIdx = 0; nIdx<nStartIdx; nIdx++ )
	return -1;
}

bool CExtTabWnd::OnTabWndSelectionChange(
	LONG nOldItemIndex,
	LONG nNewItemIndex,
	bool bPreSelectionTest
	)
{
	ASSERT_VALID( this );
	nOldItemIndex;
	if( bPreSelectionTest )
	{
		if( nNewItemIndex >= 0 )
		{
			TAB_ITEM_INFO * pTII = ItemGet( nNewItemIndex );
			ASSERT_VALID( pTII );
			if( ! pTII->EnabledGet() )
				return false;
		}
	}   
	return true;
}

CExtCmdIcon * CExtTabWnd::OnTabWndQueryItemIcon(
	const CExtTabWnd::TAB_ITEM_INFO * pTii
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	if( pTii != NULL ) 
		return (CExtCmdIcon*)&pTii->m_icon;
	return NULL;
}

__EXT_MFC_SAFE_LPCTSTR CExtTabWnd::OnTabWndQueryItemText(
	const CExtTabWnd::TAB_ITEM_INFO * pTii
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	if( pTii != NULL ) 
		return pTii->m_sText;
	return NULL;
}

__EXT_MFC_SAFE_LPCTSTR CExtTabWnd::OnTabWndQueryItemTooltipText(
	const CExtTabWnd::TAB_ITEM_INFO * pTii
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	if( pTii != NULL ) 
		return pTii->m_sTooltipText;
	return NULL;
}

CSize CExtTabWnd::OnTabWndQueryItemCloseButtonSize(
	const TAB_ITEM_INFO * pTii
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
CExtCmdIcon * pCmdIcon = OnTabWndQueryItemCloseButtonShape( pTii );
	if( pCmdIcon == NULL || pCmdIcon->IsEmpty() )
		return CSize( 0, 0 );
CSize _sizeCloseButton = pCmdIcon->GetSize();
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
	_sizeCloseButton.cx = pPM->UiScalingDo( _sizeCloseButton.cx, CExtPaintManager::__EUIST_X );
	_sizeCloseButton.cy = pPM->UiScalingDo( _sizeCloseButton.cy, CExtPaintManager::__EUIST_Y );
	return _sizeCloseButton;
}

CExtCmdIcon * CExtTabWnd::OnTabWndQueryItemCloseButtonShape(
	const TAB_ITEM_INFO * pTii
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
bool bCloseButtonOnThisTab =
			(	( (GetTabWndStyle()&__ETWS_GROUPED) == 0 )
		&&	( (GetTabWndStyleEx()&__ETWS_EX_CLOSE_ON_TABS) != 0 )
		&&	( (pTii->GetItemStyleEx()&__ETWI_EX_NO_CLOSE_ON_TAB) == 0 )
		) ? true : false;
	if( ! bCloseButtonOnThisTab )
		return NULL;
	if( ! m_iconTabItemCloseButton.IsEmpty() )
		return (&m_iconTabItemCloseButton);
CExtBitmap _bmp;
	if( ! _bmp.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_TAB_CLOSE_BTN_DEFAULT) ) )
		return NULL;
	_bmp.Make32();
	_bmp.AlphaColor( RGB(255,0,255), RGB(0,0,0), BYTE(0) );
CRect rc( 0, 0, 16, 16 );
	m_iconTabItemCloseButton.m_bmpDisabled.FromBitmap( _bmp, rc );
	m_iconTabItemCloseButton.m_bmpDisabled.AdjustHLS( COLORREF(-1L), COLORREF(-1L), 0.0, 0.0, -1.0 );
	m_iconTabItemCloseButton.m_bmpDisabled.AdjustAlpha( -0.25 );
	rc.OffsetRect( 16, 0 );
	m_iconTabItemCloseButton.m_bmpNormal.FromBitmap( _bmp, rc );
	m_iconTabItemCloseButton.m_bmpNormal.AdjustHLS( COLORREF(-1L), COLORREF(-1L), 0.0, 0.0, -0.25 );
	//m_iconTabItemCloseButton.m_bmpNormal.AdjustAlpha( -0.25 );
	rc.OffsetRect( 16, 0 );
	m_iconTabItemCloseButton.m_bmpHover.FromBitmap( _bmp, rc );
	rc.OffsetRect( 16, 0 );
	m_iconTabItemCloseButton.m_bmpPressed.FromBitmap( _bmp, rc );
	m_iconTabItemCloseButton.m_dwFlags =
		  __EXT_ICON_PERSISTENT_BITMAP_DISABLED
		| __EXT_ICON_PERSISTENT_BITMAP_HOVER
		| __EXT_ICON_PERSISTENT_BITMAP_PRESSED;
	return (&m_iconTabItemCloseButton);
}

INT CExtTabWnd::OnTabWndQueryItemCloseButtonPaintState(
	const TAB_ITEM_INFO * pTii
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	if( (GetTabWndStyleEx()&__ETWS_EX_CLOSE_ON_SELECTED_ONLY) != 0 )
	{
		if( SelectionGetPtr() != pTii )
			return INT( CExtCmdIcon::__PAINT_INVISIBLE );
	}
	if( ! pTii->EnabledGet() )
		return INT( CExtCmdIcon::__PAINT_DISABLED );
LONG nPushedIdx = GetPushedTrackingItem();
	if( nPushedIdx >= 0 )
	{
		if( nPushedIdx == ItemGetIndexOf( pTii ) )
		{
			if( m_bPushedTrackingCloseButton )
				return INT( CExtCmdIcon::__PAINT_PRESSED );
		}
	}
LONG nHoverIdx = GetHoverTrackingItem();
	if( nHoverIdx >= 0 )
	{
		if( nHoverIdx == ItemGetIndexOf( pTii ) )
			return INT( CExtCmdIcon::__PAINT_HOVER );
	}
	if( SelectionGetPtr() != pTii )
		return INT( CExtCmdIcon::__PAINT_DISABLED );
	return INT( CExtCmdIcon::__PAINT_NORMAL );
}

void CExtTabWnd::OnTabWndUpdateItemMeasure(
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	CDC & dcMeasure,
	CSize & sizePreCalc
	)
{
	ASSERT_VALID( this );
	PmBridge_GetPM()->TabWnd_UpdateItemMeasure(
		this,
		pTii,
		dcMeasure,
		sizePreCalc
		);
}

void CExtTabWnd::OnTabWndItemInsert(
	LONG nItemIndex,
	CExtTabWnd::TAB_ITEM_INFO * pTii
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	ASSERT( ItemGetCount() > 0 );
	ASSERT( nItemIndex >= 0 && nItemIndex < ItemGetCount() );
	ASSERT( ItemGetIndexOf( pTii ) == nItemIndex );
	pTii;
	nItemIndex;
}

void CExtTabWnd::OnTabWndRemoveItem(
	LONG nItemIndex,
	LONG nCount,
	bool bPreRemove
	)
{
	ASSERT_VALID( this );
	nItemIndex;
	nCount;
	bPreRemove;
}

void CExtTabWnd::OnTabWndRemoveAllItems(
	bool bPreRemove
	)
{
	ASSERT_VALID( this );
	bPreRemove;
}

CSize CExtTabWnd::OnTabWndCalcButtonSize(
	CDC & dcMeasure,
	LONG nTabAreaMetric // vertical max width or horizontal max heights of all tabs
	)
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	dcMeasure;
	nTabAreaMetric;
CSize _sizeTmp(
		::MulDiv( ::GetSystemMetrics( SM_CXSMSIZE ), 3, 4 ),
		::MulDiv( ::GetSystemMetrics( SM_CYSMSIZE ), 3, 4 )
		);
int nMetric =
	min( _sizeTmp.cx, _sizeTmp.cy ) + 1;
CSize _sizeButton(
		max( nMetric, __EXTTAB_BTN_MIN_DX ),
		max( nMetric, __EXTTAB_BTN_MIN_DY )
		);
	return _sizeButton;
}

void CExtTabWnd::OnSysColorChange() 
{
	ASSERT_VALID( this );
	CWnd::OnSysColorChange();
//CExtPaintManager * pPM = PmBridge_GetPM();
//	g_PaintManager.OnSysColorChange( this );
//	g_CmdManager.OnSysColorChange( pPM, this );
	Invalidate();
}

LRESULT CExtTabWnd::OnThemeChanged( WPARAM wParam, LPARAM lParam )
{
	wParam;
	lParam;
LRESULT lResult = Default();
//CExtPaintManager * pPM = PmBridge_GetPM();
//	g_PaintManager.OnThemeChanged( this, wParam, lParam );
//	g_CmdManager.OnThemeChanged( pPM, this, wParam, lParam );
	return lResult;
}

void CExtTabWnd::OnSettingChange(UINT uFlags, __EXT_MFC_SAFE_LPCTSTR lpszSection) 
{
	ASSERT_VALID( this );
	CWnd::OnSettingChange(uFlags, lpszSection);
//CExtPaintManager * pPM = PmBridge_GetPM();
//	g_PaintManager.OnSettingChange( this, uFlags, lpszSection );
//	g_CmdManager.OnSettingChange( pPM, this, uFlags, lpszSection );
	Invalidate();
}

LRESULT CExtTabWnd::_OnQueryRepositionCalcEffect(WPARAM wParam, LPARAM lParam)
{
	ASSERT_VALID( this );
	lParam;
	if( !m_bReflectQueryRepositionCalcEffect )
		return 0L;
CExtControlBar::QUERY_REPOSITION_CALC_EFFECT_DATA * p_qrced =
		(CExtControlBar::QUERY_REPOSITION_CALC_EFFECT_DATA *)
			(wParam);
	ASSERT( p_qrced != NULL );
	ASSERT_VALID( p_qrced->m_pWndToReposChilds );
	ASSERT( p_qrced->m_pWndToReposChilds->GetSafeHwnd() != NULL );
	if( p_qrced->IsQueryReposQuery() )
	{
		if( p_qrced->m_pWndToReposChilds->IsKindOf(
				RUNTIME_CLASS(CFrameWnd)
				)
			)
			p_qrced->ExcludeFromCenterSet();
	} // if( p_qrced->IsQueryReposQuery() )
	return 0L;
}

void CExtTabWnd::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	ASSERT_VALID( this );
	pWnd;
	point;
}

void CExtTabWnd::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);

DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle & WS_VISIBLE) == 0 )
		return;
CExtPopupMenuTipWnd * pATTW =
		OnAdvancedPopupMenuTipWndGet();
	if( pATTW != NULL )
	{
		if(		pATTW->GetSafeHwnd() != NULL
			&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
			)
			pATTW->Hide();
	}
	if( m_wndToolTip.GetSafeHwnd() != NULL )
		m_wndToolTip.DelTool( this, 1 );
	CWnd::CancelToolTips();

	UpdateTabWnd( true );
}

void CExtTabWnd::OnWindowPosChanged(WINDOWPOS FAR* lpwndpos) 
{
	CWnd::OnWindowPosChanged(lpwndpos);

DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle & WS_VISIBLE) == 0 )
		return;

CExtPopupMenuTipWnd * pATTW =
		OnAdvancedPopupMenuTipWndGet();
	if( pATTW != NULL )
	{
		if(		pATTW->GetSafeHwnd() != NULL
			&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
			)
			pATTW->Hide();
	}
	if( m_wndToolTip.GetSafeHwnd() != NULL )
		m_wndToolTip.DelTool( this, 1 );
	CWnd::CancelToolTips();

	UpdateTabWnd( true );
}

void CExtTabWnd::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CWnd::OnShowWindow(bShow, nStatus);
	
DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle & WS_VISIBLE) == 0 )
		return;

	UpdateTabWnd( true );
}

void CExtTabWnd::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
//	CWnd::OnLButtonDblClk(nFlags, point);
	nFlags;
	point;
}
void CExtTabWnd::OnRButtonDblClk(UINT nFlags, CPoint point) 
{
//	CWnd::OnRButtonDblClk(nFlags, point);
	nFlags;
	point;
}
void CExtTabWnd::OnMButtonDblClk(UINT nFlags, CPoint point) 
{
//	CWnd::OnMButtonDblClk(nFlags, point);
	nFlags;
	point;
}
int CExtTabWnd::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message) 
{
//	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
	pDesktopWnd;
	nHitTest;
	message;

bool bTopParentActive = false;
HWND hWndForeground = ::GetForegroundWindow();
CWnd * pWndForeground =
		(hWndForeground == NULL)
			? NULL
			: CWnd::FromHandlePermanent(hWndForeground)
			;
	if( pWndForeground != NULL )
	{
		CWnd * pWndTopParent =
			GetTopLevelParent();
		if( pWndTopParent != NULL )
		{
			HWND hWndLastActivePopup = ::GetLastActivePopup( pWndTopParent->m_hWnd );
			CWnd * pWndLastActivePopup =
				(hWndLastActivePopup == NULL)
					? NULL
					: CWnd::FromHandlePermanent(hWndLastActivePopup)
					;
			if( pWndForeground == pWndLastActivePopup )
				bTopParentActive = true;
		}
	}

	if( ! bTopParentActive )
	{
		HWND hWndParent = ::GetParent( GetSafeHwnd() );
		if( hWndParent != NULL )
			::SetFocus( hWndParent );
	}

	return MA_NOACTIVATE;
}
void CExtTabWnd::OnSetFocus(CWnd* pOldWnd) 
{
//	CWnd::OnSetFocus(pOldWnd);
	pOldWnd;
HWND hWndParent = ::GetParent( GetSafeHwnd() );
	if( hWndParent != NULL )
		::SetFocus( hWndParent );
}

void CExtTabWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, true, MK_LBUTTON, nFlags ) )
		CWnd::OnLButtonDown(nFlags, point);
}
void CExtTabWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, false, MK_LBUTTON, nFlags ) )
		CWnd::OnLButtonUp(nFlags, point);
}
void CExtTabWnd::OnRButtonDown(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, true, MK_RBUTTON, nFlags ) )
		CWnd::OnRButtonDown(nFlags, point);
}
void CExtTabWnd::OnRButtonUp(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, false, MK_RBUTTON, nFlags ) )
		CWnd::OnRButtonUp(nFlags, point);
}
void CExtTabWnd::OnMButtonDown(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, true, MK_MBUTTON, nFlags ) )
		CWnd::OnMButtonDown(nFlags, point);
}
void CExtTabWnd::OnMButtonUp(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, false, MK_MBUTTON, nFlags ) )
		CWnd::OnMButtonUp(nFlags, point);
}

HCURSOR CExtTabWnd::g_hCursor = ::LoadCursor( NULL, IDC_ARROW );

BOOL CExtTabWnd::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite =
		CExtCustomizeSite::GetCustomizeSite( m_hWnd );
	if(		pSite != NULL
		&&	pSite->IsCustomizeMode()
		)
		return FALSE;
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
	if( CExtControlBar::FindHelpMode(this) )
		return FALSE;
	if( g_hCursor != NULL )
	{
//		HCURSOR hCursor = GetCursor();
//		if( hCursor != g_hCursor )
//		{
//			CPoint ptRealCursorPos;
//			::GetCursorPos( &ptRealCursorPos );
//			ScreenToClient( &ptRealCursorPos );
//			CRect rcClient;
//			GetClientRect( &rcClient );
//			if( rcClient.PtInRect(ptRealCursorPos) )
				::SetCursor( g_hCursor );
//		} // if( hCursor != g_hCursor )
		return TRUE;
	} // if( g_hCursor != NULL )
	
	return CWnd::OnSetCursor(pWnd, nHitTest, message);
}

void CExtTabWnd::_ProcessMouseMove(
	UINT nFlags, 
	CPoint point
	)
{
	ASSERT_VALID( this );
	if( _IsDND() )
		return;
	nFlags;
CExtPopupMenuTipWnd * pATTW =
		OnAdvancedPopupMenuTipWndGet();
	if( m_nPushedTrackingHitTest >= 0 )
	{
		LONG nIndexToStartDrag = m_nPushedTrackingButton;
		LONG nItemCount = ItemGetCount();
		if(		nItemCount == 0
			||	nIndexToStartDrag < 0
			||	nIndexToStartDrag >= nItemCount
			)
		{
			if( pATTW != NULL )
			{
				if(		pATTW->GetSafeHwnd() != NULL
					&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
					)
					pATTW->Hide();
			}
			if( m_wndToolTip.GetSafeHwnd() != NULL )
				m_wndToolTip.DelTool( this, 1 );
			CWnd::CancelToolTips();
			bool bCancelMode = true;
			if(		m_bPushedUp
				||	m_bPushedDown
				||	m_bPushedScrollHome
				||	m_bPushedScrollEnd
				||	m_bPushedHelp
				||	m_bPushedClose
				||	m_bPushedTabList
				|| _IsScrolling()
				)
				bCancelMode = false;
			if( bCancelMode )
				SendMessage( WM_CANCELMODE );
			return;
		}
		if(		(abs(point.x-m_ptStartDrag.x)) > __EXTTAB_DRAG_START_DX
			||	(abs(point.y-m_ptStartDrag.y)) > __EXTTAB_DRAG_START_DY
			)
		{
			HWND hWndOwn = m_hWnd;
			if(		(! m_bPushedTrackingCloseButton )
				&&	OnTabWndStartDrag(nIndexToStartDrag)
				)
			{
				bool bValidOwn = ( hWndOwn != NULL && ::IsWindow( hWndOwn ) ) ? true : false;
				if( pATTW != NULL )
				{
					if(		pATTW->GetSafeHwnd() != NULL
						&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
						)
						pATTW->Hide();
				}
				if( bValidOwn && m_wndToolTip.GetSafeHwnd() != NULL )
					m_wndToolTip.DelTool( this, 1 );
				CWnd::CancelToolTips();

				if( bValidOwn )
					OnTabWndProcessDrag( nIndexToStartDrag );

				return;
			}
		}
	}

	if( m_bTrackingButtonPushed )
	{
		if( pATTW != NULL )
		{
			if(		pATTW->GetSafeHwnd() != NULL
				&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
				)
				pATTW->Hide();
		}
		if( m_wndToolTip.GetSafeHwnd() != NULL )
			m_wndToolTip.DelTool( this, 1 );
		CWnd::CancelToolTips();
		return;
	}

	if( ! CExtPopupMenuWnd::TestHoverEnabledFromActiveHWND(
			GetSafeHwnd()
			)
		)
		return;

LONG nHitTest = ItemHitTest( point );
bool bCancelTips = true;
	switch( nHitTest )
	{
	case __ETWH_BUTTON_LEFTUP:
	case __ETWH_BUTTON_RIGHTDOWN:
	case __ETWH_BUTTON_SCROLL_HOME:
	case __ETWH_BUTTON_SCROLL_END:
	case __ETWH_BUTTON_HELP:
	case __ETWH_BUTTON_CLOSE:
	case __ETWH_BUTTON_TAB_LIST:
		if( m_nHoverTrackedIndex != nHitTest )
		{
			if( OnTabWndMouseTrackingHoverStart( nHitTest ) )
				bCancelTips = false;
		} // if( m_nHoverTrackedIndex != nHitTest )
		else
			bCancelTips = false;
		break;
	default:
		{
			bool bRedraw = true;
			if(	nHitTest >= 0 )
			{
				if( m_nHoverTrackedIndex != nHitTest )
				{
					// if mouse is really on items
					if( OnTabWndMouseTrackingHoverStart( nHitTest ) )
					{
						bRedraw = OnTabWndQueryHoverChangingRedraw();
					}
				} // if( m_nHoverTrackedIndex != nHitTest )
				else
					bRedraw = false;
			}
			else
			{
				// if mouse is really NOT on items
				OnTabWndMouseTrackingHoverStop( true );
			}

			if( bRedraw )
			{
				Invalidate();
				UpdateWindow();
			}

			if(		ItemGetVisibleCount() > 0
				&&	nHitTest >= 0
				)
			{
				TAB_ITEM_INFO * pTii = ItemGet(nHitTest);
				ASSERT_VALID( pTii );
				ASSERT( pTii->VisibleGet() );
				CExtPopupMenuTipWnd * pATTW =
					OnAdvancedPopupMenuTipWndGet();

				if(		(GetTabWndStyle() & __ETWS_HOVER_FOCUS)
					&&	m_nDelayedSelIndex != nHitTest
					)
				{
					if( m_nDelayedSelIndex >= 0 )
					{
						VERIFY( SelectionDelay() );
					}
					VERIFY(
						SelectionDelay(
							nHitTest,
							__EXTTAB_SELECTION_DELAY_TIMER_PERIOD
							)
						);
					ASSERT( m_nDelayedSelIndex == nHitTest );
				}
				else if(
						OnTabWndToolTipQueryEnabled()
					&&	(	pATTW != NULL
						||	m_wndToolTip.GetSafeHwnd() != NULL
						)
					)
				{
					CString sItemTooltipText = 
						pTii->TooltipTextGet();
					if(		sItemTooltipText.IsEmpty() 
						&&	m_bEnableTrackToolTips
						)
					{
						ASSERT( GetTabWndStyle() & __ETWS_EQUAL_WIDTHS );
						if( pTii->IsToolTipAvailByExtent() )
							sItemTooltipText = pTii->TextGet();
					}
					if( ! sItemTooltipText.IsEmpty() )
					{
						bCancelTips = false;
						CRect rcItem = pTii->ItemRectGet();
						if( _IsScrollAvail() )
						{
							if( OrientationIsHorizontal() )
								rcItem.OffsetRect( -m_nScrollPos, 0 );
							else
								rcItem.OffsetRect( 0, -m_nScrollPos );
						}
						if( pATTW != NULL )
						{
							CRect rcArea( rcItem );
							rcArea.InflateRect( 4, 4 );
							ClientToScreen( &rcArea );
							OnAdvancedPopupMenuTipWndDisplay(
								*pATTW,
								rcArea,
								__EXT_MFC_SAFE_LPCTSTR(sItemTooltipText)
								);
						}
						else
							m_wndToolTip.AddTool(
								this,
								sItemTooltipText,
								&rcItem,
								1
								);
					}
				}
			}
		}
		break;
	} // switch( nHitTest )

	if( bCancelTips )
	{
		CExtPopupMenuTipWnd * pATTW =
			OnAdvancedPopupMenuTipWndGet();
		if( pATTW != NULL )
		{
			if(		pATTW->GetSafeHwnd() != NULL
				&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
				)
				pATTW->Hide();
		}
		if( m_wndToolTip.GetSafeHwnd() != NULL )
			m_wndToolTip.DelTool( this, 1 );
		CWnd::CancelToolTips();
	}
}

void CExtTabWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
//	CWnd::OnMouseMove(nFlags, point);
	_ProcessMouseMove( nFlags, point );
}

void CExtTabWnd::OnCaptureChanged(CWnd *pWnd) 
{
	CWnd::OnCaptureChanged(pWnd);
	if(		pWnd != NULL
		&&	pWnd != this
		)
	{
		OnTabWndMouseTrackingPushedStop( false );
		OnTabWndMouseTrackingHoverStop( false );
	}
	else if( m_bTrackingButtonHover )
		SendMessage( WM_CANCELMODE );
}

void CExtTabWnd::OnCancelMode() 
{
	CWnd::OnCancelMode();
	
	if( ( GetTabWndStyle() & __ETWS_SCROLL_BY_PAGE ) == 0 )
	{    
		if ( m_nScrollDirectionRest != 0 )
		{
			m_nScrollDirectionRest = 0;
			KillTimer( __EXTTAB_SCROLL_TIMER_ID );
		}
	}
	
	OnTabWndMouseTrackingPushedStop( true );
	OnTabWndMouseTrackingHoverStop( true );
}

bool CExtTabWnd::OnTabWndMouseTrackingHoverStart(
	LONG nHitTest
	)
{
	ASSERT_VALID( this );

	if( m_nHoverTrackedIndex == nHitTest )
		return true;

	if( ! CExtPopupMenuWnd::TestHoverEnabledFromActiveHWND( GetSafeHwnd() ) )
		return false;

	m_nHoverTrackingHitTest = nHitTest;
	m_bTrackingButtonHover = true;
	Invalidate();
	UpdateWindow();
	if( CExtMouseCaptureSink::GetCapture() != m_hWnd )
		CExtMouseCaptureSink::SetCapture( m_hWnd );
	m_nHoverTrackedIndex = nHitTest;
CExtPopupMenuTipWnd * pATTW =
		OnAdvancedPopupMenuTipWndGet();

	if(		OnTabWndToolTipQueryEnabled()
		&&	(	pATTW != NULL
			||	m_wndToolTip.GetSafeHwnd() != NULL
			)
		)
	{
		CRect rcTipTrack( 0, 0, 0, 0 );
		CExtSafeString sTooltipText;
		switch( nHitTest )
		{
		case __ETWH_BUTTON_LEFTUP:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_LEFTUP ) )
				sTooltipText = _T("Previous");
			rcTipTrack = m_rcBtnUp;
			break;

		case __ETWH_BUTTON_RIGHTDOWN:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_RIGHTDOWN ) )
				sTooltipText = _T("Next");
			rcTipTrack = m_rcBtnDown;
			break;
		case __ETWH_BUTTON_SCROLL_HOME:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_SCROLL_HOME ) )
				sTooltipText = _T("Home");
			rcTipTrack = m_rcBtnScrollHome;
			break;
		case __ETWH_BUTTON_SCROLL_END:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_SCROLL_END ) )
				sTooltipText = _T("End");
			rcTipTrack = m_rcBtnScrollEnd;
			break;		case __ETWH_BUTTON_HELP:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_HELP ) )
				sTooltipText = _T("Help");
			rcTipTrack = m_rcBtnHelp;
			break;
		case __ETWH_BUTTON_CLOSE:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_CLOSE ) )
				sTooltipText = _T("");
			rcTipTrack = m_rcBtnClose;
			break;
		case __ETWH_BUTTON_TAB_LIST:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_TAB_LIST ) )
				sTooltipText = _T("");
			rcTipTrack = m_rcBtnTabList;
			break;
		} // switch( nHitTest )
		if( ! sTooltipText.IsEmpty() )
		{
			if( pATTW != NULL )
			{
				CRect rcArea( rcTipTrack );
				ClientToScreen( &rcArea );
				OnAdvancedPopupMenuTipWndDisplay(
					*pATTW,
					rcArea,
					sTooltipText
					);
			}
			else
				m_wndToolTip.AddTool(
					this,
					sTooltipText,
					&rcTipTrack,
					1
					);
		}
		else
		{
			if( pATTW != NULL )
			{
				if(		pATTW->GetSafeHwnd() != NULL
					&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
					)
					pATTW->Hide();
			}
			m_wndToolTip.DelTool( this, 1 );
			CWnd::CancelToolTips();
		}
	} // if( OnTabWndToolTipQueryEnabled() ...

	return true;
}

void CExtTabWnd::OnTabWndMouseTrackingHoverStop(
	bool bEnableReleaseCapture // = true
	)
{
	ASSERT_VALID( this );

	m_nHoverTrackedIndex = -1L;

	if(! m_bTrackingButtonHover )
		return;
	m_bTrackingButtonHover = false;
	m_nHoverTrackingHitTest = __ETWH_NOWHERE;
	Invalidate();
	if( bEnableReleaseCapture )
		CExtMouseCaptureSink::ReleaseCapture();
}

bool CExtTabWnd::OnTabWndQueryHoverChangingRedraw() const
{
	ASSERT_VALID( this );
	if( (GetTabWndStyleEx()&__ETWS_EX_CLOSE_ON_TABS) != 0 )
		return true;
	return PmBridge_GetPM()->QueryTabWndHoverChangingRedraw( this );
}

void CExtTabWnd::OnTimer(__EXT_MFC_UINT_PTR nIDEvent) 
{
	switch( nIDEvent )
	{
	case __EXTTAB_SCROLL_TIMER_ID:
	{
		if( m_nScrollDirectionRest != 0 )
		{
			OnTabWndDoScroll(
				(m_nScrollDirectionRest < 0)
					? - ( min(__EXTTAB_SCROLL_STEP,(-m_nScrollDirectionRest)) )
					: + ( min(__EXTTAB_SCROLL_STEP, m_nScrollDirectionRest  ) )
				,
				false
				);
			if( m_nScrollDirectionRest < 0 )
			{
				//m_nScrollDirectionRest++;
				m_nScrollDirectionRest += __EXTTAB_SCROLL_STEP;
				if( m_nScrollDirectionRest > 0 )
					m_nScrollDirectionRest = 0;
			}
			else
			{
				//m_nScrollDirectionRest--;
				m_nScrollDirectionRest -= __EXTTAB_SCROLL_STEP;
				if( m_nScrollDirectionRest < 0 )
					m_nScrollDirectionRest = 0;
			}
		} // if( m_nScrollDirectionRest != 0 )

		if( m_nScrollDirectionRest == 0 )
			KillTimer( __EXTTAB_SCROLL_TIMER_ID );
	}
	break; // case __EXTTAB_SCROLL_TIMER_ID
	case __EXTTAB_SELECTION_DELAY_TIMER_ID:
	{
		KillTimer( __EXTTAB_SELECTION_DELAY_TIMER_ID );
		if( m_nSelIndex != m_nDelayedSelIndex
			&& m_nDelayedSelIndex >= 0
			&& m_nDelayedSelIndex < ItemGetCount()
			&& ItemGetVisibleCount() > 0
			&& ItemGet(m_nDelayedSelIndex)->VisibleGet()
			)
		{
			CPoint ptCursor( 0, 0 );
			if( ! ::GetCursorPos( &ptCursor ) )
				return;
			ScreenToClient( &ptCursor );
			LONG nHitTest = ItemHitTest( ptCursor );
			if( nHitTest == m_nDelayedSelIndex )
				SelectionSet( m_nDelayedSelIndex, true, true );
		} // if( m_nSelIndex != m_nDelayedSelIndex )
		m_nDelayedSelIndex = -1;
	}
	break; // case __EXTTAB_SELECTION_DELAY_TIMER_ID
//		case __EXTTAB_FLOATING_BAR_TIMER_ID:
//		{
//			KillTimer( __EXTTAB_FLOATING_BAR_TIMER_ID );
//			if( IsWindowVisible() )
//			{
//				CClientDC dcPaint( this );
//				CRect rcClient;
//				GetClientRect( &rcClient );
//	//			DoPaint( &dcPaint, rcClient );
//				CSize _size = rcClient.Size();
//				BITMAPINFOHEADER bih;
//				::memset( &bih, 0, sizeof( BITMAPINFOHEADER ) );
//				bih.biSize = sizeof( BITMAPINFOHEADER );
//				bih.biWidth = _size.cx;
//				bih.biHeight = _size.cy;
//				bih.biPlanes = 1;
//				bih.biBitCount = 32;
//				bih.biCompression = BI_RGB;
//				bih.biSizeImage = _size.cx * _size.cy;
//				COLORREF * pOutSurface = NULL;
//				HBITMAP hDIB =
//					::CreateDIBSection(
//						dcPaint.m_hDC,
//						(LPBITMAPINFO)&bih,
//						DIB_RGB_COLORS,
//						(void**)&pOutSurface,
//						NULL,
//						NULL
//						);
//				if( hDIB != NULL )
//				{
//					ASSERT( pOutSurface != NULL );
//					CDC dc;
//					if( dc.CreateCompatibleDC( &dcPaint ) )
//					{
//						HGDIOBJ hOldBitmap = ::SelectObject( dc.m_hDC, (HGDIOBJ)hDIB );
//						DoPaint( &dc, rcClient );
//						dcPaint.BitBlt( 0, 0, _size.cx, _size.cy, &dc, 0, 0, SRCCOPY );
//						::SelectObject( dc.m_hDC, hOldBitmap );
//					}
//					::DeleteObject( hDIB );
//				}
//			} // if( IsWindowVisible() )
//		}
//		break; // __EXTTAB_FLOATING_BAR_TIMER_ID
	default:
	{
		CWnd::OnTimer(nIDEvent);
	}
	break;
	} // switch( nIDEvent )
}

INT CExtTabWnd::OnTabWndGetParentSizingMargin(
	DWORD dwOrientation
	) const
{
	ASSERT_VALID( this );
INT nMargin = 0;
	if( PmBridge_GetPM()->TabWnd_GetParentSizingMargin(
			nMargin,
			dwOrientation,
			const_cast < CExtTabWnd * > ( this )
			)
		)
	{
		ASSERT( nMargin >= 0 );
		return nMargin;
	}
	switch( dwOrientation )
	{
	case __ETWS_ORIENT_TOP:
		return __EXTTAB_IN_FRAME_GAP_TOP_DY;
	case __ETWS_ORIENT_BOTTOM:
		return __EXTTAB_IN_FRAME_GAP_BOTTOM_DY;
	case __ETWS_ORIENT_LEFT:
		return __EXTTAB_IN_FRAME_GAP_LEFT_DX;
	case __ETWS_ORIENT_RIGHT:
		return __EXTTAB_IN_FRAME_GAP_RIGHT_DX;
	default:
		ASSERT( FALSE );
	return 0;
	} // switch( dwOrientation )
}

LRESULT CExtTabWnd::OnSizeParent( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	wParam;
	
	if( ! m_bReflectParentSizing )
		return 0;

AFX_SIZEPARENTPARAMS * lpLayout =
		(AFX_SIZEPARENTPARAMS *) lParam;
	ASSERT( lpLayout != NULL );
CRect rcFrameRest = &lpLayout->rect;
	if(		rcFrameRest.left >= rcFrameRest.right
		||	rcFrameRest.top >= rcFrameRest.bottom
		)
	{
		if( lpLayout->hDWP == NULL )
			return 0;
		::SetWindowPos(
			m_hWnd,
			NULL, 0, 0, 0, 0,
			SWP_NOSIZE|SWP_NOMOVE
				|SWP_NOZORDER|SWP_NOOWNERZORDER
				|SWP_HIDEWINDOW
			);
		return 0;
	}

	OnTabWndSyncVisibility();

DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle&WS_VISIBLE) == 0 )
		return 0;

	m_rcRecalcLayout = rcFrameRest;
	GetParent()->ClientToScreen( &m_rcRecalcLayout );
	ScreenToClient( &m_rcRecalcLayout );
	m_bDelayRecalcLayout = true;
	_RecalcLayoutImpl();
	m_rcRecalcLayout.SetRect( 0, 0, 0, 0 );
	
	if( m_rcTabItemsArea.IsRectEmpty() )
		return 0;

CSize _sizeNeeded = m_rcTabItemsArea.Size();
	if( _sizeNeeded.cx <= 0 || _sizeNeeded.cy <= 0 )
		return 0;

CRect rcOwnLayout( rcFrameRest );
DWORD dwOrientation = OrientationGet();
	if(		dwOrientation == __ETWS_ORIENT_LEFT
		||	dwOrientation == __ETWS_ORIENT_RIGHT
		)
	{
		HWND hWndParent = ::GetParent( m_hWnd );
		ASSERT( hWndParent != NULL && ::IsWindow(hWndParent) );
		__EXT_MFC_LONG_PTR dwStyleEx = ::__EXT_MFC_GetWindowLong( hWndParent, GWL_EXSTYLE );
		bool bParentRTL = ( (dwStyleEx&WS_EX_LAYOUTRTL) != 0 ) ? true : false;
		if( bParentRTL )
		{
			if( dwOrientation == __ETWS_ORIENT_LEFT )
				dwOrientation = __ETWS_ORIENT_RIGHT;
			else
				dwOrientation = __ETWS_ORIENT_LEFT;
		} // if( bParentRTL )
	}

	switch( dwOrientation )
	{
	case __ETWS_ORIENT_TOP:
	{
		ASSERT( _sizeNeeded.cy > 0 );
		//_sizeNeeded.cy += __EXTTAB_IN_FRAME_GAP_TOP_DY; // + 3;
		_sizeNeeded.cy += OnTabWndGetParentSizingMargin( dwOrientation );
		lpLayout->rect.top += _sizeNeeded.cy;
		rcOwnLayout.bottom = rcOwnLayout.top + _sizeNeeded.cy;
		lpLayout->sizeTotal.cy += _sizeNeeded.cy;
	}
	break;
	case __ETWS_ORIENT_BOTTOM:
	{
		ASSERT( _sizeNeeded.cy > 0 );
		_sizeNeeded.cy ++;
		_sizeNeeded.cy ++;
		//_sizeNeeded.cy += __EXTTAB_IN_FRAME_GAP_BOTTOM_DY;
		_sizeNeeded.cy += OnTabWndGetParentSizingMargin( dwOrientation );
		lpLayout->rect.bottom -= _sizeNeeded.cy;
		rcOwnLayout.top = rcOwnLayout.bottom - _sizeNeeded.cy;
		lpLayout->sizeTotal.cy += _sizeNeeded.cy;
	}
	break;
	case __ETWS_ORIENT_LEFT:
	{
		ASSERT( _sizeNeeded.cx > 0 );
		//_sizeNeeded.cx += __EXTTAB_IN_FRAME_GAP_LEFT_DX; // + 3;
		_sizeNeeded.cx += OnTabWndGetParentSizingMargin( dwOrientation );
		lpLayout->rect.left += _sizeNeeded.cx;
		rcOwnLayout.right = rcOwnLayout.left + _sizeNeeded.cx;
		lpLayout->sizeTotal.cx += _sizeNeeded.cx;
	}
	break;
	case __ETWS_ORIENT_RIGHT:
	{
		ASSERT( _sizeNeeded.cx > 0 );
		_sizeNeeded.cx ++;
		_sizeNeeded.cx ++;
		//_sizeNeeded.cx += __EXTTAB_IN_FRAME_GAP_RIGHT_DX;
		_sizeNeeded.cx += OnTabWndGetParentSizingMargin( dwOrientation );
		lpLayout->rect.right -= _sizeNeeded.cx;
		rcOwnLayout.left = rcOwnLayout.right - _sizeNeeded.cx;
		lpLayout->sizeTotal.cx += _sizeNeeded.cx;
	}
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( dwOrientation )

	ASSERT( ! rcOwnLayout.IsRectEmpty() );
	if( lpLayout->hDWP != NULL )
	{
		::AfxRepositionWindow(
			lpLayout,
			m_hWnd,
			&rcOwnLayout
			);
		::SetWindowPos(
			m_hWnd,
			NULL, 0, 0, 0, 0,
			SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER
				|SWP_FRAMECHANGED
			);
		UpdateTabWnd( true );
	} // if( lpLayout->hDWP != NULL )
	return 0;
}

bool CExtTabWnd::OnTabWndToolTipQueryEnabled() const
{
	return true;
}

LRESULT CExtTabWnd::_OnPaintExpandedItemContent( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	lParam;
CExtContentExpandWnd::PAINT_ITEM_CONTENT_DATA * p_picd =
		(CExtContentExpandWnd::PAINT_ITEM_CONTENT_DATA *)wParam;
	ASSERT( p_picd != NULL );
	ASSERT( p_picd->m_dc.GetSafeHdc() != NULL );
	if( ! OnTabWndDrawDraggedTabItemContent(*p_picd) )
		return 0;
	return (!0);
}

bool CExtTabWnd::OnTabWndDrawDraggedTabItemContent( CExtContentExpandWnd::PAINT_ITEM_CONTENT_DATA & picd ) const
{
	ASSERT_VALID( this );
	ASSERT( picd.m_dc.GetSafeHdc() != NULL );
LONG nSel = SelectionGet();
	if( nSel < 0L )
		return false;
bool bGroupedMode = (GetTabWndStyle() & __ETWS_GROUPED) ? true : false;
DWORD dwOrientation = OrientationGet();
	( const_cast < CExtTabWnd * > ( this ) ) -> OnTabWndEraseClientArea(
		picd.m_dc,
		picd.m_rcItem,
		picd.m_rcItem,
		( const_cast < CExtTabWnd * > ( this ) ) -> m_rcTabNearBorderArea,
		dwOrientation,
		bGroupedMode
		);
TAB_ITEM_INFO * pTii = ( const_cast < CExtTabWnd * > ( this ) ) -> ItemGet( nSel );
	ASSERT_VALID( pTii );
	ASSERT( pTii->VisibleGet() );
bool bHorz = OrientationIsHorizontal();
bool bTopLeft = OrientationIsTopLeft();
bool bCenteredText = (GetTabWndStyle() & __ETWS_CENTERED_TEXT) ? true : false;
bool bInvertedVerticalMode = (GetTabWndStyle() & __ETWS_INVERT_VERT_FONT) ? true : false;
CSize _sizeTextMeasured = pTii->GetLastMeasuredTextSize();
bool bSelected = true; // pTii->SelectedGet();
CExtSafeString sText = pTii->TextGet();
CExtCmdIcon * pIcon = pTii->IconGetPtr();
bool bInGroupActive = pTii->InGroupActiveGet();
CFont font;
	_GetTabWndFont( &font, bSelected );
	( const_cast < CExtTabWnd * > ( this ) ) -> OnTabWndDrawItem(
		picd.m_dc,
		picd.m_rcItem,
		nSel,
		pTii,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		picd.m_rcItem,
		_sizeTextMeasured,
		&font,
		sText,
		pIcon
		);
	return true;
}

bool CExtTabWnd::OnTabWndProcessDrag( LONG nIndex )
{
	ASSERT_VALID( this );
	ASSERT( nIndex >= 0 );
	if( _IsDND() )
		return false;
bool bExtendedAfterTarget = true;
bool bExtendedDND = ( ( GetTabWndStyleEx() & __ETWS_EX_EXTENDED_ITEM_DRAGGING ) != 0 ) ? true : false;
bool bExtendedShowTabShape = ( ( GetTabWndStyleEx() & __ETWS_EX_TAB_SHAPE_ITEM_DRAGGING ) != 0 ) ? true : false;
LONG nLastExtendedHT = -1L, nItemsCount = ItemGetCount();
bool bHorz = OrientationIsHorizontal();
TAB_ITEM_INFO * pTii = ItemGet( nIndex );
	ASSERT_VALID( pTii );
	ASSERT( pTii->VisibleGet() );
CExtContentExpandWnd wndDND, wndArrows;
CRect rcDND = pTii->ItemRectGet();
CPoint ptDND( 0, 0 ), ptShiftDND( 0, 0 );
HCURSOR hCursorDND = NULL;
	if( bExtendedDND )
	{
		rcDND.OffsetRect(
			bHorz ? (-m_nScrollPos) : 0,
			bHorz ? 0 : (-m_nScrollPos)
			);
		::GetCursorPos( &ptDND );
		CRect rcScreenDND = rcDND;
		ClientToScreen( &rcScreenDND );
		ptShiftDND.x = ptDND.x - rcScreenDND.left;
		ptShiftDND.y = ptDND.y - rcScreenDND.top;
		ptShiftDND.x = max( ptShiftDND.x, 5 );
		ptShiftDND.y = max( ptShiftDND.y, 3 );
		CPoint ptShiftAbs( abs(ptShiftDND.x), abs(ptShiftDND.y) );
		CSize sizeMaxShift = rcDND.Size();
		sizeMaxShift.cx -= 5;
		sizeMaxShift.cy -= 3;
		if( ptShiftAbs.x > sizeMaxShift.cx )
			ptShiftDND.x = ( ptShiftDND.x >= 0 ) ? sizeMaxShift.cx : (-sizeMaxShift.cx);
		if( ptShiftAbs.y > sizeMaxShift.cy )
			ptShiftDND.y = ( ptShiftDND.y >= 0 ) ? sizeMaxShift.cy : (-sizeMaxShift.cy);
		if( bExtendedShowTabShape )
		{
			if(	! wndDND.Activate(
					rcDND,
					this,
					__ECWAF_DEF_EXPANDED_ITEM_PAINTER
						|__ECWAF_NO_CAPTURE
						|__ECWAF_REDIRECT_MOUSE
						|__ECWAF_REDIRECT_NO_DEACTIVATE
						|__ECWAF_REDIRECT_AND_HANDLE
						|__ECWAF_HANDLE_MOUSE_ACTIVATE
						|__ECWAF_MA_NOACTIVATE
					)
				)
			{
				ASSERT( FALSE );
				return false;
			}
		}
		else
			hCursorDND = ::AfxGetApp()->LoadCursor( IDC_EXT_CMD_MOVE );
	}
HWND hWndOwn = GetSafeHwnd();
	ASSERT( hWndOwn != NULL && ::IsWindow(hWndOwn) );

	if( CExtMouseCaptureSink::GetCapture() != hWndOwn )
		CExtMouseCaptureSink::SetCapture( hWndOwn );
	m_bDragging = true;

	for( bool bStopFlag = false; (!bStopFlag) && ::IsWindow(hWndOwn); )
	{
		// Process all the messages in the message queue
		if( !::WaitMessage() )
			break;

		MSG msg;
		while( ::IsWindow(hWndOwn) && PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) )
		{
			switch( msg.message )
			{
			case WM_RBUTTONDOWN:
			case WM_RBUTTONUP:
			case WM_RBUTTONDBLCLK:
			case WM_MBUTTONDOWN:
			case WM_MBUTTONUP:
			case WM_MBUTTONDBLCLK:
			case WM_NCLBUTTONDOWN:
			case WM_NCLBUTTONUP:
			case WM_NCLBUTTONDBLCLK:
			case WM_NCRBUTTONDOWN:
			case WM_NCRBUTTONUP:
			case WM_NCRBUTTONDBLCLK:
			case WM_NCMBUTTONDOWN:
			case WM_NCMBUTTONUP:
			case WM_NCMBUTTONDBLCLK:
			case WM_MOUSEWHEEL:
			case WM_CONTEXTMENU:
			case WM_CANCELMODE:
			case WM_ACTIVATEAPP:
			case WM_SETTINGCHANGE:
			case WM_SYSCOLORCHANGE:
			case WM_LBUTTONUP:
				bStopFlag = true;
			break;
			case WM_CAPTURECHANGED:
				if( (HWND)msg.wParam != hWndOwn )
					bStopFlag = true;
			break;
			case WM_SETCURSOR:
				if( hCursorDND != NULL )
				{
					PeekMessage( &msg, NULL, WM_SETCURSOR, WM_SETCURSOR, PM_REMOVE );
					::SetCursor( hCursorDND );
					continue;
				}
			break;
			case WM_MOUSEMOVE:
			{
				if( hCursorDND != NULL )
					::SetCursor( hCursorDND );
				PeekMessage( &msg, NULL, WM_MOUSEMOVE, WM_MOUSEMOVE, PM_REMOVE );
//				for( ; PeekMessage(&msg, NULL, WM_MOUSEMOVE, WM_MOUSEMOVE, PM_NOREMOVE); )
//					PeekMessage(&msg, NULL, WM_MOUSEMOVE, WM_MOUSEMOVE, PM_REMOVE);
				CPoint pt( (DWORD)msg.lParam );
				if( msg.hwnd != m_hWnd )
				{
					::ClientToScreen( msg.hwnd, &pt );
					::ScreenToClient( m_hWnd, &pt );
				}
				Invalidate( TRUE );
				CPoint ptCursor = pt;
				ClientToScreen( &ptCursor );
//::GetCursorPos( &ptCursor );
//pt = ptCursor;
//ScreenToClient( &pt );
				if( bExtendedDND && ptDND != ptCursor )
				{
					if( bExtendedShowTabShape && wndDND.GetSafeHwnd() != NULL )
					{
						//CSize sizeShift = ptCursor - ptDND;
						CRect rcWndDND;
						wndDND.GetWindowRect( &rcWndDND );
						//rcWndDND.OffsetRect( sizeShift );
						rcWndDND.OffsetRect( ptCursor.x - rcWndDND.left - ptShiftDND.x, ptCursor.y - rcWndDND.top - ptShiftDND.y );
						wndDND.MoveWindow( &rcWndDND );
						wndDND.Invalidate( TRUE );
						wndDND.UpdateWindow();
						CExtPaintManager::stat_PassPaintMessages();
					}
					ptDND = ptCursor;
				}
				CRect rcWindow;
				GetWindowRect( &rcWindow );
				bool bScrolled = false;
				if( bHorz && ( ptCursor.x > rcWindow.right || ptCursor.x < rcWindow.left ) )
				{
					if( m_nScrollDirectionRest == 0 )
					{
						OnTabWndDoScroll( (ptCursor.x < rcWindow.left) ? -1 : +1, true );
						bScrolled = true;
					}
				}
				else if( !bHorz && ( ptCursor.y > rcWindow.bottom || ptCursor.y < rcWindow.top ) )
				{
					if( m_nScrollDirectionRest == 0 )
					{
						OnTabWndDoScroll( (ptCursor.y < rcWindow.top) ? -1 : +1, true );
						bScrolled = true;
					}
				}
				else
				{
					m_nScrollDirectionRest = 0;
					KillTimer( __EXTTAB_SCROLL_TIMER_ID );
				}
				LONG nHitTest = bScrolled ? (-1L) : ItemHitTest( pt );
				if( bExtendedDND )
				{
					if( bScrolled )
					{
						nLastExtendedHT = -1L;
						wndArrows.Deactivate();
						continue;
					}
					bool bExtendedAfterTargetPrev = bExtendedAfterTarget;
					if( nHitTest == nIndex || nHitTest < 0L )
					{
						nLastExtendedHT = -1L;
						wndArrows.Deactivate();
						continue;
					}
					CRect rcArrows = ItemGet( nHitTest ) -> ItemRectGet();
					rcArrows.OffsetRect(
						bHorz ? (-m_nScrollPos) : 0,
						bHorz ? 0 : (-m_nScrollPos)
						);
					if( bHorz )
					{
						bExtendedAfterTarget = ( ( rcArrows.right - pt.x ) < ( pt.x - rcArrows.left ) ) ? true : false;
						if( ! bExtendedAfterTarget )
							rcArrows.right = rcArrows.left;
						else
							rcArrows.left = rcArrows.right;
					}
					else
					{
						bExtendedAfterTarget = ( ( rcArrows.bottom - pt.y ) < ( pt.y - rcArrows.top ) ) ? true : false;
						if( ! bExtendedAfterTarget )
							rcArrows.bottom = rcArrows.top;
						else
							rcArrows.top = rcArrows.bottom;
					}
					if(		(    bExtendedAfterTarget   && ( nHitTest == ( nIndex - 1 ) ) )
						||	( (! bExtendedAfterTarget ) && ( nHitTest == ( nIndex + 1 ) ) )
						)
					{
						nLastExtendedHT = -1L;
						wndArrows.Deactivate();
						continue;
					}
					if( bExtendedAfterTarget )
					{
						bExtendedAfterTarget = false;
						nHitTest ++;
					}
					if( nHitTest == nLastExtendedHT && bExtendedAfterTargetPrev == bExtendedAfterTarget )
						continue;
					nLastExtendedHT = nHitTest;

					VERIFY(
						wndArrows.Activate(
							rcArrows,
							this,
							__ECWAF_DRAW_RED_ARROWS
								|__ECWAF_TRANSPARENT_ITEM
								|__ECWAF_NO_CAPTURE
								|__ECWAF_REDIRECT_MOUSE
								|__ECWAF_REDIRECT_NO_DEACTIVATE
								|__ECWAF_REDIRECT_AND_HANDLE
								|__ECWAF_HANDLE_MOUSE_ACTIVATE
								|__ECWAF_MA_NOACTIVATE
							)
						);
					if( bExtendedShowTabShape && wndArrows.GetSafeHwnd() != NULL && wndDND.GetSafeHwnd() != NULL )
					{
						wndDND.SetWindowPos( &wndArrows, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE );
						CExtPaintManager::stat_PassPaintMessages();
					}
				} // if( bExtendedDND )
				else
				{
					if( bScrolled )
						continue;
					if(		nItemsCount != 0
						&&	nIndex >= 0
						&&	nIndex < nItemsCount
						&&	nIndex != nHitTest
						&&	nHitTest >= 0
						&&	nHitTest < nItemsCount
						&&	(!m_rcLastMovedItemRect.PtInRect( pt ) || abs(nIndex - nHitTest) > 1 )
						)
					{
						m_rcLastMovedItemRect = ItemGet( nHitTest )->ItemRectGet();
						LONG nSpaceBefore = 0, nSpaceAfter = 0, nSpaceOver = 0;
							OnTabWndMeasureItemAreaMargins(
								nSpaceBefore,
								nSpaceAfter,
								nSpaceOver
								);
						m_rcLastMovedItemRect.OffsetRect( 
							bHorz ? nSpaceBefore - m_nScrollPos : 0,
							bHorz ? 0 : nSpaceBefore - m_nScrollPos
							);

						// move item into the new position
						if( ! ItemMove( nIndex, nHitTest, true ) )
							m_rcLastMovedItemRect = CRect(0,0,0,0);
						else
						{
							nIndex = nHitTest;
							SelectionSet( nIndex );
						}
					}
				} // else from if( bExtendedDND )
				continue;
			}
			break;
			default:
				if(	CExtMouseCaptureSink::GetCapture() != GetSafeHwnd()	)
					bStopFlag = true;
			break;
			} // switch( msg.message )

			if( ! ::IsWindow( hWndOwn ) )
			{
				bStopFlag = false;
				break;
			}
			if( ! AfxGetThread()->PumpMessage() )
			{
				PostQuitMessage(0);
				break; // Signal WM_QUIT received
			}
			if( bStopFlag )
				break;
		} // while( ::IsWindow(hWndOwn) && PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) )
		
		if( bStopFlag )
			break;
		if( CExtTabWnd::g_bEnableOnIdleCalls )
		{
			for(	LONG nIdleCounter = 0L;
					::AfxGetThread()->OnIdle(nIdleCounter);
					nIdleCounter ++
					);
		}
	} // for( bool bStopFlag = false; (!bStopFlag) && ::IsWindow(hWndOwn) ; )

	if( bExtendedDND )
	{
		wndDND.Deactivate();
		wndArrows.Deactivate();
	}
	if( ::IsWindow(hWndOwn) )
	{
		if( bExtendedDND && nLastExtendedHT >= 0L )
		{
			if( nLastExtendedHT > nIndex )
				nLastExtendedHT --;
			ASSERT( nLastExtendedHT >= 0 );
			ItemMove( nIndex, nLastExtendedHT, false );
		}
		if( CExtMouseCaptureSink::GetCapture() == hWndOwn )
			CExtMouseCaptureSink::ReleaseCapture();
		m_nPushedTrackingButton = (-1L);
		m_nScrollDirectionRest = 0;
		m_rcLastMovedItemRect = CRect(0,0,0,0);
		KillTimer( __EXTTAB_SCROLL_TIMER_ID );
		m_bDelayRecalcLayout = true;
		UpdateTabWnd();
		if( bExtendedDND && nLastExtendedHT >= 0L )
		{
			LONG nCount = ItemGetCount();
			LONG nLast = nCount - 1L;
			nLastExtendedHT = min( nLastExtendedHT, nLast );
			SelectionSet( nLastExtendedHT );
		}
	} // if( ::IsWindow(hWndOwn) )	
	m_bDragging = false;
	return true;
}

bool CExtTabWnd::_ItemMoveImpl( // move item into the new position
	LONG nIndex,
	LONG nIndexNew
	)
{
	ASSERT_VALID( this );
LONG nDirection = ( nIndex < nIndexNew ) ? (+1L) : (-1L);
	if( nDirection == 0 )
		return false;
LONG nSel = SelectionGet();
	if( nSel >= 0L )
		ItemGet( nSel ) -> ModifyItemStyle( __ETWI_SELECTED, 0 );
bool bSelected = ( nSel == nIndex ) ? true : false;
TAB_ITEM_INFO * pTii			= ItemGet( nIndex );
CExtSafeString sText			= pTii->TextGet();
CExtCmdIcon * pIconTmp			= OnTabWndQueryItemIcon( pTii );
CExtCmdIcon _iconEmpty;
CExtCmdIcon & _icon				= ( pIconTmp == NULL ) ? _iconEmpty : (*pIconTmp);
DWORD dwItemStyle				= pTii->GetItemStyle();
DWORD dwItemStyleEx				= pTii->GetItemStyleEx();
LPARAM lParam					= pTii->LParamGet();
CObject * pEventProvider		= pTii->EventProviderGet();
	ItemInsert( sText, _icon, dwItemStyle, ( nDirection > 0L ) ? ( nIndexNew + 1L ) : nIndexNew, lParam, false );
TAB_ITEM_INFO * pTiiNew = ItemGet( nIndexNew );
	ASSERT_VALID( pTiiNew );
	pTiiNew->EventProviderSet( pEventProvider );
	pTiiNew->ModifyItemStyleEx( 0xFFFFFFFF, dwItemStyleEx );
	pTiiNew->ModifyItemStyle( bSelected ? 0 : __ETWI_SELECTED, bSelected ? __ETWI_SELECTED : 0 );
	ItemRemove( ( nDirection > 0L ) ? nIndex : ( nIndex + 1L ), 1L, false );
	nIndexNew = ( nDirection > 0L ) ? ( nIndexNew - 1L ) : nIndexNew;
	SelectionSet( nIndexNew, false, false );
	return true;
}

bool CExtTabWnd::ItemMove(
	LONG nIndex,
	LONG nIndexNew,
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );

	LONG nItemsCount = ItemGetCount();
	if(!(	nItemsCount != 0
		&&	nIndex >= 0
		&&	nIndex < nItemsCount
		&&	nIndexNew >= 0
		&&	nIndexNew <= nItemsCount
		&&	nIndexNew != nIndex
		)
		)
		return false;
	if( ! OnTabWndItemPosChanging( nIndex, nIndexNew ) )
		return false;
	_ItemMoveImpl( nIndex, nIndexNew );
	UpdateTabWnd( bUpdateTabWnd );
	OnTabWndItemPosChanged( nIndex, nIndexNew );
	return true;
}

bool CExtTabWnd::OnTabWndItemPosChanging(
	LONG nItemIndex,
	LONG nItemNewIndex
	)
{
	ASSERT_VALID( this );
	nItemIndex;
	nItemNewIndex;
	return true;
}

void CExtTabWnd::OnTabWndItemPosChanged(
	LONG nItemIndex,
	LONG nItemNewIndex
	)
{
	ASSERT_VALID( this );
	nItemIndex;
	nItemNewIndex;
}
 
void AFXAPI __EXT_MFC_DDX_TabIndex( CDataExchange * pDX, INT nIDC, LONG & index )
{
HWND hWndCtrl = pDX->PrepareCtrl( nIDC );
	ASSERT( hWndCtrl != NULL );
	ASSERT( ::IsWindow( hWndCtrl ) );
CExtTabWnd * pWnd = 
		DYNAMIC_DOWNCAST( CExtTabWnd, CWnd::FromHandle( hWndCtrl ) );
	if( pWnd != NULL )
	{
		ASSERT_VALID( pWnd );
		if( pDX->m_bSaveAndValidate )
			index = pWnd->SelectionGet();
		else
			pWnd->SelectionSet( index, true, true );
	}
}

#if (!defined __EXT_MFC_NO_TABMDI_CTRL )

/////////////////////////////////////////////////////////////////////////////
// CExtTabMdiWnd window

IMPLEMENT_DYNCREATE( CExtTabMdiWnd, CExtTabWnd );

CExtTabMdiWnd::CExtTabMdiWnd()
{
}

CExtTabMdiWnd::~CExtTabMdiWnd()
{
}

BEGIN_MESSAGE_MAP(CExtTabMdiWnd, CExtTabWnd)
	//{{AFX_MSG_MAP(CExtTabMdiWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG

void CExtTabMdiWnd::AssertValid() const
{
	CExtTabWnd::AssertValid();
}

void CExtTabMdiWnd::Dump(CDumpContext& dc) const
{
	CExtTabWnd::Dump( dc );
}

#endif

#endif // (!defined __EXT_MFC_NO_TABMDI_CTRL )


/////////////////////////////////////////////////////////////////////////////
// CExtTabOneNoteWnd

#ifndef __EXT_MFC_NO_TAB_ONENOTE_CTRL

IMPLEMENT_DYNCREATE( CExtTabOneNoteWnd, CExtTabWnd );

#define __EXTTAB_ONENOTE_INDENT_TOP		1
#define __EXTTAB_ONENOTE_INDENT_BOTTOM	1
#define __EXTTAB_ONENOTE_INDENT_LEFT	0
#define __EXTTAB_ONENOTE_INDENT_RIGHT	4

CExtTabOneNoteWnd::CExtTabOneNoteWnd()
	: m_nNextColorIndex( 0 )
{
}

CExtTabOneNoteWnd::~CExtTabOneNoteWnd()
{
}

bool CExtTabOneNoteWnd::_IsCustomLayoutTabWnd() const
{
	return true;
}

BOOL CExtTabOneNoteWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
	UINT nDlgCtrlID, // = UINT( __EXT_MFC_IDC_STATIC )
	DWORD dwWindowStyle, // = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS
	DWORD dwTabStyle, // = __ETWS_ONENOTE_DEFAULT
	CCreateContext * pContext // = NULL
	)
{
	ASSERT( pParentWnd != NULL );
	ASSERT( pParentWnd->GetSafeHwnd() != NULL );
	if(	! CExtTabWnd::Create(
			pParentWnd,
			rcWnd,
			nDlgCtrlID,
			dwWindowStyle,
			dwTabStyle,
			pContext
			)
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	return TRUE;
}

void CExtTabOneNoteWnd::PreSubclassWindow() 
{
	CExtTabWnd::PreSubclassWindow();

	if( m_bDirectCreateCall )
		return;

	ModifyTabWndStyle( 
		0xFFFFFFFF, 
		__ETWS_ONENOTE_DEFAULT
		);
}

CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * CExtTabOneNoteWnd::ItemInsert(
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL
	HICON hIcon, // = NULL
	bool bCopyIcon, // = true
	DWORD dwItemStyle, // = 0 // visible & non-group
	LONG nIndex, // = -1 // default - append
	LPARAM lParam, // = 0
	COLORREF clrBkLight, // = (COLORREF)(-1L),
	COLORREF clrBkDark, // = (COLORREF)(-1L),
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
CExtCmdIcon _icon;
	if( hIcon != NULL )
		_icon.AssignFromHICON( hIcon, bCopyIcon );
	return
		ItemInsert(
			sText,
			_icon,
			dwItemStyle,
			nIndex,
			lParam,
			clrBkLight,
			clrBkDark,
			bUpdateTabWnd
			);
}

CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * CExtTabOneNoteWnd::ItemInsert(
	__EXT_MFC_SAFE_LPCTSTR sText,
	const CExtCmdIcon & _icon,
	DWORD dwItemStyle, // = 0 // visible & non-group
	LONG nIndex, // = -1 // default - append
	LPARAM lParam, // = 0
	COLORREF clrBkLight, // = (COLORREF)(-1L),
	COLORREF clrBkDark, // = (COLORREF)(-1L),
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
LONG nCount = ItemGetCount();
	if( nIndex < 0 || nIndex > nCount )
		nIndex = nCount;

TAB_ITEM_INFO * pTiiPrev = NULL;
	if( nIndex > 0 )
	{
		pTiiPrev = ItemGet( nIndex - 1 );
		ASSERT_VALID( pTiiPrev );
	}

TAB_ITEM_INFO * pTiiNext = NULL;
	if( nIndex < nCount )
	{
		pTiiNext = ItemGet( nIndex );
		ASSERT_VALID( pTiiNext );
	}

	if(		clrBkLight == (COLORREF)(-1L)
		||	clrBkDark == (COLORREF)(-1L)
		)
	{
		GenerateItemBkColors(
			m_nNextColorIndex++,
			&clrBkLight,
			&clrBkDark
		);
		ASSERT( clrBkLight != (COLORREF)(-1L) 
			&&	clrBkDark != (COLORREF)(-1L) 
			);
	}

TAB_ITEM_INFO_ONENOTE * pTiiON =
		new TAB_ITEM_INFO_ONENOTE(
			this,
			pTiiPrev,
			pTiiNext,
			sText,
			&_icon,
			dwItemStyle,
			lParam,
			clrBkLight,
			clrBkDark
			);
TAB_ITEM_INFO * pTii = static_cast < TAB_ITEM_INFO * > ( pTiiON );

	m_arrItems.InsertAt( nIndex, pTii );
	ASSERT_VALID( pTii );
	if( m_nSelIndex >= nIndex ) 
		m_nSelIndex++;

	if( pTii->VisibleGet() )
		m_nVisibleItemCount ++;

	ASSERT_VALID( this );

	OnTabWndItemInsert(
		nIndex,
		pTii
		);
	
	UpdateTabWnd( bUpdateTabWnd );
	return pTiiON;
}

bool CExtTabOneNoteWnd::_ItemMoveImpl( // move item into the new position
	LONG nIndex,
	LONG nIndexNew
	)
{
	ASSERT_VALID( this );
LONG nDirection = ( nIndex < nIndexNew ) ? (+1L) : (-1L);
	if( nDirection == 0 )
		return false;
LONG nSel = SelectionGet();
	if( nSel >= 0L )
		ItemGet( nSel ) -> ModifyItemStyle( __ETWI_SELECTED, 0 );
bool bSelected = ( nSel == nIndex ) ? true : false;
TAB_ITEM_INFO_ONENOTE * pTii	= static_cast < TAB_ITEM_INFO_ONENOTE * > ( ItemGet( nIndex ) );
CExtSafeString sText			= pTii->TextGet();
CExtCmdIcon * pIconTmp			= OnTabWndQueryItemIcon( pTii );
CExtCmdIcon _iconEmpty;
CExtCmdIcon & _icon				= ( pIconTmp == NULL ) ? _iconEmpty : (*pIconTmp);
DWORD dwItemStyle				= pTii->GetItemStyle();
DWORD dwItemStyleEx				= pTii->GetItemStyleEx();
LPARAM lParam					= pTii->LParamGet();
COLORREF clrBkLight				= pTii->GetColorBkLight();
COLORREF clrBkDark				= pTii->GetColorBkDark();
CObject * pEventProvider		= pTii->EventProviderGet();
	ItemInsert( sText, _icon, dwItemStyle, ( nDirection > 0L ) ? ( nIndexNew + 1L ) : nIndexNew, lParam, clrBkLight, clrBkDark, false );
TAB_ITEM_INFO * pTiiNew = ItemGet( nIndexNew );
	ASSERT_VALID( pTiiNew );
	pTiiNew->EventProviderSet( pEventProvider );
	pTiiNew->ModifyItemStyleEx( 0xFFFFFFFF, dwItemStyleEx );
	pTiiNew->ModifyItemStyle( bSelected ? 0 : __ETWI_SELECTED, bSelected ? __ETWI_SELECTED : 0 );
	ItemRemove( ( nDirection > 0L ) ? nIndex : ( nIndex + 1L ), 1L, false );
	nIndexNew = ( nDirection > 0L ) ? ( nIndexNew - 1L ) : nIndexNew;
	SelectionSet( nIndexNew, false, false );
	return true;
}

CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * CExtTabOneNoteWnd::ItemGet( LONG nIndex )
{
	ASSERT_VALID( this );
LONG nCount = ItemGetCount();
	if( nIndex < 0 || nIndex >= nCount )
	{
		ASSERT( FALSE );
		return NULL;
	}
TAB_ITEM_INFO_ONENOTE * pTii = 
		static_cast
			< TAB_ITEM_INFO_ONENOTE * >
			( m_arrItems[ nIndex ] );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	return pTii;
}

const CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * CExtTabOneNoteWnd::ItemGet( LONG nIndex ) const
{
	return
		( const_cast
			< CExtTabOneNoteWnd * >
			( this )
		) -> ItemGet( nIndex );
}

const CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * CExtTabOneNoteWnd::SelectionGetPtr() const
{
	ASSERT_VALID( this );
	if( m_nSelIndex < 0 || ItemGetCount() == 0 )
		return NULL;
	return ItemGet( m_nSelIndex );
}

CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * CExtTabOneNoteWnd::SelectionGetPtr()
{
	ASSERT_VALID( this );
	LONG nCount = ItemGetCount();
	if( m_nSelIndex < 0 || nCount == 0 )
		return NULL;
	return ItemGet( m_nSelIndex );
}	

bool CExtTabOneNoteWnd::OnTabWndQueryHoverChangingRedraw() const
{
	ASSERT_VALID( this );
	return true;
}

void CExtTabOneNoteWnd::OnTabWndMeasureItemAreaMargins(
	LONG & nSpaceBefore,
	LONG & nSpaceAfter,
	LONG & nSpaceOver
	)
{
	ASSERT_VALID( this );
	nSpaceBefore	= 1;
	nSpaceAfter		= 2;
	nSpaceOver		= 3;
}

void CExtTabOneNoteWnd::OnTabWndUpdateItemMeasure(
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	CDC & dcMeasure,
	CSize & sizePreCalc
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	ASSERT( pTii->GetTabWnd() == this );
	pTii;
	dcMeasure;
bool bHorz = OrientationIsHorizontal();
bool bFirstInChain = false;
LONG nIndex = pTii->GetIndexOf();
	ASSERT( nIndex >= 0 );
bool bFirstVisible = true;
CExtTabWnd::TAB_ITEM_INFO * pTiiWalk = pTii->GetPrev();
	for( ; pTiiWalk != NULL; pTiiWalk = pTiiWalk->GetPrev() )
	{
		if( pTiiWalk->VisibleGet() )
		{
			bFirstVisible = false;
			break;
		}
	}
	if( bFirstVisible )
		bFirstInChain = true;
	else
	{
		bool bMultiRowColumn = OnTabWndQueryMultiRowColumnLayout();
		CExtTabWnd::TAB_ITEM_INFO * pTiiWalk = NULL;
		nIndex --;
		for( ; true;  )
		{
			pTiiWalk = ItemGet( nIndex );
			ASSERT( pTiiWalk != NULL );
			bool bVisibleItem = pTiiWalk->VisibleGet();
			if( bVisibleItem )
				break;
			pTiiWalk = NULL;
			if( nIndex == 0 )
				break;
			nIndex --;
		}
		if( pTiiWalk == NULL )
			bFirstInChain = true;
		else if( bMultiRowColumn && pTiiWalk->MultiRowWrapGet() )
			bFirstInChain = true;
	}
	if( bHorz )
	{
		sizePreCalc.cx += (__EXTTAB_ONENOTE_INDENT_LEFT + __EXTTAB_ONENOTE_INDENT_RIGHT);
//		if( pTii->GetIndexOf() == 0 )
		if( bFirstInChain )
			sizePreCalc.cx += 16;
		sizePreCalc.cy = 19;
	}
	else
	{
		sizePreCalc.cy += (__EXTTAB_ONENOTE_INDENT_LEFT + __EXTTAB_ONENOTE_INDENT_RIGHT);
// 		if( pTii->GetIndexOf() == 0 )
		if( bFirstInChain )
			sizePreCalc.cy += 16;
		sizePreCalc.cx = 19;
	}
	if ( !bHorz )
	{
		sizePreCalc.cx = g_PaintManager->UiScalingDo(sizePreCalc.cx, CExtPaintManager::__EUIST_Y);
		//sizePreCalc.cy = g_PaintManager->UiScalingDo(sizePreCalc.cy, CExtPaintManager::__EUIST_X);
	}
	else
	{
		//sizePreCalc.cx = g_PaintManager->UiScalingDo(sizePreCalc.cx, CExtPaintManager::__EUIST_X);
		sizePreCalc.cy = g_PaintManager->UiScalingDo(sizePreCalc.cy, CExtPaintManager::__EUIST_Y);
	}
}

void CExtTabOneNoteWnd::OnTabWndEraseClientArea(
	CDC & dc,
	CRect & rcClient,
	CRect & rcTabItemsArea,
	CRect & rcTabNearBorderArea,
	DWORD dwOrientation,
	bool bGroupedMode
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	rcTabItemsArea;
	bGroupedMode;

	PmBridge_GetPM()->TabOneNoteWnd_DrawClientArea( 
		dc, 
		rcClient 
		);

	if( ! rcTabNearBorderArea.IsRectEmpty() )
	{
		CRect rcTabNearMargin( rcTabNearBorderArea ); // prepare tab border margin rect
		CRect rcColorLine( rcClient );

		switch( dwOrientation )
		{
		case __ETWS_ORIENT_TOP:
			rcTabNearMargin.bottom = rcTabNearMargin.top + 1;
			rcTabNearMargin.OffsetRect(0,-1);
			rcColorLine.top = rcTabNearMargin.bottom;
		break;
		case __ETWS_ORIENT_BOTTOM:
			rcTabNearMargin.top = rcTabNearMargin.bottom - 1;
			rcTabNearMargin.OffsetRect(0,1);
			rcColorLine.bottom = rcTabNearMargin.top; 
		break;
		case __ETWS_ORIENT_LEFT:
			rcTabNearMargin.right = rcTabNearMargin.left + 1;
			rcTabNearMargin.OffsetRect(-1,0);
			rcColorLine.left = rcTabNearMargin.right;
		break;
		case __ETWS_ORIENT_RIGHT:
			rcTabNearMargin.left = rcTabNearMargin.right - 1;
			rcTabNearMargin.OffsetRect(1,0);
			rcColorLine.right = rcTabNearMargin.left;
		break;
		default:
			ASSERT( FALSE );
		break;
		} // switch( dwOrientation )

		// paint tab border margin
		dc.FillSolidRect(
			&rcTabNearMargin,
			PmBridge_GetPM()->GetColor( COLOR_3DSHADOW, this )
			);

		if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) > 8 )
		{
			COLORREF clrBkDark = COLORREF( -1L );
			LONG nSelIndex = SelectionGet();
			if( nSelIndex >= 0L )
			{
				OnTabWndQueryItemColors(
					nSelIndex,
					false,
					false,
					ItemEnabledGet( nSelIndex ),
					NULL,
					NULL,
					NULL,
					&clrBkDark,
					NULL
					);
			}
			dc.FillSolidRect( 
				&rcColorLine, 
				clrBkDark != COLORREF( -1L )
					? clrBkDark
					: PmBridge_GetPM()->GetColor( COLOR_3DFACE, this )
				);
		}
		else 
		{
			dc.FillSolidRect( 
				&rcColorLine, 
				PmBridge_GetPM()->GetColor(
					SelectionGet() >= 0
						? COLOR_WINDOW
						: COLOR_3DFACE
						,
					this
					)
				);
		}
	} // if( ! rcTabNearBorderArea.IsRectEmpty() )
}

void CExtTabOneNoteWnd::GenerateItemBkColors(
	LONG nIndex,
	COLORREF * pclrBkLight, // = NULL,
	COLORREF * pclrBkDark // = NULL
	)
{
	ASSERT_VALID( this );
COLORREF clrBkLight = (COLORREF)(-1L);
COLORREF clrBkDark = (COLORREF)(-1L);
	if( nIndex >= 0 )
	{
		int nIndexBase = nIndex % 7;
		switch( nIndexBase ) 
		{
		case 0:
			clrBkDark = RGB(138, 168, 228);
			clrBkLight = RGB(221, 230, 247);
			break;
		case 1:
			clrBkDark = RGB(255, 216, 105);
			clrBkLight = RGB(255, 244, 213);
			break;
		case 2:
			clrBkDark = RGB(183, 201, 151);
			clrBkLight = RGB(234, 240, 226);
			break;
		case 3:
			clrBkDark = RGB(238, 149, 151);
			clrBkLight = RGB(249, 225, 226);
			break;
		case 4:
			clrBkDark = RGB(180, 158, 222);
			clrBkLight = RGB(234, 227, 245);
			break;
		case 5:
			clrBkDark = RGB(145, 186, 174);
			clrBkLight = RGB(224, 236, 232);
			break;
		case 6:
			clrBkDark = RGB(246, 176, 120);
			clrBkLight = RGB(252, 233, 217);
			break;
		case 7:
			clrBkDark = RGB(213, 164, 187);
			clrBkLight = RGB(243, 229, 236);
			break;
#ifdef _DEBUG
		default:
			{
				ASSERT( FALSE );
			}
			break;
#endif // _DEBUG
		} // switch( nIndexBase )
	} // if( nIndex >= 0 )
	else
	{
		clrBkDark =
			clrBkLight = 
			PmBridge_GetPM()->GetColor( COLOR_3DFACE, this );
	}
	if( pclrBkLight != NULL )
		*pclrBkLight = clrBkLight;
	if( pclrBkDark != NULL )
		*pclrBkDark = clrBkDark;
}

void CExtTabOneNoteWnd::OnTabWndQueryItemColors(
	LONG nItemIndex,
	bool bSelected,
	bool bHover,
	bool bEnabled,
	COLORREF * pclrBorderLight, // = NULL,
	COLORREF * pclrBorderDark, // = NULL,
	COLORREF * pclrBkLight, // = NULL,
	COLORREF * pclrBkDark, // = NULL,
	COLORREF * pclrText // = NULL
	)
{
	ASSERT_VALID( this );

COLORREF clrBorderLight = (COLORREF)(-1L);
COLORREF clrBorderDark = (COLORREF)(-1L);
COLORREF clrBkLight = (COLORREF)(-1L);
COLORREF clrBkDark = (COLORREF)(-1L);
COLORREF clrText = (COLORREF)(-1L);

	PmBridge_GetPM()->GetTabOneNoteItemColors(
		bSelected,
		bHover,
		bEnabled,
		clrBorderLight,
		clrBorderDark,
		clrBkLight,
		clrBkDark,
		clrText
		);

	if(		( (!bHover) || (!bEnabled) ) 
		&&	nItemIndex >= 0 
		)
	{
		TAB_ITEM_INFO_ONENOTE * pTii = 
			static_cast
				< TAB_ITEM_INFO_ONENOTE * >
				( ItemGet( nItemIndex ) );
		clrBkLight = pTii->GetColorBkLight();
		clrBkDark  = pTii->GetColorBkDark();
	}

	if( pclrBorderLight != NULL )
		*pclrBorderLight = clrBorderLight;
	if( pclrBorderDark != NULL )
		*pclrBorderDark = clrBorderDark;
	if( pclrBkLight != NULL )
		*pclrBkLight = clrBkLight;
	if( pclrBkDark != NULL )
		*pclrBkDark = clrBkDark;
	if( pclrText != NULL )
		*pclrText = clrText;
}

void CExtTabOneNoteWnd::OnTabWndDrawItem(
	CDC & dc,
	CRect & rcTabItemsArea,
	LONG nItemIndex,
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( pFont != NULL );
	ASSERT( pFont->GetSafeHandle() != NULL );
	stat_OnTabWndDrawItemOneNoteImpl(
		this,
		dc,
		rcTabItemsArea,
		nItemIndex,
		pTii,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		rcEntireItem,
		sizeTextMeasured,
		pFont,
		sText,
		pIcon
		);
}

void CExtTabOneNoteWnd::stat_OnTabWndDrawItemOneNoteImpl(
	CExtTabOneNoteWnd * pWndTabs,
	CDC & dc,
	CRect & rcTabItemsArea,
	LONG nItemIndex,
	TAB_ITEM_INFO * pTii,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon,
	COLORREF clrForceTabText, // = COLORREF(-1L)
	COLORREF clrForceTabBk1, // = COLORREF(-1L)
	COLORREF clrForceTabBk2 // = COLORREF(-1L)
	)
{
	ASSERT_VALID( pWndTabs );
	ASSERT_VALID( pTii );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( pFont != NULL );
	ASSERT( pFont->GetSafeHandle() != NULL );
	pWndTabs; rcTabItemsArea; sizeTextMeasured;
CExtCmdIcon * pIconTabItemCloseButton = pWndTabs->OnTabWndQueryItemCloseButtonShape( pTii );
CExtCmdIcon::e_paint_type_t ePaintStateITICB = (CExtCmdIcon::e_paint_type_t) pWndTabs->OnTabWndQueryItemCloseButtonPaintState( pTii );
CRect rcTabItemCloseButton( 0, 0, 0, 0 );
	if( pIconTabItemCloseButton != NULL )
		rcTabItemCloseButton = pTii->CloseButtonRectGet();
bool bEnabled = pTii->EnabledGet();
bool bHover = ( (!pWndTabs->_IsDND()) && pWndTabs->m_nHoverTrackingHitTest == nItemIndex ) ? true : false;
	if( (pTii->GetItemStyle() & __ETWI_CENTERED_TEXT) != 0 )
		bCenteredText = true;
bool bInGroupFirst = false;
	if( bGroupedMode )
	{
		TAB_ITEM_INFO * pTiiStart = pTii->GetInGroupFirst();
		ASSERT_VALID( pTiiStart );
		bInGroupFirst = ( nItemIndex == pWndTabs->ItemGetIndexOf( pTiiStart ) );
	}
bool bFirstVisible = true;
CExtTabWnd::TAB_ITEM_INFO * pTiiWalk = pTii->GetPrev();
	for( ; pTiiWalk != NULL; pTiiWalk = pTiiWalk->GetPrev() )
	{
		if( pTiiWalk->VisibleGet() )
		{
			bFirstVisible = false;
			break;
		}
	}
bool bFirstItem =
		(	bFirstVisible // nItemIndex == 0
		||	(	bInGroupFirst
			&&	nItemIndex != pWndTabs->SelectionGet()
			)
		);
CRect rcItem( rcEntireItem );
CRect rcItemRgn( rcItem );
POINT arrPointsBorders[11] =
		{	{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 },
		};
POINT arrPointsInnerArea[10] = 
		{	{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 },
		};
POINT arrPointsClipArea[13] =
		{	{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 },
		};
	if( bHorz )
	{
		if( bTopLeft )
		{
			rcItemRgn.InflateRect( nItemIndex == 0 ? 0 : -1, 0, 1, 0 );

			arrPointsBorders[0] = CPoint( rcItemRgn.right,		rcItemRgn.bottom );
			arrPointsBorders[1] = CPoint( rcItemRgn.right,		rcItemRgn.top + 2 );
			arrPointsBorders[2] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top );
			arrPointsBorders[3] = CPoint( rcItemRgn.left + 5,	rcItemRgn.top );
			arrPointsBorders[4] = CPoint( rcItemRgn.left + 4,	rcItemRgn.top + 1 );
			arrPointsBorders[5] = CPoint( rcItemRgn.left + 3,	rcItemRgn.top + 1 );
			arrPointsBorders[6] = CPoint( rcItemRgn.left + 2,	rcItemRgn.top + 2 );
			arrPointsBorders[7] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top + 2 );
			arrPointsBorders[8] = CPoint( rcItemRgn.left,		rcItemRgn.top + 3 );
			if( bSelected || bFirstItem )
			{
				arrPointsBorders[9] = CPoint( rcItemRgn.left - 15, rcItemRgn.bottom - 1 );
				arrPointsBorders[10] = CPoint( rcItemRgn.left - 16, rcItemRgn.bottom - 1 );
			}
			else
			{
				arrPointsBorders[9]= CPoint( rcItemRgn.left, rcItemRgn.bottom - 1 );
				arrPointsBorders[10]= arrPointsBorders[9];
			}
			if( bFirstVisible /*nItemIndex == 0*/ )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].x += 16;
			
			arrPointsInnerArea[0] = CPoint( rcItemRgn.right - 1,	rcItemRgn.bottom );
			arrPointsInnerArea[1] = CPoint( rcItemRgn.right - 1,	rcItemRgn.top + 2 );
			arrPointsInnerArea[2] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top + 1 );
			arrPointsInnerArea[3] = CPoint( rcItemRgn.left + 5,		rcItemRgn.top + 1 );
			arrPointsInnerArea[4] = CPoint( rcItemRgn.left + 4,		rcItemRgn.top + 2 );
			arrPointsInnerArea[5] = CPoint( rcItemRgn.left + 3,		rcItemRgn.top + 2 );
			arrPointsInnerArea[6] = CPoint( rcItemRgn.left + 2,		rcItemRgn.top + 3 );
			arrPointsInnerArea[7] = CPoint( rcItemRgn.left + 1,		rcItemRgn.top + 3 );
			arrPointsInnerArea[8] = CPoint( rcItemRgn.left,			rcItemRgn.top + 4 );
			if( bSelected || bFirstItem )
				arrPointsInnerArea[9] = CPoint( rcItemRgn.left - 14, rcItemRgn.bottom - 1 );
			else
				arrPointsInnerArea[9] = CPoint( rcItemRgn.left, rcItemRgn.bottom - 1 );
			if( bFirstVisible /*nItemIndex == 0*/ )
				for( int i = 3 ; i < sizeof(arrPointsInnerArea)/sizeof(arrPointsInnerArea[0]); i++ )
					arrPointsInnerArea[i].x += 16;

			arrPointsClipArea[0] = CPoint( rcItemRgn.right - 2,		rcItemRgn.bottom );
			arrPointsClipArea[1] = CPoint( rcItemRgn.right - 2,		rcItemRgn.top + 2 );
			arrPointsClipArea[2] = CPoint( rcItemRgn.left + 5,		rcItemRgn.top + 2 );
			arrPointsClipArea[3] = CPoint( rcItemRgn.left + 4,		rcItemRgn.top + 3 );
			arrPointsClipArea[4] = CPoint( rcItemRgn.left + 3,		rcItemRgn.top + 3 );
			arrPointsClipArea[5] = CPoint( rcItemRgn.left + 2,		rcItemRgn.top + 4 );
			arrPointsClipArea[6] = CPoint( rcItemRgn.left + 1,		rcItemRgn.top + 4 );
			arrPointsClipArea[7] = CPoint( rcItemRgn.left,			rcItemRgn.top + 5 );
			if( bSelected || bFirstItem )
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.left - 14,	rcItemRgn.bottom );
				arrPointsClipArea[9] = CPoint( rcItemRgn.left - 16,	rcItemRgn.bottom );
				arrPointsClipArea[10]= CPoint( rcItemRgn.left - 16,	rcItemRgn.bottom );
				arrPointsClipArea[11]= CPoint( rcItemRgn.right,		rcItemRgn.bottom );
				if( bSelected )
				{
					arrPointsClipArea[10].y++;
					arrPointsClipArea[11].y++;
				}
			}
			else
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.left,	rcItemRgn.bottom );
				arrPointsClipArea[11]= CPoint( rcItemRgn.right - 2, rcItemRgn.bottom );
				arrPointsClipArea[10] = arrPointsClipArea[9] = arrPointsClipArea[8];
			}
			arrPointsClipArea[12] = CPoint( rcItemRgn.right, rcItemRgn.bottom - 1 );
			if( bFirstVisible /*nItemIndex == 0*/ )
				for( int i = 2 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]) - 3; i++ )
					arrPointsClipArea[i].x += 16;
	
		} // if( bTopLeft )
		else
		{
			rcItemRgn.InflateRect( nItemIndex == 0 ? 0 : -1, 0, 0, 0 );

			arrPointsBorders[0] = CPoint( rcItemRgn.right,		rcItemRgn.top );
			arrPointsBorders[1] = CPoint( rcItemRgn.right,		rcItemRgn.bottom - 3 );
			arrPointsBorders[2] = CPoint( rcItemRgn.right - 3,	rcItemRgn.bottom );
			arrPointsBorders[3] = CPoint( rcItemRgn.left + 5,	rcItemRgn.bottom );
			arrPointsBorders[4] = CPoint( rcItemRgn.left + 4,	rcItemRgn.bottom - 1 );
			arrPointsBorders[5] = CPoint( rcItemRgn.left + 3,	rcItemRgn.bottom - 1 );
			arrPointsBorders[6] = CPoint( rcItemRgn.left + 2,	rcItemRgn.bottom - 2 );
			arrPointsBorders[7] = CPoint( rcItemRgn.left + 1,	rcItemRgn.bottom - 2 );
			arrPointsBorders[8] = CPoint( rcItemRgn.left - 1,	rcItemRgn.bottom - 4 );
			if( bSelected || bFirstItem )
			{
				arrPointsBorders[9] = CPoint( rcItemRgn.left - 16, rcItemRgn.top + 1 );
				arrPointsBorders[10] = CPoint( rcItemRgn.left - 17, rcItemRgn.top + 1 );
			}
			else
			{
				arrPointsBorders[9]= CPoint( rcItemRgn.left - 1, rcItemRgn.top + 1 );
				arrPointsBorders[10]= arrPointsBorders[9];
			}
			if( bFirstVisible /*nItemIndex == 0*/ )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].x += 16;
			
			arrPointsInnerArea[0] = CPoint( rcItemRgn.right - 1,	rcItemRgn.top );
			arrPointsInnerArea[1] = CPoint( rcItemRgn.right - 1,	rcItemRgn.bottom - 3 );
			arrPointsInnerArea[2] = CPoint( rcItemRgn.right - 3,	rcItemRgn.bottom - 1 );
			arrPointsInnerArea[3] = CPoint( rcItemRgn.left + 5,		rcItemRgn.bottom - 1 );
			arrPointsInnerArea[4] = CPoint( rcItemRgn.left + 4,		rcItemRgn.bottom - 2 );
			arrPointsInnerArea[5] = CPoint( rcItemRgn.left + 3,		rcItemRgn.bottom - 2 );
			arrPointsInnerArea[6] = CPoint( rcItemRgn.left + 2,		rcItemRgn.bottom - 3 );
			arrPointsInnerArea[7] = CPoint( rcItemRgn.left + 1,		rcItemRgn.bottom - 3 );
			arrPointsInnerArea[8] = CPoint( rcItemRgn.left - 1,		rcItemRgn.bottom - 5 );
			if( bSelected || bFirstItem )
				arrPointsInnerArea[9] = CPoint( rcItemRgn.left - 14, rcItemRgn.top + 1 );
			else
				arrPointsInnerArea[9] = CPoint( rcItemRgn.left - 1, rcItemRgn.top + 1 );
			if( bFirstVisible /*nItemIndex == 0*/ )
				for( int i = 3 ; i < sizeof(arrPointsInnerArea)/sizeof(arrPointsInnerArea[0]); i++ )
					arrPointsInnerArea[i].x += 16;

			arrPointsClipArea[0] = CPoint( rcItemRgn.right - 2,		rcItemRgn.top );
			arrPointsClipArea[1] = CPoint( rcItemRgn.right - 2,		rcItemRgn.bottom - 2 );
			arrPointsClipArea[2] = CPoint( rcItemRgn.left + 5,		rcItemRgn.bottom - 2 );
			arrPointsClipArea[3] = CPoint( rcItemRgn.left + 4,		rcItemRgn.bottom - 3 );
			arrPointsClipArea[4] = CPoint( rcItemRgn.left + 3,		rcItemRgn.bottom - 3 );
			arrPointsClipArea[5] = CPoint( rcItemRgn.left + 2,		rcItemRgn.bottom - 4 );
			arrPointsClipArea[6] = CPoint( rcItemRgn.left + 1,		rcItemRgn.bottom - 4 );
			arrPointsClipArea[7] = CPoint( rcItemRgn.left - 1,		rcItemRgn.bottom - 6 );
			if( bSelected || bFirstItem )
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.left - 15,	rcItemRgn.top );
				arrPointsClipArea[9] = CPoint( rcItemRgn.left - 16,	rcItemRgn.top + 1 );
				arrPointsClipArea[10]= CPoint( rcItemRgn.left - 16,	rcItemRgn.top + 1 );
				arrPointsClipArea[11]= CPoint( rcItemRgn.right,		rcItemRgn.top + 1 );
				if( bSelected )
				{
					arrPointsClipArea[10].y -= 1;
					arrPointsClipArea[11].y -= 1;
				}
			}
			else
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.left - 1, rcItemRgn.top + 1 );
				arrPointsClipArea[11]= CPoint( rcItemRgn.right, rcItemRgn.top + 1 );
				arrPointsClipArea[10] = arrPointsClipArea[9] = arrPointsClipArea[8];
			}
			arrPointsClipArea[12] = CPoint( rcItemRgn.right, rcItemRgn.top + 1 );
			if( bFirstVisible /*nItemIndex == 0*/ )
				for( int i = 2 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]) - 3; i++ )
					arrPointsClipArea[i].x += 16;

		} // else if( bTopLeft )
	} // if( bHorz )
	else
	{
		if( bTopLeft )
		{
			rcItemRgn.InflateRect( 0, nItemIndex == 0 ? 0 : -1, 0, 1 );

			arrPointsBorders[0] = CPoint( rcItemRgn.right,		rcItemRgn.bottom );
			arrPointsBorders[1] = CPoint( rcItemRgn.left + 3,	rcItemRgn.bottom );
			arrPointsBorders[2] = CPoint( rcItemRgn.left,		rcItemRgn.bottom - 3 );
			arrPointsBorders[3] = CPoint( rcItemRgn.left,		rcItemRgn.top + 5 );
			arrPointsBorders[4] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top + 4 );
			arrPointsBorders[5] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top + 3 );
			arrPointsBorders[6] = CPoint( rcItemRgn.left + 2,	rcItemRgn.top + 2 );
			arrPointsBorders[7] = CPoint( rcItemRgn.left + 2,	rcItemRgn.top + 1 );
			arrPointsBorders[8] = CPoint( rcItemRgn.left + 3,	rcItemRgn.top );
			if( bSelected || bFirstItem )
			{
				arrPointsBorders[9] = CPoint( rcItemRgn.right - 1, rcItemRgn.top - 16 );
				arrPointsBorders[10] = CPoint( rcItemRgn.right - 1, rcItemRgn.top - 17 );
			}
			else
			{
				arrPointsBorders[9] = CPoint( rcItemRgn.left + 3, rcItemRgn.top );
				arrPointsBorders[10]= CPoint( rcItemRgn.right, rcItemRgn.top );
			}
			if( bFirstVisible /*nItemIndex == 0*/ )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].y += 16;
			
			arrPointsInnerArea[0] = CPoint( rcItemRgn.right,	rcItemRgn.bottom - 1 );
			arrPointsInnerArea[1] = CPoint( rcItemRgn.left + 3,	rcItemRgn.bottom - 1 );
			arrPointsInnerArea[2] = CPoint( rcItemRgn.left + 1,	rcItemRgn.bottom - 3 );
			arrPointsInnerArea[3] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top + 5 );
			arrPointsInnerArea[4] = CPoint( rcItemRgn.left + 2,	rcItemRgn.top + 4 );
			arrPointsInnerArea[5] = CPoint( rcItemRgn.left + 2,	rcItemRgn.top + 3);
			arrPointsInnerArea[6] = CPoint( rcItemRgn.left + 3,	rcItemRgn.top + 2 );
			arrPointsInnerArea[7] = CPoint( rcItemRgn.left + 3,	rcItemRgn.top + 1 );
			arrPointsInnerArea[8] = CPoint( rcItemRgn.left + 4,	rcItemRgn.top );
			if( bSelected || bFirstItem )
				arrPointsInnerArea[9] = CPoint( rcItemRgn.right, rcItemRgn.top - 15 );
			else
				arrPointsInnerArea[9] = CPoint( rcItemRgn.right, rcItemRgn.top );
			if( bFirstVisible /*nItemIndex == 0*/ )
				for( int i = 3 ; i < sizeof(arrPointsInnerArea)/sizeof(arrPointsInnerArea[0]); i++ )
					arrPointsInnerArea[i].y += 16;

			arrPointsClipArea[0] = CPoint( rcItemRgn.right,		rcItemRgn.bottom - 2 );
			arrPointsClipArea[1] = CPoint( rcItemRgn.left + 3,	rcItemRgn.bottom - 2 );
			arrPointsClipArea[2] = CPoint( rcItemRgn.left + 2,	rcItemRgn.bottom - 3 );
			arrPointsClipArea[3] = CPoint( rcItemRgn.left + 2,	rcItemRgn.top + 5 );
			arrPointsClipArea[4] = CPoint( rcItemRgn.left + 3,	rcItemRgn.top + 4 );
			arrPointsClipArea[5] = CPoint( rcItemRgn.left + 3,	rcItemRgn.top + 3);
			arrPointsClipArea[6] = CPoint( rcItemRgn.left + 4,	rcItemRgn.top + 2 );
			arrPointsClipArea[7] = CPoint( rcItemRgn.left + 4,	rcItemRgn.top + 1 );
			if( bSelected || bFirstItem )
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.right,		rcItemRgn.top - 15 );
				arrPointsClipArea[9] = CPoint( rcItemRgn.right,		rcItemRgn.top - 16 );
				arrPointsClipArea[10]= CPoint( rcItemRgn.right,		rcItemRgn.top - 16 );
				arrPointsClipArea[11]= CPoint( rcItemRgn.right - 1,	rcItemRgn.bottom );
				if( bSelected )
				{
					arrPointsClipArea[10].x += 1;
					arrPointsClipArea[11].x += 1;
				}
	  			arrPointsClipArea[12]= arrPointsClipArea[11];
			}
			else
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.left + 5,	rcItemRgn.top );
				arrPointsClipArea[9] = CPoint( rcItemRgn.right,		rcItemRgn.top );
				arrPointsClipArea[10]= CPoint( rcItemRgn.right,		rcItemRgn.bottom );
				arrPointsClipArea[12]= 
					arrPointsClipArea[11] = 
					arrPointsClipArea[10];
			}
			if( bFirstVisible /*nItemIndex == 0*/ )
				for( int i = 3 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]) - 3; i++ )
					arrPointsClipArea[i].y += 16;

		} // if( bTopLeft )
		else
		{
			//rcItemRgn.InflateRect( 0, nItemIndex == 0 ? 0 : -1, 0, 0 );

			arrPointsBorders[0] = CPoint( rcItemRgn.left,		rcItemRgn.bottom );
			arrPointsBorders[1] = CPoint( rcItemRgn.right - 3,	rcItemRgn.bottom );
			arrPointsBorders[2] = CPoint( rcItemRgn.right,		rcItemRgn.bottom - 3 );
			arrPointsBorders[3] = CPoint( rcItemRgn.right,		rcItemRgn.top + 5 );
			arrPointsBorders[4] = CPoint( rcItemRgn.right - 1,	rcItemRgn.top + 4 );
			arrPointsBorders[5] = CPoint( rcItemRgn.right - 1,	rcItemRgn.top + 3 );
			arrPointsBorders[6] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top + 2 );
			arrPointsBorders[7] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top + 1 );
			arrPointsBorders[8] = CPoint( rcItemRgn.right - 3,	rcItemRgn.top );
			if( bSelected || bFirstItem )
			{
				arrPointsBorders[9] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top - 16 );
				arrPointsBorders[10]= CPoint( rcItemRgn.left + 1,	rcItemRgn.top - 17 );
			}
			else
			{
				arrPointsBorders[9] = CPoint( rcItemRgn.right - 3, rcItemRgn.top );
				arrPointsBorders[10]= CPoint( rcItemRgn.left, rcItemRgn.top );
			}
			if( bFirstVisible /*nItemIndex == 0*/ )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].y += 16;

			arrPointsInnerArea[0] = CPoint( rcItemRgn.left,			rcItemRgn.bottom - 1 );
			arrPointsInnerArea[1] = CPoint( rcItemRgn.right - 3,	rcItemRgn.bottom - 1 );
			arrPointsInnerArea[2] = CPoint( rcItemRgn.right - 1,	rcItemRgn.bottom - 3 );
			arrPointsInnerArea[3] = CPoint( rcItemRgn.right - 1,	rcItemRgn.top + 5 );
			arrPointsInnerArea[4] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top + 4 );
			arrPointsInnerArea[5] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top + 3);
			arrPointsInnerArea[6] = CPoint( rcItemRgn.right - 3,	rcItemRgn.top + 2 );
			arrPointsInnerArea[7] = CPoint( rcItemRgn.right - 3,	rcItemRgn.top + 1 );
			arrPointsInnerArea[8] = CPoint( rcItemRgn.right - 4,	rcItemRgn.top );
			if( bSelected || bFirstItem )
				arrPointsInnerArea[9] = CPoint( rcItemRgn.left + 1, rcItemRgn.top - 15 );
			else
				arrPointsInnerArea[9] = CPoint( rcItemRgn.left + 1, rcItemRgn.top );
			if( bFirstVisible /*nItemIndex == 0*/ )
				for( int i = 3 ; i < sizeof(arrPointsInnerArea)/sizeof(arrPointsInnerArea[0]); i++ )
					arrPointsInnerArea[i].y += 16;

			arrPointsClipArea[0] = CPoint( rcItemRgn.left + 1,	rcItemRgn.bottom - 2 );
			arrPointsClipArea[1] = CPoint( rcItemRgn.right - 3,	rcItemRgn.bottom - 2 );
			arrPointsClipArea[2] = CPoint( rcItemRgn.right - 2,	rcItemRgn.bottom - 3 );
			arrPointsClipArea[3] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top + 5 );
			arrPointsClipArea[4] = CPoint( rcItemRgn.right - 3,	rcItemRgn.top + 4 );
			arrPointsClipArea[5] = CPoint( rcItemRgn.right - 3,	rcItemRgn.top + 3);
			arrPointsClipArea[6] = CPoint( rcItemRgn.right - 4,	rcItemRgn.top + 2 );
			arrPointsClipArea[7] = CPoint( rcItemRgn.right - 4,	rcItemRgn.top + 1 );

			if( bSelected || bFirstItem )
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top - 14 );
				arrPointsClipArea[9] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top - 15 );
				arrPointsClipArea[10]= CPoint( rcItemRgn.left + 1,	rcItemRgn.top - 15 );
				arrPointsClipArea[11]= CPoint( rcItemRgn.left + 1,	rcItemRgn.bottom );
				if( bSelected )
				{
					arrPointsClipArea[10].x -= 1;
					arrPointsClipArea[11].x -= 1;
				}
	  			arrPointsClipArea[12]= arrPointsClipArea[11];
			}
			else
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.right - 5,	rcItemRgn.top );
				arrPointsClipArea[9] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top );
				arrPointsClipArea[10]= CPoint( rcItemRgn.left + 1,  rcItemRgn.bottom );
				arrPointsClipArea[12]= 
					arrPointsClipArea[11] = 
					arrPointsClipArea[10];
			}
			if( bFirstVisible /*nItemIndex == 0*/ )
				for( int i = 3 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]) - 3; i++ )
					arrPointsClipArea[i].y += 16;

		} // else if( bTopLeft )

	} // else if( bHorz )

COLORREF clrBorderLight = (COLORREF)(-1L);
COLORREF clrBorderDark = (COLORREF)(-1L);
COLORREF clrBkLight = (COLORREF)(-1L);
COLORREF clrBkDark = (COLORREF)(-1L);
COLORREF clrText = (COLORREF)(-1L);

	pWndTabs->OnTabWndQueryItemColors(
		nItemIndex,
		bSelected,
		bHover,
		bEnabled,
		&clrBorderLight,
		&clrBorderDark,
		&clrBkLight,
		&clrBkDark,
		&clrText
		);
	if( clrForceTabText != COLORREF(-1L) )
		clrText = clrForceTabText;
	if( clrForceTabBk1 != COLORREF(-1L) )
		clrBkLight = clrForceTabBk1;
	if( clrForceTabBk2 != COLORREF(-1L) )
		clrBkDark = clrForceTabBk2;

// draw item border 
CBrush brushBorders( clrBorderDark );
CBrush brushBordersInnerArea( clrBorderLight );
CRgn rgnBorders, rgnBordersInnerArea, rgnClipArea;
	VERIFY( rgnBorders.CreatePolygonRgn( arrPointsBorders, 11, ALTERNATE ) );
	VERIFY( rgnBordersInnerArea.CreatePolygonRgn( arrPointsInnerArea, 10, ALTERNATE ) );
	VERIFY( rgnClipArea.CreatePolygonRgn( arrPointsClipArea, 13, ALTERNATE ) );
	dc.FrameRgn( &rgnBorders, &brushBorders, 1, 1 );
	dc.FrameRgn( &rgnBordersInnerArea, &brushBordersInnerArea, 1, 1 );

	// fill item background 
CRect rcFill( rcItemRgn );
	rcFill.InflateRect(
		bHorz ? 20 :  0,
		bHorz ?  0 : 20,
		bHorz ? 20 :  0,
		bHorz ?  0 : 20
		);
	if( bTopLeft )
		rcFill.InflateRect(
			0,
			0,
			bHorz ? 0 : 1,
			bHorz ? 1 : 0
			);

	dc.SelectClipRgn( &rgnClipArea, RGN_AND );
	if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) > 8 )
	{
		CExtPaintManager::stat_PaintGradientRect(
			dc,
			&rcFill,
			bHorz 
				? ( bTopLeft ? clrBkDark : clrBkLight ) 
				: ( bTopLeft ? clrBkLight : clrBkDark ),
			bHorz 
				? ( bTopLeft ? clrBkLight : clrBkDark ) 
				: ( bTopLeft ? clrBkDark : clrBkLight ),
			bHorz
		);
	} // if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) > 8 )
	else 
	{
		dc.FillSolidRect( 
			&rcFill, 
			bSelected 
				? pWndTabs->PmBridge_GetPM()->GetColor( COLOR_WINDOW, pWndTabs )
				: pWndTabs->PmBridge_GetPM()->GetColor( COLOR_3DFACE, pWndTabs )
			);
	} // else from if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) > 8 )

	rcItem.DeflateRect(
		bHorz 
			? __EXTTAB_ONENOTE_INDENT_LEFT 
			: ( bTopLeft ? __EXTTAB_ONENOTE_INDENT_TOP * 2 : __EXTTAB_ONENOTE_INDENT_BOTTOM ),
		bHorz 
			? ( bTopLeft ? __EXTTAB_ONENOTE_INDENT_TOP : 0 ) 
			: __EXTTAB_ONENOTE_INDENT_LEFT,
		bHorz 
			? __EXTTAB_ONENOTE_INDENT_RIGHT 
			: 0,
		bHorz 
			? 0 
			: __EXTTAB_ONENOTE_INDENT_RIGHT
		);

	// first item indent
	if( bFirstVisible /*nItemIndex == 0*/ )
		rcItem.DeflateRect(
			bHorz ? 16 : 0,
			(!bHorz) ? 16 : 0,
			0,
			0
			);

bool bDrawIcon =
		(	pIcon != NULL 
		&&	(!pIcon->IsEmpty()) 
		&&	(pWndTabs->GetTabWndStyle()&__ETWS_HIDE_ICONS) == 0 
		) ? true : false;

	// if tab has no icons - increase left indent
	if( (!bDrawIcon) && (pWndTabs->GetTabWndStyle()&__ETWS_CENTERED_TEXT) == 0 )
		rcItem.DeflateRect(
			bHorz ? 4 : 0,
			0,
			0,
			0
			);

CSize _sizeIcon( 0, 0 );
	if( bDrawIcon )
	{
		_sizeIcon = pIcon->GetSize();
		ASSERT( _sizeIcon.cx > 0 && _sizeIcon.cy > 0 );
	}
CRect rcItemForIcon( rcItem );
	if(		bDrawIcon
		&&	_sizeIcon.cx > 0
		&&	_sizeIcon.cy > 0
		)
	{
		rcItemForIcon.right = rcItemForIcon.left + _sizeIcon.cx;
		rcItemForIcon.bottom = rcItemForIcon.top + _sizeIcon.cy;
		rcItemForIcon.OffsetRect(
			bHorz ? 0 : ((rcItem.Width() - _sizeIcon.cx) / 2),
			bHorz ? ((rcItem.Height() - _sizeIcon.cy) / 2) : 0
			);
		if( rcItemForIcon.left < rcItem.left )
			rcItemForIcon.left = rcItem.left;
		if( rcItemForIcon.right > rcItem.right )
			rcItemForIcon.right = rcItem.right;
		if( rcItemForIcon.top < rcItem.top )
			rcItemForIcon.top = rcItem.top;
		if( rcItemForIcon.bottom > rcItem.bottom )
			rcItemForIcon.bottom = rcItem.bottom;
	}

CExtSafeString sItemText( (sText == NULL) ? _T("") : sText );

CRect rcText(
		rcItem.left
			+	(	bHorz
					? (_sizeIcon.cx + ( ( _sizeIcon.cx > 0 ) ? 2*__EXTTAB_MARGIN_ICON2TEXT_X : 0 ) )
					: 0
				),
		rcItem.top 
			+	(	bHorz
					? 0
					: ( _sizeIcon.cy + ( ( _sizeIcon.cy > 0 ) ? __EXTTAB_MARGIN_ICON2TEXT_Y : 0 ) )
				),
		rcItem.right,
		rcItem.bottom
		);
	if( ! bHorz )
	{
		if( pIconTabItemCloseButton != NULL )
			rcText.bottom = min( rcText.bottom, rcTabItemCloseButton.top );
	} // if( !bHorz )
	else
	{
		if( pIconTabItemCloseButton != NULL )
			rcText.right = min( rcText.right, rcTabItemCloseButton.left );
	}

bool bDrawText = ( ( ! sItemText.IsEmpty() ) && rcText.Width() > 6 && rcText.Height() > 6 ) ? true : false;

INT nIconAlignment = CExtPaintManager::__ALIGN_HORIZ_LEFT | CExtPaintManager::__ALIGN_VERT_TOP;
	if( ( ! bDrawText) && ( ! ( bGroupedMode && ( ! bInGroupActive ) ) ) )
	{
		if( bCenteredText )
		{
			nIconAlignment = CExtPaintManager::__ALIGN_HORIZ_CENTER|CExtPaintManager::__ALIGN_VERT_CENTER;
			rcItemForIcon = rcItem;
		}
		else
			rcItemForIcon.OffsetRect(
				bHorz ? ( rcItem.Width() - _sizeIcon.cx ) / 2 : 0,
				bHorz ? 0 : ( rcItem.Height() - _sizeIcon.cy ) / 2
				);
	}

	if( bDrawIcon )
	{
		if(		(     bHorz   && rcItemForIcon.Width()  >= _sizeIcon.cx )
			||	( ( ! bHorz ) && rcItemForIcon.Height() >= _sizeIcon.cy )
			)
		{
			rcItemForIcon.OffsetRect(
				bHorz ? 4 : 0,
				bHorz ? 0 : 4
				);
			pWndTabs->PmBridge_GetPM()->PaintIcon(
				dc,
				bHorz,
				pIcon,
				rcItemForIcon,
				false,
				bEnabled,
				false,
				nIconAlignment
				);
		}
	}
	else
	{
		if( ! bHorz )
		{
			if( bDrawText )
				rcText.OffsetRect( 0, 4 );
		}
	}

	if( bDrawText )
	{ // if we have sense to paint text on tab item
		ASSERT( pFont != NULL );
		ASSERT( pFont->GetSafeHandle() != NULL );
		bool bNoPrefix = ( ( pWndTabs->GetTabWndStyleEx() & __ETWS_EX_NO_PREFIX ) != 0 ) ? true : false;
		COLORREF clrOldText = dc.SetTextColor( clrText );
		INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
		CFont * pOldFont = dc.SelectObject( pFont );
		UINT nFormat = DT_SINGLELINE|DT_VCENTER|DT_END_ELLIPSIS;
		CExtRichContentLayout::e_layout_orientation_t eLO = CExtRichContentLayout::__ELOT_NORMAL;
		if( ! bHorz )
			eLO = bInvertedVerticalMode ? CExtRichContentLayout::__ELOT_270_CW : CExtRichContentLayout::__ELOT_90_CW;
		if( bCenteredText )
			nFormat |= DT_CENTER;
		else
			nFormat |= DT_LEFT;
		if( bNoPrefix )
			nFormat |= DT_NOPREFIX;
		CExtRichContentLayout::stat_DrawText(
			CExtRichContentLayout::__ELFMT_AUTO_DETECT, eLO,
			dc.m_hDC, LPCTSTR(sItemText), sItemText.GetLength(), rcText, nFormat, 0
			);
		dc.SelectObject( pOldFont );
		dc.SetBkMode( nOldBkMode );
		dc.SetTextColor( clrOldText );
	} // if we have sense to paint text on tab item
	
	if( pIconTabItemCloseButton != NULL )
	{
		ASSERT( ! pIconTabItemCloseButton->IsEmpty() );
		if( dc.RectVisible( &rcTabItemCloseButton ) )
		{
			CRect _rcTabItemCloseButton = rcTabItemCloseButton;
			if( bHorz )
			{
				if( bTopLeft )
					_rcTabItemCloseButton.OffsetRect( 0, 1 );
			}
			else
			{
				if( bTopLeft )
					_rcTabItemCloseButton.OffsetRect( 1, 0 );
			}
			pIconTabItemCloseButton->Paint(
				g_PaintManager.GetPM(),
				dc.m_hDC,
				_rcTabItemCloseButton,
				ePaintStateITICB
				);
		}
	} // if( pIconTabItemCloseButton != NULL )

	dc.SelectClipRgn( NULL );
}

#if (!defined __EXT_MFC_NO_TABMDI_CTRL )

/////////////////////////////////////////////////////////////////////////////
// CExtTabMdiOneNoteWnd window

IMPLEMENT_DYNCREATE( CExtTabMdiOneNoteWnd, CExtTabOneNoteWnd );

CExtTabMdiOneNoteWnd::CExtTabMdiOneNoteWnd()
{
}

CExtTabMdiOneNoteWnd::~CExtTabMdiOneNoteWnd()
{
	ASSERT_VALID( this );
}

BEGIN_MESSAGE_MAP(CExtTabMdiOneNoteWnd, CExtTabOneNoteWnd)
	//{{AFX_MSG_MAP(CExtTabMdiOneNoteWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG

void CExtTabMdiOneNoteWnd::AssertValid() const
{
	CExtTabOneNoteWnd::AssertValid();
}

void CExtTabMdiOneNoteWnd::Dump(CDumpContext& dc) const
{
	CExtTabOneNoteWnd::Dump( dc );
}

#endif

void CExtTabMdiOneNoteWnd::OnMdiTabImplAdjustBorderSpaces()
{
	ASSERT_VALID( this );
DWORD dwWndStyle = GetStyle();
bool bInvisibleTab =
		( (dwWndStyle & WS_VISIBLE) == 0 ) ? true : false;
DWORD dwOrientation = OrientationGet();
	m_rcReservedBorderSpace.SetRect(
		(bInvisibleTab || dwOrientation == __ETWS_ORIENT_LEFT) ? 0 : __EXTTAB_MDI_NC_AREA_GAP_DX_L,
		(bInvisibleTab || dwOrientation == __ETWS_ORIENT_TOP) ? 0 : __EXTTAB_MDI_NC_AREA_GAP_DY_T,
		(bInvisibleTab || dwOrientation == __ETWS_ORIENT_RIGHT) ? 0 : __EXTTAB_MDI_NC_AREA_GAP_DX_R,
		(bInvisibleTab || dwOrientation == __ETWS_ORIENT_BOTTOM) ? 0 : __EXTTAB_MDI_NC_AREA_GAP_DY_B
		);
}

void CExtTabMdiOneNoteWnd::OnMdiTabImplDrawOuterBorder(
	CDC & dc,
	const CRect & rcOuterBorder,
	const CRect & rcMdiAreaClient,
	const CRect & rcMdiAreaWnd,
	HWND hWndHooked
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	rcMdiAreaWnd; hWndHooked;
	if( rcOuterBorder == rcMdiAreaClient )
		return;
CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * pTII =
		SelectionGetPtr();
	if( pTII == NULL )
		return;
	dc.FillSolidRect( &rcOuterBorder, pTII->GetColorBkDark() );
}

#endif // (!defined __EXT_MFC_NO_TABMDI_CTRL )

#endif // __EXT_MFC_NO_TAB_ONENOTE_CTRL


/////////////////////////////////////////////////////////////////////////////
// CExtTabWhidbeyWnd

#ifndef __EXT_MFC_NO_TAB_WHIDBEY_CTRL

IMPLEMENT_DYNCREATE( CExtTabWhidbeyWnd, CExtTabWnd );

#define __EXTTAB_WHIDBEY_INDENT_TOP		1
#define __EXTTAB_WHIDBEY_INDENT_BOTTOM	1
#define __EXTTAB_WHIDBEY_INDENT_LEFT	6
#define __EXTTAB_WHIDBEY_INDENT_RIGHT	6

CExtTabWhidbeyWnd::CExtTabWhidbeyWnd()
{
}

CExtTabWhidbeyWnd::~CExtTabWhidbeyWnd()
{
}

bool CExtTabWhidbeyWnd::_IsCustomLayoutTabWnd() const
{
	return true;
}

BOOL CExtTabWhidbeyWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
	UINT nDlgCtrlID, // = UINT( __EXT_MFC_IDC_STATIC )
	DWORD dwWindowStyle, // = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS
	DWORD dwTabStyle, // = __ETWS_WHIDBEY_DEFAULT
	CCreateContext * pContext // = NULL
	)
{
	ASSERT( pParentWnd != NULL );
	ASSERT( pParentWnd->GetSafeHwnd() != NULL );
	if(	! CExtTabWnd::Create( pParentWnd, rcWnd, nDlgCtrlID, dwWindowStyle, dwTabStyle, pContext ) )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	return TRUE;
}

void CExtTabWhidbeyWnd::PreSubclassWindow() 
{
	CExtTabWnd::PreSubclassWindow();
	if( m_bDirectCreateCall )
		return;
	ModifyTabWndStyle( 0xFFFFFFFF, __ETWS_WHIDBEY_DEFAULT );
}

void CExtTabWhidbeyWnd::OnTabWndMeasureItemAreaMargins(
	LONG & nSpaceBefore,
	LONG & nSpaceAfter,
	LONG & nSpaceOver
	)
{
	ASSERT_VALID( this );
	nSpaceBefore	= 1;
	nSpaceAfter		= 2;
	nSpaceOver		= 3;
}

void CExtTabWhidbeyWnd::OnTabWndUpdateItemMeasure(
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	CDC & dcMeasure,
	CSize & sizePreCalc
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	ASSERT( pTii->GetTabWnd() == this );
	dcMeasure;
bool bHorz = OrientationIsHorizontal();
	if( bHorz )
	{
		int  nIndent = ( __EXTTAB_WHIDBEY_INDENT_LEFT + __EXTTAB_WHIDBEY_INDENT_RIGHT );
		nIndent = g_PaintManager->UiScalingDo(nIndent, CExtPaintManager::__EUIST_X);

		sizePreCalc.cx += nIndent;
		if( pTii->GetIndexOf() == 0 )
			sizePreCalc.cx += 9;
		sizePreCalc.cy = 17;
	}
	else
	{
		int  nIndent = ( __EXTTAB_WHIDBEY_INDENT_LEFT + __EXTTAB_WHIDBEY_INDENT_RIGHT );
		nIndent = g_PaintManager->UiScalingDo(nIndent, CExtPaintManager::__EUIST_Y);

		sizePreCalc.cy += nIndent;
		if ( pTii->GetIndexOf() == 0 )
			sizePreCalc.cy += 9;
		sizePreCalc.cx = 17;
	}
	if ( !bHorz )
	{
		sizePreCalc.cx = g_PaintManager->UiScalingDo(sizePreCalc.cx, CExtPaintManager::__EUIST_Y);
		//sizePreCalc.cy = g_PaintManager->UiScalingDo(sizePreCalc.cy, CExtPaintManager::__EUIST_X);
	}
	else
	{
		//sizePreCalc.cx = g_PaintManager->UiScalingDo(sizePreCalc.cx, CExtPaintManager::__EUIST_X);
		sizePreCalc.cy = g_PaintManager->UiScalingDo(sizePreCalc.cy, CExtPaintManager::__EUIST_Y);
	}
}

void CExtTabWhidbeyWnd::OnTabWndEraseClientArea(
	CDC & dc,
	CRect & rcClient,
	CRect & rcTabItemsArea,
	CRect & rcTabNearBorderArea,
	DWORD dwOrientation,
	bool bGroupedMode
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	rcTabItemsArea;
	bGroupedMode;
	dc.FillSolidRect( 
		&rcClient, 
		PmBridge_GetPM()->GetColor( COLOR_3DLIGHT, this ) 
		);
	if( ! rcTabNearBorderArea.IsRectEmpty() )
	{
		CRect rcTabNearMargin( rcTabNearBorderArea ); // prepare tab border margin rect
		switch( dwOrientation )
		{
			case __ETWS_ORIENT_TOP:
				rcTabNearMargin.bottom = rcTabNearMargin.top + 1;
				rcTabNearMargin.OffsetRect(0,-1);
			break;
			case __ETWS_ORIENT_BOTTOM:
				rcTabNearMargin.top = rcTabNearMargin.bottom - 1;
				rcTabNearMargin.OffsetRect(0,1);
			break;
			case __ETWS_ORIENT_LEFT:
				rcTabNearMargin.right = rcTabNearMargin.left + 1;
			break;
			case __ETWS_ORIENT_RIGHT:
				rcTabNearMargin.left = rcTabNearMargin.right - 1;
			break;
			default:
				ASSERT( FALSE );
			break;
		} // switch( dwOrientation )
		// paint tab border margin
		dc.FillSolidRect( &rcTabNearMargin, PmBridge_GetPM()->GetColor( COLOR_3DSHADOW, this ) );
	} // if( ! rcTabNearBorderArea.IsRectEmpty() )
}

void CExtTabWhidbeyWnd::OnTabWndDrawItem(
	CDC & dc,
	CRect & rcTabItemsArea,
	LONG nItemIndex,
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( pFont != NULL );
	ASSERT( pFont->GetSafeHandle() != NULL );
	stat_OnTabWndDrawItemWhidbeyImpl(
		this,
		dc,
		rcTabItemsArea,
		nItemIndex,
		pTii,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		rcEntireItem,
		sizeTextMeasured,
		pFont,
		sText,
		pIcon
		);
}

void CExtTabWhidbeyWnd::stat_OnTabWndDrawItemWhidbeyImpl(
	CExtTabWhidbeyWnd * pWndTabs,
	CDC & dc,
	CRect & rcTabItemsArea,
	LONG nItemIndex,
	TAB_ITEM_INFO * pTii,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon,
	COLORREF clrForceTabText, // = COLORREF(-1L)
	COLORREF clrForceTabBk1, // = COLORREF(-1L)
	COLORREF clrForceTabBk2 // = COLORREF(-1L)
	)
{
	ASSERT_VALID( pWndTabs );
	ASSERT_VALID( pTii );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( pFont != NULL );
	ASSERT( pFont->GetSafeHandle() != NULL );
	pWndTabs; sizeTextMeasured;

bool bEnabled = pTii->EnabledGet();
bool bHover = ( (!pWndTabs->_IsDND()) && pWndTabs->m_nHoverTrackingHitTest == nItemIndex ) ? true : false;

	if( (pTii->GetItemStyle() & __ETWI_CENTERED_TEXT) != 0 )
		bCenteredText = true;

bool bInGroupFirst = false;
	if( bGroupedMode )
	{
		TAB_ITEM_INFO * pTiiStart = pTii->GetInGroupFirst();
		ASSERT_VALID( pTiiStart );
		bInGroupFirst = ( nItemIndex == pWndTabs->ItemGetIndexOf( pTiiStart ) );
	}
bool bFirstItem =
		(	nItemIndex == 0
		||	(	bInGroupFirst
			&&	nItemIndex != pWndTabs->SelectionGet()
			)
		);

CExtCmdIcon * pIconTabItemCloseButton = pWndTabs->OnTabWndQueryItemCloseButtonShape( pTii );
CExtCmdIcon::e_paint_type_t ePaintStateITICB = (CExtCmdIcon::e_paint_type_t) pWndTabs->OnTabWndQueryItemCloseButtonPaintState( pTii );
CRect rcTabItemCloseButton( 0, 0, 0, 0 );
	if( pIconTabItemCloseButton != NULL )
		rcTabItemCloseButton = pTii->CloseButtonRectGet();

CRect rcItem( rcEntireItem );
POINT arrPointsBorders[11] =
		{	{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 },
		};
POINT arrPointsClipArea[11] =
		{	{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 },
		};

	if( bHorz )
	{
		if( bTopLeft )
		{
			rcItem.OffsetRect( 0, 2 );

			arrPointsBorders[0] = CPoint( rcItem.right,		rcItem.bottom );
			arrPointsBorders[1] = CPoint( rcItem.right,		rcItem.top + 2 );
			arrPointsBorders[2] = CPoint( rcItem.right - 2,	rcItem.top );
			arrPointsBorders[3] = CPoint( rcItem.left + 9,	rcItem.top );
			arrPointsBorders[4] = CPoint( rcItem.left + 8,	rcItem.top + 1 );
			arrPointsBorders[5] = CPoint( rcItem.left + 7,	rcItem.top + 1 );
			arrPointsBorders[6] = CPoint( rcItem.left + 6,	rcItem.top + 2 );
			arrPointsBorders[7] = CPoint( rcItem.left + 5,	rcItem.top + 2 );
			arrPointsBorders[8] = CPoint( rcItem.left + 4,	rcItem.top + 3 );
			arrPointsBorders[9] = CPoint( rcItem.left,		rcItem.top + 7 );
			if( bSelected || bFirstItem )
				arrPointsBorders[10] = CPoint( rcItem.left - 10, rcItem.bottom );
			else
				arrPointsBorders[10] = CPoint( rcItem.left, rcItem.bottom );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].x += 9;

			arrPointsClipArea[0] = CPoint( rcItem.right,	rcItem.bottom );
			arrPointsClipArea[1] = CPoint( rcItem.right,	rcItem.top + 2 );
			arrPointsClipArea[2] = CPoint( rcItem.right - 2,rcItem.top );
			arrPointsClipArea[3] = CPoint( rcItem.left + 9,	rcItem.top );
			arrPointsClipArea[4] = CPoint( rcItem.left + 8,	rcItem.top + 2 );
			arrPointsClipArea[5] = CPoint( rcItem.left + 7,	rcItem.top + 2 );
			arrPointsClipArea[6] = CPoint( rcItem.left + 6,	rcItem.top + 3 );
			arrPointsClipArea[7] = CPoint( rcItem.left + 5,	rcItem.top + 3 );
			arrPointsClipArea[8] = CPoint( rcItem.left + 4,	rcItem.top + 4 );
			arrPointsClipArea[9] = CPoint( rcItem.left + 1,	rcItem.top + 7 );
			if( bSelected || bFirstItem )
				arrPointsClipArea[10] = CPoint( rcItem.left - 10, rcItem.bottom );
			else
				arrPointsClipArea[10] = CPoint( rcItem.left + 1, rcItem.bottom );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]); i++ )
					arrPointsClipArea[i].x += 9;
			if( bSelected )
			{
				arrPointsClipArea[0].y += 1;
				arrPointsClipArea[10].y += 1;
			}
		} // if( bTopLeft )
		else
		{
			arrPointsBorders[0] = CPoint( rcItem.right,		rcItem.top );
			arrPointsBorders[1] = CPoint( rcItem.right,		rcItem.bottom - 2 );
			arrPointsBorders[2] = CPoint( rcItem.right - 2,	rcItem.bottom );
			arrPointsBorders[3] = CPoint( rcItem.left + 9,	rcItem.bottom );
			arrPointsBorders[4] = CPoint( rcItem.left + 8,	rcItem.bottom - 1 );
			arrPointsBorders[5] = CPoint( rcItem.left + 7,	rcItem.bottom - 1 );
			arrPointsBorders[6] = CPoint( rcItem.left + 6,	rcItem.bottom - 2 );
			arrPointsBorders[7] = CPoint( rcItem.left + 5,	rcItem.bottom - 2 );
			arrPointsBorders[8] = CPoint( rcItem.left + 4,	rcItem.bottom - 3 );
			arrPointsBorders[9] = CPoint( rcItem.left,		rcItem.bottom - 7 );
			if( bSelected || bFirstItem )
				arrPointsBorders[10] = CPoint( rcItem.left - 10, rcItem.top );
			else
				arrPointsBorders[10] = CPoint( rcItem.left, rcItem.top );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].x += 9;
			
			arrPointsClipArea[0] = CPoint( rcItem.right,	rcItem.top + 1 );
			arrPointsClipArea[1] = CPoint( rcItem.right,	rcItem.bottom - 2 );
			arrPointsClipArea[2] = CPoint( rcItem.right - 2,rcItem.bottom );
			arrPointsClipArea[3] = CPoint( rcItem.left + 10,rcItem.bottom );
			arrPointsClipArea[4] = CPoint( rcItem.left + 9,	rcItem.bottom - 1 );
			arrPointsClipArea[5] = CPoint( rcItem.left + 8,	rcItem.bottom - 1 );
			arrPointsClipArea[6] = CPoint( rcItem.left + 7,	rcItem.bottom - 2 );
			arrPointsClipArea[7] = CPoint( rcItem.left + 6,	rcItem.bottom - 2 );
			arrPointsClipArea[8] = CPoint( rcItem.left + 5,	rcItem.bottom - 3 );
			arrPointsClipArea[9] = CPoint( rcItem.left + 1,	rcItem.bottom - 7 );
			if( bSelected || bFirstItem )
				arrPointsClipArea[10] = CPoint( rcItem.left - 9, rcItem.top + 1 );
			else
				arrPointsClipArea[10] = CPoint( rcItem.left + 1, rcItem.top + 1 );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]); i++ )
					arrPointsClipArea[i].x += 9;
			if( bSelected )
			{
				arrPointsClipArea[0].y -= 1;
				arrPointsClipArea[10].y -= 1;
			}
			if( !bSelected && bFirstItem )
				arrPointsClipArea[10].x += 1;

		} // else if( bTopLeft )
	} // if( bHorz )
	else
	{
		if( bTopLeft )
		{
			rcItem.OffsetRect( 3, 0 );

			arrPointsBorders[0] = CPoint( rcItem.right,		rcItem.bottom );
			arrPointsBorders[1] = CPoint( rcItem.left + 2,	rcItem.bottom );
			arrPointsBorders[2] = CPoint( rcItem.left,		rcItem.bottom - 2 );
			arrPointsBorders[3] = CPoint( rcItem.left,		rcItem.top + 9 );
			arrPointsBorders[4] = CPoint( rcItem.left + 1,	rcItem.top + 8 );
			arrPointsBorders[5] = CPoint( rcItem.left + 1,	rcItem.top + 7 );
			arrPointsBorders[6] = CPoint( rcItem.left + 2,	rcItem.top + 6 );
			arrPointsBorders[7] = CPoint( rcItem.left + 2,	rcItem.top + 5 );
			arrPointsBorders[8] = CPoint( rcItem.left + 3,	rcItem.top + 4 );
			arrPointsBorders[9] = CPoint( rcItem.left + 7,	rcItem.top );
			if( bSelected || bFirstItem )
				arrPointsBorders[10] = CPoint( rcItem.right, rcItem.top - 10 );
			else
				arrPointsBorders[10] = CPoint( rcItem.right, rcItem.top );

			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].y += 9;

			arrPointsClipArea[0] = CPoint( rcItem.right,	rcItem.bottom );
			arrPointsClipArea[1] = CPoint( rcItem.left + 2,	rcItem.bottom );
			arrPointsClipArea[2] = CPoint( rcItem.left + 1,	rcItem.bottom - 2 );
			arrPointsClipArea[3] = CPoint( rcItem.left + 1,	rcItem.top + 9 );
			arrPointsClipArea[4] = CPoint( rcItem.left + 2,	rcItem.top + 8 );
			arrPointsClipArea[5] = CPoint( rcItem.left + 2,	rcItem.top + 7 );
			arrPointsClipArea[6] = CPoint( rcItem.left + 3,	rcItem.top + 6 );
			arrPointsClipArea[7] = CPoint( rcItem.left + 3,	rcItem.top + 5 );
			arrPointsClipArea[8] = CPoint( rcItem.left + 4,	rcItem.top + 4 );
			arrPointsClipArea[9] = CPoint( rcItem.left + 7,	rcItem.top + 1 );
			if( bSelected || bFirstItem )
				arrPointsClipArea[10] = CPoint( rcItem.right, rcItem.top - 10 );
			else
				arrPointsClipArea[10] = CPoint( rcItem.right, rcItem.top );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]); i++ )
					arrPointsClipArea[i].y += 9;
			if( bSelected )
			{
				arrPointsClipArea[0].x += 1;
				arrPointsClipArea[10].x += 1;
			}
		
		} // if( bTopLeft )
		else
		{
			rcItem.OffsetRect( -1, 0 );

			arrPointsBorders[0] = CPoint( rcItem.left,		rcItem.bottom );
			arrPointsBorders[1] = CPoint( rcItem.right - 2,	rcItem.bottom );
			arrPointsBorders[2] = CPoint( rcItem.right,		rcItem.bottom - 2 );
			arrPointsBorders[3] = CPoint( rcItem.right,		rcItem.top + 9 );
			arrPointsBorders[4] = CPoint( rcItem.right - 1,	rcItem.top + 8 );
			arrPointsBorders[5] = CPoint( rcItem.right - 1,	rcItem.top + 7 );
			arrPointsBorders[6] = CPoint( rcItem.right - 2,	rcItem.top + 6 );
			arrPointsBorders[7] = CPoint( rcItem.right - 2,	rcItem.top + 5 );
			arrPointsBorders[8] = CPoint( rcItem.right - 3,	rcItem.top + 4 );
			arrPointsBorders[9] = CPoint( rcItem.right - 7,	rcItem.top );
			if( bSelected || bFirstItem )
				arrPointsBorders[10] = CPoint( rcItem.left, rcItem.top - 10 );
			else
				arrPointsBorders[10] = CPoint( rcItem.left, rcItem.top );

			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].y += 9;

			arrPointsClipArea[0] = CPoint( rcItem.left,		rcItem.bottom );
			arrPointsClipArea[1] = CPoint( rcItem.right - 2,rcItem.bottom );
			arrPointsClipArea[2] = CPoint( rcItem.right,	rcItem.bottom - 2 );
			arrPointsClipArea[3] = CPoint( rcItem.right,	rcItem.top + 9 );
			arrPointsClipArea[4] = CPoint( rcItem.right - 1,	rcItem.top + 8 );
			arrPointsClipArea[5] = CPoint( rcItem.right - 1,	rcItem.top + 7 );
			arrPointsClipArea[6] = CPoint( rcItem.right - 2,	rcItem.top + 6 );
			arrPointsClipArea[7] = CPoint( rcItem.right - 2,	rcItem.top + 5 );
			arrPointsClipArea[8] = CPoint( rcItem.right - 3,	rcItem.top + 4 );
			arrPointsClipArea[9] = CPoint( rcItem.right - 6,	rcItem.top + 1 );
			if( bSelected || bFirstItem )
				arrPointsClipArea[10] = CPoint( rcItem.left + 1, rcItem.top - 10 );
			else
				arrPointsClipArea[10] = CPoint( rcItem.left + 1, rcItem.top );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]); i++ )
					arrPointsClipArea[i].y += 9;
			if( bSelected )
			{
				arrPointsClipArea[0].x -= 1;
				arrPointsClipArea[10].x -= 1;
			}		
			if( !bSelected && bFirstItem )
				arrPointsClipArea[10].y += 1;
		} // else if( bTopLeft )

	} // else if( bHorz )

COLORREF clrBorder = (COLORREF)(-1L);
COLORREF clrBkLight = (COLORREF)(-1L);
COLORREF clrBkDark = (COLORREF)(-1L);
COLORREF clrText = (COLORREF)(-1L);

	pWndTabs->OnTabWndQueryItemColors(
		nItemIndex,
		bSelected,
		bHover,
		bEnabled,
		&clrBorder,
		&clrBkLight,
		&clrBkDark,
		&clrText
		);
	if( clrForceTabText != COLORREF(-1L) )
		clrText = clrForceTabText;
	if( clrForceTabBk1 != COLORREF(-1L) )
		clrBkLight = clrForceTabBk1;
	if( clrForceTabBk2 != COLORREF(-1L) )
		clrBkDark = clrForceTabBk2;

	// draw item border 
CPen pen(PS_SOLID, 1, clrBorder);
CPen * pOldPen = dc.SelectObject( &pen );
	for( int i = 0 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]) - 1; i++ )
	{
		dc.MoveTo( arrPointsBorders[i] );
		dc.LineTo( arrPointsBorders[i + 1] );
	}
	if( !bSelected )
	{
		dc.MoveTo( arrPointsBorders[10] );
		dc.LineTo( arrPointsBorders[0] );
	}
	dc.SelectObject( pOldPen );

CRgn rgnClipArea;
	VERIFY( rgnClipArea.CreatePolygonRgn( arrPointsClipArea, 11, ALTERNATE ) );

	// fill item background 
CRect rcFill( rcTabItemsArea );
	rcFill.DeflateRect(
		(!bHorz) ? ( bTopLeft ?  0 : -1 ) : -1,
		  bHorz  ? ( bTopLeft ?  3 :  0 ) : -1,
		(!bHorz) ? ( bTopLeft ? -1 :  4 ) : -1,
		  bHorz  ? ( bTopLeft ?  0 :  3 ) : -1
		);

	dc.SelectClipRgn( &rgnClipArea, RGN_AND );
	if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) > 8 )
	{
		CExtPaintManager::stat_PaintGradientRect(
			dc,
			&rcFill,
			bHorz 
				? ( bTopLeft ? clrBkDark : clrBkLight ) 
				: ( bTopLeft ? clrBkLight : clrBkDark ),
			bHorz 
				? ( bTopLeft ? clrBkLight : clrBkDark ) 
				: ( bTopLeft ? clrBkDark : clrBkLight ),
			bHorz
		);
	}
	else 
	{
		dc.FillSolidRect( 
			&rcFill, 
			bSelected 
				? pWndTabs->PmBridge_GetPM()->GetColor( COLOR_WINDOW, pWndTabs )
				: pWndTabs->PmBridge_GetPM()->GetColor( COLOR_3DFACE, pWndTabs )
			);
	}

	rcItem.DeflateRect(
		bHorz 
			? __EXTTAB_WHIDBEY_INDENT_LEFT 
			: ( bTopLeft ? __EXTTAB_WHIDBEY_INDENT_TOP * 2 : 0 ),
		bHorz 
			? ( bTopLeft ? __EXTTAB_WHIDBEY_INDENT_TOP : 0 ) 
			: __EXTTAB_WHIDBEY_INDENT_LEFT,
		bHorz 
			? __EXTTAB_WHIDBEY_INDENT_RIGHT 
			: __EXTTAB_WHIDBEY_INDENT_TOP,
		bHorz 
			? __EXTTAB_WHIDBEY_INDENT_BOTTOM 
			: __EXTTAB_WHIDBEY_INDENT_RIGHT
		);

	// first item indent
	if( nItemIndex == 0 )
		rcItem.DeflateRect(
			bHorz ? 9 : 0,
			!bHorz ? 9 : 0,
			0,
			0
			);

bool bDrawIcon = (		
			pIcon != NULL 
		&&	(!pIcon->IsEmpty()) 
		&&	(pWndTabs->GetTabWndStyle()&__ETWS_HIDE_ICONS) == 0 
		);

	// if tab has no icons - increase left indent
	if( !bDrawIcon && (pWndTabs->GetTabWndStyle()&__ETWS_CENTERED_TEXT) == 0 )
		rcItem.DeflateRect(
			bHorz ? 6 : 0,
			0,
			0,
			0
			);

	CSize _sizeIcon( 0, 0 );
	if( bDrawIcon )
	{
		_sizeIcon = pIcon->GetSize();
		ASSERT( _sizeIcon.cx > 0 && _sizeIcon.cy > 0 );
	}
CRect rcItemForIcon( rcItem );
	if(		bDrawIcon
		&&	_sizeIcon.cx > 0
		&&	_sizeIcon.cy > 0
		)
	{
		rcItemForIcon.right = rcItemForIcon.left + _sizeIcon.cx;
		rcItemForIcon.bottom = rcItemForIcon.top + _sizeIcon.cy;
		rcItemForIcon.OffsetRect(
			bHorz ? 0 : ((rcItem.Width() - _sizeIcon.cx) / 2),
			bHorz ? ((rcItem.Height() - _sizeIcon.cy) / 2) : 0
			);
		if( rcItemForIcon.left < rcItem.left )
			rcItemForIcon.left = rcItem.left;
		if( rcItemForIcon.right > rcItem.right )
			rcItemForIcon.right = rcItem.right;
		if( rcItemForIcon.top < rcItem.top )
			rcItemForIcon.top = rcItem.top;
		if( rcItemForIcon.bottom > rcItem.bottom )
			rcItemForIcon.bottom = rcItem.bottom;
	}

	CExtSafeString sItemText( (sText == NULL) ? _T("") : sText );

	// IMPORTANT:  the rcText calculation fixed by Genka
	CRect rcText(
		rcItem.left
			+	(	bHorz
					? (_sizeIcon.cx +
						((_sizeIcon.cx > 0) ? 2*__EXTTAB_MARGIN_ICON2TEXT_X : 0)
						)
					: 0
				),
		rcItem.top 
			+	(	bHorz
					? 0
					: (_sizeIcon.cy +
						((_sizeIcon.cy > 0) ? __EXTTAB_MARGIN_ICON2TEXT_Y : 0)
						)
				),
		rcItem.right,
		rcItem.bottom
		);
	if( !bHorz )
	{
		if( pIconTabItemCloseButton != NULL )
			rcText.bottom = min( rcText.bottom, rcTabItemCloseButton.top );
	} // if( !bHorz )
	else
	{
		if( pIconTabItemCloseButton != NULL )
			rcText.right = min( rcText.right, rcTabItemCloseButton.left );
	}

bool bDrawText = ( ( ! sItemText.IsEmpty() ) && rcText.Width() > 6 && rcText.Height() > 6 ) ? true : false;

INT nIconAlignment = CExtPaintManager::__ALIGN_HORIZ_LEFT | CExtPaintManager::__ALIGN_VERT_TOP;
	if( (!bDrawText) && !( bGroupedMode && (!bInGroupActive) ) )
	{
		if( bCenteredText )
		{
			nIconAlignment = CExtPaintManager::__ALIGN_HORIZ_CENTER|CExtPaintManager::__ALIGN_VERT_CENTER;
			rcItemForIcon = rcItem;
		}
		else
			rcItemForIcon.OffsetRect(
				bHorz ? (rcItem.Width() - _sizeIcon.cx)/2 : 0,
				bHorz ? 0 : (rcItem.Height() - _sizeIcon.cy)/2
				);
	}

	if( bDrawIcon )
	{
		if(		(bHorz && rcItemForIcon.Width() >= _sizeIcon.cx )
			||	(!bHorz && rcItemForIcon.Height() >= _sizeIcon.cy)
			)
		{
			rcItemForIcon.OffsetRect(
				bHorz ? 4 : 0,
				bHorz ? 0 : 4
				);
			pWndTabs->PmBridge_GetPM()->PaintIcon(
				dc,
				bHorz,
				pIcon,
				rcItemForIcon,
				false,
				bEnabled,
				false,
				nIconAlignment
				);
		}
	}

	if( bDrawText )
	{ // if we have sense to paint text on tab item
		ASSERT( pFont != NULL );
		ASSERT( pFont->GetSafeHandle() != NULL );
		bool bNoPrefix = ( ( pWndTabs->GetTabWndStyleEx() & __ETWS_EX_NO_PREFIX ) != 0 ) ? true : false;
		COLORREF clrOldText = dc.SetTextColor( clrText );
		INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
		CFont * pOldFont = dc.SelectObject( pFont );
		UINT nFormat = DT_SINGLELINE|DT_VCENTER|DT_NOCLIP|DT_END_ELLIPSIS;
		CExtRichContentLayout::e_layout_orientation_t eLO = CExtRichContentLayout::__ELOT_NORMAL;
		if( ! bHorz )
			eLO = bInvertedVerticalMode ? CExtRichContentLayout::__ELOT_270_CW : CExtRichContentLayout::__ELOT_90_CW;
		if( ( ! bHorz ) && bInvertedVerticalMode )
			rcText.InflateRect( 2, 0 );
		if( bCenteredText )
			nFormat |= DT_CENTER;
		else
			nFormat |= DT_LEFT;
		if( bNoPrefix )
			nFormat |= DT_NOPREFIX;
		CExtRichContentLayout::stat_DrawText(
			CExtRichContentLayout::__ELFMT_AUTO_DETECT, eLO,
			dc.m_hDC, LPCTSTR(sItemText), sItemText.GetLength(), rcText, nFormat, 0
			);
		dc.SelectObject( pOldFont );
		dc.SetBkMode( nOldBkMode );
		dc.SetTextColor( clrOldText );
	} // if we have sense to paint text on tab item

	if( pIconTabItemCloseButton != NULL )
	{
		ASSERT( ! pIconTabItemCloseButton->IsEmpty() );
		if( dc.RectVisible( &rcTabItemCloseButton ) )
		{
			CRect _rcTabItemCloseButton = rcTabItemCloseButton;
			if( bHorz )
			{
				if( bTopLeft )
					_rcTabItemCloseButton.OffsetRect( 0, 3 );
			}
			else
			{
				if( bTopLeft )
					_rcTabItemCloseButton.OffsetRect( 4, 0 );
			}
			pIconTabItemCloseButton->Paint(
				g_PaintManager.GetPM(),
				dc.m_hDC,
				_rcTabItemCloseButton,
				ePaintStateITICB
				);
		}
	} // if( pIconTabItemCloseButton != NULL )

	dc.SelectClipRgn( NULL );
}

void CExtTabWhidbeyWnd::OnTabWndQueryItemColors(
	LONG nItemIndex,
	bool bSelected,
	bool bHover,
	bool bEnabled,
	COLORREF * pclrBorder, // = NULL,
	COLORREF * pclrBkLight, // = NULL,
	COLORREF * pclrBkDark, // = NULL,
	COLORREF * pclrText // = NULL
	)
{
	ASSERT_VALID( this );
	bHover;
	nItemIndex;

	COLORREF clrBorder = (COLORREF)(-1L);
	COLORREF clrBkLight = (COLORREF)(-1L);
	COLORREF clrBkDark = (COLORREF)(-1L);
	COLORREF clrText = (COLORREF)(-1L);

	PmBridge_GetPM()->GetTabWhidbeyItemColors(
		bSelected,
		bHover,
		bEnabled,
		clrBorder,
		clrBkLight,
		clrBkDark,
		clrText
		);
	
	if( pclrBorder != NULL )
		*pclrBorder = clrBorder;
	if( pclrBkLight != NULL )
		*pclrBkLight = clrBkLight;
	if( pclrBkDark != NULL )
		*pclrBkDark = clrBkDark;
	if( pclrText != NULL )
		*pclrText = clrText;
}


#if (!defined __EXT_MFC_NO_TABMDI_CTRL )

/////////////////////////////////////////////////////////////////////////////
// CExtTabMdiWhidbeyWnd window

IMPLEMENT_DYNCREATE( CExtTabMdiWhidbeyWnd, CExtTabWhidbeyWnd );

CExtTabMdiWhidbeyWnd::CExtTabMdiWhidbeyWnd()
{
}

CExtTabMdiWhidbeyWnd::~CExtTabMdiWhidbeyWnd()
{
}

BEGIN_MESSAGE_MAP(CExtTabMdiWhidbeyWnd, CExtTabWhidbeyWnd)
	//{{AFX_MSG_MAP(CExtTabMdiWhidbeyWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG

void CExtTabMdiWhidbeyWnd::AssertValid() const
{
	CExtTabWhidbeyWnd::AssertValid();
}

void CExtTabMdiWhidbeyWnd::Dump(CDumpContext& dc) const
{
	CExtTabWhidbeyWnd::Dump( dc );
}

#endif // _DEBUG

#endif // (!defined __EXT_MFC_NO_TABMDI_CTRL )

#endif // __EXT_MFC_NO_TAB_WHIDBEY_CTRL

#endif // (!defined __EXT_MFC_NO_TAB_CTRL )







#if (!defined __EXT_MFC_NO_DYNAMIC_BAR_SITE)

#if (!defined __EXT_MFC_NO_TABMDI_CTRL)

void CExtTMDBS < CExtTabMdiWnd > :: OnTabWndDrawItem(
	CDC & dc,
	CRect & rcTabItemsArea,
	LONG nItemIndex,
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( pFont != NULL );
	ASSERT( pFont->GetSafeHandle() != NULL );

//bool bEnabled = pTii->EnabledGet();
//bool bHover = ( (!_IsDND()) && m_nHoverTrackingHitTest == nItemIndex ) ? true : false;
//	bEnabled;
// 	bHover;

	if( (pTii->GetItemStyle() & __ETWI_CENTERED_TEXT) != 0 )
		bCenteredText = true;

COLORREF clrText = COLORREF(-1L), clrBk = COLORREF(-1L);
HWND hWnd = (HWND) ItemLParamGet( nItemIndex );
	if(		hWnd != NULL
		&&	::IsWindow( hWnd )
		)
	{
		CWnd * pWnd = CWnd::FromHandlePermanent( hWnd );
		if(		pWnd != NULL
			&&	pWnd->IsKindOf( RUNTIME_CLASS( CExtDynamicMDIChildWnd ) )
			)
		{
			hWnd = ::GetWindow( hWnd, GW_CHILD );
			if( hWnd != NULL )
			{
				CExtDynamicBarSite * pDBS = CExtDynamicBarSite::FindBarSite( pWnd );
				if( pDBS != NULL )
				{
					CExtDynamicControlBar * pBar = pDBS->BarFindByChildHWND( hWnd );
					if( pBar != NULL )
					{
						ASSERT_VALID( pBar );
						bool bFlashCaptionHighlightedState = false;
						if( pBar->FlashCaptionIsInProgress( &bFlashCaptionHighlightedState ) )
						{
							if( bFlashCaptionHighlightedState )
							{
								clrText = pBar->m_clrFlashCaptionText;
								clrBk = pBar->m_clrFlashCaptionBackground;
								bSelected = true;
							}
						}
					}
				}
			}
		}
	}

	PmBridge_GetPM()->PaintTabItem(
		dc,
		rcTabItemsArea,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		rcEntireItem,
		sizeTextMeasured,
		pFont,
		sText,
		pIcon,
		this,
		nItemIndex,
		clrText,
		clrBk
		);
}

#if (!defined __EXT_MFC_NO_TAB_ONENOTE_CTRL)

void CExtTMDBS < CExtTabMdiOneNoteWnd > :: OnTabWndDrawItem(
	CDC & dc,
	CRect & rcTabItemsArea,
	LONG nItemIndex,
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( pFont != NULL );
	ASSERT( pFont->GetSafeHandle() != NULL );

COLORREF clrText = COLORREF(-1L), clrBk1 = COLORREF(-1L), clrBk2 = COLORREF(-1L);
HWND hWnd = (HWND) ItemLParamGet( nItemIndex );
	if(		hWnd != NULL
		&&	::IsWindow( hWnd )
		)
	{
		CWnd * pWnd = CWnd::FromHandlePermanent( hWnd );
		if(		pWnd != NULL
			&&	pWnd->IsKindOf( RUNTIME_CLASS( CExtDynamicMDIChildWnd ) )
			)
		{
			hWnd = ::GetWindow( hWnd, GW_CHILD );
			if( hWnd != NULL )
			{
				CExtDynamicBarSite * pDBS = CExtDynamicBarSite::FindBarSite( pWnd );
				if( pDBS != NULL )
				{
					CExtDynamicControlBar * pBar = pDBS->BarFindByChildHWND( hWnd );
					if( pBar != NULL )
					{
						ASSERT_VALID( pBar );
						bool bFlashCaptionHighlightedState = false;
						if( pBar->FlashCaptionIsInProgress( &bFlashCaptionHighlightedState ) )
						{
							if( bFlashCaptionHighlightedState )
							{
								clrText = pBar->m_clrFlashCaptionText;
								clrBk1 = CExtPaintManager::stat_HLS_Adjust( pBar->m_clrFlashCaptionBackground, 0.0, +0.25, 0.0 );
								clrBk2 = CExtPaintManager::stat_HLS_Adjust( pBar->m_clrFlashCaptionBackground, 0.0, -0.25, 0.0 );
								//bSelected = true;
							}
						}
					}
				}
			}
		}
	}

	stat_OnTabWndDrawItemOneNoteImpl(
		this,
		dc,
		rcTabItemsArea,
		nItemIndex,
		pTii,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		rcEntireItem,
		sizeTextMeasured,
		pFont,
		sText,
		pIcon,
		clrText,
		clrBk1,
		clrBk2
		);
}

#endif // (!defined __EXT_MFC_NO_TAB_ONENOTE_CTRL )

#if (!defined __EXT_MFC_NO_TAB_WHIDBEY_CTRL)

void CExtTMDBS < CExtTabMdiWhidbeyWnd > :: OnTabWndDrawItem(
	CDC & dc,
	CRect & rcTabItemsArea,
	LONG nItemIndex,
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( pFont != NULL );
	ASSERT( pFont->GetSafeHandle() != NULL );

COLORREF clrText = COLORREF(-1L), clrBk1 = COLORREF(-1L), clrBk2 = COLORREF(-1L);
HWND hWnd = (HWND) ItemLParamGet( nItemIndex );
	if(		hWnd != NULL
		&&	::IsWindow( hWnd )
		)
	{
		CWnd * pWnd = CWnd::FromHandlePermanent( hWnd );
		if(		pWnd != NULL
			&&	pWnd->IsKindOf( RUNTIME_CLASS( CExtDynamicMDIChildWnd ) )
			)
		{
			hWnd = ::GetWindow( hWnd, GW_CHILD );
			if( hWnd != NULL )
			{
				CExtDynamicBarSite * pDBS = CExtDynamicBarSite::FindBarSite( pWnd );
				if( pDBS != NULL )
				{
					CExtDynamicControlBar * pBar = pDBS->BarFindByChildHWND( hWnd );
					if( pBar != NULL )
					{
						ASSERT_VALID( pBar );
						bool bFlashCaptionHighlightedState = false;
						if( pBar->FlashCaptionIsInProgress( &bFlashCaptionHighlightedState ) )
						{
							if( bFlashCaptionHighlightedState )
							{
								clrText = pBar->m_clrFlashCaptionText;
								clrBk1 = CExtPaintManager::stat_HLS_Adjust( pBar->m_clrFlashCaptionBackground, 0.0, +0.25, 0.0 );
								clrBk2 = CExtPaintManager::stat_HLS_Adjust( pBar->m_clrFlashCaptionBackground, 0.0, -0.25, 0.0 );
								//bSelected = true;
							}
						}
					}
				}
			}
		}
	}

	stat_OnTabWndDrawItemWhidbeyImpl(
		this,
		dc,
		rcTabItemsArea,
		nItemIndex,
		pTii,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		rcEntireItem,
		sizeTextMeasured,
		pFont,
		sText,
		pIcon,
		clrText,
		clrBk1,
		clrBk2
		);
}

#endif // (!defined __EXT_MFC_NO_TAB_WHIDBEY_CTRL )

#endif // (!defined __EXT_MFC_NO_TABMDI_CTRL )

#endif // (!defined __EXT_MFC_NO_DYNAMIC_BAR_SITE )





#if (!defined __EXT_MFC_NO_TAB_CTRL)

/////////////////////////////////////////////////////////////////////////////
// CExtThemedTabItem

CExtThemedTabItem::CExtThemedTabItem()
	: m_dwState( 0 )
	, m_nImage( -1 )
	, m_lParam( 0L )
	, m_rcItemOriginal( 0, 0, 0, 0 )
	, m_rcItemAdjusted( 0, 0, 0, 0 )
	, m_rcSeparator( 0, 0, 0, 0 )
	, m_nItemIndex( -1 )
	, m_bHaveItemData( false )
	, m_bSelected( false )
	, m_pIcon(NULL)
{
}

CExtThemedTabItem::CExtThemedTabItem( const CExtThemedTabItem & other )
	: m_dwState( other.m_dwState )
	, m_nImage( other.m_nImage )
	, m_lParam( other.m_lParam )
	, m_strText( other.m_strText )
	, m_rcItemOriginal( other.m_rcItemOriginal )
	, m_rcItemAdjusted( other.m_rcItemAdjusted )
	, m_rcSeparator( other.m_rcSeparator )
	, m_nItemIndex( other.m_nItemIndex )
	, m_bHaveItemData( other.m_bHaveItemData )
	, m_bSelected( other.m_bSelected )
	, m_pIcon(other.m_pIcon)
{
}

CExtThemedTabItem::CExtThemedTabItem( const CTabCtrl & wndTabs, INT nItemIndex, INT nItemCount, INT nSel )
	: m_dwState( 0 )
	, m_nImage( -1 )
	, m_lParam( 0L )
	, m_rcItemOriginal( 0, 0, 0, 0 )
	, m_rcItemAdjusted( 0, 0, 0, 0 )
	, m_rcSeparator( 0, 0, 0, 0 )
	, m_nItemIndex( -1 )
	, m_bHaveItemData( false )
	, m_bSelected( false )
	, m_pIcon(NULL)
{
	m_bHaveItemData = GetItem( wndTabs, nItemIndex, nItemCount, nSel );
}

CExtThemedTabItem & CExtThemedTabItem :: operator = ( const CExtThemedTabItem & other )
{
	if ( this == &other )
		return *this;

	m_dwState = other.m_dwState;
	m_nImage = other.m_nImage;
	m_lParam = other.m_lParam;
	m_strText = other.m_strText;
	m_rcItemOriginal = other.m_rcItemOriginal;
	m_rcItemAdjusted = other.m_rcItemAdjusted;
	m_rcSeparator = other.m_rcSeparator;
	m_nItemIndex = other.m_nItemIndex;
	m_bHaveItemData = other.m_bHaveItemData;
	m_bSelected = other.m_bSelected;
	if ( m_pIcon != NULL )
		delete m_pIcon;
	m_pIcon = NULL;
	if ( other.m_pIcon != NULL )
	{
		m_pIcon = new CExtCmdIcon;
		*m_pIcon = *other.m_pIcon;
	}
	return (*this);
}

//////////////////////////////////////////////////////////////////////////
CExtThemedTabItem::~CExtThemedTabItem()
{
	Empty();
}
#if _MFC_VER >= 0xC00 
//////////////////////////////////////////////////////////////////////////
CExtThemedTabItem::CExtThemedTabItem(CExtThemedTabItem&& other)
	: CExtThemedTabItem()
{
	//call move operator
	*this = std::move(other);
}

//////////////////////////////////////////////////////////////////////////
CExtThemedTabItem& CExtThemedTabItem::operator=(CExtThemedTabItem&& other)
{
	if ( this == &other )
		return *this;

	Empty();

	m_dwState = other.m_dwState;
	m_nImage = other.m_nImage;
	m_lParam = other.m_lParam;
	m_strText = other.m_strText;
	m_rcItemOriginal = other.m_rcItemOriginal;
	m_rcItemAdjusted = other.m_rcItemAdjusted;
	m_rcSeparator = other.m_rcSeparator;
	m_nItemIndex = other.m_nItemIndex;
	m_bHaveItemData = other.m_bHaveItemData;
	m_bSelected = other.m_bSelected;
	m_pIcon = other.m_pIcon;

	other.m_dwState = 0;
	other.m_nImage = -1;
	other.m_lParam = 0L;
	other.m_rcItemOriginal.SetRect(0, 0, 0, 0);
	other.m_rcItemAdjusted.SetRect(0, 0, 0, 0);
	other.m_rcSeparator.SetRect(0, 0, 0, 0);
	other.m_nItemIndex = -1;
	other.m_strText.Empty();
	other.m_bHaveItemData = false;
	other.m_bSelected = false;
	other.m_pIcon = NULL;

	return *this;
}
#endif
bool CExtThemedTabItem::IsEmpty() const
{
	return m_bHaveItemData ? false : true;
}

void CExtThemedTabItem::Empty()
{
	m_dwState = 0;
	m_nImage = -1;
	m_lParam = 0L;
	m_rcItemOriginal.SetRect( 0, 0, 0, 0 );
	m_rcItemAdjusted.SetRect( 0, 0, 0, 0 );
	m_rcSeparator.SetRect( 0, 0, 0, 0 );
	m_nItemIndex = -1;
	m_strText.Empty();
	m_bHaveItemData = false;
	m_bSelected = false;
	if ( m_pIcon != NULL )
		delete m_pIcon;
	m_pIcon = NULL;
}

bool CExtThemedTabItem::GetItem( const CTabCtrl & wndTabs, INT nItemIndex, INT nItemCount, INT nSel )
{
	Empty();
	if( nItemCount < 0 || nItemIndex < 0 || nItemIndex >= nItemCount || wndTabs.GetSafeHwnd() == NULL )
		return false;
	ASSERT( nItemCount == wndTabs.GetItemCount() );
	if( ! wndTabs.GetItemRect( nItemIndex, &m_rcItemOriginal ) )
		m_rcItemOriginal.SetRect( 0, 0, 0, 0 );
	else
		m_rcItemAdjusted = m_rcItemOriginal;
	m_nItemIndex = nItemIndex;
	TCITEM _tcitem[20];
	::memset( &_tcitem, 0, sizeof(_tcitem) );
	_tcitem[0].mask = TCIF_TEXT|TCIF_IMAGE|TCIF_PARAM|TCIF_STATE; //|TCIF_RTLREADING
	_tcitem[0].pszText = m_strText.GetBuffer( 1024+1 );
	if( _tcitem[0].pszText != NULL )
		_tcitem[0].cchTextMax = 1024;
	else
		_tcitem[0].mask &= ~TCIF_TEXT;
	BOOL bHaveItem = wndTabs.GetItem( nItemIndex, &_tcitem[0] );
	if( _tcitem[0].pszText != NULL )
		m_strText.ReleaseBuffer();
	if( ! bHaveItem )
	{
		Empty();
		return false;
	}
	m_dwState = _tcitem[0].dwState;
	m_nImage = _tcitem[0].iImage;
	if ( m_pIcon == NULL && m_nImage != -1 )
	{
		m_pIcon = new CExtCmdIcon;
		m_pIcon->AssignFromHICON(wndTabs.GetImageList()->ExtractIcon(m_nImage), false);
	}
	m_lParam = _tcitem[0].lParam;
	if( nSel == nItemIndex )
		m_bSelected = true;
	m_bHaveItemData = true;
	return true;
}

bool CExtThemedTabItem::DoPaint(
	const CTabCtrl & wndTabs,
	CDC & dc,
	CExtPaintManager * pPM,
	CFont * pFont,
	CRect rcClient
	) const
{
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT_VALID( pPM );
	ASSERT( pFont->GetSafeHandle() != NULL );
	if( IsEmpty() )
		return false;
	DWORD dwTabWndStyle =  wndTabs.GetStyle();
	bool bDrawFocusRect = ( m_bSelected && ::GetFocus() == wndTabs.m_hWnd && ( dwTabWndStyle & TCS_FOCUSNEVER ) == 0 ) ? true : false;
	if( ( dwTabWndStyle & TCS_BUTTONS ) != 0 )
	{
		CExtPaintManager::PAINTPUSHBUTTONDATA _ppbd(
				(CObject*)(&wndTabs),										   
				true, 
				m_rcItemAdjusted, 
				(LPCTSTR)m_strText,
				m_pIcon, // CExtCmdIcon *
				( ( dwTabWndStyle & TCS_FLATBUTTONS ) != 0 ) ? true : false, // bFlat, 
				false, // Hover?
				m_bSelected, //Pushed?
				false, 
				true, // Enabled?
				true, // DrawBorder?
				bDrawFocusRect, 
				m_bSelected, // Default?
				CExtPaintManager::__ALIGN_HORIZ_CENTER | CExtPaintManager::__ALIGN_VERT_CENTER,
				(HFONT)pFont->GetSafeHandle(),
				false,
				0, 
				true
				);
		pPM->PaintPushButton( dc, _ppbd );
	}
	else
	{
		CRect rcTabItemsArea = rcClient;
		bool bCenteredText = true;
		CRect rcEntireItem = m_rcItemAdjusted;
		CSize sizeTextMeasured = rcEntireItem.Size();
		pPM->PaintTabItem(
			dc, rcClient, true, true, m_bSelected, bCenteredText, false, false, false,
			rcEntireItem,
			sizeTextMeasured,
			pFont,
			( ! m_strText.IsEmpty() ) ? LPCTSTR(m_strText) : _T(""),
			m_pIcon, // CExtCmdIcon *
			(CObject*)(&wndTabs)
			);
		if( bDrawFocusRect )
		{
			CRect rcFocus = rcEntireItem;
			rcFocus.DeflateRect( 4, 2 );
			dc.DrawFocusRect( &rcFocus );
		}
	}
	if( ! m_rcSeparator.IsRectEmpty() )
	{
		CRect rcSeparator = m_rcSeparator;
		pPM->PaintSeparator( dc, rcSeparator, true, true, (CObject*)(&wndTabs) );
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtThemedTabCtrl window

IMPLEMENT_DYNCREATE( CExtThemedTabCtrl, CTabCtrl );
IMPLEMENT_CExtPmBridge_MEMBERS( CExtThemedTabCtrl );

CExtThemedTabCtrl::CExtThemedTabCtrl()
	: m_bAutoDestroyWindow( false )
	, m_rcInflateInTabItemMode( 0, 2, 0, 3 )
{
	PmBridge_Install();
}

CExtThemedTabCtrl::~CExtThemedTabCtrl()
{
	PmBridge_Uninstall();
}

BEGIN_MESSAGE_MAP( CExtThemedTabCtrl, CTabCtrl )
	//{{AFX_MSG_MAP(CExtThemedTabCtrl)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CExtThemedTabCtrl::PmBridge_OnPaintManagerChanged( CExtPaintManager * pGlobalPM )
{
	ASSERT_VALID( this );
	CExtPmBridge::PmBridge_OnPaintManagerChanged( pGlobalPM );
	if( GetSafeHwnd() == NULL )
		return;
	RedrawWindow( NULL, NULL, RDW_INVALIDATE|RDW_ERASE|RDW_ALLCHILDREN );
}

INT CExtThemedTabCtrl::GetItemArray( // returns count of non-empty items
									std::vector < CExtThemedTabItem > & arrItems,
	bool bAdjustRects // = true
	) const
{
	if( GetSafeHwnd() == NULL )
		return false;
INT nItemIndex, nItemCount = GetItemCount(), nSel = GetCurSel(), nNonEmptyCount = 0;
	if( nItemCount == 0 )
		return true;
	//arrItems.reserve( nItemCount );
	for( nItemIndex = 0; nItemIndex < nItemCount; nItemIndex ++ )
	{
// 		CExtThemedTabItem _etti( *this, nItemIndex, nItemCount, nSel );
// 		if( ! _etti.IsEmpty() )
// 			nNonEmptyCount ++;
 		nNonEmptyCount ++;
#if _MFC_VER >= 0xC00
		arrItems.emplace_back( *this, nItemIndex, nItemCount, nSel);
#else
		arrItems.push_back( CExtThemedTabItem(*this, nItemIndex, nItemCount, nSel ) );
#endif
	}
	if( nNonEmptyCount == 0 )
		return 0;
	if( bAdjustRects )
	{
		bool bDoAdjustmentH = true, bDoAdjustmentV = true;
		DWORD dwTabWndStyle =  GetStyle();
		if( ( dwTabWndStyle & TCS_BUTTONS ) != 0 )
			bDoAdjustmentV = false;
		DWORD dwExtendedTabWndStyle = ((CTabCtrl*)this)->GetExtendedStyle();
		bool bFlatSeparators = ( ( dwExtendedTabWndStyle & TCS_EX_FLATSEPARATORS ) != 0 ) ? true : false;
		if( bDoAdjustmentH )
		{
			INT nFirstInRowIndex = -1, nPrevIndex = -1;
			CRect rcFirstInRow( 0, 0, 0, 0 ), rcPrev( 0, 0, 0, 0 );
			for( nItemIndex = 0; nItemIndex < nItemCount; nItemIndex ++ )
			{
				CExtThemedTabItem & _etti = arrItems[nItemIndex];
				if( _etti.IsEmpty() )
					continue;
				if( _etti.m_rcItemOriginal.IsRectEmpty() )
					continue;
				if( nFirstInRowIndex < 0 )
				{
					nFirstInRowIndex = nItemIndex;
					rcFirstInRow = _etti.m_rcItemOriginal;
					rcPrev = _etti.m_rcItemAdjusted;
					nPrevIndex = nItemIndex;
					continue;
				}
				if( rcFirstInRow.top != _etti.m_rcItemOriginal.top || rcFirstInRow.bottom != _etti.m_rcItemOriginal.bottom )
				{
					nFirstInRowIndex = nItemIndex;
					rcFirstInRow = _etti.m_rcItemOriginal;
					rcPrev = _etti.m_rcItemAdjusted;
					nPrevIndex = nItemIndex;
					continue;
				}
				if( _etti.m_rcItemAdjusted.left != rcPrev.right )
				{
					ASSERT( nPrevIndex >= 0 );
					if( bFlatSeparators )
					{
						_etti.m_rcSeparator.SetRect( rcPrev.left, _etti.m_rcItemAdjusted.top, _etti.m_rcItemAdjusted.right, _etti.m_rcItemAdjusted.bottom );
					}
					else
					{
						INT nMid = rcPrev.right + ( _etti.m_rcItemAdjusted.left - rcPrev.right ) / 2;
						_etti.m_rcItemAdjusted.left = rcPrev.right = nMid;
						CExtThemedTabItem & _ettiPrev = arrItems[nPrevIndex];
						_ettiPrev.m_rcItemAdjusted = rcPrev;
					}
				}
				rcPrev = _etti.m_rcItemAdjusted;
				nPrevIndex = nItemIndex;
			}
		}
		if( bDoAdjustmentV )
		{
			for( nItemIndex = 0; nItemIndex < nItemCount; nItemIndex ++ )
			{
				CExtThemedTabItem & _etti = arrItems[nItemIndex];
				if( _etti.IsEmpty() )
					continue;
				if( _etti.m_rcItemOriginal.IsRectEmpty() )
					continue;
				_etti.m_rcItemAdjusted.InflateRect( m_rcInflateInTabItemMode.left, m_rcInflateInTabItemMode.top, m_rcInflateInTabItemMode.right, m_rcInflateInTabItemMode.bottom );
				if( ! _etti.m_rcSeparator.IsRectEmpty() )
					_etti.m_rcSeparator.InflateRect( m_rcInflateInTabItemMode.left, m_rcInflateInTabItemMode.top, m_rcInflateInTabItemMode.right, m_rcInflateInTabItemMode.bottom );
			}
		}
	}
	return nNonEmptyCount;
}

INT CExtThemedTabCtrl::DoHitTestEx(
	CPoint ptClient,
	CExtThemedTabItem * p_etti = NULL
	) const
{
	ASSERT_VALID( this );
	if( p_etti != NULL )
		p_etti->Empty();
	if( GetSafeHwnd() == NULL )
		return -1;
	std::vector < CExtThemedTabItem > arrItems;
	if( GetItemArray( arrItems ) == 0 )
		return -1;
INT nItemIndex, nItemCount = INT( arrItems.size() );
	if( nItemCount == 0 )
		return -1;
	for( nItemIndex = 0; nItemIndex < nItemCount; nItemIndex ++ )
	{
		CExtThemedTabItem & _etti = arrItems[nItemIndex];
		if( _etti.IsEmpty() )
			continue;
		if( _etti.m_rcItemAdjusted.PtInRect( ptClient ) )
		{
			if( p_etti != NULL )
				(*p_etti) = _etti;
			return nItemIndex;
		}
	}
	return -1;
}

void CExtThemedTabCtrl::DoEraseBk( CDC & dc )
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	CRect rcClient;
	GetClientRect( &rcClient );
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
	pPM->PaintDockerBkgnd( true, dc, this );
DWORD dwTabWndStyle =  GetStyle();
	if( ( dwTabWndStyle & TCS_BUTTONS ) != 0 )
		return;
CRect rcData = rcClient;
	AdjustRect( FALSE, &rcData );
CRect rcTabItemsArea;
	rcTabItemsArea.SetRect( rcClient.left, rcClient.top, rcClient.right, rcData.top );
CRect rcTabNearBorderArea = rcTabItemsArea;
	rcTabNearBorderArea.top = rcTabNearBorderArea.bottom - 1;
	rcTabNearBorderArea.bottom ++;
	pPM->PaintTabClientArea( dc, rcTabItemsArea, rcClient, rcTabNearBorderArea, __ETWS_ORIENT_TOP, false, this );
}

void CExtThemedTabCtrl::DoPaint( CDC & dc )
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	DoEraseBk( dc );
	std::vector < CExtThemedTabItem > arrItems;
bool bHaveItemsToPaint = ( GetItemArray( arrItems ) > 0 ) ? true : false;
	if( ! bHaveItemsToPaint )
		return;
INT nItemIndex, nItemCount = INT( arrItems.size() ), nSel = -1;
	if( nItemCount == 0 )
		return;
CRect rcClient;
	GetClientRect( &rcClient );
CRect rcData = rcClient;
	AdjustRect( FALSE, &rcData );
	rcData.left = rcClient.left;
	rcData.right = rcClient.right;
	dc.ExcludeClipRect( &rcData );
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
CFont * pFont = GetFont();
	for( nItemIndex = 0; nItemIndex < nItemCount; nItemIndex ++ )
	{
		CExtThemedTabItem & _etti = arrItems[nItemIndex];
		if( _etti.m_bSelected )
			nSel = nItemIndex;
		else
			_etti.DoPaint( *this, dc, pPM, pFont, rcClient );
	}
	if( nSel >= 0 )
	{
		CExtThemedTabItem & _etti = arrItems[nSel];
		_etti.DoPaint( *this, dc, pPM, pFont, rcClient );
	}
}

LRESULT CExtThemedTabCtrl::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	switch( message )
	{
	case WM_LBUTTONUP:
		return 0L;
	case WM_LBUTTONDOWN:
		{
			CPoint ptClient = DWORD( lParam );
			INT nNewSel = DoHitTestEx( ptClient );
			if( nNewSel >= 0 )
			{
				INT nCurSel = GetCurSel();
				if( nNewSel != nCurSel )
				{
					HWND hWndOwn = m_hWnd;
					HWND hWndParent = ::GetParent( hWndOwn );
					if( hWndParent != NULL )
					{
						UINT nOwnID = UINT( GetDlgCtrlID() );
						NMHDR _data;
						::memset( &_data, 0, sizeof(NMHDR) );
						_data.hwndFrom = hWndOwn;
						_data.idFrom = nOwnID;
						_data.code = TCN_SELCHANGING;
						::SendMessage( hWndParent, WM_NOTIFY, WPARAM(nOwnID), LPARAM(&_data) );
						if( ! ::IsWindow( hWndOwn ) )
							return 0L;
					}
					SetCurSel( nNewSel );
					if( ! ::IsWindow( hWndOwn ) )
						return 0L;
					hWndParent = ::GetParent( hWndOwn );
					if( hWndParent != NULL )
					{
						UINT nOwnID = UINT( GetDlgCtrlID() );
						NMHDR _data;
						::memset( &_data, 0, sizeof(NMHDR) );
						_data.hwndFrom = hWndOwn;
						_data.idFrom = nOwnID;
						_data.code = TCN_SELCHANGE;
						::SendMessage( hWndParent, WM_NOTIFY, WPARAM(nOwnID), LPARAM(&_data) );
						if( ! ::IsWindow( hWndOwn ) )
							return 0L;
					}
				}
			}
		}
		return 0L;
	case WM_ERASEBKGND:
		return TRUE;
	case WM_PAINT:
		{
			CPaintDC dcPaint( this );
			CRect rcClient;
			GetClientRect( &rcClient );
			CExtMemoryDC dc( &dcPaint, &rcClient );
			DoPaint( dc );
		}
		return TRUE;
	case WM_PRINT:
	case WM_PRINTCLIENT:
		{
			CDC * pDC = CDC::FromHandle( (HDC) wParam );
			CRect rcRgnWnd, rcRgnClient;
			GetWindowRect( &rcRgnWnd );
			GetClientRect( &rcRgnClient );
// 				if( (lParam&PRF_NONCLIENT) != 0 )
// 				{
// 					CRect rcWnd = rcRgnWnd, rcClient = rcRgnClient;
// 					ClientToScreen( &rcClient );
// 					rcClient.OffsetRect( -rcWnd.TopLeft() );
// 					rcWnd.OffsetRect( -rcWnd.TopLeft() );
// 					CRgn rgnWnd;
// 					if( rgnWnd.CreateRectRgnIndirect(&rcWnd) )
// 						pDC->SelectClipRgn( &rgnWnd );
// 					pDC->ExcludeClipRect( &rcClient );
// 					DoPaintNC( pDC );
// 					pDC->SelectClipRgn( NULL );
// 				}
			if( (lParam&(PRF_CLIENT|PRF_ERASEBKGND)) != 0 )
			{
				CPoint ptVpOffset( 0, 0 );
				if( (lParam&PRF_NONCLIENT) != 0 )
				{
					CRect rcWnd = rcRgnWnd, rcClient = rcRgnClient;
					ClientToScreen( &rcClient );
					ptVpOffset.x = rcWnd.left - rcClient.left;
					ptVpOffset.y = rcWnd.top - rcClient.top;
				}
				if(		ptVpOffset.x != 0
					||	ptVpOffset.y != 0
					)
					pDC->OffsetViewportOrg(
						-ptVpOffset.x,
						-ptVpOffset.y
						);
//				DoEraseBk( *pDC );
//				DoPaint( *pDC );
				CDC dcSurface;
				CBitmap bmpSurface;
				CWindowDC dcDesktop( NULL );
				if(		dcSurface.CreateCompatibleDC( NULL )
					&&	bmpSurface.CreateCompatibleBitmap(
							&dcDesktop,
							rcRgnClient.Width(),
							rcRgnClient.Height()
							)
					)
				{
					CBitmap * pOldBmp = dcSurface.SelectObject( &bmpSurface );
					DoEraseBk( dcSurface );
					DoPaint( dcSurface );
					pDC->BitBlt(
						0,
						0,
						rcRgnClient.Width(),
						rcRgnClient.Height(),
						&dcSurface,
						0,
						0,
						SRCCOPY
						);
					dcSurface.SelectObject( pOldBmp );
				}
				if(		ptVpOffset.x != 0
					||	ptVpOffset.y != 0
					)
					pDC->OffsetViewportOrg(
						ptVpOffset.x,
						ptVpOffset.y
						);
			} // if( (lParam&(PRF_CLIENT|PRF_ERASEBKGND)) != 0 )
			if( (lParam&PRF_CHILDREN) != 0 )
				CExtPaintManager::stat_PrintChildren(
					m_hWnd,
					message,
					pDC->GetSafeHdc(),
					lParam,
					false
					);
		}
		return TRUE;
	} // switch( message )
LRESULT lResult = CTabCtrl::WindowProc( message, wParam, lParam );
	switch( message )
	{
	case WM_SYSCOLORCHANGE:
	case WM_SETTINGCHANGE:
	case WM_DISPLAYCHANGE:
	case __ExtMfc_WM_THEMECHANGED:
		RedrawWindow( NULL, NULL, RDW_INVALIDATE|RDW_ERASE|RDW_ALLCHILDREN );
	break;
	case WM_HSCROLL:
	case WM_VSCROLL:
	case TCM_INSERTITEM:
	case TCM_DELETEITEM:
	case TCM_DELETEALLITEMS:
	case TCM_SETITEM:
	case TCM_SETCURSEL:
		Invalidate();
	break;
	} // switch( message )
	return lResult;
}

BOOL CExtThemedTabCtrl::OnChildNotify( UINT message, WPARAM wParam, LPARAM lParam, LRESULT * pLResult )
{
	ASSERT_VALID( this );
	if( message == WM_NOTIFY )
	{
		NMHDR * pNMHDR = (NMHDR *)lParam;
		ASSERT( pNMHDR != NULL );
		switch( pNMHDR->code )
		{
		case TCN_KEYDOWN:
		case TCN_SELCHANGING:
		case TCN_SELCHANGE:
			Invalidate();
		break;
		}
	}
	return CTabCtrl::OnChildNotify( message, wParam, lParam, pLResult );
}

void CExtThemedTabCtrl::PreSubclassWindow()
{
	CTabCtrl::PreSubclassWindow();
	ModifyStyle( 0, WS_CLIPSIBLINGS|WS_CLIPCHILDREN );
static const TCHAR g_strSpinWndClassName[] = _T("msctls_updown32");
HWND hWnd = ::GetWindow( m_hWnd, GW_CHILD );
	for( ; hWnd != NULL ;hWnd = ::GetWindow( hWnd, GW_CHILD ) )
	{
		TCHAR strClassName[512+1];
		::memset( strClassName, 0, sizeof(strClassName) );
		INT nRetVal = ::GetClassName( hWnd, strClassName, sizeof(strClassName)/sizeof(strClassName[0]) - 1 );
		if( nRetVal != 0 && _tcsicmp( strClassName, g_strSpinWndClassName ) == 0 )
		{
			m_wndSpin.SubclassWindow( hWnd );
			m_wndSpin.ModifyStyle( 0, WS_CLIPSIBLINGS|WS_CLIPCHILDREN );
			break;
		}
	}
}

void CExtThemedTabCtrl::PostNcDestroy()
{
	CTabCtrl::PostNcDestroy();
	if( m_bAutoDestroyWindow )
		delete this;
}

#endif // (!defined __EXT_MFC_NO_TAB_CTRL )


