// This is part of the Professional User Interface Suite library.
// Copyright � 2019 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_RIBBON_BAR)

#if (!defined __EXT_RIBBON_BAR_H)
	#include <ExtRibbonBar.h>
#endif // (!defined __EXT_RIBBON_BAR_H)

#if (!defined __EXT_RICH_CONTENT_H)
	#include <ExtRichContent.h>
#endif // (!defined __EXT_RICH_CONTENT_H)

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#if (!defined __EXT_MEMORY_DC_H)
	#include <../Src/ExtMemoryDC.h>
#endif

#if (!defined __EXT_POPUP_CTRL_MENU_H)
	#include <ExtPopupCtrlMenu.h>
#endif

#if( !defined __EXTMINIDOCKFRAMEWND_H)
	#include "ExtMiniDockFrameWnd.h"
#endif

#if (!defined __PROF_UIS_RES_2007_H)
	#include <Resources/Res2007/Res2007.h>
#endif

#if (!defined __PROF_UIS_RES_2010_OFFICE_H)
	#include <Resources/Res2010office/Res2010office.h>
#endif

#if (!defined __EXT_CONTROLS_COMMON_H)
	#include <ExtControlsCommon.h>
#endif

#include <Resources/Resource.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNode

IMPLEMENT_SERIAL( CExtRibbonNode, CExtCustomizeCmdTreeNode, VERSIONABLE_SCHEMA|1 );

CExtRibbonNode::CExtRibbonNode(
	UINT nCmdIdBasic, // = 0L
	UINT nCmdIdEffective, // = 0L
	CExtRibbonNode * pParentNode, // = NULL
	DWORD dwFlags, // = 0L
	__EXT_MFC_SAFE_LPCTSTR strTextInToolbar, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strTextInMenu, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strTextUser, // = NULL
	LPARAM lParam, // = 0L
	CExtCmdIcon * pIconCustomized // = NULL
#if (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)
	,
	INT nTextFieldWidth, // = 100
	INT nDropDownWidth, // = -2 // (-1) - auto calc, (-2) - same as button area
	INT nDropDownHeightMax // = 250
#endif // (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)
	)
	: CExtCustomizeCmdTreeNode(
		( nCmdIdBasic == 0 ) ? nCmdIdEffective : nCmdIdBasic,
		( nCmdIdEffective == 0 ) ? nCmdIdBasic :  nCmdIdEffective,
		pParentNode,
		dwFlags,
		strTextInToolbar,
		strTextInMenu,
		strTextUser,
		lParam,
		pIconCustomized
#if (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)
		,
		nTextFieldWidth,
		nDropDownWidth,
		nDropDownHeightMax
#endif // (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)
		)
{
	_InitMembers();
}

CExtRibbonNode::CExtRibbonNode(
	CExtRibbonNode & other
	)
{
	_InitMembers();
	AssignFromOther( other );
}

//CExtRibbonNode::CExtRibbonNode(
//	CExtCmdItem * pCmdItem, // NOT NULL!
//	CExtCmdIcon * pIconCustomized // = NULL
//	)
//{
//	_InitMembers();
//	AssignCmdProps( pCmdItem, pIconCustomized );
//}

CExtRibbonNode::~CExtRibbonNode()
{
}

void CExtRibbonNode::_InitMembers()
{
	ASSERT_VALID( this );
	m_nTpmxAdditionalFlags = TPMX_RIBBON_MODE;
	ModifyFlags( __ECTN_RGA_DEFAULT );
	m_nRIL_VisualMin = __EXT_RIBBON_ILV_SIMPLE_MIN;
	m_nRIL_VisualCurrent = m_nRIL_VisualMax = __EXT_RIBBON_ILV_SIMPLE_MAX;
	m_nRIL_Effective = __EXT_RIBBON_ILE_MAX;
	m_nRIL_EffectiveCollapsed = __EXT_RIBBON_ILE_MAX;
	m_arrILEtoILV.RemoveAll();
	m_arrILEtoILV.SetSize( 3 );
	m_arrILEtoILV.SetAt(
		0,
		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
			__EXT_RIBBON_ILE_MAX - 1, // ::MulDiv( __EXT_RIBBON_ILE_MAX, 2, 3 )
			__EXT_RIBBON_ILV_SIMPLE_LARGE,
			false
			)
		);
	m_arrILEtoILV.SetAt(
		1,
		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
			__EXT_RIBBON_ILE_MAX - 2,
			__EXT_RIBBON_ILV_SIMPLE_NORMAL,
			false
			)
		);
	m_arrILEtoILV.SetAt(
		2,
		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
			__EXT_RIBBON_ILE_MAX - 3,
			__EXT_RIBBON_ILV_SIMPLE_SMALL,
			false
			)
		);
}

void CExtRibbonNode::OnSysColorChange(
	CExtPaintManager * pPM,
	CObject * pHelperSrc,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID( this );
	CExtCustomizeCmdTreeNode::OnSysColorChange( pPM, pHelperSrc, lParam );
	m_iconSmall.OnSysColorChange( pPM );
	m_iconBig.OnSysColorChange( pPM );
}

void CExtRibbonNode::OnSettingChange(
	UINT uFlags,
	__EXT_MFC_SAFE_LPCTSTR strSection,
	CExtPaintManager * pPM,
	CObject * pHelperSrc,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID( this );
	CExtCustomizeCmdTreeNode::OnSettingChange(
		uFlags,
		strSection,
		pPM,
		pHelperSrc,
		lParam
		);
	m_iconSmall.OnSettingChange( pPM, uFlags, strSection );
	m_iconBig.OnSettingChange( pPM, uFlags, strSection );
}

void CExtRibbonNode::OnDisplayChange(
	WPARAM _wParam,
	LPARAM _lParam,
	CExtPaintManager * pPM,
	CObject * pHelperSrc,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID( this );
	CExtCustomizeCmdTreeNode::OnDisplayChange(
		_wParam,
		_lParam,
		pPM,
		pHelperSrc,
		lParam
		);
	m_iconSmall.OnDisplayChange( pPM, (INT)_wParam, CPoint(_lParam) );
	m_iconBig.OnDisplayChange( pPM, (INT)_wParam, CPoint(_lParam) );
}

void CExtRibbonNode::OnThemeChanged(
	WPARAM _wParam,
	LPARAM _lParam,
	CExtPaintManager * pPM,
	CObject * pHelperSrc,
	LPARAM lParam // = 0L
	)
{
	ASSERT_VALID( this );
	CExtCustomizeCmdTreeNode::OnThemeChanged(
		_wParam,
		_lParam,
		pPM,
		pHelperSrc,
		lParam
		);
	m_iconSmall.OnThemeChanged( pPM, _wParam, _lParam );
	m_iconBig.OnThemeChanged( pPM, _wParam, _lParam );
}

bool CExtRibbonNode::OnGetCommandsListBoxInfo(
	CExtCustomizeCommandListBox * pLB, // IN (optional)
	CExtCustomizeSite * pSite, // = NULL // IN (optional)
	CExtCmdItem * pCmdItem, // = NULL // IN (optional)
	CExtSafeString * pStrLbText, // = NULL // OUT (optional)
	CExtCmdIcon * pLbIcon, // = NULL // OUT (optional)
	INT nDesiredIconWidth, // = 16 // IN (optional)
	INT nDesiredIconHeight // = 16 // IN (optional)
	)
{
	ASSERT_VALID( this );
	if( ! CExtCustomizeCmdTreeNode::OnGetCommandsListBoxInfo(
			pLB,
			pSite,
			pCmdItem,
			pStrLbText,
			pLbIcon,
			nDesiredIconWidth,
			nDesiredIconHeight
			)
		)
		return false;
	if( pLbIcon != NULL )
	{
		if( ! m_iconSmall.IsEmpty() )
		{
			pLbIcon->Empty();
			(*pLbIcon) = m_iconSmall;
		} // if( ! m_iconSmall.IsEmpty() )
		else if( ! m_iconBig.IsEmpty() )
		{
			pLbIcon->Empty();
			(*pLbIcon) = m_iconBig;
		} // if( ! m_iconBig.IsEmpty() )
		if(		(! pLbIcon->IsEmpty() )
			&&	nDesiredIconWidth > 0
			&&	nDesiredIconHeight > 0
			)
		{
			const SIZE _sizeScale = { nDesiredIconWidth, nDesiredIconHeight };
			pLbIcon->Scale( _sizeScale );
		} // if( (! pLbIcon->IsEmpty() . . .
	}
	return true;
}

CExtRibbonNode & CExtRibbonNode::operator = (
	CExtRibbonNode & other
	)
{
	ASSERT_VALID( this );
	AssignFromOther( other );
	ASSERT_VALID( this );
	return (*this);
}

void CExtRibbonNode::AssignFromOther(
	CExtCustomizeCmdTreeNode & other
	)
{
	ASSERT_VALID( this );
	CExtCustomizeCmdTreeNode::AssignFromOther( other );
	if( m_nCmdIdBasic == 0 )
		m_nCmdIdBasic = m_nCmdIdEffective;
	if( m_nCmdIdEffective == 0 )
		m_nCmdIdEffective = m_nCmdIdBasic;
CExtRibbonNode * pOther = DYNAMIC_DOWNCAST( CExtRibbonNode, (&other) );
	if( pOther != NULL )
	{
		RibbonILV_Set( pOther->RibbonILV_Get( -1 ), -1 );
		RibbonILV_Set( pOther->RibbonILV_Get(  0 ),  0 );
		RibbonILV_Set( pOther->RibbonILV_Get(  1 ),  1 );
		RibbonILE_Set( pOther->RibbonILE_Get() );
		RibbonILE_SetCollapsed( pOther->RibbonILE_GetCollapsed() );
		RibbonILE_RuleArraySet( pOther->RibbonILE_RuleArrayGet() );
		m_iconBig = pOther->m_iconBig;
		m_iconSmall = pOther->m_iconSmall;
		m_nTpmxAdditionalFlags = pOther->m_nTpmxAdditionalFlags;
	} // if( pOther != NULL )
	else
		_InitMembers();
	ASSERT_VALID( this );
}

//void CExtRibbonNode::AssignCmdProps(
//	CExtCmdItem * pCmdItem, // NOT NULL!
//	CExtCmdIcon * pIconCustomized // = NULL
//	)
//{
//	ASSERT_VALID( this );
//	CExtCustomizeCmdTreeNode::AssignCmdProps( pCmdItem, pIconCustomized );
//}

#ifdef _DEBUG
void CExtRibbonNode::AssertValid() const
{
	CExtCustomizeCmdTreeNode::AssertValid();
}
#endif // _DEBUG

void CExtRibbonNode::Serialize( CArchive & ar )
{
	CExtCustomizeCmdTreeNode::Serialize( ar );
DWORD dw, dwIndex, dwCount;
	if( ar.IsStoring() )
	{
		ar << DWORD( RibbonILV_Get( -1 ) );
		ar << DWORD( RibbonILV_Get(  0 ) );
		ar << DWORD( RibbonILV_Get(  1 ) );
		ar << DWORD( RibbonILE_Get() );
		ar << DWORD( RibbonILE_GetCollapsed() );
		ar << m_nTpmxAdditionalFlags;
		const CArray < DWORD, DWORD > & arrILEtoILV = RibbonILE_RuleArrayGet();
		dwCount = DWORD( arrILEtoILV.GetSize() );
		ar << dwCount;
		for( dwIndex = 0; dwIndex < dwCount; dwIndex++ )
		{
			dw = arrILEtoILV[ INT(dwIndex) ];
			ar << dw;
		} // for( dwIndex = 0; dwIndex < dwCount; dwIndex++ )
	} // if( ar.IsStoring() )
	else
	{
		CArray < DWORD, DWORD > arrILEtoILV;
		RibbonILE_RuleArraySet( arrILEtoILV ); // clear
		ar >> dw;
		RibbonILV_Set( dw, -1 );
		ar >> dw;
		RibbonILV_Set( dw,  0 );
		ar >> dw;
		RibbonILV_Set( dw,  1 );
		ar >> dw;
		RibbonILE_Set( dw );
		ar >> dw;
		RibbonILE_SetCollapsed( dw );
		ar >> m_nTpmxAdditionalFlags;
		ar >> dwCount;
		arrILEtoILV.SetSize( INT(dwCount) );
		for( dwIndex = 0; dwIndex < dwCount; dwIndex++ )
		{
			ar >> dw;
			arrILEtoILV.SetAt( INT(dwIndex), dw );
		} // for( dwIndex = 0; dwIndex < dwCount; dwIndex++ )
		RibbonILE_RuleArraySet( arrILEtoILV );
	} // else from if( ar.IsStoring() )
	m_iconBig.Serialize( ar );
	m_iconSmall.Serialize( ar );
}

CExtCmdIcon * CExtRibbonNode::GetIconPtrInMenu(
	CExtCmdItem * pCmdItem,
	bool bForceBasic // = false
	)
{
	ASSERT_VALID( this );
	if( ! m_iconBig.IsEmpty() )
		return (&m_iconBig);
	if( ! m_iconSmall.IsEmpty() )
		return (&m_iconSmall);
	return
		CExtCustomizeCmdTreeNode::GetIconPtrInMenu(
			pCmdItem,
			bForceBasic
			);
}

CExtCmdIcon * CExtRibbonNode::GetIconPtrInToolbar(
	CExtCmdItem * pCmdItem,
	bool bForceBasic // = false
	)
{
	ASSERT_VALID( this );
//	//	if( ! m_iconBig.IsEmpty() )
//	//		return (&m_iconBig);
//		if( RibbonILV_Get() <= __EXT_RIBBON_ILV_SIMPLE_NORMAL )
//		{
//			if( ! m_iconSmall.IsEmpty() )
//				return (&m_iconSmall);
//		}
	return
		CExtCustomizeCmdTreeNode::GetIconPtrInToolbar(
			pCmdItem,
			bForceBasic
			);
}

bool CExtRibbonNode::RibbonWrapFromILE( INT nILE ) const
{
	ASSERT_VALID( this );
bool bRibbonWrap = false;
	RibbonILV_fromILE( nILE, &bRibbonWrap );
	return bRibbonWrap;
}

INT CExtRibbonNode::RibbonILV_fromILE(
	INT nILE,
	bool * p_bIsWrap // = NULL
	) const
{
	ASSERT_VALID( this );
	ASSERT(
			__EXT_RIBBON_ILE_MIN <= nILE
		&&	nILE <= (__EXT_RIBBON_ILE_MAX+1)
		);
	if( p_bIsWrap != NULL )
		(*p_bIsWrap) = false;
INT nILV = RibbonILV_Get( 1 ), nIndex, nCount = INT(m_arrILEtoILV.GetSize());
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		DWORD dw = m_arrILEtoILV[ nIndex ];
		INT nStepILE = INT( __EXT_RIBBON_ILE_FROM_ENTRY(dw) );
#ifdef _DEBUG
		ASSERT(
				__EXT_RIBBON_ILE_MIN <= nStepILE
			&&	nStepILE <= (__EXT_RIBBON_ILE_MAX+1)
			);
//		if( nIndex > 0 )
//		{
//			DWORD dwPrev = m_arrILEtoILV[ nIndex - 1 ];
//			INT nStepILE_prev = INT( __EXT_RIBBON_ILE_FROM_ENTRY(dwPrev) );
//			ASSERT( nStepILE_prev > nStepILE );
//		} // if( nIndex > 0 )
#endif // _DEBUG
		INT nStepILV = INT( __EXT_RIBBON_ILV_FROM_ENTRY(dw) );
		if( nStepILE >= nILE || nIndex == 0 )
		{
			nILV = nStepILV;
			if( p_bIsWrap != NULL )
			{
				bool bRibbonWrap = __EXT_RIBBON_TOOL_WRAP_FROM_ENTRY(dw);
				(*p_bIsWrap) = bRibbonWrap;
			}
			if( nStepILE == nILE )
				break;
			continue;
		} // if( nStepILE >= nILE )
		break;
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
	return nILV;
}

INT CExtRibbonNode::RibbonILV_Get(
	INT nType // = 0 // -1 min, 0 current, 1 - max
	) const
{
	ASSERT_VALID( this );
	if( nType < 0 )
		return m_nRIL_VisualMin;
	if( nType > 0 )
		return m_nRIL_VisualMax;
	return m_nRIL_VisualCurrent;
}

void CExtRibbonNode::RibbonILV_Set(
	INT nILV,
	INT nType // = 0, // -1 min, 0 current, 1 - max
	)
{
	ASSERT_VALID( this );
	if( nType < 0 )
		m_nRIL_VisualMin = nILV;
	else if( nType > 0 )
		m_nRIL_VisualMax = nILV;
	else
		m_nRIL_VisualCurrent = nILV;
}

INT CExtRibbonNode::RibbonILE_Get() const
{
	ASSERT_VALID( this );
	ASSERT(
			__EXT_RIBBON_ILE_MIN <= m_nRIL_Effective
		&&	m_nRIL_Effective <= (__EXT_RIBBON_ILE_MAX+1)
		);
	return m_nRIL_Effective;
}

bool CExtRibbonNode::RibbonILE_Set( INT nILE ) // returns flag indicating whether ILV changed
{
	ASSERT_VALID( this );
	ASSERT(
			__EXT_RIBBON_ILE_MIN <= nILE
		&&	nILE <= (__EXT_RIBBON_ILE_MAX+1)
		&&	__EXT_RIBBON_ILE_MIN <= m_nRIL_Effective
		&&	m_nRIL_Effective <= (__EXT_RIBBON_ILE_MAX+1)
		);
//	if( m_nRIL_Effective == nILE )
//		return false;
bool bWrap_Old = false, bWrap_New = false;
INT nILV_Old = RibbonILV_fromILE( m_nRIL_Effective, &bWrap_Old );
INT nILV_New = RibbonILV_fromILE( nILE, &bWrap_New );
bool bRetVal = ( nILV_Old != nILV_New || bWrap_Old != bWrap_New ) ? true : false;
	m_nRIL_Effective = nILE;
	if( (! bRetVal ) && RibbonILV_Get() != nILV_New )
		bRetVal = true;
	if( bRetVal )
		RibbonILV_Set( nILV_New, 0 );
	return bRetVal;
}

INT CExtRibbonNode::RibbonILE_GetCollapsed() const
{
	ASSERT_VALID( this );
	ASSERT(
			__EXT_RIBBON_ILE_MIN <= m_nRIL_EffectiveCollapsed
		&&	m_nRIL_EffectiveCollapsed <= (__EXT_RIBBON_ILE_MAX+1)
		);
	return m_nRIL_EffectiveCollapsed;
}

void CExtRibbonNode::RibbonILE_SetCollapsed( INT nILE ) // returns flag indicating whether ILV changed
{
	ASSERT_VALID( this );
	ASSERT(
			__EXT_RIBBON_ILE_MIN <= nILE
		&&	nILE <= (__EXT_RIBBON_ILE_MAX+1)
		&&	__EXT_RIBBON_ILE_MIN <= m_nRIL_EffectiveCollapsed
		&&	m_nRIL_EffectiveCollapsed <= (__EXT_RIBBON_ILE_MAX+1)
		);
	m_nRIL_EffectiveCollapsed = nILE;
}


CArray < DWORD, DWORD > & CExtRibbonNode::RibbonILE_RuleArrayGet()
{
	ASSERT_VALID( this );
	return m_arrILEtoILV;
}

const CArray < DWORD, DWORD > & CExtRibbonNode::RibbonILE_RuleArrayGet() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtRibbonNode * > ( this ) )
		-> RibbonILE_RuleArrayGet();
}

void CExtRibbonNode::RibbonILE_RuleArraySet(
	const CArray < DWORD, DWORD > & arrILEtoILV
	)
{
	ASSERT_VALID( this );
	if( LPVOID(&m_arrILEtoILV) == LPVOID(&arrILEtoILV) )
		return;
	m_arrILEtoILV.RemoveAll();
INT nIndex, nCount = INT(arrILEtoILV.GetSize());
	m_arrILEtoILV.SetSize( nCount );
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		DWORD dw = arrILEtoILV[ nIndex ];
		m_arrILEtoILV.SetAt( nIndex, dw );
#ifdef _DEBUG
		INT nStepILE = INT( __EXT_RIBBON_ILE_FROM_ENTRY(dw) );
//		INT nStepILV = INT( __EXT_RIBBON_ILV_FROM_ENTRY(dw) );
		ASSERT(
				__EXT_RIBBON_ILE_MIN <= nStepILE
			&&	nStepILE <= (__EXT_RIBBON_ILE_MAX+1)
			);
//		ASSERT(
//				__EXT_RIBBON_ILV_SIMPLE_MIN <= nStepILV
//			&&	nStepILV <= __EXT_RIBBON_ILV_SIMPLE_MAX
//			);
//		if( nIndex > 0 )
//		{
//			DWORD dwPrev = m_arrILEtoILV[ nIndex - 1 ];
//			INT nStepILE_prev = INT( __EXT_RIBBON_ILE_FROM_ENTRY(dwPrev) );
//			ASSERT( nStepILE_prev > nStepILE );
//		} // if( nIndex > 0 )
#endif // _DEBUG
	} // for( nIndex = 0; nIndex < nCount; nIndex++ )
}

bool CExtRibbonNode::RibbonILE_RuleRemoveEntriesByILE(
	INT nMinILE,
	INT nMaxILE
	)
{
	ASSERT_VALID( this );
	if(		__EXT_RIBBON_ILE_MIN > nMinILE
		||	(__EXT_RIBBON_ILE_MAX+1) < nMinILE
		||	__EXT_RIBBON_ILE_MIN > nMaxILE
		||	(__EXT_RIBBON_ILE_MAX+1) < nMaxILE
		||	nMaxILE < nMinILE
		)
		return false;
CArray < DWORD, DWORD > arrRuleNew, & arrRuleExisting = RibbonILE_RuleArrayGet();
	for( INT i = 0; i < arrRuleExisting.GetSize(); i ++ )
	{
		DWORD dw = arrRuleExisting[i];
		INT nILE = INT( __EXT_RIBBON_ILE_FROM_ENTRY(dw) );
		if( nMinILE <= nILE && nILE <= nMaxILE )
			arrRuleNew.Add( dw );
	} // for( INT i = 0; i < arrRuleExisting.GetSize(); i ++ )
	if( arrRuleExisting.GetSize() == arrRuleNew.GetSize() )
		return false;
	RibbonILE_RuleArraySet( arrRuleNew );
	return true;
}

bool CExtRibbonNode::RibbonILE_RuleRemoveEntriesByILV(
	bool bRemoveSmall,
	bool bRemoveNormal,
	bool bRemoveLarge
	)
{
	ASSERT_VALID( this );
	if( ! ( bRemoveSmall || bRemoveNormal || bRemoveLarge ) )
		return false;
CArray < DWORD, DWORD > arrRuleNew, & arrRuleExisting = RibbonILE_RuleArrayGet();
	for( INT i = 0; i < arrRuleExisting.GetSize(); i ++ )
	{
		DWORD dw = arrRuleExisting[i];
		INT nILV = INT( __EXT_RIBBON_ILV_FROM_ENTRY(dw) );
		switch( nILV )
		{
		case __EXT_RIBBON_ILV_SIMPLE_SMALL:
			if( bRemoveSmall )
				continue;
		break;
		case __EXT_RIBBON_ILV_SIMPLE_NORMAL:
			if( bRemoveNormal )
				continue;
		break;
		case __EXT_RIBBON_ILV_SIMPLE_LARGE:
			if( bRemoveLarge )
				continue;
		break;
		} // switch( nILV )
		arrRuleNew.Add( dw );
	} // for( INT i = 0; i < arrRuleExisting.GetSize(); i ++ )
	if( arrRuleExisting.GetSize() == arrRuleNew.GetSize() )
		return false;
	RibbonILE_RuleArraySet( arrRuleNew );
	return true;
}

bool CExtRibbonNode::RibbonILE_RuleRemoveLargeILV()
{
	ASSERT_VALID( this );
	return RibbonILE_RuleRemoveEntriesByILV( false, false, true );
}

bool CExtRibbonNode::RibbonILE_RuleRemoveNormalILV()
{
	ASSERT_VALID( this );
	return RibbonILE_RuleRemoveEntriesByILV( false, true, false );
}

bool CExtRibbonNode::RibbonILE_RuleRemoveSmallILV()
{
	ASSERT_VALID( this );
	return RibbonILE_RuleRemoveEntriesByILV( true, false, false );
}

bool CExtRibbonNode::Ribbon_InitCommandProfileIcon(
	CExtCmdProfile * pProfile,
	bool bInit
	)
{
	ASSERT_VALID( this );
	return CExtCustomizeCmdTreeNode::Ribbon_InitCommandProfileIcon( pProfile, bInit );

//UINT nCmdID = GetCmdID( true );
//	if( nCmdID == 0 )
//	{
//		nCmdID = GetCmdID( false );
//		if( nCmdID == 0 )
//			return false;
//	}
//	if( bInit )
//	{
//		if( ! m_iconSmall.IsEmpty() )
//		{
//			pProfile->CmdSetIcon( nCmdID, m_iconSmall );
//			return true;
//		} // if( ! m_iconSmall.IsEmpty() )
//		if( ! m_iconBig.IsEmpty() )
//		{
//			pProfile->CmdSetIcon( nCmdID, m_iconBig );
//			return true;
//		} // if( ! m_iconBig.IsEmpty() )
//	} // if( bInit )
//	else
//		pProfile->CmdSetIcon( nCmdID, m_iconCustomized );
//	return true;
}

CRuntimeClass * CExtRibbonNode::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
	return NULL;
}

CExtBarButton * CExtRibbonNode::OnRibbonCreateBarButton(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	try
	{
		DWORD dwFlags = GetFlags();
		CExtBarButton * pTBB = NULL;
		CRuntimeClass * pRTC = _OnRibbonGetButtonRTC();
		UINT nCmdIDi = GetCmdID( false );
		UINT nCmdIDe = GetCmdID( true );
		if( nCmdIDi != 0 || nCmdIDe != 0 )
		{
			if( nCmdIDe == 0 )
				nCmdIDe = nCmdIDi;
		}
		if( pRTC != NULL )
		{
			pTBB = (CExtBarButton *)
				pRTC->CreateObject();
			ASSERT_VALID( pTBB );
			ASSERT_KINDOF( CExtBarButton, pTBB );
			pTBB->SetBar( pBar );
			if( nCmdIDi != 0 || nCmdIDe != 0 )
			{
				pTBB->SetCmdID( nCmdIDi, false, false );
				pTBB->SetCmdID( nCmdIDe, true, false );
			} // if( nCmdIDi != 0 || nCmdIDe != 0 )
			if(		nCmdIDi != 0
				||	nCmdIDe != 0
				||	IsKindOf( RUNTIME_CLASS( CExtRibbonNodeMdiRightButtons ) )
				)
			{
				pTBB->SetBasicCmdNode( this );
				pTBB->SetCustomizedCmdNode( this );
			}
			if( (dwFlags&__ECTN_TBB_SEPARATED_DROPDOWN) != 0 )
				pTBB->SetSeparatedDropDown();
			if( (dwFlags&__ECTN_TBB_HIDDEN) != 0 )
				pTBB->ModifyStyle( TBBS_HIDDEN, 0 );
			else
				pTBB->ModifyStyle( 0, TBBS_HIDDEN );
		} // if( pRTC != NULL )
		else
		{
			pTBB =
				pBar->OnCreateToolbarButton(
					pBar,
					this,
					this
					);
			ASSERT_VALID( pTBB );
		} // else from if( pRTC != NULL )
		ASSERT_VALID( pTBB );
		if( pParentTBB != NULL )
		{
			ASSERT_VALID( pParentTBB );
			pParentTBB->ChildButtonAdd( pTBB );
		}
		return pTBB;
	}
	catch( CException * pException )
	{
		pException->Delete();
	}
	return NULL;
}

bool CExtRibbonNode::Ribbon_InitBar(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB, // = NULL
	bool bInsertThisNode // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
#ifdef _DEBUG
	if( pParentTBB != NULL )
	{
		ASSERT_VALID( pParentTBB );
	}
#endif // _DEBUG
CExtCustomizeCmdTreeNode * pParentNode = GetParentNode();
	if(		pParentNode != NULL
		&&	(pParentNode->GetFlags()&__ECTN_DYNAMIC_POPUP) != 0
		)
		return true;
INT nIndex, nCount = GetNodeCount();
CExtBarButton * pTBB = NULL;
	if( bInsertThisNode )
		pTBB = OnRibbonCreateBarButton( pBar, pParentTBB );
bool bWalkChildren = true;
	if(	pParentNode != NULL && pParentNode->IsKindOf( RUNTIME_CLASS( CExtRibbonNodeQuickAccessButtonsCollection ) ) )
		bWalkChildren = false;
	if( bWalkChildren )
	{
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtCustomizeCmdTreeNode * pNode = ElementAt( nIndex );
			ASSERT_VALID( pNode );
			if(		pParentTBB == NULL
				&&	( pNode->GetFlags() & __ECTN_TBB_HIDDEN ) != 0
				)
				continue;
			CExtRibbonNode * pRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNode );
			if( pRibbonNode == NULL )
			{
				continue;
	//			if( pTBB != NULL )
	//				delete pTBB;
	//			return false;
			}
			if( ! pRibbonNode->Ribbon_InitBar( pBar, pTBB, true ) )
			{
				if( pTBB != NULL )
					delete pTBB;
				return false;
			}
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	}
	if( pTBB != NULL )
	{
		if( ! pBar->InsertSpecButton( -1, pTBB, FALSE ) )
		{
			if( pTBB != NULL )
				delete pTBB;
			return false;
		}
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNodeGroup

IMPLEMENT_SERIAL( CExtRibbonNodeGroup, CExtRibbonNode, VERSIONABLE_SCHEMA|1 );

CExtRibbonNodeGroup::CExtRibbonNodeGroup(
	UINT nCmdIdBasic, // = 0L
	CExtRibbonNode * pParentNode, // = NULL
	DWORD dwFlags, // = 0L
	__EXT_MFC_SAFE_LPCTSTR strTextInToolbar, // = NULL
	LPARAM lParam // = 0L
	)
	: CExtRibbonNode(
		nCmdIdBasic,
		nCmdIdBasic,
		pParentNode,
		dwFlags,
		strTextInToolbar,
		strTextInToolbar,
		strTextInToolbar,
		lParam
		)
{
}

CExtRibbonNodeGroup::CExtRibbonNodeGroup(
	CExtRibbonNodeGroup & other
	)
	: CExtRibbonNode( other )
{
	RibbonILE_RuleArrayGet().RemoveAll();
}

CExtRibbonNodeGroup::~CExtRibbonNodeGroup()
{
}

bool CExtRibbonNodeGroup::OnGetCommandsListBoxInfo(
	CExtCustomizeCommandListBox * pLB, // IN (optional)
	CExtCustomizeSite * pSite, // = NULL // IN (optional)
	CExtCmdItem * pCmdItem, // = NULL // IN (optional)
	CExtSafeString * pStrLbText, // = NULL // OUT (optional)
	CExtCmdIcon * pLbIcon, // = NULL // OUT (optional)
	INT nDesiredIconWidth, // = 16 // IN (optional)
	INT nDesiredIconHeight // = 16 // IN (optional)
	)
{
	ASSERT_VALID( this );
CExtCustomizeCmdTreeNode * pParentNode = GetParentNode();
	if( pParentNode == NULL )
		return false;
	ASSERT_VALID( pParentNode );
	if( pParentNode->IsKindOf( RUNTIME_CLASS( CExtRibbonNodeGroup ) ) )
		return false;
	return
		CExtRibbonNode::OnGetCommandsListBoxInfo(
			pLB,
			pSite,
			pCmdItem,
			pStrLbText,
			pLbIcon,
			nDesiredIconWidth,
			nDesiredIconHeight
			);
}

CRuntimeClass * CExtRibbonNodeGroup::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
//	RibbonILE_RuleArrayGet().RemoveAll();
	return ( RUNTIME_CLASS( CExtRibbonButtonGroup ) );
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNodeToolGroup

IMPLEMENT_SERIAL( CExtRibbonNodeToolGroup, CExtRibbonNode, VERSIONABLE_SCHEMA|1 );

CExtRibbonNodeToolGroup::CExtRibbonNodeToolGroup(
	UINT nCmdIdBasic, // = 0L
	CExtRibbonNode * pParentNode, // = NULL
	DWORD dwFlags, // = 0L
	__EXT_MFC_SAFE_LPCTSTR strTextInToolbar, // = NULL
	LPARAM lParam // = 0L
	)
	: CExtRibbonNodeGroup(
		nCmdIdBasic,
		pParentNode,
		dwFlags,
		strTextInToolbar,
		lParam
		)
{
}

CExtRibbonNodeToolGroup::CExtRibbonNodeToolGroup(
	CExtRibbonNodeToolGroup & other
	)
	: CExtRibbonNodeGroup( other )
{
	RibbonILE_RuleArrayGet().RemoveAll();
}

CExtRibbonNodeToolGroup::~CExtRibbonNodeToolGroup()
{
}

CRuntimeClass * CExtRibbonNodeToolGroup::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
//	RibbonILE_RuleArrayGet().RemoveAll();
	return ( RUNTIME_CLASS( CExtRibbonButtonToolGroup ) );
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNodeToolButton

IMPLEMENT_SERIAL( CExtRibbonNodeToolButton, CExtRibbonNode, VERSIONABLE_SCHEMA|1 );

CExtRibbonNodeToolButton::CExtRibbonNodeToolButton(
	UINT nCmdIdBasic, // = 0L
	CExtRibbonNode * pParentNode, // = NULL
	DWORD dwFlags, // = 0L
	__EXT_MFC_SAFE_LPCTSTR strTextInToolbar, // = NULL
	LPARAM lParam // = 0L
	)
	: CExtRibbonNode(
		nCmdIdBasic,
		nCmdIdBasic,
		pParentNode,
		dwFlags,
		strTextInToolbar,
		strTextInToolbar,
		strTextInToolbar,
		lParam
		)
{
}

CExtRibbonNodeToolButton::CExtRibbonNodeToolButton(
	CExtRibbonNodeToolButton & other
	)
	: CExtRibbonNode( other )
{
	RibbonILE_RuleArrayGet().RemoveAll();
}

CExtRibbonNodeToolButton::~CExtRibbonNodeToolButton()
{
}

void CExtRibbonNodeToolButton::_InitMembers()
{
	ASSERT_VALID( this );
	CExtRibbonNode::_InitMembers();
CArray < DWORD, DWORD > & arrRule = RibbonILE_RuleArrayGet();
	arrRule.RemoveAll();
	arrRule.Add(
		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
			__EXT_RIBBON_ILE_MAX,
			__EXT_RIBBON_ILV_SIMPLE_SMALL,
			false
			)
		);
}

CRuntimeClass * CExtRibbonNodeToolButton::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
	_InitMembers();
	return CExtRibbonNode::_OnRibbonGetButtonRTC();
}

INT CExtRibbonNodeToolButton::RibbonILV_Get(
	INT nType // = 0 // -1 min, 0 current, 1 - max
	) const
{
	ASSERT_VALID( this );
	nType;
	return __EXT_RIBBON_ILV_SIMPLE_SMALL;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNodeLabel

IMPLEMENT_SERIAL( CExtRibbonNodeLabel, CExtRibbonNode, VERSIONABLE_SCHEMA|1 );

CExtRibbonNodeLabel::CExtRibbonNodeLabel(
	UINT nCmdIdBasic, // = 0L
	CExtRibbonNode * pParentNode, // = NULL
	DWORD dwFlags, // = 0L
	__EXT_MFC_SAFE_LPCTSTR strTextInToolbar, // = NULL
	LPARAM lParam // = 0L
	)
	: CExtRibbonNode(
		nCmdIdBasic,
		nCmdIdBasic,
		pParentNode,
		dwFlags,
		strTextInToolbar,
		strTextInToolbar,
		strTextInToolbar,
		lParam
		)
{
}

CExtRibbonNodeLabel::CExtRibbonNodeLabel(
	CExtRibbonNodeLabel & other
	)
	: CExtRibbonNode( other )
{
	RibbonILE_RuleArrayGet().RemoveAll();
}

CExtRibbonNodeLabel::~CExtRibbonNodeLabel()
{
}

void CExtRibbonNodeLabel::_InitMembers()
{
	ASSERT_VALID( this );
	CExtRibbonNode::_InitMembers();
CArray < DWORD, DWORD > & arrRule = RibbonILE_RuleArrayGet();
	arrRule.RemoveAll();
	arrRule.Add(
		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
			__EXT_RIBBON_ILE_MAX,
			__EXT_RIBBON_ILV_SIMPLE_NORMAL,
			false
			)
		);
}

CRuntimeClass * CExtRibbonNodeLabel::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
	_InitMembers();
	return RUNTIME_CLASS( CExtBarLabelButton );
}

INT CExtRibbonNodeLabel::RibbonILE_Get() const
{
	ASSERT_VALID( this );
	return __EXT_RIBBON_ILE_MIN;
}

INT CExtRibbonNodeLabel::RibbonILV_Get(
	INT nType // = 0 // -1 min, 0 current, 1 - max
	) const
{
	ASSERT_VALID( this );
	nType;
	return __EXT_RIBBON_ILV_SIMPLE_SMALL;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNodeContractExpand

IMPLEMENT_SERIAL( CExtRibbonNodeContractExpand, CExtRibbonNode, VERSIONABLE_SCHEMA|1 );

CExtRibbonNodeContractExpand::CExtRibbonNodeContractExpand(
	CExtRibbonNode * pParentNode, // = NULL
	DWORD dwFlags, // = 0L
	LPARAM lParam // = 0L
	)
	: CExtRibbonNode(
		ID_EXT_RIBBON_CONTRACT_EXPAND_BUTTON,
		ID_EXT_RIBBON_CONTRACT_EXPAND_BUTTON,
		pParentNode,
		dwFlags,
		_T(""),
		_T(""),
		_T(""),
		lParam
		)
{
}

CExtRibbonNodeContractExpand::CExtRibbonNodeContractExpand(
	CExtRibbonNodeContractExpand & other
	)
	: CExtRibbonNode( other )
{
	RibbonILE_RuleArrayGet().RemoveAll();
}

CExtRibbonNodeContractExpand::~CExtRibbonNodeContractExpand()
{
}

void CExtRibbonNodeContractExpand::_InitMembers()
{
	ASSERT_VALID( this );
	CExtRibbonNode::_InitMembers();
CArray < DWORD, DWORD > & arrRule = RibbonILE_RuleArrayGet();
	arrRule.RemoveAll();
	arrRule.Add(
		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
			__EXT_RIBBON_ILE_MAX,
			__EXT_RIBBON_ILV_SIMPLE_SMALL,
			false
			)
		);
}

CRuntimeClass * CExtRibbonNodeContractExpand::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
	_InitMembers();
	return RUNTIME_CLASS( CExtRibbonContractExpandButton );
}

INT CExtRibbonNodeContractExpand::RibbonILE_Get() const
{
	ASSERT_VALID( this );
	return __EXT_RIBBON_ILE_MIN;
}

INT CExtRibbonNodeContractExpand::RibbonILV_Get(
	INT nType // = 0 // -1 min, 0 current, 1 - max
	) const
{
	ASSERT_VALID( this );
	nType;
	return __EXT_RIBBON_ILV_SIMPLE_SMALL;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonContractExpandButton

IMPLEMENT_DYNCREATE( CExtRibbonContractExpandButton, CExtBarButton );

CExtRibbonContractExpandButton::CExtRibbonContractExpandButton(
	CExtRibbonPage * pBar, // = NULL
	UINT nCmdID, // = ID_SEPARATOR
	UINT nStyle // = 0
	)
	: CExtBarButton( pBar, nCmdID, nStyle )
{
	ModifyStyle( 0, TBBS_SEPARATOR );
}

CExtRibbonContractExpandButton::~CExtRibbonContractExpandButton()
{
}

CExtSafeString CExtRibbonContractExpandButton::GetText() const
{
	return _T("");
}


bool CExtRibbonContractExpandButton::IsDisabled() const
{
	ASSERT_VALID( this );
	return false;
}

bool CExtRibbonContractExpandButton::IsSeparator() const
{
	ASSERT_VALID( this );
	return false;
}

bool CExtRibbonContractExpandButton::IsNoRibbonLayout() const
{
	ASSERT_VALID( this );
	return false;
}

INT CExtRibbonContractExpandButton::RibbonILV_Get(
	INT nType // = 0 // -1 min, 0 current, 1 - max
	) const
{
	ASSERT_VALID( this );
	nType;
	return __EXT_RIBBON_ILV_SIMPLE_SMALL;
}

bool CExtRibbonContractExpandButton::IsAbleToTrackMenu(
	bool bCustomizeMode // = false
	) const
{
	ASSERT_VALID( this );
	bCustomizeMode;
	return false;
}

void CExtRibbonContractExpandButton::OnUpdateCmdUI(
	CWnd * pTarget,
	BOOL bDisableIfNoHndler,
	int nIndex
	)
{
	ASSERT_VALID( this );
	pTarget;
	bDisableIfNoHndler;
	nIndex;
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
UINT nOldStyle = pBar->GetButtonStyle( nIndex );
UINT nNewStyle = nOldStyle & (~(TBBS_DISABLED));
	if( nOldStyle != nNewStyle )
		pBar->SetButtonStyle( nIndex, nNewStyle );
}

CExtCmdIcon * CExtRibbonContractExpandButton::GetIconPtr()
{
	ASSERT_VALID( this );
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
CExtRibbonBar * pRibbonBar = DYNAMIC_DOWNCAST( CExtRibbonBar, pBar );
	if( pRibbonBar == NULL )
		return NULL;
	if( m_iconContract.IsEmpty() )
		{ VERIFY( m_iconContract.m_bmpNormal.LoadBMP_Resource( MAKEINTRESOURCE(ID_EXT_RIBBON_CONTRACT_EXPAND_BUTTON) ) ); }
	if( m_iconExpand.IsEmpty() )
	{
		m_iconExpand.m_bmpNormal = m_iconContract.m_bmpNormal;
		VERIFY( m_iconExpand.m_bmpNormal.FlipVertical() );
	}
	if( pRibbonBar->RibbonPage_ExpandedModeGet() )
		return (&m_iconContract);
	else
		return (&m_iconExpand);
}

void CExtRibbonContractExpandButton::OnDeliverCmd()
{
	ASSERT_VALID( this );
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
CExtRibbonBar * pRibbonBar = DYNAMIC_DOWNCAST( CExtRibbonBar, pBar );
	if( pRibbonBar == NULL )
		return;
	pRibbonBar->RibbonPage_ExpandedModeSet( ! pRibbonBar->RibbonPage_ExpandedModeGet(), true );
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonButtonQuickAccessContentExpand

IMPLEMENT_DYNCREATE( CExtRibbonButtonQuickAccessContentExpand, CExtBarButton );

CExtRibbonButtonQuickAccessContentExpand::CExtRibbonButtonQuickAccessContentExpand(
	CExtRibbonPage * pBar, // = NULL
	UINT nCmdID, // = ID_SEPARATOR
	UINT nStyle // = 0
	)
	: CExtBarButton( pBar, nCmdID, nStyle )
{
	ModifyStyle( 0, TBBS_SEPARATOR );
}

CExtRibbonButtonQuickAccessContentExpand::~CExtRibbonButtonQuickAccessContentExpand()
{
}

CExtSafeString CExtRibbonButtonQuickAccessContentExpand::GetText() const
{
	return _T("");
}

bool CExtRibbonButtonQuickAccessContentExpand::IsDisabled() const
{
	ASSERT_VALID( this );
	return false;
}

bool CExtRibbonButtonQuickAccessContentExpand::IsSeparator() const
{
	ASSERT_VALID( this );
	return false;
}

bool CExtRibbonButtonQuickAccessContentExpand::IsNoRibbonLayout() const
{
	ASSERT_VALID( this );
	return false;
}

INT CExtRibbonButtonQuickAccessContentExpand::RibbonILV_Get(
	INT nType // = 0 // -1 min, 0 current, 1 - max
	) const
{
	ASSERT_VALID( this );
	nType;
	return __EXT_RIBBON_ILV_SIMPLE_SMALL;
}

CSize CExtRibbonButtonQuickAccessContentExpand::CalculateLayout(
	CDC & dc,
	CSize sizePreCalc,
	BOOL bHorz
	)
{
	ASSERT_VALID( this );
	sizePreCalc;
	bHorz;
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
CExtRibbonBar * pRibbonBar = DYNAMIC_DOWNCAST( CExtRibbonBar, pBar );
	if( pRibbonBar == NULL )
		return CSize( 0, 0 );
	if( ! pRibbonBar->Ribbon_QuickAccessContentExpandButtonCalcLayout( dc ) )
		return CSize( 0, 0 );
	return m_ActiveSize;
}

void CExtRibbonButtonQuickAccessContentExpand::PaintCompound(
	CDC & dc,
	bool bPaintParentChain,
	bool bPaintChildren,
	bool bPaintOneNearestChildrenLevelOnly
	)
{
	ASSERT_VALID( this );

	if( ! IsPaintAble( dc ) )
		return;
	if( AnimationClient_StatePaint( dc ) )
		return;
	if( bPaintParentChain )
		PaintParentChain( dc );

CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
CExtRibbonBar * pRibbonBar =
		DYNAMIC_DOWNCAST( CExtRibbonBar, pBar );
	if( pRibbonBar == NULL )
		return;
	pRibbonBar->Ribbon_QuickAccessContentExpandButtonPaint( dc );

	if( bPaintChildren )
		PaintChildren( dc, bPaintOneNearestChildrenLevelOnly );
}

bool CExtRibbonButtonQuickAccessContentExpand::IsAbleToTrackMenu(
	bool bCustomizeMode // = false
	) const
{
	ASSERT_VALID( this );
	bCustomizeMode;
	return true;
}

void CExtRibbonButtonQuickAccessContentExpand::OnUpdateCmdUI(
	CWnd * pTarget,
	BOOL bDisableIfNoHndler,
	int nIndex
	)
{
	ASSERT_VALID( this );
	pTarget;
	bDisableIfNoHndler;
	nIndex;
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
UINT nOldStyle = pBar->GetButtonStyle( nIndex );
UINT nNewStyle = nOldStyle & (~(TBBS_DISABLED));
	if( nOldStyle != nNewStyle )
		pBar->SetButtonStyle( nIndex, nNewStyle );
}

UINT CExtRibbonButtonQuickAccessContentExpand::OnTrackPopup(
	CPoint point,
	bool bSelectAny,
	bool bForceNoAnimation
	)
{
	ASSERT_VALID( this );
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
CExtRibbonBar * pRibbonBar =
		DYNAMIC_DOWNCAST( CExtRibbonBar, pBar );
	if( pRibbonBar == NULL )
		return UINT(-1L);

bool bDockSiteCustomizeMode =
		pBar->_IsDockSiteCustomizeMode();
	if( bDockSiteCustomizeMode )
		return UINT(-1L);

	if(		IsDisabled()
		&&	(! CanBePressedInDisabledState() )
		)
		return UINT(-1L);

	if( ! IsAbleToTrackMenu() )
		return UINT(-1L);
	
CRect rcBtn = Rect();
	pBar->ClientToScreen( &rcBtn );
	pBar->ClientToScreen( &point );

CExtSafeString strMenuCaption;
DWORD dwTrackFlags = OnGetTrackPopupFlags() | TPMX_OWNERDRAW_FIXED;
#if (!defined __EXT_MFC_NO_RIBBON_BAR)
	if( pBar->IsKindOf( RUNTIME_CLASS( CExtRibbonPage ) ) )
	{
		dwTrackFlags |= TPMX_COMBINE_NONE|TPMX_RIBBON_MODE|TPMX_NO_HIDE_RARELY;
		g_ResourceManager->LoadString( strMenuCaption, IDS_EXT_RIBBON_QATB_MENU_CAPTION );
	}
	else
		dwTrackFlags |= TPMX_COMBINE_DEFAULT;
#else // (!defined __EXT_MFC_NO_RIBBON_BAR)
		dwTrackFlags |= TPMX_COMBINE_DEFAULT;
#endif // (!defined __EXT_MFC_NO_RIBBON_BAR)

	if( bSelectAny )
		dwTrackFlags |= TPMX_SELECT_ANY;
	if( CExtToolControlBar::g_bMenuTrackingExpanded )
		dwTrackFlags |= TPMX_NO_HIDE_RARELY;
	if( bForceNoAnimation )
		dwTrackFlags |= TPMX_FORCE_NO_ANIMATION;


	CExtToolControlBar::g_bMenuTracking = false;

CExtPopupMenuWnd * pPopup =
		CExtPopupMenuWnd::InstantiatePopupMenu(
			pBar->GetSafeHwnd(),
			RUNTIME_CLASS(CExtPopupMenuWnd),
			this
			);
	if( ! pPopup->LoadMenu(
			pBar->GetSafeHwnd(),
			IDR_EXT_RIBON_CTX_MENU,
			true,
			true
			)
		)
	{
		delete pPopup;
		CExtToolControlBar::_CloseTrackingMenus();
		return UINT(-1L);
	}
	if( ! strMenuCaption.IsEmpty() )
	{
		pPopup->MenuCaptionTextSet( LPCTSTR(strMenuCaption) );
	} // if( ! strMenuCaption.IsEmpty() )
bool bRibbonQuickAccessBarIsAboveTheRibbon = pRibbonBar->RibbonQuickAccessBar_AboveTheRibbonGet();
INT nIndex, nCount = pPopup->ItemGetCount();
	for( nIndex = 0; nIndex < nCount; )
	{
		UINT nCmdID = pPopup->ItemGetCmdID( nIndex );
		switch( nCmdID )
		{
		case ID_EXT_RIBBON_MINIMIZE:
#if (!defined __EXT_MFC_NO_RIBBON_BAR)
			//if( pRibbonBar->Ribbon_AutoHideModeEnabledGet() )
			{
				CExtPopupMenuWnd::MENUITEMDATA & mi =
					pPopup->ItemGetInfo( nIndex );
				mi.SetNoCmdUI( true );
				mi.Enable( true );
				mi.Check( ! pRibbonBar->RibbonPage_ExpandedModeGet() );
				if( nIndex > 0 )
				{
					pPopup->ItemInsert( ID_SEPARATOR, nIndex );
					nIndex ++;
				}
				nIndex ++;
				continue;
			} // if( pRibbonBar->Ribbon_AutoHideModeEnabledGet() )
#endif // (!defined __EXT_MFC_NO_RIBBON_BAR)
			break;

		case ID_EXT_RIBBON_QATB_PLACE_BELOW:
			if( bRibbonQuickAccessBarIsAboveTheRibbon )
			{
				nIndex ++;
				continue;
			}
			break;
		case ID_EXT_RIBBON_QATB_PLACE_ABOVE:
			if( ! bRibbonQuickAccessBarIsAboveTheRibbon )
			{
				nIndex ++;
				continue;
			}
			break;
		case ID_EXT_RIBBON_QATB_RESET:
		case ID_EXT_RIBBON_QATB_CUSOMIZE:
			nIndex ++;
			continue;
		} // switch( nCmdID )
		pPopup->ItemRemove( nIndex );
		nCount --;
	} // for( nIndex = 0; nIndex < nCount; )
	
	pPopup->m_hWndNotifyMenuClosed = pBar->GetSafeHwnd();
CSize _sizeAboveTheRibbonShiftQACEB( 0, 0 );
CPoint ptTrack = point;
CRect rcBtnTrack = rcBtn;
	{ // BLOCK
		CClientDC dc( pRibbonBar );
		_sizeAboveTheRibbonShiftQACEB =
			pRibbonBar->PmBridge_GetPM()->
				Ribbon_QuickAccessContentExpandGetShift(
					dc,
					this
					);
		ptTrack += _sizeAboveTheRibbonShiftQACEB;
		rcBtnTrack.left += _sizeAboveTheRibbonShiftQACEB.cx;
		rcBtnTrack.bottom += _sizeAboveTheRibbonShiftQACEB.cy;
	} // BLOCK
	if( GetSeparatedDropDown() )
		m_bDropDownHT = true;
	g_pTrackingMenuTBB = this;
	if( ! pPopup->TrackPopupMenu(
			dwTrackFlags,
			ptTrack.x,
			ptTrack.y,
			&rcBtnTrack,
			GetBar(),
			CExtToolControlBar::_CbPaintCombinedContent,
			NULL,
			true
			)
		)
	{
		g_pTrackingMenuTBB = NULL;
		//delete pPopup;
		CExtToolControlBar::_CloseTrackingMenus();
		return UINT(-1L);
	}
	CExtToolControlBar::g_bMenuTracking = true;
	pBar->_SwitchMenuTrackingIndex(
		pBar->_GetIndexOf( this )
		);

	return UINT(-1L);
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonButtonFile

IMPLEMENT_DYNCREATE( CExtRibbonButtonFile, CExtRibbonButtonGallery );

CExtRibbonButtonFile::CExtRibbonButtonFile(
	CExtRibbonPage * pBar, // = NULL
	UINT nCmdID, // = __EXT_MFC_ID_RIBBON_APPLICATION_BUTTON
	UINT nStyle // = 0
	)
	: CExtRibbonButtonGallery( pBar, nCmdID, nStyle )
	, m_bHelperCustomLayoutApplied( false )
{
	ModifyStyle( 0, TBBS_SEPARATOR );
}

CExtRibbonButtonFile::~CExtRibbonButtonFile()
{
}

bool CExtRibbonButtonFile::OnRibbonFileMenuButtonQuery(
	CExtPopupMenuWnd::RIBBONFILEMENUBUTTONQUERY & _rfmbq,
	CExtCmdIcon & _icon,
	CExtSafeString & sText
	)
{
	ASSERT_VALID( this );
	_rfmbq;
	_icon;
	sText;
	return false;
}

bool CExtRibbonButtonFile::OnRibbonFileMenuButtonInvocation(
	CExtPopupMenuWnd::RIBBONFILEMENUBUTTONINVOCATION & _rfmbi
	)
{
	ASSERT_VALID( this );
	_rfmbi;
	return false;
}

CExtCustomizeCmdTreeNode * CExtRibbonButtonFile::GetCmdNode(
	bool bInitial // = false
	)
{
	ASSERT_VALID( this );
	bInitial;
CExtToolControlBar * pBar = GetBar();
	if( pBar == NULL )
		return NULL;
	ASSERT_VALID( pBar );
CExtRibbonBar * pRibbonBar = DYNAMIC_DOWNCAST( CExtRibbonBar, pBar );
	if( pRibbonBar == NULL )
		return NULL;
CExtRibbonNodeFile * pRibbonNodeFile = pRibbonBar->Ribbon_GetFileRootNode();
	return pRibbonNodeFile;
}

CExtSafeString CExtRibbonButtonFile::GetText() const
{
CExtSafeString s;
	if( ! g_ResourceManager->LoadString( s, IDS_EXT_RIBBON_FILE_BUTTON_TEXT ) )
		s = _T("File");
	return s;
}

bool CExtRibbonButtonFile::IsSeparator() const
{
	ASSERT_VALID( this );
	return false;
}

bool CExtRibbonButtonFile::IsNoRibbonLayout() const
{
	ASSERT_VALID( this );
	return false;
}

bool CExtRibbonButtonFile::IsInplaceGalleryMode() const
{
	ASSERT_VALID( this );
	return false;
}

void CExtRibbonButtonFile::InplaceGalleryEnsureCreated()
{
	ASSERT_VALID( this );
}

INT CExtRibbonButtonFile::RibbonILV_Get(
	INT nType // = 0 // -1 min, 0 current, 1 - max
	) const
{
	ASSERT_VALID( this );
	nType;
	return __EXT_RIBBON_ILV_SIMPLE_NORMAL;
}

CSize CExtRibbonButtonFile::CalculateLayout(
	CDC & dc,
	CSize sizePreCalc,
	BOOL bHorz
	)
{
	ASSERT_VALID( this );
	sizePreCalc;
	bHorz;
	m_bHelperCustomLayoutApplied = false;
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
CExtRibbonBar * pRibbonBar =
		DYNAMIC_DOWNCAST( CExtRibbonBar, pBar );
	if(		pRibbonBar != NULL
		&&	pRibbonBar->Ribbon_FileButtonCalcLayout( dc )
		)
	{
		m_bHelperCustomLayoutApplied = true;
		return m_ActiveSize;
	}
CExtSafeString sText = GetText();
	m_ActiveSize =
		CExtPaintManager::stat_CalcTextDimension(
			dc,
			*( pBar->OnGetToolbarFont( false, true, this ) ),
			sText
			).Size();
	m_ActiveSize.cx += 16;
	m_ActiveSize.cy += 8;
	return m_ActiveSize;
}

void CExtRibbonButtonFile::PaintCompound(
	CDC & dc,
	bool bPaintParentChain,
	bool bPaintChildren,
	bool bPaintOneNearestChildrenLevelOnly
	)
{
	ASSERT_VALID( this );

	if( ! IsPaintAble( dc ) )
		return;
	if( AnimationClient_StatePaint( dc ) )
		return;
	if( bPaintParentChain )
		PaintParentChain( dc );

CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
CExtRibbonBar * pRibbonBar =
		DYNAMIC_DOWNCAST( CExtRibbonBar, pBar );
	if(		pRibbonBar != NULL
		&&	pRibbonBar->Ribbon_FileButtonPaint( dc )
		)
		return;
CExtSafeString sText = GetText();
CExtPaintManager::PAINTPUSHBUTTONDATA _ppbd(
		this,
		true,
		*this,
		sText,
		NULL,
		true,
		IsHover(),
		IsPressed(),
		false,
		IsEnabled(),
		true,false,false,
		CExtPaintManager::__ALIGN_HORIZ_CENTER
			| CExtPaintManager::__ALIGN_VERT_CENTER,
		NULL,
		false,
		0,
		true
		);
	pBar->PmBridge_GetPM()->PaintPushButton( dc, _ppbd );

	if( bPaintChildren )
		PaintChildren( dc, bPaintOneNearestChildrenLevelOnly );
}

void CExtRibbonButtonFile::OnUpdateCmdUI(
	CWnd * pTarget,
	BOOL bDisableIfNoHndler,
	int nIndex
	)
{
	ASSERT_VALID( this );
	pTarget;
	bDisableIfNoHndler;
	nIndex;
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
UINT nOldStyle = pBar->GetButtonStyle( nIndex );
UINT nNewStyle = nOldStyle & (~(TBBS_DISABLED));
	if( nOldStyle != nNewStyle )
		pBar->SetButtonStyle( nIndex, nNewStyle );
}

bool CExtRibbonButtonFile::IsAbleToTrackMenu(
	bool bCustomizeMode // = false
	) const
{
	ASSERT_VALID( this );
	bCustomizeMode;
	return true;
}

void CExtRibbonButtonFile::OnClick(
	CPoint point,
	bool bDown
	)
{
	ASSERT_VALID( this );
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
	if( pBar->_OnHookButtonClick( this, point, bDown ) )
		return;
CExtRibbonPage * pRibbonPage = STATIC_DOWNCAST( CExtRibbonPage, GetBar() );
	ASSERT_VALID( pRibbonPage );
	pRibbonPage->_CheckHitTestablePoint( point );
	if( point.x == 32767 || point.y == 32767 )
		return;
	CExtRibbonButtonGallery::OnClick( point, bDown );
}

bool CExtRibbonButtonFile::OnKeyTipInvokeAction(
	bool & bContinueKeyTipMode
	)
{
	ASSERT_VALID( this );
	return CExtBarButton::OnKeyTipInvokeAction( bContinueKeyTipMode );
}

bool CExtRibbonButtonFile::OnDblClick(
	CPoint point
	)
{
	ASSERT_VALID( this );
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
	if( pBar->_OnHookButtonDblClick( this, point ) )
		return true;
CExtRibbonPage * pRibbonPage = STATIC_DOWNCAST( CExtRibbonPage, GetBar() );
	ASSERT_VALID( pRibbonPage );
	pRibbonPage->_CheckHitTestablePoint( point );
	if( point.x == 32767 || point.y == 32767 )
		return false;
	return CExtRibbonButtonGallery::OnDblClick( point );
}

void CExtRibbonButtonFile::OnHover(
	CPoint point,
	bool bOn,
	bool & bSuspendTips
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = STATIC_DOWNCAST( CExtRibbonPage, GetBar() );
	ASSERT_VALID( pRibbonPage );
	pRibbonPage->_CheckHitTestablePoint( point );
	if( point.x == 32767 || point.y == 32767 )
		return;
	CExtRibbonButtonGallery::OnHover( point, bOn, bSuspendTips );
}

UINT CExtRibbonButtonFile::OnTrackPopup(
	CPoint point,
	bool bSelectAny,
	bool bForceNoAnimation
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = STATIC_DOWNCAST( CExtRibbonPage, GetBar() );
	ASSERT_VALID( pRibbonPage );
	bForceNoAnimation;
bool bDockSiteCustomizeMode =
		pRibbonPage->_IsDockSiteCustomizeMode();
	if( bDockSiteCustomizeMode )
		return UINT(-1L);
	if(		IsDisabled()
		&&	(! CanBePressedInDisabledState() )
		)
		return UINT(-1L);
	pRibbonPage->_CheckHitTestablePoint( point );
	if( point.x == 32767 || point.y == 32767 )
		return UINT(-1L);
CExtRibbonBar * pRibbonBar = DYNAMIC_DOWNCAST( CExtRibbonBar, pRibbonPage );
	if( pRibbonBar != NULL )
	{
		pRibbonBar->_RibbonBarKeyTipShow( false, false );
		CExtPaintManager::stat_PassPaintMessages();
#if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
		if( pRibbonBar->BackstageView_IsSupported() )
		{
			pRibbonBar->BackstageView_Toggle( bSelectAny );
			return UINT(-1L);
		}
#endif // (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	} // if( pRibbonBar != NULL )
CExtCustomizeCmdTreeNode * pNode = GetCmdNode();
	if( pNode == NULL )
		return UINT(-1L);
	ASSERT_VALID( pNode );
CExtRibbonNodeGallery * pGalleryNode =
		DYNAMIC_DOWNCAST( CExtRibbonNodeGallery, pNode );
	if( pGalleryNode == NULL )
		return UINT(-1L);

	if( ! IsAbleToTrackMenu() )
		return UINT(-1L);
	
bool bPrevTBMT = CExtToolControlBar::g_bMenuTracking;
	if( bPrevTBMT )
		return UINT(-1L);

	if(		CExtToolControlBar::g_bMenuTracking
		&&	pRibbonPage->_GetIndexOf(this) ==
				pRibbonPage->GetMenuTrackingButton()
		)
		return UINT(-1L);

	CExtToolControlBar::_CloseTrackingMenus();

//	if( pRibbonPage->IsFloating() )
//	{
//		pRibbonPage->ActivateTopParent();
//		CFrameWnd * pFrame =
//			pRibbonPage->GetDockingFrame();
//		ASSERT_VALID( pFrame );
//		pFrame->BringWindowToTop();
//	}

CWnd * pWndCmdTarget = GetCmdTargetWnd();
	ASSERT_VALID( pWndCmdTarget );

	CExtToolControlBar::g_bMenuTracking = bPrevTBMT;

CRect rcBtn = Rect();
	pRibbonPage->ClientToScreen( &rcBtn );
	pRibbonPage->ClientToScreen( &point );

DWORD dwTrackFlags =
		OnGetTrackPopupFlags()
			| TPMX_COMBINE_NONE
			| TPMX_OWNERDRAW_FIXED
//			| TPMX_NO_SITE
			| TPMX_NO_HIDE_RARELY
			| TPMX_FORCE_NO_ANIMATION
			| TPMX_RIBBON_MODE
			;
	if( bSelectAny )
		dwTrackFlags |= TPMX_SELECT_ANY;
//	if( CExtToolControlBar::g_bMenuTrackingExpanded )
//		dwTrackFlags |= TPMX_NO_HIDE_RARELY;
//	if( bForceNoAnimation )
//		dwTrackFlags |= TPMX_FORCE_NO_ANIMATION;

	pRibbonPage->_SwitchMenuTrackingIndex(
		pRibbonPage->_GetIndexOf( this )
		);

HWND hWndTrack = GetCmdTargetWnd()->GetSafeHwnd();
CExtRibbonGalleryPopupMenuWnd * pPopup =
		new CExtRibbonGalleryPopupMenuWnd( pRibbonPage->m_hWnd, this );
	pPopup->m_wndRibbonGallery.m_pRibbonGalleryTBB = this;
	pPopup->m_hWndNotifyMenuClosed = pRibbonPage->GetSafeHwnd();
	pPopup->m_bRibbonFileMenuMode = true;
	pPopup->m_bGalleryTrackingMode = false;
	if(		( ! pPopup->CreatePopupMenu( hWndTrack ) )
		||	(! pPopup->UpdateFromCmdTree(
				hWndTrack,
				GetCmdNode()
				) )
		)
	{
		delete pPopup;
		CExtToolControlBar::_CloseTrackingMenus();
		return UINT(-1L);
	}
// 	if( bSelectAny )
// 	{
// 	} // if( bSelectAny )

//	pPopup->m_sizeChildControl = CSize( 350, 330 );
	pPopup->m_sizeChildControl = pGalleryNode->m_sizePopupGalleryControl;
	pPopup->m_sizeChildControlMin = pGalleryNode->m_sizePopupGalleryControlMin;
	pPopup->m_sizeChildControlMax = pGalleryNode->m_sizePopupGalleryControlMax;


	pPopup->m_hWndNotifyMenuClosed = pRibbonPage->GetSafeHwnd();

CPoint ptTrack = point;
CRect rcBtnTrack = rcBtn;
	if(		pRibbonBar != NULL
		&&	pRibbonBar->m_pExtNcFrameImpl != NULL
		&&	pRibbonBar->m_pExtNcFrameImpl->NcFrameImpl_IsSupported()
		&&	pRibbonBar->RibbonLayout_IsFrameIntegrationEnabled()
		)
	{
		INT nRibbonFileButtonMenuIntersectionHeight =
			pRibbonBar->PmBridge_GetPM()->
				Ribbon_FileButtonGetMenuIntersectionHeight();
		rcBtnTrack.bottom -= nRibbonFileButtonMenuIntersectionHeight;
	} // if( pRibbonBar != NULL ...

	if( pGalleryNode->IsKindOf( RUNTIME_CLASS(CExtRibbonNodeFile) ) )
	{
		pPopup->m_keyTipOptionsButton = ((CExtRibbonNodeFile*)pGalleryNode)->m_keyTipOptionsButton;
		pPopup->m_keyTipExitButton = ((CExtRibbonNodeFile*)pGalleryNode)->m_keyTipExitButton;
	}

	CExtPaintManager::stat_PassPaintMessages();
	if( GetSeparatedDropDown() )
		m_bDropDownHT = true;
	g_pTrackingMenuTBB = this;
	if( ! pPopup->TrackPopupMenu(
			dwTrackFlags,
			ptTrack.x,
			ptTrack.y,
			(&rcBtnTrack),
			pRibbonPage,
			CExtToolControlBar::_CbPaintCombinedContent,
			NULL,
			true
			)
		)
	{
		g_pTrackingMenuTBB = NULL;
		//delete pPopup;
		CExtToolControlBar::_CloseTrackingMenus();
		return UINT(-1L);
	}
	CExtToolControlBar::g_bMenuTracking = true;
	pRibbonPage->_SwitchMenuTrackingIndex(
		pRibbonPage->_GetIndexOf( this )
		);

	return UINT(-1L);
}

void CExtRibbonButtonFile::AnimationClient_NextStatePrepare(
	CDC & dc,
	const RECT & rcAcAnimationTarget,
	bool bAnimate,
	INT eAPT // __EAPT_*** animation type
	)
{
	ASSERT( this != NULL );
	ASSERT( dc.GetSafeHdc() != NULL );
	dc;
	rcAcAnimationTarget;
	bAnimate;
	eAPT;
CExtRibbonBar * pRibbonBar = DYNAMIC_DOWNCAST( CExtRibbonBar, GetBar() );
	if(		pRibbonBar != NULL
		&&	pRibbonBar->m_pExtNcFrameImpl != NULL
		&&	pRibbonBar->m_pExtNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement()
		)
		dc.FillSolidRect( &rcAcAnimationTarget, RGB(0,0,0) );
}

void CExtRibbonButtonFile::AnimationClient_NextStateAdjust(
	CExtBitmap & _bmp
	)
{
	ASSERT( this != NULL );
	if(  _bmp.IsEmpty() )
		return;
CExtRibbonBar * pRibbonBar = DYNAMIC_DOWNCAST( CExtRibbonBar, GetBar() );
	if(		pRibbonBar != NULL
		&&	pRibbonBar->m_pExtNcFrameImpl != NULL
		&&	pRibbonBar->m_pExtNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement()
		)
		_bmp.AlphaColor( RGB(0,0,0), RGB(0,0,0), BYTE(0) );
}

CPoint CExtRibbonButtonFile::OnKeyTipGetGuideLines()
{
	ASSERT_VALID( this );
CExtToolControlBar * pToolBar = GetBar();
	ASSERT_VALID( pToolBar );
	ASSERT( pToolBar->GetSafeHwnd() != NULL );
CRect rc = *this;
	pToolBar->ClientToScreen( &rc );
CPoint ptGuideLines(
		rc.left + rc.Width() / 2,
		rc.top + rc.Height() / 2
		);
	return ptGuideLines;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonButtonDialogLauncher

IMPLEMENT_DYNCREATE( CExtRibbonButtonDialogLauncher, CExtBarButton );

CExtRibbonButtonDialogLauncher::CExtRibbonButtonDialogLauncher(
	CExtRibbonPage * pBar, // = NULL
	UINT nCmdID, // = ID_SEPARATOR
	UINT nStyle // = 0
	)
	: CExtBarButton( pBar, nCmdID, nStyle )
{
	ModifyStyle( 0, TBBS_SEPARATOR );
}

CExtRibbonButtonDialogLauncher::~CExtRibbonButtonDialogLauncher()
{
}

bool CExtRibbonButtonDialogLauncher::IsRibbonPaintingMode() const
{
	ASSERT_VALID( this );
	return false;
}

CExtSafeString CExtRibbonButtonDialogLauncher::GetText() const
{
	ASSERT_VALID( this );
	return _T("");
}

CExtCmdIcon * CExtRibbonButtonDialogLauncher::GetIconPtr()
{
	ASSERT_VALID( this );
	return NULL;
}

bool CExtRibbonButtonDialogLauncher::IsSeparator() const
{
	ASSERT_VALID( this );
	return false;
}

bool CExtRibbonButtonDialogLauncher::IsVisible() const
{
	ASSERT_VALID( this );
const CExtToolControlBar * pBar = GetSafeBar();
	if( pBar != NULL )
	{
		ASSERT_VALID( pBar );
		CExtPaintManager * pPM = pBar->PmBridge_GetPM();
		ASSERT_VALID( pPM );
		if( ! pPM->Ribbon_DLB_IsVisible(
				const_cast < CExtRibbonButtonDialogLauncher * >
				( this )
				)
			)
			return false;
		CExtRibbonPage * pRibbonPage =
			DYNAMIC_DOWNCAST( CExtRibbonPage, pBar );
		if( pRibbonPage == NULL )
			return false;
		const CExtBarButton * pTBB = ParentButtonGet();
		if(		pTBB != NULL
			&&	pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonGroup  ) )
			&&	(	( ! pRibbonPage->RibbonLayout_GroupCaptionIsVisible( ((CExtRibbonButtonGroup*)pTBB) ) )
				||	(	pRibbonPage->m_bHelperPopupMode
					&&	( ! pRibbonPage->m_bHelperAutoHideMode )
					&&	( ! pRibbonPage->PmBridge_GetPM()->Ribbon_IsPopupGroupWithCaption( ((CExtRibbonButtonGroup*)pTBB) ) )
					)
				)
			)
			return false;
	} // if( pBar != NULL )
	return CExtBarButton::IsVisible();
}

bool CExtRibbonButtonDialogLauncher::IsNoRibbonLayout() const
{
	ASSERT_VALID( this );
	return true;
}

INT CExtRibbonButtonDialogLauncher::RibbonILV_Get(
	INT nType // = 0 // -1 min, 0 current, 1 - max
	) const
{
	ASSERT_VALID( this );
	nType;
	return __EXT_RIBBON_ILV_SIMPLE_SMALL;
}

CSize CExtRibbonButtonDialogLauncher::RibbonILV_CalcSize(
	CDC & dc,
	INT nILV // = -1 // -1 use current visual level
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&dc) );
	ASSERT( dc.GetSafeHdc() != NULL );
	nILV;
CExtToolControlBar * pBar =
		const_cast < CExtToolControlBar * > ( GetBar() );
	ASSERT_VALID( pBar );
CSize _size =
		pBar->PmBridge_GetPM()->Ribbon_DLB_CalcSize(
			dc,
			const_cast < CExtRibbonButtonDialogLauncher * > ( this )
			);
	return _size;
}

CSize CExtRibbonButtonDialogLauncher::CalculateLayout(
	CDC & dc,
	CSize sizePreCalc,
	BOOL bHorz
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&dc) );
	ASSERT( dc.GetSafeHdc() != NULL );
	sizePreCalc;
	bHorz;
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
	m_ActiveSize =
		pBar->PmBridge_GetPM()->Ribbon_DLB_CalcSize(
			dc,
			this
			);
	return m_ActiveSize;
}

void CExtRibbonButtonDialogLauncher::PaintCompound(
	CDC & dc,
	bool bPaintParentChain,
	bool bPaintChildren,
	bool bPaintOneNearestChildrenLevelOnly
	)
{
	ASSERT_VALID( this );

	if( ! IsPaintAble( dc ) )
		return;
	if( AnimationClient_StatePaint( dc ) )
		return;
	if( bPaintParentChain )
		PaintParentChain( dc );

CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
	pBar->PmBridge_GetPM()->Ribbon_DLB_Paint(
		dc,
		this
		);

	if( bPaintChildren )
		PaintChildren( dc, bPaintOneNearestChildrenLevelOnly );
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNodeMdiRightButtons

IMPLEMENT_SERIAL( CExtRibbonNodeMdiRightButtons, CExtRibbonNode, VERSIONABLE_SCHEMA|1 );

CExtRibbonNodeMdiRightButtons::CExtRibbonNodeMdiRightButtons(
	CExtRibbonNode * pParentNode, // = NULL
	LPARAM lParam // = 0L
	)
	: CExtRibbonNode(
		ID_SEPARATOR,
		ID_SEPARATOR,
		pParentNode,
		0L,
		NULL,
		NULL,
		NULL,
		lParam
		)
{
}

CExtRibbonNodeMdiRightButtons::CExtRibbonNodeMdiRightButtons(
	CExtRibbonNodeMdiRightButtons & other
	)
	: CExtRibbonNode( other )
{
	RibbonILE_RuleArrayGet().RemoveAll();
}

CExtRibbonNodeMdiRightButtons::~CExtRibbonNodeMdiRightButtons()
{
}

bool CExtRibbonNodeMdiRightButtons::OnGetCommandsListBoxInfo(
	CExtCustomizeCommandListBox * pLB, // IN (optional)
	CExtCustomizeSite * pSite, // = NULL // IN (optional)
	CExtCmdItem * pCmdItem, // = NULL // IN (optional)
	CExtSafeString * pStrLbText, // = NULL // OUT (optional)
	CExtCmdIcon * pLbIcon, // = NULL // OUT (optional)
	INT nDesiredIconWidth, // = 16 // IN (optional)
	INT nDesiredIconHeight // = 16 // IN (optional)
	)
{
	ASSERT_VALID( this );
	pLB;
	pSite;
	pCmdItem;
	pStrLbText;
	pLbIcon;
	nDesiredIconWidth;
	nDesiredIconHeight;
	return false;
}

CRuntimeClass * CExtRibbonNodeMdiRightButtons::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
	RibbonILE_RuleArrayGet().RemoveAll();
	return ( RUNTIME_CLASS( CExtBarMdiRightButton ) );
}

CExtBarButton * CExtRibbonNodeMdiRightButtons::OnRibbonCreateBarButton(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
CExtBarButton * pTBB = CExtRibbonNode::OnRibbonCreateBarButton( pBar, pParentTBB );
	if( pTBB != NULL )
	{
		ASSERT_VALID( pTBB );
		pTBB->ModifyStyle( TBBS_HIDDEN, TBBS_SEPARATOR );
	}
	return pTBB;
}

bool CExtRibbonNodeMdiRightButtons::Ribbon_InitBar(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB, // = NULL
	bool bInsertThisNode // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	return
		CExtRibbonNode::Ribbon_InitBar(
			pBar,
			pParentTBB,
			bInsertThisNode
			);
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonButton

IMPLEMENT_DYNCREATE( CExtRibbonButton, CExtBarButton );

CExtRibbonButton::CExtRibbonButton(
	CExtRibbonPage * pBar, // = NULL
	UINT nCmdID, // = ID_SEPARATOR
	UINT nStyle // = 0
	)
	: CExtBarButton( pBar, nCmdID, nStyle )
{
}

CExtRibbonButton::~CExtRibbonButton()
{
}

bool CExtRibbonButton::IsRibbonPaintingMode() const
{
	ASSERT_VALID( this );
	return true;
}

CSize CExtRibbonButton::CalculateLayout(
	CDC & dc,
	CSize sizePreCalc,
	BOOL bHorz
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( GetBar() );
	ASSERT_VALID( (&dc) );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( bHorz );
	sizePreCalc;
	bHorz;
CSize _size = RibbonILV_CalcSize( dc );
	return _size;
}

CExtRibbonPage * CExtRibbonButton::GetRibbonPage()
{
	ASSERT_VALID( this );
CExtToolControlBar * pBar = GetBar();
	if( pBar == NULL )
		return NULL;
	ASSERT_VALID( pBar );
CExtRibbonPage * pRibbonPage = DYNAMIC_DOWNCAST( CExtRibbonPage, pBar );
	return pRibbonPage;
}

const CExtRibbonPage * CExtRibbonButton::GetRibbonPage() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtRibbonButton * > ( this ) )
		-> GetRibbonPage();
}

CRect CExtRibbonButton::OnRibbonGetContentPadding() const
{
	ASSERT_VALID( this );
const CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
CRect rcCP = pRibbonPage->_OnRibbonContentPaddingGetButton( this );
	return rcCP;
}

INT CExtRibbonButton::OnRibbonGetSeparatorExtent( bool bHorz )
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
INT nSeparatorExtent = pRibbonPage->OnRibbonGetSeparatorExtent( this, bHorz );
	return nSeparatorExtent;
}

bool CExtRibbonButton::IsAbleToTrackMenu(
	bool bCustomizeMode // = false
	) const
{
	ASSERT_VALID( this );
	bCustomizeMode;
const CExtCustomizeCmdTreeNode * pOwnedNode = GetCmdNode();
	if( pOwnedNode == NULL )
		return false;
	ASSERT_VALID( pOwnedNode );
INT nNodeCount = pOwnedNode->GetNodeCount();
	if( nNodeCount > 0 )
		return true;
	return false;
}

void CExtRibbonButton::OnUpdateCmdUI(
	CWnd * pTarget,
	BOOL bDisableIfNoHndler,
	int nIndex
	)
{
	ASSERT_VALID( this );
	CExtBarButton::OnUpdateCmdUI( pTarget, bDisableIfNoHndler, nIndex );
}

CExtCmdIcon * CExtRibbonButton::GetIconPtr()
{
	ASSERT_VALID( this );
CExtRibbonNode * pRibbonNode = Ribbon_GetNode();
	if(		pRibbonNode != NULL
		&&	( (pRibbonNode->GetFlags()&__ECTN_TBB_AUTOCHANGE_ID) == 0 )
		)
	{
		INT nILV = RibbonILV_Get( 0 );	
		if( nILV ==__EXT_RIBBON_ILV_SIMPLE_LARGE )
		{
			if( ! pRibbonNode->m_iconBig.IsEmpty() )
				return (& pRibbonNode->m_iconBig );
			if( ! pRibbonNode->m_iconSmall.IsEmpty() )
			{
				CSize _size =
					GetRibbonPage() ->
						PmBridge_GetPM() ->
							Ribbon_GetIconSize(
								this,
								__EXT_RIBBON_ILV_SIMPLE_LARGE
								);
				pRibbonNode->m_iconBig = pRibbonNode->m_iconSmall;
				pRibbonNode->m_iconBig.Scale( _size );
				return (& pRibbonNode->m_iconBig );
			}
		}
		if( ! pRibbonNode->m_iconSmall.IsEmpty() )
			return (& pRibbonNode->m_iconSmall );
		if( ! pRibbonNode->m_iconBig.IsEmpty() )
		{
			CSize _size =
				GetRibbonPage() ->
					PmBridge_GetPM() ->
						Ribbon_GetIconSize(
							this,
							__EXT_RIBBON_ILV_SIMPLE_SMALL // nILV
							);
			pRibbonNode->m_iconSmall = pRibbonNode->m_iconBig;
			pRibbonNode->m_iconSmall.Scale( _size );
			return (& pRibbonNode->m_iconSmall );
		}
	} // if( pRibbonNode != NULL ...
	return CExtBarButton::GetIconPtr();
}

CExtSafeString CExtRibbonButton::GetText() const
{
	ASSERT_VALID( this );
INT nILV = RibbonILV_Get( 0 );	
	if( nILV == __EXT_RIBBON_ILV_SIMPLE_SMALL )
	{
// 		const CExtBarButton * pTBB = ParentButtonGet();
// 		if(		pTBB == NULL
// 			||	(! pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonTabPage) ) )
// 			)
// 			return CExtBarButton::GetText(); // QATB mode
		return _T("");
	}
	return CExtBarButton::GetText();
}

void CExtRibbonButton::PaintCompound(
	CDC & dc,
	bool bPaintParentChain,
	bool bPaintChildren,
	bool bPaintOneNearestChildrenLevelOnly
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( GetBar() );
	ASSERT_VALID( (&dc) );
	ASSERT( dc.GetSafeHdc() != NULL );

	if( ! IsPaintAble( dc ) )
		return;
	if( AnimationClient_StatePaint( dc ) )
		return;
	if( bPaintParentChain )
		PaintParentChain( dc );

	if( ! IsVisible() )
		return;
	if( ( GetStyle() & TBBS_HIDDEN ) != 0 )
		return;
	if(	 CtrlGet() != NULL )
		return;
	if( IsSeparator() )
	{
		CRect rcTBB = Rect();
		GetRibbonPage() -> PmBridge_GetPM() -> Ribbon_PaintSeparator( dc, rcTBB, this );
		return;	
	}

#if (defined __EXT_RIBBON_DEBUG_PAINTING)
CRect rcRibbonButton = *this;
	dc.FillSolidRect( &rcRibbonButton, 255 );
	dc.Draw3dRect( &rcRibbonButton, RGB(255,255,255), RGB(255,255,255) );
#else // (defined __EXT_RIBBON_DEBUG_PAINTING)
#endif // else from (defined __EXT_RIBBON_DEBUG_PAINTING)

INT nChildIndex, nChildCount = ChildButtonGetCount();
	if( bPaintChildren )
	{
		for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
		{
			CExtBarButton * pTBB = ChildButtonGetAt( nChildIndex );
			ASSERT_VALID( pTBB );
			if( ! pTBB->IsVisible() )
				continue;
			if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				continue;
			pTBB->PaintCompound( dc, bPaintChildren, bPaintOneNearestChildrenLevelOnly, false );
		} // for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
	}

	if( nChildCount != 0 )
		return;
	CExtBarButton::PaintCompound( dc, false, false, false );
}

CSize CExtRibbonButton::OnRibbonCalcLargeTextSize( CDC & dc )
{
	ASSERT_VALID( this );
	ASSERT_VALID( GetBar() );
	ASSERT_VALID( (&dc) );
	ASSERT( dc.GetSafeHdc() != NULL );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
CExtSafeString strText = GetText();
	if( strText.IsEmpty() )
		return CSize( 0, 0 );
CFont * pFont = pRibbonPage->OnGetToolbarFont( false, true, this );
CRect rcText =
		CExtPaintManager::stat_CalcTextDimension(
			dc,
			*pFont,
			strText,
			DT_LEFT|DT_TOP|DT_CALCRECT
			);
CSize _size = rcText.Size();
	_size.cx += pRibbonPage->PmBridge_GetPM()->Ribbon_GetDropDownArrowSize( dc, __EXT_RIBBON_ILV_SIMPLE_LARGE, this ).cx;
	return _size;
}

CRect CExtRibbonButton::RectDropDown() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( GetBar() );
INT nILV = RibbonILV_Get();
	if( nILV == __EXT_RIBBON_ILV_SIMPLE_LARGE )
	{
		CRect rc = GetRibbonPage()->PmBridge_GetPM()->Ribbon_CalcLargeDropDownRect( this );
		return rc;
	} // if( nILV == __EXT_RIBBON_ILV_SIMPLE_LARGE )
	return CExtBarButton::RectDropDown();
}

CRect CExtRibbonButton::RectWithoutDropDown() const
{
	ASSERT_VALID( this );
CRect rc = Rect();
	if( GetSeparatedDropDown() )
	{
		CRect rcDD = RectDropDown();
		INT nILV = RibbonILV_Get();
		if( nILV == __EXT_RIBBON_ILV_SIMPLE_LARGE )
			rc.bottom = rcDD.top;
		else
			rc.right = rcDD.left;
	}
	return rc;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonButtonTabPage

IMPLEMENT_DYNCREATE( CExtRibbonButtonTabPage, CExtBarButton );

CExtRibbonButtonTabPage::CExtRibbonButtonTabPage(
	CExtRibbonPage * pBar, // = NULL
	UINT nCmdID, // = ID_SEPARATOR
	UINT nStyle // = 0
	)
	: CExtBarButton( pBar, nCmdID, nStyle )
{
	ModifyStyle( 0, TBBS_SEPARATOR );
}

CExtRibbonButtonTabPage::~CExtRibbonButtonTabPage()
{
}

CSize CExtRibbonButtonTabPage::CalculateLayout(
	CDC & dc,
	CSize sizePreCalc,
	BOOL bHorz
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( GetBar() );
	ASSERT_VALID( (&dc) );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( bHorz );
CSize _size = CExtBarButton::CalculateLayout( dc, sizePreCalc, bHorz );
INT nAdvance =
		GetBar()->PmBridge_GetPM()->UiScalingDo(
			16,
			CExtPaintManager::__EUIST_X
			);
	_size.cx += nAdvance;
	return _size;
}

CExtRibbonPage * CExtRibbonButtonTabPage::GetRibbonPage()
{
	ASSERT_VALID( this );
CExtToolControlBar * pBar = GetBar();
	if( pBar == NULL )
		return NULL;
	ASSERT_VALID( pBar );
CExtRibbonPage * pRibbonPage = DYNAMIC_DOWNCAST( CExtRibbonPage, pBar );
	return pRibbonPage;
}

const CExtRibbonPage * CExtRibbonButtonTabPage::GetRibbonPage() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtRibbonButtonTabPage * > ( this ) )
		-> GetRibbonPage();
}

bool CExtRibbonButtonTabPage::IsSeparator() const
{
	ASSERT_VALID( this );
	return false;
}

CExtCmdIcon * CExtRibbonButtonTabPage::GetIconPtr()
{
	ASSERT_VALID( this );
	return CExtBarButton::GetIconPtr();
}

CExtSafeString CExtRibbonButtonTabPage::GetText() const
{
	ASSERT_VALID( this );
//	return CExtBarButton::GetText();
CExtCustomizeCmdTreeNode * pNode =
		((CExtBarButton*)this)->GetCmdNode();
	if( pNode != NULL )
	{
		ASSERT_VALID( pNode );
		LPCTSTR s = pNode->GetTextInToolbar( NULL );
		if( s != NULL && _tcslen(s) > 0 )
			return CExtSafeString( s );
		s = pNode->GetTextInMenu( NULL );
		if( s != NULL && _tcslen(s) > 0 )
			return CExtSafeString( s );
		s = pNode->GetTextUser();
		if( s != NULL && _tcslen(s) > 0 )
			return CExtSafeString( s );
	}
	return CExtBarButton::GetText();
}

bool CExtRibbonButtonTabPage::IsAbleToTrackMenu(
	bool bCustomizeMode // = false
	) const
{
	ASSERT_VALID( this );
	bCustomizeMode;
	return false;
}

void CExtRibbonButtonTabPage::OnClick(
	CPoint point,
	bool bDown
	)
{
	ASSERT_VALID( this );
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
	if( pBar->_OnHookButtonClick( this, point, bDown ) )
		return;
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
CExtRibbonBar * pRibbonBar =
		DYNAMIC_DOWNCAST( CExtRibbonBar, pRibbonPage );
bool bAutoHideModeEnabled = false;
	if( pRibbonBar != NULL )
		bAutoHideModeEnabled = pRibbonBar->Ribbon_AutoHideModeEnabledGet();
	if( ! bDown )
	{
		if( point.x == 0 && point.y == 0 )
		{ // if keyboard VK_RETURN or VK_SPACE
			if( bAutoHideModeEnabled )
				pRibbonBar->Ribbon_AutoHideModeDoExpanding(
					this,
					true
					);
		} // if keyboard VK_RETURN or VK_SPACE
		return;
	} // if( ! bDown )
// 	else
// 	{
// 		CExtPopupMenuWnd * pPopup = CExtPopupMenuSite::g_DefPopupMenuSite.GetInstance();
// 		if( pPopup->GetSafeHwnd() != NULL )
// 			pPopup->_ForceCancelCallbacs();
// 		CExtPopupMenuWnd::CancelMenuTracking();
// 	}
//	GetBar()->UpdateWindow();
	OnDeliverCmd();
	if( ! bAutoHideModeEnabled )
	{
 		if( ! pRibbonPage->RibbonPage_ExpandedModeGet() )
 			pRibbonPage->_OnRibbonPageExpandedModeSet( true );
	} // if( ! bAutoHideModeEnabled )
}

bool CExtRibbonButtonTabPage::OnDblClick(
	CPoint point
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( GetBar() );
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
	if( pBar->_OnHookButtonDblClick( this, point ) )
		return true;
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
CExtRibbonBar * pRibbonBar =
		DYNAMIC_DOWNCAST( CExtRibbonBar, pRibbonPage );
bool bAutoHideModeEnabled = false;
	if( pRibbonBar != NULL )
		bAutoHideModeEnabled = pRibbonBar->Ribbon_AutoHideModeEnabledGet();
	if( bAutoHideModeEnabled )
	{
		pRibbonPage->_OnRibbonPageExpandedModeSet(
			! pRibbonPage->RibbonPage_ExpandedModeGet()
			);
		return true;
	} // if( bAutoHideModeEnabled )
	if( ! pRibbonPage->RibbonPage_ExpandedModeGet() )
		return true;
	pRibbonPage->_OnRibbonPageExpandedModeSet( false );
	return true;
}

void CExtRibbonButtonTabPage::OnDeliverCmd()
{
	ASSERT_VALID( this );
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
	if( pBar->_OnHookButtonDeliverCmd( this ) )
		return;
/*
bool bAnimate = false;
	if( ! IsSelectedRibbonPage() )
	{
		bAnimate = true;
		AnimationClient_CacheGeneratorLock();
		RedrawButton( true );
//		if( AnimationClient_StateGet(true).IsEmpty() )
 			AnimationClient_CacheNextStateMinInfo(
				false,
				__EAPT_BY_PRESSED_STATE_TURNED_ON
				);
//		AnimationClient_CacheGeneratorUnlock();
	}
*/
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
// 	if( IsSelectedRibbonPage() )
// 	{
// 		CExtRibbonBar * pRibbonBar =
// 			DYNAMIC_DOWNCAST( CExtRibbonBar, pRibbonPage );
// 		if(		pRibbonBar != NULL
// 			&&	pRibbonBar->Ribbon_AutoHideModeEnabledGet()
// 			)
// 		{
// 			if( ! pRibbonBar->RibbonPage_ExpandedModeGet() )
// 				return;
// 		}
// 	} // if( IsSelectedRibbonPage() )
	pRibbonPage->m_bNonCommandButtonMode = true;
	CExtBarButton::OnDeliverCmd();
	pRibbonPage->m_bNonCommandButtonMode = false;
/*
	if( bAnimate )
	{
//		AnimationClient_CacheGeneratorLock();
		AnimationClient_CacheNextStateMinInfo(
			true,
			__EAPT_BY_SELECTED_STATE_TURNED_ON
			);
		AnimationClient_CacheGeneratorUnlock();
	}
*/
}

bool CExtRibbonButtonTabPage::OnKeyTipInvokeAction(
	bool & bContinueKeyTipMode
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
CExtRibbonBar * pRibbonBar =
		DYNAMIC_DOWNCAST( CExtRibbonBar, pRibbonPage );
	if( pRibbonBar == NULL )
		return false;
	bContinueKeyTipMode = true;
	pRibbonBar->KeyTipChainEmpty();
	if(		pRibbonBar->Ribbon_AutoHideModeEnabledGet()
		&&	( ! pRibbonPage->RibbonPage_ExpandedModeGet() )
		)
	{
		pRibbonBar->Ribbon_AutoHideModeDoExpanding(
			this,
			true
			);
		pRibbonBar->_RibbonBarKeyTipShow( false, true );
		pRibbonBar->KeyTipsDisplayedSet( false );
		return true;
	}
	OnDeliverCmd();
	pRibbonBar->KeyTipChainEmpty();
	pRibbonBar->m_bHelperKeyTipTopLevelMode = false;
	pRibbonBar->_RibbonBarKeyTipShow( false, true );
	CExtPaintManager::stat_PassPaintMessages();
	return true;
}

void CExtRibbonButtonTabPage::OnUpdateCmdUI(
	CWnd * pTarget,
	BOOL bDisableIfNoHndler,
	int nIndex
	)
{
	ASSERT_VALID( this );
	pTarget;
	bDisableIfNoHndler;
	nIndex;
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
UINT nOldStyle = pBar->GetButtonStyle( nIndex );
UINT nNewStyle = nOldStyle & (~(TBBS_DISABLED|TBBS_PRESSED|TBBS_CHECKED));
	if( nOldStyle != nNewStyle )
		pBar->SetButtonStyle( nIndex, nNewStyle );
}

BYTE CExtRibbonButtonTabPage::Get2007SeparatorAlpha() const
{
	ASSERT_VALID( this );
BYTE nSCA = GetRibbonPage()->RibbonLayout_Get2007SeparatorAlpha( this );
	return nSCA;
}

void CExtRibbonButtonTabPage::PaintCompound(
	CDC & dc,
	bool bPaintParentChain,
	bool bPaintChildren,
	bool bPaintOneNearestChildrenLevelOnly
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( GetBar() );
	ASSERT_VALID( (&dc) );
	ASSERT( dc.GetSafeHdc() != NULL );

	if( ! IsPaintAble( dc ) )
		return;
	if( AnimationClient_StatePaint( dc ) )
		return;
	if( bPaintParentChain )
		PaintParentChain( dc );

CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
	CFont * pFont = pRibbonPage->OnGetToolbarFont(false, false, this);
bool bSelected = IsSelectedRibbonPage();
#if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
CExtRibbonBar * pRibbonBar = DYNAMIC_DOWNCAST( CExtRibbonBar, pRibbonPage );
	if( bSelected && pRibbonBar != NULL && pRibbonBar->BackstageView_IsVisible() )
		bSelected = false;
#endif // (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
CRect rcEntireItem = *this;
INT nTabLineHeight = pRibbonPage->RibbonLayout_GetTabLineHeight();
INT nFrameCaptionHeight = pRibbonPage->RibbonLayout_GetFrameCaptionHeight();
INT nHeightAtTheTop = nTabLineHeight + nFrameCaptionHeight;
INT nBottomLineHeight = pRibbonPage->RibbonLayout_GetBottomLineHeight();
CRect rcClient;
	pRibbonPage->GetClientRect( &rcClient );
	rcClient.top += nHeightAtTheTop;
	rcClient.bottom -= nBottomLineHeight;
CRect rcTabLine(
		rcClient.left,
		rcClient.top - nTabLineHeight,
		rcClient.right,
		rcClient.top
		);
	pRibbonPage->RibbonLayout_AdjustTabLineRect( rcTabLine );
CExtSafeString sText = GetText();
CExtCmdIcon * pIcon = GetIconPtr();
CExtPaintManager * pPM = pRibbonPage->PmBridge_GetPM();
	if(		bSelected
		&&	(! pRibbonPage->RibbonPage_ExpandedModeGet() )
		)
	{
		if(		pRibbonBar != NULL
			&&	pRibbonBar->Ribbon_AutoHideModeEnabledGet()
			)
		{
			int nFlatTrackingIndex = pRibbonPage->_FlatTrackingIndexGet();
			if(		nFlatTrackingIndex >= 0
				&&	nFlatTrackingIndex == pRibbonPage->_GetIndexOf( this )
				)
			{
			}
			else
			{
				if(		pRibbonBar->m_pPopupPageMenuAutoHide->GetSafeHwnd() != NULL
					&&	(pRibbonBar->m_pPopupPageMenuAutoHide->GetStyle()&WS_VISIBLE) != 0
					)
				{
				}
				else
					bSelected = false;
			}
		}
		else
			bSelected = false;
	}
	pPM->Ribbon_PaintTabItem(
		dc,
		rcTabLine,
		bSelected,
		rcEntireItem,
		pFont,
		sText,
		pIcon,
		this
		);
	if( bPaintChildren )
		PaintChildren( dc, bPaintOneNearestChildrenLevelOnly );
}

bool CExtRibbonButtonTabPage::IsSelectedRibbonPage() const
{
	ASSERT_VALID( this );
const CExtCustomizeCmdTreeNode * pNode = GetCmdNode();
	if( pNode == NULL )
		return false;
	ASSERT_VALID( pNode );
	ASSERT_KINDOF( CExtRibbonNodeTabPage, pNode );
const CExtCustomizeCmdTreeNode * pNodeParent = pNode->GetParentNode();
	if( pNodeParent == NULL )
		return false;
	ASSERT_VALID( pNodeParent );
CExtRibbonNodeTabPageCollection * pRibbonNodeTabPageCollection =
		DYNAMIC_DOWNCAST( CExtRibbonNodeTabPageCollection, pNodeParent );
	if( pRibbonNodeTabPageCollection == NULL )
		return false;
INT nSelIdx = pRibbonNodeTabPageCollection->PageSelectionGet();
	if( nSelIdx < 0 )
		return false;
	if( LPVOID( pRibbonNodeTabPageCollection->ElementAt( nSelIdx ) ) != LPVOID( pNode ) )
		return false;
	return true;
}

const CExtAnimationParameters *
	CExtRibbonButtonTabPage::AnimationClient_OnQueryAnimationParameters(
		INT eAPT // __EAPT_*** animation type
		) const
{
	ASSERT_VALID( this );
CExtPaintManager * pPM = GetSafeBar()->PmBridge_GetPM();
	ASSERT_VALID( pPM );
	if( pPM->Ribbon_DwmAreaCoversTabs() )
		return (&(pPM->g_DefAnimationParametersEmpty));
	return CExtBarButton::AnimationClient_OnQueryAnimationParameters( eAPT );
}


/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNodeFile

IMPLEMENT_SERIAL( CExtRibbonNodeFile, CExtRibbonNodeGallery, VERSIONABLE_SCHEMA|1 );

CExtRibbonNodeFile::CExtRibbonNodeFile(
	CExtRibbonNode * pParentNode // = NULL
	)
	: CExtRibbonNodeGallery(
		0L,
		pParentNode
		)
{
	m_nTpmxAdditionalFlags |= TPMX_RIBBON_FILE_MENU|TPMX_RIBBON_OPTIONS_BUTTON|TPMX_RIBBON_EXIT_BUTTON;
	m_sizePopupGalleryControl = m_sizePopupGalleryControlMin =
		m_sizePopupGalleryControlMax = CSize( 350, 330 );
CExtPaintManager * pPM = g_PaintManager.GetPM();
	ASSERT_VALID( pPM );
	m_sizePopupGalleryControl.cx = pPM->UiScalingDo( m_sizePopupGalleryControl.cx, CExtPaintManager::__EUIST_X );
	m_sizePopupGalleryControl.cy = pPM->UiScalingDo( m_sizePopupGalleryControl.cy, CExtPaintManager::__EUIST_Y );
	m_sizePopupGalleryControlMin.cx = pPM->UiScalingDo( m_sizePopupGalleryControlMin.cx, CExtPaintManager::__EUIST_X );
	m_sizePopupGalleryControlMin.cy = pPM->UiScalingDo( m_sizePopupGalleryControlMin.cy, CExtPaintManager::__EUIST_Y );
	m_sizePopupGalleryControlMax.cx = pPM->UiScalingDo( m_sizePopupGalleryControlMax.cx, CExtPaintManager::__EUIST_X );
	m_sizePopupGalleryControlMax.cy = pPM->UiScalingDo( m_sizePopupGalleryControlMax.cy, CExtPaintManager::__EUIST_Y );
}

CExtRibbonNodeFile::CExtRibbonNodeFile(
	CExtRibbonNodeFile & other
	)
	: CExtRibbonNodeGallery( other )
{
	m_nTpmxAdditionalFlags |= TPMX_RIBBON_FILE_MENU|TPMX_RIBBON_OPTIONS_BUTTON|TPMX_RIBBON_EXIT_BUTTON;
	m_sizePopupGalleryControl = m_sizePopupGalleryControlMin =
		m_sizePopupGalleryControlMax = CSize( 350, 330 );
}

CExtRibbonNodeFile::~CExtRibbonNodeFile()
{
}

bool CExtRibbonNodeFile::OnGetCommandsListBoxInfo(
	CExtCustomizeCommandListBox * pLB, // IN (optional)
	CExtCustomizeSite * pSite, // = NULL // IN (optional)
	CExtCmdItem * pCmdItem, // = NULL // IN (optional)
	CExtSafeString * pStrLbText, // = NULL // OUT (optional)
	CExtCmdIcon * pLbIcon, // = NULL // OUT (optional)
	INT nDesiredIconWidth, // = 16 // IN (optional)
	INT nDesiredIconHeight // = 16 // IN (optional)
	)
{
	ASSERT_VALID( this );
	pLB;
	pSite;
	pCmdItem;
	pStrLbText;
	pLbIcon;
	nDesiredIconWidth;
	nDesiredIconHeight;
	return false;
}

CRuntimeClass * CExtRibbonNodeFile::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
	ASSERT( FALSE ); // this method must never be invoked
	return NULL;
}

CExtBarButton * CExtRibbonNodeFile::OnRibbonCreateBarButton(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	pBar;
	pParentTBB;
	ASSERT( FALSE ); // this method must never be invoked
	return NULL;
}

bool CExtRibbonNodeFile::Ribbon_InitBar(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB, // = NULL
	bool bInsertThisNode // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	pBar;
	pParentTBB;
	bInsertThisNode;
	if( pParentTBB != NULL )
	{
		ASSERT_VALID( pParentTBB );
	}
	return true;
}

void CExtRibbonNodeFile::Serialize( CArchive & ar )
{
	if( ar.IsStoring() )
		m_dwFlagsEx |= __ECTN_EX_HAVE_CCS_RFB;
	CExtRibbonNodeGallery::Serialize( ar );
	if( (m_dwFlagsEx&__ECTN_EX_HAVE_CCS_RFB) != 0 )
	{
		m_keyTipOptionsButton.Serialize( ar );
		m_keyTipExitButton.Serialize( ar );
	}
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNodeQuickAccessButtonsCollection

IMPLEMENT_SERIAL( CExtRibbonNodeQuickAccessButtonsCollection, CExtRibbonNode, VERSIONABLE_SCHEMA|1 );

CExtRibbonNodeQuickAccessButtonsCollection::CExtRibbonNodeQuickAccessButtonsCollection(
	CExtRibbonNode * pParentNode // = NULL
	)
	: CExtRibbonNode(
		0L,
		0L,
		pParentNode
		)
{
}

CExtRibbonNodeQuickAccessButtonsCollection::CExtRibbonNodeQuickAccessButtonsCollection(
	CExtRibbonNodeQuickAccessButtonsCollection & other
	)
	: CExtRibbonNode( other )
{
}

CExtRibbonNodeQuickAccessButtonsCollection::~CExtRibbonNodeQuickAccessButtonsCollection()
{
}

bool CExtRibbonNodeQuickAccessButtonsCollection::OnGetCommandsListBoxInfo(
	CExtCustomizeCommandListBox * pLB, // IN (optional)
	CExtCustomizeSite * pSite, // = NULL // IN (optional)
	CExtCmdItem * pCmdItem, // = NULL // IN (optional)
	CExtSafeString * pStrLbText, // = NULL // OUT (optional)
	CExtCmdIcon * pLbIcon, // = NULL // OUT (optional)
	INT nDesiredIconWidth, // = 16 // IN (optional)
	INT nDesiredIconHeight // = 16 // IN (optional)
	)
{
	ASSERT_VALID( this );
	pLB;
	pSite;
	pCmdItem;
	pStrLbText;
	pLbIcon;
	nDesiredIconWidth;
	nDesiredIconHeight;
	return false;
}

CRuntimeClass * CExtRibbonNodeQuickAccessButtonsCollection::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
	ASSERT( FALSE ); // this method must never be invoked
	return NULL;
}

CExtBarButton * CExtRibbonNodeQuickAccessButtonsCollection::OnRibbonCreateBarButton(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	pBar;
	pParentTBB;
	return NULL;
}

bool CExtRibbonNodeQuickAccessButtonsCollection::Ribbon_InitBar(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB, // = NULL
	bool bInsertThisNode // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	pParentTBB;
	bInsertThisNode;
#ifdef _DEBUG
	if( pParentTBB != NULL )
	{
		ASSERT_VALID( pParentTBB );
	}
#endif // _DEBUG
//	bInsertThisNode = false;
//	return CExtRibbonNode::Ribbon_InitBar( pBar, pParentTBB, bInsertThisNode );

INT nIndex, nCount = GetNodeCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtCustomizeCmdTreeNode * pNode = ElementAt( nIndex );
		ASSERT_VALID( pNode );
//		if(		pParentTBB == NULL
//			&&	( pNode->GetFlags() & __ECTN_TBB_HIDDEN ) != 0
//			)
//			continue;
		CExtRibbonNode * pRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNode );
		if( pRibbonNode == NULL )
		{
			continue;
//			return false;
		}
		if( ! pRibbonNode->Ribbon_InitBar( pBar, NULL, true ) )
			return false;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNodeDialogLauncher

IMPLEMENT_SERIAL( CExtRibbonNodeDialogLauncher, CExtRibbonNode, VERSIONABLE_SCHEMA|1 );

CExtRibbonNodeDialogLauncher::CExtRibbonNodeDialogLauncher(
	UINT nCmdIdBasic, // = 0L
	CExtRibbonNode * pParentNode, // = NULL
	LPARAM lParam // = 0L
	)
	: CExtRibbonNode(
		nCmdIdBasic,
		nCmdIdBasic,
		pParentNode,
		0,
		_T(""),
		_T(""),
		_T(""),
		lParam
		)
{
}

CExtRibbonNodeDialogLauncher::CExtRibbonNodeDialogLauncher(
	CExtRibbonNodeDialogLauncher & other
	)
	: CExtRibbonNode( other )
{
	RibbonILE_RuleArrayGet().RemoveAll();
}

CExtRibbonNodeDialogLauncher::~CExtRibbonNodeDialogLauncher()
{
}

bool CExtRibbonNodeDialogLauncher::OnGetCommandsListBoxInfo(
	CExtCustomizeCommandListBox * pLB, // IN (optional)
	CExtCustomizeSite * pSite, // = NULL // IN (optional)
	CExtCmdItem * pCmdItem, // = NULL // IN (optional)
	CExtSafeString * pStrLbText, // = NULL // OUT (optional)
	CExtCmdIcon * pLbIcon, // = NULL // OUT (optional)
	INT nDesiredIconWidth, // = 16 // IN (optional)
	INT nDesiredIconHeight // = 16 // IN (optional)
	)
{
	ASSERT_VALID( this );
	pLB;
	pSite;
	pCmdItem;
	pStrLbText;
	pLbIcon;
	nDesiredIconWidth;
	nDesiredIconHeight;
	return false;
}

void CExtRibbonNodeDialogLauncher::_InitMembers()
{
	ASSERT_VALID( this );
	CExtRibbonNode::_InitMembers();
CArray < DWORD, DWORD > & arrRule = RibbonILE_RuleArrayGet();
	arrRule.RemoveAll();
	arrRule.Add(
		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
			__EXT_RIBBON_ILE_MAX,
			__EXT_RIBBON_ILV_SIMPLE_SMALL,
			false
			)
		);
}

CRuntimeClass * CExtRibbonNodeDialogLauncher::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
	_InitMembers();
	return RUNTIME_CLASS( CExtRibbonButtonDialogLauncher );
}

INT CExtRibbonNodeDialogLauncher::RibbonILV_Get(
	INT nType // = 0 // -1 min, 0 current, 1 - max
	) const
{
	ASSERT_VALID( this );
	nType;
	return __EXT_RIBBON_ILV_SIMPLE_SMALL;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNodeRightButtonsCollection

IMPLEMENT_SERIAL( CExtRibbonNodeRightButtonsCollection, CExtRibbonNode, VERSIONABLE_SCHEMA|1 );

CExtRibbonNodeRightButtonsCollection::CExtRibbonNodeRightButtonsCollection(
	CExtRibbonNode * pParentNode // = NULL
	)
	: CExtRibbonNode(
		0L,
		0L,
		pParentNode
		)
{
}

CExtRibbonNodeRightButtonsCollection::CExtRibbonNodeRightButtonsCollection(
	CExtRibbonNodeRightButtonsCollection & other
	)
	: CExtRibbonNode( other )
{
}

CExtRibbonNodeRightButtonsCollection::~CExtRibbonNodeRightButtonsCollection()
{
}

bool CExtRibbonNodeRightButtonsCollection::OnGetCommandsListBoxInfo(
	CExtCustomizeCommandListBox * pLB, // IN (optional)
	CExtCustomizeSite * pSite, // = NULL // IN (optional)
	CExtCmdItem * pCmdItem, // = NULL // IN (optional)
	CExtSafeString * pStrLbText, // = NULL // OUT (optional)
	CExtCmdIcon * pLbIcon, // = NULL // OUT (optional)
	INT nDesiredIconWidth, // = 16 // IN (optional)
	INT nDesiredIconHeight // = 16 // IN (optional)
	)
{
	ASSERT_VALID( this );
	pLB;
	pSite;
	pCmdItem;
	pStrLbText;
	pLbIcon;
	nDesiredIconWidth;
	nDesiredIconHeight;
	return false;
}

CRuntimeClass * CExtRibbonNodeRightButtonsCollection::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
	ASSERT( FALSE ); // this method must never be invoked
	return NULL;
}

CExtBarButton * CExtRibbonNodeRightButtonsCollection::OnRibbonCreateBarButton(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	pBar;
	pParentTBB;
	return NULL;
}

bool CExtRibbonNodeRightButtonsCollection::Ribbon_InitBar(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB, // = NULL
	bool bInsertThisNode // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	pParentTBB;
	bInsertThisNode;
#ifdef _DEBUG
	if( pParentTBB != NULL )
	{
		ASSERT_VALID( pParentTBB );
	}
#endif // _DEBUG
//	bInsertThisNode = false;
//	return CExtRibbonNode::Ribbon_InitBar( pBar, pParentTBB, bInsertThisNode );

INT nIndex, nCount = GetNodeCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtCustomizeCmdTreeNode * pNode = ElementAt( nIndex );
		ASSERT_VALID( pNode );
		if(		pParentTBB == NULL
			&&	(	( pNode->GetFlags() & __ECTN_TBB_HIDDEN ) != 0
				&&	( ! pNode->IsKindOf( RUNTIME_CLASS(CExtRibbonNodeMdiRightButtons) ) )
				)
			)
			continue;
		CExtRibbonNode * pRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNode );
		if( pRibbonNode == NULL )
		{
			continue;
//			return false;
		}
		if( ! pRibbonNode->Ribbon_InitBar( pBar, NULL, true ) )
			return false;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNodeTabPageCollection

IMPLEMENT_SERIAL( CExtRibbonNodeTabPageCollection, CExtRibbonNode, VERSIONABLE_SCHEMA|1 );

CExtRibbonNodeTabPageCollection::CExtRibbonNodeTabPageCollection(
	CExtRibbonNode * pParentNode // = NULL
	)
	: m_nSelIdx( -1 )
	, CExtRibbonNode(
		0L,
		0L,
		pParentNode
		)
{
}

CExtRibbonNodeTabPageCollection::CExtRibbonNodeTabPageCollection(
	CExtRibbonNodeTabPageCollection & other
	)
	: m_nSelIdx( -1 )
	, CExtRibbonNode( other )
{
}

CExtRibbonNodeTabPageCollection::~CExtRibbonNodeTabPageCollection()
{
}

bool CExtRibbonNodeTabPageCollection::OnGetCommandsListBoxInfo(
	CExtCustomizeCommandListBox * pLB, // IN (optional)
	CExtCustomizeSite * pSite, // = NULL // IN (optional)
	CExtCmdItem * pCmdItem, // = NULL // IN (optional)
	CExtSafeString * pStrLbText, // = NULL // OUT (optional)
	CExtCmdIcon * pLbIcon, // = NULL // OUT (optional)
	INT nDesiredIconWidth, // = 16 // IN (optional)
	INT nDesiredIconHeight // = 16 // IN (optional)
	)
{
	ASSERT_VALID( this );
	pLB;
	pSite;
	pCmdItem;
	pStrLbText;
	pLbIcon;
	nDesiredIconWidth;
	nDesiredIconHeight;
	return false;
}

INT CExtRibbonNodeTabPageCollection::PageSelectionGet() const
{
	ASSERT_VALID( this );
	return m_nSelIdx;
}

void CExtRibbonNodeTabPageCollection::PageSelectionSet( INT nSelIdx )
{
	ASSERT_VALID( this );
	if( 0 > nSelIdx || nSelIdx >= GetNodeCount() )
		nSelIdx = -1;
	m_nSelIdx = nSelIdx;
}

void CExtRibbonNodeTabPageCollection::AssignFromOther(
	CExtCustomizeCmdTreeNode & other
	)
{
	CExtRibbonNode::AssignFromOther( other );
CExtRibbonNodeTabPageCollection * pOther = DYNAMIC_DOWNCAST( CExtRibbonNodeTabPageCollection, (&other) );
	if( pOther != NULL )
	{
		PageSelectionSet( pOther->PageSelectionGet() );
	} // if( pOther != NULL )
	else
	{
		PageSelectionSet( -1 );
	} // else from if( pOther != NULL )
}

void CExtRibbonNodeTabPageCollection::Serialize( CArchive & ar )
{
	CExtRibbonNode::Serialize( ar );
	if( ar.IsStoring() )
	{
		ar << DWORD( PageSelectionGet() );
	} // if( ar.IsStoring() )
	else
	{
		DWORD dw;
		ar >> dw;
		PageSelectionSet( INT(dw) );
	} // else from if( ar.IsStoring() )
}

CRuntimeClass * CExtRibbonNodeTabPageCollection::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
	ASSERT( FALSE ); // this method must never be invoked
	return NULL;
}

CExtBarButton * CExtRibbonNodeTabPageCollection::OnRibbonCreateBarButton(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	pBar;
	pParentTBB;
	return NULL;
}

bool CExtRibbonNodeTabPageCollection::Ribbon_InitBar(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB, // = NULL
	bool bInsertThisNode // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	pParentTBB;
	bInsertThisNode;
#ifdef _DEBUG
	if( pParentTBB != NULL )
	{
		ASSERT_VALID( pParentTBB );
	}
#endif // _DEBUG
//	bInsertThisNode = false;
//	return CExtRibbonNode::Ribbon_InitBar( pBar, pParentTBB, bInsertThisNode );

INT nIndex, nCount = GetNodeCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtCustomizeCmdTreeNode * pNode = ElementAt( nIndex );
		ASSERT_VALID( pNode );
//		if(		pParentTBB == NULL
//			&&	( pNode->GetFlags() & __ECTN_TBB_HIDDEN ) != 0
//			)
//			continue;
		CExtRibbonNode * pRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNode );
		if( pRibbonNode == NULL )
		{
			continue;
//			return false;
		}
		if( ! pRibbonNode->Ribbon_InitBar( pBar, NULL, true ) )
			return false;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNodeTabPage

IMPLEMENT_SERIAL( CExtRibbonNodeTabPage, CExtRibbonNode, VERSIONABLE_SCHEMA|1 );

CExtRibbonNodeTabPage::CExtRibbonNodeTabPage(
	UINT nCmdIdBasic, // = 0L
	CExtRibbonNode * pParentNode, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strTextInToolbar, // = NULL
	LPARAM lParam // = 0L
	)
	: CExtRibbonNode(
		nCmdIdBasic,
		nCmdIdBasic,
		pParentNode,
		0L,
		strTextInToolbar,
		strTextInToolbar,
		strTextInToolbar,
		lParam
		)
{
}

CExtRibbonNodeTabPage::CExtRibbonNodeTabPage(
	CExtRibbonNodeTabPage & other
	)
	: CExtRibbonNode( other )
{
	RibbonILE_RuleArrayGet().RemoveAll();
}

CExtRibbonNodeTabPage::~CExtRibbonNodeTabPage()
{
}

bool CExtRibbonNodeTabPage::OnGetCommandsListBoxInfo(
	CExtCustomizeCommandListBox * pLB, // IN (optional)
	CExtCustomizeSite * pSite, // = NULL // IN (optional)
	CExtCmdItem * pCmdItem, // = NULL // IN (optional)
	CExtSafeString * pStrLbText, // = NULL // OUT (optional)
	CExtCmdIcon * pLbIcon, // = NULL // OUT (optional)
	INT nDesiredIconWidth, // = 16 // IN (optional)
	INT nDesiredIconHeight // = 16 // IN (optional)
	)
{
	ASSERT_VALID( this );
	pLB;
	pSite;
	pCmdItem;
	pStrLbText;
	pLbIcon;
	nDesiredIconWidth;
	nDesiredIconHeight;
	return false;
}

CRuntimeClass * CExtRibbonNodeTabPage::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
	RibbonILE_RuleArrayGet().RemoveAll();
	return ( RUNTIME_CLASS( CExtRibbonButtonTabPage ) );
}

CExtBarButton * CExtRibbonNodeTabPage::OnRibbonCreateBarButton(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	return CExtRibbonNode::OnRibbonCreateBarButton( pBar, pParentTBB );
}

bool CExtRibbonNodeTabPage::Ribbon_InitBar(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB, // = NULL
	bool bInsertThisNode // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	pParentTBB;
	bInsertThisNode;
#ifdef _DEBUG
	if( pParentTBB != NULL )
	{
		ASSERT_VALID( pParentTBB );
	}
#endif // _DEBUG
//	pParentTBB = NULL;
//	bInsertThisNode = false;
//	return CExtRibbonNode::Ribbon_InitBar( pBar, pParentTBB, bInsertThisNode );
CExtCustomizeCmdTreeNode * pNode = GetParentNode();
	if( pNode == NULL )
		return false;
	ASSERT_VALID( pNode );
CExtRibbonNodeTabPageCollection * pRibbonNodeTabPageCollection =
		STATIC_DOWNCAST( CExtRibbonNodeTabPageCollection, pNode );
	if( pRibbonNodeTabPageCollection == NULL )
		return false;
CExtBarButton * pTBB = NULL;
	if( bInsertThisNode )
		pTBB = OnRibbonCreateBarButton( pBar, NULL );
INT nSelIdx = pRibbonNodeTabPageCollection->PageSelectionGet();
	if(		nSelIdx >= 0
		&&	LPVOID( pRibbonNodeTabPageCollection->ElementAt( nSelIdx ) ) == LPVOID( this )
		)
	{
		INT nIndex, nCount = GetNodeCount();
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtCustomizeCmdTreeNode * pNode = ElementAt( nIndex );
			ASSERT_VALID( pNode );
			if(		pParentTBB == NULL
				&&	( pNode->GetFlags() & __ECTN_TBB_HIDDEN ) != 0
				)
				continue;
			CExtRibbonNode * pRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNode );
			if( pRibbonNode == NULL )
			{
				continue;
//				if( pTBB != NULL )
//					delete pTBB;
//				return false;
			}
			if( ! pRibbonNode->Ribbon_InitBar( pBar, NULL, true ) )
			{
				if( pTBB != NULL )
					delete pTBB;
				return false;
			}
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	}
	if( pTBB != NULL )
	{
		pTBB->SetBasicCmdNode( this );
		pTBB->SetCustomizedCmdNode( this );
		if( ! pBar->InsertSpecButton( -1, pTBB, FALSE ) )
		{
			if( pTBB != NULL )
				delete pTBB;
			return false;
		}
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonButtonGroup

IMPLEMENT_DYNCREATE( CExtRibbonButtonGroup, CExtRibbonButton );

CExtRibbonButtonGroup::CExtRibbonButtonGroup(
	CExtRibbonPage * pBar, // = NULL
	UINT nCmdID, // = ID_SEPARATOR
	UINT nStyle // = 0
	)
	: CExtRibbonButton( pBar, nCmdID, nStyle )
	, m_bTopCollapsedState( false )
	, m_bSettingLayoutRect( false )
	, m_bRibbonPopupVisible( false )
	, m_rcCaptionTextAlignmentDLB( 0, 0, 0, 0 )
	, m_bHelperCaptionTextIsPartiallyVisible( false )
{
}

CExtRibbonButtonGroup::~CExtRibbonButtonGroup()
{
}

bool CExtRibbonButtonGroup::AnimationClient_OnQueryEnabledState(
	INT eAPT // __EAPT_*** animation type
	) const
{
	ASSERT_VALID( this );
	if( ParentButtonGet() != NULL )
		return false;
	return CExtRibbonButton::AnimationClient_OnQueryEnabledState( eAPT );
}

UINT CExtRibbonButtonGroup::OnTrackPopup(
	CPoint point,
	bool bSelectAny,
	bool bForceNoAnimation
	)
{
	ASSERT_VALID( this );
	bForceNoAnimation;
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
bool bKeyTipsDisplayed = pRibbonPage->KeyTipsDisplayedGet();

bool bDockSiteCustomizeMode =
		pRibbonPage->_IsDockSiteCustomizeMode();
	if( bDockSiteCustomizeMode )
		return UINT(-1L);

	if(		IsDisabled()
		&&	(! CanBePressedInDisabledState() )
		)
		return UINT(-1L);

CWnd * pWnd = CtrlGet();
	if( pWnd != NULL )
	{
		LRESULT lResult =
			pWnd->SendMessage(
				CExtToolControlBar::g_nMsgContinueTrackMenu,
				WPARAM( this ),
				bSelectAny ? 1L : 0L
				);
		lResult;
		return UINT(-1L);
	} // if( pWnd != NULL )

	if( ! IsAbleToTrackMenu() )
		return UINT(-1L);
	
bool bPrevTBMT = CExtToolControlBar::g_bMenuTracking;
//	if( bPrevTBMT )
//		return UINT(-1L);

	if(		CExtToolControlBar::g_bMenuTracking
		&&	pRibbonPage->_GetIndexOf(this) ==
				pRibbonPage->GetMenuTrackingButton()
		)
		return UINT(-1L);

	CExtToolControlBar::_CloseTrackingMenus();

//	if( pRibbonPage->IsFloating() )
//	{
//		pRibbonPage->ActivateTopParent();
//		CFrameWnd * pFrame =
//			pRibbonPage->GetDockingFrame();
//		ASSERT_VALID( pFrame );
//		pFrame->BringWindowToTop();
//	}

CWnd * pWndCmdTarget = GetCmdTargetWnd();
	ASSERT_VALID( pWndCmdTarget );

	CExtToolControlBar::g_bMenuTracking = bPrevTBMT;


CRect rcBtn = Rect();
	pRibbonPage->ClientToScreen( &rcBtn );
	pRibbonPage->ClientToScreen( &point );

DWORD dwTrackFlags =
		OnGetTrackPopupFlags()
			| TPMX_COMBINE_NONE
//			| TPMX_OWNERDRAW_FIXED
			| TPMX_NO_SITE
			| TPMX_NO_HIDE_RARELY
			| TPMX_FORCE_NO_ANIMATION
			| TPMX_RIBBON_MODE
			;
//	if( bSelectAny )
//		dwTrackFlags |= TPMX_SELECT_ANY;
//	if( CExtToolControlBar::g_bMenuTrackingExpanded )
//		dwTrackFlags |= TPMX_NO_HIDE_RARELY;
//	if( bForceNoAnimation )
//		dwTrackFlags |= TPMX_FORCE_NO_ANIMATION;

	pRibbonPage->_SwitchMenuTrackingIndex(
		pRibbonPage->_GetIndexOf( this )
		);

HWND hWndTrack = GetCmdTargetWnd()->GetSafeHwnd();
	if( pRibbonPage->m_pPopupPageMenuGroup == NULL )
		pRibbonPage->m_pPopupPageMenuGroup =
			new CExtRibbonPopupMenuWnd( pRibbonPage->m_hWnd );
	else if( pRibbonPage->m_pPopupPageMenuGroup->GetSafeHwnd() != NULL )
		::DestroyWindow( pRibbonPage->m_pPopupPageMenuGroup->m_hWnd ); // fade-out
	pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.m_bHelperAutoHideMode = false;
	pRibbonPage->m_pPopupPageMenuGroup->m_pSrcTrackingButtonGroup = this;
	pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.m_hWndSrcRibbonPage =
		pRibbonPage->m_hWnd;
	pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.m_hWndParentRibbonPage =
		pRibbonPage->m_hWnd;

	pRibbonPage->m_pPopupPageMenuGroup->CreatePopupMenu( hWndTrack );

CExtCustomizeCmdTreeNode * pOwnedNode = GetCmdNode();
	ASSERT_VALID( pOwnedNode );
CExtRibbonNode * pNewRootNode = new CExtRibbonNode;
	pNewRootNode->InsertNode( NULL, pOwnedNode->CloneNode() );
	pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.SetButtons( pNewRootNode );
	if( pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.m_bFlatTracking )
	{
		pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.m_bFlatTracking = false;
		pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.OnFlatTrackingStop();
	} // if( pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.m_bFlatTracking )
	pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.m_nFlatTrackingIndex = -1;
	if( bSelectAny )
	{
		INT nIndex, nCount = pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.GetButtonsCount();
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtBarButton * pTBB = pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.GetButton( nIndex );
			ASSERT_VALID( pTBB );
			if( pTBB->OnQueryFlatTrackingEnabled() )
			{
				pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.m_nFlatTrackingIndex = nIndex;
				if( ! pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.m_bFlatTracking )
				{
					pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.m_bFlatTracking = true;
					HDWP hPassiveModeDWP = NULL;
					pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.OnFlatTrackingStart( hPassiveModeDWP );
				} // if( ! pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.m_bFlatTracking )
				break;
			} // if( pTBB->OnQueryFlatTrackingEnabled() )
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	} // if( bSelectAny )
	pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.RibbonLayout_IleReset(
		NULL,
		__EXT_RIBBON_ILE_MAX,
		true,
		false,
		true
		);
	{ // block
		CClientDC dc( pRibbonPage );
		pRibbonPage->m_pPopupPageMenuGroup->m_sizeChildControl =
			pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.RibbonLayout_Calc(
				dc
				);
		pRibbonPage->m_pPopupPageMenuGroup->m_sizeChildControl.cy =
			pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.RibbonLayout_GetGroupHeight( NULL );
		if( ! pRibbonPage->m_bHelperAutoHideMode )
		{
			if( ! pRibbonPage->PmBridge_GetPM()->Ribbon_IsPopupGroupWithCaption( this ) )
				pRibbonPage->m_pPopupPageMenuGroup->m_sizeChildControl.cy -=
					pRibbonPage->RibbonLayout_GroupCaptionGetHeight( this );
		} // if( ! pRibbonPage->m_bHelperAutoHideMode )
	} // block

	CExtPaintManager::stat_PassPaintMessages();
	pRibbonPage->m_pPopupPageMenuGroup->m_hWndNotifyMenuClosed = pRibbonPage->GetSafeHwnd();
	if( GetSeparatedDropDown() )
		m_bDropDownHT = true;
	g_pTrackingMenuTBB = this;
	if( ! pRibbonPage->m_pPopupPageMenuGroup->TrackPopupMenu(
			dwTrackFlags,
			point.x,
			point.y,
			&rcBtn,
			GetBar(),
			CExtToolControlBar::_CbPaintCombinedContent,
			NULL,
			true
			)
		)
	{
		g_pTrackingMenuTBB = NULL;
		CExtToolControlBar::_CloseTrackingMenus();
		return UINT(-1L);
	}
	if( bKeyTipsDisplayed )
	{
		pRibbonPage->_CancelFlatTracking( FALSE );
		pRibbonPage->_SwitchMenuTrackingIndex();
		pRibbonPage->Invalidate();
		HDWP hPassiveModeDWP = NULL;
		pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.OnFlatTrackingStart( hPassiveModeDWP );
		pRibbonPage->KeyTipsDisplayedSet( false );
	} // if( bKeyTipsDisplayed )

	if( bSelectAny )
	{
		INT nFlatTrackingIndex =
			pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.
				OnCalcFlatTrackingIndex( VK_TAB, -1 );
		if( nFlatTrackingIndex >= 0 )
		{
			pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.
				_FlatTrackingIndexSet( nFlatTrackingIndex );
 			pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.
				_FlatTrackingSet( true );
 			pRibbonPage->m_pPopupPageMenuGroup->m_wndRibbonPage.
				_UpdateFlatTracking();
		} // if( nFlatTrackingIndex >= 0 )
	} // if( bSelectAny )
	else if( ! bKeyTipsDisplayed )
	{
		CExtToolControlBar::g_bMenuTracking = true;
		pRibbonPage->_SwitchMenuTrackingIndex(
			pRibbonPage->_GetIndexOf( this )
			);
	} // else if( ! bKeyTipsDisplayed )

	return UINT(-1L);
}

bool CExtRibbonButtonGroup::IsPaintDropDown(
	bool bCustomizeMode // = false
	) const
{
	ASSERT_VALID( this );
bool bPaintAsDropDown = CExtRibbonButton::IsPaintDropDown( bCustomizeMode );
	if( ! bPaintAsDropDown )
		return false;
INT nILV = RibbonILV_Get( 0 );	
	if( nILV == __EXT_RIBBON_ILV_SIMPLE_SMALL )
	{
		const CExtBarButton * pTBB = ParentButtonGet();
		if(		pTBB == NULL
			||	(! pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonTabPage) ) )
			)
			return false; // QATB mode
	}
	return true;
}

bool CExtRibbonButtonGroup::IsAbleToTrackMenu(
	bool bCustomizeMode // = false
	) const
{
	ASSERT_VALID( this );
	bCustomizeMode;
	if( TopCollapsedStateGet() )
		return true;
	else
		return false;
}

bool CExtRibbonButtonGroup::TopCollapsedStateGet() const
{
	ASSERT_VALID( this );
	if( m_bTopCollapsedState )
		return true;
const CExtRibbonNode * pRibbonNode = Ribbon_GetNode();
	if( pRibbonNode == NULL )
		return false;
	ASSERT_VALID( pRibbonNode );
const CExtCustomizeCmdTreeNode * pParentNode = pRibbonNode->GetParentNode();
	if(		pParentNode != NULL
		&&	pParentNode->IsKindOf( RUNTIME_CLASS(CExtRibbonNodeQuickAccessButtonsCollection) )
		)
	{
		ASSERT_VALID( pParentNode );
		return true;
	}
	return false;
}

void CExtRibbonButtonGroup::TopCollapsedStateSet( bool bTopCollapsedState )
{
	ASSERT_VALID( this );
	ASSERT_VALID( GetBar() );
	m_bTopCollapsedState = bTopCollapsedState;
	if( ! m_bTopCollapsedState )
		m_bRibbonPopupVisible = false;
}


bool CExtRibbonButtonGroup::RibbonILE_SetCollapsed( INT nILE ) // returns flag indicating whether collapsed state changed
{
	ASSERT_VALID( this );
bool bNewCollapsed = false, bOldCollapsed = TopCollapsedStateGet();
	//CExtBarButton::RibbonILE_SetCollapsed( nILE );
INT nMarginILE = RibbonILE_GetCollapsed();
	if( nILE <= nMarginILE )
		bNewCollapsed = true;
	if( bNewCollapsed != bOldCollapsed )
	{
		TopCollapsedStateSet( bNewCollapsed );
		return true;
	}
	else
		return false;
}

bool CExtRibbonButtonGroup::OnRibbonIsCaptionVisible() const
{
	ASSERT_VALID( this );
const CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
bool bCaptionVisible =
		pRibbonPage->RibbonLayout_GroupCaptionIsVisible( this );
	return bCaptionVisible;
}

CRect CExtRibbonButtonGroup::OnRibbonGetCaptionRect() const
{
	ASSERT_VALID( this );
const CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
CRect rcRibbonGroupCaption =
		pRibbonPage->RibbonLayout_GroupCaptionRectGet( this );
	return rcRibbonGroupCaption;
}

void CExtRibbonButtonGroup::OnUpdateCmdUI(
	CWnd * pTarget,
	BOOL bDisableIfNoHndler,
	int nIndex
	)
{
	ASSERT_VALID( this );
	pTarget;
	bDisableIfNoHndler;
	nIndex;
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
UINT nOldStyle = pBar->GetButtonStyle( nIndex );
UINT nNewStyle = nOldStyle & (~(TBBS_DISABLED));
	if( nOldStyle != nNewStyle )
		pBar->SetButtonStyle( nIndex, nNewStyle );
}

__EXT_MFC_INT_PTR CExtRibbonButtonGroup::OnToolHitTest(
	CPoint point,
	TOOLINFO * pTI
	)
{
	ASSERT_VALID( this );
	if( ! TopCollapsedStateGet() )
	{
		if(	! IsDisplayScreenTip() )
			return -1;
		CExtRibbonPage * pRibbonPage = GetRibbonPage();
		if( pRibbonPage != NULL && pRibbonPage->OnRibbonQueryGroupCaptionTextWithToolTip( this ) )
		{
			if( pTI != NULL )
			{
				int nCmdID = (int)GetCmdID(true);
				CExtSafeString strCaptionText = GetText();
				if( ! strCaptionText.IsEmpty() )
				{
					strCaptionText.Replace( _T("\r"), _T("") );
					strCaptionText.Replace( _T("\n"), _T(" ") );
					CRect rcArea = RectWithoutDropDown();
					::CopyRect( &(pTI->rect), &rcArea );
					pTI->uId = (UINT)nCmdID;
					pTI->hwnd = GetSafeBar()->GetSafeHwnd();
					//pTI->lpszText = LPSTR_TEXTCALLBACK;
					pTI->lpszText = _tcsdup( LPCTSTR(strCaptionText) );
					return (__EXT_MFC_INT_PTR)nCmdID;
				} // if( ! strCaptionText.IsEmpty() )
			} // if( pTI != NULL )
		} // if( pRibbonPage != NULL && pRibbonPage->OnRibbonQueryGroupCaptionTextWithToolTip( this ) )
		return -1;
	} // if( ! TopCollapsedStateGet() )
	return CExtBarButton::OnToolHitTest( point, pTI );
}

void CExtRibbonButtonGroup::PaintCompound(
	CDC & dc,
	bool bPaintParentChain,
	bool bPaintChildren,
	bool bPaintOneNearestChildrenLevelOnly
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( GetBar() );
	ASSERT_VALID( (&dc) );
	ASSERT( dc.GetSafeHdc() != NULL );
	
	if( ! IsPaintAble( dc ) )
		return;
	if( AnimationClient_StatePaint( dc ) )
		return;
	if( bPaintParentChain )
		PaintParentChain( dc );

CRect rcRibbonGroupButton = *this;
const CExtRibbonNode * pRibbonNode = Ribbon_GetNode();
	if( pRibbonNode != NULL )
	{
		ASSERT_VALID( pRibbonNode );
		const CExtCustomizeCmdTreeNode * pParentNode = pRibbonNode->GetParentNode();
		if(		pParentNode != NULL
			&&	pParentNode->IsKindOf( RUNTIME_CLASS(CExtRibbonNodeQuickAccessButtonsCollection) )
			)
		{
			ASSERT_VALID( pParentNode );
			if( ! GetRibbonPage()->PmBridge_GetPM()->
					Ribbon_PaintQuickAccessGroupButton(
						dc,
						rcRibbonGroupButton,
						this
						)
				)
				CExtBarButton::PaintCompound( dc, false, bPaintChildren, bPaintOneNearestChildrenLevelOnly );
			return;
		}
	} // if( pRibbonNode != NULL )

#if (defined __EXT_RIBBON_DEBUG_PAINTING)
	dc.FillSolidRect( &rcRibbonGroupButton, 255<<8 );
//	dc.Draw3dRect( &rcRibbonGroupButton, 0, 0 );
#else // (defined __EXT_RIBBON_DEBUG_PAINTING)

	GetRibbonPage()->PmBridge_GetPM()->
		Ribbon_PaintGroupBk(
			dc,
			rcRibbonGroupButton,
			this
			);

#endif // else from (defined __EXT_RIBBON_DEBUG_PAINTING)

	// debug version of group caption
#if (defined __EXT_RIBBON_DEBUG_PAINTING)
	if( OnRibbonIsCaptionVisible() )
	{
		CRect rcRibbonGroupCaption = OnRibbonGetCaptionRect();
		if( GetRibbonPage()->RibbonLayout_GroupCaptionIsTopAligned(this) )
			rcRibbonGroupCaption.DeflateRect( 1, 1, 1, 0 );
		else
			rcRibbonGroupCaption.DeflateRect( 1, 0, 1, 1 );
		dc.FillSolidRect( &rcRibbonGroupCaption, 0 );
	} // if( OnRibbonIsCaptionVisible() )
#else // (defined __EXT_RIBBON_DEBUG_PAINTING)
#endif // else from (defined __EXT_RIBBON_DEBUG_PAINTING)

INT nChildIndex, nChildCount = ChildButtonGetCount();
	if( bPaintChildren )
	{
		for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
		{
			CExtBarButton * pTBB = ChildButtonGetAt( nChildIndex );
			ASSERT_VALID( pTBB );
			if( ! pTBB->IsVisible() )
				continue;
			if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				continue;
			pTBB->PaintCompound( dc, false, ! bPaintOneNearestChildrenLevelOnly, false );
		} // for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
	}

}

INT CExtRibbonButtonGroup::OnRibbonGetOuterGroupDistance(
	bool bDistanceBefore
	) const
{
	ASSERT_VALID( this );
const CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
	if( ParentButtonGet() != NULL )
		return 0;
INT nDistance =
		pRibbonPage ->
			_OnRibbonContentPaddingGetOuterGroupDistance(
				this,
				bDistanceBefore
				);
	return nDistance;
}

CRect CExtRibbonButtonGroup::OnRibbonGetContentPadding() const
{
	ASSERT_VALID( this );
const CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
CRect rcCP = pRibbonPage->_OnRibbonContentPaddingGetGroup( this );
	return rcCP;
}

void CExtRibbonButtonGroup::SetLayoutRect( CDC &dc, const RECT & rectButton )
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&dc) );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( m_bSettingLayoutRect )
		return;
	m_bSettingLayoutRect = true;
	CExtRibbonButton::SetLayoutRect( dc, rectButton );
CPoint ptLayout( rectButton.left, rectButton.top );
	OnRibbonCalcLayout( dc, ptLayout, true );
	m_bSettingLayoutRect = false;
}

CSize CExtRibbonButtonGroup::RibbonILV_CalcSize(
	CDC & dc,
	INT nILV // = -1 // -1 use current visual level
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	nILV;
	if( ! IsVisible() )
		return CSize( 0, 0 );
	if( (GetStyle()&TBBS_HIDDEN) != 0 )
		return CSize( 0, 0 );
const CExtRibbonNode * pRibbonNode = Ribbon_GetNode();
	if( pRibbonNode == NULL )
		return CSize( 0, 0 );
	ASSERT_VALID( pRibbonNode );
const CExtCustomizeCmdTreeNode * pParentNode = pRibbonNode->GetParentNode();
	if(		pParentNode != NULL
		&&	pParentNode->IsKindOf( RUNTIME_CLASS(CExtRibbonNodeQuickAccessButtonsCollection) )
		)
	{
		ASSERT_VALID( pParentNode );
		return CExtBarButton::RibbonILV_CalcSize( dc, __EXT_RIBBON_ILV_SIMPLE_SMALL );
	}
// 	if( nILV < 0 )
// 		nILV = RibbonILV_Get( 0 );
CPoint ptLayout( 0, 0 );
CSize _sizeLayout =
		( const_cast < CExtRibbonButtonGroup * > ( this ) )
		-> OnRibbonCalcLayout( dc, ptLayout, false );
	return _sizeLayout;
}

CSize CExtRibbonButtonGroup::OnRibbonCalcCollapsedGroupSize( CDC & dc )
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( ! IsVisible() )
		return CSize( 0, 0 );
	if( (GetStyle()&TBBS_HIDDEN) != 0 )
		return CSize( 0, 0 );
CSize _sizeLayout( 0, 0 );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
CExtCmdIcon * pCmdIcon = pRibbonPage->PmBridge_GetPM()->Ribbon_GetGroupCollapsedIcon( this );
	if( pCmdIcon != NULL && (! pCmdIcon->IsEmpty() ) )
	{
		_sizeLayout = pCmdIcon->GetSize();
		_sizeLayout.cx = max( _sizeLayout.cx, 32 );
		_sizeLayout.cy = max( _sizeLayout.cy, 32 );
		_sizeLayout.cx += 4;
		_sizeLayout.cy += 6;
	} // if( pCmdIcon != NULL && (! pCmdIcon->IsEmpty() ) )
CSize _sizeText = OnRibbonCalcLargeTextSize( dc );
	_sizeText.cx += 10;
	_sizeLayout.cx = max( _sizeLayout.cx, _sizeText.cx );
	_sizeLayout.cy += _sizeText.cy;
CRect rcCP = OnRibbonGetContentPadding();
	_sizeLayout.cx += rcCP.left + rcCP.right;
//	_sizeLayout.cy += rcCP.top + rcCP.bottom;
	_sizeLayout.cx = max( _sizeLayout.cx, 45 );
	return _sizeLayout;
}

CSize CExtRibbonButtonGroup::OnRibbonCalcLayout(
	CDC & dc,
	CPoint & ptCalcLayoutOffset,
	bool bSetupRects
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( ! IsVisible() )
		return CSize( 0, 0 );
	if( (GetStyle()&TBBS_HIDDEN) != 0 )
		return CSize( 0, 0 );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
INT nDistanceBefore = OnRibbonGetOuterGroupDistance( true );
INT nDistanceAfter = OnRibbonGetOuterGroupDistance( false );
CRect rcCP = OnRibbonGetContentPadding();
	if( pRibbonPage->RibbonLayout_GroupCaptionIsTopAligned( this ) )
		rcCP.top += pRibbonPage->RibbonLayout_GroupCaptionGetHeight( this );
	else
		rcCP.bottom += pRibbonPage->RibbonLayout_GroupCaptionGetHeight( this );
	if( TopCollapsedStateGet() )
	{
		CSize _sizeLayout = OnRibbonCalcCollapsedGroupSize( dc );
		if( bSetupRects )
		{
			CRect rcTBB( ptCalcLayoutOffset, _sizeLayout );
			rcTBB.OffsetRect( nDistanceBefore, 0 );
			SetLayoutRect( dc, rcTBB );
		}
		_sizeLayout.cx += nDistanceBefore + nDistanceAfter;
		ptCalcLayoutOffset.x += nDistanceBefore + nDistanceAfter;
		return _sizeLayout;
	} // if( TopCollapsedStateGet() )
CPoint ptGroupLayout = ptCalcLayoutOffset;
	ptGroupLayout.x += rcCP.left + nDistanceBefore;
	ptGroupLayout.y += rcCP.top;
CSize _sizeLayout( 0, 0 );
INT nIndex, nCount = ChildButtonGetCount();
INT nLineWidth = 0, nLineHeight = 0, nMaxLineHeight = 0, nLineHeightSaved = 0,
		nMaxAvailableLineHeight = pRibbonPage->RibbonLayout_GetGroupHeight( this ) - rcCP.top - rcCP.bottom;
INT nSizeMinCaptionArea = 0;
	m_bHelperCaptionTextIsPartiallyVisible = false;
CExtBarButton * pLastVisibleTBB = NULL;
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtBarButton * pTBB = ChildButtonGetAt( nIndex );
		ASSERT_VALID( pTBB );
		if( bSetupRects )
			pTBB->SetWrap( CExtBarButton::__EVT_FLOAT, false );
		if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonDialogLauncher) ) )
		{
			pTBB->SetWrap( CExtBarButton::__EVT_FLOAT, true );
			CSize _sizeTBB = pTBB->RibbonILV_CalcSize( dc, -1 );
			if( bSetupRects )
			{
				CRect rcTBB( 0, 0, _sizeTBB.cx, _sizeTBB.cy );
				pTBB->SetLayoutRect( dc, rcTBB );
				pTBB->Show( false );
			}
			nSizeMinCaptionArea += _sizeTBB.cx + 1;
			continue;
		} // if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonDialogLauncher) ) )
		if( ! pTBB->IsVisible() )
			continue;
		if( (pTBB->GetStyle()&TBBS_HIDDEN) != 0 )
			continue;
		if( pTBB->IsSeparator() )
		{
			INT nSeparatorWidth = pTBB->OnRibbonGetSeparatorExtent( true );
			CRect rcCpTBB = pTBB->OnRibbonGetContentPadding();
			nMaxLineHeight = max( nMaxLineHeight, nLineHeight );
			_sizeLayout.cx += nLineWidth + nSeparatorWidth + rcCpTBB.left + rcCpTBB.right;
			ptGroupLayout.x += nLineWidth;
			ptGroupLayout.y = ptCalcLayoutOffset.y + rcCP.top;
			nLineWidth = nLineHeight = 0;
			if( bSetupRects )
			{
				pTBB->SetWrap( CExtBarButton::__EVT_FLOAT, true );
				if( pLastVisibleTBB != NULL )
					pLastVisibleTBB->SetWrap( CExtBarButton::__EVT_FLOAT, true );
				pLastVisibleTBB = pTBB;
				CRect rcTBB( ptGroupLayout + rcCpTBB.TopLeft(), CSize( nSeparatorWidth, 100 ) );
				rcTBB.OffsetRect( -nDistanceBefore, 0 );
				pTBB->SetLayoutRect( dc, rcTBB );
			}
			ptGroupLayout.x += nSeparatorWidth + rcCpTBB.left + rcCpTBB.right;
			continue;
		} // if( pTBB->IsSeparator() )
		CSize _sizeTBB = pTBB->RibbonILV_CalcSize( dc, -1 );
		CRect rcCpTBB = pTBB->OnRibbonGetContentPadding();
		nLineHeightSaved = nLineHeight;
		nLineHeight += _sizeTBB.cy + rcCpTBB.top + rcCpTBB.bottom;
		if( nLineHeight > nMaxAvailableLineHeight )
		{
			nMaxLineHeight = max( nMaxLineHeight, nLineHeightSaved );
			_sizeLayout.cx += nLineWidth;
			ptGroupLayout.x += nLineWidth;
			ptGroupLayout.y = ptCalcLayoutOffset.y + rcCP.top;
			nLineWidth = _sizeTBB.cx + rcCpTBB.left + rcCpTBB.right;
			nLineHeight = _sizeTBB.cy + rcCpTBB.top + rcCpTBB.bottom;
			if( bSetupRects )
			{
				if( pLastVisibleTBB != NULL )
					pLastVisibleTBB->SetWrap( CExtBarButton::__EVT_FLOAT, true );
				else
					pTBB->SetWrap( CExtBarButton::__EVT_FLOAT, true );
				pLastVisibleTBB = pTBB;
				CRect rcTBB( ptGroupLayout + rcCpTBB.TopLeft(), _sizeTBB );
				rcTBB.OffsetRect( -nDistanceBefore, 0 );
				pTBB->SetLayoutRect( dc, rcTBB );
			}
			ptGroupLayout.y += nLineHeight;
		} // if( nLineHeight > nMaxAvailableLineHeight )
		else
		{
			if( bSetupRects )
			{
				pLastVisibleTBB = pTBB;
				CRect rcTBB( ptGroupLayout + rcCpTBB.TopLeft(), _sizeTBB );
				rcTBB.OffsetRect( -nDistanceBefore, 0 );
				pTBB->SetLayoutRect( dc, rcTBB );
			}
			INT nWidthTBB = _sizeTBB.cx + rcCpTBB.left + rcCpTBB.right;
			nLineWidth = max( nLineWidth, nWidthTBB );
			nMaxLineHeight = max( nMaxLineHeight, nLineHeight );
			ptGroupLayout.y += _sizeTBB.cy + rcCpTBB.top + rcCpTBB.bottom;
		} // else from if( nLineHeight > nMaxAvailableLineHeight )
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	if( bSetupRects && pLastVisibleTBB != NULL )
		pLastVisibleTBB->SetWrap( CExtBarButton::__EVT_FLOAT, true );
	_sizeLayout.cx += nLineWidth;
	nMaxLineHeight = max( nMaxLineHeight, nLineHeightSaved );
CExtSafeString strCaptionText = GetText();
	if( ! strCaptionText.IsEmpty() )
	{
		strCaptionText.Replace( _T("\r"), _T("") );
		strCaptionText.Replace( _T("\n"), _T(" ") );
		CFont * pFont = & ( pRibbonPage->PmBridge_GetPM()->m_FontRibbonGroupCaption );
		if( pFont->GetSafeHandle() == NULL )
			pFont = pRibbonPage->OnGetToolbarFont( false, true, this );
		nSizeMinCaptionArea += CExtPaintManager::stat_CalcTextDimension( dc, *pFont, strCaptionText, DT_SINGLELINE|DT_LEFT|DT_TOP|DT_CALCRECT ).Width() + 6;
	} // if( ! strCaptionText.IsEmpty() )
	if( nSizeMinCaptionArea > _sizeLayout.cx )
	{
		m_bHelperCaptionTextIsPartiallyVisible = true;
		if( pRibbonPage->OnRibbonQueryGroupCaptionTextCompletelyVisible( this ) )
		{
			m_bHelperCaptionTextIsPartiallyVisible = false;
			INT nShift = ( nSizeMinCaptionArea - _sizeLayout.cx ) / 2;
			_sizeLayout.cx = nSizeMinCaptionArea;
			if( nShift > 0 && bSetupRects )
			{
				for( nIndex = 0; nIndex < nCount; nIndex ++ )
				{
					CExtBarButton * pTBB = ChildButtonGetAt( nIndex );
					ASSERT_VALID( pTBB );
					if( ! pTBB->IsVisible() )
						continue;
					if( (pTBB->GetStyle()&TBBS_HIDDEN) != 0 )
						continue;
					if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonDialogLauncher) ) )
						continue;
					CRect rcTBB = *pTBB;
					rcTBB.OffsetRect( nShift, 0 );
					pTBB->SetLayoutRect( dc, rcTBB );
				}
			}
		} // if( pRibbonPage->OnRibbonQueryGroupCaptionTextCompletelyVisible( this ) )
	} // if( nSizeMinCaptionArea > _sizeLayout.cx )
	_sizeLayout.cy += nMaxLineHeight;
	_sizeLayout.cx += rcCP.left + rcCP.right;
	_sizeLayout.cy += rcCP.top + rcCP.bottom;
	_sizeLayout.cy -= 2;
	if(		ParentButtonGet() == NULL
		&&	_sizeLayout.cx < 40
		)
		_sizeLayout.cx = 40;
	if( bSetupRects )
	{
		CRect rcTBB( ptCalcLayoutOffset, _sizeLayout );
 		rcTBB.OffsetRect( nDistanceBefore, 0 );
		SetLayoutRect( dc, rcTBB );
	}
	_sizeLayout.cx += nDistanceBefore + nDistanceAfter;
	return _sizeLayout;
}

CRect CExtRibbonButtonGroup::OnRibbonGetContentAlignmentRect() const
{
	ASSERT_VALID( this );
const CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
CRect rcContentAlignment =
		pRibbonPage->RibbonLayout_GetGroupContentAlignmentRect( this );
	return rcContentAlignment;
}

void CExtRibbonButtonGroup::OnRibbonGetSimpleGroupContentAlignmentFlags(
	CDC & dc,
	bool & bUseHorizontalAlignment,
	bool & bUseVerticalAlignment
	) const
{
	ASSERT_VALID( this );
const CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
	pRibbonPage->RibbonLayout_GetSimpleGroupContentAlignmentFlags(
		dc,
		this,
		bUseHorizontalAlignment,
		bUseVerticalAlignment
		);
}

void CExtRibbonButtonGroup::OnRibbonAlignDialogLauncherButtons( CDC & dc )
{
	ASSERT_VALID( this );
	m_rcCaptionTextAlignmentDLB.SetRect( 0, 0, 0, 0 );
	if( TopCollapsedStateGet() )
		return;
	if( ! OnRibbonIsCaptionVisible() )
		return;
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	if( pRibbonPage == NULL )
		return;
	ASSERT_VALID( pRibbonPage );
	if(		pRibbonPage->m_bHelperPopupMode
		&&	( ! pRibbonPage->m_bHelperAutoHideMode )
		&&	( ! pRibbonPage->PmBridge_GetPM()->Ribbon_IsPopupGroupWithCaption( this ) )
		)
		return;
	if( ! pRibbonPage->RibbonLayout_GroupCaptionIsVisible( this ) )
		return;
INT nCaptionHeight = pRibbonPage->RibbonLayout_GroupCaptionGetHeight( this );
bool bCaptionAtTop = pRibbonPage->RibbonLayout_GroupCaptionIsTopAligned( this );
CRect rcAlignment = *this;
	if( bCaptionAtTop )
		rcAlignment.bottom = rcAlignment.top + nCaptionHeight;
	else
		rcAlignment.top = rcAlignment.bottom - nCaptionHeight;
CRect rcCP = OnRibbonGetContentPadding();
INT nAlignmentHeight = rcAlignment.Height();
INT nChildIndex, nChildCount = ChildButtonGetCount();
INT nDistanceBetweenDLB = 4;
	for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
	{
		CExtBarButton * pTBB = ChildButtonGetAt( nChildIndex );
		ASSERT_VALID( pTBB );
		if( ! pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonDialogLauncher) ) )
			continue;
		if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
			continue;
		pTBB->Show( true );
		CRect rcTBB = *pTBB;
		INT nWidthTBB = rcTBB.Width();
		INT nHeightTBB = rcTBB.Height();
		INT nOffsetX = rcAlignment.right - rcTBB.right;
		INT nOffsetY = rcAlignment.top - rcTBB.top + ( nAlignmentHeight - nHeightTBB ) / 2;
//		if( nOffsetX != 0 && nOffsetY != 0 )
		{
			rcTBB.OffsetRect(
				nOffsetX - nDistanceBetweenDLB,
				nOffsetY
				);
			if( rcTBB.left <= rcAlignment.left )
				break; // other buttons will be left hidden
			pTBB->SetLayoutRect( dc, rcTBB );
		}
		pTBB->OnRibbonAlignContent( dc );
		rcAlignment.right -= nWidthTBB - nDistanceBetweenDLB;
	} // for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
	m_rcCaptionTextAlignmentDLB = rcAlignment;
}

void CExtRibbonButtonGroup::OnRibbonAlignContent( CDC & dc )
{
	ASSERT_VALID( this );
bool bUseHorizontalAlignment = false, bUseVerticalAlignment = false;
	OnRibbonGetSimpleGroupContentAlignmentFlags(
		dc,
		bUseHorizontalAlignment,
		bUseVerticalAlignment
		);
INT nAvailableLineHeight = 0, nLineHeight = 0,
	nSpaceY = 0, nY = 0, nOffsetY = 0,
	nLineWidth = 0, nOffsetX = 0,
	nLineIndex, nLineStartIndex, nVisibleButtonCountInLine = 0;
CRect rcCA( 0, 0, 0, 0 );
	if( bUseVerticalAlignment )
	{
		rcCA = OnRibbonGetContentAlignmentRect();
		nAvailableLineHeight = rcCA.Height();
	}
CRect rcContentAlignment = OnRibbonGetContentAlignmentRect();
INT nChildIndex, nChildCount = ChildButtonGetCount();
	for( nLineStartIndex = nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
	{
		CExtBarButton * pTBB = ChildButtonGetAt( nChildIndex );
		ASSERT_VALID( pTBB );
		if( ! pTBB->IsVisible() )
		{
			if( bUseHorizontalAlignment || bUseVerticalAlignment )
			{
				if( nLineStartIndex == nChildIndex )
					nLineStartIndex++;
			} // if( bUseHorizontalAlignment || bUseVerticalAlignment )
			continue;
		}
		if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
		{
			if( bUseHorizontalAlignment || bUseVerticalAlignment )
			{
				if( nLineStartIndex == nChildIndex )
					nLineStartIndex++;
			} // if( bUseHorizontalAlignment || bUseVerticalAlignment )
			continue;
		}
		if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonDialogLauncher) ) )
		{
			if( bUseHorizontalAlignment || bUseVerticalAlignment )
			{
				if( nLineStartIndex == nChildIndex )
					nLineStartIndex++;
			} // if( bUseHorizontalAlignment || bUseVerticalAlignment )
			continue;
		} // if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonDialogLauncher) ) )
		if( pTBB->IsSeparator() )
		{
			CRect rcTBB = *pTBB;
			if(		rcTBB.top != rcContentAlignment.top
				||	rcTBB.bottom != rcContentAlignment.bottom
				)
			{
				rcTBB.top = rcContentAlignment.top;
				rcTBB.bottom = rcContentAlignment.bottom;
				pTBB->SetRect( rcTBB );
			}
			if( bUseHorizontalAlignment || bUseVerticalAlignment )
			{
				nLineStartIndex = nChildIndex + 1;
				nLineWidth = nLineHeight = 0;
				nVisibleButtonCountInLine = 0;
			} // if( bUseHorizontalAlignment || bUseVerticalAlignment )
			continue;
		} // if( pTBB->IsSeparator() )
		if( bUseHorizontalAlignment || bUseVerticalAlignment )
		{
			CRect rcTBB = *pTBB;
			if( bUseHorizontalAlignment )
			{
				INT nWidthTBB = rcTBB.Width();
				nLineWidth = max( nLineWidth, nWidthTBB );
			}
			if( bUseVerticalAlignment )
				nLineHeight += rcTBB.Height();
			nVisibleButtonCountInLine ++;
			if( pTBB->IsWrap( CExtBarButton::__EVT_FLOAT ) )
			{
				if( bUseVerticalAlignment )
				{
					nSpaceY =
						::MulDiv(
							nAvailableLineHeight - nLineHeight,
							1,
							nVisibleButtonCountInLine + 1
							);
					if( nSpaceY < 0 )
						nSpaceY = 0;
					nY = rcCA.top + nSpaceY;
				}
				for( nLineIndex = nLineStartIndex; nLineIndex <= nChildIndex; nLineIndex ++ )
				{
					pTBB = ChildButtonGetAt( nLineIndex );
					ASSERT_VALID( pTBB );
					if( ! pTBB->IsVisible() )
						continue;
					if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
						continue;
					nOffsetX = nOffsetY = 0;
					CRect rcTBB = *pTBB;
					if( bUseHorizontalAlignment )
						nOffsetX = ( nLineWidth - rcTBB.Width() ) / 2;
					if( bUseVerticalAlignment )
						nOffsetY = nY - rcTBB.top;
					if( nOffsetX != 0 || nOffsetY != 0 )
					{
						rcTBB.OffsetRect(
							nOffsetX,
							nOffsetY
							);
						pTBB->SetLayoutRect( dc, rcTBB );
					} // if( nOffsetX != 0 || nOffsetY != 0 )
					pTBB->OnRibbonAlignContent( dc );
					if( bUseVerticalAlignment )
						nY += rcTBB.Height() + nSpaceY;
				} // for( nLineIndex = nLineStartIndex; nLineIndex <= nChildIndex; nLineIndex ++ )
				nLineStartIndex = nChildIndex + 1;
				nLineWidth = nLineHeight = 0;
				nVisibleButtonCountInLine = 0;
			} // if( pTBB->IsWrap( CExtBarButton::__EVT_FLOAT ) )
		} // if( bUseHorizontalAlignment || bUseVerticalAlignment )
		else
			pTBB->OnRibbonAlignContent( dc );
	} // for( nLineStartIndex = nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
	OnRibbonAlignDialogLauncherButtons( dc );
}

void CExtRibbonButtonGroup::OnRibbonPopupShow(
	CExtRibbonPopupMenuWnd * pPopup,
	bool bShow
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pPopup );
	pPopup;
	m_bRibbonPopupVisible = bShow;
	RedrawButton();
	if( ! m_bRibbonPopupVisible )
		SetHover( false );
}

bool CExtRibbonButtonGroup::OnQueryFlatTrackingEnabled() const
{
	ASSERT_VALID( this );
	if( ! IsVisible() )
		return false;
	if( ( GetStyle() & TBBS_HIDDEN ) != 0 )
		return false;
	if( TopCollapsedStateGet() )
		return true;
	return false;
}

CExtSafeString CExtRibbonButtonGroup::GetText() const
{
	ASSERT_VALID( this );
INT nILV = RibbonILV_Get( 0 );	
	if( nILV == __EXT_RIBBON_ILV_SIMPLE_SMALL )
	{
		const CExtBarButton * pTBB = ParentButtonGet();
		if(		pTBB == NULL
			||	(! pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonTabPage) ) )
			)
			return CExtBarButton::GetText(); // QATB mode
		return _T("");
	}
	return CExtBarButton::GetText();
}

bool CExtRibbonButtonGroup::IsPressed() const
{
	ASSERT_VALID( this );
	if( m_bRibbonPopupVisible )
		return true;
	return CExtRibbonButton::IsPressed();
}

CRect CExtRibbonButtonGroup::GetCaptionTextAlignmentRect() const
{
	ASSERT_VALID( this );
	return m_rcCaptionTextAlignmentDLB;
}

void CExtRibbonButtonGroup::OnKeyTipTrackingQuery(
	bool bShow,
	CExtCustomizeCmdKeyTip * pKeyTipChain,
	HDWP & hPassiveModeDWP
	)
{
	if(		bShow
		&&	( ! TopCollapsedStateGet() )
		)
	{
		CExtRibbonButton::OnKeyTipTrackingQuery( false, NULL, hPassiveModeDWP );
		return;
	}
	CExtRibbonButton::OnKeyTipTrackingQuery( bShow, pKeyTipChain, hPassiveModeDWP );
}

void CExtRibbonButtonGroup::OnKeyTipTrackingQueryNested(
	bool bShow,
	CExtCustomizeCmdKeyTip * pKeyTipChain,
	bool bApplyToThisItem,
	HDWP & hPassiveModeDWP
	)
{
	ASSERT_VALID( this );
	if(		bShow
		&&	ParentButtonGet() == NULL
		&&	TopCollapsedStateGet()
		)
	{
		CExtRibbonButton::OnKeyTipTrackingQuery( true, pKeyTipChain, hPassiveModeDWP );
		bShow = false;
		pKeyTipChain = NULL;
		bApplyToThisItem = false;
	}
	CExtRibbonButton::OnKeyTipTrackingQueryNested( bShow, pKeyTipChain, bApplyToThisItem, hPassiveModeDWP );
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonButtonToolGroup

IMPLEMENT_DYNCREATE( CExtRibbonButtonToolGroup, CExtRibbonButtonGroup );

CExtRibbonButtonToolGroup::CExtRibbonButtonToolGroup(
	CExtRibbonPage * pBar, // = NULL
	UINT nCmdID, // = ID_SEPARATOR
	UINT nStyle // = 0
	)
	: CExtRibbonButtonGroup( pBar, nCmdID, nStyle )
{
}

CExtRibbonButtonToolGroup::~CExtRibbonButtonToolGroup()
{
}

void CExtRibbonButtonToolGroup::PaintCompound(
	CDC & dc,
	bool bPaintParentChain,
	bool bPaintChildren,
	bool bPaintOneNearestChildrenLevelOnly
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( GetBar() );
	ASSERT_VALID( (&dc) );
	ASSERT( dc.GetSafeHdc() != NULL );

	if( ! IsPaintAble( dc ) )
		return;
	if( AnimationClient_StatePaint( dc ) )
		return;
	if( bPaintParentChain )
		PaintParentChain( dc );

#if (defined __EXT_RIBBON_DEBUG_PAINTING)

CRect rcRibbonGroupButton = *this;
	dc.FillSolidRect( &rcRibbonGroupButton, (255<<16)|255 );
//	dc.Draw3dRect( &rcRibbonGroupButton, 0, 0 );

INT nChildIndex, nChildCount = ChildButtonGetCount();
	if( bPaintChildren )
	{
		for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
		{
			CExtBarButton * pTBB = ChildButtonGetAt( nChildIndex );
			ASSERT_VALID( pTBB );
			if( ! pTBB->IsVisible() )
				continue;
			if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				continue;
			pTBB->PaintCompound( dc, false, ! bPaintOneNearestChildrenLevelOnly, false );
		} // for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
	}
	// debug version of group caption
	if( OnRibbonIsCaptionVisible() )
	{
		CRect rcRibbonGroupCaption = OnRibbonGetCaptionRect();
		if( GetRibbonPage()->RibbonLayout_GroupCaptionIsTopAligned(this) )
			rcRibbonGroupCaption.DeflateRect( 1, 1, 1, 0 );
		else
			rcRibbonGroupCaption.DeflateRect( 1, 0, 1, 1 );
		dc.FillSolidRect( &rcRibbonGroupCaption, 0 );
	} // if( OnRibbonIsCaptionVisible() )

#else // (defined __EXT_RIBBON_DEBUG_PAINTING)

	CExtRibbonButtonGroup::PaintCompound( dc, false, bPaintChildren, bPaintOneNearestChildrenLevelOnly );

#endif // else from (defined __EXT_RIBBON_DEBUG_PAINTING)

}

CSize CExtRibbonButtonToolGroup::OnRibbonCalcLayout(
	CDC & dc,
	CPoint & ptCalcLayoutOffset,
	bool bSetupRects
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( ! IsVisible() )
		return CSize( 0, 0 );
	if( (GetStyle()&TBBS_HIDDEN) != 0 )
		return CSize( 0, 0 );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
INT nDistanceBefore = OnRibbonGetOuterGroupDistance( true );
INT nDistanceAfter = OnRibbonGetOuterGroupDistance( false );
CRect rcCP = OnRibbonGetContentPadding();
	if( pRibbonPage->RibbonLayout_GroupCaptionIsTopAligned( this ) )
		rcCP.top += pRibbonPage->RibbonLayout_GroupCaptionGetHeight( this );
	if( TopCollapsedStateGet() )
	{
		CSize _sizeLayout = OnRibbonCalcCollapsedGroupSize( dc );
		if( bSetupRects )
		{
			CRect rcTBB( ptCalcLayoutOffset, _sizeLayout );
			rcTBB.OffsetRect( nDistanceBefore, 0 );
			SetLayoutRect( dc, rcTBB );
		}
		_sizeLayout.cx += nDistanceBefore + nDistanceAfter;
		ptCalcLayoutOffset.x += nDistanceBefore + nDistanceAfter;
		return _sizeLayout;
	} // if( TopCollapsedStateGet() )
CPoint ptGroupLayout = ptCalcLayoutOffset;
	ptGroupLayout.x += rcCP.left + nDistanceBefore;
	ptGroupLayout.y += rcCP.top;
CSize _sizeLayout( 0, 0 );
INT nIndex, nCount = ChildButtonGetCount();
INT nLineWidth = 0, nLineHeight = 0, nMaxLineWidth = 0;
INT nSizeMinCaptionArea = 0;
	m_bHelperCaptionTextIsPartiallyVisible = false;
bool bLastWrapPassed = false;
CExtBarButton * pLastVisibleTBB = NULL;
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtBarButton * pTBB = ChildButtonGetAt( nIndex );
		ASSERT_VALID( pTBB );
		if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonDialogLauncher) ) )
		{
			pTBB->SetWrap( CExtBarButton::__EVT_FLOAT, true );
			CSize _sizeTBB = pTBB->RibbonILV_CalcSize( dc, -1 );
			if( bSetupRects )
			{
				CRect rcTBB( 0, 0, _sizeTBB.cx, _sizeTBB.cy );
				pTBB->SetLayoutRect( dc, rcTBB );
				pTBB->Show( false );
			} 
			nSizeMinCaptionArea += _sizeTBB.cx + 1;
			continue;
		} // if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonDialogLauncher) ) )
		if( ! pTBB->IsVisible() )
			continue;
		if( (pTBB->GetStyle()&TBBS_HIDDEN) != 0 )
			continue;
		if( pTBB->IsSeparator() )
		{
			INT nSeparatorHeight = pTBB->OnRibbonGetSeparatorExtent( false );
			CRect rcCpTBB = pTBB->OnRibbonGetContentPadding();
			ptGroupLayout.x = ptCalcLayoutOffset.x + rcCP.left;
			ptGroupLayout.y += nLineHeight;
			_sizeLayout.cy += nLineHeight;
			nLineWidth = nLineHeight = 0;
			bLastWrapPassed = true;
			if( bSetupRects )
			{
				pTBB->SetWrap( CExtBarButton::__EVT_FLOAT, true );
				if( pLastVisibleTBB != NULL )
					pLastVisibleTBB->SetWrap( CExtBarButton::__EVT_FLOAT, true );
				pLastVisibleTBB = pTBB;
				CRect rcTBB( ptGroupLayout + rcCpTBB.TopLeft(), CSize( 100, nSeparatorHeight ) );
				rcTBB.OffsetRect( -nDistanceBefore, 0 );
				pTBB->SetLayoutRect( dc, rcTBB );
			}
			ptGroupLayout.y += nSeparatorHeight + rcCpTBB.top + rcCpTBB.bottom;
			_sizeLayout.cy += nSeparatorHeight + rcCpTBB.top + rcCpTBB.bottom;
			continue;
		} // if( pTBB->IsSeparator() )
		CSize _sizeTBB = pTBB->RibbonILV_CalcSize( dc, -1 );
		bool bRibbonWrap = pTBB->RibbonWrapFromILE( pTBB->RibbonILE_Get() );
		CRect rcCpTBB = pTBB->OnRibbonGetContentPadding();
		INT nWidthTBB = _sizeTBB.cx + rcCpTBB.left + rcCpTBB.right;
		INT nHeightTBB = _sizeTBB.cy + rcCpTBB.top + rcCpTBB.bottom;
		nLineWidth += nWidthTBB;
		nMaxLineWidth = max( nMaxLineWidth, nLineWidth );
		nLineHeight = max( nLineHeight, nHeightTBB );
		if( bSetupRects )
		{
			CRect rcTBB( ptGroupLayout + rcCpTBB.TopLeft(), _sizeTBB );
			pTBB->SetLayoutRect( dc, rcTBB );
			rcTBB.OffsetRect( -nDistanceBefore, 0 );
			pTBB->SetWrap( CExtBarButton::__EVT_FLOAT, bRibbonWrap );
			pLastVisibleTBB = pTBB;
		}
		if( bRibbonWrap )
		{
			ptGroupLayout.x = ptCalcLayoutOffset.x + rcCP.left;
			ptGroupLayout.y += nLineHeight;
			_sizeLayout.cy += nLineHeight;
			nLineWidth = nLineHeight = 0;
			bLastWrapPassed = true;
		} // if( bRibbonWrap )
		else
		{
			ptGroupLayout.x += nWidthTBB;
			bLastWrapPassed = false;
		} // else from if( bRibbonWrap )
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	if( bSetupRects && pLastVisibleTBB != NULL )
		pLastVisibleTBB->SetWrap( CExtBarButton::__EVT_FLOAT, true );
	if( ! bLastWrapPassed )
		_sizeLayout.cy += nLineHeight;
	_sizeLayout.cy --;
	_sizeLayout.cx += nMaxLineWidth;
CExtSafeString strCaptionText = GetText();
	if( ! strCaptionText.IsEmpty() )
	{
		strCaptionText.Replace( _T("\r"), _T("") );
		strCaptionText.Replace( _T("\n"), _T(" ") );
		CFont * pFont = & ( pRibbonPage->PmBridge_GetPM()->m_FontRibbonGroupCaption );
		if( pFont->GetSafeHandle() == NULL )
			pFont = pRibbonPage->OnGetToolbarFont( false, true, this );
		nSizeMinCaptionArea += CExtPaintManager::stat_CalcTextDimension( dc, *pFont, strCaptionText, DT_SINGLELINE|DT_LEFT|DT_TOP|DT_CALCRECT ).Width() + 6;
	} // if( ! strCaptionText.IsEmpty() )
	if( nSizeMinCaptionArea > _sizeLayout.cx )
	{
		m_bHelperCaptionTextIsPartiallyVisible = true;
		if( pRibbonPage->OnRibbonQueryGroupCaptionTextCompletelyVisible( this ) )
		{
			m_bHelperCaptionTextIsPartiallyVisible = false;
			INT nShift = ( nSizeMinCaptionArea - _sizeLayout.cx ) / 2;
			_sizeLayout.cx = nSizeMinCaptionArea;
			if( nShift > 0 && bSetupRects )
			{
				for( nIndex = 0; nIndex < nCount; nIndex ++ )
				{
					CExtBarButton * pTBB = ChildButtonGetAt( nIndex );
					ASSERT_VALID( pTBB );
					if( ! pTBB->IsVisible() )
						continue;
					if( (pTBB->GetStyle()&TBBS_HIDDEN) != 0 )
						continue;
					if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonDialogLauncher) ) )
						continue;
					CRect rcTBB = *pTBB;
					rcTBB.OffsetRect( nShift, 0 );
					pTBB->SetLayoutRect( dc, rcTBB );
				}
			}
		} // if( pRibbonPage->OnRibbonQueryGroupCaptionTextCompletelyVisible( this ) )
	} // if( nSizeMinCaptionArea > _sizeLayout.cx )
	_sizeLayout.cx += rcCP.left + rcCP.right;
	_sizeLayout.cy += rcCP.top + rcCP.bottom;
	if(		ParentButtonGet() == NULL
		&&	_sizeLayout.cx < 40
		)
		_sizeLayout.cx = 40;
	if( bSetupRects )
	{
		CRect rcTBB( ptCalcLayoutOffset, _sizeLayout );
		rcTBB.OffsetRect( nDistanceBefore, 0 );
		SetLayoutRect( dc, rcTBB );
	}
	_sizeLayout.cx += nDistanceBefore + nDistanceAfter;
	return _sizeLayout;
}

void CExtRibbonButtonToolGroup::OnRibbonGetToolGroupContentAlignmentFlags(
	CDC & dc,
	bool & bUseHorizontalAlignment,
	bool & bUseVerticalAlignment,
	bool & bUseToolRowVerticalAlignment
	) const
{
	ASSERT_VALID( this );
const CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
	pRibbonPage->RibbonLayout_GetToolGroupContentAlignmentFlags(
		dc,
		this,
		bUseHorizontalAlignment,
		bUseVerticalAlignment,
		bUseToolRowVerticalAlignment
		);
}

void CExtRibbonButtonToolGroup::OnRibbonAlignContent( CDC & dc )
{
	ASSERT_VALID( this );
bool bUseHorizontalAlignment = false, bUseVerticalAlignment = false,
		bUseToolRowVerticalAlignment = false;
	OnRibbonGetToolGroupContentAlignmentFlags(
		dc,
		bUseHorizontalAlignment,
		bUseVerticalAlignment,
		bUseToolRowVerticalAlignment
		);
INT nAvailableLineWidth = 0, nAvailableLineHeight = 0,
		nVisibleButtonCountInLine = 0;
CRect rcCA( 0, 0, 0, 0 );
	if( bUseHorizontalAlignment || bUseVerticalAlignment )
	{
		rcCA = OnRibbonGetContentAlignmentRect();
		nAvailableLineWidth = rcCA.Width();
		nAvailableLineHeight = rcCA.Height();
	}
CRect rcContentAlignment = OnRibbonGetContentAlignmentRect();
INT nLineWidth = 0, nLineHeight = 0, nContentHeight = 0,
		nOffsetX = 0, nSpaceY = 0, nY = 0, nOffsetY = 0,
		nLineStartIndex, nLineIndex, nLineCount = 0,
		nChildIndex, nChildCount = ChildButtonGetCount();
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
CExtPaintManager * pPM = pRibbonPage->PmBridge_GetPM();
	ASSERT_VALID( pPM );
bool bTextFieldsOnly = true;
	if( bUseVerticalAlignment )
	{
		for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
		{
			CExtBarButton * pTBB = ChildButtonGetAt( nChildIndex );
			ASSERT_VALID( pTBB );
			if( ! pTBB->IsVisible() )
				continue;
			if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				continue;
			if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonDialogLauncher) ) )
				continue;
			CRect rcTBB = *pTBB;
			CRect rcCpTBB = pTBB->OnRibbonGetContentPadding();
			INT nHeightTBB = rcTBB.Height(); // + rcCpTBB.top + rcCpTBB.bottom;
			if( pTBB->IsSeparator() )
			{
				nContentHeight += nHeightTBB;
				nLineHeight = 0;
				nLineCount ++;
				continue;
			} // if( pTBB->IsSeparator() )
			nLineHeight = max( nLineHeight, nHeightTBB );
			if( pTBB->IsWrap( CExtBarButton::__EVT_FLOAT ) )
			{
				nContentHeight += nLineHeight;
				nLineHeight = 0;
				nLineCount ++;
			} // if( pTBB->IsWrap( CExtBarButton::__EVT_FLOAT ) )
#if (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)
			if(		bTextFieldsOnly
				&&	(! pTBB->IsKindOf( RUNTIME_CLASS( CExtBarTextFieldButton ) ) )
				)
				bTextFieldsOnly = false;
#endif // (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)
		} // for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
		nLineHeight = 0;
		nSpaceY =
			::MulDiv(
				nAvailableLineHeight - nContentHeight,
				1,
				nLineCount + 1
				);
		//nY = rcCA.top + nSpaceY;
		nY = rcCA.top;
		if( nSpaceY > 0 )
			nY += nSpaceY;
	} // if( bUseVerticalAlignment )
	else
		bTextFieldsOnly = false;
INT nExtraOffset = 0, nExtraDistance = 0;
	if( ! bTextFieldsOnly )
	{
		CExtBarButton * pParentTBB = ParentButtonGet();
		if( pParentTBB == NULL )
		{
			nExtraOffset = pPM->Ribbon_GetToolGroupExtraOffset( this );
			nExtraDistance = pPM->Ribbon_GetToolGroupExtraLineDistance( this );
		} // if( pParentTBB == NULL )
	} // if( ! bTextFieldsOnly )
	nY += nExtraOffset;
	for( nLineStartIndex = nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
	{
		CExtBarButton * pTBB = ChildButtonGetAt( nChildIndex );
		ASSERT_VALID( pTBB );
		if( ! pTBB->IsVisible() )
		{
			if( bUseHorizontalAlignment || bUseVerticalAlignment || bUseToolRowVerticalAlignment )
			{
				if( nLineStartIndex == nChildIndex )
					nLineStartIndex++;
			} // if( bUseHorizontalAlignment || bUseVerticalAlignment || bUseToolRowVerticalAlignment )
			continue;
		}
		if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
		{
			if( bUseHorizontalAlignment || bUseVerticalAlignment || bUseToolRowVerticalAlignment )
			{
				if( nLineStartIndex == nChildIndex )
					nLineStartIndex++;
			} // if( bUseHorizontalAlignment || bUseVerticalAlignment || bUseToolRowVerticalAlignment )
			continue;
		}
		if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonDialogLauncher) ) )
		{
			if( bUseHorizontalAlignment || bUseVerticalAlignment || bUseToolRowVerticalAlignment )
			{
				if( nLineStartIndex == nChildIndex )
					nLineStartIndex++;
			} // if( bUseHorizontalAlignment || bUseVerticalAlignment || bUseToolRowVerticalAlignment )
			continue;
		} // if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonDialogLauncher) ) )
		if( pTBB->IsSeparator() )
		{
			CRect rcTBB = *pTBB;
			nOffsetY = 0;
			if( bUseVerticalAlignment )
			{
				nOffsetY = nY - rcTBB.top;
				rcTBB.OffsetRect( 0, nOffsetY );
			}
			if(		nOffsetY != 0
				||	rcTBB.left != rcContentAlignment.left
				||	rcTBB.right != rcContentAlignment.right
				)
			{
				rcTBB.left = rcContentAlignment.left;
				rcTBB.right = rcContentAlignment.right;
				pTBB->SetRect( rcTBB );
			}
			if( bUseVerticalAlignment )
				nY += rcTBB.Height() + nSpaceY;
			if( bUseHorizontalAlignment || bUseVerticalAlignment || bUseToolRowVerticalAlignment )
			{
				nLineStartIndex = nChildIndex + 1;
				nLineWidth = nLineHeight = 0;
				nVisibleButtonCountInLine = 0;
			} // if( bUseHorizontalAlignment || bUseVerticalAlignment || bUseToolRowVerticalAlignment )
			continue;
		} // if( pTBB->IsSeparator() )
		if( bUseHorizontalAlignment || bUseVerticalAlignment || bUseToolRowVerticalAlignment )
		{
			CRect rcTBB = *pTBB;
			CRect rcCpTBB = pTBB->OnRibbonGetContentPadding();
			if( bUseHorizontalAlignment )
				nLineWidth += rcTBB.Width() + rcCpTBB.left + rcCpTBB.right;
			if( bUseToolRowVerticalAlignment || bUseVerticalAlignment )
			{
				INT nHeightTBB = rcTBB.Height(); // + rcCpTBB.top + rcCpTBB.bottom;
				nLineHeight = max( nLineHeight, nHeightTBB );
			}
			nVisibleButtonCountInLine ++;
			if( pTBB->IsWrap( CExtBarButton::__EVT_FLOAT ) )
			{
				if( bUseHorizontalAlignment )
					nOffsetX = ( nAvailableLineWidth - nLineWidth ) / 2;
				for( nLineIndex = nLineStartIndex; nLineIndex <= nChildIndex; nLineIndex ++ )
				{
					pTBB = ChildButtonGetAt( nLineIndex );
					ASSERT_VALID( pTBB );
					if( ! pTBB->IsVisible() )
						continue;
					if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
						continue;
					if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonDialogLauncher) ) )
						continue;
					nOffsetY = 0;
					CRect rcTBB = *pTBB;
					if( bUseToolRowVerticalAlignment )
						nOffsetY += ( nLineHeight - rcTBB.Height() ) / 2;
					if( bUseVerticalAlignment )
						nOffsetY += nY - rcTBB.top;
					if( nOffsetX != 0 || nOffsetY != 0 )
					{
						rcTBB.OffsetRect(
							nOffsetX,
							nOffsetY
							);
						pTBB->SetLayoutRect( dc, rcTBB );
					} // if( nOffsetX != 0 || nOffsetY != 0 )
					pTBB->OnRibbonAlignContent( dc );
				} // for( nLineIndex = nLineStartIndex; nLineIndex <= nChildIndex; nLineIndex ++ )
				nLineHeight += nExtraDistance;
				if( bUseVerticalAlignment )
					nY += nLineHeight + nSpaceY;
				nLineStartIndex = nChildIndex + 1;
				nLineWidth = nLineHeight = 0;
				nVisibleButtonCountInLine = 0;
			} // if( pTBB->IsWrap( CExtBarButton::__EVT_FLOAT ) )
		} // if( bUseHorizontalAlignment || bUseVerticalAlignment || bUseToolRowVerticalAlignment )
		else
			pTBB->OnRibbonAlignContent( dc );
	} // for( nLineStartIndex = nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
	OnRibbonAlignDialogLauncherButtons( dc );
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonPage

IMPLEMENT_DYNCREATE( CExtRibbonPage, CExtMenuControlBar );

CExtRibbonPage::CExtRibbonPage()
	: m_pRibbonNode( NULL )
	, m_bHelperDwmPaintingMode( false )
	, m_bHelperPopupMode( false )
	, m_bHelperAutoHideMode( false )
	, m_byteHelper2007SeparatorAlpha( BYTE(0) )
	, m_pQACEB( NULL )
	, m_bRibbonQuickAccessBarIsAboveTheRibbon( true )
	, m_bRibbonPageIsExpandedMode( true )
	, m_pPopupPageMenuGroup( NULL )
	, m_pPopupPageMenuAutoHide( NULL )
	, m_nMinILE( __EXT_RIBBON_ILE_MAX )
	, m_nMaxILE( __EXT_RIBBON_ILE_MAX )
	, m_hWndSrcRibbonPage( NULL )
	, m_hWndParentRibbonPage( NULL )
	, m_bRibbonSimplifiedLayoutAlgorithm( true )
	, m_bRibbonSimplifiedLayoutDelayedFlush( false )
{
	m_bHelperKeyTipsSupported = true;
	//m_bMenuBarUsesDelayedButtonMenuTracking = false;
	m_bCustomizeHookAllowed = false;
	m_bNoForcedRecalcMetrics = true;
	m_bCustomizationAllowed = false;
	m_bForceBalloonGradientInDialogs = false;
	m_bForceNoBalloonWhenRedockable = true;
	m_bHelperFlatTrackingCalcEnabledLeftRight = true;
	m_bHelperFlatTrackingCalcEnabledUpDown = true;
	m_bHelperFlatTrackingCalcEnabledTab = true;
	_RibbonPageRslaResetStateData();
	_AdjustBGInfo();
}

void CExtRibbonPage::_RibbonPageRslaResetStateData()
{
	ASSERT_VALID( this );
	m_nRslaLastContentWidth = -32767;
	m_nRslaLastDesiredWidth = -32767;
	m_nRslaLastILE = -32767;
	m_bRslaLastStateHasCollapsedGroups = false;
	if( GetSafeHwnd() != NULL )
	{
		INT nGroupIndex, nGroupCount = RibbonGroupButton_GetCount();
		for( nGroupIndex = 0; nGroupIndex < nGroupCount; nGroupIndex ++ )
		{
			CExtRibbonButtonGroup * pGroupTBB = RibbonGroupButton_GetAt( nGroupIndex );
			ASSERT_VALID( pGroupTBB );
			pGroupTBB->TopCollapsedStateSet( false );
			pGroupTBB->RibbonILE_Set( m_nMaxILE );
			RibbonLayout_IleReset(
				pGroupTBB,
				__EXT_RIBBON_ILE_MAX, // m_nMaxILE,
				true,
				false
				);
		} // for( nGroupIndex = 0; nGroupIndex < nGroupCount; nGroupIndex ++ )
	} // if( GetSafeHwnd() != NULL )
}

CExtRibbonPage::~CExtRibbonPage()
{
	if( m_bHelperPopupMode && m_pRibbonNode != NULL )
		delete m_pRibbonNode;
	SetButtons();
	m_arrGroupButtons.RemoveAll();
	m_arrTabPageButtons.RemoveAll();
	if( m_pPopupPageMenuGroup )
		delete m_pPopupPageMenuGroup;
	if( m_pPopupPageMenuAutoHide )
		delete m_pPopupPageMenuAutoHide;
}

void CExtRibbonPage::Ribbon_UpdateLayout(
	bool bDelay // = false
	)
{
	ASSERT_VALID( this );
	m_bRibbonSimplifiedLayoutDelayedFlush = true;
	_RibbonPageRslaResetStateData();
	if( GetSafeHwnd() == NULL )
		return;
	if( ! bDelay )
		_RecalcPositionsImpl();
}

bool CExtRibbonPage::Ribbon_UseSimplifiedLayoutAlgorithmGet() const
{
	ASSERT_VALID( this );
	return m_bRibbonSimplifiedLayoutAlgorithm;
}

void CExtRibbonPage::Ribbon_UseSimplifiedLayoutAlgorithmSet( bool bSet )
{
	ASSERT_VALID( this );
	if(		( m_bRibbonSimplifiedLayoutAlgorithm && bSet )
		||	( (!m_bRibbonSimplifiedLayoutAlgorithm) && (!bSet) )
		)
		return;
	m_bRibbonSimplifiedLayoutAlgorithm = bSet;
	_RibbonPageRslaResetStateData();
}

CExtRibbonPage * CExtRibbonPage::GetMainRibbonPage()
{
	ASSERT_VALID( this );
	if(		m_hWndSrcRibbonPage == NULL
		||	(! ::IsWindow( m_hWndSrcRibbonPage ) )
		)
		return NULL;
	ASSERT_VALID( this );
	if( ! m_bHelperPopupMode )
		return this;
CWnd * pWnd = CWnd::FromHandlePermanent( m_hWndSrcRibbonPage );
	if( pWnd == NULL )
		return NULL;
	ASSERT_VALID( pWnd );
CExtRibbonPage * pRibbonPage =
		DYNAMIC_DOWNCAST( CExtRibbonPage, pWnd );
	if( pRibbonPage == NULL )
		return NULL;
	if( pRibbonPage->m_bHelperPopupMode )
	{
		CExtRibbonPage * pRibbonPage2 =
			pRibbonPage->GetMainRibbonPage();
		if( pRibbonPage2 != NULL )
			return pRibbonPage2;
		return NULL;
	}
	return pRibbonPage;
}

const CExtRibbonPage * CExtRibbonPage::GetMainRibbonPage() const
{
	return
		( const_cast < CExtRibbonPage * > ( this ) )
		-> GetMainRibbonPage();
}

CExtRibbonPage * CExtRibbonPage::GetParentRibbonPage()
{
	ASSERT_VALID( this );
	if(		m_hWndParentRibbonPage == NULL
		||	(! ::IsWindow( m_hWndParentRibbonPage ) )
		)
		return NULL;
	ASSERT_VALID( this );
CWnd * pWnd = CWnd::FromHandlePermanent( m_hWndParentRibbonPage );
	if( pWnd == NULL )
		return NULL;
	ASSERT_VALID( pWnd );
CExtRibbonPage * pRibbonPage =
		DYNAMIC_DOWNCAST( CExtRibbonPage, pWnd );
	return pRibbonPage;
}

const CExtRibbonPage * CExtRibbonPage::GetParentRibbonPage() const
{
	return
		( const_cast < CExtRibbonPage * > ( this ) )
		-> GetParentRibbonPage();
}

CExtPaintManager * CExtRibbonPage::PmBridge_GetPM() const
{
	ASSERT_VALID( this );
	return CExtMenuControlBar::PmBridge_GetPM();
}

void CExtRibbonPage::PmBridge_OnPaintManagerChanged(
	CExtPaintManager * pGlobalPM
	)
{
	ASSERT_VALID( this );
	CExtMenuControlBar::PmBridge_OnPaintManagerChanged( pGlobalPM );
	CExtCustomizeSite::PmBridge_OnPaintManagerChanged( pGlobalPM );
CExtCustomizeCmdTreeNode * pRibbonNode = Ribbon_GetRootNode();
	if( pRibbonNode != NULL )
	{
		ASSERT_VALID( pRibbonNode );
		CExtPaintManager * pPM = PmBridge_GetPM();
		ASSERT_VALID( pPM );
		pRibbonNode->OnSysColorChange( pPM, this );
	} // if( pRibbonNode != NULL )
	m_bRibbonSimplifiedLayoutDelayedFlush = true;
}

INT CExtRibbonPage::Ribbon_PageSelectionGet() const
{
	ASSERT_VALID( this );
	return -1;
}

bool CExtRibbonPage::Ribbon_PageSelectionSet(
	INT nSelIdx,
	bool bEnableAnimation // = true
	)
{
	ASSERT_VALID( this );
	nSelIdx;
	bEnableAnimation;
	return false;
}

CExtRibbonNode * CExtRibbonPage::Ribbon_GetRootNode()
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pRibbonNode != NULL )
	{
		ASSERT_VALID( m_pRibbonNode );
	}
#endif // _DEBUG
	return m_pRibbonNode;
}

const CExtRibbonNode * CExtRibbonPage::Ribbon_GetRootNode() const
{
	ASSERT_VALID( this );
	return ( const_cast < CExtRibbonPage * > ( this ) ) -> Ribbon_GetRootNode();
}

BEGIN_MESSAGE_MAP( CExtRibbonPage, CExtMenuControlBar )
    //{{AFX_MSG_MAP(CExtRibbonPage)
	ON_WM_NCCALCSIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDBLCLK()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_MBUTTONDBLCLK()
	ON_WM_MOUSEWHEEL()
	ON_WM_MOUSEACTIVATE()
	//}}AFX_MSG_MAP
	ON_WM_SYSCOLORCHANGE()
	__EXT_MFC_SAFE_ON_WM_SETTINGCHANGE()
	ON_MESSAGE(__ExtMfc_WM_THEMECHANGED, OnThemeChanged)
	ON_REGISTERED_MESSAGE( CExtPopupMenuWnd::g_nMsgNotifyMenuClosed, OnMenuClosed )
	ON_REGISTERED_MESSAGE( CExtPopupMenuWnd::g_nMsgRibbonFileMenuButtonQuery, _OnRibbonFileMenuButtonQuery )
	ON_REGISTERED_MESSAGE( CExtPopupMenuWnd::g_nMsgRibbonFileMenuButtonInvocation, _OnRibbonFileMenuButtonInvocation )
END_MESSAGE_MAP()

bool CExtRibbonPage::CustomizeStateSerialize(
	CArchive & ar,
	bool bEnableThrowExceptions // = false
	)
{
	try
	{
		if( ! CExtCustomizeSite::CustomizeStateSerialize(
				ar,
				bEnableThrowExceptions
				)
			)
			return false;
		DWORD dwRibbonSpecificFlags = 0;
		if( ar.IsStoring() )
		{
			if( RibbonQuickAccessBar_AboveTheRibbonGet() )
				dwRibbonSpecificFlags |= 0x00000001;
			if( RibbonPage_ExpandedModeGet() )
				dwRibbonSpecificFlags |= 0x00000002;
			ar << dwRibbonSpecificFlags;
		} // if( ar.IsStoring() )
		else
		{
			_RibbonPageRslaResetStateData();
			ar >> dwRibbonSpecificFlags;
			RibbonQuickAccessBar_AboveTheRibbonSet(
				( (dwRibbonSpecificFlags&0x00000001) != 0 ) ? true : false,
				false
				);
			RibbonPage_ExpandedModeSet(
				( (dwRibbonSpecificFlags&0x00000002) != 0 ) ? true : false,
				false
				);
		} // else from if( ar.IsStoring() )
		return true;
	} // try
	catch( CException * pXept )
	{
		if( bEnableThrowExceptions )
			throw;
		pXept->Delete();
		// ASSERT( FALSE );
	} // catch( CException * pXept )
	catch( ... )
	{
		if( bEnableThrowExceptions )
			throw;
		// ASSERT( FALSE );
	} // catch( ... )
	return false;
}

CWnd * CExtRibbonPage::GetCustomizeAccelUpdatingTargetWnd()
{
	ASSERT( this != NULL );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage != this && pRibbonPage != NULL )
		return pRibbonPage->GetCustomizeAccelUpdatingTargetWnd();
CWnd * pWnd = GetParent();
	if( pWnd == NULL )
		return pWnd;
	return pWnd;
}

CWnd * CExtRibbonPage::GetCustomizeTopWnd()
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage != this && pRibbonPage != NULL )
		return pRibbonPage->GetCustomizeTopWnd();
	if( GetSafeHwnd() == NULL )
		return NULL;
	return this;
}

void CExtRibbonPage::OnRibbonBuildCommandCategories(
	CExtCustomizeCmdTreeNode * pNode,
	__EXT_MFC_SAFE_LPCTSTR strCommandCategoryName // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pNode );
	if( m_bHelperPopupMode )
		return;
CExtSafeString strCommandCategoryNameNested;
	if(		strCommandCategoryName != NULL
		&&	_tcslen(strCommandCategoryName) > 0
		&&	( ! pNode->IsKindOf( RUNTIME_CLASS( CExtRibbonNodeGroup ) ) )
		)
	{
		strCommandCategoryNameNested = strCommandCategoryName;
		UINT nCmdID = pNode->GetCmdID( false );
		if( CExtCmdManager::IsCommand( nCmdID ) )
		{
			CExtCmdItem * pCmdItem =
				g_CmdManager->CmdGetPtr(
					g_CmdManager->ProfileNameFromWnd( m_hWnd ),
					nCmdID
					);
			if( pCmdItem != NULL )
			{
				if( pNode->OnGetCommandsListBoxInfo( NULL, this, pCmdItem ) )
				{
					CategoryUpdate(
						strCommandCategoryName,
						&nCmdID,
						1
						);
				}
			}
		} // if( CExtCmdManager::IsCommand( nCmdID ) )
	}
	else
	{
		if(		pNode->IsKindOf( RUNTIME_CLASS( CExtRibbonNodeTabPage ) )
			||	(	pNode->IsKindOf( RUNTIME_CLASS( CExtRibbonNodeGroup ) )
				&&	pNode->GetParentNode() == m_pRibbonNode
				)
			)
		{
			LPCTSTR strNodeText = pNode->GetTextInToolbar( NULL );
			if( strNodeText == NULL || _tcslen( strNodeText ) == 0 )
				strNodeText = pNode->GetTextInMenu( NULL );
			if( strNodeText == NULL )
				strNodeText = _T("");
			CExtSafeString strFmt;
			if( ! g_ResourceManager->LoadString( strFmt, IDS_EXT_RIBBON_CUSTOMIZE_NODE_TAB_FMT ) )
				strFmt = _T("%s Tab");
			strCommandCategoryNameNested.Format( LPCTSTR(strFmt), strNodeText );
		} // if( pNode->IsKindOf( RUNTIME_CLASS( CExtRibbonNodeTabPage ) ) )
		else
			strCommandCategoryNameNested = strCommandCategoryName;
	}
	INT nChildNodeIndex, nChildNodeCount = pNode->GetNodeCount();
	for( nChildNodeIndex = 0; nChildNodeIndex < nChildNodeCount; nChildNodeIndex ++ )
	{
		CExtCustomizeCmdTreeNode * pChildNode = pNode->ElementAt( nChildNodeIndex );
		ASSERT_VALID( pChildNode );
		OnRibbonBuildCommandCategories( pChildNode, LPCTSTR(strCommandCategoryNameNested) );
	} // for( nChildNodeIndex = 0; nChildNodeIndex < nChildNodeCount; nChildNodeIndex ++ )
}

#if (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)

CExtCustomizeSite * CExtRibbonPage::GetCustomizeSite()
{
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		CExtCustomizeSite * pSite =
			CExtMenuControlBar::GetCustomizeSite();
		return pSite;
	} // if( pRibbonPage == NULL )
	else
	{
		CExtCustomizeSite * pSite =
			pRibbonPage->GetCustomizeSite();
		return pSite;
	} // else from if( pRibbonPage == NULL )
}

CSize CExtRibbonPage::OnPopupListBoxCalcItemExtraSizes(
	const CExtBarButton * pTBB,
	const CExtCustomizeCmdTreeNode * pNode
	) const
{
	ASSERT_VALID( this );
const CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		CSize _size =
			CExtCustomizeSite::OnPopupListBoxCalcItemExtraSizes(
				pTBB,
				pNode
				);
		return _size;
	} // if( pRibbonPage == NULL )
	else
	{
		CSize _size =
			pRibbonPage->OnPopupListBoxCalcItemExtraSizes(
				pTBB,
				pNode
				);
		return _size;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnPopupListBoxInitContent(
	CExtBarButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CListBox & wndListBox
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnPopupListBoxInitContent(
				pTBB,
				pNode,
				wndListBox
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnPopupListBoxInitContent(
				pTBB,
				pNode,
				wndListBox
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

CSize CExtRibbonPage::OnPopupListBoxAdjustSize(
	CExtBarButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CListBox & wndListBox
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		CSize sizeListBox =
			CExtCustomizeSite::OnPopupListBoxAdjustSize(
				pTBB,
				pNode,
				wndListBox
				);
		return sizeListBox;
	} // if( pRibbonPage == NULL )
	else
	{
		CSize sizeListBox =
			pRibbonPage->OnPopupListBoxAdjustSize(
				pTBB,
				pNode,
				wndListBox
				);
		return sizeListBox;
	} // else from if( pRibbonPage == NULL )
}

CSize CExtRibbonPage::OnPopupListBoxMeasureTrackSize(
	const CExtBarButton * pTBB,
	const CExtCustomizeCmdTreeNode * pNode
	) const
{
	ASSERT_VALID( this );
const CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		CSize _size =
			CExtCustomizeSite::OnPopupListBoxMeasureTrackSize(
				pTBB,
				pNode
				);
		return _size;
	} // if( pRibbonPage == NULL )
	else
	{
		CSize _size =
			pRibbonPage->OnPopupListBoxMeasureTrackSize(
				pTBB,
				pNode
				);
		return _size;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnPopupListBoxItemDraw(
	CExtBarButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CListBox & wndListBox,
	LPDRAWITEMSTRUCT pDIS
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnPopupListBoxItemDraw(
				pTBB,
				pNode,
				wndListBox,
				pDIS
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnPopupListBoxItemDraw(
				pTBB,
				pNode,
				wndListBox,
				pDIS
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnPopupListBoxItemMeasure(
	CExtBarButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CListBox & wndListBox,
	LPMEASUREITEMSTRUCT pMIS
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnPopupListBoxItemMeasure(
				pTBB,
				pNode,
				wndListBox,
				pMIS
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnPopupListBoxItemMeasure(
				pTBB,
				pNode,
				wndListBox,
				pMIS
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnPopupListBoxSelChange(
	CExtBarButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CListBox & wndListBox
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnPopupListBoxSelChange(
				pTBB,
				pNode,
				wndListBox
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnPopupListBoxSelChange(
				pTBB,
				pNode,
				wndListBox
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnPopupListBoxSelEndOK(
	CExtBarButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CListBox & wndListBox
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnPopupListBoxSelEndOK(
				pTBB,
				pNode,
				wndListBox
				);
		OnMenuClosed( 0xFFFFL, 0L );
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnPopupListBoxSelEndOK(
				pTBB,
				pNode,
				wndListBox
				);
		OnMenuClosed( 0xFFFFL, 0L );
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnPopupListBoxSelEndCancel(
	CExtBarButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CListBox & wndListBox
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnPopupListBoxSelEndCancel(
				pTBB,
				pNode,
				wndListBox
				);
		OnMenuClosed( 0xFFFFL, 0L );
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnPopupListBoxSelEndCancel(
				pTBB,
				pNode,
				wndListBox
				);
		OnMenuClosed( 0xFFFFL, 0L );
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnPopupListBoxClose(
	CExtBarButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CListBox & wndListBox
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnPopupListBoxClose(
				pTBB,
				pNode,
				wndListBox
				);
//		OnMenuClosed( 0xFFFFL, 0L );
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnPopupListBoxClose(
				pTBB,
				pNode,
				wndListBox
				);
//		OnMenuClosed( 0xFFFFL, 0L );
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnPopupListBoxGetStyles(
	CExtBarButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	DWORD & dwListBoxStyles
	) const
{
	ASSERT_VALID( this );
const CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnPopupListBoxGetStyles(
				pTBB,
				pNode,
				dwListBoxStyles
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnPopupListBoxGetStyles(
				pTBB,
				pNode,
				dwListBoxStyles
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnPopupDatePickerGetStyles(
	CExtBarButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CSize & szCalendarDimensions,
	DWORD & dwDatePickerStyle
	) const
{
	ASSERT_VALID( this );
const CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnPopupDatePickerGetStyles(
				pTBB,
				pNode,
				szCalendarDimensions,
				dwDatePickerStyle
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnPopupDatePickerGetStyles(
				pTBB,
				pNode,
				szCalendarDimensions,
				dwDatePickerStyle
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnTextFieldVerify(
	CExtBarTextFieldButton * pTextFieldTBB,
	CExtCustomizeCmdTreeNode * pNode,
	__EXT_MFC_SAFE_LPCTSTR sTextOld,
	__EXT_MFC_SAFE_LPCTSTR sTextNew
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnTextFieldVerify(
				pTextFieldTBB,
				pNode,
				sTextOld,
				sTextNew
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnTextFieldVerify(
				pTextFieldTBB,
				pNode,
				sTextOld,
				sTextNew
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

void CExtRibbonPage::OnTextFieldInplaceTextGet(
	const CExtBarTextFieldButton * pTextFieldTBB,
	const CExtCustomizeCmdTreeNode * pNode,
	CExtSafeString & sTextFieldBuffer
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		CExtCustomizeSite::OnTextFieldInplaceTextGet(
			pTextFieldTBB,
			pNode,
			sTextFieldBuffer
			);
	} // if( pRibbonPage == NULL )
	else
	{
		pRibbonPage->OnTextFieldInplaceTextGet(
			pTextFieldTBB,
			pNode,
			sTextFieldBuffer
			);
	} // else from if( pRibbonPage == NULL )
}

void CExtRibbonPage::OnTextFieldInplaceTextSet(
	CExtBarTextFieldButton * pTextFieldTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CExtSafeString & sTextFieldBuffer,
	__EXT_MFC_SAFE_LPCTSTR sTextNew
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		CExtCustomizeSite::OnTextFieldInplaceTextSet(
			pTextFieldTBB,
			pNode,
			sTextFieldBuffer,
			sTextNew
			);
		OnMenuClosed( 0xFFFFL, 0L );
	} // if( pRibbonPage == NULL )
	else
	{
		pRibbonPage->OnTextFieldInplaceTextSet(
			pTextFieldTBB,
			pNode,
			sTextFieldBuffer,
			sTextNew
			);
		OnMenuClosed( 0xFFFFL, 0L );
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnTextFieldWndProcHook(
	LRESULT & lResult,
	UINT message,
	WPARAM wParam,
	LPARAM lParam,
	CEdit & wndEdit,
	CExtBarTextFieldButton * pTextFieldTBB,
	CExtCustomizeCmdTreeNode * pNode
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnTextFieldWndProcHook(
				lResult,
				message,
				wParam,
				lParam,
				wndEdit,
				pTextFieldTBB,
				pNode
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnTextFieldWndProcHook(
				lResult,
				message,
				wParam,
				lParam,
				wndEdit,
				pTextFieldTBB,
				pNode
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

CExtPopupControlMenuWnd * CExtRibbonPage::OnTextFieldCreateDropPopup(
	CExtBarTextFieldButton * pTextFieldTBB,
	CExtCustomizeCmdTreeNode * pNode,
	HWND hWndCmdReceiver,
	bool bContentExpanding
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		CExtPopupControlMenuWnd * pPopupControlMenuWnd =
			CExtCustomizeSite::OnTextFieldCreateDropPopup(
				pTextFieldTBB,
				pNode,
				hWndCmdReceiver,
				bContentExpanding
				);
		return pPopupControlMenuWnd;
	} // if( pRibbonPage == NULL )
	else
	{
		CExtPopupControlMenuWnd * pPopupControlMenuWnd =
			pRibbonPage->OnTextFieldCreateDropPopup(
				pTextFieldTBB,
				pNode,
				hWndCmdReceiver,
				bContentExpanding
				);
		return pPopupControlMenuWnd;
	} // else from if( pRibbonPage == NULL )
}

#ifndef __EXT_MFC_NO_BUILTIN_DATEFIELD

bool CExtRibbonPage::OnPopupDatePickerInitContent(
	CExtBarButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CExtDatePickerWnd & wndDatePicker
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnPopupDatePickerInitContent(
				pTBB,
				pNode,
				wndDatePicker
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnPopupDatePickerInitContent(
				pTBB,
				pNode,
				wndDatePicker
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnPopupDatePickerSelChange(
	CExtBarButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	LPVOID pSelectionNotification
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnPopupDatePickerSelChange(
				pTBB,
				pNode,
				pSelectionNotification
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnPopupDatePickerSelChange(
				pTBB,
				pNode,
				pSelectionNotification
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

#endif // __EXT_MFC_NO_BUILTIN_DATEFIELD

#ifndef __EXT_MFC_NO_UNDO_REDO_POPUP

bool CExtRibbonPage::OnPopupUndoRedoFormatCaption(
	CExtBarButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CExtPopupUndoRedoMenuWnd * pUndoRedoPopupMenuWnd,
	CExtSafeString & strCaption
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnPopupUndoRedoFormatCaption(
				pTBB,
				pNode,
				pUndoRedoPopupMenuWnd,
				strCaption
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnPopupUndoRedoFormatCaption(
				pTBB,
				pNode,
				pUndoRedoPopupMenuWnd,
				strCaption
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

#endif // __EXT_MFC_NO_UNDO_REDO_POPUP

#endif // (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)

void CExtRibbonPage::OnColorItemGenerateIcon(
	CExtBarColorButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	COLORREF clr,
	CExtCmdIcon & icon
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		CExtCustomizeSite::OnColorItemGenerateIcon(
			pTBB,
			pNode,
			clr,
			icon
			);
	} // if( pRibbonPage == NULL )
	else
	{
		pRibbonPage->OnColorItemGenerateIcon(
			pTBB,
			pNode,
			clr,
			icon
			);
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnColorItemValueGet(
	CExtBarColorButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	COLORREF & clr,
	bool bSelected
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnColorItemValueGet(
				pTBB,
				pNode,
				clr,
				bSelected
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnColorItemValueGet(
				pTBB,
				pNode,
				clr,
				bSelected
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnColorItemValueSet(
	CExtBarColorButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	COLORREF & clr,
	bool bSelected
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnColorItemValueSet(
				pTBB,
				pNode,
				clr,
				bSelected
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnColorItemValueSet(
				pTBB,
				pNode,
				clr,
				bSelected
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnColorItemChanged(
	CExtBarColorButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	bool bFinalChanging,
	COLORREF clr,
	LPARAM lParam
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnColorItemChanged(
				pTBB,
				pNode,
				bFinalChanging,
				clr,
				lParam
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnColorItemChanged(
				pTBB,
				pNode,
				bFinalChanging,
				clr,
				lParam
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnColorItemCustom(
	CExtCustomizeCmdTreeNode * pNode
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal = CExtCustomizeSite::OnColorItemCustom( pNode );
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal = pRibbonPage->OnColorItemCustom( pNode );
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnColorItemGetBtnTextDefault(
	CExtBarColorButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CExtSafeString & sBtnText
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnColorItemGetBtnTextDefault(
				pTBB,
				pNode,
				sBtnText
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnColorItemGetBtnTextDefault(
				pTBB,
				pNode,
				sBtnText
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::OnColorItemGetBtnTextCustom(
	CExtBarColorButton * pTBB,
	CExtCustomizeCmdTreeNode * pNode,
	CExtSafeString & sBtnText
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
	if( pRibbonPage == NULL )
	{
		bool bRetVal =
			CExtCustomizeSite::OnColorItemGetBtnTextCustom(
				pTBB,
				pNode,
				sBtnText
				);
		return bRetVal;
	} // if( pRibbonPage == NULL )
	else
	{
		bool bRetVal =
			pRibbonPage->OnColorItemGetBtnTextCustom(
				pTBB,
				pNode,
				sBtnText
				);
		return bRetVal;
	} // else from if( pRibbonPage == NULL )
}

bool CExtRibbonPage::DoFormCustomization(
	bool bDelayShow // = false
	)
{
	ASSERT_VALID( this );
	bDelayShow;
	return false;
}

INT CExtRibbonPage::RedrawCommandItems(
	UINT nCmdID,
	bool bUpdateWindows // = true
	) const
{
	ASSERT_VALID( this );
	ASSERT( CExtCmdManager::IsCommand(nCmdID) );
	if( ! IsVisible() )
		return 0;
INT nBtnsCount = GetButtonsCount();
	if( nBtnsCount == 0 )
		return 0;
INT nUpdateBtnsCount = 0;
bool bUpdateBar = false;
	for( INT nBtnIdx = 0; nBtnIdx < nBtnsCount; nBtnIdx++ )
	{
		CExtBarButton * pTBB = ( const_cast < CExtRibbonPage * > ( this ) ) -> GetButton( nBtnIdx );
		ASSERT_VALID( pTBB );
		UINT nTbbCmdID = pTBB->GetCmdID( false );
		if(		nTbbCmdID != nCmdID
			||	(! pTBB->IsVisible() )
			||	(pTBB->GetStyle()&TBBS_HIDDEN) != 0
			)
			continue;
		nUpdateBtnsCount ++;
		bUpdateBar = true;
		if( pTBB->IsKindOf(RUNTIME_CLASS(CExtBarColorButton)) )
			((CExtBarColorButton*)pTBB)->OnSyncIcon();
		pTBB->RedrawButton( false );
	} // for( INT nBtnIdx = 0; nBtnIdx < nBtnsCount; nBtnIdx++ )
	if( bUpdateWindows && bUpdateBar )
		( const_cast < CExtRibbonPage * > ( this ) ) -> UpdateWindow();
	return nUpdateBtnsCount;
}

INT CExtRibbonPage::UpdateAllCommandItems() const
{
	ASSERT_VALID( this );
	if( ! IsVisible() )
		return 0;
	( const_cast < CExtRibbonPage * > ( this ) ) -> DoCustomModeUpdateCmdUI();
	( const_cast < CExtRibbonPage * > ( this ) ) -> UpdateWindow();
	return 1;
}

CExtCustomizeSite::CCmdMenuInfo * CExtRibbonPage::MenuInfoGetDefault()
{
	ASSERT_VALID( this );
CCmdMenuInfo * pCmdMenuInfo = CExtCustomizeSite::MenuInfoGetDefault();
	return pCmdMenuInfo;
}

CExtCustomizeSite::CCmdMenuInfo * CExtRibbonPage::MenuInfoFindForMenuBar()
{
	ASSERT_VALID( this );
CCmdMenuInfo * pCmdMenuInfo = MenuInfoGetDefault();
	return pCmdMenuInfo;
}

CExtCustomizeSite::CCmdMenuInfo * CExtRibbonPage::MenuInfoActiveGet()
{
	ASSERT_VALID( this );
CCmdMenuInfo * pCmdMenuInfo = MenuInfoGetDefault();
	return pCmdMenuInfo;
}

CExtCustomizeSite::CCmdMenuInfo * CExtRibbonPage::MenuInfoActiveSet( CExtCustomizeSite::CCmdMenuInfo * pCmdMenuInfo )
{
	ASSERT( FALSE ); // should not be invoked
	pCmdMenuInfo;
	return NULL;
}

void CExtRibbonPage::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp)
{
	bCalcValidRects;
	lpncsp;
}

BOOL CExtRibbonPage::OnMouseWheel(UINT fFlags, short zDelta, CPoint point)
{
	ASSERT_VALID( this );
	return CExtMenuControlBar::OnMouseWheel( fFlags, zDelta, point );
}

int CExtRibbonPage::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message)
{
	ASSERT_VALID( this );
	if( m_bHelperPopupMode )
		return MA_NOACTIVATE;
	return CExtMenuControlBar::OnMouseActivate( pDesktopWnd, nHitTest, message );
}

void CExtRibbonPage::OnRegisterAdditionalToolBars()
{
	OnRegisterToolBar( this );
}

LRESULT CExtRibbonPage::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
__PROF_UIS_MANAGE_STATE;
	switch( message )
	{
	case WM_DISPLAYCHANGE:
		{
			LRESULT lRes = CExtMenuControlBar::WindowProc(message, wParam, lParam);
			CExtCustomizeCmdTreeNode * pRibbonNode = Ribbon_GetRootNode();
			if( pRibbonNode != NULL )
			{
				ASSERT_VALID( pRibbonNode );
				CExtPaintManager * pPM = PmBridge_GetPM();
				ASSERT_VALID( pPM );
				pRibbonNode->OnDisplayChange(wParam, lParam, pPM, this);
			} // if( pRibbonNode != NULL )
			return lRes;
		}
	break;
	case WM_WINDOWPOSCHANGED:
		m_bRibbonSimplifiedLayoutDelayedFlush = true;
	break;
	case WM_DESTROY:
		if(		m_hWndParentRibbonPage != NULL
			&&	( ::IsWindow( m_hWndParentRibbonPage ) )
			)
		{
			::InvalidateRect( m_hWndParentRibbonPage, NULL, TRUE );
			if( m_bHelperAutoHideMode )
				_CloseTrackingMenus();
		}
	break;
	default:
		if( message == g_nMsgSyncAccelTable )
		{
			if( ! m_bHelperPopupMode )
				OnUpdateAccelGlobalInfo( false );
			return 0L;
		} // if( nMessage == g_nMsgSyncAccelTable )
		else if( message == g_PaintManager.m_nMsgPaintManagerChanged )
		{
			MSG _msg;
			while(
				::PeekMessage(
					&_msg,
					m_hWnd,
					g_PaintManager.m_nMsgPaintManagerChanged,
					g_PaintManager.m_nMsgPaintManagerChanged,
					PM_REMOVE
					)
				);
			PmBridge_OnPaintManagerChanged( PmBridge_GetPM() );
			return 0L;
		} // else if( message == g_PaintManager.m_nMsgPaintManagerChanged )
	break;
	} // switch( message )
LRESULT lResult = CExtMenuControlBar::WindowProc( message, wParam, lParam );
	return lResult;
}

BOOL CExtRibbonPage::PreTranslateMessage(MSG* pMsg)
{
	if(		m_bHelperAutoHideMode
		&&	WM_KEYFIRST <= pMsg->message && pMsg->message <= WM_KEYLAST
		&&	m_pPopupPageMenuGroup->GetSafeHwnd() != NULL
		&&	m_pPopupPageMenuGroup->m_wndRibbonPage.GetSafeHwnd() != NULL
		)
		return
			m_pPopupPageMenuGroup->m_wndRibbonPage.
				TranslateMainFrameMessage( pMsg );
	return CExtMenuControlBar::PreTranslateMessage( pMsg );
}

void CExtRibbonPage::PreSubclassWindow()
{
	CExtMenuControlBar::PreSubclassWindow();
	if( m_bHelperPopupMode )
	{
	} // if( m_bHelperPopupMode )
	else
	{
		g_CmdManager->ProfileWndAdd(
			g_CmdManager->ProfileNameFromWnd( m_hWnd ),
			m_hWnd
			);
		EnableCustomization( this );
	} // else from if( m_bHelperPopupMode )
	//CExtMenuControlBar::PmBridge_Uninstall();
	CExtCustomizeSite::PmBridge_Uninstall();
}

bool CExtRibbonPage::OnHookWndMsg(
	LRESULT & lResult,
	HWND hWndHooked,
	UINT nMessage,
	WPARAM & wParam,
	LPARAM & lParam
	)
{
__PROF_UIS_MANAGE_STATE;
	if(		nMessage == CExtPopupMenuWnd::g_nMsgPopupNext
		||	nMessage == CExtPopupMenuWnd::g_nMsgPopupPrev
		)
		return false;
	if(		GetSafeHwnd() != NULL
		&&	CWnd::FromHandlePermanent(GetSafeHwnd()) == this
		&&	_DraggingGetBar() == NULL
		)
	{
		if( nMessage == g_nMsgQueryCustomizeSite )
		{
			CExtCustomizeSite ** pSite = (CExtCustomizeSite **)wParam;
			ASSERT( pSite != NULL );
			(*pSite) = this;
			lResult = 1L;
			return true;
		} // if( nMessage == g_nMsgQueryCustomizeSite )
		if(		nMessage == CExtPopupMenuWnd::g_nMsgPopupNext
			||	nMessage == CExtPopupMenuWnd::g_nMsgPopupPrev
			)
			return 
				CExtMenuControlBar::OnHookWndMsg(
					lResult,
					hWndHooked,
					nMessage,
					wParam,
					lParam
					);
	}

	return 
		CExtMenuControlBar::OnHookWndMsg(
			lResult,
			hWndHooked,
			nMessage,
			wParam,
			lParam
			);
}

bool CExtRibbonPage::DoDragCmdNode(
	ICustomizeDropSource * pCustomizeDragSource,
	CExtCustomizeCmdTreeNode * pNodeI,
	CExtCustomizeCmdTreeNode * pNodeC,
	RECT & rcItem
	)
{
	pCustomizeDragSource;
	pNodeI;
	pNodeC;
	rcItem;
	return false;
}

CExtPopupMenuTipWnd * CExtRibbonPage::OnAdvancedPopupMenuTipWndGet(
	CObject * pHelperSrc, // = NULL
	LPARAM lParam // = 0
	) const
{
	ASSERT_VALID( this );
	pHelperSrc;
	lParam;
	return (&m_wndScreenTip);
}

void CExtRibbonPage::OnAdvancedPopupMenuTipWndDisplay(
	CExtPopupMenuTipWnd & _ATTW,
	const RECT & rcExcludeArea,
	__EXT_MFC_SAFE_LPCTSTR strTipText,
	CObject * pHelperSrc, // = NULL
	LPARAM lParam // = 0
	) const
{
	ASSERT_VALID( this );
	m_wndScreenTip.m_cmdScreenTip.Empty();
	m_wndScreenTip.m_rcAlignment.SetRect( 0, 0, 0, 0 );
	if(		pHelperSrc != NULL
		&&	pHelperSrc->IsKindOf( RUNTIME_CLASS( CExtBarButton ) )
		)
	{
		CExtCustomizeCmdTreeNode * pNode = ((CExtBarButton*)pHelperSrc)->GetCmdNode( true );
		if( pNode != NULL )
		{
			CExtCustomizeCmdScreenTip * pCCST = pNode->CmdScreenTipFindForDisplaying( ! m_bLastDropDownHT );
			if( pCCST != NULL )
			{
				m_wndScreenTip.m_cmdScreenTip = (*pCCST);
				if(		rcExcludeArea.top != rcExcludeArea.bottom
					&&	(! pHelperSrc->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonFile ) ) )
					&&	(! pHelperSrc->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonQuickAccessContentExpand ) ) )
					&&	RibbonQuickAccessButton_GetIndexOf( (CExtBarButton*)pHelperSrc ) < 0
					&&	RibbonRightButton_GetIndexOf( (CExtBarButton*)pHelperSrc ) < 0
					)
				{
					CRect rcClient;
					GetClientRect( &rcClient );
					ClientToScreen( &rcClient );
					m_wndScreenTip.m_rcAlignment = rcExcludeArea;
					m_wndScreenTip.m_rcAlignment.top = min( m_wndScreenTip.m_rcAlignment.top, rcClient.top );
					m_wndScreenTip.m_rcAlignment.bottom = max( m_wndScreenTip.m_rcAlignment.bottom, rcClient.bottom );
				}
				//if( CExtToolControlBar::g_bToolbarShortcutKeysOnScreenTips )
				{
					UINT nCmdID = ((CExtBarButton*)pHelperSrc)->GetCmdID( false );
					if( CExtCmdManager::IsCommand( nCmdID ) )
					{
						CExtCmdItem * pCmdItem = g_CmdManager->CmdGetPtr( g_CmdManager->ProfileNameFromWnd( GetSafeHwnd() ), nCmdID );
						if(		pCmdItem != NULL
							&&	( ! pCmdItem->m_sAccelText.IsEmpty() )
							)
						{
							CExtSafeString str = m_wndScreenTip.m_cmdScreenTip.CaptionMainGet();
							if( ! str.IsEmpty() )
							{
								str += _T(" (");
								str += pCmdItem->m_sAccelText;
								str += _T(")");
								m_wndScreenTip.m_cmdScreenTip.CaptionMainSet( LPCTSTR( str ) );
							}
							else
							{
								CExtSafeString str = m_wndScreenTip.m_cmdScreenTip.TextMainGet();
								if( ! str.IsEmpty() )
								{
									str += _T(" (");
									str += pCmdItem->m_sAccelText;
									str += _T(")");
									m_wndScreenTip.m_cmdScreenTip.TextMainSet( LPCTSTR( str ) );
								}
							}
						}
					}
				}
			}
		}
	}
	CExtMenuControlBar::OnAdvancedPopupMenuTipWndDisplay( _ATTW, rcExcludeArea, strTipText, pHelperSrc, lParam );
}

CExtBarContentExpandButton * CExtRibbonPage::OnCreateBarRightBtn()
{
	ASSERT_VALID( this );
	return NULL;
}

CExtBarButton * CExtRibbonPage::OnCreateBarCommandBtn(
	UINT nCmdID,
	UINT nStyle // = 0
	)
{
	ASSERT_VALID( this );
CExtBarButton * pTBB = new CExtRibbonButton( this, nCmdID, nStyle );
	ASSERT_VALID( pTBB );
	return pTBB;
}

#ifdef _DEBUG

BOOL CExtRibbonPage::LoadToolBar(
	__EXT_MFC_SAFE_LPCTSTR lpszResourceName,
	COLORREF clrTransparent // = RGB(192,192,192)
	)
{
	ASSERT_VALID( this );
	lpszResourceName;
	clrTransparent;
	ASSERT( FALSE ); // this method should never be invoked
	return NULL;
}

BOOL CExtRibbonPage::InsertButton(
	int nPos, // = -1
	UINT nCmdID, // = ID_SEPARATOR
	BOOL bDoRecalcLayout // = TRUE
	)
{
	ASSERT_VALID( this );
	nPos;
	nCmdID;
	bDoRecalcLayout;
	ASSERT( FALSE ); // this method should never be invoked
	return NULL;
}

BOOL CExtRibbonPage::RemoveButton(
	int nPos,
	BOOL bDoRecalcLayout // = TRUE
	)
{
	ASSERT_VALID( this );
	if( IsKindOf( RUNTIME_CLASS( CExtRibbonBar ) ) )
		return CExtMenuControlBar::RemoveButton( nPos, bDoRecalcLayout );
	nPos;
	bDoRecalcLayout;
	ASSERT( FALSE ); // this method should never be invoked for CExtRibbonPage
	return NULL;
}

#endif // _DEBUG

BOOL CExtRibbonPage::InsertSpecButton(
	int nPos,
	CExtBarButton * pButton,
	BOOL bDoRecalcLayout // = TRUE
	)
{
	ASSERT_VALID( this );
	nPos;
	ASSERT_VALID( pButton );
	if( ! CExtMenuControlBar::InsertSpecButton( nPos, pButton, bDoRecalcLayout ) )
		return FALSE;
	if( pButton->ParentButtonGet() == NULL )
	{
		if( pButton->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonTabPage) ) )
			m_arrTabPageButtons.Add( ((CExtRibbonButtonTabPage*)pButton) );
		else
		{
			bool bQATBmode = false;
			CExtCustomizeCmdTreeNode * pButtonNode = pButton->GetCmdNode();
			if( pButtonNode != NULL )
			{
				ASSERT_VALID( pButtonNode );
				CExtCustomizeCmdTreeNode * pParentNode = pButtonNode->GetParentNode();
				if( pParentNode != NULL )
				{
					ASSERT_VALID( pParentNode );
					if( pParentNode->IsKindOf( RUNTIME_CLASS( CExtRibbonNodeRightButtonsCollection ) ) )
						m_arrRightButtons.Add( pButton );
					else if( pParentNode->IsKindOf( RUNTIME_CLASS( CExtRibbonNodeQuickAccessButtonsCollection ) ) )
					{
						bQATBmode = true;
						if( m_pQACEB == NULL )
						{
							m_arrQuickAccessButtons.Add( pButton );
							m_pQACEB = new CExtRibbonButtonQuickAccessContentExpand( this );
							InsertSpecButton( 0, m_pQACEB, FALSE );
							m_arrQuickAccessButtons.Add( m_pQACEB );
						}
						else
						{
							ASSERT( INT(m_arrQuickAccessButtons.GetSize()) > 0 );
							m_arrQuickAccessButtons.InsertAt( INT(m_arrQuickAccessButtons.GetSize()) - 1, pButton, 1 );
						}
					} // else if( pParentNode->IsKindOf( RUNTIME_CLASS( CExtRibbonNodeQuickAccessButtonsCollection ) ) )
				} // if( pParentNode != NULL )
			} // if( pButtonNode != NULL )
			if( ( ! bQATBmode ) && pButton->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonGroup) ) )
				m_arrGroupButtons.Add( ((CExtRibbonButtonGroup*)pButton) );
		}
	}
	return TRUE;
}

void CExtRibbonPage::_RemoveAllButtonsImpl()
{
	m_arrGroupButtons.RemoveAll();
	m_arrTabPageButtons.RemoveAll();
	m_arrRightButtons.RemoveAll();
	m_arrQuickAccessButtons.RemoveAll();
	m_pQACEB = NULL;
	CExtMenuControlBar::_RemoveAllButtonsImpl();
}

BOOL CExtRibbonPage::SetButtons(
	const UINT * lpIDArray, // = NULL
	int nIDCount // = 0
	)
{
	ASSERT_VALID( this );
	// IMPORTANT: this method can be used only to remove all the buttons
	ASSERT( lpIDArray == NULL && nIDCount == 0 );
	lpIDArray;
	nIDCount;
	return SetButtons( (CExtCustomizeCmdTreeNode*)NULL );
}

BOOL CExtRibbonPage::SetButtons(
	CExtCustomizeCmdTreeNode * pNode
	)
{
	ASSERT_VALID( this );
	m_bRibbonSimplifiedLayoutDelayedFlush = true;
	_RibbonPageRslaResetStateData();
	m_arrGroupButtons.RemoveAll();
	m_arrTabPageButtons.RemoveAll();
	MenuInfoRemove();
	CategoryRemoveAll();
	if( pNode == NULL )
	{
		CExtMenuControlBar::SetButtons();
		m_pRibbonNode = NULL;
		return TRUE;
	}
	if( m_pRibbonNode == pNode )
		return TRUE;
	CExtMenuControlBar::SetButtons();
	if( m_bHelperPopupMode && m_pRibbonNode != NULL )
		delete m_pRibbonNode;
	m_pRibbonNode = NULL;
	ASSERT_VALID( pNode );
	m_pRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNode );
	if( m_pRibbonNode == NULL )
		return FALSE;
	if( m_bHelperPopupMode )
		m_pWndTop = GetMainRibbonPage()->GetCustomizeTopWnd();
	m_pRibbonNode->Ribbon_InitCommandProfile( m_pWndTop->m_hWnd, true );
	if( m_bHelperAutoHideMode )
		m_pRibbonNode->CExtRibbonNode::Ribbon_InitBar(
			this,
			NULL,
			false
			);
	else
		m_pRibbonNode->Ribbon_InitBar( this, NULL, false );
	if( ! m_bHelperPopupMode )
	{
		MenuInfoAddEmpty( this, _T("Default"), true );
		OnRibbonBuildCommandCategories( m_pRibbonNode );
		int nCatIdx, nCatCnt = CategoryGetCount();
		for( nCatIdx = 0; nCatIdx < nCatCnt; nCatIdx ++ )
		{
			CExtCustomizeCmdTreeNode * pCatNode = CategoryGetTreeAt( nCatIdx );
			ASSERT_VALID( pCatNode );
			CExtSafeString str = pCatNode->GetTextUser();
			CategoryMakeCmdsUnique( LPCTSTR(str) );
		}
		CategoryAppendAllCommands();
		CategoryMakeAllCmdsUnique();
		OnUpdateAccelGlobalInfo( true );
	} // if( ! m_bHelperPopupMode )
	m_bRibbonSimplifiedLayoutDelayedFlush = true;
	_RibbonPageRslaResetStateData();
	Ribbon_OnCalcMinMaxILE();
	_RecalcPositionsImpl();
	return TRUE;
}

void CExtRibbonPage::OnFlatTrackingStart(
	HDWP & hPassiveModeDWP
	)
{
	ASSERT_VALID( this );
	CExtMenuControlBar::OnFlatTrackingStart( hPassiveModeDWP );
}

void CExtRibbonPage::OnFlatTrackingStop()
{
	ASSERT_VALID( this );
	CExtMenuControlBar::OnFlatTrackingStop();
// CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
// 	if(		pRibbonPage != NULL
// 		&&	pRibbonPage != this
// 		&&	CExtPopupMenuWnd::IsKeyPressed( VK_MENU )
// 		)
// 	{
// 		CExtPopupControlMenuWnd * pPopup = DYNAMIC_DOWNCAST( CExtPopupControlMenuWnd, GetParent() );
// 		if(		pPopup != NULL
// 			&&	( pPopup->TrackFlagsGet() & TPMX_NO_SITE ) != 0 
// 			)
// 			pPopup->DestroyWindow();
// 	}
}


void CExtRibbonPage::_CheckHitTestablePoint(
	CPoint & point
	)
{
	ASSERT_VALID( this );
	CExtMenuControlBar::_CheckHitTestablePoint( point );
	if( point.x == 32767 || point.y == 32767 )
		return;
INT nBarHT = HitTest( point );
	if( nBarHT < 0 )
		return;
CExtBarButton * pTBB = GetButton( nBarHT );
	ASSERT_VALID( pTBB );
	if( ! pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonFile ) ) )
		return;
CExtRibbonBar * pRibbonBar = DYNAMIC_DOWNCAST( CExtRibbonBar, this );
	if(		pRibbonBar != NULL
		&&	pRibbonBar->m_pExtNcFrameImpl != NULL
		&&	pRibbonBar->m_pExtNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement()
		)
		return;
CPoint ptScreen = point;
	ClientToScreen( &ptScreen );
UINT nNcHT = (UINT)
		SendMessage(
			WM_NCHITTEST,
			0,
			MAKELPARAM( ptScreen.x, ptScreen.y )
			);
	if( nNcHT != HTCLIENT )
	{
		point.x = point.y = 32767;
		return;
	} // if( nNcHT != HTCLIENT )
CWnd * pWndFindPopup = GetParent();
	for( ; pWndFindPopup != NULL; pWndFindPopup = pWndFindPopup->GetParent() )
	{
		if( ( pWndFindPopup->GetStyle() & WS_CHILD ) != 0 )
			continue;
		nNcHT = (UINT)
			pWndFindPopup->SendMessage(
				WM_NCHITTEST,
				0,
				MAKELPARAM( ptScreen.x, ptScreen.y )
				);
		if( nNcHT != HTCLIENT )
		{
			point.x = point.y = 32767;
			return;
		} // if( nNcHT != HTCLIENT )
		break;
	} // for( ; pWndFindPopup != NULL; pWndFindPopup = pWndFindPopup->GetParent() )
}

bool CExtRibbonPage::_UpdateHoverButton(
	CPoint point, // = CPoint(-1,-1) // default is use ::GetCursorPos()
	bool bEnableUpdateWindow // = true
	)
{
	ASSERT_VALID( this );
	bEnableUpdateWindow;
	return CExtMenuControlBar::_UpdateHoverButton( point, false );
}

void CExtRibbonPage::_DelayUpdateMenuBar()
{
	ASSERT_VALID( this );
	CExtMenuControlBar::_DelayUpdateMenuBar();
}

BOOL CExtRibbonPage::_UpdateMenuBar(
	BOOL bDoRecalcLayout //= TRUE
	)
{
	ASSERT_VALID( this );

	if( _IsMdiApp() )
	{
		if( !IsOleIpObjActive() )
			if( _InstallMdiDocButtons( FALSE ) )
				bDoRecalcLayout = TRUE;
		VERIFY( _SyncActiveMdiChild() );
	}
	
	if( bDoRecalcLayout )
	{
		Invalidate();
		_RecalcLayoutImpl();
		UpdateWindow();
		if( m_pDockSite != NULL )
		{
			CFrameWnd * pFrame = GetParentFrame();
			ASSERT_VALID( pFrame );
			if( pFrame->IsKindOf(RUNTIME_CLASS(CExtMiniDockFrameWnd)) )
				pFrame->SetWindowPos(
					NULL, 0, 0, 0, 0,
					SWP_NOACTIVATE|SWP_NOMOVE|SWP_NOSIZE
						|SWP_NOZORDER|SWP_NOOWNERZORDER
						|SWP_FRAMECHANGED
					);
		} // if( m_pDockSite != NULL )
	} // if( bDoRecalcLayout )
	return TRUE;
}

BOOL CExtRibbonPage::_InstallMdiDocButtons(
	BOOL bDoRecalcLayout // = TRUE
	)
{
	ASSERT( _IsMdiApp() );
	if( ! IsDisplayMdiDocumentButtons() )
	{
		if( bDoRecalcLayout )
			_RecalcLayoutImpl();
		return TRUE;
	}
	if( _DraggingGetBar() != NULL )
		return FALSE;
BOOL bMax = FALSE;
HWND hWndActiveChild =
		_GetActiveMdiChildWnd( bMax );
INT nCountOfButtons = RibbonRightButton_GetCount();
	if(		nCountOfButtons == 0
		&&	hWndActiveChild == NULL
		)
		return FALSE;
BOOL bRetVal = FALSE;
CExtBarMdiRightButton * pRightTBB =
		DYNAMIC_DOWNCAST(
			CExtBarMdiRightButton,
			RibbonRightButton_GetAt( nCountOfButtons - 1 )
			);
	if( hWndActiveChild == NULL || (!bMax) )
	{ // if no active MDI doc or not maximized
		if( pRightTBB != NULL )
		{
			ASSERT_VALID( pRightTBB );
			pRightTBB->ModifyStyle( TBBS_HIDDEN );
			pRightTBB->Show( false );
			bRetVal = TRUE;
		}
	} // if no active MDI doc or not maximized
	else
	{ // if active MDI doc is maximized
		ASSERT( hWndActiveChild != NULL );
		ASSERT( ::IsWindow(hWndActiveChild) );
		if( pRightTBB != NULL )
		{
			ASSERT_VALID( pRightTBB );
			pRightTBB->ModifyStyle( 0, TBBS_HIDDEN );
			pRightTBB->Show( true );
			bRetVal = TRUE;
		}
	} // if active MDI doc is maximized

	if( pRightTBB != NULL )
	{
		ASSERT_VALID( pRightTBB );
		pRightTBB->_ActiveMdiChildWndAttach( hWndActiveChild );
	}
	if( bDoRecalcLayout )
	{
		_RecalcLayoutImpl();
		bRetVal = FALSE;
	}
	return bRetVal;
}

void CExtRibbonPage::_CloseCustomMenus()
{
	ASSERT_VALID( this );
	if( (! m_bHelperPopupMode) || m_bHelperAutoHideMode )
	{
		if(		m_pPopupPageMenuGroup->GetSafeHwnd()
			&&	(! m_pPopupPageMenuGroup->m_bHelperAnimationControllerDetected )
			)
			m_pPopupPageMenuGroup->DestroyWindow();
	}
	if(		m_pPopupPageMenuAutoHide->GetSafeHwnd()
		&&	(! m_pPopupPageMenuAutoHide->m_bHelperAnimationControllerDetected )
		&&	m_pPopupPageMenuAutoHide->m_wndRibbonPage.m_pPopupPageMenuGroup->GetSafeHwnd() == NULL
		)
		m_pPopupPageMenuAutoHide->DestroyWindow();
}

bool CExtRibbonPage::_GetFullRowMode() const
{
	ASSERT_VALID( this );
	return true;
}

void CExtRibbonPage::_CalcInsideRect(
	CRect & rect,
	BOOL bHorz
	) const
{
	ASSERT_VALID( this );
	CExtMenuControlBar::_CalcInsideRect( rect, bHorz );
}

bool CExtRibbonPage::RibbonLayout_IsDwmCaptionIntegration() const
{
	ASSERT_VALID( this );
	return false;
}

CSize CExtRibbonPage::_CalcLayout(
	DWORD dwMode,
	int nLength // = -1
	)
{
	ASSERT_VALID( this );
	dwMode;
	if( nLength < 0 )
	{
		CRect rcParentClient;
		GetParent()->GetClientRect( &rcParentClient );
		nLength = rcParentClient.Width();
	} // if( nLength < 0 )
INT nGroupContentHeight = RibbonLayout_GetGroupHeight( NULL );
INT nGroupCatpionHeight = RibbonLayout_GroupCaptionGetHeight( NULL );
INT nPageHeight = nGroupContentHeight + nGroupCatpionHeight;

INT nTabLineHeight = RibbonLayout_GetTabLineHeight();
INT nTopBorderHeight = 0;
INT nFrameCaptionHeight = RibbonLayout_GetFrameCaptionHeight( &nTopBorderHeight );
INT nHeightAtTheTop = nTabLineHeight + nFrameCaptionHeight; // + nTopBorderHeight;

INT nBottomLineHeight = RibbonLayout_GetBottomLineHeight();
INT nHeight = nPageHeight + nHeightAtTheTop + nBottomLineHeight;

	if(		RibbonLayout_IsFrameIntegrationEnabled()
		&&	(! RibbonLayout_IsDwmCaptionIntegration() )
		)
		nHeight += nFrameCaptionHeight + nTopBorderHeight;
	return CSize( nLength, nHeight );
}

bool CExtRibbonPage::_AdjustBGInfo()
{
	ASSERT_VALID( this );
	m_cxLeftBorder		= 0;
	m_cxRightBorder		= 0;
	m_cyTopBorder		= 0;
	m_cyBottomBorder	= 0;
	m_nGripWidthAtLeft	= 0;
	m_nGripHeightAtTop	= 0;
	return true;
}

void CExtRibbonPage::Ribbon_OnRecalcLayout()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	_RecalcPositionsImpl();
CFrameWnd * pFrameWnd = GetParentFrame();
	if( pFrameWnd != NULL )
		pFrameWnd->RecalcLayout();
	else
		GetParent()->RepositionBars( 0, 0xFFFF, 0 );
	Invalidate();
// 	CExtMenuControlBar::PmBridge_OnPaintManagerChanged( PmBridge_GetPM() );
// 	CExtCustomizeSite::PmBridge_OnPaintManagerChanged( PmBridge_GetPM() );
}

INT CExtRibbonPage::RibbonRightButton_GetIndexOf( const CExtBarButton * pTBB ) const
{
	ASSERT_VALID( this );
	if( pTBB == NULL )
		return -1;
	ASSERT_VALID( pTBB );
INT nIndex, nCount = RibbonRightButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		if( LPCVOID(RibbonRightButton_GetAt(nIndex)) == LPCVOID(pTBB) )
			return nIndex;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return -1;
}

CExtBarButton * CExtRibbonPage::RibbonRightButton_GetAt( INT nPos )
{
	ASSERT_VALID( this );
	if( nPos < 0 )
		return NULL;
INT nCount = RibbonRightButton_GetCount();
	if( nPos >= nCount )
		return NULL;
CExtBarButton * pTBB = m_arrRightButtons.GetAt( nPos );
	ASSERT_VALID( pTBB );
	ASSERT( pTBB->GetBar() == this );
	return pTBB;
}

const CExtBarButton * CExtRibbonPage::RibbonRightButton_GetAt( INT nPos ) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtRibbonPage * > ( this ) )
		-> RibbonRightButton_GetAt( nPos );
}

INT CExtRibbonPage::RibbonRightButton_GetCount() const
{
	ASSERT_VALID( this );
INT nCount = INT( m_arrRightButtons.GetSize() );
	return nCount;
}

bool CExtRibbonPage::RibbonPage_ExpandedModeGet() const
{
	ASSERT_VALID( this );
	return m_bRibbonPageIsExpandedMode;
}

void CExtRibbonPage::RibbonPage_ExpandedModeSet(
	bool bRibbonPageIsExpandedMode,
	bool bRecalcLayout
	)
{
	ASSERT_VALID( this );
	if( m_bRibbonPageIsExpandedMode == bRibbonPageIsExpandedMode )
		return;
	_RibbonPageRslaResetStateData();
	m_bRibbonPageIsExpandedMode = bRibbonPageIsExpandedMode;
INT nIndex, nCount = RibbonGroupButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtRibbonButtonGroup * pGroupTBB = RibbonGroupButton_GetAt( nIndex );
		ASSERT_VALID( pGroupTBB );
		ASSERT( pGroupTBB->ParentButtonGet() == NULL );
		if( ! m_bRibbonPageIsExpandedMode )
		{
			pGroupTBB->Show( false );
			continue;
		}
		if( ( pGroupTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
			continue;
		pGroupTBB->Show( m_bRibbonPageIsExpandedMode );
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	if( m_bRibbonPageIsExpandedMode )
	{
		if( m_pPopupPageMenuAutoHide->GetSafeHwnd() != NULL )
			m_pPopupPageMenuAutoHide->DestroyWindow();
	}
	if( bRecalcLayout )
		Ribbon_OnRecalcLayout();
}

void CExtRibbonPage::_OnRibbonPageExpandedModeSet( bool bRibbonPageIsExpandedMode )
{
	ASSERT_VALID( this );
	RibbonPage_ExpandedModeSet( bRibbonPageIsExpandedMode, true );
}

bool CExtRibbonPage::RibbonQuickAccessBar_AboveTheRibbonGet() const
{
	ASSERT_VALID( this );
	return m_bRibbonQuickAccessBarIsAboveTheRibbon;
}

void CExtRibbonPage::RibbonQuickAccessBar_AboveTheRibbonSet(
	bool bRibbonQuickAccessBarIsAboveTheRibbon,
	bool bRecalcLayout
	)
{
	ASSERT_VALID( this );
	if( m_bRibbonQuickAccessBarIsAboveTheRibbon == bRibbonQuickAccessBarIsAboveTheRibbon )
		return;
	m_bRibbonQuickAccessBarIsAboveTheRibbon = bRibbonQuickAccessBarIsAboveTheRibbon;
	_RibbonPageRslaResetStateData();
	if( bRecalcLayout )
		Ribbon_OnRecalcLayout();
}

INT CExtRibbonPage::RibbonQuickAccessButton_GetIndexOf(
	const CExtBarButton * pTBB,
	bool bVisibleOnly // = false
	) const
{
	ASSERT_VALID( this );
	if( pTBB == NULL )
		return -1;
	ASSERT_VALID( pTBB );
INT nIndex, nCount = RibbonQuickAccessButton_GetCount( bVisibleOnly );
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		if( LPCVOID( RibbonQuickAccessButton_GetAt( nIndex, bVisibleOnly ) ) == LPCVOID( pTBB ) )
			return nIndex;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return -1;
}

CExtBarButton * CExtRibbonPage::RibbonQuickAccessButton_GetAt(
	INT nPos,
	bool bVisibleOnly // = false
	)
{
	ASSERT_VALID( this );
	if( nPos < 0 )
		return NULL;
INT nCount = RibbonQuickAccessButton_GetCount( bVisibleOnly );
	if( nPos >= nCount )
		return NULL;
	if( bVisibleOnly )
	{
		INT nWalk = 0, nIndex, nCount = INT( m_arrQuickAccessButtons.GetSize() );
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtBarButton * pTBB = m_arrQuickAccessButtons.GetAt( nPos );
			ASSERT_VALID( pTBB );
			ASSERT( pTBB->GetBar() == this );
			if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				continue;
			if( nWalk == nPos )
				return pTBB;
			nWalk ++;
		}
		return NULL;
	} // if( bVisibleOnly )
	else
	{
		CExtBarButton * pTBB = m_arrQuickAccessButtons.GetAt( nPos );
		ASSERT_VALID( pTBB );
		ASSERT( pTBB->GetBar() == this );
		return pTBB;
	} // else if( bVisibleOnly )
}

const CExtBarButton * CExtRibbonPage::RibbonQuickAccessButton_GetAt(
	INT nPos,
	bool bVisibleOnly // = false
	) const
{
	ASSERT_VALID( this );
	return ( const_cast < CExtRibbonPage * > ( this ) ) -> RibbonQuickAccessButton_GetAt( nPos, bVisibleOnly );
}

INT CExtRibbonPage::RibbonQuickAccessButton_GetCount(
	bool bVisibleOnly // = false
	) const
{
	ASSERT_VALID( this );
INT nCount = INT( m_arrQuickAccessButtons.GetSize() );
INT nRetVal = nCount;
	if( bVisibleOnly )
	{
		INT nIndex;
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			const CExtBarButton * pTBB = m_arrQuickAccessButtons.GetAt( nIndex );
			ASSERT_VALID( pTBB );
			ASSERT( pTBB->GetBar() == this );
			if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				nRetVal --;
		}
	} // if( bVisibleOnly )
	return nRetVal;
}

INT CExtRibbonPage::RibbonTabPageButton_GetIndexOf( const CExtRibbonButtonTabPage * pTBB ) const
{
	ASSERT_VALID( this );
	if( pTBB == NULL )
		return -1;
	ASSERT_VALID( pTBB );
INT nIndex, nCount = RibbonTabPageButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		if( LPCVOID(RibbonTabPageButton_GetAt(nIndex)) == LPCVOID(pTBB) )
			return nIndex;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return -1;
}

CExtRibbonButtonTabPage * CExtRibbonPage::RibbonTabPageButton_GetAt( INT nPos )
{
	ASSERT_VALID( this );
	if( nPos < 0 )
		return NULL;
INT nCount = RibbonTabPageButton_GetCount();
	if( nPos >= nCount )
		return NULL;
CExtRibbonButtonTabPage * pTBB = m_arrTabPageButtons.GetAt( nPos );
	ASSERT_VALID( pTBB );
	ASSERT( pTBB->GetBar() == this );
	return pTBB;
}

const CExtRibbonButtonTabPage * CExtRibbonPage::RibbonTabPageButton_GetAt( INT nPos ) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtRibbonPage * > ( this ) )
		-> RibbonTabPageButton_GetAt( nPos );
}

INT CExtRibbonPage::RibbonTabPageButton_GetCount() const
{
	ASSERT_VALID( this );
INT nCount = INT( m_arrTabPageButtons.GetSize() );
	return nCount;
}

INT CExtRibbonPage::RibbonGroupButton_GetIndexOf( const CExtRibbonButtonGroup * pTBB ) const
{
	ASSERT_VALID( this );
	if( pTBB == NULL )
		return -1;
	ASSERT_VALID( pTBB );
INT nIndex, nCount = RibbonGroupButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		if( LPCVOID(RibbonGroupButton_GetAt(nIndex)) == LPCVOID(pTBB) )
			return nIndex;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return -1;
}

CExtRibbonButtonGroup * CExtRibbonPage::RibbonGroupButton_GetAt( INT nPos )
{
	ASSERT_VALID( this );
	if( nPos < 0 )
		return NULL;
INT nCount = RibbonGroupButton_GetCount();
	if( nPos >= nCount )
		return NULL;
CExtRibbonButtonGroup * pTBB = m_arrGroupButtons.GetAt( nPos );
	ASSERT_VALID( pTBB );
	ASSERT( pTBB->GetBar() == this );
	return pTBB;
}

const CExtRibbonButtonGroup * CExtRibbonPage::RibbonGroupButton_GetAt( INT nPos ) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtRibbonPage * > ( this ) )
		-> RibbonGroupButton_GetAt( nPos );
}

INT CExtRibbonPage::RibbonGroupButton_GetCount() const
{
	ASSERT_VALID( this );
INT nCount = INT( m_arrGroupButtons.GetSize() );
	return nCount;
}

void CExtRibbonPage::DoPaintNC( CDC * pDC )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC ); 
	ASSERT( pDC->GetSafeHdc() != NULL );
	CExtMenuControlBar::DoPaintNC( pDC );
}

void CExtRibbonPage::DoEraseBk( CDC * pDC )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC ); 
	ASSERT( pDC->GetSafeHdc() != NULL );
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
	if( ! m_bHelperDwmPaintingMode )
		CExtMenuControlBar::DoEraseBk( pDC );
	else
	{
		CRect rcNonDwmClient;
		GetClientRect( &rcNonDwmClient );
		rcNonDwmClient.top += RibbonLayout_GetFrameCaptionHeight( NULL );
		if( pPM->Ribbon_DwmAreaCoversTabs() )
			rcNonDwmClient.top += RibbonLayout_GetTabLineHeight();
		if( ! rcNonDwmClient.IsRectEmpty() )
		{
			CExtMemoryDC dc( pDC, &rcNonDwmClient, CExtMemoryDC::MDCOPT_TO_MEMORY | CExtMemoryDC::MDCOPT_FORCE_DIB );
			//dc.FillSolidRect( &rcNonDwmClient, RGB(255,255,255) );
			CExtMenuControlBar::DoEraseBk( &dc );
		}
	}
INT nTabLineHeight = RibbonLayout_GetTabLineHeight();
INT nTopBorderHeight = 0;
INT nFrameCaptionHeight = RibbonLayout_GetFrameCaptionHeight( &nTopBorderHeight );
INT nHeightAtTheTop = nTabLineHeight + nFrameCaptionHeight;
INT nBottomLineHeight = RibbonLayout_GetBottomLineHeight();
CRect rcClient;
	GetClientRect( &rcClient );
CRect rcPageBk = rcClient;
	rcPageBk.top += nHeightAtTheTop;
	rcPageBk.bottom -= nBottomLineHeight;
	if(		RibbonLayout_IsFrameIntegrationEnabled()
		&&	(! RibbonLayout_IsDwmCaptionIntegration() )
		)
		rcPageBk.top += nFrameCaptionHeight + nTopBorderHeight;
	if(		pDC->RectVisible( &rcPageBk )
		&&	RibbonPage_ExpandedModeGet()
		)
		pPM->Ribbon_PaintPageBk( *pDC, rcPageBk, this );
CRect rcTabLine(
		rcPageBk.left,
		rcPageBk.top - nTabLineHeight,
		rcPageBk.right,
		rcPageBk.top
		);
	RibbonLayout_AdjustTabLineRect( rcTabLine );
CRect rcTabNearBorderArea = rcTabLine;
	rcTabNearBorderArea.top = rcTabNearBorderArea.bottom;
	rcTabNearBorderArea.bottom += RibbonLayout_GetTabIntersectionHeight();
	if(		pDC->RectVisible( &rcClient )
		||	pDC->RectVisible( &rcTabLine )
		||	pDC->RectVisible( &rcTabNearBorderArea )
		)
		pPM->Ribbon_PaintTabClientArea(
			*pDC,
			rcClient,
			rcTabLine,
			rcTabNearBorderArea,
			this
			);
	if(		(! RibbonQuickAccessBar_AboveTheRibbonGet() )
		&&	nBottomLineHeight > 0
		)
	{
		CRect _rcBottomContent = rcClient;
		_rcBottomContent.top = _rcBottomContent.bottom - nBottomLineHeight;
		if( pDC->RectVisible( &_rcBottomContent ) )
			pPM->RibbonQuickAccessBar_PaintBkAtTheBottom( *pDC, _rcBottomContent, this );
	}
//	{
//		CRect rc = rcPageBk;
//		pDC->Rectangle( rc );
//		pDC->MoveTo( rc.left, rc.top );
//		pDC->LineTo( rc.right, rc.bottom );
//		pDC->MoveTo( rc.right, rc.top );
//		pDC->LineTo( rc.left, rc.bottom );
//	}
//	{
//		CRect rc = rcTabLine;
//		pDC->MoveTo( rc.left, rc.top );
//		pDC->LineTo( rc.right, rc.bottom );
//		pDC->MoveTo( rc.right, rc.top );
//		pDC->LineTo( rc.left, rc.bottom );
//	}
}

void CExtRibbonPage::DoPaint( CDC * pDC )
{
	__PROF_UIS_MANAGE_STATE;
	ASSERT_VALID( this );
	ASSERT_VALID( pDC ); 
	ASSERT( pDC->GetSafeHdc() != NULL );
	CExtPaintManager::stat_ExcludeChildAreas( *pDC, *this );
CRect rcClient;
	GetClientRect( &rcClient );
CExtMemoryDC dc(
		pDC,
		m_bHelperDwmPaintingMode ? (&rcClient) : NULL,
		m_bHelperDwmPaintingMode
			? ( CExtMemoryDC::MDCOPT_TO_MEMORY | CExtMemoryDC::MDCOPT_FORCE_DIB )
			: ( CExtMemoryDC::MDCOPT_DEFAULT )
		);
 	if( dc.GetSafeHdc() != NULL )
	{
 		pDC = &dc;
		if( m_bHelperDwmPaintingMode )
			pDC->FillSolidRect( &rcClient, RGB(0,0,0) );
	}
	DoEraseBk( pDC );

	pDC->SetTextColor(
		PmBridge_GetPM()->GetColor( CExtPaintManager::CLR_TEXT_OUT, this )
		);
	pDC->SetBkMode( TRANSPARENT );
CFont * pFont =
		OnGetToolbarFont(
			( ( ( m_dwStyle & CBRS_ORIENT_HORZ ) == 0 ) && (!m_bPaletteMode) )
				? true : false,
			false,
			this
			);
	if( pFont->GetSafeHandle() == NULL )
		pFont = (&( PmBridge_GetPM()->m_FontNormal ));
CFont * pOldFont =
		pDC->SelectObject( pFont );

INT nIndex, nCount;
	nCount = RibbonGroupButton_GetCount();
	if( RibbonPage_ExpandedModeGet() )
	{
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtRibbonButtonGroup * pTBB = RibbonGroupButton_GetAt( nIndex );
			ASSERT_VALID( pTBB );
			if( ! pTBB->IsVisible() )
				continue;
			if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				continue;
			pTBB->PaintCompound( *pDC, false, true, false );
		}
	} // if( RibbonPage_ExpandedModeGet() )

INT nPageSelIdx = Ribbon_PageSelectionGet();
	nCount = RibbonTabPageButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		INT nEffectiveIndex = nIndex;
		if( nPageSelIdx >= 0 )
		{
			if( nIndex == (nCount-1) )
				nEffectiveIndex = nPageSelIdx;
			else if( nIndex >= nPageSelIdx )
				nEffectiveIndex ++;
		}
		CExtRibbonButtonTabPage * pTBB = RibbonTabPageButton_GetAt( nEffectiveIndex );
		ASSERT_VALID( pTBB );
		if( ! pTBB->IsVisible() )
			continue;
		if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
			continue;
		pTBB->PaintCompound( *pDC, false, true, false );
	}

	nCount = RibbonRightButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtBarButton * pTBB = RibbonRightButton_GetAt( nIndex );
		ASSERT_VALID( pTBB );
		if( ! pTBB->IsVisible() )
			continue;
		if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
			continue;
		pTBB->PaintCompound( *pDC, false, true, false );
	}

	if( ! RibbonLayout_IsFrameIntegrationEnabled() )
	{
		nCount = RibbonQuickAccessButton_GetCount();
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtBarButton * pTBB = RibbonQuickAccessButton_GetAt( nIndex );
			ASSERT_VALID( pTBB );
			if( ! pTBB->IsVisible() )
				continue;
			if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				continue;
			pTBB->PaintCompound( *pDC, false, true, false );
		}
	} // if( ! RibbonLayout_IsFrameIntegrationEnabled() )

	pDC->SelectObject( pOldFont );
	PmBridge_GetPM()->OnPaintSessionComplete( this );
}

CFont * CExtRibbonPage::OnGetToolbarFont(
	bool bVert,
	bool bMeasureFont,
	CObject * pQuerySrc
	)
{
	ASSERT_VALID(this);
	bVert;
	bMeasureFont;
	pQuerySrc;
	CFont & _font = PmBridge_GetPM()->m_FontRibbonNormal;
	ASSERT( _font.GetSafeHandle() != NULL );
	return (&_font);
}

bool CExtRibbonPage::_OnMouseMoveMsg( UINT nFlags, CPoint point )
{
	ASSERT_VALID( this );
	if( m_bFlatTracking )
		return true;
	return CExtMenuControlBar::_OnMouseMoveMsg( nFlags, point );
}

bool CExtRibbonPage::OnRibbonProcessMouseWheel( UINT fFlags, short zDelta, CPoint point )
{
	ASSERT_VALID( this );
	fFlags;
	zDelta;
	point;
	return false;
}

void CExtRibbonPage::OnRibbonGalleryInitContent(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup,
	CExtRibbonButtonGallery * pRibbonGalleryTBB
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT_VALID( (&wndRG) );
	ASSERT( wndRG.GetSafeHwnd() != NULL );
#ifdef _DEBUG
	if( pGalleryPopup != NULL )
	{
		ASSERT_VALID( pGalleryPopup );
		ASSERT( pGalleryPopup->GetSafeHwnd() != NULL );
	}
	if( pRibbonGalleryTBB != NULL )
	{
		ASSERT_VALID( pRibbonGalleryTBB );
	}
#endif // _DEBUG
	if( m_bHelperPopupMode )
	{
		CWnd * pWnd = GetParent();
		if( pWnd->GetSafeHwnd() == NULL )
			return;
		CExtRibbonPopupMenuWnd * pPopup =
			DYNAMIC_DOWNCAST( CExtRibbonPopupMenuWnd, pWnd );
		if( pPopup == NULL )
			return;
		CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
		if( pRibbonPage == NULL )
			return;
		pRibbonPage->OnRibbonGalleryInitContent(
			wndRG,
			pGalleryPopup,
			pRibbonGalleryTBB
			);
		return;
	} // if( m_bHelperPopupMode )
}

void CExtRibbonPage::OnRibbonGalleryItemSelEndOK(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup,
	CExtRibbonButtonGallery * pRibbonGalleryTBB,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI
	)
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( pGalleryPopup != NULL )
	{
		ASSERT_VALID( pGalleryPopup );
		ASSERT( pGalleryPopup->GetSafeHwnd() != NULL );
	}
	if( pRibbonGalleryTBB != NULL )
	{
		ASSERT_VALID( pRibbonGalleryTBB );
	}
#endif // _DEBUG
	if( m_bHelperPopupMode )
	{
		CWnd * pWnd = GetParent();
		if( pWnd->GetSafeHwnd() == NULL )
			return;
		CExtRibbonPopupMenuWnd * pPopup =
			DYNAMIC_DOWNCAST( CExtRibbonPopupMenuWnd, pWnd );
		if( pPopup == NULL )
			return;
		CExtRibbonPage * pRibbonPage = pPopup->GetMainRibbonPage();
		HWND hWndPopup = pRibbonPage->GetSafeHwnd();
		if( hWndPopup == NULL )
			return;
		pRibbonPage->OnRibbonGalleryItemSelEndOK(
			wndRG,
			pGalleryPopup,
			pRibbonGalleryTBB,
			pTBCI
			);
		if( ::IsWindow( hWndPopup ) )
			pPopup->DestroyWindow();
		CExtToolControlBar::_CloseTrackingMenus();
		CExtPopupMenuWnd::CancelMenuTracking();
		CExtToolControlBar::_CloseCustomMenusAll();
		return;
	} // if( m_bHelperPopupMode )
	if(		pGalleryPopup != NULL
		&&	pGalleryPopup->m_bHelperKeyboardProcessing
		)
		return;
CWnd * pWnd = wndRG.GetParent();
	if(		pWnd->GetSafeHwnd() != NULL
		&&	pWnd->IsKindOf( RUNTIME_CLASS( CExtPopupMenuWnd ) )
		)
		pWnd->DestroyWindow();
	CExtToolControlBar::_CloseTrackingMenus();
	CExtPopupMenuWnd::CancelMenuTracking();
	CExtToolControlBar::_CloseCustomMenusAll();
}

void CExtRibbonPage::OnRibbonGalleryItemInsert(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup,
	CExtRibbonButtonGallery * pRibbonGalleryTBB,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI
	)
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( pGalleryPopup != NULL )
	{
		ASSERT_VALID( pGalleryPopup );
		ASSERT( pGalleryPopup->GetSafeHwnd() != NULL );
	}
	if( pRibbonGalleryTBB != NULL )
	{
		ASSERT_VALID( pRibbonGalleryTBB );
	}
#endif // _DEBUG
	if( m_bHelperPopupMode )
	{
		CWnd * pWnd = GetParent();
		if( pWnd->GetSafeHwnd() == NULL )
			return;
		CExtRibbonPopupMenuWnd * pPopup =
			DYNAMIC_DOWNCAST( CExtRibbonPopupMenuWnd, pWnd );
		if( pPopup == NULL )
			return;
		CExtRibbonPage * pRibbonPage = pPopup->GetMainRibbonPage();
		if( pRibbonPage == NULL )
			return;
		pRibbonPage->OnRibbonGalleryItemInsert(
			wndRG,
			pGalleryPopup,
			pRibbonGalleryTBB,
			pTBCI
			);
		return;
	} // if( m_bHelperPopupMode )
}

void CExtRibbonPage::OnRibbonGalleryItemRemove(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup,
	CExtRibbonButtonGallery * pRibbonGalleryTBB,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI
	)
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( pGalleryPopup != NULL )
	{
		ASSERT_VALID( pGalleryPopup );
		ASSERT( pGalleryPopup->GetSafeHwnd() != NULL );
	}
	if( pRibbonGalleryTBB != NULL )
	{
		ASSERT_VALID( pRibbonGalleryTBB );
	}
#endif // _DEBUG
	if( m_bHelperPopupMode )
	{
		CWnd * pWnd = GetParent();
		if( pWnd->GetSafeHwnd() == NULL )
			return;
		CExtRibbonPopupMenuWnd * pPopup =
			DYNAMIC_DOWNCAST( CExtRibbonPopupMenuWnd, pWnd );
		if( pPopup == NULL )
			return;
		CExtRibbonPage * pRibbonPage = pPopup->GetMainRibbonPage();
		if( pRibbonPage == NULL )
			return;
		pRibbonPage->OnRibbonGalleryItemRemove(
			wndRG,
			pGalleryPopup,
			pRibbonGalleryTBB,
			pTBCI
			);
		return;
	} // if( m_bHelperPopupMode )
}

void CExtRibbonPage::OnRibbonGalleryItemActivate(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup,
	CExtRibbonButtonGallery * pRibbonGalleryTBB,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_Old,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_New
	)
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( pGalleryPopup != NULL )
	{
		ASSERT_VALID( pGalleryPopup );
		ASSERT( pGalleryPopup->GetSafeHwnd() != NULL );
	}
	if( pRibbonGalleryTBB != NULL )
	{
		ASSERT_VALID( pRibbonGalleryTBB );
	}
#endif // _DEBUG
	if( m_bHelperPopupMode )
	{
		CWnd * pWnd = GetParent();
		if( pWnd->GetSafeHwnd() == NULL )
			return;
		CExtRibbonPopupMenuWnd * pPopup =
			DYNAMIC_DOWNCAST( CExtRibbonPopupMenuWnd, pWnd );
		if( pPopup == NULL )
			return;
		CExtRibbonPage * pRibbonPage = pPopup->GetMainRibbonPage();
		if( pRibbonPage == NULL )
			return;
		pRibbonPage->OnRibbonGalleryItemActivate(
			wndRG,
			pGalleryPopup,
			pRibbonGalleryTBB,
			pTBCI_Old,
			pTBCI_New
			);
		return;
	} // if( m_bHelperPopupMode )
}

void CExtRibbonPage::OnRibbonGalleryItemSelChange(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup,
	CExtRibbonButtonGallery * pRibbonGalleryTBB,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_Old,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_New
	)
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( pGalleryPopup != NULL )
	{
		ASSERT_VALID( pGalleryPopup );
		ASSERT( pGalleryPopup->GetSafeHwnd() != NULL );
	}
	if( pRibbonGalleryTBB != NULL )
	{
		ASSERT_VALID( pRibbonGalleryTBB );
	}
#endif // _DEBUG
	if( m_bHelperPopupMode )
	{
		CWnd * pWnd = GetParent();
		if( pWnd->GetSafeHwnd() == NULL )
			return;
		CExtRibbonPopupMenuWnd * pPopup =
			DYNAMIC_DOWNCAST( CExtRibbonPopupMenuWnd, pWnd );
		if( pPopup == NULL )
			return;
		CExtRibbonPage * pRibbonPage = pPopup->GetMainRibbonPage();
		if( pRibbonPage == NULL )
			return;
		pRibbonPage->OnRibbonGalleryItemSelChange(
			wndRG,
			pGalleryPopup,
			pRibbonGalleryTBB,
			pTBCI_Old,
			pTBCI_New
			);
		return;
	} // if( m_bHelperPopupMode )
}

void CExtRibbonPage::OnRibbonGalleryItemHoverChange(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup,
	CExtRibbonButtonGallery * pRibbonGalleryTBB,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_Old,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_New
	)
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( pGalleryPopup != NULL )
	{
		ASSERT_VALID( pGalleryPopup );
		ASSERT( pGalleryPopup->GetSafeHwnd() != NULL );
	}
	if( pRibbonGalleryTBB != NULL )
	{
		ASSERT_VALID( pRibbonGalleryTBB );
	}
#endif // _DEBUG
	if( m_bHelperPopupMode )
	{
		CWnd * pWnd = GetParent();
		if( pWnd->GetSafeHwnd() == NULL )
			return;
		CExtRibbonPopupMenuWnd * pPopup =
			DYNAMIC_DOWNCAST( CExtRibbonPopupMenuWnd, pWnd );
		if( pPopup == NULL )
			return;
		CExtRibbonPage * pRibbonPage = pPopup->GetMainRibbonPage();
		if( pRibbonPage == NULL )
			return;
		pRibbonPage->OnRibbonGalleryItemHoverChange(
			wndRG,
			pGalleryPopup,
			pRibbonGalleryTBB,
			pTBCI_Old,
			pTBCI_New
			);
		return;
	} // if( m_bHelperPopupMode )
}

bool CExtRibbonPage::_IsSimplifiedDropDownButtons() const
{
	ASSERT_VALID( this );
	return false;
}

void CExtRibbonPage::_RedrawOnPosChanged()
{
	ASSERT_VALID( this );
}

void CExtRibbonPage::_ActivateOnClick()
{
	ASSERT_VALID( this );
	if( m_bHelperPopupMode )
		return;
HWND hWndOwn = GetSafeHwnd();
	if(		hWndOwn == NULL
		||	(! ::IsWindow( hWndOwn ) )
		)
		return;
	CExtMenuControlBar::_ActivateOnClick();
}

BOOL CExtRibbonPage::TranslateMainFrameMessage(MSG* pMsg)
{
	__PROF_UIS_MANAGE_STATE;
	if( GetSafeHwnd() == NULL )
		return FALSE;
	ASSERT( pMsg != NULL );
	if(		pMsg->message == WM_MOUSEWHEEL
		&&	OnQueryBarHoverProcessingEnabled()
		)
	{
		CPoint point = pMsg->lParam;
		HWND hWnd = ::WindowFromPoint( point );
		if(		hWnd != NULL
			&&	(	hWnd == m_hWnd
				||	hWnd == ::GetParent( m_hWnd )
				)
			)
		{
			UINT fFlags = LOWORD(pMsg->wParam);
			short zDelta = HIWORD(pMsg->wParam);
			if( OnRibbonProcessMouseWheel( fFlags, zDelta, point ) )
				return TRUE;
		}
	}
	return CExtMenuControlBar::TranslateMainFrameMessage( pMsg );
}

INT CExtRibbonPage::OnCalcFlatTrackingIndex(
	/*__EXT_MFC_SAFE_TCHAR*/ INT vkTCHAR,
	INT nStartSearchIndex
	)
{
	ASSERT_VALID( this );
	if(		(! m_bHelperFlatTrackingCalcEnabledLeftRight )
		&&	(! m_bHelperFlatTrackingCalcEnabledUpDown )
		)
		return -1;
	if(		vkTCHAR != VK_LEFT
		&&	vkTCHAR != VK_RIGHT
		&&	vkTCHAR != VK_UP
		&&	vkTCHAR != VK_DOWN
		&&	vkTCHAR != VK_TAB
		)
		return -1;
INT nFlatTrackingIndex =
		CExtMenuControlBar::OnCalcFlatTrackingIndex(
			vkTCHAR,
			nStartSearchIndex
			);
	return nFlatTrackingIndex;
}

bool CExtRibbonPage::OnCalcFlatTrackingIndexCheckPass(
	/*__EXT_MFC_SAFE_TCHAR*/ INT vkTCHAR,
	INT nPassIndex,
	const CExtBarButton * pPrevTBB,
	const CExtBarButton * pNextTBB
	) const
{
	ASSERT_VALID( this );
/*
	if(		pPrevTBB != NULL
		&&	pNextTBB != NULL
		&&	( vkTCHAR == VK_LEFT || vkTCHAR == VK_RIGHT )
		)
	{
		ASSERT_VALID( pPrevTBB );
		ASSERT_VALID( pNextTBB );
		const CExtBarButton * pParentPrevTBB = pPrevTBB->ParentButtonGet();
		const CExtBarButton * pParentNextTBB = pNextTBB->ParentButtonGet();
		if(		pParentPrevTBB != NULL
			&&	pParentNextTBB != NULL
			&&	pParentPrevTBB->ParentButtonGet() == NULL
			&&	pParentNextTBB->ParentButtonGet() == NULL
			&&	pParentPrevTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonGroup ) )
			&&	pParentNextTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonGroup ) )
			&&	(! pParentPrevTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonToolGroup ) ) )
			&&	(! pParentNextTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonToolGroup ) ) )
			)
		{
			ASSERT_VALID( pParentPrevTBB );
			ASSERT_VALID( pParentNextTBB );
			if( pParentPrevTBB == pParentNextTBB )
			{
				CRect rcPrevTBB = *pPrevTBB;
				CRect rcNextTBB = *pNextTBB;
				if(		rcNextTBB.top >= rcPrevTBB.bottom
					||	rcNextTBB.bottom <= rcPrevTBB.bottom
					)
					return false;
			}
		}
	}
*/
	return
		CExtMenuControlBar::OnCalcFlatTrackingIndexCheckPass(
			vkTCHAR,
			nPassIndex,
			pPrevTBB,
			pNextTBB
			);
}

bool CExtRibbonPage::OnCalcFlatTrackingTabOrder(
	CTypedPtrArray < CPtrArray, CExtBarButton * > & arrTabOrder
	)
{
	ASSERT_VALID( this );
	return CExtMenuControlBar::OnCalcFlatTrackingTabOrder( arrTabOrder );
}

bool CExtRibbonPage::OnCalcFlatTrackingIndexCheckIntersectionEnabled(
	CExtBarButton * pTBB,
	/*__EXT_MFC_SAFE_TCHAR*/ INT vkTCHAR
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
	if( pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButton ) ) )
	{
		INT nILV = pTBB->RibbonILV_Get();
		if( __EXT_RIBBON_ILV_SIMPLE_SMALL <= nILV && nILV <= __EXT_RIBBON_ILV_SIMPLE_NORMAL )
			return true;
	}
CExtBarButton * pParentTBB = pTBB->ParentButtonGet();
	if(		pParentTBB != NULL
		&&	pParentTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonToolGroup ) )
		)
		return true;
//	if( vkTCHAR == VK_LEFT || vkTCHAR == VK_RIGHT )
//	{
//		INT nILV = pTBB->RibbonILV_Get();
//		if( __EXT_RIBBON_ILV_SIMPLE_SMALL <= nILV && nILV <= __EXT_RIBBON_ILV_SIMPLE_NORMAL )
//			return false;
//	}
//	else if( vkTCHAR == VK_UP || vkTCHAR == VK_DOWN )
//	{
//		INT nILV = pTBB->RibbonILV_Get();
//		if( __EXT_RIBBON_ILV_SIMPLE_SMALL <= nILV && nILV <= __EXT_RIBBON_ILV_SIMPLE_NORMAL )
//			return false;
//	}
	return CExtMenuControlBar::OnCalcFlatTrackingIndexCheckIntersectionEnabled( pTBB, vkTCHAR );
}

bool CExtRibbonPage::SetupHookWndSink(
	HWND hWnd,
	bool bRemove, // = false
	bool bAddToHead // = false
	)
{
	ASSERT_VALID( this );
	if( m_bHelperPopupMode )
		return false;
	return
		CExtMenuControlBar::SetupHookWndSink(
			hWnd,
			bRemove,
			bAddToHead
			);
}

void CExtRibbonPage::OnSysColorChange() 
{
	CExtMenuControlBar::OnSysColorChange();
CExtCustomizeCmdTreeNode * pRibbonNode = Ribbon_GetRootNode();
	if( pRibbonNode != NULL )
	{
		ASSERT_VALID( pRibbonNode );
		CExtPaintManager * pPM = PmBridge_GetPM();
		ASSERT_VALID( pPM );
		pRibbonNode->OnSysColorChange( pPM, this );
	} // if( pRibbonNode != NULL )
}

void CExtRibbonPage::OnSettingChange(UINT uFlags, __EXT_MFC_SAFE_LPCTSTR lpszSection) 
{
	CExtMenuControlBar::OnSettingChange( uFlags, lpszSection );
CExtCustomizeCmdTreeNode * pRibbonNode = Ribbon_GetRootNode();
	if( pRibbonNode != NULL )
	{
		ASSERT_VALID( pRibbonNode );
		CExtPaintManager * pPM = PmBridge_GetPM();
		ASSERT_VALID( pPM );
		pRibbonNode->OnSettingChange( uFlags, lpszSection, pPM, this );
	} // if( pRibbonNode != NULL )
}

LRESULT CExtRibbonPage::OnThemeChanged( WPARAM wParam, LPARAM lParam )
{
LRESULT lResult = CExtMenuControlBar::OnThemeChanged( wParam, lParam );
CExtCustomizeCmdTreeNode * pRibbonNode = Ribbon_GetRootNode();
	if( pRibbonNode != NULL )
	{
		ASSERT_VALID( pRibbonNode );
		CExtPaintManager * pPM = PmBridge_GetPM();
		ASSERT_VALID( pPM );
		pRibbonNode->OnThemeChanged( wParam, lParam, pPM, this );
	} // if( pRibbonNode != NULL )
	return lResult;
}

LRESULT CExtRibbonPage::OnMenuClosed( WPARAM wParam, LPARAM lParam )
{
	wParam;
	lParam;
	_UpdateHoverButton( CPoint(-1,-1), false );
	if(		wParam != 0
		&&	m_bHelperPopupMode
		)
	{
//		GetParent()->PostMessage( WM_CLOSE );
		CExtRibbonPage * pRibbonPage = this;
		for(	;
				pRibbonPage != NULL;
				pRibbonPage = pRibbonPage->GetParentRibbonPage()
			)
		{
			if( ! pRibbonPage->m_bHelperPopupMode )
				break;
			pRibbonPage->GetParent()->PostMessage( WM_CLOSE );
		}
	}
	if( m_pPopupPageMenuAutoHide->GetSafeHwnd() != NULL )
	{
		if( m_pPopupPageMenuAutoHide->m_wndRibbonPage.m_pPopupPageMenuGroup->GetSafeHwnd() != NULL )
			m_pPopupPageMenuAutoHide->m_wndRibbonPage.m_pPopupPageMenuGroup->m_wndRibbonPage.OnMenuClosed( wParam, lParam );
		else
			m_pPopupPageMenuAutoHide->m_wndRibbonPage.OnMenuClosed( wParam, lParam );
	}
	if( m_pPopupPageMenuGroup->GetSafeHwnd() != NULL )
		m_pPopupPageMenuGroup->m_wndRibbonPage.OnMenuClosed( wParam, lParam );
	return 0L;
}

LRESULT CExtRibbonPage::_OnRibbonFileMenuButtonQuery( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	lParam;
CExtPopupMenuWnd::RIBBONFILEMENUBUTTONQUERY & _rfmbq =
		* CExtPopupMenuWnd::RIBBONFILEMENUBUTTONQUERY::FromWPARAM( wParam );
CExtCmdIcon _icon;
CExtSafeString sText;
	OnRibbonFileMenuButtonQuery( _rfmbq, _icon, sText );
	return 0L;
}

LRESULT CExtRibbonPage::_OnRibbonFileMenuButtonInvocation( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	lParam;
CExtPopupMenuWnd::RIBBONFILEMENUBUTTONINVOCATION & _rfmbi =
		* CExtPopupMenuWnd::RIBBONFILEMENUBUTTONINVOCATION::FromWPARAM( wParam );
	OnRibbonFileMenuButtonInvocation( _rfmbi );
	return 0L;
}

void CExtRibbonPage::OnRibbonFileMenuButtonQuery(
	CExtPopupMenuWnd::RIBBONFILEMENUBUTTONQUERY & _rfmbq,
	CExtCmdIcon & _icon,
	CExtSafeString & sText
	)
{
	ASSERT_VALID( this );
	_rfmbq;
	_icon;
	sText;
}

void CExtRibbonPage::OnRibbonFileMenuButtonInvocation(
	CExtPopupMenuWnd::RIBBONFILEMENUBUTTONINVOCATION & _rfmbi
	)
{
	ASSERT_VALID( this );
	switch( _rfmbi.m_dwButtonType )
	{
	case TPMX_RIBBON_EXIT_BUTTON:
		OnRibbonExitCommand();
	break;
	case TPMX_RIBBON_OPTIONS_BUTTON:
		OnRibbonOptionsDialogTrack();
	break;
	} // switch( _rfmbi.m_dwButtonType )
}

void CExtRibbonPage::OnRibbonOptionsDialogTrack(
	UINT nSelectedPageID // = __EXT_RIBBON_OPTIONS_DIALOG_PAGE_DEFAULT
	)
{
	ASSERT_VALID( this );
	nSelectedPageID;
}

void CExtRibbonPage::OnRibbonOptionsDialogInitPageRTCs(
	CExtRibbonOptionsDialog & dlgRibbonOptions
	)
{
	ASSERT_VALID( this );
	dlgRibbonOptions;
}

void CExtRibbonPage::OnRibbonExitCommand()
{
	ASSERT_VALID( this );
CWnd * pWnd = GetParent();
	if( pWnd == NULL )
		return;
	if( ( pWnd->GetStyle() & WS_CHILD ) == 0 )
		pWnd->SendMessage( WM_SYSCOMMAND, SC_CLOSE );
}

bool CExtRibbonPage::OnRibbonQueryGroupCaptionTextCompletelyVisible( CExtRibbonButtonGroup * pTBB ) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
	if( m_bHelperPopupMode )
	{
		const CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
		if( pRibbonPage != NULL )
			return pRibbonPage->OnRibbonQueryGroupCaptionTextCompletelyVisible( pTBB );
	} // if( m_bHelperPopupMode )
	return true;
}

bool CExtRibbonPage::OnRibbonQueryGroupCaptionTextWithToolTip( CExtRibbonButtonGroup * pTBB ) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
	if( m_bHelperPopupMode )
	{
		const CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
		if( pRibbonPage != NULL )
			return pRibbonPage->OnRibbonQueryGroupCaptionTextWithToolTip( pTBB );
	} // if( m_bHelperPopupMode )
	return pTBB->m_bHelperCaptionTextIsPartiallyVisible;
}

bool CExtRibbonPage::OnDeliverCmd( CExtBarButton * pTBB )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
	pTBB;
 	if( m_bHelperPopupMode )
	{
// 		GetParent()->PostMessage( WM_CLOSE );
		CExtRibbonPage * pRibbonPage = this;
		for(	;
				pRibbonPage != NULL;
				pRibbonPage = pRibbonPage->GetParentRibbonPage()
			)
		{
			if( ! pRibbonPage->m_bHelperPopupMode )
				break;
			pRibbonPage->GetParent()->PostMessage( WM_CLOSE );
		}
	} // if( m_bHelperPopupMode )
	return false;
}

INT CExtRibbonPage::RibbonLayout_GetTabIntersectionHeight() const
{
	ASSERT_VALID( this );
	return 0;
}

INT CExtRibbonPage::RibbonLayout_GetTabLineHeight() const
{
	ASSERT_VALID( this );
	return 0;
}

INT CExtRibbonPage::RibbonLayout_GetFrameCaptionHeight(
	INT * p_nTopBorderHeight // = NULL
	) const
{
	ASSERT_VALID( this );
	if( p_nTopBorderHeight != NULL )
		(*p_nTopBorderHeight) = 0;
	return 0;
}

INT CExtRibbonPage::RibbonLayout_GetBottomLineHeight() const
{
	ASSERT_VALID( this );
INT nHeightAtBottom = 0;
	if( ! RibbonQuickAccessBar_AboveTheRibbonGet() )
		nHeightAtBottom += RibbonQuickAccessBar_GetBottomHeight();
	return nHeightAtBottom;
}

INT CExtRibbonPage::RibbonQuickAccessBar_GetBottomHeight() const
{
	ASSERT_VALID( this );
INT nRetVal = PmBridge_GetPM() -> RibbonQuickAccessBar_GetBottomHeight( this );
	return nRetVal;
}

INT CExtRibbonPage::RibbonQuickAccessBar_GetButtonsAlignment() const
{
	ASSERT_VALID( this );
INT nRetVal = PmBridge_GetPM() -> RibbonQuickAccessBar_GetButtonsAlignment( this );
	return nRetVal;
}

void CExtRibbonPage::_RecalcPositionsImpl()
{
	if( GetSafeHwnd() == NULL )
		return;
	ASSERT_VALID( this );
	if( m_bRibbonSimplifiedLayoutDelayedFlush )
	{
		_RibbonPageRslaResetStateData();
		if( IsWindowVisible() )
			m_bRibbonSimplifiedLayoutDelayedFlush = false;
	}
//	_UpdateCtrlLock();
INT nTabLineHeight = RibbonLayout_GetTabLineHeight();
INT nTopBorderHeight = 0;
INT nFrameCaptionHeight = RibbonLayout_GetFrameCaptionHeight( &nTopBorderHeight );
INT nHeightAtTheTop = nTabLineHeight + nFrameCaptionHeight; // + nTopBorderHeight;
	if(		RibbonLayout_IsFrameIntegrationEnabled()
		&&	(! RibbonLayout_IsDwmCaptionIntegration() )
		)
		nHeightAtTheTop += nFrameCaptionHeight + nTopBorderHeight;
INT nBottomLineHeight = RibbonLayout_GetBottomLineHeight();
	if(		( ! Ribbon_UseSimplifiedLayoutAlgorithmGet() )
		||	m_nRslaLastDesiredWidth == (-32767)
//		||	m_bRslaLastStateHasCollapsedGroups
		)
		RibbonLayout_IleReset(
			NULL,
			__EXT_RIBBON_ILE_MAX,
			true,
			false,
			true
			);
CRect rcClient;
	GetClientRect( &rcClient );
	rcClient.top += nHeightAtTheTop;
	rcClient.bottom -= nBottomLineHeight;
CRect rcTabLine(
		rcClient.left,
		rcClient.top - nTabLineHeight,
		rcClient.right,
		rcClient.top
		);
	RibbonLayout_AdjustTabLineRect( rcTabLine );
CClientDC dc( this );
bool bAlignPageContent = true;
bool bAlignPageContentForce = false; // m_bRslaLastStateHasCollapsedGroups;
	if(		(	(! m_bHelperPopupMode )
			&&	RibbonPage_ExpandedModeGet()
			)
		||	m_bHelperAutoHideMode
		)
	{
		INT nDesiredWidth = rcClient.Width();
		INT nILE, nStartUpILE = m_nMaxILE, nCountLayouted = 0;
		bool bCompressedLayoutReached = false;
		INT nGroupIndex, nGroupCount = RibbonGroupButton_GetCount();
		if( Ribbon_UseSimplifiedLayoutAlgorithmGet() )
		{
			if( m_nRslaLastDesiredWidth != (-32767) )
			{
				//ASSERT( m_nRslaLastContentWidth != (-32767) );
				//ASSERT( m_nRslaLastILE != (-32767) );
				if( nDesiredWidth == m_nRslaLastDesiredWidth )
				{
						bAlignPageContent = false;
						bCompressedLayoutReached = true;
				} // if( nDesiredWidth == m_nRslaLastDesiredWidth )
				else if( nDesiredWidth < m_nRslaLastDesiredWidth )
				{
					if( nDesiredWidth >= m_nRslaLastContentWidth )
					{
						bAlignPageContent = false;
						bCompressedLayoutReached = true;
						m_nRslaLastDesiredWidth = nDesiredWidth;
					} // if( nDesiredWidth >= m_nRslaLastContentWidth )
					else
					{
						//if( ! m_bRslaLastStateHasCollapsedGroups )
							nStartUpILE = m_nRslaLastILE;
					} // else from if( nDesiredWidth >= m_nRslaLastContentWidth )
				} // else if( nDesiredWidth < m_nRslaLastDesiredWidth )
				else
				{
					_RibbonPageRslaResetStateData();

//					nILE = m_nRslaLastILE + 1;
//					if( nILE >= m_nMaxILE )
//					{
//						if( m_bRslaLastStateHasCollapsedGroups )
//						{
//							m_bRslaLastStateHasCollapsedGroups = false;
//							m_nRslaLastILE = nILE = m_nMinILE;
//							for( nGroupIndex = 0; nGroupIndex < nGroupCount; nGroupIndex ++ )
//							{
//								CExtRibbonButtonGroup * pGroupTBB = RibbonGroupButton_GetAt( nGroupIndex );
//								ASSERT_VALID( pGroupTBB );
//								pGroupTBB->TopCollapsedStateSet( false );
//								RibbonLayout_IleReset(
//									pGroupTBB,
//									nILE,
//									true,
//									false
//									);
//							} // for( nGroupIndex = 0; nGroupIndex < nGroupCount; nGroupIndex ++ )
//						} // if( m_bRslaLastStateHasCollapsedGroups )
//					} // if( nILE >= m_nMaxILE )
//					if( nILE <= m_nMaxILE )
//					{
//						nStartUpILE = -32767; // cancel post-collapsing
//						CSize _sizeLayout( 32767, 32767 );
//						for( ; true ; )
//						{
//							_sizeLayout.cx = _sizeLayout.cy = 32767;
//							for( nGroupIndex = 0; nGroupIndex < nGroupCount; nGroupIndex ++ )
//							{
//								CExtRibbonButtonGroup * pGroupTBB = RibbonGroupButton_GetAt( nGroupIndex );
//								ASSERT_VALID( pGroupTBB );
//								if(	RibbonLayout_FindIlvInGroup(
//										pGroupTBB,
//										nILE,
//										m_bRslaLastStateHasCollapsedGroups
//										) == 0
//									)
//									continue;
//								CPoint ptLayoutOffset( 0, 0 );
//								_sizeLayout =
//									RibbonLayout_Calc(
//										dc,
//										ptLayoutOffset,
//										true,
//										&nCountLayouted
//										);
//INT nNewBottom = rcClient.top + _sizeLayout.cy;
//rcClient.bottom = max( rcClient.bottom, nNewBottom );
//								if( _sizeLayout.cx > nDesiredWidth )
//								{
//									nStartUpILE = nILE;
//									break;
//								}
//							} // for( nGroupIndex = 0; nGroupIndex < nGroupCount; nGroupIndex ++ )
//							ASSERT( nILE <= m_nMaxILE );
//							if( nStartUpILE != (-32767) )
//								break;
//							if( nILE == m_nMaxILE )
//							{
//								if( m_bRslaLastStateHasCollapsedGroups )
//								{
//									m_bRslaLastStateHasCollapsedGroups = false;
//									m_nRslaLastILE = nILE = m_nMinILE;
//									for( nGroupIndex = 0; nGroupIndex < nGroupCount; nGroupIndex ++ )
//									{
//										CExtRibbonButtonGroup * pGroupTBB = RibbonGroupButton_GetAt( nGroupIndex );
//										ASSERT_VALID( pGroupTBB );
//										pGroupTBB->TopCollapsedStateSet( false );
//										RibbonLayout_IleReset(
//											pGroupTBB,
//											nILE,
//											true,
//											false
//											);
//									} // for( nGroupIndex = 0; nGroupIndex < nGroupCount; nGroupIndex ++ )
//									continue;
//								}
//								break;
//							}
//							nILE ++;
//						} // for( ; true ; )
//						if( _sizeLayout.cx <= nDesiredWidth )
//						{
//							bCompressedLayoutReached = true;
//							m_nRslaLastContentWidth = _sizeLayout.cx;
//							m_nRslaLastDesiredWidth = nDesiredWidth;
//							m_nRslaLastILE = nILE;
//						}
//					} // if( nILE <= m_nMaxILE )
//					else
//					{
//						bAlignPageContent = false;
//						bCompressedLayoutReached = true;
//						m_nRslaLastDesiredWidth = nDesiredWidth;
//					} // else from if( nILE <= m_nMaxILE )

				} // else from if( nDesiredWidth <= m_nRslaLastDesiredWidth )
			} // if( m_nRslaLastDesiredWidth != (-32767) )

			if( ( ! bCompressedLayoutReached ) && nStartUpILE >= m_nMinILE )
			{
				for( nILE = nStartUpILE; true ; )
				{
					if( nILE == nStartUpILE )
					{
						for( nGroupIndex = nGroupCount; nGroupIndex > 0; nGroupIndex -- )
						{
							CExtRibbonButtonGroup * pGroupTBB = RibbonGroupButton_GetAt( nGroupIndex - 1 );
							ASSERT_VALID( pGroupTBB );
							RibbonLayout_FindIlvInGroup(
									pGroupTBB,
									nILE,
									m_bRslaLastStateHasCollapsedGroups
									);
						}
						CPoint ptLayoutOffset( 0, 0 );
						CSize _sizeLayout =
							RibbonLayout_Calc(
								dc,
								ptLayoutOffset,
								true,
								&nCountLayouted
								);
INT nNewBottom = rcClient.top + _sizeLayout.cy;
rcClient.bottom = max( rcClient.bottom, nNewBottom );
						if( _sizeLayout.cx <= nDesiredWidth )
						{
							bCompressedLayoutReached = true;
//							rcClient.bottom = rcClient.top + _sizeLayout.cy;
							m_nRslaLastContentWidth = _sizeLayout.cx;
							m_nRslaLastDesiredWidth = nDesiredWidth;
							m_nRslaLastILE = nILE;
							break;
						}
					} // if( nILE == nStartUpILE )
					else
					for( nGroupIndex = nGroupCount; nGroupIndex > 0; nGroupIndex -- )
					{
						CExtRibbonButtonGroup * pGroupTBB = RibbonGroupButton_GetAt( nGroupIndex - 1 );
						ASSERT_VALID( pGroupTBB );
						if(	RibbonLayout_FindIlvInGroup(
								pGroupTBB,
								nILE,
								m_bRslaLastStateHasCollapsedGroups
								) == 0
							)
						{
							continue;
						}
						CPoint ptLayoutOffset( 0, 0 );
						CSize _sizeLayout =
							RibbonLayout_Calc(
								dc,
								ptLayoutOffset,
								true,
								&nCountLayouted
								);
INT nNewBottom = rcClient.top + _sizeLayout.cy;
rcClient.bottom = max( rcClient.bottom, nNewBottom );
						if( _sizeLayout.cx <= nDesiredWidth )
						{
							bCompressedLayoutReached = true;
//							rcClient.bottom = rcClient.top + _sizeLayout.cy;
							m_nRslaLastContentWidth = _sizeLayout.cx;
							m_nRslaLastDesiredWidth = nDesiredWidth;
							m_nRslaLastILE = nILE;
							break;
						}
					} // for( nGroupIndex = nGroupCount; nGroupIndex > 0; nGroupIndex -- )
					if( bCompressedLayoutReached )
						break;
					if( nILE == m_nMinILE )
					{
						if( m_bRslaLastStateHasCollapsedGroups )
							break;
						m_bRslaLastStateHasCollapsedGroups = true;
						m_nRslaLastILE = nILE = m_nMaxILE;
						continue;
					} // if( nILE == m_nMinILE )
					nILE --;
				} // for( nILE = nStartUpILE; true ; )
			} // if( ( ! bCompressedLayoutReached ) && nStartUpILE >= m_nMinILE )
			bCompressedLayoutReached;
			m_nRslaLastDesiredWidth = nDesiredWidth;
		} // if( Ribbon_UseSimplifiedLayoutAlgorithmGet() )
		else
		{
			for( nILE = m_nMaxILE; true ; )
			{
				INT nFound =
					RibbonLayout_FindIlvChangingCount(
						NULL,
						nILE,
						1,
						false
						);
				if( nFound > 0 || nILE == m_nMaxILE )
				{
					CPoint ptLayoutOffset( 0, 0 );
					CSize _sizeLayout =
						RibbonLayout_Calc(
							dc,
							ptLayoutOffset,
							true,
							&nCountLayouted
							);
					if( nCountLayouted == 0 )
						return; // break;
INT nNewBottom = rcClient.top + _sizeLayout.cy;
rcClient.bottom = max( rcClient.bottom, nNewBottom );
					if( _sizeLayout.cx <= nDesiredWidth )
					{
						bCompressedLayoutReached = true;
//						rcClient.bottom = rcClient.top + _sizeLayout.cy;
						break;
					}
				}
				if( nFound > 0 )
					continue;
				if( nILE == m_nMinILE )
					break;
				nILE --;
			} // for( nILE = m_nMaxILE; true ; )
			bool bCollapsedLayoutReached = false;
			if( ! bCompressedLayoutReached )
			{
				m_bRslaLastStateHasCollapsedGroups = true;
				bAlignPageContent = true;
				for( nILE = m_nMaxILE; true ; )
				{
					INT nFound =
						RibbonLayout_FindIlvChangingCount(
							NULL,
							nILE,
							1,
							false,
							true
							);
					if( nFound > 0 || nILE == m_nMaxILE )
					{
						CPoint ptLayoutOffset( 0, 0 );
						CSize _sizeLayout =
							RibbonLayout_Calc(
								dc,
								ptLayoutOffset,
								true,
								&nCountLayouted
								);
						if( nCountLayouted == 0 )
							return; // break;
						if( _sizeLayout.cx <= nDesiredWidth )
						{
							bCollapsedLayoutReached = true;
//							INT nNewBottom = rcClient.top + _sizeLayout.cy;
//							rcClient.bottom = max( rcClient.bottom, nNewBottom );
							break;
						}
					}
					if( nFound > 0 )
						continue;
					if( nILE == m_nMinILE )
						break;
					nILE --;
				} // for( nILE = m_nMaxILE; true ; )
			} // if( ! bCompressedLayoutReached )
			else
				m_bRslaLastStateHasCollapsedGroups = false;
			bCollapsedLayoutReached;
			bCompressedLayoutReached;
			nCountLayouted;
		} // if( (! m_bHelperPopupMode) ...
	} // else from if( Ribbon_UseSimplifiedLayoutAlgorithmGet() )

//	_UpdateCtrlUnlock();
INT nIndex, nCount = GetButtonsCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtBarButton * pTBB = GetButton( nIndex );
		ASSERT_VALID( pTBB );
		CExtRibbonButtonGallery * pRibbonGalleryTBB =
			DYNAMIC_DOWNCAST( CExtRibbonButtonGallery, pTBB );
		if( pRibbonGalleryTBB != NULL )
			pRibbonGalleryTBB->OnRibbonAlignContent( dc );
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	if( bAlignPageContent || bAlignPageContentForce )
		RibbonLayout_AlignPageContent( dc, rcClient );
	RibbonLayout_AlignTabLineContent( dc, rcTabLine );
}

void CExtRibbonPage::RibbonLayout_IleReset(
	CExtBarButton * pStartTBB, // = NULL
	INT nILE, // = __EXT_RIBBON_ILE_MAX
	bool bForceShowChildren, // = false
	bool bForceHideChildren, // = false
	bool bForceCleanCollapsedState // = false
	)
{
	ASSERT_VALID( this );
	ASSERT(
			__EXT_RIBBON_ILE_MIN <= nILE
		&&	nILE <= (__EXT_RIBBON_ILE_MAX+1)
		);
	ASSERT(
			( ! bForceShowChildren )
		||	( ! bForceHideChildren )
		);
	_UpdateCtrlLock();
INT nIndex;
INT nCount =
		( pStartTBB == NULL )
			? RibbonGroupButton_GetCount()
			: pStartTBB->ChildButtonGetCount()
			;
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtBarButton * pTBB =
			( pStartTBB == NULL )
				? RibbonGroupButton_GetAt( nIndex )
				: pStartTBB->ChildButtonGetAt( nIndex )
				;
		ASSERT_VALID( pTBB );
		if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
			continue;
		pTBB->RibbonILE_Set( nILE );
		if( pStartTBB != NULL )
		{
			if( bForceShowChildren || bForceHideChildren )
			{
				if( bForceShowChildren )
					pTBB->Show( true );
				else if( bForceHideChildren )
					pTBB->Show( false );
			} // if( bForceShowChildren || bForceHideChildren )
		} // if( pStartTBB != NULL )
		else
		{
			if( bForceCleanCollapsedState )
			{
				CExtRibbonButtonGroup * pGroupTBB =
					DYNAMIC_DOWNCAST( CExtRibbonButtonGroup, pTBB );
				if( pGroupTBB != NULL )
					pGroupTBB->TopCollapsedStateSet( false );
			} // if( bForceCleanCollapsedState )
		} // else from if( pStartTBB != NULL )
		RibbonLayout_IleReset(
			pTBB,
			nILE,
			bForceShowChildren,
			bForceHideChildren
			);
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	_UpdateCtrlUnlock();
}

INT CExtRibbonPage::RibbonLayout_FindIlvInGroup(
	CExtBarButton * pStartTBB,
	INT nILE,
	bool bEnableCollapsing
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pStartTBB );
	ASSERT(
			__EXT_RIBBON_ILE_MIN <= nILE
		&&	nILE <= (__EXT_RIBBON_ILE_MAX+1)
		);
	if( ( pStartTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
		return 0;
	if(		m_nRslaLastILE != (-32767)
		&&	pStartTBB->RibbonILE_Get() == nILE
		)
		return 0;
CExtRibbonButtonGroup * pGroupTBB = DYNAMIC_DOWNCAST( CExtRibbonButtonGroup, pStartTBB );
	if( pGroupTBB != NULL && pGroupTBB->ParentButtonGet() == NULL )
	{
		bool bCollapsedOld = pGroupTBB->TopCollapsedStateGet();
		if( bEnableCollapsing )
		{
			INT nCollapsedILE = pGroupTBB->RibbonILE_GetCollapsed();
			bool bCollapsedNew = ( nCollapsedILE >= nILE && nCollapsedILE != (__EXT_RIBBON_ILE_MAX+1) ) ? true : false;
			if( bCollapsedOld != bCollapsedNew )
			{
				pGroupTBB->TopCollapsedStateSet( bCollapsedNew );
				RibbonLayout_IleReset(
					pGroupTBB,
					nILE,
					( ! bCollapsedNew ),
					bCollapsedNew
					);
				return 1;
			} // if( bCollapsedOld != bCollapsedNew )
		} // if( bEnableCollapsing )
		if( bCollapsedOld )
			return 0;
	} // if( pGroupTBB != NULL && pGroupTBB->ParentButtonGet() == NULL )
INT nChangingCount = 0;
INT nIndex, nCount = pStartTBB->ChildButtonGetCount();
	for( nIndex = nCount; nIndex > 0; nIndex -- )
	{
		CExtBarButton * pTBB = pStartTBB->ChildButtonGetAt( nIndex - 1 );
		ASSERT_VALID( pTBB );
		if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
			continue;
		if( pTBB->RibbonILE_Set( nILE ) )
			nChangingCount ++;
		nChangingCount += RibbonLayout_FindIlvInGroup( pTBB, nILE, false );
	} // for( nIndex = nCount; nIndex > 0; nIndex -- )
	return nChangingCount;
}

INT CExtRibbonPage::RibbonLayout_FindIlvChangingCount(
	CExtBarButton * pStartTBB, // = NULL
	INT nILE, // = __EXT_RIBBON_ILE_MAX
	INT nMaxCount, // = 32767
	bool bUp, // = false
	bool bOneCollapsedLevelOnly // = false
	)
{
	ASSERT_VALID( this );
	ASSERT(
			__EXT_RIBBON_ILE_MIN <= nILE
		&&	nILE <= (__EXT_RIBBON_ILE_MAX+1)
		);
INT nChangingCount = 0;
INT nIndex, nCount =
		( pStartTBB == NULL )
			? RibbonGroupButton_GetCount()
			: pStartTBB->ChildButtonGetCount()
			;
	for( nIndex = nCount; nIndex > 0; nIndex -- )
	{
		CExtBarButton * pTBB =
			( pStartTBB == NULL )
				? RibbonGroupButton_GetAt( nIndex - 1 )
				: pStartTBB->ChildButtonGetAt( nIndex - 1 )
				;
		ASSERT_VALID( pTBB );
		if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
			continue;

		if(		pStartTBB != NULL
			||	bOneCollapsedLevelOnly
			)
		{
			INT nCurrentILE =
				bOneCollapsedLevelOnly
					? pTBB->RibbonILE_GetCollapsed()
					: pTBB->RibbonILE_Get()
					;
			if( bUp )
			{
				if( nCurrentILE >= nILE )
					continue;
			}
			else
			{
				if( nCurrentILE <= nILE )
					continue;
			}
			bool bChangedStateOrILV = false;
			if( bOneCollapsedLevelOnly )
			{
				bChangedStateOrILV = pTBB->RibbonILE_SetCollapsed( nILE );
				if( bChangedStateOrILV )
				{
					RibbonLayout_IleReset( pTBB, nILE, false, true );
				}
			}
			else
				bChangedStateOrILV = pTBB->RibbonILE_Set( nILE );
			if( bChangedStateOrILV )
			{
				nChangingCount ++;
				if( nChangingCount >= nMaxCount )
				{
					if( ! bOneCollapsedLevelOnly )
						RibbonLayout_IleReset( pTBB, nILE );
					return nChangingCount;
				}
			}
			continue;
		}
		ASSERT( ! bOneCollapsedLevelOnly );
		INT nFound =
			RibbonLayout_FindIlvChangingCount(
				pTBB,
				nILE,
				nMaxCount - nChangingCount,
				bUp
				);
		nChangingCount += nFound;
		if( nChangingCount >= nMaxCount )
		{
			RibbonLayout_IleReset( pTBB, nILE );
			return nChangingCount;
		}
	}
	return nChangingCount;
}

CRect CExtRibbonPage::_OnRibbonContentPaddingGetOuter() const
{
	ASSERT_VALID( this );
//	return CRect( 3, 3, 3, 4 );
CRect rcOuterContentPadding =
		PmBridge_GetPM()->Ribbon_GetPageOuterContentPadding( this );
	return rcOuterContentPadding;
}

CRect CExtRibbonPage::_OnRibbonContentPaddingGetGroup(
	const CExtRibbonButtonGroup * pGroupTBB
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pGroupTBB );
/*
	if(		pGroupTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonToolGroup ) )
		&&	pGroupTBB->ParentButtonGet() != NULL
		)
		return CRect( 2, 0, 2, 0 );
//	return CRect( 0, 0, 0, 0 );
	return CRect( 1, 1, 1, 1 );
//	return CRect( 2, 2, 2, 2 );
*/
CRect rcContentPadding = PmBridge_GetPM()->Ribbon_GetContentPadding( pGroupTBB );
	return rcContentPadding;
}

INT CExtRibbonPage::_OnRibbonContentPaddingGetOuterGroupDistance(
	const CExtRibbonButtonGroup * pGroupTBB,
	bool bDistanceBefore
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pGroupTBB );
INT nDistance =
		PmBridge_GetPM()->Ribbon_GetOuterGroupDistance(
			pGroupTBB,
			bDistanceBefore
			);
	return nDistance;
}

CRect CExtRibbonPage::_OnRibbonContentPaddingGetButton(
	const CExtBarButton * pTBB
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
/*
const CExtBarButton * pParentTBB = pTBB->ParentButtonGet();
	if( pParentTBB != NULL )
	{
		if(		pParentTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonToolGroup ) )
			&&	pParentTBB->ParentButtonGet() != NULL
			)
			return CRect( 0, 1, 0, 1 );
	}
	return CRect( 0, 0, 0, 0 );
//	return CRect( 1, 1, 1, 1 );
*/
CRect rcContentPadding = PmBridge_GetPM()->Ribbon_GetContentPadding( pTBB );
	return rcContentPadding;
}

INT CExtRibbonPage::OnRibbonGetSeparatorExtent(
	CExtBarButton * pTBB,
	bool bHorz
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
	pTBB;
	bHorz;
	return 2;
}

void CExtRibbonPage::Ribbon_OnCalcMinMaxILE()
{
	ASSERT_VALID( this );
	m_nMinILE = __EXT_RIBBON_ILE_MAX;
	m_nMaxILE = __EXT_RIBBON_ILE_MAX;
INT nIndex, nCount = RibbonGroupButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtRibbonButtonGroup * pGroupTBB = RibbonGroupButton_GetAt( nIndex );
		ASSERT_VALID( pGroupTBB );
		ASSERT( pGroupTBB->ParentButtonGet() == NULL );
		if( ( pGroupTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
			continue;
		pGroupTBB->OnRibbonCalcMinMaxILE( m_nMinILE, m_nMaxILE );
		INT nCollapsedILE = pGroupTBB->RibbonILE_GetCollapsed();
		if( nCollapsedILE >= 0 )
		{
			if( m_nMinILE > nCollapsedILE )
				m_nMinILE = nCollapsedILE;
			if( m_nMaxILE < nCollapsedILE )
				m_nMaxILE = nCollapsedILE;
		} // if( nCollapsedILE >= 0 )
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
}

CSize CExtRibbonPage::RibbonLayout_Calc(
	CDC & dc,
	CPoint ptLayoutOffset, // = CPoint( 0, 0 )
	bool bSetupRects, // = true
	INT * p_nCountLayouted // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
CSize _sizeLayout( 0, 0 );
CPoint ptCalcLayoutOffset = ptLayoutOffset;
INT nCountLayouted = 0;
	_UpdateCtrlLock();
	if( RibbonPage_ExpandedModeGet() )
	{
		CRect rcCP = _OnRibbonContentPaddingGetOuter();
		ptCalcLayoutOffset.x += rcCP.left;
		ptCalcLayoutOffset.y += rcCP.top;
		INT nIndex, nCount = RibbonGroupButton_GetCount();
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtRibbonButtonGroup * pGroupTBB = RibbonGroupButton_GetAt( nIndex );
			ASSERT_VALID( pGroupTBB );
			ASSERT( pGroupTBB->ParentButtonGet() == NULL );
			if( ! pGroupTBB->IsVisible() )
				continue;
			if( ( pGroupTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				continue;
			CSize _sizeGroup =
				pGroupTBB->OnRibbonCalcLayout(
					dc,
					ptCalcLayoutOffset,
					bSetupRects
					);
			nCountLayouted ++;
			ptCalcLayoutOffset.x += _sizeGroup.cx;
			_sizeLayout.cx += _sizeGroup.cx;
			_sizeLayout.cy = max( _sizeLayout.cy, _sizeGroup.cy );
		}
		_sizeLayout.cx += rcCP.left + rcCP.right;
		_sizeLayout.cy += rcCP.top + rcCP.bottom;
	} // if( RibbonPage_ExpandedModeGet() )
	if( p_nCountLayouted != NULL )
		(*p_nCountLayouted) = nCountLayouted;
	_UpdateCtrlUnlock();
	return _sizeLayout;
}

void CExtRibbonPage::RibbonLayout_AlignPageContent(
	CDC & dc,
	CRect rcAlignContent
	)
{
	ASSERT_VALID( this );
	_UpdateCtrlLock();
bool bRibbonPageIsExpandedMode = RibbonPage_ExpandedModeGet();
CRect rcCP = _OnRibbonContentPaddingGetOuter();
INT nIndex, nCount = RibbonGroupButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtRibbonButtonGroup * pGroupTBB = RibbonGroupButton_GetAt( nIndex );
		ASSERT_VALID( pGroupTBB );
		ASSERT( pGroupTBB->ParentButtonGet() == NULL );
		if( ! bRibbonPageIsExpandedMode )
		{
			pGroupTBB->Show( false );
			continue;
		}
		if( ( pGroupTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
			continue;
		if( ! pGroupTBB->IsVisible() )
			continue;
//		pGroupTBB->Show( true );
		CRect rcTBB = pGroupTBB->Rect();
		rcTBB.top = rcAlignContent.top + rcCP.top;
		rcTBB.bottom = rcAlignContent.bottom - rcCP.bottom;
		pGroupTBB->SetRect( rcTBB );
		pGroupTBB->OnRibbonAlignContent( dc );
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	_UpdateCtrlUnlock();
	_UpdateCtrlAll();
}

void CExtRibbonPage::RibbonLayout_AlignTabLineContent(
	CDC & dc,
	CRect rcAlignContent
	)
{
	ASSERT_VALID( this );
	m_byteHelper2007SeparatorAlpha = BYTE(0);
INT nIndex, nCount;

	nCount = RibbonRightButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtBarButton * pTBB = RibbonRightButton_GetAt( nCount - nIndex - 1 );
		ASSERT_VALID( pTBB );
		ASSERT( pTBB->ParentButtonGet() == NULL );
		if( ! pTBB->IsVisible() )
			continue;
		if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
			continue;
		CSize _sizeTBB = pTBB->CalculateLayout( dc, _GetDefButtonSize(), TRUE );
		CRect rcTBB(
			rcAlignContent.right - _sizeTBB.cx,
			rcAlignContent.top,
			rcAlignContent.right,
			rcAlignContent.top + _sizeTBB.cy
			);
		rcTBB.OffsetRect(
			0,
			( rcAlignContent.Height() - _sizeTBB.cy ) / 2
			);
		pTBB->SetRect( rcTBB );
		pTBB->OnRibbonAlignContent( dc );
		rcAlignContent.right -= _sizeTBB.cx;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )

INT nTabIntersectionHeight = RibbonLayout_GetTabIntersectionHeight();
INT nX = rcAlignContent.left, nCompressableCount = 0, nMinTbbWidth = 20,
		nContentWidth = 0, nAvailableWidth = rcAlignContent.Width();
CSize _sizePreCalcTBB( 10, rcAlignContent.Height() );
	nCount = RibbonTabPageButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtRibbonButtonTabPage * pTabPageTBB = RibbonTabPageButton_GetAt( nIndex );
		ASSERT_VALID( pTabPageTBB );
		ASSERT( pTabPageTBB->ParentButtonGet() == NULL );
		if( ! pTabPageTBB->IsVisible() )
			continue;
		if( ( pTabPageTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
			continue;
		CSize _sizeTBB = pTabPageTBB->CalculateLayout( dc, _sizePreCalcTBB, TRUE );
		CRect rcTBB(
			nX,
			rcAlignContent.top,
			nX + _sizeTBB.cx,
			rcAlignContent.bottom + nTabIntersectionHeight
			);
		nX += _sizeTBB.cx;
		nContentWidth += _sizeTBB.cx;
		pTabPageTBB->SetRect( rcTBB );
		pTabPageTBB->OnRibbonAlignContent( dc );
		if( _sizeTBB.cx >= nMinTbbWidth )
			nCompressableCount ++;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	if( nCompressableCount == 0 || nContentWidth <= nAvailableWidth )
		return;
INT nContentWidthEntire = nContentWidth;
	for( ; true ; )
	{
		INT nCompressWidth = nContentWidth - nAvailableWidth;
		ASSERT( nCompressWidth > 0 );
		INT nCompressPerTab = nCompressWidth / nCompressableCount;
		INT nCompressRest = nCompressWidth % nCompressableCount;
		if( nCompressPerTab == 0 && nCompressRest == 0 )
			break;
		nCompressableCount = nContentWidth = 0;
		nX = rcAlignContent.left;
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtRibbonButtonTabPage * pTabPageTBB = RibbonTabPageButton_GetAt( nIndex );
			ASSERT_VALID( pTabPageTBB );
			ASSERT( pTabPageTBB->ParentButtonGet() == NULL );
			if( ! pTabPageTBB->IsVisible() )
				continue;
			if( ( pTabPageTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				continue;
			CRect rcTBB = *pTabPageTBB;
			CSize _sizeTBB = rcTBB.Size();
			if( _sizeTBB.cx > nMinTbbWidth )
			{
				_sizeTBB.cx -= nCompressPerTab;
				if( nCompressRest > 0 )
				{
					nCompressRest --;
					_sizeTBB.cx --;
				}
				if( _sizeTBB.cx < nMinTbbWidth )
					_sizeTBB.cx = nMinTbbWidth;
				else if( _sizeTBB.cx > nMinTbbWidth )
					nCompressableCount ++;
			} // if( _sizeTBB.cx > nMinTbbWidth )
			rcTBB.SetRect(
				nX,
				rcAlignContent.top,
				nX + _sizeTBB.cx,
				rcAlignContent.bottom + nTabIntersectionHeight
				);
			nX += _sizeTBB.cx;
			nContentWidth += _sizeTBB.cx;
			pTabPageTBB->SetRect( rcTBB );
			pTabPageTBB->OnRibbonAlignContent( dc );
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
		if( nContentWidth <= nAvailableWidth || nCompressableCount == 0 )
			break;
	} // for( ; true ; )
	if( nContentWidthEntire > nAvailableWidth && nAvailableWidth > 0 )
	{
		INT nPercent255 = 255 - ::MulDiv( nAvailableWidth, 255, nContentWidthEntire );
		if( nPercent255 > 64 )
			m_byteHelper2007SeparatorAlpha = BYTE( nPercent255 );
	}
	else
		m_byteHelper2007SeparatorAlpha = 0x0FF;
}

bool CExtRibbonPage::RibbonLayout_IsFrameIntegrationEnabled() const
{
	ASSERT_VALID( this );
	return false;
}

void CExtRibbonPage::RibbonLayout_AdjustTabLineRect( CRect & rcTabLine )
{
	ASSERT_VALID( this );
	rcTabLine;
}

BYTE CExtRibbonPage::RibbonLayout_Get2007SeparatorAlpha(
	const CExtRibbonButtonTabPage * pTBB
	) const
{
	ASSERT_VALID( this );
	pTBB;
	return m_byteHelper2007SeparatorAlpha;
}

INT CExtRibbonPage::RibbonLayout_GetGroupHeight(
	CExtRibbonButtonGroup * pGroupTBB
	) const
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( pGroupTBB != NULL )
	{
		ASSERT_VALID( pGroupTBB );
	}
#endif // _DEBUG
	if( ! RibbonPage_ExpandedModeGet() )
	{
		if( RibbonQuickAccessBar_AboveTheRibbonGet() )
		{
			INT nHeight = PmBridge_GetPM() -> Ribbon_GetContractedMarginHeight( this );
			return nHeight;
		} // if( RibbonQuickAccessBar_AboveTheRibbonGet() )
		return 0;
	} // if( ! RibbonPage_ExpandedModeGet() )
//	return 92;
INT nHeight = PmBridge_GetPM() -> Ribbon_GetGroupHeight( pGroupTBB );
	return nHeight;
}

CRect CExtRibbonPage::RibbonLayout_GetGroupContentAlignmentRect(
	const CExtRibbonButtonGroup * pGroupTBB
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pGroupTBB );
CRect rcGroupContentAlignmentRect = *pGroupTBB;
	if( RibbonLayout_GroupCaptionIsVisible( pGroupTBB ) )
	{
		INT nCaptionHeight = RibbonLayout_GroupCaptionGetHeight( pGroupTBB );
		bool bCaptionAtTop = RibbonLayout_GroupCaptionIsTopAligned( pGroupTBB );
		if( bCaptionAtTop )
			rcGroupContentAlignmentRect.top += nCaptionHeight;
		else
			rcGroupContentAlignmentRect.bottom -= nCaptionHeight;
	} // if( RibbonLayout_GroupCaptionIsVisible( pGroupTBB ) )
CRect rcCP = pGroupTBB->OnRibbonGetContentPadding();
	rcGroupContentAlignmentRect.DeflateRect(
		rcCP.left,
		rcCP.top,
		rcCP.right,
		rcCP.bottom
		);
	return rcGroupContentAlignmentRect;
}

bool CExtRibbonPage::RibbonLayout_GroupCaptionIsVisible(
	const CExtRibbonButtonGroup * pGroupTBB
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pGroupTBB );
	if( ! m_bHelperAutoHideMode )
	{
		if( pGroupTBB->ParentButtonGet() != NULL )
			return false;
	}
	return true;
}

CRect CExtRibbonPage::RibbonLayout_GroupCaptionRectGet(
	const CExtRibbonButtonGroup * pGroupTBB
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pGroupTBB );
	if( ! RibbonLayout_GroupCaptionIsVisible( pGroupTBB ) )
		return CRect( 0, 0, 0, 0 );
CRect rcRibbonGroupCaption = *pGroupTBB;
INT nCaptionHeight = RibbonLayout_GroupCaptionGetHeight( pGroupTBB );
bool bCaptionAtTop = RibbonLayout_GroupCaptionIsTopAligned( pGroupTBB );
	if( bCaptionAtTop )
		rcRibbonGroupCaption.bottom = rcRibbonGroupCaption.top + nCaptionHeight;
	else
		rcRibbonGroupCaption.top = rcRibbonGroupCaption.bottom - nCaptionHeight;
	return rcRibbonGroupCaption;
}

bool CExtRibbonPage::RibbonLayout_GroupCaptionIsTopAligned(
	const CExtRibbonButtonGroup * pGroupTBB
	) const
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( pGroupTBB != NULL )
	{
		ASSERT_VALID( pGroupTBB );
	}
#endif // _DEBUG
bool bRetVal = PmBridge_GetPM()->Ribbon_GroupCaptionIsAtTop( pGroupTBB );
	return bRetVal;
}

INT CExtRibbonPage::RibbonLayout_GroupCaptionGetHeight(
	const CExtRibbonButtonGroup * pGroupTBB
	) const
{
	ASSERT_VALID( this );
	if( pGroupTBB != NULL )
	{
		ASSERT_VALID( pGroupTBB );
		if( pGroupTBB->ParentButtonGet() == NULL )
			return PmBridge_GetPM()->Ribbon_GroupCaptionGetHeight( pGroupTBB );
	} // if( pGroupTBB != NULL )
	return 0;
}

void CExtRibbonPage::RibbonLayout_GetSimpleGroupContentAlignmentFlags(
	CDC & dc,
	const CExtRibbonButtonGroup * pGroupTBB,
	bool & bUseHorizontalAlignment,
	bool & bUseVerticalAlignment
	) const
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( pGroupTBB != NULL )
	{
		ASSERT_VALID( pGroupTBB );
	}
#endif // _DEBUG
	dc;
const CExtCustomizeCmdTreeNode * pNode = pGroupTBB->GetCmdNode();
	if( pNode != NULL )
	{
		ASSERT_VALID( pNode );
		DWORD dwFlags = pNode->GetFlags();
		bUseHorizontalAlignment = ( (dwFlags&__ECTN_RGA_HORIZONTAL) != 0 ) ? true : false;
		bUseVerticalAlignment = ( (dwFlags&__ECTN_RGA_VERTICAL) != 0 ) ? true : false;
		return;
	} // if( pNode != NULL )
	bUseHorizontalAlignment = false;
	bUseVerticalAlignment = false;
}

void CExtRibbonPage::RibbonLayout_GetToolGroupContentAlignmentFlags(
	CDC & dc,
	const CExtRibbonButtonToolGroup * pToolGroupTBB,
	bool & bUseHorizontalAlignment,
	bool & bUseVerticalAlignment,
	bool & bUseToolRowVerticalAlignment
	) const
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( pToolGroupTBB != NULL )
	{
		ASSERT_VALID( pToolGroupTBB );
	}
#endif // _DEBUG
	dc;
const CExtCustomizeCmdTreeNode * pNode = pToolGroupTBB->GetCmdNode();
	if( pNode != NULL )
	{
		ASSERT_VALID( pNode );
		DWORD dwFlags = pNode->GetFlags();
		bUseHorizontalAlignment = ( (dwFlags&__ECTN_RGA_HORIZONTAL) != 0 ) ? true : false;
		bUseVerticalAlignment = ( (dwFlags&__ECTN_RGA_VERTICAL) != 0 ) ? true : false;
		bUseToolRowVerticalAlignment = ( (dwFlags&__ECTN_RGA_TOOL_ROW_VERTICAL) != 0 ) ? true : false;
		return;
	} // if( pNode != NULL )
	bUseHorizontalAlignment = false;
	bUseVerticalAlignment = false;
	bUseToolRowVerticalAlignment = false;
}

bool CExtRibbonPage::IsForceHoverWhenMenuTracking() const
{
	ASSERT_VALID( this );
	if( GetMenuTrackingButton() >= 0 )
		return false;
	if(		GetSafeHwnd() != NULL
		&&	GetParent()->IsKindOf( RUNTIME_CLASS( CExtPopupBaseWnd ) )
		)
		return true;
	return false;
}

bool CExtRibbonPage::OnQueryHoverBasedMenuTracking(
	const CExtBarButton * pTBB
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
	pTBB;
	return false;
}

void CExtRibbonPage::OnLButtonDown(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	CExtMenuControlBar::OnLButtonDown( nFlags, point );
}

void CExtRibbonPage::OnLButtonUp(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	CExtMenuControlBar::OnLButtonUp( nFlags, point );
}

void CExtRibbonPage::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	CExtMenuControlBar::OnLButtonDblClk( nFlags, point );
}

void CExtRibbonPage::OnRButtonDown(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
//	nFlags;
//	point;
//	//	CExtMenuControlBar::OnRButtonDown( nFlags, point );

	if( m_bHelperPopupMode )
	{
		CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
		if( pRibbonPage != NULL )
		{
			CExtRibbonBar * pRibbonBar =
				DYNAMIC_DOWNCAST( CExtRibbonBar, pRibbonPage );
			if( pRibbonBar != NULL )
			{
				CPoint ptTranslated = point;
				ClientToScreen( &ptTranslated );
				pRibbonBar->ScreenToClient( &ptTranslated );
				INT nHT = HitTest( point );
				if( nHT >= 0 )
				{
					CExtBarButton * pTBB = GetButton( nHT );
					ASSERT_VALID( pTBB );
					UINT nCmdID = pTBB->GetCmdID( false );
					nHT = pRibbonBar->CommandToIndex( nCmdID );
					if( nHT < 0 )
						return;
					pTBB = pRibbonBar->GetButton( nHT );
					ASSERT_VALID( pTBB );
					if( pRibbonBar->OnRibbonTrackButtonContextMenu( pTBB, nFlags, ptTranslated ) )
						return;
				} // if( nHT >= 0 )
				if( pRibbonBar->OnRibbonTrackBarContextMenu( nFlags, ptTranslated ) )
					return;
			} // if( pRibbonBar != NULL )
		} // if( pRibbonPage != NULL )
	} // if( m_bHelperPopupMode )
}

void CExtRibbonPage::OnRButtonUp(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	nFlags;
	point;
//	CExtMenuControlBar::OnRButtonUp( nFlags, point );
}

void CExtRibbonPage::OnRButtonDblClk(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	nFlags;
	point;
//	CExtMenuControlBar::OnRButtonDblClk( nFlags, point );
}

void CExtRibbonPage::OnMButtonDown(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	CExtMenuControlBar::OnMButtonDown( nFlags, point );
}

void CExtRibbonPage::OnMButtonUp(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	CExtMenuControlBar::OnMButtonUp( nFlags, point );
}

void CExtRibbonPage::OnMButtonDblClk(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	CExtMenuControlBar::OnMButtonDblClk( nFlags, point );
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonGalleryInplaceScrollBar

IMPLEMENT_DYNCREATE( CExtRibbonGalleryInplaceScrollBar, CExtScrollBar );

CExtRibbonGalleryInplaceScrollBar::CExtRibbonGalleryInplaceScrollBar()
	: m_nTrackedButton( CExtRibbonGalleryInplaceScrollBar::__BTT_NONE )
	, m_bPressedTracking( false )
	, m_bCheckScrollPosByItemTopFailOnce( false )
	, m_nTrackingTimerBaseID( 432 )
	, m_nTrackingTimerEllpase( 10 )
	, m_nTrackingTimerStepPX( 5 )
{
	m_bCompleteRepaint = true;
INT nIndex, nCount = sizeof(m_arrButtons) / sizeof(m_arrButtons[0]);
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		m_arrButtons[ nIndex ] = __BST_DISABLED;
	}
}

CExtRibbonGalleryInplaceScrollBar::~CExtRibbonGalleryInplaceScrollBar()
{
CExtAnimationSite * pAcAS = AnimationClient_SiteGet();
	if( pAcAS != NULL )
		pAcAS->AnimationSite_ClientRemove( this );
}

void CExtRibbonGalleryInplaceScrollBar::RgBtnClearState(
	bool bClearHover, // = false
	bool bClearPressed // = false
	)
{
	ASSERT_VALID( this );
CExtPaintManager::PAINTSCROLLBARDATA _psbd( this );
INT nIndex, nCount = sizeof(m_arrButtons) / sizeof(m_arrButtons[0]);
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		if(		( m_arrButtons[ nIndex ] != __BST_HOVER || bClearHover )
			&&	( m_arrButtons[ nIndex ] != __BST_PRESSED || bClearPressed )
			)
			m_arrButtons[ nIndex ] = __BST_NORMAL;
		if( nIndex == INT(__BTT_UP) || nIndex == INT(__BTT_DOWN) )
		{
			if( _psbd.m_bEnabled )
			{
				if( nIndex == INT(__BTT_UP) )
				{
					if( _psbd.m_DSI.nPos == _psbd.m_DSI.nMin )
						m_arrButtons[ nIndex ] = __BST_DISABLED;
				}
				else if( nIndex == INT(__BTT_DOWN) )
				{
					INT nScrollLimit = INT(_psbd.m_DSI.nMax-_psbd.m_DSI.nMin-_psbd.m_DSI.nPage-1);
					if( _psbd.m_DSI.nPos >= nScrollLimit )
						m_arrButtons[ nIndex ] = __BST_DISABLED;
				}
			}
			else
				m_arrButtons[ nIndex ] = __BST_DISABLED;
			continue;
		} // if( nIndex == INT(__BTT_UP) || nIndex == INT(__BTT_DOWN) )
	} // INT nIndex, nCount = sizeof(m_arrButtons) / sizeof(m_arrButtons[0]);
}

CExtRibbonGalleryInplaceScrollBar::e_ButtonType_t
		CExtRibbonGalleryInplaceScrollBar::RgBtnHitTest(
			CPoint point
			)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return __BTT_NONE;
	if( point.x < 0 || point.y < 0 )
	{
		if( ! ::GetCursorPos( &point ) )
			return __BTT_NONE;
		ScreenToClient( &point );
	} // if( point.x < 0 || point.y < 0 )
CRect rc;
	GetClientRect( &rc );
	if( ! rc.PtInRect( point ) )
		return __BTT_NONE;
INT nIndex, nCount = sizeof(m_arrButtons) / sizeof(m_arrButtons[0]);
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CRect rc = RgBtnGetRect( (CExtRibbonGalleryInplaceScrollBar::e_ButtonType_t)nIndex );
		if( rc.PtInRect( point ) )
			return (CExtRibbonGalleryInplaceScrollBar::e_ButtonType_t)nIndex;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return __BTT_NONE;
}

CExtRibbonGalleryInplaceScrollBar::e_ButtonType_t
		CExtRibbonGalleryInplaceScrollBar::RgBtnHitTest()
{
	ASSERT_VALID( this );
	return RgBtnHitTest( CPoint( -1, -1 ) );
}

CRect CExtRibbonGalleryInplaceScrollBar::RgBtnGetRect(
	CExtRibbonGalleryInplaceScrollBar::e_ButtonType_t nBtnIndex
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return CRect( 0, 0, 0, 0 );
	switch( nBtnIndex )
	{
	case __BTT_UP:
	case __BTT_DOWN:
	case __BTT_MENU:
		{
			CRect rcClient;
			GetClientRect( &rcClient );
			INT nBasicHeight = rcClient.Height() / INT(__BTT_COUNT_OF_BUTTONS);
			CRect rc = rcClient;
			rc.bottom = rc.top + nBasicHeight;
			rc.OffsetRect( 0, nBasicHeight * INT(nBtnIndex) );
			if( INT(nBtnIndex) == (INT(__BTT_COUNT_OF_BUTTONS)-1) )
				rc.bottom = rcClient.bottom;
			return rc;
		}
	default:
		return CRect( 0, 0, 0, 0 );
	} // switch( bBtnIndex )
}

BEGIN_MESSAGE_MAP(CExtRibbonGalleryInplaceScrollBar, CExtScrollBar)
	//{{AFX_MSG_MAP(CExtRibbonGalleryInplaceScrollBar)
	ON_WM_CANCELMODE()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_MBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDBLCLK()
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
END_MESSAGE_MAP()

void CExtRibbonGalleryInplaceScrollBar::PreSubclassWindow()
{
	CExtScrollBar::PreSubclassWindow();
}

LRESULT CExtRibbonGalleryInplaceScrollBar::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
LRESULT lResult =
		CExtScrollBar::DefWindowProc( message, wParam, lParam );
	return lResult;
}

bool CExtRibbonGalleryInplaceScrollBar::_CheckScrollPosByItemTop(
	INT nOffset // = 0
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return true;
CExtToolBoxWnd * pToolBox = DYNAMIC_DOWNCAST( CExtToolBoxWnd, GetParent() );
	if( pToolBox == NULL )
		return true;
	if( m_bCheckScrollPosByItemTopFailOnce )
	{
		m_bCheckScrollPosByItemTopFailOnce = false;
		return false;
	}
CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTopLevel, * pBottomLevel, * pRoot = pToolBox->ItemGetRoot();
	if( pRoot == NULL )
		return true;
CRect rc, rcClient = pToolBox->OnSwGetClientRect();
	rcClient.OffsetRect( 0, nOffset );
	pTopLevel = pRoot->ItemGetNext(__TBCGN_FIRST_CHILD);
	for( ; pTopLevel != NULL; pTopLevel = pTopLevel->ItemGetNext(__TBCGN_SIBLING,1) )
	{
		rc = pToolBox->ItemGetRect( pTopLevel );
		if( rc.top == rcClient.top )
			return true;
		if( rc.top > rcClient.top )
			return false;
		DWORD dwItemStyle = pTopLevel->GetItemStyle();
		if( (dwItemStyle & __TBWI_EXPANDED) == 0 )
			continue;
		pBottomLevel = pTopLevel->ItemGetNext(__TBCGN_FIRST_CHILD);
		for( ; pBottomLevel != NULL; pBottomLevel = pBottomLevel->ItemGetNext(__TBCGN_SIBLING,1) )
		{
			rc = pToolBox->ItemGetRect( pBottomLevel );
			if( rc.top == rcClient.top )
				return true;
			if( rc.top > rcClient.top )
				return false;
		} // for( ; pBottomLevel != NULL; pBottomLevel = pBottomLevel->ItemGetNext(__TBCGN_SIBLING,1) )
	} // for( ; pTopLevel != NULL; pTopLevel = pTopLevel->ItemGetNext(__TBCGN_SIBLING,1) )
	return false;
}

void CExtRibbonGalleryInplaceScrollBar::OnTimer(__EXT_MFC_UINT_PTR nIDEvent)
{
	ASSERT_VALID( this );
	if( m_nTrackingTimerBaseID <= INT(nIDEvent) && INT(nIDEvent) < ( m_nTrackingTimerBaseID + INT(__BTT_COUNT_OF_BUTTONS) ) )
	{
		bool bKillTimer = false;
		e_ButtonType_t nBT = e_ButtonType_t( INT(nIDEvent) - m_nTrackingTimerBaseID );
		INT nStepSize = 0;
		switch( nBT )
		{
		case __BTT_UP:
			nStepSize = -1;
		break;
		case __BTT_DOWN:
			nStepSize = 1;
		break;
		default:
			bKillTimer = true;
		break;
		} // switch( nBT )
		if( nStepSize != 0 )
		{
			bool bLButtonDown = CExtPopupMenuWnd::IsKeyPressed( VK_LBUTTON ) ? true : false;
			bool bScroll = ( bLButtonDown && ( nBT == RgBtnHitTest() ) ) ? true : false;
			if( (! bScroll) && (! bLButtonDown ) )
				bScroll = ! _CheckScrollPosByItemTop();
			if( bScroll )
			{
				CExtPaintManager::PAINTSCROLLBARDATA _psbd( this );
				INT nScrollLimit =
					  _psbd.m_DSI.nMax
					- _psbd.m_DSI.nMin
					- _psbd.m_DSI.nPage
					+ 1
					;
				INT nOffset = 0, nIndex, nCount = m_nTrackingTimerStepPX, nScrollPos = _psbd.m_DSI.nPos;
				for( nIndex = 0; nIndex < nCount; nIndex ++ )
				{
					nOffset += nStepSize;
					nScrollPos += nStepSize;
					if( nScrollPos < 0 )
					{
						nScrollPos = 0;
						if( nStepSize < 0 )
							bKillTimer = true;
					}
					if( nScrollPos > nScrollLimit )
					{
						nScrollPos = nScrollLimit;
						if( nStepSize > 0 )
							bKillTimer = true;
					}
					if( (! bKillTimer ) && (! bLButtonDown ) )
						bKillTimer = _CheckScrollPosByItemTop( nOffset );
					if( bKillTimer )
						break;
				} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
				if( _GetScrollPos( false ) != nScrollPos )
				{
					HWND hWndParent = ::GetParent( m_hWnd );
					if( hWndParent != NULL )
						::SendMessage(
							hWndParent,
							_psbd.m_bHorzBar ? WM_HSCROLL : WM_VSCROLL,
							MAKEWPARAM(
								( nStepSize < 0 )
									? ( _psbd.m_bHorzBar ? SB_LINELEFT : SB_LINEUP )
									: ( _psbd.m_bHorzBar ? SB_LINERIGHT : SB_LINEDOWN )
									,
								0
								),
							LPARAM(m_hWnd)
							);
					_SetScrollPos( nScrollPos, false );
//					if( hWndParent != NULL )
//						::UpdateWindow( hWndParent );
				} // if( _GetScrollPos( false ) != nScrollPos )
			} // if( bScroll )
			else
				bKillTimer = true;
		} // if( nStepSize != 0 )
		if( bKillTimer && nBT != m_nTrackedButton )
			KillTimer( m_nTrackingTimerBaseID + INT(nBT) );
		return;
	} // if( m_nTrackingTimerBaseID <= INT(nIDEvent) && INT(nIDEvent) < ( m_nTrackingTimerBaseID + INT(__BTT_COUNT_OF_BUTTONS) ) )
	CExtScrollBar::OnTimer( nIDEvent );
}

LRESULT CExtRibbonGalleryInplaceScrollBar::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if( message == WM_IDLEUPDATECMDUI )
	{
		RgBtnClearState();
		Invalidate();
		UpdateWindow();
		return 0L;
	}
bool bUnLockRedraw = false;
HWND hWndOwn = m_hWnd;
	if(		hWndOwn != NULL
		&&	::IsWindow( hWndOwn )
//		&&	m_bCompleteRepaint
		&&	_IsLockAbleMessage( message )
//		&&	(GetStyle()&WS_VISIBLE) != 0
		)
		bUnLockRedraw = true;
LRESULT lResult =
		CExtScrollBar::WindowProc( message, wParam, lParam );
	if(		bUnLockRedraw
		&&	hWndOwn != NULL
		&&	::IsWindow( hWndOwn )
		)
		PostMessage( WM_IDLEUPDATECMDUI );
	return lResult;
}

INT CExtRibbonGalleryInplaceScrollBar::_GetScrollPos(
	bool bTrackPos // = false
	) const
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
INT nRetVal = CExtScrollBar::_GetScrollPos( bTrackPos );
	return nRetVal;
}

void CExtRibbonGalleryInplaceScrollBar::_SetScrollPos(
	INT nPos,
	bool bTrackPos, // = false
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	CExtScrollBar::_SetScrollPos( nPos, bTrackPos, bRedraw );
	PostMessage( WM_IDLEUPDATECMDUI );
}

void CExtRibbonGalleryInplaceScrollBar::PostNcDestroy()
{
	ASSERT_VALID( this );
	CExtScrollBar::PostNcDestroy();
}

void CExtRibbonGalleryInplaceScrollBar::_ScanSysMertics()
{
	ASSERT_VALID( this );
	CExtScrollBar::_ScanSysMertics();
	m_nVertBarWidth = PmBridge_GetPM()->UiScalingDo( 15, CExtPaintManager::__EUIST_X );
}

void CExtRibbonGalleryInplaceScrollBar::OnSbPaint( CDC & dc )
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	PmBridge_GetPM()->RibbonGallery_PaintScrollArea( dc, this );
}

void CExtRibbonGalleryInplaceScrollBar::OnCancelMode()
{
	CExtScrollBar::OnCancelMode();
	if( m_bProcessingCancelMode )
		return;
	m_bProcessingCancelMode = true;
bool bRedraw = false;
	if( m_nTrackedButton != __BTT_NONE )
	{
		bRedraw = true;
		if( m_bPressedTracking )
		{
//			KillTimer( m_nTrackingTimerBaseID + INT(m_nTrackedButton) );
			m_bPressedTracking = false;
		}
		m_nTrackedButton = __BTT_NONE;
		RgBtnClearState( true, true );
	}
	m_bProcessingCancelMode = false;
	if( bRedraw )
	{
		Invalidate();
		///UpdateWindow();
	}
}

void CExtRibbonGalleryInplaceScrollBar::OnMouseMove(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	nFlags;
bool bAnimationLocked = AnimationClient_CacheGeneratorIsLocked();
e_ButtonType_t nHT = RgBtnHitTest( point );
	if( m_bPressedTracking )
	{
		if( m_nTrackedButton != nHT )
		{
		} // if( m_nTrackedButton != nHT )
	} // if( m_bPressedTracking )
	else
	{
		if( m_nTrackedButton == nHT )
			return;
		m_nTrackedButton = nHT;
		if( ! bAnimationLocked )
		{
			AnimationClient_CacheGeneratorLock();
//			if( AnimationClient_StateGet(true).IsEmpty() )
 				AnimationClient_CacheNextStateMinInfo(
					false,
					__EAPT_BY_HOVERED_STATE_TURNED_ON
					);
		}
		RgBtnClearState( true, true );
		if(		m_nTrackedButton == __BTT_NONE
			||	m_arrButtons[ m_nTrackedButton ] == __BST_DISABLED
			)
		{
			if( ::GetCapture() == GetSafeHwnd() )
				::ReleaseCapture();
		}
		else
		{
			m_arrButtons[ m_nTrackedButton ] = __BST_HOVER;
			SetCapture();
		}
		if( ! bAnimationLocked )
		{
 			AnimationClient_CacheNextStateMinInfo(
				true,
				__EAPT_BY_HOVERED_STATE_TURNED_ON
				);
			AnimationClient_CacheGeneratorUnlock();
		}
		Invalidate();
	} // else from if( m_bPressedTracking )
}

void CExtRibbonGalleryInplaceScrollBar::OnLButtonDown(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	nFlags;
e_ButtonType_t nHT = RgBtnHitTest( point );
	if( nHT == __BTT_NONE )
		return;
CExtRibbonGalleryWnd * pRibbonGalleryWnd =
		DYNAMIC_DOWNCAST( CExtRibbonGalleryWnd, GetParent() );
	if( pRibbonGalleryWnd )
	{
		if( pRibbonGalleryWnd->OnInplaceScrollBarButtonClick( nHT, true ) )
			return;
	}
bool bAnimationLocked = AnimationClient_CacheGeneratorIsLocked();
	if( ! bAnimationLocked )
	{
		AnimationClient_CacheGeneratorLock();
//		if( AnimationClient_StateGet(true).IsEmpty() )
 			AnimationClient_CacheNextStateMinInfo(
				false,
				__EAPT_BY_PRESSED_STATE_TURNED_ON
				);
	}
	m_nTrackedButton = nHT;
	m_bPressedTracking = true;
	RgBtnClearState( true, true );
	m_arrButtons[ m_nTrackedButton ] = __BST_PRESSED;
	if( ! bAnimationLocked )
	{
 		AnimationClient_CacheNextStateMinInfo(
			true,
			__EAPT_BY_PRESSED_STATE_TURNED_ON
			);
		AnimationClient_CacheGeneratorUnlock();
	}
	SetCapture();
INT nTimerID = m_nTrackingTimerBaseID;
	for( ; nTimerID < m_nTrackingTimerBaseID + INT(__BTT_COUNT_OF_BUTTONS); nTimerID ++ )
		KillTimer( nTimerID );
	m_bCheckScrollPosByItemTopFailOnce = false;
	m_bCheckScrollPosByItemTopFailOnce = _CheckScrollPosByItemTop();
	SetTimer(
		UINT( m_nTrackingTimerBaseID + INT(m_nTrackedButton) ),
		UINT(m_nTrackingTimerEllpase),
		NULL
		);
	Invalidate();
}

void CExtRibbonGalleryInplaceScrollBar::OnLButtonUp(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	nFlags;
	if( ! m_bPressedTracking )
		return;
	ASSERT( m_nTrackedButton != __BTT_NONE );
//	KillTimer( m_nTrackingTimerBaseID + INT(m_nTrackedButton) );
bool bAnimationLocked = AnimationClient_CacheGeneratorIsLocked();
	if( ! bAnimationLocked )
	{
		AnimationClient_CacheGeneratorLock();
//		if( AnimationClient_StateGet(true).IsEmpty() )
 			AnimationClient_CacheNextStateMinInfo(
				false,
				__EAPT_BY_PRESSED_STATE_TURNED_OFF
				);
	}
e_ButtonType_t nBT = m_nTrackedButton;
e_ButtonType_t nHT = RgBtnHitTest( point );
	m_nTrackedButton = __BTT_NONE;
	m_bPressedTracking = false;
	RgBtnClearState( true, true );
	if( ! bAnimationLocked )
	{
 		AnimationClient_CacheNextStateMinInfo(
			true,
			__EAPT_BY_PRESSED_STATE_TURNED_OFF
			);
		AnimationClient_CacheGeneratorUnlock();
	}
	if( ::GetCapture() == GetSafeHwnd() )
		::ReleaseCapture();
	Invalidate();
	UpdateWindow();
	if( nBT != nHT )
		return;
CExtRibbonGalleryWnd * pRibbonGalleryWnd =
		DYNAMIC_DOWNCAST( CExtRibbonGalleryWnd, GetParent() );
	if( pRibbonGalleryWnd )
		pRibbonGalleryWnd->OnInplaceScrollBarButtonClick( nBT, false );
}

void CExtRibbonGalleryInplaceScrollBar::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	nFlags;
	point;
}

void CExtRibbonGalleryInplaceScrollBar::OnMButtonDown(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	nFlags;
	point;
}

void CExtRibbonGalleryInplaceScrollBar::OnMButtonUp(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	nFlags;
	point;
}

void CExtRibbonGalleryInplaceScrollBar::OnMButtonDblClk(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	nFlags;
	point;
}

void CExtRibbonGalleryInplaceScrollBar::OnRButtonDown(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	nFlags;
	point;
CExtRibbonGalleryWnd * pWndRibbonGallery =
		DYNAMIC_DOWNCAST( CExtRibbonGalleryWnd, GetParent() );
	if( pWndRibbonGallery == NULL )
		return;
	ASSERT_VALID( pWndRibbonGallery );
	if( pWndRibbonGallery->m_pRibbonGalleryTBB == NULL )
		return;
	ASSERT_VALID( pWndRibbonGallery->m_pRibbonGalleryTBB );
CExtRibbonButtonGallery * pRibbonGalleryTBB =
		pWndRibbonGallery->m_pRibbonGalleryTBB;
CExtRibbonPage * pRibbonPage =
		DYNAMIC_DOWNCAST(
			CExtRibbonPage,
			pWndRibbonGallery->m_pRibbonGalleryTBB->GetSafeBar()
			);
	if( pRibbonPage == NULL )
		return;
CExtRibbonBar * pRibbonBar =
		DYNAMIC_DOWNCAST(
			CExtRibbonBar,
			pRibbonPage
			);
	if( pRibbonBar == NULL )
	{
		pRibbonPage = pRibbonPage->GetMainRibbonPage();
		if( pRibbonPage == NULL )
			return;
		pRibbonBar =
			DYNAMIC_DOWNCAST(
				CExtRibbonBar,
				pRibbonPage
				);
		if( pRibbonBar == NULL )
			return;
		UINT nCmdID = pRibbonGalleryTBB->GetCmdID( false );
		INT nMainGalleryIndex = pRibbonBar->CommandToIndex( nCmdID );
		if( nMainGalleryIndex < 0 )
			return;
		pRibbonGalleryTBB =
			DYNAMIC_DOWNCAST(
				CExtRibbonButtonGallery,
				pRibbonBar->GetButton( nMainGalleryIndex )
				);
		if( pRibbonGalleryTBB == NULL )
			return;
	} // if( pRibbonBar == NULL )
	ASSERT_VALID( pRibbonBar );
	ClientToScreen( &point );
	pRibbonBar->ScreenToClient( &point );
	pRibbonBar->OnRibbonTrackButtonContextMenu(
		pRibbonGalleryTBB,
		nFlags,
		point
		);
}

void CExtRibbonGalleryInplaceScrollBar::OnRButtonUp(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	nFlags;
	point;
}

void CExtRibbonGalleryInplaceScrollBar::OnRButtonDblClk(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	nFlags;
	point;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonGalleryWnd

IMPLEMENT_DYNCREATE( CExtRibbonGalleryWnd, CExtToolBoxWnd );

CExtRibbonGalleryWnd::CExtRibbonGalleryWnd()
	: m_bInPlaceGallery( false )
	, m_bDisableMouseWheel( true )
	, m_pRibbonGalleryTBB( NULL )
{
	m_sizeToolBoxGetItemMarginsInIconViewMode.cx = 
		m_sizeToolBoxGetItemMarginsInIconViewMode.cy = 3;
}

CExtRibbonGalleryWnd::~CExtRibbonGalleryWnd()
{
}

BEGIN_MESSAGE_MAP( CExtRibbonGalleryWnd, CExtToolBoxWnd )
	//{{AFX_MSG_MAP(CExtRibbonGalleryWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CFont * CExtRibbonGalleryWnd::OnToolBoxWndQueryItemFont(
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI
	)
{
	ASSERT_VALID( this );
	ASSERT( pTBCI != NULL );
CFont * pFont =
		CExtAnimationSite::PmBridge_GetPM()->RibbonGallery_GetItemFont(
			this,
			pTBCI
			);
	if( pFont->GetSafeHandle() != NULL )
		return pFont;
	return CExtToolBoxWnd::OnToolBoxWndQueryItemFont( pTBCI );
}

CSize CExtRibbonGalleryWnd::OnToolBoxWndMasureItem(
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI,
	CDC & dc
	)
{
	ASSERT_VALID( this );
	ASSERT( pTBCI != NULL );
CSize _size =
		CExtAnimationSite::PmBridge_GetPM()->RibbonGallery_MeasureItem(
			dc,
			this,
			pTBCI
			);
	if( _size.cx < 0 || _size.cy < 0 )
		_size = CExtToolBoxWnd::OnToolBoxWndMasureItem( pTBCI, dc );
	if( _size.cx == 0 && _size.cy == 0 )
		return _size;
	if( _size.cy < 22 )
		_size.cy = 22;
	return _size;
}

void CExtRibbonGalleryWnd::OnToolBoxWndDrawItem(
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI,
	const CRect & rcItem,
	CDC & dc
	)
{
	ASSERT_VALID( this );
	ASSERT( pTBCI != NULL );
	if( pTBCI->AnimationClient_StatePaint( dc ) )
		return;
	if( CExtAnimationSite::PmBridge_GetPM()->RibbonGallery_DrawItem(
			dc,
			this,
			rcItem,
			pTBCI
			)
		)
		return;
	CExtToolBoxWnd::OnToolBoxWndDrawItem( pTBCI, rcItem, dc );
}

void CExtRibbonGalleryWnd::OnToolBoxWndEraseEntire(
	CDC & dc,
	const CRect & rcClient,
	const CRect & rcActiveChildArea,
	CRgn & rgnActiveChildArea,
	const CRect & rcActiveChildArea2,
	CRgn & rgnActiveChildArea2
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if(	CExtAnimationSite::PmBridge_GetPM()->RibbonGallery_EraseEntireBk(
			dc,
			rcClient,
			rcActiveChildArea,
			rgnActiveChildArea,
			rcActiveChildArea2,
			rgnActiveChildArea2,
			this
			)
		)
		return;
	CExtToolBoxWnd::OnToolBoxWndEraseEntire(
		dc,
		rcClient,
		rcActiveChildArea,
		rgnActiveChildArea,
		rcActiveChildArea2,
		rgnActiveChildArea2
		);
}

bool CExtRibbonGalleryWnd::ItemIsExpanded(
	const CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI
	) const
{
	ASSERT_VALID( this );
	pTBCI;
	return true;
}

bool CExtRibbonGalleryWnd::ItemExpand(
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI,
	int nExpandCode, // = TVE_EXPAND // TVE_COLLAPSE, TVE_EXPAND or TVE_TOGGLE
	bool bUpdateCtrlNow, // = false,
	bool bEnsureVisibleSelection // = false
	)
{
	ASSERT_VALID( this );
	nExpandCode;
	return
		CExtToolBoxWnd::ItemExpand(
			pTBCI,
			TVE_EXPAND,
			bUpdateCtrlNow,
			bEnsureVisibleSelection
			);
}

bool CExtRibbonGalleryWnd::ItemExpandAll(
	int nExpandCode, // = TVE_EXPAND // TVE_COLLAPSE, TVE_EXPAND or TVE_TOGGLE
	bool bUpdateCtrlNow, // = false
	bool bEnsureVisibleSelection // = false
	)
{
	ASSERT_VALID( this );
	nExpandCode;
	return
		CExtToolBoxWnd::ItemExpandAll(
			TVE_EXPAND,
			bUpdateCtrlNow,
			bEnsureVisibleSelection
			);
}

void CExtRibbonGalleryWnd::OnToolBoxWndItemSelEndOK(
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI
	)
{
	ASSERT_VALID( this );
	CExtToolBoxWnd::OnToolBoxWndItemSelEndOK( pTBCI );
CExtRibbonGalleryPopupMenuWnd * pGalleryPopup = NULL;
CExtRibbonPage * pRibbonPage = NULL;
	if( GetSafeHwnd() != NULL )
	{
		CWnd * pWnd = GetParent();
		if( pWnd->GetSafeHwnd() != NULL )
		{
			pGalleryPopup = DYNAMIC_DOWNCAST( CExtRibbonGalleryPopupMenuWnd, pWnd );
			if( pGalleryPopup != NULL )
				pRibbonPage = pGalleryPopup->GetMainRibbonPage();
			else
				pRibbonPage = DYNAMIC_DOWNCAST( CExtRibbonPage, pWnd );
		}
	}
	if( pRibbonPage == NULL )
		return;
//	if( pRibbonPage->_GetIndexOf( m_pRibbonGalleryTBB ) < 0 )
//		return;
	if( m_pRibbonGalleryTBB == NULL )
	{
		if( pGalleryPopup == NULL )
			return;
		CExtRibbonPage * pRibbonPage = pGalleryPopup->GetMainRibbonPage();
		if( pRibbonPage == NULL )
			return;
		pRibbonPage->OnRibbonGalleryItemSelEndOK( *this, pGalleryPopup, NULL, pTBCI );
		return;
	} // if( m_pRibbonGalleryTBB == NULL )
	ASSERT_VALID( m_pRibbonGalleryTBB );
	ASSERT_VALID( m_pRibbonGalleryTBB->GetRibbonPage() );
	m_pRibbonGalleryTBB->OnRibbonGalleryItemSelEndOK( *this, pGalleryPopup, pTBCI );
}

void CExtRibbonGalleryWnd::OnToolBoxWndItemInsert(
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI
	)
{
	ASSERT_VALID( this );
	CExtToolBoxWnd::OnToolBoxWndItemInsert( pTBCI );
CExtRibbonGalleryPopupMenuWnd * pGalleryPopup = NULL;
CExtRibbonPage * pRibbonPage = NULL;
	if( GetSafeHwnd() != NULL )
	{
		CWnd * pWnd = GetParent();
		if( pWnd->GetSafeHwnd() != NULL )
		{
			pGalleryPopup = DYNAMIC_DOWNCAST( CExtRibbonGalleryPopupMenuWnd, pWnd );
			if( pGalleryPopup != NULL )
				pRibbonPage = pGalleryPopup->GetMainRibbonPage();
			else
				pRibbonPage = DYNAMIC_DOWNCAST( CExtRibbonPage, pWnd );
		}
	}
	if( pRibbonPage == NULL )
		return;
//	if( pRibbonPage->_GetIndexOf( m_pRibbonGalleryTBB ) < 0 )
//		return;
	if( m_pRibbonGalleryTBB == NULL )
	{
		if( pGalleryPopup == NULL )
			return;
		CExtRibbonPage * pRibbonPage = pGalleryPopup->GetMainRibbonPage();
		if( pRibbonPage == NULL )
			return;
		pRibbonPage->OnRibbonGalleryItemInsert( *this, pGalleryPopup, NULL, pTBCI );
		return;
	} // if( m_pRibbonGalleryTBB == NULL )
	ASSERT_VALID( m_pRibbonGalleryTBB );
	ASSERT_VALID( m_pRibbonGalleryTBB->GetRibbonPage() );
	m_pRibbonGalleryTBB->OnRibbonGalleryItemInsert( *this, pGalleryPopup, pTBCI );
}

void CExtRibbonGalleryWnd::OnToolBoxWndItemRemove(
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI
	)
{
	ASSERT_VALID( this );
	CExtToolBoxWnd::OnToolBoxWndItemRemove( pTBCI );
CExtRibbonGalleryPopupMenuWnd * pGalleryPopup = NULL;
CExtRibbonPage * pRibbonPage = NULL;
	if( GetSafeHwnd() != NULL )
	{
		CWnd * pWnd = GetParent();
		if( pWnd->GetSafeHwnd() != NULL )
		{
			pGalleryPopup = DYNAMIC_DOWNCAST( CExtRibbonGalleryPopupMenuWnd, pWnd );
			if( pGalleryPopup != NULL )
				pRibbonPage = pGalleryPopup->GetMainRibbonPage();
			else
				pRibbonPage = DYNAMIC_DOWNCAST( CExtRibbonPage, pWnd );
		}
	}
	if( pRibbonPage == NULL )
		return;
//	if( pRibbonPage->_GetIndexOf( m_pRibbonGalleryTBB ) < 0 )
//		return;
	if( m_pRibbonGalleryTBB == NULL )
	{
		if( pGalleryPopup == NULL )
			return;
		CExtRibbonPage * pRibbonPage = pGalleryPopup->GetMainRibbonPage();
		if( pRibbonPage == NULL )
			return;
		pRibbonPage->OnRibbonGalleryItemRemove( *this, pGalleryPopup, NULL, pTBCI );
		return;
	} // if( m_pRibbonGalleryTBB == NULL )
	ASSERT_VALID( m_pRibbonGalleryTBB );
	ASSERT_VALID( m_pRibbonGalleryTBB->GetRibbonPage() );
	m_pRibbonGalleryTBB->OnRibbonGalleryItemRemove( *this, pGalleryPopup, pTBCI );
}

void CExtRibbonGalleryWnd::OnToolBoxWndItemActivate(
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_Old,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_New
	)
{
	ASSERT_VALID( this );
	CExtToolBoxWnd::OnToolBoxWndItemActivate( pTBCI_Old, pTBCI_New );
CExtRibbonGalleryPopupMenuWnd * pGalleryPopup = NULL;
CExtRibbonPage * pRibbonPage = NULL;
	if( GetSafeHwnd() != NULL )
	{
		CWnd * pWnd = GetParent();
		if( pWnd->GetSafeHwnd() != NULL )
		{
			pGalleryPopup = DYNAMIC_DOWNCAST( CExtRibbonGalleryPopupMenuWnd, pWnd );
			if( pGalleryPopup != NULL )
				pRibbonPage = pGalleryPopup->GetMainRibbonPage();
			else
				pRibbonPage = DYNAMIC_DOWNCAST( CExtRibbonPage, pWnd );
		}
	}
	if( pRibbonPage == NULL )
		return;
//	if( pRibbonPage->_GetIndexOf( m_pRibbonGalleryTBB ) < 0 )
//		return;
	if( m_pRibbonGalleryTBB == NULL )
	{
		if( pGalleryPopup == NULL )
			return;
		CExtRibbonPage * pRibbonPage = pGalleryPopup->GetMainRibbonPage();
		if( pRibbonPage == NULL )
			return;
		pRibbonPage->OnRibbonGalleryItemActivate( *this, pGalleryPopup, NULL, pTBCI_Old, pTBCI_New );
		return;
	} // if( m_pRibbonGalleryTBB == NULL )
	ASSERT_VALID( m_pRibbonGalleryTBB );
	ASSERT_VALID( m_pRibbonGalleryTBB->GetRibbonPage() );
	m_pRibbonGalleryTBB->OnRibbonGalleryItemActivate( *this, pGalleryPopup, pTBCI_Old, pTBCI_New );
}

void CExtRibbonGalleryWnd::OnToolBoxWndItemSelChange(
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_Old,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_New
	)
{
	ASSERT_VALID( this );
	CExtToolBoxWnd::OnToolBoxWndItemSelChange( pTBCI_Old, pTBCI_New );
CExtRibbonGalleryPopupMenuWnd * pGalleryPopup = NULL;
CExtRibbonPage * pRibbonPage = NULL;
	if( GetSafeHwnd() != NULL )
	{
		CWnd * pWnd = GetParent();
		if( pWnd->GetSafeHwnd() != NULL )
		{
			pGalleryPopup = DYNAMIC_DOWNCAST( CExtRibbonGalleryPopupMenuWnd, pWnd );
			if( pGalleryPopup != NULL )
				pRibbonPage = pGalleryPopup->GetMainRibbonPage();
			else
				pRibbonPage = DYNAMIC_DOWNCAST( CExtRibbonPage, pWnd );
		}
	}
	if( pRibbonPage == NULL )
		return;
//	if( pRibbonPage->_GetIndexOf( m_pRibbonGalleryTBB ) < 0 )
//		return;
	if( m_pRibbonGalleryTBB == NULL )
	{
		if( pGalleryPopup == NULL )
			return;
		CExtRibbonPage * pRibbonPage = pGalleryPopup->GetMainRibbonPage();
		if( pRibbonPage == NULL )
			return;
		pRibbonPage->OnRibbonGalleryItemSelChange( *this, pGalleryPopup, NULL, pTBCI_Old, pTBCI_New );
		return;
	} // if( m_pRibbonGalleryTBB == NULL )
	ASSERT_VALID( m_pRibbonGalleryTBB );
	ASSERT_VALID( m_pRibbonGalleryTBB->GetRibbonPage() );
	m_pRibbonGalleryTBB->OnRibbonGalleryItemSelChange( *this, pGalleryPopup, pTBCI_Old, pTBCI_New );
}

void CExtRibbonGalleryWnd::OnToolBoxWndItemHoverChange(
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_Old,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_New
	)
{
	ASSERT_VALID( this );
	CExtToolBoxWnd::OnToolBoxWndItemHoverChange( pTBCI_Old, pTBCI_New );
CExtRibbonGalleryPopupMenuWnd * pGalleryPopup = NULL;
CExtRibbonPage * pRibbonPage = NULL;
	if( GetSafeHwnd() != NULL )
	{
		CWnd * pWnd = GetParent();
		if( pWnd->GetSafeHwnd() != NULL )
		{
			pGalleryPopup = DYNAMIC_DOWNCAST( CExtRibbonGalleryPopupMenuWnd, pWnd );
			if( pGalleryPopup != NULL )
				pRibbonPage = pGalleryPopup->GetMainRibbonPage();
			else
				pRibbonPage = DYNAMIC_DOWNCAST( CExtRibbonPage, pWnd );
		}
	}
	if( pRibbonPage == NULL )
		return;
//	if( pRibbonPage->_GetIndexOf( m_pRibbonGalleryTBB ) < 0 )
//		return;
	if( m_pRibbonGalleryTBB == NULL )
	{
		if( pGalleryPopup == NULL )
			return;
		if( pRibbonPage == NULL )
			return;
		pRibbonPage->OnRibbonGalleryItemHoverChange( *this, pGalleryPopup, NULL, pTBCI_Old, pTBCI_New );
		return;
	}
	ASSERT_VALID( m_pRibbonGalleryTBB );
	ASSERT_VALID( m_pRibbonGalleryTBB->GetRibbonPage() );
	m_pRibbonGalleryTBB->OnRibbonGalleryItemHoverChange( *this, pGalleryPopup, pTBCI_Old, pTBCI_New );
}

bool CExtRibbonGalleryWnd::_IsKeyFocusGallery()
{
	ASSERT_VALID( this );
	if(		( ! _IsInplaceGallery() )
		||	m_pRibbonGalleryTBB == NULL
		)
		return false;
	ASSERT_VALID( m_pRibbonGalleryTBB );
CExtRibbonPage * pRibbonPage = m_pRibbonGalleryTBB->GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
INT nFlatTrackingIndex = pRibbonPage->_FlatTrackingIndexGet();
	if( nFlatTrackingIndex < 0 )
		return false;
INT nOwnIndex = pRibbonPage->_GetIndexOf( m_pRibbonGalleryTBB );
	if( nOwnIndex != nFlatTrackingIndex )
		return false;
	return true;
}

bool CExtRibbonGalleryWnd::_IsKeyFocusGallery() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtRibbonGalleryWnd * > ( this ) ) ->
		_IsKeyFocusGallery();
}

bool CExtRibbonGalleryWnd::_IsInplaceGallery()
{
	ASSERT_VALID( this );
	return m_bInPlaceGallery;
}

bool CExtRibbonGalleryWnd::_IsInplaceGallery() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtRibbonGalleryWnd * > ( this ) ) ->
		_IsInplaceGallery();
}

CRect CExtRibbonGalleryWnd::OnSwGetClientRect() const
{
	ASSERT_VALID( this );
CRect rcClient = CExtToolBoxWnd::OnSwGetClientRect();
	CExtAnimationSite::PmBridge_GetPM() ->
		RibbonGallery_AdjustClientRect( rcClient, this );
	return rcClient;
}

void CExtRibbonGalleryWnd::_SetToolBoxFocus()
{
	ASSERT_VALID( this );
	if( ! _IsInplaceGallery() )
		return;
	CExtToolBoxWnd::_SetToolBoxFocus();
}

bool CExtRibbonGalleryWnd::_CreateScrollBar()
{
	ASSERT_VALID( this );
	if( m_bInPlaceGallery )
	{
		m_wndScrollBarV.m_eSO = CExtScrollBar::__ESO_RIGHT;
		if( ! m_wndScrollBarV.Create(
				WS_CHILD|WS_VISIBLE|SBS_VERT|SBS_RIGHTALIGN,
				CRect(0,0,0,0),
				this,
				1
				)
			)
		{
			ASSERT( FALSE );
			return false;
		}
		return true;
	} // if( m_bInPlaceGallery )
	return CExtToolBoxWnd::_CreateScrollBar();
}

CScrollBar* CExtRibbonGalleryWnd::GetScrollBarCtrl(int nBar) const
{
	ASSERT_VALID( this );
	if( ! m_bInPlaceGallery )
		return CExtToolBoxWnd::GetScrollBarCtrl( nBar );
	if( m_hWnd == NULL || (! ::IsWindow(m_hWnd) ) )
		return NULL;
	ASSERT( nBar == SB_HORZ || nBar == SB_VERT );
	if( nBar == SB_VERT )
	{
		if( m_wndScrollBarV.GetSafeHwnd() != NULL )
			return ( const_cast < CExtRibbonGalleryInplaceScrollBar * > ( &m_wndScrollBarV ) );
	} // if( nBar == SB_VERT )
	return NULL;
}

bool CExtRibbonGalleryWnd::OnInplaceScrollBarButtonClick(
	CExtRibbonGalleryInplaceScrollBar::e_ButtonType_t nBT,
	bool bLButtonDown
	)
{
	ASSERT_VALID( this );
	if( bLButtonDown )
		return false;
	if( nBT != CExtRibbonGalleryInplaceScrollBar::__BTT_MENU )
		return false;
	if( m_pRibbonGalleryTBB == NULL )
		return false;
	ASSERT_VALID( m_pRibbonGalleryTBB );
	ASSERT_VALID( m_pRibbonGalleryTBB->GetRibbonPage() );
	m_pRibbonGalleryTBB->OnTrackPopup( CPoint(-32767,-32767), false, false );
	return true;
}

bool CExtRibbonGalleryWnd::OnToolBoxWndCanHandleHover() const
{
	ASSERT_VALID( this );
	return true;
}

bool CExtRibbonGalleryWnd::OnSwDoMouseWheel(
	UINT fFlags,
	short zDelta,
	CPoint point
	)
{
	ASSERT_VALID( this );
	if( m_bDisableMouseWheel )
		return false;
	return CExtToolBoxWnd::OnSwDoMouseWheel( fFlags, zDelta, point );
}

bool CExtRibbonGalleryWnd::OnSwCanAutoHideScrollBar( bool bHorz ) const
{
	ASSERT_VALID( this );
	bHorz;
	return false;
}

void CExtRibbonGalleryWnd::OnSwUpdateScrollBars()
{
	ASSERT_VALID( this );
	CExtToolBoxWnd::OnSwUpdateScrollBars();
	if(		m_bInPlaceGallery
		&&	m_wndScrollBarV.GetSafeHwnd() != NULL
		)
		m_wndScrollBarV.PostMessage( WM_IDLEUPDATECMDUI );
}

bool CExtRibbonGalleryWnd::OnSwDoScrollBy(
	CSize sizeScroll,
	bool bDoScroll // = true
	)
{
	ASSERT_VALID( this );
bool bRetVal = CExtToolBoxWnd::OnSwDoScrollBy( sizeScroll, bDoScroll );
	if(		m_bInPlaceGallery
		&&	m_wndScrollBarV.GetSafeHwnd() != NULL
		)
		m_wndScrollBarV.PostMessage( WM_IDLEUPDATECMDUI );
	return bRetVal;
}

BOOL CExtRibbonGalleryWnd::PreTranslateMessage(MSG* pMsg)
{
	return CExtToolBoxWnd::PreTranslateMessage( pMsg );
}

void CExtRibbonGalleryWnd::PreSubclassWindow()
{
	CExtToolBoxWnd::PreSubclassWindow();
}

void CExtRibbonGalleryWnd::PostNcDestroy()
{
	CExtToolBoxWnd::PostNcDestroy();
}

LRESULT CExtRibbonGalleryWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if( message == WM_MOUSEACTIVATE )
		return MA_NOACTIVATE;
	if( message == WM_RBUTTONDOWN && m_pRibbonGalleryTBB != NULL )
	{
		ASSERT_VALID( m_pRibbonGalleryTBB );
		CExtRibbonButtonGallery * pRibbonGalleryTBB =
			m_pRibbonGalleryTBB;
		CExtRibbonPage * pRibbonPage =
			DYNAMIC_DOWNCAST(
				CExtRibbonPage,
				m_pRibbonGalleryTBB->GetSafeBar()
				);
		CExtRibbonBar * pRibbonBar =
			DYNAMIC_DOWNCAST(
				CExtRibbonBar,
				pRibbonPage
				);
		if( pRibbonBar == NULL )
		{
			pRibbonPage = pRibbonPage->GetMainRibbonPage();
			if( pRibbonPage != NULL )
			{
				pRibbonBar =
					DYNAMIC_DOWNCAST(
						CExtRibbonBar,
						pRibbonPage
						);
				if( pRibbonBar != NULL )
				{
					UINT nCmdID = pRibbonGalleryTBB->GetCmdID( false );
					INT nMainGalleryIndex = pRibbonBar->CommandToIndex( nCmdID );
					if( nMainGalleryIndex >= 0 )
					{
						pRibbonGalleryTBB =
							DYNAMIC_DOWNCAST(
								CExtRibbonButtonGallery,
								pRibbonBar->GetButton( nMainGalleryIndex )
								);
					} // if( nMainGalleryIndex >= 0 )
				} // if( pRibbonBar != NULL )
			} // if( pRibbonPage != NULL )
		} // if( pRibbonBar == NULL )
		if( pRibbonBar != NULL && pRibbonGalleryTBB )
		{
			ASSERT_VALID( pRibbonBar );
			CPoint point( short(LOWORD(lParam)), short(HIWORD(lParam)) );
			ClientToScreen( &point );
			pRibbonBar->ScreenToClient( &point );
			pRibbonBar->OnRibbonTrackButtonContextMenu(
				pRibbonGalleryTBB,
				UINT( wParam ),
				point
				);
		} // if( pRibbonBar != NULL && pRibbonGalleryTBB )
		return 0L;
	} // if( message == WM_RBUTTONDOWN && m_pRibbonGalleryTBB != NULL )
LRESULT lResult = CExtToolBoxWnd::WindowProc( message, wParam, lParam );
	return lResult;
}

BOOL CExtRibbonGalleryWnd::PreCreateWindow(CREATESTRUCT& cs)
{
	return CExtToolBoxWnd::PreCreateWindow( cs );
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonNodeGallery

IMPLEMENT_SERIAL( CExtRibbonNodeGallery, CExtRibbonNode, VERSIONABLE_SCHEMA|1 );

CExtRibbonNodeGallery::CExtRibbonNodeGallery(
	UINT nCmdId, // = 0L
	CExtRibbonNode * pParentNode, // = NULL
	DWORD dwFlags, // = 0L
	__EXT_MFC_SAFE_LPCTSTR strTextInToolbar, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strTextInMenu, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strTextUser, // = NULL
	LPARAM lParam // = 0L
	)
	: CExtRibbonNode(
		nCmdId,
		nCmdId,
		pParentNode,
		dwFlags,
		strTextInToolbar,
		strTextInMenu,
		strTextUser,
		lParam
		)
{
	_InitMembers();
}

CExtRibbonNodeGallery::CExtRibbonNodeGallery(
	CExtRibbonNodeGallery & other
	)
	: CExtRibbonNode( other )
{
	_InitMembers();
}

CExtRibbonNodeGallery::~CExtRibbonNodeGallery()
{
}

void CExtRibbonNodeGallery::AssignFromOther(
	CExtCustomizeCmdTreeNode & other
	)
{
	ASSERT_VALID( this );
	CExtRibbonNode::AssignFromOther( other );
	if( m_nCmdIdBasic == 0 )
		m_nCmdIdBasic = m_nCmdIdEffective;
	if( m_nCmdIdEffective == 0 )
		m_nCmdIdEffective = m_nCmdIdBasic;
CExtRibbonNodeGallery * pOther = DYNAMIC_DOWNCAST( CExtRibbonNodeGallery, (&other) );
	if( pOther != NULL )
	{
		m_sizePopupGalleryControl = pOther->m_sizePopupGalleryControl;
		m_sizePopupGalleryControlMin = pOther->m_sizePopupGalleryControlMin;
		m_sizePopupGalleryControlMax = pOther->m_sizePopupGalleryControlMax;
	} // if( pOther != NULL )
	else
		_InitMembers();
	ASSERT_VALID( this );
}

void CExtRibbonNodeGallery::_InitMembers()
{
	ASSERT_VALID( this );
	CExtRibbonNode::_InitMembers();
	m_sizePopupGalleryControl = CSize( 382, 315 );
	m_sizePopupGalleryControlMin = CSize( 40, 60 );
	m_sizePopupGalleryControlMax = CSize( -1, -1 );
// 	m_arrILEtoILV.RemoveAll();
// 	m_arrILEtoILV.SetSize( 3 );
// 	m_arrILEtoILV.SetAt(
// 		0,
// 		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
// 			__EXT_RIBBON_ILE_MIN+2,
// 			__EXT_RIBBON_ILV_SIMPLE_LARGE,
// 			false
// 			)
// 		);
// 	m_arrILEtoILV.SetAt(
// 		1,
// 		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
// 			__EXT_RIBBON_ILE_MIN+1,
// 			__EXT_RIBBON_ILV_SIMPLE_NORMAL,
// 			false
// 			)
// 		);
// 	m_arrILEtoILV.SetAt(
// 		2,
// 		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
// 			__EXT_RIBBON_ILE_MIN,
// 			__EXT_RIBBON_ILV_SIMPLE_SMALL,
// 			false
// 			)
// 		);
// 
// 	m_arrILEtoILV.InsertAt(
// 		0,
// 		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
// 			__EXT_RIBBON_ILE_MIN+3,
// 			100,
// 			false
// 			)
// 		);
// 	m_arrILEtoILV.InsertAt(
// 		0,
// 		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
// 			__EXT_RIBBON_ILE_MIN+4,
// 			200,
// 			false
// 			)
// 		);
// 	m_arrILEtoILV.InsertAt(
// 		0,
// 		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
// 			__EXT_RIBBON_ILE_MIN+5,
// 			300,
// 			false
// 			)
// 		);
// 	m_arrILEtoILV.InsertAt(
// 		0,
// 		__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
// 			__EXT_RIBBON_ILE_MIN+6,
// 			400,
// 			false
// 			)
// 		);
// 	m_nRIL_VisualCurrent = m_nRIL_VisualMax = 400;
}

CRuntimeClass * CExtRibbonNodeGallery::_OnRibbonGetButtonRTC()
{
	ASSERT_VALID( this );
	return ( RUNTIME_CLASS( CExtRibbonButtonGallery ) );
}

CExtBarButton * CExtRibbonNodeGallery::OnRibbonCreateBarButton(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	return CExtRibbonNode::OnRibbonCreateBarButton( pBar, pParentTBB );
}

bool CExtRibbonNodeGallery::Ribbon_InitBar(
	CExtRibbonPage * pBar,
	CExtBarButton * pParentTBB, // = NULL
	bool bInsertThisNode // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
//	return CExtRibbonNode::Ribbon_InitBar( pBar, pParentTBB, bInsertThisNode );
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
#ifdef _DEBUG
	if( pParentTBB != NULL )
	{
		ASSERT_VALID( pParentTBB );
	}
#endif // _DEBUG
CExtBarButton * pTBB = NULL;
	if( bInsertThisNode )
	{
		pTBB = OnRibbonCreateBarButton( pBar, pParentTBB );
		if( pTBB != NULL )
		{
			if( ! pBar->InsertSpecButton( -1, pTBB, FALSE ) )
			{
				if( pTBB != NULL )
					delete pTBB;
				return false;
			}
		}
	}
	return true;
}

void CExtRibbonNodeGallery::RibbonILV_Set(
	INT nILV,
	INT nType // = 0 // -1 min, 0 current, 1 - max
	)
{
	ASSERT_VALID( this );
	CExtRibbonNode::RibbonILV_Set( nILV, nType );
}

INT CExtRibbonNodeGallery::RibbonILV_fromILE(
	INT nILE,
	bool * p_bIsWrap // = NULL
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( this );
	ASSERT(
			__EXT_RIBBON_ILE_MIN <= nILE
		&&	nILE <= (__EXT_RIBBON_ILE_MAX+1)
		);
	if( p_bIsWrap != NULL )
		(*p_bIsWrap) = false;
INT nILV = CExtRibbonNode::RibbonILV_fromILE( nILE, p_bIsWrap );
	return nILV;
}

void CExtRibbonNodeGallery::Serialize( CArchive & ar )
{
	CExtRibbonNode::Serialize( ar );
	if( ar.IsStoring() )
	{
		ar << m_sizePopupGalleryControl;
		ar << m_sizePopupGalleryControlMin;
		ar << m_sizePopupGalleryControlMax;
	} // if( ar.IsStoring() )
	else
	{
		ar >> m_sizePopupGalleryControl;
		ar >> m_sizePopupGalleryControlMin;
		ar >> m_sizePopupGalleryControlMax;
	} // else from if( ar.IsStoring() )
}

void CExtRibbonNodeGallery::PostConstructMenuItem(
	LPVOID pMenuItemData,
	HWND hWndCmdRecv,
	int nItemIndex,
	CExtPopupMenuWnd * pPopupParent
	)
{
	ASSERT_VALID( this );
	hWndCmdRecv;
	nItemIndex;
	pPopupParent;
CExtPopupMenuWnd::MENUITEMDATA & _mi = *((CExtPopupMenuWnd::MENUITEMDATA*)pMenuItemData);
CExtPopupMenuWnd * pOwnerPopup = _mi.GetOwner();
	ASSERT_VALID( pOwnerPopup );
HWND hWndRibbonPage = hWndCmdRecv;
CWnd * pWnd = CWnd::FromHandlePermanent( hWndRibbonPage );
	if(		pWnd == NULL
		||	( ! pWnd->IsKindOf( RUNTIME_CLASS( CExtRibbonPage ) ) )
		)
	{
		hWndRibbonPage = pOwnerPopup->m_hWndNotifyMenuClosed;
		if( hWndRibbonPage == NULL )
		{
			CExtPopupMenuWnd * pPopup = pPopupParent; //pOwnerPopup->GetParentMenuWnd();
			for( ; pPopup != NULL; pPopup = pPopup->GetParentMenuWnd() )
			{
				hWndRibbonPage = pPopup->m_hWndNotifyMenuClosed;
				if( hWndRibbonPage != NULL )
					break;
			}
			if( hWndRibbonPage == NULL )
				return;
		}
		pWnd = CWnd::FromHandlePermanent( hWndRibbonPage );
		if(		pWnd == NULL
			||	( ! pWnd->IsKindOf( RUNTIME_CLASS( CExtRibbonPage ) ) )
			)
			return;
	}
CExtRibbonGalleryPopupMenuWnd * pPopup =
		new CExtRibbonGalleryPopupMenuWnd( hWndRibbonPage, NULL );
	pPopup->m_sizeChildControl = m_sizePopupGalleryControl;
	pPopup->m_sizeChildControlMin = m_sizePopupGalleryControlMin;
	pPopup->m_sizeChildControlMax = m_sizePopupGalleryControlMax;
INT nWidthSysVSB = ::GetSystemMetrics( SM_CXVSCROLL );
CRect rcMB = g_PaintManager->GetMenuBorderMetrics( pPopup ); // pPopup->OnQueryMenuBorderMetrics();
	pPopup->m_sizeChildControl.cx += nWidthSysVSB;// - nWidthOwnVSB;
	pPopup->m_sizeChildControl.cx += rcMB.left + rcMB.right; // + nMenuShadowSize;
	pPopup->_RecalcMinMaxResizingSizes();
	_mi.SetSpecPopup( pPopup );
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonButtonGallery

IMPLEMENT_DYNCREATE( CExtRibbonButtonGallery, CExtRibbonButton );

CExtRibbonButtonGallery::CExtRibbonButtonGallery(
	CExtRibbonPage * pBar, // = NULL
	UINT nCmdID, // = ID_SEPARATOR
	UINT nStyle // = 0
	)
	: CExtRibbonButton(
		pBar,
		nCmdID,
		nStyle
		)
	, m_bGalleryPopupVisible( false )
{
	m_wndIG.m_bInPlaceGallery = true;
	m_wndIG.m_pRibbonGalleryTBB = this;
	InplaceGalleryEnsureCreated();
}

CExtRibbonButtonGallery::~CExtRibbonButtonGallery()
{
}

CSize CExtRibbonButtonGallery::RibbonILV_CalcSize(
	CDC & dc,
	INT nILV // = -1 // -1 use current visual level
	) const
{
	ASSERT_VALID( this );
	if( IsInplaceGalleryMode() )
	{
		INT nILV = RibbonILV_Get();
		if( nILV > __EXT_RIBBON_ILV_SIMPLE_MAX )
		{
			CExtPaintManager * pPM = GetBar()->PmBridge_GetPM();
			ASSERT_VALID( pPM );
			CSize _size( nILV, 52 );
			_size.cx = pPM->UiScalingDo( _size.cx, CExtPaintManager::__EUIST_X );
			_size.cy = pPM->UiScalingDo( _size.cy, CExtPaintManager::__EUIST_Y );
			_size.cy += 8;
			return _size;
		}
	} // if( IsInplaceGalleryMode() )
CSize _size = CExtRibbonButton::RibbonILV_CalcSize( dc, nILV );
	return _size;
}

CSize CExtRibbonButtonGallery::CalculateLayout(
	CDC & dc,
	CSize sizePreCalc,
	BOOL bHorz
	)
{
	ASSERT_VALID( this );
CSize _size = CExtRibbonButton::CalculateLayout( dc, sizePreCalc, bHorz );
	return _size;
}

void CExtRibbonButtonGallery::PaintCompound(
	CDC & dc,
	bool bPaintParentChain,
	bool bPaintChildren,
	bool bPaintOneNearestChildrenLevelOnly
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() );
	if( IsInplaceGalleryMode() )
		return;
	CExtRibbonButton::PaintCompound( dc, bPaintParentChain, bPaintChildren, bPaintOneNearestChildrenLevelOnly );
}

void CExtRibbonButtonGallery::SetBar( CExtToolControlBar * pBar )
{
	ASSERT_VALID( this );
	CExtRibbonButton::SetBar( pBar );
	if( GetCmdID() != ID_SEPARATOR )
		InplaceGalleryEnsureCreated();
}

bool CExtRibbonButtonGallery::IsPressed() const
{
	ASSERT_VALID( this );
	if( m_bGalleryPopupVisible )
		return true;
	return CExtRibbonButton::IsPressed();
}

void CExtRibbonButtonGallery::OnGalleryInitContent(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&wndRG) );
	ASSERT( wndRG.GetSafeHwnd() != NULL );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
	ASSERT( pRibbonPage->GetSafeHwnd() != NULL );
#ifdef _DEBUG
	if( pGalleryPopup != NULL )
	{
		ASSERT_VALID( pGalleryPopup );
		ASSERT( pGalleryPopup->GetSafeHwnd() != NULL );
	}
#endif // _DEBUG
	pRibbonPage->OnRibbonGalleryInitContent(
		wndRG,
		pGalleryPopup,
		this
		);
}

void CExtRibbonButtonGallery::OnGalleryPopupShow(
	CExtRibbonGalleryPopupMenuWnd * pPopup,
	bool bShow
	)
{
	ASSERT_VALID( this );
	pPopup;
	m_bGalleryPopupVisible = bShow;
	RedrawButton();
	if( ! m_bGalleryPopupVisible )
		SetHover( false );
}

bool CExtRibbonButtonGallery::IsAbleToTrackMenu(
	bool bCustomizeMode // = false
	) const
{
	ASSERT_VALID( this );
	bCustomizeMode;
	return true;
}

UINT CExtRibbonButtonGallery::OnTrackPopup(
	CPoint point,
	bool bSelectAny,
	bool bForceNoAnimation
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
	bForceNoAnimation;
bool bDockSiteCustomizeMode =
		pRibbonPage->_IsDockSiteCustomizeMode();
	if( bDockSiteCustomizeMode )
		return UINT(-1L);
	if(		IsDisabled()
		&&	(! CanBePressedInDisabledState() )
		)
		return UINT(-1L);
	if( ! IsAbleToTrackMenu() )
		return UINT(-1L);
CExtCustomizeCmdTreeNode * pNode = GetCmdNode();
	if( pNode == NULL )
		return UINT(-1L);
	ASSERT_VALID( pNode );
CExtRibbonNodeGallery * pGalleryNode =
		DYNAMIC_DOWNCAST( CExtRibbonNodeGallery, pNode );
	if( pGalleryNode == NULL )
		return UINT(-1L);
bool bPrevTBMT = CExtToolControlBar::g_bMenuTracking;
	if(		CExtToolControlBar::g_bMenuTracking
		&&	pRibbonPage->_GetIndexOf(this) ==
				pRibbonPage->GetMenuTrackingButton()
		)
		return UINT(-1L);
	CExtToolControlBar::_CloseTrackingMenus();
CWnd * pWndCmdTarget = GetCmdTargetWnd();
	ASSERT_VALID( pWndCmdTarget );
	CExtToolControlBar::g_bMenuTracking = bPrevTBMT;
CRect rcBtn = Rect();
bool bInplaceMode = false;
	if(		point.x == -32767
		&&	point.y == -32767
		&&	RibbonILV_Get() > __EXT_RIBBON_ILV_SIMPLE_MAX
		&&	InplaceGalleryGet().GetSafeHwnd() != NULL
		&&	( InplaceGalleryGet().GetStyle() & WS_VISIBLE ) != 0
		)
	{
		bInplaceMode = true;
		point = rcBtn.TopLeft();
		pRibbonPage->ClientToScreen( &point );
	}
	else
	{
		pRibbonPage->ClientToScreen( &rcBtn );
		pRibbonPage->ClientToScreen( &point );
	}
DWORD dwTrackFlags =
		OnGetTrackPopupFlags()
			| TPMX_COMBINE_NONE
			| TPMX_OWNERDRAW_FIXED
			| TPMX_NO_HIDE_RARELY
			| TPMX_FORCE_NO_ANIMATION
			| TPMX_RIBBON_MODE
			;
	if( bSelectAny )
		dwTrackFlags |= TPMX_SELECT_ANY;
	pRibbonPage->_SwitchMenuTrackingIndex(
		pRibbonPage->_GetIndexOf( this )
		);
HWND hWndTrack = GetCmdTargetWnd()->GetSafeHwnd();
CExtRibbonGalleryPopupMenuWnd * pPopup =
		new CExtRibbonGalleryPopupMenuWnd( pRibbonPage->m_hWnd, this );
	pPopup->m_sizeChildControl = pGalleryNode->m_sizePopupGalleryControl;
	pPopup->m_sizeChildControlMin = pGalleryNode->m_sizePopupGalleryControlMin;
	pPopup->m_sizeChildControlMax = pGalleryNode->m_sizePopupGalleryControlMax;
INT nWidthSysVSB = ::GetSystemMetrics( SM_CXVSCROLL );
CRect rcMB = g_PaintManager->GetMenuBorderMetrics( pPopup ); // pPopup->OnQueryMenuBorderMetrics();
	if( bInplaceMode )
	{
		point.x -= rcMB.left;
		point.y -= rcMB.top;
		CRect rcOwn = *this;
		pPopup->m_sizeChildControl.cx = rcOwn.Width() /*- nWidthOwnVSB*/;
	}
	else
		pPopup->m_sizeChildControl.cx += nWidthSysVSB;// - nWidthOwnVSB;
	pPopup->m_sizeChildControl.cx += rcMB.left + rcMB.right; // + nMenuShadowSize;

	pPopup->m_hWndNotifyMenuClosed = pRibbonPage->m_hWnd;
	if(		( ! pPopup->CreatePopupMenu( hWndTrack ) )
		||	(! pPopup->UpdateFromCmdTree(
				hWndTrack,
				GetCmdNode()
				) )
		)
	{
		delete pPopup;
		CExtToolControlBar::_CloseTrackingMenus();
		return UINT(-1L);
	}
	pPopup->_RecalcMinMaxResizingSizes();
	pPopup->m_wndRibbonGallery.m_pRibbonGalleryTBB = this;
	pPopup->m_hWndNotifyMenuClosed = pRibbonPage->GetSafeHwnd();
	CExtPaintManager::stat_PassPaintMessages();
	if( GetSeparatedDropDown() )
		m_bDropDownHT = true;
	g_pTrackingMenuTBB = this;
	if( ! pPopup->TrackPopupMenu(
			dwTrackFlags,
			point.x,
			point.y,
			bInplaceMode ? NULL : (&rcBtn),
			bInplaceMode ? NULL : pRibbonPage,
			bInplaceMode ? NULL : CExtToolControlBar::_CbPaintCombinedContent,
			NULL,
			bInplaceMode ? false : true
			)
		)
	{
		g_pTrackingMenuTBB = NULL;
		//delete pPopup;
		CExtToolControlBar::_CloseTrackingMenus();
		return UINT(-1L);
	}
	CExtToolControlBar::g_bMenuTracking = true;
	pRibbonPage->_SwitchMenuTrackingIndex(
		pRibbonPage->_GetIndexOf( this )
		);
	return UINT(-1L);
}

CWnd * CExtRibbonButtonGallery::CtrlGet()
{
	ASSERT_VALID( this );
	return NULL;
}

void CExtRibbonButtonGallery::OnRibbonAlignContent( CDC & dc )
{
	ASSERT_VALID( this );
	dc;
	InplaceGalleryEnsureCreated();
CExtRibbonGalleryWnd & wndIG = InplaceGalleryGet();
	if( wndIG.GetSafeHwnd() == NULL )
		return;
bool bInplace = IsInplaceGalleryMode() ? true : false;
bool bVisble = ( (wndIG.GetStyle()&WS_VISIBLE) != 0 ) ? true : false;
	if( bInplace != bVisble )
		wndIG.ShowWindow( bInplace ? SW_SHOWNA : SW_HIDE );
CRect rc = *this;
	wndIG.MoveWindow( &rc );
}

bool CExtRibbonButtonGallery::IsInplaceGalleryMode() const
{
	ASSERT_VALID( this );
const CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
const CExtRibbonGalleryWnd & wndIG = InplaceGalleryGet();
	if(		pRibbonPage->m_bHelperPopupMode
		&&	( ! pRibbonPage->m_bHelperAutoHideMode )
		&&	wndIG.GetSafeHwnd() != NULL
		)
		return true;
const CExtBarButton * pTBB = this, * pParentTBB = pTBB->ParentButtonGet();
	for( ; pTBB != NULL; pTBB = pParentTBB )
	{
		if( ! pTBB->IsVisible() )
			return false;
		if( (pTBB->GetStyle()&TBBS_HIDDEN) != 0 )
			return false;
		pParentTBB = pTBB->ParentButtonGet();
		if( pTBB != this && pParentTBB == NULL )
		{
			CExtRibbonButtonGroup * pGroupTBB =
				DYNAMIC_DOWNCAST( CExtRibbonButtonGroup, pTBB );
			if(		pGroupTBB != NULL
				&&	pGroupTBB->TopCollapsedStateGet()
				)
				return false;
		} // if( pTBB != this )
	} // for( ; pTBB != NULL; pTBB = pTBB->ParentButtonGet() )
INT nILV = RibbonILV_Get();
	if( nILV > __EXT_RIBBON_ILV_SIMPLE_MAX )
			return true;
	return false;
}

CExtRibbonGalleryWnd & CExtRibbonButtonGallery::InplaceGalleryGet()
{
	ASSERT_VALID( this );
	return m_wndIG;
}
const CExtRibbonGalleryWnd & CExtRibbonButtonGallery::InplaceGalleryGet() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtRibbonButtonGallery * > ( this ) ) ->
		InplaceGalleryGet();
}

void CExtRibbonButtonGallery::InplaceGalleryEnsureCreated()
{
	ASSERT_VALID( this );
CExtCustomizeCmdTreeNode * pNode = GetCmdNode();
	if( pNode == NULL )
		return;
	ASSERT_VALID( pNode );
CExtRibbonNodeGallery * pGalleryNode =
		DYNAMIC_DOWNCAST( CExtRibbonNodeGallery, pNode );
	if( pGalleryNode == NULL )
		return;
CExtToolControlBar * pBar = GetBar();
	if( pBar->GetSafeHwnd() == NULL )
		return;
CExtRibbonGalleryWnd & wndIG = InplaceGalleryGet();
	if( wndIG.GetSafeHwnd() != NULL )
		return;
bool bInPlaceIsNotNeeded = false;
CArray < DWORD, DWORD > & arrRule = pGalleryNode->RibbonILE_RuleArrayGet();
INT nRuleIndex, nRuleCount = INT( arrRule.GetSize() );
	for( nRuleIndex = 0; nRuleIndex < nRuleCount; nRuleIndex ++ )
	{
		DWORD dw = arrRule[ nRuleIndex ];
		INT nILV = INT( __EXT_RIBBON_ILV_FROM_ENTRY(dw) );
		if( nILV > __EXT_RIBBON_ILV_SIMPLE_MAX )
		{
			bInPlaceIsNotNeeded = true;
			break;
		}
	} // for( nRuleIndex = 0; nRuleIndex < nRuleCount; nRuleIndex ++ )
	if( ! bInPlaceIsNotNeeded )
		return;

//INT nILV = RibbonILV_Get();
//CSize _size( 0, 0 );
//	{
//		CWindowDC dc( NULL );
//		_size = RibbonILV_CalcSize( dc, nILV );
//	}
////////////CRect _rcChildControl( 0, 0, 382, /*315*/ 60 );
//CRect _rcChildControl( 0, 0, _size.cx, _size.cy );
bool bHideGalleryTips = false;
	if(		pNode != NULL
		&&	( ( pNode->GetFlagsEx() & __ECTN_EX_NO_RIBBON_GALLERY_TIP ) != 0 )
		)
		bHideGalleryTips = true;
DWORD dwTipFlags = __TBWS_TOOLTIPS_ALL;
	if( bHideGalleryTips )
		dwTipFlags &= (~(__TBWS_TOOLTIPS_IN_ICONS_VIEW));
CRect _rcChildControl( 0, 0, 0, 0 );
	if( ! wndIG.Create(
			pBar,
			_rcChildControl,
			GetCmdID( false ),
			WS_CHILD|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			__TBWS_MULTIPLE_EXPANDED_GROUPS
				| __TBWS_ALLOW_VERTICAL_SCROLLBAR
				| __TBWS_PM_BUTTONS
				| __TBWS_RIBBON_SELECTION_MODEL
				| dwTipFlags
			)
		)
	{
		ASSERT( FALSE );
		return;
	}
	wndIG.SetOwner(
		pBar->GetOwner()
		);
	CtrlSet( &wndIG, true );

//{
//CExtCmdIcon _icon;
//HICON hIcon = ::AfxGetApp()->LoadIcon( 128 );
//_icon.m_bmpNormal.AssignFromHICON( hIcon );
//_icon.Scale( CSize(75,75) );
//DestroyIcon( hIcon );
//		for( int j = 0; j < 3; j ++ )
//		{
//			CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_GalleryGroup =
//				wndIG.ItemInsert( NULL, _T("Gallery group"), (HICON) NULL );
//pTBCI_GalleryGroup->ModifyItemStyle( 0, __TBWI_LISTBOX_VIEW );
//			wndIG.ItemExpand( pTBCI_GalleryGroup, TVE_EXPAND );
//			ASSERT( pTBCI_GalleryGroup != NULL );
//			for( int i = 0; i < (6-j); i++ )
//			{
//				CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_GalleryItem =
//					wndIG.ItemInsert( pTBCI_GalleryGroup, _T("Gallery item"), &_icon );
//				ASSERT( pTBCI_GalleryItem != NULL );
//				if( i == 0 && j == 0 )
//					pTBCI_GalleryItem->ModifyItemStyle( __TBWI_SELECTED );
//			}
//			if( j == 0 )
//				wndIG.ItemSetActive( pTBCI_GalleryGroup );
//		}
//		wndIG.OnSwUpdateScrollBars();
//		wndIG.UpdateToolBoxWnd( true );
//}
	OnGalleryInitContent( wndIG, NULL );
}

void CExtRibbonButtonGallery::OnRibbonGalleryItemSelEndOK(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
	pRibbonPage->OnRibbonGalleryItemSelEndOK(
		wndRG,
		pGalleryPopup,
		this,
		pTBCI
		);
}

void CExtRibbonButtonGallery::OnRibbonGalleryItemInsert(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
	pRibbonPage->OnRibbonGalleryItemInsert(
		wndRG,
		pGalleryPopup,
		this,
		pTBCI
		);
}

void CExtRibbonButtonGallery::OnRibbonGalleryItemRemove(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
	pRibbonPage->OnRibbonGalleryItemRemove(
		wndRG,
		pGalleryPopup,
		this,
		pTBCI
		);
}

void CExtRibbonButtonGallery::OnRibbonGalleryItemActivate(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_Old,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_New
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
	pRibbonPage->OnRibbonGalleryItemActivate(
		wndRG,
		pGalleryPopup,
		this,
		pTBCI_Old,
		pTBCI_New
		);
}

void CExtRibbonButtonGallery::OnRibbonGalleryItemSelChange(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_Old,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_New
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
	pRibbonPage->OnRibbonGalleryItemSelChange(
		wndRG,
		pGalleryPopup,
		this,
		pTBCI_Old,
		pTBCI_New
		);
}

void CExtRibbonButtonGallery::OnRibbonGalleryItemHoverChange(
	CExtRibbonGalleryWnd & wndRG,
	CExtRibbonGalleryPopupMenuWnd * pGalleryPopup,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_Old,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_New
	)
{
	ASSERT_VALID( this );
CExtRibbonPage * pRibbonPage = GetRibbonPage();
	ASSERT_VALID( pRibbonPage );
	pRibbonPage->OnRibbonGalleryItemHoverChange(
		wndRG,
		pGalleryPopup,
		this,
		pTBCI_Old,
		pTBCI_New
		);
}

bool CExtRibbonButtonGallery::OnInvalidateButton()
{
	if( IsInplaceGalleryMode() )
	{
		CExtRibbonGalleryWnd & wndIG = InplaceGalleryGet();
		if(		wndIG.GetSafeHwnd() != NULL
			&&	( wndIG.GetStyle() & WS_VISIBLE ) != 0
			)
		{
			wndIG.Invalidate();
			return true;
		}
	}
	return CExtRibbonButton::OnInvalidateButton();
}

CPoint CExtRibbonButtonGallery::OnKeyTipGetGuideLines()
{
	ASSERT_VALID( this );
	if( IsInplaceGalleryMode() )
	{
		CExtRibbonGalleryWnd & wndIG = InplaceGalleryGet();
		if(		wndIG.GetSafeHwnd() != NULL
			&&	( wndIG.GetStyle() & WS_VISIBLE ) != 0
			)
		{
			CScrollBar * pScrollBar = wndIG.GetScrollBarCtrl( SB_VERT );
			if(		pScrollBar->GetSafeHwnd() != NULL
				&&	( pScrollBar->GetStyle() & WS_VISIBLE ) != 0
				)
			{
				CExtRibbonGalleryInplaceScrollBar * pRibbonGalleryInplaceScrollBar =
					DYNAMIC_DOWNCAST( CExtRibbonGalleryInplaceScrollBar, pScrollBar );
				if( pRibbonGalleryInplaceScrollBar != NULL )
				{
					CRect rc =
						pRibbonGalleryInplaceScrollBar->RgBtnGetRect(
							CExtRibbonGalleryInplaceScrollBar::__BTT_MENU
							);
					if( ! rc.IsRectEmpty() )
					{
						pRibbonGalleryInplaceScrollBar->ClientToScreen( &rc );
						CPoint ptGuideLines(
							rc.right,
							rc.bottom
							);
						return ptGuideLines;
					}
				}
			}
		}
	} // if( IsInplaceGalleryMode() )
	return CExtRibbonButton::OnKeyTipGetGuideLines();
}

bool CExtRibbonButtonGallery::OnKeyTipInvokeAction(
	bool & bContinueKeyTipMode
	)
{
	ASSERT_VALID( this );
CExtToolControlBar * pToolBar = GetBar();
	ASSERT( pToolBar->GetSafeHwnd() != NULL );
	ASSERT_VALID( pToolBar );
CExtMenuControlBar * pMenuBar = DYNAMIC_DOWNCAST( CExtMenuControlBar, pToolBar );
	if( pMenuBar != NULL )
		pMenuBar->_CancelFlatTracking( FALSE );
	else
		pToolBar->OnFlatTrackingStop();
CExtRibbonBar * pRibbonBar =
		DYNAMIC_DOWNCAST( CExtRibbonBar, pToolBar );
	if( IsInplaceGalleryMode() )
	{
		CExtRibbonGalleryWnd & wndIG = InplaceGalleryGet();
		if(		wndIG.GetSafeHwnd() != NULL
			&&	( wndIG.GetStyle() & WS_VISIBLE ) != 0
			)
		{
			if( pRibbonBar != NULL )
			{
				bContinueKeyTipMode = false;
				pRibbonBar->_RibbonBarKeyTipShow( false, false );
			}
			pToolBar->Invalidate();
			//pToolBar->UpdateWindow();
			CExtPaintManager::stat_PassPaintMessages();
			bContinueKeyTipMode = false;
			pToolBar->KeyTipsDisplayedSet( true );
			OnTrackPopup( CPoint(-32767,-32767), false, false );
			pToolBar->KeyTipsDisplayedSet( false );
			return true;
		}
	}
	if( pRibbonBar != NULL )
	{
		bContinueKeyTipMode = false;
		pRibbonBar->_RibbonBarKeyTipShow( false, false );
	}
	pToolBar->Invalidate();
	CExtPaintManager::stat_PassPaintMessages();
	bContinueKeyTipMode = false;
	pToolBar->KeyTipsDisplayedSet( true );
	OnTrackPopup( CPoint(-32767,-32767), false, false );
	pToolBar->KeyTipsDisplayedSet( false );
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonGalleryPopupMenuWnd

IMPLEMENT_DYNCREATE( CExtRibbonGalleryPopupMenuWnd, CExtPopupControlMenuWnd );

BEGIN_MESSAGE_MAP(CExtRibbonGalleryPopupMenuWnd, CExtPopupControlMenuWnd)
	//{{AFX_MSG_MAP(CExtRibbonGalleryPopupMenuWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CExtRibbonGalleryPopupMenuWnd::CExtRibbonGalleryPopupMenuWnd(
	HWND hWndSrcRibbonPage, // = NULL
	CExtBarButton * pSrcTrackingButton // = NULL
	)
	: m_hWndSrcRibbonPage( hWndSrcRibbonPage )
	, m_pRibbonSrcTrackingButton( pSrcTrackingButton )
	, m_bGalleryTrackingMode( true )
	, m_bRibbonFileMenuMode( false )
	, m_bHelperKeyboardProcessing( false )
	, m_nRibbonSrcMenuTrackingIdx( -1 )
{
	if( pSrcTrackingButton >= 0 )
		m_nRibbonSrcMenuTrackingIdx = GetMainRibbonPage()->GetMenuTrackingButton();
//	ASSERT( m_nRibbonSrcMenuTrackingIdx >= 0 );
}

CExtRibbonGalleryPopupMenuWnd::~CExtRibbonGalleryPopupMenuWnd()
{
}

CSize CExtRibbonGalleryPopupMenuWnd::_CalcTrackSize()
{
	if( m_bRibbonFileMenuMode )
		m_sizeChildControl.cy = max( m_sizeChildControl.cy, m_sizeFullItems.cy );
CSize _sizeAdjust = m_sizeChildControl;
CRect rcMB = OnQueryMenuBorderMetrics();
int nMenuShadowSize = OnQueryMenuShadowSize();
	if( m_bRibbonFileMenuMode )
		_sizeAdjust.cy += nMenuShadowSize + rcMB.top + rcMB.bottom;
	else
		_sizeAdjust.cx += nMenuShadowSize + rcMB.left + rcMB.right;
CSize _size = CExtPopupMenuWnd::_CalcTrackSize();
	if( m_bRibbonFileMenuMode )
		_size.cy = max( _size.cy, _sizeAdjust.cy );
	else
	{
		_size.cx = max( _size.cx, _sizeAdjust.cx );
		INT nWidthFullItems = _size.cx - nMenuShadowSize - rcMB.left - rcMB.right;
		m_sizeChildControl.cx = max( nWidthFullItems, m_sizeChildControl.cx );
	}
	return _size;
}

bool CExtRibbonGalleryPopupMenuWnd::_OnMouseWheel(
	WPARAM wParam,
	LPARAM lParam,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );
	wParam;
	lParam;

	bNoEat = false;
	if( ! g_PaintManager.m_bIsWin2000orLater )
		return true;
	if( m_wndRibbonGallery.m_bDisableMouseWheel )
		return true;
	if( m_wndRibbonGallery.GetSafeHwnd() == NULL )
		return true;
struct __SAME_AS_MOUSEHOOKSTRUCTEX
{
	MOUSEHOOKSTRUCT mhs;
	DWORD mouseData;
};
__SAME_AS_MOUSEHOOKSTRUCTEX * pMHEX =
		reinterpret_cast
			< __SAME_AS_MOUSEHOOKSTRUCTEX * >
				( lParam );
	ASSERT( pMHEX != NULL );

DWORD dwWheelDeltaAndZeroFlags =
		DWORD( pMHEX->mouseData ) & 0xFFFF0000;
	if( dwWheelDeltaAndZeroFlags == 0 )
		return 1;
int yAmount =
		( int(short(dwWheelDeltaAndZeroFlags>>16)) > 0 )
			? (-1) : 1;
int nMouseWheelScrollLines =
		(int)g_PaintManager.GetMouseWheelScrollLines();
	if( nMouseWheelScrollLines > 2 )
		nMouseWheelScrollLines--; // offset is 1 less
	yAmount *= nMouseWheelScrollLines;

//CPoint ptReal;
//	::GetCursorPos( &ptReal );
//	m_wndRibbonGallery.ScreenToClient( &ptReal );
//	m_wndRibbonGallery.SendMessage( WM_MOUSEWHEEL, MAKEWPARAM( 0, yAmount ), MAKELPARAM( ptReal.x, ptReal.y ) );
//	m_wndRibbonGallery.OnSwDoMouseWheel( 0, short(yAmount), ptReal );
	m_wndRibbonGallery.OnSwDoScrollBy( CSize(0,yAmount), true );

	return true;
}

bool CExtRibbonGalleryPopupMenuWnd::_OnMouseMove(
	UINT nFlags,
	CPoint point,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );
CPoint ptScreenClick( point );
	ClientToScreen( &ptScreenClick );
HWND hWndFromPoint = ::WindowFromPoint( ptScreenClick );
	if(		hWndFromPoint != NULL
		&&	(	m_wndRibbonGallery.GetSafeHwnd() == hWndFromPoint
			||	::IsChild( m_wndRibbonGallery.GetSafeHwnd(), hWndFromPoint )
			)
		)
	{
		bNoEat = true;
		return false;
	}
bool bRetVal = CExtPopupMenuWnd::_OnMouseMove( nFlags, point, bNoEat );
	return bRetVal;
}

bool CExtRibbonGalleryPopupMenuWnd::_OnMouseClick(
	UINT nFlags,
	CPoint point,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );
CPoint ptScreenClick( point );
	ClientToScreen( &ptScreenClick );
HWND hWndFromPoint = ::WindowFromPoint( ptScreenClick );
	if(		hWndFromPoint != NULL
		&&	(	m_wndRibbonGallery.GetSafeHwnd() == hWndFromPoint
			||	::IsChild( m_wndRibbonGallery.GetSafeHwnd(), hWndFromPoint )
			)
		)
	{
		bNoEat = true;
		return false;
	}
bool bRetVal = CExtPopupMenuWnd::_OnMouseClick( nFlags, point, bNoEat );
	return bRetVal;
}

bool CExtRibbonGalleryPopupMenuWnd::_OnKeyDown(
	UINT nChar,
	UINT nRepCnt,
	UINT nFlags,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );
 	if( m_bGalleryTrackingMode )
	{
		if( _KeyTipsEnabledGet() )
		{ // BLOCK: key tip processing
			if(		( _T('A') <= nChar && nChar <= _T('Z') )
				||	( _T('0') <= nChar && nChar <= _T('9') )
				)
			{
				if( _KeyTipsTranslate( nChar ) )
					return true;
			}
		} // BLOCK: key tip processing
		if( nChar != VK_ESCAPE )
		{
			bNoEat = true;
 			return false;
		} // if( nChar != VK_ESCAPE )
	} // if( m_bGalleryTrackingMode )
	else
	{
		MSG _msg;
		::memset( &_msg, 0, sizeof(MSG) );
		_msg.hwnd = ::GetFocus();
		_msg.wParam = WPARAM(nChar);
		_msg.lParam = MAKELPARAM(nRepCnt,nFlags);
		_msg.message =
			( ( nFlags & (KF_UP) ) != 0 )
				? WM_KEYUP
				: WM_KEYDOWN
				;
		if( OnHookSpyKeyMsg( &_msg ) )
			return true;
	} // else from if( m_bGalleryTrackingMode )
bool bRetVal = CExtPopupMenuWnd::_OnKeyDown( nChar, nRepCnt, nFlags, bNoEat );
	return bRetVal;
}

bool CExtRibbonGalleryPopupMenuWnd::IsSyncFullRowItems()
{
	ASSERT_VALID( this );
	return true;
}

CExtRibbonPage * CExtRibbonGalleryPopupMenuWnd::GetMainRibbonPage()
{
	ASSERT_VALID( this );
	if(		m_hWndSrcRibbonPage == NULL
		||	(! ::IsWindow( m_hWndSrcRibbonPage ) )
		)
		return NULL;
	ASSERT_VALID( this );
CWnd * pWnd = CWnd::FromHandlePermanent( m_hWndSrcRibbonPage );
	if( pWnd == NULL )
		return NULL;
	ASSERT_VALID( pWnd );
CExtRibbonPage * pRibbonPage =
		DYNAMIC_DOWNCAST( CExtRibbonPage, pWnd );
	if( pRibbonPage == NULL )
		return NULL;
	if( ! pRibbonPage->m_bHelperPopupMode )
		return pRibbonPage;
CExtRibbonPopupMenuWnd * pPopup =
		DYNAMIC_DOWNCAST( CExtRibbonPopupMenuWnd, pRibbonPage->GetParent() );
	if( pPopup == NULL )
		return NULL;
	pRibbonPage = pPopup->GetMainRibbonPage();
	return pRibbonPage;
}

const CExtRibbonPage * CExtRibbonGalleryPopupMenuWnd::GetMainRibbonPage() const
{
	return
		( const_cast < CExtRibbonGalleryPopupMenuWnd * > ( this ) )
		-> GetMainRibbonPage();
}

CRect CExtRibbonGalleryPopupMenuWnd::_RecalcControlRect()
{
CRect _rcChildControl = CExtPopupControlMenuWnd::_RecalcControlRect();
	if( m_bRibbonFileMenuMode )
	{
		CRect rcClient, rcMB = CExtPopupMenuWnd::OnQueryMenuBorderMetrics();
		GetClientRect( &rcClient );
		_rcChildControl.OffsetRect( rcClient.right - _rcChildControl.right - rcMB.right, 0 );
	}
	else
		_rcChildControl.OffsetRect( 0, -_rcChildControl.Height() );
	return _rcChildControl;
}

void CExtRibbonGalleryPopupMenuWnd::_RecalcLayoutImpl()
{
	ASSERT_VALID( this );
	CExtPopupControlMenuWnd::_RecalcLayoutImpl();
	if( m_wndRibbonGallery.GetSafeHwnd() != NULL )
	{
		CScrollBar * pScrollBar = m_wndRibbonGallery.GetScrollBarCtrl( SB_VERT );
		if( pScrollBar != NULL )
		{
			pScrollBar->SetRedraw( FALSE );
			CRect rc1, rc2;
			m_wndRibbonGallery.GetClientRect( &rc1 );
			pScrollBar->GetWindowRect( &rc2 );
			m_wndRibbonGallery.ScreenToClient( &rc2 );
			rc2.top = rc1.top;
			rc2.bottom = rc1.bottom;
			rc2.OffsetRect( rc1.right - rc2.right, 0 );
			if( rc2 != rc1 )
				pScrollBar->MoveWindow( &rc2 );
		}
		m_wndRibbonGallery.UpdateToolBoxWnd( true, true );
		if( pScrollBar != NULL )
		{
			pScrollBar->SetRedraw( TRUE );
			pScrollBar->UpdateWindow();
		}
		m_wndRibbonGallery.UpdateWindow();
	} // if( m_wndRibbonGallery.GetSafeHwnd() != NULL )
}

HWND CExtRibbonGalleryPopupMenuWnd::OnCreateChildControl(
	const RECT & rcChildControl
	)
{
	ASSERT_VALID( this );
CRect _rcChildControl = rcChildControl;
//	_rcChildControl.OffsetRect( 0, -_rcChildControl.Height() );
bool bHideGalleryTips = false;
	if( m_pRibbonSrcTrackingButton != NULL )
	{
		CExtCustomizeCmdTreeNode * pNode = m_pRibbonSrcTrackingButton->GetCmdNode();
		if(		pNode != NULL
			&&	( ( pNode->GetFlagsEx() & __ECTN_EX_NO_RIBBON_GALLERY_TIP ) != 0 )
			)
			bHideGalleryTips = true;
	}
DWORD dwTipFlags = __TBWS_TOOLTIPS_ALL;
	if( bHideGalleryTips )
		dwTipFlags &= (~(__TBWS_TOOLTIPS_IN_ICONS_VIEW));
	if( ! m_wndRibbonGallery.Create(
			this,
			_rcChildControl,
			AFX_IDW_DIALOGBAR,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			__TBWS_MULTIPLE_EXPANDED_GROUPS
				| __TBWS_ALLOW_VERTICAL_SCROLLBAR
				| __TBWS_PM_BUTTONS
				| __TBWS_RIBBON_SELECTION_MODEL
				| dwTipFlags
			)
		)
	{
		ASSERT( FALSE );
		return NULL;
	}
	m_wndRibbonGallery.SetOwner(
		CWnd::FromHandle( m_hWndSrcRibbonPage )->GetOwner()
		);

//	{
//CExtCmdIcon _icon;
//HICON hIcon = ::AfxGetApp()->LoadIcon( 128 );
//_icon.m_bmpNormal.AssignFromHICON( hIcon );
//_icon.Scale( CSize(75,75) );
//DestroyIcon( hIcon );
//		for( int j = 0; j < 3; j ++ )
//		{
//			CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_GalleryGroup =
//				m_wndRibbonGallery.ItemInsert( NULL, _T("Gallery group"), (HICON) NULL );
//pTBCI_GalleryGroup->ModifyItemStyle( 0, __TBWI_LISTBOX_VIEW );
//			m_wndRibbonGallery.ItemExpand( pTBCI_GalleryGroup, TVE_EXPAND );
//			ASSERT( pTBCI_GalleryGroup != NULL );
//			for( int i = 0; i < (6-j); i++ )
//			{
//				CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_GalleryItem =
//					m_wndRibbonGallery.ItemInsert( pTBCI_GalleryGroup, _T("Gallery item"), &_icon );
//				ASSERT( pTBCI_GalleryItem != NULL );
//				if( i == 0 && j == 0 )
//					pTBCI_GalleryItem->ModifyItemStyle( __TBWI_SELECTED );
//			}
//			if( j == 0 )
//				m_wndRibbonGallery.ItemSetActive( pTBCI_GalleryGroup );
//		}
//		m_wndRibbonGallery.UpdateToolBoxWnd( true );
//	}
	if( m_pRibbonSrcTrackingButton != NULL )
	{
		ASSERT_VALID( m_pRibbonSrcTrackingButton );
		CExtRibbonButtonGallery * pRibbonGalleryTBB =
			DYNAMIC_DOWNCAST( CExtRibbonButtonGallery, m_pRibbonSrcTrackingButton );
		if( pRibbonGalleryTBB != NULL )
			pRibbonGalleryTBB->OnGalleryInitContent( m_wndRibbonGallery, this );
	} // if( m_pRibbonSrcTrackingButton != NULL )
	else
	{
			CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
			if( pRibbonPage != NULL )
			{
				ASSERT_VALID( pRibbonPage );
				ASSERT( pRibbonPage->GetSafeHwnd() != NULL );
				pRibbonPage->OnRibbonGalleryInitContent(
					m_wndRibbonGallery,
					this,
					NULL
					);
			} // if( pRibbonPage != NULL )
	} // else from if( m_pRibbonSrcTrackingButton != NULL )

	return m_wndRibbonGallery.m_hWnd;
}

bool CExtRibbonGalleryPopupMenuWnd::OnHookSpyMouseWheelMsg(
	MSG * pMSG
	)
{
	ASSERT_VALID( this );
	ASSERT( pMSG != NULL );
	pMSG;
	m_bHelperKeyboardProcessing = false;
//	if( m_wndRibbonGallery.PreTranslateMessage( pMSG ) )
//		return true;
CPoint point = pMSG->lParam;
UINT fFlags = LOWORD(pMSG->wParam);
short zDelta = HIWORD(pMSG->wParam);
	if( m_wndRibbonGallery.CExtScrollWnd::OnSwDoMouseWheel( fFlags, zDelta, point ) )
	{
		m_wndRibbonGallery.UpdateToolBoxWnd( false );
		return true;
	}
	return false;
}

bool CExtRibbonGalleryPopupMenuWnd::OnHookSpyMouseMoveMsg(
	MSG * pMSG
	)
{
	ASSERT_VALID( this );
	ASSERT( pMSG != NULL );
	pMSG;
	m_bHelperKeyboardProcessing = false;
	return false;
}

bool CExtRibbonGalleryPopupMenuWnd::OnHookSpyMouseClickMsg(
	MSG * pMSG
	)
{
	ASSERT_VALID( this );
	ASSERT( pMSG != NULL );
	pMSG;
	m_bHelperKeyboardProcessing = false;
	return false;
}

bool CExtRibbonGalleryPopupMenuWnd::OnHookSpyKeyMsg(
	MSG * pMSG
	)
{
	ASSERT_VALID( this );
	ASSERT( pMSG != NULL );
	if(		pMSG->message == WM_KEYDOWN
		&&	_GetCapture() == this
		)
	{
		m_bHelperKeyboardProcessing = true;
		if( m_bGalleryTrackingMode )
		{
			if( m_wndRibbonGallery._ProcessKeyDownMsg( UINT(pMSG->wParam), LOWORD(pMSG->lParam), HIWORD(pMSG->lParam) ) )
				return true;
			if( pMSG->wParam == VK_RETURN )
			{
				CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI = m_wndRibbonGallery.ItemGetActive();
				if( pTBCI != NULL )
				{
					pTBCI = pTBCI->FindSelectedChild();
					if( pTBCI != NULL )
					{
						if( m_wndRibbonGallery.m_pRibbonGalleryTBB != NULL )
						{
							ASSERT_VALID( m_wndRibbonGallery.m_pRibbonGalleryTBB );
							m_bHelperKeyboardProcessing = false;
							m_wndRibbonGallery.m_pRibbonGalleryTBB->
								OnRibbonGalleryItemSelEndOK(
									m_wndRibbonGallery,
									this,
									pTBCI
									);
						} // if( m_wndRibbonGallery.m_pRibbonGalleryTBB != NULL )
						else
						{
							CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
							if( pRibbonPage != NULL )
							{
								m_bHelperKeyboardProcessing = false;
								pRibbonPage->
									OnRibbonGalleryItemSelEndOK(
										m_wndRibbonGallery,
										this,
										NULL,
										pTBCI
										);
							}
						} // else from if( m_wndRibbonGallery.m_pRibbonGalleryTBB != NULL )
					} // if( pTBCI != NULL )
				} // if( pTBCI != NULL )
				return true;
			} // if( pMSG->wParam == VK_RETURN )
			if( pMSG->wParam == VK_DOWN || pMSG->wParam == VK_UP )
			{
				INT nIndex = _GetNextItem( __NI_ANY );
				if( nIndex >= 0 )
				{
					m_bGalleryTrackingMode = false;
					CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI1 = m_wndRibbonGallery.ItemGetRoot()->ItemGetNext( __TBCGN_CHILD, 0 );
					for( ; pTBCI1 != NULL; pTBCI1 = pTBCI1->ItemGetNext( __TBCGN_SIBLING, 1 ) )
					{
						CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI2 = pTBCI1->ItemGetNext( __TBCGN_CHILD, 0 );
						for( ; pTBCI2 != NULL; pTBCI2 = pTBCI2->ItemGetNext( __TBCGN_SIBLING, 1 ) )
						{
							if( (pTBCI2->GetItemStyle()&__TBWI_SELECTED) != 0 )
								pTBCI2->ModifyItemStyle( 0, __TBWI_SELECTED );
						}
					}
 					if( pMSG->wParam == VK_UP )
 					{
						_ItemFocusSet( nIndex, FALSE, FALSE );
						nIndex = _GetNextItem( __NI_PREV );
					}
					_ItemFocusSet( nIndex, FALSE, TRUE );
					return true;
				}
			}
			return false;
		} // if( m_bGalleryTrackingMode )
		if(		(	m_nCurIndex >= 0
				||	m_bRibbonFileMenuMode
				)
			&&	(	pMSG->wParam == VK_DOWN
				||	pMSG->wParam == VK_UP
				)
			)
		{
//			
			INT nIndex = _GetNextItem( ( pMSG->wParam == VK_UP ) ? __NI_PREV : __NI_NEXT );
			if( nIndex < 0 )
				return false;
			if( pMSG->wParam == VK_UP )
			{
				if( nIndex >= m_nCurIndex )
				{
					_ItemFocusCancel( TRUE );
					m_bGalleryTrackingMode = true;
					CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI =
							m_wndRibbonGallery.ItemGetRoot()->ItemGetNext( __TBCGN_LAST_CHILD, 0 );
					if( pTBCI != NULL )
					{
						m_wndRibbonGallery.ItemSetActive( pTBCI );
						pTBCI = pTBCI->ItemGetNext( __TBCGN_LAST_CHILD, 0 );
						if( pTBCI != NULL )
						{
							pTBCI->ModifyItemStyle( __TBWI_SELECTED );
							if( m_wndRibbonGallery.m_pRibbonGalleryTBB != NULL )
							{
								ASSERT_VALID( m_wndRibbonGallery.m_pRibbonGalleryTBB );
								m_wndRibbonGallery.m_pRibbonGalleryTBB->
									OnRibbonGalleryItemSelChange(
										m_wndRibbonGallery,
										this,
										NULL,
										pTBCI
										);
							} // if( m_wndRibbonGallery.m_pRibbonGalleryTBB != NULL )
							else
							{
								CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
								if( pRibbonPage != NULL )
								{
									pRibbonPage->
										OnRibbonGalleryItemSelChange(
											m_wndRibbonGallery,
											this,
											NULL,
											NULL,
											pTBCI
											);
								}
							} // else from if( m_wndRibbonGallery.m_pRibbonGalleryTBB != NULL )
						}
						m_wndRibbonGallery.SetScrollPos( SB_VERT, 0, FALSE );
						m_wndRibbonGallery.UpdateToolBoxWnd( true, true );
					}
					return true;
				}
			}
			else
			{
				if( nIndex <= m_nCurIndex )
				{
					_ItemFocusCancel( TRUE );
					m_bGalleryTrackingMode = true;
					CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI =
							m_wndRibbonGallery.ItemGetRoot()->ItemGetNext( __TBCGN_CHILD, 0 );
					if( pTBCI != NULL )
					{
						m_wndRibbonGallery.ItemSetActive( pTBCI );
						pTBCI = pTBCI->ItemGetNext( __TBCGN_CHILD, 0 );
						if( pTBCI != NULL )
						{
							pTBCI->ModifyItemStyle( __TBWI_SELECTED );
							if( m_wndRibbonGallery.m_pRibbonGalleryTBB != NULL )
							{
								ASSERT_VALID( m_wndRibbonGallery.m_pRibbonGalleryTBB );
								m_wndRibbonGallery.m_pRibbonGalleryTBB->
									OnRibbonGalleryItemSelChange(
										m_wndRibbonGallery,
										this,
										NULL,
										pTBCI
										);
							} // if( m_wndRibbonGallery.m_pRibbonGalleryTBB != NULL )
							else
							{
								CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
								if( pRibbonPage != NULL )
								{
									pRibbonPage->OnRibbonGalleryItemSelChange(
										m_wndRibbonGallery,
										this,
										NULL,
										NULL,
										pTBCI
										);
								}
							}// else from if( m_wndRibbonGallery.m_pRibbonGalleryTBB != NULL )
						}
						m_wndRibbonGallery.SetScrollPos( SB_VERT, 0, FALSE );
						m_wndRibbonGallery.UpdateToolBoxWnd( true, true );
					}
					return true;
				}
			}
		} // if( ( m_nCurIndex >= 0 . . .
		else
		{
			bool bNoEat = false;
			if( CExtPopupMenuWnd::_OnKeyDown( UINT(pMSG->wParam), 1, 0, bNoEat ) )
				return true;
			m_bGalleryTrackingMode = true;
			return true;
		}
	} // if( pMSG->message == WM_KEYDOWN )
	return false;
}

BOOL CExtRibbonGalleryPopupMenuWnd::DestroyWindow()
{
	HookSpyUnregister();
	if( GetSafeHwnd() == NULL )
		return TRUE;
	if( m_pRibbonSrcTrackingButton != NULL )
	{
		CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
		if( pRibbonPage != NULL )
		{
			ASSERT_VALID( m_pRibbonSrcTrackingButton );
			if( m_pRibbonSrcTrackingButton == pRibbonPage->GetButton( m_nRibbonSrcMenuTrackingIdx ) )
			{
				CExtRibbonButtonGallery * pRibbonGalleryTBB =
					DYNAMIC_DOWNCAST( CExtRibbonButtonGallery, m_pRibbonSrcTrackingButton );
				if( pRibbonGalleryTBB != NULL )
					pRibbonGalleryTBB->OnGalleryPopupShow( this, false );
			}
		} // if( pRibbonPage != NULL )
		m_pRibbonSrcTrackingButton = NULL;
	} // if( m_pRibbonSrcTrackingButton != NULL )
	return CExtPopupControlMenuWnd::DestroyWindow();
}

LRESULT CExtRibbonGalleryPopupMenuWnd::WindowProc(
	UINT message,
	WPARAM wParam,
	LPARAM lParam
	)
{
	if( message == WM_CREATE )
	{
		HookSpyRegister( __EHSEF_KEYBOARD|__EHSEF_MOUSE_ALL );
		if( m_pRibbonSrcTrackingButton != NULL )
		{
			CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
			if( pRibbonPage != NULL )
			{
				ASSERT_VALID( m_pRibbonSrcTrackingButton );
				if( m_pRibbonSrcTrackingButton == pRibbonPage->GetButton( m_nRibbonSrcMenuTrackingIdx ) )
				{
					CExtRibbonButtonGallery * pRibbonGalleryTBB =
						DYNAMIC_DOWNCAST( CExtRibbonButtonGallery, m_pRibbonSrcTrackingButton );
					if( pRibbonGalleryTBB != NULL )
						pRibbonGalleryTBB->OnGalleryPopupShow( this, true );
				}
			} // if( pRibbonPage != NULL )
		} // if( m_pRibbonSrcTrackingButton != NULL )
	}
	else if( message == WM_DESTROY )
	{
/*
		if( m_pRibbonSrcTrackingButton != NULL )
		{
			CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
			if( pRibbonPage != NULL )
			{
				ASSERT_VALID( m_pRibbonSrcTrackingButton );
				if( m_pRibbonSrcTrackingButton == pRibbonPage->GetButton( m_nRibbonSrcMenuTrackingIdx ) )
				{
					CExtRibbonButtonGallery * pRibbonGalleryTBB =
						DYNAMIC_DOWNCAST( CExtRibbonButtonGallery, m_pRibbonSrcTrackingButton );
					if( pRibbonGalleryTBB != NULL )
						pRibbonGalleryTBB->OnGalleryPopupShow( this, false );
				}
			} // if( pRibbonPage != NULL )
		} // m_pRibbonSrcTrackingButton
*/
		HookSpyUnregister();
		g_CmdManager->ProfileWndRemove(
			m_hWnd
			);
	}
LRESULT lResult = CExtPopupControlMenuWnd::WindowProc( message, wParam, lParam );
	return lResult;
}

CRect CExtRibbonGalleryPopupMenuWnd::OnQueryMenuBorderMetrics() const
{
	ASSERT_VALID( this );
CRect rcMenuBorderMetrics =
		CExtPopupControlMenuWnd::OnQueryMenuBorderMetrics();
	if( m_bRibbonFileMenuMode )
		rcMenuBorderMetrics.right += m_sizeChildControl.cx;
	else
		rcMenuBorderMetrics.top += m_sizeChildControl.cy;
	return rcMenuBorderMetrics;
}


/////////////////////////////////////////////////////////////////////////////
// CExtRibbonPopupMenuWnd

IMPLEMENT_DYNCREATE( CExtRibbonPopupMenuWnd, CExtPopupControlMenuWnd );

BEGIN_MESSAGE_MAP(CExtRibbonPopupMenuWnd, CExtPopupControlMenuWnd)
	//{{AFX_MSG_MAP(CExtRibbonPopupMenuWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CExtRibbonPopupMenuWnd::CExtRibbonPopupMenuWnd(
	HWND hWndSrcRibbonPage, // = NULL
	CExtRibbonButtonGroup * pSrcTrackingButtonGroup, // = NULL
	CExtRibbonButtonTabPage * pSrcTrackingButtonTabPage // = NULL
	)
	: m_pSrcTrackingButtonGroup( pSrcTrackingButtonGroup )
	, m_pSrcTrackingButtonTabPage( pSrcTrackingButtonTabPage )
{
	m_wndRibbonPage.m_hWndSrcRibbonPage = hWndSrcRibbonPage;
	m_nSrcMenuTrackingIdx = GetMainRibbonPage()->GetMenuTrackingButton();
//	ASSERT( m_nSrcMenuTrackingIdx >= 0 );
	m_wndRibbonPage.m_bHelperPopupMode = true;
}

CExtRibbonPopupMenuWnd::~CExtRibbonPopupMenuWnd()
{
}

CExtRibbonPage * CExtRibbonPopupMenuWnd::GetMainRibbonPage()
{
	ASSERT_VALID( this );
	return m_wndRibbonPage.GetMainRibbonPage();
}

const CExtRibbonPage * CExtRibbonPopupMenuWnd::GetMainRibbonPage() const
{
	return
		( const_cast < CExtRibbonPopupMenuWnd * > ( this ) )
		-> GetMainRibbonPage();
}

CExtRibbonPage * CExtRibbonPopupMenuWnd::GetParentRibbonPage()
{
	ASSERT_VALID( this );
	return m_wndRibbonPage.GetParentRibbonPage();
}

const CExtRibbonPage * CExtRibbonPopupMenuWnd::GetParentRibbonPage() const
{
	return
		( const_cast < CExtRibbonPopupMenuWnd * > ( this ) )
		-> GetParentRibbonPage();
}

HWND CExtRibbonPopupMenuWnd::OnCreateChildControl(
	const RECT & rcChildControl
	)
{
	ASSERT_VALID( this );
	m_wndRibbonPage.m_bPresubclassDialogMode = true;
	if( ! m_wndRibbonPage.Create(
			NULL,
			this,
			AFX_IDW_DIALOGBAR,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS
				|CBRS_ALIGN_TOP //|CBRS_TOP|CBRS_GRIPPER
				|CBRS_TOOLTIPS
				|CBRS_FLYBY|CBRS_SIZE_DYNAMIC
				//|CBRS_HIDE_INPLACE
			)
		)
	{
		ASSERT( FALSE );
		return NULL;
	}
	m_wndRibbonPage.SetOwner(
		CWnd::FromHandle( m_wndRibbonPage.m_hWndSrcRibbonPage )->GetOwner()
		);
	m_wndRibbonPage.MoveWindow( &rcChildControl );
CExtRibbonPage * pMain = GetMainRibbonPage();
	if(		pMain != NULL
		&&	pMain != (&m_wndRibbonPage)
		&&	(pMain->GetExStyle()&WS_EX_LAYOUTRTL) != 0
		)
		m_wndRibbonPage.ModifyStyleEx( 0, WS_EX_LAYOUTRTL, SWP_FRAMECHANGED );
	m_wndRibbonPage._RecalcPositionsImpl();
	m_wndRibbonPage.DoCustomModeUpdateCmdUI();
	return m_wndRibbonPage.m_hWnd;
}

//bool CExtRibbonPopupMenuWnd::_OnMouseWheel(
//	WPARAM wParam,
//	LPARAM lParam,
//	bool & bNoEat
//	)
//{
//	ASSERT_VALID( this );
//
//TranslateMouseWheelEventData_t _td( this, wParam, lParam, bNoEat );
//	if( _td.Notify() )
//	{
//		bNoEat = _td.m_bNoEat;
//		return true;
//	}
//
//	bNoEat = false;
//	return true;
//}
//
//bool CExtRibbonPopupMenuWnd::_OnMouseMove(
//	UINT nFlags,
//	CPoint point,
//	bool & bNoEat
//	)
//{
//	ASSERT_VALID( this );
//
//	if( GetSafeHwnd() == NULL )
//		return false;
//
//	if( !m_bAnimFinished )
//		return true;
//
//CExtPopupMenuSite & _site = GetSite();
//	if(	_site.IsShutdownMode()
//		|| _site.IsEmpty()
//		|| _site.GetAnimated() != NULL
//		)
//		return true;
//
//TranslateMouseMoveEventData_t _td( this, nFlags, point, bNoEat );
//	if( _td.Notify() )
//	{
//		bNoEat = _td.m_bNoEat;
//		return true;
//	}
//
//	bNoEat = true;
//	return false;
//}
//
//bool CExtRibbonPopupMenuWnd::_OnMouseClick(
//	UINT nFlags,
//	CPoint point,
//	bool & bNoEat
//	)
//{
//	ASSERT_VALID( this );
//
//	if( GetSafeHwnd() == NULL )
//		return false;
//
//	if( ! m_bAnimFinished )
//		return true;
//
//CExtPopupMenuSite & _site = GetSite();
//	if(	_site.IsShutdownMode()
//		|| _site.IsEmpty()
//		|| _site.GetAnimated() != NULL
//		)
//		return true;
//
//TranslateMouseClickEventData_t _td( this, nFlags, point, bNoEat );
//	if( _td.Notify() )
//	{
//		bNoEat = _td.m_bNoEat;
//		return true;
//	}
//
//	bNoEat = true;
//	return false;
//}
//
//bool CExtRibbonPopupMenuWnd::_OnKeyDown(
//	UINT nChar,
//	UINT nRepCnt,
//	UINT nFlags,
//	bool & bNoEat
//	)
//{
//	ASSERT_VALID( this );
//	if( GetSafeHwnd() == NULL )
//		return true; //false;
//	if( GetSite().GetAnimated() != NULL )
//		return true;
//
//TranslateKeyboardEventData_t _td( this, nChar, nRepCnt, nFlags, bNoEat );
//	if( _td.Notify() )
//	{
//		bNoEat = _td.m_bNoEat;
//		return true;
//	}
//
//	return
//		CExtPopupControlMenuWnd::_OnKeyDown(
//			nChar,
//			nRepCnt,
//			nFlags,
//			bNoEat
//			);
//}

bool CExtRibbonPopupMenuWnd::OnHookSpyMouseWheelMsg(
	MSG * pMSG
	)
{
	ASSERT_VALID( this );
	ASSERT( pMSG != NULL );
	if(		GetSafeHwnd() == NULL
		||	pMSG->hwnd == m_hWnd
		||	::IsChild( m_hWnd, pMSG->hwnd )
		)
		return false;
	if( CExtPopupMenuWnd::IsMenuTracking() )
	{
		HWND hWnd = pMSG->hwnd;
		for( ; hWnd != NULL; hWnd = ::GetParent( hWnd ) )
		{
			CWnd * pWnd = CWnd::FromHandlePermanent( hWnd );
			if(		pWnd != NULL
				&&	pWnd->IsKindOf( RUNTIME_CLASS( CExtPopupBaseWnd ) )
				)
				return false;
		} // for( ; hWnd != NULL; hWnd = ::GetParent( hWnd ) )
	} // if( CExtPopupMenuWnd::IsMenuTracking() )
	DestroyWindow();
	CExtToolControlBar::_CloseTrackingMenus();
	return false;
}

bool CExtRibbonPopupMenuWnd::OnHookSpyMouseMoveMsg(
	MSG * pMSG
	)
{
	ASSERT_VALID( this );
	ASSERT( pMSG != NULL );
	pMSG;
	return false;
}

bool CExtRibbonPopupMenuWnd::OnHookSpyMouseClickMsg(
	MSG * pMSG
	)
{
	ASSERT_VALID( this );
	ASSERT( pMSG != NULL );
	if(		GetSafeHwnd() == NULL
		||	pMSG->hwnd == m_hWnd
		||	::IsChild( m_hWnd, pMSG->hwnd )
		)
		return false;

	if( m_wndRibbonPage.m_bHelperAutoHideMode )
	{
		if( m_wndRibbonPage.m_pPopupPageMenuGroup->GetSafeHwnd() != NULL )
			return false;
	} // if( m_wndRibbonPage.m_bHelperAutoHideMode )
	else
	{
		bool bInnerPageClick = false;
		bool bHelperAutoHideMode = false;
		CExtRibbonPage * pRibbonPage = GetParentRibbonPage();
		for(	;
				pRibbonPage != NULL;
				pRibbonPage = pRibbonPage->GetParentRibbonPage()
			)
		{
			if( pMSG->hwnd == pRibbonPage->m_hWnd )
				bInnerPageClick = true;
			
			if( (!bHelperAutoHideMode) && pRibbonPage->m_bHelperAutoHideMode )
				bHelperAutoHideMode = true;
		}
		if( ! bInnerPageClick )
		{
			DestroyWindow();
			CExtToolControlBar::_CloseTrackingMenus();
			return true;
		}
		if( bHelperAutoHideMode )
			return false;
	} // else from if( m_wndRibbonPage.m_bHelperAutoHideMode )

bool bRetVal = false;
	if(		pMSG->hwnd == m_wndRibbonPage.m_hWndSrcRibbonPage
		&&	(	pMSG->message == WM_LBUTTONDOWN
			||	pMSG->message == WM_LBUTTONUP
			)
		)
	{
		CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
		if( pRibbonPage != NULL )
		{
			INT nMenuTrackingIdx = pRibbonPage->GetMenuTrackingButton();
			if( nMenuTrackingIdx >= 0 )
			{
				INT nHT = pRibbonPage->HitTest( pMSG->pt );
				if( nMenuTrackingIdx == nHT )
				{
					if( pMSG->message == WM_LBUTTONUP )
						return false;
					bRetVal = true;
				}
			}
		}
	}
	else if( CExtPopupMenuWnd::IsMenuTracking() )
	{
		HWND hWnd = pMSG->hwnd;
		for( ; hWnd != NULL; hWnd = ::GetParent( hWnd ) )
		{
			CWnd * pWnd = CWnd::FromHandlePermanent( hWnd );
			if(		pWnd != NULL
				&&	pWnd->IsKindOf( RUNTIME_CLASS( CExtPopupBaseWnd ) )
				)
				return false;
		} // for( ; hWnd != NULL; hWnd = ::GetParent( hWnd ) )
	} // else if( CExtPopupMenuWnd::IsMenuTracking() )
CExtRibbonBar * pRibbonBar = DYNAMIC_DOWNCAST( CExtRibbonBar, GetMainRibbonPage() );
	if( pRibbonBar != NULL )
	{
		if(		pRibbonBar->m_pExtNcFrameImpl != NULL
			&&	pRibbonBar->m_pExtNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement()
			&&	pMSG->message == WM_NCLBUTTONUP
			)
			return false;
		if( pMSG->message == WM_LBUTTONUP && pMSG->hwnd == pRibbonBar->GetSafeHwnd() )
			return false;
	}
	DestroyWindow();
	CExtToolControlBar::_CloseTrackingMenus();
	return bRetVal;
}

bool CExtRibbonPopupMenuWnd::OnHookSpyKeyMsg(
	MSG * pMSG
	)
{
	ASSERT_VALID( this );
	ASSERT( pMSG != NULL );
	if(		GetSafeHwnd() == NULL
		||	::IsChild( m_hWnd, pMSG->hwnd )
		)
		return false;

	if( pMSG->wParam == VK_ESCAPE )
	{
		HWND hWndOwn = m_hWnd;
		m_wndRibbonPage.OnMenuClosed( 0xFFFFL, 0L );
		if( ::IsWindow( hWndOwn ) )
			DestroyWindow();
		return true;
	} // if( pMSG->wParam == VK_ESCAPE )

	if( m_wndRibbonPage.m_bHelperAutoHideMode )
	{
		if( m_wndRibbonPage.m_pPopupPageMenuGroup->GetSafeHwnd() != NULL )
		{
			if( m_wndRibbonPage.m_pPopupPageMenuGroup->m_wndRibbonPage.GetSafeHwnd() != NULL )
			{
				m_wndRibbonPage.m_pPopupPageMenuGroup->m_wndRibbonPage.TranslateMainFrameMessage( pMSG );
				return true;
			}
			return false;
		}
	} // if( m_wndRibbonPage.m_bHelperAutoHideMode )
	else
	{
		CExtRibbonPage * pRibbonPage = GetParentRibbonPage();
		for(	;
				pRibbonPage != NULL;
				pRibbonPage = pRibbonPage->GetParentRibbonPage()
			)
		{
			if( pRibbonPage->m_bHelperAutoHideMode )
				return false;
		}
	} // else from if( m_wndRibbonPage.m_bHelperAutoHideMode )

	if( pMSG->hwnd == m_hWnd )
	{
//		if( pMSG->wParam != VK_ESCAPE )
//			return false;
	}
	else if( CExtPopupMenuWnd::IsMenuTracking() )
	{
		HWND hWnd = pMSG->hwnd;
		for( ; hWnd != NULL; hWnd = ::GetParent( hWnd ) )
		{
			CWnd * pWnd = CWnd::FromHandlePermanent( hWnd );
			if(		pWnd != NULL
				&&	pWnd->IsKindOf( RUNTIME_CLASS( CExtPopupBaseWnd ) )
				)
				return false;
		} // for( ; hWnd != NULL; hWnd = ::GetParent( hWnd ) )
	} // else if( CExtPopupMenuWnd::IsMenuTracking() )

	if( ! m_wndRibbonPage.m_bFlatTracking )
	{
		m_wndRibbonPage.m_bFlatTracking = true;
		HDWP hPassiveModeDWP = NULL;
		m_wndRibbonPage.OnFlatTrackingStart( hPassiveModeDWP );
	} // if( ! m_wndRibbonPage.m_bFlatTracking )
	m_wndRibbonPage.TranslateMainFrameMessage( pMSG );

	return true;
}

bool CExtRibbonPopupMenuWnd::_IsFadeOutAnimation() const
{
	ASSERT_VALID( this );
	return false;
}

void CExtRibbonPopupMenuWnd::_DeleteFadeOutMenu()
{
	ASSERT_VALID( this );
}

BOOL CExtRibbonPopupMenuWnd::DestroyWindow()
{
	HookSpyUnregister();
	if( GetSafeHwnd() == NULL )
		return TRUE;
	if( m_pSrcTrackingButtonGroup != NULL )
	{
		CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
		if( pRibbonPage != NULL )
		{
			ASSERT_VALID( m_pSrcTrackingButtonGroup );
			if( m_pSrcTrackingButtonGroup == pRibbonPage->GetButton( m_nSrcMenuTrackingIdx ) )
				m_pSrcTrackingButtonGroup->OnRibbonPopupShow( this, false );
		} // if( pRibbonPage != NULL )
		m_pSrcTrackingButtonGroup = NULL;
	} // if( m_pSrcTrackingButtonGroup != NULL )
	return CExtPopupControlMenuWnd::DestroyWindow();
}

LRESULT CExtRibbonPopupMenuWnd::WindowProc(
	UINT message,
	WPARAM wParam,
	LPARAM lParam
	)
{
	if( message == WM_CREATE )
	{
		g_CmdManager->ProfileWndAdd(
			g_CmdManager->ProfileNameFromWnd( m_wndRibbonPage.m_hWndSrcRibbonPage ),
			m_hWnd
			);
		HookSpyRegister( __EHSEF_KEYBOARD|__EHSEF_MOUSE_ALL );
		if( ! m_wndRibbonPage.m_bHelperAutoHideMode )
		{
			if( m_pSrcTrackingButtonGroup != NULL )
			{
				CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
				if( pRibbonPage != NULL )
				{
					ASSERT_VALID( m_pSrcTrackingButtonGroup );
					if( m_pSrcTrackingButtonGroup == pRibbonPage->GetButton( m_nSrcMenuTrackingIdx ) )
						m_pSrcTrackingButtonGroup->OnRibbonPopupShow( this, true );
				} // if( pRibbonPage != NULL )
			} // m_pSrcTrackingButtonGroup
		} // if( ! m_wndRibbonPage.m_bHelperAutoHideMode )
	}
	else if( message == WM_DESTROY )
	{
		HWND hWndOwn = m_hWnd;
		if( ! m_wndRibbonPage.m_bHelperAutoHideMode )
		{
			if(		m_wndRibbonPage.m_hWndSrcRibbonPage != NULL
				&&	::IsWindow( m_wndRibbonPage.m_hWndSrcRibbonPage )
				)
			{
				if( m_pSrcTrackingButtonGroup != NULL )
				{
					m_pSrcTrackingButtonGroup->SetHover( false );
					m_pSrcTrackingButtonGroup->ModifyStyle( 0, TBBS_PRESSED );
					CRect rc = *m_pSrcTrackingButtonGroup;
					::InvalidateRect( m_wndRibbonPage.m_hWndSrcRibbonPage, &rc, TRUE );
				} // if( m_pSrcTrackingButtonGroup != NULL )
				else
					::InvalidateRect( m_wndRibbonPage.m_hWndSrcRibbonPage, NULL, TRUE );
				GetMainRibbonPage()->_CancelFlatTracking();
				CancelMenuTracking();
				CExtToolControlBar::_CloseTrackingMenus();
				::SendMessage( m_wndRibbonPage.m_hWndSrcRibbonPage, WM_CANCELMODE, 0, 0 );
			}
			if( m_pSrcTrackingButtonGroup != NULL )
			{
				CExtRibbonPage * pRibbonPage = GetMainRibbonPage();
				if( pRibbonPage != NULL )
				{
					ASSERT_VALID( m_pSrcTrackingButtonGroup );
					if( m_pSrcTrackingButtonGroup == pRibbonPage->GetButton( m_nSrcMenuTrackingIdx ) )
						m_pSrcTrackingButtonGroup->OnRibbonPopupShow( this, false );
				} // if( pRibbonPage != NULL )
			} // m_pSrcTrackingButtonGroup
		} // if( ! m_wndRibbonPage.m_bHelperAutoHideMode )
		HookSpyUnregister();
		g_CmdManager->ProfileWndRemove(
			hWndOwn
			);
	}
LRESULT lResult = CExtPopupControlMenuWnd::WindowProc( message, wParam, lParam );
	return lResult;
}

CRect CExtRibbonPopupMenuWnd::OnQueryMenuBorderMetrics() const
{
	ASSERT_VALID( this );
CRect rcMenuBorderMetrics =
		PmBridge_GetPM() ->
			Ribbon_GetPopupGroupBorderMetrics( &m_wndRibbonPage );
	return rcMenuBorderMetrics;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonOptionsPage

IMPLEMENT_DYNAMIC( CExtRibbonOptionsPage, CExtResizableDialog );

CExtRibbonOptionsPage::CExtRibbonOptionsPage(
	UINT nPageID
	)
	: CExtResizableDialog( nPageID )
	, m_clrPageText( RGB(0,0,0) )
	, m_clrPageBkgnd( RGB(255,255,255) )
{
	//{{AFX_DATA_INIT(CExtRibbonOptionsPage)
	//}}AFX_DATA_INIT
	m_bmpPageWaterMark.LoadBMP_Resource(
		MAKEINTRESOURCE( IDB_EXT_2007_OPTIONS_PAGE_WATERMARK )
		);
LOGFONT _lf;
	::memset( &_lf, 0, sizeof(LOGFONT) );
	g_PaintManager->m_FontNormal.GetLogFont( &_lf );
	_lf.lfWidth =  ::MulDiv( _lf.lfWidth,  5, 4 );
	_lf.lfHeight = ::MulDiv( _lf.lfHeight, 5, 4 );
	m_fontPageCaption.CreateFontIndirect( &_lf );
}

CExtRibbonOptionsPage::~CExtRibbonOptionsPage()
{
}

void CExtRibbonOptionsPage::CConsistentLabel::OnEraseBackground(
	CDC & dc,
	const CRect & rcClient
	)
{
	//if( ! PmBridge_GetPM()->Ribbon_OptionsPageBackgroundIsDefault() )
	//{
		CWnd * pWnd = GetParent();
		if( pWnd != NULL )
		{
			CExtRibbonOptionsPage * pPage =
				DYNAMIC_DOWNCAST( CExtRibbonOptionsPage, pWnd );
			if( pPage != NULL )
			{
				CPoint ptOffset = rcClient.TopLeft();
				ClientToScreen( &ptOffset );
				pPage->ScreenToClient( &ptOffset );
				dc.OffsetViewportOrg( -ptOffset.x, -ptOffset.y );
				pPage->OnEraseBackground( dc, rcClient );
				dc.OffsetViewportOrg( ptOffset.x, ptOffset.y );
				return;
			} // if( pPage != NULL )
		} // if( pWnd != NULL )
	//} // if( ! PmBridge_GetPM()->Ribbon_OptionsPageBackgroundIsDefault() )
	CExtLabel::OnEraseBackground( dc, rcClient );
}

void CExtRibbonOptionsPage::DoDataExchange(CDataExchange* pDX)
{
	CExtResizableDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExtRibbonOptionsPage)
	//}}AFX_DATA_MAP
}

CExtRibbonBar * CExtRibbonOptionsPage::RibbonOptionsPage_GetBar()
{
	ASSERT_VALID( this );
CExtRibbonOptionsDialog * pDialogRibbonOptions =
		RibbonOptionsPage_GetContainer();
	if( pDialogRibbonOptions == NULL )
		return NULL;
	if( pDialogRibbonOptions->m_pRibbonBar == NULL )
		return NULL;
	ASSERT_VALID( pDialogRibbonOptions->m_pRibbonBar );
	return pDialogRibbonOptions->m_pRibbonBar;
}

const CExtRibbonBar * CExtRibbonOptionsPage::RibbonOptionsPage_GetBar() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtRibbonOptionsPage * > ( this ) )
		-> RibbonOptionsPage_GetBar();
}

CExtRibbonOptionsDialog * CExtRibbonOptionsPage::RibbonOptionsPage_GetContainer()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return NULL;
CWnd * pWndParent = GetParent();
	if( pWndParent == NULL )
		return NULL;
CExtRibbonOptionsDialog * pDialogRibbonOptions =
		DYNAMIC_DOWNCAST( CExtRibbonOptionsDialog, pWndParent );
	return pDialogRibbonOptions;
}

const CExtRibbonOptionsDialog * CExtRibbonOptionsPage::RibbonOptionsPage_GetContainer() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtRibbonOptionsPage * > ( this ) )
		-> RibbonOptionsPage_GetContainer();
}

bool CExtRibbonOptionsPage::RibbonOptionsPage_InitContent()
{
	ASSERT_VALID( this );
//CExtRibbonOptionsDialog * pDialogRibbonOptions =
//		RibbonOptionsPage_GetContainer();
//	if( pDialogRibbonOptions == NULL )
//		return false;
	return true;
}

void CExtRibbonOptionsPage::RibbonOptionsPage_Apply()
{
	ASSERT_VALID( this );
}

void CExtRibbonOptionsPage::RibbonOptionsPage_Cancel()
{
	ASSERT_VALID( this );
}

BEGIN_MESSAGE_MAP( CExtRibbonOptionsPage, CExtResizableDialog )
	//{{AFX_MSG_MAP(CExtRibbonOptionsPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CExtRibbonOptionsPage::OnInitDialog()
{
	if( ! CExtResizableDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	ShowSizeGrip( FALSE );
HWND hWndCaptionAlignment = ::GetDlgItem( m_hWnd, IDD_EXT_RIBBON_QATB_CUSTOMIZE_CAPTION_ALIGNMENT );
	if( hWndCaptionAlignment != NULL )
	{
		AddAnchor( hWndCaptionAlignment, __RDA_LT, __RDA_RT );
		int nTextLen = ::GetWindowTextLength( hWndCaptionAlignment );
		if( nTextLen > 0 )
		{
			::GetWindowText(
				hWndCaptionAlignment,
				m_strPageTitleLong.GetBuffer( nTextLen + 2 ),
				nTextLen + 1
				);
			m_strPageTitleLong.ReleaseBuffer();
			m_strPageTitleShort = m_strPageTitleLong;
		} // if( nTextLen > 0 )
	} // if( hWndCaptionAlignment != NULL )
	ModifyStyle( 0, WS_BORDER, SWP_FRAMECHANGED );
	return TRUE;
}

void CExtRibbonOptionsPage::OnEraseBackground(
	CDC & dc,
	const CRect & rcClient
	)
{
	ASSERT_VALID( this );
CExtPaintManager * pPM = PmBridge_GetPM();
bool bDefaultBackground = pPM->Ribbon_OptionsPageBackgroundIsDefault();
	if( bDefaultBackground )
	{
		if( ! pPM->PaintDockerBkgnd( true, dc, this ) )
			dc.FillSolidRect( &rcClient, pPM->GetColor( CExtPaintManager::CLR_3DFACE_OUT) );
	}
	else
	{
		dc.FillSolidRect( &rcClient, m_clrPageBkgnd );
		if( ! m_bmpPageWaterMark.IsEmpty() )
		{
			CSize _sizeBmpPageWaterMark = m_bmpPageWaterMark.GetSize();
			CRect rcDst( 0, 0, _sizeBmpPageWaterMark.cx, _sizeBmpPageWaterMark.cy );
			if( dc.RectVisible( &rcDst ) )
				m_bmpPageWaterMark.Draw( dc.m_hDC, rcDst );
		} // if( ! m_bmpPageWaterMark.IsEmpty() )
	}
HWND hWndCaptionAlignment = ::GetDlgItem( m_hWnd, IDD_EXT_RIBBON_QATB_CUSTOMIZE_CAPTION_ALIGNMENT );
	if(		hWndCaptionAlignment != NULL
		&&	( ! m_strPageTitleLong.IsEmpty() )
		&&	m_fontPageCaption.GetSafeHandle() != NULL
		)
	{
		CRect rcCaption;
		::GetWindowRect( hWndCaptionAlignment, &rcCaption );
		ScreenToClient( &rcCaption );
		if( dc.RectVisible( &rcCaption ) )
		{
			INT nCaptionTextLen = INT( m_strPageTitleLong.GetLength() );
			int nBkModeOld = dc.SetBkMode( TRANSPARENT );
			COLORREF clrTextOld = dc.SetTextColor( bDefaultBackground ? pPM->GetColor( COLOR_BTNTEXT ) : m_clrPageText );
			CFont * pOldFont = dc.SelectObject( &m_fontPageCaption );
			if( bDefaultBackground )
			{
				COLORREF clrSaved = dc.SetTextColor( pPM->GetColor( COLOR_3DFACE ) );
				CRect rcCaptionShadow = rcCaption;
				rcCaptionShadow.OffsetRect( 1, 1 );
				CExtRichContentLayout::stat_DrawText( dc.m_hDC, LPCTSTR( m_strPageTitleLong ), nCaptionTextLen, &rcCaptionShadow, DT_SINGLELINE|DT_LEFT|DT_VCENTER|DT_END_ELLIPSIS, 0 );
				dc.SetTextColor( clrSaved );
			}
			CExtRichContentLayout::stat_DrawText( dc.m_hDC, LPCTSTR( m_strPageTitleLong ), nCaptionTextLen, &rcCaption, DT_SINGLELINE|DT_LEFT|DT_VCENTER|DT_END_ELLIPSIS, 0 );
			dc.SelectObject( pOldFont );
			dc.SetTextColor( clrTextOld );
			dc.SetBkMode( nBkModeOld );
		} // if( dc.RectVisible( &rcCaption ) )
	} // if( hWndCaptionAlignment != NULL . . .
}

void CExtRibbonOptionsPage::PostNcDestroy()
{
	delete this;
}

LRESULT CExtRibbonOptionsPage::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch( message )
	{
	case WM_ERASEBKGND:
		//if( ! PmBridge_GetPM()->Ribbon_OptionsPageBackgroundIsDefault() )
			return FALSE;
	break;
	case WM_PAINT:
		//if( ! PmBridge_GetPM()->Ribbon_OptionsPageBackgroundIsDefault() )
		{
			CPaintDC dcPaint( this );
			CRect rcClient;
			GetClientRect( &rcClient );
			CExtMemoryDC dc( &dcPaint, &rcClient );
			OnEraseBackground( dc, rcClient );
			return TRUE;
		}
	break;
	} // switch( message )
LRESULT lResult = CExtResizableDialog::WindowProc( message,  wParam,  lParam );
	return lResult;
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonOptionsPageCustomizeQATB

IMPLEMENT_DYNCREATE( CExtRibbonOptionsPageCustomizeQATB, CExtRibbonOptionsPage );

CExtRibbonOptionsPageCustomizeQATB::CExtRibbonOptionsPageCustomizeQATB()
	: CExtRibbonOptionsPage( IDD_EXT_RIBBON_QATB_CUSTOMIZE_PAGE )
	, m_pNodeColQATB( NULL )
{
	ASSERT( __EXT_RIBBON_OPTIONS_DIALOG_PAGE_CQATB == IDD_EXT_RIBBON_QATB_CUSTOMIZE_PAGE );

	m_listBoxChooseCommandsFrom.m_bAllowDragStart           = false;

	m_listBoxChooseCommandsFrom.m_bShowSelAlways            = true;
	m_listBoxChooseCommandsFrom.m_bRibbonQatbMode           = true;
	m_listBoxChooseCommandsFrom.m_bRibbonQatbTargetListBox  = false;

	m_listBoxCustomizeTarget.m_bShowSelAlways               = true;
	m_listBoxCustomizeTarget.m_bRibbonQatbMode              = true;
	m_listBoxCustomizeTarget.m_bRibbonQatbTargetListBox     = true;

	//{{AFX_DATA_INIT(CExtRibbonOptionsPageCustomizeQATB)
	//}}AFX_DATA_INIT
}

CExtRibbonOptionsPageCustomizeQATB::~CExtRibbonOptionsPageCustomizeQATB()
{
	if( m_pNodeColQATB != NULL )
	{
		ASSERT_VALID( m_pNodeColQATB );
		delete m_pNodeColQATB;
	}
}

UINT CExtRibbonOptionsPageCustomizeQATB::RibbonOptionsPage_GetID() const
{
	ASSERT_VALID( this );
	return IDD_EXT_RIBBON_QATB_CUSTOMIZE_PAGE;
}

bool CExtRibbonOptionsPageCustomizeQATB::RibbonOptionsPage_InitContent()
{
	ASSERT_VALID( this );
CExtRibbonBar * pRibbonBar = RibbonOptionsPage_GetBar();
	if( pRibbonBar == NULL )
		return false;
	m_checkShowQatbBelow.SetCheck( pRibbonBar->RibbonQuickAccessBar_AboveTheRibbonGet() ? 0 : 1 );
CExtRibbonNodeTabPageCollection * pColNode = pRibbonBar->Ribbon_GetTabPageRootNode();
	if( pColNode != NULL )
	{
		ASSERT_VALID( pColNode );
		// initialize categories combo box
		INT nPageNodeIndex, nPageNodeCount = INT( pColNode->GetNodeCount() );
		for( nPageNodeIndex = 0; nPageNodeIndex < nPageNodeCount; nPageNodeIndex ++ )
		{
			CExtCustomizeCmdTreeNode * pNode = pColNode->ElementAt( nPageNodeIndex );
			ASSERT_VALID( pNode );
			LPCTSTR strNodeText = pNode->GetTextInToolbar( NULL );
			if( strNodeText == NULL || _tcslen( strNodeText ) == 0 )
				strNodeText = pNode->GetTextInMenu( NULL );
			if( strNodeText == NULL )
				strNodeText = _T("");
			CExtSafeString strFmt, strInComboText;
			if( ! g_ResourceManager->LoadString( strFmt, IDS_EXT_RIBBON_CUSTOMIZE_NODE_TAB_FMT ) )
				strFmt = _T("%s Tab");
			strInComboText.Format( LPCTSTR(strFmt), strNodeText );
			m_comboChooseCommandsFrom.InsertString( nPageNodeIndex, LPCTSTR(strInComboText) );
		} // for( nPageNodeIndex = 0; nPageNodeIndex < nPageNodeCount; nPageNodeIndex ++ )

		CExtRibbonNode * pFileNode = pRibbonBar->Ribbon_GetFileRootNode();
		if( pFileNode != NULL )
		{
			CExtSafeString strFileMenu;
			strFileMenu = pFileNode->GetTextInToolbar( NULL, true );
			if( strFileMenu.IsEmpty() )
			{
				strFileMenu = pFileNode->GetTextInMenu( NULL, true );
				if( strFileMenu.IsEmpty() )
				{
					const CExtCustomizeCmdScreenTip * pCCST = pFileNode->CmdScreenTipFindForDisplaying();
					if( pCCST != NULL )
						strFileMenu = pCCST->CaptionMainGet();
				}
			}
			if( ! strFileMenu.IsEmpty() )
				m_comboChooseCommandsFrom.AddString( LPCTSTR(strFileMenu) );
		}

		CExtSafeString strAllCommands;
		if( ! g_ResourceManager->LoadString( strAllCommands, IDS_EXT_ALL_COMMANDS ) )
			strAllCommands = _T("All commands");
		m_comboChooseCommandsFrom.AddString( LPCTSTR(strAllCommands) );
	
		m_comboChooseCommandsFrom.SetCurSel( 0 );
		OnSrcComboSelEndOK();
	} // if( pColNode != NULL )

	// initialize QATB list box
CExtRibbonNodeQuickAccessButtonsCollection * pNodeColQATB =
		pRibbonBar->Ribbon_GetQuickAccessRootNode();
	if( pNodeColQATB == NULL )
		return false;
	ASSERT_VALID( pNodeColQATB );
	ASSERT( m_pNodeColQATB == NULL );
	m_pNodeColQATB =
		DYNAMIC_DOWNCAST(
			CExtRibbonNodeQuickAccessButtonsCollection,
			pNodeColQATB->CloneNode()
			);
	if( m_pNodeColQATB == NULL )
		return false;
	ASSERT_VALID( m_pNodeColQATB );
INT nQatbNodeIndex, nRibbonIndexInQATB, nQatbNodeCount = m_pNodeColQATB->GetNodeCount();
#ifdef _DEBUG
INT nRibbonCountInQATB = pRibbonBar->RibbonQuickAccessButton_GetCount( false );
	ASSERT( nRibbonCountInQATB == (nQatbNodeCount+1) ); // +1 - for content expand button in QATB
	nRibbonCountInQATB;
#endif // _DEBUG
	for( nQatbNodeIndex = 0, nRibbonIndexInQATB = 0; nQatbNodeIndex < nQatbNodeCount; nRibbonIndexInQATB ++ )
	{
		CExtCustomizeCmdTreeNode * pNodeQATB =
			m_pNodeColQATB->ElementAt( nQatbNodeIndex );
		ASSERT_VALID( pNodeQATB );
		CExtBarButton * pTBB = pRibbonBar->RibbonQuickAccessButton_GetAt( nRibbonIndexInQATB, false );
		ASSERT_VALID( pTBB );
#ifdef _DEBUG
		UINT nCmdID_Node = pNodeQATB->GetCmdID( false );
		UINT nCmdID_TBB = pTBB->GetCmdID( false );
		ASSERT( nCmdID_Node == nCmdID_TBB );
		nCmdID_Node;
		nCmdID_TBB;
#endif // _DEBUG
		if( (pTBB->GetStyle()&TBBS_HIDDEN) == 0 )
		{
			pNodeQATB->Ribbon_InitCommandsListBox( m_listBoxCustomizeTarget, -1, true, false );
			nQatbNodeIndex ++;
		} // if( (pTBB->GetStyle()&TBBS_HIDDEN) == 0 )
		else
		{
			pNodeQATB->RemoveSelf( NULL, false );
			nQatbNodeCount --;
		} // else from if( (pTBB->GetStyle()&TBBS_HIDDEN) == 0 )
	} // for( nQatbNodeIndex = 0, nRibbonIndexInQATB = 0; nQatbNodeIndex < nQatbNodeCount; nRibbonIndexInQATB ++ )
	if( m_listBoxCustomizeTarget.GetCount() > 0 )
		m_listBoxCustomizeTarget.SetCurSel( 0 );

	_UpdateButtons();
	return true;
}

void CExtRibbonOptionsPageCustomizeQATB::RibbonOptionsPage_Apply()
{
	ASSERT_VALID( this );
CExtRibbonBar * pRibbonBar = RibbonOptionsPage_GetBar();
	if( pRibbonBar == NULL )
		return;
bool bQuatbIsNowAbove = pRibbonBar->RibbonQuickAccessBar_AboveTheRibbonGet() ? true : false;
bool bQuatbShouldBeAbove = m_checkShowQatbBelow.GetCheck() ? false : true;
	if( bQuatbIsNowAbove != bQuatbShouldBeAbove )
	{
		if( bQuatbShouldBeAbove )
			pRibbonBar->_OnCmdRibbonQuickAccessToolbarPlaceAbove();
		else
			pRibbonBar->_OnCmdRibbonQuickAccessToolbarPlaceBelow();
	} // if( bQuatbIsNowAbove != bQuatbShouldBeAbove )
CExtRibbonNodeQuickAccessButtonsCollection * pParentNode = pRibbonBar->Ribbon_GetQuickAccessRootNode();
	if( pParentNode != NULL )
	{
		ASSERT_VALID( pParentNode );
		ASSERT_VALID( m_pNodeColQATB );
		bool bApplyingInitialState = m_buttonReset.IsWindowEnabled() ? false : true;
		INT nIndex, nCount = pRibbonBar->RibbonQuickAccessButton_GetCount();
		for( nIndex = 0; nIndex < nCount; )
		{
			CExtBarButton * pTBB = pRibbonBar->RibbonQuickAccessButton_GetAt( nIndex );
			ASSERT_VALID( pTBB );
			if( pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonQuickAccessContentExpand ) ) )
			{
				nIndex ++;
				continue;
			}
			CExtCustomizeCmdTreeNode * pNode = pTBB->GetCmdNode();
			if( pNode == NULL )
			{
				nIndex ++;
				continue;
			}
			ASSERT_VALID( pNode );
			if( (pNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
			{
				INT nPlainIndex = pRibbonBar->_GetIndexOf( pTBB );
				ASSERT( nPlainIndex >= 0 );
				pRibbonBar->m_arrQuickAccessButtons.RemoveAt( nIndex );
				pRibbonBar->CExtToolControlBar::RemoveButton( nPlainIndex );
				pNode->RemoveSelf( NULL );
				nCount --;
				continue;
			} // if( (pNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
			else
			{
				if( bApplyingInitialState )
					pTBB->ModifyStyle( 0, TBBS_HIDDEN ); // show all initial buttons
				else
					pTBB->ModifyStyle( TBBS_HIDDEN ); // hide all initial buttons
				nIndex ++;
				continue;
			} // else from if( (pNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
		} // for( nIndex = 0; nIndex < nCount; )
		if( ! bApplyingInitialState )
		{
			INT nQatbNodeIndex, nQatbNodeCount = m_pNodeColQATB->GetNodeCount();
			for( nQatbNodeIndex = 0; nQatbNodeIndex < nQatbNodeCount; nQatbNodeIndex++ )
			{
				CExtCustomizeCmdTreeNode * pNodeQATB =
					m_pNodeColQATB->ElementAt( nQatbNodeIndex );
				ASSERT_VALID( pNodeQATB );
				CExtRibbonNode * pRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNodeQATB );
				if( pRibbonNode == NULL )
					continue;
				CExtBarButton * pButton = pRibbonNode->OnRibbonCreateBarButton( pRibbonBar, NULL );
				CExtCustomizeCmdTreeNode * pNewNode = pRibbonNode->CloneNode();
				if( pNewNode == NULL )
					continue;
				CExtRibbonNode * pNewRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNewNode );
				if( pNewRibbonNode != NULL )
				{
					CArray < DWORD, DWORD > arrILEtoILV;
					arrILEtoILV.Add(
						__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
							__EXT_RIBBON_ILE_MAX,
							__EXT_RIBBON_ILV_SIMPLE_SMALL,
							false
							)
						);
					pNewRibbonNode->RibbonILE_RuleArraySet( arrILEtoILV );
				} // if( pNewRibbonNode != NULL )
				pNewNode->ModifyFlags( __ECTN_RIBBON_QA_CLONED_COPY );
				pParentNode->InsertNode( NULL, pNewNode );
				pNewNode->SetParentNode( pParentNode );
				pButton->SetBasicCmdNode( pNewNode );
				pButton->SetCustomizedCmdNode( pNewNode );
				pRibbonBar->CExtToolControlBar::InsertSpecButton( -1, pButton, FALSE );
				if( pRibbonBar->m_pQACEB == NULL )
				{
					pRibbonBar->m_arrQuickAccessButtons.Add( pButton );
					pRibbonBar->m_pQACEB = new CExtRibbonButtonQuickAccessContentExpand( pRibbonBar );
					pRibbonBar->InsertSpecButton( -1, pRibbonBar->m_pQACEB, FALSE );
					pRibbonBar->m_arrQuickAccessButtons.Add( pRibbonBar->m_pQACEB );
				}
				else
				{
					ASSERT( INT(pRibbonBar->m_arrQuickAccessButtons.GetSize()) > 0 );
					pRibbonBar->m_arrQuickAccessButtons.InsertAt( INT(pRibbonBar->m_arrQuickAccessButtons.GetSize()) - 1, pButton, 1 );
				}
			} // for( nQatbNodeIndex = 0; nQatbNodeIndex < nQatbNodeCount; nQatbNodeIndex++ )
#ifdef _DEBUG
			INT nRibbonCountInQATB = pRibbonBar->RibbonQuickAccessButton_GetCount( true );
			ASSERT( nRibbonCountInQATB == (nQatbNodeCount+1) ); // +1 - for content expand button in QATB
			nRibbonCountInQATB;
#endif // _DEBUG
		} // if( ! bApplyingInitialState )
		pRibbonBar->Ribbon_OnRecalcLayout();
		if(		pRibbonBar->PmBridge_GetPM()->Ribbon_FileButtonIsItegrationSupported()
			&&	pRibbonBar->RibbonQuickAccessBar_AboveTheRibbonGet()
			)
		{
			CWnd * pWnd = pRibbonBar->GetParent();
			if(		pWnd != NULL
				&&	( pWnd->GetStyle() & WS_CHILD ) == 0
				)
				pWnd->SendMessage( WM_NCPAINT );
		}
	} // if( pParentNode != NULL )
}

void CExtRibbonOptionsPageCustomizeQATB::RibbonOptionsPage_Cancel()
{
	ASSERT_VALID(this);
}

void CExtRibbonOptionsPageCustomizeQATB::_UpdateButtons()
{
	ASSERT_VALID( this );
BOOL bEnabledMoveUP = FALSE, bEnabledMoveDOWN = FALSE,
		bEnabledAdd = FALSE, bEnabledRemove = FALSE, bEnabledReset = FALSE;
	if( m_pNodeColQATB != NULL )
	{
		ASSERT_VALID( m_pNodeColQATB );
		CExtRibbonBar * pRibbonBar = RibbonOptionsPage_GetBar();
		if( pRibbonBar != NULL )
		{
			ASSERT_VALID( pRibbonBar );
			CExtRibbonNodeQuickAccessButtonsCollection * pNodeColQATB =
				pRibbonBar->Ribbon_GetQuickAccessRootNode();
			if( pNodeColQATB != NULL && m_pNodeColQATB != NULL )
			{
				ASSERT_VALID( pNodeColQATB );
				ASSERT_VALID( m_pNodeColQATB );
				INT nDstSel = m_listBoxCustomizeTarget.GetCurSel(),
					nDstCount = m_listBoxCustomizeTarget.GetCount(),
					nSrcSel = m_listBoxChooseCommandsFrom.GetCurSel(),
					nSrcCount = m_listBoxChooseCommandsFrom.GetCount();
				if( 0 <= nDstSel && nDstSel < nDstCount )
				{
					bEnabledRemove = TRUE;
					if( nDstSel != 0 )
						bEnabledMoveUP = TRUE;
					if( nDstSel != (nDstCount-1) )
						bEnabledMoveDOWN = TRUE;
				} // if( 0 <= nDstSel && nDstSel < nDstCount )
				if( nSrcCount >= 0 && nSrcSel >= 0 )
				{
					bEnabledAdd = TRUE;

					if( pRibbonBar->_OnRibbonBarQueryUniqueButtonModeForQATB() )
					{
						CExtCustomizeCmdTreeNode * pNode = m_listBoxChooseCommandsFrom.m_arrCmds[ nSrcSel ];
						ASSERT_VALID( pNode );
						UINT nAddCmdID = pNode->GetCmdID( false );
						INT nIndex, nCount = m_pNodeColQATB->GetNodeCount();
						for( nIndex = 0; nIndex < nCount; nIndex ++ )
						{
							CExtCustomizeCmdTreeNode * pNodeInsideQATB = m_pNodeColQATB->ElementAt( nIndex );
							ASSERT_VALID( pNodeInsideQATB );
							UINT nCmdID = pNodeInsideQATB->GetCmdID( false );
							if( nCmdID == nAddCmdID )
							{
								bEnabledAdd = FALSE; // already present inside QATB
								break;
							}
							// for axel.lehnert@innoplus.de, 2/2/2016
							bEnabledAdd = pRibbonBar->_OnRibbonBarQueryEnableAddNodeInsideQATB(pNodeInsideQATB);
							if ( !bEnabledAdd )
								break;
							// for axel.lehnert@innoplus.de, 2/2/2016
						}
					} // if( pRibbonBar->_OnRibbonBarQueryUniqueButtonModeForQATB() )
				}
				INT nRibbonIndexInQATB, nRibbonCountInQATB = pRibbonBar->RibbonQuickAccessButton_GetCount(),
						nOwnIndex, nOwnCount = m_pNodeColQATB->GetNodeCount();
				for( nRibbonIndexInQATB = 0, nOwnIndex = 0; nRibbonIndexInQATB < nRibbonCountInQATB; nRibbonIndexInQATB ++ )
				{
					CExtBarButton * pTBB = pRibbonBar->RibbonQuickAccessButton_GetAt( nRibbonIndexInQATB );
					ASSERT_VALID( pTBB );
					if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonQuickAccessContentExpand) ) )
						continue;
					CExtCustomizeCmdTreeNode * pNodeInRibbonQATB = pTBB->GetCmdNode( true );
					ASSERT_VALID( pNodeInRibbonQATB );
					if( (pNodeInRibbonQATB->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
						continue;

					if( nOwnIndex >= nOwnCount )
					{
						bEnabledReset = TRUE;
						break;
					}

					CExtCustomizeCmdTreeNode * pOwnNode = m_pNodeColQATB->ElementAt( nOwnIndex );
					ASSERT_VALID( pOwnNode );
//					if( (pOwnNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
//					{
//						bEnabledReset = TRUE;
//						break;
//					}
					UINT nCmdID_Ribbon = pNodeInRibbonQATB->GetCmdID( false );
					UINT nCmdID_Own = pOwnNode->GetCmdID( false );
					if( nCmdID_Ribbon != nCmdID_Own )
					{
						bEnabledReset = TRUE;
						break;
					}
					nOwnIndex ++;
				} // for( nRibbonIndexInQATB = 0, nOwnIndex = 0; nRibbonIndexInQATB < nRibbonCountInQATB; nRibbonIndexInQATB ++ )
				if( ( ! bEnabledReset ) && nOwnIndex < nOwnCount )
					bEnabledReset = TRUE;
			} // if( pNodeColQATB != NULL && m_pNodeColQATB != NULL )
		} // if( pRibbonBar != NULL )
	} // if( m_pNodeColQATB != NULL )
	m_buttonMoveUp.EnableWindow( bEnabledMoveUP );
	m_buttonMoveDown.EnableWindow( bEnabledMoveDOWN );
	m_buttonAdd.EnableWindow( bEnabledAdd );
	m_buttonRemove.EnableWindow( bEnabledRemove );
	m_buttonReset.EnableWindow( bEnabledReset );
}

void CExtRibbonOptionsPageCustomizeQATB::DoDataExchange(CDataExchange* pDX)
{
	CExtRibbonOptionsPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExtRibbonOptionsPageCustomizeQATB)
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_CHOOSE_COMMANDS_FROM, m_labelChooseCommandsFrom );
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_CHOOSE_COMMANDS_FROM_COMBO, m_comboChooseCommandsFrom );
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_ADD, m_buttonAdd );
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_REMOVE, m_buttonRemove );
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_LIST_FOR, m_labelCustomizeTarget );
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_LIST_FOR_COMBO, m_comboCustomizeTarget );
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_RESET, m_buttonReset );
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_MODIFY, m_buttonModify );
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_MOVE_UP, m_buttonMoveUp );
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_MOVE_DOWN, m_buttonMoveDown );
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_CHK_BELOW_THE_RIBBON, m_checkShowQatbBelow );
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_KB_STATIC, m_labelKB );
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_KB, m_buttonKB );
	//}}AFX_DATA_MAP
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_CHOOSE_COMMANDS_FROM_LB, m_listBoxChooseCommandsFrom );
	DDX_Control( pDX, IDD_EXT_RIBBON_QATB_CUSTOMIZE_LIST_FOR_LB, m_listBoxCustomizeTarget );
}

BEGIN_MESSAGE_MAP( CExtRibbonOptionsPageCustomizeQATB, CExtRibbonOptionsPage )
	//{{AFX_MSG_MAP(CExtRibbonOptionsPageCustomizeQATB)
	ON_BN_CLICKED( IDD_EXT_RIBBON_QATB_CUSTOMIZE_CHK_BELOW_THE_RIBBON, OnShowQatbBelowTheRibbon )
	ON_BN_CLICKED( IDD_EXT_RIBBON_QATB_CUSTOMIZE_KB, OnCustomuzeKeyboard )
	ON_BN_CLICKED( IDD_EXT_RIBBON_QATB_CUSTOMIZE_ADD, OnQatbButtonAdd )
	ON_BN_CLICKED( IDD_EXT_RIBBON_QATB_CUSTOMIZE_REMOVE, OnQatbButtonRemove )
	ON_BN_CLICKED( IDD_EXT_RIBBON_QATB_CUSTOMIZE_RESET, OnQatbButtonReset )
	ON_BN_CLICKED( IDD_EXT_RIBBON_QATB_CUSTOMIZE_MODIFY, OnQatbButtonModify )
	ON_BN_CLICKED( IDD_EXT_RIBBON_QATB_CUSTOMIZE_MOVE_UP, OnQatbButtonMoveUp )
	ON_BN_CLICKED( IDD_EXT_RIBBON_QATB_CUSTOMIZE_MOVE_DOWN, OnQatbButtonMoveDown )
	ON_CBN_SELENDOK( IDD_EXT_RIBBON_QATB_CUSTOMIZE_CHOOSE_COMMANDS_FROM_COMBO, OnSrcComboSelEndOK )
	ON_CBN_SELENDOK( IDD_EXT_RIBBON_QATB_CUSTOMIZE_LIST_FOR_COMBO, OnDstComboSelEndOK )
	ON_LBN_SELCHANGE( IDD_EXT_RIBBON_QATB_CUSTOMIZE_CHOOSE_COMMANDS_FROM_LB, OnSrcListBoxSelEndOK )
	ON_LBN_SELCHANGE( IDD_EXT_RIBBON_QATB_CUSTOMIZE_LIST_FOR_LB, OnDstListBoxSelEndOK )
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CExtRibbonOptionsPageCustomizeQATB::OnInitDialog()
{
	if( ! CExtRibbonOptionsPage::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}

#if ( _MFC_VER != 0x700 )
	m_listBoxChooseCommandsFrom.m_rcNcsbCustomNcAreaSizes.SetRect( 0, 0, 0, 0 );
	m_listBoxCustomizeTarget.m_rcNcsbCustomNcAreaSizes.SetRect( 0, 0, 0, 0 );
	m_listBoxChooseCommandsFrom.m_clrNcsbCustomNcAreaFill = RGB( 0, 0, 0 );
	m_listBoxCustomizeTarget.m_clrNcsbCustomNcAreaFill = RGB( 0, 0, 0 );
	m_listBoxChooseCommandsFrom.ModifyStyle( WS_BORDER, 0, SWP_FRAMECHANGED );
	m_listBoxCustomizeTarget.ModifyStyle( WS_BORDER, 0, SWP_FRAMECHANGED );
	m_listBoxChooseCommandsFrom.ModifyStyleEx( WS_EX_STATICEDGE|WS_EX_CLIENTEDGE|WS_EX_DLGMODALFRAME, 0, SWP_FRAMECHANGED );
	m_listBoxCustomizeTarget.ModifyStyleEx( WS_EX_STATICEDGE|WS_EX_CLIENTEDGE|WS_EX_DLGMODALFRAME, 0, SWP_FRAMECHANGED );
#endif // ( _MFC_VER != 0x700 )

//	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_CHOOSE_COMMANDS_FROM, __RDA_LT, CSize( 50, 0 ) );
	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_CHOOSE_COMMANDS_FROM_COMBO, __RDA_LT, CSize( 50, 0 ) );
	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_CHOOSE_COMMANDS_FROM_LB, __RDA_LT, CSize( 50, 100 ) );

	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_ADD, CSize( 50, 50 ) );
	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_REMOVE, CSize( 50, 50 ) );

	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_LIST_FOR, CSize( 50, 0 ) );
	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_LIST_FOR_COMBO, CSize( 50, 0 ), __RDA_RT );
	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_LIST_FOR_LB, CSize( 50, 0 ), __RDA_RB );
	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_RESET, CSize( 50, 100 ) );
	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_MODIFY, CSize( 50, 100 ) );

	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_MOVE_UP, CSize( 100, 50 ) );
	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_MOVE_DOWN, CSize( 100, 50 ) );

	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_CHK_BELOW_THE_RIBBON, __RDA_LB );
	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_KB_STATIC, __RDA_LB );
	AddAnchor( IDD_EXT_RIBBON_QATB_CUSTOMIZE_KB, __RDA_LB );

	if( ! PmBridge_GetPM()->Ribbon_OptionsPageBackgroundIsDefault() )
	{
		m_labelChooseCommandsFrom.SetBkColor( m_clrPageBkgnd );
		m_labelCustomizeTarget.SetBkColor( m_clrPageBkgnd );
		m_labelKB.SetBkColor( m_clrPageBkgnd );
		m_checkShowQatbBelow.SetBkColor( m_clrPageBkgnd );

		m_buttonAdd.SetBkColor( m_clrPageBkgnd );
		m_buttonRemove.SetBkColor( m_clrPageBkgnd );
		m_buttonReset.SetBkColor( m_clrPageBkgnd );
		m_buttonModify.SetBkColor( m_clrPageBkgnd );
		m_buttonKB.SetBkColor( m_clrPageBkgnd );

		m_labelChooseCommandsFrom.SetTextColor( true, m_clrPageText );
		m_labelCustomizeTarget.SetTextColor( true, m_clrPageText );
		m_labelKB.SetTextColor( true, m_clrPageText );
		m_checkShowQatbBelow.SetTextColorNormal( m_clrPageText );
		m_checkShowQatbBelow.SetTextColorDisabled( m_clrPageText );
		m_checkShowQatbBelow.SetTextColorHover( m_clrPageText );
		m_checkShowQatbBelow.SetTextColorPressed( m_clrPageText );
	} // if( ! PmBridge_GetPM()->Ribbon_OptionsPageBackgroundIsDefault() )

	m_labelCustomizeTarget.ShowWindow( SW_HIDE );
	m_comboCustomizeTarget.ShowWindow( SW_HIDE );
	m_buttonModify.EnableWindow( FALSE );
	m_buttonModify.ShowWindow( SW_HIDE );
//	m_labelKB.EnableWindow( FALSE );
//	m_buttonKB.EnableWindow( FALSE );
	VERIFY( g_ResourceManager->LoadString( m_strPageTitleShort, IDS_EXT_RIBBON_QATB_CUSTOMIZE_SHORT_PAGE_NAME ) );
	return TRUE;
}

void CExtRibbonOptionsPageCustomizeQATB::OnShowQatbBelowTheRibbon()
{
	ASSERT_VALID( this );
}

void CExtRibbonOptionsPageCustomizeQATB::OnCustomuzeKeyboard()
{
	ASSERT_VALID( this );
HWND hWndOwn = m_hWnd;
	ASSERT( ::IsWindow( hWndOwn ) );
CExtRibbonBar * pRibbonBar = RibbonOptionsPage_GetBar();
	if( pRibbonBar == NULL )
		return;
CWnd * pWndParent = RibbonOptionsPage_GetContainer();
	ASSERT_VALID( pWndParent );
HWND hWndParent = pWndParent->m_hWnd;
	ASSERT( ::IsWindow( hWndParent ) );
CExtCustomizeForm _form;
	try
	{
		_form.m_bAutoDestroyCustomizeForm = false;
		CExtCustomizePageKeyboard * pPageKeyboard = new CExtCustomizePageKeyboard;
		_form.AddPage( pPageKeyboard );
		pPageKeyboard->m_strProfileName = g_CmdManager->ProfileNameFromWnd( pRibbonBar->m_hWnd );
		DWORD dwStyle = (DWORD)-1;
		DWORD dwExStyle = 0;
		if( ( g_ResourceManager->OnQueryLangLayout() & LAYOUT_RTL ) != 0 )
			dwExStyle |= WS_EX_LAYOUTRTL;
		::EnableWindow( hWndParent, FALSE );
		if( _form.Create( pWndParent, dwStyle, dwExStyle ) )
		{
			for( MSG msg; _form.GetSafeHwnd() != NULL;  )
			{
				::WaitMessage();
				for( ; PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ); )
				{ // process all the messages in the message queue
					if(		msg.hwnd == _form.m_hWnd
						&&	(		msg.message == WM_DESTROY
							||		msg.message == WM_CLOSE
							||	(	msg.message == WM_COMMAND
								&&	(	msg.wParam == IDOK
									||	msg.wParam == IDCANCEL
									)
								)
							||	(	msg.message == WM_SYSCOMMAND
								&&	msg.wParam == SC_CLOSE
								)
							)
						)
					{
						if( ::IsWindow( hWndParent ) )
						{
							if( ! ::IsWindowEnabled( hWndParent ) )
								::EnableWindow( hWndParent, TRUE );
							m_buttonKB.SetFocus();
						} // if( ::IsWindow( hWndParent ) )
					}
					if( ! AfxGetThread()->PumpMessage() )
					{
						PostQuitMessage(0);
						break; // signal WM_QUIT received
					}
				} // process all the messages in the message queue
			} // for( MSG msg; _form.GetSafeHwnd() != NULL;  )
		} // if( _form.Create( this, dwStyle, dwExStyle ) )
	} // try
	catch( CException * pExept )
	{
		ASSERT( FALSE );
		pExept->Delete();
	} // catch( CException * pExept )
	catch( ... )
	{
		ASSERT( FALSE );
	} // catch( ... )
	if( hWndParent != NULL && ::IsWindow( hWndParent ) )
	{
		if( ! ::IsWindowEnabled( hWndParent ) )
			::EnableWindow( hWndParent, TRUE );
		m_buttonKB.SetFocus();
	} // if( hWndParent != NULL && ::IsWindow( hWndParent ) )
	if( _form.GetSafeHwnd() != NULL )
		_form.DestroyWindow();
	if( ::IsWindow( hWndOwn ) )
		pRibbonBar->OnUpdateAccelGlobalInfo( false );
}

void CExtRibbonOptionsPageCustomizeQATB::OnQatbButtonAdd()
{
	ASSERT_VALID( this );
CExtRibbonBar * pRibbonBar = RibbonOptionsPage_GetBar();
	if( pRibbonBar == NULL )
		return;
	ASSERT_VALID( pRibbonBar );
	if( m_pNodeColQATB == NULL )
		return;
	ASSERT_VALID( m_pNodeColQATB );
INT nSrcSel = m_listBoxChooseCommandsFrom.GetCurSel();
	if( nSrcSel < 0 )
		return;
INT nDstPosLB = m_listBoxCustomizeTarget.GetCount(),
		nDstSel = m_listBoxCustomizeTarget.GetCurSel();
	if( nDstSel >= 0 )
		nDstPosLB = nDstSel + 1;
CExtCustomizeCmdTreeNode * pNode =
		 m_listBoxChooseCommandsFrom.m_arrCmds[ nSrcSel ];
	if( pNode == NULL )
	{ // ribbon separator item
	} // ribbon separator item
	else
	{ // normal item

		UINT nCmdIdBasic = pNode->GetCmdID( false );
		UINT nCmdIdEffective = pNode->GetCmdID( true );

		if( pRibbonBar->_OnRibbonBarQueryUniqueButtonModeForQATB() )
		{
			INT nIndex, nCount = m_pNodeColQATB->GetNodeCount();
			for( nIndex = 0; nIndex < nCount; nIndex ++ )
			{
				CExtCustomizeCmdTreeNode * pNodeInsideQATB = m_pNodeColQATB->ElementAt( nIndex );
				ASSERT_VALID( pNodeInsideQATB );
				UINT nCmdID = pNodeInsideQATB->GetCmdID( false );
				if( nCmdID == nCmdIdBasic )
					return; // already present inside QATB
			}			
		} // if( pRibbonBar->_OnRibbonBarQueryUniqueButtonModeForQATB() )

		CExtCustomizeCmdTreeNode * pNewNode = pNode->CloneNode();
		if( pNewNode == NULL )
			return;
		CExtRibbonNode * pNewRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNewNode );
		if( pNewRibbonNode == NULL )
		{

			pNewRibbonNode =
				new CExtRibbonNode(
					nCmdIdBasic,
					nCmdIdEffective,
					NULL,
					pNode->GetFlags(),
					NULL,
					NULL,
					NULL,
					pNode->GetLParam(),
					NULL
//#if (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)
//					,
//					INT nTextFieldWidth = 100,
//					INT nDropDownWidth = -2, // (-1) - auto calc, (-2) - same as button area
//					INT nDropDownHeightMax = 250
//#endif // (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)
				);
			delete pNewNode;
			pNewNode = pNewRibbonNode;
		} // if( pNewRibbonNode == NULL )
		ASSERT( pNewRibbonNode != NULL );
		CArray < DWORD, DWORD > arrILEtoILV;
		arrILEtoILV.Add(
			__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
				__EXT_RIBBON_ILE_MAX,
				__EXT_RIBBON_ILV_SIMPLE_SMALL,
				false
				)
			);
		pNewRibbonNode->RibbonILE_RuleArraySet( arrILEtoILV );
		pNewNode->ModifyFlags( __ECTN_RIBBON_QA_CLONED_COPY );
		m_pNodeColQATB->InsertNode( NULL, pNewNode, nDstPosLB );
		pNewNode->Ribbon_InitCommandsListBox( m_listBoxCustomizeTarget, nDstPosLB, true, false );
	} // normal item
	m_listBoxCustomizeTarget.SetCurSel( nDstPosLB );

	_UpdateButtons();
}

void CExtRibbonOptionsPageCustomizeQATB::OnQatbButtonRemove()
{
	ASSERT_VALID( this );
CExtRibbonBar * pRibbonBar = RibbonOptionsPage_GetBar();
	if( pRibbonBar == NULL )
		return;
	ASSERT_VALID( pRibbonBar );
	if( m_pNodeColQATB == NULL )
		return;
	ASSERT_VALID( m_pNodeColQATB );
INT nDstSel = m_listBoxCustomizeTarget.GetCurSel(),
		nDstCount = m_listBoxCustomizeTarget.GetCount();
	if( nDstCount == 0 || nDstSel < 0 )
		return;
	// for axel.lehnert@innoplus.de, 2/2/2016
	m_listBoxCustomizeTarget.m_arrCmds.RemoveAt(nDstSel, 1);
	m_listBoxCustomizeTarget.DeleteString( nDstSel );
	// for axel.lehnert@innoplus.de, 2/2/2016
	m_pNodeColQATB->RemoveNodes(nDstSel, 1);
	nDstCount --;
	if( nDstCount > 0 )
	{
		if( nDstSel >= nDstCount )
			nDstSel = nDstCount - 1;
		m_listBoxCustomizeTarget.SetCurSel( nDstSel );
	} // if( nDstCount > 0 )

	_UpdateButtons();
}

void CExtRibbonOptionsPageCustomizeQATB::OnQatbButtonReset()
{
	ASSERT_VALID( this );
//CExtRibbonBar * pRibbonBar = RibbonOptionsPage_GetBar();
//	if( pRibbonBar == NULL )
//		return;
//	pRibbonBar->_OnCmdRibbonQuickAccessToolbarReset( this );

CExtRibbonBar * pRibbonBar = RibbonOptionsPage_GetBar();
	if( pRibbonBar == NULL )
		return;
	ASSERT_VALID( pRibbonBar );
	if( m_pNodeColQATB == NULL )
		return;
	ASSERT_VALID( m_pNodeColQATB );
CExtRibbonNodeQuickAccessButtonsCollection * pNodeColQATB =
		pRibbonBar->Ribbon_GetQuickAccessRootNode();
	if( pNodeColQATB == NULL )
		return;

CExtSafeString strQuestionText, strQuestionCaption;
	if(		(! g_ResourceManager->LoadString(
				strQuestionText,
				ID_EXT_RIBBON_QATB_RESET_QUESTION_TEXT
				) )
		||	(! g_ResourceManager->LoadString(
				strQuestionCaption,
				ID_EXT_RIBBON_QATB_RESET_QUESTION_CAPTION
				) )
		)
		return;
#if (!defined __EXT_MFC_NO_MSG_BOX)
	if( ::ProfUISMsgBox( GetSafeHwnd(), LPCTSTR(strQuestionText), LPCTSTR(strQuestionCaption), MB_YESNO|MB_ICONWARNING ) != IDYES )
		return;
#else
	if( ::MessageBox( GetSafeHwnd(), LPCTSTR(strQuestionText), LPCTSTR(strQuestionCaption), MB_YESNO|MB_ICONWARNING ) != IDYES )
		return;
#endif

	m_listBoxCustomizeTarget.ResetContent();
	m_listBoxCustomizeTarget.m_arrCmds.RemoveAll();

	delete m_pNodeColQATB;
	m_pNodeColQATB =
		DYNAMIC_DOWNCAST(
			CExtRibbonNodeQuickAccessButtonsCollection,
			pNodeColQATB->CloneNode()
			);
	if( m_pNodeColQATB == NULL )
		return;
	ASSERT_VALID( m_pNodeColQATB );
INT nQatbNodeIndex, nQatbNodeCount = m_pNodeColQATB->GetNodeCount();
	for( nQatbNodeIndex = 0; nQatbNodeIndex < nQatbNodeCount; )
	{
		CExtCustomizeCmdTreeNode * pNodeQATB =
			m_pNodeColQATB->ElementAt( nQatbNodeIndex );
		ASSERT_VALID( pNodeQATB );
		if( (pNodeQATB->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
		{
			pNodeQATB->RemoveSelf( NULL, false );
			nQatbNodeCount --;
			continue;
		}
		pNodeQATB->Ribbon_InitCommandsListBox( m_listBoxCustomizeTarget, -1, true, false );
		nQatbNodeIndex ++;
	} // for( nQatbNodeIndex = 0; nQatbNodeIndex < nQatbNodeCount; )
	if( m_listBoxCustomizeTarget.GetCount() > 0 )
		m_listBoxCustomizeTarget.SetCurSel( 0 );

	_UpdateButtons();
}

void CExtRibbonOptionsPageCustomizeQATB::OnQatbButtonModify()
{
	ASSERT_VALID( this );
}

void CExtRibbonOptionsPageCustomizeQATB::OnQatbButtonMoveUp()
{
	ASSERT_VALID( this );
CExtRibbonBar * pRibbonBar = RibbonOptionsPage_GetBar();
	if( pRibbonBar == NULL )
		return;
	ASSERT_VALID( pRibbonBar );
	if( m_pNodeColQATB == NULL )
		return;
	ASSERT_VALID( m_pNodeColQATB );
INT nDstCount = m_listBoxCustomizeTarget.GetCount();
	if( nDstCount <= 1 )
		return;
INT nDstSel = m_listBoxCustomizeTarget.GetCurSel();
	if( nDstSel <= 0 )
		return;
CString s;
	m_listBoxCustomizeTarget.GetText( nDstSel, s );

	m_listBoxCustomizeTarget.DeleteString( nDstSel );

	m_pNodeColQATB->InsertNode(
		NULL,
		m_pNodeColQATB->ElementAt( nDstSel )->CloneNode(),
		nDstSel - 1
		);
	m_pNodeColQATB->RemoveNodes( nDstSel + 1, 1 );
	m_listBoxCustomizeTarget.m_arrCmds[ nDstSel - 1 ] = m_pNodeColQATB->ElementAt( nDstSel - 1 );
	m_listBoxCustomizeTarget.m_arrCmds[ nDstSel ] = m_pNodeColQATB->ElementAt( nDstSel );

	m_listBoxCustomizeTarget.InsertString( nDstSel - 1, LPCTSTR(s) );

	m_listBoxCustomizeTarget.SetCurSel( nDstSel - 1 );
	m_listBoxCustomizeTarget.Invalidate();

	_UpdateButtons();
}

void CExtRibbonOptionsPageCustomizeQATB::OnQatbButtonMoveDown()
{
	ASSERT_VALID( this );
CExtRibbonBar * pRibbonBar = RibbonOptionsPage_GetBar();
	if( pRibbonBar == NULL )
		return;
	ASSERT_VALID( pRibbonBar );
	if( m_pNodeColQATB == NULL )
		return;
	ASSERT_VALID( m_pNodeColQATB );
INT nDstCount = m_listBoxCustomizeTarget.GetCount();
	if( nDstCount <= 1 )
		return;
INT nDstSel = m_listBoxCustomizeTarget.GetCurSel();
	if( nDstSel < 0 || nDstSel >= (nDstCount-1) )
		return;
CString s;
	m_listBoxCustomizeTarget.GetText( nDstSel, s );

	m_listBoxCustomizeTarget.DeleteString( nDstSel );

	m_pNodeColQATB->InsertNode(
		NULL,
		m_pNodeColQATB->ElementAt( nDstSel )->CloneNode(),
		nDstSel + 2
		);
	m_pNodeColQATB->RemoveNodes( nDstSel, 1 );
	m_listBoxCustomizeTarget.m_arrCmds[ nDstSel ] = m_pNodeColQATB->ElementAt( nDstSel );
	m_listBoxCustomizeTarget.m_arrCmds[ nDstSel + 1 ] = m_pNodeColQATB->ElementAt( nDstSel + 1 );

	m_listBoxCustomizeTarget.InsertString( nDstSel + 1, LPCTSTR(s) );

	m_listBoxCustomizeTarget.SetCurSel( nDstSel + 1 );
	m_listBoxCustomizeTarget.Invalidate();

	_UpdateButtons();
}

void CExtRibbonOptionsPageCustomizeQATB::OnSrcComboSelEndOK()
{
	ASSERT_VALID( this );
	m_listBoxChooseCommandsFrom.ResetContent();
CExtRibbonBar * pRibbonBar = RibbonOptionsPage_GetBar();
	if( pRibbonBar == NULL )
		return;
CExtRibbonNodeTabPageCollection * pColNode = pRibbonBar->Ribbon_GetTabPageRootNode();
	if( pColNode == NULL )
		return;
INT nComboItemCount = m_comboChooseCommandsFrom.GetCount();
INT nCategoryCurSel = m_comboChooseCommandsFrom.GetCurSel();
	if( nCategoryCurSel < 0 )
		return;
	ASSERT_VALID( pColNode );

//	// insert separator item
//	m_listBoxChooseCommandsFrom.AddCommand( NULL );

INT nPageNodeCount = INT( pColNode->GetNodeCount() );
	ASSERT( nCategoryCurSel <= ( nPageNodeCount + 1 ) );
	// initialize categories combo box
	if( nCategoryCurSel == nPageNodeCount && nComboItemCount == ( nPageNodeCount + 2 ) )
	{ // the "file button" case
		CExtRibbonNode * pFileNode = pRibbonBar->Ribbon_GetFileRootNode();
		ASSERT_VALID( pFileNode );
		pFileNode->Ribbon_InitCommandsListBox( m_listBoxChooseCommandsFrom );
	} // the "file button" case
	else
	if(		( nCategoryCurSel == nPageNodeCount && nComboItemCount == ( nPageNodeCount + 1 ) )
		||	( nCategoryCurSel == ( nPageNodeCount + 1 ) && nComboItemCount == ( nPageNodeCount + 2 ) )
		)
	{ // the "all commands" case
		CExtRibbonNode * pFileNode = pRibbonBar->Ribbon_GetFileRootNode();
		if( pFileNode != NULL )
		{
			ASSERT_VALID( pFileNode );
			pFileNode->Ribbon_InitCommandsListBox( m_listBoxChooseCommandsFrom );
		}
		INT nPageNodeIndex;
		for( nPageNodeIndex = 0; nPageNodeIndex < nPageNodeCount; nPageNodeIndex ++ )
		{
			CExtCustomizeCmdTreeNode * pNode =
				pColNode->ElementAt( nPageNodeIndex );
			ASSERT_VALID( pNode );
			pNode->Ribbon_InitCommandsListBox( m_listBoxChooseCommandsFrom );
		} // for( nPageNodeIndex = 0; nPageNodeIndex < nPageNodeCount; nPageNodeIndex ++ )
	} // the "all commands" case
	else
	{
		CExtCustomizeCmdTreeNode * pNode =
			pColNode->ElementAt( nCategoryCurSel );
		ASSERT_VALID( pNode );
		pNode->Ribbon_InitCommandsListBox( m_listBoxChooseCommandsFrom );
	} // else from if( nCategoryCurSel == nPageNodeCount )
	if( m_listBoxChooseCommandsFrom.GetCount() > 0 )
		m_listBoxChooseCommandsFrom.SetCurSel( 0 );

	_UpdateButtons();
}

void CExtRibbonOptionsPageCustomizeQATB::OnDstComboSelEndOK()
{
	ASSERT_VALID( this );
	_UpdateButtons();
}

void CExtRibbonOptionsPageCustomizeQATB::OnSrcListBoxSelEndOK()
{
	ASSERT_VALID( this );
	_UpdateButtons();
}

void CExtRibbonOptionsPageCustomizeQATB::OnDstListBoxSelEndOK()
{
	ASSERT_VALID( this );
	_UpdateButtons();
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonOptionsTabsWnd window

IMPLEMENT_DYNCREATE( CExtRibbonOptionsTabsWnd, CExtRibbonGalleryWnd );

CExtRibbonOptionsTabsWnd::CExtRibbonOptionsTabsWnd()
{
// 	CExtRibbonGalleryWnd::m_wndScrollBarV.m_bCompleteRepaint = false;
// 	CExtRibbonGalleryWnd::m_wndScrollBarV.m_bSmoothPainting = false;
// 	CExtToolBoxWnd::m_wndScrollBarV.m_bCompleteRepaint = false;
// 	CExtToolBoxWnd::m_wndScrollBarV.m_bSmoothPainting = false;
}

CExtRibbonOptionsTabsWnd::~CExtRibbonOptionsTabsWnd()
{
}

BEGIN_MESSAGE_MAP( CExtRibbonOptionsTabsWnd, CExtRibbonGalleryWnd )
    //{{AFX_MSG_MAP( CExtRibbonOptionsTabsWnd )
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CExtRibbonOptionsTabsWnd::PreSubclassWindow()
{
	ASSERT_VALID( this );
	CExtRibbonGalleryWnd::PreSubclassWindow();
	ResetContent();
	ModifyStyle(
		0,
		WS_BORDER,
		SWP_FRAMECHANGED
		);
	CExtToolBoxWnd::ModifyToolBoxWndStyle(
		__TBWS_MULTIPLE_EXPANDED_GROUPS
			| __TBWS_ALLOW_VERTICAL_SCROLLBAR
			| __TBWS_PM_BUTTONS
			| __TBWS_RIBBON_SELECTION_MODEL
			| __TBWS_TOOLTIPS_ALL
			| __TBWS_ZERO_HEIGHT_CAPTIONS,
		0
		);
}

LRESULT CExtRibbonOptionsTabsWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch( message )
	{
	case WM_DESTROY:
		ResetContent();
	break;
	} // switch( message )
LRESULT lResult = CExtRibbonGalleryWnd::WindowProc( message,  wParam,  lParam );
	return lResult;
}

bool CExtRibbonOptionsTabsWnd::AddPage(
	CExtRibbonOptionsPage * pPage,
	bool bSelect // = false
	)
{
	ASSERT_VALID( this );
CExtCmdIcon _icon;
	return AddPage( pPage, _icon, bSelect );
}

bool CExtRibbonOptionsTabsWnd::AddPage(
	CExtRibbonOptionsPage * pPage,
	CExtCmdIcon & _icon,
	bool bSelect // = false
	)
{
	ASSERT_VALID( this );
	if( pPage == NULL )
		return false;
	ASSERT_VALID( pPage );
	if( GetIndexOfPage( pPage ) >= 0 )
		return false;
CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_GalleryGroup =
		ItemGetRoot()->ItemGetNext( __TBCGN_CHILD, 0 );
	if( pTBCI_GalleryGroup == NULL )
	{
		pTBCI_GalleryGroup =
			ItemInsert( NULL, _T("Gallery group"), (HICON) NULL );
		if( pTBCI_GalleryGroup == NULL )
		{
			ASSERT( FALSE );
			return false;
		} // if( pTBCI_GalleryGroup == NULL )
		ItemExpand( pTBCI_GalleryGroup );
		ItemSetActive( pTBCI_GalleryGroup );
	} // if( pTBCI_GalleryGroup == NULL )
LPCTSTR strText = LPCTSTR( pPage->m_strPageTitleShort );
CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_GalleryItem =
		ItemInsert( pTBCI_GalleryGroup, strText, &_icon );
	if( pTBCI_GalleryItem == NULL )
	{
		ASSERT( FALSE );
		return false;
	} // if( pTBCI_GalleryItem == NULL )
	pTBCI_GalleryItem->LPARAM_Set(
		LPARAM( pPage )
		);
	if( bSelect )
	{
		pTBCI_GalleryItem->ModifyItemStyle( __TBWI_SELECTED );
	}
	return true;
}

INT CExtRibbonOptionsTabsWnd::GetIndexOfPage(
	const CExtRibbonOptionsPage * pPage
	) const
{
	ASSERT_VALID( this );
	if( pPage == NULL )
		return -1;
	ASSERT_VALID( pPage );
const CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_GalleryGroup =
		ItemGetRoot()->ItemGetNext( __TBCGN_CHILD, 0 );
	if( pTBCI_GalleryGroup == NULL )
		return -1;
INT nIndex = 0;
const CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_GalleryItem =
		pTBCI_GalleryGroup->ItemGetNext( __TBCGN_CHILD, 0 );
	for(	;
			pTBCI_GalleryItem != NULL;
			pTBCI_GalleryItem =
					pTBCI_GalleryItem->ItemGetNext(
						__TBCGN_SIBLING, 1 ),
				nIndex ++
			)
	{
		if( pTBCI_GalleryItem->LPARAM_Get() == LPARAM( pPage ) )
			return nIndex;
	}
	return -1;
}

bool CExtRibbonOptionsTabsWnd::SelectPage(
	CExtRibbonOptionsPage * pPage
	)
{
	ASSERT_VALID( this );
	if( pPage == NULL )
		return false;
	ASSERT_VALID( pPage );
INT nIndex = GetIndexOfPage( pPage );
	if( nIndex < 0 )
		return false;
CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_GalleryGroup =
		ItemGetRoot()->ItemGetNext( __TBCGN_CHILD, 0 );
	if( pTBCI_GalleryGroup == NULL )
		return false;
CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_GalleryItem =
		pTBCI_GalleryGroup->ItemGetNext( __TBCGN_CHILD, nIndex );
	if( pTBCI_GalleryItem == NULL )
		return false;
	ASSERT( pTBCI_GalleryItem->LPARAM_Get() == LPARAM(pPage) );
	pTBCI_GalleryItem->ModifyItemStyle( __TBWI_SELECTED );
	OnToolBoxWndItemSelChange( NULL, pTBCI_GalleryItem );
	return true;
}

void CExtRibbonOptionsTabsWnd::ResetContent()
{
	ASSERT_VALID( this );
	ItemRemoveAll();
}

bool CExtRibbonOptionsTabsWnd::OnSwCanAutoHideScrollBar( bool bHorz ) const
{
	ASSERT_VALID( this );
	bHorz;
	return true;
}

void CExtRibbonOptionsTabsWnd::OnToolBoxWndItemSelChange(
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_Old,
	CExtToolBoxWnd::TOOLBOX_ITEM_DATA * pTBCI_New
	)
{
	ASSERT_VALID( this );
	CExtToolBoxWnd::OnToolBoxWndItemSelChange( pTBCI_Old, pTBCI_New );
	if( GetSafeHwnd() == NULL || pTBCI_New == NULL )
		return;
CWnd * pWndParent = GetParent();
	if( pWndParent == NULL )
		return;
CExtRibbonOptionsPage * pPage = (CExtRibbonOptionsPage *)(pTBCI_New->LPARAM_Get());
	ASSERT_VALID( pPage );
	ASSERT_KINDOF( CExtRibbonOptionsPage, pPage );
CExtRibbonOptionsDialog * pDialogRibbonOptions =
		DYNAMIC_DOWNCAST( CExtRibbonOptionsDialog, pWndParent );
	if( pDialogRibbonOptions != NULL )
		pDialogRibbonOptions->OnPageSelected( pPage );
}

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonOptionsDialog

IMPLEMENT_DYNCREATE( CExtRibbonOptionsDialog, CExtResizableDialog );

CExtRibbonOptionsDialog::CExtRibbonOptionsDialog(
	UINT nSelectedPageID, // = __EXT_RIBBON_OPTIONS_DIALOG_PAGE_DEFAULT
	CExtRibbonBar * pRibbonBar, // = NULL
	CWnd * pWndParent // = NULL
	)
	: CExtNCW < CExtResizableDialog > ( IDD_EXT_RIBBON_OPTIONS_DIALOG, pWndParent )
	, m_nSelectedPageID( nSelectedPageID )
	, m_pRibbonBar( pRibbonBar )
	, m_strSaveRestoreKey( _T("ResizableDialogs") )
	, m_strSaveRestoreEntry( _T("RibbonOptionsDialog") )
{
	//{{AFX_DATA_INIT(CExtRibbonOptionsDialog)
	//}}AFX_DATA_INIT
#ifdef _DEBUG
	if( m_pRibbonBar != NULL )
	{
		ASSERT_VALID( m_pRibbonBar );
		ASSERT( m_pRibbonBar->GetSafeHwnd() != NULL );
	}
#endif // _DEBUG
}

CExtRibbonOptionsDialog::~CExtRibbonOptionsDialog()
{
}

void CExtRibbonOptionsDialog::DoDataExchange(CDataExchange* pDX)
{
	CExtNCW < CExtResizableDialog > :: DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExtRibbonOptionsDialog)
	DDX_Control( pDX, IDC_EXT_RIBBON_STATIC_FRAME, m_labelEmptyFrame );
	DDX_Control( pDX, IDOK, m_buttonOK );
	DDX_Control( pDX, IDCANCEL, m_buttonCancel );
	//}}AFX_DATA_MAP
	DDX_Control( pDX, IDC_EXT_RIBBON_LIST_BOX_TABS, m_wndOptionsTabs );
}

BEGIN_MESSAGE_MAP( CExtRibbonOptionsDialog, CExtResizableDialog )
	//{{AFX_MSG_MAP(CExtRibbonOptionsDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CExtRibbonOptionsDialog::OnInitDialog()
{
	m_wndOptionsTabs.ResetContent();

	if(		m_pRibbonBar->GetSafeHwnd() == NULL
		||	( ! CExtNCW < CExtResizableDialog > :: OnInitDialog() )
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	ASSERT_VALID( m_pRibbonBar );
	m_strCommandProfileName =
		g_CmdManager->ProfileNameFromWnd( m_pRibbonBar->m_hWnd );
	ASSERT( ! m_strCommandProfileName.IsEmpty() );
	g_CmdManager->ProfileWndAdd(
		LPCTSTR( m_strCommandProfileName ),
		m_hWnd
		);

CRect rcEmptyFrame, rcButton, rcClient;
	GetClientRect( &rcClient );
	m_labelEmptyFrame.GetWindowRect( &rcEmptyFrame );
	ScreenToClient( &rcEmptyFrame );
	m_buttonOK.GetWindowRect( &rcButton );
	ScreenToClient( &rcButton );
INT nOffsetY =
		  ( rcClient.bottom - rcEmptyFrame.bottom - rcButton.Height() ) / 2
		- ( rcButton.top - rcEmptyFrame.bottom )
		;
	if( nOffsetY != 0 )
	{
		rcButton.OffsetRect( 0, nOffsetY );
		m_buttonOK.MoveWindow( &rcButton );
		m_buttonCancel.GetWindowRect( &rcButton );
		ScreenToClient( &rcButton );
		rcButton.OffsetRect( 0, nOffsetY );
		m_buttonCancel.MoveWindow( &rcButton );
	}

CWnd * pWnd = CExtControlBar::stat_GetWndForPlacement( m_pRibbonBar );
HICON hIcon = NULL;
	if( pWnd->GetSafeHwnd() != NULL )
	{
		hIcon = (HICON)(__EXT_MFC_DWORD_PTR) ::SendMessage( pWnd->m_hWnd, WM_GETICON, ICON_BIG, 0L );
		if( hIcon == NULL ) 
		{
			hIcon = (HICON)(__EXT_MFC_DWORD_PTR) ::SendMessage( pWnd->m_hWnd, WM_GETICON, ICON_BIG, 0L );
			if( hIcon == NULL ) 
			{
				hIcon = (HICON)(__EXT_MFC_DWORD_PTR) ::__EXT_MFC_GetClassLong( pWnd->m_hWnd, __EXT_MFC_GCL_HICON );
				if(hIcon == NULL) 
					hIcon = ::AfxGetApp()->LoadStandardIcon( IDI_WINLOGO );
			} // if( hIcon == NULL ) 
		} // if( hIcon == NULL ) 
	} // if( pWnd->GetSafeHwnd() != NULL )
	if( hIcon != NULL )
	{
		SetIcon( hIcon, TRUE );
		SetIcon( hIcon, FALSE );
	} // if( hIcon != NULL )

	AddAnchor( IDC_EXT_RIBBON_LIST_BOX_TABS, __RDA_LT, __RDA_LB );
	AddAnchor( IDC_EXT_RIBBON_STATIC_FRAME, __RDA_LT, __RDA_RB );
	AddAnchor( IDOK, __RDA_RB );
	AddAnchor( IDCANCEL, __RDA_RB );
CRect rcWnd;
	GetWindowRect( &rcWnd );
CSize _sizeMinTrack(
		::MulDiv( rcWnd.Width(),  5, 6 ),
		::MulDiv( rcWnd.Height(), 7, 8 )
		);
	SetMinTrackSize( _sizeMinTrack );

CRect rcPage;
	m_labelEmptyFrame.GetWindowRect( &rcPage );
	ScreenToClient( &rcPage );
	m_pRibbonBar->OnRibbonOptionsDialogInitPageRTCs( *this );
HWND hWndSelPage = NULL;
HWND hWndInsertAfter = m_wndOptionsTabs.m_hWnd;
INT nPageIndex, nPageCount = INT( m_arrPageRTCs.GetSize() );
	for( nPageIndex = 0; nPageIndex < nPageCount; nPageIndex ++ )
	{
		CRuntimeClass * pRTC = m_arrPageRTCs[ nPageIndex ];
		if(		pRTC == NULL
			||	( ! pRTC->IsDerivedFrom( RUNTIME_CLASS( CExtRibbonOptionsPage ) ) )
			)
		{
			ASSERT( FALSE );
			return FALSE;
		}
		CObject * pObject = pRTC->CreateObject();
		if( pObject == NULL )
		{
			ASSERT( FALSE );
			return FALSE;
		}
		CExtRibbonOptionsPage * pPage =
			STATIC_DOWNCAST( CExtRibbonOptionsPage, pObject );
		UINT nPageID = pPage->RibbonOptionsPage_GetID();
		if( ! pPage->Create( nPageID, this ) )
		{
			ASSERT( FALSE );
			return FALSE;
		}
		pPage->ShowWindow( SW_HIDE );
		if( ! m_wndOptionsTabs.AddPage( pPage ) )
		{
			ASSERT( FALSE );
			return FALSE;
		}
		if( hWndSelPage == NULL )
			hWndSelPage = pPage->m_hWnd;
		else if( m_nSelectedPageID == nPageID )
			hWndSelPage = pPage->m_hWnd;
		::SetWindowPos(
			pPage->m_hWnd,
			hWndInsertAfter,
			rcPage.left,
			rcPage.top,
			rcPage.Width(),
			rcPage.Height(),
			SWP_NOACTIVATE|SWP_NOOWNERZORDER
			);
		AddAnchor( pPage->m_hWnd, __RDA_LT, __RDA_RB );
		hWndInsertAfter = pPage->m_hWnd;
	} // for( nPageIndex = 0; nPageIndex < nPageCount; nPageIndex ++ )

	pWnd = GetWindow( GW_CHILD );
	for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )
	{
		CExtRibbonOptionsPage * pPage =
			DYNAMIC_DOWNCAST( CExtRibbonOptionsPage, pWnd );
		if( pPage == NULL )
			continue;
		if( ! pPage->RibbonOptionsPage_InitContent() )
		{
			ASSERT( FALSE );
			return FALSE;
		} // if( ! pPage->RibbonOptionsPage_InitContent() )
	} // for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )

	if( m_nSelectedPageID == __EXT_RIBBON_OPTIONS_DIALOG_PAGE_DEFAULT )
		m_nSelectedPageID = __EXT_RIBBON_OPTIONS_DIALOG_PAGE_CQATB;
CExtRibbonOptionsPage * pSelectedPage = NULL;
	//pWnd = GetDlgItem( m_nSelectedPageID );
	if( hWndSelPage != NULL )
		pWnd = CWnd::FromHandle( hWndSelPage );
	if( pWnd != NULL )
		pSelectedPage = DYNAMIC_DOWNCAST( CExtRibbonOptionsPage, pWnd );
	if( pSelectedPage == NULL )
	{
		pWnd = GetWindow( GW_CHILD );
		for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )
		{
			pSelectedPage = DYNAMIC_DOWNCAST( CExtRibbonOptionsPage, pWnd );
			if( pSelectedPage != NULL )
				break;
		} // for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )
	} // if( pSelectedPage == NULL )
	if( pSelectedPage != NULL )
		m_wndOptionsTabs.SelectPage( pSelectedPage );
	m_wndOptionsTabs.OnSwUpdateScrollBars();

	if( hWndSelPage != NULL )
	{
		m_labelEmptyFrame.ShowWindow( SW_HIDE );
		::ShowWindow( hWndSelPage, SW_SHOW );
	}

	if( ( ! m_strSaveRestoreKey.IsEmpty() ) && ( ! m_strSaveRestoreEntry.IsEmpty() ) )
		EnableSaveRestore( LPCTSTR(m_strSaveRestoreKey), LPCTSTR(m_strSaveRestoreEntry)  );

	if( ! m_pRibbonBar->m_bAllowMinimizeRibbonOptionsDialog )
	{
		CMenu * pSysMenu = GetSystemMenu( FALSE );
		if( pSysMenu != NULL )
		{
			ASSERT_KINDOF( CMenu, pSysMenu );
			pSysMenu->EnableMenuItem( SC_MINIMIZE, MF_BYCOMMAND|MF_DISABLED );
		}
		ModifyStyle( WS_MINIMIZEBOX, 0, SWP_FRAMECHANGED );
	}

	return TRUE;
}

LRESULT CExtRibbonOptionsDialog::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch( message )
	{
	case WM_DESTROY:
		if(		(! m_strCommandProfileName.IsEmpty() )
			&&	LPCTSTR( g_CmdManager->ProfileNameFromWnd( m_hWnd ) ) != NULL
			)
			g_CmdManager->ProfileWndRemove( m_hWnd );
		break;
	} // switch( message )
LRESULT lResult = CExtNCW < CExtResizableDialog > :: WindowProc( message,  wParam,  lParam );
	return lResult;
}

void CExtRibbonOptionsDialog::OnPageSelected( CExtRibbonOptionsPage * pPage )
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL || pPage->GetSafeHwnd() == NULL )
		return;
	ASSERT_VALID( pPage );
	pPage->ShowWindow( SW_SHOW );
CWnd * pWnd = GetWindow( GW_CHILD );
	for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )
	{
		CExtRibbonOptionsPage * pOwnPage =
			DYNAMIC_DOWNCAST( CExtRibbonOptionsPage, pWnd );
		if( pOwnPage == NULL || pOwnPage == pPage )
			continue;
		pOwnPage->ShowWindow( SW_HIDE );
	} // for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )
}

void CExtRibbonOptionsDialog::OnOK()
{
	ASSERT_VALID( this );
CWnd * pWnd = GetWindow( GW_CHILD );
	for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )
	{
		CExtRibbonOptionsPage * pPage =
			DYNAMIC_DOWNCAST( CExtRibbonOptionsPage, pWnd );
		if( pPage == NULL )
			continue;
		pPage->RibbonOptionsPage_Apply();
	} // for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )
	CExtNCW < CExtResizableDialog > :: OnOK();
}

void CExtRibbonOptionsDialog::OnCancel()
{
	ASSERT_VALID( this );
CWnd * pWnd = GetWindow( GW_CHILD );
	for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )
	{
		CExtRibbonOptionsPage * pPage =
			DYNAMIC_DOWNCAST( CExtRibbonOptionsPage, pWnd );
		if( pPage == NULL )
			continue;
		pPage->RibbonOptionsPage_Cancel();
	} // for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )
	CExtNCW < CExtResizableDialog > :: OnCancel();
}


/////////////////////////////////////////////////////////////////////////////
// CExtRibbonBar

IMPLEMENT_DYNCREATE( CExtRibbonBar, CExtRibbonPage );

CExtRibbonBar::CExtRibbonBar()
	: m_pExtNcFrameImpl( NULL )
	, m_nHelperCaptionHeight( -1 )
	, m_nHelperTopBorderHeight( -1 )
	, m_pFileTBB( NULL )
	, m_rcHelperQA( 0, 0, 0, 0 )
	, m_rcHelperEmbeddedCaptionText( 0, 0, 0, 0 )
	, m_strCaptionDelimiter( _T(" - ") )
	, m_nHelperCtxIndexQA( -1 )
	, m_bHelperErasingNC( false )
	, m_bAutoHideModeEnabled( true )
	, m_bHelperKeyTipTopLevelMode( true )
	, m_nPmBridgeCounter( 0 )
	, m_nLastNcHT( UINT(-1) )
	, m_bUniqueButtonsInQATB( true )
	, m_bAllowMinimizeRibbonOptionsDialog( true )
	, m_bFileTabButtonDisplaysText( false )
#if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	, m_pBackstageViewWnd( NULL )
#endif // (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
{
}

CExtRibbonBar::~CExtRibbonBar()
{
}

BEGIN_MESSAGE_MAP( CExtRibbonBar, CExtRibbonPage )
    //{{AFX_MSG_MAP(CExtRibbonBar)
	ON_WM_NCLBUTTONDOWN()
	ON_WM_NCLBUTTONDBLCLK()
	ON_WM_NCRBUTTONDOWN()
	ON_WM_NCRBUTTONUP()
	ON_WM_NCMOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDBLCLK()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_MBUTTONDBLCLK()
	ON_WM_MOUSEWHEEL()
	ON_WM_MOUSEACTIVATE()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
	__EXT_MFC_ON_WM_NCHITTEST()
	ON_COMMAND( ID_EXT_RIBBON_QATB_PLACE_ABOVE, _OnCmdRibbonQuickAccessToolbarPlaceAbove )
	ON_COMMAND( ID_EXT_RIBBON_QATB_PLACE_BELOW, _OnCmdRibbonQuickAccessToolbarPlaceBelow )
	ON_COMMAND( ID_EXT_RIBBON_QATB_CUSOMIZE, _OnCmdRibbonQuickAccessToolbarCustomize )
	ON_COMMAND( IDD_EXT_RIBBON_OPTIONS_DIALOG, _OnCmdRibbonShowOptionsDlg )
	ON_COMMAND( ID_EXT_RIBBON_QATB_RESET, _OnCmdRibbonQuickAccessToolbarReset )
	ON_COMMAND( ID_EXT_RIBBON_QATB_ADD_TO, _OnCmdRibbonQuickAccessToolbarAddTo )
	ON_UPDATE_COMMAND_UI( ID_EXT_RIBBON_QATB_ADD_TO, _OnCmdRibbonQuickAccessToolbarAddTo_Update )
	ON_COMMAND( ID_EXT_RIBBON_QATB_REMOVE_FROM, _OnCmdRibbonQuickAccessToolbarRemoveFrom )
	ON_COMMAND( ID_EXT_RIBBON_MINIMIZE, _OnCmdRibbonMinimize )
END_MESSAGE_MAP()

#if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)

bool CExtRibbonBar::BackstageView_IsSupported() const
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return false;
	if( m_pBackstageViewWnd == NULL )
		return false;
	ASSERT_VALID( m_pBackstageViewWnd );
	return true;
}

bool CExtRibbonBar::BackstageView_IsVisible() const
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return false;
	if( m_pBackstageViewWnd == NULL )
		return false;
	ASSERT_VALID( m_pBackstageViewWnd );
	return m_pBackstageViewWnd->BsvIsVisible();
}

void CExtRibbonBar::BackstageView_Toggle( bool bShowKeyTips )
{
	ASSERT_VALID( this );
	if( ! BackstageView_IsSupported() )
		return;
bool bBsvIsVisible = BackstageView_IsVisible();
	if( bBsvIsVisible )
		BackstageView_EnsureHidden();
	else
		BackstageView_EnsureVisible( bShowKeyTips );
}

void CExtRibbonBar::BackstageView_EnsureVisible( bool bShowKeyTips )
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	if( m_pBackstageViewWnd == NULL )
		return;
	ASSERT_VALID( m_pBackstageViewWnd );
bool bBsvIsVisible = BackstageView_IsVisible();
	if( bBsvIsVisible )
		return;
	_RibbonBarKeyTipShow( false, false );
	CExtPaintManager::stat_PassPaintMessages();
	if( ! m_pBackstageViewWnd->BsvShow( true, this ) )
		return;
	if( bShowKeyTips )
		m_pBackstageViewWnd->OnRcgtdKeyTipTrackingStart();
}

void CExtRibbonBar::BackstageView_EnsureHidden()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	if( m_pBackstageViewWnd == NULL )
		return;
	ASSERT_VALID( m_pBackstageViewWnd );
bool bBsvIsVisible = m_pBackstageViewWnd->BsvIsVisible();
	if( ! bBsvIsVisible )
		return;
	m_pBackstageViewWnd->BsvShow( false, this );
}

bool CExtRibbonBar::BackstageView_PreTranslateMessage( MSG * pMsg )
{
	ASSERT_VALID( this );
	if( ! BackstageView_IsVisible() )
		return false;
	ASSERT_VALID( m_pBackstageViewWnd );
	switch( pMsg->message )
	{
	case WM_SETFOCUS:
		if( pMsg->hwnd != NULL && ::IsWindow(pMsg->hwnd) && pMsg->hwnd == m_pBackstageViewWnd->BsvGetFrameWnd()->GetSafeHwnd() )
		{ // if need to set focus to backstage view
			m_pBackstageViewWnd->SetFocus();
			return true;
		} // if need to set focus to backstage view
	break; // case WM_SETFOCUS
	} // switch( pMsg->message )
	return m_pBackstageViewWnd->PreTranslateMessage( pMsg ) ? true : false;
}

#endif // (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)

void CExtRibbonBar::OnAdvancedPopupMenuTipWndDisplay(
	CExtPopupMenuTipWnd & _ATTW,
	const RECT & rcExcludeArea,
	__EXT_MFC_SAFE_LPCTSTR strTipText,
	CObject * pHelperSrc, // = NULL
	LPARAM lParam // = 0
	) const
{
	ASSERT_VALID( this );
#if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	if( BackstageView_IsVisible() )
	{
		_ATTW.Hide();
		return;
	}
#endif // (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	CExtRibbonPage::OnAdvancedPopupMenuTipWndDisplay( _ATTW, rcExcludeArea, strTipText, pHelperSrc, lParam );
}

bool CExtRibbonBar::_OnHookButtonClick( CExtBarButton * pTBB, CPoint point, bool bDown )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
#if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	if( BackstageView_IsVisible() && ( ! pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonFile ) ) ) )
	{
		BackstageView_EnsureHidden();
	}
#endif // (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	return CExtRibbonPage::_OnHookButtonClick( pTBB, point, bDown );
}

bool CExtRibbonBar::_OnHookButtonRClick( CExtBarButton * pTBB, CPoint point, bool bDown )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
#if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	if( BackstageView_IsVisible() )
		return true;
#endif // (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	return CExtRibbonPage::_OnHookButtonRClick( pTBB, point, bDown );
}

bool CExtRibbonBar::_OnHookButtonDblClick( CExtBarButton * pTBB, CPoint point )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
#if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	if( BackstageView_IsVisible() && ( ! pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonFile ) ) ) )
	{
		BackstageView_EnsureHidden();
	}
#endif // (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	return CExtRibbonPage::_OnHookButtonDblClick( pTBB, point );
}

CSize CExtRibbonBar::_CalcLayout(
	DWORD dwMode,
	int nLength // = -1
	)
{
	ASSERT_VALID( this );
CSize _size = CExtRibbonPage::_CalcLayout( dwMode, nLength );
	return _size;
}

void CExtRibbonBar::_RecalcPositionsImpl()
{
	if( GetSafeHwnd() == NULL )
		return;
	ASSERT_VALID( this );
	CExtRibbonPage::_RecalcPositionsImpl();
}

void CExtRibbonBar::PreSubclassWindow()
{
	CExtRibbonPage::PreSubclassWindow();
	m_pExtNcFrameImpl =
		CExtNcFrameImpl::NcFrameImpl_FindInstance(
			m_hWnd,
			this
			);
	if( m_pExtNcFrameImpl != NULL )
		m_pExtNcFrameImpl->m_bNcFrameImpl_PivotPmSyncMode = true; // RibbonLayout_IsFrameIntegrationEnabled();
}

void CExtRibbonBar::PostNcDestroy()
{
	m_nHelperCtxIndexQA = -1;
	m_rcHelperQA.SetRect( 0, 0, 0, 0 );
	m_rcHelperEmbeddedCaptionText.SetRect( 0, 0, 0, 0 );
	m_nHelperCaptionHeight = -1;
	m_nHelperTopBorderHeight = -1;
	m_pExtNcFrameImpl = NULL;
	m_nLastNcHT = UINT(-1);
	CExtRibbonPage::PostNcDestroy();
}

void CExtRibbonBar::DoPaintNC( CDC * pDC )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC ); 
	ASSERT( pDC->GetSafeHdc() != NULL );
	CExtRibbonPage::DoPaintNC( pDC );
}

void CExtRibbonBar::DoEraseBk( CDC * pDC )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC ); 
	ASSERT( pDC->GetSafeHdc() != NULL );

	if(		m_pExtNcFrameImpl != NULL
		&&	m_pExtNcFrameImpl->NcFrameImpl_IsNcLocked()
		)
	{
		CExtMenuControlBar::DoEraseBk( pDC );
		return;
	}

	if( ! m_bHelperErasingNC )
		CExtRibbonPage::DoEraseBk( pDC );
CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
	if( ! RibbonLayout_IsFrameIntegrationEnabled() )
	{
		if( pFileTBB != NULL )
		{
			CRect rcTBB = *pFileTBB;
			if( pDC->RectVisible( &rcTBB ) )
				pFileTBB->PaintCompound( *pDC, false, true, false );
		}
		return;
	}
	ASSERT( m_pExtNcFrameImpl != NULL );
	ASSERT( m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow()->GetSafeHwnd() != NULL );
	ASSERT( m_pExtNcFrameImpl->NcFrameImpl_IsSupported() );
bool bDwmMode = false;
	if( m_pExtNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement() )
		bDwmMode = true;
CWnd * pWndFrameImpl = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
CRect rcWndFrame, rcWndFrame2, rcClientFrame;
	pWndFrameImpl->GetWindowRect( &rcWndFrame );
	rcWndFrame2 = rcWndFrame;
	pWndFrameImpl->ScreenToClient( &rcWndFrame2 );
	pWndFrameImpl->GetClientRect( &rcClientFrame );
CSize sizeDistance(
		rcClientFrame.left - rcWndFrame2.left,
		0 // rcClientFrame.top - rcWndFrame2.top
		);

INT nTopBorderHeight = 0;
INT nFrameCaptionHeight = RibbonLayout_GetFrameCaptionHeight( &nTopBorderHeight );

CRect rcClient, rcCaption;
	GetClientRect( &rcClient );
	rcCaption = rcClient;
	rcCaption.top += nFrameCaptionHeight;
	rcCaption.bottom = rcCaption.top + nFrameCaptionHeight + nTopBorderHeight;

INT nWidthFromLeft = 0;
bool bQatbDwmMode = false, bRibbonQuickAccessBarIsAboveTheRibbon = RibbonQuickAccessBar_AboveTheRibbonGet();
	if( bRibbonQuickAccessBarIsAboveTheRibbon )
		nWidthFromLeft = m_rcHelperQA.right;
bool bZoomedDwmMode = false;
	if( bDwmMode )
	{
		pDC->OffsetViewportOrg( - sizeDistance.cx, - sizeDistance.cy );
		CExtPaintManager * pPM = PmBridge_GetPM();
		ASSERT_VALID( pPM );
		bool bDrawDwmCaptionTitle = false;
		CRect rcDwmText( 0, 0, 0, 0 );
		CString strDwmCaptionTitle;
		if(		RibbonLayout_IsFrameIntegrationEnabled()
			&&	RibbonLayout_GetFrameCaptionHeight() > 0
			)
		{
			CWnd * pWnd = GetParent();
			for( ; (pWnd->GetStyle()&WS_CHILD) != 0; pWnd = pWnd->GetParent() );
			if( pWnd != NULL )
			{
				bZoomedDwmMode = pWnd->IsZoomed() ? true : false;
				bQatbDwmMode = true;
				pWnd->GetWindowText( strDwmCaptionTitle );
				if( strDwmCaptionTitle.GetLength() > 0 )
				{
					INT nV = ::GetSystemMetrics( SM_CYFRAME );
					INT nCV = nV + ::GetSystemMetrics( SM_CYCAPTION );
					pWnd->GetClientRect( &rcDwmText );
					rcDwmText.bottom = rcDwmText.top + nCV;
					if( pWndFrameImpl->IsZoomed() )
						rcDwmText.OffsetRect( 0, nV );
					if(		(! m_rcHelperQA.IsRectEmpty() )
						&&	RibbonQuickAccessBar_AboveTheRibbonGet()
						)
						rcDwmText.left = max( rcDwmText.left, m_rcHelperQA.right );
					pPM->Ribbon_EmbeddedCaptionAdjustTextRect( rcDwmText, this );
					RECT rcCaptionButtonBounds = { 0, 0, 0, 0 };
					HRESULT hr =
						g_PaintManager.m_DWM.ExtDwmGetWindowAttribute(
							pWnd->m_hWnd,
							CExtDWM::__EXT_DWMWA_CAPTION_BUTTON_BOUNDS,
							&rcCaptionButtonBounds,
							sizeof(rcCaptionButtonBounds)
							);
					if( hr == S_OK )
						rcDwmText.right = min( rcDwmText.right, rcCaptionButtonBounds.left );
					bDrawDwmCaptionTitle = true;
				} // if( strDwmCaptionTitle.GetLength() > 0 )
			} // if( pWnd != NULL )
		} // if( RibbonLayout_IsFrameIntegrationEnabled() )
//		bool bRTL = ( (GetExStyle()&WS_EX_LAYOUTRTL) != 0 ) ? true : false;
//		DWORD dwOldLayout = LAYOUT_LTR;
//		if( bRTL )
//			dwOldLayout = pDC->SetLayout( LAYOUT_RTL );
		m_pExtNcFrameImpl->NcFrameImpl_GetPM()->Ribbon_NcOverPaint(
			*pDC,
			m_pExtNcFrameImpl->NcFrameImpl_IsActive(),
			nWidthFromLeft,
			true,
			bDrawDwmCaptionTitle ? (&rcDwmText) : NULL,
			bDrawDwmCaptionTitle ? LPCTSTR(strDwmCaptionTitle) : NULL,
			(CObject*)this
			);
//		if( bRTL )
//			pDC->SetLayout( dwOldLayout );
		pDC->OffsetViewportOrg( sizeDistance.cx, sizeDistance.cy );
	}
	else
	{
		CRect rcMemDC( rcCaption );
		rcMemDC.InflateRect( sizeDistance.cx, 0 );
		CExtMemoryDC dc( pDC, &rcMemDC );
		bool bRTL = ( (GetExStyle()&WS_EX_LAYOUTRTL) != 0 ) ? true : false;
		DWORD dwOldLayout = LAYOUT_LTR;
		if( bRTL )
			dwOldLayout = dc.SetLayout( LAYOUT_RTL );
		dc.OffsetViewportOrg( - sizeDistance.cx, nFrameCaptionHeight );
		m_pExtNcFrameImpl->NcFrameImpl_OnNcPaint( dc, true );
		m_pExtNcFrameImpl->NcFrameImpl_GetPM()->Ribbon_NcOverPaint(
			dc,
			m_pExtNcFrameImpl->NcFrameImpl_IsActive(),
			nWidthFromLeft,
			false,
			NULL,
			NULL,
			(CObject*)this
			);
		if( bRTL )
			dc.SetLayout( dwOldLayout );
		dc.OffsetViewportOrg( sizeDistance.cx, - nFrameCaptionHeight );
	} // else from if( bDwmMode )
// pDC->MoveTo( rcCaption.left, rcCaption.top );
// pDC->LineTo( rcCaption.right, rcCaption.bottom );
// pDC->MoveTo( rcCaption.left, rcCaption.bottom );
// pDC->LineTo( rcCaption.right, rcCaption.top );
// 	pDC->MoveTo( rcCaption.left, rcCaption.bottom );
// 	pDC->LineTo( rcCaption.right, rcCaption.bottom );
// return;

    pFileTBB = Ribbon_FileButtonGet(); // renew it
	if( pFileTBB != NULL )
	{
		CRect rcTBB = *pFileTBB;
		if( pDC->RectVisible( &rcTBB ) )
			pFileTBB->PaintCompound( *pDC, false, true, false );
	}

bool bDrawQatbDefault = true;
INT nIndex, nCount;
	nCount = RibbonQuickAccessButton_GetCount();
	if( bQatbDwmMode )
	{
		INT nDrawOverDwmCount = 0;
		CRect rcDwmQATB(0,0,0,0);
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtBarButton * pTBB = RibbonQuickAccessButton_GetAt( nIndex );
			ASSERT_VALID( pTBB );
			if( ! pTBB->IsVisible() )
				continue;
			if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				continue;
			CRect rcTBB = *pTBB;
			if( pDC->RectVisible( &rcTBB ) )
			{
				if( nDrawOverDwmCount == 0 )
					rcDwmQATB = rcTBB;
				else
				{
					rcDwmQATB.left   = min( rcDwmQATB.left,   rcTBB.left   );
					rcDwmQATB.top    = min( rcDwmQATB.top,    rcTBB.top    );
					rcDwmQATB.right  = max( rcDwmQATB.right,  rcTBB.right  );
					rcDwmQATB.bottom = max( rcDwmQATB.bottom, rcTBB.bottom );
				}
				nDrawOverDwmCount++;
			}
		}
		if( nDrawOverDwmCount > 0 )
		{
			static POINT g_ptZoomedDwmShift = { 0, 0 };
			CRect rcPaintQATB( rcDwmQATB );
			INT nCurrH = rcPaintQATB.Height();
			INT nMinH =
				  ::GetSystemMetrics( SM_CYCAPTION )
				+ ::GetSystemMetrics( SM_CYFRAME )
				+ g_ptZoomedDwmShift.y + 2
				;
			if( nCurrH < nMinH )
				rcPaintQATB.bottom = rcPaintQATB.top + nMinH;
			CExtMemoryDC dcMM( pDC, &rcPaintQATB,
				  CExtMemoryDC::MDCOPT_TO_MEMORY
				| CExtMemoryDC::MDCOPT_FORCE_DIB
				| CExtMemoryDC::MDCOPT_FILL_BITS
				| CExtMemoryDC::MDCOPT_RTL_COMPATIBILITY
				);
// 			CDC & dcMM = *pDC;
			CExtBitmap _bmp;
			if( _bmp.FromSurface( dcMM.m_hDC, rcPaintQATB ) )
			{
				HBITMAP hDIB = _bmp.CreateBitmap( false );
				if( hDIB != NULL )
				{
					CBitmap bmpX;
					bmpX.Attach( hDIB );
					CDC dcDwmQATB;
					if( dcDwmQATB.CreateCompatibleDC( pDC ) )
					{
						CBitmap * pOldBmpX = dcDwmQATB.SelectObject( &bmpX );
						dcDwmQATB.OffsetViewportOrg( -rcPaintQATB.left, -rcPaintQATB.top );
						COLORREF clrTransparentValue = RGB(164,164,164);
						dcDwmQATB.FillSolidRect( &rcPaintQATB, clrTransparentValue );
						if( bZoomedDwmMode )
							dcDwmQATB.OffsetViewportOrg(
								g_ptZoomedDwmShift.x,
								g_ptZoomedDwmShift.y
								);
						for( nIndex = 0; nIndex < nCount; nIndex ++ )
						{
							CExtBarButton * pTBB = RibbonQuickAccessButton_GetAt( nIndex );
							ASSERT_VALID( pTBB );
							if( ! pTBB->IsVisible() )
								continue;
							if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
								continue;
							CRect rcTBB = *pTBB;
							if( dcDwmQATB.RectVisible( &rcTBB ) )
								pTBB->PaintCompound( dcDwmQATB, false, true, false );
						}
						if( bZoomedDwmMode )
							dcDwmQATB.OffsetViewportOrg(
								-g_ptZoomedDwmShift.x,
								-g_ptZoomedDwmShift.y
								);
						if( _bmp.FromSurface( dcDwmQATB.m_hDC, rcPaintQATB ) )
						{
							_bmp.AlphaColor( clrTransparentValue, RGB(10,10,10), BYTE(0) );
							DWORD dwOldRTF = _bmp.RunTimeFlagsGet();
							_bmp.RunTimeFlagsSet( dwOldRTF | __EXT_BMP_FLAG_NO_RTL_DETECTION );
							_bmp.AlphaBlend( pDC->m_hDC, rcPaintQATB );
							_bmp.RunTimeFlagsSet( dwOldRTF );
							bDrawQatbDefault = false;
						} // if( _bmp.FromSurface( dcDwmQATB.m_hDC, rcDwmQATB ) )
						dcDwmQATB.SelectObject( &pOldBmpX );
					} // if( dcDwmQATB.CreateCompatibleDC( pDC ) )
				} // if( hDIB != NULL )
			} // if( _bmp.FromSurface( dcMM.m_hDC, rcDwmQATB ) )
			dcMM.__Flush( FALSE );
		}
	} // if( bQatbDwmMode )
	if( bDrawQatbDefault )
	{
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtBarButton * pTBB = RibbonQuickAccessButton_GetAt( nIndex );
			ASSERT_VALID( pTBB );
			if( ! pTBB->IsVisible() )
				continue;
			if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				continue;
			CRect rcTBB = *pTBB;
			if( pDC->RectVisible( &rcTBB ) )
				pTBB->PaintCompound( *pDC, false, true, false );
		}
	} // if( bDrawQatbDefault )
}

void CExtRibbonBar::OnPaint()
{
	m_bHelperDwmPaintingMode = false;
	if(		RibbonLayout_IsFrameIntegrationEnabled()
		&&	RibbonLayout_IsDwmCaptionIntegration()
		)
	{
		CRect rcClient;
		GetClientRect( &rcClient );
		CPaintDC dcPaint( this );
		CExtMemoryDC dc(
			&dcPaint,
			&rcClient,
			CExtMemoryDC::MDCOPT_TO_MEMORY | CExtMemoryDC::MDCOPT_FORCE_DIB
			);
		dc.FillSolidRect( &rcClient, RGB(0,0,0) );
		m_bHelperDwmPaintingMode = true;
		DoPaint( &dc );
		return;
	}
	CExtRibbonPage::OnPaint();
}

void CExtRibbonBar::DoPaint( CDC * pDC )
{
	__PROF_UIS_MANAGE_STATE;
	ASSERT_VALID( this );
	ASSERT_VALID( pDC ); 
	ASSERT( pDC->GetSafeHdc() != NULL );

	if(		m_pExtNcFrameImpl != NULL
		&&	m_pExtNcFrameImpl->NcFrameImpl_IsNcLocked()
		)
	{
		CExtMenuControlBar::DoEraseBk( pDC );
		return;
	}

	CExtRibbonPage::DoPaint( pDC );

	if( ! RibbonLayout_IsFrameIntegrationEnabled() )
	{
		CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
		if( pFileTBB != NULL )
		{
			CRect rcTBB = *pFileTBB;
			if( pDC->RectVisible( &rcTBB ) )
				pFileTBB->PaintCompound( *pDC, false, true, false );
		}
	}
	else
	{
		if(		m_pExtNcFrameImpl != NULL
			&&	m_pExtNcFrameImpl->NcFrameImpl_IsNcLocked()
			)
			return;
	}
	if(		(! RibbonPage_ExpandedModeGet() )
		&&	RibbonQuickAccessBar_AboveTheRibbonGet()
		)
		PmBridge_GetPM()->Ribbon_PaintContractedMargin( *pDC, this );
}

bool CExtRibbonBar::OnDeliverCmd( CExtBarButton * pTBB )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
bool bRetVal = CExtRibbonPage::OnDeliverCmd( pTBB );
	if( pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonTabPage ) ) )
	{
		bool bResetStateInfo = true;
		INT nOldSelIdx = Ribbon_PageSelectionGet();
		INT nNewSelIdx = nOldSelIdx;
		CExtCustomizeCmdTreeNode * pNode = pTBB->GetCmdNode();
		if(		pNode != NULL
			&&	pNode->IsKindOf( RUNTIME_CLASS( CExtRibbonNodeTabPage ) )
			)
		{
			CExtCustomizeCmdTreeNode * pNodeParent = pNode->GetParentNode();
			if(		pNodeParent != NULL
				&&	pNodeParent->IsKindOf( RUNTIME_CLASS( CExtRibbonNodeTabPageCollection ) )
				)
			{
				CExtRibbonNodeTabPageCollection * pColNode = (CExtRibbonNodeTabPageCollection *)pNodeParent;
				nNewSelIdx = pColNode->GetNodeIndex( pNode );
				ASSERT( nNewSelIdx >= 0 );
				bResetStateInfo = false;
				if( nNewSelIdx != nOldSelIdx )
				{
					if( bResetStateInfo )
					{
						if( ! KeyTipsDisplayedGet() )
							_RibbonPageRslaResetStateData();
					} // if( bResetStateInfo )
					Ribbon_PageSelectionSet( nNewSelIdx );
				} // if( nNewSelIdx != nOldSelIdx )
			} // if( pNodeParent != NULL ...
		} // if( pNode != NULL ...
		if( bResetStateInfo && ( ! KeyTipsDisplayedGet() ) )
			_RibbonPageRslaResetStateData();
		bool bAutoHideModeEnabled = Ribbon_AutoHideModeEnabledGet();
		if( bAutoHideModeEnabled )
		{
			bool ExpandedMode = RibbonPage_ExpandedModeGet();
			bool bDoAutoExpand = ( ! ExpandedMode );
			if(		bDoAutoExpand
				&&	_FlatTrackingIndexGet() >= 0
				//&&	nNewSelIdx != nOldSelIdx
				)
				bDoAutoExpand = false;
			if( bDoAutoExpand )
			{
				Ribbon_AutoHideModeDoExpanding(
					STATIC_DOWNCAST( CExtRibbonButtonTabPage, pTBB ),
					false
					);
			} // if( bDoAutoExpand )
		} // if( bAutoHideModeEnabled )
		else
			RibbonPage_ExpandedModeSet( true, true );
	} // if( pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonTabPage ) ) )
	return bRetVal;
}

void CExtRibbonBar::PmBridge_OnPaintManagerChanged(
	CExtPaintManager * pGlobalPM
	)
{
	ASSERT_VALID( this );
	if( g_PaintManager.m_nInstallPaintManagerCounter != 0 )
	{
		::PostMessage( m_hWnd, g_PaintManager.m_nMsgPaintManagerChanged, 0L, 0L );
		return;
	}
	m_nPmBridgeCounter++;
	m_rcHelperQA.SetRect( 0, 0, 0, 0 );
	m_rcHelperEmbeddedCaptionText.SetRect( 0, 0, 0, 0 );
	m_nHelperCaptionHeight = -1;
	m_nHelperTopBorderHeight = -1;
CExtNcFrameImpl * pExtNcFrameImpl = m_pExtNcFrameImpl;
	m_pExtNcFrameImpl =
		CExtNcFrameImpl::NcFrameImpl_FindInstance(
			m_hWnd,
			this
			);
	if( pExtNcFrameImpl == NULL )
		pExtNcFrameImpl = m_pExtNcFrameImpl;
	if( pExtNcFrameImpl != NULL )
		pExtNcFrameImpl->m_bNcFrameImpl_PivotPmSyncMode = true; // RibbonLayout_IsFrameIntegrationEnabled();
bool bAdjustDWM = false;
	if(		GetSafeHwnd() != NULL
		&&	(	m_pExtNcFrameImpl == NULL
			||	(! RibbonLayout_IsDwmCaptionIntegration() )
			)
		)
		bAdjustDWM = true;
HWND hWndSurface = NULL;
CWnd * pWnd = NULL;
	if( m_pExtNcFrameImpl != NULL )
	{
		pWnd = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
		if(		pWnd->GetSafeHwnd() != NULL
			&&	(pWnd->GetStyle()&(WS_VISIBLE|WS_CHILD)) == WS_VISIBLE
			)
		{
			CRect rcSurface;
			pWnd->GetWindowRect( &rcSurface );
			hWndSurface =
				::CreateWindowEx(
					0,
					_T("Static"),
					_T(""),
					WS_POPUP,
					rcSurface.left,
					rcSurface.top,
					rcSurface.Width(),
					rcSurface.Height(),
					pWnd->m_hWnd,
					(HMENU)NULL,
					::AfxGetInstanceHandle(),
					NULL
					);
			if( hWndSurface != NULL )
			{
				::EnableWindow( hWndSurface, FALSE );
				::ShowWindow( hWndSurface, SW_SHOWNOACTIVATE );
			} // if( hWndSurface != NULL )
// 			_RibbonPageRslaResetStateData();
// 			m_pExtNcFrameImpl->NcFrameImpl_RecalcNcFrame();
// 			m_pExtNcFrameImpl->NcFrameImpl_SetupRgn();
// 			_RecalcPositionsImpl();
		}
	} // if( m_pExtNcFrameImpl != NULL )
	CExtRibbonPage::PmBridge_OnPaintManagerChanged( pGlobalPM );
	if( pExtNcFrameImpl != NULL )
	{
		m_pExtNcFrameImpl->NcFrameImpl_AdjustVistaDwmCompatibilityIssues();
		pExtNcFrameImpl->m_BridgeNC._AdjustThemeSettings();
	}
	if( bAdjustDWM )
	{
		HWND hWnd = NULL;
		CFrameWnd * pFrameWnd = GetParentFrame();
		if( pFrameWnd != NULL )
			hWnd = pFrameWnd->m_hWnd;
		else
		{
			hWnd = ::GetParent( m_hWnd );
			if( ( ::__EXT_MFC_GetWindowLong( hWnd, GWL_STYLE ) & WS_CHILD ) != 0 )
				hWnd = NULL;
		} // else from if( pFrameWnd != NULL )
		if( hWnd != NULL )
		{
			if( g_PaintManager.m_DWM.IsCompositionEnabled() )
			{
				CExtDWM::__EXT_DWMNCRENDERINGPOLICY ncrp = CExtDWM::__EXT_DWMNCRP_ENABLED;
				HRESULT hr = 
					g_PaintManager.m_DWM.ExtDwmSetWindowAttribute(
						hWnd,
						CExtDWM::__EXT_DWMWA_NCRENDERING_POLICY,
						&ncrp,
						sizeof(ncrp)
						);
				//ASSERT( hr == S_OK );
				BOOL bAllow = FALSE;
				hr = 
					g_PaintManager.m_DWM.ExtDwmSetWindowAttribute(
						hWnd,
						CExtDWM::__EXT_DWMWA_ALLOW_NCPAINT,
						&bAllow,
						sizeof(bAllow)
						);
				//ASSERT( hr == S_OK );
				hr;
			} // if( g_PaintManager.m_DWM.IsCompositionEnabled() )
			CExtUxTheme::__EXT_UX_MARGINS _margins = { 0, 0, 0, 0 };
			g_PaintManager.m_DWM.ExtDwmExtendFrameIntoClientArea(hWnd, &_margins);
			::SendMessage( hWnd, WM_NCPAINT, 0L, 0L );
		} // if( hWnd != NULL )
	} // if( GetSafeHwnd() != NULL && m_pExtNcFrameImpl == NULL )
	if( hWndSurface != NULL )
	{
		::DestroyWindow( hWndSurface );
		CExtPaintManager::stat_PassPaintMessages();
	}
}

INT CExtRibbonBar::RibbonLayout_GetTabIntersectionHeight() const
{
	ASSERT_VALID( this );
INT nRetVal = PmBridge_GetPM() -> Ribbon_GetTabIntersectionHeight( this );
	return nRetVal;
}

INT CExtRibbonBar::RibbonLayout_GetTabLineHeight() const
{
	ASSERT_VALID( this );
INT nRetVal = PmBridge_GetPM() -> Ribbon_GetTabLineHeight( this );
	return nRetVal;
}

HRGN CExtRibbonBar::NcFrameImplBridge_GetNcExcludeHRGN() const
{
	ASSERT_VALID( this );
	if( m_pExtNcFrameImpl == NULL )
		return NULL;
	if( ! RibbonLayout_IsFrameIntegrationEnabled() )
		return NULL;
CWnd * pWndFrameImpl = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
WINDOWPLACEMENT _wp;
	::memset( &_wp, 0, sizeof(WINDOWPLACEMENT) );
	pWndFrameImpl->GetWindowPlacement( &_wp );
	if( _wp.showCmd == SW_SHOWMAXIMIZED )
		return NULL;
HRGN hRgn = m_pExtNcFrameImpl->NcFrameImpl_GetPM()->Ribbon_GetRgnCornerExclude( (CObject*)this );
	return hRgn;
}

HRGN CExtRibbonBar::NcFrameImplBridge_GetNcResizingHRGN() const
{
	ASSERT_VALID( this );
	if( m_pExtNcFrameImpl == NULL )
		return NULL;
	if( ! RibbonLayout_IsFrameIntegrationEnabled() )
		return NULL;
HRGN hRgn =
		m_pExtNcFrameImpl->NcFrameImpl_GetPM()->Ribbon_GetRgnCornerResizing( (CObject*)this );
	return hRgn;
}

bool CExtRibbonBar::NcFrameImplBridge_GetNcScRect( UINT nSC, CRect & rc ) const
{
	ASSERT_VALID( this );
	nSC;
	if(		RibbonLayout_GetFrameCaptionHeight() > 0
		&&	PmBridge_GetPM()->Ribbon_FileButtonIsItegrationSupported()
		&&	(	(! m_rcHelperQA.IsRectEmpty() )
			&&	RibbonQuickAccessBar_AboveTheRibbonGet()
			)
		&&	m_rcHelperEmbeddedCaptionText.left > m_rcHelperEmbeddedCaptionText.right
		)
	{
		rc.SetRect( -32761, -32761, -32760, -32760 );
		return true;
	}
	return false;
}

bool CExtRibbonBar::NcFrameImpl_GetNcHtRect(
	CRect & rc,
	UINT nHT,
	bool bScreenMapping, // = true
	bool bLayoutBordersH, // = false
	bool bLayoutBordersV, // = false
	LPMINMAXINFO pMinMaxInfo, // = NULL
	LPCRECT pRectWnd // = NULL
	) const
{
	ASSERT_VALID( this );
	bScreenMapping;
	bLayoutBordersH;
	bLayoutBordersV;
	pMinMaxInfo;
	if( ! RibbonLayout_IsFrameIntegrationEnabled() )
		return false;
	ASSERT( m_pExtNcFrameImpl != NULL );
	ASSERT( m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow()->GetSafeHwnd() != NULL );
	ASSERT( m_pExtNcFrameImpl->NcFrameImpl_IsSupported() );
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
	if( ! pPM->Ribbon_FileButtonIsItegrationSupported() )
		return false;
	if(		nHT == HTSYSMENU
		&&	(! pPM->Ribbon_IsSystemMenuIconPresent() )
		)
	{
		rc.SetRect( -32761, -32761, -32760, -32760 );
		return true;
	} // if( nHT == HTSYSMENU )
	if( nHT == HTCAPTION )
	{
		INT nTopBorderHeight = 0;
		INT nFrameCaptionHeight = RibbonLayout_GetFrameCaptionHeight( &nTopBorderHeight );

		CRect rcClient, rcCaption;
		GetClientRect( &rcClient );
		rcCaption = rcClient;
		rcCaption.top += nFrameCaptionHeight;
		rcCaption.bottom = rcCaption.top + nFrameCaptionHeight + nTopBorderHeight;

		const CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
		if( pFileTBB != NULL )
		{
			CRect rcTBB = *pFileTBB;
			rcCaption.left = max( rcCaption.left, rcTBB.right );
		}
		if(		(! m_rcHelperQA.IsRectEmpty() )
			&&	RibbonQuickAccessBar_AboveTheRibbonGet()
			)
			rcCaption.left = max( rcCaption.left, m_rcHelperQA.left );

		ClientToScreen( &rcCaption );
		if( ! bScreenMapping )
		{
			CRect rcWnd( 0, 0, 0, 0 );
			if( pRectWnd != NULL )
				rcWnd = (*pRectWnd);
			else
				m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow()->GetWindowRect( &rcWnd );
			rcCaption.OffsetRect( -rcWnd.TopLeft() );
		} // if( ! bScreenMapping )
		rc = rcCaption;
		return true;
	} // if( nHT == HTCAPTION )
	if(		nHT == HTCLOSE
		||	nHT == HTMINBUTTON
		||	nHT == HTMAXBUTTON
		||	nHT == HTHELP
		)
	{
		if(		RibbonLayout_GetFrameCaptionHeight() > 0
			&&	PmBridge_GetPM()->Ribbon_FileButtonIsItegrationSupported()
			&&	(	(! m_rcHelperQA.IsRectEmpty() )
				&&	RibbonQuickAccessBar_AboveTheRibbonGet()
				)
			&&	m_rcHelperEmbeddedCaptionText.left > m_rcHelperEmbeddedCaptionText.right
			)
		{
			rc.SetRect( -32761, -32761, -32760, -32760 );
			return true;
		}
	}
	return false;
}

void CExtRibbonBar::NcFrameImplBridge_OnOverPaint(
	CDC & dc,
	CRect rcWnd,
	CRect rcClient
	) const
{
	ASSERT_VALID( this );
	rcWnd;
	rcClient;
	if(		m_pExtNcFrameImpl == NULL
		||	RibbonLayout_IsDwmCaptionIntegration()
		)
		return;
CWnd * pWndFrameImpl = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
	pWndFrameImpl->GetClientRect( &rcClient );
	pWndFrameImpl->ClientToScreen( &rcClient );
	pWndFrameImpl->GetWindowRect( &rcWnd );
CSize _sizeDistance(
		rcClient.left - rcWnd.left,
		rcClient.top - rcWnd.top
		);
	dc.OffsetViewportOrg(
		_sizeDistance.cx,
		_sizeDistance.cy
		);
	m_bHelperErasingNC = true;
	( const_cast < CExtRibbonBar * > ( this ) ) -> DoEraseBk( &dc );
	m_bHelperErasingNC = false;
	dc.OffsetViewportOrg(
		- _sizeDistance.cx,
		- _sizeDistance.cy
		);
}

bool CExtRibbonBar::NcFrameImplBridge_OnOverPaint_IsEnabled() const
{
	return RibbonLayout_IsFrameIntegrationEnabled();
}

void CExtRibbonBar::NcFrameImplBridge_OnDrawCaptionText(
	CDC & dc,
	__EXT_MFC_SAFE_LPCTSTR strCaption,
	CRect rcDrawText
	) const
{
	ASSERT_VALID( this );
	if(		strCaption == NULL
		||	_tcslen(strCaption) == 0
		||	(! dc.RectVisible(&rcDrawText) )
		)
		return;
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
	pPM->Ribbon_EmbeddedCaptionAdjustTextRect( rcDrawText, this );
bool bFrameActive = true;
	if( m_pExtNcFrameImpl != NULL )
		bFrameActive = m_pExtNcFrameImpl->NcFrameImpl_IsActive();
	pPM->Ribbon_EmbeddedCaptionPaintText( dc, bFrameActive, strCaption, rcDrawText, m_strCaptionDelimiter, this );
}

bool CExtRibbonBar::NcFrameImplBridge_OnQueryDrawIcon(
	CRect & rcAdjustLocation
	) const
{
	ASSERT_VALID( this );
	rcAdjustLocation;
	if(		RibbonLayout_GetFrameCaptionHeight() > 0
		&&	RibbonLayout_IsFrameIntegrationEnabled()
		)
	{
		CExtPaintManager * pPM = PmBridge_GetPM();
		ASSERT_VALID( pPM );
		if(		pPM->Ribbon_FileButtonIsItegrationSupported()
			&&	(! pPM->Ribbon_IsSystemMenuIconPresent() )
			)
			return false;
	}
	return true;
}

bool CExtRibbonBar::NcFrameImplBridge_OnQueryDrawCaptionText(
	CRect & rcAdjustLocation,
	UINT & nAdjustDrawTextFlags
	) const
{
	ASSERT_VALID( this );
//	rcAdjustLocation;
//	if(		RibbonLayout_GetFrameCaptionHeight() > 0
//		&&	PmBridge_GetPM()->Ribbon_FileButtonIsItegrationSupported()
// 		)
//		return false;
//	return true;
	if( ! RibbonLayout_IsFrameIntegrationEnabled() )
		return true;
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
	nAdjustDrawTextFlags =
		pPM->Ribbon_EmbeddedCaptionGetTextAlignmentFlags( this );
	if(		RibbonLayout_GetFrameCaptionHeight() > 0
		&&	pPM->Ribbon_FileButtonIsItegrationSupported()
		&&	(! m_rcHelperQA.IsRectEmpty() )
		)
	{
		if(		(! m_rcHelperQA.IsRectEmpty() )
			&&	RibbonQuickAccessBar_AboveTheRibbonGet()
			)
			rcAdjustLocation.left = max( rcAdjustLocation.left, m_rcHelperQA.right );
		m_rcHelperEmbeddedCaptionText = rcAdjustLocation;
		pPM->Ribbon_EmbeddedCaptionAdjustTextRect( m_rcHelperEmbeddedCaptionText, this );
	}
	return false;
}

void CExtRibbonBar::NcFrameImpl_PreSetWindowPlacement( const WINDOWPLACEMENT & _wp )
{
	ASSERT_VALID( this );
	_wp;
}

void CExtRibbonBar::NcFrameImpl_PostSetWindowPlacement( const WINDOWPLACEMENT & _wp )
{
	ASSERT_VALID( this );
	_wp;
}

void CExtRibbonBar::NcFrameImpl_AdjustDwmArea( CExtUxTheme::__EXT_UX_MARGINS & _margins )
{
	ASSERT_VALID( this );
	PmBridge_GetPM()->Ribbon_DwmAreaAdjust( this, _margins );
}

HWND CExtRibbonBar::NcFrameImplBridge_GetSafeHwnd() const
{
	ASSERT_VALID( this );
	return GetSafeHwnd();
}

bool CExtRibbonBar::NcFrameImplBridge_OnQueryCaptionMergeMode() const
{
	ASSERT_VALID( this );
	return RibbonLayout_IsFrameIntegrationEnabled();
}

bool CExtRibbonBar::RibbonLayout_IsFrameIntegrationEnabled() const
{
	ASSERT_VALID( this );
	if(		m_pExtNcFrameImpl != NULL
		&&	m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow()->GetSafeHwnd() != NULL
		&&	m_pExtNcFrameImpl->NcFrameImpl_IsSupported()
		&&	GetSafeHwnd() != NULL
		&&	IsVisible()
		)
		return true;
	return false;
}

INT CExtRibbonBar::RibbonLayout_GetFrameCaptionHeight(
	INT * p_nTopBorderHeight // = NULL
	) const
{
	ASSERT_VALID( this );
	if( p_nTopBorderHeight != NULL )
		(*p_nTopBorderHeight) = 0;
	if( ! RibbonLayout_IsFrameIntegrationEnabled() )
		return 0;
	ASSERT( m_pExtNcFrameImpl != NULL );
	ASSERT( m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow()->GetSafeHwnd() != NULL );
	ASSERT( m_pExtNcFrameImpl->NcFrameImpl_IsSupported() );
	if(		m_pExtNcFrameImpl->NcFrameImpl_IsDwmBased()
		//&&	(! m_pExtNcFrameImpl->NcFrameImpl_IsNcLocked() )
		)
	{
		INT nV = ::GetSystemMetrics( SM_CYFRAME );
		INT nCV = nV + ::GetSystemMetrics( SM_CYCAPTION );
		if( m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow()->IsZoomed() )
		{
			nCV += nV;
			nV = 0;
		}
		if( p_nTopBorderHeight != NULL )
			(*p_nTopBorderHeight) = nV;
		return nCV;
	}
CWnd * pSkinnedWnd = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
	if( m_nHelperCaptionHeight < 0 )
	{
		CExtPaintManager * pPM = m_pExtNcFrameImpl->NcFrameImpl_GetPM();
		ASSERT_VALID( pPM );
		m_nHelperCaptionHeight =
			pPM->NcFrame_GetCaptionHeight(
				m_pExtNcFrameImpl->NcFrameImpl_IsActive(),
				pSkinnedWnd
				);
		CRect rcNcBorders, rcThemePadding;
		pPM->NcFrame_GetMetrics(
			rcNcBorders,
			rcThemePadding,
			pSkinnedWnd
			);
		m_nHelperTopBorderHeight = rcNcBorders.top;
		//m_nHelperCaptionHeight += rcNcBorders.top;
	}
	ASSERT( m_nHelperTopBorderHeight >= 0 );
	if( p_nTopBorderHeight != NULL )
	{
		if( pSkinnedWnd->IsZoomed() )
			(*p_nTopBorderHeight) = 0;
		else
			(*p_nTopBorderHeight) = m_nHelperTopBorderHeight;
	}
	return m_nHelperCaptionHeight;
}

INT CExtRibbonBar::RibbonLayout_GetBottomLineHeight() const
{
	ASSERT_VALID( this );
	return CExtRibbonPage::RibbonLayout_GetBottomLineHeight();
}

void CExtRibbonBar::RibbonLayout_AlignPageContent(
	CDC & dc,
	CRect rcAlignContent
	)
{
	ASSERT_VALID( this );
	CExtRibbonPage::RibbonLayout_AlignPageContent( dc, rcAlignContent );
}

void CExtRibbonBar::RibbonLayout_AlignTabLineContent(
	CDC & dc,
	CRect rcAlignContent
	)
{
	ASSERT_VALID( this );
CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
	if( pFileTBB != NULL )
	{
		pFileTBB->CalculateLayout( dc, CSize(10,10), TRUE );
		CSize _size = pFileTBB->Size();
		CRect rcTBB = rcAlignContent;
		rcTBB.left += 5;
		rcTBB.right = rcTBB.left + _size.cx;
		rcTBB.bottom = rcTBB.top + _size.cy;
		rcTBB.OffsetRect(
			0,
			( rcAlignContent.Height() - rcTBB.Height() ) / 2
			);
		if( ! pFileTBB->m_bHelperCustomLayoutApplied )
		{
			CRect _rcTBB = rcTBB;
			pFileTBB->SetRect( _rcTBB );
		}
		rcAlignContent.left = rcTBB.right + 5;
	}

bool bRibbonQuickAccessBarIsAboveTheRibbon = RibbonQuickAccessBar_AboveTheRibbonGet();
	if(		(! bRibbonQuickAccessBarIsAboveTheRibbon )
		||	(! RibbonLayout_IsFrameIntegrationEnabled() )
		||	(! PmBridge_GetPM()->Ribbon_FileButtonIsItegrationSupported() )
		)
	{
		m_rcHelperQA.SetRect( 0, 0, 0, 0 );
		m_rcHelperEmbeddedCaptionText.SetRect( 0, 0, 0, 0 );
		INT nIndex, nCount, nCountItegrated = 0;
		nCount = RibbonQuickAccessButton_GetCount();
		CRect _rcBottomContent( 0, 0, 0, 0 );
		INT nQAA = -1;
		if( ! bRibbonQuickAccessBarIsAboveTheRibbon )
		{
			INT nBottomLineHeight = RibbonLayout_GetBottomLineHeight();
			CRect rcClient;
			GetClientRect( &rcClient );
			_rcBottomContent = rcClient;
			_rcBottomContent.top = _rcBottomContent.bottom - nBottomLineHeight;
			m_rcHelperQA = _rcBottomContent;
			nQAA = RibbonQuickAccessBar_GetButtonsAlignment();
			if( nQAA != 0 )
				_rcBottomContent.DeflateRect( 2, 0 );
		} // if( ! bRibbonQuickAccessBarIsAboveTheRibbon )
		INT nSummaryContentWidth = 0;
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtBarButton * pTBB =
				RibbonQuickAccessButton_GetAt(
					( nQAA == 1 ) ? ( nCount - nIndex - 1 ) :  nIndex
					);
			ASSERT_VALID( pTBB );
			ASSERT( pTBB->ParentButtonGet() == NULL );
			if( ! pTBB->IsVisible() )
				continue;
			if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				continue;
			CSize _sizeTBB = pTBB->CalculateLayout( dc, _GetDefButtonSize(), TRUE );
			CRect rcTBB( 0, 0, 0, 0 );
			if( bRibbonQuickAccessBarIsAboveTheRibbon )
			{
				rcTBB.SetRect(
					rcAlignContent.left,
					rcAlignContent.top,
					rcAlignContent.left + _sizeTBB.cx,
					rcAlignContent.top + _sizeTBB.cy
					);
				rcTBB.OffsetRect(
					0,
					( rcAlignContent.Height() - _sizeTBB.cy ) / 2
					);
				rcAlignContent.left += _sizeTBB.cx;
			} // if( bRibbonQuickAccessBarIsAboveTheRibbon )
			else
			{
				if( nQAA == 1 )
				{
					rcTBB.SetRect(
						_rcBottomContent.right - _sizeTBB.cx,
						_rcBottomContent.top,
						_rcBottomContent.right,
						_rcBottomContent.top + _sizeTBB.cy
						);
					rcTBB.OffsetRect(
						0,
						( _rcBottomContent.Height() - _sizeTBB.cy ) / 2
						);
					_rcBottomContent.right -= _sizeTBB.cx;
				} // if( nQAA == 1 )
				else
				{
					rcTBB.SetRect(
						_rcBottomContent.left,
						_rcBottomContent.top,
						_rcBottomContent.left + _sizeTBB.cx,
						_rcBottomContent.top + _sizeTBB.cy
						);
					rcTBB.OffsetRect(
						0,
						( _rcBottomContent.Height() - _sizeTBB.cy ) / 2
						);
					_rcBottomContent.left += _sizeTBB.cx;
				} // else from if( nQAA == 1 )
			} // else from if( bRibbonQuickAccessBarIsAboveTheRibbon )
			pTBB->SetRect( rcTBB );
			pTBB->OnRibbonAlignContent( dc );
			nCountItegrated ++;
			nSummaryContentWidth += _sizeTBB.cx;
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
		if( bRibbonQuickAccessBarIsAboveTheRibbon )
		{
			if( nCountItegrated > 0 )
				rcAlignContent.left += 2;
		} // if( bRibbonQuickAccessBarIsAboveTheRibbon )
		else if( nQAA == 0 && nSummaryContentWidth < m_rcHelperQA.Width() )
		{
			INT nOffset = ( m_rcHelperQA.Width() - nSummaryContentWidth ) / 2;
			if( nOffset > 0 )
			{
				for( nIndex = 0; nIndex < nCount; nIndex ++ )
				{
					CExtBarButton * pTBB =
						RibbonQuickAccessButton_GetAt( nIndex );
					ASSERT_VALID( pTBB );
					ASSERT( pTBB->ParentButtonGet() == NULL );
					if( ! pTBB->IsVisible() )
						continue;
					if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
						continue;
					CSize _sizeTBB = pTBB->CalculateLayout( dc, _GetDefButtonSize(), TRUE );
					CRect rcTBB = *pTBB;
					rcTBB.OffsetRect( nOffset, 0 );
					pTBB->SetRect( rcTBB );
				} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
			} // if( nOffset > 0 )
		} // else if( nQAA == 0 && nSummaryContentWidth < m_rcHelperQA.Width() )
	} // if( (! RibbonLayout_IsFrameIntegrationEnabled() ) ...

	CExtRibbonPage::RibbonLayout_AlignTabLineContent( dc, rcAlignContent );
}

void CExtRibbonBar::RibbonLayout_AdjustTabLineRect( CRect & rcTabLine )
{
	ASSERT_VALID( this );
	CExtRibbonPage::RibbonLayout_AdjustTabLineRect( rcTabLine );
}

CSize CExtRibbonBar::RibbonLayout_Calc(
	CDC & dc,
	CPoint ptLayoutOffset, // = CPoint( 0, 0 )
	bool bSetupRects, // = true
	INT * p_nCountLayouted // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
CSize _size =
		CExtRibbonPage::RibbonLayout_Calc(
			dc,
			ptLayoutOffset,
			bSetupRects,
			p_nCountLayouted
			);
	return _size;
}

void CExtRibbonBar::_RibbonBarKeyTipSetupForQATB()
{
	ASSERT_VALID( this );
static char * g_arrQatbKeyCodeSequences[] =
{
	 "1",  "2",  "3",  "4",  "5",  "6",  "7",  "8",  "9",
	"09", "08", "07", "06", "05", "04", "03", "02", "01",
	"0A", "0B", "0C", "0D", "0E", "0F", "0G", "0H", "0I", "0J", "0K", "0L", "0M", "0N", "0O", "0P", "0Q", "0R", "0S", "0T", "0U", "0V", "0W", "0X", "0Y", "0Z",
	"1A", "1B", "1C", "1D", "1E", "1F", "1G", "1H", "1I", "1J", "1K", "1L", "1M", "1N", "1O", "1P", "1Q", "1R", "1S", "1T", "1U", "1V", "1W", "1X", "1Y", "1Z",
	"2A", "2B", "2C", "2D", "2E", "2F", "2G", "2H", "2I", "2J", "2K", "2L", "2M", "2N", "2O", "2P", "2Q", "2R", "2S", "2T", "2U", "2V", "2W", "2X", "2Y", "2Z",
	"3A", "3B", "3C", "3D", "3E", "3F", "3G", "3H", "3I", "3J", "3K", "3L", "3M", "3N", "3O", "3P", "3Q", "3R", "3S", "3T", "3U", "3V", "3W", "3X", "3Y", "3Z",
	"4A", "4B", "4C", "4D", "4E", "4F", "4G", "4H", "4I", "4J", "4K", "4L", "4M", "4N", "4O", "4P", "4Q", "4R", "4S", "4T", "4U", "4V", "4W", "4X", "4Y", "4Z",
	"5A", "5B", "5C", "5D", "5E", "5F", "5G", "5H", "5I", "5J", "5K", "5L", "5M", "5N", "5O", "5P", "5Q", "5R", "5S", "5T", "5U", "5V", "5W", "5X", "5Y", "5Z",
	"6A", "6B", "6C", "6D", "6E", "6F", "6G", "6H", "6I", "6J", "6K", "6L", "6M", "6N", "6O", "6P", "6Q", "6R", "6S", "6T", "6U", "6V", "6W", "6X", "6Y", "6Z",
	"7A", "7B", "7C", "7D", "7E", "7F", "7G", "7H", "7I", "7J", "7K", "7L", "7M", "7N", "7O", "7P", "7Q", "7R", "7S", "7T", "7U", "7V", "7W", "7X", "7Y", "7Z",
	"8A", "8B", "8C", "8D", "8E", "8F", "8G", "8H", "8I", "8J", "8K", "8L", "8M", "8N", "8O", "8P", "8Q", "8R", "8S", "8T", "8U", "8V", "8W", "8X", "8Y", "8Z",
	"9A", "9B", "9C", "9D", "9E", "9F", "9G", "9H", "9I", "9J", "9K", "9L", "9M", "9N", "9O", "9P", "9Q", "9R", "9S", "9T", "9U", "9V", "9W", "9X", "9Y", "9Z",
	NULL,
};
INT nStringIndex = 0, nCharIndex, nButtonIndex, nButtonCount = RibbonQuickAccessButton_GetCount();
char * pKeyTipString = g_arrQatbKeyCodeSequences[ nStringIndex ];
	ASSERT( pKeyTipString != NULL );
	for( nButtonIndex = 0; nButtonIndex < nButtonCount; nButtonIndex++ )
	{
		CExtBarButton * pTBB = RibbonQuickAccessButton_GetAt( nButtonIndex );
		ASSERT_VALID( pTBB );
		if(		( ! pTBB->IsVisible() )
			||	( pTBB->GetStyle() & TBBS_HIDDEN ) != 0
			)
			continue;
		CExtCustomizeCmdKeyTip * pCmdKeyTip = pTBB->OnKeyTipGetInfo();
		if( pCmdKeyTip == NULL )
		{
			CExtCustomizeCmdTreeNode * pNode = pTBB->GetCmdNode();
			if( pNode == NULL )
				continue;
			ASSERT_VALID( pNode );
			pCmdKeyTip = new CExtCustomizeCmdKeyTip;
			pNode->CmdKeyTipSet( pCmdKeyTip, false );
		} // if( pCmdKeyTip == NULL )
		else
			pCmdKeyTip->KeyCodeRemoveAll();
		if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonQuickAccessContentExpand) ) )
			continue;
		if( pKeyTipString != NULL )
		{
			for( nCharIndex = 0; pKeyTipString[nCharIndex] != '\0'; nCharIndex++ )
				pCmdKeyTip->KeyCodeAdd( DWORD(pKeyTipString[nCharIndex]) );
			nStringIndex++;
			pKeyTipString = g_arrQatbKeyCodeSequences[ nStringIndex ];
		} // if( pKeyTipString != NULL )
	} // for( nButtonIndex = 0; nButtonIndex < nButtonCount; nButtonIndex++ )
}

void CExtRibbonBar::OnFlatTrackingStart(
	HDWP & hPassiveModeDWP
	)
{
	ASSERT_VALID( this );
	hPassiveModeDWP;
MSG _msg;
	if(		::PeekMessage( &_msg, NULL, WM_ACTIVATEAPP, WM_ACTIVATEAPP, PM_NOREMOVE )
		&&	_msg.wParam == 0
		)
		return;
	if( ! CExtPopupMenuWnd::TestHoverEnabledFromActiveHWND( m_hWnd ) )
		return;
	_RibbonBarKeyTipSetupForQATB();
	if( m_bHelperKeyTipTopLevelMode )
		_RibbonBarKeyTipShow( true, false );
	else
		_RibbonBarKeyTipShow( false, true );
}

void CExtRibbonBar::OnFlatTrackingStop()
{
	ASSERT_VALID( this );
	_RibbonBarKeyTipShow( false, false );
	m_bHelperKeyTipTopLevelMode = true;
}

void CExtRibbonBar::_RibbonBarKeyTipShow(
	bool bShowKeyTipsAtTopLevel,
	bool bShowKeyTipsAtPageLevel
	)
{
	ASSERT_VALID( this );
CExtBarButton * pTBB;
INT nIndex, nCount;
HDWP hPassiveModeDWP = NULL;
	if( bShowKeyTipsAtTopLevel || bShowKeyTipsAtPageLevel )
	{
		KeyTipsDisplayedSet( true );
		KeyTipChainEmpty();
		CExtPaintManager::stat_PassPaintMessages();
//		if(		g_PaintManager.m_bIsWinVistaOrLater
//			&&	g_PaintManager.m_DWM.IsCompositionEnabled()
//			)
			hPassiveModeDWP = ::BeginDeferWindowPos( 64 );
	}

	pTBB = Ribbon_FileButtonGet();
	if( pTBB != NULL )
	{
		ASSERT_VALID( pTBB );
		pTBB->OnKeyTipTrackingQuery( bShowKeyTipsAtTopLevel, &(KeyTipChainGet()), hPassiveModeDWP );
	}

	nCount = RibbonQuickAccessButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		pTBB = RibbonQuickAccessButton_GetAt( nIndex );
		ASSERT_VALID( pTBB );
		pTBB->OnKeyTipTrackingQuery( bShowKeyTipsAtTopLevel, &(KeyTipChainGet()), hPassiveModeDWP );
	}

	nCount = RibbonRightButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		pTBB = RibbonRightButton_GetAt( nIndex );
		ASSERT_VALID( pTBB );
		pTBB->OnKeyTipTrackingQuery( bShowKeyTipsAtTopLevel, &(KeyTipChainGet()), hPassiveModeDWP );
	}

	nCount = RibbonTabPageButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		pTBB = RibbonTabPageButton_GetAt( nIndex );
		ASSERT_VALID( pTBB );
		pTBB->OnKeyTipTrackingQuery( bShowKeyTipsAtTopLevel, &(KeyTipChainGet()), hPassiveModeDWP );
	}

	nCount = RibbonGroupButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		pTBB = RibbonGroupButton_GetAt( nIndex );
		ASSERT_VALID( pTBB );
		pTBB->OnKeyTipTrackingQueryNested( bShowKeyTipsAtPageLevel, &(KeyTipChainGet()), true, hPassiveModeDWP );
	}

	if( ! ( bShowKeyTipsAtTopLevel || bShowKeyTipsAtPageLevel ) )
	{
		KeyTipsDisplayedSet( false );
		KeyTipChainEmpty();
	}
	if( hPassiveModeDWP != NULL )
	{
		::EndDeferWindowPos( hPassiveModeDWP );
		CExtPaintManager::stat_PassPaintMessages();
//		nCount = GetButtonsCount();
//		for( nIndex = 0; nIndex < nCount; nIndex++ )
//		{
//			pTBB = GetButton( nIndex );
//			ASSERT_VALID( pTBB );
//			CExtPopupKeyTipWnd * pWndKeyTip = pTBB->OnKeyTipGetWnd();
//			if(		pWndKeyTip->GetSafeHwnd() != NULL
//				&&	(pWndKeyTip->GetStyle()&WS_VISIBLE) != 0
//				)
//				pWndKeyTip->Invalidate( FALSE );
//		}
	}
	if( bShowKeyTipsAtTopLevel || bShowKeyTipsAtPageLevel )
		CExtPopupMenuTipWnd::UpdateDelayedLayeredBehaviorAll();
}

BOOL CExtRibbonBar::SetButtons(
	CExtCustomizeCmdTreeNode * pNode
	)
{
	ASSERT_VALID( this );
	m_bRibbonSimplifiedLayoutDelayedFlush = true;
	if( m_pPopupPageMenuGroup->GetSafeHwnd() != NULL )
		m_pPopupPageMenuGroup->DestroyWindow();
	if( m_pPopupPageMenuAutoHide->GetSafeHwnd() != NULL )
		m_pPopupPageMenuAutoHide->DestroyWindow();
	if( m_pExtNcFrameImpl == NULL )
		m_pExtNcFrameImpl =
			CExtNcFrameImpl::NcFrameImpl_FindInstance(
				m_hWnd,
				this
				);
	if( m_pExtNcFrameImpl != NULL )
		m_pExtNcFrameImpl->m_bNcFrameImpl_PivotPmSyncMode = true; // RibbonLayout_IsFrameIntegrationEnabled();
	_RibbonPageRslaResetStateData();
	m_arrGroupButtons.RemoveAll();
	m_arrTabPageButtons.RemoveAll();
	m_pFileTBB = NULL;
	MenuInfoRemove();
	CategoryRemoveAll();
	if( pNode == NULL )
	{
		CExtMenuControlBar::SetButtons();
		m_pRibbonNode = NULL;
		return TRUE;
	}
	if( m_pRibbonNode == pNode )
		return TRUE;
	CExtMenuControlBar::SetButtons();
	m_pRibbonNode = NULL;
	ASSERT_VALID( pNode );
	m_pRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNode );
	if( m_pRibbonNode == NULL )
		return FALSE;
	m_pRibbonNode->Ribbon_InitCommandProfile( m_hWnd, true );
	m_pRibbonNode->Ribbon_InitBar( this, NULL, false );
	MenuInfoAddEmpty( this, _T("Default"), true );
CExtRibbonNodeTabPageCollection * pColNode = Ribbon_GetTabPageRootNode();
	if( pColNode != NULL )
		OnRibbonBuildCommandCategories( pColNode );
int nCatIdx, nCatCnt = CategoryGetCount();
	for( nCatIdx = 0; nCatIdx < nCatCnt; nCatIdx ++ )
	{
		CExtCustomizeCmdTreeNode * pCatNode = CategoryGetTreeAt( nCatIdx );
		ASSERT_VALID( pCatNode );
		CExtSafeString str = pCatNode->GetTextUser();
		CategoryMakeCmdsUnique( LPCTSTR(str) );
	}
	CategoryAppendAllCommands();
	CategoryMakeAllCmdsUnique();
	OnUpdateAccelGlobalInfo( true );
	Ribbon_FileButtonInit();
	_RibbonPageRslaResetStateData();
	Ribbon_OnCalcMinMaxILE();
	if( m_pExtNcFrameImpl != NULL )
		m_pExtNcFrameImpl->NcFrameImpl_AdjustVistaDwmCompatibilityIssues();
	_RecalcPositionsImpl();
	return TRUE;
}

BOOL CExtRibbonBar::_TrackFrameSystemMenu(
	CWnd * pFrame,
	CPoint * pPoint, // = NULL, // NULL means calc meny track area automatically
	BOOL bSelectAny, // = FALSE
	LPCRECT rcExcludeArea, // = NULL
	UINT nTrackFlags, // = (UINT)(-1)
	BOOL bCombinedMode // = FALSE
	)
{
	ASSERT_VALID( this );
	nTrackFlags |= TPMX_FORCE_NO_ANIMATION|TPMX_NO_FADE_OUT_ANIMATION;
	if(		pFrame->GetSafeHwnd() != NULL
		&&	( pFrame->GetStyle() & WS_CHILD ) == 0
		&&	RibbonLayout_IsFrameIntegrationEnabled()
		)
	{
		INT nCaptionHeight = RibbonLayout_GetFrameCaptionHeight();
		CRect _rcExcludeArea;
		GetClientRect( &_rcExcludeArea );
		_rcExcludeArea.right = _rcExcludeArea.left + nCaptionHeight; // trick
		_rcExcludeArea.bottom = _rcExcludeArea.top + nCaptionHeight;
		if(		RibbonLayout_IsFrameIntegrationEnabled()
			&&	( ! RibbonLayout_IsDwmCaptionIntegration() )
			)
			_rcExcludeArea.OffsetRect( 0, nCaptionHeight );
		ClientToScreen( &_rcExcludeArea );
		if(		pPoint == NULL
			||	_rcExcludeArea.PtInRect( *pPoint )
			)
			return
				CExtRibbonPage::_TrackFrameSystemMenu(
					pFrame,
					pPoint,
					bSelectAny,
					&_rcExcludeArea,
					nTrackFlags,
					bCombinedMode
					);
	}
	return
		CExtRibbonPage::_TrackFrameSystemMenu(
			pFrame,
			pPoint,
			bSelectAny,
			rcExcludeArea,
			nTrackFlags,
			bCombinedMode
			);
}

bool CExtRibbonBar::_TranslateMouseMsg(
	UINT message,
	UINT nFlags,
	CPoint point
	)
{
	ASSERT_VALID( this );
	nFlags;
	if( ! RibbonLayout_IsFrameIntegrationEnabled() )
		return false;
//	if( HitTest( point ) >= 0 )
//		return false;
	if( m_pExtNcFrameImpl == NULL )
		return false;
	if( m_pExtNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement() )
		return false;
	if(		RibbonLayout_GetFrameCaptionHeight() > 0
		&&	PmBridge_GetPM()->Ribbon_FileButtonIsItegrationSupported()
		&&	(! m_rcHelperQA.IsRectEmpty() )
		&&	m_rcHelperQA.PtInRect( point )
		)
		return false;
	ASSERT( m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow()->GetSafeHwnd() != NULL );
	ASSERT( m_pExtNcFrameImpl->NcFrameImpl_IsSupported() );
CRect rcCaption;
	GetClientRect( &rcCaption );
INT nTopBorderHeight = 0;
INT nFrameCaptionHeight = RibbonLayout_GetFrameCaptionHeight( &nTopBorderHeight );
	rcCaption.bottom = rcCaption.top + nFrameCaptionHeight + nTopBorderHeight;
	rcCaption.OffsetRect(
		0,
		nFrameCaptionHeight + nTopBorderHeight
		);
	if( ! rcCaption.PtInRect( point ) )
		return false;
bool bCheckCursor = false;
	switch( message )
	{
	case WM_MOUSEMOVE:
		message = WM_NCMOUSEMOVE;
		bCheckCursor = true;
		break;
	case WM_LBUTTONDOWN:
		message = WM_NCLBUTTONDOWN;
		break;
	case WM_LBUTTONUP:
		message = WM_NCLBUTTONUP;
		bCheckCursor = true;
		break;
	case WM_LBUTTONDBLCLK:
		message = WM_NCLBUTTONDBLCLK;
		break;
	case WM_RBUTTONDOWN:
		message = WM_NCRBUTTONDOWN;
		break;
	case WM_RBUTTONUP:
		message = WM_NCRBUTTONUP;
		break;
	case WM_RBUTTONDBLCLK:
		message = WM_NCRBUTTONDBLCLK;
		break;
	case WM_MBUTTONDOWN:
		message = WM_NCMBUTTONDOWN;
		break;
	case WM_MBUTTONUP:
		message = WM_NCMBUTTONUP;
		break;
	case WM_MBUTTONDBLCLK:
		message = WM_NCMBUTTONDBLCLK;
		break;
	default:
		return false;
	} // switch( message )
CPoint ptScreen = point;
	ClientToScreen( &ptScreen );
CWnd * pWndFrameImpl = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
LPARAM lParam = MAKELPARAM( ptScreen.x, ptScreen.y );
UINT nHT = (UINT)
	pWndFrameImpl->
		SendMessage(
			WM_NCHITTEST,
			0,
			lParam
			);
	pWndFrameImpl->
		SendMessage(
			message,
			WPARAM(nHT),
			lParam
			);
	if( bCheckCursor )
		m_pExtNcFrameImpl->NcFrameImpl_CheckCursor( ptScreen, HTCAPTION, false );
	return false;
}

bool CExtRibbonBar::OnQueryBarHoverProcessingEnabled() const
{
	ASSERT_VALID( this );
	if( RibbonLayout_IsFrameIntegrationEnabled() )
		return true;
	return CExtRibbonPage::OnQueryBarHoverProcessingEnabled();
}

bool CExtRibbonBar::_OnMouseMoveMsg( UINT nFlags, CPoint point )
{
	ASSERT_VALID( this );
	if( _TranslateMouseMsg(
			WM_MOUSEMOVE,
			nFlags,
			point
			)
		)
		return true;
	if( ! CExtRibbonPage::OnQueryBarHoverProcessingEnabled() )
		return false;
	return CExtRibbonPage::_OnMouseMoveMsg( nFlags, point );
}

UINT CExtRibbonBar::OnNcHitTest(CPoint point) 
{
	ASSERT_VALID( this );

	bool bRibbonLayout_IsFrameIntegrationEnabled = RibbonLayout_IsFrameIntegrationEnabled();

#ifdef _DEBUG
	bRibbonLayout_IsFrameIntegrationEnabled = false;
#endif

	if( bRibbonLayout_IsFrameIntegrationEnabled )
	{
		if( m_pExtNcFrameImpl != NULL )
		{
			CWnd * pSkinnedWnd = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
			if( pSkinnedWnd->GetSafeHwnd() != NULL )
			{
				if( RibbonLayout_IsDwmCaptionIntegration() )
				{
					LRESULT lResult = LRESULT(HTCLIENT);
					if ( g_PaintManager.m_DWM.ExtDwmDefWindowProc(
							pSkinnedWnd->m_hWnd,
							WM_NCHITTEST,
							0,
							MAKELPARAM( point.x, point.y ),
							&lResult
							)
						)
					{
						m_nLastNcHT = (UINT)lResult;
						if(		m_nLastNcHT == HTBORDER
							||	m_nLastNcHT == HTTOP
							||	m_nLastNcHT == HTBOTTOM
							||	m_nLastNcHT == HTLEFT
							||	m_nLastNcHT == HTRIGHT
							||	m_nLastNcHT == HTTOPLEFT
							||	m_nLastNcHT == HTTOPRIGHT
							||	m_nLastNcHT == HTBOTTOMLEFT
							||	m_nLastNcHT == HTBOTTOMRIGHT
							||	m_nLastNcHT == HTMINBUTTON
							||	m_nLastNcHT == HTMAXBUTTON
							||	m_nLastNcHT == HTCLOSE
							||	m_nLastNcHT == HTHELP
							//||	m_nLastNcHT == HTCAPTION
							)
						{
							//CExtRibbonPage::OnNcHitTest( point );
							//return m_nLastNcHT;
							return UINT(HTTRANSPARENT);
						}
					}
					INT nV = ::GetSystemMetrics( SM_CYFRAME );
					INT nCV = nV + ::GetSystemMetrics( SM_CYCAPTION );
					if( pSkinnedWnd->IsZoomed() )
						nCV += nV;
					CRect rc;
					pSkinnedWnd->GetWindowRect( &rc );
					rc.bottom = rc.top + nV;
					if( rc.PtInRect( point ) )
						return UINT(HTTOP);
					rc.bottom = rc.top + nCV;
					if( rc.PtInRect( point ) )
						return UINT(HTCAPTION);
				}
				m_nLastNcHT = (UINT)
					pSkinnedWnd->SendMessage(
						WM_NCHITTEST,
						0,
						MAKELPARAM( point.x, point.y )
						);
				if(		m_nLastNcHT == HTCLIENT
					&&	(! m_pExtNcFrameImpl->m_rcNcFrameImpl_LastExternalNcHT.IsRectEmpty() )
					)
				{
					CPoint ptClient( point);
					ScreenToClient( &ptClient );
					if( ! m_pExtNcFrameImpl->m_rcNcFrameImpl_LastExternalNcHT.PtInRect( ptClient ) )
					{
						m_pExtNcFrameImpl->m_nNcFrameImpl_LastExternalNcHT = UINT(-1);
						InvalidateRect( &m_pExtNcFrameImpl->m_rcNcFrameImpl_LastExternalNcHT );
						m_pExtNcFrameImpl->m_rcNcFrameImpl_LastExternalNcHT.SetRect( -1, -1, -1, -1 );
					}
				}
				if(		m_nLastNcHT == HTBORDER
					||	m_nLastNcHT == HTTOP
					||	m_nLastNcHT == HTBOTTOM
					||	m_nLastNcHT == HTLEFT
					||	m_nLastNcHT == HTRIGHT
					||	m_nLastNcHT == HTTOPLEFT
					||	m_nLastNcHT == HTTOPRIGHT
					||	m_nLastNcHT == HTBOTTOMLEFT
					||	m_nLastNcHT == HTBOTTOMRIGHT
					)
				{
					//CExtRibbonPage::OnNcHitTest( point );
					return m_nLastNcHT;
				}
				return HTCLIENT;
			}
		}
	}
	return CExtRibbonPage::OnNcHitTest( point );
}

void CExtRibbonBar::OnNcMouseMove( UINT nHitTest, CPoint point )
{
	ASSERT_VALID( this );
	if( m_pExtNcFrameImpl == NULL )
		return;
CWnd * pSkinnedWnd = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
	if( pSkinnedWnd->GetSafeHwnd() == NULL )
		return;
LPARAM lParam = MAKELPARAM( point.x, point.y );
	pSkinnedWnd->
		SendMessage(
			WM_NCMOUSEMOVE,
			WPARAM(nHitTest),
			lParam
			);
}

void CExtRibbonBar::OnNcLButtonDown(UINT nHitTest, CPoint point)
{
	ASSERT_VALID( this );
	if( m_pExtNcFrameImpl == NULL )
		return;
CWnd * pSkinnedWnd = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
	if( pSkinnedWnd->GetSafeHwnd() == NULL )
		return;
LPARAM lParam = MAKELPARAM( point.x, point.y );
	pSkinnedWnd->
		SendMessage(
			WM_NCLBUTTONDOWN,
			WPARAM(nHitTest),
			lParam
			);
}

void CExtRibbonBar::OnNcLButtonDblClk(UINT nHitTest, CPoint point)
{
	ASSERT_VALID( this );
	if( m_pExtNcFrameImpl == NULL )
		return;
CWnd * pSkinnedWnd = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
	if( pSkinnedWnd->GetSafeHwnd() == NULL )
		return;
LPARAM lParam = MAKELPARAM( point.x, point.y );
	pSkinnedWnd->
		SendMessage(
			WM_NCLBUTTONDBLCLK,
			WPARAM(nHitTest),
			lParam
			);
}

void CExtRibbonBar::OnNcRButtonDown(UINT nHitTest, CPoint point)
{
	ASSERT_VALID( this );
	if( m_pExtNcFrameImpl == NULL )
		return;
CWnd * pSkinnedWnd = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
	if( pSkinnedWnd->GetSafeHwnd() == NULL )
		return;
//CPoint ptClient = point;
//	ScreenToClient( &ptClient );
//	if( _OnMouseMoveMsg( 0, ptClient ) )
//	{
//		CExtRibbonPage::OnNcRButtonUp( nHitTest, point );
//		return;
//	}
LPARAM lParam = MAKELPARAM( point.x, point.y );
	pSkinnedWnd->
		SendMessage(
			WM_NCRBUTTONDOWN,
			WPARAM(nHitTest),
			lParam
			);
}

void CExtRibbonBar::OnNcRButtonUp(UINT nHitTest, CPoint point)
{
	ASSERT_VALID( this );
	if( m_pExtNcFrameImpl == NULL )
		return;
CWnd * pSkinnedWnd = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
	if( pSkinnedWnd->GetSafeHwnd() == NULL )
		return;
CPoint ptClient = point;
	ScreenToClient( &ptClient );
	if( _OnMouseMoveMsg( 0, ptClient ) )
	{
		CExtRibbonPage::OnNcRButtonUp( nHitTest, point );
		return;
	}
LPARAM lParam = MAKELPARAM( point.x, point.y );
	pSkinnedWnd->
		SendMessage(
			WM_NCRBUTTONUP,
			WPARAM(nHitTest),
			lParam
			);
}

void CExtRibbonBar::OnLButtonDown(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	if( _TranslateMouseMsg(
			WM_LBUTTONDOWN,
			nFlags,
			point
			)
		)
		return;
	CExtRibbonPage::OnLButtonDown( nFlags, point );
}

void CExtRibbonBar::OnLButtonUp(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	if( _TranslateMouseMsg(
			WM_LBUTTONUP,
			nFlags,
			point
			)
		)
		return;
	CExtRibbonPage::OnLButtonUp( nFlags, point );
}

void CExtRibbonBar::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
HWND hWndOwn = GetSafeHwnd();
	if( _TranslateMouseMsg(
			WM_LBUTTONDBLCLK,
			nFlags,
			point
			)
		)
		return;
	if( hWndOwn == NULL || (! ::IsWindow(hWndOwn) ) )
		return;
	CExtRibbonPage::OnLButtonDblClk( nFlags, point );
}

void CExtRibbonBar::OnRButtonDown(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	if( _TranslateMouseMsg(
			WM_RBUTTONDOWN,
			nFlags,
			point
			)
		)
		return;
INT nHT = HitTest( point );
	if( nHT >= 0 )
	{
		CExtBarButton * pTBB = GetButton( nHT );
		ASSERT_VALID( pTBB );
		if( OnRibbonTrackButtonContextMenu( pTBB, nFlags, point ) )
			return;
	} // if( nHT >= 0 )
	if( OnRibbonTrackBarContextMenu( nFlags, point ) )
		return;
	CExtRibbonPage::OnRButtonDown( nFlags, point );
}

void CExtRibbonBar::OnRButtonUp(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	if( _TranslateMouseMsg(
			WM_RBUTTONUP,
			nFlags,
			point
			)
		)
		return;
	CExtRibbonPage::OnRButtonUp( nFlags, point );
}

void CExtRibbonBar::OnRButtonDblClk(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	if( _TranslateMouseMsg(
			WM_RBUTTONDBLCLK,
			nFlags,
			point
			)
		)
		return;
	CExtRibbonPage::OnRButtonDblClk( nFlags, point );
}

void CExtRibbonBar::OnMButtonDown(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	if( _TranslateMouseMsg(
			WM_MBUTTONDOWN,
			nFlags,
			point
			)
		)
		return;
	CExtRibbonPage::OnMButtonDown( nFlags, point );
}

void CExtRibbonBar::OnMButtonUp(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	if( _TranslateMouseMsg(
			WM_MBUTTONUP,
			nFlags,
			point
			)
		)
		return;
	CExtRibbonPage::OnMButtonUp( nFlags, point );
}

void CExtRibbonBar::OnMButtonDblClk(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
	if( _TranslateMouseMsg(
			WM_MBUTTONDBLCLK,
			nFlags,
			point
			)
		)
		return;
	CExtRibbonPage::OnMButtonDblClk( nFlags, point );
}

BOOL CExtRibbonBar::OnMouseWheel(UINT fFlags, short zDelta, CPoint point)
{
	ASSERT_VALID( this );
	if( OnRibbonProcessMouseWheel( fFlags, zDelta, point ) )
		return TRUE;
	return CExtRibbonPage::OnMouseWheel( fFlags, zDelta, point );
}

bool CExtRibbonBar::OnRibbonProcessMouseWheel( UINT fFlags, short zDelta, CPoint point )
{
	ASSERT_VALID( this );
	fFlags;
	point;
	if( GetSafeHwnd() == NULL )
		return false;
	if( zDelta == 0 )
		return false;
	if( ! RibbonPage_ExpandedModeGet() )
		return false;
	if( m_bFlatTracking )
		return false;
	if( ( GetStyle() & WS_DISABLED ) != 0 )
		return false;
CWnd * pWnd = GetParent();
	if(		pWnd != NULL
		&&	( pWnd->GetStyle() & WS_DISABLED ) != 0
		)
		return false;
INT nIndex, nCount = RibbonTabPageButton_GetCount();
	if( nCount <= 1 )
		return false;
HWND hWndFocus = ::GetFocus();
	if(		hWndFocus != NULL
		&&	(	hWndFocus == m_hWnd
			||	::IsChild( m_hWnd, hWndFocus )
			)
		)
		return false;
bool bNext = ( int(zDelta) < 0 ) ? true : false;
CExtRibbonButtonTabPage * pNewSelTBB = NULL;
bool bSelPassed = false;
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtRibbonButtonTabPage * pTBB = RibbonTabPageButton_GetAt( nIndex );
		ASSERT_VALID( pTBB );
		if( pTBB->IsSelectedRibbonPage() )
		{
			bSelPassed = true;
			continue;
		}
		if( ! pTBB->OnQueryFlatTrackingEnabled() )
			continue;
		if( bSelPassed )
		{
			if( bNext )
			{
				if( pNewSelTBB == NULL )
				{
					pNewSelTBB = pTBB;
					break;
				}
			}
			else
				break;
		} // if( bSelPassed )
		else
		{
			if( bNext )
				continue;
			pNewSelTBB = pTBB;
		} // else from if( bSelPassed )
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	if( pNewSelTBB == NULL )
		return false;
	Invalidate();
	if( m_bFlatTracking )
	{
		m_bFlatTracking = false;
		OnFlatTrackingStop();
	} // if( m_bFlatTracking )
	_UpdateFlatTracking( false );
INT nPageIndex, nPageCount = RibbonTabPageButton_GetCount();
	for( nPageIndex = 0; nPageIndex < nPageCount; nPageIndex ++ )
	{
		CExtRibbonButtonTabPage * pTabPageTBB = RibbonTabPageButton_GetAt( nPageIndex );
		ASSERT( pTabPageTBB );
		pTabPageTBB->SetHover( false );
		pTabPageTBB->ModifyStyle( 0, TBBS_PRESSED|TBBS_CHECKED );
	} // for( . . .
	_UpdateHoverButton( CPoint(-1,-1), false );
	pNewSelTBB->OnDeliverCmd();
	return true;
}

int CExtRibbonBar::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message)
{
	ASSERT_VALID( this );
	return CExtRibbonPage::OnMouseActivate( pDesktopWnd, nHitTest, message );
}

bool CExtRibbonBar::Ribbon_FileButtonInit()
{
	ASSERT_VALID( this );
	ASSERT( m_pFileTBB == NULL );
	m_pFileTBB = new CExtRibbonButtonFile( this );
	if( ! InsertSpecButton( 0, m_pFileTBB, FALSE ) )
	{
		delete m_pFileTBB;
		m_pFileTBB = NULL;
		return false;
	}
	return true;
}

CExtRibbonButtonQuickAccessContentExpand * CExtRibbonBar::Ribbon_QuickAccessContentExpandButtonGet()
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pQACEB != NULL )
	{
		ASSERT_VALID( m_pQACEB );
	}
#endif // _DEBUG
	return m_pQACEB;
}

const CExtRibbonButtonQuickAccessContentExpand * CExtRibbonBar::Ribbon_QuickAccessContentExpandButtonGet() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtRibbonBar * > ( this ) ) ->
		Ribbon_QuickAccessContentExpandButtonGet();
}

bool CExtRibbonBar::Ribbon_QuickAccessContentExpandButtonCalcLayout( CDC & dc )
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
CExtRibbonButtonQuickAccessContentExpand * pQACEB = Ribbon_QuickAccessContentExpandButtonGet();
	if( pQACEB == NULL )
		return false;
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
	if(	! pPM->Ribbon_QuickAccessContentExpandButtonCalcLayout(
			dc,
			pQACEB
			)
		)
		return false;
	return true;
}

bool CExtRibbonBar::Ribbon_QuickAccessContentExpandButtonPaint( CDC & dc )
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
CExtRibbonButtonQuickAccessContentExpand * pQACEB = Ribbon_QuickAccessContentExpandButtonGet();
	if( pQACEB == NULL )
		return false;
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( this );
	if(	! pPM->Ribbon_QuickAccessContentExpandButtonPaint(
			dc,
			pQACEB
			)
		)
		return false;
	return true;
}

bool CExtRibbonBar::Ribbon_AutoHideModeEnabledGet() const
{
	ASSERT_VALID( this );
	return m_bAutoHideModeEnabled;
}

void CExtRibbonBar::Ribbon_AutoHideModeEnabledSet( bool bAutoHideModeEnabled )
{
	ASSERT_VALID( this );
	if(		( m_bAutoHideModeEnabled && bAutoHideModeEnabled )
		||	( (!m_bAutoHideModeEnabled) && (!bAutoHideModeEnabled) )
		)
		return;
	m_bAutoHideModeEnabled = bAutoHideModeEnabled;
	_RibbonPageRslaResetStateData();
	if( GetSafeHwnd() == NULL )
		return;
}

bool CExtRibbonBar::Ribbon_AutoHideModeDoExpanding(
	CExtRibbonButtonTabPage * pTBB,
	bool bKeyboardTrackingEvent
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
	ASSERT( GetSafeHwnd() != NULL );
	bKeyboardTrackingEvent;
	if( ! Ribbon_AutoHideModeEnabledGet() )
		return false;
	if( RibbonPage_ExpandedModeGet() )
		return false;
INT nSelIdx = RibbonTabPageButton_GetIndexOf( pTBB );
	if( nSelIdx < 0 )
		return false;
	Ribbon_PageSelectionSet( nSelIdx, false );
bool bKeyTipsDisplayed = KeyTipsDisplayedGet();
INT nIndex, nCount = AnimationSite_ClientGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtAnimationClient * pAC = AnimationSite_ClientGetAt( nIndex );
		ASSERT( pAC != NULL );
		AnimationSite_ClientProgressStop( pAC );
		pAC->AnimationClient_StateGet( false ).Empty();
		pAC->AnimationClient_StateGet( true ).Empty();
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
INT nTabLineHeight = RibbonLayout_GetTabLineHeight();
INT nFrameCaptionHeight = RibbonLayout_GetFrameCaptionHeight();
INT nHeightAtTheTop = nTabLineHeight + nFrameCaptionHeight;
	if(		RibbonLayout_IsFrameIntegrationEnabled()
		&&	(! RibbonLayout_IsDwmCaptionIntegration() )
		)
	{
		INT nTopBorderHeight = 0;
		INT nFrameCaptionHeight = RibbonLayout_GetFrameCaptionHeight( &nTopBorderHeight );
		nHeightAtTheTop += nFrameCaptionHeight + nTopBorderHeight;
	}
CRect rcClient;
	GetClientRect( &rcClient );
CRect rcPageBk = rcClient;
	rcPageBk.top += nHeightAtTheTop;
	rcPageBk.bottom = rcPageBk.top
		+ PmBridge_GetPM() ->
			Ribbon_GetGroupHeight( NULL )
		;
CRect rcTabLine(
		rcPageBk.left,
		rcPageBk.top - nTabLineHeight,
		rcPageBk.right,
		rcPageBk.top
		);
HWND hWndTrack = pTBB->GetCmdTargetWnd()->GetSafeHwnd();
	if( m_pPopupPageMenuAutoHide == NULL )
		m_pPopupPageMenuAutoHide =
			new CExtRibbonPopupMenuWnd( m_hWnd );
	else if( m_pPopupPageMenuAutoHide->GetSafeHwnd() != NULL )
		::DestroyWindow( m_pPopupPageMenuAutoHide->m_hWnd ); // fade-out
	m_pPopupPageMenuAutoHide->m_wndRibbonPage.m_bHelperAutoHideMode = true;
	m_pPopupPageMenuAutoHide->m_pSrcTrackingButtonTabPage =
		STATIC_DOWNCAST( CExtRibbonButtonTabPage, pTBB );
	m_pPopupPageMenuAutoHide->m_wndRibbonPage.m_hWndSrcRibbonPage =
		m_hWnd;
	m_pPopupPageMenuAutoHide->m_wndRibbonPage.m_hWndParentRibbonPage =
		m_hWnd;

	m_pPopupPageMenuAutoHide->CreatePopupMenu( hWndTrack );

	CExtCustomizeCmdTreeNode * pOwnedNode = pTBB->GetCmdNode();
	ASSERT_VALID( pOwnedNode );
//CExtRibbonNode * pNewRootNode = new CExtRibbonNode;
//	pNewRootNode->InsertNode( NULL, pOwnedNode->CloneNode() );
	CExtCustomizeCmdTreeNode * pNewRootNode = pOwnedNode->CloneNode();
	m_pPopupPageMenuAutoHide->m_wndRibbonPage.SetButtons( pNewRootNode );

//	if( m_pPopupPageMenuAutoHide->m_wndRibbonPage.m_bFlatTracking )
//	{
//		m_pPopupPageMenuAutoHide->m_wndRibbonPage.m_bFlatTracking = false;
//		m_pPopupPageMenuAutoHide->m_wndRibbonPage.OnFlatTrackingStop();
//	} // m_pPopupPageMenuAutoHide->m_wndRibbonPage.if( m_bFlatTracking )
//	m_pPopupPageMenuAutoHide->m_wndRibbonPage.m_nFlatTrackingIndex = -1;
//	if( bSelectAny )
//	{
//		INT nIndex, nCount = m_pPopupPageMenuAutoHide->m_wndRibbonPage.GetButtonsCount();
//		for( nIndex = 0; nIndex < nCount; nIndex ++ )
//		{
//			CExtBarButton * pTBB = m_pPopupPageMenuAutoHide->m_wndRibbonPage.GetButton( nIndex );
//			ASSERT_VALID( pTBB );
//			if( pTBB->OnQueryFlatTrackingEnabled() )
//			{
//				m_pPopupPageMenuAutoHide->m_wndRibbonPage.m_nFlatTrackingIndex = nIndex;
//				if( ! m_pPopupPageMenuAutoHide->m_wndRibbonPage.m_bFlatTracking )
//				{
//					m_pPopupPageMenuAutoHide->m_wndRibbonPage.m_bFlatTracking = true;
//					m_pPopupPageMenuAutoHide->m_wndRibbonPage.OnFlatTrackingStart();
//				} // if( ! m_pPopupPageMenuAutoHide->m_wndRibbonPage.m_bFlatTracking )
//				break;
//			} // if( pTBB->OnQueryFlatTrackingEnabled() )
//		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
//	} // if( bSelectAny )
	m_pPopupPageMenuAutoHide->m_wndRibbonPage.RibbonLayout_IleReset(
		NULL,
		__EXT_RIBBON_ILE_MAX,
		true,
		false,
		true
		);
//	{ // block
//		CClientDC dc( this );
//		m_pPopupPageMenuAutoHide->m_sizeChildControl =
//			m_pPopupPageMenuAutoHide->m_wndRibbonPage.RibbonLayout_Calc(
//				dc
//				);
//		m_pPopupPageMenuAutoHide->m_sizeChildControl.cy =
//		m_pPopupPageMenuAutoHide->m_wndRibbonPage.RibbonLayout_GetGroupHeight( NULL );
//		if( ! pRibbonPage->m_bHelperAutoHideMode )
//		{
//		//	if( ! PmBridge_GetPM()->Ribbon_IsPopupGroupWithCaption( this ) )
//		//		m_pPopupPageMenuAutoHide->m_sizeChildControl.cy -=
//		//			RibbonLayout_GroupCaptionGetHeight( pTBB );
//		} // if( ! pRibbonPage->m_bHelperAutoHideMode )
//	} // block
	m_pPopupPageMenuAutoHide->m_sizeChildControl = rcPageBk.Size();
	ClientToScreen( &rcPageBk );
	ClientToScreen( &rcTabLine );

	CExtPaintManager::stat_PassPaintMessages();
	m_pPopupPageMenuAutoHide->m_hWndNotifyMenuClosed = m_hWnd;
	DWORD dwTrackFlags =
		  TPMX_COMBINE_NONE
		| TPMX_OWNERDRAW_FIXED
		| TPMX_NO_SITE
		| TPMX_NO_HIDE_RARELY
		| TPMX_FORCE_NO_ANIMATION
		| TPMX_RIBBON_MODE
		;
	if( ! m_pPopupPageMenuAutoHide->TrackPopupMenu(
			dwTrackFlags,
			rcPageBk.left,
			rcPageBk.top,
			&rcTabLine,
			this,
			CExtToolControlBar::_CbPaintCombinedContent,
			NULL,
			true
			)
		)
	{
		CExtToolControlBar::_CloseTrackingMenus();
		return false;
	}
	if( bKeyTipsDisplayed )
	{
		_CancelFlatTracking( FALSE );
		_SwitchMenuTrackingIndex();
		Invalidate();
		HDWP hPassiveModeDWP = NULL;
		m_pPopupPageMenuAutoHide->m_wndRibbonPage.OnFlatTrackingStart( hPassiveModeDWP );
		KeyTipsDisplayedSet( false );
	} // if( bKeyTipsDisplayed )
	else if( bKeyboardTrackingEvent )
	{
		INT nFlatTrackingIndex =
			m_pPopupPageMenuAutoHide->m_wndRibbonPage.
				OnCalcFlatTrackingIndex( VK_TAB, -1 );
		if( nFlatTrackingIndex >= 0 )
		{
			m_pPopupPageMenuAutoHide->m_wndRibbonPage.
				_FlatTrackingIndexSet( nFlatTrackingIndex );
 			m_pPopupPageMenuAutoHide->m_wndRibbonPage.
 				_FlatTrackingSet( true );
 			m_pPopupPageMenuAutoHide->m_wndRibbonPage.
				_UpdateFlatTracking();
		} // if( nFlatTrackingIndex >= 0 )
	} // else if( bKeyboardTrackingEvent )
	else
	{
		CExtToolControlBar::g_bMenuTracking = true;
		_SwitchMenuTrackingIndex( _GetIndexOf( pTBB ) );
	} // else from else if( bKeyboardTrackingEvent )
	return true;
}

CExtRibbonButtonFile * CExtRibbonBar::Ribbon_FileButtonGet()
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pFileTBB != NULL )
	{
		ASSERT_VALID( m_pFileTBB );
	}
#endif // _DEBUG
	return m_pFileTBB;
}

const CExtRibbonButtonFile * CExtRibbonBar::Ribbon_FileButtonGet() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtRibbonBar * > ( this ) ) ->
		Ribbon_FileButtonGet();
}

INT CExtRibbonBar::RibbonLayout_CalcQatbAboveRibbonHorzStart()
{
	ASSERT_VALID( this );
	return PmBridge_GetPM()->RibbonLayout_CalcQatbAboveRibbonHorzStart( this );
}

bool CExtRibbonBar::Ribbon_FileButtonCalcLayout( CDC & dc )
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
	if( pFileTBB == NULL )
		return false;
	if( ! RibbonLayout_IsFrameIntegrationEnabled() )
		return false;
	ASSERT( m_pExtNcFrameImpl != NULL );
	ASSERT( m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow()->GetSafeHwnd() != NULL );
	ASSERT( m_pExtNcFrameImpl->NcFrameImpl_IsSupported() );
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
	if(	! pPM->Ribbon_FileButtonCalcLayout(
			dc,
			m_pExtNcFrameImpl,
			pFileTBB
			)
		)
		return false;

	if(		pPM->Ribbon_FileButtonIsItegrationSupported()
		&&	RibbonQuickAccessBar_AboveTheRibbonGet()
		)
	{
		CRect rcFileTBB = *pFileTBB;
		CRect rcAlignContent;
		GetClientRect( &rcAlignContent );
		INT nTopBorderHeight = 0;
		INT nFrameCaptionHeight = RibbonLayout_GetFrameCaptionHeight( &nTopBorderHeight );
		if(		RibbonLayout_IsFrameIntegrationEnabled()
			&&	(! RibbonLayout_IsDwmCaptionIntegration() )
			)
			rcAlignContent.OffsetRect( 0, nFrameCaptionHeight + nTopBorderHeight );
		bool bDwmMode = false;
		if( m_pExtNcFrameImpl != NULL && m_pExtNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement() )
			bDwmMode = true;
		rcAlignContent.top += pPM->RibbonQuickAccessBar_GetAdditionalShiftFromTop( bDwmMode, this );
		rcAlignContent.bottom = rcAlignContent.top + nFrameCaptionHeight;
		rcAlignContent.left = RibbonLayout_CalcQatbAboveRibbonHorzStart();
		if( rcAlignContent.left < 0 )
			rcAlignContent.left = rcFileTBB.right + 2;
		m_rcHelperQA = rcAlignContent;
		INT nIndex, nCount, nCountItegrated = 0;
		nCount = RibbonQuickAccessButton_GetCount();
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtBarButton * pTBB = RibbonQuickAccessButton_GetAt( nIndex );
			ASSERT_VALID( pTBB );
			ASSERT( pTBB->ParentButtonGet() == NULL );
			if( ! pTBB->IsVisible() )
				continue;
			if( ( pTBB->GetStyle() & TBBS_HIDDEN ) != 0 )
				continue;
			CSize _sizeTBB = pTBB->CalculateLayout( dc, _GetDefButtonSize(), TRUE );
			CRect rcTBB(
				rcAlignContent.left,
				rcAlignContent.top,
				rcAlignContent.left + _sizeTBB.cx,
				rcAlignContent.top + _sizeTBB.cy
				);
			rcTBB.OffsetRect( 0, ( rcAlignContent.Height() - _sizeTBB.cy ) / 2 );
			pPM->RibbonQuickAccessBar_AdjustButtonLocation( pTBB, rcTBB );
			pTBB->SetRect( rcTBB );
			pTBB->OnRibbonAlignContent( dc );
			rcAlignContent.left += _sizeTBB.cx;
			nCountItegrated ++;
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
		if( nCountItegrated > 0 )
		{
			rcAlignContent.left += pPM->RibbonQuickAccessBar_GetAdditionalSpaceAtRight( bDwmMode, this );
			rcAlignContent.left += 2;
			m_rcHelperQA.right = rcAlignContent.left;
		}
		else
			m_rcHelperQA.SetRect( 0, 0, 0, 0 );
	} // if( pPM->Ribbon_FileButtonIsItegrationSupported() ...

	return true;
}

bool CExtRibbonBar::Ribbon_FileButtonPaint( CDC & dc )
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
	if( pFileTBB == NULL )
		return false;
	if( ! RibbonLayout_IsFrameIntegrationEnabled() )
		return false;
	ASSERT( m_pExtNcFrameImpl != NULL );
	ASSERT( m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow()->GetSafeHwnd() != NULL );
	ASSERT( m_pExtNcFrameImpl->NcFrameImpl_IsSupported() );
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
	if(	! pPM->Ribbon_FileButtonPaint( dc, pFileTBB ) )
		return false;
	return true;
}

void CExtRibbonBar::_OnCmdRibbonQuickAccessToolbarPlaceAbove()
{
	ASSERT_VALID( this );
//HWND hWndSurface = NULL;
//	if( RibbonLayout_IsFrameIntegrationEnabled() )
//	{
//		ASSERT( m_pExtNcFrameImpl != NULL );
//		ASSERT( m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow()->GetSafeHwnd() != NULL );
//		ASSERT( m_pExtNcFrameImpl->NcFrameImpl_IsSupported() );
//		CWnd * pWnd = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
//		if( (pWnd->GetStyle()&(WS_VISIBLE|WS_CHILD)) == WS_VISIBLE )
//		{
//			CRect rcSurface;
//			pWnd->GetWindowRect( &rcSurface );
//			hWndSurface =
//				::CreateWindowEx(
//					0,
//					_T("Static"),
//					_T(""),
//					WS_POPUP,
//					rcSurface.left,
//					rcSurface.top,
//					rcSurface.Width(),
//					rcSurface.Height(),
//					pWnd->m_hWnd,
//					(HMENU)NULL,
//					::AfxGetInstanceHandle(),
//					NULL
//					);
//			if( hWndSurface != NULL )
//			{
//				::EnableWindow( hWndSurface, FALSE );
//				::ShowWindow( hWndSurface, SW_SHOWNOACTIVATE );
//			} // if( hWndSurface != NULL )
//		} // if( (pWnd->GetStyle()&(WS_VISIBLE|WS_CHILD)) == WS_VISIBLE )
//	} // if( RibbonLayout_IsFrameIntegrationEnabled() )
	RibbonQuickAccessBar_AboveTheRibbonSet( true, false );
	m_bRibbonSimplifiedLayoutDelayedFlush = true;
	_RecalcPositionsImpl();
	m_bRibbonSimplifiedLayoutDelayedFlush = true;
CFrameWnd * pFrameWnd = GetParentFrame();
	if( pFrameWnd != NULL )
	{
		pFrameWnd->RecalcLayout();
		pFrameWnd->SendMessage( WM_NCPAINT );
	} // if( pFrameWnd != NULL )
	else
	{
		CWnd * pWndParent = GetParent();
		if( pWndParent != NULL )
		{
			pWndParent->RepositionBars( 0, 0xFFFF, 0 );
			pWndParent->SendMessage( WM_NCPAINT );
		} // if( pWndParent != NULL )
	} // else from if( pFrameWnd != NULL )
//	if( hWndSurface != NULL )
//		::DestroyWindow( hWndSurface );
	CExtPaintManager::stat_PassPaintMessages();
}

void CExtRibbonBar::_OnCmdRibbonQuickAccessToolbarPlaceBelow()
{
	ASSERT_VALID( this );
//HWND hWndSurface = NULL;
//	if( RibbonLayout_IsFrameIntegrationEnabled() )
//	{
//		ASSERT( m_pExtNcFrameImpl != NULL );
//		ASSERT( m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow()->GetSafeHwnd() != NULL );
//		ASSERT( m_pExtNcFrameImpl->NcFrameImpl_IsSupported() );
//		CWnd * pWnd = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
//		if( (pWnd->GetStyle()&(WS_VISIBLE|WS_CHILD)) == WS_VISIBLE )
//		{
//			CRect rcSurface;
//			pWnd->GetWindowRect( &rcSurface );
//			hWndSurface =
//				::CreateWindowEx(
//					0,
//					_T("Static"),
//					_T(""),
//					WS_POPUP,
//					rcSurface.left,
//					rcSurface.top,
//					rcSurface.Width(),
//					rcSurface.Height(),
//					pWnd->m_hWnd,
//					(HMENU)NULL,
//					::AfxGetInstanceHandle(),
//					NULL
//					);
//			if( hWndSurface != NULL )
//			{
//				::EnableWindow( hWndSurface, FALSE );
//				::ShowWindow( hWndSurface, SW_SHOWNOACTIVATE );
//			} // if( hWndSurface != NULL )
//		} // if( (pWnd->GetStyle()&(WS_VISIBLE|WS_CHILD)) == WS_VISIBLE )
//	} // if( RibbonLayout_IsFrameIntegrationEnabled() )
	RibbonQuickAccessBar_AboveTheRibbonSet( false, false );
	m_bRibbonSimplifiedLayoutDelayedFlush = true;
	_RecalcPositionsImpl();
CFrameWnd * pFrameWnd = GetParentFrame();
	if( pFrameWnd != NULL )
	{
		pFrameWnd->RecalcLayout();
		pFrameWnd->SendMessage( WM_NCPAINT );
	} // if( pFrameWnd != NULL )
	else
	{
		CWnd * pWndParent = GetParent();
		if( pWndParent != NULL )
		{
			pWndParent->RepositionBars( 0, 0xFFFF, 0 );
			pWndParent->SendMessage( WM_NCPAINT );
		} // if( pWndParent != NULL )
	} // else from if( pFrameWnd != NULL )
//	if( hWndSurface != NULL )
//		::DestroyWindow( hWndSurface );
	CExtPaintManager::stat_PassPaintMessages();
}

void CExtRibbonBar::_OnCmdRibbonQuickAccessToolbarCustomize()
{
	ASSERT_VALID( this );
	OnRibbonOptionsDialogTrack( __EXT_RIBBON_OPTIONS_DIALOG_PAGE_CQATB );
}

void CExtRibbonBar::_OnCmdRibbonShowOptionsDlg()
{
	ASSERT_VALID( this );
	OnRibbonOptionsDialogTrack();
}

void CExtRibbonBar::_OnCmdRibbonQuickAccessToolbarReset()
{
	ASSERT_VALID( this );
	_OnCmdRibbonQuickAccessToolbarReset( this );
}

void CExtRibbonBar::_OnCmdRibbonQuickAccessToolbarReset(
	CWnd * pWndForMsgBox
	)
{
	ASSERT_VALID( this );
	if( pWndForMsgBox != NULL )
	{
		ASSERT_VALID( pWndForMsgBox );
		ASSERT( pWndForMsgBox->GetSafeHwnd() != NULL );
		CExtSafeString strQuestionText, strQuestionCaption;
		if(		(! g_ResourceManager->LoadString(
					strQuestionText,
					ID_EXT_RIBBON_QATB_RESET_QUESTION_TEXT
					) )
			||	(! g_ResourceManager->LoadString(
					strQuestionCaption,
					ID_EXT_RIBBON_QATB_RESET_QUESTION_CAPTION
					) )
			)
			return;
#if (!defined __EXT_MFC_NO_MSG_BOX)
		if(	::ProfUISMsgBox( pWndForMsgBox->GetSafeHwnd(), LPCTSTR(strQuestionText), LPCTSTR(strQuestionCaption), MB_YESNO|MB_ICONWARNING ) != IDYES )
			return;
#else
		if(	::MessageBox( pWndForMsgBox->GetSafeHwnd(), LPCTSTR(strQuestionText), LPCTSTR(strQuestionCaption), MB_YESNO|MB_ICONWARNING ) != IDYES )
			return;
#endif
	} // if( pWndForMsgBox != NULL )
INT nIndex, nCount = RibbonQuickAccessButton_GetCount();
	for( nIndex = 0; nIndex < nCount; )
	{
		CExtBarButton * pTBB = RibbonQuickAccessButton_GetAt( nIndex );
		ASSERT_VALID( pTBB );
		if( pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonQuickAccessContentExpand ) ) )
		{
			nIndex ++;
			continue;
		}
		CExtCustomizeCmdTreeNode * pNode = pTBB->GetCmdNode();
		if( pNode == NULL )
		{
			nIndex ++;
			continue;
		}
		ASSERT_VALID( pNode );
		if( (pNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
		{
			INT nPlainIndex = _GetIndexOf( pTBB );
			ASSERT( nPlainIndex >= 0 );
			m_arrQuickAccessButtons.RemoveAt( nIndex );
			CExtToolControlBar::RemoveButton( nPlainIndex );
			pNode->RemoveSelf( NULL );
			nCount --;
			continue;
		} // if( (pNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
		else
		{
			pTBB->ModifyStyle( 0, TBBS_HIDDEN );
			nIndex ++;
			continue;
		} // else from if( (pNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
	} // for( nIndex = 0; nIndex < nCount; )

	Ribbon_OnRecalcLayout();
	if(		PmBridge_GetPM()->Ribbon_FileButtonIsItegrationSupported()
		&&	RibbonQuickAccessBar_AboveTheRibbonGet()
		)
	{
		CWnd * pWnd = GetParent();
		if(		pWnd != NULL
			&&	( pWnd->GetStyle() & WS_CHILD ) == 0
			)
			pWnd->SendMessage( WM_NCPAINT );
	}
}

void CExtRibbonBar::_OnCmdRibbonQuickAccessToolbarAddTo()
{
	ASSERT_VALID( this );
	if( m_nHelperCtxIndexQA < 0 || m_nHelperCtxIndexQA >= GetButtonsCount() )
		return;
CExtRibbonNode * pRibbonNodeRoot = Ribbon_GetRootNode();
	if( pRibbonNodeRoot == NULL )
		return;
	ASSERT_VALID( pRibbonNodeRoot );
CExtBarButton * pTBB = GetButton( m_nHelperCtxIndexQA );
	m_nHelperCtxIndexQA = -1;
	if( pTBB == NULL )
		return;
	ASSERT_VALID( pTBB );
INT nIdxQA = RibbonQuickAccessButton_GetIndexOf( pTBB );
	if( nIdxQA >= 0 )
		return;
CExtCustomizeCmdTreeNode * pNode = pTBB->GetCmdNode();
	if( pNode == NULL )
		return;
	ASSERT_VALID( pNode );
CExtRibbonNode * pRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNode );
	if( pRibbonNode == NULL )
		return;
CExtRibbonNodeQuickAccessButtonsCollection * pParentNode =
		Ribbon_GetQuickAccessRootNode();
	if( pParentNode == NULL )
		return;

	if( _OnRibbonBarQueryUniqueButtonModeForQATB() )
	{
		UINT nAddCmdID = pNode->GetCmdID( false );
		INT nIndex, nCount = pParentNode->GetNodeCount();
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtCustomizeCmdTreeNode * pNodeInsideQATB = pParentNode->ElementAt( nIndex );
			ASSERT_VALID( pNodeInsideQATB );
			UINT nCmdID = pNodeInsideQATB->GetCmdID( false );
			if( nCmdID == nAddCmdID )
				return; // already present inside QATB
		}
	} // if( _OnRibbonBarQueryUniqueButtonModeForQATB() )

CExtBarButton * pButton = pRibbonNode->OnRibbonCreateBarButton( this, NULL );
CExtCustomizeCmdTreeNode * pNewNode = pNode->CloneNode();
	if( pNewNode == NULL )
		return;
CExtRibbonNode * pNewRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNewNode );
	if( pNewRibbonNode != NULL )
	{
		CArray < DWORD, DWORD > arrILEtoILV;
		arrILEtoILV.Add(
			__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
				__EXT_RIBBON_ILE_MAX,
				__EXT_RIBBON_ILV_SIMPLE_SMALL,
				false
				)
			);
		pNewRibbonNode->RibbonILE_RuleArraySet( arrILEtoILV );
	} // if( pNewRibbonNode != NULL )
	pNewNode->ModifyFlags( __ECTN_RIBBON_QA_CLONED_COPY );
	pParentNode->InsertNode( NULL, pNewNode );
	pButton->SetBasicCmdNode( pNewNode );
	pButton->SetCustomizedCmdNode( pNewNode );
	CExtToolControlBar::InsertSpecButton( -1, pButton, FALSE );
	if( m_pQACEB == NULL )
	{
		m_arrQuickAccessButtons.Add( pButton );
		m_pQACEB = new CExtRibbonButtonQuickAccessContentExpand( this );
		InsertSpecButton( -1, m_pQACEB, FALSE );
		m_arrQuickAccessButtons.Add( m_pQACEB );
	}
	else
	{
		ASSERT( INT(m_arrQuickAccessButtons.GetSize()) > 0 );
		m_arrQuickAccessButtons.InsertAt( INT(m_arrQuickAccessButtons.GetSize()) - 1, pButton, 1 );
	}

	Ribbon_OnRecalcLayout();
	if(		PmBridge_GetPM()->Ribbon_FileButtonIsItegrationSupported()
		&&	RibbonQuickAccessBar_AboveTheRibbonGet()
		)
	{
		CWnd * pWnd = GetParent();
		if(		pWnd != NULL
			&&	( pWnd->GetStyle() & WS_CHILD ) == 0
			)
			pWnd->SendMessage( WM_NCPAINT );
	}
}

void CExtRibbonBar::_OnCmdRibbonQuickAccessToolbarAddTo_Update( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	ASSERT( pCmdUI );
BOOL bEnable = TRUE;
	if( m_nHelperCtxIndexQA < 0 || m_nHelperCtxIndexQA >= GetButtonsCount() )
		bEnable = FALSE;
	else
	{
		CExtRibbonNode * pRibbonNodeRoot = Ribbon_GetRootNode();
		if( pRibbonNodeRoot == NULL )
			bEnable = FALSE;
		ASSERT_VALID( pRibbonNodeRoot );
		CExtBarButton * pTBB = GetButton( m_nHelperCtxIndexQA );
		if( pTBB == NULL )
			bEnable = FALSE;
		else
		{
			ASSERT_VALID( pTBB );
			INT nIdxQA = RibbonQuickAccessButton_GetIndexOf( pTBB );
			if( nIdxQA >= 0 )
				bEnable = FALSE;
			else
			{
				CExtCustomizeCmdTreeNode * pNode = pTBB->GetCmdNode();
				if( pNode == NULL )
					bEnable = FALSE;
				else
				{
					ASSERT_VALID( pNode );
					CExtRibbonNode * pRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNode );
					if( pRibbonNode == NULL )
						bEnable = FALSE;
					else
					{
						CExtRibbonNodeQuickAccessButtonsCollection * pParentNode = Ribbon_GetQuickAccessRootNode();
						if( pParentNode == NULL )
							bEnable = FALSE;
						else
						{
							ASSERT_VALID( pParentNode );
							if( _OnRibbonBarQueryUniqueButtonModeForQATB() )
							{
								UINT nAddCmdID = pNode->GetCmdID( false );
								INT nIndex, nCount = pParentNode->GetNodeCount();
								for( nIndex = 0; nIndex < nCount; nIndex ++ )
								{
									CExtCustomizeCmdTreeNode * pNodeInsideQATB = pParentNode->ElementAt( nIndex );
									ASSERT_VALID( pNodeInsideQATB );
									UINT nCmdID = pNodeInsideQATB->GetCmdID( false );
									if( nCmdID == nAddCmdID )
									{
										bEnable = FALSE; // already present inside QATB
										break;
									}
								}
							} // if( _OnRibbonBarQueryUniqueButtonModeForQATB() )
						}
					}
				}
			}
		}
	}
	pCmdUI->Enable( bEnable );
}

void CExtRibbonBar::_OnCmdRibbonQuickAccessToolbarRemoveFrom()
{
	ASSERT_VALID( this );
	if( m_nHelperCtxIndexQA < 0 || m_nHelperCtxIndexQA >= GetButtonsCount() )
		return;
CExtRibbonNode * pRibbonNode = Ribbon_GetRootNode();
	if( pRibbonNode == NULL )
		return;
	ASSERT_VALID( pRibbonNode );

CExtBarButton * pTBB = GetButton( m_nHelperCtxIndexQA );
INT nPlainIdxSaved = m_nHelperCtxIndexQA;
	m_nHelperCtxIndexQA = -1;
	if( pTBB == NULL )
		return;
	ASSERT_VALID( pTBB );
INT nIdxQA = RibbonQuickAccessButton_GetIndexOf( pTBB );
	if( nIdxQA < 0 )
		return;
CExtCustomizeCmdTreeNode * pNode = pTBB->GetCmdNode();
	if( pNode == NULL )
		return;
	ASSERT_VALID( pNode );

	if( (pNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
	{
		m_arrQuickAccessButtons.RemoveAt( nIdxQA );
		CExtToolControlBar::RemoveButton( nPlainIdxSaved );
		pNode->RemoveSelf( NULL );
	} // if( (pNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
	else
		pTBB->ModifyStyle( TBBS_HIDDEN );

	Ribbon_OnRecalcLayout();
	if(		PmBridge_GetPM()->Ribbon_FileButtonIsItegrationSupported()
		&&	RibbonQuickAccessBar_AboveTheRibbonGet()
		)
	{
		CWnd * pWnd = GetParent();
		if(		pWnd != NULL
			&&	( pWnd->GetStyle() & WS_CHILD ) == 0
			)
			pWnd->SendMessage( WM_NCPAINT );
	}
}

void CExtRibbonBar::_OnCmdRibbonMinimize()
{
	ASSERT_VALID( this );
//	if( ! Ribbon_AutoHideModeEnabledGet() )
//		return;
	RibbonPage_ExpandedModeSet( ! RibbonPage_ExpandedModeGet(), true );
}

bool CExtRibbonBar::OnRibbonPrepareButtonContextMenu(
	CExtPopupMenuWnd * pWndPopupMenu,
	CExtBarButton * pTBB,
	UINT nFlags,
	CPoint point
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pWndPopupMenu );
	ASSERT_VALID( pTBB );
	pWndPopupMenu;
	pTBB;
	nFlags;
	point;
	return true;
}

bool CExtRibbonBar::OnRibbonPrepareBarContextMenu(
	CExtPopupMenuWnd * pWndPopupMenu,
	UINT nFlags,
	CPoint point
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pWndPopupMenu );
	pWndPopupMenu;
	nFlags;
	point;

HWND hWnd = ::WindowFromPoint( point );
	if( hWnd != m_hWnd )
		return true; // some popup part of ribbon is clicked
CPoint ptClient = point;
	ScreenToClient( &ptClient );
INT nIdx = HitTest( ptClient );
	if( nIdx < 0 )
		return true; // click is not over any button
CExtBarButton * pTBB = GetButton( nIdx );
	ASSERT_VALID( pTBB );
CExtRibbonButtonTabPage * pRibbonTabPageTBB = DYNAMIC_DOWNCAST( CExtRibbonButtonTabPage, pTBB );
	if( pRibbonTabPageTBB == NULL )
		return true; // click is not over tab page button
	
	return true;
}

bool CExtRibbonBar::OnRibbonTrackButtonContextMenu(
	CExtBarButton * pTBB,
	UINT nFlags,
	CPoint point
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
	nFlags;
	if( GetSafeHwnd() == NULL )
		return false;
//	if(		RibbonLayout_IsFrameIntegrationEnabled()
//		&&	(! m_rcHelperEmbeddedCaptionText.IsRectEmpty() )
//		&&	m_rcHelperEmbeddedCaptionText.PtInRect( point )
//		)
//		return false;
	if(		pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonFile ) )
		||	pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonQuickAccessContentExpand ) )
		||	pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonTabPage ) )
		||	RibbonRightButton_GetIndexOf( pTBB ) >= 0
		)
		return false;
#if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	BackstageView_EnsureHidden();
#endif // (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
CExtBarButton * pCtxTBB = NULL;
CExtRibbonButtonDialogLauncher * pDlbTBB = DYNAMIC_DOWNCAST( CExtRibbonButtonDialogLauncher, pTBB );
	if( pDlbTBB != NULL )
		pTBB = pDlbTBB->ParentButtonGet();
CExtRibbonButtonGroup * pGroupTBB = DYNAMIC_DOWNCAST( CExtRibbonButtonGroup, pTBB );
	if( pGroupTBB != NULL )
	{
		for( ; pTBB != NULL; )
		{
			ASSERT_VALID( pGroupTBB );
			CExtBarButton * pTBB2 = pGroupTBB->ParentButtonGet();
			if( pTBB2 == NULL )
				break;
			ASSERT_VALID( pTBB2 );
			pGroupTBB = DYNAMIC_DOWNCAST( CExtRibbonButtonGroup, pTBB2 );
			if( pGroupTBB == NULL )
				break;
		} // for( ; pTBB != NULL; )
		if( pGroupTBB == NULL )
			return false;
		pCtxTBB = pGroupTBB;
	} // if( pGroupTBB != NULL )
	else
	{
		if( pTBB->ChildButtonGetCount() != 0 )
			return false;
		pCtxTBB = pTBB;
	} // else from if( pGroupTBB != NULL )
	ASSERT( pCtxTBB != NULL );
	ASSERT_VALID( pCtxTBB );
	ClientToScreen( &point );
DWORD dwTrackFlags = TPMX_OWNERDRAW_FIXED|TPMX_COMBINE_NONE|TPMX_RIBBON_MODE|TPMX_NO_HIDE_RARELY|TPMX_FORCE_NO_ANIMATION;
CExtPopupMenuWnd * pPopup = CExtPopupMenuWnd::InstantiatePopupMenu( GetSafeHwnd(), RUNTIME_CLASS(CExtPopupMenuWnd), this );
	if( ! pPopup->LoadMenu( GetSafeHwnd(), IDR_EXT_RIBON_CTX_MENU, true, true ) )
	{
		delete pPopup;
		return false;
	}
bool bQaButton = ( RibbonQuickAccessButton_GetIndexOf( pCtxTBB ) >= 0 ) ? true : false;
bool bRibbonQuickAccessBarIsAboveTheRibbon = RibbonQuickAccessBar_AboveTheRibbonGet();
INT nIndexInQA = RibbonQuickAccessButton_GetIndexOf( pCtxTBB );
INT nIndex, nCount = pPopup->ItemGetCount();
	for( nIndex = 0; nIndex < nCount; )
	{
		UINT nCmdID = pPopup->ItemGetCmdID( nIndex );
		switch( nCmdID )
		{
		case ID_EXT_RIBBON_QATB_ADD_TO:
		{
			CExtPopupMenuWnd::MENUITEMDATA & _mii = pPopup->ItemGetInfo( nIndex );
			_mii.SetNoCmdUI( false ); // The ID_EXT_RIBBON_QATB_ADD_TO command should use Cmd UI
			if( nIndexInQA >= 0 )
				break;
			pPopup->ItemInsert( 0, nIndex + 1 );
			nIndex += 2;
			nCount ++;
			continue;
		}
		case ID_EXT_RIBBON_QATB_REMOVE_FROM:
			if( nIndexInQA < 0 )
				break;
			pPopup->ItemInsert( 0, nIndex + 1 );
			nIndex += 2;
			nCount ++;
			continue;

		case ID_EXT_RIBBON_MINIMIZE:
			//if( Ribbon_AutoHideModeEnabledGet() )
			{
				CExtPopupMenuWnd::MENUITEMDATA & mi =
					pPopup->ItemGetInfo( nIndex );
				mi.SetNoCmdUI( true );
				mi.Enable( true );
				mi.Check( ! RibbonPage_ExpandedModeGet() );
				if( nIndex > 0 )
				{
					pPopup->ItemInsert( ID_SEPARATOR, nIndex );
					nIndex ++;
				}
				nIndex ++;
				continue;
			} // if( Ribbon_AutoHideModeEnabledGet() )
			break;
		case ID_EXT_RIBBON_QATB_PLACE_BELOW:
			if( bRibbonQuickAccessBarIsAboveTheRibbon && bQaButton )
			{
				nIndex ++;
				continue;
			}
			break;
		case ID_EXT_RIBBON_QATB_PLACE_ABOVE:
			if( ( ! bRibbonQuickAccessBarIsAboveTheRibbon ) && bQaButton )
			{
				nIndex ++;
				continue;
			}
			break;

		case ID_EXT_RIBBON_QATB_RESET:
		case ID_EXT_RIBBON_QATB_CUSOMIZE:
			nIndex ++;
			continue;
		} // switch( nCmdID )
		pPopup->ItemRemove( nIndex );
		nCount --;
	} // for( nIndex = 0; nIndex < nCount; )
	m_nHelperCtxIndexQA = _GetIndexOf( pCtxTBB );
	pPopup->m_hWndNotifyMenuClosed = GetSafeHwnd();
	if( ! OnRibbonPrepareButtonContextMenu( pPopup, pTBB, nFlags, point ) )
	{
		delete pPopup;
		return false;
	}
	if( pPopup->ItemGetCount() == 0 ) 
	{
		delete pPopup;
		return false;
	}
	if( ! pPopup->TrackPopupMenu( dwTrackFlags, point.x, point.y ) )
	{
		//delete pPopup;
		return false;
	}
	return true;
}

bool CExtRibbonBar::OnRibbonTrackBarContextMenu(
	UINT nFlags,
	CPoint point
	)
{
	ASSERT_VALID( this );
	nFlags;
	if( GetSafeHwnd() == NULL )
		return false;
	ClientToScreen( &point );
	if(		RibbonLayout_IsFrameIntegrationEnabled()
		&&	RibbonQuickAccessBar_AboveTheRibbonGet()
		&&	(! m_rcHelperEmbeddedCaptionText.IsRectEmpty() )
		&&	m_rcHelperEmbeddedCaptionText.PtInRect( point )
		)
		return false;
	else if( GetParent()->SendMessage( WM_NCHITTEST, 0, MAKELPARAM( point.x, point.y ) ) != HTCLIENT )
		return false;
#if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	BackstageView_EnsureHidden();
#endif // (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
DWORD dwTrackFlags = TPMX_OWNERDRAW_FIXED|TPMX_COMBINE_NONE|TPMX_RIBBON_MODE|TPMX_NO_HIDE_RARELY|TPMX_FORCE_NO_ANIMATION;
CExtPopupMenuWnd * pPopup = CExtPopupMenuWnd::InstantiatePopupMenu( GetSafeHwnd(), RUNTIME_CLASS(CExtPopupMenuWnd), this );
	if( ! pPopup->LoadMenu( GetSafeHwnd(), IDR_EXT_RIBON_CTX_MENU, true, true ) )
	{
		delete pPopup;
		return false;
	}
bool bRibbonQuickAccessBarIsAboveTheRibbon = RibbonQuickAccessBar_AboveTheRibbonGet();
INT nIndex, nCount = pPopup->ItemGetCount();
	for( nIndex = 0; nIndex < nCount; )
	{
		UINT nCmdID = pPopup->ItemGetCmdID( nIndex );
		switch( nCmdID )
		{
		case ID_EXT_RIBBON_MINIMIZE:
			//if( Ribbon_AutoHideModeEnabledGet() )
			{
				CExtPopupMenuWnd::MENUITEMDATA & mi =
					pPopup->ItemGetInfo( nIndex );
				mi.SetNoCmdUI( true );
				mi.Enable( true );
				mi.Check( ! RibbonPage_ExpandedModeGet() );
				if( nIndex > 0 )
				{
					pPopup->ItemInsert( ID_SEPARATOR, nIndex );
					nIndex ++;
				}
				nIndex ++;
				continue;
			} // if( Ribbon_AutoHideModeEnabledGet() )
			break;
		case ID_EXT_RIBBON_QATB_PLACE_BELOW:
			if( bRibbonQuickAccessBarIsAboveTheRibbon )
			{
				nIndex ++;
				continue;
			}
			break;
		case ID_EXT_RIBBON_QATB_PLACE_ABOVE:
			if( ! bRibbonQuickAccessBarIsAboveTheRibbon )
			{
				nIndex ++;
				continue;
			}
			break;
		case ID_EXT_RIBBON_QATB_RESET:
		case ID_EXT_RIBBON_QATB_CUSOMIZE:
			nIndex ++;
			continue;
		} // switch( nCmdID )
		pPopup->ItemRemove( nIndex );
		nCount --;
	} // for( nIndex = 0; nIndex < nCount; )
	pPopup->m_hWndNotifyMenuClosed = GetSafeHwnd();
	if( ! OnRibbonPrepareBarContextMenu( pPopup, nFlags, point ) )
	{
		delete pPopup;
		return false;
	}
	if( pPopup->ItemGetCount() == 0 )
	{
		delete pPopup;
		return false;
	}
	if( ! pPopup->TrackPopupMenu( dwTrackFlags, point.x, point.y ) )
	{
		//delete pPopup;
		return false;
	}
	return true;
}

void CExtRibbonBar::_OnRibbonPageExpandedModeSet( bool bRibbonPageIsExpandedMode )
{
	ASSERT_VALID( this );
	if( bRibbonPageIsExpandedMode == RibbonPage_ExpandedModeGet() )
		return;
	ASSERT_VALID( this );
	_RibbonPageRslaResetStateData();
//HWND hWndSurface = NULL;
//	if( RibbonLayout_IsFrameIntegrationEnabled() )
//	{
//		ASSERT( m_pExtNcFrameImpl != NULL );
//		ASSERT( m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow()->GetSafeHwnd() != NULL );
//		ASSERT( m_pExtNcFrameImpl->NcFrameImpl_IsSupported() );
//		CWnd * pWnd = m_pExtNcFrameImpl->NcFrameImpl_GetFrameWindow();
//		if( (pWnd->GetStyle()&(WS_VISIBLE|WS_CHILD)) == WS_VISIBLE )
//		{
//			CRect rcSurface;
//			pWnd->GetWindowRect( &rcSurface );
//			hWndSurface =
//				::CreateWindowEx(
//					0,
//					_T("Static"),
//					_T(""),
//					WS_POPUP,
//					rcSurface.left,
//					rcSurface.top,
//					rcSurface.Width(),
//					rcSurface.Height(),
//					pWnd->m_hWnd,
//					(HMENU)NULL,
//					::AfxGetInstanceHandle(),
//					NULL
//					);
//			if( hWndSurface != NULL )
//			{
//				::EnableWindow( hWndSurface, FALSE );
//				::ShowWindow( hWndSurface, SW_SHOWNOACTIVATE );
//			} // if( hWndSurface != NULL )
//		} // if( (pWnd->GetStyle()&(WS_VISIBLE|WS_CHILD)) == WS_VISIBLE )
//	} // if( RibbonLayout_IsFrameIntegrationEnabled() )
	RibbonPage_ExpandedModeSet( bRibbonPageIsExpandedMode, false );
	_RecalcPositionsImpl();
CFrameWnd * pFrameWnd = GetParentFrame();
	if( pFrameWnd != NULL )
		pFrameWnd->RecalcLayout();
	else
		GetParent()->RepositionBars( 0, 0xFFFF, 0 );
//	if( hWndSurface != NULL )
//		::DestroyWindow( hWndSurface );
	CExtPaintManager::stat_PassPaintMessages();
}

bool CExtRibbonBar::_OnRibbonBarQueryUniqueButtonModeForQATB() const
{
	ASSERT_VALID(this);
	return m_bUniqueButtonsInQATB;
}

// for axel.lehnert@innoplus.de, 2/2/2016
BOOL CExtRibbonBar::_OnRibbonBarQueryEnableAddNodeInsideQATB(CExtCustomizeCmdTreeNode * pNode) const
{
	ASSERT_VALID(this);
	pNode;
	return TRUE;
}
// for axel.lehnert@innoplus.de, 2/2/2016

INT CExtRibbonBar::OnCalcFlatTrackingIndex(
	/*__EXT_MFC_SAFE_TCHAR*/ INT vkTCHAR,
	INT nStartSearchIndex
	)
{
	ASSERT_VALID( this );
	if(		(! m_bHelperFlatTrackingCalcEnabledLeftRight )
		&&	(! m_bHelperFlatTrackingCalcEnabledUpDown )
		)
		return -1;
	if(		vkTCHAR != VK_LEFT
		&&	vkTCHAR != VK_RIGHT
		&&	vkTCHAR != VK_UP
		&&	vkTCHAR != VK_DOWN
		&&	vkTCHAR != VK_TAB
		&&	vkTCHAR != VK_MENU
		)
		return -1;
INT nRightIndex = -1, nTabPageIndex = -1, nQuickAccessIndex = -1;
CExtBarButton * pStartTBB = NULL;
	if( nStartSearchIndex >= 0 && vkTCHAR != VK_MENU )
	{
		CExtBarButton * pTBB = GetButton( nStartSearchIndex );
		ASSERT_VALID( pTBB );
		pStartTBB = pTBB;
		nRightIndex = RibbonRightButton_GetIndexOf( pStartTBB );
		if( pStartTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonTabPage ) ) )
			nTabPageIndex = RibbonTabPageButton_GetIndexOf( (CExtRibbonButtonTabPage*)pStartTBB );
		nQuickAccessIndex = RibbonQuickAccessButton_GetIndexOf( pStartTBB );
		if( pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonQuickAccessContentExpand ) ) )
		{
			if(		m_bHelperFlatTrackingCalcEnabledLeftRight
				&&	vkTCHAR == VK_RIGHT
				&&	RibbonQuickAccessBar_AboveTheRibbonGet()
				&&	RibbonLayout_IsFrameIntegrationEnabled()
				&&	RibbonLayout_GetFrameCaptionHeight() > 0
				&&	PmBridge_GetPM()->Ribbon_FileButtonIsItegrationSupported()
				)
			{
				CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
				if( pFileTBB == NULL )
					return nStartSearchIndex;
				ASSERT_VALID( pFileTBB );
				if( ! pFileTBB->OnQueryFlatTrackingEnabled() )
					return nStartSearchIndex;
				nStartSearchIndex = _GetIndexOf( pFileTBB );
				return nStartSearchIndex;
			}
		} // if( pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonQuickAccessContentExpand ) ) )
		else if( pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonFile ) ) )
		{
			if(		m_bHelperFlatTrackingCalcEnabledLeftRight
				&&	vkTCHAR == VK_LEFT
				)
			{
				if(		RibbonQuickAccessBar_AboveTheRibbonGet()
					&&	RibbonLayout_IsFrameIntegrationEnabled()
					&&	RibbonLayout_GetFrameCaptionHeight() > 0
					&&	PmBridge_GetPM()->Ribbon_FileButtonIsItegrationSupported()
					)
				{
					pTBB = NULL;
					INT nIndex, nCount = RibbonQuickAccessButton_GetCount();
					for( nIndex = 0; nIndex < nCount; nIndex ++ )
					{
						pTBB = RibbonQuickAccessButton_GetAt( nCount - nIndex - 1 );
						if( pTBB->OnQueryFlatTrackingEnabled() )
							break;
						pTBB = NULL;
					} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
					if( pTBB != NULL )
						nStartSearchIndex = _GetIndexOf( pTBB );
					return nStartSearchIndex;
				}
				else
				{
					pTBB = NULL;
					INT nIndex, nCount = RibbonRightButton_GetCount();
					for( nIndex = 0; nIndex < nCount; nIndex ++ )
					{
						pTBB = RibbonRightButton_GetAt( nCount - nIndex - 1 );
						if( pTBB->OnQueryFlatTrackingEnabled() )
							break;
						pTBB = NULL;
					} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
					if( pTBB != NULL )
					{
						nStartSearchIndex = _GetIndexOf( pTBB );
						return nStartSearchIndex;
					}
					pTBB = NULL;
					nCount = RibbonTabPageButton_GetCount();
					for( nIndex = 0; nIndex < nCount; nIndex ++ )
					{
						pTBB = RibbonTabPageButton_GetAt( nCount - nIndex - 1 );
						if( pTBB->OnQueryFlatTrackingEnabled() )
							break;
						pTBB = NULL;
					} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
					if( pTBB != NULL )
						nStartSearchIndex = _GetIndexOf( pTBB );
					return nStartSearchIndex;
				}
			}
		} // else if( pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonFile ) ) )
		else
		{ // other special cases
			if( nRightIndex >= 0 )
			{
				if( m_bHelperFlatTrackingCalcEnabledLeftRight )
				{
					if( vkTCHAR == VK_LEFT )
					{
						pTBB = NULL;
						INT nIndex = nRightIndex, nCount;
						for( ; nIndex > 0; nIndex -- )
						{
							pTBB = RibbonRightButton_GetAt( nIndex - 1 );
							ASSERT_VALID( pTBB );
							if( pTBB->OnQueryFlatTrackingEnabled() )
								break;
							pTBB = NULL;
						} // for( ; nIndex > 0; nIndex -- )
						if( pTBB != NULL )
						{
							nStartSearchIndex = _GetIndexOf( pTBB );
							return nStartSearchIndex;
						}
						nCount = RibbonTabPageButton_GetCount();
						for( nIndex = 0; nIndex < nCount; nIndex ++ )
						{
							pTBB = RibbonTabPageButton_GetAt( nCount - nIndex - 1 );
							ASSERT_VALID( pTBB );
							if( pTBB->OnQueryFlatTrackingEnabled() )
								break;
							pTBB = NULL;
						} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
						if( pTBB != NULL )
						{
							pTBB->OnDeliverCmd();
							nStartSearchIndex = _GetIndexOf( pTBB );
							return nStartSearchIndex;
						}
						CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
						if(		pFileTBB != NULL
							&&	pFileTBB->OnQueryFlatTrackingEnabled()
							)
						{
							nStartSearchIndex = _GetIndexOf( pFileTBB );
							return nStartSearchIndex;
						}
					} // if( vkTCHAR == VK_LEFT )
					else if( vkTCHAR == VK_RIGHT )
					{
						CExtBarButton * pTBB = NULL;
						INT nIndex = nRightIndex + 1, nCount = RibbonRightButton_GetCount();
						for( ; nIndex < nCount; nIndex ++ )
						{
							pTBB = RibbonRightButton_GetAt( nIndex );
							ASSERT_VALID( pTBB );
							if( pTBB->OnQueryFlatTrackingEnabled() )
								break;
							pTBB = NULL;
						} // for( ; nIndex < nCount; nIndex ++ )
						if( pTBB != NULL )
						{
							nStartSearchIndex = _GetIndexOf( pTBB );
							return nStartSearchIndex;
						}
						CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
						if(		pFileTBB != NULL
							&&	pFileTBB->OnQueryFlatTrackingEnabled()
							)
						{
							nStartSearchIndex = _GetIndexOf( pFileTBB );
							return nStartSearchIndex;
						}
					} // else if( vkTCHAR == VK_RIGHT )
				} // if( m_bHelperFlatTrackingCalcEnabledLeftRight )
				if( m_bHelperFlatTrackingCalcEnabledUpDown )
				{
					if( vkTCHAR == VK_UP )
					{
					} // if( vkTCHAR == VK_UP )
					else if( vkTCHAR == VK_DOWN )
					{
					} // else if( vkTCHAR == VK_DOWN )
				} // if( m_bHelperFlatTrackingCalcEnabledUpDown )
			} // if( nRightIndex >= 0 )
			else if( nTabPageIndex >= 0 )
			{
				if( m_bHelperFlatTrackingCalcEnabledLeftRight )
				{
					if( vkTCHAR == VK_LEFT )
					{
						pTBB = NULL;
						INT nIndex = nTabPageIndex;
						for( ; nIndex > 0; nIndex -- )
						{
							pTBB = RibbonTabPageButton_GetAt( nIndex - 1 );
							ASSERT_VALID( pTBB );
							if( pTBB->OnQueryFlatTrackingEnabled() )
								break;
							pTBB = NULL;
						} // for( ; nIndex > 0; nIndex -- )
						if( pTBB != NULL )
						{
							pTBB->OnDeliverCmd();
							nStartSearchIndex = _GetIndexOf( pTBB );
							return nStartSearchIndex;
						}
						if( RibbonQuickAccessBar_AboveTheRibbonGet() )
						{
							if(		RibbonLayout_IsFrameIntegrationEnabled()
								&&	RibbonLayout_GetFrameCaptionHeight() > 0
								&&	PmBridge_GetPM()->Ribbon_FileButtonIsItegrationSupported()
								)
							{
							}
							else
							{
								INT nCount = RibbonQuickAccessButton_GetCount();
								for( nIndex = 0; nIndex < nCount; nIndex ++ )
								{
									pTBB = RibbonQuickAccessButton_GetAt( nCount - nIndex - 1 );
									ASSERT_VALID( pTBB );
									if( pTBB->OnQueryFlatTrackingEnabled() )
										break;
									pTBB = NULL;
								} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
								if( pTBB != NULL )
								{
									nStartSearchIndex = _GetIndexOf( pTBB );
									return nStartSearchIndex;
								}
							}
						} // if( RibbonQuickAccessBar_AboveTheRibbonGet() )
						CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
						if(		pFileTBB != NULL
							&&	pFileTBB->OnQueryFlatTrackingEnabled()
							)
						{
							nStartSearchIndex = _GetIndexOf( pFileTBB );
							return nStartSearchIndex;
						}
					} // if( vkTCHAR == VK_LEFT )
					else if( vkTCHAR == VK_RIGHT )
					{
						pTBB = NULL;
						INT nIndex = nTabPageIndex + 1, nCount = RibbonTabPageButton_GetCount();
						for( ; nIndex < nCount; nIndex ++ )
						{
							pTBB = RibbonTabPageButton_GetAt( nIndex );
							ASSERT_VALID( pTBB );
							if( pTBB->OnQueryFlatTrackingEnabled() )
								break;
							pTBB = NULL;
						} // for( ; nIndex < nCount; nIndex ++ )
						if( pTBB != NULL )
						{
							pTBB->OnDeliverCmd();
							nStartSearchIndex = _GetIndexOf( pTBB );
							return nStartSearchIndex;
						}
						nCount = RibbonRightButton_GetCount();
						for( nIndex = 0; nIndex < nCount; nIndex ++ )
						{
							pTBB = RibbonRightButton_GetAt( nIndex );
							ASSERT_VALID( pTBB );
							if( pTBB->OnQueryFlatTrackingEnabled() )
								break;
							pTBB = NULL;
						} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
						if( pTBB != NULL )
						{
							nStartSearchIndex = _GetIndexOf( pTBB );
							return nStartSearchIndex;
						}
						CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
						if(		pFileTBB != NULL
							&&	pFileTBB->OnQueryFlatTrackingEnabled()
							)
						{
							nStartSearchIndex = _GetIndexOf( pFileTBB );
							return nStartSearchIndex;
						}
					} // else if( vkTCHAR == VK_RIGHT )
				} // if( m_bHelperFlatTrackingCalcEnabledLeftRight )
				if( m_bHelperFlatTrackingCalcEnabledUpDown )
				{
					if( vkTCHAR == VK_UP )
					{
					} // if( vkTCHAR == VK_UP )
					else if( vkTCHAR == VK_DOWN )
					{
					} // else if( vkTCHAR == VK_DOWN )
				} // if( m_bHelperFlatTrackingCalcEnabledUpDown )
			} // else if( nTabPageIndex >= 0 )
//			else if( nQuickAccessIndex >= 0 )
//			{
//				if( m_bHelperFlatTrackingCalcEnabledLeftRight )
//				{
//					if( vkTCHAR == VK_LEFT )
//					{
//					} // if( vkTCHAR == VK_LEFT )
//					else if( vkTCHAR == VK_RIGHT )
//					{
//					} // else if( vkTCHAR == VK_RIGHT )
//				} // if( m_bHelperFlatTrackingCalcEnabledLeftRight )
//				if( m_bHelperFlatTrackingCalcEnabledUpDown )
//				{
//					if( vkTCHAR == VK_UP )
//					{
//					} // if( vkTCHAR == VK_UP )
//					else if( vkTCHAR == VK_DOWN )
//					{
//						if(		RibbonQuickAccessBar_AboveTheRibbonGet()
//							&&	RibbonLayout_IsFrameIntegrationEnabled()
//							&&	RibbonLayout_GetFrameCaptionHeight() > 0
//							&&	PmBridge_GetPM()->Ribbon_FileButtonIsItegrationSupported()
//							)
//						{
//							pTBB = NULL;
//							INT nIndex, nCount = RibbonTabPageButton_GetCount();
//							for( nIndex = 0; nIndex < nCount; nIndex ++ )
//							{
//								pTBB = RibbonTabPageButton_GetAt( nIndex );
//								ASSERT_VALID( pTBB );
//								if( pTBB->OnQueryFlatTrackingEnabled() )
//									break;
//								pTBB = NULL;
//							} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
//							if( pTBB != NULL )
//							{
//								pTBB->OnDeliverCmd();
//								nStartSearchIndex = _GetIndexOf( pTBB );
//								return nStartSearchIndex;
//							}
//						}
//					} // else if( vkTCHAR == VK_DOWN )
//				} // if( m_bHelperFlatTrackingCalcEnabledUpDown )
//			} // else if( nQuickAccessIndex >= 0 )
		} // other special cases
	} // if( nStartSearchIndex >= 0 && vkTCHAR != VK_MENU )
	else
	{
		bool bRibbonPageIsExpandedMode = RibbonPage_ExpandedModeGet();
		if( ! bRibbonPageIsExpandedMode )
		{
			CExtRibbonButtonFile * pFileTBB =  Ribbon_FileButtonGet();
			if( pFileTBB != NULL )
			{
				ASSERT_VALID( pFileTBB );
				nStartSearchIndex = _GetIndexOf( pFileTBB );
				return nStartSearchIndex;
			} // if( pFileTBB != NULL )
		} // if( ! bRibbonPageIsExpandedMode )
		INT nIndex, nCount = RibbonTabPageButton_GetCount();
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtRibbonButtonTabPage * pTBB = RibbonTabPageButton_GetAt( nIndex );
			ASSERT_VALID( pTBB );
			if( pTBB->IsSelectedRibbonPage() )
			{
				//pTBB->OnDeliverCmd();
				nStartSearchIndex = _GetIndexOf( pTBB );
				return nStartSearchIndex;
			}
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtRibbonButtonTabPage * pTBB = RibbonTabPageButton_GetAt( nIndex );
			ASSERT_VALID( pTBB );
			if( pTBB->OnQueryFlatTrackingEnabled() )
			{
				pTBB->OnDeliverCmd();
				nStartSearchIndex = _GetIndexOf( pTBB );
				return nStartSearchIndex;
			}
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	} // else from if( nStartSearchIndex >= 0 && vkTCHAR != VK_MENU )
INT nFlatTrackingIndex =
		CExtRibbonPage::OnCalcFlatTrackingIndex(
			vkTCHAR,
			nStartSearchIndex
			);
	if(		pStartTBB != NULL
		&&	nFlatTrackingIndex != nStartSearchIndex
		&&	nFlatTrackingIndex >= 0
		&&	nRightIndex < 0
		&&	nTabPageIndex < 0
		&&	nQuickAccessIndex < 0
		&&	(! pStartTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonFile ) ) )
		)
	{
		ASSERT_VALID( pStartTBB );
		CExtBarButton * pNewCalcSelTBB = GetButton( nFlatTrackingIndex );
		ASSERT_VALID( pNewCalcSelTBB );
		CExtRibbonButtonTabPage * pTabTBB = DYNAMIC_DOWNCAST( CExtRibbonButtonTabPage, pNewCalcSelTBB );
		if(		pTabTBB != NULL
			&&	(! pTabTBB->IsSelectedRibbonPage() )
			)
		{
			if( pStartTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonFile ) ) )
			{
				INT nIndex, nCount = RibbonTabPageButton_GetCount();
				for( nIndex = 0; nIndex < nCount; nIndex ++ )
				{
					CExtRibbonButtonTabPage * pTBB = RibbonTabPageButton_GetAt( nIndex );
					ASSERT_VALID( pTBB );
					if( pTBB->OnQueryFlatTrackingEnabled() )
					{
						pTBB->OnDeliverCmd();
						nStartSearchIndex = _GetIndexOf( pTBB );
						return nStartSearchIndex;
					}
				} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
			} // if( pStartTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonFile ) ) )
			INT nIndex, nCount = RibbonTabPageButton_GetCount();
			for( nIndex = 0; nIndex < nCount; nIndex ++ )
			{
				CExtRibbonButtonTabPage * pTBB = RibbonTabPageButton_GetAt( nIndex );
				ASSERT_VALID( pTBB );
				if( pTBB == pTabTBB )
					continue;
				if( pTBB->IsSelectedRibbonPage() )
				{
					//pTBB->OnDeliverCmd();
					nStartSearchIndex = _GetIndexOf( pTBB );
					return nStartSearchIndex;
				}
			} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
		}
		//nRightIndex = RibbonRightButton_GetIndexOf( pNewCalcSelTBB );
		//if( pStartTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonTabPage ) ) )
		//	nTabPageIndex = RibbonTabPageButton_GetIndexOf( (CExtRibbonButtonTabPage*)pNewCalcSelTBB );
		nQuickAccessIndex = RibbonQuickAccessButton_GetIndexOf( pNewCalcSelTBB );
		if(		(	nQuickAccessIndex >= 0
				&&	RibbonQuickAccessBar_AboveTheRibbonGet()
				)
			||	pNewCalcSelTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonFile ) )
			)
		{
			INT nIndex, nCount = RibbonTabPageButton_GetCount();
			for( nIndex = 0; nIndex < nCount; nIndex ++ )
			{
				CExtRibbonButtonTabPage * pTBB = RibbonTabPageButton_GetAt( nIndex );
				ASSERT_VALID( pTBB );
				if( pTBB->IsSelectedRibbonPage() )
				{
					//pTBB->OnDeliverCmd();
					nStartSearchIndex = _GetIndexOf( pTBB );
					return nStartSearchIndex;
				}
			} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
		} // if( nQuickAccessIndex >= 0 && RibbonQuickAccessBar_AboveTheRibbonGet() )
	} // pStartTBB != NULL ...
	if( nFlatTrackingIndex >= 0 )
	{
		CExtBarButton * pTBB = GetButton( nFlatTrackingIndex );
		ASSERT_VALID( pTBB );
		if( pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonTabPage ) ) )
		{
			pTBB->OnDeliverCmd();
			nFlatTrackingIndex = _GetIndexOf( pTBB );
		} // if( pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonTabPage ) ) )
		else if( nQuickAccessIndex < 0 && nRightIndex < 0 && nTabPageIndex < 0 )
		{
			if( vkTCHAR != VK_TAB )
			{
				nRightIndex = RibbonRightButton_GetIndexOf( pTBB );
				if( nRightIndex >= 0 )
					return nStartSearchIndex;
			} // if( vkTCHAR != VK_TAB )
		} // else if( nQuickAccessIndex < 0 && nRightIndex < 0 && nTabPageIndex < 0 )
	} // if( nFlatTrackingIndex >= 0 )
	return nFlatTrackingIndex;
}

bool CExtRibbonBar::OnCalcFlatTrackingIndexCheckPass(
	/*__EXT_MFC_SAFE_TCHAR*/ INT vkTCHAR,
	INT nPassIndex,
	const CExtBarButton * pPrevTBB,
	const CExtBarButton * pNextTBB
	) const
{
	ASSERT_VALID( this );
	if(		pPrevTBB != NULL
		&&	pNextTBB != NULL
		&&	( vkTCHAR == VK_LEFT || vkTCHAR == VK_RIGHT )
		&&	pPrevTBB->ParentButtonGet() != NULL
		&&	pNextTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonTabPage ) ) //ParentButtonGet() == NULL
		)
		return false;
	return
		CExtRibbonPage::OnCalcFlatTrackingIndexCheckPass(
			vkTCHAR,
			nPassIndex,
			pPrevTBB,
			pNextTBB
			);
}

bool CExtRibbonBar::OnCalcFlatTrackingTabOrder(
	CTypedPtrArray < CPtrArray, CExtBarButton * > & arrTabOrder
	)
{
	ASSERT_VALID( this );
	arrTabOrder.RemoveAll();
INT nIndex, nCount;
CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
	if( pFileTBB != NULL )
		arrTabOrder.InsertAt( 0, pFileTBB, 1 );
bool bRibbonQuickAccessBarIsAboveTheRibbon = RibbonQuickAccessBar_AboveTheRibbonGet();
	if( bRibbonQuickAccessBarIsAboveTheRibbon )
	{
		nCount = RibbonQuickAccessButton_GetCount();
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtBarButton * pTBB = RibbonQuickAccessButton_GetAt( nIndex );
			ASSERT_VALID( pTBB );
			if( ! pTBB->OnQueryFlatTrackingEnabled() )
				continue;
			arrTabOrder.Add( pTBB );
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	} // if( bRibbonQuickAccessBarIsAboveTheRibbon )

	nCount = RibbonTabPageButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtRibbonButtonTabPage * pTBB = RibbonTabPageButton_GetAt( nIndex );
		ASSERT_VALID( pTBB );
		if( ! pTBB->OnQueryFlatTrackingEnabled() )
			continue;
		if( ! pTBB->IsSelectedRibbonPage() )
			continue;
		arrTabOrder.Add( pTBB );
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )

	nCount = RibbonRightButton_GetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtBarButton * pTBB = RibbonRightButton_GetAt( nIndex );
		ASSERT_VALID( pTBB );
		if( ! pTBB->OnQueryFlatTrackingEnabled() )
			continue;
		arrTabOrder.Add( pTBB );
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )

	nCount = GetButtonsCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtBarButton * pTBB = GetButton( nIndex );
		ASSERT_VALID( pTBB );
		if( ! pTBB->OnQueryFlatTrackingEnabled() )
			continue;
		if( pTBB == pFileTBB )
			continue;
		if(		pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonTabPage ) )
			&&	RibbonTabPageButton_GetIndexOf( (CExtRibbonButtonTabPage*)pTBB ) >= 0
			)
			continue;
		if( RibbonRightButton_GetIndexOf( pTBB ) >= 0 )
			continue;
		if( RibbonQuickAccessButton_GetIndexOf( pTBB ) >= 0 )
			continue;
		arrTabOrder.Add( pTBB );
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )

	if( ! bRibbonQuickAccessBarIsAboveTheRibbon )
	{
		nCount = RibbonQuickAccessButton_GetCount();
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtBarButton * pTBB = RibbonQuickAccessButton_GetAt( nIndex );
			ASSERT_VALID( pTBB );
			if( ! pTBB->OnQueryFlatTrackingEnabled() )
				continue;
			arrTabOrder.Add( pTBB );
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	} // if( ! bRibbonQuickAccessBarIsAboveTheRibbon )

	return true;
}

bool CExtRibbonBar::OnCalcFlatTrackingIndexCheckIntersectionEnabled(
	CExtBarButton * pTBB,
	/*__EXT_MFC_SAFE_TCHAR*/ INT vkTCHAR
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
	return CExtRibbonPage::OnCalcFlatTrackingIndexCheckIntersectionEnabled( pTBB, vkTCHAR );
}

BOOL CExtRibbonBar::TranslateMainFrameMessage(MSG* pMsg)
{
	__PROF_UIS_MANAGE_STATE;
	if( GetSafeHwnd() == NULL )
		return  FALSE;
	ASSERT( pMsg != NULL );
	if( pMsg->message == __ExtMfc_WM_THEMECHANGED && m_pExtNcFrameImpl != NULL )
		m_pExtNcFrameImpl->m_BridgeNC._AdjustThemeSettings();
#if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	if( BackstageView_PreTranslateMessage( pMsg ) )
		return TRUE;
#endif // (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	return CExtRibbonPage::TranslateMainFrameMessage( pMsg );
}

CExtRibbonNodeFile * CExtRibbonBar::Ribbon_GetFileRootNode()
{
	ASSERT_VALID( this );
CExtRibbonNode * pRibbonNodeRoot = Ribbon_GetRootNode();
	if( pRibbonNodeRoot == NULL )
		return NULL;
	ASSERT_VALID( pRibbonNodeRoot );
INT nIndex, nCount = pRibbonNodeRoot->GetNodeCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtCustomizeCmdTreeNode * pNode = pRibbonNodeRoot->ElementAt( nIndex );
		ASSERT_VALID( pNode );
		CExtRibbonNodeFile * pRetValNode =
			DYNAMIC_DOWNCAST( CExtRibbonNodeFile, pNode );
		if( pRetValNode != NULL )
			return pRetValNode;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return NULL;
}

const CExtRibbonNodeFile * CExtRibbonBar::Ribbon_GetFileRootNode() const
{
	ASSERT_VALID( this );
	return ( const_cast < CExtRibbonBar * > ( this ) ) -> Ribbon_GetFileRootNode();
}

CExtRibbonNodeQuickAccessButtonsCollection * CExtRibbonBar::Ribbon_GetQuickAccessRootNode()
{
	ASSERT_VALID( this );
CExtRibbonNode * pRibbonNodeRoot = Ribbon_GetRootNode();
	if( pRibbonNodeRoot == NULL )
		return NULL;
	ASSERT_VALID( pRibbonNodeRoot );
INT nIndex, nCount = pRibbonNodeRoot->GetNodeCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtCustomizeCmdTreeNode * pNode = pRibbonNodeRoot->ElementAt( nIndex );
		ASSERT_VALID( pNode );
		CExtRibbonNodeQuickAccessButtonsCollection * pRetValNode =
			DYNAMIC_DOWNCAST( CExtRibbonNodeQuickAccessButtonsCollection, pNode );
		if( pRetValNode != NULL )
			return pRetValNode;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return NULL;
}

const CExtRibbonNodeQuickAccessButtonsCollection * CExtRibbonBar::Ribbon_GetQuickAccessRootNode() const
{
	ASSERT_VALID( this );
	return ( const_cast < CExtRibbonBar * > ( this ) ) -> Ribbon_GetQuickAccessRootNode();
}

CExtRibbonNodeTabPageCollection * CExtRibbonBar::Ribbon_GetTabPageRootNode()
{
	ASSERT_VALID( this );
CExtRibbonNode * pRibbonNodeRoot = Ribbon_GetRootNode();
	if( pRibbonNodeRoot == NULL )
		return NULL;
	ASSERT_VALID( pRibbonNodeRoot );
INT nIndex, nCount = pRibbonNodeRoot->GetNodeCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtCustomizeCmdTreeNode * pNode = pRibbonNodeRoot->ElementAt( nIndex );
		ASSERT_VALID( pNode );
		CExtRibbonNodeTabPageCollection * pRetValNode =
			DYNAMIC_DOWNCAST( CExtRibbonNodeTabPageCollection, pNode );
		if( pRetValNode != NULL )
			return pRetValNode;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return NULL;
}

const CExtRibbonNodeTabPageCollection * CExtRibbonBar::Ribbon_GetTabPageRootNode() const
{
	ASSERT_VALID( this );
	return ( const_cast < CExtRibbonBar * > ( this ) ) -> Ribbon_GetTabPageRootNode();
}

CExtRibbonNodeRightButtonsCollection * CExtRibbonBar::Ribbon_GetRightRootNode()
{
	ASSERT_VALID( this );
CExtRibbonNode * pRibbonNodeRoot = Ribbon_GetRootNode();
	if( pRibbonNodeRoot == NULL )
		return NULL;
	ASSERT_VALID( pRibbonNodeRoot );
INT nIndex, nCount = pRibbonNodeRoot->GetNodeCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtCustomizeCmdTreeNode * pNode = pRibbonNodeRoot->ElementAt( nIndex );
		ASSERT_VALID( pNode );
		CExtRibbonNodeRightButtonsCollection * pRetValNode =
			DYNAMIC_DOWNCAST( CExtRibbonNodeRightButtonsCollection, pNode );
		if( pRetValNode != NULL )
			return pRetValNode;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return NULL;
}

const CExtRibbonNodeRightButtonsCollection * CExtRibbonBar::Ribbon_GetRightRootNode() const
{
	ASSERT_VALID( this );
	return ( const_cast < CExtRibbonBar * > ( this ) ) -> Ribbon_GetRightRootNode();
}

bool CExtRibbonBar::Ribbon_OnPageSelectionChanging(
	INT nOldSelIdx,
	INT nNewSelIdx,
	bool & bEnableAnimation
	)
{
	ASSERT_VALID( this );
	nOldSelIdx;
	nNewSelIdx;
	bEnableAnimation;
const CExtRibbonNodeTabPageCollection * pColNode = Ribbon_GetTabPageRootNode();
	if( pColNode == NULL )
		return false;
	return true;
}

void CExtRibbonBar::Ribbon_OnPageSelectionChanged(
	INT nOldSelIdx,
	INT nNewSelIdx
	)
{
	ASSERT_VALID( this );
	nOldSelIdx;
	nNewSelIdx;
}

INT CExtRibbonBar::Ribbon_PageSelectionGet() const
{
	ASSERT_VALID( this );
const CExtRibbonNodeTabPageCollection * pColNode = Ribbon_GetTabPageRootNode();
	if( pColNode == NULL )
		return -1;
INT nPageSelIdx =
		pColNode->PageSelectionGet();
	return nPageSelIdx;
}

bool CExtRibbonBar::Ribbon_PageSelectionSet(
	INT nNewSelIdx,
	bool bEnableAnimation // = true
	)
{
	ASSERT_VALID( this );
CExtRibbonNodeTabPageCollection * pColNode = Ribbon_GetTabPageRootNode();
	if( pColNode == NULL )
		return false;
INT nPageIndex, nPageCount = pColNode->GetNodeCount();
	ASSERT( nPageCount == RibbonTabPageButton_GetCount() );
	if( nPageCount == 0 )
		return false;
#if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
	BackstageView_EnsureHidden();
#endif // (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)
INT nOldSelIdx = pColNode->PageSelectionGet();
	if(		nOldSelIdx == nNewSelIdx
		||	( nOldSelIdx < 0 && nNewSelIdx < 0 )
		)
		return true;
	if( nOldSelIdx >= nPageCount || nNewSelIdx >= nPageCount )
		return false;
	CExtToolControlBar::_CloseCustomMenusAll();
	CExtToolControlBar::_CloseTrackingMenus();
	CExtPopupMenuWnd::CancelMenuTracking();
	_RibbonPageRslaResetStateData();
CExtBarButton * pFlatTrackingTBB = NULL;
CExtRibbonButtonTabPage * pOldSelTBB = NULL;
CExtRibbonButtonTabPage * pNewSelTBB = NULL;
	if( m_bFlatTracking && m_nFlatTrackingIndex >= 0 )
	{
		pFlatTrackingTBB = GetButton( m_nFlatTrackingIndex );
		ASSERT_VALID( pFlatTrackingTBB );
	}
	if( nOldSelIdx >= 0 )
	{
		pOldSelTBB = RibbonTabPageButton_GetAt( nOldSelIdx );
		ASSERT_VALID( pOldSelTBB );
	}
	if( nNewSelIdx >= 0 )
	{
		pNewSelTBB = RibbonTabPageButton_GetAt( nNewSelIdx );
		ASSERT_VALID( pNewSelTBB );
	}

	if( ! Ribbon_OnPageSelectionChanging(
			nOldSelIdx,
			nNewSelIdx,
			bEnableAnimation
			)
		)
		return false;

	_UpdateCtrlLock();

bool bAutoHideModeEnabled = Ribbon_AutoHideModeEnabledGet();
	if( ! bAutoHideModeEnabled )
	{
		if( ! RibbonPage_ExpandedModeGet() )
			RibbonPage_ExpandedModeSet( true, true );
	} // if( ! bAutoHideModeEnabled )

	for( nPageIndex = 0; nPageIndex < nPageCount; nPageIndex ++ )
	{
		if( nPageIndex == nNewSelIdx )
			continue;
		CExtRibbonButtonTabPage * pTabPageTBB = RibbonTabPageButton_GetAt( nPageIndex );
		ASSERT( pTabPageTBB );
		pTabPageTBB->SetHover( false );
		pTabPageTBB->ModifyStyle( 0, TBBS_PRESSED|TBBS_CHECKED );
	} // for( . . .

bool bFinalAnimationForOld = false;
	if( bEnableAnimation )
	{
		if( pOldSelTBB != NULL && pFlatTrackingTBB == NULL )
		{
			bFinalAnimationForOld = true;
			ASSERT_VALID( pOldSelTBB );
			pOldSelTBB->AnimationClient_CacheGeneratorLock();
//			pOldSelTBB->RedrawButton( true );
//			if( pOldSelTBB->AnimationClient_StateGet(true).IsEmpty() )
 				pOldSelTBB->AnimationClient_CacheNextStateMinInfo(
					false,
					__EAPT_BY_PRESSED_STATE_TURNED_OFF
					);
//			pOldSelTBB->AnimationClient_CacheGeneratorUnlock();
		} // if( pOldSelTBB != NULL && pFlatTrackingTBB == NULL )
		if( pNewSelTBB != NULL )
		{
			ASSERT_VALID( pNewSelTBB );
			pNewSelTBB->AnimationClient_CacheGeneratorLock();
//			pNewSelTBB->RedrawButton( true );
//			if( pNewSelTBB->AnimationClient_StateGet(true).IsEmpty() )
 				pNewSelTBB->AnimationClient_CacheNextStateMinInfo(
					false,
					__EAPT_BY_PRESSED_STATE_TURNED_ON
					);
//			pNewSelTBB->AnimationClient_CacheGeneratorUnlock();
		} // if( pNewSelTBB != NULL )
	} // if( bEnableAnimation )

	pColNode->PageSelectionSet( nNewSelIdx );

// 	if( bEnableAnimation )
// 	{
// 		if( pOldSelTBB != NULL && pFlatTrackingTBB != NULL )
// 		{
// 			ASSERT( ! bFinalAnimationForOld );
// 			ASSERT_VALID( pOldSelTBB );
// 			pOldSelTBB->SetHover( true );
// 			pOldSelTBB->SetHover( false );
// // 			pOldSelTBB->AnimationClient_CacheGeneratorLock();
// // //			pOldSelTBB->RedrawButton( true );
// // //			if( pOldSelTBB->AnimationClient_StateGet(true).IsEmpty() )
// //  				pOldSelTBB->AnimationClient_CacheNextStateMinInfo(
// // 					false,
// // 					__EAPT_BY_HOVERED_STATE_TURNED_OFF
// // 					);
// // //			pOldSelTBB->AnimationClient_CacheGeneratorUnlock();
// 		} // if( pOldSelTBB != NULL && pFlatTrackingTBB != NULL )
// 	}
	
INT nGroupCount = RibbonGroupButton_GetCount();
	for( ; nGroupCount > 0; nGroupCount = RibbonGroupButton_GetCount() )
	{
		CExtRibbonButtonGroup * pGroupTBB = RibbonGroupButton_GetAt( 0 );
		ASSERT_VALID( pGroupTBB );
		m_arrGroupButtons.RemoveAt( 0 );
		RemoveButton( _GetIndexOf( pGroupTBB ), FALSE );
	} // for( . . .

CExtRibbonNode * pRibbonNodeNewSel =
		STATIC_DOWNCAST( CExtRibbonNode, pColNode->ElementAt( nNewSelIdx ) );
	pRibbonNodeNewSel->Ribbon_InitCommandProfile( m_hWnd, true );
	pRibbonNodeNewSel->Ribbon_InitBar( this, NULL, false );
	Ribbon_OnCalcMinMaxILE();

	if( pFlatTrackingTBB != NULL )
	{
		m_nFlatTrackingIndex = _GetIndexOf( pFlatTrackingTBB ); // can return -1
		if( m_nFlatTrackingIndex < 0 )
		{
			if( m_bFlatTracking )
			{
				m_bFlatTracking = false;
				OnFlatTrackingStop();
			} // if( m_bFlatTracking )
			_UpdateFlatTracking( false );
		}
	}

	CWnd * pTarget = GetOwner();
	if( pTarget == NULL && (!m_bPresubclassDialogMode) )
	{
		CFrameWnd * pTargetFrame = GetParentFrame();
		if( pTargetFrame != NULL )
		{
			BOOL bDisableIfNoHandler = pTargetFrame -> m_bAutoMenuEnable;
			OnUpdateCmdUI( pTargetFrame, bDisableIfNoHandler );
		} // if( pTargetFrame != NULL )
	} // if( pTarget == NULL && (!m_bPresubclassDialogMode) )
	else
		DoCustomModeUpdateCmdUI();

	_UpdateCtrlUnlock();

	m_bRibbonSimplifiedLayoutDelayedFlush = true;
	_RecalcPositionsImpl();
	Invalidate();
	_UpdateHoverButton( CPoint(-1,-1), false );

	if( bEnableAnimation )
	{
		if( pOldSelTBB != NULL )
		{
			ASSERT_VALID( pOldSelTBB );
			if( bFinalAnimationForOld )
			{
//				pOldSelTBB->AnimationClient_CacheGeneratorLock();
				pOldSelTBB->AnimationClient_CacheNextStateMinInfo(
					true,
					__EAPT_BY_SELECTED_STATE_TURNED_OFF
					);
				pOldSelTBB->AnimationClient_CacheGeneratorUnlock();
			} // if( bFinalAnimationForOld )
			else
			{
				pOldSelTBB->SetHover( true );
				pOldSelTBB->SetHover( false );
			} // else from if( bFinalAnimationForOld )
		} // if( pOldSelTBB != NULL && bFinalAnimationForOld )
		if( pNewSelTBB != NULL )
		{
			ASSERT_VALID( pNewSelTBB );
//			pNewSelTBB->AnimationClient_CacheGeneratorLock();
			pNewSelTBB->AnimationClient_CacheNextStateMinInfo(
				true,
				__EAPT_BY_SELECTED_STATE_TURNED_ON
				);
			pNewSelTBB->AnimationClient_CacheGeneratorUnlock();
		} // if( pNewSelTBB != NULL )
	} // if( bEnableAnimation )

	Ribbon_OnPageSelectionChanged(
		nOldSelIdx,
		nNewSelIdx
		);

	return true;
}

void CExtRibbonBar::OnRibbonFileMenuButtonQuery(
	CExtPopupMenuWnd::RIBBONFILEMENUBUTTONQUERY & _rfmbq,
	CExtCmdIcon & _icon,
	CExtSafeString & sText
	)
{
	ASSERT_VALID( this );
	switch( _rfmbq.m_dwButtonType )
	{
	case TPMX_RIBBON_EXIT_BUTTON:
		if( _icon.IsEmpty() )
		{
			_icon.m_bmpNormal.LoadBMP_Resource( MAKEINTRESOURCE( ID_EXT_RIBBON_FILE_MENU_EXIT ) );
			if( ! _icon.m_bmpNormal.IsEmpty() )
			{
				if( _icon.m_bmpNormal.Make32() )
					_icon.m_bmpNormal.AlphaColor( RGB(255,0,255), RGB(0,0,0),  BYTE(0) );
			}
		}
		if( sText.IsEmpty() )
		{
			if( ! g_ResourceManager->LoadString( sText, ID_EXT_RIBBON_FILE_MENU_EXIT ) )
				sText = _T("Exit");
		}
	break;
	case TPMX_RIBBON_OPTIONS_BUTTON:
		if( _icon.IsEmpty() )
		{
			_icon.m_bmpNormal.LoadBMP_Resource( MAKEINTRESOURCE( ID_EXT_RIBBON_FILE_MENU_OPTIONS ) );
			if( ! _icon.m_bmpNormal.IsEmpty() )
			{
				if( _icon.m_bmpNormal.Make32() )
					_icon.m_bmpNormal.AlphaColor( RGB(255,0,255), RGB(0,0,0),  BYTE(0) );
			}
		}
		if( sText.IsEmpty() )
		{
			if( ! g_ResourceManager->LoadString( sText, ID_EXT_RIBBON_FILE_MENU_OPTIONS ) )
				sText = _T("Options");
		}
	break;
	} // switch( _rfmbq.m_dwButtonType )
CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
	if( pFileTBB != NULL )
	{
		if( pFileTBB->OnRibbonFileMenuButtonQuery(
				_rfmbq,
				_icon,
				sText
				)
			)
			return;
	}
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
	pPM->Ribbon_FileMenuButtonQuery(
		& _rfmbq,
		_icon.IsEmpty() ? NULL : (&_icon),
		sText.IsEmpty() ? NULL : LPCTSTR(sText)
		);
}

void CExtRibbonBar::OnRibbonFileMenuButtonInvocation(
	CExtPopupMenuWnd::RIBBONFILEMENUBUTTONINVOCATION & _rfmbi
	)
{
	ASSERT_VALID( this );
CExtRibbonButtonFile * pFileTBB = Ribbon_FileButtonGet();
	if(		pFileTBB != NULL
		&&	pFileTBB->OnRibbonFileMenuButtonInvocation( _rfmbi )
		)
		return;
	CExtRibbonPage::OnRibbonFileMenuButtonInvocation( _rfmbi );
}

void CExtRibbonBar::OnRibbonOptionsDialogTrack(
	UINT nSelectedPageID // = __EXT_RIBBON_OPTIONS_DIALOG_PAGE_DEFAULT
	)
{
	ASSERT_VALID( this );
CExtRibbonOptionsDialog dlgRibbonOptionsDialog( nSelectedPageID, this, this );
	dlgRibbonOptionsDialog.DoModal();
}

void CExtRibbonBar::OnRibbonOptionsDialogInitPageRTCs(
	CExtRibbonOptionsDialog & dlgRibbonOptions
	)
{
	ASSERT_VALID( this );
	dlgRibbonOptions.m_arrPageRTCs.Add(
		RUNTIME_CLASS( CExtRibbonOptionsPageCustomizeQATB )
		);
}

bool CExtRibbonBar::CustomizeStateSerialize(
	CArchive & ar,
	bool bEnableThrowExceptions // = false
	)
{
	try
	{
		if( ! CExtRibbonPage::CustomizeStateSerialize(
				ar,
				bEnableThrowExceptions
				)
			)
			return false;
		DWORD dwInitialCountInQATB = 0;
		CList < DWORD, DWORD > _listSerializationCode;
		CTypedPtrList < CPtrList, CExtRibbonNode * > _listCustomButtons;
		POSITION pos;
		INT nQatbIndex, nQatbCount;
		if( ar.IsStoring() )
		{
			nQatbCount = RibbonQuickAccessButton_GetCount();
			for( nQatbIndex = 0; nQatbIndex < nQatbCount; nQatbIndex ++ )
			{
				CExtBarButton * pTBB = RibbonQuickAccessButton_GetAt( nQatbIndex );
				ASSERT_VALID( pTBB );
				if( pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonQuickAccessContentExpand) ) )
					continue;
				CExtCustomizeCmdTreeNode * pNode = pTBB->GetCmdNode( true );
				ASSERT_VALID( pNode );
				CExtRibbonNode * pRibbonNode = DYNAMIC_DOWNCAST( CExtRibbonNode, pNode );
				if( pRibbonNode == NULL )
					continue;
				DWORD dwCmdID = DWORD( pTBB->GetCmdID( false ) );
				_listSerializationCode.AddTail( dwCmdID );
				DWORD dwCmdFlags = 0;
				if( (pNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) == 0 )
				{
					dwInitialCountInQATB ++;
					dwCmdFlags |= 0x00000001;
					if( (pTBB->GetStyle()&TBBS_HIDDEN) != 0 )
						dwCmdFlags |= 0x00000002;
					_listSerializationCode.AddTail( dwCmdFlags );
					continue;
				} // if( (pNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) == 0 )
				_listSerializationCode.AddTail( dwCmdFlags );
				_listCustomButtons.AddTail( pRibbonNode );
			} // for( nQatbIndex = 0; nQatbIndex < nQatbCount; nQatbIndex ++ )
			ar << dwInitialCountInQATB;
			_listSerializationCode.Serialize( ar );
			ar << DWORD( _listCustomButtons.GetCount() );
			for( pos = _listCustomButtons.GetHeadPosition(); pos != NULL; )
			{
				CExtRibbonNode * pRibbonNode = _listCustomButtons.GetNext( pos );
				ASSERT_VALID( pRibbonNode );
				ASSERT_KINDOF( CExtRibbonNode, pRibbonNode );
				CRuntimeClass * pRTC = pRibbonNode->GetRuntimeClass();
				ASSERT( pRTC != NULL );
				ar.WriteClass( pRTC );
				pRibbonNode->Serialize( ar );
			} // for( pos = _listCustomButtons.GetHeadPosition(); pos != NULL; )
		} // if( ar.IsStoring() )
		else
		{
			_RibbonPageRslaResetStateData();
			ar >> dwInitialCountInQATB;
			_listSerializationCode.Serialize( ar );
			DWORD dwCustomButtonIndex, dwCustomButtonCount = 0;
			ar >> dwCustomButtonCount;
			try
			{
				for( dwCustomButtonIndex = 0; dwCustomButtonIndex < dwCustomButtonCount; dwCustomButtonIndex ++ )
				{
					CRuntimeClass * pRTC = ar.ReadClass();
					ASSERT( pRTC != NULL );
					CExtRibbonNode * pRibbonNode = (CExtRibbonNode*)pRTC->CreateObject();
					ASSERT_VALID( pRibbonNode );
					ASSERT_KINDOF( CExtRibbonNode, pRibbonNode );
					pRibbonNode->Serialize( ar );
					ASSERT_VALID( pRibbonNode );
					_listCustomButtons.AddTail( pRibbonNode );
				} // for( dwCustomButtonIndex = 0; dwCustomButtonIndex < dwCustomButtonCount; dwCustomButtonIndex ++ )
				// pre-reset QATB
				nQatbCount = RibbonQuickAccessButton_GetCount();
				for( nQatbIndex = 0; nQatbIndex < nQatbCount; )
				{
					CExtBarButton * pTBB = RibbonQuickAccessButton_GetAt( nQatbIndex );
					ASSERT_VALID( pTBB );
					if( pTBB->IsKindOf( RUNTIME_CLASS( CExtRibbonButtonQuickAccessContentExpand ) ) )
					{
						nQatbIndex ++;
						continue;
					}
					CExtCustomizeCmdTreeNode * pNode = pTBB->GetCmdNode();
					if( pNode == NULL )
					{
						nQatbIndex ++;
						continue;
					}
					ASSERT_VALID( pNode );
					if( (pNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
					{
						INT nPlainQatbIndex = _GetIndexOf( pTBB );
						ASSERT( nPlainQatbIndex >= 0 );
						m_arrQuickAccessButtons.RemoveAt( nQatbIndex );
						CExtToolControlBar::RemoveButton( nPlainQatbIndex );
						pNode->RemoveSelf( NULL );
						nQatbCount --;
						continue;
					} // if( (pNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
					else
					{
						pTBB->ModifyStyle( 0, TBBS_HIDDEN );
						nQatbIndex ++;
						continue;
					} // else from if( (pNode->GetFlags()&__ECTN_RIBBON_QA_CLONED_COPY) != 0 )
				} // for( nQatbIndex = 0; nQatbIndex < nQatbCount; )
				// apply custom buttons and existing button styles
				nQatbIndex = 0;
				for( pos = _listSerializationCode.GetHeadPosition(); pos != NULL; )
				{
					DWORD dwCmdID = _listSerializationCode.GetNext( pos );
					DWORD dwCmdFlags = _listSerializationCode.GetNext( pos );
					if( (dwCmdFlags&0x00000001) != 0 )
					{
						if( nQatbIndex >= nQatbCount )
							continue;
						CExtBarButton * pTBB = RibbonQuickAccessButton_GetAt( nQatbIndex );
						ASSERT_VALID( pTBB );
						if( ! pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonQuickAccessContentExpand) ) )
						{
							UINT nTbbCmdID = pTBB->GetCmdID( false );
							if( nTbbCmdID == UINT(dwCmdID) )
							{
								if( (dwCmdFlags&0x00000002) != 0 )
									pTBB->ModifyStyle( TBBS_HIDDEN );
							} // if( nTbbCmdID == UINT(dwCmdID) )
						} // if( ! pTBB->IsKindOf( RUNTIME_CLASS(CExtRibbonButtonQuickAccessContentExpand) ) )
						nQatbIndex ++;
						continue;
					} // if( (dwCmdFlags&0x00000001) != 0 )
					CExtRibbonNode * pRibbonNode = _listCustomButtons.RemoveHead();
					ASSERT_VALID( pRibbonNode );
					CExtBarButton * pButton = pRibbonNode->OnRibbonCreateBarButton( this, NULL );
					CArray < DWORD, DWORD > arrILEtoILV;
					arrILEtoILV.Add(
						__EXT_RIBBON_MAKE_RULE_ARRAY_ENTRY(
							__EXT_RIBBON_ILE_MAX,
							__EXT_RIBBON_ILV_SIMPLE_SMALL,
							false
							)
						);
					pRibbonNode->RibbonILE_RuleArraySet( arrILEtoILV );
					pRibbonNode->ModifyFlags( __ECTN_RIBBON_QA_CLONED_COPY );
					Ribbon_GetQuickAccessRootNode()->InsertNode( NULL, pRibbonNode );
					pButton->SetBasicCmdNode( pRibbonNode );
					pButton->SetCustomizedCmdNode( pRibbonNode );
					CExtToolControlBar::InsertSpecButton( -1, pButton, FALSE );
					if( m_pQACEB == NULL )
					{
						m_arrQuickAccessButtons.Add( pButton );
						m_pQACEB = new CExtRibbonButtonQuickAccessContentExpand( this );
						InsertSpecButton( -1, m_pQACEB, FALSE );
						m_arrQuickAccessButtons.Add( m_pQACEB );
					}
					else
					{
						ASSERT( INT(m_arrQuickAccessButtons.GetSize()) > 0 );
						m_arrQuickAccessButtons.InsertAt( INT(m_arrQuickAccessButtons.GetSize()) - 1, pButton, 1 );
					}
				} // for( pos = _listSerializationCode.GetHeadPosition(); pos != NULL; )
				// free rest of buttons left due to customer's development process
				for( pos = _listCustomButtons.GetHeadPosition(); pos != NULL; )
				{
					CExtRibbonNode * pRibbonNode = _listCustomButtons.GetNext( pos );
					ASSERT_VALID( pRibbonNode );
					ASSERT_KINDOF( CExtRibbonNode, pRibbonNode );
					delete pRibbonNode;
				} // for( pos = _listCustomButtons.GetHeadPosition(); pos != NULL; )
			} // try
			catch( ... )
			{
				for( pos = _listCustomButtons.GetHeadPosition(); pos != NULL; )
				{
					CExtRibbonNode * pRibbonNode = _listCustomButtons.GetNext( pos );
					ASSERT_VALID( pRibbonNode );
					ASSERT_KINDOF( CExtRibbonNode, pRibbonNode );
					delete pRibbonNode;
				} // for( pos = _listCustomButtons.GetHeadPosition(); pos != NULL; )
				_listCustomButtons.RemoveAll();
				throw;
			} // catch( ... )
			ASSERT( _listCustomButtons.GetCount() == 0 );
		} // else from if( ar.IsStoring() )
		return true;
	} // try
	catch( CException * pXept )
	{
		if( bEnableThrowExceptions )
			throw;
		pXept->Delete();
		// ASSERT( FALSE );
	} // catch( CException * pXept )
	catch( ... )
	{
		if( bEnableThrowExceptions )
			throw;
		// ASSERT( FALSE );
	} // catch( ... )
	return false;
}

bool CExtRibbonBar::RibbonLayout_IsDwmCaptionIntegration() const
{
	ASSERT_VALID( this );
	if(		m_pExtNcFrameImpl != NULL
		&&	m_pExtNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement()
		&&	GetSafeHwnd() != NULL
		&&	IsVisible()
		)
		return true;
	else
		return false;
}

#if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)

/////////////////////////////////////////////////////////////////////////////
// CExtRibbonBackstageViewWnd window

IMPLEMENT_DYNCREATE( CExtRibbonBackstageViewWnd, CExtRichGenWnd );

CExtRibbonBackstageViewWnd::CExtRibbonBackstageViewWnd()
	: m_pRibbonBar( NULL )
	, m_clockLastMenuEscapement( 0 )
{
#if (!defined __EXT_MFC_NO_CUSTOMIZE)
	m_bHelperKeyTipsSupported = true;
#endif // from (!defined __EXT_MFC_NO_CUSTOMIZE)
}

CExtRibbonBackstageViewWnd::~CExtRibbonBackstageViewWnd()
{
	OnRcgtdHandleShutdown();
}

BEGIN_MESSAGE_MAP( CExtRibbonBackstageViewWnd, CExtRichGenWnd )
	//{{AFX_MSG_MAP(CExtRibbonBackstageViewWnd)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE( CExtPopupBaseWnd::g_nMsgNotifyMenuClosed, OnMenuClosed )
END_MESSAGE_MAP()

LRESULT CExtRibbonBackstageViewWnd::OnMenuClosed( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
UINT nCmdID = UINT(wParam);
CExtPopupMenuWnd * pPopup = (CExtPopupMenuWnd *)lParam;
	ASSERT_VALID( pPopup );
	if( nCmdID == 0 && pPopup->m_hWndNotifyMenuClosed == m_hWnd )
		m_clockLastMenuEscapement = ::clock();
	return 0L;
}

CExtRibbonBar * CExtRibbonBackstageViewWnd::BsvGetRibbonBar()
	{ ASSERT_VALID( this ); return m_pRibbonBar; }

CWnd * CExtRibbonBackstageViewWnd::BsvGetFrameWnd()
{
	ASSERT_VALID( this );
CExtRibbonBar * pRibbonBar = BsvGetRibbonBar();
	if( pRibbonBar->GetSafeHwnd() == NULL )
		return NULL;
CWnd * pFrameWnd = pRibbonBar->GetParent();
	return pFrameWnd;
}

bool CExtRibbonBackstageViewWnd::BsvIsVisible() const
	{ ASSERT_VALID( this ); return ( GetSafeHwnd() != NULL && BsvGetRibbonBar() != NULL && ( GetStyle() & WS_VISIBLE ) != 0 ) ? true : false; }

bool CExtRibbonBackstageViewWnd::BsvShow(
	bool bShow,
	CExtRibbonBar * pRibbonBar // = NULL
	)
{
	ASSERT_VALID( this );
CWnd * pFrameWnd = BsvGetFrameWnd();
	if( m_pRibbonBar->GetSafeHwnd() == NULL && pRibbonBar != NULL )
		m_pRibbonBar = pRibbonBar;
bool bBsvIsVisible = BsvIsVisible();
	if( ( bBsvIsVisible && bShow ) || ( (!bBsvIsVisible) && (!bShow) ) )
		return true;
	if( pFrameWnd->GetSafeHwnd() == NULL )
		pFrameWnd = BsvGetFrameWnd();
	if( ! bShow )
	{
		if( GetSafeHwnd() != NULL )
		{
			SendMessage( WM_CANCELMODE );
			if( RcgtdKeyTipsDisplayedGet() )
				OnRcgtdKeyTipTrackingStop();
			if( ( GetStyle() & WS_VISIBLE ) != 0 )
			{
				ShowWindow( SW_HIDE );
				//DestroyWindow();
			}
			m_pRibbonBar->Invalidate();
			if( pFrameWnd->GetSafeHwnd() != NULL && pFrameWnd->IsWindowEnabled() && ( pFrameWnd->GetStyle() & WS_VISIBLE ) != 0 )
				pFrameWnd->SetFocus();
		} // if( GetSafeHwnd() != NULL )
		return true;
	} // if( ! bShow )
	if( pFrameWnd->GetSafeHwnd() == NULL )
	{
		ASSERT( FALSE );
		return false;
	}
	SetOwner( pFrameWnd );
bool bFirstCreated = false;
CRect rcBsv = BsvCalcRect();
	if( GetSafeHwnd() == NULL )
	{
		m_helperHookedHWND = pFrameWnd->m_hWnd;
		if( ! Create(
				pFrameWnd,
				rcBsv,
				UINT( __EXT_MFC_IDC_STATIC ),
				__EXT_RCS_QUICK_RESTYLE_MODE | __EXT_RCS_EVENTS_ALL,
				0L,
				WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN
				)
			)
		{
			ASSERT( FALSE );
			return false;
		}
		OnBsvInitContent();
		bFirstCreated = true;
	} // if( GetSafeHwnd() == NULL )
	RcsDelayRecalcLayout();
	SetWindowPos( &wndTop, rcBsv.left, rcBsv.top, rcBsv.Width(), rcBsv.Height(), SWP_NOOWNERZORDER|SWP_NOACTIVATE );
	OnSwUpdateScrollBars();
	OnRcgwUpdateCmdUI( pFrameWnd );
	if( bFirstCreated )
		RcsDelayRecalcLayout();
	OnSwRecalcLayout( true );
	if( ( GetStyle() & WS_VISIBLE ) == 0 )
		ShowWindow( SW_SHOW );
	m_pRibbonBar->Invalidate();
	SetFocus();
	return true;
}

bool CExtRibbonBackstageViewWnd::OnBsvInitContent()
{
	ASSERT_VALID( this );
	return true;	
}

CRect CExtRibbonBackstageViewWnd::BsvCalcRect()
{
	ASSERT_VALID( this );
CRect rcBsv( 0, 0, 0, 0 );
CWnd * pFrameWnd = BsvGetFrameWnd();
	if( pFrameWnd->GetSafeHwnd() == NULL )
	{
		ASSERT( FALSE );
		return rcBsv;
	}
CExtRibbonBar * pRibbonBar = BsvGetRibbonBar();
	if( pRibbonBar->GetSafeHwnd() == NULL )
	{
		ASSERT( FALSE );
		return rcBsv;
	}
	pFrameWnd->GetClientRect( &rcBsv );

//INT nGroupContentHeight = pRibbonBar->RibbonLayout_GetGroupHeight( NULL );
//INT nGroupCatpionHeight = pRibbonBar->RibbonLayout_GroupCaptionGetHeight( NULL );
//INT nPageHeight = nGroupContentHeight + nGroupCatpionHeight;

INT nTabLineHeight = pRibbonBar->RibbonLayout_GetTabLineHeight();
INT nTopBorderHeight = 0;
INT nFrameCaptionHeight = pRibbonBar->RibbonLayout_GetFrameCaptionHeight( &nTopBorderHeight );
INT nHeightAtTheTop = nTabLineHeight + nFrameCaptionHeight;

//INT nBottomLineHeight = pRibbonBar->RibbonLayout_GetBottomLineHeight();
INT nHeight = /*nPageHeight +*/ nHeightAtTheTop /*+ nBottomLineHeight*/;

	if(		pRibbonBar->RibbonLayout_IsFrameIntegrationEnabled()
		&&	(! pRibbonBar->RibbonLayout_IsDwmCaptionIntegration() )
		)
		nHeight += nFrameCaptionHeight + nTopBorderHeight;

	rcBsv.top += nHeight;
	return rcBsv;
}

void CExtRibbonBackstageViewWnd::PreSubclassWindow()
{
	ASSERT_VALID( this );
	CExtRichGenWnd::PreSubclassWindow();
	_InitThemeRes();
}

void CExtRibbonBackstageViewWnd::_InitThemeRes()
{
	ASSERT_VALID( this );
CExtRichDocObjectBase & _OC = RcsLayoutGet()->GetObjectContainerLocal(); // CExtRichContentLayout::stat_GetObjectContainerGlobal();
CExtRichDocObjectImage * pObjPic = NULL;

	pObjPic = new CExtRichDocObjectImage( _T("bsv_top_container_background.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_BACKGROUND_2010_R1) ) );
	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_top_level_tab_item_hover.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_TOP_LEVEL_TAB_ITEM_HOVER_2010_R1) ) );
	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_top_level_tab_item_selected.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_TOP_LEVEL_TAB_ITEM_SELECTED_2010_R1) ) );
	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_top_level_tab_item_focused.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_TOP_LEVEL_TAB_ITEM_FOCUSED_2010_R1) ) );
	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_top_level_tab_item_disabled.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_TOP_LEVEL_TAB_ITEM_DISABLED_2010_R1) ) );
	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_top_level_command_button_hover.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_TOP_LEVEL_COMMAND_BUTTON_HOVER_2010_R1) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_top_level_tab_pane_background.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_TOP_LEVEL_LIGHT_PANE_BACKGROUND_2010_R1) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_tab_item_arrow_ltr.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_TAB_ITEM_ARROW_LTR_2010_R1) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_tab_item_arrow_rtl.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_TAB_ITEM_ARROW_RTL_2010_R1) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_separator_background.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_SEPARATOR_BACKGROUND) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_command_button_normal.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_COMMAND_BUTTON_NORMAL) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_command_button_hover.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_COMMAND_BUTTON_HOVER) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_command_button_pressed.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_COMMAND_BUTTON_PRESSED) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_command_button_disabled.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_COMMAND_BUTTON_DISABLED) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_command_button_flat_hover.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_COMMAND_BUTTON_FLAT_HOVER) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_command_button_flat_pressed.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_COMMAND_BUTTON_FLAT_PRESSED) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_command_button_flat_accent.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_COMMAND_BUTTON_FLAT_ACCENT) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_left_pane_dark.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_LEFT_PANE_DARK) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_middle_pane_light.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_MIDDLE_PANE_LIGHT) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_middle_pane_dark.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_MIDDLE_PANE_DARK) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_right_pane_dark.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_RIGHT_PANE_DARK) ) );
//	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_element_group_attention_low.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_ELEMENT_GROUP_ATTENTION_LOW) ) );
	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_element_group_attention_high.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_ELEMENT_GROUP_ATTENTION_HIGH) ) );
	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );

	pObjPic = new CExtRichDocObjectImage( _T("bsv_command_button_drop_down_arrow.bmp") );
	VERIFY( pObjPic->ImageGet().LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_BSV_COMMAND_BUTTON_DROP_DOWN_ARROW) ) );
	pObjPic->ImageGet().PreMultipliedRGBChannelsSet( true );
//	VERIFY( pObjPic->ImageGet().Make32() );
	VERIFY( _OC.ObjectAdd( pObjPic ) );
}

bool CExtRibbonBackstageViewWnd::OnRcgwGenerateContent( CExtSafeString & strContent, HTREEITEM htiGen, INT nGenContentType )
{
	ASSERT_VALID( this );
	ASSERT( htiGen != NULL );
	ASSERT( RcgtdItemIsPresent( htiGen ) );
ULONG nItemIndent = RcgtdItemIndentGet( htiGen );
HTREEITEM htiRoot = RcgtdItemGetRoot();
	ASSERT( htiRoot != NULL );
CExtRichGenItemData * pItemData = (CExtRichGenItemData *) RcgtdItemDataGet( htiGen );
	ASSERT( pItemData != NULL );
	switch( nGenContentType )
	{
	case __BSV_CONTENT_TYPE_CSS:
		if( htiGen == htiRoot )
		{
			ASSERT( nItemIndent == 0L );
			strContent =
				_T("<style>\r\n")
				_T("bsv_top_container                             {   display:block; width:100%; height:100%; background-image:url(bsv_top_container_background.bmp); ")
				                                                 _T(" }\r\n")
				_T("bsv_top_level_menu                            {   display:ui-dock; ui-dock:left; width:8.5em; }\r\n")
				_T("bsv_top_level_tab_pane, bsv_second_level_tab_pane ")
                                                             _T(" {   display:none; ui-dock:pre-fill; width:100%; ui-can-be-focus:no; ui-can-be-selected:yes; }\r\n")
				_T("bsv_top_level_tab_pane:selection, bsv_second_level_tab_pane:selection ")
				                                             _T(" {   display:ui-dock; } \r\n")
				_T("bsv_top_level_tab_pane                        {   ui-background-slice:15px 15px 15px 15px; ui-background-extend:0px 0px 0px 12px; ")
				                                                 _T(" padding:12px 12px 12px 12px; background-image:url(bsv_top_level_tab_pane_background.bmp); }\r\n")

				_T("bsv_top_level_command_button                  {   display:block; width: 95%; height:3.5em; padding:7px 0.5em 7px 1em; margin:5px; color:white; ui-background-slice:3px 3px 3px 3px; ")
				                                                 _T(" ui-can-be-hover:yes; ui-can-be-pressed:yes; ui-can-be-focus:yes; ui-can-be-selected:yes; ui-can-be-disabled:yes; ")
				                                                 _T(" ui-focus-ensure-visibility:yes; ui-list-navigation:list-y; } \r\n")
				_T("bsv_top_level_command_button:hover, bsv_top_level_command_button:pressed, bsv_top_level_command_button:selection, bsv_top_level_command_button:focus, bsv_top_level_command_button:focus:pressed ")
				                                             _T(" {   background-image:url(bsv_top_level_command_button_hover.bmp); } \r\n")
				_T("bsv_top_level_command_button:disabled ")
				                                             _T(" {   color:gray; } \r\n")

				_T("bsv_top_level_tab_item                        {   display:block; width:100%; height:3.5em; color:white; ui-background-slice:1px 1px 7px 1px; ")
				                                                 _T(" ui-can-be-hover:yes; ui-can-be-pressed:yes; ui-can-be-focus:yes; ui-can-be-selected:yes; ui-can-be-disabled:yes; ")
				                                                 _T(" ui-focus-ensure-visibility:yes; ui-list-navigation:list-y; ")
				                                                 _T(" ui-focus-on-make-selected:yes; ui-focus-on-make-unselected-sibling:yes; } \r\n")
				_T("bsv_top_level_tab_item:selection, bsv_top_level_tab_item:focus ")
				                                             _T(" {   background-image:url(bsv_top_level_tab_item_selected.bmp); ")
				                                                 _T(" ui-delayed-draw:pre-fixed-absolute; } \r\n")
				_T("bsv_top_level_tab_item:hover                  {   background-image:url(bsv_top_level_tab_item_hover.bmp); } \r\n")
				_T("bsv_top_level_tab_item:pressed, bsv_top_level_tab_item:focus:hover, bsv_top_level_tab_item:focus:pressed, ")
				_T("bsv_top_level_tab_item:selection:focus, bsv_top_level_tab_item:selection:hover ")
				                                             _T(" {   background-image:url(bsv_top_level_tab_item_focused.bmp); } \r\n")
				_T("bsv_top_level_tab_item:disabled ")
				                                             _T(" {   color:gray; ui-focus-on-make-selected:no; ui-focus-on-make-unselected-sibling:no;  } \r\n")
				_T("bsv_top_level_tab_item:disabled:focus, bsv_top_level_tab_item:disabled:pressed, bsv_top_level_tab_item:disabled:hover, bsv_top_level_tab_item:disabled:pressed:hover ")
				                                             _T(" {   color:gray; background-image:url(bsv_top_level_tab_item_disabled.bmp); } \r\n")

				_T("bsv_second_level_tab_item                     {   display:block; width:100%; height:2.8em; color:black; ui-background-slice:5px 5px 12px 5px; ")
				                                                 _T(" padding:0px; ui-background-extend:0px 12px 0px 0px; ")
				                                                 _T(" ui-can-be-hover:yes; ui-can-be-pressed:yes; ui-can-be-focus:yes; ui-can-be-selected:yes; ui-can-be-disabled:yes; ")
				                                                 _T(" ui-focus-ensure-visibility:yes; ui-list-navigation:list-y; ")
				                                                 _T(" ui-focus-on-make-selected:yes; ui-focus-on-make-unselected-sibling:yes; } \r\n")
				_T("bsv_second_level_tab_item:focus, bsv_second_level_tab_item:selection, bsv_second_level_tab_item:focus:selection ")
				                                             _T(" {   background-image:url(bsv_command_button_flat_accent.bmp); } \r\n")
				_T("bsv_second_level_tab_item:hover ")
				                                             _T(" {   background-image:url(bsv_command_button_flat_hover.bmp); } \r\n")
				_T("bsv_second_level_tab_item:pressed, bsv_second_level_tab_item:focus:pressed, ")
				                                             _T(" {   background-image:url(bsv_command_button_flat_pressed.bmp); } \r\n")
				_T("bsv_second_level_tab_item:disabled            {   color:grey; } \r\n")
				
				_T("bsv_top_level_tab_item_arrow, bsv_second_level_tab_item_arrow ")
				                                              _T("{   display:none; ui-dock:pre-fill; background-image:url(bsv_tab_item_arrow_ltr.bmp); ")
				                                                 _T(" background-repeat:no-repeat; background-attachment:fixed; background-position: center right; ")
				                                                 _T(" ui-can-be-focus:no; ui-can-be-selected:yes; ")
				                                                 _T(" ui-delayed-draw:post-fixed-absolute; } \r\n")
				_T("bsv_second_level_tab_item_arrow               {   position:relative; left:12px; top:0.3em; } \r\n")
				_T("bsv_top_level_tab_item_arrow:selection, bsv_second_level_tab_item_arrow:selection ")
				                                              _T("{   display:ui-dock; } \r\n")

				_T("bsv_top_level_tab_item_content_container      {   display:block; padding:10px 2em 16px 2em; ui-can-be-focus:no; } \r\n")
				_T("bsv_second_level_tab_item_content_container   {   display:block; padding:0.6em; ui-can-be-focus:no; } \r\n")

				_T("bsv_caption, bsv_caption_attention_low, bsv_caption_attention_high ")
				                                             _T(" {   display:block; font-weight:bold; font-size:110%; margin:1em 0em 0em 0em; background-color:transparent; } \r\n")
				_T("bsv_caption_attention_low                     {   color:orangered; } \r\n")
				_T("bsv_caption_attention_high                    {   color:rgb(201,13,13); } \r\n")
				_T("bsv_separator                                 {   display:inline-block; width:100%; height:20px; margin:10px 0px 8px 0px; background-image:url(bsv_separator_background.bmp); ")
				                                                 _T(" ui-background-draw-method:tile-horizontally; } \r\n")

				_T("bsv_left_pane_light                           {   display:ui-dock; ui-dock:left; width:50%;  padding-right:1em; }\r\n")
				_T("bsv_left_pane_dark                            {   display:ui-dock; ui-dock:left; width:50%;  padding-right:1em; ")
				                                                 _T(" background-image:url(bsv_left_pane_dark.bmp);  ui-background-slice:4px 16px 4px 4px; ")
				                                                 _T(" ui-background-extend:0px 0px 0px 12px; }\r\n")
				_T("bsv_middle_pane_light                         {   display:ui-dock; ui-dock:middle; width:50%;  padding-right:1em; ")
				                                                 _T(" background-image:url(bsv_middle_pane_light.bmp);  ui-background-slice:4px 4px 4px 16px; ")
				                                                 _T(" ui-background-extend:0px 0px 0px 12px; }\r\n")
				_T("bsv_middle_pane_dark                          {   display:ui-dock; ui-dock:middle; width:50%;  padding-right:1em; ")
				                                                 _T(" background-image:url(bsv_middle_pane_dark.bmp);  ui-background-slice:4px 4px 4px 16px; ")
				                                                 _T(" ui-background-extend:0px 0px 0px 12px; }\r\n")
				_T("bsv_right_pane_light                          {   display:ui-dock; ui-dock:fill; width:100%; padding-left:1em; }\r\n")
				_T("bsv_right_pane_dark                           {   display:ui-dock; ui-dock:fill; width:100%; padding-left:1em; ")
				                                                 _T(" background-image:url(bsv_right_pane_dark.bmp); ui-background-slice:4px 4px 4px 16px; }\r\n")

				_T("bsv_scroll_container_horz                     {   display:inline-block; width:100%; height:100%; overflow:scroll; ui-sbv:hidden; } \r\n")
				_T("bsv_scroll_container_vert                     {   display:inline-block; width:100%; height:100%; overflow:scroll; ui-sbh:hidden; } \r\n")

				_T("bsv_mru_file_item                             {   ui-background-slice:3px 3px 5px 3px; ")
				                                                 _T(" display:inline-block; width:100%; height:3.5em; padding:0.5em; margin:2px 2em 2px 2px; color:black; ")
				                                                 _T(" ui-can-be-hover:yes; ui-can-be-pressed:yes; ui-can-be-focus:yes; ui-can-be-selected:yes; ")
				                                                 _T(" ui-can-be-disabled:yes; ui-focus-ensure-visibility:yes; ui-list-navigation:list-y; } \r\n")
				_T("bsv_mru_file_item:hover, bsv_mru_file_item:focus ")
				                                             _T(" {   background-image:url(bsv_command_button_flat_hover.bmp); } \r\n")
				_T("bsv_mru_file_item:pressed, bsv_mru_file_item:focus:pressed ")
				                                             _T(" {   background-image:url(bsv_command_button_flat_pressed.bmp); } \r\n")
				_T("bsv_mru_file_item:disabled                    {   color:gray; } \r\n")

				_T("bsv_command_button_square, bsv_command_button_square_flat, bsv_command_button_wide, bsv_command_button_wide_flat ")
				                                             _T(" {   display:inline-block; ui-list-navigation:list-xy; background-color:transparent; ")
				                                                 _T(" ui-can-be-hover:yes; ui-can-be-pressed:yes; ui-can-be-focus:yes; ui-can-be-selected:yes; ")
				                                                 _T(" ui-can-be-disabled:yes; ui-focus-ensure-visibility:yes; } \r\n")
				_T("bsv_command_button_square, bsv_command_button_square_flat ")
				                                             _T(" {   ui-background-slice:3px 3px 5px 3px; ")
				                                                 _T(" max-width:7.0em; height:7.0em; padding:0.8em 0.2em 0.8em 0.2em; margin:0.0em; } ")
				_T("bsv_command_button_square                     {   min-width:7.0em; } ")

                _T("bsv_command_button_wide, bsv_command_button_wide_flat ")
				                                             _T(" {   ui-background-slice:5px 5px 12px 5px; padding:7px 1em 7px 0.5em; margin:5px; color:black; } ")

				_T("bsv_command_button_square, bsv_command_button_wide ")
				                                             _T(" {   background-image:url(bsv_command_button_normal.bmp); } ")
				_T("bsv_command_button_square:hover, bsv_command_button_square:focus, bsv_command_button_wide:hover, bsv_command_button_wide:focus ")
				                                             _T(" {   background-image:url(bsv_command_button_hover.bmp); } \r\n")
				_T("bsv_command_button_square:pressed, bsv_command_button_square:focus:pressed, bsv_command_button_wide:pressed, bsv_command_button_wide:focus:pressed ")
				                                             _T(" {   background-image:url(bsv_command_button_pressed.bmp); } \r\n")
				_T("bsv_command_button_square:disabled            {   background-image:url(bsv_command_button_disabled.bmp); } \r\n")
				_T("bsv_command_button_square_flat:hover, bsv_command_button_square_flat:focus, bsv_command_button_wide_flat:hover, bsv_command_button_wide_flat:focus ")
				                                             _T(" {   background-image:url(bsv_command_button_flat_hover.bmp); } \r\n")
				_T("bsv_command_button_square_flat:pressed, bsv_command_button_square_flat:focus:pressed, bsv_command_button_wide_flat:pressed, bsv_command_button_wide_flat:focus:pressed ")
				                                             _T(" {   background-image:url(bsv_command_button_flat_pressed.bmp); } \r\n")
				_T("bsv_command_button_square:disabled, bsv_command_button_square_flat:disabled, bsv_command_button_wide:disabled, bsv_command_button_wide_flat:disabled ")
				                                             _T(" {   color:grey; } \r\n")

				_T("bsv_command_button_square_caption_text, bsv_command_button_flat_caption_text, bsv_command_button_flat_description_text ")
				                                             _T(" {   ui-can-be-focus:not; display:inline; width:100%; background-color:transparent; color:black; } \r\n")
				_T("bsv_command_button_square_caption_text        {   text-align:center; } \r\n")
				_T("bsv_command_button_flat_caption_text, bsv_command_button_flat_description_text ")
				                                             _T(" {   text-align:left; } \r\n")
				_T("bsv_command_button_flat_caption_text          {   color:black; } \r\n")
				_T("bsv_command_button_flat_description_text      {   color:grey; } \r\n")
				_T("bsv_command_button_square_caption_text:disabled, bsv_command_button_flat_caption_text:disabled ")
				                                             _T(" {   color:grey; } \r\n")

				_T("bsv_element_group, bsv_element_group_attention_low, bsv_element_group_attention_high ")
				                                             _T(" {   display:inline-block; width:100%; margin:5px 5px 0px 5px; padding:22px 0.75em 0px 0.75em; } \r\n")
				_T("bsv_element_group                             {   padding-top:2px; padding-bottom:2px; }\r\n")
				_T("bsv_element_group_attention_low               {   background-image:url(bsv_element_group_attention_low.bmp);  ui-background-slice:12px 7px 0px 7px; }\r\n")
				_T("bsv_element_group_attention_high              {   background-image:url(bsv_element_group_attention_high.bmp); ui-background-slice:12px 7px 0px 7px; }\r\n")

				_T("img, td, tr, table, p, div, a, br, ")
				_T("bsv_command_button_square_caption_text, bsv_command_button_flat_caption_text, bsv_command_button_flat_description_text, ")
				_T("bsv_top_level_tab_item_content_container, bsv_second_level_tab_item_content_container, ")
				_T("bsv_caption, bsv_caption_attention_low, bsv_caption_attention_high, bsv_top_level_tab_item_arrow, bsv_second_level_tab_item_arrow, ")
				_T("bsv_scroll_container_horz, bsv_scroll_container_vert, ")
				_T("bsv_left_pane_light, bsv_left_pane_dark, bsv_middle_pane_light, bsv_middle_pane_dark, bsv_right_pane_light, bsv_right_pane_dark, ")
				_T("bsv_element_group, bsv_element_group_attention_low, bsv_element_group_attention_high, bsv_top_container, ")
				_T("bsv_top_level_menu, bsv_top_level_tab_pane, bsv_second_level_tab_pane ")
				                                             _T(" {   ui-can-be-focus:not; } \r\n")
 
				_T("td, table                                     {   border-width:0px; padding:0px; margin:0px; border-spacing:0px; background-color:transparent; } \r\n")
				_T("a                                             {   color:grey; text-decoration:underline; } \r\n")
				_T("a:hover                                       {   color:orangered; } \r\n")
				_T("<br>                                          {   display:block; height:3px; background-color:red; } \r\n")
				_T("<img>                                         {   min-height:0px; } \r\n")

				_T("\r\n")
				_T("</style>\r\n")
				;
			return true;
		} // if( htiGen == htiRoot )
	break; // case __BSV_CONTENT_TYPE_CSS
	case __BSV_CONTENT_TYPE_HTML:
		if( htiGen == htiRoot )
		{
			ASSERT( nItemIndent == 0L );
			CExtSafeString strInnerContent;
			CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_top_container( htiGen, pItemData, strContent, _T("bsv_top_container") );
			{ // block for bsv_top_level_menu
				CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_top_level_menu( htiGen, pItemData, strContent, _T("bsv_top_level_menu") );
				OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, nGenContentType );
				strContent += strInnerContent;
			} // block for bsv_top_level_menu
			strInnerContent.Empty();
			OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, __BSV_CONTENT_TYPE_HTML_PANES );
			strContent += strInnerContent;
		} // if( htiGen == htiRoot )
		else
		{
			ASSERT( nItemIndent >= 1L );
			if( nItemIndent == 1L )
			{
				switch( pItemData->m_nItemType )
				{
				case __BSVIT_TAB_ITEM:
				{
					CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_top_level_tab_item( htiGen, NULL, strContent, _T("bsv_top_level_tab_item") );
					strContent += _T("<bsv_top_level_tab_item_arrow/>");
					CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_top_level_tab_item_content_container( htiGen, pItemData, strContent, _T("bsv_top_level_tab_item_content_container") );
				}
				break; // case __BSVIT_TAB_ITEM
				case __BSVIT_COMMAND_BUTTON_TOP_LEVEL:
				{
					CExtRichGenItemData::ItemWriter_t _writer( htiGen, pItemData, strContent, _T("bsv_top_level_command_button"), false, false );
					_writer.WriteHeader();
					CExtSafeString strImageSrcAttr = pItemData->ParmGet( __RCGI_PARM_COMMAND_BUTTON_ICON_IMAGE );
					CExtSafeString strCommandButtonCaption = pItemData->ParmGet( __RCGI_PARM_COMMAND_BUTTON_CAPTION );
					if(		( ! strImageSrcAttr.IsEmpty() )
						||	( ! strCommandButtonCaption.IsEmpty() )
						)
					{
						_writer.Format( _T("<p style=\" vertical-align:middle; border:0px; margin:0px; padding:0px; \" >") );
						if( ! strImageSrcAttr.IsEmpty() )
							_writer.Format( _T("<img style=\" min-width:0px; \" src=\"%s\" > "), LPCTSTR(strImageSrcAttr) );
						if( ! strCommandButtonCaption.IsEmpty() )
						{
							_writer.Format( _T("%s"), LPCTSTR(strCommandButtonCaption) );
						}
						_writer.Format( _T("</p>") );
					}
					_writer.WriteFooter();
				}
				break; // case __BSVIT_COMMAND_BUTTON_TOP_LEVEL
				} // switch( pItemData->m_nItemType )
			} // if( nItemIndent == 1L )
			else
			{
				switch( pItemData->m_nItemType )
				{
				case __BSVIT_TAB_ITEM:
				{
					CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_second_level_tab_item( htiGen, NULL, strContent, _T("bsv_second_level_tab_item") );
					strContent += _T("<bsv_second_level_tab_item_arrow/>");
					CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_second_level_tab_item_content_container( htiGen, pItemData, strContent, _T("bsv_second_level_tab_item_content_container") );
				}
				break; // case __BSVIT_TAB_ITEM
				} // switch( pItemData->m_nItemType )
			} // else if( nItemIndent == 1L )
			bool bDropDown = false;
			switch( pItemData->m_nItemType )
			{
			case __BSVIT_SEPARATOR:
			{
				CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_separator( htiGen, pItemData, strContent, _T("bsv_separator") );
			}
			break;
			case __BSVIT_COMMAND_BUTTON_SQUARE_DD:
			case __BSVIT_COMMAND_BUTTON_SQUARE_FLAT_DD:
				bDropDown = true;
			case __BSVIT_COMMAND_BUTTON_SQUARE:
			case __BSVIT_COMMAND_BUTTON_SQUARE_FLAT:
				if( nItemIndent > 1L )
				{
					CExtRichGenItemData::ItemWriter_t _writer(
						htiGen, pItemData, strContent,
						( pItemData->m_nItemType == __BSVIT_COMMAND_BUTTON_SQUARE_FLAT ) ? _T("bsv_command_button_square_flat") : _T("bsv_command_button_square"),
						false, false
						);
					_writer.WriteHeader();
					CExtSafeString strImageSrcAttr = pItemData->ParmGet( __RCGI_PARM_COMMAND_BUTTON_ICON_IMAGE );
					CExtSafeString strCommandButtonCaption = pItemData->ParmGet( __RCGI_PARM_COMMAND_BUTTON_CAPTION );
					if(		( ( ! strCommandButtonCaption.IsEmpty() ) || bDropDown )
						||	( ! strCommandButtonCaption.IsEmpty() )
						)
					{
						if( ! strImageSrcAttr.IsEmpty() )
						{
							CExtSafeString strImageAreaTags = pItemData->ParmGet( __RCGI_PARM_IMAGE_AREA_TAG_PARAMETERS );
							_writer.Format(
								_T("<div style=\" width=100%%; height:60%%; text-align:center; border-bottom:0.5em; \" %s >")
								_T("<img style=\" min-width:0px; \" src=\"%s\" > </div>"),
								( ! strImageAreaTags.IsEmpty() ) ? LPCTSTR(strImageAreaTags) : _T(""),
								LPCTSTR(strImageSrcAttr)
								);
						}
						if( ( ! strCommandButtonCaption.IsEmpty() ) || bDropDown )
						{
							_writer.Format( _T("<p style=\" width:100%%; text-align:center; \" >") );
							_writer.Format(
								_T("<bsv_command_button_square_caption_text>%s%s</bsv_command_button_square_caption_text>"),
								( ! strCommandButtonCaption.IsEmpty() ) ? LPCTSTR(strCommandButtonCaption) : _T(""),
								bDropDown ? _T("<img src=\"bsv_command_button_drop_down_arrow.bmp\" style=\" min-width:0px; \" />") : _T("")
								);
							_writer.Format( _T("</p>") );
						}
					}
					_writer.WriteFooter();
				} // if( nItemIndent > 1L )
			break; // cases __BSVIT_COMMAND_BUTTON_SQUARE*** and __BSVIT_COMMAND_BUTTON_SQUARE_FLAT***
			case __BSVIT_COMMAND_BUTTON_WIDE_DD:
			case __BSVIT_COMMAND_BUTTON_WIDE_FLAT_DD:
				bDropDown = true;
			case __BSVIT_COMMAND_BUTTON_WIDE:
			case __BSVIT_COMMAND_BUTTON_WIDE_FLAT:
				if( nItemIndent > 1L )
				{
					CExtRichGenItemData::ItemWriter_t _writer(
						htiGen, pItemData, strContent,
						( pItemData->m_nItemType == __BSVIT_COMMAND_BUTTON_WIDE_FLAT ) ? _T("bsv_command_button_wide_flat") : _T("bsv_command_button_wide"),
						false, false
						);
					_writer.WriteHeader();
					CExtSafeString strImageSrcAttr = pItemData->ParmGet( __RCGI_PARM_COMMAND_BUTTON_ICON_IMAGE );
					CExtSafeString strCommandButtonCaption = pItemData->ParmGet( __RCGI_PARM_COMMAND_BUTTON_CAPTION );
					CExtSafeString strCommandButtonDescription = pItemData->ParmGet( __RCGI_PARM_FLAT_COMMAND_BUTTON_TEXT_AT_RIGHT_DESCRIPTION );
					if(		( ! strImageSrcAttr.IsEmpty() )
						||	( ! strCommandButtonCaption.IsEmpty() )
						||	( ! strCommandButtonDescription.IsEmpty() )
						)
					{
						_writer.Format( _T("<table style=\" width:100%%; padding:0px; margin:0px; border-width:0px; border-collapse:collapse; \" ><tr>") );
						if( ! strImageSrcAttr.IsEmpty() )
						{
							CExtSafeString strImageAreaTags = pItemData->ParmGet( __RCGI_PARM_IMAGE_AREA_TAG_PARAMETERS );
							_writer.Format(
								_T("<td style=\" width:64px; text-align:center; padding:0px; margin:0px; text-align:center; vertical-align:middle; \" %s >")
								_T("<img style=\" min-width:0px; \" src=\"%s\" > </td>"),
								( ! strImageAreaTags.IsEmpty() ) ? LPCTSTR(strImageAreaTags) : _T(""),
								LPCTSTR(strImageSrcAttr)
								);
						}
						if(		( ( ! strCommandButtonCaption.IsEmpty() ) || bDropDown )
							||	( ! strCommandButtonDescription.IsEmpty() )
							)
						{
							_writer.Format( _T("<td style=\" width:100%%; padding:0px; margin:0px; padding-left:1em; \" >") );
							if( ( ! strCommandButtonCaption.IsEmpty() ) || bDropDown )
								_writer.Format(
								_T("<p style=\" margin-bottom:0.5em; \" ><bsv_command_button_flat_caption_text>%s</bsv_command_button_flat_caption_text></p>"),
									( ! strCommandButtonCaption.IsEmpty() ) ? LPCTSTR(strCommandButtonCaption) : _T(""),
									bDropDown ? _T("<img src=\"bsv_command_button_drop_down_arrow.bmp\" style=\" min-width:0px; \" />") : _T("")
									);
							if( ! strCommandButtonDescription.IsEmpty() )
								_writer.Format( _T("<bsv_command_button_flat_description_text>%s</bsv_command_button_flat_description_text>"), LPCTSTR(strCommandButtonDescription) );
							_writer.Format( _T("</td>") );
						}
						_writer.Format( _T("</tr></table>") );
					}
					_writer.WriteFooter();
				} // if( nItemIndent > 1L )
			break; // cases __BSVIT_COMMAND_BUTTON_WIDE and __BSVIT_COMMAND_BUTTON_WIDE_FLAT
			case __BSVIT_DIRECT_CONTENT:
			{
				if( ! pItemData->m_strHtmlInner.IsEmpty() )
					strContent += LPCTSTR(pItemData->m_strHtmlInner);
			}
			break; // case __BSVIT_DIRECT_CONTENT
			case __BSVIT_CAPTION:
			{
				CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_caption( htiGen, pItemData, strContent, _T("bsv_caption") );
			}
			break; // case __BSVIT_CAPTION
			case __BSVIT_CAPTION_ATTENTION_LOW:
			{
				CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_caption( htiGen, pItemData, strContent, _T("bsv_caption_attention_low") );
			}
			break; // case __BSVIT_CAPTION_ATTENTION_LOW
			case __BSVIT_CAPTION_ATTENTION_HIGH:
			{
				CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_caption( htiGen, pItemData, strContent, _T("bsv_caption_attention_high") );
			}
			break; // case __BSVIT_CAPTION_ATTENTION_HIGH
			case __BSVIT_SCROLL_CONTAINER_HORZ:
			case __BSVIT_SCROLL_CONTAINER_VERT:
			{
				CExtRichGenItemData::ItemWriter_t _writer(
					htiGen, pItemData, strContent,
					( pItemData->m_nItemType == __BSVIT_SCROLL_CONTAINER_HORZ ) ? _T("bsv_scroll_container_horz") : _T("bsv_scroll_container_vert")
					);
				CExtSafeString strInnerContent;
				OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, nGenContentType );
				strContent += strInnerContent;
			}
			break; // case __BSVIT_SCROLL_CONTAINER
			case __BSVIT_MRU_FILE_ITEM:
			{
					CExtRichGenItemData::ItemWriter_t _writer( htiGen, pItemData, strContent, _T("bsv_mru_file_item"), false, false );
					_writer.WriteHeader();
					CExtSafeString strImageSrcAttr = pItemData->ParmGet( __RCGI_PARM_COMMAND_BUTTON_ICON_IMAGE );
					CExtSafeString strCommandButtonCaption = pItemData->ParmGet( __RCGI_PARM_COMMAND_BUTTON_CAPTION );
					CExtSafeString strCommandButtonDescription = pItemData->ParmGet( __RCGI_PARM_FLAT_COMMAND_BUTTON_TEXT_AT_RIGHT_DESCRIPTION );
					if(		( ! strImageSrcAttr.IsEmpty() )
						||	( ! strCommandButtonCaption.IsEmpty() )
						||	( ! strCommandButtonDescription.IsEmpty() )
						)
					{
						_writer.Format( _T("<table style=\" max-width:100%%; \" ><tr>") );
						if( ! strImageSrcAttr.IsEmpty() )
							_writer.Format( _T("<td style=\" width:40px; text-align:center; \" ><img style=\" min-width:0px; \" src=\"%s\" > </td>"), LPCTSTR(strImageSrcAttr) );
						if(		( ! strCommandButtonCaption.IsEmpty() )
							||	( ! strCommandButtonDescription.IsEmpty() )
							)
						{
							_writer.Format( _T("<td style=\" max-width:100%%; background-color:transparent; \" >") );
							if( ! strCommandButtonCaption.IsEmpty() )
								_writer.Format( _T("<p><bsv_command_button_flat_caption_text>%s</bsv_command_button_flat_caption_text></p>"), LPCTSTR(strCommandButtonCaption) );
							if( ! strCommandButtonDescription.IsEmpty() )
								_writer.Format( _T("<bsv_command_button_flat_description_text>%s</bsv_command_button_flat_description_text>"), LPCTSTR(strCommandButtonDescription) );
							_writer.Format( _T("</td>") );
						}
						_writer.Format( _T("</tr></table>") );
					}
					_writer.WriteFooter();
			}
			break; // case __BSVIT_MRU_FILE_ITEM
			case __BSVIT_TABLE:
			{
				CExtRichGenItemData::ItemWriter_t _writer( htiGen, pItemData, strContent, _T("table") );
				CExtSafeString strInnerContent;
				OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, nGenContentType );
				strContent += strInnerContent;
			}
			break; // case __BSVIT_TABLE
			case __BSVIT_TABLE_ROW:
			{
				CExtRichGenItemData::ItemWriter_t _writer( htiGen, pItemData, strContent, _T("tr") );
				CExtSafeString strInnerContent;
				OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, nGenContentType );
				strContent += strInnerContent;
			}
			break; // case __BSVIT_TABLE
			case __BSVIT_TABLE_CELL:
			{
				CExtRichGenItemData::ItemWriter_t _writer( htiGen, pItemData, strContent, _T("td") );
				CExtSafeString strInnerContent;
				OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, nGenContentType );
				strContent += strInnerContent;
			}
			break; // case __BSVIT_TABLE
			case __BSVIT_ELEMENT_GROUP:
			{
				CExtRichGenItemData::ItemWriter_t _writer( htiGen, pItemData, strContent, _T("bsv_element_group") );
				CExtSafeString strInnerContent;
				OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, nGenContentType );
				strContent += strInnerContent;
			}
			break; // case __BSVIT_ELEMENT_GROUP
			case __BSVIT_ELEMENT_GROUP_ATTENTION_LOW:
			{
				CExtRichGenItemData::ItemWriter_t _writer( htiGen, pItemData, strContent, _T("bsv_element_group_attention_low") );
				CExtSafeString strInnerContent;
				OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, nGenContentType );
				strContent += strInnerContent;
			}
			break; // case __BSVIT_ELEMENT_GROUP_ATTENTION_LOW
			case __BSVIT_ELEMENT_GROUP_ATTENTION_HIGH:
			{
				CExtRichGenItemData::ItemWriter_t _writer( htiGen, pItemData, strContent, _T("bsv_element_group_attention_high") );
				CExtSafeString strInnerContent;
				OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, nGenContentType );
				strContent += strInnerContent;
			}
			break; // case __BSVIT_ELEMENT_GROUP_ATTENTION_HIGH
			} // switch( pItemData->m_nItemType )
		} // else from if( htiGen == htiRoot )
	break; // case __BSV_CONTENT_TYPE_HTML
	case __BSV_CONTENT_TYPE_HTML_PANES:
				switch( pItemData->m_nItemType )
				{
				case __BSVIT_TAB_PANE:
				{
					bool bTopLevelPane = false;
					if( nItemIndent <= 3 )
						bTopLevelPane = true;
					CExtRichGenItemData::ItemWriter_t _writer(
						htiGen, pItemData, strContent, bTopLevelPane ? _T("bsv_top_level_tab_pane") : _T("bsv_second_level_tab_pane") );
					CExtSafeString strInnerContent;
					OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, __BSV_CONTENT_TYPE_HTML_PANES ); // for left/right sub panes
					strContent += strInnerContent;
					strInnerContent.Empty();
					OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, __BSV_CONTENT_TYPE_HTML );
					strContent += strInnerContent;
				}
				break; // case __BSVIT_TAB_PANE
				case __BSVIT_LEFT_PANE_LIGHT:
				{
					CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_left_pane_light( htiGen, pItemData, strContent, _T("bsv_left_pane_light") );
					CExtSafeString strInnerContent;
					OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, __BSV_CONTENT_TYPE_HTML );
					strContent += strInnerContent;
				}
				break; // case __BSVIT_LEFT_PANE_LIGHT
				case __BSVIT_LEFT_PANE_DARK:
				{
					CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_left_pane_dark( htiGen, pItemData, strContent, _T("bsv_left_pane_dark") );
					CExtSafeString strInnerContent;
					OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, __BSV_CONTENT_TYPE_HTML );
					strContent += strInnerContent;
				}
				break; // case __BSVIT_LEFT_PANE_DARK
				case __BSVIT_MIDDLE_PANE_LIGHT:
				{
					CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_middle_pane_light( htiGen, pItemData, strContent, _T("bsv_middle_pane_light") );
					CExtSafeString strInnerContent;
					OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, __BSV_CONTENT_TYPE_HTML );
					strContent += strInnerContent;
					strInnerContent.Empty();
					OnRcgwGenerateContent_WalkChildren_2ndLevelTabPanes( strInnerContent, htiGen );
					strContent += strInnerContent;
				}
				break; // case __BSVIT_MIDDLE_PANE_LIGHT
				case __BSVIT_MIDDLE_PANE_DARK:
				{
					CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_middle_pane_dark( htiGen, pItemData, strContent, _T("bsv_middle_pane_dark") );
					CExtSafeString strInnerContent;
					OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, __BSV_CONTENT_TYPE_HTML );
					strContent += strInnerContent;
					strInnerContent.Empty();
					OnRcgwGenerateContent_WalkChildren_2ndLevelTabPanes( strInnerContent, htiGen );
					strContent += strInnerContent;
				}
				break; // case __BSVIT_MIDDLE_PANE_DARK
				case __BSVIT_RIGHT_PANE_LIGHT:
				{
					CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_right_pane_light( htiGen, pItemData, strContent, _T("bsv_right_pane_light") );
					CExtSafeString strInnerContent;
					OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, __BSV_CONTENT_TYPE_HTML );
					strContent += strInnerContent;
					strInnerContent.Empty();
					OnRcgwGenerateContent_WalkChildren_2ndLevelTabPanes( strInnerContent, htiGen );
					strContent += strInnerContent;
				}
				break; // case __BSVIT_RIGHT_PANE_LIGHT
				case __BSVIT_RIGHT_PANE_DARK:
				{
					CExtRichGenItemData::ItemWriter_t ItemWriter_bsv_right_pane_dark( htiGen, pItemData, strContent, _T("bsv_right_pane_dark") );
					CExtSafeString strInnerContent;
					OnRcgwGenerateContent_WalkChildren( strInnerContent, htiGen, __BSV_CONTENT_TYPE_HTML );
					strContent += strInnerContent;
					strInnerContent.Empty();
					OnRcgwGenerateContent_WalkChildren_2ndLevelTabPanes( strInnerContent, htiGen );
					strContent += strInnerContent;
				}
				break; // case __BSVIT_RIGHT_PANE_DARK
				default:
					OnRcgwGenerateContent_WalkChildren( strContent, htiGen, __BSV_CONTENT_TYPE_HTML_PANES );
				break;
				} // switch( pItemData->m_nItemType )
	break; // case __BSV_CONTENT_TYPE_HTML_PANES
	} // switch( nGenContentType )
	//return CExtRichGenWnd::OnRcgwGenerateContent( strContent, htiGen, nGenContentType );
	return true;
}

bool CExtRibbonBackstageViewWnd::OnRcgwGenerateContent_WalkChildren_2ndLevelTabPanes( CExtSafeString & strContent, HTREEITEM htiGen )
{
	ASSERT_VALID( this );
	ASSERT( htiGen != NULL );
	ASSERT( RcgtdItemIsPresent( htiGen ) );
	ASSERT( RcgtdItemGetRoot() != NULL );
	ASSERT( htiGen != RcgtdItemGetRoot() );
#ifdef _DEBUG
	{ // block
		ULONG nItemIndent = RcgtdItemIndentGet( htiGen );
		ASSERT( nItemIndent > 2L );
		CExtRichGenItemData * pItemData = (CExtRichGenItemData *) RcgtdItemDataGet( htiGen );
		ASSERT( pItemData != NULL );
		ASSERT(
				pItemData->m_nItemType == __BSVIT_MIDDLE_PANE_LIGHT || pItemData->m_nItemType == __BSVIT_MIDDLE_PANE_DARK
			||	pItemData->m_nItemType == __BSVIT_RIGHT_PANE_LIGHT || pItemData->m_nItemType == __BSVIT_RIGHT_PANE_DARK
			);
	} // block
#endif // _DEBUG
CList < HTREEITEM, HTREEITEM > listTabItems, listChildPanes;
HTREEITEM htiWalk = RcgtdItemGetParent( htiGen );
	if( htiWalk == NULL )
		return true;
	// parent of right-pane should be top-level tab pane
CExtRichGenItemData * pItemData = (CExtRichGenItemData *) RcgtdItemDataGet( htiWalk );
	ASSERT( pItemData != NULL );
	if( pItemData->m_nItemType != __BSVIT_TAB_PANE )
		return true;
	// find left-pane
	for( htiWalk = RcgtdItemGetFirstChild( htiWalk ); htiWalk != NULL; htiWalk = RcgtdItemGetNextSibling( htiWalk ) )
	{
		CExtRichGenItemData * pItemData = (CExtRichGenItemData *) RcgtdItemDataGet( htiWalk );
		ASSERT( pItemData != NULL );
		if( pItemData->m_nItemType == __BSVIT_LEFT_PANE_LIGHT || pItemData->m_nItemType == __BSVIT_LEFT_PANE_DARK )
			break;
	} // for( htiWalk = RcgtdItemGetFirstChild( htiWalk ); htiWalk != NULL; htiWalk = RcgtdItemGetNextSibling( htiWalk ) )
	if( htiWalk == NULL )
		return true;
	// find tab items in left-pane
	for( htiWalk = RcgtdItemGetFirstChild( htiWalk ); htiWalk != NULL; htiWalk = RcgtdItemGetNextSibling( htiWalk ) )
	{
		CExtRichGenItemData * pItemData = (CExtRichGenItemData *) RcgtdItemDataGet( htiWalk );
		ASSERT( pItemData != NULL );
		if( pItemData->m_nItemType == __BSVIT_TAB_ITEM )
		{
			listTabItems.AddTail( htiWalk );
			// find tab-pane in this tab item
			for( HTREEITEM htiWalk2 = RcgtdItemGetFirstChild( htiWalk ); htiWalk2 != NULL; htiWalk2 = RcgtdItemGetNextSibling( htiWalk2 ) )
			{
				pItemData = (CExtRichGenItemData *) RcgtdItemDataGet( htiWalk2 );
				ASSERT( pItemData != NULL );
				if( pItemData->m_nItemType == __BSVIT_TAB_PANE )
				{
					listChildPanes.AddTail( htiWalk2 );
					break;
				}
			} // for( HTREEITEM htiWalk2 = RcgtdItemGetFirstChild( htiWalk ); htiWalk2 != NULL; htiWalk2 = RcgtdItemGetNextSibling( htiWalk2 ) )
		}
	} // for( htiWalk = RcgtdItemGetFirstChild( htiWalk ); htiWalk != NULL; htiWalk = RcgtdItemGetNextSibling( htiWalk ) )
	if( listTabItems.GetCount() == 0 || listChildPanes.GetCount() == 0 )
		return true;
POSITION pos1 = listTabItems.GetHeadPosition(), pos2 = listChildPanes.GetHeadPosition();
	for( ; pos1 != NULL && pos2 != NULL; )
	{
		HTREEITEM htiTabItem = listTabItems.GetNext( pos1 );
		HTREEITEM htiTabPane = listChildPanes.GetNext( pos2 );
		ASSERT( htiTabItem != NULL && htiTabPane != NULL );
		CExtRichGenItemData * pItemData_TabPane = (CExtRichGenItemData *) RcgtdItemDataGet( htiTabPane );
		ASSERT( pItemData_TabPane != NULL );
		CExtSafeString strID;
		strID.Format( _T(" uiiddst=\"%p\" "), LPVOID(htiTabItem) );
		pItemData_TabPane->ParmAppend( __RCGI_PARM_TAG_PARAMETERS, LPCTSTR(strID) );
		CExtSafeString strContentInner;
		if( ! OnRcgwGenerateContent( strContentInner, htiTabPane, __BSV_CONTENT_TYPE_HTML_PANES ) )
			return false;
		strContent += strContentInner;
	} // for( ; pos1 != NULL && pos2 != NULL; )
	return true;
}

#if (!defined __EXT_MFC_NO_CUSTOMIZE)

HTREEITEM CExtRibbonBackstageViewWnd::OnRcgwTranslateTreeItemKeyTip( HTREEITEM hti )
	{ ASSERT_VALID( this ); ASSERT( hti != NULL ); ASSERT( RcgtdItemIsPresent( hti ) ); return hti; }
CExtRichContentItem * CExtRibbonBackstageViewWnd::OnRcgwTranslateElementKeyTip( CExtRichContentItem * pRCI )
	{ ASSERT_VALID( this ); ASSERT( pRCI != NULL ); ASSERT( LPVOID(&(pRCI->m_layout)) == LPVOID(RcsLayoutGet()) ); return pRCI; }

CPoint CExtRibbonBackstageViewWnd::OnRcgtdKeyTipGetGuideLines( HTREEITEM hti )
{
CPoint ptKeyTipGetGuideLines( __EXT_RICH_CONTENT_NEGATIVE_UNUSED, __EXT_RICH_CONTENT_NEGATIVE_UNUSED );
	if( hti == NULL )
		return ptKeyTipGetGuideLines;
	ASSERT( RcgtdItemIsPresent( hti ) );
HTREEITEM htiTranslated = OnRcgwTranslateTreeItemKeyTip( hti );
	if( htiTranslated == NULL )
		return ptKeyTipGetGuideLines;
	if( htiTranslated == NULL )
		return ptKeyTipGetGuideLines;
	ASSERT( RcgtdItemIsPresent( htiTranslated ) );
CExtRichGenItemData * pData = (CExtRichGenItemData*)RcgtdItemDataGet( htiTranslated );
	if( pData == NULL )
		return ptKeyTipGetGuideLines;
ULONG nItemIndent = RcgtdItemIndentGet( htiTranslated );
	if( nItemIndent < 3 )
	{
		if( ! m_bHelperUserStartKeyTipMode )
			return ptKeyTipGetGuideLines;
	}
	else
	{
		if( m_bHelperUserStartKeyTipMode )
			return ptKeyTipGetGuideLines;
	}
	ptKeyTipGetGuideLines = CExtRichGenWnd::OnRcgtdKeyTipGetGuideLines( hti );
	if( ptKeyTipGetGuideLines.x == __EXT_RICH_CONTENT_NEGATIVE_UNUSED || ptKeyTipGetGuideLines.y == __EXT_RICH_CONTENT_NEGATIVE_UNUSED )
		return ptKeyTipGetGuideLines;
	switch( pData->m_nItemType )
	{
	case __BSVIT_TAB_ITEM:
	case __BSVIT_COMMAND_BUTTON_TOP_LEVEL:
		ptKeyTipGetGuideLines.x += 20;
		ptKeyTipGetGuideLines.y += 6;
	break;
	} // switch( pData->m_nItemType )
	return ptKeyTipGetGuideLines;
}

bool CExtRibbonBackstageViewWnd::OnRcgtdKeyTipInvokeAction(
	HTREEITEM hti,
	bool & bContinueKeyTipMode
	)
{
	ASSERT( hti != NULL );
	ASSERT( RcgtdItemIsPresent( hti ) );
// #if (!defined __EXT_MFC_NO_CUSTOMIZE)
// 	OnRcgtdKeyTipTrackingStop();
// #endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
	bContinueKeyTipMode = false;
CExtRichGenItemData * pData = (CExtRichGenItemData*)RcgtdItemDataGet( hti );
	if( pData == NULL )
		return false;
	if( pData->m_nItemType == __BSVIT_TAB_ITEM  )
		bContinueKeyTipMode = true;
	//VERIFY( RcgwTreeItemSelect( hti, true, false ) );
	VERIFY( RcgwTreeItemFocus( hti ) );
	if( pData->m_nItemType == __BSVIT_TAB_ITEM  )
	{
		OnRcgtdKeyTipTrackingStop();
		m_bHelperUserStartKeyTipMode = false;
		OnRcgtdKeyTipTrackingStart();
		return false;
	}
	if(		( __BSVIT_COMMAND_BUTTON_FIRST <= pData->m_nItemType && pData->m_nItemType <= __BSVIT_COMMAND_BUTTON_LAST )
		||	pData->m_nItemType == __BSVIT_MRU_FILE_ITEM
		)
	{
		OnRcgtdItemInvoke( hti, CExtRichGenTreeData::__E_RCTD_IIT_KEY_TIP );
		return true;
	}
	return true;
}

#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

bool CExtRibbonBackstageViewWnd::OnRcgtdItemInvoke( HTREEITEM hti, CExtRichGenTreeData::e_item_invoke_type eIIT )
{
	ASSERT_VALID( this );
	ASSERT( hti != NULL );
	ASSERT( RcgtdItemIsPresent( hti ) );
CExtRichContentLayout * pRCL = RcsLayoutGet();
	if( pRCL == NULL )
		return CExtRichGenWnd::OnRcgtdItemInvoke( hti, eIIT );
CExtRichGenItemData * pData = (CExtRichGenItemData*)RcgtdItemDataGet( hti );
	if( pData == NULL )
		return CExtRichGenWnd::OnRcgtdItemInvoke( hti, eIIT );
	if(		( ! ( __BSVIT_COMMAND_BUTTON_FIRST <= pData->m_nItemType && pData->m_nItemType <= __BSVIT_COMMAND_BUTTON_LAST ) )
		&&	( ! ( pData->m_nItemType == __BSVIT_MRU_FILE_ITEM ) )
		)
		return CExtRichGenWnd::OnRcgtdItemInvoke( hti, eIIT );
	if(		eIIT == __E_RCTD_IIT_MOUSE_MBUTTON_DOWN
		||	eIIT == __E_RCTD_IIT_MOUSE_MBUTTON_UP
		||	eIIT == __E_RCTD_IIT_MOUSE_RBUTTON_DOWN
		||	eIIT == __E_RCTD_IIT_MOUSE_RBUTTON_UP
		)
		return CExtRichGenWnd::OnRcgtdItemInvoke( hti, eIIT );
HWND hWndOwn = m_hWnd;
CObject * pThis = this;
	if(		eIIT == __E_RCTD_IIT_KEY_VK_SPACE_DOWN
		||	eIIT == __E_RCTD_IIT_KEY_VK_RETURN_DOWN
		||	eIIT == __E_RCTD_IIT_KEY_TIP
		)
	{ // switch buttons into pressed state
		CExtSafeString strID;
		strID.Format( _T("%p"), LPVOID(hti) );
		CExtRichContentItem * pRCI = pRCL->ElementByUiBindingSrcID( LPCTSTR(strID) );
		if( pRCI != NULL )
		{
			CExtRichStyleDescription::pseudo_class_def_t statesPressedOnly;
			statesPressedOnly.m_eynPressed = CExtRichStyleDescription::e_ui_yn_yes;
			//bool bHaveElementsWithResetStyles = false;
			//pRCI->RciEvent_OnPressedEnter( nChar, nRepCnt, nFlags, m_htInfoPressed, htInfoPressed, m_hWnd );
			m_htInfoPressed.Empty();
					m_htInfoPressed.m_pStartHtRCI = pRCI;
					CExtRichContentHitTestInfo::ht_item_t _ht_item( pRCI, __ERCHT_ON_ITEM_INNER_AREA );
					m_htInfoPressed.m_listHtBranch.AddTail( _ht_item );
					m_bPressedTracking = true;
			pRCI->ResetElementStateInfo( statesPressedOnly, CExtRichStyleDescription::e_ui_yn_yes );
			if( pRCI->HaveAffectedCssPseudoClasses( statesPressedOnly ) )
			{
				//bHaveElementsWithResetStyles = true;
				OnRcsResetElementStyle( pRCI );
				OnSwInvalidate( m_bRedrawErase );
				OnSwUpdateWindow();
			}
		} // if( pRCI != NULL )
	} // switch buttons into pressed state
	if(		pData->m_nItemType == __BSVIT_COMMAND_BUTTON_SQUARE_DD
		||	pData->m_nItemType == __BSVIT_COMMAND_BUTTON_SQUARE_FLAT_DD
		||	pData->m_nItemType == __BSVIT_COMMAND_BUTTON_WIDE_DD
		||	pData->m_nItemType == __BSVIT_COMMAND_BUTTON_WIDE_FLAT_DD
		)
	{ // if drop-down buttons
		if(		eIIT == __E_RCTD_IIT_MOUSE_LBUTTON_DOWN
			||	eIIT == __E_RCTD_IIT_KEY_VK_SPACE_DOWN
			||	eIIT == __E_RCTD_IIT_KEY_VK_RETURN_DOWN
			||	eIIT == __E_RCTD_IIT_KEY_TIP
			)
			_OnBsvItemInvokeDropDownMenuImpl( hti, eIIT );
	} // if drop-down buttons
	else
	{ // if command buttons
		if(		eIIT == __E_RCTD_IIT_MOUSE_LBUTTON_UP
			||	eIIT == __E_RCTD_IIT_KEY_VK_SPACE_UP
			||	eIIT == __E_RCTD_IIT_KEY_VK_RETURN_UP
			||	eIIT == __E_RCTD_IIT_KEY_TIP
			)
			_OnBsvItemInvokeCommandImpl( hti, eIIT );
	} // if command buttons
	if( ! ::IsWindow( hWndOwn ) )
		return true;
	if( CWnd::FromHandlePermanent( hWndOwn ) != pThis )
		return true;
	if(		eIIT == __E_RCTD_IIT_KEY_VK_SPACE_UP
		||	eIIT == __E_RCTD_IIT_KEY_VK_RETURN_UP
		||	eIIT == __E_RCTD_IIT_KEY_TIP
		)
	{ // switch buttons into non-pressed state
		CExtSafeString strID;
		strID.Format( _T("%p"), LPVOID(hti) );
		CExtRichContentItem * pRCI = pRCL->ElementByUiBindingSrcID( LPCTSTR(strID) );
		if( pRCI != NULL )
		{
			CExtRichStyleDescription::pseudo_class_def_t statesPressedOnly;
			statesPressedOnly.m_eynPressed = CExtRichStyleDescription::e_ui_yn_yes;
			//bool bHaveElementsWithResetStyles = false;
			//pRCI->RciEvent_OnPressedEnter( nChar, nRepCnt, nFlags, m_htInfoPressed, htInfoPressed, m_hWnd );
			pRCI->ResetElementStateInfo( statesPressedOnly, CExtRichStyleDescription::e_ui_yn_unspecified );
			if( pRCI->HaveAffectedCssPseudoClasses( statesPressedOnly ) )
			{
				//bHaveElementsWithResetStyles = true;
				OnRcsResetElementStyle( pRCI );
				OnSwInvalidate( m_bRedrawErase );
				OnSwUpdateWindow();
			}
		} // if( pRCI != NULL )
	} // switch buttons into non-pressed state
	return true;
}

void CExtRibbonBackstageViewWnd::_OnBsvItemInvokeCommandImpl( HTREEITEM hti, CExtRichGenTreeData::e_item_invoke_type eIIT )
{
	ASSERT_VALID( this );
	ASSERT( hti != NULL );
	if( ! RcgwTreeItemIsEnabled( hti ) )
		return;
	OnBsvItemInvokeCommand( hti, eIIT );
}

void CExtRibbonBackstageViewWnd::OnBsvItemInvokeCommand( HTREEITEM hti, CExtRichGenTreeData::e_item_invoke_type eIIT )
{
	ASSERT_VALID( this );
	ASSERT( hti != NULL );
	ASSERT( RcgtdItemIsPresent( hti ) );
	eIIT;
CWnd * pWndCmdTarget = GetOwner();
	if( pWndCmdTarget->GetSafeHwnd() == NULL )
		return;
CExtRichGenItemData * pItemData  = (CExtRichGenItemData*)RcgtdItemDataGet( hti );
	if( pItemData == NULL || ( ! CExtCmdManager::IsCommand( pItemData->m_nCmdID ) ) )
		return;
	pWndCmdTarget->SendMessage( WM_COMMAND, WPARAM(pItemData->m_nCmdID) );
}

void CExtRibbonBackstageViewWnd::_OnBsvItemInvokeDropDownMenuImpl( HTREEITEM hti, CExtRichGenTreeData::e_item_invoke_type eIIT )
{
	ASSERT_VALID( this );
	ASSERT( hti != NULL );
	if( ! RcgwTreeItemIsEnabled( hti ) )
		return;
bool bHaveElementsWithResetStyles = false; 
CExtRichContentLayout * pRCL = RcsLayoutGet();
	ASSERT( pRCL != NULL );
CExtRichContentItem * pRootRCI = ( pRCL->m_listHeap.GetCount() > 0 ) ? pRCL->m_listHeap.GetHead() : NULL;
	if( pRootRCI != NULL )
	{
		CExtRichStyleDescription::pseudo_class_def_t statesPressedAndHover;
		statesPressedAndHover.m_eynPressed = CExtRichStyleDescription::e_ui_yn_yes;
		statesPressedAndHover.m_eynHover = CExtRichStyleDescription::e_ui_yn_yes;
		if( OnRcsResetElementStates( pRootRCI, statesPressedAndHover, CExtRichStyleDescription::e_ui_yn_unspecified ) )
			bHaveElementsWithResetStyles = true;
		if( bHaveElementsWithResetStyles )
		{
			OnSwInvalidate( m_bRedrawErase );
			RcsDelayRecalcLayout();
			OnSwRecalcLayout( true );
		}
		m_htInfoHover.Empty();
		m_htInfoPressed.Empty();
		m_bPressedTracking = false;
	}
	OnBsvItemInvokeDropDownMenu( hti, eIIT );
}

void CExtRibbonBackstageViewWnd::OnBsvItemInvokeDropDownMenu( HTREEITEM hti, CExtRichGenTreeData::e_item_invoke_type eIIT )
{
	ASSERT_VALID( this );
	ASSERT( hti != NULL );
	ASSERT( RcgtdItemIsPresent( hti ) );
	hti; eIIT;
}

CExtRichContentItem * CExtRibbonBackstageViewWnd::OnRcgwTranslateElement( CExtRichContentItem * pRCI )
{
	ASSERT_VALID( this );
	ASSERT( pRCI != NULL );
	ASSERT( LPVOID(&(pRCI->m_layout)) == LPVOID(RcsLayoutGet()) );
	if(		pRCI->m_pParent != NULL
		&&	(	pRCI->m_strTextParsed.CompareNoCase( _T("bsv_top_level_tab_item_content_container") ) == 0
			||	pRCI->m_strTextParsed.CompareNoCase( _T("bsv_second_level_tab_item_content_container") ) == 0
			)
		)
		return pRCI->m_pParent;
	return pRCI;
}

bool CExtRibbonBackstageViewWnd::RciEvent_OnSelect(
	CExtRichContentItem * pRCI,
	HWND hWndContentViewer,
	bool bSelected
	)
{
	ASSERT_VALID( this );
	ASSERT( pRCI != NULL );
	ASSERT( hWndContentViewer == m_hWnd && ::IsWindow(hWndContentViewer) );
	hWndContentViewer;
//#if (!defined __EXT_MFC_NO_CUSTOMIZE)
//	OnRcgtdKeyTipTrackingStop();
//#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
	if( pRCI->m_strTextParsed.CompareNoCase( _T("bsv_top_level_tab_item") ) == 0 )
	{
		CExtRichContentLayout * pRCL = RcsLayoutGet();
		ASSERT( pRCL != NULL );
		CExtRichStyleDescription::pseudo_class_def_t statesSelectionOnly;
		statesSelectionOnly.m_eynSelection = CExtRichStyleDescription::e_ui_yn_yes;
		CExtRichContentItem * pRCI_arrow = pRCI->FindElementByText( _T("bsv_top_level_tab_item_arrow") );
		if( pRCI_arrow != NULL )
			pRCI_arrow->RciEvent_ResetElementStateInfo( m_hWnd, statesSelectionOnly, bSelected ? CExtRichStyleDescription::e_ui_yn_yes : CExtRichStyleDescription::e_ui_yn_unspecified );
		CExtRichContentItem * pRCI_tab_item_content_container = pRCI->FindElementByText( _T("bsv_top_level_tab_item_content_container") );
		if( pRCI_tab_item_content_container != NULL )
		{
			CExtSafeString strID = pRCI_tab_item_content_container->ElementUiBindingSrcIdGet();
			ASSERT( ! strID.IsEmpty() );
			HTREEITEM htiTabItemToSelect = NULL;
			int nFieldsScanned =
#if _MFC_VER >= 0x800
				_stscanf_s( LPCTSTR(strID), _T("%p"), &htiTabItemToSelect );
#else
				_stscanf( LPCTSTR(strID), _T("%p"), &htiTabItemToSelect );
#endif
			ASSERT( nFieldsScanned == 1 );
			nFieldsScanned;
			ASSERT( htiTabItemToSelect != NULL && RcgtdItemIsPresent( htiTabItemToSelect ) );
			HTREEITEM htiTabPaneToShow = RcgtdItemGetFirstChild( htiTabItemToSelect );
			if( htiTabPaneToShow != NULL )
			{
				CExtRichGenItemData * pData_TabPane = (CExtRichGenItemData*)RcgtdItemDataGet( htiTabPaneToShow );
				if( pData_TabPane != NULL && pData_TabPane->m_nItemType == __BSVIT_TAB_PANE )
				{
					strID.Format( _T("%p"), LPVOID(htiTabPaneToShow) );
					CExtRichContentItem * pRCI_pane = pRCL->ElementByUiBindingSrcID( LPCTSTR(strID) );
					if( pRCI_pane != NULL )
						pRCI_pane->RciEvent_ResetElementStateInfo( m_hWnd, statesSelectionOnly, bSelected ? CExtRichStyleDescription::e_ui_yn_yes : CExtRichStyleDescription::e_ui_yn_unspecified, false );
				}
			} // if( htiTabPaneToShow != NULL )
		} // if( pRCI_tab_item_content_container != NULL )
		RcsDelayRecalcLayout();
	} // if( pRCI->m_strTextParsed.CompareNoCase( _T("bsv_top_level_tab_item") ) == 0 )
	else if( pRCI->m_strTextParsed.CompareNoCase( _T("bsv_second_level_tab_item") ) == 0 )
	{
		CExtRichContentLayout * pRCL = RcsLayoutGet();
		ASSERT( pRCL != NULL );
		CExtRichStyleDescription::pseudo_class_def_t statesSelectionOnly;
		statesSelectionOnly.m_eynSelection = CExtRichStyleDescription::e_ui_yn_yes;
		CExtRichContentItem * pRCI_arrow = pRCI->FindElementByText( _T("bsv_second_level_tab_item_arrow") );
		if( pRCI_arrow != NULL )
			pRCI_arrow->RciEvent_ResetElementStateInfo( m_hWnd, statesSelectionOnly, bSelected ? CExtRichStyleDescription::e_ui_yn_yes : CExtRichStyleDescription::e_ui_yn_unspecified );
		CExtSafeString strID = pRCI->ElementUiBindingSrcIdGet();
		ASSERT( ! strID.IsEmpty() );
		HTREEITEM htiTabItemToSelect = NULL;
		int nFieldsScanned =
#if _MFC_VER >= 0x800
			_stscanf_s( LPCTSTR(strID), _T("%p"), &htiTabItemToSelect );
#else
			_stscanf( LPCTSTR(strID), _T("%p"), &htiTabItemToSelect );
#endif
		ASSERT( nFieldsScanned == 1 );
		nFieldsScanned;
		ASSERT( htiTabItemToSelect != NULL && RcgtdItemIsPresent( htiTabItemToSelect ) );
//		if( ! RcgwTreeItemIsEnabled( htiTabItemToSelect ) )
//			return true;
		strID.Format( _T("%p"), LPVOID(htiTabItemToSelect) );
		CExtRichContentItem * pRCI_pane = pRCL->ElementByUiBindingDstID( LPCTSTR(strID) );
		if( pRCI_pane != NULL )
			pRCI_pane->RciEvent_ResetElementStateInfo( m_hWnd, statesSelectionOnly, bSelected ? CExtRichStyleDescription::e_ui_yn_yes : CExtRichStyleDescription::e_ui_yn_unspecified, false );
		RcsDelayRecalcLayout();
	} // if( pRCI->m_strTextParsed.CompareNoCase( _T("bsv_second_level_tab_item") ) == 0 )
	return CExtRichGenWnd::RciEvent_OnSelect( pRCI, hWndContentViewer, bSelected );
}

CExtRichContentItem * CExtRibbonBackstageViewWnd::OnRcsFindNextTabStopElement(
	CExtRichContentItem * pStartSearchRCI,
	bool bNext,
	UINT nRepCnt // VK_TAB pressing count
	)
{
	ASSERT_VALID( this );
CExtRichContentItem * pWalkRCI = pStartSearchRCI;
	for( ; nRepCnt > 0; nRepCnt -- )
	{
		CExtRichContentItem * pRCI = CExtRichGenWnd::OnRcsFindNextTabStopElement( pWalkRCI, bNext, 1 );
		if( pRCI == NULL || LPVOID(pRCI) == LPVOID(pStartSearchRCI) )
			return NULL;
		CExtSafeString strID = pRCI->ElementUiBindingSrcIdGet();
		ASSERT( ! strID.IsEmpty() );
		HTREEITEM hti = NULL;
		int nFieldsScanned =
		#if _MFC_VER >= 0x800
			_stscanf_s( LPCTSTR(strID), _T("%p"), &hti );
		#else
			_stscanf( LPCTSTR(strID), _T("%p"), &hti );
		#endif
		ASSERT( nFieldsScanned == 1 );
		nFieldsScanned;
		ASSERT( hti != NULL && RcgtdItemIsPresent( hti ) );
		if( RcgwTreeItemIsEnabled( hti ) )
			return pRCI;
		pWalkRCI = pRCI;
		nRepCnt++;
	} // for( ; nRepCnt > 0; nRepCnt -- )
	return NULL;
}

bool CExtRibbonBackstageViewWnd::OnRcsAnalyzeKeyEvent(
	bool bSysKey,
	bool bKeyDownEvent, // true - key-down event, false - key-up event
	UINT nChar, // virtual key code
	UINT nRepCnt, // key-down/key-up press count
	UINT nFlags // key-down/key-up event flags
	)
{
	ASSERT_VALID( this );
	switch( nChar )
	{
	case VK_ESCAPE:
		if(		( abs( m_clockLastMenuEscapement - ::clock() ) > ( (clock_t) ::GetDoubleClickTime() ) )
			&&	(! RcgtdKeyTipsDisplayedGet() )
			&&	(! CExtPopupMenuWnd::IsMenuTracking() )
			)
		{
			CExtRibbonBar * pRibbonBar = BsvGetRibbonBar();
			if( pRibbonBar->GetSafeHwnd() != NULL && BsvIsVisible() )
			{
				BsvShow( false, NULL );
				return true;	
			}
		}
	break; // case VK_ESCAPE
	case VK_TAB:
	case VK_LEFT:
	case VK_RIGHT:
		if( m_pRCI_focus != NULL )
		{
			CExtSafeString strID = m_pRCI_focus->ElementUiBindingSrcIdGet();
			ASSERT( ! strID.IsEmpty() );
			HTREEITEM hti = NULL;
			int nFieldsScanned =
#if _MFC_VER >= 0x800
				_stscanf_s( LPCTSTR(strID), _T("%p"), &hti );
#else
				_stscanf( LPCTSTR(strID), _T("%p"), &hti );
#endif
			ASSERT( nFieldsScanned == 1 );
			nFieldsScanned;
			if( hti != NULL )
			{
				CExtRichGenItemData * pItemData = (CExtRichGenItemData *) RcgtdItemDataGet( hti );
				if( pItemData != NULL && pItemData->m_nItemType == __BSVIT_TAB_ITEM )
				{ // if left/right arrow keys on tab item
					//
					HTREEITEM htiSearch = RcgtdItemGetFirstChild( hti );
					for( ; htiSearch != NULL; htiSearch = RcgtdItemGetNextSibling( htiSearch ) )
					{
						CExtRichGenItemData * pItemData = (CExtRichGenItemData *) RcgtdItemDataGet( htiSearch );
						if( pItemData != NULL && pItemData->m_nItemType == __BSVIT_TAB_PANE )
							break;
					} // for( ; htiSearch != NULL; htiSearch = RcgtdItemGetNextSibling( htiSearch ) )
					if( htiSearch != NULL )
					{ // if tab pane was found
						CExtRichContentLayout * pRCL = RcsLayoutGet();
						ASSERT( pRCL != NULL );
						CExtSafeString strID;
						strID.Format( _T("%p"), LPVOID(htiSearch) );
						CExtRichContentItem * pRCI = pRCL->ElementByUiBindingSrcID( LPCTSTR(strID) );
						if( pRCI != NULL )
						{
							CExtRichContentItem * pNewFocusRCI = OnRcsFindNextTabStopElement( pRCI, true, nRepCnt );
							if( pNewFocusRCI != NULL )
							{
								if( bKeyDownEvent )
								{
									ASSERT( LPVOID(pNewFocusRCI) != LPVOID(m_pRCI_focus) );
									ASSERT( pNewFocusRCI->IsParticipantOfFocus() );
									bool bQuickReStyleMode = ( ( RcsGetStyle() & __EXT_RCS_QUICK_RESTYLE_MODE ) ) ? true : false;
									bool bRecalcPassed = false, bHaveElementsWithResetStyles = false;

#if (defined __EXT_RCS_WND_APPLY_HOVER_RESET_WORKAROUND__)
									// BEGIN // temporarily solution, reset hovered state to avoid incorrect inheritance from parent element inside OnRcsResetElementStates()
									if( ! m_htInfoHover.IsEmpty() )
									{
										m_htInfoHover.Empty();
										CExtRichStyleDescription::pseudo_class_def_t statesHoverOnly;
										statesHoverOnly.m_eynHover = CExtRichStyleDescription::e_ui_yn_yes;
										OnRcsResetElementStates( pRootRCI, statesHoverOnly, CExtRichStyleDescription::e_ui_yn_unspecified );
									}
				//  END  // temporarily solution, reset hovered state to avoid incorrect inheritance from parent element inside OnRcsResetElementStates()
#endif // (defined __EXT_RCS_WND_APPLY_HOVER_RESET_WORKAROUND__)

									CExtRichStyleDescription::pseudo_class_def_t statesFocusOnly;
									statesFocusOnly.m_eynFocus = CExtRichStyleDescription::e_ui_yn_yes;
									CExtRichContentItem * pOldFocusRCI = m_pRCI_focus;
									if( m_pRCI_focus != NULL )
									{
										m_pRCI_focus->RciEvent_OnFocusLeave( m_hWnd, pNewFocusRCI );
										if( OnRcsResetElementStates( m_pRCI_focus, statesFocusOnly, CExtRichStyleDescription::e_ui_yn_unspecified ) )
											bHaveElementsWithResetStyles = true;
									}
									m_pRCI_focus = pNewFocusRCI;
									bRecalcPassed = true;
									if( ! bQuickReStyleMode )
									{
										RcsDelayRecalcLayout();
										OnSwRecalcLayout( true );
									}
									m_pRCI_focus->RciEvent_OnFocusEnter( m_hWnd, pOldFocusRCI );
									OnSwInvalidate( m_bRedrawErase );
									if( OnRcsResetElementStates( m_pRCI_focus, statesFocusOnly, CExtRichStyleDescription::e_ui_yn_yes ) )
										bHaveElementsWithResetStyles = true;
									if( ! bHaveElementsWithResetStyles )
										return true;
									if( ! bQuickReStyleMode )
									{
										RcsDelayRecalcLayout();
										OnSwRecalcLayout( true );
										OnSwUpdateScrollBars();
									}
								} // if( bKeyDownEvent )
								return true;
							} // if( pRCI != NULL )
						} // if( pRCI != NULL )
					} // if tab pane was found
				} // if left/right arrow keys on tab item
			} // if( hti != NULL )
		} // if( m_pRCI_focus != NULL )
	break; // case VK_TAB, VK_LEFT and VK_RIGHT
	} // switch( nChar )
	if( CExtRichGenWnd::OnRcsAnalyzeKeyEvent( bSysKey, bKeyDownEvent, nChar, nRepCnt, nFlags ) )
		return true;
/*
	switch( nChar )
	{
	} // switch( nChar )
*/
	return false;
}

bool CExtRibbonBackstageViewWnd::OnHookWndMsg(
	LRESULT & lResult,
	HWND hWndHooked,
	UINT nMessage,
	WPARAM & wParam,
	LPARAM & lParam
	)
{
__PROF_UIS_MANAGE_STATE;
	if( GetSafeHwnd() != NULL && m_helperHookedHWND != NULL && hWndHooked == m_helperHookedHWND && m_pRibbonBar->GetSafeHwnd() != NULL )
	{
		switch( nMessage )
		{
		case WM_SIZE:
			if( ! ::IsIconic( hWndHooked ) )
			{ // if need to resize backstage view
				CRect rcBsv = BsvCalcRect();
				SetWindowPos( &wndTop, rcBsv.left, rcBsv.top, rcBsv.Width(), rcBsv.Height(), SWP_NOOWNERZORDER|SWP_NOACTIVATE );
			} // if need to resize backstage view
		break; // case WM_SIZE
		} // switch( nMessage )
	} // if( GetSafeHwnd() != NULL && m_helperHookedHWND != NULL && hWndHooked == m_helperHookedHWND && m_pRibbonBar->GetSafeHwnd() != NULL )
	return 
		CExtRichGenWnd::OnHookWndMsg(
			lResult,
			hWndHooked,
			nMessage,
			wParam,
			lParam
			);
}

#endif // #if (!defined __EXT_MFC_NO_BACKSTAGEVIEWWND)

#endif // (!defined __EXT_MFC_NO_RIBBON_BAR)

