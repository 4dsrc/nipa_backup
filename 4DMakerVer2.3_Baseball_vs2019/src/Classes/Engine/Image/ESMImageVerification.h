////////////////////////////////////////////////////////////////////////////////
//
//	TGImageVerification.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-10
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "TGImageColorDetect.h"


class CTGImageVerification
{

public:
	CTGImageVerification();
	virtual ~CTGImageVerification();

	int m_nImgResultType;
	IMAGE_RESULT m_ImgResult;

public:
	DWORD		m_dwSequence;
	void InitData();

	//-- 2012-09-25 joonho.kim
	BOOL SaveFailImage(CString& strImgPath, IplImage* pImage);
	void SetResult(int nResult);
	int	 GetResult();
	BOOL InsertToDB(CString strImgPath);

	BOOL DetectRGB(IplImage* pImage);
	BOOL DetectRGBColor(IplImage* pImage, int nColor);
	
	IplImage* ImageCheck(unsigned char* pBuf, int nWidth, int nHeight);
	IplImage* GetColorImage(IplImage* img, int nMin, int nMax);
	IplImage* FilterRGB(IplImage* pImgSrc, CTGImageColorDetect* pColorDetect);

	void cvOverlayImage(IplImage* src, IplImage* overlay, CTGImageColorDetect colorDetect);

	//-- 2012-08-22 hongsu@esmlab.com
	//-- Send Draw Info 
	void DrawLatencyGraph(CString strTime, double dbPercentage);
};

