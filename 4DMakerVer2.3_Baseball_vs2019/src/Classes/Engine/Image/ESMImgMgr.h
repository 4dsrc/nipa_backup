////////////////////////////////////////////////////////////////////////////////
//
//	ESMImgMgrIndex.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-25
//
////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "resource.h"
#include "cv.h"
#include "highgui.h"
#include "FFmpegManager.h"
#include "ESMImgMgrIndex.h"
#include "ESMDefine.h"
#include <string>       // std::string
#include <vector>       // std::vector
using namespace std;


void _SetAdjust(void *param);
void _Create3DFile(void *param);

struct stImageBuffer;

class CESMImgMgr
{
public:
	CESMImgMgr();
	virtual ~CESMImgMgr();

	int  GetEncoderClsid(const WCHAR* format, CLSID* pClsid);
	void GetScaleRate(double* pdbHorzScaling, double* pdbVertScaling);
	void InitScaleRate();
	void SetScaleRate(double dbHorzScaling, double dbVertScaling);

	int GetIntensity(IplImage* src_hlsimg);			// ����

	//-- 2013-04-25 hongsu@esmlab.com
	//-- Detect Binary Position
	//-- 2013-08-27 jaehyun@esmlab.com
	//-- Add Angle
	//BOOL GetAdjustDetect(CString strFile, CvPoint2D32f* pPoint, double* pdbAngle, double* dTargetSize, CvPoint2D32f* pRotatePoint, int* nIntensity, double dTargetDistance, int nIndex);
	//BOOL GetAdjustDetect2(CString strFile, CvPoint2D32f* pPoint, double* pdbAngle, double* dTargetSize, CvPoint2D32f* pRotatePoint, int* nIntensity, double dTargetDistance, int nIndex);
	void SetAdjust(CString strSrcFile, CString strAdjFile, CString strTargetFile, 
						CvPoint2D32f ptAdj, CvPoint2D32f ptLT, CvPoint2D32f ptRB, 
						double nAngleAdjust, double dAdjustSize, double dAdjustDistance, double dbMaxAngle, CvPoint2D32f ptRotate, CString strDSC ,
						BOOL b3D = FALSE);

	BOOL SearchPoint(IplImage*	pNewGray, vector<CPoint>* arrBeginPoint, vector<CPoint>* arrPointRange, vector<CPoint>* pArrPoint);
	BOOL MakeAvgImage(CString strTargetFolder, int nImageCount, int* arrModCamIndicater);
	BOOL AvgImageFileChange(CString strTargetFolder, int nImageCount);

	//-- 2013-09-30 jaehyun@esmlab.com
	//-- Histogram matching function
	void HistogramMatchRefValue(char* strRefFile);
	char* HistogramMatch(char* strSrcFile);

	//-- 2013-09-03 jaehyun@esmlab.com
	//-- Scale converter function
	double ToRelativePosition(double nAbsolute, int nSize);
	double ToAbsolutePosition(double nRelative, int nSize);


	void SetImageMarginX(int nMargin){m_nImageMarginX = nMargin;}
	void SetImageMarginY(int nMargin){m_nImageMarginY = nMargin;}
	int GetImageMarginX(){ return m_nImageMarginX; }
	int GetImageMarginY(){ return m_nImageMarginY; }

	//-- 2013-10-07 hongsu@esmlab.com
	//-- 3D File
	void Create3DFile(CString strAdjFileLeft, CString strAdjFileRight, CString strTargetFile);

	int AddBanner(CString strTargetFolder, int nImageCount, const vector<int>& vPLogo);
	int AddBanner2(stImageBuffer* pImageBuffer);
	void overlayImage(const cv::Mat &background, const cv::Mat &foreground, cv::Mat &output, cv::Point2i location);
	void overlayImage2(const IplImage *background, const IplImage *foreground, IplImage *output, CvPoint location);

private:
	//-- for SendMessage
	HWND m_hMainWnd;

	double	m_dbHorzScaling;
	double	m_dbVertScaling;

	//-- 2013-12-22 kcd
	//-- Margin Data
	int m_nImageMarginX;
	int m_nImageMarginY;
	//-- 2013-10-02 jaehyun@esmlab.com
	//-- Histogram matching member pointer variable
	double*	m_dbRefCDF;
	//------------------------------------------------------------------------------
	//! @brief		For Adjustment
	//! @date		2013-10-07
	//! @owner		Hongsu Jung (hongsu@esmlab.com)
	//------------------------------------------------------------------------------	

public:	
	int m_nObj;
	int m_nKind;	
	int m_nAddFile;
	CRITICAL_SECTION _ESMImageEngine;
	CRITICAL_SECTION m_CS_ESMImageAjust;

	//-- Change from CStringArray -> vector
	std::vector<CString> m_arCheckAdjust;

	void ResetAdjust(int nObj, int nKind)		{ m_arCheckAdjust.clear();	m_nAddFile = 0; m_nObj = nObj; m_nKind = nKind; }
	CString GetCheckAdjust(int nIndex);
	BOOL IsFinishAdjust();
	int AdjustGetCount();
	void AddAdjust(CString strFile);
	void FinishAdjust(CString strFile);

	void SetAdjust2( stImageBuffer* pImageBuffer);
	//static unsigned SetAdjustThread(LPVOID param);

	void SetMargin(int& nMarginX, int& nMarginY, stAdjustInfo AdjustData );
	stFPoint CESMImgMgr::GetRotatePoint(stFPoint ptCenter, stFPoint ptRot, double dbAngle);

	//wgkim 190429 Margin Process
public:
	CRect SetRectMargin(stAdjustInfo AdjustData );
	CRect GetMinimumMargin();
	void ShowMarginLog(int nMinMarginX, int nMinMarginY, CRect rtMargin);
	int m_nWidth;
	int m_nHeight;
	vector<CRect> m_vecMargin;

};

struct Adjust3DInfo
{
	CESMImgMgr* m_pImgMgr;
	CString m_strAdjFile[2], m_strTargetFile;
};