////////////////////////////////////////////////////////////////////////////////
//
//	TGImageFunc.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "StdAfx.h"
#include "TGImageFunc.h"
#include "MainFrm.h"

//-- 2010-9-8 keunbae.song
//-- Change NIVision into OpenCV
#include "cv.h"
#include "highgui.h"

CString GetIDFromFile(CString strPath)
{
	int nLocate;
	CString strID = strPath;
	nLocate = strPath.ReverseFind ('\\');

	if( nLocate >= 0 )
		strID = strPath.Right(strPath.GetLength ()-nLocate-1);

	nLocate = strID.ReverseFind ('.');

	if( nLocate >= 0 )
		strID = strID.Mid(0, nLocate);

	return strID;
}

CString GetImageExtension()
{
#ifdef _ImageExt_JPG
	return _T("jpg");
#else
	return _T("bmp");
#endif
}


CString GetUniqueFileName(CString strFilePath, CString strID)
{
	if( strFilePath.IsEmpty() )
	{
		TGLog(5, _T("[GetUniqueFileName] return....Path is NULL !!!"));
		return _T("");
	}

	TGLog(2, _T("[GetUniqueFileName] %s"), strFilePath);

	WIN32_FIND_DATA		wfd;
	HANDLE hFindFile = FindFirstFile(strFilePath, &wfd);

	if( hFindFile == INVALID_HANDLE_VALUE && strID.IsEmpty() )
		return strFilePath;

	int nCnt = 0;
	CString strUniqueName;

	CString strExt = _T("");
	CString strName = strFilePath;

	int nExtPos = strFilePath.ReverseFind('.');
	int nDirPos = strFilePath.ReverseFind('\\');
	
	if( nDirPos > nExtPos )	nExtPos = -1;
	if( nExtPos >= 0 )
	{
		strName = strFilePath.Left(nExtPos);
		strExt  = strFilePath.Right(strFilePath.GetLength()-nExtPos);
	}

	CString strFrm = _T("");
	if( strID.GetLength() > 0 )	strFrm.Format(_T("_%s"), strID);
		
	do
	{		
		FindClose(hFindFile);
		strUniqueName.Format(_T("%s%s_%d%s"), strName, strFrm, ++nCnt, strExt);
		hFindFile = FindFirstFile(strUniqueName, &wfd);		

	}while ( hFindFile != INVALID_HANDLE_VALUE );

	if(hFindFile)
	{
		FindClose(hFindFile);
	}

	return strUniqueName;	
}

//-- 2009-07-08
//-- For Memory Leak
//-- 2010-9-10 keunbae.song
//-- Change NIVision into OpenCV
//void DisposeProc(PVOID* object)
//{
//	if( *object )
//	{
//		imaqDispose(*object);
//		*object = NULL;
//	}
//}


BOOL CertificationDirecotry(CString strPath)
{
	TGLog(3, _T("[CertificationDirecotry] %S"), strPath);

	if(INVALID_FILE_ATTRIBUTES == GetFileAttributes(strPath))
	{
		if(  ::CreateDirectory(strPath, NULL) )
			TGLog(3, _T("[CertificationDirecotry] Success"));
		else
		{
			TGLog(3, _T("[CertificationDirecotry] Fail"));
			return FALSE;
		}
	}
	return TRUE;				
}

BOOL U_IsByPosition(UINT uFlags)
{
	return (uFlags & MF_BYPOSITION) ? TRUE : FALSE;
}

UINT U_GetMenuState(HMENU hMenu, UINT uId, UINT uFlags)
{
	MENUITEMINFO mii;
	memset((char *)&mii, 0, sizeof(mii));
	mii.cbSize = sizeof(mii);
	mii.fMask  = MIIM_STATE;

	if(0 == ::GetMenuItemInfo(hMenu, uId, U_IsByPosition(uFlags), &mii))
	{
		return 0;
	}

	return mii.fState;
}


BOOL EnableMenuProc(HMENU hMenu,   UINT nSubID, DWORD dwState )
{
	if( hMenu == NULL ) return FALSE;

	// state
	if(dwState == -1 ) 
	{
		dwState = U_GetMenuState(hMenu, nSubID, MF_BYCOMMAND);

		// check
		if( dwState & MF_GRAYED ) 
		{ 
			return EnableMenuItem(hMenu, nSubID, MF_BYCOMMAND | MF_ENABLED); 
		}
		else 
		{ 
			return EnableMenuItem(hMenu, nSubID, MF_BYCOMMAND | MF_GRAYED); 
		} 
	}
	else
	{
		dwState |= MF_BYCOMMAND;
		return EnableMenuItem(hMenu, nSubID, dwState); 
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////////

BOOL GetForegroundWndCapture(CString strFileFullName, HWND hForeWnd)
{
	if( hForeWnd == NULL )
		hForeWnd = GetForegroundWindow();

	if( hForeWnd == NULL ) 
	{
		TRACE(_T("[GetForegroundWndCapture] GetForegroundWindow == NULL \r\n"));	
		return FALSE;
	}

	BOOL bRst =	FALSE;
	
	CDC dc;
    HDC hdc = ::GetWindowDC(hForeWnd);
    dc.Attach(hdc);
    
    CDC dcMem;
    dcMem.CreateCompatibleDC(&dc);
    
    CRect rcWnd;
	::GetWindowRect(hForeWnd, &rcWnd);
    
    //char szTitle[512];
	TCHAR szTitle[512];
	memset(szTitle, 0x00, sizeof(szTitle));
	::GetWindowText(hForeWnd, szTitle, sizeof(szTitle));
	TRACE(_T("[GetForegroundWndCapture] %s\r\n"), szTitle);	

    CSize sizeWnd(rcWnd.Width(), rcWnd.Height());
    
	CBitmap bitmap;
	bitmap.CreateCompatibleBitmap(&dc, sizeWnd.cx, sizeWnd.cy);

    CBitmap * pOldBitmap = dcMem.SelectObject(&bitmap);
    dcMem.BitBlt(0, 0, sizeWnd.cx, sizeWnd.cy, &dc, 0, 0, SRCCOPY);

	bRst = DDB2DIB((HBITMAP)bitmap, strFileFullName);

	if( pOldBitmap )		SelectObject(dcMem,pOldBitmap);
	DeleteDC(dcMem);
	if( bitmap.m_hObject )	DeleteObject((HBITMAP)bitmap);	

	TRACE(_T("-[GetForegroundWndCapture]\r\n"));	
	return bRst;
}

///////////////////////////////////////////////////////////////////////////////
//! @comment   Not in use. 2010-9-8 keunbae.song
//BOOL SaveToJPG(CString strPath)	
//{
//	//-- 2010-9-8 keunbae.song
//	//-- Change NIVision into OpenCV
//	BOOL	bRst = FALSE;
//	Image*	tmpImage = imaqCreateImage(IMAQ_IMAGE_RGB, 0);
//
//    char templateFile[MAX_PATH];
//	strcpy(templateFile, strPath);
//    
//	if( 0 == imaqReadFile(tmpImage, templateFile, NULL, NULL))
//	    goto SaveToJPG_Error;
//
//	if( 0 == imaqWriteFile(tmpImage, templateFile, NULL) )
//		goto SaveToJPG_Error;
//	bRst = TRUE;
//
//SaveToJPG_Error:
//	if(tmpImage) 
//		imaqDispose(tmpImage);	
//    return bRst;
//}

//--
//-- FOR REPORTING
//-- 2009-06-24
//--
BOOL SaveToJPG(CString strPath, CString strJpgPath, int nWidth, int nHeight)	
{
	//-- 2010-9-8 keunbae.song
	//-- Change NIVision into OpenCV

//	BOOL	bRst = FALSE;
//	Image*	tmpImage = imaqCreateImage(IMAQ_IMAGE_RGB, 0);	
//
//	char bmpFile[MAX_PATH], jpgFile[MAX_PATH];
//	strcpy(bmpFile, strPath);
//	strcpy(jpgFile, strJpgPath);	
//    
//	//-- READ BMP File
//	if( 0 == imaqReadFile(tmpImage, bmpFile, NULL, NULL))
//	{
//		goto SaveToJPG_Error;
//	}
//
//	//-- Zero Size
//	if(nWidth == 0 || nHeight == 0)
//		imaqGetImageSize(tmpImage, &nWidth, &nHeight);
//
//	//-- RESIZE
//	if( 0 == imaqResample(tmpImage, tmpImage, nWidth, nHeight, IMAQ_BILINEAR, IMAQ_NO_RECT))				
//	{
//		goto SaveToJPG_Error;		
//	}
//
//	if( 0 == imaqWriteFile(tmpImage, jpgFile, NULL) )
//	{
//		goto SaveToJPG_Error;
//	}
//	bRst = TRUE;
//
//SaveToJPG_Error:
//	if(tmpImage)
//		imaqDispose(tmpImage);	
//    return bRst;

	BOOL bRst = FALSE;
	//Image*	tmpImage = imaqCreateImage(IMAQ_IMAGE_RGB, 0);	

	char bmpFilePath[MAX_PATH] = {0};
	char jpgFilePath[MAX_PATH] = {0};

#ifdef UNICODE
	int nRequiredSize = wcstombs(bmpFilePath, strPath, MAX_PATH); 
	nRequiredSize = wcstombs(jpgFilePath, strJpgPath, MAX_PATH); 
#else
	strcpy(bmpFilePath, strPath);
	strcpy(jpgFilePath, strJpgPath);
#endif

	//-- READ BMP File
	IplImage* bmpImage = cvLoadImage(bmpFilePath, CV_LOAD_IMAGE_UNCHANGED);
	if(NULL == bmpImage)
	{
		TGLog(1, _T("[IM] ERROR: [SaveToJPG] Read File [BMP] : [%s]"),bmpFilePath);        
		goto SaveToJPG_Error;
	}

	//-- Zero Size
	if(nWidth == 0 || nHeight == 0)
	{
		nWidth  = bmpImage->width;
		nHeight = bmpImage->height;
	}

	//-- RESIZE
	IplImage* bmpTempImage = cvCreateImage(cvSize(nWidth, nHeight), bmpImage->depth, bmpImage->nChannels);
	if(NULL == bmpTempImage)
	{
		TGLog(1, _T("[IM] ERROR: [SaveToJPG] Resize"));        
		goto SaveToJPG_Error;		
	}

	cvResize(bmpImage, bmpTempImage, CV_INTER_LINEAR); 

	//-- Write JPG File
	cvSaveImage(jpgFilePath, bmpTempImage);	
	bRst = TRUE;

SaveToJPG_Error:	
	if(bmpImage)
		cvReleaseImage(&bmpImage);
	if(bmpTempImage)
		cvReleaseImage(&bmpTempImage);

	return bRst;
}

BOOL ResizeFile(CDC *pDC, CString strOrgFullName, CString strNewFullName, int nX, int nY, int nWidth, int nHeight)
{
	HDC hDC = pDC->GetSafeHdc();

	// File Load ( DIB2DDB)
	HBITMAP hBitmap = MakeDDBFromDIB(hDC, strOrgFullName);

	HDC dcMem;
	HBITMAP OldBitmap;
	int bx,by;
	BITMAP bit;
	
	dcMem=CreateCompatibleDC(hDC);
	OldBitmap=(HBITMAP)SelectObject(dcMem, hBitmap);

	GetObject(hBitmap,sizeof(BITMAP),&bit);
	bx=bit.bmWidth/2;
	by=bit.bmHeight/2;

	HDC hCopyDC = CreateCompatibleDC(hDC);
	HBITMAP hCopyBitmap = CreateCompatibleBitmap(dcMem,nWidth,nHeight);
	HBITMAP hbmOld = (HBITMAP)SelectObject(hCopyDC,hCopyBitmap);
	BitBlt( hCopyDC , 0 , 0 , nWidth , nHeight , dcMem , nX , nY , SRCCOPY );

	SelectObject( hCopyDC , hbmOld );
	DeleteDC( hCopyDC );

	SelectObject(dcMem,OldBitmap);
	DeleteDC(dcMem);

	BOOL bRst = DDB2DIB((HBITMAP)hCopyBitmap, strNewFullName);

	DeleteObject(hCopyBitmap);
	return TRUE;
	
}

HBITMAP MakeDDBFromDIB(HDC hdc, CString strPath)
{
	HANDLE	hFile;
	DWORD	dwFileSize, dwRead;
	PBYTE	pData;
	HBITMAP hBitmap;

	hFile=CreateFile(strPath, GENERIC_READ, 0, NULL, OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if ( hFile==INVALID_HANDLE_VALUE )
		return NULL;

	dwFileSize=GetFileSize(hFile,NULL);
	if ( dwFileSize == 0 ) 
		return NULL;

	pData = new BYTE[dwFileSize];
	ReadFile(hFile, pData, dwFileSize, &dwRead, NULL);
    CloseHandle(hFile);

	hBitmap = CreateDIBitmap(	hdc,
								(BITMAPINFOHEADER *)(pData+sizeof(BITMAPFILEHEADER)),
								CBM_INIT, 
								((BITMAPFILEHEADER *)pData)->bfOffBits + pData  ,
								(BITMAPINFO *)(pData+sizeof(BITMAPFILEHEADER)), // color-format data
								DIB_RGB_COLORS	);	// color-data usage
	delete[] pData;
	return hBitmap;
}

//! @comment   Not in use. 2010-9-8 keunbae.song
//BOOL GetScreenCapture(CString strFileFullName)					   
//{
//	TRACE(_T("+[GetScreenCapture] %s\r\n"), strFileFullName);	
//	if( strFileFullName.IsEmpty() ) return FALSE;
//	
//	BOOL bRst = FALSE;
//
//	// 현재 화면 캡처.
//	CRect rect;
//	rect.top = rect.left = 0;
//	rect.right = ::GetSystemMetrics(SM_CXSCREEN);
//	rect.bottom = ::GetSystemMetrics(SM_CYSCREEN);
//
//	CBitmap bitmap;
//	CBitmap* pOldBitmap = NULL;
//	CDC dcMem, dcScreen;
//	
//	if( FALSE == dcScreen.CreateDC("DISPLAY", NULL, NULL, NULL) )
//		return FALSE;
//
//	if( FALSE == dcMem.CreateCompatibleDC(&dcScreen) )
//		return FALSE;
//	
//	if( !bitmap.CreateCompatibleBitmap(&dcScreen,rect.Width(),rect.Height()))
//		goto GetScreenCapture_Error;
//
//	pOldBitmap = dcMem.SelectObject(&bitmap);
//	dcMem.BitBlt(0,0,rect.Width(),rect.Height(),&dcScreen,rect.left ,rect.top ,SRCCOPY );	
//
//	if( 0 == strFileFullName.Right(3).CompareNoCase(_T("JPG"))) 
//	{
//		if( DDB2DIB((HBITMAP)bitmap, strFileFullName) )
//			bRst = SaveToJPG(strFileFullName);
//	}
//	else
//		bRst = DDB2DIB((HBITMAP)bitmap, strFileFullName);
//
//GetScreenCapture_Error:
//
//	if( pOldBitmap )
//		SelectObject(dcMem,pOldBitmap);
//
//	DeleteDC(dcMem);
//
//	if( bitmap.m_hObject )
//		DeleteObject((HBITMAP)bitmap);	
//
//	TRACE(_T("-[GetScreenCapture]\r\n"));	
//	return bRst;
//}


// DDB를 DIB로 바꾸어 파일로 저장한다.
BOOL DDB2DIB(HBITMAP hbit,CString strPath)
{
	BOOL bRst = FALSE;
    BITMAPFILEHEADER fh;
    BITMAPINFOHEADER ih;
    BITMAP bit;
    BITMAPINFO *pih;
    int PalSize;
    HANDLE hFile;
    DWORD dwWritten,Size;
    HDC hdc;

    // 전체 화면에 대한 DC를 구한다.
	hdc=::GetDC(NULL);

    // 비트맵 정보로부터 정보 구조체를 초기화한다.
    GetObject(hbit,sizeof(BITMAP),&bit);
    ih.biSize=sizeof(BITMAPINFOHEADER);
    ih.biWidth=bit.bmWidth;
    ih.biHeight=bit.bmHeight;
    ih.biPlanes=1;
    ih.biBitCount=bit.bmPlanes*bit.bmBitsPixel;
    if (ih.biBitCount > 8) ih.biBitCount=24;
    ih.biCompression=BI_RGB;
    ih.biSizeImage=0;
    ih.biXPelsPerMeter=0;
    ih.biYPelsPerMeter=0;
    ih.biClrUsed=0;
    ih.biClrImportant=0;

    // 정보 구조체 + 팔레트 크기만큼 메모리를 할당하고 이 버퍼에 정보 구조체를 복사한다.
    PalSize=(ih.biBitCount==24 ? 0:1 << ih.biBitCount)*sizeof(RGBQUAD);
    pih=(BITMAPINFO *)malloc(ih.biSize+PalSize);
	if( pih == NULL )
	{
		MessageBox(NULL,_T("Not enough memory for requested operation."), _T("Error"), MB_OK | MB_ICONEXCLAMATION);
		goto DDB2DIB_Error;
	}

    pih->bmiHeader=ih;

    // 비트맵의 크기를 구한다.
    GetDIBits(hdc,hbit,0,bit.bmHeight,NULL,pih,DIB_RGB_COLORS);
    ih=pih->bmiHeader;

    // 비트맵 크기가 구해지지 않았을 경우 수작업으로 직접 계산한다.
    if (ih.biSizeImage == 0) {
        ih.biSizeImage=((((ih.biWidth*ih.biBitCount)+31) & ~31) >> 3) * ih.biHeight;
    }

    // 래스터 데이터를 읽기위해 메모를 재할당한다.
    Size=ih.biSize+PalSize+ih.biSizeImage;
    pih=(BITMAPINFO *)realloc(pih,Size);
	if( pih == NULL )
	{
		MessageBox(NULL,_T("Not enough memory for requested operation."), _T("Error"), MB_OK | MB_ICONEXCLAMATION);
		goto DDB2DIB_Error;
	}

    // 래스터 데이터를 읽어들인다.	
    GetDIBits(hdc,hbit,0,bit.bmHeight,(PBYTE)pih+ih.biSize+PalSize,pih,DIB_RGB_COLORS);

    // 파일 헤더를 만든다.
    fh.bfOffBits=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+PalSize;
    fh.bfReserved1=0;
    fh.bfReserved2=0;
    fh.bfSize=Size+sizeof(BITMAPFILEHEADER);
    fh.bfType=0x4d42;

    // 파일을 생성하고 파일 헤더와 정보 구조체, 팔레트, 래스터 데이터를 출력한다.
    hFile=CreateFile(	strPath,	GENERIC_WRITE, 0, NULL,
						CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);

	if( hFile !=  INVALID_HANDLE_VALUE )
	{
		if( WriteFile(hFile,&fh,sizeof(fh),&dwWritten,NULL) )
			bRst = WriteFile(hFile,pih,Size,&dwWritten,NULL);
		CloseHandle(hFile);
	}


DDB2DIB_Error:

	if(pih)
		free(pih);
	::ReleaseDC(NULL,hdc);
	return TRUE;
}

// 특문들어 있는 경우, Project Wizard에서 저장 못한다. 
CString GetEraseSpecialChar(CString str)
{
	str.Trim();
	str.Replace(_T("\\"),		_T("_"));
	str.Replace(_T("/"),		_T("_"));
	str.Replace(_T(":"),		_T("_"));
	str.Replace(_T("*"),		_T("_"));
	str.Replace(_T("?"),		_T("_"));
	str.Replace(_T("\""),		_T("_"));
	str.Replace(_T("|"),		_T("_"));
	str.Replace(_T(" "),		_T("_"));
	str.Replace(_T("<"),		_T("_"));
	str.Replace(_T(">"),		_T("_"));
	return str;
}

//-- 2012-07-27 hongsu@esmlab.com
IplImage* TGResizeImage(IplImage* pImg, int nWidth, int nHeight)
{
	IplImage* pResizeImg = NULL;	
	pResizeImg = cvCreateImage(cvSize(nWidth, nHeight), pImg->depth, pImg->nChannels);
	cvResize(pImg, pResizeImg, CV_INTER_LINEAR);
	return pResizeImg;
}	

CvPoint TGScalePoint (CvPoint pt, float nScaleWidth, float nScaleHeight)
{
	CvPoint pt_change;
	pt_change.x = pt.x / nScaleWidth;
	pt_change.y = pt.y / nScaleHeight;
	return pt_change;
}