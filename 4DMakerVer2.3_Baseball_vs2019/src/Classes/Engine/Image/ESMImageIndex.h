////////////////////////////////////////////////////////////////////////////////
//
//	TGImageIndex.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-11
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

enum
{
	COLOR_R		= 0,
	COLOR_G		,
	COLOR_B		,
	COLOR_ALL,
};

enum
{
	RANGE_MIN		= 0,
	RANGE_MAX		,		
	RANGE_ALL,
};

enum
{
	POINT_LEFT_TOP	= 0,
	//POINT_RIGHT_TOP		,
	POINT_RIGHT_BOTTOM	,
	//POINT_LEFT_BOTTOM	,
	POINT_ALL,
};


#define	TG_COLOR_DETECT_WIDTH			300
#define	TG_COLOR_DETECT_HEIGHT			200

#define TG_BASE_COLOR_TOLERANCE			40

#define TG_IMAGE_DETECT_RANGE_NON_MIN	0
#define TG_IMAGE_DETECT_RANGE_NON_MAX	120
#define TG_IMAGE_DETECT_RANGE_ON_MIN	130
#define TG_IMAGE_DETECT_RANGE_ON_MAX	255

#define TG_IMAGE_DETECT_PERCENTAGE_TOLERANCE	20
