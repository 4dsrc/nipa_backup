////////////////////////////////////////////////////////////////////////////////
//
//	ESMThinning.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-124
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "cv.h"
#include "highgui.h"

class CESMThinning
{
public:
	CESMThinning(void);
public:
	~CESMThinning(void);
public:	
	void	DeleteA(char* img, char* timg,int cx,int cy, int widthStep) ;
	int		nays(char* img, int i, int j, int widthStep) ;
	int		Connect(char* img, int i, int j, int widthStep);
	IplImage*	Thin(IplImage* InImage) ;	
};


