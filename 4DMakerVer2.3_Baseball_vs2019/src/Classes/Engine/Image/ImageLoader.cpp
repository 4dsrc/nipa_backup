#include "StdAfx.h"
#include "ESMFunc.h"
#include "ImageLoader.h"
#include "DSCItem.h"
#include "ESMFileOperation.h"

//-- 2015-03-30 cygil@esmlab.com ImageLoader �߰�
CImageLoader::CImageLoader(void)
{
	AllClearImageList();
}

CImageLoader::~CImageLoader(void)
{
	AllClearImageList();
}

BOOL CImageLoader::SetImageFromMovie(CString strMovieFile, int nStartTime, int nEndTime)
{
	if(m_mapImageList.find(strMovieFile) != m_mapImageList.end())
		ClearImageList(strMovieFile);

	vector<ST_IMAGELIST> iplImageList;
	FFmpegManager FFmpegMgr(FALSE);

	iplImageList.clear();
	int nListCount = nEndTime - nStartTime + 1;
	CString strFileName;


	//-- Get Frame Path
	if(ESMGetFrameRecord().GetLength())
	{
		if(ESMGetExecuteMode())
		{
			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);
			int nSelectDsc = ESMGetDSCIndex(strMovieFile);
			CDSCItem* pItem = (CDSCItem*)arDSCList.GetAt(nSelectDsc);
			CString strDscIp = pItem->GetDSCInfo(DSC_INFO_LOCATION);
			strFileName.Format(_T("%s\\%s.mp4"), ESMGetClientBackupRamPath(strDscIp), strMovieFile);
		}
		else
			strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), strMovieFile);
	}
	BOOL bRet = FFmpegMgr.GetCaptureImage(strFileName, iplImageList, nStartTime, nListCount);
	if(bRet)
		m_mapImageList.insert(make_pair(strMovieFile, iplImageList));
	return bRet;
}

BOOL CImageLoader::IsMovieFile(int nDscIndex, int nSelectFIndex)
{
	CString strFileName;

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	if( arDSCList.GetCount() <=  nDscIndex)
		return FALSE;
	CDSCItem* pItem = (CDSCItem*)arDSCList.GetAt(nDscIndex);

	if( pItem == NULL)
		return FALSE;

	if(ESMGetFrameRecord().GetLength())
	{
		if(ESMGetExecuteMode())
		{
			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);
			CString strDscIp = pItem->GetDSCInfo(DSC_INFO_LOCATION);
			//strFileName.Format(_T("%s\\%s.mp4"), ESMGetClientBackupRamPath(strDscIp), pItem->GetDeviceDSCID());
			strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nSelectFIndex);
		}
		else
			strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), pItem->GetDeviceDSCID());
	}
	CESMFileOperation fo;
	if(fo.IsFileExist(strFileName))
		return TRUE;
	else
		return FALSE;
}

BOOL CImageLoader::IsImageFromList(CString strMovieFile, int nCheckTime)
{
	map<CString, vector<ST_IMAGELIST>>::iterator itmapList = m_mapImageList.find(strMovieFile);
	if(itmapList != m_mapImageList.end())
	{
		vector<ST_IMAGELIST> vecImageList = itmapList->second;
		for(int nCnt = 0; nCnt < vecImageList.size(); nCnt++)
		{
			if(vecImageList[nCnt].nFrameIndex == nCheckTime)
				return TRUE;
		}
	}
	return FALSE;
}
CBitmap* CImageLoader::GetImageFromList(CString strMovieFile, int nGetTime, CDC* pDC, int nWidth, int nHeight)
{
	CBitmap* pImage = NULL;
	map<CString, vector<ST_IMAGELIST>>::iterator itmapList = m_mapImageList.find(strMovieFile);
	if(itmapList != m_mapImageList.end())
	{
		vector<ST_IMAGELIST> vecImageList = itmapList->second;
		for(int nCnt = 0; nCnt < vecImageList.size(); nCnt++)
		{
			if(vecImageList[nCnt].nFrameIndex == nGetTime)
			{
				pImage = new CBitmap();
  				pImage->CreateCompatibleBitmap(pDC, nWidth, nHeight);

				IplImage* iplImage = cvCreateImage(cvSize(nWidth, nHeight), vecImageList[nCnt].ImagePtr->depth, vecImageList[nCnt].ImagePtr->nChannels);
				cvResize(vecImageList[nCnt].ImagePtr, iplImage, 1);
				BYTE* pSrcBits = (BYTE*)(iplImage->imageData);
				
				BYTE* pBmpBits = (BYTE*)calloc(sizeof(BYTE), nWidth*nHeight*4);

				for(int h=0; h<nHeight; h++)
				{
					BYTE* pSrc = pSrcBits + iplImage->widthStep * h;
					BYTE* pDst = pBmpBits + nWidth * 4 * h;
					for(int w=0; w<nWidth; w++)
					{
						*(pDst++) = *(pSrc++);
						*(pDst++) = *(pSrc++);
						*(pDst++) = *(pSrc++);
						*(pDst++) = 0;
					}
				}
				pImage->SetBitmapBits(nWidth*nHeight*4, pBmpBits);
				cvReleaseImage( &iplImage );
				free(pBmpBits);
			}
		}
	}
	return pImage;
}

void CImageLoader::ClearImageList(CString strMovieFile)
{
	map<CString, vector<ST_IMAGELIST>>::iterator itmapList = m_mapImageList.find(strMovieFile);
	if(itmapList != m_mapImageList.end())
	{
		vector<ST_IMAGELIST> vecImageList = itmapList->second;
		for(int nCnt = 0; nCnt < vecImageList.size(); nCnt++)
		{
			cvReleaseImage( &vecImageList[nCnt].ImagePtr );
		}
		vecImageList.clear();
		m_mapImageList.erase(strMovieFile);
	}
}

void CImageLoader::AllClearImageList()
{
	if(m_mapImageList.size() > 0)
	{
		map<CString, vector<ST_IMAGELIST>>::iterator itmapList = m_mapImageList.begin();
		for(; itmapList != m_mapImageList.end(); ++ itmapList)
		{
			vector<ST_IMAGELIST> vecImageList = itmapList->second;
			for(int nCnt = 0; nCnt < vecImageList.size(); nCnt++)
			{
				cvReleaseImage( &vecImageList[nCnt].ImagePtr);
			}
			vecImageList.clear();
		}
		m_mapImageList.clear();
	}
}