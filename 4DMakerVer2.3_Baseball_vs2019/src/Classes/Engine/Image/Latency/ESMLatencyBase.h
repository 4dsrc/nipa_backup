////////////////////////////////////////////////////////////////////////////////
//
//	TGLatencyBase.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	joonho.kim
// @Date	2012-08-24
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

//==============================================================================
//	include headers
//==============================================================================
#include "TGFunc.h"	//-- TGArray

class CTGLatencyBase;

class CTGLatencyBase
{
public:
	CTGLatencyBase() {};
	CTGLatencyBase(CString strTitle);
	virtual ~CTGLatencyBase ();
	
	int m_nType;
	CString m_strTitle;
	CTGArray m_arChild;
	CStringArray m_arTimeData;
	CStringArray m_arPercentData;
public:
	
	int AddChild(CTGLatencyBase* pchild);
	int AddTime(CString strTime );
	int AddPercent(float nPercent );
	int GetChildCount();	

	CTGLatencyBase* GetChild(int nIndex);				
	CTGLatencyBase* GetChild(CString strChild);

	void RemoveAllChild();
};


