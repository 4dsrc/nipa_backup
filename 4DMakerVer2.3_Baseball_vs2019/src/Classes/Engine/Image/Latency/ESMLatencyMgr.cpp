////////////////////////////////////////////////////////////////////////////////
//
//	TGLatencyMgr.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	joonho.kim
// @Date	2012-08-24
//
////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------
//-- include headers
//---------------------------------------------------------------------------
#include "StdAfx.h"
#include "TGLatencyMgr.h"
#include "TGLatencyCodec.h"
#include "TGLatencyResolution.h"
#include "TGLatencyFramerate.h"
#include "TGLatencyColor.h"

CTGLatencyMgr::CTGLatencyMgr(void)
{
}

CTGLatencyMgr::~CTGLatencyMgr(void)
{
	RemoveAllChild();
}

void CTGLatencyMgr::AddLatencyData(LatencyResult* pNew)
{
	AddResultData( pNew );
}

LatencyResult  CTGLatencyMgr::GetResultData( LatencyResult* pNew )
{
	CStringArray arTime;
	CString strTime , strTemp;
	LatencyResult			Result;
	CTGLatencyIP*			pIP			= NULL;
	CTGLatencyCodec*		pCodec		= NULL;
	CTGLatencyResolution*	pResolution = NULL;
	CTGLatencyFramerate*	pFrame		= NULL;
	CTGLatencyColor*		pColor		= NULL;
	int nCount = 0;
	int nSec = 0, nMilli = 0, nTemp = 0, nSum = 0, nGap = 0;
	float nSumPercent = 0;

	pIP = (CTGLatencyIP*)GetChild( pNew->strIP );	
	pCodec = (CTGLatencyCodec*)pIP->GetChild( pNew->strCodec );
	pResolution = (CTGLatencyResolution*)pCodec->GetChild( pNew->strResolution );
	pFrame = (CTGLatencyFramerate*)pResolution->GetChild( pNew->strFramerate );
	pColor = (CTGLatencyColor*)pFrame->GetChild( pNew->strColor );
	
	Result.nCount = pColor->GetTimeCount();
	if(Result.nCount > 1)
	{
		for(int nTime = 0; nTime < Result.nCount; nTime++)
		{
			strTime = pColor->GetTime(nTime);
			strTime.Replace(_T("["),_T(""));
			strTime.Replace(_T("]"),_T(""));
			strTemp = strTime.Left(2);
			nSec = _ttoi(strTemp);
			strTemp = strTime.Right(3);
			nMilli = _ttoi(strTemp);
			nSum += ((nSec*1000) + nMilli);	

			strTime.Format(_T("%d"), (nSec*1000) + nMilli);
			arTime.Add(strTime);

			nSumPercent += _tstof(pColor->GetPercent(nTime));
		}
		nSum = nSum/Result.nCount;
		nSec = nSum / 1000;
		nMilli = nSum - (nSec*1000);

		strTemp.Format(_T("%02d.%03d s"), nSec, nMilli );
		Result.strTime = strTemp;

		nSum = 0;
		for(int nDeviation = 0; nDeviation < Result.nCount-1; nDeviation++)
		{
			nGap = _ttoi(arTime.GetAt(nDeviation)) - _ttoi(arTime.GetAt(nDeviation+1));
			nSum += (nGap * nGap);
		}
		nSum = (int)sqrt((double)(nSum / (Result.nCount-1)));
		nSec = nSum / 1000;
		nMilli = nSum - (nSec*1000);

		strTemp.Format(_T("%02d.%03d s"), nSec, nMilli );
		Result.strDeviation = strTemp;
		Result.dbPercentage = nSumPercent / Result.nCount;
	}
	else
	{
		Result.strTime = pNew->strTime;
		Result.strTime.Replace(_T("["),_T(""));
		Result.strTime.Replace(_T("]"),_T(""));
		Result.strTime += _T(" s");

		Result.dbPercentage = pNew->dbPercentage;
		Result.strDeviation.Format(_T("00.000 s"));
		nSumPercent = pNew->dbPercentage;
	}
	Result.strColor = pNew->strColor;
	return Result;
}
void CTGLatencyMgr::AddResultData( LatencyResult* pNew )
{
	CTGLatencyIP*			pIPCheck	= NULL;
	CTGLatencyIP*			pIP			= NULL;
	CTGLatencyCodec*		pCodec		= NULL;
	CTGLatencyResolution*	pResolution = NULL;
	CTGLatencyFramerate*	pFrame		= NULL;
	CTGLatencyColor*		pColor		= NULL;

	pIPCheck = (CTGLatencyIP*)GetChild( pNew->strIP );
	if(pIPCheck == NULL)
	{
		pIP = new CTGLatencyIP( pNew->strIP );
		CTGLatencyBase::AddChild((CTGLatencyBase*) pIP );	
		pCodec = new CTGLatencyCodec( pNew->strCodec );
		pIP->AddChild((CTGLatencyBase*) pCodec );
		pResolution = new CTGLatencyResolution( pNew->strResolution );
		pCodec->AddChild((CTGLatencyBase*) pResolution );
		pFrame = new CTGLatencyFramerate( pNew->strFramerate );
		pResolution->AddChild((CTGLatencyBase*) pFrame );
		pColor = new CTGLatencyColor( pNew->strColor );
		pFrame->AddChild((CTGLatencyBase*) pColor );
	}
	else
	{
		pCodec = (CTGLatencyCodec*)pIPCheck->GetChild( pNew->strCodec );
		if(pCodec == NULL)
		{
			pCodec = new CTGLatencyCodec( pNew->strCodec );
			pIPCheck->AddChild((CTGLatencyBase*) pCodec );
			pResolution = new CTGLatencyResolution( pNew->strResolution );
			pCodec->AddChild((CTGLatencyBase*) pResolution );
			pFrame = new CTGLatencyFramerate( pNew->strFramerate );
			pResolution->AddChild((CTGLatencyBase*) pFrame );
			pColor = new CTGLatencyColor( pNew->strColor );
			pFrame->AddChild( pColor );
			
		}
		else
		{
			pResolution = (CTGLatencyResolution*)pCodec->GetChild( pNew->strResolution );
			if(pResolution == NULL )
			{
				pResolution = new CTGLatencyResolution( pNew->strResolution );
				pCodec->AddChild((CTGLatencyBase*) pResolution );
				pFrame = new CTGLatencyFramerate( pNew->strFramerate );
				pResolution->AddChild((CTGLatencyBase*) pFrame );
				pColor = new CTGLatencyColor( pNew->strColor );
				pFrame->AddChild((CTGLatencyBase*) pColor );	
			}
			else
			{
				pFrame = (CTGLatencyFramerate*)pResolution->GetChild( pNew->strFramerate );
				if(pFrame == NULL )
				{
					pFrame = new CTGLatencyFramerate( pNew->strFramerate );
					pResolution->AddChild((CTGLatencyBase*) pFrame );
					pColor = new CTGLatencyColor( pNew->strColor );
					pFrame->AddChild((CTGLatencyBase*) pColor );
					
				}
				else
				{
					pColor = (CTGLatencyColor*)pFrame->GetChild( pNew->strColor );
					if(pColor == NULL)
					{	
						pColor = new CTGLatencyColor( pNew->strColor );
						pFrame->AddChild((CTGLatencyBase*) pColor );
					}
				}
			}
		}
	}
	pColor->AddTime( pNew->strTime );
	pColor->AddPercent( pNew->dbPercentage );	
}