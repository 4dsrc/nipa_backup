////////////////////////////////////////////////////////////////////////////////
//
//	TGLatencyBase.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	joonho.kim
// @Date	2012-08-24
//
////////////////////////////////////////////////////////////////////////////////


//==============================================================================
//	include headers
//==============================================================================
#include "stdafx.h"
#include "TGLatencyBase.h"


//==============================================================================
//	Constructor/Destructor
//==============================================================================
CTGLatencyBase::CTGLatencyBase(CString strTitle) 
{
	m_strTitle	= strTitle;
}

CTGLatencyBase::~CTGLatencyBase() 
{
	RemoveAllChild();
}

//------------------------------------------------------------------------------ 
//! @brief	  Child Get Pointer
//! @date	  2012-08-27
//! @owner    Joonho.kim
//------------------------------------------------------------------------------ 
CTGLatencyBase* CTGLatencyBase::GetChild(CString strChild)
{
	CTGLatencyBase* pChild = NULL;
	int nAll = GetChildCount();
	while(nAll--)
	{
		pChild = GetChild(nAll);
		if(!pChild)
			break;

		if(pChild->m_strTitle == strChild)
			return pChild;
	}
	return NULL;
}
//------------------------------------------------------------------------------ 
//! @brief	  Child Add Pointer
//! @date	  2012-08-27
//! @owner    Joonho.kim
//------------------------------------------------------------------------------ 
int CTGLatencyBase::AddChild(CTGLatencyBase* pchild)
{
	return m_arChild.Add((CObject*)pchild);
}

//------------------------------------------------------------------------------ 
//! @brief	  Child Add Time
//! @date	  2012-08-27
//! @owner    Joonho.kim
//------------------------------------------------------------------------------ 
int CTGLatencyBase::AddTime(CString strTime)
{
	return m_arTimeData.Add( strTime );
}

//------------------------------------------------------------------------------ 
//! @brief	  Child Add Percentage
//! @date	  2012-08-27
//! @owner    Joonho.kim
//------------------------------------------------------------------------------ 
int CTGLatencyBase::AddPercent(float nPercent)
{
	CString strData;
	strData.Format(_T("%.03f"), nPercent );
	return m_arPercentData.Add( strData );
}
//------------------------------------------------------------------------------ 
//! @brief	  Child Get Count
//! @date	  2012-08-27
//! @owner    Joonho.kim
//------------------------------------------------------------------------------ 
int CTGLatencyBase::GetChildCount()
{
	int nRet =  (int)m_arChild.GetCount();
	return nRet;
}
//------------------------------------------------------------------------------ 
//! @brief	  Child Get Pointer
//! @date	  2012-08-27
//! @owner    Joonho.kim
//------------------------------------------------------------------------------ 
CTGLatencyBase* CTGLatencyBase::GetChild(int nIndex)	
{
	CTGLatencyBase* pBase = NULL;

	if(m_arChild.GetCount())
		pBase = (CTGLatencyBase*)m_arChild.GetAt(nIndex);
	
	return pBase;
}
//------------------------------------------------------------------------------ 
//! @brief	  Child Remove All Pointer
//! @date	  2012-08-27
//! @owner    Joonho.kim
//------------------------------------------------------------------------------ 
void CTGLatencyBase::RemoveAllChild()
{
	//-- Clear All List
	CTGLatencyBase* pExist = NULL;
	int nAll = GetChildCount();
	while(nAll--)
	{
		pExist = GetChild(nAll);
		if( pExist)
		{
			delete pExist;
			pExist = NULL;
			m_arChild.RemoveAt(nAll);
		}
	}
	if(m_arChild.GetCount())
		m_arChild.RemoveAll();
}