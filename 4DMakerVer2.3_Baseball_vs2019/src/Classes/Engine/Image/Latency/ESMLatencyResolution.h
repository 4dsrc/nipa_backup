////////////////////////////////////////////////////////////////////////////////
//
//	TGLatencyResolution.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	joonho.kim
// @Date	2012-08-24
//
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "TGLatencyIndex.h"
#include "TGLatencyBase.h"

//---------------------------------------------------------------------------
//-- Profile Array Data class
//--------------------------------------------------------------------------
class CTGLatencyResolution : public CTGLatencyBase
{
public:
	CTGLatencyResolution(CString strTitle);
	virtual ~CTGLatencyResolution ();
};


