////////////////////////////////////////////////////////////////////////////////
//
//	TGLatencyIP.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	joonho.kim
// @Date	2012-08-24
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TGLatencyIP.h"
#include "TGLatencyIndex.h"

CTGLatencyIP::CTGLatencyIP(CString strModel) 
{
	m_strTitle	= strModel;	
}

CTGLatencyIP::~CTGLatencyIP() 
{
	RemoveAllChild();
}

