////////////////////////////////////////////////////////////////////////////////
//
//	TGLatencyMgr.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	joonho.kim
// @Date	2012-08-24
//
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "TGFunc.h"
#include "TGLatencyIndex.h"
#include "TGLatencyBase.h"

#include "TGLatencyIP.h"



class CTGLatencyMgr : public CTGLatencyBase
{
public:
	CTGLatencyMgr(void);
	~CTGLatencyMgr(void);

	

public:
	LatencyResult GetResultData( LatencyResult* pNew );
	void AddLatencyData(LatencyResult* pNew);
	void AddResultData( LatencyResult* pNew );
};

