#pragma once

#define MAXIMAGEREVICOUNT 1000
#include <vector>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/imgproc_c.h"

#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"

using namespace cv;
using namespace std;
#define IMAGEHIGHTDIVEDECOUNT	100
#define IMAGEWIDTHDIVIDECOUNT	100

class ImageRevisionMgr
{
public:

	struct stRGB{
		int nRed;
		int nGreen;
		int nBlue;
	};

	ImageRevisionMgr(void);
	~ImageRevisionMgr(void);

	double m_nRevisionRed[256];
	double m_nRevisionGreen[256];
	double m_nRevisionBlue[256];
	CRITICAL_SECTION crFileOpen;

	CString m_strRefImage, m_strTarImage;
	int m_nMatchingPoitCount;
	BOOL m_bMatchaingView;
	HANDLE m_hMakeObjMovie[1000];
	int m_nThreadCount;
	BOOL PorcessRevision(CString strRefImage, CString strTarImage, int nDegree);

	BOOL GetPointRGB(CString strRefImage, CString strTarImage, vector<CPoint>* arrSiftRefPoint, vector<CPoint>* arrSiftTarPoint, vector<stRGB>* pArrRefPointRGB, vector<stRGB>* pArrTarPointRGB);
	BOOL GetRST(vector<stRGB>* pArrTarPointRGB, vector<stRGB>* pArrRefPointRGB, int nDegree, double dRgbLSTSQData[][MAXIMAGEREVICOUNT]);
	BOOL MakeTable(int nDegree, double dRgbLSTSQData[][MAXIMAGEREVICOUNT]);
	int AdjustImage(CString strTarImage, CString strOutImage);			// RBG Table 만들기
	int AdjustImage(IplImage*	pMoveImage, CString strOutImage, int nMarginX, int nMarginY);			// RBG Table 만들기
	int AdjustImage(IplImage*	pMoveImage, int nMarginX = 0, int nMarginY = 0);
	BOOL GetSIFTPoint(CString strRefImage, CString strTarImage, vector<CPoint>* arrSiftRefPoint, vector<CPoint>* arrSiftTarPoint);
	int lstsq(double *x,double *y,int m,int n,double *c);

	BOOL MakeAvaregeImage(CString strTarImage, CString strCompareImage1, CString strCompareImage2, int nXMargin, int nYMargin);
	BOOL MakeAvaregeImage(IplImage* pCvImage, IplImage* pCvCompareImage1, IplImage* pCvCompareImage2, int nXMargin, int nYMargin);
	BOOL GetAvgRGB(CString strImage, int nHightDivCount, int nWidhtDivCount, 
		int nAvgBlue[][IMAGEWIDTHDIVIDECOUNT], int nAvgRed[][IMAGEWIDTHDIVIDECOUNT], int nAvgGreen[][IMAGEWIDTHDIVIDECOUNT] , int nXMargin, int nYMargin);
	BOOL GetAvgRGB(IplImage*	pPicture, int nHightDivCount, int nWidhtDivCount, 
		int nAvgBlue[][IMAGEWIDTHDIVIDECOUNT], int nAvgRed[][IMAGEWIDTHDIVIDECOUNT], int nAvgGreen[][IMAGEWIDTHDIVIDECOUNT] , int nXMargin, int nYMargin);
	BOOL MakeResultImage(CString strImage, int nHightDivCount, int nWidhtDivCount, 
		int nAvgBlue[][IMAGEWIDTHDIVIDECOUNT], int nAvgRed[][IMAGEWIDTHDIVIDECOUNT], int nAvgGreen[][IMAGEWIDTHDIVIDECOUNT] , int nXMargin, int nYMargin );
	BOOL MakeResultImage(IplImage*	pPicture, int nHightDivCount, int nWidhtDivCount, 
		int nAvgBlue[][IMAGEWIDTHDIVIDECOUNT], int nAvgRed[][IMAGEWIDTHDIVIDECOUNT], int nAvgGreen[][IMAGEWIDTHDIVIDECOUNT] , int nXMargin, int nYMargin );
	BOOL LoadRGBData(CString strInputData);
	BOOL SaveRGBData(CString strOutputData);

	double* GetRedTable() { return m_nRevisionRed; }
	double* GetGreenTable() { return m_nRevisionGreen; }
	double* GetBlueTable() { return m_nRevisionBlue;}
	int GetMatchingCount() { return m_nMatchingPoitCount; }
	void SetMatchingView(BOOL bMatchingView) { m_bMatchaingView = bMatchingView; }

	static unsigned WINAPI MakeAvaregeThread(LPVOID param);
	void SetImageCount(int nImageCount);
	void CallMakeAvaregeImage(CString strTarImage, CString strCompareImage1, CString strCompareImage2, int nXMargin, int nYMargin);


	void GetFileNameAndPath(CString strFullPath, CString &FileName,CString &strPath);

};

