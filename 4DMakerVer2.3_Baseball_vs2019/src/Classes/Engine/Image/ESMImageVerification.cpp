////////////////////////////////////////////////////////////////////////////////
//
//	TGImageVerification.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-10
//
//			Memory Leak
//			cvCreateImage	-> cvReleaseImage
//			cvLoadImage		-> cvReleaseImage
//			cvCloneImage	-> cvReleaseImage
//			cvQueryImage	-> cvReleaseImage
//
//			cvCreateCameraCapture	-> cvReleaseCapture
//			cvCaptureFramCAM		-> cvReleaseCapture
//
//			cvCreateMemStorage	-> cvReleaseMemStorage
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TGImageVerification.h"
#include "TGImageFunc.h"
#include "TGUtil.h"

//-- 2012-08-23 hongsu@esmlab.com
//-- For Latency Report
#include "TGLatencyResultListCtrl.h"
//-- For Get Color String 
#include "TestStepNetDef.h"

#include <atlimage.h>

CTGImageVerification::CTGImageVerification()	
{ 
	m_nImgResultType = 0; 
	m_dwSequence = 0;
	
	m_ImgResult.bCheck		= FALSE;
	m_ImgResult.bPreRGB		= TRUE;
	m_ImgResult.bPreTBRvs	= TRUE;
	m_ImgResult.bPreLRRvs	= TRUE;
	m_ImgResult.bAfrRGB		= TRUE;
	m_ImgResult.bAfrTBRvs	= TRUE;
	m_ImgResult.bAfrLRRvs	= TRUE;

}
CTGImageVerification::~CTGImageVerification()	{}

IplImage* CTGImageVerification::ImageCheck(unsigned char* pBuf, int nWidth, int nHeight)
{
	IplImage* pImg = NULL;
	//-- 2012-07-10 hongsu@esmlab.com
	//-- Set Image From Bitmap 
	pImg = cvCreateImageHeader(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 3); 
	cvSetData(pImg, pBuf, pImg->widthStep);
	//-- Clear captured window
	if(!pImg)
		return NULL;

	//-- 2012-07-10 hongsu@esmlab.com
	//-- 
	int nCheckType = TGGetValue(TG_OPT_LATENCY_COLOR);

	switch(nCheckType)
	{
	case COLOR_R	:
	case COLOR_G	:
	case COLOR_B	:
		if(!DetectRGBColor(pImg, nCheckType))
			TGLog(0,_T("Detect RGB Problem"));
		break;
	default:
		if(!DetectRGB(pImg))
		{
			TGLog(0,_T("Detect RGB Problem"));
		}
		break;
	}
	return pImg;
}

//------------------------------------------------------------------------------
//! @function	DetectRGB
//! @date		2012-07-10
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return		
//------------------------------------------------------------------------------
BOOL CTGImageVerification::DetectRGB(IplImage* pImage)
{
	CString strImgPath = _T("");
	BOOL bRet = TRUE;
	CString strColor = _T("");
	LONG	ltemp = 0L;
	ImageConfirmResult Result;

	CTGImageColorDetect colorDetect[COLOR_ALL];
	IplImage* pImg[COLOR_ALL];

	//-- 2012-07-11 hongsu@esmlab.com
	//-- Set Detect Color Init 
	for(int i = 0 ; i < COLOR_ALL; i ++)
	{
		colorDetect[i].SetInit(pImage, i);
		//pImg[i] = FilterRGB(pImage, &colorDetect[i]);
		pImg[i] = FilterRGB(colorDetect[i].GetResizeImg(), &colorDetect[i]);
	}

	//-- 2012-07-11 hongsu@esmlab.com
	//-- Check Detection

	//-- 2012-08-21 hongsu@esmlab.com
	//-- Get Check Image Color

	for(int i = 0 ; i < COLOR_ALL; i ++)
	{
		//-- 2012-07-11 hongsu@esmlab.com
		//-- Draw PolyLine
		colorDetect[i].DrawLine(pImage);//pImg[i]);
		//-- 2012-07-12 hongsu@esmlab.com
		//-- Write Information
		colorDetect[i].DrawInfo(pImage);//pImg[i]);
	}


	//-- Check Image
	//-- Color
	//-- 2012-08-20 joonho.kim
	for(int i = 0 ; i < COLOR_ALL; i ++)
	{
		if(!colorDetect[i].CheckRange())
		{
			switch( i )
			{
			case 0: strColor.Format(_T("R")); break;
			case 1: strColor.Format(_T("G")); break;
			case 2: strColor.Format(_T("B")); break;
			}
			TGLog(0,_T("Get Range Area [%s]"),strColor);		
			ltemp = TG_ERR_COLOR;
			bRet = FALSE;
			m_ImgResult.bAfrRGB = FALSE;
		}
	}
	
	//-- Check Position
	//-- 2012-08-20 joonho.kim
	CvPoint ColorPoint[COLOR_ALL];
	for(int i = 0 ; i < COLOR_ALL; i ++)
	{
		ColorPoint[i]	=	colorDetect[i].GetPosition(POINT_LEFT_TOP);	
	}

	if(	ColorPoint[COLOR_R].x > ColorPoint[COLOR_B].x)
	{
		TGLog(0,_T("Image Position Left & Right Reverse"));	
		ltemp = ltemp | TG_ERR_LR_REVERSE;
		bRet = FALSE;
		m_ImgResult.bAfrLRRvs = FALSE;
	}

	if( ColorPoint[COLOR_G].y > ColorPoint[COLOR_R].y ||
		ColorPoint[COLOR_G].y > ColorPoint[COLOR_B].y)
	{
		TGLog(0,_T("Image Position Top & Bottom Reverse"));		
		ltemp = ltemp | TG_ERR_TB_REVERSE;
		bRet = FALSE;
		m_ImgResult.bAfrTBRvs = FALSE;
	}
	
	int c = cvWaitKey(1);
	if(c!=-1)
		return FALSE;

	for(int i = 0 ; i < COLOR_ALL; i ++)
		if(pImg[i])
			cvReleaseImage(&pImg[i]);

	SetResult( ltemp );

	//-- 2012-10-08 joonho.kim
	//-- Screen Capture
	if(!m_ImgResult.bCheck)
	{
		if(m_ImgResult.bAfrRGB		||
		   m_ImgResult.bAfrTBRvs	||
		   m_ImgResult.bAfrLRRvs)
		{	
			if(SaveFailImage(strImgPath, pImage))
			{
				m_ImgResult.bCheck = TRUE;
				InsertToDB(strImgPath);
			}
		}
	}
	else
	{
		if(!bRet)
		{
			if(m_ImgResult.bPreRGB		!= m_ImgResult.bAfrRGB	||
			   m_ImgResult.bPreTBRvs	!= m_ImgResult.bAfrTBRvs	||
			   m_ImgResult.bPreLRRvs	!= m_ImgResult.bAfrLRRvs)
			{
				if(SaveFailImage(strImgPath, pImage))
				InsertToDB(strImgPath);

				m_ImgResult.bPreRGB		= m_ImgResult.bAfrRGB;	
				m_ImgResult.bPreTBRvs	= m_ImgResult.bAfrTBRvs;
				m_ImgResult.bPreLRRvs	= m_ImgResult.bAfrLRRvs;
			}	
		}
	}
	return bRet;
}

//------------------------------------------------------------------------------
//! @function	SaveFailImage
//! @date		2012-10-09
//! @owner		joonho.kim
//! @return		
//------------------------------------------------------------------------------
BOOL CTGImageVerification::SaveFailImage(CString& strImgPath, IplImage* pImage)
{
	BOOL bRet = FALSE;
	CStringA strPath;
	COleDateTime time;
	time = COleDateTime::GetCurrentTime();
	CString strTime;
	strTime.Format(_T("[%.4d-%.2d-%.2d %.2dh%.2dm%.2ds]"),
		time.GetYear(),
		time.GetMonth(),
		time.GetDay(),
		time.GetHour(),
		time.GetMinute(),
		time.GetSecond());

	//IplImage* pImage = NULL;
	//pImage = (IplImage*)TGGetImage();
	if(pImage)
	{
		strImgPath.Format(_T("%s\\%s Fail_Image.jpg"), TGGetPath(TG_PATH_IMAGE_CONFIRM), strTime);
		CvvImage capImage;
		capImage.CopyOf( pImage , 1 );

		char* pszPath = NULL;
		pszPath = TGUtil::GetCharString(strImgPath);
		
		if(capImage.Save(pszPath))
			bRet = TRUE;
		else
			bRet = FALSE;

		//- Remove
		delete pszPath;
		TGSetImage(NULL);
	}

	return bRet;
}

void CTGImageVerification::InitData()
{
	m_nImgResultType = 0;
	
	m_ImgResult.bCheck		= FALSE;
	m_ImgResult.bPreRGB		= TRUE;
	m_ImgResult.bPreTBRvs	= TRUE;
	m_ImgResult.bPreLRRvs	= TRUE;
	m_ImgResult.bAfrRGB		= TRUE;
	m_ImgResult.bAfrTBRvs	= TRUE;
	m_ImgResult.bAfrLRRvs	= TRUE;
}
//------------------------------------------------------------------------------
//! @function	Insert To DB
//! @date		2012-10-09
//! @owner		joonho.kim
//! @return		
//------------------------------------------------------------------------------
BOOL CTGImageVerification::InsertToDB(CString strImgPath) 
{
	CString strUUID , strTestItem, strResult;
	ImageConfirmResult Result;
	strUUID		= TGGetUniqueID();

	//-- RGB Result
	if( (m_nImgResultType & TG_ERR_COLOR) == TG_ERR_COLOR)
		Result.strRGB	=	TG_DETAIL_FAIL;
	else
		Result.strRGB	=	TG_DETAIL_PASS;
	//-- Top & Bottom Result
	if( (m_nImgResultType & TG_ERR_TB_REVERSE) == TG_ERR_TB_REVERSE)
		Result.strVeticalFlip	=	TG_DETAIL_FAIL;
	else
		Result.strVeticalFlip	=	TG_DETAIL_PASS;
	//-- Left & Right Result
	if( (m_nImgResultType & TG_ERR_LR_REVERSE) == TG_ERR_LR_REVERSE)
		Result.strHorizontalFlip	=	TG_DETAIL_FAIL;
	else
		Result.strHorizontalFlip	=	TG_DETAIL_PASS;

	Result.strImagePath = strImgPath;

	m_dwSequence++;
	CTGImageConfirmResult* pInfo = new CTGImageConfirmResult( m_dwSequence , strUUID, Result);
	if(!TGSendMessageToDB(WM_TG_ODBC_IMAGE_CONFIRM_RESULT, (PVOID)pInfo))
	{
		delete pInfo;
		pInfo = NULL;
	}
	return TRUE;
}
//------------------------------------------------------------------------------
//! @function	DetectRGBColor		
//! @brief				
//! @date		2012-08-21
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
BOOL CTGImageVerification::DetectRGBColor(IplImage* pImage, int nColor)
{
	BOOL bRet = TRUE;

	CTGImageColorDetect colorDetect;
	IplImage* pImg;

	//-- 2012-07-11 hongsu@esmlab.com
	//-- Set Detect Color Init 
	colorDetect.SetInit(pImage, nColor);
	pImg = FilterRGB(colorDetect.GetResizeImg(), &colorDetect);

	//-- 2012-07-11 hongsu@esmlab.com
	//-- Draw PolyLine
	colorDetect.DrawLine(pImage);
	//-- 2012-07-12 hongsu@esmlab.com
	//-- Write Information
	colorDetect.DrawInfo(pImage);

	//-- 2012-08-21 hongsu@esmlab.com
	//-- Get Detect Percentage
	float nPercentage = colorDetect.GetPercentage();
	//if(nPercentage > 50)
	{
		CString strTime = TGGetOption(TG_VAL_TESTAREA_TIME);
		//-- 2012-08-21 hongsu@esmlab.com
		//-- Get TestArea Time				
		TGLog(1,_T("Detect Color [%s][%3.02f%%]"),strTime,nPercentage);
		//-- 2012-08-22 hongsu@esmlab.com
		//-- Draw Graph
		DrawLatencyGraph(strTime,nPercentage);
	}

	int c = cvWaitKey(1);
	if(c!=-1)
		return FALSE;
	if(pImg)
		cvReleaseImage(&pImg);
	return bRet;
}

void CTGImageVerification::SetResult(int nResult) 
{
	m_nImgResultType = m_nImgResultType | nResult;
}
int CTGImageVerification::GetResult() 
{
	return m_nImgResultType;
}


IplImage* CTGImageVerification::FilterRGB(IplImage* pImgSrc, CTGImageColorDetect* pColorDetect)
{
	IplImage* pImgMask;
	if(pColorDetect->IsColorMode())
		pImgMask = cvCreateImage(cvSize(pImgSrc->width, pImgSrc->height),IPL_DEPTH_8U, 3);
	else
		pImgMask = cvCreateImage(cvSize(pImgSrc->width, pImgSrc->height),IPL_DEPTH_8U, 1);

	uchar* temp;
	uchar* temp2;
	int nType = pColorDetect->GetType();
	int nGapOfColor;
	for(int i = 0; i < pImgSrc->width; i++)
	{
		for(int j = 0; j < pImgSrc->height; j++)
		{
			pColorDetect->AddTestCount();
			temp = &((uchar*)(pImgSrc->imageData + pImgSrc->widthStep*j))[i*3];
			
			if(pColorDetect->IsColorMode())
				temp2= &((uchar*)(pImgMask->imageData + pImgMask->widthStep*j))[i*3];

			//-- 나머지 평균값보다 Tolerance로 찾는다.			
			switch(nType)
			{
			case COLOR_R: nGapOfColor = temp[2] - (temp[0]+temp[1])/2; break;
			case COLOR_G: nGapOfColor = temp[1] - (temp[0]+temp[2])/2; break;
			case COLOR_B: nGapOfColor = temp[0] - (temp[1]+temp[2])/2; break;
			default:	  nGapOfColor = 0;							   break;
			}

			int nTemp = pColorDetect->GetColorTolerance();
			if(	nGapOfColor > nTemp)
			{
				pColorDetect->AddDetectCount();
				if(pColorDetect->IsColorMode())
				{
					switch(nType)
					{
						case COLOR_R: temp2[0] =   0;	temp2[1] = 0;	temp2[2] = 255;	break;
						case COLOR_G: temp2[0] =   0;	temp2[1] = 255;	temp2[2] =   0;	break;
						case COLOR_B: temp2[0] = 255;	temp2[1] = 0;	temp2[2] =   0;	break;
						default:	  temp2[0] = temp[0];
									  temp2[1] = temp[1];
									  temp2[2] = temp[2]; break;
					}
					
					//-- 2012-07-11 hongsu@esmlab.com
					//-- Add RGB
					pColorDetect->AddColor(COLOR_B, temp[0]);
					pColorDetect->AddColor(COLOR_G, temp[1]);
					pColorDetect->AddColor(COLOR_R, temp[2]);
					//-- 2012-07-11 hongsu@esmlab.com
					//-- Position
					pColorDetect->SetPosition(i,j);
				}
				else
					((uchar*)(pImgMask->imageData + pImgMask->widthStep*j))[i] = 255;
			}
			else{
				if(pColorDetect->IsColorMode())
				{
					temp2[0] = 0;
					temp2[1] = 0;
					temp2[2] = 0;
				}
				else
					((uchar*)(pImgMask->imageData + pImgMask->widthStep*j))[i] = 0;
			}
		}
	}


	//-- 2012-07-11 hongsu@esmlab.com
	//-- Get RGB Average
	pColorDetect->SetAverageColor();
	return pImgMask;
}


void CTGImageVerification::cvOverlayImage(IplImage* src, IplImage* overlay, CTGImageColorDetect colorDetect)
{
	CvPoint ptTop, ptBottom; 
	ptTop		= colorDetect.GetPosition(POINT_LEFT_TOP);
	ptBottom	= colorDetect.GetPosition(POINT_RIGHT_BOTTOM);

	int x = ptTop.x;
	int y = ptTop.y;
	int width	= ptBottom.x = x;
	int height	= ptBottom.y - y;
	double alpha	= 0.5; 
	double beta		= 1.0 - alpha; 

	//cvSetImageROI(src, cvRect(x, y, width, height)); 
	//cvSetImageROI(overlay, cvRect(0, 0, width, height)); 

	cvAddWeighted(src, alpha, overlay, beta, 0.0, src); 
	cvResetImageROI(src); 
} 

void CTGImageVerification::DrawLatencyGraph(CString strTime, double dbPercentage)
{
	//-- 2012-08-22 hongsu@esmlab.com
	//-- Just Add NULL Ready
	if(strTime == STR_READY)
		return;

	//-- Check Tolerance
	//-- Send Report Data
	BOOL bPercentCheck = (BOOL)TGGetValue(TG_OPT_LATENCY_PERCENTAGE_CHECK);
	int nToleranceLTColor = TGGetValue(TG_OPT_LATENCY_TOLERANCE_COLOR);
	if(dbPercentage > nToleranceLTColor && (!bPercentCheck))
	{
		TGSetValue(TG_OPT_LATENCY_PERCENTAGE_CHECK , 1);
		TGEvent* pMsg = new TGEvent;
		pMsg->message = WM_TG_CTRL_LATENCY_REPORT;	

		CString strResolution;
		strResolution.Format(_T("%dX%d"),TGGetValue(TG_VAL_FRAME_WIDTH), TGGetValue(TG_VAL_FRAME_HEIGHT));

		LatencyResult* pData = new LatencyResult;
		pData->strIP			= TGGetOption(TG_VAL_TEST_IP);
		pData->strCodec			= TGGetOption(TG_VAL_ENCORDING);
		pData->strResolution	= strResolution;
		pData->strFramerate		= TGUtil::SetString(TGGetValue(TG_VAL_FRAMERATE));
		pData->strColor			= g_arPropertyImageDetectColor[TGGetValue(TG_OPT_LATENCY_COLOR)];
		pData->strTime			= strTime;
		pData->dbPercentage		= dbPercentage;
		pData->nCount			= 1;

		pMsg->pParam  = (LPARAM)pData;
		//-- Framerate Check
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(),  WM_TG, (WPARAM)WM_TG_CTRL, (LPARAM)pMsg);
	}

	//-- 2012-04-26 hongsu
	TGEvent* pMsg = new TGEvent;
	pMsg->message = WM_TG_CTRL_LATENCY_INFO;	
	pMsg->pParam  = (LPARAM)TGUtil::GetCharString(strTime);
	dbPercentage *= 100;
	pMsg->nParam  = (UINT)dbPercentage;

	//-- Framerate Check
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(),  WM_TG, (WPARAM)WM_TG_CTRL, (LPARAM)pMsg);
}