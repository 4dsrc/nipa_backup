/////////////////////////////////////////////////////////////////////////////
//
// TGImageManager.cpp
//
//
// Copyright (c) 2009 ESMLab, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of ESMLab, Inc. ("Confidential Information").  
//
// @author	Jung Hongsu (hongsu@esmlab.com)
// @Date	2009-05-20
// @Note	2012-12-07 
//			Memory Leak
//			cvCreateImage	-> cvReleaseImage
//			cvLoadImage		-> cvReleaseImage
//			cvCloneImage	-> cvReleaseImage
//			cvQueryImage	-> cvReleaseImage
//
//			cvCreateCameraCapture	-> cvReleaseCapture
//			cvCaptureFramCAM		-> cvReleaseCapture
//
//			cvCreateMemStorage	-> cvReleaseMemStorage
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TGImageManager.h"
#include "TGImageFrameDlg.h"
#include "MainFrm.h"

#include "TGLabeling.h"
#include "TGFileOperation.h"


const int nOCR_W = 300;//12;	//30
const int nOCR_H = 200;//28;	//40

const int nNum_W_Big = 24;
const int nNum_H_Big = 36;

const int nNum_W_Small = 5;
const int nNum_H_Small = 16;

IMPLEMENT_DYNAMIC(CTGImageManager, CDialog)

CTGImageManager::CTGImageManager(CWnd* pParent /*=NULL*/)
: CDialog(CTGImageManager::IDD, pParent)
{
	m_pCaptureImage	= NULL;
	m_pDisplayImage	= NULL;

	m_dbHorzScaling	= 1;
	m_dbVertScaling	= 1;
}

//------------------------------------------------------------------------------ 
//! @brief		OnInitDialog()
//! @date		
//! @attention	none
//! @comment
//------------------------------------------------------------------------------ 
BOOL CTGImageManager::OnInitDialog() 
{
	CDialog::OnInitDialog();

	//-- Init Image Pointer
	m_pCaptureImage = NULL;
	m_pDisplayImage = NULL;

	return TRUE; 
}

CTGImageManager::~CTGImageManager()
{
	if(m_pCaptureImage)
		cvReleaseImage(&m_pCaptureImage);
	if(m_pDisplayImage)
		cvReleaseImage(&m_pDisplayImage);	
}

void CTGImageManager::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);		
}


BEGIN_MESSAGE_MAP(CTGImageManager, CDialog)
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_SHOWWINDOW()
	ON_WM_ACTIVATE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

//------------------------------------------------------------------------------ 
//! @brief		  OnSize
//! @date		    2010-9-14
//! @attention	none
//! @comment
//------------------------------------------------------------------------------ 
void CTGImageManager::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);	
}

//------------------------------------------------------------------------------ 
//! @brief		  OnPaint
//! @date		    2010-9-14
//! @attention	none
//! @comment    
//------------------------------------------------------------------------------ 
void CTGImageManager::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	if(!m_pDisplayImage)
		return ;

	//[Client-Window] width, height
	CRect ClientRect;
	GetClientRect(&ClientRect);
	int nClientWidth  = ClientRect.Width();
	int nClientHeight = ClientRect.Height();

	//[Image] width, height
	int nImageWidth  = 0;
	int nImageHeight = 0;
	IplImage* pImage = NULL;
	pImage = m_pDisplayImage;
	nImageWidth  = m_pDisplayImage->width;
	nImageHeight = m_pDisplayImage->height;

	//Draw image in the center of client-area
	if(pImage)
	{
		CvvImage vvImage;
		vvImage.CopyOf(pImage, 1);

		CRect rect; 
		rect.left   = nClientWidth/2  - nImageWidth/2; 
		rect.top    = nClientHeight/2 - nImageHeight/2;
		rect.right  = rect.left + nImageWidth; 
		rect.bottom = rect.top  + nImageHeight; 
		vvImage.DrawToHDC(dc.GetSafeHdc(), rect);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		SetMode(int nWndMode)
//! @date		
//! @attention	none
//! @comment		Pattern Matching
//------------------------------------------------------------------------------ 
void CTGImageManager::SetMode(int nWndMode)	
{
	m_nImageType = nWndMode;	
}

//------------------------------------------------------------------------------ 
//! @brief		FitSizeShow
//! @date		
//! @attention	none
//! @comment		Pattern Matching
//------------------------------------------------------------------------------ 
BOOL CTGImageManager::FitSizeShow()
{
	int nOriW = 0;
	int nOriY = 0;
	//-- 2012-07-19 hongsu@esmlab.com
	//-- Check 
	if(!m_pCaptureImage)
		return FALSE;

	nOriW = m_pCaptureImage->width;
	nOriY = m_pCaptureImage->height;

	if(!nOriW || !nOriY)
		return FALSE;

	CRect	rcClient;
	GetClientRect(rcClient);
	int cx = rcClient.Width();
	//-- 2010-9-15 keunbae.song
	//int cy = rcClient.Height()- H_RESULT_STATIC - H_PROGERSSBAR-30;
	int cy = rcClient.Height();
	int n1 = cx*nOriY;
	int n2 = cy*nOriW;

	float	nWidth, nHeight;

	if(n1 >= n2)
	{
		nHeight = (float)cy;
		nWidth  = n2/(float)nOriY;
	}
	else
	{
		nHeight = n1/(float)nOriW;
		nWidth  = (float)cx;
	}

	float fWidthRate	= 0.0;
	float fHeightRate	= 0.0;

	fWidthRate = nWidth / nOriW;
	fHeightRate = nHeight / nOriY;

	return ResampleImage(fWidthRate, fHeightRate);
}

//------------------------------------------------------------------------------ 
//! @brief		GetScaleRate(double* pdbHorzScaling, double* pdbVertScaling)
//! @date		
//! @attention	none
//! @comment	Pattern Matching
//------------------------------------------------------------------------------ 
void CTGImageManager::GetScaleRate(double* pdbHorzScaling, double* pdbVertScaling)
{
	if( pdbHorzScaling )
		*pdbHorzScaling = m_dbHorzScaling;

	if( pdbVertScaling )
		*pdbVertScaling = m_dbVertScaling;
}

//------------------------------------------------------------------------------ 
//! @brief		ResampleImage
//! @date		
//! @attention	none
//! @comment		Pattern Matching
//------------------------------------------------------------------------------ 
BOOL CTGImageManager::ResampleImage(double dbWidthRate, double dbHeightRate)
{
	if(0 == dbWidthRate)
		dbWidthRate = 1;

	if(0 == dbHeightRate)
		dbHeightRate = 1;

	IplImage* pImage = NULL;
	pImage = m_pCaptureImage;
	if(!pImage)
		return FALSE;

	//축소/확대 Rate
	int nWidth  = (int)(pImage->width*dbWidthRate);
	int nHeight = (int)(pImage->height*dbHeightRate);

	if(m_pDisplayImage)
		cvReleaseImage(&m_pDisplayImage);
	
	m_pDisplayImage = cvCreateImage(cvSize(nWidth, nHeight), pImage->depth, pImage->nChannels);
	cvResize(pImage, m_pDisplayImage);
	SetScaleRate(dbWidthRate, dbHeightRate);	
	
	//-- 2012-07-17 hongsu@esmlab.com
	//-- Invalidate();
	Invalidate(FALSE);

	return TRUE;
}


//------------------------------------------------------------------------------ 
//! @brief		InitScaleRate()
//! @date		
//! @attention	none
//! @comment
//------------------------------------------------------------------------------ 
void CTGImageManager::InitScaleRate()
{
	m_dbHorzScaling		= 1;
	m_dbVertScaling		= 1;
}

//------------------------------------------------------------------------------ 
//! @brief		  GetImageScore
//! @date		    2010-9-14
//! @attention	none
//! @comment		Pattern Matching
//------------------------------------------------------------------------------ 
float CTGImageManager::GetImageScore(COMPIMAGEINFO* pInfo , IplImage* pPatternOriginal)
{
	if(!m_pCaptureImage || !pPatternOriginal)
		return 0.0;

	//-- 2010-9-15 keunbae.song
	((CTGImageFrameDlg*)GetParent())->ProgressIt(TRUE);

	Rect_struct rcDraw;
	memcpy(&rcDraw, &(pInfo->rcRect), sizeof(Rect_struct));

	int nWidth  = m_pCaptureImage->width	- pPatternOriginal->width+1;
	int nHeight = m_pCaptureImage->height	- pPatternOriginal->height+1;

	//-- [2010-10-13] Lee JungTaek
	//-- Fail When the Width, Height Minus
	if(nWidth < 0 || nHeight < 0)
		return 0.0;

	IplImage* coeff = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_32F, 1);
	//-- 2010-9-15 keunbae.song
	((CTGImageFrameDlg*)GetParent())->ProgressIt();
	//상관계수를 구하여 coeff에 그려준다.
	cvMatchTemplate(m_pCaptureImage, pPatternOriginal, coeff, CV_TM_CCOEFF_NORMED);
	//-- 2010-9-15 keunbae.song
	((CTGImageFrameDlg*)GetParent())->ProgressIt();

	// 상관계수가 최대값을 가지는 위치를 찾는다 
	CvPoint left_top;
	double min, max;
	cvMinMaxLoc(coeff, &min, &max, NULL, &left_top); 

	//-- 2010-9-15 keunbae.song
	((CTGImageFrameDlg*)GetParent())->ProgressIt();

	BOOL bPatterResult = FALSE;
	float fScore = float(max*1000);
	int nTolerableScore =  TGGetValue(TG_OPT_IMG_TOLERABLESCORE); 

	((CTGImageFrameDlg*)GetParent())->ProgressIt();

	if(nTolerableScore <= fScore)
	{
		bPatterResult = TRUE;	
		//찾은 물체에 사각형 박스를 그린다.
		cvRectangle(m_pCaptureImage, left_top, cvPoint(left_top.x + pPatternOriginal->width, 
			left_top.y + pPatternOriginal->height), CV_RGB(255, 255, 0), 5);
	}

	//-- 2010-9-15 keunbae.song
	((CTGImageFrameDlg*)GetParent())->ProgressIt();

	//Display Image
	if(!FitSizeShow())
	{
		TGLog(1, _T("[IM] ERROR: FitSizeShow \n")) ;
		((CTGImageFrameDlg*)GetParent())->ProgressIt();
		return FALSE; 
	}

	//-- 2010-9-15 keunbae.song
	((CTGImageFrameDlg*)GetParent())->ProgressIt();

	CString	strResult;

	if(bPatterResult)
	{
		strResult.Format(_T("Result : %s , Matching Point : %0.f "), TG_POSTFAIL_PASS, fScore);
		//-- 2010-9-15 keunbae.song
		((CTGImageFrameDlg*)GetParent())->ProgressIt(FALSE, strResult);
	}
	else
	{
		strResult.Format(_T("Result : %s , Matching Point : %0.f "), TG_POSTFAIL_FAIL, fScore);
		//-- 2010-9-15 keunbae.song
		((CTGImageFrameDlg*)GetParent())->ProgressIt(FALSE, strResult);
	}

	Invalidate();

	//-- 2011-12-07 hongsu.jung	
	cvReleaseImage(&coeff);


	return fScore;
}

//------------------------------------------------------------------------------ 
//! @brief		  GetImageScore
//! @date		    2010-9-14
//! @attention	none
//! @comment		Pattern Matching With ROI
//------------------------------------------------------------------------------ 
float CTGImageManager::GetImageScoreWithROI(COMPIMAGEINFO* pInfo , IplImage* pPatternOriginal)
{
	//-- Get ROI Info
	CRect rcROIRect = rcROIRect = pInfo->rcROIRect;
	//-- [2010-10-11] Lee JungTaek
	//-- Add Select Area (ROI)
	cvSetImageROI(m_pCaptureImage, cvRect(rcROIRect.left,rcROIRect.top,rcROIRect.Width(),rcROIRect.Height()));

	int nWidth  = m_pCaptureImage->roi->width - pPatternOriginal->width+1;
	int nHeight = m_pCaptureImage->roi->height - pPatternOriginal->height+1;

	//-- [2010-10-13] Lee JungTaek
	//-- Fail When the Width, Height Minus
	if(nWidth < 0 || nHeight < 0)
		return 0.0;

	IplImage* coeff = cvCreateImage(cvSize(nWidth,nHeight), IPL_DEPTH_32F, 1);

	//-- 2010-9-15 keunbae.song
	((CTGImageFrameDlg*)GetParent())->ProgressIt();

	//상관계수를 구하여 coeff에 그려준다.
	cvMatchTemplate(m_pCaptureImage, pPatternOriginal, coeff, CV_TM_CCOEFF_NORMED);

	//-- [2010-10-11] Lee JungTaek
	//-- Reset ROI(Region of interest)
	cvResetImageROI(m_pCaptureImage);

	//-- 2010-9-15 keunbae.song
	((CTGImageFrameDlg*)GetParent())->ProgressIt();

	// 상관계수가 최대값을 가지는 위치를 찾는다 
	CvPoint left_top;
	double min, max;
	cvMinMaxLoc(coeff, &min, &max, NULL, &left_top); 

	//-- 2010-9-15 keunbae.song
	((CTGImageFrameDlg*)GetParent())->ProgressIt();

	BOOL bPatterResult = FALSE;
	float fScore = float(max*1000);
	int nTolerableScore =  TGGetValue(TG_OPT_IMG_TOLERABLESCORE); 

	((CTGImageFrameDlg*)GetParent())->ProgressIt();

	if(nTolerableScore <= fScore)
	{
		bPatterResult = TRUE;		

		//찾은 물체에 사각형 박스를 그린다.
		//-- [2010-10-11] Lee JungTaek
		//-- Resize With ROI info
		left_top.x += rcROIRect.left;
		left_top.y += rcROIRect.top;

		cvRectangle(m_pCaptureImage, left_top, cvPoint(left_top.x + pPatternOriginal->width, 
			left_top.y + pPatternOriginal->height), CV_RGB(255, 255, 0), 5);
	}

	//-- 2010-9-15 keunbae.song
	((CTGImageFrameDlg*)GetParent())->ProgressIt();

	//Display Image
	if(!FitSizeShow())
	{
		TGLog(1, _T("[IM] ERROR: FitSizeShow \n")) ;		
		return FALSE; 
	}

	//-- 2010-9-15 keunbae.song
	((CTGImageFrameDlg*)GetParent())->ProgressIt();

	CString	strResult;

	if(bPatterResult)
	{
		strResult.Format(_T("Result : %s , Matching Point : %0.f "), TG_POSTFAIL_PASS, fScore);
		//-- 2010-9-15 keunbae.song
		((CTGImageFrameDlg*)GetParent())->ProgressIt(FALSE, strResult);
	}
	else
	{
		strResult.Format(_T("Result : %s , Matching Point : %0.f "), TG_POSTFAIL_FAIL, fScore);
		//-- 2010-9-15 keunbae.song
		((CTGImageFrameDlg*)GetParent())->ProgressIt(FALSE, strResult);
	}
	
	Invalidate();

	//-- 2011-12-07 hongsu.jung	
	cvReleaseImage(&coeff);

	return fScore;
}

//------------------------------------------------------------------------------ 
//! @brief		LoadViewerImage
//! @date		
//! @attention	none
//! @comment
//------------------------------------------------------------------------------ 
BOOL CTGImageManager::LoadViewerImage(CString strPath)
{
	//-- Captured Image	
	if(m_pCaptureImage)
		cvReleaseImage(&m_pCaptureImage);

	char pchPath[MAX_PATH] = {0};
	int nRequiredSize = wcstombs(pchPath, strPath, MAX_PATH); 
	m_pCaptureImage = cvLoadImage(pchPath, CV_LOAD_IMAGE_UNCHANGED);

	//-- 2010-9-14 keunbae.song
	//-- Clear captured window
	if(!m_pCaptureImage)
	{
		ClearImage();
		return FALSE; 
	}

	m_pDisplayImage = cvCloneImage(m_pCaptureImage);
	//Display Image
	if(!FitSizeShow())
	{
		TGLog(1, _T("[IM] ERROR: FitSizeShow \n")) ;
		return FALSE; 
	}

	//-- 2012-07-17 hongsu@esmlab.com
	//-- Check Blink
	// Invalidate();
	return TRUE;
}
//------------------------------------------------------------------------------ 
//! @brief		LoadViewerImage
//! @date		
//! @attention	none
//! @comment
//------------------------------------------------------------------------------ 
BOOL CTGImageManager::LoadViewerImage()
{
	CvScalar colorR, colorG, colorB;
	colorR = CV_RGB(0xff,0x00,0x00);
	colorG = CV_RGB(0x00,0xff,0x00/*0x00,0x66,0x33*/);
	colorB = CV_RGB(0x00,0x00,0xff);

	int nWidth[3];
	int nHeight[3];

	//-- Captured Image	
	if(m_pDisplayImage)
		cvReleaseImage(&m_pDisplayImage);
	if(m_pCaptureImage)
		cvReleaseImage(&m_pCaptureImage);

	//[Client-Window] width, height
	CRect ClientRect;
	GetClientRect(&ClientRect);
	int nClientWidth  = ClientRect.Width();
	int nClientHeight = ClientRect.Height();

	//Red
	nWidth[0] = nClientWidth / 2;
	nHeight[0] = nClientHeight / 2;
	//Green
	nWidth[1] = nClientWidth;
	nHeight[1] = nClientHeight / 2;
	//Blue
	nWidth[2] = nClientWidth / 2;
	nHeight[2] = nClientHeight / 2;

	m_pDisplayImage = cvCreateImage(cvSize(nClientWidth,nClientHeight), IPL_DEPTH_32F, 3);	

	//-- 2012-08-21 hongsu@esmlab.com
	//-- Set Color
	//Red
	cvRectangle(m_pDisplayImage	,
		cvPoint(0,nClientHeight / 2),
		cvPoint(nClientWidth / 2,nClientHeight),
		colorR,
		CV_FILLED);

	//Green
	cvRectangle(m_pDisplayImage	,
		cvPoint(0,0),
		cvPoint(nClientWidth,nClientHeight / 2),
		colorG,
		CV_FILLED);

	//Blue
	cvRectangle(m_pDisplayImage	,
		cvPoint(nClientWidth / 2,nClientHeight / 2),
		cvPoint(nClientWidth,nClientHeight),
		colorB,
		CV_FILLED);
	//-- 2012-08-21 hongsu@esmlab.com
	//-- Set Point 
	m_pCaptureImage	= cvCloneImage( m_pDisplayImage );
	//Display Image
	if(!FitSizeShow())
		TGLog(1, _T("[IM] ERROR: FitSizeShow")) ;


	//-- 2010-9-14 keunbae.song
	//-- Clear captured window
	if(!m_pCaptureImage)
	{
		ClearImage();
		return FALSE; 
	}

	//Display Image
	if(!FitSizeShow())
	{
		TGLog(1, _T("[IM] ERROR: FitSizeShow \n")) ;
		return FALSE; 
	}

	return TRUE;
}
//------------------------------------------------------------------------------ 
//! @brief		LoadViewerImage
//! @date		
//! @attention	none
//! @comment
//------------------------------------------------------------------------------ 
BOOL CTGImageManager::LoadViewerImage(IplImage* pImg)
{
	//-- Captured Image	
	if(m_pCaptureImage)
		cvReleaseImage(&m_pCaptureImage);

	m_pCaptureImage = (IplImage*)cvClone(pImg);

	//-- Clear captured window
	if(!m_pCaptureImage)
	{
		ClearImage();
		return FALSE; 
	}
	
	//Display Image
	if(!FitSizeShow())
	{
		TGLog(1, _T("[IM] ERROR: FitSizeShow \n")) ;
		return FALSE; 
	}

	//-- 2012-07-17 hongsu@esmlab.com
	//-- Blink
	// Invalidate(FALSE);
	return TRUE;
}


//------------------------------------------------------------------------------ 
//! @brief		SetScaleRate(double dbHorzScaling, double dbVertScaling)
//! @date		
//! @attention	none
//! @comment		Pattern Matching
//------------------------------------------------------------------------------ 
void CTGImageManager::SetScaleRate(double dbHorzScaling, double dbVertScaling)
{
	m_dbHorzScaling = dbHorzScaling;
	m_dbVertScaling = dbVertScaling;
	TGLog(6, _T("[CTGImageManager::SetScaleRate] HScaling=%f, VScaling=%f"), m_dbHorzScaling, m_dbVertScaling);
}

//------------------------------------------------------------------------------ 
//! @brief    Get OCR Check (Number)
//! @date     2011-11-29
//! @owner    hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT CTGImageManager::CheckOCR(OCR_CHECK_INFO* pInfo)
{
	int nLabelCnt = 0;

	//-- 2011-11-29 hongsu.jung	
	((CTGImageFrameDlg*)GetParent())->ProgressIt(TRUE);

	//-- 2012-04-12 hongsu.jung
	//-- 1. Labeling
	nLabelCnt = LabelImage(pInfo->strFilePath);
	if(nLabelCnt < CNT_DATE)
	{
		TGLog(0,_T("Labeling Number is not enough [%d]"),nLabelCnt);
		TGLog(0,_T("Check Snapshot and Time OCR"));
		return TG_ERR_OCR;
	}

	//-- 2. Read Number
	if(!GetNumber(nLabelCnt))
	{
		TGLog(0,_T("Detect Number"));
		return TG_ERR_OCR;
	}

	//-- 3. CompareTime
	return CompareTime(pInfo);
}

//------------------------------------------------------------------------------ 
//! @brief		LabelImage
//! @date		2012-04-12
//! @owner		hongsu.jung
//! @note     
//------------------------------------------------------------------------------ 
int CTGImageManager::LabelImage(CString strPath)
{
	int nLabeledCnt = 0;
	CvPoint	fire_pt1,fire_pt2;

	IplImage*	pImage;
	IplImage*	pGray;
	IplImage*   pClone;
	IplImage*	pLabeled;


	//-- Load Image
	char cFilePath[MAX_PATH] = {0};
	int nRequiredSize = wcstombs(cFilePath, strPath, MAX_PATH); 
	pImage = cvLoadImage(cFilePath, -1);
	if(!pImage)
		return -1;

	//-- Gray Image
	pGray = cvCreateImage(cvGetSize(pImage), IPL_DEPTH_8U, 1);
	if(!pGray)
	{
		cvReleaseImage(&pImage);
		return -1;
	}

	cvCvtColor(pImage, pGray, CV_RGB2GRAY);
	cvThreshold(pGray, pGray, 1, 255.0, CV_THRESH_BINARY | CV_THRESH_OTSU);//이진화

	pLabeled = cvCreateImage( cvSize( pGray->width, pGray->height ), 8, 3 );//레이블링 이미지
	cvCvtColor(pGray, pLabeled, CV_GRAY2BGR );//그레이를 RGB로 바꿔준다.		
	

	// 레이블링 알고리즘 사용
	CTGLabeling* pLabel = new CTGLabeling();
	pLabel->SetParam(pGray, 15 );	// 레이블링 할 이미지와 최소 픽셀수(20) 설정		
	pLabel->DoLabeling(); //레이블링 실시	

	TGLog(3,_T("Label Area [%d]"),pLabel->m_nBlobs);

	//-- 잡영 제거 부분.
	//-- Remove Big Image
	pLabel->BlobBigSizeConstraint( nNum_W_Big, nNum_H_Big );
	TGLog(3,_T("Label Area [%d] After Remove Big Label"),pLabel->m_nBlobs);
	//-- Remove Small Image
	pLabel->BlobSmallSizeConstraint( nNum_W_Small, nNum_H_Small );
	TGLog(3,_T("Label Area [%d] After Remove Small Label"),pLabel->m_nBlobs);

	//-- Get Same Y Positions.
	//-- Detect Time Line
	pLabel->BlobSameHeightConstraint();

	//-- Sort Array
	int nAll = pLabel->m_nBlobs;
	int nTemp[4];
	//-- Draw Detected Image
	for(int i = 0; i < nAll ; i ++)
	{
		for(int j = i+1; j < nAll ; j ++)
		{
			if(pLabel->m_recBlobs[i].x > pLabel->m_recBlobs[j].x)
			{
				nTemp[0] = pLabel->m_recBlobs[i].x;
				nTemp[1] = pLabel->m_recBlobs[i].y;
				nTemp[2] = pLabel->m_recBlobs[i].width;
				nTemp[3] = pLabel->m_recBlobs[i].height;

				pLabel->m_recBlobs[i].x		=pLabel->m_recBlobs[j].x		;
				pLabel->m_recBlobs[i].y		=pLabel->m_recBlobs[j].y		;
				pLabel->m_recBlobs[i].width	=pLabel->m_recBlobs[j].width	;
				pLabel->m_recBlobs[i].height=pLabel->m_recBlobs[j].height;

				pLabel->m_recBlobs[j].x			=nTemp[0] ;
				pLabel->m_recBlobs[j].y			=nTemp[1] ;
				pLabel->m_recBlobs[j].width		=nTemp[2] ;
				pLabel->m_recBlobs[j].height	=nTemp[3] ;
			}
		}
	}


	//-- Draw Detected Image
	for( int i=0; i < nAll; i++ )
	{
		//-- 이미지는 템플릿 사이즈로 조정해준다.
		m_pImgNum[i] = cvCreateImage(cvSize(nOCR_W, nOCR_H), IPL_DEPTH_8U, 1);
		//-- 버퍼에 이미지를 받는다.
		cvSetImageROI(pGray,cvRect(pLabel->m_recBlobs[i].x,
									pLabel->m_recBlobs[i].y,
									pLabel->m_recBlobs[i].width,
									pLabel->m_recBlobs[i].height));
		TRACE("[%02d] : (%d,%d),w:%d,h:%d\n",i,
									pLabel->m_recBlobs[i].x,
									pLabel->m_recBlobs[i].y,
									pLabel->m_recBlobs[i].width,
									pLabel->m_recBlobs[i].height);
		//-- 교환해서 리사이즈한다.	
		pClone = (IplImage*)cvClone(pGray);
		cvResize(pClone, m_pImgNum[i], CV_INTER_LINEAR );		
		//레이블링된 각 위치값을 잡아주고
		fire_pt1 = cvPoint(				 pLabel->m_recBlobs[i].x,
										 pLabel->m_recBlobs[i].y );
		fire_pt2 = cvPoint(	fire_pt1.x + pLabel->m_recBlobs[i].width,
							fire_pt1.y + pLabel->m_recBlobs[i].height );
		// 각 레이블 표시
		CvScalar color	= cvScalar( 0, 0, 255 );	
		cvDrawRect(pLabeled, fire_pt1, fire_pt2, color );
		//-- Release
		cvReleaseImage(&pClone);
	}

	//-- 2012-04-10 hongsu.jung
	nLabeledCnt = pLabel->m_nBlobs;

	LoadViewerImage(pLabeled);

	cvReleaseImage(&pImage);
	cvReleaseImage(&pGray);
	cvReleaseImage(&pLabeled);		

	delete pLabel;
	pLabel = NULL;

	return nLabeledCnt;
}

//------------------------------------------------------------------------------ 
//! @brief		LabelImage
//! @date		2012-04-12
//! @owner		hongsu.jung
//! @note     
//------------------------------------------------------------------------------ 
BOOL CTGImageManager::GetNumber(int nLebelCnt)
{
	BOOL bRet = TRUE;

	CString temp;
	IplImage* pRsrNum[10];
	int count=0;
	int number_temp[10];	//정렬을 위한 배열 2개
	int number_num[10];
	int number_temp_temp;
	int number_num_temp;

	char final[CNT_DATE+CNT_WEEK]="";
	char cFilePath[MAX_PATH] = {0};
	CString strFile;


	//-- Check Model
	//-- 2012-04-12 hongsu.jung		
	CString strFolder;		
	CString strModel = TGGetModel();	

	if(strModel.GetLength())
		strFolder.Format(_T("%s\\%s"),TGGetPath(TG_PATH_IMAGE_OCR),strModel);	
	else
		strFolder.Format(_T("%s\\%s"),TGGetPath(TG_PATH_IMAGE_OCR),STR_DEFAULT);
	
	CTGFileOperation fo;
	if(!fo.IsFileFolder(strFolder))
		strFolder.Format(_T("%s\\%s"),TGGetPath(TG_PATH_IMAGE_OCR),STR_DEFAULT);	

	for(int i = 0 ; i < 10 ; i ++)
	{
		strFile.Format(_T("%s\\ocr_%d.bmp"),strFolder,i);
		int nRequiredSize = wcstombs(cFilePath, strFile, MAX_PATH); 
		pRsrNum[i] = cvLoadImage(cFilePath, -1);
	}	
	
	//-- 받은 이미지 이진화
	for(int i=0; i <10; i++)
		cvThreshold(pRsrNum[i], pRsrNum[i], 1, 255, CV_THRESH_BINARY);		
	//-- 번호 이미지 이진화
	for(int i=0; i <nLebelCnt; i++)
		cvThreshold(m_pImgNum[i], m_pImgNum[i], 1, 255, CV_THRESH_BINARY);	
	
	//-- Test
	//-- Save Change File		
	for(int i=0; i < nLebelCnt; i++)
	{
		strFile.Format(_T("%s\\Record\\test_%d.bmp"),TGGetPath(TG_PATH_IMAGE_OCR),i);	
		int nRequiredSize = wcstombs(cFilePath, strFile, MAX_PATH); 
		cvSaveImage(cFilePath,m_pImgNum[i]);		
	}
	
	

	for(int a=0; a<nLebelCnt; a++)
	{
		for(int i=0; i<10; i++)
		{
			count=0;
			for(int x=0; x<nOCR_W; x++)
			{
				for(int y=0; y<nOCR_H; y++)
				{
					int index = x + y *pRsrNum[i]->widthStep;
					unsigned char value = pRsrNum[i]->imageData[index];					
					int index2 = x + y *m_pImgNum[0]->widthStep;
					unsigned char value2 = m_pImgNum[a]->imageData[index2];					
					if(value < value2)
						count++;
				}
			}
			number_temp[i] = count;
			number_num[i]= i;
		}
		
		//버블정렬 결과
		for(int i=0; i<10; i++)
		{
			for(int j=0; j<10-i-1; j++)
			{
				if(number_temp[j] > number_temp[j+1])
				{
					number_num_temp = number_num[j];
					number_num[j] = number_num[j+1];
					number_num[j+1] = number_num_temp;
					
					number_temp_temp = number_temp[j];
					number_temp[j] = number_temp[j+1];
					number_temp[j+1] = number_temp_temp;
				}
			}
		}
	
		final[a] = number_num[0];
		TRACE("Select [%d]\n",final[a]);
		for(int i=0 ; i < 10; i ++)
			TRACE("Calculate [%d] / [%d] => [%d]\n",i, number_num[i],number_temp[i]);
	}

	//-- Set Load Data
	if(nLebelCnt == 14)
	{
		for(int i = 0; i < CNT_DATE; i ++)
			m_nDigit[i] = final[i];
	}	
	else
		bRet = FALSE;

	
	//사용한 이미지들을 릴리즈 해준다.
	for(int i=0; i<10; i++)
		cvReleaseImage(&pRsrNum[i]);		
	//-- 번호 이미지 이진화
	for(int i=0; i <nLebelCnt; i++)
		cvReleaseImage(&m_pImgNum[i]);

	Invalidate(FALSE);

	return bRet;
}

//------------------------------------------------------------------------------ 
//! @brief    Get OCR Check (Number)
//! @date     2011-11-29
//! @owner    hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT CTGImageManager::CompareTime(OCR_CHECK_INFO* pInfo)
{
	LONG ret = TG_ERR_OCR;
	//-- Change To Date/Time
	int nOCR[2][6];
	nOCR[0][0] =	m_nDigit[ 0]*1000	+ 
					m_nDigit[ 1]*100	+
					m_nDigit[ 2]*10		+
					m_nDigit[ 3];
	nOCR[0][1] =	m_nDigit[ 4]*10	+
					m_nDigit[ 5];
	nOCR[0][2] =	m_nDigit[ 6]*10	+
					m_nDigit[ 7];
	nOCR[0][3] = 	m_nDigit[ 8]*10	+
					m_nDigit[ 9];
	nOCR[0][4] =	m_nDigit[10]*10	+
					m_nDigit[11];
	nOCR[0][5] =	m_nDigit[12]*10	+
					m_nDigit[13];

	//-- Check Find Number
	CString	strOCR;
	strOCR.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"),
		nOCR[0][0], nOCR[0][1], nOCR[0][2], nOCR[0][3], nOCR[0][4], nOCR[0][5]);

	//-- 2011-12-1 hongsu.jung
	//-- Check Time
	if( !
		(
			(nOCR[0][0] >= 1900 && nOCR[0][0] <= 2100 )	&&
			(nOCR[0][1] >= 1 && nOCR[0][1] <= 12 )		&&		
			(nOCR[0][2] >= 1 && nOCR[0][2] <= 31 )		&&
			(nOCR[0][3] >= 0 && nOCR[0][3] <= 23 )		&&
			(nOCR[0][4] >= 0 && nOCR[0][4] <= 59 )		&&
			(nOCR[0][5] >= 0 && nOCR[0][5] <= 59 )	
		)
	  )
	{
		TGLog(1,_T("Date, Time OCR [%s]"),strOCR);
		return TG_ERR_OCR;		
	}	

	//-- 2012-1-2 hongsu.jung
	//-- Check Time Validation
	//-- Check Find Number
	((CTGImageFrameDlg*)GetParent())->ProgressIt(FALSE, strOCR);
	TGLog(1,_T("OCR Date/Time [%s]"),strOCR);

	//-- Check Time
	SYSTEMTIME tTimeOCR = {0,};
	tTimeOCR.wYear	= nOCR[0][0];
	tTimeOCR.wMonth = nOCR[0][1];
	tTimeOCR.wDay	= nOCR[0][2];
	tTimeOCR.wHour	= nOCR[0][3];
	tTimeOCR.wMinute= nOCR[0][4];
	tTimeOCR.wSecond= nOCR[0][5];
	//-- Get Compare Information
	nOCR[1][0] =pInfo->tTime.wYear;
	nOCR[1][1] =pInfo->tTime.wMonth;
	nOCR[1][2] =pInfo->tTime.wDay;
	nOCR[1][3] =pInfo->tTime.wHour;
	nOCR[1][4] =pInfo->tTime.wMinute;
	nOCR[1][5] =pInfo->tTime.wSecond;

	CString strCompare;
	strCompare.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"),
		nOCR[1][0], nOCR[1][1], nOCR[1][2], nOCR[1][3], nOCR[1][4], nOCR[1][5]);
	//-- 2011-12-1 hongsu.jung
	//-- Check Time
	if( (nOCR[1][0] < 1900 )					||
		(nOCR[1][1] < 1 && nOCR[1][1] > 12 )	||
		(nOCR[1][2] < 1 && nOCR[1][2] > 31 )	||
		(nOCR[1][3] < 0 && nOCR[1][3] > 23 )	||
		(nOCR[1][4] < 0 && nOCR[1][4] > 59 )	||
		(nOCR[1][5] < 0 && nOCR[1][5] > 59 ))
	{
		TGLog(0,_T("Unreable Date, Time Compare [%s]"),strCompare);
		return TG_ERR_OCR;
	}
	
	SYSTEMTIME tTimeCompare = {0,};
	tTimeCompare.wYear	= nOCR[1][0];
	tTimeCompare.wMonth = nOCR[1][1];
	tTimeCompare.wDay	= nOCR[1][2];
	tTimeCompare.wHour	= nOCR[1][3];
	tTimeCompare.wMinute= nOCR[1][4];
	tTimeCompare.wSecond= nOCR[1][5];
	TGLog(1,_T("Snapshot Path : %s"), pInfo->strFilePath);

	if(TGStringCompare(pInfo->strSequence, g_arPropertyDateTimeSeq[PROPERTY_DATE_TIME_SEQ_START]))
	{
		TGSetDSTStartOCRTime(tTimeOCR);
		TGSetDSTStartImagePath(pInfo->strFilePath);
	}
	else if(TGStringCompare(pInfo->strSequence, g_arPropertyDateTimeSeq[PROPERTY_DATE_TIME_SEQ_END]))
	{
		TGSetDSTEndOCRTime(tTimeOCR);
		TGSetDSTEndImagePath(pInfo->strFilePath);
	}

	return TGTimeCompare(tTimeCompare, tTimeOCR, TGGetValue(TG_OPT_TOLERANCE_OCR));
}


//------------------------------------------------------------------------------ 
//! @brief		  ClearImage
//! @date		    2010-9-14
//! @attention	none
//! @comment    Clear oracle or captured image
//------------------------------------------------------------------------------ 
void CTGImageManager::ClearImage()
{
	if(m_pDisplayImage)
		cvReleaseImage(&m_pDisplayImage);
	Invalidate();
}


BOOL CTGImageManager::LoadViewerImage(CvScalar color)
{
	//-- 2012-08-21 hongsu@esmlab.com
	//-- SetBackground Color 
	m_colorBK = color;

	//-- Captured Image	
	if(m_pDisplayImage)
		cvReleaseImage(&m_pDisplayImage);
	if(m_pCaptureImage)
		cvReleaseImage(&m_pCaptureImage);

	//[Client-Window] width, height
	CRect ClientRect;
	GetClientRect(&ClientRect);
	int nClientWidth  = ClientRect.Width();
	int nClientHeight = ClientRect.Height();

	m_pDisplayImage = cvCreateImage(cvSize(nClientWidth,nClientHeight), IPL_DEPTH_32F, 3);	

	//-- 2012-08-21 hongsu@esmlab.com
	//-- Set Color
	cvRectangle(m_pDisplayImage	,
		cvPoint(0,0),
		cvPoint(nClientWidth,nClientHeight),
		m_colorBK,
		CV_FILLED);
	//-- 2012-08-21 hongsu@esmlab.com
	//-- Set Point 
	m_pCaptureImage	= cvCloneImage( m_pDisplayImage );
	//Display Image
	if(!FitSizeShow())
		TGLog(1, _T("[IM] ERROR: FitSizeShow")) ;


	//-- 2010-9-14 keunbae.song
	//-- Clear captured window
	if(!m_pCaptureImage)
	{
		ClearImage();
		return FALSE; 
	}

	//Display Image
	if(!FitSizeShow())
	{
		TGLog(1, _T("[IM] ERROR: FitSizeShow \n")) ;
		return FALSE; 
	}

	return TRUE;
}


void CTGImageManager::DrawTimer(CString strTime)
{
	
	CvScalar color = cvScalar(
					m_colorBK.val[0],
					m_colorBK.val[1],
					m_colorBK.val[2]);
	CvScalar Fontcolor = cvScalar(
					255-m_colorBK.val[0],
					255-m_colorBK.val[1],
					255-m_colorBK.val[2]);

	LoadViewerImage(color);	

	if(!m_pDisplayImage)
		return;
	//-- 2012-08-21 hongsu@esmlab.com
	//-- Get Text Color	

	//-- 2012-07-12 hongsu@esmlab.com
	//-- Draw Information
	CvFont font;
	double hScale, vScale;
	int    lineWidth;


	//-- 2012-07-13 hongsu@esmlab.com
	//-- Set Size From Image Size	
	hScale = (double)m_pDisplayImage->width/800;
	vScale = (double)m_pDisplayImage->height/800;
	lineWidth = 1 + m_pDisplayImage->height/500;

	cvInitFont(&font,CV_FONT_HERSHEY_SIMPLEX, hScale,vScale,0,lineWidth);

	//-- 2012-07-12 hongsu@esmlab.com
	//-- Set String 
	char* chInfo = new char[STR_MAX_LEN];
	CvPoint ptCenter = cvPoint(m_pDisplayImage->width/2, m_pDisplayImage->height/2);
	size_t CharactersConverted = 0;


	wcstombs_s(&CharactersConverted, chInfo, strTime.GetLength()+1, strTime.GetBuffer(), _TRUNCATE);
	strTime.ReleaseBuffer();
	cvPutText (m_pDisplayImage,chInfo,ptCenter,&font,Fontcolor);

	delete [] chInfo;
	Invalidate(TRUE);
}
BOOL CTGImageManager::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
}
