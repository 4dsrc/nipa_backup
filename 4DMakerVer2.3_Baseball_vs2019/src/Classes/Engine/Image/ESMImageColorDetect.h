////////////////////////////////////////////////////////////////////////////////
//
//	TGImageColorDetect.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-11
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "cv.h"
#include "highgui.h"
#include "TGImageIndex.h"

class CTGImageColorDetect
{
public:

	CTGImageColorDetect();
	~CTGImageColorDetect();

private:
	int m_nType;
	IplImage* m_pResizeImg;
	//RECT m_rectPos;
	CvPoint m_pt[POINT_ALL];

	int m_nTolerance[COLOR_ALL][RANGE_ALL];
	int m_nRGBColor[COLOR_ALL];
	BOOL m_bMode;
	int m_nAllRGBCnt;
	int m_nDetectRGBCnt;
	int m_nColorTolerance;		// Difference of Color value

	float m_nScaleWidth;
	float m_nScaleHeight;

public:
	IplImage* GetResizeImg()	{ return m_pResizeImg; }

	int GetType()	{ return m_nType; }
	void SetInit(IplImage* pImg, int nColor);
	BOOL IsColorMode()		{ return m_bMode; }
	void SetAverageColor();
	CvScalar GetColor(int nType);
	void DrawLine(IplImage* pImg);
	void DrawInfo(IplImage* pImg);

	void AddTestCount()			{ m_nAllRGBCnt++; }
	void AddDetectCount()		{ m_nDetectRGBCnt++;}

	int GetColorTolerance()			{ return m_nColorTolerance; }
	void SetColorTolerance(int n)	{ m_nColorTolerance = n; }

	void SetPosition(int nX, int nY);
	CvPoint GetPosition(int nType)	{ return m_pt[nType]; }
	CvPoint GetCenterPosition();

	int GetRange(int nColor, int nRange)	{ return m_nTolerance[nColor][nRange];}
	void AddColor(int nColor, int nValue)	{ m_nRGBColor[nColor] += nValue; }
	
	float GetPercentage();

	BOOL CheckRange();
	BOOL CheckRGB();	
};