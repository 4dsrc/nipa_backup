#pragma once

//-- 2015-03-30 cygil@esmlab.com ImageLoader �߰�

#include "ESMDefine.h"
#include "FFmpegManager.h"
#include <map>
using namespace std;

class CImageLoader
{
public:
	CImageLoader(void);
	~CImageLoader(void);

	map<CString, vector<ST_IMAGELIST>> m_mapImageList;

	BOOL SetImageFromMovie(CString strMovieFile, int nStartTime, int nEndTime);
	BOOL IsMovieFile(int nDscIndex, int nSelectFIndex);
	BOOL IsImageFromList(CString strMovieFile, int nCheckTime);
	CBitmap* GetImageFromList(CString strMovieFile, int nGetTime, CDC* pDC, int nWidth, int nHeight);
	void ClearImageList(CString strMovieFile);
	void AllClearImageList();
};