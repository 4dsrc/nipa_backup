#include "StdAfx.h"

#include "ImageRevisionMgr.h"
#include "ESMUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

enum IMAREVICOLORINDEX {IMAREVI_RED, IMAREVI_GREEN, IMAREVI_BLUE, IMAREVI_COUNT};
#define MAXIMAGEREVICOUNT 1000

struct stMakeAvaraData
{
	ImageRevisionMgr* pImageRevisionMgr;
	CString strTarImage;
	CString strCompareImage1;
	CString strCompareImage2;
	int nXMargin;
	int nYMargin;
};

ImageRevisionMgr::ImageRevisionMgr(void)
{
	memset(m_nRevisionRed, 0, sizeof(m_nRevisionRed));
	memset(m_nRevisionGreen, 0, sizeof(m_nRevisionGreen));
	memset(m_nRevisionBlue, 0, sizeof(m_nRevisionBlue));
	memset(m_hMakeObjMovie, 0, sizeof(m_hMakeObjMovie));
	m_nMatchingPoitCount = 0;
	m_nThreadCount = 0;
	m_bMatchaingView = FALSE;
	InitializeCriticalSection (&crFileOpen);
}


ImageRevisionMgr::~ImageRevisionMgr(void)
{
	WaitForMultipleObjects(m_nThreadCount, m_hMakeObjMovie, TRUE, INFINITE);
	for( int i = 0 ;i < m_nThreadCount; i++)
	{	
		CloseHandle(m_hMakeObjMovie[i]);
		m_hMakeObjMovie[i] = NULL;
	}
	DeleteCriticalSection (&crFileOpen);
}

BOOL ImageRevisionMgr::PorcessRevision(CString strRefImage, CString strTarImage, int nDegree)
{
	vector<CPoint> arrSiftRefPoint;
	vector<CPoint> arrSiftTarPoint;

	vector<stRGB> arrTarPointRGB, arrRefPointRGB;
	double dRgbLSTSQData[IMAREVI_COUNT][MAXIMAGEREVICOUNT];

	m_strRefImage = strRefImage;
	m_strTarImage = strTarImage;

	GetSIFTPoint(m_strRefImage, m_strTarImage, &arrSiftRefPoint, &arrSiftTarPoint);		// SIFT Point 얻기

	GetPointRGB(m_strRefImage, m_strTarImage, &arrSiftRefPoint, &arrSiftTarPoint, &arrRefPointRGB, &arrTarPointRGB);	// Point 별 RGB 얻기

	GetRST( &arrTarPointRGB, &arrRefPointRGB, nDegree, dRgbLSTSQData);				// 인자 얻기

	MakeTable(nDegree, dRgbLSTSQData);			// RBG Table 만들기

	return TRUE;
}

BOOL ImageRevisionMgr::GetPointRGB(CString strRefImage, CString strTarImage, vector<CPoint>* arrSiftRefPoint, vector<CPoint>* arrSiftTarPoint,
	vector<stRGB>* pArrRefPointRGB, vector<stRGB>* pArrTarPointRGB)
{
	IplImage*	pRefPicture;
	IplImage*	pTarPicture;

	char pchPath[MAX_PATH] = {0};
	m_nMatchingPoitCount = 0;

	int nRequiredSize = (int)wcstombs(pchPath, strRefImage, MAX_PATH);
//	pRefPicture = cvLoadImage(pchPath);
	if(!pRefPicture)
	{
		//ESMLog(0,_T("Non Exsit Adjust Picture[%s]"),strRefImage);
		return FALSE;
	}

	nRequiredSize = (int)wcstombs(pchPath, strTarImage, MAX_PATH);
//	pTarPicture = cvLoadImage(pchPath);
	if(!pTarPicture)
	{
		//ESMLog(0,_T("Non Exsit Adjust Picture[%s]"),strRefImage);
		return FALSE;
	}

	stRGB tpRefRGB, tpTarRGB;
	CPoint tpRefPoint, tpTarPoint;
	for(int i =0 ;i < arrSiftRefPoint->size(); i++)
	{
		tpRefPoint = arrSiftRefPoint->at(i);
		tpRefRGB.nBlue	= (unsigned char)pRefPicture->imageData[tpRefPoint.y * pRefPicture->widthStep + tpRefPoint.x*3];		// Blue
		tpRefRGB.nGreen = (unsigned char)pRefPicture->imageData[tpRefPoint.y * pRefPicture->widthStep + tpRefPoint.x*3 + 1];	// Green
		tpRefRGB.nRed = (unsigned char)pRefPicture->imageData[tpRefPoint.y * pRefPicture->widthStep + tpRefPoint.x*3 + 2];	// Red

		tpTarPoint = arrSiftTarPoint->at(i);
		tpTarRGB.nBlue	= (unsigned char)pTarPicture->imageData[tpTarPoint.y * pTarPicture->widthStep + tpTarPoint.x*3];		// Blue
		tpTarRGB.nGreen = (unsigned char)pTarPicture->imageData[tpTarPoint.y * pTarPicture->widthStep + tpTarPoint.x*3 + 1];	// Green
		tpTarRGB.nRed = (unsigned char)pTarPicture->imageData[tpTarPoint.y * pTarPicture->widthStep + tpTarPoint.x*3 + 2];	// Red

		if( abs(tpRefRGB.nBlue - tpTarRGB.nBlue) < 30 && abs(tpRefRGB.nGreen - tpTarRGB.nGreen) < 30 &&abs(tpRefRGB.nRed - tpTarRGB.nRed) < 30)
		{
			m_nMatchingPoitCount++;
			pArrRefPointRGB->push_back(tpRefRGB);
			pArrTarPointRGB->push_back(tpTarRGB);
		}
	}

	cvReleaseImage(&pRefPicture);
	cvReleaseImage(&pTarPicture);
	return TRUE;
}

BOOL ImageRevisionMgr::GetRST(vector<stRGB>* pArrTarPointRGB, vector<stRGB>* pArrRefPointRGB, int nDegree, double dRgbLSTSQData[][MAXIMAGEREVICOUNT])
{
	int iDataSize = pArrTarPointRGB->size();
	double dRedDataX[2000], dRedDataY[2000], dGreenDataX[2000], dGreenDataY[2000], dBlueDataX[2000], dBlueDataY[2000];
	memset(dRedDataX, 0, sizeof(dRedDataX));
	memset(dRedDataY, 0, sizeof(dRedDataY));
	memset(dGreenDataX, 0, sizeof(dGreenDataX));
	memset(dGreenDataY, 0, sizeof(dGreenDataY));
	memset(dBlueDataX, 0, sizeof(dBlueDataX));
	memset(dBlueDataY, 0, sizeof(dBlueDataY));

	for(int i = 0; i< iDataSize; i++)
	{
		dRedDataX[i] = pArrRefPointRGB->at(i).nRed;
		dRedDataY[i] = pArrTarPointRGB->at(i).nRed;
		dGreenDataX[i] = pArrRefPointRGB->at(i).nGreen;
		dGreenDataY[i] = pArrTarPointRGB->at(i).nGreen;
		dBlueDataX[i] = pArrRefPointRGB->at(i).nBlue;
		dBlueDataY[i] = pArrTarPointRGB->at(i).nBlue;

	}

	int iError = lstsq(dRedDataY, dRedDataX ,nDegree, iDataSize, dRgbLSTSQData[IMAREVI_RED]); 
	if( iError != 0)
	{
		return FALSE;
	}
	iError = lstsq(dGreenDataY, dGreenDataX ,nDegree, iDataSize, dRgbLSTSQData[IMAREVI_GREEN]); 
	if( iError != 0)
	{
		return FALSE;
	}
	iError = lstsq(dBlueDataY, dBlueDataX ,nDegree, iDataSize, dRgbLSTSQData[IMAREVI_BLUE]); 
	if( iError != 0)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL ImageRevisionMgr::MakeTable(int nDegree, double dRgbLSTSQData[][MAXIMAGEREVICOUNT])
{
	double dTarData = 0.0;
	for(int iValue =0 ;iValue < 256; iValue++)
	{
		dTarData = 0.0;
		for(int n = 0; n <= nDegree; n++)
			dTarData += dRgbLSTSQData[IMAREVI_RED][n]*pow((double)iValue , n );

		if( dTarData < 0)
			dTarData = 0;
		if( dTarData > 255)
			dTarData = 255;
		m_nRevisionRed[iValue] = (int)(dTarData + 0.5) ;

		dTarData = 0.0;
		for(int n = 0; n <= nDegree; n++)
			dTarData += dRgbLSTSQData[IMAREVI_GREEN][n]*pow((double)iValue , n );

		if( dTarData < 0)
			dTarData = 0;
		if( dTarData > 255)
			dTarData = 255;
		m_nRevisionGreen[iValue] = (int)(dTarData + 0.5) ;

		dTarData = 0.0;
		for(int n = 0; n <= nDegree; n++)
			dTarData += dRgbLSTSQData[IMAREVI_BLUE][n]*pow((double)iValue , n );

		if( dTarData < 0)
			dTarData = 0;
		if( dTarData > 255)
			dTarData = 255;
		m_nRevisionBlue[iValue] = (int)(dTarData + 0.5) ;
	}
	return TRUE;
}

void ImageRevisionMgr::CallMakeAvaregeImage(CString strTarImage, CString strCompareImage1, CString strCompareImage2, int nXMargin, int nYMargin)
{
	stMakeAvaraData* pStMakeAvaraData;
	pStMakeAvaraData = new stMakeAvaraData;
	pStMakeAvaraData->pImageRevisionMgr = this;
	pStMakeAvaraData->strTarImage = strTarImage;
	pStMakeAvaraData->strCompareImage1 = strCompareImage1;
	pStMakeAvaraData->strCompareImage2 = strCompareImage2;
	pStMakeAvaraData->nXMargin = nXMargin;
	pStMakeAvaraData->nYMargin = nYMargin;
	m_hMakeObjMovie[m_nThreadCount++] = (HANDLE) _beginthreadex(NULL, 0, MakeAvaregeThread, (void *)pStMakeAvaraData, 0, NULL);
}

unsigned WINAPI ImageRevisionMgr::MakeAvaregeThread(LPVOID param)
{
	stMakeAvaraData* pStMakeAvaraData = (stMakeAvaraData*)param;
	ImageRevisionMgr* pImageRevisionMgr = pStMakeAvaraData->pImageRevisionMgr;
	CString strTarImage = pStMakeAvaraData->strTarImage;
	CString strCompareImage1 = pStMakeAvaraData->strCompareImage1;
	CString strCompareImage2 = pStMakeAvaraData->strCompareImage2;
	int nXMargin = pStMakeAvaraData->nXMargin;
	int nYMargin = pStMakeAvaraData->nYMargin;
	
	pImageRevisionMgr->MakeAvaregeImage(strTarImage, strCompareImage1, strCompareImage2, nXMargin, nYMargin);
	delete pStMakeAvaraData;
	pStMakeAvaraData = NULL;
	return 0;
}

BOOL ImageRevisionMgr::MakeAvaregeImage(CString strTarImage, CString strCompareImage1, CString strCompareImage2, int nXMargin, int nYMargin)
{
	if(strTarImage == _T("") ||(strCompareImage1 == _T("") && strCompareImage2 == _T("")))
	{
		return FALSE;
	}

	char pchPath[MAX_PATH] = {0};

	int arrGabBlue[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrGabRed[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrGabGreen[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT];
	memset(arrGabBlue, 0, sizeof(arrGabBlue));
	memset(arrGabRed, 0, sizeof(arrGabRed));
	memset(arrGabGreen, 0, sizeof(arrGabGreen));

	int arrTarBlue[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrTarRed[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrTarGreen[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT];
	memset(arrTarBlue, 0, sizeof(arrTarBlue));
	memset(arrTarRed, 0, sizeof(arrTarRed));
	memset(arrTarGreen, 0, sizeof(arrTarGreen));

	int arrCom1Blue[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrCom1Red[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrCom1Green[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT];
	memset(arrCom1Blue, 0, sizeof(arrCom1Blue));
	memset(arrCom1Red, 0, sizeof(arrCom1Red));
	memset(arrCom1Green, 0, sizeof(arrCom1Green));

	int arrCom2Blue[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrCom2Red[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrCom2Green[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT];
	memset(arrCom2Blue, 0, sizeof(arrCom2Blue));
	memset(arrCom2Red, 0, sizeof(arrCom2Red));
	memset(arrCom2Green, 0, sizeof(arrCom2Green));


	//EnterCriticalSection (&crFileOpen);
	int nImageCount = 1;
	GetAvgRGB(strTarImage, IMAGEHIGHTDIVEDECOUNT, IMAGEWIDTHDIVIDECOUNT, arrTarBlue, arrTarRed, arrTarGreen, nXMargin, nYMargin);
	if( strCompareImage1 != _T(""))
	{
		if(GetAvgRGB(strCompareImage1, IMAGEHIGHTDIVEDECOUNT, IMAGEWIDTHDIVIDECOUNT, arrCom1Blue, arrCom1Red, arrCom1Green, nXMargin, nYMargin))
			nImageCount++;
	}
	if( strCompareImage2 != _T(""))
	{
		if(GetAvgRGB(strCompareImage2, IMAGEHIGHTDIVEDECOUNT, IMAGEWIDTHDIVIDECOUNT, arrCom2Blue, arrCom2Red, arrCom2Green, nXMargin, nYMargin))
			nImageCount++;
	}
//	LeaveCriticalSection (&crFileOpen);

	//세영상의 구간별 RGB 평균
	CString strPath, strFileName, strAdjustFile, WriteData, strPictureName;
	ESMUtil::GetFileNameAndPath(strTarImage, strFileName, strPath );
	strPictureName = strFileName;
	strPictureName = strPictureName.Left(strFileName.GetLength() - 4);
	strPath = strPath.Left(strPath.GetLength() - 1);
	ESMUtil::GetFileNameAndPath(strPath, strFileName, strPath );

	for( int i =0  ;i < IMAGEHIGHTDIVEDECOUNT; i++)
	{
		for (int j =0 ; j< IMAGEWIDTHDIVIDECOUNT; j++)
		{
			//세영상의 구간별 RGB 평균
			arrGabBlue[i][j] = (arrTarBlue[i][j] + arrCom1Blue[i][j] + arrCom2Blue[i][j])  / nImageCount;
			arrGabRed[i][j] = (arrTarRed[i][j] + arrCom1Red[i][j] + arrCom2Red[i][j])  / nImageCount;
			arrGabGreen[i][j] = (arrTarGreen[i][j] + arrCom1Green[i][j] + arrCom2Green[i][j])  / nImageCount;

			// 구간별 원영상과 평균의 Gab 
			arrGabBlue[i][j] = arrGabBlue[i][j] - arrTarBlue[i][j];
			arrGabRed[i][j] = arrGabRed[i][j] - arrTarRed[i][j];
			arrGabGreen[i][j] = arrGabGreen[i][j] - arrTarGreen[i][j];
		}
	}
	int j = 0;
	int nPixel = 0, bNextPixel = 0;
	int nMaxGab = 1;
	for( int i =0  ;i < IMAGEHIGHTDIVEDECOUNT; i++)
	{
		for (j =0 ; j< IMAGEWIDTHDIVIDECOUNT-1; j++)
		{
			nPixel = arrGabBlue[i][j];
			bNextPixel = arrGabBlue[i][j + 1];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabBlue[i][j + 1] = nPixel - nMaxGab;
				else
					arrGabBlue[i][j + 1] = nPixel + nMaxGab;
			}

			nPixel = arrGabRed[i][j];
			bNextPixel = arrGabRed[i][j + 1];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabRed[i][j + 1] = nPixel - nMaxGab;
				else
					arrGabRed[i][j + 1] = nPixel + nMaxGab;
			}

			nPixel = arrGabGreen[i][j];
			bNextPixel = arrGabGreen[i][j + 1];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabGreen[i][j + 1] = nPixel - nMaxGab;
				else
					arrGabGreen[i][j + 1] = nPixel + nMaxGab;
			}

		}
		if( i != IMAGEHIGHTDIVEDECOUNT - 1)
		{
			if(abs(arrGabBlue[i][j]) - abs(arrGabBlue[i + 1][j]) > nMaxGab)
			{
				if( arrGabBlue[i][j] > arrGabBlue[i + 1][j ])
					arrGabBlue[i][j + 1] = arrGabBlue[i][j] - nMaxGab;
				else
					arrGabBlue[i][j + 1] = arrGabBlue[i][j] + nMaxGab;
			}

			if(abs(arrGabRed[i][j]) - abs(arrGabRed[i + 1][j]) > nMaxGab)
			{
				if( arrGabRed[i][j] > arrGabRed[i + 1][j])
					arrGabRed[i][j + 1] = arrGabRed[i][j] - nMaxGab;
				else
					arrGabRed[i][j + 1] = arrGabRed[i][j] + nMaxGab;
			}

			if(abs(arrGabGreen[i][j]) - abs(arrGabGreen[i + 1][j]) > nMaxGab)
			{
				if( arrGabGreen[i][j] > arrGabGreen[i + 1][j])
					arrGabGreen[i][j + 1] = arrGabGreen[i][j] - nMaxGab;
				else
					arrGabGreen[i][j + 1] = arrGabGreen[i][j] + nMaxGab;
			}
		}
	}
	for( int i =0  ;i < IMAGEHIGHTDIVEDECOUNT-1; i++)
	{
		for (j =0 ; j< IMAGEWIDTHDIVIDECOUNT; j++)
		{
			nPixel = arrGabBlue[i][j];
			bNextPixel = arrGabBlue[i + 1][j];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabBlue[i + 1][j] = nPixel - nMaxGab;
				else
					arrGabBlue[i + 1][j] = nPixel + nMaxGab;
			}

			nPixel = arrGabRed[i][j];
			bNextPixel = arrGabRed[i + 1][j];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabRed[i + 1][j] = nPixel - nMaxGab;
				else
					arrGabRed[i + 1][j] = nPixel + nMaxGab;
			}

			nPixel = arrGabGreen[i][j];
			bNextPixel = arrGabGreen[i + 1][j];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabGreen[i + 1][j] = nPixel - nMaxGab;
				else
					arrGabGreen[i + 1][j] = nPixel + nMaxGab;
			}

		}
		if( j != IMAGEHIGHTDIVEDECOUNT - 1)
		{
			if(abs(arrGabBlue[i][j]) - abs(arrGabBlue[i][j + 1]) > nMaxGab)
			{
				if( arrGabBlue[i][j] > arrGabBlue[i][j + 1 ])
					arrGabBlue[i + 1][j] = arrGabBlue[i][j] - nMaxGab;
				else
					arrGabBlue[i + 1][j] = arrGabBlue[i][j] + nMaxGab;
			}

			if(abs(arrGabRed[i][j]) - abs(arrGabRed[i][j + 1]) > nMaxGab)
			{
				if( arrGabRed[i][j] > arrGabRed[i][j + 1])
					arrGabRed[i + 1][j] = arrGabRed[i][j] - nMaxGab;
				else
					arrGabRed[i + 1][j] = arrGabRed[i][j] + nMaxGab;
			}

			if(abs(arrGabGreen[i][j]) - abs(arrGabGreen[i][j + 1]) > nMaxGab)
			{
				if( arrGabGreen[i][j] > arrGabGreen[i][j + 1])
					arrGabGreen[i + 1][j] = arrGabGreen[i][j] - nMaxGab;
				else
					arrGabGreen[i + 1][j] = arrGabGreen[i][j] + nMaxGab;
			}
		}
	}
// 	//Table 출력 확인용.
// 	CFile WriteFileRed, WriteFileBlue, WriteFileGreen;
// 	strFileName.Format(_T("C:\\TestImage\\%sRed.csv"), strPictureName);
// 	if (!WriteFileRed.Open(strFileName, CFile::modeWrite | CFile::modeCreate | CFile::modeNoTruncate))
// 	{
// 		return FALSE;
// 	}
// 	strFileName.Format(_T("C:\\TestImage\\%sBlue.csv"), strPictureName);
// 	if (!WriteFileBlue.Open(strFileName, CFile::modeWrite | CFile::modeCreate | CFile::modeNoTruncate))
// 	{
// 		return FALSE;
// 	}
// 	strFileName.Format(_T("C:\\TestImage\\%sGreen.csv"), strPictureName);
// 	if (!WriteFileGreen.Open(strFileName, CFile::modeWrite | CFile::modeCreate | CFile::modeNoTruncate))
// 	{
// 		return FALSE;
// 	}
// 	for( int i =0  ;i < IMAGEWIDTHDIVIDECOUNT-1; i++)
// 	{
// 		for (j =0 ; j< IMAGEHIGHTDIVEDECOUNT; j++)
// 		{
// 			CString WriteData;
// 
// 			WriteData.Format(_T(",%d"), arrGabRed[i][j]);
// 			WriteFileRed.Write(WriteData, WriteData.GetLength()*2);
// 			WriteData.Format(_T(",%d"), arrGabBlue[i][j]);
// 			WriteFileBlue.Write(WriteData, WriteData.GetLength()*2);
// 			WriteData.Format(_T(",%d"), arrGabGreen[i][j]);
// 			WriteFileGreen.Write(WriteData, WriteData.GetLength()*2);
// 
// 		}
// 		WriteData.Format(_T("\r\n"));
// 		WriteFileRed.Write(WriteData, WriteData.GetLength()*2);
// 		WriteFileBlue.Write(WriteData, WriteData.GetLength()*2);
// 		WriteFileGreen.Write(WriteData, WriteData.GetLength()*2);
// 	}
// 	WriteFileRed.Close();
// 	WriteFileBlue.Close();
// 	WriteFileGreen.Close();
	MakeResultImage(strTarImage, IMAGEHIGHTDIVEDECOUNT, IMAGEWIDTHDIVIDECOUNT, arrGabBlue, arrGabRed, arrGabGreen, nXMargin, nYMargin);
	return TRUE;
}

BOOL ImageRevisionMgr::MakeAvaregeImage(IplImage* pCvImage, IplImage* pCvCompareImage1, IplImage* pCvCompareImage2, int nXMargin, int nYMargin)
{
	if(pCvImage == NULL ||(pCvCompareImage1 == NULL && pCvCompareImage2 == NULL))
	{
		return FALSE;
	}

	char pchPath[MAX_PATH] = {0};

	int arrGabBlue[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrGabRed[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrGabGreen[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT];
	memset(arrGabBlue, 0, sizeof(arrGabBlue));
	memset(arrGabRed, 0, sizeof(arrGabRed));
	memset(arrGabGreen, 0, sizeof(arrGabGreen));

	int arrTarBlue[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrTarRed[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrTarGreen[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT];
	memset(arrTarBlue, 0, sizeof(arrTarBlue));
	memset(arrTarRed, 0, sizeof(arrTarRed));
	memset(arrTarGreen, 0, sizeof(arrTarGreen));

	int arrCom1Blue[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrCom1Red[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrCom1Green[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT];
	memset(arrCom1Blue, 0, sizeof(arrCom1Blue));
	memset(arrCom1Red, 0, sizeof(arrCom1Red));
	memset(arrCom1Green, 0, sizeof(arrCom1Green));

	int arrCom2Blue[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrCom2Red[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT], arrCom2Green[IMAGEHIGHTDIVEDECOUNT][IMAGEWIDTHDIVIDECOUNT];
	memset(arrCom2Blue, 0, sizeof(arrCom2Blue));
	memset(arrCom2Red, 0, sizeof(arrCom2Red));
	memset(arrCom2Green, 0, sizeof(arrCom2Green));

	int nImageCount = 1;
	GetAvgRGB(pCvImage, IMAGEHIGHTDIVEDECOUNT, IMAGEWIDTHDIVIDECOUNT, arrTarBlue, arrTarRed, arrTarGreen, nXMargin, nYMargin);
	if( pCvCompareImage1 != NULL)
	{
		if(GetAvgRGB(pCvCompareImage1, IMAGEHIGHTDIVEDECOUNT, IMAGEWIDTHDIVIDECOUNT, arrCom1Blue, arrCom1Red, arrCom1Green, nXMargin, nYMargin))
			nImageCount++;
	}
	if( pCvCompareImage2 != NULL)
	{
		if(GetAvgRGB(pCvCompareImage2, IMAGEHIGHTDIVEDECOUNT, IMAGEWIDTHDIVIDECOUNT, arrCom2Blue, arrCom2Red, arrCom2Green, nXMargin, nYMargin))
			nImageCount++;
	}

	for( int i =0  ;i < IMAGEHIGHTDIVEDECOUNT; i++)
	{
		for (int j =0 ; j< IMAGEWIDTHDIVIDECOUNT; j++)
		{
			//세영상의 구간별 RGB 평균
			arrGabBlue[i][j] = (arrTarBlue[i][j] + arrCom1Blue[i][j] + arrCom2Blue[i][j])  / nImageCount;
			arrGabRed[i][j] = (arrTarRed[i][j] + arrCom1Red[i][j] + arrCom2Red[i][j])  / nImageCount;
			arrGabGreen[i][j] = (arrTarGreen[i][j] + arrCom1Green[i][j] + arrCom2Green[i][j])  / nImageCount;

			// 구간별 원영상과 평균의 Gab 
			arrGabBlue[i][j] = arrGabBlue[i][j] - arrTarBlue[i][j];
			arrGabRed[i][j] = arrGabRed[i][j] - arrTarRed[i][j];
			arrGabGreen[i][j] = arrGabGreen[i][j] - arrTarGreen[i][j];
		}
	}
	int j = 0;
	int nPixel = 0, bNextPixel = 0;
	int nMaxGab = 1;
	for( int i =0  ;i < IMAGEHIGHTDIVEDECOUNT; i++)
	{
		for (j =0 ; j< IMAGEWIDTHDIVIDECOUNT-1; j++)
		{
			nPixel = arrGabBlue[i][j];
			bNextPixel = arrGabBlue[i][j + 1];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabBlue[i][j + 1] = nPixel - nMaxGab;
				else
					arrGabBlue[i][j + 1] = nPixel + nMaxGab;
			}

			nPixel = arrGabRed[i][j];
			bNextPixel = arrGabRed[i][j + 1];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabRed[i][j + 1] = nPixel - nMaxGab;
				else
					arrGabRed[i][j + 1] = nPixel + nMaxGab;
			}

			nPixel = arrGabGreen[i][j];
			bNextPixel = arrGabGreen[i][j + 1];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabGreen[i][j + 1] = nPixel - nMaxGab;
				else
					arrGabGreen[i][j + 1] = nPixel + nMaxGab;
			}

		}
		if( i != IMAGEHIGHTDIVEDECOUNT - 1)
		{
			if(abs(arrGabBlue[i][j]) - abs(arrGabBlue[i + 1][j]) > nMaxGab)
			{
				if( arrGabBlue[i][j] > arrGabBlue[i + 1][j ])
					arrGabBlue[i][j + 1] = arrGabBlue[i][j] - nMaxGab;
				else
					arrGabBlue[i][j + 1] = arrGabBlue[i][j] + nMaxGab;
			}

			if(abs(arrGabRed[i][j]) - abs(arrGabRed[i + 1][j]) > nMaxGab)
			{
				if( arrGabRed[i][j] > arrGabRed[i + 1][j])
					arrGabRed[i][j + 1] = arrGabRed[i][j] - nMaxGab;
				else
					arrGabRed[i][j + 1] = arrGabRed[i][j] + nMaxGab;
			}

			if(abs(arrGabGreen[i][j]) - abs(arrGabGreen[i + 1][j]) > nMaxGab)
			{
				if( arrGabGreen[i][j] > arrGabGreen[i + 1][j])
					arrGabGreen[i][j + 1] = arrGabGreen[i][j] - nMaxGab;
				else
					arrGabGreen[i][j + 1] = arrGabGreen[i][j] + nMaxGab;
			}
		}
	}
	for( int i =0  ;i < IMAGEHIGHTDIVEDECOUNT-1; i++)
	{
		for (j =0 ; j< IMAGEWIDTHDIVIDECOUNT; j++)
		{
			nPixel = arrGabBlue[i][j];
			bNextPixel = arrGabBlue[i + 1][j];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabBlue[i + 1][j] = nPixel - nMaxGab;
				else
					arrGabBlue[i + 1][j] = nPixel + nMaxGab;
			}

			nPixel = arrGabRed[i][j];
			bNextPixel = arrGabRed[i + 1][j];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabRed[i + 1][j] = nPixel - nMaxGab;
				else
					arrGabRed[i + 1][j] = nPixel + nMaxGab;
			}

			nPixel = arrGabGreen[i][j];
			bNextPixel = arrGabGreen[i + 1][j];
			if(abs(nPixel - bNextPixel) > nMaxGab)
			{
				if( nPixel > bNextPixel)
					arrGabGreen[i + 1][j] = nPixel - nMaxGab;
				else
					arrGabGreen[i + 1][j] = nPixel + nMaxGab;
			}

		}
		if( j != IMAGEHIGHTDIVEDECOUNT - 1)
		{
			if(abs(arrGabBlue[i][j]) - abs(arrGabBlue[i][j + 1]) > nMaxGab)
			{
				if( arrGabBlue[i][j] > arrGabBlue[i][j + 1 ])
					arrGabBlue[i + 1][j] = arrGabBlue[i][j] - nMaxGab;
				else
					arrGabBlue[i + 1][j] = arrGabBlue[i][j] + nMaxGab;
			}

			if(abs(arrGabRed[i][j]) - abs(arrGabRed[i][j + 1]) > nMaxGab)
			{
				if( arrGabRed[i][j] > arrGabRed[i][j + 1])
					arrGabRed[i + 1][j] = arrGabRed[i][j] - nMaxGab;
				else
					arrGabRed[i + 1][j] = arrGabRed[i][j] + nMaxGab;
			}

			if(abs(arrGabGreen[i][j]) - abs(arrGabGreen[i][j + 1]) > nMaxGab)
			{
				if( arrGabGreen[i][j] > arrGabGreen[i][j + 1])
					arrGabGreen[i + 1][j] = arrGabGreen[i][j] - nMaxGab;
				else
					arrGabGreen[i + 1][j] = arrGabGreen[i][j] + nMaxGab;
			}
		}
	}
	MakeResultImage(pCvImage, IMAGEHIGHTDIVEDECOUNT, IMAGEWIDTHDIVIDECOUNT, arrGabBlue, arrGabRed, arrGabGreen, nXMargin, nYMargin);
	return TRUE;
}

void ImageRevisionMgr::GetFileNameAndPath(CString strFullPath, CString &FileName,CString &strPath)
{
	int index = strFullPath.ReverseFind(_T('\\'));
	strPath = strFullPath.Left(index+1);
	FileName = strFullPath.Mid(index+1);
}


BOOL ImageRevisionMgr::MakeResultImage(CString strImage, int nHightDivCount, int nWidhtDivCount, 
	int arrGabBlue[][IMAGEWIDTHDIVIDECOUNT], int arrGabRed[][IMAGEWIDTHDIVIDECOUNT], int arrGabGreen[][IMAGEWIDTHDIVIDECOUNT] , int nXMargin, int nYMargin )
{
	char pchPath[MAX_PATH] = {0};
	int nRequiredSize = (int)wcstombs(pchPath, strImage, MAX_PATH);
	IplImage*	pPicture = NULL;
//	if ((pPicture = cvLoadImage(pchPath)) == 0) 
	{
		return FALSE;
	}

	double nHightCoeff = double(pPicture->height - nYMargin * 2)/ (double)nHightDivCount;
	double nWidthCoeff = double(pPicture->width - nXMargin * 2) / (double)nWidhtDivCount;
	for(int nHiDivIndex = 0; nHiDivIndex < nHightDivCount; nHiDivIndex++)
	{
		for(int nWiDivIndex = 0; nWiDivIndex < nWidhtDivCount; nWiDivIndex++)
		{
			for (int nHightIndex = int(nHightCoeff * nHiDivIndex) + nYMargin; nHightIndex < int(nHightCoeff * (nHiDivIndex + 1)) + nYMargin; nHightIndex++ )
			{
				for (int nWidthIndex = int(nWidthCoeff * nWiDivIndex) + nXMargin; nWidthIndex < int(nWidthCoeff * (nWiDivIndex + 1))  + nXMargin; nWidthIndex++ )
				{
					int nBlue = (unsigned char)pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3];
					if( nBlue + arrGabBlue[nHiDivIndex][nWiDivIndex] > 255)
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 ] = (char)255;
					else if(nBlue + arrGabBlue[nHiDivIndex][nWiDivIndex] < 0)
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 ] = (char)0;
					else
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 ] = nBlue + arrGabBlue[nHiDivIndex][nWiDivIndex];			//Blue

					int nGreen = (unsigned char)pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3+ 1];
					if(nGreen + arrGabGreen[nHiDivIndex][nWiDivIndex] > 255 )
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 1] = (char)255;
					else if(nGreen + arrGabGreen[nHiDivIndex][nWiDivIndex] < 0)
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 1] = (char)0;
					else
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 1] = nGreen + arrGabGreen[nHiDivIndex][nWiDivIndex];		//Green

					int nRed = (unsigned char)pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3+ 2];
					if( nRed + arrGabRed[nHiDivIndex][nWiDivIndex] > 255 )
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 2] = (char)255;
					else if(nRed + arrGabRed[nHiDivIndex][nWiDivIndex] < 0)
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 2] = (char)0;
					else
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 2] = nRed + arrGabRed[nHiDivIndex][nWiDivIndex];		//Red

				}
			}
		}
	}

	CString strOutImage, strPath, strFileName, strAdjustFile;
	GetFileNameAndPath(strImage, strFileName, strPath );
	strFileName = _T("M_") + strFileName;
	strOutImage = strPath + strFileName;

	//-- Save File
	wcstombs(pchPath, strOutImage, MAX_PATH);

	//cvSaveImage(pchPath, pPicture);
	cvReleaseImage(&pPicture);

	return TRUE;
}

BOOL ImageRevisionMgr::MakeResultImage(IplImage*	pPicture, int nHightDivCount, int nWidhtDivCount, 
	int arrGabBlue[][IMAGEWIDTHDIVIDECOUNT], int arrGabRed[][IMAGEWIDTHDIVIDECOUNT], int arrGabGreen[][IMAGEWIDTHDIVIDECOUNT] , int nXMargin, int nYMargin )
{
	double nHightCoeff = double(pPicture->height - nYMargin * 2)/ (double)nHightDivCount;
	double nWidthCoeff = double(pPicture->width - nXMargin * 2) / (double)nWidhtDivCount;
	for(int nHiDivIndex = 0; nHiDivIndex < nHightDivCount; nHiDivIndex++)
	{
		for(int nWiDivIndex = 0; nWiDivIndex < nWidhtDivCount; nWiDivIndex++)
		{
			for (int nHightIndex = int(nHightCoeff * nHiDivIndex) + nYMargin; nHightIndex < int(nHightCoeff * (nHiDivIndex + 1)) + nYMargin; nHightIndex++ )
			{
				for (int nWidthIndex = int(nWidthCoeff * nWiDivIndex) + nXMargin; nWidthIndex < int(nWidthCoeff * (nWiDivIndex + 1))  + nXMargin; nWidthIndex++ )
				{
					int nBlue = (unsigned char)pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3];
					if( nBlue + arrGabBlue[nHiDivIndex][nWiDivIndex] > 255)
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 ] = (char)255;
					else if(nBlue + arrGabBlue[nHiDivIndex][nWiDivIndex] < 0)
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 ] = (char)0;
					else
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 ] = nBlue + arrGabBlue[nHiDivIndex][nWiDivIndex];			//Blue

					int nGreen = (unsigned char)pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3+ 1];
					if(nGreen + arrGabGreen[nHiDivIndex][nWiDivIndex] > 255 )
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 1] = (char)255;
					else if(nGreen + arrGabGreen[nHiDivIndex][nWiDivIndex] < 0)
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 1] = (char)0;
					else
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 1] = nGreen + arrGabGreen[nHiDivIndex][nWiDivIndex];		//Green

					int nRed = (unsigned char)pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3+ 2];
					if( nRed + arrGabRed[nHiDivIndex][nWiDivIndex] > 255 )
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 2] = (char)255;
					else if(nRed + arrGabRed[nHiDivIndex][nWiDivIndex] < 0)
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 2] = (char)0;
					else
						pPicture->imageData[nHightIndex*pPicture->widthStep + nWidthIndex*3 + 2] = nRed + arrGabRed[nHiDivIndex][nWiDivIndex];		//Red

				}
			}
		}
	}
	return TRUE;
}

BOOL ImageRevisionMgr::GetAvgRGB(CString strImage, int nHightDivCount, int nWidhtDivCount, 
	int nAvgBlue[][IMAGEWIDTHDIVIDECOUNT], int nAvgRed[][IMAGEWIDTHDIVIDECOUNT], int nAvgGreen[][IMAGEWIDTHDIVIDECOUNT] , int nXMargin, int nYMargin)
{
	char pchPath[MAX_PATH] = {0};
	int nRequiredSize = (int)wcstombs(pchPath, strImage, MAX_PATH);
	IplImage*	pPicture = NULL;
//	if ((pPicture = cvLoadImage(pchPath)) == 0) 
	{
		return FALSE;
	}

	double nHightCoeff = double(pPicture->height - nYMargin * 2)/ (double)nHightDivCount;
	double nWidthCoeff = double(pPicture->width - nXMargin * 2) / (double)nWidhtDivCount;
	for(int nHiDivIndex = 0; nHiDivIndex < nHightDivCount; nHiDivIndex++)
	{
		for(int nWiDivIndex = 0; nWiDivIndex < nWidhtDivCount; nWiDivIndex++)
		{
			for (int nHightIndex = int(nHightCoeff * nHiDivIndex) + nYMargin; nHightIndex < int(nHightCoeff * (nHiDivIndex + 1)) + nYMargin; nHightIndex++ )
			{
				for (int nWidthIndex = int(nWidthCoeff * nWiDivIndex) + nXMargin; nWidthIndex < int(nWidthCoeff * (nWiDivIndex + 1))  + nXMargin; nWidthIndex++ )
				{
					nAvgBlue[nHiDivIndex][nWiDivIndex] += (unsigned char)pPicture->imageData[nHightIndex * pPicture->widthStep + nWidthIndex *3 ];		// Blue
					nAvgRed[nHiDivIndex][nWiDivIndex] +=(unsigned char)pPicture->imageData[nHightIndex * pPicture->widthStep + nWidthIndex * 3 + 2];	// Red
					nAvgGreen[nHiDivIndex][nWiDivIndex] +=(unsigned char)pPicture->imageData[nHightIndex * pPicture->widthStep + nWidthIndex * 3 + 1];	// Green
				}
			}
			nAvgBlue[nHiDivIndex][nWiDivIndex] = nAvgBlue[nHiDivIndex][nWiDivIndex] / (int)(nHightCoeff * nWidthCoeff);
			nAvgRed[nHiDivIndex][nWiDivIndex] = nAvgRed[nHiDivIndex][nWiDivIndex] / (int)(nHightCoeff * nWidthCoeff);
			nAvgGreen[nHiDivIndex][nWiDivIndex] = nAvgGreen[nHiDivIndex][nWiDivIndex] / (int)(nHightCoeff * nWidthCoeff);
		}
	}
	cvReleaseImage(&pPicture);
	return TRUE;
}

BOOL ImageRevisionMgr::GetAvgRGB(IplImage*	pPicture, int nHightDivCount, int nWidhtDivCount, 
	int nAvgBlue[][IMAGEWIDTHDIVIDECOUNT], int nAvgRed[][IMAGEWIDTHDIVIDECOUNT], int nAvgGreen[][IMAGEWIDTHDIVIDECOUNT] , int nXMargin, int nYMargin)
{
	double nHightCoeff = double(pPicture->height - nYMargin * 2)/ (double)nHightDivCount;
	double nWidthCoeff = double(pPicture->width - nXMargin * 2) / (double)nWidhtDivCount;
	for(int nHiDivIndex = 0; nHiDivIndex < nHightDivCount; nHiDivIndex++)
	{
		for(int nWiDivIndex = 0; nWiDivIndex < nWidhtDivCount; nWiDivIndex++)
		{
			for (int nHightIndex = int(nHightCoeff * nHiDivIndex) + nYMargin; nHightIndex < int(nHightCoeff * (nHiDivIndex + 1)) + nYMargin; nHightIndex++ )
			{
				for (int nWidthIndex = int(nWidthCoeff * nWiDivIndex) + nXMargin; nWidthIndex < int(nWidthCoeff * (nWiDivIndex + 1))  + nXMargin; nWidthIndex++ )
				{
					nAvgBlue[nHiDivIndex][nWiDivIndex] += (unsigned char)pPicture->imageData[nHightIndex * pPicture->widthStep + nWidthIndex *3 ];		// Blue
					nAvgRed[nHiDivIndex][nWiDivIndex] +=(unsigned char)pPicture->imageData[nHightIndex * pPicture->widthStep + nWidthIndex * 3 + 2];	// Red
					nAvgGreen[nHiDivIndex][nWiDivIndex] +=(unsigned char)pPicture->imageData[nHightIndex * pPicture->widthStep + nWidthIndex * 3 + 1];	// Green
				}
			}
			nAvgBlue[nHiDivIndex][nWiDivIndex] = nAvgBlue[nHiDivIndex][nWiDivIndex] / (int)(nHightCoeff * nWidthCoeff);
			nAvgRed[nHiDivIndex][nWiDivIndex] = nAvgRed[nHiDivIndex][nWiDivIndex] / (int)(nHightCoeff * nWidthCoeff);
			nAvgGreen[nHiDivIndex][nWiDivIndex] = nAvgGreen[nHiDivIndex][nWiDivIndex] / (int)(nHightCoeff * nWidthCoeff);
		}
	}
	return TRUE;
}

BOOL ImageRevisionMgr::GetSIFTPoint(CString strRefImage, CString strTarImage, vector<CPoint>* arrSiftRefPoint, vector<CPoint>* arrSiftTarPoint)
{
	char RefImage[255];
	char TarImage[255];

	sprintf(RefImage,"%S",strRefImage);
	sprintf(TarImage,"%S",strTarImage);

	Mat img1 = imread(RefImage, cv::IMREAD_GRAYSCALE);
	Mat img2 = imread(TarImage, cv::IMREAD_GRAYSCALE);

	if(img1.empty() || img2.empty())
	{
		printf("Can't read one of the images\n");
		return FALSE;
	}

	

	// detecting keypoints
	//SurfFeatureDetector detector(100);
	Ptr<cv::xfeatures2d::SURF> detector = xfeatures2d::SURF::create(100);	
	vector<KeyPoint> keypoints1, keypoints2;
	detector->detect(img1, keypoints1);
	detector->detect(img2, keypoints2);	
	
	
	// computing descriptors
	//SurfDescriptorExtractor extractor;	
	Ptr<xfeatures2d::SURF> extractor = xfeatures2d::SURF::create();
	Mat descriptors1, descriptors2;
	extractor->compute(img1, keypoints1, descriptors1);
	extractor->compute(img2, keypoints2, descriptors2);

	// matching descriptors
	BFMatcher matcher(NORM_L2);
	vector<DMatch> matches;
	matcher.match(descriptors1, descriptors2, matches);

	vector<DMatch> tpMatches;
	for(int i =0 ;i < matches.size(); i++)
	{
		matches.at(i).queryIdx;		// keypoints1 의 Index;
		matches.at(i).trainIdx;		// keypoints1 의 Index;
		KeyPoint key1 = keypoints1.at(matches.at(i).queryIdx);
		KeyPoint key2 = keypoints2.at(matches.at(i).trainIdx);

		int nX = (int)(key1.pt.x - key2.pt.x);
		int nY = (int)(key1.pt.y - key2.pt.y);
		if(abs(nX) < 30 )
		{
			if(abs(nY) < 30 )
			{
				tpMatches.push_back(matches.at(i));
				CPoint point;
				point.x = (LONG)(key1.pt.x);
				point.y = (LONG)(key1.pt.y);
				arrSiftRefPoint->push_back(point);

				point.x = (LONG)(key2.pt.x);
				point.y = (LONG)(key2.pt.y);
				arrSiftTarPoint->push_back(point);

			}
		}
	}

	if( m_bMatchaingView )
	{
		namedWindow("matches", 1);
		Mat img_matches;
		drawMatches(img1, keypoints1, img2, keypoints2, tpMatches, img_matches);
		imshow("matches", img_matches);
		waitKey(0);
	}

	return TRUE;
}

int ImageRevisionMgr::AdjustImage(CString strTarImage, CString strOutImage)			// RBG Table 만들기
{
	IplImage*	pTarPicture;
	char pchPath[MAX_PATH] = {0};

	(int)wcstombs(pchPath, strTarImage, MAX_PATH);
//	pTarPicture = cvLoadImage(pchPath);
	if(!pTarPicture)
	{
		//ESMLog(0,_T("Non Exsit Adjust Picture[%s]"),strRefImage);
		return FALSE;
	}

	for(int h=0; h<pTarPicture->height; ++h)
	{
		BYTE* pData = (BYTE*)(pTarPicture->imageData + h*pTarPicture->widthStep);
		for(int w=0; w<pTarPicture->width; ++w)
		{
			// color image (BGR 순서입니다.)
			pData[w * 3]	 =  (BYTE)(m_nRevisionBlue[pData[w * 3]]		);
			pData[w * 3 + 1] =  (BYTE)(m_nRevisionGreen[pData[w * 3 + 1]]	);
			pData[w * 3 + 2] =  (BYTE)(m_nRevisionRed[pData[w * 3 + 2]]		);
		}
	}

	//-- Save File
	wcstombs(pchPath, strOutImage, MAX_PATH);
	//cvSaveImage(pchPath, pTarPicture);


	cvReleaseImage(&pTarPicture);
	return 0;
}

int ImageRevisionMgr::AdjustImage(IplImage*	pMoveImage, CString strOutImage, int nMarginX, int nMarginY)			// RBG Table 만들기
{
	char pchPath[MAX_PATH] = {0};

	if(!pMoveImage)
	{
		//ESMLog(0,_T("Non Exsit Adjust Picture[%s]"),strRefImage);
		return FALSE;
	}

	for(int h=nMarginY; h<pMoveImage->height - nMarginY; ++h)
	{
		BYTE* pData = (BYTE*)(pMoveImage->imageData + h*pMoveImage->widthStep);
		for(int w=nMarginX; w<pMoveImage->width - nMarginX; ++w)
		{
			// color image (BGR 순서입니다.)
			pData[w * 3]		=  (BYTE)(m_nRevisionBlue[pData[w * 3]]		);
			pData[w * 3 + 1]	=  (BYTE)(m_nRevisionGreen[pData[w * 3 + 1]]);
			pData[w * 3 + 2]	=  (BYTE)(m_nRevisionRed[pData[w * 3 + 2]]	);
		}
	}

	//-- Save File
	wcstombs(pchPath, strOutImage, MAX_PATH);
	//cvSaveImage(pchPath, pMoveImage);
	return 0;
}

int ImageRevisionMgr::AdjustImage(IplImage*	pMoveImage, int nMarginX, int nMarginY)
{
	char pchPath[MAX_PATH] = {0};

	if(!pMoveImage)
	{
		//ESMLog(0,_T("Non Exsit Adjust Picture[%s]"),strRefImage);
		return FALSE;
	}

	for (int nHightIndex = nMarginY; nHightIndex < pMoveImage->height - nMarginY; nHightIndex++ )
	{
		for (int nWidthIndex = nMarginX; nWidthIndex < pMoveImage->width - nMarginX; nWidthIndex++ )
		{
			int nBlue =		(unsigned char)pMoveImage->imageData[nHightIndex*pMoveImage->widthStep + nWidthIndex*3];
			int nGreen =	(unsigned char)pMoveImage->imageData[nHightIndex * pMoveImage->widthStep + nWidthIndex * 3 + 1];	// Green
			int nRed =		(unsigned char)pMoveImage->imageData[nHightIndex * pMoveImage->widthStep + nWidthIndex * 3 + 2];

			pMoveImage->imageData[nHightIndex*pMoveImage->widthStep + nWidthIndex*3]			= (char)m_nRevisionBlue[nBlue];
			pMoveImage->imageData[nHightIndex * pMoveImage->widthStep + nWidthIndex * 3 + 1]	= (char)m_nRevisionGreen[nGreen];
			pMoveImage->imageData[nHightIndex * pMoveImage->widthStep + nWidthIndex * 3 + 2]	= (char)m_nRevisionRed[nRed];
		}
	}
	return 0;
}
int ImageRevisionMgr::lstsq(double *x,double *y,int m,int n,double *c)
{
	int i,j,k,l;
	double w1,w2,w3,pivot,aik, a[21][22], w[42];

	memset(a, 0, sizeof(double)*21*22);
	memset(w, 0, sizeof(double)*42);

	if(m>=n || m<1 || m>20) return 999;

	for(i=0; i<m*2; i++) 
	{
		w1=0.0;
		for (j=0; j<n; j++) 
		{
			w2=w3=x[j];
			for (k=0; k<i; k++) w2*=w3;
			w1+=w2;
		}
		w[i]=w1;
	}
	for(i=0; i<m+1; i++) 
	{
		for(j=0; j<m+1; j++) 
		{
			l=i+j-1;
			if(l<0)
				l=0;
			a[i][j]=w[l];
		}
	}

	a[0][0]=n;
	w1=0;

	for(i=0; i<n; i++)   w1+=y[i];

	a[0][m+1]=w1;
	for(i=0; i<m; i++) 
	{
		w1=0;
		for(j=0; j<n; j++) 
		{
			w2=w3=x[j];
			for(k=0; k<i; k++) w2*=w3;
			w1+=y[j]*w2;
		}
		a[i+1][m+1]=w1;
	}

	for(k=0; k<m+1; k++) 
	{
		pivot=a[k][k];
		for(j=k; j<m+2; j++) a[k][j]/=pivot;
		for(i=0; i<m+1; i++) 
		{
			if(i!=k) 
			{
				aik=a[i][k];
				for(j=k; j<m+2; j++) a[i][j]-=aik*a[k][j];
			}
		}
	}
	for(i=0; i<m+1; i++)  c[i]=a[i][m+1];

	return 0;
}

BOOL ImageRevisionMgr::LoadRGBData(CString strInputData)
{
	if( strInputData == _T(""))
		return FALSE;

	CFile ReadFile;
	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		return FALSE;
	}

	INT iLength = (INT)(ReadFile.GetLength());
	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);
	pBuffer[iLength-1] = '\0';
	TCHAR *pwszBuf = new TCHAR[iLength];
	memcpy(pwszBuf, pBuffer, iLength);
	pwszBuf[iLength-1] = '\0';

	CString sBuffer;
	sBuffer.Format(_T("%s"), pwszBuf);

	ReadFile.Close();


	int iPos = 0;
	CString sLine;
	sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;
	CString strTpValue;
	int iIndex = 0;
	while (iPos <= iLength)
	{
		AfxExtractSubString(strTpValue, sLine, 0, ',');
		m_nRevisionRed[iIndex] = _ttof(strTpValue);
		AfxExtractSubString(strTpValue, sLine, 1, ',');
		m_nRevisionGreen[iIndex] = _ttof(strTpValue);
		AfxExtractSubString(strTpValue, sLine, 2, ',');
		m_nRevisionBlue[iIndex] = _ttof(strTpValue);

		//반올림
		m_nRevisionRed[iIndex] = (int)(m_nRevisionRed[iIndex] + 0.5);
		m_nRevisionGreen[iIndex] = (int)(m_nRevisionGreen[iIndex] + 0.5);
		m_nRevisionBlue[iIndex] = (int)(m_nRevisionBlue[iIndex] + 0.5);

		iIndex++;
		if( iIndex >= 256)
			break;

		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);
		if( iPos == -1)
			break;
	}

	delete[] pwszBuf;
	pwszBuf = NULL;
	delete[] pBuffer;
	pBuffer = NULL;

	return TRUE;
}

BOOL ImageRevisionMgr::SaveRGBData(CString strOutputData)
{
	if( strOutputData == _T(""))
		return FALSE;

	CFile WriteFile;
	if (!WriteFile.Open(strOutputData, CFile::modeWrite | CFile::modeCreate))
	{
		return FALSE;
	}

	CString WriteData;
	for( int i = 0; i< 256; i++)
	{
		WriteData.Format(_T("%.3lf, %.3lf, %.3lf\r\n"), m_nRevisionRed[i], m_nRevisionGreen[i], m_nRevisionBlue[i]);
		WriteFile.Write(WriteData, WriteData.GetLength()*2);
	}
	WriteFile.Close();

	return TRUE;
}