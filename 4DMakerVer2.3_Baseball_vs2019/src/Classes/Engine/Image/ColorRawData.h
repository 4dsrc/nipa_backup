#pragma once

#include "stdafx.h"

#if !defined(USERINT_HEADER) && !defined(_CVI_RECT_DEFINED)
typedef struct Rect_struct 
{
    int top;    //Location of the top edge of the rectangle.
    int left;   //Location of the left edge of the rectangle.
    int height; //Height of the rectangle.
    int width;  //Width of the rectangle.
} Rect_struct;
#define _CVI_RECT_DEFINED
#endif


#define POLLING_TASKINFO_MAX_SIZE 1000
typedef struct _POLLING_TASKINFO
{
	int iTaskID;
	int iDataSize;
	int irgTaskInfo[POLLING_TASKINFO_MAX_SIZE];
	int stopFlag;
	//-- 2009-11-12 hongsu.jung
	//-- iTaskSize
	int iTaskTop;
	int iTaskSize;	
} POLLING_TASKINFO;

class CColorRawData : public CObject
{
public:
	CColorRawData()
	{
		InitColorRawData();		
	}

	BOOL IsEmpty()
	{
		return (( m_pdbRed == NULL || m_pdbGreen == NULL || m_pdbBlue == NULL || m_pdbY == NULL ) ? TRUE:FALSE);
	}

	BOOL CreateRawData(Rect_struct rc, int nSize)
	{
		DeleteRawData();
		memcpy(&m_rcDraw, &rc, sizeof(Rect_struct));

		m_nSize		= nSize;
		m_pdbRed	= new double[m_nSize];
		m_pdbGreen	= new double[m_nSize];
		m_pdbBlue	= new double[m_nSize];
		m_pdbY		= new double[m_nSize];	

		if( m_pdbRed == NULL || m_pdbGreen == NULL || m_pdbBlue == NULL || m_pdbY == NULL )
		{
			DeleteRawData();
			return FALSE;
		}

		return TRUE;
	}

	BOOL SetRawData(int nIdx, double r, double g, double b)
	{
		if( nIdx >= m_nSize )
			return FALSE;

		m_pdbRed	[nIdx]	= r;
		m_pdbGreen	[nIdx]	= g;
		m_pdbBlue	[nIdx]	= b;
		m_pdbY		[nIdx]	= ( r + g + b)/3 ;
		
		return TRUE;
	}

	CColorRawData( const CColorRawData& ref )
	{
		CopyData(ref);
	}

	CColorRawData& operator=(const CColorRawData& ref)
	{
		CopyData(ref);
		return *this;
	}

	BOOL CopyData(const CColorRawData& ref)
	{
		if( ref.m_nSize		<= 0		|| 
			ref.m_pdbRed		== NULL		||
			ref.m_pdbGreen		== NULL		||
			ref.m_pdbBlue		== NULL		||
			ref.m_pdbY			== NULL			) 
			return FALSE;

		DeleteRawData();
		if( !CreateRawData(ref.m_rcDraw, ref.m_nSize) )
			return FALSE;
				
		int nMemSize = m_nSize * sizeof(double);
		memcpy(m_pdbRed,		ref.m_pdbRed,		nMemSize);
		memcpy(m_pdbGreen,		ref.m_pdbGreen,	nMemSize);
		memcpy(m_pdbBlue,		ref.m_pdbBlue,		nMemSize);
		memcpy(m_pdbY,			ref.m_pdbY,		nMemSize);

		return TRUE;
	}

	~CColorRawData()
	{
		DeleteRawData();
	}

	void InitColorRawData()
	{
		memset(&m_rcDraw, 0x00, sizeof(Rect_struct));
		m_nSize			= 0;
		m_pdbRed		= NULL;
		m_pdbGreen		= NULL;
		m_pdbBlue		= NULL;
		m_pdbY			= NULL;		
	}

	void DeleteRawData()
	{
		if( m_pdbRed )
			delete [] m_pdbRed;

		if( m_pdbGreen )
			delete [] m_pdbGreen;

		if( m_pdbBlue )
			delete [] m_pdbBlue;

		if( m_pdbY )
			delete [] m_pdbY;

		InitColorRawData();
	}

	Rect_struct		m_rcDraw;
	
	int			m_nSize;
	double*		m_pdbRed;
	double*		m_pdbGreen;
	double*		m_pdbBlue;
	double*		m_pdbY;		// Luminance =  0.3 * r + ( 0.59 * g) + (0.11 * b)
};

class CTaskInfo: public CObject
{
public:
	CTaskInfo()		{ Init();}
	~CTaskInfo()	{ DeleteRawData(); }

	void Init()
	{
		m_nSize			= 0;
		m_pPercentage		= NULL;		
	}

	BOOL CopyData(const POLLING_TASKINFO& tInfo)
	{
		if( tInfo.iDataSize <= 0) 
			return FALSE;
		DeleteRawData();

		double nPercentage;
		float nTopAddr;
		float nTaskSize;
		//-- SET SIZE
		m_nSize = tInfo.iDataSize;
		nTaskSize = (float)tInfo.iTaskSize;
		nTopAddr =  (float)tInfo.iTaskTop;

		m_pPercentage = new double[m_nSize];		
		for(int i= 0; i<m_nSize; i++)
		{
			nPercentage = (double)((nTopAddr + nTaskSize - tInfo.irgTaskInfo[i]) / nTaskSize * 100);
			if(nPercentage > 100)
				nPercentage = 100;

			m_pPercentage[i] = nPercentage;
		}
		return TRUE;
	}

	void DeleteRawData()
	{
		if( m_pPercentage )
			delete [] m_pPercentage;
		Init();
	}

public:
	int	 m_nSize;
	double* m_pPercentage;
	CString m_strTaskName;
	int m_nTestTime;
};