////////////////////////////////////////////////////////////////////////////////
//
//	TGImageColorDetect.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-11
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TGUtil.h"

#include "TGImageColorDetect.h"
#include "TGImageFunc.h"


CTGImageColorDetect::CTGImageColorDetect()	
{
	m_pResizeImg	= NULL;
	m_nScaleWidth	= 0;
	m_nScaleHeight	= 0;

	m_nAllRGBCnt	= 0;
	m_nDetectRGBCnt	= 0;
	m_bMode			= TRUE;
	for(int i = 0 ; i < COLOR_ALL ; i ++)
		m_nRGBColor[i] = 0;

	SetColorTolerance(TG_BASE_COLOR_TOLERANCE);
	//-- 2012-07-18 hongsu@esmlab.com
	//-- CIC Check
	m_nType = COLOR_R;
	for(int i = 0 ; i < POINT_ALL ; i ++)
		m_pt[i] = cvPoint(0,0);
}
CTGImageColorDetect::~CTGImageColorDetect() 
{
	if(m_pResizeImg)
		cvReleaseImage(&m_pResizeImg);
}

void CTGImageColorDetect::SetInit(IplImage* pImg, int nColor)
{
	m_nType = nColor;	
	//-- Tolerance set
	switch(nColor)
	{
	case COLOR_R:
		m_nTolerance[COLOR_R][RANGE_MIN]		= TG_IMAGE_DETECT_RANGE_ON_MIN;
		m_nTolerance[COLOR_R][RANGE_MAX]		= TG_IMAGE_DETECT_RANGE_ON_MAX;
		m_nTolerance[COLOR_G][RANGE_MIN]		= TG_IMAGE_DETECT_RANGE_NON_MIN;
		m_nTolerance[COLOR_G][RANGE_MAX]		= TG_IMAGE_DETECT_RANGE_NON_MAX;
		m_nTolerance[COLOR_B][RANGE_MIN]		= TG_IMAGE_DETECT_RANGE_NON_MIN;
		m_nTolerance[COLOR_B][RANGE_MAX]		= TG_IMAGE_DETECT_RANGE_NON_MAX;
		break;
	case COLOR_G:
		m_nTolerance[COLOR_R][RANGE_MIN]		= TG_IMAGE_DETECT_RANGE_NON_MIN;
		m_nTolerance[COLOR_R][RANGE_MAX]		= TG_IMAGE_DETECT_RANGE_NON_MAX;
		m_nTolerance[COLOR_G][RANGE_MIN]		= TG_IMAGE_DETECT_RANGE_ON_MIN;
		m_nTolerance[COLOR_G][RANGE_MAX]		= TG_IMAGE_DETECT_RANGE_ON_MAX;
		m_nTolerance[COLOR_B][RANGE_MIN]		= TG_IMAGE_DETECT_RANGE_NON_MIN;
		m_nTolerance[COLOR_B][RANGE_MAX]		= TG_IMAGE_DETECT_RANGE_NON_MAX;
		break;
	case COLOR_B:
		m_nTolerance[COLOR_R][RANGE_MIN]		= TG_IMAGE_DETECT_RANGE_NON_MIN;
		m_nTolerance[COLOR_R][RANGE_MAX]		= TG_IMAGE_DETECT_RANGE_NON_MAX;
		m_nTolerance[COLOR_G][RANGE_MIN]		= TG_IMAGE_DETECT_RANGE_NON_MIN;
		m_nTolerance[COLOR_G][RANGE_MAX]		= TG_IMAGE_DETECT_RANGE_NON_MAX;
		m_nTolerance[COLOR_B][RANGE_MIN]		= TG_IMAGE_DETECT_RANGE_ON_MIN;
		m_nTolerance[COLOR_B][RANGE_MAX]		= TG_IMAGE_DETECT_RANGE_ON_MAX;
		break;
	}

	//-- 2012-07-27 hongsu@esmlab.com
	//-- Change Size (300X200)
	m_nScaleWidth = (float)TG_COLOR_DETECT_WIDTH/(float)pImg->width;
	m_nScaleHeight = (float)TG_COLOR_DETECT_HEIGHT/(float)pImg->height;

	if(m_pResizeImg)
		cvReleaseImage(&m_pResizeImg);
	m_pResizeImg = TGResizeImage(pImg, TG_COLOR_DETECT_WIDTH, TG_COLOR_DETECT_HEIGHT);


	//-- 2012-07-11 hongsu@esmlab.com
	//-- Rect Reset
	m_pt[POINT_LEFT_TOP].x = m_pResizeImg->width;
	m_pt[POINT_LEFT_TOP].y = m_pResizeImg->height;
//	m_pt[POINT_RIGHT_TOP].x = 0;
//	m_pt[POINT_RIGHT_TOP].y = pImg->height;
	m_pt[POINT_RIGHT_BOTTOM].x = 0;
	m_pt[POINT_RIGHT_BOTTOM].y = 0;
//	m_pt[POINT_LEFT_BOTTOM].x = pImg->width;
//	m_pt[POINT_LEFT_BOTTOM].y = 0;
	
}

void CTGImageColorDetect::SetAverageColor()
{
	for(int i = 0 ; i < COLOR_ALL ; i ++)
	{
		if(m_nRGBColor[i])
			m_nRGBColor[i] /= m_nDetectRGBCnt;
	}
}

void CTGImageColorDetect::SetPosition(int nX, int nY)
{	
	/*
	if ( (nX <= m_pt[POINT_LEFT_TOP	].x	) && ( nY <= m_pt[POINT_LEFT_TOP	].y	))	{ m_pt[POINT_LEFT_TOP		].x	= nX;	m_pt[POINT_LEFT_TOP		].y = nY; }
	if ( (nX >= m_pt[POINT_RIGHT_TOP	].x	) && ( nY <= m_pt[POINT_RIGHT_TOP	].y	))	{ m_pt[POINT_RIGHT_TOP		].x	= nX;	m_pt[POINT_RIGHT_TOP	].y = nY; }
	if ( (nX >= m_pt[POINT_RIGHT_BOTTOM	].x	) && ( nY >= m_pt[POINT_RIGHT_BOTTOM].y	))	{ m_pt[POINT_RIGHT_BOTTOM	].x	= nX;	m_pt[POINT_RIGHT_BOTTOM	].y = nY; }
	if ( (nX <= m_pt[POINT_LEFT_BOTTOM	].x	) && ( nY >= m_pt[POINT_LEFT_BOTTOM	].y	))	{ m_pt[POINT_LEFT_BOTTOM	].x	= nX;	m_pt[POINT_LEFT_BOTTOM	].y = nY; }
	*/
	if ( nX <= m_pt[POINT_LEFT_TOP		].x	)  m_pt[POINT_LEFT_TOP		].x	= nX; 
	if ( nX >= m_pt[POINT_RIGHT_BOTTOM	].x	)  m_pt[POINT_RIGHT_BOTTOM	].x	= nX; 	
	if ( nY <= m_pt[POINT_LEFT_TOP		].y	)  m_pt[POINT_LEFT_TOP		].y	= nY; 
	if ( nY >= m_pt[POINT_RIGHT_BOTTOM	].y	)  m_pt[POINT_RIGHT_BOTTOM	].y	= nY;
}

CvPoint CTGImageColorDetect::GetCenterPosition()
{
	CvPoint ptCenter;
	CvPoint pt[POINT_ALL];	
	pt[POINT_LEFT_TOP]		= TGScalePoint(m_pt[POINT_LEFT_TOP]		, m_nScaleWidth, m_nScaleHeight);
	pt[POINT_RIGHT_BOTTOM]	= TGScalePoint(m_pt[POINT_RIGHT_BOTTOM]	, m_nScaleWidth, m_nScaleHeight);

	ptCenter.x = ( pt[POINT_LEFT_TOP].x	+ pt[POINT_RIGHT_BOTTOM	].x	 ) / 2;
	ptCenter.y = ( pt[POINT_LEFT_TOP].y	+ pt[POINT_RIGHT_BOTTOM	].y	 ) / 2;
	return ptCenter;
}

CvScalar CTGImageColorDetect::GetColor(int nType)
{
	CvScalar color;
	switch(nType)
	{
		case COLOR_R: color = cvScalar(  0,  0,255);	break;
		case COLOR_G: color = cvScalar(  0,255,  0);	break;
		case COLOR_B: color = cvScalar(255,  0,  0);	break;
		default:	
			color = cvScalar(0,0,0);
			break;
	}	
	return color;
}

void CTGImageColorDetect::DrawLine(IplImage* pImg)
{
	if(!pImg)
		return;

	CvScalar color = GetColor(m_nType);	

	//-- 2012-07-12 hongsu@esmlab.com	
	CvPoint pt[POINT_ALL];
	pt[POINT_LEFT_TOP]		= TGScalePoint(m_pt[POINT_LEFT_TOP]		, m_nScaleWidth, m_nScaleHeight);
	pt[POINT_RIGHT_BOTTOM]	= TGScalePoint(m_pt[POINT_RIGHT_BOTTOM]	, m_nScaleWidth, m_nScaleHeight);

	cvDrawRect(pImg,
		pt[POINT_LEFT_TOP		],
		pt[POINT_RIGHT_BOTTOM	],
		color,
		3);
}

void CTGImageColorDetect::DrawInfo(IplImage* pImg)
{
	CvScalar color = cvScalar(255,255,255);
	//-- 2012-07-12 hongsu@esmlab.com
	//-- Draw Information
	CvFont font;
	double hScale, vScale;
	int    lineWidth;

	//-- 2012-07-13 hongsu@esmlab.com
	//-- Set Size From Image Size	
	hScale = (double)pImg->width/800;
	vScale = (double)pImg->height/800;
	lineWidth = 1 + pImg->height/500;

	cvInitFont(&font,CV_FONT_HERSHEY_SIMPLEX, hScale,vScale,0,lineWidth);

	//-- 2012-07-12 hongsu@esmlab.com
	//-- Set String 
	CString strInfo;
	char* chInfo = new char[STR_MAX_LEN];
	CvPoint ptCenter = GetCenterPosition();	
	size_t CharactersConverted = 0;

	//-- 2012-07-12 hongsu@esmlab.com
	//-- Set String (COLOR) 
	switch(m_nType)
	{
	case COLOR_R :		strInfo.Format(_T("RED"));		break;
	case COLOR_G :		strInfo.Format(_T("GREEN"));	break;
	case COLOR_B :		strInfo.Format(_T("BLUE"));		break;
	}
	
	wcstombs_s(&CharactersConverted, chInfo, strInfo.GetLength()+1, strInfo.GetBuffer(), _TRUNCATE);
	strInfo.ReleaseBuffer();

	//-- Position	
	ptCenter.x -= pImg->width/30;
	cvPutText (pImg,chInfo,ptCenter,&font,color);


	//-- Set String 
	//-- PERCENTAGE
	
	float nPercentage = GetPercentage();
	strInfo.Format(_T("%3.02f%%"),nPercentage);	

	wcstombs_s(&CharactersConverted, chInfo, strInfo.GetLength()+1, strInfo.GetBuffer(), _TRUNCATE);
	strInfo.ReleaseBuffer();

	//-- Position
	ptCenter.y += pImg->height/20;
	cvPutText (pImg,chInfo,ptCenter,&font,color);

	//-- delete char
	delete []chInfo;
	
}

float CTGImageColorDetect::GetPercentage()
{
	return  (float)m_nDetectRGBCnt/(float)m_nAllRGBCnt*100;
}

BOOL CTGImageColorDetect::CheckRange()
{
	float nPercentage = (float)m_nDetectRGBCnt/(float)m_nAllRGBCnt*100;
	int nTolerance = 0;
	CString strColor = _T("");

	//-- 2012-08-31 joonho.kim
	switch( m_nType )
	{
	case 0: strColor.Format(_T("R")); break;
	case 1: strColor.Format(_T("G")); break;
	case 2: strColor.Format(_T("B")); break;
	}
	TGLog(5,_T("Get Range Area [%s:%3.02f%%]"),strColor,nPercentage);

	//-- 2012-08-20 hongsu@esmlab.com
	//-- Check Percentage
	switch(m_nType)
	{
	case COLOR_R :	nTolerance =TGGetValue(TG_OPT_VERIFICATION_PER_R);	break;
	case COLOR_G :	nTolerance =TGGetValue(TG_OPT_VERIFICATION_PER_G);	break;
	case COLOR_B :	nTolerance =TGGetValue(TG_OPT_VERIFICATION_PER_B);	break;
	}
	
	if(nPercentage < nTolerance )
		return FALSE;
	return TRUE;
}
