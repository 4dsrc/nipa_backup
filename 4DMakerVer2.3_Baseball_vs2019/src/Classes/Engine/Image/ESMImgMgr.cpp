/////////////////////////////////////////////////////////////////////////////
//
// ESMImgMgr.cpp
//
//
// Copyright (c) 2012 ESMLab, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of ESMLab, Inc. ("Confidential Information").  
//
// @author	Jung Hongsu (hongsu@esmlab.com)
// @Date	2009-05-20
// @Note	2012-12-07 
//			Memory Leak
//			cvCreateImage	-> cvReleaseImage
//			cvLoadImage		-> cvReleaseImage
//			cvCloneImage	-> cvReleaseImage
//			cvQueryImage	-> cvReleaseImage
//
//			cvCreateCameraCapture	-> cvReleaseCapture
//			cvCaptureFramCAM		-> cvReleaseCapture
//
//			cvCreateMemStorage	-> cvReleaseMemStorage
//
// @Note	2013-04-25 
//			Change To Manager from CDialog
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMImgMgr.h"
#include "MainFrm.h"
#include <cmath>
#include "ESMUtil.h"
#include "ESMLabeling.h"
#include "ESMFileOperation.h"
#include "ImageRevisionMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//-- 2013-08-20 jaehyun@esmlab.com
//-- use atan2()
#include <math.h>


#define IMG_ADJUST_MIN_HEIGHT	FULL_HD_HEIGHT / 5
#define IMG_ADJUST_MIN_WIDTH	FULL_HD_WIDTH  / 5


#define IMG_COLOR_BLACK 0
#define IMG_COLOR_WHITE 255

#define IMG_ADJUST_DETECT_COLOR		IMG_COLOR_WHITE
#define IMG_ADJUST_UNDETECT_COLOR	abs(IMG_ADJUST_DETECT_COLOR - IMG_COLOR_WHITE)


//-- 2013-08-20 jaehyun@esmlab.com
#define TRINANGER_MIN_HEIGHT		50
#define TRINANGER_MIN_WHITE_VALUE	1

#define PI							3.141592
#define PARARALLEL					180

//Define range for (x1,y1), (x2,y2), (x3,y3) setting
#define RANGE_SETTING_LINE_FIRST	10		//Total pixel : RANGE_SETTING_LINE_FIRST * 3
#define RANGE_SETTING_LINE_SECOND	20		//Total pixel : RANGE_SETTING_LINE_SECOND * 2
#define RANGE_SETTING_PERSENT		0.7

//로테이트 영상의 지우는 부분 마진 비율
#define ROTATE_ERASE_MARGIN_SCALE_X		200
#define ROTATE_ERASE_MARGIN_SCALE_Y		100


CESMImgMgr::CESMImgMgr()
{
	m_hMainWnd = ESMGetMainWnd();

	m_dbHorzScaling	= 1;
	m_dbVertScaling	= 1;

	m_nImageMarginX = 0;
	m_nImageMarginY = 0;
	//-- 2013-10-02 jaehyun@esmlab.com
	//-- Histogram matching member pointer variable Initialize
	m_dbRefCDF = new double[768];
	InitializeCriticalSection (&_ESMImageEngine);	
	InitializeCriticalSection (&m_CS_ESMImageAjust);
}

CESMImgMgr::~CESMImgMgr()
{
	Sleep(1000);
	DeleteCriticalSection (&_ESMImageEngine);	
	DeleteCriticalSection (&m_CS_ESMImageAjust);	
	//-- 2013-10-02 jaehyun@esmlab.com
	//-- Histogram matching member pointer variable delete
	delete[] m_dbRefCDF;
	m_dbRefCDF = NULL;
}

//------------------------------------------------------------------------------ 
//! @brief		GetScaleRate(double* pdbHorzScaling, double* pdbVertScaling)
//! @date		
//! @attention	none
//! @comment	Pattern Matching
//------------------------------------------------------------------------------ 
void CESMImgMgr::GetScaleRate(double* pdbHorzScaling, double* pdbVertScaling)
{
	if( pdbHorzScaling )
		*pdbHorzScaling = m_dbHorzScaling;

	if( pdbVertScaling )
		*pdbVertScaling = m_dbVertScaling;
}

//------------------------------------------------------------------------------ 
//! @brief		InitScaleRate()
//! @date		
//! @attention	none
//! @comment
//------------------------------------------------------------------------------ 
void CESMImgMgr::InitScaleRate()
{
	m_dbHorzScaling		= 1;
	m_dbVertScaling		= 1;
}

//------------------------------------------------------------------------------ 
//! @brief		SetScaleRate(double dbHorzScaling, double dbVertScaling)
//! @date		
//! @attention	none
//! @comment		Pattern Matching
//------------------------------------------------------------------------------ 
void CESMImgMgr::SetScaleRate(double dbHorzScaling, double dbVertScaling)
{
	m_dbHorzScaling = dbHorzScaling;
	m_dbVertScaling = dbVertScaling;
	ESMLog(6, _T("[CESMImgMgr::SetScaleRate] HScaling=%f, VScaling=%f"), m_dbHorzScaling, m_dbVertScaling);
}

//------------------------------------------------------------------------------ 
//! @brief		Adjust Get Detect Position & Angle
//! @date		2013-04-25
//! @attention	Gray -> Binary -> Get 255 color (x,y)
//! @note	 	hongsu.jung
//------------------------------------------------------------------------------ 
/*
BOOL CESMImgMgr::GetAdjustDetect(CString strFile, CvPoint2D32f* pPoint, double* pdbAngle, double* dPictureSize, CvPoint2D32f* pRotatePoint, int* nIntensity, double dTargetDistance, int nIndex)
{
	IplImage*	pImage;
	IplImage*	pGray;
	IplImage*	pNewGray;
	IplImage*	pTpPicture;
	//
	double dTargetLength = 0;
	int nTarHeight = 1080, nRealHeight = 0, nRealWidth = 0;
	int nTarWidth = 1920;

	CString strSaveFile;
	char pchPath[MAX_PATH] = {0};
	int nRequiredSize = (int)wcstombs(pchPath, strFile, MAX_PATH); 
	double nTotalBlue = 0, nTotalRed =0 , nTotalGreen =0;

	CString strPath, strFileName, strAdjustFile, strFileNamePath;
	ESMUtil::GetFileNameAndPath(strFile, strFileName, strPath );
	strFileNamePath = strFileName;
	strPath = strPath.Left(strPath.GetLength() - 1);
	ESMUtil::GetFileNameAndPath(strPath, strFileName, strPath );

	//-- Load Image
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
	{
		IplImage*	pTpPictureSize = NULL;
		pTpPicture = cvLoadImage(pchPath);//, cv::IMREAD_UNCHANGED);
		if(!pTpPicture)
		{
			ESMLog(0,_T("Non Exsit Adjust Picture[%s]"),strFile);
			return FALSE;
		}
		nRealWidth = pTpPicture->width;
		nRealHeight = (int)((double)pTpPicture->height / double((double)pTpPicture->width / (double)nTarWidth));
		int nGapHeight = nRealHeight - nTarHeight;
		nGapHeight  = nGapHeight /2;

		// 1:1 비율로 변환
		pTpPictureSize = cvCreateImage(cvSize(nTarWidth, nRealHeight), pTpPicture->depth, pTpPicture->nChannels); 
		cvResize(pTpPicture, pTpPictureSize, cv::INTER_LANCZOS4);
		cvReleaseImage(&pTpPicture);

		pImage = cvCreateImage(cvSize(nTarWidth, nTarHeight), pTpPictureSize->depth, pTpPictureSize->nChannels); 
		if( nGapHeight >= 0)	// Gab 만큼 Image를 제거
		{
			cvSetImageROI(pTpPictureSize, cvRect(0, nGapHeight, pImage->width, pImage->height));
			cvCopy(pTpPictureSize, pImage);
			cvResetImageROI(pTpPictureSize);
		}
		else					// Gab 만큼 Image에 삽입
		{
			cvSetImageROI(pImage, cvRect(0, nGapHeight, pTpPictureSize->width, pTpPictureSize->height));
			cvCopy(pTpPictureSize, pImage);
			cvResetImageROI(pImage);
		}
		cvReleaseImage(&pTpPictureSize);
		strFileNamePath = strFileNamePath.Left(strFileNamePath.GetLength() - 4);
		strAdjustFile.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_ADJUST), strFileNamePath);
	}
	else
	{
		//-- Load Image
		FFmpegManager ffmpegMgr(FALSE);
		CString strMovieFile;
		strMovieFile.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_RECORD_FILE), ESMGetFrameRecord(), strFileName);
		pImage = cvCreateImage(cvSize(nTarWidth, nTarHeight), IPL_DEPTH_8U, 3); 

		ffmpegMgr.GetCaptureImage(strMovieFile, pImage, 0);
		if(!pImage)
			return FALSE;
		nRealWidth = pImage->width;
		strAdjustFile.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_ADJUST), strFileName);
	}

	int nPixelDefaultSize = 0;
	if( nRealWidth == 1920)
		nPixelDefaultSize = DEFAULT_PIXEL1920_DISTANCE;
	else if( nRealWidth == 5472 && nRealWidth == 3080)
		nPixelDefaultSize = DEFAULT_PIXEL5472_3080_DISTANCE;
	else if( nRealWidth == 5472 && nRealWidth != 3080)
		nPixelDefaultSize = DEFAULT_PIXEL5472_DISTANCE;
	else
		nPixelDefaultSize = DEFAULT_PIXEL1920_DISTANCE;

	dTargetLength = ESMGetValue(ESM_VALUE_ADJ_TARGETLENGTH	) * nPixelDefaultSize  / DEFAULT_TARGETLENGTH;
	if( dTargetDistance != 0.0)
		dTargetLength = dTargetLength / dTargetDistance;
	else
		dTargetLength =1.0;

	dTargetLength = dTargetLength * ESMGetValue(ESM_VALUE_ADJ_CAMERAZOOM) /DEFAULT_CAMERAZOOM;
	//-- Gray Image
	pGray = cvCreateImage(cvGetSize(pImage), IPL_DEPTH_8U, 1);
	if(!pGray)
	{
		cvReleaseImage(&pImage);
		cvReleaseImage(&pGray);
		return FALSE;
	}
	//-- Gray Image
	pNewGray = cvCreateImage(cvGetSize(pImage), IPL_DEPTH_8U, 1);
	if(!pNewGray)
	{
		cvReleaseImage(&pImage);
		cvReleaseImage(&pGray);
		cvReleaseImage(&pNewGray);
		return FALSE;

	}
	//-- GRAY
	cvCvtColor(pImage, pGray, cv::COLOR_RGB2GRAY);
	//-- 2013-08-20 jaehyun@esmlab.com
	//-- Threshold change
	int nThreshold = ESMGetValue(ESM_VALUE_ADJ_TH);
	cvThreshold(pGray, pNewGray, nThreshold, 255 , CV_THRESH_BINARY);


	vector<CPoint> arrBeginPoint;
	vector<CPoint> arrCenterPoint;
	vector<CPoint> arrPointRange;
	CPoint TpBeginP, PointRange;
	TpBeginP.x = ESMGetValue(ESM_VALUE_ADJ_POINTX);
	TpBeginP.y = ESMGetValue(ESM_VALUE_ADJ_FIRSTPOINTY);
	arrBeginPoint.push_back(TpBeginP);

	TpBeginP.y = ESMGetValue(ESM_VALUE_ADJ_FIRSTPOINTY) + ESMGetValue(ESM_VALUE_ADJ_FIRSTYRANGE);
	arrBeginPoint.push_back(TpBeginP);

	TpBeginP.y = ESMGetValue(ESM_VALUE_ADJ_FIRSTPOINTY) + ESMGetValue(ESM_VALUE_ADJ_FIRSTYRANGE) + ESMGetValue(ESM_VALUE_ADJ_SECONDYRANGE);
	arrBeginPoint.push_back(TpBeginP);

	PointRange.x = ESMGetValue(ESM_VALUE_ADJ_XRANGE);
	PointRange.y = ESMGetValue(ESM_VALUE_ADJ_FIRSTYRANGE);
	arrPointRange.push_back(PointRange);
	PointRange.y = ESMGetValue(ESM_VALUE_ADJ_SECONDYRANGE);
	arrPointRange.push_back(PointRange);
	PointRange.y = ESMGetValue(ESM_VALUE_ADJ_THIRDYRANGE);
	arrPointRange.push_back(PointRange);
	SearchPoint(pNewGray, &arrBeginPoint, &arrPointRange, &arrCenterPoint);

	if( arrCenterPoint.size() > 2)
		cvLine(pNewGray, cvPoint(arrCenterPoint.at(0).x, arrCenterPoint.at(0).y), cvPoint(arrCenterPoint.at(2).x, arrCenterPoint.at(2).y), cvScalar(0, 0, 0, 0), 1, 8, 0);

	for(int i =0 ;i < arrBeginPoint.size(); i++)
		cvRectangle(pNewGray, cvPoint(arrBeginPoint.at(i).x, arrBeginPoint.at(i).y), cvPoint(arrBeginPoint.at(i).x + arrPointRange.at(i).x, arrBeginPoint.at(i).y + arrPointRange.at(i).y), cvScalar(0, 0, 0, 0), 1, 8, 0);

	strSaveFile.Format(_T("%s_Prev.jpg"),strAdjustFile);
	wcstombs(pchPath, strSaveFile, MAX_PATH);	
	cvSaveImage(pchPath, pNewGray);

	if( arrCenterPoint.size() < 3)
	{
		cvReleaseImage(&pImage);
		cvReleaseImage(&pGray);
		cvReleaseImage(&pNewGray);
		ESMLog(0,_T("Not Search 3 Point!"));
		return FALSE;
	}

	if ( !ESMGetValue(ESM_VALUE_ADJ_RESIZE_USE) )
	{
		pPoint->x = (float)arrCenterPoint.at(1).x;
		pPoint->y = (float)arrCenterPoint.at(1).y;
		pRotatePoint->x = pPoint->x;
		pRotatePoint->y = pPoint->y;
		cvLine(pNewGray, cvPoint(arrCenterPoint.at(0).x, arrCenterPoint.at(0).y), cvPoint(arrCenterPoint.at(2).x, arrCenterPoint.at(2).y), cvScalar(0, 0, 0, 0), 1, 8, 0);
	}

	if( arrCenterPoint.size() < 3)
		return FALSE;

	int nHeight = arrCenterPoint.at(2).y - arrCenterPoint.at(0).y;
	int nWidth = abs(arrCenterPoint.at(2).x - arrCenterPoint.at(0).x);

	double dTpResize = 0.0;
	if( nWidth != 0)
		dTpResize = sqrt(double(nHeight * nHeight + nWidth * nWidth));
	else
		dTpResize = nHeight;

	if ( ESMGetValue(ESM_VALUE_ADJ_RESIZE_USE) )
		*dPictureSize = dTargetLength / dTpResize ;
	else
		*dPictureSize = 1;

	IplImage* pImage2 = cvCreateImage(cvSize((int)(pImage->width * (*dPictureSize)), (int)(pImage->height * (*dPictureSize))), pImage->depth, pImage->nChannels); 

	//크기 줄이기
	if ( ESMGetValue(ESM_VALUE_ADJ_RESIZE_USE) )
		cvResize(pImage, pImage2, cv::INTER_LANCZOS4);  // For Resize

	cvReleaseImage(&pGray);
	cvReleaseImage(&pNewGray);
	pNewGray = NULL;
	pGray = NULL;
	//-- Gray Image
	pGray = cvCreateImage(cvGetSize(pImage2), IPL_DEPTH_8U, 1);
	if(!pGray)
	{
		cvReleaseImage(&pGray);
		return FALSE;
	}
	//-- Gray Image
	pNewGray = cvCreateImage(cvGetSize(pImage2), IPL_DEPTH_8U, 1);
	if(!pNewGray)
	{
		cvReleaseImage(&pGray);
		cvReleaseImage(&pNewGray);
		return FALSE;

	}
	//-- GRAY
	cvCvtColor(pImage2, pGray, cv::COLOR_RGB2GRAY);
	//-- 2013-08-20 jaehyun@esmlab.com
	//-- Threshold change
	cvThreshold(pGray, pNewGray, nThreshold, 255 , CV_THRESH_BINARY);

	if ( ESMGetValue(ESM_VALUE_ADJ_RESIZE_USE) )
	{
		arrCenterPoint.clear();
		BOOL bRet = FALSE;

		bRet = SearchPoint(pNewGray, &arrBeginPoint, &arrPointRange, &arrCenterPoint);

		for(int i =0 ;i < arrBeginPoint.size(); i++)
			cvRectangle(pNewGray, cvPoint(arrBeginPoint.at(i).x, arrBeginPoint.at(i).y), cvPoint(arrBeginPoint.at(i).x + arrPointRange.at(i).x, arrBeginPoint.at(i).y + arrPointRange.at(i).y), cvScalar(0, 0, 0, 0), 1, 8, 0);

		if( bRet == FALSE)
		{
			strSaveFile.Format(_T("%s_Non 3Point.jpg"),strAdjustFile);
			wcstombs(pchPath, strSaveFile, MAX_PATH);	
			cvSaveImage(pchPath, pNewGray);
			return FALSE;
		}
		if( arrCenterPoint.size() > 2)
			cvLine(pNewGray, cvPoint(arrCenterPoint.at(0).x, arrCenterPoint.at(0).y), cvPoint(arrCenterPoint.at(2).x, arrCenterPoint.at(2).y), cvScalar(0, 0, 0, 0), 1, 8, 0);

		strSaveFile.Format(_T("%s_Last.jpg"),strAdjustFile);
		wcstombs(pchPath, strSaveFile, MAX_PATH);	
		cvSaveImage(pchPath, pNewGray);

		if( arrCenterPoint.size() < 3)
		{
			cvReleaseImage(&pImage);
			cvReleaseImage(&pGray);
			cvReleaseImage(&pNewGray);
			ESMLog(0,_T("Not Search 3 Point!"));
			return FALSE;
		}

		if( bRet == FALSE)
			return FALSE;
	}

	cvReleaseImage(&pImage2);

	cvLine(pNewGray, cvPoint(arrCenterPoint.at(0).x, arrCenterPoint.at(0).y), cvPoint(arrCenterPoint.at(2).x, arrCenterPoint.at(2).y), cvScalar(0, 0, 0, 0), 1, 8, 0);

	for(int i =0 ;i < arrBeginPoint.size(); i++)
		cvRectangle(pNewGray, cvPoint(arrBeginPoint.at(i).x, arrBeginPoint.at(i).y), cvPoint(arrBeginPoint.at(i).x + PointRange.x, arrBeginPoint.at(i).y + PointRange.y), cvScalar(0, 0, 0, 0), 1, 8, 0);

	int dMargin = 5;
	*pdbAngle = atan2((double)(arrCenterPoint.at(2).y - arrCenterPoint.at(0).y), 
		(double)(arrCenterPoint.at(2).x - arrCenterPoint.at(0).x)) * (180/3.141592);

	if ( ESMGetValue(ESM_VALUE_ADJ_RESIZE_USE) )
	{
		pPoint->x = (float)arrCenterPoint.at(1).x;
		pPoint->y = (float)arrCenterPoint.at(1).y;
		pRotatePoint->x = pPoint->x;
		pRotatePoint->y = pPoint->y;
	}

	cvReleaseImage(&pImage);
	cvReleaseImage(&pGray);
	cvReleaseImage(&pNewGray);

	return TRUE;
}

BOOL CESMImgMgr::GetAdjustDetect2(CString strFile, CvPoint2D32f* pPoint, double* pdbAngle, double* dPictureSize, CvPoint2D32f* pRotatePoint, int* nIntensity, double dTargetDistance, int nIndex)
{
	IplImage*	pImage;
	IplImage*	pGray;
	IplImage*	pTpPicture;
	//
	double dTargetLength = 0;
	int nTarHeight = 1238, nTarWidth = 2200, nRealHeight = 0, nRealWidth = 0;
	int nOriginWidth = 0, nOriginHeight= 0;;

	CString strSaveFile;
	char pchPath[MAX_PATH] = {0};
	int nRequiredSize = (int)wcstombs(pchPath, strFile, MAX_PATH); 
	double nTotalBlue = 0, nTotalRed =0 , nTotalGreen =0;

	CString strPath, strFileName, strAdjustFile, strFileNamePath;
	ESMUtil::GetFileNameAndPath(strFile, strFileName, strPath );
	strFileNamePath = strFileName;
	strPath = strPath.Left(strPath.GetLength() - 1);
	ESMUtil::GetFileNameAndPath(strPath, strFileName, strPath );

	strFileNamePath = strFileNamePath.Left(strFileNamePath.GetLength() - 4);
	strAdjustFile.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_ADJUST), strFileNamePath);

	IplImage*	pTpPictureSize = NULL;
	pTpPicture = cvLoadImage(pchPath);//, cv::IMREAD_UNCHANGED)	
	if(!pTpPicture)
	{
		ESMLog(0,_T("Non Exsit Adjust Picture[%s]"),strFile);
		return FALSE;
	}
	nOriginWidth = pTpPicture->width;
	nOriginHeight = pTpPicture->height;

	nRealWidth = pTpPicture->width;
	nRealHeight = (int)((double)pTpPicture->height / double((double)pTpPicture->width / (double)nTarWidth));
	int nGapHeight = nRealHeight - nTarHeight;
	if( nGapHeight > 0)
	{
		nGapHeight  = nGapHeight /2;

		// 1:1 비율로 변환
		pTpPictureSize = cvCreateImage(cvSize(nTarWidth, nRealHeight), pTpPicture->depth, pTpPicture->nChannels); 
		cvResize(pTpPicture, pTpPictureSize, cv::INTER_LANCZOS4);
		cvReleaseImage(&pTpPicture);

		pImage = cvCreateImage(cvSize(nTarWidth, nTarHeight), pTpPictureSize->depth, pTpPictureSize->nChannels); 
		if( nGapHeight >= 0)	// Gab 만큼 Image를 제거
		{
			cvSetImageROI(pTpPictureSize, cvRect(0, nGapHeight, pImage->width, pImage->height));
			cvCopy(pTpPictureSize, pImage);
			cvResetImageROI(pTpPictureSize);
		}
		else					// Gab 만큼 Image에 삽입
		{
			cvSetImageROI(pImage, cvRect(0, nGapHeight, pTpPictureSize->width, pTpPictureSize->height));
			cvCopy(pTpPictureSize, pImage);
			cvResetImageROI(pImage);
		}
		cvReleaseImage(&pTpPictureSize);
	}

	else
	{
		pImage = cvCreateImage(cvSize(nTarWidth, nTarHeight), pTpPicture->depth, pTpPicture->nChannels); 
		cvResize(pTpPicture, pImage, cv::INTER_LANCZOS4);
	}
	cvReleaseImage(&pTpPicture);

	//-- Gray Image
	pGray = cvCreateImage(cvGetSize(pImage), IPL_DEPTH_8U, 1);
	if(!pGray)
	{
		cvReleaseImage(&pImage);
		cvReleaseImage(&pGray);
		return FALSE;
	}
	//-- GRAY
	cvCvtColor(pImage, pGray, cv::COLOR_RGB2GRAY);
	//-- 2013-08-20 jaehyun@esmlab.com
	//-- Threshold change
	int nThreshold = ESMGetValue(ESM_VALUE_ADJ_TH);
	cvThreshold(pGray, pGray, nThreshold, 255 , CV_THRESH_BINARY);


	vector<CPoint> arrBeginPoint;
	vector<CPoint> arrCenterPoint;
	vector<CPoint> arrPointRange;
	CPoint TpBeginP, PointRange;
	TpBeginP.x = ESMGetValue(ESM_VALUE_ADJ_POINTX);
	TpBeginP.y = ESMGetValue(ESM_VALUE_ADJ_FIRSTPOINTY);
	arrBeginPoint.push_back(TpBeginP);

	TpBeginP.y = ESMGetValue(ESM_VALUE_ADJ_FIRSTPOINTY) + ESMGetValue(ESM_VALUE_ADJ_FIRSTYRANGE);
	arrBeginPoint.push_back(TpBeginP);

	TpBeginP.y = ESMGetValue(ESM_VALUE_ADJ_FIRSTPOINTY) + ESMGetValue(ESM_VALUE_ADJ_FIRSTYRANGE) + ESMGetValue(ESM_VALUE_ADJ_SECONDYRANGE);
	arrBeginPoint.push_back(TpBeginP);

	PointRange.x = ESMGetValue(ESM_VALUE_ADJ_XRANGE);
	PointRange.y = ESMGetValue(ESM_VALUE_ADJ_FIRSTYRANGE);
	arrPointRange.push_back(PointRange);
	PointRange.y = ESMGetValue(ESM_VALUE_ADJ_SECONDYRANGE);
	arrPointRange.push_back(PointRange);
	PointRange.y = ESMGetValue(ESM_VALUE_ADJ_THIRDYRANGE);
	arrPointRange.push_back(PointRange);
	SearchPoint(pGray, &arrBeginPoint, &arrPointRange, &arrCenterPoint);

	if( arrCenterPoint.size() > 2)
		cvLine(pGray, cvPoint(arrCenterPoint.at(0).x, arrCenterPoint.at(0).y), cvPoint(arrCenterPoint.at(2).x, arrCenterPoint.at(2).y), cvScalar(0, 0, 0, 0), 1, 8, 0);

	for(int i =0 ;i < arrBeginPoint.size(); i++)
		cvRectangle(pGray, cvPoint(arrBeginPoint.at(i).x, arrBeginPoint.at(i).y), cvPoint(arrBeginPoint.at(i).x + arrPointRange.at(i).x, arrBeginPoint.at(i).y + arrPointRange.at(i).y), cvScalar(0, 0, 0, 0), 1, 8, 0);

	strSaveFile.Format(_T("%s_Prev.jpg"),strAdjustFile);
	wcstombs(pchPath, strSaveFile, MAX_PATH);	
	cvSaveImage(pchPath, pGray);

	if( arrCenterPoint.size() < 2)
	{
		cvReleaseImage(&pImage);
		cvReleaseImage(&pGray);
		return FALSE;
	}





	//회전
	*pdbAngle = atan2((double)(arrCenterPoint.at(2).y - arrCenterPoint.at(0).y), 
		(double)(arrCenterPoint.at(2).x - arrCenterPoint.at(0).x)) * (180/3.141592);


	//Size
	int nPixelDefaultSize = 0;
	if( nOriginWidth == 1920)
		nPixelDefaultSize = DEFAULT_PIXEL1920_DISTANCE;
	else if( nOriginWidth == 5472 && nOriginHeight != 3080)
		nPixelDefaultSize = DEFAULT_PIXEL5472_DISTANCE;
	else if( nOriginWidth == 5472 && nOriginHeight == 3080)
		nPixelDefaultSize = DEFAULT_PIXEL5472_3080_DISTANCE;
	else
		nPixelDefaultSize = DEFAULT_PIXEL1920_DISTANCE;

	dTargetLength = ESMGetValue(ESM_VALUE_ADJ_TARGETLENGTH	) * nPixelDefaultSize  / DEFAULT_TARGETLENGTH;
	if( dTargetDistance != 0.0)
		dTargetLength = dTargetLength / dTargetDistance;
	else
		dTargetLength =1.0;

	dTargetLength = dTargetLength * ESMGetValue(ESM_VALUE_ADJ_CAMERAZOOM) /DEFAULT_CAMERAZOOM;

	int nHeight = arrCenterPoint.at(2).y - arrCenterPoint.at(0).y;
	int nWidth = abs(arrCenterPoint.at(2).x - arrCenterPoint.at(0).x);
	if( nHeight != 0 || nWidth != 0 )
	{
		double dTpResize = 0.0;
		if( nWidth != 0)
			dTpResize = sqrt(double(nHeight * nHeight + nWidth * nWidth));
		else
			dTpResize = nHeight;

		if ( ESMGetValue(ESM_VALUE_ADJ_RESIZE_USE) )
			*dPictureSize = dTargetLength / dTpResize ;
		else
			*dPictureSize = 1;

		//CenterPoint

		pPoint->x = (float)arrCenterPoint.at(1).x;
		pPoint->y = (float)arrCenterPoint.at(1).y;
		pRotatePoint->x = pPoint->x;
		pRotatePoint->y = pPoint->y;

		pPoint->x = pRotatePoint->x = int((double)arrCenterPoint.at(1).x * *dPictureSize) ;
		pPoint->y = pRotatePoint->y = int((double)arrCenterPoint.at(1).y * *dPictureSize) ;
		//*pdbAngle = *pdbAngle * -1;
	}

	cvReleaseImage(&pImage);
	cvReleaseImage(&pGray);

	return TRUE;
}
*/

BOOL CESMImgMgr::MakeAvgImage(CString strTargetFolder, int nImageCount, int* arrModCamIndicater)
{
	CString strFileName, strRef1File, strRef2File;
	ImageRevisionMgr ImageReMgr;
	for(int i =1 ;i <= nImageCount; i++)
	{
		if( arrModCamIndicater[i - 1] == -1)
			continue;

		strFileName.Format(_T("%s\\frame%04d.jpg"), strTargetFolder, i);
		strRef1File.Format(_T("%s\\frame%04d.jpg"), strTargetFolder, i - 1);
		strRef2File.Format(_T("%s\\frame%04d.jpg"), strTargetFolder, i + 1);

		ImageReMgr.CallMakeAvaregeImage(strFileName, strRef1File, strRef2File, m_nImageMarginX, m_nImageMarginY);

	}
	return TRUE;
}

BOOL CESMImgMgr::AvgImageFileChange(CString strTargetFolder, int nImageCount)
{
	CString strFileName, strNewFile;
	ImageRevisionMgr ImageReMgr;
	for(int i =1 ;i <= nImageCount; i++)
	{
		strNewFile.Format(_T("%s\\frame%04d.jpg"), strTargetFolder, i);
		strFileName.Format(_T("%s\\M_frame%04d.jpg"), strTargetFolder, i);
		if(FileExists(strFileName) == TRUE)
		{
			DeleteFiles(strNewFile);
			CFile::Rename(strFileName, strNewFile);
		}
	}
	return TRUE;
}

BOOL CESMImgMgr::SearchPoint(IplImage*	pNewGray, vector<CPoint>* arrBeginPoint, vector<CPoint>* arrPointRange, vector<CPoint>* pArrPoint)
{
	CPoint TpBeginP;
	CPoint TpPoints;
	if( pNewGray->width < arrBeginPoint->at(2).x + arrPointRange->at(2).x)
		return FALSE;
	if( pNewGray->height < arrBeginPoint->at(2).y + arrPointRange->at(2).y)
		return FALSE;

	for( int i = 0; i< arrBeginPoint->size(); i++)
	{
		CPoint CenterPoint;
		CPoint tpPoint;
		vector<CPoint> arrPoint;
		int x1 = 0;
		int nImageWidth	= pNewGray->width;
		int nImageHeight = pNewGray->height;
		TpPoints = arrPointRange->at(i);

		for(int y1 = arrBeginPoint->at(i).y ; y1 < arrBeginPoint->at(i).y + TpPoints.y ; y1++)
		{
			for(x1 = arrBeginPoint->at(i).x ; x1 < arrBeginPoint->at(i).x + TpPoints.x ; x1++)
			{
				if((unsigned char)pNewGray->imageData[y1*pNewGray->widthStep + x1] < 10)// 흑을 만난다면 Continue;
				{
					tpPoint.x = x1;
					tpPoint.y = y1;
					arrPoint.push_back(tpPoint);
					break;
				}
			}
			if( x1 == arrBeginPoint->at(i).x + TpPoints.x)
				continue;

			//Detect Right Line
			for(x1 = arrBeginPoint->at(i).x + TpPoints.x ; x1 > arrBeginPoint->at(i).x ; x1--)
			{
				if((unsigned char)pNewGray->imageData[y1*pNewGray->widthStep + x1] < 10)// 흑색을 만난다면 Continue;
				{
					tpPoint.x = x1;
					tpPoint.y = y1;
					arrPoint.push_back(tpPoint);
					break;
				}
			}
		}
		if( arrPoint.size() < 1)
			return FALSE;
		int dMinX = arrPoint.at(0).x, dMinY = arrPoint.at(0).y, dMaxX = arrPoint.at(0).x, dMaxY = arrPoint.at(0).y;
		for(int nIndex =0 ;nIndex < arrPoint.size() ; nIndex++)
		{
			if( arrPoint.at(nIndex).x < dMinX)
				dMinX = arrPoint.at(nIndex).x;
			else if( arrPoint.at(nIndex).y < dMinY)
				dMinY = arrPoint.at(nIndex).y;
			else if( arrPoint.at(nIndex).x > dMaxX)
				dMaxX = arrPoint.at(nIndex).x;
			else if( arrPoint.at(nIndex).x > dMaxY)
				dMaxY = arrPoint.at(nIndex).y;
		}
		TpBeginP.x = (dMinX + dMaxX) / 2;
		TpBeginP.y = (dMinY + dMaxY) / 2;
		pArrPoint->push_back(TpBeginP);

	}
	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief		Move Image Via Adjust
//! @date		2013-04-25
//! @attention	Image -> Move Position -> Save Image with Margin
//! @note	 	hongsu.jung
//------------------------------------------------------------------------------ 
void CESMImgMgr::SetAdjust(CString strSrcFile, CString strAdjFile, CString strTargetFile, CvPoint2D32f ptAdj, CvPoint2D32f ptLT, CvPoint2D32f ptRB, double dbAngleAdjust, double dAdjustSize
	, double dAdjustDistance, double dbMaxAngle, CvPoint2D32f ptRotate, CString strDSC, BOOL b3D)
{

	HANDLE handle = NULL;

	//-- 2013-10-07 hongsu@esmlab.com
	//-- Set Values 
	AdjustInfo* pAdjInfo = new AdjustInfo();

	pAdjInfo->m_pImgMgr				= this;
	pAdjInfo->m_AdjstrSrcFile		= strSrcFile	;
	pAdjInfo->m_AdjstrAdjFile		= strAdjFile	;
	pAdjInfo->m_AdjstrTargetFile	= strTargetFile	;	

	pAdjInfo->m_AdjptAdj			= ptAdj			;
	pAdjInfo->m_AdjptLT				= ptLT			;
	pAdjInfo->m_AdjptRB				= ptRB			;
	pAdjInfo->m_AdjptRotate			= ptRotate		;

	pAdjInfo->m_AdjdbAngleAdjust	= dbAngleAdjust	;
	pAdjInfo->m_AdjdAdjustSize		= dAdjustSize	;
	pAdjInfo->m_AdjdbMaxAngle		= dbMaxAngle	;
	pAdjInfo->m_strDSC				= strDSC	;
	pAdjInfo->m_b3D					= b3D			;

	//-- 2013-10-22 hongsu@esmlab.com
	//-- Add Count 
	if(!b3D)	AddAdjust(strTargetFile);
	else		AddAdjust(strAdjFile);
	handle = (HANDLE) _beginthread( _SetAdjust, 0, (void*)pAdjInfo); // create thread
}

void _SetAdjust(void *param)
{
	AdjustInfo* pAdjInfo= (AdjustInfo*)param;
	
	CESMImgMgr* pImgMgr = (CESMImgMgr*)pAdjInfo->m_pImgMgr;
	CString strSrcFile, strAdjFile, strTargetFile, strDSC;
	CvPoint2D32f ptAdj, ptLT, ptRB, ptRotate;
	double dbAngleAdjust = 0.0, dbMaxAngle = 0.0, dAdjustSize = 1.0;
	BOOL b3D;

	strSrcFile		= pAdjInfo->m_AdjstrSrcFile		;
	strAdjFile		= pAdjInfo->m_AdjstrAdjFile		;
	strTargetFile	= pAdjInfo->m_AdjstrTargetFile	;

	ptAdj			= pAdjInfo->m_AdjptAdj			;
	ptLT			= pAdjInfo->m_AdjptLT			;
	ptRB			= pAdjInfo->m_AdjptRB			;
	ptRotate		= pAdjInfo->m_AdjptRotate		;

	dAdjustSize		= pAdjInfo->m_AdjdAdjustSize	;
	dbAngleAdjust	= pAdjInfo->m_AdjdbAngleAdjust	;
	dbMaxAngle		= pAdjInfo->m_AdjdbMaxAngle		;
	strDSC			= pAdjInfo->m_strDSC		;
	if( dAdjustSize == 0)
		dAdjustSize = 1.0;
	if( dbAngleAdjust == 0.0)
		dbAngleAdjust = -90.0;

	b3D				= pAdjInfo->m_b3D				;
	delete pAdjInfo;
	pAdjInfo = NULL;


	IplImage*	pPicture;
	IplImage*	pMoveImage;
	IplImage*	pTpPicture = NULL;
	CString		strSaveFile;

	char pchPath[MAX_PATH] = {0};
	int nRequiredSize = (int)wcstombs(pchPath, strSrcFile, MAX_PATH);

	
	//-- Load Image
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
	{
		IplImage*	pTpPictureSize = NULL;;
		int nTarHeight = 1238, nRealHeight = 0;
		int nTarWidth = 2200;
//		pPicture = cvLoadImage(pchPath);//, cv::IMREAD_UNCHANGED);
		if(!pPicture)
		{
			ESMLog(0,_T("Non Exsit Adjust Picture[%s]"),strSrcFile);
			return ;
		}

// 		nRealHeight = (int)((double)pTpPicture->height / double((double)pTpPicture->width / (double)nTarWidth));
// 		int nGapHeight = nRealHeight - nTarHeight;
// 
// // 		if( nGapHeight != 0)
// // 		{
// // 			nGapHeight  = nGapHeight /2;
// 
// 			// 1:1 비율로 변환
// 			pTpPictureSize = cvCreateImage(cvSize(nTarWidth, nRealHeight), pTpPicture->depth, pTpPicture->nChannels); 
// 			cvResize(pTpPicture, pTpPictureSize, cv::INTER_LANCZOS4);
// 			cvReleaseImage(&pTpPicture);
// 
// 			pPicture = cvCreateImage(cvSize(nTarWidth, nTarHeight), pTpPictureSize->depth, pTpPictureSize->nChannels); 
// 			if( nGapHeight >= 0)	// Gab 만큼 Image를 제거
// 			{
// 				cvSetImageROI(pTpPictureSize, cvRect(0, nGapHeight, pPicture->width, pPicture->height));
// 				cvCopy(pTpPictureSize, pPicture);
// 				cvResetImageROI(pTpPictureSize);
// 			}
// 			else					// Gab 만큼 Image에 삽입
// 			{
// 				cvSetImageROI(pPicture, cvRect(0, nGapHeight, pTpPictureSize->width, pTpPictureSize->height));
// 				cvCopy(pTpPictureSize, pPicture);
// 				cvResetImageROI(pPicture);
// 			}
// 			cvReleaseImage(&pTpPictureSize);
// 		}
// 		else
// 		{
// 			pPicture = cvCreateImage(cvSize(nTarWidth * dAdjustSize, nTarHeight * dAdjustSize), pTpPicture->depth, pTpPicture->nChannels); 
// 			cvResize(pTpPicture, pPicture, cv::INTER_LANCZOS4);
// 			cvReleaseImage(&pTpPicture);
// 		}
	}
	else
	{
		//pPicture = cvLoadImage(pchPath);//, cv::IMREAD_UNCHANGED);
		//if(!pPicture)
		//{
		//	ESMLog(0,_T("Non Exsit Adjust Picture[%s]"),strSrcFile);
		//	return ;
		//}
	}
	

//	int nHeight = pPicture->height;
//	int nWidth	= pPicture->width;

	//#################Image Size Adjust Start#################
// 
// 	if ( ESMGetValue(ESM_VALUE_ADJ_RESIZE_USE) )
// 	{
// 		pTpPicture = cvCreateImage(cvSize(pPicture->width * dAdjustSize, pPicture->height * dAdjustSize), pPicture->depth, pPicture->nChannels); 
// 
// 		cvResize(pPicture, pTpPicture, cv::INTER_LANCZOS4);  // For Resize
// 		//-- Draw All blank
// 		memset(pPicture->imageData,0x00,sizeof(char)*nHeight*nWidth*3);
// 		int nImageWidth = 0, nImageHight = 0;
// 		if( dAdjustSize > 1)
// 		{
// 			nImageWidth = pPicture->width;
// 			nImageHight = pPicture->height;
// 			cvSetImageROI(pTpPicture, cvRect(0, 0, nImageWidth, nImageHight));
// 		}
// 		else
// 		{
// 			nImageWidth = pPicture->width * dAdjustSize;
// 			nImageHight = pPicture->height * dAdjustSize;
// 			cvSetImageROI(pPicture, cvRect(0, 0, nImageWidth, nImageHight));
// 		}
// 		cvCopy(pTpPicture, pPicture);
// 		cvResetImageROI(pPicture);
// 		cvResetImageROI(pTpPicture);
// 		cvReleaseImage(&pTpPicture);
// 	}
	//################# Image Size Adjust End#################
	/*
	int nStartPointX, nStartPointY;
	if(abs(ptLT.x) < abs(ptRB.x))
		nStartPointX = cvRound(abs(ptRB.x));
	else
		nStartPointX = cvRound(abs(ptLT.x));
	if(abs(ptLT.y) < abs(ptRB.y))
		nStartPointY = cvRound(abs(ptRB.y));
	else
		nStartPointY = cvRound(abs(ptLT.y));

	//-- Now Point X,Y
	int nChangeX = -1 * cvRound(ptAdj.x);
	int nChangeY = -1 * cvRound(ptAdj.y);

	*/

	double nCutRatio = 4;
	int nXsize = (double)pPicture->width * (100 -nCutRatio)/100;
	int nYsize = (double)pPicture->height * (100 -nCutRatio)/100;
	int nMarginX = ((double)pPicture->width * nCutRatio/100/ 2);
	int nMarginY = ((double)pPicture->height * nCutRatio/100/ 2);
	pMoveImage = cvCreateImage(cvSize(nXsize , nYsize), pPicture->depth, pPicture->nChannels);
	if(!pMoveImage)
	{
	cvReleaseImage(&pMoveImage);
	ESMLog(0,_T("Non Exsit pMoveImage in SetAdjust"));
	return ;
	}
	//-- Draw All blank
	memset(pMoveImage->imageData,0x00,sizeof(char)*nXsize*nYsize*3);

	cvSetImageROI(pPicture, cvRect(nMarginX - ptAdj.x, nMarginY - ptAdj.y, nXsize, nYsize));
	cvSetImageROI(pMoveImage, cvRect(0, 0, nXsize, nYsize));

	if(pPicture->roi->width != pMoveImage->roi->width || pPicture->roi->height != pMoveImage->roi->height)
		ESMLog(0,_T("[Adjust] Not same Source ROI & Target ROI"));
	else
		cvCopy(pPicture, pMoveImage);

	cvResetImageROI(pPicture);
	cvResetImageROI(pMoveImage);
	cvReleaseImage(&pPicture);

	//   	-- 2013-08-27 jaehyun@esmlab.com
	//Image rotate
	CvPoint2D32f rotate_center = cvPoint2D32f(ptRotate.x ,ptRotate.y);
	//CvPoint2D32f rotate_center = cvPoint2D32f(pHistmatImage->width/2, pHistmatImage->height/2);	//Set rotate center
	CvMat *rot_mat = cvCreateMat( 2, 3, CV_32FC1);
	dbAngleAdjust = -1 * (dbAngleAdjust + 90);
	cv2DRotationMatrix( rotate_center, dbAngleAdjust, dAdjustSize, rot_mat); // 메트릭스 변환

	// cvWarpAffine( pHistmatImage, pHistmatImage, rot_mat, cv::INTER_LINEAR+cv::WARP_FILL_OUTLIERS); // Color Adjust
	cvWarpAffine( pMoveImage, pMoveImage, rot_mat, cv::INTER_LANCZOS4+cv::WARP_FILL_OUTLIERS); // 선형보간

	//-- Release rotate Images
	cvReleaseMat(&rot_mat);


	double nMarginRatio = 3;
	nXsize = (double)pMoveImage->width * (100 -nMarginRatio)/100;
	nYsize = (double)pMoveImage->height * (100 -nMarginRatio)/100;
	nMarginX = ((double)pMoveImage->width * nMarginRatio/100/ 2);
	nMarginY = ((double)pMoveImage->height * nMarginRatio/100/ 2);
	pPicture = cvCreateImage(cvSize(nXsize , nYsize), pMoveImage->depth, pMoveImage->nChannels);
	cvSetImageROI(pPicture, cvRect(0, 0, nXsize, nYsize));
	cvSetImageROI(pMoveImage, cvRect(nMarginX, nMarginY, nXsize, nYsize));

	if(pPicture->roi->width != pMoveImage->roi->width || pPicture->roi->height != pMoveImage->roi->height)
		ESMLog(0,_T("[Adjust] Not same Source ROI & Target ROI"));
	else
		cvCopy(pMoveImage, pPicture);

	cvResetImageROI(pPicture);
	cvResetImageROI(pMoveImage);

	cvReleaseImage(&pMoveImage);
	pMoveImage = cvCreateImage(cvSize(1920 , 1080), pPicture->depth, pPicture->nChannels);
	cvResize(pPicture, pMoveImage, cv::INTER_LANCZOS4);
	/*
	//-- 2013-08-27 jaehyun@esmlab.com
	//-- Erasing the blank of rotated image
	int nMarginX = 0, nMarginY = 0;
	if( dbMaxAngle != 0.0)
	{
		nMarginX = abs(abs(dbMaxAngle) - abs(90))*(nWidth/ROTATE_ERASE_MARGIN_SCALE_X) + nStartPointX + 1;
		nMarginY =  abs(abs(dbMaxAngle) - abs(90))*(nHeight/ROTATE_ERASE_MARGIN_SCALE_Y) + nStartPointY + 1;
	}
// 	pImgMgr->SetImageMarginX(nMarginX);
// 	pImgMgr->SetImageMarginY(nMarginY);

	*/
	//-- Image Size
// 	int nXsize = nWidth - (nMarginX*2);
// 	int nYsize = nHeight - (nMarginY*2);
// 	pMoveImage = cvCreateImage(cvSize(nXsize , nYsize), pPicture->depth, pPicture->nChannels);
// 	if(!pMoveImage)
// 	{
// 		cvReleaseImage(&pMoveImage);
// 		ESMLog(0,_T("Non Exsit pMoveImage in SetAdjust"));
// 		return ;
// 	}
// 	//-- Draw All blank
// 	memset(pMoveImage->imageData,0x00,sizeof(char)*nXsize*nYsize*3);
// 	
// 	cvSetImageROI(pPicture, cvRect(nMarginX - ptAdj.x, nMarginY - ptAdj.y, nXsize, nYsize));
// 	cvSetImageROI(pMoveImage, cvRect(0, 0, nXsize, nYsize));
// 
// 	if(pPicture->roi->width != pMoveImage->roi->width || pPicture->roi->height != pMoveImage->roi->height)
// 		ESMLog(0,_T("[Adjust] Not same Source ROI & Target ROI"));
// 	else
// 		cvCopy(pPicture, pMoveImage);
// 
// 	cvResetImageROI(pPicture);
// 	cvResetImageROI(pMoveImage);
// 	cvReleaseImage(&pPicture);
	
	CString strPath, strFileName, strAdjustFile;
	ESMUtil::GetFileNameAndPath(strSrcFile, strFileName, strPath );
	strPath = strPath.Left(strPath.GetLength() - 1);
	ESMUtil::GetFileNameAndPath(strPath, strFileName, strPath );
	wcstombs(pchPath, strAdjFile, MAX_PATH);

	//cvSaveImage(pchPath, pMoveImage);
	//cvSaveImage(pchPath, pMoveImage);

	//-- Release Images
	cvReleaseImage(&pPicture);
	cvReleaseImage(&pMoveImage);
	//cvReleaseImage(&pTpPicture);
	
	
	//-- 2013-10-07 hongsu@esmlab.com
	//-- Copy To Target
	if(!b3D)
	{
		CESMFileOperation fo;
		fo.Copy(strAdjFile, strTargetFile);
		//-- 2013-10-07 hongsu@esmlab.com
		//-- Release Adjust A\]\\ay 
		pImgMgr->FinishAdjust(strTargetFile);
	}
	else
	{
		pImgMgr->FinishAdjust(strAdjFile);
	}	
}



#include <iostream>
#include "opencv2/opencv.hpp"
#include "opencv2/core/cuda.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/cudaarithm.hpp"
#include "opencv2/cudaimgproc.hpp"

void CESMImgMgr::SetAdjust2(stImageBuffer* pImageBuffer)
{
	HANDLE handle = NULL;

	CESMImgMgr* pImgMgr = (CESMImgMgr*)pImageBuffer->pAdjustInfo.m_pImgMgr;
	CString strSrcFile, strAdjFile, strTargetFile, strDSC;
	CvPoint2D32f ptAdj, ptLT, ptRB, ptRotate;
	double dbAngleAdjust = 0.0, dbMaxAngle = 0.0, dAdjustSize = 1.0;
	BOOL b3D;

	strSrcFile		= pImageBuffer->pAdjustInfo.m_AdjstrSrcFile		;
	strAdjFile		= pImageBuffer->pAdjustInfo.m_AdjstrAdjFile		;
	strTargetFile	= pImageBuffer->pAdjustInfo.m_AdjstrTargetFile	;

	ptAdj			= pImageBuffer->pAdjustInfo.m_AdjptAdj			;
	ptLT			= pImageBuffer->pAdjustInfo.m_AdjptLT			;
	ptRB			= pImageBuffer->pAdjustInfo.m_AdjptRB			;
	ptRotate		= pImageBuffer->pAdjustInfo.m_AdjptRotate		;

	dAdjustSize		= pImageBuffer->pAdjustInfo.m_AdjdAdjustSize	;
	dbAngleAdjust	= pImageBuffer->pAdjustInfo.m_AdjdbAngleAdjust	;
	dbMaxAngle		= pImageBuffer->pAdjustInfo.m_AdjdbMaxAngle		;
	strDSC			= pImageBuffer->pAdjustInfo.m_strDSC		;
	if( dAdjustSize == 0)
		dAdjustSize = 1.0;
	if( dbAngleAdjust == 0.0)
		dbAngleAdjust = -90.0;
	b3D				= pImageBuffer->pAdjustInfo.m_b3D				;

	IplImage*	pPicture = pImageBuffer->cvImage;
	IplImage*	pMoveImage;
	IplImage*	pTpPicture = NULL;
	CString		strSaveFile;

	char pchPath[MAX_PATH] = {0};
	int nRequiredSize = (int)wcstombs(pchPath, strSrcFile, MAX_PATH);

	//-- Load Image
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
	{
		IplImage*	pTpPictureSize = NULL;;
		int nTarHeight = 1080, nRealHeight = 0;
		int nTarWidth = 1920;
//		pTpPicture = cvLoadImage(pchPath);//, cv::IMREAD_UNCHANGED);
		if(!pTpPicture)
		{
			ESMLog(0,_T("Non Exsit Adjust Picture[%s]"),strSrcFile);
			return ;
		}

		nRealHeight = (int)((double)pTpPicture->height / double((double)pTpPicture->width / (double)nTarWidth));
		int nGapHeight = nRealHeight - nTarHeight;
		nGapHeight  = nGapHeight /2;

		// 1:1 비율로 변환
		pTpPictureSize = cvCreateImage(cvSize(nTarWidth, nRealHeight), pTpPicture->depth, pTpPicture->nChannels); 
		cvResize(pTpPicture, pTpPictureSize, cv::INTER_LANCZOS4);
		cvReleaseImage(&pTpPicture);

		pPicture = cvCreateImage(cvSize(nTarWidth, nTarHeight), pTpPictureSize->depth, pTpPictureSize->nChannels); 
		if( nGapHeight >= 0)	// Gab 만큼 Image를 제거
		{
			cvSetImageROI(pTpPictureSize, cvRect(0, nGapHeight, pPicture->width, pPicture->height));
			cvCopy(pTpPictureSize, pPicture);
			cvResetImageROI(pTpPictureSize);
		}
		else					// Gab 만큼 Image에 삽입
		{
			cvSetImageROI(pPicture, cvRect(0, nGapHeight, pTpPictureSize->width, pTpPictureSize->height));
			cvCopy(pTpPictureSize, pPicture);
			cvResetImageROI(pPicture);
		}
		cvReleaseImage(&pTpPictureSize);
	}

	int nHeight = pPicture->height;
	int nWidth	= pPicture->width;
	ptRotate.x = ptRotate.x * dAdjustSize;
	ptRotate.y = ptRotate.y * dAdjustSize;
	//#################Image Size Adjust Start#################
	pTpPicture = cvCreateImage(cvSize(pPicture->width * dAdjustSize, pPicture->height * dAdjustSize), pPicture->depth, pPicture->nChannels); 

	cvResize(pPicture, pTpPicture, cv::INTER_LANCZOS4);  // For Resize
	//-- Draw All blank
	memset(pPicture->imageData,0x00,sizeof(char)*nHeight*nWidth*3);
	int nImageWidth = 0, nImageHight = 0;
	if( dAdjustSize > 1)
	{
		nImageWidth = pPicture->width;
		nImageHight = pPicture->height;
		cvSetImageROI(pTpPicture, cvRect(0, 0, nImageWidth, nImageHight));
	}
	else
	{
		nImageWidth = pPicture->width * dAdjustSize;
		nImageHight = pPicture->height * dAdjustSize;
		cvSetImageROI(pPicture, cvRect(0, 0, nImageWidth, nImageHight));
	}
	cvCopy(pTpPicture, pPicture);
	cvResetImageROI(pPicture);
	cvResetImageROI(pTpPicture);
	cvReleaseImage(&pTpPicture);
	//################# Image Size Adjust End#################

	int nStartPointX, nStartPointY;
	if(abs(ptLT.x) < abs(ptRB.x))
		nStartPointX = cvRound(abs(ptRB.x));
	else
		nStartPointX = cvRound(abs(ptLT.x));
	if(abs(ptLT.y) < abs(ptRB.y))
		nStartPointY = cvRound(abs(ptRB.y));
	else
		nStartPointY = cvRound(abs(ptLT.y));

	//-- Now Point X,Y
	int nChangeX = -1 * cvRound(ptAdj.x);
	int nChangeY = -1 * cvRound(ptAdj.y);

	//Image rotate
	CvPoint2D32f rotate_center = cvPoint2D32f(ptRotate.x ,ptRotate.y);
	CvMat *rot_mat = cvCreateMat( 2, 3, CV_32FC1);
	dbAngleAdjust = -1 * (dbAngleAdjust + 90);
	cv2DRotationMatrix( rotate_center, dbAngleAdjust, 1.0, rot_mat); // 메트릭스 변환
	cvWarpAffine( pPicture, pPicture, rot_mat, cv::INTER_LANCZOS4+cv::WARP_FILL_OUTLIERS); // 선형보간

	//-- Release rotate Images
	cvReleaseMat(&rot_mat);

	//-- Erasing the blank of rotated image
	int nMarginX = 0, nMarginY = 0;
	if( dbMaxAngle != 0.0)
	{
		nMarginX = abs(abs(dbMaxAngle) - abs(90))*(nWidth/ROTATE_ERASE_MARGIN_SCALE_X) + nStartPointX + 1;
		nMarginY =  abs(abs(dbMaxAngle) - abs(90))*(nHeight/ROTATE_ERASE_MARGIN_SCALE_Y) + nStartPointY + 1;
	}

	pImgMgr->SetImageMarginX(nMarginX);
	pImgMgr->SetImageMarginY(nMarginY);

	//-- Image Size
	int nXsize = nWidth - (nMarginX*2);
	int nYsize = nHeight - (nMarginY*2);
	pMoveImage = cvCreateImage(cvGetSize(pPicture), pPicture->depth, pPicture->nChannels);
	if(!pMoveImage)
	{
		cvReleaseImage(&pMoveImage);
		ESMLog(0,_T("Non Exsit pMoveImage in SetAdjust"));
		return ;
	}

	//-- Draw All blank
	memset(pMoveImage->imageData,0x00,sizeof(char)*nHeight*nWidth*3);

	cvSetImageROI(pPicture, cvRect(nMarginX + nChangeX, nMarginY + nChangeY, nXsize, nYsize));
	cvSetImageROI(pMoveImage, cvRect(nMarginX, nMarginY, nXsize, nYsize));

	if(pPicture->roi->width != pMoveImage->roi->width || pPicture->roi->height != pMoveImage->roi->height)
		ESMLog(0,_T("[Adjust] Not same Source ROI & Target ROI"));
	else
		cvCopy(pPicture, pMoveImage);

	cvResetImageROI(pPicture);
	cvResetImageROI(pMoveImage);

	CString strPath, strFileName, strAdjustFile;
	ESMUtil::GetFileNameAndPath(strSrcFile, strFileName, strPath );
	strPath = strPath.Left(strPath.GetLength() - 1);
	ESMUtil::GetFileNameAndPath(strPath, strFileName, strPath );
	wcstombs(pchPath, strAdjFile, MAX_PATH);

	CvPoint Pt1, Pt2;
	int thickness = nMarginY;
	Pt1.x = 0;  	Pt1.y = nMarginY /2;	Pt2.x = pMoveImage->width; 	Pt2.y = Pt1.y;
	cvLine( pMoveImage, Pt1, Pt2, CV_RGB(0, 0, 0), thickness);

	Pt1.y = pMoveImage->height - nMarginY /2;	Pt2.y = Pt1.y;
	cvLine( pMoveImage, Pt1, Pt2, CV_RGB(0, 0, 0), thickness);

	thickness = nMarginX;
	Pt1.x = nMarginX /2;  	Pt1.y = 0;	Pt2.x = Pt1.x; 	Pt2.y = pMoveImage->height;
	cvLine( pMoveImage, Pt1, Pt2, CV_RGB(0, 0, 0), thickness);

	Pt1.x = pMoveImage->width - nMarginX /2;	Pt2.x = Pt1.x;
	cvLine( pMoveImage, Pt1, Pt2, CV_RGB(0, 0, 0), thickness);

	// 2014.06.09 hjjang Add Banner Image
	int nIndex= pImageBuffer->nIndex + 1;
	CString strBannerPath;
	strBannerPath.Format(_T("%s\\05.Banner\\%d_Frame\\%d.png") ,ESMGetPath(ESM_PATH_IMAGE), nIndex, nIndex);
//	pImgMgr->AddBanner(pMoveImage, strBannerPath);

	cvCopy(pMoveImage, pPicture);

	pImageBuffer->WakingImage = cvCreateImage(cvGetSize(pPicture), pPicture->depth, pPicture->nChannels);
	cvCopy(pPicture, pImageBuffer->WakingImage);

	//-- Release Images
	cvReleaseImage(&pMoveImage);
	cvReleaseImage(&pTpPicture);

	//-- Copy To Target
	if(!b3D)
	{
		CESMFileOperation fo;
		fo.Copy(strAdjFile, strTargetFile);
		pImgMgr->FinishAdjust(strTargetFile);
	}
	else
		pImgMgr->FinishAdjust(strAdjFile);

	return ;
}

int CESMImgMgr::GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	ImageCodecInfo* pImageCodecInfo = NULL;

	GetImageEncodersSize(&num, &size);
	if(size == 0)
		return -1;  // Failure

	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if(pImageCodecInfo == NULL)
		return -1;  // Failure

	GetImageEncoders(num, size, pImageCodecInfo);

	for(UINT j = 0; j < num; ++j)
	{
		if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}    
	}

	free(pImageCodecInfo);
	return -1;  // Failure
}

int CESMImgMgr::GetIntensity(IplImage* src_hlsimg)
{
	// 명도 구하는 법
	IplImage* pMoveImage	= cvCreateImage(cvSize(src_hlsimg->width,src_hlsimg->height), 8, src_hlsimg->nChannels);
	IplImage* Hue			= cvCreateImage(cvSize(src_hlsimg->width,src_hlsimg->height), 8, 1);
	IplImage* Intensity		= cvCreateImage(cvSize(src_hlsimg->width, src_hlsimg->height), 8, 1);
	IplImage* Saturation	= cvCreateImage(cvSize(src_hlsimg->width, src_hlsimg->height), 8, 1);

	cvCvtColor(src_hlsimg, pMoveImage, CV_BGR2HLS);   //src_hlsimg IplImage 구조체에 HLS 이미지 담긴다
	cvCvtColor(pMoveImage, src_hlsimg, CV_BGR2HSV);   //src_hlsimg IplImage 구조체에 HLS 이미지 담긴다
	//cvCvtPixToPlane( pMoveImage, Hue, Intensity, Saturation, NULL );  //HLS 이미지 각 속성별로 나눔
	cvSplit(pMoveImage, Hue, Intensity, Saturation, NULL);

	int nTotalIntensity = 0;
	int nInterval = 250;
	for (int i = nInterval; i < pMoveImage->height - nInterval; i++)
	{
		for (int j = nInterval; j < pMoveImage->width - nInterval; j++  )
		{
			nTotalIntensity += cvGetReal2D(Intensity, i, j);
		}
	}
	nTotalIntensity = nTotalIntensity / ((pMoveImage->height - 2*nInterval) * (pMoveImage->width - 2*nInterval));

	//-- Save File
	char pchPath[MAX_PATH] = {0};
	CString strTp;
	static int nIndex = 0;
	strTp.Format(_T("C:\\Test_%d.jpg"), nIndex++);
	wcstombs(pchPath, strTp, MAX_PATH);
	//cvSaveImage(pchPath, pMoveImage);


	cvReleaseImage(&pMoveImage);
	cvReleaseImage(&Hue);
	cvReleaseImage(&Intensity);
	cvReleaseImage(&Saturation);

	return nTotalIntensity;
}

//------------------------------------------------------------------------------ 
//! @brief		Conversion Coordinate
//! @date		2013-09-03
//! @attention	Absolute Position -> Relative Position
//! @note	 	jaehyun.kim
//------------------------------------------------------------------------------ 
double CESMImgMgr::ToRelativePosition(double nAbsolute, int nSize)
{
	double dbRelative;

	dbRelative = nAbsolute / (nSize/100);
	return dbRelative;
}

//------------------------------------------------------------------------------ 
//! @brief		Conversion Coordinate
//! @date		2013-09-03
//! @attention	Relative Position -> Absolute Position
//! @note	 	jaehyun.kim
//------------------------------------------------------------------------------ 
double CESMImgMgr::ToAbsolutePosition(double nRelative, int nSize)
{
	double nAbsolute;

	nAbsolute = nRelative*(nSize/100);
	return nAbsolute;

}


//------------------------------------------------------------------------------
//! @brief		Color Correction : Histogram matching method
//! @date		2013-09-30
//! @reference	http://code.google.com/p/faps/source/browse/trunk/fAPS/MyTabThree.cpp?r=226
//! @note	 	jaehyun.kim
//------------------------------------------------------------------------------ 
#define HISTMATCH_EPSILON 0.000001

// Compute histogram and CDF for an image with mask
void do1ChnHist(const cv::Mat& _i, const cv::Mat& mask, double* h, double* cdf)
{
	cv::Mat _t = _i.reshape(1,1); 

	// Get the histogram with or without the mask
	if (true)
	{
		cv::Mat _tm;
		mask.copyTo(_tm);
		_tm = _tm.reshape(1,1);
		for(int p=0; p<_t.cols; p++)
		{                      
			uchar m = _tm.at<uchar>(0,p);
			if(m > 0)
			{ // Mask value
				uchar c = _t.at<uchar>(0,p); // Image value
				h[c] += 1.0;
			}
		}
	}
	else {
		for(int p=0; p<_t.cols; p++)
		{
			uchar c = _t.at<uchar>(0,p);   // Image value
			h[c] += 1.0;
		}
	}
	//normalize hist     
	cv::Mat _tmp(1,256,CV_64FC1,h);    
	double minVal,maxVal;   
	minMaxLoc(_tmp,&minVal,&maxVal);  
	_tmp = _tmp / maxVal;    
	cdf[0] = h[0];        
	for(int j=1; j < 256; j++)
	{              
		cdf[j] = cdf[j-1]+h[j];  
	}       
	//normalize CDF        
	_tmp.data = (uchar*)cdf;  
	minMaxLoc(_tmp,&minVal,&maxVal);   
	_tmp = _tmp / maxVal;
} 

void CESMImgMgr::HistogramMatchRefValue(char* strRefFile)
{
	//cv::Mat mRefImg = cvLoadImage(strRefFile);
	Mat mRefImg = imread(strRefFile);

	vector<cv::Mat> chns1;
	split(mRefImg,chns1);

	cv::Mat ref_hist = cv::Mat::zeros(1,256,CV_64FC1);

	for (int i=0;i<3;i++)
	{
		double* _ref_hist = ref_hist.ptr<double>();
		do1ChnHist(chns1[i],mRefImg,_ref_hist,&m_dbRefCDF[i*256]);
	}

	mRefImg.release();
	ref_hist.release();
}

char* CESMImgMgr::HistogramMatch(char* strSrcFile)
{
	//cv::Mat mSrcImg = cvLoadImage(strSrcFile);
	Mat mSrcImg = imread(strSrcFile);

	vector<cv::Mat> chns;
	split(mSrcImg,chns);
	cv::Mat src_hist = cv::Mat::zeros(1,256,CV_64FC1);
	cv::Mat src_cdf = cv::Mat::zeros(1,256,CV_64FC1);
	cv::Mat Mv(1,256,CV_8UC1);
	uchar* M = Mv.ptr<uchar>();

	for (int i=0;i<3;i++)
	{
		uchar last = 0;
		double* _src_cdf = src_cdf.ptr<double>();
		double* _src_hist = src_hist.ptr<double>();
		do1ChnHist(chns[i],mSrcImg,_src_hist,_src_cdf);
		for(int j=0;j<src_cdf.cols;j++)
		{
			double F1j = _src_cdf[j];
			for(uchar k = last; k<src_cdf.cols; k++)
			{
				double F2k = m_dbRefCDF[k + (i*256)];
				if(abs(F2k - F1j) < HISTMATCH_EPSILON || F2k > F1j)
				{
					M[j] = k;
					last = k;
					break;
				}
			}
		}
		cv::Mat lut(1,256,CV_8UC1,M);
		LUT(chns[i],lut,chns[i]);
	}

	cv::Mat res;
	merge(chns,res);
	imwrite(strSrcFile, res);

	mSrcImg.release();
	src_cdf.release();
	src_hist.release();
	Mv.release();

	return strSrcFile;
}

BOOL CESMImgMgr::IsFinishAdjust()
{
	ESMEvent* pMsg	= new ESMEvent();
	pMsg->message	= WM_ESM_VIEW_OBJ_PROGRESS;
	pMsg->nParam1	= m_nObj;		
	pMsg->nParam2	= m_nKind;

	EnterCriticalSection (&_ESMImageEngine);		
	int nAll = AdjustGetCount();
	LeaveCriticalSection (&_ESMImageEngine);

	if(!nAll)
	{
		m_arCheckAdjust.clear();
		pMsg->pParam	= (LPARAM)100;
		::SendMessage(m_hMainWnd, WM_ESM, WM_ESM_VIEW, (LPARAM)pMsg);
		return TRUE;
	}

	//-- 2013-10-08 hongsu@esmlab.com
	//-- Calculate %	
	if(m_nAddFile)
	{
		float fProgress;
		fProgress = 100 - (float)((float)nAll / (float)m_nAddFile * 100);		
		pMsg->pParam	= (LPARAM)fProgress;
		::SendMessage(m_hMainWnd, WM_ESM, WM_ESM_VIEW, (LPARAM)pMsg);		
	}
	return FALSE;
}

void CESMImgMgr::AddAdjust(CString strFile)
{
	EnterCriticalSection (&_ESMImageEngine);
	m_arCheckAdjust.push_back(strFile);
	m_nAddFile++;
	LeaveCriticalSection (&_ESMImageEngine);

}

CString CESMImgMgr::GetCheckAdjust(int nIndex)
{
	int nAll = AdjustGetCount();
	if(nAll <= nIndex )
		return _T("");
	return m_arCheckAdjust[nIndex];
}


int CESMImgMgr::AdjustGetCount()
{
	EnterCriticalSection (&_ESMImageEngine);
	int nAll = m_arCheckAdjust.size();
	LeaveCriticalSection (&_ESMImageEngine);
	return nAll;
}

void CESMImgMgr::FinishAdjust(CString strFile)
{
	CString strExist;
	EnterCriticalSection (&_ESMImageEngine);
	int nAll = m_arCheckAdjust.size();
	while(nAll--)
	{
		strExist = GetCheckAdjust(nAll);				
		if(strExist == strFile)
		{
			m_arCheckAdjust.erase(m_arCheckAdjust.begin()+nAll);
			LeaveCriticalSection (&_ESMImageEngine);
			break;
		}		
	}	
	LeaveCriticalSection (&_ESMImageEngine);
}

//------------------------------------------------------------------------------
//! @brief		Create 3D File from Left/Right Image
//! @date		2013-10-07
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//------------------------------------------------------------------------------
void CESMImgMgr::Create3DFile(CString strAdjFileLeft, CString strAdjFileRight, CString strTargetFile)
{
	Adjust3DInfo* p3DInfo = new Adjust3DInfo;

	p3DInfo->m_pImgMgr		 = this;
	p3DInfo->m_strAdjFile[0] = strAdjFileLeft;
	p3DInfo->m_strAdjFile[1] = strAdjFileRight;
	p3DInfo->m_strTargetFile = strTargetFile;

	AddAdjust(strTargetFile);
	HANDLE handle;
	handle = (HANDLE) _beginthread( _Create3DFile, 0, (void*)p3DInfo); // create thread
}

void _Create3DFile(void *param)
{
	Adjust3DInfo* p3DInfo= (Adjust3DInfo*)param;

	CString strAdjFileLeft, strAdjFileRight, strTargetFile;

	CESMImgMgr* pImgMgr = p3DInfo->m_pImgMgr;
	strAdjFileLeft	=	p3DInfo->m_strAdjFile[0];
	strAdjFileRight	=	p3DInfo->m_strAdjFile[1];
	strTargetFile	=	p3DInfo->m_strTargetFile;

	delete p3DInfo;
	p3DInfo = NULL;

	//-- 2013-02-05 hongsu.jung		
	//-- Make Left 50%, Right 50%
	CStringA ansiStrPicLeft(strAdjFileLeft);
	CStringA ansiStrPicRight(strAdjFileRight);
	CvSize sizeMovie = cvSize(FULL_HD_WIDTH, FULL_HD_HEIGHT);

//	IplImage * image1 = cvLoadImage(ansiStrPicLeft);
//	if(!image1)
	{
		pImgMgr->FinishAdjust(strTargetFile);
		return;
	}

//	IplImage * image2 = cvLoadImage(ansiStrPicRight);
//	if(!image2)
	{
		pImgMgr->FinishAdjust(strTargetFile);
//		cvReleaseImage( &image1 );	
		return;
	}

	//-- 2013-02-05 hongsu.jung
	//-- Full HD 1920 X 540 (1080/2)
	CvSize size_half = cvSize(FULL_HD_WIDTH/2, FULL_HD_HEIGHT);
//	IplImage * temp1	= cvCreateImage(size_half	, image1->depth, image1->nChannels);
//	cvResize( image1, temp1 );		

//	IplImage * temp2	= cvCreateImage(size_half	, image2->depth, image2->nChannels);
//	cvResize( image2, temp2 );

//	int  depth =  image1->depth; 
//	int  nChannels = image1->nChannels;
//	cvReleaseImage( &image1 );		
//	cvReleaseImage( &image2 );	

	//-- 3D Image
//	IplImage * temp3D	= cvCreateImage (sizeMovie	, depth, nChannels);

//	cvSetImageROI (temp3D, cvRect(0, 0, temp1->width, temp1->height));
//	cvCopy(temp1, temp3D);
//	cvSetImageROI (temp3D, cvRect(temp1->width, 0 , temp2->width, temp2->height));
//	cvCopy(temp2, temp3D);
//	cvResetImageROI(temp3D);

	//-- Release Image	
//	cvReleaseImage( &temp1 );	
//	cvReleaseImage( &temp2 );

	//-- Save File
	char pchPath[MAX_PATH] = {0};
	wcstombs(pchPath, strTargetFile, MAX_PATH);
	//cvSaveImage(pchPath, temp3D);
//	cvReleaseImage( &temp3D );		
	pImgMgr->FinishAdjust(strTargetFile);
}

int CESMImgMgr::AddBanner2(stImageBuffer* pImageBuffer)
{
	if (!pImageBuffer->bLogo)
		return 0;

	CString strBannerPath;
	int nIndex=0;
	
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;
	for( int i =0 ;i < arDSCList.GetSize(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if (pImageBuffer->strDscName == pItem->GetDeviceDSCID() )
		{
			nIndex = i;
			break;
		}
	}

	strBannerPath.Format(_T("%s\\05.Banner\\%d.png") ,ESMGetPath(ESM_PATH_IMAGE), nIndex, nIndex);
	char pBannerPath[MAX_PATH] = {0};
	wcstombs(pBannerPath, strBannerPath, MAX_PATH);
	if(FileExists(strBannerPath) == FALSE)
		return 0;

//	IplImage* pBanner = cvLoadImage(pBannerPath, cv::IMREAD_UNCHANGED);
//	IplImage* pResult = cvCreateImage(cvSize(pImageBuffer->cvImage->width, pImageBuffer->cvImage->height), pImageBuffer->cvImage->depth, pImageBuffer->cvImage->nChannels);

//	overlayImage2(pImageBuffer->cvImage, pBanner, pResult, cvPoint(0,0));
//	cvCopy(pResult, pImageBuffer->cvImage);
	
	return 0;;
}

int CESMImgMgr::AddBanner(CString strTargetFolder, int nIndex, const vector<int>& vPLogo)
{
	CString strFileName,strBannerPath;
	for(int i =1 ;i <= nIndex; i++)
	{
		int nCamIdx = 0;
		nCamIdx = vPLogo[i-1];
		strFileName.Format(_T("%s\\frame%04d.jpg"), strTargetFolder, i);
		strBannerPath.Format(_T("%s\\05.Banner\\%d.png") ,ESMGetPath(ESM_PATH_IMAGE), nCamIdx, nCamIdx);

		if(FileExists(strBannerPath) == FALSE)
		{
			return 0;
		}

		char pBannerPath[MAX_PATH] = {0};
		char pFilePath[MAX_PATH] = {0};

		wcstombs(pBannerPath, strBannerPath, MAX_PATH);
		wcstombs(pFilePath, strFileName, MAX_PATH);
		
		cv::Mat BannerMat = imread(pBannerPath, -1);
		cv::Mat SourceMat = imread(pFilePath);
		cv::Mat result;

		overlayImage(SourceMat, BannerMat, result, cv::Point(0,0));

		DeleteFiles(strFileName);
		cv::imwrite(pFilePath, result);
	}
	return 0;;
}

void CESMImgMgr::overlayImage(const cv::Mat &SourceMat, const cv::Mat &BannerMat, cv::Mat &output, cv::Point2i location)
{
	SourceMat.copyTo(output);

	// start at the row indicated by location, or at row 0 if location.y is negative.
	for(int y = std::max(location.y , 0); y < SourceMat.rows; ++y)
	{
		int fY = y - location.y; // because of the translation

		// we are done of we have processed all rows of the BannerMat image.
		if(fY >= BannerMat.rows)
			break;

		// start at the column indicated by location, 

		// or at column 0 if location.x is negative.
		for(int x = std::max(location.x, 0); x < SourceMat.cols; ++x)
		{
			int fX = x - location.x; // because of the translation.

			// we are done with this row if the column is outside of the BannerMat image.
			if(fX >= BannerMat.cols)
				break;

			// determine the opacity of the foregrond pixel, using its fourth (alpha) channel.
			double opacity = ((double)BannerMat.data[fY * BannerMat.step + fX * BannerMat.channels() + 3]) / 255.;

			opacity = opacity + 0.3;
			if(opacity > 1)
				opacity = 1.0;
			// and now combine the SourceMat and BannerMat pixel, using the opacity, 

			// but only if opacity > 0.
			for(int c = 0; opacity > 0 && c < output.channels(); ++c)
			{
				unsigned char BannerMatPx =
					BannerMat.data[fY * BannerMat.step + fX * BannerMat.channels() + c];
				unsigned char SourceMatPx =
					SourceMat.data[y * SourceMat.step + x * SourceMat.channels() + c];
				output.data[y*output.step + output.channels()*x + c] =
					SourceMatPx * (1.-opacity) + BannerMatPx * opacity;
			}
		}
	}
}

void CESMImgMgr::overlayImage2(const IplImage *background, const IplImage *foreground, IplImage *output, CvPoint location)
{
	cvCopy(background, output);
	
	// start at the row indicated by location, or at row 0 if location.y is negative.
	for(int y = std::max(location.y , 0); y < background->height; ++y)
	{
		int fY = y - location.y; // because of the translation

		// we are done of we have processed all rows of the foreground image.
		if(fY >= foreground->height)
			break;

		// start at the column indicated by location, 

		// or at column 0 if location.x is negative.
		for(int x = std::max(location.x, 0); x < background->width; ++x)
		{
			int fX = x - location.x; // because of the translation.

			// we are done with this row if the column is outside of the foreground image.
			if(fX >= foreground->width)
				break;

			// determine the opacity of the foregrond pixel, using its fourth (alpha) channel.
			double opacity = ((double)foreground->imageData[fY * foreground->widthStep + fX * foreground->nChannels + 3]) / 255.;
			if ( opacity < 0 )
				opacity = opacity + 1.;

			// and now combine the background and foreground pixel, using the opacity, 

			// but only if opacity > 0.
			for(int c = 0; opacity > 0 && c < output->nChannels; ++c)
			{
				unsigned char foregroundPx =
					foreground->imageData[fY * foreground->widthStep + fX * foreground->nChannels + c];
				unsigned char backgroundPx =
					background->imageData[y * background->widthStep + x * background->nChannels + c];
				output->imageData[y*(output->widthStep) + output->nChannels*x + c] =
					backgroundPx * (1.-opacity) + foregroundPx * opacity;

				double value = backgroundPx * (1.-opacity) + foregroundPx * opacity;
			}

		}
	}
}

void CESMImgMgr::SetMargin(int& nMarginX, int& nMarginY, stAdjustInfo AdjustData )
{
	ESMLog(1, _T("SetMargin X = %d, Y = %d"), nMarginX, nMarginY);
	if ( /*AdjustData.AdjMove.x == 0.000000 || AdjustData.AdjMove.y == 0.000000  ||*/ AdjustData.AdjptRotate.x == 0  || AdjustData.AdjptRotate.x == 0 )
		return;

	int nWidth, nHeight, nNewMarginX, nNewMarginY;
	stFPoint pt1,pt2,pt3,pt4,ptR1,ptR2,ptR3,ptR4;

	nWidth = AdjustData.nWidth;
	nHeight = AdjustData.nHeight;
	double dbAngleAdjust = -1 * (AdjustData.AdjAngle + 90);
	double dbRAngle = dbAngleAdjust*3.141592/180;



	// Get Resize Rect Point
	pt1.x = AdjustData.AdjptRotate.x + (0 - AdjustData.AdjptRotate.x) * AdjustData.AdjSize;
	pt1.y = AdjustData.AdjptRotate.y + (0 - AdjustData.AdjptRotate.y) * AdjustData.AdjSize;

	pt2.x = AdjustData.AdjptRotate.x + (nWidth - AdjustData.AdjptRotate.x) * AdjustData.AdjSize;
	pt2.y = pt1.y;

	pt3.x = pt2.x;
	pt3.y = AdjustData.AdjptRotate.y + (nHeight - AdjustData.AdjptRotate.y) * AdjustData.AdjSize;

	pt4.x = pt1.x;
	pt4.y = pt3.y;

	// Get Rotate Rect Point
	ptR1 = GetRotatePoint(AdjustData.AdjptRotate,pt1, dbRAngle);
	ptR2 = GetRotatePoint(AdjustData.AdjptRotate,pt2, dbRAngle);
	ptR3 = GetRotatePoint(AdjustData.AdjptRotate,pt3, dbRAngle);
	ptR4 = GetRotatePoint(AdjustData.AdjptRotate,pt4, dbRAngle);

	// Get Original Rect Point
	pt1.x = 0;
	pt1.y = 0;
	pt2.x = nWidth;
	pt2.y = pt1.y;
	pt3.x = pt2.x;
	pt3.y = nHeight;
	pt4.x = pt1.x;
	pt4.y = pt3.y;

	double dbMarginX=AdjustData.AdjMove.x, dbMarginY=AdjustData.AdjMove.y;

	// Get Margin
	// p1 ------------ p2
	// |                |
	// |                |
	// |                |
	// p4 ------------ p3

	// p1
	if ( ptR1.x	> pt1.x && ptR1.y > pt1.y)
	{
		dbMarginX = max(ptR1.x + AdjustData.AdjMove.x, dbMarginX);
		dbMarginY = max(ptR1.y + AdjustData.AdjMove.y, dbMarginY);
	}
	else if ( ptR1.x > pt1.x )
		dbMarginX = max(abs( ptR1.x - pt1.x ) - abs(tan(dbRAngle)*( ptR1.y - pt1.y )) + AdjustData.AdjMove.x, dbMarginX );
	else if ( ptR1.y > pt1.y )
		dbMarginY = max(abs( ptR1.y - pt1.y ) - abs(tan(dbRAngle)* ( ptR1.x - pt1.x )) + AdjustData.AdjMove.y, dbMarginY);;

	// p2
	if ( ptR2.x - pt2.x < 0 && ptR2.y  > pt2.y )
	{
		dbMarginX = max( (double)(pt2.x - ptR2.x)  - AdjustData.AdjMove.x, dbMarginX );
		dbMarginY = max( (double) (ptR1.y - pt2.y) + AdjustData.AdjMove.y, dbMarginY );
	}
	else if ( ptR2.x - pt2.x  < 0 )
		dbMarginX = max( (double) abs( ptR2.x - pt2.x )  - abs(tan(dbRAngle)*( ptR2.y - pt2.y ))  - AdjustData.AdjMove.x   , dbMarginX );
	else if ( ptR2.y > pt2.y )
		dbMarginY = max( (double) abs( ptR2.y - pt2.y )   - abs(tan(dbRAngle)*( ptR2.x - pt2.x )) + AdjustData.AdjMove.y , dbMarginY );

	//p3
	if ( ptR3.x - pt3.x < 0 && ptR3.y - pt3.y < 0 )
	{
		dbMarginX = max( (double)(pt3.x - ptR3.x)  - AdjustData.AdjMove.x, dbMarginX );
		dbMarginY = max( (double)(pt3.y - ptR3.y) - AdjustData.AdjMove.y, dbMarginY );
	}
	else if ( ptR3.x - pt3.x < 0 )
		dbMarginX = max(  (double) abs( ptR3.x - pt3.x ) - abs(tan(dbRAngle)*abs( ptR3.y - pt3.y ))  - AdjustData.AdjMove.x , dbMarginX  );
	else if ( ptR3.y - pt3.y < 0 )
		dbMarginY = max( (double) abs( ptR3.y - pt3.y )  - abs(tan(dbRAngle)* ( ptR3.x - pt3.x )) - AdjustData.AdjMove.y, dbMarginY );

	//p4
	if ( ptR4.x	> pt4.x && ptR4.y - pt4.y < 0 )
	{
		dbMarginX = max( (double)ptR4.x  + AdjustData.AdjMove.x, dbMarginX );
		dbMarginY = max( (double)(pt4.y - ptR4.y) - AdjustData.AdjMove.y, dbMarginY );
	}
	else if ( ptR4.x	> pt4.x )
		dbMarginX = max( (double) abs( ptR4.x - pt4.x ) - abs(tan(dbRAngle)*( ptR4.y - pt4.y )) + AdjustData.AdjMove.x , dbMarginX );
	else if ( ptR4.y - pt4.y < 0 )
		dbMarginY = max( (double) abs( ptR4.y - pt4.y )  - abs(tan(dbRAngle)*( ptR4.x - pt4.x ))  - AdjustData.AdjMove.y, dbMarginY );

	if ( dbMarginX > dbMarginY * nWidth / nHeight )
		dbMarginY = dbMarginX * nHeight / nWidth;
	else
		dbMarginX = dbMarginY * nWidth / nHeight;

	nNewMarginX = (int)dbMarginX + 1;
	nNewMarginY = (int)dbMarginY + 1;

	ESMLog(5, _T("Margin Value : X[%d], Y[%d]"), nNewMarginX, nNewMarginY);

	nMarginX = max(nMarginX, nNewMarginX);
	nMarginY = max(nMarginY, nNewMarginY);
}

stFPoint CESMImgMgr::GetRotatePoint(stFPoint ptCenter, stFPoint ptRot, double dbAngle)
{
	stFPoint ptResult;

	// 회전 중심좌표와의 상대좌표
	ptRot.x = ptRot.x - ptCenter.x;
	ptRot.y = -(ptRot.y - ptCenter.y); 

	double cosX = cos(dbAngle);
	double sinX = sin(dbAngle);

	ptResult.x = ptRot.x*cos(dbAngle) - ptRot.y*sin(dbAngle);
	ptResult.y = ptRot.x*sin(dbAngle) + ptRot.y*cos(dbAngle);

	ptResult.x = ptResult.x + ptCenter.x;
	ptResult.y = -(ptResult.y - ptCenter.y);

	return ptResult;
}

CRect CESMImgMgr::SetRectMargin(stAdjustInfo AdjustData )
{
	//static int nCnt = 0;
	//nCnt ++;
	//char strIdx[4];
	//sprintf(strIdx,"%d",nCnt);

	//ESMLog(1, _T("SetNewMargin X = %d, Y = %d"), nMarginX, nMarginY);
	if ( AdjustData.AdjptRotate.x == 0  || AdjustData.AdjptRotate.y == 0 )
	{
		m_vecMargin.push_back(CRect());
		return CRect();
	}

	int nWidth, nHeight, nNewMarginX, nNewMarginY;
	stFPoint pt1,pt2,pt3,pt4,ptR1,ptR2,ptR3,ptR4;

	nWidth = AdjustData.nWidth;
	nHeight = AdjustData.nHeight;
	m_nWidth = nWidth;
	m_nHeight = nHeight;

	double dbAngleAdjust = -1 * (AdjustData.AdjAngle + 90);
	double dbRAngle = dbAngleAdjust*3.141592/180;

	// Get Resize Rect Point
	pt1.x = AdjustData.AdjptRotate.x + (0 - AdjustData.AdjptRotate.x) * AdjustData.AdjSize;
	pt1.y = AdjustData.AdjptRotate.y + (0 - AdjustData.AdjptRotate.y) * AdjustData.AdjSize;

	pt2.x = AdjustData.AdjptRotate.x + (nWidth - AdjustData.AdjptRotate.x) * AdjustData.AdjSize;
	pt2.y = pt1.y;

	pt3.x = pt2.x;
	pt3.y = AdjustData.AdjptRotate.y + (nHeight - AdjustData.AdjptRotate.y) * AdjustData.AdjSize;

	pt4.x = pt1.x;
	pt4.y = pt3.y;

	// Get Rotate Rect Point
	ptR1 = GetRotatePoint(AdjustData.AdjptRotate,pt1, dbRAngle);
	ptR2 = GetRotatePoint(AdjustData.AdjptRotate,pt2, dbRAngle);
	ptR3 = GetRotatePoint(AdjustData.AdjptRotate,pt3, dbRAngle);
	ptR4 = GetRotatePoint(AdjustData.AdjptRotate,pt4, dbRAngle);

	// Get Original Rect Point
	pt1.x = 0;
	pt1.y = 0;
	pt2.x = nWidth;
	pt2.y = pt1.y;
	pt3.x = pt2.x;
	pt3.y = nHeight;
	pt4.x = pt1.x;
	pt4.y = pt3.y;

	double dbMarginX=AdjustData.AdjMove.x, dbMarginY=AdjustData.AdjMove.y;

	// Get Margin
	// p1 ------------ p2
	// |                |
	// |                |
	// |                |
	// p4 ------------ p3

	// p1
	//if( ptR1.x == 0. && ptR1.y == 0. &&
	//	ptR2.x == 0. && ptR2.y == 0. &&
	//	ptR3.x == 0. && ptR3.y == 0. &&
	//	ptR4.x == 0. && ptR4.y == 0.
	//	)
	//	return;

	int nNewMarginLeft = 0;//nMarginX;
	int nNewMarginTop = 0;//nMarginY;
	int nNewMarginRight = nWidth;//nMarginY;
	int nNewMarginBottom = nHeight;//nMarginY;

	if(ptR1.x + AdjustData.AdjMove.x > nNewMarginLeft)
		nNewMarginLeft = ptR1.x + AdjustData.AdjMove.x;
	if(ptR1.y + AdjustData.AdjMove.y > nNewMarginTop)
		nNewMarginTop = ptR1.y + AdjustData.AdjMove.y;

	if(ptR2.x + AdjustData.AdjMove.x < nNewMarginRight)
		nNewMarginRight = ptR2.x + AdjustData.AdjMove.x;
	if(ptR2.y + AdjustData.AdjMove.y > nNewMarginTop)
		nNewMarginTop = ptR2.y + AdjustData.AdjMove.y;

	if(ptR3.x + AdjustData.AdjMove.x < nNewMarginRight)
		nNewMarginRight = ptR3.x + AdjustData.AdjMove.x;
	if(ptR3.y + AdjustData.AdjMove.y < nNewMarginBottom)
		nNewMarginBottom = ptR3.y + AdjustData.AdjMove.y;

	if(ptR4.x + AdjustData.AdjMove.x > nNewMarginLeft)
		nNewMarginLeft = ptR4.x + AdjustData.AdjMove.x;
	if(ptR4.y + AdjustData.AdjMove.y < nNewMarginBottom)
		nNewMarginBottom = ptR4.y + AdjustData.AdjMove.y;

	if( nNewMarginLeft > nNewMarginTop * nWidth / nHeight )
		nNewMarginTop = nNewMarginLeft * nHeight / nWidth;
	else
		nNewMarginLeft = nNewMarginTop * nWidth / nHeight;

	if( nNewMarginRight < nNewMarginBottom * nWidth / nHeight )
		nNewMarginBottom = nNewMarginRight * nHeight / nWidth;
	else
		nNewMarginRight = nNewMarginBottom * nWidth / nHeight;

	ESMLog(5, _T("New Margin LT Value : X[%d], Y[%d]"), nNewMarginLeft, nNewMarginTop);
	ESMLog(5, _T("New Margin RB Value : X[%d], Y[%d]"), nNewMarginRight, nNewMarginBottom);

	//nMarginX = max(nMarginX, nNewMarginX);
	//nMarginY = max(nMarginY, nNewMarginY);

	//wgkim 190425
	//	int nAddSpaceX = nWidth/4.;
	//	int nAddSpaceY = nHeight/4.;
	//	Mat mTestImage(nHeight + nAddSpaceY*2, nWidth + nAddSpaceX*2, CV_8UC3, CV_RGB(255, 255, 255));
	//
	//	line(mTestImage, Point2d(nAddSpaceX+pt1.x, nAddSpaceY+pt1.y), 
	//		Point2d(nAddSpaceX+pt2.x, nAddSpaceY+pt2.y), CV_RGB(0, 0, 255), 5, cv::LINE_AA, 0);
	//	line(mTestImage, Point2d(nAddSpaceX+pt2.x, nAddSpaceY+pt2.y), 
	//		Point2d(nAddSpaceX+pt3.x, nAddSpaceY+pt3.y), CV_RGB(0, 0, 255), 5, cv::LINE_AA, 0);
	//	line(mTestImage, Point2d(nAddSpaceX+pt3.x, nAddSpaceY+pt3.y), 
	//		Point2d(nAddSpaceX+pt4.x, nAddSpaceY+pt4.y), CV_RGB(0, 0, 255), 5, cv::LINE_AA, 0);
	//	line(mTestImage, Point2d(nAddSpaceX+pt4.x, nAddSpaceY+pt4.y), 
	//		Point2d(nAddSpaceX+pt1.x, nAddSpaceY+pt1.y), CV_RGB(0, 0, 255), 5, cv::LINE_AA, 0);
	//
	//	line(mTestImage, Point2d(nAddSpaceX+pt3.x, nAddSpaceY+pt3.y), 
	//		Point2d(nAddSpaceX+pt1.x, nAddSpaceY+pt1.y), CV_RGB(0, 0, 255), 5, cv::LINE_AA, 0);
	//
	//	line(mTestImage, Point2d(nAddSpaceX+ptR1.x + AdjustData.AdjMove.x, nAddSpaceY+ptR1.y + AdjustData.AdjMove.y), 
	//		Point2d(nAddSpaceX+ptR2.x + AdjustData.AdjMove.x, nAddSpaceY+ptR2.y + AdjustData.AdjMove.y), CV_RGB(255, 0, 0), 5, cv::LINE_AA, 0);
	//	line(mTestImage, Point2d(nAddSpaceX+ptR2.x + AdjustData.AdjMove.x, nAddSpaceY+ptR2.y + AdjustData.AdjMove.y), 
	//		Point2d(nAddSpaceX+ptR3.x + AdjustData.AdjMove.x, nAddSpaceY+ptR3.y + AdjustData.AdjMove.y), CV_RGB(255, 0, 0), 5, cv::LINE_AA, 0);
	//	line(mTestImage, Point2d(nAddSpaceX+ptR3.x + AdjustData.AdjMove.x, nAddSpaceY+ptR3.y + AdjustData.AdjMove.y), 
	//		Point2d(nAddSpaceX+ptR4.x + AdjustData.AdjMove.x, nAddSpaceY+ptR4.y + AdjustData.AdjMove.y), CV_RGB(255, 0, 0), 5, cv::LINE_AA, 0);
	//	line(mTestImage, Point2d(nAddSpaceX+ptR4.x + AdjustData.AdjMove.x, nAddSpaceY+ptR4.y + AdjustData.AdjMove.y), 
	//		Point2d(nAddSpaceX+ptR1.x + AdjustData.AdjMove.x, nAddSpaceY+ptR1.y + AdjustData.AdjMove.y), CV_RGB(255, 0, 0), 5, cv::LINE_AA, 0);
	//
	//
	//	int nVirtualLineLength = 1000;
	//	line(mTestImage, 
	//		Point2d(nAddSpaceX+ptR1.x + AdjustData.AdjMove.x, nAddSpaceY+ptR1.y + AdjustData.AdjMove.y), 
	//		Point2d(nAddSpaceX+ptR1.x + nVirtualLineLength + AdjustData.AdjMove.x, nAddSpaceY+ptR1.y + AdjustData.AdjMove.y), CV_RGB(128, 128, 128), 5, cv::LINE_AA, 0);
	//	line(mTestImage, 
	//		Point2d(nAddSpaceX+ptR1.x + AdjustData.AdjMove.x, nAddSpaceY+ptR1.y + AdjustData.AdjMove.y), 
	//		Point2d(nAddSpaceX+ptR1.x + AdjustData.AdjMove.x, nAddSpaceY+ptR1.y + AdjustData.AdjMove.y + nVirtualLineLength), CV_RGB(128, 128, 128), 5, cv::LINE_AA, 0);
	//
	//	line(mTestImage, 
	//		Point2d(nAddSpaceX+ptR2.x + AdjustData.AdjMove.x, nAddSpaceY+ptR2.y + AdjustData.AdjMove.y), 
	//		Point2d(nAddSpaceX+ptR2.x + AdjustData.AdjMove.x - nVirtualLineLength, nAddSpaceY+ptR2.y + AdjustData.AdjMove.y), CV_RGB(128, 128, 128), 5, cv::LINE_AA, 0);
	//	line(mTestImage, 
	//		Point2d(nAddSpaceX+ptR2.x + AdjustData.AdjMove.x, nAddSpaceY+ptR2.y + AdjustData.AdjMove.y), 
	//		Point2d(nAddSpaceX+ptR2.x + AdjustData.AdjMove.x, nAddSpaceY+ptR2.y + AdjustData.AdjMove.y + nVirtualLineLength), CV_RGB(128, 128, 128), 5, cv::LINE_AA, 0);
	//
	//	line(mTestImage, 
	//		Point2d(nAddSpaceX+ptR3.x + AdjustData.AdjMove.x, nAddSpaceY+ptR3.y + AdjustData.AdjMove.y), 
	//		Point2d(nAddSpaceX+ptR3.x + AdjustData.AdjMove.x - nVirtualLineLength, nAddSpaceY+ptR3.y + AdjustData.AdjMove.y), CV_RGB(128, 128, 128), 5, cv::LINE_AA, 0);
	//	line(mTestImage, 
	//		Point2d(nAddSpaceX+ptR3.x + AdjustData.AdjMove.x, nAddSpaceY+ptR3.y + AdjustData.AdjMove.y), 
	//		Point2d(nAddSpaceX+ptR3.x + AdjustData.AdjMove.x, nAddSpaceY+ptR3.y + AdjustData.AdjMove.y - nVirtualLineLength), CV_RGB(128, 128, 128), 5, cv::LINE_AA, 0);
	//
	//	line(mTestImage, 
	//		Point2d(nAddSpaceX+ptR4.x + AdjustData.AdjMove.x, nAddSpaceY+ptR4.y + AdjustData.AdjMove.y), 
	//		Point2d(nAddSpaceX+ptR4.x + AdjustData.AdjMove.x + nVirtualLineLength, nAddSpaceY+ptR4.y + AdjustData.AdjMove.y), CV_RGB(128, 128, 128), 5, cv::LINE_AA, 0);
	//	line(mTestImage, 
	//		Point2d(nAddSpaceX+ptR4.x + AdjustData.AdjMove.x, nAddSpaceY+ptR4.y + AdjustData.AdjMove.y), 
	//		Point2d(nAddSpaceX+ptR4.x + AdjustData.AdjMove.x, nAddSpaceY+ptR4.y + AdjustData.AdjMove.y - nVirtualLineLength), CV_RGB(128, 128, 128), 5, cv::LINE_AA, 0);
	//
	//	//wgkim 190425
	//	line(mTestImage, Point2d(nAddSpaceX + nNewMarginLeft, nAddSpaceY + nNewMarginTop), 
	//		Point2d(nAddSpaceX + nNewMarginRight, nAddSpaceY + nNewMarginTop), CV_RGB(0, 255, 0), 5, cv::LINE_AA);
	//	line(mTestImage, Point2d(nAddSpaceX + nNewMarginRight, nAddSpaceY + nNewMarginTop), 
	//		Point2d(nAddSpaceX + nNewMarginRight, nAddSpaceY + nNewMarginBottom), CV_RGB(0, 255, 0), 5, cv::LINE_AA);
	//	line(mTestImage, Point2d(nAddSpaceX + nNewMarginRight, nAddSpaceY + nNewMarginBottom), 
	//		Point2d(nAddSpaceX + nNewMarginLeft, nAddSpaceY + nNewMarginBottom), CV_RGB(0, 255, 0), 5, cv::LINE_AA);
	//	line(mTestImage, Point2d(nAddSpaceX + nNewMarginLeft, nAddSpaceY + nNewMarginBottom), 
	//		Point2d(nAddSpaceX + nNewMarginLeft, nAddSpaceY + nNewMarginTop), CV_RGB(0, 255, 0), 5, cv::LINE_AA);
	//
	//	circle(mTestImage, Point2d(nAddSpaceX+nNewMarginLeft, nAddSpaceY+nNewMarginTop), 7, CV_RGB(0, 255, 0), 5, cv::LINE_AA);
	//	circle(mTestImage, Point2d(nAddSpaceX+nNewMarginRight, nAddSpaceY+nNewMarginBottom), 7, CV_RGB(0, 255, 0), 5, cv::LINE_AA);
	//
	////	ESMLog(5, _T("Margin Value : X[%d], Y[%d]"), nNewMarginX, nNewMarginY);
	//
	//	//nMarginX = max(nMarginX, nNewMarginLT_X);
	//	//nMarginY = max(nMarginY, nNewMarginLT_Y);
	//
	//	//wgkim 190425
	//	//line(mTestImage, Point2d(nAddSpaceX+nMarginX, nAddSpaceY+nMarginY), 
	//	//	Point2d(nAddSpaceX+nWidth - nMarginX, nAddSpaceY+nMarginY), CV_RGB(0, 0, 0), 5, cv::LINE_AA);
	//	//line(mTestImage, Point2d(nAddSpaceX+nWidth - nMarginX, nAddSpaceY+nMarginY), 
	//	//	Point2d(nAddSpaceX+nWidth - nMarginX, nAddSpaceY+nHeight - nMarginY), CV_RGB(0, 0, 0), 5, cv::LINE_AA);
	//	//line(mTestImage, Point2d(nAddSpaceX+nWidth - nMarginX, nAddSpaceY+nHeight - nMarginY), 
	//	//	Point2d(nAddSpaceX+nMarginX, nAddSpaceY+nHeight - nMarginY), CV_RGB(0, 0, 0), 5, cv::LINE_AA);
	//	//line(mTestImage, Point2d(nAddSpaceX+nMarginX, nAddSpaceY+nHeight - nMarginY), 
	//	//	Point2d(nAddSpaceX+nMarginX, nAddSpaceY+nMarginY), CV_RGB(0, 0, 0), 5, cv::LINE_AA);
	//
	//	//circle(mTestImage, Point2d(nAddSpaceX+nMarginX, nAddSpaceY+nMarginY), 7, CV_RGB(0, 0, 0), 5, 8);
	//	//circle(mTestImage, Point2d(nAddSpaceX+nMarginX, nAddSpaceY+nMarginY), 7, CV_RGB(0, 0, 0), 5, 8);
	//
	//	//putText(mTestImage, strIdx, cv::Point(200, 200), cv::FONT_HERSHEY_PLAIN, 10, cv::Scalar(0, 0, 0), 3, 8);
	//
	//	resize(mTestImage, mTestImage, cv::Size(960+nAddSpaceX*2/2, 540+nAddSpaceY*2/2));
	//	imshow("New Margin TestImage TwoPoint", mTestImage);
	//	waitKey(0);

	CRect rtRect = CRect(nNewMarginLeft, nNewMarginTop, nNewMarginRight, nNewMarginBottom);
	m_vecMargin.push_back(rtRect);
	return rtRect;
}

CRect CESMImgMgr::GetMinimumMargin()
{
	if(m_vecMargin.size() == 0)
		return CRect();

	CRect rtMinRect;

	int nMinTop		= 0;
	int nMinLeft	= 0;

	int nMinBottom	= m_nHeight;
	int nMinRight	= m_nWidth;

	for(int i = 0; i < m_vecMargin.size(); i++)
	{
		if(m_vecMargin[i].left > nMinLeft && m_vecMargin[i].top > nMinTop)
		{
			nMinLeft = m_vecMargin[i].left;
			nMinTop = m_vecMargin[i].top;

			rtMinRect = CRect(nMinLeft, nMinTop, rtMinRect.right, rtMinRect.bottom);
		}

		if(m_vecMargin[i].right < nMinRight && m_vecMargin[i].bottom < nMinBottom)
		{
			if(m_vecMargin[i].right == 0 || m_vecMargin[i].bottom == 0)
				continue;

			nMinRight = m_vecMargin[i].right;
			nMinBottom = m_vecMargin[i].bottom;

			rtMinRect = CRect(rtMinRect.left, rtMinRect.top, nMinRight, nMinBottom);
		}
	}
	return rtMinRect;
}

void CESMImgMgr::ShowMarginLog(int nMinMarginX, int nMinMarginY, CRect rtMargin)
{
	float fLegacyMarginLength = (m_nWidth - (nMinMarginX*2));
	float fNewMarginLength = m_nWidth - abs(rtMargin.left+(m_nWidth-rtMargin.right));

	float fLegacyMarginLengthRatio = fLegacyMarginLength / (float)m_nWidth;
	float fNewMarginLengthRatio = fNewMarginLength / (float)m_nWidth;

	float fLegacyMarginArea= ((m_nWidth - nMinMarginX*2) * (m_nHeight- nMinMarginY*2));
	float fNewMarginArea= (rtMargin.right - rtMargin.left) * (rtMargin.bottom - rtMargin.top);

	float fLegacyMarginAreaRatio = fLegacyMarginArea / ((float)m_nWidth * m_nHeight);
	float fNewMarginAreaRatio = fNewMarginArea / ((float)m_nWidth * m_nHeight);

	CString strLegacyMargin, strNewMargin, strNewMarginRect;
	strLegacyMargin.Format(_T("Legacy Margin Ratio : %f%%, %f%%"), fLegacyMarginLengthRatio*100, fLegacyMarginAreaRatio*100);
	strNewMargin.Format(_T("New Margin Ratio : %f%%, %f%%"), fNewMarginLengthRatio*100, fNewMarginAreaRatio*100);
	strNewMarginRect.Format(_T("New Margin Rect: %d, %d, %d, %d"), 
		(int)rtMargin.left, (int)rtMargin.right, (int)rtMargin.top, (int)rtMargin.bottom);
	ESMLog(5, strLegacyMargin);
	ESMLog(5, strNewMargin);
	ESMLog(5, strNewMarginRect);
}
