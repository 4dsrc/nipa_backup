////////////////////////////////////////////////////////////////////////////////
//
//	ESMImgMgr.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#define CNT_DATE		14
#define CNT_WEEK		3
#define STR_MAX_LEN		255

#define ESM_POSTFAIL_PASS				_T("PASS")
#define ESM_POSTFAIL_STOP				_T("STOP")
#define ESM_POSTFAIL_RETRY				_T("RETRY")
#define ESM_POSTFAIL_FAIL				_T("FAIL")

typedef struct _LINEPOINT
{
	CPoint	start;
	CPoint	end;
}CLine;

typedef struct _EdgeOptions2 {
	UINT            nThreshold;         //Specifies the threshold value for the contrast of the edge.
	UINT            nWidth;             //The number of pixels that the function averages to find the contrast at either side of the edge.
	UINT            nSteepness;         //The span, in pixels, of the slope of the edge projected along the path specified by the input points.
	//InterpolationMethod subpixelType;      //The method for interpolating.
	//unsigned            subpixelDivisions; //The number of samples the function obtains from a pixel.
	UINT			nWishEdges;					// 찾아야 하는 Edge개수
} EdgeOptions2;

// typedef struct _COMPIMAGEINFO
// {
// 	CString			strOraclePath;			// Original image path
// 	CString			strCapturedPath;		// Template image path
// 	Rect			rcRect;
// 	CLine			line;
// 	COLORREF		rgb;
// 	UINT			nScore;					// 기준점수
// 	EdgeOptions2	EdgeOption;				
// 	CString			strSearchText;	
// 	UINT			uiSizeAccuarcy;
// 	UINT			nCompareIndex;		// 현재 비교 중인 Rect의 번호.
// 	PVOID			pSomeThing;
// 	CRect			rcROIRect;
// 	BOOL			bROIRect;
// } COMPIMAGEINFO;

//-- 2011-11-26 hongsu.jung
//-- OCR Check
typedef struct
{
	CString strSequence;
	CString strFilePath;
	SYSTEMTIME tTime;
}OCR_CHECK_INFO;


typedef struct
{
	int top;    //Location of the top edge of the rectangle.
	int left;   //Location of the left edge of the rectangle.
	int height; //Height of the rectangle.
	int width;  //Width of the rectangle.
} Rect_struct; 

