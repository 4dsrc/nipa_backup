////////////////////////////////////////////////////////////////////////////////
//
//	TGImageFunc.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "cv.h"
#include "highgui.h"

extern	CString GetUniqueFileName			(CString strFilePath, CString str=_T(""));
//-- 2010-9-10 keunbae.song
//-- Change NIVision into OpenCV
//extern	void	DisposeProc					(PVOID* object);
extern	CString GetIDFromFile				(CString strPath);
extern  CString GetImageExtension			();
extern	BOOL	CertificationDirecotry		(CString strPath);
extern	UINT	U_GetMenuState				(HMENU hMenu,	UINT uId,	UINT uFlags);
extern	BOOL	EnableMenuProc				(HMENU hMenu,   UINT nSubID, DWORD dwState );
extern	BOOL	U_IsByPosition				(UINT uFlags);
extern	BOOL	DDB2DIB						(HBITMAP hbit, CString strPath);	
extern	HBITMAP	MakeDDBFromDIB				(HDC hdc, CString strPath);
extern  BOOL	ResizeFile					(CDC *pDC, CString strOrgFullName, CString strNewFullName, int nX, int nY, int nWidth, int nHeight);
//-- 2010-9-8 keunbae.song
//-- Not in use.
//extern  BOOL	SaveToJPG					(CString strPath);	
extern  BOOL	GetForegroundWndCapture		(CString strFileFullName, HWND hWnd=NULL);
extern  CString GetEraseSpecialChar			(CString str);
//- 2009-06-24
extern  BOOL	SaveToJPG					(CString strPath, CString strJpgPath, int nWidth = 0, int nHeight = 0);	
//-- 2010-9-8 keunbae.song
//extern  BOOL	GetScreenCapture			(CString strFileFullName);

//-- 2012-07-27 hongsu@esmlab.com 
extern	IplImage* TGResizeImage				(IplImage* pImg, int nWidth, int nHeight);
extern	CvPoint TGScalePoint				(CvPoint pt, float nScaleWidth, float nScaleHeight);
