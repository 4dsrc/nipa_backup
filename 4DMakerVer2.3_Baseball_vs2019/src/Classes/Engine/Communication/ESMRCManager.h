/////////////////////////////////////////////////////////////////////////////
//
//	ESMRCManager.h
//
//
//  ESMLab, LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  ESMLab, LTD. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, LTD.
//
//  Copyright (C) 2012 ESMLab, LTD. All rights reserved.
//
// @author	yongmin.lee (yongmin@esmlab.com)
// @Date	  2013-09-28
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include <map>
#include "ESMTCPSocket.h"
#include "ESMIndexStructure.h"
#include "DSCItem.h"
#include "CommThread.h"
#include "ESMDefine.h"
#include "ESMBackup.h"
#include "ESMAJANetworkDefine.h"

//#include "Globalindex.h"
#define MAXSOCKET	3

typedef struct _RCPHeader
{
	_RCPHeader() {	Init(); }
	void Init()
	{
		memset(protocol, 0, sizeof(char) * 8);
		command = -1;
		memset(Camera, 0, sizeof(char) * 8);
		param1 = -1;
		param2 = -1;
		param3 = -1;
		bodysize = 0;
	}
	char protocol[8]; //always "RCP1.0"
	DWORD command;
	char Camera[8];
	DWORD param1;
	DWORD param2;
	DWORD param3;
	DWORD bodysize;
} RCPHeader, *PRCPHeader;

typedef struct _RCP_CONNECT_INFO
{
	char	szModel[8];
	char	szUniqueID[16];
	DWORD	nStatus;
} RCP_Connect_Info, *PRCP_Connect_Info;

typedef struct _RCP_PROPERTY_INFO
{
	_RCP_PROPERTY_INFO()
	{
		memset(szProp, 0, 24);
	}
	DWORD	nProp;
	char	szProp[24];
} RCP_Property_Info, *pRCP_Property_Info;

typedef struct _RCP_FILESAVE_INFO
{
	char	szFileName[128];
} RCP_FileSave_Info, *pRCP_FileSave_Info;

struct _RCP_ADJUST_INFO
{
	_RCP_ADJUST_INFO()
	{
		AdjAngle = 0.0;
		AdjSize = 0.0;
		nWidth = 0;
		nHeight = 0;
	}
	stFPoint AdjMove, AdjptRotate;
	double AdjAngle, AdjSize;
	int nWidth, nHeight;
	stMarginInfo stMargin;
	CRect rtMargin;
};

enum RCPCommand
{
	RCPCMD_ITEM = 0,
	RCPCMD_STATUS, 
	RCPCMD_CAPTURE,
	RCPCMD_FORMAT,
	RCPCMD_SET_ENLARGE_TOCLIENT,
	RCPCMD_HALF_SHUTTER_TOCLIENT,
	RCPCMD_GETFOCUS_TOCLIENT,
	RCPCMD_GETFOCUS_TOSERVER,
	RCPCMD_SETFOCUS_TOCLIENT,	
	RCPCMD_PROPERTY_GET,
	RCPCMD_PROPERTY_SET,
	RCPCMD_PROPERTY_SET_ALL,
	RCPCMD_LIVEVIEW,
	RCPCMD_TICKCOUNT_GET,
	RCPCMD_TICKCOUNT_SET,
	RCPCMD_PRE_REC_INIT_GH5,
	RCPCMD_PRE_REC_INIT_GH5_RESULT,
	RCPCMD_CAMERA_SYNC,
	RCPCMD_CAMERA_SYNC_RESULT,
	RCPCMD_CAMERA_SYNC_CHECK,
	RCPCMD_MOVIESAVE_CHECK,
	//RCPCMD_TIMESYNC_SLEEP,
	RCPCMD_CONVERT_FINISH,
	RCPCMD_HIDDEN_COMMAND,
	RCPCMD_OPTION_PATH,
	RCPCMD_BACKUP_STRING,
	RCPCMD_DELETE_DSC,
	RCPCMD_OPTION_ETC, RCPCMD_OPTION_BASE,  RCPCMD_OPTION_MOVIESIZE,RCPCMD_OPTION_MAKE_TYPE,
	RCPCMD_CAPTURE_CANCEL,	RCPCMD_MAKEMOVIE_TOCLIENT,
	RCPCMD_SET_ADJUSTINFO, RCPCMD_MAKEMOVIE_FINISH_TOSERVER,
	RCPCMD_SET_MARGIN,
	RCPCMD_USBCONNECTFAIL, 
	RCPCMD_MAKEFRAMEMOVIE_FINISH_TOSERVER,
	RCPCMD_FRAME_LOAD_TIME_TOSERVER,
	RCPCMD_REQUEST_DATA_TOSERVER,
	RCPCMD_REQUEST_DATA_TO4DP,
	RCPCMD_REQUEST_MUX_DATA,//180307 hjcho
	RCPCMD_FWUPDATE,	
	RCPCMD_CAPTURERESUME,
	RCPCMD_SENSORSYNCOUT_TOSERVER, 
	RCPCMD_ERRORFRAME_TOSERVER,
	RCPCMD_ADDFRAME_TOSERVER,
	RCPCMD_SKIPFRAME_TOSERVER,
	RCPCMD_VIEWLARGE, 
	RCPCMD_SET_FOCUS,
	RCPCMD_EXCEPTIONDSC_TOSERVER,
	RCPCMD_CAPTUREDELAY_TOCLIENT,
	RCPCMD_GET_ITEMFOCUS_TOCLIENT,
	RCPCMD_INITMOVIE_TOCLIENT,
	RCPCMD_CAMSHUTDOWN,	
	RCPCMD_LCDON,	
	RCPCMD_LCDOFF,
	RCPCMD_OPTION_COUNTDOWN, 
	RCPCMD_VALIDSKIP_TOSERVER,
	RCPCMD_RECORDFINISH_TOSERVER,
	RCPCMD_CONNECT_CHECK,
	RCPCMD_MOVIEMAKINGSTOP, 
	RCPCMD_COPYTOSERVER_TOSERVER,
	RCPCMD_MAKEFRAMEMOVIE_TOCLIENT, 
	RCPCMD_REQUEST_DATA, 
	RCPCMD_SENSORSYNCTIME_TOSERVER,
	RCPCMD_OPTION_ADJ_PATH,
	RCPCMD_OPTION_MAN_COPY_PATH,
	RCPCMD_OPTION_MAN_DEL_PATH,
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	RCPCMD_GET_FOCUS_ALL,
	RCPCMD_GETFOCUS_TOCLIENT_READ_ONLY,
	RCPCMD_SET_FOCUS_VALUE,
	RCPCMD_FOCUS_LOCATE_SAVE,
	RCPCMD_GET_DISKINFO,
	RCPCMD_RESULT_IMAGE_RECORD_TOSERVER,
	RCPCMD_RESULT_GET_TICK_TOSERVER,
	RCPCMD_ERROR_MSG,
	RCPCMD_ERROR_MSG_GH5,
	RCPCMD_CONNECT_STATUS_CHECK,
	//jhhan 16-09-23
	RCPCMD_SET_FRAME_RECORD,	
	RCPCMD_GET_GPUUSE,
	RCPCMD_GET_STARTSENDFRAME,
	RCPCMD_GET_STOPSENDFRAME,
	RCPCMD_SET_MAKING_INFO,
	RCPCMD_DUMP_PROCDOWN,
	RCPCMD_RUN_IPERF,
	RCPCMD_MUX_SENDDATA,
	RCPCMD_MUX_SENDFINISH,
	RCPCMD_OPTION_SYNC,
	//wgkim 170727
	RCPCMD_OPTION_LIGHT_WEIGHT,
	RCPCMD_OPTION_VIEW_INFO,
	RCPCMD_GET_BACKUP_INFO,
	RCPCMD_USBDEVICE_CHECK,
	RCPCMD_PROPERTY_1STEP,
	RCPCMD_SEND_KTSENDTIME,
	RCPCMD_SEND_KTSTOPTIME,
	//jhhan 171019
	RCPCMD_REMOTE_START,
	RCPCMD_REMOTE_STOP,
	RCPCMD_REMOTE_MAKE,
	//wgkim
	RCPCMD_BACKUP_FILECOUNT_ALL,
	RCPCMD_BACKUP_SET_PATH,	
	RCPCMD_BACKUP_START,
	RCPCMD_BACKUP_END,
	RCPCMD_BACKUP_FILECOUNT_CURRENT,
	RCPCMD_BACKUP_DIRECT_DELETE,
	RCPCMD_OPTION_RECORD_FILE_SPLIT_TOCLIENT,
	RCP_AJA_TEST,
	RCPCMD_RUN_IPERF_OFF,	
	RCPCMD_OPTION_SET_FRAME_RATE,
	RCPCMD_GET_FOCUS_FRAME_TOSERVER,
	RCPCMD_SET_FOCUS_FRAME_TOCLIENT,
	
	RCPCMD_REFEREE_DATA_TO_CLIENT,
	RCPCMD_REFEREE_REQUEST_TO_SERVER,
	RCPCMD_REFEREETRANSCODE_FINISH,
	RCPCMD_REFEREEMUX_FINISH,
	RCPCMD_REFEREE_OPENCLOSE,
	RCPCMD_REFEREE_LIVE,
	RCPCMD_REFEREE_SELECT,
	RCPCMD_REFEREE_SELECT_TO_CLIENT,
	RCPCMD_REFEREE_RECORD,
	RCPCMD_REFEREE_GET_INFO,
	RCPCMD_REFEREE_SET_INFO,
	
	RCPCMD_GET_TICK_CMD_TOCLIENT,
	RCPCMD_GET_TICK_CMD_RESULT_TOSERVER,
	RCPCMD_DELETE_FILE,
	//wgkim 181001
	RCPCMD_SEND_KZONE_POINTS,
	RCPCMD_OPTION_AUTOADJUST_KZONE,
	//jhhan 181126
	RCPCMD_DETECT_INFO,
	RCPCMD_DETECT_RESULT,
	RCPCMD_MULTIVIEW_MAKING,//181221 hjcho
	RCPCMD_MULTIVIEW_FINISH,

	RCPCMD_RTSP_STATE_CHECK,//190226 hjcho
	RCPCMD_SEND_KZONE_JUDGMENT,//wgkim 191217
};


enum RCPMode
{
	RCPMODE_BOTH = 0,
	RCPMODE_EDITER,
	RCPMODE_REMOTE,
};

enum RCPAJA
{
	RCPCMD_AJA_CONNECT = 0,
	RCPCMD_AJA_DATA,
};

enum RCP_REFEREE
{
	RCPCMD_REFEREE_MUX = 0,
	RCPCMD_REFEREE_ORIGIN,
};
class CDSCItem;
class CCommThread;

void _SendThread(void *param);
void _AcceptThread(void *param);
void _FrameDataSendThread(void *param);
void _FrameDataEncodeThread(void *param);
void _KTSendThread(void *param);
void _MovieDataSendThread(void *param);

void _AcceptRemoteThread(void *param);
class CESMRCManager : public CWinThread
{
	DECLARE_DYNCREATE(CESMRCManager)

private:
	CString m_strIP;
	//USHORT  m_nPort;
	CString m_strSendIP;
	CString m_strSendPort;
	//jhhan 16-11-09
	CString m_strSourceIP;
	CString m_strSourceDSC;
	//jhhan	 16-11-15
	BOOL m_bKTConnected;

	int	m_nLocalTime;	
	int m_nAgentTime;

	int	m_nSyncStartTime;	
	int	m_nMinLocalTime;	
	int	m_nMinAgentTime;	

	LARGE_INTEGER m_qwTicksPerSec;
	LARGE_INTEGER m_qwTicksCntSec;
	int		m_nRemoteID;
	HANDLE hHandle;

	//jhhan 16-12-22
	int m_AgentStatus;

	int m_nGapTime;

public:	
	CESMRCManager();
	CESMRCManager(CString strIP, USHORT nPort);
	virtual ~CESMRCManager();

	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run(void);
	bool m_bConnectStatusCheck;
	//BOOL StartServer(USHORT port);
	BOOL StartServer(BOOL bRemote = FALSE);
	BOOL ConnectToAgent(BOOL b4DA = TRUE);	//jhhan 170926

	void SendSocketData(ESMEvent* pMsg);
	void ParsePacket(char* headerbuf, char* bodybuf, int packetsize);
	void ParsePacketServer(char* headerbuf, char* bodybuf, int packetsize);		
	void ParsePacketClient(char* headerbuf, char* bodybuf, int packetsize);
	float OrderFloat(float fVal);

	BOOL Disconnect(BOOL bRflashUI = TRUE);
	void GetReceiveSocket();
	void SetCamConnectStatus(ESMEvent* pMsg);

	void SendMessageESM(UINT pWParam, UINT message, UINT nParam1=0, UINT nParam2=0, UINT nParam3=0, char* pParam=NULL, CString strID = _T(""));
	void SyncTickCount(BOOL bServer);

	void AddMsg(ESMEvent* pMsg, BOOL bReferee = FALSE);
	void RemoveAllMsg();
	void StartSendThread();
	void StartAcceptThread(BOOL bRemote);

	CString GetIP()	{ return m_strIP; }
	void SetIP(CString strIP)	{ m_strIP = strIP; }
	int GetID()	{ return m_nRemoteID; }
	// 	int GetPort()	{ return m_nPort; }
	// 	void SetPort(int nPort)	{ m_nPort = nPort; }

	int	_GetLocalTime()	{ return m_nLocalTime;}
	int GetAgentTime()	{ return m_nAgentTime;}	

	BOOL GetIsConnected()		{ return m_bConnected; }
	void SetConnected(BOOL b)	{ m_bConnected = b;	}
	CESMTCPSocket* GetClientSocket(int nIndex) { return m_pClientSocket[nIndex]; }
	CESMTCPSocket* GetServerSocket() { return m_pServerSocket; }
	//void SetThreadSleep(BOOL bSleep);
	void ConnectCheck(int nFrameRate, int nTime = -1000);	//jhhan 170713
	void GetDiskSize();

	//jhhan 16-11-22
	BOOL m_bNetworkMode;	//T = NetworkEx, F = Network
	void ConnectStatusCheck(BOOL bNetworkEx = FALSE);
	static unsigned WINAPI DoConnectStatusCheckThread(LPVOID param);
	void Wait(DWORD dwMillisecond);

	void SetPcSyncStop(BOOL bPcSyncStop) {m_bPcSyncStop = bPcSyncStop;}
	BOOL GetPcSyncStop(){return m_bPcSyncStop;}

	void StartSendFrame();
	void StopSendFrame();
	BOOL ConnectToKTServer(CString strIP, int nPort);
	void DisConnectToKTServer();

	BOOL SendFrameInfo(vector<MakeFrameInfo>*);
	BOOL SendFrameInfoOpenClose(int nType);
	BOOL SendFrameData(KSSHeader *pstData);

	void SetSendIP(CString strSendIP) {m_strSendIP = strSendIP;}
	CString GetSendIP() {return m_strSendIP;}
	
	void SetSendPort(CString strSendPort) {m_strSendPort = strSendPort;}
	CString GetSendPort() {return m_strSendPort;}
	//jhhan 16-11-09
	void SetSourceIP(CString strSourceIP) {m_strSourceIP = strSourceIP;}
	CString GetSourceIP() {return m_strSourceIP;}
	void SetSourceDSC(CString strSourceDSC) {m_strSourceDSC = strSourceDSC;}
	CString GetSourceDSC() {return m_strSourceDSC;}
	//jhhan 16-11-15
	void SetKTConnected(BOOL b)	{m_bKTConnected = b;}
	BOOL GetIsKTConnected(){return m_bKTConnected;}
	void DumpProcDown();

	void IPerfRun(BOOL bOn = TRUE);

	void SendMakeMuxData(MuxDataInfo* pMuxdata);
	void SendMuxFinish(int nResult, CString strCameraID);
	//hjcho 17-08-17
	void SendFrameInfo(MakeYUVInfo*);
	void SendFrameInfo(CStringArray*);

	//wgkim 171222
	CESMBackup* m_pBackupMgr;
	void BackupAllFileCount();
	void SetBackupMgr(CESMBackup* pBackupMgr);
	void BackupStart(bool bUseFolderList, bool bDeleteBackup);
	void BackupEnd();
	void BackupCurrentFileCount();
	void DeleteSelectList();

	//180719 hjcho
	void SetGroupIdx(int n){m_nGrpIdx = n;}
	int  GetGroupIdx(){return m_nGrpIdx;}

	//181221 hjcho
	void SendMakeMultiViewData(MakeMultiView* pViewData);
	void SetMultiViewStart(BOOL b){m_bMultiViewStart = b;}
	BOOL GetMultiViewStart(){return m_bMultiViewStart;}
private:
	CRITICAL_SECTION _ESMRCMGR;
	HANDLE			m_ThreadHandle;
	HANDLE			m_hAcceptThread[MAXSOCKET];
	CESMTCPSocket*	m_pClientSocket[MAXSOCKET];
	CESMTCPSocket*	m_pServerSocket;
	FrameData*		m_pFrameData;
	BOOL			m_bNetMode; //true : agent   false : server
	BOOL			m_bLiveView;
	BOOL			m_bPcSyncStop;

	//  2013-10-21 Ryumin
public:
	//BOOL m_bAccept;
	//int	 m_nThreadSleep;
	USHORT  m_nBasicPort;		// Both, Recorder
	USHORT  m_nMakingPort;		// Editor
	USHORT  m_nRemotePort;		// To Remote Port
	static unsigned WINAPI SockRecvThread(LPVOID param);
	CString m_strLocalPath;
public:	
	RCP_Connect_Info	*m_pConBody;
	RCP_FileSave_Info   *m_pFileBody;
	CESMArray	m_arMsg;
	CESMArray	m_arReferee;
	//CCommThread	m_pSerial[4];

	CString		m_strProp;
	
	BOOL		m_bPCSyncTime;
	BOOL		m_bConnected;
	BOOL		m_bCheckConnect;
	BOOL		m_bThreadStop;
	int			m_nStatus;
	int			m_nCamConnect;
	int			m_nDiskTotalSize;
	int			m_nDiskFreeSize;
	BOOL		m_bGPUUse;
	BOOL		m_bNetSpeed;
	BOOL		m_bMUXRunning;
	BOOL		m_bIsExcutingBackupProgram;
	HANDLE		m_hBackupProgramProgressHandle;

	int			m_nSyncMax;
	int			m_nSyncMin;
	int			m_nSyncCount;
	int			m_nSyncAvg;
	int			m_nSyncTotal;
	int			m_nSyncFailCnt;

	//wgkim
	int			m_nBackupSrcFileCount;
	int			m_nBackupDstFileCount;
	int			m_nBackupAllFileCount;
	int			m_nBackupCurrentFileCount;
	BOOL		m_bFinish;


	//KT 전송 동기화
	int			m_nKTSendTime;
	int			m_nKTStopCnt;
	CObArray	m_arSendList;

	//180719 hjcho
	int m_nGrpIdx;

	//181011 wgkim
	KZoneDataInfo *m_pKZoneDataInfo;

	BOOL m_bMultiViewStart;
protected:
	DECLARE_MESSAGE_MAP()
public:
	void SetAgentStatus(int nStatus = 0){m_AgentStatus = nStatus;}
	int GetAgentStatus(){return m_AgentStatus;}

	void SetGapTime(int nTime = 0) { m_nGapTime = nTime; }
	int GetGapTime(){ return m_nGapTime; }

	void RequestData(CString strPath);

	BOOL ConnectToProcessor();
	BOOL DisconnectToProcessor(BOOL bRflashUI = TRUE);
	void StartKTSendThread();

	CRITICAL_SECTION m_crRTSend;

	BOOL ConnectToRemote();

	void SendMovieFinish(CString strPath);
	void SendRemote(int nMode, int nData = 0);

	BOOL m_bKTThreadRun;
	void SetKTThreadRun(BOOL b){m_bKTThreadRun = b;}
	BOOL GetKTThreadRun(){return m_bKTThreadRun;}

	BOOL m_bKTThreadStop;
	void SetKTThreadStop(BOOL b){m_bKTThreadStop = b;}
	BOOL GetKTThreadStop(){return m_bKTThreadStop;}

	//180306 hjcho
	BOOL RequestData(CString strMoviePath,CString strSavePath,int nSencCnt);
	BOOL m_bMuxTCP;

	//180508 hjcho
	BOOL m_bAJAUsage;
	BOOL ConnectToAJA(CString strIP,int nPort);
	void SendToAJA(ESMEvent* pMsg);
	void ReceiveFromAJA(char* headerbuf, char* bodybuf, int packetsize);
	void SendRefereeData(ESMEvent* pMsg);
};

struct ThreadSendData{
	ThreadSendData()
	{
		nSocIndex = 0;
		pESMRCManager = NULL;
		pArrFrameInfo = NULL;
	}
	int nSocIndex;
	CESMRCManager * pESMRCManager ;
	vector<MakeFrameInfo>* pArrFrameInfo;
};

struct ThreadSendYUVData{
	ThreadSendYUVData()
	{
		nSocIndex = 0;
		pESMRCManager = NULL;
		pYUVInfo = NULL;
	}
	int nSocIndex;
	CESMRCManager * pESMRCManager ;
	MakeYUVInfo* pYUVInfo;
};
struct ThreadSendStringData{
	ThreadSendStringData()
	{
		nSocIndex = 0;
		pESMRCManager = NULL;
		pstrMovieArr = NULL;
	}
	int nSocIndex;
	CESMRCManager * pESMRCManager ;
	CStringArray  * pstrMovieArr;
};
