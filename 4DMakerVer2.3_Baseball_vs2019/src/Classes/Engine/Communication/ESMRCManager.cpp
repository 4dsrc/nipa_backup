/////////////////////////////////////////////////////////////////////////////
//
//	ESMRCManager.cpp
//
//
//  ESMLab, LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  ESMLab, LTD. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, LTD.
//
//  Copyright (C) 2012 ESMLab, LTD. All rights reserved.
//
// @author	yongmin.lee (yongmin@esmlab.com)
// @Date	  2013-09-28
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMRCManager.h"
#include "ESMUtil.h"
#include "ESMFunc.h"
#include "DSCItem.h"
#include "ESMDefine.h"
#include "FFmpegManager.h"
#include "ESMFileOperation.h"
#include "TimeLineView.h"
//#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <process.h>
#include <MMSystem.h>
#include <windows.h>
#pragma comment(lib, "winmm")

IMPLEMENT_DYNCREATE(CESMRCManager, CWinThread)
BEGIN_MESSAGE_MAP(CESMRCManager, CWinThread)
END_MESSAGE_MAP()

CRITICAL_SECTION _ESMSENDRCMGR;
//----------------------------------------------------------------
//-- 2013-04-22 hongsu.jung
//-- Send Thread Function
//----------------------------------------------------------------
void _SendThread(void *param)
{
	CESMRCManager* pRCM = (CESMRCManager*)param;	
	if (!pRCM) return;
	ESMEvent* pMsg	= NULL;

	TRACE(_T("_SendThread START\r\n"));
	while(!pRCM->m_bThreadStop)
	{
		Sleep(1);
		if (pRCM->m_bConnected)
		{
			int nAll = pRCM->m_arMsg.GetSize();
			while(nAll--)
			{
				//-- yield thread
				//Sleep(pRCM->m_nThreadSleep);
				pMsg = (ESMEvent*)pRCM->m_arMsg.GetAt(0);
				if(!pMsg)
				{
					pRCM->m_arMsg.RemoveAt(0);
					continue;
				}
				if (pRCM->m_bConnected)
					pRCM->SendSocketData(pMsg);

				pRCM->m_arMsg.RemoveAt(0);
			}
			//jhhan referee
			int nReferee = pRCM->m_arReferee.GetSize();
			while(nReferee--)
			{
				pMsg = (ESMEvent*)pRCM->m_arReferee.GetAt(0);
				if(!pMsg)
				{
					pRCM->m_arMsg.RemoveAt(0);
					continue;
				}
				if(pRCM->m_bConnected)
					pRCM->SendRefereeData(pMsg);

				pRCM->m_arReferee.RemoveAt(0);
			}
		}
	}
	TRACE(_T("_SendThread END\r\n"));
	_endthread();
}

void _AcceptThread(void *param)
{
	ThreadSendData* pSendData = (ThreadSendData*)param;
	int nSockIndex = pSendData->nSocIndex;
	CESMRCManager* pRCM = pSendData->pESMRCManager;	
	delete pSendData;

	if (!pRCM) return;
	ESMEvent* pMsg	= NULL;

	int client_sockfd = -1;

	TRACE(_T("_AcceptThread START\r\n"));
	while(!pRCM->m_bThreadStop)
	{
		if( pRCM->GetClientSocket(nSockIndex) == NULL)
		{
			pRCM->SetConnected(FALSE);
			continue;
		}
		client_sockfd = pRCM->GetClientSocket(nSockIndex)->Accept();
		if(client_sockfd < 0)
		{
			if(pRCM->GetClientSocket(nSockIndex) == NULL)
				pRCM->SetConnected(FALSE);
			ESMLog(0,_T("Accept socket[%d] WSAGetLastError[%d])"), client_sockfd, WSAGetLastError());
			continue;
		}
		else
		{

			pRCM->SetConnected(TRUE);

			CString strMode;
			if(nSockIndex  == RCPMODE_BOTH)
				strMode = _T("Both");
			else if(nSockIndex == RCPMODE_EDITER)
				strMode = _T("Editer");
			else
				strMode = _T("Remote");

			ESMLog(3,_T("Accept Agent [%d], %s"), client_sockfd, strMode);



			//pRCM->SendMessageESM(WM_ESM_DSC, WM_RS_RC_NETWORK_CONNECT);
			if(nSockIndex < RCPMODE_REMOTE)
			{
				ESMEvent* pMsg	= new ESMEvent;
				pMsg->message	= WM_RS_RC_NETWORK_CONNECT;	
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_DSC, (LPARAM)pMsg);
			}
			

			ThreadSendData* pThreadSendData = new ThreadSendData;
			pThreadSendData->nSocIndex = nSockIndex;
			pThreadSendData->pESMRCManager = pRCM;
			HANDLE hHandle = (HANDLE) _beginthreadex(NULL, 0, pRCM->SockRecvThread, (void *)pThreadSendData, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	TRACE(_T("_AcceptThread END\r\n"));
	_endthread();
}
//------------------------------------------------------------------------------ 
//! @brief    Constructor
//! @date     2013-09-28
//! @owner    yongmin
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
CESMRCManager::CESMRCManager()
{
	m_pClientSocket[0]	= NULL;
	m_pClientSocket[1]	= NULL;
	m_pClientSocket[2]	= NULL;
	m_pServerSocket	= NULL;
	m_ThreadHandle	= NULL;
	m_bConnected	= FALSE;
	m_bThreadStop	= FALSE;
	//m_bAccept		= FALSE;
	m_bLiveView		= FALSE;
	m_bAutoDelete	= FALSE;
	m_nStatus		= NET_STATUS_UNKNOWN;
	m_nCamConnect	= 0;
	m_nDiskTotalSize= 0;
	m_nDiskFreeSize = 0;
	m_bPCSyncTime	= FALSE;
	m_bCheckConnect = FALSE;
	m_pFrameData	= NULL;	

	m_nBackupSrcFileCount		= 0;
	m_nBackupDstFileCount		= 0;
	m_nBackupAllFileCount		= 0;
	m_nBackupCurrentFileCount	= 0;
	m_bFinish					= FALSE;

	m_hAcceptThread[0]	= NULL;
	m_hAcceptThread[1]	= NULL;
	//m_nThreadSleep	= 1;
	m_bPcSyncStop	= FALSE;
	m_nBasicPort	= 25000;
	m_nMakingPort	= 25001;
	m_nRemotePort	= 25002;
	m_bGPUUse = FALSE;
	m_bNetSpeed = FALSE;
	m_bMUXRunning = FALSE;

	StartSendThread();
	QueryPerformanceFrequency(&m_qwTicksPerSec);
	InitializeCriticalSection (&_ESMRCMGR);
	InitializeCriticalSection (&_ESMSENDRCMGR);

	//jhhan 16-11-22
	m_bNetworkMode = FALSE;
	m_bKTConnected = FALSE;

	m_AgentStatus = 0;
	m_nGapTime = 0;

	m_nSyncMax = 0;
	m_nSyncMin = -1;
	m_nSyncCount = 0;
	m_nSyncAvg = 0;
	m_nSyncTotal = 0;
	m_nSyncFailCnt = 0;
	InitializeCriticalSection(&m_crRTSend);

	m_bKTThreadRun = FALSE;
	m_bKTThreadStop = FALSE;

	m_bIsExcutingBackupProgram = FALSE;
	m_hBackupProgramProgressHandle = NULL;

	m_bMuxTCP = FALSE;
	m_bAJAUsage = FALSE;

	m_pKZoneDataInfo = NULL;
	m_bMultiViewStart = FALSE;
}

CESMRCManager::CESMRCManager(CString strIP, USHORT nPort)
{
	m_pClientSocket[0]	= NULL;
	m_pClientSocket[1]	= NULL;
	m_pClientSocket[2]	= NULL;
	m_pServerSocket	= NULL;
	m_ThreadHandle	= NULL;
	m_bConnected	= FALSE;
	m_bThreadStop	= FALSE;
	//m_bAccept		= FALSE;
	m_bLiveView		= FALSE;
	m_bAutoDelete	= FALSE;
	m_nStatus		= NET_STATUS_UNKNOWN;
	m_nCamConnect	= 0;
	m_nDiskTotalSize= 0;
	m_nDiskFreeSize = 0;
	m_bPCSyncTime	= FALSE;
	m_bCheckConnect = FALSE;
	m_pFrameData	= NULL;

	m_hAcceptThread[0]	= NULL;
	m_hAcceptThread[1]	= NULL;
	//m_nThreadSleep	= 1;
	m_bPcSyncStop	= FALSE;

	//-- 2013-10-08 hongsu@esmlab.com
	//-- Set Information 
	m_strIP = strIP;
	//m_nPort = nPort;
	m_nBasicPort	= 25000;
	m_nMakingPort	= 25001;
	m_nRemotePort	= 25002;
	m_nRemoteID = ESMGetIDfromIP(strIP);
	m_bGPUUse = FALSE;
	m_bNetSpeed = FALSE;
	m_bMUXRunning = FALSE;

	StartSendThread();
	QueryPerformanceFrequency(&m_qwTicksPerSec);
	InitializeCriticalSection (&_ESMRCMGR);
	InitializeCriticalSection (&_ESMSENDRCMGR);

	//jhhan 16-11-22
	m_bNetworkMode = FALSE;
	m_bKTConnected = FALSE;

	m_nSyncMax = 0;
	m_nSyncMin = -1;
	m_nSyncCount = 0;
	m_nSyncAvg = 0;
	m_nSyncTotal = 0;
	m_nSyncFailCnt = 0;

	m_bIsExcutingBackupProgram = FALSE;
	m_hBackupProgramProgressHandle = NULL;	

	m_nBackupSrcFileCount = 0;
	m_nBackupDstFileCount = 0;
	m_nBackupAllFileCount = 0;
	m_nBackupCurrentFileCount = 0;
	m_bFinish = FALSE;
	m_bAJAUsage = FALSE;
	m_bMultiViewStart = FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief    Destructor
//! @date     2013-09-28
//! @owner    yongmin
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
CESMRCManager::~CESMRCManager()
{

	// 10/14/2013 hongsu.jung
	// Stop Thread
	if(m_pFrameData)
	{
		delete[] m_pFrameData->nFrameData;
		delete m_pFrameData;
	}
	RemoveAllMsg();
	Disconnect(FALSE);
	m_bThreadStop = TRUE;
	WaitForSingleObject( m_ThreadHandle,  INFINITE);
	WaitForSingleObject( m_hThread,  INFINITE);
 	DeleteCriticalSection (&_ESMRCMGR);
	DeleteCriticalSection (&_ESMSENDRCMGR);
	//DeleteCriticalSection (&m_crRTSend);

}

//------------------------------------------------------------------------------ 
//! @brief    InitInstance must be overridden to initialize each new instance of 
//!           a user-interface thread.
//! @date     2013-09-28
//! @owner    yongmin
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------
BOOL CESMRCManager::InitInstance()
{
	m_bConnectStatusCheck = FALSE;
	hHandle = NULL;
	//To Do
	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief    Called by the framework from within a rarely overridden Run member 
//!           function to exit this instance of the thread, or if a call to 
//!           InitInstance fails.
//! @date     2013-09-28
//! @owner    yongmin
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMRCManager::ExitInstance()
{
	//To Do

	return CWinThread::ExitInstance();
}

//------------------------------------------------------------------------------ 
//! @brief    Provides a default message loop for user-interface threads.
//! @date     2013-09-28
//! @owner    yongmin
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------
int CESMRCManager::Run()
{
// 	while(!m_bThreadStop) 
// 	{
// 		//-- 2013-11-27 hongsu@esmlab.com
// 		//-- CPU Overwork 
// 		// Sleep(0);
// 		Sleep(m_nThreadSleep);
// 
// 		if(m_bConnected == TRUE)
// 			GetReceiveSocket();
// 	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////
// SERVER
BOOL CESMRCManager::ConnectToAgent(BOOL b4DA/* = TRUE*/)
{
	//connect 시 첫촬영 delay time 적용
	ESMSetFirstConnect(TRUE);

	if(m_pServerSocket)
		Disconnect();

	char szHostIP[50];
	char* strId = ESMUtil::CStringToChar(m_strIP);
	memcpy(szHostIP, strId, sizeof(char) * 50);
	if( strId)
	{
		delete strId;
		strId = NULL;
	}

	//m_pTCPSocket = new CESMTCPSocket(CLIENTMODE);

	int nPort = 0;
	if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
		nPort = m_nMakingPort;
	else		// Both, Recorder
		nPort = m_nBasicPort;

	if(b4DA == FALSE)
	{
		if(ESMGetProcessorShare())
		{
			if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_BOTH)
			{
				nPort = m_nMakingPort;
			}
		}
	}

	EnterCriticalSection(&_ESMRCMGR);
	m_pServerSocket = new CESMTCPSocket;
	m_pServerSocket->Create(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0);
	int ret = m_pServerSocket->Connect(szHostIP, nPort);
	LeaveCriticalSection (&_ESMRCMGR);

	if(ret != 0)
	{
		Disconnect();	
		return FALSE;
	}
	ThreadSendData* pThreadSendData = new ThreadSendData;
	pThreadSendData->nSocIndex = 0;
	pThreadSendData->pESMRCManager = (CESMRCManager*)this;
	m_hAcceptThread[0] = (HANDLE) _beginthreadex(NULL, 0, SockRecvThread, (void *)pThreadSendData, 0, NULL);

	m_bConnected	= TRUE;
	//m_bAccept		= FALSE;
	m_bNetMode	= TRUE;

	//ESMLog(3,_T("[CESMRCManager] Connect"));

	//-- Sync Tick Count
	//-- Get Local Time / Agent Time
	//	SetThreadSleep(FALSE);
	//jhhan 170926
	if(b4DA)
	{
		m_nSyncMin = -1;
		SyncTickCount(TRUE);
	}

	//-- 2013-10-14 hongsu@esmlab.com
	//-- Update NetworkView
	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent();
	pMsg->message = WM_ESM_VIEW_NET_UPDATE_CONNECTION;
	pMsg->nParam1 = m_nRemoteID;
	pMsg->nParam2 = m_bConnected;
	pMsg->nParam3 = m_bKTConnected;
	pMsg->pParam  = (LPARAM)this;
	::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

	//Get Client HD Size
	GetDiskSize();

	//Get ConnectStatusCheck	
	//ConnectStatusCheck();
	return TRUE;
}


BOOL CESMRCManager::Disconnect(BOOL bRflashUI)
{
	EnterCriticalSection(&_ESMRCMGR);
	m_bConnected = FALSE;
	//m_bAccept	 = FALSE;
	if( m_nStatus != NET_STATUS_CONNECTING)
		m_nStatus	 = NET_STATUS_DISCONNECT;

	if(m_bNetMode)
	{
		if(!m_pServerSocket)
		{
			LeaveCriticalSection (&_ESMRCMGR);
			return TRUE;
		}

		m_pServerSocket->Close();
		delete m_pServerSocket;
		m_pServerSocket= NULL;

		ESMLog(5,_T("[CESMRCManager] NetMode[%d] Disconnect"), m_nRemoteID);
		
		if(m_bAJAUsage)
		{
			m_bAJAUsage = FALSE;
			ESMSetConnectAJANetwork(m_bAJAUsage);
		}

		if(ESMGetValue(ESM_VALUE_RTSP))
		{
			if(ESMGetRecordStatus())//촬영 시 비정상 종료
			{
				//서버로 해당 문제 카메라 인덱스 전송
				CObArray dsclist;
				ESMGetDSCList(&dsclist);
				CString strCurIP;
				strCurIP.Format(_T("%d"),m_nRemoteID);
				for(int i = 0 ; i < dsclist.GetCount() ; i++)
				{
					CDSCItem* pItem = (CDSCItem*) dsclist.GetAt(i);
					CString strDSCIP = pItem->GetDSCInfo(DSC_INFO_LOCATION);
					CString strLastIP;
					int nIdx = strDSCIP.ReverseFind(_T('.'));
					strLastIP = strDSCIP.Mid(nIdx+1,strDSCIP.GetLength()-nIdx+1);
					
					if(strCurIP == strLastIP)
					{
						ESMSendRTSPHttpSync(i+1);
						CString strMessage;
						strMessage.Format(_T("[%s/%d] 4DAP Disconnect "),pItem->GetDeviceDSCID(),i+1);
						AfxMessageBox(strMessage);
						break;
					}
				}
			}
		}
		//jhhan 170926
		ESMSetDelete4DPMgr(m_nRemoteID);

		if(m_bNetMode && bRflashUI == TRUE)
		{
			ESMEvent* pMsg = NULL;
			pMsg = new ESMEvent();
			pMsg->message = WM_ESM_VIEW_NET_UPDATE_CONNECTION;
			pMsg->nParam1 = m_nRemoteID;
			pMsg->nParam2 = m_bConnected;
			pMsg->nParam3 = m_bKTConnected;
			pMsg->pParam  = (LPARAM)this;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		}

		RemoveAllMsg();
	}
	else		// 처리 방향 확인.
	{
		for( int i = 0;i < MAXSOCKET; i++)
		{
			if(!m_pClientSocket[i])
			{
				LeaveCriticalSection (&_ESMRCMGR);
				return TRUE;
			}

			m_pClientSocket[i]->Close();
			if (m_hAcceptThread)
			{
				if(WaitForMultipleObjects(MAXSOCKET, m_hAcceptThread, FALSE, 3000))
					//if(WaitForSingleObject( m_hAcceptThread,  3000) != WAIT_OBJECT_0)
				{
					// 			DWORD dwError = 0;
					// 			TerminateThread(m_hAcceptThread[0], dwError);
					// 			CloseHandle(m_hAcceptThread[0]);
				}
			}
			//m_hAcceptThread = NULL;
			delete m_pServerSocket;
			m_pServerSocket= NULL;

			ESMLog(5,_T("[CESMRCManager] NetMode[%d] Disconnect"), m_bNetMode);
			if(m_bAJAUsage)
			{
				m_bAJAUsage = FALSE;
				ESMSetConnectAJANetwork(m_bAJAUsage);
			}
			RemoveAllMsg();
		}

	}

	

	m_bConnectStatusCheck = FALSE;
	TerminateThread(hHandle,0);
	CloseHandle(hHandle);
	LeaveCriticalSection (&_ESMRCMGR);
	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
// CLIENT
//BOOL CESMRCManager::StartServer(USHORT port)
BOOL CESMRCManager::StartServer(BOOL bRemote)
{
	int ret = 0;

	//m_pTCPSocket = new CESMTCPSocket(SERVERMODE);
	USHORT nPort = 0;

	if(bRemote == TRUE)
	{
		nPort = m_nRemotePort;

		if(m_pClientSocket[RCPMODE_REMOTE])
			Disconnect();

		m_pClientSocket[RCPMODE_REMOTE] = new CESMTCPSocket;
		ret = m_pClientSocket[RCPMODE_REMOTE]->CreateServer(AF_INET, SOCK_STREAM, IPPROTO_TCP, nPort);

		//ESMLog(3,_T("[CESMRCManager] StartRemoteServer"));

		if(ret < 0)
		{
			Disconnect();
			return FALSE;
		}
		m_nStatus = NET_STATUS_DISCONNECT;
		m_bConnected	= FALSE;
		//		m_bAccept		= TRUE;
		m_bNetMode	= FALSE;

	}else
	{
		for (int i =0 ;i < MAXSOCKET - 1; i++)
		{
			/*if(i == RCPMODE_REMOTE)
			{
			if(bRemote == FALSE)
			continue;
			else
			{
			ESMLog(3,_T("[CESMRCManager] StartRemoteServer"));
			}
			}*/

			if( i == RCPMODE_BOTH)
				nPort = m_nBasicPort;
			else if( i == RCPMODE_EDITER)
				nPort = m_nMakingPort;
			else
				nPort = m_nRemotePort;

			if(m_pClientSocket[i])
				Disconnect();

			m_pClientSocket[i] = new CESMTCPSocket;
			ret = m_pClientSocket[i]->CreateServer(AF_INET, SOCK_STREAM, IPPROTO_TCP, nPort);

			if(ret < 0)
			{
				Disconnect();
				return FALSE;
			}
			m_nStatus = NET_STATUS_DISCONNECT;
			m_bConnected	= FALSE;
			//		m_bAccept		= TRUE;
			m_bNetMode	= FALSE;
		}
	}

	StartAcceptThread(bRemote);
	ESMLog(3,_T("[CESMRCManager] %s StartServer"), bRemote?_T("Remote"):_T(""));
	return TRUE;
}

unsigned WINAPI CESMRCManager::SockRecvThread(LPVOID param)
{
	ThreadSendData* pThreadSendData = (ThreadSendData*)param;
	int nSockIndex = pThreadSendData->nSocIndex;
	CESMRCManager* pRCManager= pThreadSendData->pESMRCManager;
	delete pThreadSendData;


	char* recvbodybuf = NULL;
	RCPHeader recvheadbuf;
	int nTickTime = 0;
	int recved = 0;
	int nIndex = 0;
	if( pRCManager->m_bNetMode)
	{
		if( pRCManager->m_pServerSocket == NULL)
			return 0;
	}
	else
	{
		if( pRCManager->m_pClientSocket[nSockIndex] == NULL)
			return 0;
	}

	while(1)
	{
		if( pRCManager->m_bThreadStop )
			break;

		if(pRCManager->m_bNetMode)
		{
			if(pRCManager->m_pServerSocket == NULL)
				break;

			recved = pRCManager->m_pServerSocket->Recv((char*)&recvheadbuf, sizeof(RCPHeader));
			if(recved <= 0)
			{
				if(recved != -WSAEWOULDBLOCK  && recved <= 0)
				{
					ESMLog(1, _T("#Recv Disconnect %s"), pRCManager->GetIP());
					pRCManager->Disconnect();
				}
				return 0;
			}
			if(strcmp(recvheadbuf.protocol, "RCP1.0"))
			{
				ESMLog(0, _T("Unknown Protocol"));
				return 0;
			}
			if(recvheadbuf.bodysize)
			{
				DWORD dwBodySize = recvheadbuf.bodysize;
				recvbodybuf = new char[dwBodySize];
				memset(recvbodybuf, 0x0, dwBodySize);
				nIndex = 0;

				while(1)
				{
					recved = 0;
					recved = pRCManager->m_pServerSocket->Recv(&recvbodybuf[nIndex], ((dwBodySize >= 8192) ? 8192 : dwBodySize));
					if(recved <= 0)
					{
						if(recved != -WSAEWOULDBLOCK  && recved < 0)
							pRCManager->Disconnect();
						break;
					}

					if((recved == dwBodySize) || (dwBodySize == 0))
						break;

					nIndex += recved;
					dwBodySize -= recved;
				}

				pRCManager->ParsePacket((char*)&recvheadbuf, recvbodybuf, recvheadbuf.bodysize);
				delete []recvbodybuf;
				recvbodybuf = NULL;
			}else
				pRCManager->ParsePacket((char*)&recvheadbuf, NULL, recvheadbuf.bodysize);
		}
		else
		{
			if(pRCManager->m_pClientSocket[nSockIndex] != NULL)
			{
				recved = pRCManager->m_pClientSocket[nSockIndex]->Recv((char*)&recvheadbuf, sizeof(RCPHeader));
				if(recved <= 0)
				{
					ESMLog(1, _T("#Recv Disconnect"));
					return 0;
				}
				if(strcmp(recvheadbuf.protocol, "RCP1.0"))
				{
					ESMLog(0, _T("Unknown Protocol"));
					continue;
				}

				if(recvheadbuf.bodysize)
				{
					DWORD dwBodySize = recvheadbuf.bodysize;
					recvbodybuf = new char[dwBodySize];
					memset(recvbodybuf, 0x0, dwBodySize);
					nIndex = 0;

					while(1)
					{
						recved = 0;
						recved = pRCManager->m_pClientSocket[nSockIndex]->Recv( &recvbodybuf[nIndex], ((dwBodySize >= 8192) ? 8192 : dwBodySize));
						if(recved < 0)
							break;

						if((recved == dwBodySize) || (dwBodySize == 0))
							break;

						nIndex += recved;
						dwBodySize -= recved;
					}

					pRCManager->ParsePacket((char*)&recvheadbuf, recvbodybuf, recvheadbuf.bodysize);
					delete []recvbodybuf;
					recvbodybuf = NULL;
				}else
					pRCManager->ParsePacket((char*)&recvheadbuf, NULL, recvheadbuf.bodysize);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// SHARE
void CESMRCManager::GetReceiveSocket()
{
// 	char* recvbodybuf = NULL;
// 	RCPHeader recvheadbuf;
// 	int nTickTime = 0;
// 	int recved = 0;
// 	int nIndex = 0;
// 	if( m_pTCPSocket == NULL)
// 	{
// 		return;
// 	}
// 
// 	if(m_bNetMode)
// 	{
// 		recved = m_pTCPSocket->Recv((char*)&recvheadbuf, sizeof(RCPHeader));
// 		if(recved <= 0)
// 		{
// 			if(recved != -WSAEWOULDBLOCK  && recved <= 0)
// 				Disconnect();
// 			return;
// 		}
// 		if(strcmp(recvheadbuf.protocol, "RCP1.0"))
// 		{
// 			ESMLog(0, _T("Unknown Protocol"));
// 			return;
// 		}
// 		if(recvheadbuf.bodysize)
// 		{
// 			DWORD dwBodySize = recvheadbuf.bodysize;
// 			recvbodybuf = new char[dwBodySize];
// 			memset(recvbodybuf, 0x0, dwBodySize);
// 			nIndex = 0;
// 
// 			while(1)
// 			{
// 				recved = 0;
// 				recved = m_pTCPSocket->Recv(&recvbodybuf[nIndex], ((dwBodySize >= 8192) ? 8192 : dwBodySize));
// 				if(recved <= 0)
// 				{
// 					if(recved != -WSAEWOULDBLOCK  && recved < 0)
// 						Disconnect();
// 					break;
// 				}
// 
// 				if((recved == dwBodySize) || (dwBodySize == 0))
// 					break;
// 
// 				nIndex += recved;
// 				dwBodySize -= recved;
// 			}
// 
// 			ParsePacket((char*)&recvheadbuf, recvbodybuf, recvheadbuf.bodysize);
// 			delete []recvbodybuf;
// 			recvbodybuf = NULL;
// 		}else
// 			ParsePacket((char*)&recvheadbuf, NULL, recvheadbuf.bodysize);
// 	}
// 	else
// 	{
// 		for(int nCnt = 0; nCnt < MAXSOCKET; nCnt++)
// 		{
// 			if(INVALID_SOCKET != m_pTCPSocket->GetClientSocket(nCnt))
// 			{
// 				recved = m_pTCPSocket->ASyncRecv(nCnt, (char*)&recvheadbuf, sizeof(RCPHeader));
// 				if(recved <= 0)
// 				{
// 					if(recved < 0)
// 					{
// 						if(m_pTCPSocket->GetClientSocketCount() == 0)
// 						{
// 							m_nStatus = NET_STATUS_DISCONNECT;
// 							m_bConnected = FALSE;
// 							m_bAccept = TRUE;
// 							return;
// 						}
// 					}
// 					else
// 						recved = 0;
// 				}
// 				if(recved <= 0)
// 					continue;
// 
// 				if(strcmp(recvheadbuf.protocol, "RCP1.0"))
// 				{
// 					ESMLog(0, _T("Unknown Protocol"));
// 					continue;
// 				}
// 
// 				if(recvheadbuf.bodysize)
// 				{
// 					DWORD dwBodySize = recvheadbuf.bodysize;
// 					recvbodybuf = new char[dwBodySize];
// 					memset(recvbodybuf, 0x0, dwBodySize);
// 					nIndex = 0;
// 
// 					while(1)
// 					{
// 						recved = 0;
// 						recved = m_pTCPSocket->ASyncRecv(nCnt, &recvbodybuf[nIndex], ((dwBodySize >= 8192) ? 8192 : dwBodySize));
// 						if(recved < 0)
// 							break;
// 
// 						if((recved == dwBodySize) || (dwBodySize == 0))
// 							break;
// 
// 						nIndex += recved;
// 						dwBodySize -= recved;
// 					}
// 
// 					ParsePacket((char*)&recvheadbuf, recvbodybuf, recvheadbuf.bodysize);
// 					delete []recvbodybuf;
// 					recvbodybuf = NULL;
// 				}else
// 					ParsePacket((char*)&recvheadbuf, NULL, recvheadbuf.bodysize);
// 			}
// 		}
// 	}
}

void CESMRCManager::SetCamConnectStatus(ESMEvent* pMsg)
{
	int nIp = pMsg->nParam1;
	int nCamConnectStatus = pMsg->nParam2;
	int nCamConnect = (int)pMsg->pParam;
	CString strIpId;
	AfxExtractSubString(strIpId, m_strIP, 3, '.');
	int nTargetIp = _ttoi(strIpId);
	if( nIp == nTargetIp)
	{
		m_nStatus = NET_STATUS_CONNECT;
		m_nCamConnect = nCamConnect;
	}
}

#include "ptpIndex.h"
void CESMRCManager::SendSocketData(ESMEvent* pMsg)
{
	if(!m_bConnected)
		return;

	if(m_bNetMode)
	{
		if(!m_pServerSocket)
			return;
	}
	else
		if(!m_pClientSocket[0])
			return;

	char* bSendbuf = NULL;
	RCPHeader packetheader;
	char* pBodyData = NULL, *pTpData = NULL;
	CDSCItem* pItem = NULL;
	FrameData* pFrameData = NULL;
	int nTpData1 = 0, nTpData2 = 0; 
	RCP_Property_Info	*pPropBody = NULL;

	if(pMsg->pDest > 0)
		pItem = (CDSCItem*)pMsg->pDest;
	switch(pMsg->message)
	{
	case WM_RS_RC_CAPTURE_IMAGE_REQUIRE:
		{
			if(pMsg->nParam1 == WM_RS_MC_CAPTURE_IMAGE_REQUIRE_5 || pMsg->nParam1 == WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2)
			{
				bSendbuf = new char[sizeof(RCP_FileSave_Info) + sizeof(RCPHeader) ];
				memset(bSendbuf,0x00,sizeof(RCP_FileSave_Info) + sizeof(RCPHeader) );
			}
			else
			{
				bSendbuf = new char[sizeof(RCPHeader) ];
				memset(bSendbuf,0x00,sizeof(RCPHeader) );
			}

			
			m_pFileBody = (RCP_FileSave_Info*)(bSendbuf + sizeof(RCPHeader));
			packetheader.command = RCPCMD_CAPTURE;
			if(pMsg->nParam1 == WM_RS_MC_CAPTURE_IMAGE_REQUIRE_5 || pMsg->nParam1 == WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2)
			{
				CString pstrTemp = ESMGetFrameRecord();
				if(pstrTemp.IsEmpty() != TRUE)
				{
#if 0
					memset(m_pFileBody->szFileName,0x00,128*sizeof(char));
					sprintf(m_pFileBody->szFileName,"%S",pstrTemp);
#else
					char* strId = NULL;
					strId = ESMUtil::CStringToChar(pstrTemp);
					if(strId)
					{
						int nLength = strlen(strId);
						strncpy(m_pFileBody->szFileName,strId,nLength);
						//memcpy(m_pFileBody->szFileName,strId,sizeof(char)*nLength);
						//m_pFileBody->szFileName[nLength] = '\0';

						/*if(ESMMemcpy(m_pFileBody->szFileName,strId,sizeof(char)*128) == FALSE)
						{
							ESMLog(5,_T("[WM_RS_RC_CAPTURE_IMAGE_REQUIRE] memcpy error"));
						}*/

						//memcpy(m_pFileBody->szFileName, strId, sizeof(char) * 128);
						
						//joonho.kim 19.01.25 임시주석
						/*
						if(strId)
						{
							delete strId;
							strId = NULL;
						}*/
					}
#endif					
				}
				packetheader.bodysize = sizeof(RCP_FileSave_Info);
			}
		}
		break;
	case WM_RS_RC_CAPTURE_CANCEL:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_CAPTURE_CANCEL;
		}
		break;
	case WM_ESM_LIST_FORMAT_ALL:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_FORMAT;	
		}		
		break;
	//-- 2014-9-4 hongsu@esmlab.com
	//-- F/W Update
	case WM_ESM_LIST_FW_UPDATE:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_FWUPDATE;	
		}		
		break;
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	case WM_ESM_LIST_PROPERTY_FOCUS_ALL:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_GET_FOCUS_ALL;	
		}		
		break;

	case WM_ESM_LIST_MOVIEMAKINGSTOP:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_MOVIEMAKINGSTOP;	
		}		
		break;
	case WM_ESM_LIST_CAPTURERESUME:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_CAPTURERESUME;	
		}		
		break;
	case WM_ESM_LIST_CAMSHUTDOWN:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_CAMSHUTDOWN;	
		}		
		break;
	case WM_ESM_LIST_INITMOVIEFILE:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_INITMOVIE_TOCLIENT;	
		}		
		break;
	case WM_ESM_LIST_VIEWLARGE:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_VIEWLARGE;
		}			
		break;
	case WM_RS_RC_STATUS_CHANGE:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_STATUS;
		}
		break;
	case WM_RS_RC_PRE_REC_INIT_GH5_RESULT:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_PRE_REC_INIT_GH5_RESULT;
			
		}
		break;
	case WM_RS_RC_NETWORK_CONNECT:	
		{
			bSendbuf = new char[sizeof(RCPHeader) + sizeof(RCP_Connect_Info) ];
			m_pConBody = (RCP_Connect_Info*)&bSendbuf[sizeof(RCPHeader)];
			char* strData = ESMUtil::CStringToChar(pItem->GetDeviceModel());
			memcpy(m_pConBody->szModel, strData, sizeof(char) * 8);
			if( strData)
			{
				delete strData;
				strData = NULL;
			}

			strData = ESMUtil::CStringToChar(pItem->GetDeviceUniqueID());
			memcpy(m_pConBody->szUniqueID, strData, sizeof(char) * 16);
			if( strData)
			{
				delete strData;
				strData = NULL;
			}
			m_pConBody->nStatus = pItem->GetDSCStatus();

			packetheader.command = RCPCMD_ITEM;
			packetheader.bodysize = sizeof(RCP_Connect_Info);
			break;
		}
	case WM_ESM_LIST_CHECK:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_ITEM;
		}
		break;
	case WM_ESM_LIST_GET_PROPERTY:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1 = pMsg->nParam1;
			packetheader.command = RCPCMD_PROPERTY_GET;
		}
		break;
	case WM_ESM_LIST_SET_PROPERTY_EX:
		{
			TRACE(_T("### Command [%x] Type [%x]\n"), pMsg->nParam1, pMsg->nParam2);
			bSendbuf = new char[sizeof(RCPHeader) + sizeof(RCP_Property_Info) ];
			pPropBody = (RCP_Property_Info*)&bSendbuf[sizeof(RCPHeader)];
			packetheader.param1 = pMsg->nParam1;
			packetheader.param2 = pMsg->nParam2;
			pPropBody->nProp = pMsg->pParam;
			packetheader.bodysize = sizeof(RCP_Property_Info);
			packetheader.command = RCPCMD_PROPERTY_SET;
		}
		break;
	case WM_ESM_LIST_SET_PROPERTY:
		{
			bSendbuf = new char[sizeof(RCPHeader) + sizeof(RCP_Property_Info) ];
			pPropBody = (RCP_Property_Info*)&bSendbuf[sizeof(RCPHeader)];
			packetheader.param1 = pMsg->nParam1;
			packetheader.param2 = pMsg->nParam2;
			if (pMsg->nParam2 == PTP_VALUE_STRING)
			{
				if(pMsg->nParam1 == 0x5003)
				{
					TRACE(_T(""));
				}
				CString pstrTemp;// = (TCHAR*)pMsg->pParam;
				TCHAR* pstrTpData = (TCHAR*)pMsg->pParam;
				pstrTemp.Format(_T("%s"), (TCHAR*)pMsg->pParam);
				//ESMLog(1, _T("############# %s"), pstrTemp);

				char* strData = ESMUtil::CStringToChar(pstrTemp);
				memcpy(pPropBody->szProp, strData, sizeof(char) * 24);
				if( strData)
				{
					delete strData;
					strData = NULL;
				}
				pstrTpData = NULL;
			}
			// kcd NX 1000 전송 수정
			else if(pMsg->nParam2 == PTP_VALUE_MODE)
			{
				pMsg->nParam2 = (int)pMsg->pParam;
			}
			else
				pPropBody->nProp = pMsg->pParam;

			packetheader.bodysize = sizeof(RCP_Property_Info);
			packetheader.command = RCPCMD_PROPERTY_SET;
		}
		break;
	case WM_ESM_LIST_GETITEMFOCUS:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1 = pMsg->nParam1;
			packetheader.command = RCPCMD_GET_ITEMFOCUS_TOCLIENT;	
		}
		break;
	case WM_ESM_LIST_SET_GROUP_PROPERTY:
		{
			bSendbuf = new char[sizeof(RCPHeader) + sizeof(RCP_Property_Info) ];
			pPropBody = (RCP_Property_Info*)&bSendbuf[sizeof(RCPHeader)];
			packetheader.param1 = pMsg->nParam1;
			packetheader.param2 = pMsg->nParam2;
			if (pMsg->nParam2 == PTP_VALUE_STRING)
			{
				CString pstrTemp = (TCHAR*)pMsg->pParam;
				pstrTemp.Format(_T("%s"), (TCHAR*)pMsg->pParam);

				char* strData = ESMUtil::CStringToChar(pstrTemp);
				memcpy(pPropBody->szProp, strData, sizeof(char) * 24);
				if( strData)
				{
					delete strData;
					strData = NULL;
				}

			}else
				pPropBody->nProp = pMsg->pParam;

			packetheader.bodysize = sizeof(RCP_Property_Info);
			packetheader.command = RCPCMD_PROPERTY_SET_ALL;
		}
		break;
	case WM_ESM_LIST_PROPERTY_RESULT:
		{
			IDeviceInfo			*pDeviceInfo = NULL;
			RCP_Property_Info* pPropBody = NULL;
			pDeviceInfo = (IDeviceInfo*)pMsg->pParam;
			if(!pDeviceInfo)
			{
				if(pMsg)
				{
					delete pMsg;
					pMsg = NULL;
				}
				return;
			}

			int nAll = 0;
			pMsg->nParam1 = pDeviceInfo->GetPropCode();
			if (pMsg->nParam1 == PTP_CODE_IMAGESIZE)
			{
				CStringArray* pArList = (CStringArray*)pDeviceInfo->GetPropValueListStr();
				CString strSize; 
				nAll = (int)pArList->GetCount();
				bSendbuf = new char[sizeof(RCPHeader) + sizeof(RCP_Property_Info)  * nAll];
				packetheader.bodysize = sizeof(RCP_Property_Info) * nAll;
				pBodyData = bSendbuf + sizeof(RCPHeader);
				for(int i=0 ; i<nAll ; i++ )
				{
					strSize = pArList->GetAt(i);
					TCHAR* strTpData = (TCHAR*)(LPCTSTR)strSize;
					pPropBody = (pRCP_Property_Info)pBodyData;
					memcpy(pPropBody->szProp, strTpData, strSize.GetLength() * 2 + 2);

					if(pArList->GetAt(i) == pDeviceInfo->GetCurrentStr())
					{
						pPropBody->nProp = 1;
					}
					pBodyData += sizeof(RCP_Property_Info);
					pMsg->nParam2 = nAll;
				}
			}
			else
			{
				CUIntArray* pArList = (CUIntArray*)pDeviceInfo->GetPropValueListEnum();
				nAll = (int)pArList->GetCount();

				if(nAll)
				{
					bSendbuf = new char[sizeof(RCPHeader) + sizeof(RCP_Property_Info)  * nAll];
					for(int i=0 ; i<nAll ; i++ )
					{
						pPropBody = (RCP_Property_Info*)&bSendbuf[sizeof(RCPHeader)+packetheader.bodysize];
						pPropBody->nProp = pArList->GetAt(i);
						packetheader.bodysize += sizeof(RCP_Property_Info);

						if(pArList->GetAt(i) == pDeviceInfo->GetCurrentEnum())
						{
							TRACE(_T("[SendProperty] [Type:%d]  [Value:%d]\n"),pMsg->nParam1,pPropBody->nProp);
							pPropBody->szProp[0] = 0x01;
						}
						else
							pPropBody->szProp[0] = 0x00;
					}
				}else
				{
					bSendbuf = new char[sizeof(RCPHeader) + sizeof(RCP_Property_Info)];
					pPropBody = (RCP_Property_Info*)&bSendbuf[sizeof(RCPHeader)+packetheader.bodysize];
					pPropBody->nProp = pDeviceInfo->GetCurrentEnum();
					pPropBody->szProp[0] = 0x01;
					packetheader.bodysize += sizeof(RCP_Property_Info);
					nAll = 1;
				}

				pMsg->nParam2 = nAll;
			}

			packetheader.command = RCPCMD_PROPERTY_GET;
		}
		break;
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	case WM_ESM_LIST_GETFOCUS_TOCLIENT_READ_ONLY:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_GETFOCUS_TOCLIENT_READ_ONLY;		
		}
		break;
	case WM_ESM_LIST_GETFOCUS_TOCLIENT:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_GETFOCUS_TOCLIENT;		
		}
		break;
	case WM_RS_RC_USBCONNECTFAIL:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_USBCONNECTFAIL;		
		}		
		break;
	case WM_ESM_LIST_GET_FOCUS_FRAME_TOSERVER:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_GET_FOCUS_FRAME_TOSERVER;	
		}
		break;	
	case WM_ESM_LIST_GETFOCUS_TOSERVER:
		{
			//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
			bSendbuf = new char[sizeof(RCPHeader) + sizeof(FocusPosition) + sizeof(int)];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			memcpy(pBodyData, (void *)pMsg->pParam, sizeof(FocusPosition));
			memcpy(pBodyData+sizeof(FocusPosition), &pMsg->nParam3, sizeof(int));
			packetheader.bodysize = sizeof(FocusPosition)+sizeof(int);
			packetheader.command = RCPCMD_GETFOCUS_TOSERVER;		
		}		
		break;
	case WM_ESM_LIST_SET_ENLARGE_TOCLIENT:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_SET_ENLARGE_TOCLIENT;		
		}
		break;
	case WM_ESM_LIST_HALF_SHUTTER_TOCLIENT:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_HALF_SHUTTER_TOCLIENT;		
		}
		break;
	case WM_ESM_LIST_SETFOCUS_TOCLIENT:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_SETFOCUS_TOCLIENT;		
		}
		break;
	case WM_ESM_LIST_HIDDEN_COMMAND:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1 = pMsg->nParam1;
			packetheader.param2 = pMsg->nParam2;
			packetheader.command = RCPCMD_HIDDEN_COMMAND;
		}
		break;
	case WM_ESM_LIST_SET_FOCUS:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1 = pMsg->nParam1;
			packetheader.param2 = pMsg->nParam2;

			packetheader.command = RCPCMD_SET_FOCUS;
		}
		break;
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	case WM_ESM_LIST_SET_FOCUS_VALUE:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1 = pMsg->nParam1;
			packetheader.param2 = pMsg->nParam2;

			packetheader.command = RCPCMD_SET_FOCUS_VALUE;
		}
		break;
	case WM_ESM_LIST_FOCUS_LOCATE_SAVE_TOCLIENT:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_FOCUS_LOCATE_SAVE;	
		}
		break;
	case WM_ESM_LIST_SET_FOCUS_FRAME_TOCLIENT:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_SET_FOCUS_FRAME_TOCLIENT;	
		}
		break;
	case WM_RS_RC_PRE_REC_INIT_GH5:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			packetheader.command = RCPCMD_PRE_REC_INIT_GH5;
		}
		break;
	case WM_RS_RC_CAMERA_SYNC:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			packetheader.command = RCPCMD_CAMERA_SYNC;
		}
		break;
	case WM_RS_RC_CAMERA_SYNC_RESULT:
		{
			nTpData1 = (sizeof(UINT));
			packetheader.bodysize = nTpData1;
			bSendbuf = new char[ sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);

			memcpy(pBodyData,&pMsg->nParam1, nTpData1);
			packetheader.command = RCPCMD_CAMERA_SYNC_RESULT;
		}
		break;
	case WM_RS_RC_GET_TICK_CMD:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			packetheader.command = RCPCMD_GET_TICK_CMD_TOCLIENT;
		}
		break;
	case WM_RS_RC_GET_TICK_CMD_RESULT:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1 = pMsg->nParam1;
			packetheader.param2 = pMsg->nParam2;
			packetheader.param3 = pMsg->nParam3;
			packetheader.command = RCPCMD_GET_TICK_CMD_RESULT_TOSERVER;
		}
		break;
	case WM_RS_RC_CAMERA_SYNC_CHECK:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1 = pMsg->nParam1;
			packetheader.param2 = pMsg->nParam2;
			packetheader.param3 = pMsg->nParam3;
			packetheader.command = RCPCMD_CAMERA_SYNC_CHECK;
		}
		break;
	case WM_RS_RC_NETWORK_STATUS:	
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			SetCamConnectStatus(pMsg);
		}
		break;
	case WM_RS_RC_MOVIESAVE_CHECK:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_MOVIESAVE_CHECK;
		}
		break;
	case WM_ESM_FRAME_CONVERT_FINISH:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_CONVERT_FINISH;
		}
		break;
	case WM_ESM_NET_OPTION_ADJ_TARGET_PATH:
		{
			CString* strPathData;
			strPathData = (CString*)pMsg->pParam;
			packetheader.bodysize = strPathData->GetLength();
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char *buffer;
			buffer = new char[packetheader.bodysize + 1];
			strcpy(buffer,CT2A(*strPathData));
			memcpy(pBodyData, buffer, packetheader.bodysize);
			delete [] buffer;
			packetheader.command = RCPCMD_OPTION_ADJ_PATH;
		}
		break;
	case WM_ESM_NET_OPTION_MAN_COPY_PATH:
		{
			COPYINFO* strPathData;
			strPathData = (COPYINFO*)pMsg->pParam;
			packetheader.bodysize = sizeof(COPYINFO);
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);			
			memcpy(pBodyData, strPathData, packetheader.bodysize);			

			packetheader.command = RCPCMD_OPTION_MAN_COPY_PATH;
		}
		break;

	case WM_ESM_NET_OPTION_MAN_DEL_PACH:
		{
			COPYINFO* strPathData;
			strPathData = (COPYINFO*)pMsg->pParam;
			packetheader.bodysize = sizeof(COPYINFO);
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);			
			memcpy(pBodyData, strPathData, packetheader.bodysize);			

			packetheader.command = RCPCMD_OPTION_MAN_DEL_PATH;
		}
		break;
	case WM_ESM_NET_OPTION_PATH :
		{
			CString* strPathData;
			strPathData = (CString*)pMsg->pParam;
			packetheader.bodysize = strPathData->GetLength();
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char *buffer;
			buffer = new char[packetheader.bodysize + 1];
			strcpy(buffer,CT2A(*strPathData));
			memcpy(pBodyData, buffer, packetheader.bodysize);
			delete [] buffer;
			packetheader.command = RCPCMD_OPTION_PATH;
		}
		break;
	case WM_ESM_NET_BACKUP_STRING:
		{
			CString* strPathData;
			strPathData = (CString*)pMsg->pParam;
			packetheader.bodysize = strPathData->GetLength();
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char *buffer;
			buffer = new char[packetheader.bodysize + 1];
			strcpy(buffer,CT2A(*strPathData));
			memcpy(pBodyData, buffer, packetheader.bodysize);
			delete [] buffer;
			packetheader.command = RCPCMD_BACKUP_STRING;
		}
		break;
	case WM_ESM_NET_DELETE_DSC:
		{
			CString* strPathData;
			strPathData = (CString*)pMsg->pParam;
			packetheader.bodysize = strPathData->GetLength();
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char *buffer;
			buffer = new char[packetheader.bodysize + 1];
			strcpy(buffer,CT2A(*strPathData));
			memcpy(pBodyData, buffer, packetheader.bodysize);
			delete [] buffer;
			packetheader.command = RCPCMD_DELETE_DSC;
		}
		break;
	case WM_ESM_NET_OPTION_ETC	:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;
			packetheader.param3  = pMsg->nParam3;
			packetheader.command = RCPCMD_OPTION_ETC;
		}
		break;
	case WM_ESM_NET_OPTION_SYNC:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;
			packetheader.param3	 = pMsg->nParam3;
			packetheader.command = RCPCMD_OPTION_SYNC;
		}
		break;
	case WM_ESM_NET_OPTION_BASE :
		{
			CString* strPathData;
			strPathData = (CString*)pMsg->pParam;
			packetheader.bodysize = strPathData->GetLength();
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char *buffer;
			buffer = new char[packetheader.bodysize + 1];
			strcpy(buffer,CT2A(*strPathData));
			memcpy(pBodyData, buffer, packetheader.bodysize);
			delete [] buffer;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;
			packetheader.param3	 = pMsg->nParam3;
			packetheader.command = RCPCMD_OPTION_BASE;
		}
		break;
	case WM_ESM_NET_OPTION_MAKING_TYPE:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;
			packetheader.param3	 = pMsg->nParam3;
			packetheader.command = RCPCMD_OPTION_MAKE_TYPE;
		}
		break;
	case WM_ESM_NET_OPTION_MOVIESIZE :
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;
			packetheader.param3	 = pMsg->nParam3;
			packetheader.command = RCPCMD_OPTION_MOVIESIZE;
		}
		break;
	
	//wgkim 170727
	case WM_ESM_NET_OPTION_LIGHT_WEIGHT:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;
			packetheader.param3  = pMsg->nParam3;
			packetheader.command = RCPCMD_OPTION_LIGHT_WEIGHT;
		}
		break;
	//wgkim 170727
	case WM_ESM_NET_OPTION_SET_FRAME_RATE:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1	 = pMsg->nParam1;
			packetheader.command = RCPCMD_OPTION_SET_FRAME_RATE;
		}
		break;
	case WM_ESM_NET_OPTION_VIEW_INFO:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;
			packetheader.param3	 = pMsg->nParam3;
			packetheader.command = RCPCMD_OPTION_VIEW_INFO;
		}
		break;
		//wgkim 181005
	case WM_ESM_NET_OPTION_AUTOADJUST_KZONE:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1	 = pMsg->nParam1;
			packetheader.command = RCPCMD_OPTION_AUTOADJUST_KZONE;
		}
		break;
#ifdef _4DMODEL
		//-- 2015-02-15 cygil@esmlab.com
		//-- Add picture coutdown info 전송
	case WM_ESM_NET_OPTION_COUNTDOWN :
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1	 = pMsg->nParam1;
			packetheader.command = RCPCMD_OPTION_COUNTDOWN;
		}
		break;
#endif
	case WM_RS_RC_SET_MAKING_INFO:
		{
			making_info* pMakingInfo;
			pMakingInfo = (making_info*)pMsg->pParam;
			packetheader.bodysize = sizeof(making_info);
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);			
			memcpy(pBodyData, pMakingInfo, packetheader.bodysize);			

			packetheader.command = RCPCMD_SET_MAKING_INFO;
		}
		break;
	case WM_RS_RC_LIVEVIEW:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.param1 = pMsg->nParam1;
			m_bLiveView = pMsg->nParam1;
			packetheader.command = RCPCMD_LIVEVIEW;
		}		
		break;
	case WM_ESM_NET_LIVEVIEW_RESULT:
		{
			packetheader.command = RCPCMD_LIVEVIEW;
			pTpData = (char*)pMsg->pParam;
			pFrameData = (FrameData*)pTpData;
			nTpData1 = (sizeof(UINT) *2); // Image Size Infor
			nTpData2 = pFrameData->nHeight * pFrameData->nWidth * 3; // Image Size
			packetheader.bodysize = nTpData1 +  nTpData2;

			bSendbuf = new char[ sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			memcpy(pBodyData,pTpData, nTpData1);
			memcpy(pBodyData + nTpData1, pTpData + nTpData1, nTpData2);
			delete[] pTpData;
		}
		break;
	case WM_ESM_MOVIE_SET_ADJUSTINFO:
		{
			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);

			// Camera numbers
			UINT nSize = pMsg->nParam1;
			int nSizeAdj = sizeof(_RCP_ADJUST_INFO);
			int nSizeInt = sizeof(UINT);

			packetheader.command = RCPCMD_SET_ADJUSTINFO;

			// body -> Camera Number + ( camera name length + camera name + adjustinfo ) * num
			packetheader.bodysize = sizeof(UINT) + nSizeAdj * nSize + nSizeInt * nSize;
			CString strDSC;
			for ( int i=0 ; i<nSize ; i++ )
			{
				pItem = (CDSCItem*)arDSCList.GetAt(i);
				strDSC =  pItem->GetDeviceDSCID();
				UINT nSizeDsc = strDSC.GetLength();
				packetheader.bodysize = packetheader.bodysize + nSizeDsc;
			}
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize ];
			pBodyData = bSendbuf + sizeof(RCPHeader);

			memcpy(pBodyData, &nSize, nSizeInt );

			pBodyData = pBodyData + nSizeInt;

			//// [length of cam name][cam name][adjInfo] + [length of cam name][cam name][adjInfo]...
			for ( int i=0 ; i<nSize ; i++ )
			{
				pItem = (CDSCItem*)arDSCList.GetAt(i);

				_RCP_ADJUST_INFO adjInfo;
				adjInfo.AdjAngle = pItem->GetDSCAdj(DSC_ADJ_A);
				adjInfo.AdjSize = pItem->GetDSCAdj(DSC_ADJ_SCALE);
				adjInfo.AdjptRotate.x = pItem->GetDSCAdj(DSC_ADJ_RX);
				adjInfo.AdjptRotate.y = pItem->GetDSCAdj(DSC_ADJ_RY);
				adjInfo.AdjMove.x = pItem->GetDSCAdj(DSC_ADJ_X);
				adjInfo.AdjMove.y = pItem->GetDSCAdj(DSC_ADJ_Y);
				adjInfo.nWidth = pItem->GetWidth();
				adjInfo.nHeight = pItem->GetHeight();
				adjInfo.stMargin = pItem->GetMargin();
				adjInfo.rtMargin = pItem->GetMarginRect();

				CString strDSC =  pItem->GetDeviceDSCID();
				UINT nSizeDsc = strDSC.GetLength();

				char *cDSC = new char[nSizeDsc + 1];
				strcpy(cDSC,CT2A(strDSC));

				//length of cam name
				memcpy(pBodyData, &nSizeDsc, nSizeInt);
				int ntest;
				memcpy(&ntest, pBodyData ,sizeof(int) );
				pBodyData = pBodyData + nSizeInt;
				

				// came name
				memcpy(pBodyData, cDSC, nSizeDsc);
				char svName[5];
				memcpy(&svName, pBodyData ,nSizeDsc );
				pBodyData = pBodyData + nSizeDsc;

				// adjInfo
				memcpy(pBodyData, &adjInfo, nSizeAdj);
				pBodyData = pBodyData + nSizeAdj;

				delete[] cDSC;
			}			
		}
		break;
	case WM_ESM_MOVIE_SET_MARGIN:
		{
			packetheader.command = RCPCMD_SET_MARGIN;

			int nMarginX = pMsg->nParam1;
			int nMarginY = pMsg->nParam2;
			packetheader.bodysize = sizeof(int) * 2;
			bSendbuf = new char[ sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			memcpy(pBodyData, &nMarginX, sizeof(int));

			pBodyData = bSendbuf +sizeof(RCPHeader)+ sizeof(int);
			memcpy(pBodyData, &nMarginY, sizeof(int));
		}
		break;
	case WM_ESM_NET_MAKEMOVIETOCLIENT :
		{
			packetheader.command = RCPCMD_MAKEMOVIE_TOCLIENT;
			TimeLineInfo* pMovieData;
			vector<TimeLineInfo>* arrTimeLineObject;
			arrTimeLineObject = (vector<TimeLineInfo>* )pMsg->pParam;
			int nMovieSize = arrTimeLineObject->size();

			// TimeLine size + ( MakeMovieInfo + EffectInfo Size + ( EffectInfo ) * EffectInfo Size ) * TimeLine size
			packetheader.bodysize = sizeof(int) + sizeof(MakeMovieInfo) * nMovieSize;
			for ( int i = 0 ; i < nMovieSize ; i++ )
			{
				int nFrameSize = arrTimeLineObject->at(i).arrEffectData.size();
				packetheader.bodysize = packetheader.bodysize + sizeof(EffectInfo) * nFrameSize + sizeof(int);
			}

			bSendbuf = new char[ sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			memcpy(pBodyData, &nMovieSize, sizeof(int));
			pBodyData = pBodyData + sizeof(int);
			for( int i = 0; i< nMovieSize; i++)
			{
				pMovieData = &((arrTimeLineObject->at(i)));
				pMovieData->stMovieData.strDscID;
				pMovieData->stMovieData.nStartFrame;
				pMovieData->stMovieData.nEndFrame;

				memcpy(pBodyData, &(pMovieData->stMovieData), sizeof(MakeMovieInfo));
				pBodyData = pBodyData + sizeof(MakeMovieInfo);

				int nFrameSize = arrTimeLineObject->at(i).arrEffectData.size();

				memcpy(pBodyData, &nFrameSize, sizeof(int));
				pBodyData = pBodyData + sizeof(int);

				for ( int j = 0 ; j < nFrameSize ; j++ )
				{
					EffectInfo stEffectInfo = arrTimeLineObject->at(i).arrEffectData.at(j);
					memcpy( pBodyData, &stEffectInfo, sizeof(EffectInfo) );
					pBodyData = pBodyData + sizeof(EffectInfo);
				}
			}
			delete arrTimeLineObject;
		}
		break;
	case WM_ESM_NET_MAKEMOVIE_FINISH:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_MAKEMOVIE_FINISH_TOSERVER;
			packetheader.param1 = pMsg->nParam1;
			packetheader.param2 = pMsg->nParam2;
		}
		break;
	case WM_ESM_NET_REQUEST_SEND_DATA:
		{
			ESMLog(5,_T("[#NETWORK] Return Send Request Data [%d]"),m_nRemoteID);
			packetheader.command = RCPCMD_REQUEST_DATA_TO4DP;
			if(pMsg->pParam)
			{
				bSendbuf = new char[sizeof(RCP_FileSave_Info) + sizeof(RCPHeader) + pMsg->nParam1 ];
				m_pFileBody = (RCP_FileSave_Info*)(bSendbuf + sizeof(RCPHeader));

				CString* pstrTemp_ = (CString*)pMsg->pParam;
				CString pstrTemp;
				pstrTemp.Format(_T("%s"),*pstrTemp_);
				char* strId = ESMUtil::CStringToChar(pstrTemp);
				if(pstrTemp_)
				{
					delete pstrTemp_;
					pstrTemp_ = NULL;
				}
				//memcpy(m_pFileBody->szFileName, strId, sizeof(char) * 128);
				if( strId)
				{
					memset(m_pFileBody->szFileName, 0, sizeof(m_pFileBody->szFileName));
					strncpy(m_pFileBody->szFileName, strId, strlen(strId));
					delete strId;
					strId = NULL;
				}

				memcpy(m_pFileBody->szFileName + sizeof(char) * 128, (void*)pMsg->pDest, pMsg->nParam1);
				pItem = NULL;
				if(pMsg->pDest)
					delete (void*)pMsg->pDest;

				packetheader.param1 = sizeof(RCP_FileSave_Info);
				packetheader.param2 = pMsg->nParam1;
				packetheader.bodysize = sizeof(RCP_FileSave_Info) + pMsg->nParam1;
			}
		}
		break;
	case WM_ESM_NET_REQUEST_DATA:
		{
			vector<MakeFrameInfo>* pMovieInfo = (vector<MakeFrameInfo>*)pMsg->pParam;
			MakeFrameInfo* pMakeFrameInfo;
			int nInfoCount = pMovieInfo->size();
			packetheader.bodysize = sizeof(MakeFrameInfo) * nInfoCount;

			bSendbuf = new char[ sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);

			for( int i = 0; i< nInfoCount; i++)
			{
				pMakeFrameInfo = &((pMovieInfo->at(i)));

				memcpy(pBodyData, pMakeFrameInfo, sizeof(MakeFrameInfo));
				pBodyData = pBodyData + sizeof(MakeFrameInfo);
			}
			packetheader.command = RCPCMD_REQUEST_DATA;
			pMsg->nParam1 = nInfoCount;

			delete pMovieInfo;
		}
		break;
	case WM_ESM_NET_MAKEFRAMEMOVIETOCLIENT :
		{
			vector<MakeFrameInfo>* pMovieInfo = (vector<MakeFrameInfo>*)pMsg->pParam;
			MakeFrameInfo* pMakeFrameInfo;

			int nInfoCount = pMovieInfo->size();
			packetheader.bodysize = sizeof(MakeFrameInfo) * nInfoCount;

			bSendbuf = new char[ sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);

			for( int i = 0; i< nInfoCount; i++)
			{
				pMakeFrameInfo = &((pMovieInfo->at(i)));

				memcpy(pBodyData, pMakeFrameInfo, sizeof(MakeFrameInfo));
				pBodyData = pBodyData + sizeof(MakeFrameInfo);
			}

			packetheader.command = RCPCMD_MAKEFRAMEMOVIE_TOCLIENT;

			packetheader.param1 = pMsg->nParam1;

			pMsg->nParam1 = nInfoCount;
			packetheader.param2 = pMsg->nParam2;
			packetheader.param3 = pMsg->nParam3;

			delete pMovieInfo;
		}
		break;
	case WM_ESM_NET_FRAME_LOAD_TIMETOSERVER:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_FRAME_LOAD_TIME_TOSERVER;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;	
			packetheader.param3	 = pMsg->nParam3;	
		}
		break;
	case WM_ESM_NET_REQUEST_DATA_TOSERVER:
	case WM_ESM_NET_REFEREE_TRANSCODE_FINISH:
	case WM_ESM_NET_REFEREE_MUX_FINISH:
	case WM_ESM_NET_MULTIVIEW_FINISH:
		{
			ESMLog(5,_T("[#NETWORK] Send Request Data"));

			if(pMsg->message == WM_ESM_NET_REQUEST_DATA_TOSERVER)
				packetheader.command = RCPCMD_REQUEST_DATA_TOSERVER;
			else if(pMsg->message == WM_ESM_NET_REFEREE_TRANSCODE_FINISH)
				packetheader.command = RCPCMD_REFEREETRANSCODE_FINISH;
			else if(pMsg->message == WM_ESM_NET_REFEREE_MUX_FINISH)
				packetheader.command = RCPCMD_REFEREEMUX_FINISH;
			else if(pMsg->message == WM_ESM_NET_MULTIVIEW_FINISH)
				packetheader.command = RCPCMD_MULTIVIEW_FINISH;

			if(pMsg->pParam)
			{
				bSendbuf = new char[sizeof(RCP_FileSave_Info) + sizeof(RCPHeader) + pMsg->nParam3 ];
				m_pFileBody = (RCP_FileSave_Info*)(bSendbuf + sizeof(RCPHeader));
				CString* pstrTemp_ = (CString*)pMsg->pParam;
				CString pstrTemp;
				pstrTemp.Format(_T("%s"),*pstrTemp_);
				char* strId = ESMUtil::CStringToChar(pstrTemp);
				if(pstrTemp_)
				{
					delete pstrTemp_;
					pstrTemp_ = NULL;
				}
				//memcpy(m_pFileBody->szFileName, strId, sizeof(char) * 128);
				if( strId)
				{
					memset(m_pFileBody->szFileName, 0, sizeof(m_pFileBody->szFileName));
					strncpy(m_pFileBody->szFileName, strId, strlen(strId));
					delete strId;
					strId = NULL;
				}

				memcpy(m_pFileBody->szFileName + sizeof(char) * 128, (void*)pMsg->pDest, pMsg->nParam3);
				pItem = NULL;
				if(pMsg->pDest)
					delete (void*)pMsg->pDest;

				packetheader.param1 = pMsg->nParam1;//Movie Index
				packetheader.param2 = sizeof(RCP_FileSave_Info);
				packetheader.param3 = pMsg->nParam3;//Movie Size
				/*ESMLog(5,_T("param1: %d"),packetheader.param1);
				ESMLog(5,_T("param2: %d"),packetheader.param2);
				ESMLog(5,_T("param3: %d"),packetheader.param3);*/
				packetheader.bodysize = sizeof(RCP_FileSave_Info) + pMsg->nParam3;
			}
		}
		break;
	case WM_ESM_NET_MAKEFRAMEMOVIEFINISHTOSERVER:
		{
			CString *strPath = (CString *)pMsg->pParam;
			ESMLog(5,_T("Make Frame Movie Finish!! [%s][%d.mp4][%d][%d]"),  *strPath,pMsg->nParam1, pMsg->nParam2,pMsg->nParam3);
			
			CString strDelete;
			strDelete.Format(_T("M:\\Movie\\%s"),*strPath);
			ESMDeleteFile(strDelete);

			packetheader.command = RCPCMD_MAKEFRAMEMOVIE_FINISH_TOSERVER;

			if(pMsg->pParam)
			{
				bSendbuf = new char[sizeof(RCP_FileSave_Info) + sizeof(RCPHeader) ];

				m_pFileBody = (RCP_FileSave_Info*)(bSendbuf + sizeof(RCPHeader));


				CString* pstrTemp_ = (CString*)pMsg->pParam;
				CString pstrTemp;
				pstrTemp.Format(_T("%s"),*pstrTemp_);
				
#if 0
				char* strId = ESMUtil::CStringToChar(pstrTemp);
				memcpy(m_pFileBody->szFileName, strId, sizeof(char) * 128);
#else
				int nLength = pstrTemp.GetLength();
				sprintf(m_pFileBody->szFileName,"%S",pstrTemp);
				//char* strId = new char[nLength];
				//if(strId != NULL)
				//{
				//	sprintf(strId,"%S",pstrTemp);
				//	if(ESMMemcpy(m_pFileBody->szFileName,strId,sizeof(char)*128) == FALSE)
				//	{
				//		ESMLog(0,_T("[WM_ESM_NET_MAKEFRAMEMOVIEFINISHTOSERVER] memcpy Error"));
				//		/*BOOL bFinish = TRUE;
				//		while(bFinish)
				//		{
				//			if(ESMMemcpy(m_pFileBody->szFileName,strId,sizeof(char)*128))
				//				bFinish = FALSE;

				//			Sleep(1);
				//		}*/
				//	}
				//}
				/*else
				{
					ESMLog(0,_T("[WM_ESM_NET_MAKEFRAMEMOVIEFINISHTOSERVER] Memcpy Error"));
					BOOL bLoad = TRUE;
					
					while(bLoad)
					{
						strId = new char[nLength];
						if(strId)
						{
							sprintf(strId,"%S",pstrTemp);
							ESMMemcpy(m_pFileBody->szFileName,strId,sizeof(char)*128);

							bLoad = FALSE;
						}
						else
						{
							delete strId;
							strId = NULL;
						}
					}
				}*/
#endif
				

				//joonho.kim 19.01.25 임시주석
				/*if(pstrTemp_)
				{
					delete pstrTemp_;
					pstrTemp_ = NULL;
				}*/



				/*if( strId)
				{
					delete strId;
					strId = NULL;
				}*/

				packetheader.bodysize = sizeof(RCP_FileSave_Info);
			}
			else
			{
				bSendbuf = new char[sizeof(RCPHeader)];
			}
		}
		break;
	case WM_ESM_NET_SENSORSYNCOUT:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_SENSORSYNCOUT_TOSERVER;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;	
			packetheader.param3	 = pMsg->nParam3;	
		}
		break;
	case WM_ESM_NET_SENSORSYNCTIME:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_SENSORSYNCTIME_TOSERVER;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;	
			packetheader.param3	 = pMsg->nParam3;
		}
		break;
	case WM_ESM_NET_CAPTUREDELAY:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_CAPTUREDELAY_TOCLIENT;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;
			packetheader.param3	 = pMsg->nParam3;
		}
		break;
	case WM_ESM_NET_OPTION_RECORD_FILE_SPLIT:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_OPTION_RECORD_FILE_SPLIT_TOCLIENT;
			packetheader.param1	 = pMsg->nParam1;
		}
		break;
	//-- 2014-09-17 hongsu@esmlab.com
	//-- Send Info to Server 
	case WM_ESM_NET_ERRORFRAME:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_ERRORFRAME_TOSERVER;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;			
		}		
		break;
	case WM_ESM_NET_VALIDSKIP:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_VALIDSKIP_TOSERVER;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;			
		}		
		break;
	case WM_ESM_NET_RESULT_IMAGE_RECORD_TOSERVER:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_RESULT_IMAGE_RECORD_TOSERVER;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;	
			packetheader.param3	 = pMsg->nParam3;	
		}
		break;
	case WM_ESM_NET_RESULT_GET_TICK_TOSERVER:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_RESULT_GET_TICK_TOSERVER;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;	
			packetheader.param3	 = pMsg->nParam3;	
		}
		break;
	case WM_ESM_NET_ERROR_MSG_GH5:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_ERROR_MSG_GH5;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;	
			packetheader.param3	 = pMsg->nParam3;	
		}
		break;
	case WM_ESM_NET_ERROR_MSG:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_ERROR_MSG;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;	
			packetheader.param3	 = pMsg->nParam3;	
		}
		break;
	case WM_ESM_NET_RECORDFINISH:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_RECORDFINISH_TOSERVER;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;			
		}		
		break;
	case WM_ESM_NET_COPYTOSERVER:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_COPYTOSERVER_TOSERVER;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;			
		}		
		break;
	case WM_ESM_NET_ADDFRAME:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_ADDFRAME_TOSERVER;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;			
		}		
		break;
	case WM_ESM_NET_SKIPFRAME:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_SKIPFRAME_TOSERVER;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;			
		}		
		break;
		//-- 2014-09-19 kcd
		//-- Send Info to Server 
	case WM_ESM_NET_EXCEPTIONDSC:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_EXCEPTIONDSC_TOSERVER;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;
			break;
		}	
	case WM_ESM_NET_CONNECT_CHECK:
		{
			m_bCheckConnect = FALSE;

			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_CONNECT_CHECK;
			break;
		}
	case WM_ESM_NET_GET_DSIKSIZE:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_GET_DISKINFO;
			packetheader.param1 = pMsg->nParam1;
			packetheader.param2 = pMsg->nParam2;
			packetheader.param3 = pMsg->nParam3;
			break;
		}
	case WM_ESM_NET_CONNECT_STATUS_CHECK:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_CONNECT_STATUS_CHECK;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;

			//jhhan 16-11-22
			if(m_bNetworkMode)
				packetheader.param3 = ESM_NETWORK_4DP;
		}
		break;
		//jhhan 16-09-23
	case WM_RS_RC_SET_FRAME_RECORD:
		{
			bSendbuf = new char[sizeof(RCP_FileSave_Info) + sizeof(RCPHeader) ];
			m_pFileBody = (RCP_FileSave_Info*)(bSendbuf + sizeof(RCPHeader));
			packetheader.command = RCPCMD_SET_FRAME_RECORD;
			{
				CString pstrTemp = ESMGetFrameRecord();
				char* strId = ESMUtil::CStringToChar(pstrTemp);
				//memcpy(m_pFileBody->szFileName, strId, sizeof(char) * 128);
				if( strId)
				{
					memset(m_pFileBody->szFileName, 0, sizeof(m_pFileBody->szFileName));
					strncpy(m_pFileBody->szFileName, strId, strlen(strId));
					delete strId;
					strId = NULL;
				}

				packetheader.bodysize = sizeof(RCP_FileSave_Info);
			}
		}
		break;
	case WM_RS_RC_SEND_USBDEVICE_CHECK:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_USBDEVICE_CHECK;
		}
		break;
	case WM_ESM_NET_PROPERTY_1STEP:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_PROPERTY_1STEP;
			packetheader.param1 = pMsg->nParam1;
		}
		break;
	case WM_ESM_NET_SEND_KTSENDTIME:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_SEND_KTSENDTIME;
			packetheader.param1 = pMsg->nParam1;
		}
		break;
	case WM_ESM_NET_SEND_KTSTOPTIME:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_SEND_KTSTOPTIME;
			packetheader.param1 = pMsg->nParam1;
		}
		break;
	case WM_ESM_NET_REFEREE_OPENCLOSE:
		{
			ESMLog(5,_T("[%d]Insert OpenClose!"),GetGroupIdx());
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_REFEREE_OPENCLOSE;
			packetheader.param1 = pMsg->nParam1;
			//packetheader.param2 = GetGroupIdx();
		}
		break;
	case WM_ESM_NET_DELETE_FILE:
		{
			bSendbuf = new char[sizeof(RCP_FileSave_Info) + sizeof(RCPHeader) ];
			m_pFileBody = (RCP_FileSave_Info*)(bSendbuf + sizeof(RCPHeader));
			packetheader.command = RCPCMD_DELETE_FILE;
			{
				//CString pstrTemp = ESMGetFrameRecord();
				CString pstrTemp = ESMGetDeleteFolder();
				//if(pstrTemp.IsEmpty() != TRUE)
				{
					char* strId = NULL;
					strId = ESMUtil::CStringToChar(pstrTemp);
					if( strId)
					{
						//memcpy(m_pFileBody->szFileName, strId, sizeof(char) * 128);
						memset(m_pFileBody->szFileName, 0, sizeof(m_pFileBody->szFileName));
						strncpy(m_pFileBody->szFileName, strId, strlen(strId));
						delete strId;
						strId = NULL;
					}
				}
				
				packetheader.bodysize = sizeof(RCP_FileSave_Info);
			}
		}
		break;
	//wgkim 181001
	case WM_ESM_NET_KZONE_POINTS:
		{
			int nDscCount = pMsg->nParam1;
			KZoneDataInfo* pKZoneDataInfo = (KZoneDataInfo*)pMsg->pParam;

			int nSizeInt = sizeof(int);
			int nSizeKZoneData = sizeof(KZoneDataInfo) * nDscCount;

			packetheader.bodysize = nSizeInt + nSizeKZoneData;
			packetheader.command = RCPCMD_SEND_KZONE_POINTS;

			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);

			memcpy(pBodyData, &nDscCount, nSizeInt );
			pBodyData = pBodyData + nSizeInt;

			memcpy(pBodyData, pKZoneDataInfo, nSizeKZoneData );
			pBodyData = pBodyData + nSizeKZoneData;

			ESMLog(5, _T("[K-Zone]K_Zone Data Send to Other Processor."));
		}
		break;

	//wgkim 191217
	case WM_ESM_NET_KZONE_JUDGMENT:
	{
		bSendbuf = new char[sizeof(RCPHeader)];
		packetheader.command = RCPCMD_SEND_KZONE_JUDGMENT;
		packetheader.param1 = pMsg->nParam1;
	}
	break;
	case WM_ESM_NET_DETECT_INFO :
		{
			//jhhan 181126
			vector<DetectInfo>* pDetectInfo = (vector<DetectInfo>*)pMsg->pParam;
			DetectInfo* pDetect;

			int nInfoCount = pDetectInfo->size();
			packetheader.bodysize = sizeof(DetectInfo) * nInfoCount;

			bSendbuf = new char[ sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);

			for( int i = 0; i< nInfoCount; i++)
			{
				pDetect = &((pDetectInfo->at(i)));

				memcpy(pBodyData, pDetect, sizeof(DetectInfo));
				pBodyData = pBodyData + sizeof(DetectInfo);
			}

			packetheader.command = RCPCMD_DETECT_INFO;

			packetheader.param1 = pMsg->nParam1;
			packetheader.param2 = pMsg->nParam2;
			packetheader.param3 = pMsg->nParam3;

			//delete pDetectInfo;
		}
		break;
	case WM_ESM_NET_DETECT_RESULT:
		{
			//jhhan 181126
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_DETECT_RESULT;
			packetheader.param1 = pMsg->nParam1;
			packetheader.param2 = pMsg->nParam2;
			packetheader.param3 = pMsg->nParam3;
		}
		break;
	case WM_ESM_NET_REFEREE_LIVE:
		{
			CString* strPathData;
			strPathData = (CString*)pMsg->pParam;
			packetheader.bodysize = strPathData->GetLength();
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char *buffer;
			buffer = new char[packetheader.bodysize + 1];
			strcpy(buffer,CT2A(*strPathData));
			memcpy(pBodyData, buffer, packetheader.bodysize);
			delete [] buffer;
			packetheader.command = RCPCMD_REFEREE_LIVE;
		}
		break;
	case WM_ESM_NET_REFEREE_SELECT:
		{
			CString* strPathData;
			strPathData = (CString*)pMsg->pParam;
			packetheader.bodysize = strPathData->GetLength();
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char *buffer;
			buffer = new char[packetheader.bodysize + 1];
			strcpy(buffer,CT2A(*strPathData));
			memcpy(pBodyData, buffer, packetheader.bodysize);
			delete [] buffer;
			packetheader.command = RCPCMD_REFEREE_SELECT_TO_CLIENT;
		}
		break;
	case WM_ESM_NET_RTSP_STATE_CHECK:
		{
			CString* strPathData;
			strPathData = (CString*)pMsg->pParam;
			packetheader.bodysize = strPathData->GetLength();
			bSendbuf = new char[sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);
			char *buffer;
			buffer = new char[packetheader.bodysize + 1];
			strcpy(buffer,CT2A(*strPathData));
			memcpy(pBodyData, buffer, packetheader.bodysize);
			delete [] buffer;

			packetheader.command = RCPCMD_RTSP_STATE_CHECK;
			//ESMLog(5,_T("[MsgNet][%d] Message - %d,Data - %d"),pMsg->nParam2,pMsg->nParam1,pMsg->nParam3);
		}
		break;
	default:
		ESMLog(0, _T("SendESMEvent Fail Not message[%d]"),pMsg->message);
		return;
	}

	strcpy(packetheader.protocol, "RCP1.0");
	if(pItem)
	{
		//char* strId = ESMUtil::CStringToChar(pItem->GetDeviceDSCID());
		char* strId = ESMUtil::CStringToChar(pItem->m_strInfo[DSC_INFO_ID]);
		memcpy(packetheader.Camera, strId, sizeof(char) * 8);
		if( strId)
		{
			delete strId;
			strId = NULL;
		}
	}

	packetheader.param1     = pMsg->nParam1;
	packetheader.param2     = pMsg->nParam2;
	packetheader.param3     = pMsg->nParam3;

	int length = sizeof(RCPHeader) + packetheader.bodysize;
	int sent = 0;
	memcpy(bSendbuf, &packetheader, sizeof(RCPHeader));
	if( pMsg->message != WM_RS_RC_NETWORK_STATUS)
	{
		if(m_bNetMode)
		{
			if(m_pServerSocket)
			{
				sent = m_pServerSocket->Send(bSendbuf, length);

				if(m_bMuxTCP)
				{
					ESMSubMuxSendCnt();
				}
				//ESMLog(5, _T("Send 0x%X : %d"), &m_pServerSocket, sent);
			}

			if(pMsg->nParam1 == 0x5003)
			{
				TRACE(_T("%d"),length);
			}
		}
		else
		{
			for( int i =0 ; i< MAXSOCKET; i++)
			{
				if(m_pClientSocket[i])
					sent = m_pClientSocket[i]->Send(bSendbuf, length);
			}
		}
	}

	if( bSendbuf)
	{
		delete[] bSendbuf;
		bSendbuf = NULL;
	}
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	return;
}

void CESMRCManager::ParsePacket(char* headerbuf, char* bodybuf, int packetsize)
{
	RCPHeader* pHeader = (RCPHeader*)headerbuf;

	DWORD bodysize = packetsize;
	DWORD command  = pHeader->command;

	if(m_bNetMode)
	{
		if(m_bAJAUsage == TRUE)
			ReceiveFromAJA(headerbuf,bodybuf,packetsize);
		else
			ParsePacketServer( headerbuf,  bodybuf, packetsize);
	}
	else
	{
		ParsePacketClient( headerbuf,  bodybuf, packetsize);
	}
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		
//! @owner		
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMRCManager::ParsePacketServer(char* headerbuf, char* bodybuf, int packetsize)
{
	RCPHeader* pHeader = (RCPHeader*)headerbuf;

	DWORD bodysize = packetsize;
	DWORD command  = pHeader->command;
	char *pData = NULL;
	RCP_Property_Info* pPropBody = NULL;

	switch(command)
	{
	case RCPCMD_TICKCOUNT_SET:
		{
			int nTickCount = ESMGetTick();
			m_nAgentTime	= pHeader->param1;


			int nGap = nTickCount-m_nLocalTime;
			if(m_nSyncMax < nGap )
				m_nSyncMax = nGap;

			if(m_nSyncMin < 0)
			{
				m_nSyncMin = nGap;
				m_nMinLocalTime = m_nLocalTime;	
				m_nMinAgentTime = m_nAgentTime;
				m_nSyncCount = 0;
				m_nSyncStartTime = ESMGetTick();
				//m_bPCSyncTime = TRUE;
			}

			if(nGap < m_nSyncMin)
			{
				m_nSyncMin = nGap;
				m_nMinLocalTime = m_nLocalTime;	
				m_nMinAgentTime = m_nAgentTime;
				//m_bPCSyncTime = TRUE;
			}

			m_nSyncCount++;
			m_nSyncTotal += nGap;

			if(ESMGetValue(ESM_VALUE_PC_SYNC_TIME) > 100)
			{
				if((nTickCount-m_nSyncStartTime)  > ESMGetValue(ESM_VALUE_PC_SYNC_TIME) || m_nSyncMin == 1 || m_nSyncMin == 0)
				{
					m_nLocalTime = m_nMinLocalTime;	
					m_nAgentTime = m_nMinAgentTime;
					TRACE(_T("[Sync Time] [Group:%d] 5. Get Agent Time : %d   Local Time : %d => Gap[%d]\n"),m_nRemoteID ,m_nAgentTime ,m_nLocalTime ,m_nSyncMin);
					ESMLog(5, _T("[Sync Time] [Group:%d] 5. Get Agent Time : %d  Local Time : %d => Gap[%d] us ,  m_nSyncCount[%d]"), m_nRemoteID, m_nAgentTime, m_nLocalTime, m_nSyncMin*100, m_nSyncCount);
					m_bPCSyncTime = TRUE;
					m_nSyncCount = 0;
				}else
				{
					if (ESMGetValue(ESM_VALUE_SERVERMODE) != ESM_SERVERMODE_MAKING)
						SyncTickCount(TRUE);
				}
			}
			else
			{
				if( GetPcSyncStop())
				{
					ESMLog(5, _T("PC Sync Stop"));
					//				SetThreadSleep(TRUE);
				}
				//joonho.kim 17.06.28 Sync
				//else if( (nTickCount-m_nLocalTime) > 8)   //PC Sync Time
				else if( (nTickCount-m_nLocalTime) > ESMGetValue(ESM_VALUE_PC_SYNC_TIME))   //PC Sync Time
				{
					TRACE(_T("[Sync Time] [Group:%d] 3. Get Communication Time : Send[%d] Receive[%d] => Gap[%d]\n"),m_nRemoteID, m_nLocalTime , nTickCount, nTickCount - m_nLocalTime );
					//ESMLog(5, _T("[Sync Time] [Group:%d] 3. Get Communication Time : Send[%d] Receive[%d] => Gap[%d]"),m_nRemoteID, m_nLocalTime , nTickCount, nTickCount - m_nLocalTime);
					if (ESMGetValue(ESM_VALUE_SERVERMODE) != ESM_SERVERMODE_MAKING)
						SyncTickCount(TRUE);	
				}
				else
				{
					TRACE(_T("[Sync Time] [Group:%d] 5. Get Agent Time : %d  [%d][%d] => Gap[%d]\n"),m_nRemoteID ,m_nAgentTime, nTickCount,m_nLocalTime,nTickCount - m_nLocalTime);
					//ESMLog(5, _T("[Sync Time] [Group:%d] 5. Get Agent Time : %d  [%d][%d] => Gap[%d]"),m_nRemoteID ,m_nAgentTime, nTickCount,m_nLocalTime,nTickCount - m_nLocalTime);
					m_bPCSyncTime = TRUE;
					//				SetThreadSleep(TRUE);
				}
			}
			break;
		}
	case RCPCMD_CONNECT_CHECK:
		{
			int nTickCount = ESMGetTick();
			int nAgentReceiveTimeGap = pHeader->param1-GetAgentTime();
			int nChangeAgentTime = _GetLocalTime() + nAgentReceiveTimeGap;
			TRACE(_T("RCPCMD_CONNECT_CHECKRCPCMD_CONNECT_CHECK 1 %d\n"), GetTickCount());
			//ESMLog(5,_T("RCPCMD_CONNECT_CHECK [%d] Agent Receive Time[%d] Server Receive Time[%d]"), m_nRemoteID, nChangeAgentTime, nTickCount);

			m_bCheckConnect = TRUE;
		}
		break;
	case RCPCMD_ITEM:
		if(bodybuf)
		{
			m_pConBody = (RCP_Connect_Info*)bodybuf;
			CDSCItem* pItem = NULL;
			pItem = new CDSCItem(ESMUtil::CharToCString(pHeader->Camera),DSC_REMOTE);

			//CMiLRe 20160128 카메라 연결 count
			CString strDSCItemNumber;
			strDSCItemNumber.Format(_T("%d"), ESMGetTotalCameraCount());

			pItem->SetDSCInfo(DSC_INFO_NUM	 , strDSCItemNumber);
			pItem->SetDSCInfo(DSC_INFO_MODEL	 , ESMUtil::CharToCString(m_pConBody->szModel));
			pItem->SetDSCInfo(DSC_INFO_UNIQUEID, ESMUtil::CharToCString(m_pConBody->szUniqueID));
			pItem->SetDSCInfo(DSC_INFO_LOCATION, m_strIP);
			pItem->SetType(DSC_REMOTE);
			pItem->m_nRemoteID = m_nRemoteID;
			pItem->SetDSCStatus(m_pConBody->nStatus);
			if( m_pConBody->nStatus == SDI_STATUS_CONNECTED
				|| m_pConBody->nStatus == SDI_STATUS_CAPTURE_OK || m_pConBody->nStatus ==SDI_STATUS_REC_FINISH)
			{
				CDSCItem* tPpItem = NULL;
				CObArray arDSCList;
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				{
					tPpItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if(tPpItem->GetDeviceDSCID() == pItem->GetDeviceDSCID())
					{
						ESMEvent* pMsg	= new ESMEvent;
						pMsg->message	= WM_ESM_LIST_SELECT;
						pMsg->pDest		= (LPARAM)tPpItem;	
						::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
					}
				}
			}
			SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_ADD, 0, 0, 0, (char*)pItem);
			//CMiLRe 20160128 카메라 연결 count
			int nCountCamera = ESMGetTotalCameraCount();
			ESMSetTotalCameraCount(++nCountCamera);
		}
		break;
	case RCPCMD_STATUS:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_STATUS_CHANGE, pHeader->param1,0U,0U,NULL,strID);
		}		
		break;
	case RCPCMD_PRE_REC_INIT_GH5_RESULT:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_PRE_REC_INIT_GH5_RESULT, pHeader->param1,0U,0U,NULL,strID);
		}
		break;
	case RCPCMD_CONVERT_FINISH:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_CONVERT_FINISH, pHeader->param1,0U,0U,NULL,strID);
		}		
		break;
	case RCPCMD_LIVEVIEW:
		if(bodybuf)
		{
			FrameData tpFrameData;
			int nTpInt;			// 임시 변수 
			nTpInt = sizeof(UINT) * 2;

			if (bodybuf == NULL)
			{
				ESMLog(0, _T("@@@ RCPCMD_LIVEVIEW: bodybuf null..."));
			}

			memset(&tpFrameData, 0, sizeof(struct FrameData));
			//memcpy(&tpFrameData, bodybuf, nTpInt);
			if(ESMMemcpy(&tpFrameData, bodybuf, nTpInt) == FALSE)
				break;

			if( m_pFrameData == NULL)
			{
				m_pFrameData = new FrameData;
				memset(m_pFrameData, 0, sizeof(struct FrameData));
				m_pFrameData->nFrameData = new BYTE[tpFrameData.nHeight * tpFrameData.nWidth * 3];
			}

			if( tpFrameData.nHeight != m_pFrameData->nHeight || tpFrameData.nWidth != m_pFrameData->nWidth )
			{
				if (m_pFrameData->nFrameData)
				{
					delete[] m_pFrameData->nFrameData;					
					m_pFrameData->nFrameData = NULL;
				}

				m_pFrameData->nFrameData = new BYTE[tpFrameData.nHeight * tpFrameData.nWidth * 3];
			}
			memcpy(m_pFrameData, bodybuf, nTpInt);
			memcpy(m_pFrameData->nFrameData, bodybuf + nTpInt, bodysize - nTpInt);
			SendMessageESM(WM_ESM_VIEW, WM_ESM_VIEW_LIVEVIEW_BUFFER, 0, 0, 0, (char*)m_pFrameData);
		}else
		{
			//ESMLog(0, _T("Liveview bodybuf is null."));
		}
		break;
	case RCPCMD_GET_FOCUS_FRAME_TOSERVER:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));			

			SendMessageESM(WM_ESM_VIEW, WM_ESM_VIEW_GET_FOCUS_FRAME, pHeader->param1, pHeader->param2, pHeader->param3, NULL, strID);
		}
		break;	
	case RCPCMD_GETFOCUS_TOSERVER:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			pData = new char[sizeof(FocusPosition)];
			memcpy(pData, bodybuf, sizeof(FocusPosition));			
			int bSave = 0;
			memcpy(&bSave, bodybuf+sizeof(FocusPosition), sizeof(int));;
			//CMiLRe 20160204 Focus All Setting 시 Focus 저장되는 버그 수정
			if(bSave != 0 &&  bSave != 1)
				bSave = 0;
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_GET_FOCUS, 0, 0, bSave, pData, strID);
		}
		break;
	case RCPCMD_PROPERTY_GET:
		{
			if( pHeader->param1 == 0xd815)
				TRACE(_T("###Get Property : 0x%x\n"), pHeader->param1);

			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			int ni = pHeader->param2;
			bodysize = 0;

			//m_pPropBody = (RCP_Property_Info*)bodybuf;

			if(pHeader->param1 == PTP_CODE_IMAGESIZE)
			{
				CStringArray* pArList = new CStringArray;
				for(int i=0 ; i<ni ; i++)
				{

#if 1 // Property IMAGE_SIZE 수정
					pPropBody = (RCP_Property_Info*)&bodybuf[bodysize];

					m_strProp = (TCHAR*)pPropBody->szProp;
					pArList->Add(m_strProp);

					if(pPropBody->nProp == 1)
					pHeader->param2 = i;

					bodysize += sizeof(RCP_Property_Info);
#else
					/*pPropBody = (RCP_Property_Info*)&bodybuf[bodysize];

					m_strProp = (TCHAR*)pPropBody->szProp;
					pArList->Add(m_strProp);

					if(pPropBody->nProp == 1)
						pHeader->param2 = i;

					bodysize += sizeof(RCP_Property_Info);*/
#endif
				}
				SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_PROPERTY_RESULT, pHeader->param1, pHeader->param2, 0, (char*)(LPCTSTR)pArList, strID);
			}
			else
			{
				CUIntArray* pArList = new CUIntArray;
				for(int i=0 ; i<ni ; i++)
				{
					pPropBody = (RCP_Property_Info*)&bodybuf[bodysize];
					pArList->Add(pPropBody->nProp);

					if(pPropBody->szProp[0] == 0x01)
					{
						//TRACE(_T("[GetProperty From Client] [Num:%d] [Type:%d] [Value:%d]\n"),i,pHeader->param1,pPropBody->nProp);
						pHeader->param2 = pPropBody->nProp;
					}
					bodysize += sizeof(RCP_Property_Info);
				}
				SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_PROPERTY_RESULT, pHeader->param1, pHeader->param2, 0, (char*)(LPCTSTR)pArList,strID);
			}
		}
		break;
	case RCPCMD_MAKEMOVIE_FINISH_TOSERVER:
		{
			SendMessageESM(WM_ESM_MOVIE, WM_ESM_MOVIE_MAKE_FINISH, pHeader->param1, pHeader->param2);
		}
		break;
	case RCPCMD_FRAME_LOAD_TIME_TOSERVER:
		{
			SendMessageESM(WM_ESM_MOVIE, WM_ESM_FRAME_LOAD_TIME, pHeader->param1, pHeader->param2, pHeader->param3);
		}
		break;
	case RCPCMD_SENSORSYNCOUT_TOSERVER:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));

			SendMessageESM(WM_ESM_DSC, WM_RS_RC_SENSORSYNCOUT, pHeader->param1, pHeader->param2, pHeader->param3, NULL, strID);
		}
		break;

	case RCPCMD_SENSORSYNCTIME_TOSERVER:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			//SendMessageESM(WM_ESM_DSC, WM_RS_RC_SETSENSORTIME, pHeader->param1, pHeader->param2, pHeader->param3, NULL, strID);
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_RS_RC_SETSENSORTIME;	
			pMsg->nParam1	= (LPARAM)pHeader->param1;
			pMsg->nParam2	= (LPARAM)pHeader->param2;	
			pMsg->nParam3	= (LPARAM)pHeader->param3;
			if(strID != _T(""))
			{
				CString* pStr = new CString;
				pStr->Format(_T("%s"),strID);
				pMsg->pDest		= (LPARAM)pStr;
			}
			else
				pMsg->pDest		= NULL;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_DSC, (LPARAM)pMsg);
		}
		break;

	//-- 2014-09-17 hongsu@esmlab.com
	//-- Get Event From Agent 
	case RCPCMD_ERRORFRAME_TOSERVER:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_ERRORFRAME, pHeader->param1, pHeader->param2, 0, NULL, strID);
		}
		break;
	case RCPCMD_ADDFRAME_TOSERVER:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_ADDFRAME, pHeader->param1, pHeader->param2, 0, NULL, strID);
		}
		break;
	case RCPCMD_SKIPFRAME_TOSERVER:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_SKIPFRAME, pHeader->param1, pHeader->param2, 0, NULL, strID);
		}
		break;
	case RCPCMD_VALIDSKIP_TOSERVER:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_VALIDSKIP, pHeader->param1, pHeader->param2, 0, NULL, strID);
		}
		break;	
	case RCPCMD_RESULT_IMAGE_RECORD_TOSERVER:
		{
			//ESMLog(5, _T("@@@@@@@ RCPCMD_RESULT_IMAGE_RECORD_TOSERVER"));
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));

			int nInitGH5Result = pHeader->param2;
			BOOL bCompleteSendCapture = pHeader->param3;

			TRACE(_T("### RCPCMD_RESULT_IMAGE_RECORD_TOSERVER >> Complete Send Capture ID[%s], completed[%d] \n"), strID, bCompleteSendCapture);

			CString strModel;
			CDSCItem* pItem = NULL;
			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);
			int nDSCCnt =  arDSCList.GetCount();
			for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
			{
				pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
				if(strID == pItem->GetDeviceDSCID())
				{
					pItem->SetCaptureImageRecExcept(!bCompleteSendCapture);

					strModel = pItem->GetDeviceModel();
				}
			}
			
			if (bCompleteSendCapture && (strModel.CompareNoCase(_T("GH5")) == 0) && nInitGH5Result != -1)
			{
				if (nInitGH5Result != SDI_ERR_OK)
				{
					CString strMsg1;
					strMsg1.Format(_T("#######[ID: %s] PTP_InitGH5 Fail Result [0x%X]"), ESMUtil::CharToCString(pHeader->Camera), nInitGH5Result);
					ESMLog(5, strMsg1);
				}
			}

			if (nInitGH5Result == -1)
			{
// 				CString strMsg;
// 				strMsg.Format(_T("#######[ID: %s] result RECORD_STOP"), ESMUtil::CharToCString(pHeader->Camera));
// 				ESMLog(5, strMsg);

 				CString strID;
 				strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
 				SendMessageESM(WM_ESM_DSC, WM_RS_RC_RECORDFINISH, pHeader->param1, pHeader->param2, 0, NULL, strID);
 
 				SendMessageESM(WM_ESM_LIST, WM_RS_RC_RECORDFINISH, pHeader->param1, pHeader->param2, 0, NULL, strID);
			}

#if 1
			CString strMsg;
			if (bCompleteSendCapture)
			{
				int nResult = (int)pHeader->param1;

				if (nResult == SDI_ERR_OK)
				{
					nResult = 0x2001; //PTP_RC_OK
				}
				else
				{
					strMsg.Format(_T("###[ID: %s] PTP_SendCaptrue_Require Result 0x%X"), ESMUtil::CharToCString(pHeader->Camera), nResult);
					ESMLog(5, strMsg);
				}
			}
			else
			{
				strMsg.Format(_T("$$$[ID: %s] PTP_SendCaptrue_Require Result Wait"), ESMUtil::CharToCString(pHeader->Camera));
			}
#endif
		}
		break;
	case RCPCMD_RESULT_GET_TICK_TOSERVER:
		{		
			CString strMsg;
			strMsg.Format(_T("###[ID: %s] PTP_GET_TICK - RESULT   pc = %u us, dsc = %u, Gap = %u us"), ESMUtil::CharToCString(pHeader->Camera), pHeader->param1*100, pHeader->param2, pHeader->param3*100);

			ESMLog(5, strMsg);
		}
		break;
	case RCPCMD_ERROR_MSG_GH5:
		{
			CString strID, strErr;
			strID.Format(_T("%s"), ESMUtil::CharToCString(pHeader->Camera));

 			if (pHeader->param1 == ERROR_TEMPERATURE_REC_STOP_ADVANCE_NOTICE ||
 				pHeader->param1 == ERROR_TEMPERATURE_REC_STOP_NOTICE ||
 				pHeader->param1 == ERROR_TEMPERATURE_REC_STOP_DROP)
			{
				SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_CAM_ERROR_MSG, pHeader->param1, 0, 0, NULL, strID);
			}

			switch(pHeader->param1)
			{
			case ERROR_CODE_OVERWRITE:
				strErr.Format(_T("Overwrite movie in memory"));
				break;
#if 0		// 많은 대수의 카메라에서 온도 로그가 동시 발생시 프로그램에 문제가 발생..
			case ERROR_TEMPERATURE_REC_STOP_ADVANCE_NOTICE:		// An advance notice of recording stop
				strErr.Format(_T("[CAM Temperature Alarm] Temperature drop. Recording available.")); 
				break;
			case ERROR_TEMPERATURE_REC_STOP_NOTICE:				// A notice of recording stop
				strErr.Format(_T("[CAM Temperature Alarm] Temperature rises. Recording Stop. ")); 
				break;
			case ERROR_TEMPERATURE_REC_STOP_DROP:				// Drop Temperature Permission to start recording
				strErr.Format(_T("[CAM Temperature Alarm] Temperature rises. May be a problem with the recording. ")); 
				break;
#else
			case ERROR_TEMPERATURE_REC_STOP_ADVANCE_NOTICE:
			case ERROR_TEMPERATURE_REC_STOP_NOTICE:
			case ERROR_TEMPERATURE_REC_STOP_DROP:
				return;
#endif
			case ERROR_GET_DEVICEPROP:
				strErr.Format(_T("GetDevicePropDesc error"));
				break;
			case ERROR_SET_DEVICEPROP:
				strErr.Format(_T("SetDevicePropValue error"));
				break;
			case ERROR_FILE_TRANSFER:
				strErr.Format(_T("Filetransfer error"));
				break;
			case ERROR_SET_TICK:
				strErr.Format(_T("SetTick Error"));
				break;
			case ERROR_MOVIE_STOP:
				strErr.Format(_T("Reason the movie of stopping"));
				break;
			case ERROR_SENSOR_WRONG:
				strErr.Format(_T("Sensor VD Time maybe wrong"));
				break;
			case ERROR_ZOOM_MEMORY:
				strErr.Format(_T("Zoom memory Error"));
				break;
			case ERROR_CAMERA_ERROR_1:
				strErr.Format(_T("Camera Error"));
				break;
			case ERROR_CAMERA_ERROR_2:
				strErr.Format(_T("Camera Error"));
				break;
			default:
				strErr.Format(_T("UnKnown"));
				break;
			}
			strID.Format(_T("###[ID: %s] PTP_ERROR_MSG %s p1 = %u, p2 = %u, p3 = %u"), ESMUtil::CharToCString(pHeader->Camera), strErr, pHeader->param1, pHeader->param2, pHeader->param3);
			
			if(pHeader->param1 != ERROR_CODE_TEST && pHeader->param1 != ERROR_CODE_TEST2)
				ESMLog(0, strID);

#if 1
			if(pHeader->param1 == ERROR_GET_DEVICEPROP || pHeader->param1 == ERROR_SET_DEVICEPROP)
			{
				CString strMsg;
				switch (pHeader->param2)
				{
				case 0x5000:	strMsg = _T("Undefined"); break;
				case 0x5002:	strMsg = _T("Recording Mode"); break;
				case 0x5003:	strMsg = _T("Capture - Picture Size"); break;
				case 0x5004:	strMsg = _T("Capture - Quality"); break;
				case 0x5005:	strMsg = _T("White Balance"); break;
				case 0x5007:	strMsg = _T("Aperture"); break;
				case 0x500A:	strMsg = _T("Focus Mode"); break;
				case 0x500F:	strMsg = _T("ISO - Light Sensitivity"); break;
				case 0x5013:	strMsg = _T("Photo and Drive Mode"); break;
				case 0xD802:	strMsg = _T("ISO Increments"); break;
				case 0xD808:	strMsg = _T("Extended ISO"); break;
				case 0xD80D:	strMsg = _T("Stabilizer - Operation Mode"); break;
				case 0xD815:	strMsg = _T("Shutter Speed"); break;
				case 0xD81C:	strMsg = _T("UUID ( 13 letter )"); break;
				case 0xD842:	strMsg = _T("Economy - Sleep Mode"); break;
				case 0xD84B:	strMsg = _T("Rec Quality"); break;
				case 0xD90D:	strMsg = _T("Language"); break;
				case 0xD91A:	strMsg = _T("Guide Line"); break;
				case 0xD95A:	strMsg = _T("Setup - Sensor Cleaning"); break;
				case 0xD97F:	strMsg = _T("Lens (mm Info)"); break;
				case 0xD980:	strMsg = _T("Lens (mm Set)"); break;
				case 0xD981:	strMsg = _T("Lens Power Control"); break;
				case 0xD991:	strMsg = _T("Version Disp - Body Firmware"); break;
				case 0x9009:	strMsg = _T("Lens (Focus Info)"); break;
				case 0x9008:	strMsg = _T("Lens (Focus Set)"); break;
				case 0x900A:	strMsg = _T("Contrast of image"); break;
				case 0xE111:	strMsg = _T("White Balance"); break;
				case 0xE112:	strMsg = _T("White Balance"); break;
				case 0xE113:	strMsg = _T("White Balance"); break;
				case 0xE114:	strMsg = _T("White Balance"); break;
				case 0xE121:	strMsg = _T("Colour Temperature"); break;
				case 0xE122:	strMsg = _T("Colour Temperature"); break;
				case 0xE123:	strMsg = _T("Colour Temperature"); break;
				case 0xE124:	strMsg = _T("Colour Temperature"); break;
				case 0xE301:	strMsg = _T("Aspect Ratio"); break;
				case 0xE401:	strMsg = _T("Rec Format"); break;
				case 0xE402:	strMsg = _T("Luminance Level"); break;
				case 0xE403:	strMsg = _T("I Dynamic"); break;
				case 0xE404:	strMsg = _T("I Resolution"); break;
				case 0xE405:	strMsg = _T("E Stabilization"); break;
				case 0xE406:	strMsg = _T("Length Set"); break;
				case 0xE407:	strMsg = _T("Level Adj"); break;
				case 0xE408:	strMsg = _T("Level Limiter"); break;
				case 0xE409:	strMsg = _T("Noise Canceller"); break;
				case 0xE40A:	strMsg = _T("Digital Zoom"); break;
				case 0xE501:	strMsg = _T("Rec Area"); break;
				case 0xE502:	strMsg = _T("Position Resume"); break;
				case 0xE503:	strMsg = _T("System Frequency"); break;
				case 0xE504:	strMsg = _T("LCD"); break;
				case 0xE505:	strMsg = _T("Peaking"); break;
				case 0xE506:	strMsg = _T("Photo Style"); break;
				case 0xE507:	strMsg = _T("Contrast of PhotoStyle"); break;
				case 0xE508:	strMsg = _T("Sharpness of PhotoStyle"); break;
				case 0xE509:	strMsg = _T("Noise Reduction of PhotoStyle"); break;
				case 0xE50A:	strMsg = _T("Saturation of PhotoStyle"); break;
				case 0xE50B:	strMsg = _T("Hue of PhotoStyle"); break;
				case 0xE50C:	strMsg = _T("Filter Effect of PhotoStyle"); break;
				case 0xE50D:	strMsg = _T("White Balance Adjust A-B"); break;
				case 0xE50E:	strMsg = _T("White Balance Adjust M-G"); break;
				case 0xE50F:	strMsg = _T("Temp of message send before camera stop"); break;
				}
				
				CString strTemp;
				strTemp.Format(_T("###[ID: %s] error description :  %s"), ESMUtil::CharToCString(pHeader->Camera), strMsg);

				ESMLog(0, strTemp);
			}
#endif
		}
		break;
	case RCPCMD_ERROR_MSG:
		{
			if(pHeader->param1 == 1) //Card Error
			{
				CString strID;
				strID.Format(_T("%s Card Error"),ESMUtil::CharToCString(pHeader->Camera));
				ESMLog(0, strID);
				AfxMessageBox(strID);
			}
		}
		break;
	case RCPCMD_RECORDFINISH_TOSERVER:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_RECORDFINISH, pHeader->param1, pHeader->param2, 0, NULL, strID);

			SendMessageESM(WM_ESM_LIST, WM_RS_RC_RECORDFINISH, pHeader->param1, pHeader->param2, 0, NULL, strID);
		}
		break;
	case RCPCMD_COPYTOSERVER_TOSERVER:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_COPYTOSERVER, pHeader->param1, pHeader->param2, 0, NULL, strID);
		}
		break;
	case RCPCMD_EXCEPTIONDSC_TOSERVER:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_EXCEPTIONDSC, pHeader->param1, pHeader->param2, 0, NULL, strID);
		}
		break;
	case RCPCMD_CAMERA_SYNC_CHECK:
		{
			//ESMEvent* pMsg	= new ESMEvent;
			//pMsg->message	= WM_ESM_DSC_SYNC_CHECK;	
			//pMsg->nParam1	= (LPARAM)pHeader->param1;
			//pMsg->nParam2	= (LPARAM)pHeader->param2;	
			//pMsg->nParam3	= (LPARAM)pHeader->param3;	
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_ESM_DSC_SYNC_CHECK, m_nRemoteID, pHeader->param2, pHeader->param3, NULL, strID);

			//ESMLog(5, _T("[DSC Sync Time] [Group:%d] [DSC:%d] Gap[%d] Count[%d]"), m_nRemoteID, pHeader->param2, pHeader->param3, pHeader->param1);
		}
		break;
	case RCPCMD_CAMERA_SYNC_RESULT:
		{
			int nRemoteID;
			memcpy(&nRemoteID, bodybuf, sizeof(int));
			SendMessageESM(WM_ESM_DSC, WM_ESM_DSC_SYNC_RESULT, nRemoteID, 0);
		}
		break;
	case RCPCMD_GET_TICK_CMD_RESULT_TOSERVER:
		{		
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));

			SendMessageESM(WM_ESM_DSC, WM_ESM_DSC_GET_TICK_RESULT_CMD, pHeader->param1, pHeader->param2, 0, NULL, strID);

			SendMessageESM(WM_ESM_LIST, WM_ESM_DSC_GET_TICK_RESULT_CMD, pHeader->param1, pHeader->param2, 0, NULL, strID);
		}
		break;
	case RCPCMD_REQUEST_DATA_TOSERVER:
		{
			//ESMLog(1, _T("[#NETWORK] RCPCMD_REQUEST_DATA_TOSERVER1[%d]"),m_nRemoteID);
			
			BOOL bAJAMaking = pHeader->param2;
			ESMEvent* pMsg	= new ESMEvent;

			if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetConnectAJANetwork() 
				&& ESMGetAJAScreen() == FALSE && bAJAMaking == TRUE)
			{	
				char* pData = new char[pHeader->bodysize];
				memcpy(pData,bodybuf,pHeader->bodysize);

				pMsg->message = F4DC_SEND_CPU_FILE;
				pMsg->pDest   = (LPARAM) pData;
				pMsg->nParam1 = pHeader->param1;
				pMsg->nParam2 = bodysize;
				pMsg->nParam3 = pHeader->param3;
				ESMSendAJANetwork(pMsg);
			}
			else
			{
				pMsg->message	= WM_MAKEMOVIE_REQUEST_DATA_FINISH;	
				pMsg->pParam      = (LPARAM) bodybuf;
				pMsg->nParam1 = pHeader->param1;
				pMsg->nParam2 = bodysize;
				pMsg->nParam3 = pHeader->param3;

				//ESMLog(1, _T("[#NETWORK] ParsePacketServer"));

				char* path = new char[sizeof(RCP_FileSave_Info)];
				CString *pPath = new CString;
				memcpy(path, bodybuf, sizeof(RCP_FileSave_Info));
				pPath->Format(_T("%s"),ESMUtil::CharToCString(path));
				pMsg->pDest			= (LPARAM) pPath;	

				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

				if(path)
					delete path;
			}
		}
		break;
	case RCPCMD_REFEREETRANSCODE_FINISH:
		{
			char* pData = new char[pHeader->bodysize];
			memcpy(pData,bodybuf,pHeader->bodysize);

			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_MAKEMOVIE_REFEREE_LIVE_MERGE;
			pMsg->nParam1 = pHeader->param1;//sec index
			pMsg->nParam2 = GetGroupIdx();//Group index
			pMsg->nParam3 = pHeader->bodysize;//용량
			pMsg->pDest = (LPARAM) pData;

			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

			if(pData)
			{
				delete pData;
				pData = NULL;
			}
		}
		break;
	case RCPCMD_REFEREEMUX_FINISH:
		{
			char* path = new char[sizeof(RCP_FileSave_Info)];
			CString *pPath = new CString;
			memcpy(path, bodybuf, sizeof(RCP_FileSave_Info));
			pPath->Format(_T("%s"),ESMUtil::CharToCString(path));

			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_MAKEMOVIE_REFEREE_MUX_FINISH;	
			pMsg->pParam    = (LPARAM) bodybuf;
			pMsg->nParam1	= pHeader->param1;
			pMsg->nParam2	= bodysize;
			pMsg->nParam3	= pHeader->param3;	
			pMsg->pDest		= (LPARAM) pPath;	

			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

			if(path)
			{
				delete path;
				path = NULL;
			}
		}
		break;
	case RCPCMD_MAKEFRAMEMOVIE_FINISH_TOSERVER:
		{
			//SendMessageESM(WM_ESM_VIEW, WM_MAKEMOVIE_DP_MOVIE_FINISH, pHeader->param1, pHeader->param2, m_nRemoteID);
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_MAKEMOVIE_DP_MOVIE_FINISH;	
			pMsg->nParam1	= (LPARAM)pHeader->param1;
			pMsg->nParam2	= (LPARAM)pHeader->param2;	
			pMsg->nParam3	= (LPARAM)m_nRemoteID;
			pMsg->pParam      = (LPARAM) pHeader->param3;
			m_pFileBody = (RCP_FileSave_Info*)bodybuf;
			CString *pPath = new CString;
			pPath->Format(_T("%s"),ESMUtil::CharToCString(m_pFileBody->szFileName));
			pMsg->pDest			= (LPARAM) pPath;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		}
		break;
	case RCPCMD_USBCONNECTFAIL:
		{
			ESMLog(1, _T("OnEventUsbConnectFail"));
		}
		break;
	case RCPCMD_CONNECT_STATUS_CHECK:
	{
		int ip =pHeader->param1 ;		
		//ESMLog(1,_T("%d,come on~"),ip);


		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message = WM_ESM_NET_CONNECT_STATUS_CHECK_RESULT;
		pMsg->nParam1 = ip;
		pMsg->pParam  = (LPARAM)this;
		//jhhan 16-11-22
		pMsg->nParam3 = pHeader->param3;
		::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}
	break;
	case RCPCMD_GET_DISKINFO:
		{
			m_nDiskTotalSize = pHeader->param1;
			m_nDiskFreeSize	 = pHeader->param2;

			// pHeader->param3 0,2 GPU X		1,3  GPU O
			if((pHeader->param3 == 0) || (pHeader->param3 == 2))
				m_bGPUUse		= FALSE;
			else
				m_bGPUUse		= TRUE;

			// pHeader->param3 0,1 1G X			2,3  1G O
			if(pHeader->param3 < 2)
				m_bNetSpeed		= FALSE;
			else
				m_bNetSpeed		= TRUE;
			
			ESMEvent* pMsg = NULL;
			pMsg = new ESMEvent();
			pMsg->message = WM_ESM_VIEW_NET_UPDATE_DISKSIZE;
			pMsg->nParam1 = m_nRemoteID;
			pMsg->nParam2 = m_bConnected;
			pMsg->nParam3 = m_bGPUUse;
			pMsg->pParam  = (LPARAM)this;
			::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
			
			TRACE(_T("Client Disk Size [%d] %d/%d\r\n"),m_nRemoteID , pHeader->param2, pHeader->param1);
		}
		break;
	case RCPCMD_MUX_SENDFINISH:
		{
			CString strTemp = ESMUtil::CharToCString(pHeader->Camera);
			CString *strCamID = new CString;
			strCamID->Format(_T("%s"),strTemp);

			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_ESM_FRAME_MUX_FINISH;
			pMsg->nParam1	= pHeader->param1;
			//pMsg->nParam2	= pHeader->param2;
			pMsg->nParam3	= m_nRemoteID;//pHeader->param3;
			//ESMLog(5,_T("%d"),m_nRemoteID);
			pMsg->pDest		= (LPARAM)strCamID;
			::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);

			m_bMUXRunning = FALSE;
		}
		break;
	case RCPCMD_USBDEVICE_CHECK:
		{
			ESMLog(1, _T("USB Device Check Agent IP [%d]"), m_nRemoteID);
		}
		break;		

	case RCPCMD_BACKUP_FILECOUNT_ALL:
		{
			ESMLog(5,_T("RCPCMD_BACKUP_FILECOUNT_ALL [%d]"), pHeader->param1);
			
			m_bCheckConnect = TRUE;
		}
		break;
	case RCPCMD_BACKUP_START:
		{			
			ESMLog(5,_T("Backup Start : %s(File : %d)"), GetIP(), pHeader->param1);
			m_nBackupAllFileCount = pHeader->param1;
		}
		break;
	case RCPCMD_BACKUP_END:
		{			
			ESMLog(5,_T("Backup End: %d"), pHeader->param1);			
		}
		break;
	case RCPCMD_BACKUP_FILECOUNT_CURRENT:
		{			
			//ESMLog(5,_T("File Count: %d, IP : %s"), pHeader->param1, m_strIP);

			m_nBackupCurrentFileCount = pHeader->param1;
			m_bFinish = pHeader->param2;
		}
		break;
	case RCPCMD_BACKUP_DIRECT_DELETE:
		{			
			//ESMLog(5,_T("RCPCMD_BACKUP_DIRECT_DELETE: %d"), pHeader->param1);			
		}
		break;
	case RCPCMD_REFEREE_OPENCLOSE:
		{
			//ESMLog(5,_T("ServerOpenClose"));
			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_MAKEMOVIE_REFEREE_OPENCLOSE;
			pMsg->nParam1 = pHeader->param1;
			pMsg->nParam2 = GetGroupIdx();
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		}
		break;
	//case RCPCMD_BACKUP_SET_PATH:
	//	{

	//	}
	//	break;
	case RCPCMD_DETECT_RESULT:
		{
			//jhhan 181126
			ESMLog(5, _T("-----Detect Result---- [%d] [%d]"), pHeader->param1, pHeader->param2);
		}
		break;
	case RCPCMD_MULTIVIEW_FINISH:
		{
			//multiview finish
			int nIdx = pHeader->param1;
			BOOL bMaking = pHeader->param2;

			if(bMaking)
			{
				char* path = new char[sizeof(RCP_FileSave_Info)];
				CString strTemp;
				strTemp.Format(_T("M:\\Movie\\%d.mp4"),nIdx);

				CFile file;

				if(file.Open(strTemp, CFile::modeCreate | CFile::modeReadWrite))
				{
					file.Write((void*)(bodybuf + sizeof(RCP_FileSave_Info)), bodysize-sizeof(RCP_FileSave_Info));
					file.Close();
				}
				ESMLog(5,_T("[MULTIVIEW] - %d received"),nIdx);
			}

			SetMultiViewStart(FALSE);
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_ESM_MOVIE_MULTIVIEW_FINISH;
			pMsg->nParam1	= nIdx;
			pMsg->nParam2	= m_nRemoteID;//pHeader->param3;
			pMsg->nParam3	= bMaking;
			//ESMLog(5,_T("%d"),m_nRemoteID);
			::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);
		}
		break;
	case RCPCMD_RTSP_STATE_CHECK:
		{
			CString* pStrPath;
			pStrPath = new CString;
			char* pPathData = NULL;
			pPathData = new char[pHeader->bodysize + 1];
			memset(pPathData, 0, pHeader->bodysize + 1);
			memcpy(pPathData, bodybuf, bodysize);

			*pStrPath = CA2T(pPathData);

			ESMRTSPStateUpdate(*pStrPath,pHeader->param2,pHeader->param1,pHeader->param3);

			if(pStrPath)
			{
				delete pStrPath;
				pStrPath = NULL;
			}
		}
		break;
	default:
		break;
	}
}

void CESMRCManager::ParsePacketClient(char* headerbuf, char* bodybuf, int packetsize)
{
	RCPHeader* pHeader = (RCPHeader*)headerbuf;

	DWORD bodysize = packetsize;
	DWORD command  = pHeader->command;
	char *pData = NULL;
	RCP_Property_Info* pPropBody = NULL;

	switch(command)
	{
	case RCPCMD_REQUEST_DATA_TOSERVER:
		{
			ESMLog(1, _T("[#NETWORK] RCPCMD_REQUEST_DATA_TOSERVER2[%d]"),m_nRemoteID);
			//Save Data
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_MAKEMOVIE_REQUEST_DATA_FINISH;	
			pMsg->pParam      = (LPARAM) bodybuf;

			pMsg->nParam2 = bodysize;
			pMsg->nParam3 = pHeader->param3;
			
			//ESMLog(1, _T("[#NETWORK] ParsePacketClient"));

			char* path = new char[sizeof(RCP_FileSave_Info)];
			CString *pPath = new CString;
			memcpy(path, bodybuf, sizeof(RCP_FileSave_Info));
			pPath->Format(_T("%s"),ESMUtil::CharToCString(path));
			pMsg->pDest			= (LPARAM) pPath;

			if(path)
				delete path;

			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		}
		break;
	case RCPCMD_REQUEST_DATA_TO4DP:
		{
			ESMLog(1, _T("[#NETWORK] RCPCMD_REQUEST_DATA_TO4DP"));
			//Save Data
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_MAKEMOVIE_REQUEST_DATA_SEND_FINISH;	
			pMsg->pParam      = (LPARAM) bodybuf;

			pMsg->nParam2 = bodysize;

			char* path = new char[sizeof(RCP_FileSave_Info)];
			CString *pPath = new CString;
			memcpy(path, bodybuf, sizeof(RCP_FileSave_Info));
			pPath->Format(_T("%s"),ESMUtil::CharToCString(path));
			pMsg->pDest			= (LPARAM) pPath;

			if(path)
				delete path;

			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		}
		break;
	case RCPCMD_TICKCOUNT_GET:
		SyncTickCount(FALSE);
		break;
	case RCPCMD_CONNECT_CHECK:
		{
			int nReceiveTime = ESMGetTick();
			//ESMLog(5,_T("Receive RCPCMD_CONNECT_CHECK [ESMGetTick():%d]"), nReceiveTime);

			//ESMSetFrameRate(pHeader->param2);	//jhhan 170713
			//180308 카메라타입 및 해상도 타입 동시전송
			ESMSetIdentify(pHeader->param2);

			ESMLog(5, _T("ESMSetFrameRate : %d"), ESMGetFrameRate());
			
			if(pHeader->param3 >= 0)
			{
				ESMLog(5, _T("ESMSetStoredTime : %d"), pHeader->param3);
				//jhhan 180822
				ESMSetStoredTime(pHeader->param3);
			}
			
			char buf[1024] = {0};
			RCPHeader* packetheader = (RCPHeader*)buf;

			strcpy(packetheader->protocol, "RCP1.0");
			packetheader->command   = RCPCMD_CONNECT_CHECK;
			packetheader->param1	= nReceiveTime;
			packetheader->bodysize  = 0;

			int length = sizeof(RCPHeader);	
			for( int i =0 ; i< MAXSOCKET; i++)
			{
				if(m_pClientSocket[i])
					int sent = m_pClientSocket[i]->Send(buf, length);
			}
			//int sent = m_pTCPSocket->Send(buf, length);
		}
		break;
	case RCPCMD_ITEM:
		SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_CHECK);
		break;
	case RCPCMD_FORMAT:
		SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_FORMAT_ALL, pHeader->param1, pHeader->param2);
		break;
	case RCPCMD_FWUPDATE:
		SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_FW_UPDATE, pHeader->param1, pHeader->param2);
		break;
	case RCPCMD_MOVIEMAKINGSTOP:
		SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_MOVIEMAKINGSTOP, pHeader->param1, pHeader->param2);
		break;
	case RCPCMD_CAPTURERESUME:
		SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_CAPTURERESUME, pHeader->param1, pHeader->param2);
		break;
	case RCPCMD_CAMSHUTDOWN:
		SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_CAMSHUTDOWN, pHeader->param1, pHeader->param2);
		break;
	case RCPCMD_INITMOVIE_TOCLIENT:
		SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_INITMOVIEFILE, pHeader->param1, pHeader->param2);
		break;
	case RCPCMD_VIEWLARGE:
		SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_VIEWLARGE, pHeader->param1, pHeader->param2);
		break;	
	case RCPCMD_SET_ENLARGE_TOCLIENT:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_SET_ENLARGE_TOCLIENT, pHeader->param1, 0, 0, NULL, strID);
		}
		break;
	case RCPCMD_HALF_SHUTTER_TOCLIENT:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_HALF_SHUTTER_TOCLIENT, pHeader->param1, pHeader->param2, 0, NULL, strID);
		}
		break;
	case RCPCMD_GETFOCUS_TOCLIENT:
		//CMiLRe 20160204 Focus All Setting 시 Focus 저장되는 버그 수정
		SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_GETFOCUS_TOCLIENT, pHeader->param1, pHeader->param2, pHeader->param3);
		break;
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	case RCPCMD_GETFOCUS_TOCLIENT_READ_ONLY:
		SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_GETFOCUS_TOCLIENT_READ_ONLY, pHeader->param1, pHeader->param2);
		break;		
	case RCPCMD_SETFOCUS_TOCLIENT:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_SETFOCUS_TOCLIENT, pHeader->param1, pHeader->param2, 0, NULL, strID);
		}		
		break;
	case RCPCMD_CAPTURE:
		{
			if(pHeader->param1 == WM_RS_MC_CAPTURE_IMAGE_REQUIRE_5 || pHeader->param1 == WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2)
			{
				//joonho.kim 
				ESMSetNextRecordStatus(pHeader->param3);

				m_pFileBody = (RCP_FileSave_Info*)bodybuf;
				CString strName = ESMUtil::CharToCString(m_pFileBody->szFileName);
				ESMSetFrameRecord(strName);

				//jhhan 16-09-23
				ESMSetExecuteMode(TRUE);

				//hjcho 181003
				if(ESMGetValue(ESM_VALUE_RTSP))
				{
					CString strSavePath;
					strSavePath.Format(_T("%s\\%s\\"),ESMGetPath(ESM_PATH_MOVIE_FILE),strName);
					ESMLog(5,strSavePath+_T("++++++++++++++++++"));
					ESMSetRTSPMovieSavePath(strSavePath);
				}
			}

			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));

			SendMessageESM(WM_ESM_DSC, WM_RS_RC_CAPTURE_IMAGE_REQUIRE, pHeader->param1, pHeader->param2, 0, NULL, strID);
		}
		break;
	case RCPCMD_CAPTURE_CANCEL:
		{
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_CAPTURE_CANCEL, pHeader->param1, pHeader->param2);
			break;
		}		
	case RCPCMD_LIVEVIEW:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_LIVEVIEW, pHeader->param1, 0U, 0U, NULL, strID);
		}
		break;
	case RCPCMD_PROPERTY_GET:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_GET_PROPERTY, pHeader->param1, 0U, 0U, NULL, strID);
		}
		break;
	case RCPCMD_PROPERTY_SET:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			pPropBody = (RCP_Property_Info*)bodybuf;
			if(pHeader->param2 == PTP_VALUE_STRING)
			{
				m_strProp = ESMUtil::CharToCString(pPropBody->szProp);
				
				char* pData = new char[MAX_PATH];

				TCHAR ddd[256]; 
				_stprintf(ddd, m_strProp);//유니코드
				memcpy(pData, ddd, MAX_PATH);

				/*if(pHeader->param1 == 0x5003)
				{
					TRACE(_T("IMAGE_SIZE Test"));
				}*/
#if 1  //IMAGE_SIZE Test
				SendMessageESM(WM_ESM_DSC, WM_RS_RC_SET_PROPERTY, pHeader->param1, pHeader->param2, 0U, pData,strID);
#else
				SendMessageESM(WM_ESM_DSC, WM_RS_RC_SET_PROPERTY, pHeader->param1, pHeader->param2, 0U, (char*)(LPCTSTR)m_strProp,strID);
#endif
			}
			else
				SendMessageESM(WM_ESM_DSC, WM_RS_RC_SET_PROPERTY, pHeader->param1, pHeader->param2, 0U, (char*)pPropBody->nProp,strID);
		}
		break;
	case RCPCMD_PROPERTY_SET_ALL:
		{
			pPropBody = (RCP_Property_Info*)bodybuf;
			if(pHeader->param2 == PTP_VALUE_STRING)
			{
				m_strProp = ESMUtil::CharToCString(pPropBody->szProp);
				SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_SET_GROUP_PROPERTY, pHeader->param1, pHeader->param2, 0U, (char*)(LPCTSTR)m_strProp);
			}
			else
				SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_SET_GROUP_PROPERTY, pHeader->param1, pHeader->param2, 0U, (char*)pPropBody->nProp);
		}
		break;
	case RCPCMD_HIDDEN_COMMAND:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_SET_PROPERTY, pHeader->param1, pHeader->param2, 0U, NULL, strID);
		}
		break;		
	case RCPCMD_SET_FOCUS:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RCPCMD_SET_FOCUS, pHeader->param1, pHeader->param2, 0U, NULL, strID);
		}
		break;	
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	case RCPCMD_SET_FOCUS_VALUE:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RCPCMD_SET_FOCUS_VALUE, pHeader->param1, pHeader->param2, 0U, NULL, strID);
		}
		break;
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	case RCPCMD_GET_FOCUS_ALL:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_ESM_LIST_GETFOCUS_TOCLIENT, pHeader->param1, pHeader->param2, 0U, NULL, strID);
		}
		break;
	case RCPCMD_FOCUS_LOCATE_SAVE:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_FOCUS_LOCATE_SAVE_TOCLIENT, pHeader->param1, pHeader->param2, 0U, NULL, strID);
		}
		break;
	case RCPCMD_SET_FOCUS_FRAME_TOCLIENT:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));			

			SendMessageESM(WM_ESM_LIST, WM_ESM_LIST_SET_FOCUS_FRAME_TOCLIENT, pHeader->param1,pHeader->param2,pHeader->param3,0, strID);
		}
		break;
	case RCPCMD_PRE_REC_INIT_GH5:
		SendMessageESM(WM_ESM_DSC, WM_RS_RC_PRE_REC_INIT_GH5);
		break;
	case RCPCMD_GET_TICK_CMD_TOCLIENT:
		//SendMessageESM(WM_ESM_DSC, WM_RS_RC_GET_TICK_CMD);
		SendMessageESM(WM_ESM_LIST, WM_RS_RC_GET_TICK_CMD);
		break;
	case RCPCMD_CAMERA_SYNC:
		SendMessageESM(WM_ESM_DSC, WM_RS_RC_CAMERA_SYNC);
		break;
	case RCPCMD_MOVIESAVE_CHECK:
		SendMessageESM(WM_ESM_DSC, WM_RS_RC_MOVIESAVE_CHECK, pHeader->param1);
		break;
// 	case RCPCMD_TIMESYNC_SLEEP:
// 		{
// 			if(pHeader->param1)
// 				m_nThreadSleep = 1;
// 			else
// 				m_nThreadSleep = 0;
// 		}
// 		break;
	case RCPCMD_OPTION_ADJ_PATH:
		{
			CString* pStrPath;
			pStrPath = new CString;
			char* pPathData = NULL;
			pPathData = new char[pHeader->bodysize + 1];
			memset(pPathData, 0, pHeader->bodysize + 1);
			memcpy(pPathData, bodybuf, bodysize);

			*pStrPath = CA2T(pPathData);
			SendMessageESM(WM_ESM_OPT, WM_OPTION_ADJ_SETPATH, pHeader->param1, pHeader->param2, 0U, (char*)pStrPath);
		}
		break;
	case RCPCMD_OPTION_MAN_COPY_PATH:
		{
			SendMessageESM(WM_ESM_OPT, WM_OPTION_MAN_SETCOPY, pHeader->param1, pHeader->param2, 0U, (char*)bodybuf);
		}
		break;
	case RCPCMD_OPTION_MAN_DEL_PATH:
		{
			SendMessageESM(WM_ESM_OPT, WM_OPTION_MAN_SETDEL, pHeader->param1, pHeader->param2, 0U, (char*)bodybuf);
		}
		break;
	
	case RCPCMD_OPTION_PATH :
		{
			CString* pStrPath;
			pStrPath = new CString;
			char* pPathData = NULL;
			pPathData = new char[pHeader->bodysize + 1];
			memset(pPathData, 0, pHeader->bodysize + 1);
			memcpy(pPathData, bodybuf, bodysize);

			*pStrPath = CA2T(pPathData);
			SendMessageESM(WM_ESM_OPT, WM_OPTION_SETPATH, pHeader->param1, pHeader->param2, 0U, (char*)pStrPath);
		}
		break;
	case RCPCMD_BACKUP_STRING:
		{
			CString* pStrPath;
			pStrPath = new CString;
			char* pPathData = NULL;
			pPathData = new char[pHeader->bodysize + 1];
			memset(pPathData, 0, pHeader->bodysize + 1);
			memcpy(pPathData, bodybuf, bodysize);

			*pStrPath = CA2T(pPathData);
			SendMessageESM(WM_ESM_OPT, WM_BACKUP_STRING, pHeader->param1, pHeader->param2, 0U, (char*)pStrPath);
		}
		break;
	case RCPCMD_DELETE_DSC:
		{
			CString* pStrPath;
			pStrPath = new CString;
			char* pPathData = NULL;
			pPathData = new char[pHeader->bodysize + 1];
			memset(pPathData, 0, pHeader->bodysize + 1);
			memcpy(pPathData, bodybuf, bodysize);

			*pStrPath = CA2T(pPathData);
			SendMessageESM(WM_ESM_MOVIE, WM_ESM_MOVIE_DELETE_DSC, pHeader->param1, pHeader->param2, 0U, (char*)pStrPath);
		}
		break;
	case RCPCMD_SET_MAKING_INFO:
		{
			ESMLog(1, _T("######RCPCMD_SET_MAKING_INFO"));
			SendMessageESM(WM_ESM_MOVIE, WM_ESM_MOVIE_MAKING_INFO, pHeader->param1, pHeader->param2, 0U, (char*)bodybuf);
		}
		break;
	case RCPCMD_OPTION_ETC	:
		{
			SendMessageESM(WM_ESM_OPT, WM_OPTION_SETETC, pHeader->param1, pHeader->param2, pHeader->param3, NULL);
		}
		break;
	case RCPCMD_OPTION_SYNC:
		{
			SendMessageESM(WM_ESM_OPT, WM_OPTION_SETSYNC, pHeader->param1, pHeader->param2, pHeader->param3, NULL);
		}
		break;
	case RCPCMD_OPTION_BASE :
		{
			CString* pStrData;
			pStrData = new CString;
			char* pPathData = NULL;
			pPathData = new char[pHeader->bodysize + 1];
			memset(pPathData, 0, pHeader->bodysize + 1);
			memcpy(pPathData, bodybuf, bodysize);

			*pStrData = CA2T(pPathData);
			SendMessageESM(WM_ESM_OPT, WM_OPTION_SETBASE, pHeader->param1, pHeader->param2, pHeader->param3, (char*)pStrData);
		}
		break;

	case RCPCMD_OPTION_MOVIESIZE :
		{
			SendMessageESM(WM_ESM_OPT, WM_OPTION_MOVIESIZE, pHeader->param1, pHeader->param2, pHeader->param3);
		}
		break;
	case RCPCMD_OPTION_MAKE_TYPE:
		{
			SendMessageESM(WM_ESM_OPT, WM_OPTION_MAKE_TYPE, pHeader->param1, pHeader->param2, pHeader->param3);
		}
		break;
	
	//wgkim 170727
	case RCPCMD_OPTION_LIGHT_WEIGHT:
		{
			SendMessageESM(WM_ESM_OPT, WM_OPTION_LIGHT_WEIGHT, pHeader->param1, pHeader->param2,pHeader->param3);
		}
		break;
	case RCPCMD_OPTION_SET_FRAME_RATE:
		{
			SendMessageESM(WM_ESM_OPT, WM_OPTION_SET_FRAMERATE, pHeader->param1,pHeader->param2,pHeader->param3);
		}
		break;
	case RCPCMD_OPTION_RECORD_FILE_SPLIT_TOCLIENT:
		{
			SendMessageESM(WM_ESM_OPT, WM_OPTION_RECORD_FILE_SPLIT, pHeader->param1);
		}
		break;
	case RCPCMD_OPTION_VIEW_INFO:
		{
			SendMessageESM(WM_ESM_OPT, WM_OPTION_VIEW_INFO, pHeader->param1, pHeader->param2, pHeader->param3);
		}
		break;
	case RCPCMD_OPTION_AUTOADJUST_KZONE:
		{
			SendMessageESM(WM_ESM_OPT, WM_OPTION_SET_AUTOADJUST_KZONE, pHeader->param1);
		}
		break;
#ifdef _4DMODEL
		//-- 2015-02-15 cygil@esmlab.com
		//-- Add picture coutdown info 전송
	case RCPCMD_OPTION_COUNTDOWN :
		SendMessageESM(WM_ESM_OPT, WM_OPTION_SETCOUNTDOWN, pHeader->param1);
		break;
#endif
	case RCPCMD_SET_ADJUSTINFO :
		{
			ESMLog(5, _T("RCPCMD_SET_ADJUSTINFO"));
			UINT nSize = 0;
			size_t nSizeUint = sizeof(UINT);
			UINT nSizeAdj = sizeof(_RCP_ADJUST_INFO);
			memcpy(&nSize, bodybuf, sizeof(UINT));
			char* pLoc = bodybuf + sizeof(UINT);

			for ( int i=0 ; i<nSize ; i++ )
			{
				UINT nSizeDsc=0;
				memcpy(&nSizeDsc, pLoc, nSizeUint);
				pLoc = pLoc + nSizeUint;

				char *cDSC = new char[nSizeDsc + 1];
				memset(cDSC, 0, nSizeDsc + 1);
				memcpy(cDSC, pLoc, nSizeDsc);
				pLoc = pLoc + nSizeDsc;

				CString strDSC;
				strDSC = CA2T(cDSC);
				delete[] cDSC;

				_RCP_ADJUST_INFO pAdjInfoRecv;
				memcpy(&pAdjInfoRecv, pLoc, nSizeAdj);
				pLoc = pLoc + nSizeAdj;

// 				CObArray arDSCList;
// 				ESMGetDSCList(&arDSCList);

// 				CDSCItem* pItem = NULL;
// 				for( int j =0 ;j < arDSCList.GetSize(); j++)
// 				{
// 					pItem = (CDSCItem*)arDSCList.GetAt(j);
// 
// 					if ( strDSC == pItem->GetDeviceDSCID())
// 					{
				stAdjustInfo* pAdjInfo = new stAdjustInfo;
				pAdjInfo ->AdjAngle = pAdjInfoRecv.AdjAngle;
				pAdjInfo ->AdjMove.x = pAdjInfoRecv.AdjMove.x;
				pAdjInfo ->AdjMove.y = pAdjInfoRecv.AdjMove.y;
				pAdjInfo ->AdjptRotate.x = pAdjInfoRecv.AdjptRotate.x;
				pAdjInfo ->AdjptRotate.y = pAdjInfoRecv.AdjptRotate.y;
				pAdjInfo->AdjSize= pAdjInfoRecv.AdjSize;
				pAdjInfo->nHeight = pAdjInfoRecv.nHeight;
				pAdjInfo->nWidth = pAdjInfoRecv.nWidth;
				pAdjInfo->strDSC = strDSC;
				pAdjInfo->stMargin = pAdjInfoRecv.stMargin;
				pAdjInfo->rtMargin = pAdjInfoRecv.rtMargin;

				ESMEvent* pMsg = new ESMEvent;
				//pMsg->nParam1 = j;
				pMsg->message = WM_ESM_MOVIE_SET_ADJUSTINFO_FROM_SERVER;
				pMsg->pParam = (LPARAM)pAdjInfo;

				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
// 					}
// 				}
			}
		}
		break;
	case RCPCMD_MAKEMOVIE_TOCLIENT:
		{
			// TimeLine size + ( MakeMovieInfo + EffectInfo Size + ( EffectInfo ) * EffectInfo Size ) * TimeLine size 
			ESMLog(5, _T("Recieve Make Movie "));
			vector<TimeLineInfo>* arrTimeLineInfo;
			arrTimeLineInfo = new vector<TimeLineInfo>;
			

			int nDataSize = 0;
			memcpy(&nDataSize, bodybuf, sizeof(int));
			bodybuf = bodybuf + sizeof(int);

			MakeMovieInfo pMovieData;
			for(int i =0 ;i < nDataSize; i++)
			{
				TimeLineInfo stTimeLineInfo;
				memcpy(&pMovieData, bodybuf, sizeof(MakeMovieInfo));
				stTimeLineInfo.stMovieData = pMovieData;
				bodybuf += sizeof(MakeMovieInfo);

				int nFrameSize = 0;
				memcpy(&nFrameSize, bodybuf, sizeof(int));
				bodybuf = bodybuf + sizeof(int);

				EffectInfo stEffectInfo;
				for ( int j = 0 ; j < nFrameSize ; j++ )
				{

					memcpy(&stEffectInfo, bodybuf, sizeof(EffectInfo));
					stTimeLineInfo.arrEffectData.push_back(stEffectInfo);
					bodybuf += sizeof(EffectInfo);
				}
				
				arrTimeLineInfo->push_back(stTimeLineInfo);
			}
			//Movie Movie Mgr에 영상 제작 정보 전달.
			SendMessageESM(WM_ESM_MOVIE, WM_ESM_MOVIE_MAKEMOVIE, pHeader->param1, pHeader->param2, 0U, (char*)arrTimeLineInfo);
		}
		break;
	case RCPCMD_REQUEST_DATA:
		{
			vector<MakeFrameInfo>* pArrMovieInfo = new vector<MakeFrameInfo>;
			for(int i =0 ;i < pHeader->param1 ; i++)
			{
				MakeFrameInfo stMakeFrameInfo;
				memcpy(&stMakeFrameInfo, bodybuf, sizeof(MakeFrameInfo));
				bodybuf += sizeof(MakeFrameInfo);

				pArrMovieInfo->push_back(stMakeFrameInfo);
			}

			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_MAKEMOVIE_REQUES_DATA;	
			pMsg->nParam1	= (LPARAM)pHeader->param1;
			pMsg->nParam2	= (LPARAM)pHeader->param2;	
			pMsg->nParam3	= (LPARAM)pHeader->param3;
			pMsg->pParam	= (LPARAM)pArrMovieInfo;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		}
		break;
	case RCPCMD_MAKEFRAMEMOVIE_TOCLIENT:
		{
			vector<MakeFrameInfo>* pArrMovieInfo = new vector<MakeFrameInfo>;
			//pArrMovieInfo->at(0).strTime;
			for(int i =0 ;i < pHeader->param1 ; i++)
			{
				MakeFrameInfo stMakeFrameInfo;
				memcpy(&stMakeFrameInfo, bodybuf, sizeof(MakeFrameInfo));
				bodybuf += sizeof(MakeFrameInfo);

				pArrMovieInfo->push_back(stMakeFrameInfo);
			}

			//SendMessageESM(WM_ESM_VIEW, WM_MAKEMOVIE_DP_FRAME, pHeader->param1, pHeader->param2, 0U, (char*)pArrMovieInfo);
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_MAKEMOVIE_DP_FRAME;	
			pMsg->nParam1	= (LPARAM)pHeader->param1;
			pMsg->nParam2	= (LPARAM)pHeader->param2;	
			pMsg->nParam3	= (LPARAM)pHeader->param3;
			pMsg->pParam	= (LPARAM)pArrMovieInfo;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		}
		break;
	case RCPCMD_SET_MARGIN:
		{
			int nMarginX;
			int nMarginY;

			memcpy(&nMarginX, bodybuf, sizeof(int));
			bodybuf = bodybuf + sizeof(int);
			memcpy(&nMarginY, bodybuf, sizeof(int));

			SendMessageESM(WM_ESM_MOVIE, WM_ESM_MOVIE_SET_MARGIN_FROM_SERVER, pHeader->param1, pHeader->param2);
		}
		break;

	case RCPCMD_CAPTUREDELAY_TOCLIENT:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			SendMessageESM(WM_ESM_DSC, WM_RS_RC_CAPTUREDELAY, pHeader->param1, pHeader->param2, pHeader->param3, NULL, strID);
		}
		break;
	case RCPCMD_GET_ITEMFOCUS_TOCLIENT:
		{
			CString strID;
			strID.Format(_T("%s"),ESMUtil::CharToCString(pHeader->Camera));
			//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
			SendMessageESM(WM_ESM_DSC, WM_ESM_LIST_GETITEMFOCUS, pHeader->param1, 0U, 0U, NULL, strID);
			break;
		}
	case RCPCMD_CONNECT_STATUS_CHECK:
		{
			WORD wVersionRequested;
			WSADATA wsaData;
			char name[255];
			CString ip; 
			PHOSTENT hostinfo;
			wVersionRequested = MAKEWORD( 2, 0 );
			if ( WSAStartup( wVersionRequested, &wsaData ) == 0 ) 
			{
				if( gethostname ( name, sizeof(name)) == 0)
				{
					if((hostinfo = gethostbyname(name)) != NULL)
					{
						ip = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
					}
				}      
				WSACleanup( );
			} 
			CString ipStr;
			AfxExtractSubString( ipStr, ip, 3, '.');

			int ipInt = _ttoi(ipStr);	

			SendMessageESM(WM_ESM_NET, WM_ESM_NET_CONNECT_STATUS_CHECK,ipInt);

		}
		break;
	case RCPCMD_GET_DISKINFO:
		{
			ULARGE_INTEGER FreeByteAvailable;
			ULARGE_INTEGER unTotalSize;
			ULARGE_INTEGER unFreeSize;

			CString strPath = ESMGetPath(ESM_PATH_LOCALMOVIE).Left(2);
			GetDiskFreeSpaceEx(strPath ,&FreeByteAvailable, &unTotalSize, &unFreeSize);
			//GetDiskFreeSpaceEx(TEXT("C:\\") ,&FreeByteAvailable, &unTotalSize, &unFreeSize);

			LONGLONG nTotalSize = (UINT)((unTotalSize.QuadPart)/1024/1024/1024);
			LONGLONG nFreeSize = nTotalSize - ((UINT)((unFreeSize.QuadPart)/1024/1024/1024));
			LONGLONG bGpu	= ESMGetGPUDecodeSupport();
			LONGLONG nNetSpeed	= ESMUtil::GetLocalNetSpeed();
			LONGLONG nState = 0;

			if(nNetSpeed < 1000)
			{
				if(!bGpu)
					nState = 0;
				else
					nState = 1;
			}
			else
			{
				if(!bGpu)
					nState = 2;
				else
					nState = 3;
			}

			SendMessageESM(WM_ESM_NET, WM_ESM_NET_GET_DSIKSIZE, nTotalSize, nFreeSize, nState);

			TRACE(_T("[disk size]  Total disk size : %d\n") ,nTotalSize);
			TRACE(_T("[disk size]  Free disk size : %d\n") ,nFreeSize);
		}
		break;
		//jhhan 16-09-23
	case RCPCMD_SET_FRAME_RECORD:
		{
			m_pFileBody = (RCP_FileSave_Info*)bodybuf;
			CString strName = ESMUtil::CharToCString(m_pFileBody->szFileName);
			ESMSetFrameRecord(strName);
			ESMLog(5, _T("ESMSetFrameRecord : %s"), strName);
			ESMSetExecuteMode(FALSE);
		}
		break;
	case RCPCMD_GET_STARTSENDFRAME:
		{
			m_pFileBody = (RCP_FileSave_Info*)bodybuf;
			CString strName = ESMUtil::CharToCString(m_pFileBody->szFileName);
			ConnectToKTServer(strName,(int)pHeader->param1);
			ESMLog(5, _T("Start Send Frame Socket : %s"), strName);
		}
		break;
	case RCPCMD_GET_STOPSENDFRAME:
		{
			DisConnectToKTServer();
			ESMLog(5, _T("Stop Send Frame Socket"));
		}
		break;
	case RCPCMD_RUN_IPERF:
		{
			CString strCmd,strOpt;
			strCmd.Format(_T("%s\\bin\\iperf3.exe"),ESMGetPath(ESM_PATH_HOME));
			strOpt.Format(_T("-s"));

			SHELLEXECUTEINFO lpExecInfo;
			lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
			lpExecInfo.lpFile = strCmd;
			lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
			lpExecInfo.hwnd = NULL;  
			lpExecInfo.lpVerb = L"open";
			lpExecInfo.lpParameters = strOpt;
			lpExecInfo.lpDirectory = NULL;
			lpExecInfo.nShow	=SW_SHOW;
			//lpExecInfo.nShow = SW_HIDE; // hide shell during execution
			lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;

			BOOL bState = ShellExecuteEx(&lpExecInfo);

			// wait until the process is finished
			if (lpExecInfo.hProcess != NULL)
			{
				::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
				::CloseHandle(lpExecInfo.hProcess);
			}
		}
		break;
	case RCPCMD_DUMP_PROCDOWN:
		{
			ESMUtil::KillProcess(_T("procdump64.exe"));
			ESMLog(5, _T("Dump Proc Down"));
		}
		break;
	case RCPCMD_MUX_SENDDATA:
		{
			ESMLog(5, _T("Mux DP Data"));

			MuxDataInfo* pMuxDataInfo = new MuxDataInfo;
			memcpy(pMuxDataInfo, bodybuf, sizeof(MuxDataInfo));

			_RCP_ADJUST_INFO rcpAdj;
			memcpy(&rcpAdj,bodybuf+sizeof(MuxDataInfo),sizeof(_RCP_ADJUST_INFO));

			pMuxDataInfo->pAdjInfo = new stAdjustInfo;

			pMuxDataInfo->pAdjInfo->strDSC.Format(_T("%s"),pMuxDataInfo->strCamID);
			pMuxDataInfo->pAdjInfo->AdjAngle = rcpAdj.AdjAngle;
			pMuxDataInfo->pAdjInfo->AdjSize   = rcpAdj.AdjSize;
			pMuxDataInfo->pAdjInfo->AdjptRotate.x = rcpAdj.AdjptRotate.x;
			pMuxDataInfo->pAdjInfo->AdjptRotate.y = rcpAdj.AdjptRotate.y;
			pMuxDataInfo->pAdjInfo->AdjMove.x = rcpAdj.AdjMove.x;
			pMuxDataInfo->pAdjInfo->AdjMove.y = rcpAdj.AdjMove.y;
			pMuxDataInfo->pAdjInfo->nWidth	 = rcpAdj.nWidth;
			pMuxDataInfo->pAdjInfo->nHeight	 = rcpAdj.nHeight;
			pMuxDataInfo->pAdjInfo->stMargin = rcpAdj.stMargin;

			//ESMLog(5,_T("REVERSE: %d"),pHeader->param1);
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->pParam	= (LPARAM)pMuxDataInfo;
			pMsg->nParam1	= pHeader->param1;
			pMsg->message	= WM_ESM_MOVIE_MUX;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}
		break;
	case RCPCMD_PROPERTY_1STEP:
		{
			ESMSetProperty1Step((int)pHeader->param1);
		}
		break;
	case RCPCMD_SEND_KTSENDTIME:
		{
			ESMSetKTSendSyncTime((int)pHeader->param1);
		}
		break;
	case RCPCMD_SEND_KTSTOPTIME:
		{
			ESMSetKTSendStopTime((int)pHeader->param1);
		}
		break;
	case RCPCMD_REMOTE_START:
	case RCPCMD_REMOTE_STOP:
	case RCPCMD_REMOTE_MAKE:
		{
			ESMEvent* pMsg = new ESMEvent;
			
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}
		break;
	case RCPCMD_BACKUP_START:
		{
			BOOL bUseDeleteAfterTransfer = pHeader->param2;
			BOOL bUseFolder = pHeader->param3;

			m_pBackupMgr = (CESMBackup*)ESMGetBackupMgr();
			if(m_pBackupMgr == NULL)
				break;

			m_pBackupMgr->SetUseFolderList(bUseFolder);
			m_pBackupMgr->SetUseDeleteAfterTransfer(bUseDeleteAfterTransfer);

			m_pBackupMgr->SetStrLocalRootPath(ESMGetBackupString(ESM_BACKUP_STRING_SRCFOLDER));
			m_pBackupMgr->SetStrFtpRootPath(ESMGetBackupString(ESM_BACKUP_STRING_DSTFOLDER));
			m_pBackupMgr->SetFTPInfo(
				ESMGetBackupString(ESM_BACKUP_STRING_FTPHOST), _T("21"),
				ESMGetBackupString(ESM_BACKUP_STRING_FTPID),
				ESMGetBackupString(ESM_BACKUP_STRING_FTPPW));
			m_pBackupMgr->BackupByFtp();
			
			char buf[1024] = {0};
			RCPHeader* packetheader = (RCPHeader*)buf;

			strcpy(packetheader->protocol, "RCP1.0");
			packetheader->command   = RCPCMD_BACKUP_START;
			packetheader->param1	= m_pBackupMgr->GetFileCount();			
			packetheader->bodysize  = 0;

			int length = sizeof(RCPHeader);	
			for( int i =0 ; i< MAXSOCKET; i++)
			{
				if(m_pClientSocket[i])
					int sent = m_pClientSocket[i]->Send(buf, length);
			}			
		}
		break;
	case RCPCMD_BACKUP_END:
		{
			m_pBackupMgr = (CESMBackup*)ESMGetBackupMgr();
			if(m_pBackupMgr == NULL)
				break;
			m_pBackupMgr->CloseFtp();

			char buf[1024] = {0};
			RCPHeader* packetheader = (RCPHeader*)buf;

			strcpy(packetheader->protocol, "RCP1.0");
			packetheader->command   = RCPCMD_BACKUP_END;
			packetheader->param1	= 0;
			packetheader->bodysize  = 0;

			int length = sizeof(RCPHeader);	
			for( int i =0 ; i< MAXSOCKET; i++)
			{
				if(m_pClientSocket[i])
					int sent = m_pClientSocket[i]->Send(buf, length);
			}			
		}
		break;
	case RCPCMD_BACKUP_FILECOUNT_CURRENT:
		{
			m_pBackupMgr = (CESMBackup*)ESMGetBackupMgr();
			if(m_pBackupMgr == NULL)
				break;
			
 			int ftpState = m_pBackupMgr->GetBackupFtpState();
 			int nFinish = 0;
 			if (ftpState == (m_pBackupMgr->BACKUP_FTP_PROCESS_STATE_END))
			{
 				nFinish = 1;
			}

			char buf[1024] = {0};
			RCPHeader* packetheader = (RCPHeader*)buf;

			strcpy(packetheader->protocol, "RCP1.0");
			packetheader->command   = RCPCMD_BACKUP_FILECOUNT_CURRENT;
			packetheader->param1	= m_pBackupMgr->m_arrBFileUploadCheck.size();;
			packetheader->param2	= nFinish;
			packetheader->bodysize  = 0;

			int length = sizeof(RCPHeader);	
			for( int i =0 ; i< MAXSOCKET; i++)
			{
				if(m_pClientSocket[i])
					int sent = m_pClientSocket[i]->Send(buf, length);
			}			
		}
		break;
	case RCPCMD_BACKUP_DIRECT_DELETE:
		{
			m_pBackupMgr = (CESMBackup*)ESMGetBackupMgr();
			m_pBackupMgr->SetStrLocalRootPath(ESMGetBackupString(ESM_BACKUP_STRING_SRCFOLDER));
			if(m_pBackupMgr == NULL)
				break;

			m_pBackupMgr->DeleteSelectList();

		}
		break;
	case RCPCMD_RUN_IPERF_OFF:
		{
			ESMUtil::KillProcess(_T("iperf3.exe"));
		}
		break;
//	case RCPCMD_BACKUP_FILECOUNT_ALL:
//		{
////			int nReceiveTime = ESMGetTick();
//			//ESMLog(5,_T("Receive RCPCMD_CONNECT_CHECK [ESMGetTick():%d]"), nReceiveTime);
//
////			ESMSetFrameRate(pHeader->param2);
////			ESMLog(5, _T("ESMBackupCheck: %d"), pHeader->param2);
//			pHeader->param2 = m_pBackupMgr->GetFileCount();
//
//			char buf[1024] = {0};
//			RCPHeader* packetheader = (RCPHeader*)buf;
//
//			strcpy(packetheader->protocol, "RCP1.0");
//			packetheader->command   = RCPCMD_BACKUP_FILECOUNT_ALL;
//			packetheader->param1	= m_pBackupMgr->GetFileCount();
//			packetheader->bodysize  = 0;
//
//			int length = sizeof(RCPHeader);	
//			for( int i =0 ; i< MAXSOCKET; i++)
//			{
//				if(m_pClientSocket[i])
//				{
//					int sent = m_pClientSocket[i]->Send(buf, length);
//					if(SOCKET_ERROR == sent)
//					{
//						ESMLog(5, _T("ESMBackupConnectCheck Error"));
//					}
//				}
//			}			
//		}
//		break;
	case RCPCMD_REFEREE_REQUEST_TO_SERVER:
		{
			//jhhan referee
			//시간ms,Type-HD,UHD, temp
			//pHeader->param1;
			//pHeader->param2;
			//pHeader->param3;
			ESMLog(5, _T("RCPCMD_REFEREE_REQUEST_TO_SERVER : %d, %d, %d"),pHeader->param1, pHeader->param2, pHeader->param3);
			
			ESMEvent* pMsg = new ESMEvent;
			pMsg->nParam2  = pHeader->param2;
			pMsg->nParam3  = pHeader->param3;

			//pHeader->param1 : message
			switch(pHeader->param1)
			{
			case RCPCMD_REFEREE_MUX:
				{
					//pHeader->param2 : Movie Index
					//pHeader->param3 : Frame Index
					pMsg->message  = WM_MAKEMOVIE_REFEREE_MUXSTART;
				}
				break;
			case RCPCMD_REFEREE_ORIGIN:
				{
					//pHeader->param2 : Camera Index
					//pHeader->param3 : Frame Index
					pMsg->message  = WM_MAKEMOVIE_REFEREE_4KMOVIE;
				}
				break;
			default:
				{
					if(pMsg)
					{
						delete pMsg;
						pMsg = NULL;
					}
					return;
				}
				break;
			}
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		}
		break;
	case RCPCMD_DELETE_FILE:
		{
			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_ESM_DELETE_FILE;
			m_pFileBody = (RCP_FileSave_Info*)bodybuf;
			CString strName = ESMUtil::CharToCString(m_pFileBody->szFileName);
			ESMLog(5, _T("RCPCMD_DELETE_FILE : %s"), strName.Trim());
			ESMSetDeleteFolder(strName);
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}
		break;
	case RCPCMD_SEND_KZONE_POINTS:
		{
			ESMLog(5, _T("[K-Zone]K_Zone Data Receive from 4DC."));

			int nDscCount = 0;
			int nSizeInt = sizeof(int);
			memcpy(&nDscCount, bodybuf, nSizeInt);
			char* pLoc = bodybuf + nSizeInt;

			int nSizeKZoneData = sizeof(KZoneDataInfo)*nDscCount;
			ESMLog(5, _T("[K-Zone]Data Size."));
			
//			KZoneDataInfo *pKZoneDataInfo = new KZoneDataInfo[nDscCount];
			if(m_pKZoneDataInfo != NULL)
				delete[] m_pKZoneDataInfo;
			m_pKZoneDataInfo = new KZoneDataInfo[nDscCount];
			memcpy(m_pKZoneDataInfo, pLoc, nSizeKZoneData);
			ESMLog(5, _T("[K-Zone]memcpy."));

			ESMEvent* pKZoneMsg	= new ESMEvent();
			pKZoneMsg->message		= WM_ESM_MOVIE_SET_KZONE_POINTS;
			pKZoneMsg->nParam1		= nDscCount;
			pKZoneMsg->pParam		= (LPARAM)m_pKZoneDataInfo;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pKZoneMsg);
			ESMLog(5, _T("[K-Zone]Data Send to ESMMovie."));
		}
		break;
	case RCPCMD_SEND_KZONE_JUDGMENT:
	{
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_MOVIE_SET_KZONE_JUDGMENT;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	}
	break;
	case RCPCMD_DETECT_INFO:
		{
			//jhhan 181126
			vector<DetectInfo>* pDetectInfo = new vector<DetectInfo>;
			int nTime = 0;
			nTime = pHeader->param2;
			for(int i = 0 ; i < pHeader->param1 ; i++)
			{
				DetectInfo stDetect;
				memcpy(&stDetect, bodybuf, sizeof(DetectInfo));
				bodybuf += sizeof(DetectInfo);
				//ESMLog(5, _T("DETECT_INFO [%s] - %d"),stDetect._strId, nTime);
				pDetectInfo->push_back(stDetect);
			}
			ESMEvent *pEvent = new ESMEvent();
			pEvent->message = WM_ESM_DETECT_CMD;
			pEvent->nParam1 = nTime;		//Detect Time
			pEvent->pParam = (LPARAM) pDetectInfo;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pEvent);
		}
		break;
	case RCPCMD_MULTIVIEW_MAKING:
		{
			MakeMultiView* pMultiViewData = new MakeMultiView;
			memcpy(pMultiViewData, bodybuf, sizeof(MakeMultiView));

			ESMEvent *pEvent = new ESMEvent();
			pEvent->message = WM_ESM_MOVIE_MULTIVIEW_MAKING;
			pEvent->pParam = (LPARAM) pMultiViewData;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pEvent);
		}
		break;
	case RCPCMD_REFEREE_LIVE:
		{
			CString* pStrPath;
			pStrPath = new CString;
			char* pPathData = NULL;
			pPathData = new char[pHeader->bodysize + 1];
			memset(pPathData, 0, pHeader->bodysize + 1);
			memcpy(pPathData, bodybuf, bodysize);

			*pStrPath = CA2T(pPathData);
			SendMessageESM(WM_ESM_REFEREE, WM_ESM_REFEREE_LIVE, pHeader->param1, pHeader->param2, 0U, (char*)pStrPath);
		}
		break;
	case RCPCMD_REFEREE_SELECT_TO_CLIENT:
		{
			CString* pStrPath;
			pStrPath = new CString;
			char* pPathData = NULL;
			pPathData = new char[pHeader->bodysize + 1];
			memset(pPathData, 0, pHeader->bodysize + 1);
			memcpy(pPathData, bodybuf, bodysize);

			*pStrPath = CA2T(pPathData);
			//jhhan 181126
			ESMLog(5, _T("-----Referee Select to client---- [%d] [%d] [%d] [%s]"), pHeader->param1, pHeader->param2, pHeader->param3, *pStrPath);
			SendMessageESM(WM_ESM_REFEREE, WM_ESM_REFEREE_MAKE_SELECT, pHeader->param1, pHeader->param2, 0U, (char*)pStrPath);
		}
		break;
	case RCPCMD_REFEREE_SET_INFO:
		{

		}
		break;

	case RCPCMD_REFEREE_SELECT:
		{
			//jhhan 181126
			ESMLog(5, _T("-----Referee Select---- [%d] [%d] [%d]"), pHeader->param1, pHeader->param2, pHeader->param3);
			
			ESMEvent* pMsg = new ESMEvent();
			pMsg->message = WM_ESM_REFEREE_SELECT;
			pMsg->nParam1 = pHeader->param1;
			pMsg->nParam2 = pHeader->param2;
			pMsg->nParam3 = pHeader->param3;
				
			::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_REFEREE, (LPARAM)pMsg);
		}
		break;
	case RCPCMD_REFEREE_RECORD:
		{
			ESMLog(5, _T("-----Referee Record---- [%d] [%d] [%d]"), pHeader->param1, pHeader->param2, pHeader->param3);
			
			ESMEvent *pMsg = new ESMEvent();
			pMsg->message = WM_ESM_REFEREE_RECORD;
			pMsg->nParam1 = pHeader->param1;
			pMsg->nParam2 = pHeader->param2;
			pMsg->nParam3 = pHeader->param3;
			::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_REFEREE, (LPARAM)pMsg);
		}
		break;
	case RCPCMD_REFEREE_GET_INFO:
		{
			ESMLog(5, _T("-----Referee Get Info---- [%d] [%d] [%d]"), pHeader->param1, pHeader->param2, pHeader->param3);

			ESMEvent *pMsg = new ESMEvent();
			pMsg->message = WM_ESM_REFEREE_INFO;
			pMsg->nParam1 = pHeader->param1;
			pMsg->nParam2 = pHeader->param2;
			pMsg->nParam3 = pHeader->param3;
			::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_REFEREE, (LPARAM)pMsg);
		}
		break;
	default:
		break;
	}
}

float CESMRCManager::OrderFloat(float fVal)
{
	DWORD *puifVal = (DWORD*)&fVal;
	*puifVal = *puifVal;
	return fVal;
}

//------------------------------------------------------------------------------ 
//! @brief    
//! @date     2013-09-28
//! @owner    yongmin
//! @note     [IN] bodybuf  : 
//!           [IN] bodysize :
//! @return        
//! @revision 
//------------------------------------------------------------------------------
void CESMRCManager::SendMessageESM(UINT pWParam, UINT message,  UINT nParam1, UINT nParam2, UINT nParam3, char* pParam, CString strID)
{
	/*if(nParam1 == 0x5003)
	{
		TRACE(_T("IMAGE_SIZE Test"));
	}*/

	HWND m_hMainWnd = ESMGetMainWnd();
	ESMEvent* pMsg	= new ESMEvent;

	pMsg->message	= message;	
	pMsg->nParam1	= (LPARAM)nParam1;
	pMsg->nParam2	= (LPARAM)nParam2;	
	pMsg->nParam3	= (LPARAM)nParam3;	
	pMsg->pParam	= (LPARAM)pParam;

	//-- 2014-09-09 hongsu@esmlab.com
	//-- Check DSC ID
	if(strID != _T(""))
	{
		CString* pStr = new CString;
		pStr->Format(_T("%s"),strID);
		pMsg->pDest		= (LPARAM)pStr;
	}
	else
		pMsg->pDest		= NULL;
	
	//-- 2014-09-10 hongsu@esmlab.com
	//-- DEBUG SEND RC MESSAGE 
	//TRACE(_T("[SEND] RC : %04x / ID[%s]\n"), message, strID);	
	::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)pWParam, (LPARAM)pMsg);
}

void CESMRCManager::AddMsg(ESMEvent* pMsg, BOOL bReferee/* = FALSE*/)
{
	if(!pMsg)
		return;
	
	if(m_bConnected && m_bThreadStop == FALSE)
	{
		if(bReferee)
			m_arReferee.Add((CObject*) pMsg);
		else
			m_arMsg.Add((CObject*)pMsg);
	}
	else
	{
		if(m_bThreadStop)
		{
			m_bThreadStop = FALSE;
			m_ThreadHandle	= NULL;
			StartSendThread();
			ESMLog(5, _T("Restart Sennd Thread"));
		}
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
	}
}

/************************************************************************
 * @method:		CESMRCManager::RemoveAllMsg
 * @function:	RemoveAllMsg
 * @date:		2013-10-22
 * @owner		Ryumin (ryumin@esmlab.com)
 * @return:		void
 */
void CESMRCManager::RemoveAllMsg()
{
	ESMEvent* pMsg	= NULL;

	int nCnt = m_arMsg.GetCount();
	for (int nIdx = 0; nIdx < nCnt; ++nIdx)
	{
		pMsg = (ESMEvent*)m_arMsg.GetAt(nIdx);
		if (pMsg)
		{
			delete pMsg;
			pMsg = NULL;
			m_arMsg.RemoveAt(nIdx);
		}
	}

	if(m_arMsg.GetCount() > 0)
		m_arMsg.RemoveAll();

	//jhhan referee
	nCnt = m_arReferee.GetCount();
	for (int nIdx = 0; nIdx < nCnt; ++nIdx)
	{
		pMsg = (ESMEvent*)m_arReferee.GetAt(nIdx);
		if (pMsg)
		{
			delete pMsg;
			pMsg = NULL;
			m_arReferee.RemoveAt(nIdx);
		}
	}

	if(m_arReferee.GetCount() > 0)
		m_arReferee.RemoveAll();
}

void CESMRCManager::StartSendThread()
{
	if (m_bThreadStop) return;
	if (m_ThreadHandle) return;	

	m_ThreadHandle = (HANDLE) _beginthread( _SendThread, 0, (void*)this); // create thread
}

void CESMRCManager::StartAcceptThread(BOOL bRemote)
{
	//if (m_hAcceptThread) return;
	ThreadSendData* pSendData = NULL;

	if(bRemote == TRUE)
	{
		pSendData = new ThreadSendData;
		pSendData->nSocIndex = RCPMODE_REMOTE;
		pSendData->pESMRCManager = this;
		m_hAcceptThread[2] = (HANDLE) _beginthread( _AcceptThread, 0, (void*)pSendData); // create thread
	}else
	{
		pSendData = new ThreadSendData;
		pSendData->nSocIndex = RCPMODE_BOTH;
		pSendData->pESMRCManager = this;
		m_hAcceptThread[0] = (HANDLE) _beginthread( _AcceptThread, 0, (void*)pSendData); // create thread

		pSendData = new ThreadSendData;
		pSendData->nSocIndex = RCPMODE_EDITER;
		pSendData->pESMRCManager = this;
		m_hAcceptThread[1] = (HANDLE) _beginthread( _AcceptThread, 0, (void*)pSendData); // create thread
	}
	
}

void CESMRCManager::SyncTickCount(BOOL bServer)
{
	if(!m_bConnected)
		return;

	if(bServer)
	{
		if(!m_pServerSocket)
			return;

		if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
			return;

		char buf[1024] = {0};
		RCPHeader* packetheader = (RCPHeader*)buf;

		strcpy(packetheader->protocol, "RCP1.0");
		packetheader->command     = RCPCMD_TICKCOUNT_GET;
		packetheader->bodysize  = 0;

		int length = sizeof(RCPHeader);	

		//-- Server Time
		//packetheader->param1 = (double)m_nLocalTime;
		m_nLocalTime = ESMGetTick();
		int sent = m_pServerSocket->Send(buf, length);		
	}
	else
	{
		if(!m_pClientSocket[0])
			return;
		//-- Agent Time
		m_nLocalTime = ESMGetTick();	

		char buf[1024] = {0};
		RCPHeader* packetheader = (RCPHeader*)buf;

		strcpy(packetheader->protocol, "RCP1.0");
		packetheader->command     = RCPCMD_TICKCOUNT_SET;
		packetheader->bodysize  = 0;

		int length = sizeof(RCPHeader);	
		packetheader->param1 = m_nLocalTime;
		for( int i =0 ;i< MAXSOCKET; i++)
		{
			if(m_pClientSocket[i])
				int sent = m_pClientSocket[i]->Send(buf, length);
		}
		//int sent = m_pTCPSocket->Send(buf, length);
		TRACE(_T("[Sync Time]  2. Get Client Time : %d\n") ,m_nLocalTime);

	}
	return;
}

// void CESMRCManager::SetThreadSleep(BOOL bSleep)
// {
// 	if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER && ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
// 		return;
// 
// 	char buf[1024] = {0};
// 	RCPHeader* packetheader = (RCPHeader*)buf;
// 
// 	strcpy(packetheader->protocol, "RCP1.0");
// 	packetheader->command   = RCPCMD_TIMESYNC_SLEEP;
// 	packetheader->bodysize  = 0;
// 
// 	if(bSleep)
// 	{
// 		m_nThreadSleep = 1;
// 		packetheader->param1 = 1;
// 	}
// 	else
// 	{
// 		m_nThreadSleep = 0;
// 		packetheader->param1 = 0;
// 	}
// 
// 	int length = sizeof(RCPHeader);	
// 	m_pTCPSocket->Send(buf, length);		
// 
// 	return;
// }


void CESMRCManager::ConnectCheck(int nFrameRate, int nTime/* = -1*/)
{
	m_bCheckConnect = FALSE;

	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;

	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_CONNECT_CHECK;
	packetheader->bodysize  = 0;

	int length = sizeof(RCPHeader);	
	packetheader->param1 = 1;
	packetheader->param2 = nFrameRate;	//180308 장비타입 및 해상도 타입 정보 전달 //jhhan 170713
	packetheader->param3 = nTime;	//jhhan 180822
	
	
	//int sent = m_pTCPSocket->Send(buf, length);
	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
}

void CESMRCManager::GetDiskSize()
{
	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;

	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_GET_DISKINFO;
	packetheader->bodysize  = 0;

	int length = sizeof(RCPHeader);	
	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
}

void CESMRCManager::ConnectStatusCheck(BOOL bNetworkEx/* = FALSE*/)
{
	if(m_bConnectStatusCheck)
		return;

	m_bConnectStatusCheck = TRUE;

	m_bNetworkMode = bNetworkEx;

	//Wait(10000);
	ESMLog(1,_T("ConnectStatusCheck Start"));
	
	hHandle = (HANDLE) _beginthreadex(NULL, 0, DoConnectStatusCheckThread,this, 0, NULL);
}


unsigned WINAPI CESMRCManager::DoConnectStatusCheckThread(LPVOID param)
{
	Sleep(5000);
	CESMRCManager * rcManager = (CESMRCManager *)param;
	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;
	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_CONNECT_STATUS_CHECK;
	packetheader->bodysize  = 0;

	//jhhan 16-11-22
	if(rcManager->m_bNetworkMode)
		packetheader->param3 = ESM_NETWORK_4DP;


	int length = sizeof(RCPHeader);	
	if(rcManager->m_pServerSocket)

	while(1)
	{	
		if(rcManager->m_bConnectStatusCheck && rcManager)
		{
			int sent = rcManager->m_pServerSocket->Send(buf, length);
			
		}
		else
			return 0;
		Sleep(1000);

	}
	
	//ESMLog(1,rcManager->m_strIP);
	return 0 ;
}

void CESMRCManager::Wait(DWORD dwMillisecond)
{
	MSG msg;
	DWORD dwStart;
	dwStart = GetTickCount();

	while(GetTickCount() - dwStart < dwMillisecond)
	{
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}

void CESMRCManager::StartSendFrame()
{
	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;

	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_GET_STARTSENDFRAME;
	packetheader->param1	= 8281;
	packetheader->bodysize  = sizeof(RCP_FileSave_Info);

	RCP_FileSave_Info* pFileBody = (RCP_FileSave_Info*)(buf + sizeof(RCPHeader));

	CString pstrTemp = GetSendIP();
	char* strId = ESMUtil::CStringToChar(pstrTemp);
	memcpy(pFileBody->szFileName, strId, sizeof(char) * 128);
	if( strId)
	{
		delete strId;
		strId = NULL;
	}

	int length = sizeof(RCPHeader)+packetheader->bodysize;	
	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
}

void CESMRCManager::StopSendFrame()
{
	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;

	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_GET_STOPSENDFRAME;
	packetheader->bodysize	= 0;

	int length = sizeof(RCPHeader);	
	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
}

void _FrameDataSendThread(void *param)
{
	ThreadSendData* pSendData = (ThreadSendData*)param;
	int nType = pSendData->nSocIndex;
	CESMRCManager* pRCM = pSendData->pESMRCManager;	
	vector<MakeFrameInfo>* pArrFrameInfo = pSendData->pArrFrameInfo;	
	delete pSendData;

	if (!pRCM) 
		return;

	TRACE(_T("_FrameDataSendThread START\r\n"));

	//Send Data
	KSSHeader KData;
	MakeFrameInfo* pFrameInfo;
	EnterCriticalSection(&_ESMSENDRCMGR);
	if(nType == 0)
	{
		if (!pArrFrameInfo) 
		{
			LeaveCriticalSection (&_ESMSENDRCMGR);	
			return;
		}
		DWORD nTick = GetTickCount();
		ESMLog(5, _T("SendFrameData -------------------------- Gap :  %d ms"), nTick - ESMGetSenderGap());
		ESMSetSenderGap(nTick);

#ifndef _FRAME_TRANS
		for( int i = 1; i<= pArrFrameInfo->size(); i++)
		{
#ifdef	_LAST_1_FRAME

			if(i % 30 == 0)
				continue;
#else
			if(i % 15 == 0)
				continue;
#endif


			pFrameInfo = &(pArrFrameInfo->at(i-1));

			KData.command[0] = 0x64;
			KData.command[1] = 0x61;
			KData.command[2] = 0x74;
			KData.command[3] = 0x61;

			/*
			KData.cameraid	= htonl(pFrameInfo->nCameraID);
			KData.index		= htonl(pFrameInfo->nFrameIndex);
			//영상 사이즈 자동 변환
			KData.lenght	= htonl((pFrameInfo->nHeight * pFrameInfo->nWidth * 1.5));
			KData.secIdx	= htonl(pFrameInfo->nSecIdx);
			*/

			//jhhan 16-11-29 Endian 처리 변경
			//KData.cameraid	= pFrameInfo->strCameraID;
			memcpy(KData.cameraid, pFrameInfo->strCameraID, MAX_PATH);
			KData.index		= pFrameInfo->nFrameIndex;
			//영상 사이즈 자동 변환
			KData.lenght	= pFrameInfo->nHeight * pFrameInfo->nWidth * 1.5;
			KData.secIdx	= pFrameInfo->nSecIdx;	


			KData.ImageData	= (char*)pFrameInfo->pYUVImg;

			if(KData.lenght != 0)	//jhhan 16-11-29 사이즈 확인 후 전송 0일때 Skip
			{
				pRCM->SendFrameData(&KData);

				//ESMLog(5, _T("SendFrameData Camera %d, File %d, Frame %d"), KData.cameraid, KData.secIdx, KData.index);
			}else
			{
				//ESMLog(5, _T("SendFrameData[X] Camera %d, File %d, Frame %d"), KData.cameraid, KData.secIdx, KData.index);
			}
			
		}
#else
		pFrameInfo = &(pArrFrameInfo->at(0));

		KData.command[0] = 0x64;
		KData.command[1] = 0x61;
		KData.command[2] = 0x74;
		KData.command[3] = 0x61;

		/*
		KData.cameraid	= htonl(pFrameInfo->nCameraID);
		KData.index		= htonl(pFrameInfo->nFrameIndex);
		//영상 사이즈 자동 변환
		KData.lenght	= htonl((pFrameInfo->nHeight * pFrameInfo->nWidth * 1.5));
		KData.secIdx	= htonl(pFrameInfo->nSecIdx);
		*/

		//jhhan 16-11-29 Endian 처리 변경
		memcpy(KData.cameraid, pFrameInfo->strCameraID, MAX_PATH);
		//KData.index		= pFrameInfo->nFrameIndex;
		//영상 사이즈 자동 변환
		//KData.lenght	= pFrameInfo->nHeight * pFrameInfo->nWidth * 1.5;
		KData.secIdx	= pFrameInfo->nSecIdx;

		FILE *fp;
		CString strLog;
		CString strPath = pFrameInfo->strFramePath;
		char filename[255] = {0,};	

		sprintf(filename, "%S", strPath);

		fp = fopen(filename, "rb");

		if(fp == NULL)
		{
			strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), strPath);
			ESMLog(0,strLog);
			return;
		}

		//strLog.Format(_T("[#NETWORK] Send File [%s]"), strPath);
		ESMLog(1,strLog);

		fseek(fp, 0L, SEEK_END);
		int nLength = ftell(fp);
		char* buf = new char[nLength];
		fseek(fp, 0, SEEK_SET);
		fread(buf, sizeof(char), nLength, fp);

		KData.lenght	= nLength;
		KData.ImageData	= (char*)buf;

		if(KData.lenght != 0)	//jhhan 16-11-29 사이즈 확인 후 전송 0일때 Skip
		{
			pRCM->SendFrameData(&KData);

			//ESMLog(5, _T("SendFrameData Camera %d, File %d, Frame %d"), KData.cameraid, KData.secIdx, KData.index);
		}

		if(buf)
		{
			delete[] buf;
			buf = NULL;
		}
		fclose(fp);

		//File Delete(Encoded movie & Original movie)
		CESMFileOperation fo;
		if(fo.IsFileExist(pArrFrameInfo->at(0).strFramePath))
		{
			//Encoded file
			fo.Delete(pArrFrameInfo->at(0).strFramePath);
		}
		if(fo.IsFileExist(pArrFrameInfo->at(1).strFramePath))
		{
			//Original file
			fo.Delete(pArrFrameInfo->at(1).strFramePath);
		}
#endif
		int nEndTick = GetTickCount();
		//ESMLog(5, _T("SendFrameData leadtime : %d ms / %d ---------- Gap : %d"), nEndTick-nTick, KData.secIdx, nEndTick - ESMGetSenderGap());
		ESMLog(5, _T("SendFrameData leadtime : %d ms / %d"), nEndTick-nTick, KData.secIdx);

		//ESMSetSenderGap(nEndTick);
	}
	else
	{
		if(nType == 1) 
		{
			KData.command[0] = 0x6f;
			KData.command[1] = 0x70;
			KData.command[2] = 0x65;
			KData.command[3] = 0x6e;

			//Open CameraID Add
			//ESMLog(5, _T("SendFrameData ---- Open Camera ID : %d"), _ttoi(pRCM->GetSourceDSC()));
			_stprintf(KData.cameraid, _T("%s"), pRCM->GetSourceDSC());
			
		}
		else
		{
			KData.command[0] = 0x63;
			KData.command[1] = 0x6c;
			KData.command[2] = 0x6f;
			KData.command[3] = 0x73;
		}
		pRCM->SendFrameData(&KData);

		if(nType == 1)
		{
			ESMLog(5, _T("SendFrameData ----- Open Camera ID : %s"), pRCM->GetSourceDSC());
		}else
		{
			ESMLog(5, _T("SendFrameData ----- Close"));
		}

		LeaveCriticalSection (&_ESMSENDRCMGR);
		return;
	}
	LeaveCriticalSection (&_ESMSENDRCMGR);	

	//Memory 해제
	for( int i = 0; i< pArrFrameInfo->size(); i++)
	{
		pFrameInfo = &(pArrFrameInfo->at(i));

		if(pFrameInfo->pYUVImg)
		{
			delete[] pFrameInfo->pYUVImg;
			pFrameInfo->pYUVImg = NULL;
		}

		if(pFrameInfo->Image)
		{
			delete[] pFrameInfo->Image;
			pFrameInfo->Image = NULL;
		}	
	}

	pArrFrameInfo->clear();
	delete pArrFrameInfo;
	TRACE(_T("_FrameDataSendThread END\r\n"));
	_endthread();
}

BOOL CESMRCManager::SendFrameInfo(vector<MakeFrameInfo>* pFrameInfo)
{
	HANDLE ThreadHandle;
	ThreadSendData* pSendData = new ThreadSendData;
	pSendData->nSocIndex = 0;
	pSendData->pESMRCManager = this;
	pSendData->pArrFrameInfo = pFrameInfo;
	

	ThreadHandle = (HANDLE) _beginthread( _FrameDataSendThread, 0, (void*)pSendData); // create thread
	CloseHandle(ThreadHandle);
	return true;
}

BOOL CESMRCManager::SendFrameInfoOpenClose(int nType)
{
	HANDLE ThreadHandle;
	ThreadSendData* pSendData = new ThreadSendData;
	pSendData->nSocIndex = nType;
	pSendData->pESMRCManager = this;
	pSendData->pArrFrameInfo = NULL;
	

	ThreadHandle = (HANDLE) _beginthread( _FrameDataSendThread, 0, (void*)pSendData); // create thread
	//CloseHandle(ThreadHandle);	
	return TRUE;
}

BOOL CESMRCManager::SendFrameData(KSSHeader *pstData)
{
	if(!pstData)
		return FALSE;

	if(!m_pServerSocket)
		return FALSE;

	int sent = m_pServerSocket->Send((char*)pstData, sizeof(int)*5);

	if(0 >= sent)
		return FALSE;

	if (pstData->lenght)
	{
#if _MSWINDOWS
		int sent = m_pServerSocket->Send(pstData->ImageData, ntohl(pstData->lenght));
#else	//jhhan 16-11-29 Endian 처리 변경
		int sent = m_pServerSocket->Send(pstData->ImageData, pstData->lenght);
#endif
		//ESMLog(5,_T("[%d] : %d - %d"),pstData->secIdx,sent,pstData->lenght);
		if(0 >= sent)
			return FALSE;
	}
	/*
	if(pstData->lenght)
	{
		int nSend = 8192;
		int nSendSize = 0;
		char* poffset = (char*)pstData->ImageData;

		while(1)
		{
			if(pstData->lenght < nSend)
				nSend = pstData->lenght;

			if ((pstData->lenght-nSendSize) < nSend)
				nSend = pstData->lenght-nSendSize;

			int sent = m_pServerSocket->Send((char*)poffset[nSendSize], nSend);
			if(0 >= sent)
				return FALSE;

			nSendSize += nSend;

			if (nSendSize >= pstData->lenght)
				break;
		}
	}
	*/

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
// KT Data Sender
BOOL CESMRCManager::ConnectToKTServer(CString strIP, int nPort)
{
	if(!ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
	{
		ESMLog(5,_T("ESMGetValue(ESM_VALUE_NET_MODE)"));
		return FALSE;
	}

	if(m_pServerSocket)
	{
		m_pServerSocket->Close();
		delete m_pServerSocket;
		m_pServerSocket= NULL;
	}

	char szHostIP[50];
	char* strId = ESMUtil::CStringToChar(strIP);
	memcpy(szHostIP, strId, sizeof(char) * 50);

	if( strId)
	{
		delete strId;
		strId = NULL;
	}

	EnterCriticalSection(&_ESMRCMGR);
	m_pServerSocket = new CESMTCPSocket;
	m_pServerSocket->Create(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0);

	int ret = m_pServerSocket->Connect(szHostIP, nPort/*, 1*/);
	LeaveCriticalSection (&_ESMRCMGR);

	if(ret != 0)
	{
		ESMLog(5,_T("ret: %d"),ret);

		m_pServerSocket->Close();
		delete m_pServerSocket;
		m_pServerSocket= NULL;
		SetKTConnected(FALSE);
		return FALSE;
	}

	SetKTConnected(TRUE);
	return TRUE;
}

void CESMRCManager::DisConnectToKTServer()
{
	if(!ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
		return;

	m_pServerSocket->Close();
	delete m_pServerSocket;
	m_pServerSocket= NULL;

	SetKTConnected(FALSE);
}

void CESMRCManager::IPerfRun(BOOL bOn/* = TRUE*/)
{
	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;

	strcpy(packetheader->protocol, "RCP1.0");
	if(bOn)
		packetheader->command   = RCPCMD_RUN_IPERF;
	else
		packetheader->command	= RCPCMD_RUN_IPERF_OFF;
	packetheader->bodysize	= 0;

	int length = sizeof(RCPHeader);	
	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
}

void CESMRCManager::DumpProcDown()
{
	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;

	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_DUMP_PROCDOWN;
	packetheader->bodysize	= 0;

	int length = sizeof(RCPHeader);	
	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
}

void CESMRCManager::SendMakeMuxData(MuxDataInfo* pMuxdata)
{
	char buf[5128] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;
	char *pBodyData = buf + sizeof(RCPHeader);

	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_MUX_SENDDATA;
	packetheader->param1	= ESMGetReverseMovie();
	//ESMLog(5,_T("[MUX]Reverse: %d - %d"),packetheader->param1,ESMGetReverseMovie());
	packetheader->bodysize	= sizeof(MuxDataInfo) + sizeof(_RCP_ADJUST_INFO);

	memcpy(pBodyData, pMuxdata, sizeof(MuxDataInfo));

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CDSCItem* pItem = NULL;

	CString strDSCID;
	strDSCID.Format(_T("%s"),pMuxdata->strCamID);

	for(int i = 0; i < arDSCList.GetCount() ; i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		
		if(strDSCID == pItem->GetDeviceDSCID())
		{
			_RCP_ADJUST_INFO adjInfo;
			adjInfo.AdjAngle = pItem->GetDSCAdj(DSC_ADJ_A);
			adjInfo.AdjSize = pItem->GetDSCAdj(DSC_ADJ_SCALE);
			adjInfo.AdjptRotate.x = pItem->GetDSCAdj(DSC_ADJ_RX);
			adjInfo.AdjptRotate.y = pItem->GetDSCAdj(DSC_ADJ_RY);
			adjInfo.AdjMove.x = pItem->GetDSCAdj(DSC_ADJ_X);
			adjInfo.AdjMove.y = pItem->GetDSCAdj(DSC_ADJ_Y);
			adjInfo.nWidth = pItem->GetWidth();
			adjInfo.nHeight = pItem->GetHeight();
			adjInfo.stMargin = pItem->GetMargin();

			// adjInfo
			memcpy(pBodyData + sizeof(MuxDataInfo), 
				&adjInfo, sizeof(_RCP_ADJUST_INFO));
			break;

		}
	}


	int length = sizeof(RCPHeader) + sizeof(MuxDataInfo) + sizeof(_RCP_ADJUST_INFO);	
	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
}

void CESMRCManager::SendMuxFinish(int nResult, CString strCameraID)
{
	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;

	char* pstrData = NULL;
	pstrData = ESMUtil::CStringToChar(strCameraID);
	
	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_MUX_SENDFINISH;
	packetheader->param1 = nResult;
	//packetheader->param2 = nCameraID;
	packetheader->param3 = m_nRemoteID;
	packetheader->bodysize	= 0;

	char* strId = ESMUtil::CStringToChar(strCameraID);
	memcpy(packetheader->Camera, strId, sizeof(char) * 8);
	if( strId)
	{
		delete strId;
		strId = NULL;
	}

	int length = sizeof(RCPHeader);	

	if(m_pClientSocket[0])
		int sent = m_pClientSocket[0]->Send(buf, length);

	ESMLog(5,_T("Send Finish [ %s]"),strCameraID);
}

void CESMRCManager::RequestData(CString strPath)
{
	FILE *fp;
	CString strLog;

	char filename[255] = {0,};	
	sprintf(filename, "%S", strPath);

	fp = fopen(filename, "rb");

	if(fp == NULL)
	{
		strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), strPath);
		ESMLog(0,strLog);
		return;
	}

	strLog.Format(_T("[#NETWORK] Send File [%s]"), strPath);
	//ESMLog(1,strLog);

	fseek(fp, 0L, SEEK_END);
	int nLength = ftell(fp);
	char* buf = new char[nLength];
	fseek(fp, 0, SEEK_SET);
	fread(buf, sizeof(char), nLength, fp);

	CString *pPath = new CString;
	pPath->Format(_T("%s"),strPath);

	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_REQUEST_DATA_TOSERVER;
	//pMsg->message = WM_ESM_NET_REQUEST_SEND_DATA;
	
	pMsg->pParam = (LPARAM)pPath;
	pMsg->pDest = (LPARAM)buf;
	pMsg->nParam3 = nLength;
	AddMsg(pMsg);
	//::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	
	fclose(fp);
}

BOOL CESMRCManager::RequestData(CString strMoviePath,CString strSavePath,int nSencCnt)
{
	FILE *fp;
	CString strLog;

	char filename[255] = {0,};	
	sprintf(filename, "%S", strMoviePath);

	fp = fopen(filename, "rb");

	if(fp == NULL)
	{
		strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), strMoviePath);
		ESMLog(0,strLog);
		return FALSE;
	}

	strLog.Format(_T("[#NETWORK] Send File [%s]"), strMoviePath);
	//ESMLog(1,strLog);

	fseek(fp, 0L, SEEK_END);
	int nLength = ftell(fp);
	char* buf = new char[nLength];
	fseek(fp, 0, SEEK_SET);
	fread(buf, sizeof(char), nLength, fp);

	CString *pPath = new CString;
	pPath->Format(_T("%s"),strSavePath);

	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_REQUEST_DATA_TOSERVER;
	//pMsg->message = WM_ESM_NET_REQUEST_SEND_DATA;

	pMsg->pParam = (LPARAM)pPath;
	pMsg->pDest = (LPARAM)buf;
	pMsg->nParam3 = nLength;
	AddMsg(pMsg);
	//::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

	fclose(fp);

	return TRUE;
}
BOOL CESMRCManager::ConnectToProcessor()
{
	//connect 시 첫촬영 delay time 적용
	ESMSetFirstConnect(TRUE);

	if(m_pServerSocket)
		DisconnectToProcessor();

	char szHostIP[50];
	char* strId = ESMUtil::CStringToChar(m_strIP);
	memcpy(szHostIP, strId, sizeof(char) * 50);
	if( strId)
	{
		delete strId;
		strId = NULL;
	}

	//m_pTCPSocket = new CESMTCPSocket(CLIENTMODE);

	int nPort = m_nRemotePort/*m_nMakingPort*/;
	
	EnterCriticalSection(&_ESMRCMGR);
	m_pServerSocket = new CESMTCPSocket;
	m_pServerSocket->Create(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0);
	int ret = m_pServerSocket->Connect(szHostIP, nPort);
	LeaveCriticalSection (&_ESMRCMGR);

	if(ret != 0)
	{
		Disconnect();	
		return FALSE;
	}

	//ESMLog(5, _T("Connect 0x%X"), &m_pServerSocket);

	ThreadSendData* pThreadSendData = new ThreadSendData;
	pThreadSendData->nSocIndex = 0;
	pThreadSendData->pESMRCManager = (CESMRCManager*)this;
	m_hAcceptThread[0] = (HANDLE) _beginthreadex(NULL, 0, SockRecvThread, (void *)pThreadSendData, 0, NULL);

	m_bConnected	= TRUE;
	//m_bAccept		= FALSE;
	m_bNetMode	= TRUE;

	//ESMLog(3,_T("[CESMRCManager] Connect"));

	//-- Sync Tick Count
	//-- Get Local Time / Agent Time
	//	SetThreadSleep(FALSE);
	//SyncTickCount(TRUE);

	//-- 2013-10-14 hongsu@esmlab.com
	//-- Update NetworkView
	/*ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent();
	pMsg->message = WM_ESM_VIEW_NET_UPDATE_CONNECTION;
	pMsg->nParam1 = m_nRemoteID;
	pMsg->nParam2 = m_bConnected;
	pMsg->nParam3 = m_bKTConnected;
	pMsg->pParam  = (LPARAM)this;
	::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);*/

	//Get Client HD Size
	//GetDiskSize();

	//Get ConnectStatusCheck	
	//ConnectStatusCheck();

	return TRUE;
}


BOOL CESMRCManager::DisconnectToProcessor(BOOL bRflashUI)
{
	EnterCriticalSection(&_ESMRCMGR);
	m_bConnected = FALSE;
	//m_bAccept	 = FALSE;
	if( m_nStatus != NET_STATUS_CONNECTING)
		m_nStatus	 = NET_STATUS_DISCONNECT;

	
	if(!m_pServerSocket)
	{
		LeaveCriticalSection (&_ESMRCMGR);
		return TRUE;
	}

	m_pServerSocket->Close();
	delete m_pServerSocket;
	m_pServerSocket= NULL;

	ESMLog(5,_T("[Processor] Disconnect"));

	//if(m_bNetMode && bRflashUI == TRUE)
	//{
	//	ESMEvent* pMsg = NULL;
	//	pMsg = new ESMEvent();
	//	pMsg->message = WM_ESM_VIEW_NET_UPDATE_CONNECTION;
	//	pMsg->nParam1 = m_nRemoteID;
	//	pMsg->nParam2 = m_bConnected;
	//	pMsg->nParam3 = m_bKTConnected;
	//	pMsg->pParam  = (LPARAM)this;
	//	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	//}
	RemoveAllMsg();

	
	m_bConnectStatusCheck = FALSE;
	TerminateThread(hHandle,0);
	CloseHandle(hHandle);
	LeaveCriticalSection (&_ESMRCMGR);
	return TRUE;
}
void CESMRCManager::SendFrameInfo(MakeYUVInfo* pYUVInfo)
{
#ifdef KT_SEND_SYNC
	HANDLE ThreadHandle;
	ThreadSendYUVData* pSendData = new ThreadSendYUVData;
	pSendData->nSocIndex = 0;
	pSendData->pESMRCManager = this;
	pSendData->pYUVInfo = pYUVInfo;

	ThreadHandle = (HANDLE)_beginthread( _FrameDataEncodeThread, 0, (void*)pSendData);

#else
	//KT 전송 동기화 4DP Sync Time
	m_arSendList.Add((CObject*)pYUVInfo);
#endif

}
void CESMRCManager::SendFrameInfo(CStringArray* pstrArrSendFrame)
{
	//hjcho
#if 1
	ESMLog(5,_T("[%s] - Send Start %s"),pstrArrSendFrame->GetAt(1),pstrArrSendFrame->GetAt(2));
	HANDLE ThreadHandle = NULL;	ThreadSendStringData* pSendData = new ThreadSendStringData;
	pSendData->nSocIndex	= 0;
	pSendData->pESMRCManager = this;
	pSendData->pstrMovieArr = pstrArrSendFrame;

	ThreadHandle = (HANDLE)_beginthread( _MovieDataSendThread, 0, (void*)pSendData);
	CloseHandle(ThreadHandle);
#else
	if(ESMGetValue(ESM_VALUE_4DPMETHOD) == 1 || ESMGetValue(ESM_VALUE_4DPMETHOD)==2)
	{
		//ESMLog(5,_T("[%s] Enter Add Frame Info"),ESMGetSecIdxFromPath(pstrArrSendFrame->GetAt(3)));
		EnterCriticalSection(&m_crRTSend);
		//ESMLog(5,_T("Add FrameInfo"));
		m_arSendList.Add((CObject*)pstrArrSendFrame);
		LeaveCriticalSection(&m_crRTSend);
		//ESMLog(5,_T("[%s] Leave Frame Info"),ESMGetSecIdxFromPath(pstrArrSendFrame->GetAt(3)));
	}
#endif

}
void _MovieDataSendThread(void *param)
{
	ThreadSendStringData* pSendData = (ThreadSendStringData*)param;
	int nType = pSendData->nSocIndex;
	CESMRCManager* pRCM = pSendData->pESMRCManager;	
	CStringArray* pArrMovieData = pSendData->pstrMovieArr;	
	delete pSendData;

	if (!pRCM) 
		return;

	TRACE(_T("_FrameDataSendThread START\r\n"));
	ESMLog(5,_T("_FrameDataSendThread START\r\n"));
	//Send Data
	KSSHeader KData;
	MakeFrameInfo* pFrameInfo;
	EnterCriticalSection(&_ESMSENDRCMGR);
	if(nType == 0)
	{
		if (!pArrMovieData) 
		{
			LeaveCriticalSection (&_ESMSENDRCMGR);	
			return;
		}
		DWORD nTick = GetTickCount();
		ESMLog(5, _T("SendFrameData -------------------------- Gap :  %d ms"), nTick - ESMGetSenderGap());
		ESMSetSenderGap(nTick);

		KData.command[0] = 0x64;
		KData.command[1] = 0x61;
		KData.command[2] = 0x74;
		KData.command[3] = 0x61;

		//StringArray
		//0 - CAMID
		//1 - SecIdx
		//2 - Encoded File
		//3 - Original File

		_stprintf(KData.cameraid, _T("%s"), pArrMovieData->GetAt(0));
		KData.secIdx	= _ttoi(pArrMovieData->GetAt(1));

		FILE *fp;
		CString strLog;
		CString strPath = pArrMovieData->GetAt(2);
		char filename[255] = {0,};	

		sprintf(filename, "%S", strPath);

		fp = fopen(filename, "rb");
		ESMLog(5,_T("FrameDataRead --------------------------%d ms"),GetTickCount()-nTick);
		if(fp == NULL)
		{
			strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), strPath);
			ESMLog(0,strLog);
			LeaveCriticalSection (&_ESMSENDRCMGR);				
			return;
		}

		strLog.Format(_T("[#NETWORK] Send File [%s]"), strPath);
		ESMLog(1,strLog);

		fseek(fp, 0L, SEEK_END);
		int nLength = ftell(fp);
		char* buf = new char[nLength];
		fseek(fp, 0, SEEK_SET);
		fread(buf, sizeof(char), nLength, fp);

		KData.lenght	= nLength;
		KData.ImageData	= (char*)buf;

		if(KData.lenght != 0)	//jhhan 16-11-29 사이즈 확인 후 전송 0일때 Skip
		{
			pRCM->SendFrameData(&KData);

			//ESMLog(5, _T("SendFrameData Camera %d, File %d, Frame %d"), KData.cameraid, KData.secIdx, KData.index);
		}

		int nEndTick = GetTickCount();
		//ESMLog(5, _T("SendFrameData leadtime : %d ms / %d ---------- Gap : %d"), nEndTick-nTick, KData.secIdx, nEndTick - ESMGetSenderGap());
		ESMLog(5, _T("SendFrameData leadtime : %d ms / %d"), nEndTick-nTick, KData.secIdx);

		if(buf)
		{
			delete[] buf;
			buf = NULL;
		}
		fclose(fp);

		//File Delete(Encoded movie & Original movie)
		CESMFileOperation fo;
		if(fo.IsFileExist(pArrMovieData->GetAt(2)))
		{
			//Encoded File
			fo.Delete(pArrMovieData->GetAt(2));
		}		if(fo.IsFileExist(pArrMovieData->GetAt(3)))
		{
			//Original file
			fo.Delete(pArrMovieData->GetAt(3));
			//ESMLog(5,_T("Delete %s"),pArrMovieData->GetAt(3));
		}
		if(ESMGetValue(ESM_VALUE_4DPMETHOD) == 2)
		{
			if(fo.IsFileExist(pArrMovieData->GetAt(4)))
			{
				//Original file
				fo.Delete(pArrMovieData->GetAt(4));
			}
			if(fo.IsFileExist(pArrMovieData->GetAt(5)))
			{
				//Original file
				fo.Delete(pArrMovieData->GetAt(5));
			}
			if(fo.IsFileExist(pArrMovieData->GetAt(6)))
			{
				//Original file
				fo.Delete(pArrMovieData->GetAt(6));
			}
		}
	}
	else
	{
		if(nType == 1) 
		{
			KData.command[0] = 0x6f;
			KData.command[1] = 0x70;
			KData.command[2] = 0x65;
			KData.command[3] = 0x6e;

			//Open CameraID Add
			//ESMLog(5, _T("SendFrameData ---- Open Camera ID : %d"), _ttoi(pRCM->GetSourceDSC()));
			_stprintf(KData.cameraid, _T("%s"), pRCM->GetSourceDSC());

		}
		else
		{
			KData.command[0] = 0x63;
			KData.command[1] = 0x6c;
			KData.command[2] = 0x6f;
			KData.command[3] = 0x73;
		}
		pRCM->SendFrameData(&KData);

		if(nType == 1)
		{
			ESMLog(5, _T("SendFrameData ----- Open Camera ID : %s"), pRCM->GetSourceDSC());
		}else
		{
			ESMLog(5, _T("SendFrameData ----- Close"));
		}

		LeaveCriticalSection (&_ESMSENDRCMGR);
		return;
	}
	LeaveCriticalSection (&_ESMSENDRCMGR);	

	//Memory 해제
	delete pArrMovieData;
	pArrMovieData = NULL;

	TRACE(_T("_FrameDataSendThread END\r\n"));
	_endthread();
}
//KT 전송 동기화 4DP Sync Time
void CESMRCManager::StartKTSendThread()
{
	HANDLE ThreadHandle;

	int nAll = (int)m_arSendList.GetCount();
	while(nAll--)
	{
#if 0
		MakeYUVInfo* pYUVInfo = NULL;
		pYUVInfo = (MakeYUVInfo*)m_arSendList.GetAt(nAll);
		if(pYUVInfo)
		{
			//Memory Delete
			for(int i = 0 ; i < pYUVInfo->pArrYImage->size(); i++)
			{
				pYUVInfo->pArrYImage->at(i).release();
				pYUVInfo->pArrUImage->at(i).release();
				pYUVInfo->pArrVImage->at(i).release();
			}

			pYUVInfo->pArrYImage->clear();
			pYUVInfo->pArrUImage->clear();
			pYUVInfo->pArrVImage->clear();

			delete pYUVInfo->pArrYImage;
			delete pYUVInfo->pArrUImage;
			delete pYUVInfo->pArrVImage;

			pYUVInfo->pArrYImage = NULL;
			pYUVInfo->pArrUImage = NULL;
			pYUVInfo->pArrVImage = NULL;

			delete pYUVInfo;
			pYUVInfo = NULL;
		}
#endif
		m_arSendList.RemoveAt(nAll);
	}
	m_arSendList.RemoveAll();

	if(GetKTThreadRun() == TRUE)
	{
		ESMLog(5,_T("Still Run...."));
		SetKTThreadStop(TRUE);

		while(1)
		{
			if(GetKTThreadRun() == FALSE || GetKTThreadStop() == FALSE)
				break;

			Sleep(10);
		}

		ESMLog(5,_T("Run Finish...."));
	}

	ThreadHandle = (HANDLE)_beginthread( _KTSendThread, 0, (void*)this);
}

//KT 전송 동기화 4DP Sync Time
void _KTSendThread(void *param)
{
	CESMRCManager* pRCM = (CESMRCManager*)param;
	MakeYUVInfo* pYUVInfo = NULL;
	CStringArray* pstrArrSendFrame = NULL;
	int nTime = ESMGetTick();
	int nSendCnt = 0;
	int nGap = 0;
	pRCM->SetKTThreadRun(TRUE);

	if(pRCM->m_nKTSendTime > nTime)
	{
		while(1)
		{
			int nTime = ESMGetTick();
			if(nTime >= pRCM->m_nKTSendTime)
			{
				//ESMLog(5,_T("[%d] Enter CriticalSection"),nSendCnt);
				EnterCriticalSection(&pRCM->m_crRTSend);
				int nCount = pRCM->m_arSendList.GetCount();
				LeaveCriticalSection(&pRCM->m_crRTSend);
				//ESMLog(5,_T("[%d] Leave CriticalSection"),nSendCnt);

				if(nCount/*pRCM->m_arSendList.GetCount()*/ 
					&& pRCM->m_nKTStopCnt >= nSendCnt)
				{
					//ESMLog(5,_T("[%d] Enter _MovieDataSendThread"),nSendCnt);
					EnterCriticalSection(&pRCM->m_crRTSend);
					//ESMLog(5,_T("[Load] Send List Size: %d"),pRCM->m_arSendList.GetCount());
					pstrArrSendFrame = (CStringArray*)pRCM->m_arSendList.GetAt(0);
					pRCM->m_arSendList.RemoveAt(0);
					LeaveCriticalSection(&pRCM->m_crRTSend);
					//ESMLog(5,_T("[%s] Leave _MovieDataSendThread - %d"),
					//	ESMGetSecIdxFromPath(pstrArrSendFrame->GetAt(3)),pRCM->m_arSendList.GetCount());
					HANDLE ThreadHandle;
					ThreadSendStringData* pSendData = new ThreadSendStringData;
					pSendData->nSocIndex	= 0;
					pSendData->pESMRCManager = pRCM;
					pSendData->pstrMovieArr = pstrArrSendFrame;

					ThreadHandle = (HANDLE)_beginthread( _MovieDataSendThread, 0, (void*)pSendData);
					nSendCnt++;

					pRCM->m_nKTSendTime += (10000 - nGap);
					nGap = 0;
				}
				else if(pRCM->m_nKTStopCnt <= nSendCnt)
				{
					//ESMLog(5,_T("[RCManager] Exit......"));
					break;
				}
				else
				{
					nGap = nTime - pRCM->m_nKTSendTime;
					//ESMLog(5,_T("[ELSE]%d"),nCount);
					//return;
				}

				//pRCM->m_nKTSendTime += (10000 - nGap);
			}
			if(pRCM->GetKTThreadStop() == TRUE)
				break;

			Sleep(1);
		}
	}
	pRCM->SetKTThreadStop(FALSE);
	pRCM->SetKTThreadRun(FALSE);
	ESMLog(5,_T("Exit _KTSendThread"));
}

void _FrameDataEncodeThread(void *param)
{
	ThreadSendYUVData* pSendData = (ThreadSendYUVData*)param;
	int nType = pSendData->nSocIndex;
	CESMRCManager* pRCM = pSendData->pESMRCManager;	
	MakeYUVInfo* pYUVInfo = pSendData->pYUVInfo;	
	delete pSendData;

	int nFrameSize = pYUVInfo->pArrYImage->size();
	CString strSavePath = pYUVInfo->strPath;
	cv::Size *pszOutputSize = pYUVInfo->pszOutputSize;
	vector<MakeFrameInfo>* pArrFrameInfo = pYUVInfo->pArrFrameInfo;

	int nStart = GetTickCount();
	FFmpegManager ffmpegMgr;
	BOOL bEncode = ffmpegMgr.InitEncode(strSavePath,30,AV_CODEC_ID_MPEG2VIDEO,15,3840,2160);
/*
	namedWindow("Y",CV_WINDOW_FREERATIO);
	namedWindow("U",CV_WINDOW_FREERATIO);
	namedWindow("V",CV_WINDOW_FREERATIO);*/

	if(bEncode)
	{
		int nPacketSize = 0;
		for(int i = 0 ; i < nFrameSize; i++)
		{
			ffmpegMgr.EncodePushUsingYUV(pYUVInfo->pArrYImage->at(i),pYUVInfo->pArrUImage->at(i),
				pYUVInfo->pArrVImage->at(i),3840,2160);

/*
			imshow("Y",pYUVInfo->pArrYImage->at(i));
			imshow("U",pYUVInfo->pArrUImage->at(i));
			imshow("V",pYUVInfo->pArrVImage->at(i));
			waitKey(0);*/
		}
		
		ffmpegMgr.CloseEncode(TRUE);

		int nEnd = GetTickCount();

		//sprintf(pArrFrameInfo->at(0).strFramePath,"%S",strSavePath);
		//memcpy(pArrFrameInfo->at(0).strFramePath,strSavePath,sizeof(strSavePath));
		//pArrFrameInfo->at(0).strFramePath = strSavePath.GetBuffer(strSavePath.GetLength());
		_tcscpy_s(pArrFrameInfo->at(0).strFramePath,MAX_PATH,strSavePath);

		pRCM->SendFrameInfo(pArrFrameInfo);
		
		ESMLog(5,_T("[%d]Encoding Time: %d (%dx%d)"),pArrFrameInfo->at(0).nSecIdx,nEnd - nStart,
			pszOutputSize->width,pszOutputSize->height);
	}
	else
	{
		ESMLog(0,_T("[%d]Encoding Fail"),pArrFrameInfo->at(0));
	}

	//Memory Delete
	for(int i = 0 ; i < pYUVInfo->pArrYImage->size(); i++)
	{
		pYUVInfo->pArrYImage->at(i).release();
		pYUVInfo->pArrUImage->at(i).release();
		pYUVInfo->pArrVImage->at(i).release();
	}
	pYUVInfo->pArrYImage->clear();
	pYUVInfo->pArrUImage->clear();
	pYUVInfo->pArrVImage->clear();

	delete pYUVInfo->pArrYImage;
	delete pYUVInfo->pArrUImage;
	delete pYUVInfo->pArrVImage;

	pYUVInfo->pArrYImage = NULL;
	pYUVInfo->pArrUImage = NULL;
	pYUVInfo->pArrVImage = NULL;

	//MakeFrameInfo pFrameInfo;

	//for(int i = 0 ; i < pArrFrameInfo->size(); i++)
	//{
	//	pFrameInfo = pArrFrameInfo->at(i);
	//	if(pFrameInfo.pYUVImg)
	//	{
	//		delete[] pFrameInfo.pYUVImg;
	//		pFrameInfo.pYUVImg = NULL;
	//	}

	//	if(pFrameInfo.Image)
	//	{
	//		delete []pFrameInfo.Image;
	//		pFrameInfo.Image = NULL;
	//	}	
	//}
	//pArrFrameInfo->clear();
	//delete pArrFrameInfo;
	delete pYUVInfo;
}

BOOL CESMRCManager::ConnectToRemote()
{
	//connect 시 첫촬영 delay time 적용
	ESMSetFirstConnect(TRUE);

	if(m_pServerSocket)
		Disconnect();

	char szHostIP[50];
	char* strId = ESMUtil::CStringToChar(m_strIP);
	memcpy(szHostIP, strId, sizeof(char) * 50);
	if( strId)
	{
		delete strId;
		strId = NULL;
	}

	//m_pTCPSocket = new CESMTCPSocket(CLIENTMODE);

	int nPort = m_nRemotePort;


	EnterCriticalSection(&_ESMRCMGR);
	m_pServerSocket = new CESMTCPSocket;
	m_pServerSocket->Create(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0);
	int ret = m_pServerSocket->Connect(szHostIP, nPort);
	LeaveCriticalSection (&_ESMRCMGR);

	if(ret != 0)
	{
		Disconnect();	
		return FALSE;
	}

	ThreadSendData* pThreadSendData = new ThreadSendData;
	pThreadSendData->nSocIndex = 0;
	pThreadSendData->pESMRCManager = (CESMRCManager*)this;
	m_hAcceptThread[0] = (HANDLE) _beginthreadex(NULL, 0, SockRecvThread, (void *)pThreadSendData, 0, NULL);

	m_bConnected	= TRUE;
	//m_bAccept		= FALSE;
	m_bNetMode	= TRUE;

	//ESMLog(3,_T("[CESMRCManager] Connect"));

	return 0;
}

void CESMRCManager::SendMovieFinish(CString strPath)
{
	FILE *fp;
	CString strLog;

	char filename[255] = {0,};	
	sprintf(filename, "%S", strPath);

	fp = fopen(filename, "rb");

	if(fp == NULL)
	{
		strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), strPath);
		ESMLog(0,strLog);
		return;
	}

	strLog.Format(_T("[#NETWORK] Send File [%s]"), strPath);
	//ESMLog(1,strLog);

	fseek(fp, 0L, SEEK_END);
	int nLength = ftell(fp);
	char* buf = new char[nLength];
	fseek(fp, 0, SEEK_SET);
	fread(buf, sizeof(char), nLength, fp);

	RCPHeader packetheader;
	char* bSendbuf = NULL;

	//ESMLog(5,_T("[#NETWORK] Send Request Data"));

	packetheader.command = RCPCMD_REQUEST_DATA_TOSERVER;
	if(!strPath.IsEmpty())
	{
		bSendbuf = new char[sizeof(RCP_FileSave_Info) + sizeof(RCPHeader) + nLength ];
		m_pFileBody = (RCP_FileSave_Info*)(bSendbuf + sizeof(RCPHeader));
		
		char* strId = ESMUtil::CStringToChar(strPath);
		
		memcpy(m_pFileBody->szFileName, strId, sizeof(char) * 128);
		if( strId)
		{
			delete strId;
			strId = NULL;
		}

		memcpy(m_pFileBody->szFileName + sizeof(char) * 128, (void*)buf, nLength);
		
		if(buf)
			delete (void*)buf;

		packetheader.param1 = sizeof(RCP_FileSave_Info);
		packetheader.param2 = nLength;
		packetheader.param3 = 100;
		packetheader.bodysize = sizeof(RCP_FileSave_Info) + nLength;

	}

	strcpy(packetheader.protocol, "RCP1.0");
	packetheader.command = RCPCMD_REQUEST_DATA_TOSERVER;
	
	int length = sizeof(RCPHeader) + packetheader.bodysize;
	int sent = 0;
	memcpy(bSendbuf, &packetheader, sizeof(RCPHeader));

	//0 or 1 select
	int nSock = 0;
	if(ESMGetProcessorShare())
		nSock = 1;

	if(m_pClientSocket[nSock])
	{
		int sent = m_pClientSocket[nSock]->Send(bSendbuf, length);
		ESMLog(5,_T("[#NETWORK] Send - %d"),sent);
	}

	fclose(fp);
	//ESMLog(5,_T("[#NETWORK] Send Movie Finish [ %s ]"),strPath);
}

void CESMRCManager::SendRemote(int nMode, int nData)
{
	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;

	strcpy(packetheader->protocol, "RCP1.0");
	
	packetheader->command   = nMode;
	//packetheader->command   = RCPCMD_REMOTE_START;
	//packetheader->command   = RCPCMD_REMOTE_STOP;
	//packetheader->command   = RCPCMD_REMOTE_MAKE;
	
	packetheader->bodysize  = 0;

	packetheader->param1 = nData;

	int length = sizeof(RCPHeader);	
	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
}

//171222 wgkim
void CESMRCManager::BackupAllFileCount()
{
	m_bCheckConnect = FALSE;

	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;

	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_BACKUP_FILECOUNT_ALL;
	packetheader->bodysize  = 0;

	int length = sizeof(RCPHeader);	
	packetheader->param1 = 1;
	packetheader->param2 = 0;

	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
}

void CESMRCManager::BackupStart(bool bUseFolderList, bool bUseDeleteAfterTransfer)
{
	int nUseFolder = 0;
	if (bUseFolderList) nUseFolder = 1;
	
	int nUseDelete = 0;
	if (bUseDeleteAfterTransfer) nUseDelete = 1;

	m_bCheckConnect = FALSE;	

	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;

	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_BACKUP_START;
	packetheader->bodysize  = 0;

	int length = sizeof(RCPHeader);	
	packetheader->param1 = 1;
	packetheader->param2 = nUseDelete;
	packetheader->param3 = nUseFolder;

	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
}

void CESMRCManager::BackupEnd()
{
	m_bCheckConnect = FALSE;

	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;

	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_BACKUP_END;
	packetheader->bodysize  = 0;

	int length = sizeof(RCPHeader);	
	packetheader->param1 = 1;
	packetheader->param2 = 0;

	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
}

void CESMRCManager::BackupCurrentFileCount()
{
	m_bCheckConnect = FALSE;

	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;

	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_BACKUP_FILECOUNT_CURRENT;
	packetheader->bodysize  = 0;

	int length = sizeof(RCPHeader);	
	packetheader->param1 = 1;
	packetheader->param2 = 0;

	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
}

void CESMRCManager::SetBackupMgr(CESMBackup* pBackupMgr)
{
	m_pBackupMgr = pBackupMgr;
}

void CESMRCManager::DeleteSelectList()
{
	char buf[1024] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;

	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_BACKUP_DIRECT_DELETE;
	packetheader->bodysize  = 0;

	int length = sizeof(RCPHeader);	
	packetheader->param1 = 1;

	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
}
BOOL CESMRCManager::ConnectToAJA(CString strIP,int nPort)
{
	hHandle = NULL;
	if(m_pServerSocket)
		Disconnect();

	char szHostIP[50];
	char* strId = ESMUtil::CStringToChar(strIP);
	memcpy(szHostIP, strId, sizeof(char) * 50);
	if( strId)
	{
		delete strId;
		strId = NULL;
	}

	//m_pTCPSocket = new CESMTCPSocket(CLIENTMODE);

	//int nPort = nPort;
	
	EnterCriticalSection(&_ESMRCMGR);
	m_pServerSocket = new CESMTCPSocket;
	m_pServerSocket->Create(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0);
	int ret = m_pServerSocket->Connect(szHostIP, nPort);
	LeaveCriticalSection (&_ESMRCMGR);

	if(ret != 0)
	{
		Disconnect();	
		return FALSE;
	}
	//ThreadSendData* pThreadSendData = new ThreadSendData;
	//pThreadSendData->nSocIndex = 0;
	//pThreadSendData->pESMRCManager = (CESMRCManager*)this;
	//m_hAcceptThread[0] = (HANDLE) _beginthreadex(NULL, 0, SockRecvThread, (void *)pThreadSendData, 0, NULL);

	ThreadSendData* pThreadSendData = new ThreadSendData;
	pThreadSendData->nSocIndex = 0;
	pThreadSendData->pESMRCManager = this;
	HANDLE hHandle = (HANDLE) _beginthreadex(NULL, 0, SockRecvThread, (void *)pThreadSendData, 0, NULL);
	CloseHandle(hHandle);

	m_bConnected	= TRUE;
	//m_bAccept		= FALSE;
	m_bNetMode	= TRUE;

	return TRUE;
}
void CESMRCManager::SendToAJA(ESMEvent* pMsg)
{
	char strTemp[100];

	char buf[5128] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;
	char *pBodyData = buf + sizeof(RCPHeader) ;
	CDSCItem* pItem = NULL;
	char* bSendbuf = NULL;

	strcpy(packetheader->protocol, "RCP1.0");
	switch(pMsg->message)
	{
	case F4DC_PROGRAM_STATE:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader->command   = F4DC_PROGRAM_STATE;
			packetheader->param1	= pMsg->nParam1;
			packetheader->param2	= movie_next_frame_time; //FrameRate
			packetheader->param3	= movie_frame_per_second;//Total frame per file
		}
		break;
	case F4DC_MAKING_START:
		{
			CString* pStr = (CString*)pMsg->pDest;

			packetheader->bodysize = pStr->GetLength();

			bSendbuf = new char[ sizeof(RCPHeader) + packetheader->bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);

			memset(pBodyData,0,packetheader->bodysize);

			char* str = ESMUtil::CStringToChar(*pStr);
			memcpy(pBodyData,str,sizeof(char)*pStr->GetLength());

			packetheader->command = F4DC_MAKING_START;
			packetheader->param1  = pMsg->nParam1;
			packetheader->param2	= movie_next_frame_time; //FrameRate
			packetheader->param3	= movie_frame_per_second;//Total frame per file
			if(str)
			{
				delete str;
				str = NULL;
			}
			if(pStr)
			{
				delete pStr;
				pStr = NULL;
			}
		}
		break;
	case F4DC_MAKING_TOTALCNT:
		{
			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader->command   = F4DC_MAKING_TOTALCNT;
			packetheader->param1	= pMsg->nParam1;
			packetheader->param2	= pMsg->nParam2;
		}
		break;
	case F4DC_ADJUST_SEND:
		{
			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);

			// Camera numbers
			UINT nSize = pMsg->nParam1;
			int nSizeAdj = sizeof(_RCP_ADJUST_INFO);
			int nSizeInt = sizeof(UINT);

			packetheader->command = F4DC_ADJUST_SEND;

			// body -> Camera Number + ( camera name length + camera name + adjustinfo ) * num
			packetheader->bodysize = sizeof(UINT) + nSizeAdj * nSize + nSizeInt * nSize;
			CString strDSC;
			for ( int i=0 ; i<nSize ; i++ )
			{
				pItem = (CDSCItem*)arDSCList.GetAt(i);
				strDSC =  pItem->GetDeviceDSCID();
				UINT nSizeDsc = strDSC.GetLength();
				packetheader->bodysize = packetheader->bodysize + nSizeDsc;
			}
			bSendbuf = new char[sizeof(RCPHeader) + packetheader->bodysize ];
			pBodyData = bSendbuf + sizeof(RCPHeader);


			memcpy(pBodyData, &nSize, nSizeInt );

			pBodyData = pBodyData + nSizeInt;
			int nMarginX = ESMGetMarginX();
			int nMarginY = ESMGetMarginY();
			//// [length of cam name][cam name][adjInfo] + [length of cam name][cam name][adjInfo]...
			for ( int i=0 ; i<nSize ; i++ )
			{
				pItem = (CDSCItem*)arDSCList.GetAt(i);

				_RCP_ADJUST_INFO adjInfo;
				adjInfo.AdjAngle = pItem->GetDSCAdj(DSC_ADJ_A);
				adjInfo.AdjSize = pItem->GetDSCAdj(DSC_ADJ_SCALE);
				adjInfo.AdjptRotate.x = pItem->GetDSCAdj(DSC_ADJ_RX);
				adjInfo.AdjptRotate.y = pItem->GetDSCAdj(DSC_ADJ_RY);
				adjInfo.AdjMove.x = pItem->GetDSCAdj(DSC_ADJ_X);
				adjInfo.AdjMove.y = pItem->GetDSCAdj(DSC_ADJ_Y);
				adjInfo.nWidth = pItem->GetWidth();
				adjInfo.nHeight = pItem->GetHeight();
				adjInfo.stMargin = pItem->GetMargin();

				CString strDSC =  pItem->GetDeviceDSCID();
				UINT nSizeDsc = strDSC.GetLength();

				char *cDSC = new char[nSizeDsc + 1];
				strcpy(cDSC,CT2A(strDSC));

				//length of cam name
				memcpy(pBodyData, &nSizeDsc, nSizeInt);
				int ntest;
				memcpy(&ntest, pBodyData ,sizeof(int) );
				pBodyData = pBodyData + nSizeInt;


				// came name
				memcpy(pBodyData, cDSC, nSizeDsc);
				char svName[5];
				memcpy(&svName, pBodyData ,nSizeDsc );
				pBodyData = pBodyData + nSizeDsc;

				// adjInfo
				memcpy(pBodyData, &adjInfo, nSizeAdj);
				pBodyData = pBodyData + nSizeAdj;

				delete[] cDSC;
			}			
		}
		break;
	case F4DC_GPUFRAMEPATH_SEND:
		{
#if 1
			vector<MakeFrameInfo>* pMovieInfo = (vector<MakeFrameInfo>*)pMsg->pParam;
			MakeFrameInfo* pMakeFrameInfo;

			int nInfoCount = pMovieInfo->size();
			packetheader->bodysize = sizeof(MakeFrameInfo) * nInfoCount;

			bSendbuf = new char[ sizeof(RCPHeader) + packetheader->bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);

			for( int i = 0; i< nInfoCount; i++)
			{
				pMakeFrameInfo = &((pMovieInfo->at(i)));

				memcpy(pBodyData, pMakeFrameInfo, sizeof(MakeFrameInfo));
				pBodyData = pBodyData + sizeof(MakeFrameInfo);
			}

			packetheader->command = F4DC_GPUFRAMEPATH_SEND;

			packetheader->param1 = nInfoCount;
			packetheader->param2 = pMsg->nParam1;
			packetheader->param3 = ESMGetReverseMovie();
			delete pMovieInfo;
		}
#else
			AJASendGPUFrameInfo* pAJASendInfo = (AJASendGPUFrameInfo*) pMsg->pParam;

			packetheader->bodysize = sizeof(AJASendGPUFrameInfo);

			bSendbuf = new char[ sizeof(RCPHeader) + packetheader->bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);

			memcpy(pBodyData,pAJASendInfo,sizeof(AJASendGPUFrameInfo));

			packetheader->command = F4DC_GPUFRAMEPATH_SEND;

			delete pAJASendInfo;
		}
#endif
		break;
	case F4DC_CPUFRAME_INFO:
		{
			/*bSendbuf = new char[sizeof(RCPHeader)];
			packetheader.command = RCPCMD_FRAME_LOAD_TIME_TOSERVER;
			packetheader.param1	 = pMsg->nParam1;
			packetheader.param2	 = pMsg->nParam2;	
			packetheader.param3	 = pMsg->nParam3;	*/

			bSendbuf = new char[sizeof(RCPHeader)];
			packetheader->command   = F4DC_CPUFRAME_INFO;
			packetheader->param1	= pMsg->nParam1;
		}
		break;
	case F4DC_CPUFRAME_MAKE_FINISH:
		{
			CString* pStr = (CString*) pMsg->pDest;

			packetheader->bodysize = pStr->GetLength()*sizeof(char);

			bSendbuf = new char[sizeof(RCPHeader) + packetheader->bodysize];
			memset(bSendbuf,0,packetheader->bodysize);
			pBodyData = bSendbuf + sizeof(RCPHeader);
			
			char* str = ESMUtil::CStringToChar(*pStr);
			memcpy(pBodyData,str,packetheader->bodysize);

			packetheader->command	= F4DC_CPUFRAME_MAKE_FINISH;
			packetheader->param1	= pMsg->nParam1;
			packetheader->param2	= pMsg->nParam2;
		}
		break;
	case F4DC_AUTO_ADD:
		{
			CString* pStr = (CString*) pMsg->pDest;

			packetheader->bodysize = pStr->GetLength()*sizeof(char);

			bSendbuf = new char[sizeof(RCPHeader) + packetheader->bodysize];
			memset(bSendbuf,0,packetheader->bodysize);
			pBodyData = bSendbuf + sizeof(RCPHeader);

			char* str = ESMUtil::CStringToChar(*pStr);
			memcpy(pBodyData,str,packetheader->bodysize);

			packetheader->command = F4DC_AUTO_ADD;
		}
		break;
	case F4DC_SEND_CPU_FILE:
		{
			packetheader->command = F4DC_SEND_CPU_FILE;
			//pMsg-> = bodysize;-파일 크기
			//pMsg->nParam3 = pHeader->param3;-전체 크기
			
			if(pMsg->pDest)
			{
				packetheader->bodysize = pMsg->nParam3;

				bSendbuf = new char[sizeof(RCPHeader) + packetheader->bodysize ];
				memset(bSendbuf,0,packetheader->bodysize);
				pBodyData = bSendbuf + sizeof(RCPHeader);
				char*pTempBuf = new char[pMsg->nParam2];
				memcpy(pTempBuf,(LPARAM*)pMsg->pDest,pMsg->nParam2);
				memcpy(pBodyData,pTempBuf+128,packetheader->bodysize);
				//memcpy(pBodyData,pTempBuf+128,packetheader->bodysize);

				if(pMsg->pDest)
				{
					delete (LPARAM*)pMsg->pDest;
					pMsg->pDest = NULL;
				}

				if(pTempBuf)
				{
					delete[] pTempBuf;
					pTempBuf = NULL;
				}
			}
			packetheader->param1 = pMsg->nParam3;
			packetheader->param2 = pMsg->nParam1;
		}
		break;
	default:
		{
			ESMLog(5,_T("[AJA] Error Messeage"));

		}
		break;
	}

	int length = sizeof(RCPHeader) + packetheader->bodysize;
	int sent = 0;
	memcpy(bSendbuf, packetheader, sizeof(RCPHeader));
	if(m_pServerSocket)
		sent = m_pServerSocket->Send(bSendbuf, length);

	if( bSendbuf)
	{
		delete[] bSendbuf;
		bSendbuf = NULL;
	}
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
	//if(packetheader)
	//{
	//	delete packetheader;
	//	packetheader = NULL;
	//}

	return;
}
void CESMRCManager::ReceiveFromAJA(char* headerbuf, char* bodybuf, int packetsize)
{
	RCPHeader* pHeader = (RCPHeader*)headerbuf;

	DWORD bodysize = packetsize;
	DWORD command  = pHeader->command;
	char *pData = NULL;

	switch(command)
	{
	case AJA_BOARDSTATE:
		{
			int nParam1 = pHeader->param1;
			int nBodySize = pHeader->bodysize;
			char *strData = new char[nBodySize +1];//[MAX_PATH];

			memset(strData,0,pHeader->bodysize + 1);
			memcpy(strData,bodybuf,nBodySize);

			CString str = ESMUtil::CharToCString(strData);
			ESMSetAJAMaking(FALSE);
			ESMSetAJAScreen(nParam1);
			ESMSetAJADivProcess(pHeader->param2);

			ESMLog(5,str);
		}
		break;
	case AJA_MAKING_START:
		{
			int nBodySize = pHeader->bodysize;
			char *strData = new char[nBodySize +1];//[MAX_PATH];

			memset(strData,0,pHeader->bodysize + 1);
			memcpy(strData,bodybuf,nBodySize);
			CString str = ESMUtil::CharToCString(strData);
			int nMakingTime = GetTickCount() - ESMGetStartMakeTime();
			ESMSetMakeMovieTime(nMakingTime);

			ESMLog(5,_T("[CHECK_H] AJA Play Start"));
			if(strData)
			{
				delete strData;
				strData = NULL;
			}
		}
		break;
	case AJA_MAKING_FINISH:
		{
			int nParam1 = pHeader->param1;
			int nBodySize = pHeader->bodysize;
			char *strData = new char[nBodySize +1];//[MAX_PATH];

			memset(strData,0,pHeader->bodysize + 1);
			memcpy(strData,bodybuf,nBodySize);
			CString str = ESMUtil::CharToCString(strData);
			ESMSetAJAMaking(pHeader->param1);
			if(nParam1 == FALSE)//Making Finish
			{
				ESMLog(5,_T("AJA Making Finish"));
				ESMEvent* pMsg1 = NULL;
				pMsg1 = new ESMEvent();
				pMsg1->message = WM_ESM_VIEW_SAVE_INFO;
				pMsg1->nParam1 = !nParam1;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg1);
			}

			if(strData)
			{
				delete strData;
				strData = NULL;
			}
		}
		break;
	case AJA_ENCODING_FINISH:
		{
			int nParam1 = pHeader->param1;
			int nBodySize = pHeader->bodysize;
			char *strData = new char[nBodySize +1];//[MAX_PATH];

			memset(strData,0,pHeader->bodysize + 1);
			memcpy(strData,bodybuf,nBodySize);
			CString strPath = ESMUtil::CharToCString(strData);

			ESMLog(5,_T("[CHECK_J] Encoding finish: %s"),strPath);

			CESMFileOperation fo;
			if(fo.IsFileExist(strPath))
			{
				CESMIni ini;
				CString strFile;
				strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_SDIPLAYER);

				int nIndex = strPath.ReverseFind('\\') + 1;
				CString strFileName = strPath.Right(strPath.GetLength() - nIndex);

				if(ini.SetIniFilename (strFile))
				{
					ini.WriteString(STR_SECTION_SDIPLAYER,STR_STATE,_T("O"));
					ini.WriteString(STR_SECTION_SDIPLAYER,STR_FILENAME,strFileName);
					ini.WriteString(STR_SECTION_SDIPLAYER,STR_FILEPATH,strPath);
				}	
			}

			if(ESMGetValue(ESM_VALUE_GIF_MAKING))
			{
				SYSTEMTIME st;
				GetLocalTime(&st);

				CString strDate = _T("");
				strDate.Format(_T("%04d%02d%02d"),st.wYear,st.wMonth,st.wDay); 

				CTimeLineView timeline;
				timeline.CreateGifFile(strPath,strDate,_T(""));
			}
			if(strData)
			{
				delete strData;
				strData = NULL;
			}
		}
		break;
	case AJA_ENCODING_START:
		{
			int nParam1 = pHeader->param1;
			int nBodySize = pHeader->bodysize;
			char *strData = new char[nBodySize +1];//[MAX_PATH];

			memset(strData,0,pHeader->bodysize + 1);
			memcpy(strData,bodybuf,nBodySize);
			CString str = ESMUtil::CharToCString(strData);
			ESMSetAJAMaking(pHeader->param1);
			ESMLog(5,_T("[CHECK_I] Encoding Start"));
			if(strData)
			{
				delete strData;
				strData = NULL;
			}
		}
		break;
	case AJA_PLAY_SCREEN:
		{
			int nState = pHeader->param1;
			ESMSetAJAScreen(nState);
		}
		break;
	default:
		{

		}
		break;
	}
}
void CESMRCManager::SendRefereeData(ESMEvent* pMsg)
{
	if(!m_bConnected)
		return;

	if(!m_pClientSocket[RCPMODE_REMOTE])
		return;

	char* bSendbuf = NULL;
	char* pBodyData = NULL;
	RCPHeader packetheader;
		
	switch(pMsg->message)
	{
	
	case WM_ESM_NET_REFEREE_DATA_TO_CLIENT:
		{
			ESMLog(5,_T("[#NETWORK] Send Referee Data"));

			packetheader.command = RCPCMD_REFEREE_DATA_TO_CLIENT;
			int* pLength = (int*)pMsg->pParent;
			int nLength = *pLength;
			if(pMsg->pParam)
			{
				bSendbuf = new char[sizeof(RCP_FileSave_Info) + sizeof(RCPHeader) + nLength ];
				m_pFileBody = (RCP_FileSave_Info*)(bSendbuf + sizeof(RCPHeader));

				CString* pstrTemp_ = (CString*)pMsg->pParam;
				CString pstrTemp;
				pstrTemp.Format(_T("%s"),*pstrTemp_);
				char* strId = ESMUtil::CStringToChar(pstrTemp);
				if(pstrTemp_)
				{
					delete pstrTemp_;
					pstrTemp_ = NULL;
				}
				memcpy(m_pFileBody->szFileName, strId, sizeof(char) * 128);
				if( strId)
				{
					delete strId;
					strId = NULL;
				}

				memcpy(m_pFileBody->szFileName + sizeof(char) * 128, (void*)pMsg->pDest, nLength);
				//pItem = NULL;
				if(pMsg->pDest)
					delete (void*)pMsg->pDest;

				packetheader.param1 = pMsg->nParam1;
				packetheader.param2 = pMsg->nParam2;
				packetheader.param3 = pMsg->nParam3;
				packetheader.bodysize = sizeof(RCP_FileSave_Info) + nLength;
			}
		}
		break;
	case WM_ESM_NET_REFEREE_INFO:
		{
			vector<ESMRefereeInfo>* pRefereeInfo = (vector<ESMRefereeInfo>*)pMsg->pParam;
			
			int nInfo = pRefereeInfo->size();
			packetheader.bodysize = sizeof(ESMRefereeInfo) * nInfo;

			bSendbuf = new char[ sizeof(RCPHeader) + packetheader.bodysize];
			pBodyData = bSendbuf + sizeof(RCPHeader);

			for( int i = 0; i< nInfo; i++)
			{
				ESMRefereeInfo* pDataInfo = NULL;
				pDataInfo = &((pRefereeInfo->at(i)));

				memcpy(pBodyData, pDataInfo, sizeof(ESMRefereeInfo));
				pBodyData = pBodyData + sizeof(ESMRefereeInfo);
			}

			packetheader.command = RCPCMD_REFEREE_SET_INFO;

			packetheader.param1 = nInfo;
			packetheader.param2 = pMsg->nParam2;
			packetheader.param3 = pMsg->nParam3;

			delete pRefereeInfo;

			
		}
		break;
	default:
		ESMLog(0, _T("Send Referee Fail Not message[%d]"),pMsg->message);
		return;
	}

	strcpy(packetheader.protocol, "RCP1.0");
	
	int length = sizeof(RCPHeader) + packetheader.bodysize;
	int sent = 0;
	memcpy(bSendbuf, &packetheader, sizeof(RCPHeader));

	if(m_pClientSocket[RCPMODE_REMOTE])
		sent = m_pClientSocket[RCPMODE_REMOTE]->Send(bSendbuf, length);

	if( bSendbuf)
	{
		delete[] bSendbuf;
		bSendbuf = NULL;
	}
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	return;
}
void CESMRCManager::SendMakeMultiViewData(MakeMultiView* pViewData)
{
	char buf[5128] = {0};
	RCPHeader* packetheader = (RCPHeader*)buf;
	char *pBodyData = buf + sizeof(RCPHeader);

	strcpy(packetheader->protocol, "RCP1.0");
	packetheader->command   = RCPCMD_MULTIVIEW_MAKING;
	packetheader->bodysize	= sizeof(MakeMultiView);

	memcpy(pBodyData, pViewData, sizeof(MakeMultiView));

	int length = sizeof(RCPHeader) + sizeof(MakeMultiView);

	if(m_pServerSocket)
		int sent = m_pServerSocket->Send(buf, length);
	
	ESMLog(5,_T("[MULTIVIEW] - %d send"),pViewData->nMovIdx);

	SetMultiViewStart(TRUE);
}