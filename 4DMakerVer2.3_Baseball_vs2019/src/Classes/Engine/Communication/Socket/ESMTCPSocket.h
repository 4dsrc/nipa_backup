/////////////////////////////////////////////////////////////////////////////
//
// ESMTCPSocket.h : Defines the class behaviors for the application.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  ESMLab, Inc., and may not be copied
//  nor disclosed except upon written agreement
//  by ESMLab, Inc..
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author yongmin.lee (yongmin@esmlab.com)
// @Date	 2012-4-4
//
/////////////////////////////////////////////////////////////////////////////

#pragma once


class CESMTCPSocket 
{
public:
	CESMTCPSocket();
	CESMTCPSocket(SOCKET sock);
	~CESMTCPSocket();

	int GetSocketHandle();
	int Create(IN int af, IN int type, IN int protocol, IN int sendtimeout=0, IN int recvtimeout=0);
	int CreateServer(IN int af, IN int type, IN int protocol, IN int port);
	int SetSockOpt(IN int optname, IN int timeout);
	int Close();
	int GetSockOpt(IN int optname, OUT UINT& optval);
	int Bind(IN USHORT hostport);
	int Bind(IN char* hostip, IN USHORT hostport);
	int Connect(IN char* hostip, IN USHORT hostport);
	int Connect(IN char* hostip, IN USHORT hostport, IN int timeoutsec);

	int Listen();
	SOCKET Accept();
	SOCKET Accept(IN struct timeval timeout);
	int Send(IN const char* sendbuf, IN int buflen);
	int Send(IN const char* sendbuf, IN int buflen, IN int timeoutsec);
	int Recv(IN char* recvbuf, IN int buflen);
	int Recv(IN char* recvbuf, IN int buflen, IN int timeoutsec);
	int GetPeerAddr(OUT char* ipaddr, OUT USHORT& ipport);

	static DWORD WINAPI HandleRunner(IN void* param);
	static CString GetErrorDescription(int nErrorCode);

private:
	SOCKET m_hSocket;
	SOCKET m_hListenSocket;
	//HANDLE m_hRecvThread;

	void Run();
};
