/////////////////////////////////////////////////////////////////////////////
//
// ESMTCPSocket.cpp : Defines the class behaviors for the application.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  ESMLab, Inc., and may not be copied
//  nor disclosed except upon written agreement
//  by ESMLab, Inc..
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author yongmin.lee (yongmin@esmlab.com)
// @Date	 2013-09-28
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMTCPSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//------------------------------------------------------------------------------ 
//! @brief    Constructor
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
CESMTCPSocket::CESMTCPSocket()
{
	m_hSocket       = INVALID_SOCKET;
	m_hListenSocket	= INVALID_SOCKET;
	//m_hRecvThread   = INVALID_HANDLE_VALUE;
}

//------------------------------------------------------------------------------ 
//! @brief    Constructor
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
CESMTCPSocket::CESMTCPSocket(SOCKET sock)
{
	m_hSocket       = sock;
	//m_hRecvThread   = INVALID_HANDLE_VALUE;
}

//------------------------------------------------------------------------------ 
//! @brief    Destructor
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
CESMTCPSocket::~CESMTCPSocket()
{
	Close();
}

int CESMTCPSocket::GetSocketHandle()
{
	return m_hSocket;
}


//------------------------------------------------------------------------------ 
//! @brief    Creates a socket and sets socket option.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note  
//! @return   if no error occurs, this function return zero.        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::Create(int af, int type, int protocol, int sendtimeout, int recvtimeout)
{
	//TCP:socket(AF_INET, SOCK_STREAM, 0)  UDP:socket(AF_INET, SOCK_DGRAM, 0)
	m_hSocket = socket(af, type, protocol);
	if(INVALID_SOCKET == m_hSocket)
	{
		return -1;
	}

	if(sendtimeout > 0)
	{
		return SetSockOpt(SO_SNDTIMEO, 1000);   //milliseconds
	}

	if(recvtimeout > 0)
	{
		return SetSockOpt(SO_RCVTIMEO, 1000);  //milliseconds
	}


	return 0;  //created
}

//------------------------------------------------------------------------------ 
//! @brief    Creates a socket and sets socket option.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note  
//! @return   if no error occurs, this function return zero.        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::CreateServer(int af, int type, int protocol, int port)
{
	//TCP:socket(AF_INET, SOCK_STREAM, 0)  UDP:socket(AF_INET, SOCK_DGRAM, 0)
	m_hListenSocket = socket(af, type, protocol);

	if(INVALID_SOCKET == m_hListenSocket)
		return -1;

	if(Bind(port))
		return -1;

	if(Listen())
		return -1;

	return 0;  //created
}

//------------------------------------------------------------------------------ 
//! @brief    
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
DWORD WINAPI CESMTCPSocket::HandleRunner(void* param)
{
	CESMTCPSocket* thread = (CESMTCPSocket*)param;

	thread->Run();

	return 0;
}

void CESMTCPSocket::Run()
{
	char recvbuf[8192];

	int recved = 0;

	while(1)
	{
		recved = recv(m_hSocket, recvbuf, 8192, 0);
		if(recved <= 0)
			break;
	}
}


//------------------------------------------------------------------------------ 
//! @brief    Closes an existing socket.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::Close()
{
	if(INVALID_SOCKET != m_hSocket)
	{
		if(SOCKET_ERROR == closesocket(m_hSocket))
		{
			return (-WSAGetLastError());
		}
	}

	if(INVALID_SOCKET != m_hListenSocket)
	{
		if(SOCKET_ERROR == closesocket(m_hListenSocket))
		{
			return (-WSAGetLastError());
		}
	}

	//if(WaitForSingleObject(m_hRecvThread, 3000) == WAIT_OBJECT_0) 
	//{
	//}

	//CloseHandle(m_hRecvThread);

	return 0;  //if no error occurs
}

//------------------------------------------------------------------------------ 
//! @brief    Sets a socket option.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::SetSockOpt(int optname, int timeout)
{
	int result = 0;  //if no error occurs

	switch(optname)
	{
	case SO_LINGER: //Lingers on close if unsent data is present.
		{
			struct linger zl;  //zero-linger	
			zl.l_onoff  = 1;	
			zl.l_linger = 0;
			if(SOCKET_ERROR == setsockopt(m_hSocket, SOL_SOCKET, SO_LINGER, (char*)&zl, sizeof(zl)))
			{
				result = -WSAGetLastError();
			}
			break;
		}
	case SO_RCVTIMEO: //Sets the timeout, in milliseconds, for blocking receive calls.
		{
			int rto = timeout; //receiving time-out
			if(SOCKET_ERROR == setsockopt(m_hSocket, SOL_SOCKET, SO_RCVTIMEO, (char*)&rto, sizeof(rto)))
			{
				result = -WSAGetLastError();
			}
			break;
		}
	case SO_SNDTIMEO: //Sets the timeout, in milliseconds, for blocking send calls. 
		{
			int sto = timeout; 
			if(SOCKET_ERROR == setsockopt(m_hSocket, SOL_SOCKET, SO_SNDTIMEO, (char*)&sto, sizeof(sto)))
			{
				result = -WSAGetLastError();
			}
			break;
		}
	default:
		{
			result = SOCKET_ERROR;
			break;
		}
	}

	return result;
}

//------------------------------------------------------------------------------ 
//! @brief    Retrieves a socket option.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note     optname : SO_RCVBUF/SO_SNDBUF
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
//optname : SO_RCVBUF, SO_SNDBUF
int CESMTCPSocket::GetSockOpt(int optname, unsigned int & optval)
{
	int optlen = sizeof(optval);

	if(SOCKET_ERROR == getsockopt(m_hSocket, SOL_SOCKET, optname, (char*)&optval, &optlen))
	{
		return -WSAGetLastError();
	}

	return 0; //if no error occurs
}


//------------------------------------------------------------------------------ 
//! @brief    Associates a local address with a socket.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::Bind(IN char* hostip, IN USHORT hostport)
{
	struct sockaddr_in hostaddr;
	memset(&hostaddr, 0, sizeof(hostaddr));
	hostaddr.sin_family      = AF_INET;
	hostaddr.sin_addr.s_addr = inet_addr(hostip);  
	hostaddr.sin_port        = htons(hostport);

	if(SOCKET_ERROR == bind(m_hListenSocket, (struct sockaddr*)&hostaddr, sizeof(hostaddr)))
	{
		return (-WSAGetLastError());
	}

	return 0; //if no error occurs
}

//------------------------------------------------------------------------------ 
//! @brief    Associates a local address with a socket.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::Bind(USHORT hostport)
{
	if(INVALID_SOCKET == m_hListenSocket)
	{
		return SOCKET_ERROR;
	}

	struct sockaddr_in hostaddr;
	memset(&hostaddr, 0, sizeof(hostaddr));
	hostaddr.sin_family      = AF_INET;
	hostaddr.sin_addr.s_addr = htonl(INADDR_ANY);  
	hostaddr.sin_port        = htons(hostport);

	if(SOCKET_ERROR == bind(m_hListenSocket, (struct sockaddr*)&hostaddr, sizeof(hostaddr)))
	{
		return (-WSAGetLastError());
	}

	return 0; //if no error occurs
}

//------------------------------------------------------------------------------ 
//! @brief    Establishes a connection to a specified socket.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::Connect(char* hostip, USHORT hostport)
{
	struct sockaddr_in hostaddr;
	memset(&hostaddr, 0, sizeof(hostaddr));
	hostaddr.sin_family      = AF_INET;
	hostaddr.sin_addr.s_addr = inet_addr(hostip);  
	hostaddr.sin_port        = htons(hostport);

	if(SOCKET_ERROR == connect(m_hSocket, (struct sockaddr*)&hostaddr, sizeof(hostaddr)))
	{
		int nError = WSAGetLastError();
		return (-WSAGetLastError());
	}

	//char peeraddr[16]= {0};
	//USHORT peerport = 0;
	//GetPeerAddr(peeraddr, peerport);

	//if(0 == strcmp(peeraddr, hostip))
	//{
	//	DWORD dwid = 0;
	//  m_hRecvThread = CreateThread(NULL, 0, &HandleRunner, (void*)this, 0, &dwid);

	//	return 0;
	//}

	return 0;  //if no error occurs
}

//------------------------------------------------------------------------------ 
//! @brief    Establishes a connection to a specified socket.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::Connect(char* hostip, USHORT hostport, int timeoutsec)
{
	struct sockaddr_in hostaddr;
	memset(&hostaddr, 0, sizeof(hostaddr));
	hostaddr.sin_family      = AF_INET;
	hostaddr.sin_addr.s_addr = inet_addr(hostip);  
	hostaddr.sin_port        = htons(hostport);

	ULONG argp = 1;  //non-blocking mode is enabled
	if(SOCKET_ERROR == ioctlsocket(m_hSocket, FIONBIO, &argp))  
	{
		return (-WSAGetLastError());
	}

	int connected = connect(m_hSocket, (struct sockaddr*)&hostaddr, sizeof(hostaddr));

	if(SOCKET_ERROR == connected)
	{
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(m_hSocket, &fds);

		struct timeval t;
		t.tv_sec  = timeoutsec;  //seconds
		t.tv_usec = 0;
		select(0, 0, &fds, 0, &t);
		if(!FD_ISSET(m_hSocket, &fds))
		{
			connected = -WSAGetLastError();  //failed to connect
		}
		else
		{
			connected = 1; //connected
		}
	}

	argp = 0; //blocking mode is enabled
	if(SOCKET_ERROR == ioctlsocket(m_hSocket, FIONBIO, &argp))
	{
		return (-WSAGetLastError());
	}

	// 	char peeraddr[16]= {0};
	// 	USHORT peerport = 0;
	// 	GetPeerAddr(peeraddr, peerport);
	// 
	// 	if(0 == strcmp(peeraddr, hostip))
	// 	{
	// 		return 0;
	// 	}

	return connected;
}

//------------------------------------------------------------------------------ 
//! @brief    Places a socket in a state in which it is listening for an incoming
//!           connection.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::Listen()
{
	if(SOCKET_ERROR == listen(m_hListenSocket, SOMAXCONN))  //5
	{
		return (-WSAGetLastError());
	}

	return 0;  //if no error occurs
}

//------------------------------------------------------------------------------ 
//! @brief    Permits an incoming connection attempt on a socket.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
SOCKET CESMTCPSocket::Accept()
{
	struct sockaddr_in clientaddr;
	memset(&clientaddr, 0, sizeof(clientaddr));
	int addrlen = sizeof(clientaddr);

	m_hSocket = accept(m_hListenSocket, (struct sockaddr*)&clientaddr, &addrlen);
	//if error occurs, return INVALID_SOCKET
	return m_hSocket;
	//  return accept(m_hSocket, (struct sockaddr*)&clientaddr, &addrlen);
}

//------------------------------------------------------------------------------ 
//! @brief    Permits an incoming connection attempt on a socket.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
SOCKET CESMTCPSocket::Accept(struct timeval timeout)
{
	fd_set fds;

	FD_ZERO(&fds);
	FD_SET(m_hSocket, &fds);;

	if(select(0, &fds, 0, 0, &timeout) == SOCKET_ERROR)
		return INVALID_SOCKET;

	if(FD_ISSET(m_hListenSocket, &fds))
		return accept(m_hSocket, 0, 0);

	return INVALID_SOCKET;
}

//------------------------------------------------------------------------------ 
//! @brief    Sends data on a connected socket.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::Send(const char* sendbuf, int buflen)
{
	if( m_hSocket == NULL)
		return 0;
	int sent = send(m_hSocket, sendbuf, buflen, 0);
	if(SOCKET_ERROR == sent)
	{
		return (-WSAGetLastError());
	}

	return sent;
}

//------------------------------------------------------------------------------ 
//! @brief    Sends data on a connected socket.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::Send(const char* sendbuf, int buflen, int timeoutsec)
{
	ULONG argp = 1;  //non-blocking mode is enabled
	// 	if(SOCKET_ERROR == ioctlsocket(m_hSocket, FIONBIO, &argp))  
	// 	{
	// 		return (-WSAGetLastError());
	// 	}

	int sent = send(m_hSocket, sendbuf, buflen, 0);
	if(SOCKET_ERROR == sent)
	{
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(m_hSocket, &fds);

		struct timeval t;
		t.tv_sec  = timeoutsec;  //seconds
		t.tv_usec = 0;
		select(0, 0, &fds, 0, &t);
		if(!FD_ISSET(m_hSocket, &fds))
		{
			sent = -WSAGetLastError();  //failed to connect
		}
		else
		{
			//connected = 0; //connected
			sent = send(m_hSocket, sendbuf, buflen, 0);
		}
	}

	argp = 0; //blocking mode is enabled
	// 	if(SOCKET_ERROR == ioctlsocket(m_hSocket, FIONBIO, &argp))
	// 	{
	// 		return (-WSAGetLastError());
	// 	}

	return sent;
}

//------------------------------------------------------------------------------ 
//! @brief    Receives data from a connected socket or a bound connectionless socket.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::Recv(char* recvbuf, int buflen)
{
	int recved = recv(m_hSocket, recvbuf, buflen, 0);
	if(0 == recved) //If the connection has been gracefully closed
	{
		return 0;
	}

	else if(SOCKET_ERROR == recved)
	{
		return (-WSAGetLastError());
	}

	return recved;
}

//------------------------------------------------------------------------------ 
//! @brief    Receives data from a connected socket or a bound connectionless socket.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::Recv(char* recvbuf, int buflen, int timeoutsec)
{
	ULONG argp = 1;  //non-blocking mode is enabled
	// 	if(SOCKET_ERROR == ioctlsocket(m_hSocket, FIONBIO, &argp))  
	// 	{
	// 		return (-WSAGetLastError());
	// 	}

	int recved = recv(m_hSocket, recvbuf, buflen, 0);
	if(SOCKET_ERROR == recved)
	{
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(m_hSocket, &fds);

		struct timeval t;
		t.tv_sec  = timeoutsec;  //seconds
		t.tv_usec = 0;
		select(0, &fds, 0, 0, &t);
		if(!FD_ISSET(m_hSocket, &fds))
		{
			recved = -WSAGetLastError();  //failed to connect
		}
		else
		{
			recved = recv(m_hSocket, recvbuf, buflen, 0);
		}
	}

	argp = 0; //blocking mode is enabled
	// 	if(SOCKET_ERROR == ioctlsocket(m_hSocket, FIONBIO, &argp))
	// 	{
	// 		return (-WSAGetLastError());
	// 	}

	return recved;

}

//------------------------------------------------------------------------------ 
//! @brief    Retrieves the address of the peer to which a socket is connected.
//! @date     2013-09-28
//! @owner    yongmin (yongmin@esmlab.com)
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
int CESMTCPSocket::GetPeerAddr(char* ipaddr, USHORT& ipport)
{
	struct sockaddr_in peer;
	memset(&peer, 0, sizeof(peer));
	int sizelen = sizeof(peer);

	if(SOCKET_ERROR == getpeername(m_hSocket, (struct sockaddr*)&peer, &sizelen))
	{
		return (-WSAGetLastError());
	}

	strcpy(ipaddr, inet_ntoa(peer.sin_addr));
	ipport = peer.sin_port;

	return 0;  //if no error occurs
}

//------------------------------------------------------------------------------ 
//! @brief    Retrieves the descriptions of the specified error code.
//! @date     2012-06-26
//! @owner    yongmin
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
CString CESMTCPSocket::GetErrorDescription(int nErrorCode)
{
	int nCode = -nErrorCode;
	CString strError = _T(""); 
	strError.Format(_T("Unknown error code(%d)"), nCode);

	switch(nCode)
	{
	case 0: //closed
		strError.Format(_T("The connection has been closed(%d)."), nCode);
		break;
	case WSAECONNRESET: //10054
		strError.Format(_T("An existing connection was forcibly closed by the remote host(%d)."), nCode);
		break;
	case WSAEISCONN: //10056
		strError.Format(_T("A connect request was made on an already connected socket(%d)."), nCode);
		break;
	case WSAECONNREFUSED: //10061
		strError.Format(_T("Web-Agent is not running(%d)."), nCode);
		break;
	}

	return strError;
}


