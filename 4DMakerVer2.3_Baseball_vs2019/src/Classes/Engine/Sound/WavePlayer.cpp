
/****************************************************************************
                 cSound.cpp  by  Elm?(elmue.de.vu)
****************************************************************************/

#include "stdafx.h"
#include "WavePlayer.h"

CWavePlayer::CWavePlayer()
{
}

CWavePlayer::~CWavePlayer()
{
}

/****************************************
       Plays Wav or Midi file
****************************************/

DWORD CWavePlayer::PlaySoundFile(CString strWaveFile)
{
	CString strExt = _tcsrchr (strWaveFile, '.');

	if (strExt.IsEmpty())
		return ERR_INVALID_FILETYPE;

	if (strExt.CompareNoCase(_T(".WAV")) == 0)
	{
		StopSoundFile();
		if (!PlaySound(strWaveFile, 0, SND_FILENAME | SND_ASYNC))
			return ERR_PLAY_WAV;

		return 0;
	}
	return ERR_INVALID_FILETYPE;
}

void CWavePlayer::StopSoundFile()
{
	PlaySound(0, 0, SND_PURGE); // Stop Wav
}

