
#include <Mmsystem.h>

#define  ERR_INVALID_FILETYPE 50123
#define  ERR_PLAY_WAV         50124

class CWavePlayer
{
public:
	CWavePlayer();
	virtual   ~CWavePlayer();

	DWORD      PlaySoundFile(CString strWaveFile);
	void       StopSoundFile();
};