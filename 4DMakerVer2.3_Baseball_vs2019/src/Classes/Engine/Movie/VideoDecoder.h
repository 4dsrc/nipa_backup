// Decoder.h: interface for the CVideoDecoder class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <gdiplus.h>
#include "dec_avc.h"	// H264
#include "dec_mp4v.h"	// Mpeg4
#include "decore.h"	// Penta

using namespace Gdiplus;		// GDI+ for Rendering


class CVideoDecoder  
{
public:
	CVideoDecoder();
	virtual ~CVideoDecoder();

	void InitDecoder();
	void ReleaseDecoder();

	long DecodeH264Frame( LPBYTE pInputFrameData, long lInputFrameSize, int nInputFrameWidth, int nInputFrameHeight, int nInputFrameType );
	long DecodeMpeg4Frame( LPBYTE pInputFrameData, long lInputFrameSize, int nInputFrameWidth, int nInputFrameHeight, int nInputFrameType );
	long DecodeMjpegFrame( LPBYTE pInputFrameData, long lInputFrameSize, int nInputFrameWidth, int nInputFrameHeight, int nInputFrameType );
	//long DecodePentaFrame( LPBYTE pInputFrameData, long lInputFrameSize, int nInputFrameWidth, int nInputFrameHeight, int nInputFrameType );

	void SetViewRect(RECT rect){ memcpy(&m_Rect, &rect, sizeof(RECT));}
	inline void SetHDC( const HDC hDC ) { m_hDC = hDC; };
//	inline void SetSnapShot(CString strImage) { m_strImagePath = strImage; }

	RECT m_Rect;
//	CString		m_strImagePath;

	int GetEncoderClsid(const WCHAR *format, CLSID *pClsid);
	
	long InitH264Decoder();
	void ReleaseH264Decoder();
	
	long InitMpeg4Decoder();
	void ReleaseMpeg4Decoder();
	
	long InitMjpegDecoder();
	void ReleaseMjpegDecoder();
	
	//long InitPentaDecoder();
	//void ReleasePentaDecoder();
	
	//void DBG(const char *fmt, ...);

	BOOL DrawImage( LPBYTE pImageBuffer, int nImageWidth, int nImageHeight );

	HDC m_hDC;				// DC for drawing
private:
	HWND m_hWnd;			// hWnd of the Screen
	ULONG_PTR m_pGDIToken;	// GDI Plus Token

	// Main Concept Decoder
	bufstream_tt* m_pH264Bufstream;
	frame_tt m_stH264Output_frame;			// Settings for output frame structure
	BYTE* m_pH264Buf;
	bufstream_tt* m_pMpeg4Bufstream;
	frame_tt m_stMpeg4Output_frame;			// Settings for output frame structure
	BYTE* m_pMpeg4Buf;

	// GDI+ MJpeg Decoding Buffer
	HGLOBAL m_hBuffer;

	// Penta Decoder
	BYTE* m_pPentaBuf;
	DEC_FRAME m_dec_frame;
	DEC_PARAM m_dec_param;
	DEC_MEM_REQS m_dec_mem_reqs;

public:
	bool	m_bH264First;
	bool	m_bMpeg4First;
	bool	m_bPentaFirst;

};
