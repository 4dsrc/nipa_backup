/* ----------------------------------------------------------------------------
 * File: dec_mp4v.h
 *
 * Desc: MPEG-4 Video Decoder API
 *
 * Copyright (c) 2008, MainConcept GmbH. All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * MainConcept GmbH and may be used only in accordance with the terms of
 * your license from MainConcept GmbH.
 * ----------------------------------------------------------------------------
 */

#if !defined (__DEC_MP4V_API_INCLUDED__)
#define __DEC_MP4V_API_INCLUDED__

#include "mcdefs.h"
#include "mcfourcc.h"
#include "bufstrm.h"
#include "mcapiext.h"

#ifdef __cplusplus
extern "C" {
#endif

bufstream_tt * MC_EXPORT_API open_mpeg4in_Video_stream_ex(void *(*get_rc)(char* name), long reserved1, long reserved2);
bufstream_tt * MC_EXPORT_API open_mpeg4in_Video_stream(void);

APIEXTFUNC     MC_EXPORT_API mpeg4in_Video_GetAPIExt(uint32_t func);


#define M4D_UNSUPPORTED  -1

#define M4DS_OK                                           0x00000000
#define M4DS_FALSE                                        0x00000001
#define M4DE_FAILED                                       0x80004005
#define M4DE_NOT_SUPPORTED                                0x80004006
#define M4DE_PARAM_UNKNOWN                                0x80004007
#define M4DE_PARSE                                        0x80004008
#define M4DE_INVALID_ARGUMENT                             0x80070057
#define M4DE_POINTER                                      0x80004003
#define M4DE_OUTOFMEMORY                                  0x80004009
#define M4D_NULL                                          0x00000000


#define DEFAULT_BRIGHTNESS  128
#define DEFAULT_CONTRAST    128


#define M4D_SET_RATE                                      0x80001000
#define M4D_GET_FRAME                                     0x80001001
#define M4D_SET_FRAME_START_TIME                          0x80001002
#define M4D_GET_FRAME_START_TIME                          0x80001003
#define M4D_GET_FRAME_TIME                                0x80001004
#define M4D_GET_DECODER_STATISTIC                         0x80001005
#define M4D_GET_FRAME_INFO                                0x80001006




#define M4D_DEINTERLACE_WEAVE                 0x00000000
#define M4D_DEINTERLACE_VERTICAL_FILTER       0x00000001
#define M4D_DEINTERLACE_FIELD_INTERPOLATION   0x00000002
#define M4D_DEINTERLACE_AUTO                  0x00000003

#define M4D_ERROR_RESILIENCE_SKIP_TILL_INTRA  0x00000000
#define M4D_ERROR_RESILIENCE_PROCEED_ANYWAY   0x00000001

#define M4D_SKIP_MODE_NONE                    0x00000000  
#define M4D_SKIP_MODE_DECODE_REFERENCE        0x00000001  
#define M4D_SKIP_MODE_DECODE_INTRA            0x00000002  
#define M4D_SKIP_MODE_OBEY_QUALITY_MESSAGES   0x00000003

#define M4D_IDCT_PRECISION_INTEGER            0x00000000    
#define M4D_IDCT_PRECISION_FLOAT              0x00000001    

typedef struct m4d_decoder_settings
{
    int32_t   post_process_flag;      
    int32_t   deblock_strength;
    int32_t   reserved_1[10];

    int32_t   display_osd_flag;            
    int32_t   reserved_2[10];
    
    int32_t   skip_mode;
    int32_t   error_resilience_mode;
    int32_t   deinterlace_mode;
    int32_t   idct_precision_mode; 
    int32_t   stream_order;
    int32_t   reserved_3[29];

    int32_t   brightness;       
    int32_t   contrast;
    int32_t   reserved_4[10];

    int32_t   reserved_5[100];

} m4d_decoder_settings_tt;

typedef struct m4d_vop_time
{
    int64_t start;
    int64_t end;
    bool    valid;

} m4d_vop_time_tt;


typedef struct m4d_frame_info
{
    m4d_vop_time_tt vop_time;    
} m4d_frame_info_tt;


typedef struct m4d_statistic
{
    int32_t   number_frames_played;
    int32_t   number_frames_skipped;
}m4d_statistic_tt;

typedef struct m4d_rate
{
    int64_t start_time;
    double  rate;

}m4d_rate_tt;

typedef struct m4d_frame
{
    uint8_t   *pbuffer;
    uint32_t   fourcc;
}m4d_frame_tt;

#ifdef __cplusplus
}
#endif



#endif // #if !defined (__DEC_MP4V_API_INCLUDED__)

