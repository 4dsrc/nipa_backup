/* ----------------------------------------------------------------------------
 * File: dec_mjpg.h
 *
 * Desc: MJPEG Decoder API
 *
 * Copyright (c) 2009, MainConcept GmbH. All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * MainConcept GmbH and may be used only in accordance with the terms of
 * your license from MainConcept GmbH.
 * ----------------------------------------------------------------------------
 */

#if !defined (__DEC_MJPG_H__)
#define __DEC_MJPG_H__

#include "mctypes.h"
#include "mcfourcc.h"
#include "mcapiext.h"

#if defined(WIN32)
  #define MC_EXPORT_API __cdecl
#else
  #define MC_EXPORT_API
#endif

#ifndef MAKEFOURCC
    #define MAKEFOURCC(ch0, ch1, ch2, ch3)                                \
                ((uint32_t)(uint8_t)(ch0) | ((uint32_t)(uint8_t)(ch1) << 8) |         \
                ((uint32_t)(uint8_t)(ch2) << 16) | ((uint32_t)(uint8_t)(ch3) << 24 ))
#endif // MAKEFOURCC

////////// available fourcc's
/*
#define FOURCC_BGR3 0x33524742
#define FOURCC_BGRA 0x41524742
#define FOURCC_BGR4 0x34524742
#define FOURCC_UYVY 0x59565955
#define FOURCC_R555 0x35353552
#define FOURCC_R565 0x35363552
#define FOURCC_YUY2 0x32595559
#define FOURCC_YUYV 0x56595559
#define FOURCC_VYUY 0x59555956
*/

#ifndef FOURCC_VYUY
#define FOURCC_VYUY MAKEFOURCC('V', 'Y', 'U', 'Y')
#endif
#ifndef FOURCC_YuYv
#define FOURCC_YuYv MAKEFOURCC('Y', 'u', 'Y', 'v') //No clamping to ITU601 range, (decode only for now)
#endif
#ifndef FOURCC_UyVy
#define FOURCC_UyVy MAKEFOURCC('U', 'y', 'V', 'y') //No clamping to ITU601 range  (decode only for now)
#endif
#ifndef FOURCC_bgra
#define FOURCC_bgra MAKEFOURCC('b', 'g', 'r', 'a') //No clamping to ITU601 range  (decode only for now)
#endif
#ifndef FOURCC_bgr4
#define FOURCC_bgr4 MAKEFOURCC('b', 'g', 'r', '4') //No clamping to ITU601 range  (decode only for now)
#endif
#ifndef FOURCC_bgr3
#define FOURCC_bgr3 MAKEFOURCC('b', 'g', 'r', '3') //No clamping to ITU601 range  (decode only for now)
#endif
//////////


////////// decompression flags
#define DCOMP_ONE_FIELD_SMART  0x00000100
#define DCOMP_ONE_FIELD_ONLY   0x00000200
#define DCOMP_INV_FIELD_ORDER  0x00000400
#define DCOMP_USE_AVI1         0x00002000
#define DCOMP_IGNORE_ITU601    0x00004000   //No clamping to ITU601 range, (decode only for now)



#ifdef __cplusplus
extern "C" {
#endif

uint32_t MC_EXPORT_API DecompressBuffer_MJPG(unsigned char  *pb_src,
                                             uint32_t        src_buf_len, 
                                             unsigned char  *pb_dst,
                                             int32_t         stride,
                                             uint32_t        width,
                                             uint32_t        height,
                                             uint32_t        flags,
                                             uint32_t        fourcc,
                                             uint32_t        opt_flags,
                                             void           *ext_info);


APIEXTFUNC MC_EXPORT_API MJPG_Decompress_GetAPIExt (uint32_t func);


#ifdef __cplusplus
}
#endif

#endif // __DEC_MJPG_H__
