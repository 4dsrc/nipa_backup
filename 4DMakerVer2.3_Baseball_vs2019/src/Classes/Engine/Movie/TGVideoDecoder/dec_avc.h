/* ----------------------------------------------------------------------------
 * File: dec_avc.h
 *
 * Desc: AVC/H.264 Decoder API
 *
 * Copyright (c) 2009, MainConcept GmbH. All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * MainConcept GmbH and may be used only in accordance with the terms of
 * your license from MainConcept GmbH.
 * ----------------------------------------------------------------------------
 */
 
#if !defined (__DEC_AVC_API_INCLUDED__)
#define __DEC_AVC_API_INCLUDED__


#include "bufstrm.h"
#include "mcdefs.h"
#include "mcapiext.h"


#define SET_ERROR_RESILIENCE_MODE          0x00010081
enum h264vd_error_resilience_mode {
	H264VD_ER_PROCESS_ANYWAY = 0,
	H264VD_ER_WAIT_I_SLICE,
	H264VD_ER_WAIT_IDR_SLICE
};
#define AVC_DECODER_AUTH                   0x00010082

#define SET_LOOP_FILTER_MODE        0x00010085
enum h264vd_loop_filter_mode {
	H264VD_LF_OFF = 0,
	H264VD_LF_STANDARD = 1,
	H264VD_LF_REFERENCE_ONLY = 2
};

#define GET_RECOVERY_POINT          0x00010087
#define SET_RECOVERY_POINT          0x00010088

typedef struct user_data_packet_s{
	uint8_t *data;
	int32_t payload_size;
}user_data_packet_t;

typedef struct user_data_s{
	user_data_packet_t *user_data_packet;
	int32_t num_user_data_packets;
}user_data_t;

typedef struct sei_payload_s{
	uint8_t *payload;
	int32_t payload_type;
	int32_t payload_size;
}sei_payload_t;

typedef struct sei_message_s{
	sei_payload_t *sei_payload;
	int32_t num_messages;
}sei_message_t;

#define SET_DPB_SIZE				0x00010089
#define	SET_MV_PRECISION			0x0001008a
enum h264vd_mv_precision {
	H264VD_MV_PRECISION_QUARTER = 0,
	H264VD_MV_PRECISION_HALF = 1,
	H264VD_MV_PRECISION_FULL = 2
};

#define GET_DPB_SIZE				0x0001008b
#define SET_NUM_UNUSED_FRAMES		0x0001008c
#define	SET_8BIT_OUTPUT				0x0001008d
#define	SET_DECODE_BASE_LAYERS		0x0001008e
#define	SET_MAX_LAYER				0x0001008f

#define GET_H264SEQ_PAR_SET			0x00010091
#define GET_H264PIC_PAR_SET			0x00010092

//#define ANALYSE_OPTIONS			0x00010093
//#define ANALYSE_MB				0x00000001
//#define ANALYSE_INTRA				0x00000002
//#define ANALYSE_INTER				0x00000004
//#define ANALYSE_SCOEFF			0x00000008
//#define ANALYSE_RESDIFF			0x00000010
//#define ANALYSE_LEVEL				0x00000020
//#define ANALYSE_BITSTREAM			0x00000040		// coded bitstream extraction
//#define ANALYSE_GET_PIC			0x00010094

typedef struct h264_nalu_s {
	int32_t	forbidden_zero_bit;
	int32_t	nal_ref_idc;
	int32_t	nal_unit_type;
	int32_t	reserved_one_bit;
	int32_t	idr_flag;
	int32_t	priority_id;
	int32_t	no_inter_layer_pred_flag;
	int32_t	dependency_id;
	int32_t	quality_id;
	int32_t	temporal_id;
	int32_t	use_ref_base_pic_flag;
	int32_t	discardable_flag;
	int32_t	output_flag;
	int32_t	reserved_three_2bits;
	int32_t store_ref_base_pic_flag;
	int32_t prefix_nal_unit_additional_extension_flag;
}h264_nalu_t;

typedef	struct h264_hrd_s {
	int32_t	cpb_cnt_minus1;
	int32_t	bit_rate_scale;
	int32_t	cpb_size_scale;
	uint32_t bit_rate_value_minus1[32];
	uint32_t cpb_size_value_minus1[32];
	int32_t	cbr_flag[32];
	int32_t	initial_cpb_removal_delay_length_minus1;
	int32_t	cpb_removal_delay_length_minus1;
	int32_t	dpb_output_delay_length_minus1;
	int32_t	time_offset_length;
}h264_hrd_t;

typedef	struct h264_vui_s {
	int32_t	aspect_ratio_info_present_flag;
	int32_t	aspect_ratio_idc;
	int32_t	sar_width;
	int32_t	sar_height;
	int32_t	overscan_info_present_flag;
	int32_t	overscan_appropriate_flag;
	int32_t	video_signal_type_present_flag;
	int32_t	video_format;
	int32_t	video_full_range_flag;
	int32_t	colour_description_present_flag;
	int32_t	colour_primaries;
	int32_t	transfer_characteristics;
	int32_t	matrix_coefficients;
	int32_t	chroma_loc_info_present_flag;
	int32_t	chroma_sample_loc_type_top_field;
	int32_t	chroma_sample_loc_type_bottom_field;
	int32_t	timing_info_present_flag;
	uint32_t num_units_in_tick;
	uint32_t time_scale;
	int32_t	fixed_frame_rate_flag;
	int32_t	nal_hrd_parameters_present_flag;
	h264_hrd_t nal_hrd_parameters;
	int32_t vcl_hrd_parameters_present_flag;
	h264_hrd_t vcl_hrd_parameters;
	int32_t	low_delay_hrd_flag;
	int32_t	pic_struct_present_flag;
	int32_t	bitstream_restriction_flag;
	int32_t	motion_vectors_over_pic_boundaries_flag;
	int32_t	max_bytes_per_pic_denom;
	int32_t	max_bits_per_mb_denom;
	int32_t	log2_max_mv_length_horizontal;
	int32_t	log2_max_mv_length_vertical;
	int32_t	num_reorder_frames;
	int32_t	max_dec_frame_buffering;
}h264_vui_t;

typedef struct h264_seq_par_set_s {
	h264_nalu_t nalu;
	int32_t profile_idc;
	int32_t	constraint_set0_flag;
	int32_t	constraint_set1_flag;
	int32_t	constraint_set2_flag;
	int32_t	constraint_set3_flag;
	int32_t	level_idc;
	int32_t reserved_zero_4bits;
	int32_t	seq_parameter_set_id;
	int32_t	chroma_format_idc;
	int32_t	separate_colour_plane_flag;
	int32_t	bit_depth_luma_minus8;
	int32_t	bit_depth_chroma_minus8;
	int32_t	qpprime_y_zero_transform_bypass_flag;
	int32_t	seq_scaling_matrix_present_flag;
	int32_t	seq_scaling_list_present_flag[12];
	int32_t	scaling_list_4x4[6][16];
	int32_t	scaling_list_8x8[6][64];
	int32_t	log2_max_frame_num_minus4;
	int32_t	pic_order_cnt_type;
	int32_t	log2_max_pic_order_cnt_lsb_minus4;
	int32_t	delta_pic_order_always_zero_flag;
	int32_t	offset_for_non_ref_pic;
	int32_t	offset_for_top_to_bottom_field;
	int32_t	num_ref_frames_in_pic_order_cnt_cycle;
	int32_t	offset_for_ref_frame[8];
	int32_t	num_ref_frames;
	int32_t	gaps_in_frame_num_value_allowed_flag;
	int32_t	pic_width_in_mbs_minus1;
	int32_t	pic_height_in_map_units_minus1;
	int32_t	frame_mbs_only_flag;
	int32_t	mb_adaptive_frame_field_flag;
	int32_t	direct_8x8_inference_flag;
	int32_t	frame_cropping_flag;
	int32_t	frame_crop_left_offset;
	int32_t	frame_crop_right_offset;
	int32_t	frame_crop_top_offset;
	int32_t	frame_crop_bottom_offset;
	int32_t	vui_parameters_present_flag;
	h264_vui_t vui_parameters;
	int32_t interlayer_deblocking_filter_control_present_flag;
	int32_t extended_spatial_scalability;
	int32_t chroma_phase_x_plus1_flag;
	int32_t chroma_phase_y_plus1;
	int32_t seq_ref_layer_chroma_phase_x_plus1_flag;
	int32_t seq_ref_layer_chroma_phase_y_plus1;
	int32_t seq_scaled_ref_layer_left_offset;
	int32_t seq_scaled_ref_layer_top_offset;
	int32_t seq_scaled_ref_layer_right_offset;
	int32_t seq_scaled_ref_layer_bottom_offset;
	int32_t seq_tcoeff_level_prediction_flag;
	int32_t adaptive_tcoeff_level_prediction_flag;
	int32_t slice_header_restriction_flag;
	int32_t svc_vui_parameters_present_flag;
	int32_t additional_extension2_flag;
	int32_t additional_extension2_data_flag;
}h264_seq_par_set_t;

typedef struct h264_pic_par_set_s {
	h264_nalu_t nalu;
	int32_t	pic_parameter_set_id;
	int32_t	seq_parameter_set_id;
	int32_t	entropy_coding_mode_flag;
	int32_t	pic_order_present_flag;
	int32_t	num_slice_groups_minus1;
	int32_t	slice_group_map_type;
	int32_t	run_length_minus1[8];
	int32_t	top_left[8];
	int32_t	bottom_right[8];
	int32_t	slice_group_change_direction_flag;
	int32_t	slice_group_change_rate_minus1;
	int32_t	pic_size_in_map_units_minus1;
	uint8_t *slice_group_id;
	int32_t	num_ref_idx_l0_active_minus1;
	int32_t	num_ref_idx_l1_active_minus1;
	int32_t	weighted_pred_flag;
	int32_t	weighted_bipred_idc;
	int32_t	pic_init_qp_minus26;
	int32_t	pic_init_qs_minus26;
	int32_t	chroma_qp_index_offset;
	int32_t	second_chroma_qp_index_offset;
	int32_t	deblocking_filter_control_present_flag;
	int32_t	constrained_intra_pred_flag;
	int32_t	redundant_pic_cnt_present_flag;
	int32_t	transform_8x8_mode_flag;
	int32_t	pic_scaling_matrix_present_flag;
	int32_t	pic_scaling_list_present_flag[12];
	int32_t	scaling_list_4x4[6][16];
	int32_t	scaling_list_8x8[6][64];
}h264_pic_par_set_t;

typedef struct h264_sei_pic_timing_s {
	uint32_t cpb_removal_delay;
	uint32_t dpb_output_delay;
	int32_t pic_struct;
	int32_t clock_timestamp_flag[3];
	int32_t ct_type;
	int32_t nuit_field_based_flag;
	int32_t counting_type;
	int32_t full_timestamp_flag;
	int32_t discontinuity_flag;
	int32_t cnt_dropped_flag;
	int32_t n_frames;
	int32_t seconds_flag;
	int32_t seconds_value;
	int32_t minutes_flag;
	int32_t minutes_value;
	int32_t hours_flag;
	int32_t hours_value;
	int32_t time_offset;
}h264_sei_pic_timing_t;

typedef struct h264_sei_buffering_period_s {
	int32_t seq_paramater_set_id;
	uint32_t nal_initial_cpb_removal_delay[32];
	uint32_t nal_initial_cpb_removal_delay_offset[32];
	uint32_t vcl_initial_cpb_removal_delay[32];
	uint32_t vcl_initial_cpb_removal_delay_offset[32];
}h264_sei_buffering_period_t;

typedef struct h264_sei_s {
	void *p;
	uint32_t payload_type;
}h264_sei_t;

typedef	struct h264_ref_pic_list_reordering_s {
	int32_t ref_pic_list_reordering_flag[2];
	int32_t reordering_of_pic_nums_idc[2][32];
	union {
		int32_t abs_diff_pic_num_minus1[2][32];
		int32_t long_term_pic_num[2][32];
	};
}h264_ref_pic_list_reordering_t;

typedef struct h264_dec_ref_pic_marking_s {
	int32_t memory_management_control_operation;
	int32_t difference_of_pic_nums_minus1;
	union {
		int32_t long_term_pic_num;
		int32_t long_term_frame_idx;
		int32_t max_long_term_frame_idx_plus1;
	};
}h264_dec_ref_pic_marking_t;

typedef struct h264_pred_weight_s {
	int32_t luma_log2_weight_denom;
	int32_t chroma_log2_weight_denom;
	int8_t luma_weight_flag[2][32];
	int8_t chroma_weight_flag[2][32];
	int16_t luma_weight[2][32];
	int16_t luma_offset[2][32];
	int16_t chroma_weight[2][32][2];
	int16_t chroma_offset[2][32][2];
}h264_pred_weight_t;

typedef struct h264_slice_header_s {
	h264_nalu_t nalu;
	int32_t	first_mb_in_slice;
	int32_t	slice_type;
	int32_t	pic_parameter_set_id;
	int32_t	colour_plane_id;
	int32_t	frame_num;
	int32_t	field_pic_flag;
	int32_t bottom_field_flag;
	int32_t idr_pic_id;
	int32_t	pic_order_cnt_lsb;
	int32_t	delta_pic_order_cnt_bottom;
	int32_t	delta_pic_order_cnt[2];
	int32_t	redundant_pic_cnt;
	int32_t	direct_spatial_mv_pred_flag;
	int32_t	num_ref_idx_active_override_flag;
	int32_t	num_ref_idx_l0_active_minus1;
	int32_t	num_ref_idx_l1_active_minus1;
	int32_t	cabac_init_idc;
	int32_t	slice_qp_delta;
	int32_t	sp_for_switch_flag;
	int32_t	slice_qs_delta;
	int32_t	disable_deblocking_filter_idc;
	int32_t	slice_alpha_c0_offset_div2;
	int32_t	slice_beta_offset_div2;
	int32_t	slice_group_change_cycle;
	int32_t	no_output_of_prior_pics_flag;
	int32_t	long_term_reference_flag;
	int32_t	adaptive_ref_pic_buffering_flag;
	int32_t no_inter_layer_pred_flag;
	int32_t quality_id;
	int32_t base_mmco_count;
	int32_t base_pred_weight_table_flag;
	int32_t base_adaptive_ref_pic_marking_mode_flag;
	int32_t ref_layer_dq_id;
	int32_t disable_interlayer_deblocking_filter_idc;
	int32_t interlayer_slice_alpha_c0_offset_div2;
	int32_t interlayer_slice_beta_offset_div2;
	int32_t constrained_intra_resampling_flag;
	int32_t ref_layer_chroma_phase_x_plus1_flag;
	int32_t ref_layer_chroma_phase_y_plus1;
	int32_t scaled_ref_layer_left_offset;
	int32_t scaled_ref_layer_top_offset;
	int32_t scaled_ref_layer_right_offset;
	int32_t scaled_ref_layer_bottom_offset;
	int32_t slice_skip_flag;
	int32_t num_mbs_in_slice_minus1;
	int32_t adaptive_prediction_flag;
	int32_t default_base_mode_flag;
	int32_t adaptive_motion_prediction_flag;
	int32_t default_motion_prediction_flag;
	int32_t adaptive_residual_prediction_flag;
	int32_t default_residual_prediction_flag;
	int32_t tcoeff_level_prediction_flag;
	int32_t scan_idx_start;
	int32_t scan_idx_end;
	h264_ref_pic_list_reordering_t ref_pic_list_reordering;
	h264_pred_weight_t pred_weight_table;
	h264_dec_ref_pic_marking_t dec_ref_pic_marking[67];
	h264_dec_ref_pic_marking_t base_dec_ref_pic_marking[67];
}h264_slice_header_t;

typedef struct h264_macroblock_s {
	int8_t mb_skip_flag;
	int8_t mb_field_decoding_flag;
	int8_t mb_type;
	int8_t transform_size_8x8_flag;
	int8_t intra_chroma_pred_mode;
	int8_t sub_mb_type[4];
	int8_t coded_block_pattern;
	uint32_t coded_block_pattern_ext[2];
	int8_t mb_qp_delta;

	//non-coded parameters
	int32_t slice_nr;
	int8_t	qp[3];
	uint16_t bits_mb;
	uint16_t bits_residual;
	uint16_t bits_residual_chroma;

	int32_t reserved[3];
}h264_macroblock_t;

typedef struct h264_mv_s {
	int16_t xy[16][2];
}h264_mv_t;

typedef struct h264_ref_pic_s {
	uint32_t id;
	int32_t bottom_field_flag;
	int32_t short_term_reference_flags;
	int32_t long_term_reference_flags;
	int32_t long_term_frame_idx;
	int32_t non_existing_flag;
}h264_ref_pic_t;

typedef struct h264_dpb_s {
	h264_ref_pic_t ref_pic[16];
}h264_dpb_t;

typedef struct h264_slice_s {
	h264_slice_header_t *slice_header;
	
	int32_t ref_count[2];
	h264_ref_pic_t ref_pic[2][32];
	
	uint8_t *slice_data;
	uint32_t bytes_in_slice;

	uint8_t	reserved[512];
}h264_slice_t;

typedef struct h264_picture_s {
	h264_pic_par_set_t	*pic_par_set;
	h264_sei_t			*sei;
	h264_slice_t		*slice;

	uint32_t	id;
	int32_t		sei_count;
	int32_t		slice_count;
	int32_t		poc;

	h264_macroblock_t	*mb;
	
	int8_t		*ipred;
	h264_mv_t	*mvd[2];
	h264_mv_t	*mv[2];
	int8_t		*ref_idx[2];
	void		*level[3];
	void		*coeff[3];
	void		*residual[3];
	
	void	*samples_predicted[3];
	int32_t	stride_samples_predicted[3];

	void	*samples_restored[3];
	int32_t	stride_samples_restored[3];

	void	*samples[3];
	int32_t	stride_samples[3];

	uint8_t	qmatrix_4x4[6][16];
	uint8_t	qmatrix_8x8[6][64];

	uint8_t	reserved[512];
}h264_picture_t;

typedef struct h264_frame_s {
	h264_seq_par_set_t	*seq_par_set;
	h264_picture_t		*pic[2];

	uint32_t id;
	int32_t	interlaced_frame_flag;
	int32_t skip_flag;
	int32_t	frame_num;
	int32_t	poc[2];

	int32_t	short_term_reference_flags;
	int32_t	long_term_reference_flags;

	uint8_t	reserved[512];
}h264_frame_t;
/*
sample usage of streaming H264-decoder

bufstream_tt *bs;
struct SEQ_Params *pSEQ;
struct PIC_Params *pPIC;
frame_tt output_frame;
unsigned long state;
int n;

// Create the h.264 parser.
bs = open_h264in_Video_stream();
// Init the parser to start using it.
bs->auxinfo(bs,0,PARSE_INIT,NULL,0);
// Set the parser to parsing frames mode.
bs->auxinfo(bs,0,PARSE_FRAMES,NULL,0);

while(chunk_size > 0) {
	// Put chunk of coded stream to the parser.
	n = bs->copybytes(bs,chunk_ptr,chunk_size);
	chunk_ptr += n;
	chunk_size -= n;
	
	// Get and clean parser's state.
	state = bs->auxinfo(bs,0,CLEAN_PARSE_STATE,NULL,0);

	// Check parser's state.
	if( state & (PIC_VALID_FLAG|PIC_FULL_FLAG) ) {
		// Get the headers to which this frame refers.
		// SEQ_Params contains information about frame resolution.
		// PIC_Params contains information about frame type and its POC.
		dec->auxinfo(dec,0,GET_SEQ_PARAMSP,&pSEQ,sizeof(struct SEQ_Params));
		dec->auxinfo(dec,0,GET_PIC_PARAMSP,&pPIC,sizeof(struct PIC_Params));

		// Set color format in which you would like to get the frame
		// and pointers to samples' buffer in frame_tt structure.
		output_frame.four_cc = MAKEFOURCC('Y','V','1','2');
		output_frame.plane[0] = ptr_Y;
		output_frame.plane[1] = ptr_V;
		output_frame.plane[2] = ptr_U;
		output_frame.stride[0] = stride_Y;
		output_frame.stride[1] = stride_V;
		output_frame.stride[2] = stride_U;
		output_frame.width = pSEQ->horizontal_size;
		output_frame.height = pSEQ->vertical_size;

		if( BS_OK==dec->auxinfo(dec,0,GET_PIC,&output_frame,sizeof(frame_tt)) ) {
			//..
		}
	}
}

// Flush the decoder to get all pended frames.
do {
	n = bs->copybytes(bs,NULL,0);
	state = bs->auxinfo(bs,0,CLEAN_PARSE_STATE,NULL,0);
	if( state & (PIC_VALID_FLAG|PIC_FULL_FLAG) ) {
		//...
	}
}while(n);

// Destroy instance.
bs->free(bs);

*/


#ifdef __cplusplus
extern "C" {
#endif

	bufstream_tt *  MC_EXPORT_API open_h264in_Video_stream(void); //quick and dirty default initialization
	bufstream_tt *  MC_EXPORT_API open_h264in_Video_stream_ex(void *(*get_rc)(char* name), long reserved1, long reserved2);

	APIEXTFUNC      MC_EXPORT_API h264in_Video_GetAPIExt(uint32_t func);

#ifdef __cplusplus
}
#endif

#endif  // #if !defined (__ENC_AVC_API_INCLUDED__)
