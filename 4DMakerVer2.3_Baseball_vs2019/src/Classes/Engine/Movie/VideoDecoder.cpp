// Decoder.cpp: implementation of the CVideoDecoder class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VideoDecoder.h"
#include <atlimage.h>


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

int	get_pic_size(int pic_x,int pic_y, int fourcc)
{
	if( fourcc == FOURCC_BGRA || fourcc == FOURCC_BGR4 )
		return 4*pic_x*pic_y;
	else if( fourcc == FOURCC_R565	|| fourcc == FOURCC_R555 || 
		fourcc == FOURCC_YUY2	|| fourcc == FOURCC_UYVY || fourcc == FOURCC_YUYV)
		return 2*pic_x*pic_y;
	else if( fourcc == FOURCC_BGR3 )
		return 3*pic_x*pic_y;
	else if( fourcc == FOURCC_YV12 || fourcc == FOURCC_I420)
		return (3*pic_x*pic_y)/2;
	
	return 0;
}

int	get_pic_stride(int pic_x,int fourcc) 
{
	if( fourcc == FOURCC_BGRA || fourcc == FOURCC_BGR4 )
		return 4*pic_x;
	else if(fourcc == FOURCC_R565	|| fourcc == FOURCC_R555 || 
		fourcc == FOURCC_YUY2	|| fourcc == FOURCC_UYVY || fourcc == FOURCC_YUYV)
		return 2*pic_x;
	else if( fourcc == FOURCC_BGR3 )
		return 3*pic_x;
	else if( fourcc == FOURCC_YV12 || fourcc == FOURCC_I420 )
		return pic_x;
	
	return 0;
}

// Decoder DLL
typedef bufstream_tt* (__cdecl *OPEN_H264_IN_VIDEO_STREAM)(void);
typedef bufstream_tt* (__cdecl *OPEN_MPEG4_IN_VIDEO_STREAM)(void);

OPEN_MPEG4_IN_VIDEO_STREAM	fpOpen_Mpeg4in_Video_Stream = NULL;
OPEN_H264_IN_VIDEO_STREAM	fpOpen_H264in_Video_Stream  = NULL;

HANDLE	g_hMpeg4Dll = NULL;
HANDLE	g_hH264Dll  = NULL;

CVideoDecoder::CVideoDecoder()
{
	m_pH264Bufstream = NULL;
	ZeroMemory( &m_stH264Output_frame, sizeof(frame_tt) );
	m_pMpeg4Bufstream = NULL;
	ZeroMemory( &m_stMpeg4Output_frame, sizeof(frame_tt) );

	m_pH264Buf = NULL;
	m_pMpeg4Buf = NULL;
	m_pPentaBuf = NULL;

	ZeroMemory( &m_dec_frame, sizeof(DEC_FRAME) );
	ZeroMemory( &m_dec_param, sizeof(DEC_PARAM) );
	ZeroMemory( &m_dec_mem_reqs, sizeof(DEC_MEM_REQS) );

	GdiplusStartupInput gpsi;
	if ( GdiplusStartup(&m_pGDIToken, &gpsi, NULL) != Ok)
	{
		//OutputDebugString("Error");
		TRACE(_T("Error"));
	}

//	m_strImagePath = _T("");
	m_bH264First	= true;
	m_bMpeg4First	= true;
	m_bPentaFirst	= true;

	g_hMpeg4Dll = NULL;
	g_hH264Dll = NULL;

	//-- 2012-07-17 hongsu@esmlab.com
	//-- CIC Check
	m_hBuffer	= NULL;
	m_hDC		= NULL;
	m_hWnd		= NULL;

	memset(&m_Rect, 0, sizeof(m_Rect));
}

CVideoDecoder::~CVideoDecoder()
{
	ReleaseDecoder();

	if(m_pH264Buf) delete[] m_pH264Buf;
	if(m_pMpeg4Buf) delete[] m_pMpeg4Buf;
	if(m_pPentaBuf) delete[] m_pPentaBuf;

	GdiplusShutdown(m_pGDIToken);

	if(g_hMpeg4Dll) FreeLibrary((HMODULE)g_hMpeg4Dll);	
	g_hMpeg4Dll = NULL;
	
	if(g_hH264Dll) FreeLibrary((HMODULE)g_hH264Dll);	
	g_hH264Dll = NULL;
}

void CVideoDecoder::InitDecoder()
{
	InitH264Decoder();
	InitMpeg4Decoder();
	InitMjpegDecoder();
	//InitPentaDecoder();
}

void CVideoDecoder::ReleaseDecoder()
{
	ReleaseH264Decoder();
	ReleaseMpeg4Decoder();
	ReleaseMjpegDecoder();
	//ReleasePentaDecoder();
}


long CVideoDecoder::InitH264Decoder()
{
	char	szPath[MAX_PATH] = "";
	char	szFilePath[MAX_PATH] = "";
	
	sprintf(szFilePath, "%s%s", szPath, ".\\mc_dec_avc.dll");
	g_hH264Dll = LoadLibraryA(szFilePath);
	fpOpen_H264in_Video_Stream = (OPEN_H264_IN_VIDEO_STREAM)GetProcAddress((HMODULE)g_hH264Dll, "open_h264in_Video_stream");
	
	// Reset Old Stream
	if(m_pH264Bufstream) 
		ReleaseH264Decoder();
	
	m_pH264Bufstream = fpOpen_H264in_Video_Stream();
	// Last Error would be Set in Media.cpp
	if(m_pH264Bufstream == NULL)
		return -1;

	// Create MainConcept Decoder
	if(!fpOpen_H264in_Video_Stream)
		return -1;

	// Init Decoder (or call when data stream is broken like after seeking)
	m_pH264Bufstream->auxinfo( m_pH264Bufstream, 0, PARSE_INIT, NULL, 0 );

	//m_pH264Bufstream->auxinfo( m_pH264Bufstream, LOW_LATENCY_FLAG, PARSE_OPTIONS, NULL, 0);

	// Use Multi-Thread by Picture, using 4 Threads
	m_pH264Bufstream->auxinfo(m_pH264Bufstream, SMP_BY_PICTURES, SET_SMP_MODE, NULL, 0);
	m_pH264Bufstream->auxinfo(m_pH264Bufstream, 4, SET_CPU_NUM, NULL, 0);
	
	//m_pH264Bufstream->auxinfo(m_pH264Bufstream, 1, SET_8BIT_OUTPUT, NULL, 0);

	// Set low Motion Vector Precision
	m_pH264Bufstream->auxinfo(m_pH264Bufstream, H264VD_MV_PRECISION_QUARTER, SET_MV_PRECISION, 0, 0);

	// Prepare for Sequence Header
	m_pH264Bufstream->auxinfo(m_pH264Bufstream, 0, PARSE_SEQ_HDR, NULL, 0);
	// Get Sequence Header but Pass here (Don't Copy)
	m_pH264Bufstream->copybytes(m_pH264Bufstream, NULL, 0);

	return 0;
}

void CVideoDecoder::ReleaseH264Decoder()
{
	if(m_pH264Bufstream == NULL) 
		return;

	int iNumberOfBytesUsed=0;
	unsigned int uiCurDecState;
	
	do
	{
		m_pH264Bufstream->auxinfo(m_pH264Bufstream, 0, PARSE_FRAMES, &m_stH264Output_frame, sizeof(frame_tt));
		iNumberOfBytesUsed = m_pH264Bufstream->copybytes(m_pH264Bufstream, NULL, 0);
		
		uiCurDecState = m_pH264Bufstream->auxinfo(m_pH264Bufstream, 0, CLEAN_PARSE_STATE,NULL, 0);
		
		if(uiCurDecState & PARSE_DONE_FLAG)
		{
			if(m_stH264Output_frame.plane[0])
			{
				//DBG("\n[Close] Decoded frame");
				break;
			}
		}
	}
	while(iNumberOfBytesUsed>0);

	close_bufstream(m_pH264Bufstream, 0);
	m_pH264Bufstream = NULL;
}

long CVideoDecoder::InitMpeg4Decoder()
{
	char	szPath[MAX_PATH] = "";
	char	szFilePath[MAX_PATH] = "";
	
	sprintf(szFilePath, "%s%s", szPath, ".\\mc_dec_mp4v.dll");
	g_hMpeg4Dll = LoadLibraryA(szFilePath);
	fpOpen_Mpeg4in_Video_Stream = (OPEN_MPEG4_IN_VIDEO_STREAM)GetProcAddress((HMODULE)g_hMpeg4Dll, "open_mpeg4in_Video_stream");
	
	if(m_pMpeg4Bufstream) ReleaseMpeg4Decoder();
	
	//DBG("InitMpeg4Decoder() DLL Load - mc_dec_mp4v.dll ... OK \n");


	// Create MainConcept Decoder
	if(!fpOpen_Mpeg4in_Video_Stream) return -1;
	
	m_pMpeg4Bufstream = fpOpen_Mpeg4in_Video_Stream();
	
	// Last Error would be Set in Media.cpp
	if(m_pMpeg4Bufstream == NULL) return -1;

	// Init Decoder (or call when data stream is broken like after seeking)
	m_pMpeg4Bufstream->auxinfo(m_pMpeg4Bufstream, 0, PARSE_INIT, NULL, 0);

	// Settings
	m4d_decoder_settings dec_sett;
	memset(&dec_sett, 0, sizeof(m4d_decoder_settings));
	
	dec_sett.brightness				= DEFAULT_BRIGHTNESS;			// 0 ~ 255
	dec_sett.contrast				= DEFAULT_CONTRAST;				// 0 ~ 255
	dec_sett.deinterlace_mode		= M4D_DEINTERLACE_VERTICAL_FILTER;
	m_pMpeg4Bufstream->auxinfo(m_pMpeg4Bufstream, 0, PARSE_OPTIONS, &dec_sett, sizeof(m4d_decoder_settings));

	//DBG("InitMpeg4Decoder() m4d dec setting brightness:%d constrast:%d deinterlace mode:%d \n", dec_sett.brightness,dec_sett.contrast,dec_sett.deinterlace_mode);
	//OutputDebugString("\n");
	//OutputDebugString("InitMpeg4Decoder()");
	return 0;
}

void CVideoDecoder::ReleaseMpeg4Decoder()
{
	if(m_pMpeg4Bufstream == NULL) return;

	int iNumberOfBytesUsed = 0;
	unsigned int uiCurDecState;
	
	do
	{		
		m_pMpeg4Bufstream->auxinfo(m_pMpeg4Bufstream, 0, PARSE_FRAMES, &m_stMpeg4Output_frame, sizeof(frame_tt));
		iNumberOfBytesUsed = m_pMpeg4Bufstream->copybytes(m_pMpeg4Bufstream, NULL, 0);
		
		uiCurDecState = m_pMpeg4Bufstream->auxinfo(m_pMpeg4Bufstream, 0, CLEAN_PARSE_STATE,NULL, 0);
		
		if(uiCurDecState & PARSE_DONE_FLAG)
		{
			if(m_stMpeg4Output_frame.plane[0])
			{
				//DBG("\n[Close] Decoded frame");
				break;
			}
		}
	}
	while(iNumberOfBytesUsed>0);
	
	close_bufstream(m_pMpeg4Bufstream, 0);
}

long CVideoDecoder::InitMjpegDecoder()
{
	// Do Nothing
	return 0;
}

void CVideoDecoder::ReleaseMjpegDecoder()
{
	// Do Nothing
}

/*
long CVideoDecoder::InitPentaDecoder()
{
	ReleasePentaDecoder();
	
	m_dec_param.output_format = DEC_RGB24_INV;
	m_dec_param.time_incr = 0;
	
	decore( 1, DEC_OPT_MEMORY_REQS, &m_dec_param, &m_dec_mem_reqs);
	
	m_dec_param.buffers.mp4_edged_ref_buffers = malloc(m_dec_mem_reqs.mp4_edged_ref_buffers_size);
	m_dec_param.buffers.mp4_edged_for_buffers = malloc(m_dec_mem_reqs.mp4_edged_for_buffers_size);
	m_dec_param.buffers.mp4_edged_bak_buffers = malloc(m_dec_mem_reqs.mp4_edged_bak_buffers_size);
	m_dec_param.buffers.mp4_display_buffers = malloc(m_dec_mem_reqs.mp4_display_buffers_size);
	m_dec_param.buffers.mp4_state = malloc(m_dec_mem_reqs.mp4_state_size);
	m_dec_param.buffers.mp4_tables = malloc(m_dec_mem_reqs.mp4_tables_size);
	m_dec_param.buffers.mp4_stream = malloc(m_dec_mem_reqs.mp4_stream_size);
	
	memset(m_dec_param.buffers.mp4_edged_ref_buffers, 0, m_dec_mem_reqs.mp4_edged_ref_buffers_size);
	memset(m_dec_param.buffers.mp4_edged_for_buffers, 0, m_dec_mem_reqs.mp4_edged_for_buffers_size);
	memset(m_dec_param.buffers.mp4_edged_bak_buffers, 0, m_dec_mem_reqs.mp4_edged_bak_buffers_size);
	memset(m_dec_param.buffers.mp4_display_buffers, 0, m_dec_mem_reqs.mp4_display_buffers_size);
	memset(m_dec_param.buffers.mp4_state, 0, m_dec_mem_reqs.mp4_state_size);
	memset(m_dec_param.buffers.mp4_tables, 0, m_dec_mem_reqs.mp4_tables_size);
	memset(m_dec_param.buffers.mp4_stream, 0, m_dec_mem_reqs.mp4_stream_size);
	
	decore( 1, DEC_OPT_INIT, &m_dec_param, NULL);
	
	m_dec_frame.bmp = NULL;
	m_dec_frame.bitstream = NULL;
	m_dec_frame.render_flag = 0;
	m_dec_frame.stride = m_dec_param.x_dim;

	return 0;
}

void CVideoDecoder::ReleasePentaDecoder()
{
	decore( 1, DEC_OPT_RELEASE, NULL, NULL);
	
	if (m_dec_param.buffers.mp4_edged_ref_buffers) {
		free(m_dec_param.buffers.mp4_edged_ref_buffers);
		m_dec_param.buffers.mp4_edged_ref_buffers = NULL;
	}
	if (m_dec_param.buffers.mp4_edged_for_buffers) {
		free(m_dec_param.buffers.mp4_edged_for_buffers);
		m_dec_param.buffers.mp4_edged_for_buffers = NULL;
	}
	if (m_dec_param.buffers.mp4_edged_bak_buffers) {
		free(m_dec_param.buffers.mp4_edged_bak_buffers);
		m_dec_param.buffers.mp4_edged_bak_buffers = NULL;
	}
	if (m_dec_param.buffers.mp4_display_buffers) {
		free(m_dec_param.buffers.mp4_display_buffers);
		m_dec_param.buffers.mp4_display_buffers = NULL;
	}
	if (m_dec_param.buffers.mp4_state) {
		free(m_dec_param.buffers.mp4_state);
		m_dec_param.buffers.mp4_state = NULL;
	}
	if (m_dec_param.buffers.mp4_tables) {
		free(m_dec_param.buffers.mp4_tables);
		m_dec_param.buffers.mp4_tables = NULL;
	}
	if (m_dec_param.buffers.mp4_stream) {
		free(m_dec_param.buffers.mp4_stream);
		m_dec_param.buffers.mp4_stream = NULL;
	}
}


*/

long CVideoDecoder::DecodeH264Frame(LPBYTE pInputFrameData, long lInputFrameSize, int nInputFrameWidth, int nInputFrameHeight, int nInputFrameType)
{
	if(m_bH264First)
	{
		m_bH264First = false;
		
		int nFourcc = FOURCC_BGR3; // 24 Bit BGR
		
		//2012-3-20 keunbae.song
		if(m_pH264Buf)
		{
			delete[] m_pH264Buf;
			m_pH264Buf = NULL;
		}

		m_pH264Buf = new BYTE[get_pic_size(nInputFrameWidth, nInputFrameHeight, nFourcc)];
		
		m_stH264Output_frame.four_cc   = nFourcc;
		m_stH264Output_frame.width     = nInputFrameWidth;
		m_stH264Output_frame.height    = nInputFrameHeight;
		m_stH264Output_frame.plane[0]  = m_pH264Buf;
		m_stH264Output_frame.stride[0] = get_pic_stride(nInputFrameWidth, nFourcc);
		m_stH264Output_frame.done      = NULL;
		
		m_pH264Bufstream->auxinfo(m_pH264Bufstream, SKIP_NONE, PARSE_FRAMES, &m_stH264Output_frame, sizeof(m_stH264Output_frame));
		
		//2012-3-29 keunbae.song (kobysong@esmlab.com)
//		::InvalidateRect(m_hWnd, &m_Rect, TRUE);
	}

	long lUsedSize = 0;
	UINT nFlag = 0;
	while(lUsedSize < lInputFrameSize)
	{
		if(!m_pH264Bufstream)
			break;
		uint32_t aaaa = m_pH264Bufstream->copybytes(m_pH264Bufstream, (pInputFrameData + lUsedSize), (lInputFrameSize - lUsedSize));
		lUsedSize += aaaa;
		
		//-- 2012-03-19 hongsu@esmlab.com		 
		if(m_pH264Bufstream)		
			nFlag = m_pH264Bufstream->auxinfo( m_pH264Bufstream, 0, GET_PARSE_STATE, NULL, 0);
		
		if(nFlag & (PIC_VALID_FLAG | PIC_FULL_FLAG))
		{
			if(BS_OK == m_pH264Bufstream->auxinfo(m_pH264Bufstream, 0, GET_PIC, &m_stH264Output_frame, sizeof(frame_tt)))
			{
				DrawImage(m_pH264Buf, m_stH264Output_frame.width, m_stH264Output_frame.height);
			}
		}
	}
	
	return 0;
}

long CVideoDecoder::DecodeMpeg4Frame(LPBYTE pInputFrameData, long lInputFrameSize, int nInputFrameWidth, int nInputFrameHeight, int nInputFrameType)
{
	if(m_pMpeg4Bufstream == NULL) return 1;

	if(m_bMpeg4First)
	{
		m_bMpeg4First = false;
		
		int nFourcc = FOURCC_BGR3; // 24 Bit BGR
		
		m_pMpeg4Buf = new BYTE[get_pic_size(nInputFrameWidth, nInputFrameHeight, nFourcc)];
		
		m_stMpeg4Output_frame.four_cc = nFourcc;
		m_stMpeg4Output_frame.width = nInputFrameWidth;
		m_stMpeg4Output_frame.height = nInputFrameHeight;
		m_stMpeg4Output_frame.plane[0] = m_pMpeg4Buf;
		m_stMpeg4Output_frame.stride[0] = get_pic_stride(nInputFrameWidth, nFourcc);
		m_stMpeg4Output_frame.done = NULL;

		m_pMpeg4Bufstream->auxinfo(m_pMpeg4Bufstream, SKIP_NONE, PARSE_FRAMES, &m_stMpeg4Output_frame, sizeof(m_stMpeg4Output_frame));
	}
	
	long lUsedSize = 0;
	UINT nFlag = 0;
	
	while(lUsedSize < lInputFrameSize)
	{
		lUsedSize += m_pMpeg4Bufstream->copybytes(m_pMpeg4Bufstream, (pInputFrameData + lUsedSize), (lInputFrameSize - lUsedSize));
		
		nFlag = m_pMpeg4Bufstream->auxinfo(m_pMpeg4Bufstream, 0, GET_PARSE_STATE, NULL, 0);
		
		if(nFlag & PARSE_DONE_FLAG)
		{
			if(BS_OK == m_pMpeg4Bufstream->auxinfo(m_pMpeg4Bufstream, 0, GET_PIC, &m_stMpeg4Output_frame, sizeof(frame_tt)))
			{
				DrawImage(m_pMpeg4Buf, m_stMpeg4Output_frame.width, m_stMpeg4Output_frame.height);
			}
		}
	}
	
	return 0;
}

long CVideoDecoder::DecodeMjpegFrame(LPBYTE pInputFrameData, long lInputFrameSize, int nInputFrameWidth, int nInputFrameHeight, int nInputFrameType)
{
	m_hBuffer = ::GlobalAlloc(GMEM_MOVEABLE, lInputFrameSize);
	if(m_hBuffer)
	{
		void* pBuffer = ::GlobalLock(m_hBuffer);
		if(pBuffer)
		{
			CopyMemory(pBuffer, pInputFrameData, lInputFrameSize);

			IStream* pStream = NULL;
			if(::CreateStreamOnHGlobal(m_hBuffer, FALSE, &pStream) == S_OK)
			{
				Bitmap* pBitmap = Gdiplus::Bitmap::FromStream(pStream);

				if(pBitmap)
				{
					if(pBitmap->GetLastStatus() == Gdiplus::Ok)
					{
						Graphics G(m_hDC);
						G.SetInterpolationMode(InterpolationModeHighQualityBicubic);
						
//						if(m_pParent->GetViewScreen())
							//G.DrawImage( pBitmap, 0, 0, 640, 480 );
						G.DrawImage(pBitmap, m_Rect.left, m_Rect.top, m_Rect.right, m_Rect.bottom);
						
						G.ReleaseHDC(m_hDC);
					}
/*
					if(m_strImagePath.GetLength() != 0)
					{
						CImage Image;
						Image.Load(pStream);
						Image.Save(m_strImagePath, Gdiplus::ImageFormatBMP);
						m_strImagePath = _T("");
					}
*/
					pStream->Release();
					delete pBitmap;
				}
			}
			::GlobalUnlock(m_hBuffer);
		}
		::GlobalFree(m_hBuffer);
		m_hBuffer = NULL;
	}

	return 0;
}

/*
long CVideoDecoder::DecodePentaFrame( LPBYTE pInputFrameData, long lInputFrameSize, int nInputFrameWidth, int nInputFrameHeight, int nInputFrameType )
{
	int nSize = get_pic_size(nInputFrameWidth, nInputFrameHeight, FOURCC_BGR3);
	
	if ( m_bPentaFirst )
	{
		m_bPentaFirst = false;
		
		m_pPentaBuf = new BYTE[nSize];
	}

	if ( m_dec_param.x_dim != nInputFrameWidth || m_dec_param.y_dim != nInputFrameHeight )
	{
		m_dec_param.x_dim = nInputFrameWidth;
		m_dec_param.y_dim = nInputFrameHeight;

		InitPentaDecoder();

		decore( 1, DEC_OPT_INIT, &m_dec_param, NULL);
	}

	m_dec_frame.bitstream = pInputFrameData;
	m_dec_frame.length = lInputFrameSize;
	m_dec_frame.bmp = m_pPentaBuf;
	m_dec_frame.stride = m_dec_param.x_dim;

	if(!m_dec_frame.bmp)
		return -1;
	
	// Dummy Loop
	BOOL bRepeat = FALSE;
	
	do
	{
		if (decore( 1, 0, &m_dec_frame, &m_dec_param) != DEC_OK)
			return -1;
		
		if (m_dec_frame.render_flag == 0)
		{
			m_dec_frame.render_flag = 1;
			// decore Again
			continue;
		}
	} while ( bRepeat );
	
	DrawImage( m_pPentaBuf, m_dec_param.x_dim, m_dec_param.y_dim );

	return 0;
}
*/

BOOL CVideoDecoder::DrawImage(LPBYTE pImageBuffer, int nImageWidth, int nImageHeight)
{
	// MJpeg would not call this function
	if(pImageBuffer == NULL || nImageWidth <= 0 || nImageHeight <= 0 ) 
		return FALSE;

	Graphics G(m_hDC);
	G.SetInterpolationMode(InterpolationModeHighQualityBicubic);
	
	Bitmap bitmap(nImageWidth, nImageHeight, PixelFormat24bppRGB);
	Gdiplus::Rect rc(0, 0, nImageWidth, nImageHeight);
	
	TRACE(_T("\n************************************************\n"));
	TRACE(_T("nImageWidth=%d, nImageHeight=%d\n"), nImageWidth, nImageHeight);
	TRACE(_T("************************************************\n"));

	BitmapData bitmapData;
	bitmapData.Width       = nImageWidth;
	bitmapData.Height      = nImageHeight;
	bitmapData.Stride      = get_pic_stride(nImageWidth, FOURCC_BGR3);
	bitmapData.PixelFormat = PixelFormat24bppRGB;
	bitmapData.Scan0       = (void*)pImageBuffer;
	bitmapData.Reserved    = NULL;
	
	bitmap.LockBits(&rc, ImageLockModeWrite | ImageLockModeUserInputBuf, PixelFormat24bppRGB, &bitmapData);
	bitmap.UnlockBits(&bitmapData);

	//2012-3-29 keunbae.song (kobysong@esmlab.com)
	//G.DrawImage(&bitmap, m_Rect.left, m_Rect.top, m_Rect.right, m_Rect.bottom);
	G.DrawImage(&bitmap, m_Rect.left, m_Rect.top, m_Rect.right-m_Rect.left, m_Rect.bottom-m_Rect.top);

	//if(m_strImagePath.GetLength() != 0)
	//{
	//	CLSID bmpClsid;
	//	GetEncoderClsid(_T("image/bmp"), &bmpClsid);
	//	bitmap.Save(m_strImagePath, &bmpClsid, NULL);
	//	m_strImagePath = _T("");
	//}
	
	G.ReleaseHDC(m_hDC);

	return TRUE;
}

int CVideoDecoder::GetEncoderClsid(const WCHAR *format, CLSID *pClsid)
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes
	ImageCodecInfo* pImageCodecInfo = NULL;
	GetImageEncodersSize(&num, &size);
	if(size == 0)
		return -1;  // Failure
	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if(pImageCodecInfo == NULL)
		return -1;  // Failure
	GetImageEncoders(num, size, pImageCodecInfo);
	for(UINT j = 0; j < num; ++j)
	{
		if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}    
	}
	free(pImageCodecInfo);
	return -1;  // Failure
}

//void CVideoDecoder::DBG(const char *fmt, ...)
//{
//	char s[2048];
//	va_list args;
//
//	va_start(args, fmt);
//	vsprintf(s, fmt, args);
//	va_end(args);
//
//	TRACE(s);
//}


