/////////////////////////////////////////////////////////////////////////////
//
//  ThumbNailList.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include <vector>
#include "PictureViewerDlg.h"
#include "ToolTipListCtrl.h"
#include "NXRemoteStudioFunc.h"
// CThumbNailList dialog
class CThumbNailList : public CRSChildDialog
{
	DECLARE_DYNAMIC(CThumbNailList)
public:
	CThumbNailList(CWnd* pParent = NULL);   // standard constructor
	virtual ~CThumbNailList();

// Dialog Data
	enum { IDD = IDD_DLG_THUMBNAIL };

	enum {
		FILE_NONE = -1,
		FILE_ADD,
		FILE_DELETE,
		FILE_ERR,
	};

	enum {
		PRESSED_NOTHING = -1,
		PRESSED_LEFT,
		PRESSED_RIGHT,
		//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
		PRESSED_MID,
	};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();	
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);		
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnReload(WPARAM , LPARAM );
	DECLARE_MESSAGE_MAP()	
	
public:
	CPictureViewerDlg* m_pPictureViewerDlg;
	//CMiLRe 20141127 픽쳐뷰 -> windows 사진뷰어 실행

private:
	
#ifdef PICTUREVIEW
	CPictureViewerDlg* m_pPictureViewerDlg;
#endif
	CImageList	m_ImageListThumb;		// image list holding the thumbnails

	CString		m_strImagePath;
	CString		m_strOpenedFile;
	CStringArray	m_arImageNames;	
	CStringArray	m_arMovieNames;
	CStringArray	m_arImageArray;
	int				m_nSelectedPicture;
	int				m_nFirstPic;
	int				m_nTooltipPicture;
	int				m_nDrawedPicture;	
	
	BOOL			m_bDrawAll;	
	BOOL			m_bLeftAlign;
	CRect			m_rtScrollBase;
	CRect			m_rtScrollBar;
	CToolTipCtrl	m_ToolTipFileName;
	BOOL			m_bScrollState;

	//dh0.seo 2014-11-11
	int m_nIndex;
	int m_nImgCount;
	int m_nCheckState;
	int m_nCheckSelect;
	int m_nAll;

	//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
	//BOOL m_bScrollBasePress;
	void MouseEvent();
public:
	BOOL m_bScrollBasePress;
	int				m_nPressed;
	BOOL m_bKey;

	void SetParentWnd(CWnd* pParent) {m_pParentWnd = pParent;}

	void DrawList(CRect rect);
	void OnMoveScroll(BOOL bRight, BOOL bOneStep);
	
	//---------------------------------------------------------------
	//-- Control Thumbnail List
	//---------------------------------------------------------------
	int	GetImageFileNames();	// gather the image file names
	void SetImageList();		// draw the thumbnails in list control
	void LoadThumbnail(CString strFileName = _T(""));
	void DrawSelectedImage(CString strFileName = _T(""));	
	BOOL IsDrawAll()	{ return m_bDrawAll; }
	BOOL IsFirstPic()			{ if(m_nFirstPic) return FALSE; else return TRUE; }
	BOOL IsLeftAlign()		{ return  m_bLeftAlign; }
	float GetInnerBarLength();
	float GetInnerBarLocation();
	int GetImageCount() { return m_arImageNames.GetCount();}

	//dh0.seo 2014-11-11
	int GetCameraIndex(int nIndex);
	BOOL CheckMovieFile(CString& strFileName);
	
	//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
	int GetThumnailFileCount(CString strFilePath = _T(""));
	//CMiLRe 20141124 섬네일 연사일 경우 여러자 못가지고 오는 버그 수정
	BOOL GetDirInFileCount();

	void ThumbnailLetf();
	void ThumbnailRight();

private:
	//---------------------------------------------------------------
	//-- Control File Manage Ment
	//---------------------------------------------------------------
	void UpdateFrames();
	BOOL CheckAlign(CRect rect);
	void DrawPicturesLeft(CDC* pDC, CRect rect);
	void DrawPicturesRight(CDC* pDC, CRect rect);
	void DrawScrollbar(CDC* pDC, CRect rect);	
	void MoveToLast();
	void DeleteSeletedFile(int nIndex);	
	BOOL GetImagePath();
	BOOL IsRaw(CString strPath);
	BOOL IsOpened(CString str);
	int PtInRect(CPoint pt);
	BOOL PtInBar(CPoint pt);
	CString m_strCameraID;
	int m_nLastCount;
	BOOL m_bCaptureCheck;

	void MoveToFirst();
};
