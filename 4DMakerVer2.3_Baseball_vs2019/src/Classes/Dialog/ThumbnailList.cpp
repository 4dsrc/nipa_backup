/////////////////////////////////////////////////////////////////////////////
//
//  ThumbNailList.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudioDlg.h"
#include "FileOperations.h"
#include "ThumbNailList.h"
#include <sys/timeb.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const int thumbnail_width		= 78;
static const int thumbnail_height		= 78;
static const int thumbnail_margin	= 12;
static const int thumbnail_margin_y	= 25;

static const int thumbnail_margin_start	= 3+15;
static const int thumbnail_picture_size	= 88;
static const int thumbnail_picture_gap	= 6;

static const int timer_button_pressed		= WM_USER + 0x0001;
static const int wait_pressed_time			= 400;

// CThumbNailList dialog
IMPLEMENT_DYNAMIC(CThumbNailList,  CRSChildDialog)

CThumbNailList::CThumbNailList(CWnd* pParent /*=NULL*/)
	: CRSChildDialog(CThumbNailList::IDD, pParent)
{
	//-- 2011-05-18 hongsu.jung
	//-- Load From Config
	m_strImagePath	 = _T("");
	m_strOpenedFile = _T("");
	m_nSelectedPicture = -1;
	m_nTooltipPicture	= -1;
	m_nDrawedPicture	= -1;
	m_nFirstPic = 0;
	m_bDrawAll = TRUE;
	m_bLeftAlign = TRUE;
	m_nPressed	= BTN_NOTHING;
	m_nLastCount = 0;
	//-- 2011-06-07 hongsu.jung
	m_pPictureViewerDlg = ((CNXRemoteStudioDlg*)AfxGetMainWnd())->m_pPictureViewer;
	//CMiLRe 20141127 픽쳐뷰 -> windows 사진뷰어 실행
#ifdef PICTUREVIEW
	m_pPictureViewerDlg = ((CNXRemoteStudioDlg*)AfxGetMainWnd())->m_pPictureViewer;	
#endif
	m_rtScrollBase		= CRect(0,0,0,0);
	m_rtScrollBar			= CRect(0,0,0,0);
	m_nIndex = 0;
	m_nImgCount = 0;
	//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
	m_bScrollBasePress = FALSE;
	m_strCameraID = _T("");
	m_nCheckState = 0;
	m_nCheckSelect = 0;
	m_bScrollState = FALSE;
	m_bKey = FALSE;
}

CThumbNailList::~CThumbNailList()
{
}

void CThumbNailList::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CThumbNailList, CRSChildDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()	
	ON_WM_LBUTTONDBLCLK()
	ON_WM_MOUSEMOVE()	
	ON_WM_TIMER()
	ON_MESSAGE(WM_LOAD_THUMBNAIL,OnReload)	
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()


void CThumbNailList::OnPaint()
{	
	UpdateFrames();
	CDialogEx::OnPaint();
}

void CThumbNailList::OnSize(UINT nType, int cx, int cy) 
{
    CDialog::OnSize(nType, cx, cy);
	UpdateFrames();	
} 

//------------------------------------------------------------------------------ 
//! @brief		DrawList()
//! @date		2010-07-27
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbNailList::UpdateFrames()
{
	if(!ISSet())
		return;

	//-- Rounding Dialog
	CPaintDC dc(this);
	
	CRect rectAll;
	GetClientRect(&rectAll);	

	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rectAll.Width(), rectAll.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rectAll.Width(), rectAll.Height(), WHITENESS);

	//-- Check Draw Picture Size & Align
	if(!CheckAlign(rectAll))
		return;

	//-- Draw BackGround
	mDC.FillRect(rectAll, &CBrush(RGB(63,63,63)));
	//-- Draw Pictures
	if(m_bLeftAlign)	
		DrawPicturesLeft(&mDC, rectAll);
	else				
		DrawPicturesRight(&mDC, rectAll);

	// Draw Scrollbar
	DrawScrollbar(&mDC, rectAll);
	dc.BitBlt(0, 0, rectAll.Width(), rectAll.Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		CheckAlign
//! @date		2011-07-28
//! @author	hongsu.jung
//! static const int thumbnail_picture_size	= 88;
//! static const int thumbnail_picture_gap	= 6;
//------------------------------------------------------------------------------ 
BOOL CThumbNailList::CheckAlign(CRect rect)
{
	int nAll = m_ImageListThumb.GetImageCount();	
	int nW = thumbnail_picture_size;
	int nWG = thumbnail_picture_gap;
	int nAW = rect.Width();

	if(!nAll) 
		return FALSE;

	//-- Calculate Draw Image Count	
	m_nDrawedPicture = (nAW) / (nW+nWG);
	if(nAW >= nAll*(nW+nWG))
	{
		m_bDrawAll = TRUE;
		m_nDrawedPicture = nAll;
		m_nFirstPic = 0;
		m_bLeftAlign = TRUE;		
		return TRUE;
	}
	else
	{
		//-- Draw Cutted Picture
		m_nDrawedPicture++;
		m_bDrawAll = FALSE;		
	
		//-- Drawed Picture would be started with setted First Picture
		if(m_nFirstPic)
		{	
			if(m_bKey)
			{
				if(m_nDrawedPicture + m_nFirstPic >= nAll && nAll - m_nSelectedPicture < 2)
				{
					m_bLeftAlign = FALSE;	
					//-- Change m_nFirstPic		
					if(m_nDrawedPicture < nAll )					
						m_nFirstPic = nAll - m_nDrawedPicture;
					return TRUE;
				}
			}
			else
			{
				if(m_nDrawedPicture + m_nFirstPic >= nAll)
				{
					m_bLeftAlign = FALSE;	
					//-- Change m_nFirstPic		
					if(m_nDrawedPicture < nAll )					
						m_nFirstPic = nAll - m_nDrawedPicture;
					return TRUE;
				}
			}
			
		}
	}

 	m_bLeftAlign = TRUE;
	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief		DrawPicturesLeft
//! @date		2011-07-28
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbNailList::DrawPicturesLeft(CDC* pDC, CRect rect)
{	
	CString strFile;
	int nW = thumbnail_picture_size;
	int nWG = thumbnail_picture_gap;
	int nAW = rect.Width();
	int nX = 0;
	int nY = 2;
	//-- Draw pictures how many ? DrawedPicture
	for(int nIndex = m_nFirstPic; nIndex < m_nFirstPic+m_nDrawedPicture; nIndex ++)
	{
		nX = (nIndex-m_nFirstPic)*(nW+nWG);
		//-- Draw Background
		if(m_nSelectedPicture == nIndex ) 	strFile.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_frame_focus.png")));
		else										strFile.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_frame_noimage.png")));
		DrawGraphicsImage(pDC, strFile,  CPoint(nX, nY), CSize(88,88));
		m_ImageListThumb.Draw (pDC, nIndex,CPoint(nX+5, nY+5 ),NULL);
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		DrawPicturesRight()
//! @date		2010-08-01
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbNailList::DrawPicturesRight(CDC* pDC, CRect rect)
{
	CString strFile;
	int nAll = m_ImageListThumb.GetImageCount();
	int nW = thumbnail_picture_size;
	int nWG = thumbnail_picture_gap;
	int nAW = rect.Width();
	int nX = 0;
	int nY = 2;
	//-- Draw pictures how many ? DrawedPicture
	for(int nIndex = nAll-1 ; nIndex >= nAll - m_nDrawedPicture ; nIndex --)
	{
		nX = nAW - (nAll - nIndex)*(nW+nWG);
		//-- Draw Background
		if(m_nSelectedPicture == nIndex ) 	strFile.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_frame_focus.png")));
		else										strFile.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_frame_noimage.png")));
		DrawGraphicsImage(pDC, strFile,  CPoint(nX, nY), CSize(thumbnail_picture_size,thumbnail_picture_size));
		m_ImageListThumb.Draw (pDC, nIndex,CPoint(nX+5, nY+5 ),NULL);
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		GetInnerBarLength
//! @date		2011-07-27
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
float CThumbNailList::GetInnerBarLength()
{
	float nRet = -1;
	int nAll = m_ImageListThumb.GetImageCount();
	if(nAll)
		nRet = (float)m_nDrawedPicture/nAll;
	return nRet;
}

//------------------------------------------------------------------------------ 
//! @brief		GetInnerBarLocation
//! @date		2011-07-27
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
float CThumbNailList::GetInnerBarLocation()
{
	float nRet = -1;
	int nAll = m_ImageListThumb.GetImageCount();
	if(nAll)
		nRet = (float)m_nFirstPic/nAll;
	return nRet;
}

//------------------------------------------------------------------------------ 
//! @brief		DrawScrollbar()
//! @date		2010-08-01
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbNailList::DrawScrollbar(CDC* pDC, CRect rect)
{
	int nAll = m_ImageListThumb.GetImageCount();
	if( nAll > m_nDrawedPicture - 1 || !IsFirstPic() )
	{
		if(IsDrawAll())
			return;

		m_rtScrollBase		= CRect(0,0,0,0);
		m_rtScrollBar			= CRect(0,0,0,0);

		CString strFilePath;
		int nX, nY, nH, nW, nAW;		
		//--
		//-- Basic Position			
		{
			nY = rect.bottom - 25;
			nH = 14;			
			nAW = rect.Width();
			//-- Set Base Rect
			m_rtScrollBase	= CRect(CPoint(rect.left, nY), CSize(nAW, nH));
			//-- Draw Bar
			nX = rect.left;
			nW = 5;			
			strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_scroll_bg_top.png")));		DrawGraphicsImage(pDC, strFilePath,  nX, nY, nW, nH);
			nX += 5;
			nW = nAW - 10;
			strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_scroll_bg_medium.png")));	DrawImage(pDC, strFilePath,  nX, nY, nW, nH);
			nX += nW;
			nW = 5;
			strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_scroll_bg_bottom.png")));	DrawGraphicsImage(pDC, strFilePath,  nX, nY, nW, nH);
			
		}

		//-- Draw Inner Bar
		//-- Calculator
		{
			float nLocation		= GetInnerBarLocation();
			float nLength		= GetInnerBarLength();
			if(nLocation <0 || nLength < 0)
				return;			
			nY = rect.bottom - 24;
			nH = 12;			
			nX = rect.left+1;
			nW = 3;

			nX += (int)((float)nAW*nLocation);

			//-- Check End Of Bar - 2011-08-11 jeansu : 위치 변경. top 그리기 전에 X 위치 재계산 하는 것으로 변경.
			if(nX + (int)((float)nAW*nLength) + 3 + 3 + 1 >= nAW)	
			{
				//nW = nAW-nX-3-1;
				nX = nAW - 3 - 3 - 1 - (int)((float)nAW*nLength);
			}
			if(m_bScrollState == FALSE)
			{
				strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_scroll_bar_top.png")));		
				DrawGraphicsImage(pDC, strFilePath,  nX, nY, nW, nH);
			}
			else
			{
				strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_scroll_bar_top_dim.png")));		
				DrawGraphicsImage(pDC, strFilePath,  nX, nY, nW, nH);
			}

			nX += 3;
			nW = (int)((float)nAW*nLength);

			//-- Set Base Rect
			m_rtScrollBar	= CRect(CPoint(nX-3, nY), CSize(nW+3, nH));
			if(m_bScrollState == FALSE)
			{
				strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_scroll_bar_medium.png")));	
				DrawImage			(pDC, strFilePath,  nX, nY, nW, nH);
			}
			else
			{
				strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_scroll_bar_medium_dim.png")));	
				DrawImage			(pDC, strFilePath,  nX, nY, nW, nH);
			}

			nX += nW;
			nW = 3;

			if(m_bScrollState == FALSE)
			{
				strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_scroll_bar_bottom.png")));	
				DrawGraphicsImage(pDC, strFilePath,  nX, nY, nW, nH);
			}
			else
			{
				strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_scroll_bar_bottom_dim.png")));	
				DrawGraphicsImage(pDC, strFilePath,  nX, nY, nW, nH);
			}
		}
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		OnMoveScroll(BOOL bRight)
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CThumbNailList::OnMoveScroll(BOOL bRight, BOOL bOneStep)
{
	if(IsDrawAll())
		return;

	CRect rect;
	GetClientRect(&rect);
	//-- Just 1 Step
	if(bOneStep)
	{
		if(bRight)
		{	
			if(IsLeftAlign())
				m_nFirstPic++;
		}
		else		
		{	
			if(m_nFirstPic > 0)
				m_nFirstPic--; 
			else					
				m_nFirstPic = 0;
		}	
	}
	else
	{
		if(bRight)
		{	
			if(IsLeftAlign())
			{
				m_nFirstPic += m_nDrawedPicture;				
			}
		}
		else		
		{	
			if(m_nFirstPic > 0)	
			{
				m_nFirstPic-=m_nDrawedPicture; 
				if(m_nFirstPic<0)
					m_nFirstPic = 0;
			}
			else					
				m_nFirstPic = 0;
		}	
	}
	Invalidate(FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief		OnTimer
//! @date		2011-6-16
//------------------------------------------------------------------------------ 
void CThumbNailList::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
		case timer_button_pressed:
		{
			KillTimer(timer_button_pressed);

			switch(m_nPressed)
			{
			case PRESSED_LEFT:		
				OnMoveScroll(FALSE,FALSE);	
				Invalidate(FALSE);
				if(IsFirstPic())
					return;
				break;
			case PRESSED_RIGHT:	
				OnMoveScroll(TRUE,FALSE);
				Invalidate(FALSE);
				if(!IsLeftAlign())
					return;
				break;
			//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
			case PRESSED_MID:
				if(IsFirstPic() || IsFirstPic())
				{
					m_bScrollBasePress = FALSE;
					return;
				}
				break;
			default: 
				CRSChildDialog::OnTimer(nIDEvent);
				return;
			}
			//-- Check Next Timer
			SetTimer(timer_button_pressed, wait_pressed_time/4, NULL);									
		}
		break;
	}
	CRSChildDialog::OnTimer(nIDEvent);
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-08-02
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbNailList::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_nPressed = BTN_NOTHING;
	//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
	m_bScrollBasePress = FALSE;
	m_bScrollState = FALSE;
	UpdateFrames();
	Invalidate(FALSE);
	CDialog::OnLButtonUp(nFlags, point);		
}

void CThumbNailList::ThumbnailLetf()
{
	int nX = 0;	
	int nItems = 0;
	CRect rect, rectPrev;
	GetClientRect(&rect);
	int nAW = rect.Width();
	int nAll = m_ImageListThumb.GetImageCount();

	m_bKey = TRUE;

	if(m_bLeftAlign)
	{
		if(m_nSelectedPicture > 0)
		{
			InvalidateRect(rect);
			m_nSelectedPicture = m_nSelectedPicture - 1;

			nX = (m_nSelectedPicture-m_nFirstPic)*(88+6);
			//-- Get Prev Selected Item				
			rectPrev = CRect( CPoint(nX,2), CSize(88,88));
			CBrush brush = CBrush(RGB(63,63,63));
			InvalidateRect(rectPrev);

			int nCount;
			nCount = m_nSelectedPicture / 5;
//			m_nFirstPic = nCount * 5;

			if(m_nSelectedPicture < 1)
				m_nFirstPic = 0;
			else if(m_nFirstPic-m_nSelectedPicture == 1)
				m_nFirstPic = m_nSelectedPicture;

			UpdateFrames();
		}
	}
	//-- Right Align
	else
	{
		if(m_nSelectedPicture > 0)
		{
			InvalidateRect(rect);
			m_nSelectedPicture = m_nSelectedPicture - 1;

			nX = nAW - (nAll - m_nSelectedPicture)*(thumbnail_picture_size+thumbnail_picture_gap);	
			//-- Get Prev Selected Item				
			rectPrev = CRect( CPoint(nX,2), CSize(88,88));
			CBrush brush = CBrush(RGB(63,63,63));
			InvalidateRect(rectPrev);

			int nCount;
			nCount = m_nSelectedPicture / 5;
//			m_nFirstPic = nCount * 5;

			if(m_nSelectedPicture < nAll - 5)
			{
				m_nFirstPic = m_nSelectedPicture;
			}

			UpdateFrames();
		}
	}
}

void CThumbNailList::ThumbnailRight()
{
	int nX = 0;	
	int nItems = 0;
	CRect rect, rectPrev;
	GetClientRect(&rect);
	int nAW = rect.Width();
	int nAll = m_ImageListThumb.GetImageCount();

	m_bKey = TRUE;

	if(m_bLeftAlign)
	{
		if(m_nSelectedPicture < nAll-1)
		{
			InvalidateRect(rect);
			m_nSelectedPicture = m_nSelectedPicture + 1;

			nX = (m_nSelectedPicture-m_nFirstPic)*(88+6);
			//-- Get Prev Selected Item				
			rectPrev = CRect( CPoint(nX,2), CSize(88,88));
			CBrush brush = CBrush(RGB(63,63,63));
			InvalidateRect(rectPrev);

			int nCount;
			nCount = m_nSelectedPicture / 5;
//			m_nFirstPic = nCount * 5;

			if(m_nSelectedPicture > 4 && m_nSelectedPicture - m_nFirstPic > 4)
				m_nFirstPic = m_nSelectedPicture - 4;

			UpdateFrames();
		}
	}
	//-- Right Align
	else
	{
		if(m_nSelectedPicture < nAll-1)
		{
			InvalidateRect(rect);
			m_nSelectedPicture = m_nSelectedPicture + 1;

			nX = nAW - (nAll - m_nSelectedPicture)*(thumbnail_picture_size+thumbnail_picture_gap);	
			//-- Get Prev Selected Item				
			rectPrev = CRect( CPoint(nX,2), CSize(88,88));
			CBrush brush = CBrush(RGB(63,63,63));
			InvalidateRect(rectPrev);

			int nCount;
			nCount = m_nSelectedPicture / 5;
			m_nFirstPic = nCount * 5;

			UpdateFrames();
		}
	}
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CThumbNailList::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nX = 0;	
	int nItems = 0;
	CRect rect, rectPrev;

	if(PtInBar(point))
	{
		Invalidate(FALSE);
		UpdateWindow();
		return;
	}

	if(point.x >= m_rtScrollBar.left && point.x <= m_rtScrollBar.right)
	{
		
		if(point.y >= m_rtScrollBar.top && point.y <= m_rtScrollBar.bottom)
		{
			m_bKey = FALSE;
			m_bScrollState = TRUE;
			UpdateFrames();
			Invalidate(FALSE);
		}
	}

	//-- Left Align
	if(m_bLeftAlign)
	{
		for(int nIndex = m_nFirstPic; nIndex < m_nFirstPic + m_nDrawedPicture ; nIndex ++)
		{
			nX = nItems*(88+6);
			rect = CRect( CPoint(nX,2), CSize(88,88));
			if(rect.PtInRect(point))
			{
				if(m_nSelectedPicture >= m_nFirstPic && m_nSelectedPicture < m_nFirstPic + m_nDrawedPicture)
				{
					nX = (m_nSelectedPicture-m_nFirstPic)*(88+6);
					//-- Get Prev Selected Item				
					rectPrev = CRect( CPoint(nX,2), CSize(88,88));
					CBrush brush = CBrush(RGB(63,63,63));	
					InvalidateRect(rectPrev);
				}
				m_nSelectedPicture =  nIndex;
				InvalidateRect(rect);
				break;
			}	
			nItems++;
		}
	}
	//-- Right Align
	else
	{
		int nAll = m_ImageListThumb.GetImageCount();
		GetClientRect(&rect);
		int nAW = rect.Width();		
		for(int nIndex = nAll-1 ; nIndex >= nAll - m_nDrawedPicture ; nIndex --)
		{
			nX = nAW - (nAll - nIndex)*(thumbnail_picture_size+thumbnail_picture_gap);
			rect = CRect(CPoint(nX, 2), CSize(thumbnail_picture_size,thumbnail_picture_size));
			if(rect.PtInRect(point))
			{
				if(nAll > m_nSelectedPicture && m_nSelectedPicture >= nAll - m_nDrawedPicture)
				{
					nX = nAW - (nAll - m_nSelectedPicture)*(thumbnail_picture_size+thumbnail_picture_gap);	
					//-- Get Prev Selected Item				
					rectPrev = CRect( CPoint(nX,2), CSize(88,88));
					InvalidateRect(rectPrev);
				}				
				m_nSelectedPicture =  nIndex;
				InvalidateRect(rect);
				break;
			}			
		}
	}
	CDialog::OnLButtonDown(nFlags, point);	
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDblClk
//! @date		2011-07-27
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CThumbNailList::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	if(point.y < 90)
		DrawSelectedImage();
	CDialog::OnLButtonDown(nFlags, point);	
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-07-14
//! @author	hongsu.jung
//---------------------------------------------------------------------------
BOOL CThumbNailList::PreTranslateMessage(MSG* pMsg) 
{
	m_ToolTipFileName.RelayEvent(pMsg);
	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CThumbNailList::CheckMovieFile(CString& strFileName)
{
	BOOL bRet = FALSE;
	CString strMovie1, strMovie2;
	strMovie1 = strFileName;
	strMovie2 = strFileName;
	strMovie1.Replace(_T("jpg"), _T("mp4"));
	strMovie2.Replace(_T("jpg"), _T("avi"));

	for(int n = 0; n < m_arMovieNames.GetCount(); n++)
	{
		if(strMovie1.CompareNoCase(m_arMovieNames.GetAt(n)) == 0 ||
			strMovie2.CompareNoCase(m_arMovieNames.GetAt(n)) == 0 )
		{
			strFileName = m_arMovieNames.GetAt(n);
			bRet = TRUE;
			break;
		}
	}
	return bRet;
}


//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CThumbNailList::OnMouseMove(UINT nFlags, CPoint point)
{
	
	int nCount = m_ImageListThumb.GetImageCount();
	int nIndex = PtInRect(point);

	if(nIndex > -1 && nCount > nIndex)
	{
		
		CString strToolTip;
		CString strTemp;
		if(m_nTooltipPicture != nIndex)
		{
			m_ToolTipFileName.Pop();
		}
		m_nTooltipPicture = nIndex;
		
		if(m_nTooltipPicture >= m_arImageArray.GetCount())
			return;

		strToolTip = m_arImageArray.GetAt(m_nTooltipPicture);
		CheckMovieFile(strToolTip);

		
		m_ToolTipFileName.AddTool(this, (LPCTSTR)strToolTip, NULL, 0);//툴팁정의
		m_ToolTipFileName.Activate(TRUE);//툴팁활성화
	}
	if(nCount == 0)
		m_ToolTipFileName.Activate(FALSE);

	//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
	if((m_bScrollBasePress == TRUE) &&(m_nPressed == PRESSED_MID))
	{
		CRect rect;
		GetClientRect(&rect);	
		if(point.y < m_rtScrollBar.top + rect.bottom && point.y >m_rtScrollBar.top-rect.bottom)
		//if(point.y < m_rtScrollBar.top+10 && point.y >m_rtScrollBar.top-10)
		{
			if(point.x > m_rtScrollBar.right)
			{
				OnMoveScroll(TRUE,FALSE);
				SetTimer(timer_button_pressed, wait_pressed_time, NULL);	
			}
			else if(point.x < m_rtScrollBar.left)
			{
				OnMoveScroll(FALSE,FALSE);
				SetTimer(timer_button_pressed, wait_pressed_time, NULL);	
			}
		}
		else
		{
			m_bScrollBasePress = FALSE;
		}
		MouseEvent();
	}
	CDialog::OnMouseMove(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInBar(CPoint pt)
//! @date		2011-08-01
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
BOOL CThumbNailList::PtInBar(CPoint pt)
{
	if(m_rtScrollBase.PtInRect(pt))
	{
		//-- Move Left
		if(pt.x < m_rtScrollBar.left) 
		{
			OnMoveScroll(FALSE,FALSE);
			m_nPressed = PRESSED_LEFT;
			SetTimer(timer_button_pressed, wait_pressed_time, NULL);	
		}
		else if(pt.x > m_rtScrollBar.right) 
		{
			OnMoveScroll(TRUE,FALSE);
			m_nPressed = PRESSED_RIGHT;
			SetTimer(timer_button_pressed, wait_pressed_time, NULL);	
		}
		else
		{
			//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
			m_bScrollBasePress = TRUE;
			m_nPressed = PRESSED_MID;
		}
		return FALSE;
	}
	
	m_nPressed = BTN_NOTHING;
	return FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect(CPoint pt)
//! @date		2011-07-27
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
int  CThumbNailList::PtInRect(CPoint pt)
{
	int nX = 0;	
	CRect rect;
	if(m_bLeftAlign)
	{
		for(int nIndex = m_nFirstPic; nIndex < m_nFirstPic + m_nDrawedPicture ; nIndex ++)
		{
			nX = (nIndex-m_nFirstPic)*(thumbnail_picture_size+thumbnail_picture_gap);
			rect = CRect( CPoint(nX,2), CSize(thumbnail_picture_size,thumbnail_picture_size));
			if(rect.PtInRect(pt))
				return nIndex;
		}
	}
	else
	{
		int nAll = m_ImageListThumb.GetImageCount();
		GetClientRect(&rect);
		int nAW = rect.Width();		
		for(int nIndex = nAll-1 ; nIndex >= nAll - m_nDrawedPicture ; nIndex --)
		{
			nX = nAW - (nAll - nIndex)*(thumbnail_picture_size+thumbnail_picture_gap);
			rect = CRect(CPoint(nX, 2), CSize(thumbnail_picture_size,thumbnail_picture_size));
			if(rect.PtInRect(pt))
				return nIndex;
		}
	}
	return -1;
}

//------------------------------------------------------------------------------ 
//! @brief		OnInitDialog()
//! @date		2010-08-01
//! @author	hongsu.jung
// CThumbNailList message handlers
//------------------------------------------------------------------------------ 
BOOL CThumbNailList::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();

	//-- 2011-07-14
	//-- Create Tooltip
	if (m_ToolTipFileName.m_hWnd == NULL) 
	{ 
		// Create ToolTip control 
		m_ToolTipFileName.Create(this); 
		// Create inactive 
		m_ToolTipFileName.Activate(FALSE);
	} 

	if(!GetImagePath())
		return FILE_NONE;

	//-- Create Image List
	m_ImageListThumb.Create(thumbnail_width, thumbnail_height, ILC_COLOR24, 0, 1);//| ILC_MASK, 0, 1);	
	//PostMessage(WM_LOAD_THUMBNAIL);

	::SendMessage(m_ToolTipFileName.m_hWnd, TTM_SETDELAYTIME, TTDT_INITIAL, (LPARAM)MAKELONG(10,0));
	return TRUE;  // return TRUE  unless you set the focus to a control
}

//------------------------------------------------------------------------------ 
//! @brief		OnReload()
//! @date		2010-08-01
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT CThumbNailList::OnReload(WPARAM w, LPARAM l)
{
	LoadThumbnail();
	return 0;
}

//------------------------------------------------------------------------------ 
//! @brief		LoadThumbnail()
//! @date		2010-07-07
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbNailList::LoadThumbnail(CString strFileName)
{
	if(!strFileName.GetLength())
	{
		int nRet = FILE_NONE;	
		nRet = GetImageFileNames();

		if (FILE_ERR == nRet )
		{
			//m_arImageNames.RemoveAll();

			//CMiLRe 20141124 섬네일 연사일 경우 여러자 못가지고 오는 버그 수정
			int nSize = m_ImageListThumb.GetImageCount();
			while(nSize--)
			{
				m_ImageListThumb.Remove(nSize);			
			}
		
			m_pParentWnd->Invalidate();
			Invalidate(FALSE);
			return;
		}

		SetImageList();		
		
		//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
		if(m_nImgCount == m_ImageListThumb.GetImageCount())
		{			
//			MoveToLast();
			MoveToFirst();
			//CMiLRe 20141120 사진 파일을 지울때 삭제되지 않는 버그 수정
			m_pParentWnd->Invalidate();
			Invalidate(FALSE);
			return;
		}
		else
			m_nImgCount = m_ImageListThumb.GetImageCount();

		//-- Redraw
		Invalidate(FALSE);
		//-- Move To Last
		//MoveToLast();
		MoveToFirst();
		//-- Open Added Files
		if(strFileName.GetLength() && nRet == FILE_ADD)
			DrawSelectedImage(strFileName);
	}
	else
	{
		CBitmap* pImage		= NULL;
		Bitmap* pThumbnail	= NULL;
		HBITMAP	hBmp = NULL;

		int i = m_ImageListThumb.GetImageCount();
		m_ImageListThumb.SetImageCount(i+1);

		CString strTemp = strFileName;
		strTemp.Replace(m_strImagePath,_T(""));
		strTemp.Replace(_T("\\"),_T(""));
		m_arImageNames.Add(strTemp);

		Bitmap img(strFileName);
		pThumbnail = static_cast<Bitmap*>(img.GetThumbnailImage(thumbnail_width, thumbnail_height, NULL, NULL));
		if(!pThumbnail)		
			return;
		// attach the thumbnail bitmap handle to an CBitmap object
		pThumbnail->GetHBITMAP(NULL, &hBmp);
		pImage = new CBitmap();		 
		pImage->Attach(hBmp);
		// add bitmap to our image list
		m_ImageListThumb.Replace(i, pImage, NULL);
		delete pImage;
		delete pThumbnail;

		Invalidate(FALSE);
		MoveToLast();
		DrawSelectedImage(strFileName);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		MoveToLast()
//! @date		2010-08-01
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbNailList::MoveToLast()
{
	//-- Check IsAll
	if(IsDrawAll())
		return;
	int nAll = m_ImageListThumb.GetImageCount();
	//-- 2011-08-05 jeansu - m_nFirstPic = nAll ---> m_nFirstPic = nAll -1 로 변경.
	m_nFirstPic = nAll - 1;
}

void CThumbNailList::MoveToFirst()
{
	if(IsDrawAll())
		return;
	m_nFirstPic = 0;
}

//------------------------------------------------------------------------------ 
//! @brief		GetImagePath()
//! @date		2010-07-07
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
BOOL CThumbNailList::GetImagePath()
{	
	//-- Set File Name
#ifdef	IFA
	CString strPicturePath = RSGetValueStr(0);	
#else
	CString strPicturePath = RSGetValueStr(RS_OPT_PCSET1_PICTUREPATH);	
#endif
	switch(RSGetValueEnum(RS_OPT_PCSET2_FILENAME_TYPE))
	{
	case RS_FILENAME_PRE_NAME_DATE_TIME:
		CreateAllDirectories(strPicturePath);
		m_strImagePath = strPicturePath;
		break;
	case RS_FILENAME_DATE_PRE_NAME_TIME:
		strPicturePath.AppendFormat(_T("\\%s"), GetDate());
		CreateAllDirectories(strPicturePath);
		m_strImagePath =  strPicturePath;
		break;
	case RS_FILENAME_NAME_PRE_DATE_TIME:
		{
			//-- Set m_strImagePath
			//CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
			// CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetApp()->GetMainWnd();	// Thread Ver.

			//CMiLRe 20141124 섬네일 연사일 경우 여러자 못가지고 오는 버그 수정
			int nCnt = GetSelectedItem();
			CString strName = GetPTPName(nCnt);
			if(!GetPTPCount())
				return FALSE;
			//int nCnt = pMainWnd->GetSelectedItem();
			//CString strName = pMainWnd->GetPTPName(nCnt);
			
			//-- 2011-8-18 Lee JungTaek
			//-- Check Name
			//if(!pMainWnd->m_SdiMultiMgr.GetPTPCount())
				//return FALSE;
			
			strPicturePath.AppendFormat(_T("\\%s"), strName);
			CreateAllDirectories(strPicturePath);
			m_strImagePath =  strPicturePath;
		}
		break;
	default:
		m_strImagePath = strPicturePath;
		break;
	}

	return TRUE;
}

//dh0.seo 2014-11-11
int CThumbNailList::GetCameraIndex(int nIndex)
{
	m_nIndex = nIndex;
	return nIndex;
}

//------------------------------------------------------------------------------ 
//! @brief		GetImageFileNames()
//! @date		2010-07-07
//! @date		2010-07-25
//! @author	hongsu.jung
//! @edit		Add Option (Added, Deleted, Error)
//------------------------------------------------------------------------------ 
int  CThumbNailList::GetImageFileNames()
{			
	int nRet = FILE_NONE;
	CString	strExt;
	CString	strName;
	CString	strPattern;
	BOOL	bRC = TRUE;

	HANDLE					hFind = NULL;
	WIN32_FIND_DATA		FindFileData;
	CStringArray				arImageNames;
	CStringArray				arMovieNames;
	CStringArray				arImageArray;

	//-- 2011-07-19
	if(!GetImagePath())
		return FILE_NONE;

	//dh0.seo 2014-11-11  // CameraNum Folder
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	if(pMainWnd)
		m_strCameraID = pMainWnd->m_SdiMultiMgr.GetDevUniqueID(m_nIndex);
//	CString strCamera = _T("Camera");
	CString strFolder;
	CString strFilePath = m_strImagePath;
//	int nCameraIndex = m_nIndex + 1;
//	strFolder.Format(_T("%s"), m_strCameraID);//strCamera, nCameraIndex);  // CameraNum Folder
//	m_strImagePath.Format(_T("%s\\%s"), strFilePath, strFolder); // CameraNum Folder

	if(m_strImagePath[m_strImagePath.GetLength() - 1] == TCHAR('\\') )	
		strPattern.Format(_T("%s*.*"), m_strImagePath);
	else
		strPattern.Format(_T("%s\\*.*"), m_strImagePath);

	hFind = ::FindFirstFile(strPattern, &FindFileData);	// strat search	
	if (hFind == INVALID_HANDLE_VALUE)
	{
		LPVOID  msg;
		::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, 
			NULL, 
			GetLastError(), 
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&msg, 
			0, 
			NULL);		
		::LocalFree(msg);
		nRet = FILE_ERR;
		return nRet;
	}

	// filter off the system files and directories
	if (!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)  &&
		!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM)     &&
		!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)     &&
		!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_TEMPORARY))
	{  	  
		// test file extension
		strName = FindFileData.cFileName;
		strExt = strName.Right(3);

		if ( (strExt.CompareNoCase( TEXT("bmp") ) == 0) ||
			(strExt.CompareNoCase( TEXT("jpg") ) == 0) ||
			(strExt.CompareNoCase( TEXT("gif") ) == 0) ||
			(strExt.CompareNoCase( TEXT("tif") ) == 0) ||
			(strExt.CompareNoCase( TEXT("srw") ) == 0) ||
			(strExt.CompareNoCase( TEXT("png") ) == 0) )
		{
			// save the image file name
			arImageNames.Add(strName);
		}
		else if( (strExt.CompareNoCase( TEXT("mp4") ) == 0) ||
			(strExt.CompareNoCase( TEXT("avi") ) == 0) )
		{
			arMovieNames.Add(strName);
		}
	}
	

	// loop through to add all of them to our vector	
	while (bRC)
	{
		bRC = ::FindNextFile(hFind, &FindFileData);
		if (bRC)
		{
			// filter off the system files and directories
			if (!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)  &&
				!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM)     &&
				!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)     &&
				!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_TEMPORARY))
			{
				// test file extension
				strName = FindFileData.cFileName;
				strExt = strName.Right(3);

				if ( (strExt.CompareNoCase( TEXT("bmp") ) == 0) ||
					(strExt.CompareNoCase( TEXT("jpg") ) == 0) ||
					(strExt.CompareNoCase( TEXT("gif") ) == 0) ||
					(strExt.CompareNoCase( TEXT("tif") ) == 0) ||
					(strExt.CompareNoCase( TEXT("srw") ) == 0) ||
					(strExt.CompareNoCase( TEXT("png") ) == 0) )
				{
					// save the image file name
					arImageNames.Add(strName);
				}
				else if( (strExt.CompareNoCase( TEXT("mp4") ) == 0) ||
					(strExt.CompareNoCase( TEXT("avi") ) == 0) )
				{
					arMovieNames.Add(strName);
				}
			}
		}  
		else
		{
			DWORD err = ::GetLastError();
			if (err !=  ERROR_NO_MORE_FILES)
			{
				LPVOID msg;
				::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, 
					NULL, err, 
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					(LPTSTR)&msg, 0, NULL);
				//MessageBox((LPTSTR)msg, CString((LPCSTR)IDS_TITLE), MB_OK|MB_ICONSTOP);
				::LocalFree(msg);
				::FindClose(hFind);
				nRet = FILE_ERR;
				return nRet;
			}
		}
	} // end of while loop

	// close the search handle
	::FindClose(hFind);
	
//------이미지 거꾸로 가져오기 위해서 추가 Start-----------
	int nAll = arImageNames.GetSize();
	while(nAll--)
	{
		CString strName;
		strName.Format(_T("%s"),arImageNames.GetAt(nAll));
		arImageArray.Add(strName);
	}
//------이미지 거꾸로 가져오기 위해서 추가 End-----------

	int nPrev = m_arImageNames.GetSize();
	int nNew = arImageNames.GetSize();
	
	
	//CMiLRe 20141120 사진 파일을 지울때 삭제되지 않는 버그 수정
	if(nPrev > nNew )
		nRet = FILE_DELETE;
	else	if(nPrev < nNew )
		nRet = FILE_ADD;
	else if(nNew == 0 && GetThumnailFileCount() != 0)		
		nRet = FILE_ERR;
	else							
		nRet = FILE_NONE;

//------Capture 시 새로 Array 사용하기 위해 추가 Start-----------
	m_arImageArray.RemoveAll();
	m_arImageArray.Copy(arImageArray);
//------Capture 시 새로 Array 사용하기 위해 추가 End-----------

	m_arImageNames.RemoveAll();
	m_arImageNames.Copy(arImageNames);

	m_arMovieNames.RemoveAll();
	m_arMovieNames.Copy(arMovieNames);

	arImageNames.RemoveAll();
	arMovieNames.RemoveAll();
	arImageArray.RemoveAll();//사용 끝난 Array 제거

	return nRet;
}

//------------------------------------------------------------------------------ 
//! @brief		SetImageList()
//! @date		2010-07-07
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void  CThumbNailList::SetImageList()
{
	BOOL bMovie = FALSE;
	CBitmap* pImage		= NULL;
	Bitmap* pThumbnail	= NULL;
	HBITMAP	hBmp = NULL;
	CString	strPath;
	CString	strFileName;
	int			nAll;
	CString strPlayPath;
	Bitmap* pPlayImage	= NULL;
	CBitmap* pPlayImage_	= NULL;
	HBITMAP	hBmp_ = NULL;
	int			nCount;

	//-- Clean Previous Data
	// reset our image list
	int nSize = m_ImageListThumb.GetImageCount();
	while(nSize--)
	{
		m_ImageListThumb.Remove(nSize);			
	}

	// set the size of the image list
	m_ImageListThumb.SetImageCount(m_arImageNames.GetCount());
	// Reverse the elements in the vector
	nAll = m_arImageNames.GetSize();

	BOOL bFileState;
	bFileState = RSGetFileWriteStatus();

	if(m_nCheckState == 1 && m_bCaptureCheck == TRUE && bFileState == TRUE && nAll > 0) //m_nLastCount != 0 && m_nLastCount != nAll && 
	{
		CString PictureName;
		CString strPictureName;
		CString	strPath;
		CString strPicturePath;
		strPath = RSImagePath();

		strPicturePath.Format(_T("%s.jpg"), strPath);

		PictureName = strPath.Right(strPath.GetLength()-strPath.ReverseFind(('\\'))-1);

		strPictureName.Format(_T("%s.jpg"), PictureName);

//		PictureName = m_arImageNames.GetAt(nAll-1);
		m_pPictureViewerDlg->PictureTitle(strPictureName);

/*		if(m_strImagePath[m_strImagePath.GetLength()-1] == TCHAR('\\'))			
			strPath.Format( _T("%s%s"), m_strImagePath, PictureName );
		else
			strPath.Format( _T("%s\\%s"), m_strImagePath, PictureName ); */
		
		if(m_nCheckSelect == 0)
			ShellExecute(NULL ,_T("open"),strPicturePath, _T("") , _T(""), SW_SHOWNORMAL);	
		else
		{
			m_pPictureViewerDlg->SetImagePath(strPicturePath);

			BOOL bOpened = m_pPictureViewerDlg->IsWindowVisible();
			if(!bOpened)
				m_pPictureViewerDlg->ShowWindow(SW_SHOW);

			m_pPictureViewerDlg->Invalidate(false);
			m_pPictureViewerDlg->SendMessage(WM_SETFOCUS);
		}
		RSCaptureCheck(FALSE);
	}

	m_nLastCount = nAll;

	CString strName;
	while(nAll--)
//	for(int i = 0; i < nAll; i++)
	{
		bMovie = FALSE;
		strName.Format(_T("%s"),m_arImageArray.GetAt(nAll)); // m_arImageNames -> m_arImageArray 변경
		
		// load the bitmap
		strPath.Format(_T("%s\\%s"), m_strImagePath, strName);
		strPlayPath.Format(RSGetRoot(_T("img\\03.Live View\\Movie_Play.png")));

		if(IsRaw(strPath))
			m_ImageListThumb.Replace(nAll, AfxGetApp()->LoadIcon(IDI_RAWFILE));
		else
		{
			Bitmap img(strPath);
			pThumbnail = static_cast<Bitmap*>(img.GetThumbnailImage(thumbnail_width, thumbnail_height, NULL, NULL));
			if(!pThumbnail)		
				return;
			// attach the thumbnail bitmap handle to an CBitmap object
			pThumbnail->GetHBITMAP(NULL, &hBmp);
			pImage = new CBitmap();		 
			pImage->Attach(hBmp);
			// add bitmap to our image list
			m_ImageListThumb.Replace(nAll, pImage, NULL);

			bMovie = CheckMovieFile(strName);
			if(bMovie)
			{
				Bitmap image(strPlayPath);
				pPlayImage = static_cast<Bitmap*>(image.GetThumbnailImage(20, 20, NULL, NULL));
				pPlayImage->GetHBITMAP(NULL, &hBmp_);
				pPlayImage_ = new CBitmap();
				pPlayImage_->Attach(hBmp_);
				m_ImageListThumb.Replace(nAll, pPlayImage_, NULL);
				delete pPlayImage;
				delete pPlayImage_;
			}
			
			delete pImage;
			delete pThumbnail;				
		}		
	}

/*	for(int i=0; i<nAll; i++)
	{
		bMovie = FALSE;
		strName = m_arImageNames.GetAt(i);

		// load the bitmap
		strPath.Format(_T("%s\\%s"), m_strImagePath, strName);
		strPlayPath.Format(RSGetRoot(_T("img\\03.Live View\\Movie_Play.png")));

		if(IsRaw(strPath))
			m_ImageListThumb.Replace(i, AfxGetApp()->LoadIcon(IDI_RAWFILE));
		else
		{
			Bitmap img(strPath);
			pThumbnail = static_cast<Bitmap*>(img.GetThumbnailImage(thumbnail_width, thumbnail_height, NULL, NULL));
			if(!pThumbnail)		
				return;
			// attach the thumbnail bitmap handle to an CBitmap object
			pThumbnail->GetHBITMAP(NULL, &hBmp);
			pImage = new CBitmap();		 
			pImage->Attach(hBmp);
			// add bitmap to our image list
			m_ImageListThumb.Replace(i, pImage, NULL);

			bMovie = CheckMovieFile(strName);
			if(bMovie)
			{
				Bitmap image(strPlayPath);
				pPlayImage = static_cast<Bitmap*>(image.GetThumbnailImage(20, 20, NULL, NULL));
				pPlayImage->GetHBITMAP(NULL, &hBmp_);
				pPlayImage_ = new CBitmap();
				pPlayImage_->Attach(hBmp_);
				m_ImageListThumb.Replace(i, pPlayImage_, NULL);
				delete pPlayImage;
				delete pPlayImage_;
			}

			delete pImage;
			delete pThumbnail;				
		}		
	} */
}

//------------------------------------------------------------------------------ 
//! @brief		DrawSelectedImage()
//! @date		2010-07-07
//! @edit		2010-07-25
//! @author	hongsu.jung
#include "Registry.h"
//------------------------------------------------------------------------------ 
//CMiLRe 20141127 픽쳐뷰 -> windows 사진뷰어 실행, m_arImageNames -> m_arImageArray로 변경
#ifndef PICTUREVIEW
void CThumbNailList::DrawSelectedImage(CString strFileName)
{
	CString strTemp;
	BOOL bAddFile = FALSE;

	if(strFileName.GetLength())	
	{
		bAddFile = TRUE;

		int nPos = strFileName.ReverseFind(_T('\\'));
		strFileName = strFileName.Mid(nPos + 1);
	}

	if(bAddFile)
		return;

	if(bAddFile)
	{
		for(int i = m_arImageArray.GetCount() - 1; i >= 0; i--)
		{
			if(strFileName.CompareNoCase(m_arImageArray.GetAt(i)) == 0)
			{
				m_nSelectedPicture = i;
				break;
			}
		}
	}
	else
	{
		if(m_nSelectedPicture < 0 ||  m_nSelectedPicture > m_arImageArray.GetSize())
			return;
		strFileName = m_arImageArray.GetAt(m_nSelectedPicture);
	}
	
	strTemp = strFileName;
	if(CheckMovieFile(strTemp))
	{
		strFileName = strTemp;

		//-- Add Exe File Name
		CString strMovieFile;
		strMovieFile.Format(_T("%s\\%s"), m_strImagePath, strFileName);
		//-- Check FExe file Exist
		CFileOperation fo;
		if(!fo.IsFileExist(strMovieFile))
		{
			MessageBox(_T("No File"), _T("Assist"), MB_OK | MB_ICONSTOP);			
			return;
		}
		ShellExecute(NULL ,_T("open"),strMovieFile, _T("") , _T(""), SW_SHOWNORMAL);	
	}
	else
	{
		if(m_nSelectedPicture < 0 ||  m_nSelectedPicture > m_arImageNames.GetSize())
			return;

		CString strPath = _T("");
		// read the image file
		if(m_strImagePath[m_strImagePath.GetLength()-1] == TCHAR('\\'))			strPath.Format( _T("%s%s"), m_strImagePath, strFileName );
		else																	strPath.Format( _T("%s\\%s"), m_strImagePath, strFileName );

		if(IsOpened(strPath))
			return;

		//-- IsRaw?
		if(IsRaw(strPath))
		{
			//-- Check RAW Converter in Registry
			CRegistry reg;
			CString strSRWFile = _T("");
			if(reg.VerifyKey(HKEY_LOCAL_MACHINE, REG_RAW_ROOT))
			{
				if(reg.Open(HKEY_LOCAL_MACHINE, REG_RAW_ROOT))
				{
					reg.Read (REG_RAW_DIR, strSRWFile);
					reg.Close();			
				}
			}
			//-- Add Exe File Name
			strSRWFile.Append(_T("\\RAWConv4.exe"));
			//-- Check FExe file Exist
			CFileOperation fo;
			if(!fo.IsFileExist(strSRWFile))
			{
				MessageBox(_T("Please Install Samsung Raw Converter"), _T("Assist"), MB_OK | MB_ICONSTOP);			
				return;
			}
			ShellExecute(NULL ,_T("open"),strSRWFile, strFileName , m_strImagePath, SW_SHOWNORMAL);		

			reg.Close();
		}
		else
		{
			//------------------------------------------------
			//-- Get Open 3rd vendor Program
			//------------------------------------------------
			CRegistry reg;
			CString strProgram;
			LPCTSTR lpszSection;			

			lpszSection = REG_RS_OPENPROGRAM;
			if(reg.VerifyKey(HKEY_LOCAL_MACHINE, REG_RS_ROOT))
			{
				if(reg.Open(HKEY_LOCAL_MACHINE, lpszSection))
				{
					reg.Read (REG_RS_UPDATE, strProgram);			
					reg.Close();			
				}
				if(strProgram.GetLength())
				{
					ShellExecute(NULL ,_T("open"),strProgram,strFileName,NULL,SW_SHOWNORMAL);				
				}
				else
				{							
					ShellExecute(NULL ,_T("open"),strPath,NULL,NULL,SW_SHOWNORMAL);					
				}

				reg.Close();
			}
			else
			{		
				ShellExecute(NULL ,_T("open"),strPath, _T("") , _T(""), SW_SHOWNORMAL);	
				reg.Close();	
				return;
			}
		}
	}
	
}
#else
void CThumbNailList::DrawSelectedImage(CString strFileName)
{
	CString strTemp;
	BOOL bAddFile = FALSE;

	//-- Check AddFile
	if(strFileName.GetLength())	
	{
		bAddFile = TRUE;

		int nPos = strFileName.ReverseFind(_T('\\'));
		strFileName = strFileName.Mid(nPos + 1);
	}

	BOOL bOpened = m_pPictureViewerDlg->IsWindowVisible();
	if(bAddFile && !bOpened)
		return;

	//-- 2011-8-31 Lee JungTaek
	//-- Add File Name
	if(bAddFile)
	{
		for(int i = m_arImageNames.GetCount() - 1; i >= 0; i--)
		{
			if(strFileName.CompareNoCase(m_arImageNames.GetAt(i)) == 0)
			{
				m_nSelectedPicture = i;
				break;
			}
		}
	}
	else
	{
		//-- 2013-02-05 hongsu.jung
		//-- debugging
		if(m_nSelectedPicture < 0 ||  m_nSelectedPicture > m_arImageNames.GetSize())
			return;
		strFileName = m_arImageNames.GetAt(m_nSelectedPicture);
	}
	
	strTemp = strFileName;
	if(CheckMovieFile(strTemp))
	{
		strFileName = strTemp;

		//-- Add Exe File Name
		CString strMovieFile;
		strMovieFile.Format(_T("%s\\%s"), m_strImagePath, strFileName);
		//-- Check FExe file Exist
		CFileOperation fo;
		if(!fo.IsFileExist(strMovieFile))
		{
			MessageBox(_T("No File"), _T("Assist"), MB_OK | MB_ICONSTOP);			
			return;
		}
		ShellExecute(NULL ,_T("open"),strMovieFile, _T("") , _T(""), SW_SHOWNORMAL);	
	}
	else
	{
		if(m_nSelectedPicture < 0 ||  m_nSelectedPicture > m_arImageNames.GetSize())
			return;

		CString strPath = _T("");
		// read the image file
		if(m_strImagePath[m_strImagePath.GetLength()-1] == TCHAR('\\'))			strPath.Format( _T("%s%s"), m_strImagePath, strFileName );
		else																	strPath.Format( _T("%s\\%s"), m_strImagePath, strFileName );

		if(IsOpened(strPath))
			return;

		//-- IsRaw?
		if(IsRaw(strPath))
		{
			if(m_pPictureViewerDlg->IsWindowVisible())
				m_pPictureViewerDlg->ShowWindow(SW_HIDE);

			//-- Check RAW Converter in Registry
			CRegistry reg;
			CString strSRWFile = _T("");
			if(reg.VerifyKey(HKEY_LOCAL_MACHINE, REG_RAW_ROOT))
			{
				if(reg.Open(HKEY_LOCAL_MACHINE, REG_RAW_ROOT))
				{
					reg.Read (REG_RAW_DIR, strSRWFile);
					reg.Close();			
				}
			}
			//-- Add Exe File Name
			strSRWFile.Append(_T("\\RAWConv4.exe"));
			//-- Check FExe file Exist
			CFileOperation fo;
			if(!fo.IsFileExist(strSRWFile))
			{
				MessageBox(_T("Please Install Samsung Raw Converter"), _T("Assist"), MB_OK | MB_ICONSTOP);			
				return;
			}
			ShellExecute(NULL ,_T("open"),strSRWFile, strFileName , m_strImagePath, SW_SHOWNORMAL);		

			reg.Close();
		}
		else
		{
			//------------------------------------------------
			//-- Get Open 3rd vendor Program
			//------------------------------------------------
			CRegistry reg;
			CString strProgram;
			LPCTSTR lpszSection;			

			lpszSection = REG_RS_OPENPROGRAM;
			if(reg.VerifyKey(HKEY_LOCAL_MACHINE, REG_RS_ROOT))
			{
				if(reg.Open(HKEY_LOCAL_MACHINE, lpszSection))
				{
					reg.Read (REG_RS_UPDATE, strProgram);			
					reg.Close();			
				}
				//-- 2011-07-07 hongsu.jung
				//-- Open Program Exist
				if(strProgram.GetLength())
				{
					ShellExecute(NULL ,_T("open"),strProgram,strFileName,NULL,SW_SHOWNORMAL);				
				}

				//-- 2011-06-01 hongsu.jung
				//-- Open Inhouse Window
				else
				{			
					//-- 2013-03-08 hongsu.jung
					//-- Open With Exploere			
					ShellExecute(NULL ,_T("open"),strPath,NULL,NULL,SW_SHOWNORMAL);
					/*			
					m_pPictureViewerDlg->SetImagePath(strPath);
					if(!m_pPictureViewerDlg->SetImageName(strFileName))
					{
						reg.Close();
						return;
					}
					
					m_pPictureViewerDlg->Invalidate(false);
					m_pPictureViewerDlg->ShowWindow(SW_SHOW);
					//-- Set Focus Windows
					m_pPictureViewerDlg->SendMessage(WM_SETFOCUS);
					*/
				}

				reg.Close();
			}
			//-- 2011-06-01 hongsu.jung
			//-- Open Inhouse Window
			else
			{	
				m_pPictureViewerDlg->SetImagePath(strPath);
				if(!m_pPictureViewerDlg->SetImageName(strFileName))
				{
					reg.Close();		
					return;					
				}
				//-- Check Windows 
				//-- Set Focus Windows
				if(!bOpened)
					m_pPictureViewerDlg->ShowWindow(SW_SHOW);
				m_pPictureViewerDlg->Invalidate(false);						
				m_pPictureViewerDlg->SendMessage(WM_SETFOCUS);

				reg.Close();	
				return;
			}
		}
	}
	
}
#endif

//------------------------------------------------------------------------------ 
//! @brief		DeleteSeletedFile
//! @date		2011-06-01
//! @author		Lee JungTaek
//------------------------------------------------------------------------------ 
void CThumbNailList::DeleteSeletedFile(int nIndex)
{
	CString	strPath;
	CString	strFileName;
	int nFile = m_arImageNames.GetSize() - nIndex -1;
	strFileName = m_arImageNames.GetAt(nFile);
	//-- Get Image Path
	if(m_strImagePath[m_strImagePath.GetLength() - 1] == TCHAR('\\'))		strPath.Format( _T("%s%s"), m_strImagePath, strFileName);
	else																					strPath.Format( _T("%s\\%s"), m_strImagePath, strFileName);
	//-- Delete Image From Folder
	DeleteFile(strPath);
}

//------------------------------------------------------------------------------ 
//! @brief		IsRaw
//! @date		2011-07-07
//! @author	hongsu.jung
//! Check Raw File or not
//------------------------------------------------------------------------------ 
BOOL CThumbNailList::IsRaw(CString strPath)
{
	CString strExt = strPath.Right(3);
	if ((strExt.CompareNoCase( TEXT("srw") ) == 0))
		return TRUE;
	return FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief		IsOpened
//! @date		2011-07-19
//! @author	hongsu.jung
//! Check Raw File or not
//------------------------------------------------------------------------------ 
BOOL CThumbNailList::IsOpened(CString str)
{
//CMiLRe 20141127 픽쳐뷰 -> windows 사진뷰어 실행
#ifdef PICTUREVIEW
	if(m_strOpenedFile == str)
	{
		if(m_pPictureViewerDlg->IsWindowVisible())
			return TRUE;
	}
		
	m_strOpenedFile = str;
#endif
	return FALSE;
}

//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
int CThumbNailList::GetThumnailFileCount(CString strFilePath)
{
	//CMiLRe 20141124 섬네일 연사일 경우 여러자 못가지고 오는 버그 수정
	if(!GetDirInFileCount())
		return -1;
	return m_nImgCount;
}

//CMiLRe 20141124 섬네일 연사일 경우 여러자 못가지고 오는 버그 수정
BOOL CThumbNailList::GetDirInFileCount()
{
	int nFileCount = 0;
	int nRet = FILE_NONE;
	CString	strExt;
	CString	strName;
	CString	strPattern;
	BOOL	bRC = TRUE;
	HANDLE					hFind = NULL;
	WIN32_FIND_DATA		FindFileData;

	if(!GetImagePath())
	{
		return FALSE;
	}

	//dh0.seo 2014-11-11
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	if(pMainWnd)
	{
		m_strCameraID = pMainWnd->m_SdiMultiMgr.GetDevUniqueID(m_nIndex);
		m_nCheckState = pMainWnd->m_nAutoViewCheck;
		m_nCheckSelect = pMainWnd->m_nAutoViewSelect;
		m_bCaptureCheck = pMainWnd->m_bCaptureCheck;
	}
//	CString strCamera = _T("Camera");
//	CString strFolder;
	CString strFilePath = m_strImagePath;
//	int nCameraIndex = m_nIndex + 1;
//	strFolder.Format(_T("%s"), m_strCameraID);//strCamera, nCameraIndex);
	m_strImagePath.Format(_T("%s"), strFilePath);

	if(m_strImagePath[m_strImagePath.GetLength() - 1] == TCHAR('\\') )	
		strPattern.Format(_T("%s*.*"), m_strImagePath);
	else
		strPattern.Format(_T("%s\\*.*"), m_strImagePath);

	hFind = ::FindFirstFile(strPattern, &FindFileData);	// strat search	
	if (hFind == INVALID_HANDLE_VALUE)
	{
		LPVOID  msg;
		::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, 
			NULL, 
			GetLastError(), 
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&msg, 
			0, 
			NULL);		
		::LocalFree(msg);
		nRet = FILE_ERR;
		m_nImgCount = nFileCount;
		return FALSE;
	}

	// filter off the system files and directories
	if (!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)  &&
		!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM)     &&
		!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)     &&
		!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_TEMPORARY))
	{  	  
		// test file extension
		strName = FindFileData.cFileName;
		strExt = strName.Right(3);

		if ( (strExt.CompareNoCase( TEXT("bmp") ) == 0) ||
			(strExt.CompareNoCase( TEXT("jpg") ) == 0) ||
			(strExt.CompareNoCase( TEXT("gif") ) == 0) ||
			(strExt.CompareNoCase( TEXT("tif") ) == 0) ||
			(strExt.CompareNoCase( TEXT("srw") ) == 0) ||
			(strExt.CompareNoCase( TEXT("png") ) == 0) )
		{
			nFileCount++;
		}		
	}
	while (bRC)
	{
		bRC = ::FindNextFile(hFind, &FindFileData);
		if (bRC)
		{
			// filter off the system files and directories
			if (!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)  &&
				!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM)     &&
				!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)     &&
				!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_TEMPORARY))
			{
				// test file extension
				strName = FindFileData.cFileName;
				strExt = strName.Right(3);

				if ( (strExt.CompareNoCase( TEXT("bmp") ) == 0) ||
					(strExt.CompareNoCase( TEXT("jpg") ) == 0) ||
					(strExt.CompareNoCase( TEXT("gif") ) == 0) ||
					(strExt.CompareNoCase( TEXT("tif") ) == 0) ||
					(strExt.CompareNoCase( TEXT("srw") ) == 0) ||
					(strExt.CompareNoCase( TEXT("png") ) == 0) )
				{
					// save the image file name
					nFileCount++;
				}				
			}
		}  
		else
		{
			DWORD err = ::GetLastError();
			if (err !=  ERROR_NO_MORE_FILES)
			{
				LPVOID msg;
				::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, 
					NULL, err, 
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					(LPTSTR)&msg, 0, NULL);
				::LocalFree(msg);
				::FindClose(hFind);
				nRet = FILE_ERR;
				m_nImgCount = nFileCount;
				return FALSE;
			}
		}
	} // end of while loop
	::FindClose(hFind);
	m_nImgCount = nFileCount;
	//CMiLRe 20141124 섬네일 연사일 경우 여러자 못가지고 오는 버그 수정
	if(m_ImageListThumb.GetImageCount() != m_nImgCount)
		return FALSE;	
	return TRUE;	
}

void CThumbNailList::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = GetSafeHwnd();
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	::_TrackMouseEvent(&tme);
}

LRESULT CThumbNailList::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	m_bScrollBasePress = FALSE;
	m_bScrollState = FALSE;
	UpdateFrames();
	Invalidate(FALSE);

	CPoint point;
	GetCursorPos(&point);
	ScreenToClient(&point);

	CRect rect;
	GetClientRect(&rect);

/*	if(point.y < rect.top)
		return FALSE;	
	else if(point.y  + 12 > rect.bottom)
		return FALSE;	
	else if(point.x + 12 > rect.right)
		return FALSE;	
	else if(point.x - 12 < rect.left)
		return FALSE;	*/

	return 0L;
}