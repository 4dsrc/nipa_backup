/////////////////////////////////////////////////////////////////////////////
//
//  SmartPanelDlg.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "BkDialogST.h"
#include "SmartPanelChildDlg.h"
#include "MultiApplyDlg.h"
#include "RSComboList.h"
#include "SmartPanelBarometerChildDlg.h"
#include "SmartPanelSliderChildDlg.h"
#include "SmartPanelModeSelectChildDlg.h"
#include "NXRemoteStudioFunc.h"

// CSmartPanelDlg dialog
class CSmartPanelDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CSmartPanelDlg)

public:
	CSmartPanelDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSmartPanelDlg();

	// Dialog Data
	enum { IDD = IDD_DLG_SMARTPANEL};

public:
	//-- Update Frame
	BOOL UpdateDialog(BOOL bModeChange = FALSE,BOOL bFirst = TRUE);
	
	//---------------------------------------------------------------
	//-- Device Property
	//---------------------------------------------------------------
	//-- Get Property Status
	int GetPropValueStatus(int nDepth, int nPropIndex, int nPropValue = 0);
	//-- Send Property Value
	void SetPropertyValue(int nPropIndex, int nPropValue);
			
	//---------------------------------------------------------------
	//-- Control MultiApply
	//---------------------------------------------------------------
	void MutliapplyBtnClick();
	void SaveBankBtnClick();
	void LoadBankBtnClick();
	void HelpBtnClick();

	//---------------------------------------------------------------
	//-- Target Info
	//---------------------------------------------------------------
	void SetMainCamera(int nMainCamera) {m_nMainCamera = nMainCamera;}
	int GetMainCamera() {return m_nMainCamera;}

	void SetTargetModel(CString strModel) {m_strModel = strModel;}
	CString GetTargetModel() {return m_strModel;}

	CString GetTargetMode() {return m_strMode;}
	void SetTargetMode(int nMode, BOOL bCreate = FALSE);
	
	//-- 2011-6-10 Lee JungTaek
	//-- New Function
	void SetItem(int nPTPCode, CString strValue);
	void SetItem(int nPTPCode, int nPropValue);

	//-- 2011-7-19 Lee JungTaek
	int GetMode()		{	return m_nMode;	}

	//-- 2011-7-28 Lee JungTaek
	//-- Set Detail Values
	void SetDetailProperty(int nPropIndex, int nValue);

	//-- 2011-8-5 Lee JungTaek
	//-- Check Capture Option
	void SetCapturing(BOOL bCapturing) {m_bCapturing = bCapturing;}
	BOOL IsCapturing() {return m_bCapturing;}

	//-- 2011-8-9 Lee JungTaek
	void CloseMultiApplyDlg() {if(m_pMultiApplyDlg)	delete m_pMultiApplyDlg; m_pMultiApplyDlg = NULL;}

	CString m_strModel;

	void SetPropertyDescription(int nPropIndex);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnPaint();	
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	DECLARE_MESSAGE_MAP()

private:
	//---------------------------------------------------------------
	//-- Init Smart Panel
	//---------------------------------------------------------------
	//-- Create Frame
	void CreateFrame();
	//-- Init SmartPanel
	void LoadCameraInfoFile();
	//-- Create Contorl
	void CreateControlByType();
	//-- Get Device Property
	void GetDevicePropDesc(BOOL bModeChange);
	//---------------------------------------------------------------
	//-- Control Array Info
	//---------------------------------------------------------------
	int AddCtrlInfo(ControlTypeInfo* controlTypeInfo);
	BOOL RemoveCtrlInfoAll();
	int GetCtrlInfoCount();
	ControlTypeInfo* GetCtrlInfo(int nIndex);

	//---------------------------------------------------------------
	//-- Create Control
	//---------------------------------------------------------------
	void CreateBtnCtrl(CRSPropertyLayout* pPropertyLayout, int nPropIndex);
	void CreateBarometerCtrl(CRSPropertyLayout* pPropertyLayout, int nPropIndex);
	void CreateSliderCtrl(CRSPropertyLayout* pPropertyLayout, int nPropIndex);
	void CreateModeSelectCtrl(CRSPropertyLayout* pPropertyLayout, int nPropIndex);
	void CreateMagicModeCtrl(CRSPropertyLayout* pPropertyLayout, int nPropIndex);
	void ShowControlAll(BOOL bShow = TRUE);
	void ClearPreInfo();

	//---------------------------------------------------------------
	//-- SmartPanel 2Depth
	//---------------------------------------------------------------
	//-- Create 2Depth Panel
	void CreateSmartPanelChild(int nCtrlID,BOOL bShow = TRUE);
	void CreateBtnChildDlg(int nCtrlID, ControlTypeInfo* controlTypeInfo, BOOL bShow);
	void CreateBarometerChildDlg(int nCtrlID, ControlTypeInfo* controlTypeInfo, BOOL bShow);
	void CreateSliderChildDlg(int nCtrlID, ControlTypeInfo* controlTypeInfo, BOOL bShow);
	void CreateModeSelectChildDlg(int nCtrlID, ControlTypeInfo* controlTypeInfo, BOOL bShow);
	
	//-- Chang Pre Selected Property Image
	void ChangePrePropertyImage(int nCtrlID);
	//-- Second Control
	void CreateControlByType(int nCurPropIndex);
	void SetChildWindowPos(int nCnt, CRect rect);
	//-- Close Smart Panel
	void CloseSmartPanelChild();
	int ChangePanelIndexToPTPCode(int nPropIndex);
	//-- Set Property String
private:
	int m_nMainCamera;
	CRSArray m_arrCtrlInfo;

	BOOL m_bConnect;
	BOOL m_bApplyMode;

	//-- Manage Pre Selected Property
	int m_nPrePropIndex;
	int m_nSelectPTPCode;
	CString m_strOpenPropertyName;

	//-- Target Info
	CString m_strMode;
	int		m_nMode;

	//-- Layout Info
	CRSLayoutInfo m_rsLayoutInfo;
	//-- 2011-7-14 Lee JungTaek
	CString m_strPropDesc;
	CRect m_rectPropDesc;
	//-- 2011-7-14 Lee JungTaek
	//-- LoadBank
	CRSComboList* m_pComboList;

	//-- 2011-7-26 Lee JungTaek
	//-- Check Timer
	BOOL m_bModeChange;

	//-- 2011-8-5 Lee JungTaek
	BOOL m_bCapturing;
	BOOL m_bReloadMultiApply;

	int m_nChangePTPCode;
public:
	CMultiApplyDlg* m_pMultiApplyDlg;
	CSmartPanelChildDlg* m_pSmartPanelChild;
	CSmartPanelEditDlg *m_pSaveBankEdit;

	//-- 2011-9-15 Lee JungTaek
	CSmartPanelBarometerChildDlg* m_pSmartBMChildDlg;
	CSmartPanelSliderChildDlg* m_pSmartSDChildDlg;
	CSmartPanelModeSelectChildDlg* m_pSmartMSChildDlg;

	//-- 2011-5-20 Lee JungTaek
	//-- Bit button
	CButtonST m_btnLoadBank;
	CButtonST m_btnSaveBank;
	CButtonST m_btnMultiApply;
	CButtonST m_btnHelp;
	CString GetModel(){return m_strModel;}

	//-- 2014-10-01 joonho.kim
	//-- Enable Control
	BOOL m_bEnable;
	void EnableControl(BOOL bEnable) { m_bEnable = bEnable;}

	BOOL m_b3DMode;
	//CMiLRe 20141114 IOS Auto Range가 Value에서 Enum으로 변경
	BOOL Set3DMode(int nPTPCode, int nPropValue);

	void SetApplyMode(BOOL bValue){m_bApplyMode = bValue;}
	BOOL GetApplyMode() {return m_bApplyMode;}
	//CMiLRe 20141118 스마트 패널 선택 -> 좌클릭, 마우스 무브시 테두리표시
	int GetSmartPanelCtrCnt (int nMode);
	
};