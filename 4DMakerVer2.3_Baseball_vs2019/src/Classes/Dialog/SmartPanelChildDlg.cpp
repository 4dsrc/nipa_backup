/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelChildDlg.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SmartPanelChildDlg.h"
#include "afxdialogex.h"
#include "SmartPanelDlg.h"
#include "SdiDefines.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CSmartPanelChildDlg dialog

IMPLEMENT_DYNAMIC(CSmartPanelChildDlg, CDialogEx)

	CSmartPanelChildDlg::CSmartPanelChildDlg(CWnd* pParent /*=NULL*/)
	: CBkDialogST(CSmartPanelChildDlg::IDD, pParent)	
{
	m_bMoveIn = FALSE;
	m_bDetailSetting = FALSE;
	m_pSmartPanelWBDetail = NULL;
	m_pSmartPanelWBColorDetail = NULL;
	m_pSmartPanelPWDetail = NULL;
	//CMiLRe NX1 SmartWizard Detail 추가
	//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
	nPWType = PW_DETAIL_COLOR;
	m_pSmartPanelPWDetail_Color_NX1 = NULL;
	m_pSmartPanelPWDetail_Saturation_NX1 = NULL;
	m_pSmartPanelPWDetail_Sharpness_NX1 = NULL;
	m_pSmartPanelPWDetail_Contrast_NX1 = NULL;
	m_pSmartPanelPWDetail_Hue_NX1 = NULL;
	m_pSmartPanelDriveDetail = NULL;
	m_pSmartPanelFlashDetailDlg = NULL;

	m_nSelectedIndex = 0;
	m_nSelected1st = 0;
	//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
	m_nFocusIndex = -1;
	m_nValueIndex = 0;
}

CSmartPanelChildDlg::~CSmartPanelChildDlg()
{
	CloseSmartPanelChild();
}

void CSmartPanelChildDlg::DoDataExchange(CDataExchange* pDX)
{
	CBkDialogST::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_SPC_DETAIL,m_btnAdjust);
	DDX_Control(pDX, IDC_BTN_SPC_OK,	m_btnOK);
}

BEGIN_MESSAGE_MAP(CSmartPanelChildDlg, CBkDialogST)
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()

//-- Remove All
BOOL CSmartPanelChildDlg::RemoveCtrlInfoAll()
{
	ControlTypeInfo* pExist = NULL;
	int nAll = GetCtrlInfoCount();

	while(nAll--)
	{
		pExist = (ControlTypeInfo*)GetCtrlInfo(nAll);

		if(pExist)
		{
			switch(pExist->nType)
			{
			case SMARTPANEL_CONTROL_BUTTON:
				{
					CRSButton* pRSButton = (CRSButton*)pExist->pRSCtrl;
					delete pRSButton;
					pRSButton = NULL;
				}
				break;				
			}

			delete pExist;
			pExist = NULL;

			m_arrCtrlInfo.RemoveAt(nAll);
		}
		else
			return FALSE;
	}

	//-- CHECK REMOVE ALL
	m_arrCtrlInfo.RemoveAll();
	return TRUE;
}

//-- Return Button Count
int CSmartPanelChildDlg::GetCtrlInfoCount()
{
	return (int)m_arrCtrlInfo.GetSize();
}

//-- Return Specific Button Control
ControlTypeInfo* CSmartPanelChildDlg::GetCtrlInfo(int nIndex)
{
	if(nIndex < GetCtrlInfoCount())
		return (ControlTypeInfo*)m_arrCtrlInfo.GetAt(nIndex);
	return NULL;
}

//-- Add Each Button Control
int CSmartPanelChildDlg::AddCtrlInfo(ControlTypeInfo* controlTypeInfo)
{
	return (int)m_arrCtrlInfo.Add((CObject*)controlTypeInfo);
}

BOOL CSmartPanelChildDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame()
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Create Panel
//------------------------------------------------------------------------------
void CSmartPanelChildDlg::CreateFrame(CString strPath)
{
	CFileFind	find;
	BOOL bNext = find.FindFile((LPCTSTR)strPath);
	//NX1 dh0.seo 2014-08-21
	m_strModel = m_rsLayoutInfo->GetTargetModel();
	
	//-- Exist
  	if(bNext)
 		SetBitmap(strPath, RGB(255,255,255));

	//-- 2011-7-28 Lee JungTaek
	//-- Add Detail Btn
	if(m_bDetailSetting)
	{
		m_btnAdjust.SetBitmaps(RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_adjust_focus.png")), RGB(255,255,255), RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_adjust.png")), RGB(255,255,255));
		m_btnOK.SetBitmaps(RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_focus_user.png")), RGB(255,255,255), RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_normal_user.png")), RGB(255,255,255));
		m_btnOK.SetUserText(_T("OK"));

		switch(m_nPropType)
		{
		case PROP_DETAIL_TYPE_PHOTO_SIZE :
			m_btnOK.MoveWindow(253, 122, 55, 25);
			break;
		case PROP_DETAIL_TYPE_OIS:
		case PROP_DETAIL_TYPE_METERING :
			m_btnOK.MoveWindow(130, 47, 55, 25);
			break;
		case PROP_DETAIL_TYPE_AF_MODE :
		case PROP_DETAIL_TYPE_AF_AREA :
			m_btnOK.MoveWindow(190, 47, 55, 25);
			break;
		case PROP_DETAIL_TYPE_WB :	
			{
				//NX1 dh0.seo 2014-08-21
				if(!m_strModel.Compare(_T("NX1")))
				{
					m_btnAdjust.MoveWindow(8, 122, 55, 25);
					m_btnOK.MoveWindow(253, 122, 55, 25);
				}
				else
				{
					m_btnAdjust.MoveWindow(8, 86, 55, 25);
					m_btnOK.MoveWindow(253, 86, 55, 25);
				}				
			}
			break;
		case PROP_DETAIL_TYPE_DRIVE : // 위치 이동
			m_btnAdjust.MoveWindow(8, 122, 55, 25);
			m_btnOK.MoveWindow(253, 122, 55, 25);
			break;
		case PROP_DETAIL_TYPE_PW :
			{
				m_btnAdjust.MoveWindow(8, 124, 55, 25);
				m_btnOK.MoveWindow(253, 124, 55, 25);
			}
			break;
		//dh0.seo Flash Detail 추가 2014-11-24
		case PROP_DETAIL_TYPE_SF:
		case PROP_DETAIL_TYPE_QUALITY :
		case PROP_DETAIL_TYPE_FLASH :
			m_btnOK.MoveWindow(253, 82, 55, 25);
			break;
		default:				break;
		}

		m_btnAdjust.ShowWindow(SW_SHOW);
		m_btnOK.ShowWindow(SW_SHOW);
	}
	else
	{
		m_btnAdjust.ShowWindow(SW_HIDE);
		m_btnOK.ShowWindow(SW_HIDE);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		CreateControlByType()
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Create Control By Type
//------------------------------------------------------------------------------
void CSmartPanelChildDlg::CreateControlByType(int nCurProperty, CRSLayoutInfo& arrRSLayoutInfo)
{
	m_rsLayoutInfo = &arrRSLayoutInfo;

	CRSLayoutSubInfo* pRSLayoutSubInfo = (CRSLayoutSubInfo*)arrRSLayoutInfo.GetLayoutSubClassInfo(nCurProperty);

	//-- 2011-7-28 Lee JungTaek
	//-- Check Property Name
	CRSPropertyLayout* pPropertyLayout = arrRSLayoutInfo.GetLayoutInfo(nCurProperty);

	if(pPropertyLayout)
	{
		CString strPropName = _T("");
		strPropName = pPropertyLayout->m_strPropertyName;

		if(!strPropName.CompareNoCase(_T("WHITE BALANCE")))
		{
			m_bDetailSetting = TRUE;
			m_nPropType = PROP_DETAIL_TYPE_WB;
		}
		else if(!strPropName.CompareNoCase(_T("PICTURE WIZARD")))
		{
			m_bDetailSetting = TRUE;
			m_nPropType = PROP_DETAIL_TYPE_PW;
		}
		else if(!strPropName.CompareNoCase(_T("DRIVE")))
		{
			m_bDetailSetting = TRUE;
			m_nPropType = PROP_DETAIL_TYPE_DRIVE;
		}
		else if(!strPropName.CompareNoCase(_T("METERING")))
		{
			m_bDetailSetting = TRUE;
			m_nPropType = PROP_DETAIL_TYPE_METERING;
		}
		else if(!strPropName.CompareNoCase(_T("AF MODE")))
		{
			m_bDetailSetting = TRUE;
			m_nPropType = PROP_DETAIL_TYPE_AF_MODE;
		}
		else if(!strPropName.CompareNoCase(_T("AF Area")))
		{
			m_bDetailSetting = TRUE;
			m_nPropType = PROP_DETAIL_TYPE_AF_AREA;
		}
		else if(!strPropName.CompareNoCase(_T("PHOTO SIZE")))
		{
			m_bDetailSetting = TRUE;
			m_nPropType = PROP_DETAIL_TYPE_PHOTO_SIZE;
		}
		else if(!strPropName.CompareNoCase(_T("QUALITY")))
		{
			m_bDetailSetting = TRUE;
			m_nPropType = PROP_DETAIL_TYPE_QUALITY;
		}
		else if(!strPropName.CompareNoCase(_T("SMART FILTER")))
		{
			m_bDetailSetting = TRUE;
			m_nPropType = PROP_DETAIL_TYPE_SF;
		}
		//dh0.seo Flash Detail 추가 2014-11-24
		else if(!strPropName.CompareNoCase(_T("FLASH")))
		{
			m_bDetailSetting = TRUE;
			m_nPropType = PROP_DETAIL_TYPE_FLASH;
		}
		else if(!strPropName.CompareNoCase(_T("OIS")))
		{
			m_bDetailSetting = TRUE;
			m_nPropType = PROP_DETAIL_TYPE_OIS;
		}
		else
		{
			m_bDetailSetting = TRUE;
			m_nPropType = PROP_DETAIL_TYPE_ETC;
		}
	}

	//-- 2011-7-19 Lee JungTaek
	//-- Code sonar
	if(pRSLayoutSubInfo)
	{
		CString strPropName = _T("");
		strPropName = pPropertyLayout->m_strPropertyName;
		if(!strPropName.CompareNoCase(_T("DRIVE")))
			SetChildWindowPos(11); //갯수 고정 depth 추가 시 11
		else
			SetChildWindowPos(pRSLayoutSubInfo->GetPopertyValueCount());

		//-- If Property Exist
		if(pRSLayoutSubInfo->GetPopertyValueCount())
		{
			m_nValueCnt = pRSLayoutSubInfo->GetPopertyValueCount();

			for(int i = 0; i < pRSLayoutSubInfo->GetPopertyValueCount(); i++)
			{
				//-- Get Specific Property Value
				CRSPropertyValue* rsPropertyValue = (CRSPropertyValue*)pRSLayoutSubInfo->GetPropertyValue(i);

				//-- Code Sonar
				if(rsPropertyValue)
				{
					switch(rsPropertyValue->m_nType)
					{
					case SMARTPANEL_CONTROL_BUTTON:
						CreateBtnCtrl(rsPropertyValue, i);				
						break;
					}
				}				
			}
		}	
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		CreateBtnCtrl()
//! @date		2011-05-23
//! @author		Lee JungTaek
//! @note	 	Create and Set Button Control
//------------------------------------------------------------------------------
void CSmartPanelChildDlg::CreateBtnCtrl(CRSPropertyValue* rsPropertyValue, int nPropValue)
{
	//-- Create Button
	CRSButton* ctrlButton = new CRSButton(BTN_BK_TYPE_2ND);
	if(ctrlButton)
		ctrlButton->Create(NULL, WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, rsPropertyValue->m_rectProperty, this,ID_BTN_SMARTPANEL_CHILD + nPropValue);
	else
		return;

	//-- 2011-7-28 Lee JungTaek
	if(!ctrlButton)
		return;

	//-- Set Image
	CString strImagePath;
	strImagePath.Format(_T("%s\\%s\\%s\\"), RSGetRoot(), STR_IMAGE_SMARTPANNEL_ICON_PATH, rsPropertyValue->m_strPropertyName);

	//-- 2011-6-15 Lee JungTaek
	//-- Get Each Property Status
	int nStatus = GetPropValueStatus(m_n1DepthCtrlID - ID_BTN_SMARTPANEL, nPropValue);
	
	switch(nStatus)
	{
	case PROPERTY_STATUS_DISABLE:
		{
			int nBtnPosType = GetButtonPos(nPropValue);
			ctrlButton->SetBtnStatus(BTN_BK_2ND_STATUS_NORMAL, nBtnPosType);
			strImagePath.AppendFormat(_T("%s"), rsPropertyValue->m_strImagePathD);
			ctrlButton->EnableWindow(FALSE);
		}
		break;
	case PROPERTY_STATUS_NORMAL:
		{
			int nBtnPosType = GetButtonPos(nPropValue);
			ctrlButton->SetBtnStatus(BTN_BK_2ND_STATUS_NORMAL, nBtnPosType);
			strImagePath.AppendFormat(_T("%s"), rsPropertyValue->m_strImagePathN); 
			ctrlButton->EnableWindow();
		}		
		break;
	case PROPERTY_STATUS_SELECTED:
		{
			int nBtnPosType = GetButtonPos(nPropValue);
			ctrlButton->SetBtnStatus(BTN_BK_2ND_STATUS_SELECT, nBtnPosType);
			strImagePath.AppendFormat(_T("%s"), rsPropertyValue->m_strImagePathN); 
			m_nSelectedIndex = nPropValue;
			m_nSelected1st = nPropValue;			
			ctrlButton->EnableWindow();
		}
		break;
	}
	
	//-- Button Create
	ctrlButton->SetBitmaps(strImagePath);

	//-- Set Property
	ctrlButton->m_strPropertyName = rsPropertyValue->m_strPropertyName;

	//-- Add Ctrl Info
	ControlTypeInfo* controlTypeInfo = new ControlTypeInfo;
	controlTypeInfo->nType = SMARTPANEL_CONTROL_BUTTON;
	controlTypeInfo->pRSCtrl = (CObject*)ctrlButton;

	AddCtrlInfo(controlTypeInfo);
}

//------------------------------------------------------------------------------ 
//! @brief		GetDevicePropDesc()
//! @date		2011-05-25
//! @author		Lee JungTaek
//! @note	 	Set SmartPannel Location
//------------------------------------------------------------------------------
#define SMARTPANEL_RAW_INDEX 4
#define SMARTPANEL_2DEPTH_RAW_INDEX 5

void CSmartPanelChildDlg::SetChildWindowPos(int nCnt)
{
	//-- Smart Panel Rect
	CString strType;
	int nPopertyCnt;
	int nRaw;
	int nCul;
		
	//-- 2011-05-31 hongsu.jung
	CString strPath = RSGetRoot(_T("img\\02.Smart Panel\\2ndpopup_bg\\"));
	BOOL bSetPath = FALSE;

	CRect rect;
	GetParent()->GetWindowRect(&rect);

	//-- 1. Check Child Type
	if(m_bDetailSetting)
		strType = _T("2ndpopup_detail");
	else
		strType = _T("2ndpopup");

	//-- 2. Check Property Value Count
	if(nCnt >= SMARTPANEL_2DEPTH_RAW_INDEX)
		nPopertyCnt = SMARTPANEL_2DEPTH_RAW_INDEX;
	else
	{
		nPopertyCnt = nCnt;
		bSetPath = TRUE;
	}

	//-- 3. Check Raw
	if(nCnt%SMARTPANEL_2DEPTH_RAW_INDEX == 0)
		nRaw = nCnt / SMARTPANEL_2DEPTH_RAW_INDEX;
	else
		nRaw = nCnt / SMARTPANEL_2DEPTH_RAW_INDEX + 1;

	//-- 4. Check Cul
	int nPos = m_rectParentCtrl.left - (rect.left + 14);

	BOOL bCheckMode = RSCheckChilde();
	
	nCul = nPos / (m_rectParentCtrl.Width() + SMARTPANEL_RAW_INDEX - 1) + 1;

	if(bSetPath)
		strPath.AppendFormat(_T("%s_%d_%d.png"), strType, nCnt, nCul);
	else
	{
		if(bCheckMode == TRUE)
			strPath.AppendFormat(_T("%s_%d_%d_%d.png"), strType, nPopertyCnt, nRaw, 4);
		else
			strPath.AppendFormat(_T("%s_%d_%d_%d.png"), strType, nPopertyCnt, nRaw, nCul);
	}
		

	//-- Create Frame
	CreateFrame(strPath);

	//-- Smart Panel Child Rect
	CRect rectChild;
	GetWindowRect(&rectChild);

	Image img(strPath);
	//-- Move Window
	//-- 2013-03-09 hongsu.jung
	//-- Debugging Popup Window on Smart Panel
	TRACE(_T("Open Child Path : %s W:%d H:%d\n"),strPath,img.GetWidth(),img.GetHeight());
	TRACE(_T("Child Rect X:%d, Y:%d, W:%d H:%d\n"), rectChild.left, rectChild.top, rectChild.Width(), rectChild.Height());
	
	rectChild.top = rectChild.bottom - img.GetHeight();
	TRACE(_T("Child Rect X:%d, Y:%d, W:%d H:%d\n"), rectChild.left, rectChild.top, rectChild.Width(), rectChild.Height());
	TRACE(_T("Parent Rect X:%d, Y:%d, W:%d H:%d\n"), m_rectParentCtrl.left, m_rectParentCtrl.top, m_rectParentCtrl.Width(), m_rectParentCtrl.Height());
	
	//-- 2011-9-7 Lee JungTaek
	if(nCnt < SMARTPANEL_2DEPTH_RAW_INDEX)
	{
		switch(nCnt)
		{
		case 1:			CheckWindowRect(m_rectParentCtrl.left		, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth(),img.GetHeight());						break;
		case 2:					
			switch(nCul)
			{
			case 1:		CheckWindowRect(m_rectParentCtrl.left					, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth() + 100,	img.GetHeight());	break;
 			case 2:		CheckWindowRect(m_rectParentCtrl.right - img.GetWidth()	, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth() + 100,	img.GetHeight());	break;
 			case 3:		CheckWindowRect(m_rectParentCtrl.left					, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth() + 100,	img.GetHeight());	break;
 			case 4:		CheckWindowRect(m_rectParentCtrl.right - img.GetWidth()	, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth() + 100,	img.GetHeight());	break;
			}
			break;
		case 3:		
			switch(nCul)
			{
			case 1:		CheckWindowRect(m_rectParentCtrl.left							, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth(), img.GetHeight());	break;
			case 2:		CheckWindowRect(m_rectParentCtrl.left - img.GetWidth() / 2 + 33	, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth(), img.GetHeight()); 	break;
			case 3:		CheckWindowRect(m_rectParentCtrl.left - img.GetWidth() / 2 + 33	, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth(), img.GetHeight()); 	break;
			case 4:		CheckWindowRect(m_rectParentCtrl.right - img.GetWidth()			, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth(), img.GetHeight());	break;
			}
			break;
		case 4:		
			switch(nCul)
			{
			case 1:		CheckWindowRect(m_rectParentCtrl.left									, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth(), img.GetHeight());	break;
			case 2:		CheckWindowRect(m_rectParentCtrl.left - m_rectParentCtrl.Width() + 3	, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth(), img.GetHeight()); 	break;
			case 3:		CheckWindowRect(m_rectParentCtrl.left - m_rectParentCtrl.Width() * 2 + 3, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth(), img.GetHeight()); 	break;
			case 4:		CheckWindowRect(m_rectParentCtrl.right - img.GetWidth()					, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth(), img.GetHeight());	break;
			}
			break;
		case 5:			CheckWindowRect(m_rectParentCtrl.left		, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top), img.GetWidth(),img.GetHeight());						break;
		}
	}		
	else if(nCnt >= SMARTPANEL_2DEPTH_RAW_INDEX)
						CheckWindowRect(rect.left - 4	, m_rectParentCtrl.bottom - 30 - (rectChild.bottom - rectChild.top),img.GetWidth(),img.GetHeight());
}

//------------------------------------------------------------------------------ 
//! @brief		GetButtonPos
//! @date		2011-8-30
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CSmartPanelChildDlg::GetButtonPos(int nPropValue)
{
	//-- Under 5 Values
	if(m_nValueCnt <= SMARTPANEL_2DEPTH_RAW_INDEX)
	{
		if(nPropValue == 0)							return BTN_BK_2ND_POS_LEFT;
		else if(nPropValue + 1 == m_nValueCnt)		return BTN_BK_2ND_POS_RIGHT;
		else										return BTN_BK_2ND_POS_NORMAL;	
	}
	else
	{
		//-- Check TopLeft, TopRight
		switch(nPropValue)
		{
		case 0 :	return BTN_BK_2ND_POS_TOP_LEFT;	
		case 4 :	return BTN_BK_2ND_POS_TOP_RIGHT;
		default:
			{
				//-- Use Check BottomLeft, BottomRight?
				if(m_bDetailSetting)	return BTN_BK_2ND_POS_NORMAL;
				else
				{
					int nRaw;
					if(m_nValueCnt%SMARTPANEL_2DEPTH_RAW_INDEX == 0)
						nRaw = m_nValueCnt / SMARTPANEL_2DEPTH_RAW_INDEX;
					else
						nRaw = m_nValueCnt / SMARTPANEL_2DEPTH_RAW_INDEX + 1;

					//-- Check BottomLeft
					if(nPropValue + 1 == (nRaw-1) * SMARTPANEL_2DEPTH_RAW_INDEX + 1)					
						return BTN_BK_2ND_POS_BOTTOM_LEFT;

					//-- Check BottomRight
					if((nPropValue + 1)%SMARTPANEL_2DEPTH_RAW_INDEX == 0 && (nPropValue + 1) == m_nValueCnt)
						return BTN_BK_2ND_POS_BOTTOM_RIGHT;

					return BTN_BK_2ND_POS_NORMAL;
				}
			}
			break;
		}	
	}

	return BTN_BK_2ND_POS_NULL;
}

//------------------------------------------------------------------------------ 
//! @brief		GetCurrentStatus()
//! @date		2011-06-15
//! @author		Lee JungTaek
//! @note	 	Get Device All Property
//------------------------------------------------------------------------------
int CSmartPanelChildDlg::GetPropValueStatus(int nPropIndex, int nPropValue)
{
	int nStatus = ((CSmartPanelDlg*)GetParent())->GetPropValueStatus(SMARTPANEL_DEPTH_TYPE_2ND, nPropIndex, nPropValue);
	return nStatus;
}

//------------------------------------------------------------------------------ 
//! @brief		SetSelectedPropValue()
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Get Device All Property
//------------------------------------------------------------------------------
void CSmartPanelChildDlg::SetSelectedPropValue(int nValueIndex)
{
	CString strImagePath = _T("");

	CRSLayoutSubInfo* pRSLayoutSubInfo = m_rsLayoutInfo->GetLayoutSubClassInfo(m_n1DepthCtrlID - ID_BTN_SMARTPANEL);

	if(pRSLayoutSubInfo)
	{
		//-- If Property Exist
		if(pRSLayoutSubInfo->GetPopertyValueCount())
		{
			//-- 2011-5-30 Lee JungTaek
			//-- Send Selected Data
			((CSmartPanelDlg*)GetParent())->SetPropertyValue(m_n1DepthCtrlID - ID_BTN_SMARTPANEL, nValueIndex);
		}
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		GetDevicePropDesc()
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	CSmartPanelChildDlg message handlers
//------------------------------------------------------------------------------
BOOL CSmartPanelChildDlg::PreTranslateMessage(MSG* pMsg) 
{
	//-- 2011-8-12 Lee JungTaek
	//-- Always Focus
// 	if(!m_pSmartPanelWBDetail && !m_pSmartPanelWBColorDetail && !m_pSmartPanelPWDetail && !m_pSmartPanelDriveDetail)
// 		this->SetFocus();

	if(pMsg->message == WM_LBUTTONDOWN)
	{
		//-- Get Control ID
		int nCtrlID = 0;
		CString strCtrlID = _T("");

		nCtrlID = ::GetDlgCtrlID(pMsg->hwnd);
		strCtrlID.Format(_T("%d"), nCtrlID);

		switch(m_nPropType)
		{
		case PROP_DETAIL_TYPE_FLASH:
		case PROP_DETAIL_TYPE_METERING:
		case PROP_DETAIL_TYPE_AF_MODE:
		case PROP_DETAIL_TYPE_AF_AREA:
		case PROP_DETAIL_TYPE_PHOTO_SIZE:
		case PROP_DETAIL_TYPE_QUALITY:
		case PROP_DETAIL_TYPE_SF:
		case PROP_DETAIL_TYPE_WB:			
		case PROP_DETAIL_TYPE_PW:	
		case PROP_DETAIL_TYPE_DRIVE:
		case PROP_DETAIL_TYPE_OIS:
//		case PROP_DETAIL_TYPE_FLASH://dh0.seo Flash Detail 추가 2014-11-24
			{
				if(nCtrlID >= ID_BTN_SMARTPANEL_CHILD && nCtrlID < ID_BTN_SMARTPANEL_CHILD + m_nValueCnt)
				{
					//-- 2011-8-12 Lee JungTaek
					//-- Change Pre Selecte Ctrl
					int nPreSelectedIndex;
					nPreSelectedIndex = m_nSelectedIndex;
					ChangePreSelectedItemStatus(m_nSelectedIndex);
					//-- Set Current Selected Ctrl
					m_nValueIndex = nCtrlID - ID_BTN_SMARTPANEL_CHILD;
					ChangeSelectedItemStatus(m_nValueIndex);
					//-- Set Value
								
				}
				else
				{
					switch(nCtrlID)
					{
					case IDC_BTN_SPC_DETAIL:	
						if (m_nSelectedIndex == 0 && m_nPropType == PROP_DETAIL_TYPE_PW) //dh0.seo 2014-09-23 추가
							break;
						else
							CreateAdjustDlg(m_nSelectedIndex);	break;
					case IDC_BTN_SPC_OK:
						{
							SetSelectedPropValue(m_nValueIndex);	
							CloseSmartPanelChild();
						}
						break;
					default : CloseSmartPanelChild();				break;
					}					
				}
			}
			break;
		case PROP_DETAIL_TYPE_ETC:
			{
				if(nCtrlID >= ID_BTN_SMARTPANEL_CHILD && nCtrlID < ID_BTN_SMARTPANEL_CHILD + m_nValueCnt)
				{
					int nValueIndex = nCtrlID - ID_BTN_SMARTPANEL_CHILD;
					SetSelectedPropValue(nValueIndex);
					//-- Destroy Window
					CloseSmartPanelChild();
				}
				else
					CloseSmartPanelChild();
			}
			break;
		}		
		return TRUE;
	}
	//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
	else if(pMsg->message == WM_MOUSEMOVE)
	{
		//-- Get Control ID
		int nCtrlID = 0;
		CString strCtrlID = _T("");

		nCtrlID = ::GetDlgCtrlID(pMsg->hwnd);
		strCtrlID.Format(_T("%d"), nCtrlID);
				
		switch(m_nPropType)
		{
		case PROP_DETAIL_TYPE_FLASH:
		case PROP_DETAIL_TYPE_METERING:
		case PROP_DETAIL_TYPE_AF_MODE:
		case PROP_DETAIL_TYPE_AF_AREA:
		case PROP_DETAIL_TYPE_PHOTO_SIZE:
		case PROP_DETAIL_TYPE_QUALITY:
		case PROP_DETAIL_TYPE_SF:
		case PROP_DETAIL_TYPE_WB:			
		case PROP_DETAIL_TYPE_PW:	
		case PROP_DETAIL_TYPE_DRIVE:
		case PROP_DETAIL_TYPE_OIS:
//		case PROP_DETAIL_TYPE_FLASH://dh0.seo Flash Detail 추가 2014-11-24
			{
				if(nCtrlID >= ID_BTN_SMARTPANEL_CHILD && nCtrlID < ID_BTN_SMARTPANEL_CHILD + m_nValueCnt)
				{

					int nPreSelectedIndex = m_nFocusIndex;
					if(m_nFocusIndex != m_nSelectedIndex)
						ChangePreSelectedItemStatus(nPreSelectedIndex);

					//-- Set Current Selected Ctrl
					int nValueIndex = nCtrlID - ID_BTN_SMARTPANEL_CHILD;
					if(nValueIndex == m_nSelectedIndex)
						ChangeSelectedItemStatus(m_nSelectedIndex);
					else
						ChangeFocustemStatus(nValueIndex);					
				}	
				else
				{
					switch(nCtrlID)
					{
					case IDC_BTN_SPC_DETAIL:						
					case IDC_BTN_SPC_OK:		
						return FALSE;
						break;
					}					
				}
			}
			break;
		case PROP_DETAIL_TYPE_ETC:
			{
				if(nCtrlID >= ID_BTN_SMARTPANEL_CHILD && nCtrlID < ID_BTN_SMARTPANEL_CHILD + m_nValueCnt)
				{
					int nPreSelectedIndex = m_nFocusIndex;
					if(m_nFocusIndex != m_nSelectedIndex)
						ChangePreSelectedItemStatus(nPreSelectedIndex);

					//-- Set Current Selected Ctrl
					int nValueIndex = nCtrlID - ID_BTN_SMARTPANEL_CHILD;
					if(nValueIndex == m_nSelectedIndex)
						ChangeSelectedItemStatus(m_nSelectedIndex);
					else
						ChangeFocustemStatus(nValueIndex);			
				}
			}
			break;
		}	
		MouseEvent();
		return TRUE;
	}
	return CBkDialogST::PreTranslateMessage(pMsg);
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-8-17
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
BOOL CSmartPanelChildDlg::CheckFirstLastValue(int nValueIndex, BOOL bWheelUp)
{
	ControlTypeInfo * controlTypeInfo = NULL;
	CRSButton* pRSButton = NULL;

	if(bWheelUp)
	{
		for(int i = nValueIndex; i < m_nValueCnt; i++)
		{
			controlTypeInfo = GetCtrlInfo(i);
			//-- Code Sonar
			if(!controlTypeInfo)
				return FALSE;

			pRSButton = (CRSButton*)controlTypeInfo->pRSCtrl;
			if(pRSButton)
			{
				//-- 2011-8-12 Lee JungTaek
				//-- Check Enable Ctrl
				if(pRSButton->IsWindowEnabled())
					return TRUE;			
			}
		}
	}
	else
	{
		for(int i = nValueIndex; i >= 0; i--)
		{
			controlTypeInfo = GetCtrlInfo(i);
			//-- Code Sonar
			if(!controlTypeInfo)
				return FALSE;

			pRSButton = (CRSButton*)controlTypeInfo->pRSCtrl;
			if(pRSButton)
			{
				//-- 2011-8-12 Lee JungTaek
				//-- Check Enable Ctrl
				if(pRSButton->IsWindowEnabled())
					return TRUE;			
			}
		}
	}	

	return FALSE;	
}

//------------------------------------------------------------------------------ 
//! @brief		ChangePreSelectedItemStatus
//! @date		2011-8-12
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelChildDlg::ChangePreSelectedItemStatus(int nValueIndex)
{
	ControlTypeInfo* controlTypeInfo = GetCtrlInfo(nValueIndex);
	//-- Code Sonar
	if(!controlTypeInfo)
		return;

	CRSButton* pRSButton = (CRSButton*)controlTypeInfo->pRSCtrl;
	if(pRSButton)
	{
		int nBtnPosType = GetButtonPos(nValueIndex);
		pRSButton->SetBtnStatus(BTN_BK_2ND_STATUS_NORMAL, nBtnPosType);
	}
}

//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
void CSmartPanelChildDlg::ChangeFocustemStatus(int nValueIndex, BOOL bChabge)
{
	ControlTypeInfo* controlTypeInfo = GetCtrlInfo(nValueIndex);
	//-- Code Sonar
	if(!controlTypeInfo)
		return;

	CRSButton* pRSButton = (CRSButton*)controlTypeInfo->pRSCtrl;
	if(pRSButton)
	{
		int nBtnPosType = GetButtonPos(nValueIndex);
		if(bChabge)
			pRSButton->SetBtnStatus(BTN_BK_2ND_STATUS_FOCUS, nBtnPosType);
		else
		{
			pRSButton->SetBtnStatus(BTN_BK_2ND_STATUS_SELECT, nBtnPosType);
		}		
		m_nFocusIndex = nValueIndex;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeSelectedItemStatus
//! @date		2011-8-12
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelChildDlg::ChangeSelectedItemStatus(int nValueIndex)
{
	ControlTypeInfo * controlTypeInfo = GetCtrlInfo(nValueIndex);
	//-- Code Sonar
	if(!controlTypeInfo)
		return;

	CRSButton* pRSButton = (CRSButton*)controlTypeInfo->pRSCtrl;
	if(pRSButton)
	{
		int nBtnPosType = GetButtonPos(nValueIndex);
		pRSButton->SetBtnStatus(BTN_BK_2ND_STATUS_SELECT, nBtnPosType);
		m_nSelectedIndex = nValueIndex;	
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeSelectedItemStatus
//! @date		2011-8-12
//! @author		Lee JungTaek
//! @note	 	Recursive
//------------------------------------------------------------------------------ 
BOOL CSmartPanelChildDlg::ChangeSelectedItemStatus(int nValueIndex, BOOL bWheelUp)
{
	ControlTypeInfo * controlTypeInfo = GetCtrlInfo(nValueIndex);
	//-- Code Sonar
	if(!controlTypeInfo)
		return FALSE;

	CRSButton* pRSButton = (CRSButton*)controlTypeInfo->pRSCtrl;
	if(pRSButton)
	{
		//-- 2011-8-12 Lee JungTaek
		//-- Check Enable Ctrl
		if(pRSButton->IsWindowEnabled())
		{
			int nBtnPosType = GetButtonPos(nValueIndex);
			pRSButton->SetBtnStatus(BTN_BK_2ND_STATUS_SELECT, nBtnPosType);
			m_nSelectedIndex = nValueIndex;

			return TRUE;
		}
		else
		{
			if(bWheelUp)			
			{
				//-- Find Next Enable Ctrl
				int nCurValueIndex = nValueIndex;

				if(nCurValueIndex <= m_nValueCnt-1)		
				{
					if(ChangeSelectedItemStatus(++nCurValueIndex, TRUE))
						return TRUE;
				}
				else	return FALSE;
			}
			else
			{
				//-- Find Previous Enable Ctrl
				int nCurValueIndex = nValueIndex;

				if(nCurValueIndex >= 0)
				{
					if(ChangeSelectedItemStatus(--nCurValueIndex, FALSE))
						return TRUE;
				}
				else	return FALSE;
			}
		}
	}

	return FALSE;
}

//-- Destroy Window
//------------------------------------------------------------------------------ 
//! @brief		GetDevicePropDesc()
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Get Device All Property
//------------------------------------------------------------------------------
void CSmartPanelChildDlg::CloseSmartPanelChild()
{
	if(m_pSmartPanelWBDetail)
	{
		m_pSmartPanelWBDetail->DestroyWindow();
		delete m_pSmartPanelWBDetail;
		m_pSmartPanelWBDetail = NULL;
	}

	if(m_pSmartPanelWBColorDetail)
	{
		m_pSmartPanelWBColorDetail->DestroyWindow();
		delete m_pSmartPanelWBColorDetail;
		m_pSmartPanelWBColorDetail = NULL;
	}

	if(m_pSmartPanelPWDetail)
	{
		m_pSmartPanelPWDetail->DestroyWindow();
		delete m_pSmartPanelPWDetail;
		m_pSmartPanelPWDetail = NULL;
	}
	
	//CMiLRe NX1 SmartWizard Detail 추가
	if(m_pSmartPanelPWDetail_Color_NX1)
	{
		m_pSmartPanelPWDetail_Color_NX1->DestroyWindow();
		delete m_pSmartPanelPWDetail_Color_NX1;
		m_pSmartPanelPWDetail_Color_NX1 = NULL;
	}

	//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
	if(m_pSmartPanelPWDetail_Saturation_NX1)
	{
		m_pSmartPanelPWDetail_Saturation_NX1->DestroyWindow();
		delete m_pSmartPanelPWDetail_Saturation_NX1;
		m_pSmartPanelPWDetail_Saturation_NX1 = NULL;
	}

	//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
	if(m_pSmartPanelPWDetail_Sharpness_NX1)
	{
		m_pSmartPanelPWDetail_Sharpness_NX1->DestroyWindow();
		delete m_pSmartPanelPWDetail_Sharpness_NX1;
		m_pSmartPanelPWDetail_Sharpness_NX1 = NULL;
	}

	//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
	if(m_pSmartPanelPWDetail_Contrast_NX1)
	{
		m_pSmartPanelPWDetail_Contrast_NX1->DestroyWindow();
		delete m_pSmartPanelPWDetail_Contrast_NX1;
		m_pSmartPanelPWDetail_Contrast_NX1 = NULL;
	}
	
	//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
	if(m_pSmartPanelPWDetail_Hue_NX1)
	{
		m_pSmartPanelPWDetail_Hue_NX1->DestroyWindow();
		delete m_pSmartPanelPWDetail_Hue_NX1;
		m_pSmartPanelPWDetail_Hue_NX1 = NULL;
	}

	if(m_pSmartPanelDriveDetail)
	{
		m_pSmartPanelDriveDetail->DestroyWindow();
		delete m_pSmartPanelDriveDetail;
		m_pSmartPanelDriveDetail = NULL;
	}

	if(m_pSmartPanelFlashDetailDlg)
	{
		m_pSmartPanelFlashDetailDlg->DestroyWindow();
		delete m_pSmartPanelFlashDetailDlg;
		m_pSmartPanelFlashDetailDlg = NULL;
	}

	RemoveCtrlInfoAll();	

	ShowWindow(SW_HIDE);
}

//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
void CSmartPanelChildDlg::ChangeSmartPanelPWChild(PW_DETAIL_TYPE nType, int nValueIndex)
{
	int nSelectPW = nValueIndex;
	CloseSmartPanelChild();
	
	switch(nType)
	{
	case PW_DETAIL_COLOR:
		nPWType = PW_DETAIL_COLOR;
		CreatePWDetail_Color_NX1(nSelectPW);
		break;
	case PW_DETAIL_SATURATION:
		nPWType = PW_DETAIL_SATURATION;
		CreatePWDetail_Saturation_NX1(nSelectPW);
		break;
	case PW_DETAIL_SHARPNESS:
		nPWType = PW_DETAIL_SHARPNESS;
		CreatePWDetail_Sharpness_NX1(nSelectPW);
		break;
	case PW_DETAIL_CONTRACT:
		nPWType = PW_DETAIL_CONTRACT;
		CreatePWDetail_Contrast_NX1(nSelectPW);
		break;
	case PW_DETAIL_HUE:
		nPWType = PW_DETAIL_HUE;
		CreatePWDetail_Hue_NX1(nSelectPW);
		break;
	}
}
//------------------------------------------------------------------------------ 
//! @brief		CreateAdjustDlg
//! @date		2011-7-28
//! @author		Lee JungTaek
//! @note	 	Create Detail Property Setting
//------------------------------------------------------------------------------ 
void CSmartPanelChildDlg::CreateAdjustDlg(int nValueIndex)
{
	switch(m_nPropType)
	{
	case PROP_DETAIL_TYPE_WB:	
		{
			switch(nValueIndex)
			{
			case DSC_WB_CUSTOM:		break;
			case DSC_WB_K:		CreateWBColorDetail(nValueIndex);	break;
			default:			CreateWBDetail(nValueIndex);		break;
			}
		}
		break;
	//CMiLRe NX1 SmartWizard Detail 추가
	case PROP_DETAIL_TYPE_PW:	
		if(m_strModel.Compare(modelType[MDOEL_NX2000].strModelName) == 0)
			CreatePWDetail(nValueIndex);	
		if(m_strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)
			CreatePWDetail_Color_NX1(nValueIndex);		
		break;
	case PROP_DETAIL_TYPE_DRIVE:
		{
			switch(nValueIndex)
			{
			case DSC_DRIVE_BURST:		CreateDriveDetail(nValueIndex, DRIVE_DETAIL_TYPE_BURST);	break;
			case DSC_DRIVE_TIMER:		CreateDriveDetail(nValueIndex, DRIVE_DETAIL_TYPE_BURST /*DRIVE_DETAIL_TYPE_TIMER*/);	break;
			default:					break;
			}
		}
	//dh0.seo Flash Detail 추가 2014-11-24
/*	case PROP_DETAIL_TYPE_FLASH:
		if(m_strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)
			CreateFlashDetail(nValueIndex);
		break;*/
	default:	break;
	}
}

//dh0.seo Flash Detail 추가 2014-11-24
/*void CSmartPanelChildDlg::CreateFlashDetail(int nValueIndex)
{
	//-- Close All Detail Dialog
	if(m_pSmartPanelFlashDetailDlg)
	{
		m_pSmartPanelFlashDetailDlg->DestroyWindow();
		delete m_pSmartPanelFlashDetailDlg;
		m_pSmartPanelFlashDetailDlg = NULL;
	}

	m_pSmartPanelFlashDetailDlg = new CSmartPanelFlashDetailDlg();
	m_pSmartPanelFlashDetailDlg->Create(CSmartPanelFlashDetailDlg::IDD, this);			

	int nPropCode = m_pSmartPanelFlashDetailDlg->GetDetailPropCode(nValueIndex);

	//-- Get Detail Value
	RSEvent* pMsgToMain = new RSEvent();
	pMsgToMain->nParam1 = ((CSmartPanelDlg*)GetParent())->GetMainCamera();				
	pMsgToMain->nParam2 = nPropCode;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED, (LPARAM)pMsgToMain);

	//-- Move Window
	CRect rect;
	GetWindowRect(&rect);

	CPoint point;
	GetCursorPos(&point);

	CheckWindowRect(point.x + 5, rect.top + 30, 357, 100, (CWnd*)m_pSmartPanelFlashDetailDlg);

	//-- 2011-8-18 Lee JungTaek
	//-- Move Cursor
	m_pSmartPanelFlashDetailDlg->GetWindowRect(&rect);
	SetCursorPos(rect.left + rect.Width() / 2, rect.top + rect.Height() / 2);

	m_pSmartPanelFlashDetailDlg->ShowWindow(SW_SHOW);
}*/

//------------------------------------------------------------------------------ 
//! @brief		CreateWBDetail
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelChildDlg::CreateWBDetail(int nValueIndex)
{
	//-- Close All Detail Dialog
	if(m_pSmartPanelWBDetail)
	{
		m_pSmartPanelWBDetail->DestroyWindow();
		delete m_pSmartPanelWBDetail;
		m_pSmartPanelWBDetail = NULL;
	}
	if(m_pSmartPanelWBColorDetail)
	{
		m_pSmartPanelWBColorDetail->DestroyWindow();
		delete m_pSmartPanelWBColorDetail;
		m_pSmartPanelWBColorDetail = NULL;
	}

	m_pSmartPanelWBDetail = new CSmartPanelWBDetailDlg();
	m_pSmartPanelWBDetail->Create(CSmartPanelWBDetailDlg::IDD, this);			

	int nPropCode = m_pSmartPanelWBDetail->GetDetailPropCode(nValueIndex);

	//-- Get Detail Value
	RSEvent* pMsgToMain = new RSEvent();
	pMsgToMain->nParam1 = ((CSmartPanelDlg*)GetParent())->GetMainCamera();				
	//CMiLRe 20141029 White Balnace Detail 종류별로 R/W
	pMsgToMain->nParam2 = PTP_DPC_SAMSUNG_WB_DETAIL_X;//nPropCode;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED, (LPARAM)pMsgToMain);

	//-- Move Window
	CRect rect;
	GetWindowRect(&rect);

	CPoint point;
	GetCursorPos(&point);

	CheckWindowRect(point.x + 300, rect.top + 30, 267, 299, m_pSmartPanelWBDetail);

	//-- 2011-8-18 Lee JungTaek
	//-- Move Cursor
	m_pSmartPanelWBDetail->GetWindowRect(&rect);
	SetCursorPos(rect.left + rect.Width() / 2, rect.top + rect.Height() / 2);

	m_pSmartPanelWBDetail->ShowWindow(SW_SHOW);
}

//------------------------------------------------------------------------------ 
//! @brief		CreateWBDetail
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelChildDlg::CreateWBColorDetail(int nValueIndex)
{
	//-- Close All Detail Dialog
	if(m_pSmartPanelWBDetail)
	{
		m_pSmartPanelWBDetail->DestroyWindow();
		delete m_pSmartPanelWBDetail;
		m_pSmartPanelWBDetail = NULL;
	}
	if(m_pSmartPanelWBColorDetail)
	{
		m_pSmartPanelWBColorDetail->DestroyWindow();
		delete m_pSmartPanelWBColorDetail;
		m_pSmartPanelWBColorDetail = NULL;
	}
	
	m_pSmartPanelWBColorDetail = new CSmartPanelWBColorDetailDlg();
	m_pSmartPanelWBColorDetail->Create(CSmartPanelWBColorDetailDlg::IDD, this);			

	int nPropCode = m_pSmartPanelWBColorDetail->GetDetailPropCode(nValueIndex);

	//-- Get Detail Value
	RSEvent* pMsgToMain = new RSEvent();
	pMsgToMain->nParam1 = ((CSmartPanelDlg*)GetParent())->GetMainCamera();				
	pMsgToMain->nParam2 = nPropCode;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED, (LPARAM)pMsgToMain);
 
	//-- Move Window
 	CRect rect;
 	GetWindowRect(&rect);
 
 	CPoint point;
 	GetCursorPos(&point);

	CheckWindowRect(point.x + 5, rect.top + 30, 357, 100, (CWnd*)m_pSmartPanelWBColorDetail);

	//-- 2011-8-18 Lee JungTaek
	//-- Move Cursor
	m_pSmartPanelWBColorDetail->GetWindowRect(&rect);
	SetCursorPos(rect.left + rect.Width() / 2, rect.top + rect.Height() / 2);

	m_pSmartPanelWBColorDetail->ShowWindow(SW_SHOW);
}

//------------------------------------------------------------------------------ 
//! @brief		CreatePWDetail
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelChildDlg::CreatePWDetail(int nValueIndex)
{
	if(m_pSmartPanelPWDetail)
	{
		m_pSmartPanelPWDetail->DestroyWindow();
		delete m_pSmartPanelPWDetail;
		m_pSmartPanelPWDetail = NULL;
	}

	m_pSmartPanelPWDetail = new CSmartPanelPWDetailDlg();
	m_pSmartPanelPWDetail->Create(CSmartPanelPWDetailDlg::IDD, this);		

	int nPropCode = m_pSmartPanelPWDetail->GetDetailPropCode(nValueIndex);

	//-- Get Detail Value
	RSEvent* pMsgToMain = new RSEvent();
	pMsgToMain->nParam1 = ((CSmartPanelDlg*)GetParent())->GetMainCamera();				
	pMsgToMain->nParam2 = nPropCode;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED, (LPARAM)pMsgToMain);

	//-- Move Window
	CRect rect;
	GetWindowRect(&rect);

	CPoint point;
	GetCursorPos(&point);

	CheckWindowRect(point.x + 5, rect.top + 30, 357, 209, (CWnd*)m_pSmartPanelPWDetail);

	//-- 2011-8-18 Lee JungTaek
	//-- Move Cursor
 	m_pSmartPanelPWDetail->GetWindowRect(&rect);
 	SetCursorPos(rect.left - 1, rect.top - 1);

	m_pSmartPanelPWDetail->ShowWindow(SW_SHOW);
}

//CMiLRe NX1 SmartWizard Detail 추가
//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
void CSmartPanelChildDlg::CreatePWDetail_Color_NX1(int nValueIndex)
{
	if(m_pSmartPanelPWDetail_Color_NX1)
	{
		m_pSmartPanelPWDetail_Color_NX1->DestroyWindow();
		delete m_pSmartPanelPWDetail_Color_NX1;
		m_pSmartPanelPWDetail_Color_NX1 = NULL;
	}

	m_pSmartPanelPWDetail_Color_NX1 = new CSmartPanelPWDetailDlg_Color_NX1();
	m_pSmartPanelPWDetail_Color_NX1->Create(CSmartPanelPWDetailDlg_Color_NX1::IDD, this);		

	int nPropCode = m_pSmartPanelPWDetail_Color_NX1->GetDetailPropCode(nValueIndex);

	RSEvent* pMsgToMain = new RSEvent();
	pMsgToMain->nParam1 = ((CSmartPanelDlg*)GetParent())->GetMainCamera();				
	pMsgToMain->nParam2 = nPropCode;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED, (LPARAM)pMsgToMain);

	//-- Move Window
	CRect rect;
	GetWindowRect(&rect);

	CPoint point;
	GetCursorPos(&point);

	CheckWindowRect(rect.left, rect.top, 357, 216, (CWnd*)m_pSmartPanelPWDetail_Color_NX1);

	m_pSmartPanelPWDetail_Color_NX1->ShowWindow(SW_SHOW);
}

//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
void CSmartPanelChildDlg::CreatePWDetail_Saturation_NX1(int nValueIndex)
{
	if(m_pSmartPanelPWDetail_Saturation_NX1)
	{
		m_pSmartPanelPWDetail_Saturation_NX1->DestroyWindow();
		delete m_pSmartPanelPWDetail_Saturation_NX1;
		m_pSmartPanelPWDetail_Saturation_NX1 = NULL;
	}

	m_pSmartPanelPWDetail_Saturation_NX1 = new CSmartPanelPWDetailDlg_Saturation_NX1();
	m_pSmartPanelPWDetail_Saturation_NX1->Create(CSmartPanelPWDetailDlg_Saturation_NX1::IDD, this);		

	int nPropCode = m_pSmartPanelPWDetail_Saturation_NX1->GetDetailPropCode(nValueIndex);

	RSEvent* pMsgToMain = new RSEvent();
	pMsgToMain->nParam1 = ((CSmartPanelDlg*)GetParent())->GetMainCamera();				
	pMsgToMain->nParam2 = nPropCode;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED, (LPARAM)pMsgToMain);
	
	//-- Move Window
	CRect rect;
	GetWindowRect(&rect);

	CPoint point;
	GetCursorPos(&point);

	CheckWindowRect(rect.left, rect.top, 357, 216, (CWnd*)m_pSmartPanelPWDetail_Saturation_NX1);

	m_pSmartPanelPWDetail_Saturation_NX1->ShowWindow(SW_SHOW);
}

//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
void CSmartPanelChildDlg::CreatePWDetail_Sharpness_NX1(int nValueIndex)
{
	if(m_pSmartPanelPWDetail_Sharpness_NX1)
	{
		m_pSmartPanelPWDetail_Sharpness_NX1->DestroyWindow();
		delete m_pSmartPanelPWDetail_Sharpness_NX1;
		m_pSmartPanelPWDetail_Sharpness_NX1 = NULL;
	}

	m_pSmartPanelPWDetail_Sharpness_NX1 = new CSmartPanelPWDetailDlg_Sharpness_NX1();
	m_pSmartPanelPWDetail_Sharpness_NX1->Create(CSmartPanelPWDetailDlg_Sharpness_NX1::IDD, this);		

	int nPropCode = m_pSmartPanelPWDetail_Sharpness_NX1->GetDetailPropCode(nValueIndex);

	RSEvent* pMsgToMain = new RSEvent();
	pMsgToMain->nParam1 = ((CSmartPanelDlg*)GetParent())->GetMainCamera();				
	pMsgToMain->nParam2 = nPropCode;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED, (LPARAM)pMsgToMain);
	
	//-- Move Window
	CRect rect;
	GetWindowRect(&rect);

	CPoint point;
	GetCursorPos(&point);

	CheckWindowRect(rect.left, rect.top, 357, 216, (CWnd*)m_pSmartPanelPWDetail_Sharpness_NX1);

	m_pSmartPanelPWDetail_Sharpness_NX1->ShowWindow(SW_SHOW);
}

//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
void CSmartPanelChildDlg::CreatePWDetail_Contrast_NX1(int nValueIndex)
{
	if(m_pSmartPanelPWDetail_Contrast_NX1)
	{
		m_pSmartPanelPWDetail_Contrast_NX1->DestroyWindow();
		delete m_pSmartPanelPWDetail_Contrast_NX1;
		m_pSmartPanelPWDetail_Contrast_NX1 = NULL;
	}

	m_pSmartPanelPWDetail_Contrast_NX1 = new CSmartPanelPWDetailDlg_Contrast_NX1();
	m_pSmartPanelPWDetail_Contrast_NX1->Create(CSmartPanelPWDetailDlg_Contrast_NX1::IDD, this);		

	int nPropCode = m_pSmartPanelPWDetail_Contrast_NX1->GetDetailPropCode(nValueIndex);

	RSEvent* pMsgToMain = new RSEvent();
	pMsgToMain->nParam1 = ((CSmartPanelDlg*)GetParent())->GetMainCamera();				
	pMsgToMain->nParam2 = nPropCode;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED, (LPARAM)pMsgToMain);
	
	//-- Move Window
	CRect rect;
	GetWindowRect(&rect);

	CPoint point;
	GetCursorPos(&point);

	CheckWindowRect(rect.left, rect.top, 357, 216, (CWnd*)m_pSmartPanelPWDetail_Contrast_NX1);

	m_pSmartPanelPWDetail_Contrast_NX1->ShowWindow(SW_SHOW);
}

//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
void CSmartPanelChildDlg::CreatePWDetail_Hue_NX1(int nValueIndex)
{
	if(m_pSmartPanelPWDetail_Hue_NX1)
	{
		m_pSmartPanelPWDetail_Hue_NX1->DestroyWindow();
		delete m_pSmartPanelPWDetail_Hue_NX1;
		m_pSmartPanelPWDetail_Hue_NX1 = NULL;
	}

	m_pSmartPanelPWDetail_Hue_NX1 = new CSmartPanelPWDetailDlg_Hue_NX1();
	m_pSmartPanelPWDetail_Hue_NX1->Create(CSmartPanelPWDetailDlg_Hue_NX1::IDD, this);		

	int nPropCode = m_pSmartPanelPWDetail_Hue_NX1->GetDetailPropCode(nValueIndex);

	RSEvent* pMsgToMain = new RSEvent();
	pMsgToMain->nParam1 = ((CSmartPanelDlg*)GetParent())->GetMainCamera();				
	pMsgToMain->nParam2 = nPropCode;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED, (LPARAM)pMsgToMain);
	
	//-- Move Window
	CRect rect;
	GetWindowRect(&rect);

	CPoint point;
	GetCursorPos(&point);

	CheckWindowRect(rect.left, rect.top, 357, 216, (CWnd*)m_pSmartPanelPWDetail_Hue_NX1);

	m_pSmartPanelPWDetail_Hue_NX1->ShowWindow(SW_SHOW);
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-8-16
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelChildDlg::CreateDriveDetail(int nValueIndex, int nType)
{
	if(m_pSmartPanelDriveDetail)
	{
		m_pSmartPanelDriveDetail->DestroyWindow();
		delete m_pSmartPanelDriveDetail;
		m_pSmartPanelDriveDetail = NULL;
	}

	m_pSmartPanelDriveDetail = new CSmartPanelDriveDetailDlg(nType);
	m_pSmartPanelDriveDetail->Create(CSmartPanelDriveDetailDlg::IDD, this);			

	int nPropCode = -1;
	
	switch(nType)
	{
	case DRIVE_DETAIL_TYPE_BURST:	nPropCode = PTP_CODE_SAMSUNG_DRIVE_DETAIL_BURST;	break;
	case DRIVE_DETAIL_TYPE_TIMER:	nPropCode = PTP_CODE_SAMSUNG_DRIVE_DETAIL_TIMER;	break;
	}

	m_pSmartPanelDriveDetail->SetPTPCode(nPropCode);

	//-- Get Detail Value
	RSEvent* pMsgToMain = new RSEvent();
	pMsgToMain->nParam1 = ((CSmartPanelDlg*)GetParent())->GetMainCamera();				
	pMsgToMain->nParam2 = nPropCode;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED, (LPARAM)pMsgToMain);

	//-- Move Window
	CRect rect;
	GetWindowRect(&rect);

	CPoint point;
	GetCursorPos(&point);

//	CheckWindowRect(point.x + 5, rect.top + 30, 357, 100, (CWnd*)m_pSmartPanelDriveDetail);
	CheckWindowRect(point.x + 5, rect.top + 30, 357, 209, (CWnd*)m_pSmartPanelDriveDetail);

	//-- 2011-8-18 Lee JungTaek
	//-- Move Cursor
	m_pSmartPanelDriveDetail->GetWindowRect(&rect);
	SetCursorPos(rect.left + rect.Width() / 2, rect.top + rect.Height() / 2);

	m_pSmartPanelDriveDetail->ShowWindow(SW_SHOW);	
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-28
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelChildDlg::SetDetailProperty(int nPropType, int nValue)
{
	switch(nPropType)
	{
	case PROP_DETAIL_TYPE_WB:
		if(m_pSmartPanelWBDetail)	
			m_pSmartPanelWBDetail->SetCursorIndex(nValue);
		else if(m_pSmartPanelWBColorDetail)
			m_pSmartPanelWBColorDetail->SetValueIndex(nValue);
		break;
	//CMiLRe NX1 SmartWizard Detail 추가
	case PROP_DETAIL_TYPE_PW:
		if(m_pSmartPanelPWDetail && m_strModel.Compare(modelType[MDOEL_NX2000].strModelName) == 0)	
			m_pSmartPanelPWDetail->SetValueIndex(nValue);
		//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
		else if(m_strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)
		{
			if(m_pSmartPanelPWDetail_Color_NX1 != NULL && nPWType == PW_DETAIL_COLOR)
				m_pSmartPanelPWDetail_Color_NX1->SetValueIndex(nValue);
			else if(m_pSmartPanelPWDetail_Saturation_NX1 != NULL && nPWType == PW_DETAIL_SATURATION)
				m_pSmartPanelPWDetail_Saturation_NX1->SetValueIndex(nValue);
			else if(m_pSmartPanelPWDetail_Sharpness_NX1 != NULL && nPWType == PW_DETAIL_SHARPNESS)
				m_pSmartPanelPWDetail_Sharpness_NX1->SetValueIndex(nValue);
			else if(m_pSmartPanelPWDetail_Contrast_NX1 != NULL && nPWType == PW_DETAIL_CONTRACT)
				m_pSmartPanelPWDetail_Contrast_NX1->SetValueIndex(nValue);
			else if(m_pSmartPanelPWDetail_Hue_NX1 != NULL && nPWType == PW_DETAIL_HUE)
				m_pSmartPanelPWDetail_Hue_NX1->SetValueIndex(nValue);
		}
		break;
	case PROP_DETAIL_TYPE_DRIVE:
		if(m_pSmartPanelDriveDetail)
			m_pSmartPanelDriveDetail->SetValueIndex(nValue);		
		break;
	case PROP_DETAIL_TYPE_ETC:		break;
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelChildDlg::OnMouseMove(UINT nFlags, CPoint point)
{
// 	//-- MOUSE LEAVE
 	MouseEvent();
 	CDialog::OnMouseMove(nFlags, point);
}

void CSmartPanelChildDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = GetSafeHwnd();
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	::_TrackMouseEvent(&tme);
}

LRESULT CSmartPanelChildDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	CPoint point;
	GetCursorPos(&point);
	ScreenToClient(&point);

	CRect rect;
	GetClientRect(&rect);

/*	if(!rect.PtInRect(point)) 
		CloseSmartPanelChild();*/

	if(point.y < rect.top)
		CloseSmartPanelChild();
	//CMiLRe 20141127 스마트패널 차일드 마우스 리브때 사라짐
	else if(point.y + 12> rect.bottom)
		CloseSmartPanelChild();
	//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
	else if(point.x + 12> rect.right)
		CloseSmartPanelChild();
	//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
	else if(point.x -12 < rect.left)
		CloseSmartPanelChild();

	return 0L;
}   