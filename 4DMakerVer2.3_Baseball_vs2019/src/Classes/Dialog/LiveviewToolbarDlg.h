/////////////////////////////////////////////////////////////////////////////
//
//  LiveviewToolbarDlg.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#pragma once


#include "resource.h"
#include "RSChildDialog.h"
#include "NXRemoteStudioFunc.h"

enum
	{
		BTN_BAR_NULL				= -1,
		BTN_BAR_SHIFT_LEFT		,
		BTN_BAR_SHIFT_RIGHT		,
		BTN_BAR_ZOOM			,
		BTN_BAR_GRID				,
		BTN_BAR_FOCUS_BODY	,
		BTN_BAR_FOCUS_POINT	,
		BTN_BAR_FOCUS_REW		,
		BTN_BAR_FOCUS_PREV		,
		BTN_BAR_FOCUS_NEXT	,
		BTN_BAR_FOCUS_FF		,
		BTN_BAR_FOCUS_STATUS	,
		BTN_BAR_CNT				,
	};

// CLiveviewToolbarDlg dialog
class CLiveviewToolbarDlg : public CRSChildDialog
{
	DECLARE_DYNAMIC(CLiveviewToolbarDlg)

public:
	CLiveviewToolbarDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLiveviewToolbarDlg();

// Dialog Data
	enum { IDD = IDD_DLG_BOARD};
		
	enum
	{
		PT_BAR_IN_NOTHING = -1,
		PT_BAR_NOR	,
		PT_BAR_INSIDE,
		PT_BAR_DOWN,
		PT_BAR_DIS,
	};

	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	DECLARE_MESSAGE_MAP()

	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);


private:
	int		m_nPressed;
	int		m_nMouseIn;	
	CRect	m_rect[BTN_BAR_CNT];
	int		m_nStatus[BTN_BAR_CNT];

	void DrawBackground();
	void DrawItem(CDC* pDC, int nItem);
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	void ChangeStatus(int nItem, int nStatus);
	void ChangeStatusAll();
	void MouseEvent();

	BOOL IsMF();
	void ShiftLeft();
	void ShiftRight();		

	//-- 2011-6-24 Lee JungTaek
	void SetFocusPosition(int nType);		
	//-- 2011-6-24 Lee JungTaek
	FocusPosition m_tFocusPosition;

	//-- Tooltip
	int m_nToolTipBtn;

	int m_nAFStatus;

public:
	void SetFocus(FocusPosition* );
	void GetFocus();
	void UpdateScroll();
	void RedrawIcon(int );
	void SetAFFocus(int nValue);
	CToolTipCtrl m_ToolTip;
};
