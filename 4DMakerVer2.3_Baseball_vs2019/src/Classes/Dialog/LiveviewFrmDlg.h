/////////////////////////////////////////////////////////////////////////////
//
//  LiveviewFrmDlg.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "Liveview.h"
#include "RSChildDialog.h"
#include "NXRemoteStudioFunc.h"

// CLiveviewFrmDlg dialog
class CLiveviewFrmDlg : public CRSChildDialog
{
	DECLARE_DYNAMIC(CLiveviewFrmDlg)

public:
	CLiveviewFrmDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLiveviewFrmDlg();

// Dialog Data
	enum { IDD = IDD_DLG_BOARD};
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	DECLARE_MESSAGE_MAP()
	
private:
	int m_nShift;
	int m_nRatio;
	CLiveview* m_pLiveview;	
	void GetLiveviewRect(LPRECT lpRect);

public:
	CLiveview* GetLiveview()	{ return m_pLiveview; }
	void DrawLiveview();
	void SetRotate(int nRotate);
	void ShiftLeft();
	void ShiftRight();
	BOOL IsRotate()	{ return m_nShift % 2; }
	void SetRatio	(LPRECT lpRect);	
	int GetRatio()		{ return m_nRatio; }
	CSize GetSrcSize()		{ return m_pLiveview->GetSrcSize(); }
	CSize GetOffsetSize()	{ return m_pLiveview->GetOffsetSize(); }
};
