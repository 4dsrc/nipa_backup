/////////////////////////////////////////////////////////////////////////////
//
//  OptionPCSet2Dlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "OptionPCSet2Dlg.h"
#include "OptionBodyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// COptionPCSet2Dlg dialog
IMPLEMENT_DYNAMIC(COptionPCSet2Dlg, COptionBaseDlg)

//------------------------------------------------------------------------------ 
//! @brief		COptionPCSet2Dlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
COptionPCSet2Dlg::COptionPCSet2Dlg(CWnd* pParent /*=NULL*/)
	: COptionBaseDlg(COptionPCSet2Dlg::IDD, pParent)
{
	m_strPrefix = _T("NX");
	//CMiLRe 20141118 사진 저장 위치 고정 - 카메라 NO별로 저장
	m_nFileNameType = 2;	//-- Set From File
	m_nStatus[0] = BTN_NORMAL;
	m_nStatus[1] = BTN_NORMAL;
	m_nCheckView = 0;
	m_nSelectView = 0;
	m_nSelectViewSize =0;
}

COptionPCSet2Dlg::~COptionPCSet2Dlg()
{
	//CMiLRe 20141113 메모리 릭 제거
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		if(m_staticLabel[i])
		{
			delete m_staticLabel[i];
			m_staticLabel[i] = NULL;
		}

		if(m_staticLabelBar[i])
		{
			delete m_staticLabelBar[i];
			m_staticLabelBar[i] = NULL;
		}		
	}
}

void COptionPCSet2Dlg::DoDataExchange(CDataExchange* pDX)
{
	COptionBaseDlg::DoDataExchange(pDX);		
	DDX_Text(pDX, IDC_EDIT_1, m_strPrefix);
	DDX_Control(pDX, IDC_EDIT_1, m_edit);
	DDX_Control(pDX, IDC_CMB_1, m_comboBox_View[0]);
	DDX_Control(pDX, IDC_CMB_2, m_comboBox_View[1]);
	DDX_Control(pDX, IDC_CMB_3, m_comboBox_View[2]);
//	DDX_CBIndex(pDX,IDC_CMB_1, m_combo_View);
}				

BEGIN_MESSAGE_MAP(COptionPCSet2Dlg, COptionBaseDlg)	
	ON_WM_LBUTTONDOWN()	
	ON_WM_MOUSEMOVE()
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	//CMiLRe 20141118 사진 저장 위치 고정 - 카메라 NO별로 저장
#ifdef PICTURE_SAVE_PATH_FIXED
	ON_BN_CLICKED(IDC_CHECKBOX_1, OnPrefixCameraName	)		
	ON_BN_CLICKED(IDC_CHECKBOX_2, OnDateNPrefixCameraName	)		
	ON_BN_CLICKED(IDC_CHECKBOX_3, OnCameraNameDateNPrefix	)		
#endif
	ON_BN_CLICKED(IDC_BTN_OPT_APPLY, OnCancel	)		
	ON_BN_CLICKED(IDC_BTN_OPT_CANCEL, OnStart)	
//	ON_BN_CLICKED(IDC_CHECKBOX_3, OnAutoView_Check)
	ON_CBN_SELCHANGE(IDC_CMB_1, OnSelchangeView)
	ON_CBN_SELCHANGE(IDC_CMB_2, OnSelchangeViewSize)
	ON_CBN_SELCHANGE(IDC_CMB_3, OnSelchangeViewState)
END_MESSAGE_MAP()

// CLiveview message handlers

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionPCSet2Dlg::CreateFrames()
{	
	
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	//CMiLRe 20141118 사진 저장 위치 고정 - 카메라 NO별로 저장
#ifdef PICTURE_SAVE_PATH_FIXED
	m_PushBtn[0].Create(_T(""), WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX, CRect(CPoint(40, 28*2 + 5),CSize(20,21)), this, IDC_CHECKBOX_1);	
	m_PushBtn[1].Create(_T(""), WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX, CRect(CPoint(40, 28*3 + 5),CSize(20,21)), this, IDC_CHECKBOX_2);
	m_PushBtn[2].Create(_T(""), WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX, CRect(CPoint(40, 28*4 + 5),CSize(20,21)), this, IDC_CHECKBOX_3);
	m_PushBtn[3].Create(_T("Cancel"), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, CRect(CPoint(8,28*5 + 5),CSize(60,26)), this, IDC_BTN_OPT_APPLY);
	m_PushBtn[4].Create(_T("Apply"), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, CRect(CPoint(322+40, 28*5 + 5),CSize(60,26)), this, IDC_BTN_OPT_CANCEL);
#else
	m_PushBtn[0].Create(_T("Cancel"), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, CRect(CPoint(8,28*5 + 5),CSize(60,26)), this, IDC_BTN_OPT_APPLY);
	m_PushBtn[1].Create(_T("Apply"), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, CRect(CPoint(322+40, 28*5 + 5),CSize(60,26)), this, IDC_BTN_OPT_CANCEL);
#endif

	//CMiLRe 20141125 영문 'g' 아랫 부분 짤리는 버그 수정
	m_edit.MoveWindow	(80, 28*1 +	6,	150,22);

	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		m_staticLabel[i] = new CStatic();
		m_staticLabelBar[i] = new CStatic();
	}
	
	m_staticLabel[0]->Create(_T("Download Image File Name"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*0+10, 200, 31*1), this, IDC_STATIC_ETC_1);		
	m_staticLabel[1]->Create(_T("Prefix"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*1+10, 60, 31*2), this, IDC_STATIC_ETC_2);
	m_staticLabel[2]->Create(_T("Viewer"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*2+10, 60, 31*3), this, IDC_STATIC_ETC_3);
	//CMiLRe 20141118 사진 저장 위치 고정 - 카메라 NO별로 저장
	//m_staticLabel[2]->Create(_T("Prefix_CameraName_Date_Time"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(80, 28*2 +10, 300, 31*3), this, IDC_STATIC_ETC_3);	
	//m_staticLabel[3]->Create(_T("Date\\Prefix_CameraName_Time"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(80, 28*3 +10, 300, 31*4), this, IDC_STATIC_ETC_4);	
	//m_staticLabel[4]->Create(_T("CameraName\\Prefix_Date_Time"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(80, 28*4 +10, 300, 31*5), this, IDC_STATIC_ETC_5);	
	
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		m_staticLabelBar[i]->Create(_T(""),  WS_CHILD|WS_VISIBLE|SS_BLACKRECT | SS_LEFT,  CRect(1,30+28*i, OPTION_DLG_WIDTH, 31+28*i), this );
	}

//	m_btnAutoView.Create(_T(""), WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX, CRect(CPoint(15, 29*2 + 5),CSize(20,21)), this, IDC_CHECKBOX_3);
	m_comboBox_View[0].MoveWindow(80, 28*2 +5,	140,22);
	m_comboBox_View[0].AddString(_T("Windows Viewer"));
	m_comboBox_View[0].AddString(_T("SRS Viewer"));

	m_comboBox_View[0].SetCurSel(m_nSelectView);

//	m_btnAutoView.SetCheck(m_nCheckView);

//	m_btnAutoView.ShowWindow(FALSE);

	m_comboBox_View[1].MoveWindow(225, 28*2 +5,	70,22);
	m_comboBox_View[1].AddString(_T("Normal"));
	m_comboBox_View[1].AddString(_T("Full"));

	m_comboBox_View[1].SetCurSel(m_nSelectViewSize);

	m_comboBox_View[2].MoveWindow(300, 28*2 +5, 50, 22);
	m_comboBox_View[2].AddString(_T("Off"));
	m_comboBox_View[2].AddString(_T("On"));

	m_comboBox_View[2].SetCurSel(m_nCheckView);

//	if(m_btnAutoView.GetCheck())

	if(m_comboBox_View[2].GetCurSel() == 0)
	{
		m_comboBox_View[0].EnableWindow(FALSE);
		m_comboBox_View[1].EnableWindow(FALSE);
	}
	else
	{
		m_comboBox_View[0].EnableWindow(TRUE);
		if(m_comboBox_View[0].GetCurSel() == 0)
			m_comboBox_View[1].EnableWindow(FALSE);
		else
			m_comboBox_View[1].EnableWindow(TRUE);
	}

	this->SetBackgroundColor(GetBKColor());
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionPCSet2Dlg::UpdateFrames()
{
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	COLORREF color	= GetTextColor();
		
	CString strPath = RSGetRoot();
	CString strFullPath;
	
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_line.png"),strPath);
	//-- 2011-07-14 hongsu.jung
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Download Image File Name"			),15, 28*0 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*1, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Prefix"										),15, 28*1 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*2, rect.Width(),1);	
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Prefix_CameraName_Date_Time"		),80, 28*2 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*3, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Date\\Prefix_CameraName_Time"		),80, 28*3 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*4, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("CameraName\\Prefix_Date_Time"		),80, 28*4 +10,color);		
	//-- Draw Buttons 	
	for(int i = 0; i < 3; i ++)
	{
		if( i == m_nFileNameType)	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_icon_checked.png"),strPath);	
		else							strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_icon_uncheck.png"),strPath);
		DrawGraphicsImage(&dc, strFullPath, m_rectBtn[i]);
	}	
	
	//-- Draw Buttons Background
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_softkey_bg.png"),strPath);
	DrawGraphicsImage(&dc, strFullPath, 0,28*5, rect.Width(),32);

	switch(m_nStatus[0])
	{
		case BTN_INSIDE:		{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png"		),strPath);		color = RGB(255,255,255); }	break;
		case BTN_DOWN:		{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_push.png"			),strPath);		color = RGB(219,219,219); }	break;
		default:					{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png"	),strPath);		color = RGB(219,219,219); }	break;	
	}	
	DrawGraphicsImage(&dc, strFullPath, m_rectBtn[3]);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Cancel"),m_rectBtn[3],color);
	switch(m_nStatus[1])
	{
		case BTN_INSIDE:		{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png"	),strPath);		color = RGB(255,255,255); }	break;
		case BTN_DOWN:		{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_push.png"		),strPath);		color = RGB(219,219,219); }	break;		
		default:					{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png"	),strPath);		color = RGB(219,219,219); }	break;	
	}	
	DrawGraphicsImage(&dc, strFullPath, m_rectBtn[4]);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Apply"),m_rectBtn[4],color);

	Invalidate(FALSE);	
}
	
//------------------------------------------------------------------------------ 
//! @brief		InitProp
//! @date		2011-6-16
//! @author		Lee JungTaek
//! @note	 	Init From File
//------------------------------------------------------------------------------ 
void COptionPCSet2Dlg::InitProp(RSPCSet2& opt)
{
	m_strPrefix		=  opt.strPrefix;
	//CMiLRe 20141118 사진 저장 위치 고정 - 카메라 NO별로 저장
	m_nFileNameType	=  opt.nFileNameType = 2;
	m_nCheckView	=  opt.nCheckView;
	m_nSelectView	=  opt.nSelectView;
	m_nSelectViewSize	=  opt.nSelectViewSize;


	RSCheckState(m_nCheckView, m_nSelectView, m_nSelectViewSize);
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	//CMiLRe 20141118 사진 저장 위치 고정 - 카메라 NO별로 저장
#ifdef PICTURE_SAVE_PATH_FIXED
	for(int i = 0 ; i < 3 ; i ++)
	{
		m_PushBtn[i].SetCheck(0);
	}
	m_PushBtn[m_nFileNameType].SetCheck(1);
#endif
	UpdateData(FALSE);

	OnChangeState();
}

//------------------------------------------------------------------------------ 
//! @brief		SaveProp
//! @date		2011-6-16
//! @author		Lee JungTaek
//! @note	 	Init From File
//------------------------------------------------------------------------------ 
void COptionPCSet2Dlg::SaveProp(RSPCSet2& opt)
{
	if(!UpdateData(TRUE))
		return;
	opt.strPrefix		= m_strPrefix;
	//CMiLRe 20141118 사진 저장 위치 고정 - 카메라 NO별로 저장
	opt.nFileNameType	= m_nFileNameType = 2;
	opt.nCheckView		= m_nCheckView;
	opt.nSelectView		= m_nSelectView;
	opt.nSelectViewSize = m_nSelectViewSize;

	RSCheckState(m_nCheckView, m_nSelectView, m_nSelectViewSize);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int COptionPCSet2Dlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = -1;
	for(int i = 0; i < 5; i ++)
	{
		if(m_rectBtn[i].PtInRect(pt))
		{
			nReturn = i;
			break;
		}
	}
	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionPCSet2Dlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	
	switch(nItem)
	{
		case 0: 
		case 1: 
		case 2:
			InvalidateRect(m_rectBtn[m_nFileNameType ]);
			InvalidateRect(m_rectBtn[nItem]);
			m_nFileNameType = nItem;
			break;
		case 3:
			//-- Close Windows
			((COptionBodyDlg*)GetParent())->InitOptionProp();
			::SendMessage(GetParent()->GetParent()->m_hWnd,WM_CLOSE,NULL,NULL);
			break;
		case 4:	
			//-- Apply
			((COptionBodyDlg*)GetParent())->SaveOptionProp();
			break;
	}
	CDialog::OnLButtonDown(nFlags, point);	
}

	//CMiLRe 20141118 사진 저장 위치 고정 - 카메라 NO별로 저장
#ifdef PICTURE_SAVE_PATH_FIXED
//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionPCSet2Dlg::OnPrefixCameraName()
{
	for(int i = 0 ; i < 3 ; i ++)
	{
		m_PushBtn[i].SetCheck(0);
	}
	m_PushBtn[0].SetCheck(1);
	m_nFileNameType = 0;
}

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionPCSet2Dlg::OnDateNPrefixCameraName()
{
	for(int i = 0 ; i < 3 ; i ++)
	{
		m_PushBtn[i].SetCheck(0);
	}
	m_PushBtn[1].SetCheck(1);
	m_nFileNameType = 1;
}

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionPCSet2Dlg::OnCameraNameDateNPrefix()
{
	for(int i = 0 ; i < 3 ; i ++)
	{
		m_PushBtn[i].SetCheck(0);
	}
	m_PushBtn[2].SetCheck(1);
	m_nFileNameType = 2;
}
#endif

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionPCSet2Dlg::OnStart()
{
	((COptionBodyDlg*)GetParent())->SaveOptionProp();	
}

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionPCSet2Dlg::OnCancel()
{
	((COptionBodyDlg*)GetParent())->InitOptionProp();
	::SendMessage(GetParent()->GetParent()->m_hWnd,WM_CLOSE,NULL,NULL);
}
//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionPCSet2Dlg::OnMouseMove(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);
	if(nItem == 3)
	{
		if(m_nStatus[0] != BTN_INSIDE) 
		{ 
			m_nStatus[0] = BTN_INSIDE;	
			InvalidateRect(m_rectBtn[nItem]);
		}
	}
	else if(nItem == 4)
	{
		if(m_nStatus[1] != BTN_INSIDE) 
		{ 
			m_nStatus[1] = BTN_INSIDE;	
			InvalidateRect(m_rectBtn[nItem]);
		}
	}
	else
		ResetButton();
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseLeave
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT COptionPCSet2Dlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	ResetButton();
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		ResetButton
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionPCSet2Dlg::ResetButton()
{
	if(m_nStatus[0] != BTN_NORMAL)
	{
		m_nStatus[0] = BTN_NORMAL;	
		InvalidateRect(m_rectBtn[3]);
	}
	if(m_nStatus[1] != BTN_NORMAL)
	{
		m_nStatus[1] = BTN_NORMAL;	
		InvalidateRect(m_rectBtn[4]);
	}
}

/*void COptionPCSet2Dlg::OnAutoView_Check() 
{
	if(m_btnAutoView.GetCheck())
	{
		m_comboBox_View[0].EnableWindow(TRUE);
		m_comboBox_View[1].EnableWindow(TRUE);
		m_nCheckView = 1;
	}
	else
	{
		m_comboBox_View[0].EnableWindow(FALSE);
		m_comboBox_View[1].EnableWindow(FALSE);
		m_nCheckView = 0;
	}
} */

void COptionPCSet2Dlg::OnSelchangeView() 
{
	if(m_comboBox_View[0].GetCurSel() == 0)
	{
		m_comboBox_View[1].EnableWindow(FALSE);
		m_nSelectView = 0;
	}
	else
	{
		m_comboBox_View[1].EnableWindow(TRUE);
		m_nSelectView = 1;
	}
}

void COptionPCSet2Dlg::OnChangeState()
{
	m_comboBox_View[0].SetCurSel(m_nSelectView);
	m_comboBox_View[1].SetCurSel(m_nSelectViewSize);
	m_comboBox_View[2].SetCurSel(m_nCheckView);
//	m_btnAutoView.SetCheck(m_nCheckView);

	if(m_comboBox_View[2].GetCurSel() == 1)
	{
		m_comboBox_View[0].EnableWindow(TRUE);

		if(m_comboBox_View[0].GetCurSel() == 0)
			m_comboBox_View[1].EnableWindow(FALSE);
		else
			m_comboBox_View[1].EnableWindow(TRUE);
	}
	else
	{
		m_comboBox_View[0].EnableWindow(FALSE);
		m_comboBox_View[1].EnableWindow(FALSE);
	}

	
}

void COptionPCSet2Dlg::OnSelchangeViewSize()
{
	if(m_comboBox_View[1].GetCurSel() == 0)
	{
		m_nSelectViewSize = 0;
	}
	else
	{
		m_nSelectViewSize = 1;
	}
}

void COptionPCSet2Dlg::OnSelchangeViewState()
{
	if(m_comboBox_View[2].GetCurSel() == 0)
	{
		m_comboBox_View[0].EnableWindow(FALSE);
		m_comboBox_View[1].EnableWindow(FALSE);
		m_nCheckView = 0;
	}
	else
	{
		m_nCheckView = 1;
		if(m_comboBox_View[0].GetCurSel() == 0)
		{
			m_comboBox_View[0].EnableWindow(TRUE);
			m_comboBox_View[1].EnableWindow(FALSE);
		}
		else
		{
			m_comboBox_View[0].EnableWindow(TRUE);
			m_comboBox_View[1].EnableWindow(TRUE);
		}
	}
}

BOOL COptionPCSet2Dlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_RETURN )
		{
			pMsg->wParam = NULL;	
		}
	}
	return COptionBaseDlg::PreTranslateMessage(pMsg);
}
