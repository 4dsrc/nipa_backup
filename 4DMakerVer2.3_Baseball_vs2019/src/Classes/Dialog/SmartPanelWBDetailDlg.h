#pragma once
/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelWBDetailDlg.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-07-01
//
/////////////////////////////////////////////////////////////////////////////

// CSmartPanelWBDetailDlg dialog
#include "BKDialogST.h"
#include "NXRemoteStudioFunc.h"

enum
{
	BTN_WB_NULL = -1,
	BTN_WB_UP,
	BTN_WB_DOWN ,
	BTN_WB_LEFT ,
	BTN_WB_RIGHT,
	BTN_WB_RESET,
	BTN_WB_OK,
	BTN_WB_CURSOR,
	BTN_WB_CNT	,
};

enum
{
	PT_WB_IN_NOTHING = -1,
	PT_WB_NOR,
	PT_WB_INSIDE,
	PT_WB_DOWN,
	PT_WB_DIS,
};

class CSmartPanelWBDetailDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CSmartPanelWBDetailDlg)

public:
	CSmartPanelWBDetailDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSmartPanelWBDetailDlg();

// Dialog Data
	enum { IDD = IDD_DLG_SMARTPANEL_WB_DETAIL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	int		m_nPTPCode;
	int		m_nValueIndex;
	//-- Item Info
	int		m_nMouseIn;
	int		m_nStatus[BTN_WB_CNT];
	int		m_nXIndex;
	int		m_nYIndex;
	int		m_nXIndex1st;	
	int		m_nYIndex1st;
	BOOL	m_bInit;
	CRect m_rect[BTN_WB_CNT];
	CRect m_rectCursor;
	
	void DrawBackground();	
	void DrawItem(CDC* pDC, int nItem);
	void CleanDC(CDC* pDC);

	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	void ChangeStatus(int nItem, int nStatus);
	void ChangeStatusAll();
	void MouseEvent();

	//-- 2011-7-29 Lee JungTaek
	void ResetValue();
	void SetDetailValue();
	BOOL SetPosition(CPoint point);
	void SetFirstValue();

public:
	void SetCursorRect(int x, int y);
	void SetCursorIndex(int nValue);	

	//-- 2011-7-28 Lee JungTaek
	int GetDetailPropCode(int nValueIndex);
};
