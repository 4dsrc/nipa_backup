/////////////////////////////////////////////////////////////////////////////
//
//  LiveviewToolbarDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudioDlg.h"
#include "LiveviewToolbarDlg.h"
#include "SRSIndex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const int timer_button_pressed		= WM_USER + 0x0001;
static const int wait_pressed_time			= 600;

// CLiveviewToolbarDlg dialog
IMPLEMENT_DYNAMIC(CLiveviewToolbarDlg,  CRSChildDialog)

CLiveviewToolbarDlg::CLiveviewToolbarDlg(CWnd* pParent /*=NULL*/)
	: CRSChildDialog(CLiveviewToolbarDlg::IDD, pParent)
{
	m_nMouseIn = BTN_BAR_NULL;	
	for(int i=0; i<BTN_BAR_CNT; i++)
	{
		m_nStatus[i] = PT_BAR_NOR;
		m_rect[i] = CRect(0,0,0,0);
	}

	//-- Focus
	m_tFocusPosition.max		= 100;
	m_tFocusPosition.min		= 0;
	m_tFocusPosition.current	= 50;

	m_nAFStatus = 0;

	//-- Pressed
	m_nPressed = BTN_BAR_NULL;
}

CLiveviewToolbarDlg::~CLiveviewToolbarDlg()
{	
}

void CLiveviewToolbarDlg::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);	
}

BEGIN_MESSAGE_MAP(CLiveviewToolbarDlg, CRSChildDialog)	
	ON_WM_SIZE()	
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_TIMER()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()

void CLiveviewToolbarDlg::OnPaint()
{	
	DrawBackground();
	CRSChildDialog::OnPaint();
}

// CLiveviewToolbarDlg message handlers
BOOL CLiveviewToolbarDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();

	//-- 2011-8-28 Lee JungTaek
	//-- Create ToolTip
	if (m_ToolTip.m_hWnd == NULL) 
	{ 
		// Create ToolTip control 
		m_ToolTip.Create(this); 
		// Create inactive 
		m_ToolTip.Activate(FALSE);
	} 

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLiveviewToolbarDlg::OnSize(UINT nType, int cx, int cy) 
{
    CDialog::OnSize(nType, cx, cy);
	DrawBackground();
} 

//------------------------------------------------------------------------------ 
//! @brief		OnTimer
//! @date		2011-6-16
//------------------------------------------------------------------------------ 
void CLiveviewToolbarDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
		case timer_button_pressed:
		{
			KillTimer(timer_button_pressed);
			switch(m_nPressed)
			{
			case BTN_BAR_FOCUS_REW:		if(IsMF())	SetFocusPosition(FOCUS_REW	);	break;
			case BTN_BAR_FOCUS_PREV:		if(IsMF())	SetFocusPosition(FOCUS_PREV	);	break;
			case BTN_BAR_FOCUS_NEXT:		if(IsMF())	SetFocusPosition(FOCUS_NEXT);	break;
			case BTN_BAR_FOCUS_FF:			if(IsMF())	SetFocusPosition(FOCUS_FF	);	break;
			default: 
				CRSChildDialog::OnTimer(nIDEvent);
				return;
			}
			//-- Check Next Timer
			SetTimer(timer_button_pressed, wait_pressed_time/3, NULL);									
		}
		break;
	}
	CRSChildDialog::OnTimer(nIDEvent);
}
//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveviewToolbarDlg::DrawBackground()
{
	if(!ISSet())
		return;
	
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	int nImgX, nImgY, nImgW, nImgH;

	//-- Set Position
//	m_rect[BTN_BAR_SHIFT_LEFT		]	= CRect(CPoint(rect.right- 439, 26)	,	CSize(42,30));
//	m_rect[BTN_BAR_SHIFT_RIGHT	]	= CRect(CPoint(rect.right- 394, 26)	,	CSize(42,30));
//	m_rect	[BTN_BAR_ZOOM			]	= CRect(CPoint(rect.right- 349, 26)	,	CSize(42,30));
//	m_rect[BTN_BAR_GRID				]	= CRect(CPoint(rect.right- 304, 26)	,	CSize(42,30));
//	m_rect[BTN_BAR_GRID				]	= CRect(CPoint(rect.right- 349, 26)	,	CSize(42,30));
	m_rect[BTN_BAR_FOCUS_REW		]	= CRect(CPoint(rect.right- 245, 29)	,	CSize(25,25));
	m_rect[BTN_BAR_FOCUS_PREV	]	= CRect(CPoint(rect.right- 217, 29)	,	CSize(25,25));
	m_rect[BTN_BAR_FOCUS_BODY	]	= CRect(CPoint(rect.right- 188, 33)	,	CSize(110,16));
	m_rect[BTN_BAR_FOCUS_POINT	]	= CRect(CPoint(rect.right- 138, 35)	,	CSize(8,12));
	m_rect[BTN_BAR_FOCUS_NEXT	]	= CRect(CPoint(rect.right-  72, 29)	,	CSize(25,25));
	m_rect[BTN_BAR_FOCUS_FF		]	= CRect(CPoint(rect.right-  44, 29)	,	CSize(25,25));
	m_rect[BTN_BAR_FOCUS_STATUS	]	= CRect(CPoint(rect.left + 10, 10)	,	CSize(10,10));

	
	m_rect[BTN_BAR_SHIFT_LEFT		]	= CRect(CPoint(rect.right - (rect.right/2 + 67), 3)	,	CSize(42,30));
	m_rect[BTN_BAR_SHIFT_RIGHT	]	= CRect(CPoint(rect.right - (rect.right/2 + 22), 3)	,	CSize(42,30));
	m_rect[BTN_BAR_GRID				]	= CRect(CPoint(rect.right - (rect.right/2 - 23), 3)	,	CSize(42,30));

	//-- Draw Background		
	nImgX		= rect.left;	
	nImgY		= rect.top;
	nImgH	= rect.Height();			
	nImgW	= rect.Width();

	//-- 2011-8-16 Lee JungTaek
	DrawImage(&mDC, RSGetRoot(_T("img\\03.Live View\\03_menu_bg_left.png"))	,nImgX, nImgY, 1, nImgH);		
	DrawImage(&mDC, RSGetRoot(_T("img\\03.Live View\\03_menu_bg_center.png")),nImgX + 1, nImgY, nImgW - 2, nImgH);	
	DrawImage(&mDC, RSGetRoot(_T("img\\03.Live View\\03_menu_bg_right.png")),nImgX + nImgW - 1, nImgY, 1, nImgH);

	//-- Draw Buttons
	DrawItem(&mDC, BTN_BAR_SHIFT_LEFT			);
	DrawItem(&mDC, BTN_BAR_SHIFT_RIGHT			);
	//dh0.seo 2014-11-14 주석
//	DrawItem(&mDC, BTN_BAR_ZOOM				);
	DrawItem(&mDC, BTN_BAR_GRID					);
	//dh0.seo 2014-11-12 주석
/*	DrawItem(&mDC, BTN_BAR_FOCUS_REW		);
	DrawItem(&mDC, BTN_BAR_FOCUS_PREV	);
	DrawItem(&mDC, BTN_BAR_FOCUS_BODY		);
	DrawItem(&mDC, BTN_BAR_FOCUS_POINT	);
	DrawItem(&mDC, BTN_BAR_FOCUS_NEXT		);	
	DrawItem(&mDC, BTN_BAR_FOCUS_FF			);	*/
	DrawItem(&mDC, BTN_BAR_FOCUS_STATUS		);

	//-- Draw Text		
//	DrawStr(&mDC, STR_SIZE_TOOLBAR, _T("Menu"),rect.right - 370, 6, RGB(25,25,25));
//	DrawStr(&mDC, STR_SIZE_MIDDLE, _T("Menu"),rect.right - 370, 6, RGB(25,25,25));
//	DrawStr(&mDC, STR_SIZE_TOOLBAR, _T("Focus"),rect.right - 155, 6, RGB(25,25,25));	//dh0.seo 2014-11-12 주석

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	//Invalidate(FALSE);
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);	
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveviewToolbarDlg::DrawItem(CDC* pDC, int nItem)
{
	int nLeft = 0;	
	switch(nItem)
	{
	case BTN_BAR_SHIFT_LEFT:
		if(m_nStatus[nItem] == PT_BAR_DOWN)			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_btn_push.png"))		,m_rect[nItem]); 
		else if(m_nStatus[nItem] == PT_BAR_INSIDE)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_btn_over.png"))		,m_rect[nItem]); 
		else													DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_btn_nor.png"))		,m_rect[nItem]); 
		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_icon_rotate_L.png"))				,m_rect[nItem]); 		
		break;
	case BTN_BAR_SHIFT_RIGHT:
		if(m_nStatus[nItem] == PT_BAR_DOWN)			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_btn_push.png"))		,m_rect[nItem]); 
		else if(m_nStatus[nItem] == PT_BAR_INSIDE)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_btn_over.png"))		,m_rect[nItem]); 
		else													DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_btn_nor.png"))		,m_rect[nItem]); 		
		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_icon_rotate_R.png"))				,m_rect[nItem]); 		
		break;
	case BTN_BAR_ZOOM:
		if(m_nStatus[nItem] == PT_BAR_DOWN)			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_btn_push.png"))		,m_rect[nItem]); 
		else if(m_nStatus[nItem] == PT_BAR_INSIDE)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_btn_over.png"))		,m_rect[nItem]); 
		else													DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_btn_nor.png"))		,m_rect[nItem]); 		
		switch(((CLiveviewDlg*)GetParent())->GetZoomOpt())
			{
			case ZOOM_OFF	: DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_icon_closeup.png"))				,m_rect[nItem]); 		break;
			case ZOOM_1_2	: DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_icon_closeupx2.png"))				,m_rect[nItem]); 		break;
			case ZOOM_1_3	: DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_icon_closeupx3.png"))				,m_rect[nItem]); 		break;
			case ZOOM_1_4	: DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_icon_closeupx4.png"))				,m_rect[nItem]); 		break;			
			}		
		break;
	case BTN_BAR_GRID:
		if(m_nStatus[nItem] == PT_BAR_DOWN)			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_btn_push.png"))		,m_rect[nItem]); 
		else if(m_nStatus[nItem] == PT_BAR_INSIDE)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_btn_over.png"))		,m_rect[nItem]); 
		else													DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_btn_nor.png"))		,m_rect[nItem]); 
		switch(((CLiveviewDlg*)GetParent())->GetGridOpt())
			{
			case GRID_NULL	 : DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_icon_grid_off.png"))				,m_rect[nItem]); 		break;
			case GRID_2X2	 : DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_icon_grid_2x2.png"))				,m_rect[nItem]); 		break;
			case GRID_3X3	 : DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_icon_grid_3x3.png"))				,m_rect[nItem]); 		break;
			case GRID_7X7	 : DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_icon_grid_7x7.png"))				,m_rect[nItem]); 		break;
			case GRID_PLUS	 : DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_icon_grid_plus.png"))			,m_rect[nItem]); 		break;
			case GRID_X		 : DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_menu_icon_grid_X.png"))				,m_rect[nItem]); 		break;
			}		
		break;
	case BTN_BAR_FOCUS_REW	:
		//-- Check MF/AF
		if(!IsMF())
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_rew_disable.png")),m_rect[nItem]); 
		else
		{
			if(m_nStatus[nItem] == PT_BAR_DOWN)			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_rew_push.png"))		,m_rect[nItem]); 
			else if(m_nStatus[nItem] == PT_BAR_INSIDE)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_rew_focus.png"))		,m_rect[nItem]); 
			else													DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_rew_normal.png"))		,m_rect[nItem]); 
		}
		break;
	case BTN_BAR_FOCUS_PREV:
		//-- Check MF/AF
		if(!IsMF())
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_prev_disable.png")),m_rect[nItem]); 
		else
		{
			if(m_nStatus[nItem] == PT_BAR_DOWN)			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_prev_push.png"))		,m_rect[nItem]); 
			else if(m_nStatus[nItem] == PT_BAR_INSIDE)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_prev_focus.png"))		,m_rect[nItem]); 
			else													DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_prev_normal.png"))		,m_rect[nItem]); 
		}
		break;
	case BTN_BAR_FOCUS_BODY:
		//if(!IsMF())		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_focus_progress_bg_disable.png"))	,m_rect[nItem]); 
		//else			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_focus_progress_bg_nor.png"))		,m_rect[nItem]);
		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_bg.png"))		,m_rect[nItem]);
		break;
	case BTN_BAR_FOCUS_POINT:
		{
			//-- 2011-07-07 hongsu.jung
			//-- avoid zero devision
			int nDivision = (m_tFocusPosition.max - m_tFocusPosition.min);
			if(nDivision == 0)
				break;
			//-- Change Point
			int nPercentage = 100 * (m_tFocusPosition.max - m_tFocusPosition.current) / nDivision;

			if(nPercentage < 3)			m_rect[nItem].left	= m_rect[BTN_BAR_FOCUS_BODY].right -10;
			else if (nPercentage >97)	m_rect[nItem].left	= m_rect[BTN_BAR_FOCUS_BODY].left + 2;
			else						m_rect[nItem].left	= m_rect[BTN_BAR_FOCUS_BODY].left + m_rect[BTN_BAR_FOCUS_BODY].Width()/10 + m_rect[BTN_BAR_FOCUS_BODY].Width() * (100-nPercentage) / 100 * 0.9 - 9;
			m_rect[nItem].right	= m_rect[nItem].left	 + 8;
			//if(!IsMF())	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_bar.png"))			,m_rect[nItem]); 
			//else		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_bar.png"))				,m_rect[nItem]);
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_bar.png")),m_rect[nItem]);
		}
		break;
	case BTN_BAR_FOCUS_NEXT:
		if(!IsMF())
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_next_disable.png"))		,m_rect[nItem]); 
		else
		{
			if(m_nStatus[nItem] == PT_BAR_DOWN)			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_next_push.png"))		,m_rect[nItem]); 
			else if(m_nStatus[nItem] == PT_BAR_INSIDE)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_next_focus.png"))		,m_rect[nItem]); 
			else													DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_next_normal.png"))		,m_rect[nItem]); 
		}
		break;
	case BTN_BAR_FOCUS_FF:
		if(!IsMF())
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_ff_disable.png"))		,m_rect[nItem]); 
		else
		{
			if(m_nStatus[nItem] == PT_BAR_DOWN)			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_ff_push.png"))		,m_rect[nItem]); 
			else if(m_nStatus[nItem] == PT_BAR_INSIDE)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_ff_focus.png"))		,m_rect[nItem]); 
			else													DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_ff_normal.png"))		,m_rect[nItem]); 
		}
		break;
	case BTN_BAR_FOCUS_STATUS:
		if(m_nAFStatus == 1)			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_Set.png"))		,m_rect[nItem]); 
		else if(m_nAFStatus == 2)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_fail.png"))		,m_rect[nItem]); 
		else													DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_normal.png"))		,m_rect[nItem]); 
		break;
	}
	Invalidate(FALSE);
}

void CLiveviewToolbarDlg::SetAFFocus(int nValue)
{
	switch(nValue)
	{
	case AF_DETECT:				m_nAFStatus = 1; 	break;
	case AF_FAIL:				m_nAFStatus = 2;	break;
	case AF_CANCEL:				
	case AF_FAIL_CONTINOUSAF:	m_nAFStatus = 0;	break;
		//case AF_NULL:				m_colorAF = Color(0,0,0,0);		break;
	default:					m_nAFStatus = 0;	break;
	}
	Invalidate(FALSE);
}
//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CLiveviewToolbarDlg::RedrawIcon(int nItem)
{
	CPaintDC dc(this);	

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, m_rect[nItem].Width(), m_rect[nItem].Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,m_rect[nItem].Width(), m_rect[nItem].Height(), WHITENESS);

	DrawItem(&mDC, nItem);

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, m_rect[nItem].Width(), m_rect[nItem].Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CLiveviewToolbarDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	m_nMouseIn = PtInRect(point, TRUE);

	//-- MOUSE LEAVE
	MouseEvent();

	//-- 2011-8-28 Lee JungTaek
	//-- Add ToolTip
	if(m_nToolTipBtn != m_nMouseIn)
		m_ToolTip.Pop();	
	//-- Half Shutter
	if(m_nMouseIn > BTN_BAR_NULL && m_nMouseIn < BTN_BAR_CNT )
	{
		CString strToolTip;
		switch(m_nMouseIn)
		{
		case BTN_BAR_SHIFT_LEFT	:	strToolTip.Format(_T("Rotate Left"));	break;
		case BTN_BAR_SHIFT_RIGHT:	strToolTip.Format(_T("Rotate Right"));	break;	
		case BTN_BAR_ZOOM		:	strToolTip.Format(_T("Zoom"));			break;
		case BTN_BAR_GRID		:	strToolTip.Format(_T("Grid"));			break;
		case BTN_BAR_FOCUS_REW	:	strToolTip.Format(_T("Nearer"));		break;
		case BTN_BAR_FOCUS_PREV	:	strToolTip.Format(_T("Near"));		break;
		case BTN_BAR_FOCUS_NEXT	:	strToolTip.Format(_T("Far"));		break;
		case BTN_BAR_FOCUS_FF	:	strToolTip.Format(_T("Farther"));		break;
		}

		m_nToolTipBtn = m_nMouseIn;
		m_ToolTip.AddTool(this, (LPCTSTR)strToolTip, NULL, 0);
//		m_ToolTip.Activate(TRUE);
	}
	CDialog::OnMouseMove(nFlags, point);
}

void CLiveviewToolbarDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CLiveviewToolbarDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_BAR_CNT; i ++)
		ChangeStatus(i,PT_BAR_NOR);
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CLiveviewToolbarDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	
	switch(nItem)
	{
	case BTN_BAR_SHIFT_LEFT:		
		//-- 2011-07-15 hongsu.jung
		//-- Check Liveview On
		if(!((CNXRemoteStudioDlg*)((CLiveviewDlg*)GetParent())->GetParent())->IsLiveviewOn())
			return;
		ShiftLeft();		
		break;
	case BTN_BAR_SHIFT_RIGHT:		
		if(!((CNXRemoteStudioDlg*)((CLiveviewDlg*)GetParent())->GetParent())->IsLiveviewOn())
			return;
		ShiftRight();	
		break;
	case BTN_BAR_ZOOM:				
		((CNXRemoteStudioDlg*)((CLiveviewDlg*)GetParent())->GetParent())->PopupList(CMB_ZOOM,point);		
		break;
	case BTN_BAR_GRID:				
		((CNXRemoteStudioDlg*)((CLiveviewDlg*)GetParent())->GetParent())->PopupList(CMB_GRID,point);		
		break;
	//-- 2011-6-24 Lee JungTaek
	case BTN_BAR_FOCUS_REW	:		if(IsMF())	{ SetFocusPosition(FOCUS_REW	);	m_nPressed = nItem;	SetTimer(timer_button_pressed, wait_pressed_time, NULL);	}	break;
	case BTN_BAR_FOCUS_PREV	:		if(IsMF())	{ SetFocusPosition(FOCUS_PREV	);	m_nPressed = nItem;	SetTimer(timer_button_pressed, wait_pressed_time, NULL);	}	break;
	case BTN_BAR_FOCUS_NEXT	:		if(IsMF())	{ SetFocusPosition(FOCUS_NEXT);	m_nPressed = nItem;	SetTimer(timer_button_pressed, wait_pressed_time, NULL);		}	break;
	case BTN_BAR_FOCUS_FF		:		if(IsMF())	{ SetFocusPosition(FOCUS_FF	);	m_nPressed = nItem;	SetTimer(timer_button_pressed, wait_pressed_time, NULL);		}	break;	
	}
	CDialog::OnLButtonDown(nFlags, point);	
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CLiveviewToolbarDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ChangeStatusAll();	
	//-- 2011-08-02 hongsu.jung
	m_nPressed = BTN_BAR_NULL;
	CDialog::OnLButtonUp(nFlags, point);
}

//-- 2011-05-20 hongsu.jung
void CLiveviewToolbarDlg::ShiftLeft()	
{
	RSEvent* pMsg		= new RSEvent();
	pMsg->message	= WM_LIVEVIEW_EVENT_SHIFT;
	pMsg->nParam1		= SHIFT_LEFT;
	::SendMessageW(GetParent()->m_hWnd,WM_LIVEVIEW,(WPARAM)pMsg,NULL);	
}

void CLiveviewToolbarDlg::ShiftRight()	
{
	RSEvent* pMsg		= new RSEvent();
	pMsg->message	= WM_LIVEVIEW_EVENT_SHIFT;
	pMsg->nParam1		= SHIFT_RIGHT;
	::SendMessageW(GetParent()->m_hWnd,WM_LIVEVIEW,(WPARAM)pMsg,NULL);	
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-6-24
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CLiveviewToolbarDlg::SetFocusPosition(int nType)
{
	RSEvent* pMsg	= new RSEvent;
	pMsg->message	= WM_LIVEVIEW_EVENT_SET_FOCUS;
	pMsg->nParam1	= nType;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CLiveviewToolbarDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = PT_BAR_IN_NOTHING;
	int nStart = 0;
	for(int i = nStart ; i < BTN_BAR_CNT; i ++)
	{
		if(i == BTN_BAR_FOCUS_BODY || i == BTN_BAR_FOCUS_POINT)
			continue;
		if(!IsMF())
			if( i >= BTN_BAR_FOCUS_REW && i <= BTN_BAR_FOCUS_FF )
				continue;

		if(m_rect[i].PtInRect(pt))
		{
			nReturn = i;
			if(bMove)
				ChangeStatus(i,PT_BAR_INSIDE);
			else
				ChangeStatus(i,PT_BAR_DOWN); 
		}
		else
			ChangeStatus(i,PT_BAR_NOR); 
	}
	return nReturn;
}
//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CLiveviewToolbarDlg::ChangeStatus(int nItem, int nStatus)
{
	if(m_nStatus[nItem] != nStatus)
	{
		m_nStatus[nItem] = nStatus;
		CPaintDC dc(this);

		//-- 2011-08-02 hongsu.jung
		// Double Buffering
		CDC mDC;
		CBitmap mBitmap;		
		mDC.CreateCompatibleDC(&dc);
		mBitmap.CreateCompatibleBitmap(&dc, m_rect[nItem].Width(), m_rect[nItem].Height()); 
		mDC.SelectObject(&mBitmap);
		mDC.PatBlt(0,0,m_rect[nItem].Width(), m_rect[nItem].Height(), WHITENESS);

		DrawItem(&mDC, nItem);		

		//-- 2011-08-02 hongsu.jung
		// Double Buffering
		dc.BitBlt(0, 0, m_rect[nItem].Width(), m_rect[nItem].Height(), &mDC, 0, 0, SRCCOPY);	
		InvalidateRect(m_rect[nItem]);
	}
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CLiveviewToolbarDlg::ChangeStatusAll()
{
	int nStart = 0;
	for(int i = nStart; i < BTN_BAR_CNT; i ++)
	{
		if(m_nMouseIn == i)
			ChangeStatus(i,PT_BAR_INSIDE); 
		else
			ChangeStatus(i,PT_BAR_NOR); 
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-27
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
BOOL CLiveviewToolbarDlg::IsMF()
{
	CNXRemoteStudioDlg* pParent = NULL;
	pParent = (CNXRemoteStudioDlg*)((CLiveviewDlg*)GetParent())->GetParent();
	if(pParent)
		return pParent->m_pCommander->IsMF();
	return FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief		GetFocus
//! @date		2011-07-06
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveviewToolbarDlg::GetFocus()
{
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);	
	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	SetFocusPosition(FOCUS_GET);	 
	for(int nItem = BTN_BAR_FOCUS_BODY; nItem <= BTN_BAR_FOCUS_FF; nItem ++)		
	{		
		DrawItem(&mDC, nItem);		
		InvalidateRect(m_rect[nItem],FALSE);	
	}

	UpdateScroll();
	UpdateWindow();

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);	
}
//------------------------------------------------------------------------------ 
//! @brief		SetFocus
//! @date		2011-07-06
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveviewToolbarDlg::SetFocus(FocusPosition* pFocus)
{
	CPaintDC dc(this);

	CRect rect;
	GetClientRect(&rect);	
	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	
	//-- Set Focus Position
	m_tFocusPosition.current	= pFocus->current	; 
	m_tFocusPosition.max		= pFocus->max		; 
	m_tFocusPosition.min		= pFocus->min		; 

	TRACE(_T("Set Focus [ %04d > %04d > %04d ]\n"),	m_tFocusPosition.min,	m_tFocusPosition.current	,	m_tFocusPosition.max			); 	
	for(int nItem = BTN_BAR_FOCUS_BODY; nItem <= BTN_BAR_FOCUS_FF; nItem ++)	
	{		
		DrawItem(&dc, nItem);
		InvalidateRect(m_rect[nItem],FALSE);	
	}	

	UpdateScroll();	
	UpdateWindow();		

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);	
}

void CLiveviewToolbarDlg::UpdateScroll()
{
	CRect rect;
	rect.CopyRect(m_rect[BTN_BAR_FOCUS_BODY]);	
	rect.top -= 8;
	rect.bottom += 8;
	InvalidateRect(rect,FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-07-14
//! @author	hongsu.jung
//---------------------------------------------------------------------------
BOOL CLiveviewToolbarDlg::PreTranslateMessage(MSG* pMsg) 
{
	//if(pMsg->message == WM_MOUSEMOVE)
	m_ToolTip.RelayEvent(pMsg);
	return CDialog::PreTranslateMessage(pMsg);
}
