/////////////////////////////////////////////////////////////////////////////
//
//  OptionPCSet1Dlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "OptionBaseDlg.h"
#include "NXRemoteStudioFunc.h"

#define SETDIALOG_ITEM_VIEW_SIZE 2

// COptionPCSet1Dlg dialog
class COptionPCSet1Dlg : public COptionBaseDlg
{
	DECLARE_DYNAMIC(COptionPCSet1Dlg)	
public:
	COptionPCSet1Dlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionPCSet1Dlg();

// Dialog Data
	enum { IDD = IDD_DLG_OPT_PC_SET1 };

protected:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);	
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);	
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	afx_msg void OnPictureDownloadFilePathFolderOpen();		
	afx_msg void OnLoadNSaveBankFilePathFolderOpen();	
	afx_msg void OnPictureFolderOpen();	
	afx_msg void OnCancel();	
	afx_msg void OnAppy();	
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
	DECLARE_MESSAGE_MAP()
public:
	void UpdateFrames();
private:	
	//-- 2011-07-15 hongsu.jung
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();	
	int PtInRect(CPoint pt, BOOL bMove);
	void ResetButton();
	CString GetDir(BOOL bPicPath);

public:
	void InitProp(RSPCSet1& opt);
	void SaveProp(RSPCSet1& opt);

	CRect	m_rectBtn[4];	
	int		m_nStatus[4];
	CEdit	m_edit[2];
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	CStatic * m_staticLabel[SETDIALOG_ITEM_VIEW_SIZE];
	CStatic* m_staticLabelBar[SETDIALOG_ITEM_VIEW_SIZE];
	CButton  m_PushBtn[5];

	CString m_strPictureRootPath;
	CString m_strBankPath;
	//-- Temp
	CString m_strTempPath;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};