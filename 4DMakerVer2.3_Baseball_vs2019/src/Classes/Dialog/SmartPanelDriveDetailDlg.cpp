/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelDriveDetailDlg.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-07-01
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudio.h"
#include "SmartPanelDriveDetailDlg.h"
#include "afxdialogex.h"
#include "NXRemoteStudioDlg.h"
#include "SmartPanelChildDlg.h"
#include "SmartPanelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define RECT_BAR	CRect(CPoint(114, 20), CSize(500, 130))

CSmartPanelDlg* m_pSmartPanel;
CSmartPanelChildDlg* m_pSmartPanelChild;

// CSmartPanelDriveDetailDlg dialog
IMPLEMENT_DYNAMIC(CSmartPanelDriveDetailDlg, CDialogEx)

	CSmartPanelDriveDetailDlg::CSmartPanelDriveDetailDlg(int nType, CWnd* pParent /*=NULL*/)
	: CBkDialogST(CSmartPanelDriveDetailDlg::IDD, pParent)
{
	m_nMouseIn = BTN_DRIVE_DETAIL_NULL;	

	for(int i=0; i<BTN_DRIVE_DETAIL_CNT - 1; i++)
	{
		m_nStatus[i] = PT_WB_NOR;
		m_rectBtn[i] = CRect(0,0,0,0);
	}

	m_nPTPCode		= 0;


	m_nDriveDetail = 0;
	m_nDriveDetail1st = 0;

	//NX1 dh0.seo 2014-09-02
	m_nAllValue = 0;
	m_nTimerValue = 0;
	m_nCountValue = 0;
	m_nIntervalValue = 0;
	//CMiLRe 20140922 PLM P140917-04607 - Reset 기능 추가
	m_bInit_Timer = TRUE;
	m_nTimerValue_1ST = 0;
	m_nCountValue_1ST = 0;
	m_nIntervalValue_1ST = 0;

	m_bInit = FALSE;	

	m_nType = nType;
}

CSmartPanelDriveDetailDlg::~CSmartPanelDriveDetailDlg()
{
}

void CSmartPanelDriveDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSmartPanelDriveDetailDlg, CBkDialogST)
	ON_WM_PAINT()	
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()


BOOL CSmartPanelDriveDetailDlg::OnInitDialog()
{
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	m_strModel = pMainWnd->GetModel();

	CRSChildDialog::OnInitDialog();		

	SetItemPosition();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSmartPanelDriveDetailDlg::SetItemPosition()
{
	if(!m_strModel.Compare(_T("NX1")))
	{
		//NX1 dh0.seo 2014-08-25
		m_rectBtn[BTN_DRIVE_DETAIL_RESET]	=	CRect(CPoint(8,175), CSize(55,25));
		m_rectBtn[BTN_DRIVE_DETAIL_OK	]	=	CRect(CPoint(294,175), CSize(55,25));

		m_rectBtn[BTN_DRIVE_DETAIL_MINUS1] = CRect(CPoint(60,120), CSize(20,20));
		m_rectBtn[BTN_DRIVE_DETAIL_MINUS2] = CRect(CPoint(60,70), CSize(20,20));
		m_rectBtn[BTN_DRIVE_DETAIL_MINUS3] = CRect(CPoint(60,20), CSize(20,20));

		m_rectBtn[BTN_DRIVE_DETAIL_PLUS1] = CRect(CPoint(320,120), CSize(20,20));
		m_rectBtn[BTN_DRIVE_DETAIL_PLUS2] = CRect(CPoint(320,70), CSize(20,20));
		m_rectBtn[BTN_DRIVE_DETAIL_PLUS3] = CRect(CPoint(320,20), CSize(20,20));

		m_rectInterval = CRect(CPoint(90,20), CSize(220,20));
		m_rectCount = CRect(CPoint(90,70), CSize(220,20));
		m_rectTime = CRect(CPoint(90,120), CSize(220,20));
	}
	else
	{
		//-- Icon
		m_rectIcon		=	CRect(CPoint(12,21), CSize(24,24)) ;
		//-- Value
		m_rectValue		=	CRect(CPoint(32,21), CSize(44,24)) ;
		//-- Bar
		m_rectBar		=	CRect(CPoint(114,25), CSize(197,16)) ;
		//-- Handle
		m_rectHandler	=	CRect(CPoint(130,22), CSize(12,28)) ;
		//-- Button
		m_rectBtn[BTN_DRIVE_DETAIL_LEFT]	=	CRect(CPoint(80,21), CSize(25,24));
		m_rectBtn[BTN_DRIVE_DETAIL_RIGHT]	=	CRect(CPoint(320,21), CSize(25,24));
		m_rectBtn[BTN_DRIVE_DETAIL_RESET]	=	CRect(CPoint(8,68), CSize(55,25));
		m_rectBtn[BTN_DRIVE_DETAIL_OK	]	=	CRect(CPoint(294,68), CSize(55,25));
	}
}

void CSmartPanelDriveDetailDlg::OnPaint()
{	
	DrawBackground();	
	CRSChildDialog::OnPaint();
}

void CSmartPanelDriveDetailDlg::CleanDC(CDC* pDC)
{
	for(int i = 0; i < BTN_DRIVE_DETAIL_CNT - 1; i++)
	{
		if(i < BTN_DRIVE_DETAIL_CNT - 2)
		{
			CBrush brush = RGB_BLACK;
			pDC->FillRect(m_rectBtn[i], &brush);
		}
		else
		{
			CBrush brush = RGB_DEFAULT_DLG;
			pDC->FillRect(m_rectBtn[i], &brush);
		}
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::DrawBackground()
{
	CPaintDC dc(this);
	

	CRect rect;
	GetClientRect(&rect);

	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- Draw Round Dialog
	DrawRoundDialogs(&mDC, TRUE);
	
	//NX1 dh0.seo 2014-09-01
	if(!m_strModel.Compare(_T("NX1")))
	{
		DrawImageBase(&mDC, RSGetRoot(_T("img\\02.Smart Panel\\02_pw_adjust_bg.png")),0, 0);
		DrawLine(&mDC);
		DrawMenu(BTN_DRIVE_DETAIL_PLUS1, &mDC);
		DrawMenu(BTN_DRIVE_DETAIL_PLUS2, &mDC);
		DrawMenu(BTN_DRIVE_DETAIL_PLUS3, &mDC);
		DrawMenu(BTN_DRIVE_DETAIL_MINUS1, &mDC);
		DrawMenu(BTN_DRIVE_DETAIL_MINUS2, &mDC);
		DrawMenu(BTN_DRIVE_DETAIL_MINUS3, &mDC);
		DrawMenu(BTN_DRIVE_DETAIL_RESET, &mDC);
		DrawMenu(BTN_DRIVE_DETAIL_OK, &mDC);
	}
	else
	{
		DrawImageBase(&mDC, RSGetRoot(_T("img\\02.Smart Panel\\02_adjust_bg.png")),0, 0);
		//-- Draw Line
		DrawLine(&mDC);

		//-- Draw Menu
		DrawMenu(BTN_DRIVE_DETAIL_LEFT, &mDC);
		DrawMenu(BTN_DRIVE_DETAIL_RIGHT, &mDC);
		DrawMenu(BTN_DRIVE_DETAIL_RESET, &mDC);
		DrawMenu(BTN_DRIVE_DETAIL_OK, &mDC);
	}
	

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawLine / DrawIcon / DrawValue / DrawBar / DrawBarHandler
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Main Camera / Name / Mode / Check Box
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::DrawLine(CDC* pDC)
{
	switch(m_nType)
	{
	case DRIVE_DETAIL_TYPE_BURST	:	DrawLineBurst(pDC);		break;
	case DRIVE_DETAIL_TYPE_TIMER	:	DrawLineTimer(pDC);		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawLineBurst
//! @date		2011-8-16
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::DrawLineBurst(CDC* pDC)
{
	//NX1 dh0.seo 2014-09-01
	if(!m_strModel.Compare(_T("NX1")))
	{
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Timer"), 12, 120 ,RGB(255,255,255));
		DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_bar_bg_01.png")), m_rectTime);
		DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\03.Live View\\03_focus_progress_handle_over.png")), m_rectTimeHandler);
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("2"), 85, 140, RGB(255,255,255));
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("16"), 190, 140, RGB(255,255,255));
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("30"), 300, 140, RGB(255,255,255));

		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Count"), 12, 70 ,RGB(255,255,255));
		DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_bar_bg_03.png")), m_rectCount);
		DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\03.Live View\\03_focus_progress_handle_over.png")), m_rectCountHandler);
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("1"), 85, 90, RGB(255,255,255));
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("5"), 183, 90, RGB(255,255,255));
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("10"), 300, 90, RGB(255,255,255));

		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Interval"), 12, 20 ,RGB(255,255,255));
		DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_bar_bg_02.png")), m_rectInterval);
		DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\03.Live View\\03_focus_progress_handle_over.png")), m_rectIntervalHandler);
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("0.5"), 85, 40, RGB(255,255,255));
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("1"), 158, 40, RGB(255,255,255));
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("1.5"), 227, 40, RGB(255,255,255));
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("2"), 300, 40, RGB(255,255,255));
	}
	else
	{
		//-- Draw Icon
		DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_adjust_icon_burst.png")), m_rectIcon);
		//-- Draw Value
		CString strValue = _T("");
		strValue.Format(_T("%d"), m_nDriveDetail);
		DrawStr(pDC, STR_SIZE_TEXT_BOLD, strValue, 44, 26, RGB(255,255,255));
		//-- Draw Bar
		DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_adjust_bar_burst.png")), m_rectBar);
		//-- Draw Handler
		DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\03.Live View\\03_focus_progress_handle_over.png")), m_rectHandler);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawLineTimer
//! @date		2011-8-16
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::DrawLineTimer(CDC* pDC)
{
	//-- Draw Icon
	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_adjust_icon_timer.png")), m_rectIcon);
	//-- Draw Value
	CString strValue = _T("");
	strValue.Format(_T("%dsec"), m_nDriveDetail);
	DrawStr(pDC, STR_SIZE_MIDDLE, strValue, 37, 27 ,RGB(255,255,255));
	//-- Draw Bar
	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_adjust_bar_timer_user.png")), m_rectBar);
	//-- Draw Handler
	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\03.Live View\\03_focus_progress_handle_over.png")), m_rectHandler);

	//-- Draw Value
	DrawStr(pDC, STR_SIZE_MIDDLE, _T("2"), 138, 48 ,RGB(255,255,255));
	DrawStr(pDC, STR_SIZE_MIDDLE, _T("15"), 201, 48 ,RGB(255,255,255));
	DrawStr(pDC, STR_SIZE_MIDDLE, _T("30"), 275, 48 ,RGB(255,255,255));	
}

//------------------------------------------------------------------------------ 
//! @brief		DrawMenu
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Draw Buttons
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::DrawMenu(int nItem, CDC* pDC)
{
	int nX, nY, nW, nH;
	nX	= m_rectBtn[nItem].left;	
	nY	= m_rectBtn[nItem].top;
	nW	= m_rectBtn[nItem].Width();
	nH	= m_rectBtn[nItem].Height();

	COLORREF color;

	switch(nItem)
	{
	case BTN_DRIVE_DETAIL_LEFT:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_focus.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_push.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_disable.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		default	:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_normal.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		}
		break;
	case BTN_DRIVE_DETAIL_RIGHT:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_focus.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_push.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_disable.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_normal.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		}
		break;	
	case BTN_DRIVE_DETAIL_RESET:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_push.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_disable.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(219,219,219); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Reset"),m_rectBtn[nItem],color);	
		break;
	case BTN_DRIVE_DETAIL_OK:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_push.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_disable.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(219,219,219); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("OK "),m_rectBtn[nItem],color);	
		break;
	case BTN_DRIVE_DETAIL_PLUS1:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn2_plus_sel.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_plus_dim.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_plus_dim.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\03_pz_bar_btn_plus_nor.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(219,219,219); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("OK "),m_rectBtn[nItem],color);	
		break;
	case BTN_DRIVE_DETAIL_PLUS2:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn2_plus_sel.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_plus_dim.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_plus_dim.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\03_pz_bar_btn_plus_nor.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(219,219,219); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("OK "),m_rectBtn[nItem],color);	
		break;
	case BTN_DRIVE_DETAIL_PLUS3:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn2_plus_sel.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_plus_dim.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_plus_dim.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\03_pz_bar_btn_plus_nor.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(219,219,219); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("OK "),m_rectBtn[nItem],color);	
		break;
	case BTN_DRIVE_DETAIL_MINUS1:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn2_minus_sel.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_minus_dim.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_minus_dim.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\03_pz_bar_btn_minus_nor.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(219,219,219); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("OK "),m_rectBtn[nItem],color);	
		break;
	case BTN_DRIVE_DETAIL_MINUS2:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn2_minus_sel.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_minus_dim.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_minus_dim.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\03_pz_bar_btn_minus_nor.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(219,219,219); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("OK "),m_rectBtn[nItem],color);	
		break;
	case BTN_DRIVE_DETAIL_MINUS3:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn2_minus_sel.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_minus_dim.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_minus_dim.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\03_pz_bar_btn_minus_nor.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(219,219,219); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("OK "),m_rectBtn[nItem],color);	
		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	m_nMouseIn = PtInRect(point, TRUE);
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

void CSmartPanelDriveDetailDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CSmartPanelDriveDetailDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_DRIVE_DETAIL_CNT; i ++)
		ChangeStatus(i,PT_WB_NOR);
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	
	
	switch(m_nType)
	{
	case DRIVE_DETAIL_TYPE_BURST	:		
		{
			switch(nItem)
			{
			case BTN_DRIVE_DETAIL_LEFT			:		SetBurstValue(FALSE);		break;
			case BTN_DRIVE_DETAIL_RIGHT			:		SetBurstValue(TRUE);		break;
			case BTN_DRIVE_DETAIL_PLUS1			:		SetTimer(TRUE);				break;
			case BTN_DRIVE_DETAIL_PLUS2			:		SetCount(TRUE);				break;
			case BTN_DRIVE_DETAIL_PLUS3			:		SetInterval(TRUE);			break;
			case BTN_DRIVE_DETAIL_MINUS1		:		SetTimer(FALSE);			break;
			case BTN_DRIVE_DETAIL_MINUS2		:		SetCount(FALSE);			break;
			case BTN_DRIVE_DETAIL_MINUS3		:		SetInterval(FALSE);			break;
			case BTN_DRIVE_DETAIL_RESET			:		ResetValue();	break;
			case BTN_DRIVE_DETAIL_OK			:		((CSmartPanelChildDlg*)GetParent())->CloseSmartPanelChild();				return;
			default:								if(SetPositionBurst(point))		SetDetailValue();		return;	//-- Check Enable Rect
			}
		}
		break;
	case DRIVE_DETAIL_TYPE_TIMER	:				
		{
			switch(nItem)
			{
			case BTN_DRIVE_DETAIL_LEFT			:		SetTimerValue(FALSE);		break;
			case BTN_DRIVE_DETAIL_RIGHT			:		SetTimerValue(TRUE);		break;
			case BTN_DRIVE_DETAIL_RESET			:		ResetValue();	break;
			case BTN_DRIVE_DETAIL_OK			:		((CSmartPanelChildDlg*)GetParent())->CloseSmartPanelChild();				return;
			default:								if(SetPositionTimer(point))		SetDetailValue();		return;	//-- Check Enable Rect
			}
		}
		break;
	}

	if(m_strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)	
		SetDetailValueBust(nItem);
	else
		SetDetailValue();

	CDialog::OnLButtonDown(nFlags, point);	
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ChangeStatusAll();	
	CDialog::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CSmartPanelDriveDetailDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = PT_WB_IN_NOTHING;
	int nStart = 0;

	for(int i = nStart ; i < BTN_DRIVE_DETAIL_CNT; i ++)
	{
		if(m_rectBtn[i].PtInRect(pt))
		{
			nReturn = i;
			if(bMove)
				ChangeStatus(i,PT_WB_INSIDE);
			else
				ChangeStatus(i,PT_WB_DOWN); 
		}
		else
			ChangeStatus(i,PT_WB_NOR); 
	}
	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::ChangeStatus(int nItem, int nStatus)
{
	if(m_nStatus[nItem] != nStatus)
	{
		m_nStatus[nItem] = nStatus;
		CPaintDC dc(this);
		DrawMenu(nItem, &dc);
		InvalidateRect(m_rectBtn[nItem],FALSE);
	}
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::ChangeStatusAll()
{
	int nStart = 0;
	for(int i = nStart; i < BTN_DRIVE_DETAIL_CNT; i ++)
	{
		if(m_nMouseIn == i)
			ChangeStatus(i,PT_WB_INSIDE); 
		else
			ChangeStatus(i,PT_WB_NOR); 
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		SetColorTempValue
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Set Values (Min:0 / Max:21)
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::SetBurstValue(BOOL bRight)
{
	if(bRight)
	{
		m_nDriveDetailIndex++;
		if(m_nDriveDetailIndex > 1)			m_nDriveDetailIndex = 2;
	}
	else
	{
		m_nDriveDetailIndex--;
		if(m_nDriveDetailIndex < 0)			m_nDriveDetailIndex = 0;
	}

	switch(m_nDriveDetailIndex)
	{
	case 0:	m_nDriveDetail = 10;	break;
	case 1:	m_nDriveDetail = 15;	break;
	case 2:	m_nDriveDetail = 30;	break;
	}
}

void CSmartPanelDriveDetailDlg::SetTimerValue(BOOL bRight)
{
	if(bRight)
	{
		m_nDriveDetailIndex++;
		if(m_nDriveDetailIndex > 27)			m_nDriveDetailIndex = 28;
	}
	else
	{
		m_nDriveDetailIndex--;
		if(m_nDriveDetailIndex < 0)			m_nDriveDetailIndex = 0;
	}

	m_nDriveDetail = m_nDriveDetailIndex + 2;
}

//------------------------------------------------------------------------------ 
//! @brief		SetColorTempValue
//! @date		2014-9-1
//! @author		dh0.seo
//! @note	 	Set Timer (Min:1 / Max:30)
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::SetTimer(BOOL bPlus)
{
	int Timer_X = 0, Timer_Y = 0;
	
	if(bPlus)
	{
		m_nTimerValue = m_nTimerValue + 1;
		if(m_nTimerValue > 30)
			m_nTimerValue = 30;
		Timer_X = 90 + (m_nTimerValue-2) * 7.5; Timer_Y = 118;
		m_rectTimeHandler =	CRect(CPoint(Timer_X, Timer_Y), CSize(12,28));		
	}
	else
	{
		m_nTimerValue = m_nTimerValue - 1;
		if(m_nTimerValue < 2)
			m_nTimerValue = 2;
		Timer_X = 90 + (m_nTimerValue-2) * 7.5; Timer_Y = 118;
		m_rectTimeHandler =	CRect(CPoint(Timer_X, Timer_Y), CSize(12,28));
	}
	Invalidate(FALSE);
	InvalidateRect(m_rectTimeHandler, FALSE);
}
void CSmartPanelDriveDetailDlg::SetCount(BOOL bPlus)
{
	int Count_X = 0, Count_Y = 0;

	if(bPlus)
	{
		m_nCountValue = m_nCountValue +1;
		if(m_nCountValue > 9)
			m_nCountValue = 10;
		Count_X = 90 + (m_nCountValue-1) * 23.4; Count_Y = 68;
		m_rectCountHandler = CRect(CPoint(Count_X, Count_Y), CSize(12,28));
	}
	else
	{
		m_nCountValue = m_nCountValue - 1;
		if(m_nCountValue < 2)
			m_nCountValue = 1;
		Count_X = 90 + (m_nCountValue-1) * 23.4; Count_Y = 68;
		m_rectCountHandler = CRect(CPoint(Count_X, Count_Y), CSize(12,28));
	}
	Invalidate(FALSE);
	InvalidateRect(m_rectCountHandler, FALSE);
}
void CSmartPanelDriveDetailDlg::SetInterval(BOOL bPlus)
{
	int Interval_X = 0, Interval_Y = 0;

	if(bPlus)
	{
		m_nIntervalValue = m_nIntervalValue + 1;
		if(m_nIntervalValue > 3)
			m_nIntervalValue = 3;
		Interval_X = 90 + m_nIntervalValue * 70;; Interval_Y = 18;
		m_rectIntervalHandler = CRect(CPoint(Interval_X, Interval_Y), CSize(12,28));
	}
	else
	{
		m_nIntervalValue = m_nIntervalValue - 1;
		if(m_nIntervalValue < 0)
			m_nIntervalValue = 0;
		Interval_X = 90 + m_nIntervalValue * 70;; Interval_Y = 18;
		m_rectIntervalHandler = CRect(CPoint(Interval_X, Interval_Y), CSize(12,28));
	}
	Invalidate(FALSE);
	InvalidateRect(m_rectIntervalHandler, FALSE);
}
//------------------------------------------------------------------------------ 
//! @brief		SetValueIndex
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Set First Values
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::SetValueIndex(int nValue)
{
	switch(m_nType)
	{
	case DRIVE_DETAIL_TYPE_BURST	:	
		if(m_strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)
		{
			SetValueIndexTimer_NX1(nValue);	break;
		}
		else
		{
			SetValueIndexBurst(nValue);		break;
		}
	case DRIVE_DETAIL_TYPE_TIMER	:	SetValueIndexTimer(nValue);		break;
	}
}

//NX1 dh0.seo 2014-09-02
void CSmartPanelDriveDetailDlg::SetValueIndexTimer_NX1(int nValue)
{
	m_nAllValue = nValue;
		
	m_nTimerValue = (m_nAllValue & 0xFF00)>>8;	
	m_nCountValue = (m_nAllValue & 0x00F0)>>4;
	if(m_nCountValue == 0)
		m_nCountValue = 1;
	m_nIntervalValue = m_nAllValue & 0x00F;	

	//CMiLRe 20140922 PLM P140917-04607 - Reset 기능 추가
	if(m_bInit_Timer)
	{
		m_nTimerValue_1ST = m_nTimerValue;
		m_nCountValue_1ST = m_nCountValue;
		m_nIntervalValue_1ST = m_nIntervalValue;
		m_bInit_Timer = FALSE;
	}
	SetHandler();
}

void CSmartPanelDriveDetailDlg::SetValueIndexBurst(int nValue)
{
	 //-- Set Value
	m_nDriveDetail = nValue;
	//-- Set Index
	switch(nValue)
	{
	case 10:	m_nDriveDetailIndex = 0;	break;
	case 15:	m_nDriveDetailIndex = 1;	break;
	case 30:	m_nDriveDetailIndex = 2;	break;
	}
	
	if(!m_bInit)
	{
		m_nDriveDetail1st	= m_nDriveDetail;
		m_bInit = TRUE;
	}
	
	//-- Set Handle Position And Redraw
	InvalidateRect(m_rectValue, FALSE);	
	SetHandler();
}

void CSmartPanelDriveDetailDlg::SetValueIndexTimer(int nValue)
{
	//-- Set Value
	m_nDriveDetail = nValue;
	//-- Set Index
	m_nDriveDetailIndex = nValue - 2;

	if(!m_bInit)
	{
		m_nDriveDetail1st	= m_nDriveDetail;
		m_bInit = TRUE;
	}

	//-- Set Handle Position And Redraw
	InvalidateRect(m_rectValue, FALSE);	
	SetHandler();
}

//------------------------------------------------------------------------------ 
//! @brief		SetHandler
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	Set Bar Handler Position
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::SetHandler()
{
	switch(m_nType)
	{
	case DRIVE_DETAIL_TYPE_BURST	:
		if(m_strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)
		{
			setHandlerTimer_NX1(); break;
		}
		else
		{
			SetHandlerBurst();		break;
		}
	case DRIVE_DETAIL_TYPE_TIMER	:	SetHandlerTimer();		break;
	}
}

//NX1 dh0.seo 2014-09-02
void CSmartPanelDriveDetailDlg::setHandlerTimer_NX1()
{
	int Timer_X = 0, Timer_Y = 0;
	int Count_X = 0, Count_Y = 0;
	int Interval_X = 0, Interval_Y = 0;

	Timer_X = 90 + (m_nTimerValue-2) * 7.5; Timer_Y = 118;
	Count_X = 90 + (m_nCountValue-1) * 23.4; Count_Y = 68;
	Interval_X = 90 + m_nIntervalValue * 70;; Interval_Y = 18;

	m_rectTimeHandler =	CRect(CPoint(Timer_X, Timer_Y), CSize(12,28));
	m_rectCountHandler = CRect(CPoint(Count_X, Count_Y), CSize(12,28));
	m_rectIntervalHandler = CRect(CPoint(Interval_X, Interval_Y), CSize(12,28));

	if(this->GetSafeHwnd())
	{
		//CMiLRe 20140903 Driver Timer Setting
		Invalidate(FALSE);
		InvalidateRect(m_rectTimeHandler, FALSE);
		InvalidateRect(m_rectCountHandler, FALSE);
		InvalidateRect(m_rectIntervalHandler, FALSE);
	}
}

void CSmartPanelDriveDetailDlg::SetHandlerBurst()
{
	int nX = 0, nY = 0;

	nX = 130 + m_nDriveDetailIndex * 78;	nY = 22;
	m_rectHandler	=  CRect(CPoint(nX,nY), CSize(12,28));

 	//-- Update Image
 	if(this->GetSafeHwnd())		
 		InvalidateRect(RECT_BAR, FALSE);
}
	
void CSmartPanelDriveDetailDlg::SetHandlerTimer()
{
	int nX = 0, nY = 0;

	nX = 140 + m_nDriveDetailIndex * 5;	nY = 22;
	m_rectHandler	=  CRect(CPoint(nX,nY), CSize(12,28));

	//-- Update Image
	if(this->GetSafeHwnd())		
		InvalidateRect(RECT_BAR, FALSE);
}


//------------------------------------------------------------------------------ 
//! @brief		SetPosition
//! @date		2011-7-29
//! @author		Lee JungTaek
//! @note	 	Set Bar Event
//------------------------------------------------------------------------------ 
BOOL CSmartPanelDriveDetailDlg::SetPositionBurst(CPoint point)
{
	if(m_rectBar.PtInRect(point))
	{
		m_nDriveDetailIndex = (point.x - 130) / 78;

		//-- Check Index
		if(m_nDriveDetailIndex > 1)			m_nDriveDetailIndex = 2;
		else if(m_nDriveDetailIndex < 0)		m_nDriveDetailIndex = 0;
	
		switch(m_nDriveDetailIndex)
		{
		case 0:	m_nDriveDetail = 10;	break;
		case 1:	m_nDriveDetail = 15;	break;
		case 2:	m_nDriveDetail = 30;	break;
		}
	
		return TRUE;
	}	

	return FALSE;
}

BOOL CSmartPanelDriveDetailDlg::SetPositionTimer(CPoint point)
{
	if(m_rectBar.PtInRect(point))
	{
		m_nDriveDetailIndex = (point.x - 140) / 5;

		//-- Check Index
		if(m_nDriveDetailIndex > 27)			m_nDriveDetailIndex = 28;
		else if(m_nDriveDetailIndex < 0)		m_nDriveDetailIndex = 0;

		m_nDriveDetail = m_nDriveDetailIndex + 2;

		return TRUE;
	}	

	return FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief		ResetValue
//! @date		2011-7-29e
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::ResetValue()
{
	switch(m_nType)
	{
	case DRIVE_DETAIL_TYPE_BURST:		
		{
			//CMiLRe 20140903 Driver Timer Setting
			//CMiLRe 20140922 PLM P140917-04607 - Reset 기능 추가
			m_nTimerValue = 10; //m_nTimerValue_1ST;
			m_nCountValue = 0; //m_nCountValue_1ST;
			m_nIntervalValue = 0; //m_nIntervalValue_1ST;
			SetDetailValueBust(BTN_DRIVE_DETAIL_PLUS1);
		}
		break;
	case DRIVE_DETAIL_TYPE_TIMER:		
		{
			m_nDriveDetail = 10;
			m_nDriveDetailIndex = m_nDriveDetail - 2;
		}
		SetDetailValue();
		break;
	}
	//-- 2011-8-1 Lee JungTaek
	
}

//------------------------------------------------------------------------------ 
//! @brief		SetDetailValue
//! @date		2011-7-29
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::SetDetailValue()
{
	//-- Set Detail Value
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	pMainWnd->SetPropertyValue(pMainWnd->GetSelectedItem(), m_nPTPCode, m_nDriveDetail);
	
}

void CSmartPanelDriveDetailDlg::SetDetailValueBust(int nItem)
{	
	//CMiLRe 20140903 Driver Timer Setting
	int nComplexTime = 0;
	int nComplexCount = 0;	

	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	if(m_strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)
	{
		switch(nItem)
		{	
		case BTN_DRIVE_DETAIL_PLUS1:
		case BTN_DRIVE_DETAIL_MINUS1:
		case BTN_DRIVE_DETAIL_PLUS2:
		case BTN_DRIVE_DETAIL_MINUS2:
		case BTN_DRIVE_DETAIL_PLUS3:
		case BTN_DRIVE_DETAIL_MINUS3:
			nComplexTime = m_nTimerValue << 8;				
			nComplexCount = m_nCountValue << 4;												
			nComplexTime = nComplexTime + nComplexCount + m_nIntervalValue;
			pMainWnd->SetPropertyValue(pMainWnd->GetSelectedItem(), PTP_CODE_SAMSUNG_DRIVE_DETAIL_BURST, nComplexTime);		
			break;	
		}
	}
	else
	{
		pMainWnd->SetPropertyValue(pMainWnd->GetSelectedItem(), m_nPTPCode, m_nDriveDetail);
	}
	//-- Set Detail Value
	
	
}
//------------------------------------------------------------------------------ 
//! @brief		SetFirstValue
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelDriveDetailDlg::SetFirstValue()
{
	CSmartPanelChildDlg *pSmartPanelChild = (CSmartPanelChildDlg*)GetParent();

	//-- Set First Values
	m_nDriveDetail	= m_nDriveDetail1st		;

	//-- Set Detail Value
	SetDetailValue();
	//-- Set 2 Depth Value
	int nValueIndex = pSmartPanelChild->GetSelected1stValue();
	pSmartPanelChild->SetSelectedPropValue(nValueIndex);
	//-- Close Dialog
	pSmartPanelChild->CloseSmartPanelChild();
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
BOOL CSmartPanelDriveDetailDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_ESCAPE)
		{
			SetFirstValue();
			return TRUE;
		}
	}
	else if(pMsg->message == WM_MOUSEWHEEL)		return TRUE;

	return CBkDialogST::PreTranslateMessage(pMsg);
}