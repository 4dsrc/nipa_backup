/////////////////////////////////////////////////////////////////////////////
//
//  MultiviewDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudioDlg.h"
#include "MultiviewDlg.h"
#include "SRSIndex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	MULTIVIEW_WIDTH		150
#define	MULTIVIEW_HEIGHT	650

#define	MULTIVIEW_TOP_MARGIN	30
#define	MULTIVIEW_LEFT_MARGIN 15
#define	MULTIVIEW_VIEWER_MARGIN 6
#define	MULTIVIEW_VIEWER_SIZE		120

// CMultiviewDlg dialog

IMPLEMENT_DYNAMIC(CMultiviewDlg,  CRSChildDialog)

CMultiviewDlg::CMultiviewDlg(CWnd* pParent /*=NULL*/)
	: CRSChildDialog(CMultiviewDlg::IDD, pParent)
{	
	//- Init Liveview
	for(int i=0; i < MULTILIVEVIEW_CNT ; i++)
		m_pLiveview[i] = NULL;	

	//CMiLRe 20141118 카메라 선택 시 프로퍼티 로드 중 창 컨트롤 막음
	m_bEnable = TRUE;
	m_nCurrentIndex = 0;
	m_nCameraCount = 0;
	m_nCheckCount = 0;
	m_bCheckFirst = FALSE;
	m_nCount = 0;
}

CMultiviewDlg::~CMultiviewDlg()
{	
	//- Init Liveview
	for(int i=0; i < MULTILIVEVIEW_CNT ; i++)
	{
		delete m_pLiveview[i];
		m_pLiveview[i] = NULL;
	}
}

void CMultiviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);	
}

BEGIN_MESSAGE_MAP(CMultiviewDlg, CRSChildDialog)	
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_TIMER()
END_MESSAGE_MAP()

// CLiveview message handlers
BOOL CMultiviewDlg::OnEraseBkgnd(CDC* pDC)
{
	return FALSE;
}

void CMultiviewDlg::OnPaint()
{	
	DrawMultiView();
	CRSChildDialog::OnPaint();
}

// CMultiviewDlg message handlers
BOOL CMultiviewDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();	
	//- Init Liveview
	for(int i=0; i < MULTILIVEVIEW_CNT ; i++)
	{
		m_pLiveview[i] = new CLiveview(i,FALSE, LIVEVIEW_TYPE_MULTIFRM);
 		m_pLiveview[i]->Create(NULL,_T(""),WS_VISIBLE|WS_CHILD,CRect(0,0,0,0),this,ID_MULTI_TINY_LIVEVIEW + i);
	}
	return TRUE;  // return TRUE  unless you set the focus to a control
}
void CMultiviewDlg::OnSize(UINT nType, int cx, int cy) 
{
    CDialog::OnSize(nType, cx, cy);    
	//-- Draw MultiLiveview
	DrawMultiView();
} 

//------------------------------------------------------------------------------ 
//! @brief		DrawMultiView
//! @date		2010-05-30
//! @author	hongsu.jung
//! static const int multiview_width		= 155;
//! static const int resize_frame			= 15;
//------------------------------------------------------------------------------ 

static const int frame_width		= 138;
static const int frame_height		= 506;
static const int frame_margin		= 5;

static const int icon_width			= 26;
static const int icon_height		= 25;

static const int viewframe_width			= 124;
static const int viewframe_height			= 114;//94; //114;
static const int viewframe_titile			= 20; //114;
static const int viewframe_left_margin	= 16;
static const int viewframe_margin		= 10;

static const int lvframe_width	= 112;
static const int lvframe_height	= 100;
static const int lvframe_margin= 6;

void CMultiviewDlg::DrawMultiView()
{
	if(!ISSet())
		return;

	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	int nImgX, nImgY, nImgW, nImgH;
	int nLVX, nLVY, nLVW, nLVH;

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- Draw Background Color	
	mDC.FillRect(rect,&CBrush(RGB(183,183,183)));

	//-- Draw Title	
	rect = CRect(CPoint(12,9),CSize(133,20));
	DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\03.Live View\\03_thumb_title_bg.png")),rect);
	DrawStr(&mDC, STR_SIZE_MIDDLE, _T("Multi Camera View"), 28,13,RGB(45,45,45));

	GetClientRect(&rect);
	//-- Border	
	nImgX		= 10;
	nImgY		= 35;
	nImgW	= frame_width;
	nImgH	= 40;		
	DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\03.Live View\\03_thumb_bg_line_top.png")) ,nImgX, nImgY, nImgW, nImgH);	
	
	//-- Each Border
	nImgX		= rect.left + viewframe_left_margin;
	nImgW	= viewframe_width;
	nImgH	= viewframe_height;

	nLVX		= nImgX + lvframe_margin;
	nLVW		= lvframe_width;
	nLVH		= lvframe_height;

	CString strCamera = _T("");
	//-- Selected Liveview
	int nSelected = 	((CNXRemoteStudioDlg*)GetParent()->GetParent())->GetSelectedItem();
	for(int i=0; i < MULTILIVEVIEW_CNT ; i++)
	{
		CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
		CString strCameraID = _T("");
		CString strTitle;
		strCameraID = pMainWnd->m_SdiMultiMgr.GetDevUniqueID(i);
		strTitle.Format(_T("ID:%s"), strCameraID);
		if(i>0)
		{
			if(strTitle == strCamera || strCamera == _T(""))
				strTitle=_T("");
		}
		//-- Frame
		nImgY =  40;
		nImgY +=  viewframe_margin + ( i * ( viewframe_height + viewframe_margin));
		if(nSelected == i)
		{
			DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\03.Live View\\03_Multi_frame_focus.png")) ,nImgX, nImgY, nImgW, nImgH);
			DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\03.Live View\\03_Multi_frame_arr.png")) ,nImgX-9, nImgY + nImgH/2 - 9, 8, 15);
		}
		else
		{
			DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\03.Live View\\03_Multi_frame_nor.png")) ,nImgX, nImgY, nImgW, nImgH);		
		}
		//-- liveview
		nLVY	 = nImgY + lvframe_margin;
		m_pLiveview[i]->MoveWindow(nLVX, nLVY -1, nLVW, nLVH  - 4);

		CRect rectTitle;
		rectTitle = CRect(CPoint(nImgX+2, nImgY + lvframe_height+2), CSize(150, 10));
//		CString strTitle = ((CNXRemoteStudioDlg*)GetParent()->GetParent())->GetItemTitle(i);
//		DrawStr(&mDC, STR_SIZE_SMALL, strTitle, rectTitle, RGB(255,255,255));
		DrawStr(&mDC, STR_SIZE_SMALL, strTitle, rectTitle, RGB(255,255,255));
		strCamera = strTitle;
	}	

	//-- Border	
	nImgX		= 10;
	nImgW	= frame_width;
	nImgH	= 40;				
	nImgY =  viewframe_margin + ( 4 * ( viewframe_height + viewframe_margin)) +5;
	DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\03.Live View\\03_thumb_bg_line_bottom.png")) ,nImgX, nImgY, nImgW, nImgH);		

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
	Invalidate(FALSE);
	/*
	if(!ISSet())
		return;

	CPaintDC dc(this);
	CRect rect;
	int nImgX, nImgY, nImgW, nImgH;
	int nLVX, nLVY, nLVW, nLVH;

	//-- Draw Background Color
	GetClientRect(&rect);	
	dc.FillRect(rect,&CBrush(RGB(133,133,133)));

	//-- Draw Title	
	rect = CRect(17,4,131,18);	
	dc.FillRect(rect,&CBrush(RGB(133,133,133)));
	DrawGraphicsImage(&dc, RSGetRoot(_T("img\\03.Live View\\03_thumb_title_bg.png")),rect);
	DrawStr(&dc, STR_SIZE_SMALL, _T("Multi Camera View"), 25,6,RGB(45,45,45));

	GetClientRect(&rect);	

	//-- Border	
	//-- Border	
	nImgX		= rect.left + frame_margin;
	nImgY		= 30;
	nImgW	= frame_width;
	nImgH	= 52;		
	DrawGraphicsImage(&dc, RSGetRoot(_T("img\\03.Live View\\03_thumb_bg_line_top.png")) ,nImgX, nImgY, nImgW, nImgH);	

	nImgX		= rect.left + frame_margin;
	nImgW	= frame_width;
	nImgH	= 52;		
	//nImgY =  40;//( rect.bottom - frame_height ) / 2;
	nImgY =  viewframe_margin + ( 4 * ( viewframe_height + viewframe_margin));	
	DrawGraphicsImage(&dc, RSGetRoot(_T("img\\03.Live View\\03_thumb_bg_line_bottom.png")) ,nImgX, nImgY, nImgW, nImgH);	


	//-- Each Border
	nImgX		= rect.left + viewframe_left_margin;
	nImgW	= viewframe_width;
	nImgH	= viewframe_height;

	nLVX		= nImgX + lvframe_margin;
	nLVW		= lvframe_width;
	nLVH		= lvframe_height;

	//-- Selected Liveview
	int nSelected = 	((CNXRemoteStudioDlg*)GetParent()->GetParent())->GetSelectedItem();
	for(int i=0; i < MULTILIVEVIEW_CNT ; i++)
	{
		//-- Frame
		nImgY =  ( rect.bottom - frame_height ) / 2;
		nImgY +=  viewframe_margin + ( i * ( viewframe_height + viewframe_margin));
		if(nSelected == i)
		{
			DrawGraphicsImage(&dc, RSGetRoot(_T("img\\03.Live View\\03_Multi_frame_focus.png")) ,nImgX, nImgY, nImgW, nImgH);
			DrawGraphicsImage(&dc, RSGetRoot(_T("img\\03.Live View\\03_Multi_frame_arr.png")) ,nImgX-9, nImgY + nImgH/2 - 9, 8, 15);
		}
		else
		{
			DrawGraphicsImage(&dc, RSGetRoot(_T("img\\03.Live View\\03_Multi_frame_nor.png")) ,nImgX, nImgY, nImgW, nImgH);		
		}
		//-- liveview
		nLVY	 = nImgY + lvframe_margin;
		m_pLiveview[i]->MoveWindow(nLVX, nLVY -1, nLVW, nLVH  - 4);

		CRect rectTitle;
		rectTitle = CRect(CPoint(nImgX+2, nImgY + lvframe_height+4), CSize(nImgW, 10));
		CString strTitle = ((CNXRemoteStudioDlg*)GetParent()->GetParent())->GetItemTitle(i);
		DrawStr(&dc, STR_SIZE_SMALL, strTitle, rectTitle, RGB(255,255,255));
	}
	*/
}

//------------------------------------------------------------------------------ 
//! @brief		SetMultiViewer
//! @date		2010-05-31
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
BOOL CMultiviewDlg::SetMultiViewer(int nAll)
{
	//-- Set Liveview Pointer
	for(int i = 0; i < MULTILIVEVIEW_CNT; i++)
	{
		if(i < nAll)	{	m_pLiveview[i]->SetLiveview(TRUE);	TRACE(_T("MultiViewer [%d] Get Liveview : 0x%x\n"),i, ((CNXRemoteStudioDlg*)AfxGetMainWnd())->GetLiveviewBuffer(i)); }
		else			{	m_pLiveview[i]->SetLiveview(FALSE);	TRACE(_T("MultiViewer [%d] Non Liveview : 0x%x\n"),i, ((CNXRemoteStudioDlg*)AfxGetMainWnd())->GetLiveviewBuffer(i)); }
	}
	Invalidate(FALSE);
	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief		SetLiveview
//! @date		2010-06-29
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CMultiviewDlg::SetLiveview(BOOL b)
{
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	int nAll = pMainWnd->m_SdiMultiMgr.GetPTPCount();
	int nCameraNum= 0;
	for(int i = 0; i < nAll; i++)
	{
		m_pLiveview[i]->SetLiveview(b);
		nCameraNum++;
	}

	m_nCheckCount++;

	if(m_bCheckFirst)
		m_nCameraCount = nCameraNum;

	if(m_nCameraCount != nCameraNum && m_nCheckCount > 2 && m_bCheckFirst == FALSE)
	{
		m_nCameraCount = nCameraNum;

		/*CString strCamera;
		CString strCameraMsg;
		if(nCameraNum == 2)
			strCamera = _T("2nd Camera");
		if(nCameraNum == 3)
			strCamera = _T("3rd Camera");
		if(nCameraNum == 4)
			strCamera = _T("4th Camera");

		strCameraMsg.Format(_T("If you want to using %s, you should to change in Multiview."), strCamera);

		if(nCameraNum > 1)
			AfxMessageBox(strCameraMsg);*/

		if(nCameraNum > 1)
		{
			RSEvent* pMsgToMain = NULL;
			pMsgToMain = new RSEvent();
			pMsgToMain->message	= WM_SDI_EVENT_ERR_MSG;							
			pMsgToMain->nParam1	= 100;	
			pMsgToMain->nParam2 = nCameraNum;
			::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_ERR_MSG, (LPARAM)pMsgToMain);

			//SetTimer(0, 1000, NULL);
		}
	}
}


BOOL CMultiviewDlg::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_LBUTTONDOWN)
	{
		int nViewID = 0;
		nViewID = ::GetDlgCtrlID(pMsg->hwnd);
		int nSelectIndex = nViewID - ID_MULTI_TINY_LIVEVIEW;
		if(m_nCurrentIndex == nSelectIndex)
		{
			return m_nCurrentIndex;
		}
		else
		{
			if(GetDiscountCapture() == 0)// &&!GetResetCaptureDone() )
			{
				if(m_nCheckCount == 2)
					m_bCheckFirst = TRUE;
				else
					m_bCheckFirst = FALSE;

			//CMiLRe 20141118 카메라 선택 시 프로퍼티 로드 중 창 컨트롤 막음
				if(m_bEnable)
				{
					//-- Check ID
					int nViewID = 0;
					nViewID = ::GetDlgCtrlID(pMsg->hwnd);

					if(nViewID >= ID_MULTI_TINY_LIVEVIEW && nViewID < ID_MULTI_TINY_LIVEVIEW + MULTILIVEVIEW_CNT)
					{
	/*					int nSelectIndex = nViewID - ID_MULTI_TINY_LIVEVIEW;
						if(nSelectIndex == m_CurrentIndex)
						{
							return m_CurrentIndex;
						}
						else
						{ */
							m_nCurrentIndex = nSelectIndex;
							if(m_pLiveview[nSelectIndex]->IsSetLiveview())
							{
								//-- Combobox 변경
								RSEvent *pMsg = new RSEvent();
								pMsg->message = WM_RS_LV_CHANGE_DEVICE;
								pMsg->nParam1 = nSelectIndex;				
								::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_RS_LV_CHANGE_DEVICE, (LPARAM)pMsg);
								//-- 
								//SendMessage(WM_PAINT);
								Invalidate(FALSE);
							}
	//					}		
					}
				}
			}
			else
			{
				RSEvent* pMsgToMain = NULL;
				pMsgToMain = new RSEvent();
				pMsgToMain->message	= WM_SDI_EVENT_ERR_MSG;							
				pMsgToMain->nParam1	= 101;	
				::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_ERR_MSG, (LPARAM)pMsgToMain);
			}
		}
	}	
	return CRSChildDialog::PreTranslateMessage(pMsg);
}

void CMultiviewDlg::OnTimer(UINT_PTR nIDEvent)
{
	m_nCount++;

	if(m_nCount > 4)
	{
		m_nCount = 0;
		RSEvent* pMsgToMain = NULL;
		pMsgToMain = new RSEvent();
		pMsgToMain->message	= WM_SDI_EVENT_ERR_MSG;							
		pMsgToMain->nParam1	= 173;	
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_ERR_MSG, (LPARAM)pMsgToMain);
		KillTimer(0);
	}

	CRSChildDialog::OnTimer(nIDEvent);
}
