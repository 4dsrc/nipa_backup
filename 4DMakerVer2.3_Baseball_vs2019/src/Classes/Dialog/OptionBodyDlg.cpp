/////////////////////////////////////////////////////////////////////////////
//
//  OptionBodyDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "OptionBodyDlg.h"

//-- SubTab
#include "OptionTimeLapseDlg.h"
#include "OptionCustomDlg.h"
#include "OptionDSCSet1Dlg.h"
#include "OptionDSCSet2Dlg.h"
#include "OptionDSCSet3Dlg.h"
#include "OptionDSCSetEtcDlg.h"
#include "OptionPCSet1Dlg.h"
#include "OptionPCSet2Dlg.h"
#include "OptionAboutDlg.h"

#include "Ini.h"
#include "DSCOptionIndex.h"
#include "NXRemoteStudioDlg.h"
#include "NXRemoteStudioFunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// COptionBodyDlg dialog
IMPLEMENT_DYNAMIC(COptionBodyDlg, CDialogEx)

//------------------------------------------------------------------------------ 
//! @brief		COptionBodyDlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
COptionBodyDlg::COptionBodyDlg(CRSOption *pOption, CWnd* pParent /*=NULL*/)
	: CRSChildDialog(COptionBodyDlg::IDD, pParent)
{
	SetDragMove(FALSE);
	//-- Tab Dialogs
	for(int i = 0 ; i < OPT_TAB_CNT; i++)
		m_pOptDlg[i] = NULL;

	m_pOption = pOption;
	//CMiLRe 20140922 Picture Interval Capture 추가
	m_bChangedDP = TRUE;
}

COptionBodyDlg::~COptionBodyDlg() 
{
	//-- 2011-6-24  chlee
	for(int i = 0 ; i < OPT_TAB_CNT; i++)
	{
		if(m_pOptDlg[i])
		{
			delete m_pOptDlg[i];
			m_pOptDlg[i] = NULL;
		}
	}
}

void COptionBodyDlg::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);	
}				

BEGIN_MESSAGE_MAP(COptionBodyDlg, CRSChildDialog)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()	
END_MESSAGE_MAP()

// CLiveview message handlers
BOOL COptionBodyDlg::OnEraseBkgnd(CDC* pDC)	{	return FALSE;}
void COptionBodyDlg::OnPaint()
{	
	UpdateFrames();
	CDialogEx::OnPaint();
}

BOOL COptionBodyDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();
	//-- 2011-05-30
	CreateFrames();
	//-- 2011-05-18 hongsu.jung
	UpdateFrames();

	return TRUE; 	
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionBodyDlg::UpdateFrames()
{
//	Invalidate(false);
}

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
static const int size_title_h				= 30;
void COptionBodyDlg::CreateFrames()
{
	m_pOptDlg[OPT_TAB_TIMELAPSE	]	= new COptionTimeLapseDlg();
	m_pOptDlg[OPT_TAB_TIMELAPSE	]	->Create(COptionTimeLapseDlg::IDD,this);

	m_pOptDlg[OPT_TAB_CUSTOM	]	= new COptionCustomDlg();
	m_pOptDlg[OPT_TAB_CUSTOM	]	->Create(COptionCustomDlg::IDD,this);		
	
	m_pOptDlg[OPT_TAB_DSC_SET1	]	= new COptionDSCSet1Dlg();
	m_pOptDlg[OPT_TAB_DSC_SET1	]	->Create(COptionDSCSet1Dlg::IDD,this);		
	
	m_pOptDlg[OPT_TAB_DSC_SET2	]	= new COptionDSCSet2Dlg();
	m_pOptDlg[OPT_TAB_DSC_SET2	]	->Create(COptionDSCSet2Dlg::IDD,this);		

	m_pOptDlg[OPT_TAB_DSC_SET3	]	= new COptionDSCSet3Dlg();
	m_pOptDlg[OPT_TAB_DSC_SET3	]	->Create(COptionDSCSet3Dlg::IDD,this);	
	
	m_pOptDlg[OPT_TAB_PC_SET1	]	= new COptionPCSet1Dlg();
	m_pOptDlg[OPT_TAB_PC_SET1	]	->Create(COptionPCSet1Dlg::IDD,this);		
	
	m_pOptDlg[OPT_TAB_PC_SET2	]	= new COptionPCSet2Dlg();
	m_pOptDlg[OPT_TAB_PC_SET2	]	->Create(COptionPCSet2Dlg::IDD,this);		
	
	m_pOptDlg[OPT_TAB_DSC_SET_ETC		]	= new COptionDSCSetEtcDlg();
	m_pOptDlg[OPT_TAB_DSC_SET_ETC	]	->Create(COptionDSCSetEtcDlg::IDD,this);		

	m_pOptDlg[OPT_TAB_ABOUT		]	= new COptionAboutDlg();
	m_pOptDlg[OPT_TAB_ABOUT	]	->Create(COptionAboutDlg::IDD,this);		
	
	
	
	for(int i = 0; i < OPT_TAB_CNT; i ++)
	{		
		if(i == OPT_TAB_DSC_SET_ETC) continue;

		m_pOptDlg[i]	->MoveWindow(0,0,OPTION_DLG_WIDTH, OPTION_DLG_HEIGHT - OPTION_TAB_HEIGHT);	
		m_pOptDlg[i]	->ShowWindow(SW_HIDE);
	}

	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	m_pOptDlg[OPT_TAB_DSC_SET_ETC]	->MoveWindow(0,0,OPTION_DLG_WIDTH-15, OPTION_DLG_HEIGHT - 69);		
	m_pOptDlg[OPT_TAB_DSC_SET_ETC]	->ShowWindow(SW_HIDE);
	
	//-- 2011-6-16 Lee JungTaek
	InitOptionProp();
}

void COptionBodyDlg::SetActivate(int nBefore, int nNewTab )
{
	m_pOptDlg[nBefore]->ShowWindow(SW_HIDE);
	m_pOptDlg[nNewTab]->ShowWindow(SW_SHOW);
	m_pOptDlg[nNewTab]->UpdateWindow();
}

//-- Init Option Property
void COptionBodyDlg::InitOptionProp()
{
	COptionTimeLapseDlg* dlgOptTimeLapse = (COptionTimeLapseDlg*)m_pOptDlg[OPT_TAB_TIMELAPSE	];
	dlgOptTimeLapse->InitProp(m_pOption->m_TimeLapse);

	COptionPCSet1Dlg* dlgOptPCSet1 = (COptionPCSet1Dlg*)m_pOptDlg[OPT_TAB_PC_SET1	];
	dlgOptPCSet1->InitProp(m_pOption->m_PCSet1);

	COptionPCSet2Dlg* dlgOptPCSet2 = (COptionPCSet2Dlg*)m_pOptDlg[OPT_TAB_PC_SET2	];
	dlgOptPCSet2->InitProp(m_pOption->m_PCSet2);

	UpdateData(FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief		FileSave
//! @date		2011-6-20
//! @author		Lee JungTaek
//! @note	 	Save Option
//------------------------------------------------------------------------------ 
void COptionBodyDlg::FileSave(CString strOptionPath)
{
	CIni ini;
	CString strInfo;
	if(!ini.SetIniFilename (strOptionPath))
		return;

	//-- Time Lapse
	ini.WriteBoolean(INFO_OPT_TIMELAPSE,INFO_TIMELAPSE_DELAYENABLE	, m_pOption->m_TimeLapse.bDelaySet			);
	ini.WriteInt(INFO_OPT_TIMELAPSE,	INFO_TIMELAPSE_DELAYMIN		, m_pOption->m_TimeLapse.nDelayMin			);
	ini.WriteInt(INFO_OPT_TIMELAPSE,	INFO_TIMELAPSE_DELAYSEC		, m_pOption->m_TimeLapse.nDelaySec			);
	ini.WriteBoolean(INFO_OPT_TIMELAPSE,INFO_TIMELAPSE_TIMERENABLE	, m_pOption->m_TimeLapse.bTimerCaptureSet	);
	ini.WriteInt(INFO_OPT_TIMELAPSE,	INFO_TIMELAPSE_TIMERMIN		, m_pOption->m_TimeLapse.nTimerMin			);
	ini.WriteInt(INFO_OPT_TIMELAPSE,	INFO_TIMELAPSE_TIMERSEC		, m_pOption->m_TimeLapse.nTimerSec			);
	ini.WriteInt(INFO_OPT_TIMELAPSE,	INFO_TIMELAPSE_TIMERCOUNT	, m_pOption->m_TimeLapse.nCaptureCount		);

	//-- PC Set1
	if(IsWow64())
		strInfo = INFO_OPT_PCSET1_64;
	else
		strInfo = INFO_OPT_PCSET1_32;

	ini.WriteString(strInfo , INFO_PCSET1_PICTUREPATH	, m_pOption->m_PCSet1.strPictureRootPath);
	ini.WriteString(strInfo , INFO_PCSET1_BANKPATH		, m_pOption->m_PCSet1.strBankPath);

	//-- PC Set2
	ini.WriteString(INFO_OPT_PCSET2 ,  INFO_PCSET2_PREFIX		, m_pOption->m_PCSet2.strPrefix);
	ini.WriteInt(INFO_OPT_PCSET2	,  INFO_PCSET2_FILENAME_TYPE, m_pOption->m_PCSet2.nFileNameType);
	ini.WriteInt(INFO_OPT_PCSET2	,  INFO_PCSET2_CHECKVIEW	, m_pOption->m_PCSet2.nCheckView);
	ini.WriteInt(INFO_OPT_PCSET2	,  INFO_PCSET2_SELECTVIEW	, m_pOption->m_PCSet2.nSelectView);
	ini.WriteInt(INFO_OPT_PCSET2	,  INFO_PCSET2_SELECTVIEWSIZE	, m_pOption->m_PCSet2.nSelectViewSize);
}
//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void COptionBodyDlg::SetDSCOptionProp(int nPTPCode, int nPropIndex)
{
	((CNXRemoteStudioDlg*)AfxGetMainWnd())->SetPropertyValue(m_nMainCamera, nPTPCode, nPropIndex);
}
void COptionBodyDlg::SetDSCOptionProp(int nPTPCode, CString strPropValue)
{
	((CNXRemoteStudioDlg*)AfxGetMainWnd())->SetPropertyValue(m_nMainCamera, nPTPCode, strPropValue);
}

//------------------------------------------------------------------------------ 
//! @brief		SaveOptionProp
//! @date		2011-6-23 Lee JungTaek
//! @attention	none
//! @note	 	Save Option
//------------------------------------------------------------------------------ 
void COptionBodyDlg::SaveOptionProp(BOOL bTimerCapture)
{
	//-- Time Lapse
	COptionTimeLapseDlg* dlgOptTimeLapse = (COptionTimeLapseDlg*)m_pOptDlg[OPT_TAB_TIMELAPSE	];
	dlgOptTimeLapse->SaveProp(m_pOption->m_TimeLapse);
	
	//-- Custom Option
	COptionCustomDlg* dlgOptCustom = (COptionCustomDlg*)m_pOptDlg[OPT_TAB_CUSTOM	];
	dlgOptCustom->SaveProp(m_pOption->m_Custom);

	//-- DSC Settting1 Option
	COptionDSCSet1Dlg* dlgOptDSCSet1 = (COptionDSCSet1Dlg*)m_pOptDlg[OPT_TAB_DSC_SET1	];
	dlgOptDSCSet1->SaveProp(m_pOption->m_DSCSet1);
	
	//-- DSC Setting2	Option
	COptionDSCSet2Dlg* dlgOptDSCSet2 = (COptionDSCSet2Dlg*)m_pOptDlg[OPT_TAB_DSC_SET2	];
	dlgOptDSCSet2->SaveProp(m_pOption->m_DSCSet2);

	//-- DSC Setting3 Option
	COptionDSCSet3Dlg* dlgOptDSCSet3 = (COptionDSCSet3Dlg*)m_pOptDlg[OPT_TAB_DSC_SET3	];
	dlgOptDSCSet3->SaveProp(m_pOption->m_DSCSet3);

	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) != 0)
	{
		COptionDSCSetEtcDlg* dlgOptDSCSetEtc = (COptionDSCSetEtcDlg*)m_pOptDlg[OPT_TAB_DSC_SET_ETC	];
		dlgOptDSCSetEtc->SaveProp(m_pOption->m_DSCSetEtc);
	}

	//-- PC Set1
	COptionPCSet1Dlg* dlgOptPCSet1 = (COptionPCSet1Dlg*)m_pOptDlg[OPT_TAB_PC_SET1	];
	dlgOptPCSet1->SaveProp(m_pOption->m_PCSet1);

	//-- PC Set1
	COptionPCSet2Dlg* dlgOptPCSet2 = (COptionPCSet2Dlg*)m_pOptDlg[OPT_TAB_PC_SET2	];
	dlgOptPCSet2->SaveProp(m_pOption->m_PCSet2);

	//-- File Save
	CString strOptionPath = _T("");
	strOptionPath.Format(_T("%s\\RSOption.txt"), STR_CONFIG_PATH);

	FileSave(RSGetRoot(strOptionPath));
	
	//-- Reload
	RSReloadOption();
	//-- Init
	InitOptionProp();
	
	if(!bTimerCapture)
		::SendMessage(GetParent()->GetSafeHwnd(), WM_CLOSE, NULL, NULL);

	//-- Reload Thumbnail 
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	pMainWnd->m_pDlgLiveview->m_pDlgThumbnail->LoadThumbnail();
}

//------------------------------------------------------------------------------ 
//! @brief		SetItem
//! @date		2010-06-10
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionBodyDlg::SetItem(int nPTPCode, int nPropValue)
{
	int nPropIndex = 0;

	//-- Draw Icon
	switch(nPTPCode)
	{
	//-- Custom
	case	PTP_CODE_SAMSUNG_ISOSTEP:			nPropIndex = DSC_OPT_CUSTOM_ISOSTEP;		break;		
	case	PTP_CODE_SAMSUNG_AUTOISORANGE:		nPropIndex = DSC_OPT_CUSTOM_AUTOISORANGE;	break;
	case	PTP_CODE_SAMSUNG_LDC:				nPropIndex = DSC_OPT_CUSTOM_LDC;			break;
	case	PTP_CODE_SAMSUNG_AFLAMP:			nPropIndex = DSC_OPT_CUSTOM_AFLAMP;			break;
	//CMiLRe 20140902 ISO Expansion 추가
	case	PTP_CODE_SAMSUNG_ISOEXPANSION:		nPropIndex = DSC_OPT_CUSTOM_ISOEXPANSION;	break;
	//-- DSC Set1
	case	PTP_CODE_SAMSUNG_FILENAME	:		nPropIndex = DSC_OPT_DSC1_FILENAME;			break;
	case	PTP_CODE_SAMSUNG_FILENUMBER	:		nPropIndex = DSC_OPT_DSC1_FILENUMBER;		break;
	case	PTP_CODE_SAMSUNG_FOLDERTYPE	:		nPropIndex = DSC_OPT_DSC1_FOLDERTYPE;		break;
		//-- DSC Set2
	case	PTP_CODE_SAMSUNG_DATETIMEHOUR	:	nPropIndex = DSC_OPT_DSC2_DATETIME_HOUR			;	break;
	case	PTP_CODE_SAMSUNG_DATETIMEDST	:	nPropIndex = DSC_OPT_DSC2_DATETIME_SUMMERTIME	;	break;
	case	PTP_CODE_SAMSUNG_DATETIMEIMPRINT:	nPropIndex = DSC_OPT_DSC2_DATETIME_IMPRINT		;	break;
	//-- DSC Set3
	case	PTP_CODE_SAMSUNG_SYSTEMVOLUME	:	nPropIndex = DSC_OPT_DSC3_VOL_SYSTEM	;	break;
	case	PTP_CODE_SAMSUNG_AFSOUND		:	nPropIndex = DSC_OPT_DSC3_VOL_AFSOUND	;	break;
	case	PTP_CODE_SAMSUNG_BUTTONSOUND	:	nPropIndex = DSC_OPT_DSC3_VOL_BTNSOUND	;	break;
	//CMiLRe 20140901 E Shutter Sound 추가
	case PTP_CODE_SAMSUNG_E_SHUTTER_SOUND: nPropIndex = DSC_OPT_DSC3_VOL_ESHSOUND; break;
	//CMiLRe 20140910 Movie Voice&Size 추가
	case PTP_CODE_SAMSUNG_VOICE_VOLUME: nPropIndex = DSC_OPT_DSC3_VOL_VOICE; break;
	case PTP_CODE_SAMSUNG_MOV_SIZE: nPropIndex = DSC_OPT_DSC3_MOVIE_SIZE; break;
	//dh0.seo 2014-09-12 Power Save
	case PTP_CODE_SAMSUNG_POWERSAVE: nPropIndex = DSC_OPT_DSC1_POWERSAVE; break;
	//CMiLRe 20140918 Picture Setting 추가
	case PTP_DPC_SAMSUNG_OLED_COLOR: nPropIndex = DSC_OPT_DSC_ETC_OLED_COLOR; break;
	case PTP_DPC_SAMSUNG_MFASSIST: nPropIndex = DSC_OPT_DSC_ETC_MFASSIST; break;
	case PTP_DPC_SAMSUNG_FOCUS_PEAKING_LEVEL: nPropIndex = DSC_OPT_DSC_ETC_FOCUS_PEAKING_LEVEL; break;
	case PTP_DPC_SAMSUNG_FOCUS_PEAKING_COLOR: nPropIndex = DSC_OPT_DSC_ETC_FOCUS_PEAKING_COLOR; break;
	case PTP_DPC_SAMSUNG_BRIGHTNESS_ADJUST_GUIDE: nPropIndex = DSC_OPT_DSC_ETC_BRIGHTNESS_ADJUST_GUIDE; break;
	case PTP_DPC_SAMSUNG_FRAMING_MODE: nPropIndex = DSC_OPT_DSC_ETC_FRAMING_MODE; break;
	case PTP_DPC_SAMSUNG_DYNAMIC_RANGE: nPropIndex = DSC_OPT_DSC_ETC_DYNAMIC_RANGE; break;
	case PTP_DPC_SAMSUNG_LINK_AE_2_AF: nPropIndex = DSC_OPT_DSC_ETC_LINK_AE_2_AF; break;
	case PTP_DPC_SAMSUNG_OVER_EXPOSURE: nPropIndex = DSC_OPT_DSC_ETC_OVER_EXPOSURE; break;
	//CMiLRe 20140918 Picture Setting 추가
	case PTP_DPC_SAMSUNG_FLASH_USE_WIRELESS: nPropIndex = DSC_OPT_DSC_ETC_FLASH_USE_WIRELESS; break;
	case PTP_DPC_SAMSUNG_FLASH_CHANNEL: nPropIndex = DSC_OPT_DSC_ETC_FLASH_CHANNEL; break;
	case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_A_ATTL: nPropIndex = DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_A_ATTL; break;
	case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_A_MANUAL: nPropIndex = DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_A_MANUAL; break;
	case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_A: nPropIndex = DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_A; break;
	case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_B_ATTL: nPropIndex = DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_B_ATTL; break;
	case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_B_MANUAL: nPropIndex = DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_B_MANUAL; break;
	case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_B: nPropIndex = DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_B; break;
	case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_C_ATTL: nPropIndex = DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_C_ATTL; break;
	case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_C_MANUAL: nPropIndex = DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_C_MANUAL; break;
	case PTP_DPC_SAMSUNG_GROUP_FLASH_GROUP_C: nPropIndex = DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_C; break;

	case PTP_DPC_SAMSUNG_EXT_FLASH_USE_WIRELESS: nPropIndex = DSC_OPT_DSC_ETC_EXT_FLASH_USE_WIRELESS; break;
	case PTP_DPC_SAMSUNG_EXT_FLASH_CHANNEL: nPropIndex = DSC_OPT_DSC_ETC_EXT_FLASH_CHANNEL; break;
	case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_A_ATTL: nPropIndex = DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_A_ATTL; break;
	case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_A_MANUAL: nPropIndex = DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_A_MANUAL; break;
	case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_A: nPropIndex = DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_A; break;
	case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_B_ATTL: nPropIndex = DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_B_ATTL; break;
	case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_B_MANUAL: nPropIndex = DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_B_MANUAL; break;
	case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_B: nPropIndex = DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_B; break;
	case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_C_ATTL: nPropIndex = DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_C_ATTL; break;
	case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_C_MANUAL: nPropIndex = DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_C_MANUAL; break;
	case PTP_DPC_SAMSUNG_EXT_GROUP_FLASH_GROUP_C: nPropIndex = DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_C; break;

	//CMiLRe 20140918 Movie Setting 추가
	case PTP_DPC_SAMSUNG_MOV_QUALITY: nPropIndex = DSC_OPT_DSC_ETC_MOV_QUALITY; break;
	case PTP_DPC_SAMSUNG_MOV_MULTI_MOTION: nPropIndex = DSC_OPT_DSC_ETC_MOV_MULTI_MOTION; break;
	case PTP_DPC_SAMSUNG_MOV_FADER: nPropIndex = DSC_OPT_DSC_ETC_MOV_FADER; break;
	case PTP_DPC_SAMSUNG_WIND_CUT: nPropIndex = DSC_OPT_DSC_ETC_WIND_CUT; break;
	case PTP_DPC_SAMSUNG_MIC_MANUAL_VALUE: nPropIndex = DSC_OPT_DSC_ETC_MIC_MANUAL_VALUE; break;
	case PTP_DPC_SAMSUNG_MOVIE_SMART_RANGE: nPropIndex = DSC_OPT_DSC_ETC_MOVIE_SMART_RANGE; break;
		
	case PTP_DPC_SAMSUNG_INTERVAL_CYCLE_TIME_HOUR: nPropIndex = DSC_OPT_RIME_LAPSE_INTERVAL_CYCLE_TIME_HOUR; break;
	case PTP_DPC_SAMSUNG_INTERVAL_NUM: nPropIndex = DSC_OPT_RIME_LAPSE_INTERVAL_NUM; break;
	case PTP_DPC_SAMSUNG_INTERVAL_BEGINE_TIME_USE: nPropIndex = DSC_OPT_RIME_LAPSE_INTERVAL_BEGINE_TIME_USE; break;
	case PTP_DPC_SAMSUNG_TIMELABPS: nPropIndex = DSC_OPT_RIME_LAPSE_TIMELABPS; break;
	case PTP_DPC_SAMSUNG_INTERVAL_BEGINE_TIME_HOUR: nPropIndex = DSC_OPT_RIME_LAPSE_INTERVAL_BEGINE_TIME_HOUR; break;
	case PTP_DPC_SAMSUNG_INTERVAL_BEGINE_TIME_MIN: nPropIndex = DSC_OPT_RIME_LAPSE_INTERVAL_BEGINE_TIME_MIN; break;
	//CMiLRe 20140922 Picture Interval Capture 추가
	case PTP_DPC_SAMSUNG_INTERVAL_SET: nPropIndex = DSC_OPT_RIME_LAPSE_INTERVAL_SET; break;
	//CMiLRe 20140929 Custom 추가
	case PTP_DPC_SAMSUNG_DISP_BRIGHT_LEVEL: nPropIndex = DSC_OPT_DSC_ETC_DISP_BRIGHT_LEVEL; break;
	case PTP_DPC_SAMSUNG_BRK_DEPTH_SET:	nPropIndex = DSC_OPT_DSC_ETC_BRK_DEPTH; break;
	case PTP_DPC_SAMSUNG_DISP_AUTO_BRIGHT: nPropIndex = DSC_OPT_DSC_ETC_DISP_AUTO_BRIGHT; break;
	case PTP_DPC_SAMSUNG_DISP_COLOR_DETAIL_XY: nPropIndex = DSC_OPT_DSC_ETC_DISP_COLOR_DETAIL_XY; break;
	case PTP_DPC_SAMSUNG_COLOR_SPACE: nPropIndex = DSC_OPT_DSC_ETC_COLOR_SPACE; break;
	case PTP_DPC_SAMSUNG_DISTORTION_CORRECT: nPropIndex = DSC_OPT_DSC_ETC_DISTORTION_CORRECT; break;
	case PTP_DPC_SAMSUNG_WIFI_SENDNG_SIZE: nPropIndex = DSC_OPT_DSC_ETC_WIFI_SENDNG_SIZE; break;
	case PTP_DPC_SAMSUNG_EFS: nPropIndex = DSC_OPT_DSC_ETC_EFS; break;
	//CMiLRe 20141001 Setting Battery Selection 추가
	//CMiLRe 20141021 Setting Battery Selection 추가 - Internal, External Battry Level 추가
	case PTP_CODE_BATTERYLEVEL:	nPropIndex = DSC_OPT_DSC_ETC_BATTERY_LEVEL; break;	
	case PTP_DPC_BATTERY_LEVEL_EXT:nPropIndex = DSC_OPT_DSC_ETC_BATTERY_LEVEL_EXT; break;	
	case PTP_DPC_SAMSUNG_BATTERY_SELECTION: nPropIndex = DSC_OPT_DSC_ETC_BATTERY_SELECTION; break;	
	//CMiLRe 20141008 Bracket Settings (AE,WB)
	case PTP_DPC_SAMSUNG_USER_DISP_ICON: nPropIndex = DSC_OPT_DSC_ETC_USER_DISP_ICON; break;
	case PTP_DPC_SAMSUNG_USER_DISP_DATE_TIME: nPropIndex = DSC_OPT_DSC_ETC_USER_DISP_DATE_TIME; break;
	case PTP_DPC_SAMSUNG_USER_DISP_BUTTONS: nPropIndex = DSC_OPT_DSC_ETC_USER_DISP_BUTTONS; break;
	case PTP_DPC_SAMSUNG_I_FN_FNO: nPropIndex = DSC_OPT_DSC_ETC_I_FN_FNO; break;
	case PTP_DPC_SAMSUNG_I_FN_SHUTTER_SPEED: nPropIndex = DSC_OPT_DSC_ETC_I_FN_SHUTTER_SPEED; break;
	case PTP_DPC_SAMSUNG_I_FN_EV: nPropIndex = DSC_OPT_DSC_ETC_I_FN_EV; break;
	case PTP_DPC_SAMSUNG_I_FN_ISO: nPropIndex = DSC_OPT_DSC_ETC_I_FN_ISO; break;
	case PTP_DPC_SAMSUNG_I_FN_WB: nPropIndex = DSC_OPT_DSC_ETC_I_FN_WB; break;
	case PTP_DPC_SAMSUNG_I_FN_I_ZOOM: nPropIndex = DSC_OPT_DSC_ETC_I_FN_I_ZOOM; break;
	case PTP_DPC_SAMSUNG_BRK_AE_STEP: nPropIndex = DSC_OPT_DSC_ETC_BRK_AE_STEP; break;
	case PTP_DPC_SAMSUNG_BRK_WB_SET: nPropIndex = DSC_OPT_DSC_ETC_BRK_WB_SET; break;
	case PTP_DPC_SAMSUNG_BRK_PW_TYPE: nPropIndex = DSC_OPT_DSC_ETC_BRK_PW_TYPE; break;
	//dh0.seo 2014-10-02 Key Mapping
	case PTP_DPC_SAMSUNG_KEY_MPPING_PREVIEW: nPropIndex = DSC_OPT_DSC_ETC_KEY_PREVIEW; break;
	case PTP_DPC_SAMSUNG_KEY_MPPING_AEL: nPropIndex = DSC_OPT_DSC_ETC_KEY_AEL; break;
	case PTP_DPC_SAMSUNG_KEY_MPPING_AF_ON: nPropIndex = DSC_OPT_DSC_ETC_KEY_AF_ON; break;
	case PTP_DPC_SAMSUNG_KEY_MPPING_CUSTOM_KEY1: nPropIndex = DSC_OPT_DSC_ETC_KEY_CUSTOM1; break;
	case PTP_DPC_SAMSUNG_KEY_MPPING_CUSTOM_KEY2: nPropIndex = DSC_OPT_DSC_ETC_KEY_CUSTOM2; break;
	case PTP_DPC_SAMSUNG_KEY_MPPING_CUSTOM_KEY3: nPropIndex = DSC_OPT_DSC_ETC_KEY_CUSTOM3; break;
	case PTP_DPC_SAMSUNG_KEY_MPPING_CUSTOM_WHEEL: nPropIndex = DSC_OPT_DSC_ETC_KEY_WHEEL; break;
	case PTP_DPC_SAMSUNG_KEY_MPPING_COMMAND_DIAL: nPropIndex = DSC_OPT_DSC_ETC_KEY_DIAL; break;

	//dh0.seo 2014-09-15 Auto Display Off
	case PTP_CODE_SAMSUNG_AUTODISPLAY: nPropIndex = DSC_OPT_CUSTOM_AUTODISPLAY; break;
	//dh0.seo 2014-09-15 Quick View
	case PTP_CODE_SAMSUNG_QUICKVIEW: nPropIndex = DSC_OPT_DSC2_QUICKVIEW; break;
	//SDK Test
	//CMiLRe 20140929 Custom 추가
	case PTP_DPC_SAMSUNG_VIDEO_OUT: nPropIndex = DSC_OPT_DSC_ETC_VIDEO_OUT; break;
	case PTP_DPC_SAMSUNG_ANYNET: nPropIndex = DSC_OPT_DSC_ETC_ANYNET; break;
	case PTP_DPC_SAMSUNG_HDMI: nPropIndex = DSC_OPT_DSC_ETC_HDMI; break;
	case PTP_DPC_SAMSUNG_3D_HDMI: nPropIndex = DSC_OPT_DSC_ETC_3D_HDMI; break;
	case PTP_DPC_SAMSUNG_LANGUAGE: nPropIndex = DSC_OPT_DSC_ETC_LANGUAGE; break;
	case PTP_DPC_SAMSUNG_MODE_HELP_GUIDE: nPropIndex = DSC_OPT_DSC_ETC_MODE_HELP_GUIDE; break;
	case PTP_DPC_SAMSUNG_FUNCTION_HELP_GUIDE: nPropIndex = DSC_OPT_DSC_ETC_FUNCTION_HELP_GUIDE; break;
	case PTP_DPC_SAMSUNG_TOUCH_OPERATION: nPropIndex = DSC_OPT_DSC_ETC_TOUCH_OPERATION; break;
	case PTP_DPC_SAMSUNG_AF_RELEASE_PRIORITY: nPropIndex = DSC_OPT_DSC_ETC_AF_RELEASE_PRIORITY; break;
	case PTP_DPC_SAMSUNG_EVF_BUTTON_INTERACTION: nPropIndex = DSC_OPT_DSC_ETC_EVF_BUTTON_INTERACTION; break;
	case PTP_DPC_SAMSUNG_GRID_LINE: nPropIndex = DSC_OPT_DSC_ETC_GRID_LINE; break;
	case PTP_DPC_SAMSUNG_NOISE_HIGH_ISO: nPropIndex = DSC_OPT_DSC_ETC_NOISE_HIGH_ISO; break;
	case PTP_DPC_SAMSUNG_NOISE_LONG_TERM: nPropIndex = DSC_OPT_DSC_ETC_NOISE_LONG_TERM; break;
	case PTP_DPC_SAMSUNG_BLUETOOTH: nPropIndex = DSC_OPT_DSC_ETC_BLUETOOTH; break;
	case PTP_DPC_SAMSUNG_BLUETOOTH_AUTO_TIME: nPropIndex = DSC_OPT_DSC_ETC_BLUETOOTH_AUTO_TIME; break;
	case PTP_DPC_SAMSUNG_MY_SMARTPHONE: nPropIndex = DSC_OPT_DSC_ETC_MY_SMARTPHONE; break;
	case PTP_DPC_SAMSUNG_WIFI_PRIVACY_LOCK: nPropIndex = DSC_OPT_DSC_ETC_WIFI_PRIVACY_LOCK; break;
	case PTP_DPC_SAMSUNG_DUAL_BAND_MOBILE_AP: nPropIndex = DSC_OPT_DSC_ETC_DUAL_BAND_MOBILE_AP; break;
	case PTP_DPC_SAMSUNG_SENSOR_CLEANING_START_UP: nPropIndex = DSC_OPT_DSC_ETC_SENSOR_CLEANING_START_UP; break;
	case PTP_DPC_SAMSUNG_SENSOR_CLEANING_SHUT_DOWN: nPropIndex = DSC_OPT_DSC_ETC_SENSOR_CLEANING_SHUT_DOWN; break;		
	//CMiLRe 20141030 DMF 추가
	case PTP_DPC_SAMSUNG_DMF: nPropIndex = DSC_OPT_DSC_ETC_DMF; break;	
	//CMiLRe 20141114 IOS Auto Range가 Value에서 Enum으로 변경
	case PTP_DPC_SAMSUNG_3D_SHOT: nPropIndex = DSC_OPT_DSC_3D_SHOT; break;
	case PTP_DPC_SAMSUNG_MIN_SHUTTER_SPEED: nPropIndex = DSC_OPT_DSC_MIN_SHUTTER; break;
	case PTP_DPC_SAMSUNG_CENTER_MARKER_DISPLAY: nPropIndex = DSC_OPT_DSC_CENTER_MARKER_DISPLAY; break;
	case PTP_DPC_SAMSUNG_CENTER_MARKER_SIZE: nPropIndex = DSC_OPT_DSC_CENTER_MARKER_SIZE; break;
	case PTP_DPC_SAMSUNG_CENTER_MARKER_TRANSPARENCY: nPropIndex = DSC_OPT_DSC_CENTER_MARKER_TRANSPARENCY; break;
	case PTP_DPC_SAMSUNG_TOUCH_AF: nPropIndex = DSC_OPT_DSC_ETC_TOUCH_AF; break;
	default:									break;
	}

	switch(nPropIndex)
	{
	//CMiLRe 20141114 IOS Auto Range가 Value에서 Enum으로 변경
	case DSC_OPT_DSC_3D_SHOT:
		{
			COptionCustomDlg* dlgOptCustom = (COptionCustomDlg*)m_pOptDlg[OPT_TAB_CUSTOM	];
			dlgOptCustom->SetLensOption3DMode(nPropValue);
			dlgOptCustom->Set3DISORangeShow();
		}
		break;
	case DSC_OPT_CUSTOM_ISOSTEP			:				
	case DSC_OPT_CUSTOM_AUTOISORANGE	:			
	case DSC_OPT_CUSTOM_LDC				:			
	case DSC_OPT_CUSTOM_AFLAMP			:
	//CMiLRe 20140902 ISO Expansion 추가
	case DSC_OPT_CUSTOM_ISOEXPANSION:
	//dh0.seo 2014-09-15 Auto Display Off
	case DSC_OPT_CUSTOM_AUTODISPLAY	:
		{
			COptionCustomDlg* dlgOptCustom = (COptionCustomDlg*)m_pOptDlg[OPT_TAB_CUSTOM	];
			dlgOptCustom->SetOptionProp(nPropIndex, nPropValue, m_pOption->m_Custom);
		}
		break;
	case DSC_OPT_DSC1_FILENAME		:
	case DSC_OPT_DSC1_FILENUMBER	:
	case DSC_OPT_DSC1_FOLDERTYPE	:
	//dh0.seo 2014-09-12 Power Save
	case DSC_OPT_DSC1_POWERSAVE		:
		{
			COptionDSCSet1Dlg* dlgOptDSCSet1 = (COptionDSCSet1Dlg*)m_pOptDlg[OPT_TAB_DSC_SET1	];
			dlgOptDSCSet1->SetOptionProp(nPropIndex, nPropValue, m_pOption->m_DSCSet1);
		}
		break;
	case DSC_OPT_DSC2_DATETIME_SUMMERTIME	:
	case DSC_OPT_DSC2_DATETIME_HOUR			:
	case DSC_OPT_DSC2_DATETIME_IMPRINT		:
	//dh0.seo 2014-09-15 Quick View
	case DSC_OPT_DSC2_QUICKVIEW				:
	//SDK Test
//	case DSC_OPT_DSC2_TEST					:
		{
			COptionDSCSet2Dlg* dlgOptDSCSet2 = (COptionDSCSet2Dlg*)m_pOptDlg[OPT_TAB_DSC_SET2	];
			dlgOptDSCSet2->SetOptionProp(nPropIndex, nPropValue, m_pOption->m_DSCSet2);
		}
		break;
	case DSC_OPT_DSC3_VOL_SYSTEM	:			
	case DSC_OPT_DSC3_VOL_AFSOUND	:		
	case DSC_OPT_DSC3_VOL_BTNSOUND	:
	//CMiLRe 20140901 E Shutter Sound 추가
	case DSC_OPT_DSC3_VOL_ESHSOUND:
	//CMiLRe 20140910 Movie Voice&Size 추가
	case DSC_OPT_DSC3_VOL_VOICE:
	case DSC_OPT_DSC3_MOVIE_SIZE:
		{
			COptionDSCSet3Dlg* dlgOptDSCSet3 = (COptionDSCSet3Dlg*)m_pOptDlg[OPT_TAB_DSC_SET3	];
			dlgOptDSCSet3->SetOptionProp(nPropIndex, nPropValue, m_pOption->m_DSCSet3);
			COptionDSCSetEtcDlg* pOptionEtcDlg = (COptionDSCSetEtcDlg*)m_pOptDlg[OPT_TAB_DSC_SET_ETC];
			pOptionEtcDlg->SetOptionProp(nPropIndex, nPropValue, m_pOption->m_DSCSetEtc);	
		}
		break;
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	case DSC_OPT_DSC_ETC_OLED_COLOR:
	case DSC_OPT_DSC_ETC_MFASSIST:
	case DSC_OPT_DSC_ETC_FOCUS_PEAKING_LEVEL:
	case DSC_OPT_DSC_ETC_FOCUS_PEAKING_COLOR:
	case DSC_OPT_DSC_ETC_BRIGHTNESS_ADJUST_GUIDE:
	case DSC_OPT_DSC_ETC_FRAMING_MODE:
	case DSC_OPT_DSC_ETC_DYNAMIC_RANGE:
	case DSC_OPT_DSC_ETC_LINK_AE_2_AF:
	case DSC_OPT_DSC_ETC_OVER_EXPOSURE:
	//CMiLRe 20140918 Picture Setting 추가
	case DSC_OPT_DSC_ETC_FLASH_USE_WIRELESS:
	case DSC_OPT_DSC_ETC_FLASH_CHANNEL:
	case DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_A_ATTL:
	case DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_A_MANUAL:
	case DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_A:
	case DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_B_ATTL:
	case DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_B_MANUAL:
	case DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_B:
	case DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_C_ATTL:
	case DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_C_MANUAL:
	case DSC_OPT_DSC_ETC_GROUP_FLASH_GROUP_C:

	case DSC_OPT_DSC_ETC_EXT_FLASH_USE_WIRELESS:
	case DSC_OPT_DSC_ETC_EXT_FLASH_CHANNEL:
	case DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_A_ATTL:
	case DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_A_MANUAL:
	case DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_A:
	case DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_B_ATTL:
	case DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_B_MANUAL:
	case DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_B:
	case DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_C_ATTL:
	case DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_C_MANUAL:
	case DSC_OPT_DSC_ETC_EXT_GROUP_FLASH_GROUP_C:

	//CMiLRe 20140918 Movie Setting 추가
	case DSC_OPT_DSC_ETC_MOV_QUALITY:
	case DSC_OPT_DSC_ETC_MOV_MULTI_MOTION:
	case DSC_OPT_DSC_ETC_MOV_FADER:
	case DSC_OPT_DSC_ETC_WIND_CUT:
	case DSC_OPT_DSC_ETC_MIC_MANUAL_VALUE:
	case DSC_OPT_DSC_ETC_MOVIE_SMART_RANGE:
	//CMiLRe 20140929 Custom 추가
	case DSC_OPT_DSC_ETC_DISP_BRIGHT_LEVEL:
	case DSC_OPT_DSC_ETC_DISP_AUTO_BRIGHT:
	case DSC_OPT_DSC_ETC_DISP_COLOR_DETAIL_XY:
	case DSC_OPT_DSC_ETC_VIDEO_OUT:
	case DSC_OPT_DSC_ETC_ANYNET:
	case DSC_OPT_DSC_ETC_HDMI:
	case DSC_OPT_DSC_ETC_3D_HDMI:
	case DSC_OPT_DSC_ETC_LANGUAGE:
	case DSC_OPT_DSC_ETC_MODE_HELP_GUIDE:
	case DSC_OPT_DSC_ETC_FUNCTION_HELP_GUIDE:
	case DSC_OPT_DSC_ETC_TOUCH_OPERATION:
	case DSC_OPT_DSC_ETC_AF_RELEASE_PRIORITY:
	case DSC_OPT_DSC_ETC_EVF_BUTTON_INTERACTION:
	case DSC_OPT_DSC_ETC_GRID_LINE:
	case DSC_OPT_DSC_ETC_NOISE_HIGH_ISO:
	case DSC_OPT_DSC_ETC_NOISE_LONG_TERM:
	case DSC_OPT_DSC_ETC_BLUETOOTH:
	case DSC_OPT_DSC_ETC_BLUETOOTH_AUTO_TIME:
	case DSC_OPT_DSC_ETC_MY_SMARTPHONE:
	case DSC_OPT_DSC_ETC_WIFI_PRIVACY_LOCK:
	case DSC_OPT_DSC_ETC_DUAL_BAND_MOBILE_AP:
	case DSC_OPT_DSC_ETC_SENSOR_CLEANING_START_UP:
	case DSC_OPT_DSC_ETC_SENSOR_CLEANING_SHUT_DOWN:
	case DSC_OPT_DSC_ETC_COLOR_SPACE:
	case DSC_OPT_DSC_ETC_DISTORTION_CORRECT:
	case DSC_OPT_DSC_ETC_WIFI_SENDNG_SIZE:
	case DSC_OPT_DSC_ETC_EFS:
	//CMiLRe 20141001 Setting Battery Selection 추가
	//CMiLRe 20141021 Setting Battery Selection 추가 - Internal, External Battry Level 추가
	case DSC_OPT_DSC_ETC_BATTERY_LEVEL:
	case DSC_OPT_DSC_ETC_BATTERY_LEVEL_EXT:
	case DSC_OPT_DSC_ETC_BATTERY_SELECTION:
	//CMiLRe 20141008 Bracket Settings (AE,WB)
	case DSC_OPT_DSC_ETC_USER_DISP_ICON:
	case DSC_OPT_DSC_ETC_USER_DISP_DATE_TIME:
	case DSC_OPT_DSC_ETC_USER_DISP_BUTTONS:
	case DSC_OPT_DSC_ETC_I_FN_FNO:
	case DSC_OPT_DSC_ETC_I_FN_SHUTTER_SPEED:
	case DSC_OPT_DSC_ETC_I_FN_EV:
	case DSC_OPT_DSC_ETC_I_FN_ISO:
	case DSC_OPT_DSC_ETC_I_FN_WB:
	case DSC_OPT_DSC_ETC_I_FN_I_ZOOM:
	case DSC_OPT_DSC_ETC_BRK_AE_STEP:
	case DSC_OPT_DSC_ETC_BRK_WB_SET:
	case DSC_OPT_DSC_ETC_BRK_PW_TYPE:
	case DSC_OPT_DSC_ETC_BRK_DEPTH:
	//dh0.seo 2014-10-02 Key Mapping
	case DSC_OPT_DSC_ETC_KEY_PREVIEW:
	case DSC_OPT_DSC_ETC_KEY_AEL:
	case DSC_OPT_DSC_ETC_KEY_AF_ON:
	case DSC_OPT_DSC_ETC_KEY_CUSTOM1:
	case DSC_OPT_DSC_ETC_KEY_CUSTOM2:
	case DSC_OPT_DSC_ETC_KEY_CUSTOM3:
	case DSC_OPT_DSC_ETC_KEY_WHEEL:
	case DSC_OPT_DSC_ETC_KEY_DIAL:
	//CMiLRe 20141030 DMF 추가
	case DSC_OPT_DSC_ETC_DMF:
	case DSC_OPT_DSC_MIN_SHUTTER:
	case DSC_OPT_DSC_CENTER_MARKER_DISPLAY: 
	case DSC_OPT_DSC_CENTER_MARKER_SIZE:
	case DSC_OPT_DSC_CENTER_MARKER_TRANSPARENCY:
	case DSC_OPT_DSC_ETC_TOUCH_AF:
		{
			COptionDSCSetEtcDlg* dlgOptDSCSetEtc = (COptionDSCSetEtcDlg*)m_pOptDlg[OPT_TAB_DSC_SET_ETC	];
			dlgOptDSCSetEtc->SetOptionProp(nPropIndex, nPropValue, m_pOption->m_DSCSetEtc);
		}
		break;
	case DSC_OPT_RIME_LAPSE_INTERVAL_CYCLE_TIME_HOUR:
	case DSC_OPT_RIME_LAPSE_INTERVAL_NUM:
	case DSC_OPT_RIME_LAPSE_INTERVAL_BEGINE_TIME_USE:
	case DSC_OPT_RIME_LAPSE_TIMELABPS:
	case DSC_OPT_RIME_LAPSE_INTERVAL_BEGINE_TIME_HOUR:
	case DSC_OPT_RIME_LAPSE_INTERVAL_BEGINE_TIME_MIN:
	//CMiLRe 20140922 Picture Interval Capture 추가
	case DSC_OPT_RIME_LAPSE_INTERVAL_SET:
		{
			COptionTimeLapseDlg* dlgOptDSCSetTimeLapse = (COptionTimeLapseDlg*)m_pOptDlg[OPT_TAB_TIMELAPSE	];
			dlgOptDSCSetTimeLapse->SetOptionProp(nPropIndex, nPropValue, m_pOption->m_TimeLapse);
		}
		break;	
	}

	//CMiLRe 20140922 Picture Interval Capture 추가
	if(m_bChangedDP)
	{
		COptionTimeLapseDlg* dlgOptDSCSetTimeLapse = (COptionTimeLapseDlg*)m_pOptDlg[OPT_TAB_TIMELAPSE	];
		if(dlgOptDSCSetTimeLapse->m_bChangedDP)
		{				
			dlgOptDSCSetTimeLapse->SetModel(GetModel());
			dlgOptDSCSetTimeLapse->SetModelTypeDisplay(GetModel(), TRUE);
		}
		m_bChangedDP = FALSE;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		SetItem
//! @date		2010-06-10
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionBodyDlg::SetItem(int nPTPCode, CString strPropValue)
{
	int nPropIndex = 0;

	//-- Draw Icon
	switch(nPTPCode)
	{
	//-- DSC Set3
	case	PTP_CODE_SAMSUNG_DATETIMETIMEZONE	:	nPropIndex = DSC_OPT_DSC2_DATETIME_TIMEZONE	;	break;
	//CMiLRe 20141001 Date, Time 추가
	case	PTP_OC_SAMSUNG_DATE_TIME					:	nPropIndex = DSC_OPT_DSC2_DATETIME			;	break;
	case	PTP_CODE_SAMSUNG_DATETIMETYPE		:	nPropIndex = DSC_OPT_DSC2_DATETIME_TYPE		;	break;
	default:									break;
	}		

	switch(nPropIndex)
	{
	case DSC_OPT_DSC2_DATETIME_TIMEZONE	:
	case DSC_OPT_DSC2_DATETIME			:
	case DSC_OPT_DSC2_DATETIME_TYPE		:
		{
			COptionDSCSet2Dlg* dlgOptDSCSet2 = (COptionDSCSet2Dlg*)m_pOptDlg[OPT_TAB_DSC_SET2	];
			dlgOptDSCSet2->SetOptionProp(nPropIndex, strPropValue, m_pOption->m_DSCSet2);
		}
		break;	
	}
}

void COptionBodyDlg::UpdateCapturedCnt(int nCnt)
{
	COptionTimeLapseDlg* pOptTimeLapseDlg = (COptionTimeLapseDlg*)m_pOptDlg[OPT_TAB_TIMELAPSE];

	if(pOptTimeLapseDlg)
		pOptTimeLapseDlg->UpdateCapturedCnt(nCnt);
}

void COptionBodyDlg::UpdateRemainTime(int nTime)
{
	COptionTimeLapseDlg* pOptTimeLapseDlg = (COptionTimeLapseDlg*)m_pOptDlg[OPT_TAB_TIMELAPSE];

	if(pOptTimeLapseDlg)
		pOptTimeLapseDlg->UpdateRemainTime(nTime);
}

void COptionBodyDlg::CloseStatusDialog()
{
	COptionTimeLapseDlg* pOptTimeLapseDlg = (COptionTimeLapseDlg*)m_pOptDlg[OPT_TAB_TIMELAPSE];

	if(pOptTimeLapseDlg)
		pOptTimeLapseDlg->CloseStatusDialog();
}
void COptionBodyDlg::SetMode(int nMode)
{

	COptionTimeLapseDlg* pOptTimeLapseDlg = (COptionTimeLapseDlg*)m_pOptDlg[OPT_TAB_TIMELAPSE	];
	COptionCustomDlg* pOptCustomDlg = (COptionCustomDlg*)m_pOptDlg[OPT_TAB_CUSTOM];
	COptionDSCSetEtcDlg* pOptionEtcDlg = (COptionDSCSetEtcDlg*)m_pOptDlg[OPT_TAB_DSC_SET_ETC];

	m_nMode = nMode;
	pOptTimeLapseDlg->SetMode(m_nMode);
	pOptCustomDlg->SetMode(m_nMode);
	pOptionEtcDlg->SetMode(m_nMode);
	pOptCustomDlg->SetUpdate();
	pOptionEtcDlg->SetUpdate();
}

//CMiLRe 20141124 인터벌 캡쳐 디바이스에서 종료시 스타트 버튼 활성화
COptionBaseDlg* COptionBodyDlg::GetDlgTimeLap() 
{
	COptionTimeLapseDlg* pOptTimeLapseDlg = (COptionTimeLapseDlg*)m_pOptDlg[OPT_TAB_TIMELAPSE	];
	return m_pOptDlg[OPT_TAB_TIMELAPSE	];
}

COptionBaseDlg* COptionBodyDlg::StartInterval()
{
	COptionTimeLapseDlg* dlgOptTimeLapse = (COptionTimeLapseDlg*)m_pOptDlg[OPT_TAB_TIMELAPSE	];
	dlgOptTimeLapse->IntervalCapture();
	return 0;
}