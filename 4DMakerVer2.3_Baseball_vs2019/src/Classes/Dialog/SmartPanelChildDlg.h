/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelChildDlg.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "BkDialogST.h"
#include "RSButton.h"
#include "RSLayoutInfo.h"
#include "SmartPanelWBDetailDlg.h"
#include "SmartPanelWBColorDetailDlg.h"
#include "SmartPanelPWDetailDlg.h"
//CMiLRe NX1 SmartWizard Detail 추가
#include "SmartPanelPWDetailDlg_Color_NX1.h"
//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
#include "SmartPanelPWDetailDlg_Saturation_NX1.h"
#include "SmartPanelPWDetailDlg_Sharpness_NX1.h"
#include "SmartPanelPWDetailDlg_Contrast_NX1.h"
#include "SmartPanelPWDetailDlg_Hue_NX1.h"
#include "SmartPanelDriveDetailDlg.h"
#include "NXRemoteStudioFunc.h"
#include "SmartPanelFlashDetailDlg.h"

// CSmartPanelChildDlg dialog

//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
typedef enum
{	
	PW_DETAIL_COLOR = 0,
	PW_DETAIL_SATURATION,
	PW_DETAIL_SHARPNESS,
	PW_DETAIL_CONTRACT,
	PW_DETAIL_HUE,
}PW_DETAIL_TYPE;

class CSmartPanelChildDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CSmartPanelChildDlg)

public:
	CSmartPanelChildDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSmartPanelChildDlg();

	// Dialog Data
	enum { IDD = IDD_DLG_SMARTPANEL_CHILD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
private:
	//---------------------------------------------------------------
	//-- Init Smart Panel
	//---------------------------------------------------------------
	//-- Create Frame
	void CreateFrame(CString strPath);
	//-- Move Smart Pannel
	void SetChildWindowPos(int nCnt);

	//---------------------------------------------------------------
	//-- Control Array Info
	//---------------------------------------------------------------
	int AddCtrlInfo(ControlTypeInfo* controlTypeInfo);
	BOOL RemoveCtrlInfoAll();
	int GetCtrlInfoCount();
	ControlTypeInfo* GetCtrlInfo(int nIndex);

	//---------------------------------------------------------------
	//-- Button Control
	//---------------------------------------------------------------
	void CreateBtnCtrl(CRSPropertyValue* rsPropertyValue, int nPropValue);

	//---------------------------------------------------------------
	//-- Device Property
	//---------------------------------------------------------------
	//-- Get Current Status
	int GetPropValueStatus(int nPropIndex, int nPropValue);

	//-- 2011-8-12 Lee JungTaek
	//- Change Status
	void ChangePreSelectedItemStatus(int nValueIndex);
	void ChangeSelectedItemStatus(int nValueIndex);
	//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
	void ChangeFocustemStatus(int nValueIndex, BOOL bChabge = TRUE);
	
	BOOL ChangeSelectedItemStatus(int nValueIndex, BOOL bWheelUp);
	BOOL CheckFirstLastValue(int nValueIndex, BOOL bWheelUp);

	//---------------------------------------------------------------
	//-- Detail 
	//---------------------------------------------------------------
	void CreateAdjustDlg(int nValueIndex);
	void CreateWBDetail(int nValueIndex);
	void CreateWBColorDetail(int nValueIndex);
	void CreatePWDetail(int nValueIndex);
	//CMiLRe NX1 SmartWizard Detail 추가
	//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
	PW_DETAIL_TYPE nPWType;
	void CreatePWDetail_Color_NX1(int nValueIndex);	
	void CreatePWDetail_Saturation_NX1(int nValueIndex);	
	void CreatePWDetail_Sharpness_NX1(int nValueIndex);	
	void CreatePWDetail_Contrast_NX1(int nValueIndex);	
	void CreatePWDetail_Hue_NX1(int nValueIndex);
	//dh0.seo Flash Detail 추가 2014-11-24
	void CreateFlashDetail(int nValueIndex);
	
	void CreateDriveDetail(int nValueIndex, int nType);

	//-- 2011-8-30 Lee JungTaek
	int GetButtonPos(int nPropValue);

	void MouseEvent();

public:
	//-- Close Window
	void CloseSmartPanelChild();
	//-- Change PW Detail Window
	//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
	void ChangeSmartPanelPWChild(PW_DETAIL_TYPE nType, int nValueIndex);
	//---------------------------------------------------------------
	//-- Button Control
	//---------------------------------------------------------------
	void CreateControlByType(int nCurProperty, CRSLayoutInfo& arrRSArray);	
	//-- Selected Image Path
	void SetSelectedPropValue(int nValueIndex);	
	//---------------------------------------------------------------
	//-- Property Value Info
	//---------------------------------------------------------------
	void SetSelectedProperty(int nCtrlID) {m_n1DepthCtrlID = nCtrlID;}
	void SetParentRect(CRect rectParentCtrl) {m_rectParentCtrl = rectParentCtrl;}		
	void SetDetailProperty(int nPropType, int nValue);

	int GetSelected1stValue()	{return m_nSelected1st;}
	CString m_strModel;

private:
	CButtonST m_btnAdjust;
	CButtonST m_btnOK;

	BOOL m_bDetailSetting;

	CRSLayoutInfo* m_rsLayoutInfo;
	CRSArray m_arrCtrlInfo;

	//-- Set Parent Ctrl Rect
	int m_n1DepthCtrlID;
	ControlTypeInfo * m_pParentCtrl;
	CRect m_rectParentCtrl;

	BOOL m_bMoveIn;
	int m_nMouseIn;
	int m_nPropType;
	
	int m_nSelectedIndex;
	//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
	int m_nFocusIndex;
	int m_nValueCnt;
	int m_nSelected1st;
	int m_nValueIndex;

	CSmartPanelWBDetailDlg * m_pSmartPanelWBDetail;
	CSmartPanelWBColorDetailDlg * m_pSmartPanelWBColorDetail;
	CSmartPanelPWDetailDlg * m_pSmartPanelPWDetail;
	//CMiLRe NX1 SmartWizard Detail 추가
	CSmartPanelPWDetailDlg_Color_NX1 * m_pSmartPanelPWDetail_Color_NX1;	
	//CMiLRe 20140911 NX1 SmartWizard PW Detail  추가
	CSmartPanelPWDetailDlg_Saturation_NX1* m_pSmartPanelPWDetail_Saturation_NX1;
	CSmartPanelPWDetailDlg_Sharpness_NX1* m_pSmartPanelPWDetail_Sharpness_NX1;
	CSmartPanelPWDetailDlg_Contrast_NX1* m_pSmartPanelPWDetail_Contrast_NX1;
	CSmartPanelPWDetailDlg_Hue_NX1* m_pSmartPanelPWDetail_Hue_NX1;

	CSmartPanelDriveDetailDlg * m_pSmartPanelDriveDetail;

	//dh0.seo Flash Detail 추가 2014-11-24
	CSmartPanelFlashDetailDlg* m_pSmartPanelFlashDetailDlg;
};
