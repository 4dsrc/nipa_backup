/////////////////////////////////////////////////////////////////////////////
//
//  SmartPanelDlg.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudioDlg.h"
#include "SmartPanelDlg.h"
#include "RSButton.h"
#include "RSSliderCtrl.h"
#include "RSBarometerCtrl.h"
#include "RSModeSelectCtrl.h"
#include "RSMagicFilterSelectCtrl.h"
#include "SdiDefines.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CSmartPanelDlg dialog
static const int timer_start_check_mode	= WM_USER + 0x0001;
IMPLEMENT_DYNAMIC(CSmartPanelDlg, CDialogEx)

CSmartPanelDlg::CSmartPanelDlg(CWnd* pParent /*=NULL*/) : CBkDialogST(CSmartPanelDlg::IDD, pParent) 
{
	m_pMultiApplyDlg = NULL;
	//m_strModel = _T("NX2000");
	m_strMode = _T("");
	m_nMode = DSC_MODE_NULL;
	m_pSmartPanelChild = NULL;
	m_pSmartBMChildDlg = NULL;
	m_pSmartSDChildDlg = NULL;
	m_pSmartMSChildDlg = NULL;
	m_nPrePropIndex = -1;
	m_nSelectPTPCode = -1;
	m_nMainCamera = 0;
	m_bConnect = TRUE;
	//-- SmartPanel Style
	m_bSmartStyle = TRUE;
	//-- 2011-7-14 Lee JungTaek
	m_pSaveBankEdit = NULL;
	m_strPropDesc = _T("");
	m_rectPropDesc = CRect(CPoint(8 , 30), CSize(MAIN_WND_WIDTH - 16, 25));
	m_bApplyMode = FALSE;
	//-- 2011-8-5 Lee JungTaek
	m_bCapturing = FALSE;
	m_bEnable = TRUE;
	m_nChangePTPCode = 0;
}
CSmartPanelDlg::~CSmartPanelDlg()
{
	TRACE(_T("Destroy:CSmartPanelDlg Start\n"));
	RemoveCtrlInfoAll();

	CloseSmartPanelChild();

	if(m_pSaveBankEdit)
	{
		delete m_pSaveBankEdit;
		m_pSaveBankEdit = NULL;
	}

	if(m_pMultiApplyDlg)
	{
		delete m_pMultiApplyDlg;
		m_pMultiApplyDlg = NULL;
	}

	TRACE(_T("Destroy:CSmartPanelDlg End\n"));
}

void CSmartPanelDlg::DoDataExchange(CDataExchange* pDX)
{
	CBkDialogST::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_SP_LOADBANK, m_btnLoadBank);
	DDX_Control(pDX, IDC_BTN_SP_SAVEBANK, m_btnSaveBank);
	DDX_Control(pDX, IDC_BTN_SP_MULTIAPPLY, m_btnMultiApply);
	DDX_Control(pDX, IDC_BTN_SP_HELP, m_btnHelp);
}

BEGIN_MESSAGE_MAP(CSmartPanelDlg, CBkDialogST)
	ON_WM_PAINT()	
	ON_WM_NCHITTEST()
	ON_WM_TIMER()	
END_MESSAGE_MAP()

//------------------------------------------------------------------------------ 
//! @brief		SetPropertyValue
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Close Smart Panel Child
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::CloseSmartPanelChild()
{
	if(m_pSmartPanelChild)
	{
		m_pSmartPanelChild->CloseSmartPanelChild();
		delete m_pSmartPanelChild;
		m_pSmartPanelChild =NULL;
	}
	
	if(m_pSmartBMChildDlg)
	{
		delete m_pSmartBMChildDlg;
		m_pSmartBMChildDlg = NULL;
	}

	if(m_pSmartSDChildDlg)
	{
		delete m_pSmartSDChildDlg;
		m_pSmartSDChildDlg = NULL;
	}

	if(m_pSmartMSChildDlg)
	{
		delete m_pSmartMSChildDlg;
		m_pSmartMSChildDlg = NULL;
	}
}

BOOL CSmartPanelDlg::RemoveCtrlInfoAll()
{
	ControlTypeInfo* pExist = NULL;
	int nAll = GetCtrlInfoCount();

	while(nAll--)
	{
		pExist = (ControlTypeInfo*)GetCtrlInfo(nAll);

		if(pExist)
		{
			switch(pExist->nType)
			{
			case SMARTPANEL_CONTROL_BUTTON:
				{
					CRSButton* pRSButton = (CRSButton*)pExist->pRSCtrl;
					if(pRSButton)
					{
						delete pRSButton;
						pRSButton = NULL;
					}
				}
				break;	
			case SMARTPANEL_CONTROL_BAROMETER:
				{
					CRSBarometerCtrl* pRSBarometer = (CRSBarometerCtrl*)pExist->pRSCtrl;
					if(pRSBarometer)
					{
						delete pRSBarometer;
						pRSBarometer = NULL;
					}					
				}
				break;
			case SMARTPANEL_CONTROL_SLIDER:
				{
					CRSSliderCtrl* pRSSlider = (CRSSliderCtrl*)pExist->pRSCtrl;
					if(pRSSlider)
					{
						delete pRSSlider;
						pRSSlider = NULL;
					}
				}
				break;
			case SMARTPANEL_CONTROL_MODESELECT:
				{
					CRSModeSelectCtrl* pRSModeSelect = (CRSModeSelectCtrl*)pExist->pRSCtrl;
					if(pRSModeSelect)
					{
						delete pRSModeSelect;
						pRSModeSelect = NULL;
					}
				}
				break;
			case SMARTPANEL_CONTROL_MAGICMODE:
				{
					CRSMagicFilterSelectCtrl* pRSMagicMode = (CRSMagicFilterSelectCtrl*)pExist->pRSCtrl;
					if(pRSMagicMode)
					{
						delete pRSMagicMode;
						pRSMagicMode = NULL;
					}
				}
				break;
			}

			delete pExist;
			pExist = NULL;

			m_arrCtrlInfo.RemoveAt(nAll);
		}
		else
			return FALSE;
	}

	//-- CHECK REMOVE ALL
	m_arrCtrlInfo.RemoveAll();
	return TRUE;
}

//-- Return Button Count
int CSmartPanelDlg::GetCtrlInfoCount()
{
	return (int)m_arrCtrlInfo.GetSize();
}

//-- Return Specific Button Control
ControlTypeInfo* CSmartPanelDlg::GetCtrlInfo(int nIndex)
{
	if(nIndex < GetCtrlInfoCount())
		return (ControlTypeInfo*)m_arrCtrlInfo.GetAt(nIndex);
	return NULL;
}

//-- Add Each Button Control
int CSmartPanelDlg::AddCtrlInfo(ControlTypeInfo* controlTypeInfo)
{
	return (int)m_arrCtrlInfo.Add((CObject*)controlTypeInfo);
}

// CSmartPanelDlg message handlers
BOOL CSmartPanelDlg::OnInitDialog() 
{
	TRACE(_T("[SP] OninitDialog\n"));
	CBkDialogST::OnInitDialog();

	GetWindowRect(&m_rect);	
	
	TRACE(_T("[SP] UpdateSmartPanel Dialog\n"));
	UpdateDialog();
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
int m_gCount = 0;
BOOL CSmartPanelDlg::UpdateDialog(BOOL bModeChange, BOOL bFirst) 
{
	
	RSSetLogFlag(FALSE);
	TRACE(_T("UpdateDialog(%d)\n"),bModeChange);
	//-- 2011-5-20 Lee JungTaek
	//-- Create Frame
	ShowControlAll(FALSE);
	//-- 2011-7-14 Lee JungTaek
	ClearPreInfo();
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	m_nMainCamera = pMainWnd->GetSelectedItem();

	//-- 2013-07-03 SetTagetModel ymlee
	SetTargetModel(pMainWnd->m_SdiMultiMgr.GetCurModel(m_nMainCamera));
	m_rsLayoutInfo.SetTargetModel(GetTargetModel());

	int nModeIndex = pMainWnd->GetModeIndex(m_nMainCamera, bFirst);
	SetTargetMode(nModeIndex);
	CreateFrame();	
	
	//-- 2011-8-26 Lee JungTaek
	//-- Modify
	if(!m_bConnect)
	{
		Invalidate();
		return FALSE;
	}
	else
	{
		if(!m_bApplyMode)
		{
			pMainWnd->GetIndicator(bModeChange);
			Invalidate();
			return FALSE;
		}
	}

	//-- 2011-5-16 Lee JungTaek
	//-- Load Layout Info
	LoadCameraInfoFile();
	//-- Create Control
	CreateControlByType();	
	// 	//-- Get Device Property
	GetDevicePropDesc(bModeChange);
	//-- 2011-7-6 Lee JungTaek
	ShowControlAll();

	/*CString strTemp = RSGetShutterVal();

	int nFind = strTemp.Find(_T("Bulb"));
	if(nFind != 0)*/
		RSSetTimeInit();
	
	return TRUE;  // return TRUE unless you set the focus to a control
}

//------------------------------------------------------------------------------ 
//! @brief		ClearPreInfo
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	Clear Pre Layout
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::ClearPreInfo()
{
	m_rsLayoutInfo.RemoveLayoutSubInfoClass();
	m_rsLayoutInfo.RemoveLayoutInfo();
	RemoveCtrlInfoAll();
	m_nPrePropIndex = -1;
	m_nSelectPTPCode = -1;
	//-- 2011-8-5 Lee JungTaek
	m_bCapturing = FALSE;
	//-- 2011-8-9 Lee JungTaek
	CloseMultiApplyDlg();
	m_strPropDesc = _T("");
}

//------------------------------------------------------------------------------ 
//! @brief		OnPaint
//! @date		2011-7-13
//! @author		Lee JungTaek
//! @note	 	Draw Title Bar & Property Info
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::OnPaint()
{	
	CPaintDC dc(this);
//	DrawDlgTitle(&dc, _T("Smart Panel"), RGB(45, 45,45));
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Smart Panel"), 5, 5, RGB(45,45,45));
	//-- Get Center Pos
	CSize szStr;
	szStr = dc.GetOutputTextExtent(m_strPropDesc);
	int x = MAIN_WND_WIDTH / 2 - szStr.cx / 2;
	int y = 30;

// 	CDC *pDC = &dc;
// 	CBrush brush = RGB_DEFAULT_DLG;
// 	pDC->FillRect(m_rectPropDesc, &brush);

	if(m_bConnect)
	{
		if(m_bApplyMode)
			DrawStr(&dc, STR_SIZE_MIDDLE, m_strPropDesc, x, y, RGB(244,245,255));
	}
	else
	{
		DrawStr(&dc, STR_SIZE_MIDDLE, _T(""), 80, 50, RGB(244,245,255));
	}
}

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Create Frame
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::CreateFrame()
{
	
	//-- 2011-5-31 Lee JungTaek
	//-- Temp Code
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	int nCnt = pMainWnd->m_SdiMultiMgr.GetPTPCount();

	this->SetBackgroundColor(RGB(255,255,255));

	if(!nCnt)
	{
		//-- Disconnect
		this->SetBackgroundImage(IDR_IMG_BG_SMARTPANEL1_DIS, CDialogEx::BACKGR_TOPLEFT);
		m_btnLoadBank.SetBitmaps(IDR_IMG_SP_NOIMAGE, NULL);
		m_btnSaveBank.SetBitmaps(IDR_IMG_SP_NOIMAGE, NULL);
		m_btnMultiApply.SetBitmaps(IDR_IMG_SP_NOIMAGE, NULL);
		m_btnHelp.SetBitmaps(IDR_IMG_SP_NOIMAGE, NULL);

		m_btnLoadBank.ShowWindow(SW_HIDE);
		m_btnSaveBank.ShowWindow(SW_HIDE);
		m_btnHelp.ShowWindow(SW_HIDE);
//		m_btnMultiApply.ShowWindow(SW_HIDE); //dh0.seo 2014-09-29 주석

		m_bConnect = FALSE;
		m_bApplyMode = FALSE;
	}
	else
	{
		if(m_bApplyMode)
		{
			this->SetBackgroundImage(IDR_IMG_BG_SMARTPAENL1, CDialogEx::BACKGR_TOPLEFT);
			m_btnLoadBank.SetBitmaps(RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_menu_btn_focus.png")), RGB(255,255,255), RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_menu_btn_normal.png")), RGB(255,255,255));
			m_btnSaveBank.SetBitmaps(RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_menu_btn_focus.png")), RGB(255,255,255), RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_menu_btn_normal.png")), RGB(255,255,255));
			m_btnMultiApply.SetBitmaps(RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_menu_btn_focus.png")), RGB(255,255,255), RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_menu_btn_normal.png")), RGB(255,255,255));
			m_btnHelp.SetBitmaps(RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_menu_btn_focus.png")), RGB(255,255,255), RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_menu_btn_normal.png")), RGB(255,255,255));
			
			m_btnLoadBank.SetUserText(_T("Load Mode"));
			m_btnSaveBank.SetUserText(_T("Save Mode"));
			m_btnHelp.SetUserText(_T("Help"));
//			m_btnMultiApply.SetUserText(_T("Multi Apply")); //dh0.seo 2014-09-29 주석
//			m_btnLoadBank.ShowWindow(SW_SHOW);//dh0.seo 2015-01-20 삭제
//			m_btnSaveBank.ShowWindow(SW_SHOW);//dh0.seo 2014-01-20 삭제
//			m_btnMultiApply.ShowWindow(SW_SHOW);
			m_btnHelp.ShowWindow(SW_SHOW);

			m_bConnect = TRUE;
		}
		else
		{
			this->SetBackgroundImage(IDR_IMG_BG_SMARTPANEL1_NO_SUPPORT, CDialogEx::BACKGR_TOPLEFT);
			m_btnLoadBank.SetBitmaps(IDR_IMG_SP_NOIMAGE, NULL);
			m_btnSaveBank.SetBitmaps(IDR_IMG_SP_NOIMAGE, NULL);
			m_btnMultiApply.SetBitmaps(IDR_IMG_SP_NOIMAGE, NULL);
			m_btnHelp.SetBitmaps(IDR_IMG_SP_NOIMAGE, NULL);

			m_btnLoadBank.ShowWindow(SW_HIDE);
			m_btnSaveBank.ShowWindow(SW_HIDE);
			m_btnHelp.ShowWindow(SW_HIDE);
//			m_btnMultiApply.ShowWindow(SW_HIDE); //dh0.seo 2014-09-29 주석

			m_bConnect = TRUE;
		}
		
	}

	//-- Move Window
	CRect rect;
	((CNXRemoteStudioDlg*)GetParent())->GetWindowRect(&rect);
	this->MoveWindow(rect.left, rect.top + rect.Height(), MAIN_WND_WIDTH, SMART_PANEL_HEIGHT);

	m_btnLoadBank.MoveWindow(14, 253, 89, 24);
	m_btnSaveBank.MoveWindow(107, 253, 89, 24);
	m_btnHelp.MoveWindow(200, 253, 89, 24);
//	m_btnMultiApply.MoveWindow(200, 253, 89, 24); //dh0.seo 2014-09-29 주석
}

//------------------------------------------------------------------------------ 
//! @brief		LoadCameraInfoFile
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Load SmartPanel Layout Using Model / Mode
//------------------------------------------------------------------------------
void CSmartPanelDlg::LoadCameraInfoFile()
{
	CString strPath = _T("");

	switch(m_nMode)
	{
	case DSC_MODE_P:
	case DSC_MODE_A:
	case DSC_MODE_S:
	case DSC_MODE_BULB					://	break; dh0.seo 2015-01-12 위치 이동
	case DSC_MODE_M:	strPath.Format(_T("%s\\%s\\%s\\%s_MODE_PASM.info"), RSGetRoot(), STR_CONFIG_LAYOUT_PATH, m_strModel, m_strModel);	break;
	case DSC_MODE_SMART:	
	case DSC_MODE_I						:			
	case DSC_MODE_CAPTURE_MOVIE			:	
	case DSC_MODE_MAGIC_MAGIC_FRAME		:
	case DSC_MODE_MAGIC_SMART_FILTER	:
	case DSC_MODE_PANORAMA				:	
	case DSC_MODE_SCENE_BEAUTY			:	
	case DSC_MODE_SCENE_NIGHT			:	
	case DSC_MODE_SCENE_LANDSCAPE		:	
	case DSC_MODE_SCENE_PORTRAIT		:	
	case DSC_MODE_SCENE_CHILDREN		:	
	case DSC_MODE_SCENE_SPORTS			:	
	case DSC_MODE_SCENE_CLOSE_UP		:	
	case DSC_MODE_SCENE_TEXT			:	
	case DSC_MODE_SCENE_SUNSET			:	
	case DSC_MODE_SCENE_DAWN			:	
	case DSC_MODE_SCENE_BACKLIGHT		:	
	case DSC_MODE_SCENE_FIREWORK		:	
	case DSC_MODE_SCENE_BEACH_SNOW		:	
	case DSC_MODE_SCENE_SOUND_PICTURE	:	
	case DSC_MODE_SCENE_3D				:	
	case DSC_MODE_SCENE_BEST			:
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_ACTION			:
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
	case DSC_MODE_SCENE_CREATIVE		:
	case DSC_MODE_SCENE_PANORAMA		:
	//dh0.seo 2014-07-29
	case DSC_MODE_SCENE_ACTION_FREEZE	:
	case DSC_MODE_SCENE_LIGHT_TRACE		:
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
	case DSC_MODE_MOVIE					:	strPath.Format(_T("%s\\%s\\%s\\%s_MODE_%s.info"), RSGetRoot(), STR_CONFIG_LAYOUT_PATH, m_strModel, m_strModel, m_strMode);	break;
	}

	//2014-10-18 dh0.seo 추가

	if(m_pSmartSDChildDlg)
		m_pSmartSDChildDlg->ShowWindow(SW_HIDE);
	if(m_pSmartBMChildDlg)
		m_pSmartBMChildDlg->ShowWindow(SW_HIDE);
	if(m_pSmartMSChildDlg)
		m_pSmartMSChildDlg->ShowWindow(SW_HIDE);

	m_rsLayoutInfo.LoadInfo(strPath, m_strModel, m_strMode);
}

//------------------------------------------------------------------------------ 
//! @brief		LoadCameraInfoFile
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Create Controls by Control Type
//------------------------------------------------------------------------------
void CSmartPanelDlg::CreateControlByType()
{
	for(int i = 0; i < m_rsLayoutInfo.GetLayoutCount(); i++)
	{
		CRSPropertyLayout* pPropertyLayout = (CRSPropertyLayout*)m_rsLayoutInfo.GetLayoutInfo(i);
		
		switch(pPropertyLayout->m_nType)
		{
		case SMARTPANEL_CONTROL_BUTTON:
			CreateBtnCtrl(pPropertyLayout, i);
			break;
		case SMARTPANEL_CONTROL_BAROMETER:
			CreateBarometerCtrl(pPropertyLayout, i);
			break;
		case SMARTPANEL_CONTROL_SLIDER:
 			CreateSliderCtrl(pPropertyLayout, i);
			break;
		case SMARTPANEL_CONTROL_MODESELECT:
			CreateModeSelectCtrl(pPropertyLayout, i);
			break;
		case SMARTPANEL_CONTROL_MAGICMODE:
			CreateMagicModeCtrl(pPropertyLayout, i);
			break;
		}
	}
}

//------------------------------------------------------------------------------ 
//! @brief		GetDevicePropDesc
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Get Device Property
//------------------------------------------------------------------------------
void CSmartPanelDlg::GetDevicePropDesc(BOOL bModeChange)
{
	TRACE(_T("[SP] GetDevicePropDesc Start [%d]\n"),bModeChange);
  	((CNXRemoteStudioDlg*)GetParent())->GetIndicator(bModeChange);
  	((CNXRemoteStudioDlg*)GetParent())->GetSmartPanel();		
	//((CNXRemoteStudioDlg*)GetParent())->GetDSCOption();	
	TRACE(_T("[SP] GetDevicePropDesc End\n"));
}

//------------------------------------------------------------------------------ 
//! @brief		CreateBtnCtrl
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Create Button Ctrl
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::CreateBtnCtrl(CRSPropertyLayout* pPropertyLayout, int nPropIndex)
{
	//-- Create Button
	CRSButton* ctrlButton = new CRSButton(BTN_BK_TYPE_1ST);
	ctrlButton->Create(NULL, WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, pPropertyLayout->m_rectProperty, this,ID_BTN_SMARTPANEL+nPropIndex);
	ctrlButton->DrawTransparent(TRUE);
	ctrlButton->DrawBorder(FALSE, TRUE);

	ctrlButton->ShowWindow(SW_HIDE);

	ControlTypeInfo* controlTypeInfo = new ControlTypeInfo;
	controlTypeInfo->nType = SMARTPANEL_CONTROL_BUTTON;
	controlTypeInfo->pRSCtrl = (CObject*)ctrlButton;

	AddCtrlInfo(controlTypeInfo);
}

//------------------------------------------------------------------------------ 
//! @brief		CreateBarometerCtrl
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Create Button Ctrl
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::CreateBarometerCtrl(CRSPropertyLayout* pPropertyLayout, int nPropIndex)
{
	CRSBarometerCtrl *pCtrlBarometer = new CRSBarometerCtrl();
	pCtrlBarometer->Create(_T(""), WS_VISIBLE | WS_CHILD | SS_NOTIFY, pPropertyLayout->m_rectProperty, this, ID_BTN_SMARTPANEL + nPropIndex);
	pCtrlBarometer->SetPropInfo(nPropIndex, pPropertyLayout->m_strPropertyName);
	pCtrlBarometer->UpdateBarometerCtrl();

	pCtrlBarometer->ShowWindow(SW_HIDE);

	ControlTypeInfo* controlTypeInfo = new ControlTypeInfo;
	controlTypeInfo->nType = SMARTPANEL_CONTROL_BAROMETER;
	controlTypeInfo->pRSCtrl = (CObject*)pCtrlBarometer;

	AddCtrlInfo(controlTypeInfo);
}

//------------------------------------------------------------------------------ 
//! @brief		CreateSliderCtrl
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Create Button Ctrl
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::CreateSliderCtrl(CRSPropertyLayout* pPropertyLayout, int nPropIndex)
{
	CRSSliderCtrl *pCtrlSlider = new CRSSliderCtrl();
	pCtrlSlider->Create(_T(""), WS_VISIBLE | WS_CHILD | SS_NOTIFY, pPropertyLayout->m_rectProperty, this, ID_BTN_SMARTPANEL + nPropIndex);
	pCtrlSlider->SetPropInfo(nPropIndex, pPropertyLayout->m_strPropertyName);

	pCtrlSlider->ShowWindow(SW_HIDE);

	ControlTypeInfo* controlTypeInfo = new ControlTypeInfo;
	controlTypeInfo->nType = SMARTPANEL_CONTROL_SLIDER;
	controlTypeInfo->pRSCtrl = (CObject*)pCtrlSlider;

	AddCtrlInfo(controlTypeInfo);
}

//------------------------------------------------------------------------------ 
//! @brief		CreateModeSelectCtrl
//! @date		2011-7-23
//! @author		Lee JungTaek
//! @note	 	Create Mode Select Control
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::CreateModeSelectCtrl(CRSPropertyLayout* pPropertyLayout, int nPropIndex)
{
	CRSModeSelectCtrl *pModeSelect = new CRSModeSelectCtrl();
	pModeSelect->Create(_T(""), WS_VISIBLE | WS_CHILD | SS_NOTIFY, pPropertyLayout->m_rectProperty, this, ID_BTN_SMARTPANEL + nPropIndex);
	pModeSelect->SetPropInfo(nPropIndex, pPropertyLayout->m_strPropertyName);

	pModeSelect->ShowWindow(SW_HIDE);
	 
	ControlTypeInfo* controlTypeInfo = new ControlTypeInfo;
	controlTypeInfo->nType = SMARTPANEL_CONTROL_MODESELECT;
	controlTypeInfo->pRSCtrl = (CObject*)pModeSelect;

	AddCtrlInfo(controlTypeInfo);
}

//------------------------------------------------------------------------------ 
//! @brief		CreateMagicModeCtrl
//! @date		2011-7-23
//! @author		Lee JungTaek
//! @note	 	Create Mode Select Control
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::CreateMagicModeCtrl(CRSPropertyLayout* pPropertyLayout, int nPropIndex)
{
	CRSMagicFilterSelectCtrl *pMagicMode = new CRSMagicFilterSelectCtrl();
	pMagicMode->Create(_T(""), WS_VISIBLE | WS_CHILD | SS_NOTIFY, pPropertyLayout->m_rectProperty, this, ID_BTN_SMARTPANEL + nPropIndex);
	pMagicMode->SetPropInfo(nPropIndex, pPropertyLayout->m_strPropertyName);

	pMagicMode->ShowWindow(SW_HIDE);

	ControlTypeInfo* controlTypeInfo = new ControlTypeInfo;
	controlTypeInfo->nType = SMARTPANEL_CONTROL_MAGICMODE;
	controlTypeInfo->pRSCtrl = (CObject*)pMagicMode;

	AddCtrlInfo(controlTypeInfo);
}

//------------------------------------------------------------------------------ 
//! @brief		ShowControlAll
//! @date		2011-7-6
//! @author		Lee JungTaek
//! @note	 	Show Control All
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::ShowControlAll(BOOL bShow)
{
	TRACE(_T("CSmartPanelDlg::ShowControlAll Start\n"));
	//-- Check None Button Property
	for(int i = 0; i < GetCtrlInfoCount(); i++)
	{
		ControlTypeInfo* controlTypeInfo = GetCtrlInfo(i);

		if(controlTypeInfo)
		{
			int nType = controlTypeInfo->nType;

			switch(nType)
			{
			case SMARTPANEL_CONTROL_BUTTON:
				{
					CRSButton *pRSButton = (CRSButton*)controlTypeInfo->pRSCtrl;
					if(bShow)
						pRSButton->ShowWindow(SW_SHOW);
					else
						pRSButton->ShowWindow(SW_HIDE);
				}				
				break;
			case SMARTPANEL_CONTROL_BAROMETER:
				{
					CRSBarometerCtrl *pRSBarometer = (CRSBarometerCtrl*)controlTypeInfo->pRSCtrl;
					if(bShow)
						pRSBarometer->ShowWindow(SW_SHOW);
					else
						pRSBarometer->ShowWindow(SW_HIDE);
				}				
				break;
			case SMARTPANEL_CONTROL_SLIDER:
				{
					CRSSliderCtrl *pRSSlider = (CRSSliderCtrl*)controlTypeInfo->pRSCtrl;
					if(bShow)
						pRSSlider->ShowWindow(SW_SHOW);
					else
						pRSSlider->ShowWindow(SW_HIDE);
				}
				break;		
			case SMARTPANEL_CONTROL_MODESELECT:
				{
					CRSModeSelectCtrl *pModeSelect = (CRSModeSelectCtrl*)controlTypeInfo->pRSCtrl;
					if(bShow)
						pModeSelect->ShowWindow(SW_SHOW);
					else
						pModeSelect->ShowWindow(SW_HIDE);
				}
				break;	
			case SMARTPANEL_CONTROL_MAGICMODE:
				{
					CRSMagicFilterSelectCtrl *pMagicMode = (CRSMagicFilterSelectCtrl*)controlTypeInfo->pRSCtrl;
					if(bShow)
						pMagicMode->ShowWindow(SW_SHOW);
					else
						pMagicMode->ShowWindow(SW_HIDE);
				}
				break;	
			}
		}
	}	
	TRACE(_T("CSmartPanelDlg::ShowControlAll End\n"));
}

//------------------------------------------------------------------------------ 
//! @brief		CreateSmartPanelChild
//! @date		2011-05-23
//! @author		Lee JungTaek
//! @note	 	Create 2Depth SmartPannel
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::CreateSmartPanelChild(int nCtrlID, BOOL bShow)
{
	//-- Create Smart Panel Child Window
	CloseSmartPanelChild();

	//-- Check None Button Property
	ControlTypeInfo* controlTypeInfo = GetCtrlInfo(nCtrlID - ID_BTN_SMARTPANEL);

	if(!controlTypeInfo)
		return;

	int nType = controlTypeInfo->nType;

	switch(nType)
	{
	case SMARTPANEL_CONTROL_BAROMETER:
		CreateBarometerChildDlg(nCtrlID, controlTypeInfo, bShow);
		break;
	case SMARTPANEL_CONTROL_SLIDER:
		CreateSliderChildDlg(nCtrlID, controlTypeInfo, bShow);
		m_pSmartSDChildDlg->SetMode(m_nMode);
		break;
	case SMARTPANEL_CONTROL_MODESELECT:
//		CreateModeSelectChildDlg(nCtrlID, controlTypeInfo, bShow);
		((CNXRemoteStudioDlg*)GetParent())->m_bCheckChilde = TRUE;
		CreateBtnChildDlg(nCtrlID, controlTypeInfo, bShow);
		break;
	case SMARTPANEL_CONTROL_MAGICMODE:
		return;
	case SMARTPANEL_CONTROL_BUTTON:
		((CNXRemoteStudioDlg*)GetParent())->m_bCheckChilde = FALSE;
		CreateBtnChildDlg(nCtrlID, controlTypeInfo, bShow);
		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		CreateBtnChildDlg
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	Create Button Type 2Depth Dialog
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::CreateBtnChildDlg(int nCtrlID, ControlTypeInfo* controlTypeInfo, BOOL bShow)
{
	m_pSmartPanelChild = new CSmartPanelChildDlg();
	m_pSmartPanelChild->Create(CSmartPanelChildDlg::IDD, this);
	m_pSmartPanelChild->SetMagneticWindow(FALSE);
	m_pSmartPanelChild->SetSelectedProperty(nCtrlID);

	CRSButton* pRSButton = (CRSButton*)controlTypeInfo->pRSCtrl;

	//-- Set Parent Info
	CRect rect;
	pRSButton->GetWindowRect(&rect);
	m_pSmartPanelChild->SetParentRect(rect);

	//-- Create Dynamic Dialog
	m_pSmartPanelChild->CreateControlByType(nCtrlID - ID_BTN_SMARTPANEL, m_rsLayoutInfo);
	
	if(bShow)
	{
		//-- 2011-8-18 Lee JungTaek
		//-- Move Mouse Center
		CRect rectChild;
		m_pSmartPanelChild->GetWindowRect(&rectChild);
		//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
//		SetCursorPos(rectChild.left + rectChild.Width() / 2, rectChild.top + rectChild.Height() / 2);

		m_pSmartPanelChild->ShowWindow(SW_SHOW);		
	}
}

//------------------------------------------------------------------------------ 
//! @brief		CreateBarometerChildDlg
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::CreateBarometerChildDlg(int nCtrlID, ControlTypeInfo* controlTypeInfo, BOOL bShow)
{
	
	m_pSmartBMChildDlg = new CSmartPanelBarometerChildDlg();
	m_pSmartBMChildDlg->Create(CSmartPanelBarometerChildDlg::IDD, this);
	m_pSmartBMChildDlg->SetMagneticWindow(FALSE);

	if((nCtrlID - ID_BTN_SMARTPANEL) % 2)
		m_pSmartBMChildDlg->SetPosType(TRUE);
	else
		m_pSmartBMChildDlg->SetPosType(FALSE);

	CRSBarometerCtrl* pRSBarometer = (CRSBarometerCtrl*)controlTypeInfo->pRSCtrl;

	if(pRSBarometer)
	{
		//-- Set Property
		m_pSmartBMChildDlg->SetCtrolInfo(pRSBarometer);

		if(bShow)
		{
			CRect rect;
			GetWindowRect(&rect);
			//-- Set Parent Info
			CRect rectCtrl;
			pRSBarometer->GetWindowRect(&rectCtrl);
			CheckWindowRect(rect.left - 6, rectCtrl.top + 30 - 52, 317, 52, (CWnd*)m_pSmartBMChildDlg);

			//-- 2011-8-18 Lee JungTaek
			//-- Move Mouse Center
			CRect rectChild;
			m_pSmartBMChildDlg->GetWindowRect(&rectChild);
			//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
//			SetCursorPos(rectChild.left + rectChild.Width() / 2, rectChild.top + rectChild.Height() / 2);
		
			//-- Show Dialog
			m_pSmartBMChildDlg->ShowWindow(SW_SHOW);	
			m_pSmartBMChildDlg->UpdateWindow();
		}
			
	}		
}

//------------------------------------------------------------------------------ 
//! @brief		CreateSliderChildDlg
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::CreateSliderChildDlg(int nCtrlID, ControlTypeInfo* controlTypeInfo, BOOL bShow)
{
	
	m_pSmartSDChildDlg = new CSmartPanelSliderChildDlg();
	m_pSmartSDChildDlg->Create(CSmartPanelSliderChildDlg::IDD, this);
	m_pSmartSDChildDlg->SetMagneticWindow(FALSE);

	CRSSliderCtrl* pRSSlider = (CRSSliderCtrl*)controlTypeInfo->pRSCtrl;

	if(pRSSlider)
	{
		//-- Set Property
		m_pSmartSDChildDlg->SetCtrolInfo(pRSSlider);

		if(bShow)
		{
			CRect rect;
			GetWindowRect(&rect);
			//-- Set Parent Info
			CRect rectCtrl;
			pRSSlider->GetWindowRect(&rectCtrl);
			CheckWindowRect(rect.left - 6, rectCtrl.top + 30 - 52, 317, 52, (CWnd*)m_pSmartSDChildDlg);

			//-- 2011-8-18 Lee JungTaek
			//-- Move Mouse Center
			CRect rectChild;
			m_pSmartSDChildDlg->GetWindowRect(&rectChild);
			//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
//			SetCursorPos(rectChild.left + rectChild.Width() / 2, rectChild.top + rectChild.Height() / 2);

			//-- Show Dialog
			m_pSmartSDChildDlg->ShowWindow(SW_SHOW);	
			m_pSmartSDChildDlg->UpdateWindow();
		}			
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-9-22
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::CreateModeSelectChildDlg(int nCtrlID, ControlTypeInfo* controlTypeInfo, BOOL bShow)
{
	m_pSmartMSChildDlg = new CSmartPanelModeSelectChildDlg();
	m_pSmartMSChildDlg->Create(CSmartPanelModeSelectChildDlg::IDD, this);
	m_pSmartMSChildDlg->SetMagneticWindow(FALSE);

	CRSModeSelectCtrl* pRModeSelect = (CRSModeSelectCtrl*)controlTypeInfo->pRSCtrl;

	if(pRModeSelect)
	{
		//-- Set Property
		m_pSmartMSChildDlg->SetCtrolInfo(pRModeSelect);

		if(bShow)
		{
			CRect rect;
			GetWindowRect(&rect);
			//-- Set Parent Info
			CRect rectCtrl;
			pRModeSelect->GetWindowRect(&rectCtrl);
			CheckWindowRect(rect.left - 31,rectCtrl.top + 30 - 54, 368, 54, (CWnd*)m_pSmartMSChildDlg);

			//-- 2011-8-18 Lee JungTaek
			//-- Move Mouse Center
			CRect rectChild;
			m_pSmartMSChildDlg->GetWindowRect(&rectChild);
			//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
			SetCursorPos(rectChild.left + rectChild.Width() / 2, rectChild.top + rectChild.Height() / 2);

			//-- Show Dialog
			m_pSmartMSChildDlg->ShowWindow(SW_SHOW);	
			m_pSmartMSChildDlg->UpdateWindow();
		}			
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		ChangePrePropertyImage
//! @date		2011-05-23
//! @author		Lee JungTaek
//! @note	 	Change Selected Property Image
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::ChangePrePropertyImage(int nCtrlID)
{
	//-- Change Pre Property Image
	CString strImagePath = _T("");

 	if(m_nPrePropIndex != -1)
 	{
		if(m_nPrePropIndex == nCtrlID - ID_BTN_SMARTPANEL)
			return;

		//-- Chang Control
		ControlTypeInfo* pControlTypeInfo = GetCtrlInfo(m_nPrePropIndex);

		if(!pControlTypeInfo)
			return;

		m_rsLayoutInfo.ChangePrePropImage(pControlTypeInfo, m_nPrePropIndex);
	}
	else
	{
		switch(m_nMode)
		{
		case DSC_MODE_MAGIC_MAGIC_FRAME		:			
		case DSC_MODE_MAGIC_SMART_FILTER	:			
		case DSC_MODE_SCENE_BEAUTY			:
		case DSC_MODE_SCENE_NIGHT			:
		case DSC_MODE_SCENE_LANDSCAPE		:
		case DSC_MODE_SCENE_PORTRAIT		:
		case DSC_MODE_SCENE_CHILDREN		:
		case DSC_MODE_SCENE_SPORTS			:
		case DSC_MODE_SCENE_CLOSE_UP		:
		case DSC_MODE_SCENE_TEXT			:
		case DSC_MODE_SCENE_SUNSET			:
		case DSC_MODE_SCENE_DAWN			:
		case DSC_MODE_SCENE_BACKLIGHT		:
		case DSC_MODE_SCENE_FIREWORK		:
		case DSC_MODE_SCENE_BEACH_SNOW		:
		case DSC_MODE_SCENE_SOUND_PICTURE	:
		case DSC_MODE_SCENE_3D				:
		// dh9.seo 2013-06-25
		case DSC_MODE_SCENE_BEST			:
		case DSC_MODE_SCENE_MACRO			:
		case DSC_MODE_SCENE_ACTION			:
		case DSC_MODE_SCENE_RICH			:
		case DSC_MODE_SCENE_WATERFALL		:
		case DSC_MODE_SCENE_SILHOUETTE		:
		case DSC_MODE_SCENE_LIGHT			:
		case DSC_MODE_SCENE_CREATIVE		:
		case DSC_MODE_SCENE_PANORAMA		:
		//NX1 dh0.seo 2014-08-19
		case DSC_MODE_SCENE_ACTION_FREEZE	:
		case DSC_MODE_SCENE_LIGHT_TRACE		:
		case DSC_MODE_SCENE_MULTI_EXPOSURE	:
		case DSC_MODE_SCENE_AUTO_SHUTTER	:
			{
				ControlTypeInfo* pControlTypeInfo = GetCtrlInfo(0);
				if(!pControlTypeInfo)
					return;

				m_rsLayoutInfo.ChangePrePropImage(pControlTypeInfo, m_nPrePropIndex);
			}
			break;
		default:									break;
		}
	}
	
	//-- 2011-8-4 Lee JungTaek
	ControlTypeInfo* pControlTypeInfo = GetCtrlInfo(nCtrlID - ID_BTN_SMARTPANEL);

	if(!pControlTypeInfo)
		return;

	int nType = pControlTypeInfo->nType;

	switch(nType)
	{
	case SMARTPANEL_CONTROL_BUTTON:
		{
			CRSButton *pRSButton = (CRSButton*)pControlTypeInfo->pRSCtrl;
			if(pRSButton)	pRSButton->SetBtnStatus(BTN_BK_1ST_STATUS_FOCUS);
		}
		break;
	case SMARTPANEL_CONTROL_BAROMETER:
		{
			CRSBarometerCtrl *pRSBarometer = (CRSBarometerCtrl*)pControlTypeInfo->pRSCtrl;			
			if(pRSBarometer)		pRSBarometer->SetBarometerSelected(TRUE);
		}				
		break;
	case SMARTPANEL_CONTROL_SLIDER:
		{
			CRSSliderCtrl *pRSSlider = (CRSSliderCtrl*)pControlTypeInfo->pRSCtrl;
			if(pRSSlider)			pRSSlider->SetSliderSelected(TRUE);
		}
		break;		
	case SMARTPANEL_CONTROL_MODESELECT:
		{
			CRSModeSelectCtrl *pModeSelect = (CRSModeSelectCtrl*)pControlTypeInfo->pRSCtrl;
			if(pModeSelect)			pModeSelect->SetModeSelectSelected(TRUE);
		}
		break;	
	case SMARTPANEL_CONTROL_MAGICMODE:
		{
			CRSMagicFilterSelectCtrl *pMagicMode = (CRSMagicFilterSelectCtrl*)pControlTypeInfo->pRSCtrl;
			if(pMagicMode)			pMagicMode->SetMagicFilterSelected(TRUE);
		}
		break;
	}	

	m_nPrePropIndex = nCtrlID - ID_BTN_SMARTPANEL;
	m_nSelectPTPCode = ChangePanelIndexToPTPCode(m_nPrePropIndex);
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropertyValue
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Set Property Value
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::SetPropertyValue(int nPropIndex, int nPropValue)
{
	int nPTPCode = 0;
	nPTPCode = ChangePanelIndexToPTPCode(nPropIndex);
	
 	//-- Set Property Balue
 	((CNXRemoteStudioDlg*)GetParent())->SetPropertyValue(m_nMainCamera, nPTPCode, nPropValue);

	m_nSelectPTPCode = nPTPCode;

	m_nChangePTPCode = nPTPCode;

	//-- 2014-09-30 joonho.kim
	//-- Select PhotoSize
	if(nPTPCode == PTP_CODE_IMAGESIZE)
	{
		RSEvent* pMsgToMain = new RSEvent();
		pMsgToMain->nParam1 = nPropValue;			
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_SET_PHOTOSIZE, (LPARAM)pMsgToMain);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		GetCurrentStatus
//! @date		2011-6-15
//! @author		Lee JungTaek
//! @note	 	Get Status 
//------------------------------------------------------------------------------ 
int CSmartPanelDlg::GetPropValueStatus(int nDepth, int nPropIndex, int nPropValue)
{
	BOOL nStatus = PROPERTY_STATUS_DISABLE;

	//-- 2011-6-23 Lee JungTaek
	//-- Check Enable By PropType
	switch(m_nMode)
	{
	case DSC_MODE_P:
		{
			switch(nPropIndex)
			{
			case DSC_PASM_SHUTTERSPEED	:	return FALSE;
			case DSC_PASM_APERTURE		:	return FALSE;
			case DSC_PASM_EV			:	return TRUE;
			}
		}
		break;
	case DSC_MODE_A:
		{
			switch(nPropIndex)
			{
			case DSC_PASM_SHUTTERSPEED	:	return FALSE;
			case DSC_PASM_APERTURE		:	return TRUE;
			case DSC_PASM_EV			:	return TRUE;
			}
		}
		break;
	case DSC_MODE_S:
		{
			switch(nPropIndex)
			{
			case DSC_PASM_SHUTTERSPEED	:	return TRUE;
			case DSC_PASM_APERTURE		:	return FALSE;
			case DSC_PASM_EV			:	return TRUE;
			}
		}
		break;
	case DSC_MODE_M:
	case DSC_MODE_BULB: //dh0.seo 2015-01-12 위치 이동
		{
			if(!m_strModel.Compare(_T("NX1")))
			{
				switch(nPropIndex)
				{
					case DSC_PASM_SHUTTERSPEED	:	return TRUE;
					case DSC_PASM_APERTURE		:	return TRUE;
					case DSC_PASM_EV			:	return TRUE; //NX1 EV 사용
				}
			}
			else
			{
				switch(nPropIndex)
				{
					case DSC_PASM_SHUTTERSPEED	:	return TRUE;
					case DSC_PASM_APERTURE		:	return TRUE;
					case DSC_PASM_EV			:	return FALSE; //NX1 EV 사용
				}
			}
		}
		break;
	case DSC_MODE_SMART:
		{
			switch(nPropIndex)
			{
			case DSC_PASM_SHUTTERSPEED	:	return FALSE;
			case DSC_PASM_APERTURE		:	return FALSE;
			}
		}
		break;
	case DSC_MODE_I						:			
	case DSC_MODE_CAPTURE_MOVIE			:		
	case DSC_MODE_MAGIC_MAGIC_FRAME		:		
	case DSC_MODE_MAGIC_SMART_FILTER	:		break;
	case DSC_MODE_PANORAMA				:		
	case DSC_MODE_SCENE_BEAUTY			:	
	case DSC_MODE_SCENE_NIGHT			:	
	case DSC_MODE_SCENE_LANDSCAPE		:	
	case DSC_MODE_SCENE_PORTRAIT		:	
	case DSC_MODE_SCENE_CHILDREN		:	
	case DSC_MODE_SCENE_SPORTS			:	
	case DSC_MODE_SCENE_CLOSE_UP		:	
	case DSC_MODE_SCENE_TEXT			:	
	case DSC_MODE_SCENE_SUNSET			:	
	case DSC_MODE_SCENE_DAWN			:	
	case DSC_MODE_SCENE_BACKLIGHT		:	
	case DSC_MODE_SCENE_FIREWORK		:	
	case DSC_MODE_SCENE_BEACH_SNOW		:	
	case DSC_MODE_SCENE_SOUND_PICTURE	:	
	case DSC_MODE_SCENE_3D				:
	// dh9.seo 2013-06-24
	case DSC_MODE_SCENE_BEST				:
	case DSC_MODE_SCENE_MACRO				:
	case DSC_MODE_SCENE_ACTION				:
	case DSC_MODE_SCENE_RICH				:
	case DSC_MODE_SCENE_WATERFALL			:
	case DSC_MODE_SCENE_SILHOUETTE			:
	case DSC_MODE_SCENE_LIGHT				:
	case DSC_MODE_SCENE_CREATIVE			:
	case DSC_MODE_SCENE_PANORAMA			:
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
	case DSC_MODE_SCENE_LIGHT_TRACE		:
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_BEAUTY_SCENECHANGE	:	return TRUE;
			case DSC_SCENE_BEAUTY_SHUTTERSPEED	:	return FALSE;
			case DSC_SCENE_BEAUTY_APERTURE		:	return FALSE;
			}
		}
		break;
	case DSC_MODE_MOVIE					:		break;
	}	

	int nPTPCode = 0;
	nPTPCode = ChangePanelIndexToPTPCode(nPropIndex);

	//-- Set Property Value
	nStatus = ((CNXRemoteStudioDlg*)GetParent())->GetPropValueStatus(nDepth, m_nMainCamera, nPTPCode, nPropValue);	
	return nStatus;
}
//------------------------------------------------------------------------------ 
//! @brief		ChangePanelIndexToPTPCode
//! @date		2011-6-15
//! @author		Lee JungTaek
//! @note	 	Change Panel Index to PTPCode
//------------------------------------------------------------------------------ 
int CSmartPanelDlg::ChangePanelIndexToPTPCode(int nPropIndex)
{
	int nPTPCode = 0;

	switch(m_nMode)
	{
	case DSC_MODE_P:
	case DSC_MODE_A:
	case DSC_MODE_S:
	case DSC_MODE_M:
	case DSC_MODE_BULB: //dh0.seo 2015-01-12 위치 이동
		{
			switch(nPropIndex)
			{
			case DSC_PASM_SHUTTERSPEED		:	nPTPCode = PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_PASM_APERTURE			:	nPTPCode = PTP_CODE_F_NUMBER				;	break;
			case DSC_PASM_OIS				:	nPTPCode = PTP_CODE_SAMSUNG_OIS				;	break;
			case DSC_PASM_EV				:	nPTPCode = PTP_CODE_SAMSUNG_EV				;	break;
			case DSC_PASM_ISO				:	nPTPCode = PTP_CODE_EXPOSUREINDEX			;	break;
			case DSC_PASM_FLASH				:	nPTPCode = PTP_CODE_FLASHMODE				;	break;
			case DSC_PASM_METERING			:	nPTPCode = PTP_CODE_EXPOSUREMETERINGMODE	;	break;
			case DSC_PASM_WHITEBALANCE		:	nPTPCode = PTP_CODE_WHITEBALANCE			;	break;
			case DSC_PASM_PICTUREWIZARD		:	nPTPCode = PTP_CODE_SAMSUNG_PICTUREWIZARD	;	break;
			case DSC_PASM_COLORSPACE		:	nPTPCode = PTP_CODE_SAMSUNG_CSPACE			;	break;
			case DSC_PASM_SMARTRANGE		:	nPTPCode = PTP_CODE_SAMSUNG_SMARTRANGE		;	break;
			case DSC_PASM_AFMODE			:	nPTPCode = PTP_CODE_FOCUSMODE				;	break;
			case DSC_PASM_FOCUSAREA			:	nPTPCode = PTP_CODE_FOCUSMETERINGMODE		;	break;
			case DSC_PASM_DRIVE				:	nPTPCode = PTP_CODE_STILLCAPTUREMODE		;	break;
			case DSC_PASM_PHOTOSIZE			:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_PASM_QUALITY			:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			// dh9.seo 2013-06-21
			case DSC_PASM_SMARTFILTER		:	nPTPCode = PTP_CODE_SAMSUNG_SMARTFILTER		;	break;
			default							:	nPTPCode = nPropIndex						;	break;
			}
		}
		break;
	case DSC_MODE_SMART:
		{
			switch(nPropIndex)
			{
			case DSC_SMARTAUTO_SHUTTERSPEED	:	nPTPCode = PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SMARTAUTO_APERTURE		:	nPTPCode = PTP_CODE_F_NUMBER				;	break;			
			case DSC_SMARTAUTO_DRIVE		:	nPTPCode = PTP_CODE_STILLCAPTUREMODE		;	break;			
			case DSC_SMARTAUTO_COLORSPACE	:	nPTPCode = PTP_CODE_SAMSUNG_CSPACE			;	break;
			case DSC_SMARTAUTO_PHOTOSIZE	:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_SMARTAUTO_FLASH		:	nPTPCode = PTP_CODE_FLASHMODE				;	break;
			// dh9.seo 2013-06-22
			case DSC_SMARTAUTO_OIS			:	nPTPCode = PTP_CODE_SAMSUNG_OIS				;	break;
			default							:	nPTPCode = nPropIndex						;	break;
			}
		}
		break;
	case DSC_MODE_I						:		break;
	case DSC_MODE_CAPTURE_MOVIE			:		break;
	case DSC_MODE_MAGIC_MAGIC_FRAME		:	
		{
			switch(nPropIndex)
			{
			case DSC_MAGIC_FRAME_MAGICMODECHANGE	:	nPTPCode = PTP_CODE_SAMSUNG_MAGIC_MODE		;	break;
			case DSC_MAGIC_FRAME_MAGICTYPECHANGE	:	nPTPCode = PTP_CODE_SAMSUNG_MAGIC_FRAME		;	break;
			case DSC_MAGIC_FRAME_AFMODE				:	nPTPCode = PTP_CODE_FOCUSMODE				;	break;
			case DSC_MAGIC_FRAME_COLORSPACE			:	nPTPCode = PTP_CODE_SAMSUNG_CSPACE			;	break;
			case DSC_MAGIC_FRAME_PHOTOSIZE			:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_MAGIC_FRAME_QUALITY			:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			case DSC_MAGIC_FRAME_DRIVE				:	nPTPCode = PTP_CODE_STILLCAPTUREMODE		;	break;
			case DSC_MAGIC_FRAME_OIS				:	nPTPCode = PTP_CODE_SAMSUNG_OIS				;	break;
			default:									nPTPCode = nPropIndex;							break;
			}
		}
		break;
	case DSC_MODE_MAGIC_SMART_FILTER	:	
		{
			switch(nPropIndex)
			{
			case DSC_SMART_FILTER_MAGICMODECHANGE	:	nPTPCode = PTP_CODE_SAMSUNG_MAGIC_MODE		;	break;
			case DSC_SMART_FILTER_MAGICTYPECHANGE	:	nPTPCode = PTP_CODE_SAMSUNG_SMARTFILTER		;	break;
			case DSC_SMART_FILTER_AFMODE			:	nPTPCode = PTP_CODE_FOCUSMODE				;	break;
			case DSC_SMART_FILTER_COLORSPACE		:	nPTPCode = PTP_CODE_SAMSUNG_CSPACE			;	break;
			case DSC_SMART_FILTER_PHOTOSIZE			:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_SMART_FILTER_QUALITY			:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			case DSC_SMART_FILTER_DRIVE				:	nPTPCode = PTP_CODE_STILLCAPTUREMODE		;	break;
			case DSC_SMART_FILTER_OIS				:	nPTPCode = PTP_CODE_SAMSUNG_OIS				;	break;
			default:									nPTPCode = nPropIndex;							break;
			}
		}		
		break;
	case DSC_MODE_PANORAMA				:
	case DSC_MODE_SCENE_BEAUTY			:	
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_BEAUTY_SCENECHANGE	:	nPTPCode = 	PTP_CODE_SAMSUNG_SCENE_MODE		;	break;						
			case DSC_SCENE_BEAUTY_SHUTTERSPEED	:	nPTPCode = 	PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SCENE_BEAUTY_APERTURE		:	nPTPCode = 	PTP_CODE_F_NUMBER				;	break;
			case DSC_SCENE_BEAUTY_AFMODE		:	nPTPCode = 	PTP_CODE_FOCUSMODE				;	break;
			case DSC_SCENE_BEAUTY_FOCUSAREA		:	nPTPCode = 	PTP_CODE_FOCUSMETERINGMODE		;	break;
			case DSC_SCENE_BEAUTY_PHOTOSIZE		:	nPTPCode =	PTP_CODE_IMAGESIZE				;	break;
			case DSC_SCENE_BEAUTY_QUALITY		:	nPTPCode = 	PTP_CODE_COMPRESSIONSETTING		;	break;
			case DSC_SCENE_BEAUTY_DRIVE			:	nPTPCode = 	PTP_CODE_STILLCAPTUREMODE		;	break;
			case DSC_SCENE_BEAUTY_COLORSPACE	:	nPTPCode = 	PTP_CODE_SAMSUNG_CSPACE			;	break;
			case DSC_SCENE_BEAUTY_OIS			:	nPTPCode = 	PTP_CODE_SAMSUNG_OIS			;	break;
			case DSC_SCENE_BEAUTY_FLASH			:	nPTPCode = 	PTP_CODE_FLASHMODE				;	break;
			case DSC_SCENE_BEAUTY_FACETONE		:	nPTPCode = 	PTP_CODE_SAMSUNG_FACE_TONE		;	break;
			case DSC_SCENE_BEAUTY_FACERETOUCH	:	nPTPCode = 	PTP_CODE_SAMSUNG_FACE_RETOUCH	;	break;
			default:									nPTPCode = nPropIndex;							break;
			}																			
		}
		break;
	case DSC_MODE_SCENE_PORTRAIT		:	
	case DSC_MODE_SCENE_NIGHT			:	
	case DSC_MODE_SCENE_CHILDREN		:	
	case DSC_MODE_SCENE_SPORTS			:	
	case DSC_MODE_SCENE_BACKLIGHT		:	
	case DSC_MODE_SCENE_BEACH_SNOW		:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_PORTRAIT_SCENECHANGE		:	nPTPCode =	PTP_CODE_SAMSUNG_SCENE_MODE			;	break;
			case DSC_SCENE_PORTRAIT_SHUTTERSPEED	:	nPTPCode =	PTP_CODE_SAMSUNG_SHUTTERSPEED		;	break;
			case DSC_SCENE_PORTRAIT_APERTURE		:	nPTPCode =	PTP_CODE_F_NUMBER					;	break;
			case DSC_SCENE_PORTRAIT_AFMODE			:	nPTPCode =	PTP_CODE_FOCUSMODE					;	break;
			case DSC_SCENE_PORTRAIT_FOCUSAREA		:	nPTPCode =	PTP_CODE_FOCUSMETERINGMODE			;	break;
			case DSC_SCENE_PORTRAIT_PHOTOSIZE		:	nPTPCode =	PTP_CODE_IMAGESIZE					;	break;
			case DSC_SCENE_PORTRAIT_QUALITY			:	nPTPCode =	PTP_CODE_COMPRESSIONSETTING			;	break;
			case DSC_SCENE_PORTRAIT_DRIVE			:	nPTPCode =	PTP_CODE_STILLCAPTUREMODE			;	break;
			case DSC_SCENE_PORTRAIT_COLORSPACE		:	nPTPCode =	PTP_CODE_SAMSUNG_CSPACE				;	break;
			case DSC_SCENE_PORTRAIT_OIS				:	nPTPCode =	PTP_CODE_SAMSUNG_OIS				;	break;
			case DSC_SCENE_PORTRAIT_FLASH			:	nPTPCode =	PTP_CODE_FLASHMODE					;	break;
			default									:	nPTPCode = nPropIndex							;	break;
			}
		}
		break;	
	case DSC_MODE_SCENE_DAWN			:
	case DSC_MODE_SCENE_SUNSET			:	
	case DSC_MODE_SCENE_TEXT			:
	case DSC_MODE_SCENE_CLOSE_UP		:	
	case DSC_MODE_SCENE_LANDSCAPE		:	
	case DSC_MODE_SCENE_3D				:	
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_DAWN_SCENECHANGE		:	nPTPCode =	PTP_CODE_SAMSUNG_SCENE_MODE		;	break;
			case DSC_SCENE_DAWN_SHUTTERSPEED	:	nPTPCode =	PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SCENE_DAWN_APERTURE		:	nPTPCode =	PTP_CODE_F_NUMBER				;	break;
			case DSC_SCENE_DAWN_AFMODE			:	nPTPCode =	PTP_CODE_FOCUSMODE				;	break;
			case DSC_SCENE_DAWN_FOCUSAREA		:	nPTPCode =	PTP_CODE_FOCUSMETERINGMODE		;	break;
			case DSC_SCENE_DAWN_PHOTOSIZE		:	nPTPCode =	PTP_CODE_IMAGESIZE				;	break;
			case DSC_SCENE_DAWN_QUALITY			:	nPTPCode =	PTP_CODE_COMPRESSIONSETTING		;	break;
			case DSC_SCENE_DAWN_DRIVE			:	nPTPCode =	PTP_CODE_STILLCAPTUREMODE		;	break;
			case DSC_SCENE_DAWN_OIS				:	nPTPCode =	PTP_CODE_SAMSUNG_OIS			;	break;
			default								:	nPTPCode = nPropIndex						;	break;								
			}
		}
		break;	
	case DSC_MODE_SCENE_FIREWORK		:	
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_FIREWORK_SCENECHANGE	:	nPTPCode = PTP_CODE_SAMSUNG_SCENE_MODE		;	break;
			case DSC_SCENE_FIREWORK_SHUTTERSPEED:	nPTPCode = PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SCENE_FIREWORK_APERTURE	:	nPTPCode = PTP_CODE_F_NUMBER				;	break;
			case DSC_SCENE_FIREWORK_AFMODE		:	nPTPCode = PTP_CODE_FOCUSMODE				;	break;
			case DSC_SCENE_FIREWORK_COLORSPACE	:	nPTPCode = PTP_CODE_SAMSUNG_CSPACE			;	break;
			case DSC_SCENE_FIREWORK_PHOTOSIZE	:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_SCENE_FIREWORK_QUALITY		:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			case DSC_SCENE_FIREWORK_DRIVE		:	nPTPCode = PTP_CODE_STILLCAPTUREMODE		;	break;
			case DSC_SCENE_FIREWORK_OIS			:	nPTPCode = PTP_CODE_SAMSUNG_OIS				;	break;
			default								:	nPTPCode = nPropIndex						;	break;
			}
		}
		break;
	case DSC_MODE_SCENE_SOUND_PICTURE	:	
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_SOUNDPICTURE_SCENECHANGE	:	nPTPCode = PTP_CODE_SAMSUNG_SCENE_MODE		;	break;
			}
		}
		break;
	case DSC_MODE_MOVIE					:	
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_BEST_SCENECHANGE		:	nPTPCode = PTP_CODE_SAMSUNG_SCENE_MODE		;	break;
			case DSC_SCENE_BEST_SHUTTERSPEED	:	nPTPCode = PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SCENE_BEST_APERTURE		:	nPTPCode = PTP_CODE_F_NUMBER				;	break;
			case DSC_SCENE_BEST_QUALITY			:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			default:									nPTPCode = nPropIndex;							break;
			}
		}
		break;
	case DSC_MODE_SCENE_BEST				:	
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_BEST_SCENECHANGE		:	nPTPCode = PTP_CODE_SAMSUNG_SCENE_MODE		;	break;
			case DSC_SCENE_BEST_SHUTTERSPEED	:	nPTPCode = PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SCENE_BEST_APERTURE		:	nPTPCode = PTP_CODE_F_NUMBER				;	break;
			case DSC_SCENE_BEST_PHOTOSIZE		:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_SCENE_BEST_QUALITY			:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			case DSC_SCENE_BEST_DRIVE			:	nPTPCode = PTP_CODE_STILLCAPTUREMODE		;	break;
			default:									nPTPCode = nPropIndex;							break;
			}
		}
		break;
	case DSC_MODE_SCENE_ACTION				:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_ACTION_SCENECHANGE	:	nPTPCode = PTP_CODE_SAMSUNG_SCENE_MODE		;	break;
			case DSC_SCENE_ACTION_SHUTTERSPEED	:	nPTPCode = PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SCENE_ACTION_APERTURE		:	nPTPCode = PTP_CODE_F_NUMBER				;	break;
			case DSC_SCENE_ACTION_AFMODE		:	nPTPCode = PTP_CODE_FOCUSMODE				;	break;
			case DSC_SCENE_ACTION_FOCUSAREA		:	nPTPCode = PTP_CODE_FOCUSMETERINGMODE		;	break;
			case DSC_SCENE_ACTION_PHOTOSIZE		:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_SCENE_ACTION_QUALITY		:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			case DSC_SCENE_ACTION_DRIVE			:	nPTPCode = PTP_CODE_STILLCAPTUREMODE		;	break;
			default:									nPTPCode = nPropIndex;							break;
			}
		}
		break;
	case DSC_MODE_SCENE_MACRO				:
	case DSC_MODE_SCENE_RICH				:
	case DSC_MODE_SCENE_WATERFALL			:
	case DSC_MODE_SCENE_SILHOUETTE			:
	case DSC_MODE_SCENE_LIGHT				:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_MACRO_SCENECHANGE	:	nPTPCode = PTP_CODE_SAMSUNG_SCENE_MODE		;	break;
			case DSC_SCENE_MACRO_SHUTTERSPEED	:	nPTPCode = PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SCENE_MACRO_APERTURE		:	nPTPCode = PTP_CODE_F_NUMBER				;	break;
			case DSC_SCENE_MACRO_FOCUSAREA		:	nPTPCode = PTP_CODE_FOCUSMETERINGMODE		;	break;
			case DSC_SCENE_MACRO_PHOTOSIZE		:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_SCENE_MACRO_QUALITY		:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			case DSC_SCENE_MACRO_DRIVE			:	nPTPCode = PTP_CODE_STILLCAPTUREMODE		;	break;
			default:									nPTPCode = nPropIndex;							break;
			}
		}
		break;
	case DSC_MODE_SCENE_CREATIVE			:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_CREATIVE_SCENECHANGE	:	nPTPCode = PTP_CODE_SAMSUNG_SCENE_MODE		;	break;
			case DSC_SCENE_CREATIVE_SHUTTERSPEED:	nPTPCode = PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SCENE_CREATIVE_APERTURE	:	nPTPCode = PTP_CODE_F_NUMBER				;	break;
			case DSC_SCENE_CREATIVE_PHOTOSIZE	:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_SCENE_CREATIVE_QUALITY		:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			case DSC_SCENE_CREATIVE_DRIVE		:	nPTPCode = PTP_CODE_STILLCAPTUREMODE		;	break;
			case DSC_SCENE_CREATIVE_FLASH		:	nPTPCode = PTP_CODE_FLASHMODE				;	break;
			default:									nPTPCode = nPropIndex;							break;
			}
		}
		break;
	case DSC_MODE_SCENE_PANORAMA			:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_FIREWORK_SCENECHANGE	:	nPTPCode = PTP_CODE_SAMSUNG_SCENE_MODE		;	break;
			case DSC_SCENE_FIREWORK_SHUTTERSPEED:	nPTPCode = PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SCENE_FIREWORK_APERTURE	:	nPTPCode = PTP_CODE_F_NUMBER				;	break;
			case DSC_SCENE_BEAUTY_AFMODE		:	nPTPCode = PTP_CODE_FOCUSMODE				;	break;
			case DSC_SCENE_BEAUTY_FOCUSAREA		:	nPTPCode = PTP_CODE_FOCUSMETERINGMODE		;	break;
			case DSC_SCENE_BEAUTY_PHOTOSIZE		:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_SCENE_BEAUTY_QUALITY		:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			case DSC_SCENE_BEAUTY_DRIVE			:	nPTPCode = PTP_CODE_STILLCAPTUREMODE		;	break;
			case DSC_SCENE_PORTRAIT_FLASH		:	nPTPCode = PTP_CODE_FLASHMODE				;	break;
			default:									nPTPCode = nPropIndex;							break;
			}
		}
		break;
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_ACTION_FREEZE_SCENECHANGE	:	nPTPCode = PTP_CODE_SAMSUNG_SCENE_MODE		;	break;
			case DSC_SCENE_ACTION_FREEZE_SHUTTERSPEED	:	nPTPCode = PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SCENE_ACTION_FREEZE_APERTURE		:	nPTPCode = PTP_CODE_F_NUMBER				;	break;
			case DSC_SCENE_ACTION_FREEZE_AFMODE			:	nPTPCode = PTP_CODE_FOCUSMODE				;	break;
			case DSC_SCENE_ACTION_FREEZE_FOCUSAREA		:	nPTPCode = PTP_CODE_FOCUSMETERINGMODE		;	break;
			case DSC_SCENE_ACTION_FREEZE_PHOTOSIZE		:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_SCENE_ACTION_FREEZE_QUALITY		:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			case DSC_SCENE_ACTION_FREEZE_DRIVE			:	nPTPCode = PTP_CODE_STILLCAPTUREMODE		;	break;
			default:									nPTPCode = nPropIndex;							break;
			}
		}
		break;
	case DSC_MODE_SCENE_LIGHT_TRACE		:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_LIGHT_TRACE_SCENECHANGE	:	nPTPCode = PTP_CODE_SAMSUNG_SCENE_MODE		;	break;
			case DSC_SCENE_LIGHT_TRACE_SHUTTERSPEED	:	nPTPCode = PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SCENE_LIGHT_TRACE_APERTURE		:	nPTPCode = PTP_CODE_F_NUMBER				;	break;
			case DSC_SCENE_LIGHT_TRACE_FOCUSAREA		:	nPTPCode = PTP_CODE_FOCUSMETERINGMODE		;	break;
			case DSC_SCENE_LIGHT_TRACE_PHOTOSIZE		:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_SCENE_LIGHT_TRACE_QUALITY		:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			case DSC_SCENE_LIGHT_TRACE_DRIVE		:	nPTPCode = PTP_CODE_STILLCAPTUREMODE		;	break;
			default:									nPTPCode = nPropIndex;							break;
			}
		}
		break;
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_MULTI_EXPOSURE_SCENECHANGE	:	nPTPCode = PTP_CODE_SAMSUNG_SCENE_MODE		;	break;
			case DSC_SCENE_MULTI_EXPOSURE_SHUTTERSPEED	:	nPTPCode = PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SCENE_MULTI_EXPOSURE_APERTURE		:	nPTPCode = PTP_CODE_F_NUMBER				;	break;
			case DSC_SCENE_MULTI_EXPOSURE_FOCUSAREA		:	nPTPCode = PTP_CODE_FOCUSMETERINGMODE		;	break;
			case DSC_SCENE_MULTI_EXPOSURE_PHOTOSIZE		:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_SCENE_MULTI_EXPOSURE_QUALITY		:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			default:									nPTPCode = nPropIndex;							break;
			}
		}
		break;
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_AUTO_SHUTTER_SCENECHANGE		:	nPTPCode = PTP_CODE_SAMSUNG_SCENE_MODE		;	break;
			case DSC_SCENE_AUTO_SHUTTER_SHUTTERSPEED	:	nPTPCode = PTP_CODE_SAMSUNG_SHUTTERSPEED	;	break;
			case DSC_SCENE_AUTO_SHUTTER_APERTURE		:	nPTPCode = PTP_CODE_F_NUMBER				;	break;
			case DSC_SCENE_AUTO_SHUTTER_PHOTOSIZE		:	nPTPCode = PTP_CODE_IMAGESIZE				;	break;
			case DSC_SCENE_AUTO_SHUTTER_QUALITY			:	nPTPCode = PTP_CODE_COMPRESSIONSETTING		;	break;
			default:									nPTPCode = nPropIndex;							break;
			}
		}
		break;
	}	
	return nPTPCode;
}

//CMiLRe 20141118 스마트 패널 선택 -> 좌클릭, 마우스 무브시 테두리표시
int CSmartPanelDlg::GetSmartPanelCtrCnt (int nMode)
{
	int nSmartPanelCtrCnt = 0;
	if(m_strModel == _T("NX200") || m_strModel == _T("NX1000") || m_strModel == _T("NX2000") || m_strModel == _T("NX1")) // || m_strModel == _T("NX1") //-- 2014-7-28 dh0.seo
	{
		switch(m_nMode)
		{
		case DSC_MODE_P:
		case DSC_MODE_A:
		case DSC_MODE_S:
		case DSC_MODE_M:
		case DSC_MODE_BULB					:	nSmartPanelCtrCnt = DSC_PASM_COUNT;				break; //dh0.seo 2015-01-12 위치 이동
		case DSC_MODE_SMART					:	nSmartPanelCtrCnt = DSC_SMARTAUTO_COUNT;		break;	
		case DSC_MODE_I						:	break;
		case DSC_MODE_CAPTURE_MOVIE			:	break;
		case DSC_MODE_MAGIC_MAGIC_FRAME		:	nSmartPanelCtrCnt = DSC_MAGIC_FRAME_COUNT;		break;
		case DSC_MODE_MAGIC_SMART_FILTER	:	nSmartPanelCtrCnt = DSC_SMART_FILTER_COUNT;		break;
		case DSC_MODE_PANORAMA				:	nSmartPanelCtrCnt = DSC_SCENE_PANORAMA_COUNT;	break;
		case DSC_MODE_SCENE_BEAUTY			:	nSmartPanelCtrCnt = DSC_SCENE_BEAUTY_COUNT;		break;
		case DSC_MODE_SCENE_PORTRAIT		:	
		case DSC_MODE_SCENE_NIGHT			:	
		case DSC_MODE_SCENE_CHILDREN		:	
		case DSC_MODE_SCENE_SPORTS			:	
		case DSC_MODE_SCENE_BACKLIGHT		:
		case DSC_MODE_SCENE_BEACH_SNOW		:	nSmartPanelCtrCnt = DSC_SCENE_PORTRAIT_COUNT;	break;				
		case DSC_MODE_SCENE_DAWN			:
		case DSC_MODE_SCENE_SUNSET			:	
		case DSC_MODE_SCENE_TEXT			:
		case DSC_MODE_SCENE_CLOSE_UP		:	
		case DSC_MODE_SCENE_LANDSCAPE		:	nSmartPanelCtrCnt = DSC_SCENE_BACKLIGHT_COUNT;	break;	
		case DSC_MODE_SCENE_FIREWORK		:	nSmartPanelCtrCnt = DSC_SCENE_FIREWORK_COUNT;	break;	
		case DSC_MODE_SCENE_SOUND_PICTURE	:	nSmartPanelCtrCnt = DSC_SCENE_SOUNDPICTURE_COUNT;break;
		case DSC_MODE_SCENE_3D				:	nSmartPanelCtrCnt = DSC_SCENE_3D_COUNT;			break;
		case DSC_MODE_MOVIE					:	break;
			// dh9.seo 2013-06-24
		case DSC_MODE_SCENE_BEST			:	nSmartPanelCtrCnt = DSC_SCENE_BEST_COUNT;		break;
		case DSC_MODE_SCENE_ACTION			:	nSmartPanelCtrCnt = DSC_SCENE_ACTION_COUNT;		break;
		case DSC_MODE_SCENE_MACRO			:
		case DSC_MODE_SCENE_RICH			:
		case DSC_MODE_SCENE_WATERFALL		:
		case DSC_MODE_SCENE_SILHOUETTE		:
		case DSC_MODE_SCENE_LIGHT			:	nSmartPanelCtrCnt = DSC_SCENE_MACRO_COUNT;		break;
		case DSC_MODE_SCENE_CREATIVE		:	nSmartPanelCtrCnt = DSC_SCENE_CREATIVE_COUNT;	break;
		case DSC_MODE_SCENE_PANORAMA		:	nSmartPanelCtrCnt = DSC_SCENE_PANORAMA_COUNT;	break;
			//dh0.seo 2014-07-29
		case DSC_MODE_SCENE_ACTION_FREEZE	:	nSmartPanelCtrCnt = DSC_SCENE_ACTION_FREEZE_COUNT;	break;
		case DSC_MODE_SCENE_LIGHT_TRACE		:	nSmartPanelCtrCnt = DSC_SCENE_LIGHT_TRACE_COUNT;	break;
		case DSC_MODE_SCENE_MULTI_EXPOSURE	:	nSmartPanelCtrCnt = DSC_SCENE_MULTI_EXPOSURE_COUNT;	break;
		case DSC_MODE_SCENE_AUTO_SHUTTER	:	nSmartPanelCtrCnt = DSC_SCENE_AUTO_SHUTTER_COUNT;	break;
		}	 
	}
	return nSmartPanelCtrCnt;
}
//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	WM Message
//------------------------------------------------------------------------------ 
//CMiLRe 20141118 스마트 패널 선택 -> 좌클릭, 마우스 무브시 테두리표시
BOOL CSmartPanelDlg::PreTranslateMessage(MSG* pMsg) 
{
	CString strCtrlID = _T("");
	int nCtrlID = 0;

	//TRACE(_T("pMsg->message = %x\n"), pMsg->message);

	//CMiLRe 20141118 카메라 선택 시 프로퍼티 로드 중 창 컨트롤 막음
	if(!m_bEnable)
	{
		return FALSE;
	}

	if(pMsg->message == 0xa1)
	{
		if(m_pSmartBMChildDlg)
			m_pSmartBMChildDlg->ShowWindow(SW_HIDE);
		if(m_pSmartMSChildDlg)
			m_pSmartMSChildDlg->ShowWindow(SW_HIDE);
		if(m_pSmartSDChildDlg)
			m_pSmartSDChildDlg->ShowWindow(SW_HIDE);
		if(m_pSmartPanelChild)
			m_pSmartPanelChild->ShowWindow(SW_HIDE);

		return TRUE;
	}

	//-- Create 2Depth SmartPanel
	if(pMsg->message == WM_RBUTTONDOWN)
	{
		if(((CNXRemoteStudioDlg*)GetParent())->m_bInterval == TRUE)
			return TRUE;

		//-- 2011-8-18 Lee JungTaek
		((CNXRemoteStudioDlg*)GetParent())->HidePopup();

		//-- 2011-8-5 Lee JungTaek
		if(IsCapturing())
			return TRUE;

		//-- To use Mouse wheel
		nCtrlID = ::GetDlgCtrlID(pMsg->hwnd);
		strCtrlID.Format(_T("%d\n"), nCtrlID);
		
		//-- Check Control ID
		if(nCtrlID == IDC_BTN_SP_MULTIAPPLY) 
			return FALSE;

		//-- Count Control Count
		int nSmartPanelCtrCnt = GetSmartPanelCtrCnt(m_nMode);	

		//-- Check Unwanted Control ID
		if(nCtrlID < ID_BTN_SMARTPANEL || nCtrlID > ID_BTN_SMARTPANEL + nSmartPanelCtrCnt)
			return FALSE;

		//-- Create New Pannel
		CreateSmartPanelChild(nCtrlID);

		//-- Change Pre Selected Property Image
		ChangePrePropertyImage(nCtrlID);	

		return TRUE;
	}
	else if(pMsg->message == WM_LBUTTONDOWN)
	{
		if(((CNXRemoteStudioDlg*)GetParent())->m_bInterval == TRUE)
			return TRUE;

		//-- 2011-8-18 Lee JungTaek
		((CNXRemoteStudioDlg*)GetParent())->HidePopup();

		//-- 2011-8-5 Lee JungTaek
		if(IsCapturing())
			return TRUE;

		//-- To use Mouse wheel
		nCtrlID = ::GetDlgCtrlID(pMsg->hwnd);
		strCtrlID.Format(_T("%d\n"), nCtrlID);
		
		switch(nCtrlID)
		{
		case IDC_BTN_SP_LOADBANK:
			LoadBankBtnClick();
			return TRUE;
		case IDC_BTN_SP_SAVEBANK:
			SaveBankBtnClick();
			return TRUE;
		case IDC_BTN_SP_MULTIAPPLY:
			MutliapplyBtnClick();
			return TRUE;
		case IDC_BTN_SP_HELP:
			HelpBtnClick();
			return TRUE;
		}


		//-- Check Control ID
		//if(nCtrlID == IDC_BTN_SP_MULTIAPPLY) 
			//return FALSE;

		//-- Count Control Count
		int nSmartPanelCtrCnt = GetSmartPanelCtrCnt(m_nMode);		

		//-- Check Unwanted Control ID
		if(nCtrlID < ID_BTN_SMARTPANEL || nCtrlID > ID_BTN_SMARTPANEL + nSmartPanelCtrCnt)
			return FALSE;
		//-- Create New Pannel
		CreateSmartPanelChild(nCtrlID);

		//-- Change Pre Selected Property Image
		ChangePrePropertyImage(nCtrlID);	
		return TRUE;
	}
#if 0
	else if(pMsg->message == WM_MOUSEWHEEL)
	{
		//-- 2011-8-18 Lee JungTaek
		((CNXRemoteStudioDlg*)GetParent())->HidePopup();

		//-- 2011-8-5 Lee JungTaek
		if(IsCapturing())
			return TRUE;

		//-- Check Unwanted Control ID
		nCtrlID = ID_BTN_SMARTPANEL + m_nPrePropIndex;

		int nSmartPanelCtrCnt = GetSmartPanelCtrCnt(m_nMode);	
		
		if(nCtrlID < ID_BTN_SMARTPANEL || nCtrlID > ID_BTN_SMARTPANEL + nSmartPanelCtrCnt)
			return FALSE;

		//-- 2011-9-16 Lee JungTaek
		//-- Check Mouse Wheel Direction
		BOOL bMouseWheelUp;
		int delta = GET_WHEEL_DELTA_WPARAM(pMsg->wParam);

		if(delta < 0)
			bMouseWheelUp = FALSE;
		else
			bMouseWheelUp = TRUE;
		
		//-- Set Data
		ControlTypeInfo* controlTypeInfo = GetCtrlInfo(m_nPrePropIndex);

		if(controlTypeInfo)
		{
			switch(controlTypeInfo->nType)
			{
			case SMARTPANEL_CONTROL_BUTTON:
				break;
			case SMARTPANEL_CONTROL_BAROMETER:
				{
					CRSBarometerCtrl * pBarometerCtrl = (CRSBarometerCtrl*)controlTypeInfo->pRSCtrl;
					pBarometerCtrl->SetPos(bMouseWheelUp);
				}					
				break;
			case SMARTPANEL_CONTROL_SLIDER:
				{
					CRSSliderCtrl * pSliderCtrl = (CRSSliderCtrl*)controlTypeInfo->pRSCtrl;
					pSliderCtrl->SetPos(bMouseWheelUp);
				}					
				break;
			case SMARTPANEL_CONTROL_MODESELECT:
				{
					CRSModeSelectCtrl * pModeSelect = (CRSModeSelectCtrl*)controlTypeInfo->pRSCtrl;
					pModeSelect->SetPos(bMouseWheelUp);
				}					
				break;
			case SMARTPANEL_CONTROL_MAGICMODE:
				{
					CRSMagicFilterSelectCtrl * pMagicMode = (CRSMagicFilterSelectCtrl*)controlTypeInfo->pRSCtrl;
					pMagicMode->SetPos(bMouseWheelUp);
				}					
				break;
			}			
		}

		return TRUE;
	}
	
	
#endif
	//CMiLRe 20141118 스마트 패널 선택 -> 좌클릭, 마우스 무브시 테두리표시
	else if(WM_MOUSEMOVE == pMsg->message)
	{
		if(m_pSmartPanelChild)
		{
			if(m_pSmartPanelChild->IsWindowVisible())
			{
				//CMiLRe 20141127 스마트패널 차일드 마우스 리브때 사라짐
				//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
				//m_pSmartPanelChild->ShowWindow(SW_HIDE);
				return FALSE;
			}
		}

		if(!m_btnLoadBank.IsWindowVisible())
			((CNXRemoteStudioDlg*)GetParent())->HidePopup();

		//-- 2011-8-5 Lee JungTaek
		if(IsCapturing())
			return TRUE;

		nCtrlID = ::GetDlgCtrlID(pMsg->hwnd);
		strCtrlID.Format(_T("%d\n"), nCtrlID);
				
		//-- Count Control Count
		int nSmartPanelCtrCnt = GetSmartPanelCtrCnt(m_nMode);	

		//-- Check Unwanted Control ID
		if(nCtrlID < ID_BTN_SMARTPANEL || nCtrlID > ID_BTN_SMARTPANEL + nSmartPanelCtrCnt)
			return FALSE;

		ChangePrePropertyImage(nCtrlID);

		return TRUE;
	}

	//CMiLRe 20141118 Enter, ESC 키 누를 시 화면 사라지거나 종료되는 문제
	else if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE )
			pMsg->wParam = NULL;	
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropertyDescription
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	Get Property Description and Display
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::SetPropertyDescription(int nPropIndex)
{
	ControlTypeInfo* pControlTypeInfo = GetCtrlInfo(nPropIndex);
	//-- Check Layout Info
	if(!pControlTypeInfo)
		return;
	
	m_strPropDesc = m_rsLayoutInfo.SetPropertyDescription(pControlTypeInfo, nPropIndex);
	
	//2014-09-20 dh0.seo 추가
//	Invalidate(FALSE);
	InvalidateRect(m_rectPropDesc, TRUE);
}

void CSmartPanelDlg::HelpBtnClick()
{
	CString strPath = RSGetRoot(_T("SRS_User_Manual_eng_v2.0.5.docx"));
	ShellExecute(NULL ,_T("open"), strPath, NULL , NULL, SW_SHOWNORMAL);
}

//------------------------------------------------------------------------------ 
//! @brief		MutliapplyBtnClick
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Open Multi Apply Dialog
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::MutliapplyBtnClick()
{
	if(((CNXRemoteStudioDlg*)GetParent())->m_SdiMultiMgr.GetPTPCount())
	{
		if(m_pSaveBankEdit)
		{
			delete m_pSaveBankEdit;
			m_pSaveBankEdit = NULL;
		}
		
		((CNXRemoteStudioDlg*)GetParent())->HidePopup(CMB_BANK);

		if(m_pMultiApplyDlg)
		{
			if(m_pMultiApplyDlg->IsWindowVisible())
				m_pMultiApplyDlg->ShowWindow(SW_HIDE);
			
			//-- Move Window
			CRect rect;
			GetWindowRect(&rect);

			CRect rectChild;
			m_pMultiApplyDlg->GetWindowRect(&rectChild);

			CPoint point;
			GetCursorPos(&point);

			CheckWindowRect(rect.right - rectChild.Width(), rect.bottom - rectChild.Height() -20, rectChild.Width(), rectChild.Height(), (CWnd*)m_pMultiApplyDlg);

			m_pMultiApplyDlg->UpdateBankInfo();
			m_pMultiApplyDlg->ShowWindow(SW_SHOW);			
		}
		else
		{
			m_pMultiApplyDlg = new CMultiApplyDlg();
			m_pMultiApplyDlg->Create(CMultiApplyDlg::IDD, this);

			m_pMultiApplyDlg->ShowWindow(SW_SHOW);
		}
	}
}

//------------------------------------------------------------------------------ 
//! @brief		SaveBankBtnClick
//! @date		2011-7-11
//! @author		Lee JungTaek
//! @note	 	Save Bank Click
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::SaveBankBtnClick()
{
	if(m_pMultiApplyDlg)	m_pMultiApplyDlg->ShowWindow(SW_HIDE);

	if(m_pSaveBankEdit)
	{
		delete m_pSaveBankEdit;
		m_pSaveBankEdit = NULL;
	}
	((CNXRemoteStudioDlg*)GetParent())->HidePopup(CMB_BANK);

	m_pSaveBankEdit = new CSmartPanelEditDlg();
	m_pSaveBankEdit->Create(CSmartPanelEditDlg::IDD, this);

	CRect thisRect;
 	//m_pSaveBankEdit->GetWindowRect(&thisRect);

	GetParent()->GetWindowRect(&thisRect);

	CPoint point;
	GetCursorPos(&point);
	//CheckWindowRect(point.x - thisRect.Width()/5, point.y - thisRect.Height() + 100, thisRect.Width() + 6, thisRect.Height() + 8, m_pSaveBankEdit);
	//CheckWindowRect(thisRect.Width()/5, thisRect.Height() + 100, thisRect.Width() + 6, thisRect.Height() + 8, m_pSaveBankEdit);
	m_pSaveBankEdit->MoveWindow(thisRect.left+85, thisRect.top+550, 224,92);
 	m_pSaveBankEdit->ShowWindow(SW_SHOW);
}

//------------------------------------------------------------------------------ 
//! @brief		LoadBankBtnClick
//! @date		2011-7-15
//! @author		Lee JungTaek
//! @note	 	Load Bank Click
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::LoadBankBtnClick()
{
	if(m_pMultiApplyDlg)	m_pMultiApplyDlg->ShowWindow(SW_HIDE);
	if(m_pSaveBankEdit)
	{
		delete m_pSaveBankEdit;
		m_pSaveBankEdit = NULL;
	}

	CPoint point;

	GetCursorPos(&point);
	ScreenToClient(&point);

	((CNXRemoteStudioDlg*)GetParent())->PopupList(CMB_BANK,point);
}

//------------------------------------------------------------------------------ 
//! @brief		SetItem
//! @date		2010-06-10
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::SetItem(int nPTPCode, CString strValue)
{
	//-- 2011-8-16 Lee JungTaek
	//-- Close Open 2 Depth
	if(m_pSmartPanelChild)
	{
		//-- Check Control Property by directly
		if(nPTPCode == PTP_CODE_FUNCTIONALMODE || nPTPCode == PTP_CODE_SAMSUNG_OIS || nPTPCode == PTP_CODE_FOCUSMODE)
		{
			if(m_pSmartPanelChild->IsWindowVisible())
				CloseSmartPanelChild();
		}			
	}

	int nPropIndex = -1;

	switch(m_nMode)
	{
	case DSC_MODE_P:
	case DSC_MODE_A:
	case DSC_MODE_S:
	case DSC_MODE_M:
	case DSC_MODE_BULB: //dh0.seo 2015-01-12 위치 이동
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_IMAGESIZE:			nPropIndex = DSC_PASM_PHOTOSIZE;				break;	
			default:		return;
			}		
		}
		break;
	case DSC_MODE_SMART:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_IMAGESIZE:			nPropIndex = DSC_SMARTAUTO_PHOTOSIZE;			break;	
			default:		return;
			}		
		}
		break;
	case DSC_MODE_I						:		break;			
	case DSC_MODE_CAPTURE_MOVIE			:		break;
	case DSC_MODE_MAGIC_MAGIC_FRAME		:
	case DSC_MODE_MAGIC_SMART_FILTER	:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_IMAGESIZE:			nPropIndex = DSC_MAGIC_FRAME_PHOTOSIZE;			break;	
			default:		return;
			}		
		}
		break;
	case DSC_MODE_PANORAMA				:
	case DSC_MODE_SCENE_BEAUTY			:	
	case DSC_MODE_SCENE_NIGHT			:	
	case DSC_MODE_SCENE_LANDSCAPE		:	
	case DSC_MODE_SCENE_PORTRAIT		:	
	case DSC_MODE_SCENE_CHILDREN		:	
	case DSC_MODE_SCENE_SPORTS			:	
	case DSC_MODE_SCENE_CLOSE_UP		:	
	case DSC_MODE_SCENE_TEXT			:	
	case DSC_MODE_SCENE_SUNSET			:	
	case DSC_MODE_SCENE_DAWN			:	
	case DSC_MODE_SCENE_BACKLIGHT		:	
	case DSC_MODE_SCENE_FIREWORK		:	
	case DSC_MODE_SCENE_BEACH_SNOW		:	
	case DSC_MODE_SCENE_SOUND_PICTURE	:	
	case DSC_MODE_SCENE_3D				:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_IMAGESIZE:			nPropIndex = DSC_SCENE_BEAUTY_PHOTOSIZE;			break;	
			default:		return;
			}		
		}
		break;
	case DSC_MODE_SCENE_BEST			:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_IMAGESIZE:			nPropIndex = DSC_SCENE_BEST_PHOTOSIZE;			break;	
			default:		return;
			}		
		}
		break;
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_IMAGESIZE:			nPropIndex = DSC_SCENE_MACRO_PHOTOSIZE;			break;	
			default:		return;
			}		
		}
		break;
	case DSC_MODE_SCENE_ACTION			:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_IMAGESIZE:			nPropIndex = DSC_SCENE_ACTION_PHOTOSIZE;			break;	
			default:		return;
			}		
		}
		break;
	case DSC_MODE_SCENE_CREATIVE		:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_IMAGESIZE:			nPropIndex = DSC_SCENE_CREATIVE_PHOTOSIZE;			break;	
			default:		return;
			}		
		}
		break;
	case DSC_MODE_SCENE_PANORAMA		:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_IMAGESIZE:			nPropIndex = DSC_SCENE_BEST_PHOTOSIZE;			break;	
			default:		return;
			}		
		}
		break;
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_IMAGESIZE:			nPropIndex = DSC_SCENE_ACTION_FREEZE_PHOTOSIZE;			break;	
			default:		return;
			}		
		}
		break;
	case DSC_MODE_SCENE_LIGHT_TRACE		:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_IMAGESIZE:			nPropIndex = DSC_SCENE_LIGHT_TRACE_PHOTOSIZE;			break;	
			default:		return;
			}		
		}
		break;
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_IMAGESIZE:			nPropIndex = DSC_SCENE_MULTI_EXPOSURE_PHOTOSIZE;			break;	
			default:		return;
			}		
		}
		break;
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_IMAGESIZE:			nPropIndex = DSC_SCENE_AUTO_SHUTTER_PHOTOSIZE;			break;	
			default:		return;
			}		
		}
		break;	
	case DSC_MODE_MOVIE					:		break;
	}
	
	//-- 2011-8-4 Lee JungTaek
	//-- Check One more
	if(nPropIndex < 0)
		return;

	//-- Get Control
	ControlTypeInfo* pControlTypeInfo = GetCtrlInfo(nPropIndex);
	//-- Check Layout Info
	if(!pControlTypeInfo)
			return;
	//-- 2011-6-15 Lee JungTaek
	int nStatus = PROPERTY_STATUS_DISABLE;
	nStatus = GetPropValueStatus(SMARTPANEL_DEPTH_TYPE_1ST, nPropIndex);

	if(m_nSelectPTPCode == nPTPCode)
	{
		if(nStatus == PROPERTY_STATUS_DISABLE)
		{
			m_nSelectPTPCode = -1;
			m_nPrePropIndex = -1;
		}
		else
			nStatus = PROPERTY_STATUS_SELECTED;
	}

	//-- DrawIcon		
	m_rsLayoutInfo.DrawIcon(pControlTypeInfo, nPropIndex, strValue, nStatus);

	//-- 2011-7-14 Lee JungTaek
	//-- Set Property Description
//	if(m_nSelectPTPCode == nPTPCode)
	if(m_nChangePTPCode == nPTPCode)
		SetPropertyDescription(nPropIndex);

}

//CMiLRe 20141114 IOS Auto Range가 Value에서 Enum으로 변경
BOOL CSmartPanelDlg::Set3DMode(int nPTPCode, int nPropValue)
{
	if(nPTPCode == PTP_DPC_SAMSUNG_3D_SHOT && nPropValue == 0x81)
	{
		m_b3DMode = TRUE;
		this->ShowWindow((SW_HIDE));
	}
	else
	{
		m_b3DMode = FALSE;
		this->ShowWindow((SW_SHOW));
	}

	SetApplyMode(!m_b3DMode);	
	return m_b3DMode;
}

//------------------------------------------------------------------------------ 
//! @brief		SetItem
//! @date		2010-06-10
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::SetItem(int nPTPCode, int nPropValue)
{
	//-- 2011-8-16 Lee JungTaek
	//-- Close Open 2 Depth
	if(m_pSmartPanelChild)
	{
		//-- Check Control Property by directly
		if(nPTPCode == PTP_CODE_FUNCTIONALMODE || nPTPCode == PTP_CODE_SAMSUNG_OIS || nPTPCode == PTP_CODE_FOCUSMODE)
		{
			if(m_pSmartPanelChild->IsWindowVisible())
				CloseSmartPanelChild();
		}	
	}

	ControlTypeInfo* pControlTypeInfo = NULL;	
	int nPropIndex = -1;

	switch(m_nMode)
	{
	case DSC_MODE_P:
	case DSC_MODE_A:
	case DSC_MODE_S:
	case DSC_MODE_M:
	case DSC_MODE_BULB: //dh0.seo 2015-01-12 위치 이동
		{
			/*
			if(m_b3DMode)
			{
				break;
			}
			else*/
			{
				switch(nPTPCode)
				{
				case	PTP_CODE_SAMSUNG_SHUTTERSPEED:		nPropIndex = DSC_PASM_SHUTTERSPEED;		break;
				case	PTP_CODE_F_NUMBER:					nPropIndex = DSC_PASM_APERTURE;			break;
				case	PTP_CODE_SAMSUNG_OIS:				nPropIndex = DSC_PASM_OIS;				break;
				case	PTP_CODE_SAMSUNG_EV:				nPropIndex = DSC_PASM_EV;				break;
				case	PTP_CODE_EXPOSUREINDEX:				nPropIndex = DSC_PASM_ISO;				break;
				case	PTP_CODE_FLASHMODE:					nPropIndex = DSC_PASM_FLASH;			break;
				case	PTP_CODE_EXPOSUREMETERINGMODE:		nPropIndex = DSC_PASM_METERING;			break;
				case	PTP_CODE_WHITEBALANCE:				nPropIndex = DSC_PASM_WHITEBALANCE;		break;
				case	PTP_CODE_SAMSUNG_PICTUREWIZARD:		nPropIndex = DSC_PASM_PICTUREWIZARD;	break;
				case	PTP_CODE_SAMSUNG_CSPACE:			nPropIndex = DSC_PASM_COLORSPACE;		break;
				case	PTP_CODE_SAMSUNG_SMARTRANGE:		nPropIndex = DSC_PASM_SMARTRANGE;		break;
				case	PTP_CODE_FOCUSMODE:					nPropIndex = DSC_PASM_AFMODE;			break;
				case	PTP_CODE_FOCUSMETERINGMODE:			nPropIndex = DSC_PASM_FOCUSAREA;		break;
					//CMiLRe 20140902 DRIVER GET 설정
				case PTP_DPC_STILL_CAPTURE_MODE_H_FRAME:		
				case PTP_DPC_STILL_CAPTURE_MODE_L_FRAME:
				case	PTP_CODE_STILLCAPTUREMODE:			nPropIndex = DSC_PASM_DRIVE;			break;
				case	PTP_CODE_IMAGESIZE:					nPropIndex = DSC_PASM_PHOTOSIZE;		break;
				case	PTP_CODE_COMPRESSIONSETTING:		nPropIndex = DSC_PASM_QUALITY;			break;
					// dh9.seo 2012-06-21
				case	PTP_CODE_SAMSUNG_SMARTFILTER:		nPropIndex = DSC_PASM_SMARTFILTER;		break;
				default:																			return;
				}
			}		
		}
		break;
	case DSC_MODE_SMART:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_SAMSUNG_SHUTTERSPEED:		nPropIndex = DSC_SMARTAUTO_SHUTTERSPEED;	break;
			case	PTP_CODE_F_NUMBER:					nPropIndex = DSC_SMARTAUTO_APERTURE;		break;
			case	PTP_DPC_STILL_CAPTURE_MODE_H_FRAME:		
			case	PTP_DPC_STILL_CAPTURE_MODE_L_FRAME:
			case	PTP_CODE_STILLCAPTUREMODE:			nPropIndex = DSC_SMARTAUTO_DRIVE;			break;
			case	PTP_CODE_SAMSUNG_CSPACE:			nPropIndex = DSC_SMARTAUTO_COLORSPACE;		break;
			case	PTP_CODE_FLASHMODE:					nPropIndex = DSC_SMARTAUTO_FLASH;			break;
			case	PTP_CODE_IMAGESIZE:					nPropIndex = DSC_SMARTAUTO_PHOTOSIZE;		break;
			// dh9.seo 2013-06-22
			case	PTP_CODE_SAMSUNG_OIS:				nPropIndex = DSC_SMARTAUTO_OIS;				break;
			case	PTP_CODE_FOCUSMODE:					nPropIndex = DSC_SMARTAUTO_AFMODE;			break;
			default:																			return;
			}		
		}
		break;	
	case DSC_MODE_I						:		break;
	case DSC_MODE_CAPTURE_MOVIE			:		break;
	case DSC_MODE_MAGIC_MAGIC_FRAME		:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_SAMSUNG_MAGIC_MODE		:	nPropIndex = DSC_MAGIC_FRAME_MAGICMODECHANGE	;	break;
			case	PTP_CODE_SAMSUNG_MAGIC_FRAME	:	nPropIndex = DSC_MAGIC_FRAME_MAGICTYPECHANGE	;	break;
			case	PTP_CODE_FOCUSMODE				:	nPropIndex = DSC_MAGIC_FRAME_AFMODE				;	break;
			case	PTP_CODE_SAMSUNG_CSPACE			:	nPropIndex = DSC_MAGIC_FRAME_COLORSPACE			;	break;
			case	PTP_CODE_IMAGESIZE				:	nPropIndex = DSC_MAGIC_FRAME_PHOTOSIZE			;	break;
			case	PTP_CODE_COMPRESSIONSETTING		:	nPropIndex = DSC_MAGIC_FRAME_QUALITY			;	break;
			case	PTP_CODE_STILLCAPTUREMODE		:	nPropIndex = DSC_MAGIC_FRAME_DRIVE				;	break;
			case	PTP_CODE_SAMSUNG_OIS			:	nPropIndex = DSC_MAGIC_FRAME_OIS				;	break;	
			default:																			return;
			}		
		}
		break;
	case DSC_MODE_MAGIC_SMART_FILTER	:
		{
			switch(nPTPCode)
			{
			case	PTP_CODE_SAMSUNG_MAGIC_MODE		:	nPropIndex = DSC_SMART_FILTER_MAGICMODECHANGE	;	break;
			case	PTP_CODE_SAMSUNG_SMARTFILTER	:	nPropIndex = DSC_SMART_FILTER_MAGICTYPECHANGE	;	break;
			case	PTP_CODE_FOCUSMODE				:	nPropIndex = DSC_SMART_FILTER_AFMODE			;	break;
			case	PTP_CODE_SAMSUNG_CSPACE			:	nPropIndex = DSC_SMART_FILTER_COLORSPACE		;	break;
			case	PTP_CODE_IMAGESIZE				:	nPropIndex = DSC_SMART_FILTER_PHOTOSIZE			;	break;
			case	PTP_CODE_COMPRESSIONSETTING		:	nPropIndex = DSC_SMART_FILTER_QUALITY			;	break;
			case	PTP_CODE_STILLCAPTUREMODE		:	nPropIndex = DSC_SMART_FILTER_DRIVE				;	break;
			case	PTP_CODE_SAMSUNG_OIS			:	nPropIndex = DSC_SMART_FILTER_OIS				;	break;	
			default:																			return;
			}		
		}
		break;
	case DSC_MODE_PANORAMA				:
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_AUTO_SHUTTER_SCENECHANGE		;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:	nPropIndex = DSC_SCENE_AUTO_SHUTTER_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:	nPropIndex = DSC_SCENE_AUTO_SHUTTER_APERTURE		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:	nPropIndex = DSC_SCENE_AUTO_SHUTTER_QUALITY			;	break;
			default:																			return;
			}
		}
		break;
	case DSC_MODE_SCENE_BEAUTY			:	
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:		nPropIndex = DSC_SCENE_BEAUTY_SCENECHANGE	;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:		nPropIndex = DSC_SCENE_BEAUTY_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:		nPropIndex = DSC_SCENE_BEAUTY_APERTURE		;	break;
			case PTP_CODE_FOCUSMODE				:		nPropIndex = DSC_SCENE_BEAUTY_AFMODE		;	break;
			case PTP_CODE_FOCUSMETERINGMODE		:		nPropIndex = DSC_SCENE_BEAUTY_FOCUSAREA		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:		nPropIndex = DSC_SCENE_BEAUTY_QUALITY		;	break;
			case PTP_CODE_STILLCAPTUREMODE		:		nPropIndex = DSC_SCENE_BEAUTY_DRIVE			;	break;
			case PTP_CODE_SAMSUNG_CSPACE		:		nPropIndex = DSC_SCENE_BEAUTY_COLORSPACE	;	break;
			case PTP_CODE_SAMSUNG_OIS			:		nPropIndex = DSC_SCENE_BEAUTY_OIS			;	break;
			case PTP_CODE_FLASHMODE				:		nPropIndex = DSC_SCENE_BEAUTY_FLASH			;	break;
			case PTP_CODE_SAMSUNG_FACE_TONE		:		nPropIndex = DSC_SCENE_BEAUTY_FACETONE		;	break;
			case PTP_CODE_SAMSUNG_FACE_RETOUCH	:		nPropIndex = DSC_SCENE_BEAUTY_FACERETOUCH	;	break;	
			default:																			return;
			}
		}
		break;
	case DSC_MODE_SCENE_PORTRAIT		:	
	case DSC_MODE_SCENE_NIGHT			:	
	case DSC_MODE_SCENE_CHILDREN		:	
	case DSC_MODE_SCENE_SPORTS			:	
	case DSC_MODE_SCENE_BACKLIGHT		:
	case DSC_MODE_SCENE_BEACH_SNOW		:
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_PORTRAIT_SCENECHANGE		;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:	nPropIndex = DSC_SCENE_PORTRAIT_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:	nPropIndex = DSC_SCENE_PORTRAIT_APERTURE		;	break;
			case PTP_CODE_FOCUSMODE				:	nPropIndex = DSC_SCENE_PORTRAIT_AFMODE			;	break;
			case PTP_CODE_FOCUSMETERINGMODE		:	nPropIndex = DSC_SCENE_PORTRAIT_FOCUSAREA		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:	nPropIndex = DSC_SCENE_PORTRAIT_QUALITY			;	break;
			case PTP_CODE_STILLCAPTUREMODE		:	nPropIndex = DSC_SCENE_PORTRAIT_DRIVE			;	break;
			case PTP_CODE_SAMSUNG_CSPACE		:	nPropIndex = DSC_SCENE_PORTRAIT_COLORSPACE		;	break;
			case PTP_CODE_SAMSUNG_OIS			:	nPropIndex = DSC_SCENE_PORTRAIT_OIS				;	break;
			case PTP_CODE_FLASHMODE				:	nPropIndex = DSC_SCENE_PORTRAIT_FLASH			;	break;
			default:								return;
			}
		}
		break;		
	case DSC_MODE_SCENE_DAWN			:
	case DSC_MODE_SCENE_SUNSET			:	
	case DSC_MODE_SCENE_TEXT			:
	case DSC_MODE_SCENE_CLOSE_UP		:	
	case DSC_MODE_SCENE_LANDSCAPE		:	
	case DSC_MODE_SCENE_3D				:
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_BACKLIGHT_SCENECHANGE	;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:	nPropIndex = DSC_SCENE_BACKLIGHT_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:	nPropIndex = DSC_SCENE_BACKLIGHT_APERTURE		;	break;
			case PTP_CODE_FOCUSMODE				:	nPropIndex = DSC_SCENE_BACKLIGHT_AFMODE			;	break;
			case PTP_CODE_FOCUSMETERINGMODE		:	nPropIndex = DSC_SCENE_BACKLIGHT_FOCUSAREA		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:	nPropIndex = DSC_SCENE_BACKLIGHT_QUALITY		;	break;
			case PTP_CODE_STILLCAPTUREMODE		:	nPropIndex = DSC_SCENE_BACKLIGHT_DRIVE			;	break;
			case PTP_CODE_SAMSUNG_CSPACE		:	nPropIndex = DSC_SCENE_BACKLIGHT_COLORSPACE		;	break;
			case PTP_CODE_SAMSUNG_OIS			:	nPropIndex = DSC_SCENE_BACKLIGHT_OIS			;	break;
			default:								return;
			}
		}
		break;	
	case DSC_MODE_SCENE_FIREWORK		:	
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_FIREWORK_SCENECHANGE		;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:	nPropIndex = DSC_SCENE_FIREWORK_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:	nPropIndex = DSC_SCENE_FIREWORK_APERTURE		;	break;
			case PTP_CODE_FOCUSMODE				:	nPropIndex = DSC_SCENE_FIREWORK_AFMODE			;	break;
			case PTP_CODE_SAMSUNG_CSPACE		:	nPropIndex = DSC_SCENE_FIREWORK_COLORSPACE		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:	nPropIndex = DSC_SCENE_FIREWORK_QUALITY			;	break;
			case PTP_CODE_STILLCAPTUREMODE		:	nPropIndex = DSC_SCENE_FIREWORK_DRIVE			;	break;
			case PTP_CODE_SAMSUNG_OIS			:	nPropIndex = DSC_SCENE_FIREWORK_OIS				;	break;
			default:																			return;
			}
		}
		break;
	case DSC_MODE_SCENE_SOUND_PICTURE	:		
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_SOUNDPICTURE_SCENECHANGE		;	break;
			default:								return;
			}
		}
		break;
	case DSC_MODE_MOVIE					:		break;
	// dh9.seo 2013-06-24
	case DSC_MODE_SCENE_BEST	:
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_BEST_SCENECHANGE		;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:	nPropIndex = DSC_SCENE_BEST_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:	nPropIndex = DSC_SCENE_BEST_APERTURE		;	break;
			case PTP_CODE_IMAGESIZE				:	nPropIndex = DSC_SCENE_BEST_PHOTOSIZE		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:	nPropIndex = DSC_SCENE_BEST_QUALITY			;	break;
			case PTP_CODE_STILLCAPTUREMODE		:	nPropIndex = DSC_SCENE_BEST_DRIVE			;	break;
			default:																			return;
			}
		}
		break;
	case DSC_MODE_SCENE_ACTION	:
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_ACTION_SCENECHANGE	;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:	nPropIndex = DSC_SCENE_ACTION_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:	nPropIndex = DSC_SCENE_ACTION_APERTURE		;	break;
			case PTP_CODE_FOCUSMODE				:	nPropIndex = DSC_SCENE_ACTION_AFMODE		;	break;
			case PTP_CODE_FOCUSMETERINGMODE		:	nPropIndex = DSC_SCENE_ACTION_FOCUSAREA		;	break;
			case PTP_CODE_IMAGESIZE				:	nPropIndex = DSC_SCENE_ACTION_PHOTOSIZE		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:	nPropIndex = DSC_SCENE_ACTION_QUALITY		;	break;
			case PTP_CODE_STILLCAPTUREMODE		:	nPropIndex = DSC_SCENE_ACTION_DRIVE			;	break;
			default:																			return;
			}		
		}
		break;
	case DSC_MODE_SCENE_MACRO		:
	case DSC_MODE_SCENE_RICH		:
	case DSC_MODE_SCENE_WATERFALL	:	
	case DSC_MODE_SCENE_SILHOUETTE	:
	case DSC_MODE_SCENE_LIGHT		:
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_LIGHT_SCENECHANGE	;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:	nPropIndex = DSC_SCENE_LIGHT_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:	nPropIndex = DSC_SCENE_LIGHT_APERTURE		;	break;
			case PTP_CODE_FOCUSMETERINGMODE		:	nPropIndex = DSC_SCENE_LIGHT_FOCUSAREA		;	break;
			case PTP_CODE_IMAGESIZE				:	nPropIndex = DSC_SCENE_LIGHT_PHOTOSIZE		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:	nPropIndex = DSC_SCENE_LIGHT_QUALITY		;	break;
			case PTP_CODE_STILLCAPTUREMODE		:	nPropIndex = DSC_SCENE_LIGHT_DRIVE			;	break;
			default:																			return;
			}
		}
		break;
	case DSC_MODE_SCENE_CREATIVE		:
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_CREATIVE_SCENECHANGE		;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:	nPropIndex = DSC_SCENE_CREATIVE_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:	nPropIndex = DSC_SCENE_CREATIVE_APERTURE		;	break;
			case PTP_CODE_IMAGESIZE				:	nPropIndex = DSC_SCENE_CREATIVE_PHOTOSIZE		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:	nPropIndex = DSC_SCENE_CREATIVE_QUALITY			;	break;
			case PTP_CODE_STILLCAPTUREMODE		:	nPropIndex = DSC_SCENE_CREATIVE_DRIVE			;	break;
			case PTP_CODE_FLASHMODE				:	nPropIndex = DSC_SCENE_CREATIVE_FLASH			;	break;
			default:																			return;
			}
		}
		break;
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_ACTION_FREEZE_SCENECHANGE	;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:	nPropIndex = DSC_SCENE_ACTION_FREEZE_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:	nPropIndex = DSC_SCENE_ACTION_FREEZE_APERTURE		;	break;
			case PTP_CODE_FOCUSMODE				:	nPropIndex = DSC_SCENE_ACTION_FREEZE_AFMODE			;	break;
			case PTP_CODE_FOCUSMETERINGMODE		:	nPropIndex = DSC_SCENE_ACTION_FREEZE_FOCUSAREA		;	break;
			case PTP_CODE_IMAGESIZE				:	nPropIndex = DSC_SCENE_ACTION_FREEZE_PHOTOSIZE		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:	nPropIndex = DSC_SCENE_ACTION_FREEZE_QUALITY		;	break;
			case PTP_DPC_STILL_CAPTURE_MODE_H_FRAME:		
			case PTP_DPC_STILL_CAPTURE_MODE_L_FRAME:
			case PTP_CODE_STILLCAPTUREMODE		:	nPropIndex = DSC_SCENE_ACTION_FREEZE_DRIVE			;	break;
			default:																			return;
			}		
		}
		break;
	case DSC_MODE_SCENE_LIGHT_TRACE		:
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_LIGHT_TRACE_SCENECHANGE		;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:	nPropIndex = DSC_SCENE_LIGHT_TRACE_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:	nPropIndex = DSC_SCENE_LIGHT_TRACE_APERTURE		;	break;
			case PTP_CODE_FOCUSMETERINGMODE		:	nPropIndex = DSC_SCENE_LIGHT_TRACE_FOCUSAREA		;	break;
			case PTP_CODE_IMAGESIZE				:	nPropIndex = DSC_SCENE_LIGHT_TRACE_PHOTOSIZE		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:	nPropIndex = DSC_SCENE_LIGHT_TRACE_QUALITY			;	break;
			case PTP_CODE_STILLCAPTUREMODE		:	nPropIndex = DSC_SCENE_LIGHT_TRACE_DRIVE			;	break;
			default:																			return;
			}
		}
		break;
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_MULTI_EXPOSURE_SCENECHANGE		;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:	nPropIndex = DSC_SCENE_MULTI_EXPOSURE_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:	nPropIndex = DSC_SCENE_MULTI_EXPOSURE_APERTURE		;	break;
			case PTP_CODE_FOCUSMETERINGMODE		:	nPropIndex = DSC_SCENE_MULTI_EXPOSURE_FOCUSAREA		;	break;
			case PTP_CODE_IMAGESIZE				:	nPropIndex = DSC_SCENE_MULTI_EXPOSURE_PHOTOSIZE		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:	nPropIndex = DSC_SCENE_MULTI_EXPOSURE_QUALITY			;	break;
			default:																			return;
			}
		}
		break;
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_AUTO_SHUTTER_SCENECHANGE		;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:	nPropIndex = DSC_SCENE_AUTO_SHUTTER_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:	nPropIndex = DSC_SCENE_AUTO_SHUTTER_APERTURE		;	break;
			case PTP_CODE_IMAGESIZE				:	nPropIndex = DSC_SCENE_AUTO_SHUTTER_PHOTOSIZE		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:	nPropIndex = DSC_SCENE_AUTO_SHUTTER_QUALITY			;	break;
			default:																			return;
			}
		}
		break;
	case DSC_MODE_SCENE_PANORAMA		:					
		{
			switch(nPTPCode)
			{
			case PTP_CODE_SAMSUNG_SCENE_MODE	:	nPropIndex = DSC_SCENE_AUTO_SHUTTER_SCENECHANGE		;	break;
			case PTP_CODE_SAMSUNG_SHUTTERSPEED	:	nPropIndex = DSC_SCENE_AUTO_SHUTTER_SHUTTERSPEED	;	break;
			case PTP_CODE_F_NUMBER				:	nPropIndex = DSC_SCENE_AUTO_SHUTTER_APERTURE		;	break;
			case PTP_CODE_COMPRESSIONSETTING	:	nPropIndex = DSC_SCENE_AUTO_SHUTTER_QUALITY			;	break;
			default:																			return;
			}
		}
		break;
	}	

	//-- 2011-8-4 Lee JungTaek
	//-- Check One More
	if(nPropIndex < 0)
		return;

	//-- Get Control
	pControlTypeInfo = GetCtrlInfo(nPropIndex);

	//-- Check Layout Info
	if(!pControlTypeInfo)
		return;

	//-- 2011-6-15 Lee JungTaek
	int nStatus = PROPERTY_STATUS_DISABLE;
	nStatus = GetPropValueStatus(SMARTPANEL_DEPTH_TYPE_1ST, nPropIndex);

	if(m_nSelectPTPCode == nPTPCode)
	{
		if(nStatus == PROPERTY_STATUS_DISABLE)
		{	
			m_nPrePropIndex = -1;
			m_nSelectPTPCode = -1;
		}
		else
			nStatus = PROPERTY_STATUS_SELECTED;
	}

	//-- 2011-6-21 Lee JungTaek
	switch(m_nMode)
	{
	case DSC_MODE_P:
	case DSC_MODE_A:
	case DSC_MODE_S:
	case DSC_MODE_M:
	case DSC_MODE_BULB: //dh0.seo 2015-01-12 위치 이동
		{
			switch(nPropIndex)
			{
			case DSC_PASM_APERTURE:	
				{
					CUIntArray *pazEnum = ((CNXRemoteStudioDlg*)GetParent())->GetPropValueListEnum(nPTPCode);

					CRSBarometerCtrl *pRSBarometer = (CRSBarometerCtrl *)pControlTypeInfo->pRSCtrl;
					pRSBarometer->SetFNumberList(pazEnum);
				}
				break;
			case DSC_PASM_ISO:
				{
					CUIntArray *pazEnum = ((CNXRemoteStudioDlg*)GetParent())->GetPropValueListEnum(nPTPCode);

					CRSBarometerCtrl *pRSBarometer = (CRSBarometerCtrl *)pControlTypeInfo->pRSCtrl;
					pRSBarometer->SetISOList(pazEnum);
				}
			default:	break;
			}
		}
		break;
	case DSC_MODE_SMART:
	case DSC_MODE_I						:		break;		
	case DSC_MODE_CAPTURE_MOVIE			:		break;
	case DSC_MODE_MAGIC_MAGIC_FRAME		:		break;
	case DSC_MODE_MAGIC_SMART_FILTER	:		break;
	case DSC_MODE_PANORAMA				:
	case DSC_MODE_SCENE_BEAUTY			:		
	case DSC_MODE_SCENE_NIGHT			:	
	case DSC_MODE_SCENE_LANDSCAPE		:	
	case DSC_MODE_SCENE_PORTRAIT		:	
	case DSC_MODE_SCENE_CHILDREN		:	
	case DSC_MODE_SCENE_SPORTS			:	
	case DSC_MODE_SCENE_CLOSE_UP		:	
	case DSC_MODE_SCENE_TEXT			:	
	case DSC_MODE_SCENE_SUNSET			:	
	case DSC_MODE_SCENE_DAWN			:	
	case DSC_MODE_SCENE_BACKLIGHT		:	
	case DSC_MODE_SCENE_FIREWORK		:	
	case DSC_MODE_SCENE_BEACH_SNOW		:	
	case DSC_MODE_SCENE_SOUND_PICTURE	:
	// dh9.seo 2013-06-25
	case DSC_MODE_SCENE_3D				:
	case DSC_MODE_SCENE_BEST			:
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_ACTION			:
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
	case DSC_MODE_SCENE_CREATIVE		:
	case DSC_MODE_SCENE_PANORAMA		:
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
	case DSC_MODE_SCENE_LIGHT_TRACE		:
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_BEAUTY_APERTURE:	
				{
					CUIntArray *pazEnum = ((CNXRemoteStudioDlg*)GetParent())->GetPropValueListEnum(nPTPCode);

					CRSBarometerCtrl *pRSBarometer = (CRSBarometerCtrl *)pControlTypeInfo->pRSCtrl;
					pRSBarometer->SetFNumberList(pazEnum);
				}
				break;
			default:	break;
			}
		}
		break;
	case DSC_MODE_MOVIE					:		break;
	}	
	
	//-- DrawIcon		
	m_rsLayoutInfo.DrawIcon(pControlTypeInfo, nPropIndex, nPropValue, nStatus);
	//-- 2011-7-14 Lee JungTaek
	//-- Set Property Description
//	if(m_nSelectPTPCode == nPTPCode)
	if(m_nChangePTPCode == nPTPCode)
		SetPropertyDescription(nPropIndex);
	else if(nPTPCode == 0xd846)
		SetPropertyDescription(nPropIndex);

	//2014-10-20 dh0.seo 추가
/*	if(m_pSmartBMChildDlg)
		m_pSmartBMChildDlg->ShowWindow(SW_HIDE);
	if(m_pSmartSDChildDlg)
		m_pSmartSDChildDlg->ShowWindow(SW_HIDE);
	if(m_pSmartMSChildDlg)
		m_pSmartMSChildDlg->ShowWindow(SW_HIDE); */

	if(nPTPCode == PTP_CODE_FOCUSMODE)
	{
		if(nPropValue == eUCS_CAP_AF_MODE_MF)
			RSSetMFFocus(TRUE);
		else
			RSSetMFFocus(FALSE);
	}
}

LRESULT CSmartPanelDlg::OnNcHitTest(CPoint point)
{
	LRESULT nHitTest = CWnd::OnNcHitTest(point);
	if( nHitTest == HTCLIENT)
		nHitTest = HTCAPTION;
	return nHitTest;
}

//------------------------------------------------------------------------------ 
//! @brief		SetTargetMode
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	Set TargetMdoe
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::SetTargetMode(int nMode, BOOL bCreate)
{
	m_bApplyMode = FALSE;

	for(int i = 0; i < ARRAYSIZE(SZNX200PropModeValue); i++)
	{
		if(nMode == SZNX200PropModeValue[i].nValue)
		{
			m_strMode = SZNX200PropModeValue[i].strDesc;
			m_nMode = SZNX200PropModeValue[i].nIndex;
			
			switch(m_nMode)
			{
			case DSC_MODE_SMART :
			case DSC_MODE_P		:
			case DSC_MODE_A		:
			case DSC_MODE_S		:
			case DSC_MODE_M		:
			case DSC_MODE_BULB					: //dh0.seo 2015-01-12 위치 이동
			case DSC_MODE_I						:
			case DSC_MODE_CAPTURE_MOVIE			:
			case DSC_MODE_MAGIC_MAGIC_FRAME		:
			case DSC_MODE_MAGIC_SMART_FILTER	:			
			case DSC_MODE_SCENE_SPORTS			:
			case DSC_MODE_SCENE_CLOSE_UP		:
			case DSC_MODE_SCENE_TEXT			:
			case DSC_MODE_SCENE_FIREWORK		:
			case DSC_MODE_SCENE_BEACH_SNOW		:
			case DSC_MODE_SCENE_SOUND_PICTURE	:
			case DSC_MODE_SCENE_3D				:
				if(m_nMode == DSC_MODE_M && m_b3DMode == TRUE)
					m_bApplyMode = FALSE;
				else
					m_bApplyMode = TRUE;	
				break;
			case DSC_MODE_SCENE_BEAUTY			:
			case DSC_MODE_SCENE_NIGHT			:
			case DSC_MODE_SCENE_LANDSCAPE		:
			case DSC_MODE_SCENE_PORTRAIT		:
			case DSC_MODE_SCENE_CHILDREN		:
			case DSC_MODE_SCENE_SUNSET			:
			case DSC_MODE_SCENE_DAWN			:
			// dh9.seo 2013-06-25
			case DSC_MODE_SCENE_BEST			:
			case DSC_MODE_SCENE_MACRO			:
			case DSC_MODE_SCENE_ACTION			:
			case DSC_MODE_SCENE_RICH			:
			case DSC_MODE_SCENE_WATERFALL		:
			case DSC_MODE_SCENE_SILHOUETTE		:
			case DSC_MODE_SCENE_LIGHT			:
			case DSC_MODE_SCENE_CREATIVE		:
			case DSC_MODE_SCENE_PANORAMA		:
			case DSC_MODE_SCENE_ACTION_FREEZE	:
			case DSC_MODE_SCENE_LIGHT_TRACE		:
			case DSC_MODE_SCENE_MULTI_EXPOSURE	:
			case DSC_MODE_SCENE_AUTO_SHUTTER	:
			case DSC_MODE_PANORAMA				:
			case DSC_MODE_SCENE_BACKLIGHT		:			m_bApplyMode = !((CNXRemoteStudioDlg*)GetParent())->IsIMode();			break;
			case DSC_MODE_SCENE_SMART_FILTER	:			((CNXRemoteStudioDlg*)GetParent())->GetProperty(PTP_CODE_SAMSUNG_I_SMART_FILTER); 	m_bApplyMode = FALSE;	break;
			case DSC_MODE_MOVIE					:			m_bApplyMode = FALSE;	break;			
			default:										break;
			}
			break;
		}		
	}

	if(!m_bApplyMode)
	{
		m_strMode = _T("");
		m_nMode = DSC_MODE_NULL;
	}

}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-26
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
	case timer_start_check_mode:
		{
			TRACE(_T("In Timer\n"));
			m_bModeChange = TRUE;
		}
		break;
	}

	CRSChildDialog::OnTimer(nIDEvent);
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-28
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelDlg::SetDetailProperty(int nPropIndex, int nValue)
{
	if(m_pSmartPanelChild)
	{
		m_pSmartPanelChild->SetDetailProperty(nPropIndex, nValue);
	}
}
