/////////////////////////////////////////////////////////////////////////////
//
//  LiveviewFrmDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LiveviewFrmDlg.h"
#include "SRSIndex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CLiveviewFrmDlg dialog
IMPLEMENT_DYNAMIC(CLiveviewFrmDlg,  CRSChildDialog)

CLiveviewFrmDlg::CLiveviewFrmDlg(CWnd* pParent /*=NULL*/)
	: CRSChildDialog(CLiveviewFrmDlg::IDD, pParent)
{
	m_pLiveview		= NULL;	
	m_nShift		= SHIFT_0;
	m_nRatio		= 0;
}

CLiveviewFrmDlg::~CLiveviewFrmDlg()
{
	if(m_pLiveview)
	{
		delete m_pLiveview;
		m_pLiveview = NULL;
	}
}

void CLiveviewFrmDlg::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);	
}

BEGIN_MESSAGE_MAP(CLiveviewFrmDlg, CRSChildDialog)	
	ON_WM_SIZE()	
	ON_WM_PAINT()		
END_MESSAGE_MAP()

void CLiveviewFrmDlg::OnPaint()
{	
	//-- DrawLiveview
	DrawLiveview();
	CRSChildDialog::OnPaint();
}

// CLiveviewFrmDlg message handlers
BOOL CLiveviewFrmDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();	
	
	m_pLiveview = new CLiveview(MAIN_LIVEVIEW, TRUE, LIVEVIEW_TYPE_LIVEVIEWFRM);
	m_pLiveview->Create(NULL,_T(""),WS_VISIBLE|WS_CHILD,CRect(0,0,0,0),this,IDC_LIVEVIEWFRAME);
	
	//-- Zoom Position
	m_pLiveview->SetDrawZoomPos(TRUE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLiveviewFrmDlg::OnSize(UINT nType, int cx, int cy) 
{
    CDialog::OnSize(nType, cx, cy);
	//-- 2011-05-30 Draw Liveview
	DrawLiveview();
} 

//------------------------------------------------------------------------------ 
//! @brief		DrawLiveview
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveviewFrmDlg::DrawLiveview()
{
	if(!m_pLiveview)
		return;

	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- 1. Background
	//mDC.FillRect(rect, &CBrush(RGB(63,63,63)));
	mDC.FillRect(rect, &CBrush(RGB(0,0,0)));

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);	
	Invalidate(FALSE);

	//-- 2. Liveview
	GetLiveviewRect(&rect);
	m_pLiveview->MoveWindow(rect);
	//-- 3. Set Ratio
	SetRatio(&rect);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawLiveview
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
static const int size_margin	= 0;
void CLiveviewFrmDlg::GetLiveviewRect(LPRECT lpRect)
{
	CSize srcSize = GetSrcSize();
	int width	= srcSize .cx;
	int height	= srcSize .cy;	
	if(width == 0 || height == 0)
		return;

	if(IsRotate())
	{
		//-- Change Width/Height
		int nTemp;
		nTemp	= height	;
		height		= width;
		width		= nTemp;
	}

	//-- 2011-05-19
	//-- Move To Center
	int nW, nH;
	nW	= lpRect->right		- lpRect->left;
	nH		= lpRect->bottom	- lpRect->top;
	//-- guarantee Ratio
	int nRatioW, nRatioH;
	nRatioW = (int)(((float)width/(float)height)*(float)nH);
	nRatioH = (int)(((float)height/(float)width)*(float)nW);

	if( nW > nRatioW )		
	{
		lpRect->top		= size_margin;
		lpRect->left		= nW/2 - nRatioW/2;			
		lpRect->right		= nW/2 + nRatioW/2;
		lpRect->bottom = nH - size_margin;
	}
	else
	{	
		lpRect->left		= size_margin;
		lpRect->right		= nW - size_margin;
		lpRect->top		= nH/2 - nRatioH/2;
		lpRect->bottom	= nH/2 + nRatioH/2;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		ShiftLeft(CString strPath)
//! @date		2010-05-19
//! @author	hongsu.jung
//! @note	 	Viewer Shift -90 Degree
//------------------------------------------------------------------------------ 
void CLiveviewFrmDlg::ShiftLeft()
{	
	m_nShift--;
	m_nShift += 4;
	m_nShift = m_nShift % 4;
	//-- 2011-06-01 hongsu.jung
	m_pLiveview->SetShift(m_nShift);
	Invalidate(TRUE);   
}

void CLiveviewFrmDlg::SetRotate(int nRotate)
{
	//-- Rotate
	switch(nRotate)
	{
	case eUCI_PB_ROTATE_EVENT_0:			m_nShift = SHIFT_0; break;
	case eUCI_PB_ROTATE_EVENT_LEFT_90:		m_nShift = SHIFT_90; break;
	case eUCI_PB_ROTATE_EVENT_180:			m_nShift = SHIFT_180; break;
	case eUCI_PB_ROTATE_EVENT_RIGHT_90:		m_nShift = SHIFT_270; break;
	default:
		m_nShift = 0;
		break;
	}
	m_pLiveview->SetShift(m_nShift);
	Invalidate(TRUE);   
}

//------------------------------------------------------------------------------ 
//! @brief		ShiftRight(CString strPath)
//! @date		2010-05-19
//! @author	hongsu.jung
//! @note	 	Viewer Shift +90 Degree
//------------------------------------------------------------------------------ 
void CLiveviewFrmDlg::ShiftRight()
{
	m_nShift++; 	
	m_nShift %= 4; 
	//-- 2011-06-01 hongsu.jung
	m_pLiveview->SetShift(m_nShift);
	Invalidate(TRUE); 
}

BOOL CLiveviewFrmDlg::PreTranslateMessage(MSG* pMsg) 
{

	switch(pMsg->message)
	{
	case WM_LBUTTONDBLCLK:
		{
			//-- Check ID
			int nViewID = 0;
			nViewID = ::GetDlgCtrlID(pMsg->hwnd);
 			if(nViewID == IDC_LIVEVIEWFRAME)
			{
				/*GetLiveview()->m_bDBFlag = TRUE;

				RSEvent* pMsg = new RSEvent();
				pMsg->message	= WM_RS_SET_AF_FOCUS;
				pMsg->nParam1	= GetLiveview()->m_nX;
				pMsg->nParam2	= GetLiveview()->m_nY;
				::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);*/

				/*RSEvent* pMsgToMain = NULL;
				pMsgToMain = new RSEvent();
				pMsgToMain->message	= WM_SDI_EVENT_MOVIE_CANCLE;
				::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS,  (WPARAM)WM_SDI_EVENT_MOVIE_CANCLE, (LPARAM)pMsgToMain);	*/
			}
			break;
		}		
	}

	return CRSChildDialog::PreTranslateMessage(pMsg);
}

void CLiveviewFrmDlg::SetRatio(LPRECT lpRect)
{
	float nSrcWidth;
	float nRatio;
	if(IsRotate())
		nSrcWidth	= (float) GetSrcSize().cy;
	else
		nSrcWidth	= (float) GetSrcSize().cx;

	if(nSrcWidth)
	{
		nRatio	= ( lpRect->right - lpRect->left ) / nSrcWidth * 100;
		m_nRatio = (int)nRatio;		
	}
	else
		m_nRatio = 0;	
}
