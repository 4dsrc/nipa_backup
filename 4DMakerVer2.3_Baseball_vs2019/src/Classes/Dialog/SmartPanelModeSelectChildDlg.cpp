/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelModeSelectChildDlg.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-09-15
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SmartPanelModeSelectChildDlg.h"
#include "afxdialogex.h"
#include "SmartPanelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CSmartPanelModeSelectChildDlg, CDialog)

CSmartPanelModeSelectChildDlg::CSmartPanelModeSelectChildDlg(CWnd* pParent /*=NULL*/)
	: CBkDialogST(CSmartPanelModeSelectChildDlg::IDD, pParent)
{
	m_nMouseIn = BTN_MS_NULL;	
	for(int i=0; i<BTN_MS_CNT - 1; i++)
	{
		m_nStatus[i] = BTN_MS_CNT;
		m_rectBtn[i] = CRect(0,0,0,0);
	}

	m_pRSModeSelect = NULL;

	//-- 2011-7-14 Lee JungTaek
	m_rectValeu[MODE_POS_FIRST] = CRect(CPoint(40,18),CSize(35,30));
	m_rectValeu[MODE_POS_SECOND] = CRect(CPoint(82,18),CSize(35,30));
	m_rectValeu[MODE_POS_THIRD] = CRect(CPoint(124,14),CSize(45,30));
	m_rectValeu[MODE_POS_FOURTH] = CRect(CPoint(180,18),CSize(35,30));
	m_rectValeu[MODE_POS_FIFTH] = CRect(CPoint(222,18),CSize(35,30));
	m_rectValeu[MODE_POS_SIXTH] = CRect(CPoint(264,18),CSize(35,30));
	m_rectValeu[MODE_POS_SEVENTH] = CRect(CPoint(306,18),CSize(35,30));

	m_rectBtn[BTN_MS_LEFT] = CRect(CPoint(9,10), CSize(25,24));
	m_rectBtn[BTN_MS_RIGHT] = CRect(CPoint(332,10), CSize(25,24));

	m_rectValue = CRect(CPoint(45,15), CSize(230,20));
}

CSmartPanelModeSelectChildDlg::~CSmartPanelModeSelectChildDlg()
{
}

void CSmartPanelModeSelectChildDlg::DoDataExchange(CDataExchange* pDX)
{
	CBkDialogST::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSmartPanelModeSelectChildDlg, CBkDialogST)
	ON_WM_PAINT()	
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()

BOOL CSmartPanelModeSelectChildDlg::OnInitDialog()
{
	CBkDialogST::OnInitDialog();		
	
	SetBitmap(RSGetRoot(_T("img\\02.Smart Panel\\02_bg_magic_02_popup.png")),RGB(255,255,255));

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSmartPanelModeSelectChildDlg::OnPaint()
{	
	DrawBackground();	
	CBkDialogST::OnPaint();
}

void CSmartPanelModeSelectChildDlg::DrawBackground()
{
	CPaintDC dc(this);
	
	CRect rect;
	GetClientRect(&rect);

	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- Draw Background
	DrawImageBase(&mDC, RSGetRoot(_T("img\\02.Smart Panel\\02_bg_magic_02_popup.png")),0, 0);
	
	//-- Draw Value
	DrawDlgValue(&mDC);
	InvalidateRect(m_rectValue, FALSE);
	
	//-- Draw Menu
	DrawMenu(BTN_MS_LEFT, &mDC);
	DrawMenu(BTN_MS_RIGHT, &mDC);	

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawMenu
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Draw Buttons
//------------------------------------------------------------------------------ 
void CSmartPanelModeSelectChildDlg::DrawMenu(int nItem, CDC* pDC)
{
	int nX, nY, nW, nH;
	nX	= m_rectBtn[nItem].left;	
	nY	= m_rectBtn[nItem].top;
	nW	= m_rectBtn[nItem].Width();
	nH	= m_rectBtn[nItem].Height();

	switch(nItem)
	{
	case BTN_MS_LEFT:
		switch(m_nStatus[nItem])
		{
		case PT_MS_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_focus.png")),		nX, nY,nW, nH);	break;
		case PT_MS_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_push.png")),		nX, nY,nW, nH);	break;
		case PT_MS_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_disable.png")),	nX, nY,nW, nH);	break;
		default	:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_normal.png")),		nX, nY,nW, nH);	break;
		}
		break;
	case BTN_MS_RIGHT:
		switch(m_nStatus[nItem])
		{
		case PT_MS_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_focus.png")),		nX, nY,nW, nH);	break;
		case PT_MS_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_push.png")),		nX, nY,nW, nH);	break;
		case PT_MS_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_disable.png")),	nX, nY,nW, nH);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_normal.png")),	nX, nY,nW, nH);	break;
		}
		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawDlgValue
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelModeSelectChildDlg::DrawDlgValue(CDC* pDC)
{
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_MAGIC_MAGIC_FRAME		:	
		switch(m_pRSModeSelect->GetPropIndex())
		{
		case DSC_MAGIC_FRAME_MAGICTYPECHANGE:		DrawCtrlValue(pDC);			break;
		}
		break;
	case DSC_MODE_MAGIC_SMART_FILTER	:		
		switch(m_pRSModeSelect->GetPropIndex())
		{
		case DSC_SMART_FILTER_MAGICTYPECHANGE:		DrawCtrlValue(pDC);			break;
		}
		break;
	case DSC_MODE_SCENE_BEAUTY		 	:		
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_LANDSCAPE		:		
	case DSC_MODE_SCENE_PORTRAIT		:		
	case DSC_MODE_SCENE_CHILDREN		:		
	case DSC_MODE_SCENE_SPORTS		 	:		
	case DSC_MODE_SCENE_CLOSE_UP		:		
	case DSC_MODE_SCENE_TEXT			:		
	case DSC_MODE_SCENE_SUNSET		 	:		
	case DSC_MODE_SCENE_DAWN			:		
	case DSC_MODE_SCENE_BACKLIGHT		:		
	case DSC_MODE_SCENE_FIREWORK		:		
	case DSC_MODE_SCENE_BEACH_SNOW	 	:		
	case DSC_MODE_SCENE_SOUND_PICTURE	:		
	case DSC_MODE_SCENE_3D			 	:
	// dh9.seo 2013-07-02
	case DSC_MODE_SCENE_BEST			:
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_ACTION			:
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
	case DSC_MODE_SCENE_CREATIVE		:
	case DSC_MODE_SCENE_PANORAMA		:
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
	case DSC_MODE_SCENE_LIGHT_TRACE		:
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
		switch(m_pRSModeSelect->GetPropIndex())
		{
		case DSC_SCENE_BEAUTY_SCENECHANGE:		DrawCtrlValue(pDC);			break;
		}
		break;
	default:		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-13
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelModeSelectChildDlg::DrawCtrlValue(CDC* pDC)
{
	//-- Set Alpha Color Matrix
	ColorMatrix matrix1 = {
		1,0,0,0,0,
		0,1,0,0,0,
		0,0,1,0,0,
		0,0,0,0.7f,0,
		0,0,0,0.0,1,
	};		
	ColorMatrix matrix2 = {
		1,0,0,0,0,
		0,1,0,0,0,
		0,0,1,0,0,
		0,0,0,0.2f,0,
		0,0,0,0.0,1,
	};

	ImageAttributes   IA1;   
	IA1.SetColorMatrix(&matrix1,ColorMatrixFlagsDefault,ColorAdjustTypeBitmap);

	ImageAttributes   IA2;   
	IA2.SetColorMatrix(&matrix2,ColorMatrixFlagsDefault,ColorAdjustTypeBitmap);

	//-- Set Rect
	Rect		DesRect;
	DesRect.X = 40;
	DesRect.Y = 12;
	DesRect.Width = 30;
	DesRect.Height = 23;

	Graphics gc(pDC->GetSafeHdc());

	//-- Value 1
	Image ImgValue1(m_pRSModeSelect->GetPropStrValue(MODE_POS_FIRST));
	gc.DrawImage(&ImgValue1, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA2);

	//-- Value 2
	Image ImgValue2(m_pRSModeSelect->GetPropStrValue(MODE_POS_SECOND));
	DesRect.X += 42;	
	gc.DrawImage(&ImgValue2, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);	
	
	//-- Value 3
	Image ImgValue3(m_pRSModeSelect->GetPropStrValue(MODE_POS_THIRD));
	DesRect.X += 42;
	gc.DrawImage(&ImgValue3, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);	

	//-- Value 4
	Image ImgValue4(m_pRSModeSelect->GetPropStrValue(MODE_POS_FOURTH));
	DesRect.X += 42;
	gc.DrawImage(&ImgValue4, DesRect);	

	//-- Value 5
	Image ImgValue5(m_pRSModeSelect->GetPropStrValue(MODE_POS_FIFTH));
	DesRect.X += 42;
	gc.DrawImage(&ImgValue5, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);	

	//-- Value 6
	Image ImgValue6(m_pRSModeSelect->GetPropStrValue(MODE_POS_SIXTH));
	DesRect.X += 42;
	gc.DrawImage(&ImgValue6, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);	

	//-- Value 7
	Image ImgValue7(m_pRSModeSelect->GetPropStrValue(MODE_POS_SEVENTH));
	DesRect.X += 42;
	gc.DrawImage(&ImgValue7, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA2);	
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelModeSelectChildDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	m_nMouseIn = PtInRect(point, TRUE);
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

void CSmartPanelModeSelectChildDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CSmartPanelModeSelectChildDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_MS_CNT; i ++)
		ChangeStatus(i,PT_MS_NOR);

	CPoint point;
	GetCursorPos(&point);
	ScreenToClient(&point);

	CRect rect;
	GetClientRect(&rect);

	if(!rect.PtInRect(point)) 
		this->ShowWindow(SW_HIDE);

	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelModeSelectChildDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	
	
	switch(nItem)
	{
	case BTN_MS_LEFT			:	m_pRSModeSelect->SetPos(FALSE);				break;	
	case BTN_MS_RIGHT			:	m_pRSModeSelect->SetPos(TRUE);				break;	
	default:						SetPropValueIndex(PtInRectValue(point));	break;	
	}

	//-- 2011-8-2 Lee JungTaek
	Invalidate(FALSE);

	CDialog::OnLButtonDown(nFlags, point);	
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelModeSelectChildDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ChangeStatusAll();	
	CDialog::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CSmartPanelModeSelectChildDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = PT_MS_IN_NOTHING;
	int nStart = 0;

	for(int i = nStart ; i < BTN_MS_CNT; i ++)
	{
		if(m_rectBtn[i].PtInRect(pt))
		{
			nReturn = i;
			if(bMove)
				ChangeStatus(i,PT_MS_INSIDE);
			else
				ChangeStatus(i,PT_MS_DOWN); 
		}
		else
			ChangeStatus(i,PT_MS_NOR); 
	}
	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelModeSelectChildDlg::ChangeStatus(int nItem, int nStatus)
{
	if(m_nStatus[nItem] != nStatus)
	{
		m_nStatus[nItem] = nStatus;
		CPaintDC dc(this);
		DrawMenu(nItem, &dc);
		InvalidateRect(m_rectBtn[nItem],FALSE);
	}
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelModeSelectChildDlg::ChangeStatusAll()
{
	int nStart = 0;
	for(int i = nStart; i < BTN_MS_CNT; i ++)
	{
		if(m_nMouseIn == i)
			ChangeStatus(i,PT_MS_INSIDE); 
		else
			ChangeStatus(i,PT_MS_NOR); 
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRectValue
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CSmartPanelModeSelectChildDlg::PtInRectValue(CPoint point)
{
	for(int i = 0 ; i < MODE_POS_CNT; i ++)
	{
		if(m_rectValeu[i].PtInRect(point))
		{
			if(i != MODE_POS_FOURTH)
				return i;
		}			
	}

	return MODE_POS_NULL;
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropValueIndex
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelModeSelectChildDlg::SetPropValueIndex(int nIndex)
{
	if(nIndex == MODE_POS_NULL)		return;

	if(nIndex > MODE_POS_FOURTH)
	{
		if(m_pRSModeSelect->GetPropIndexValue() + 1 == m_pRSModeSelect->GetPropListCnt())
			return;

		//-- Just Change
		for(int i = MODE_POS_FOURTH; i < nIndex - 1; i++)
			m_pRSModeSelect->SetPos(TRUE, FALSE);
		//-- Set Real
		m_pRSModeSelect->SetPos(TRUE);
	}
	else
	{
		if(m_pRSModeSelect->GetPropIndexValue() == 0)
			return;

		//-- Just Change
		for(int i = MODE_POS_FOURTH; i > nIndex + 1; i--)
			m_pRSModeSelect->SetPos(FALSE, FALSE);
		//-- Set Real
		m_pRSModeSelect->SetPos(FALSE);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
BOOL CSmartPanelModeSelectChildDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_MOUSEWHEEL)
	{
		BOOL bMouseWheelUp;
		int delta = GET_WHEEL_DELTA_WPARAM(pMsg->wParam);

		if(delta < 0)		bMouseWheelUp = FALSE;
		else				bMouseWheelUp = TRUE;

		m_pRSModeSelect->SetPos(bMouseWheelUp);
		Invalidate(FALSE);
		
		return TRUE;
	}		

	return CBkDialogST::PreTranslateMessage(pMsg);
}