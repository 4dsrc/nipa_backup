#pragma once
/////////////////////////////////////////////////////////////////////////////
//
//  CPopupMsgDlg.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-07-01
//
/////////////////////////////////////////////////////////////////////////////

// CPopupMsgDlg dialog
#include "BKDialogST.h"
#include "NXRemoteStudioFunc.h"

enum
{
	POPUP_TYPE_NULL = -1,
	POPUP_TYPE_ONLY_MESSAGE,
	POPUP_TYPE_WITH_2BTN,
};

enum
{
	POPUP_DELAY_NONE = 0,
	POPUP_DELAY_1SEC	,
	POPUP_DELAY_3SEC	,
	POPUP_DELAY_5SEC	,
};

enum
{
	PT_POPUP_IN_NOTHING = -1,
	PT_POPUP_NOR,
	PT_POPUP_INSIDE,
	PT_POPUP_DOWN,
};

enum
{
	BTN_POPUP_NULL = -1,
	BTN_POPUP_YES,
	BTN_POPUP_NO,
	BTN_POPUP_CNT,
};

class CPopupMsgDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CPopupMsgDlg)

public:
	CPopupMsgDlg(int nType = POPUP_TYPE_NULL, CString strMsg = _T(""), int nDelay = POPUP_DELAY_NONE, CWnd* pParent = NULL);   // standard constructor
	virtual ~CPopupMsgDlg();

	// Dialog Data
	enum { IDD = IDD_DLG_POPUP_MSG_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
// 	virtual BOOL PreTranslateMessage(MSG* pMsg);
 	afx_msg void OnPaint();	
	afx_msg void OnTimer(UINT_PTR nIDEvent);
 	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
 	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
 	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
 	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	//-- Item Info	
	int		m_nIndex;
	int		m_nType;
	int		m_nMouseIn;
	int		m_nStatus[BTN_POPUP_CNT];
	CRect	m_rect[BTN_POPUP_CNT];
	CRect	m_rectMsg;

	CString m_strMsg;
	int m_nDelay;
	RSEvent *m_pMsg;

	//-- Draw Item
	void DrawBackground();	
	void CleanDC(CDC* pDC);

	void DrawOnlyMessage(CDC* pDC);	
	void DrawWith2Btn(CDC* pDC);		
	void DrawBtn(CDC* pDC, int nItem);

	void SetItemPosition();

	//-- Control Mouse Event
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	void ChangeStatus(int nItem, int nStatus);
	void ChangeStatusAll();
	void MouseEvent();
public:
	//-- Action
	void SetCommandMsg(RSEvent* pMsg) {m_pMsg = pMsg;}
};
