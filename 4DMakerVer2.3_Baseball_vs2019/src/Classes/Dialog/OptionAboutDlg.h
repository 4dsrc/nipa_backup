/////////////////////////////////////////////////////////////////////////////
//
//  OptionAboutDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "OptionBaseDlg.h"
#include "NXRemoteStudioFunc.h"

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
#define SETDIALOG_ITEM_VIEW_SIZE 5

// COptionAboutDlg dialog
class COptionAboutDlg : public COptionBaseDlg
{
	DECLARE_DYNAMIC(COptionAboutDlg)	
public:
	COptionAboutDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionAboutDlg();

	BOOL GetFileVersion(CString strFullPath, WORD& wMajor1, WORD& wMajor2, WORD& wMinor1, WORD& wMinor2);
// Dialog Data
	enum { IDD = IDD_DLG_OPT_ABOUT };

	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	CStatic * m_staticLabel[SETDIALOG_ITEM_VIEW_SIZE];
	CButton  m_PushBtn;
	CButton  PushBtn;
protected:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	afx_msg void OnUpdate();	
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
	DECLARE_MESSAGE_MAP()

private:	
	int m_nStatus;
	BOOL m_bUpdate;
	CRect m_rectBtn;

	CString m_strLocalVersion;
	CString m_strLatestVersion	;
	CString m_strDownloadURL;
	CString m_strDownloadFile;

	//-- 2011-05-30 hongsu.jung6
	void CreateFrames();	
	void UpdateFrames();	
	void LoadVersion();
	void DownloadFile();
	BOOL PtInRect(CPoint pt, BOOL bMove);	
	
};