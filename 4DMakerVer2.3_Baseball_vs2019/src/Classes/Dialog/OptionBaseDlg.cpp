/////////////////////////////////////////////////////////////////////////////
//
//  COptionBaseDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "OptionBaseDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// COptionBaseDlg dialog
IMPLEMENT_DYNAMIC(COptionBaseDlg, CDialogEx)

//------------------------------------------------------------------------------ 
//! @brief		COptionBaseDlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
COptionBaseDlg::COptionBaseDlg(CWnd* pParent ): CRSChildDialog(COptionBaseDlg::IDD, pParent)				{}
COptionBaseDlg::COptionBaseDlg(UINT uResourceID, CWnd* pParent) : CRSChildDialog(uResourceID, pParent)	{}

COptionBaseDlg::~COptionBaseDlg() {}
void COptionBaseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);		
}				

BEGIN_MESSAGE_MAP(COptionBaseDlg, CDialogEx)
	ON_WM_PAINT()	
END_MESSAGE_MAP()

//BOOL COptionBaseDlg::OnEraseBkgnd(CDC* pDC) {return FALSE;}
void COptionBaseDlg::OnPaint()
{	
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	//UpdateFrames();

	CDialogEx::OnPaint();
}

BOOL COptionBaseDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	//this->SetBackgroundColor(RGB(0,0,0));
	//-- 2011-05-30
	CreateFrames();
	//-- 2011-05-18 hongsu.jung
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	//UpdateFrames();
	return TRUE; 	
}

//------------------------------------------------------------------------------ 
//! @brief		MouseEvent
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionBaseDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}
