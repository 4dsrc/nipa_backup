/////////////////////////////////////////////////////////////////////////////
//
//  OptionDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "OptionDlg.h"
#include "SRSIndex.h"
//-- For Magnetic Windows
#include "NXRemoteStudioDlg.h"
#include "NXRemoteStudioFunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// COptionDlg dialog
IMPLEMENT_DYNAMIC(COptionDlg, CDialogEx)

//------------------------------------------------------------------------------ 
//! @brief		COptionDlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
COptionDlg::COptionDlg(CWnd* pParent /*=NULL*/)
	: CRSChildDialog(COptionDlg::IDD, pParent)
{
	SetDragMove(TRUE);
	m_pOptionTab	= NULL;
	m_pOptionBody	= NULL;
	m_bCloseState = FALSE;
}

COptionDlg::~COptionDlg() 
{
	if(m_pOptionTab		)	{
		delete m_pOptionTab;
		m_pOptionTab	= NULL;
	}	
	if(m_pOptionBody)	{
		delete m_pOptionBody;
		m_pOptionBody = NULL;
	}	
}
void COptionDlg::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);	
//	DDX_Control(pDX,  IDR_IMG_CANCEL_DIASBLE		,m_btnCancel		);
}				

BEGIN_MESSAGE_MAP(COptionDlg, CRSChildDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()		
	ON_WM_ERASEBKGND()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDR_IMG_CANCEL_DIASBLE, OnBnClickedBtnCancel)	
	ON_MESSAGE(TAB_CHANGE,OnOptionTabMsg)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

// CLiveview message handlers
BOOL COptionDlg::OnEraseBkgnd(CDC* pDC)
{	
	//CMiLRe 20141127 최소화 후에 복구 하면 탭창이 안나오는 버그 수정
	m_pOptionTab->m_bFirstLoad = TRUE;
	return FALSE;
}
void COptionDlg::OnPaint()
{	
	UpdateFrames();
	CDialogEx::OnPaint();
}

BOOL COptionDlg::OnInitDialog()
{
	//-- always on the Top
	SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	//-- 2011-07-14 hongsu.jung
	//-- Enable Tooltip
	EnableToolTips(TRUE);

	CRSChildDialog::OnInitDialog();
	
	//-- 2011-6-16 Lee JungTaek
	LoadOptionInfo();	
	//-- 2011-05-30
	CreateFrames();
	//-- 2011-05-18 hongsu.jung
	UpdateFrames();

	return TRUE; 	
}

void COptionDlg::OnSize(UINT nType, int cx, int cy)
{
	CRSChildDialog::OnSize(nType, cx, cy);
	//-- 2011-05-18 hongsu.jung
 	UpdateFrames();
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionDlg::UpdateFrames()
{
	//-- Draw Back Ground
	DrawBackGround();
	//-- Draw Dialogs
//	DrawDialogs();
}

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 

static const int size_title_w		= 7;
static const int size_title_h		= 23;
static const int size_body_w	= 7;

void COptionDlg::CreateFrames()
{
	m_RectClose = CRect(420, 4 , 420+17, 4+17);
	//-- Skin Button
//	m_btnCancel		.LoadBitmaps(IDR_IMG_CANCEL_DIASBLE		,NULL,NULL,NULL);
	//-- Tab
	if(!m_pOptionTab)
	{
		m_pOptionTab = new COptionTabDlg();
		m_pOptionTab->Create(COptionTabDlg::IDD,this);		
		m_pOptionTab->MoveWindow(size_title_w,size_title_h,OPTION_DLG_WIDTH,OPTION_TAB_HEIGHT);		
	}
	//-- Body
	if(!m_pOptionBody)
	{
		m_pOptionBody = new COptionBodyDlg(&m_RSOption);
		m_pOptionBody->Create(COptionBodyDlg::IDD,this);		
		m_pOptionBody->MoveWindow(size_body_w,OPTION_TAB_HEIGHT+size_title_h,OPTION_DLG_WIDTH - 2*size_body_w, OPTION_DLG_HEIGHT - (OPTION_TAB_HEIGHT+size_title_h+size_body_w));		
	}
	//-- Set Default Activate
	m_pOptionBody->SetActivate(OPT_TAB_TIMELAPSE, OPT_TAB_TIMELAPSE);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackGround
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionDlg::DrawBackGround()
{
	CPaintDC dc(this);

	CRect rect;
	GetClientRect(&rect);

	//int nImgX, nImgY, nImgW, nImgH;
	CString strPath = RSGetRoot();
	CString strFullPath;

	//-- 1. Body	
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_optionpopup_bg.png"),strPath);
	//DrawImageBase(&dc, strFullPath, 0,0);
	DrawGraphicsImage(&dc, strFullPath, 0, 0, OPTION_DLG_WIDTH, OPTION_DLG_HEIGHT);
	//-- 2011-05-31 hongsu.jung
	//	DrawDlgTitle(&dc, _T("Option Dialog"), RGB(3,3,3));
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Option Dialog"), 5, 5, RGB(45,45,45));

	if(m_bCloseState)
		DrawGraphicsImage(&dc, RSGetRoot(_T("img\\00.Common\\00_window_title_right_close_mouseon.png"))		,m_RectClose.left, m_RectClose.top, m_RectClose.Width(),m_RectClose.Height()); 
	else
		DrawGraphicsImage(&dc, RSGetRoot(_T("img\\00.Common\\00_window_title_right_close_mouseover.png"))		,m_RectClose.left, m_RectClose.top, m_RectClose.Width(), m_RectClose.Height());

	InvalidateRect(m_RectClose, FALSE);
}
//------------------------------------------------------------------------------ 
//! @brief		DrawDialogs
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------
static const int size_btn_size			= 16;
static const int btn_top_margin			= 4;
static const int btn_right_margin		= 7;
static const int liveview_top_margin	= 4;
static const int thumbnail_height		= 125;
static const int toolbar_height			= 58;
static const int multiview_width		= 155;
static const int resize_frame			= 15;

void COptionDlg::DrawDialogs()
{	
//	if(!m_btnCancel)
//		return;

	//-- Top Title.
	CRect rectDialog, rectBtn, rectLiveview, rectToolbar, rectThumbnail, rectMultiview;
	GetClientRect(&rectDialog);

	{	//-- 1. Button
		rectBtn.top		= btn_top_margin;
		rectBtn.bottom	= btn_top_margin + size_btn_size;
		rectBtn.left		= rectDialog.Width() - size_btn_size - btn_right_margin;
		rectBtn.right		= rectDialog.Width() - btn_right_margin;	
//		m_btnCancel		.MoveWindow(rectBtn);
	}

	Invalidate(false); 
}

void COptionDlg::OnBnClickedBtnCancel()		{ SendMessage(WM_CLOSE);	}

void COptionDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = OPTION_DLG_WIDTH;
	lpMMI->ptMaxTrackSize.x = OPTION_DLG_WIDTH;
	lpMMI->ptMinTrackSize.y = OPTION_DLG_HEIGHT;
	lpMMI->ptMaxTrackSize.y = OPTION_DLG_HEIGHT;

	RECT rcWorkArea;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcWorkArea, SPIF_SENDCHANGE);
	lpMMI->ptMaxSize.x = OPTION_DLG_WIDTH;
	lpMMI->ptMaxSize.y = OPTION_DLG_HEIGHT;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}

//------------------------------------------------------------------------------ 
//! @brief		OnOptionTabMsg
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT COptionDlg::OnOptionTabMsg(WPARAM w, LPARAM l)
{
	int nBeforeTab = (int)w;
	int nNewSelectTab = (int)l;
	m_pOptionBody->SetActivate(nBeforeTab, nNewSelectTab);
	return 0L;
}

//------------------------------------------------------------------------------ 
//! @brief		LoadOptionInfo
//! @date		2011-6-16
//! @author		Lee JungTaek
//! @note	 	Load Option Info
//------------------------------------------------------------------------------ 
void COptionDlg::LoadOptionInfo()
{
	//-- Load Option Property
	CString strOptionPath = _T("");
	strOptionPath.Format(_T("%s\\RSOption.txt"), STR_CONFIG_PATH);

	m_RSOption.Load(RSGetRoot(strOptionPath));
}

void COptionDlg::SetMode(int nMode)
{
	m_nMode = nMode;
	m_pOptionBody->SetMode(m_nMode);
}

//dh0.seo
void COptionDlg::OnMouseMove(UINT nFlags, CPoint point)
{	
	if(m_RectClose.PtInRect(point))
	{
		m_bCloseState = TRUE;
		//Invalidate(FALSE);
		InvalidateRect(m_RectClose,FALSE);
		m_pOptionTab->m_bFirstLoad = TRUE;
	}
	else if(!m_RectClose.PtInRect(point) && m_bCloseState == TRUE)
	{
		m_bCloseState = FALSE;
		//Invalidate(FALSE);
		InvalidateRect(m_RectClose,FALSE);
		m_pOptionTab->m_bFirstLoad = TRUE;
	}

	CDialog::OnMouseMove(nFlags, point);
}

void COptionDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	if(m_RectClose.PtInRect(point))
	{
		m_bCloseState = FALSE;
		SendMessage(WM_CLOSE);
	}

	CRSChildDialog::OnLButtonDown(nFlags, point);
}