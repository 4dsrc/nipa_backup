/////////////////////////////////////////////////////////////////////////////
//
//  CommandDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudioDlg.h"
#include "CommandDlg.h"
#include "SRSIndex.h"
#include "OptionDlg.h"
#include "SdiDefines.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//CMiLRe 20141119 Sleep Mode 에서 캡쳐
static const int timer_button_S2_pressed		= WM_USER + 0x0001;
static const int timer_button_FOCUS		= WM_USER + 0x0002;
static const int timer_button_SEND_WAIT_DLG = WM_USER + 0x0003;
static const int timer_recording = WM_USER + 0x0004;
static const int timer_enable = WM_USER + 0x0005;
static const int timer_recording_start = WM_USER + 0x000;

// CCommandDlg dialog
IMPLEMENT_DYNAMIC(CCommandDlg,  CRSChildDialog)
CCommandDlg::CCommandDlg(CWnd* pParent /*=NULL*/)
	: CRSChildDialog(CCommandDlg::IDD, pParent)
{	
	m_nMouseIn = BTN_CMD_NULL;
	m_bMF		= FALSE;
	m_bConnection	= FALSE;
	for(int i=0; i<BTN_CMD_CNT; i++)
		m_nStatus[i] = BTN_NORMAL;
	SetShutter(SHUTTER_NULL);
	//-- 2011-8-8 Lee JungTaek
	m_bAFDetect = FALSE;
	m_bTimerFinish = FALSE;
	m_bTimerStart = FALSE;
	m_bEnable = TRUE;
	m_bRecord =FALSE;

	m_tFocusPosition.max		= 100;
	m_tFocusPosition.min		= 0;
	m_tFocusPosition.current	= 50;
	//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
	m_bFocusHandelPress = FALSE;
	//CMiLRe 20141119 Sleep Mode 에서 캡쳐
	m_bSetTimerFocus = FALSE;
	m_bSetTimerCapture = FALSE;
	m_nRecordTime = 0;
	m_nCameraMode1 = 0;
	m_bRecordStatus = FALSE;
	m_nCaptureCount = 0;
}

CCommandDlg::~CCommandDlg() 
{
	KillTimer(timer_recording);
}
void CCommandDlg::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);		
}

BEGIN_MESSAGE_MAP(CCommandDlg, CRSChildDialog)	
	ON_WM_PAINT()	
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()	
	ON_WM_MOUSEMOVE()
	//CMiLRe 20141119 Sleep Mode 에서 캡쳐
	ON_WM_TIMER()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)	
END_MESSAGE_MAP()

void CCommandDlg::OnPaint()
{	
	DrawBackground();
	CRSChildDialog::OnPaint();
}

// CCommandDlg message handlers
BOOL CCommandDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();
	//-- Draw Liveview, Option String		
	
	//-- Set Position
	m_rect[BTN_CMD_SHUT]		= CRect(CPoint(239, 40), CSize(53,53));
	m_rect[BTN_CMD_LIVE]		= CRect(CPoint(200,  5), CSize(89,25));	
	m_rect[BTN_CMD_OPT]			= CRect(CPoint(107,  5), CSize(89,25));	
//	m_rect[BTN_CMD_MULTI]		= CRect(CPoint( 13,  5), CSize(89,25));	
	
	m_rectAFMFBtn				= CRect(183, 39 , 183+26, 39+13);	
	m_rect[BTN_CMD_AFMF]		= CRect(146, 36 , 146+85, 36+21);
	m_rect[BTN_MF_AF]			= CRect(130, 36 , 130+100, 36+25);

	m_rectFocusBK				= CRect(  8,  61, 8+207, 61+47);
	m_rect[BTN_CMD_REW]			= CRect( 13, 66,  13+21, 66+20);
	m_rect[BTN_CMD_PREV]		= CRect( 38, 66,  38+21, 66+20);
	m_rect[BTN_MF_NEAR]			= CRect( 63, 66,  63+21, 66+21);
	m_rect[BTN_MF_BAR]			= CRect( 13, 93,  38+171, 93+7);
	m_rect[BTN_MF_BAR_HANDLE]	= CRect( 63, 90,  63+13, 90+13);
	m_rect[BTN_MF_FAR]			= CRect(137, 66,  137+21, 66+21);
	m_rect[BTN_CMD_NEXT]		= CRect(163, 66, 163+21, 66+20);
	m_rect[BTN_CMD_FF]			= CRect(188, 66, 188+21, 66+20);
	m_rect[BTN_CMD_RECORD]		= CRect(220, 80, 220+26, 80+26);

	m_rect[BTN_RECORD_START]	= CRect(30, 36, 30+60, 37+20);
	m_rect[BTN_RECORD_STOP]		= CRect(30, 36, 30+60, 37+20);
	
	m_bRecStartStop = FALSE;

	SetTimer(timer_recording, 1000, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CCommandDlg::DrawBackground()
{
	CString strCaptureCount;

	if(!ISSet())
		return;
	CPaintDC dc(this);

	CRect rect;
	GetClientRect(&rect);

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	DrawImageBase(&mDC, RSGetRoot(_T("img\\01.Light Studio\\command.png")),0, 0);

	//-- 2011-06-08 hongsu.jung
	//-- AF/MF	
	if(((CNXRemoteStudioDlg*)GetParent())->m_nFocusCount >3)
	{
		if(m_bMF)		DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_afmf_lever_bg01.png"))	, m_rect[BTN_CMD_AFMF].left, m_rect[BTN_CMD_AFMF].top, m_rect[BTN_CMD_AFMF].Width(), m_rect[BTN_CMD_AFMF].Height());
		else			DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_afmf_lever_bg02.png"))	, m_rect[BTN_CMD_AFMF].left, m_rect[BTN_CMD_AFMF].top, m_rect[BTN_CMD_AFMF].Width(), m_rect[BTN_CMD_AFMF].Height());
	}
	else
	{
		if(m_bMF)		DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_afmf_lever_bg01_MF.png"))	, m_rect[BTN_MF_AF].left - 15, m_rect[BTN_MF_AF].top + 5, m_rect[BTN_MF_AF].Width(), m_rect[BTN_MF_AF].Height());
		else			DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_afmf_lever_bg01_AF.png"))	, m_rect[BTN_MF_AF].left - 15, m_rect[BTN_MF_AF].top + 5, m_rect[BTN_MF_AF].Width(), m_rect[BTN_MF_AF].Height());
	}
	//-- Focus Background
	DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_bg.png"))				, m_rectFocusBK.left, m_rectFocusBK.top, m_rectFocusBK.Width(), m_rectFocusBK.Height());
	DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_shutter_record.png"))	, m_rect[BTN_CMD_RECORD].left, m_rect[BTN_CMD_RECORD].top, m_rect[BTN_CMD_RECORD].Width(), m_rect[BTN_CMD_RECORD].Height());
	
	//-- Draw Buttons
	DrawItem(&mDC, BTN_CMD_AFMF);
	DrawItem(&mDC, BTN_CMD_REW);
	DrawItem(&mDC, BTN_CMD_PREV);
	DrawItem(&mDC, BTN_CMD_NEXT);
	DrawItem(&mDC, BTN_CMD_FF);
	DrawItem(&mDC, BTN_CMD_SHUT);
	DrawItem(&mDC, BTN_CMD_LIVE);
	DrawItem(&mDC, BTN_CMD_OPT);	
//	DrawItem(&mDC, BTN_CMD_MULTI);
	DrawItem(&mDC, BTN_CMD_RECORD);
	DrawItem(&mDC, BTN_MF_NEAR);
	DrawItem(&mDC, BTN_MF_BAR);
	DrawItem(&mDC, BTN_MF_BAR_HANDLE);
	DrawItem(&mDC, BTN_MF_FAR);
	DrawItem(&mDC, BTN_RECORD_START);
	DrawItem(&mDC, BTN_RECORD_STOP);
	
	COLORREF color;
	if(m_bMF)		color = RGB(255,255,255);
	else			color = RGB(60,60,60);
//	DrawStr(&mDC, STR_SIZE_MIDDLE, _T("Focus"),m_rectFocusBK, color);

	//if(m_nRecordTime%2 == 1)
	{
		
		if(m_bRecordStatus)
		{
			if(m_nRecordTime == 1)
			{
				m_nMin = 0;
			}
			CString strRecTime;
			if(m_nSec >= 60)
			{
				m_nMin++;
				m_nSec = 0;
			}
			strRecTime.Format(_T("%.2d:%.2d"), m_nMin, m_nSec);
				
			DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_shutter_record.png")),30, 13, 10, 10);
			DrawStr(&mDC, STR_SIZE_MIDDLE, strRecTime, 40,  12, color = RGB(255,0,0));
		}
		else
		{
			m_nMin = 0;
			m_nSec = 0;
			m_nRecordTime = 0;
			DrawStr(&mDC, STR_SIZE_MIDDLE, _T(""),  13,  5, color = RGB(219,219,219));
		}

/*		strCaptureCount.Format(_T("%d"), m_nCaptureCount);
		if(m_nCaptureCount < 0)
			DrawStr(&mDC, STR_SIZE_MIDDLE, _T("no card"),  250,  93, color = RGB(255,255,255));
		else
			DrawStr(&mDC, STR_SIZE_MIDDLE, strCaptureCount,  267,  93, color = RGB(255,255,255)); */
	}
	

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
	//Invalidate(FALSE);	
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
BOOL g_bTimer = FALSE;
void CCommandDlg::DrawItem(CDC* pDC, int nItem)
{
	int nLeft = 0;	
	COLORREF color;

	switch(nItem)
	{
	case BTN_CMD_AFMF:
		if(IsMF())	nLeft = m_rectAFMFBtn.left;			
		else		nLeft = m_rectAFMFBtn.left - 18;	
		//if(m_nStatus[nItem] == BTN_INSIDE)		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_afmf_lever_focus.png")), nLeft, m_rectAFMFBtn.top, m_rectAFMFBtn.Width(), m_rectAFMFBtn.Height());
		//else											DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_afmf_lever.png"))	, nLeft, m_rectAFMFBtn.top, m_rectAFMFBtn.Width(), m_rectAFMFBtn.Height());
		//-- 2011-9-14 Lee JungTaek
		if(((CNXRemoteStudioDlg*)GetParent())->m_nFocusCount >3)
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_afmf_lever.png"))	, nLeft, m_rectAFMFBtn.top, m_rectAFMFBtn.Width(), m_rectAFMFBtn.Height());
//		else
//			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_afmf_lever_push.png"))	, nLeft, m_rectAFMFBtn.top, m_rectAFMFBtn.Width(), m_rectAFMFBtn.Height());
		break;
	case BTN_CMD_REW:
		if(!IsMF())
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_rew_disable.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		else
		{
			if(m_nStatus[nItem] == BTN_INSIDE)			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_rew_focus.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
			else if(m_nStatus[nItem] == BTN_DOWN)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_rew_push.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
			else												DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_rew_normal.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		}
		break;															
	case BTN_CMD_PREV:
		if(!IsMF())
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_prev_disable.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		else
		{
			if(m_nStatus[nItem] == BTN_INSIDE)			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_prev_focus.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
			else if(m_nStatus[nItem] == BTN_DOWN)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_prev_push.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
			else												DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_prev_normal.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		}
		break;	 
	case BTN_CMD_NEXT:
		if(!IsMF())
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_next_disable.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		else
		{
			if(m_nStatus[nItem] == BTN_INSIDE)			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_next_focus.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
			else if(m_nStatus[nItem] == BTN_DOWN)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_next_push.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
			else												DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_next_normal.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());
		}
		break;	 
	case BTN_CMD_FF:
		if(!IsMF())
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_ff_disable.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		else
		{
			if(m_nStatus[nItem] == BTN_INSIDE)			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_ff_focus.png"))			,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
			else if(m_nStatus[nItem] == BTN_DOWN)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_ff_push.png"))			,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
			else												DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_focus_ff_normal.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		}
		break;	 
	case BTN_CMD_SHUT:
		if(m_nStatus[nItem] == BTN_INSIDE)		
		{
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_shutter_focus.png"))			,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());
			m_nCameraMode1 =  GetCurMode();
		}
		else if(m_nStatus[nItem] == BTN_DOWN)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_shutter_push.png"))			,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		else													DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_shutter_normal.png"))			,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); break;			
	case BTN_CMD_RECORD:
//		if(!m_bRecStartStop)
//		{
			if(m_nStatus[nItem] == BTN_INSIDE)		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_shutter_record_focus.png"))			,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
			else if(m_nStatus[nItem] == BTN_DOWN)
			{
				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_shutter_record_push.png"))			,m_rect[nItem].left, m_rect[nItem].top, m_rect[nItem].Width(), m_rect[nItem].Height()); 
				//CMiLRe 20141127 Display Save Mode 에서 Wake up
				//if(!m_bRecord /*&& (GetDPSleepModeStatus() == FALSE)*/)
				/*{
					if(!g_bTimer) 
					{
						SetTimer(timer_recording, 1000, NULL);
						g_bTimer = TRUE;
					}
				}
				else
				{
					m_nRecordTime = 0;
					g_bTimer = FALSE;
					KillTimer(timer_recording);
					DrawStr(pDC, STR_SIZE_MIDDLE, _T(""),  13,  5, color = RGB(219,219,219));
					Invalidate(FALSE);
				}*/
			}
			else									DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_shutter_record_normal.png"))			,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); break;
//		}
//		else
//		{
//			if(m_nStatus[nItem] == BTN_INSIDE)		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_shutter_record_focus.png"))			,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
//			else if(m_nStatus[nItem] == BTN_DOWN)	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_shutter_record_push.png"))			,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
//			else									DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_shutter_record.png"))			,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); break;
//		}
		break;
	case BTN_CMD_LIVE:
		if(m_nStatus[nItem] == BTN_INSIDE)		{ DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_option_btn_focus.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());		 color = RGB(255,255,255); }	
		else if(m_nStatus[nItem] == BTN_DOWN)	{ DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_option_btn_push.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());		 color = RGB(255,255,255); }
		else													{ DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_option_btn_normal.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());  color = RGB(219,219,219); }
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Liveview"),m_rect[BTN_CMD_LIVE].left+20, m_rect[BTN_CMD_LIVE].top+5,color);	
		Invalidate(FALSE);
		break;		
	case BTN_CMD_OPT:
		if(m_bRecordStatus)
		{
			{ DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_option_btn_normal.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); color = RGB(219,219,219); }
		}
		else
		{
			if(m_nStatus[nItem] == BTN_INSIDE)		{ DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_option_btn_focus.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());		color = RGB(255,255,255); }	
			else if(m_nStatus[nItem] == BTN_DOWN)	{ DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_option_btn_push.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());		color = RGB(255,255,255); }
			else													{ DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_option_btn_normal.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); color = RGB(219,219,219); }
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Option"),m_rect[BTN_CMD_OPT].left+25, m_rect[BTN_CMD_OPT].top+5,color);
		Invalidate(FALSE);
		break;
	case BTN_CMD_MULTI:
		{
			//-- 2011-07-26 hongsu.jung
			//-- Not Connected
			CString strDSC = GetSelectDSCTitle();
			m_bConnection = TRUE;
			if(strDSC == STR_NOT_CONNECTED)			
			{ 
				m_bConnection = FALSE; 
			}
			//dh0.seo 2014-11-13 주석
/*			if(strDSC == STR_NOT_CONNECTED)			{ pDC->FillRect(m_rect[nItem], &CBrush(RGB(56,60,66))); DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_option_btn_disable.png"))	,m_rect[nItem]);	color = RGB(119,119,119); strDSC = _T("Camera"); m_bConnection = FALSE; }
			else if(m_nStatus[nItem] == BTN_INSIDE)	{ DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_option_btn_focus.png"))		,m_rect[nItem]);	color = RGB(255,255,255); }	
			else if(m_nStatus[nItem] == BTN_DOWN)	{ DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_option_btn_push.png"))		,m_rect[nItem]);	color = RGB(255,255,255); }
			else												{ DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_lightstudio_option_btn_normal.png"))	,m_rect[nItem]);	color = RGB(219,219,219); }				
			DrawStr(pDC, STR_SIZE_MIDDLE, strDSC, m_rect[nItem], color); */
			break;		
		}
	case BTN_MF_NEAR:
		if(!IsMF())
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\03_MF_near_disable.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		else
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\03_MF_near.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		break;
	case BTN_MF_FAR:
		if(!IsMF())
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\03_MF_far_disable.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		else
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\03_MF_far.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		break;
	case BTN_MF_BAR:
		if(!IsMF())
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\03_focus_progress_bg_disable.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		else
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\03_focus_progress_bg_nor.png"))		,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		break;
	//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
	case BTN_CMD_MF_CHANGE:
		{			
			int nDivision = (m_tFocusPosition.max - m_tFocusPosition.min);
			if(nDivision == 0)
				break;
			//-- Change Point
			int nPercentage = 100 - (100 * (m_tFocusPosition.max - m_tFocusPosition.current) / nDivision);

			if(nPercentage == 1)
				m_rect[BTN_MF_BAR_HANDLE].left	= m_rect[BTN_MF_BAR].right - 10;
			else if(nPercentage == 100 )
				m_rect[BTN_MF_BAR_HANDLE].left	= m_rect[BTN_MF_BAR].left +2;
			else					
				m_rect[BTN_MF_BAR_HANDLE].left	= m_rect[BTN_MF_BAR].left + m_rect[BTN_MF_BAR].Width()/10 + m_rect[BTN_MF_BAR].Width() * (100-nPercentage) / 100 * 0.9 - 9;

			m_rect[BTN_MF_BAR_HANDLE].right	= m_rect[BTN_MF_BAR_HANDLE].left	 + 10;

			if(!IsMF())
				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\03_focus_progress_handle_diable.png"))		,m_rect[BTN_MF_BAR_HANDLE]);
			else
				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\03_focus_progress_handle_nor.png"))		,m_rect[BTN_MF_BAR_HANDLE]);

			Invalidate(FALSE);
		}
		break;

	//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
	case BTN_MF_BAR_HANDLE:
		{
			int nDivision = (m_tFocusPosition.max - m_tFocusPosition.min);
			if(nDivision == 0)
				break;
			//-- Change Point
			int nPercentage = 100-(100 * (m_tFocusPosition.max - m_tFocusPosition.current) / nDivision);

			if(nPercentage == 1)
				m_rect[BTN_MF_BAR_HANDLE].left	= m_rect[BTN_MF_BAR].right-10;
			else if(nPercentage == 100 )
				m_rect[BTN_MF_BAR_HANDLE].left	= m_rect[BTN_MF_BAR].left+2;	
			else						
				m_rect[nItem].left	= m_rect[BTN_MF_BAR].left + m_rect[BTN_MF_BAR].Width()/10 + m_rect[BTN_MF_BAR].Width() * (100-nPercentage) / 100 * 0.9 - 9;
			m_rect[nItem].right	= m_rect[nItem].left	 + 15;

			if(!IsMF())
				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\03_focus_progress_handle_diable.png"))		,m_rect[nItem]);
			else
				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\03_focus_progress_handle_nor.png"))		,m_rect[nItem]);

			Invalidate(FALSE);
		}
		break;
	}
}

//CMiLRe 20141119 Sleep Mode 에서 캡쳐
void CCommandDlg::MouseMoveSet(CPoint point)
{
	if(!m_bRecStartStop)
		{
			if(m_nMouseIn == BTN_CMD_SHUT && GetShutter() == SHUTTER_NULL)	
			{
				//-- 2013-03-01 ignore panorama
				if(DSC_MODE_PANORAMA == GetCurMode() || DSC_MODE_BULB == GetCurMode() || DSC_MODE_SCENE_AUTO_SHUTTER == GetCurMode())
				{
					return;
				}
				GetCaptureOrder(S1_PRESS);
				GetCaptureOrder(S1_PRESS);
			}
			//NX1 dh0.seo 2014-08-28
			else if(m_nMouseIn != BTN_CMD_SHUT && GetShutter()  == S1_PRESS)
			{
				if(DSC_MODE_PANORAMA == GetCurMode() || DSC_MODE_BULB == GetCurMode() || DSC_MODE_SCENE_AUTO_SHUTTER == GetCurMode()) //dh0.seo Bulb 추가 2014-09-23		
				{
					return;
				}
				GetCaptureOrder(S1_RELEASE);
			}
			else if(m_nMouseIn != BTN_CMD_SHUT && GetShutter()  == S2_PRESS)
			{
				GetCaptureOrder(S2_RELEASE);
				GetCaptureOrder(S1_RELEASE);
			}
			//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
			else if((m_nMouseIn == BTN_MF_BAR_HANDLE || m_nMouseIn == BTN_MF_BAR))
			{
				if(m_bFocusHandelPress == TRUE)
				{					
					FocusPosition pos;
					pos.max = m_tFocusPosition.max;
					pos.min =  m_tFocusPosition.min;
					float nDivRangeForcus = (float)( (float)(m_tFocusPosition.max-m_tFocusPosition.min)/m_rect[BTN_MF_BAR].Width());
					pos.current= nDivRangeForcus * (m_rect[BTN_MF_BAR].right - point.x) + pos.min;
					if(m_rect[BTN_MF_BAR].right-10 < point.x)
						pos.current = m_tFocusPosition.min+nDivRangeForcus;		
					if(m_rect[BTN_MF_BAR].left+2 > point.x)
						pos.current = m_tFocusPosition.max-nDivRangeForcus;		
					
					SetFocus(&pos, BTN_CMD_MF_CHANGE);
					SetFocusPositionValue(m_tFocusPosition.current);
				}
			}
		}
}
//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CCommandDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if(m_bEnable)
	{
		//-- Set MouseMove
		m_nMouseIn = PtInRect(point, TRUE);
		//-- 2011-9-14 Lee JungTaek
		//-- Revision : Half Shutter
		
		MouseMoveSet(point);
		//-- MOUSE LEAVE
		MouseEvent();		
	}
	else
	{
		//CMiLRe 20141119 Sleep Mode 에서 캡쳐
		if(m_rect[BTN_CMD_SHUT].PtInRect(point))
		{
			ChangeStatus(BTN_CMD_SHUT, BTN_INSIDE);
		}
		else
		{
			ChangeStatus(BTN_CMD_SHUT, BTN_NORMAL);
		}
	}
	CDialog::OnMouseMove(nFlags, point);
}

void CCommandDlg::MouseEvent()
{
	if(m_bEnable)
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(tme);
		tme.hwndTrack = m_hWnd;
		tme.dwFlags = TME_LEAVE;
		tme.dwHoverTime = 1;
		TrackMouseEvent(&tme);
	}
}

LRESULT CCommandDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	if(m_bEnable)
	{
		for(int i = 0; i < BTN_CMD_CNT; i ++)
			ChangeStatus(i,BTN_NORMAL);

		//-- 2011-06-29 hongsu.jung
		//-- check shutter
		if(GetShutter() == S1_PRESS)
			GetCaptureOrder(S1_RELEASE);
		else if(GetShutter() == S2_PRESS)
		{
			//-- S2 Release Event Send		
			GetCaptureOrder(S2_RELEASE);
			GetCaptureOrder(S1_RELEASE);
		}
		//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
		if(m_bFocusHandelPress)
		{
			FocusPosition pos;
			pos.max = m_tFocusPosition.max;
			pos.min = m_tFocusPosition.min;
			pos.current = m_tFocusPosition.current;
			SetFocus(&pos, BTN_CMD_MF_CHANGE);
			SetFocusPositionValue(m_tFocusPosition.current);
		}
		m_bFocusHandelPress = FALSE;
		
		
		//CMiLRe 20141119 Sleep Mode 에서 캡쳐
		if(m_bSetTimerFocus)
		{
			KillTimer(timer_button_FOCUS);
			m_bSetTimerFocus = FALSE;
		}
		if(m_bSetTimerCapture)
		{
			KillTimer(timer_button_S2_pressed);
			m_bSetTimerCapture = FALSE;
		}
	}
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CCommandDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	RSEvent* pMsg;
	if(m_bEnable)
	{
		//-- 2011-8-18 Lee JungTaek
		((CNXRemoteStudioDlg*)GetParent())->HidePopup();

		int nItem = PtInRect(point, FALSE);	
		switch(nItem)
		{
		case BTN_CMD_LIVE:
			((CNXRemoteStudioDlg*)GetParent())->LiveviewWindowOnOff();
			break;

		case BTN_CMD_SHUT:
			{
				

				//-- 2013-03-01 ignore panorama
				if(DSC_MODE_PANORAMA == GetCurMode() || DSC_MODE_BULB == GetCurMode() || DSC_MODE_SCENE_AUTO_SHUTTER == GetCurMode())	
				{
					return;
				}

				m_nCameraMode2 = GetCurMode();
				if(m_nCameraMode2 != m_nCameraMode1 && !RSGetMFFocus())
				{
					GetCaptureOrder(S1_RELEASE);
					GetCaptureOrder(S1_PRESS);
					GetCaptureOrder(S2_PRESS);
				}
				//-- 2011-06-29 hongsu.jung
				//-- check shutter
				//if(m_bAFDetect || RSGetMFFocus())
				{
					if(GetShutter() == S1_PRESS)	
					{
						//-- S2 Press Event Send
						if(RSGetMFFocus())
						{
							GetCaptureOrder(S1_RELEASE);
							GetCaptureOrder(S1_PRESS);
						}
						GetCaptureOrder(S2_PRESS);
						//-- 2011-8-8 Lee JungTaek
						//-- Check SmartPanel Enable
						SetSmartPanelEnable();
					}
					else if(GetShutter() == SHUTTER_NULL)	
					{
						GetCaptureOrder(S1_PRESS);		
						GetCaptureOrder(S2_PRESS);		
						//-- 2011-8-8 Lee JungTaek
						//-- Check SmartPanel Enable
						SetSmartPanelEnable();
					}

					//RSSetFocus(FALSE);
				}
				//CMiLRe 20141119 Sleep Mode 에서 캡쳐

				/*else if((!m_bAFDetect || RSGetMFFocus()) && GetDPSleepModeStatus())
				{					
					SetTimer(timer_button_FOCUS, 1000, NULL);
					SetTimer(timer_button_S2_pressed, 1000, NULL);
					
				}*/
				//-- 2011-06-29
				//-- OneShout
				//CaptureOnce();	
			}				
			break;
		//NX1 dh0.seo 2014-08-28
		case BTN_CMD_RECORD:
			{
				//dh0.seo 2014-09-24
				if(DSC_MODE_PANORAMA == GetCurMode() || DSC_MODE_BULB == GetCurMode() || DSC_MODE_SCENE_AUTO_SHUTTER == GetCurMode() || DSC_MODE_SCENE_AUTO_SHUTTER == GetCurMode())		
				{
					return;
				}

				//CMiLRe 20141127 Display Save Mode 에서 Wake up
				/*if(GetDPSleepModeStatus())
				{
					RSEvent* pMsgWakeUp		= new RSEvent;
					pMsgWakeUp->message	= WM_RS_DISPLAY_SAVE_MODE_WAKE_UP;
					::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsgWakeUp->message, (LPARAM)pMsgWakeUp);
					return;
				}*/

				if(GetShutter() == SHUTTER_NULL)
				{
					GetCaptureOrder(S3_PRESS);
					//-- 2014-09-24 joonho.kim
					//-- Record Transfer Popup Msg
					if(m_bRecord)
					{
						m_bRecord = FALSE;
						m_bRecStartStop = FALSE;
					}
					else
					{
						m_bRecord = TRUE;
					}
					/*
					RSEvent* pMsg	= new RSEvent;
					pMsg->message	= WM_RECORD_POPUP_DLG;
					pMsg->nParam1	= m_bRecord;	
					::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
					*/

					Sleep(500);
					GetCaptureOrder(S3_RELEASE); //dh0.seo 2014-09-26 
					//LoadPopupMsg(POPUP_TYPE_ONLY_MESSAGE, _T("Saved !"), POPUP_DELAY_1SEC);
					
				}
				else
				{
					SetSmartPanelEnable();
				}
			}
			break;
		//dh0.seo 2014-11-13 주석
/*		case BTN_CMD_MULTI:
			if(m_bConnection )
				((CNXRemoteStudioDlg*)GetParent())->PopupList(CMB_MULTI,point);
			break; */
		case BTN_CMD_AFMF:
			if(!m_bRecordStatus)
			{
				if(m_bMF == FALSE)
				{
					pMsg = new RSEvent;
					pMsg->message	= WM_RS_SET_FOCUS_MODE;
					pMsg->nParam1	= PTP_CODE_FOCUSMODE;	
					pMsg->nParam2	= 3;
					pMsg->nParam3	= eUCS_CAP_AF_MODE_MF;
					::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);

					Sleep(100);
					//ChangeMF(!m_bMF);
				}
				else
				{
					//ChangeMF(!m_bMF);
					Sleep(100);

					pMsg = new RSEvent;
					pMsg->message	= WM_RS_SET_FOCUS_MODE;
					pMsg->nParam1	= PTP_CODE_FOCUSMODE;	
					pMsg->nParam2	= 3;
					pMsg->nParam3	= eUCS_CAP_AF_MODE_SAF;
					::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
				}
				//m_SdiMultiMgr.SetPropertyValueEnum(0, PTP_CODE_FOCUSMODE, 3, eUCS_CAP_AF_MODE_MF);
				//-- 2013-03-01 hongsu.jung
				//-- Open Function 
				//-- Needs Function Check
				
			}
			break;
		//-- 2011-06-14 hongsu.jung
		case BTN_CMD_OPT:
			if(!m_bRecordStatus)
			{
				((CNXRemoteStudioDlg*)GetParent())->OptionOnOff();
				GetCurMode();
				((CNXRemoteStudioDlg*)GetParent())->SetMode(m_nMode);
				break;
			}
			else
			{
				break;
			}
			
		//-- 2011-06-27 hongsu.jung
		//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
		case BTN_CMD_REW	:	SetFocusPosition(FOCUS_FF	);		break;
		case BTN_CMD_PREV	:	SetFocusPosition(FOCUS_NEXT	);		break;
		case BTN_CMD_NEXT	:	SetFocusPosition(FOCUS_PREV	);		break;
		case BTN_CMD_FF		:	SetFocusPosition(FOCUS_REW	);		break;

		case BTN_MF_BAR_HANDLE:
			{				
				m_bFocusHandelPress = TRUE;
			}
			break;
		default:
//			((CNXRemoteStudioDlg*)GetParent())->HidePopup(CMB_MULTI);
			if(IsMF())
				if(SetPosition(point))
				{
					
				}
				break;
		}		
	}
	CDialog::OnLButtonDown(nFlags, point);	
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CCommandDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	if(m_bEnable)
	{
		int nItem = PtInRect(point, FALSE);	
		switch(nItem)
		{
			case BTN_CMD_SHUT:
				EnableControl(FALSE);
				SetTimer(timer_enable, 1000, NULL);
				//-- 2011-06-29 hongsu.jung
				//-- check shutter
				if(GetShutter() == S2_PRESS)
				{
					//-- S2 Release Event Send
					GetCaptureOrder(S2_RELEASE);
					GetCaptureOrder(S1_RELEASE);
					//EnableControl(FALSE);
					//CMiLRe 20141119 Sleep Mode 에서 캡쳐

					//SetTimer(timer_button_SEND_WAIT_DLG, 1000, NULL);
					
					
				}
				/*
				else if(GetShutter() == S1_PRESS)	
					//-- S1 Release Event Send
					GetCaptureOrder(S1_RELEASE);
				*/
				break;
			
			case BTN_CMD_RECORD:
				//dh0.seo 2014-09-24
				if(DSC_MODE_PANORAMA == GetCurMode() || DSC_MODE_BULB == GetCurMode() || DSC_MODE_SCENE_AUTO_SHUTTER == GetCurMode() || DSC_MODE_SCENE_AUTO_SHUTTER == GetCurMode())
				{
					return;
				}

				//CMiLRe 20141127 Display Save Mode 에서 Wake up
				//if(GetDPSleepModeStatus())
				//	return;

				if(GetShutter() == SHUTTER_NULL)
				{
					CNXRemoteStudioDlg* pMain = (CNXRemoteStudioDlg*)GetParent();
					if(!m_bRecordStatus)
					{
						((CNXRemoteStudioDlg*)GetParent())->OptionClose();
						GetCaptureOrder(S3_RELEASE);
						//m_bRecStartStop = TRUE;
						m_bRecord = TRUE;
						if(pMain)
							pMain->m_pDlgSmartPanel->EnableWindow(FALSE);
					}
					else
					{
						GetCaptureOrder(S3_RELEASE);
						//m_bRecStartStop = FALSE;
						m_bRecord = FALSE;
						if(pMain)
						pMain->m_pDlgSmartPanel->EnableWindow(TRUE);
					}

					if(GetShutter() == SHUTTER_NULL)
					{
						RSEvent* pMsg	= new RSEvent;
						pMsg->message	= WM_RECORD_POPUP_DLG;
						pMsg->nParam1	= m_bRecord;
						pMsg->nParam2   = FALSE;
						//::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
					}
					else
						SetSmartPanelEnable();
				}
				break;			
			default:
				{
				if(m_nMouseIn == nItem)
					ChangeStatus(nItem,BTN_INSIDE); 
				else
					ChangeStatus(nItem,BTN_NORMAL); 
				}
				break;
		}

		//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
		if(m_bFocusHandelPress)
		{
			SetFocusPositionValue(m_tFocusPosition.current);
		}

		m_bFocusHandelPress = FALSE;
		ChangeStatusAll();		
	}
	CDialog::OnLButtonUp(nFlags, point);
}

BOOL CCommandDlg::SetPosition(CPoint point)
{
	if(m_rect[BTN_MF_BAR].PtInRect(point))
	{
		return TRUE;
	}	

	return FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CCommandDlg::ChangeMF(BOOL bMF)
{
	//-- 2011-9-14 Lee JungTaek
	if(m_bMF == bMF)
		return;

	 m_bMF = bMF;
	 //-- Redraw Buttons
	 int nStatus = BTN_NORMAL;	 
	 for(int i = BTN_CMD_REW; i <= BTN_CMD_FF; i ++)
	 	ChangeStatus(i, nStatus, FALSE);
	 //-- 2011-9-7 Lee JungTaek
	 //-- Revision
	 Invalidate(FALSE);
	 //-- 2011-06-22 hongsu.jung
	 UpdateWindow();
	 //-- Change LiveviewFrame
	 ((CNXRemoteStudioDlg*)GetParent())->GetFocus();
}


//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CCommandDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = BTN_CMD_NULL;
	int nStart = 0;
	if(!IsMF())
		nStart = BTN_CMD_OPT;
	for(int i = nStart ; i < BTN_CMD_CNT; i ++)
	{
		if(m_rect[i].PtInRect(pt))
		{
			nReturn = i;
			//-- 2013-03-01 ignore panorama
			if(i == BTN_CMD_SHUT && DSC_MODE_PANORAMA == GetCurMode()) //dh0.seo 2014-09-23 Bulb 추가

				continue;

			//-- 2011-7-26 hongsu.jung
			//-- Disable Not Connected 
			if(i == BTN_CMD_MULTI && !m_bConnection )
				continue;
	
			//-- Change Status
			//CMiLRe 20141119 Sleep Mode 에서 캡쳐
			if(bMove)	
			{
				if(!m_bSetTimerFocus &&  i == BTN_CMD_SHUT)
					ChangeStatus(i,BTN_INSIDE);
				else if(i != BTN_CMD_SHUT)
					ChangeStatus(i,BTN_INSIDE);
			}
			else		
				ChangeStatus(i,BTN_DOWN);						
		}
		else
			ChangeStatus(i,BTN_NORMAL);
	}
	return nReturn;
}
//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CCommandDlg::ChangeStatus(int nItem, int nStatus, BOOL bRedraw)
{
	if(!m_bConnection && nItem == BTN_CMD_MULTI)
		return;

	if(m_nStatus[nItem] != nStatus)
	{
		m_nStatus[nItem] = nStatus;
		if(bRedraw)
		{
			CPaintDC dc(this);
			//-- 2011-08-02 hongsu.jung
			// Double Buffering
			CDC mDC;
			CBitmap mBitmap;		
			mDC.CreateCompatibleDC(&dc);
			mBitmap.CreateCompatibleBitmap(&dc, m_rect[nItem].Width(), m_rect[nItem].Height()); 
			mDC.SelectObject(&mBitmap);
			mDC.PatBlt(0,0,m_rect[nItem].Width(), m_rect[nItem].Height(), WHITENESS);

			DrawItem(&mDC, nItem);
			InvalidateRect(m_rect[nItem]);
			//-- 2011-08-02 hongsu.jung
			// Double Buffering
			dc.BitBlt(0, 0, m_rect[nItem].Width(), m_rect[nItem].Height(), &mDC, 0, 0, SRCCOPY);
		}
	}	
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CCommandDlg::ChangeStatusAll()
{
	int nStart = 0;
	if(!IsMF())
		nStart = BTN_CMD_OPT;
	for(int i = nStart; i < BTN_CMD_CNT; i ++)
	{
		
		//-- 2013-03-01 ignore panorama
		if(i == BTN_CMD_SHUT && DSC_MODE_PANORAMA == GetCurMode()) //dh0.seo 2014-09-23 Bulb 추가
			continue;

		//-- 2011-7-26 hongsu.jung
		//-- Disable Not Connected 
		if(i == BTN_CMD_MULTI && !m_bConnection )
			continue;

		if(m_nMouseIn == i)
			ChangeStatus(i,BTN_INSIDE);
		else
			ChangeStatus(i,BTN_NORMAL);		
	}
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-6-24
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CCommandDlg::SetFocusPosition(int nType)
{
	//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
	int nPosDetailUnit	= (m_tFocusPosition.max - m_tFocusPosition.min) / 100;
	int nPosRoughUnit	= (m_tFocusPosition.max - m_tFocusPosition.min) / 10;

	switch(nType)
	{
	case FOCUS_REW:
		if(m_tFocusPosition.min > (m_tFocusPosition.current + nPosRoughUnit))
			return;
		break;	
	case FOCUS_PREV:
		if(m_tFocusPosition.min > (m_tFocusPosition.current + nPosDetailUnit))
			return;
		break;
	case FOCUS_NEXT:
		if(m_tFocusPosition.max < (m_tFocusPosition.current - nPosDetailUnit))
			return;
		break;
	case FOCUS_FF:
		if(m_tFocusPosition.max < (m_tFocusPosition.current - nPosRoughUnit))
			return;
		break;
	}
	
	RSEvent* pMsg	= new RSEvent;
	pMsg->message	= WM_LIVEVIEW_EVENT_SET_FOCUS;
	pMsg->nParam1	= nType;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
void CCommandDlg::SetFocusPositionValue(int nValue)
{
	if(m_tFocusPosition.current >= m_tFocusPosition.min && m_tFocusPosition.current <= m_tFocusPosition.max)
	{
		RSEvent* pMsg	= new RSEvent;
		pMsg->message	= WM_LIVEVIEW_EVENT_SET_FOCUS_VALUE;
		pMsg->nParam1	= nValue;	
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
	}
}
//------------------------------------------------------------------------------ 
//! @brief		Get Selected DSC Name
//! @date		2011-06-28
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
CString CCommandDlg::GetSelectDSCTitle()
{
	return ((CNXRemoteStudioDlg*)GetParent())->GetPTPName(((CNXRemoteStudioDlg*)GetParent())->GetSelectedItem());
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CCommandDlg::RedrawIcon(int nItem)
{
	CPaintDC dc(this);	
	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, m_rect[nItem].Width(), m_rect[nItem].Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,m_rect[nItem].Width(), m_rect[nItem].Height(), WHITENESS);

	DrawItem(&mDC, nItem);
	InvalidateRect(m_rect[nItem],FALSE);

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, m_rect[nItem].Width(), m_rect[nItem].Height(), &mDC, 0, 0, SRCCOPY);	
}

//------------------------------------------------------------------------------ 
//! @brief		GetMTPMgr
//! @date		2011-06-29
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
CSdiMultiMgr* CCommandDlg::GetMTPMgr()
{
	return &((CNXRemoteStudioDlg*)GetParent())->m_SdiMultiMgr;
}

void CCommandDlg::GetRecordStatus()
{
	int nAll = GetMTPMgr()->GetPTPCount();
	int nSelect = ((CNXRemoteStudioDlg*)GetParent())->GetSelectedItem();
	if(!nAll)
		return;
	
	GetMTPMgr()->GetRecordStatus(nSelect, 0);
}

void CCommandDlg::GetCaptureCount()
{
	int nAll = GetMTPMgr()->GetPTPCount();
	int nSelect = ((CNXRemoteStudioDlg*)GetParent())->GetSelectedItem();
	if(!nAll)
		return;
	
	GetMTPMgr()->GetCaptureCount(nSelect, 0);
}
//------------------------------------------------------------------------------ 
//! @brief		GetCaptureOrder()
//! @date		2011-07-07
//! @author	hongsu.jung
//! @note	 	S1, S2 Check 
//------------------------------------------------------------------------------
void CCommandDlg::GetCaptureOrder(int nOrder)
{
	CString strLog;
	strLog.Format(_T("[SEND] GetCaptureOrder nOrder = %d, nParam2 = %d, nParam3 = %d"), nOrder);
	LogCombine(strLog, __FUNCTION__, __LINE__);

	//-- Check Connection
	int nAll = GetMTPMgr()->GetPTPCount();
	int nSelect = ((CNXRemoteStudioDlg*)GetParent())->GetSelectedItem();
	if(!nAll)
		return;
	//-- Capture All
	for(int i = 0 ; i < nAll ; i ++)
	{
		switch(nOrder)
		{
		case S1_PRESS:		
			SetShutter(S1_PRESS);		break;
		case S2_PRESS:		
			SetShutter(S2_PRESS);		break;
		case S2_RELEASE:	
			SetShutter(SHUTTER_NULL);		break;
		case S1_RELEASE:	
			SetShutter(SHUTTER_NULL);	break;
		//NX1 dh0.seo 2014-08-27
		case S3_PRESS:		SetShutter(S3_PRESS);		break;
		case S3_RELEASE:	SetShutter(SHUTTER_NULL);		break;
		}
		GetMTPMgr()->GetCaptureOrder(nSelect, i, nOrder);
	}
	//-- 2011-07-22 hongsu.jung
	//-- Set Clean To AF Focus Option
	if(nOrder == S2_RELEASE || nOrder == S1_RELEASE)
	{
		((CNXRemoteStudioDlg*)GetParent())->m_pDlgLiveview->m_pDlgToolbar->SetAFFocus(AF_NULL);
		((CNXRemoteStudioDlg*)GetParent())->m_pDlgLiveview->m_pDlgLiveviewFrm->GetLiveview()->SetAFFocus(AF_NULL);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		GetRecordOrder()
//! @date		2014-08-28
//! @author		dh0.seo
//! @note	 	 
//------------------------------------------------------------------------------
void CCommandDlg::GetRecordOrder(int nOrder)
{
	int nAll = GetMTPMgr()->GetPTPCount();
	int nSelect = ((CNXRemoteStudioDlg*)GetParent())->GetSelectedItem();
	AfxMessageBox(_T("Record"));
}

//------------------------------------------------------------------------------ 
//! @brief		CaptureOnce()
//! @date		2011-07-07
//! @author	hongsu.jung
//! @note	 	Capture Once
//------------------------------------------------------------------------------
void CCommandDlg::CaptureOnce()
{
	int nAll = GetMTPMgr()->GetPTPCount();
	int nSelect = ((CNXRemoteStudioDlg*)GetParent())->GetSelectedItem();

	for(int i = 0 ; i < nAll ; i ++)
		GetMTPMgr()->GetCaptureOrder(nSelect, i, CAPTURE_ONCE);
}

//------------------------------------------------------------------------------ 
//! @brief		SetSmartPanelEnable
//! @date		2011-8-8
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CCommandDlg::SetSmartPanelEnable()
{
	CNXRemoteStudioDlg* pMain = (CNXRemoteStudioDlg*)GetParent();
	if(!pMain)
		return;

	//-- 2011-8-5 Lee JungTaek
	//-- Set Capturing Operation
	if(pMain->m_pDlgSmartPanel)
	{
		if(m_bAFDetect)
			pMain->m_pDlgSmartPanel->SetCapturing(TRUE);		
		else
			pMain->m_pDlgSmartPanel->SetCapturing(FALSE);		
	}
	else
		return;

	//-- Drive-Timer
	int nValue = pMain->m_SdiMultiMgr.GetCurrentPropValueEnum(pMain->GetSelectedItem(), PTP_CODE_STILLCAPTUREMODE);
//	if(nValue == eUCS_CAP_DRIVE_TIMER)
	if(nValue == 0xa00)
	{
		if(m_bTimerStart)
		{
			pMain->m_pDlgSmartPanel->SetCapturing(FALSE);	
			m_bTimerStart = FALSE;
			m_bTimerFinish = FALSE;
		}
		else
		{
			if(m_bTimerFinish)	pMain->m_pDlgSmartPanel->SetCapturing(FALSE);	
			else				
				pMain->m_pDlgSmartPanel->SetCapturing(TRUE);	

			m_bTimerStart = TRUE;
			m_bTimerFinish = FALSE;
		}
	}
/*	else
	{
		pMain->m_pDlgSmartPanel->SetCapturing(TRUE); //dh0.seo Captureing Processing...중에 m_bLiveview 를 False -> True로 바꿔주기 위함.
	}	*/
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-9-14
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CCommandDlg::GetCurMode()
{
	int nMode = (((CNXRemoteStudioDlg*)GetParent())->GetModeIndex(((CNXRemoteStudioDlg*)GetParent())->GetSelectedItem()));
	
	switch(nMode)
	{
		case eUCS_UI_MODE_SMARTAUTO				:	m_nMode = DSC_MODE_SMART					 ;	break;
		case eUCS_UI_MODE_PROGRAM				:	m_nMode = DSC_MODE_P						 ;	break;
		case eUCS_UI_MODE_A						:	m_nMode = DSC_MODE_A						 ;	break;
		case eUCS_UI_MODE_S						:	m_nMode = DSC_MODE_S						 ;	break;
		case eUCS_UI_MODE_M						:	m_nMode = DSC_MODE_M						 ;	break;
		case eUCS_UI_MODE_BULB					:	m_nMode = DSC_MODE_BULB					 ;	break;
		case eUCS_UI_MODE_I						:	m_nMode = DSC_MODE_I						 ;	break;
		case eUCS_UI_MODE_CAPTURE_MOVIE			:	m_nMode = DSC_MODE_CAPTURE_MOVIE			 ;	break;
		case eUCS_UI_MODE_MAGIC_MAGIC_FRAME		:	m_nMode = DSC_MODE_MAGIC_MAGIC_FRAME		 ;	break;
		case eUCS_UI_MODE_MAGIC_SMART_FILTER	:	m_nMode = DSC_MODE_MAGIC_SMART_FILTER		 ;	break;
		case eUCS_UI_MODE_PANORAMA				:	m_nMode = DSC_MODE_PANORAMA				 ;	break;
		case eUCS_UI_MODE_SCENE_BEAUTY			:	m_nMode = DSC_MODE_SCENE_BEAUTY			 ;	break;
		case eUCS_UI_MODE_SCENE_NIGHT			:	m_nMode = DSC_MODE_SCENE_NIGHT			 ;	break;
		case eUCS_UI_MODE_SCENE_LANDSCAPE		:	m_nMode = DSC_MODE_SCENE_LANDSCAPE		 ;	break;
		case eUCS_UI_MODE_SCENE_PORTRAIT		:	m_nMode = DSC_MODE_SCENE_PORTRAIT			 ;	break;
		case eUCS_UI_MODE_SCENE_CHILDREN		:	m_nMode = DSC_MODE_SCENE_CHILDREN			 ;	break;
		case eUCS_UI_MODE_SCENE_SPORTS			:	m_nMode = DSC_MODE_SCENE_SPORTS			 ;	break;
		case eUCS_UI_MODE_SCENE_CLOSE_UP		:	m_nMode = DSC_MODE_SCENE_CLOSE_UP			 ;	break;
		case eUCS_UI_MODE_SCENE_TEXT			:	m_nMode = DSC_MODE_SCENE_TEXT				 ;	break;
		case eUCS_UI_MODE_SCENE_SUNSET			:	m_nMode = DSC_MODE_SCENE_SUNSET			 ;	break;
		case eUCS_UI_MODE_SCENE_DAWN			:	m_nMode = DSC_MODE_SCENE_DAWN				 ;	break;
		case eUCS_UI_MODE_SCENE_BACKLIGHT		:	m_nMode = DSC_MODE_SCENE_BACKLIGHT		 ;	break;
		case eUCS_UI_MODE_SCENE_FIREWORK		:	m_nMode = DSC_MODE_SCENE_FIREWORK			 ;	break;
		case eUCS_UI_MODE_SCENE_BEACH_SNOW		:	m_nMode = DSC_MODE_SCENE_BEACH_SNOW		 ;	break;
		case eUCS_UI_MODE_SCENE_SOUND_PICTURE	:	m_nMode = DSC_MODE_SCENE_SOUND_PICTURE	 ;	break;
		case eUCS_UI_MODE_SCENE_3D				:	m_nMode = DSC_MODE_SCENE_3D				 ;	break;
		case eUCS_UI_MODE_MOVIE					:	m_nMode = DSC_MODE_MOVIE					 ;	break;
		case eUCS_UI_MODE_SCENE_SMART_FILTER	:	m_nMode = DSC_MODE_SCENE_SMART_FILTER		 ;	break;
		// dh9.seo 2013-06-24
		case eUCS_UI_MODE_SCENE_BEST			:	m_nMode = DSC_MODE_SCENE_BEST				;	break;
		case eUCS_UI_MODE_SCENE_MACRO			:	m_nMode = DSC_MODE_SCENE_MACRO			;	break;
		case eUCS_UI_MODE_SCENE_ACTION			:	m_nMode = DSC_MODE_SCENE_ACTION			;	break;
		case eUCS_UI_MODE_SCENE_RICH			:	m_nMode = DSC_MODE_SCENE_RICH				;	break;
		case eUCS_UI_MODE_SCENE_WATERFALL		:	m_nMode = DSC_MODE_SCENE_WATERFALL		;	break;
		case eUCS_UI_MODE_SCENE_SILHOUETTE		:	m_nMode = DSC_MODE_SCENE_SILHOUETTE		;	break;
		case eUCS_UI_MODE_SCENE_LIGHT			:	m_nMode = DSC_MODE_SCENE_LIGHT			;	break;
		case eUCS_UI_MODE_SCENE_CREATIVE		:	m_nMode = DSC_MODE_SCENE_CREATIVE			;	break;
		case eUCS_UI_MODE_SCENE_PANORAMA		:	m_nMode = DSC_MODE_SCENE_PANORAMA			;	break;
		//NX1 dh0.seo 2014-08-19
		case eUCS_UI_MODE_ACTION_FREEZE			:	m_nMode = DSC_MODE_SCENE_ACTION_FREEZE	;	break;
		case eUCS_UI_MODE_LIGHT_TRACE			:	m_nMode = DSC_MODE_SCENE_LIGHT_TRACE		;	break;
		case eUCS_UI_MODE_SCENE_MULTI_EXPOSURE	:	m_nMode = DSC_MODE_SCENE_MULTI_EXPOSURE	;	break;
		case eUCS_UI_MODE_SCENE_AUTO_SHUTTER	:	m_nMode = DSC_MODE_SCENE_AUTO_SHUTTER		;	break;
	}
	return m_nMode;
}

//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
void CCommandDlg::SetFocus(FocusPosition* pFocus, BUTTON_COMMAND CMD_TYPE)
{
	CPaintDC dc(this);

	CRect rect;
	GetClientRect(&rect);	
	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);


	//-- Set Focus Position
	m_tFocusPosition.current	= pFocus->current	; 
	m_tFocusPosition.max		= pFocus->max		; 
	m_tFocusPosition.min		= pFocus->min		; 

	TRACE(_T("Set Focus [ %04d > %04d > %04d ]\n"),	m_tFocusPosition.min,	m_tFocusPosition.current	,	m_tFocusPosition.max			); 	
	for(int nItem = BTN_MF_BAR; nItem <= BTN_CMD_FF; nItem ++)	
	{		
		//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
		if(CMD_TYPE == BTN_CMD_MF_CHANGE &&  nItem == BTN_MF_BAR_HANDLE)
		{
			DrawItem(&dc, BTN_CMD_MF_CHANGE);
			continue;
		}
		DrawItem(&dc, nItem);
		InvalidateRect(m_rect[nItem],FALSE);	
	}	
	UpdateWindow();		

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);	
}

//CMiLRe 20141119 Sleep Mode 에서 캡쳐
void CCommandDlg::OnTimer(UINT_PTR nIDEvent)
{
	RSEvent* pMsgToMain = NULL;
	switch(nIDEvent)
	{
		case timer_enable:
			pMsgToMain = new RSEvent();
			pMsgToMain->message	= WM_SDI_EVENT_ERR_MSG;							
			pMsgToMain->nParam1	= PTP_DPC_SAMSUNG_CAPTURE_FAIL;	
			::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_ERR_MSG, (LPARAM)pMsgToMain);
			KillTimer(timer_enable);
			break;
		case timer_button_FOCUS:				
			m_bSetTimerFocus = TRUE;
			if(GetDPSleepModeStatus())
			{				
				GetCaptureOrder(S1_PRESS);	
			}
			else
			{
				KillTimer(timer_button_FOCUS);
				m_bSetTimerFocus = FALSE;				
			}
			break;
		case timer_button_S2_pressed:
			m_bSetTimerCapture = TRUE;
			if((m_bAFDetect || RSGetMFFocus()))			
			{
				if(GetShutter() == S1_PRESS)	
				{
					GetCaptureOrder(S2_PRESS);
					SetSmartPanelEnable();
				}
				else if(GetShutter() == SHUTTER_NULL)	
				{
					GetCaptureOrder(S1_PRESS);		
					GetCaptureOrder(S2_PRESS);		
					SetSmartPanelEnable();
				}
				MouseEvent();
				KillTimer(timer_button_S2_pressed);			
				m_bSetTimerCapture = FALSE;
			}
			else
			{
				if(!m_bSetTimerFocus)
				{
					GetCaptureOrder(S1_RELEASE);	
					Sleep(200);
					GetCaptureOrder(S1_PRESS);	
					GetCaptureOrder(S1_PRESS);	
				}
			}
			break;
		case timer_button_SEND_WAIT_DLG:
			{
				pMsgToMain = new RSEvent();
				pMsgToMain->message	= WM_SDI_EVENT_ERR_MSG;							
				pMsgToMain->nParam1	= 1;	
				::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_ERR_MSG, (LPARAM)pMsgToMain);
				KillTimer(timer_button_SEND_WAIT_DLG);		
			}
			break;
		case timer_recording:
				GetRecordStatus();
				GetCaptureCount();
				
				m_nSec++;
				m_nRecordTime++;
				DrawBackground();
			break;
		default: 
			CRSChildDialog::OnTimer(nIDEvent);
			return;
	}	
	CRSChildDialog::OnTimer(nIDEvent);
}