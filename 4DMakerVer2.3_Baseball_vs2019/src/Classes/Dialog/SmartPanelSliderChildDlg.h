/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelSliderChildDlg.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-09-15
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "BKDialogST.h"
#include "RSSliderCtrl.h"
#include "NXRemoteStudioFunc.h"

enum
{
	BTN_SD_NULL = -1,
	BTN_SD_LEFT,
	BTN_SD_RIGHT,	
	BTN_SD_CNT	,
};

enum
{
	PT_SD_IN_NOTHING = -1,
	PT_SD_NOR,
	PT_SD_INSIDE,
	PT_SD_DOWN,
	PT_SD_DIS,
};

class CSmartPanelSliderChildDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CSmartPanelSliderChildDlg)

public:
	CSmartPanelSliderChildDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSmartPanelSliderChildDlg();

// Dialog Data
	enum { IDD = IDD_DLG_SMARTPANEL_BAROMETER_DETAIL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	//-- Item Info
	int	m_nMouseIn;
	int	m_nStatus[BTN_SD_CNT];
	CRect m_rectBtn[BTN_SD_CNT];
	CRect m_rectIndi[EV_POS_CNT];
	
	//-- Draw Image
	void DrawBackground();	
	void CleanDC(CPaintDC* pDC);
	void DrawMenu(int nItem, CDC* pDC);
	void DrawImageIndicator(CDC* pDC);

	//-- Mouse Function
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	void ChangeStatus(int nItem, int nStatus);
	void ChangeStatusAll();
	void MouseEvent();

	//-- Draw Value
	int SetIndiPos();
	//-- Click
	int PtInRectValue(CPoint point);
	void SetPropValueIndex(int nIndex);
	
private:
	//-- 2011-9-15 Lee JungTaek
	CRSSliderCtrl* m_pRSSlider;
	int m_nMode;

public:
	//-- 2011-9-15 Lee JungTaek
	void SetCtrolInfo(CRSSliderCtrl* pRSSlider)	{m_pRSSlider = pRSSlider;}
	void SetMode(int nMode){m_nMode = nMode;}
};
