/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelWBColorDetailDlg.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-07-01
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudio.h"
#include "SmartPanelWBColorDetailDlg.h"
#include "afxdialogex.h"
#include "NXRemoteStudioDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define RECT_BAR	CRect(CPoint(114, 20), CSize(200, 30))

// CSmartPanelWBColorDetailDlg dialog
IMPLEMENT_DYNAMIC(CSmartPanelWBColorDetailDlg, CDialogEx)

	CSmartPanelWBColorDetailDlg::CSmartPanelWBColorDetailDlg(CWnd* pParent /*=NULL*/)
	: CBkDialogST(CSmartPanelWBColorDetailDlg::IDD, pParent)
{
	m_nMouseIn = BTN_WB_COLORTEMP_NULL;	

	for(int i=0; i<BTN_WB_COLORTEMP_CNT - 1; i++)
	{
		m_nStatus[i] = PT_WB_NOR;
		m_rectBtn[i] = CRect(0,0,0,0);
	}


	m_nPTPCode		= 0;
	m_nValueIndex	= 0;

	m_nColorTemp = 0;
	m_nColorTemp1st = 0;

	m_bInit = FALSE;	
}

CSmartPanelWBColorDetailDlg::~CSmartPanelWBColorDetailDlg()
{
}

void CSmartPanelWBColorDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSmartPanelWBColorDetailDlg, CBkDialogST)
	ON_WM_PAINT()	
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()


BOOL CSmartPanelWBColorDetailDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();		

	//SetBitmap(RSGetRoot(_T("img\\02.Smart Panel\\02_adjust_bg.png")),RGB(255,255,255));
	SetItemPosition();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSmartPanelWBColorDetailDlg::SetItemPosition()
{
	//-- Icon
	m_rectIcon		=	CRect(CPoint(8,20), CSize(24,24)) ;
	//-- Value
	m_rectValue		=	CRect(CPoint(32,23), CSize(50,24)) ;
	//-- Bar
	m_rectBar		=	CRect(CPoint(114,25), CSize(197,16)) ;
	//-- Handle
	m_rectHandler	=	CRect(CPoint(130,22), CSize(12,28)) ;
	//-- Button
	m_rectBtn[BTN_WB_COLORTEMP_LEFT]	=	CRect(CPoint(80,21), CSize(25,24));
	m_rectBtn[BTN_WB_COLORTEMP_RIGHT]	=	CRect(CPoint(320,21), CSize(25,24));
	m_rectBtn[BTN_WB_COLORTEMP_RESET]	=	CRect(CPoint(8,68), CSize(55,25));
	m_rectBtn[BTN_WB_COLORTEMP_OK	]	=	CRect(CPoint(294,68), CSize(55,25));
}

void CSmartPanelWBColorDetailDlg::OnPaint()
{	
	DrawBackground();	
	CRSChildDialog::OnPaint();
}

void CSmartPanelWBColorDetailDlg::CleanDC(CDC* pDC)
{
	for(int i = 0; i < BTN_WB_COLORTEMP_CNT - 1; i++)
	{
		if(i < BTN_WB_COLORTEMP_CNT - 2)
		{
			CBrush brush = RGB_BLACK;
			pDC->FillRect(m_rectBtn[i], &brush);
		}
		else
		{
			CBrush brush = RGB_DEFAULT_DLG;
			pDC->FillRect(m_rectBtn[i], &brush);
		}
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::DrawBackground()
{
	CPaintDC dc(this);
	
	//CleanDC(&dc);

	CRect rect;
	GetClientRect(&rect);

	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- Draw Round Dialog
	DrawRoundDialogs(&mDC, TRUE);
	DrawImageBase(&mDC, RSGetRoot(_T("img\\02.Smart Panel\\02_adjust_bg.png")),0, 0);
	
	//-- Draw Line
	DrawLine(&mDC);

	//-- Draw Menu
	DrawMenu(BTN_WB_COLORTEMP_LEFT, &mDC);
	DrawMenu(BTN_WB_COLORTEMP_RIGHT, &mDC);
	DrawMenu(BTN_WB_COLORTEMP_RESET, &mDC);
	DrawMenu(BTN_WB_COLORTEMP_OK, &mDC);

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawLine / DrawIcon / DrawValue / DrawBar / DrawBarHandler
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Main Camera / Name / Mode / Check Box
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::DrawLine(CDC* pDC)
{
	//-- Draw Icon
	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_adjust_icon_colortemp.png")), m_rectIcon);
	//-- Draw Value
	DrawStr(pDC, STR_SIZE_BUTTON, GetValues(), m_rectValue,RGB(255,255,255));
	//-- Draw Bar
	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_bar_bg.png")), m_rectBar);
	//-- Draw Handler
	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\03.Live View\\03_focus_progress_handle_over.png")), m_rectHandler);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawMenu
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Draw Buttons
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::DrawMenu(int nItem, CDC* pDC)
{
	int nX, nY, nW, nH;
	nX	= m_rectBtn[nItem].left;	
	nY	= m_rectBtn[nItem].top;
	nW	= m_rectBtn[nItem].Width();
	nH	= m_rectBtn[nItem].Height();

	COLORREF color;

	switch(nItem)
	{
	case BTN_WB_COLORTEMP_LEFT:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_focus.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_push.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_disable.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		default	:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_normal.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		}
		break;
	case BTN_WB_COLORTEMP_RIGHT:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_focus.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_push.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_disable.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_normal.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	break;
		}
		break;	
	case BTN_WB_COLORTEMP_RESET:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_push.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_disable.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(219,219,219); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Reset"),m_rectBtn[nItem],color);	
		break;
	case BTN_WB_COLORTEMP_OK:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_push.png")),		m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_disable.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png")),	m_rectBtn[nItem].left, m_rectBtn[nItem].top, m_rectBtn[nItem].Width(), m_rectBtn[nItem].Height());	color = RGB(219,219,219); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("OK "),m_rectBtn[nItem],color);	
		break;
	}
}

CString CSmartPanelWBColorDetailDlg::GetValues()
{
	CString strValue = _T("");

	strValue.Format(_T("%dK"), m_nColorTemp);

	return strValue;
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	m_nMouseIn = PtInRect(point, TRUE);
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

void CSmartPanelWBColorDetailDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CSmartPanelWBColorDetailDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_WB_COLORTEMP_CNT; i ++)
		ChangeStatus(i,PT_WB_NOR);
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	

	int x = 0 , y = 0;

	switch(nItem)
	{
	case BTN_WB_COLORTEMP_LEFT			:		SetColorTempValue(FALSE);	break;
	case BTN_WB_COLORTEMP_RIGHT			:		SetColorTempValue(TRUE);	break;
	case BTN_WB_COLORTEMP_RESET			:		ResetValue();	break;
	case BTN_WB_COLORTEMP_OK			:		((CSmartPanelChildDlg*)GetParent())->CloseSmartPanelChild();				return;
	default:								if(SetPosition(point))		SetDetailValue();		return;	//-- Check Enable Rect
	}

	//-- 2011-8-2 Lee JungTaek
	SetDetailValue();	

	CDialog::OnLButtonDown(nFlags, point);	
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ChangeStatusAll();	
	CDialog::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CSmartPanelWBColorDetailDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = PT_WB_IN_NOTHING;
	int nStart = 0;

	for(int i = nStart ; i < BTN_WB_COLORTEMP_CNT; i ++)
	{
		if(m_rectBtn[i].PtInRect(pt))
		{
			nReturn = i;
			if(bMove)
				ChangeStatus(i,PT_WB_INSIDE);
			else
				ChangeStatus(i,PT_WB_DOWN); 
		}
		else
			ChangeStatus(i,PT_WB_NOR); 
	}
	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::ChangeStatus(int nItem, int nStatus)
{
	if(m_nStatus[nItem] != nStatus)
	{
		m_nStatus[nItem] = nStatus;
		CPaintDC dc(this);
		DrawMenu(nItem, &dc);
		InvalidateRect(m_rectBtn[nItem],FALSE);
	}
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::ChangeStatusAll()
{
	int nStart = 0;
	for(int i = nStart; i < BTN_WB_COLORTEMP_CNT; i ++)
	{
		if(m_nMouseIn == i)
			ChangeStatus(i,PT_WB_INSIDE); 
		else
			ChangeStatus(i,PT_WB_NOR); 
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		SetColorTempValue
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Set Values (Min:0 / Max:21)
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::SetColorTempValue(BOOL bRight)
{
	if(bRight)
	{
		m_nColorTempIndex++;
		if(m_nColorTempIndex > 74)			m_nColorTempIndex = 75;
	}
	else
	{
		m_nColorTempIndex--;
		if(m_nColorTempIndex < 0)			m_nColorTempIndex = 0;
	}

	m_nColorTemp = m_nColorTempIndex * 100 + 2500;

	TRACE(_T("m_nColorTempIndex = %d / m_nColorTemp = %d\n"), m_nColorTempIndex, m_nColorTemp);
}

//------------------------------------------------------------------------------ 
//! @brief		SetValueIndex
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Set First Values
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::SetValueIndex(int nValue)
{
	 //-- Set Value
	m_nColorTemp = nValue;
	//-- Set Index
	m_nColorTempIndex = (nValue - 2500) / 100;

	if(!m_bInit)
	{
		m_nColorTemp1st	= m_nColorTemp;
		m_bInit = TRUE;
	}
	
	//-- Set Handle Position And Redraw
	InvalidateRect(m_rectValue, FALSE);	
	SetHandler();
}

//------------------------------------------------------------------------------ 
//! @brief		SetHandler
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	Set Bar Handler Position
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::SetHandler()
{
	int nX = 0, nY = 0;

	nX = 114 + m_nColorTempIndex * 2.5;	nY = 22;
	m_rectHandler	=  CRect(CPoint(nX,nY), CSize(12,28));

	if(this->GetSafeHwnd())
	{
		//-- Update Image
		InvalidateRect(RECT_BAR, FALSE);
	}		
}	

//------------------------------------------------------------------------------ 
//! @brief		SetPosition
//! @date		2011-7-29
//! @author		Lee JungTaek
//! @note	 	Set Bar Event
//------------------------------------------------------------------------------ 
BOOL CSmartPanelWBColorDetailDlg::SetPosition(CPoint point)
{
	if(m_rectBar.PtInRect(point))
	{
		m_nColorTempIndex = (point.x - 114) / 2.5;

		//-- Check Index
		if(m_nColorTempIndex > 74)			m_nColorTempIndex = 75;
		else if(m_nColorTempIndex < 0)		m_nColorTempIndex = 0;
	
		m_nColorTemp = m_nColorTempIndex * 100 + 2500;

		return TRUE;
	}	

	return FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief		ResetValue
//! @date		2011-7-29e
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::ResetValue()
{
	//-- 2011-8-1 Lee JungTaek
	m_nColorTemp = m_nColorTemp1st;
	m_nColorTempIndex = (m_nColorTemp - 2500) / 100;; 
	SetDetailValue();
}

//------------------------------------------------------------------------------ 
//! @brief		SetDetailValue
//! @date		2011-7-29
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::SetDetailValue()
{
	//-- Set Detail Value
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	pMainWnd->SetPropertyValue(pMainWnd->GetSelectedItem(), m_nPTPCode, m_nColorTemp);
}

//------------------------------------------------------------------------------ 
//! @brief		GetDetailPropCode
//! @date		2011-7-28
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CSmartPanelWBColorDetailDlg::GetDetailPropCode(int nValueIndex)
{
	int nPropCode = 0;
	//CMiLRe 20140903 �𵨺� WB detail �߰�
	CString strModel = ((CNXRemoteStudioDlg*)AfxGetMainWnd())->GetModel();	
	if(strModel.Compare(modelType[MDOEL_NX2000].strModelName) == 0)
	{

		switch(nValueIndex)
		{
		case DSC_WB_DETAIL_AUTO				:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_AUTO				;	break;
		case DSC_WB_DETAIL_DAYLIGHT			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_DAYLIGHT			;	break;
		case DSC_WB_DETAIL_CLOUDY			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_CLOUDY			;	break;
		case DSC_WB_DETAIL_FLURESCENT_W		:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_W		;	break;
		case DSC_WB_DETAIL_FLURESCENT_NW	:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_NW	;	break;
		case DSC_WB_DETAIL_FLURESCENT_D		:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_D		;	break;
		case DSC_WB_DETAIL_TUNGSTEN			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_TUNGSTEN			;	break;
		case DSC_WB_DETAIL_FLASH			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLASH			;	break;
		case DSC_WB_DETAIL_CUSTOM			:	break;
		case DSC_WB_DETAIL_KELVIN			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN			;	break;
		case DSC_WB_DETAIL_AUTO_TUNGSTEN	:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_AUTO_TUNGSTEN	;	break;
		}
	}
	else if(strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)
	{
		switch(nValueIndex)
		{
		case DSC_WB_DETAIL_AUTO_NX1				:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_AUTO				;	break;
		case DSC_WB_DETAIL_AUTO_TUNGSTEN_NX1	:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_AUTO_TUNGSTEN	;	break;
		case DSC_WB_DETAIL_TUNGSTEN_NX1			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_TUNGSTEN			;	break;
		case DSC_WB_DETAIL_DAYLIGHT_NX1			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_DAYLIGHT			;	break;
		case DSC_WB_DETAIL_CLOUDY_NX1			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_CLOUDY			;	break;
		case DSC_WB_DETAIL_FLURESCENT_W_NX1		:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_W		;	break;
		case DSC_WB_DETAIL_FLURESCENT_NW_NX1	:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_NW	;	break;
		case DSC_WB_DETAIL_FLURESCENT_D_NX1		:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_D		;	break;	
		case DSC_WB_DETAIL_FLASH_NX1		:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLASH			;	break;
		case DSC_WB_DETAIL_CUSTOM_NX1			:	break;
		case DSC_WB_DETAIL_KELVIN_NX1			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN			;	break;	
		}
	}
	m_nPTPCode = nPropCode;
	m_nValueIndex = nValueIndex;

	return nPropCode;
}

//------------------------------------------------------------------------------ 
//! @brief		SetFirstValue
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelWBColorDetailDlg::SetFirstValue()
{
	CSmartPanelChildDlg *pSmartPanelChild = (CSmartPanelChildDlg*)GetParent();

	//-- Set First Values
	m_nColorTemp	= m_nColorTemp1st		;

	//-- Set Detail Value
	SetDetailValue();
	//-- Set 2 Depth Value
	int nValueIndex = pSmartPanelChild->GetSelected1stValue();
	pSmartPanelChild->SetSelectedPropValue(nValueIndex);
	//-- Close Dialog
	pSmartPanelChild->CloseSmartPanelChild();
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
BOOL CSmartPanelWBColorDetailDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_ESCAPE)
		{
			SetFirstValue();
			return TRUE;
		}
	}
	else if(pMsg->message == WM_MOUSEWHEEL)		return TRUE;

	return CBkDialogST::PreTranslateMessage(pMsg);
}