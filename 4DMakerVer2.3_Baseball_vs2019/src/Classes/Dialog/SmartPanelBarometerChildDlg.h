/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelBarometerChildDlg.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-09-15
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "BKDialogST.h"
#include "RSBarometerCtrl.h"
#include "NXRemoteStudioFunc.h"

enum
{
	BTN_BM_NULL = -1,
	BTN_BM_LEFT,
	BTN_BM_RIGHT,	
	BTN_BM_CNT	,
};

enum
{
	PT_BM_IN_NOTHING = -1,
	PT_BM_NOR,
	PT_BM_INSIDE,
	PT_BM_DOWN,
	PT_BM_DIS,
};

class CSmartPanelBarometerChildDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CSmartPanelBarometerChildDlg)

public:
	CSmartPanelBarometerChildDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSmartPanelBarometerChildDlg();

// Dialog Data
	enum { IDD = IDD_DLG_SMARTPANEL_BAROMETER_DETAIL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	//-- Item Info
	int	m_nMouseIn;
	int	m_nStatus[BTN_BM_CNT];
	CRect m_rectBtn[BTN_BM_CNT];
	CRect m_rectText[TEXT_POS_CNT];
	CRect m_rectValue;

	//-- Draw Image
	void DrawBackground();	
	void DrawMenu(int nItem, CDC* pDC);

	//-- Mouse Function
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	void ChangeStatus(int nItem, int nStatus);
	void ChangeStatusAll();
	void MouseEvent();

	//-- Draw Value
	void DrawDlgValue(CDC* pDC);
	void DrawCtrlText(CDC* pDC);
	void DrawCtrlValue(CDC* pDC);
	
	//-- Click
	int PtInRectValue(CPoint point);
	

private:
	BOOL m_bPosType;
	CString m_strBarometerBKPath;

	//-- 2011-9-15 Lee JungTaek
	CRSBarometerCtrl* m_pRSBarometer;

public:
	//-- 1 Depth Info
	void SetPosType(BOOL b)	{m_bPosType = b;	SetBGImageType();}
	void SetBGImageType();

	//-- 2011-9-15 Lee JungTaek
	CRSBarometerCtrl* GetCtrolInfo() { return m_pRSBarometer;}
	void SetCtrolInfo(CRSBarometerCtrl* pRSBarometer)	{m_pRSBarometer = pRSBarometer;}
	void ChangeValue(int index);
	void SetPropValueIndex(int nIndex);
};
