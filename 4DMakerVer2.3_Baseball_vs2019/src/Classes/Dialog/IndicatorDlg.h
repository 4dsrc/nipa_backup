/////////////////////////////////////////////////////////////////////////////
//
//  IndicatorDlg.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "RSChildDialog.h"
#include "ptpindex.h"
#include "NXRemoteStudioFunc.h"

// CIndicatorDlg dialog
class CIndicatorDlg : public CRSChildDialog
{
	DECLARE_DYNAMIC(CIndicatorDlg)

public:
	CIndicatorDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CIndicatorDlg();

// Dialog Data
	enum { IDD = IDD_DLG_BOARD};
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	DECLARE_MESSAGE_MAP()

private:
	int m_nValue;
	int m_nIndicator[RS_INDICATOR_SIZE];
	CRect m_rect[RS_INDICATOR_SIZE];
	//-- 2011-6-29 Lee JungTaek
	CString m_strDSCName;

	BOOL m_bDotString;
	BOOL m_bAutoISO;

	void DrawBackground();
	int GetIndicator(int nPTPCode);

	void GetFNumberValue(CDC* pDC, int nIndicator, int nValue);
	void GetShutterSpeedValue(CDC* pDC, int nIndicator, int nValue);
	void GetDSCName(CDC* pDC, int nIndicator);
	
	
	//void DrawStringImage(CDC* pDC, int nIndicator, int nChar, int nIndex);
	void DrawStringImage(CDC* pDC, int nIndicator, CString strValue);
	CString ChangeValueToImage(int nIndicator, int nChar);

	//-- 2011-8-26 Lee JungTaek
	int CheckIModeScene(int nValue);
	int CheckIModeSmartFilter();

public:
	void SetItem(int nPTPCode, int nValue);
	void SetDefaultIndicator();
	void SetDSCName(CString strValue) {m_strDSCName = strValue; m_nIndicator[RS_INDICATOR_DSC_NAME] = DSC_NAME_EXIST; Invalidate();}

	//CMiLRe 20140902 DRIVER GET ����
	CString m_strModel;
	int m_nCaptureCount;
protected:
	void DrawIndicator (int nIndicator, CDC* pDC);
};
