/////////////////////////////////////////////////////////////////////////////
//
//  ThumbNailDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudioDlg.h"
#include "FileOperations.h"
#include "ThumbnailDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const int timer_button_pressed		= WM_USER + 0x0001;
static const int timer_thumbnail_update		= WM_USER + 0x0002;
static const int wait_pressed_time			= 400;

// CThumbNailDlg dialog
IMPLEMENT_DYNAMIC(CThumbnailDlg,  CRSChildDialog)

CThumbnailDlg::CThumbnailDlg(CWnd* pParent /*=NULL*/)
	: CRSChildDialog(CThumbnailDlg::IDD, pParent)
{
	//-- 2011-05-18 hongsu.jung
	//-- Load From Config
	m_pDlgThumbnailList	= NULL;
	m_nPressed				= PRESSED_NOTHING;
	for(int i = 0 ; i < 3; i ++)
		m_nStatus[i] = BTN_NORMAL;
	m_pThumbnailLoader = NULL;

	//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
	m_nThumnailCnt = 0;

	//CMiLRe 20141124 섬네일 로드 쓰레드로 변경
	m_hThread = NULL;
	m_bLoadThumbnail = FALSE;
	m_nIndex = 0;
	m_strCameraID = _T("");
}

CThumbnailDlg::~CThumbnailDlg()
{
	if(m_pDlgThumbnailList)
	{
		//CMiLRe 20141124 섬네일 로드 쓰레드로 변경
		if(m_hThread)
		{
			if(WaitForSingleObject(m_hThread, 1000) != WAIT_OBJECT_0)
			{
				DWORD dwError = 0;
				TerminateThread(m_hThread, dwError);
			}
			CloseHandle(m_hThread);
			m_hThread = NULL;
		}

		if(m_pThumbnailLoader)
		{
			m_pThumbnailLoader->SetThumbnailList(NULL);			
			m_pThumbnailLoader->StopThread();
			m_pThumbnailLoader->ExitInstance();		
		}
		delete m_pDlgThumbnailList;
		m_pDlgThumbnailList = NULL;
	}
}


void CThumbnailDlg::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CThumbnailDlg, CRSChildDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONUP()	
	ON_WM_MOUSEMOVE()		
	ON_WM_TIMER()	
	ON_MESSAGE(WM_LOAD_THUMBNAIL,OnReload)	
END_MESSAGE_MAP()

//------------------------------------------------------------------------------ 
//! @brief		OnTimer
//! @date		2011-6-16
//------------------------------------------------------------------------------ 
void CThumbnailDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
		//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
		case timer_thumbnail_update:
			{
				//CMiLRe 20141124 섬네일 연사일 경우 여러자 못가지고 오는 버그 수정
				int nThumnailCount = GetThumnailFileCount();
				if(nThumnailCount < 0)
					m_nThumnailCnt = 0;
				if(nThumnailCount != m_nThumnailCnt)
				{
					m_nThumnailCnt = nThumnailCount;
					//CMiLRe 20141124 섬네일 로드 쓰레드로 변경
					m_bLoadThumbnail = TRUE;
				}				
			}
		break;
		case timer_button_pressed:
		{
			KillTimer(timer_button_pressed);

			switch(m_nPressed)
			{
			case PRESSED_LEFT:		
				MoveThumbnail(m_nPressed);
				//Invalidate(FALSE);
				if(m_pDlgThumbnailList->IsFirstPic())
					return;
				break;
			case PRESSED_RIGHT:	
				MoveThumbnail(m_nPressed);
				//Invalidate(FALSE);
				if(!m_pDlgThumbnailList->IsLeftAlign())
					return;
				break;
			default: 
				CRSChildDialog::OnTimer(nIDEvent);
				return;
			}
			//-- Check Next Timer	
			SetTimer(timer_button_pressed, wait_pressed_time/5, NULL);
		}
		break;
	}
	CRSChildDialog::OnTimer(nIDEvent);
}

void CThumbnailDlg::OnPaint()
{	
	UpdateFrames();
	CDialogEx::OnPaint();
}

void CThumbnailDlg::OnSize(UINT nType, int cx, int cy) 
{
    CDialog::OnSize(nType, cx, cy);
	UpdateFrames();	
} 

LRESULT CThumbnailDlg::OnReload(WPARAM w, LPARAM l)
{
	UpdateFrames();
	return 0;
}

void CThumbnailDlg::UpdateFrames()
{
	if(!ISSet())
		return;

	//-- Check Disable
	CheckDisable();

	//-- Rounding Dialog
	CPaintDC dc(this);	

	CRect rect;
	GetClientRect(&rect);	

	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	CString strFilePath;
	CBrush brush = CBrush(RGB(63,63,63));
	mDC.FillRect(rect, &brush);

	//-- Draw Title
/*	m_rect[2] = CRect(CPoint(rect.Width()/2 - 100/2, 4), CSize(100,16));	
	mDC.FillRect(m_rect[2], &CBrush(RGB(63,63,63)));
	DrawGraphicsImage(&mDC, RSGetRoot(_T("img\\03.Live View\\03_thumb_title_bg.png")),m_rect[2]);
	DrawStr(&mDC, STR_SIZE_SMALL, _T("Thumbnail View"), m_rect[2].left + 9, m_rect[2].top + 3, RGB(45,45,45));*/

	//-- Draw Left Button
	switch(m_nStatus[0])
	{
		case BTN_INSIDE:		{ strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_prev_focus.png")));		}	break;
		case BTN_DOWN:		{ strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_Thumb_btn_left_push.png")));		}	break;
		case BTN_DISABLE:		{ strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_Thumb_btn_left_disable.png")));	}	break;
		default:					{ strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_Thumb_btn_left_nor.png")));		}	break;	
	}
	m_rect[0] = CRect(CPoint(15, 25+35), CSize(25,25));	
	DrawGraphicsImage(&mDC, strFilePath,  m_rect[0]);

	//-- Draw Right Button
	switch(m_nStatus[1])
	{
		case BTN_INSIDE:		{ strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_liveview_focus_next_focus.png")));	 }	break;
		case BTN_DOWN:		{ strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_Thumb_btn_right_push.png")));	 }	break;
		case BTN_DISABLE:		{ strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_Thumb_btn_right_disable.png")));	 }	break;
		default:					{ strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_Thumb_btn_right_nor.png")));		 }	break;	
	}	
	m_rect[1] = CRect(CPoint(rect.right - 15 - 25, 25+35), CSize(25,25)); 
	DrawGraphicsImage(&mDC, strFilePath,  m_rect[1]);

	//dh0.seo 2014-11-12
	switch(m_nStatus[2])
	{
		case BTN_INSIDE:		{ strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\02_thumbnail_btn03_focus.png"))); Invalidate(FALSE);}	break;
		case BTN_DOWN:		{ strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03.Live View\\03_thumb_title_bg.png"))); Invalidate(FALSE);}	break;
		case BTN_DISABLE:		{ strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_title_bg.png"))); Invalidate(FALSE);}	break;
		default:					{ strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_thumb_title_bg.png"))); Invalidate(FALSE);}	break;
	}
	//-- Draw Title
	m_rect[2] = CRect(CPoint(rect.Width()/2 - 110/2, 4), CSize(90,20));	
	mDC.FillRect(m_rect[2], &CBrush(RGB(63,63,63)));
	DrawGraphicsImage(&mDC, strFilePath, m_rect[2]);
	DrawStr(&mDC, STR_SIZE_MIDDLE, _T("Open Folder"), m_rect[2].left + 9, m_rect[2].top + 3, RGB(45,45,45));

	//-- Get Draw Rectangle
	CRect rectBody;

	//-- Draw Background
	rectBody = CRect(CPoint(20+25+6,25), CSize(35,91));					strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_Thumb_bg_line_left.png")));		DrawGraphicsImage(&mDC, strFilePath, rectBody );
	rectBody = CRect(CPoint(rect.right-(20+25+6+35),25),CSize(35,91));		strFilePath.Format(RSGetRoot(_T("img\\03.Live View\\03_Thumb_bg_line_right.png")));	DrawGraphicsImage(&mDC, strFilePath, rectBody );
	
	//-- Set Picture Body
	rectBody.top = rect.top + 30;
	rectBody.left	= rect.left + 15+25+6+15+3;
	rectBody.right = rect.right - (15+25+6+15+3) ;
	rectBody.bottom = rect.bottom;;
		
	//-- Thumbnail List
	if(m_pDlgThumbnailList)
	{
		m_pDlgThumbnailList->ShowWindow(SW_SHOW);
		m_pDlgThumbnailList->MoveWindow(rectBody);
		//m_pDlgThumbnailList->Invalidate(FALSE);
	}

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
	Invalidate(FALSE);
}

// CThumbNailDlg message handlers
BOOL CThumbnailDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();
	//-- 2011-07-27 hongsu.jung
	//-- Create Thumbnail List
	m_pDlgThumbnailList = new CThumbNailList();
	m_pDlgThumbnailList->Create(CThumbNailList::IDD, this);	
	m_pDlgThumbnailList->SetParentWnd(this);

	//-- 2011-09-01 jeansu
	//-- Create ThumbnailLoader
	
	//CMiLRe 20141124 섬네일 연사일 경우 여러자 못가지고 오는 버그 수정
	//m_pThumbnailLoader = new CThumbnailLoader();
	//m_pThumbnailLoader->SetThumbnailList(m_pDlgThumbnailList);
	//m_pThumbnailLoader->CreateThread();

	//CMiLRe 20141124 섬네일 로드 쓰레드로 변경
	m_hThread = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)DrawRunner, (PVOID)this, 0, NULL);
	

	//-- Update Frame
	PostMessage(WM_LOAD_THUMBNAIL);

	SetTimer(timer_thumbnail_update,1000, NULL);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

//CMiLRe 20141124 섬네일 로드 쓰레드로 변경
DWORD CThumbnailDlg::DrawRunner(LPVOID Ipvoid)
{
	return ((CThumbnailDlg*)Ipvoid)->Run();
}

//CMiLRe 20141124 섬네일 로드 쓰레드로 변경
int CThumbnailDlg::Run(void)
{
	while(1)
	{
		if(m_bLoadThumbnail)
		{
			LoadThumbnail();
			m_bLoadThumbnail = FALSE;
		}
		Sleep(777);
	}	
	return 0;
}

//------------------------------------------------------------------------------ 
//! @brief		LoadThumbnail()
//! @date		2010-07-07
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbnailDlg::LoadThumbnail(CString strFileName)
{
	if(m_pDlgThumbnailList)
	{
		//dh0.seo 2014-11-11
		m_pDlgThumbnailList->GetCameraIndex(m_nIndex);
		m_pDlgThumbnailList->LoadThumbnail(strFileName);
	}	
}

//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
int CThumbnailDlg::GetThumnailFileCount(CString strFilePath)
{
	if(m_pDlgThumbnailList)
	{
		//dh0.seo 2014-11-11
		m_pDlgThumbnailList->GetCameraIndex(m_nIndex);
		return m_pDlgThumbnailList->GetThumnailFileCount(strFilePath);		
	}	
	return -1;
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CThumbnailDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	for ( int i = 0 ; i < 2 ; i ++)
	{
		if(m_nStatus[i] == BTN_DISABLE) 
			continue;

		if(m_rect[i].PtInRect(point))
		{
			m_pDlgThumbnailList->m_bKey = FALSE;
			CheckButton(i,BTN_DOWN);
			//-- LEFT/RIGHT BUTTON
			MoveThumbnail(i);
			//Invalidate(FALSE);
			//-- Check Pressed
			m_nPressed = i;
			SetTimer(timer_button_pressed, wait_pressed_time, NULL);
			break;
		}	
	}

	CString strRootPath;
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	if(pMainWnd)
		m_strCameraID = pMainWnd->m_SdiMultiMgr.GetDevUniqueID(m_nIndex);
//	strRootPath.Format(_T("%s\\%s"),  RSGetValueStr(RS_OPT_PCSET1_PICTUREPATH), m_strCameraID); //Camera Num
	strRootPath.Format(_T("%s"), RSGetValueStr(RS_OPT_PCSET1_PICTUREPATH));
	//-- Open Explore
	if(m_rect[2].PtInRect(point))
#ifdef	IFA
		ShellExecute(NULL ,_T("open"), RSGetValueStr(0), NULL , NULL, SW_SHOWNORMAL);
#else
		ShellExecute(NULL ,_T("open"), strRootPath, NULL , NULL, SW_SHOWNORMAL);
#endif

	CDialog::OnLButtonDown(nFlags, point);	
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-08-02
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbnailDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_nPressed = PRESSED_NOTHING;
	for ( int i = 0 ; i < 3 ; i ++)
	{
		if(m_nStatus[i] == BTN_DOWN)
			CheckButton(i,BTN_INSIDE);
	}

	CDialog::OnLButtonUp(nFlags, point);	
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CThumbnailDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	LoadThumbnail(FALSE);
	CDialog::OnRButtonDown(nFlags, point);	
}

//------------------------------------------------------------------------------ 
//! @brief		CheckButton
//! @date		2011-08-02
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbnailDlg::CheckButton(int nIndex, int nStatus)
{
	if(m_nStatus[nIndex] != nStatus) 
	{ 
		m_nStatus[nIndex] = nStatus;	
		InvalidateRect(m_rect[nIndex]);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		CheckDisable
//! @date		2011-08-02
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbnailDlg::CheckDisable()
{
	//-- Check Disable
	if(m_pDlgThumbnailList)
	{
		if(m_pDlgThumbnailList->IsDrawAll())
		{
			CheckButton(0,BTN_DISABLE);
			CheckButton(1,BTN_DISABLE);
			return;
		}
		else if(!m_pDlgThumbnailList->IsLeftAlign())
		{
			CheckButton(1,BTN_DISABLE);
			if(m_nStatus[0] == BTN_DISABLE)
				m_nStatus[0] = BTN_NORMAL;
			return;
		}
		else if(m_pDlgThumbnailList->IsFirstPic())
		{
			CheckButton(0,BTN_DISABLE);
			if(m_nStatus[1] == BTN_DISABLE)
				m_nStatus[1] = BTN_NORMAL;
			return;
		}
	}

	if(m_nStatus[0] == BTN_DISABLE)
		m_nStatus[0] = BTN_NORMAL;
	if(m_nStatus[1] == BTN_DISABLE)
		m_nStatus[1] = BTN_NORMAL;
}


//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CThumbnailDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Check Disable
	CheckDisable();

	for ( int i = 0 ; i < 3 ; i ++)
	{
		if(m_nStatus[i] == BTN_DISABLE)
			continue;

		if(m_rect[i].PtInRect(point))
		{
			if((m_pDlgThumbnailList->m_bScrollBasePress == TRUE) && (i == 2))
			{
				continue;
			}
			CheckButton(i,BTN_INSIDE);		
			//m_nStatus[i] = BTN_INSIDE;	
			//InvalidateRect(m_rect[i]);
			MouseEvent();
			return CDialog::OnMouseMove(nFlags, point);
		}
		else
			CheckButton(i,BTN_NORMAL);
	}

	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseLeave
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT CThumbnailDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	//-- Check Disable
	CheckDisable();

	for(int i = 0 ; i < 3 ; i ++)
	{
		if(m_nStatus[i] == BTN_DISABLE)
			continue;
		CheckButton(i,BTN_NORMAL);
	}
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		MouseEvent
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbnailDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

//------------------------------------------------------------------------------ 
//! @brief		MouseEvent
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbnailDlg::MoveThumbnail(BOOL bRight)
{
	if(m_pDlgThumbnailList)
		m_pDlgThumbnailList->OnMoveScroll(bRight, TRUE);
}

//-- 2011-09-01 jeansu : Make ThumbnailLoader Thread

CThumbnailLoader::CThumbnailLoader(void)
{
	m_bThreadFlag	 = TRUE;
	m_pThumbnailList = NULL;
	m_bLoadThumbnail = FALSE;
}
CThumbnailLoader::~CThumbnailLoader(void)
{}

int CThumbnailLoader::Run(void)
{
	while(m_bThreadFlag)
	{
		//CMiLRe 20141124 섬네일 연사일 경우 여러자 못가지고 오는 버그 수정
		if(m_bLoadThumbnail && m_pThumbnailList)
		{
			m_pThumbnailList->LoadThumbnail();
			m_bLoadThumbnail = FALSE;
		}
	}
	m_bAutoDelete = FALSE;
	m_bThreadFlag = FALSE;
	return 0;
}
void CThumbnailLoader::StopThread()
{
	m_bThreadFlag = FALSE;
	int nResult = WaitForSingleObject(this->m_hThread, INFINITE);
}


BOOL CThumbnailDlg::PreTranslateMessage(MSG* pMsg)
{
	return CRSChildDialog::PreTranslateMessage(pMsg);
}
