/////////////////////////////////////////////////////////////////////////////
//
//  CommandDlg.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "RSChildDialog.h"
#include "SdiMultiMgr.h"
#include "RSButton.h"
#include "NXRemoteStudioFunc.h"

// CCommandDlg dialog
class CCommandDlg : public CRSChildDialog
{
	DECLARE_DYNAMIC(CCommandDlg)

public:
	CCommandDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCommandDlg();

// Dialog Data
	enum { IDD = IDD_DLG_COMMANDER_FRM};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();	
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();	
	//CMiLRe 20141119 Sleep Mode 에서 캡쳐
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	DECLARE_MESSAGE_MAP()	
	
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	
	BOOL m_bRecStartStop; //NX1 dh0.seo 2014-08-28
	CSdiMultiMgr* GetMTPMgr();	
	//CMiLRe 20141119 Sleep Mode 에서 캡쳐
	void MouseMoveSet(CPoint point);
private:
	int m_nMin;
	int m_nSec;
	BOOL m_bMF;
	BOOL m_bConnection;
	int		m_nMouseIn;	
	int		m_nShutterStatus;
	int		m_nStatus[BTN_CMD_CNT];
	CRect m_rect[BTN_CMD_CNT];		// 0~3 focus button, 4:Option 5:Liveview 6:Shutter 7:AFMF
	CRect m_rectAFMFBtn;
	CRect m_rectFocusBK;	

	//-- 2011-8-8 Lee JungTaek
	BOOL m_bAFDetect;
	BOOL m_bTimerFinish;
	BOOL m_bTimerStart;

	int m_nRecordTime;
	int m_nCameraMode1;
	int m_nCameraMode2;

	void DrawBackground();	
	void DrawItem(CDC* pDC, int nItem);
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	void ChangeStatus(int nItem, int nStatus, BOOL bRedraw = TRUE);
	void ChangeStatusAll();
	void MouseEvent();
	void SetShutter(int n)		{ m_nShutterStatus	= n ;}
	void DrawRecord();
	
	//-- 2011-6-27 hongsu.jung
	void SetFocusPosition(int nType);
	CString GetSelectDSCTitle();
		
	//NX1 dh0.seo 2014-08-28
	void GetRecordOrder(int nOrder);

	//-- 2011-9-14 Lee JungTaek
	int GetCurMode();
	FocusPosition m_tFocusPosition;
	//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
	BOOL m_bFocusHandelPress;
	void SetFocusPositionValue(int nValue);

	//dh0.seo 2014-11-14
	BOOL SetPosition(CPoint point);

public:
	void GetCaptureOrder(int nOrder);
	void GetRecordStatus();
	void GetCaptureCount();
	int m_nMode;
	BOOL IsMF() { return m_bMF; }
	void SetMF(BOOL b)	{ChangeMF(b);}
	void ChangeMF(BOOL bMF);
	void RedrawIcon(int );
	void CaptureOnce();
	int	 GetShutter()			{ return m_nShutterStatus;}
	BOOL GetConnectStatus()		{ return m_bConnection;}
	
	//-- 2011-8-8 Lee JungTaek
	void SetAFFocus(BOOL bAFDetect) {m_bAFDetect = bAFDetect;}
	void SetTimerFinish(BOOL bTimerFinish) {m_bTimerFinish = bTimerFinish;}
	void SetTimerStart(BOOL bTimerStart) {m_bTimerStart = bTimerStart;}
	void SetSmartPanelEnable();
	void SetDefaultState()	{m_bAFDetect = FALSE; m_bTimerFinish = FALSE; m_bTimerStart = FALSE;}

	//-- 2014-09-24 joonho.kim
	BOOL m_bRecord;
	BOOL m_bEnable;
	void EnableControl(BOOL bEnable) { m_bEnable = bEnable;}
	
	int GetMode(){return m_nMode;}
	CSdiMultiMgr m_SdiMultiMgr;	
	//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
	void SetFocus(FocusPosition* pFocus, BUTTON_COMMAND CMD_TYPE= BTN_CMD_NULL);

	//CMiLRe 20141119 Sleep Mode 에서 캡쳐
	BOOL m_bSetTimerFocus;
	BOOL m_bSetTimerCapture;

	BOOL m_bRecordStatus;
	int m_nCaptureCount;
};
