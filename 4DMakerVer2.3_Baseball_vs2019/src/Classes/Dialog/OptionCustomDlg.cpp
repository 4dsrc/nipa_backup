/////////////////////////////////////////////////////////////////////////////
//
//  OptionCustomDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "OptionCustomDlg.h"
#include "OptionBodyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// COptionCustomDlg dialog
IMPLEMENT_DYNAMIC(COptionCustomDlg, COptionBaseDlg)

//------------------------------------------------------------------------------ 
//! @brief		COptionCustomDlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
COptionCustomDlg::COptionCustomDlg(CWnd* pParent /*=NULL*/)
	: COptionBaseDlg(COptionCustomDlg::IDD, pParent)
{
	m_nISOStep		= 0;	
	m_nAutoISORange	= 0;
	m_nLDC			= 0;
	m_nAFLamp		= 0;
	//CMiLRe 20140902 ISO Expansion 추가
	m_nISOExpansion	= 0;
	//dh0.seo 2014-09-15 Auto Display Off
	m_nAutoDisplay	= 0;

	//CMiLRe 20141114 IOS Auto Range가 Value에서 Enum으로 변경
	m_b3DMode = FALSE;
}

COptionCustomDlg::~COptionCustomDlg()
{
	//CMiLRe 20141113 메모리 릭 제거
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE -1 ; i++)
	{
		if(m_staticLabel[i])
		{
			delete m_staticLabel[i];
			m_staticLabel[i] = NULL;
		}

		if(m_staticLabelBar[i])
		{
			delete m_staticLabelBar[i];
			m_staticLabelBar[i] = NULL;
		}
	}
}

void COptionCustomDlg::DoDataExchange(CDataExchange* pDX)
{
	COptionBaseDlg::DoDataExchange(pDX);		
	DDX_Control(pDX, IDC_CMB_1, m_comboBox[0]);
	DDX_Control(pDX, IDC_CMB_2, m_comboBox[1]);
	DDX_Control(pDX, IDC_CMB_3, m_comboBox[2]);
	DDX_Control(pDX, IDC_CMB_4, m_comboBox[3]);
	//CMiLRe 20140902 ISO Expansion 추가
	DDX_Control(pDX, IDC_CMB_5, m_comboBox[4]);
	//dh0.seo 2014-09-15 Auto Display Off
	DDX_Control(pDX, IDC_CMB_6, m_comboBox[5]);
}				

BEGIN_MESSAGE_MAP(COptionCustomDlg, COptionBaseDlg)	
	ON_CBN_SELCHANGE(IDC_CMB_1, OnSelchangeCmbISOStep		)
	ON_CBN_SELCHANGE(IDC_CMB_2, OnSelchangeCmbAutoISORange	)
	ON_CBN_SELCHANGE(IDC_CMB_3, OnSelchangeCmbLDC			)
	ON_CBN_SELCHANGE(IDC_CMB_4, OnSelchangeCmbAFLamp		)
	//CMiLRe 20140902 ISO Expansion 추가
	ON_CBN_SELCHANGE(IDC_CMB_5, OnSelchangeCmbISOExpansion	)
	//dh0.seo 2014-09-15 Auto Display Off
	ON_CBN_SELCHANGE(IDC_CMB_6, OnSelchangeCmbAutoDisplayOff)
END_MESSAGE_MAP()

// CLiveview message handlers
//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionCustomDlg::CreateFrames()
{
	m_comboBox[0].MoveWindow(160, 28*0 +	5,	150,22);
	m_comboBox[1].MoveWindow(160, 28*1 +	5,	150,22);
//	m_comboBox[2].MoveWindow(160, 28*2 +	5,	150,22);
	m_comboBox[3].MoveWindow(160, 28*2 +	5,	150,22);
	//CMiLRe 20140902 ISO Expansion 추가
	m_comboBox[4].MoveWindow(160, 28*3 +	5,	150,22);
//	m_comboBox[5].MoveWindow(160, 28*5 +	5,	150,22);
	
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE - 1; i++)
	{
		m_staticLabel[i] = new CStatic();
		m_staticLabelBar[i] = new CStatic();
	}
	m_staticLabel[0]->Create(_T("ISO Step"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*0+10, 150, 31*1), this, IDC_STATIC_ETC_1);		
	m_staticLabel[1]->Create(_T("Auto ISO Range"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*1+10, 150, 31*2), this, IDC_STATIC_ETC_2);		
//	m_staticLabel[2]->Create(_T("LDC"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*2+10, 150, 31*3), this, IDC_STATIC_ETC_3);		
	m_staticLabel[3]->Create(_T("AF Lamp"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*2+10, 150, 31*4), this, IDC_STATIC_ETC_4);		
	m_staticLabel[4]->Create(_T("ISO Expansion"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*3+10, 150, 31*5), this, IDC_STATIC_ETC_5);		
//	m_staticLabel[5]->Create(_T("Auto Display Off"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*5+10, 150, 31*6), this, IDC_STATIC_ETC_6);		
	
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE - 1; i++)
	{
		m_staticLabelBar[i]->Create(_T(""),  WS_CHILD|WS_VISIBLE|SS_BLACKRECT | SS_LEFT,  CRect(1,30+28*i, OPTION_DLG_WIDTH, 31+28*i), this );
	}


/*	CWnd *p_tipssoft_combo = GetDlgItem(IDC_CMB_2);
	if(p_tipssoft_combo != NULL){

		// 콤보 박스 내부에 생성된 에디트 컨트롤의 핸들을 얻는다.
		HWND h_find_edit = (HWND)FindWindowEx(p_tipssoft_combo->m_hWnd, NULL, _T("Edit"), NULL);
		if(h_find_edit != NULL){

			// 에디트 컨트롤이 사용하고 있는 기본 윈도우 스타일 정보를 얻는다.
			LONG old_style = ::GetWindowLong(h_find_edit, GWL_STYLE);

			// 왼쪽 정렬 속성(ES_LEFT)을 제거하고 중앙 정렬 속성(ES_CENTER)을 추가한다음 에디트 컨트롤에 설정한다.
			::SetWindowLong(h_find_edit, GWL_STYLE, ES_CENTER | (old_style & ~ES_LEFT));
		}
	} */

	//-- ISO Step
	m_comboBox[0].AddString(_T("1/3 Step"));
	m_comboBox[0].AddString(_T("1 Step"));
		
	//-- LDC
//	m_comboBox[2].AddString(_T("OFF"));
//	m_comboBox[2].AddString(_T("ON"));
	//-- AF Lamp
	m_comboBox[3].AddString(_T("OFF"));
	m_comboBox[3].AddString(_T("High Brightness"));
	m_comboBox[3].AddString(_T("Medium Brightness"));
	m_comboBox[3].AddString(_T("Low Brightness"));

	//-- ISO Expansion
	//CMiLRe 20140902 ISO Expansion 추가
 	m_comboBox[4].AddString(_T("OFF"));
 	m_comboBox[4].AddString(_T("ON"));
		
	//dh0.seo 2014-09-15 Auto Display Off
/*	m_comboBox[5].AddString(_T("Off"));
	m_comboBox[5].AddString(_T("30 sec"));
	m_comboBox[5].AddString(_T("1 min"));
	m_comboBox[5].AddString(_T("3 min"));
	m_comboBox[5].AddString(_T("5 min"));
	m_comboBox[5].AddString(_T("10 min")); */
	
	this->SetBackgroundColor(GetBKColor());
}
//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionCustomDlg::UpdateFrames()
{
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	COLORREF color;
		
	CString strPath = RSGetRoot();
	CString strFullPath;
	
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_line.png"),strPath);
	//-- 2011-07-14 hongsu.jung
	color = GetTextColor();
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("ISO Step"				),15, 28*0 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*1, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Auto ISO Range"		),15, 28*1 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*2, rect.Width(),1);
//	DrawStr(&dc, STR_SIZE_MIDDLE, _T("LDC"					),15, 28*2 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*3, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("AF Lamp"				),15, 28*2 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*4, rect.Width(),1);
	//CMiLRe 20140902 ISO Expansion 추가
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("ISO Expansion"		),15, 28*3 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*5, rect.Width(),1);
	//dh0.seo 2014-09-15 Auto Display Off
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Auto Display Off"		),15, 28*4 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*6, rect.Width(),1);

	//CMiLRe 20140902 DRIVER GET 설정
	if(((COptionBodyDlg*)GetParent())->GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
	{
		m_comboBox[2].EnableWindow(TRUE);
		m_comboBox[4].EnableWindow(FALSE);
	}
	else
	{
		m_comboBox[2].EnableWindow(FALSE);
		m_comboBox[4].EnableWindow(TRUE);
	}
	Invalidate(FALSE);	
}

void COptionCustomDlg::SetOptionProp(int nPropIndex, int nPropValue, RSCustom& opt)
{
	int nEnum = 0;
	nEnum = ChangeIntValueToIndex(nPropIndex, nPropValue);

	switch(nPropIndex)
	{
	case DSC_OPT_CUSTOM_ISOSTEP			:	
		{
			m_nISOStep		= nEnum;	
			UpdateISOExpansion(nEnum);
		}
		break;		
	case DSC_OPT_CUSTOM_AUTOISORANGE	:	m_nAutoISORange	= nEnum;	break;		
	case DSC_OPT_CUSTOM_LDC				:	m_nLDC			= nEnum;	break;		
	case DSC_OPT_CUSTOM_AFLAMP			:	m_nAFLamp		= nEnum;	break;	
	//CMiLRe 20140902 ISO Expansion 추가
	case DSC_OPT_CUSTOM_ISOEXPANSION	:	m_nISOExpansion	= nEnum;	break;
	//dh0.seo 2014-09-15 Auto Display Off
	case DSC_OPT_CUSTOM_AUTODISPLAY		:	m_nAutoDisplay	= nEnum;	break;
	}

	m_comboBox[nPropIndex].SetCurSel(nEnum);

	if(((COptionBodyDlg*)GetParent())->GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
	{
		m_comboBox[2].EnableWindow(TRUE);
		m_comboBox[4].EnableWindow(FALSE);
		m_comboBox[5].EnableWindow(FALSE);
	}
	else
	{
		m_comboBox[2].EnableWindow(FALSE);
		m_comboBox[4].EnableWindow(TRUE);
		m_comboBox[5].EnableWindow(TRUE);
	}	

	UpdateData(FALSE);
}

//CMiLRe 20141114 IOS Auto Range가 Value에서 Enum으로 변경
void COptionCustomDlg::Set3DISORangeShow()
{
	//CMiLRe 20141114 IOS Auto Range가 Value에서 Enum으로 변경
	if(GetLensOption3DMode())
	{
		m_comboBox[0].EnableWindow(FALSE);
		m_comboBox[1].EnableWindow(FALSE);
	}
	else
	{
		m_comboBox[0].EnableWindow(TRUE);
		m_comboBox[1].EnableWindow(TRUE);
	}
	UpdateData(TRUE);
}
void COptionCustomDlg::SetOptionISOStep(int nEnum)
{
	//m_comboBox[0].SetCurSel(nEnum);
}

int COptionCustomDlg::ChangeIntValueToIndex(int nPropIndex, int nPropValue)
{
	int nEnum = 0;

	switch(nPropIndex)
	{
	case DSC_OPT_CUSTOM_ISOSTEP	:
		switch(nPropValue)
		{
		case eUCS_CAP_ISO_STEP_1_3 :		nEnum = DSC_ISOSTEP_STEP1_3	;	break;
		case eUCS_CAP_ISO_STEP_1 :			nEnum =	DSC_ISOSTEP_STEP1	;	break;
		}
		break;	
	case DSC_OPT_CUSTOM_AUTOISORANGE:
		switch(m_nISOStep)
		{
		case DSC_ISOSTEP_STEP1_3:
		//CMiLRe 20141114 IOS Auto Range가 Value에서 Enum으로 변경
		case DSC_ISOSTEP_STEP1	:
			nEnum = nPropValue;
			break;
		}
		break;
	case DSC_OPT_CUSTOM_LDC:
		switch(nPropValue)
		{
		case eUCS_CAP_LDC_OFF	:	nEnum = DSC_LDC_OFF;	break;
		case eUCS_CAP_LDC_ON	:	nEnum = DSC_LDC_ON;	break;
		}
		break;
	case DSC_OPT_CUSTOM_AFLAMP:
		switch(nPropValue)
		{
		case eUCS_CAP_AF_LAMP_OFF	:	nEnum = DSC_AFLAMP_OFF;				break;
		case eUCS_CAP_AF_LAMP_HIGHT	:	nEnum = DSC_AFLAMP_HIGHT;			break;
		case eUCS_CAP_AF_LAMP_MEDIUM:	nEnum = DSC_AFLAMP_MEDIUM;			break;
		case eUCS_CAP_AF_LAMP_LOW	:	nEnum = DSC_AFLAMP_LOW;				break;
		}
		break;
	//CMiLRe 20140902 ISO Expansion 추가
 	case DSC_OPT_CUSTOM_ISOEXPANSION:									
 		switch(nPropValue)
 		{
 		case eUCS_CAP_ISO_EXPANSION_OFF	:	nEnum = DSC_ISOEXPANSION_OFF;	break;
 		case eUCS_CAP_ISO_EXPANSION_ON	:	nEnum = DSC_ISOEXPANSION_ON;	break;
 		}
 		break;
	//dh0.seo 2014-09-15 Auto Display Off
	case DSC_OPT_CUSTOM_AUTODISPLAY:
		switch(nPropValue)
		{
		case eUCS_MENU_AUTO_DISPLAY_OFF_OFF		:	nEnum = DSC_MENU_AUTODISPLAY_OFF;	break;
		case eUCS_MENU_AUTO_DISPLAY_OFF_30SEC	:	nEnum = DSC_MENU_AUTODISPLAY_30SEC;	break;
		case eUCS_MENU_AUTO_DISPLAY_OFF_1MIN	:	nEnum = DSC_MENU_AUTODISPLAY_1MIN;	break;
		case eUCS_MENU_AUTO_DISPLAY_OFF_3MIN	:	nEnum = DSC_MENU_AUTODISPLAY_3MIN;	break;
		case eUCS_MENU_AUTO_DISPLAY_OFF_5MIN	:	nEnum = DSC_MENU_AUTODISPLAY_5MIN;	break;
		case eUCS_MENU_AUTO_DISPLAY_OFF_10MIN	:	nEnum = DSC_MENU_AUTODISPLAY_10MIN;	break;
		}
		break;
	}

	return nEnum;
}

void COptionCustomDlg::SaveProp(RSCustom& opt)
{
	if(!UpdateData(TRUE))
		return;

	opt.nISOStep		= m_nISOStep		;	
	opt.nAutoISORange	= m_nAutoISORange	;
	opt.nLDC			= m_nLDC			;
	opt.nAFLamp			= m_nAFLamp			;
	//CMiLRe 20140902 ISO Expansion 추가
	opt.nISOExpansion	= m_nISOExpansion	;
	//dh0.seo 2014-09-15 Auto Display Off
	opt.nAutoDisplay	= m_nAutoDisplay	;
}

void COptionCustomDlg::UpdateISOExpansion(int nEnum)
{
	m_comboBox[1].ResetContent();

	switch(nEnum)
	{
	case DSC_ISOSTEP_STEP1_3:		
		{
			m_comboBox[1].AddString(_T("125"));
			m_comboBox[1].AddString(_T("160"));
			m_comboBox[1].AddString(_T("200"));
			m_comboBox[1].AddString(_T("250"));
			m_comboBox[1].AddString(_T("320"));
			m_comboBox[1].AddString(_T("400"));
			m_comboBox[1].AddString(_T("500"));
			m_comboBox[1].AddString(_T("640"));
			m_comboBox[1].AddString(_T("800"));
			m_comboBox[1].AddString(_T("1000"));
			m_comboBox[1].AddString(_T("1250"));
			m_comboBox[1].AddString(_T("1600"));
			m_comboBox[1].AddString(_T("2000"));
			m_comboBox[1].AddString(_T("2500"));
			m_comboBox[1].AddString(_T("3200"));
			m_comboBox[1].AddString(_T("4000"));
			m_comboBox[1].AddString(_T("5000"));
			m_comboBox[1].AddString(_T("6400"));
			m_comboBox[1].AddString(_T("8000"));
			m_comboBox[1].AddString(_T("10000"));
			m_comboBox[1].AddString(_T("12800"));
			m_comboBox[1].AddString(_T("16000"));
			m_comboBox[1].AddString(_T("20000"));
			m_comboBox[1].AddString(_T("25600"));

			SetDropDownHeight(&m_comboBox[1], 24);
		}								
		break;							
	case DSC_ISOSTEP_STEP1	:			
		{
			m_comboBox[1].AddString(_T("200"));
			m_comboBox[1].AddString(_T("400"));
			m_comboBox[1].AddString(_T("800"));
			m_comboBox[1].AddString(_T("1600"));
			m_comboBox[1].AddString(_T("3200"));
			m_comboBox[1].AddString(_T("6400"));
			m_comboBox[1].AddString(_T("12800"));
			m_comboBox[1].AddString(_T("25600"));

			SetDropDownHeight(&m_comboBox[1], 8);
		}
		break;
	}

	

}

void COptionCustomDlg::SetDropDownHeight(CNXComboBox* pMyComboBox, int itemsToShow)
{
  //Get rectangles
  CRect rctComboBox, rctDropDown;
  //Combo rect
  pMyComboBox->GetClientRect(&rctComboBox); 
  //DropDownList rect
  pMyComboBox->GetDroppedControlRect(&rctDropDown); 

  //Get Item height
  int itemHeight = pMyComboBox->GetItemHeight(-1); 
  //Converts coordinates
  pMyComboBox->GetParent()->ScreenToClient(&rctDropDown); 
  //Set height
  rctDropDown.bottom = rctDropDown.top + rctComboBox.Height() + itemHeight*itemsToShow; 
  //apply changes
  pMyComboBox->MoveWindow(&rctDropDown); 
}


//------------------------------------------------------------------------------ 
//! @brief		OnSelchangeCmb
//! @date		2011-7-15
//! @author		Lee JungTaek
//! @note	 	Change Value
//------------------------------------------------------------------------------ 
void COptionCustomDlg::OnSelchangeCmbISOStep() 
{
	m_nISOStep = m_comboBox[DSC_OPT_CUSTOM_ISOSTEP].GetCurSel();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_ISOSTEP, m_nISOStep);
}
void COptionCustomDlg::OnSelchangeCmbAutoISORange() 
{
	m_nAutoISORange = m_comboBox[DSC_OPT_CUSTOM_AUTOISORANGE].GetCurSel();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_AUTOISORANGE, m_nAutoISORange);
}
void COptionCustomDlg::OnSelchangeCmbLDC() 
{
	m_nLDC = m_comboBox[DSC_OPT_CUSTOM_LDC].GetCurSel();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_LDC, m_nLDC);
}
void COptionCustomDlg::OnSelchangeCmbAFLamp() 
{
	m_nAFLamp = m_comboBox[DSC_OPT_CUSTOM_AFLAMP].GetCurSel();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_AFLAMP, m_nAFLamp);
}

//CMiLRe 20140902 ISO Expansion 추가
void COptionCustomDlg::OnSelchangeCmbISOExpansion()
{
	m_nISOExpansion = m_comboBox[DSC_OPT_CUSTOM_ISOEXPANSION].GetCurSel();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_ISOEXPANSION, m_nISOExpansion);
}

//dh0.seo 2014-09-15 Auto Display Off
void COptionCustomDlg::OnSelchangeCmbAutoDisplayOff()
{
	m_nAutoDisplay = m_comboBox[DSC_OPT_CUSTOM_AUTODISPLAY].GetCurSel();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_AUTODISPLAY, m_nAutoDisplay);
}

//dh0.seo 2014-10-16
void COptionCustomDlg::SetUpdate()
{
	if(GetMode() == DSC_MODE_SMART || GetMode() > DSC_MODE_M)
	{
		m_comboBox[0].EnableWindow(FALSE);
		m_comboBox[1].EnableWindow(FALSE);
	}
	else
	{
		m_comboBox[0].EnableWindow(TRUE);
		m_comboBox[1].EnableWindow(TRUE);
	}

	UpdateData(FALSE);
}