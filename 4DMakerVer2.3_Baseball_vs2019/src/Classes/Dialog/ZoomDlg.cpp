/////////////////////////////////////////////////////////////////////////////
//
//  ZoomDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-22
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZoomDlg.h"
#include "SRSIndex.h"
//-- For Magnetic Windows
#include "NXRemoteStudioDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CZoomDlg dialog
IMPLEMENT_DYNAMIC(CZoomDlg, CDialogEx)

//------------------------------------------------------------------------------ 
//! @brief		CZoomDlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
CZoomDlg::CZoomDlg(CWnd* pParent /*=NULL*/)
	: CBkDialogST(CZoomDlg::IDD, pParent)
{
	m_bSmartStyle		= FALSE;
	m_pZoomviewFrm	= NULL;
}

CZoomDlg::~CZoomDlg() 
{
	if(m_pZoomviewFrm)
	{
		delete m_pZoomviewFrm;
		m_pZoomviewFrm = NULL;
	}
}
void CZoomDlg::DoDataExchange(CDataExchange* pDX)
{
	CBkDialogST::DoDataExchange(pDX);	
	DDX_Control(pDX,  IDC_BTN_MAXIMIZE	,m_btnMax		);
	DDX_Control(pDX,  IDC_BTN_CANCEL		,m_btnCancel		);
}				

BEGIN_MESSAGE_MAP(CZoomDlg, CBkDialogST)
	ON_WM_NCHITTEST()
	ON_WM_PAINT()
	ON_WM_SIZE()		
	ON_WM_ERASEBKGND()
	ON_WM_GETMINMAXINFO()
	ON_MESSAGE(WM_END_RESIZE_EVENT	,OnEndResizeEvent)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BTN_CANCEL, OnBnClickedBtnCancel)	
	ON_BN_CLICKED(IDC_BTN_MAXIMIZE, OnBnClickedBtnMaximize)	
END_MESSAGE_MAP()

// CLiveview message handlers
void CZoomDlg::OnPaint()
{	
	UpdateFrames();	
	CDialogEx::OnPaint();
}

BOOL CZoomDlg::OnEraseBkgnd(CDC* pDC)	{	return FALSE;}
BOOL CZoomDlg::OnInitDialog()
{
	//-- always on the Top
	SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

	CBkDialogST::OnInitDialog();
	//-- 2011-05-30
	CreateFrames();
	//-- 2011-05-18 hongsu.jung
	UpdateFrames();
	return TRUE;
}

void CZoomDlg::OnSize(UINT nType, int cx, int cy)
{
	CBkDialogST::OnSize(nType, cx, cy);
	//-- 2011-05-18 hongsu.jung
 	UpdateFrames();
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CZoomDlg::UpdateFrames()
{
	//-- Draw Back Ground
	DrawBackGround();
	//-- Max,Cancel Btn
	DrawDialogs();
	//-- Draw Zoom
	DrawZoom();	
	Invalidate(FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CZoomDlg::CreateFrames()
{
	//-- Skin Button
	m_btnMax		.LoadBitmaps(IDR_IMG_MAXIMIZE	,NULL,NULL,NULL);
	m_btnCancel		.LoadBitmaps(IDR_IMG_CANCEL		,NULL,NULL,NULL);

	m_pZoomviewFrm = new CZoomviewFrmDlg();
	m_pZoomviewFrm->Create(CZoomviewFrmDlg::IDD, this);		
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackGround
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
static const int size_title_w				= 7;
static const int size_title_h				= 30;
static const int size_body_w			= 3;

void CZoomDlg::DrawBackGround()
{
	//-- Rounding Dialog
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);	
	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- Draw Round Dialog
	DrawRoundDialogs(&mDC);

	int nImgX, nImgY, nImgW, nImgH;
	CString strPath = RSGetRoot();
	CString strFullPath;
	
	//-- 1. Title Bar
	nImgY		= 0;
	nImgH	= size_title_h;

	nImgX		= 0;			
	nImgW	= size_title_w;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_title_left.png"),strPath);
	DrawImage(&mDC, strFullPath, nImgX, nImgY, nImgW, nImgH);

	nImgX		= size_title_w;
	nImgW	= rect.Width() -  2 * size_title_w ;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_title_center.png"),strPath);
	DrawImage(&mDC, strFullPath, nImgX, nImgY, nImgW, nImgH);

	nImgX		= rect.Width() - size_title_w;
	nImgW	= size_title_w;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_title_right.png"),strPath);
	DrawImage(&mDC, strFullPath, nImgX, nImgY, nImgW, nImgH);

	//-- 2. Body	
	//-- 2. Body	
	nImgY		= size_title_h;
	nImgH	= rect.Height()-size_title_h;			
	//-- 2.1 Top/Left
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_top_left.png"),strPath);
	DrawImage(&mDC, strFullPath, 0, size_title_h, 8, 7);
	//-- 2.2 Top/Center
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_top_center.png"),strPath);
	DrawImage(&mDC, strFullPath, 8, size_title_h, rect.Width() - (8+8), 7);
	//-- 2.3 Top/Right
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_top_right.png"),strPath);
	DrawImage(&mDC, strFullPath, rect.Width()-8, size_title_h, 8, 7);

	//-- 2.4 Top/Left
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_medium_left.png"),strPath);
	DrawImage(&mDC, strFullPath, 0, size_title_h+7, 8, rect.Height() - 7*2 - size_title_h);
	//-- 2.5 Top/Center
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_medium_right.png"),strPath);
	DrawImage(&mDC, strFullPath, rect.Width()-8, size_title_h+7, 8, rect.Height() - 7*2 - size_title_h);

	//-- 2.6 Top/Left
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_bottom_left.png"),strPath);
	DrawImage(&mDC, strFullPath, 0, rect.Height() - 7, 8, 7);
	//-- 2.7 Top/Center
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_bottom_center.png"),strPath);
	DrawImage(&mDC, strFullPath, 8, rect.Height() - 7, rect.Width() - (8+8), 7);
	//-- 2.8 Top/Right
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_bottom_right.png"),strPath);
	DrawImage(&mDC, strFullPath, rect.Width()-8, rect.Height() - 7, 8, 7);
	
	//-- 2011-05-31 hongsu.jung
	DrawDlgTitle(&mDC, _T("Zoom Dialog"));	

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);	
}
//------------------------------------------------------------------------------ 
//! @brief		DrawDialogs
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------
static const int size_btn_size			= 16;
static const int btn_top_margin		= 6;
void CZoomDlg::DrawDialogs()
{	
	if(!ISSet())
		return;

	//-- Top Title.
	CRect rectDialog, rectBtn, rectLiveview, rectToolbar, rectThumbnail, rectMultiview;
	GetClientRect(&rectDialog);

	{	//-- 1. Button
		rectBtn.top		= btn_top_margin;
		rectBtn.bottom	= btn_top_margin + size_btn_size;
		rectBtn.left		= rectDialog.Width() - size_btn_size - btn_top_margin;
		rectBtn.right		= rectDialog.Width() - btn_top_margin;	
		m_btnCancel		.MoveWindow(rectBtn);

		rectBtn.left		= rectBtn.left - size_btn_size - size_body_w;
		rectBtn.right		= rectBtn.right - size_btn_size - size_body_w;
		m_btnMax		.MoveWindow(rectBtn);
	}

	Invalidate(false); 
}

void CZoomDlg::OnBnClickedBtnCancel()		
{ 
	//-- 2011-07-14
	//-- SendMessage Change To Zoom Off
	RSEvent* pMsg		= new RSEvent();
	pMsg->message	= WM_LIVEVIEW_ZOOM_WINDOW;
	pMsg->nParam1		= ZOOM_OFF;
	((CNXRemoteStudioDlg*)AfxGetMainWnd())->m_pDlgLiveview->SendMessage(WM_LIVEVIEW,(WPARAM)pMsg,NULL);

	SendMessage(WM_CLOSE);	
}

void CZoomDlg::OnBnClickedBtnMaximize()	
{ 
	WINDOWPLACEMENT wndpl;
	GetWindowPlacement(&wndpl);
	if( wndpl.showCmd == SW_MAXIMIZE)
	{
		m_btnMax		.LoadBitmaps(IDR_IMG_MAXIMIZE	,NULL,NULL,NULL);
		ShowWindow(SW_NORMAL);	
	}
	else
	{
		m_btnMax		.LoadBitmaps(IDR_IMG_WINNORMAL,NULL,NULL,NULL);
		ShowWindow(SW_MAXIMIZE);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawSelectedImage
//! @date		2011-05-21
//! @author		Lee JungTaek
//! @note	 	Draw Selected Image
//------------------------------------------------------------------------------ 
void CZoomDlg::DrawZoom()
{
	if(!m_pZoomviewFrm)
		return;

	CRect rect;
	GetClientRect(&rect);

	rect.top += size_title_h + size_body_w;
	rect.left += size_title_w;
	rect.right -= size_title_w;
	rect.bottom -= size_title_w;

	m_pZoomviewFrm->MoveWindow(rect);
}

static const int min_frame_size_width	= 400;
static const int min_frame_size_height	= 300;

void CZoomDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = min_frame_size_width;
	lpMMI->ptMinTrackSize.y = min_frame_size_height;

	RECT rcWorkArea;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcWorkArea, SPIF_SENDCHANGE);

	lpMMI->ptMaxSize.x = rcWorkArea.right;
	lpMMI->ptMaxSize.y = rcWorkArea.bottom;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}

LRESULT CZoomDlg::OnNcHitTest(CPoint point) 
{
	LRESULT nHitTest = CWnd::OnNcHitTest(point);
	//if( nHitTest == HTCLIENT)
	if(point.y > 0 && point.y < size_title_h)
		nHitTest = HTCAPTION;
	return nHitTest;
}

LRESULT CZoomDlg::OnEndResizeEvent(WPARAM w, LPARAM l)
{
	int left = HIWORD(w);
	int right = LOWORD(w);

	int top = HIWORD(l);
	int bottom = LOWORD(l);
	MoveWindow(left,top,right-left,bottom-top,1);
	mouse_event( MOUSEEVENTF_LEFTUP,0,0,0,0);
	return 0L;
}

void CZoomDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	CRect rt;
	this->GetWindowRect(&rt);
	if(point.x<5 || point.y<10 || rt.right-5 <point.x || (rt.bottom-rt.top-10 < point.y) || (rt.right-rt.left-10<point.x))
	{
// 		mouse_event(MOUSEEVENTF_LEFTUP,0,0,0,0);
// 		CTransDlg dlg;
// 		dlg.SetWindowRect(rt.left,rt.top,rt.right-rt.left  ,rt.bottom - rt.top);
// 		dlg.SetRestrict(LIVEVIEW_DLG_WIDTH,LIVEVIEW_DLG_HEIGHT);
// 		dlg.DoModal();
	}
	CBkDialogST::OnLButtonDown(nFlags, point);
}