/////////////////////////////////////////////////////////////////////////////
//
//  OptionDSCSet3Dlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "OptionBaseDlg.h"
#include "PopupMsgDlg.h"
#include "NXRemoteStudioFunc.h"

//CMiLRe 20140910 Movie Voice&Size 추가
#define SETDIALOG_ITEM_VIEW_SIZE 6
// COptionDSCSet3Dlg dialog
class COptionDSCSet3Dlg : public COptionBaseDlg
{
	DECLARE_DYNAMIC(COptionDSCSet3Dlg)	
public:
	COptionDSCSet3Dlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionDSCSet3Dlg();

// Dialog Data
	enum { IDD = IDD_DLG_OPT_DSC_SET3 };

protected:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);	
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnSelchangeCmbSysVolume();
	afx_msg void OnSelchangeCmbAFSound();
	afx_msg void OnSelchangeCmbBtnSound();
	//CMiLRe 20140901 E Shutter Sound 추가
	afx_msg void OnSelchangeCmbESHSound();
	//CMiLRe 20140910 Movie Voice&Size 추가
	afx_msg void OnSelchangeCmbVoiceVolume();	
	afx_msg void OnSelchangeCmbMoveiSize();	
	//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
	afx_msg void OnSelSensorCleaning();		
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
	DECLARE_MESSAGE_MAP()

private:	
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();	
	void UpdateFrames();	
	BOOL PtInRect(CPoint pt, BOOL bMove);
	CRect m_rectBtn;
	int m_nStatus;
	COptionBaseDlg* m_pOptDlg[OPT_TAB_CNT];
	int m_nMovieSize;

	CPopupMsgDlg* m_pPopupMsg;

public:	
	//CMiLRe 20140901
	CComboBox	m_comboBox[SETDIALOG_ITEM_VIEW_SIZE];
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	CStatic * m_staticLabel[SETDIALOG_ITEM_VIEW_SIZE];
	CStatic* m_staticLabelBar[SETDIALOG_ITEM_VIEW_SIZE];
	int m_nSystemVol	;
	int m_nAFSound		;
	int m_nBtnSound		;
	//CMiLRe 20140901 E Shutter Sound 추가
	int m_nESHSound		;
	//CMiLRe 20140910 Movie Voice&Size 추가
	int m_nVoiceVolume		;

	//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
	CButton  m_PushBtn;
public:
	void InitProp(RSDSCSet3& opt);
	void SaveProp(RSDSCSet3& opt);

	void SetOptionProp(int nPropIndex, int nPropValue, RSDSCSet3& opt);
	int ChangeIntValueToIndex(int nPropIndex, int nPropValue);

	void LoadPopupMsg(int nType, CString strMsg, int nDelay, RSEvent* pMsg = NULL);
};
