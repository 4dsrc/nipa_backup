/////////////////////////////////////////////////////////////////////////////
//
//  OptionPCSet2Dlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "OptionBaseDlg.h"
#include "NXRemoteStudioFunc.h"

//CMiLRe 20141118 사진 저장 위치 고정 - 카메라 NO별로 저장
#define SETDIALOG_ITEM_VIEW_SIZE 3

// COptionPCSet2Dlg dialog
class COptionPCSet2Dlg : public COptionBaseDlg
{
	DECLARE_DYNAMIC(COptionPCSet2Dlg)	
public:
	COptionPCSet2Dlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionPCSet2Dlg();

// Dialog Data
	enum { IDD = IDD_DLG_OPT_PC_SET2 };

protected:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
//CMiLRe 20141118 사진 저장 위치 고정 - 카메라 NO별로 저장
#ifdef PICTURE_SAVE_PATH_FIXED
	afx_msg void OnPrefixCameraName();		
	afx_msg void OnDateNPrefixCameraName();	
	afx_msg void OnCameraNameDateNPrefix();	
#endif
	afx_msg void OnCancel();	
	afx_msg void OnStart();	
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
//	afx_msg void OnAutoView_Check();
	afx_msg void OnSelchangeView();
	afx_msg void OnSelchangeViewSize();
	afx_msg void OnSelchangeViewState();
	DECLARE_MESSAGE_MAP()
public:
	
private:	
	//-- 2011-07-15 hongsu.jung
	void UpdateFrames();
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();	
	int PtInRect(CPoint pt, BOOL bMove);
	void ResetButton();

public:
	void InitProp(RSPCSet2& opt);
	void SaveProp(RSPCSet2& opt);

	CString m_strPrefix;
	int m_nFileNameType;

	CRect	m_rectBtn[5];	
	int		m_nStatus[2];
	CEdit	m_edit;

	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	CButton  m_PushBtn[SETDIALOG_ITEM_VIEW_SIZE];
	CStatic * m_staticLabel[SETDIALOG_ITEM_VIEW_SIZE];
	CStatic* m_staticLabelBar[SETDIALOG_ITEM_VIEW_SIZE];

	CButton  m_btnAutoView;
	int	m_combo_View;
	CComboBox	m_comboBox_View[3];
	int	m_nCheckView;
	int	m_nSelectView;
	int m_nSelectViewSize;

	void OnChangeState();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};