/////////////////////////////////////////////////////////////////////////////
//
//  OptionPCSet1Dlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "NXRemoteStudio.h"
#include "OptionPCSet1Dlg.h"
#include "OptionBodyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// COptionPCSet1Dlg dialog
IMPLEMENT_DYNAMIC(COptionPCSet1Dlg, COptionBaseDlg)

//------------------------------------------------------------------------------ 
//! @brief		COptionPCSet1Dlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
COptionPCSet1Dlg::COptionPCSet1Dlg(CWnd* pParent /*=NULL*/)
	: COptionBaseDlg(COptionPCSet1Dlg::IDD, pParent)
{
	m_strPictureRootPath	= RSGetRoot();
	m_strBankPath			= RSGetRoot();
	for(int i=0; i<4; i++)
		m_nStatus[i] = BTN_NORMAL;
}

COptionPCSet1Dlg::~COptionPCSet1Dlg()
{
	//CMiLRe 20141113 메모리 릭 제거
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		if(m_staticLabel[i])
		{
			delete m_staticLabel[i];
			m_staticLabel[i] = NULL;
		}

		if(m_staticLabelBar[i])
		{
			delete m_staticLabelBar[i];
			m_staticLabelBar[i] = NULL;
		}
	}
}

void COptionPCSet1Dlg::DoDataExchange(CDataExchange* pDX)
{
	COptionBaseDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptionPCSet1Dlg) 
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_EDIT_1, m_strPictureRootPath	);
	DDX_Text(pDX, IDC_EDIT_2, m_strBankPath			);
	DDX_Control(pDX, IDC_EDIT_1, m_edit[0]);
	DDX_Control(pDX, IDC_EDIT_2, m_edit[1]);
	
}				

BEGIN_MESSAGE_MAP(COptionPCSet1Dlg, COptionBaseDlg)	
	ON_WM_LBUTTONDOWN()	
	ON_WM_MOUSEMOVE()
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	ON_BN_CLICKED(IDC_BTN_OPT_OPEN_FOLDER_1, OnPictureDownloadFilePathFolderOpen	)		
	ON_BN_CLICKED(IDC_BTN_OPT_OPEN_FOLDER_2, OnLoadNSaveBankFilePathFolderOpen	)
	ON_BN_CLICKED(IDC_BTN_OPT_OPEN_FOLDER_3, OnPictureFolderOpen	)	
	ON_BN_CLICKED(IDC_BTN_OPT_APPLY, OnAppy	)		
	ON_BN_CLICKED(IDC_BTN_OPT_CANCEL, OnCancel)		
END_MESSAGE_MAP()

// CLiveview message handlers

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionPCSet1Dlg::CreateFrames()
{
	//-- Folder Open	
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	//CMiLRe 20141125 영문 'g' 아랫 부분 짤리는 버그 수정
	m_edit[0].MoveWindow	(110, 28*1 +	6,	210,22);
//	m_edit[1].MoveWindow(180, 28*2 +	6,	180,22);

	m_PushBtn[0].Create(_T("Find"), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, CRect(CPoint(325, 28*1 + 7),CSize(38,21)), this, IDC_BTN_OPT_OPEN_FOLDER_1);	
//	m_PushBtn[1].Create(_T("Find"), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, CRect(CPoint(370, 28*2 + 5),CSize(38,21)), this, IDC_BTN_OPT_OPEN_FOLDER_2);
	m_PushBtn[4].Create(_T("Open"), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, CRect(CPoint(370, 28*1 + 7),CSize(38,21)), this, IDC_BTN_OPT_OPEN_FOLDER_3);
	m_PushBtn[2].Create(_T("Cancel"), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, CRect(CPoint(8,	28*5 + 2),CSize(60,26)), this, IDC_BTN_OPT_CANCEL);
	m_PushBtn[3].Create(_T("Apply"), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, CRect(CPoint(322+40, 28*5 + 5),CSize(60,26)), this, IDC_BTN_OPT_APPLY);

/*	CString strPath = RSGetRoot();
	CString strFullPath;
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_btn_fold_open_icon_normal.png"),strPath); */

/*	CImage cPngImg;
	if(cPngImg.Load(strFullPath) == S_OK)
	{
		HBITMAP hBitmap = cPngImg.Detach();
		m_PushBtn[0].SetBitmap(hBitmap);
		m_PushBtn[1].SetBitmap(hBitmap);
	} */
	

	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		m_staticLabel[i] = new CStatic();
		m_staticLabelBar[i] = new CStatic();
	}
	m_staticLabel[0]->Create(_T("Path Setting"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*0+10, 200, 31*1), this, IDC_STATIC_ETC_1);		
	m_staticLabel[1]->Create(_T("Picture Path :"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*1+10, 110, 31*2), this, IDC_STATIC_ETC_2);		
//	m_staticLabel[2]->Create(_T("Load / Save Bank Path :"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*2+10, 180, 31*3), this, IDC_STATIC_ETC_3);		
	
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		m_staticLabelBar[i]->Create(_T(""),  WS_CHILD|WS_VISIBLE|SS_BLACKRECT | SS_LEFT,  CRect(1,30+28*i, OPTION_DLG_WIDTH, 31+28*i), this );
	}

	this->SetBackgroundColor(GetBKColor());

	UpdateData(FALSE);
}
//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionPCSet1Dlg::UpdateFrames()
{
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	COLORREF color	= GetTextColor();
		
	CString strPath = RSGetRoot();
	CString strFullPath;
	
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_line.png"),strPath);
	//-- 2011-07-14 hongsu.jung
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Path Setting"						),15, 28*0 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*1, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Picture Download Path :"		),15, 28*1 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*2, rect.Width(),1);	
//	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Load / Save Bank Path :"		),15, 28*2 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*3, rect.Width(),1);
																													DrawGraphicsImage(&dc, strFullPath, 1, 28*4, rect.Width(),1);
	//-- Draw Buttons 	
	switch(m_nStatus[0])
	{
		case BTN_INSIDE:		{ strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_btn_fold_open_icon_focus.png"),strPath);	}	break;		
		default:					{ strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_btn_fold_open_icon_normal.png"),strPath);	 }	break;	
	}	
	DrawGraphicsImage(&dc, strFullPath, m_rectBtn[0]);
	switch(m_nStatus[1])
	{
		case BTN_INSIDE:		{ strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_btn_fold_open_icon_focus.png"),strPath);	}	break;				
		default:					{ strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_btn_fold_open_icon_normal.png"),strPath);	}	break;	
	}	
	DrawGraphicsImage(&dc, strFullPath, m_rectBtn[1]);


	//-- Draw Buttons Background
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_softkey_bg.png"),strPath);
	DrawGraphicsImage(&dc, strFullPath, 0,28*5, rect.Width(),32);

	switch(m_nStatus[2])
	{
		case BTN_INSIDE:		{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png"		),strPath);		color = RGB(255,255,255); }	break;
		case BTN_DOWN:		{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_push.png"			),strPath);		color = RGB(219,219,219); }	break;
		default:					{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png"	),strPath);		color = RGB(219,219,219); }	break;	
	}	
	DrawGraphicsImage(&dc, strFullPath, m_rectBtn[2]);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Cancel"),m_rectBtn[2],color);
	switch(m_nStatus[3])
	{
		case BTN_INSIDE:		{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png"		),strPath);		color = RGB(255,255,255); }	break;
		case BTN_DOWN:		{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_push.png"			),strPath);		color = RGB(219,219,219); }	break;		
		default:					{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png"	),strPath);		color = RGB(219,219,219); }	break;	
	}	
	DrawGraphicsImage(&dc, strFullPath, m_rectBtn[3]);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Apply"),m_rectBtn[3],color);

	Invalidate(FALSE);	
}
	
//------------------------------------------------------------------------------ 
//! @brief		InitProp
//! @date		2011-6-16
//! @author		Lee JungTaek
//! @note	 	Init From File
//------------------------------------------------------------------------------ 
void COptionPCSet1Dlg::InitProp(RSPCSet1& opt)
{
	m_strPictureRootPath =  opt.strPictureRootPath;
	m_strBankPath		 =	opt.strBankPath;

	UpdateData(FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief		SaveProp
//! @date		2011-6-16
//! @author		Lee JungTaek
//! @note	 	Init From File
//------------------------------------------------------------------------------ 
void COptionPCSet1Dlg::SaveProp(RSPCSet1& opt)
{
	if(!UpdateData(TRUE))
		return;

	opt.strPictureRootPath	=   m_strPictureRootPath;
	opt.strBankPath			=	m_strBankPath		;
}



//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int COptionPCSet1Dlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = -1;
	for(int i = 0; i < 4; i ++)
	{
		if(m_rectBtn[i].PtInRect(pt))
		{
			nReturn = i;
			break;
		}
	}
	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionPCSet1Dlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	
	switch(nItem)
	{
		case 0: //-- Open FilePath Dialog
			m_strPictureRootPath = GetDir(TRUE);
			UpdateData(FALSE);
			break;
		case 1: //-- Open FilePath Dialog			
			m_strBankPath = GetDir(FALSE);
			UpdateData(FALSE);
			break;
		case 2:
			//-- Close Windows
			((COptionBodyDlg*)GetParent())->InitOptionProp();
			::SendMessage(GetParent()->GetParent()->m_hWnd,WM_CLOSE,NULL,NULL);
			break;
		case 3:	
			//-- Apply
			((COptionBodyDlg*)GetParent())->SaveOptionProp();
			break;
	}
	CDialog::OnLButtonDown(nFlags, point);	
}

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionPCSet1Dlg::OnPictureDownloadFilePathFolderOpen()
{
	m_strTempPath = m_strPictureRootPath;
	m_strPictureRootPath = GetDir(TRUE);
	UpdateData(FALSE);
}

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionPCSet1Dlg::OnLoadNSaveBankFilePathFolderOpen()
{
	m_strTempPath = m_strBankPath;
	m_strBankPath = GetDir(FALSE);
	UpdateData(FALSE);
}

void COptionPCSet1Dlg::OnPictureFolderOpen()
{
	ShellExecute(NULL ,_T("open"), m_strPictureRootPath, NULL , NULL, SW_SHOWNORMAL);
}

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionPCSet1Dlg::OnAppy()
{
	CString sdfds;
	 m_edit[0].GetWindowTextW(m_strPictureRootPath);
	((COptionBodyDlg*)GetParent())->SaveOptionProp();
}

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionPCSet1Dlg::OnCancel()
{
	((COptionBodyDlg*)GetParent())->InitOptionProp();
	::SendMessage(GetParent()->GetParent()->m_hWnd,WM_CLOSE,NULL,NULL);
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionPCSet1Dlg::OnMouseMove(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);
	if(nItem > -1)
	{
		if(m_nStatus[nItem] != BTN_INSIDE) 
		{ 
			m_nStatus[nItem] = BTN_INSIDE;	
			InvalidateRect(m_rectBtn[nItem]);
		}
	}
	else
		ResetButton();
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseLeave
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT COptionPCSet1Dlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	ResetButton();
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		ResetButton
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionPCSet1Dlg::ResetButton()
{
	for(int i = 0; i < 4; i ++)
	{
		if(m_nStatus[i] != BTN_NORMAL)
		{
			m_nStatus[i] = BTN_NORMAL;	
			InvalidateRect(m_rectBtn[i]);
		}
	}
}
//------------------------------------------------------------------------------ 
//! @brief		GetDir
//! @date		2011-07-15
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
//prevent
#define MAX_BUFFER 519
CString COptionPCSet1Dlg::GetDir(BOOL bPicPath)
{
	BROWSEINFO bi;
	memset(&bi, 0, sizeof(bi));
	CString strPath;

	bi.hwndOwner = this->GetSafeHwnd();
	bi.lpszTitle = _T("Select Folder");
	bi.ulFlags =BIF_NEWDIALOGSTYLE;
	bi.lParam = (LPARAM)(LPCTSTR)m_strTempPath;
	bi.lpfn = BrowseCallbackProc;
	
	::OleInitialize(NULL);
	LPITEMIDLIST pIDL = ::SHBrowseForFolder(&bi);

	if ( pIDL )
	{
		char buffer[MAX_BUFFER] = {'\0'};
		if (::SHGetPathFromIDList(pIDL, (LPWSTR)buffer) != 0)
		{
			strPath.Format(_T("%s"), buffer);
		}
		::OleUninitialize();
		m_strTempPath = strPath;
	}

	if(!strPath.GetLength())
	{
		if(bPicPath)			strPath = m_strPictureRootPath;
		else					strPath = m_strBankPath;
	}

	return strPath;
}

BOOL COptionPCSet1Dlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_RETURN )
		{
			pMsg->wParam = NULL;	
		}
	}
	return COptionBaseDlg::PreTranslateMessage(pMsg);
}
