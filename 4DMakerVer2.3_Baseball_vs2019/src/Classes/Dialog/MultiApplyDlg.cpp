/////////////////////////////////////////////////////////////////////////////
//
//  MultiApplyDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "MultiApplyDlg.h"
#include "afxdialogex.h"
#include "SRSIndex.h"
#include "NXRemoteStudioDlg.h"
#include "SmartPanelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CMultiApplyDlg dialog

static const int first_line_top		= 12;
static const int second_line_top	= 39;
static const int third_line_top		= 66;
static const int fourth_line_top	= 93;

static const int line_main_left		= 13;	
static const int line_name_left		= 46;
static const int line_mode_left		= 151;
static const int line_chkbox_left	= 189;

static const int icon_width			= 33;
static const int name_width			= 113;		

static const int icon_height		= 27;
static const int name_height		= 27;		


IMPLEMENT_DYNAMIC(CMultiApplyDlg,  CBkDialogST)

CMultiApplyDlg::CMultiApplyDlg(CWnd* pParent /*=NULL*/)
	: CBkDialogST(CMultiApplyDlg::IDD, pParent)
{
	m_bSelectAll = FALSE;
	m_pChangeNameEdit = NULL;

	for(int i = 0; i < CAMERA_SIZE; i++)
	{
		m_nCheckBox[i]		= MA_CAMERA_UNCHECK_NULL;
		m_nPreCheckBox[i]	= MA_CAMERA_UNCHECK_NULL;		
		m_bCheckBox[i]		= FALSE;
	}

	m_bEnableSelect = TRUE;
}

CMultiApplyDlg::~CMultiApplyDlg()
{
	TRACE(_T("Destroy:CMultiApplyDlg Start\n"));
	if(m_pChangeNameEdit)
	{
		delete m_pChangeNameEdit;
		m_pChangeNameEdit = NULL;
	}
	DestroyWindow();
	TRACE(_T("Destroy:CMultiApplyDlg End\n"));
}

void CMultiApplyDlg::DoDataExchange(CDataExchange* pDX)
{
	CBkDialogST::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMultiApplyDlg, CBkDialogST)
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()

BOOL CMultiApplyDlg::OnInitDialog() 
{
	SetBitmap(GetBackgroundImage(), RGB(255,255,255));
	SetItemPosition();

	GetCameraInfo();
	//-- 2011-7-20 Lee JungTaek
	//-- Draw
	Invalidate(FALSE);

	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief		GetBackgroundImage
//! @date		2011-7-9
//! @author		Lee JungTaek
//! @note	 	Set Background Image by Connect Device count
//------------------------------------------------------------------------------ 
CString CMultiApplyDlg::GetBackgroundImage()
{
	CString strBGImagePath = _T("");
	
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	int nCnt = pMainWnd->m_SdiMultiMgr.GetPTPCount() - 1;

	if(nCnt > CAMERA_CNT_4)
		nCnt = CAMERA_CNT_4 -1;

	switch(nCnt)
	{
	case CAMERA_CNT_1:
		strBGImagePath = RSGetRoot(_T("img\\02.Smart Panel\\MultiApply_bg\\ma_1ea.png"));
		break;
	case CAMERA_CNT_2:
		strBGImagePath = RSGetRoot(_T("img\\02.Smart Panel\\MultiApply_bg\\ma_2ea.png"));
		break;
	case CAMERA_CNT_3:
		strBGImagePath = RSGetRoot(_T("img\\02.Smart Panel\\MultiApply_bg\\ma_3ea.png"));
		break;
	case CAMERA_CNT_4:
		strBGImagePath = RSGetRoot(_T("img\\02.Smart Panel\\MultiApply_bg\\ma_4ea.png"));
		break;
	}

	return strBGImagePath;
}

//------------------------------------------------------------------------------ 
//! @brief		SetItemPosition
//! @date		2011-7-9
//! @author		Lee JungTaek
//! @note	 	Set Image Position By Line
//------------------------------------------------------------------------------ 
void CMultiApplyDlg::SetItemPosition()
{
	m_rectMainCamera[MA_LINE_FIRST]	= CRect(CPoint(line_main_left, first_line_top), CSize(icon_width,icon_height));	
	m_rectName[MA_LINE_FIRST]		= CRect(CPoint(line_name_left, first_line_top + 10), CSize(name_width,name_height));	
	m_rectMode[MA_LINE_FIRST]		= CRect(CPoint(line_mode_left, first_line_top), CSize(icon_width,icon_height));
	m_rectBtn[MA_LINE_FIRST]		= CRect(CPoint(line_chkbox_left, first_line_top), CSize(icon_width,icon_height));

	m_rectMainCamera[MA_LINE_SECOND]= CRect(CPoint(line_main_left, second_line_top), CSize(icon_width,icon_height));	
	m_rectName[MA_LINE_SECOND]		= CRect(CPoint(line_name_left, second_line_top + 10), CSize(name_width,name_height));	
	m_rectMode[MA_LINE_SECOND]		= CRect(CPoint(line_mode_left, second_line_top), CSize(icon_width,icon_height));
	m_rectBtn[MA_LINE_SECOND]		= CRect(CPoint(line_chkbox_left, second_line_top), CSize(icon_width,icon_height));

	m_rectMainCamera[MA_LINE_THIRD] = CRect(CPoint(line_main_left, third_line_top), CSize(icon_width,icon_height));	
	m_rectName[MA_LINE_THIRD]		= CRect(CPoint(line_name_left, third_line_top + 10), CSize(name_width,name_height));		
	m_rectMode[MA_LINE_THIRD]		= CRect(CPoint(line_mode_left, third_line_top), CSize(icon_width,icon_height));
	m_rectBtn[MA_LINE_THIRD]		= CRect(CPoint(line_chkbox_left, third_line_top), CSize(icon_width,icon_height));

	m_rectMainCamera[MA_LINE_FOURTH]= CRect(CPoint(line_main_left, fourth_line_top), CSize(icon_width,icon_height));	
	m_rectName[MA_LINE_FOURTH]		= CRect(CPoint(line_name_left, fourth_line_top), CSize(name_width,name_height));		
	m_rectMode[MA_LINE_FOURTH]		= CRect(CPoint(line_mode_left, fourth_line_top), CSize(icon_width,icon_height));
	m_rectBtn[MA_LINE_FOURTH]		= CRect(CPoint(line_chkbox_left, fourth_line_top), CSize(icon_width,icon_height));
	
	Image img(GetBackgroundImage());

	m_rectBtn[BTN_MA_CANCLE		]	= CRect(CPoint(18, img.GetHeight() - 24 - 25), CSize(55,25));
	m_rectBtn[BTN_MA_SELECT		]	= CRect(CPoint(86, img.GetHeight() - 24 - 25),  CSize(75,25));
	m_rectBtn[BTN_MA_OK			]	= CRect(CPoint(174, img.GetHeight() - 24 - 25),  CSize(55,25));

	CRect parentRect;
	GetParent()->GetWindowRect(&parentRect);
	
	CheckWindowRect(parentRect.right - img.GetWidth(), parentRect.bottom -  img.GetHeight() - 20, img.GetWidth(), img.GetHeight());
}

void CMultiApplyDlg::OnPaint()
{	
	DrawBackground();	
	CBkDialogST::OnPaint();
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2011-7-9
//! @author		Lee JungTaek
//! @note	 	OnPaint
//------------------------------------------------------------------------------ 
void CMultiApplyDlg::DrawBackground()
{
	CPaintDC dc(this);
	CDC *pDC = &dc;

	for(int i = 0; i < BTN_MA_CNT - 1; i++)
	{
		if(i >= BTN_MA_CNT - 3)
		{
			CBrush brush = RGB_DEFAULT_DLG;
			pDC->FillRect(m_rectBtn[i], &brush);
		}
	}

	DrawLine(MA_LINE_FIRST, &dc);
	DrawLine(MA_LINE_SECOND, &dc);
	DrawLine(MA_LINE_THIRD, &dc);
	DrawLine(MA_LINE_FOURTH, &dc);
	DrawMenu(BTN_MA_CANCLE, &dc);
	DrawMenu(BTN_MA_SELECT, &dc);
	DrawMenu(BTN_MA_OK, &dc);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawLine / DrawMainCamera / DrawName / DrawCheckBox
//! @date		2011-7-9
//! @author		Lee JungTaek
//! @note	 	Main Camera / Name / Mode / Check Box
//------------------------------------------------------------------------------ 
void CMultiApplyDlg::DrawLine(int nLine, CPaintDC* pDC)
{
	DrawMainCamera(nLine, pDC);
	DrawName(nLine, pDC);
	DrawMode(nLine, pDC);
	DrawCheckBox(nLine, pDC);
}

void CMultiApplyDlg::DrawMainCamera(int nLine, CPaintDC* pDC)
{
	int nX, nY, nW, nH;
	nX		= m_rectMainCamera[nLine].left;	
	nY		= m_rectMainCamera[nLine].top;
	nW		= m_rectMainCamera[nLine].Width();
	nH		= m_rectMainCamera[nLine].Height();

	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	int nMainCamera = pMainWnd->GetSelectedItem();

	if(nMainCamera == nLine)
		DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_multiapply_icon_radio.png")), nX, nY, nW, nH);
}

void CMultiApplyDlg::DrawName(int nLine, CPaintDC* pDC)
{
	COLORREF color = RGB(0,0,0);

	switch(m_nCheckBox[nLine])
	{
	case MA_CAMERA_UNCHECK	:	
	case MA_CAMERA_CHECK	:	
	case MA_CAMERA_MAIN		:	color = RGB(255,255,255);	break;
	case MA_CAMERA_DISABLE	:	color = RGB(58,58,58);	break;
	}
	
	//-- Get Center Pos
	CSize szStr;
	szStr = pDC->GetOutputTextExtent(m_strDrawName[nLine]);
	int nX = (m_rectName[nLine].left +  m_rectName[nLine].right) / 2 - szStr.cx / 2;
	int nY = m_rectName[nLine].top;;

	DrawStr(pDC, STR_SIZE_MIDDLE, m_strDrawName[nLine], nX, nY, color);	
}

void CMultiApplyDlg::DrawMode (int nLine, CPaintDC* pDC)
{
	int nX, nY, nW, nH;
	nX		= m_rectMode[nLine].left;	
	nY		= m_rectMode[nLine].top;
	nW		= m_rectMode[nLine].Width();
	nH		= m_rectMode[nLine].Height();
 
	switch(m_nMode[nLine])
	{
	case eUCS_UI_MODE_SMARTAUTO	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\Mode Icon\\02_mode_icon_smart_mode_photo.png")), nX, nY, nW, nH);		break;
	case eUCS_UI_MODE_PROGRAM	:	DrawGraphicsImage(pDC, 	RSGetRoot(_T("img\\02.Smart Panel\\Mode Icon\\02_mode_icon_p.png")), nX, nY, nW, nH);		break;
	case eUCS_UI_MODE_A			:	DrawGraphicsImage(pDC, 	RSGetRoot(_T("img\\02.Smart Panel\\Mode Icon\\02_mode_icon_a.png")), nX, nY, nW, nH);		break;
	case eUCS_UI_MODE_S			:	DrawGraphicsImage(pDC, 	RSGetRoot(_T("img\\02.Smart Panel\\Mode Icon\\02_mode_icon_s.png")), nX, nY, nW, nH);		break;
	case eUCS_UI_MODE_M			:	DrawGraphicsImage(pDC, 	RSGetRoot(_T("img\\02.Smart Panel\\Mode Icon\\02_mode_icon_m.png")), nX, nY, nW, nH);		break;
	}
}

void CMultiApplyDlg::DrawCheckBox (int nLine, CPaintDC* pDC)
{
	int nX, nY, nW, nH;
	nX		= m_rectBtn[nLine].left;	
	nY		= m_rectBtn[nLine].top;
	nW		= m_rectBtn[nLine].Width();
	nH		= m_rectBtn[nLine].Height();

	switch(m_nCheckBox[nLine])
	{
	case MA_CAMERA_UNCHECK	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_multiapply_checkbox_off.png")), nX, nY, nW, nH);		break;
	case MA_CAMERA_CHECK	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_multiapply_checkbox_on.png")), nX, nY, nW, nH);		break;
	case MA_CAMERA_DISABLE	:
	case MA_CAMERA_MAIN		:	break;
	}
}

void CMultiApplyDlg::DrawMenu(int nItem, CPaintDC* pDC)
{
	int nX, nY, nW, nH;
	nX	= m_rectBtn[nItem].left;	
	nY	= m_rectBtn[nItem].top;
	nW	= m_rectBtn[nItem].Width();
	nH	= m_rectBtn[nItem].Height();

	COLORREF color;

	switch(nItem)
	{
	case BTN_MA_CANCLE	:	
		switch(m_nStatus[nItem])
		{
		case PT_MA_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn02_focus.png")),	nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_MA_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn02_push.png")),	nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn02_normal.png")), nX, nY, nW, nH);		color = RGB(219,219,219);	break;		
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Cancel"),m_rectBtn[nItem],color);	
		break;
	case BTN_MA_SELECT	:	
		//-- 2011-8-12 Lee JungTaek
		//-- Exception
		if(!m_bEnableSelect)	
		{
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn02_disable.png")),	nX, nY, nW, nH);	
			color = RGB(25,25,25);
		}
		else
		{
			switch(m_nStatus[nItem])
			{
			case PT_MA_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn02_focus.png")),	nX, nY, nW, nH);	color = RGB(255,255,255);	break;
			case PT_MA_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn02_push.png")),	nX, nY, nW, nH);	color = RGB(255,255,255);	break;
			default:				DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn02_normal.png")), nX, nY, nW, nH);	color = RGB(219,219,219);	break;		
			}
		}	

		if(m_bSelectAll)
			DrawStr(pDC, STR_SIZE_MIDDLE, _T("Cancel All"),m_rectBtn[nItem],color);	
		else
			DrawStr(pDC, STR_SIZE_MIDDLE, _T("Select All"),m_rectBtn[nItem],color);	
		break;
	case BTN_MA_OK		:	
		switch(m_nStatus[nItem])
		{
		case PT_MA_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn02_focus.png")),	nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_MA_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn02_push.png")),	nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn02_normal.png")), nX, nY, nW, nH);		color = RGB(219,219,219);	break;		
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("OK"),m_rectBtn[nItem],color);	
		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		GetCameraInfo
//! @date		2011-7-9
//! @author		Lee JungTaek
//! @note	 	Get Camera Info
//------------------------------------------------------------------------------ 
void CMultiApplyDlg::GetCameraInfo()
{
	//-- Draw Camera Name
	GetCameraNameAll();
	//-- Draw Mode Item
	GetCameraModeAll();
	//-- Draw Check Box
	GetCameraCheckBoxAll();
}

void CMultiApplyDlg::GetCameraNameAll()
{
	CPaintDC dc(this);

	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	int nCnt = pMainWnd->m_SdiMultiMgr.GetPTPCount();

	for(int i = 0; i < nCnt; i++)
	{
		CString strName = pMainWnd->GetPTPName(i);
		m_strName[i] = strName;

		//-- Check Name Lengh
		strName = TrimLongName(strName, m_rectName[i].Width() - 10);
		m_strDrawName[i] = strName;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		TrimLongName
//! @date		2011-7-15
//! @author		Lee JungTaek
//! @note	 	Trim Long Name
//------------------------------------------------------------------------------ 
CString CMultiApplyDlg::TrimLongName(CString strName, int nPixel)
{
	//-- Check Length
	BOOL bCut = FALSE;
	CSize szStr;
	CPaintDC dc(this);
	szStr = dc.GetOutputTextExtent(strName);
	while(szStr.cx > nPixel)
	{
		strName = strName.Left(strName.GetLength()-1);
		szStr = dc.GetOutputTextExtent(strName);		
		bCut = TRUE;
	}

	//-- Cut String
	if(bCut)
	{
		strName = strName.Left(strName.GetLength()-3);
		strName += _T("...");
	}

	return strName;
}

void CMultiApplyDlg::GetCameraModeAll()
{
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	int nCnt = pMainWnd->m_SdiMultiMgr.GetPTPCount();

	for(int i = 0; i < nCnt; i++)
	{
		int nMode = pMainWnd->GetModeIndex(i, TRUE);
		m_nMode[i] = nMode;
	}
}

void CMultiApplyDlg::GetCameraCheckBoxAll()
{
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	int nCnt = pMainWnd->m_SdiMultiMgr.GetPTPCount();

	int nMainCamera = pMainWnd->GetSelectedItem();
	int nCurrentMode = pMainWnd->GetModeIndex(nMainCamera);

	for(int i = 0; i < nCnt; i++)
	{
		if(nMainCamera == i)
			m_nCheckBox[i] = MA_CAMERA_MAIN;	
		else
		{
			if(nCurrentMode == pMainWnd->GetModeIndex(i))
				m_nCheckBox[i] = MA_CAMERA_UNCHECK;
			else
				m_nCheckBox[i] = MA_CAMERA_DISABLE;
		}
	}

	//-- 2011-7-15 Lee JungTaek
	//-- Save Pre Index
	for(int i = 0; i < CAMERA_SIZE; i++)
	{
		m_nPreCheckBox[i] = m_nCheckBox[i];	
	}

	//-- 2011-8-12 Lee JungTaek
	//-- Exception
	int bCheckBox = FALSE;
	for(int i = 0; i < CAMERA_SIZE; i++)
	{
		if(m_nCheckBox[i] == MA_CAMERA_UNCHECK ||m_nCheckBox[i] == MA_CAMERA_CHECK)		
		{
			bCheckBox = TRUE;
			break;
		}
	}	
	m_bEnableSelect = bCheckBox;
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CMultiApplyDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	m_nMouseIn = PtInRect(point, TRUE);
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

void CMultiApplyDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CMultiApplyDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_MA_CNT; i ++)
		ChangeStatus(i,PT_MA_NOR);
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CMultiApplyDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	

	int x = 0 , y = 0;

	switch(nItem)
	{
	case BTN_MA_CHECKBOX1:		
	case BTN_MA_CHECKBOX2:		
	case BTN_MA_CHECKBOX3:		
	case BTN_MA_CHECKBOX4:		
		{
			if(m_bCheckBox[nItem] == TRUE)
			{
				if(m_nCheckBox[nItem] == MA_CAMERA_CHECK)
				{
					m_nCheckBox[nItem] = MA_CAMERA_UNCHECK;
					m_bCheckBox[nItem] = FALSE;
				}				
			}
			else
			{
				if(m_nCheckBox[nItem] == MA_CAMERA_UNCHECK)
				{
					m_nCheckBox[nItem] = MA_CAMERA_CHECK;
					m_bCheckBox[nItem] = TRUE;
				}
			}
			InvalidateRect(m_rectBtn[nItem], TRUE);
		}		
		break;
	case BTN_MA_SELECT	:	
		{
			//-- 2011-8-12 Lee JungTaek
			//-- Exception
			if(!m_bEnableSelect)				return;

			if(m_bSelectAll)
			{
				for(int i = 0; i < CAMERA_SIZE; i++)
				{
					switch(m_nCheckBox[i])
					{
					case MA_CAMERA_UNCHECK	:	
					case MA_CAMERA_CHECK	:	m_nCheckBox[i] = MA_CAMERA_UNCHECK;	break;
					default					:	break;
					}
				}
				
				m_bSelectAll = FALSE;
			}
			else
			{
				for(int i = 0; i < CAMERA_SIZE; i++)
				{
					switch(m_nCheckBox[i])
					{
					case MA_CAMERA_UNCHECK	:	
					case MA_CAMERA_CHECK	:	m_nCheckBox[i] = MA_CAMERA_CHECK;	break;
					default					:	break;
					}
				}

				m_bSelectAll = TRUE;
			}

			Invalidate(TRUE);
		}	
		break;
	case BTN_MA_CANCLE	:	SaveSetting(FALSE);		break;
	case BTN_MA_OK		:	SaveSetting(TRUE);		break;
	}
	CDialog::OnLButtonDown(nFlags, point);	
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CMultiApplyDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ChangeStatusAll();	
	CDialog::OnLButtonUp(nFlags, point);
}

void CMultiApplyDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	CRect rect (line_name_left, first_line_top, name_width, name_height * 4);
	
	int nItem = -1;

	if(rect.PtInRect(point))
	{
		if(m_pChangeNameEdit)
		{
			delete m_pChangeNameEdit;
			m_pChangeNameEdit = NULL;
		}

		nItem = PtInRectName(point);

		if(nItem > PT_MA_IN_NOTHING)
		{
			m_pChangeNameEdit = new CSmartPanelEditDlg(TRUE, nItem, m_strName[nItem]);
			m_pChangeNameEdit->Create(CSmartPanelEditDlg::IDD, this);
			GetWindowRect(&rect);
			CheckWindowRect(rect.left + m_rectName[nItem].left, rect.top + m_rectName[nItem].top, 217 + 6, 84 + 8, m_pChangeNameEdit);
			m_pChangeNameEdit->ShowWindow(SW_SHOW);
		}		
	}
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CMultiApplyDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = PT_MA_IN_NOTHING;
	int nStart = 0;

	for(int i = nStart ; i < BTN_MA_CNT; i ++)
	{
		if(m_rectBtn[i].PtInRect(pt))
		{
			nReturn = i;

			//-- 2011-8-12 Lee JungTaek
			//-- Exception
			if(i == BTN_MA_SELECT)
			{
				if(!m_bEnableSelect)
					return nReturn;
			}
			
			if(bMove)
				ChangeStatus(i,PT_MA_INSIDE);
			else
				ChangeStatus(i,PT_MA_DOWN); 
		}
		else
			ChangeStatus(i,PT_MA_NOR); 
	}
	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRectName
//! @date		2011-7-9
//! @author		Lee JungTaek
//! @note	 	Only Rect Name
//------------------------------------------------------------------------------ 
int CMultiApplyDlg::PtInRectName(CPoint pt)
{
	int nReturn = PT_MA_IN_NOTHING;
	int nStart = 0;

	for(int i = nStart ; i < MA_LINE_CNT; i ++)
	{
		if(m_rectName[i].PtInRect(pt))
			nReturn = i;
	}

	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CMultiApplyDlg::SaveSetting(BOOL bSave)
{
	//-- Save Check Setting
	if(!bSave)
	{
		for(int i = 0; i < CAMERA_SIZE; i++)
			m_nCheckBox[i] = m_nPreCheckBox[i];
	}
	else
	{
		for(int i = 0; i < CAMERA_SIZE; i++)
			m_nPreCheckBox[i] = m_nCheckBox[i];
	}
		
	//-- Set Check Status
	for(int i = 0; i < CAMERA_SIZE; i++)
	{
		switch(m_nCheckBox[i])
		{
		case MA_CAMERA_CHECK :	m_bCheckBox[i] = TRUE;	break;
		case MA_CAMERA_UNCHECK:	m_bCheckBox[i] = FALSE;	break;
		default:	break;			
		}				
	}

	if(bSave)
	{
		CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
		pMainWnd->LoadBankMulti();
	}

	this->ShowWindow(SW_HIDE);
}
//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CMultiApplyDlg::ChangeStatus(int nItem, int nStatus)
{
	if(m_nStatus[nItem] != nStatus)
	{
 		m_nStatus[nItem] = nStatus;
 		CPaintDC dc(this);
 		DrawMenu(nItem, &dc);
  		InvalidateRect(m_rectBtn[nItem],FALSE);
	}
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CMultiApplyDlg::ChangeStatusAll()
{
	int nStart = 0;
	for(int i = nStart; i < BTN_MA_CNT; i ++)
	{
		if(m_nMouseIn == i)
			ChangeStatus(i,PT_MA_INSIDE); 
		else
			ChangeStatus(i,PT_MA_NOR); 
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
BOOL CMultiApplyDlg::IsBankChecked(int nIndex)
{
	switch(m_nCheckBox[nIndex])
	{
	case MA_CAMERA_CHECK	:		return TRUE;
	case MA_CAMERA_MAIN		:	
	case MA_CAMERA_UNCHECK	:	
	case MA_CAMERA_DISABLE	:	
	default:					return FALSE;
	}
	return FALSE;
}
