/////////////////////////////////////////////////////////////////////////////
//
//  CThumbnailPreviewDlg.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	
// @Date	2011-05-21
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudio.h"
#include "ThumbnailPreviewDlg.h"
#include "cv.h"
#include "highgui.h"

// CThumbnailPreviewDlg dialog

IMPLEMENT_DYNAMIC(CThumbnailPreviewDlg, CBkDialogSTEx)

CThumbnailPreviewDlg::CThumbnailPreviewDlg(CWnd* pParent /*=NULL*/)
	: CBkDialogSTEx(CThumbnailPreviewDlg::IDD, pParent)	
{
	m_strImagePath = _T("");
}

CThumbnailPreviewDlg::~CThumbnailPreviewDlg()
{

}

void CThumbnailPreviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CBkDialogSTEx::DoDataExchange(pDX);
	DDX_Control(pDX,  IDC_BTN_MAXIMIZE	,m_btnMax		);
	DDX_Control(pDX,  IDC_BTN_CANCEL		,m_btnCancel		);
//	DDX_Control(pDX, IDC_VIEW_THUMBNAIL_PREVIEW, m_ImagePreview);
}

BEGIN_MESSAGE_MAP(CThumbnailPreviewDlg, CBkDialogSTEx)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BTN_CANCEL, OnBnClickedBtnCancel)	
	ON_BN_CLICKED(IDC_BTN_MAXIMIZE, OnBnClickedBtnMaximize)	
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()

// CLiveview message handlers
BOOL CThumbnailPreviewDlg::OnEraseBkgnd(CDC* pDC)	{	return FALSE;}
void CThumbnailPreviewDlg::OnPaint() 
{
	UpdateFrames();
 	CDialogEx::OnPaint();
}

BOOL CThumbnailPreviewDlg::OnInitDialog()
{
	CBkDialogSTEx::OnInitDialog();
		
	//-- 2011-05-30
	CreateFrames();
	this->SetMagneticWindow(FALSE);

	return TRUE; 	
}

void CThumbnailPreviewDlg::OnSize(UINT nType, int cx, int cy)
{
// 	CDialog::OnSize(nType, cx, cy);
// 	//-- 2011-05-30 Draw Liveview
	CBkDialogSTEx::OnSize(nType, cx, cy);
	UpdateFrames();
}

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbnailPreviewDlg::CreateFrames()
{
	//-- Skin Button
	m_btnMax		.LoadBitmaps(IDR_IMG_MAXIMIZE	,NULL,NULL,NULL);
	m_btnCancel		.LoadBitmaps(IDR_IMG_CANCEL		,NULL,NULL,NULL);
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CThumbnailPreviewDlg::UpdateFrames()
{
	//-- Draw Back Ground
	DrawBackGround();
	//-- Draw Dialogs
	DrawDialogs();
	//-- 2011-6-1 Lee JungTaek
	DrawSelectedImage();
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackGround
//! @date		2010-05-30
//! @author	hongsu.jung
//! PREVIEW_DLG_WIDTH	640
//! PREVIEW_DLG_HEIGHT	480
//------------------------------------------------------------------------------ 
static const int size_title_w				= 7;
static const int size_title_h				= 30;
static const int size_body_w			= 3;

void CThumbnailPreviewDlg::DrawBackGround()
{
	//-- Rounding Dialog
	CPaintDC dc(this);
	//-- Draw Round Dialog
	DrawRoundDialogs(&dc);

	CRect rect;
	GetClientRect(&rect);
	int nImgX, nImgY, nImgW, nImgH;
	CString strPath = RSGetRoot();
	CString strFullPath;

	TRACE(_T("x: %d y: %d w : %d h : %d\n"), rect.left, rect.top, rect.Width(), rect.Height());

	//-- 1. Title Bar
	nImgY		= 0;
	nImgH	= size_title_h;

	nImgX		= 0;			
	nImgW	= size_title_w;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_title_left.png"),strPath);
	DrawImage(&dc, strFullPath, nImgX, nImgY, nImgW, nImgH);

	nImgX		= size_title_w;
	nImgW	= rect.Width() -  2 * size_title_w ;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_title_center.png"),strPath);
	DrawImage(&dc, strFullPath, nImgX, nImgY, nImgW, nImgH);

	nImgX		= rect.Width() - size_title_w;
	nImgW	= size_title_w;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_title_right.png"),strPath);
	DrawImage(&dc, strFullPath, nImgX, nImgY, nImgW, nImgH);

	//-- 2. Body	
	nImgY		= size_title_h;
	nImgH	= rect.Height()-size_title_h;			
	//-- 2.1 Left
	nImgX		= 0;			
	nImgW	= size_body_w;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_menu_bg_left.png"),strPath);
	DrawImage(&dc, strFullPath, nImgX, nImgY, nImgW, nImgH);
	//-- 2.2 Center
	nImgX		= size_body_w;			
	nImgW	= rect.Width() -  2 * size_body_w ;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_menu_bg_center.png"),strPath);
	DrawImage(&dc, strFullPath, nImgX, nImgY, nImgW, nImgH);
	//-- 2.3 Right
	nImgX		= rect.Width() - size_body_w;
	nImgW	= size_body_w;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_menu_bg_right.png"),strPath);
	DrawImage(&dc, strFullPath, nImgX, nImgY, nImgW, nImgH);

	//-- 2011-05-31 hongsu.jung
	CString strTitleName = _T("");
	strTitleName.Format(_T("Preview - %s"), m_strImageName);

	DrawDlgTitle(&dc, _T("Preview"));	
}

//------------------------------------------------------------------------------ 
//! @brief		DrawDialogs
//! @date		2010-05-30
//! @author	hongsu.jung
//! PREVIEW_DLG_WIDTH	640
//! PREVIEW_DLG_HEIGHT	480
//------------------------------------------------------------------------------ 

static const int size_btn_size			= 16;
static const int btn_top_margin		= 6;
static const int liveview_top_margin	= 4;
static const int thumbnail_height		= 125;
static const int toolbar_height			= 58;
static const int multiview_width		= 155;
static const int resize_frame			= 15;

void CThumbnailPreviewDlg::DrawDialogs()
{	
 	if(!m_btnCancel || !m_btnMax)
 		return;

	//-- Top Title.
	CRect rectDialog, rectBtn, rectLiveview, rectToolbar, rectThumbnail, rectMultiview;
	GetClientRect(&rectDialog);


	{	//-- 1. Button
		rectBtn.top		= btn_top_margin;
		rectBtn.bottom	= btn_top_margin + size_btn_size;
		rectBtn.left		= rectDialog.Width() - size_btn_size - btn_top_margin;
		rectBtn.right		= rectDialog.Width() - btn_top_margin;	
		m_btnCancel		.MoveWindow(rectBtn);

		rectBtn.left		= rectBtn.left - size_btn_size - size_body_w;
		rectBtn.right		= rectBtn.right - size_btn_size - size_body_w;
		m_btnMax		.MoveWindow(rectBtn);
	}
	
	Invalidate(false); 
}

void CThumbnailPreviewDlg::OnBnClickedBtnCancel()		{ SendMessage(WM_CLOSE);	}
void CThumbnailPreviewDlg::OnBnClickedBtnMaximize()	
{ 
	WINDOWPLACEMENT wndpl;
	GetWindowPlacement(&wndpl);
	if( wndpl.showCmd == SW_MAXIMIZE)
	{
		m_btnMax		.LoadBitmaps(IDR_IMG_MAXIMIZE	,NULL,NULL,NULL);
		ShowWindow(SW_NORMAL);	
	}
	else
	{
		m_btnMax		.LoadBitmaps(IDR_IMG_WINNORMAL,NULL,NULL,NULL);
		ShowWindow(SW_MAXIMIZE);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawSelectedImage
//! @date		2011-05-21
//! @author		Lee JungTaek
//! @note	 	Draw Selected Image
//------------------------------------------------------------------------------ 
void CThumbnailPreviewDlg::DrawSelectedImage()
{
 	if(!m_strImagePath.GetLength())
		return;

	CPaintDC dc(this);

	CRect rect;
	GetClientRect(&rect);
	int nImgX, nImgY, nImgW, nImgH;

	nImgX		= rect.left;	
	nImgY		= rect.top + size_title_h;
	nImgH	= rect.Height() - size_title_h;			
	nImgW	= rect.Width();
	DrawImage(&dc, m_strImagePath ,nImgX, nImgY, nImgW, nImgH);
}

void CThumbnailPreviewDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = PREVIEW_DLG_WIDTH;
	lpMMI->ptMinTrackSize.y = PREVIEW_DLG_HEIGHT;

	RECT rcWorkArea;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcWorkArea, SPIF_SENDCHANGE);

	lpMMI->ptMaxSize.x = rcWorkArea.right;
	lpMMI->ptMaxSize.y = rcWorkArea.bottom;

	CRSChildDialog::OnGetMinMaxInfo(lpMMI);
}