/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelWBDetailDlg.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-07-01
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudio.h"
#include "SmartPanelWBDetailDlg.h"
#include "afxdialogex.h"
#include "SmartPanelChildDlg.h"
#include "NXRemoteStudioDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define STEP_RGBBOX_X	14
#define STEP_RGBBOX_Y	14
#define RECT_RGBBOX	CRect(CPoint(55, 55), CSize(162, 157))
#define DEFAULT_X_INDEX	7
#define DEFAULT_Y_INDEX	7


// CSmartPanelWBDetailDlg dialog
IMPLEMENT_DYNAMIC(CSmartPanelWBDetailDlg, CDialogEx)

CSmartPanelWBDetailDlg::CSmartPanelWBDetailDlg(CWnd* pParent /*=NULL*/)
	: CBkDialogST(CSmartPanelWBDetailDlg::IDD, pParent)
{
	m_nMouseIn = BTN_WB_NULL;	
	for(int i=0; i<BTN_WB_CNT - 1; i++)
	{
		m_nStatus[i] = PT_WB_NOR;
		m_rect[i] = CRect(0,0,0,0);
	}

	m_bInit = FALSE;
	m_nXIndex = 0;
	m_nYIndex = 0;
	m_nXIndex1st = 0;
	m_nYIndex1st = 0;
}

CSmartPanelWBDetailDlg::~CSmartPanelWBDetailDlg()
{
}

void CSmartPanelWBDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSmartPanelWBDetailDlg, CBkDialogST)
	ON_WM_PAINT()	
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()


BOOL CSmartPanelWBDetailDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();		

	SetBitmap(RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_popup_bg.png")), RGB(255,255,255));

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSmartPanelWBDetailDlg::OnPaint()
{	
	DrawBackground();	
	CRSChildDialog::OnPaint();
}

void CSmartPanelWBDetailDlg::CleanDC(CDC* pDC)
{
/*	for(int i = 0; i < BTN_WB_CNT - 1; i++)
	{
		if(i < BTN_WB_CNT - 3)
		{
			CBrush brush = RGB_BLACK;
			pDC->FillRect(m_rect[i], &brush);
		}
		else
		{
			CBrush brush = RGB_DEFAULT_DLG;
			pDC->FillRect(m_rect[i], &brush);
		}
	}*/
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CSmartPanelWBDetailDlg::DrawBackground()
{
	CPaintDC dc(this);

	//CleanDC(&dc);

	CRect rect;
	GetClientRect(&rect);

	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- Draw Round Dialog
	DrawRoundDialogs(&mDC, TRUE);
	DrawImageBase(&mDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_popup_bg.png")),0, 0);

	//-- Set Position
	m_rect[BTN_WB_UP	]	= CRect(CPoint(121, 10), CSize(25,24));
	m_rect[BTN_WB_DOWN	]	= CRect(CPoint(121,230), CSize(25,24));	
	m_rect[BTN_WB_LEFT	]	= CRect(CPoint(11 ,120), CSize(25,24));	
	m_rect[BTN_WB_RIGHT	]	= CRect(CPoint(231,120), CSize(25,24));
	m_rect[BTN_WB_RESET	]	= CRect(CPoint(8,266),  CSize(55,25));
	m_rect[BTN_WB_OK	]	= CRect(CPoint(204,266), CSize(55,25));
	m_rect[BTN_WB_CURSOR]	= m_rectCursor;
	
	DrawItem(&mDC, BTN_WB_UP		);
	DrawItem(&mDC, BTN_WB_DOWN	);
	DrawItem(&mDC, BTN_WB_LEFT	);
	DrawItem(&mDC, BTN_WB_RIGHT	);
	DrawItem(&mDC, BTN_WB_RESET	);
	DrawItem(&mDC, BTN_WB_OK		);
	DrawItem(&mDC, BTN_WB_CURSOR	);

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

void CSmartPanelWBDetailDlg::DrawItem(CDC* pDC, int nItem)
{
	int nX, nY, nW, nH;
	nX	= m_rect[nItem].left;	
	nY	= m_rect[nItem].top;
	nW	= m_rect[nItem].Width();
	nH	= m_rect[nItem].Height();

	COLORREF color;
	
	switch(nItem)
	{
	case BTN_WB_UP:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_up_focus.png")),	nX, nY, nW, nH);	break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_up_push.png")),		nX, nY, nW, nH);	break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_up_disable.png")),	nX, nY, nW, nH);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_up_normal.png")),	nX, nY, nW, nH);	break;
		}
		break;
	case BTN_WB_DOWN:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_down_focus.png")),		nX, nY, nW, nH);	break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_down_push.png")),		nX, nY, nW, nH);	break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_down_disable.png")),	nX, nY, nW, nH);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_down_normal.png")),		nX, nY, nW, nH);	break;
		}
		break;
	case BTN_WB_LEFT:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_focus.png")),		nX, nY, nW, nH);	break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_push.png")),		nX, nY, nW, nH);	break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_disable.png")),	nX, nY, nW, nH);	break;
		default	:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_normal.png")),		nX, nY, nW, nH);	break;
		}
		break;
	case BTN_WB_RIGHT:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_focus.png")),		nX, nY, nW, nH);	break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_push.png")),		nX, nY, nW, nH);	break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_disable.png")),	nX, nY, nW, nH);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_normal.png")),	nX, nY, nW, nH);	break;
		}
		break;
	case BTN_WB_RESET:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png")),	nX, nY, nW, nH);	color = RGB(255,255,255); break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_push.png")),	nX, nY, nW, nH);	color = RGB(255,255,255); break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_disable.png")),	nX, nY, nW, nH);	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png")),	nX, nY, nW, nH);	color = RGB(219,219,219); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Reset"),m_rect[nItem],color);	
		break;
	case BTN_WB_OK:
		switch(m_nStatus[nItem])
		{
		case PT_WB_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png")),	nX, nY, nW, nH);	color = RGB(255,255,255); break;
		case PT_WB_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_push.png")),	nX, nY, nW, nH);	color = RGB(255,255,255); break;
		case PT_WB_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_disable.png")),	nX, nY, nW, nH);	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png")),	nX, nY, nW, nH);	color = RGB(219,219,219); break;
		}																													
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("OK "),m_rect[nItem],color);	
		break;
	case BTN_WB_CURSOR:
 		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_cursor.png")),		nX, nY, nW, nH);
		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelWBDetailDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	m_nMouseIn = PtInRect(point, TRUE);
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

void CSmartPanelWBDetailDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CSmartPanelWBDetailDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_WB_CNT; i ++)
		ChangeStatus(i,PT_WB_NOR);
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelWBDetailDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	

	int x = 0 , y = 0;

	switch(nItem)
	{
	case BTN_WB_UP		:	
		{
			x = m_rectCursor.left;

			if(m_nYIndex < STEP_RGBBOX_Y)
			{
				y = m_rectCursor.top - 10;
				m_rectCursor.top = y;
				m_nYIndex++;
			}
			else
			{
				y = m_rectCursor.top;
				m_nYIndex = STEP_RGBBOX_Y;
			}
		}
		break;
	case BTN_WB_DOWN	:
		{
			x = m_rectCursor.left;

			if(m_nYIndex > - (STEP_RGBBOX_Y) && m_nYIndex > 0)
			{
				y = m_rectCursor.top + 10;
				m_rectCursor.top = y;
				m_nYIndex--;
			}
			else
			{
				y = m_rectCursor.top;
//				m_nYIndex = - (STEP_RGBBOX_Y);
			}
		}
		break;
	case BTN_WB_LEFT	:		
		{
			if(m_nXIndex > 0)//- (STEP_RGBBOX_X))
			{
				x = m_rectCursor.left - 10;
				m_rectCursor.left = x;
				m_nXIndex--;
			}
			else
			{
				x = m_rectCursor.left;
//				m_nXIndex = - (STEP_RGBBOX_X);
			}

			y = m_rectCursor.top;
		}
		break;
	case BTN_WB_RIGHT	:
		{
			if(m_nXIndex < STEP_RGBBOX_X && m_nXIndex > 0)
			{
				x = m_rectCursor.left + 10;
				m_rectCursor.left = x;
				m_nXIndex++;
			}
			else
			{
				x = m_rectCursor.left;
				m_nXIndex = STEP_RGBBOX_X;
			}	

			y = m_rectCursor.top;
		}
		break;
	case BTN_WB_RESET	:		ResetValue();		break;
	case BTN_WB_OK		:		((CSmartPanelChildDlg*)GetParent())->CloseSmartPanelChild();	return;
	default:					if(SetPosition(point))	SetDetailValue();	return;	//-- Check Enable Rect
	}
	//-- 2011-8-2 Lee JungTaek
	SetDetailValue();

	CDialog::OnLButtonDown(nFlags, point);
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelWBDetailDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ChangeStatusAll();	
	CDialog::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CSmartPanelWBDetailDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = PT_WB_IN_NOTHING;
	int nStart = 0;
	for(int i = nStart ; i < BTN_WB_CNT; i ++)
	{
		if(m_rect[i].PtInRect(pt))
		{
			nReturn = i;
			if(bMove)
				ChangeStatus(i,PT_WB_INSIDE);
			else
				ChangeStatus(i,PT_WB_DOWN); 
		}
		else
			ChangeStatus(i,PT_WB_NOR); 
	}
	return nReturn;
}
//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelWBDetailDlg::ChangeStatus(int nItem, int nStatus)
{
	if(m_nStatus[nItem] != nStatus)
	{
		m_nStatus[nItem] = nStatus;
		CPaintDC dc(this);
		DrawItem(&dc, nItem);
		InvalidateRect(m_rect[nItem],FALSE);
	}
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelWBDetailDlg::ChangeStatusAll()
{
	int nStart = 0;
	for(int i = nStart; i < BTN_WB_CNT; i ++)
	{
		if(m_nMouseIn == i)
			ChangeStatus(i,PT_WB_INSIDE); 
		else
			ChangeStatus(i,PT_WB_NOR); 
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		SetCursorRect
//! @date		2011-7-5
//! @author		Lee JungTaek
//! @note	 	Set Cursor Rect
//------------------------------------------------------------------------------ 
void CSmartPanelWBDetailDlg::SetCursorRect(int x, int y)
{
	m_rectCursor =  CRect(CPoint(x,y), CSize(7,7));

	if(this->GetSafeHwnd())
		InvalidateRect(RECT_RGBBOX, TRUE);		
}

//------------------------------------------------------------------------------ 
//! @brief		SetCursorIndex
//! @date		2011-7-5
//! @author		Lee JungTaek
//! @note	 	Set Cursor Index
//------------------------------------------------------------------------------ 
void CSmartPanelWBDetailDlg::SetCursorIndex(int nValue)
{
	int nX = HIBYTE(nValue);
	int nY = LOBYTE(nValue);

	m_nXIndex = nX;	
	m_nYIndex = nY;

	//-- 2011-8-2 Lee JungTaek
	if(!m_bInit)
	{
		m_nXIndex1st = nX;
		m_nYIndex1st = nY;

		m_bInit = TRUE;
	}	

	nX = 60 + m_nXIndex * 10;
	nY = 200 - (m_nYIndex) * 10;

	SetCursorRect(nX,nY);
}

//------------------------------------------------------------------------------ 
//! @brief		GetDetailPropCode
//! @date		2011-7-28
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CSmartPanelWBDetailDlg::GetDetailPropCode(int nValueIndex)
{
	int nPropCode = 0;
	//CMiLRe 20140903 모델별 WB detail 추가
	CString strModel = ((CNXRemoteStudioDlg*)AfxGetMainWnd())->GetModel();	
	if(strModel.Compare(modelType[MDOEL_NX2000].strModelName) == 0)
	{
		switch(nValueIndex)
		{
		case DSC_WB_DETAIL_AUTO				:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_AUTO				;	break;
		case DSC_WB_DETAIL_DAYLIGHT			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_DAYLIGHT			;	break;
		case DSC_WB_DETAIL_CLOUDY			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_CLOUDY			;	break;
		case DSC_WB_DETAIL_FLURESCENT_W		:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_W		;	break;
		case DSC_WB_DETAIL_FLURESCENT_NW	:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_NW	;	break;
		case DSC_WB_DETAIL_FLURESCENT_D		:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_D		;	break;
		case DSC_WB_DETAIL_TUNGSTEN			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_TUNGSTEN			;	break;
		case DSC_WB_DETAIL_FLASH			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLASH			;	break;
		case DSC_WB_DETAIL_CUSTOM			:	break;
		case DSC_WB_DETAIL_KELVIN			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN			;	break;
		case DSC_WB_DETAIL_AUTO_TUNGSTEN	:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_AUTO_TUNGSTEN	;	break;
		}
	}
	else if(strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)
	{
		switch(nValueIndex)
		{
		case DSC_WB_DETAIL_AUTO_NX1				:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_AUTO				;	break;
		case DSC_WB_DETAIL_AUTO_TUNGSTEN_NX1	:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_AUTO_TUNGSTEN	;	break;
		case DSC_WB_DETAIL_TUNGSTEN_NX1			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_TUNGSTEN			;	break;
		case DSC_WB_DETAIL_DAYLIGHT_NX1			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_DAYLIGHT			;	break;
		case DSC_WB_DETAIL_CLOUDY_NX1			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_CLOUDY			;	break;
		case DSC_WB_DETAIL_FLURESCENT_W_NX1		:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_W		;	break;
		case DSC_WB_DETAIL_FLURESCENT_NW_NX1	:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_NW	;	break;
		case DSC_WB_DETAIL_FLURESCENT_D_NX1		:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLURESCENT_D		;	break;	
		case DSC_WB_DETAIL_FLASH_NX1			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_FLASH			;	break;
		case DSC_WB_DETAIL_CUSTOM_NX1			:	break;
		case DSC_WB_DETAIL_KELVIN_NX1			:	nPropCode = PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN			;	break;	
		}
	}

	m_nPTPCode = nPropCode;
	m_nValueIndex = nValueIndex;

	return nPropCode;
}

//------------------------------------------------------------------------------ 
//! @brief		ResetValue
//! @date		2011-7-29e
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelWBDetailDlg::ResetValue()
{
	m_nXIndex = DEFAULT_X_INDEX;
	m_nYIndex = DEFAULT_Y_INDEX;

	int nX = 60 + m_nXIndex * 10;
	int nY = 200 - (m_nYIndex) * 10;

	SetCursorRect(nX,nY);

	//-- 2011-8-2 Lee JungTaek
	SetDetailValue();
}

//------------------------------------------------------------------------------ 
//! @brief		SetDetailValue
//! @date		2011-7-29
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelWBDetailDlg::SetDetailValue()
{
	int nValue = m_nXIndex;
	nValue = nValue << 8;
	nValue = nValue + m_nYIndex;

	//-- Set Detail Value
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	//CMiLRe 20141029 White Balnace Detail 종류별로 R/W
	pMainWnd->SetPropertyValue(pMainWnd->GetSelectedItem(), PTP_DPC_SAMSUNG_WB_DETAIL_X, nValue);
}

//------------------------------------------------------------------------------ 
//! @brief		SetPosition
//! @date		2011-7-29
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
BOOL CSmartPanelWBDetailDlg::SetPosition(CPoint point)
{
	int nX = point.x;
	int nY = point.y;

	//-- Check Boundary
	if(nX > 55 && nX < 210 && nY > 55 && nY < 205)
	{
		//-- Get Index
		m_nXIndex = (nX - 60) / 10;
		m_nYIndex = (200 - nY) / 10;

		//-- Reposition
		nX	= 60 + m_nXIndex * 10;
		nY	= 200 - (m_nYIndex) * 10; 
		
		return TRUE;
	}	

	return FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief		SetFirstValue
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelWBDetailDlg::SetFirstValue()
{
	CSmartPanelChildDlg *pSmartPanelChild = (CSmartPanelChildDlg*)GetParent();
	
	//-- Set First Values
	m_nXIndex = m_nXIndex1st;
	m_nYIndex = m_nYIndex1st;
	//-- Set Detail Value
	SetDetailValue();
	//-- Set 2 Depth Value
	int nValueIndex = pSmartPanelChild->GetSelected1stValue();
	pSmartPanelChild->SetSelectedPropValue(nValueIndex);
	//-- Close Dialog
	pSmartPanelChild->CloseSmartPanelChild();
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
BOOL CSmartPanelWBDetailDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_ESCAPE)
		{
			SetFirstValue();
			return TRUE;
		}
	}
	else if(pMsg->message == WM_MOUSEWHEEL)		return TRUE;

	return CBkDialogST::PreTranslateMessage(pMsg);
}