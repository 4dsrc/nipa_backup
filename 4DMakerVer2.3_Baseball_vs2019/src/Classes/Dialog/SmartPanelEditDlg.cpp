/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelEditDlg.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-07-01
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudio.h"
#include "SmartPanelEditDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const int max_text_name_len	= 12;
static const int max_bank_name_len	= 20;

// CSmartPanelEditDlg dialog
IMPLEMENT_DYNAMIC(CSmartPanelEditDlg, CDialogEx)

CSmartPanelEditDlg::CSmartPanelEditDlg(int nType, int nIndex, CString strText, CWnd* pParent /*=NULL*/)
	: CBkDialogST(CSmartPanelEditDlg::IDD, pParent)
{
	m_nMouseIn = BTN_EDIT_NULL;	
	for(int i=0; i<BTN_EDIT_CNT - 1; i++)
	{
		m_nStatus[i] = PT_EDIT_NOR;
		m_rect[i] = CRect(0,0,0,0);
	}

	m_nType = nType;
	m_nIndex = nIndex;
	m_strText = strText;
	m_pPopupMsg = NULL;
}

CSmartPanelEditDlg::~CSmartPanelEditDlg()
{
	if(m_pPopupMsg)
	{
		delete m_pPopupMsg;
		m_pPopupMsg = NULL;
	}
}

void CSmartPanelEditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_1, m_strText);
	DDX_Control(pDX, IDC_EDIT_1, m_edit);
}

BEGIN_MESSAGE_MAP(CSmartPanelEditDlg, CBkDialogST)
	ON_WM_PAINT()	
	ON_WM_TIMER()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()

BOOL CSmartPanelEditDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();		

	CString strBKImagePath = _T("");	
	
	m_rect[BTN_EDIT_CANCEL	]	= CRect(CPoint(8,52),  CSize(55,25));
	m_rect[BTN_EDIT_SAVE	]	= CRect(CPoint(154, 52), CSize(55,25));
	this->MoveWindow(0,0, 217, 84);

	m_edit.ModifyStyle(NULL, ES_UPPERCASE);
//	m_edit.ModifyStyle(NULL, NULL);
	m_edit.SetLimitText(max_text_name_len);			
	m_edit.MoveWindow(8,15,200,20);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSmartPanelEditDlg::OnPaint()
{	
	DrawBackground();	
	CRSChildDialog::OnPaint();
}

void CSmartPanelEditDlg::CleanDC(CDC* pDC)
{
	for(int i = 0; i < BTN_EDIT_CNT - 1; i++)
	{
		CBrush brush = RGB_DEFAULT_DLG;
		pDC->FillRect(m_rect[i], &brush);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CSmartPanelEditDlg::DrawBackground()
{
	CPaintDC dc(this);

	//CleanDC(&dc);	

	CRect rect;
	GetClientRect(&rect);

	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- Draw Round Dialog
	DrawRoundDialogs(&mDC, TRUE);
	DrawImageBase(&mDC, RSGetRoot(_T("img\\02.Smart Panel\\02_savebank_popup_bg_user.png")),0, 0);

	DrawItem(&mDC, BTN_EDIT_CANCEL	);
	DrawItem(&mDC, BTN_EDIT_SAVE	);

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawItem
//! @date		2011-7-9
//! @author		Lee JungTaek
//! @note	 	Draw Item
//------------------------------------------------------------------------------ 
void CSmartPanelEditDlg::DrawItem(CDC* pDC, int nItem)
{
	int nLeft = 0;	
	COLORREF color;
	switch(nItem)
	{
	case BTN_EDIT_CANCEL:
		switch(m_nStatus[nItem])
		{
		case PT_EDIT_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png")),		m_rect[nItem].left, m_rect[nItem].top, m_rect[nItem].Width(), m_rect[nItem].Height());	color = RGB(255,255,255); break;
		case PT_EDIT_DOWN	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_push.png")),		m_rect[nItem].left, m_rect[nItem].top, m_rect[nItem].Width(), m_rect[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png")),	m_rect[nItem].left, m_rect[nItem].top, m_rect[nItem].Width(), m_rect[nItem].Height());	color = RGB(219,219,219); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Cancel"),m_rect[nItem],color);	
		break;
	case BTN_EDIT_SAVE:
		switch(m_nStatus[nItem])
		{
		case PT_EDIT_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png")),	m_rect[nItem].left, m_rect[nItem].top, m_rect[nItem].Width(), m_rect[nItem].Height());	color = RGB(255,255,255); break;
		case PT_EDIT_DOWN	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_push.png")),	m_rect[nItem].left, m_rect[nItem].top, m_rect[nItem].Width(), m_rect[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png")),	m_rect[nItem].left, m_rect[nItem].top, m_rect[nItem].Width(), m_rect[nItem].Height());	color = RGB(219,219,219); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Save "),m_rect[nItem],color);	
		break;		
	}
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelEditDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	m_nMouseIn = PtInRect(point, TRUE);
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

void CSmartPanelEditDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CSmartPanelEditDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_EDIT_CNT; i ++)
		ChangeStatus(i,PT_EDIT_NOR);
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelEditDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	

	int x = 0 , y = 0;

	switch(nItem)
	{
	case BTN_EDIT_CANCEL:	DestroyWindow();	break;
	case BTN_EDIT_SAVE	:	
		switch(m_nType)
		{
		case EDIT_SAVEBANK:		SaveBank();			break;
		case EDIT_CHANGENAME:	SaveDSCName();		break;	
		}
		break;
	}
	CDialog::OnLButtonDown(nFlags, point);	
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelEditDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ChangeStatusAll();	
	CDialog::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CSmartPanelEditDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = PT_EDIT_IN_NOTHING;
	int nStart = 0;
	for(int i = nStart ; i < BTN_EDIT_CNT; i ++)
	{
		if(m_rect[i].PtInRect(pt))
		{
			nReturn = i;
			if(bMove)
				ChangeStatus(i,PT_EDIT_INSIDE);
			else
				ChangeStatus(i,PT_EDIT_DOWN); 
		}
		else
			ChangeStatus(i,PT_EDIT_NOR); 
	}
	return nReturn;
}
//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelEditDlg::ChangeStatus(int nItem, int nStatus)
{
	if(m_nStatus[nItem] != nStatus)
	{
		m_nStatus[nItem] = nStatus;
		CPaintDC dc(this);
		DrawItem(&dc, nItem);
		InvalidateRect(m_rect[nItem],TRUE);
	}
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelEditDlg::ChangeStatusAll()
{
	int nStart = 0;
	for(int i = nStart; i < BTN_EDIT_CNT; i ++)
	{
		if(m_nMouseIn == i)
			ChangeStatus(i,PT_EDIT_DOWN); 
		else
			ChangeStatus(i,PT_EDIT_NOR); 
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		SaveDSCName
//! @date		2011-7-9
//! @author		Lee JungTaek
//! @note	 	Save DSC Name
//------------------------------------------------------------------------------ 
void CSmartPanelEditDlg::SaveDSCName()
{
	UpdateData();	

	if(!m_strText.GetLength())
		return;

	RSEvent *pMsg = new RSEvent;
	pMsg->message = WM_RS_SET_DSC_NAME;
	pMsg->nParam1 = m_nIndex;
	pMsg->pParam = (LPARAM)(LPCTSTR)m_strText.MakeUpper();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
	
	DestroyWindow();	
}

//------------------------------------------------------------------------------ 
//! @brief		SaveBank
//! @date		2011-7-11
//! @author		Lee JungTaek
//! @note	 	Save Bank
//------------------------------------------------------------------------------ 
void CSmartPanelEditDlg::SaveBank()
{
	UpdateData();

	if(!m_strText.GetLength())
		return;

	RSEvent *pMsg = new RSEvent;
	pMsg->message = WM_RS_SAVE_BANK;
	pMsg->pParam = (LPARAM)(LPCTSTR)m_strText;

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);

	this->ShowWindow(SW_HIDE);
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-7-15
//! @author		Lee JungTaek
//! @note	 	Key Input
//------------------------------------------------------------------------------ 
BOOL CSmartPanelEditDlg::PreTranslateMessage(MSG* pMsg) 
{
	short shift;
	shift= GetAsyncKeyState(VK_SHIFT);

	if( pMsg->wParam == VK_RETURN)
	{
		UpdateData();

		if(!m_strText.GetLength())
			return FALSE;

		RSEvent *pMsg = new RSEvent;
		pMsg->message = WM_RS_SAVE_BANK;
		pMsg->pParam = (LPARAM)(LPCTSTR)m_strText;

		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);

		this->ShowWindow(SW_HIDE);
	}
	else if(pMsg->wParam == VK_ESCAPE)
	{
		this->ShowWindow(SW_HIDE);
		return FALSE;
	}
	else
	{
		if(pMsg->message == WM_KEYDOWN)
		{
			switch(m_nType)
			{
			case EDIT_SAVEBANK	:	
				{
					//-- 2011-8-24 Lee JungTaek
					//-- Check Disable Char (\/:*?"<>|)
					int nChar = (int)pMsg->wParam;
					if(nChar == 220 || nChar == 191 || nChar == 111 ||	nChar == 186 && shift
						|| nChar == 56 && shift || nChar == 106 || nChar == 191 && shift || nChar == 222 && shift 
						|| nChar == 188 && shift || nChar == 190 && shift || nChar == 220 && shift)						
						return TRUE;
				}
				break;
			case EDIT_CHANGENAME:	
				{
					//-- 2011-7-15 Lee JungTaek
					//-- Check Enable Char (0 ~ 9 , A - Z , _)
					int nChar = (int)pMsg->wParam;
					if(pMsg->wParam == VK_BACK || pMsg->wParam == VK_DELETE 
						||(nChar >= 48 && nChar <= 57)  
						|| (nChar >= 64 && nChar <= 90) 
						|| nChar == 189 && shift)	
						return FALSE;
					else	return TRUE;

				}
				break;
			}
		}
	}

	return CBkDialogST::PreTranslateMessage(pMsg);	
}