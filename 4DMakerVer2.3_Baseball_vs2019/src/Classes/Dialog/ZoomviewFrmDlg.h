/////////////////////////////////////////////////////////////////////////////
//
//  ZoomviewFrmDlg.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "Zoomview.h"
#include "RSChildDialog.h"
#include "NXRemoteStudioFunc.h"

// CZoomviewFrmDlg dialog
class CZoomviewFrmDlg : public CRSChildDialog
{
	DECLARE_DYNAMIC(CZoomviewFrmDlg)

public:
	CZoomviewFrmDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CZoomviewFrmDlg();

// Dialog Data
	enum { IDD = IDD_DLG_BOARD};
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	DECLARE_MESSAGE_MAP()
	
private:
	int m_nShift;
	int m_nRatio;
	CSize m_size;
	CZoomview* m_pZoomView;
	void GetZoomviewRect(LPRECT lpRect);	

public:
	CZoomview* GetZoomview()	{ return m_pZoomView; }
	void DrawZoomview();
	CSize GetSrcSize()				{ return m_size; }
	void SetSrcSize(CSize size)		{ m_size = size; }
	void RotateSrc(BOOL bRotate);//					{ int nTemp = m_size.cx; m_size.cx = m_size.cy; m_size.cy = nTemp; }
	void SetRatio	(LPRECT lpRect);	
	int GetRatio()		{ return m_nRatio; }

};
