#pragma once
/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelDriveDetailDlg.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-07-01
//
/////////////////////////////////////////////////////////////////////////////

// CSmartPanelDriveDetailDlg dialog
#include "BKDialogST.h"
#include "NXRemoteStudioFunc.h"

enum
{
	BTN_DRIVE_DETAIL_NULL = -1,
	BTN_DRIVE_DETAIL_LEFT,
	BTN_DRIVE_DETAIL_RIGHT,
	BTN_DRIVE_DETAIL_RESET,
	BTN_DRIVE_DETAIL_OK,
	BTN_DRIVE_DETAIL_PLUS1,
	BTN_DRIVE_DETAIL_PLUS2,
	BTN_DRIVE_DETAIL_PLUS3,
	BTN_DRIVE_DETAIL_MINUS1,
	BTN_DRIVE_DETAIL_MINUS2,
	BTN_DRIVE_DETAIL_MINUS3,
	BTN_DRIVE_DETAIL_CNT	,
};

enum
{
	DRIVE_DETAIL_TYPE_NULL = -1,
	DRIVE_DETAIL_TYPE_BURST,
	DRIVE_DETAIL_TYPE_TIMER,
};

class CSmartPanelDriveDetailDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CSmartPanelDriveDetailDlg)

public:
	CSmartPanelDriveDetailDlg(int nType, CWnd* pParent = NULL);   // standard constructor
	virtual ~CSmartPanelDriveDetailDlg();

	// Dialog Data
//	enum { IDD = IDD_DLG_SMARTPANEL_WB_COLORTEMP_DETAIL };
	enum { IDD = IDD_DLG_SMARTPANEL_PW_DETAIL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	int m_nPTPCode;
	CString m_strModel;
	//-- Item Info
	int		m_nMouseIn;
	int		m_nStatus[BTN_DRIVE_DETAIL_CNT];
	CRect m_rectBtn[BTN_DRIVE_DETAIL_CNT];
	CRect m_rectIcon;
	CRect m_rectValue;
	CRect m_rectBar;
	CRect m_rectHandler;

	//NX1 dh0.seo 2014-08-25
	CRect m_rectCount;
	CRect m_rectTime;
	CRect m_rectInterval;
	CRect m_rectCountHandler;
	CRect m_rectTimeHandler;
	CRect m_rectIntervalHandler;

	//-- Values
	int m_nDriveDetail;
	int m_nDriveDetailIndex;
	int m_nDriveDetail1st;
	BOOL m_bInit;

	//-- 2011-8-16 Lee JungTaek
	int m_nType;

	//-- Draw Function
	void SetItemPosition();
	void DrawBackground();	
	void CleanDC(CDC* pDC);

	void DrawLine(CDC* pDC);
	void DrawLineBurst(CDC* pDC);
	void DrawLineTimer(CDC* pDC);
	void DrawMenu(int nItem, CDC* pDC);

	//-- Mouse Function
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	void ChangeStatus(int nItem, int nStatus);
	void ChangeStatusAll();
	void MouseEvent();

	//-- Get Information
	void SetBurstValue(BOOL bRight);		
	void SetTimerValue(BOOL bRight);		
	void SetHandler();
	void SetHandlerBurst();
	void SetHandlerTimer();

	//-- Button Input
	void ResetValue();
	void SetDetailValue();
	void SetDetailValueBust(int nItem);
	
	BOOL SetPositionBurst(CPoint point);
	BOOL SetPositionTimer(CPoint point);
	void SetFirstValue();

	void SetValueIndexBurst(int nValue);
	void SetValueIndexTimer(int nValue);

	//NX1 dh0.seo 2014-09-01
	void SetValueIndexTimer_NX1(int nValue);
	void setHandlerTimer_NX1();
	void setHandlerCount_NX1();
	void setHandlerInterval_NX1();
	void SetTimer(BOOL bPlus);
	void SetCount(BOOL bPlus);
	void SetInterval(BOOL bPlus);
	int m_nAllValue;
	int m_nTimerValue;
	int m_nCountValue;
	int m_nIntervalValue;

	//CMiLRe 20140922 PLM P140917-04607 - Reset ��� �߰�
	BOOL	m_bInit_Timer;	
	int m_nTimerValue_1ST;
	int m_nCountValue_1ST;
	int m_nIntervalValue_1ST;


public:
	void SetValueIndex(int nValue);
	void SetPTPCode(int nPTPCode)	{m_nPTPCode = nPTPCode;}
};
