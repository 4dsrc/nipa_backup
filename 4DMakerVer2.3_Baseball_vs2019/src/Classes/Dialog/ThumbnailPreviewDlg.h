/////////////////////////////////////////////////////////////////////////////
//
//  CThumbnailPreviewDlg.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	
// @Date	2011-05-21
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
//#include "BkDialogST.h"
#include "ImageArea.h"
#include "BkDialogSTEx.h"

// CThumbnailPreviewDlg dialog

//class CThumbnailPreviewDlg : public CDialogEx
class CThumbnailPreviewDlg : public CBkDialogSTEx
{
	DECLARE_DYNAMIC(CThumbnailPreviewDlg)

public:
	CThumbnailPreviewDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CThumbnailPreviewDlg();

	// Dialog Data
	enum { IDD = IDD_DLG_THUMBNAIL_PREVIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBnClickedBtnMaximize();
	afx_msg void OnBnClickedBtnCancel();
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	DECLARE_MESSAGE_MAP()

public:
	//CImageArea	m_ImagePreview;
	CString m_strImagePath;
	CString m_strImageName;

public:
	void DrawSelectedImage();
	void SetImagePath(CString strImagePath) {m_strImagePath = strImagePath;}
	void SetImageName(CString strImageName) {m_strImageName = strImageName;}

	void UpdateDlgFrames()	{UpdateFrames();}
	
private:
	CBitmapButton m_btnMax		;
	CBitmapButton m_btnCancel	;

	void CreateFrames();
	void UpdateFrames();
	void DrawBackGround();
	void DrawDialogs();
};
