/////////////////////////////////////////////////////////////////////////////
//
//  OptionTimeLapseDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "OptionBaseDlg.h"
#include "TimerCaptureStausDlg.h"
#include "CommandDlg.h"
#include "NXRemoteStudioFunc.h"
#include "MsgPopupDlg.h"

#define SETDIALOG_ITEM_VIEW_SIZE 5
#define SETDIALOG_ITEM_VIEW_SIZE_NX1 8

// COptionTimeLapseDlg dialog
class COptionTimeLapseDlg : public COptionBaseDlg
{
	DECLARE_DYNAMIC(COptionTimeLapseDlg)	
public:
	COptionTimeLapseDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionTimeLapseDlg();
// Dialog Data
	enum { IDD = IDD_DLG_OPT_TIME_LAPSE };

protected:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	afx_msg void OnDelaySetting();		
	afx_msg void OnIntervalTimer();	
	afx_msg void OnCancel();	
	afx_msg void OnStart();

	afx_msg void OnSelchangeCmbntervalCapture_Start_AMPM();
	afx_msg void OnSelchangeCmbIntervalCapture_Time_Lapse();
	afx_msg void OnIntervalCapture_Start_Check();
//	afx_msg void OnChangeIntervalCapture_Hour(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnChangeIntervalCapture_Minute();
	afx_msg void OnChangeIntervalCapture_Second();
	afx_msg void OnChangeIntervalCapture_Start_Hour();
	afx_msg void OnChangeIntervalCapture_Start_Minute();
	afx_msg void OnChangeIntervalCapture_Count_Edit();

	afx_msg void OnChangeIntervalCountTH();
	afx_msg void OnChangeIntervalCountH();
	afx_msg void OnChangeIntervalCountT();
	afx_msg void OnChangeIntervalCountO();

	//CMiLRe 20141120 인터벌 캡쳐 중 스타트 버튼 못누르게 함
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
	DECLARE_MESSAGE_MAP()	

private:
	CMsgPopupDlg*	m_pMsgPopupDlg;
	//-- 2011-05-18 hongsu.jung
	void UpdateFrames();
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();	
	//-- Check Icon Button Click
	int PtInRect(CPoint pt, BOOL bMove);	
	void ResetButton();
	void ReloadCheckImage();

	int SetTimes(BOOL bSetDelayTime);
	void OnStart_Check();
	//CMiLRe 20140922 Picture Interval Capture 추가
	CString m_strModel;
	int m_nMode;
public:
	void InitProp(RSTimeLapse& opt);
	void SaveProp(RSTimeLapse& opt);
	void UpdateCapturedCnt(int nCnt)	{if(m_pTimerCaptureStatus) m_pTimerCaptureStatus->UpdateCapturedCnt(nCnt);}
	void UpdateRemainTime(int nTime)	{if(m_pTimerCaptureStatus) m_pTimerCaptureStatus->UpdateRemainTime(nTime);}
	void CloseStatusDialog()			{if(m_pTimerCaptureStatus) m_pTimerCaptureStatus->DestroyWindow();}
	void IntervalCapture();

public:		
	BOOL m_bDelaySet;	
	BOOL m_bIntervalTimer;
	CRect m_rectBtn[4];	
	int	m_nStatus[2];


	int m_nDelayTimeMin;
	int m_nDelayTimeSec;
	int m_nTimerMin;
	int m_nTimerSec;
	int m_nCaptureTime;	
	CEdit m_edit[5];	

	//-- 2011-7-16 Lee JungTaek
	CTimerCaptureStausDlg* m_pTimerCaptureStatus;

	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	CButton  m_PushBtn[4];
	CStatic * m_staticLabel[SETDIALOG_ITEM_VIEW_SIZE+2];
	CStatic* m_staticLabelBar[SETDIALOG_ITEM_VIEW_SIZE];


	int m_nIntervalCapture_Hour;
	int m_nIntervalCapture_Minute;
	int m_nIntervalCapture_Second;
	int m_nIntervalCapture_Count;	
	int m_nIntervalCapture_Start_Hour;
	int m_nIntervalCapture_Start_Minute;
	int m_nIntervalCapture_Start_AMPM;
	int m_nIntervalCapture_Time_Lapse;
//	CString m_strIntervalCapture_Hour;
//	CString m_strIntervalCapture_Minute;
//	CString m_strIntervalCapture_Second;
	CString m_strIntervalCapture_Count;
//	CString m_strIntervalCapture_Start_Hour;
//	CString m_strIntervalCapture_Start_Minute;
	int m_nIntervalCapture_Second_;

	int m_nIntervalCapture_CountTH;
	int m_nIntervalCapture_CountH;
	int m_nIntervalCapture_CountT;
	int m_nIntervalCapture_CountO;

	BOOL m_bMinute;

	CEdit			m_editCount_NX1[6];
	CButton  m_PushBtn_NX1;
	CComboBox	m_comboBox_NX1[11];

	CStatic * m_staticLabel_NX1[SETDIALOG_ITEM_VIEW_SIZE_NX1];

	void SetOptionProp(int nPropIndex, int nPropValue, RSTimeLapse& opt);
	int ChangeIntValueToIndex(int nPropIndex, int nPropValue);
	int SetDetailValue();
	int SetDetailCount();

	//CMiLRe 20140922 Picture Interval Capture 추가
	CString GetModel(){return m_strModel;}
	void SetModel(CString strModel) {m_strModel = strModel;}

	//CMiLRe 20140922 Picture Interval Capture 추가
	void SetModelTypeDisplay(CString strModel, BOOL bChange = FALSE);
	BOOL m_bChangedDP;

	void SetMode(int nMode) {m_nMode = nMode;}
	int GetMode(){return m_nMode;}
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
