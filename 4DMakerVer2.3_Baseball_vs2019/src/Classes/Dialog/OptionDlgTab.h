/////////////////////////////////////////////////////////////////////////////
//
//  OptionDlgTab.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "RSChildDialog.h"

// COptionDlgTab dialog
class COptionDlgTab : public CRSChildDialog
{
	DECLARE_DYNAMIC(COptionDlgTab)	
public:
	COptionDlgTab(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionDlgTab();

// Dialog Data
	enum { IDD = IDD_DLG_BOARD};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();	
	DECLARE_MESSAGE_MAP()
public:
	
private:	
	//-- 2011-05-18 hongsu.jung
	void UpdateFrames();
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();
	void DrawBackGround();	
};