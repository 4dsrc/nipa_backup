/////////////////////////////////////////////////////////////////////////////
//
//  OptionDSCSet3Dlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "OptionDSCSet3Dlg.h"
#include "OptionBodyDlg.h"
#include "OptionDSCSetEtcDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//COptionDSCSetEtcDlg* m_pOptionDSCSetEtcDlg = new COptionDSCSetEtcDlg();
// COptionDSCSet3Dlg dialog
IMPLEMENT_DYNAMIC(COptionDSCSet3Dlg, COptionBaseDlg)

//------------------------------------------------------------------------------ 
//! @brief		COptionDSCSet3Dlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
COptionDSCSet3Dlg::COptionDSCSet3Dlg(CWnd* pParent /*=NULL*/)
	: COptionBaseDlg(COptionDSCSet3Dlg::IDD, pParent)
{
	m_nStatus = BTN_NORMAL;
	//m_pOptionDSCSetEtcDlg = NULL;
	m_pPopupMsg = NULL;
}

COptionDSCSet3Dlg::~COptionDSCSet3Dlg() 
{
	if(m_pPopupMsg)
	{
		delete m_pPopupMsg;
		m_pPopupMsg = NULL;
	}
	/*
	if(m_pOptionDSCSetEtcDlg)
	{
		delete m_pOptionDSCSetEtcDlg;
		m_pOptionDSCSetEtcDlg = NULL;
	}
	*/
	//CMiLRe 20141113 메모리 릭 제거
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		if(m_staticLabel[i])
		{
			delete m_staticLabel[i];
			m_staticLabel[i] = NULL;
		}

		if(m_staticLabelBar[i])
		{
			delete m_staticLabelBar[i];
			m_staticLabelBar[i] = NULL;
		}
	}
}
void COptionDSCSet3Dlg::DoDataExchange(CDataExchange* pDX)
{
	COptionBaseDlg::DoDataExchange(pDX);		
	DDX_Control(pDX, IDC_CMB_1, m_comboBox[0]);
	DDX_Control(pDX, IDC_CMB_2, m_comboBox[1]);
	DDX_Control(pDX, IDC_CMB_3, m_comboBox[2]);
	//CMiLRe 20140901 E Shutter Sound 추가
//	DDX_Control(pDX, IDC_CMB_4, m_comboBox[3]);
	//CMiLRe 20140910 Movie Voice&Size 추가
	DDX_Control(pDX, IDC_CMB_4, m_comboBox[3]);
	DDX_Control(pDX, IDC_CMB_5, m_comboBox[4]);

	DDX_CBIndex(pDX,IDC_CMB_1, m_nSystemVol	);
	DDX_CBIndex(pDX,IDC_CMB_2, m_nAFSound	);
	DDX_CBIndex(pDX,IDC_CMB_3, m_nBtnSound	);
	//CMiLRe 20140901 E Shutter Sound 추가
//	DDX_CBIndex(pDX,IDC_CMB_4, m_nESHSound	);
	//CMiLRe 20140910 Movie Voice&Size 추가
	DDX_CBIndex(pDX,IDC_CMB_4, m_nVoiceVolume	);
	DDX_CBIndex(pDX,IDC_CMB_5, m_nMovieSize	);
	//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
	DDX_Control(pDX, IDC_BUTTON1, m_PushBtn);
	
}				

BEGIN_MESSAGE_MAP(COptionDSCSet3Dlg, COptionBaseDlg)
	ON_WM_LBUTTONDOWN()	
	ON_WM_MOUSEMOVE()
	ON_CBN_SELCHANGE(IDC_CMB_1, OnSelchangeCmbSysVolume	)
	ON_CBN_SELCHANGE(IDC_CMB_2, OnSelchangeCmbAFSound	)
	ON_CBN_SELCHANGE(IDC_CMB_3, OnSelchangeCmbBtnSound	)
	//CMiLRe 20140901 E Shutter Sound 추가
//	ON_CBN_SELCHANGE(IDC_CMB_4, OnSelchangeCmbESHSound	)	
	//CMiLRe 20140910 Movie Voice&Size 추가
	ON_CBN_SELCHANGE(IDC_CMB_4, OnSelchangeCmbVoiceVolume	)	
	ON_CBN_SELCHANGE(IDC_CMB_5, OnSelchangeCmbMoveiSize	)	
	//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
	ON_BN_CLICKED(IDC_BUTTON1, OnSelSensorCleaning)
END_MESSAGE_MAP()

// CLiveview message handlers

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionDSCSet3Dlg::CreateFrames()
{
	m_comboBox[0].MoveWindow(160, 28*0 +	5,	150,22);
	m_comboBox[1].MoveWindow(160, 28*1 +	5,	150,22);
	m_comboBox[2].MoveWindow(160, 28*2 +	5,	150,22);
	//CMiLRe 20140901 E Shutter Sound 추가
	m_comboBox[3].MoveWindow(160, 28*3 +	5,	150,22);
	//CMiLRe 20140910 Movie Voice&Size 추가
	m_comboBox[4].MoveWindow(160, 28*4 +	5,	150,22);
//	m_comboBox[5].MoveWindow(160, 28*5 +	5,	150,22);

	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		m_staticLabel[i] = new CStatic();
		m_staticLabelBar[i] = new CStatic();
	}
	m_staticLabel[0]->Create(_T("System Volume"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*0+10, 150, 31*1), this, IDC_STATIC_ETC_1);		
	m_staticLabel[1]->Create(_T("AF Sound"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*1+10, 150, 31*2), this, IDC_STATIC_ETC_2);		
	m_staticLabel[2]->Create(_T("Button Sound"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*2+10, 150, 31*3), this, IDC_STATIC_ETC_3);		
//	m_staticLabel[3]->Create(_T("E Shutter Sound"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*3+10, 150, 31*4), this, IDC_STATIC_ETC_4);		
	m_staticLabel[3]->Create(_T("Voice Volume"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*3+10, 150, 31*5), this, IDC_STATIC_ETC_5);		
	m_staticLabel[4]->Create(_T("Movie Size"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*4+10, 150, 31*6), this, IDC_STATIC_ETC_6);
	//dh0.seo 2014-11-04
	//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
	m_staticLabel[5]->Create(_T("Sensor Cleaning"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*5+10, 150, 31*7), this, IDC_STATIC_ETC_7);
	
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		m_staticLabelBar[i]->Create(_T(""),  WS_CHILD|WS_VISIBLE|SS_BLACKRECT | SS_LEFT,  CRect(1,30+28*i, OPTION_DLG_WIDTH, 31+28*i), this );
	}

	//-- System Volume
	m_comboBox[0].AddString(_T("Off"));
	m_comboBox[0].AddString(_T("Low"));
	m_comboBox[0].AddString(_T("Medium"));
	m_comboBox[0].AddString(_T("High"));
	//-- AF Sound
	m_comboBox[1].AddString(_T("Off"));
	m_comboBox[1].AddString(_T("On"));
	//-- Button Sound
	m_comboBox[2].AddString(_T("Off"));
	m_comboBox[2].AddString(_T("On"));

	//CMiLRe 20140901 E Shutter Sound 추가
//	m_comboBox[3].AddString(_T("Off"));
//	m_comboBox[3].AddString(_T("On"));

	//CMiLRe 20140910 Movie Voice&Size 추가
	m_comboBox[3].AddString(_T("Off"));
	m_comboBox[3].AddString(_T("On"));

	//CMiLRe 20140910 Movie Voice&Size 추가
	for(int i = 0; i < ARRAYSIZE(SZOptDSCSet3MoveSizeVale_NX1); i++)
	{
		m_comboBox[4].AddString(SZOptDSCSet3MoveSizeVale_NX1[i].strPropertyValue);
	}	

	//-- Sensor Cleaning
	//CMiLRe 20140901 E Shutter Sound 추가
//	m_rectBtn = CRect(CPoint(0, 28*6+1), CSize(386,27));
	//dh0.seo 2014-11-04
	m_rectBtn = CRect(160, 28*5+5,	150,22);	
	
	//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
	m_PushBtn.MoveWindow(160, 28*5+5,	150,22);
	m_PushBtn.SetWindowText(_T("Start"));

	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	this->SetBackgroundColor(GetBKColor());

}
//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionDSCSet3Dlg::UpdateFrames()
{
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	COLORREF color;
		
	CString strPath = RSGetRoot();
	CString strFullPath;
	
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_line.png"),strPath);
	//-- 2011-07-14 hongsu.jung
	color = GetTextColor();

	DrawStr(&dc, STR_SIZE_MIDDLE, _T("System Volume"		),15, 28*0 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*1, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("AF Sound"			),15, 28*1 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*2, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Button Sound"		),15, 28*2 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*3, rect.Width(),1);
	//CMiLRe 20140901 E Shutter Sound 추가
//	DrawStr(&dc, STR_SIZE_MIDDLE, _T("E Shutter Sound"		),15, 28*3 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*4, rect.Width(),1);

	//CMiLRe 20140910 Movie Voice&Size 추가
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Voice Volume"),15, 28*4 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*5, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Movie Size"),15, 28*5 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*6, rect.Width(),1);

	DrawGraphicsImage(&dc, strFullPath, 1, 28*4, rect.Width(),1);
	DrawGraphicsImage(&dc, strFullPath, 1, 28*5, rect.Width(),1);
	
	//dh0.seo 2014-11-04
	//-- Draw Background
	switch(m_nStatus)
	{
	case BTN_DOWN:
		color = RGB(0,0,0);
	case BTN_INSIDE:	
		strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_focus bar.png"),strPath);
		DrawGraphicsImage(&dc, strFullPath, m_rectBtn);
		break;
	}
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_line.png"),strPath);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Sensor Cleaning"),15, 28*6 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*7, rect.Width(),1);

	Invalidate(FALSE);	
}

void COptionDSCSet3Dlg::SetOptionProp(int nPropIndex, int nPropValue, RSDSCSet3& opt)
{
	int nEnum = 0;
	nEnum = ChangeIntValueToIndex(nPropIndex, nPropValue);

	switch(nPropIndex)
	{
	case DSC_OPT_DSC3_VOL_SYSTEM	:	m_nSystemVol	= nEnum;	break;	
	case DSC_OPT_DSC3_VOL_AFSOUND	:	m_nAFSound		= nEnum;	break;	
	case DSC_OPT_DSC3_VOL_BTNSOUND	:	m_nBtnSound		= nEnum;	break;	
	//CMiLRe 20140901 E Shutter Sound 추가
//	case DSC_OPT_DSC3_VOL_ESHSOUND	:	m_nESHSound		= nEnum;	break;	
	//CMiLRe 20140910 Movie Voice&Size 추가
	case DSC_OPT_DSC3_VOL_VOICE	:	m_nVoiceVolume		= nEnum;	break;		
	//CMiLRe 20140910 Movie Voice&Size 추가
	case DSC_OPT_DSC3_MOVIE_SIZE:	m_nMovieSize		= nEnum;	break;
	}

	if(((COptionBodyDlg*)GetParent())->GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
	{
		m_comboBox[3].EnableWindow(FALSE);
		m_comboBox[4].EnableWindow(FALSE);
	}
	else
	{
		m_comboBox[3].EnableWindow(TRUE);
		m_comboBox[4].EnableWindow(TRUE);
	}

	UpdateData(FALSE);
}

int COptionDSCSet3Dlg::ChangeIntValueToIndex(int nPropIndex, int nPropValue)
{
	int nEnum = 0;
	switch(nPropIndex)
	{
	case DSC_OPT_DSC3_VOL_SYSTEM	:
		switch(nPropValue)
		{
		case eUCS_MENU_SOUND_OFF	:		nEnum = DSC_MENU_SYSTEMVOLUME_SOUND_OFF		;	break;
		case eUCS_MENU_SOUND_LOW	:		nEnum =	DSC_MENU_SYSTEMVOLUME_SOUND_LOW		;	break;
		case eUCS_MENU_SOUND_MEDIUM	:		nEnum = DSC_MENU_SYSTEMVOLUME_SOUND_MEDIUM	;	break;
		case eUCS_MENU_SOUND_HIGH	:		nEnum =	DSC_MENU_SYSTEMVOLUME_SOUND_HIGH	;	break;
		}
		break;
	case DSC_OPT_DSC3_VOL_AFSOUND:									
		switch(nPropValue)
		{
		case eUCS_MENU_AF_SOUND_OFF	:	nEnum =DSC_MENU_AFSOUND_OFF;	break;
		case eUCS_MENU_AF_SOUND_ON	:	nEnum =DSC_MENU_AFSOUND_ON;		break;
		}
		break;
	case DSC_OPT_DSC3_VOL_BTNSOUND:
		switch(nPropValue)
		{
		case eUCS_MENU_BUTTON_SOUND_OFF	:	nEnum = DSC_MENU_BUTTONSOUND_OFF;	break;
		case eUCS_MENU_BUTTON_SOUND_ON	:	nEnum = DSC_MENU_BUTTONSOUND_ON	;	break;
		}
		break;	
	//CMiLRe 20140901 E Shutter Sound 추가
	case DSC_OPT_DSC3_VOL_ESHSOUND:
		switch(nPropValue)
		{
		case eUCS_MENU_E_SHUTTERSOUND_OFF	:	nEnum = DSC_MENU_E_SHUTTERSOUND_OFF;	break;
		case eUCS_MENU_E_SHUTTERSOUND_ON	:	nEnum = DSC_MENU_E_SHUTTERSOUND_ON	;	break;
		}
		break;	
	//CMiLRe 20140910 Movie Voice&Size 추가
	case DSC_OPT_DSC3_VOL_VOICE:
		switch(nPropValue)
		{
		case eUCS_MENU_VOICE_VOLUME_OFF	:	nEnum = DSC_MENU_VOICE_VOLUME_OFF;	break;
		case eUCS_MENU_VOICE_VOLUME_ON	:		nEnum = DSC_MENU_VOICE_VOLUME_ON	;	break;
		}
		break;
	//CMiLRe 20140910 Movie Voice&Size 추가
	case DSC_OPT_DSC3_MOVIE_SIZE:			
		for(int i = 0; i < ARRAYSIZE(SZOptDSCSet3MoveSizeVale_NX1); i++)
		{
			if(nPropValue == SZOptDSCSet3MoveSizeVale_NX1[i].nIndex + MOVIE_SIZE_DEFAULT_SELECT)
			{
				nEnum = SZOptDSCSet3MoveSizeVale_NX1[i].nIndex;
				break;
			}
		}
		break;
	}

	return nEnum;
}

void COptionDSCSet3Dlg::SaveProp(RSDSCSet3& opt)
{
	if(!UpdateData(TRUE))
		return;

	opt.nSystemVol	= m_nSystemVol	;
	opt.nAFSound	= m_nAFSound	;
	opt.nBtnSound	= m_nBtnSound	;
	//CMiLRe 20140901 E Shutter Sound 추가
	opt.nESHSound	= m_nESHSound	;
	//CMiLRe 20140910 Movie Voice&Size 추가
	opt.nVoiceVolume	= m_nVoiceVolume	;
	opt.nMovieSize = m_nMovieSize;
}

//------------------------------------------------------------------------------ 
//! @brief		LoadPopupMsg
//! @date		2011-8-11
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void COptionDSCSet3Dlg::LoadPopupMsg(int nType, CString strMsg, int nDelay, RSEvent* pMsg)
{
	//-- 2011-8-10 Lee JungTaek
	//-- Add Popup Message
	if(m_pPopupMsg)
	{
		delete m_pPopupMsg;
		m_pPopupMsg = NULL;
	}

	m_pPopupMsg = new CPopupMsgDlg(nType, strMsg, nDelay);
	m_pPopupMsg->Create(CPopupMsgDlg::IDD, this);
	m_pPopupMsg->SetCommandMsg(pMsg);

	CRect rect;
	this->GetWindowRect(&rect);

	Image Img(RSGetRoot(_T("img\\02.Smart Panel\\02_popup_bg02.png")));

	int nX, nY, nWidth, nHeight;

	nWidth = Img.GetWidth();
	nHeight = Img.GetHeight();
	nX = (rect.left + rect.right - nWidth) / 2 - 3;
	nY = (rect.top + rect.bottom - nWidth) / 2 + 50;

	m_pPopupMsg->MoveWindow(nX, nY, Img.GetWidth() + 6, Img.GetHeight() + 8);
	m_pPopupMsg->ShowWindow(SW_SHOW);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
BOOL COptionDSCSet3Dlg::PtInRect(CPoint pt, BOOL bMove)
{
	if(m_rectBtn.PtInRect(pt))
		return TRUE;
	return FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionDSCSet3Dlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	if(PtInRect(point, FALSE))
	{
		m_nStatus = BTN_DOWN;
		InvalidateRect(m_rectBtn);

		RSEvent* pMsg		= new RSEvent;
		pMsg->message	= WM_RS_OPT_SENSOR_CLEANING_DEVICE;
		LoadPopupMsg(POPUP_TYPE_WITH_2BTN, _T("Sensor Cleaning ?"), POPUP_DELAY_NONE, pMsg);
	}
	CDialog::OnLButtonDown(nFlags, point);	
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionDSCSet3Dlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	if(PtInRect(point, FALSE))
	{
		if(m_nStatus != BTN_INSIDE) { m_nStatus = BTN_INSIDE;	InvalidateRect(m_rectBtn);}
	}
	CDialog::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionDSCSet3Dlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if(PtInRect(point, FALSE))
	{
		if(m_nStatus != BTN_INSIDE) 
		{ 
			m_nStatus = BTN_INSIDE;	
			InvalidateRect(m_rectBtn);
		}		
	}
	else
	{
		if(m_nStatus != BTN_NORMAL) 
		{ 
			m_nStatus = BTN_NORMAL;	
			InvalidateRect(m_rectBtn);
		}	
	}

	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseLeave
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT COptionDSCSet3Dlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	if(m_nStatus != BTN_NORMAL) 
	{ 
		m_nStatus = BTN_NORMAL;	
		InvalidateRect(m_rectBtn);
	}		
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnSelchangeCmb
//! @date		2011-7-15
//! @author		Lee JungTaek
//! @note	 	Change Value
//------------------------------------------------------------------------------ 
void COptionDSCSet3Dlg::OnSelchangeCmbSysVolume() 
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_SYSTEMVOLUME, m_nSystemVol);
}
void COptionDSCSet3Dlg::OnSelchangeCmbAFSound() 
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_AFSOUND, m_nAFSound);
}
void COptionDSCSet3Dlg::OnSelchangeCmbBtnSound() 
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_BUTTONSOUND, m_nBtnSound);
}
//CMiLRe 20140901 E Shutter Sound 추가
void COptionDSCSet3Dlg::OnSelchangeCmbESHSound() 
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_E_SHUTTER_SOUND, m_nESHSound);
}

//CMiLRe 20140910 Movie Voice&Size 추가
void COptionDSCSet3Dlg::OnSelchangeCmbVoiceVolume() 
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_VOICE_VOLUME, m_nVoiceVolume);
}

//CMiLRe 20140910 Movie Voice&Size 추가
void COptionDSCSet3Dlg::OnSelchangeCmbMoveiSize() 
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_MOV_SIZE, m_nMovieSize);
}

//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
void COptionDSCSet3Dlg::OnSelSensorCleaning() 
{
	RSEvent* pMsg		= new RSEvent;
	pMsg->message	= WM_RS_OPT_SENSOR_CLEANING_DEVICE;
	LoadPopupMsg(POPUP_TYPE_WITH_2BTN, _T("Sensor Cleaning ?"), POPUP_DELAY_NONE, pMsg);
}
