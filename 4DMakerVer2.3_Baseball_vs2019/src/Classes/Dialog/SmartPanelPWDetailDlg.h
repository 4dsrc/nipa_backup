#pragma once
/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelPWDetailDlg.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-07-01
//
/////////////////////////////////////////////////////////////////////////////

// CSmartPanelPWDetailDlg dialog
#include "BKDialogST.h"
#include "NXRemoteStudioFunc.h"

enum
{
	PW_LINE_NULL = -1,
	PW_LINE_FIRST,
	PW_LINE_SECOND,
	PW_LINE_THIRD,
	PW_LINE_FOURTH,
	PW_LINE_RED,
	PW_LINE_GREEN,
	PW_LINE_BLUE,
	PW_LINE_CNT,
};

enum
{
	BTN_PW_NULL = -1,
	BTN_PW_COLOR_LEFT,
	BTN_PW_COLOR_RIGHT,
	BTN_PW_SATURATION_LEFT,
	BTN_PW_SATURATION_RIGHT,
	BTN_PW_SHARPNESS_LEFT,
	BTN_PW_SHARPNESS_RIGHT,
	BTN_PW_CONTRAST_LEFT,
	BTN_PW_CONTRAST_RIGHT,
	BTN_PW_RED_PLUS,
	BTN_PW_GREEN_PLUS,
	BTN_PW_BLUE_PLUS,
	BTN_PW_RED_MINUS,
	BTN_PW_GREEN_MINUS,
	BTN_PW_BLUE_MINUS,
	BTN_PW_RESET,
	BTN_PW_OK,
	BTN_PW_CNT	,
};

enum
{
	PT_PW_IN_NOTHING = -1,
	PT_PW_NOR,
	PT_PW_INSIDE,
	PT_PW_DOWN,
	PT_PW_DIS,
};

enum
{
	PW_COLOR_NULL = -1,
	PW_COLOR_FF0B04		,
	PW_COLOR_FF6B0F		,
	PW_COLOR_FDB61E		,
	PW_COLOR_FCFD2D		,
	PW_COLOR_CDFD2C		,
	PW_COLOR_89FD2C		,
	PW_COLOR_00FD2D		,
	PW_COLOR_00FE1F		,
	PW_COLOR_00FD92		,
	PW_COLOR_00FE9C		,
	PW_COLOR_00FDD9		,
	PW_COLOR_0DD2FD		,
	PW_COLOR_1973FB		,
	PW_COLOR_1C24FB		,
	PW_COLOR_2B1BFB		,
	PW_COLOR_7018FB		,
	PW_COLOR_C51AFB		,
	PW_COLOR_F01BFO		,
	PW_COLOR_FF16D7		,
	PW_COLOR_FF0A67		,
	PW_COLOR_FF0834		,
};

class CSmartPanelPWDetailDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CSmartPanelPWDetailDlg)

public:
	CSmartPanelPWDetailDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSmartPanelPWDetailDlg();

// Dialog Data
	enum { IDD = IDD_DLG_SMARTPANEL_PW_DETAIL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	int m_nPTPCode;
	int m_nValueIndex;
	//-- Item Info
	int		m_nMouseIn;
	int		m_nSelected;
	int		m_nStatus[BTN_PW_CNT];
	CRect m_rectBtn[BTN_PW_CNT];
	CRect m_rectIcon[PW_LINE_CNT];
	CRect m_rectValue[PW_LINE_CNT];
	CRect m_rectBar[PW_LINE_CNT];
	CRect m_rectHandler[PW_LINE_CNT];

	//-- Values
	int m_nColor;
	int m_nSaturation;
	int m_nSharpness;
	int m_nContrast;

	int m_nColor1st;
	int m_nSaturation1st;
	int m_nSharpness1st;
	int m_nContrast1st;
	BOOL	m_bInit;

	//-- Draw Function
	void SetItemPosition();
	void DrawBackground();	
	void CleanDC(CPaintDC* pDC);

	void DrawLine(int nLine, CDC* pDC);
	void DrawTitle(int nLine, CDC* pDC);
	void DrawIcon(int nLine, CDC* pDC);
	void DrawValue (int nLine, CDC* pDC);
	void DrawBar (int nLine, CDC* pDC);
	void DrawBarHandler(int nLine, CDC* pDC);
	void DrawMenu(int nItem, CDC* pDC);

	//-- Mouse Function
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	int PtInBarRect(CPoint pt);
	void ChangeStatus(int nItem, int nStatus);
	void ChangeStatusAll();
	void MouseEvent();

	//-- Get Information
	CString GetValues(int nLIne);
	COLORREF GetColorValues();
	void SetColorValue(BOOL bRight);		
	void SetSaturationValue(BOOL bRight);	
	void SetSharpnessValue(BOOL bRight);	
	void SetContrastValue(BOOL bRight);	
	void SetHandler(int nLine);

	//-- Button Input
	void ResetValue();
	void SetDetailValue();
	BOOL SetPosition(CPoint point);
	void SetFirstValue();

	//NX1 dh0.seo 2014-09-03
	CString m_strModel;
	void SetRedValue(BOOL bPlus);
	void SetGreenValue(BOOL bPlus);
	void SetBlueValue(BOOL bPlus);
	int m_nRed;
	int m_nGreen;
	int m_nBlue;

public:
	void SetValueIndex(int nValue);
	int GetDetailPropCode(int nValueIndex);
};
