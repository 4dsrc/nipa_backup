/////////////////////////////////////////////////////////////////////////////
//
//  OptionTimeLapseDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "OptionTimeLapseDlg.h"
#include "OptionBodyDlg.h"
#include "NXRemoteStudioDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//CMiLRe 20141120 인터벌 캡쳐 중 스타트 버튼 못누르게 함
static const int timer_check_capture_done		= WM_USER + 0x0001;
//CMiLRe 20141127 Display Save Mode 에서 Wake up
static const int timer_check_dp_wake_up_wait		= WM_USER + 0x0002;

// COptionTimeLapseDlg dialog
IMPLEMENT_DYNAMIC(COptionTimeLapseDlg, COptionBaseDlg)

//------------------------------------------------------------------------------ 
//! @brief		COptionTimeLapseDlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
COptionTimeLapseDlg::COptionTimeLapseDlg(CWnd* pParent /*=NULL*/)
	: COptionBaseDlg(COptionTimeLapseDlg::IDD, pParent)
{	
	m_nDelayTimeMin	= 0;
	m_nDelayTimeSec	= 30;
	m_nTimerMin	= 0;
	m_nTimerSec	= 7;
	m_nCaptureTime = 3;

	m_bDelaySet			= FALSE;	
	m_bIntervalTimer	= FALSE;	
	m_nStatus[0]			= BTN_NORMAL;
	m_nStatus[1]			= BTN_NORMAL;

	m_pTimerCaptureStatus = NULL;


	m_nIntervalCapture_Hour = 0;
	m_nIntervalCapture_Minute = 0;
	m_nIntervalCapture_Second = 0;
	m_nIntervalCapture_Count = 0;
	m_nIntervalCapture_Start_Hour = 0;
	m_nIntervalCapture_Start_Minute = 0;
	m_nIntervalCapture_Start_AMPM = 0;
	m_nIntervalCapture_Time_Lapse = 0;
	m_strIntervalCapture_Count=_T("");
	//CMiLRe 20140922 Picture Interval Capture 추가
	m_bChangedDP = TRUE;

	m_pMsgPopupDlg = NULL;

	m_nIntervalCapture_Second_ = 0;
	m_bMinute = FALSE;
}
COptionTimeLapseDlg::~COptionTimeLapseDlg() 
{
	if(m_pTimerCaptureStatus)
	{
		delete m_pTimerCaptureStatus;
		m_pTimerCaptureStatus = NULL;
	}

	//CMiLRe 20141113 메모리 릭 제거
/*	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE+2 ; i++)
	{
		if(m_staticLabel[i])
		{
			delete m_staticLabel[i];
			m_staticLabel[i] = NULL;
		}
	} */

	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		if(m_staticLabelBar[i])
		{
			delete m_staticLabelBar[i];
			m_staticLabelBar[i] = NULL;
		}
	}
	
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE_NX1 ; i++)
	{
		if(m_staticLabel_NX1[i])
		{
			delete m_staticLabel_NX1[i];
			m_staticLabel_NX1[i] = NULL;
		}
	}

	if(m_pMsgPopupDlg)
	{
		delete m_pMsgPopupDlg;
		m_pMsgPopupDlg = NULL;
	}
}
void COptionTimeLapseDlg::DoDataExchange(CDataExchange* pDX)
{
	COptionBaseDlg::DoDataExchange(pDX);			
	//{{AFX_DATA_MAP(COptionTimeLapseDlg) 
/*	DDX_Text(pDX, IDC_EDIT_1, m_nDelayTimeMin		);
	DDX_Text(pDX, IDC_EDIT_2, m_nDelayTimeSec		);
	DDX_Text(pDX, IDC_EDIT_3, m_nTimerMin	);
	DDX_Text(pDX, IDC_EDIT_4, m_nTimerSec	);
	DDX_Text(pDX, IDC_EDIT_5, m_nCaptureTime		); */
	//NX2000 주석
/*	DDX_Control(pDX, IDC_EDIT_1, m_edit[0]);
	DDX_Control(pDX, IDC_EDIT_2, m_edit[1]);
	DDX_Control(pDX, IDC_EDIT_3, m_edit[2]);
	DDX_Control(pDX, IDC_EDIT_4, m_edit[3]);
	DDX_Control(pDX, IDC_EDIT_5, m_edit[4]);
	DDV_MinMaxInt(pDX, m_nDelayTimeMin, 0, 99);		
	DDV_MinMaxInt(pDX, m_nDelayTimeSec, 0, 59);		
	DDV_MinMaxInt(pDX, m_nTimerMin, 0, 99);	
	DDV_MinMaxInt(pDX, m_nTimerSec, 0, 59);		
	DDV_MinMaxInt(pDX, m_nCaptureTime, 0, 10000); */
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_CMB_1, m_comboBox_NX1[0]);
	DDX_Control(pDX, IDC_CMB_2, m_comboBox_NX1[1]);
	DDX_Control(pDX, IDC_CMB_3, m_comboBox_NX1[2]);
	DDX_Control(pDX, IDC_CMB_4, m_comboBox_NX1[3]);	
	DDX_Control(pDX, IDC_CMB_5, m_comboBox_NX1[4]);
	DDX_Control(pDX, IDC_CMB_6, m_comboBox_NX1[5]);
	DDX_Control(pDX, IDC_CMB_7, m_comboBox_NX1[6]);
	DDX_Control(pDX, IDC_CMB_8, m_comboBox_NX1[7]);
	DDX_Control(pDX, IDC_CMB_9, m_comboBox_NX1[8]);
	DDX_Control(pDX, IDC_CMB_10, m_comboBox_NX1[9]);
	DDX_Control(pDX, IDC_CMB_11, m_comboBox_NX1[10]); 

/*	DDX_Control(pDX, IDC_EDIT_6, m_editCount_NX1[0]);	
	DDX_Control(pDX, IDC_EDIT_7, m_editCount_NX1[1]);	
	DDX_Control(pDX, IDC_EDIT_8, m_editCount_NX1[2]);	*/
//	DDX_Control(pDX, IDC_EDIT_9, m_editCount_NX1[3]);	
/*	DDX_Control(pDX, IDC_EDIT_10, m_editCount_NX1[4]);	
	DDX_Control(pDX, IDC_EDIT_11, m_editCount_NX1[5]);	*/

	DDX_CBIndex(pDX,IDC_CMB_1, m_nIntervalCapture_Start_AMPM);
	DDX_CBIndex(pDX,IDC_CMB_2, m_nIntervalCapture_Time_Lapse);
	DDX_CBIndex(pDX,IDC_CMB_3, m_nIntervalCapture_Minute);
	DDX_CBIndex(pDX,IDC_CMB_4, m_nIntervalCapture_Second);
	DDX_CBIndex(pDX,IDC_CMB_5, m_nIntervalCapture_Start_Hour);
	DDX_CBIndex(pDX,IDC_CMB_6, m_nIntervalCapture_Start_Minute);
	DDX_CBIndex(pDX,IDC_CMB_7, m_nIntervalCapture_Second_);

	DDX_CBIndex(pDX,IDC_CMB_8, m_nIntervalCapture_CountTH);
	DDX_CBIndex(pDX,IDC_CMB_9, m_nIntervalCapture_CountH);
	DDX_CBIndex(pDX,IDC_CMB_10, m_nIntervalCapture_CountT);
	DDX_CBIndex(pDX,IDC_CMB_11, m_nIntervalCapture_CountO);

//	DDX_Text(pDX, IDC_EDIT_9, m_strIntervalCapture_Count);

/*	DDX_Text(pDX, IDC_EDIT_6, m_strIntervalCapture_Hour);	
	DDX_Text(pDX, IDC_EDIT_7, m_strIntervalCapture_Minute);	
	DDX_Text(pDX, IDC_EDIT_8, m_strIntervalCapture_Second);	
	DDX_Text(pDX, IDC_EDIT_9, m_strIntervalCapture_Count);	
	DDX_Text(pDX, IDC_EDIT_10, m_strIntervalCapture_Start_Hour);	
	DDX_Text(pDX, IDC_EDIT_11, m_strIntervalCapture_Start_Minute);	*/
}				

BEGIN_MESSAGE_MAP(COptionTimeLapseDlg, COptionBaseDlg)	
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()	
	ON_WM_MOUSEMOVE()
	//CMiLRe 20141120 인터벌 캡쳐 중 스타트 버튼 못누르게 함
	ON_WM_TIMER()
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
//	ON_BN_CLICKED(IDC_CHECKBOX_1, OnDelaySetting	)		
//	ON_BN_CLICKED(IDC_CHECKBOX_2, OnIntervalTimer	)		
//	ON_BN_CLICKED(IDC_CHECKBOX_2, OnIntervalCapture_Start_Check	)		
	ON_BN_CLICKED(IDC_BTN_OPT_APPLY, OnStart	)		
	ON_BN_CLICKED(IDC_BTN_OPT_CANCEL, OnCancel)		

	ON_CBN_SELCHANGE(IDC_CMB_1, OnSelchangeCmbntervalCapture_Start_AMPM)
	ON_CBN_SELCHANGE(IDC_CMB_2, OnSelchangeCmbIntervalCapture_Time_Lapse)
	ON_CBN_SELCHANGE(IDC_CMB_3, OnChangeIntervalCapture_Minute)
	ON_CBN_SELCHANGE(IDC_CMB_4, OnChangeIntervalCapture_Second)
	ON_CBN_SELCHANGE(IDC_CMB_5, OnChangeIntervalCapture_Start_Hour)
	ON_CBN_SELCHANGE(IDC_CMB_6, OnChangeIntervalCapture_Start_Minute)
	ON_CBN_SELCHANGE(IDC_CMB_7, OnChangeIntervalCapture_Second)


	ON_CBN_SELCHANGE(IDC_CMB_8, OnChangeIntervalCountTH)
	ON_CBN_SELCHANGE(IDC_CMB_9, OnChangeIntervalCountH)
	ON_CBN_SELCHANGE(IDC_CMB_10, OnChangeIntervalCountT)
	ON_CBN_SELCHANGE(IDC_CMB_11, OnChangeIntervalCountO)


	ON_BN_CLICKED(IDC_CHECKBOX_3, OnIntervalCapture_Start_Check)	

//	ON_EN_KILLFOCUS(IDC_EDIT_6, OnChangeIntervalCapture_Hour_Edit)
//	ON_EN_KILLFOCUS(IDC_EDIT_7, OnChangeIntervalCapture_Minute_Edit)
//	ON_EN_KILLFOCUS(IDC_EDIT_8, OnChangeIntervalCapture_Second_Edit)
//	ON_EN_UPDATE(IDC_EDIT_9, OnChangeIntervalCapture_Count_Edit)
//	ON_EN_KILLFOCUS(IDC_EDIT_10, OnChangeIntervalCapture_Start_Hour_Edit)
//	ON_EN_KILLFOCUS(IDC_EDIT_11, OnChangeIntervalCapture_Start_Minute_Edit)

END_MESSAGE_MAP()

// CLiveview message handlers

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionTimeLapseDlg::CreateFrames()
{
	//-- Base Height : 28
	//-- Base Left Margin : 33
	//m_btnTimeLapse.MoveWindow(10,10,200,25);	
	//m_btnTimeLapse.MoveWindow(28, 20, 22 , 22);

	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
//	m_PushBtn[0].Create(_T(""), WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX, CRect(CPoint(8, 28*0 + 5),CSize(20,21)), this, IDC_CHECKBOX_1);	
//	m_PushBtn[1].Create(_T(""), WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX, CRect(CPoint(8, 28*2 + 5),CSize(20,21)), this, IDC_CHECKBOX_2);
	//CMiLRe 20141113 IntervalCapture Stop 추가
	m_PushBtn[2].Create(_T("Stop"), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, CRect(CPoint(8,28*5 + 5),CSize(60,26)), this, IDC_BTN_OPT_CANCEL);
	m_PushBtn[3].Create(_T("Start"), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, CRect(CPoint(322+40, 28*5 + 5),CSize(60,26)), this, IDC_BTN_OPT_APPLY);
	
/*	m_edit[0].MoveWindow(240, 28*1 +	6,	50,18);
	m_edit[1].MoveWindow(310, 28*1 +	6,	50,18);
	m_edit[2].MoveWindow(240, 28*3 +	6,	50,18);
	m_edit[3].MoveWindow(310, 28*3 +	6,	50,18);
	m_edit[4].MoveWindow(210, 28*4 +	6,	50,18);
	
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE+2 ; i++)
	{
		m_staticLabel[i] = new CStatic();		
	}
	m_staticLabel[0]->Create(_T("Delay setting"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(32,28*0+10, 200, 31*1), this, IDC_STATIC_ETC_1);		
	m_staticLabel[1]->Create(_T("Delay Time (00:00 ~ 99:59)"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(32,28*1+10, 250, 31*2), this, IDC_STATIC_ETC_2);		
	m_staticLabel[2]->Create(_T("Interval timer capture"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(32,28*2+10, 200, 31*3), this, IDC_STATIC_ETC_3);		
	m_staticLabel[3]->Create(_T("Capture Interval (00:05~99:59)"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(32,28*3+10, 250, 31*4), this, IDC_STATIC_ETC_4);		
	m_staticLabel[4]->Create(_T("Number of Capture"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(32,28*4+10, 200, 31*5), this, IDC_STATIC_ETC_5);		
	m_staticLabel[5]->Create(_T(":"), WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(298, 28*1+10, 400, 31*3), this, IDC_STATIC_ETC_6);		
	m_staticLabel[6]->Create(_T(":"), WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(298, 28*3 +10, 400, 31*5), this, IDC_STATIC_ETC_7);		 */

	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		m_staticLabelBar[i] = new CStatic();
		m_staticLabelBar[i]->Create(_T(""),  WS_CHILD|WS_VISIBLE|SS_BLACKRECT | SS_LEFT,  CRect(1,30+28*i, OPTION_DLG_WIDTH, 31+28*i), this );
	}

	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE_NX1 ; i++)
	{
		m_staticLabel_NX1[i] = new CStatic();		
	}
	m_staticLabel_NX1[0]->Create(_T("Interval Time                      :"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(32, 30*0+10, 150, 26*1), this, IDC_STATIC_ETC_8);		
	m_staticLabel_NX1[1]->Create(_T("Count"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(32, 29*1+10, 100, 27*2), this, IDC_STATIC_ETC_9);		
	m_staticLabel_NX1[2]->Create(_T("Start Time"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(32, 28*2+10, 100, 28*3), this, IDC_STATIC_ETC_10);		
	m_staticLabel_NX1[3]->Create(_T("Time Lapse"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(32, 27*3+10, 150, 28*4), this, IDC_STATIC_ETC_11);
	m_staticLabel_NX1[6]->Create(_T(":"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(245, 27*2+10, 255, 28*3), this, IDC_STATIC_ETC_14);
	m_staticLabel_NX1[4]->Create(_T(":"), WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(245, 30*0+7, 255, 26*1), this, IDC_STATIC_ETC_12);

//	m_editCount_NX1[3].MoveWindow(190, 28*1 +	4,	115,22);//count


	m_PushBtn_NX1.Create(_T(""), WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX, CRect(CPoint(8, 28*2 + 5),CSize(20,21)), this, IDC_CHECKBOX_3);	

	m_comboBox_NX1[0].MoveWindow(315,28*2 + 5,	50,22);
	m_comboBox_NX1[1].MoveWindow(190,28*3 + 5,	115,22);
	m_comboBox_NX1[2].MoveWindow(190,27*0 + 4,	50,22);
	m_comboBox_NX1[3].MoveWindow(255,27*0 +	4,	50,22);
	m_comboBox_NX1[4].MoveWindow(190,28*2 +	5,	50,22);
	m_comboBox_NX1[5].MoveWindow(255,28*2 +	5,	50,22);
	m_comboBox_NX1[6].MoveWindow(255,27*0 +	4,	50,22);


	m_comboBox_NX1[7].MoveWindow(190,28*1 + 5,	35,22);
	m_comboBox_NX1[8].MoveWindow(230,28*1 + 5,	35,22);
	m_comboBox_NX1[9].MoveWindow(270,28*1 + 5,	35,22);
	m_comboBox_NX1[10].MoveWindow(310,28*1 + 5,	35,22);


	m_comboBox_NX1[0].AddString(_T("AM"));
	m_comboBox_NX1[0].AddString(_T("PM"));

	m_comboBox_NX1[1].AddString(_T("Off"));
	m_comboBox_NX1[1].AddString(_T("On"));

	for(int i=0; i < 10; i++)
	{
		CString strCount;
		strCount.Format(_T("%d"), i);
		m_comboBox_NX1[2].AddString(strCount);
	}

	for(int i=0; i < 60; i++)
	{
		CString strCount;
		strCount.Format(_T("%d"), i);
		m_comboBox_NX1[3].AddString(strCount);
		m_comboBox_NX1[5].AddString(strCount);
	}

	for(int i=1; i < 13; i++)
	{
		CString strCount;
		strCount.Format(_T("%d"), i);
		m_comboBox_NX1[4].AddString(strCount);
	}

	for(int i=3; i < 60; i++)
	{
		CString strCount;
		strCount.Format(_T("%d"), i);
		m_comboBox_NX1[6].AddString(strCount);
	}

//	m_PushBtn[1].ShowWindow(SW_HIDE);

	m_comboBox_NX1[0].EnableWindow(FALSE);
	m_comboBox_NX1[4].EnableWindow(FALSE);
	m_comboBox_NX1[5].EnableWindow(FALSE);


	for(int i = 0; i < 10; i++)
	{
		CString strCount;
		strCount.Format(_T("%d"), i);
		m_comboBox_NX1[7].AddString(strCount);
		m_comboBox_NX1[8].AddString(strCount);
		m_comboBox_NX1[9].AddString(strCount);
		m_comboBox_NX1[10].AddString(strCount);
	}

	this->SetBackgroundColor(GetBKColor());

	m_PushBtn[2].EnableWindow(FALSE);
}
//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionTimeLapseDlg::UpdateFrames()
{
/*	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	COLORREF color;
		
	CString strPath = RSGetRoot();
	CString strFullPath;
	
	if(m_bDelaySet)	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_icon_checked.png"),strPath);
	else				strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_icon_uncheck.png"),strPath);
	DrawGraphicsImage(&dc, strFullPath, m_rectBtn[0]);
	
		
	if(m_bIntervalTimer)	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_icon_checked.png"),strPath);
	else					strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_icon_uncheck.png"),strPath);
	DrawGraphicsImage(&dc, strFullPath, m_rectBtn[1]);


	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_line.png"),strPath);
	//-- 2011-07-14 hongsu.jung
	if(m_bDelaySet)	color = GetTextColor();
	else				color = RGB(60,60,60);
	DrawStr(&dc, STR_SIZE_TEXT_POPUP, _T("Delay setting"						),32, 28*0 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*1, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_TEXT_POPUP, _T("Delay Time (00:00 ~ 99:59)"		),32, 28*1 +10,color);		
	DrawStr(&dc, STR_SIZE_TEXT_POPUP, _T(":								"		),295, 28*1 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*2, rect.Width(),1);
	
	if(m_bIntervalTimer)	color = GetTextColor();
	else					color = RGB(60,60,60);
	DrawStr(&dc, STR_SIZE_TEXT_POPUP, _T("Interval timer capture"				),32, 28*2 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*3, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_TEXT_POPUP, _T("Capture Interval (00:05~99:59)"	),32, 28*3 +10,color);		
	DrawStr(&dc, STR_SIZE_TEXT_POPUP, _T(":								"		),295, 28*3 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*4, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_TEXT_POPUP, _T("Number of Capture"				),32, 28*4 +10,color);		//DrawGraphicsImage(&dc, strFullPath, 1, 28*5, rect.Width(),1);

	ReloadCheckImage();

	//-- Draw Buttons Background
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_softkey_bg.png"),strPath);
	DrawGraphicsImage(&dc, strFullPath, 0,28*5, rect.Width(),32);
	//-- Draw Buttons 	
	switch(m_nStatus[0])
	{
		case BTN_INSIDE:		{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png"		),strPath);		color = RGB(255,255,255); }	break;
		case BTN_DOWN:		{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_push.png"			),strPath);		color = RGB(219,219,219); }	break;
		default:					{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png"	),strPath);		color = RGB(219,219,219); }	break;	
	}	
	DrawGraphicsImage(&dc, strFullPath, m_rectBtn[2]);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Cancel"),m_rectBtn[2],color);

	switch(m_nStatus[1])
	{
		case BTN_INSIDE:		{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png"		),strPath);		color = RGB(255,255,255); }	break;
		case BTN_DOWN:		{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_push.png"			),strPath);		color = RGB(219,219,219); }	break;
		case BTN_DISABLE:		{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_disable.png"	),strPath);		color = RGB( 10, 10, 10); }	break;
		default:					{ strFullPath.Format(_T("%s\\img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png"	),strPath);		color = RGB(219,219,219); }	break;	
	}	
	DrawGraphicsImage(&dc, strFullPath, m_rectBtn[3]);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Start"),m_rectBtn[3],color);

	Invalidate(FALSE);	*/
}
	

//------------------------------------------------------------------------------ 
//! @brief		InitProp
//! @date		2011-6-16
//! @author		Lee JungTaek
//! @note	 	Init From File
//------------------------------------------------------------------------------ 
void COptionTimeLapseDlg::InitProp(RSTimeLapse& opt)
{
//NX2000 주석
/*	m_bDelaySet			=	opt.bDelaySet;
	m_bIntervalTimer	=	opt.bTimerCaptureSet;
	m_nDelayTimeMin		=	opt.nDelayMin;
	m_nDelayTimeSec		=	opt.nDelaySec;
	m_nTimerMin	=	opt.nTimerMin;
	m_nTimerSec	=	opt.nTimerSec;
	m_nCaptureTime		=	opt.nCaptureCount;
	
	UpdateData(FALSE);

	opt.nDeleyTime = SetTimes(TRUE);
	opt.nTimerTime = SetTimes(FALSE);
	if (!m_bDelaySet && !m_bIntervalTimer);
	else	m_nStatus[1] = BTN_NORMAL;

	ReloadCheckImage(); */

	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
//	m_PushBtn[0].SetCheck(m_bDelaySet);
//	m_PushBtn[0].ShowWindow(SW_HIDE);
//	m_PushBtn[1].SetCheck(m_bIntervalTimer);

}


void COptionTimeLapseDlg::SetOptionProp(int nPropIndex, int nPropValue, RSTimeLapse& opt)
{
	int nEnum = 0;
	nEnum = ChangeIntValueToIndex(nPropIndex, nPropValue);

	switch(nPropIndex)
	{	
	case DSC_OPT_RIME_LAPSE_INTERVAL_NUM:											
		{
			m_nIntervalCapture_Count	= nEnum;

			m_nIntervalCapture_CountTH = m_nIntervalCapture_Count / 1000;
			m_nIntervalCapture_CountH = (m_nIntervalCapture_Count%1000) / 100;
			m_nIntervalCapture_CountT = (m_nIntervalCapture_Count%100) / 10;
			m_nIntervalCapture_CountO = (m_nIntervalCapture_Count%100) % 10;

			if(m_nIntervalCapture_Count == 0)
				m_strIntervalCapture_Count.Format(_T(""));
			else
				m_strIntervalCapture_Count.Format(_T("%d"), m_nIntervalCapture_Count);
		}
		break;	
	case DSC_OPT_RIME_LAPSE_INTERVAL_BEGINE_TIME_USE:	
		{
			m_PushBtn_NX1.SetCheck(nEnum);	
			if(m_PushBtn_NX1.GetCheck())
			{	
				m_comboBox_NX1[0].EnableWindow(TRUE);
				m_comboBox_NX1[4].EnableWindow(TRUE);
				m_comboBox_NX1[5].EnableWindow(TRUE);
			}
			else
			{
				m_comboBox_NX1[0].EnableWindow(FALSE);
				m_comboBox_NX1[4].EnableWindow(FALSE);
				m_comboBox_NX1[5].EnableWindow(FALSE);
			}
		}
		break;	
	case DSC_OPT_RIME_LAPSE_TIMELABPS:											
		m_nIntervalCapture_Time_Lapse = nEnum;	
		break;
	case DSC_OPT_RIME_LAPSE_INTERVAL_CYCLE_TIME_HOUR	:		
		{		
			int nLeft = HIWORD(nEnum) ;
			int nRight = LOWORD(nEnum) ;
			m_nIntervalCapture_Hour = LOBYTE(nRight);
			m_nIntervalCapture_Minute = HIBYTE(nRight);
			m_nIntervalCapture_Second = LOBYTE(nLeft);

			m_nIntervalCapture_Second_ = m_nIntervalCapture_Second;

			if(m_nIntervalCapture_Minute == 0)
			{
				if(m_nIntervalCapture_Second_<3)
					m_nIntervalCapture_Second_ = 0;
				else
					m_nIntervalCapture_Second_ = m_nIntervalCapture_Second_ - 3;

				m_comboBox_NX1[3].ShowWindow(SW_HIDE);
				m_comboBox_NX1[6].ShowWindow(SW_SHOW);
			}
			else
			{
				m_nIntervalCapture_Second = m_nIntervalCapture_Second;
				m_comboBox_NX1[3].ShowWindow(SW_SHOW);
				m_comboBox_NX1[6].ShowWindow(SW_HIDE);
			}
		}
		break;	
	
	case DSC_OPT_RIME_LAPSE_INTERVAL_BEGINE_TIME_HOUR:				
		{
			m_nIntervalCapture_Start_Hour = nEnum;
			
			if(m_nIntervalCapture_Start_Hour > 12)
			{		
				if(m_nIntervalCapture_Start_Hour == 24)
				{
					m_comboBox_NX1[4].SetCurSel(11);
//					m_strIntervalCapture_Start_Hour.Format(_T("12"));	
				}
				else
				{
					int nStartHour = m_nIntervalCapture_Start_Hour - 12;
					m_comboBox_NX1[4].SetCurSel(nStartHour - 1);
//					m_strIntervalCapture_Start_Hour.Format(_T("%d"), m_nIntervalCapture_Start_Hour-12);					
				}		
			}
			else
			{	
				if(m_nIntervalCapture_Start_Hour == 12 || m_nIntervalCapture_Start_Hour == 0)
				{
					m_comboBox_NX1[4].SetCurSel(11);
//					m_strIntervalCapture_Start_Hour.Format(_T("12"));	
				}
				else
				{
					int nStartHour = m_nIntervalCapture_Start_Hour - 1;
					m_comboBox_NX1[4].SetCurSel(nStartHour);
//					m_strIntervalCapture_Start_Hour.Format(_T("%d"), m_nIntervalCapture_Start_Hour);	
				}
			}	

			if(m_nIntervalCapture_Start_Hour >= 12)
			{
				m_comboBox_NX1[0].SetCurSel(1);
			}
			else
			{
				m_comboBox_NX1[0].SetCurSel(0);
			}
			UpdateData(TRUE);
		}
		break;
	case DSC_OPT_RIME_LAPSE_INTERVAL_BEGINE_TIME_MIN:	
		{
			m_nIntervalCapture_Start_Minute = nEnum;			
		}
		break;
	}
	
	UpdateData(FALSE);
}

int COptionTimeLapseDlg::ChangeIntValueToIndex(int nPropIndex, int nPropValue)
{
	int nEnum = 0;
	switch(nPropIndex)
	{
	case DSC_OPT_RIME_LAPSE_TIMELABPS	:
	case DSC_OPT_RIME_LAPSE_INTERVAL_BEGINE_TIME_USE:
		switch(nPropValue)
		{
		case eUCS_MENU_SELECT_OFF	:		nEnum = DSC_MENU_ETC_SELECT_OFF		;	break;
		case eUCS_MENU_SELECT_ON		:		nEnum = DSC_MENU_ETC_SELECT_ON	;	break;
		}
		break;	
	case DSC_OPT_RIME_LAPSE_INTERVAL_CYCLE_TIME_HOUR:
	case DSC_OPT_RIME_LAPSE_INTERVAL_NUM:
	case DSC_OPT_RIME_LAPSE_INTERVAL_BEGINE_TIME_HOUR:
	case DSC_OPT_RIME_LAPSE_INTERVAL_BEGINE_TIME_MIN:
		nEnum = nPropValue;
		break;
	}

	return nEnum;
}


int COptionTimeLapseDlg::SetDetailValue()
{
	UINT32 nValue = 0;
		
	nValue = (UINT8)m_nIntervalCapture_Hour;
	nValue = nValue << 8;

	nValue = nValue + (UINT8)m_nIntervalCapture_Minute;
	nValue = nValue << 8;

	if(m_nIntervalCapture_Minute == 0)
	{
		if(m_bMinute == TRUE)
		{
			nValue = nValue + (UINT8)m_nIntervalCapture_Second;
			nValue = nValue << 8;
			m_bMinute = FALSE;
		}
		else
		{
			nValue = nValue + (UINT8)m_nIntervalCapture_Second_ + 3;
			nValue = nValue << 8;
		}
	}
	else
	{
		if(m_bMinute == TRUE)
		{
			nValue = nValue + (UINT8)m_nIntervalCapture_Second;
			nValue = nValue << 8;
			m_bMinute = FALSE;
		}
		else
		{
			nValue = nValue + (UINT8)m_nIntervalCapture_Second;
			nValue = nValue << 8;
		}
		
	}

	nValue = nValue + 0x00;
	return nValue;
}

int COptionTimeLapseDlg::SetDetailCount()
{	
	int nCountTH = m_nIntervalCapture_CountTH * 1000;
	int nCountH = m_nIntervalCapture_CountH * 100;
	int nCountT = m_nIntervalCapture_CountT * 10;
	int nCountO = m_nIntervalCapture_CountO;

	m_nIntervalCapture_Count = nCountTH + nCountH + nCountT + nCountO;

	return m_nIntervalCapture_Count;
}


//CMiLRe 20140922 Picture Interval Capture 추가
void COptionTimeLapseDlg::SetModelTypeDisplay(CString strModel, BOOL bChange)
{
	if(bChange == TRUE && m_bChangedDP == TRUE)
	{
		if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
		{			
			for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE_NX1 ; i++) m_staticLabel_NX1[i]->ShowWindow(SW_HIDE);
			for(int i = 0 ; i < 6 ; i++)
			{
				m_editCount_NX1[i].ShowWindow(SW_HIDE);
			}			
			m_PushBtn_NX1.ShowWindow(SW_HIDE);
			m_comboBox_NX1[0].ShowWindow(SW_HIDE);
			m_comboBox_NX1[1].ShowWindow(SW_HIDE);
		}
		else
		{
/*			m_PushBtn[0].ShowWindow(SW_HIDE);
			m_PushBtn[1].ShowWindow(SW_HIDE);
			for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)	m_edit[i].ShowWindow(SW_HIDE);
			for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE+2 ; i++) m_staticLabel[i]->ShowWindow(SW_HIDE);*/
		}
		m_bChangedDP = FALSE;
	}
}
//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-16
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int COptionTimeLapseDlg::SetTimes(BOOL bSetDelayTime)
{
	int nTime = 0;

	if(bSetDelayTime)
		nTime = (m_nDelayTimeMin * 60 + m_nDelayTimeSec) * 1000;
	else
		nTime = (m_nTimerMin * 60 + m_nTimerSec) * 1000;

	return nTime;
}

//------------------------------------------------------------------------------ 
//! @brief		SaveProp
//! @date		2011-6-16
//! @author		Lee JungTaek
//! @note	 	Init From File
//------------------------------------------------------------------------------ 
void COptionTimeLapseDlg::SaveProp(RSTimeLapse& opt)
{
//NX2000 주석
/*	if(!UpdateData(TRUE))
		return;
	opt.bDelaySet			=	m_bDelaySet			;
	opt.bTimerCaptureSet	=	m_bIntervalTimer	;
	opt.nDelayMin			=	m_nDelayTimeMin		;
	opt.nDelaySec			=	m_nDelayTimeSec		;
	opt.nTimerMin			=	m_nTimerMin	;
	opt.nTimerSec			=	m_nTimerSec	;
	opt.nCaptureCount		=	m_nCaptureTime		;
	opt.nIntervalCapture_Hour = m_nIntervalCapture_Hour;
	opt.nIntervalCapture_Minute = m_nIntervalCapture_Minute;
	opt.nIntervalCapture_Second = m_nIntervalCapture_Second;
	opt.nIntervalCapture_Count = m_nIntervalCapture_Count;	
	opt.nIntervalCapture_Start_Hour = m_nIntervalCapture_Start_Hour;
	opt.nIntervalCapture_Start_Minute = m_nIntervalCapture_Start_Minute;
	opt.nIntervalCapture_Start_AMPM = m_nIntervalCapture_Start_AMPM;
	opt.nIntervalCapture_Time_Lapse = m_nIntervalCapture_Time_Lapse;
	opt.bStartTime = (BOOL)m_PushBtn_NX1.GetCheck(); */
}

void COptionTimeLapseDlg::ReloadCheckImage()
{
//NX 2000 주석
/*	m_edit[0].EnableWindow(m_bDelaySet);
	m_edit[1].EnableWindow(m_bDelaySet);		
	m_edit[2].EnableWindow(m_bIntervalTimer);
	m_edit[3].EnableWindow(m_bIntervalTimer);	
	m_edit[4].EnableWindow(m_bIntervalTimer);	

	if (!m_bDelaySet && !m_bIntervalTimer)
		m_nStatus[1] = BTN_DISABLE; */
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int COptionTimeLapseDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = -1;
	for(int i = 0; i < 4; i ++)
	{
		if(m_rectBtn[i].PtInRect(pt))
		{
			if(i == 3 && m_nStatus[1] == BTN_DISABLE)
				continue;
			nReturn = i;
			break;
		}
	}
	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionTimeLapseDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	
	switch(nItem)
	{
		case 0: 
			m_bDelaySet = !m_bDelaySet ;				
			if(m_bDelaySet)
				m_nStatus[1] = BTN_NORMAL;
			Invalidate();
			break;
		case 1: 
			m_bIntervalTimer  = !m_bIntervalTimer;	
			if(m_bIntervalTimer)
				m_nStatus[0] = BTN_NORMAL;
			Invalidate();
			break;
		case 2:	//m_nStatus[0] = BTN_DOWN;	InvalidateRect(m_rectBtn[2]);	break;
			//-- Close Windows
			((COptionBodyDlg*)GetParent())->InitOptionProp();
			::SendMessage(GetParent()->GetParent()->m_hWnd,WM_CLOSE,NULL,NULL);
			break;
		case 3:	
			//-- Save Prop And Open Sub Dialog
			((COptionBodyDlg*)GetParent())->SaveOptionProp(TRUE);

			BOOL bSingle = FALSE;

			//-- Delete
			if(m_pTimerCaptureStatus)
			{
				delete m_pTimerCaptureStatus;
				m_pTimerCaptureStatus = NULL;
			}
			m_pTimerCaptureStatus = new CTimerCaptureStausDlg(SetTimes(TRUE) / 1000 , SetTimes(FALSE) / 1000, m_nCaptureTime);
			m_pTimerCaptureStatus->Create(CTimerCaptureStausDlg::IDD, this);

			//-- 2011-7-18 Lee JungTaek
			//-- Send Timer Capture Event
			RSEvent* pMsg		= new RSEvent;
			pMsg->message	= WM_RS_START_TIMER_CAPUTRE;
			::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);		
	}
	CDialog::OnLButtonDown(nFlags, point);	
}

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionTimeLapseDlg::OnDelaySetting()
{
	m_bDelaySet = !m_bDelaySet ;				
	if(m_bDelaySet)
		m_nStatus[1] = BTN_NORMAL;
	else
		m_nStatus[1] = BTN_INSIDE;
	Invalidate();
}

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionTimeLapseDlg::OnIntervalTimer()
{
	m_bIntervalTimer  = !m_bIntervalTimer;	
	if(m_bIntervalTimer)
		m_nStatus[0] = BTN_NORMAL;
	else
		m_nStatus[0] = BTN_INSIDE;
	Invalidate();
}

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionTimeLapseDlg::OnStart()
{
/*	if(m_nIntervalCapture_Start_Minute == 0 && m_nIntervalCapture_Second < 3)
		OnStart_Check();
	else */
		IntervalCapture();
}

void COptionTimeLapseDlg::OnStart_Check()
{
	RSEvent* pMsgToMain = NULL;
	pMsgToMain = new RSEvent();
	pMsgToMain->message	= WM_SDI_EVENT_INTERVAL_CHECK;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS,  (WPARAM)WM_SDI_EVENT_INTERVAL_CHECK, (LPARAM)pMsgToMain);
}

void COptionTimeLapseDlg::IntervalCapture()
{
	if(m_nIntervalCapture_Hour == 0 && m_nIntervalCapture_Minute == 0&& m_nIntervalCapture_Second < 3)
	{
		m_nIntervalCapture_Second = 3;
//		m_strIntervalCapture_Second = _T("3");
		UpdateData(FALSE);

		((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_CYCLE_TIME_HOUR, SetDetailValue());
	}

	((COptionBodyDlg*)GetParent())->SaveOptionProp(TRUE);

	//CMiLRe 20140922 Picture Interval Capture 추가
	if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
	{
		BOOL bSingle = FALSE;

		//-- Delete
		if(m_pTimerCaptureStatus)
		{
			delete m_pTimerCaptureStatus;
			m_pTimerCaptureStatus = NULL;
		}
		m_pTimerCaptureStatus = new CTimerCaptureStausDlg(SetTimes(TRUE) / 1000 , SetTimes(FALSE) / 1000, m_nCaptureTime);
		m_pTimerCaptureStatus->Create(CTimerCaptureStausDlg::IDD, this);		

		RSEvent* pMsg		= new RSEvent;
		pMsg->message	= WM_RS_START_TIMER_CAPUTRE;
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);	
	}
	else
	{
		if(GetMode() == DSC_MODE_P || GetMode() == DSC_MODE_A || GetMode() == DSC_MODE_S || GetMode() == DSC_MODE_M || GetMode() == DSC_MODE_BULB)
		{	
			//CMiLRe 20141127 Display Save Mode 에서 Wake up
			if(GetDPSleepModeStatus())
			{
				RSEvent* pMsgWakeUp		= new RSEvent;
				pMsgWakeUp->message	= WM_RS_DISPLAY_SAVE_MODE_WAKE_UP;
				::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsgWakeUp->message, (LPARAM)pMsgWakeUp);
				SetTimer(timer_check_dp_wake_up_wait, 500, NULL);
			}
			else		
			{
				//CMiLRe 20140922 Picture Interval Capture 추가
				((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_SET, 1);
				//CMiLRe 20141120 인터벌 캡쳐 중 스타트 버튼 못누르게 함
				SetDiscountCaptureInit(m_nIntervalCapture_Count);
				//CMiLRe 20140922 Picture Setting Interval Capture - Capture 

				Sleep(500);

				RSEvent* pMsg2		= new RSEvent;
				pMsg2->message	= WM_RS_START_INTERVAL_CAPUTRE;
				::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg2->message, (LPARAM)pMsg2);
				//CMiLRe 20141120 인터벌 캡쳐 중 스타트 버튼 못누르게 함

				RSIntervalCaptureCheck(TRUE);
				m_PushBtn[3].EnableWindow(FALSE);
				m_PushBtn[2].EnableWindow(TRUE);
				//				if(m_pSmartPanelDlg)
				//					m_pSmartPanelDlg->EnableWindow(FALSE);
				//CMiLRe 20141127 인터벌 캡쳐시 연사일때 팝업창 복귀 후 스타트 버튼 비활성화
				//ResetCaptureDone(TRUE);
				SetTimer(timer_check_capture_done, 500, NULL);

			}
		}
		else
		{
			return;
		}	
	}
}

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionTimeLapseDlg::OnCancel()
{
#if 0
	((COptionBodyDlg*)GetParent())->InitOptionProp();
	::SendMessage(GetParent()->GetParent()->m_hWnd,WM_CLOSE,NULL,NULL);
#else
	//CMiLRe 20141113 IntervalCapture Stop 추가
	RSEvent* pMsg		= new RSEvent;
	pMsg->message	= WM_RS_STOP_INTERVAL_CAPUTRE;
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_SET, 0);
	
#endif
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionTimeLapseDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	
	switch(nItem)
	{
		case 2:
			if(m_nStatus[0] != BTN_INSIDE) 
			{
				m_nStatus[0] = BTN_INSIDE;	
				InvalidateRect(m_rectBtn[2]);
			}
			break;
		case 3:	
			if(m_nStatus[1] != BTN_INSIDE) 
			{
				m_nStatus[1] = BTN_INSIDE;
				InvalidateRect(m_rectBtn[3]);
			}	
			break;
		default:	ResetButton();
	}
	CDialog::OnLButtonUp(nFlags, point);
}
//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionTimeLapseDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	
	switch(nItem)
	{
		case 2:	if(m_nStatus[0] != BTN_INSIDE) { m_nStatus[0] = BTN_INSIDE;	InvalidateRect(m_rectBtn[2]);} break;
		case 3:	if(m_nStatus[1] != BTN_INSIDE) { m_nStatus[1] = BTN_INSIDE;	InvalidateRect(m_rectBtn[3]);}	break;
		default:	ResetButton();
	}
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseLeave
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT COptionTimeLapseDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	ResetButton();
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		ResetButton
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionTimeLapseDlg::ResetButton()
{
	if(m_nStatus[0] != BTN_NORMAL)
	{
		m_nStatus[0] = BTN_NORMAL;	
		InvalidateRect(m_rectBtn[2]);
	}

	if(m_nStatus[1] == BTN_DISABLE)
		return;
	if(m_nStatus[1] != BTN_NORMAL)
	{
		m_nStatus[1] = BTN_NORMAL;	
		InvalidateRect(m_rectBtn[3]);
	}
}
	

void COptionTimeLapseDlg::OnSelchangeCmbIntervalCapture_Time_Lapse() 
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_TIMELABPS, m_nIntervalCapture_Time_Lapse);
}

void COptionTimeLapseDlg::OnIntervalCapture_Start_Check() 
{
	OnIntervalTimer();
	UpdateData();
	m_PushBtn_NX1.SetCheck(m_nStatus[0]);
	if(m_PushBtn_NX1.GetCheck())
	{	
		m_comboBox_NX1[0].EnableWindow(TRUE);
		m_comboBox_NX1[4].EnableWindow(TRUE);
		m_comboBox_NX1[5].EnableWindow(TRUE);
		
	}
	else
	{
		m_comboBox_NX1[0].EnableWindow(FALSE);
		m_comboBox_NX1[4].EnableWindow(FALSE);
		m_comboBox_NX1[5].EnableWindow(FALSE);
	}
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_BEGINE_TIME_USE, m_nStatus[0]);
}

void COptionTimeLapseDlg::OnChangeIntervalCapture_Minute()
{
	m_bMinute = TRUE;
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_CYCLE_TIME_HOUR, SetDetailValue());

}

void COptionTimeLapseDlg::OnChangeIntervalCapture_Second()
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_CYCLE_TIME_HOUR, SetDetailValue());
}

void COptionTimeLapseDlg::OnChangeIntervalCapture_Start_Hour()
{
	UpdateData();
	if(m_comboBox_NX1[0].GetCurSel() == 0)//오전
	{
		if(m_nIntervalCapture_Start_Hour > 12)
			m_nIntervalCapture_Start_Hour = m_nIntervalCapture_Start_Hour - 11;
		else if(m_nIntervalCapture_Start_Hour == 11)
			m_nIntervalCapture_Start_Hour = -1;
	}
	else
	{
		if(m_nIntervalCapture_Start_Hour < 12)
			m_nIntervalCapture_Start_Hour = m_nIntervalCapture_Start_Hour + 12;
	}
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_BEGINE_TIME_HOUR, m_nIntervalCapture_Start_Hour + 1);
}

void COptionTimeLapseDlg::OnSelchangeCmbntervalCapture_Start_AMPM() 
{	
	UpdateData();
	if(m_comboBox_NX1[0].GetCurSel() == 0)//오전
	{
		if(m_nIntervalCapture_Start_Hour > 12)
			m_nIntervalCapture_Start_Hour = m_nIntervalCapture_Start_Hour - 11;
	}
	else//오후
	{
		if(m_nIntervalCapture_Start_Hour < 12)
			m_nIntervalCapture_Start_Hour = m_nIntervalCapture_Start_Hour + 12;
	}
	
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_BEGINE_TIME_HOUR, m_nIntervalCapture_Start_Hour + 1); 
}

void COptionTimeLapseDlg::OnChangeIntervalCapture_Start_Minute()
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_BEGINE_TIME_MIN, m_nIntervalCapture_Start_Minute);
}

void COptionTimeLapseDlg::OnChangeIntervalCapture_Count_Edit() 
{	
/*	CString strIntervalCapture_Count;
	m_editCount_NX1[3].GetWindowTextW(strIntervalCapture_Count);
	m_strIntervalCapture_Count.Format(_T("%s"), strIntervalCapture_Count);
	m_nIntervalCapture_Count = _ttoi(m_strIntervalCapture_Count);
	
	UpdateData();
	//CMiLRe 20141120 인터벌 캡쳐 중 스타트 버튼 못누르게 함
	if((UINT)m_nIntervalCapture_Count > 65535)
		m_nIntervalCapture_Count = 0;

	TRACE(_T("Interval Capture Count %d\n"), m_nIntervalCapture_Count);

	SetDiscountCaptureInit(m_nIntervalCapture_Count);	
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_NUM, m_nIntervalCapture_Count); */
}

//CMiLRe 20141120 인터벌 캡쳐 중 스타트 버튼 못누르게 함
void COptionTimeLapseDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
	case timer_check_capture_done:
		{
			if(GetDiscountCapture() == 0 )
			{
				RSIntervalCaptureCheck(FALSE);
				KillTimer(timer_check_capture_done);	
				ResetCaptureDone();
				m_PushBtn[3].EnableWindow(TRUE);
				m_PushBtn[2].EnableWindow(FALSE);
				((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_SET, 0);
				OnCancel();
			}				
		}
		break;
	//CMiLRe 20141127 Display Save Mode 에서 Wake up
	case timer_check_dp_wake_up_wait:
		{
			if(!GetDPSleepModeStatus())
			{
				Sleep(2000);

				((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_SET, 1);
				SetDiscountCaptureInit(m_nIntervalCapture_Count);

				RSEvent* pMsg		= new RSEvent;
				pMsg->message	= WM_RS_START_INTERVAL_CAPUTRE;
				::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
				KillTimer(timer_check_dp_wake_up_wait);	
				m_PushBtn[3].EnableWindow(FALSE);
				ResetCaptureDone(TRUE);
				SetTimer(timer_check_capture_done, 500, NULL);				
			}
		}
		break;
	default: 
		COptionBaseDlg::OnTimer(nIDEvent);
		return;
	}
	COptionBaseDlg::OnTimer(nIDEvent);
}


BOOL COptionTimeLapseDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_RETURN )
		{
			pMsg->wParam = NULL;	
		}
	}

	return COptionBaseDlg::PreTranslateMessage(pMsg);
}


void COptionTimeLapseDlg::OnChangeIntervalCountTH()
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_NUM, SetDetailCount());
}

void COptionTimeLapseDlg::OnChangeIntervalCountH()
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_NUM, SetDetailCount());
}

void COptionTimeLapseDlg::OnChangeIntervalCountT()
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_NUM, SetDetailCount());
}

void COptionTimeLapseDlg::OnChangeIntervalCountO()
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_DPC_SAMSUNG_INTERVAL_NUM, SetDetailCount());
}
