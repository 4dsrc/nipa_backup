/////////////////////////////////////////////////////////////////////////////
//
//  OptionAboutDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "OptionAboutDlg.h"
#include "Registry.h"
#include <afxinet.h> 

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// COptionAboutDlg dialog
IMPLEMENT_DYNAMIC(COptionAboutDlg, COptionBaseDlg)

//------------------------------------------------------------------------------ 
//! @brief		COptionAboutDlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
COptionAboutDlg::COptionAboutDlg(CWnd* pParent /*=NULL*/)
	: COptionBaseDlg(COptionAboutDlg::IDD, pParent)
{
	m_nStatus = BTN_NORMAL;
	m_strLocalVersion	= _T("Unknown");
	m_strLatestVersion	= _T("Unknown");
	m_bUpdate = FALSE;
	m_strDownloadURL	= _T("");
	m_strDownloadFile	= _T("");
}

COptionAboutDlg::~COptionAboutDlg() 
{
	//CMiLRe 20141113 메모리 릭 제거
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		if(m_staticLabel[i])
		{
			delete m_staticLabel[i];
			m_staticLabel[i] = NULL;
		}
	}
}
void COptionAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	COptionBaseDlg::DoDataExchange(pDX);		
}				

BEGIN_MESSAGE_MAP(COptionAboutDlg, COptionBaseDlg)	
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()	
	ON_WM_MOUSEMOVE()
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	ON_BN_CLICKED(IDC_BTN_OPT_APPLY, OnUpdate	)
END_MESSAGE_MAP()

// CLiveview message handlers
//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionAboutDlg::CreateFrames()
{
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
//	m_PushBtn.Create(_T("Update"), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, CRect(CPoint(160, 90), CSize(89,28)), this, IDC_BTN_OPT_APPLY);
	
	PushBtn.Create(_T(""), WS_CHILD|WS_VISIBLE|BS_BITMAP|BS_FLAT | BS_CENTER | BS_VCENTER, CRect(CPoint(30, 30), CSize(64,64)), this, IDC_BTN_OPT_CANCEL);
	
	CString strPath = RSGetRoot();
	CString strFullPath;
	strFullPath.Format(_T("%s\\img\\00.Common\\00_window_icon_srs_64x64.png"),strPath);
	CImage cPngImg;
	if(cPngImg.Load(strFullPath) == S_OK)
	{
		HBITMAP hBitmap = cPngImg.Detach();
		PushBtn.SetBitmap(hBitmap);
	}

	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		m_staticLabel[i] = new CStatic();
	}
	
	CString strText = _T("");
	m_staticLabel[0]->Create(_T("Samsung Remote Studio"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(CPoint(135,26),CSize(250, 20)), this, IDC_STATIC_ETC_1);
	m_staticLabel[1]->Create(strText,  WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(CPoint(135,44),CSize(250, 20)), this, IDC_STATIC_ETC_2);	
//	m_staticLabel[2]->Create(strText,  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(CPoint(135,62),CSize(250, 20)), this, IDC_STATIC_ETC_3);		
//	m_staticLabel[3]->Create(_T("ⓒ 2011 Samsung Electronics"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(CPoint(135,122),CSize(250, 20)), this, IDC_STATIC_ETC_4);		
//	m_staticLabel[4]->Create(_T("All rights reserved."),  WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(CPoint(135,140),CSize(250, 20)), this, IDC_STATIC_ETC_5);		


	//-- LoadVersion
	LoadVersion();
	
//	strText.Format(_T("Local Version : %s"),m_strLocalVersion);
	strText.Format(SRS_SW_VERSION_ABOUT());
	m_staticLabel[1]->SetWindowTextW(strText);
//	strText.Format(_T("Latest Version : %s"),m_strLatestVersion);
//	m_staticLabel[2]->SetWindowTextW(strText);
	
/*	if(!m_bUpdate)
		m_PushBtn.EnableWindow(FALSE);*/
	this->SetBackgroundColor(GetBKColor());
}

//------------------------------------------------------------------------------ 
//! @brief		COptionAboutDlg
//! @date		2010-07-15
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionAboutDlg::UpdateFrames()
{
	CPaintDC dc(this);
	CRect rect;	
	COLORREF color = GetTextColor();
		
	CString strPath = RSGetRoot();
	CString strFullPath;
	CString strText;
	
	strFullPath.Format(_T("%s\\img\\00.Common\\00_window_icon_srs_64x64.png"),strPath);
	DrawGraphicsImage(&dc, strFullPath, 30, 30, 64, 64);	

	strText.Format(_T("Samsung Remote Studio"));	
	rect = CRect(CPoint(85,26),CSize(250, 20));	DrawStr(&dc, STR_SIZE_MIDDLE, strText, rect, color);
//	strText.Format(_T("Local Version : %s"),m_strLocalVersion);
	//strText.Format(_T("Version : 2.2.3"));
	rect = CRect(CPoint(85,44),CSize(250, 20));	DrawStr(&dc, STR_SIZE_MIDDLE, strText, rect, color);
//	strText.Format(_T("Latest Version : %s"),m_strLatestVersion);
//	rect = CRect(CPoint(85,62),CSize(250, 20));	DrawStr(&dc, STR_SIZE_MIDDLE, strText, rect, color);

//	rect = CRect(CPoint(85,122),CSize(250, 20));	DrawStr(&dc, STR_SIZE_MIDDLE, _T("ⓒ 2014 Samsung Electronics"),rect, color);
//	rect = CRect(CPoint(85,140),CSize(250, 20));	DrawStr(&dc, STR_SIZE_MIDDLE, _T("All rights reserved."),rect, color);
	
	//-- Draw Buttons 	
/*	if(!m_bUpdate)
	{ 
		strFullPath.Format(_T("%s\\img\\01.Light Studio\\01_lightstudio_option_btn_disable.png"),strPath);		color = RGB(10,10,10); 
	}
	else
	{
		switch(m_nStatus)
		{
			case BTN_INSIDE:		{ strFullPath.Format(_T("%s\\img\\01.Light Studio\\01_lightstudio_option_btn_focus.png"),strPath);		color = RGB(255,255,255); }	break;
			case BTN_DOWN:		{ strFullPath.Format(_T("%s\\img\\01.Light Studio\\01_lightstudio_option_btn_push.png"),strPath);		color = RGB(219,219,219); }	break;
			default:					{ strFullPath.Format(_T("%s\\img\\01.Light Studio\\01_lightstudio_option_btn_normal.png"),strPath);		color = RGB(219,219,219); }	break;	
		}	
	}
	DrawGraphicsImage(&dc, strFullPath, m_rectBtn);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Update"),m_rectBtn,color);	*/
	Invalidate(FALSE);	
}


//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
BOOL COptionAboutDlg::PtInRect(CPoint pt, BOOL bMove)
{
	if(m_bUpdate)
	{
		if(m_rectBtn.PtInRect(pt))
			return TRUE;
	}
	return FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionAboutDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	if(PtInRect(point, FALSE))
	{
		m_nStatus = BTN_DOWN;
		InvalidateRect(m_rectBtn);
		//-- Download Install File
		DownloadFile();
	}
	CDialog::OnLButtonDown(nFlags, point);	
}

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
void COptionAboutDlg::OnUpdate()
{
	m_nStatus = BTN_DOWN;
	DownloadFile();
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionAboutDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	if(PtInRect(point, FALSE))
	{
		m_nStatus = BTN_INSIDE;
		InvalidateRect(m_rectBtn);
	}
	CDialog::OnLButtonUp(nFlags, point);
}
//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionAboutDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if(PtInRect(point, FALSE))
	{
		if(m_nStatus != BTN_INSIDE) 
		{ 
			m_nStatus = BTN_INSIDE;	
			InvalidateRect(m_rectBtn);
		}		
	}
	else
	{
		if(m_nStatus != BTN_NORMAL) 
		{ 
			m_nStatus = BTN_NORMAL;	
			InvalidateRect(m_rectBtn);
		}	
	}

	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseLeave
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT COptionAboutDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	if(m_nStatus != BTN_NORMAL) 
	{ 
		m_nStatus = BTN_NORMAL;	
		InvalidateRect(m_rectBtn);
	}		
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		LoadVersion
//! @date		2011-07-19
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionAboutDlg::LoadVersion()
{
	//prevent
	WCHAR buf[_MAX_PATH];
	BOOL bResult;	
	WORD wLocal[4] = {0,};	
	WORD wLatest[4] = {0,};

	//------------------------------------------------
	//-- Get Local Version
	//------------------------------------------------

	// Get the filename of this executable
	if (GetModuleFileName(NULL, buf, _MAX_PATH) == 0)
	    throw "GetModulePath failed";
	//strFile;
	bResult = GetFileVersion(buf, wLocal[0], wLocal[1], wLocal[2], wLocal[3]);
	m_strLocalVersion.Format(_T("%d.%d.%d.%d"),wLocal[0], wLocal[1], wLocal[2], wLocal[3]);

	//------------------------------------------------
	//-- Get Local Version
	//------------------------------------------------
	//-- Latest
	CRegistry reg;
	CString strFileName;
	LPCTSTR lpszSection;	

	lpszSection = REG_RS_ROOT;
    if(reg.VerifyKey(HKEY_LOCAL_MACHINE, lpszSection))
	{
		if(reg.Open(HKEY_LOCAL_MACHINE, lpszSection))
		{
			reg.Read (REG_RS_UPDATE, strFileName);			
			reg.Close();			
		}
		//-- Get Version
		CInternetSession session;
		CInternetFile* pFile = NULL; 
		try 
		{ 
			//-- 2013-03-08 hongsu.jung
			strFileName = _T("");
			pFile = (CInternetFile*) session.OpenURL(strFileName); 
		} 
		catch (CInternetException* m_pException) 
		{ 
			pFile = NULL; 
			m_pException->Delete(); 
		} 
		if(pFile) 
		{ 
			UINT len = (UINT) pFile->GetLength(); 
			BYTE *pBuf = new BYTE[len];

			if(pBuf)
				pFile->Read(pBuf, len); 
			{ 
				BOOL bURL = FALSE;
				CHAR szTemp[256] = {0};
				int j, nVer;
				j = 0;				
				nVer=0;
				memset(szTemp,0,256);
				m_strDownloadURL = _T("");
				m_strDownloadFile = _T("");
				for(int i = 0; i < (int)len; i++)
				{
					if(!bURL)
					{
						if(pBuf[i] == '.' || pBuf[i] == ',' )
						{
							wLatest[nVer++] = atoi(szTemp);							
							j = 0;
							memset(szTemp,0,256);
							continue;
						}
						else if(pBuf[i] == '\n')
						{
							wLatest[nVer++] = atoi(szTemp);							
							m_strLatestVersion.Format(_T("%d.%d.%d.%d"),wLatest[0], wLatest[1], wLatest[2], wLatest[3]);
							j = 0;
							memset(szTemp,0,256);
							bURL = TRUE;							
						}
						else
							szTemp[j++] = pBuf[i];
					}
					else
					{										
						m_strDownloadURL.AppendChar(pBuf[i]);
					}
				}						
			} 
			pFile->Close();   
			delete pFile; 

			if(pBuf)
			{
				delete[] pBuf; 
				pBuf = NULL;
			}
		}

		ULONGLONG LocalVersion = ((ULONGLONG)wLocal[0]<<48) | ((ULONGLONG)wLocal[1]<<32) | ((ULONGLONG)wLocal[2]<<16) | (ULONGLONG)wLocal[3];
		ULONGLONG NewVersion   = ((ULONGLONG)wLatest[0]<<48) | ((ULONGLONG)wLatest[1]<<32) | ((ULONGLONG)wLatest[2]<<16) | (ULONGLONG)wLatest[3];
		m_bUpdate = (LocalVersion < NewVersion);

		int nFileName= 0;
		nFileName = m_strDownloadURL.ReverseFind(_T('/'));
		m_strDownloadFile = m_strDownloadURL.Right(m_strDownloadURL.GetLength() - nFileName - 1);
	}	
}
//------------------------------------------------------------------------------ 
//! @brief		LoadVersion
//! @date		2011-07-20
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionAboutDlg::DownloadFile()
{
	CInternetSession session; 
	CInternetFile* pFile = NULL; 	
	try 
	{ 
		pFile = (CInternetFile*) session.OpenURL(m_strDownloadURL); 
	} 	
	catch (CInternetException* m_pException) 
	{ 
		pFile = NULL;
		m_pException->Delete(); 
	} 
	
	if(pFile)
	{
		UINT len = (UINT) pFile->GetLength(); 
		char buf[2000];
		int numread;

		CString strLocalDownload, strLocalDownloadFile;
		strLocalDownload.Format(_T("%s\\download"),RSGetRoot());
		::CreateDirectory (strLocalDownload, NULL);
		strLocalDownloadFile.Format(_T("%s\\%s"),strLocalDownload, m_strDownloadFile);
		
		//-- 2011-07-21 hongsu.jung
		BeginWaitCursor();
		CFile myfile(strLocalDownloadFile, CFile::modeCreate|CFile::modeWrite|CFile::typeBinary);
		while ((numread = pFile->Read(buf,sizeof(buf)-1)) > 0)
		{
			buf[numread] = '\0';
			myfile.Write(buf, numread);
		}
		myfile.Close();
		pFile->Close();   	
		delete pFile; 		
		//-- 2011-07-21 hongsu.jung		
		EndWaitCursor();

		CString strComment;
		strComment.Format(_T("File Download Completed\n[%s]\nDo you want to update now?"),strLocalDownloadFile);
		//-- Check Update
		int nReturn = AfxMessageBox(strComment,MB_YESNO);
		switch(nReturn)
		{
		case IDYES:		
			{
				//-- Execute Downloaded File
				//CreateProcess(strLocalDownloadFile,NULL,NULL,NULL,false,/*INHERIT_CALLER_PRIORITYor */CREATE_NEW_CONSOLE,NULL,NULL,NULL, NULL);
				ShellExecute(NULL ,_T("open"), strLocalDownloadFile, NULL , NULL, SW_SHOWNORMAL);
				::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_CLOSE, NULL, NULL);
			}
			break;
		case IDNO:	return;
		}
		

	}
}

//------------------------------------------------------------------------------ 
//! @brief			Get file version of a specified file
//! @date		    2010-8-6
//! @revision		2010-8-6. if a specified file does not exist, return the version number "0.0.0.0"
//------------------------------------------------------------------------------
BOOL COptionAboutDlg::GetFileVersion(CString strFullPath, WORD& wMajor1, WORD& wMajor2, WORD& wMinor1, WORD& wMinor2)
{
	BOOL bResult    = FALSE;
	LPBYTE lpBuffer = NULL;
	DWORD dwHandle;
	DWORD dwSize = GetFileVersionInfoSize(strFullPath, &dwHandle);
	if(dwSize <= 0)
		goto TGUPDATERDLG_ERROR;

	lpBuffer = new BYTE[dwSize];

	if(FALSE == GetFileVersionInfo(strFullPath, 0, dwSize, lpBuffer))
		goto TGUPDATERDLG_ERROR;

	UINT cbTranslate = 0;
	VS_FIXEDFILEINFO* pFixedInfo;
	if(!VerQueryValue(lpBuffer, TEXT("\\"), (LPVOID*)&pFixedInfo,	&cbTranslate))
		goto TGUPDATERDLG_ERROR;

	wMajor1 = HIWORD(pFixedInfo->dwFileVersionMS);
	wMajor2 = LOWORD(pFixedInfo->dwFileVersionMS);
	wMinor1 = HIWORD(pFixedInfo->dwFileVersionLS);
	wMinor2 = LOWORD(pFixedInfo->dwFileVersionLS);

	bResult = TRUE;

TGUPDATERDLG_ERROR:
	if(lpBuffer)
	{
		delete[] lpBuffer; 
		lpBuffer = NULL;
	}
	if(!bResult)
		wMajor1 = wMajor2 = wMinor1 = wMinor2 = 0;
	
	return bResult;
}
