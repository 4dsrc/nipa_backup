/////////////////////////////////////////////////////////////////////////////
//
//  OptionDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "RSChildDialog.h"
#include "OptionTabDlg.h"
#include "OptionBodyDlg.h"
#include "RSOption.h"
#include "NXRemoteStudioFunc.h"

// COptionDlg dialog
class COptionDlg : public CRSChildDialog
{
	DECLARE_DYNAMIC(COptionDlg)	
public:
	COptionDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionDlg();

// Dialog Data
	enum { IDD = IDD_DLG_OPTION};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();	
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedBtnCancel();
	LRESULT OnOptionTabMsg(WPARAM w, LPARAM l);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

public:
	COptionTabDlg* m_pOptionTab;
	COptionBodyDlg* m_pOptionBody;

	//-- 2011-6-16 Lee JungTaek
	void LoadOptionInfo();

	//-- 2011-6-16 Lee JungTaek
	CRSOption	m_RSOption;
	//CMiLRe 20140902 DRIVER GET ����
	CString GetModel(){return m_strModel;}
	void SetModel(CString strModel) {m_strModel = strModel;}

	void SetMode(int nMode);
private:	
	CBitmapButton m_btnCancel	;

	//-- 2011-05-18 hongsu.jung
	void UpdateFrames();
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();
	void DrawBackGround();
	void DrawDialogs();	
	//CMiLRe 20140902 DRIVER GET ����
	CString m_strModel;
	int m_nMode;

	CRect m_RectClose;
	BOOL m_bCloseState;
};