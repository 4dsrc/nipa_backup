/////////////////////////////////////////////////////////////////////////////
//
//  OptionDSCSet2Dlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "OptionDSCSet2Dlg.h"
#include "OptionBodyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// COptionDSCSet2Dlg dialog
IMPLEMENT_DYNAMIC(COptionDSCSet2Dlg, COptionBaseDlg)

//------------------------------------------------------------------------------ 
//! @brief		COptionDSCSet2Dlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
COptionDSCSet2Dlg::COptionDSCSet2Dlg(CWnd* pParent /*=NULL*/)
	: COptionBaseDlg(COptionDSCSet2Dlg::IDD, pParent)
{
//	SetDragMove(FALSE);
}

COptionDSCSet2Dlg::~COptionDSCSet2Dlg()
{
	//CMiLRe 20141113 메모리 릭 제거
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		if(m_staticLabel[i])
		{
			delete m_staticLabel[i];
			m_staticLabel[i] = NULL;
		}

		if(m_staticLabelBar[i])
		{
			delete m_staticLabelBar[i];
			m_staticLabelBar[i] = NULL;
		}
	}
}

void COptionDSCSet2Dlg::DoDataExchange(CDataExchange* pDX)
{
	COptionBaseDlg::DoDataExchange(pDX);		
	DDX_Control(pDX, IDC_CMB_1, m_comboBox[0]);	
	DDX_Control(pDX, IDC_EDIT_1, m_edit[0]);
	DDX_Control(pDX, IDC_SPIN_1, m_spin[0]);
	DDX_Control(pDX, IDC_EDIT_2, m_edit[1]);
	DDX_Control(pDX, IDC_SPIN_2, m_spin[1]);
	DDX_Control(pDX, IDC_EDIT_3, m_edit[2]);
	DDX_Control(pDX, IDC_SPIN_3, m_spin[2]);
	DDX_Control(pDX, IDC_EDIT_4, m_edit[3]);
	DDX_Control(pDX, IDC_SPIN_4, m_spin[3]);
	//CMiLRe 20141021 AM/PM 표시 및 12/24 변경 수정
	DDX_Control(pDX, IDC_EDIT_6, m_edit[7]);
	DDX_Control(pDX, IDC_EDIT_5, m_edit[4]);
	DDX_Control(pDX, IDC_SPIN_5, m_spin[4]);
	DDX_Control(pDX, IDC_CMB_2, m_comboBox[1]);
	DDX_Control(pDX, IDC_CMB_3, m_comboBox[2]);	
	DDX_Control(pDX, IDC_CMB_4, m_comboBox[3]);
	//dh0.seo 2014-09-15 Quick View
	DDX_Control(pDX, IDC_CMB_5, m_comboBox[4]);
	DDX_CBIndex(pDX,IDC_CMB_1, m_nTimeZone);
	DDX_Text(pDX, IDC_EDIT_1, m_nYear);
	DDX_Text(pDX, IDC_EDIT_2, m_nMonth);
	DDX_Text(pDX, IDC_EDIT_3, m_nDay);
	DDX_Text(pDX, IDC_EDIT_4, m_nHour);
	DDX_Text(pDX, IDC_EDIT_5, m_nMin);
	DDX_CBString(pDX,IDC_CMB_2, m_strType);
	DDX_CBIndex(pDX,IDC_CMB_3, m_nHourType);
	DDX_CBIndex(pDX,IDC_CMB_4, m_nImprint);
	DDX_CBIndex(pDX,IDC_CMB_5, m_nQuickView);
	//CMiLRe 20141001 Date, Time 추가
	DDX_Control(pDX, IDC_BUTTON1, m_PushRefreshBtn);
}				

BEGIN_MESSAGE_MAP(COptionDSCSet2Dlg, COptionBaseDlg)	
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_1, &COptionDSCSet2Dlg::OnDeltaposSpinYear)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_2, &COptionDSCSet2Dlg::OnDeltaposSpinMonth)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_3, &COptionDSCSet2Dlg::OnDeltaposSpinDay)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_4, &COptionDSCSet2Dlg::OnDeltaposSpinHour)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_5, &COptionDSCSet2Dlg::OnDeltaposSpinMinute)
	ON_CBN_SELCHANGE(IDC_CMB_1, OnSelchangeCmbTimeZone		)
	ON_CBN_SELCHANGE(IDC_CMB_2, OnSelchangeCmbType			)
	ON_CBN_SELCHANGE(IDC_CMB_3, OnSelchangeCmbHourType		)
	ON_CBN_SELCHANGE(IDC_CMB_4, OnSelchangeCmbImprint		)
	ON_CBN_SELCHANGE(IDC_CMB_5, OnSelchangeCmbQuickView		)

	//CMiLRe 20141001 Date, Time 추가
	ON_BN_CLICKED(IDC_BUTTON1, OnSelchangeCmbRefresh	)
END_MESSAGE_MAP()

// CLiveview message handlers

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionDSCSet2Dlg::CreateFrames()
{
	//-- 1st Line
	m_comboBox[0].MoveWindow(160, 28*0 +	5,	150,22);
	//-- 2nd Linve
 	m_edit[0].MoveWindow		(160,28*1 +	5,	60,18);			//-- Year
 	m_spin[0].MoveWindow	(220,28*1 +	5,	15,18);
 	m_edit[1].MoveWindow		(250,28*1 +	5,	40,18);			//-- Month
 	m_spin[1].MoveWindow	(290,28*1 +	5,	15,18);
 	m_edit[2].MoveWindow		(320,28*1 +	5,	40,18);			//-- Day
 	m_spin[2].MoveWindow	(360,28*1 +	5,	15,18);

	//CMiLRe 20141001 Date, Time 추가
	m_PushRefreshBtn.MoveWindow(380,28*1 +	5,	45,18);
	m_PushRefreshBtn.SetWindowTextW(_T("Re"));
	//-- 3rd Line
	m_edit[3].MoveWindow		(160,28*2 +	5,	40,18);			//-- Hour
 	m_spin[3].MoveWindow	(200,28*2 +	5,	15,18); 		
	m_edit[4].MoveWindow		(230,28*2 +	5,	40,18);			//-- Min
 	m_spin[4].MoveWindow	(270,28*2 +	5,	15,18); 	
	//CMiLRe 20141021 AM/PM 표시 및 12/24 변경 수정
	m_edit[7].MoveWindow		(300,28*2 +	5,	40,18);
	//-- 4th Line
	m_comboBox[1].MoveWindow(160,28*3 +	5,120,22);
	m_comboBox[2].MoveWindow(300,28*3 +	5,60, 22);
	//-- 5th Line
	m_comboBox[3].MoveWindow(160,28*4 +	5,120,22);
	//dh0.seo 2014-09-15 Quick View
	m_comboBox[4].MoveWindow(160,28*5 +	5,120,22);

	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		m_staticLabel[i] = new CStatic();
		m_staticLabelBar[i] = new CStatic();
	}
	m_staticLabel[0]->Create(_T("Time Zone"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*0+10, 150, 31*1), this, IDC_STATIC_ETC_1);		
	m_staticLabel[1]->Create(_T("Date"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*1+10, 150, 31*2), this, IDC_STATIC_ETC_2);		
	m_staticLabel[2]->Create(_T("Time"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*2+10, 150, 31*3), this, IDC_STATIC_ETC_3);		
	m_staticLabel[3]->Create(_T("Type"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*3+10, 150, 31*4), this, IDC_STATIC_ETC_4);		
	m_staticLabel[4]->Create(_T("Imprint"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*4+10, 150, 31*5), this, IDC_STATIC_ETC_5);		
	m_staticLabel[5]->Create(_T("Quick View"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*5+10, 150, 31*6), this, IDC_STATIC_ETC_6);		
	
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		m_staticLabelBar[i]->Create(_T(""),  WS_CHILD|WS_VISIBLE|SS_BLACKRECT | SS_LEFT,  CRect(1,30+28*i, OPTION_DLG_WIDTH, 31+28*i), this );
	}

	//-- TimeZone
	for(int i = 0; i < ARRAYSIZE(SZOptDSCSet2TimeZoneValue); i++)
		m_comboBox[0].AddString(SZOptDSCSet2TimeZoneValue[i].strDesc);
	//-- Temp Code
	m_comboBox[0].SetCurSel(0);

	m_edit[0].SetWindowText(_T("2011"));		//-- Year	
	m_edit[1].SetWindowText(_T("01"));		//-- Month
	m_edit[2].SetWindowText(_T("01"));		//-- Day
	m_edit[3].SetWindowText(_T("01"));		//-- Hour
	m_edit[4].SetWindowText(_T("00"));		//-- Min

	//-- Date Type
 	for(int i = 0; i < ARRAYSIZE(SZOptDSCSet2DateTimeTypeValue); i++)
 		m_comboBox[1].AddString(SZOptDSCSet2DateTimeTypeValue[i].strDesc);

	//-- Hour Type
	m_comboBox[2].AddString(_T("24H"));
 	m_comboBox[2].AddString(_T("12H"));

	//-- Imprint
	m_comboBox[3].AddString(_T("Off"));
	m_comboBox[3].AddString(_T("On"));

	//dh0.seo 2014-09-15
	m_comboBox[4].AddString(_T("Off"));
//	m_comboBox[4].AddString(_T("Hold+Focus Zoom"));
//	m_comboBox[4].AddString(_T("Hold"));
	m_comboBox[4].AddString(_T("1 sec"));
	m_comboBox[4].AddString(_T("3 sec"));
	m_comboBox[4].AddString(_T("5 sec"));
		
	this->SetBackgroundColor(GetBKColor());
}
//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionDSCSet2Dlg::UpdateFrames()
{
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	COLORREF color;
		
	CString strPath = RSGetRoot();
	CString strFullPath;
	
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_line.png"),strPath);
	//-- 2011-07-14 hongsu.jung
	color = GetTextColor();
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Time Zone"	),15, 28*0 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*1, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Date"			),15, 28*1 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*2, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Time"			),15, 28*2 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*3, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Type"			),15, 28*3 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*4, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Imprint"		),15, 28*4 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*5, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Quick View"	),15, 28*5 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*6, rect.Width(),1);

	//CMiLRe 20140902 DRIVER GET 설정
	if(((COptionBodyDlg*)GetParent())->GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
		m_comboBox[3].EnableWindow(TRUE);
	else
		m_comboBox[3].EnableWindow(FALSE);

	Invalidate(FALSE);	
}
void COptionDSCSet2Dlg::SetOptionProp(int nPropIndex, int nPropValue, RSDSCSet2& opt)
{
	int nEnum = 0;

	nEnum = ChangeIntValueToIndex(nPropIndex, nPropValue);	

	switch(nPropIndex)
	{
	case DSC_OPT_DSC2_DATETIME_SUMMERTIME:		m_nDST = nEnum;			break;
	//CMiLRe 20141021 AM/PM 표시 및 12/24 변경 수정
	case DSC_OPT_DSC2_DATETIME_HOUR:			
		{
			m_nHourType = nEnum;
			if(m_nHourType)
			{
				m_edit[7].ShowWindow(TRUE);
			}
			else
			{
				m_edit[7].ShowWindow(FALSE);
			}
			break;
		}
	case DSC_OPT_DSC2_DATETIME_IMPRINT:			m_nImprint = nEnum;		break;
	//dh0.seo 2014-09-15
	case DSC_OPT_DSC2_QUICKVIEW:				m_nQuickView = nEnum;	break;
	//SDK Test
//	case DSC_OPT_DSC2_TEST:						m_nQuickView = nEnum;	break;
	}	

	if(((COptionBodyDlg*)GetParent())->GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
	{
		m_comboBox[3].EnableWindow(TRUE);
		m_comboBox[4].EnableWindow(FALSE);
	}
	else
	{
		m_comboBox[3].EnableWindow(FALSE);
		m_comboBox[4].EnableWindow(TRUE);
	}

	UpdateData(FALSE);
}

void COptionDSCSet2Dlg::SetOptionProp(int nPropIndex, CString strPropValue, RSDSCSet2& opt)
{
	int nEnum = 0;
	
	switch(nPropIndex)
	{
	case DSC_OPT_DSC2_DATETIME:			
		{
			m_strDateTime = strPropValue;
			SetDateTime(strPropValue);
		}
		break;
	case DSC_OPT_DSC2_DATETIME_TYPE:
		{
			m_strType = strPropValue;
			nEnum = ChangeStringValueToIndex(nPropIndex, strPropValue);	
			m_comboBox[nPropIndex - DSC_OPT_CUSTOM_CNT - DSC_OPT_DSC1_CNT].SetCurSel(nEnum);
		}
		break;
	case DSC_OPT_DSC2_DATETIME_TIMEZONE:
		{
			m_strTimeZone = strPropValue;
			m_nTimeZone = ChangeStringValueToIndex(nPropIndex, strPropValue);	
			m_comboBox[nPropIndex - DSC_OPT_CUSTOM_CNT - DSC_OPT_DSC1_CNT].SetCurSel(m_nTimeZone);
		}
		break;
	}	
	if(((COptionBodyDlg*)GetParent())->GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
		m_comboBox[3].EnableWindow(TRUE);
	else
		m_comboBox[3].EnableWindow(FALSE);
	UpdateData(FALSE);
}

int COptionDSCSet2Dlg::ChangeIntValueToIndex(int nPropIndex, int nPropValue)
{
	int nEnum = 0;

	switch(nPropIndex)
	{
	case DSC_OPT_DSC2_DATETIME_SUMMERTIME:									
		switch(nPropValue)
		{
		case eUCS_MENU_SUMMER_TIME_OFF	:	nEnum = DSC_MENU_SUMMER_TIME_OFF;	break;
		case eUCS_MENU_SUMMER_TIME_ON	:	nEnum = DSC_MENU_SUMMER_TIME_ON	;	break;
		}
		break;
	case DSC_OPT_DSC2_DATETIME_HOUR:
		switch(nPropValue)
		{
		case eUCS_MENU_DATETYPE_HOUR_24H	:	nEnum = DSC_MENU_HOURTYPE_HOUR_24H;		break;
		case eUCS_MENU_DATETYPE_HOUR_12H	:	nEnum = DSC_MENU_HOURTYPE_HOUR_12H;		break;
		}	 
		break;		
	case DSC_OPT_DSC2_DATETIME_IMPRINT:
		switch(nPropValue)
		{
		case eUCS_MENU_IMPRINT_OFF		:	nEnum = DSC_MENU_IMPRINT_OFF;	break;
		case eUCS_MENU_IMPRINT_ON		:	nEnum = DSC_MENU_IMPRINT_ON	;	break;
		}
		break;	
	//dh0.seo 2014-09-15 Quick View
	case DSC_OPT_DSC2_QUICKVIEW:
		switch(nPropValue)
		{
		case eUCS_MENU_QUICK_VIEW_OFF			:	nEnum = DSC_MENU_QUICKVIEW_OFF;	break;
		case eUCS_MENU_QUICK_VIEW_1SEC			:	nEnum = DSC_MENU_QUICKVIEW_1SEC	;	break;
		case eUCS_MENU_QUICK_VIEW_3SEC			:	nEnum = DSC_MENU_QUICKVIEW_3SEC;	break;
		case eUCS_MENU_QUICK_VIEW_5SEC			:	nEnum = DSC_MENU_QUICKVIEW_5SEC	;	break;
		case eUCS_MENU_QUICK_VIEW_HOLD			:	nEnum = DSC_MENU_QUICKVIEW_HOLD;	break;
		case eUCS_MENU_QUICK_VIEW_HOLD_ZOOM		:	nEnum = DSC_MENU_QUICKVIEW_HOLD_ZOOM;	break;
		}
		break;
	}

	return nEnum;
}

int COptionDSCSet2Dlg::ChangeStringValueToIndex(int nPropIndex, CString strPropValue)
{
	int nValueIndex = 0;

	switch(nPropIndex)
	{
	case DSC_OPT_DSC2_DATETIME_TIMEZONE:
		{
			for(int i = 0; i < ARRAYSIZE(SZOptDSCSet2TimeZoneValue); i++)
			{
				if(strPropValue.CompareNoCase(SZOptDSCSet2TimeZoneValue[i].strPropValue) == 0)
				{
					nValueIndex = SZOptDSCSet2TimeZoneValue[i].nIndex;
					break;
				}
			}
		}
		break;	
	case DSC_OPT_DSC2_DATETIME_TYPE:
		{
			for(int i = 0; i < ARRAYSIZE(SZOptDSCSet2DateTimeTypeValue); i++)
			{
				if(strPropValue.CompareNoCase(SZOptDSCSet2DateTimeTypeValue[i].strPropValue) == 0)
				{
					nValueIndex = SZOptDSCSet2DateTimeTypeValue[i].nIndex;
					break;
				}
			}
		}
		break;
	case  DSC_OPT_DSC2_DATETIME:
		break;
	}

	return nValueIndex;
}

void COptionDSCSet2Dlg::SetDateTime(CString strDateTime)
{
	CString strDate = _T("");
	CString strTime = _T("");
	CString strValue = _T("");

	int nPos = 0;
	nPos = strDateTime.Find(_T("T"));

	strDate = strDateTime.Left(nPos);

	m_nYear	= _ttoi(strDate.Left(4));
	strValue.Format(_T("%02d"), m_nYear);
	m_edit[0].SetWindowText(strValue);

	m_nMonth = _ttoi(strDate.Mid(4,2));
	strValue.Format(_T("%02d"), m_nMonth);
	m_edit[1].SetWindowText(strValue);

	m_nDay	= _ttoi(strDate.Right(2));
	strValue.Format(_T("%02d"), m_nDay);
	m_edit[2].SetWindowText(strValue);

	strTime = strDateTime.Mid(nPos + 1);
	m_nHour	= _ttoi(strTime.Left(2));
	//CMiLRe 20141021 AM/PM 표시 및 12/24 변경 수정
	int nHour = m_nHour;
	if(m_nHourType)
 	{
		if(m_nHour>12)
		{
			nHour = m_nHour-12;
			m_edit[7].SetWindowText(_T("PM"));
		}
		else
		{
			m_edit[7].SetWindowText(_T("AM"));
		}
	}
	strValue.Format(_T("%02d"), nHour);
	m_edit[3].SetWindowText(strValue);

	m_nMin  = _ttoi(strTime.Mid(2,2));
	strValue.Format(_T("%02d"), m_nMin);
	m_edit[4].SetWindowText(strValue);

	UpdateData(TRUE);
}

CString COptionDSCSet2Dlg::GetDateTime()
{
	CString strDateTime = _T("");
	strDateTime.Format(_T("%.4d%.2d%.2dT%.2d%.2d00.0"), m_nYear, m_nMonth, m_nDay, m_nHour, m_nMin);

	return strDateTime;
}

void COptionDSCSet2Dlg::SaveProp(RSDSCSet2& opt)
{
	if(!UpdateData(TRUE))
		return;

	CString strDateTime = GetDateTime();

	//-- Set TimeZone
	opt.strTimeZone =	SZOptDSCSet2TimeZoneValue[m_nTimeZone].strPropValue;
	opt.strDateTime	=	strDateTime;
	opt.strType		=	m_strType;
	opt.nDST		=	m_nDST;
	opt.nHourType	=	m_nHourType;
	opt.nImprint	=	m_nImprint;
	//dh0.seo 2014-09-15
	opt.nQuickView	=	m_nQuickView;
}

void COptionDSCSet2Dlg::OnDeltaposSpinYear(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	CString strText = _T("");
	m_edit[0].GetWindowText(strText);

	if(pNMUpDown->iDelta<0)   
	{
		if(_ttoi(strText) < 2100)		strText.Format(_T("%.4d"), _ttoi(strText) + 1);
	}
	else
	{
		if(_ttoi(strText) > 2000)		strText.Format(_T("%.4d"), _ttoi(strText) - 1);
	}
		
	m_edit[0].SetWindowText(strText);
	
	*pResult = 0;

	ChangeDateTime();
}

void COptionDSCSet2Dlg::OnDeltaposSpinMonth(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	CString strText = _T("");
	m_edit[1].GetWindowText(strText);

	if(pNMUpDown->iDelta<0)   
	{
		if(_ttoi(strText) < 12)			strText.Format(_T("%02d"), _ttoi(strText) + 1);	
	}
	else
	{
		if(_ttoi(strText) > 1)			strText.Format(_T("%02d"), _ttoi(strText) - 1);
	}

	m_edit[1].SetWindowText(strText);

	*pResult = 0;

	ChangeDateTime();
}
void COptionDSCSet2Dlg::OnDeltaposSpinDay(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	CString strText = _T("");
	m_edit[2].GetWindowText(strText);

	if(pNMUpDown->iDelta<0)  
	{
		if(_ttoi(strText) < 31)		strText.Format(_T("%02d"), _ttoi(strText) + 1);
	}
	else
	{
		if(_ttoi(strText) > 1)		strText.Format(_T("%02d"), _ttoi(strText) - 1);
	}

	m_edit[2].SetWindowText(strText);

	*pResult = 0;

	ChangeDateTime();
}
void COptionDSCSet2Dlg::OnDeltaposSpinHour(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	CString strText = _T("");
	m_edit[3].GetWindowText(strText);

	if(pNMUpDown->iDelta<0)   
	{
		if(_ttoi(strText) < 23)			strText.Format(_T("%02d"), _ttoi(strText) + 1);	
	}
	else
	{
		if(_ttoi(strText) > 0)			strText.Format(_T("%02d"), _ttoi(strText) - 1);		
	}
	
	m_edit[3].SetWindowText(strText);

	*pResult = 0;

	ChangeDateTime();
}
void COptionDSCSet2Dlg::OnDeltaposSpinMinute(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	CString strText = _T("");
	m_edit[4].GetWindowText(strText);

	if(pNMUpDown->iDelta<0)   
	{
		if(_ttoi(strText) < 59)			strText.Format(_T("%02d"), _ttoi(strText) + 1);	
	}
	else
	{
		if(_ttoi(strText) > 0)			strText.Format(_T("%02d"), _ttoi(strText) - 1);
	}

	m_edit[4].SetWindowText(strText);

	*pResult = 0;

	ChangeDateTime();
}

//------------------------------------------------------------------------------ 
//! @brief		OnSelchangeCmb
//! @date		2011-7-15
//! @author		Lee JungTaek
//! @note	 	Change Value
//------------------------------------------------------------------------------ 
void COptionDSCSet2Dlg::ChangeDateTime()
{
	UpdateData();
	CString strDateTime = GetDateTime();
	//CMiLRe 20141001 Date, Time 추가
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_OC_SAMSUNG_DATE_TIME, strDateTime);
}

void COptionDSCSet2Dlg::OnSelchangeCmbTimeZone() 
{
	UpdateData();
	CString strTimeZoneValue = SZOptDSCSet2TimeZoneValue[m_nTimeZone].strPropValue;
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_DATETIMETIMEZONE, strTimeZoneValue);
}
void COptionDSCSet2Dlg::OnSelchangeCmbType() 
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_DATETIMETYPE, m_strType);
}
void COptionDSCSet2Dlg::OnSelchangeCmbHourType() 
{
	UpdateData();
 	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_DATETIMEHOUR, m_nHourType);
}
void COptionDSCSet2Dlg::OnSelchangeCmbImprint() 
{
	UpdateData();
 	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_DATETIMEIMPRINT, m_nImprint);
}
//dh0.seo 2014-09-15 Quick View
void COptionDSCSet2Dlg::OnSelchangeCmbQuickView() 
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_QUICKVIEW, m_nQuickView);
}

//CMiLRe 20141001 Date, Time 추가
void COptionDSCSet2Dlg::OnSelchangeCmbRefresh() 
{
	RSEvent* pEvent = new RSEvent();
	pEvent->message	= WM_RS_OPT_DATE_N_TIME_UPDATE;
	pEvent->nParam1	= PTP_OC_SAMSUNG_DATE_TIME;	
	pEvent->nParam2	= 0;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pEvent->message, (LPARAM)pEvent);
}


BOOL COptionDSCSet2Dlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if( pMsg->wParam == VK_RETURN )
		{
			pMsg->wParam = NULL;	
		}
	}

	return COptionBaseDlg::PreTranslateMessage(pMsg);
}
