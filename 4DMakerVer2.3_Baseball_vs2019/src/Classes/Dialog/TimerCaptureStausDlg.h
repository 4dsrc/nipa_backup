/////////////////////////////////////////////////////////////////////////////
//
//  MultiApplyDlg.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "afxwin.h"
#include "afxcmn.h"
#include "BkDialogST.h"
#include "NXRemoteStudioFunc.h"

enum
{
	BTN_TCS_STOP,
	BTN_TCS_CNT,
};

enum
{
	TCS_STATUS_TEXT_NEXT_CAPTURE,
	TCS_STATUS_TEXT_TAKEN_NUM	,
	TCS_STATUS_TEXT_REMAIN_NUM	,
	TCS_STATUS_TEXT_CNT		,
};

enum
{
	TCS_STATUS_VALUE_NEXT_CAPTURE,
	TCS_STATUS_VALUE_TAKEN_NUM	,
	TCS_STATUS_VALUE_REMAIN_NUM	,
	TCS_STATUS_VALUE_CNT		,
};

enum
{
	TCS_STATUS_LINE_FIRST	,
	TCS_STATUS_LINE_SECOND	,
	TCS_STATUS_LINE_TIRTH	,
	TCS_STATUS_LINE_CNT		,

};

enum
{
	PT_TCS_IN_NOTHING = -1,
	PT_TCS_NOR,
	PT_TCS_INSIDE,
	PT_TCS_DOWN,
};

// CTimerCaptureStausDlg dialog
class CTimerCaptureStausDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CTimerCaptureStausDlg)

public:
	CTimerCaptureStausDlg(int nDelayTime, int nTimerCaptureTime, int nTimerCaptureCnt, CWnd* pParent = NULL);   // standard constructor
	virtual ~CTimerCaptureStausDlg();

// Dialog Data
	enum { IDD = IDD_DLG_OPT_TIME_LAPSE_STATUS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
 	afx_msg void OnPaint();	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	int m_nMouseIn;
	int	m_nStatus;

	CRect m_rectTitle;
	CRect m_rectStatusText[TCS_STATUS_LINE_CNT];
	CRect m_rectStatusValue[TCS_STATUS_LINE_CNT];
	CRect m_rectBtn;

	//-- Value
	CString m_strRemainTime;
	CString m_strRemainCnt;
	CString m_strAlreadyCnt;

	int m_nDealyTime;
	int m_nTimerCaptureTime;
	int m_nTimerCaptureCnt;
	
	void SetItemPosition();
	void DrawBackground();	
	void DrawTitle(CDC* pDC);
	void DrawLine(int nLine, CDC* pDC);
	void DrawStatusText(int nItem, CDC* pDC);
	void DrawStatusValue(int nItem, CDC* pDC);
	void DrawMenu(int nItem, CDC* pDC);

	//-- To Control Mouse Event
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	int PtInRectName(CPoint pt);
	void ChangeStatus(int nItem, int nStatus);
	void ChangeStatusAll();
	void MouseEvent();
public:
	void SetItemValue();
	void UpdateCapturedCnt(int nCnt);
	void UpdateRemainTime(int nTime);
};
