#pragma once
/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelWBColorDetailDlg.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-07-01
//
/////////////////////////////////////////////////////////////////////////////

// CSmartPanelWBColorDetailDlg dialog
#include "BKDialogST.h"
#include "NXRemoteStudioFunc.h"

enum
{
	BTN_WB_COLORTEMP_NULL = -1,
	BTN_WB_COLORTEMP_LEFT,
	BTN_WB_COLORTEMP_RIGHT,
	BTN_WB_COLORTEMP_RESET,
	BTN_WB_COLORTEMP_OK,
	BTN_WB_COLORTEMP_CNT	,
};

class CSmartPanelWBColorDetailDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CSmartPanelWBColorDetailDlg)

public:
	CSmartPanelWBColorDetailDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSmartPanelWBColorDetailDlg();

	// Dialog Data
	enum { IDD = IDD_DLG_SMARTPANEL_WB_COLORTEMP_DETAIL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	int m_nPTPCode;
	int m_nValueIndex;
	//-- Item Info
	int		m_nMouseIn;
	int		m_nStatus[BTN_WB_COLORTEMP_CNT];
	CRect m_rectBtn[BTN_WB_COLORTEMP_CNT];
	CRect m_rectIcon;
	CRect m_rectValue;
	CRect m_rectBar;
	CRect m_rectHandler;

	//-- Values
	int m_nColorTemp;
	int m_nColorTempIndex;
	int m_nColorTemp1st;
	BOOL m_bInit;

	//-- Draw Function
	void SetItemPosition();
	void DrawBackground();	
	void CleanDC(CDC* pDC);

	void DrawLine(CDC* pDC);
	void DrawMenu(int nItem, CDC* pDC);

	//-- Mouse Function
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	void ChangeStatus(int nItem, int nStatus);
	void ChangeStatusAll();
	void MouseEvent();

	//-- Get Information
	CString GetValues();
	void SetColorTempValue(BOOL bRight);		
	void SetHandler();

	//-- Button Input
	void ResetValue();
	void SetDetailValue();
	BOOL SetPosition(CPoint point);
	void SetFirstValue();

public:
	void SetValueIndex(int nValue);
	int GetDetailPropCode(int nValueIndex);
};
