/////////////////////////////////////////////////////////////////////////////
//
//  OptionTabDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "OptionTabDlg.h"
#include "SRSIndex.h"
//-- For Magnetic Windows
#include "NXRemoteStudioDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// COptionTabDlg dialog
IMPLEMENT_DYNAMIC(COptionTabDlg, CDialogEx)

//------------------------------------------------------------------------------ 
//! @brief		COptionTabDlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
COptionTabDlg::COptionTabDlg(CWnd* pParent /*=NULL*/)
	: CRSChildDialog(COptionTabDlg::IDD, pParent)
{
	SetDragMove(FALSE);
	m_nSelectedTab = OPT_TAB_TIMELAPSE;
	m_nToolTipBtn	= OPT_TAB_NOTHING;
	//CMiLRe 20141125 탭 마우스 무브시 하이라이트
	m_bFirstLoad = TRUE;
}

COptionTabDlg::~COptionTabDlg() {}
void COptionTabDlg::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);		
}				

BEGIN_MESSAGE_MAP(COptionTabDlg, CRSChildDialog)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()	
END_MESSAGE_MAP()

// CLiveview message handlers
BOOL COptionTabDlg::OnEraseBkgnd(CDC* pDC)	{	return FALSE;}
void COptionTabDlg::OnPaint()
{	
	//CMiLRe 20141125 탭 마우스 무브시 하이라이트
	if(m_bFirstLoad)
		UpdateFrames();
	CDialogEx::OnPaint();
}

BOOL COptionTabDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();
	//-- 2011-05-30
	CreateFrames();
	//-- 2011-05-18 hongsu.jung
	UpdateFrames();
	return TRUE; 	
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionTabDlg::UpdateFrames()
{
	//-- Draw Back Ground
	//CMiLRe 20141125 탭 마우스 무브시 하이라이트
	DrawBackGround(m_nSelectedTab);	
	//Invalidate(false);
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
static const int btn_gap		= 3;

void COptionTabDlg::CreateFrames()
{
	//-- Button
	for(int i = OPT_TAB_TIMELAPSE ; i < OPT_TAB_CNT ; i ++)
		m_btnRect[i] = CRect(CPoint(48*i,0),  CSize(46,39));
	
	//-- 2011-07-14
	//-- Create Tooltip
	if (m_ToolTip.m_hWnd == NULL) 
	{ 
		// Create ToolTip control 
		m_ToolTip.Create(this); 
		// Create inactive 
		m_ToolTip.Activate(FALSE);
	} 

}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackGround
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
//CMiLRe 20141125 탭 마우스 무브시 하이라이트
void COptionTabDlg::DrawBackGround(int nOptTab)
{
	if(!ISSet())
		return;
	
	//-- Rounding Dialog
	CPaintDC dc(this);
	CString strPath = RSGetRoot();
	CString strFullPath;
	//-- Draw
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_tab_blank.png"),strPath);	
	DrawGraphicsImage(&dc, strFullPath, CPoint(0,0), CSize(1,39));

	for(int i = OPT_TAB_TIMELAPSE ; i < OPT_TAB_CNT ; i ++)
	{
		if(i == m_nSelectedTab)
		{
			strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_tab_0%d_select.png"),strPath,i+1);	
			if(i == OPT_TAB_TIMELAPSE)
				DrawGraphicsImage(&dc, strFullPath, m_btnRect[i].TopLeft(), CSize(49,39));	
			else if(i == OPT_TAB_CNT-1)
				DrawGraphicsImage(&dc, strFullPath, CPoint(m_btnRect[i].left -2, 0), CSize(49,39));
			else
				DrawGraphicsImage(&dc, strFullPath, CPoint(m_btnRect[i].left -2, 0), CSize(50,39));
		}
		else
		{
			strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_tab_0%d.png"),strPath,i+1);	
			DrawGraphicsImage(&dc, strFullPath, m_btnRect[i].TopLeft(), m_btnRect[i].Size());
			//-- Draw Separeter
			if(i<OPT_TAB_CNT-1 && i != m_nSelectedTab-1)
			{
				strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_tab_separate.png"),strPath);	
				DrawGraphicsImage(&dc, strFullPath, CPoint(48*(i+1) - 2, 0),  CSize(2,39));
			}
		}

		//CMiLRe 20141125 탭 마우스 무브시 하이라이트
		if(i == nOptTab)
		{
			if(m_nSelectedTab == nOptTab) continue;
			strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_tab_0%d_Point.png"),strPath,i+1);					
			if(i == OPT_TAB_TIMELAPSE)
				DrawGraphicsImage(&dc, strFullPath, m_btnRect[i].TopLeft(), CSize(49,39));	
			else if(i == OPT_TAB_CNT-1)
				DrawGraphicsImage(&dc, strFullPath, CPoint(m_btnRect[i].left -2, 0), CSize(49,39));
			else
				DrawGraphicsImage(&dc, strFullPath, CPoint(m_btnRect[i].left -2, 0), CSize(50,39));
		}
	}		


	if(m_nSelectedTab != OPT_TAB_CNT-1 )
	{
		strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_tab_blank.png"),strPath);	
		DrawGraphicsImage(&dc, strFullPath, CPoint(48*(OPT_TAB_CNT) - 2,0), CSize(1,39));
	}
	//CMiLRe 20141125 탭 마우스 무브시 하이라이트
	m_bFirstLoad = FALSE;
	Invalidate(FALSE);	
}


//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionTabDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	
	if(nItem != OPT_TAB_NOTHING)
	{
		int nBefore = m_nSelectedTab;
		m_nSelectedTab = nItem;
		CRect rectRedraw;
		//-- Changed		
		rectRedraw.CopyRect(m_btnRect[nBefore]);
		rectRedraw.left -=2;
		rectRedraw.right += 2;
		InvalidateRect(rectRedraw);
		//-- Changing
		rectRedraw.CopyRect(m_btnRect[m_nSelectedTab]);
		rectRedraw.left -=2;
		rectRedraw.right += 2;
		InvalidateRect(rectRedraw);
		//-- Change Body Dialog
		::SendMessage(GetParent()->m_hWnd, TAB_CHANGE, (WPARAM)nBefore, (LPARAM)nItem );
	}
	CDialog::OnLButtonDown(nFlags, point);	
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionTabDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	CDialog::OnLButtonUp(nFlags, point);
}


//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int COptionTabDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = OPT_TAB_NOTHING;
	//CMiLRe 20140919 모델에 따라 ETC 창 안열림
	if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
	{
		for(int i = 0; i < OPT_TAB_CNT-1; i ++)
		{
			if(m_btnRect[i].PtInRect(pt))
				nReturn = i;
		}
	}
	else if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
	{
		for(int i = 0; i < OPT_TAB_CNT; i ++)
		{
			if(m_btnRect[i].PtInRect(pt))
				nReturn = i;
		}
	}
	return nReturn;
}


//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-07-14
//! @author	hongsu.jung
//---------------------------------------------------------------------------
BOOL COptionTabDlg::PreTranslateMessage(MSG* pMsg) 
{
	//if(pMsg->message == WM_MOUSEMOVE)
	m_ToolTip.RelayEvent(pMsg);
	return CDialog::PreTranslateMessage(pMsg);
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionTabDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	int nOnBtn = PtInRect(point, TRUE);
	if(m_nToolTipBtn != nOnBtn)
		m_ToolTip.Pop();	
	//-- Half Shutter
	if(nOnBtn > OPT_TAB_NOTHING && nOnBtn < OPT_TAB_CNT )
	{
		CString strToolTip;
		switch(nOnBtn)
		{
			//CMiLRe 20141125 탭 마우스 무브시 하이라이트
			case OPT_TAB_TIMELAPSE		: strToolTip.Format(_T("Timer Capture"));		DrawBackGround(OPT_TAB_TIMELAPSE);	break;
			case OPT_TAB_CUSTOM			: strToolTip.Format(_T("Custom"));				DrawBackGround(OPT_TAB_CUSTOM);	break;
			case OPT_TAB_DSC_SET1		: strToolTip.Format(_T("DSC Setting 1"));			DrawBackGround(OPT_TAB_DSC_SET1);	break;
			case OPT_TAB_DSC_SET2		: strToolTip.Format(_T("DSC Setting 2"));			DrawBackGround(OPT_TAB_DSC_SET2);	break;
			case OPT_TAB_DSC_SET3		: strToolTip.Format(_T("DSC Setting 3"));			DrawBackGround(OPT_TAB_DSC_SET3);	break;
			case OPT_TAB_PC_SET1		: strToolTip.Format(_T("PC Setting 1"));			DrawBackGround(OPT_TAB_PC_SET1);	break;
			case OPT_TAB_PC_SET2		: strToolTip.Format(_T("PC Setting 2"));			DrawBackGround(OPT_TAB_PC_SET2);	break;
			case OPT_TAB_ABOUT			: strToolTip.Format(_T("About"));						DrawBackGround(OPT_TAB_ABOUT);	break;
			case OPT_TAB_DSC_SET_ETC	: strToolTip.Format(_T("Other Setting"));		DrawBackGround(OPT_TAB_DSC_SET_ETC);	break;
		}
		m_nToolTipBtn = nOnBtn;
		m_ToolTip.AddTool(this, (LPCTSTR)strToolTip, NULL, 0);//툴팁정의
		m_ToolTip.Activate(TRUE);//툴팁활성화
	}
	CDialog::OnMouseMove(nFlags, point);
}
