/////////////////////////////////////////////////////////////////////////////
//
//  SeparateDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SeparateDlg.h"
#include "SRSIndex.h"

// CSeparateDlg dialog

IMPLEMENT_DYNAMIC(CSeparateDlg, CRSChildDialog)

CSeparateDlg::CSeparateDlg(BOOL bVertical, CWnd* pParent /*=NULL*/)
	: CRSChildDialog(CSeparateDlg::IDD, pParent)
{	
	m_bVertical	= bVertical;		
}

CSeparateDlg::~CSeparateDlg()
{	
}

void CSeparateDlg::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);	
}

BEGIN_MESSAGE_MAP(CSeparateDlg, CRSChildDialog)	
	ON_WM_SIZE()	
	ON_WM_PAINT()	
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

BOOL CSeparateDlg::OnEraseBkgnd(CDC* pDC){return FALSE;}
void CSeparateDlg::OnPaint()
{	
	DrawBackground();
	CRSChildDialog::OnPaint();
}

// CSeparateDlg message handlers
BOOL CSeparateDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();		
	return TRUE;  // return TRUE  unless you set the focus to a control
}
void CSeparateDlg::OnSize(UINT nType, int cx, int cy) 
{
    CDialog::OnSize(nType, cx, cy);
	DrawBackground();
} 


//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
static const int line_margin	= 2;

void CSeparateDlg::DrawBackground()
{
	if(!ISSet())
		return;

	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	int nImgX, nImgY, nImgW, nImgH;
	//-- Draw Background		
	nImgX		= rect.left + line_margin;
	nImgY		= rect.top + line_margin;
	nImgH	= rect.Height() - line_margin*2;			
	nImgW	= rect.Width() - line_margin*2;
	if(IsVertical())
		DrawImage(&dc, 
				RSGetRoot(_T("img\\03.Live View\\03_liveview_side_handle-vertical.png"))
				,nImgX, nImgY, nImgW, nImgH);	
	else
		DrawImage(&dc, 
				RSGetRoot(_T("img\\03.Live View\\03_liveview_side_handle-horizon.png"))
				,nImgX, nImgY, nImgW, nImgH);
}


BOOL CSeparateDlg::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_LBUTTONDOWN)
	{
		//-- Send Message To Liveview Frame
		RSEvent* pEvent = new RSEvent();		
		pEvent ->nParam1	= m_bVertical;		
		pEvent ->message	= WM_LIVEVIEW_EVENT_SEPARATOR;
		//-- SendMessage Hide Sibling Windows
		::SendMessage(GetParent()->m_hWnd,WM_LIVEVIEW,(WPARAM)pEvent,NULL);
	}
	return CRSChildDialog::PreTranslateMessage(pMsg);
}