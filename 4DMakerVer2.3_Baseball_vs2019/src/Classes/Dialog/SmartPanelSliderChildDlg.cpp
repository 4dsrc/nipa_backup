/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelSliderChildDlg.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-09-15
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SmartPanelSliderChildDlg.h"
#include "afxdialogex.h"
#include "SmartPanelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CSmartPanelSliderChildDlg, CDialog)

CSmartPanelSliderChildDlg::CSmartPanelSliderChildDlg(CWnd* pParent /*=NULL*/)
	: CBkDialogST(CSmartPanelSliderChildDlg::IDD, pParent)
{
	m_nMouseIn = BTN_SD_NULL;	
	for(int i=0; i<BTN_SD_CNT - 1; i++)
	{
		m_nStatus[i] = BTN_SD_CNT;
		m_rectBtn[i] = CRect(0,0,0,0);
	}

	m_pRSSlider = NULL;

	for(int i = 0; i < EV_POS_CNT; i++)
	{
		int nX = 52 + i * 11;

		m_rectIndi[i] = CRect(CPoint(nX, 5), CSize(10, 35));
	}	

	m_rectBtn[BTN_SD_LEFT] = CRect(CPoint(9,10), CSize(25,24));
	m_rectBtn[BTN_SD_RIGHT] = CRect(CPoint(283,10), CSize(25,24));
}

CSmartPanelSliderChildDlg::~CSmartPanelSliderChildDlg()
{
}

void CSmartPanelSliderChildDlg::DoDataExchange(CDataExchange* pDX)
{
	CBkDialogST::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSmartPanelSliderChildDlg, CBkDialogST)
	ON_WM_PAINT()	
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()

BOOL CSmartPanelSliderChildDlg::OnInitDialog()
{
	CBkDialogST::OnInitDialog();		
	
	SetBitmap(RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_bg1_05.png")),RGB(255,255,255));

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSmartPanelSliderChildDlg::OnPaint()
{	
	DrawBackground();	
	CBkDialogST::OnPaint();
}

void CSmartPanelSliderChildDlg::DrawBackground()
{
	CPaintDC dc(this);

	CRect rect;
	GetClientRect(&rect);

	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);
	CString		strModel;
	strModel= ((CSmartPanelDlg*)GetParent())->GetTargetModel();

	if(!strModel.Compare(DEFAULT_MODEL))
		DrawImageBase(&mDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_bg2_05.png")),0, 0); //Draw Background
	else
		DrawImageBase(&mDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_bg2_03.png")),0, 0); //Draw Background
	//-- Draw Indi
	DrawImageIndicator(&mDC);
	//-- Draw Menu
	DrawMenu(BTN_SD_LEFT, &mDC);
	DrawMenu(BTN_SD_RIGHT, &mDC);	

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);

	Invalidate(FALSE);
}

void CSmartPanelSliderChildDlg::DrawImageIndicator(CDC* pDC)
{
	Rect		DesRect;
	CRect		clRect;
	//-- Draw Indicator
	Image ImgIndi(RSGetRoot(_T("img\\02.Smart Panel\\02_ev_indi.png")));

	this->GetClientRect(&clRect);
	DesRect.X = (clRect.left + clRect.right)/2 - ImgIndi.GetWidth()/2 + SetIndiPos() + 3;
	DesRect.Y = clRect.bottom - ImgIndi.GetHeight() - 10;
	DesRect.Width = ImgIndi.GetWidth();
	DesRect.Height = ImgIndi.GetHeight();

	Graphics gc(pDC->GetSafeHdc());
	gc.DrawImage(&ImgIndi, DesRect);
}
//------------------------------------------------------------------------------ 
//! @brief		DrawDlgValue
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
#define EV_3STEP_MOVE_PIXEL 11
#define EV_3STEP_MOVE_INDEX 9
#define EV_3STEP_MOVE_INDEX_ 15
#define EV_3STEP_MOVE_PIXEL_ 8

int CSmartPanelSliderChildDlg::SetIndiPos()
{
	int nPos = 0;
	//NX1 dh0.seo 2014-08-14
	CString		strModel;
	strModel= ((CSmartPanelDlg*)GetParent())->GetTargetModel();
	
	if(!strModel.Compare(DEFAULT_MODEL))
	{
		if(m_pRSSlider->m_nEVIndex < DSC_EV_MINUS_0)
			nPos = -EV_3STEP_MOVE_PIXEL_ * EV_3STEP_MOVE_INDEX_ + (EV_3STEP_MOVE_PIXEL_ * ((m_pRSSlider->m_nEVIndex)% 15));
		else if(m_pRSSlider->m_nEVIndex == DSC_EV_PLUS_5_0)
			nPos = EV_3STEP_MOVE_PIXEL_ * EV_3STEP_MOVE_INDEX_;
		else if(m_pRSSlider->m_nEVIndex > DSC_EV_MINUS_0)
			nPos = EV_3STEP_MOVE_PIXEL_ * (m_pRSSlider->m_nEVIndex % 15);
		else
			nPos = 0;
	}
	else
	{
		if(m_pRSSlider->m_nEVIndex < DSC_EV_MINUS_0)
			nPos = -EV_3STEP_MOVE_PIXEL * EV_3STEP_MOVE_INDEX + (EV_3STEP_MOVE_PIXEL * ((m_pRSSlider->m_nEVIndex)% 9));
		else if(m_pRSSlider->m_nEVIndex == DSC_EV_PLUS_3_0)
			nPos = EV_3STEP_MOVE_PIXEL * EV_3STEP_MOVE_INDEX;
		else if(m_pRSSlider->m_nEVIndex > DSC_EV_MINUS_0)
			nPos = EV_3STEP_MOVE_PIXEL * (m_pRSSlider->m_nEVIndex % 9);
		else
			nPos = 0;
	}
	CString temp;
	temp.Format(_T("nPos : %d\n\n"), nPos);
	TRACE(temp);
	return nPos;
}

//------------------------------------------------------------------------------ 
//! @brief		DrawMenu
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Draw Buttons
//------------------------------------------------------------------------------ 
void CSmartPanelSliderChildDlg::DrawMenu(int nItem, CDC* pDC)
{
	int nX, nY, nW, nH;
	nX	= m_rectBtn[nItem].left;	
	nY	= m_rectBtn[nItem].top;
	nW	= m_rectBtn[nItem].Width();
	nH	= m_rectBtn[nItem].Height();

	switch(nItem)
	{
	case BTN_SD_LEFT:
		switch(m_nStatus[nItem])
		{
		case PT_SD_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_focus.png")),		nX, nY,nW, nH);	break;
		case PT_SD_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_push.png")),		nX, nY,nW, nH);	break;
		case PT_SD_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_disable.png")),	nX, nY,nW, nH);	break;
		default	:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_normal.png")),		nX, nY,nW, nH);	break;
		}
		break;
	case BTN_SD_RIGHT:
		switch(m_nStatus[nItem])
		{
		case PT_SD_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_focus.png")),		nX, nY,nW, nH);	break;
		case PT_SD_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_push.png")),		nX, nY,nW, nH);	break;
		case PT_SD_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_disable.png")),	nX, nY,nW, nH);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_normal.png")),	nX, nY,nW, nH);	break;
		}
		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelSliderChildDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	m_nMouseIn = PtInRect(point, TRUE);
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

void CSmartPanelSliderChildDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CSmartPanelSliderChildDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_SD_CNT; i ++)
		ChangeStatus(i,PT_SD_NOR);

	CPoint point;
	GetCursorPos(&point);
	ScreenToClient(&point);

	CRect rect;
	GetClientRect(&rect);

	if(!rect.PtInRect(point)) 
		this->ShowWindow(SW_HIDE);

	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelSliderChildDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	

 	switch(nItem)
 	{
 	case BTN_SD_LEFT			:	m_pRSSlider->SetPos(FALSE);				break;	
 	case BTN_SD_RIGHT			:	m_pRSSlider->SetPos(TRUE);				break;	
 	default:						SetPropValueIndex(PtInRectValue(point));	break;
	}
 
 	//-- 2011-8-2 Lee JungTaek
 	Invalidate(FALSE);

	CDialog::OnLButtonDown(nFlags, point);	
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelSliderChildDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ChangeStatusAll();	
	CDialog::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CSmartPanelSliderChildDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = PT_SD_IN_NOTHING;
	int nStart = 0;

	for(int i = nStart ; i < BTN_SD_CNT; i ++)
	{
		if(m_rectBtn[i].PtInRect(pt))
		{
			nReturn = i;
			if(bMove)
				ChangeStatus(i,PT_SD_INSIDE);
			else
				ChangeStatus(i,PT_SD_DOWN); 
		}
		else
			ChangeStatus(i,PT_SD_NOR); 
	}
	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelSliderChildDlg::ChangeStatus(int nItem, int nStatus)
{
	if(m_nStatus[nItem] != nStatus)
	{
		m_nStatus[nItem] = nStatus;
		CPaintDC dc(this);
		DrawMenu(nItem, &dc);
		InvalidateRect(m_rectBtn[nItem],FALSE);
	}
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelSliderChildDlg::ChangeStatusAll()
{
	int nStart = 0;
	for(int i = nStart; i < BTN_SD_CNT; i ++)
	{
		if(m_nMouseIn == i)
			ChangeStatus(i,PT_SD_INSIDE); 
		else
			ChangeStatus(i,PT_SD_NOR); 
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRectValue
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CSmartPanelSliderChildDlg::PtInRectValue(CPoint point)
{
	int i = 0;

	if(point.x >= 36 && point.x <= 280)
	{
		i = (point.x - 36)/8;
		return i;
	}

/*	for(int i = 0 ; i < EV_POS_CNT; i ++)
	{
		if(m_rectIndi[i].PtInRect(point))
			return i;
	} */

	return EV_POS_NULL;
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropValueIndex
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelSliderChildDlg::SetPropValueIndex(int nIndex)
{
	if(nIndex == EV_POS_NULL)		return;

	int nCurIndex = m_pRSSlider->m_nEVIndex;

	if(nIndex > nCurIndex)
	{
		//-- Just Change
		for(int i = nCurIndex; i < nIndex - 1; i++)
			m_pRSSlider->SetPos(TRUE, FALSE);
		//-- Set Real
		m_pRSSlider->SetPos(TRUE);
	}
	else
	{
		//-- Just Change
		for(int i = nCurIndex; i > nIndex + 1; i--)
			m_pRSSlider->SetPos(FALSE, FALSE);
		//-- Set Real
		m_pRSSlider->SetPos(FALSE);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
BOOL CSmartPanelSliderChildDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_MOUSEWHEEL)
	{
		BOOL bMouseWheelUp;
		int delta = GET_WHEEL_DELTA_WPARAM(pMsg->wParam);

		if(delta < 0)		bMouseWheelUp = FALSE;
		else				bMouseWheelUp = TRUE;

		m_pRSSlider->SetPos(bMouseWheelUp);
		Invalidate(FALSE);
		
		return TRUE;
	}		

	return CBkDialogST::PreTranslateMessage(pMsg);
}