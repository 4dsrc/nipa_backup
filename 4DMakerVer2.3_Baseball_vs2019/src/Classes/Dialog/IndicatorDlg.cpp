/////////////////////////////////////////////////////////////////////////////
//
//  IndicatorDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "IndicatorDlg.h"
#include "NXRemoteStudioDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const int first_line_top		= 5;
static const int second_line_top	= 33;
static const int third_line_top		= 57;
static const int fourth_line_top	= 81;

static const int first_line_left		= 5;
static const int second_line_left	= 42;
static const int third_line_left		= 80;
static const int fourth_line_left	= 112;

static const int big_icon_width	= 32;
static const int big_icon_height	= 28;
static const int small_icon_width	= 28;
static const int small_icon_height	= 22;

static const int string_normal_icon_width = 8;
static const int string_dot_icon_width = 3;
static const int string_normal_icon_height = 13;


// CIndicatorDlg dialog
IMPLEMENT_DYNAMIC(CIndicatorDlg,  CRSChildDialog)

CIndicatorDlg::CIndicatorDlg(CWnd* pParent /*=NULL*/)
	: CRSChildDialog(CIndicatorDlg::IDD, pParent)
{
	//-- 2011-06-13 hongsu.jung
	//-- Default Indicator
	SetDefaultIndicator();	

	m_bDotString = FALSE;
	//CMiLRe 20140902 DRIVER GET 설정
	m_strModel = _T("");
	m_nCaptureCount = 0;
	m_bAutoISO = FALSE;
}

CIndicatorDlg::~CIndicatorDlg()
{	
}

void CIndicatorDlg::SetDefaultIndicator()
{
	//-- Set Default Nothing Infomation
	m_nIndicator[RS_INDICATOR_MODE		] = DSC_MODE_NULL;
	m_nIndicator[RS_INDICATOR_DSC_NAME	] = DSC_NAME_NULL;
	m_nIndicator[RS_INDICATOR_METERING	] = DSC_METERING_NULL;
	m_nIndicator[RS_INDICATOR_WB		] = DSC_WB_NULL;
	m_nIndicator[RS_INDICATOR_ISO		] = DSC_ISO_NULL;
	m_nIndicator[RS_INDICATOR_FLASH		] = DSC_FLASH_NULL;
	m_nIndicator[RS_INDICATOR_DRIVE		] = DSC_DRIVE_NULL;
	m_nIndicator[RS_INDICATOR_EV		] = DSC_EV_NULL;
	m_nIndicator[RS_INDICATOR_BATTERY	] = DSC_BATTERY_NULL;
	m_nIndicator[RS_INDICATOR_FNUMBER	] = DSC_FNUMBER_NULL;
	m_nIndicator[RS_INDICATOR_SHUTTERSPEED	] = DSC_SHUTTERSPEED_NULL;

	m_rect[RS_INDICATOR_MODE		] = CRect(CPoint(first_line_left, first_line_top),			CSize(big_icon_width, big_icon_height));
	m_rect[RS_INDICATOR_DSC_NAME	] = CRect(CPoint(first_line_left + big_icon_width + 13, first_line_top), CSize(big_icon_width, 110));
	m_rect[RS_INDICATOR_SHUTTERSPEED	] = CRect(CPoint(first_line_left, second_line_top + (big_icon_height - string_normal_icon_height) / 2),	CSize(big_icon_width, big_icon_height));	
	m_rect[RS_INDICATOR_FNUMBER	] = CRect(CPoint((second_line_left + third_line_left) / 2 + 10, second_line_top + (big_icon_height - string_normal_icon_height) / 2),	CSize(big_icon_width, big_icon_height));
	m_rect[RS_INDICATOR_DRIVE		] = CRect(CPoint(fourth_line_left, second_line_top),	CSize(small_icon_width, small_icon_height));	
	m_rect[RS_INDICATOR_WB			] = CRect(CPoint(first_line_left, third_line_top),			CSize(small_icon_width, small_icon_height));	
	m_rect[RS_INDICATOR_ISO		] = CRect(CPoint(second_line_left, third_line_top),		CSize(small_icon_width, small_icon_height));	
	m_rect[RS_INDICATOR_METERING	] = CRect(CPoint(third_line_left, third_line_top),			CSize(small_icon_width, small_icon_height));
	m_rect[RS_INDICATOR_FLASH		] = CRect(CPoint(fourth_line_left, third_line_top),		CSize(small_icon_width, small_icon_height));		
	m_rect[RS_INDICATOR_BATTERY	] = CRect(CPoint(fourth_line_left, fourth_line_top),		CSize(small_icon_width, small_icon_height));
	m_rect[RS_INDICATOR_CAPTURE		] = CRect(CPoint(first_line_left, first_line_top),			CSize(big_icon_width, big_icon_height));
}

void CIndicatorDlg::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);		
}

BEGIN_MESSAGE_MAP(CIndicatorDlg, CRSChildDialog)	
	ON_WM_SIZE()	
	ON_WM_PAINT()	
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

void CIndicatorDlg::OnPaint()
{	
	DrawBackground();
	CRSChildDialog::OnPaint();
}

// CIndicatorDlg message handlers
BOOL CIndicatorDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();		
	return TRUE;  // return TRUE  unless you set the focus to a control
}
void CIndicatorDlg::OnSize(UINT nType, int cx, int cy) 
{
    CDialog::OnSize(nType, cx, cy);
	DrawBackground();
} 


//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CIndicatorDlg::DrawBackground()
{
	if(!ISSet())
		return;
		
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);
	
	int nImgX, nImgY, nImgW, nImgH;
	//-- Draw Background		
	nImgX		= rect.left;	
	nImgY		= rect.top;
	nImgH	= rect.Height();			
	nImgW	= rect.Width();
//	DrawImage(&mDC, RSGetRoot(_T("img\\01.Light Studio\\indicator.png")), nImgX, nImgY, nImgW, nImgH);	
	DrawImage(&mDC, RSGetRoot(_T("img\\01.Light Studio\\indicator_.png")), nImgX, nImgY, nImgW, nImgH);	
	//--
	//-- Draw Items
	//--
	DrawIndicator(RS_INDICATOR_MODE		,&mDC);
	DrawIndicator(RS_INDICATOR_DSC_NAME, &mDC);
	DrawIndicator(RS_INDICATOR_SHUTTERSPEED	,&mDC);
	DrawIndicator(RS_INDICATOR_FNUMBER		,&mDC);
	DrawIndicator(RS_INDICATOR_DRIVE		,&mDC);
	DrawIndicator(RS_INDICATOR_WB			,&mDC);
	DrawIndicator(RS_INDICATOR_ISO			,&mDC);
	DrawIndicator(RS_INDICATOR_METERING	,&mDC);
	DrawIndicator(RS_INDICATOR_FLASH		,&mDC);
	DrawIndicator(RS_INDICATOR_EV			,&mDC);
	DrawIndicator(RS_INDICATOR_BATTERY		,&mDC);
	DrawIndicator(RS_INDICATOR_CAPTURE		,&mDC);
	
	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		SetItem
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CIndicatorDlg::SetItem(int nPTPCode, int nValue)
{
	//CMiLRe 20140902 DRIVER GET 설정
	m_strModel = ((CNXRemoteStudioDlg*)GetParent())->GetModel();

	int nIndicator = 0, nEnum = 0;
	//-- Set Value
	switch(nPTPCode)
	{
		case	PTP_CODE_FUNCTIONALMODE:	nIndicator = RS_INDICATOR_MODE;
			switch(nValue)
			{
			case eUCS_UI_MODE_SMARTAUTO					:	nEnum = DSC_MODE_SMART						;			break;
			case eUCS_UI_MODE_PROGRAM					:	nEnum = DSC_MODE_P							;			break;
			case eUCS_UI_MODE_A							:	nEnum = DSC_MODE_A							;			break;
			case eUCS_UI_MODE_S							:	nEnum = DSC_MODE_S							;			break;
			case eUCS_UI_MODE_M							:	nEnum = DSC_MODE_M							;			break;			
			case eUCS_UI_MODE_BULB						:	nEnum = DSC_MODE_BULB						;			break; 
			case eUCS_UI_MODE_I							:	nEnum = DSC_MODE_I							;			break;
			case eUCS_UI_MODE_CAPTURE_MOVIE				:	nEnum = DSC_MODE_CAPTURE_MOVIE				;			break;
			case eUCS_UI_MODE_MAGIC_MAGIC_FRAME			:	nEnum = DSC_MODE_MAGIC_MAGIC_FRAME			;			break;
			case eUCS_UI_MODE_MAGIC_SMART_FILTER		:	nEnum = DSC_MODE_MAGIC_SMART_FILTER			;			break;
			case eUCS_UI_MODE_SCENE_BEAUTY				:	nEnum = CheckIModeScene(eUCS_UI_MODE_SCENE_BEAUTY);		break;		
			case eUCS_UI_MODE_SCENE_NIGHT				:	nEnum = CheckIModeScene(eUCS_UI_MODE_SCENE_NIGHT);		break;		
			case eUCS_UI_MODE_SCENE_LANDSCAPE			:	nEnum = CheckIModeScene(eUCS_UI_MODE_SCENE_LANDSCAPE);	break;		
			case eUCS_UI_MODE_SCENE_PORTRAIT			:	nEnum = CheckIModeScene(eUCS_UI_MODE_SCENE_PORTRAIT	);	break;		
			case eUCS_UI_MODE_SCENE_CHILDREN			:	nEnum = CheckIModeScene(eUCS_UI_MODE_SCENE_CHILDREN	);	break;		
			case eUCS_UI_MODE_SCENE_SPORTS				:	nEnum = DSC_MODE_SCENE_SPORTS				;			break;
			case eUCS_UI_MODE_SCENE_CLOSE_UP			:	nEnum = DSC_MODE_SCENE_CLOSE_UP				;			break;
			case eUCS_UI_MODE_SCENE_TEXT				:	nEnum = DSC_MODE_SCENE_TEXT					;			break;
			case eUCS_UI_MODE_SCENE_SUNSET				:	nEnum = CheckIModeScene(eUCS_UI_MODE_SCENE_SUNSET	);	break;		
			case eUCS_UI_MODE_SCENE_DAWN				:	nEnum = CheckIModeScene(eUCS_UI_MODE_SCENE_DAWN	);		break;		
			case eUCS_UI_MODE_SCENE_BACKLIGHT			:	nEnum = CheckIModeScene(eUCS_UI_MODE_SCENE_BACKLIGHT);	break;		
			case eUCS_UI_MODE_SCENE_FIREWORK			:	nEnum = DSC_MODE_SCENE_FIREWORK				;			break;
			case eUCS_UI_MODE_SCENE_BEACH_SNOW			:	nEnum = DSC_MODE_SCENE_BEACH_SNOW			;			break;
			case eUCS_UI_MODE_SCENE_SOUND_PICTURE		:	nEnum = DSC_MODE_SCENE_SOUND_PICTURE		;			break;
			case eUCS_UI_MODE_SCENE_3D					:	nEnum = DSC_MODE_SCENE_3D					;			break;
			case eUCS_UI_MODE_MOVIE						:	nEnum = DSC_MODE_MOVIE						;			break;
			// dh9.seo 2013-06-24
			case eUCS_UI_MODE_SCENE_BEST				:	nEnum = DSC_MODE_SCENE_BEST					;	break;
			case eUCS_UI_MODE_SCENE_MACRO				:	nEnum = DSC_MODE_SCENE_MACRO				;	break;
			case eUCS_UI_MODE_SCENE_ACTION				:	nEnum = DSC_MODE_SCENE_ACTION				;	break;
			case eUCS_UI_MODE_SCENE_RICH				:	nEnum = DSC_MODE_SCENE_RICH					;	break;
			case eUCS_UI_MODE_SCENE_WATERFALL			:	nEnum = DSC_MODE_SCENE_WATERFALL			;	break;
			case eUCS_UI_MODE_SCENE_SILHOUETTE			:	nEnum = DSC_MODE_SCENE_SILHOUETTE			;	break;
			case eUCS_UI_MODE_SCENE_LIGHT				:	nEnum = DSC_MODE_SCENE_LIGHT				;	break;
			case eUCS_UI_MODE_SCENE_CREATIVE			:	nEnum = DSC_MODE_SCENE_CREATIVE				;	break;
			case eUCS_UI_MODE_SCENE_PANORAMA			:	nEnum = DSC_MODE_SCENE_PANORAMA				;	break;
			//NX1 dh0.seo 2014-08-19
			case eUCS_UI_MODE_ACTION_FREEZE				:	nEnum = DSC_MODE_SCENE_ACTION_FREEZE		;	break;
			case eUCS_UI_MODE_LIGHT_TRACE				:	nEnum = DSC_MODE_SCENE_LIGHT_TRACE			;	break;	
			case eUCS_UI_MODE_SCENE_MULTI_EXPOSURE		:	nEnum = DSC_MODE_SCENE_MULTI_EXPOSURE		;	break;
			case eUCS_UI_MODE_SCENE_AUTO_SHUTTER		:	nEnum = DSC_MODE_SCENE_AUTO_SHUTTER			;	break;
			case eUCS_UI_MODE_PANORAMA					:	nEnum = DSC_MODE_PANORAMA					;			break;
			default:										nEnum = DSC_MODE_NULL						;	break;
			}
			break;
		case	PTP_CODE_BATTERYLEVEL:	nIndicator = RS_INDICATOR_BATTERY;
			if(!m_strModel.Compare(_T("NX1")))
			{
				m_nValue = nValue;
			}
			else
			{
				switch(nValue)
				{	
				case eUCS_BATT_LEVEL_MIDBAR3	:	nEnum = DSC_BATTERY_FULL;	break;
				case eUCS_BATT_LEVEL_MIDBAR2	:	nEnum = DSC_BATTERY_2	;	break;
				case eUCS_BATT_LEVEL_MIDBAR1	:	nEnum = DSC_BATTERY_1	;	break;
				case eUCS_BATT_LEVEL_MIDBAR0	:	
				case eUCS_BATT_LEVEL_RED		:	
				case eUCS_BATT_LEVEL_OFF		:	nEnum = DSC_BATTERY_0	;	break;
				default:							nEnum = DSC_BATTERY_NULL;		break;
				}
				break;
			}
			nEnum = nValue;
			break;
		case	PTP_CODE_SAMSUNG_SHUTTERSPEED:	nIndicator = RS_INDICATOR_SHUTTERSPEED;
			nEnum = nValue;
			break;
		case	PTP_CODE_F_NUMBER:				nIndicator = RS_INDICATOR_FNUMBER;
			nEnum = nValue;
			break;
		case	PTP_CODE_WHITEBALANCE:	nIndicator = RS_INDICATOR_WB;
			switch(nValue)
			{
			case eUCS_CAP_WB_AUTO			:				nEnum = DSC_WB_AUTO			;	break;
			case eUCS_CAP_WB_AUTO_TUNGSTEN	:				nEnum = DSC_WB_AUTO_TUNGSTEN;	break;
			case eUCS_CAP_WB_DAYLIGHT		:				nEnum = DSC_WB_DAYLIGHT		;	break;
			case eUCS_CAP_WB_CLOUDY			:				nEnum = DSC_WB_CLOUDY		;	break;
			case eUCS_CAP_WB_FLUORESCENT_W	:				nEnum = DSC_WB_FLUORESCENT_W;	break;
			case eUCS_CAP_WB_FLUORESCENT_NW	:				nEnum = DSC_WB_FLUORESCENT_N;	break;
			case eUCS_CAP_WB_FLUORESCENT_D	:				nEnum = DSC_WB_FLUORESCENT_D;	break;
			case eUCS_CAP_WB_TUNGSTEN		:				nEnum = DSC_WB_TUNGSTEN;		break;
			case eUCS_CAP_WB_FLASH			:				nEnum = DSC_WB_FLASH	;		break;
			case eUCS_CAP_WB_CUSTOM			:				nEnum = DSC_WB_CUSTOM	;		break;
			case eUCS_CAP_WB_K				:				nEnum = DSC_WB_K		;		break;
			default:										nEnum = DSC_WB_NULL;			break;
			}
			break;
		case	PTP_CODE_FLASHMODE:	nIndicator = RS_INDICATOR_FLASH;
			switch(nValue)
			{
			case eUCS_CAP_FLASH_OFF			:				nEnum = DSC_FLASH_OFF			;		break;		
			case eUCS_CAP_FLASH_SMART		:				nEnum = DSC_FLASH_SMART			;		break;
			case eUCS_CAP_FLASH_AUTO		:				nEnum = DSC_FLASH_AUTO			;		break;
			case eUCS_CAP_FLASH_RED_EYE		:				nEnum = DSC_FLASH_RED			;		break;
			case eUCS_CAP_FLASH_FILL_IN		:				nEnum = DSC_FLASH_FILLIN		;		break;
			case eUCS_CAP_FLASH_FILL_IN_RED	:				nEnum = DSC_FLASH_FILLIN_RED	;		break;
			case eUCS_CAP_FLASH_1ST_CURTAIN	:				nEnum = DSC_FLASH_1ST_CURTAIN	;		break;
			case eUCS_CAP_FLASH_2ND_CURTAIN	:				nEnum = DSC_FLASH_2ND_CURTAIN	;		break;
			default:										nEnum = DSC_FLASH_NULL			;		break;
			}
			break;		
		//CMiLRe 20140902 DRIVER GET 설정
		case PTP_DPC_STILL_CAPTURE_MODE_H_FRAME:		
		case PTP_DPC_STILL_CAPTURE_MODE_L_FRAME:
		case	PTP_CODE_STILLCAPTUREMODE:	nIndicator = RS_INDICATOR_DRIVE;
			if(m_strModel.Compare(modelType[MDOEL_NX2000].strModelName) == 0)
			{
				switch(nValue)
				{
					//			case eUCS_CAP_DRIVE_OFF				:			nEnum = DSC_DRIVE_OFF			;
				case eUCS_CAP_DRIVE_SINGLE			:			nEnum = DSC_DRIVE_SINGLE		;		break;
				case eUCS_CAP_DRIVE_CONTINUOUS_3FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_3FRAME	;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_4FRAME	;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_5FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_5FRAME	;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_6FRAME	;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_8FRAME	;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_10FRAME;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_12FRAME;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_15FRAME;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS		:			nEnum = DSC_DRIVE_CONTINUOUS	;		break;
				case eUCS_CAP_DRIVE_BURST_CAPTURE	:			nEnum = DSC_DRIVE_BURST			;		break;
				case eUCS_CAP_DRIVE_TIMER			:			nEnum = DSC_DRIVE_TIMER			;		break;
				case eUCS_CAP_DRIVE_BRACKET_AE		:			nEnum = DSC_DRIVE_AEB			;		break;
				case eUCS_CAP_DRIVE_BRACKET_WB		:			nEnum = DSC_DRIVE_WBB			;		break;
				case eUCS_CAP_DRIVE_BRACKET_PW		:			nEnum = DSC_DRIVE_PWB			;		break;
				case eUCS_CAP_DRIVE_DEPTH_BRACKET	:			nEnum = DSC_DRIVE_DEPTH			;		break;
				default:
					//20140903 dh.seo 인디게이터 타이머 표시
					if(nValue >= 0x0200 && nValue <= 0x1E93)
					{
						nEnum = DSC_DRIVE_TIMER;		break;
					}
					else
					{
						nEnum = DSC_MODE_NULL;		break;
					}
				}
			}
			else if(m_strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)
			{
				switch(nValue)
				{					
				case eUCS_CAP_DRIVE_SINGLE			:			nEnum = DSC_DRIVE_SINGLE_NX1		;		break;				
				case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_4FRAME_NX1	;	break;				
				case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_6FRAME_NX1	;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_8FRAME_NX1	;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_10FRAME_NX1;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_12FRAME_NX1;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:			nEnum = DSC_DRIVE_CONTINUOUS_15FRAME_NX1;	break;								
				case eUCS_CAP_DRIVE_TIMER			:			nEnum = DSC_DRIVE_TIMER_NX1			;		break;
				case eUCS_CAP_DRIVE_BRACKET_AE		:			nEnum = DSC_DRIVE_AEB_NX1			;		break;
				case eUCS_CAP_DRIVE_BRACKET_WB		:			nEnum = DSC_DRIVE_WBB_NX1			;		break;
				case eUCS_CAP_DRIVE_BRACKET_PW		:			nEnum = DSC_DRIVE_PWB_NX1			;		break;
				case eUCS_CAP_DRIVE_DEPTH_BRACKET	:			nEnum = DSC_DRIVE_DEPTH_NX1			;		break;
				default:
					//20140903 dh.seo 인디게이터 타이머 표시
					if(nValue >= 0x0200 && nValue <= 0x1E93)
					{
						int nTimer = nValue>>8;
						for(int i = 0 ; i < 29 ; i ++)
						{
							if(nTimer == i+2)
							{
								nEnum = DSC_DRIVE_TIMER_2+i;											
								break;
							}
						}
					}
					else
					{
						nEnum = DSC_MODE_NULL;		break;
					}					
				}
			}
			break;
		case PTP_DPC_SAMSUNG_AUTO_ISO_VALUE :		nIndicator = RS_INDICATOR_ISO;
			switch(nValue)
			{
			case	eUCS_CAP_ISO_1_3_100	:		nEnum = DSC_ISO_100			;	break;
			case	eUCS_CAP_ISO_1_3_125	:		nEnum = DSC_ISO_125			;	break;
			case	eUCS_CAP_ISO_1_3_160	:		nEnum = DSC_ISO_160			;	break;
			case	eUCS_CAP_ISO_1_3_200	:		nEnum = DSC_ISO_200			;	break;
			case	eUCS_CAP_ISO_1_3_250	:		nEnum = DSC_ISO_250			;	break;
			case	eUCS_CAP_ISO_1_3_320	:		nEnum = DSC_ISO_320			;	break;
			case	eUCS_CAP_ISO_1_3_400	:		nEnum = DSC_ISO_400			;	break;
			case	eUCS_CAP_ISO_1_3_500	:		nEnum = DSC_ISO_500			;	break;
			case	eUCS_CAP_ISO_1_3_640	:		nEnum = DSC_ISO_640			;	break;
			case	eUCS_CAP_ISO_1_3_800	:		nEnum = DSC_ISO_800			;	break;
			case	eUCS_CAP_ISO_1_3_1000	:		nEnum = DSC_ISO_1000		;	break;
			case	eUCS_CAP_ISO_1_3_1250	:		nEnum = DSC_ISO_1250		;	break;
			case	eUCS_CAP_ISO_1_3_1600	:		nEnum = DSC_ISO_1600		;	break;
			case	eUCS_CAP_ISO_1_3_2000	:		nEnum = DSC_ISO_2000		;	break;
			case	eUCS_CAP_ISO_1_3_2500	:		nEnum = DSC_ISO_2500		;	break;
			case	eUCS_CAP_ISO_1_3_3200	:		nEnum = DSC_ISO_3200		;	break;
			case	eUCS_CAP_ISO_1_3_4000	:		nEnum = DSC_ISO_4000		;	break;
			case	eUCS_CAP_ISO_1_3_5000	:		nEnum = DSC_ISO_5000		;	break;
			case	eUCS_CAP_ISO_1_3_6400	:		nEnum = DSC_ISO_6400		;	break;
			case	eUCS_CAP_ISO_1_3_8000	:		nEnum = DSC_ISO_8000		;	break;
			case	eUCS_CAP_ISO_1_3_10000	:		nEnum = DSC_ISO_10000		;	break;
			case	eUCS_CAP_ISO_1_3_12800	:		nEnum = DSC_ISO_12800		;	break;
			case	eUCS_CAP_ISO_1_3_16000	:		nEnum = DSC_ISO_16000		;	break;
			case	eUCS_CAP_ISO_1_3_20000	:		nEnum = DSC_ISO_20000		;	break;
			case	eUCS_CAP_ISO_1_3_25600	:		nEnum = DSC_ISO_25600		;	break;
			case	eUCS_CAP_ISO_1_3_51200	:		nEnum = DSC_ISO_51200		;	break;
			default:								nEnum = DSC_ISO_NULL;			break;
			}
			break;
		case	PTP_CODE_EXPOSUREINDEX:		nIndicator = RS_INDICATOR_ISO;				
			switch(nValue)
			{
			case	eUCS_CAP_ISO_1_3_AUTO	:		nEnum = DSC_ISO_AUTO		; m_bAutoISO = TRUE;	break;
			case	eUCS_CAP_ISO_1_3_100	:		nEnum = DSC_ISO_100			; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_125	:		nEnum = DSC_ISO_125			; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_160	:		nEnum = DSC_ISO_160			; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_200	:		nEnum = DSC_ISO_200			; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_250	:		nEnum = DSC_ISO_250			; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_320	:		nEnum = DSC_ISO_320			; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_400	:		nEnum = DSC_ISO_400			; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_500	:		nEnum = DSC_ISO_500			; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_640	:		nEnum = DSC_ISO_640			; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_800	:		nEnum = DSC_ISO_800			; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_1000	:		nEnum = DSC_ISO_1000		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_1250	:		nEnum = DSC_ISO_1250		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_1600	:		nEnum = DSC_ISO_1600		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_2000	:		nEnum = DSC_ISO_2000		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_2500	:		nEnum = DSC_ISO_2500		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_3200	:		nEnum = DSC_ISO_3200		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_4000	:		nEnum = DSC_ISO_4000		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_5000	:		nEnum = DSC_ISO_5000		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_6400	:		nEnum = DSC_ISO_6400		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_8000	:		nEnum = DSC_ISO_8000		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_10000	:		nEnum = DSC_ISO_10000		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_12800	:		nEnum = DSC_ISO_12800		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_16000	:		nEnum = DSC_ISO_16000		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_20000	:		nEnum = DSC_ISO_20000		; m_bAutoISO = FALSE;	break;
			case	eUCS_CAP_ISO_1_3_25600	:		nEnum = DSC_ISO_25600		; m_bAutoISO = FALSE;	break;
			//NX1 dh0.seo 2014-08-13
//			case	eUCS_CAP_ISO_1_3_32000	:		nEnum = DSC_ISO_32000		;	break;
//			case	eUCS_CAP_ISO_1_3_40000	:		nEnum = DSC_ISO_40000		;	break;
			case	eUCS_CAP_ISO_1_3_51200	:		nEnum = DSC_ISO_51200		;	break;
			default:								nEnum = DSC_ISO_NULL;			break;
			}
			break;
		case	PTP_CODE_SAMSUNG_EV:		nIndicator = RS_INDICATOR_EV;				
			switch((INT8)nValue)
			{
			case	eUCS_CAP_EVC_m_5_0:		nEnum = DSC_EV_MINUS_5_0	;	break;
			case	eUCS_CAP_EVC_m_4_6:		nEnum = DSC_EV_MINUS_4_6	;	break;
			case	eUCS_CAP_EVC_m_4_3:		nEnum = DSC_EV_MINUS_4_3	;	break;
			case	eUCS_CAP_EVC_m_4_0:		nEnum = DSC_EV_MINUS_4_0	;	break;
			case	eUCS_CAP_EVC_m_3_6:		nEnum = DSC_EV_MINUS_3_6	;	break;
			case	eUCS_CAP_EVC_m_3_3:		nEnum = DSC_EV_MINUS_3_3	;	break;
			case	eUCS_CAP_EVC_m_3_0:		nEnum = DSC_EV_MINUS_3_0	;	break;
			case	eUCS_CAP_EVC_m_2_6:		nEnum = DSC_EV_MINUS_2_6	;	break;
			case	eUCS_CAP_EVC_m_2_3:		nEnum = DSC_EV_MINUS_2_3	;	break;
			case	eUCS_CAP_EVC_m_2_0:		nEnum = DSC_EV_MINUS_2_0	;	break;
			case	eUCS_CAP_EVC_m_1_6:		nEnum = DSC_EV_MINUS_1_6	;	break;
			case	eUCS_CAP_EVC_m_1_3:		nEnum = DSC_EV_MINUS_1_3	;	break;
			case	eUCS_CAP_EVC_m_1_0:		nEnum = DSC_EV_MINUS_1_0	;	break;
			case	eUCS_CAP_EVC_m_0_6:		nEnum = DSC_EV_MINUS_0_6	;	break;
			case	eUCS_CAP_EVC_m_0_3:		nEnum = DSC_EV_MINUS_0_3	;	break;
			case	eUCS_CAP_EVC_0_0  :		nEnum = DSC_EV_MINUS_0		;	break;
			case	eUCS_CAP_EVC_p_0_3:		nEnum = DSC_EV_PLUS_0_3		;	break;
			case	eUCS_CAP_EVC_p_0_6:		nEnum = DSC_EV_PLUS_0_6		;	break;
			case	eUCS_CAP_EVC_p_1_0:		nEnum = DSC_EV_PLUS_1_0		;	break;
			case	eUCS_CAP_EVC_p_1_3:		nEnum = DSC_EV_PLUS_1_3		;	break;
			case	eUCS_CAP_EVC_p_1_6:		nEnum = DSC_EV_PLUS_1_6		;	break;
			case	eUCS_CAP_EVC_p_2_0:		nEnum = DSC_EV_PLUS_2_0		;	break;
			case	eUCS_CAP_EVC_p_2_3:		nEnum = DSC_EV_PLUS_2_3		;	break;
			case	eUCS_CAP_EVC_p_2_6:		nEnum = DSC_EV_PLUS_2_6		;	break;
			case	eUCS_CAP_EVC_p_3_0:		nEnum = DSC_EV_PLUS_3_0		;	break;
			case	eUCS_CAP_EVC_p_3_3:		nEnum = DSC_EV_PLUS_3_3		;	break;
			case	eUCS_CAP_EVC_p_3_6:		nEnum = DSC_EV_PLUS_3_6		;	break;
			case	eUCS_CAP_EVC_p_4_0:		nEnum = DSC_EV_PLUS_4_0		;	break;
			case	eUCS_CAP_EVC_p_4_3:		nEnum = DSC_EV_PLUS_4_3		;	break;
			case	eUCS_CAP_EVC_p_4_6:		nEnum = DSC_EV_PLUS_4_6		;	break;
			case	eUCS_CAP_EVC_p_5_0:		nEnum = DSC_EV_PLUS_5_0		;	break;
			default:						nEnum = DSC_EV_NULL			;	break;
			}
			break;
		case	PTP_CODE_EXPOSUREMETERINGMODE:		nIndicator = RS_INDICATOR_METERING;				
			switch(nValue)
			{
			case	eUCS_CAP_METERING_MULTI	:		nEnum = DSC_METERING_MULTI		;	break;
			case	eUCS_CAP_METERING_CENTER:		nEnum = DSC_METERING_CENTER		;	break;
			case	eUCS_CAP_METERING_SPOT	:		nEnum = DSC_METERING_SPOT		;	break;
			default:								nEnum = DSC_METERING_NULL		;	break;
			}
			break;
		//-- 2011-8-26 Lee JungTaek
		case PTP_CODE_SAMSUNG_I_SMART_FILTER:		nIndicator = RS_INDICATOR_MODE;		
			switch(nValue)
			{
			case eUCS_CAP_SMART_FILTER_I_MODE_VIGNETTING	:	nEnum = DSC_I_MODE_SMARTFILTER_VIGNETTING	;	break;
			case eUCS_CAP_SMART_FILTER_I_MODE_MINIATURE		:	nEnum = DSC_I_MODE_SMARTFILTER_MINIATURE	;	break;
			case eUCS_CAP_SMART_FILTER_I_MODE_FISH_EYE		:	nEnum = DSC_I_MODE_SMARTFILTER_FISH_EYE		;	break;
			case eUCS_CAP_SMART_FILTER_I_MODE_SKETCH		:	nEnum = DSC_I_MODE_SMARTFILTER_SKETCH		;	break;
			case eUCS_CAP_SMART_FILTER_I_MODE_DEFOG			:	nEnum = DSC_I_MODE_SMARTFILTER_DEFOG		;	break;
			case eUCS_CAP_SMART_FILTER_I_MODE_HALF_TONE		:	nEnum = DSC_I_MODE_SMARTFILTER_HALF_TONE	;	break;
			// dh9.seo 2013-06-26
			default:											nEnum = DSC_MODE_NULL;							break;
			}
			break;
	}
	
	//-- Set Value
	m_nIndicator[nIndicator] = nEnum;
	//-- Redraw
	CPaintDC dc(this);
	DrawIndicator (nIndicator, &dc);
	
	Invalidate(FALSE);
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		CheckIModeScene
//! @date		2011-8-26
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CIndicatorDlg::CheckIModeScene(int nValue)
{
	BOOL bIMode = ((CNXRemoteStudioDlg*)GetParent())->IsIMode();
	int nEnum = -1;

	if(bIMode)
	{
		switch(nValue)
		{
		case eUCS_UI_MODE_SCENE_BEAUTY		:	nEnum = DSC_I_MODE_SCENE_BEAUTY		;	break;
		case eUCS_UI_MODE_SCENE_PORTRAIT	:	nEnum = DSC_I_MODE_SCENE_PORTRAIT	;	break;
		case eUCS_UI_MODE_SCENE_CHILDREN	:	nEnum = DSC_I_MODE_SCENE_CHILDREN	;	break;
		case eUCS_UI_MODE_SCENE_BACKLIGHT	:	nEnum = DSC_I_MODE_SCENE_BACKLIGHT	;	break;
		case eUCS_UI_MODE_SCENE_LANDSCAPE	:	nEnum = DSC_I_MODE_SCENE_LANDSCAPE	;	break;
		case eUCS_UI_MODE_SCENE_SUNSET		:	nEnum = DSC_I_MODE_SCENE_SUNSET		;	break;
		case eUCS_UI_MODE_SCENE_DAWN		:	nEnum = DSC_I_MODE_SCENE_DAWN		;	break;
		case eUCS_UI_MODE_SCENE_NIGHT		:	nEnum = DSC_I_MODE_SCENE_NIGHT		;	break;
		}	
	}
	else
	{
		switch(nValue)
		{
		case eUCS_UI_MODE_SCENE_BEAUTY		:	nEnum = DSC_MODE_SCENE_BEAUTY		;	break;
		case eUCS_UI_MODE_SCENE_PORTRAIT	:	nEnum = DSC_MODE_SCENE_PORTRAIT		;	break;
		case eUCS_UI_MODE_SCENE_CHILDREN	:	nEnum = DSC_MODE_SCENE_CHILDREN		;	break;
		case eUCS_UI_MODE_SCENE_BACKLIGHT	:	nEnum = DSC_MODE_SCENE_BACKLIGHT	;	break;
		case eUCS_UI_MODE_SCENE_LANDSCAPE	:	nEnum = DSC_MODE_SCENE_LANDSCAPE	;	break;
		case eUCS_UI_MODE_SCENE_SUNSET		:	nEnum = DSC_MODE_SCENE_SUNSET		;	break;
		case eUCS_UI_MODE_SCENE_DAWN		:	nEnum = DSC_MODE_SCENE_DAWN			;	break;
		case eUCS_UI_MODE_SCENE_NIGHT		:	nEnum = DSC_MODE_SCENE_NIGHT		;	break;
		}	
	}

	return nEnum;
}

//------------------------------------------------------------------------------ 
//! @brief		CheckIModeSmartFilter
//! @date		2011-8-26
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CIndicatorDlg::CheckIModeSmartFilter()
{
	int nEnum = -1;
	int nValue = ((CNXRemoteStudioDlg*)GetParent())->m_SdiMultiMgr.GetCurrentPropValueEnum(((CNXRemoteStudioDlg*)GetParent())->GetSelectedItem(), PTP_CODE_SAMSUNG_I_SMART_FILTER);

	switch(nValue)
	{
	case eUCS_CAP_SMART_FILTER_I_MODE_VIGNETTING	:	nEnum = DSC_I_MODE_SMARTFILTER_VIGNETTING	;	break;
	case eUCS_CAP_SMART_FILTER_I_MODE_MINIATURE		:	nEnum = DSC_I_MODE_SMARTFILTER_MINIATURE	;	break;
	case eUCS_CAP_SMART_FILTER_I_MODE_FISH_EYE		:	nEnum = DSC_I_MODE_SMARTFILTER_FISH_EYE		;	break;
	case eUCS_CAP_SMART_FILTER_I_MODE_SKETCH		:	nEnum = DSC_I_MODE_SMARTFILTER_SKETCH		;	break;
	case eUCS_CAP_SMART_FILTER_I_MODE_DEFOG			:	nEnum = DSC_I_MODE_SMARTFILTER_DEFOG		;	break;
	case eUCS_CAP_SMART_FILTER_I_MODE_HALF_TONE		:	nEnum = DSC_I_MODE_SMARTFILTER_HALF_TONE	;	break;
	default:											nEnum = -1;										break;
	}

	return nEnum;
}

//------------------------------------------------------------------------------ 
//! @brief		GetIndicator
//! @date		2010-06-08
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
int CIndicatorDlg::GetIndicator(int nPTPCode)
{
	switch(nPTPCode)
	{
		case	PTP_CODE_FUNCTIONALMODE		: return RS_INDICATOR_MODE		;
		case	PTP_CODE_BATTERYLEVEL		: return RS_INDICATOR_BATTERY	;	
		case	PTP_CODE_WHITEBALANCE		 : return RS_INDICATOR_WB			;
		case	PTP_CODE_SAMSUNG_ISOSTEP	 : return RS_INDICATOR_ISO			;
		case	PTP_CODE_FLASHMODE			 : return RS_INDICATOR_FLASH		;
		case	PTP_CODE_STILLCAPTUREMODE	 : return RS_INDICATOR_DRIVE		;
		case	PTP_CODE_FOCUSMETERINGMODE	 : return RS_INDICATOR_METERING		;
	}
	return RS_INDICATOR_NONE;
}
//------------------------------------------------------------------------------ 
//! @brief		DrawIndicator
//! @date		2010-06-08
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CIndicatorDlg::DrawIndicator (int nIndicator, CDC* pDC)
{
	CString strModel = ((CNXRemoteStudioDlg*)GetParent())->GetModel();
//	if(sdf.GetLength() > 0)
//		TRACE(sdf);
	int nX, nY, nW, nH;
	nX		= m_rect[nIndicator].left;	
	nY		= m_rect[nIndicator].top;
	nW		= m_rect[nIndicator].Width();
	nH		= m_rect[nIndicator].Height(); 
	switch(nIndicator)
	{
	case RS_INDICATOR_CAPTURE:
		{
			int nCnt = ((CNXRemoteStudioDlg*)GetParent())->m_SdiMultiMgr.GetPTPCount();
			COLORREF color;
			CString strCaptureCount;
			strCaptureCount.Format(_T("%d"), m_nCaptureCount);
			
			if(nCnt)
			{
				if(m_nCaptureCount < 0)
				{
					DrawStr(pDC, STR_SIZE_TEXT_BOLD, _T("No Card"),  m_rect[RS_INDICATOR_CAPTURE].left + 75,  m_rect[RS_INDICATOR_CAPTURE].top + 10, color = RGB(0,0,0));
				}
				else if(m_nCaptureCount == 8193)
				{
					DrawStr(pDC, STR_SIZE_TEXT_BOLD, _T(""),  m_rect[RS_INDICATOR_CAPTURE].left + 105,  m_rect[RS_INDICATOR_CAPTURE].top + 10, color = RGB(0,0,0));
				}
				else
				{
					DrawStr(pDC, STR_SIZE_TEXT_BOLD, strCaptureCount,  m_rect[RS_INDICATOR_CAPTURE].left + 105,  m_rect[RS_INDICATOR_CAPTURE].top + 10, color = RGB(0,0,0));
				}
			}
			else
				DrawStr(pDC, STR_SIZE_TEXT_BOLD, _T(""),  m_rect[RS_INDICATOR_CAPTURE].left + 105,  m_rect[RS_INDICATOR_CAPTURE].top + 10, color = RGB(0,0,0));
		}
		break;
	case RS_INDICATOR_MODE:
		switch(m_nIndicator[nIndicator])
		{
/*			case DSC_MODE_A:						DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_a.png")) ,nX, nY, nW, nH);		break;
			case DSC_MODE_M:						DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_m.png")) ,nX, nY, nW, nH);		break;
			case DSC_MODE_P:						DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_p.png")) ,nX, nY, nW, nH);		break;
			case DSC_MODE_S:						DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_s.png")) ,nX, nY, nW, nH);		break;
			case DSC_MODE_SMART:					DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_smart.png")) ,nX, nY, nW, nH);	break; */
			case DSC_MODE_A:						DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_a.png")) ,nX, nY, nW, nH);		break;
			case DSC_MODE_M:						DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_m.png")) ,nX, nY, nW, nH);		break;
			case DSC_MODE_P:						DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_p.png")) ,nX, nY, nW, nH);		break;
			case DSC_MODE_S:						DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_s.png")) ,nX, nY, nW, nH);		break;
			case DSC_MODE_SMART:					DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_auto.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_I						:	break;
			case DSC_MODE_CAPTURE_MOVIE			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_movie_m.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_MAGIC_MAGIC_FRAME		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_magicframe.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_MAGIC_SMART_FILTER	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_smartfilter.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_SOUND_PICTURE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_soundpicture_dim.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_BEAUTY			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_smart_beautyface_dim.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_NIGHT			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_smart_night_dim.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_LANDSCAPE		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_smart_landscape_dim.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_PORTRAIT		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_portrait.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_CHILDREN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_children.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_SPORTS			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_sports.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_CLOSE_UP		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_closeup.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_TEXT			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_text.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_SUNSET			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_sunset.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_DAWN			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_dawn.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_BACKLIGHT		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_backlight.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_FIREWORK		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_smart_fireworks_dim.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_SCENE_BEACH_SNOW		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_snow_beach.png")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_MOVIE					:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_movie_m.PNG")) ,nX, nY, nW, nH);	break;
			case DSC_MODE_BULB					:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_m.png")) ,nX, nY, nW, nH);		break;
			case DSC_MODE_PANORAMA				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_panorama_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_MODE_SCENE_3D				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_scene_3d.png")) ,nX, nY, nW, nH);		break;
			//-- 2011-8-26 Lee JungTaek
			case DSC_I_MODE_SCENE_BEAUTY			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_smart_beautyface_dim.png")) ,nX, nY, nW, nH);		break;	
			case DSC_I_MODE_SCENE_PORTRAIT			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_imode_scene_portrait.png")) ,nX, nY, nW, nH);	break;
			case DSC_I_MODE_SCENE_CHILDREN			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_imode_scene_children.png")) ,nX, nY, nW, nH);	break;
			case DSC_I_MODE_SCENE_BACKLIGHT			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_imode_scene_backlight.png")) ,nX, nY, nW, nH);	break;
			case DSC_I_MODE_SCENE_LANDSCAPE			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_imode_scene_landscape.png")) ,nX, nY, nW, nH);	break;
			case DSC_I_MODE_SCENE_SUNSET			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_imode_scene_sunset.png")) ,nX, nY, nW, nH);		break;	
			case DSC_I_MODE_SCENE_DAWN				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_imode_scene_dawn.png")) ,nX, nY, nW, nH);		break;
			case DSC_I_MODE_SCENE_NIGHT				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_imode_scene_night.png")) ,nX, nY, nW, nH);		break;
			case DSC_I_MODE_SMARTFILTER_VIGNETTING	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_imode_scene_vignetting.png")) ,nX, nY, nW, nH);	break;
			case DSC_I_MODE_SMARTFILTER_MINIATURE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_imode_scene_miniature.png")) ,nX, nY, nW, nH);	break;
			case DSC_I_MODE_SMARTFILTER_FISH_EYE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_imode_scene_fisheye.png")) ,nX, nY, nW, nH);		break;	
			case DSC_I_MODE_SMARTFILTER_SKETCH		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_imode_scene_sketch.png")) ,nX, nY, nW, nH);	break;
			case DSC_I_MODE_SMARTFILTER_DEFOG		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_imode_scene_defog.png")) ,nX, nY, nW, nH);		break;
			case DSC_I_MODE_SMARTFILTER_HALF_TONE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\01_mode_icon_imode_scene_halftone.png")) ,nX, nY, nW, nH);	break;
			// dh9.seo 2013-06-24
			case DSC_MODE_SCENE_BEST				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\10_icon_smart_best_face.png")) ,nX, nY, nW, nH);		;	break;
			case DSC_MODE_SCENE_MACRO				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\10_icon_smart_macro.png")) ,nX, nY, nW, nH);		;	break;
			case DSC_MODE_SCENE_ACTION				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\10_icon_smart_actionfreeze.png")) ,nX, nY, nW, nH);		;	break;
			case DSC_MODE_SCENE_RICH				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\10_icon_smart_rich_tone.png")) ,nX, nY, nW, nH);		;	break;
			case DSC_MODE_SCENE_WATERFALL			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\10_icon_smart_waterfalltrace.png")) ,nX, nY, nW, nH);		;	break;
			case DSC_MODE_SCENE_SILHOUETTE			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_smart_silhouette_dim.png")) ,nX, nY, nW, nH);		;	break;
			case DSC_MODE_SCENE_LIGHT				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\10_icon_smart_lighttrace.png")) ,nX, nY, nW, nH);		;	break;	
			case DSC_MODE_SCENE_CREATIVE			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\10_icon_smart_creativeshot.png")) ,nX, nY, nW, nH);		;	break;
			case DSC_MODE_SCENE_PANORAMA			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\10_icon_smart_panorama.png")) ,nX, nY, nW, nH);		break;
			//NX1 dh0.seo 2014-08-19
			case DSC_MODE_SCENE_ACTION_FREEZE		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_smart_actionfreeze_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_MODE_SCENE_LIGHT_TRACE			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_smart_lighttrace.png")) ,nX, nY, nW, nH);		break;
			case DSC_MODE_SCENE_MULTI_EXPOSURE		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_smart_multiexposure.png")) ,nX, nY, nW, nH);		break;
			case DSC_MODE_SCENE_AUTO_SHUTTER		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Mode\\05_mode_icon_smart_auto_shutter.png")) ,nX, nY, nW, nH);		break;
			default:				break;
		}
		break;
	case RS_INDICATOR_DSC_NAME:
		{
			switch(m_nIndicator[nIndicator])
			{
			case DSC_NAME_NULL:	break;
			default: GetDSCName(pDC, RS_INDICATOR_DSC_NAME); break;
			}			
		}
		break;
	case RS_INDICATOR_BATTERY:
		if(!strModel.Compare(_T("NX1")))
		{
			if(m_nValue <= 100 && m_nValue > 75)
			{
				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Battery\\NX1_battery_normal_bg1.png")) ,nX, nY, nW, nH);
			}

			else if(m_nValue <= 75 && m_nValue > 50)
			{
				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Battery\\NX1_battery_normal_bg2.png")) ,nX, nY, nW, nH);
			}

			else if(m_nValue <= 50 && m_nValue > 25)
			{
				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Battery\\NX1_battery_normal_bg3.png")) ,nX, nY, nW, nH);
			}

			else if(m_nValue <= 25 && m_nValue > 10)
			{
				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Battery\\NX1_battery_normal_bg4.png")) ,nX, nY, nW, nH);
			}
			else
			{
				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Battery\\NX1_battery_normal_bg5.png")) ,nX, nY, nW, nH);
			}
			break;
		}
		else
		{
			switch(m_nIndicator[nIndicator])
			{
			case DSC_BATTERY_FULL	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Battery\\01_lcd_icon_battery_03.png")) ,nX, nY, nW, nH);		break;
			case DSC_BATTERY_2		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Battery\\01_lcd_icon_battery_02.png")) ,nX, nY, nW, nH);		break;
			case DSC_BATTERY_1		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Battery\\01_lcd_icon_battery_01.png")) ,nX, nY, nW, nH);		break;
			case DSC_BATTERY_0		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Battery\\01_lcd_icon_battery_00.png")) ,nX, nY, nW, nH);		break;		
			default:				break;
			}
			break;
		}
	case RS_INDICATOR_METERING:
		if(!strModel.Compare(_T("NX1")))
		{
			switch(m_nIndicator[nIndicator])
			{
			case DSC_METERING_MULTI		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Metering\\01_icon_indi_metering_multi_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_METERING_CENTER	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Metering\\01_icon_indi_metering_center_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_METERING_SPOT		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Metering\\01_icon_indi_metering_spot_dim.png")) ,nX, nY, nW, nH);		break;
			default:					break;
			}	
			break;
		}
		else
		{
			switch(m_nIndicator[nIndicator])
			{
			case DSC_METERING_MULTI		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Metering\\01_lcd_icon_metering_multi.png")) ,nX, nY, nW, nH);		break;
			case DSC_METERING_CENTER	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Metering\\01_lcd_icon_metering_center.png")) ,nX, nY, nW, nH);		break;
			case DSC_METERING_SPOT		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Metering\\01_lcd_icon_metering_spot.png")) ,nX, nY, nW, nH);		break;
			default:					break;
			}	
			break;
		}
	case RS_INDICATOR_WB:
		if(!strModel.Compare(_T("NX1")))
		{
			switch(m_nIndicator[nIndicator])
			{
			case DSC_WB_AUTO				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_icon_indi_wbl_auto_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_WB_CLOUDY				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_icon_indi_wbl_cloudy_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_WB_CUSTOM				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_icon_indi_wbl_coustom_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_WB_DAYLIGHT			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_icon_indi_wbl_daylight_dim.png")) ,nX, nY, nW, nH);		break;		
			case DSC_WB_FLASH				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_icon_indi_wbl_flash_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_WB_FLUORESCENT_D		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_icon_indi_wbl_fluorescent_d_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_WB_FLUORESCENT_N		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_icon_indi_wbl_fluorescent_n_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_WB_FLUORESCENT_W		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_icon_indi_wbl_fluorescent_w_dim.png")) ,nX, nY, nW, nH);		break;		
			case DSC_WB_K					:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_icon_indi_wbl_colortemp_dim.png")) ,nX, nY, nW, nH);				break;
			case DSC_WB_TUNGSTEN			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_icon_indi_wbl_tungsten_dim.png")) ,nX, nY, nW, nH);		break;		
			case DSC_WB_AUTO_TUNGSTEN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_icon_indi_wbl_auto_tungsten_dim.png")) ,nX, nY, nW, nH);		break;		
			default:							break;
			}	
		}
		else
		{
			switch(m_nIndicator[nIndicator])
			{
			case DSC_WB_AUTO				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_lcd_icon_wb_auto.png")) ,nX, nY, nW, nH);		break;
			case DSC_WB_CLOUDY				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_lcd_icon_wb_cloudy.png")) ,nX, nY, nW, nH);		break;
			case DSC_WB_CUSTOM				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_lcd_icon_wb_custom.png")) ,nX, nY, nW, nH);		break;
			case DSC_WB_DAYLIGHT			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_lcd_icon_wb_daylight.png")) ,nX, nY, nW, nH);		break;		
			case DSC_WB_FLASH				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_lcd_icon_wb_flash.png")) ,nX, nY, nW, nH);		break;
			case DSC_WB_FLUORESCENT_D		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_lcd_icon_wb_fluorescent_d.png")) ,nX, nY, nW, nH);		break;
			case DSC_WB_FLUORESCENT_N		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_lcd_icon_wb_fluorescent_n.png")) ,nX, nY, nW, nH);		break;
			case DSC_WB_FLUORESCENT_W		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_lcd_icon_wb_fluorescent_w.png")) ,nX, nY, nW, nH);		break;		
			case DSC_WB_K					:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_lcd_icon_wb_k.png")) ,nX, nY, nW, nH);				break;
			case DSC_WB_TUNGSTEN			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\01_lcd_icon_wb_tungsten.png")) ,nX, nY, nW, nH);		break;		
			case DSC_WB_AUTO_TUNGSTEN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\White Balance\\02_icon_wb_auto_tungsten_dim.png")) ,nX, nY, nW, nH);		break;		
			default:							break;
			}	
		}
		break;
	case RS_INDICATOR_ISO:	
		if(!strModel.Compare(_T("NX1")))
		{
			if(m_bAutoISO = FALSE)
			{
				switch(m_nIndicator[nIndicator])
				{
				case DSC_ISO_100		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_100.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_1000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_1000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_10000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_10000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_125		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_125.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_1250		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_1250.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_12800		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_12800.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_160		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_160.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_1600		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_1600.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_16000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_16000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_200		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_200.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_2000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_2000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_20000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_20000.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_250		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_250.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_2500		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_2500.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_25600		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_25600.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_320		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_320.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_3200		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_3200.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_32000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_32000.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_400		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_400.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_4000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_4000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_40000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_40000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_500		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_500.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_5000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_5000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_640		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_640.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_6400		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_6400.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_800		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_800.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_8000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_8000.png")) ,nX, nY, nW, nH);		break;
//				case DSC_ISO_AUTO		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_auto.png")) ,nX, nY, nW, nH);		break;
					//NX1 dh0.seo 2014-08-13
				case DSC_ISO_51200		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_51200.png")) ,nX, nY, nW, nH);		break;		
				default:				break;
				}
			}
			else
			{	
				switch(m_nIndicator[nIndicator])
				{
				case DSC_ISO_100		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_100.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_1000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_1000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_10000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_10000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_125		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_125.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_1250		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_1250.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_12800		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_12800.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_160		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_160.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_1600		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_1600.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_16000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_16000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_200		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_200.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_2000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_2000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_20000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_20000.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_250		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_250.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_2500		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_2500.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_25600		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_25600.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_320		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_320.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_3200		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_3200.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_32000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_32000.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_400		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_400.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_4000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_4000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_40000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_40000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_500		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_500.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_5000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_5000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_640		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_640.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_6400		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_6400.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_800		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_800.png")) ,nX, nY, nW, nH);		break;		
				case DSC_ISO_8000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_8000.png")) ,nX, nY, nW, nH);		break;
				case DSC_ISO_51200		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\icon_iso_51200.png")) ,nX, nY, nW, nH);		break;		
				default:				break;
				}
			}
			break;
		}
		else
		{
			switch(m_nIndicator[nIndicator])
			{
			case DSC_ISO_100		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_100.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_1000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_1000.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_10000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_10000.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_125		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_125.png")) ,nX, nY, nW, nH);		break;		
			case DSC_ISO_1250		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_1250.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_12800		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_12800.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_160		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_160.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_1600		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_1600.png")) ,nX, nY, nW, nH);		break;		
			case DSC_ISO_16000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_16000.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_200		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_200.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_2000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_2000.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_20000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_20000.png")) ,nX, nY, nW, nH);		break;		
			case DSC_ISO_250		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_250.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_2500		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_2500.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_25600		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_25600.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_320		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_320.png")) ,nX, nY, nW, nH);		break;		
			case DSC_ISO_3200		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_3200.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_400		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_400.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_4000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_4000.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_500		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_500.png")) ,nX, nY, nW, nH);		break;		
			case DSC_ISO_5000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_5000.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_640		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_640.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_6400		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_6400.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_800		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_800.png")) ,nX, nY, nW, nH);		break;		
			case DSC_ISO_8000		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_8000.png")) ,nX, nY, nW, nH);		break;
			case DSC_ISO_AUTO		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\ISO\\01_lcd_icon_iso_auto.png")) ,nX, nY, nW, nH);		break;
			default:				break;
			}	
			break;
		}
	case RS_INDICATOR_FLASH:
		if(!strModel.Compare(_T("NX1")))
		{
			switch(m_nIndicator[nIndicator])
			{
			case DSC_FLASH_RED				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\NX1_icon_indi_flash_auto_red_dim.png")) ,nX, nY, nW, nH);			break;
			case DSC_FLASH_1ST_CURTAIN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\NX1_icon_indi_flash_1st_curtain_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_FLASH_2ND_CURTAIN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\NX1_icon_indi_flash_2nd_curtain_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_FLASH_AUTO				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\NX1_icon_indi_flash_auto_dim.png")) ,nX, nY, nW, nH);		break;		
			case DSC_FLASH_FILLIN			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\NX1_icon_indi_flash_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_FLASH_OFF				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\NX1_icon_indi_flash_off_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_FLASH_SMART			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\NX1_icon_indi_flash_smart_dim.png")) ,nX, nY, nW, nH);		break;	
			case DSC_FLASH_FILLIN_RED		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\NX1_icon_indi_flash_fillin_red_dim.png")) ,nX, nY, nW, nH);		break;	
			default:
				break;
			}
		}
		else
		{
			switch(m_nIndicator[nIndicator])
			{
			case DSC_FLASH_RED				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\01_lcd_icon_flash_auto_red.png")) ,nX, nY, nW, nH);			break;
			case DSC_FLASH_1ST_CURTAIN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\01_lcd_icon_flash_1st_curtain.png")) ,nX, nY, nW, nH);		break;
			case DSC_FLASH_2ND_CURTAIN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\01_lcd_icon_flash_2nd_curtain.png")) ,nX, nY, nW, nH);		break;
			case DSC_FLASH_AUTO				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\01_lcd_icon_flash_auto.png")) ,nX, nY, nW, nH);		break;		
			case DSC_FLASH_FILLIN			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\01_lcd_icon_flash_fillin.png")) ,nX, nY, nW, nH);		break;
			case DSC_FLASH_OFF				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\01_lcd_icon_flash_off.png")) ,nX, nY, nW, nH);		break;
			case DSC_FLASH_SMART			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\01_lcd_icon_flash_smart.png")) ,nX, nY, nW, nH);		break;	
			case DSC_FLASH_FILLIN_RED		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Flash\\01_lcd_icon_flash_fillin_red.png")) ,nX, nY, nW, nH);		break;	
			default:
				break;
			}
		}	
		break;
	case RS_INDICATOR_DRIVE:
		if(strModel.CompareNoCase(_T("NX1")) == 0)
		{
			switch(m_nIndicator[nIndicator])
			{	
			case DSC_DRIVE_SINGLE_NX1		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_single_dim.png")) ,nX, nY, nW, nH);		break;				
			case DSC_DRIVE_CONTINUOUS_4FRAME_NX1:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_continuous_n_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_CONTINUOUS_6FRAME_NX1:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_continuous_n_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_CONTINUOUS_8FRAME_NX1:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_continuous_8_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_CONTINUOUS_10FRAME_NX1:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_continuous_10_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_CONTINUOUS_12FRAME_NX1:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_continuous_12_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_CONTINUOUS_15FRAME_NX1:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_continuous_15_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_AEB_NX1:					DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_aeb_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_WBB_NX1:					DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_wbb_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_PWB_NX1:					DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_pwb_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_DEPTH_NX1			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_deb_dim.png")) ,nX, nY, nW, nH);		break;
			default:
				{
					if(m_nIndicator[nIndicator] >= DSC_DRIVE_TIMER_2 && m_nIndicator[nIndicator] <= DSC_DRIVE_MAX)
					{
						int sdf = m_nIndicator[nIndicator];
						CString strIconPath = _T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_time_");
						CString strIconNO;
						strIconNO.Format(_T("%d.png"), m_nIndicator[nIndicator]-DSC_DRIVE_TIMER_2 + 2);
						//TRCAE(strIconNO);
						strIconPath.Append(strIconNO);
						DrawGraphicsImage(pDC, RSGetRoot(strIconPath) ,nX, nY, nW, nH);
					}
				}
				break;
			}	
			break;
		}
		else
		{
			switch(m_nIndicator[nIndicator])
			{
				//			case DSC_DRIVE_OFF				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_lcd_icon_drive_off.png")) ,nX, nY, nW, nH);		break;		
			case DSC_DRIVE_SINGLE			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_lcd_icon_drive_single.png")) ,nX, nY, nW, nH);		break;				
			case DSC_DRIVE_CONTINUOUS_3FRAME:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_lcd_icon_drive_continuous_3fps.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_CONTINUOUS_4FRAME:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_continuous_04.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_CONTINUOUS_5FRAME:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_lcd_icon_drive_continuous_5fps.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_CONTINUOUS_6FRAME:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_continuous_06.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_CONTINUOUS_8FRAME:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_continuous_08.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_CONTINUOUS_10FRAME:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_continuous_10.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_CONTINUOUS_12FRAME:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_continuous_12.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_CONTINUOUS_15FRAME:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_continuous_15.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_CONTINUOUS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_lcd_icon_drive_continuous_7fps.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_BURST			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_lcd_icon_drive_burst.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_TIMER			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_lcd_icon_drive_timer.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_AEB				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_lcd_icon_drive_aeb.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_WBB				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_lcd_icon_drive_wbb.png")) ,nX, nY, nW, nH);		break;		
			case DSC_DRIVE_PWB				:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_lcd_icon_drive_pwb.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_DEPTH			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_drive_deb_dim.png")) ,nX, nY, nW, nH);		break;
			case DSC_DRIVE_TIMER_2			:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\Drive\\01_icon_indi_time_02.png")) ,nX, nY, nW, nH);		break;
			default:
				break;
			}	
			break;
		}
	case RS_INDICATOR_FNUMBER:
		switch(m_nIndicator[nIndicator])
		{
			case DSC_FNUMBER_NULL: break;
			default: GetFNumberValue(pDC, RS_INDICATOR_FNUMBER, m_nIndicator[nIndicator]); break;
		}			
		break;
	case RS_INDICATOR_SHUTTERSPEED:
		switch(m_nIndicator[nIndicator])
		{
		case DSC_SHUTTERSPEED_NULL: break;
		default: GetShutterSpeedValue(pDC, RS_INDICATOR_SHUTTERSPEED, m_nIndicator[nIndicator]); break;
		}
		break;
	case RS_INDICATOR_EV:
		nX = 52; nY = 95; nW = 10; nH = 10;

		int nStep = 14;
		int nInStep = 4;
		
		//NX1 dh0.seo 2014-08-22
		if(!strModel.Compare(_T("NX1")))
		{
			nStep = 9;
			nInStep = 4;
		}

		switch(m_nIndicator[nIndicator])
		{
			case DSC_EV_MINUS_5_0			:	nX-= nStep * 5					;			break;
			case DSC_EV_MINUS_4_6			:	nX-= nStep * 4 + nInStep * 2	;			break;
			case DSC_EV_MINUS_4_3			:	nX-= nStep * 4 + nInStep		;			break;
			case DSC_EV_MINUS_4_0			:	nX-= nStep * 4					;			break;
			case DSC_EV_MINUS_3_6			:	nX-= nStep * 3+ nInStep * 2		;			break;
			case DSC_EV_MINUS_3_3			:	nX-= nStep * 3 + nInStep		;			break;
			case DSC_EV_MINUS_3_0			:	nX-= nStep * 3					;			break;
			case DSC_EV_MINUS_2_6			:	nX-= nStep * 2 + nInStep * 2	;			break;
			case DSC_EV_MINUS_2_3			:	nX-= nStep * 2 + nInStep		;			break;
			case DSC_EV_MINUS_2_0			:	nX-= nStep * 2					;			break;
			case DSC_EV_MINUS_1_6			:	nX-= nStep + nInStep * 2		;			break;
			case DSC_EV_MINUS_1_3			:	nX-= nStep + nInStep			;			break;
			case DSC_EV_MINUS_1_0			:	nX-= nStep						;			break;
			case DSC_EV_MINUS_0_6			:	nX-= nInStep * 2				;			break;
			case DSC_EV_MINUS_0_3			:	nX-= nInStep					;			break;
			case DSC_EV_MINUS_0				:												break;
			case DSC_EV_PLUS_0_3			:	nX+= nInStep					;			break;
			case DSC_EV_PLUS_0_6			:	nX+= nInStep * 2				;			break;
			case DSC_EV_PLUS_1_0			:	nX+= nStep						;			break;
			case DSC_EV_PLUS_1_3			:	nX+= nStep + nInStep			;			break;
			case DSC_EV_PLUS_1_6			:	nX+= nStep + nInStep * 2		;			break;
			case DSC_EV_PLUS_2_0			:	nX+= nStep * 2					;			break;
			case DSC_EV_PLUS_2_3			:	nX+= nStep * 2 + nInStep		;			break;
			case DSC_EV_PLUS_2_6			:	nX+= nStep * 2 + nInStep * 2	;			break;
			case DSC_EV_PLUS_3_0			:	nX+= nStep * 3					;			break;
			case DSC_EV_PLUS_3_3			:	nX+= nStep * 3 + nInStep		;			break;
			case DSC_EV_PLUS_3_6			:	nX+= nStep * 3 + nInStep * 2	;			break;
			case DSC_EV_PLUS_4_0			:	nX+= nStep * 4					;			break;
			case DSC_EV_PLUS_4_3			:	nX+= nStep * 4 + nInStep		;			break;
			case DSC_EV_PLUS_4_6			:	nX+= nStep * 4 + nInStep * 2	;			break;
			case DSC_EV_PLUS_5_0			:	nX+= nStep * 5					;			break;
			default:
				break;
		}

		if(m_nIndicator[nIndicator] != DSC_EV_NULL) 
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_evgage.png")) ,nX, nY, nW, nH);	
		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		GetFNumberValue
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Change F Number Value to CString
//------------------------------------------------------------------------------ 
void CIndicatorDlg::GetFNumberValue(CDC* pDC, int nIndicator, int nValue)
{
	CString strFNumber = _T("");

	if(nValue % 100)
		strFNumber.Format(_T("F%.1f"), (float)nValue / 100);
	else
	{
		if(nValue >= 1000)
			strFNumber.Format(_T("F%.0f"), (float)nValue / 100);
		else
			strFNumber.Format(_T("F%.1f"), (float)nValue / 100);
	}		
		
	DrawStringImage(pDC, RS_INDICATOR_FNUMBER, strFNumber);
}

//------------------------------------------------------------------------------ 
//! @brief		GetShutterSpeedValue
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Change Shutter Speed to CString
//------------------------------------------------------------------------------ 
void CIndicatorDlg::GetShutterSpeedValue(CDC* pDC, int nIndicator, int nValue)
{
	int nChild = HIWORD(nValue) ;
	int nParent = LOWORD(nValue) ;

 	CString strShutterSpeed = _T("");
	int nChar = 0;

	if(nValue == 0x00000001)
	{
		int nX		= m_rect[RS_INDICATOR_SHUTTERSPEED].left;
		int nY		= m_rect[RS_INDICATOR_SHUTTERSPEED].top;

		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_bulb.png")) ,nX, nY, 24, 13);
		return;
	}

	switch(nParent)
	{
	case 10:
		{
			if(nChild == 1)
			{
				strShutterSpeed.Format(_T("%d/%d"), nChild , nParent);
				break;
			}
			if(nChild % nParent)
				strShutterSpeed.Format(_T("%.1f"), (float)nChild / nParent);
			else
				strShutterSpeed.Format(_T("%.0f"), (float)nChild / nParent);
		}
		break;
	case 1:
		strShutterSpeed.Format(_T("%d"), nChild);					break;
	default:
		strShutterSpeed.Format(_T("%d/%d"), nChild , nParent);		break;
	}

	if(strShutterSpeed.Find(_T('/')) < 0)
		strShutterSpeed.Append(_T("\""));

	DrawStringImage(pDC, RS_INDICATOR_SHUTTERSPEED, strShutterSpeed);
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-6-29
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CIndicatorDlg::GetDSCName(CDC* pDC, int nIndicator)
{
	CString strName = m_strDSCName;
	DrawStringImage(pDC, RS_INDICATOR_DSC_NAME, strName);
}


//------------------------------------------------------------------------------ 
//! @brief		DrawStringImage
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Draw Image
//------------------------------------------------------------------------------ 
void CIndicatorDlg::DrawStringImage(CDC* pDC, int nIndicator, CString strValue)		
{
	for(int i = 0; i < strValue.GetLength(); i++)
	{
		int nX = 0, nY = 0, nW = 0, nH = 0;
		int nChar = strValue[i];

		CString strImagePath = _T("");
		strImagePath = ChangeValueToImage(nIndicator, nChar);

		switch(nIndicator)
		{
		case RS_INDICATOR_DSC_NAME:
			{
				nX		= m_rect[nIndicator].left + i * string_normal_icon_width + 1;	
				nY		= m_rect[nIndicator].top + 10;
				nW	= string_normal_icon_width;
				nH		= string_normal_icon_height;
			}
			break;
		case RS_INDICATOR_FNUMBER:
			{
				switch(nChar)
				{
				case  RS_INDICATOR_ASCII_DOT:
					{
						nX		= m_rect[nIndicator].left + i * string_normal_icon_width + 1;	
						nY		= m_rect[nIndicator].top;
						nW		= string_dot_icon_width;
						nH		= string_normal_icon_height;

						m_bDotString = TRUE;
					}					
					break;
				default:
					{
						if(m_bDotString)
						{
							nX		= m_rect[nIndicator].left + (i - 1) * string_normal_icon_width + string_dot_icon_width + 1;
							m_bDotString = FALSE;
						}
						else
							nX		= m_rect[nIndicator].left + i * string_normal_icon_width + 1;	

						nY		= m_rect[nIndicator].top;
						nW		= string_normal_icon_width;
						nH		= string_normal_icon_height;
					}
					break;
				}
			}
			break;
		case RS_INDICATOR_SHUTTERSPEED:
			switch(nChar)
			{
			case RS_INDICATOR_ASCII_DOT:
				{
					nX		= m_rect[nIndicator].left + i * string_normal_icon_width + 1;	
					nY		= m_rect[nIndicator].top;
					nW		= string_dot_icon_width;
					nH		= string_normal_icon_height;

					m_bDotString = TRUE;
				}					
				break;
			default:
				{
					if(m_bDotString)
					{
						nX		= m_rect[nIndicator].left + (i - 1) * string_normal_icon_width + string_dot_icon_width + 1;
						m_bDotString = FALSE;
					}
					else
						nX		= m_rect[nIndicator].left + i * string_normal_icon_width + 1;	

					nY		= m_rect[nIndicator].top;
					nW		= string_normal_icon_width;
					nH		= string_normal_icon_height;
				}
				break;
			}
		}

		//-- 2011-07-26 hongsu.jung
		//-- Check Not Connected
		if(nIndicator == RS_INDICATOR_DSC_NAME && strValue == STR_NOT_CONNECTED)
			nX -= 28;
		DrawGraphicsImage(pDC, strImagePath ,nX , nY, nW, nH);
	}	
}	

//------------------------------------------------------------------------------ 
//! @brief		ChangeValueToImage
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Change Value To LCD Image
//------------------------------------------------------------------------------ 
CString CIndicatorDlg::ChangeValueToImage(int nIndicator, int nChar)
{
	CString strImagePath = _T("");

	switch(nChar)
	{
	case RS_INDICATOR_ASCII_QUOTE	:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_bulb_min.png"));	break;
	case RS_INDICATOR_ASCII_DOT		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_dot.png"));	break;
	case RS_INDICATOR_ASCII_SLASH	:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_slash.png"));	break;
	case RS_INDICATOR_ASCII_0		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_00.png"));	break;
	case RS_INDICATOR_ASCII_1		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_01.png"));	break;
	case RS_INDICATOR_ASCII_2		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_02.png"));	break;
	case RS_INDICATOR_ASCII_3		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_03.png"));	break;
	case RS_INDICATOR_ASCII_4		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_04.png"));	break;
	case RS_INDICATOR_ASCII_5		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_05.png"));	break;
	case RS_INDICATOR_ASCII_6		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_06.png"));	break;
	case RS_INDICATOR_ASCII_7		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_07.png"));	break;
	case RS_INDICATOR_ASCII_8		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_08.png"));	break;
	case RS_INDICATOR_ASCII_9		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_09.png"));	break;
	case RS_INDICATOR_ASCII_A		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_a.png"));		break;
	case RS_INDICATOR_ASCII_B		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_b.png"));		break;
	case RS_INDICATOR_ASCII_C		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_c.png"));		break;
	case RS_INDICATOR_ASCII_D		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_d.png"));		break;
	case RS_INDICATOR_ASCII_E		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_e.png"));		break;
	case RS_INDICATOR_ASCII_F		:	
		switch(nIndicator)
		{
		case RS_INDICATOR_FNUMBER:		strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_num_f.png"));	break;
		default:						strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_f.png"));		break;
		}
		break;
	case RS_INDICATOR_ASCII_G		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_g.png"));		break;
	case RS_INDICATOR_ASCII_H		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_h.png"));		break;
	case RS_INDICATOR_ASCII_I		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_i.png"));		break;
	case RS_INDICATOR_ASCII_J		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_j.png"));		break;
	case RS_INDICATOR_ASCII_K		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_k.png"));		break;
	case RS_INDICATOR_ASCII_L		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_l.png"));		break;
	case RS_INDICATOR_ASCII_M		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_m.png"));		break;
	case RS_INDICATOR_ASCII_N		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_n.png"));		break;
	case RS_INDICATOR_ASCII_O		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_o.png"));		break;
	case RS_INDICATOR_ASCII_P		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_p.png"));		break;
	case RS_INDICATOR_ASCII_Q		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_q.png"));		break;
	case RS_INDICATOR_ASCII_R		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_r.png"));		break;
	case RS_INDICATOR_ASCII_S		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_s.png"));		break;
	case RS_INDICATOR_ASCII_T		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_t.png"));		break;
	case RS_INDICATOR_ASCII_U		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_u.png"));		break;
	case RS_INDICATOR_ASCII_V		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_v.png"));		break;
	case RS_INDICATOR_ASCII_W		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_w.png"));		break;
	case RS_INDICATOR_ASCII_X		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_x.png"));		break;
	case RS_INDICATOR_ASCII_Y		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_y.png"));		break;
	case RS_INDICATOR_ASCII_Z		:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_z.png"));		break;
	case RS_INDICATOR_ASCII_UNDERBAR:	strImagePath = RSGetRoot(_T("img\\01.Light Studio\\lcd\\01_lcd_id_underbar.png"));		break;
	}

	return strImagePath;
}

void CIndicatorDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	//-- 2011-8-18 Lee JungTaek
	((CNXRemoteStudioDlg*)GetParent())->HidePopup();

	CDialog::OnLButtonDown(nFlags, point);	
}
