/////////////////////////////////////////////////////////////////////////////
//
//  PictureViewerDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "BkDialogST.h"
#include "NXRemoteStudioFunc.h"

// CPictureViewerDlg dialog
class CPictureViewerDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CPictureViewerDlg)	
public:
	CPictureViewerDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPictureViewerDlg();

	// Dialog Data
	enum { IDD = IDD_DLG_PICTUREVIEWER};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();	
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg LRESULT OnNcHitTest(CPoint point);
	DECLARE_MESSAGE_MAP()

	afx_msg void OnBnClickedBtnMaximize();
	afx_msg void OnBnClickedBtnCancel();
public:
	void SetImagePath(CString str)	{ m_strImagePath = str; }
	BOOL SetImageName(CString str);
	void GetFullSize();

private:
	CString m_strImagePath;
	CString m_strImageName;

	CBitmapButton m_btnMax		;
	CBitmapButton m_btnCancel	;

	//-- 2011-05-18 hongsu.jung
	void UpdateFrames();
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();
	void DrawBackGround(CDC* pDC, CRect rect);
	void DrawSelectedImage(CDC* pDC, CRect rect);
	void DrawDialogs();	
	void Maximize();

	int m_nPictureCount;
	int m_nAutoViewSize;
	int m_nSizeCount;
	int m_nMaxCount;
	BOOL m_bDrawPicture;
public:	
	LRESULT OnEndResizeEvent(WPARAM w, LPARAM l);
	CString PictureTitle(CString strTilte){m_strImageName = strTilte; return m_strImagePath;}
};
