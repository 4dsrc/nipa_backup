/////////////////////////////////////////////////////////////////////////////
//
//  ZoomDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-22
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "BkDialogST.h"
#include "ZoomviewFrmDlg.h"
#include "NXRemoteStudioFunc.h"

// CZoomDlg dialog
class CZoomDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CZoomDlg)	
public:
	CZoomDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CZoomDlg();

// Dialog Data
	enum { IDD = IDD_DLG_PICTUREVIEWER};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();	
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	DECLARE_MESSAGE_MAP()

	afx_msg void OnBnClickedBtnMaximize();
	afx_msg void OnBnClickedBtnCancel();


private:
	CBitmapButton m_btnMax		;
	CBitmapButton m_btnCancel	;	
	//-- 2011-05-18 hongsu.jung
	void UpdateFrames();
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();
	void DrawBackGround();
	void DrawDialogs();
	void DrawZoom();
	CZoomviewFrmDlg* m_pZoomviewFrm;

public:	
	CZoomview*			GetZoomView() { return m_pZoomviewFrm->GetZoomview(); }
	CZoomviewFrmDlg*	GetZoomViewFrm() { return m_pZoomviewFrm; }
	LRESULT OnEndResizeEvent(WPARAM w, LPARAM l);
};