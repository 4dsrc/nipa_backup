/////////////////////////////////////////////////////////////////////////////
//
//  CPopupMsgDlg.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-07-01
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudio.h"
#include "PopupMsgDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const int max_text_name_len	= 12;
static const int max_bank_name_len	= 20;

static const int timer_start_1sec		= WM_USER + 0x0001;
static const int timer_start_3sec		= WM_USER + 0x0002;
static const int timer_start_5sec		= WM_USER + 0x0003;

// CPopupMsgDlg dialog
IMPLEMENT_DYNAMIC(CPopupMsgDlg,  CBkDialogST)

	CPopupMsgDlg::CPopupMsgDlg(int nType, CString strMsg, int nDelay, CWnd* pParent /*=NULL*/)
	: CBkDialogST(CPopupMsgDlg::IDD, pParent)
{
	m_nMouseIn = BTN_POPUP_CNT;	
	for(int i=0; i<BTN_POPUP_CNT - 1; i++)
	{
		m_nStatus[i] = PT_POPUP_NOR;
		m_rect[i] = CRect(0,0,0,0);
	}
	m_rectMsg = CRect(0,0,0,0);

	m_nType = nType;
	m_strMsg = strMsg;
	m_nDelay = nDelay;
	m_pMsg = NULL;
}

CPopupMsgDlg::~CPopupMsgDlg()
{
	TRACE(_T("Destroy:CPopupMsgDlg Start\n"));
	TRACE(_T("Destroy:CPopupMsgDlg End\n"));
}

void CPopupMsgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPopupMsgDlg, CBkDialogST)
	ON_WM_PAINT()	
	ON_WM_TIMER()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()

BOOL CPopupMsgDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();		

	switch(m_nType)
	{
	case POPUP_TYPE_ONLY_MESSAGE	:		break;
	case POPUP_TYPE_WITH_2BTN		:	SetItemPosition();	break;
	}
	
	switch(m_nDelay)
	{
	case POPUP_DELAY_NONE	:	break;
	case POPUP_DELAY_1SEC	:	SetTimer(timer_start_1sec, 1000, NULL);		break;
	case POPUP_DELAY_3SEC	:	SetTimer(timer_start_3sec, 3000, NULL);		break;
	case POPUP_DELAY_5SEC	:	SetTimer(timer_start_5sec, 5000, NULL);		break;
	default:					break;
	}	

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CPopupMsgDlg::SetItemPosition()
{
	m_rectMsg				=  CRect(CPoint(6,6),  CSize(210,62));
	m_rect[BTN_POPUP_YES]	=  CRect(CPoint(52,62),  CSize(125,26));
	m_rect[BTN_POPUP_NO]	=  CRect(CPoint(52,97),  CSize(125,26));
}

void CPopupMsgDlg::OnPaint()
{	
	DrawBackground();	
	CRSChildDialog::OnPaint();
}

void CPopupMsgDlg::CleanDC(CDC* pDC)
{
	for(int i = 0; i < BTN_POPUP_CNT - 1; i++)
	{
		CBrush brush = RGB_DEFAULT_DLG;
		pDC->FillRect(m_rect[i], &brush);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CPopupMsgDlg::DrawBackground()
{
	CPaintDC dc(this);

	//CleanDC(&dc);	

	CRect rect;
	GetClientRect(&rect);

	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- Draw Round Dialog
	DrawRoundDialogs(&mDC, TRUE);
	
	switch(m_nType)
	{
	case POPUP_TYPE_ONLY_MESSAGE	:		
		{
			DrawImageBase(&mDC, RSGetRoot(_T("img\\02.Smart Panel\\02_popup_bg01.png")),0, 0);
			DrawOnlyMessage(&mDC);	
		}
		break;
	case POPUP_TYPE_WITH_2BTN		:	
		{
			DrawImageBase(&mDC, RSGetRoot(_T("img\\02.Smart Panel\\02_popup_bg02.png")),0, 0);
			DrawWith2Btn(&mDC);
		}
		break;			
	}
		
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawOnlyMessage
//! @date		2011-8-10
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CPopupMsgDlg::DrawOnlyMessage(CDC* pDC)
{
	CRect rect;
	this->GetClientRect(&rect);
	
	CSize szStr = pDC->GetOutputTextExtent(m_strMsg);

	DrawStr(pDC, STR_SIZE_TEXT_POPUP, m_strMsg , (rect.Width() - szStr.cx) / 2 - 5, (rect.Height() - 16) / 2,RGB(50,51,54));	
}

//------------------------------------------------------------------------------ 
//! @brief		Width Button Ctrl
//! @date		2011-8-11
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CPopupMsgDlg::DrawWith2Btn(CDC* pDC)
{
	CSize szStr = pDC->GetOutputTextExtent(m_strMsg);

	DrawStr(pDC, STR_SIZE_TEXT_POPUP, m_strMsg , (m_rectMsg.Width() - szStr.cx) / 2 - 5, (m_rectMsg.Height() - 16) / 2,RGB(50,51,54));	

	DrawBtn(pDC, BTN_POPUP_YES	);
	DrawBtn(pDC, BTN_POPUP_NO	);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBtn
//! @date		2011-8-11
//! @author		Lee JungTaek
//! @note	 	Button Draw
//------------------------------------------------------------------------------ 
void CPopupMsgDlg::DrawBtn(CDC* pDC, int nItem)
{
	int nLeft = 0;	
	COLORREF color;
	switch(nItem)
	{
	case BTN_POPUP_YES	:
		switch(m_nStatus[nItem])
		{
		case PT_POPUP_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_popup_btn_focus.png")),		m_rect[nItem].left, m_rect[nItem].top, m_rect[nItem].Width(), m_rect[nItem].Height());	color = RGB(255,255,255); break;
		case PT_POPUP_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_popup_btn_push.png")),		m_rect[nItem].left, m_rect[nItem].top, m_rect[nItem].Width(), m_rect[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_popup_btn_normal.png")),	m_rect[nItem].left, m_rect[nItem].top, m_rect[nItem].Width(), m_rect[nItem].Height());	color = RGB(255,255,255); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Yes"),m_rect[nItem],color);	
		break;
	case BTN_POPUP_NO	:
		switch(m_nStatus[nItem])
		{
		case PT_POPUP_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_popup_btn_focus.png")),		m_rect[nItem].left, m_rect[nItem].top, m_rect[nItem].Width(), m_rect[nItem].Height());	color = RGB(255,255,255); break;
		case PT_POPUP_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_popup_btn_push.png")),		m_rect[nItem].left, m_rect[nItem].top, m_rect[nItem].Width(), m_rect[nItem].Height());	color = RGB(255,255,255); break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_popup_btn_normal.png")),	m_rect[nItem].left, m_rect[nItem].top, m_rect[nItem].Width(), m_rect[nItem].Height());	color = RGB(255,255,255); break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("No "),m_rect[nItem],color);	
		break;	
	}
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CPopupMsgDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	m_nMouseIn = PtInRect(point, TRUE);
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

void CPopupMsgDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CPopupMsgDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_POPUP_CNT; i ++)
		ChangeStatus(i,PT_POPUP_NOR);
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CPopupMsgDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	

	int x = 0 , y = 0;

	switch(nItem)
	{
	case BTN_POPUP_YES	:	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)m_pMsg->message, (LPARAM)m_pMsg);		break;
	case BTN_POPUP_NO	:	break;
	}

	DestroyWindow();

	CDialog::OnLButtonDown(nFlags, point);	
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CPopupMsgDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ChangeStatusAll();	
	CDialog::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CPopupMsgDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = PT_POPUP_IN_NOTHING;
	int nStart = 0;
	for(int i = nStart ; i < BTN_POPUP_CNT; i ++)
	{
		if(m_rect[i].PtInRect(pt))
		{
			nReturn = i;
			if(bMove)
				ChangeStatus(i,PT_POPUP_INSIDE);
			else
				ChangeStatus(i,PT_POPUP_DOWN); 
		}
		else
			ChangeStatus(i,PT_POPUP_NOR); 
	}
	return nReturn;
}
//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CPopupMsgDlg::ChangeStatus(int nItem, int nStatus)
{
	if(m_nStatus[nItem] != nStatus)
	{
		m_nStatus[nItem] = nStatus;
		CPaintDC dc(this);
		DrawBtn(&dc, nItem);
		InvalidateRect(m_rect[nItem],TRUE);
	}
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CPopupMsgDlg::ChangeStatusAll()
{
	int nStart = 0;
	for(int i = nStart; i < BTN_POPUP_CNT; i ++)
	{
		if(m_nMouseIn == i)
			ChangeStatus(i,PT_POPUP_DOWN); 
		else
			ChangeStatus(i,PT_POPUP_NOR); 
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		OnTimer
//! @date		2011-8-10
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CPopupMsgDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
	case timer_start_1sec:
		{
			KillTimer(timer_start_1sec);
			DestroyWindow();			
		}
		break;
	case timer_start_3sec:
		{
			KillTimer(timer_start_3sec);
			DestroyWindow();
		}
		break;
	case timer_start_5sec:
		{
			KillTimer(timer_start_5sec);
			DestroyWindow();
		}
		break;
	}
}