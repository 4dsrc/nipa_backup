/////////////////////////////////////////////////////////////////////////////
//
//  MultiApplyDlg.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "afxwin.h"
#include "afxcmn.h"
#include "BkDialogST.h"
#include "SmartPanelEditDlg.h"
#include "NXRemoteStudioFunc.h"

// CMultiApplyDlg dialog

enum
{
	MA_LINE_NULL = -1,
	MA_LINE_FIRST,
	MA_LINE_SECOND,
	MA_LINE_THIRD,
	MA_LINE_FOURTH,
	MA_LINE_CNT,
};

enum camera_cnt
{
	CAMERA_CNT_1 = 0,
	CAMERA_CNT_2	,
	CAMERA_CNT_3	,
	CAMERA_CNT_4	,
	CAMERA_SIZE		,
};

enum
{
	MA_CAMERA_MODE_NULL = -1,
	MA_CAMERA_MODE_SMARTAUTO,
	MA_CAMERA_MODE_P,
	MA_CAMERA_MODE_A,
	MA_CAMERA_MODE_M,
	MA_CAMERA_MODE_S,
};

enum
{
	MA_CAMERA_UNCHECK_NULL = -1,
	MA_CAMERA_UNCHECK,
	MA_CAMERA_CHECK,
	MA_CAMERA_DISABLE,
	MA_CAMERA_MAIN,
};

enum
{
	BTN_MA_CHECKBOX1,
	BTN_MA_CHECKBOX2,
	BTN_MA_CHECKBOX3,
	BTN_MA_CHECKBOX4,
	BTN_MA_CANCLE,
	BTN_MA_SELECT,
	BTN_MA_OK,
	BTN_MA_CNT,
};

enum
{
	PT_MA_IN_NOTHING = -1,
	PT_MA_NOR,
	PT_MA_INSIDE,
	PT_MA_DOWN,
	PT_MA_DISABLE,
};

class CMultiApplyDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CMultiApplyDlg)

public:
	CMultiApplyDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMultiApplyDlg();

// Dialog Data
	enum { IDD = IDD_DLG_SMARTPANEL_MULTIAPPLY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
private:
	CSmartPanelEditDlg * m_pChangeNameEdit;

	//-- Item Rect Info
	CRect m_rectMainCamera[CAMERA_SIZE];
	CRect m_rectName[CAMERA_SIZE];
	CRect m_rectMode[CAMERA_SIZE];
	CRect m_rectBtn[BTN_MA_CNT];

	//-- Item Data
	BOOL m_bMainCamera[CAMERA_SIZE];
	CString m_strDrawName[CAMERA_SIZE];
	CString m_strName[CAMERA_SIZE];
	int m_nMode[CAMERA_SIZE];	
	int m_nCheckBox[CAMERA_SIZE];
	int m_nPreCheckBox[CAMERA_SIZE];

	//-- Set Items Data
	BOOL m_bSelectAll;
	BOOL m_bCheckBox[CAMERA_SIZE];
	int	m_nStatus[BTN_MA_CNT];
	int	m_nMouseIn;
	int m_bEnableSelect;

	//-- Draw Image
	CString GetBackgroundImage();
	void SetItemPosition();
	void DrawBackground();	
	void DrawLine(int nLine, CPaintDC* pDC);
	void DrawMainCamera(int nLine, CPaintDC* pDC);
	void DrawName (int nLine, CPaintDC* pDC);
	void DrawMode (int nLine, CPaintDC* pDC);
	void DrawCheckBox (int nLine, CPaintDC* pDC);
	void DrawMenu(int nItem, CPaintDC* pDC);
	
	//-- Get Camera Infos
	void GetCameraInfo();
	void GetCameraNameAll();
	void GetCameraModeAll();	
	void GetCameraCheckBoxAll();
	CString TrimLongName(CString str, int nPixel);

	//-- To Control Mouse Event
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	int PtInRectName(CPoint pt);
	void ChangeStatus(int nItem, int nStatus);
	void ChangeStatusAll();
	void MouseEvent();
	//-- 2011-7-15 Lee JungTaek
	void SaveSetting(BOOL bSave);
public:
	BOOL IsBankChecked(int nIndex);
	void UpdateBankInfo() {GetCameraInfo(); Invalidate(FALSE);}
};