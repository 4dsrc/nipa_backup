#pragma once
/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelEditDlg.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-07-01
//
/////////////////////////////////////////////////////////////////////////////

// CSmartPanelEditDlg dialog
#include "BKDialogST.h"
#include "PopupMsgDlg.h"
#include "NXRemoteStudioFunc.h"

enum
{
	BTN_EDIT_NULL = -1,
	BTN_EDIT_CANCEL	,
	BTN_EDIT_SAVE	,
	BTN_EDIT_CNT	,
};

enum
{
	PT_EDIT_IN_NOTHING = -1,
	PT_EDIT_NOR,
	PT_EDIT_INSIDE,
	PT_EDIT_DOWN,
};

enum
{
	EDIT_SAVEBANK = 0,
	EDIT_CHANGENAME,
};

class CSmartPanelEditDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CSmartPanelEditDlg)

public:
	CSmartPanelEditDlg(int nType = FALSE, int nIndex = -1, CString strText = _T(""), CWnd* pParent = NULL);   // standard constructor
	virtual ~CSmartPanelEditDlg();

// Dialog Data
	enum { IDD = IDD_DLG_SMARTPANEL_EDIT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	//-- Item Info	
	int		m_nIndex;
	int		m_nType;
	int		m_nMouseIn;
	int		m_nStatus[BTN_EDIT_CNT];
	CRect	m_rect[BTN_EDIT_CNT];

	CString m_strText;
	CEdit m_edit;
	CPopupMsgDlg* m_pPopupMsg;

	//-- Draw Item
	void DrawBackground();	
	void DrawItem(CDC* pDC, int nItem);
	void CleanDC(CDC* pDC);

	//-- Control Mouse Event
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	void ChangeStatus(int nItem, int nStatus);
	void ChangeStatusAll();
	void MouseEvent();

	//-- Action
	void SaveDSCName();
	void SaveBank();
};
