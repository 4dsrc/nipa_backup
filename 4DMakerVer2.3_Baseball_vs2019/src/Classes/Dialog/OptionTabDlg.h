/////////////////////////////////////////////////////////////////////////////
//
//  OptionTabDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "RSChildDialog.h"
#include "SRSIndex.h"
#include "NXRemoteStudioFunc.h"

// COptionTabDlg dialog
class COptionTabDlg : public CRSChildDialog
{
	DECLARE_DYNAMIC(COptionTabDlg)	
public:
	COptionTabDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionTabDlg();

// Dialog Data
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	enum { IDD = IDD_DLG_BOARD_OPT};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();	
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);	
	DECLARE_MESSAGE_MAP()
	
private:	

	int m_nSelectedTab;
	CRect m_btnRect[OPT_TAB_CNT];
	//-- 2011-05-18 hongsu.jung
	void UpdateFrames();
	int PtInRect(CPoint pt, BOOL bMove = TRUE);	
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();
	//CMiLRe 20141125 탭 마우스 무브시 하이라이트
	void DrawBackGround(int nOptTab = OPT_TAB_NOTHING);	

	//-- Tooltip
	int m_nToolTipBtn;
	CToolTipCtrl m_ToolTip;	
	//CMiLRe 20140919 모델에 따라 ETC 창 안열림
	CString m_strModel;
public:
	//CMiLRe 20140919 모델에 따라 ETC 창 안열림
	CString GetModel(){return m_strModel;}
	void SetModel(CString strModel) {m_strModel = strModel;}
	//CMiLRe 20141125 탭 마우스 무브시 하이라이트
	BOOL m_bFirstLoad;
};