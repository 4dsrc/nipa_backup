/////////////////////////////////////////////////////////////////////////////
//
//  LiveviewDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "NXRemoteStudioDlg.h"
#include "LiveviewDlg.h"
#include "SRSIndex.h"
//#include "TransDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CLiveviewDlg dialog
IMPLEMENT_DYNAMIC(CLiveviewDlg, CDialogEx)

//------------------------------------------------------------------------------ 
//! @brief		CLiveviewDlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
CLiveviewDlg::CLiveviewDlg(CWnd* pParent /*=NULL*/)
	: CBkDialogST(CLiveviewDlg::IDD, pParent)
{
	m_pDlgLiveviewFrm	= NULL;
	m_pDlgThumbnail		= NULL;
	m_pDlgMultiview		= NULL;
	m_pDlgToolbar		= NULL;
	m_bHide[0]			= FALSE;	//-- Horizon (m_pDlgThumbnail)
//	m_bHide[1]			= FALSE;	//-- Vertical  (m_pDlgMultiview)
	m_bHide[1]			= TRUE;		// dh0.seo 2014-12-16 MutiView 삭제

	m_pDlgZoom			= ((CNXRemoteStudioDlg*)AfxGetMainWnd())->m_pDlgZoom;
	m_bSmartStyle		= FALSE;

	m_nGridLine			= GRID_NULL;
	m_nZoomOpt			= ZOOM_OFF;
	m_rectSideHandle	= CRect(0,0,0,0);
	//-- 2011-07-05 hongsu.jung
	SetShow(FALSE);

	m_nX = 0;
	m_nY = 0;
	m_nMaxX = 0;
	m_nMaxY = 0;
	m_nPointX = 0;
	m_nPointY = 0;
	m_nRightSize = 0;
	m_nBottomSize = 0;
	//CMiLRe 20141118 카메라 선택 시 프로퍼티 로드 중 창 컨트롤 막음
	m_bEnable = TRUE;
	
	//dh0.seo
	m_bMaxState = FALSE;
	m_bCloseState = FALSE;
}
//------------------------------------------------------------------------------ 
//! @brief		~CLiveviewDlg()
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
CLiveviewDlg::~CLiveviewDlg()
{
	if(m_pDlgLiveviewFrm)
	{
		delete m_pDlgLiveviewFrm;
		m_pDlgLiveviewFrm = NULL;
	}	

	if(m_pDlgMultiview)
	{
		delete m_pDlgMultiview ;
		m_pDlgMultiview = NULL;
	}	

	if(	m_pDlgToolbar)
	{
		delete m_pDlgToolbar;
		m_pDlgToolbar= NULL;
	}		

	if(m_pDlgThumbnail)
	{
		delete m_pDlgThumbnail ;
		m_pDlgThumbnail = NULL;
	}

}

void CLiveviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CBkDialogST::DoDataExchange(pDX);	
//	DDX_Control(pDX,  IDC_BTN_MAXIMIZE	,m_btnMax		);
//	DDX_Control(pDX,  IDC_BTN_CANCEL		,m_btnCancel		);
}				

BEGIN_MESSAGE_MAP(CLiveviewDlg, CBkDialogST)
	ON_WM_PAINT()
	ON_WM_SIZE()		
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BTN_CANCEL, OnBnClickedBtnCancel)	
	ON_BN_CLICKED(IDC_BTN_MAXIMIZE, OnBnClickedBtnMaximize)	
	ON_MESSAGE(WM_END_RESIZE_EVENT	,OnEndResizeEvent)
	ON_MESSAGE(WM_LIVEVIEW,OnLiveviewMsg)	
	ON_WM_NCHITTEST()
	ON_WM_WINDOWPOSCHANGED()
	//dh0.seo
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

//dh0.seo
void CLiveviewDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if(m_RectMax.PtInRect(point))
	{
		m_bMaxState = TRUE;
		//Invalidate(FALSE);
		InvalidateRect(m_RectMax,FALSE);
	}
	else if(!m_RectMax.PtInRect(point) && m_bMaxState == TRUE)
	{
		m_bMaxState = FALSE;
		//Invalidate(FALSE);
		InvalidateRect(m_RectMax,FALSE);
	}


	if(m_RectClose.PtInRect(point))
	{
		m_bCloseState = TRUE;
		//Invalidate(FALSE);
		InvalidateRect(m_RectClose,FALSE);
	}
	else if(!m_RectClose.PtInRect(point) && m_bCloseState == TRUE)
	{
		m_bCloseState = FALSE;
		//Invalidate(FALSE);
		InvalidateRect(m_RectClose,FALSE);
	}

	CDialog::OnMouseMove(nFlags, point);
}

void CLiveviewDlg::OnWindowPosChanged(WINDOWPOS* lpwndpos)
{
	//-- 2011-06-28 hongsu.jung
	((CNXRemoteStudioDlg*)GetParent())->HidePopup();	
	CBkDialogST::OnWindowPosChanging(lpwndpos);	
}

// CLiveview message handlers
//CMiLRe 20141120 화면 겹칠때 잔상 남는 현상 수정
BOOL CLiveviewDlg::OnEraseBkgnd(CDC* pDC)
{	
	UpdateFrames();
//	Invalidate(FALSE);
	return CRSChildDialog::OnEraseBkgnd(pDC);
}
void CLiveviewDlg::OnPaint()
{	
	UpdateFrames();
	CDialogEx::OnPaint();
}

BOOL CLiveviewDlg::OnInitDialog()
{
	CBkDialogST::OnInitDialog();

	//dh0.seo
	m_RectMax = CRect(750, 6 , 750+17, 6+17);
	m_RectClose = CRect(770, 6 , 770+17, 6+17);


	//-- 2011-05-30
	CreateFrames();
	//-- 2011-07-01 hongsu.jung	
	UpdateFrames();
	return TRUE; 	
}

void CLiveviewDlg::OnSize(UINT nType, int cx, int cy)
{
	CBkDialogST::OnSize(nType, cx, cy);
	//-- 2011-05-18 hongsu.jung
 	UpdateFrames();
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveviewDlg::UpdateFrames()
{
	//-- Rounding Dialog
	CPaintDC dc(this);
	
	CRect rect;
	GetClientRect(&rect);

	m_nMaxX = rect.Width();
	m_nMaxY = rect.Height();

/*	if(rect.Width() < GetSystemMetrics(SM_CXFULLSCREEN) && rect.Height() < GetSystemMetrics(SM_CYFULLSCREEN))
	{
		m_nX = rect.Width();
		m_nY = rect.Height();
	} */

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- Draw Back Ground
	DrawBackGround(&mDC);
//	Invalidate(FALSE);
	//-- Draw Dialogs
	DrawDialogs(&mDC);

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveviewDlg::CreateFrames()
{
	//-- Skin Button
//	m_btnMax		.LoadBitmaps(IDR_IMG_MAXIMIZE	,NULL,NULL,NULL);
//	m_btnCancel		.LoadBitmaps(IDR_IMG_CANCEL		,NULL,NULL,NULL);

	//-- 2011-05-16 hongsu.jung
	//-- Create Liveview
	m_pDlgLiveviewFrm = new CLiveviewFrmDlg();
	m_pDlgLiveviewFrm->Create(CLiveviewFrmDlg::IDD, this);		

	//-- 2011-5-17 Lee JungTaek
	//-- Create ThumbnailView
	m_pDlgThumbnail = new CThumbnailDlg();
	m_pDlgThumbnail->Create(CThumbnailDlg::IDD, this);		

	//-- 2011-05-18 hongsu.jung
	//-- Create MultiView Dialog	
	m_pDlgMultiview= new CMultiviewDlg();
	m_pDlgMultiview->Create(CMultiviewDlg::IDD, this);	

	//-- 2011-05-30 hongsu.jung
	//-- Create MultiView Dialog	
	m_pDlgToolbar= new CLiveviewToolbarDlg();
	m_pDlgToolbar->Create(CLiveviewToolbarDlg::IDD, this);	
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackGround
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
static const int size_title_w				= 7;
static const int size_title_h				= 30;
static const int size_body_w				= 3;
static const int ratio_x					= 7;
static const int ratio_y					= 17;
void CLiveviewDlg::DrawBackGround(CDC* pDC)
{
	if(!ISSet())
		return;	
	//-- Draw Round Dialog
//	DrawRoundDialogs(pDC);

	CRect rect;
	GetClientRect(&rect);

	//-- BackGround
	pDC->FillRect(rect, &CBrush(RGB(183,183,183)));

	int nImgX, nImgY, nImgW, nImgH;
	CString strPath = RSGetRoot();
	CString strFullPath;
	
	//-- 1. Title Bar
	nImgY		= 0;
	nImgH	= size_title_h;

	nImgX		= 0;			
	nImgW	= size_title_w;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_title_left.png"),strPath);
	DrawImage(pDC, strFullPath, nImgX, nImgY, nImgW, nImgH);

	nImgX		= size_title_w;
	nImgW	= rect.Width() -  2 * size_title_w ;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_title_center.png"),strPath);
	DrawImage(pDC, strFullPath, nImgX, nImgY, nImgW, nImgH);

	nImgX		= rect.Width() - size_title_w;
	nImgW	= size_title_w;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_title_right.png"),strPath);
	DrawImage(pDC, strFullPath, nImgX, nImgY, nImgW, nImgH);

	
	nImgY		= size_title_h;
	nImgH	= rect.Height()-size_title_h;			
	//-- 2.1 Left
	nImgX		= 0;			
	nImgW	= 2;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_bg_left.png"),strPath);
	DrawImage(pDC, strFullPath, nImgX, nImgY, nImgW, nImgH);
	//-- 2.2 Right
	nImgX		= rect.Width() - 2;
	nImgW	= 2;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_bg_right.png"),strPath);
	DrawImage(pDC, strFullPath, nImgX, nImgY, nImgW, nImgH);

	nImgY		= rect.Height()-2;			
	nImgH	= 2;			

	//-- 2.3 Left_Bottom
	nImgX		= 0;			
	nImgW	= 2;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_bg_edge_left.png"),strPath);
	DrawImage(pDC, strFullPath, nImgX, nImgY, nImgW, nImgH);
	//-- 2.3 Middle_Bottom
	nImgX		= 2;			
	nImgW	= rect.Width() -4;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_bg_bottom.png"),strPath);
	DrawImage(pDC, strFullPath, nImgX, nImgY, nImgW, nImgH);
	//-- 2.3 Right_Bottom
	nImgX		= rect.Width() - 2;
	nImgW	= 2;	
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_bg_edge_right.png"),strPath);
	DrawImage(pDC, strFullPath, nImgX, nImgY, nImgW, nImgH);

	//-- 2011-05-31 hongsu.jung
//	DrawDlgTitle(pDC, _T("Liveview"));	
	DrawStr(pDC, STR_SIZE_MIDDLE, _T("Liveview"), 5, 7, RGB(219,219,219));
	//-- 2011-06-07 hongsu.jung
	//-- Draw Scaling Ratio

	//2014-12-11 dh0.seo 주석
/*	if(m_pDlgLiveviewFrm)
	{
		CString strRatio;
		strRatio.Format(_T("%d%%"),m_pDlgLiveviewFrm->GetRatio());
		//-- 2011-07-22 hongsu.jung
		CRect rtBox = CRect(CPoint(5, rect.Height()-(3+11+3)), CSize(50,14));
		strFullPath.Format(_T("%s\\img\\03.Live View\\03_zoom_value.png"),strPath);
		DrawImage(pDC, strFullPath, rtBox);
		rtBox.top += 3;
		DrawStr(pDC, STR_SIZE_SMALL, strRatio, rtBox, RGB(71,71,71));
	} */
	//-- 2011-06-08 hongsu.jung
	//-- Draw Resize Icon
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_window_handle.png"),strPath);
	DrawGraphicsImage(pDC, strFullPath , rect.right - 14, rect.bottom - 14, 10, 10);

	if(rect.right > m_nRightSize || rect.right < m_nRightSize || rect.bottom > m_nBottomSize || rect.bottom < m_nBottomSize)
	{
		Invalidate(FALSE);
	}

	m_nRightSize = rect.right;
	m_nBottomSize = rect.bottom;

	/*800x600
	*/
	m_RectMax = CRect(rect.right - 50, 6 , rect.right - 50+17, 6+17);
	m_RectClose = CRect(rect.right - 30, 6 , rect.right - 30+17, 6+17);
	
	int nStatus = GetWindowShowStatus();
	if(nStatus == SW_MAXIMIZE)
	{
		if(m_bMaxState)
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\00.Common\\00_window_title_right_normal_mouseon.png"))		,m_RectMax); 
		else
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\00.Common\\00_window_title_right_normal_mouseover.png"))		,m_RectMax); 
	}
	else
	{
		if(m_bMaxState)
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\00.Common\\00_window_title_right_max_mouseon.png"))		,m_RectMax); 
		else
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\00.Common\\00_window_title_right_max_mouseover.png"))		,m_RectMax); 
	}
	
	if(m_bCloseState)
		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\00.Common\\00_window_title_right_close_mouseon.png"))		,m_RectClose); 
	else
		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\00.Common\\00_window_title_right_close_mouseover.png"))		,m_RectClose); 
}

//------------------------------------------------------------------------------ 
//! @brief		DrawDialogs
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 

static const int size_btn_size			= 16;
static const int btn_top_margin			= 6;
static const int liveview_top_margin	= 4;
static const int thumbnail_height		= 130;
static const int toolbar_height			= 58;
static const int multiview_width		= 145;
static const int resize_frame			= 10;
static const int bottom_margin			= 20;
static const int side_handle			= 17;
static const int side_right_margin		= 3;

void CLiveviewDlg::DrawDialogs(CDC* pDC)
{	
	if(!m_pDlgLiveviewFrm || !m_pDlgThumbnail || !m_pDlgMultiview ||! m_pDlgToolbar )
		return;

	//-- Top Title.
	CRect rectDialog, rectBtn, rectLiveview, rectThumbnail, rectMultiview;
	GetClientRect(&rectDialog);

	//-- 1. Button
	rectBtn.top		= btn_top_margin;
	rectBtn.bottom	= btn_top_margin + size_btn_size;
	rectBtn.left		= rectDialog.Width() - size_btn_size - btn_top_margin;
	rectBtn.right		= rectDialog.Width() - btn_top_margin;	
//	m_btnCancel		.MoveWindow(rectBtn);

	rectBtn.left		= rectBtn.left - size_btn_size - size_body_w;
	rectBtn.right		= rectBtn.right - size_btn_size - size_body_w;
//	m_btnMax		.MoveWindow(rectBtn);

	//-- Vertical  HIDE (m_pDlgMultiview)
	if(m_bHide[1]) 
	{	
		//-- 2. Liveview Frame MoveWindows
		rectLiveview.top = size_title_h + liveview_top_margin;
		if(m_bHide[0])	rectLiveview.bottom = rectDialog.bottom - toolbar_height - bottom_margin;
		else				rectLiveview.bottom = rectDialog.bottom - thumbnail_height - toolbar_height - bottom_margin;

		rectLiveview.left = liveview_top_margin	;
		rectLiveview.right =  rectDialog.Width() - liveview_top_margin; //- side_handle - 2*side_right_margin;	//dh0.seo 2014-12-16 MultiView 삭제 및 - liveview_top_margin 추가
		m_pDlgLiveviewFrm->MoveWindow(rectLiveview);
		//-- Side Icon
		m_rectSideHandle.top =  rectDialog.Height()/2 - side_handle/2;
		m_rectSideHandle.bottom = m_rectSideHandle.top + side_handle;
		m_rectSideHandle.right = rectDialog.Width() - side_right_margin;	
		m_rectSideHandle.left = m_rectSideHandle.right - side_handle;
		//-- 2011-07-22 hongsu.jung
		//-- Draw Icon			
		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_side_handle01.png")) ,m_rectSideHandle);
		//-- 3.Multiview Frame MoveWindows
		m_pDlgMultiview->ShowWindow(SW_HIDE);
	}
	else	 //-- Vertical  SHOW (m_pDlgMultiview)
	{
		//-- 2. Liveview Frame MoveWindows
		rectLiveview.top = size_title_h + liveview_top_margin;
		if(m_bHide[0])	rectLiveview.bottom = rectDialog.bottom - toolbar_height - bottom_margin;
		else				rectLiveview.bottom = rectDialog.bottom - thumbnail_height - toolbar_height - bottom_margin;
		rectLiveview.left = liveview_top_margin	;
		rectLiveview.right =  rectDialog.Width() - multiview_width- side_handle - 2*side_right_margin;	
		m_pDlgLiveviewFrm->MoveWindow(rectLiveview);

		//-- 3.Multiview Frame MoveWindows
		rectMultiview.top =  rectLiveview.top;
		rectMultiview.bottom = rectDialog.bottom - bottom_margin;
		rectMultiview.left = rectLiveview.right;
		rectMultiview.right = rectMultiview.left + multiview_width;
		//-- 3.Multiview Frame MoveWindows
		m_pDlgMultiview->ShowWindow(SW_SHOW);
		m_pDlgMultiview->MoveWindow(rectMultiview);

		//-- 2011-07-22 hongsu.jung
		//-- Side Icon
		m_rectSideHandle.top =  rectDialog.Height()/2 - side_handle/2;
		m_rectSideHandle.bottom = m_rectSideHandle.top + side_handle;
		m_rectSideHandle.right = rectDialog.Width() - side_right_margin;	
		m_rectSideHandle.left = m_rectSideHandle.right - side_handle;				
		//-- Side Icon
		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\03.Live View\\03_liveview_side_handle02.png")) ,m_rectSideHandle);
	}

	if(m_bHide[0]) //-- Horizon  HIDE (m_pDlgThumbnail)
	{	
		//-- 4. Toolbar Frame MoveWindows
		m_rectToolbar.top = rectLiveview.bottom;
		m_rectToolbar.bottom = m_rectToolbar.top + toolbar_height;
		m_rectToolbar.left = rectLiveview.left;
		m_rectToolbar.right = rectLiveview.right;
		m_pDlgToolbar->MoveWindow(m_rectToolbar);
		//-- 7. Separator (Horizon)
		{
			/*
			rectHorozon.top =  m_rectToolbar.bottom;
			rectHorozon.bottom = rectHorozon.top + separator_width;
			rectHorozon.left = rectLiveview.left;
			rectHorozon.right = rectLiveview.right;
			m_pDlgSeparate[0]->MoveWindow(rectHorozon);
			*/
		}
		//-- 5.Thumbnail Frame MoveWindows
		m_pDlgThumbnail->ShowWindow(SW_HIDE);
	}
	else //-- Horizon  SHOW (m_pDlgThumbnail)
	{	
		//-- 4. Toolbar Frame MoveWindows
		m_rectToolbar.top = rectLiveview.bottom;
		m_rectToolbar.bottom = m_rectToolbar.top + toolbar_height - 20;
		m_rectToolbar.left = rectLiveview.left;
		m_rectToolbar.right = rectLiveview.right;
		m_pDlgToolbar->MoveWindow(m_rectToolbar);
		//-- 7. Separator (Horizon)
		{
			/*
			rectHorozon.top =  m_rectToolbar.bottom;
			rectHorozon.bottom = rectHorozon.top + separator_width;
			rectHorozon.left = rectLiveview.left;
			rectHorozon.right = rectLiveview.right;
			m_pDlgSeparate[0]->MoveWindow(rectHorozon);
			*/
		}
		//-- 5.Thumbnail Frame MoveWindows
		rectThumbnail.top = m_rectToolbar.bottom;
		rectThumbnail.bottom = rectDialog.bottom - bottom_margin;
		rectThumbnail.left = rectLiveview.left;
		rectThumbnail.right = rectLiveview.right;
		m_pDlgThumbnail->ShowWindow(SW_SHOW);
		m_pDlgThumbnail->MoveWindow(rectThumbnail);
	}

	//m_pDlgSeparate[0] ->SendMessage(WM_PAINT);
	//m_pDlgSeparate[0] ->Invalidate(FALSE);
	//m_pDlgSeparate[1] ->SendMessage(WM_PAINT);
	//m_pDlgSeparate[1] ->Invalidate(FALSE);	
	//m_pDlgToolbar		->SendMessage(WM_PAINT);
	//m_pDlgToolbar		->Invalidate(FALSE);	
	//Invalidate(TRUE); 
}

//------------------------------------------------------------------------------ 
//! @brief		DrawLiveview
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT CLiveviewDlg::OnLiveviewMsg(WPARAM w, LPARAM l)
{
	RSEvent* pMsg = NULL;
	pMsg = (RSEvent*)w;
	switch((int)pMsg->message)
	{
		//-- 2011-05-24
		case WM_LIVEVIEW_EVENT_SHIFT:
			{
				if(pMsg->nParam1 == SHIFT_LEFT)
					m_pDlgLiveviewFrm->ShiftLeft();
				else
					m_pDlgLiveviewFrm->ShiftRight();
				//-- Redraw
				m_pDlgLiveviewFrm->DrawLiveview();

				//-- 2011-07-15 hongsu.jung
				//-- Check Zoom
				m_pDlgZoom->GetZoomViewFrm()->RotateSrc(m_pDlgLiveviewFrm->IsRotate());
				if(m_pDlgZoom->IsWindowVisible())
					m_pDlgZoom->SendMessage(WM_PAINT);
			}
			break;
		case WM_LIVEVIEW_EVENT_FULL_SCREEN:
			OnBnClickedBtnMaximize();
			break;
		case WM_LIVEVIEW_GRID_CHANGE:
			ChangeGridOpt(pMsg->nParam1);
			break;
		case WM_LIVEVIEW_ZOOM_WINDOW:
			ChangeZoomOpt(pMsg->nParam1);			
			break;
		case WM_LIVEVIEW_EVENT_SEPARATOR:
			{
				//-- Get Hide/Show Window
				if(pMsg ->nParam1) //- Vertical
					m_bHide[1] = !m_bHide[1];	//-- Vertical  (m_pDlgMultiview)
				else //- Horizon
					m_bHide[0] = !m_bHide[0];	//-- Horizon (m_pDlgThumbnail)
				//UpdateFrames();
				Invalidate(FALSE);
			}
			break;
	}
	//-- delete message pointer
	delete pMsg;
	pMsg = NULL;
	return 0L;
}
void CLiveviewDlg::OnBnClickedBtnCancel()		{ SendMessage(WM_CLOSE);	}
//------------------------------------------------------------------------------ 
//! @brief		GetWindowShowStatus
//! @date		2010-06-08
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
int CLiveviewDlg::GetWindowShowStatus()
{
	WINDOWPLACEMENT wndpl;
	GetWindowPlacement(&wndpl);
	return wndpl.showCmd;
}

int gMonitorLeft = 0;
int gMonitorRight = 0;
int gMonitorTop = 0;
int gMonitorBottom = 0;

BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData)
{
	MONITORINFOEX mi;
	mi.cbSize = sizeof(MONITORINFOEX);
	GetMonitorInfo(hMonitor, &mi); 

	CPoint    pos;                
    GetCursorPos(&pos);        

	if(pos.x > mi.rcMonitor.left && pos.x < mi.rcMonitor.right)
	{
		gMonitorLeft = mi.rcMonitor.left;
		gMonitorRight = mi.rcMonitor.right;
		gMonitorTop = mi.rcMonitor.top;
		gMonitorBottom = mi.rcMonitor.bottom;
	}
	return TRUE;
}

void CLiveviewDlg::GetMonitorFullSize()
{
	HDC hDC = GetDC()->GetSafeHdc();
	::EnumDisplayMonitors(NULL, NULL, MonitorEnumProc, (LPARAM)hDC);
}

void CLiveviewDlg::OnBnClickedBtnMaximize()	
{ 
	int nStatus = GetWindowShowStatus();
//	if(m_nMaxX == GetSystemMetrics(SM_CXFULLSCREEN) && m_nMaxY ==GetSystemMetrics(SM_CYFULLSCREEN))
	if(nStatus == SW_MAXIMIZE)
	{
//		m_btnMax		.LoadBitmaps(IDR_IMG_MAXIMIZE	,NULL,NULL,NULL);
		ShowWindow(SW_NORMAL);
/*		CRect rect = CRect(m_nPointX, m_nPointY, m_nX, m_nY);
		MoveWindow(rect); */
	}
	else
	{
		int nMonitor = GetSystemMetrics(SM_CMONITORS);
		GetMonitorFullSize();
//		m_btnMax		.LoadBitmaps(IDR_IMG_WINNORMAL,NULL,NULL,NULL);
		ShowWindow(SW_SHOWMAXIMIZED);

		MoveWindow(gMonitorLeft, gMonitorTop, abs(gMonitorLeft - gMonitorRight), abs(gMonitorTop - gMonitorBottom) ); 
		
/*		int xWidth = GetSystemMetrics(SM_CXFULLSCREEN);
		int yHeight = GetSystemMetrics(SM_CYFULLSCREEN);

		CRect rect = CRect(0, 0, xWidth, yHeight);
		MoveWindow(rect);*/
	}
	Invalidate(FALSE);
}

//-- Reload Image
void CLiveviewDlg::ReloadThumbnailView(CString strFileName)
{
	m_pDlgThumbnail->LoadThumbnail(strFileName);
}

//------------------------------------------------------------------------------ 
//! @brief		SetMultiViewer
//! @date		2010-05-31
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
BOOL CLiveviewDlg::SetMultiViewer(int nAll)
{
	if(!m_pDlgMultiview)
		return FALSE;
	return m_pDlgMultiview->SetMultiViewer(nAll);
}

//------------------------------------------------------------------------------ 
//! @brief		OnGetMinMaxInfo
//! @date		2010-05-31
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveviewDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = LIVEVIEW_DLG_WIDTH;
	lpMMI->ptMinTrackSize.y = LIVEVIEW_DLG_HEIGHT;
	 
	RECT rcWorkArea;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcWorkArea, SPIF_SENDCHANGE);
	 
	lpMMI->ptMaxSize.x = rcWorkArea.right;
	lpMMI->ptMaxSize.y = rcWorkArea.bottom;

	CRSChildDialog::OnGetMinMaxInfo(lpMMI);
}
LRESULT CLiveviewDlg::OnEndResizeEvent(WPARAM w, LPARAM l)
{
	int left = HIWORD(w);
	int right = LOWORD(w);
	int top = HIWORD(l);
	int bottom = LOWORD(l);

	MoveWindow(left,top,right-left,bottom-top,1);
	mouse_event( MOUSEEVENTF_LEFTUP,0,0,0,0);
	return 0L;
}

void CLiveviewDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	//CMiLRe 20141118 카메라 선택 시 프로퍼티 로드 중 창 컨트롤 막음
	if(m_bEnable)
	{
		int nItem = PtInRect(point);
		switch(nItem)
		{
		case BTN_SIDE_HANDLE:
			m_bHide[1] = !m_bHide[1];	//-- Vertical  (m_pDlgMultiview)
			Invalidate(FALSE);
			break;
		}
	}

	if(m_RectMax.PtInRect(point))
	{
		int nStatus = GetWindowShowStatus();
		if(nStatus == SW_MAXIMIZE)
		{
			m_bMaxState = FALSE;
			m_bCloseState = FALSE;
			ShowWindow(SW_NORMAL);
		}
		else
		{
			m_bMaxState = FALSE;
			m_bCloseState = FALSE;
			int nMonitor = GetSystemMetrics(SM_CMONITORS);
			GetMonitorFullSize();
			ShowWindow(SW_SHOWMAXIMIZED);
			MoveWindow(gMonitorLeft, gMonitorTop, abs(gMonitorLeft - gMonitorRight), abs(gMonitorTop - gMonitorBottom) ); 
		}
		Invalidate(FALSE);
	}

	if(m_RectClose.PtInRect(point))
	{
		m_bMaxState = FALSE;
		m_bCloseState = FALSE;
		SendMessage(WM_CLOSE);
	}


	CBkDialogST::OnLButtonDown(nFlags, point);
}

//CMiLRe 20141118 Enter, ESC 키 누를 시 화면 사라지거나 종료되는 문제
BOOL CLiveviewDlg::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_ESCAPE )
			pMsg->wParam = NULL;

		if(pMsg->wParam == VK_LEFT)
		{
			m_pDlgThumbnail->m_pDlgThumbnailList->ThumbnailLetf();
		}

		if(pMsg->wParam == VK_RIGHT)
		{
			m_pDlgThumbnail->m_pDlgThumbnailList->ThumbnailRight();
		}

		if( pMsg->wParam == VK_RETURN)
			m_pDlgThumbnail->m_pDlgThumbnailList->DrawSelectedImage();

		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CLiveviewDlg::PtInRect(CPoint pt)
{
	int nReturn = BTN_NOTHING;
	if(m_rectSideHandle.PtInRect(pt))
		nReturn = BTN_SIDE_HANDLE;

	Invalidate(FALSE);
	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeGridOpt
//! @date		2011-06-08
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveviewDlg::ChangeGridOpt(int nGridOpt)
{
	//-- Set Option	
	m_nGridLine = nGridOpt;
	GetLiveview()->SetGridOpt(nGridOpt);
	GetLiveview()->Invalidate(FALSE);
	//-- Change Icon
	m_pDlgToolbar->RedrawIcon(BTN_BAR_GRID); 
}

void CLiveviewDlg::ChangeZoomOpt(int nGridOpt)
{
	//-- Set Option
	m_nZoomOpt = nGridOpt;			
	if(nGridOpt == ZOOM_OFF)	m_pDlgZoom->ShowWindow(SW_HIDE);
	else								m_pDlgZoom->ShowWindow(SW_SHOW);
	GetLiveview()->SetZoomOpt(nGridOpt);
	GetLiveview()->Invalidate(FALSE);	
	//-- Change Icon
	m_pDlgToolbar->RedrawIcon(BTN_BAR_ZOOM); 
}

void CLiveviewDlg::SetLiveview(BOOL b)
{
	//-- Main Liveview
	GetLiveview()->SetLiveview(b);
	//-- MultiView
	m_pDlgMultiview->SetLiveview(b);
}

//CMiLRe 20141118 카메라 선택 시 프로퍼티 로드 중 창 컨트롤 막음
void CLiveviewDlg::EnableControl(BOOL bEnable)
{
	 m_bEnable = bEnable;
	 if(m_pDlgMultiview)
		 m_pDlgMultiview->EnableControl(bEnable);
}