/////////////////////////////////////////////////////////////////////////////
//
//  MultiviewDlg.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "Liveview.h"
#include "RSChildDialog.h"
#include "SRSIndex.h"
#include "NXRemoteStudioFunc.h"

// CMultiviewDlg dialog
class CMultiviewDlg : public CRSChildDialog
{
	DECLARE_DYNAMIC(CMultiviewDlg)	
public:
	CMultiviewDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMultiviewDlg();

// Dialog Data
	enum { IDD = IDD_DLG_MULTIVIEW};
	//-- 2011-05-30	
	CLiveview* m_pLiveview[MULTILIVEVIEW_CNT];

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	
	DECLARE_MESSAGE_MAP()

private:
	void DrawMultiView();	
	
	//CBitmapButton m_btnMultiUp;	
	//CBitmapButton m_btnMultiDown;	

	int m_nCurrentIndex;
	int m_nCameraCount;
	int m_nCheckCount;
	BOOL m_bCheckFirst;

public:
	CLiveview* GetLiveview(int n)	{ return m_pLiveview[n]; }
	//-- 2011-05-31 hongsu.jung
	BOOL SetMultiViewer(int );
	void SetLiveview(BOOL );

	int m_nCount;
	//CMiLRe 20141118 카메라 선택 시 프로퍼티 로드 중 창 컨트롤 막음
	BOOL m_bEnable;
	void EnableControl(BOOL bEnable) { m_bEnable = bEnable;}
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
